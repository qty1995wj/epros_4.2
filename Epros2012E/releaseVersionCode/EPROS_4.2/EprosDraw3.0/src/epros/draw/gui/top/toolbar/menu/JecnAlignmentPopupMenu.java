package epros.draw.gui.top.toolbar.menu;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.figure.unit.JecnTempFigureBean;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 排列 右键菜单( 自动对齐、左对齐、水平居中、右对齐、顶端对齐、垂直居中、底端对齐)
 * 
 * @author ZXH
 * @date： 日期：12 30, 2017 时间：11:28:56 AM
 */
public class JecnAlignmentPopupMenu extends JPopupMenu implements ActionListener {
	/** 左对齐 */
	private JMenuItem leftAlignMenu;
	/** 水平居中 */
	private JMenuItem horizontallyCenterMenu;
	/** 右对齐 */
	private JMenuItem rightAlignMenu;
	/** 顶端对齐 */
	private JMenuItem topAlignMenu;
	/** 垂直居中 */
	private JMenuItem verticalCenterMenu;
	/** 底端对齐 */
	private JMenuItem bottomAlignMenu;

	public JecnAlignmentPopupMenu() {
		// 左对齐
		leftAlignMenu = new JMenuItem();
		leftAlignMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("leftDq"));// 左对齐
		// 添加事件监听
		leftAlignMenu.addActionListener(this);
		leftAlignMenu.setName(AlignMenu.leftAlign.toString());
		leftAlignMenu.setIcon(JecnFileUtil.getToolBarImageIcon(AlignMenu.leftAlign.toString()));

		// 水平居中
		horizontallyCenterMenu = new JMenuItem();
		horizontallyCenterMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("centerHorizontally"));// 水平居中
		// 添加事件监听
		horizontallyCenterMenu.addActionListener(this);
		horizontallyCenterMenu.setName(AlignMenu.horizontallyCenter.toString());
		horizontallyCenterMenu.setIcon(JecnFileUtil.getToolBarImageIcon(AlignMenu.horizontallyCenter.toString()));

		// 右对齐
		rightAlignMenu = new JMenuItem();
		rightAlignMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("rightDq"));// 右对齐
		// 添加事件监听
		rightAlignMenu.addActionListener(this);
		rightAlignMenu.setName(AlignMenu.rightAlign.toString());
		rightAlignMenu.setIcon(JecnFileUtil.getToolBarImageIcon(AlignMenu.rightAlign.toString()));

		// 顶端对齐
		topAlignMenu = new JMenuItem();
		topAlignMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("topAlign"));// 顶端对其
		// 添加事件监听
		topAlignMenu.addActionListener(this);
		topAlignMenu.setName(AlignMenu.topAlign.toString());
		topAlignMenu.setIcon(JecnFileUtil.getToolBarImageIcon(AlignMenu.topAlign.toString()));

		// 垂直居中
		verticalCenterMenu = new JMenuItem();
		verticalCenterMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("verticalCenter"));// 垂直居中
		// 添加事件监听
		verticalCenterMenu.addActionListener(this);
		verticalCenterMenu.setName(AlignMenu.verticalCenter.toString());
		verticalCenterMenu.setIcon(JecnFileUtil.getToolBarImageIcon(AlignMenu.verticalCenter.toString()));

		// 底端对齐
		bottomAlignMenu = new JMenuItem();
		bottomAlignMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("alignBottom"));// 底端对齐
		// 添加事件监听
		bottomAlignMenu.addActionListener(this);
		bottomAlignMenu.setName(AlignMenu.bottomAlign.toString());
		bottomAlignMenu.setIcon(JecnFileUtil.getToolBarImageIcon(AlignMenu.bottomAlign.toString()));

		this.add(leftAlignMenu);

		this.add(horizontallyCenterMenu);
		this.add(rightAlignMenu);
		this.add(topAlignMenu);
		this.add(verticalCenterMenu);
		this.add(bottomAlignMenu);
	}

	public void actionPerformed(ActionEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		List<JecnBaseFlowElementPanel> selectFigureList = getSelectFigureList(desktopPane);
		if (selectFigureList.size() < 2) {
			return;
		}
		if (e.getSource() instanceof JMenuItem) {
			// 获取JMenuItem
			JMenuItem menuItem = (JMenuItem) e.getSource();
			String menuName = menuItem.getName();

			AlignMenu alignMenu = AlignMenu.valueOf(menuName);

			JecnBaseFlowElementPanel standardFigure = getStartFigure(selectFigureList);
			selectFigureList.remove(standardFigure);

			// 操作前数据
			JecnUndoRedoData undoData = new JecnUndoRedoData();
			// 操作后数据
			JecnUndoRedoData redoData = new JecnUndoRedoData();

			undoData.recodeFlowElement(selectFigureList);

			switch (alignMenu) {
			case leftAlign:// 左对齐
				leftAlignFigure(selectFigureList, standardFigure, desktopPane);
				break;
			case horizontallyCenter:
				horizontallyCenterFigure(selectFigureList, standardFigure, desktopPane);
				break;
			case rightAlign:
				rightAlignFigure(selectFigureList, standardFigure, desktopPane);
				break;
			case topAlign:
				topAlignFigure(selectFigureList, standardFigure, desktopPane);
				break;
			case verticalCenter:
				verticalCenterFigure(selectFigureList, standardFigure, desktopPane);
				break;
			case bottomAlign:
				bottomAlignFigure(selectFigureList, standardFigure, desktopPane);
				break;
			default:
				break;
			}

			redoData.recodeFlowElement(selectFigureList);
			List<JecnBaseFigurePanel> selectFigures = new ArrayList<JecnBaseFigurePanel>();
			for (JecnBaseFlowElementPanel jecnBaseFlowElementPanel : selectFigureList) {
				selectFigures.add((JecnBaseFigurePanel) jecnBaseFlowElementPanel);
			}

			// 重画图形相关的连接线
			JecnDraggedFiguresPaintLine.paintLineByFigures(selectFigures);
			// 清空所有虚拟图形
			desktopPane.removeTempListAll();
			// 清空虚拟图形
			desktopPane.updateUI();

			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
			desktopPane.getUndoRedoManager().addEdit(unredoProcess);
		}

	}

	private JecnBaseFlowElementPanel getStartFigure(List<JecnBaseFlowElementPanel> selectFigureList) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane.isSelectArea()) {// 圈选，选中元素集合，获取最左侧的元素
			return getMinLeftFigure(selectFigureList);
		}
		return selectFigureList.get(0);
	}

	private JecnBaseFlowElementPanel getMinLeftFigure(List<JecnBaseFlowElementPanel> selectFigureList) {
		JecnBaseFlowElementPanel leftFigure = selectFigureList.get(0);
		for (JecnBaseFlowElementPanel elementPanel : selectFigureList) {
			leftFigure = leftFigure.getX() < elementPanel.getX() ? leftFigure : elementPanel;
		}

		return leftFigure;
	}

	/**
	 * 左对齐
	 * 
	 * @param selectFigureList
	 * @param desktopPane
	 */
	private void leftAlignFigure(List<JecnBaseFlowElementPanel> selectFigureList,
			JecnBaseFlowElementPanel standardFigure, JecnDrawDesktopPane desktopPane) {
		for (JecnBaseFlowElementPanel curFigurePanel : selectFigureList) {
			resetFigureLocal(curFigurePanel, standardFigure.getX(), curFigurePanel.getY());
		}
	}

	private void rightAlignFigure(List<JecnBaseFlowElementPanel> selectFigureList,
			JecnBaseFlowElementPanel standardFigure, JecnDrawDesktopPane desktopPane) {
		for (JecnBaseFlowElementPanel curFigurePanel : selectFigureList) {
			int localX = standardFigure.getX() + standardFigure.getWidth() - curFigurePanel.getWidth();
			resetFigureLocal(curFigurePanel, localX > 0 ? localX : 1, curFigurePanel.getY());
		}
	}

	private void topAlignFigure(List<JecnBaseFlowElementPanel> selectFigureList,
			JecnBaseFlowElementPanel standardFigure, JecnDrawDesktopPane desktopPane) {
		for (JecnBaseFlowElementPanel curFigurePanel : selectFigureList) {
			resetFigureLocal(curFigurePanel, curFigurePanel.getX(), standardFigure.getY());
		}
	}

	private void bottomAlignFigure(List<JecnBaseFlowElementPanel> selectFigureList,
			JecnBaseFlowElementPanel standardFigure, JecnDrawDesktopPane desktopPane) {
		for (JecnBaseFlowElementPanel curFigurePanel : selectFigureList) {
			int localY = standardFigure.getY() + standardFigure.getHeight() - curFigurePanel.getHeight();
			resetFigureLocal(curFigurePanel, curFigurePanel.getX(), localY > 0 ? localY : 1);
		}
	}

	private void horizontallyCenterFigure(List<JecnBaseFlowElementPanel> selectFigureList,
			JecnBaseFlowElementPanel standardFigure, JecnDrawDesktopPane desktopPane) {
		for (JecnBaseFlowElementPanel curFigurePanel : selectFigureList) {
			double localX = standardFigure.getX() + standardFigure.getWidth() / 2.0 - curFigurePanel.getWidth() / 2.0;
			resetFigureLocal(curFigurePanel, localX > 0 ? DrawCommon.convertDoubleToInt(localX) : 1, curFigurePanel
					.getY());
		}
	}

	private void verticalCenterFigure(List<JecnBaseFlowElementPanel> selectFigureList,
			JecnBaseFlowElementPanel standardFigure, JecnDrawDesktopPane desktopPane) {
		for (JecnBaseFlowElementPanel curFigurePanel : selectFigureList) {
			double localY = standardFigure.getY() + standardFigure.getHeight() / 2.0 - curFigurePanel.getHeight() / 2.0;
			resetFigureLocal(curFigurePanel, curFigurePanel.getX(), localY > 0 ? DrawCommon.convertDoubleToInt(localY)
					: 1);
		}
	}

	private void resetFigureLocal(JecnBaseFlowElementPanel curFigurePanel, int x, int y) {
		// 获取图形元素对象
		JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) curFigurePanel;
		// 获取拖动时生成的虚拟图形
		JecnTempFigureBean tempFigureBean = curFigure.getTempFigureBean();
		// 设置图形位置点和大小
		tempFigureBean.reSetTempFigureBean(new Point(x, y), curFigure.getWidth(), curFigure.getHeight());
		// 拖动图形重设图形位置点、大小
		curFigure.moveFigureResetLocation();
		// 重置显示点
		JecnPaintFigureUnit.showResizeHandles(curFigure);
	}

	private List<JecnBaseFlowElementPanel> getSelectFigureList(JecnDrawDesktopPane desktopPane) {
		List<JecnBaseFlowElementPanel> panelList = desktopPane.getCurrentSelectElement();
		List<JecnBaseFlowElementPanel> selectFigureList = new ArrayList<JecnBaseFlowElementPanel>();
		for (JecnBaseFlowElementPanel elementPanel : panelList) {
			if (elementPanel instanceof JecnBaseFigurePanel) {
				selectFigureList.add(elementPanel);
			}
		}
		return selectFigureList;
	}

	enum AlignMenu {
		leftAlign, horizontallyCenter, rightAlign, topAlign, verticalCenter, bottomAlign
	}
}
