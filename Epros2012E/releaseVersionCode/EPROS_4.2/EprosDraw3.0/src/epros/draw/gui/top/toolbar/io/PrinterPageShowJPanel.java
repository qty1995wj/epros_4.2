package epros.draw.gui.top.toolbar.io;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import epros.draw.util.JecnUIUtil;

/***
 * 打印纸张显示面板
 * @author fuzhh
 *
 */
public class PrinterPageShowJPanel extends JPanel {

    private int w;
    private int h;

    public PrinterPageShowJPanel(int w, int h) {
        this.w = w;
        this.h = h;
        // 设置大小
        this.setPreferredSize(new Dimension(w, h));
        // 设置背景色
        this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
        this.setBorder(new MatteBorder(1, 1, 2, 2, Color.black));
    }

    @Override
    public Dimension getPreferredSize() {
        Insets ins = getInsets();
        return new Dimension(this.w + ins.left + ins.right, this.h + ins.top
                + ins.bottom);
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }
}
