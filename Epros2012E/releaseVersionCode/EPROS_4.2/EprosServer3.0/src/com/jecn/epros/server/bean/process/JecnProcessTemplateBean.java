package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JecnProcessTemplateBean implements Serializable {
	private long id;
	private String type;// 类型 totalMap：流程地图partMap：流程图orgMap： 组织图
	private Blob content;
	private byte[] bytes;
	private Long createPeopleId;// 创建人peopleId
	private Date createDate;// 创建时间
	private Date updateDate;// 更新时间
	private Long updatePeopleId;// 更新人ID
	private List<JecnProcessTemplateFollowBean> listJecnProcessTemplateFollowBean;// 模板图标数据list

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Blob getContent() {
		return content;
	}

	public void setContent(Blob content) {
		this.content = content;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

	public List<JecnProcessTemplateFollowBean> getListJecnProcessTemplateFollowBean() {
		if (listJecnProcessTemplateFollowBean == null) {
			listJecnProcessTemplateFollowBean = new ArrayList<JecnProcessTemplateFollowBean>();
		}
		return listJecnProcessTemplateFollowBean;
	}

	public void setListJecnProcessTemplateFollowBean(
			List<JecnProcessTemplateFollowBean> listJecnProcessTemplateFollowBean) {
		this.listJecnProcessTemplateFollowBean = listJecnProcessTemplateFollowBean;
	}
}
