package epros.designer.gui.system.config.ui.property.type;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextArea;
import epros.designer.gui.system.config.ui.comp.JecnDocumentListener;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 邮件配置
 * 
 * @author ZHOUXY
 * 
 */
public class JecnMailSetPanel extends JecnPanel {
	/** 邮箱主面板 */
	private JecnMailPanel mailPanel = null;
	/** 地址提示Lab */
	private JLabel addrLab = null;
	/** 地址提示Area */
	private JecnConfigTextArea addArea = null;
	/** 地址提示滚动面板 */
	private JScrollPane addrScrollPane = null;

	/** 密码提示Lab */
	private JLabel pwdLab = null;
	/** 密码提示Area */
	private JecnConfigTextArea pwdArea = null;
	/** 密码提示滚动面板 */
	private JScrollPane pwdScrollPane = null;

	/** * 落款Lab */
	private JLabel inscribedLab = null;
	/** * 落款Area */
	private JecnConfigTextArea inscribedArea = null;
	/** 落款滚动面板 */
	private JScrollPane inscribedScrollPane = null;

	/** * 平台名称Lab */
	private JLabel platLab = null;
	/** * 平台Area */
	private JecnConfigTextArea platArea = null;
	/** 平台滚动面板 */
	private JScrollPane platScrollPane = null;

	/** 地址提示信息 */
	private JecnUserInfoTextArea addrInfoTextArea = null;
	/** 密码提示信息 */
	private JecnUserInfoTextArea pwdInfoTextArea = null;
	/** 落款信息 */
	private JecnUserInfoTextArea inscribedInfoTextArea = null;
	/** 平台提示信息 */
	private JecnUserInfoTextArea platTextArea = null;

	/** 选中节点 */
	private JecnConfigTypeDesgBean configTypeDesgBean = null;

	public JecnMailSetPanel(JecnMailPanel mailPanel) {
		this.mailPanel = mailPanel;
		initComponents();
		initLayout();
	}

	private void initComponents() {
		// 地址提示Lab
		addrLab = new JLabel(JecnProperties.getValue("addrPromptC"));
		// 地址提示Area
		addArea = new JecnConfigTextArea();
		// 地址提示滚动面板
		addrScrollPane = new JScrollPane(addArea);

		// 密码提示Lab
		pwdLab = new JLabel(JecnProperties.getValue("pwdPromptC"));
		// 密码提示Area
		pwdArea = new JecnConfigTextArea();
		// 密码提示滚动面板
		pwdScrollPane = new JScrollPane(pwdArea);

		// * 落款Lab
		inscribedLab = new JLabel(JecnProperties.getValue("inscribedC"));
		// * 落款Area
		inscribedArea = new JecnConfigTextArea();
		// 落款滚动面板
		inscribedScrollPane = new JScrollPane(inscribedArea);

		platLab = new JLabel(JecnConfigTool.concatColon(JecnConfigTool
				.getConfigItemName(ConfigItemPartMapMark.mailPlatName)));
		platArea = new JecnConfigTextArea();
		platScrollPane = new JScrollPane(platArea);

		// 地址提示信息
		addrInfoTextArea = new JecnUserInfoTextArea();
		// 密码提示信息
		pwdInfoTextArea = new JecnUserInfoTextArea();
		// 落款信息
		inscribedInfoTextArea = new JecnUserInfoTextArea();
		platTextArea = new JecnUserInfoTextArea();

		this.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("emailSetingLab")));

		addArea.setBorder(null);
		pwdArea.setBorder(null);
		inscribedArea.setBorder(null);
		platArea.setBorder(null);

		// 地址提示
		addrScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		addrScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 密码提示
		pwdScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pwdScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 落款
		inscribedScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		inscribedScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		platScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		platScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 地址提示信息
		addrInfoTextArea.setVisible(false);
		// 密码提示信息
		pwdInfoTextArea.setVisible(false);
		// 落款信息
		inscribedInfoTextArea.setVisible(false);
		platTextArea.setVisible(false);

		// 添加事件
		addArea.getDocument().addDocumentListener(new JecnDocumentListener(mailPanel, addArea, addrInfoTextArea));
		pwdArea.getDocument().addDocumentListener(new JecnDocumentListener(mailPanel, pwdArea, pwdInfoTextArea));
		inscribedArea.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, inscribedArea, inscribedInfoTextArea));
		platArea.getDocument().addDocumentListener(new JecnDocumentListener(mailPanel, platArea, platTextArea));
	}

	private void initLayout() {

		Insets insets = new Insets(5, 5, 5, 5);
		// 地址提示
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, insets, 0, 0);
		this.add(addrLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(addrScrollPane, c);
		// 地址提示信息
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(addrInfoTextArea, c);

		// 密码提示
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(pwdLab, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(pwdScrollPane, c);
		// 密码提示信息
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(pwdInfoTextArea, c);

		// 落款
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(inscribedLab, c);
		c = new GridBagConstraints(1, 4, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(inscribedScrollPane, c);
		// 落款信息
		c = new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(inscribedInfoTextArea, c);

		c = new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(platLab, c);
		c = new GridBagConstraints(1, 6, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(platScrollPane, c);
		c = new GridBagConstraints(1, 7, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(platTextArea, c);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {

		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {// 非表数据
			// 唯一标识
			String mark = itemBean.getMark();
			// 值
			String value = itemBean.getValue();

			if (ConfigItemPartMapMark.mailAddrTip.toString().equals(mark)) {// 邮件地址提示
				addArea.setItemBean(itemBean);
				addArea.setText(value);
			} else if (ConfigItemPartMapMark.mailPwdTip.toString().equals(mark)) {// 邮件密码提示
				pwdArea.setItemBean(itemBean);
				pwdArea.setText(value);
			} else if (ConfigItemPartMapMark.mailEndContent.toString().equals(mark)) { // 邮件落款
				inscribedArea.setItemBean(itemBean);
				inscribedArea.setText(value);
			} else if (ConfigItemPartMapMark.mailPlatName.toString().equals(mark)) { // 平台名称
				platArea.setItemBean(itemBean);
				platArea.setText(value);
			}
		}
	}

	/**
	 * 
	 * 校验
	 * 
	 * @return boolean true：成功；false：失败
	 */
	public boolean check() {

		// 存在错误标识：true，无错：false
		boolean exsitsError = false;

		// 地址提示不能超过1200个字符或600个汉字
		String info = JecnUserCheckUtil.checkNoteLength(addArea.getText());
		exsitsError = mailPanel.setErrorInfo(addrInfoTextArea, info);

		// 密码提示不能超过1200个字符或600个汉字
		info = JecnUserCheckUtil.checkNoteLength(pwdArea.getText());
		boolean pwdBln = mailPanel.setErrorInfo(pwdInfoTextArea, info);
		if (exsitsError || pwdBln) {
			exsitsError = true;
		}

		// 落款不能超过1200个字符或600个汉字
		info = JecnUserCheckUtil.checkNoteLength(inscribedArea.getText());
		boolean inscribedBln = mailPanel.setErrorInfo(inscribedInfoTextArea, info);

		if (exsitsError || inscribedBln) {
			exsitsError = true;
		}

		info = JecnUserCheckUtil.checkNoteLength(platArea.getText());
		boolean platBln = mailPanel.setErrorInfo(platTextArea, info);

		if (exsitsError || platBln) {
			exsitsError = true;
		}

		return exsitsError;
	}
}
