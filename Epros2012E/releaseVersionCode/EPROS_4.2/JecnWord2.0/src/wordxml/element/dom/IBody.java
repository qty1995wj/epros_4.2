package wordxml.element.dom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.WordGraph;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

/**
 * 继承该类的元素 拥有添加段落、表格、图片的能力
 * 
 */
public class IBody {

	// 文档中的所有段落元素
	protected List<Paragraph> paragraphs = new ArrayList<Paragraph>();
	// 文档中的所有表格元素
	protected List<Table> tables = new ArrayList<Table>();
	// 所有内容元素 (表格和段落）
	protected List<IBodyElement> bodyElements = new ArrayList<IBodyElement>();

	/**
	 * 添加段落
	 * 
	 * @author weidp
	 * @date 2014-10-24 下午03:41:52
	 * @return
	 */
	public Paragraph addParagraph(Paragraph p) {
		p.setParent(this);
		this.paragraphs.add(p);
		this.bodyElements.add(p);
		return p;
	}

	/**
	 * 添加一个文本为text的段落，带样式
	 * 
	 * @author weidp
	 * @date 2014-10-25 上午09:45:39
	 * @param text
	 * @return
	 */
	public Paragraph createParagraph(String text, ParagraphBean pBean) {
		return addParagraph(new Paragraph(text, pBean));
	}

	/**
	 * 添加一个文本数组，带样式
	 * 
	 * @author weidp
	 * @date 2014-10-25 上午09:45:39
	 * @param text
	 * @return
	 */
	public void createParagraph(String[] text, ParagraphBean pBean) {
		for (String cotent : text) {
			addParagraph(new Paragraph(cotent, pBean));
		}
	}

	/**
	 * 添加一个文本为text的段落
	 * 
	 * @author weidp
	 * @date 2014-10-25 上午09:45:39
	 * @param text
	 * @return
	 */
	public Paragraph createParagraph(String text) {
		return addParagraph(new Paragraph(text));
	}
	
	
	public void createMutilEmptyParagraph(int num) {
		
		if(num<1){
			return;
		}
		for(int i=0;i<num;i++){
			addParagraph(new Paragraph(""));
		}
	}

	/**
	 * 添加一个图形为graph的段落
	 * 
	 * @author weidp
	 * @date 2014-10-25 上午09:46:10
	 * @param graph
	 * @return
	 */
	public Paragraph createParagraph(WordGraph graph) {
		return addParagraph(new Paragraph(graph));
	}

	/**
	 * 添加一个图形为graph的段落
	 * 
	 * @author weidp
	 * @date 2014-10-25 上午09:46:10
	 * @param graph
	 * @return
	 */
	public Paragraph createParagraph(WordGraph graph, ParagraphBean pBean) {
		return addParagraph(new Paragraph(graph, pBean));
	}

	/**
	 * 添加表格
	 * 
	 * @author weidp
	 * @date 2014-10-14 下午06:48:22
	 * @return
	 */
	public Table addTable(Table tbl) {
		tbl.setParent(this);
		this.tables.add(tbl);
		this.bodyElements.add(tbl);
		return tbl;
	}

	/**
	 * 创建一个表格，带样式
	 * 
	 * @author weidp
	 * @date 2014-10-25 上午10:09:06
	 * @param tableBean
	 * @param cellWidths
	 * @return
	 */
	public Table createTable(TableBean tableBean, float[] cellWidths) {
		return addTable(new Table(tableBean, cellWidths));
	}
	
	

	/**
	 * 创建一个表格
	 * 
	 * @author weidp
	 * @date 2014-10-25 上午10:09:06
	 * @param tableBean
	 * @param cellWidths
	 * @return
	 */
	public Table createTable(float[] cellWidths) {
		return addTable(new Table(cellWidths));
	}

	/**
	 * 获取当前对象内的所有段落
	 * 
	 * @author weidp
	 * @date 2014-10-15 上午09:40:12
	 * @return
	 */
	public List<Paragraph> getParagraphs() {
		if (paragraphs.size() == 0) {
			return Paragraph.defaultParagraphs();
		}
		return Collections.unmodifiableList(paragraphs);
	}

	/**
	 * 获取当前对象内的段落
	 * 
	 * @author weidp
	 * @date 2014-10-15 上午09:40:12
	 * @return
	 */
	public Paragraph getParagraph(int pos) {
		return (pos < 0 || pos >= paragraphs.size()) ? null : paragraphs
				.get(pos);
	}

	/**
	 * 获取当前对象内的所有元素
	 * 
	 * @author weidp
	 * @date 2014-10-15 上午09:40:38
	 * @return
	 */
	public List<IBodyElement> getBodyElements() {
		return Collections.unmodifiableList(bodyElements);
	}

	/**
	 * 获取当前对象内的所有表格
	 * 
	 * @author weidp
	 * @date 2014-10-15 上午09:41:03
	 * @return
	 */
	public List<Table> getTables() {
		return Collections.unmodifiableList(tables);
	}

}
