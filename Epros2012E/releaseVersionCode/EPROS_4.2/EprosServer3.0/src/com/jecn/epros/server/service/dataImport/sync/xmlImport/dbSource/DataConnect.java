package com.jecn.epros.server.service.dataImport.sync.xmlImport.dbSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

import com.jecn.epros.server.service.file.impl.FileServiceImpl;

/**
 * 
 * 数据库操作
 * 
 * @author Administrator
 * 
 */
public class DataConnect {
	private static Logger log = Logger.getLogger(DataConnect.class);
	/**
	 * 创建数据库连接
	 * 
	 * @param xpath
	 *            xml配置文件路径
	 * @return conn 获得连接
	 */
	public static Connection getConn(String xpath) {
		Connection conn = null;
		try {
			SourceParse.getDataSource(xpath);
			Class.forName(SourceParse.getDriver()).newInstance();
			String url = SourceParse.getUrl();
			conn = DriverManager.getConnection(url, SourceParse.getUsername(),
					SourceParse.getPassword());
		} catch (InstantiationException e) {
			log.error("",e);
		} catch (IllegalAccessException e) {
			log.error("",e);
		} catch (ClassNotFoundException e) {
			log.error("",e);
		} catch (SQLException e) {
			log.error("",e);
		} catch (DocumentException e) {
			log.error("",e);
		}
		return conn;
	}

	/**
	 * 
	 * @param conn
	 *            数据库连接
	 * @return stmt 获得stmt
	 */
	public static Statement getStmt(Connection conn) {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			log.error("",e);
		}
		return stmt;
	}

	/**
	 * 
	 * @param conn
	 *            数据库连接
	 * @param sql
	 *            sql语句
	 * @return 获得pstmt
	 */
	public static PreparedStatement getPStme(Connection conn, String sql) {
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
		} catch (SQLException e) {
			log.error("",e);
		}
		return pstmt;
	}

	/**
	 * 
	 * @param stmt
	 * @return 获得结果集 rs
	 */
	public static ResultSet getRs(Statement stmt) {
		ResultSet rs = null;
		try {
			rs = stmt.getResultSet();
		} catch (SQLException e) {
			log.error("",e);
		}
		return rs;
	}

	/**
	 * 
	 * 关闭连接
	 * 
	 * @param rs
	 * @param stmt
	 * @param conn
	 */
	public static void close(ResultSet rs, Statement stmt, Connection conn) {
		if (rs != null) {
			try {
				rs.close();
				rs = null;
			} catch (SQLException e) {
				log.error("",e);
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
				stmt = null;
			} catch (SQLException e) {
				log.error("",e);
			}
		}
		if (conn != null) {
			try {
				conn.close();
				conn = null;
			} catch (SQLException e) {
				log.error("",e);
			}
		}
	}
}
