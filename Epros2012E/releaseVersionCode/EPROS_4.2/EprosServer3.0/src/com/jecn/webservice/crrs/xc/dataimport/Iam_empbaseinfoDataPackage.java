/**
 * Iam_empbaseinfoDataPackage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crrs.xc.dataimport;

public class Iam_empbaseinfoDataPackage  extends com.jecn.webservice.crrs.xc.dataimport.BasePackage  implements java.io.Serializable {
    private java.lang.String name;

    private java.math.BigInteger pageSize;

    private java.lang.Boolean hasMore;

    private java.math.BigInteger recordCount;

    private com.jecn.webservice.crrs.xc.dataimport.Iam_empbaseinfoLine[] dataList;

    public Iam_empbaseinfoDataPackage() {
    }

    public Iam_empbaseinfoDataPackage(
           java.lang.String error,
           java.lang.Boolean success,
           java.lang.String name,
           java.math.BigInteger pageSize,
           java.lang.Boolean hasMore,
           java.math.BigInteger recordCount,
           com.jecn.webservice.crrs.xc.dataimport.Iam_empbaseinfoLine[] dataList) {
        super(
            error,
            success);
        this.name = name;
        this.pageSize = pageSize;
        this.hasMore = hasMore;
        this.recordCount = recordCount;
        this.dataList = dataList;
    }


    /**
     * Gets the name value for this Iam_empbaseinfoDataPackage.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this Iam_empbaseinfoDataPackage.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the pageSize value for this Iam_empbaseinfoDataPackage.
     * 
     * @return pageSize
     */
    public java.math.BigInteger getPageSize() {
        return pageSize;
    }


    /**
     * Sets the pageSize value for this Iam_empbaseinfoDataPackage.
     * 
     * @param pageSize
     */
    public void setPageSize(java.math.BigInteger pageSize) {
        this.pageSize = pageSize;
    }


    /**
     * Gets the hasMore value for this Iam_empbaseinfoDataPackage.
     * 
     * @return hasMore
     */
    public java.lang.Boolean getHasMore() {
        return hasMore;
    }


    /**
     * Sets the hasMore value for this Iam_empbaseinfoDataPackage.
     * 
     * @param hasMore
     */
    public void setHasMore(java.lang.Boolean hasMore) {
        this.hasMore = hasMore;
    }


    /**
     * Gets the recordCount value for this Iam_empbaseinfoDataPackage.
     * 
     * @return recordCount
     */
    public java.math.BigInteger getRecordCount() {
        return recordCount;
    }


    /**
     * Sets the recordCount value for this Iam_empbaseinfoDataPackage.
     * 
     * @param recordCount
     */
    public void setRecordCount(java.math.BigInteger recordCount) {
        this.recordCount = recordCount;
    }


    /**
     * Gets the dataList value for this Iam_empbaseinfoDataPackage.
     * 
     * @return dataList
     */
    public com.jecn.webservice.crrs.xc.dataimport.Iam_empbaseinfoLine[] getDataList() {
        return dataList;
    }


    /**
     * Sets the dataList value for this Iam_empbaseinfoDataPackage.
     * 
     * @param dataList
     */
    public void setDataList(com.jecn.webservice.crrs.xc.dataimport.Iam_empbaseinfoLine[] dataList) {
        this.dataList = dataList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Iam_empbaseinfoDataPackage)) return false;
        Iam_empbaseinfoDataPackage other = (Iam_empbaseinfoDataPackage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.pageSize==null && other.getPageSize()==null) || 
             (this.pageSize!=null &&
              this.pageSize.equals(other.getPageSize()))) &&
            ((this.hasMore==null && other.getHasMore()==null) || 
             (this.hasMore!=null &&
              this.hasMore.equals(other.getHasMore()))) &&
            ((this.recordCount==null && other.getRecordCount()==null) || 
             (this.recordCount!=null &&
              this.recordCount.equals(other.getRecordCount()))) &&
            ((this.dataList==null && other.getDataList()==null) || 
             (this.dataList!=null &&
              java.util.Arrays.equals(this.dataList, other.getDataList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getPageSize() != null) {
            _hashCode += getPageSize().hashCode();
        }
        if (getHasMore() != null) {
            _hashCode += getHasMore().hashCode();
        }
        if (getRecordCount() != null) {
            _hashCode += getRecordCount().hashCode();
        }
        if (getDataList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDataList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDataList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Iam_empbaseinfoDataPackage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "Iam_empbaseinfoDataPackage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageSize");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "pageSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hasMore");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "hasMore"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "recordCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "dataList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "iam_empbaseinfoLine"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "line"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
