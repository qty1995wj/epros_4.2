package epros.designer.gui.process.guide.explain;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.bean.process.driver.JecnFlowDriverTimeT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.process.guide.explain.JecnProcessDriveRuleNew.TimeTable.TimeTableMode;
import epros.designer.gui.system.PeopleMutilSelectCommon;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.operationConfig.JecnTextArea;
import epros.draw.gui.operationConfig.JecnTextField;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 新版本的驱动规则
 * 
 * @author hyl
 * 
 */
public class JecnProcessDriveRuleNew extends JecnFileDescriptionComponent {
	private static Logger log = Logger.getLogger(JecnProcessDriveRuleNew.class);

	/** 流程驱动规则Lab */
	protected JLabel flowDriveRuleLab = new JLabel();

	/** 驱动类型Lab */
	protected JLabel driveTypeLab = new JLabel();

	/** 驱动类型ComboBox */
	protected JComboBox driveTypeCombox = new JComboBox(new String[] { JecnProperties.getValue("eventDriven"),
			JecnProperties.getValue("timeDrive"), JecnProperties.getValue("timeAndEventDriven") });

	/** 驱动规则Lab */
	protected JLabel driveRuleLab = new JLabel();

	/** 驱动规则Field */
	protected JecnTextArea driveRuleArea = new JecnTextArea();
	/** 驱动规则滚动面板 */
	protected JScrollPane driveRuleScrollPane = new JScrollPane(driveRuleArea);

	private boolean isShowTime;

	/** 数据 */
	private JecnFlowBasicInfoT jecnFlowBasicInfoT;

	private JecnFlowDriverT jecnFlowDriverT = null;

	private String tipName = "";

	PeopleMutilSelectCommon peopleSelectCommon = null;

	private String[] months = create(1, 12);
	private String[] days = create(1, 31);
	private String[] weeks = createWeeks();
	private String[] hours = create(0, 24);
	private String[] quars = create(1, 3);

	private JTable yearTable = new YearTable();
	private JTable quarTable = new QuarTable();
	private JTable monthTable = new MonthTable();
	private JTable weekTable = new WeekTable();
	private JTable dayTable = new DayTable();
	private JTable curTable = new JTable();

	/** 类型选择Panel */
	protected JecnPanel typeChangePanel = new JecnPanel(530, 20);

	protected JLabel typeChangeLab = new JLabel(JecnProperties.getValue("typeChoice"));

	/** 类型选择ComboBox */
	protected JComboBox typeChangeCombox = new JComboBox(new String[] { "", JecnProperties.getValue("day"),
			JecnProperties.getValue("weeks"), JecnProperties.getValue("month"), JecnProperties.getValue("season"),
			JecnProperties.getValue("years") });

	/** 时间Panel */
	protected JScrollPane timePanel = new JScrollPane();

	/** 频率Lab */
	protected JLabel frequencyLab = new JLabel();

	/** 频率Field */
	protected JecnTextField frequencyField = new JecnTextField(100);

	/** 频率次数Lab */
	protected JLabel orderLab = new JLabel();

	protected JecnPanel timeMainPanel = new JecnPanel();
	protected JecnPanel timeButtonPanel = new JecnPanel();
	protected JButton addBut = new JButton(JecnProperties.getValue("add"));
	protected JButton delBut = new JButton(JecnProperties.getValue("delete"));
	protected JCheckBox selectAll = new JCheckBox(JecnProperties.getValue("fullSelectOrCancel"));

	public JecnProcessDriveRuleNew(int index, String paragrapHeadingName, JecnPanel contentPanel, boolean isRequest,
			JecnFlowBasicInfoT jecnFlowBasicInfoT, JecnDialog jecnDialog, String tip) {
		super(index, paragrapHeadingName, isRequest, contentPanel, tip);
		this.jecnFlowBasicInfoT = jecnFlowBasicInfoT;
		isShowTime = JecnConfigTool.isShowTimeDriverText();
		// 驱动规则
		driveRuleScrollPane.setBorder(null);
		driveRuleScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		driveRuleScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBorder(BorderFactory.createTitledBorder(""));
		JLabel driveTypeLab = new JLabel();
		// 流程驱动类型
		driveTypeLab.setText(JecnProperties.getValue("drivingType"));
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(driveTypeLab, c);
		// 默认显示时间驱动
		driveTypeCombox.setSelectedIndex(1);
		c = new GridBagConstraints(1, 0, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(driveTypeCombox, c);
		// 驱动规则
		driveRuleLab.setText(JecnProperties.getValue("driveRules"));
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(driveRuleLab, c);

		c = new GridBagConstraints(1, 1, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(driveRuleScrollPane, c);
		if (isShowTime) {
			addTimePanel(c, insets);
			List<JecnTreeBean> jecnTreeBeans = null;
			try {
				jecnTreeBeans = ConnectionPool.getProcessAction().getDirverEmailPeopleByFlowId(
						jecnFlowBasicInfoT.getFlowId());
			} catch (Exception e) {
				log.error("JecnProcessDriveRule is error", e);
			}
			peopleSelectCommon = new PeopleMutilSelectCommon(4, centerPanel, insets, jecnTreeBeans, JecnProperties
					.getValue("remindPC"), jecnDialog, false);
		}

		initData();
		initListener();
	}

	private void initListener() {
		addBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JTable table = JecnProcessDriveRuleNew.this.curTable;
				if (table != null) {
					((TimeTableMode) table.getModel()).addRow(createRow());
					flushFrequency();
				}
			}
		});
		delBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JTable table = JecnProcessDriveRuleNew.this.curTable;
				if (table != null) {
					int rowCount = table.getRowCount();
					if (rowCount > 0) {
						boolean hasOne = false;
						for (int i = rowCount - 1; i >= 0; i--) {
							JCheckBox box = (JCheckBox) table.getValueAt(i, 5);
							if (box.isSelected()) {
								((DefaultTableModel) table.getModel()).removeRow(i);
								hasOne = true;
							}
						}
						if (!hasOne) {
							JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("chooseOneRow"));
						}
					}
					flushFrequency();
				}
			}
		});
		selectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean selected = selectAll.isSelected();
				JTable table = JecnProcessDriveRuleNew.this.curTable;
				if (table != null) {
					int rowCount = table.getRowCount();
					if (rowCount > 0) {
						for (int i = rowCount - 1; i >= 0; i--) {
							JCheckBox box = (JCheckBox) table.getValueAt(i, 5);
							box.setSelected(selected);
						}
					}
					table.repaint();
					flushFrequency();
				}
			}
		});

	}

	protected void flushFrequency() {
		if (this.curTable != null) {
			frequencyField.setText(String.valueOf(this.curTable.getRowCount()));
		} else {
			frequencyField.setText(null);
		}
	}

	protected Vector createRow() {
		Vector<Object> v = new Vector<Object>();
		boolean quar = typeChangeCombox.getSelectedIndex() == 4 ? true : false;
		JComboBox m = new JComboBox(quar ? quars : months);
		JComboBox w = new JComboBox(weeks);
		JComboBox d = new JComboBox(days);
		JComboBox h = new JComboBox(hours);
		v.add(null);
		v.add(m);
		v.add(w);
		v.add(d);
		v.add(h);
		v.add(new JCheckBox());
		return v;
	}

	private String[] createWeeks() {
		return new String[] { JecnProperties.getValue("Monday"), JecnProperties.getValue("onTuesday"),
				JecnProperties.getValue("wednesday"), JecnProperties.getValue("thursday"),
				JecnProperties.getValue("friday"), JecnProperties.getValue("saturday"),
				JecnProperties.getValue("sunday") };
	}

	private String[] create(int start, int num) {
		String[] result = new String[num];
		for (int i = 0; i < num; i++) {
			result[i] = String.valueOf(start++);
		}
		return result;
	}

	private void addTimePanel(GridBagConstraints c, Insets insets) {

		typeChangeCombox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				typeChangePerformed(e);
			}
		});

		Insets insetp = new Insets(0, 0, 0, 0);
		c = new GridBagConstraints(0, 2, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insetp, 0, 0);
		centerPanel.add(typeChangePanel, c);
		typeChangePanel.setLayout(new GridBagLayout());
		// typeChangePanel.setBorder(BorderFactory.createTitledBorder(""));
		// typeChangeLab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		typeChangePanel.add(typeChangeLab, c);
		// typeChangeCombox
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		typeChangePanel.add(typeChangeCombox, c);

		// // emptyChange
		// c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
		// insets, 0, 0);
		// typeChangePanel.add(emptyChange, c);

		// 频率 frequencyLab
		frequencyLab.setText(JecnProperties.getValue("frequency"));
		orderLab.setText(JecnProperties.getValue("number"));
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		typeChangePanel.add(frequencyLab, c);
		c = new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		typeChangePanel.add(frequencyField, c);
		c = new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		typeChangePanel.add(orderLab, c);
		c = new GridBagConstraints(0, 3, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insetp, 0,
				0);
		centerPanel.add(timeMainPanel, c);
		initTimePanel();
	}

	private void initTimePanel() {
		// timeMainPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 设置默认背景色
		yearTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		yearTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		monthTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		monthTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		quarTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		quarTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		weekTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		weekTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		dayTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		dayTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());

		timeMainPanel.setLayout(new GridBagLayout());
		timeButtonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		timeButtonPanel.add(addBut);
		timeButtonPanel.add(delBut);
		timeButtonPanel.add(selectAll);
		timePanel.setViewportView(curTable);
		Dimension dimension = new Dimension(530, 100);
		timePanel.setMinimumSize(dimension);
		timePanel.setPreferredSize(dimension);
		Insets insetp = new Insets(0, 0, 0, 0);
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, insetp, 0, 0);
		timeMainPanel.add(timeButtonPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insetp, 0,
				0);
		timeMainPanel.add(timePanel, c);
	}

	private void initData() {
		if (jecnFlowBasicInfoT.getDriveType() != null) {
			if (jecnFlowBasicInfoT.getDriveType() == 0) {
				driveTypeCombox.setSelectedIndex(0);
			} else if (jecnFlowBasicInfoT.getDriveType() == 1) {
				driveTypeCombox.setSelectedIndex(1);
			} else {
				driveTypeCombox.setSelectedIndex(2);
			}
		}

		driveRuleArea.setRows(4);

		driveRuleArea
				.setText(this.jecnFlowBasicInfoT.getDriveRules() == null ? "" : jecnFlowBasicInfoT.getDriveRules());
		try {
			jecnFlowDriverT = ConnectionPool.getProcessAction().getJecnFlowDriverT(jecnFlowBasicInfoT.getFlowId());
			if (jecnFlowDriverT == null) {
				jecnFlowDriverT = new JecnFlowDriverT();
			}
		} catch (Exception e) {
			log.error("jecnProcessDriveRule initData is error", e);
		}
		if (jecnFlowDriverT != null) {
			// 类型选择选中项设置
			String quantity = null;
			// 频率
			String strFrequency = null;
			quantity = jecnFlowDriverT.getQuantity();
			strFrequency = jecnFlowDriverT.getFrequency();
			if (quantity != null && !"".equals(quantity)) {
				if ("1".equals(quantity)) {// 日
					typeChangeCombox.setSelectedIndex(1);
				} else if ("2".equals(quantity)) {// 周
					typeChangeCombox.setSelectedIndex(2);
				} else if ("3".equals(quantity)) {// 月
					typeChangeCombox.setSelectedIndex(3);
				} else if ("4".equals(quantity)) {// 季度
					typeChangeCombox.setSelectedIndex(4);
				} else if ("5".equals(quantity)) {// 年
					typeChangeCombox.setSelectedIndex(5);
				}
			}
			initCurTable(quantity);
			initTime(quantity);
			frequencyField.setText(strFrequency);
		}
	}

	private void initTime(String quantity) {
		if (curTable != null) {
			boolean quar = "4".equals(quantity);
			((DefaultTableModel) curTable.getModel()).setRowCount(0);
			Vector<Vector<Object>> content = updateTableContent(jecnFlowDriverT.getTimes(), quar);
			if (JecnUtil.isNotEmpty(content)) {
				for (Vector<Object> c : content) {
					((DefaultTableModel) curTable.getModel()).addRow(c);
				}
			}
		}
	}

	private void initCurTable(String quantity) {
		JTable table = null;
		if ("1".equals(quantity)) {// 日
			table = dayTable;
		} else if ("2".equals(quantity)) {// 周
			table = weekTable;
		} else if ("3".equals(quantity)) {// 月
			table = monthTable;
		} else if ("4".equals(quantity)) {// 季度
			table = quarTable;
		} else if ("5".equals(quantity)) {// 年
			table = yearTable;
		}
		this.curTable = table;
		this.timePanel.setViewportView(table);
		this.timePanel.repaint();
		if (table != null) {
			table.setRowHeight(20);
		}
	}

	/**
	 * 驱动类型选择
	 */
	protected void driveTypeComboxPerformed(ItemEvent e) {
		if (e.getStateChange() == e.SELECTED) {
			if (driveTypeCombox.getSelectedIndex() == 0 || driveTypeCombox.getSelectedIndex() == 2) {
				typeChangeCombox.setEnabled(false);
			} else {
				typeChangeCombox.setEnabled(true);
			}
			flushFrequency();
		}
	}

	private String[] returnStrNum(int startNum, int endNum) {
		List<Integer> strList = new ArrayList<Integer>();
		for (int i = startNum; i <= endNum; i++) {
			strList.add(i);
		}
		Object[] objs = strList.toArray();
		String[] strs = new String[objs.length];
		for (int j = 0; j < objs.length; j++) {
			strs[j] = objs[j].toString();
		}
		return strs;
	}

	protected void addRowToPanel(JecnPanel panel, JComponent leftLabel, int colsp) {
		GridBagConstraints c = new GridBagConstraints(colsp, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0);
		panel.add(leftLabel, c);
	}

	/**
	 * 时间 类型选择
	 */
	protected void typeChangePerformed(ItemEvent e) {
		if (e.getStateChange() == e.SELECTED) {
			initCurTable(String.valueOf(typeChangeCombox.getSelectedIndex()));
		}
	}

	public boolean isUpdate() {
		JecnFlowDriverT jecnFlowDriver = this.getJecnFlowDriver();
		// 流程驱动类型
		if (!DrawCommon.checkStringSame(String.valueOf(driveTypeCombox.getSelectedIndex()), String
				.valueOf(jecnFlowBasicInfoT.getDriveType() == null ? 1 : jecnFlowBasicInfoT.getDriveType()))) {
			return true;
		}
		if (!DrawCommon.checkStringSame(driveRuleArea.getText(), jecnFlowBasicInfoT.getDriveRules())) {
			return true;
		}
		if (this.isShowTime) {
			if (!DrawCommon.checkStringSame(jecnFlowDriver.getFrequency(), jecnFlowDriverT.getFrequency())) {
				return true;
			}

			if (isFlowDriver(jecnFlowDriver.getQuantity(), jecnFlowDriverT.getQuantity())) {
				return true;
			}
			if (peopleSelectCommon != null) {
				if (peopleSelectCommon.isUpdate()) {
					return true;
				}
			}
		}
		if (tableUpdate()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return
	 */
	private boolean tableUpdate() {
		if (JecnUtil.isEmpty(this.jecnFlowDriverT.getTimes()) && this.curTable == null) {
			return false;
		}
		if (this.jecnFlowDriverT.getTimes().size() != this.curTable.getRowCount()) {
			return true;
		}
		Set<String> s1 = driverTimeToSet(this.jecnFlowDriverT.getTimes());
		Set<String> s2 = driverTimeToSet(getTimtes(this.curTable));
		s1.removeAll(s2);
		return s1.size() > 0;
	}

	private Set<String> driverTimeToSet(List<JecnFlowDriverTimeT> times) {
		Set<String> s = new HashSet<String>();
		for (JecnFlowDriverTimeT time : times) {
			s.add(concat(JecnUtil.nullToDeafult(time.getMonth(), 1), JecnUtil.nullToDeafult(time.getWeek(), 1),
					JecnUtil.nullToDeafult(time.getDay(), 1), JecnUtil.nullToDeafult(time.getHour(), 0)));
		}
		return s;
	}

	private String concat(Object... data) {
		StringBuilder b = new StringBuilder();
		for (Object o : data) {
			if (o == null) {
				b.append("");
			} else {
				b.append(o);
			}
			b.append("-");
		}
		return b.toString();
	}

	private boolean isFlowDriver(String quantityNow, String quantityDb) {
		if ("0".equals(quantityNow)) {
			if (DrawCommon.isNullOrEmtryTrim(quantityDb) || "0".equals(quantityDb)) {
				return false;
			} else {
				return true;
			}
		} else {
			if (DrawCommon.checkStringSame(quantityNow, quantityDb)) {
				return false;
			} else {
				return true;
			}
		}
	}

	public boolean validateStr(JLabel verfyLab) {
		if (DrawCommon.checkNoteLength(driveRuleArea.getText().toString())) {
			verfyLab.setText(tipName + JecnProperties.getValue("lengthNotOut"));
			return true;
		}
		return false;
	}

	public String getDriveRules() {
		return this.driveRuleArea.getText().trim();
	}

	public Long getDriveType() {
		return Long.valueOf(driveTypeCombox.getSelectedIndex());
	}

	private Set<Long> getDriverEmailPeopleId() {
		if (peopleSelectCommon == null) {
			return new HashSet<Long>();
		}
		return this.peopleSelectCommon.getResultIdsSet();
	}

	/**
	 * 获取驱动规则数据
	 * 
	 * @author fuzhh Jan 7, 2013
	 * @return
	 */
	public JecnFlowDriverT getJecnFlowDriver() {
		JecnFlowDriverT jecnFlowDriver = new JecnFlowDriverT();
		jecnFlowDriver.setFlowId(jecnFlowBasicInfoT.getFlowId());
		if (this.isShowTime) {
			/** *******************更新流程驱动规则临时表数据********************* */
			String strQuantity = String.valueOf(typeChangeCombox.getSelectedIndex());
			jecnFlowDriver.setQuantity(strQuantity);
			jecnFlowDriver.setFrequency(frequencyField.getText());
			jecnFlowDriver.setDriverEmailPeopleIds(getDriverEmailPeopleId());
			jecnFlowDriver.setTimes(getTimtes(curTable));
		}
		return jecnFlowDriver;
	}

	private List<JecnFlowDriverTimeT> getTimtes(JTable table) {
		List<JecnFlowDriverTimeT> result = new ArrayList<JecnFlowDriverTimeT>();
		if (table == null) {
			return result;
		}
		int count = table.getRowCount();
		Long flowId = this.jecnFlowBasicInfoT.getFlowId();
		for (int i = 0; i < count; i++) {
			JecnFlowDriverTimeT t = new JecnFlowDriverTimeT();
			t.setFlowId(flowId);
			t.setMonth(getValueInteger(table, i, 1));
			t.setWeek(getIndexInteger(table, i, 2) + 1);
			t.setDay(getValueInteger(table, i, 3));
			t.setHour(getValueInteger(table, i, 4));
			result.add(t);
		}
		return result;
	}

	private Integer getIndexInteger(JTable table, int row, int col) {
		JComboBox b = (JComboBox) table.getValueAt(row, col);
		return b.getSelectedIndex();
	}

	private Integer getValueInteger(JTable table, int row, int col) {
		JComboBox b = (JComboBox) table.getValueAt(row, col);
		if (b.getSelectedItem() != null) {
			return Integer.valueOf(b.getSelectedItem().toString());
		}
		return null;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return JecnValidateCommon.validateAreaNotPass(driveRuleArea.getText(), verfyLab, JecnProperties
				.getValue("driveRule"));
	}

	public Vector<Vector<Object>> updateTableContent(List<JecnFlowDriverTimeT> data, boolean quar) {
		Vector<Vector<Object>> vectors = new Vector<Vector<Object>>();
		if (JecnUtil.isNotEmpty(data)) {
			for (JecnFlowDriverTimeT time : data) {
				Integer month = time.getMonth();
				Integer week = time.getWeek();
				Integer day = time.getDay();
				Integer hour = time.getHour();
				Vector<Object> v = new Vector<Object>();
				JComboBox m = new JComboBox(quar ? quars : months);
				JComboBox w = new JComboBox(weeks);
				JComboBox d = new JComboBox(days);
				JComboBox h = new JComboBox(hours);
				v.add(null);
				v.add(m);
				v.add(w);
				v.add(d);
				v.add(h);
				v.add(new JCheckBox());
				vectors.add(v);
				if (month != null) {
					m.setSelectedIndex(month - 1);
				}
				if (week != null) {
					w.setSelectedIndex(week - 1);
				}
				if (day != null) {
					d.setSelectedIndex(day - 1);
				}
				if (hour != null) {
					h.setSelectedIndex(hour);
				}
			}
		}
		return vectors;
	}

	class YearTable extends TimeTable {

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0, 2 };
		}
	}

	class MonthTable extends TimeTable {

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0, 1, 2 };
		}
	}

	class WeekTable extends TimeTable {

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0, 1, 3 };
		}
	}

	class DayTable extends TimeTable {

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0, 1, 2, 3 };
		}
	}

	class QuarTable extends TimeTable {

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0, 2 };
		}
	}

	abstract class TimeTable extends JTable {
		private static final long serialVersionUID = 1L;

		TimeTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			this.getColumnModel().getColumn(1).setCellEditor(new ComButtonEditor(new JComboBox()));
			this.getColumnModel().getColumn(1).setCellRenderer(new ComBoxRenderer());
			this.getColumnModel().getColumn(2).setCellEditor(new ComButtonEditor(new JComboBox()));
			this.getColumnModel().getColumn(2).setCellRenderer(new ComBoxRenderer());
			this.getColumnModel().getColumn(3).setCellEditor(new ComButtonEditor(new JComboBox()));
			this.getColumnModel().getColumn(3).setCellRenderer(new ComBoxRenderer());
			this.getColumnModel().getColumn(4).setCellEditor(new ComButtonEditor(new JComboBox()));
			this.getColumnModel().getColumn(4).setCellRenderer(new ComBoxRenderer());
			this.getColumnModel().getColumn(5).setCellEditor(new CheckButtonEditor(new JCheckBox()));
			this.getColumnModel().getColumn(5).setCellRenderer(new ComBoxRenderer());

			this.getColumnModel().getColumn(5).setMaxWidth(40);
		}

		public TimeTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("");
			if (this instanceof QuarTable) {
				title.add(JecnProperties.getValue("next") + "（）" + JecnProperties.getValue("month"));
			} else {
				title.add(JecnProperties.getValue("month"));
			}
			title.add(JecnProperties.getValue("week"));
			title.add(JecnProperties.getValue("day"));
			title.add(JecnProperties.getValue("hour"));
			title.add(JecnProperties.getValue("select"));
			return new TimeTableMode(new Vector<Vector<Object>>(), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		protected abstract int[] gethiddenCols();

		class TimeTableMode extends DefaultTableModel {
			public TimeTableMode(Vector<Vector<Object>> data, Vector<String> title) {
				super(data, title);

			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return true;
			}
		}

		class ComButtonEditor extends DefaultCellEditor implements ItemListener {
			private JComboBox button;

			public ComButtonEditor(JComboBox checkBox) {
				super(checkBox);
			}

			public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
					int column) {
				if (value == null)
					return null;
				button = (JComboBox) value;
				button.addItemListener(this);
				return (Component) value;
			}

			public Object getCellEditorValue() {
				button.removeItemListener(this);
				return button;
			}

			public void itemStateChanged(ItemEvent e) {
				super.fireEditingStopped();
			}
		}

		class ComBoxRenderer implements TableCellRenderer {

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				if (value == null) {
					return null;
				}
				return (Component) value;
			}
		}

		class CheckButtonEditor extends DefaultCellEditor implements ItemListener {
			private JCheckBox button;

			public CheckButtonEditor(JCheckBox checkBox) {
				super(checkBox);
			}

			public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row,
					int column) {
				if (value == null)
					return null;
				button = (JCheckBox) value;
				button.addItemListener(this);
				return (Component) value;
			}

			public Object getCellEditorValue() {
				button.removeItemListener(this);
				return button;
			}

			public void itemStateChanged(ItemEvent e) {
				super.fireEditingStopped();
			}
		}
	}

}
