package com.jecn.epros.server.dao.popedom;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnFlowFilePosPerm;
import com.jecn.epros.server.bean.popedom.JecnOrgAccessPermissionsT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface IOrgAccessPermissionsDao extends IBaseDao<JecnOrgAccessPermissionsT, Long> {

	/**
	 * @author zhangchen Oct 4, 2013
	 * @description：多个节点
	 * @param relateId
	 *            关联ID
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 *            是组织集合
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0是管理员
	 * 
	 * @param projectId
	 *            项目ID
	 * @param set
	 *            mysql数据库 存在默认子节点的权限
	 * @throws Exception
	 */
	public void savaOrUpdate(Long relateId, int type, String orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception;
	
	/**
	 * @author zhangchen Oct 4, 2013
	 * @description：多个节点
	 * @param relateId
	 *            关联ID
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 *            是组织集合
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0是管理员
	 * 
	 * @param projectId
	 *            项目ID
	 * @param set
	 *            mysql数据库 存在默认子节点的权限
	 * @throws Exception
	 */
	public void savaOrUpdate(Long relateId, int type, List<AccessId> orgId, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception;

	/**
	 * @author zhangchen Oct 4, 2013
	 * @description：单一节点
	 * @param relateId
	 *            关联ID
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 *            是组织集合
	 * @throws Exception
	 */
	public void savaOrUpdate(Long relateId, int type, String orgIds, boolean isSave) throws Exception;

	/**
	 * @author zhangchen Oct 4, 2013
	 * @description：单一节点
	 * @param relateId
	 *            关联ID
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @param orgIds
	 *            是组织集合
	 * @throws Exception
	 */
	public void savaOrUpdate(Long relateId, int type, List<AccessId> Ids, boolean isSave) throws Exception;

	/**
	 * @author yxw 2012-6-12
	 * @description:增加或修改多条记录
	 * @param list
	 * @throws Exception
	 */

	/**
	 * @author yxw 2012-6-15
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的组织权限
	 * @param relateId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getOrgAccessPermissions(Long relateId, int type, boolean isPub) throws Exception;

	public void del(Long relateId, int type, String orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception;
	
	public void del(Long relateId, int type, List<AccessId> orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception;


	public void add(Long relateId, int type, String orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception;
	
	public void add(Long relateId, int type, List<AccessId> orgIds, int orgType, int userType, Long projectId,
			TreeNodeType nodeType) throws Exception;

	public List<JecnFlowFilePosPerm> findFlowFileDownloadPerms(Set<Long> flowSet) throws Exception;
	
	/**
	 * 根据父节点的 权限 添加到本节点
	 * 
	 * @param pId
	 * @param thisId
	 */
	void addPermissionisParentNode(Long pId, Long thisId);


}