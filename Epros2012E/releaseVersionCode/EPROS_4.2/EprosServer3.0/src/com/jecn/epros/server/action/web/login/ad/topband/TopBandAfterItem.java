package com.jecn.epros.server.action.web.login.ad.topband;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.zhongruitongxin.ZhongRuiAfterItem;
import com.jecn.epros.server.util.JecnPath;

/**
 * 
 * 中睿通信单点登录配置信息
 * 
 */
public class TopBandAfterItem {

	private static Logger log = Logger.getLogger(ZhongRuiAfterItem.class);
	// 管理员用户和密码
	public static String SYS_NAME = null;
	public static String SYS_PASSWORD = null;

	public static void start() {
		// 读取配置文件
		InputStream fis = null;
		Properties properties = new Properties();
		try {
			String url = JecnPath.CLASS_PATH + "cfgFile/topband/topband.properties";
			log.info("配置文件路径:" + url);
			fis = new FileInputStream(url);
			properties.load(fis);

			SYS_NAME = properties.getProperty("sys_username");
			SYS_PASSWORD = properties.getProperty("sys_password");
		} catch (Exception e) {
			log.error("解析中睿配置登录配置文件异常", e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					log.error(e);
				}
			}
		}
	}
}
