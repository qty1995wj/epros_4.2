package epros.draw.gui.operationConfig;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnResourceUtil;

/**
 * 关键活动
 * 
 * @author 2012-08-28
 * 
 */
public class DrawKeyActivitiesDialog extends JecnDialog {

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();

	/** 活动编号Lab */
	private JLabel actNumLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activitiesNumbers") + ":");

	/** 活动编号Field */
	private JTextField actNumField = new JecnTextField();

	/** 活动名称Lab */
	private JLabel actNameLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activityTitle") + ":");

	/** 活动名称Field */
	private JTextField actNameField = new JecnTextField();

	/** 活动说明Lab */
	private JLabel activityDesLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activityIndicatingThat") + ":");

	/** 活动说明Area */
	private JTextArea activityDesArea = new JecnTextArea();

	private JScrollPane activityDesScrollPane = new JScrollPane(activityDesArea);

	/** 关键说明Lab */
	private JLabel keyDesLab;

	/** 关键说明Area */
	private JTextArea keyArea = new JecnTextArea();

	private JScrollPane keyScrollPane = new JScrollPane(keyArea);

	/** 确定按钮 */
	private JButton confirmBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("confirm"));

	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("cancel"));

	/** 传入显示的数据 */
	private JecnActivityShowBean activityShowBean;

	/** 点击的table */
	private JTable questionAreaTable;
	/** 点击table的行数 */
	private int selectRow;

	public DrawKeyActivitiesDialog(JecnActivityShowBean activityShowBean, JTable questionAreaTable, int selectRow) {
		this.activityShowBean = activityShowBean;
		this.questionAreaTable = questionAreaTable;
		this.selectRow = selectRow;
		this.setSize(500, 360);
		this.setModal(true);
		this.setResizable(true);
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("keyActivities"));
		if ("1".equals(activityShowBean.getActiveFigure().getFlowElementData().getActiveKeyType())) { // PA
			keyDesLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("note") + ":");
		} else if ("2".equals(activityShowBean.getActiveFigure().getFlowElementData().getActiveKeyType())) { // KSF
			keyDesLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("keySuccessFactors") + ":");
		} else if ("3".equals(activityShowBean.getActiveFigure().getFlowElementData().getActiveKeyType())) { // KCP
			keyDesLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("keyControlPoint") + ":");
		}

		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());

		// 活动说明
		activityDesScrollPane.setBorder(null);
		activityDesScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		activityDesScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 关键说明
		keyScrollPane.setBorder(null);
		keyScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		keyScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 确定
		confirmBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				confirmButAction();
			}
		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButAction();
			}
		});
		initLayout();
		// 初始化数据
		initializationData();
	}

	private void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);
		// infoPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 活动编号
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(actNumLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(actNumField, c);
		// 活动名称
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(actNameLab, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(actNameField, c);
		// 活动说明
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(activityDesLab, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(activityDesScrollPane, c);
		// 关键说明
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(keyDesLab, c);
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(keyScrollPane, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(confirmBut);
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 初始化数据
	 * 
	 * @author fuzhh Aug 28, 2012
	 */
	private void initializationData() {
		// 活动编号
		actNumField.setText(activityShowBean.getActiveFigure().getFlowElementData().getActivityNum());
		// 活动名称
		actNameField.setText(activityShowBean.getActiveFigure().getFlowElementData().getFigureText());
		// 活动说明
		activityDesArea.setText(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow());
		// 关键活动
		keyArea.setText(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShowAndControl());
	}

	/**
	 * 确定事件
	 * 
	 * @author fuzhh Oct 11, 2012
	 */
	private void confirmButAction() {
		// 活动编号
		activityShowBean.getActiveFigure().getFlowElementData().setActivityNum(actNumField.getText());
		// 活动名称
		activityShowBean.getActiveFigure().getFlowElementData().setFigureText(actNameField.getText());
		// 活动说明
		activityShowBean.getActiveFigure().getFlowElementData().setAvtivityShow(activityDesArea.getText());
		// 关键活动
		activityShowBean.getActiveFigure().getFlowElementData().setAvtivityShowAndControl(keyArea.getText());

		questionAreaTable.setValueAt(actNumField.getText(), selectRow, 1);

		questionAreaTable.setValueAt(actNameField.getText(), selectRow, 2);

		questionAreaTable.setValueAt(activityDesArea.getText(), selectRow, 3);

		questionAreaTable.setValueAt(keyArea.getText(), selectRow, 4);

		this.dispose();
	}

	private void cancelButAction() {
		this.dispose();
	}
}
