package epros.designer.gui.system.config.ui.property.type;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.JecnUIUtil;

/**
 * 流程其它配置
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-26 时间：下午04:30:22
 */
public class JecnRuleBasicItemPanel extends JecnAbstractPropertyBasePanel implements ActionListener, ChangeListener {

	
	/**
	 * 下载权限
	 */
	private JecnConfigRadioButton isShowDownLoadRadio = new JecnConfigRadioButton(JecnProperties.getValue("isLab"));
	private JecnConfigRadioButton isHiddenDownLoadRadio = new JecnConfigRadioButton(JecnProperties.getValue("notLab"));
	


	public JecnRuleBasicItemPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
		initLayout();
	}

	private void initComponents() {
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setLayout(new GridBagLayout());
		initDownLoadRadio();

	}


	private void initDownLoadRadio() {
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(isShowDownLoadRadio);
		bgpp.add(isHiddenDownLoadRadio);
		isShowDownLoadRadio.setOpaque(false);
		isHiddenDownLoadRadio.setOpaque(false);
		// 监听
		isShowDownLoadRadio.addActionListener(this);
		isHiddenDownLoadRadio.addActionListener(this);
	}
	


	private void initLayout() {
		
		JecnPanel isDownLoad = getIsDownLoadPanel();
		int gridy = 0;
		// 流程元素编辑点
		GridBagConstraints c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0);
		this.add(isDownLoad, c);
		// 空闲区域
		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0),
				0, 0);
		this.add(new JLabel(), c);
	}



	/**
	 * 是否具有下载权限
	 * @return
	 */
	private JecnPanel getIsDownLoadPanel() {
		
		JecnPanel activityPanel = new JecnPanel();
		activityPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		activityPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("downLoadAccess")));
		activityPanel.add(isShowDownLoadRadio);
		activityPanel.add(isHiddenDownLoadRadio);
		return activityPanel;
	}



	private JecnUserInfoTextArea getInfoLable() {
		return dialog.getOkCancelJButtonPanel().getInfoLabel();
	}

	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 唯一标识
		String mark = null;
		String value = null;
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getTableItemList()) {// 非表数据
			// 唯一标识
			mark = itemBean.getMark();
			// 值
			value = itemBean.getValue();
		 if (ConfigItemPartMapMark.isShowRuleDownLoad.toString().equals(mark)) {// 流程下载权限是否显示：1显示
				initIsDownLoad(itemBean);
			}
		}

	}


	/**
	 * @param itemBean
	 */
	private void initIsDownLoad(JecnConfigItemBean itemBean) {
		isShowDownLoadRadio.setItemBean(itemBean);
		isHiddenDownLoadRadio.setItemBean(itemBean);

		if ("0".equals(itemBean.getValue())) {
			isHiddenDownLoadRadio.setSelected(true);
		} else if ("1".equals(itemBean.getValue())) {
			isShowDownLoadRadio.setSelected(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigRadioButton) {
			// 流程元素编辑点
			JecnConfigRadioButton configRadioButton = (JecnConfigRadioButton) e.getSource();
			if (configRadioButton.isSelected()) {// 选中
				if (configRadioButton == isShowDownLoadRadio) {// 显示
					isShowDownLoadRadio.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				}else if (configRadioButton == isHiddenDownLoadRadio) {// 隐藏
					isHiddenDownLoadRadio.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				}
			}
		}
	}



	private JButton getOkJButton() {
		return dialog.getOkCancelJButtonPanel().getOkJButton();
	}

	@Override
	public boolean check() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		// TODO Auto-generated method stub
		
	}
}
