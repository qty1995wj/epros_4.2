package com.jecn.epros.server.bean.process;

import java.util.Date;

public class FlowFileInputi3GUIDBean implements java.io.Serializable {
	private String guid;
	private Long flowId;
	private Date createDate;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
