package epros.designer.gui.popedom.organization;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 组织/岗位名称重命名
 * 
 * @author 2012-06-12
 * 
 */
public class EditOrgNameDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(EditOrgNameDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	public EditOrgNameDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		//如果是岗位重命名
		if(selectNode.getJecnTreeBean().getTreeNodeType()==TreeNodeType.position){
			super.postRename=true; 
		}
		try {
			this.setName(selectNode.getJecnTreeBean().getName());
		} catch (Exception e) {
			log.error("EditOrgNameDialog is error", e);
		}
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		try {
			if (TreeNodeType.organization.equals(selectNode.getJecnTreeBean()
					.getTreeNodeType())) {
				// 组织重命名
				ConnectionPool.getOrganizationAction().reNameOrg(
						selectNode.getJecnTreeBean().getId(), this.getName());
			} else if (TreeNodeType.position.equals(selectNode.getJecnTreeBean()
					.getTreeNodeType())) {
				// 岗位重命名
				ConnectionPool.getOrganizationAction().reNamePosition(
						selectNode.getJecnTreeBean().getId(), this.getName());
			}
			// 岗位重命名
		} catch (Exception e) {
			log.error("EditOrgNameDialog saveData is error", e);
		}
		// 重命名
		JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
		this.dispose();
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
	}
	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
