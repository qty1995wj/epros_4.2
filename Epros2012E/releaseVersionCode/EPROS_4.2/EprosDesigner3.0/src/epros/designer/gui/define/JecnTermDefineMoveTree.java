package epros.designer.gui.define;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class JecnTermDefineMoveTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(JecnTermDefineMoveTree.class);

	public JecnTermDefineMoveTree(List<Long> listIds) {
		super(listIds);
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new JecnTermDefineMoveTreeListener(this.getListIds(), jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		try {
			list = ConnectionPool.getTermDefinitionAction().getChildDirs(0L);
		} catch (Exception e) {
			log.error(JecnProperties.getValue("termDefineMoveGetDataError"), e);
		}
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.termDefineRoot, JecnProperties
				.getValue("termDefineTree"));
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, this.getListIds());
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
