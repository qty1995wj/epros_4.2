package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.server.action.web.login.ad.hisense.JecnHisenseAfterItem;
import com.jecn.epros.server.bean.dataimport.BaseAndPosBean;
import com.jecn.epros.server.bean.dataimport.BasePosBean;
import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.file.IJecnSyncFileService;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.ICheckUserDataColumns;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.ISysImportService;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.ImportDataFactory;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.AbstractSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.SyncDataFilterFactory;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl.ImportDataFactoryImpl.ImportType;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool.DataErrorType;
import com.jecn.epros.server.util.JecnKey;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpOriDept;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.PosBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 
 * 人员数据导入处理类
 * 
 * @author ZHANGXIAOHU
 * 
 */
public class UserImportBuss extends AbstractFileSaveDownBuss {
	private final Log log = LogFactory.getLog(UserImportBuss.class);

	/** 部门行内 */
	private CheckDeptRow deptCheckRow = null;
	/** 岗位行内 */
	private CheckPosRow checkPosRow = null;
	/** 人员行内 */
	private CheckUserRow checkUserRow = null;
	/** 基准岗位校验 */
	private CheckBasePos checkBasePos = null;
	/** 当前项目ID */
	private Long currProjectID = null;
	/** 人员同步数据变更 */
	private ExportDataOfPersonnelChangesBuss personnelChangesBuss;
	/** 人员同步数据行间校验 */
	private ICheckUserDataColumns userDataColumns = null;
	private ISysImportService importService;
	/** 获取外来的待导入数据对象 */
	protected ImportDataFactory importDataFactory = null;

	@Resource(name = "JecnSyncFileServiceImpl")
	private IJecnSyncFileService syncFileService;

	/**
	 * 
	 * 获取上次导入的项目ID，与当前项目ID做比较
	 * 
	 * 查询部门临时表（TMP_JECN_IO_DEPT）的项目ID：
	 * 
	 * 如果查询出来结果没有值，直接导入；
	 * 
	 * 如果查询结果有数据有且只有一条数据，判断项目ID是否等于当前主项目ID：
	 * 
	 * 如果是执行导入，如果不是不导入提示不能导入
	 * 
	 * 如果查询结果有数据且有多条数据，返回提示不正确
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean checkProjectId() throws Exception {
		// 获取主项目ID
		currProjectID = importService.getProtectedId();
		if (currProjectID == null) {
			return false;
		}
		// 查询部门临时表（TMP_JECN_IO_DEPT）
		List<Long> deptBeanList = importService.checkProjectId();

		if (deptBeanList == null || deptBeanList.size() == 0) {
			return true;
		} else if (deptBeanList.size() >= 2) {
			return false;
		}

		// 获取上次导入的项目ID
		Long projectId = deptBeanList.get(0).longValue();

		// 当前项目ID==上次导入项目ID
		if (currProjectID.equals(projectId)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 获取当前项目ID
	 * 
	 * @return
	 * @throws Exception
	 */
	public Long getCurJecnProject() throws Exception {
		return importService.getProtectedId();
	}

	/**
	 * 
	 * 导入人员数据
	 * 
	 * 部门接口集合：部门编号（唯一）、部门名称、上级部门编号
	 * 
	 * 人员接口集合：人员编号（唯一）、真实姓名、任职岗位编号、岗位名称、岗位所属部门编号、邮箱地址、联系电话、内外网邮件标识(内网：0,外网：1)
	 * 
	 * @param importUser
	 *            AbstractImportUser
	 * @throws Exception
	 */
	public DataErrorType importData(BaseBean baseBean) throws Exception {
		// 默认无验证错误
		DataErrorType errorType = DataErrorType.none;
		// 1.读取数据：通用接口
		if (baseBean == null || baseBean.isNull()) {
			return errorType;
		}
		log.info("读取数据：通用接口");

		// 初始化公司数据过滤类
		AbstractSyncDataFilter syncDataFilter = SyncDataFilterFactory.create(baseBean);
		// 过滤原始数据
		syncDataFilter.filterData();

		// 2.验证数据：行内验证
		// ----------------------验证数据 start
		// 部门

		if (!deptCheckRow.checkDeptRow(baseBean) && !isAllowError()) {// 部门行内验证
			return DataErrorType.rowsType;
		}
		log.info("验证数据：部門行内验证、行间验证结束");
		// 在人员结构下岗位校验(到这校验应该达到完全正确才行)
		if (!checkUserRow.checkUserPosRow(baseBean) && !(isAllowError() || JecnContants.otherLoginType == 23)) {// 人员原始结构行内校验
			return DataErrorType.rowsType;
		}

		// if (checkUserRow.getUserNumberList().size() > JecnKey
		// .getServerPeopleCounts()
		// && !isAllowError()) {// 导入人数大于服务端最大限制
		// log.info("获取导入的人员最大数 checkUserRow.getUserNumberList().size() = "
		// + checkUserRow.getUserNumberList().size());
		// return DataErrorType.serverCount;
		// }

		log.info("3.清除原始数据临时数据，并添加新数据 开始");
		// 3.清除原始数据临时卡，并添加新数据
		importService.deleteAndInsertTmpOriJecnIOTable(baseBean);

		log.info("3.清除原始数据临时数据，并添加新数据 结束");

		log.info("岗位行内校验结束" + System.currentTimeMillis());
		// 4 .人员，部门行间校验
		log.info("验证数据：4 .人员，部门行间校验 开始");

		/** 【 true:不存在部门；如果不存在部门集合，不更新人员中部门和岗位信息(客户是手动创建组织架构-只同步人员) 】 */
		boolean isSyncUserOnly = JecnConfigTool.isSyncUserOnly();

		// TODO 暂时未添加岗位组校验
		if (!userDataColumns.checkUserPosColu(isSyncUserOnly) && !isAllowError()) {// 人员原始结构行间校验
			return DataErrorType.columnsType;
		}
		if (!userDataColumns.checkDeptColu() && !isAllowError()) {// 部门原始结构行间校验
			return DataErrorType.columnsType;
		}
		log.info("验证数据：4 .人员，部门行间校验 结束");

		// 岗位数据校验
		// 从人员中提取出岗位集合
		baseBean.resolveUserPos();
		log.info("从人员中提取出岗位集合" + System.currentTimeMillis());
		// 岗位行内校验
		if (!checkPosRow.checkPosRow(baseBean) && !(isAllowError() || JecnContants.otherLoginType == 23)) {
			return DataErrorType.rowsType;
		}
		// 添加当前项目到DeptBean中
		for (DeptBean deptBean : baseBean.getDeptBeanList()) {
			deptBean.setProjectId(getCurrProjectID());
		}

		// =====================基准岗位同步==============
		log.info("基准岗位同步开始!" + System.currentTimeMillis());

		// 4、基准岗位或岗位组导入
		DataErrorType dataErrorType = importBasePosInfo(baseBean);
		if (dataErrorType.equals(DataErrorType.rowsType)) {
			return dataErrorType;
		}

		// 正式入库前清除错误数据
		syncDataFilter.removeUnusefulDataAfterCheck();

		// 5.清除临时库、入库临时库
		importService.deleteAndInsertTmpJecnIOTable(baseBean);
		// 6.与真实库数据做验证
		// 待导入部门、岗位以及人员中在各自真实库中存在的数据， 更新其操作标识设置为1
		importService.updateProFlag(currProjectID, isSyncUserOnly);

		// 同步同步真实库前获取流程中相关部门，岗位、及人员变更的数据统计
		// 岗位组同步，岗位变更未处理
		personnelChangesExcelExport(isSyncUserOnly);

		// 数据是否异常分析 1、部门或岗位、或人员数据曾或者删超过100 条，需手动同步
		if (!isSyncUserOnly && "1".equals(baseBean.getIsAuto())) {// 自动同步验证
			log.info("自动同步，异常校验开始  + auto = " + baseBean.getIsAuto());
			if (baseBean.getDeptBeanList().size() == 0) {
				return DataErrorType.syncDataOrgNumError;
			} else if (baseBean.getUserBeanList().size() == 0) {
				return DataErrorType.syncDataUserNumError;
			} else {
				String validateMessage = this.importService.syncDataValidate(currProjectID);
				if (StringUtils.isNotBlank(validateMessage)) {
					log.error(validateMessage);
					return DataErrorType.syncDataUserNumError;
				}
			}
		}

		// 7.同步真实库 ***************下面的操作就需要事务处理
		importService.syncImortDataAndDB(baseBean, currProjectID, isSyncUserOnly);

		if (JecnConfigTool.isOpenSyncFileFromOrg()) {
			try {
				// 同步文件
				syncFileService.syncFile(currProjectID);
			} catch (Exception e) {
				log.error("文件同步失败!", e);
				e.printStackTrace();
			}
		}
		// 获取人员同步变更数据信息
		return errorType;
	}

	private DataErrorType importBasePosInfo(BaseBean baseBean) {
		// 获取待导入的基准岗位数据
		if (JecnContants.otherUserSyncType == 8) {
			// 将基准岗位和基准岗位与岗位的关系拆分出来
			log.info("基准岗位与岗位的关系拆分开始！");
			// 岗位编号和 人员岗位的对应关系
			Map<String, PosBean> mapPosBeans = new HashMap<String, PosBean>();
			for (PosBean posBean : baseBean.getPosBeanList()) {
				mapPosBeans.put(posBean.getPosNum(), posBean);
			}
			// BaseAndPosBean 基准岗位和基准岗位与岗位关系集合封装bean
			BaseAndPosBean baseAndPosBean = basePosSplit(baseBean.getBasePosBean(), mapPosBeans);
			// 拆分好的基准岗位集合
			baseBean.setJecnBasePosBeanList(baseAndPosBean.getBasePosBeanList());
			log.info("基准岗位校验开始");
			// 基准岗位校验
			if (checkBasePos.checkBasePos(baseAndPosBean.getBasePosBeanList(), baseAndPosBean.getExistsBasePosNum())) {
				return DataErrorType.rowsType; // 基准岗位校验出错
			}
			// log.info("基准岗位与岗位关系校验开始！");
			// 基准岗位中关联的岗位在人员岗位中不存在
			// checkBasePos.checkBaseRelPos(baseAndPosBean.getNoExistsPosNum(),mapPosBeans);
			// 验证通过，入库正式库（基准岗位同步没有临时库直接入库正式库）
		}
		return DataErrorType.none;
	}

	/**
	 * 基准岗位拆分,基准岗位与岗位的关系拆分
	 * 
	 * @author cheshaowei
	 * @param basePosBeanList
	 *            视图中获取的基准岗位原始数据集合（没有经过数据拆分处理）
	 * 
	 * **/
	private BaseAndPosBean basePosSplit(List<BasePosBean> basePosBeanList, Map<String, PosBean> mapPosBeans) {
		if (basePosBeanList == null || mapPosBeans == null) {
			return null;
		}

		// 基准岗位临时bean
		List<JecnBasePosBean> basePosBeans = new ArrayList<JecnBasePosBean>();
		// 基准岗位与岗位关系临时bean
		// Map<String, List<JecnPoseBean>> mapRelBeans = new HashMap<String,
		// List<JecnPoseBean>>();

		log.info("拆分基准岗位数据开始");
		// 拆分数据
		// JecnPoseBean jecnPoseBean = null;
		JecnBasePosBean jecnBasePosBean = null;

		// 已存在的重复基准岗位编号
		List<String> existsBasePosNum = new ArrayList<String>();
		Map<String, String> basePosNum = new HashMap<String, String>();

		// 基准岗位的岗位在人员岗位中不存在的 岗位编号
		List<BasePosBean> noExistsPosNum = new ArrayList<BasePosBean>();

		for (BasePosBean basePosBean : basePosBeanList) {
			if (SyncTool.isNullOrEmtry(basePosBean.getBasePosNum())) {
				continue;
			}

			// 2、基准岗位的岗位在人员岗位中不存在
			if (mapPosBeans.get(basePosBean.getPosNum()) == null) {
				noExistsPosNum.add(basePosBean);
			}
			// 1、基准岗位是否重复记录
			if (basePosNum.containsKey(basePosBean.getBasePosNum())
					&& !basePosNum.containsValue(basePosBean.getBasePosName())) {
				// 记录重复的基准岗位编号
				existsBasePosNum.add(basePosBean.getBasePosNum());
				// 记录重复的基准岗位
				jecnBasePosBean = new JecnBasePosBean();
				// 基准岗位主键ID
				jecnBasePosBean.setBasePosId(UUID.randomUUID().toString());
				// 基准岗位编号
				jecnBasePosBean.setBasePosNum(basePosBean.getBasePosNum());
				// 基准岗位名称
				jecnBasePosBean.setBasePosName(basePosBean.getBasePosName());
				basePosBeans.add(jecnBasePosBean);
			} else if (!basePosNum.containsKey(basePosBean.getBasePosNum())) {
				basePosNum.put(basePosBean.getBasePosNum(), basePosBean.getBasePosName());
				jecnBasePosBean = new JecnBasePosBean();
				// 基准岗位主键ID
				jecnBasePosBean.setBasePosId(UUID.randomUUID().toString());
				// 基准岗位编号
				jecnBasePosBean.setBasePosNum(basePosBean.getBasePosNum());
				// 基准岗位名称
				jecnBasePosBean.setBasePosName(basePosBean.getBasePosName());

				basePosBeans.add(jecnBasePosBean);
			}
		}

		basePosNum = null;
		return new BaseAndPosBean(basePosBeans, existsBasePosNum, noExistsPosNum);
	}

	/**
	 * 是否允许错误数据（客户非海信的情况下，出现错误数据立即返回）
	 * 
	 * 【允许错误数据只是为了不在校验的过程中被打断，当校验结束后，应当移除BaseBean中的错误数据】
	 * 
	 * @return
	 */
	private boolean isAllowError() {
		// 海信登录:10 并且配置文件中规定了允许错误数据
		if (JecnContants.otherLoginType == 10 && JecnHisenseAfterItem.ALLOW_ERROR) {
			return true;
		}
		return false;
	}

	/**
	 * 同步完成后发送邮件给系统管理员
	 * 
	 * @param result
	 * 
	 * @return
	 * @throws Exception
	 */
	public void sendEmailForImportData(MessageResult result) throws Exception {
		String path = result.isSuccess() ? personnelChangesBuss.getOutputExcelDataPath() : SyncTool
				.getOutPutExcelErrorDataPath();
		this.importService.changeDataSendMailMessageToAdmin(result, path, personnelChangesBuss.isHasChange());
	}

	/**
	 * 人员变更数据导出 excel zhangft 2013-5-29
	 * 
	 * @throws Exception
	 */
	private void personnelChangesExcelExport(boolean isSyncUserOnly) throws Exception {
		personnelChangesBuss.personnelChangesExcelExport(currProjectID, isSyncUserOnly); // 返回boolean值
	}

	/**
	 * 人员变更 ： 同步数据 导出excel
	 * 
	 * @throws Exception
	 */
	private void personnelChangesSyncExcelExport() throws Exception {
		personnelChangesBuss.syncDataExcelExport(currProjectID);// 返回boolean值
	}

	/**
	 * 
	 * 把BaseBean对象中数据生成excel文件存储到本地
	 * 
	 * @param baseBean
	 * @return
	 */
	public boolean saveErrorExcelToLocal(BaseBean baseBean) {
		if (baseBean == null || baseBean.isNull()) {
			return false;
		}
		if (JecnContants.otherUserSyncType == 8) {
			this.setOutputExcelModelPath(SyncTool.getOutputErrorExcelBaseModelPaht());
		} else {
			// 人员错误数据导出模板文件
			this.setOutputExcelModelPath(SyncTool.getOutputErrorExcelModelPath());
		}
		// 人员错误数据导出数据文件
		this.setOutputExcelDataPath(SyncTool.getOutPutExcelErrorDataPath());
		if (JecnContants.otherUserSyncType == 8) {
			// 从DB中读数据写入到excel再保存到本地
			return editExcel(baseBean.getDeptBeanList(), baseBean.getUserPosBeanList(), baseBean
					.getJecnBasePosBeanList(), baseBean.getBasePosBean(), true);
		} else {
			// 从DB中读数据写入到excel再保存到本地
			return editExcel(baseBean.getDeptBeanList(), baseBean.getUserPosBeanList(), true);
		}

	}

	/**
	 * 
	 * 
	 * 删除给定路径的文件
	 * 
	 * @param filePath
	 */
	public void deleteErrorLocalExcel(String filePath) {
		if (filePath == null) {
			return;
		}
		File file = new File(filePath);
		// 文件是否存在存在删除
		if (file.exists()) {
			file.delete();
		}
	}

	/**
	 * 
	 * 把文件存储到本地
	 * 
	 * @param upLoadFile
	 *            待保存文件
	 * @param fileName
	 *            文件名称
	 * @param localPath
	 *            目的地路径
	 * @return String null表示成功，不为null表示失败
	 */
	public String saveExcelToLocal(File upLoadFile, String fileName, String localPath) {

		// 判断给定参数是否合法
		if (SyncTool.isNullOrEmtryTrim(fileName) || SyncTool.isNullObj(upLoadFile)
				|| SyncTool.isNullOrEmtryTrim(localPath)) {
			return SyncErrorInfo.IMPORT_FILE_NULL;
		} else {
			if (!fileName.toUpperCase().endsWith(".XLS") && !fileName.toUpperCase().endsWith(".XLSX")) {
				return SyncErrorInfo.IMPORT_JSP_EXCEL_NAME_NOT_XLS;
			}
		}

		// 本地的输出流
		FileOutputStream outStream = null;
		// 远程excel输入流
		FileInputStream inputStream = null;
		try {
			File fileLocal = new File(localPath);
			// 判断文件是否存在
			if (!fileLocal.exists()) {
				fileLocal.createNewFile();
			}

			// 本地的输出流
			outStream = new FileOutputStream(fileLocal);
			// 远程excel输入流
			inputStream = new FileInputStream(upLoadFile);

			// 读取给定的文件数据存入到指定目录
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = inputStream.read(buffer)) > 0) {
				outStream.write(buffer, 0, len);
			}
			outStream.flush();
			return null;

		} catch (IOException ex) {
			log.error("把远程excel文件存放到本地时错误：" + ex.getMessage());
			return SyncErrorInfo.IMPORT_FILE_SAVE_LOCAL_FAIL;
		} finally {

			if (outStream != null) {
				try {
					outStream.close();
				} catch (IOException e) {
					log.error("把远程excel文件存放到本地时错误：" + e.getMessage());
					return SyncErrorInfo.IMPORT_FILE_SAVE_LOCAL_FAIL;
				}
			}

			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("把远程excel文件存放到本地时错误：" + e.getMessage());
					return SyncErrorInfo.IMPORT_FILE_SAVE_LOCAL_FAIL;
				}
			}
		}
	}

	/**
	 * <<<<<<< .working
	 * 
	 * 编辑部门sheet，把DB中部门数据写入给定sheet中
	 * 
	 * ======= >>>>>>> .merge-right.r31601 获取人员同步原始数据
	 * 
	 * @return
	 */

	public boolean exportExcelOriData() throws Exception {
		List<JecnTmpPosAndUserBean> userBeanList = importService.findAllUserAndPosOri();
		List<JecnTmpOriDept> deptBeanList = importService.findAllDeptOri();
		// 人员错误数据导出模板文件
		this.setOutputExcelModelPath(SyncTool.getOutputErrorExcelModelPath());
		// 人员错误数据导出数据文件
		this.setOutputExcelDataPath(SyncTool.getOutPutExcelErrorDataPath());
		// 人员岗位原始数据集合
		List<UserBean> listUserPosBean = new ArrayList<UserBean>();
		if (userBeanList != null) {
			for (JecnTmpPosAndUserBean posAndUserBean : userBeanList) {
				UserBean userBean = new UserBean();
				userBean.setUserNum(posAndUserBean.getUserNum());
				userBean.setTrueName(posAndUserBean.getTrueName());
				userBean.setPosNum(posAndUserBean.getPosNum());
				userBean.setPosName(posAndUserBean.getPosName());
				userBean.setDeptNum(posAndUserBean.getDeptNum());
				userBean.setEmail(posAndUserBean.getEmail());
				userBean.setEmailType(posAndUserBean.getEmailType());
				userBean.setError(posAndUserBean.getError());
				userBean.setPhone(posAndUserBean.getPhone());
				listUserPosBean.add(userBean);
			}
		}
		// 人员岗位原始数据集合
		List<DeptBean> listDeptBean = new ArrayList<DeptBean>();
		if (userBeanList != null) {
			for (JecnTmpOriDept oriDept : deptBeanList) {
				DeptBean dept = new DeptBean();
				dept.setDeptNum(oriDept.getDeptNum());
				dept.setDeptName(oriDept.getDeptName());
				dept.setPerDeptNum(oriDept.getPerDeptNum());
				dept.setError(oriDept.getError());
				listDeptBean.add(dept);
			}
		}
		// 从DB中读数据写入到excel再保存到本地
		return editExcel(listDeptBean, listUserPosBean, true);
	}

	public MessageResult importDataBuss(boolean isTimer, String importType, File tempFile, String fileName)
			throws Exception {
		MessageResult result = importing(importType, tempFile, fileName);
		// 发送邮件处理
		try {
			sendEmailForImportData(result);
		} catch (Exception e) {
			log.error("发送人员导入信息邮件异常", e);
		}
		return result;
	}

	private MessageResult importing(String importType, File tempFile, String fileName) throws Exception {
		MessageResult result = new MessageResult();
		// 1. 清除错误文件
		deleteErrorLocalExcel(SyncTool.getOutPutExcelErrorDataPath());
		log.info("1. 清除错误文件成功");

		// 2. 判断当前项目ID:
		// 如果查询出来结果没有值，直接导入；
		// 如果查询结果有数据有且只有一条数据，判断项目ID是否等于当前主项目ID：
		// 如果是执行导入，如果不是不导入提示不能导入
		// 如果查询结果有数据且有多条数据，返回提示不正确
		boolean projectB = checkProjectId();
		if (!projectB) {
			// 当前人员同步的项目和上次人员同步的项目不一致，请切换到上次人员同步的项目下后再执行人员同步
			result.setResultMessage(SyncErrorInfo.PROJECT_ID_NOT_SAME_FAIL);
			result.setSuccess(false);
			log.info("导入的项目ID不等于上次同步的项目ID");
			return result;
		}
		log.info(" 2. 判断当前项目ID验证成功");

		// 3. excel读取数据方式:上传excel到服务器本地
		if (ImportType.excel.toString().equals(importType)) {
			String retString = saveExcelToLocal(tempFile, fileName);
			if (!SyncTool.isNullOrEmtryTrim(retString)) {
				result.setResultMessage(retString);
				result.setSuccess(false);
				return result;
			}
		}

		log.info("获取待同步数据开始：" + System.currentTimeMillis());

		// 4. 获取外来的待导入数据
		BaseBean baseBean = importDataFactory.getImportData(importType);
		if (baseBean == null || (baseBean.getDeptBeanList() == null || baseBean.getDeptBeanList().size() == 0)
				&& (baseBean.getUserPosBeanList() == null || baseBean.getUserPosBeanList().size() == 0)) {// 不存在导入数据
			log.info("获取外来的待导入数据为空！");
			result.setResultMessage("获取外来的待导入数据为空！");
			result.setSuccess(false);
			return result;
		}
		AbstractConfigBean initTime = importDataFactory.getInitTime(importType);
		if (initTime != null) {
			baseBean.setIsAuto(initTime.getIaAuto());
		}
		log.info(" 4. 获取外来的待导入数据");
		log.info("获取待同步数据结束：" + System.currentTimeMillis());

		DataErrorType errorType = importData(baseBean);
		if (errorType != DataErrorType.none) {
			result.setSuccess(false);
		}

		log.info(" 5. 执行数据导入 errorType = " + errorType);
		if (errorType == DataErrorType.rowsType) {// 行间校验出错，直接导出本地Excel
			log.info("错误信息保存到服务器本地开始：" + System.currentTimeMillis());

			// 把错误信息保存到服务器本地
			saveErrorExcelToLocal(baseBean);

			log.info("错误信息保存到服务器本地结束：" + System.currentTimeMillis());
			result.setResultMessage(SyncErrorInfo.IMPORT_ERROR);
			// 上传文件成功保存到服务器本地
			return result;
		} else if (errorType == DataErrorType.columnsType) {// 行内校验出错
			log.info("错误信息保存到服务器本地开始：" + System.currentTimeMillis());

			// 把错误信息保存到服务器本地
			exportExcelOriData();
			log.info("错误信息保存到服务器本地结束：" + System.currentTimeMillis());

			// 拼装返回页面
			result.setResultMessage(SyncErrorInfo.IMPORT_ERROR);
			return result;
		} else if (errorType == DataErrorType.serverCount) {// 人数超出限制
			int serverCount = JecnKey.getServerPeopleCounts();
			log.info("人员同步导入人员超出服务最大限制最大用户数为：" + serverCount);
			// 拼装返回页面 人员同步人数超出上限！
			result.setResultMessage(SyncErrorInfo.USER_IMPORT_CAPS + serverCount + "!");
			return result;
		} else if (errorType == DataErrorType.syncDataUserNumError || errorType == DataErrorType.syncDataOrgNumError) {
			// 邮件通知
			result.setResultMessage("组织、岗位增删总数超出100或人员增删总数超出500，请确认数据正确性，然后修改为手动同步，再执行同步!");
			result.setSuccess(false);
			return result;
		}
		result.setResultMessage(SyncErrorInfo.IMPORT_SUCCESS);
		return result;
	}

	/**
	 * 
	 * 上传excel到服务器本地
	 * 
	 */
	private String saveExcelToLocal(File file, String fileName) {
		return saveExcelToLocal(file, fileName, SyncTool.getExcelDataPath());
	}

	@Override
	protected String getOutFileName() {

		return null;
	}

	@Override
	protected String getOutFilepath() {
		return null;
	}

	public CheckDeptRow getDeptCheckRow() {
		return deptCheckRow;
	}

	public void setDeptCheckRow(CheckDeptRow deptCheckRow) {
		this.deptCheckRow = deptCheckRow;
	}

	public CheckPosRow getCheckPosRow() {
		return checkPosRow;
	}

	public void setCheckPosRow(CheckPosRow checkPosRow) {
		this.checkPosRow = checkPosRow;
	}

	public CheckUserRow getCheckUserRow() {
		return checkUserRow;
	}

	public void setCheckUserRow(CheckUserRow checkUserRow) {
		this.checkUserRow = checkUserRow;
	}

	public Long getCurrProjectID() {
		return currProjectID;
	}

	public void setCurrProjectID(Long currProjectID) {
		this.currProjectID = currProjectID;
	}

	public ExportDataOfPersonnelChangesBuss getPersonnelChangesBuss() {
		return personnelChangesBuss;
	}

	public void setPersonnelChangesBuss(ExportDataOfPersonnelChangesBuss personnelChangesBuss) {
		this.personnelChangesBuss = personnelChangesBuss;
	}

	public ISysImportService getImportService() {
		return importService;
	}

	public void setImportService(ISysImportService importService) {
		this.importService = importService;
	}

	public ICheckUserDataColumns getUserDataColumns() {
		return userDataColumns;
	}

	public void setUserDataColumns(ICheckUserDataColumns userDataColumns) {
		this.userDataColumns = userDataColumns;
	}

	public CheckBasePos getCheckBasePos() {
		return checkBasePos;
	}

	public void setCheckBasePos(CheckBasePos checkBasePos) {
		this.checkBasePos = checkBasePos;
	}

	public ImportDataFactory getImportDataFactory() {
		return importDataFactory;
	}

	public void setImportDataFactory(ImportDataFactory importDataFactory) {
		this.importDataFactory = importDataFactory;
	}

}
