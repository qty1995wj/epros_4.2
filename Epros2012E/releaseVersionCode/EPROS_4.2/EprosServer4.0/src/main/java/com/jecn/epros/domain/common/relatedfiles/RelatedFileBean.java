package com.jecn.epros.domain.common.relatedfiles;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.util.JecnProperties;

/**
 * Created by Angus on 2017/4/20.
 */
public class RelatedFileBean {
    private String id;
    private String name;
    private String type;
    private String typeName;
    private int isPublic;
    private LinkResource link;

    public RelatedFileBean(String id, String name, String type, LinkResource link) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.link = link;
    }

    public RelatedFileBean(String id, String name, String type, LinkResource link, int isPublic) {
        this(id, name, type, link);
        this.isPublic = isPublic;
    }


    public RelatedFileBean(LinkResource resource) {
        this(resource.getId(), resource.getName(), AuthenticatedUserUtil.isLanguageType() == 0 ? resource.getType().getName() : resource.getType().getEnName(), resource, resource.getIsPublic());
    }

    public String getTypeName() {
        switch (type) {
            case "PROCESS":
                this.typeName = JecnProperties.getValue("process");
                break;
            case "PROCESS_MAP":
                this.typeName = JecnProperties.getValue("processMap");
                break;
            case "RULE":
                this.typeName = JecnProperties.getValue("rule");
                break;
            case "FILE":
                this.typeName = JecnProperties.getValue("file");
                break;
            case "RISK":
                this.typeName = JecnProperties.getValue("risk");
                break;
            case "STANDARD":
                this.typeName = JecnProperties.getValue("standard");
                break;
            default:
                typeName = type;
                break;
        }
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LinkResource getLink() {
        return link;
    }

    public void setLink(LinkResource link) {
        this.link = link;
    }
}
