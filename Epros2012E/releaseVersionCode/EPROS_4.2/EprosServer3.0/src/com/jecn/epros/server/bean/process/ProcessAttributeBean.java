package com.jecn.epros.server.bean.process;

import java.io.Serializable;

public class ProcessAttributeBean implements Serializable {

	/** 流程类别ID */
	private Long processTypeId;
	/** 流程类别名称 */
	private String processTypeName;
	/** 流程责任部门ID */
	private Long dutyOrgId;
	/** 流程责任部门名称 */
	private String dutyOrgName;
	/** 流程责任类型 0岗位 1人员 */
	private int dutyUserType;
	/** 责任人ID */
	private Long dutyUserId;
	/** 流程责任人名称 */
	private String dutyUserName;
	/**** 流程责任人岗位名称 **********/
	private String dutuUserPosName;
	/** 有效期 */
	private String expiry;
	/** 流程监护人ID */
	private Long guardianId;
	/** 流程监护人名称 */
	private String guardianName;
	/** 流程拟制人ID */
	private Long fictionPeopleId;
	/** 流程拟制人名称 */
	private String fictionPeopleName;
	/** 流程拟制人岗位名称 */
	private String fictionPeoplePosName;
	/** 流程专员 */
	private Long commissionerId;
	private String commissionerName;

	/** 关键字 */
	private String keyWord;
	
	private String bussType = "0";
	
	

	public String getBussType() {
		return bussType;
	}

	public void setBussType(String bussType) {
		this.bussType = bussType;
	}

	public Long getFictionPeopleId() {
		return fictionPeopleId;
	}

	public void setFictionPeopleId(Long fictionPeopleId) {
		this.fictionPeopleId = fictionPeopleId;
	}

	public String getFictionPeopleName() {
		return fictionPeopleName;
	}

	public void setFictionPeopleName(String fictionPeopleName) {
		this.fictionPeopleName = fictionPeopleName;
	}

	public String getFictionPeoplePosName() {
		return fictionPeoplePosName;
	}

	public void setFictionPeoplePosName(String fictionPeoplePosName) {
		this.fictionPeoplePosName = fictionPeoplePosName;
	}

	public Long getProcessTypeId() {
		return processTypeId;
	}

	public void setProcessTypeId(Long processTypeId) {
		this.processTypeId = processTypeId;
	}

	public String getProcessTypeName() {
		return processTypeName;
	}

	public void setProcessTypeName(String processTypeName) {
		this.processTypeName = processTypeName;
	}

	public Long getDutyOrgId() {
		return dutyOrgId;
	}

	public void setDutyOrgId(Long dutyOrgId) {
		this.dutyOrgId = dutyOrgId;
	}

	public String getDutyOrgName() {
		return dutyOrgName;
	}

	public void setDutyOrgName(String dutyOrgName) {
		this.dutyOrgName = dutyOrgName;
	}

	public int getDutyUserType() {
		return dutyUserType;
	}

	public void setDutyUserType(int dutyUserType) {
		this.dutyUserType = dutyUserType;
	}

	public Long getDutyUserId() {
		return dutyUserId;
	}

	public void setDutyUserId(Long dutyUserId) {
		this.dutyUserId = dutyUserId;
	}

	public String getDutyUserName() {
		return dutyUserName;
	}

	public void setDutyUserName(String dutyUserName) {
		this.dutyUserName = dutyUserName;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public Long getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(Long guardianId) {
		this.guardianId = guardianId;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getDutuUserPosName() {
		return dutuUserPosName;
	}

	public void setDutuUserPosName(String dutuUserPosName) {
		this.dutuUserPosName = dutuUserPosName;
	}

	public Long getCommissionerId() {
		return commissionerId;
	}

	public void setCommissionerId(Long commissionerId) {
		this.commissionerId = commissionerId;
	}

	public String getCommissionerName() {
		return commissionerName;
	}

	public void setCommissionerName(String commissionerName) {
		this.commissionerName = commissionerName;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

}
