package com.jecn.epros.server.webBean.process;

import java.util.List;

public class ProcessSystemListBean {
	// 总的级别数
	private int levelTotal;
	// 流程清单
	private List<ProcessWebBean> listProcessWebBean;
	/** 流程每个级别的个数 0、1、2.....（统计） */
	private List<Integer> listInt;
	/** 流程个数 */
	private int flowTotal;
	/** 待建流程个数 */
	private int noPubFlowTotal;
	/** 审批流程个数 */
	private int approveFlowTotal;
	/** 发布流程个数 */
	private int pubFlowTotal;

	public int getFlowTotal() {
		return flowTotal;
	}

	public void setFlowTotal(int flowTotal) {
		this.flowTotal = flowTotal;
	}

	public int getNoPubFlowTotal() {
		return noPubFlowTotal;
	}

	public void setNoPubFlowTotal(int noPubFlowTotal) {
		this.noPubFlowTotal = noPubFlowTotal;
	}

	public int getApproveFlowTotal() {
		return approveFlowTotal;
	}

	public void setApproveFlowTotal(int approveFlowTotal) {
		this.approveFlowTotal = approveFlowTotal;
	}

	public int getPubFlowTotal() {
		return pubFlowTotal;
	}

	public void setPubFlowTotal(int pubFlowTotal) {
		this.pubFlowTotal = pubFlowTotal;
	}

	public int getLevelTotal() {
		return levelTotal;
	}

	public void setLevelTotal(int levelTotal) {
		this.levelTotal = levelTotal;
	}

	public List<ProcessWebBean> getListProcessWebBean() {
		return listProcessWebBean;
	}

	public void setListProcessWebBean(List<ProcessWebBean> listProcessWebBean) {
		this.listProcessWebBean = listProcessWebBean;
	}

	public List<Integer> getListInt() {
		return listInt;
	}

	public void setListInt(List<Integer> listInt) {
		this.listInt = listInt;
	}
}
