package com.jecn.epros.service.report.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.*;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.inventory.FileInventoryCommonBean;
import com.jecn.epros.domain.inventory.PosInventoryRoleBean;
import com.jecn.epros.domain.org.PosRelatedBean;
import com.jecn.epros.domain.org.SearchPosGroupStatisticsResultBean;
import com.jecn.epros.domain.org.SearchPosResultBean;
import com.jecn.epros.domain.process.FlowActiveBaseBean;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.process.relatedFile.RelatedActivity;
import com.jecn.epros.domain.process.relatedFile.RelatedFlow;
import com.jecn.epros.domain.report.*;
import com.jecn.epros.domain.report.base.CharItem;
import com.jecn.epros.domain.report.base.LineChartOptions;
import com.jecn.epros.domain.report.base.SerieData;
import com.jecn.epros.mapper.*;
import com.jecn.epros.security.AuthenticatedUser;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.report.ReportService;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.excel.ExcelUtils;
import com.jecn.epros.util.excel.InventoryStyleManager;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportMapper reportMapper;

    @Autowired
    private RuleMapper ruleMapper;

    @Autowired
    private ProcessMapper processMapper;

    @Autowired
    private FileMapper fileMapper;

    @Autowired
    private SelectFileMapper selectFileMapper;

    @Override
    public List<ProcessDetail> processDetailView(Map<String, Object> map) {
        List<ProcessDetail> processDetails = reportMapper.processDetailView(map);
        int pubCount = 0;
        int notPubCount = 0;
        int total = 0;
        int percentage = 0;
        for (ProcessDetail detail : processDetails) {
            pubCount += detail.getPubCount();
            notPubCount += detail.getNotPubCount();
            total += detail.getTotal();
        }
        percentage = total == 0 ? 0 : pubCount * 100 / total;
        ProcessDetail detail = new ProcessDetail();
        detail.setName(JecnProperties.getValue("totals"));
        detail.setPubCount(pubCount);
        detail.setNotPubCount(notPubCount);
        detail.setTotal(total);
        detail.setPercentage(percentage);
        processDetails.add(detail);
        return processDetails;
    }

    @Override
    public List<ProcessScan> processOptimizeView(Map<String, Object> map) {
        return reportMapper.processOptimizeView(map);
    }

    @Override
    public List<KpiView> processKpiView(Map<String, Object> map) {
        List<KpiView> views = reportMapper.processKpiView(map);
        return views;
    }

    @Override
    public List<InformationStatistic> processInformation(Map<String, Object> map) {
        return reportMapper.processInformation(map);
    }

    @Override
    public List<RoleStatistic> processRoleNum(Map<String, Object> map) {
        return reportMapper.processRoleNum(map);
    }

    @Override
    public List<ActivityStatistic> processActivityNum(Map<String, Object> map) {

        return reportMapper.processActivityNum(map);
    }

    @Override
    public List<FlowActiveBaseBean> processInformationDetail(Map<String, Object> map) {
        Long id = (Long) map.get("id");
        Integer type = (Integer) map.get("type");
        Long itId = (Long) map.get("itId");
        return null;
    }

    @Override
    public Inventory getRoleInventory(Map<String, Object> map) {
        int totalNum = reportMapper.processRoleDetailNum(map);
        List<RelatedFlow> processes = new ArrayList<RelatedFlow>();
        if (totalNum > 0) {
            Paging paging = (Paging) map.get("paging");
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "jfs.flow_id");
            processes = reportMapper.processRoleDetail(map);
        }
        Inventory inventory = new Inventory();
        inventory.setTotalNum(totalNum);
        return getFlowInventoryContent(processes, inventory);
    }

    /**
     * 流程清单
     *
     * @param processes
     * @param inventory
     */
    private Inventory getFlowInventoryContent(List<RelatedFlow> processes, Inventory inventory) {
        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        initInventoryTitle(inventory.getStartLevel(), inventory.getMaxLevel(), data);
        InventoryCell cell = null;
        for (RelatedFlow process : processes) {
            List<InventoryCell> content = new ArrayList<InventoryCell>();

            cell = new InventoryCell();
            cell.setText(process.getName());
            LinkResource link = new LinkResource(process.getId(), process.getName(), ResourceType.PROCESS);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell.setLinks(links);
            content.add(cell);

            // 编号
            cell = new InventoryCell();
            cell.setText(process.getFlowCode());
            content.add(cell);
            // 责任人
            cell = new InventoryCell();
            cell.setText(process.getResPeopleName());
            content.add(cell);
            // 责任部门
            cell = new InventoryCell();
            cell.setText(process.getResOrgName());
            content.add(cell);

            data.getContents().add(content);
        }
        return inventory;
    }

    /**
     * 流程清单
     *
     * @param start
     * @param data
     */
    private void initInventoryTitle(int start, int maxLevel, InventoryTable data) {
        List<InventoryCell> title = new ArrayList<InventoryCell>();
        InventoryCell cell = null;
        cell = new InventoryCell();
        cell.setText(JecnProperties.getValue("processName"));
        title.add(cell);
        cell = new InventoryCell();
        cell.setText(JecnProperties.getValue("processNum"));
        title.add(cell);
        cell = new InventoryCell();
        cell.setText(JecnProperties.getValue("personLiable"));
        title.add(cell);
        cell = new InventoryCell();
        cell.setText(JecnProperties.getValue("responsibilityDepartment"));
        title.add(cell);
        data.getContents().add(title);
    }

    @Override
    public Inventory getActivityInventory(Map<String, Object> map) {
        int totalNum = reportMapper.processActivityDetailNum(map);
        List<RelatedFlow> processes = new ArrayList<RelatedFlow>();
        if (totalNum > 0) {
            Paging paging = (Paging) map.get("paging");
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "jfs.flow_id");
            processes = reportMapper.processActivityDetail(map);
        }
        Inventory inventory = new Inventory();
        inventory.setTotalNum(totalNum);
        return getFlowInventoryContent(processes, inventory);
    }

    @Override
    public Inventory getInformationInventory(Map<String, Object> map) {
        int totalNum = reportMapper.processInformationDetailNum(map);
        List<RelatedActivity> activirys = new ArrayList<RelatedActivity>();
        if (totalNum > 0) {
            Paging paging = (Paging) map.get("paging");
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "jfsi.figure_id");
            activirys = reportMapper.processInformationDetail(map);
        }
        Inventory inventory = new Inventory();
        inventory.setTotalNum(totalNum);
        return getActivityInventoryContent(activirys, inventory);
    }

    @Override
    public Inventory primaryManage(PrimaryActivitySearchParam param, Paging paging) {
        Map<String, Object> map = new HashMap<>();
        map.put("param", param);
        map.put("paging", paging);
        int totalNum = reportMapper.primaryManageNum(map);
        Inventory inventory = new Inventory();
        inventory.setTotalNum(totalNum);
        if (totalNum == 0) {
            inventory = getPrimaryManage(new ArrayList<>());
            return inventory;
        }
        if (param != null && !param.isDownload()) {
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize());
        }
        List<PrimaryStatistic> beans = reportMapper.fetchPrimaryManage(map);
        inventory = getPrimaryManage(beans);
        return inventory;
    }

    @Override
    public Map<String, Object> primaryManageStatistic() {
        Map<String, Object> doMap = reportMapper.primaryManageStatistic();
        return doMap;
    }

    private Inventory getPrimaryManage(List<PrimaryStatistic> beans) {
        Inventory inventory = new Inventory();
        final List<List<InventoryCell>> data = inventory.getData().getContents();
        boolean hasBussType = ConfigUtils.isBussTypeShow();
        boolean hasAllowRule = ConfigUtils.isAllowRuleShow();
        EasyCell easyCell = EasyCell.newInstance().add(new InventoryCellStyle(2), JecnProperties.getValue("processName"))
                .add(new InventoryCellStyle(2), JecnProperties.getValue("flowWithPrimaryActivityNum"))
                .add(new InventoryCellStyle(1), hasBussType, JecnProperties.getValue("bussType"))
                .add(new InventoryCellStyle(4), hasAllowRule, JecnProperties.getValue("allowRuleName"))
                .add(new InventoryCellStyle(2), JecnProperties.getValue("responsibilityDepartment"));
        List<InventoryCell> titles = easyCell.buildWithTitle();
        Map<String, List<InventoryCellStyle>> styleMap = easyCell.getStyleMap();
        inventory.setStyleMap(styleMap);
        data.add(titles);
        if (beans != null && beans.size() > 0) {
            beans.stream().forEach(bean -> {
                List<InventoryCell> cells = EasyCell.newInstance().add(new InventoryCell(new LinkResource(bean.getFlowId(), bean.getFlowName(), ResourceType.PROCESS)))
                        .add(new InventoryCellStyle(), bean.getNum())
                        .add(hasBussType, bean.getBussTypeStr())
                        .add(hasAllowRule, bean.getAllowRule())
                        .add(bean.getOrgName()).build();
                data.add(cells);
            });
        }
        return inventory;
    }

    /**
     * 活动
     *
     * @param activitys
     * @param inventory
     */
    private Inventory getActivityInventoryContent(List<RelatedActivity> activitys, Inventory inventory) {
        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        initActivityInventoryTitle(data);
        InventoryCell cell = null;
        for (RelatedActivity activity : activitys) {
            List<InventoryCell> content = new ArrayList<InventoryCell>();

            cell = new InventoryCell();
            cell.setText(activity.getName());
            LinkResource link = new LinkResource(activity.getId(), activity.getName(), ResourceType.ACTIVITY);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell.setLinks(links);
            content.add(cell);

            cell = new InventoryCell();
            cell.setText(activity.getNumber());
            content.add(cell);

            data.getContents().add(content);
        }
        return inventory;
    }

    /**
     * 活动
     *
     * @param data
     */
    private void initActivityInventoryTitle(InventoryTable data) {
        List<InventoryCell> title = new ArrayList<InventoryCell>();
        InventoryCell cell = null;
        cell = new InventoryCell();
        cell.setText(JecnProperties.getValue("activityNam"));
        title.add(cell);
        cell = new InventoryCell();
        cell.setText(JecnProperties.getValue("activityNumber"));
        title.add(cell);
        data.getContents().add(title);
    }

    @Override
    public ChineseEnglisListWebBean getChineseEnglishList(Map<String, Object> map) {
        ChineseEnglisListWebBean webBean = new ChineseEnglisListWebBean();
        Paging paging = (Paging) map.get("paging");
        Long id = Long.valueOf(map.get("id").toString());
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "T.SORT_ID");
        List<ChineseEnglisListBean> rules = ruleMapper.getRuleList(map);
        List<Long> ids = new ArrayList<Long>();
        if (rules != null && !rules.isEmpty()) {
            for (ChineseEnglisListBean bean : rules) {
                ids.add(bean.getId());
            }
        }
        map.put("ids", ids);
        List<Map<String, Object>> countBean = ruleMapper.getChineseEnglishListCount(map);
        assembleBean(rules, countBean);//组装中英文 条数
        // 初始化清单
        Inventory inventory = new Inventory();
        inventory.setStartLevel(0);
        if (id != 0) {
            inventory.setMaxLevel(1);
        }
        inventory.setTotalNum(ruleMapper.getRuleListCount(map));
        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        data.addContentRow(ExcelUtils.getInventoryTitleNoNum(inventory.getStartLevel(), inventory.getMaxLevel(),
                InventoryStyleManager.getChineseEnglishListCountInventoryTitles()));
        InventoryCellStyle inStyle = new InventoryCellStyle();
        inStyle.setAlign(InventoryTable.Align.center);
        InventoryCell cell = null;
        for (ChineseEnglisListBean bean : rules) {
            List<InventoryCell> content = new ArrayList<InventoryCell>();
            data.getContents().add(content);
            if (id != 0) {
                //0级名称
                cell = new InventoryCell();
                cell.setText(bean.getpName());
                content.add(cell);
            }
            //1级名称
            cell = new InventoryCell();
            cell.setText(bean.getName());
            content.add(cell);
            //中文数量
            cell = new InventoryCell();
            cell.setText(bean.getZhCount() == null ? "0" : bean.getZhCount().toString());
            cell.setStyle(inStyle);
            content.add(cell);
            //英文数量
            cell = new InventoryCell();
            cell.setText(bean.getEnCount() == null ? "0" : bean.getEnCount().toString());
            cell.setStyle(inStyle);
            content.add(cell);
        }
        webBean.setInventory(inventory);
        webBean.setRules(rules);
        return webBean;
    }


    /**
     * 拼接 中英文统计对象
     *
     * @param rules
     * @param countBean
     * @return
     */
    private void assembleBean(List<ChineseEnglisListBean> rules, List<Map<String, Object>> countBean) {
        if (countBean != null && !countBean.isEmpty() && rules != null && !rules.isEmpty()) {
            for (ChineseEnglisListBean bean : rules) {
                for (Map mapBean : countBean) {
                    long mapId = mapBean.get("ID").toString() == null ? 0 : Long.valueOf(mapBean.get("ID").toString()).longValue();
                    String type = mapBean.get("TYPE") == null ? "" : mapBean.get("TYPE").toString();
                    Integer count = mapBean.get("COUNT") == null ? 0 : Integer.valueOf(mapBean.get("COUNT").toString());
                    if (bean.getId().longValue() == mapId) {
                        if ("EN".equals(type)) {
                            bean.setEnCount(count);
                        } else {
                            bean.setZhCount(count);
                        }
                    }
                }
            }
        }
    }


    @Override
    public Inventory reportPermissionsReports(InventoryParam param) {
        Map doMap = new HashedMap();
        doMap.put("ids", param.getIds());
        doMap.put("orgId", param.getOrgId() == null ? "-1" : param.getOrgId());
        doMap.put("type", param.getType());
        List<PermissionsReport> permissionsReports = new ArrayList<PermissionsReport>();
        if ((param.getIds() != null && !param.getIds().isEmpty()) || (param.getOrgId() != null && param.getOrgId() > 0)) {
            permissionsReports = reportMapper.reportPermissionsReports(doMap);
        }
        // 初始化清单
        Inventory inventory = new Inventory();
        inventory.setStartLevel(1);
        inventory.setTotalNum(permissionsReports.size());
        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        data.addContentRow(ExcelUtils.getInventoryTitleNoNum(inventory.getStartLevel(), inventory.getMaxLevel(),
                InventoryStyleManager.reportPermissionsReportsTitles()));
        inventory.setStyleMap(InventoryStyleManager.getPosInventoryStyles(data.getTitileContents()));
        InventoryCell cell = null;
        for (PermissionsReport bean : permissionsReports) {
            List<InventoryCell> content = new ArrayList<InventoryCell>();
            data.getContents().add(content);
            //责任部门
            cell = new InventoryCell();
            cell.setText(bean.getOrgName());
            content.add(cell);

            //文件名称
            cell = new InventoryCell();
            cell.setText(bean.getName());
            content.add(cell);
            //构建权限报表数据
            builPermissionData(bean, data, content);
        }
        return inventory;
    }

    //构建权限报表数据
    private void builPermissionData(PermissionsReport bean, InventoryTable data, List<InventoryCell> content) {
        int max = bean.getOrgData().size();
        if (max < bean.getPosData().size()) {
            max = bean.getPosData().size();
        }
        if (max < bean.getPosGroupData().size()) {
            max = bean.getPosGroupData().size();
        }
        if (max > 0) {
            boolean posIsFirst = true;
            for (int i = 0; i < max; i++) {
                PermissionsData org = null;
                if (bean.getOrgData().size() > i) {
                    org = bean.getOrgData().get(i);
                }
                PermissionsData pos = null;
                if (bean.getPosData().size() > i) {
                    pos = bean.getPosData().get(i);
                }
                PermissionsData posGroup = null;
                if (bean.getPosGroupData().size() > i) {
                    posGroup = bean.getPosGroupData().get(i);
                }
                if (!posIsFirst) {// 角色换行显示
                    content = new ArrayList<InventoryCell>();
                    data.getContents().add(content);
                    fillEmptyCell(2, content, data);
                }
                if (org != null) {
                    //部门名称
                    InventoryCell cell = new InventoryCell();
                    cell.setText(org.getName());
                    content.add(cell);
                    //部门权限类型
                    cell = new InventoryCell();
                    cell.setText("1".equals(org.getAccessType()) ? JecnProperties.getValue("consult") + "/" + JecnProperties.getValue("downLoad") : JecnProperties.getValue("consult"));
                    content.add(cell);
                } else {
                    fillEmptyCell(2, content, data);
                }
                if (pos != null) {
                    //岗位名称
                    InventoryCell cell = new InventoryCell();
                    cell.setText(pos.getName());
                    content.add(cell);
                    //岗位权限类型
                    cell = new InventoryCell();
                    cell.setText("1".equals(pos.getAccessType()) ? JecnProperties.getValue("consult") + "/" + JecnProperties.getValue("downLoad") : JecnProperties.getValue("consult"));
                    content.add(cell);
                } else {
                    fillEmptyCell(2, content, data);
                }

                if (posGroup != null) {
                    //岗位组名称
                    InventoryCell cell = new InventoryCell();
                    cell.setText(posGroup.getName());
                    content.add(cell);
                    //岗位组权限类型
                    cell = new InventoryCell();
                    cell.setText("1".equals(posGroup.getAccessType()) ? JecnProperties.getValue("consult") + "/" + JecnProperties.getValue("downLoad") : JecnProperties.getValue("consult"));
                    content.add(cell);
                } else {
                    fillEmptyCell(2, content, data);
                }
                posIsFirst = false;
            }
        } else {
            fillEmptyCell(6, content, data);
        }

    }

    private void fillEmptyCell(int orgFillNum, List<InventoryCell> content, InventoryTable data) {
        InventoryCell cell = null;
        for (int i = 0; i < orgFillNum; i++) {
            cell = new InventoryCell();
            cell.setText("");
            content.add(cell);
        }

    }
}
