package epros.draw.gui.top.toolbar;

import java.awt.GridBagConstraints;

import javax.swing.SwingConstants;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏大类：流程操作说明类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnOperInstrToolBarItemPanel extends JecnAstractToolBarItemPanel {
	/** 操作说明配置 */
	private JecnToolbarContentButton operSetBtn = null;
	/** 下载操作说明 */
	private JecnToolbarContentButton operDownloadBtn = null;
	/** 编辑操作说明 */
	private JecnToolbarContentButton operEditBtn = null;

	public JecnOperInstrToolBarItemPanel(JecnToolBarPanel toolBarPanel) {
		super(toolBarPanel);
		initchildComponents();
	}

	@Override
	protected void initTitleType() {
		this.titleType = ToolBarElemType.operTitle;
	}

	@Override
	protected void createChildComponents() {
		// 操作说明配置
		operSetBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.operSetItem), ToolBarElemType.operSetItem);
		// 编辑操作说明
		operEditBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.operEdit), ToolBarElemType.operEdit);
		// 下载操作说明
		operDownloadBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.operDownload), ToolBarElemType.operDownload);

		// 操作说明配置
		// operSetBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		// operSetBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		// 下载操作说明
		operDownloadBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		operDownloadBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		// 编辑操作说明
		// operEditBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		// operEditBtn.setVerticalTextPosition(SwingConstants.BOTTOM);

		// 边框
		this.setBorder(JecnUIUtil.getTootBarBorder());
	}

	@Override
	protected void layoutChildComponents() {
		// 操作说明配置
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0(), 0, 0);
		this.add(operSetBtn.getJToolBar(), c);
		// 编辑操作说明
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(operEditBtn.getJToolBar(), c);
		// 下载操作说明
		c = new GridBagConstraints(1, 0, 1, 2, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(operDownloadBtn.getJToolBar(), c);
	}
}
