<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript">
var basePath = "${basePath}";
</script>
	</head>

	<body>
		<div style="width: 100%;" align="right">
			<table style="margin-right: 25px; margin-top: 3px;">
				<tr align="left">
					<td align="right" style="padding-right: 5px;">
						<s:text name="total"></s:text>
						<span id="allRiskTotal"></span>
					</td>
					<td>
							<img src="${basePath}images/allShow.gif" style="cursor: pointer;" target='_blank' class="table" onclick="showAllRiskList()" />
							<!-- 是否有下载权限 -->
							<s:if test="#session.webLoginBean.isAdmin||#session.webLoginBean.isViewAdmin">
							<img src="${basePath}images/download.gif" style="cursor: pointer;" target='_blank' class="table" onclick="downloadRiskList()" />
						   </s:if>
					</td>
				</tr>
			</table>
		</div>
		<div id="riskInventory_Div" />
			<input type="hidden" id="riskId">
	</body>
</html>
