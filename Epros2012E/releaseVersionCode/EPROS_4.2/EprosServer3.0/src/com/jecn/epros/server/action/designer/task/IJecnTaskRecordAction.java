package com.jecn.epros.server.action.designer.task;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.bean.task.PrfRelatedTemplet;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.task.TaskTemplet;
import com.jecn.epros.server.bean.task.temp.JecnTaskResubmitDesignerBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/**
 * 任务审批数据校验、处理类 action层
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Oct 24, 2012 时间：10:41:31 AM
 */
public interface IJecnTaskRecordAction {
	/**
	 * 获得人员名称和人员主键记录的集合信息
	 * 
	 * @param set
	 *            人员主键集合
	 * @param projectId
	 *            当前项目ID
	 * @return 人员名称和人员主键记录的集合信息
	 * @throws Exception
	 */
	public List<Object[]> getJecnUserByUserIds(Set<Long> set, Long projectId) throws Exception;

	/**
	 * 判断审批人是否为相同人员
	 * 
	 * @param userIds
	 * @return true 存在相同人员
	 * @throws Exception
	 */
	public boolean checkAuditPeopleIsSame(Set<Long> userIds) throws Exception;

	/**
	 * 验证版本号是否存在
	 * 
	 * @param id
	 *            流程或制度ID
	 * @param version
	 *            版本号
	 * @param type
	 *            类型 0 流程，1文件，2制度
	 * @return true 版本好存在
	 * @throws DaoException
	 */
	public boolean isExistVersionbyFlowId(Long id, String version, int type) throws Exception;

	/**
	 * 是否编辑
	 * 
	 * @param rid
	 * @param nodeType
	 * @return
	 * @throws Exception
	 */
	public boolean isEdit(Long rid, TreeNodeType nodeType) throws Exception;

	/**
	 * 是否审批
	 * 
	 * @param rid节点ID
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	public boolean isInTask(Long rid, TreeNodeType nodeType) throws Exception;

	/**
	 * 是否审批
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	public boolean idsIsInTask(List<Long> rids, TreeNodeType nodeType) throws Exception;

	/**
	 * 验证人员是否存在岗位
	 * 
	 * @param peopleid
	 *            人员主键ID
	 * @return true ：存在岗位
	 * @throws Exception
	 */
	public boolean isExsitPos(Long peopleid) throws Exception;

	/**
	 * 提交审批验证
	 * 
	 * @param rid
	 *            关联ID
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	boolean submitToTask(Long rid, TreeNodeType nodeType) throws Exception;

	/**
	 * 是否已发布
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点是已发布状态
	 * @throws Exception
	 */
	public boolean idsIsPub(List<Long> rids, TreeNodeType nodeType) throws Exception;

	/**
	 * 制度文件引用文件管理中的文件是否审批
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点处于任务中
	 * @throws Exception
	 */
	public boolean idsIsInRuleTask(List<Long> rids, TreeNodeType nodeType) throws Exception;

	/**
	 * 文件树节点在制度文件中是否已发布（文件、制度文件）
	 * 
	 * @param rids节点ID集合
	 * @param nodeType
	 *            节点类型
	 * @return true 节点是已发布状态
	 * @throws Exception
	 */
	public boolean idsIsRulePubByFile(List<Long> rids, TreeNodeType nodeType) throws Exception;

	public List<TaskTemplet> listTaskTempletsByType(int taskType) throws Exception;

	public List<TaskConfigItem> listTaskConfigItemByTempletId(String id) throws Exception;

	public TaskTemplet saveTaskTemplet(TaskTemplet taskTemplet) throws Exception;

	public void updateTaskTemplet(TaskTemplet taskTemplet) throws Exception;

	public boolean validateUpdateName(String name, String id, int taskType) throws Exception;

	public void deleteTaskTemplet(List<String> listIds) throws Exception;

	public void updateTaskConfigItems(List<TaskConfigItem> configs) throws Exception;

	public void deletePrfRelated(Long prfId, int taskType) throws Exception;

	public void saveOrUpdatePrfRelated(Long prfId, int taskType, String approveId, String borrowId, String desuetudeId,
			Long updatePeopleId) throws Exception;

	public List<PrfRelatedTemplet> listPrfRelatedTemplet() throws Exception;

	public boolean validateAddName(String name, int taskType) throws Exception;

	boolean reSubmitToTask(Long rid, TreeNodeType nodeType) throws Exception;

	JecnTaskResubmitDesignerBean getReSubmitTaskBeanNew(Long rid, TreeNodeType nodeType) throws Exception;

	List<JecnTaskStage> getJecnTaskStageList(Long taskId);

	public String getTempletIdByTempletType(Long id, int taskType, Integer templetType) throws Exception;

	public int getChildCount(Long id, TreeNodeType treeNodeType) throws Exception;
}
