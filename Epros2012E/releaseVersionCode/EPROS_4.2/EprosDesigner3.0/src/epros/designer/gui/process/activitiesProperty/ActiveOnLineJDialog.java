package epros.designer.gui.process.activitiesProperty;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.system.ButtonPanelCommon;
import epros.designer.gui.system.SupportToolCommon;
import epros.designer.gui.system.area.TextAreaFour;
import epros.designer.gui.system.date.DateCommon;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.designer.JecnActiveOnLineTBean;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;

public class ActiveOnLineJDialog extends JecnDialog {
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();
	/** 支持工具 */
	SupportToolCommon supportToolCommon;
	/** 事务代码 */
	TextAreaFour textAreaFour;
	/** 上线时间 */
	DateCommon dateCommon;
	/** 按钮 */
	ButtonPanelCommon buttonPanelCommon;

	private List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean;

	private int selectRow;

	private ActiveOnLineInfoPanel activeOnLineInfoPanel;

	public ActiveOnLineJDialog(String titleName, int selectRow, ActiveOnLineInfoPanel activeOnLineInfoPanel) {
		this.setSize(getWidthMin(), 300);
		this.setTitle(titleName);
		this.selectRow = selectRow;
		this.activeOnLineInfoPanel = activeOnLineInfoPanel;
		this.listJecnActiveOnLineTBean = activeOnLineInfoPanel.getResultList();
		initLayout();
		actionInit();
		this.setResizable(true);
		this.setModal(true);
		this.setLocationRelativeTo(null);
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(3, 1, 3, 1);
		Insets insetss = new Insets(0, 1, 0, 1);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		JecnTreeBean jecnTreeBean = null;
		String informationDescription = "";
		Date onLineDate = null;
		if (selectRow != -1) {
			JecnActiveOnLineTBean jecnActiveOnLineTBean = listJecnActiveOnLineTBean.get(selectRow);
			if (jecnActiveOnLineTBean != null) {
				if (jecnActiveOnLineTBean.getToolId() != null) {
					jecnTreeBean = new JecnTreeBean(jecnActiveOnLineTBean.getToolId(), jecnActiveOnLineTBean
							.getSysName(), TreeNodeType.tool);
				}
				if (jecnActiveOnLineTBean.getInformationDescription() != null) {
					informationDescription = jecnActiveOnLineTBean.getInformationDescription();
				}
				onLineDate = jecnActiveOnLineTBean.getOnLineTime();
			}
		}
		int rows = 0;
		supportToolCommon = new SupportToolCommon(rows, infoPanel, insets, jecnTreeBean, this,false);
		rows++;
		dateCommon = new DateCommon(onLineDate, infoPanel, rows, insets, JecnProperties.getValue("onTimeLineC"));
		rows++;
		textAreaFour = new TextAreaFour(rows, infoPanel, insets, JecnConfigTool.getItDesc()+"：",
				informationDescription, JecnProperties.getValue("transactionCode"));
		buttonPanelCommon = new ButtonPanelCommon(mainPanel, insetss);
		this.getContentPane().add(mainPanel);
	}

	private void actionInit() {
		// 取消
		buttonPanelCommon.getCancelBut().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		// 确定

		buttonPanelCommon.getOkBut().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});

	}

	/**
	 * 支持工具已经存在
	 * 
	 * @return
	 */
	private boolean isExist() {
		// 添加
		if (selectRow == -1) {
			for (JecnActiveOnLineTBean jecnActiveOnLineTBean : listJecnActiveOnLineTBean) {
				if (supportToolCommon.getIdResult().equals(jecnActiveOnLineTBean.getToolId())) {
					return true;
				}
			}
		} else {
			for (int i = 0; i < listJecnActiveOnLineTBean.size(); i++) {
				if (selectRow != i) {
					if (supportToolCommon.getIdResult().equals(listJecnActiveOnLineTBean.get(i).getToolId())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	protected void okButPerformed() {
		if (supportToolCommon.getIdResult() == null) {
			// 名称不能为空
			buttonPanelCommon.getVerfyLab().setText(
					JecnProperties.getValue("flowTool") + JecnProperties.getValue("isNotEmpty"));
			return;
		}
		if (isExist()) {
			buttonPanelCommon.getVerfyLab().setText(
					supportToolCommon.getNameResult() + JecnProperties.getValue("isExist"));
			return;
		}
		if (dateCommon.getResult() == null) {
			buttonPanelCommon.getVerfyLab().setText(JecnProperties.getValue("onlineTimeCannotBeEmpty"));
			return;
		}
		if (textAreaFour.isValidateNotPass(buttonPanelCommon.getVerfyLab())) {
			return;
		}
		JecnActiveOnLineTBean activeOnLineTBean = new JecnActiveOnLineTBean();
		activeOnLineTBean.setToolId(supportToolCommon.getIdResult());
		activeOnLineTBean.setSysName(supportToolCommon.getNameResult());
		activeOnLineTBean.setOnLineTime(dateCommon.getResult());
		activeOnLineTBean.setInformationDescription(textAreaFour.getResultStr());
		if(this.selectRow==-1){
			this.activeOnLineInfoPanel.addRow(activeOnLineTBean);
			// 获取流程ID
		}else{
			this.activeOnLineInfoPanel.updateRow(selectRow, activeOnLineTBean);
		}
		this.setVisible(false);
	}

	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	private boolean isUpdate() {
		if (supportToolCommon.isUpdate()) {
			return true;
		}
		if (dateCommon.isUpdate()) {
			return true;
		}
		if (textAreaFour.isUpdate()) {
			return true;
		}
		return false;
	}
}
