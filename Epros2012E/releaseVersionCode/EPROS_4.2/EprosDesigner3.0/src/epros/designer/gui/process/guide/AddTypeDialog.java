package epros.designer.gui.process.guide;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 类别名称    添加  
 * @author 2012-07-05
 *
 */
public class AddTypeDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(AddTypeDialog.class);
	protected boolean isOperation = false;
	private Long typeId;
	
	public boolean isOperation() {
		return isOperation;
	}

	public void setOperation(boolean isOperation) {
		this.isOperation = isOperation;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public AddTypeDialog(){
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addBtn");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		ProceeRuleTypeBean proceeRuleTypeBean = new ProceeRuleTypeBean();
		proceeRuleTypeBean.setTypeName(this.getName());
		proceeRuleTypeBean.setEnName(this.geteName());
		try {
			typeId = ConnectionPool.getProcessRuleTypeAction().addFlowType(proceeRuleTypeBean);
			isOperation = true;
			this.dispose();
		} catch (Exception e) {
			log.error("AddTypeDialog saveData is error",e);
		}
		
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		List<ProceeRuleTypeBean> listProcessRuleTypeBean= new ArrayList<ProceeRuleTypeBean>();
		try {
			listProcessRuleTypeBean = ConnectionPool.getProcessRuleTypeAction().getFlowAttributeType();
			for(ProceeRuleTypeBean proruleTypeBean : listProcessRuleTypeBean){
				if(proruleTypeBean.getTypeName().equals(name)){
					return true;
				}
			}
		} catch (Exception e) {
			log.error("AddTypeDialog validateNodeRepeat is error",e);
		}
		return false;
	}

}
