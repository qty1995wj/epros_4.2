package com.jecn.epros.server.dao.control.impl;

import java.util.List;

import org.hibernate.Hibernate;

import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.service.file.history.JecnCommonFileHistory;
import com.jecn.epros.server.service.process.history.JecnCommonProcessAndMapPublicHistory;
import com.jecn.epros.server.service.process.history.JecnCommonProcessHistory;
import com.jecn.epros.server.service.process.history.JecnCommonProcessMapHistory;
import com.jecn.epros.server.service.rule.history.JecnCommonRuleHistory;

public class JecnDocControlDaoImpl extends AbsBaseDao<JecnTaskHistoryNew, Long> implements IJecnDocControlDao {

	/**
	 * 验证输入的编号在当前版本记录下是否唯一
	 * 
	 * @param docId
	 *            主键ID
	 * @return String 返回文件存储在本地的路径
	 * @throws Exception
	 */
	public int getVersionIdCountById(String versionId) throws Exception {
		String sql = "SELECT COUNT(VERSION_ID) FROM JECN_TASK_HISTORY_NEW WHERE VERSION_ID = ? ";
		return this.countAllByParamsNativeSql(sql, versionId);
	}

	/**
	 * 
	 * 根据流程ID获取文控信息版本号集合
	 * 
	 * @param flowId
	 *            流程ID
	 * @param type
	 *            0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
	 * @return List<String>文控信息版本号集合
	 */
	public List<String> findVersionListByFlowId(Long flowId, int type) {
		String hql = "select versionId from JecnTaskHistoryNew where relateId = ? and type = ? order by id desc";
		return this.listHql(hql, 0, 5, flowId, type);
	}

	/**
	 * 文件文控类型和关联ID获取文控信息版本记录
	 * 
	 * @param refId
	 *            关联ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @return List<JecnTaskHistoryNew>
	 */
	public List<JecnTaskHistoryNew> getJecnTaskHistoryNewList(Long refId, int type) {
		String condition = "";
		switch (type) {
		case 0:
		case 4:
			condition = " in (0,4) ";
			break;
		case 1:
			condition = " = 1 ";
			break;
		case 2:
		case 3:
			condition = " in (2,3) ";
			break;
		default:
			break;
		}
		// 通过制度ID找到制度标题
		String hql = "from JecnTaskHistoryNew where relateId=? and type " + condition + " order by publishDate desc";
		return this.listHql(hql, refId);
	}

	/**
	 * 文件文控类型和关联ID获取文控信息版本记录
	 * 
	 * @param refId
	 *            关联ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @return List<JecnTaskHistoryNew>
	 */
	public List<JecnTaskHistoryNew> getTaskHistoryNewListById(Long refId) {
		// 通过ID找到制度标题
		String hql = "from JecnTaskHistoryNew where relateId=? and (type=0 or type=4) order by publishDate desc";
		return this.listHql(hql, refId);
	}

	/**
	 * 
	 * 获取流程中存在的制度ID
	 * 
	 * @param flowId
	 *            Long 流程主键ID
	 * @return List<Object[]> Object[]在流程中存在的制度主键集合 0:制度主键ID，1制度类别,2编号
	 */
	public List<Object[]> getRulesInProcess(Long flowId) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 * 获取流程地图中存在的制度ID
	 * 
	 * @param flowId
	 *            Long 流程地图主键ID
	 * @return List<Long>在流程地图中存在的制度主键集合
	 */
	public List<Long> getRulesInProcessMap(Long flowId) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 获取任务审批阶段信息
	 * 
	 * @param flowId
	 *            流程id
	 * @param projectId
	 *            项目ID
	 * @return
	 * 
	 *         由于sqlserver时间 是2015-03-20 10:45:56.56 去掉后面的.56 然后oracle
	 *         取出来是2015-03-20 没有时分秒
	 * 
	 */
	public List<Object[]> getTaskCheckStageInfo(long flowId, long projectId) {
		String sql = "select distinct ts.stage_mark,"
				+ "                ts.stage_name,"
				+ "                u.true_name,"
				+ "                org.org_name,"
				+ "                tr.Update_Time,"
				+ "                ts.sort,"
				+ "                u.people_id,"
				+ "		           pos.figure_text"
				+ "  from JECN_TASK_STAGE ts"
				+ "  left join JECN_TASK_FOR_RECODE_NEW tr on TR.TASK_ID = ts.task_id"
				+ "                                       and tr.UP_STATE = ts.stage_mark"
				+ "                                       and tr.Task_Else_State <> 11"
				+ "  left join JECN_USER u on u.people_id = tr.FROM_PERSON_ID"
				+ "  left join JECN_USER_POSITION_RELATED up on up.people_id = u.people_id"
				// +
				// "                                         and u.project_id = ?"
				+ "  left join JECN_FLOW_ORG_IMAGE pos on pos.figure_id = up.figure_id"
				+ "  left join JECN_FLOW_ORG org on org.org_id = pos.org_id"
				+ "                             and org.PROJECTID = ?"
				+ "  left join JECN_TASK_HISTORY_NEW thn on thn.task_id = ts.task_id"
				+ " where thn.RELATE_ID = ?"
				+ "   and thn.TYPE = 0"
				+ "	  and thn.task_id is not null"
				+ "	  and thn.publish_date =(select max( thn2.PUBLISH_DATE)from JECN_TASK_HISTORY_NEW thn2 where thn2.RELATE_ID =?  and thn2.TYPE = 0 group by thn2.RELATE_ID)"
				+ "   and thn.task_id is not null and ts.stage_mark <>10 and ts.is_selected_user=1 order by ts.sort";
		return this.listNativeSql(sql, projectId, flowId, flowId);
	}

	/**
	 * 保存文控信息
	 * 
	 * @param historyNew
	 * @throws Exception
	 */
	public void saveJecnTaskHistoryNew(JecnTaskHistoryNew historyNew) throws Exception {
		String sql = "select count(t.id) from jecn_task_history_new t where t.relate_id=? and t.type=? and t.version_id=?";
		if (historyNew.getTaskId() != null) {
			sql = sql + " and t.task_id=?";
		}
		int count = 0;
		if (historyNew.getTaskId() != null) {
			count = this.countAllByParamsNativeSql(sql, historyNew.getRelateId(), historyNew.getType(), historyNew
					.getVersionId(), historyNew.getTaskId());
		} else {
			count = this.countAllByParamsNativeSql(sql, historyNew.getRelateId(), historyNew.getType(), historyNew
					.getVersionId());
		}
		if (count > 0) {
			throw new IllegalArgumentException("文控信息中已存在版本号为:" + historyNew.getVersionId());
		}
		// 保存文控信息
		this.save(historyNew);
		// 保存 文控信息表从表
		for (JecnTaskHistoryFollow historyFollow : historyNew.getListJecnTaskHistoryFollow()) {
			historyFollow.setHistoryId(historyNew.getId());
			this.getSession().save(historyFollow);
		}
		recordFileInfoHistory(historyNew.getRelateId(), historyNew.getType(), historyNew.getId());
	}

	/**
	 * 记录文件历史信息
	 * 
	 * @param relateId
	 * @param type
	 *            0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
	 * @param histroyId
	 */
	private void recordFileInfoHistory(Long relateId, int type, Long histroyId) {
		switch (type) {
		case 4:// 流程地图 架构
			JecnCommonProcessAndMapPublicHistory.recordProcessMapPublicHistory(relateId, histroyId, this);// 流程（架构）公用表记录
			JecnCommonProcessMapHistory.recordProcessMapHistory(relateId, histroyId, this);// 流程地图（架构）文控信息记录
			break;
		case 0:// 流程
			JecnCommonProcessHistory.recordProcessHistory(relateId, histroyId, this);// 流程文控信息记录
			JecnCommonProcessAndMapPublicHistory.recordProcessMapPublicHistory(relateId, histroyId, this);// 流程（架构）公用表记录
			break;
		case 1:// 文件
			JecnCommonFileHistory.recordFileModeHistory(relateId, histroyId, this);// 文件文控信息记录
			break;
		case 3:
			// 制度主表 文控
			JecnCommonRuleHistory.recordRuleStandardizedFileHistory(relateId, histroyId, this);// 制度标准化文件
			JecnCommonRuleHistory.recordRuleFileHistory(relateId, histroyId, this);
			break;
		case 2:// 制度模板
			JecnCommonRuleHistory.recordRuleModeHistory(relateId, histroyId, this);// 制度模板
			JecnCommonRuleHistory.recordRuleFileHistory(relateId, histroyId, this);
			break;
		default:
			break;
		}

	}

	/**
	 * 删除文控信息
	 * 
	 * @param historyNew
	 * @throws Exception
	 */
	public void deleteJecnTaskHistoryNew(List<Long> listId) throws Exception {
		// 删除文控信息
		String sql = "delete JecnTaskHistoryNew where id in " + JecnCommonSql.getIds(listId);
		this.execteHql(sql);
		// this.deleteObject(historyNew);
		// 删除文控信息从表数据
		sql = "delete JecnTaskHistoryFollow where historyId in " + JecnCommonSql.getIds(listId);
		this.execteHql(sql);

	}

	@Override
	public JecnTaskHistoryNew getJecnTaskHistoryNew(Long id) throws Exception {
		JecnTaskHistoryNew jecnTaskHistoryNew = this.get(id);
		if (jecnTaskHistoryNew == null) {
			jecnTaskHistoryNew = new JecnTaskHistoryNew();
		}
		jecnTaskHistoryNew.setListJecnTaskHistoryFollow(getJecnTaskHistoryFollowList(id));
		return jecnTaskHistoryNew;
	}

	/**
	 * 根据文控信息主键ID获取从表信息
	 * 
	 * @param historyId
	 *            文控主键ID
	 * @return 文控信息对应的各阶段审批人信息
	 * @throws Exception
	 */
	@Override
	public List<JecnTaskHistoryFollow> getJecnTaskHistoryFollowList(Long historyId) throws Exception {
		String hql = " FROM  JecnTaskHistoryFollow WHERE historyId =? ORDER BY sort ";
		return this.listHql(hql, historyId);
	}

	@Override
	public JecnTaskHistoryNew getLatestHistory(Long flowId) throws Exception {
		return getLatestHistory(flowId, 0);
	}

	@Override
	public JecnTaskHistoryNew getLatestHistory(Long rid, int type) throws Exception {
		String conditon = "";
		switch (type) {
		case 0:
		case 4:
			conditon = " in (0,4)";
			break;
		case 1:
			conditon = " =1";
			break;
		case 2:
		case 3:
			conditon = " in (2,3)";
			break;
		default:
			break;
		}
		String sql = "select max(id) as id from jecn_task_history_new where relate_id=? and type" + conditon;
		List<Long> ids = this.listObjectNativeSql(sql, "id", Hibernate.LONG, rid);
		for (Long id : ids) {
			if (id == null) {
				return null;
			}
			JecnTaskHistoryNew taskHistoryNew = this.getJecnTaskHistoryNew(id);
			if (taskHistoryNew != null) {
				// 查询审批记录
				String hql = "from JecnTaskHistoryFollow where historyId=? order by sort asc";
				List<JecnTaskHistoryFollow> records = this.listHql(hql, taskHistoryNew.getId());
				taskHistoryNew.setListJecnTaskHistoryFollow(records);
			}
			return taskHistoryNew;
		}
		return null;
	}

	/**
	 * 根据流程ID 获取所有的版本号
	 */
	@Override
	public List<String> findVersionListByFlowIdNoPage(Long id, int type) {
		String hql = "select versionId from JecnTaskHistoryNew where relateId = ? and type = ? order by id desc";
		return this.listHql(hql,id, type);
	}
	
}
