package com.jecn.epros.server.connector.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import sun.misc.BASE64Encoder;

import com.alibaba.fastjson.JSONObject;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.task.IJecnAbstractTaskDao;
import com.jecn.epros.server.dao.task.design.IJecnTaskRecordDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.sun.org.apache.xml.internal.security.utils.Base64;

/**
 * 烽火待办
 * 
 * @author Angus
 * @date 2018-09-27
 * 
 */
public class FiberHomeTaskInfo extends JecnBaseTaskSend {
	
	private IPersonDao personDao;


//	private static String url = "http://10.7.101.34:8161/api/message/todo.queue";
//	private static String username = "admin";
//	private static String password = "admin2015";
//	private static String mqserver = "tcp://10.7.101.34:61616";
//	private static String clientId = "restful";

	private static String url = "http://10.7.101.31:8161/api/message/todo.queue";
	private static String username = "admin";
	private static String password = "admin2015";
	private static String mqserver = "tcp://10.7.101.31:61616";
	private static String clientId = "restful";
	
	// private static String url=
	// "http://10.7.101.34:8161/api/message/todo.queue";
	// private static String username = "admin";
	// private static String password = "admin2015";
	// private static String mqserver = "tcp://10.7.101.34:61616";
	// private static String clientId = "restful";

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
			// 创建任务
			createTasks(myTaskBean);
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 审批完成，删除待办
			deleteToDoTask(myTaskBean, curPeopleId, this.personDao);
		} else {// 一条from的数据,一条to 的数据
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());
			// 上一个阶段，处理任务，代办待办删除
			deleteToDoTask(myTaskBean, curPeopleId, this.personDao);
			if ((beanNew.getTaskElseState() == 2 || beanNew.getTaskElseState() == 3)
					|| beanNew.getState() != beanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
				// 审批任务，发送下一环节任务待办
				createTasks(myTaskBean);
			}
		}
	}

	/**
	 * 删除待办
	 * 
	 * @param myTaskBean
	 * @param handlerPeopleId
	 * @throws Exception
	 */
	private void deleteToDoTask(MyTaskBean myTaskBean, Long handlerPeopleId, IPersonDao personDap) throws Exception {
		JecnUser user = personDao.get(handlerPeopleId);
		String recordID = ""; 

		recordID = String.valueOf(personDao.getJecnTaskForTodoRecodeIdByTaskIdPeopleId(myTaskBean.getTaskId(),user.getPeopleId()));			

		
		String curName = user.getEmail().substring(0,user.getEmail().indexOf("@"));
		JSONObject o = new JSONObject();
		o.put("msgtype", "done");
		o.put("taskid", myTaskBean.getTaskId() + "_" + user.getPeopleId()+"_"+recordID);
		o.put("businesskey", myTaskBean.getTaskId());
		o.put("processname", myTaskBean.getTaskName());
		o.put("processinsid", myTaskBean.getTaskId());
		o.put("auditor", curName);
		o.put("fromsys", "S075");
		postQueue(o);
//		String out = this.sendPost(url, o.toJSONString());
//		log.info(out);
	}

	private void createTasks(MyTaskBean myTaskBean) throws Exception {
		JecnUser creater = personDao.get(myTaskBean.getCreatePeopleId());
		String createrName = creater.getEmail().substring(0,creater.getEmail().indexOf("@"));
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String time = format1.format(System.currentTimeMillis());
		for (Long handlerPeopleId : myTaskBean.getHandlerPeopleIds()) {
			Map<String, String> map = new HashMap<String, String>();
			JecnUser user = personDao.get(handlerPeopleId);
			//2019年1月16日17:00  烽火ID冲突  所以新加一个id  使用  topersonid  这条记录
			String curName = user.getEmail().substring(0,user.getEmail().indexOf("@"));
			String recordID = ""; 
			recordID = String.valueOf(personDao.getJecnTaskForTodoRecodeIdByTaskIdPeopleId(myTaskBean.getTaskId(),user.getPeopleId()));			
			JSONObject o = new JSONObject();
			o.put("msgtype", "todo");
			o.put("initor", createrName);
			o.put("initname", creater.getTrueName());
			o.put("inittime", myTaskBean.getCreateTime());
			o.put("preactor", "");
			o.put("businesskey", myTaskBean.getTaskId());
			o.put("processname", "Epros");
			o.put("processinsid", myTaskBean.getTaskId());
			o.put("curtiname", "");
			o.put("tasknamecn", myTaskBean.getTaskName());
			o.put("taskid", myTaskBean.getTaskId() + "_" + user.getPeopleId()+"_"+recordID);
			o.put("todourl", getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, false));
			o.put("remark", "");
			o.put("moduleid", "");
			o.put("fromsys", "S075");
			o.put("createtime", time);
			o.put("assignee", curName);
			postQueue(o);
//			String out = this.sendPost(url, o.toJSONString());
//			log.info(out);
		}
	}

	protected String getMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("getCetc10ToDOTasks获取系统配置URL为空!");
		}
		return basicEprosURL + "loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
				+ loginId + "&isApp=" + isApp + "&isView=" + isView;
	}

	protected String getMibileMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("getCetc10ToDOTasks获取系统配置URL为空!");
		}
		return basicEprosURL + "/loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
				+ loginId + "&isApp=" + isApp + "&isView=" + isView;
	}

	private String getInstanceId(MyTaskBean myTaskBean, Long curPeopleId) {
		return myTaskBean.getTaskId() + "_" + myTaskBean.getRid() + "_" + curPeopleId;
	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		JecnUser createUser = personDao.get(beanNew.getCreatePersonId());
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setCreateTime(JecnCommon.getStringbyDateHMS(beanNew.getCreateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(getTaskName(beanNew.getTaskName()));
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setRid(beanNew.getRid().toString());
		myTaskBean.setCreatePeopleLoginName(createUser.getLoginName());
		myTaskBean.setOperationType(beanNew.getTaskElseState());
		myTaskBean.setStageName(beanNew.getStateMark());
		myTaskBean.setCreatePeopleId(beanNew.getCreatePersonId());
		return myTaskBean;
	}

	private String getTaskName(String name) {
		return name;
	}

	/**
	 * 取消任务代办
	 */
	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew beanNew, JecnUser jecnUser, JecnUser createUser) {
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());
		try {
			deleteToDoTask(myTaskBean, jecnUser.getPeopleId(), this.personDao);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());

		// 批量删除待办
		deleteTasks(myTaskBean, cancelPeoples);

		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
		// 审批任务，发送下一环节任务待办
		createTasks(myTaskBean);
	}

	private void deleteTasks(MyTaskBean myTaskBean, List<Long> handlerPeopleIds) throws Exception {
		for (Long handlerPeopleId : handlerPeopleIds) {
			deleteToDoTask(myTaskBean, handlerPeopleId, this.personDao);
		}
	}

	/**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 */
	public static String sendPost(String url, String param) {
		OutputStreamWriter out = null;
		BufferedReader in = null;
		String result = "";
		log.info("url = " + url);
		try {
			URL realUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
			// 打开和URL之间的连接

			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST"); // POST方法

			// 设置通用的请求属性

			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

			String auth = Base64.encode((username + ":" + password).getBytes());
			conn.setRequestProperty("Authorization", "Basic " + auth);

			conn.connect();

			// 获取URLConnection对象对应的输出流
			out = new OutputStreamWriter(conn.getOutputStream(), "utf-8");
			// 发送请求参数
			out.write(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			log.error("sendPost error " + url, e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	public static boolean postQueue(JSONObject msg) {
		String selfId = clientId + RandomStringUtils.random(6, true, true);
		Map<String, String> params = new HashMap<String, String>();
		params.put("clientId", selfId);
		params.put("type", "queue");
		BASE64Encoder encoder = new BASE64Encoder();
		String auth = encoder.encode((username + ":" + password).getBytes());
		params.put("body", msg.toString());
		List<NameValuePair> pairs = new ArrayList<NameValuePair>(params.size());
		for (Map.Entry<String, String> entry : params.entrySet()) {
			String value = entry.getValue();
			if (value != null) {
				pairs.add(new BasicNameValuePair(entry.getKey(), value));
			}
		}
		HttpPost login_post = new HttpPost(url);
		log.info(msg);
		try {
			// 定义HttpClient
			HttpClient httpClient = new DefaultHttpClient();
			// 传入请求头和数据
			login_post.addHeader("Content-type", "application/x-www-form-urlencoded");
			login_post.setHeader("Authorization", "Basic " + auth);//
			login_post.setEntity(new UrlEncodedFormEntity(pairs, Charset.forName("UTF-8")));

			// 执行请求
			HttpResponse httpResponse1 = httpClient.execute(login_post);
			InputStream in1 = httpResponse1.getEntity().getContent();

			StringBuilder sb1 = new StringBuilder();
			BufferedReader reader1 = new BufferedReader(new InputStreamReader(in1, "utf-8"));
			for (String line = reader1.readLine(); line != null; line = reader1.readLine()) {
				sb1.append(line);
			}
			in1.close();
			log.error(sb1);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			login_post.releaseConnection();
		}
		return true;
	}
}
