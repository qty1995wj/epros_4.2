package com.jecn.epros.server.dao.integration.impl;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.integration.IJecnActivityTypeDao;

/**
 * 活动类别Dao处理
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-10-31 时间：上午10:22:14
 */
public class JecnActivityTypeDaoImpl extends
		AbsBaseDao<JecnActiveTypeBean, Long> implements IJecnActivityTypeDao {

}
