package epros.designer.gui.rule.mode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public abstract class RuleModeTitelDialog extends JecnDialog {

	/** 主面板 */
	private JPanel mainPanel = null;

	/** 按钮面板 */
	private JPanel buttonPanel = null;

	/** 名称Lab */
	private JLabel nameLab = null;

	/** 名称Field */
	private JTextField nameField = null;
	
	/** 名称Lab */
	private JLabel nameLabEn = null;

	/** 名称Field */
	private JTextField nameFieldEn = null;

	/** 名称验证 */
	private JLabel namePrompt = null;

	private JLabel verfyNameRequiredLab = new JLabel(JecnProperties.getValue("required"));

	private JLabel verfyEnNameRequiredLab = new JLabel(JecnProperties.getValue("required"));


	/** 类型Lab */
	private JLabel typeLab = null;

	/** 类型Combox */
	private JComboBox typeCombox = null;

	/** 必填项 */
	private JLabel requiredLabel = null;

	/** 必填项 */
	private JComboBox requiredCombox = null;

	/** 确定按钮 */
	private JButton okBut = null;

	/** 取消按钮 */
	private JButton cancelBtn = null;

	public boolean isOperation = false;
	

	public JTextField getNameFieldEn() {
		return nameFieldEn;
	}

	public void setNameFieldEn(JTextField nameFieldEn) {
		this.nameFieldEn = nameFieldEn;
	}

	public JComboBox getRequiredCombox() {
		return requiredCombox;
	}

	public JTextField getNameField() {
		return nameField;
	}

	public JComboBox getTypeCombox() {
		return typeCombox;
	}

	public RuleModeTitelDialog() {
		initCompotents();
		initLayout();
		this.setModal(true);
	}

	/**
	 * 初始化组件
	 */
	public void initCompotents() {
		// 主面板
		mainPanel = new JPanel();

		// 按钮面板
		buttonPanel = new JPanel();

		// 名称Lab
		nameLab = new JLabel(JecnProperties.getValue("nameC"));
		// 英文名称Lab
		nameLabEn = new JLabel(JecnProperties.getValue("enNameC"));

		// 名称Field
		nameField = new JTextField();
		
		// 英文名称Field
		nameFieldEn = new JTextField();

		// 名称验证
		namePrompt = new JLabel();
		// 设置验证提示文字颜色
		namePrompt.setForeground(Color.red);

		// 类型Lab
		typeLab = new JLabel(JecnProperties.getValue("typeC"));

		// 类型Combox
		typeCombox = new JComboBox();
		typeCombox.addItem(JecnProperties.getValue("content"));
		typeCombox.addItem(JecnProperties.getValue("fileForm"));
		typeCombox.addItem(JecnProperties.getValue("flowForm"));
		typeCombox.addItem(JecnProperties.getValue("flowMapForm"));
		typeCombox.addItem(JecnProperties.getValue("standarForm"));
		typeCombox.addItem(JecnProperties.getValue("riskForm"));
		typeCombox.addItem(JecnProperties.getValue("ruleForm"));

		// 必填项
		requiredLabel = new JLabel(JecnProperties.getValue("basicTableHeaderName"));
		requiredCombox = new JComboBox();
		// 非必填
		requiredCombox.addItem(JecnProperties.getValue("task_emptyBtn"));
		requiredCombox.addItem(JecnProperties.getValue("task_notEmptyBtn"));

		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okBtnnAction();
			}
		});

		// 取消按钮
		cancelBtn = new JButton(JecnProperties.getValue("cancelBtn"));
		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelBtnAction();
			}
		});

		this.setSize(400, 220);
		// 窗体不可编辑
		this.setResizable(true);

		// 设置各个面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置面板大小
		// 设置按钮面板的大小
		/** 设置大小 */
		Dimension dimension = new Dimension(240, 30);
		buttonPanel.setPreferredSize(dimension);
		buttonPanel.setMinimumSize(dimension);

		// 设置名称Field的大小
		dimension = new Dimension(200, 25);
		nameField.setPreferredSize(dimension);
		nameField.setMinimumSize(dimension);
		// 设置类型ComboBox的大小
		dimension = new Dimension(200, 25);
		typeCombox.setPreferredSize(dimension);
		typeCombox.setMinimumSize(dimension);

		requiredCombox.setPreferredSize(dimension);
		requiredCombox.setMinimumSize(dimension);

		verfyNameRequiredLab.setForeground(Color.red);
		verfyEnNameRequiredLab.setForeground(Color.red);
	}

	/**
	 * 布局
	 */
	public void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());

		// 名称面板
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);

		int row = 0;
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(nameLab, c);
		c = new GridBagConstraints(1, row, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(nameField, c);
		c = new GridBagConstraints(2, row, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(verfyNameRequiredLab, c);
		row++;
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(nameLabEn, c);
		c = new GridBagConstraints(1, row, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(nameFieldEn, c);
		c = new GridBagConstraints(2, row, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(verfyEnNameRequiredLab, c);
		row++;
		// c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
		// GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE, insets,
		// 0, 0);
		// mainPanel.add(namePrompt,c);

		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(typeLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(typeCombox, c);
		row++;

		// 必填项
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(requiredLabel, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(requiredCombox, c);
		row++;

		c = new GridBagConstraints(0, row, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		row++;

		// 按钮面板
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(namePrompt);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBtn);

		c = new GridBagConstraints(0, 3, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0);
		mainPanel.add(new JLabel(), c);
		this.getContentPane().add(mainPanel);
		// 窗体居中显示
		this.setLocationRelativeTo(null);

	}

	private void okBtnnAction() {
		String name = nameField.getText().trim();
		String enName = nameFieldEn.getText().trim();
		// 验证名称是否正确
		// if (!JecnValidateCommon.validateName(name, namePrompt)) {
		// return;
		// }
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			namePrompt.setText(JecnProperties.getValue("nameC") + JecnProperties.getValue("isNotEmpty"));
			return;
		} else if (DrawCommon.checkNameMaxLength(name)) {
			// 名称不能超过122个字符或61个汉字
			namePrompt.setText(JecnProperties.getValue("nameC") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		if (DrawCommon.isNullOrEmtryTrim(enName)) {
			// 名称不能为空
			namePrompt.setText(JecnProperties.getValue("enNameC") + JecnProperties.getValue("isNotEmpty"));
			return;
		} else if (DrawCommon.checkNameMaxLength(enName)) {
			// 名称不能超过122个字符或61个汉字
			namePrompt.setText(JecnProperties.getValue("enNameC") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 验证名称是否重复
		if (validateNodeRepeat(name)) {
			namePrompt.setText(JecnProperties.getValue("nameHaved"));
			return;
		} else {
			namePrompt.setText("");
		}
		// 验证名称是否重复
		if (validateNodeRepeat(enName)) {
			namePrompt.setText(JecnProperties.getValue("nameHaved"));
			return;
		} else {
			namePrompt.setText("");
		}
		// 验证表单类型的模板标题的唯一性
		if (this.typeCombox.getSelectedIndex() == 2 && validateTitleType(JecnProperties.getValue("flowForm"))) {
			namePrompt.setText(JecnProperties.getValue("ruleModeFlowFormOnly"));
			return;
		} else {
			namePrompt.setText("");
		}
		// 验证流程地图表单类型的模板标题的唯一性
		if (this.typeCombox.getSelectedIndex() == 3 && validateTitleType(JecnProperties.getValue("flowMapForm"))) {
			namePrompt.setText(JecnProperties.getValue("ruleModeFlowMapFormOnly"));
			return;
		} else {
			namePrompt.setText("");
		}
		// 验证标准表单类型的模板标题的唯一性
		if (this.typeCombox.getSelectedIndex() == 4 && validateTitleType(JecnProperties.getValue("standarForm"))) {
			namePrompt.setText(JecnProperties.getValue("ruleModeStandardFormOnly"));
			return;
		} else {
			namePrompt.setText("");
		}
		// 验证风险表单类型的模板标题的唯一性
		if (this.typeCombox.getSelectedIndex() == 5 && validateTitleType(JecnProperties.getValue("riskForm"))) {
			namePrompt.setText(JecnProperties.getValue("ruleModeRiskFormOnly"));
			return;
		} else {
			namePrompt.setText("");
		}
		// 验证制度表单类型的模板标题的唯一性
		if (this.typeCombox.getSelectedIndex() == 6 && validateTitleType(JecnProperties.getValue("ruleForm"))) {
			namePrompt.setText(JecnProperties.getValue("ruleModeRuleFormOnly"));
			return;
		} else {
			namePrompt.setText("");
		}
		this.isOperation = true;
		this.dispose();
	}

	public abstract boolean validateNodeRepeat(String name);

	public abstract boolean validateTitleType(String titleType);

	private void cancelBtnAction() {
		this.dispose();
	}
}
