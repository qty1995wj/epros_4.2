/**
 * GetFinishWorkItemsBySystemIDResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetFinishWorkItemsBySystemIDResponse  implements java.io.Serializable {
    private java.lang.String getFinishWorkItemsBySystemIDResult;

    public GetFinishWorkItemsBySystemIDResponse() {
    }

    public GetFinishWorkItemsBySystemIDResponse(
           java.lang.String getFinishWorkItemsBySystemIDResult) {
           this.getFinishWorkItemsBySystemIDResult = getFinishWorkItemsBySystemIDResult;
    }


    /**
     * Gets the getFinishWorkItemsBySystemIDResult value for this GetFinishWorkItemsBySystemIDResponse.
     * 
     * @return getFinishWorkItemsBySystemIDResult
     */
    public java.lang.String getGetFinishWorkItemsBySystemIDResult() {
        return getFinishWorkItemsBySystemIDResult;
    }


    /**
     * Sets the getFinishWorkItemsBySystemIDResult value for this GetFinishWorkItemsBySystemIDResponse.
     * 
     * @param getFinishWorkItemsBySystemIDResult
     */
    public void setGetFinishWorkItemsBySystemIDResult(java.lang.String getFinishWorkItemsBySystemIDResult) {
        this.getFinishWorkItemsBySystemIDResult = getFinishWorkItemsBySystemIDResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetFinishWorkItemsBySystemIDResponse)) return false;
        GetFinishWorkItemsBySystemIDResponse other = (GetFinishWorkItemsBySystemIDResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getFinishWorkItemsBySystemIDResult==null && other.getGetFinishWorkItemsBySystemIDResult()==null) || 
             (this.getFinishWorkItemsBySystemIDResult!=null &&
              this.getFinishWorkItemsBySystemIDResult.equals(other.getGetFinishWorkItemsBySystemIDResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetFinishWorkItemsBySystemIDResult() != null) {
            _hashCode += getGetFinishWorkItemsBySystemIDResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetFinishWorkItemsBySystemIDResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetFinishWorkItemsBySystemIDResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getFinishWorkItemsBySystemIDResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GetFinishWorkItemsBySystemIDResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
