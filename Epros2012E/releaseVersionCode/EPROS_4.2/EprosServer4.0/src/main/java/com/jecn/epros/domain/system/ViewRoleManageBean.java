package com.jecn.epros.domain.system;

import java.util.List;

public class ViewRoleManageBean {

    private int total = 0;
    private List<ViewRoleDetailBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ViewRoleDetailBean> getData() {
        return data;
    }

    public void setData(List<ViewRoleDetailBean> data) {
        this.data = data;
    }
}
