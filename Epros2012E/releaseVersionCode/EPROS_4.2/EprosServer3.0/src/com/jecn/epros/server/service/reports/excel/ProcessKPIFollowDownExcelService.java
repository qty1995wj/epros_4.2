package com.jecn.epros.server.service.reports.excel;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.block.BorderArrangement;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.CompositeTitle;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.Layer;
import org.jfree.ui.RectangleEdge;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.reports.ProcessKPIFollowBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIWebBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIWebListBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIWebValueBean;

/**
 * 
 * 下载流程KPI跟踪表
 * 
 * @author huoyl
 * 
 */
public class ProcessKPIFollowDownExcelService {

	private Log log = LogFactory.getLog(this.getClass());
	/** 数据对象 */
	private ProcessKPIFollowBean processKPIFollowBean;

	public ProcessKPIFollowBean getProcessKPIFollowBean() {
		return processKPIFollowBean;
	}

	public void setProcessKPIFollowBean(
			ProcessKPIFollowBean processKPIFollowBean) {
		this.processKPIFollowBean = processKPIFollowBean;
	}

	public byte[] getExcelFile() throws Exception {

		byte[] tempDate = null;
		if (processKPIFollowBean == null) {
			throw new Exception(
					"ProcessKPIFollowDownExcelService类processKPIFollowBean为空");
		}
		if (0 == processKPIFollowBean.getkPIWebListBean().size()) {
			throw new Exception(
					"ProcessKPIFollowDownExcelService类processKPIFollowBean.getkPIWebListBean().size()为0");
		}
		List<ProcessKPIWebListBean> kPIWebListBeans = processKPIFollowBean
				.getkPIWebListBean();

		// 设置kpi报表中的sheet的名称
		setProcessKpiSheetName(kPIWebListBeans);

		WritableWorkbook wbookData = null;
		File file = null;
		List<File> imageFiles = new ArrayList<File>();
		try {
			// 当前sheet页
			int curSheet = 0;
			// 图片高度
			int imageHeight = 400;
			// 图片宽度
			int imageWidth = 300;
			// 行
			int row = 0;
			// 列
			int col = 0;
			// 序号
			int index = 1;
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path
					+ JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);

			// 序号
			String indexStr = JecnUtil.getValue("index");
			// 流程名称
			String processNameOrKeyProcess = JecnUtil
					.getValue("processNameOrKeyProcess");
			// 责任部门
			String responsibilityDepartment = JecnUtil
					.getValue("responsibilityDepartment");
			// 流程责任人
			String processResponsiblePersons = JecnUtil
					.getValue("processResponsiblePersons");
			// 流程监护人
			String processTheGuardian = JecnUtil.getValue("processTheGuardian");
			// 流程拟制人
			String processArtificialPerson = JecnUtil
					.getValue("processArtificialPerson");
			// 定义样式
			WritableCellFormat contentStyle = getSheetContentStyle();
			// 表头的样式 背景为黄色
			WritableCellFormat titleStyleY = getSheetTitleStyle();
			// 表头的样式 背景为橘黄色
			WritableCellFormat titleStyleO = getSheetTitleStyle();
			titleStyleO.setBackground(Colour.LIGHT_ORANGE);
			// 流程名称的样式 背景为红色字体为白色
			WritableCellFormat processNameStyle = getSheetProcessNameStyleStyle();
			// 流程监护人是否显示
			boolean guardianPeopleIsShow = processKPIFollowBean
					.isGuardianPeopleIsShow();
			// 流程拟制人是否显示
			boolean artificialPersonIsShow = processKPIFollowBean
					.isArtificialPersonIsShow();
			// kpi属性是否显示
			boolean kpiProperty = processKPIFollowBean.isKpiPropertyIsShow();

			// kpi目标值合并的单元格的个数
			int kpiTargetCos = 0;
			// 写出动态的内容
			for (JecnConfigItemBean itemBean : processKPIFollowBean
					.getConfigList()) {

				if (itemBean.getMark().equals(
						ConfigItemPartMapMark.kpiTargetValue.toString())) {
					kpiTargetCos += 2;
				} else if (itemBean.getMark().equals(
						ConfigItemPartMapMark.kpiUtilName.toString())) {
					kpiTargetCos += 1;
				}

			}

			// 写出所有数据
			for (ProcessKPIWebListBean kPIBean : kPIWebListBeans) {

				// 每一个部门是一个sheet页
				WritableSheet wsheet = wbookData.createSheet(kPIBean
						.getSheetName(), curSheet);
				// 写出每一个部门下的数据
				List<ProcessKPIWebBean> allKPIWebBeans = kPIBean
						.getAllKPIWebBeans();
				// 调整列宽
				// 索引
				wsheet.setColumnView(col++, 5);
				// 流程名称（或关键过程）
				wsheet.setColumnView(col++, 28);
				// 流程责任部门
				wsheet.setColumnView(col++, 24);
				// 流程责任人
				wsheet.setColumnView(col++, 11);
				// 流程监护人
				if (guardianPeopleIsShow) {
					wsheet.setColumnView(col++, 11);
				}
				// 流程拟制人
				if (artificialPersonIsShow) {
					wsheet.setColumnView(col++, 11);
				}

				String showValue = "";
				for (ProcessKPIWebBean webBean : allKPIWebBeans) {
					col = 0;
					// 设置高度
					wsheet.setRowView(row, 480);
					// 表头
					wsheet
							.addCell(new Label(col++, row, indexStr,
									titleStyleY));
					wsheet.addCell(new Label(col++, row,
							processNameOrKeyProcess, titleStyleY));
					wsheet.addCell(new Label(col++, row,
							responsibilityDepartment, titleStyleY));
					wsheet.addCell(new Label(col++, row,
							processResponsiblePersons, titleStyleY));
					if (guardianPeopleIsShow) {
						wsheet.addCell(new Label(col++, row,
								processTheGuardian, titleStyleY));
					}
					if (artificialPersonIsShow) {
						wsheet.addCell(new Label(col++, row,
								processArtificialPerson, titleStyleY));
					}


					// 写出动态表头标题
					for (JecnConfigItemBean itemBean : processKPIFollowBean
							.getConfigList()) {

						wsheet.setColumnView(col, 11);
						wsheet.addCell(new Label(col++, row,
								itemBean.getName(), titleStyleO));

					}

					// 动态的日期标题
					for (ProcessKPIWebValueBean kpiValueBean : webBean
							.getKpiValueList()) {
						wsheet.setColumnView(col, 10);
						// 表头的日期
						wsheet.addCell(new Label(col++, row, kpiValueBean
								.getKpiHorVlaue(), contentStyle));
					}

					// 写入下一行
					row++;
					col = 0;

					// 写入内容
					// 设置高度
					wsheet.setRowView(row, 480);
					wsheet.addCell(new Label(col++, row, String.valueOf(index),
							contentStyle));
					wsheet.addCell(new Label(col++, row, webBean.getFlowName(),
							contentStyle));
					wsheet.addCell(new Label(col++, row, webBean.getOrgName(),
							contentStyle));
					wsheet.addCell(new Label(col++, row, webBean
							.getResPeopleName(), contentStyle));
					if (guardianPeopleIsShow) {
						wsheet.addCell(new Label(col++, row, webBean
								.getWardPeopleName(), contentStyle));
					}
					if (artificialPersonIsShow) {
						wsheet.addCell(new Label(col++, row, webBean
								.getDraftPersonName(), contentStyle));
					}

					// 写出动态的内容
					for (JecnConfigItemBean itemBean : processKPIFollowBean
							.getConfigList()) {

						showValue = getShowValueByMark(itemBean.getMark(),
								webBean);
						wsheet.addCell(new Label(col++, row, showValue,
								contentStyle));

					}


					// 写出值
					for (ProcessKPIWebValueBean kpiValueBean : webBean
							.getKpiValueList()) {
						// 表头的日期
						wsheet.addCell(new Label(col++, row, String
								.valueOf(kpiValueBean.getKpiValue()),
								contentStyle));
					}

					row++;

					// 写出图片
					if (webBean.getKpiValueList().size() > 0) {
						// 写出流程名称
						// 合并单元格
						wsheet.mergeCells(1, row, 5, row + 1);
						wsheet.addCell(new Label(1, row, webBean.getFlowName(),
								processNameStyle));
						row = row + 2;
						// 写出图片
						JFreeChart localJFreeChart = createChart(
								createDataset(webBean), webBean);

						int valueSize = webBean.getKpiValueList().size();

						// 将值与值之间的间隔设为20
						if (valueSize > 5) {
							imageWidth = valueSize * 60;
						}
						// 生成的图片右侧的图片定为100
						imageWidth = imageWidth + 100;
						File tempFile = jfreeChartToPng(localJFreeChart,
								imageWidth, imageHeight);
						// imageFiles.add(tempFile);
						// 插入到Excel中
						JecnUtil.addPictureToExcel(wsheet, tempFile, row, 1);
						// 图片的高度大概相当于24格
						row = row + 24;
					}
					row = row + 1;
					col = 0;
					index++;
				}

				// 重新开始一个新的sheet页
				row = 0;
				col = 0;
				// 序号的初始值
				index = 1;
				curSheet++;
			}

			// 后续处理--------------------------------------
			wbookData.write();
			// 工作簿不关闭不会写出数据
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;
			// 把一个文件转化为字节
			tempDate = JecnUtil.getByte(file);

		} catch (Exception e) {
			throw new Exception(
					"ProcessKPIFollowDownExcelService类getExcelFile方法异常", e);

		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();

				} catch (Exception e) {
					throw new Exception(
							"ProcessKPIFollowDownExcelService类关闭excel工作表异常", e);
				}
			}
			// 删除临时文件临时图片
			if (file != null && file.exists()) {
				file.delete();
			}
			for (File tempFile : imageFiles) {
				if (tempFile.exists()) {
					tempFile.delete();
				}
			}
		}

		return tempDate;

	}

	private String getShowValueByMark(String mark, ProcessKPIWebBean webBean) {

		String showValue = "";
		if (mark.equals(ConfigItemPartMapMark.kpiName.toString())) {
			showValue = webBean.getKpiName();
		}
		else if (mark.equals(ConfigItemPartMapMark.kpiDesignFormulas.toString())) {
			// 计算公式
			showValue = webBean.getKpiStatisticalMethods();
		} else if (mark.equals(ConfigItemPartMapMark.kpiTargetValue.toString())) {
			// 目标值
			showValue = String.valueOf(webBean.getKpiTarget());
		} else if (mark.equals(ConfigItemPartMapMark.kpiType.toString())) {
			// kpi类型
			showValue = webBean.getKpiType();
		} else if (mark.equals(ConfigItemPartMapMark.kpiUtilName.toString())) {
			// kpi值单位名称
			showValue = webBean.getKpiVertical();

		} else if (mark.equals(ConfigItemPartMapMark.kpiTimeAndFrequency
				.toString())) {
			// 数据统计时间\频率
			showValue = webBean.getKpiHorizontal();

		} else if (mark.equals(ConfigItemPartMapMark.kpiDataAccess.toString())) {
			// 数据获取方式 TODO
			showValue = webBean.getKpiDataMethod();

		} else if (mark.equals(ConfigItemPartMapMark.kpiSourceIndex.toString())) {
			// 数据获取方式 TODO
			showValue = webBean.getKpiTargetType();
		} else if (mark.equals(ConfigItemPartMapMark.kpiDefined.toString())) {
			// kpi定义
			showValue = webBean.getKpiDefinition();
		} else if (mark.equals(ConfigItemPartMapMark.kpiRrelevancy.toString())) {
			// 相关度
			showValue = webBean.getKpiRelevance();
		} else if (mark.equals(ConfigItemPartMapMark.kpiIdSystem.toString())) {
			// IT系统
			showValue = webBean.getItSystem();
		} else if (mark
				.equals(ConfigItemPartMapMark.kpiDataProvider.toString())) {
			// 数据提供者名称19
			showValue = webBean.getKpiDataPeopleName();
		} else if (mark.equals(ConfigItemPartMapMark.kpiSupportLevelIndicator
				.toString())) {
			// 支撑的一级指标内容20
			showValue = webBean.getKpiFirstTarget();
		} else if (mark.equals(ConfigItemPartMapMark.kpiPurpose.toString())) {

			showValue = webBean.getKpiPurpose();
		} else if (mark.equals(ConfigItemPartMapMark.kpiPoint.toString())) {

			showValue = webBean.getKpiPoint();
		} else if (mark.equals(ConfigItemPartMapMark.kpiExplain.toString())) {

			showValue = webBean.getKpiExplain();
		} else if (mark.equals(ConfigItemPartMapMark.kpiPeriod.toString())) {

			showValue = webBean.getKpiPeriod();
		}

		return showValue;
	}

	/**
	 * 设置Kpi报表的sheet名称（防止重复）
	 * 
	 * @param kPIWebListBeans
	 */
	private void setProcessKpiSheetName(
			List<ProcessKPIWebListBean> kPIWebListBeans) {

		Map<String, Integer> sheetNames = new HashMap<String, Integer>();
		for (ProcessKPIWebListBean bean : kPIWebListBeans) {
			String sheetName = bean.getName();
			if (!sheetNames.keySet().contains(sheetName)) {
				sheetNames.put(bean.getName(), 0);
			} else {// 如果sheet的名称重复那么累加1
				sheetNames.put(sheetName, sheetNames.get(sheetName) + 1);
				sheetName = sheetName + "(" + sheetNames.get(sheetName) + ")";
			}
			bean.setSheetName(sheetName);
		}

	}

	/**
	 * 数据源
	 * 
	 * @param webBean
	 *            ProcessKPIWebBean 流程kpi的显示bean
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private CategoryDataset createDataset(ProcessKPIWebBean webBean) {
		DefaultCategoryDataset localDefaultCategoryDataset = new DefaultCategoryDataset();

		List<ProcessKPIWebValueBean> kpiValueList = webBean.getKpiValueList();
		String kpiName = webBean.getKpiName();
		for (ProcessKPIWebValueBean kpiWebValue : kpiValueList) {
			localDefaultCategoryDataset.addValue(kpiWebValue.getKpiValue(),
					kpiName, kpiWebValue.getKpiHorVlaue());

		}
		// 目标值
		double kpiTarget = webBean.getKpiTarget();
		if (-1 != kpiTarget) {
			localDefaultCategoryDataset.addValue(webBean.getKpiTarget(),
					JecnUtil.getValue("targetValue"), "");

		}

		return localDefaultCategoryDataset;
	}

	/*
	 * 将数据显示出来
	 */
	private JFreeChart createChart(CategoryDataset paramCategoryDataset,
			ProcessKPIWebBean webBean) {

		double maxCount = 0;
		int unitNum = 0;
		List<ProcessKPIWebValueBean> kpiValueList = webBean.getKpiValueList();
		for (ProcessKPIWebValueBean kpiValue : kpiValueList) {
			double kpiVal = kpiValue.getKpiValue();
			if (kpiVal > maxCount) {
				maxCount = kpiVal;
			}

		}
		if (maxCount > 10) {
			unitNum = (int) maxCount / 10;
			maxCount = maxCount + maxCount / 10;

		} else {
			maxCount = 11;
			unitNum = 1;
		}

		String unit = webBean.getKpiVertical();
		if (unit == null) {
			unit = "";
		}
		JFreeChart localJFreeChart = ChartFactory.createLineChart(webBean
				.getKpiName(), "", JecnUtil.getValue("unit") + ":" + unit,
				paramCategoryDataset, PlotOrientation.VERTICAL, false, false,
				false);
		CategoryPlot localCategoryPlot = (CategoryPlot) localJFreeChart
				.getPlot();

		// 折线的描述（就是描述各种类型的线或者图）
		LegendTitle legend = new LegendTitle(localCategoryPlot);
		legend.setPosition(RectangleEdge.LEFT);
		BlockContainer blockcontainer = new BlockContainer(
				new BorderArrangement());
		blockcontainer.add(legend, RectangleEdge.LEFT);// 这行代码可以控制位置
		CompositeTitle compositetitle = new CompositeTitle(blockcontainer);
		compositetitle.setPosition(RectangleEdge.RIGHT);// 放置图形代表区域位置的代码
		localJFreeChart.addSubtitle(compositetitle);

		// 背景色
		localCategoryPlot.setBackgroundAlpha(0.5f);

		CategoryAxis domainAxis = localCategoryPlot.getDomainAxis();
		// 设置两列之间间隔的宽度
		domainAxis.setCategoryMargin(0.1);

		// 下边栏的倾斜度
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);

		double kpiTarget = webBean.getKpiTarget();

		// 水平线 目标值为-1时不显示
		if (-1 != kpiTarget) {
			if (kpiTarget > maxCount) {
				unitNum = (int) (kpiTarget / 10);
				maxCount = kpiTarget + unitNum;

			}
			IntervalMarker localIntervalMarker = new IntervalMarker(kpiTarget,
					kpiTarget);
			// 填充的颜色
			localIntervalMarker.setOutlinePaint(Color.red);
			localCategoryPlot.addRangeMarker(localIntervalMarker,
					Layer.BACKGROUND);
		}

		NumberAxis localNumberAxis = (NumberAxis) localCategoryPlot
				.getRangeAxis();

		// 设置刻度长度
		localNumberAxis.setAutoTickUnitSelection(false);

		NumberTickUnit leftNtu = new NumberTickUnit(unitNum);
		localNumberAxis.setTickUnit(leftNtu);
		// 设置最大值
		localNumberAxis.setUpperBound(maxCount);
		// 将纵坐标的那列注册到左边还是右边
		localCategoryPlot.setRangeAxis(0, localNumberAxis);

		localNumberAxis.setStandardTickUnits(NumberAxis
				.createIntegerTickUnits());
		LineAndShapeRenderer localLineAndShapeRenderer = (LineAndShapeRenderer) localCategoryPlot
				.getRenderer(0);
		localLineAndShapeRenderer.setBaseShapesVisible(true);
		localLineAndShapeRenderer.setDrawOutlines(true);
		localLineAndShapeRenderer.setUseFillPaint(true);

		localLineAndShapeRenderer
				.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		localLineAndShapeRenderer.setBaseItemLabelsVisible(true);

		// 设置折线的颜色
		localLineAndShapeRenderer.setSeriesPaint(0, Color.BLUE);

		localLineAndShapeRenderer.setSeriesPaint(1, Color.RED);

		// 每一个刻度的横线
		localLineAndShapeRenderer.setBaseFillPaint(Color.white);

		return localJFreeChart;
	}

	/**
	 * 流程名称的样式
	 */
	private WritableCellFormat getSheetProcessNameStyleStyle()
			throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 15,
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.WHITE);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.RED);
		return tempCellFormat;
	}

	/**
	 * 设置表头的样式
	 */
	private WritableCellFormat getSheetTitleStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.YELLOW);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);
		return tempCellFormat;
	}

	/**
	 * 表头的内容样式
	 */
	private WritableCellFormat getSheetContentStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		return tempCellFormat;
	}

	private File jfreeChartToPng(JFreeChart localJFreeChart, int width,
			int height) {

		String path = JecnPath.APP_PATH;
		// 获取存储临时文件的目录
		String tempFileDir = path + "images/tempImage/" + "PROCESS_KPI/";
		// 创建临时文件目录
		File fileDir = new File(tempFileDir);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		String timefile = UUID.randomUUID().toString();
		String fileName = tempFileDir + timefile + ".png";
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fileName);
			ChartUtilities.writeChartAsPNG(fos, localJFreeChart, width, height);

			fos.flush();
		} catch (Exception e) {
			log.error("ProcessKPIFollowDownExcelService生成图片异常", e);
		} finally {

			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {

					log.error("ProcessKPIFollowDownExcelService关闭图片流异常", e);
				}

			}
		}
		return new File(fileName);
	}

}
