package com.jecn.epros.server.action.web.integration;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.integration.IJecnRiskService;
import com.jecn.epros.server.service.integration.buss.RiskListDownload;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.integration.RiskDataListBean;
import com.jecn.epros.server.webBean.integration.RiskDetailBean;
import com.jecn.epros.server.webBean.integration.RiskList;
import com.jecn.epros.server.webBean.integration.RiskRelatedProcessBean;
import com.jecn.epros.server.webBean.integration.RiskRelatedRuleBean;
import com.jecn.epros.server.webBean.integration.RiskSystemSearchBean;
import com.jecn.epros.server.webBean.integration.RiskWebBean;

/**
 * 风险体系Web Action
 * 
 * @author Administrator
 * 
 */
public class RiskAction extends BaseAction {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(RiskAction.class);

	/** 当前树节点 */
	private long node;
	/** 开始行数 */
	private int start;
	/** 每页显示条数 */
	private int limit;
	/** Risk请求方式 */
	private String visitType;
	/** 风险点ID */
	private long id;
	/** 风险ID */
	private long riskId;
	/** 根据ID查询风险点详情 */
	private String riskDetailId;
	/** 风险描述 */
	private String name;
	/** 风险编号 */
	private String riskCode;
	/** 是否显示全部 */
	private boolean showAll;

	/** 风险Service接口 */
	private IJecnRiskService riskService;

	/** 查询风险点详细信息 */
	private RiskDetailBean riskDetailBean;
	/** 风险相关制度 */
	private List<RiskRelatedRuleBean> riskRelatedRuleBean;
	/** 风险相关流程 */
	private List<RiskRelatedProcessBean> listRiskRelatedProcessBean;

	/** 打开文件文件流 */
	private byte[] content = null;
	/** 文件名称 */
	private String fileName;
	/** 风险对应流程的责任部门名称 */
	private String orgName;
	/** 风险对应流程的责任部门ID */
	private Long orgId;

	private boolean ruleTypeIsShow;

	/**
	 * 相关制度
	 * 
	 * @return
	 * @author ZHF
	 */
	public String getRelatedRuleByPid() {
		try {
			riskRelatedRuleBean = riskService.getRiskRelatedRuleById(id);
		} catch (Exception e) {
			log.error("相关制度查询出错", e);
		}
		return SUCCESS;
	}

	/**
	 * 相关流程
	 * 
	 * @author ZHF
	 */
	public String getRiskRelatedProcessBeanById() {
		try {
			listRiskRelatedProcessBean = riskService.getRiskRelatedProcessBeanById(id);
		} catch (Exception e) {
			log.error("查询相关流程出错！", e);
		}
		return SUCCESS;
	}

	/**
	 * 获取风险目录下子节点
	 * 
	 * @return
	 * @author ZHF
	 */
	public String getChildRisks() {
		try {
			List<JecnTreeBean> list = riskService.getChildsRisk(node, getProjectId());
			if (list != null && list.size() > 0) {
				List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
				for (JecnTreeBean jecnTreeBean : list) {
					WebTreeBean webTreeBean = new WebTreeBean();
					webTreeBean.setId(jecnTreeBean.getId().toString());
					webTreeBean.setText(jecnTreeBean.getName());
					webTreeBean.setLeaf(!jecnTreeBean.isChildNode());
					webTreeBean.setType(jecnTreeBean.getTreeNodeType().toString());
					webTreeBean.setIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString() + ".gif");
					listWebTreeBean.add(webTreeBean);
				}
				JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("服务端风险树加载出错：", e);
		}
		return null;
	}

	/**
	 * 风险体系搜索
	 * 
	 * @return
	 */
	public String getRiskList() {
		RiskSystemSearchBean riskSystemSearchBean = new RiskSystemSearchBean();
		riskSystemSearchBean.setName(name);
		riskSystemSearchBean.setRiskCode(riskCode);
		riskSystemSearchBean.setOrgId(orgId);
		riskSystemSearchBean.setOrgName(orgName);
		try {
			int total = riskService.getRiskTotal(riskSystemSearchBean, getProjectId());
			List<RiskWebBean> list = riskService.getRiskList(riskSystemSearchBean, start, limit, getProjectId());

			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("风险体系探索出错：", e);
		}
		return null;
	}

	/**
	 * 风险基本信息和使用情况
	 * 
	 * @return
	 */
	public String getRiskDetailById() {
		try {
			riskDetailBean = riskService.getJecnRiskByIdForWeb(id);
			riskRelatedRuleBean = riskService.getRiskRelatedRuleById(id);
			listRiskRelatedProcessBean = riskService.getRiskRelatedProcessBeanById(id);
			ruleTypeIsShow = JecnUtil.ifValueIsOneReturnTrue("ruleType");
		} catch (Exception e) {
			log.error("风险基本信息查询出错：", e);
		}
		return SUCCESS;
	}

	public String getRiskById() {
		try {
			riskDetailBean = riskService.getJecnRiskByIdForWeb(Long.valueOf(riskDetailId));
			riskRelatedRuleBean = riskService.getRiskRelatedRuleById(Long.valueOf(riskDetailId));
			listRiskRelatedProcessBean = riskService.getRiskRelatedProcessBeanById(Long.valueOf(riskDetailId));
		} catch (Exception e) {
			log.error("风险基本信息查询出错：", e);
		}
		return SUCCESS;
	}

	/**
	 * 根据风险目录ID查询当前所有子节点
	 * 
	 * @return
	 */
	public String getRiskListByPid() {
		try {
			int total = riskService.getRiskTotalByPid(riskId);
			if (total > 0) {
				List<RiskWebBean> listRiskWebBean = riskService.getRiskListByPid(riskId, start, limit);
				JSONArray jsonArray = JSONArray.fromObject(listRiskWebBean);
				outJsonPage(jsonArray.toString(), total);
			} else {
				outJsonPage("[]", total);
			}
		} catch (Exception e) {
			log.error("双击目录节点查询风险列表错误！", e);
		}
		return null;
	}

	/**
	 * 风险清单
	 * 
	 * @author fuzhh 2013-12-13
	 * @return
	 */
	public String findRiskList() {
		try {
			// 获取传入ID所在的节点
			int level = riskService.findRiskListLevel(riskId);
			// 清单数据
			RiskList riskList = riskService.getRiskInventoryALL(riskId, getProjectId(), level, showAll);
			// stores
			String stores = riskListStores(riskList);
			// columns
			String columns = riskListColumns(riskList, level);
			// 内容
			String content = riskListContent(riskList, showAll);
			// 返回Json
			outJsonString(JecnCommon.composeJson(stores, columns, content, 0, 0, 0, riskList.getRiskCount()));
		} catch (Exception e) {
			log.error("查询风险清单错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * 风险清单下载
	 * 
	 * @author fuzhh 2013-12-17
	 * @return
	 */
	public String downfindRiskList() {
		try {
			// 获取传入ID所在的节点
			int level = riskService.findRiskListLevel(riskId);
			// 清单数据
			RiskList riskList = riskService.getRiskInventoryALL(riskId, getProjectId(), level, true);
			content = RiskListDownload.createRiskListDesc(riskList, level);
			// 风险清单
			fileName = JecnUtil.getValue("riskList") + ".xls";
		} catch (Exception e) {
			log.error("下载时查询风险清单错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * 风险清单 Stores
	 * 
	 * @author fuzhh 2013-12-16
	 * @param riskList
	 * @return
	 */
	private String riskListStores(RiskList riskList) {
		StringBuffer storesBuf = new StringBuffer();
		storesBuf.append("[");
		for (int i = 0; i < riskList.getLevelTotal(); i++) {
			// 风险名称
			storesBuf.append(JecnCommon.storesJson(i + "riskName", i + "riskName") + ",");
		}
		// 风险点编号
		storesBuf.append(JecnCommon.storesJson("riskCode", "riskCode") + ",");
		// 风险描述
		storesBuf.append(JecnCommon.storesJson("riskName", "riskName") + ",");
		// 风险等级
		storesBuf.append(JecnCommon.storesJson("grade", "grade") + ",");
		// 相关要求
		storesBuf.append(JecnCommon.storesJson("requirements", "requirements") + ",");
		// 标准化控制
		storesBuf.append(JecnCommon.storesJson("standardControl", "standardControl") + ",");
		// 控制目标
		storesBuf.append(JecnCommon.storesJson("controlDescription", "controlDescription") + ",");
		// 控制编号
		storesBuf.append(JecnCommon.storesJson("controlNum", "controlNum") + ",");
		// 控制活动简述
		storesBuf.append(JecnCommon.storesJson("activeShow", "activeShow") + ",");
		// 角色名称
		storesBuf.append(JecnCommon.storesJson("roleName", "roleName") + ",");
		// 活动名称
		storesBuf.append(JecnCommon.storesJson("activeName", "activeName") + ",");
		// 流程名称
		storesBuf.append(JecnCommon.storesJson("flowName", "flowName") + ",");
		// 制度名称
		storesBuf.append(JecnCommon.storesJson("ruleName", "ruleName") + ",");
		// 责任部门
		storesBuf.append(JecnCommon.storesJson("resOrgName", "resOrgName") + ",");
		// 控制方法
		storesBuf.append(JecnCommon.storesJson("controlMethod", "controlMethod") + ",");
		// 控制类型
		storesBuf.append(JecnCommon.storesJson("controlType", "controlType") + ",");
		// 是否为关键控制
		storesBuf.append(JecnCommon.storesJson("keyControl", "keyControl") + ",");
		// 控制频率
		storesBuf.append(JecnCommon.storesJson("controlFrequency", "controlFrequency") + "]");
		return storesBuf.toString();
	}

	/**
	 * 风险清单 Columns
	 * 
	 * @author fuzhh 2013-12-16
	 * @param riskList
	 * @param depth
	 * @return
	 */
	private String riskListColumns(RiskList riskList, int depth) {
		StringBuffer columnsBuf = new StringBuffer();
		columnsBuf.append("[new Ext.grid.RowNumberer(),");
		for (int i = 0; i < riskList.getLevelTotal(); i++) {
			if (i >= depth) {
				// 级
				columnsBuf.append(JecnCommon.columnsJson(i + JecnUtil.getValue("level"), i + "riskName", null) + ",");
			}
		}
		// 风险点编号
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("riskNumber"), "riskCode", null) + ",");
		// 风险描述
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("riskName"), "riskName", null) + ",");
		// 风险等级
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("riskGrade"), "grade", null) + ",");
		// 相关要求
		columnsBuf.append(JecnCommon
				.columnsJson(JecnUtil.getValue("DetailGuide"), "requirements", "commonNameRenderer")
				+ ",");
		// 标准化控制
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("standardControl"), "standardControl", null) + ",");
		// 控制目标
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("controlTarget"), "controlDescription", null) + ",");
		// 控制编号
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("controlNumber"), "controlNum", null) + ",");
		// 控制活动简描述
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("controlActiveDescription"), "activeShow",
				"commonNameRenderer")
				+ ",");
		// 角色名称
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("roleName"), "roleName", null) + ",");
		// 活动名称
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("activityTitle"), "activeName", null) + ",");
		// 流程名称
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("processName"), "flowName", null) + ",");
		// 制度名称
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("ruleName"), "ruleName", null) + ",");
		// 责任部门
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("responsibilityDepartment"), "resOrgName", null)
				+ ",");
		// 控制方法：人工/自动
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("controlMethod"), "controlMethod", null) + ",");
		// 控制类型：预防性/发现性
		columnsBuf
				.append(JecnCommon.columnsJson(JecnUtil.getValue("controlTypePreventive"), "controlType", null) + ",");
		// 是否为关键控制
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("whetherItIsTheKeyToControl"), "keyControl", null)
				+ ",");
		// 控制频率
		columnsBuf
				.append(JecnCommon.columnsJson(JecnUtil.getValue("controlFrequency"), "controlFrequency", null) + "]");
		return columnsBuf.toString();
	}

	/***
	 * 递归查找当前风险的所有父级目录，并且拼装到JSON中
	 * 
	 * @param riskList
	 *            风险清单封装 riskDataBean 单个风险清单数据 bufer
	 * @author chehuanbo
	 * @since V3.06
	 */
	private void findDir(RiskList riskList, RiskDataListBean riskDataBean, StringBuffer bufer) {

		RiskDataListBean riskDataListBean1 = riskDataBean;
		boolean isExit = false; // 判断是否存在父节点 ，默认：false 不存在
		for (RiskDataListBean perRiskListBean : riskList.getDataListBeans()) {
			if (riskDataListBean1.getParentId().longValue() == perRiskListBean.getRiskId()) {
				// 风险名称 （将目录名称添加到Json中）
				bufer.append(JecnCommon.contentJson(perRiskListBean.getLevel() + "riskName", perRiskListBean
						.getRiskName())
						+ ",");
				// 更改临时数据bean 主要用来下一次查找父级节点的查找
				riskDataListBean1 = perRiskListBean;
				isExit = true;
				break;
			}
		}
		if (isExit) { // 如果存在父节点，继续递归查找
			findDir(riskList, riskDataListBean1, bufer);
		}
	}

	/**
	 * 风险清单内容
	 * 
	 * @author fuzhh 2013-12-16
	 * @param riskList
	 * @param depth
	 *            当前选中
	 * @return
	 */
	private String riskListContent(RiskList riskList, boolean showAll) {

		// 风险数据拼装
		StringBuffer riskStr = new StringBuffer("[");
		int num = 0;
		for (int i = 0; i < riskList.getDataListBeans().size(); i++) {
			RiskDataListBean riskListBean = riskList.getDataListBeans().get(i);
			// 目录
			if (riskListBean.getIsDir() == 0) {
				continue;
			}
			if (!showAll && num == 20) {
				break;
			}
			num++;
			riskStr.append("{"); // JSON数据拼装开始

			// 查找当前风险所有父级目录方法
			findDir(riskList, riskListBean, riskStr);

			// 风险点编号
			String riskNum = riskListBean.getRiskCode();
			if (riskListBean.getIsDir() == 1) {
				riskNum = "<a href='getRiskById.action?riskDetailId=" + riskListBean.getRiskId()
						+ "' style='cursor: pointer;' target='_blank' class='table'>" + riskListBean.getRiskCode()
						+ "</a>";
			}
			riskStr.append(JecnCommon.contentJson("riskCode", riskNum) + ",");

			// 风险描述
			if (riskListBean.getIsDir() == 1) {
				riskStr.append(JecnCommon.contentJson("riskName", riskListBean.getRiskName()) + ",");
			}

			// 风险等级
			String grade = "";
			if (riskListBean.getRiskGrade() == 1) {
				// 高
				grade = JecnUtil.getValue("high");
			} else if (riskListBean.getRiskGrade() == 2) {
				// 中
				grade = JecnUtil.getValue("In");
			} else if (riskListBean.getRiskGrade() == 3) {
				// 低
				grade = JecnUtil.getValue("Low");
			}
			riskStr.append(JecnCommon.contentJson("grade", grade) + ",");

			// 相关要求
			riskStr.append(JecnCommon.contentJson("requirements", riskListBean.getGuideVal()) + ",");

			// 标准化控制
			riskStr.append(JecnCommon.contentJson("standardControl", riskListBean.getStandardControl()) + ",");

			// 控制目标
			riskStr.append(JecnCommon.contentJson("controlDescription", riskListBean.getControlDescroption()) + ",");

			// 控制编号
			riskStr.append(JecnCommon.contentJson("controlNum", riskListBean.getControlNum()) + ",");

			// 控制活动描述
			riskStr.append(JecnCommon.contentJson("activeShow", riskListBean.getActiveShow()) + ",");

			// 角色名称
			riskStr.append(JecnCommon.contentJson("roleName", riskListBean.getRoleName()) + ",");

			// 活动名称开始
			if (!JecnCommon.isNullOrEmtryTrim(riskListBean.getActiveName())) {
				// 活动名称
				String activeName = "<a href='processActive.action?activeId=" + riskListBean.getActiveId()
						+ "' style='cursor: pointer;' target='_blank' class='table'>" + riskListBean.getActiveName()
						+ "</a>";
				// 先将活动名称拼装成html格式
				if (!JecnCommon.isNullOrEmtryTrim(activeName)) {
					riskStr.append(JecnCommon.contentJson("activeName", activeName) + ",");
				}
			}

			// 流程名称开始
			if (!JecnCommon.isNullOrEmtryTrim(riskListBean.getFlowName())) {
				// 流程名称(由于需要带超链接，所以需要拼装html)
				String flowName = "<a href='process.action?type=process&flowId=" + riskListBean.getFlowId()
						+ "' target='_blank'" + " style='cursor: pointer;' class='table'>" + riskListBean.getFlowName()
						+ "</a>";
				riskStr.append(JecnCommon.contentJson("flowName", flowName) + ",");
			}

			// 制度名称开始
			if (!JecnCommon.isNullOrEmtryTrim(riskListBean.getRuleName())) {
				String ruleName = "<a href='rule.action?type=ruleModeFile&ruleId=" + riskListBean.getRuld()
						+ "' style='cursor: pointer;' target='_blank' class='table'>" + riskListBean.getRuleName()
						+ "</a>";
				if (!JecnCommon.isNullOrEmtryTrim(ruleName)) {
					riskStr.append(JecnCommon.contentJson("ruleName", ruleName.replaceAll("\n", "<br>")) + ",");
				}
			}
			boolean isRule = false;
			if (riskListBean.getRuleName() != null && !"".equals(riskListBean.getRuleName())) {
				isRule = true;
			}
			String resOrg = riskListBean.getResOrgName();
			Long resId = riskListBean.getResOrgId();
			if (isRule) {
				resOrg = riskListBean.getRuleOrgName();
				resId = riskListBean.getRuleOrgId();
			}
			// 责任部门开始
			if (!JecnCommon.isNullOrEmtryTrim(resOrg)) {
				String orgName = "<a href='organization.action?orgId=" + resId
						+ "' target='_blank' style='cursor: pointer;' class='table'>" + resOrg + "</a>";
				if (!JecnCommon.isNullOrEmtryTrim(orgName)) {
					riskStr.append(JecnCommon.contentJson("resOrgName", orgName.replaceAll("\n", "<br>")) + ",");
				}
			}

			// 控制方法 默认：人工
			String controlMethod = JecnUtil.getValue("artificial");
			if (riskListBean.getControlMethod() == 1) {
				// IT
				controlMethod = JecnUtil.getValue("it");
			} else if (riskListBean.getControlMethod() == 2) {
				// 人工/IT
				controlMethod = JecnUtil.getValue("renOrIt");
			}
			riskStr.append(JecnCommon.contentJson("controlMethod", controlMethod) + ",");

			// 控制类型 默认：预防性
			String controlType = JecnUtil.getValue("preventive");
			if (riskListBean.getControlType() == 1) {
				// 发现性
				controlType = JecnUtil.getValue("discover");
			}
			riskStr.append(JecnCommon.contentJson("controlType", controlType) + ",");

			// 是否为关键控制 默认：是
			String keyControl = JecnUtil.getValue("is");
			if (riskListBean.getKeyControl() == 1) {
				// 否
				keyControl = JecnUtil.getValue("no");
			}
			riskStr.append(JecnCommon.contentJson("keyControl", keyControl) + ",");

			// 控制频率 默认：随时(不定期)
			String controlFrequency = JecnUtil.getValue("noScheduled");
			if (riskListBean.getControlFrequency() == 1) {
				// 日
				controlFrequency = JecnUtil.getValue("day");
			} else if (riskListBean.getControlFrequency() == 2) {
				// 周
				controlFrequency = JecnUtil.getValue("weeks");
			} else if (riskListBean.getControlFrequency() == 3) {
				// 月
				controlFrequency = JecnUtil.getValue("month");
			} else if (riskListBean.getControlFrequency() == 4) {
				// 季度
				controlFrequency = JecnUtil.getValue("seasons");
			} else if (riskListBean.getControlFrequency() == 5) {
				// 年度
				controlFrequency = JecnUtil.getValue("year");
			}
			riskStr.append(JecnCommon.contentJson("controlFrequency", controlFrequency));

			riskStr.append("},");
		}
		riskList.setRiskCount(num);
		if (riskStr.toString().endsWith(",")) {
			return riskStr.substring(0, riskStr.length() - 1) + "]";
		} else {
			return riskStr.append("]").toString();
		}

	}

	/**
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() throws Exception {
		// 获得文件
		if (content == null) {
			// 下载文件信息异常
			outResultHtml(this.getText("fileInformation"), "");
			throw new IllegalArgumentException("getInputStream方法中参数为空!");
		}
		return new ByteArrayInputStream(content);
	}

	public long getNode() {
		return node;
	}

	public void setNode(long node) {
		this.node = node;
	}

	public IJecnRiskService getRiskService() {
		return riskService;
	}

	public void setRiskService(IJecnRiskService riskService) {
		this.riskService = riskService;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getVisitType() {
		return visitType;
	}

	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public RiskDetailBean getRiskDetailBean() {
		return riskDetailBean;
	}

	public void setRiskDetailBean(RiskDetailBean riskDetailBean) {
		this.riskDetailBean = riskDetailBean;
	}

	public long getRiskId() {
		return riskId;
	}

	public void setRiskId(long riskId) {
		this.riskId = riskId;
	}

	public String getRiskDetailId() {
		return riskDetailId;
	}

	public void setRiskDetailId(String riskDetailId) {
		this.riskDetailId = riskDetailId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public boolean isShowAll() {
		return showAll;
	}

	public void setShowAll(boolean showAll) {
		this.showAll = showAll;
	}

	public List<RiskRelatedRuleBean> getRiskRelatedRuleBean() {
		return riskRelatedRuleBean;
	}

	public void setRiskRelatedRuleBean(List<RiskRelatedRuleBean> riskRelatedRuleBean) {
		this.riskRelatedRuleBean = riskRelatedRuleBean;
	}

	public List<RiskRelatedProcessBean> getListRiskRelatedProcessBean() {
		return listRiskRelatedProcessBean;
	}

	public void setListRiskRelatedProcessBean(List<RiskRelatedProcessBean> listRiskRelatedProcessBean) {
		this.listRiskRelatedProcessBean = listRiskRelatedProcessBean;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getFileName() {
		return JecnCommon.getDownFileNameByEncoding(getResponse(), fileName, "");
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public boolean isRuleTypeIsShow() {
		return ruleTypeIsShow;
	}

	public void setRuleTypeIsShow(boolean ruleTypeIsShow) {
		this.ruleTypeIsShow = ruleTypeIsShow;
	}

}
