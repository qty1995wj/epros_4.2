package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseRhombus;

/**
 * 
 * IT决策
 * 
 * @author ZHANGXH
 * 
 */
public class ITRhombus extends JecnBaseRhombus {

	public ITRhombus(JecnFigureData jecnActiveData) {
		super(jecnActiveData);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// 定义里边菱形坐标属性
		int X1, X2, X3, X4, Y1, Y2, Y3, Y4;
		X1 = this.getWidth() / 2;
		Y1 = 0 + 7;
		X2 = this.getWidth() - 10;
		Y2 = this.getHeight() / 2;
		X3 = this.getWidth() / 2;
		Y3 = this.getHeight() - 1 - 7;
		X4 = 0 + 10;
		Y4 = this.getHeight() / 2;
		// 定义画笔
		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { X1, X2, X3, X4 }, new int[] { Y1, Y2, Y3,
				Y4 }, 4);
	}
}
