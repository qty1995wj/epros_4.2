package com.jecn.epros.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.process.processFile.BaseProcessFileBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件表格
 *
 * @author ZXH
 */
public class InventoryTable extends BaseProcessFileBean {
    /**
     * 1: 横表， 第一行是标题；2：纵表，第一列是标题
     */
    private List<List<InventoryCell>> contents = new ArrayList<List<InventoryCell>>();

    public List<List<InventoryCell>> getContents() {
        return contents;
    }

    public List<InventoryCell> getTitileContents() {
        if (contents.size() == 0) {
            return null;
        }
        return contents.get(0);
    }

    @JsonIgnore
    public InventoryCell newCell() {
        return new InventoryCell();
    }

    public enum Status {
        pub, notPub, approve
    }

    public enum Align {
        left, right, center
    }

    public void addContentRow(List<InventoryCell> row) {
        contents.add(row);
    }

}
