package epros.designer.gui.popedom.role;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 
 * 添加角色
 * 
 * @author zhangjie 2012-05-04
 * 
 */
public class AddRoleDialog extends RoleDialog {
	private static Logger log = Logger.getLogger(AddRoleDialog.class);
	private JecnTreeNode pNode = null;
	private JecnTree jTree = null;

	public AddRoleDialog(JecnTreeNode pNode, JecnTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name, TreeNodeType.role);
	}

	@Override
	public void saveData() {
		JecnRoleInfo jecnRoleInfo = new JecnRoleInfo();
		jecnRoleInfo.setRoleName(getRoleName());
		jecnRoleInfo.setIsDir(0);
		jecnRoleInfo.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnRoleInfo.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnRoleInfo.setProjectId(JecnConstants.projectId);
		jecnRoleInfo.setRoleRemark(getRoleContent());
		jecnRoleInfo.setPerId(pNode.getJecnTreeBean().getId());
		jecnRoleInfo.setSortId(JecnTreeCommon.getMaxSort(pNode));
		try {
			Long id = ConnectionPool.getJecnRole().addRole(jecnRoleInfo, getRelateIdMap(), JecnConstants.getUserId());
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(jecnRoleInfo.getRoleName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.role);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("AddRoleDialog saveData is error" + JecnProperties.getValue("serverConnException"), e);
		}
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addRole");
	}

}
