package com.jecn.epros.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.security.Authority;

import java.util.List;

public class JecnUser {

    private Long id;

    private String userNumber;// 员工编号
    private String loginName;// 登录名
    @JsonIgnore
    private String password;// 密码

    private String email;// 邮箱

    private String name;// 真实姓名

    private Long isLock;// 0是解锁，1是锁定

    private String macAddress;// 网卡地址 是否登录

    private String emailType;// 邮件内外网,内网inner,外网outer

    private String phoneStr;// 电话

    private Long projectId;// 项目ID

    private Long figureId;

    public JecnUser() {
    }

    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    private List<Authority> authorities; // 权限

    public JecnUser(Long id) {
        this.id = id;
    }

    public JecnUser(Long id, String name) {
        this(id);
        this.name = name;
    }

    // Getter and setter methods
    // ...
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getIsLock() {
        return isLock;
    }

    public void setIsLock(Long isLock) {
        this.isLock = isLock;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public String getPhoneStr() {
        return phoneStr;
    }

    public void setPhoneStr(String phoneStr) {
        this.phoneStr = phoneStr;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

}