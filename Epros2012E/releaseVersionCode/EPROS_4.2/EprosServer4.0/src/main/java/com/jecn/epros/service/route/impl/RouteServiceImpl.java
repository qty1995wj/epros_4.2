package com.jecn.epros.service.route.impl;

import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.mapper.RouteMapper;
import com.jecn.epros.service.route.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RouteServiceImpl implements RouteService {

    @Autowired
    private RouteMapper routeMapper;

    @Override
    public List<TreeNode> findNodeRoute(Map<String, Object> map) {
        int type = (int) map.get("type");

        switch (type) {
            // 流程架构
            case 1:
                return routeMapper.findFlowRoute(map);
            // 制度
            case 2:
                return routeMapper.findRuleRoute(map);
            // 标准
            case 3:
                return routeMapper.findStandardRoute(map);
            // 风险
            case 4:
                return routeMapper.findRiskRoute(map);
            // 文件
            case 5:
                return routeMapper.findFileRoute(map);
            default:
                return null;
        }
    }

}
