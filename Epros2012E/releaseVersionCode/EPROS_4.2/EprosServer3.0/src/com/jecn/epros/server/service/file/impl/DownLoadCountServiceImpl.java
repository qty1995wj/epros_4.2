package com.jecn.epros.server.service.file.impl;

import java.util.Date;

import com.jecn.epros.server.bean.file.FileDownLoadCountBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.dao.file.IDownLoadCountDao;
import com.jecn.epros.server.service.file.IDownLoadCountService;

/**
 * 文件下载次数限制service,继承AbsBaseService
 * 
 * @author CHB
 * 
 * */
public class DownLoadCountServiceImpl extends
		AbsBaseService<FileDownLoadCountBean, Long> implements
		IDownLoadCountService {
    
	IDownLoadCountDao downLoadCountDao;
     
	/***
	 * 
	 * 文件下载统计
	 * @author CHB
	 * @param:
	 *      peopleId:人员ID
	 * @return boolean
	 *      true:下载统计成功
	 *      false:已经达到最大下载次数
	 * */
	@Override
	public void downLoadCount(Long peopleId,int dowLoadMaxCount) {
			//根据人员ID,判断当前登录人员是否在下载统计表中存在，如果存在返回下载统计Bean
			FileDownLoadCountBean downLoadCountBean = downLoadCountDao
					.findPeopleExits(peopleId);
			if (downLoadCountBean == null) {// 如果为空,添加一条新数据，并将下载次数加1
				downLoadCountBean = new FileDownLoadCountBean();
				//下载次数
				downLoadCountBean.setDownLoadCount(1L);
				//人员ID
				downLoadCountBean.setPeopleId(peopleId);
				//修改时间
				downLoadCountBean.setUpdateTime(new Date());
				//创建时间
				downLoadCountBean.setCreateTime(new Date());
				// 执行添加
				downLoadCountDao.save(downLoadCountBean); 
			} else {
				// 如果当前登录用户在下载统计表存在,则判断是否达到最大下载次数
				if (downLoadCountBean.getDownLoadCount() > dowLoadMaxCount) {
					return;
				}
				//如果未达到最大下载次数，次数加1
				downLoadCountBean.setDownLoadCount(downLoadCountBean
						.getDownLoadCount() + 1);
				//更新修改时间
				downLoadCountBean.setUpdateTime(new Date());
				downLoadCountDao.update(downLoadCountBean); // 执行修改
			}
	}
	
	
	/***
	 * 
	 * 判断文件下载是否达到最大下载次数
	 * @author CHB
	 * @param:
	 *      peopleId:人员ID dowLoadMaxCount:允许最大下载次数
	 * @return boolean
	 *      true:没有达到最大下载次数
	 *      false:已经达到最大下载次数
	 * */
	@Override
	public boolean downLoadNum(Long peopleId, int dowLoadMaxCount) {
		if (dowLoadMaxCount == 0) { // 系统设置中下载次数 没有填 或者 为0的时候 可以不限次数下载
			return true;
		}
		//根据人员ID,判断当前登录人员是否在下载统计表中存在，如果存在返回下载统计Bean
		FileDownLoadCountBean downLoadCountBean = downLoadCountDao
				.findPeopleExits(peopleId);
	    if(downLoadCountBean!=null&&downLoadCountBean.getDownLoadCount()>=dowLoadMaxCount){
	    	return false;
	    }
		return true;
	}

	
	
     
	/**
	 * 清空下载统计表数据service
	 * @author CHB
	 * */
	@Override
	public void clearDownLoadCountData()throws Exception {
		// 清空文件统计表中数据
		downLoadCountDao.clearDownLoadCountData();
	}

	public IDownLoadCountDao getDownLoadCountDao() {
		return downLoadCountDao;
	}

	public void setDownLoadCountDao(IDownLoadCountDao downLoadCountDao) {
		this.downLoadCountDao = downLoadCountDao;
	}





	
	
	
	
	
}
