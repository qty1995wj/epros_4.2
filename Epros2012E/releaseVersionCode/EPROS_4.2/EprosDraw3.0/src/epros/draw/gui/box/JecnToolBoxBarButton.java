package epros.draw.gui.box;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 带有Toolbar的按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolBoxBarButton extends JButton {
	/** 装载按钮容器:为了出悬浮按钮上，出现选中状态 */
	protected JToolBar toolBar = null;

	public JecnToolBoxBarButton() {
		initComponents();
	}

	public JToolBar getToolBar() {
		return toolBar;
	}

	private void initComponents() {
		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 图片内容显示方式：靠左
		this.setHorizontalAlignment(SwingConstants.LEFT);
		// 无边框
		this.setBorder(null);
		// 不显示焦点状态
		this.setFocusPainted(false);
		//透明
		this.setOpaque(false);

		toolBar = new JToolBar();
		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());
		toolBar.add(this);
	}
}