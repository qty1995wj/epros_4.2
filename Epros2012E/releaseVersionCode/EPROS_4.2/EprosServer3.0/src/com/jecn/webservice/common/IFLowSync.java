/**
 * 
 */
package com.jecn.webservice.common;

/**
 * @author xiaobo
 * 
 */
public interface IFLowSync {

	/**
	 * 流程推送总方法
	 * 
	 * @param flowId
	 *            : 流程编号 syNctype：公司类型 path 保存路径
	 * @return 0:失败 1：成功 2：不存在流程开始元素（普元专用）
	 * 
	 * */
	public int sysncMethd(Long flowId, int syNctype, String path);

}
