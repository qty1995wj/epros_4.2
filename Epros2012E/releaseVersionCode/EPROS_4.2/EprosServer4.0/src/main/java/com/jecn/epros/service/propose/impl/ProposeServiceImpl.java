package com.jecn.epros.service.propose.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.StringUtil;
import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.email.EmailBasicInfo;
import com.jecn.epros.domain.email.EmailTip;
import com.jecn.epros.domain.propose.*;
import com.jecn.epros.email.BaseEmailBuilder;
import com.jecn.epros.email.EmailBuilderFactory;
import com.jecn.epros.email.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.mapper.ConfigItemMapper;
import com.jecn.epros.mapper.EmailMapper;
import com.jecn.epros.mapper.ProposeMapper;
import com.jecn.epros.mapper.UserMapper;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.security.SecurityValidatorData;
import com.jecn.epros.service.IMixRPCService;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.propose.ProposeService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import com.jecn.epros.sqlprovider.server3.ConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.sqlprovider.server3.Util;
import com.jecn.epros.util.ConfigUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
@Transactional
public class ProposeServiceImpl implements ProposeService {

    @Autowired
    private ProposeMapper proposeMapper;
    @Autowired
    private ConfigItemMapper configItemMapper;
    @Autowired
    private EmailMapper emailMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IMixRPCService mixRPCService;

    /**
     * 根据搜索条件以及合理化建议所在的tab页
     */
    @Override
    public Integer getProposeNum(ProposeSearch proposeParam, Integer proposeState, Boolean isAdmin,
                                 Long loginPeopleId) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("proposeParam", proposeParam);
        paramMap.put("proposeState", proposeState);
        paramMap.put("isAdmin", isAdmin);
        paramMap.put("loginPeopleId", loginPeopleId);
        Long historyId = getMaxHistoryId(paramMap);
        paramMap.put("historyId", historyId);
        Integer num = proposeMapper.getProposeNum(paramMap);
        return num;
    }

    private Long getMaxHistoryId(Map<String, Object> paramMap) {
        ProposeSearch proposeParam = (ProposeSearch) paramMap.get("proposeParam");
        int type = proposeParam.getType();
        // 其它类型没有文控信息
        if (type == 0 || type == 1 || type == 2 || type == 3) {
            return proposeMapper.getMaxHistoryId(paramMap);
        }
        return null;
    }

    @Override
    public List<Propose> fetchProposes(ProposeSearch proposeParam, Boolean isAdmin, Long loginPeopleId) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("proposeParam", proposeParam);
        paramMap.put("isAdmin", isAdmin);
        paramMap.put("loginPeopleId", loginPeopleId);
        Long historyId = getMaxHistoryId(paramMap);
        paramMap.put("historyId", historyId);
        paramMap.put("proposeState", proposeParam.getCurState());
        List<Propose> proposes = proposeMapper.fetchProposes(paramMap);
        IfytekCommon.getIflytekhProposeUserName(proposes);
        paramMap.clear();
        paramMap.put("relatedId", proposeParam.getRelatedId());
        paramMap.put("type", proposeParam.getType());
        ProposeTopic topic = proposeMapper.getProposeTopic(paramMap);
        initProposeButtonConfigs(proposeParam, proposes, isAdmin, loginPeopleId, topic);
        return proposes;
    }

    private void initProposeButtonConfigs(ProposeSearch proposeParam, List<Propose> proposes, Boolean isAdmin,
                                          Long loginPeopleId, ProposeTopic topic) {

        if (proposes == null || proposes.size() == 0) {
            return;
        }
        // 是不是合理化建议处理人
        boolean isHandlePeople = false;
        // 是否有权限可以删除回复的信息
        boolean curPeopelIsCanDelAnswer = false;
        if (topic != null && topic.getHandlePeople() != null && loginPeopleId != null
                && loginPeopleId.equals(topic.getHandlePeople())) {
            isHandlePeople = true;
            curPeopelIsCanDelAnswer = true;
        }

        ProposeButtonShowConfig proposeButtonShowConfig = null;
        for (Propose propose : proposes) {
            // 登录人是不是创建人
            boolean isCreatePeople = false;

            if (propose.getCreatePersonId() != null && loginPeopleId != null
                    && loginPeopleId.equals(propose.getCreatePersonId())) {
                isCreatePeople = true;
            }

            proposeButtonShowConfig = this.initProposeButtonConfig(loginPeopleId, isAdmin, isCreatePeople,
                    isHandlePeople, curPeopelIsCanDelAnswer, propose);
            propose.setButtonShowConfig(proposeButtonShowConfig);

        }
    }

    private ProposeButtonShowConfig initProposeButtonConfig(Long loginUserId, boolean isAdmin, boolean isCreatePeople,
                                                            boolean isHandlePeople, boolean curPeopelIsCanDelAnswer, Propose propose) {

        ProposeButtonShowConfig proposeButtonShowConfig = new ProposeButtonShowConfig();
        boolean acceptIsShow = false;
        /** 采纳按钮是否置灰 */
        boolean acceptIsGray = false;

        /** 取消采纳(拒绝)按钮是否显示 */
        boolean cancelAcceptIsShow = false;
        /** 取消采纳(拒绝)按钮是否置灰 */
        boolean cancelAcceptIsGray = false;

        /** 回复按钮是否显示 */
        boolean replayIsShow = false;
        /** 回复按钮是否置灰 */
        boolean replayIsGray = false;

        /** 已读按钮是否显示 */
        boolean readIsShow = false;
        /** 已读按钮是否置灰 */
        boolean readIsGray = false;

        /** 编辑按钮是否显示 */
        boolean editIsShow = false;
        /** 编辑按钮是否置灰 */
        boolean editIsGray = false;

        /** 删除按钮是否置灰 */
        boolean delIsShow = false;
        /** 删除按钮是否置灰 */
        boolean delIsGray = false;

        if (isAdmin || isHandlePeople || isCreatePeople) {
            // 未读
            switch (propose.getState()) {
                case 0:// 未读
                    if (isHandlePeople) {
                        // 采纳显示，不置灰
                        acceptIsShow = true;
                        acceptIsGray = false;
                        // 取消采纳显示，置灰
                        cancelAcceptIsShow = true;
                        cancelAcceptIsGray = false;
                        // 回复显示，不置灰
                        replayIsShow = true;
                        replayIsGray = false;
                        // 已读显示，不置灰
                        readIsShow = true;
                        readIsGray = false;
                    }

                    // 删除显示，不置灰 只有管理员与建议创建人才可以删除
                    if (isAdmin || isCreatePeople) {
                        delIsShow = true;
                        delIsGray = false;
                    }

                    // 意见提交人才可以编辑
                    if (isCreatePeople) {
                        // 编辑显示，不置灰
                        editIsShow = true;
                        editIsGray = false;

                    } else {
                        // 编辑不显示，置灰
                        editIsShow = false;
                        editIsGray = true;

                    }
                    break;
                // 已读
                case 1:
                    if (isHandlePeople) {
                        // 采纳显示，不置灰
                        acceptIsShow = true;
                        acceptIsGray = false;
                        // 取消采纳显示，置灰
                        cancelAcceptIsShow = true;
                        cancelAcceptIsGray = false;
                        // 回复显示，不置灰
                        replayIsShow = true;
                        replayIsGray = false;
                        // 已读显示，置灰
                        readIsShow = true;
                        readIsGray = true;
                    }
                    // 删除显示，不置灰
                    // 删除显示，不置灰 只有管理员与建议创建人才可以删除
                    if (isAdmin) {
                        delIsShow = true;
                        delIsGray = false;
                    }

                    // 是不是意见提交人
                    if (isCreatePeople) {
                        // 编辑显示，不置灰
                        editIsShow = true;
                        editIsGray = false;

                    } else {
                        // 编辑不显示，置灰
                        editIsShow = false;
                        editIsGray = true;

                    }
                    break;
                // 采纳
                case 2:
                    if (isHandlePeople) {
                        // 采纳显示，置灰
                        acceptIsShow = true;
                        acceptIsGray = true;
                        // 取消采纳显示，不置灰
                        cancelAcceptIsShow = true;
                        cancelAcceptIsGray = false;
                        // 回复显示，不置灰
                        replayIsShow = true;
                        replayIsGray = false;
                        // 已读显示，置灰
                        readIsShow = true;
                        readIsGray = true;
                    }
                    // 删除显示，不置灰
                    // 删除显示，不置灰 只有管理员与建议创建人才可以删除
                    if (isAdmin) {
                        delIsShow = true;
                        delIsGray = false;
                    }

                    // 是不是意见提交人
                    if (isCreatePeople) {
                        // 编辑显示，不置灰
                        editIsShow = true;
                        editIsGray = true;

                    } else {
                        // 编辑不显示，置灰
                        editIsShow = false;
                        editIsGray = true;

                    }
                    break;
                // 拒绝
                case 3:
                    if (isHandlePeople) {
                        // 采纳显示，置灰
                        acceptIsShow = true;
                        acceptIsGray = false;
                        // 取消采纳显示，不置灰
                        cancelAcceptIsShow = true;
                        cancelAcceptIsGray = true;
                        // 回复显示，不置灰
                        replayIsShow = true;
                        replayIsGray = false;
                        // 已读显示，置灰
                        readIsShow = true;
                        readIsGray = true;
                    }
                    // 删除显示，不置灰
                    // 删除显示，不置灰 只有管理员与建议创建人才可以删除
                    if (isAdmin) {
                        delIsShow = true;
                        delIsGray = false;
                    }

                    // 是不是意见提交人
                    if (isCreatePeople) {
                        // 编辑显示，不置灰
                        editIsShow = true;
                        editIsGray = false;

                    } else {
                        // 编辑不显示，置灰
                        editIsShow = false;
                        editIsGray = true;

                    }
                    break;
                default:
                    break;
            }

        }

        proposeButtonShowConfig.setAcceptIsGray(acceptIsGray);
        proposeButtonShowConfig.setAcceptIsShow(acceptIsShow);
        proposeButtonShowConfig.setCancelAcceptIsShow(cancelAcceptIsShow);
        proposeButtonShowConfig.setCancelAcceptIsGray(cancelAcceptIsGray);
        proposeButtonShowConfig.setDelIsGray(delIsGray);
        proposeButtonShowConfig.setDelIsShow(delIsShow);
        proposeButtonShowConfig.setEditIsGray(editIsGray);
        proposeButtonShowConfig.setEditIsShow(editIsShow);
        proposeButtonShowConfig.setReadIsGray(readIsGray);
        proposeButtonShowConfig.setReadIsShow(readIsShow);
        proposeButtonShowConfig.setReplayIsGray(replayIsGray);
        proposeButtonShowConfig.setReplayIsShow(replayIsShow);
        proposeButtonShowConfig.setCurPeopelIsCanDelAnswer(curPeopelIsCanDelAnswer);

        return proposeButtonShowConfig;

    }

    @Override
    public int updateProposeState(String proposeId, Integer proposeState, String strContent, JecnUser handleUser) {
        Propose propose = proposeMapper.getPropose(proposeId);
        if (propose == null) {
            return Propose.HAS_DELETED;
        }

        boolean isPerm = logingPeopleIsProposeHandlePeople(propose, handleUser.getId());
        if (!isPerm) {
            return Propose.HAS_NO_PERM;
        }

        int state = propose.getState();

        // 如果是回复内容不为空，如果接受或者拒绝的情况下不可以修改合理化建议的状态，保存原始状态
        if (strContent != null && !"".equals(strContent) && (state == Propose.ACCEPT || state == Propose.REFUSE)) {
            proposeState = state;
        }

        // 在采纳或者拒绝的情况下是不可以改变状态为已读和未读
        if ((proposeState == Propose.READ || proposeState == Propose.UNREAD) && (state == Propose.ACCEPT)) {
            return Propose.HAS_ACCEPTED;
        }
        if ((proposeState == Propose.READ || proposeState == Propose.UNREAD) && (state == Propose.REFUSE)) {
            return Propose.HAS_REFUSEED;
        }

        Date updateTime = new Date();
        updatePropose(proposeState, strContent, handleUser, propose, updateTime);
        updateTopic(propose, updateTime);
        return Propose.SUCCESS;
    }


    private void updatePropose(Integer proposeState, String strContent, JecnUser handleUser, Propose propose,
                               Date updateTime) {
        Long loginUserId = handleUser.getId();
        propose.setState(proposeState);
        propose.setReplyPersonId(loginUserId);
        propose.setUpdatePersonId(loginUserId);
        propose.setReplyContent(strContent);
        propose.setUpdateTime(updateTime);
        propose.setReplyTime(updateTime);
        proposeMapper.updatePropose(propose);
    }

    private boolean logingPeopleIsProposeHandlePeople(Propose propose, Long peopleId) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("relatedId", propose.getRelationId());
        paramMap.put("type", propose.getType());
        paramMap.put("peopleId", peopleId);
        int num = proposeMapper.logingPeopleIsProposeHandlePeople(paramMap);
        if (num > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Propose savePropose(ProposeCreate param, Long peopleId) {
        // 创建合理化建议
        Propose propose = createProposeAndResult(param, peopleId);
        // 新建或者更新主题
        createOrUpdateTopic(propose);
        return propose;
    }

    private void createOrUpdateTopic(Propose propose) {
        // 通过联合主键查询是否存在主题
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("id", propose.getRelationId());
        paramMap.put("type", propose.getType());
        int num = proposeMapper.topicIsExist(paramMap);
        if (num == 0) {// 不存在
            // 创建主题
            ProposeTopic topic = new ProposeTopic();
            topic.setCreateTime(propose.getCreateTime());
            topic.setUpdateTime(propose.getCreateTime());
            topic.setRelatedId(propose.getRelationId());
            topic.setType(propose.getType());
            topic.setName(propose.getRelationName());
            //查询处理人
            topic.setHandlePeople(findHandlePeople(propose)); //处理人 Id
            proposeMapper.saveProposeTopic(topic);
        } else {
            // 更新主题表时间
            paramMap.clear();
            paramMap.put("updateTime", propose.getCreateTime());
            paramMap.put("relatedId", propose.getRelationId());
            paramMap.put("type", propose.getType());
            proposeMapper.updateProposeTopicUpdateTime(paramMap);
        }
    }

    private Long findHandlePeople(Propose propose) {
        String commissionerValue = ConfigUtils.getValue(ConfigItemPartMapMark.commissioner);//专员
        String personLiableValue = ConfigUtils.getValue(ConfigItemPartMapMark.personLiable);//责任人
        List<Long> handlePeopleId = null;
        if (commissionerValue != null && !"0".equals(commissionerValue)) {//查询 专员
            propose.setQueryProcessingState(0);
            handlePeopleId = proposeMapper.getHandlePeopleSql(propose);
        } else if (personLiableValue != null && !"0".equals(personLiableValue)) {//查询责任人
            propose.setQueryProcessingState(1);
            handlePeopleId = proposeMapper.getHandlePeopleSql(propose);
        }
        if (handlePeopleId != null && handlePeopleId.size() > 0) { //责任人为岗位情况下 随机取第一个人员
            return handlePeopleId.get(0);
        }
        return 0L;
    }


    private Propose createProposeAndResult(ProposeCreate param, Long peopleId) {
        // 合理化建议Bean
        Propose propose = new Propose();
        // 建议表设置主键
        propose.setId(Common.getUUID());
        propose.setRelationId(param.getId());
        /** 0流程，1制度，2流程架构，3文件，4风险，5标准 **/
        propose.setType(param.getType());
        propose.setState(0);
        propose.setProposeContent(param.getContent());
        propose.setCreatePersonId(peopleId);
        Date createDate = new Date();
        propose.setCreateTime(createDate);
        propose.setUpdatePersonId(peopleId);
        propose.setUpdateTime(createDate);
        propose.setShowType(param.getShowType());
        propose.setReplyContent("");
        // 查询版本号
        /** 0流程，1制度，2流程架构，3文件，4风险，5标准 **/
        Map<String, Object> relatedMap = proposeMapper.getRelated(propose);
        if (propose.getType() < 4) {// 有文控信息的
            propose.setHistoryId(relatedMap.get("HISTORY_ID") == null ? null : Long.valueOf(relatedMap.get("HISTORY_ID").toString()));
        }
        propose.setRelationName(relatedMap.get("RELATED_NAME").toString());
        proposeMapper.savePropose(propose);

        if (param.getFile() != null) {
            MultipartFile uploadFile = param.getFile();
            // 合理化建议附件Bean
            ProposeFile file = createProposeFile(peopleId, propose, createDate, uploadFile);
            proposeMapper.saveProposeFile(file);
        }
        return propose;
    }

    private ProposeFile createProposeFile(Long peopleId, Propose propose, Date createDate, MultipartFile uploadFile) {
        ProposeFile file = new ProposeFile();
        file.setId(Common.getUUID());
        file.setProposeId(propose.getId());
        file.setCreatePersonId(peopleId);
        file.setCreateTime(createDate);
        file.setUpdatePersonId(peopleId);
        file.setUpdateTime(createDate);
        file.setFileName(uploadFile.getOriginalFilename());
        try {
            file.setFileStream(uploadFile.getBytes());
        } catch (IOException e) {
            throw new EprosBussinessException("createProposeFile方法异常", e);
        }
        return file;
    }

    @Override
    public int editPropose(ProposeEdit param, Long peopleId) {
        Propose propose = proposeMapper.getPropose(param.getProposeId());
        if (propose.getState() == Propose.HAS_ACCEPTED) {
            return Propose.HAS_ACCEPT_CANNOT_EDIT;
        }
        Date updateTime = new Date();
        updatePropose(param, peopleId, propose, updateTime);
        // 更新主题表时间
        updateTopic(propose, updateTime);
        return Propose.SUCCESS;
    }

    private void updateTopic(Propose propose, Date updateTime) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("updateTime", updateTime);
        paramMap.put("relatedId", propose.getRelationId());
        paramMap.put("type", propose.getType());
        proposeMapper.updateProposeTopicUpdateTime(paramMap);
    }

    private void updatePropose(ProposeEdit param, Long peopleId, Propose propose, Date updateTime) {
        propose.setUpdatePersonId(peopleId);
        propose.setProposeContent(param.getContent());
        propose.setReplyTime(updateTime);
        propose.setShowType(param.getShowType());
        proposeMapper.updatePropose(propose);
        if (param.getDelFile()) {// 页面删除附件
            // 删除附加
            proposeMapper.deleteProposeFileByProposeId(param.getProposeId());
        } else if (param.getFile() != null) {// 页面添加或者更新附件
            // 先查询是否存在该附件
            List<ProposeFile> files = proposeMapper.getProposeFileByProposeId(param.getProposeId());
            if (files.size() == 0) {// 添加
                ProposeFile file = createProposeFile(peopleId, propose, updateTime, param.getFile());
                proposeMapper.saveProposeFile(file);
            } else {// 更新
                ProposeFile file = files.get(0);
                file.setFileName(param.getFile().getOriginalFilename());
                try {
                    file.setFileStream(param.getFile().getBytes());
                } catch (IOException e) {
                    throw new EprosBussinessException("editPropose异常", e);
                }
                file.setUpdatePersonId(peopleId);
                file.setUpdateTime(updateTime);
                proposeMapper.updateProposeFile(file);
            }
        }
    }

    @Override
    public void clearReply(String proposeId) {
        proposeMapper.clearReyly(proposeId);
        // 更新主题表时间
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("id", proposeId);
        Propose propose = proposeMapper.getPropose(proposeId);
        paramMap.clear();
        paramMap.put("updateTime", new Date());
        paramMap.put("relatedId", propose.getRelationId());
        paramMap.put("type", propose.getType());
        proposeMapper.updateProposeTopicUpdateTime(paramMap);
    }

    @Override
    public MyProposeTopic getMyProposesTopic(Long peopleId, Boolean isPaging) {

        // // 我处理的
        // int hasHandleNum = proposeMapper.fetchMyProposeTopicNum(peopleId,0);
        // // 我未处理的
        // int notHandleNum = proposeMapper.fetchMyProposeTopicNum(peopleId,1);
        // // 我提交的已处理的
        // int selfHasHandleNum =
        // proposeMapper.fetchMyProposeTopicNum(peopleId,2);
        // // 我提交的未处理的
        // int selfNotHandleNum =
        // proposeMapper.fetchMyProposeTopicNum(peopleId,3);

        MyProposeTopic myPropose = new MyProposeTopic();

        // myPropose.setHasHandleNum(hasHandleNum);
        // myPropose.setNotHandleNum(notHandleNum);
        // myPropose.setSelfHasHandleNum(selfHasHandleNum);
        // myPropose.setSelfNotHandleNum(selfNotHandleNum);

        if (isPaging) {
            PageHelper.startPage(0, 10, "pt.update_time desc");
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("peopleId", peopleId);
        paramMap.put("type", 0);
        // 我已经处理的（单个的合理化建议有我已经处理过的）
        List<ProposeTopic> hasHandle = proposeMapper.fetchMyTopic(paramMap);
        myPropose.setHasHandle(hasHandle);

        // 我未处理的（单个的合理化建议有我未处理过的）
        paramMap.put("type", 1);
        List<ProposeTopic> notHandle = proposeMapper.fetchMyTopic(paramMap);
        myPropose.setNotHandle(notHandle);

        // 我提交的已处理的
        paramMap.put("type", 2);
        List<ProposeTopic> selfHasHandle = proposeMapper.fetchMyTopic(paramMap);
        myPropose.setSelfHasHandle(selfHasHandle);

        // 我提交的未处理的
        paramMap.put("type", 3);
        List<ProposeTopic> selfNotHandle = proposeMapper.fetchMyTopic(paramMap);
        myPropose.setSelfNotHandle(selfNotHandle);

        return myPropose;
    }

    @Override
    public List<ProposeTopic> searchProposeTopic(TopicSearch topicSearch, boolean isAdmin, Long peopleId,
                                                 Paging paging) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("topicSearch", topicSearch);
        paramMap.put("isAdmin", isAdmin);
        paramMap.put("peopleId", peopleId);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "update_time desc");
        List<ProposeTopic> topic = proposeMapper.searchProposeTopic(paramMap);
        IfytekCommon.getIflytekhProposeTopicUserName(topic);
        return topic;
    }

    /**
     * 根据合理化建议的 type 类型获取查询权限用的枚举类型
     *
     * @param type
     * @return
     */
    private TreeNodeUtils.NodeType getProposeTopicFileType(Integer type) {
        switch (type) {
            case 0:
                return TreeNodeUtils.NodeType.process;
            case 1:
                return TreeNodeUtils.NodeType.process;
            case 2:
                return TreeNodeUtils.NodeType.processMap;
            case 3:
                return TreeNodeUtils.NodeType.file;
        }
        return null;
    }

    @Override
    public ProposeTopic getProposeTopic(Map<String, Object> paramMap) {
        return proposeMapper.getProposeTopic(paramMap);
    }

    @Override
    public List<ProposeTopic> searchProposeTopic(Integer type, Paging paging) {
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "update_time desc");
        return proposeMapper.searchProposeTopicByType(type);
    }

    @Override
    public int searchProposeTopicNum(Integer type) {
        return proposeMapper.searchProposeTopicNumByType(type);
    }

    @Override
    public void distributeHandlePeople(Map<String, Object> paramMap) {

        proposeMapper.updateProposeSetHandlePeople(paramMap);

    }

    @Override
    public int searchProposeTopicNum(TopicSearch topicSearch, boolean isAdmin, Long peopleId) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("topicSearch", topicSearch);
        paramMap.put("isAdmin", isAdmin);
        paramMap.put("peopleId", peopleId);
        return proposeMapper.searchProposeTopicNum(paramMap);
    }

    @Override
    public int deletePropose(String proposeId, Long peopleId, boolean isAdmin) {

        Propose propose = proposeMapper.getPropose(proposeId);
        if (propose == null) {
            return Propose.HAS_DELETED;
        }
        // 只有管理员和提交人可以删除合理化建议，暂时处理人是不可以删除的
        if (!isAdmin) {// 非管理员
            // ProposeTopic proposeTopic =
            // proposeMapper.getProposeTopic(paramMap);
            // Long handlePeople = proposeTopic.getHandlePeople();
            // if (!peopleId.equals(handlePeople)) {// 非处理人

            if (peopleId.equals(propose.getCreatePersonId())) {
                if (propose.getState() != 0) {
                    return Propose.HAS_HANDLE_CAN_NOT_DEL;
                }
            } else {
                return Propose.HAS_NO_PERM;
            }

            // }
        }

        proposeMapper.deleteProposeFileByProposeId(proposeId);
        proposeMapper.deletePropose(proposeId);

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("relatedId", propose.getRelationId());
        paramMap.put("type", propose.getType());
        // 查询该主题下是否还有合理化建议
        int overplusCount = proposeMapper.findProposeTotalByRelated(paramMap);
        // 如果没有删除该主题
        if (overplusCount == 0) {
            proposeMapper.deleteProposeTopic(paramMap);
        }

        return Propose.SUCCESS;
    }

    @Override
    public ProposeTotalChart findProposeTotalChart(Map<String, Object> map) {
        List<ProposeTotalChartData> listProposeTotalDatas = proposeMapper.findProposeTotalChart(map);
        ProposeTotalSearchBean proposeTotalSearchBean = (ProposeTotalSearchBean) map.get("proposeTotalSearchBean");
        // 选择比较的组织、岗位、人员
        List<Long> orgIds = proposeTotalSearchBean.getOrgSetIds();
        ProposeTotalChart proposeTotalChart = new ProposeTotalChart();
        /** 横轴 集合 */
        List<String> horizontals = new ArrayList<String>();

        /** 内容 */
        List<ProposeTotalContentChart> proposeTotalContent = new ArrayList<ProposeTotalContentChart>();
        ProposeTotalContentChart proposeTotalContentChart = null;
        List<Integer> listContents = null;
        int count = 0;
        for (Long orgId : orgIds) {
            proposeTotalContentChart = new ProposeTotalContentChart();
            listContents = new ArrayList<Integer>();
            for (ProposeTotalChartData proposeTotalData : listProposeTotalDatas) {
                if (orgId.equals(proposeTotalData.getId())) {
                    proposeTotalContentChart.setName(proposeTotalData.getName());
                    listContents.add(proposeTotalData.getTotalPropose());
                    if (proposeTotalChart.getMaxVertical() < proposeTotalData.getTotalPropose()) {
                    }
                    if (count == 0) {
                        horizontals.add(proposeTotalData.getDateStr());
                    }
                }

            }
            proposeTotalContentChart.setListContents(listContents);
            proposeTotalContent.add(proposeTotalContentChart);
            count++;
        }
        proposeTotalChart.setHorizontals(horizontals);
        proposeTotalChart.setProposeTotalContent(proposeTotalContent);
        return proposeTotalChart;
    }

    @Override
    public List<ProposeTotalData> findProposeTotalData(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "CREATE_TIME desc");
        return proposeMapper.findProposeTotalData(map);
    }

    @Override
    public int findProposeTotal(Map<String, Object> map) {
        return proposeMapper.findProposeTotal(map);
    }

    @Override
    public List<ProposeFile> getProposeFileByProposeId(Map<String, Object> map) {
        return proposeMapper.getProposeFileByProposeId(map.get("proposeId").toString());
    }

    @Override
    public JecnUser getTopicHandler(Map<String, Object> map) {
        return proposeMapper.getTopicHandler(map);
    }

    /**
     * 采纳或者拒绝发送邮件
     *
     * @param intflag 操作
     * @author hyl
     */
    private void handleSendEmail(Propose propose, int intflag) {

        if (propose != null) {
            JecnUser toUser = userMapper.getUser(propose.getCreatePersonId());
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("relatedId", propose.getRelated());
            paramMap.put("type", propose.getType());
            ProposeTopic proposeTopic = proposeMapper.getProposeTopic(paramMap);
            propose.setRelationName(proposeTopic.getName());
            if (proposeTopic == null || proposeTopic.getHandlePeople() == null) {
                return;
            }
            // 处理人
            JecnUser handleUser = userMapper.getUser(proposeTopic.getHandlePeople());
            if (toUser != null && toUser.getIsLock() == 0 && handleUser != null && handleUser.getIsLock() == 0) {
                BaseEmailBuilder emailBuilder = EmailBuilderFactory
                        .getEmailBuilder(EmailBuilderType.PROPOSE_HANDLER_RESULT);
                List<JecnUser> users = new ArrayList<JecnUser>();
                users.add(toUser);
                EmailTip emailTip = Util.getEmailTip(configItemMapper);
                emailBuilder.setData(users, emailTip, handleUser, propose, intflag);
                List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
                Util.saveEmails(buildEmail, emailMapper);
            }
        }
    }

    /**
     * 提交合理化建议发送邮件给处理人发邮件
     *
     * @param propose
     */
    private void submitProposeSendEmail(Propose propose) {
        String value = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.mailProposeToHandle.toString());
        if ("1".equals(value)) {
            submitSendEmail(propose);
        }
    }

    private void submitSendEmail(Propose propose) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("relatedId", propose.getRelationId());
        paramMap.put("type", propose.getType());
        ProposeTopic proposeTopic = proposeMapper.getProposeTopic(paramMap);
        if (proposeTopic != null && proposeTopic.getHandlePeople() != null) {
            JecnUser handlePeople = userMapper.getUser(proposeTopic.getHandlePeople());
            if (handlePeople != null && handlePeople.getIsLock() == 0) {
                BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.PROPOSE_HANDLER);
                EmailTip emailTip = Util.getEmailTip(configItemMapper);
                emailBuilder.setData(new ArrayList<JecnUser>() {
                    {
                        add(handlePeople);
                    }
                }, emailTip, propose);
                List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
                Util.saveEmails(buildEmail, emailMapper);
            }
        }
    }

}
