package com.jecn.epros.domain.process.kpi;

public class KPISearchBean {
    /**
     * KPI主表纵坐标值
     */
    private String startTime;
    /**
     * KPI主表纵坐标值
     */
    private String endTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
