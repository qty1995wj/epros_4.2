package com.jecn.epros.server.action.web.login.ad.dongh;

/**
 * 单点登录返回Bean
 * 
 * @author fuzhh Mar 20, 2013
 * 
 */
public class LoginInJson implements java.io.Serializable {
	/**
	 * 0为成功 1为用户名或密码错误 2为用户已锁定 3为系统维护中
	 * 4为用户第一次登录，需要将初始化密码重置，使用该值时必须提供Url参数，联合办公将会把用户重定向到Url所指向的三方系统连接地址，
	 * 重置用户密码，三方系统重置完密码后，则要将页面跳回到QueryString中的B2EReturnUrl参数指定的Url，具体参见备注
	 * 255为联合办公为给出的其他错误信息。 如果为255，则联合办公会使用Message字段作为登录失败的信息
	 */
	private String Result;
	private String Message;
	private String Expires;

	public String getResult() {
		return Result;
	}

	public void setResult(String result) {
		Result = result;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getExpires() {
		return Expires;
	}

	public void setExpires(String expires) {
		Expires = expires;
	}
}
