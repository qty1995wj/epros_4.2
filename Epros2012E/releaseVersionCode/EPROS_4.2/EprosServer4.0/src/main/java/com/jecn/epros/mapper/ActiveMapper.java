package com.jecn.epros.mapper;

import com.jecn.epros.domain.file.FileBaseBean;
import com.jecn.epros.domain.process.*;
import com.jecn.epros.sqlprovider.ProcessActiveSqlProvider;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

public interface ActiveMapper {
    /**
     * 活动基本信息
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessActiveSqlProvider.class, method = "findFlowActiveBaseBean")
    @ResultMap("mapper.Active.flowActiveBaseBean")
    public FlowActiveBaseBean findFlowActiveBaseBean(Map<String, Object> map);

    /**
     * 活动相关风险控制点
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessActiveSqlProvider.class, method = "findFlowActiveRelatedRiskControlPoints")
    @ResultMap("mapper.Active.flowActiveRelatedRiskControlPoint")
    public List<FlowActiveRelatedRiskControlPoint> findFlowActiveRelatedRiskControlPoints(Map<String, Object> map);

    /**
     * 活动相关指标
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessActiveSqlProvider.class, method = "findFlowActiveRelatedRefIndicators")
    @ResultMap("mapper.Active.flowActiveRelatedRefIndicators")
    public List<FlowActiveRelatedRefIndicators> findFlowActiveRelatedRefIndicators(Map<String, Object> map);

    /**
     * 活动相关标准
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessActiveSqlProvider.class, method = "findFlowActiveRelatedStandardBeans")
    @ResultMap("mapper.Active.flowActiveRelatedStandardBean")
    public List<FlowActiveRelatedStandardBean> findFlowActiveRelatedStandardBeans(Map<String, Object> map);

    /**
     * 活动相关标准
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessActiveSqlProvider.class, method = "findFlowActiveOnLineBeans")
    @ResultMap("mapper.Active.flowActiveOnLineBean")
    public List<FlowActiveOnLineBean> findFlowActiveOnLineBeans(Map<String, Object> map);

    /**
     * 活动操作模块
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessActiveSqlProvider.class, method = "findActiveOperationFiles")
    @ResultMap("mapper.Active.fileBaseBean")
    public List<FileBaseBean> findActiveOperationFiles(Map<String, Object> map);

    /**
     * 活动输入模块
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessActiveSqlProvider.class, method = "findActiveInFiles")
    @ResultMap("mapper.Active.fileBaseBean")
    public List<FileBaseBean> findActiveInFiles(Map<String, Object> map);

    /**
     * 新版活动输入模块
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessActiveSqlProvider.class, method = "findNewActiveInFiles")
    @ResultMap("mapper.Active.fileNewBaseBean")
    public List<FileBaseBean> findNewActiveInFiles(Map<String, Object> map);

    /**
     * 活动输出模块
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessActiveSqlProvider.class, method = "findActiveOutFiles")
    @ResultMap("mapper.Active.flowActiveOutTemplet")
    public List<FlowActiveOutTemplet> findActiveOutFiles(Map<String, Object> map);
}
