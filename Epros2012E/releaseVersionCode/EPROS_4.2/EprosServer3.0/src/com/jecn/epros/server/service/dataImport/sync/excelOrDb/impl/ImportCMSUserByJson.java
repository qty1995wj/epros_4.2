package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;
import com.jecn.webservice.cms.sync.dataimport.CMSWebServiceClient;

/**
 * 基于Json同步数据
 * 
 * @author ZXH
 * 
 */
public class ImportCMSUserByJson extends AbstractImportUser {
	protected final Log log = LogFactory.getLog(this.getClass());

	private BaseBean baseBean = null;

	public ImportCMSUserByJson(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 获取部门数据集合
	 */
	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		baseBean = baseBean == null ? CMSWebServiceClient.INSTANCE.syncDataViews() : baseBean;
		return baseBean == null ? null : baseBean.getDeptBeanList();
	}

	/**
	 * 获取人员数据集合O
	 */
	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		baseBean = baseBean == null ? CMSWebServiceClient.INSTANCE.syncDataViews() : baseBean;
		return baseBean.getUserPosBeanList();
	}
}
