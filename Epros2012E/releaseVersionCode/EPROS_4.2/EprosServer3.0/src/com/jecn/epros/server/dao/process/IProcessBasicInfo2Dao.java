package com.jecn.epros.server.dao.process;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT2;
import com.jecn.epros.server.common.IBaseDao;

public interface IProcessBasicInfo2Dao extends
		IBaseDao<JecnFlowBasicInfoT2, Long> {

}
