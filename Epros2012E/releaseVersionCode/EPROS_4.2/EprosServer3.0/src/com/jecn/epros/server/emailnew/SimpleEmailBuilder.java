package com.jecn.epros.server.emailnew;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.common.JecnCommon;

public class SimpleEmailBuilder extends FixedContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo() {
		Object[] data = getData();
		String content = data[0].toString();
		String title = data[1].toString();
		File attachment = null;
		String attachmentName = null;
		if (data.length > 2) {
			attachment = (File) data[2];
			attachmentName = data[3].toString();
		}
		return createEmail(content, title, attachment, attachmentName);
	}

	public EmailBasicInfo createEmail(String content, String subject, File attachment, String attachmentName) {

		StringBuffer buf = new StringBuffer();
		buf.append(content);
		buf.append(EmailContentBuilder.strBr);
		buf.append(EmailContentBuilder.strBr);
		buf.append("此邮件为系统自动发送，请不要直接回复，谢谢！");
		buf.append(EmailContentBuilder.strBr);
		buf.append(JecnCommon.getStringbyDateHMS(new Date()));

		EmailBasicInfo basicInfo = new EmailBasicInfo();
		basicInfo.setContent(buf.toString());
		basicInfo.setSubject(subject);

		if (attachment != null && attachment.exists() && StringUtils.isNotBlank(attachmentName)) {
			// 附件
			EmailAttachment emailAttachment = new EmailAttachment();
			emailAttachment.setAttachment(attachment);
			emailAttachment.setAttachmentName(attachmentName);
			List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
			attachments.add(emailAttachment);
			basicInfo.setAttachments(attachments);
		}
		return basicInfo;

	}

}
