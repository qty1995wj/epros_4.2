package epros.designer.gui.process.guide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowKpi;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.util.DrawCommon;

/*******************************************************************************
 * 编辑KPI值
 * 
 * @author 2012-07-12
 * 
 */
public class UpdateKPIValueDialog extends EditKPIValueDialog {
	private static Logger log = Logger.getLogger(UpdateKPIValueDialog.class);
	protected String verName;
	protected String honName;
	protected String kpiName;
	protected Long flowKpiId;
	protected Long flowId = null;
	public boolean isOperation = false;
	protected JecnFlowKpi flowKpi = null;
	protected String honValue = null;

	public UpdateKPIValueDialog(JecnFlowKpi flowKpi, String verName,
			String honName, String kpiName, Long flowKpiId, Long flowId) {
		this.flowId = flowId;
		this.flowKpi = flowKpi;
		this.verName = verName;
		this.honName = honName;
		this.kpiName = kpiName;
		this.flowKpiId = flowKpiId;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String horTime = null;
		if (honName != null || !"".equals(honName)) {
			if (honName.equals(JecnProperties.getValue("dayLab"))) {
				this.setFlagNum(0L);
				this.transverseDateField.setText(sdf.format(flowKpi
						.getKpiHorVlaue()));
			} else if (honName.equals(JecnProperties.getValue("yearLab"))) {
				this.setFlagNum(4L);
				horTime = sdf.format(flowKpi.getKpiHorVlaue());
				if (horTime != null) {
					this.yearField.setText(horTime.substring(0, 4));
				}
			} else if (honName.equals(JecnProperties.getValue("weekLab"))) {
				this.setFlagNum(1L);
				sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				horTime = sdf.format(flowKpi.getKpiHorVlaue());
				if (horTime != null) {
					this.longOneField.setText(horTime.substring(0, 4));
					this.longtwoField.setText(horTime.split(":")[1]);
				}
			} else if (honName.equals(JecnProperties.getValue("monthLab"))) {
				this.setFlagNum(2L);
				horTime = sdf.format(flowKpi.getKpiHorVlaue());
				if (horTime != null) {
					this.longOneField.setText(horTime.substring(0, 4));
					this.longtwoField.setText(horTime.substring(5, 7));
				}
			} else if (honName.equals(JecnProperties.getValue("quarterLab"))) {
				this.setFlagNum(3L);
				horTime = sdf.format(flowKpi.getKpiHorVlaue());
				if (horTime != null) {
					this.longOneField.setText(horTime.substring(0, 4));
					this.longtwoField.setText(horTime.substring(6, 7));
				}
			}
		}
		// 纵向值
		this.longitudinalField.setText(flowKpi.getKpiValue());

		this.setLocationRelativeTo(null);
	}

	@Override
	public void saveData() {
		String reg = "^[0-9]*$";
		String nreg = "[2-9][0-9]{3}";//"^\\d{4}$";
		String strLongitudinal = this.longitudinalField.getText();

		String weekTex = this.longtwoField.getText();// 周 月 季度 天数
		String yearTex = this.yearField.getText();// 年
		String quarYearTex = this.longOneField.getText();//非年类型中的年份
		// SimpleDateFormat sdf = null;
		// sdf = new SimpleDateFormat("yyyy");
		// Date dateHonName = null;
		if (strLongitudinal == null || "".equals(strLongitudinal)) {
			this.okShowInfo.setText(JecnProperties
					.getValue("pleaseInputKPIYCoordinateValue"));
			return;
		} else if (!strLongitudinal.matches(reg)) {
			this.okShowInfo.setText(JecnProperties
					.getValue("KPIOrdinateValueCanOnlyInputDigital"));
			return;
		} else if (DrawCommon.checkNameMaxLength(strLongitudinal)) {
			this.okShowInfo.setText(JecnProperties
					.getValue("KPIOrdinateValues")
					+ JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		if (honName.equals(JecnProperties.getValue("dayLab"))) {
			if (this.transverseDateField.getText() == null
					|| "".equals(this.transverseDateField.getText().toString())) {
				this.okShowInfo.setText(JecnProperties
						.getValue("pleaseInputKPIAbscissaValue！"));
				return;
			}
		} else {
			if (!honName.equals(JecnProperties.getValue("yearLab"))) {

				if (weekTex == null || "".equals(weekTex)) {
					// 周
					if (honName.equals(JecnProperties.getValue("weekLab"))) {
						this.okShowInfo.setText(JecnProperties
								.getValue("KPIAbscissaValueWeeksCantForEmpty"));
					}
					// 月
					else if (honName
							.equals(JecnProperties.getValue("monthLab"))) {
						this.okShowInfo.setText(JecnProperties
								.getValue("KPIAbscissaValueMonthCantForEmpty"));
					}
					// 季度
					else if (honName.equals(JecnProperties
							.getValue("quarterLab"))) {
						this.okShowInfo
								.setText(JecnProperties
										.getValue("KPIAbscissaValueQuarterCantForEmpty"));
					}
					return;
				} else {
					//验证周，月，季度 类型的Field只能输入数字
					if (!weekTex.matches(reg)) {
						// dateHonName = sdf.parse(honValue);
						this.okShowInfo.setText(JecnProperties
								.getValue("KPAbscissaValueCanOnlyInputDigital"));
						return;
					}
					// 周
					if (honName.equals(JecnProperties.getValue("weekLab"))) {
						if (quarYearTex == null || "".equals(quarYearTex)) {
							this.okShowInfo.setText(JecnProperties
									.getValue("KPIAbscissaValueYearsCantForEmpty"));
							return;
						} else if (!quarYearTex.matches(nreg)) {
							// dateHonName = sdf.parse(honValue);
							this.okShowInfo.setText(JecnProperties
									.getValue("KPIAbscissaValueYearsCanOnlyInputFourDigits"));
							return;
						}
						int yearAllWeek = this.getMaxWeekNumOfYear(Integer.valueOf(quarYearTex));
						if (Integer.parseInt(weekTex) > yearAllWeek) {
								this.okShowInfo.setText(JecnProperties.getValue("KPIAbscissaValueCycleIsNotGreaterThan52")+yearAllWeek);
							return;
						}

					}
					// 月
					 if (honName.equals(JecnProperties.getValue("monthLab"))) {
						if (quarYearTex == null || "".equals(quarYearTex)) {
							this.okShowInfo.setText(JecnProperties
									.getValue("KPIAbscissaValueYearsCantForEmpty"));
							return;
						} else if (!quarYearTex.matches(nreg)) {
							// dateHonName = sdf.parse(honValue);
							this.okShowInfo.setText(JecnProperties
									.getValue("KPIAbscissaValueYearsCanOnlyInputFourDigits"));
							return;
						}
						if (Integer.parseInt(weekTex) > 12) {
							this.okShowInfo.setText(JecnProperties
									.getValue("KPIAbscissaValueMonthValueIsNotGreaterThan12"));
							return;
						}

					}
					// 季度
					 if (honName.equals(JecnProperties.getValue("quarterLab"))) {
						if (quarYearTex == null || "".equals(quarYearTex)) {
							this.okShowInfo.setText(JecnProperties
									.getValue("KPIAbscissaValueYearsCantForEmpty"));
							return;
						} else if (!quarYearTex.matches(nreg)) {
							// dateHonName = sdf.parse(honValue);
							this.okShowInfo.setText(JecnProperties
									.getValue("KPIAbscissaValueYearsCanOnlyInputFourDigits"));
							return;
						}
						if (Integer.parseInt(weekTex) > 4) {
							this.okShowInfo.setText(JecnProperties
									.getValue("KPIAbscissaValueQuarterValueIsNotGreaterThan4"));
							return;
						}

					}
				}
			}else if (yearTex == null || "".equals(yearTex)) {
				this.okShowInfo.setText(JecnProperties
						.getValue("KPIAbscissaValueYearsCantForEmpty"));
				return;
			} else if (!yearTex.matches(nreg)) {
				// dateHonName = sdf.parse(honValue);
				this.okShowInfo.setText(JecnProperties
						.getValue("KPIAbscissaValueYearsCanOnlyInputFourDigits"));
				return;
			}

		}
		SimpleDateFormat sdf = null;
		String mothStr = null;
		if (honName != null || !"".equals(honName)) {
			if (honName.equals(JecnProperties.getValue("dayLab"))) {
				sdf = new SimpleDateFormat("yyyy-MM-dd");
				honValue = this.transverseDateField.getText();
			} else if (honName.equals(JecnProperties.getValue("yearLab"))) {
				sdf = new SimpleDateFormat("yyyy");
				honValue = this.yearField.getText();
			} else if (honName.equals(JecnProperties.getValue("weekLab"))) {
				sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				honValue = this.longOneField.getText() + "-01" + "-01 " + "01:"
						+ this.longtwoField.getText() + ":01";
			} else if (honName.equals(JecnProperties.getValue("monthLab"))) {
				sdf = new SimpleDateFormat("yyyy-MM");
				honValue = this.longOneField.getText() + "-"
						+ this.longtwoField.getText();
			} else if (honName.equals(JecnProperties.getValue("quarterLab"))) {
				sdf = new SimpleDateFormat("yyyy-MM");
				mothStr = this.longtwoField.getText();
				if (mothStr.equals("1")) {
					mothStr = "1";
				} else if (mothStr.equals("2")) {
					mothStr = "4";
				} else if (mothStr.equals("3")) {
					mothStr = "7";
				} else if (mothStr.equals("4")) {
					mothStr = "10";
				}
				honValue = this.longOneField.getText() + "-" + mothStr;
			}
		}
		// 将数据放入JecnFlowKpi 中
		flowKpi.setKpiAndId(flowKpiId);
		Date dateHonName = null;
		try {
			dateHonName = sdf.parse(honValue);
		} catch (ParseException e) {
			log.error("UpdateKPIValueDialog saveData is error", e);
		}
		flowKpi.setKpiHorVlaue(dateHonName);
		// 纵坐标
		flowKpi.setKpiValue(this.longitudinalField.getText());
		try {
			List<JecnFlowKpi> flowKPIList = ConnectionPool.getProcessAction()
					.getJecnFlowKpiListByKPIId(flowKpiId);
			if (flowKPIList != null) {
				for (JecnFlowKpi jecnFlowKpi : flowKPIList) {
//					if (flowKpi.getKpiValue().equals(
//							jecnFlowKpi.getKpiValue()) && !flowKpi.getKpiId().equals(jecnFlowKpi.getKpiId())) {
//						// 输入的纵坐标重复
//						this.okShowInfo.setText(JecnProperties
//								.getValue("inputTheOrdinateRepeat"));
//						return;
//					}
					if (flowKpi.getKpiHorVlaue().equals(
							jecnFlowKpi.getKpiHorVlaue()) && !flowKpi.getKpiId().equals(jecnFlowKpi.getKpiId())) {
						// 横坐标重复
						this.okShowInfo.setText(JecnProperties
								.getValue("inputTheAbscissaRepeat"));
						return;
					}  
				}
			}
			ConnectionPool
					.getProcessAction()
					.updateKPIVaule(
							flowKpi,
							JecnConstants.loginBean.getJecnUser().getPeopleId(),
							flowId);
			isOperation = true;
			this.dispose();
		} catch (Exception e) {
			log.error("UpdateKPIValueDialog saveData is error", e);
		}

	}

	@Override
	public String getKPIValueTitle() {
		return JecnProperties.getValue("editFlowKPIVal");
	}

	@Override
	public String getLongField() {
		return verName;
	}

	@Override
	public String getTransverseField() {
		return honName;
	}

	@Override
	public String getKPIName() {
		return kpiName;
	}

}
