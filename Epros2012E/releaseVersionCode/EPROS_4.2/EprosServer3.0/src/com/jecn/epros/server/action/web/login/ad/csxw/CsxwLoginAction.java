package com.jecn.epros.server.action.web.login.ad.csxw;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.action.web.login.ad.lianhedianzi.LdapAuthVerifier;
import com.jecn.epros.server.bean.popedom.JecnUser;

/**
 * 醋酸纤维
 * 
 * @author ZXH
 * 
 */
public class CsxwLoginAction extends JecnAbstractADLoginAction {
	static {
		JecnCsxwAfterItem.start();
	}

	private LdapAuthVerifier ldapAuthVerifier;

	public CsxwLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 
	 * @desc 先验证是否存在用户名和密码，如果不存在走AD域验证
	 * 
	 * @return String
	 */
	public String login() {
		try {
			// 单点登录名称
			String adLoginName = this.loginAction.getLoginName();
			// 单点登录密码
			String adPassWord = this.loginAction.getPassword();

			log.info(" adLoginName = " + adLoginName + " adPassWord = " + adPassWord);
			if (StringUtils.isNotBlank(adLoginName) && StringUtils.isNotBlank(adPassWord)) {
				return loginAction.userLoginGen(true);
			}

			if (StringUtils.isBlank(adLoginName)) {
				adLoginName = this.loginAction.getRequest().getRemoteUser();
			}

			String domain = JecnCsxwAfterItem.DOMAIN; // 邮箱的后缀名
			String user = adLoginName.indexOf(domain) > 0 ? adLoginName : adLoginName + domain;

			// 进行AD域验证
			ldapAuthVerifier = this.getLdapAuthVerifier(JecnCsxwAfterItem.ADMIN_NAME, JecnCsxwAfterItem.ADMIN_PASSWORD,
					getLadpUrl());
			int result = ldapAuthVerifier.verifyAuth();

			log.info("权限验证结果:" + result);
			// 验证成功
			if (result == LdapAuthVerifier.SUCCESS) {
				Object userNum = doDomainAuth(user);
				// 获取输入用户是否在当前域
				if (userNum != null) {
					loginAction.setLoginName(userNum.toString());
					// 不进行密码验证 登录

					String ret = loginAction.userLoginGen(false);
					// 判断用户是否存在域名，不存在更新
					// updateUser(adLoginName);
					return ret;
				} else {
					// 用户名或密码不正确！
					this.loginAction.addFieldError("loginMessage", loginAction.getText("nameOrPasswordError"));
				}
			} else if (result == LdapAuthVerifier.FAILED) {
				// 用户名或密码不正确！
				this.loginAction.addFieldError("loginMessage", loginAction.getText("nameOrPasswordError"));
			} else if (result == LdapAuthVerifier.CONNECTION_ERROR) {
				// 无法验证用户，请联系系统管理员!
				this.loginAction.addFieldError("loginMessage", loginAction.getText("anJianZhiYuanADConnectFailed"));
			}

			String userName = loginAction.getRequest().getParameter("userName");
			if (StringUtils.isNotEmpty(userName)) {
				loginAction.setLoginName(userName);
				return loginAction.userLoginGen(false);
			}
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
		return LoginAction.INPUT;
	}

	/**
	 * 判断用户是在域内
	 * 
	 * @param loginName
	 * @return
	 * @throws Exception
	 */
	private Object doDomainAuth(String loginName) throws Exception {
		return searchInformation("" + JecnCsxwAfterItem.BASE, JecnCsxwAfterItem.FILTER, loginName);
	}

	private Object searchInformation(String base, String filter, String loginName) throws Exception {
		LdapContext ctx = getLdapContext();
		SearchControls sc = new SearchControls();
		sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		// sc.setReturningAttributes(LianHeDianZiAfterItem.DATACOLUMNS.split(","));
		NamingEnumeration<SearchResult> ne = ctx.search(base, filter, sc);

		String domainName = "ZCFC\\";
		while (ne.hasMore()) {
			SearchResult sr = ne.next();
			log.info("sr.getName() = " + sr.getName());
			// System.out.println("sr.getName() = " + sr.getName());
			Attributes attrs = sr.getAttributes();
			NamingEnumeration<? extends Attribute> enumeration = attrs.getAll();
			String fullName = "";
			String userNum = "";
			while (enumeration.hasMoreElements()) {
				Attribute attribute = (Attribute) enumeration.nextElement();
				log.info("attribute = " + attribute);
				if (attribute == null) {
					continue;
				}
				if ("sAMAccountName".equals(attribute.getID())) {
					fullName = attribute.get() == null ? "" : attribute.get().toString();
				}
				if ("physicalDeliveryOfficeName".equals(attribute.getID())) {
					userNum = attribute.get() == null ? "" : attribute.get().toString();
				}

				if (StringUtils.isNotBlank(userNum) && StringUtils.isNotBlank(fullName)) {
					log.info("fullName = " + fullName);
					log.info("userNum = " + userNum + "  loginName = " + loginName);
					if ((domainName + fullName + JecnCsxwAfterItem.DOMAIN).equals(loginName)) {
						return userNum;
					}
				}
			}
		}
		ctx.close();
		return null;
	}

	/**
	 * 管理员账号认证
	 * 
	 * @return
	 * @throws NamingException
	 */
	private LdapContext getLdapContext() throws NamingException {
		return ldapAuthVerifier.verifyAndGetContext();
	}

	private LdapAuthVerifier getLdapAuthVerifier(String username, String password, String ldapServiceUrl) {
		return LdapAuthVerifier.newInstance(username, password, ldapServiceUrl);
	}

	private String getLadpUrl() {
		return "ldap://" + JecnCsxwAfterItem.DOMAIN_IP + ":" + "389";
	}

	/**
	 * 更新人员域明
	 * 
	 * @param domainName
	 * @throws Exception
	 */
	private void updateUser(String domainName) throws Exception {
		if (StringUtils.isBlank(domainName)) {
			return;
		}
		JecnUser jecnUser = loginAction.getJecnUser();
		if (StringUtils.isNotBlank(jecnUser.getDomainName())) {
			return;
		}
		jecnUser.setDomainName(domainName);
		loginAction.getPersonService().update(jecnUser);
	}
}
