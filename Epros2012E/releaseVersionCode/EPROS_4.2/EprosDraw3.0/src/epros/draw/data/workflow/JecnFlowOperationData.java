package epros.draw.data.workflow;

/**
 * 流程操作说明数据对象
 * 
 * @author ZHANGXH
 * @date： 日期：Aug 28, 2012 时间：3:47:10 PM
 */
public class JecnFlowOperationData {
	/** 流程客户 */
	private String flowCustom;
	/** 目的 */
	private String flowPurpose;
	/** 适用范围 */
	private String applicability;
	/** 术语定义 */
	private String noutGlossary;
	/** 流程输入 */
	private String flowInput;
	/** 流程输出 */
	private String flowOutput;
	/** 不适用范围 */
	private String noApplicability;
	/** 补充说明 */
	private String flowSupplement;
	/** 概况信息 */
	private String flowSummarize;
	/** 流程记录*/
	private String flowFileStr;
	/** 操作规范*/
	private String practicesStr;
	/** 相关流程*/
	private String relatedFlowStr;
	/** 相关制度*/
	private String relatedRuleStr;
	/** 流程评测及关键指标*/
	private String keyEvaluationIndiStr;

	public String getFlowCustom() {
		return flowCustom;
	}

	public void setFlowCustom(String flowCustom) {
		this.flowCustom = flowCustom;
	}

	public String getFlowPurpose() {
		return flowPurpose;
	}

	public void setFlowPurpose(String flowPurpose) {
		this.flowPurpose = flowPurpose;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String getNoutGlossary() {
		return noutGlossary;
	}

	public void setNoutGlossary(String noutGlossary) {
		this.noutGlossary = noutGlossary;
	}

	public String getFlowInput() {
		return flowInput;
	}

	public void setFlowInput(String flowInput) {
		this.flowInput = flowInput;
	}

	public String getFlowOutput() {
		return flowOutput;
	}

	public void setFlowOutput(String flowOutput) {
		this.flowOutput = flowOutput;
	}

	public String getNoApplicability() {
		return noApplicability;
	}

	public void setNoApplicability(String noApplicability) {
		this.noApplicability = noApplicability;
	}

	public String getFlowSupplement() {
		return flowSupplement;
	}

	public void setFlowSupplement(String flowSupplement) {
		this.flowSupplement = flowSupplement;
	}

	public String getFlowSummarize() {
		return flowSummarize;
	}

	public void setFlowSummarize(String flowSummarize) {
		this.flowSummarize = flowSummarize;
	}

	public String getFlowFileStr() {
		return flowFileStr;
	}

	public void setFlowFileStr(String flowFileStr) {
		this.flowFileStr = flowFileStr;
	}

	public String getPracticesStr() {
		return practicesStr;
	}

	public void setPracticesStr(String practicesStr) {
		this.practicesStr = practicesStr;
	}

	public String getRelatedFlowStr() {
		return relatedFlowStr;
	}

	public void setRelatedFlowStr(String relatedFlowStr) {
		this.relatedFlowStr = relatedFlowStr;
	}

	public String getRelatedRuleStr() {
		return relatedRuleStr;
	}

	public void setRelatedRuleStr(String relatedRuleStr) {
		this.relatedRuleStr = relatedRuleStr;
	}

	public String getKeyEvaluationIndiStr() {
		return keyEvaluationIndiStr;
	}

	public void setKeyEvaluationIndiStr(String keyEvaluationIndiStr) {
		this.keyEvaluationIndiStr = keyEvaluationIndiStr;
	}
	
	
}
