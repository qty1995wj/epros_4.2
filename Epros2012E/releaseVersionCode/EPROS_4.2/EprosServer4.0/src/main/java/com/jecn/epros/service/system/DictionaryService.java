package com.jecn.epros.service.system;

import com.jecn.epros.domain.system.Dictionary;
import com.jecn.epros.domain.system.DictionaryManage;
import com.jecn.epros.util.DictionaryEnum;

import java.util.List;
import java.util.Map;

public interface DictionaryService {
    Map<DictionaryEnum, List<Dictionary>> fetchAllDictionary();

    List<String>  getDictionaryItems();

    List<Dictionary> getDictionaryAll();

    int saveDictionary(DictionaryManage dm);

    List<Dictionary> getDictionaryByName(DictionaryEnum name);
}
