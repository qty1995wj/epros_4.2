package com.jecn.epros.server.download.wordxml;

import wordxml.element.dom.page.Sect;

import com.jecn.epros.server.bean.download.RuleDownloadBean;

/**
 * 操作说明数据项Bean
 * 
 */
public class RuleItem {

	/** 项标题 */
	private String _itemTitle;
	/** 项所属节 */
	private Sect _belongTo;
	/** 数据Bean */
	private RuleDownloadBean _dataBean;

	public RuleItem() {
	}

	public RuleItem(String itemTitle, Sect belongTo, RuleDownloadBean dataBean) {
		_itemTitle = itemTitle;
		_belongTo = belongTo;
		_dataBean = dataBean;
	}

	public RuleItem(Sect belongTo) {
		_belongTo = belongTo;
	}

	public String getItemTitle() {
		return _itemTitle;
	}

	public void setItemTitle(String itemTitle) {
		this._itemTitle = itemTitle;
	}

	public Sect getBelongTo() {
		return _belongTo;
	}

	public void setBelongTo(Sect belongTo) {
		this._belongTo = belongTo;
	}

	public void setDataBean(RuleDownloadBean dataBean) {
		this._dataBean = dataBean;
	}

	public RuleDownloadBean getDataBean() {
		return _dataBean;
	}

}
