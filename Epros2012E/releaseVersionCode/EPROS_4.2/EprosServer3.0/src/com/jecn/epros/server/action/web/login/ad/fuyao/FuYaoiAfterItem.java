package com.jecn.epros.server.action.web.login.ad.fuyao;

import java.util.ResourceBundle;

/**
 * 烽火通信
 * 
 */
public class FuYaoiAfterItem {

	private static ResourceBundle fuyaoConfig;

	static {
		fuyaoConfig = ResourceBundle.getBundle("cfgFile/fuyao/fuyaoServerConfig");
	}

	public static String getValue(String key) {
		return fuyaoConfig.getString(key);
	}
}
