package com.jecn.epros.controller;

import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.process.SearchBean;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.process.processFile.OperationFile;
import com.jecn.epros.domain.rule.*;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.rule.RuleService;
import com.jecn.epros.sqlprovider.server3.Common;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "/rule", description = "制度体系", position = 3)
public class RuleController {

    @Autowired
    private RuleService ruleService;

    @ApiOperation(value = "制度-搜索")
    @RequestMapping(value = "/rules", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RuleViewBean> search(@ModelAttribute SearchBean searchBean, @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("searchBean", searchBean);
        map.put("paging", paging);
        RuleViewBean ruleViewBean = new RuleViewBean();
        int total = ruleService.searchRuleTotal(map);
        List<SearchRuleResultBean> listSearchRuleResultBean = ruleService.searchRule(map);
        ruleViewBean.setTotal(total);
        ruleViewBean.setData(listSearchRuleResultBean);
        return ResponseEntity.ok(ruleViewBean);
    }

    @PreAuthorize("hasAuthority('ruleBaseInfo')")
    @ApiOperation(value = "制度-概况信息")
    @RequestMapping(value = "/rules/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RuleAttribute>> findBaseInfo(@PathVariable Long id, @RequestParam int isPublish, @RequestParam Long historyId, @RequestParam boolean isApp) {
        Map<String, Object> map = getRuleMap(id, isPublish, historyId, isApp);
        return ResponseEntity.ok(ruleService.findBaseInfoBean(map));
    }

    @ApiOperation(value = "制度-树节点展开")
    @RequestMapping(value = "/rules/{id}/children", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNode>> fetchChildren(
            @ApiParam(value = "父节点Id", required = true) @PathVariable Long id) {
        Map<String, Object> map = new HashMap<>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        // 父节点ID
        map.put("pId", id);
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        return ResponseEntity.ok(ruleService.findChildRules(map));
    }

    @PreAuthorize("hasAuthority('ruleRelatedFiles')")
    @ApiOperation(value = "制度-相关文件")
    @RequestMapping(value = "/rules/{id}/files", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RuleRelatedFileBean> findRelatedFiles(@PathVariable Long id, @RequestParam int isPublish, @RequestParam boolean isApp) {
        Map<String, Object> map = getRuleMap(id, isPublish, null, isApp);
        return ResponseEntity.ok(ruleService.findReleateFiles(map));
    }

    @PreAuthorize("hasAuthority('ruleDocument')")
    @ApiOperation(value = "制度-操作说明")
    @RequestMapping(value = "/rules/{id}/document", method = RequestMethod.GET)
    public ResponseEntity<OperationFile> findDocument(@PathVariable Long id, @RequestParam int isPublish, @RequestParam Long historyId, @RequestParam boolean isApp) {
        Map<String, Object> map = getRuleMap(id, isPublish, historyId, isApp);
        return ResponseEntity.ok(ruleService.findRuleOperationFile(map));
    }

    private Map<String, Object> getRuleMap(Long id, int isPublish, Long historyId, boolean isApp) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("isPub", isPublish == 1 ? "" : Common.PUB_T_STR);//
        map.put("isPubs", isPublish);//是否发布
        map.put("historyId", historyId);
        map.put("isApp", isApp);
        return map;
    }
}
