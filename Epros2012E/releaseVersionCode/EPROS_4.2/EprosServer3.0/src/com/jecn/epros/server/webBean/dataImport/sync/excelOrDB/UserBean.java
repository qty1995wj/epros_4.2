package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;

public class UserBean extends AbstractBaseBean {

	/** 人员编号 USER_NUM */
	private String userNum = null;
	/** 真实姓名 TRUE_NAME */
	private String trueName = null;
	/** 任职岗位编号 */
	private String posNum = null;
	/** 任职岗位名称 */
	private String posName = null;
	/** 岗位所属部门编号 */
	private String deptNum = null;
	/** 邮箱 EMAIL */
	private String email = null;
	/** 联系电话 PHONE */
	private String phone = null;
	/** 内外网邮件标识 EMAIL_TYPE */
	private Integer emailType = null;
	/** 存放人员岗位组合数据,即人员+关联岗位的数据 */
	private UserBean userPosBean = null;

	/** 域名称 */
	private String domainName;

	public String getUserNum() {
		return userNum;
	}

	public void setUserNum(String userNum) {
		this.userNum = userNum;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getEmailType() {
		return emailType;
	}

	public void setEmailType(Integer emailType) {
		this.emailType = emailType;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public UserBean getUserPosBean() {
		return userPosBean;
	}

	public void setUserPosBean(UserBean userPosBean) {
		this.userPosBean = userPosBean;
	}

	/**
	 * 
	 * 邮箱类型是否是0、1
	 * 
	 * @return
	 */
	public boolean checkEmailType() {
		return (emailType != null && (emailType == SyncConstant.EMAIL_TYPE_INNER_INT || emailType == SyncConstant.EMAIL_TYPE_OUTER_INT)) ? true
				: false;
	}

	/**
	 * 
	 * 拼装字符串 把源字符串拼装到目标字符串上
	 * 
	 * @param srcInfo
	 *            目标字符串
	 * @param info
	 *            源字符串
	 * @return String 拼装后的字符串
	 */
	@Override
	public void addError(String info) {
		super.addError(info);
		if (userPosBean != null) {
			userPosBean.addError(info);
		}
	}

	@Override
	public String toInfo() {
		StringBuilder strBuf = new StringBuilder();
		// 人员编号
		strBuf.append("人员编号:");
		strBuf.append(this.getUserNum());
		strBuf.append(SyncConstant.EMTRY_SPACE);

		// 真实姓名
		strBuf.append("真实姓名:");
		strBuf.append(this.getTrueName());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 任职岗位编号
		strBuf.append("任职岗位编号:");
		strBuf.append(this.getPosNum());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 邮箱
		strBuf.append("邮箱:");
		strBuf.append(this.getEmail());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 联系电话
		strBuf.append("联系电话:");
		strBuf.append(this.getPhone());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 内外网邮件标识
		strBuf.append("内外网邮件标识:");
		strBuf.append(this.getEmailType());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 错误信息
		strBuf.append("错误信息:");
		strBuf.append(this.getError());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		strBuf.append("\r\n");
		return strBuf.toString();
	}

	/**
	 * 
	 * 人员编码、岗位编码都相同返回true，否则返回false
	 * 
	 * @param userBean
	 *            UserBean
	 * @return
	 */
	public boolean repeatAttributes(UserBean userBean) {
		// 人员编号（登录名称）、岗位编号
		if (SyncTool.nullToEmtry(this.getUserNum()).equals(SyncTool.nullToEmtry(userBean.getUserNum()))
				&& SyncTool.nullToEmtry(this.getPosNum()).equals(SyncTool.nullToEmtry(userBean.getPosNum()))) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 通过String[]给属性赋值
	 * 
	 * @param strArrray
	 */
	public void setAttributes(String[] strArrray) {
		if (strArrray == null || strArrray.length != 8) {
			return;
		}
		// 人员编号
		this.setUserNum(strArrray[0]);
		// 真实姓名
		this.setTrueName(strArrray[1]);
		// 任职岗位编号
		this.setPosNum(strArrray[2]);
		// 任职岗位名称
		this.setPosName(strArrray[3]);
		// 岗位所属部门编号
		this.setDeptNum(strArrray[4]);
		// 邮箱
		this.setEmail(strArrray[5]);
		// 联系电话;
		this.setPhone(strArrray[6]);
		// 内外网邮件标识
		if (this.checkEmailType()) {
			this.setEmailType(Integer.valueOf(strArrray[7]));
		}
		if (StringUtils.isBlank(this.getPosNum()) || StringUtils.isBlank(this.getPosNum())
				|| StringUtils.isBlank(this.getDeptNum())) {
			// 任职岗位编号
			this.setPosNum(null);
			// 任职岗位名称
			this.setPosName(null);
			// 岗位所属部门编号
			this.setDeptNum(null);
		}
	}

	/**
	 * 
	 * 判断所有数据都为空
	 * 
	 * @return 都为空返回true、其他情况返回false
	 */
	public boolean isAttributesNull() {
		if (SyncTool.isNullOrEmtryTrim(this.getUserNum())// 人员编号
				&& SyncTool.isNullOrEmtryTrim(this.getTrueName())// 真实姓名
				&& SyncTool.isNullOrEmtryTrim(this.getPosNum()) // 任职岗位编号
				&& SyncTool.isNullOrEmtryTrim(this.getPosName())// 任职岗位名称
				&& SyncTool.isNullOrEmtryTrim(this.getDeptNum())// 岗位所属部门编号
				&& SyncTool.isNullOrEmtryTrim(this.getEmail())// 邮箱
				&& SyncTool.isNullOrEmtryTrim(this.getPhone())// 联系电话
				&& SyncTool.isNullObj(this.getEmailType())// 内外网邮件标识
				&& SyncTool.isNullObj(this.getDomainName())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * 
	 * 判断邮箱内外网标识是否相同，new Integer（null）情况不能判断
	 * 
	 * @param userBean
	 * @return
	 */
	public boolean isEailTypeSame(UserBean userBean) {
		if (userBean == null) {
			return false;
		}
		if (this.getEmailType() == null && userBean.getEmailType() == null) {// 都为null,返回true
			return true;
		} else if (this.getEmailType() == null && userBean.getEmailType() != null) {// 一个为null，一个不为null或，返回false
			return false;
		} else if (this.getEmailType() != null && userBean.getEmailType() == null) {// 一个为null，一个不为null或，返回false
			return false;
		} else if (this.getEmailType().equals(userBean.getEmailType())) {// 两个都不为null，且相等返回true
			return true;
		} else {
			return false;
		}

	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

}
