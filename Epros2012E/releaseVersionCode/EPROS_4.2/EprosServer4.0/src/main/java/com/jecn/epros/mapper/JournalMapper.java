package com.jecn.epros.mapper;

import com.jecn.epros.domain.journal.*;
import com.jecn.epros.sqlprovider.JournalSqlProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface JournalMapper {
    /**
     * 日志详情
     *
     * @param map
     * @return
     */
    @SelectProvider(type = JournalSqlProvider.class, method = "getLogDetailResult")
    @ResultMap("mapper.Journal.searchLogDetailResultBean")
    public List<UserVisitsResult> getLogDetailResult(Map<String, Object> map);

    /**
     * 日志统计
     *
     * @param map
     * @return
     */
    @SelectProvider(type = JournalSqlProvider.class, method = "getLogStatisticsResult")
    @ResultMap("mapper.Journal.searchLogStatisticsResultBean")
    public List<ResourceVisitsResult> getLogStatisticsResult(Map<String, Object> map);

    /**
     * 日志详情 总数
     */
    @SelectProvider(type = JournalSqlProvider.class, method = "getLogDetailResultTotal")
    @ResultType(Integer.class)
    public int getLogDetailResultTotal(Map<String, Object> map);

    /**
     * 日志统计 总数
     */
    @SelectProvider(type = JournalSqlProvider.class, method = "getLogStatisticsResultTotal")
    @ResultType(Integer.class)
    public int getLogStatisticsResultTotal(Map<String, Object> map);


    @Insert("insert into JECN_OPR_LOG(ID,RELATED_ID,RELATED_TYPE,HISTORY_ID,OPERATOR_TIME,OPERATOR_PEOPLE,OPERATOR_TYPE) " +
            "values (#{id},#{relatedId},#{relatedType},#{historyId,jdbcType=DECIMAL},#{operatorTime},#{operatorPeople},#{operatorType})")
    void addWebLog(Map<String, Object> map);

    /**
     * 提示查看的流程（未查看过的，目前是）
     *
     * @param map
     * @return
     */
    @SelectProvider(type = JournalSqlProvider.class, method = "searchNotice")
    @ResultMap("mapper.Journal.searchResultBean")
    public List<SearchResultWebLogBean> searchProcessNotice(Map<String, Object> map);

    /**
     * @param map
     * @return
     */
    @SelectProvider(type = JournalSqlProvider.class, method = "getAuthPubSqlByTypeWebServer")
    @ResultMap("mapper.Journal.RuleAndFlowWebBean")
    public List<RuleAndFlowWebBean> findFlowAndRuleByCode(Map<String, Object> map);
    /**
     * @param map
     * @return
     */
    @SelectProvider(type = JournalSqlProvider.class, method = "getFlowAndRuleAndFlowMapByCodeWebServer")
    @ResultMap("mapper.Journal.RuleAndFlowAndFlowMapWebBean")
    public List<RuleAndFlowAndFlowMapWebBean> findFlowAndRuleAndFlowMapByCode(Map<String, Object> map);


    @SelectProvider(type = JournalSqlProvider.class, method = "searchNoticeTotal")
    @ResultType(Integer.class)
    public int searchProcessNoticeTotal(Map<String, Object> paramMap);

    @SelectProvider(type = JournalSqlProvider.class, method = "getHistoryIdByLogType")
    @ResultType(Long.class)
    public Long getHistoryIdByLogType(Map<String, Object> map);

    @SelectProvider(type = JournalSqlProvider.class, method = "getLogOperateResult")
    @ResultMap("mapper.Journal.UserOperateResult")
    List<UserOperateResult> getLogOperateResult(Map<String, Object> map);

    @SelectProvider(type = JournalSqlProvider.class, method = "getLogOperateResultTotal")
    @ResultType(Integer.class)
    int getLogOperateResultTotal(Map<String, Object> map);

    @SelectProvider(type = JournalSqlProvider.class, method = "operateTimeBigEqCount")
    @ResultType(Integer.class)
    int operateTimeBigEqCount(Map<String, Object> paramMap);
}
