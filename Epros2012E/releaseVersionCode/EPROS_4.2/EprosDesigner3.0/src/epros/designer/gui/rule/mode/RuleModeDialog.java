package epros.designer.gui.rule.mode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleModeTitleBean;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

public abstract class RuleModeDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(RuleModeDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;
	/** 包括，搜索面板、结果面板、标题按钮面板 */
	private JPanel ruleModePanel = null;
	/** 模板名称面板 */
	private JPanel namePanel = null;
	/** 标题列表面板 */
	private JScrollPane titlePanel = null;
	/** 标题按钮面板面板 */
	private JPanel titleButtonPanel = null;
	/** 右标题按钮面板面板 */
	private JPanel rightTitleButtonPanel = null;
	/** 底部按钮面板 */
	private JPanel bomButtonPanel = null;

	/** 上移按钮 */
	private JButton upButton = null;
	/** 下移按钮 */
	private JButton downButton = null;
	/** 加入标题按钮 */
	private JButton addTitleButton = null;

	/** 编辑标题按钮 */
	private JButton editTitleButton = null;

	/** 删除标题按钮 */
	private JButton deleteButton = null;

	/** 预览 按钮 */
	private JButton previewButton = null;
	/** 确定按钮 */
	private JButton okButton = null;
	/** 取消按钮 */
	private JButton cancelButton = null;

	private JLabel nameLabel = null;

	private JTextField nameField = null;

	/** 名称验证提示 */
	private JLabel promptLab = null;
	private JLabel verfyRequiredLab = new JLabel(JecnProperties.getValue("required"));

	public JecnTable titleTable = null;

	public boolean isOperation = false;

	/** 分割线 */
	private JTextField lineBottomLab = new JTextField();

	/** 分割线面板 */
	private JPanel linePanel = null;

	// 获取name输入框
	public JTextField getNameField() {
		return nameField;
	}

	public RuleModeDialog() {
		initCompotents();
		initLayout();
		initButtonAction();
	}

	private void initCompotents() {

		this.setSize(500, 570);
		this.setResizable(false);
		this.setModal(true);
		this.setLocationRelativeTo(null);

		mainPanel = new JPanel();
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		ruleModePanel = new JPanel();
		ruleModePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// ruleModePanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		Dimension dimension = new Dimension(480, 480);
		ruleModePanel.setPreferredSize(dimension);
		ruleModePanel.setMinimumSize(dimension);

		// 分割线面板
		linePanel = new JPanel();
		dimension = new Dimension(480, 10);
		linePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		linePanel.setPreferredSize(dimension);
		linePanel.setMaximumSize(dimension);
		linePanel.setMinimumSize(dimension);

		// 底部面板
		bomButtonPanel = new JPanel();
		dimension = new Dimension(470, 35);
		bomButtonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		bomButtonPanel.setPreferredSize(dimension);
		bomButtonPanel.setMinimumSize(dimension);

		// 模板名称面板
		this.namePanel = new JPanel();
		dimension = new Dimension(475, 45);
		namePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		namePanel.setPreferredSize(dimension);
		namePanel.setMinimumSize(dimension);
		this.nameLabel = new JLabel(JecnProperties.getValue("modeNameC"));
		this.nameField = new JTextField(60);
		this.promptLab = new JLabel();
		// 标题列表面板
		this.titleTable = new titleTable();
		this.titlePanel = new JScrollPane();
		titlePanel.setViewportView(titleTable);
		titlePanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		dimension = new Dimension(475, 380);
		titlePanel.setPreferredSize(dimension);
		titlePanel.setMinimumSize(dimension);

		// 标题按钮面板
		this.titleButtonPanel = new JPanel();
		// titleButtonPanel.setBorder(BorderFactory.createLineBorder(Color.RED));
		dimension = new Dimension(475, 35);
		titleButtonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		titleButtonPanel.setPreferredSize(dimension);
		titleButtonPanel.setMinimumSize(dimension);
		// leftTitleButtonPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		this.rightTitleButtonPanel = new JPanel();
		rightTitleButtonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.upButton = new JButton(JecnProperties.getValue("upBtn"));
		this.downButton = new JButton(JecnProperties.getValue("downBtn"));
		this.addTitleButton = new JButton(JecnProperties.getValue("addBtn"));
		this.editTitleButton = new JButton(JecnProperties.getValue("editBtn"));
		this.deleteButton = new JButton(JecnProperties.getValue("deleteBtn"));

		// 底部按钮按钮
		this.previewButton = new JButton(JecnProperties.getValue("previewBtn"));
		this.okButton = new JButton(JecnProperties.getValue("okBtn"));
		this.cancelButton = new JButton(JecnProperties.getValue("cancelBtn"));

		// 分割线
		dimension = new Dimension(475, 1);
		lineBottomLab.setPreferredSize(dimension);
		lineBottomLab.setMaximumSize(dimension);
		lineBottomLab.setMinimumSize(dimension);
		promptLab.setForeground(Color.red);
		verfyRequiredLab.setForeground(Color.red);
	}

	public void initLayout() {
		mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		mainPanel.add(ruleModePanel);
		mainPanel.add(linePanel);
		mainPanel.add(bomButtonPanel);

		// 包括，搜索面板、结果面板、标题按钮面板
		ruleModePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		ruleModePanel.add(this.namePanel);
		ruleModePanel.add(this.titlePanel);
		ruleModePanel.add(this.titleButtonPanel);
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0,
				0);

		namePanel.setLayout(new GridBagLayout());
		namePanel.add(nameLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);

		namePanel.add(nameField, c);

		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0,
				0);

		namePanel.add(verfyRequiredLab, c);

		titleButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		titleButtonPanel.add(this.rightTitleButtonPanel);

		rightTitleButtonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		rightTitleButtonPanel.add(this.upButton);
		rightTitleButtonPanel.add(this.downButton);

		rightTitleButtonPanel.add(this.previewButton);
		rightTitleButtonPanel.add(this.addTitleButton);
		rightTitleButtonPanel.add(this.editTitleButton);
		rightTitleButtonPanel.add(this.deleteButton);

		// lineBottomLab
		// 分割线面板
		linePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		linePanel.add(lineBottomLab);

		// 底部按钮面板
		bomButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		bomButtonPanel.add(promptLab);
		bomButtonPanel.add(this.okButton);
		bomButtonPanel.add(this.cancelButton);
		this.getContentPane().add(mainPanel);
	}

	public void initButtonAction() {
		// 上移
		upButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				upButtonAction();
			}
		});

		// 下移
		downButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downButtonAction();
			}
		});

		addTitleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addTitleButtonAction();
			}
		});

		editTitleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editTitleButtonAction();
			}
		});

		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				delteTitleButtonAction();
			}
		});

		previewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				previewButtonAction();
			}
		});
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
	}

	/**
	 * 
	 * @author yxw 2012-5-15
	 * @description:上移
	 */
	public void upButtonAction() {
		this.titleTable.moveUpRows();

		// 重设序号
		JecnTableModel model = (JecnTableModel) this.titleTable.getModel();
		int rows = model.getRowCount();
		for (int i = 0; i < rows; i++) {
			model.setValueAt(i + 1 + "", i, 0);
		}
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:下移
	 */
	public void downButtonAction() {
		this.titleTable.moveDownRows();
		// 重设序号
		JecnTableModel model = (JecnTableModel) this.titleTable.getModel();
		int rows = model.getRowCount();
		for (int i = 0; i < rows; i++) {
			model.setValueAt(i + 1 + "", i, 0);
		}
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:加入标题
	 */
	public void addTitleButtonAction() {
		AddRuleModelTitleDialog dialog = new AddRuleModelTitleDialog(this.titleTable);
		dialog.setVisible(true);
		if (dialog.isOperation) {
			String type = dialog.getTypeCombox().getSelectedItem().toString();
			if (validateExistsTableItem(type)) {
				JecnOptionPane.showMessageDialog(null, type + JecnProperties.getValue("isExist"));
				return;
			}
			Vector<String> v = new Vector<String>();
			v.add(String.valueOf(this.titleTable.getRowCount() + 1));
			v.add(dialog.getNameField().getText());
			v.add(dialog.getNameFieldEn().getText());
			v.add(type);
			v.add(dialog.getRequiredCombox().getSelectedItem().toString());
			this.titleTable.addRow(v);
			dialog.isOperation = false;
		}

	}

	private boolean validateExistsTableItem(String validateType) {
		if (JecnProperties.getValue("content").equals(validateType)
				|| JecnProperties.getValue("fileForm").equals(validateType)) {// 内容和文件表单不验证
			return false;
		}
		int rows = titleTable.getRowCount();
		for (int i = 0; i < rows; i++) {
			String type = titleTable.getModel().getValueAt(i, 3).toString();
			if (validateType.equals(type)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:编辑标题
	 */
	public void editTitleButtonAction() {
		int[] selectRows = this.titleTable.getSelectedRows();
		if (selectRows.length != 1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		} else {
			String enName = "";
			String name = "";
			String type = "";
			String requiredType = "";
			if (this.titleTable.getModel().getValueAt(selectRows[0], 1) != null) {
				name = ((JecnTableModel) this.titleTable.getModel()).getValueAt(selectRows[0], 1).toString();
			}
			if (this.titleTable.getModel().getValueAt(selectRows[0], 2) != null) {
				enName = ((JecnTableModel) this.titleTable.getModel()).getValueAt(selectRows[0], 2).toString();
			}
			if (this.titleTable.getModel().getValueAt(selectRows[0], 3) != null) {
				type = ((JecnTableModel) this.titleTable.getModel()).getValueAt(selectRows[0], 3).toString();
			}
			if (this.titleTable.getModel().getValueAt(selectRows[0], 4) != null) {
				requiredType = ((JecnTableModel) this.titleTable.getModel()).getValueAt(selectRows[0], 4).toString();
			}
			EditRuleModeTitleDialog dialog = new EditRuleModeTitleDialog(this.titleTable);
			dialog.getNameField().setText(name);
			dialog.getNameFieldEn().setText(enName);
			dialog.getTypeCombox().setSelectedItem(type);
			dialog.getRequiredCombox().setSelectedItem(requiredType);
			dialog.setVisible(true);
			if (dialog.isOperation) {
				String string = dialog.getTypeCombox().getSelectedItem().toString();
				if (validateExistsTableItem(string)) {
					JecnOptionPane.showMessageDialog(null, string + JecnProperties.getValue("isExist"));
					return;
				}
				JecnTableModel tableModel = (JecnTableModel) this.titleTable.getModel();
				tableModel.setValueAt(dialog.getNameField().getText(), selectRows[0], 1);
				tableModel.setValueAt(dialog.getNameFieldEn().getText(), selectRows[0], 2);
				tableModel.setValueAt(dialog.getTypeCombox().getSelectedItem().toString(), selectRows[0], 3);
				tableModel.setValueAt(dialog.getRequiredCombox().getSelectedItem().toString(), selectRows[0], 4);
				dialog.isOperation = false;
			}

		}
	}

	public void delteTitleButtonAction() {
		int[] selectRows = this.titleTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("task_selectedRowsInfo"));
			return;
		}
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		this.titleTable.removeRows(selectRows);
		JecnTableModel model = (JecnTableModel) this.titleTable.getModel();
		int rows = model.getRowCount();

		// 重设序号
		for (int i = 0; i < rows; i++) {
			model.setValueAt(i + 1, i, 0);
		}
	}

	public void previewButtonAction() {
		Vector<Vector<String>> vs = ((JecnTableModel) this.titleTable.getModel()).getDataVector();
		RuleModePreview rp = new RuleModePreview(vs);
		rp.setTitle(JecnProperties.getValue("rulModePreview"));
		rp.setVisible(true);

	}

	public void okButtonAction() {
		String name = this.nameField.getText().trim();
		// 验证名称是否正确
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			promptLab.setText(JecnProperties.getValue("modelName") + JecnProperties.getValue("isNotEmpty"));
			return;
		} else if (!JecnUserCheckUtil.checkNameFileSpecialChar(name)) {
			promptLab.setText(JecnProperties.getValue("modelName") + JecnProperties.getValue("notInputThis"));
			return;
		} else if (JecnUserCheckUtil.getTextLength(name) > 122) {
			// 名称不能超过122个字符或61个汉字
			promptLab.setText(JecnProperties.getValue("modelName") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		try {
			if (validateNodeRepeat(name)) {
				promptLab.setText(JecnProperties.getValue("nameHaved"));
				return;
			} else {
				promptLab.setText("");
			}
		} catch (Exception e) {
			log.error("RuleModelDialog okButtonAction is error", e);
			// 提示框
			return;
		}
		List<RuleModeTitleBean> titleList = new ArrayList<RuleModeTitleBean>();
		Vector<Vector<String>> vs = ((JecnTableModel) this.titleTable.getModel()).getDataVector();
		for (Vector<String> v : vs) {
			RuleModeTitleBean tb = new RuleModeTitleBean();
			tb.setSortId(Integer.parseInt(String.valueOf(v.get(0))));
			tb.setTitleName(v.get(1));
			tb.setEnName(v.get(2));
			tb.setType(JecnUtil.formatTitleTypeToInt(v.get(3)));
			tb.setRequiredType(JecnUtil.formatRequiredTypeToInt(v.get(4)));
			titleList.add(tb);
			tb = null;
		}

		try {
			savaData(titleList);
		} catch (Exception e) {
			log.error("RuleModelDialog okButtonAction is error", e);
		}

	}

	public void cancelButtonAction() {
		this.dispose();
	}

	public abstract boolean validateNodeRepeat(String name);

	public abstract void savaData(List<RuleModeTitleBean> titleList) throws Exception;

	public Vector<String> getTableTitle() {
		Vector<String> vector = new Vector<String>();
		vector.add(JecnProperties.getValue("sort"));
		vector.add(JecnProperties.getValue("name"));
		vector.add(JecnProperties.getValue("EnglishName"));
		vector.add(JecnProperties.getValue("type"));
		// 必填项
		vector.add(JecnProperties.getValue("basicTableHeaderName"));
		return vector;
	}

	public abstract Vector<Vector<String>> getContent();

	class titleTable extends JecnTable {

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent());
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] {};
		}

	}
}
