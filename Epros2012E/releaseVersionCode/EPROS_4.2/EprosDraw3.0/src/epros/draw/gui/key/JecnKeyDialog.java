package epros.draw.gui.key;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.toolbar.io.ImportFlowFilter;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 密钥验证对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnKeyDialog extends JecnDialog implements ActionListener {
	/** 主面板 */
	private JPanel mainPanel = null;
	/** 提示信息框 */
	private JLabel infoLabel = null;
	/** 按钮面板 */
	private JPanel btnPanel = null;
	/** 上传密钥按钮 */
	private JButton loadBtn = null;
	/** 取消 */
	private JButton cancelBtn = null;
	/** 确认按钮标识 */
	private boolean isOk = false;

	public JecnKeyDialog() {
		initComponents();
	}

	private void initComponents() {
		Dimension size = new Dimension(300, 180);

		// 主面板
		mainPanel = new JPanel();
		// 提示信息框
		infoLabel = new JLabel();
		// 按钮面板
		btnPanel = new JPanel();
		// 取消
		cancelBtn = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("cancelBtn"));
		// 上传密钥按钮
		loadBtn = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("loadBtn"));

		this.setModal(true);
		this.setSize(size);
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.hiddBtns();
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("MYtitle"));
		this.setResizable(false);

		infoLabel.setOpaque(false);
		infoLabel.setFont(new Font(Font.DIALOG, Font.PLAIN, 14));
		infoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		infoLabel.setVerticalAlignment(SwingConstants.CENTER);
		infoLabel.setVerticalTextPosition(SwingConstants.TOP);
		infoLabel.setIcon(JecnFileUtil.getImageIconforPath("/epros/draw/images/key/error.gif"));

		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.setBorder(null);

		btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 8, 8));
		btnPanel.setOpaque(false);
		btnPanel.setBorder(null);

		cancelBtn.addActionListener(this);
		loadBtn.addActionListener(this);

		mainPanel.add(infoLabel, BorderLayout.CENTER);
		mainPanel.add(btnPanel, BorderLayout.SOUTH);
		this.getContentPane().add(mainPanel);

		// 上传密钥按钮
		btnPanel.add(loadBtn);
		// 取消
		btnPanel.add(cancelBtn);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == loadBtn) { // 上传密钥按钮
			isOk = false;
			if (openFileChooser()) {// 密钥保存成功，再次验证密钥
				okActionPerformed();
			}
		} else if (e.getSource() == cancelBtn) { // 取消
			System.exit(0);
		}
	}

	public void setInfoLabelText(String info) {
		if (info == null) {
			infoLabel.setText("");
			return;
		}
		String[] infoArr = info.split("\\n");
		String htmlInfo = "<html>";
		for (String sub : infoArr) {
			if (!"<html>".equals(htmlInfo)) {
				htmlInfo += "<br>";
			}
			htmlInfo += sub;
		}
		htmlInfo += "</html>";
		infoLabel.setText(htmlInfo);
	}

	/**
	 * 
	 * 确认处理
	 * 
	 */
	private void okActionPerformed() {
		// 密钥验证
		JecnKeyCheck keyCheck = new JecnKeyCheck();
		String info = keyCheck.checkKeyString();
		if (DrawCommon.isNullOrEmtryTrim(info)) {
			JecnKeyDialog.this.setVisible(false);
			isOk = true;
		} else {
			// 密钥验证中....
			setInfoLabelText(info);
			isOk = false;
			return;
		}
	}

	/**
	 * 
	 * 打开本地文件
	 * 
	 * @return
	 */
	private boolean openFileChooser() {
		// 文件选择器
		JecnFileChooser fileChooser = new JecnFileChooser();
		// 移除系统给定的文件过滤器
		fileChooser.setAcceptAllFileFilterUsed(false);
		// 标题
		fileChooser.setDialogTitle(JecnResourceUtil.getJecnResourceUtil().getValue("loadBtn"));
		// 过滤文件后缀
		fileChooser.setFileFilter(new ImportFlowFilter(JecnWorkflowConstant.KEY_FILE_NAME_SUFFIX));
		// 设置按钮为打开
		fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setEidtTextFiled(false);
		fileChooser.setApproveButtonText(JecnResourceUtil.getJecnResourceUtil().getValue("fileUpOKBtn"));

		// 显示文件选择器
		int isOk = fileChooser.showOpenDialog(JecnDrawMainPanel.getMainPanel());

		// 选择确认（yes、ok）后返回该值。
		if (isOk == JFileChooser.APPROVE_OPTION) {

			File selectedFile = fileChooser.getSelectedFile();
			// 获取输出文件的路径
			String filePath = selectedFile.getPath();
			// 判断文件后缀名是否为epros结尾
			if (filePath.endsWith(JecnWorkflowConstant.KEY_FILE_NAME_SUFFIX)) {
				boolean success = JecnFileUtil.writeFileToLocal(selectedFile, JecnWorkflowConstant.KEY_FILE_NAME);

				return success;
			}
		}

		return false;
	}

	boolean isOk() {
		return isOk;
	}
}
