package com.jecn.epros.server.webBean.standard;
/**
 * @author yxw 2012-12-5
 * @description：标准列表显示
 */
public class StandardWebBean {
	/**主键*/
	private long id;
	/**标准名称*/
	private String name;
	/**上级目录*/
	private String pname;
	/**类型 1文件标准，2流程标准 3.流程地图标准*/
	private String type;
	/**文件Id*/
	private long fileId;
	/**文件名称*/
	private String fileName;
	/** 密级 */
	private long secretLevel;
	/**内容*/
	private String stanContent;
	/**是否形成程序文件*/
	private long isProfile;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public long getSecretLevel() {
		return secretLevel;
	}
	public void setSecretLevel(long secretLevel) {
		this.secretLevel = secretLevel;
	}
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	public String getStanContent() {
		return stanContent;
	}
	public void setStanContent(String stanContent) {
		this.stanContent = stanContent;
	}
	public long getIsProfile() {
		return isProfile;
	}
	public void setIsProfile(long isProfile) {
		this.isProfile = isProfile;
	}
	
}
