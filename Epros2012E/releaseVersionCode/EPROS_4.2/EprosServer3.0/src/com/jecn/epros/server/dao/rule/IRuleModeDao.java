package com.jecn.epros.server.dao.rule;

import com.jecn.epros.server.bean.rule.RuleModeBean;
import com.jecn.epros.server.common.IBaseDao;

public interface IRuleModeDao extends IBaseDao<RuleModeBean, Long> {

}
