package com.jecn.webservice.nine999;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import javax.xml.namespace.QName;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.log4j.Logger;

import com.jecn.webservice.cetc10.Cetc10TaskInfoItem;

/**
 * This class was generated by Apache CXF 2.7.18 2018-09-29T17:23:32.667+08:00
 * Generated source version: 2.7.18
 * 
 */
public final class ISysNotifyTodoWebService_ISysNotifyTodoWebServicePort_Client {

	private static final QName SERVICE_NAME = new QName("http://webservice.notify.sys.kmss.landray.com/",
			"ISysNotifyTodoWebServiceService");

	private ISysNotifyTodoWebService_ISysNotifyTodoWebServicePort_Client() {
	}

	static Logger logger = Logger.getLogger(Cetc10TaskInfoItem.class);

	public static void main(String args[]) throws java.lang.Exception {
		// <appName>Epros</appName>
		// <createTime>2018-09-29 03:03:45</createTime>
		// <docCreator></docCreator>
		// <extendContent></extendContent>
		// <key></key>
		// <level></level>
		// <link><![CDATA[http://10.145.98.108:8080loginMail.action?accessType=mailApprove&mailTaskId=100&mailPeopleId=51436&isApp=false&isView=false]]></link>
		// <modelId>10051436</modelId>
		// <modelName>Epros</modelName>
		// <others></others>
		// <param1></param1>
		// <param2></param2>
		// <subject>测试</subject>
		// <targets>{"LoginName":"chenyiming"}</targets>
		// <type>1</type>
		// <weixinflag></weixinflag>
		// <weixintype></weixintype>
		//
		// Object[] sendInfoParams = { "EPROS", "2018-09-29 03:03:45",null ,
		// null, null, 0,
		// "http://10.145.98.108:8080/static-web/#/dashboard", "2", "EPROS",
		// null, null, null, "测试",
		// "chenyiming", 1, null, null };
		// String serviceURL =
		// "http://km.999.com.cn/sys/webservice/sysNotifyTodoWebService?wsdl";
		// JaxWsDynamicClientFactory FACTORY =
		// JaxWsDynamicClientFactory.newInstance();// 1，获取一个工厂实例
		// Client client = FACTORY.createClient(serviceURL);//
		// 2，生成针对指定服务接口URL的客户端
		// Object[] objs = client.invoke("sendTodo", sendInfoParams);
		//
		// System.out.println(objs);

		ISysNotifyTodoWebServiceServiceLocator webServiceServiceLocator = new ISysNotifyTodoWebServiceServiceLocator();
		ISysNotifyTodoWebService webService = webServiceServiceLocator.getISysNotifyTodoWebServicePort();
		NotifyTodoSendContext sendContext = new NotifyTodoSendContext();
		sendContext.setAppName("EPROS");
		sendContext.setCreateTime("2018-09-29 03:03:45");
		sendContext.setType(1);
		sendContext.setLink("http://10.145.98.108:8080/static-web/#/dashboard");
		sendContext.setSubject("测试");
		sendContext.setTargets("{\"LoginName\":\"chenyiming\"}");
		sendContext.setModelId("2");
		sendContext.setModelName("chenyiming");
		webService.sendTodo(sendContext);
		System.exit(0);
	}
}
