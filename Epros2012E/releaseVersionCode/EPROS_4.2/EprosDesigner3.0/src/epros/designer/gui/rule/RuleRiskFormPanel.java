package epros.designer.gui.rule;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.integration.risk.RiskChooseDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;

/***
 * 风险表单 2013-11-22
 * 
 */
public class RuleRiskFormPanel extends JecnFileDescriptionTable {
	protected JButton selectBut = new JButton(JecnProperties.getValue("select"));

	private List<JecnTreeBean> riskBeans;
	protected List<Long> listIds = new ArrayList<Long>();

	public RuleRiskFormPanel(int index, String name, JecnPanel contentPanel, Long relatedId, TreeNodeType nodeType,
			boolean isRequest) {
		super(index, name, isRequest, contentPanel);
		titlePanel.add(selectBut);
		try {
			if (relatedId == null) {
				riskBeans = new ArrayList<JecnTreeBean>();
			} else {
				riskBeans = ConnectionPool.getJecnRiskAction().findJecnRuleRiskBeanTByRuleId(relatedId);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		initTable();
		selectBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				select();
			}
		});
	}

	protected void select() {
		RiskChooseDialog chooseDialog = new RiskChooseDialog(riskBeans);
		chooseDialog.setVisible(true);
		if (chooseDialog.isOperation()) {// 操作
			Vector<Vector<String>> data = ((JecnTableModel) getTable().getModel()).getDataVector();
			if (riskBeans != null && riskBeans.size() > 0) {
				if (data != null) {
					data.clear();
				}
				for (JecnTreeBean jecnTreeBean : riskBeans) {
					((DefaultTableModel) table.getModel()).addRow(this.getRowData(jecnTreeBean));
				}
			} else {
				// 清空表格
				((DefaultTableModel) table.getModel()).setRowCount(0);
			}
		}
	}

	private Vector<String> getRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> vector = new Vector<String>();
		vector.add(String.valueOf(jecnTreeBean.getId()));
		vector.add(jecnTreeBean.getNumberId());
		// 描述
		vector.add(jecnTreeBean.getName());
		return vector;
	}

	@Override
	protected void dbClickMethod() {
		select();
	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		title.add(JecnProperties.getValue("riskNumber"));
		title.add(JecnProperties.getValue("riskDesc"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		// 相关标准
		Vector<Vector<String>> vector = new Vector<Vector<String>>();
		for (JecnTreeBean jecnTreeBean : riskBeans) {
			Vector<String> data = new Vector<String>();
			data.add(jecnTreeBean.getId().toString());
			data.add(jecnTreeBean.getNumberId());
			data.add(jecnTreeBean.getName());
			vector.add(data);
			listIds.add(jecnTreeBean.getId());
		}
		return vector;
	}

	public Set<Long> getResultIds() {
		Set<Long> set = new HashSet<Long>();
		for (JecnTreeBean jecnTreeBean : riskBeans) {
			set.add(jecnTreeBean.getId());
		}
		return set;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return JecnUtil.isChangeListLong(getResultListLong(), listIds);
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
