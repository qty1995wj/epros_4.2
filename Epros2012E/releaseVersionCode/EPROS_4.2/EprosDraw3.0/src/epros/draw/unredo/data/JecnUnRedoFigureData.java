/*      Copyright (c) 2012 JECN Corporation                     */
/*      JECN CONFIDENTIAL AND PROPRIETARY                       */
/*      All rights reserved by JECN Corporation.                */
/*      This program must be used solely for the purpose for    */
/*      which it was furnished by JECN Corporation.   No part   */
/*      of this program may be reproduced  or  disclosed  to    */
/*      others,  in  any form,  without  the  prior  written    */
/*      permission of JECN Corporation. Use  of  copyright      */
/*      notice does not evidence publication of the program.    */
package epros.draw.unredo.data;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 
 * 图形属性备份类，只备份图形重要的属性
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUnRedoFigureData {
	/** 备份的图形数据 */
	private JecnFigureData figureDataClone = null;
	/** 图形对象 */
	private JecnBaseFigurePanel figure = null;

	public JecnUnRedoFigureData(JecnBaseFigurePanel figure) {
		if (figure == null) {
			JecnLogConstants.log_JecnUnRedoFigureData
					.error("JecnUnRedoFigureData(JecnBaseFigurePanel figure)异常：参数为空");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.figure = figure;
		this.figureDataClone = figure.getFlowElementData().clone();
	}

	public JecnFigureData getFigureDataClone() {
		return figureDataClone;
	}

	public JecnBaseFigurePanel getFigure() {
		return figure;
	}

	/**
	 * 
	 * 获取图形的数据对象
	 * 
	 * @return
	 */
	public JecnFigureData getFigureData() {
		if (figure != null) {
			return figure.getFlowElementData();
		}
		return null;
	}
}
