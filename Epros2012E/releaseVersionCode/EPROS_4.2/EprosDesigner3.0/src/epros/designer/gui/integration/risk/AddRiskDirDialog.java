package epros.designer.gui.integration.risk;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.util.JecnUserCheckUtil;

/***
 * 新建风险目录 2013-10-30
 * 
 */
public class AddRiskDirDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(AddRiskDirDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public AddRiskDirDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("createRiskDir");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("riskDirC");
	}

	@Override
	public void saveData() {
		// 向JecnRisk文件中添加数据
		JecnRisk jecnRisk = new JecnRisk();
		if (!"".equals(JecnUserCheckUtil.checkName(getName()))) {
			promptLab.setText(JecnUserCheckUtil.checkName(getName()));
			return;
		}
		jecnRisk.setRiskName(getName());
		jecnRisk.setParentId(pNode.getJecnTreeBean().getId());
		jecnRisk.setIsDir(0);
		jecnRisk.setSort(JecnTreeCommon.getMaxSort(pNode));
		Date createDatime = new Date();
		jecnRisk.setCreateTime(createDatime);
		jecnRisk.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnRisk.setUpdateTime(createDatime);
		jecnRisk.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnRisk.setProjectId(JecnConstants.projectId);
		// 向数据库中添加风险目录
		Long id = null;
		try {
			id = ConnectionPool.getJecnRiskAction().addJecnRiskOrDir(jecnRisk);
			// 向树节点添加风险目录节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.riskDir);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
		} catch (Exception e) {
			log.error("AddRiskDirDialog saveData is error", e);
		}
		// 关闭窗体
		this.dispose();
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name, TreeNodeType.riskDir);
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
