package com.jecn.epros.server.service.dataImport.match;

import java.util.List;

import com.jecn.epros.server.webBean.dataImport.match.ActualPosBean;
import com.jecn.epros.server.webBean.dataImport.match.PosEprosRelBean;

/**
 * 岗位匹配数据处理
 * 
 * @author xiaohu
 * 
 */
public interface IPostionMatchService {
	/**
	 * 获取岗位匹配总数
	 * 
	 * @return
	 * @throws Exception
	 */
	public int getStatcionRowCount() throws Exception;

	/**
	 * 获取当前页岗位匹配集合
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<PosEprosRelBean> getStatcionData(int pageSize, int startRow)
			throws Exception;

	/**
	 * 导入的项目是否为当前项目
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean checkProjectId() throws Exception;

	/**
	 * 获取未匹配的岗位结果
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<ActualPosBean> getActualPosBeanList(int type, int pageSize,
			int startRow,String posName) throws Exception;

	/**
	 * 
	 * 获取未匹配的岗位或基准岗位或流程岗位
	 * 
	 * @param type
	 *            1:HR实际岗位；5：流程岗位；6基准岗位
	 * @return
	 * @throws DaoException
	 */
	int getNoMatchObjectCount(int type,String posName) throws Exception;
}
