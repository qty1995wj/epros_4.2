package com.jecn.epros.domain.journal;

import io.swagger.annotations.ApiModelProperty;

/***
 * 用户访问日志
 *
 * @author lenovo
 *
 */
public class SearchUserOperates {
    /**
     * 用户ID
     */
    private Long operationUserId;
    @ApiModelProperty(value = "用户类型 0是人员，1是岗位，2是部门")
    private int userType;
    /**
     * 文件ID
     */
    private Long relatedId;
    @ApiModelProperty(value = " 日志类型 1是流程，2是文件，3是角色,4是制度,5是标准,6是组织,9是风险,15 术语定义 16岗位组 ，17支持工具， 18风险点，" +
            " 19制度模板 ， 20模型")
    private int relatedType = -1;
    @ApiModelProperty(value = "资源名称 ")
    private String relatedName;

    @ApiModelProperty(value = "开始时间 ")
    private String startTime;

    @ApiModelProperty("结束时间 ")
    private String endTime;

    public Long getOperationUserId() {
        return operationUserId;
    }

    public void setOperationUserId(Long operationUserId) {
        this.operationUserId = operationUserId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public int getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(int relatedType) {
        this.relatedType = relatedType;
    }

    public String getRelatedName() {
        return relatedName;
    }

    public void setRelatedName(String relatedName) {
        this.relatedName = relatedName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
