package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.system.FavoriteUpdate;
import com.jecn.epros.server.email.buss.ToolEmail;

public class FavoriteUpdateEmailBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

		Object[] data = getData();
		FavoriteUpdate favorite = (FavoriteUpdate) data[0];

		return createEmail(user, favorite);
	}

	public EmailBasicInfo createEmail(JecnUser user, FavoriteUpdate favorite) {

		String fileType = getFileTypeByTaskType(favorite.getTaskType());
		// 生成邮件的正文
		EmailContentBuilder contentBuilder = new EmailContentBuilder();
		contentBuilder.setContent("您好，您收藏的" + fileType + "'" + favorite.getrName() + "'已经更新了，请点击链接查看。");
		contentBuilder.setResourceUrl(getHttpUrl(String.valueOf(favorite.getrId()), String.valueOf(user.getPeopleId()),
				favorite.getTaskType()));
		String subject = "收藏文件更新";
		EmailBasicInfo emailBasicInfo = new EmailBasicInfo();
		emailBasicInfo.setSubject(subject);
		emailBasicInfo.setContent(contentBuilder.buildContent());
		emailBasicInfo.addRecipients(user);
		return emailBasicInfo;

	}

	private String getFileTypeByTaskType(Integer taskType) {
		if (taskType == 0 || taskType == 4) {
			return "流程";
		} else if (taskType == 2 || taskType == 3) {
			return "制度";
		} else if (taskType == 1) {
			return "文件";
		}

		return "";
	}

	private String getHttpUrl(String rId, String peopleId, int taskType) {
		// "0 流程", "1架构", "2制度", "3内控", "4文件", "5标准"
		String httpUrl = "";
		// 如果是流程任务
		if (taskType == 0) {
			httpUrl = ToolEmail.getHttpUrl(rId, "process", peopleId);
		} else if (taskType == 2)// 制度模板任务
		{
			httpUrl = ToolEmail.getHttpUrl(rId, "ruleModeFile", peopleId);
		} else if (taskType == 3)// 制度文件任务
		{
			httpUrl = ToolEmail.getHttpUrl(rId, "ruleFile", peopleId);
		} else if (taskType == 4)// 流程地图
		{
			httpUrl = ToolEmail.getHttpUrl(rId, "processMap", peopleId);
		} else if (taskType == 1)// 文件任务
		{
			httpUrl = ToolEmail.getHttpUrl(rId, "infoFile", peopleId);
		}

		return httpUrl;
	}

}
