package com.jecn.epros.server.dao.process.impl;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnKPIFirstTargetBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.dao.process.IKPIFirstTargetDao;
/***
 *  一级指标
 * @Time 2014-10-17S
 *
 */
public class KPIFirstTargetDaoImpl extends AbsBaseDao<JecnKPIFirstTargetBean, Long>
		implements IKPIFirstTargetDao {
	/***
	 * 添加一级指标
	 * @throws Exception
	 */
	@Override
	public String addFirstTarget(List<JecnKPIFirstTargetBean> list) throws Exception {
		String newFirstTargetId = null;
		// 获取数据库中一级指标数据
		String hql = "from JecnKPIFirstTargetBean";
		List<JecnKPIFirstTargetBean> dataFirstTargets = this.listHql(hql);
		if (list != null) {
			for (JecnKPIFirstTargetBean jecnKPIFirstTargetBean : list) {
				boolean isExit = false;
				for (JecnKPIFirstTargetBean dataFirstTargetBean : dataFirstTargets) {
					if (jecnKPIFirstTargetBean.getId() != null
							&& !"".equals(dataFirstTargetBean.getId())) {
						if (jecnKPIFirstTargetBean.getId().equals(
								dataFirstTargetBean.getId())) {
							dataFirstTargets.remove(dataFirstTargetBean);
							isExit = true;
							// 与数据库中ID相同的进行更新
							this.getSession().merge(
									jecnKPIFirstTargetBean);
							break;
						}
					}
				}
				if (!isExit) {
					// 没有主键ID的进行添加
					String newUUID = JecnCommon.getUUID();
					jecnKPIFirstTargetBean.setId(newUUID);
					if(jecnKPIFirstTargetBean.getSortId() == 1){
						newFirstTargetId = newUUID;
					}
					this.save(jecnKPIFirstTargetBean);
				}
			}
		}
		// 删除数据库中的数据，条件：数据库中的不存在于最新数据中的
		for (JecnKPIFirstTargetBean dataFirstTargetBean : dataFirstTargets) {
			this.deleteObject(dataFirstTargetBean);
		}
		return newFirstTargetId;
	}
	/**
	 * 获取所有  一级指标数据集合
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getAllFirstTarget() throws Exception {
		String sql = "select * from JECN_FIRST_TARGER";
		return this.listNativeSql(sql);
	}

	
	public JecnKPIFirstTargetBean getFirstTarget(String id) throws Exception {
		String hql = "from JecnKPIFirstTargetBean where id=?";
		return this.getObjectHql(hql, id);
	}
	

}
