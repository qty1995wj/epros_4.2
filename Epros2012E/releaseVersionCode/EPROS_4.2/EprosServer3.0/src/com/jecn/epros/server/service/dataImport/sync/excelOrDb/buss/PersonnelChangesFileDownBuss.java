package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;

/**
 * 人员变更数据excel下载
 * @author zhangft
 * @date   2013-6-13 16:34	
 */
public class PersonnelChangesFileDownBuss extends AbstractFileDownBuss {

	/**
	 * 导出文件路径
	 * @return String 文件路径
	 */
	@Override
	public String getOutFilepath() {
		return SyncTool.getPersonnelChangesFilePath();
	}

	/**
	 * 获得下载时保存的文件名
	 * @return String 文件名称 
	 */
	@Override
	public String getOutFileName() {
		return SyncConstant.EXPORT_DATA_OF_PERSONNELCHANGES_FILE_NAME;
	}

}
