package com.jecn.epros.email;

import com.jecn.epros.domain.task.TaskBean;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.TaskCommon;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;

/**
 * 邮件数据处理
 *
 * @author Administrator
 */
public class ToolEmail {
    public static final Log log = LogFactory.getLog(ToolEmail.class);
    public static String TASK_DESC = "任务说明：";
    public static String IS_FINISH_PUB = "已完成并发布, 请查阅！";
    public static String TASK_CREATE_PEOPLE = "任务创建人：";
    public static String TASK_CREATE_DATE = "任务创建时间：";
    public static String TASK_APPROVE = "已提交，请审批！";
    public static String TASK_NO_APPROVE = "已打回，请重新提交！";
    public static String TASK_TITLE = "任务：";
    public static String task_stage = "任务进度：";
    public static String IS_FINISH = "已发布！";
    /**
     * 试运行已发布，请填写试运行报告！
     */
    public static String TEST_RUN_IS_PUFLIC = "试运行已发布，请填写试运行报告！";
    /**
     * 任务试运行已到期！
     */
    public static String TEST_RUN_IS_EXPIRED = "任务试运行已到期！";
    /**
     * 试运行已结束！
     */
    public static String TEST_RUN_IS_END = "试运行已结束！";

    /**
     * 请填写试运行报告！
     */
    public static String PLEASE_FILL_REPORT = "请填写试运行报告！";

    /**
     * 试运行文件路径
     */
    public static String testRunFilePath = null;
    /**
     * 试运行文件名称
     */
    public static String TASK_RUN_FILE = "流程试运行总结表.xls";
    public static String TASK_ON_APPROVE = "正在审批！";


    /**
     * 多邮件处理
     *
     * @param listStr
     * @return
     * @throws AddressException
     */
    public static Address[] getArrayAddress(ArrayList<String> listStr) throws AddressException {
        Address[] adds = new InternetAddress[listStr.size()];
        for (int i = 0; i < listStr.size(); i++) {
            adds[i] = new InternetAddress(listStr.get(i));
        }
        return adds;
    }


    /**
     * 获取任务正文
     *
     * @param state
     * @return
     */
    public static String getTaskDesc(TaskBean taskBeanNew) {
        String taskDesc = "";
        if (taskBeanNew.getTaskDesc() != null && !"".equals(taskBeanNew.getTaskDesc())) {
            taskDesc = taskBeanNew.getTaskDesc().replaceAll("\n", "<br>");
        }
        //更新时间
        String updateTime = Common.getStringbyDate(taskBeanNew.getUpdateTime());

        //文件名称
        String rname = taskBeanNew.getRname();

        // 如果任务时结束状态
        if (taskBeanNew.getTaskState() == 5) {
            taskBeanNew.setStateMark(TaskCommon.FINISH);
        }

        // 任务状态
        String stateStr = task_stage + taskBeanNew.getStateMark();
        String createName = "";
        if (taskBeanNew.getCreatePeopleTemporaryName() != null
                && !"".equals(taskBeanNew.getCreatePeopleTemporaryName())) {
            createName = TASK_CREATE_PEOPLE + taskBeanNew.getCreatePeopleTemporaryName() + "<br>";
        }

        String str = "文件名称："
                + rname
                + "<br>"
                + "更新时间："
                + updateTime
                + "<br>"
                + "变更说明："
                + taskDesc
                + "<br>"
                + createName
                + TASK_CREATE_DATE
                + Common.getStringbyDate(taskBeanNew.getCreateTime())
                + "<br>"
                + stateStr;
        return str;
    }

    /**
     * 获取任务审批 指定下一个操作人的连接地址
     *
     * @param taskId   任务主键ID
     * @param peopleId 下一个操作人人员主键ID（目标人）
     * @return String指定下一个操作人的连接地址
     */
    public static String getTaskHttpUrl(Long taskId, Long peopleId) {
        // 拼装邮件连接地址
        return "/loginMail.action?mailTaskId=" + taskId + "&mailPeopleId=" + peopleId + "&accessType="
                + MailLoginEnum.mailApprove.toString();
    }

    /**
     * 获取任务试运行连接地址
     *
     * @return String指定下一个操作人的连接地址
     */
    public static String getTestRunHttpUrl() {
        // 拼装邮件连接地址
        return "/login.jsp";
    }

    /**
     * 获取发布流程的邮件地址
     *
     * @param flowId
     * @param peopleId 邮件接收者人员ID
     * @return
     */
    public static String getHttpUrl(Long flowId, String prfType, Long peopleId) {
        // 拼装邮件连接地址process.action?type=process&flowId=362
        String ipAddressUrl = "/loginMail.action?mailFlowType=" + prfType + "&mailFlowId=" + flowId + "&mailPeopleId="
                + peopleId + "&accessType=" + MailLoginEnum.mailPubApprove.toString();
        return ipAddressUrl;
    }

    /**
     * 根据任务操作获取对应的邮件标题
     *
     * @param jecnTaskBeanNew
     * @return
     */
    public static String getTaskSubject(TaskBean jecnTaskBeanNew) {
        String subject = "";
        if (jecnTaskBeanNew.getTaskElseState() == 4) {// 打回
            subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + ToolEmail.TASK_NO_APPROVE;
        } else if (jecnTaskBeanNew.getTaskState() == 5)// 任务完成
        {
            subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + ToolEmail.IS_FINISH;

        } else {
            subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + ToolEmail.TASK_APPROVE;
        }

        return subject;
    }

    /**
     * 根据任务操作获取对应的邮件标题(给拟稿人发送的邮件)
     *
     * @param jecnTaskBeanNew
     * @param toPeopleType    0、为审批人员  1、为拟稿人
     * @return
     */
    public static String getTaskSubjectForPeopleMake(TaskBean jecnTaskBeanNew) {
        String subject = "";
        if (jecnTaskBeanNew.getTaskState() == 5)// 任务完成
        {
            subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + "," + ToolEmail.IS_FINISH;

        } else {
            subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + ","
                    + ToolEmail.TASK_ON_APPROVE;
        }

        return subject;

    }


    /**
     * @param id       流程或者流程地图或者制度或者文件的id
     * @param prfType  文件类型
     * @param peopleId 人员id
     * @return String 直接打开文件信息链接地址
     * @author hyl
     * 直接发布拼装链接到详细信息界面
     */
    public static String getHttpUrl(String id, String prfType, String peopleId) {

        String ipAddressUrl = "/login.jsp";
        // 拼装邮件连接地址process.action?type=process&flowId=362
        if (prfType.equals("process")) {
            ipAddressUrl = "/loginMail.action?mailFlowType=" + prfType + "&mailFlowId=" + id + "&mailPeopleId="
                    + peopleId + "&accessType=" + MailLoginEnum.mailPubApprove.toString();
        } else if (prfType.equals("processMap")) {
            ipAddressUrl = "/loginMail.action?mailFlowType=" + prfType + "&mailFlowId=" + id + "&mailPeopleId="
                    + peopleId + "&accessType=" + MailLoginEnum.mailPubApprove.toString();
        } else if (prfType.equals("ruleFile"))//制度文件
        {
            ipAddressUrl = "/loginMail.action?mailRuleType=" + prfType + "&mailRuleId=" + id + "&mailPeopleId="
                    + peopleId + "&accessType=" + MailLoginEnum.mailOpenRule.toString() + "&isPub=true";
        } else if (prfType.equals("ruleModeFile"))//制度模板文件
        {
            ipAddressUrl = "/loginMail.action?mailRuleType=" + prfType + "&mailRuleId=" + id + "&mailPeopleId="
                    + peopleId + "&accessType=" + MailLoginEnum.mailOpenRule.toString() + "&isPub=true";
        } else if (prfType.equals("infoFile"))//文件任务
        {
            ipAddressUrl = "/loginMail.action?mailFileId=" + id + "&mailPeopleId="
                    + peopleId + "&accessType=" + MailLoginEnum.mailOpenFile.toString() + "&isPub=true";
        }

        return ipAddressUrl;
    }


    /**
     * 直接登录系统方式
     */
    public enum MailLoginEnum {
        mailApprove, // 任务审批：邮件登录到审批页面
        mailPubApprove, // 任务审批：邮件登录到流程页面
        mailOpenMap, // 邮件方式直接打开流程图/流程地图页面
        mailOpenActiveInfo, // 邮件方式直接打开活动明细页面
        mailOpenApprovingList, // 任务审批列表数据页面
        mailOpenButtPublicFlow, // 流程发布的信息数据显示界面(包括：我关注的流程、我参与的流程)
        mailOpenDominoRuleModelFile, // 华帝Domino平台打开制度模板文件页面
        mailOpenDominoMap, // 华帝domino平台打开流程图/流程地图页面
        mailOpenDominoMapSearchJSP, // 华帝Domino平台打开流程地图搜索页面
        mailOpenTaskManagement, // 管理员打开任务管理页面
        mailOpenRule, // 邮件方式直接打开制度模板/制度文件页面
        mailOpenFile, // 邮件方式直接打开文件
        mailProposeHandle, // 邮件方式处理合理化建议
        mailOpenPropose,
        // 邮件方式打开合理化建议
        gtasksCount, // 登录人待办任务总数
        myTaskJson, // 我的任务返回json
        numberOfBack, // 待办总数：输出纯数字
        myTaskXml, // 广机输出任务XML
        toDOTasks, // 待审批任务（不包含查阅） 只提供任务名称和连接地址
        toDOTasksCount
        // 待审批任务（不包含查阅） 只提供任务名称和连接地址
    }


}
