package com.jecn.epros.server.bean.define;

import java.util.Date;

public class TermDefinitionLibrary implements java.io.Serializable{
	private Long id;
	private String name;
	private Long parentId;
	private String instructions;// 定义
	private int isDir;// 0是目录,1是文件
	private Date createTime;
	private Long createPersonId;
	private Date updateTime;
	private Long updatePersonId;
	private int sortId;
	private String tPath;// tpath 节点层级关系
	private int tLevel;// level 节点级别，默认0开始
	private String viewSort;
	public String getViewSort() {
		return viewSort;
	}

	public void setViewSort(String viewSort) {
		this.viewSort = viewSort;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public int getIsDir() {
		return isDir;
	}

	public void setIsDir(int isDir) {
		this.isDir = isDir;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getUpdatePersonId() {
		return updatePersonId;
	}

	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}

	public int getSortId() {
		return sortId;
	}

	public void setSortId(int sortId) {
		this.sortId = sortId;
	}

	public String gettPath() {
		return tPath;
	}

	public void settPath(String tPath) {
		this.tPath = tPath;
	}

	public int gettLevel() {
		return tLevel;
	}

	public void settLevel(int tLevel) {
		this.tLevel = tLevel;
	}
}
