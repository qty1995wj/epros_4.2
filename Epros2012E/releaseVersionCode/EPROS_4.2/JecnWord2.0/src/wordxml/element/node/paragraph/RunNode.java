package wordxml.element.node.paragraph;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import wordxml.element.bean.common.LineBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.dom.paragraph.Run;
import wordxml.element.node.graph.GraphNode;
import wordxml.util.Tools;

/***
 * 显示run段操作
 * 
 * @author admin
 * 
 */
public class RunNode {
	private static final Log log = LogFactory.getLog(RunNode.class);

	/**
	 * 创建一个运行模块
	 * 
	 * @param run
	 * @param parentEle
	 */
	public static void createRun(Run run, Element parentEle) {
		if (run == null || parentEle == null) {
			log.info("RunNode class createRun Argument is not valid");
			return;
		}
		String text = run.getText();
		// 添加文字
		if (text == null) {
			text = "";
		}
		Element r = parentEle.addElement("w:r");
		if (run.getParent().getParagraphBean() != null) {
			Element rPr = r.addElement("w:rPr");
			// 以自身文本样式为主
			FontBean fBean = null;
			if (run.getFontBean() == null) {
				fBean = run.getParent().getParagraphBean().getFontBean();
			} else {
				fBean = run.getFontBean();
			}
			setAttr(rPr, fBean);
		}
		// 添加图片
		if (run.getGraph() != null) {
			GraphNode.createGraph(run.getGraph(), r);
		}
		// 特殊处理(当前页码) 插入页码表达式
		if (run.isSpecialText()) {
			if ("PAGE".equals(text) || "SectionPages".equals(text)) {
				// text=PAGE 当前页 text=NUMPAGES 总页数 {={PAGE}-1}
				// 对应{
				r.addElement("w:fldChar").addAttribute("w:fldCharType", "begin");
				r.addElement("w:instrText").addText(text);
				// 对应}
				r.addElement("w:fldChar").addAttribute("w:fldCharType", "end");
			}
			// 特殊处理（总页数）页眉页脚引用标签使用如下方式。页面引用使用另一种方式 
			else if ("NUMPAGES".equals(text)) {
				r.addElement("w:fldChar").addAttribute("w:fldCharType", "begin");
				r.addElement("w:r").addElement("w:instrText").addText("=");
				r.addElement("w:r").addElement("w:fldChar").addAttribute("w:fldCharType", "begin");
				r.addElement("w:r").addElement("w:instrText").addText("PAGEREF EndPage");// NUMPAGES  EndPage
				r.addElement("w:r").addElement("w:fldChar").addAttribute("w:fldCharType", "separate");
				r.addElement("w:r").addElement("w:fldChar").addAttribute("w:fldCharType", "end");
				r.addElement("w:r").addElement("w:instrText").addText("-" + run.getTitleNumPages());
				r.addElement("w:r").addElement("w:fldChar").addAttribute("w:fldCharType", "separate");
				r.addElement("w:r").addElement("w:fldChar").addAttribute("w:fldCharType", "end");
			}
		} else {
			setShowText(r.addElement("w:t"), text);
		}
	}

	/**
	 * 设置显示的文本内容
	 * 
	 * @param text
	 */
	private static void setShowText(Element t, String text) {
		if (t == null) {
			return;
		}
		t.addText(text);
	}

	/**
	 * 设置文本的属性
	 * 
	 * @param fontBean
	 */
	private static void setAttr(Element rPr, FontBean fontBean) {
		if (rPr == null || fontBean == null) {
			return;
		}
		// 字体 fareast 东亚字体
		Element rFonts = rPr.addElement("w:rFonts");
		rFonts.addAttribute("w:ascii", fontBean.getFontFamily());
		rFonts.addAttribute("w:fareast", fontBean.getFontFamily());
		rFonts.addAttribute("w:h-ansi", fontBean.getFontFamily());
		rPr.addElement("wx:font").addAttribute("wx:val", fontBean.getFontFamily());
		// 字体颜色
		rPr.addElement("w:color").addAttribute("w:val", Tools.convertColorToRGBStr(fontBean.getFontColor()));
		// 字号
		rPr.addElement("w:sz").addAttribute("w:val", String.valueOf(fontBean.getFontSize()));
		rPr.addElement("w:sz-cs").addAttribute("w:val", String.valueOf(fontBean.getFontSize()));
		// 加粗
		if (fontBean.isB()) {
			rPr.addElement("w:b");
			rPr.addElement("w:b-cs");
		}
		// 倾斜
		if (fontBean.isI()) {
			rPr.addElement("w:i");
			rPr.addElement("w:i-cs");
		}
		// 下划线
		LineBean lineBean = fontBean.getUnderlineBean();
		if (lineBean != null) {
			// <w:u w:val="dash" w:color="FF0000"/>
			Element u = rPr.addElement("w:u");
			u.addAttribute("w:val", lineBean.getType().getValue());
			u.addAttribute("w:color", Tools.convertColorToRGBStr(lineBean.getColor()));
		}
		// 绘制一条单线穿过文本
		if (fontBean.isStrike()) {
			rPr.addElement("w:strike");
		}
		// 绘制双线穿过文本
		if (fontBean.isDstrike()) {
			rPr.addElement("w:dstrike");
		}
		// 放大缩小比例
		if (fontBean.getTextScaleVal() != 0) {
			rPr.addElement("w:w").addAttribute("w:val", String.valueOf(fontBean.getTextScaleVal()));
		}
		// 底纹
		if (fontBean.getShdBean() != null) {
			// <w:shd w:val="pct-15" w:color="auto" w:fill="FFFFFF"
			// wx:bgcolor="D9D9D9"/>
			Element shd = rPr.addElement("w:shd");
			shd.addAttribute("w:val", fontBean.getShdBean().getVal().getValue());
			shd.addAttribute("w:color", fontBean.getShdBean().getColor());
			shd.addAttribute("w:fill", Tools.convertColorToRGBStr(fontBean.getShdBean().getFill()));
			// shd.addAttribute("wx:bgcolor",
			// fontBean.getShdBean().getBgcolor());
		}
	}
}
