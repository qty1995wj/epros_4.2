package com.jecn.webservice.crs.task.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebService;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.connector.SinglePointBean;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.task.search.IJecnTaskSearchService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.webservice.common.TaskApiUrlHandler;
import com.jecn.webservice.crs.task.ICrsTaskWebService;
import com.jecn.webservice.crs.task.RuleInfoData;

@WebService
public class CrsTaskWebServiceImpl implements ICrsTaskWebService {
	private static final Logger log = Logger.getLogger(CrsTaskWebServiceImpl.class);

	@WebMethod
	public String GetPList(String PSCODE, String PPRINCIPAL, String PTYPE, int PNUM) {
		if (StringUtils.isBlank(PPRINCIPAL)) {
			log.error("获取用户待办异常，用户名为空！");
			return null;
		}
		IPersonService personService = (IPersonService) ApplicationContextUtil.getContext()
				.getBean("PersonServiceImpl");
		IJecnTaskSearchService iJecnTaskSearchService = (IJecnTaskSearchService) ApplicationContextUtil.getContext()
				.getBean("searchServiceImpl");
		// 获取用户信息
		WebLoginBean webLoginBean = null;
		List<MyTaskBean> listTask = null;
		try {
			webLoginBean = personService.getWebLoginBean(PPRINCIPAL, "", false);
			Long peopleId = webLoginBean.getJecnUser().getPeopleId();
			// 数据库查询 我的待办
			listTask = iJecnTaskSearchService.getToDotasksBean(peopleId, 0, PNUM);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户任务数据时，用户登录验证失败！原因：用户名不存在，loginName = " + PPRINCIPAL);
		}
		if (webLoginBean == null || webLoginBean.getLoginState() == 0) {
			log.error("获取用户任务数据时，用户登录验证失败！原因：用户名不存在，loginName = " + PPRINCIPAL);
			return null;
		}

		return getXmlByTaskBeans(PSCODE, PPRINCIPAL, PTYPE, webLoginBean.getJecnUser().getLoginName(), listTask);
	}

	/**
	 * 根据登录名获取制度
	 */
	@WebMethod
	public String getRuleList(String loginName, int PNUM) {
		if (StringUtils.isBlank(loginName)) {
			return "登录名称不能为空！";
		}
		IRuleService ruleService = (IRuleService) ApplicationContextUtil.getContext().getBean("RuleServiceImpl");
		try {
			List<Object[]> ruleList = ruleService.getRuleByWebService(loginName, 0, PNUM);

			List<RuleInfoData> resultList = getRuleListByObjectsList(ruleList);
			JSONArray jsonArray = JSONArray.fromObject(resultList);
			return jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "获取制度数据异常！";
		}
	}

	public static List<RuleInfoData> getRuleListByObjectsList(List<Object[]> ruleList) {
		List<RuleInfoData> ruleInfoList = new ArrayList<RuleInfoData>();
		RuleInfoData infoData = null;
		List<String> existIds = new ArrayList<String>();

		Map<String, RuleInfoData> ruleInfoMap = new HashMap<String, RuleInfoData>();
		// 数据库配置URL
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		// 0:ID,1:制度名称;2:编号3:拟稿人(创建人);4:发布部门;5:发布时间
		for (Object[] objects : ruleList) {
			String id = objects[0].toString();
			infoData = new RuleInfoData();
			if (existIds.contains(id)) {
				joinStringOrgName(valueOfString(objects[4]), infoData);
			} else {
				infoData.setDrafter(valueOfString(objects[3]));
				infoData.setId(id);
				infoData.setPubOrg(valueOfString(objects[4]));
				infoData.setPubTime(JecnCommon.getStringbyDate(JecnCommon.getDateByString(valueOfString(objects[5]))));
				// 制度名称，不含扩展名
				String ruleName = valueOfString(objects[1]);
				if (StringUtils.isBlank(ruleName)) {
					continue;
				}
				String isDir = valueOfString(objects[6]);
				if ("1".equals(isDir)) {
					infoData.setRuleName(ruleName);
				} else {
					infoData.setRuleName(ruleName.indexOf(".") != -1 ? ruleName.substring(0, ruleName.lastIndexOf("."))
							: ruleName);
				}
				infoData.setRuleNum(valueOfString(objects[2]));
				infoData.setUrl(basicEprosURL + TaskApiUrlHandler.getRule(id, "true"));
				existIds.add(id);
				ruleInfoMap.put(id, infoData);
				ruleInfoList.add(infoData);
			}
		}
		return ruleInfoList;
	}

	public static void main(String[] args) {
		String ruleName = "1.1.1.1.1测试制度模板.doc";
		System.out.println(ruleName.indexOf(".") != -1 ? ruleName.substring(0, ruleName.lastIndexOf(".")) : ruleName);
	}

	private static String valueOfString(Object obj) {
		return obj == null ? null : obj.toString();
	}

	private static void joinStringOrgName(String orgName, RuleInfoData infoData) {
		if (StringUtils.isBlank(orgName)) {
			return;
		}
		infoData.setPubOrg(infoData.getPubOrg() + "/" + orgName);
	}

	/**
	 * 待办数据XML结构返回
	 * 
	 * @param PSCODE
	 * @param PPRINCIPAL
	 * @param PTYPE
	 * @param loginName
	 * @param listTask
	 * @return
	 */
	private String getXmlByTaskBeans(String PSCODE, String PPRINCIPAL, String PTYPE, String loginName,
			List<MyTaskBean> listTask) {
		Document doc = null;
		try {
			doc = DocumentHelper.createDocument();

			// 数据库配置URL
			String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
			// 1、设置workflow
			Element root = doc.addElement("ROWS");
			Element row = null;
			String PSCODEZH = "流程管理平台";
			Element ele = null;
			if (listTask == null) {
				return root.asXML();
			}
			for (MyTaskBean myTaskBean : listTask) {
				row = root.addElement("ROW");
				ele = row.addElement("PSCODE");
				ele.setText(PSCODE);
				ele = row.addElement("PCODE");
				ele.setText(String.valueOf(myTaskBean.getTaskId()));
				ele = row.addElement("PTITLE");
				ele.setText(myTaskBean.getTaskName());
				ele = row.addElement("PDATE");
				ele.setText(myTaskBean.getUpdateTime());
				ele = row.addElement("PPRINCIPAL");
				ele.setText(loginName);
				ele = row.addElement("PURL");
				ele.setText(basicEprosURL + SinglePointBean.getLoginMailApproveDominoUrl(myTaskBean));
				ele = row.addElement("PORANIGER");
				ele.setText(myTaskBean.getCreateName());
				ele = row.addElement("PTYP");
				ele.setText(PTYPE);
				ele = row.addElement("PSCODEZH");
				ele.setText(PSCODEZH);
			}
			return root.asXML();
		} catch (Exception e) {
			log.error("构建dom文档异常", e);
			return null;
		} finally {
			doc = null;
		}
	}
}
