package com.jecn.epros.server.dao.dataImport.excelorDB;

import java.util.List;

/**
 * 读数据写入到excel再保存到本地 人员数据变更导出excel,sheet数据
 * 
 * @author zhangft 2013-5-30
 * 
 */
public interface IExportDataOfPersonnelChangesDao {

	/**
	 * 流程图责任部门表
	 */
	public List<Object[]> getProcessTaskDeptList(Long currProjectID);

	/**
	 * 文件表 责任部门
	 */
	public List<Object[]> getDutyDeptFileList(Long currProjectID);
	
	/**
	 * 制度责任部门
	 * @param currProjectID
	 * @return
	 */
	public List<Object[]> getDutyDeptRuleList(Long currProjectID);

	/**
	 * 部门查阅权限
	 */
	public List<Object[]> getDeptAccessPermList(Long currProjectID);

	/**
	 * 删除岗位影响的岗位组
	 */
	public List<Object[]> getDeleteAffectedPositionGroupList(Long currProjectID);

	/**
	 * 流程基本信息
	 */
	public List<Object[]> getFlowBasicInfoList(Long currProjectID);

	/**
	 * 流程角色与岗位关联
	 */
	public List<Object[]> getFlowRoleWithPositionList(Long currProjectID);

	/**
	 * 岗位查阅权限
	 */
	public List<Object[]> getPositionAccessPermList();

	/**
	 * 删除人员，流程主表创建人为空
	 */
	public List<Object[]> getDeleteUserFlowList();

	/**
	 * 删除人员影响的文件表信息
	 */
	public List<Object[]> getDeleteUserImpactFileInfoList();

	/************ 人员变更 ： 同步数据 ****************/
	/**
	 * 添加部门
	 */
	public List<Object[]> getAddDeptList(Long currProjectID);

	/**
	 * 添加岗位
	 */
	public List<Object[]> getAddPosList();

	/**
	 * 添加人员
	 */
	public List<Object[]> getAddUserList();

	/**
	 * 删除部门
	 */
	public List<Object[]> getDeleteDeptList(Long currProjectID);

	/**
	 * 删除岗位
	 */
	public List<Object[]> getDeletePosList();

	/**
	 * 删除人员
	 */
	public List<Object[]> getDeleteUserList();

	/**
	 * 更新部门
	 */
	public List<Object[]> getUpdateDeptList(Long currProjectID);

	/**
	 * 更新岗位
	 */
	public List<Object[]> getUpdatePosList(Long currProjectID);

	/**
	 * 更新人员
	 */
	public List<Object[]> getUpdateUserList();

	List<Object[]> getUpdatePosGroupList();

	List<Object[]> getDeletePosGroupList();

	List<Object[]> getAddPosGroupList();

	
}
