package com.jecn.epros.server.action.web.login.ad.mengniu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnPath;

public class MengNiuAfterItem {
	private static Logger log = Logger.getLogger(MengNiuAfterItem.class);

	/** 单点请求地址 */
	public static String SSO_ADDRESS = null;
	public static String APP_CODE = null;
	public static String APP_KEY = null;
	public static String CREATE_TASKS = null;
	public static String PROCESS_TASKS = null;
	public static String START_WORK_FLOW = null;
	public static String FINISH_WORK_FLOW = null;
	public static String PROCESS_CODE = null;
	public static String OA_ORG_SERVICE = null;
	public static String OA_USER_POSTION_SERVICE = null;

	public static String NOT_SYNC_DEPTS = null;

	public static void start() {
		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/mengniu/megnNiuConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);
			// 域后缀
			SSO_ADDRESS = props.getProperty("SSO_ADDRESS");
			APP_CODE = props.getProperty("APP_CODE");
			APP_KEY = props.getProperty("APP_KEY");

			// 代办接口地址
			CREATE_TASKS = props.getProperty("CREATE_TASKS");
			PROCESS_TASKS = props.getProperty("PROCESS_TASKS");
			START_WORK_FLOW = props.getProperty("START_WORK_FLOW");
			FINISH_WORK_FLOW = props.getProperty("FINISH_WORK_FLOW");
			// 处理流程唯一标识
			PROCESS_CODE = props.getProperty("PROCESS_CODE");

			// 人员同步接口
			OA_ORG_SERVICE = props.getProperty("OA_ORG_SERVICE");
			OA_USER_POSTION_SERVICE = props.getProperty("OA_USER_POSTION_SERVICE");
			NOT_SYNC_DEPTS = props.getProperty("NOT_SYNC_DEPTS");
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);

			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);

			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		}
	}
}