<table class="gradient" align="right">
         <tr class="gradient" style="color:gray;">
            <s:if test="#session.webLoginBean.isAdmin">
                 <s:if test="#application.versionType">
	               <td>&nbsp;<a class="table" style="cursor: pointer;" onclick="homePage()"><s:text name="homePage"/></a></td>
	               <td>|<a class="table" style="cursor: pointer;" onclick="personallyFunction()"><s:text name="myhome"/></a></td>
	             </s:if>
	             <s:else>
	                <td>&nbsp;<a class="table" style="cursor: pointer;" onclick="personallyFunction()"><s:text name="myhome"/></a></td>
	             </s:else>
	             <td>|<a class="table" style="cursor: pointer;" onclick="flowStruct()"><s:text name="processSystem"/></a></td>
	            
	             <s:if test="#application.isHiddenSysten">
		             <td>|<a class="table" style="cursor: pointer;" onclick="ruleSysNav()"><s:text name="guide.RuleSystem"/></a></td>
	             </s:if>
	              <s:if test="#application.isHiddenStandard">
		             <td>|<a class="table" style="cursor: pointer;" onclick="standardSysNav()"><s:text name="standardSystem"/></a></td>
	             </s:if>
	              <s:if test="#application.isHiddenRisk">
		             <td>|<a class="table" style="cursor: pointer;" onclick="riskSystemManage()"><s:text name="riskSys"/></a></td>
	             </s:if>
		             
	             <td>|<a class="table" style="cursor: pointer;" onclick="fileSysNav()"><s:text name="guide.Repository"/></a></td>
	             <td>|<a class="table" style="cursor: pointer;" onclick="orgAndPostion()"><s:text name="organizationAndTheJob"/></a></td>
	             <td>|<a class="table" style="cursor: pointer;" onclick="taskDialog()"><s:text name="taskManagement"/></a></td>
	             <s:if test="#application.versionType">
	               <td>|<a class="table" style="cursor: pointer;" onclick="rationalizationProposals()"><s:text name="rationalizationproposals"/></a></td>
	               <td>|<a class="table" style="cursor: pointer;" onclick="reportDialog()"><s:text name="theReport"/></a></td>
	             </s:if>
	             <td>|<a class="table" style="cursor: pointer;" onclick="systemManager()"><s:text name="systemManagement"/></a></td>
             </s:if>
             <s:else>
	             <td><a class="table" style="cursor: pointer;" onclick="personallyFunction()"><s:text name="myhome"/></a></td>
	             <td>|<a class="table" style="cursor: pointer;" onclick="flowStruct()"><s:text name="processSystem"/></a></td>
	             
	              <s:if test="#application.isHiddenSysten">
		             <td>|<a class="table" style="cursor: pointer;" onclick="ruleSysNav()"><s:text name="guide.RuleSystem"/></a></td>
	             </s:if>
	              <s:if test="#application.isHiddenStandard">
		              <td>|<a class="table" style="cursor: pointer;" onclick="standardSysNav()"><s:text name="standardSystem"/></a></td>
	             </s:if>
	              <s:if test="#application.isHiddenRisk">
		             <td>|<a class="table" style="cursor: pointer;" onclick="riskSystemManage()">${riskSys}</a></td>
	             </s:if>
	             <td>|<a class="table" style="cursor: pointer;" onclick="fileSysNav()"><s:text name="guide.Repository"/></a></td>
	             <td>|<a class="table" style="cursor: pointer;" onclick="orgAndPostion()"><s:text name="organizationAndTheJob"/></a></td>
	             <td>|<a class="table" style="cursor: pointer;" onclick="taskDialog()"><s:text name="taskManagement"/></a></td>
	             <s:if test="#application.versionType">
	                <td>|<a class="table" style="cursor: pointer;" onclick="rationalizationProposals()"><s:text name="rationalizationproposals"/></a></td>
	                <s:if test="#session.webLoginBean.isViewAdmin">
	                	<td>|<a class="table" style="cursor: pointer;" onclick="reportDialog()"><s:text name="theReport"/></a></td>
	                </s:if>
	             </s:if>
            </s:else>
         </tr>
</table>