package com.jecn.epros.server.service.popedom.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.DownloadPopedomBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnLineSegmentDep;
import com.jecn.epros.server.bean.popedom.JecnOpenOrgBean;
import com.jecn.epros.server.bean.popedom.JecnPositionFigInfo;
import com.jecn.epros.server.bean.popedom.JecnPositionRefSystem;
import com.jecn.epros.server.bean.popedom.PositionUpperLowerBean;
import com.jecn.epros.server.bean.popedom.PositonInfo;
import com.jecn.epros.server.bean.process.JecnFlowStructureImage;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnRefIndicators;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.system.JecnMaintainNode;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IFlowOrgImageDao;
import com.jecn.epros.server.dao.popedom.IOrganizationDao;
import com.jecn.epros.server.dao.popedom.IPositionFigInfoDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessKPIDao;
import com.jecn.epros.server.service.popedom.IOrganizationService;
import com.jecn.epros.server.service.popedom.buss.PositionDescription;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.popedom.JobDescriptionBean;
import com.jecn.epros.server.webBean.popedom.OrgListBean;
import com.jecn.epros.server.webBean.popedom.OrgRelateFlowBean;
import com.jecn.epros.server.webBean.popedom.ProcessListBean;
import com.jecn.epros.server.webBean.process.JecnRoleProcessBean;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;
import com.jecn.epros.server.webBean.process.ProcessRoleWebBean;

@Transactional(rollbackFor = Exception.class)
public class OrganizationServiceImpl extends AbsBaseService<JecnFlowOrg, Long> implements IOrganizationService {
	private Logger log = Logger.getLogger(OrganizationServiceImpl.class);

	/** 组织 */
	private IOrganizationDao organizationDao;

	/** 岗位基本信息 */
	private IPositionFigInfoDao positionFigInfoDao;
	/** 岗位组织元素表 */
	private IFlowOrgImageDao flowOrgImageDao;
	/** 流程KPI */
	private IProcessKPIDao processKPIDao;
	/** 流程元素 */
	private IFlowStructureDao flowStructureDao;
	/** 文件表 */
	private IFileDao fileDao;

	public IFileDao getFileDao() {
		return fileDao;
	}

	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
	}

	public IPositionFigInfoDao getPositionFigInfoDao() {
		return positionFigInfoDao;
	}

	public void setPositionFigInfoDao(IPositionFigInfoDao positionFigInfoDao) {
		this.positionFigInfoDao = positionFigInfoDao;
	}

	public IOrganizationDao getOrganizationDao() {
		return organizationDao;
	}

	public void setOrganizationDao(IOrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
		this.baseDao = organizationDao;
	}

	public void setFlowOrgImageDao(IFlowOrgImageDao flowOrgImageDao) {
		this.flowOrgImageDao = flowOrgImageDao;
	}

	public IProcessKPIDao getProcessKPIDao() {
		return processKPIDao;
	}

	public void setProcessKPIDao(IProcessKPIDao processKPIDao) {
		this.processKPIDao = processKPIDao;
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：组织子节点通用查询
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjects(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectOrg(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getChildOrgs(Long pId, Long projectId) throws Exception {
		try {
			String sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where del_state=0 and per_org_id=jfo.org_id ) as count"
					+ " ,jfo.ORG_NUMBER"
					+ " from jecn_flow_org jfo where jfo.per_org_id=? and jfo.projectid=? and jfo.del_state=0"
					+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
			List<Object[]> list = organizationDao.listNativeSql(sql, pId, projectId);

			return getOrgResultBean(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private List<JecnTreeBean> getOrgResultBean(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = getMoveJecnTreeBeanByObjectOrg(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception {
		List<Object[]> list = organizationDao.getRoleAuthChildFiles(pId, projectId, peopleId);
		return getOrgResultBean(list);
	}

	/***************************************************************************
	 * @description：组织对象转换成树对象
	 * @param obj
	 * @return
	 */
	private JecnTreeBean getMoveJecnTreeBeanByObjectOrg(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[4] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();

		// 组织节点ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 组织节点名称
		jecnTreeBean.setName(obj[1].toString());
		// 组织节点父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
		// 组织节点父类名称
		jecnTreeBean.setPname("");
		// 组织节点组织编号
		jecnTreeBean.setNumberId(obj[4].toString());
		jecnTreeBean.setTreeNodeType(TreeNodeType.organization);
		if (Integer.parseInt(obj[3].toString()) > 0) {
			// 组织节点是否有子节点
			jecnTreeBean.setChildNode(true);
		} else {
			jecnTreeBean.setChildNode(false);
		}
		return jecnTreeBean;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getAllOrgs(Long projectId) throws Exception {
		try {
			String sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where del_state=0 and per_org_id=jfo.org_id ) as count"
					+ " from jecn_flow_org jfo where jfo.projectid=? and jfo.del_state=0"
					+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
			List<Object[]> list = organizationDao.listNativeSql(sql, projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getPnodesPos(Long orgId, Long projectId) throws Exception {
		try {
			// 部门
			List<Object[]> listOrg = organizationDao.getSearchOrgIds(orgId, projectId);

			// 查定位的岗位的兄弟节点包括岗位和部门,查询方式为根据要定位的岗位的部门ID查询子节点
			List<JecnTreeBean> listPosAndOrg = getChildOrgsAndPositon(orgId, projectId, true);
			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			for (Object[] obj : listOrg) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanFromObjectOrg(obj);
				if (jecnTreeBean != null) {
					listResult.add(jecnTreeBean);
				}
			}
			listResult.addAll(listPosAndOrg);
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getPnodesOrg(Long orgId, Long projectId) throws Exception {
		try {
			List<Object[]> list = organizationDao.getSearchOrgIds(orgId, projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getChildOrgsAndPositon(Long id, Long projectId, boolean isLeaf) throws Exception {
		try {
			String sql = "select distinct  tt.org_id, tt.org_name, tt.per_org_id,"
					+ "case when  org2.org_id is null then 0 else 1 end as org_count ,"
					+ "case when  image.figure_id  is null then 0 else 1 end  as  pos_count,tt.sort_id,"
					+ "  tt.org_number" + "  from jecn_flow_org tt"
					+ "  left join jecn_flow_org org2 on tt.org_id = org2.per_org_id" + "  and org2.del_state = 0"
					+ "  left join jecn_flow_org_image image on tt.org_id = image.org_id"
					+ "  and image.figure_type = 'PositionFigure'" + "  where tt.per_org_id = ?"
					+ "  and tt.projectid = ?" + "  and tt.del_state = 0"
					+ " order by tt.per_org_id, tt.sort_id, tt.org_id";

			// 组织
			List<Object[]> listOrg = organizationDao.listNativeSql(sql, id, projectId);
			// 岗位
			List<Object[]> listPos = null;
			if (isLeaf) {
				listPos = organizationDao.getPosAndPersonByOrgId(id, projectId);
			} else {
				listPos = organizationDao.getPosByOrgId(id, projectId);
			}

			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			for (Object[] obj : listOrg) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectOrg(obj);
				if (jecnTreeBean != null) {
					listResult.add(jecnTreeBean);
				}
			}
			for (Object[] obj : listPos) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
				if (jecnTreeBean != null) {
					if (JecnConfigTool.isIflytekLogin()) {// 科大讯飞不显示人员
						jecnTreeBean.setChildNode(false);
					} else {
						if (isLeaf) {
							if (obj[5] != null && Integer.parseInt(obj[5].toString()) > 0) {
								jecnTreeBean.setChildNode(true);
							} else {
								jecnTreeBean.setChildNode(false);
							}
						} else {
							jecnTreeBean.setChildNode(false);
						}
					}
					listResult.add(jecnTreeBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getGroups(Long orgId, Long projectId) throws Exception {

		try {
			// 树集合（存放岗位组、文件夹、岗位）
			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			String sql = "select t.id,t.name,t.per_id,t.is_dir from jecn_position_group t where t.project_id=? and per_id=?";
			// 执行查询SQL,获取岗位组
			List<Object[]> listGroup = organizationDao.listNativeSql(sql, projectId, orgId);
			// 遍历岗位存放到树集合中
			for (Object[] obj : listGroup) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectGroup(obj);
				if (jecnTreeBean != null) {
					listResult.add(jecnTreeBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public Long addOrg(String name, Long pId, String orgNumber, Long projectId, int sortId, String orgNum)
			throws Exception {
		try {
			JecnFlowOrg jecnFlowOrg = new JecnFlowOrg();
			jecnFlowOrg.setOrgName(name);
			jecnFlowOrg.setPreOrgId(pId);
			// 添加父组织编号
			jecnFlowOrg.setPreOrgNumber(orgNumber);
			jecnFlowOrg.setProjectId(projectId);
			jecnFlowOrg.setOccupier(0L);
			jecnFlowOrg.setDelState(0L);
			jecnFlowOrg.setSortId(Long.valueOf(sortId));
			jecnFlowOrg.setOrgNumber(orgNum);
			Long id = organizationDao.save(jecnFlowOrg);
			// 更新tpath 和tlevel
			updateTpathAndTLevel(jecnFlowOrg);
			return id;

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 更新tpath 和tlevel
	 * 
	 */
	private void updateTpathAndTLevel(JecnFlowOrg jecnFlowOrg) throws Exception {
		// 获取上级节点
		JecnFlowOrg preFlow = organizationDao.get(jecnFlowOrg.getPreOrgId());
		if (preFlow == null) {
			jecnFlowOrg.settLevel(0);
			jecnFlowOrg.settPath(jecnFlowOrg.getOrgId() + "-");
			jecnFlowOrg.setViewSort(JecnUtil.concatViewSort("", jecnFlowOrg.getSortId().intValue()));
		} else {
			jecnFlowOrg.settLevel(preFlow.gettLevel() + 1);
			jecnFlowOrg.settPath(preFlow.gettPath() + jecnFlowOrg.getOrgId() + "-");
			jecnFlowOrg.setViewSort(JecnUtil.concatViewSort(preFlow.getViewSort(), jecnFlowOrg.getSortId().intValue()));
		}
		// 更新
		organizationDao.update(jecnFlowOrg);
		organizationDao.getSession().flush();
	}

	@Override
	public Long addPosition(JecnFlowOrgImage jecnFlowOrgImage) throws Exception {
		try {
			// 获取岗位编码，设计器端生成岗位时，组织节点未存储岗位编码
			JecnFlowOrg flowOrg = organizationDao.get(jecnFlowOrgImage.getOrgId());
			jecnFlowOrgImage.setOrgNumberId(flowOrg.getOrgNumber());
			organizationDao.getSession().save(jecnFlowOrgImage);
			JecnPositionFigInfo jecnPositionFigInfo = new JecnPositionFigInfo();
			jecnPositionFigInfo.setFigureId(jecnFlowOrgImage.getFigureId());
			positionFigInfoDao.save(jecnPositionFigInfo);
			return jecnFlowOrgImage.getFigureId();
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 获得上级岗位和下级岗位
	 * 
	 * @param upPositionList
	 * @param lowerPositionList
	 * @param posId
	 */
	private void getUpperLowerPos(List<JecnTreeBean> upPositionList, List<JecnTreeBean> lowerPositionList, Long posId) {
		// 上级岗位、下属岗位
		String sql = "select t.leve_position_id,(select j.figure_text from jecn_flow_org_image j where j.figure_id=t.leve_position_id) as name,"
				+ "      t.leve_type"
				+ ",      (select jo.org_id from jecn_flow_org jo,jecn_flow_org_image ji where ji.figure_id=t.leve_position_id and jo.org_id = ji.org_id  ) as pid,"
				+ "     (select jo.org_name from jecn_flow_org jo,jecn_flow_org_image ji where ji.figure_id=t.leve_position_id and jo.org_id = ji.org_id ) as pname "
				+ "    from jecn_position_upper_lower t where t.position_id=?";
		List<Object[]> listPosUpperLower = organizationDao.listNativeSql(sql, posId);
		for (Object[] obj : listPosUpperLower) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[4] == null) {
				continue;
			}
			JecnTreeBean tb = new JecnTreeBean();
			tb.setId(Long.valueOf(obj[0].toString()));
			tb.setName(obj[1].toString());
			tb.setPid(Long.valueOf(obj[3].toString()));
			tb.setPname(obj[4].toString());
			if ("0".equals(obj[2].toString())) {
				upPositionList.add(tb);
			} else {
				lowerPositionList.add(tb);
			}
		}
	}

	/**
	 * 获得岗位部门内的相关制度
	 * 
	 * @param inListRule
	 * @param outListRule
	 * @param posId
	 * @param isPub
	 */
	private void orgRelatesRule(List<JecnTreeBean> inListRule, List<JecnTreeBean> outListRule, Long posId, boolean isPub) {
		String sql = "";
		if (isPub) {
			sql = "select jr.system_id,t.rule_name,jr.type,t.is_dir,t.file_id"
					+ "     from jecn_position_ref_system jr,jecn_rule t"
					+ "     where jr.system_id =t.id and t.del_state=0 and jr.position_id=?";
		} else {
			sql = "select jr.system_id,t.rule_name,jr.type,t.is_dir,t.file_id"
					+ "     from jecn_position_ref_system jr,jecn_rule_t t"
					+ "     where jr.system_id =t.id and t.del_state=0 and jr.position_id=?";
		}
		List<Object[]> objSys = organizationDao.listNativeSql(sql, posId);
		for (Object[] obj : objSys) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}
			JecnTreeBean tb = new JecnTreeBean();
			tb.setId(Long.valueOf(obj[0].toString()));
			tb.setName(obj[1].toString());
			// 类型
			tb.setRelationId(Long.valueOf(obj[3].toString()));
			// 文件ID
			if (obj[4] != null) {
				tb.setModeId(Long.valueOf(obj[4].toString()));
			}
			if ("0".equals(obj[2].toString())) {
				inListRule.add(tb);
			} else {
				outListRule.add(tb);
			}
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public PositonInfo getPositionInfo(Long id) throws Exception {
		try {
			PositonInfo positonInfo = new PositonInfo();
			JecnPositionFigInfo jecnPositionFigInfo = (JecnPositionFigInfo) organizationDao.getSession().get(
					JecnPositionFigInfo.class, id);
			if (jecnPositionFigInfo == null) {
				jecnPositionFigInfo = new JecnPositionFigInfo();
				jecnPositionFigInfo.setFigureId(id);
				this.organizationDao.getSession().save(jecnPositionFigInfo);
			}
			// 获取岗位说明书附件
			Long fileId = jecnPositionFigInfo.getFileId();
			jecnPositionFigInfo.setFileName("");
			if (fileId != null) {
				JecnFileBeanT fileBean = fileDao.get(fileId);
				if (fileBean != null && fileBean.getDelState() != null && fileBean.getDelState().intValue() == 0) {
					if (fileBean.getFileName() != null) {
						jecnPositionFigInfo.setFileName(fileBean.getFileName());
					}
				}
			}
			positonInfo.setPositionBean(jecnPositionFigInfo);

			List<JecnTreeBean> upPositionList = new ArrayList<JecnTreeBean>();
			List<JecnTreeBean> lowerPositionList = new ArrayList<JecnTreeBean>();
			this.getUpperLowerPos(upPositionList, lowerPositionList, id);

			positonInfo.setUpPositionList(upPositionList);
			positonInfo.setLowerPositionList(lowerPositionList);
			// 部门的相关制度
			List<JecnTreeBean> inListRule = new ArrayList<JecnTreeBean>();
			List<JecnTreeBean> outListRule = new ArrayList<JecnTreeBean>();
			this.orgRelatesRule(inListRule, outListRule, id, false);
			positonInfo.setInListRule(inListRule);
			positonInfo.setOutListRule(outListRule);
			return positonInfo;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void reNameOrg(Long id, String newName) throws Exception {
		try {
			JecnFlowOrg jecnFlowOrg = organizationDao.get(id);
			jecnFlowOrg.setOrgName(newName);
			organizationDao.update(jecnFlowOrg);
			String hql = "from JecnFlowOrgImage where linkOrgId = ? and figureType='Rect'";
			List<JecnFlowOrgImage> listJecnFlowOrgImage = organizationDao.listHql(hql, id);
			for (JecnFlowOrgImage jecnFlowOrgImage : listJecnFlowOrgImage) {
				jecnFlowOrgImage.setFigureText(newName);
				organizationDao.getSession().update(jecnFlowOrgImage);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void reNamePosition(Long id, String newName) throws Exception {
		try {
			JecnFlowOrgImage jecnFlowOrgImage = (JecnFlowOrgImage) organizationDao.getSession().get(
					JecnFlowOrgImage.class, id);
			String oldPosName = jecnFlowOrgImage.getFigureText();
			jecnFlowOrgImage.setFigureText(newName);
			organizationDao.getSession().update(jecnFlowOrgImage);
			// 角色图标 设置岗位(临时)
			String hql = "select jf from JecnFlowStructureImageT as jf,JecnFlowStationT as js "
					+ "where jf.figureId = js.figureFlowId and js.type='0' and js.figurePositionId=? and jf.figureType IN "
					+ JecnCommonSql.getOnlyRoleString();
			List<JecnFlowStructureImageT> listJecnFlowStructureImageT = organizationDao.listHql(hql, id);
			for (JecnFlowStructureImageT jecnFlowStructureImageT : listJecnFlowStructureImageT) {
				if (oldPosName.equals(jecnFlowStructureImageT.getFigureText())) {
					jecnFlowStructureImageT.setFigureText(newName);
					organizationDao.getSession().update(jecnFlowStructureImageT);
				}
			}
			// 角色图标 设置岗位
			hql = "select jf from JecnFlowStructureImage as jf,JecnFlowStation as js "
					+ "where jf.figureId = js.figureFlowId and js.type='0' and js.figurePositionId=? and jf.figureType IN "
					+ JecnCommonSql.getOnlyRoleString();
			List<JecnFlowStructureImage> listJecnFlowStructureImage = organizationDao.listHql(hql, id);
			for (JecnFlowStructureImage jecnFlowStructureImage : listJecnFlowStructureImage) {
				if (oldPosName.equals(jecnFlowStructureImage.getFigureText())) {
					jecnFlowStructureImage.setFigureText(newName);
					organizationDao.getSession().update(jecnFlowStructureImage);
				}
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId) throws Exception {
		try {

			String pViewSort = "";
			if (pId.intValue() != 0) {
				String hql = "from JecnFlowOrg where orgId=?";
				List<JecnFlowOrg> data = organizationDao.listHql(hql, pId);
				pViewSort = data.get(0).getViewSort();
			}

			String hql = "from JecnFlowOrg where preOrgId=? and projectId=? order by sortId";
			List<JecnFlowOrg> listJecnFlowOrg = organizationDao.listHql(hql, pId, projectId);
			List<JecnTreeDragBean> listPos = new ArrayList<JecnTreeDragBean>();
			// 组织
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				if (TreeNodeType.organization.equals(jecnTreeDragBean.getTreeNodeType())) {
					for (JecnFlowOrg jecnFlowOrg : listJecnFlowOrg) {
						if (jecnTreeDragBean.getId().equals(jecnFlowOrg.getOrgId())) {
							jecnFlowOrg.setSortId(Long.valueOf(jecnTreeDragBean.getSortId()));
							jecnFlowOrg.setViewSort(JecnUtil.concatViewSort(pViewSort, jecnTreeDragBean.getSortId()));
							organizationDao.update(jecnFlowOrg);
							break;
						}
					}
				} else if (TreeNodeType.position.equals(jecnTreeDragBean.getTreeNodeType())) {
					listPos.add(jecnTreeDragBean);
				}
			}
			if (listPos.size() > 0) {
				// 岗位
				hql = "from JecnFlowOrgImage where figureType='PositionFigure' and orgId=? order by sortId";
				List<JecnFlowOrgImage> listJecnFlowOrgImage = organizationDao.listHql(hql, pId);
				for (JecnTreeDragBean jecnTreeDragBean : listPos) {
					for (JecnFlowOrgImage jecnFlowOrgImage : listJecnFlowOrgImage) {
						if (jecnTreeDragBean.getId().equals(jecnFlowOrgImage.getFigureId())) {
							jecnFlowOrgImage.setSortId(Long.valueOf(jecnTreeDragBean.getSortId()));
							organizationDao.getSession().update(jecnFlowOrgImage);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void moveNodesOrg(List<Long> listIds, Long pId, String orgNumber, Long updatePersonId,
			TreeNodeType moveNodeType) throws Exception {

		// 判断参数是否正确
		if (listIds == null || listIds.size() == 0 || pId == null || orgNumber == null || "".equals(orgNumber)) {
			log.error("参数不合法" + this.getClass() + "moveNodesOrg");
			throw new Exception("参数不合法");
		}
		try {
			String t_path = "";
			int level = -1;
			if (pId.intValue() != 0) {
				JecnFlowOrg jecnFlowOrg = this.get(pId);
				t_path = jecnFlowOrg.gettPath();
				level = jecnFlowOrg.gettLevel();
			}
			JecnCommon.updateNodesChildTreeBeans(listIds, pId, orgNumber, new Date(), updatePersonId, t_path, level,
					organizationDao, moveNodeType, 1);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void moveNodesPos(List<Long> listIds, Long pId, String orgNumber, Long updatePersonId,
			TreeNodeType moveNodeType) throws Exception {
		// 判断参数是否正确
		if (listIds == null || listIds.size() == 0 || pId == null || orgNumber == null || "".equals(orgNumber)) {
			log.error("参数不合法" + this.getClass() + "moveNodesOrg");
			throw new Exception("参数不合法");
		}
		try {

			JecnCommon.updateNodesChildTreeBeans(listIds, pId, orgNumber, new Date(), updatePersonId, "", 1,
					organizationDao, moveNodeType, 1);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void deleteOrgs(List<Long> listIds, Long updatePersonId) throws Exception {
		try {

			String sql = "update Jecn_Flow_Org set del_State=1 where org_Id in"
					+ JecnCommonSql.getChildObjectsSqlByTypeDelAddBracket(listIds, TreeNodeType.organization);
			organizationDao.execteNative(sql);
			// 添加组织
			JecnUtil.saveJecnJournals(listIds, 6, 6, updatePersonId, organizationDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void deletePosition(List<Long> listIds) throws Exception {
		try {
			/** 岗位与人员的关系表 */
			String hql = "delete from JecnUserInfo where figureId in";
			executeHqlList(hql, listIds);

			/** 岗位与流程中角色的关系 浏览端 */
			hql = "delete from JecnFlowStation where  type = '0' and figurePositionId in";
			executeHqlList(hql, listIds);

			/** 岗位与流程中角色的关系 设计器 */
			hql = "delete from JecnFlowStationT where  type = '0' and figurePositionId in";
			executeHqlList(hql, listIds);

			/** 岗位与岗位组的关系表 */
			hql = "delete from JecnPositionGroupOrgRelated where figureId in";
			executeHqlList(hql, listIds);

			/** 岗位的基本信息表 */
			hql = "delete from JecnPositionFigInfo where figureId in";
			executeHqlList(hql, listIds);

			/** 岗位与制度的关系表 */
			hql = "delete from JecnPositionRefSystem where positionId in";
			executeHqlList(hql, listIds);

			/** 删除上级岗位、上属岗位 */
			hql = "delete from PositionUpperLowerBean where positionId in";
			executeHqlList(hql, listIds);

			/** 删除上级岗位、上属岗位 */
			hql = "delete from PositionUpperLowerBean where levePositionId in";
			executeHqlList(hql, listIds);

			/** 查阅权限中岗位 */
			hql = "delete from JecnAccessPermissions where figureId in";
			executeHqlList(hql, listIds);

			/** 删除岗位对象 */
			hql = "delete from JecnFlowOrgImage where figureId in";
			executeHqlList(hql, listIds);

		} catch (Exception e) {
			log.error("删除岗位及岗位关系表出现异常", e);
			throw e;
		}
	}

	/**
	 * 执行当前Dao增删改hql 用于操作Id集合
	 * 
	 * @author weidp
	 * @date 2014-9-30 上午09:33:00
	 * @param hql
	 * @param listIds
	 *            Id集合
	 */
	private void executeHqlList(String hql, List<Long> listIds) {
		List<String> hqlList = JecnCommonSql.getListSqls(hql, listIds);
		for (String hqlStr : hqlList) {
			organizationDao.execteHql(hqlStr);
		}
	}

	@Override
	public void updatePosition(JecnPositionFigInfo positionInfoBean, String upPositionIds, String lowerPositionIds,
			String innerRuleIds, String outRuleIds, Long posId, String posName, String posNum) throws Exception {
		try {
			this.positionFigInfoDao.saveOrUpdate(positionInfoBean);
			// 不需要删除的id集合
			List<Long> listIds = new ArrayList<Long>();

			// 上级岗位
			// 新添加对象集合
			List<PositionUpperLowerBean> listSave = new ArrayList<PositionUpperLowerBean>();

			List<PositionUpperLowerBean> listPositionUpper = null;
			if (upPositionIds != null && !"".equals(upPositionIds)) {
				String hql = "from PositionUpperLowerBean where leveType=0 and positionId=?";
				listPositionUpper = positionFigInfoDao.listHql(hql, positionInfoBean.getFigureId());
				String[] idsArr = upPositionIds.split(",");
				for (String str : idsArr) {
					if (str == null || "".equals(str)) {
						continue;
					}
					boolean isExist = false;
					// 判断关系是否已存在
					for (PositionUpperLowerBean positionUpperLowerBean : listPositionUpper) {
						if (str.equals(positionUpperLowerBean.getLevePositionId().toString())) {
							isExist = true;
							break;
						}
					}
					if (isExist) {
						listIds.add(Long.valueOf(str));
					} else {
						PositionUpperLowerBean positionUpperLowerBean = new PositionUpperLowerBean();
						positionUpperLowerBean.setLeveType(Long.valueOf(0));
						positionUpperLowerBean.setPositionId(positionInfoBean.getFigureId());
						positionUpperLowerBean.setLevePositionId(Long.valueOf(str));
						listSave.add(positionUpperLowerBean);
					}
				}
			}

			if (listIds.size() > 0) {
				// 删除消取关联的数据
				String hql = "delete from PositionUpperLowerBean where leveType=0 and positionId=? and levePositionId not in"
						+ JecnCommonSql.getIds(listIds);
				positionFigInfoDao.execteHql(hql, positionInfoBean.getFigureId());
			} else {
				if (listPositionUpper == null || listPositionUpper.size() > 0) {
					// 删除此岗位下的所有上属岗位
					String hql = "delete from PositionUpperLowerBean where leveType=0 and positionId=?";
					positionFigInfoDao.execteHql(hql, positionInfoBean.getFigureId());
				}
			}
			for (PositionUpperLowerBean positionUpperLowerBean : listSave) {
				positionFigInfoDao.getSession().save(positionUpperLowerBean);
			}
			// 下属岗位
			listIds = new ArrayList<Long>();
			listSave = new ArrayList<PositionUpperLowerBean>();
			// 数据库现有下属岗位
			List<PositionUpperLowerBean> listPositionLower = null;
			if (lowerPositionIds != null && !"".equals(lowerPositionIds)) {
				String hql = "from PositionUpperLowerBean where leveType=1 and positionId=?";
				listPositionLower = positionFigInfoDao.listHql(hql, positionInfoBean.getFigureId());
				String[] idsArr = lowerPositionIds.split(",");
				for (String str : idsArr) {
					if (str == null || "".equals(str)) {
						continue;
					}
					boolean isExist = false;
					for (PositionUpperLowerBean positionUpperLowerBean : listPositionLower) {
						if (str.equals(positionUpperLowerBean.getLevePositionId().toString())) {
							isExist = true;
							break;
						}
					}
					if (isExist) {
						listIds.add(Long.valueOf(str));
					} else {
						PositionUpperLowerBean positionUpperLowerBean = new PositionUpperLowerBean();
						positionUpperLowerBean.setLeveType(Long.valueOf(1));
						positionUpperLowerBean.setPositionId(positionInfoBean.getFigureId());
						positionUpperLowerBean.setLevePositionId(Long.valueOf(str));
						listSave.add(positionUpperLowerBean);
					}
				}
			}
			if (listIds.size() > 0) {
				String hql = "delete from PositionUpperLowerBean where leveType=1 and positionId=? and levePositionId not in"
						+ JecnCommonSql.getIds(listIds);
				positionFigInfoDao.execteHql(hql, positionInfoBean.getFigureId());
			} else {
				if (listPositionLower == null || listPositionLower.size() > 0) {
					String hql = "delete from PositionUpperLowerBean where leveType=1 and positionId=?";
					positionFigInfoDao.execteHql(hql, positionInfoBean.getFigureId());
				}
			}
			for (PositionUpperLowerBean positionUpperLowerBean : listSave) {
				positionFigInfoDao.getSession().save(positionUpperLowerBean);
			}
			// 部门内制度
			// 和数据库数据作比较，把已存在数据增加
			listIds = new ArrayList<Long>();
			List<JecnPositionRefSystem> listSaveJecnPositionRefSystem = new ArrayList<JecnPositionRefSystem>();
			List<JecnPositionRefSystem> listJecnPositionRefSystem = null;
			if (innerRuleIds != null && !"".equals(innerRuleIds)) {
				String hql = "from JecnPositionRefSystem where type='0' and positionId=?";
				listJecnPositionRefSystem = positionFigInfoDao.listHql(hql, positionInfoBean.getFigureId());
				String[] idsArr = innerRuleIds.split(",");
				for (String str : idsArr) {
					if (str == null || "".equals(str)) {
						continue;
					}
					boolean isExist = false;
					for (JecnPositionRefSystem jecnPositionRefSystem : listJecnPositionRefSystem) {
						if (str.equals(jecnPositionRefSystem.getSystemId().toString())) {
							isExist = true;
							break;
						}
					}
					if (isExist) {
						listIds.add(Long.valueOf(str));
					} else {
						JecnPositionRefSystem jecnPositionRefSystem = new JecnPositionRefSystem();
						jecnPositionRefSystem.setType("0");
						jecnPositionRefSystem.setPositionId(positionInfoBean.getFigureId());
						jecnPositionRefSystem.setSystemId(Long.valueOf(str));
						listSaveJecnPositionRefSystem.add(jecnPositionRefSystem);
					}
				}
			}
			if (listIds.size() > 0) {
				String hql = "delete from JecnPositionRefSystem where type='0' and positionId=? and systemId not in"
						+ JecnCommonSql.getIds(listIds);
				positionFigInfoDao.execteHql(hql, positionInfoBean.getFigureId());
			} else if (listJecnPositionRefSystem == null || listJecnPositionRefSystem.size() > 0) {
				String hql = "delete from JecnPositionRefSystem where type='0' and positionId=?";
				positionFigInfoDao.execteHql(hql, positionInfoBean.getFigureId());
			}

			for (JecnPositionRefSystem jecnPositionRefSystem : listSaveJecnPositionRefSystem) {
				positionFigInfoDao.getSession().save(jecnPositionRefSystem);
			}

			// 部门外制度
			listIds = new ArrayList<Long>();
			listSaveJecnPositionRefSystem = new ArrayList<JecnPositionRefSystem>();
			listJecnPositionRefSystem = null;
			if (outRuleIds != null && !"".equals(outRuleIds)) {
				String hql = "from JecnPositionRefSystem where type='1' and positionId=?";
				listJecnPositionRefSystem = positionFigInfoDao.listHql(hql, positionInfoBean.getFigureId());
				String[] idsArr = outRuleIds.split(",");
				for (String str : idsArr) {
					if (str == null || "".equals(str)) {
						continue;
					}
					boolean isExist = false;
					for (JecnPositionRefSystem jecnPositionRefSystem : listJecnPositionRefSystem) {
						if (str.equals(jecnPositionRefSystem.getSystemId().toString())) {
							isExist = true;
							break;
						}
					}
					if (isExist) {
						listIds.add(Long.valueOf(str));
					} else {
						JecnPositionRefSystem jecnPositionRefSystem = new JecnPositionRefSystem();
						jecnPositionRefSystem.setType("1");
						jecnPositionRefSystem.setPositionId(positionInfoBean.getFigureId());
						jecnPositionRefSystem.setSystemId(Long.valueOf(str));
						listSaveJecnPositionRefSystem.add(jecnPositionRefSystem);
					}
				}
			}
			if (listIds.size() > 0) {
				String hql = "delete from JecnPositionRefSystem where type='1' and positionId=? and systemId not in"
						+ JecnCommonSql.getIds(listIds);
				positionFigInfoDao.execteHql(hql, positionInfoBean.getFigureId());
			} else if (listJecnPositionRefSystem == null || listJecnPositionRefSystem.size() > 0) {
				String hql = "delete from JecnPositionRefSystem where type='1' and positionId=?";
				positionFigInfoDao.execteHql(hql, positionInfoBean.getFigureId());
			}
			for (JecnPositionRefSystem jecnPositionRefSystem : listSaveJecnPositionRefSystem) {
				positionFigInfoDao.getSession().save(jecnPositionRefSystem);
			}

			// 岗位名称，编号更新
			JecnFlowOrgImage jecnFlowOrgImage = (JecnFlowOrgImage) organizationDao.getSession().get(
					JecnFlowOrgImage.class, posId);
			String oldPosName = jecnFlowOrgImage.getFigureText();
			jecnFlowOrgImage.setFigureText(posName);
			jecnFlowOrgImage.setFigureNumberId(posNum);
			organizationDao.getSession().update(jecnFlowOrgImage);
			// 角色图标 设置岗位(临时)
			String hql = "select jf from JecnFlowStructureImageT as jf,JecnFlowStationT as js "
					+ "where jf.figureId = js.figureFlowId and js.type='0' and js.figurePositionId=? and jf.figureType in"
					+ JecnCommonSql.getOnlyRoleString();
			List<JecnFlowStructureImageT> listJecnFlowStructureImageT = organizationDao.listHql(hql, posId);
			for (JecnFlowStructureImageT jecnFlowStructureImageT : listJecnFlowStructureImageT) {
				if (oldPosName.equals(jecnFlowStructureImageT.getFigureText())) {
					jecnFlowStructureImageT.setFigureText(posName);
					organizationDao.getSession().update(jecnFlowStructureImageT);
				}
			}
			// 角色图标 设置岗位
			hql = "select jf from JecnFlowStructureImage as jf,JecnFlowStation as js "
					+ "where jf.figureId = js.figureFlowId and js.type='0' and js.figurePositionId=? and jf.figureType in"
					+ JecnCommonSql.getOnlyRoleString();
			List<JecnFlowStructureImage> listJecnFlowStructureImage = organizationDao.listHql(hql, posId);
			for (JecnFlowStructureImage jecnFlowStructureImage : listJecnFlowStructureImage) {
				if (oldPosName.equals(jecnFlowStructureImage.getFigureText())) {
					jecnFlowStructureImage.setFigureText(posName);
					organizationDao.getSession().update(jecnFlowStructureImage);
				}
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	@Override
	public void updateOrgDuty(Long orgId, String duty, String orgNum) throws Exception {
		try {
			JecnFlowOrg jecnFlowOrg = this.organizationDao.get(orgId);
			jecnFlowOrg.setOrgRespon(duty);
			jecnFlowOrg.setOrgNumber(orgNum);
			organizationDao.update(jecnFlowOrg);

			// 修改组织下的直接子节点，首先修改组织
			String orgSql = "UPDATE JECN_FLOW_ORG SET " + "PREORG_NUMBER = '" + orgNum + "' WHERE PER_ORG_ID = "
					+ orgId;
			String posSql = "UPDATE JECN_FLOW_ORG_IMAGE SET ORG_NUMBER_ID = '" + orgNum + "' WHERE ORG_ID = " + orgId;
			// 执行部门更新
			organizationDao.execteNative(orgSql);
			// 执行岗位更新
			organizationDao.execteNative(posSql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchByName(String name, Long projectId) throws Exception {
		try {
			String sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count"
					+ " from jecn_flow_org jfo where jfo.org_name like ? and jfo.projectid=? and jfo.del_state=0"
					+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
			List<Object[]> list = organizationDao.listNativeSql(sql, 0, 100, "%" + name + "%", projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchPositionByName(String name, Long projectId) throws Exception {
		try {
			// 岗位
			String sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name"
					+ "        from jecn_flow_org_image t,jecn_flow_org jfo where t.figure_text like ? and t.org_id=jfo.org_id and jfo.del_state=0"
					+ "        and jfo.projectid=? and t.figure_type = 'PositionFigure' order by t.org_id,t.sort_id";
			List<Object[]> listPos = organizationDao.listNativeSql(sql, 0, 40, "%" + name + "%", projectId);
			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			for (Object[] obj : listPos) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
				if (jecnTreeBean != null) {
					listResult.add(jecnTreeBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchPositionByName(Map<String, Object> nameMap, Long projectId) throws Exception {
		try {
			String posName = nameMap.get("posName") != null ? nameMap.get("posName").toString() : "";
			String deptName = nameMap.get("deptName") != null ? nameMap.get("deptName").toString() : "";
			String peopleName = nameMap.get("peopleName") != null ? nameMap.get("peopleName").toString() : "";
			StringBuilder str = new StringBuilder();
			if (StringUtils.isNotBlank(deptName)) {
				str.append("and jfo.org_name like '%" + deptName + "%'");
			}
			if (JecnConfigTool.isIflytekLogin()) {
				List<String> peopleNames = (List<String>) nameMap.get("peopleNames");
				if (peopleNames != null && !peopleNames.isEmpty()) {
					str.append(" and ju.LOGIN_NAME in " + JecnCommonSql.getStrs(peopleNames));
				}
			} else {
				if (StringUtils.isNotBlank(peopleName)) {
					str.append(" and ju.true_name like   '%" + peopleName + "%'");
				}
			}
			// 岗位
			String sql = "select distinct pos.figure_id," + " pos.figure_text," + "                t.org_id,"
					+ "                t.org_name" + "  from jecn_flow_org_image pos" + " inner join jecn_flow_org jfo"
					+ "      on jfo.del_state = 0" + " inner join jecn_flow_org t" + "   on "
					+ JecnCommonSqlTPath.INSTANCE.getSqlSubStr("jfo") + "  and pos.org_id = t.org_id "
					+ "  left join jecn_user_position_related u" + "    on pos.figure_id = u.figure_id"
					+ "  left join jecn_user ju" + "  on ju.people_id = u.people_id  and ju.islock = 0 "
					+ " where pos.figure_text like ? " + str.toString() + "   and jfo.projectid = ?"
					+ "   and pos.figure_type = 'PositionFigure'" + " order by t.org_id";
			List<Object[]> listPos = organizationDao.listNativeSql(sql, 0, 100, "%" + posName + "%", projectId);
			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			for (Object[] obj : listPos) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
				if (jecnTreeBean != null) {
					listResult.add(jecnTreeBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchGroupByName(String name, Long projectId) throws Exception {
		try {
			String sql = "select t.id,t.name,t.project_id,t.per_id,t.is_dir from jecn_position_group t where  t.name like ? and t.project_id=? and is_dir=0";
			List<Object[]> listPos = organizationDao.listNativeSql(sql, 0, 20, "%" + name + "%", projectId);
			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			for (Object[] obj : listPos) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
				if (jecnTreeBean != null) {
					listResult.add(jecnTreeBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public Set<Long> deleteIdsOrg(List<Long> ListIds, Long updatePeopleId) throws Exception {
		try {
			Set<Long> setOrgIds = new HashSet<Long>();
			String sql = JecnCommonSql.getChildObjectsSqlByType(ListIds, TreeNodeType.organization);
			List<Object[]> listAll = organizationDao.listNativeSql(sql);
			for (Object[] obj : listAll) {
				if (obj == null || obj[0] == null || obj[1] == null) {
					continue;
				}
				setOrgIds.add(Long.valueOf(obj[0].toString()));
			}
			if (setOrgIds.size() > 0) {
				organizationDao.deleteOrgs(setOrgIds);
				// 添加日志
				JecnUtil.saveJecnJournals(setOrgIds, 6, 3, updatePeopleId, organizationDao);
			}
			return setOrgIds;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	@Override
	public List<Object[]> getDelsOrg(Long projectId) throws Exception {
		try {
			String hql = "select orgId,orgName,preOrgId from JecnFlowOrg where delState =1 and projectId=? order by preOrgId,sortId,orgId";
			return organizationDao.listHql(hql, projectId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateRecoverDelOrg(List<Long> ids) throws Exception {
		try {
			if (ids != null && ids.size() > 0) {
				String hql = "update JecnFlowOrg  set delState=0 where orgId in " + JecnCommonSql.getIds(ids);
				organizationDao.execteHql(hql);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getParentsContainSelf(Long projectId, Long id) throws Exception {
		try {
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sql = "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
						+ id
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.PER_ORG_ID=JFS.ORG_ID)"
						+ " ,MY_JECN AS(SELECT * FROM JECN_FLOW_ORG JFF  WHERE JFF.ORG_ID = "
						+ id
						+ " UNION ALL"
						+ " SELECT JFO.* FROM MY_JECN  INNER JOIN JECN_FLOW_ORG JFO ON JFO.PER_ORG_ID = MY_JECN.ORG_ID)"
						+ " SELECT jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count,jfo.del_state "
						+ " FROM JECN_FLOW_ORG jfo where jfo.projectid = ? and  jfo.org_id in (select org_id from MY_ORG) "
						+ " or jfo.per_org_id in  (select org_id from MY_ORG where jfo.del_state =1)"
						+ " or jfo.org_id in  (select org_id from MY_JECN)"
						+ " or jfo.per_org_id in  (select org_id from MY_JECN)"
						+ "order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
				List<Object[]> list = organizationDao.listNativeSql(sql, projectId);
				return findListByListObjRecy(list);
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				String sql = "SELECT jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count,jfo.del_state"
						+ " FROM JECN_FLOW_ORG jfo where jfo.projectid = ? and  jfo.org_id in (SELECT T.ORG_ID FROM JECN_FLOW_ORG T CONNECT BY PRIOR T.PER_ORG_ID = T.ORG_ID START WITH T.ORG_ID ="
						+ id
						+ " or jfo.org_id in (SELECT T.ORG_ID FROM JECN_FLOW_ORG T  CONNECT BY PRIOR T.ORG_ID = T.PER_ORG_ID"
						+ " START WITH T.ORG_ID = " + id + " )" + ") order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
				List<Object[]> list = organizationDao.listNativeSql(sql, projectId);
				return findListByListObjRecy(list);
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				String sql = "SELECT jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id) as count,jfo.del_state"
						+ " FROM JECN_FLOW_ORG jfo where jfo.projectid = ? order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
				List<Object[]> list = organizationDao.listNativeSql(sql, projectId);
				return findListByListObjRecy(list);
			}
			return null;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public JecnOpenOrgBean openOrgMap(long orgId) throws Exception {
		try {
			JecnFlowOrg jecnFlowOrg = organizationDao.get(orgId);
			String hql = "from JecnFlowOrgImage where orgId=?";
			// 获取所有的组织与岗位元素
			List<JecnFlowOrgImage> listOrgImage = flowOrgImageDao.listHql(hql, orgId);
			hql = "select s from JecnLineSegmentDep as s,JecnFlowOrgImage as o where s.figureId=o.figureId and o.orgId=?";
			List<JecnLineSegmentDep> listLineSegmentDep = flowOrgImageDao.listHql(hql, orgId);

			JecnOpenOrgBean jecnOpenOrgBean = new JecnOpenOrgBean();

			if (listOrgImage != null) {
				for (JecnFlowOrgImage orgImage : listOrgImage) {

					if (orgImage.getFigureType().equals("ManhattanLine")
							|| orgImage.getFigureType().equals("CommonLine")) {
						List<JecnLineSegmentDep> listSegmentDep = new ArrayList<JecnLineSegmentDep>();

						if (null != listLineSegmentDep) {
							for (JecnLineSegmentDep lineSegmentDep : listLineSegmentDep) {
								if (lineSegmentDep.getFigureId().equals(orgImage.getFigureId())) {
									listSegmentDep.add(lineSegmentDep);
								}
							}
						}
						orgImage.setLineSegmentDepList(listSegmentDep);
					}

				}
			}

			jecnOpenOrgBean.setFlowOrgImageList(listOrgImage);
			jecnOpenOrgBean.setJecnFlowOrg(jecnFlowOrg);
			return jecnOpenOrgBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void saveOrgMap(JecnFlowOrg jecnFlowOrg, List<JecnFlowOrgImage> saveList, List<JecnFlowOrgImage> updateList,
			List<Long> deleteListLine, List<Long> deleteList) throws Exception {
		try {
			if (jecnFlowOrg != null) {
				this.organizationDao.update(jecnFlowOrg);
			}
			// 记录图形的UUID和主键ID
			Map<String, Long> map = new HashMap<String, Long>();
			// 记录连接线
			List<JecnFlowOrgImage> relateLinesList = new ArrayList<JecnFlowOrgImage>();
			// 保持新添加的流程元素
			for (JecnFlowOrgImage jecnFlowOrgImage : saveList) {
				if (JecnUtil.isRelateLine(jecnFlowOrgImage.getFigureType())) { // 连接线
					relateLinesList.add(jecnFlowOrgImage);
				} else if ("PositionFigure".equals(jecnFlowOrgImage.getFigureType())) { // 岗位图标
					jecnFlowOrgImage.setFigureNumberId(JecnCommon.getUUID());
					// 添加组织编号 v3.06
					jecnFlowOrgImage.setOrgNumberId(jecnFlowOrg.getOrgNumber());
					this.flowOrgImageDao.save(jecnFlowOrgImage);
					// 存储岗位基本信息
					JecnPositionFigInfo posFigInfo = new JecnPositionFigInfo();
					posFigInfo.setFigureId(jecnFlowOrgImage.getFigureId());
					positionFigInfoDao.save(posFigInfo);
					map.put(jecnFlowOrgImage.getFigureImageId(), jecnFlowOrgImage.getFigureId());
				} else if ("Rect".equals(jecnFlowOrgImage.getFigureType())) { // 组织图标
					this.flowOrgImageDao.save(jecnFlowOrgImage);
					map.put(jecnFlowOrgImage.getFigureImageId(), jecnFlowOrgImage.getFigureId());
				} else {
					this.flowOrgImageDao.save(jecnFlowOrgImage);
					map.put(jecnFlowOrgImage.getFigureImageId(), jecnFlowOrgImage.getFigureId());
				}
			}
			// 保存连接线
			for (JecnFlowOrgImage jecnFlowOrgImage : relateLinesList) {
				if (jecnFlowOrgImage.getStartFigure() == null || jecnFlowOrgImage.getStartFigure().intValue() == -1) {
					jecnFlowOrgImage.setStartFigure(map.get(jecnFlowOrgImage.getStartFigureUUID()));
				}
				if (jecnFlowOrgImage.getEndFigure() == null || jecnFlowOrgImage.getEndFigure().intValue() == -1) {
					jecnFlowOrgImage.setEndFigure(map.get(jecnFlowOrgImage.getEndFigureUUID()));
				}
				flowOrgImageDao.save(jecnFlowOrgImage);
				if (jecnFlowOrgImage.getLineSegmentDepList() != null) {
					for (JecnLineSegmentDep jecnLineSegmentT : jecnFlowOrgImage.getLineSegmentDepList()) {
						jecnLineSegmentT.setFigureId(jecnFlowOrgImage.getFigureId());
						organizationDao.getSession().save(jecnLineSegmentT);
					}
				}
			}

			// 主键ID不存在或为-1时，表示此图形是新添加的需要使用UUID获取其对应的主键ID
			// 设计器元素data对象中的uuid是画图面板本身使用
			// 更新的流程元素
			// 需要保存的
			List<List<JecnLineSegmentDep>> listSave = new ArrayList<List<JecnLineSegmentDep>>();
			for (JecnFlowOrgImage jecnFlowOrgImage : updateList) {
				if (JecnUtil.isRelateLine(jecnFlowOrgImage.getFigureType())) {
					if (jecnFlowOrgImage.getStartFigure() == null || jecnFlowOrgImage.getStartFigure().intValue() == -1) {
						jecnFlowOrgImage.setStartFigure(map.get(jecnFlowOrgImage.getStartFigureUUID()));
					}
					if (jecnFlowOrgImage.getEndFigure() == null || jecnFlowOrgImage.getEndFigure().intValue() == -1) {
						// 主键ID不存在或者=-1情况，通过查找新添加图形获取对应主键ID
						jecnFlowOrgImage.setEndFigure(map.get(jecnFlowOrgImage.getEndFigureUUID()));
					}
					if (jecnFlowOrgImage.getLineSegmentDepList() != null
							&& jecnFlowOrgImage.getLineSegmentDepList().size() > 0) {
						if (deleteListLine == null) {
							deleteListLine = new ArrayList<Long>();
						}
						deleteListLine.add(jecnFlowOrgImage.getFigureId());
						listSave.add(jecnFlowOrgImage.getLineSegmentDepList());
					}
				}
				// 添加组织编号 v3.06
				jecnFlowOrgImage.setOrgNumberId(jecnFlowOrg.getOrgNumber());
				this.flowOrgImageDao.update(jecnFlowOrgImage);
			}
			// 删除线
			if (deleteListLine != null && deleteListLine.size() > 0) {
				String hql = "delete from JecnLineSegmentDep where figureId in";
				List<String> listHql = JecnCommonSql.getListSqls(hql, deleteListLine);
				for (String str : listHql) {
					organizationDao.execteHql(str);
				}
			}

			// 删除图形
			if (deleteList != null && deleteList.size() > 0) {
				// 获取要删除的岗位
				String hqlPos = "select figureId from JecnFlowOrgImage where figureType='PositionFigure' and figureId in";
				List<String> listHql = JecnCommonSql.getListSqls(hqlPos, deleteList);
				List<Long> posIdList = new ArrayList<Long>();
				for (String strHql : listHql) {
					posIdList.addAll(organizationDao.getSession().createQuery(strHql).list());
				}
				// 删除岗位
				deletePosition(posIdList);
				deleteList.removeAll(posIdList);

				// 删除部门和连接线
				String hql = "delete from JecnFlowOrgImage where figureId in";
				listHql = JecnCommonSql.getListSqls(hql, deleteList);
				for (String str : listHql) {
					organizationDao.execteHql(str);
				}
			}

			// 保存线的线段
			for (List<JecnLineSegmentDep> list : listSave) {
				for (JecnLineSegmentDep jecnLineSegmentT : list) {
					organizationDao.getSession().save(jecnLineSegmentT);
				}
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnFlowOrg> getOrgInfos(Long pId) throws Exception {
		String hql = "from JecnFlowOrg where preOrgId = ?";
		List<JecnFlowOrg> listOrg = organizationDao.listHql(hql, pId);
		return listOrg;
	}

	@Override
	public boolean updateOrgCanOpen(Long userId, Long id) throws Exception {

		String hql = "update JecnFlowOrg  set occupier=? where orgId=? and (occupier =0 or occupier=?)";
		int i = organizationDao.execteHql(hql, userId, id, userId);
		if (i == 1) {
			return true;
		}
		return false;
	}

	@Override
	public void moveEdit(Long id) throws Exception {

		try {
			String hql = "update JecnFlowOrg set occupier=0 where orgId=?";
			organizationDao.execteHql(hql, id);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	/**
	 * 岗位参与的角色明显
	 * 
	 * @param figureId
	 * @param pId
	 * @param positionName
	 * @return
	 * @throws Exception
	 */
	private JecnRoleProcessBean getRelateFlowAndRoleByFigureId(Long figureId, Long pId, String positionName)
			throws Exception {
		// 获得岗位在项目下参与的角色
		List<Object[]> listRoleNoGroup = this.positionFigInfoDao.getRoleNOGroupByPosId(figureId, pId);
		// 获得岗位在项目下参与的角色(岗位组)
		List<Object[]> listRoleGroup = positionFigInfoDao.getRoleGroupByPosId(figureId, pId);
		// 收集角色集合
		Set<Long> setRoleIds = new HashSet<Long>();
		List<Object[]> listRole = new ArrayList<Object[]>();
		for (Object[] obj : listRoleNoGroup) {
			if (obj[0] != null) {
				setRoleIds.add(Long.valueOf(obj[0].toString()));
				listRole.add(obj);
			}
		}
		for (Object[] obj : listRoleGroup) {
			if (obj[0] != null) {
				if (!setRoleIds.contains(Long.valueOf(obj[0].toString()))) {
					setRoleIds.add(Long.valueOf(obj[0].toString()));
					listRole.add(obj);
				}
			}
		}
		// 搜集岗位下相关流程的活动
		List<Object[]> listActive = new ArrayList<Object[]>();
		Set<Long> setActiveId = new HashSet<Long>();
		if (setRoleIds.size() > 0) {
			// 通过角色Id获得活动
			listActive = positionFigInfoDao.getActiveByRoleListIds(setRoleIds);
		}
		for (Object[] obj : listActive) {
			if (obj[0] != null) {
				setActiveId.add(Long.valueOf(obj[0].toString()));
			}
		}
		// 获得所有活动的指标对象
		List<JecnRefIndicators> listJecnRefIndicatorsAll = new ArrayList<JecnRefIndicators>();
		if (setActiveId.size() > 0) {
			String sql = "from JecnRefIndicators where activityId in";
			List<String> listStr = JecnCommonSql.getSetSqls(sql, setActiveId);
			for (String hql : listStr) {
				List<JecnRefIndicators> listJecnRefIndicators = this.flowOrgImageDao.listHql(hql);
				for (JecnRefIndicators jecnRefIndicators : listJecnRefIndicators) {
					listJecnRefIndicatorsAll.add(jecnRefIndicators);
				}
			}
		}
		JecnRoleProcessBean jecnRoleProcessBean = new JecnRoleProcessBean();
		// 岗位ID
		jecnRoleProcessBean.setPosId(figureId);
		// 岗位名称
		jecnRoleProcessBean.setPosName(positionName);
		// 参与的角色集合
		List<ProcessRoleWebBean> roleList = new ArrayList<ProcessRoleWebBean>();
		// 参与角色
		for (Object[] objRole : listRole) {
			if (objRole == null || objRole[0] == null || objRole[3] == null || objRole[6] == null) {
				continue;
			}
			// 岗位下参与的角色
			if (figureId.toString().equals(objRole[6].toString())) {
				ProcessRoleWebBean processRoleWebBean = new ProcessRoleWebBean();
				// 角色Id
				processRoleWebBean.setRoleId(Long.valueOf(objRole[0].toString()));
				// 角色名称
				if (objRole[1] != null)
					processRoleWebBean.setRoleName(objRole[1].toString());
				// 角色职责
				if (objRole[2] != null)
					processRoleWebBean.setRoleRes(objRole[2].toString());
				// 流程id
				processRoleWebBean.setFlowId(Long.valueOf(objRole[3].toString()));
				// 流程名称
				if (objRole[4] != null)
					processRoleWebBean.setFlowName(objRole[4].toString());
				// 流程编号
				if (objRole[5] != null)
					processRoleWebBean.setFlowNumber(objRole[5].toString());
				// 参与的活动
				List<ProcessActiveWebBean> activeList = new ArrayList<ProcessActiveWebBean>();
				for (Object[] objActive : listActive) {
					// a.figureId,a.activityId,a.figureText,b.figureRoleId
					if (objActive == null || objActive[0] == null || objActive[3] == null) {
						continue;
					}
					// 角色下参与的活动
					if (objRole[0].equals(objActive[3])) {
						ProcessActiveWebBean processActiveWebBean = new ProcessActiveWebBean();
						// 活动主键
						processActiveWebBean.setActiveFigureId(Long.valueOf(objActive[0].toString()));
						// 活动编号
						if (objActive[1] != null) {
							processActiveWebBean.setActiveId(objActive[1].toString());
						}
						// 活动名称
						if (objActive[2] != null) {
							processActiveWebBean.setActiveName(objActive[2].toString());
						}
						// 活动指标
						List<JecnRefIndicators> indicatorsList = new ArrayList<JecnRefIndicators>();
						for (JecnRefIndicators jecnRefIndicators : listJecnRefIndicatorsAll) {
							if (jecnRefIndicators.getActivityId().toString().equals(objActive[0].toString())) {
								indicatorsList.add(jecnRefIndicators);
							}
						}
						processActiveWebBean.setIndicatorsList(indicatorsList);
						activeList.add(processActiveWebBean);
					}
				}
				JecnUtil.getActivitySort(activeList);
				processRoleWebBean.setActiveList(activeList);
				roleList.add(processRoleWebBean);
			}
		}
		jecnRoleProcessBean.setRoleList(roleList);
		return jecnRoleProcessBean;
	}

	@Override
	public JobDescriptionBean getJobDescription(Long posId) throws Exception {
		try {
			JobDescriptionBean jobDescriptionBean = new JobDescriptionBean();
			// 岗位ID
			jobDescriptionBean.setPosId(posId);
			// 岗位对象
			JecnFlowOrgImage jecnFlowOrgImage = this.flowOrgImageDao.get(posId);
			if (jecnFlowOrgImage == null || jecnFlowOrgImage.getOrgId() == null) {
				return null;
			}
			// 岗位名称
			jobDescriptionBean.setPosName(jecnFlowOrgImage.getFigureText());
			// 所属部门
			jobDescriptionBean.setOrgId(jecnFlowOrgImage.getOrgId());
			JecnFlowOrg jecnFlowOrg = this.get(jecnFlowOrgImage.getOrgId());
			if (jecnFlowOrg != null) {
				jobDescriptionBean.setOrgName(jecnFlowOrg.getOrgName());
			} else {
				jobDescriptionBean.setOrgName("");
			}
			List<JecnTreeBean> upPositionList = new ArrayList<JecnTreeBean>();
			List<JecnTreeBean> lowerPositionList = new ArrayList<JecnTreeBean>();
			this.getUpperLowerPos(upPositionList, lowerPositionList, posId);
			jobDescriptionBean.setUpPositionList(upPositionList);
			jobDescriptionBean.setLowerPositionList(lowerPositionList);
			// 部门的相关制度
			List<JecnTreeBean> inListRule = new ArrayList<JecnTreeBean>();
			List<JecnTreeBean> outListRule = new ArrayList<JecnTreeBean>();
			this.orgRelatesRule(inListRule, outListRule, posId, true);
			jobDescriptionBean.setInListRule(inListRule);
			jobDescriptionBean.setOutListRule(outListRule);

			JecnPositionFigInfo jecnPositionFigInfo = this.positionFigInfoDao.get(posId);
			if (jecnPositionFigInfo != null) {
				// 岗位类别
				if (StringUtils.isNotBlank(jecnPositionFigInfo.getPositionsNature())) {
					jobDescriptionBean.setPositionsNature(jecnPositionFigInfo.getPositionsNature());
				} else {
					jobDescriptionBean.setPositionsNature("");
				}
				// 核心价值
				if (StringUtils.isNotBlank(jecnPositionFigInfo.getSchoolExperience())) {
					jobDescriptionBean.setSchoolExperience(jecnPositionFigInfo.getSchoolExperience());
				} else {
					jobDescriptionBean.setSchoolExperience("");
				}
				// 学历
				if (StringUtils.isNotBlank(jecnPositionFigInfo.getKnowledge())) {
					jobDescriptionBean.setKnowledge(jecnPositionFigInfo.getKnowledge());
				} else {
					jobDescriptionBean.setKnowledge("");
				}
				// 知识
				if (StringUtils.isNotBlank(jecnPositionFigInfo.getPositionSkill())) {
					jobDescriptionBean.setPositionSkill(jecnPositionFigInfo.getPositionSkill());
				} else {
					jobDescriptionBean.setPositionSkill("");
				}
				// 技能
				if (StringUtils.isNotBlank(jecnPositionFigInfo.getPositionImportance())) {
					jobDescriptionBean.setPositionImportance(jecnPositionFigInfo.getPositionImportance());
				} else {
					jobDescriptionBean.setPositionImportance("");
				}
				// 素质模型
				if (StringUtils.isNotBlank(jecnPositionFigInfo.getPositionDiathesisModel())) {
					jobDescriptionBean.setPositionDiathesisModel(jecnPositionFigInfo.getPositionDiathesisModel());
				} else {
					jobDescriptionBean.setPositionDiathesisModel("");
				}
				// 岗位说明书附件
				if (jecnPositionFigInfo.getFileId() != null && jecnPositionFigInfo.getFileId().intValue() != -1) {
					jobDescriptionBean.setFileId(jecnPositionFigInfo.getFileId());
					// 获取附件名
					JecnFileBeanT fileBean = fileDao.findJecnFileBeanTById(jecnPositionFigInfo.getFileId());
					if (fileBean != null) {
						jobDescriptionBean.setFileName(fileBean.getFileName());
					} else {
						jobDescriptionBean.setFileName("");
					}
				} else {
					jobDescriptionBean.setFileId(null);
					jobDescriptionBean.setFileName("");

				}
			}
			// 岗位职责
			jobDescriptionBean.setJecnRoleProcessBean(this.getRelateFlowAndRoleByFigureId(posId, jecnFlowOrg
					.getProjectId(), jecnFlowOrgImage.getFigureText()));
			return jobDescriptionBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int getTotalSearchOrg(String name, Long projectId) throws Exception {
		try {
			String sql = "select count(jfo.org_id) from jecn_flow_org jfo where jfo.projectid=? and jfo.del_state=0";
			if (StringUtils.isNotBlank(name)) {
				sql = sql + " and jfo.org_name like '%" + name + "%'";
			}
			return organizationDao.countAllByParamsNativeSql(sql, projectId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchOrg(String name, int start, int limit, Long projectId) throws Exception {
		try {
			String sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,porg.org_name as pname"
					+ " from jecn_flow_org jfo left join jecn_flow_org porg on jfo.PER_ORG_ID=porg.org_id where jfo.projectid="
					+ projectId + " and jfo.del_state=0";
			if (StringUtils.isNotBlank(name)) {
				sql = sql + " and jfo.org_name like '%" + name + "%'";
			}
			List<Object[]> list = organizationDao.listNativeSql(sql, start, limit);
			List<JecnTreeBean> listJecnTreeBean = new ArrayList<JecnTreeBean>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
					return null;
				}

				JecnTreeBean jecnTreeBean = new JecnTreeBean();

				// 组织节点ID
				jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
				// 组织节点名称
				jecnTreeBean.setName(obj[1].toString());
				// 组织节点父ID
				jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
				if (obj[3] != null) {
					jecnTreeBean.setPname(obj[3].toString());
				} else {
					// 组织节点父类名称
					jecnTreeBean.setPname("");
				}
				listJecnTreeBean.add(jecnTreeBean);
			}
			return listJecnTreeBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int getTotalSearchPos(String name, Long projectId) throws Exception {
		try {
			String sql = "select count(t.figure_id)"
					+ " from jecn_flow_org_image t,jecn_flow_org jfo where t.org_id=jfo.org_id and jfo.del_state=0 and jfo.projectid="
					+ projectId + " and t.figure_type =" + JecnCommonSql.getPosString();
			if (StringUtils.isNotBlank(name)) {
				sql = sql + " and t.figure_text like '%" + name + "%'";
			}
			return organizationDao.countAllByParamsNativeSql(sql, projectId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public JecnTreeBean getJecnTreeBeanByposId(Long posId) throws Exception {
		String sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name,0 as count"
				+ " from jecn_flow_org_image t,jecn_flow_org jfo where t.org_id=jfo.org_id and jfo.del_state=0"
				+ " and t.figure_id=?";
		List<Object[]> listAll = this.organizationDao.listNativeSql(sql, posId);
		if (listAll.size() == 1) {
			return JecnUtil.getJecnTreeBeanByObjectPos(listAll.get(0));
		}
		return null;
	}

	@Override
	public List<JecnTreeBean> searchPos(String name, int start, int limit, Long projectId) throws Exception {
		try {
			String sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name"
					+ " from jecn_flow_org_image t,jecn_flow_org jfo where t.org_id=jfo.org_id and jfo.del_state=0 and jfo.projectid="
					+ projectId + " and t.figure_type =" + JecnCommonSql.getPosString();
			if (StringUtils.isNotBlank(name)) {
				sql = sql + " and t.figure_text like '%" + name + "%'";
			}
			List<Object[]> list = organizationDao.listNativeSql(sql, start, limit);
			List<JecnTreeBean> listJecnTreeBean = new ArrayList<JecnTreeBean>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
					return null;
				}
				JecnTreeBean jecnTreeBean = new JecnTreeBean();

				// 岗位节点ID
				jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
				// 岗位节点名称
				jecnTreeBean.setName(obj[1].toString());
				// 组织节点ID
				jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
				if (obj[3] != null) {
					jecnTreeBean.setPname(obj[3].toString());
				} else {
					// 组织节点父类名称
					jecnTreeBean.setPname("");
				}
				listJecnTreeBean.add(jecnTreeBean);
			}
			return listJecnTreeBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getPositionsByIds(String ids) throws Exception {
		String sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name"
				+ " from jecn_flow_org_image t,jecn_flow_org jfo where t.org_id=jfo.org_id and jfo.del_state=0 and t.figure_id in("
				+ ids + ")";
		List<Object[]> list = organizationDao.listNativeSql(sql);
		List<JecnTreeBean> listJecnTreeBean = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				return null;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();

			// 岗位节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 岗位节点名称
			jecnTreeBean.setName(obj[1].toString());
			// 组织节点ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			if (obj[3] != null) {
				jecnTreeBean.setPname(obj[3].toString());
			} else {
				// 组织节点父类名称
				jecnTreeBean.setPname("");
			}
			listJecnTreeBean.add(jecnTreeBean);
		}
		return listJecnTreeBean;
	}

	/**
	 * 查询组织处在的级别
	 * 
	 * @author fuzhh 2013-12-5
	 * @param orgId
	 * @return
	 * @throws Exception
	 */
	public int findOrgDepth(Long orgId) throws Exception {
		return this.organizationDao.get(orgId).gettLevel();
	}

	/**
	 * 角色清单
	 * 
	 * @author fuzhh 2013-12-3
	 * @return
	 * @throws Exception
	 */
	public OrgListBean findOrgList(Long orgId, Long projectId, int depth, boolean isAll) throws Exception {
		// 获取组织相关流程
		List<Long> listIds = new ArrayList<Long>();
		listIds.add(orgId);
		String sql = "with MY_ORG as (select jfo.org_id," + "                              jfo.per_org_id,"
				+ "                              jfo.org_name," + "                              jfo.org_respon,"
				+ "                              jfo.sort_id," + "                              jfo.t_level"
				+ "                         from JECN_FLOW_ORG jfo" + " INNER JOIN ("
				+ JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.organization)
				+ ") org on org.org_id = jfo.org_id" + "	where jfo.del_state = 0 )";
		sql = sql + " select " + orgListSql();
		List<Object[]> orgProcessList = organizationDao.listNativeSql(sql);
		// 存储组织数据的Map
		Map<Long, OrgRelateFlowBean> orgRelatsMap = new LinkedHashMap<Long, OrgRelateFlowBean>();
		// 获取组织数据
		OrgListBean orgListBean = new OrgListBean();
		// 级别最大值
		int maxCount = 0;
		// 未发布的流程总数
		int noPubFlowTotal = 0;
		// 正在审核的流程总数
		int approveFlowTotal = 0;
		// 发布的流程总数
		int pubFlowTotal = 0;
		if (orgProcessList != null) {
			for (Object[] obj : orgProcessList) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[8] == null) {
					continue;
				}
				// 组织ID
				Long orgStrId = Long.valueOf(obj[0].toString());
				// 组织数据
				OrgRelateFlowBean orgRelateFlowBean = orgRelatsMap.get(orgStrId);
				if (orgRelateFlowBean == null) {
					orgRelateFlowBean = new OrgRelateFlowBean();
					// 组织ID
					orgRelateFlowBean.setOrgId(orgStrId);
					// 组织父节点
					orgRelateFlowBean.setPerOreId(Long.valueOf(obj[1].toString()));
					// 组织名称
					if (obj[2] != null) {
						orgRelateFlowBean.setOrgName(obj[2].toString());
					}
					// 组织职责
					if (obj[3] != null) {
						orgRelateFlowBean.setOrgDuties(obj[3].toString());
					}
					// 组织所在级别
					int lev = Integer.valueOf(obj[8].toString());
					if (lev > maxCount) {
						maxCount = lev;
					}
					orgRelateFlowBean.setLevel(lev);
					// 添加到Map集合
					orgRelatsMap.put(orgStrId, orgRelateFlowBean);

				}
				// 流程ID
				if (obj[4] != null) {
					Long flowId = Long.valueOf(obj[4].toString());
					// 流程bean
					ProcessListBean processListBean = orgRelateFlowBean.getProcessMap().get(flowId);
					if (processListBean == null) {
						processListBean = new ProcessListBean();
						// 流程ID
						processListBean.setFlowId(flowId);
						// 流程名称
						if (obj[5] != null) {
							processListBean.setFlowName(obj[5].toString());
						}
						// 流程编号
						if (obj[6] != null) {
							processListBean.setFlowNumber(obj[6].toString());
						}
						// 有效期
						if (obj[7] != null) {
							processListBean.setExpiry(Integer.valueOf(obj[7].toString()));
						}
						// 流程责任人
						if (obj[13] != null) {
							processListBean.setResPeopleName(obj[13].toString());
						}
						// 流程监护人
						if (obj[14] != null) {
							processListBean.setGuardianName(obj[14].toString());
						}
						// 拟稿人
						if (obj[15] != null) {
							processListBean.setDraftPerson(obj[15].toString());
						}
						// 版本号
						if (obj[16] != null) {
							processListBean.setVersionId(obj[16].toString());
						}
						// 流程状态
						if (obj[19] != null) {
							// 发布
							processListBean.setFlowState(2);
							pubFlowTotal++;
							// 发布日期
							if (obj[17] != null) {
								processListBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon
										.getDateByString(obj[17].toString())));
							}
						} else {
							if (obj[18] != null) {
								// 审批中
								processListBean.setFlowState(1);
								approveFlowTotal++;
							} else {
								// 待建
								processListBean.setFlowState(0);
								noPubFlowTotal++;
							}
						}

						// 是否及时优化 0空 ,1是 ,2否
						int optimization = 0;
						// 发布日期不为空且有效期不为空不为0
						if (processListBean.getPubDate() != null && !"".equals(processListBean.getPubDate())
								&& processListBean.getExpiry() != null
								&& Integer.valueOf(processListBean.getExpiry()) != 0) {

							Calendar calender = Calendar.getInstance();
							calender.setTime(JecnCommon.getDateByString(processListBean.getPubDate()));
							calender.add(Calendar.MONTH, processListBean.getExpiry());

							Date nextScan = calender.getTime();

							Date nowDate = new Date();
							// 当前时间在下次审视时间之前 则为否
							if (nextScan.before(nowDate)) {
								optimization = 2;
							}
						}
						processListBean.setOptimization(optimization);

						orgRelateFlowBean.getProcessMap().put(flowId, processListBean);
					}
					// KPI ID 和 名称
					if (obj[9] != null && obj[10] != null) {
						// KPI ID
						Long kpiId = Long.valueOf(obj[9].toString());
						// KPI 名称
						String kpiName = obj[10].toString();
						// 向map集合添加数据
						processListBean.getFlowKPIMap().put(kpiId, kpiName);
					}
					// 活动ID 和 名称
					if (obj[11] != null && obj[12] != null) {
						// 活动ID
						Long activeId = Long.valueOf(obj[11].toString());
						// 活动名称
						String activeName = obj[12].toString();
						// 向map集合添加数据
						processListBean.getProcessActiveMap().put(activeId, activeName);
					}

				}
			}
		}
		// 总级别数
		orgListBean.setLevelTotal(maxCount + 1);
		Set<Long> orgIdSet = new HashSet<Long>();
		// 组织排序
		orgListSort(orgId, orgRelatsMap, orgListBean, orgIdSet);
		// 所有组织个数
		orgListBean.setOrgTotal(orgListBean.getOrgRelatsList().size());
		// 流程统计信息
		orgListBean.setApproveFlowTotal(approveFlowTotal);
		orgListBean.setNoPubFlowTotal(noPubFlowTotal);
		orgListBean.setPubFlowTotal(pubFlowTotal);
		return orgListBean;
	}

	/**
	 * 组织清单数据排序
	 * 
	 * @author fuzhh 2013-12-12
	 * @param processSystemListBean
	 * @param sortProcessList
	 */
	private void orgListSort(Long orgId, Map<Long, OrgRelateFlowBean> orgRelatsMap, OrgListBean orgSortListBean,
			Set<Long> orgIdSet) {
		Set<Long> orgSet = orgRelatsMap.keySet();
		for (Long oId : orgSet) {
			OrgRelateFlowBean orgRelateFlowBean = orgRelatsMap.get(oId);
			if (orgId.equals(orgRelateFlowBean.getOrgId())) {
				orgSortListBean.getOrgRelatsList().add(orgRelateFlowBean);
			} else if (orgId.equals(orgRelateFlowBean.getPerOreId())) {
				boolean isQuery = false;
				for (Long orId : orgIdSet) {
					if (orId.equals(orgRelateFlowBean.getOrgId())) {
						isQuery = true;
					}
				}
				if (!isQuery) {
					orgIdSet.add(orgRelateFlowBean.getOrgId());
					orgListSort(orgRelateFlowBean.getOrgId(), orgRelatsMap, orgSortListBean, orgIdSet);
				}
			}

		}
	}

	/**
	 * 组织清单
	 * 
	 * @author fuzhh 2013-12-19
	 * @return
	 */
	private String orgListSql() {
		String sql = " mo.org_id," + "              mo.per_org_id," + "              mo.org_name,"
				+ "              mo.Org_Respon," + "              jfst.flow_id as t_flow_id,"
				+ "              jfst.flow_name," + "              jfst.flow_id_input," + "              jfbi.expiry,"
				+ "              mo.t_level," + "              jfkn.kpi_and_id," + "              jfkn.kpi_name,"
				+ "              jfsi.figure_id," + "              jfsi.figure_text," + "				 case"
				+ " 			 when jfbi.TYPE_RES_PEOPLE = 1 then" + "				 (select jfoi.figure_text"
				+ "				 from jecn_flow_org_image jfoi" + "				 where jfoi.org_id = mo.org_id"
				+ "				 and jfoi.figure_id = jfbi.res_people_id)" + "				 when jfbi.type_res_people = 0 then"
				+ "				 (select ju.true_name" + "				 from jecn_user ju"
				+ "				 where ju.people_id = jfbi.res_people_id)" + "				 end as 流程责任人," + "				 (select ju.true_name"
				+ "				 from jecn_user ju" + "				 where ju.people_id = jfbi.ward_people_id) as 流程监护人,"
				+ "				 jthn.draft_person as 拟稿人," + "				 jthn.version_id as 版本号," + "				 jfs.pub_time as 发布日期,"
				+ "				 jtbn.state as 流程状态," + "				 jfs.flow_id as flow_id" + "         from MY_ORG mo"
				+ "         left join (SELECT distinct a.org_id, a.flow_id " + "FROM JECN_FLOW_RELATED_ORG_T a "
				+ "left join JECN_FLOW_RELATED_ORG b on a.org_id = "
				+ "                                   b.org_id) jfrot on mo.ORG_ID = "
				+ "                                                      jfrot.org_id "
				+ "left join jecn_flow_structure_t jfst on jfst.flow_id = "
				+ "                                   jfrot.flow_id "
				+ "left join JECN_FLOW_STRUCTURE jfs on jfst.flow_id = "
				+ "                                 jfs.flow_id "
				+ " left join JECN_FLOW_BASIC_INFO jfbi on jfs.flow_id = "
				+ "                                jfbi.flow_id "
				+ "    left join JECN_FLOW_KPI_NAME jfkn on jfs.flow_id = "
				+ "                               jfkn.flow_id "
				+ "left join (select ji.flow_id, ji.figure_id, ji.figure_text " + "from JECN_FLOW_STRUCTURE_IMAGE ji "
				+ "where ji.figure_type in " + JecnCommonSql.getActiveString()
				+ "and ji.linecolor in ('1', '2', '3')) jfsi on jfs.flow_id = "
				+ "                                            jfsi.flow_id "
				+ " left join JECN_TASK_HISTORY_NEW jthn on jfs.History_Id = "
				+ "                                      jthn.Id "
				+ "left join jecn_task_bean_new jtbn on jtbn.r_id = jfst.flow_id "
				+ "                             and jtbn.task_type = 0 "
				+ "                               and jtbn.is_lock = 1 "
				+ "                              and jtbn.state <> 5 "
				+ "  order by mo.per_org_id, mo.sort_id, mo.org_id ";
		return sql;
	}

	@Override
	public boolean validateAddUpdatePosNum(String posNum, Long id) throws Exception {

		try {
			String hql = "";
			List<JecnFlowOrgImage> list = new ArrayList<JecnFlowOrgImage>();
			if (id != null) {
				hql = "from JecnFlowOrgImage where figureId<> ? and figureNumberId=?";
				list = flowOrgImageDao.listHql(hql, id, posNum);
			} else {
				hql = "from JecnFlowOrgImage where figureNumberId=?";
				list = flowOrgImageDao.listHql(hql, posNum);
			}
			if (list != null && list.size() > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public boolean validateAddUpdateOrgNum(String orgNum, Long id) throws Exception {
		try {
			String hql = "";
			List<JecnFlowOrgImage> list = new ArrayList<JecnFlowOrgImage>();
			if (id != null) {
				hql = "from JecnFlowOrg where orgId<> ? and orgNumber=?";
				list = organizationDao.listHql(hql, id, orgNum);
			} else {
				hql = "from JecnFlowOrg where orgNumber=?";
				list = organizationDao.listHql(hql, orgNum);
			}
			if (list != null && list.size() > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 组织回收站 树显示
	 * 
	 * @param projectId
	 *            项目id
	 * @param pid
	 *            组织id
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pid) throws Exception {
		try {
			if (JecnContants.dbType.equals(DBType.MYSQL)) {
				String sql = " select distinct org.org_id,org.org_name,org.per_org_id,"
						+ " case when child_org.org_id is null then 0 else 1 end as child_count,org.del_state,org.sort_id"
						+ " from jecn_flow_org org" + " left join jecn_flow_org child_org"
						+ " on org.org_id=child_org.per_org_id"
						+ " where org.projectid=? order by org.per_org_id,org.sort_id,org.org_id";
				List<Object[]> list = organizationDao.listNativeSql(sql, projectId);
				return findListByListObjRecy(list);
			} else {
				String sql = " select distinct org.org_id,org.org_name,org.per_org_id,"
						+ " case when child_org.org_id is null then 0 else 1 end as child_count,org.del_state,org.sort_id"
						+ " from jecn_flow_org org" + " left join jecn_flow_org child_org"
						+ " on org.org_id=child_org.per_org_id"
						+ " where org.projectid=? and org.per_org_id=? order by org.per_org_id,org.sort_id,org.org_id";
				List<Object[]> list = organizationDao.listNativeSql(sql, projectId, pid);
				return findListByListObjRecy(list);
			}

		} catch (Exception e) {
			log.error("OrganizationServiceImpl 获取选中的组织下的组织报错", e);
			throw e;
		}
	}

	/**
	 * 组织回收站 恢复当前节点及子节点
	 */
	@Override
	public void recoverCurAndChildOrgs(List<Long> listIds, Long projectId) throws Exception {

		String sql = "";
		Set<Long> setOrgTIds = new HashSet<Long>();
		try {
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = " WITH my_org AS (" + "  SELECT * FROM jecn_flow_org JF" + "  WHERE JF.org_id in "
						+ JecnCommonSql.getIds(listIds) + "  UNION ALL" + "  SELECT JFF.*" + "  FROM my_org"
						+ "  INNER JOIN jecn_flow_org JFF ON my_org.org_id = JFF.Per_org_id)"
						+ "  SELECT distinct org_id FROM my_org";
				List<Object> list = fileDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setOrgTIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "  select distinct JF.org_id" + "  from jecn_flow_org JF"
						+ "  CONNECT BY PRIOR JF.org_id = JF.PER_org_id" + "  START WITH JF.org_id in "
						+ JecnCommonSql.getIds(listIds);
				List<Object> list = fileDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setOrgTIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				sql = "select tt.org_id,tt.per_org_id from  jecn_flow_org  tt";
				List<Object[]> listAll = fileDao.listNativeSql(sql);
				setOrgTIds = JecnCommon.getAllChilds(listIds, listAll);
			}
			if (setOrgTIds.size() > 0) {
				sql = "update jecn_flow_org set del_State=0 where org_id in";
				List<String> listSql = JecnCommonSql.getSetSqls(sql, setOrgTIds);
				for (String sqlStr : listSql) {
					fileDao.execteNative(sqlStr);
				}
			}

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 组织回收站 恢复当前节点
	 */
	@Override
	public void recoverCurOrgs(List<Long> recycleNodesIdList) throws Exception {
		try {
			String sql = "update jecn_flow_org set del_state=0" + "          where org_id in"
					+ JecnCommonSql.getIds(recycleNodesIdList);
			this.organizationDao.execteNative(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 组织回收站 定位
	 */
	@Override
	public List<JecnTreeBean> getRecyleSelectedNodes(Long id, Long projectId) throws Exception {
		try {
			JecnFlowOrg jecnFlowOrg = this.get(id);
			if (jecnFlowOrg == null) {
				return new ArrayList<JecnTreeBean>();
			}
			Set<Long> setIds = JecnCommon.getPositionTreeIds(jecnFlowOrg.gettPath(), id);
			String sql = "Select DISTINCT JF.ORG_ID,JF.ORG_NAME,JF.PER_ORG_ID"
					+ "  ,CASE WHEN SUB.ORG_ID IS NULL THEN 0 ELSE 1 END AS child_count,JF.del_state,JF.SORT_ID,JF.T_PATH"
					+ "  FROM JECN_FLOW_ORG JF" + "  LEFT JOIN JECN_FLOW_ORG SUB on SUB.PER_ORG_ID = JF.ORG_ID"
					+ "  where JF.Projectid=? AND JF.PER_ORG_ID in " + JecnCommonSql.getIdsSet(setIds)
					+ "  ORDER BY JF.PER_ORG_ID,JF.SORT_ID,JF.ORG_ID";
			List<Object[]> list = organizationDao.listNativeSql(sql, projectId);
			return findListByListObjOrgRecy(list);

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	/**
	 * 组织回收站 封装回收站的bean
	 * 
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findListByListObjOrgRecy(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[6] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 组织节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 组织节点名称
			jecnTreeBean.setName(obj[1].toString());
			// 组织节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			// 组织节点父类名称
			jecnTreeBean.setPname("");
			jecnTreeBean.setTreeNodeType(TreeNodeType.organization);
			if (Integer.parseInt(obj[3].toString()) > 0) {
				// 组织节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
			// 删除状态 0：否，1：是
			jecnTreeBean.setIsDelete(Integer.parseInt(obj[4].toString()));
			jecnTreeBean.setT_Path(obj[6].toString());

			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	/**
	 * 组织回收站 封装回收站的bean
	 * 
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findListByListObjRecy(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
				return null;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 组织节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 组织节点名称
			jecnTreeBean.setName(obj[1].toString());
			// 组织节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			// 组织节点父类名称
			jecnTreeBean.setPname("");
			jecnTreeBean.setTreeNodeType(TreeNodeType.organization);
			if (Integer.parseInt(obj[3].toString()) > 0) {
				// 组织节点是否有子节点
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
			if (obj.length >= 5 && obj[4] != null) {
				// 删除状态 0：否，1：是
				jecnTreeBean.setIsDelete(Integer.parseInt(obj[4].toString()));
			}
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	/**
	 * 组织回收站 搜索
	 */
	@Override
	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId) {
		String sql = " select jft.org_id, jft.org_name,jft.per_org_id" + "  from jecn_flow_org jft"
				+ "  where jft.org_name like '%" + name + "%' and jft.del_state=1 and jft.projectid=?";

		List<Object[]> objs = organizationDao.listNativeSql(sql, 0, 40, projectId);
		return getOrgListByObjs(objs);
	}

	/**
	 * 组织回收站bean封装
	 * 
	 * @param objs
	 * @return
	 */
	private List<JecnTreeBean> getOrgListByObjs(List<Object[]> objs) {
		List<JecnTreeBean> flieList = new ArrayList<JecnTreeBean>();
		if (objs.size() == 0) {
			return flieList;
		}
		for (Object[] obj : objs) {
			if (obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}
			JecnTreeBean orgBean = new JecnTreeBean();
			orgBean.setId(Long.valueOf(obj[0].toString()));
			orgBean.setName(obj[1].toString());
			orgBean.setPid(Long.valueOf(obj[2].toString()));
			orgBean.setTreeNodeType(TreeNodeType.organization);
			flieList.add(orgBean);
		}
		return flieList;
	}

	@Override
	public List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		List<Object[]> list = organizationDao.searchRoleAuthByName(name, projectId, peopleId);
		return findByListObjects(list);
	}

	@Override
	public JecnTreeBean getCurrentOrg(Long orgId, Long projectId) throws Exception {
		String sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where del_state=0 and per_org_id=jfo.org_id ) as count"
				+ " ,jfo.ORG_NUMBER"
				+ " from jecn_flow_org jfo where jfo.org_id=? and jfo.projectid=? and jfo.del_state=0";
		Object[] obj = organizationDao.getObjectNativeSql(sql, orgId, projectId);

		if (obj == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = getMoveJecnTreeBeanByObjectOrg(obj);

		return jecnTreeBean;
	}

	@Override
	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId, Long startId) throws Exception {
		String sql = " select jft.org_id, jft.org_name,jft.per_org_id"
				+ "  from jecn_flow_org jft inner join jecn_flow_org jftp on jftp.org_id =" + startId
				+ " and jft.t_path like jftp.t_path " + JecnCommonSql.getConcatChar()
				+ "'%' where jft.org_name like '%" + name + "%' and jft.del_state=1 and jft.projectid=?";

		List<Object[]> objs = organizationDao.listNativeSql(sql, 0, 40, projectId);
		return getOrgListByObjs(objs);
	}

	@Override
	public JecnTreeBean getCurrentOrgContainDel(Long orgId, Long projectId) throws Exception {
		String sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count"
				+ " ,jfo.ORG_NUMBER" + " from jecn_flow_org jfo where jfo.org_id=? and jfo.projectid=?";
		Object[] obj = organizationDao.getObjectNativeSql(sql, orgId, projectId);

		if (obj == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = getMoveJecnTreeBeanByObjectOrg(obj);

		return jecnTreeBean;
	}

	@Override
	public List<JecnTreeBean> searchPositionByName(String name, Long projectId, Long startId) throws Exception {
		try {
			// 岗位
			String sql = "select t.figure_id,t.figure_text,t.org_id,jfo.org_name"
					+ "        from jecn_flow_org_image t,jecn_flow_org jfo "
					+ " inner join jecn_flow_org jfop on jfo.t_path like jfop.t_path" + JecnCommonSql.getConcatChar()
					+ "'%' and jfop.org_id=" + startId
					+ " where t.figure_text like ? and t.org_id=jfo.org_id and jfo.del_state=0"
					+ "        and jfo.projectid=? and t.figure_type = 'PositionFigure' order by t.org_id,t.sort_id";
			List<Object[]> listPos = organizationDao.listNativeSql(sql, 0, 40, "%" + name + "%", projectId);
			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			for (Object[] obj : listPos) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
				if (jecnTreeBean != null) {
					listResult.add(jecnTreeBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public DownloadPopedomBean getFlowFileDownloadPermissions(Long id) throws Exception {

		DownloadPopedomBean bean = new DownloadPopedomBean();

		List<JecnTreeBean> posList = this.organizationDao.listFlowFileDownloadPosPerms(id);
		bean.setPosList(posList);

		return bean;
	}

	@Override
	public void saveOrUpdateFlowFileDownloadPermissions(Long flowId, String posIds) throws Exception {

		String sql = "select c.flow_id from jecn_flow_structure_t c"
				+ " inner join jecn_flow_structure_t p on c.t_path like p.t_path" + JecnCommonSql.getConcatChar()
				+ "'%' where p.flow_id=" + flowId;

		List<Object> objs = this.organizationDao.listNativeSql(sql);
		Set<Long> ids = new HashSet<Long>();
		for (Object obj : objs) {
			ids.add(Long.valueOf(obj.toString()));
		}

		sql = "delete from JECN_FLOW_FILE_POS_PERM where relate_id in ";
		List<String> sqls = JecnCommonSql.getSetSqls(sql, ids);
		for (String delSql : sqls) {
			this.organizationDao.execteNative(delSql);
		}

		if (StringUtils.isBlank(posIds)) {
			return;
		}
		String[] posIdArr = posIds.split(",");
		if (JecnCommon.isOracle()) {
			for (String id : posIdArr) {
				if (StringUtils.isBlank(id)) {
					continue;
				}
				sql = "insert into JECN_FLOW_FILE_POS_PERM(ID,Figure_Id,Relate_Id)"
						+ " select JECN_PERMISSIONS_SQUENCE.Nextval," + id + ",c.flow_id from jecn_flow_structure_t c"
						+ " inner join jecn_flow_structure_t p on c.t_path like p.t_path||'%' where p.flow_id="
						+ flowId;
				this.organizationDao.execteNative(sql);
			}
		} else {
			for (String id : posIdArr) {
				if (StringUtils.isBlank(id)) {
					continue;
				}
				sql = "insert into JECN_FLOW_FILE_POS_PERM(Figure_Id,Relate_Id) " + " select " + id
						+ ",c.flow_id from jecn_flow_structure_t c "
						+ " inner join jecn_flow_structure_t p on c.t_path like p.t_path+'%' where p.flow_id=" + flowId;
				this.organizationDao.execteNative(sql);
			}
		}
	}

	/**
	 * 组织权限下人员选择
	 * 
	 * @param pId
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getRoleAuthChildPersonOrgs(Long pId, Long projectId, Long peopleId) throws Exception {
		try {
			// 组织
			List<Object[]> listOrg = organizationDao.getRoleAuthChildPersonOrgs(pId, projectId, peopleId);
			// 岗位
			List<Object[]> listPos = organizationDao.getRoleAuthPosAndPersonByOrgId(pId, projectId, peopleId);
			List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
			for (Object[] obj : listOrg) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectOrg(obj);
				if (jecnTreeBean != null) {
					listResult.add(jecnTreeBean);
				}
			}
			for (Object[] obj : listPos) {
				JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectPos(obj);
				if (jecnTreeBean != null) {
					if (obj[5] != null && Integer.parseInt(obj[5].toString()) > 0) {
						jecnTreeBean.setChildNode(true);
					} else {
						jecnTreeBean.setChildNode(false);
					}
					listResult.add(jecnTreeBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public ByteFile getJobDescriptionByteFile(Long posId) throws Exception {
		// 岗位说明书数据
		JobDescriptionBean descriptionBean = this.getJobDescription(posId);
		// 生成byte[];
		ByteFile byteFile = new ByteFile();
		byteFile.setBytes(JecnFinal.getTempFileBytes(PositionDescription.createPositionDesc(descriptionBean)));
		byteFile.setFileName(descriptionBean.getPosName() + ".doc");
		return byteFile;
	}

	@Override
	public List<JecnDictionary> getDictionarys(DictionaryEnum name) throws Exception {
		List<JecnDictionary> list = JecnContants.dictionaryMap.get(name);
		if (list == null) {
			list = new ArrayList<JecnDictionary>();
		}
		return list;
	}

	@Override
	public List<JecnDictionary> getDictionarys() throws Exception {
		String hql = "from JecnDictionary WHERE VALIDABLE = 'y'  order by name,code";
		return fileDao.listHql(hql);
	}

	@Override
	public AccessId getPepoleOrgAndPosAndPosPosGroup(long userId, Long projectId) {
		AccessId accId = new AccessId();
		// 人员所在部门
		/*
		 * String orgSql = "select distinct org.org_id" +
		 * "  from jecn_flow_org org" + " inner join jecn_flow_org_image pos" +
		 * "    on pos.org_id = org. org_id" +
		 * " inner join jecn_user_position_related u" +
		 * "    on u.figure_id = pos.figure_id" + "   and u.people_id = " +
		 * userId;
		 */

		String orgSql = "select distinct pub.org_id from jecn_flow_org jfo"
				+ "      inner join jecn_flow_org pub on jfo.t_path like "
				+ JecnCommonSql.getJoinFunc("pub")
				+ "      where jfo.org_id in"
				+ "      (SELECT ORG.ORG_ID FROM JECN_FLOW_ORG ORG"
				+ "         INNER JOIN JECN_FLOW_ORG_IMAGE POS ON POS.ORG_ID=ORG.ORG_ID"
				+ "         INNER JOIN JECN_USER_POSITION_RELATED JUPR ON POS.FIGURE_ID=JUPR.FIGURE_ID AND JUPR.PEOPLE_ID="
				+ userId + "         WHERE ORG.DEL_STATE=0 AND ORG.PROJECTID=" + projectId + ")";

		List<Object> org = organizationDao.listNativeSql(orgSql);
		if (org != null && !org.isEmpty()) {
			List<AccessId> orgAccessId = new ArrayList<AccessId>();
			for (Object object : org) {
				AccessId orgAccId = new AccessId();
				orgAccId.setAccessId(Long.valueOf(object.toString()));
				orgAccessId.add(orgAccId);
			}
			accId.setOrgAccessId(orgAccessId);
		}

		// 人员所在岗位
		String postSql = "select distinct pos.figure_id" + "  from  jecn_flow_org_image pos"
				+ " inner join jecn_user_position_related u" + "    on u.figure_id = pos.figure_id"
				+ "   and u.people_id = " + userId;
		List<Object> pos = organizationDao.listNativeSql(postSql);
		if (pos != null && !pos.isEmpty()) {
			List<AccessId> posAccessId = new ArrayList<AccessId>();
			for (Object object : pos) {
				AccessId posAccId = new AccessId();
				posAccId.setAccessId(Long.valueOf(object.toString()));
				posAccessId.add(posAccId);
			}
			accId.setPosAccessId(posAccessId);
		}

		// 人员所在岗位组
		String groupSql = "select posg.id" + "  from jecn_position_group posg"
				+ " inner join jecn_position_group_r posr" + "    on posg.id = posr.group_id"
				+ " inner join jecn_flow_org_image pos" + "    on pos.figure_id = posr.figure_id"
				+ " inner join jecn_user_position_related u" + "    on u.figure_id = pos.figure_id"
				+ "   and u.people_id = " + userId;
		List<Object> posGroup = organizationDao.listNativeSql(groupSql);
		if (posGroup != null && !posGroup.isEmpty()) {
			List<AccessId> posGroupAccessId = new ArrayList<AccessId>();
			for (Object object : posGroup) {
				AccessId posGroupAccId = new AccessId();
				posGroupAccId.setAccessId(Long.valueOf(object.toString()));
				posGroupAccessId.add(posGroupAccId);
			}
			accId.setPosGroupAccessId(posGroupAccessId);
		}
		return accId;
	}

	@Override
	public List<JecnMaintainNode> getLangues() {
		String hql = "from JecnMaintainNode";
		return organizationDao.listHql(hql);
	}
}
