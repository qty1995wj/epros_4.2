package epros.draw.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.LogFactory;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.designer.JecnDesignerData;
import epros.draw.designer.TmpPageSetingBean;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.unit.JecnSelectFigureXY;
import epros.draw.gui.operationConfig.bean.JecnFlowOperationBean;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 流程图或流程地图数据对象
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowMapData {
	/** 画图面板类型：流程地图或流程图 */
	private MapType mapType = MapType.none;
	/** 画图面板唯一标识:UUID */
	private String uniqueNum = null;
	/** 流程ID */
	private Long flowId;
	/** 流程地图或流程图名称 */
	private String name = "new Flow";
	/** 面板宽 */
	private int workflowWidth = 0;
	/** 面板高 */
	private int workflowHeight = 0;

	/** 图形对象集合 */
	private List<JecnFigureData> figureList = new ArrayList<JecnFigureData>();
	/** 线对象集合 */
	private List<JecnBaseLineData> lineList = new ArrayList<JecnBaseLineData>();

	// ***************流程图专用 start***************//
	/** 是否是横向标识 true：横向 false：纵向 */
	private boolean isHFlag = true;
	/** 预估角色个数 */
	private int roleCount = 0;
	/** 预估活动个数 */
	private int roundRectCount = 0;

	/** 当前画图数据是否保存标识： true：需要保存；false：不需要保存 (默认不保存) */
	private boolean isSave = false;

	/** 文件操作状态 1：可编辑，2：任务中，3 ：只读模式 ； 4：只读模式，可编辑 ；5：占用 */
	private int isAuthSave = 1;
	/****
	 * 与 isAuthSave 配合使用 在没有编辑权限下 判断是否有更改。
	 */
	private boolean isChange = false;
	/** 面板 占用人 */
	private String occupierName;

	/** 连线时是否显示 图形编辑点 */
	private boolean isHiddenLineEditPoint = false;
	/** 流程操作说明数据对象 */
	private JecnFlowOperationBean flowOperationBean = new JecnFlowOperationBean();

	/** 分页显示基本信息 */
	private TmpPageSetingBean pageSetingBean = new TmpPageSetingBean();

	// ***************流程图专用 end***************//

	// *************（设计器专用对象） start***************//
	private JecnDesignerData designerData = null;

	private JecnSelectFigureXY modelFigureXY = null;

	// ***************（设计器专用对象）***************//

	/**
	 * 
	 * 创建流程地图对象
	 * 
	 * @param mapType
	 */
	public JecnFlowMapData(MapType mapType) {
		if (mapType == null) {
			LogFactory.getLog(JecnFlowMapData.class).error("JecnFlowMapData类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.mapType = mapType;

	}

	public JecnDesignerData getDesignerData() {
		if (designerData == null) {
			designerData = new JecnDesignerData();
		}
		return designerData;
	}

	public String getUniqueNum() {
		return uniqueNum;
	}

	public void setUniqueNum(String uniqueNum) {
		this.uniqueNum = uniqueNum;
	}

	public String getName() {
		return name;
	}

	public MapType getMapType() {
		return mapType;
	}

	public void setMapType(MapType mapType) {
		this.mapType = mapType;
	}

	public List<JecnFigureData> getFigureList() {
		return figureList;
	}

	public void setFigureList(List<JecnFigureData> figureList) {
		this.figureList = figureList;
	}

	public List<JecnBaseLineData> getLineList() {
		return lineList;
	}

	public void setLineList(List<JecnBaseLineData> lineList) {
		this.lineList = lineList;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHFlag() {
		return isHFlag;
	}

	public void setHFlag(boolean isHFlag) {
		this.isHFlag = isHFlag;
	}

	public int getRoleCount() {
		return roleCount;
	}

	public void setRoleCount(int roleCount) {
		this.roleCount = roleCount;
	}

	public int getRoundRectCount() {
		return roundRectCount;
	}

	public void setRoundRectCount(int roundRectCount) {
		this.roundRectCount = roundRectCount;
	}

	public int getWorkflowWidth() {
		return workflowWidth;
	}

	public void setWorkflowWidth(int workflowWidth) {
		this.workflowWidth = workflowWidth;
	}

	public int getWorkflowHeight() {
		return workflowHeight;
	}

	public void setSize(int workflowWidth, int workflowHeight) {
		this.workflowWidth = workflowWidth;
		this.workflowHeight = workflowHeight;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	/**
	 * 
	 * 判断大小是否相同
	 * 
	 * @param workflowWidth
	 *            宽
	 * @param workflowHeight
	 *            高
	 * @return boolean true：相同；false：不相同
	 */
	public boolean isSizeSame(int workflowWidth, int workflowHeight) {
		if (this.workflowWidth == workflowWidth && this.workflowHeight == workflowHeight) {
			return true;
		} else {
			return false;
		}
	}

	public void setWorkflowHeight(int workflowHeight) {
		this.workflowHeight = workflowHeight;
	}

	public boolean isSave() {
		return isSave;
	}

	public void setSave(boolean isSave) {
		if (isAuthSave != 1) {// 没有当前面板保存权限
			isChange = true;
			this.isSave = false;
			return;
		}
		if (!this.isSave && isSave) {
			if (JecnSystemStaticData.isEprosDesigner()) {
				// 自动保存定时器重新启动
				JecnDesignerProcess.getDesignerProcess().timerRestart();
			}
		}
		this.isSave = isSave;
	}

	public void setSaveTrue() {
		setSave(true);
	}

	public JecnFlowOperationBean getFlowOperationBean() {
		return flowOperationBean;
	}

	public void setFlowOperationBean(JecnFlowOperationBean flowOperationBean) {
		this.flowOperationBean = flowOperationBean;
	}

	public TmpPageSetingBean getPageSetingBean() {
		return pageSetingBean;
	}

	public void setPageSetingBean(TmpPageSetingBean pageSetingBean) {
		this.pageSetingBean = pageSetingBean;
	}

	public int isAuthSave() {
		return isAuthSave;
	}

	public void setAuthSave(int isAuthSave) {
		this.isAuthSave = isAuthSave;
		if (isAuthSave == 1 && isChange) {
			setSaveTrue();
		}
	}

	public String getShowName() {

		boolean isNumShow = JecnDesignerProcess.getDesignerProcess().isNumShow();
		String flowIdInput = JecnDesignerProcess.getDesignerProcess().getFlowIdInput(this.flowId);
		String showName = name;
		if ((mapType.equals(MapType.partMap) || mapType.equals(MapType.totalMap)) && isNumShow) {

			if (flowIdInput != null && !"".equals(flowIdInput)) {
				showName = flowIdInput.trim() + " " + this.name;
			}

		}
		return showName;
	}

	public String getOccupierName() {
		return occupierName;
	}

	public void setOccupierName(String occupierName) {
		this.occupierName = occupierName;
	}

	public JecnSelectFigureXY getModelFigureXY() {
		return modelFigureXY;
	}

	public void setModelFigureXY(JecnSelectFigureXY modelFigureXY) {
		this.modelFigureXY = modelFigureXY;
	}

	public boolean isHiddenLineEditPoint() {
		return isHiddenLineEditPoint;
	}

	public void setHiddenLineEditPoint(boolean isHiddenLineEditPoint) {
		this.isHiddenLineEditPoint = isHiddenLineEditPoint;
	}
}
