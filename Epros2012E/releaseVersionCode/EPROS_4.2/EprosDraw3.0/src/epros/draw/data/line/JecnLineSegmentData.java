package epros.draw.data.line;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnXYData;

/**
 * 
 * 小线段数据类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnLineSegmentData {

	/** 小线段原始位置点和大小记录对象 */
	private JecnLineSegmentDataCloneable lineSegmentData = null;
	/** 小线段的主键ID */
	private long flowLineID = -1;
	/** 是否为结束线段 */
	private boolean isEndLine = false;

	private String UUID;
	/**
	 * 
	 * 
	 * 
	 * @param lineSegmentData
	 *            JecnLineSegmentDataCloneable 小线段原始位置点和大小记录对象
	 */
	public JecnLineSegmentData(JecnLineSegmentDataCloneable lineSegmentData) {
		if (lineSegmentData == null) {

		}
		this.lineSegmentData = lineSegmentData;
	}

	/**
	 * 
	 * 小线段开始点
	 * 
	 * @return JecnXYData 开始点
	 */
	public JecnXYData getOriginalStartXYData() {
		return lineSegmentData.getStartXY();
	}

	/**
	 * 
	 * 小线段结束点
	 * 
	 * @return JecnXYData 结束点
	 */
	public JecnXYData getOriginalEndXYData() {
		return lineSegmentData.getEndXY();
	}

	/**
	 * 
	 * 得到线段方向 错误情况
	 * 
	 * @return LineDirectionEnum 线方向类型
	 */
	public LineDirectionEnum getLineDirectionEnum() {
		return lineSegmentData.getLineDirectionEnum();
	}

	/**
	 * 
	 * 获取位置点Y坐标
	 * 
	 * @return double
	 */
	public int getMinXInt() {
		return lineSegmentData.getMinXInt();
	}

	/**
	 * 
	 * 获取位置点Y坐标
	 * 
	 * @return double
	 */
	public int getMinYInt() {
		return lineSegmentData.getMinYInt();
	}

	public JecnLineSegmentDataCloneable getLineSegmentData() {
		return lineSegmentData;
	}

	/**
	 * 获取线段数据层克隆对象
	 * 
	 */
	public JecnLineSegmentData clone() {
		JecnLineSegmentData dataClone = new JecnLineSegmentData(
				this.lineSegmentData.clone());
		dataClone.isEndLine = this.isEndLine;
		return dataClone;
	}

	public long getFlowLineID() {
		return flowLineID;
	}

	public void setFlowLineID(long flowLineID) {
		this.flowLineID = flowLineID;
	}

	public boolean isEndLine() {
		return isEndLine;
	}

	public void setEndLine(boolean isEndLine) {
		this.isEndLine = isEndLine;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}
}
