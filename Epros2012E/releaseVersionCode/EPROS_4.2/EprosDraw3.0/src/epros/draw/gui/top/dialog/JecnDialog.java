package epros.draw.gui.top.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 自定义对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDialog extends JDialog {
	protected final static Logger log = Logger.getLogger(JecnDialog.class);
	/** 对话框标题头的容器 */
	private DialogPanel dialogHeaderPanel = null;
	/** 对话框标题头 */
	private JecnDialogTitlePanel dialogTitlePanel = null;
	/** 对话框内容面板 */
	private JecnDialogResizeContentPanel dialogResizeContentPanel = null;
	/** 对话框内容面板内的提供给用户使用的面板 */
	private JPanel currContent = null;
	/** 用户给定面板 */
	private Container userContainer = null;

	/** 对话框监听对象 */
	private Map<JecnDialogListener, Object> dialogListenerList = null;

	public JecnDialog() {
		this(JecnSystemStaticData.getFrame(), false);
	}

	public JecnDialog(Dialog owner) {
		this(owner, false);
	}

	public JecnDialog(Dialog owner, boolean modal) {
		this(owner, null, modal);
	}

	public JecnDialog(Dialog owner, String title, boolean modal) {
		super(owner, title, modal);
		initComponents();
	}

	public JecnDialog(Frame owner) {
		this(owner, false);
	}

	public JecnDialog(Frame owner, boolean modal) {
		this(owner, null, modal);
	}

	public JecnDialog(Frame owner, String title) {
		this(owner, title, false);
	}

	public JecnDialog(Frame owner, String title, boolean modal) {
		super((owner == null) ? JecnSystemStaticData.getFrame() : owner, title, modal);
		initComponents();
	}

	private void initComponents() {
		// 对话框标题头的容器
		dialogHeaderPanel = new DialogPanel();
		// 对话框标题头
		dialogTitlePanel = new JecnDialogTitlePanel(this);
		// 对话框内容面板
		dialogResizeContentPanel = new JecnDialogResizeContentPanel(this);
		// 对话框内容面板内的提供给用户使用的面板
		currContent = new JPanel();
		// 边框
		dialogResizeContentPanel.setBorder(new JecnDialogBorder());
		// 布局
		dialogResizeContentPanel.setLayout(new BorderLayout());
		dialogHeaderPanel.add(dialogTitlePanel);
		dialogResizeContentPanel.add(dialogHeaderPanel, BorderLayout.NORTH);

		currContent.setOpaque(false);
		currContent.setBorder(null);
		currContent.setLayout(new BorderLayout());
		dialogResizeContentPanel.add(currContent, BorderLayout.CENTER);

		// 设置标题
		setTitle(getTitle());
		// 设置内容面板
		setContentPane(dialogResizeContentPanel);
		// 图片
		// setIconImage(getIconImage());
		// 禁用此对话框的装饰
		this.setUndecorated(true);
		// 设置此对话框是否可由用户调整大小
		this.setResizable(true);
		// 大小
		this.setSize(100, 20);

	}

	public void setResizable(boolean resizable) {
		super.setResizable(false);
		if (dialogTitlePanel != null) {
			dialogResizeContentPanel.setCursor(Cursor.getDefaultCursor());
			dialogTitlePanel.setResizable(resizable);
		}

	}

	/**
	 * 
	 * 关闭按钮隐藏
	 * 
	 */
	public void setExitBtnVisible(boolean b) {
		if (dialogTitlePanel != null) {
			dialogTitlePanel.setExitBtnVisible(b);
		}

	}

	public boolean isResizable() {
		if (dialogTitlePanel != null) {
			return dialogTitlePanel.isResizable();
		}

		return false;
	}

	/**
	 * 
	 * 禁用或启用此对话框的装饰
	 * 
	 */
	public void setUndecorated(boolean undecorated) {
		super.setUndecorated(true);
	}

	/**
	 * 
	 * 添加标题
	 * 
	 */
	public void setTitle(String title) {
		super.setTitle(title);

		// 往自定义的对话框标题面板中添加标题
		if (dialogTitlePanel != null) {
			dialogTitlePanel.setTitle(title);
		}
	}

	public Container getContentPane() {
		return currContent;
	}

	/**
	 * 
	 * 设置 contentPane 属性
	 * 
	 */
	public void setContentPane(Container contentPane) {
		if (contentPane == null) {
			return;
		} else if (dialogResizeContentPanel == contentPane) {
			super.setContentPane(contentPane);
		} else {
			if (userContainer != null) {
				currContent.remove(userContainer);
			}
			userContainer = contentPane;
			currContent.add(userContainer, BorderLayout.CENTER);
		}
	}

	public Image getIconImage() {
		if (dialogTitlePanel != null) {
			return dialogTitlePanel.getIcon();
		}
		return null;
	}

	public void setIconImage(Image image) {
		super.setIconImage(image);

		// 往自定义的对话框标题面板中添加标题
		if (dialogTitlePanel != null) {
			dialogTitlePanel.setIcon(image);
		}
	}

	/**
	 * 
	 * 获取工具栏标题容器
	 * 
	 * @return
	 */
	public JecnDialogTitlePanel getToolTitlePanel() {
		return dialogTitlePanel;
	}

	Container getCurrContent() {
		return currContent;
	}

	DialogPanel getDialogHeaderPanel() {
		return dialogHeaderPanel;
	}

	/**
	 * 
	 * 此方法必须有，保证JecnDialog释放资源是能成功释放
	 * 
	 */
	public void setVisible(boolean b) {
		try {
			// 清除所有成员变量,释放内存资源
			if (!b && dialogTitlePanel != null) {
				dialogTitlePanel.setVirtualDialogFlase();
			}
			// 当关闭模式是DISPOSE_ON_CLOSE时，先释放JecnDialog成员变量资源再调用父类setVisible方法
			if (!b && JDialog.DISPOSE_ON_CLOSE == getDefaultCloseOperation()) {// 隐藏销毁对话框时
				// 清除所有成员变量,释放内存资源
				dialogTitlePanel = null;
				dialogResizeContentPanel = null;
				currContent = null;
				dialogListenerList = null;
			}
			super.setVisible(b);
		} catch (Exception e) {

		}
	}

	/**
	 * 
	 * 销毁
	 * 
	 */
	public void dispose() {
		// 清除所有成员变量,释放内存资源
		if (dialogTitlePanel != null) {
			dialogTitlePanel.setVirtualDialogFlase();
		}
		dialogTitlePanel = null;
		dialogResizeContentPanel = null;
		currContent = null;
		dialogListenerList = null;
		super.dispose();
	}

	/**
	 * 
	 * 隐藏按钮
	 * 
	 */
	public void hiddBtns() {
		dialogTitlePanel.hiddBtns();
	}

	/**
	 * 
	 * 添加监听
	 * 
	 * @param frameListener
	 *            JecnDialogListener
	 * 
	 */
	public void addJecnDialogListener(JecnDialogListener dialogListener) {
		addJecnDialogListener(dialogListener, this);
	}

	/**
	 * 
	 * 添加监听
	 * 
	 * @param frameListener
	 *            JecnDialogListener
	 * @param obj
	 *            Object 监听对象
	 * 
	 */
	public void addJecnDialogListener(JecnDialogListener dialogListener, Object obj) {
		if (dialogListener == null || obj == null) {
			return;
		}
		if (dialogListenerList == null) {
			dialogListenerList = new HashMap<JecnDialogListener, Object>();
		}
		if (!dialogListenerList.keySet().contains(dialogListener)) {
			dialogListenerList.put(dialogListener, obj);
		}
	}

	Map<JecnDialogListener, Object> getDialogListenerList() {
		return dialogListenerList;
	}

	/**
	 * 
	 * 点击退出前信息提示
	 * 
	 * @return boolean true：需要执行关闭 false：不要执行关闭
	 */
	protected boolean dialogCloseBeforeMsgTip() {
		// 提示是否退出
		int isOk = JecnOptionPane.showConfirmDialog(this, JecnResourceUtil.getJecnResourceUtil().getValue(
				"closeDialogMsg"), null, JecnOptionPane.OK_CANCEL_OPTION);

		if (JOptionPane.OK_OPTION == isOk) {
			return true;
		}
		return false;
	}

	/**
	 * 点击取消 提示保存 提示框
	 * 
	 * @return
	 */
	protected int dialogCloseBeforeMsgTipAction() {
		// true已经显示 false没有显示
		int option = JecnOptionPane.showConfirmDialog(this, JecnDrawMainPanel.getMainPanel().getResourceManager()
				.getValue("optionInfo"), null, JecnOptionPane.YES_NO_CANCEL_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.YES_OPTION) {
			return 2;
		} else if (option == JecnOptionPane.NO_OPTION) {
			return 1;
		} else {
			return 0;
		}

	}

	/**
	 * 最大宽度
	 * 
	 * @return
	 */
	public int getWidthMax() {
		return 700;
	}

	/**
	 * 最小宽度
	 * 
	 * @return
	 */
	public int getWidthMin() {
		return 410;
	}

	/**
	 * 
	 * 
	 * 对话框头面板容器
	 * 
	 * @author ZHOUXY
	 * 
	 */
	class DialogPanel extends JPanel {
		private Color changColor = new Color(221, 227, 232);

		public DialogPanel() {
			this.setBorder(null);
			this.setLayout(new BorderLayout());
		}

		/**
		 * 
		 * 绘制组件
		 * 
		 */
		protected void paintComponent(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;
			// 整体渐变
			g2d.setPaint(new GradientPaint(0, 0, Color.WHITE, 0, this.getHeight() / 2 + 5, changColor));
			g2d.fillRect(0, 0, this.getWidth(), this.getHeight());

			// 标题下边线
			g2d.setColor(JecnUIUtil.getDialogBorderColor());
			g2d.drawLine(0, this.getHeight() - 1, this.getWidth(), this.getHeight() - 1);
			// 重置颜色
			g2d.setColor(JecnUIUtil.getDefaultBackgroundColor());
		}
	}
}
