package com.jecn.epros.domain.org;

import io.swagger.annotations.ApiModelProperty;

public class Department {

	@ApiModelProperty(value = "主键", position = 0)
	private Long id;

	@ApiModelProperty(value = "名称", position = 1)
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
