package com.jecn.epros.server.dao.task;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.IBaseDao;

/**
 * 任务审批提醒Dao
 * 
 * @author weidp
 * @date： 日期：2014-5-14 时间：下午05:09:23
 */
public interface IJecnTaskApproveTipDao extends IBaseDao<JecnUser, Long> {

	/**
	 * 获取任务超时未审批的用户集合
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-14 时间：下午05:48:09
	 * @param tipDay
	 *            用户在设计器配置的审批超时天数
	 * @return
	 */
	List<Object[]> getTaskTipUserListByTipDay(int tipDay);

}
