package com.jecn.epros.server.download.wordxml.dongruanyiliao;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.BorderBean;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellBean;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.WordGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 东软医疗操作说明模版
 * 
 * @author hyl
 * 
 */
public class DRYLProcessModel extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	private final Map<Long, List<String[]>> approves = new HashMap<Long, List<String[]>>();

	public DRYLProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		super.setDefAscii("Arial");
		super.setDefFareast("宋体");
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(14F);
		getDocProperty().setNewRowHeight(0.7F);
		getDocProperty().setFlowChartScaleValue(7.5F);
		getDocProperty().setFlowChartDirection(true);
		setDocStyle(" ", textTitleStyle());

	}

	@Override
	protected void a20(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		createTitle(processFileItem);
		/** 记录保存数据 0 记录名称 1保存责任人 2保存场所 3归档时间 4保存期限 5到期处理方式 6编号 7移交责任 */
		float[] width = new float[] { 7F, 4F, 3F };
		// 记录名称
		String recordName = JecnUtil.getValue("recordName");
		String number = "记录编码";
		String org = "归档部门";

		List<String[]> rowData = new ArrayList<String[]>();
		// 标题数组
		String[] title = new String[] { recordName, number, org };
		rowData.add(title);
		for (String[] row : oldRowData) {
			rowData.add(new String[] { row[0], row[6], processDownloadBean.getOrgName() });
		}
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), true);
	}

	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {
		// 段落显示 1:段落；0:表格（默认）
		if (processDownloadBean.getFlowFileType() == 1) {
			a10InParagraph(processFileItem, rowData);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 添加标题
		rowData.add(0, titles);
		float[] width = new float[] { 2.09F, 2.09F, 2.31F, 5, 3, 3 };
		Table tbl = createTableItem(processFileItem, width, rowData);
		// 设置第一行居中
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		super.word.paragraphStyleKpi(processFileItem, oldRowData);
	}

	/**
	 * 流程文控信息
	 * 
	 * @param name
	 * @param documentList
	 * @param contentSect
	 */
	protected void a24(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 版本号
		String version = JecnUtil.getValue("versionNumber");
		String implementData = "实施日期";
		String auth = "作者";
		String change = "更改内容";
		String demand = "培训需求";
		rowData = new ArrayList<String[]>();
		rowData.add(new String[] { version, implementData, auth, change, demand });
		float[] width = new float[] { 2, 6F, 3F, 2.4F, 4.5F };
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		for (JecnTaskHistoryNew history : historys) {
			rowData.add(new String[] { history.getVersionId(), super.dateToStr(history.getImplementDate()),
					history.getDraftPerson(), history.getModifyExplain(), "" });
		}
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	@Override
	public void afterHandle() {
		docProperty.getSecondSect().createMutilEmptyParagraph(3);
		docProperty.getSecondSect().createParagraph("正文结束End of Document",
				new ParagraphBean(Align.center, Constants.wuhao, true));
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, new ParagraphLineRule(20F, LineRuleType.EXACT));
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 3.17F, 2.54F, 3.17F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(1F, 2F, 2.4F, 2F, 1F, 1.75F);
		titleSect.setSectBean(sectBean);
		addTitleSectContent();
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent() {

		Sect titleSect = docProperty.getTitleSect();
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 10.5F));
		// 设置页边距
		sectBean.setPage(2.54F, 3.17F, 2.54F, 3.17F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		titleSect.setSectBean(sectBean);

		Paragraph emptyParagraph = createEmptyParagraph();
		titleSect.addParagraph(emptyParagraph);
		titleSect.addParagraph(emptyParagraph);
		// 流程名称
		createProcessName(titleSect);
		createTitlehdr(titleSect);
		titleSect.addParagraph(emptyParagraph);
		titleSect.addParagraph(emptyParagraph);
		createApprove(titleSect);
		titleSect.addParagraph(emptyParagraph);
		titleSect.addParagraph(emptyParagraph);
		createCompany(titleSect);
	}

	private void createCompany(Sect titleSect) {
		titleSect.createParagraph(processDownloadBean.getCompanyName(), new ParagraphBean(Align.center, Constants.xiaosi, true));
		titleSect.createParagraph("NEUSOFT MEDICAL SYSTEMS CO.,LTD", new ParagraphBean(Align.center, Constants.xiaosi,
				true));
	}

	private void createApprove(Sect titleSect) {
		JecnTaskHistoryNew latestHistoryNew = processDownloadBean.getLatestHistoryNew();
		List<JecnTaskHistoryFollow> listJecnTaskHistoryFollow = new ArrayList<JecnTaskHistoryFollow>();
		if (latestHistoryNew != null) {
			listJecnTaskHistoryFollow = latestHistoryNew.getListJecnTaskHistoryFollow();
		}

		final FontBean zh = getZhFontBean(Constants.xiaosi);
		final FontBean en = getEnFontBean(Constants.xiaosi);
		ParagraphBean pBean = new ParagraphBean();
		pBean.setAlign(Align.center);
		pBean.setFontBean(new FontBean(Constants.xiaosi));

		Paragraph author = new Paragraph("", pBean);
		author.appendTextRun("拟稿", zh);
		author.appendTextRun("Author", en);

		Paragraph review = new Paragraph("", pBean);
		review.appendTextRun("审核", zh);
		review.appendTextRun("Review", en);

		Paragraph approval = new Paragraph("", pBean);
		approval.appendTextRun("批准", zh);
		approval.appendTextRun("Approval", en);

		Paragraph effectiveDate = new Paragraph("", pBean);
		effectiveDate.appendTextRun("实施日期", zh);
		effectiveDate.appendTextRun("Effective Date", en);

		Paragraph sign = new Paragraph("", pBean);
		sign.appendTextRun("签名", zh);
		sign.appendTextRun("Sign/", en);
		sign.appendTextRun("日期", zh);
		sign.appendTextRun("Date", en);

		Paragraph function = new Paragraph("", pBean);
		function.appendTextRun("部门", zh);
		function.appendTextRun("Function/", en);
		function.appendTextRun("职务", zh);
		function.appendTextRun("Title", en);

		String pubTime = latestHistoryNew == null ? "" : dateToStr(latestHistoryNew.getPublishDate());

		// 拟稿
		String authorPeople = latestHistoryNew == null ? "" : nullToEmpty(latestHistoryNew.getDraftPerson());
		String authorPeopleTime = latestHistoryNew == null ? "" : latestHistoryNew.getStartTime();

		Set<Long> draftPeopleIds = new HashSet<Long>();
		Set<Long> reviewPeopleIds = new HashSet<Long>();
		Set<Long> approveIds = new HashSet<Long>();
		// 审核阶段
		String reviewPeople = "";
		String reviewPeopleTime = "";
		// 批准人
		String approvalPeople = "";
		String approvalPeopleTime = "";
		for (JecnTaskHistoryFollow p : listJecnTaskHistoryFollow) {
			Integer state = p.getStageMark();
			if (state == null) {
				continue;
			}
			if (state == 11) {
				p.getId();
				reviewPeopleIds = getIds(p.getApproveId());
				reviewPeople = p.getName();
				reviewPeopleTime = dateToStr(p.getApprovalTime());
			} else if (state == 4) {
				approveIds = getIds(p.getApproveId());
				approvalPeople = p.getName();
				approvalPeopleTime = dateToStr(p.getApprovalTime());
			} else if (state == 0) {
				draftPeopleIds = getIds(p.getApproveId());
				authorPeople = p.getName();
				authorPeopleTime = dateToStr(p.getApprovalTime());
			}
		}

		Set<Long> ids = new HashSet<Long>();
		ids.addAll(draftPeopleIds);
		ids.addAll(reviewPeopleIds);
		ids.addAll(approveIds);
		// 搞这么复杂只是为了少查几遍
		initApproveOrgAndPos(ids);

		TableBean tableBean = new TableBean();
		BorderBean borderBottom = new BorderBean(LineType.THINK, 1F, Color.BLACK);
		tableBean.setBorder(borderBottom);
		tableBean.setTableAlign(Align.center);
		Table table = titleSect.createTable(tableBean, new float[] { 3.25F, 7.5F, 5.25F });
		List<String[]> data = new ArrayList<String[]>();
		data.add(new String[] { "", "签名Sign/日期Date", "部门Function/职务Title" });
		data.add(new String[] { "编制Author", concatApprovePeopleAndTime(authorPeople, pubTime, authorPeopleTime),
				getOrgAndPos(draftPeopleIds) });
		data.add(new String[] { "审核Review", concatApprovePeopleAndTime(reviewPeople, pubTime, reviewPeopleTime),
				getOrgAndPos(reviewPeopleIds) });
		data.add(new String[] { "批准Approval", concatApprovePeopleAndTime(approvalPeople, pubTime, approvalPeopleTime),
				getOrgAndPos(approveIds) });
		data.add(new String[] { "实施日期Effective Date", processDownloadBean.getImplementDate(), "" });
		table.createTableRow(data, pBean);
		table.setValign(Valign.center);
		table.getRow(0).setHeight(1F);
		table.getRow(1).setHeight(1F);
		table.getRow(2).setHeight(6F);
		table.getRow(3).setHeight(1F);
		table.getRow(4).setHeight(2F);
	}

	private String getOrgAndPos(Set<Long> idSet) {
		if (idSet == null || idSet.size() == 0) {
			return "";
		}
		StringBuffer buf = new StringBuffer();
		for (Long id : idSet) {
			List<String[]> orgAndPos = approves.get(id);
			if (orgAndPos != null) {
				for (String[] str : orgAndPos) {
					buf.append(str[2]);
					buf.append("/");
					buf.append(str[1]);
					buf.append("\n");
				}
			}
		}
		return buf.toString();
	}

	private Set<Long> getIds(String idStr) {
		Set<Long> ids = new HashSet<Long>();

		if (StringUtils.isNotBlank(idStr)) {
			String[] idArray = idStr.split(",");
			for (String id : idArray) {
				if (StringUtils.isNotBlank(id)) {
					ids.add(Long.valueOf(id));
				}
			}
		}
		return ids;

	}

	private void initApproveOrgAndPos(Set<Long> idSet) {
		if (idSet == null || idSet.size() == 0) {
			return;
		}
		String sql = "	select ju.people_id,ju.TRUE_NAME,jfoi.FIGURE_TEXT,jfo.ORG_NAME from JECN_USER ju"
				+ "	left join JECN_USER_POSITION_RELATED jupr on ju.people_id=jupr.people_id"
				+ "	left join JECN_FLOW_ORG_IMAGE jfoi on jfoi.figure_id=jupr.figure_id"
				+ "	left join JECN_FLOW_ORG jfo on jfoi.ORG_ID=jfo.ORG_ID" + "  where ju.people_id in"
				+ JecnCommonSql.getIdsSet(idSet);
		List<Object[]> objs = processDownloadBean.getBaseDao().listNativeSql(sql);
		if (objs.size() > 0) {
			for (Object[] obj : objs) {
				putOrElseUpdate(Long.valueOf(obj[0].toString()), new String[] { nullObjToEmpty(obj[1]),
						nullObjToEmpty(obj[2]), nullObjToEmpty(obj[3]) });
			}
		}
	}

	private void putOrElseUpdate(Long peopleId, String[] info) {
		List<String[]> list = approves.get(peopleId);
		if (list == null) {
			approves.put(peopleId, new ArrayList<String[]>());
		}
		approves.get(peopleId).add(info);
	}

	private String nullObjToEmpty(Object obj) {
		if (obj == null) {
			return "";
		} else {
			return obj.toString().trim();
		}
	}

	private String getApproveTime(String pubTime, String approveTime) {
		if (approveTime == null || "".equals(approveTime)) {
			return pubTime;
		}
		return approveTime;
	}

	private String concatApprovePeopleAndTime(String people, String pubTime, String approveTime) {
		if (people == null || "".equals(people)) {
			return "";
		}
		return nullToEmpty(people) + "/" + getApproveTime(pubTime, approveTime);
	}

	private Paragraph createEmptyParagraph() {
		Paragraph paragraph = new Paragraph("", new ParagraphBean(Align.center, Constants.sihao, false));
		return paragraph;
	}

	private void createProcessName(Sect titleSect) {
		String flowName = nullToEmpty(processDownloadBean.getFlowName());
		ParagraphBean pBean = new ParagraphBean(Align.center, Constants.erhao, true);
		ParagraphLineRule vLine1 = new ParagraphLineRule(34F, LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph paragraph = new Paragraph(flowName, pBean);
		titleSect.addParagraph(paragraph);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		createCommfhdr(firstSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setSize(29.7F, 21F);
		flowChartSect.setSectBean(sectBean);
		createCharfhdr(flowChartSect);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, float left, float center, float right) {
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		PositionBean positionBean = new PositionBean(PositionType.absolute, -20, 0, 365F, 0.32F);
		Image logoImage = (Image) getLogoImage();
		logoImage.setPositionBean(positionBean);
		header.createParagraph(logoImage, new ParagraphBean(Align.right));

		TableBean tableBean = new TableBean();
		tableBean.setBorderBottom(1F);

		String version = processDownloadBean.getLatestHistoryNew() == null ? "" : nullToEmpty(processDownloadBean
				.getLatestHistoryNew().getVersionId());
		Table table = header.createTable(tableBean, new float[] { left, center, right });
		table.createTableRow(new String[] { processDownloadBean.getFlowName(), "文件编号Document No:",
				processDownloadBean.getFlowInputNum() }, new ParagraphBean(Align.right, Constants.xiaowu, false));
		table.createTableRow(new String[] { "", "版本Revision:", version }, new ParagraphBean(Align.right,
				Constants.xiaowu, false));
		table.createTableRow(new String[] { "", "页码Page:", "" },
				new ParagraphBean(Align.right, Constants.xiaowu, false));
		Paragraph page = table.getRow(2).getCell(2).getParagraph(0);
		page.appendCurPageRun();
		page.appendTextRun("of");
		page.appendTotalPagesRun();

		table.getRow(0).getCell(0).isVmergeBeg();
		table.getRow(2).getCell(0).isVmerge();
		table.getRow(0).getCell(0).setRowspan(3);

		// 左对齐
		CellBean cellBean = new CellBean();
		cellBean.setBorderRight(1F);
		for (TableRow row : table.getRows()) {
			row.getCell(0).setCellBean(cellBean);
			row.setHeight(0.5F);
			row.getCell(2).getParagraph(0).setParagraphBean(new ParagraphBean(Align.left, Constants.xiaowu, false));
			row.getCell(0).getParagraph(0).setParagraphBean(new ParagraphBean(Align.center, Constants.xiaosi, true));
		}
		table.setValign(Valign.center);
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 7.52F, 4.58F, 2.95F);
		createCommfdr(sect);
	}

	private void createCommfdr(Sect sect) {
		Footer footer = sect.createFooter(HeaderOrFooterType.odd);
		footer.createParagraph("Chinese version is official, English version is for reference.", new ParagraphBean(
				Align.center, Constants.xiaowu, false));
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createTitlehdr(Sect sect) {
		final FontBean zh = getZhFontBean(Constants.xiaosi);
		final FontBean en = getEnFontBean(Constants.xiaosi);
		ParagraphBean pBean = new ParagraphBean(Align.left, Constants.xiaosi, false);
		Header header = sect.createHeader(HeaderOrFooterType.odd);

		ParagraphBean imagePbean = new ParagraphBean(Align.right);
		Paragraph image = new Paragraph("", imagePbean);
		image.appendGraphRun(this.getLogoImage());
		header.addParagraph(image);

		Paragraph number = new Paragraph("", pBean);
		number.appendTextRun("文件编号", zh);
		number.appendTextRun("Document No：", en);
		number.appendTextRun(nullToEmpty(processDownloadBean.getFlowInputNum()));

		Paragraph org = new Paragraph("", pBean);
		org.appendTextRun("归属部门", zh);
		org.appendTextRun("Doc. Owner：", en);
		org.appendTextRun(nullToEmpty(processDownloadBean.getOrgName()));

		Paragraph secrecy = new Paragraph("", pBean);
		secrecy.appendTextRun("保密等级", zh);
		secrecy.appendTextRun("Secrecy：", en);
		secrecy.appendTextRun(nullToEmpty(super.getPubOrSec()));

		Paragraph revision = new Paragraph("", pBean);
		revision.appendTextRun("版本", zh);
		revision.appendTextRun("Revision：", en);
		revision.appendTextRun(nullToEmpty(processDownloadBean.getFlowVersion()));

		TableBean tableBean = new TableBean();
		tableBean.setBorderBottom(1F);

		Table table = header.createTable(tableBean, new float[] { 14.5F });
		TableRow row = table.createTableRow();
		TableCell cell = row.createTableCell();
		cell.addParagraph(number);
		cell.addParagraph(org);
		cell.addParagraph(secrecy);
		cell.addParagraph(revision);
	}

	private WordGraph getLogoImage() {
		return super.getLogoImage(3.25F, 0.6F);
	}

	public FontBean getZhFontBean(int fontSize) {
		FontBean fontBean = getZhFontBean();
		fontBean.setFontSize(fontSize);
		return fontBean;
	}

	public FontBean getEnFontBean(int fontSize) {
		FontBean fontBean = getEnFontBean();
		fontBean.setFontSize(fontSize);
		return fontBean;
	}

	public FontBean getZhFontBean() {
		FontBean fontBean = new FontBean();
		fontBean.setFontFamily("宋体");
		return fontBean;
	}

	public FontBean getEnFontBean() {
		FontBean fontBean = new FontBean();
		fontBean.setFontFamily("Arial");
		return fontBean;
	}

	/***
	 * 创建流程图页眉页脚
	 * 
	 * @param sect
	 */
	private void createCharfhdr(Sect sect) {
		createCommhdr(sect, 10.52F, 6.58F, 4.95F);
		createCommfdr(sect);
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Constants.wuhao);
		textContentStyle.setInd(0F, 0F, 0F, 1F);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(12F, LineRuleType.AT_LEAST));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		FontBean font = new FontBean();
		font.setB(true);
		font.setFontSize(Constants.sihao);
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, font);
		textTitleStyle.setInd(0F, 0F, 0.76F, 0F);
		textTitleStyle.setSpace(0.5F, 0.5F, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		FontBean font = new FontBean();
		font.setFontSize(Constants.wuhao);
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left);
		tblContentStyle.setFontBean(font);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.94 厘米
		tblStyle.setTableLeftInd(1F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

}
