/**
 * TB_INF_EMPCONTACTLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crs.dataimport;

public class TB_INF_EMPCONTACTLine  implements java.io.Serializable {
    private java.math.BigDecimal c_empoid;

    private java.math.BigDecimal versioncode;

    private java.util.Date versiontime;

    private java.lang.String c_hometel;

    private java.lang.String versiondelete;

    private java.lang.String c_postalcode;

    private java.math.BigDecimal versionid;

    private java.lang.String c_worktel;

    private java.lang.String c_companymail;

    private java.lang.String c_privatemail;

    private java.lang.String c_txbz;

    private java.math.BigDecimal c_operator;

    private java.math.BigDecimal c_oid;

    private java.util.Date c_operatetime;

    private java.lang.String c_mobile;

    private java.lang.String c_status;

    private java.lang.String c_address;

    public TB_INF_EMPCONTACTLine() {
    }

    public TB_INF_EMPCONTACTLine(
           java.math.BigDecimal c_empoid,
           java.math.BigDecimal versioncode,
           java.util.Date versiontime,
           java.lang.String c_hometel,
           java.lang.String versiondelete,
           java.lang.String c_postalcode,
           java.math.BigDecimal versionid,
           java.lang.String c_worktel,
           java.lang.String c_companymail,
           java.lang.String c_privatemail,
           java.lang.String c_txbz,
           java.math.BigDecimal c_operator,
           java.math.BigDecimal c_oid,
           java.util.Date c_operatetime,
           java.lang.String c_mobile,
           java.lang.String c_status,
           java.lang.String c_address) {
           this.c_empoid = c_empoid;
           this.versioncode = versioncode;
           this.versiontime = versiontime;
           this.c_hometel = c_hometel;
           this.versiondelete = versiondelete;
           this.c_postalcode = c_postalcode;
           this.versionid = versionid;
           this.c_worktel = c_worktel;
           this.c_companymail = c_companymail;
           this.c_privatemail = c_privatemail;
           this.c_txbz = c_txbz;
           this.c_operator = c_operator;
           this.c_oid = c_oid;
           this.c_operatetime = c_operatetime;
           this.c_mobile = c_mobile;
           this.c_status = c_status;
           this.c_address = c_address;
    }


    /**
     * Gets the c_empoid value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_empoid
     */
    public java.math.BigDecimal getC_empoid() {
        return c_empoid;
    }


    /**
     * Sets the c_empoid value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_empoid
     */
    public void setC_empoid(java.math.BigDecimal c_empoid) {
        this.c_empoid = c_empoid;
    }


    /**
     * Gets the versioncode value for this TB_INF_EMPCONTACTLine.
     * 
     * @return versioncode
     */
    public java.math.BigDecimal getVersioncode() {
        return versioncode;
    }


    /**
     * Sets the versioncode value for this TB_INF_EMPCONTACTLine.
     * 
     * @param versioncode
     */
    public void setVersioncode(java.math.BigDecimal versioncode) {
        this.versioncode = versioncode;
    }


    /**
     * Gets the versiontime value for this TB_INF_EMPCONTACTLine.
     * 
     * @return versiontime
     */
    public java.util.Date getVersiontime() {
        return versiontime;
    }


    /**
     * Sets the versiontime value for this TB_INF_EMPCONTACTLine.
     * 
     * @param versiontime
     */
    public void setVersiontime(java.util.Date versiontime) {
        this.versiontime = versiontime;
    }


    /**
     * Gets the c_hometel value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_hometel
     */
    public java.lang.String getC_hometel() {
        return c_hometel;
    }


    /**
     * Sets the c_hometel value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_hometel
     */
    public void setC_hometel(java.lang.String c_hometel) {
        this.c_hometel = c_hometel;
    }


    /**
     * Gets the versiondelete value for this TB_INF_EMPCONTACTLine.
     * 
     * @return versiondelete
     */
    public java.lang.String getVersiondelete() {
        return versiondelete;
    }


    /**
     * Sets the versiondelete value for this TB_INF_EMPCONTACTLine.
     * 
     * @param versiondelete
     */
    public void setVersiondelete(java.lang.String versiondelete) {
        this.versiondelete = versiondelete;
    }


    /**
     * Gets the c_postalcode value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_postalcode
     */
    public java.lang.String getC_postalcode() {
        return c_postalcode;
    }


    /**
     * Sets the c_postalcode value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_postalcode
     */
    public void setC_postalcode(java.lang.String c_postalcode) {
        this.c_postalcode = c_postalcode;
    }


    /**
     * Gets the versionid value for this TB_INF_EMPCONTACTLine.
     * 
     * @return versionid
     */
    public java.math.BigDecimal getVersionid() {
        return versionid;
    }


    /**
     * Sets the versionid value for this TB_INF_EMPCONTACTLine.
     * 
     * @param versionid
     */
    public void setVersionid(java.math.BigDecimal versionid) {
        this.versionid = versionid;
    }


    /**
     * Gets the c_worktel value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_worktel
     */
    public java.lang.String getC_worktel() {
        return c_worktel;
    }


    /**
     * Sets the c_worktel value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_worktel
     */
    public void setC_worktel(java.lang.String c_worktel) {
        this.c_worktel = c_worktel;
    }


    /**
     * Gets the c_companymail value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_companymail
     */
    public java.lang.String getC_companymail() {
        return c_companymail;
    }


    /**
     * Sets the c_companymail value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_companymail
     */
    public void setC_companymail(java.lang.String c_companymail) {
        this.c_companymail = c_companymail;
    }


    /**
     * Gets the c_privatemail value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_privatemail
     */
    public java.lang.String getC_privatemail() {
        return c_privatemail;
    }


    /**
     * Sets the c_privatemail value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_privatemail
     */
    public void setC_privatemail(java.lang.String c_privatemail) {
        this.c_privatemail = c_privatemail;
    }


    /**
     * Gets the c_txbz value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_txbz
     */
    public java.lang.String getC_txbz() {
        return c_txbz;
    }


    /**
     * Sets the c_txbz value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_txbz
     */
    public void setC_txbz(java.lang.String c_txbz) {
        this.c_txbz = c_txbz;
    }


    /**
     * Gets the c_operator value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_operator
     */
    public java.math.BigDecimal getC_operator() {
        return c_operator;
    }


    /**
     * Sets the c_operator value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_operator
     */
    public void setC_operator(java.math.BigDecimal c_operator) {
        this.c_operator = c_operator;
    }


    /**
     * Gets the c_oid value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_oid
     */
    public java.math.BigDecimal getC_oid() {
        return c_oid;
    }


    /**
     * Sets the c_oid value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_oid
     */
    public void setC_oid(java.math.BigDecimal c_oid) {
        this.c_oid = c_oid;
    }


    /**
     * Gets the c_operatetime value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_operatetime
     */
    public java.util.Date getC_operatetime() {
        return c_operatetime;
    }


    /**
     * Sets the c_operatetime value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_operatetime
     */
    public void setC_operatetime(java.util.Date c_operatetime) {
        this.c_operatetime = c_operatetime;
    }


    /**
     * Gets the c_mobile value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_mobile
     */
    public java.lang.String getC_mobile() {
        return c_mobile;
    }


    /**
     * Sets the c_mobile value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_mobile
     */
    public void setC_mobile(java.lang.String c_mobile) {
        this.c_mobile = c_mobile;
    }


    /**
     * Gets the c_status value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_status
     */
    public java.lang.String getC_status() {
        return c_status;
    }


    /**
     * Sets the c_status value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_status
     */
    public void setC_status(java.lang.String c_status) {
        this.c_status = c_status;
    }


    /**
     * Gets the c_address value for this TB_INF_EMPCONTACTLine.
     * 
     * @return c_address
     */
    public java.lang.String getC_address() {
        return c_address;
    }


    /**
     * Sets the c_address value for this TB_INF_EMPCONTACTLine.
     * 
     * @param c_address
     */
    public void setC_address(java.lang.String c_address) {
        this.c_address = c_address;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TB_INF_EMPCONTACTLine)) return false;
        TB_INF_EMPCONTACTLine other = (TB_INF_EMPCONTACTLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.c_empoid==null && other.getC_empoid()==null) || 
             (this.c_empoid!=null &&
              this.c_empoid.equals(other.getC_empoid()))) &&
            ((this.versioncode==null && other.getVersioncode()==null) || 
             (this.versioncode!=null &&
              this.versioncode.equals(other.getVersioncode()))) &&
            ((this.versiontime==null && other.getVersiontime()==null) || 
             (this.versiontime!=null &&
              this.versiontime.equals(other.getVersiontime()))) &&
            ((this.c_hometel==null && other.getC_hometel()==null) || 
             (this.c_hometel!=null &&
              this.c_hometel.equals(other.getC_hometel()))) &&
            ((this.versiondelete==null && other.getVersiondelete()==null) || 
             (this.versiondelete!=null &&
              this.versiondelete.equals(other.getVersiondelete()))) &&
            ((this.c_postalcode==null && other.getC_postalcode()==null) || 
             (this.c_postalcode!=null &&
              this.c_postalcode.equals(other.getC_postalcode()))) &&
            ((this.versionid==null && other.getVersionid()==null) || 
             (this.versionid!=null &&
              this.versionid.equals(other.getVersionid()))) &&
            ((this.c_worktel==null && other.getC_worktel()==null) || 
             (this.c_worktel!=null &&
              this.c_worktel.equals(other.getC_worktel()))) &&
            ((this.c_companymail==null && other.getC_companymail()==null) || 
             (this.c_companymail!=null &&
              this.c_companymail.equals(other.getC_companymail()))) &&
            ((this.c_privatemail==null && other.getC_privatemail()==null) || 
             (this.c_privatemail!=null &&
              this.c_privatemail.equals(other.getC_privatemail()))) &&
            ((this.c_txbz==null && other.getC_txbz()==null) || 
             (this.c_txbz!=null &&
              this.c_txbz.equals(other.getC_txbz()))) &&
            ((this.c_operator==null && other.getC_operator()==null) || 
             (this.c_operator!=null &&
              this.c_operator.equals(other.getC_operator()))) &&
            ((this.c_oid==null && other.getC_oid()==null) || 
             (this.c_oid!=null &&
              this.c_oid.equals(other.getC_oid()))) &&
            ((this.c_operatetime==null && other.getC_operatetime()==null) || 
             (this.c_operatetime!=null &&
              this.c_operatetime.equals(other.getC_operatetime()))) &&
            ((this.c_mobile==null && other.getC_mobile()==null) || 
             (this.c_mobile!=null &&
              this.c_mobile.equals(other.getC_mobile()))) &&
            ((this.c_status==null && other.getC_status()==null) || 
             (this.c_status!=null &&
              this.c_status.equals(other.getC_status()))) &&
            ((this.c_address==null && other.getC_address()==null) || 
             (this.c_address!=null &&
              this.c_address.equals(other.getC_address())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getC_empoid() != null) {
            _hashCode += getC_empoid().hashCode();
        }
        if (getVersioncode() != null) {
            _hashCode += getVersioncode().hashCode();
        }
        if (getVersiontime() != null) {
            _hashCode += getVersiontime().hashCode();
        }
        if (getC_hometel() != null) {
            _hashCode += getC_hometel().hashCode();
        }
        if (getVersiondelete() != null) {
            _hashCode += getVersiondelete().hashCode();
        }
        if (getC_postalcode() != null) {
            _hashCode += getC_postalcode().hashCode();
        }
        if (getVersionid() != null) {
            _hashCode += getVersionid().hashCode();
        }
        if (getC_worktel() != null) {
            _hashCode += getC_worktel().hashCode();
        }
        if (getC_companymail() != null) {
            _hashCode += getC_companymail().hashCode();
        }
        if (getC_privatemail() != null) {
            _hashCode += getC_privatemail().hashCode();
        }
        if (getC_txbz() != null) {
            _hashCode += getC_txbz().hashCode();
        }
        if (getC_operator() != null) {
            _hashCode += getC_operator().hashCode();
        }
        if (getC_oid() != null) {
            _hashCode += getC_oid().hashCode();
        }
        if (getC_operatetime() != null) {
            _hashCode += getC_operatetime().hashCode();
        }
        if (getC_mobile() != null) {
            _hashCode += getC_mobile().hashCode();
        }
        if (getC_status() != null) {
            _hashCode += getC_status().hashCode();
        }
        if (getC_address() != null) {
            _hashCode += getC_address().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TB_INF_EMPCONTACTLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://hr.publisher.mdm", "TB_INF_EMPCONTACTLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_empoid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_empoid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versioncode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "versioncode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versiontime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "versiontime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_hometel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_hometel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versiondelete");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "versiondelete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_postalcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_postalcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versionid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "versionid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_worktel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_worktel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_companymail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_companymail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_privatemail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_privatemail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_txbz");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_txbz"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_operator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_operator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_oid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_oid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_operatetime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_operatetime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_mobile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_mobile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
