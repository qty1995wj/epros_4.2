package epros.draw.gui.workflow.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnFigureRefManhattanLine;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 面板删除流程元素处理
 * 
 * @author ZHANGXH
 * @date： 日期：May 16, 2012 时间：10:32:31 AM
 */
public class JecnRemoveFlowElementUnit {
	/**
	 * 面板删除流程元素
	 * 
	 * @param listPanle
	 */
	public static void removeFlowElement(List<JecnBaseFlowElementPanel> listPanle) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		for (JecnBaseFlowElementPanel flowElementPanel : listPanle) {
			if (flowElementPanel instanceof JecnBaseFigurePanel) {// 删除图形
				removeFigure((JecnBaseFigurePanel) flowElementPanel);
			} else if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {// 删除连接线
				removeManLine((JecnBaseManhattanLinePanel) flowElementPanel);
			} else if (flowElementPanel instanceof JecnBaseDividingLinePanel) {// 删除分割线
				removeDividing((JecnBaseDividingLinePanel) flowElementPanel);
			} else if (flowElementPanel instanceof JecnBaseVHLinePanel) {// 删除横竖分割线
				removeVHLine((JecnBaseVHLinePanel) flowElementPanel);
			}
		}

		// 通过流程元素记录集合计算画图面板的大小
		JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 删除连接线
	 * 
	 * @param lineSet
	 *            连接线数据集合
	 */
	public static void removeManhattanLine(Set<JecnBaseManhattanLinePanel> lineSet) {
		if (lineSet == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		for (JecnBaseManhattanLinePanel manhattanLine : lineSet) {
			// 面板删除图形元素及图形元素对应数据
			removeManLine(manhattanLine);
		}
	}

	/**
	 * 面板删除图形元素及图形元素对应数据
	 * 
	 * @param workflow
	 *            当前面板
	 * @param baseFigurePanel
	 *            待删除的图形元素
	 */
	public static void removeFigure(JecnBaseFigurePanel baseFigurePanel) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || baseFigurePanel == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 面板数组和数据集删除带删除对象及对象数据
		removeFlowElement(baseFigurePanel);

		if (baseFigurePanel instanceof CommentText) {// 如果是注释框删除注释宽的分割线
			CommentText commentText = (CommentText) baseFigurePanel;
			workflow.removeCommentLine(commentText.commentLine);
		}

		// 删除图形编辑点
		JecnPaintFigureUnit.hideConnectPort(baseFigurePanel);
		// 删除图形显示点(图形八个方向点)
		JecnPaintFigureUnit.hideResizeHandles(baseFigurePanel);

		// TODO 如果是注释框

		// 删除图形相关连接线
		removeRefFigureLine(baseFigurePanel);
		// 元素阴影或活动信息化，AR活动编号
		baseFigurePanel.removeRelatedPanel();
		// 面板删除图形
		workflow.remove(baseFigurePanel);
	}

	/**
	 * 面板删除图形元素及图形元素对应数据
	 * 
	 * @param workflow
	 *            当前面板
	 * @param baseFigurePanel
	 *            待删除的图形元素
	 */
	public static void removeReplaceFigure(JecnBaseFigurePanel baseFigurePanel) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || baseFigurePanel == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 面板数组和数据集删除带删除对象及对象数据
		removeFlowElement(baseFigurePanel);

		if (baseFigurePanel instanceof CommentText) {// 如果是注释框删除注释宽的分割线
			CommentText commentText = (CommentText) baseFigurePanel;
			workflow.removeCommentLine(commentText.commentLine);
		}

		// 删除图形编辑点
		JecnPaintFigureUnit.hideConnectPort(baseFigurePanel);
		// 删除图形显示点(图形八个方向点)
		JecnPaintFigureUnit.hideResizeHandles(baseFigurePanel);

		// 元素阴影或活动信息化，AR活动编号
		baseFigurePanel.removeRelatedPanel();
		// 面板删除图形
		workflow.remove(baseFigurePanel);
	}

	/**
	 * 删除面板分割线
	 * 
	 * @param workflow
	 *            面板
	 * @param dividingLine
	 *            分割线
	 */
	public static void removeDividing(JecnBaseDividingLinePanel dividingLine) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || dividingLine == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 删除分割线显示点
		dividingLine.hideLineResizeHandles();
		// 面板分割线集合删除当前分割线
		workflow.removePanelList(dividingLine);
	}

	/**
	 * 面板删除横竖分割线
	 * 
	 * @param workflow
	 *            面板
	 * @param baseVHLinePanel
	 *            横竖分割线
	 */
	public static void removeDividing(JecnDrawDesktopPane workflow, JecnBaseVHLinePanel baseVHLinePanel) {
		if (workflow == null || baseVHLinePanel == null) {
			return;
		}
		// 删除分割线显示点
		// baseVHLinePanel.hideLineResizeHandles(workflow);
		// 所有流程元素集合中删除当前分割线
		workflow.removePanelList(baseVHLinePanel);
	}

	/**
	 * 删除图形相关连接线
	 * 
	 * @param baseFigurePanel
	 */
	public static void removeRefFigureLine(JecnBaseFigurePanel baseFigurePanel) {
		Set<JecnBaseManhattanLinePanel> listBaseLine = new HashSet<JecnBaseManhattanLinePanel>();
		// 获取图形相关的连接线
		JecnDraggedFiguresPaintLine.getManhattanLineList(listBaseLine, baseFigurePanel);
		// 删除连接线
		removeManhattanLine(listBaseLine);
	}

	/**
	 * 删除连接线及连接线相关数据
	 * 
	 * @param workflow
	 * @param manhattanLine
	 */
	public static void removeManLine(JecnBaseManhattanLinePanel manhattanLine) {
		removeLine(manhattanLine);
		removeLineFigure(manhattanLine);
	}

	/**
	 * 删除连线，判断是否删除虚拟元素
	 * 
	 * @param manhattanLine
	 */
	private static void removeLineFigure(JecnBaseManhattanLinePanel manhattanLine) {
		// 如果连接线开始图形和结束图像元素 为（MapLineFigure） 注：此种情况只有在流程架构图中存在，流程架构连接线可
		if (manhattanLine.getStartFigure().getFlowElementData().getMapElemType() == MapElemType.MapLineFigure) {
			removeFlowElement(manhattanLine.getStartFigure());
		}
		if (manhattanLine.getEndFigure().getFlowElementData().getMapElemType() == MapElemType.MapLineFigure) {
			removeFlowElement(manhattanLine.getEndFigure());
		}
	}

	/**
	 * 删除连接线
	 * 
	 * @param manhattanLine
	 */
	public static void removeLine(JecnBaseManhattanLinePanel manhattanLine) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || manhattanLine == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 面板数组和数据集删除带删除对象及对象数据
		removeFlowElement(manhattanLine);

		// 清空线段选中点
		manhattanLine.hideManhattanLineResizeHandles();
		// 面板清空小线段
		manhattanLine.disposeLines();

		if (manhattanLine.getStartFigure() != null) {
			// 删除开始图形对应的集合
			removeListFigureRefManhattanLine(manhattanLine.getStartFigure().getListFigureRefManhattanLine(),
					manhattanLine);
		}
		if (manhattanLine.getEndFigure() != null) {
			// 删除结束图形对应的集合
			removeListFigureRefManhattanLine(manhattanLine.getEndFigure().getListFigureRefManhattanLine(),
					manhattanLine);
		}

		if (manhattanLine.getTextPanel() == null) {
			return;
		}
		// 线段存在编辑文件框，面板删除
		workflow.remove(manhattanLine.getTextPanel());
	}

	/**
	 * 
	 * 面板数组和数据集删除带删除对象及对象数据
	 * 
	 * @param workflow
	 *            面板
	 * @param flowElementPanel
	 *            待删除对象
	 */
	public static void removeFlowElement(JecnBaseFlowElementPanel flowElementPanel) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || flowElementPanel == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 流程元素集合删除连接线
		workflow.removePanelList(flowElementPanel);
	}

	/**
	 * 删除图形开始线或结束线集合中的manhattanLine
	 * 
	 * @param refLineList
	 * @param manhattanLine
	 */
	public static void removeListFigureRefManhattanLine(List<JecnFigureRefManhattanLine> refLineList,
			JecnBaseManhattanLinePanel manhattanLine) {
		if (refLineList == null || manhattanLine == null) {
			return;
		}
		for (JecnFigureRefManhattanLine jecnFigureRefManhattanLine : refLineList) {
			if (jecnFigureRefManhattanLine.getManhattanLinePanel().equals(manhattanLine)) {
				refLineList.remove(jecnFigureRefManhattanLine);
				return;
			}
		}
	}

	/**
	 * 面板删除横竖分割线
	 * 
	 * @param linePanel
	 *            JecnBaseVHLinePanel横竖分割线对象
	 */
	public static void removeVHLine(JecnBaseVHLinePanel linePanel) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || linePanel == null) {
			return;
		}
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 删除分割线显示点
		linePanel.hideParallelLinesResizeHandles();
		// 所有流程元素集合中删除当前分割线
		desktopPane.removePanelList(linePanel);
	}
}
