package com.jecn.epros.server.service.dataImport.sync.xmlImport;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.log4j.Logger;

import com.jecn.epros.server.service.dataImport.sync.xmlImport.dbSource.DataConnect;

public class ImportExcelDataFromRemoteDB {
	private static final Logger log = Logger.getLogger(ImportExcelDataFromRemoteDB.class);
	public static void main(String[] args) {
		importExcelFromDB();
	}

	public static void importExcelFromDB() {
		Connection conn = DataConnect.getConn("./dataImportInfo.xml");
		try {
			String sql = "select d.id '部门编号',d.name '部门名称',dr.parentid '上级部门编号' "
					+ "from flm_department d,flm_dep_dep dr "
					+ "where d.id=dr.id";
			PreparedStatement pstmt = DataConnect.getPStme(conn, sql);
			ResultSet rs = pstmt.executeQuery();

			OutputStream os = new FileOutputStream("sameNamesExcel.xls");
			WritableWorkbook wbook = Workbook.createWorkbook(os); // 建立excel文件
			WritableSheet wsheet = wbook.createSheet("Sheet1", 0); // Sheet1
			ResultSetMetaData meta = rs.getMetaData();
			int columnCount = meta.getColumnCount();
			for (int i = 0; i < columnCount; i++) {// 表字段名称
				wsheet.addCell(new Label(i, 0, meta.getColumnName(i + 1)));
			}
			int rowCount = 1;
			while (rs.next()) {// 结果集
				for (int i = 0; i < columnCount; i++) {
					if (i == 2 && "-1".equals(rs.getString(i + 1))) {
						wsheet.addCell(new Label(i, rowCount, "0"));
					} else {
						wsheet.addCell(new Label(i, rowCount, rs
								.getString(i + 1)));
					}
				}
				rowCount++;
			}

			sql = "select "
					+ "s.cardnum '登录名称',"
					+ "s.name '真实姓名',"
					+ "dps.post_id '任职岗位编号',"
					+ "p.name '任职岗位名称',"
					+ "dps.dep_id '岗位所属部门编号',"
					+ "d.name '岗位所属部门名称',"
					+ "s.email '邮箱地址',"
					+ "s.extnumber '联系电话',"
					+ "s.emailtype '内外网邮件标识(内网inner,外网outer)' "
					+ "from flm_staff s,flm_dep_post_staff dps,flm_post p,flm_department d "
					+ "where dps.staff_id=s.id and dps.post_id=p.id and dps.dep_id=d.id";
			pstmt = DataConnect.getPStme(conn, sql);
			rs = pstmt.executeQuery();

			wsheet = wbook.createSheet("Sheet2", 1); // Sheet2
			meta = rs.getMetaData();
			columnCount = meta.getColumnCount();
			for (int i = 0; i < columnCount; i++) {// 表字段名称
				wsheet.addCell(new Label(i, 0, meta.getColumnName(i + 1)));
			}
			rowCount = 1;
			while (rs.next()) {
				for (int i = 0; i < columnCount; i++) {
					if (i == 2) {
						wsheet.addCell(new Label(i, rowCount, (rs.getString(
								i + 1).trim() + rs.getString(i + 3).trim())));
					} else {
						wsheet.addCell(new Label(i, rowCount, rs
								.getString(i + 1)));
					}
				}
				rowCount++;
			}

			wbook.write();
			wbook.close();
			os.close();

			DataConnect.close(rs, pstmt, conn);
		} catch (Exception e) {
			log.error("", e);
		}
	}
}
