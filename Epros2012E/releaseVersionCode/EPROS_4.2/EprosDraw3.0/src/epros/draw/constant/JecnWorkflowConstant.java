package epros.draw.constant;

/**
 * 
 * 画图面板区域常量类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnWorkflowConstant {
	/** 画图面板网格大小 */
	public static final int CELL_SIZE = 40;
	/** 画板上刻度宽 */
	public static final int SCALE_PANEL_WIDTH = 18;

	public final static float[] FLOAT10 = { 10.0f };
	public final static float[] FLOAT5 = { 5.0f };
	/** 双线时 默认线间距 */
	public final static int DOUBLE_LINE_SPACE = 5;

	/** 当前版本号 */
	public final static String EPROSDRAW_RESION = "3.0";

	/** 密钥后缀 */
	public final static String KEY_FILE_NAME_SUFFIX = "license";
	/** 密钥文件路径 */
	public final static String KEY_FILE_NAME = "/keylist.license";

	/** 图片导出时 512内存 图片面积大小 */
	public final static int imageSize = 85000000;

	/** 相邻横分割线高 */
	public final static int SHORTCUT_SPACE_V = 100;
	/** 相邻竖分割线高 */
	public final static int SHORTCUT_SPACE_H = 150;

	/**
	 * 
	 * Epros软件类型
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum EprosType {
		eprosDraw, // 画图工具
		eprosDesigner, // 设计器
		eprosWeb
		// 浏览端
	}

	/**
	 * 
	 * 画图工具软件类型
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum EasyProcessType {
		easyProcessDrawer, // 画图工具（不带操作说明等）
		easyProcessDesigner, // 画图工具（带操作说明等）
		eprosDesigner
		// 设计器
	}

	/**
	 * 
	 * 上下左右四个编辑点标识
	 * 
	 * 对应老版本：2:上、3：右、4：下、1：左
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum EditPointType {
		top, // 上
		right, // 右
		bottom, // 下
		left
		// 左
	}

	/**
	 * 
	 * 改变图形大小的八个点类型
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum ReSizeFigureType {
		topLeft, // 上左
		topCenter, // 上中
		topRight, // 上右
		leftCenter, // 左中
		rightCenter, // 右中
		bottomLeft, // 下左
		bottomLCenter, // 下中
		bottomLRight
		// 下右
	}

	/**
	 * 
	 * 线的选中点类型
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum ReSizeLineType {
		start, // 开始点
		center, // 中间点
		end
		// 结束点
	}

	/**
	 * 
	 * 渐变方向类型
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum FillColorChangType {
		topTilt, // 1：向上倾斜 默认
		bottom, // 2：向下倾斜
		vertical, // 3：垂直
		horizontal
		// 4：水平
	}

	/**
	 * 
	 * 填充类型
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum FillType {
		none, // 无填充
		one, // 单色填充
		two
		// 双色填充
	}

	/**
	 * 
	 * 线段方向
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum LineDirectionEnum {
		horizontal, // 水平
		vertical, // 垂直
		error
		// 不正确类型
	}

	/**
	 * 
	 * 填充方式
	 * 
	 * @author Administrator
	 * 
	 */
	public enum FillEnum {
		none, // 未为填
		one, // 单色效果
		two
		// 双色效果
	}

	/**
	 * 
	 * 填充图形：普通、3D、阴影
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum GeoEnum {
		general, // 普通
		f3d, // 3D
		shadow, // 阴影
		shadowAnd3D
		// 既有阴影也有3D
	}

	/**
	 * 
	 * 每一次产生撤销恢复动作类型，比如图形添加时记录时为add
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum UnRedoType {
		add, // 添加
		edit, // 修改
		delete, // 删除
		addAndEdit, // 添加且修改
		deleteAndAdd, // 删除切添加
		none
		// 无产生
	}

	/**
	 * 
	 * 文件后缀类型
	 * 
	 * @author ZHOUXY
	 * 
	 */
	public enum FileTypeEnum {
		bmp, doc, docx, gif, jpg, pdf, png, ppt, pptx, txt, vsd, xls, xlsx, epros, elsefile
		// 其他文件类型
	}
}
