package com.jecn.epros.domain.process;

public class ProcessSystemInventory {
    /**
     * 流程ID
     */
    private Long flowId;
    /**
     * 流程父节点ID
     */
    private Long parentFlowId;
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程编号
     */
    private String flowIdInput;// 流程编号
    /**
     * 发布时间
     */
    private String pubDate;
    /**
     * 级别
     */
    private Integer level;
    /**
     * 版本号
     */
    private String versionId;
    /**
     * 处于什么阶段 0是待建，1是审批，2是发布
     */
    private Integer typeByData;
    /**
     * 0是流程地图，1是流程
     */
    private Integer isflow;

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Long getParentFlowId() {
        return parentFlowId;
    }

    public void setParentFlowId(Long parentFlowId) {
        this.parentFlowId = parentFlowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getFlowIdInput() {
        return flowIdInput;
    }

    public void setFlowIdInput(String flowIdInput) {
        this.flowIdInput = flowIdInput;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public Integer getTypeByData() {
        return typeByData;
    }

    public void setTypeByData(Integer typeByData) {
        this.typeByData = typeByData;
    }

    public Integer getIsflow() {
        return isflow;
    }

    public void setIsflow(Integer isflow) {
        this.isflow = isflow;
    }
}
