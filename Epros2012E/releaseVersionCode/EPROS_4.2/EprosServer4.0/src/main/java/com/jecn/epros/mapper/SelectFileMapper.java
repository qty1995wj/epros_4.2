package com.jecn.epros.mapper;

import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.selectFile.SelectTableBean;
import com.jecn.epros.sqlprovider.ProcessSqlProvider;
import com.jecn.epros.sqlprovider.SelectFileSqlProvider;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

public interface SelectFileMapper {
    /**
     * 流程架构 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findFlowMapTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findFlowMapTree(Map<String, Object> map);

    /**
     * 流程架构 搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "searchFlowMap")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> searchFlowMap(Map<String, Object> map);

    /**
     * 流程 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findFlowTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findFlowTree(Map<String, Object> map);

    /**
     * 流程 搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "searchFlow")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> searchFlow(Map<String, Object> map);

    /**
     * 制度 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findRuleTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findRuleTree(Map<String, Object> map);

    /**
     * 制度 搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "searchRule")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> searchRule(Map<String, Object> map);

    /**
     * 标准 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findStandardTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findStandardTree(Map<String, Object> map);

    /**
     * 标准 搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "searchStandard")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> searchStandard(Map<String, Object> map);


    /**
     * 风险 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findRiskTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findRiskTree(Map<String, Object> map);

    /**
     * 风险 搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "searchRisk")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> searchRisk(Map<String, Object> map);

    /**
     * 文件 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findFileTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findFileTree(Map<String, Object> map);


    /**
     * 风险 搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "searchFile")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> searchFile(Map<String, Object> map);

    /**
     * 组织 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findOrgTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findOrgTree(Map<String, Object> map);

    /**
     * 组织 搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "searchOrg")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> searchOrg(Map<String, Object> map);

    /**
     * 岗位 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findPosTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findPosTree(Map<String, Object> map);

    /**
     * 岗位 搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "searchPos")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> searchPos(Map<String, Object> map);

    /**
     * 人员选择 点击组织 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findPersonOrgTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findPersonOrgTree(Map<String, Object> map);

    /**
     * 人员选择 点击岗位树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findPersonPosTree")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> findPersonPosTree(Map<String, Object> map);

    /**
     * 人员 搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "searchPeople")
    @ResultMap("mapper.SelectFile.selectFileTree")
    public List<TreeNode> searchPeople(Map<String, Object> map);


    /**
     * 流程
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findFlowTreeNode")
    @ResultMap("mapper.SelectFile.selectFileTree")
    TreeNode findFlowTreeNode(Map<String, Object> map);

    /**
     * 风险
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findRiskTreeNode")
    @ResultMap("mapper.SelectFile.selectFileTree")
    TreeNode findRiskTreeNode(Map<String, Object> map);

    /**
     * 标准
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findStandardTreeNode")
    @ResultMap("mapper.SelectFile.selectFileTree")
    TreeNode findStandardTreeNode(Map<String, Object> map);

    /**
     * 文件
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findFileTreeNode")
    @ResultMap("mapper.SelectFile.selectFileTree")
    TreeNode findFileTreeNode(Map<String, Object> map);

    /**
     * 组织
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findOrgTreeNode")
    @ResultMap("mapper.SelectFile.selectFileTree")
    TreeNode findOrgTreeNode(Map<String, Object> map);

    /**
     * 岗位
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findPosTreeNode")
    @ResultMap("mapper.SelectFile.selectFileTree")
    TreeNode findPosTreeNode(Map<String, Object> map);

    /**
     * 人员选择
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findPersonOrgTreeNode")
    @ResultMap("mapper.SelectFile.selectFileTree")
    TreeNode findPersonOrgTreeNode(Map<String, Object> map);


    /**
     * 制度
     *
     * @param map
     * @return
     */
    @SelectProvider(type = SelectFileSqlProvider.class, method = "findRuleTreeNode")
    @ResultMap("mapper.SelectFile.selectFileTree")
    TreeNode findRuleTreeNode(Map<String, Object> map);

    @SelectProvider(type = SelectFileSqlProvider.class, method = "getNodePath")
    String getNodePath(Map<String, Object> map);

    @Select("select ORG_ID from JECN_FLOW_ORG_IMAGE where FIGURE_ID=#{id}")
    Long getPosOrgId(Long id);

    @SelectProvider(type = SelectFileSqlProvider.class, method = "processFileAuthCountSql")
    int processFileAuthCount(Map<String, Object> map);

    @Select("select FILE_ID from FILE_CONTENT where ID=#{id}")
    public Long getFileIdByContentId(Map<String, Object> paramMap);

    @SelectProvider(type = SelectFileSqlProvider.class, method = "getFileIdByRuleIdSql")
    public Long getFileIdByRuleId(Map<String, Object> paramMap);

    @SelectProvider(type = SelectFileSqlProvider.class, method = "findPersonNode")
    @ResultMap("mapper.SelectFile.selectFileTree")
    TreeNode findPersonNode(Map<String, Object> map);
}
