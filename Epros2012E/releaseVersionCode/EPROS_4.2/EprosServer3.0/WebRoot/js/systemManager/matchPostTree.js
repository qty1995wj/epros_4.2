/**
 * type:查询类型 inputDomId:显示的名称 hiddenDomId：隐藏的ID
 * 
 * 
 */
function matchPosPerMultipleSelect(type, inputDomId, hiddenDomId) {
	if (!type || type == "") {
		return;
	}
	var selectOrgPosTitle;
	var dataUrl;
	var gridUrl;
	if (type == "position") {// HR岗位选择
		selectOrgPosTitle = i_posSelect;
		dataUrl = "childsMatchPos.action";
		gridUrl = "matchPosSearch.action";
	} else if (type == "basePosition") {// 基准岗位选择
		selectOrgPosTitle = i_posSelect;
		dataUrl = "childsMatchBasePos.action";
		gridUrl = "matchBaseSearch.action";
	}

	var orgPosPerMultipleRoot = new Ext.tree.AsyncTreeNode({
				text : selectOrgPosTitle,
				expanded : true,
				id : "0"
			})

	var orgPosPerMultipleLoader = new Ext.tree.TreeLoader({
				dataUrl : dataUrl
			})

	var orgPosPerMultipleTree = new Ext.tree.TreePanel({
				root : orgPosPerMultipleRoot,
				loader : orgPosPerMultipleLoader,
				height : 415,
				width : 180,
				region : 'west',
				border : false,
				autoScroll : true,
				animate : true,// 开启动画效果
				enableDD : false,// 不允许子节点拖动
				listeners : {
					'beforeload' : function(node) {
						/**
						 * if (type == "person") { if (node.attributes.type ==
						 * "position") { orgPosPerMultipleLoader.dataUrl =
						 * "getChildPerson.action"; } else {
						 * orgPosPerMultipleLoader.dataUrl =
						 * "childsMatchPos.action"; } }
						 */

					},
					'dblclick' : function(node, e) {
						if (node.attributes.type == type) {
							var flag = false;
							resultStore.each(function(r) {
										if (node.id.split("_")[0] == r
												.get("id")) {
											resultGridPanel.getSelectionModel()
													.selectRecords([r]);
											flag = true;
											return false;
										}
									});
							if (!flag) {
								var sr = new Ext.data.Record();
								sr.set("id", node.id.split("_")[0]);
								sr.set("name", node.attributes.text);
								resultStore.add(sr);
							}

						}
					}
				}
			})
	/**
	 * 搜索
	 * 
	 */
	var searchStore = new Ext.data.JsonStore({
				autoLoad : false,
				proxy : new Ext.data.HttpProxy({
							url : gridUrl
						}),
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pName",
							mapping : "pName"
						}]
			});
	var searchModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	var searchGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				region : 'center',
				height : 240,
				border : false,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				tbar : [i_nameC, new Ext.form.TextField({
							id : 'searchNameId',
							enableKeyEvents : true,
							width : 350,
							listeners : {
								'keyup' : function(o) {
									if ($.trim(o.getValue()) != "") {
										searchStore.setBaseParam("searchName",
												o.getValue());
										searchStore.load();

									}

								}
							}
						})],
				store : searchStore,
				colModel : searchModel,
				listeners : {
					'rowdblclick' : function(grid, i, e) {
						var sr = grid.getStore().getAt(i);
						var flag = false;
						resultStore.each(function(r) {
									if (sr.get("id") == r.get("id")) {
										resultGridPanel.getSelectionModel()
												.selectRecords([r]);
										flag = true;
										return false;
									}
								});
						if (!flag) {
							resultStore.add(sr);
						}

					}
				}
			});

	/** ****************************************************************** */
	var resultStore = new Ext.data.JsonStore({
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pName",
							mapping : "pName"
						}]
			});
	var resultModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	var resultGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				height : 220,
				border : false,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				store : resultStore,
				colModel : resultModel
			});

	var selectMainPanel = new Ext.Panel({
				height : 440,
				width : 640,
				layout : 'border',
				border : false,
				frame : false,
				items : [orgPosPerMultipleTree, {
							region : 'center',
							items : [searchGridPanel, resultGridPanel]
						}]

			})
	var multipleSelectWin = new Ext.Window({
				title : selectOrgPosTitle,
				resizable : false, // 不允许改变大小
				height : 500,
				width : 650,
				minHeight : 500,
				minWidth : 650,
				modal : true,
				closeAction : "close",
				constrainHeader : true,// 保证窗口顶部不超出浏览器
				items : selectMainPanel,
				buttons : [{
					text : i_Delete,// 删除
					handler : function() {
						Ext.each(resultGridPanel.getSelectionModel()
										.getSelections(), function(r) {
									resultStore.remove(r);
								});
					},
					scope : this
				}, {
					text : i_empty,// 清空
					handler : function() {
						// '是否清空所有数据！'
						Ext.Msg.confirm(i_tip, i_isClearAllData, function(fc) {
									if (fc == "yes") {
										resultStore.removeAll();
									}
								});
					},
					scope : this
				}, {
					text : i_ok,// 确定
					handler : function() {
						var s = "";
						var ids = "";
						resultStore.each(function(r) {
									s = s + r.get("name") + "\n";
									ids = ids + r.get("id") + ",";
								});
						document.getElementById(inputDomId).value = s
								.substring(0, s.length - 1);
						document.getElementById(hiddenDomId).value = ids
								.substring(0, ids.length - 1);
						multipleSelectWin.close();// 窗口关闭
					},
					scope : this
				}, {
					text : i_close,// 关闭
					handler : function() {
						multipleSelectWin.close();// 窗口关闭
					}
				}]
			});
	if (document.getElementById(inputDomId).value != '') {
		var names = document.getElementById(inputDomId).value.split('\n');
		var ids = document.getElementById(hiddenDomId).value.split(',');
		for (i = 0; i < names.length; i++) {
			var sr = new Ext.data.Record();
			sr.set("id", ids[i]);
			sr.set("name", names[i]);
			resultStore.add(sr);
		}
	}

	multipleSelectWin.show();
}
/**
 * 岗位选择单选
 * 
 */
function orgPosPerOneSelect(type, inputDomId, hiddenDomId) {
	if (!type || type == "") {
		return;
	}
	var selectOrgPosTitle;
	var dataUrl;
	var gridUrl;
	if (type == "organization") {
		selectOrgPosTitle = i_orgSelect;
		dataUrl = "getChildOrg.action";
		gridUrl = "orgSearch.action";
	} else if (type == "position") {
		selectOrgPosTitle = i_posSelect;
		dataUrl = "getChildsOrgAndPos.action";
		gridUrl = "posSearch.action";
	} else if ("person") {
		selectOrgPosTitle = i_personSelect;
		dataUrl = "getChildsOrgAndPos.action?posLoadtype=1";
		gridUrl = "peopleSearch.action";
	}

	var orgPosPerMultipleRoot = new Ext.tree.AsyncTreeNode({
				text : selectOrgPosTitle,
				expanded : true,
				id : "0"
			})

	var orgPosPerMultipleLoader = new Ext.tree.TreeLoader({
				dataUrl : dataUrl
			})

	var orgPosPerMultipleTree = new Ext.tree.TreePanel({
		root : orgPosPerMultipleRoot,
		loader : orgPosPerMultipleLoader,
		height : 555,
		width : 180,
		border : false,
		region : 'west',
		autoScroll : true,
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
			'beforeload' : function(node) {
				if (type == "person") {
					if (node.attributes.type == "position") {
						orgPosPerMultipleLoader.dataUrl = "getChildPerson.action";
					} else {
						orgPosPerMultipleLoader.dataUrl = "getChildsOrgAndPos.action?posLoadtype=1";
					}
				}

			},
			'dblclick' : function(node, e) {
				if (node.attributes.type == type) {
					var flag = false;
					resultStore.each(function(r) {
								if (node.id.split("_")[0] == r.get("id")) {
									resultGridPanel.getSelectionModel()
											.selectRecords([r]);
									flag = true;
									return false;
								}
							});
					if (!flag) {
						resultStore.removeAll();
						var sr = new Ext.data.Record();
						sr.set("id", node.id.split("_")[0]);
						sr.set("name", node.attributes.text);
						resultStore.add(sr);
					}

				}
			}
		}
	})

	var searchStore = new Ext.data.JsonStore({
				autoLoad : false,
				proxy : new Ext.data.HttpProxy({
							url : gridUrl
						}),
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						}]
			});
	var searchModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	var searchGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				region : 'center',
				height : 240,
				border : false,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				tbar : [i_nameC, new Ext.form.TextField({
							id : 'searchNameId',
							enableKeyEvents : true,
							width : 350,
							listeners : {
								'keyup' : function(o) {
									if ($.trim(o.getValue()) != "") {
										searchStore.setBaseParam("searchName",
												o.getValue());
										searchStore.load();

									}

								}
							}
						})],
				store : searchStore,
				colModel : searchModel,
				listeners : {
					'rowdblclick' : function(grid, i, e) {
						var sr = grid.getStore().getAt(i);
						var flag = false;
						resultStore.each(function(r) {
									if (sr.get("id") == r.get("id")) {
										resultGridPanel.getSelectionModel()
												.selectRecords([r]);
										flag = true;
										return false;
									}
								});
						if (!flag) {
							resultStore.removeAll();
							resultStore.add(sr);
						}
					}
				}
			});

	/** ****************************************************************** */
	var resultStore = new Ext.data.JsonStore({
				fields : [{
							name : "id",
							mapping : "id"
						}, {
							name : "name",
							mapping : "name"
						}, {
							name : "pname",
							mapping : "pname"
						}]
			});
	var resultModel = new Ext.grid.ColumnModel({
				defaults : {
					align : 'center',
					sortable : true,
					menuDisabled : false
				},
				columns : [{
							header : i_name,
							dataIndex : "name"
						}]
			});
	var resultGridPanel = new Ext.grid.GridPanel({
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				height : 220,
				border : false,
				viewConfig : {
					forceFit : true
					// 让列的宽度和grid一样
				},
				store : resultStore,
				colModel : resultModel
			});

	var selectMainPanel = new Ext.Panel({
				height : 440,
				width : 640,
				layout : 'border',
				border : false,
				frame : false,
				items : [orgPosPerMultipleTree, {
							region : 'center',
							items : [searchGridPanel, resultGridPanel]
						}]

			})
	var multipleSelectWin = new Ext.Window({
				title : selectOrgPosTitle,
				resizable : false, // 不允许改变大小
				height : 500,
				width : 650,
				minHeight : 500,
				minWidth : 650,
				constrainHeader : true,// 保证窗口顶部不超出浏览器
				modal : true,
				closeAction : "close",
				items : selectMainPanel,
				buttons : [{
					text : i_Delete,// 删除
					handler : function() {
						Ext.each(resultGridPanel.getSelectionModel()
										.getSelections(), function(r) {
									resultStore.remove(r);
								});
					},
					scope : this
				}, {
					text : i_empty,// 清空
					handler : function() {
						// '是否清空所有数据！'
						Ext.Msg.confirm(i_tip, i_isClearAllData, function(fc) {
									if (fc == "yes") {
										resultStore.removeAll();
									}
								});
					},
					scope : this
				}, {
					text : i_ok,// 确定
					handler : function() {
						var s = "";
						var ids = "";
						resultStore.each(function(r) {
									s = s + r.get("name") + "\n";
									ids = ids + r.get("id") + ",";
								});
						document.getElementById(inputDomId).value = s
								.substring(0, s.length - 1);
						document.getElementById(hiddenDomId).value = ids
								.substring(0, ids.length - 1);
						multipleSelectWin.close();// 窗口关闭
					},
					scope : this
				}, {
					text : i_close,// 关闭
					handler : function() {
						multipleSelectWin.close();// 窗口关闭
					}
				}]
			});
	if (document.getElementById(inputDomId).value != '') {
		var names = document.getElementById(inputDomId).value.split('\n');
		var ids = document.getElementById(hiddenDomId).value.split(',');
		for (i = 0; i < names.length; i++) {
			var sr = new Ext.data.Record();
			sr.set("id", ids[i]);
			sr.set("name", names[i]);
			resultStore.add(sr);
		}
	}

	multipleSelectWin.show();
}