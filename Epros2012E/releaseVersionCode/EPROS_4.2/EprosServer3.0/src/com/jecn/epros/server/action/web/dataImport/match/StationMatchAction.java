package com.jecn.epros.server.action.web.dataImport.match;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import com.jecn.epros.server.action.web.dataImport.sync.excelorDB.UserSyncAction;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.match.IPostionMatchService;
import com.jecn.epros.server.service.dataImport.match.IUserMatchService;
import com.jecn.epros.server.webBean.dataImport.match.ActualPosBean;
import com.jecn.epros.server.webBean.dataImport.match.PosEprosRelBean;

/**
 * 三福岗位匹配
 * 
 * @author xiaohu
 * 
 */
public class StationMatchAction extends BaseAction {
	private final Log log = LogFactory.getLog(UserSyncAction.class);
	/** 记录分页处理后显示的数据 */
	private List<PosEprosRelBean> listAll;
	/** 岗位匹配数据处理 */
	private IPostionMatchService postionMatchService;
	/** 人员数据导入处理类 */
	private IUserMatchService matchService = null;
	/** 未匹配岗位的岗位集合 */
	private List<ActualPosBean> actuaList = null;
	/** 0：岗位匹配；1：未匹配岗位 */
	private String radioType = "0";
	/** 返回前台值 */
	private String stringResult = null;
	/** 搜索岗位名称 */
	/** 开始行数 */
	private int start;
	/** 每页显示条数 */
	private int limit;
	/** 搜索条件中的 岗位名称 */
	private String posName;
	/** changeId 0：流程岗位不变，1 流程岗位更改 */
	private int changeId;

	public int getChangeId() {
		return changeId;
	}

	public void setChangeId(int changeId) {
		this.changeId = changeId;
	}

	/**
	 * 获取当前页岗位匹配集合
	 * 
	 * @return
	 * @throws Exception
	 */
	public String matchPostAction() {
		try {
			log.info("获取岗位匹配数据开始！");
			// 获取岗位匹配总数
			int total = matchService.getSearchPosCount(posName);
			// 岗位匹配相关信息
			listAll = matchService.getStatcionDataBySearch(start, limit,
					posName);
			log.info("根据分页数据获取当前页信息结束！");
			if (listAll == null) {
				listAll = new ArrayList<PosEprosRelBean>();
			}
			// 获取json字符串
			JSONArray jsonArray = JSONArray.fromObject(listAll);
			outJsonPage(jsonArray.toString(), total);
			log.info("获取岗位匹配数据结束！");
		} catch (Exception ex) {
			log.error("获取岗位匹配结果异常！", ex);
		}
		return "success";
	}

	/** 根据匹配岗位的编码、ID查找对应的岗位名称 */
	public List<Object[]> getPosNameList() {
		// 流程岗位名称、 实际岗位名称数据集合
		List<Object[]> objectList;
		try {
			objectList = matchService.selectActEpsNames();
		} catch (Exception e) {
			log.error(e);
			return null;
		}
		return objectList;
	}

	/**
	 * 添加新的岗位匹配
	 * 
	 * @return
	 * @throws Exception
	 */
	public String addPosEpsRel() {
		HttpServletRequest request = this.getRequest();
		// 流程岗位ID
		String flowPosId = request.getParameter("epsPosId");
		// 匹配岗位类型 0:实际岗位类型，1：基准岗位类型
		String relType = request.getParameter("relType");
		// 实际岗位编码
		String posNums = request.getParameter("positionIds");
		// isFlag 标识， 0：新增 1：编辑
		String isFlag = request.getParameter("isFlag");
		if (JecnCommon.isNullOrEmtryTrim(relType)
				|| JecnCommon.isNullOrEmtryTrim(flowPosId)
				|| JecnCommon.isNullOrEmtryTrim(posNums)
				|| JecnCommon.isNullOrEmtryTrim(isFlag)) {// 数据异常
			outJsonString(getJsonStr(0));
			return null;
		}
		// 新增或编辑成功 返回true
		boolean isok = false;
		int type = 0;
		if (relType != null && "0".equals(relType)) {// 选择的HR实际岗位
			type = 0;
		} else if (relType != null && "1".equals(relType)) {// 选择的基准岗位
			type = 1;
		}
		try {

			if (changeId == 1 || isFlag.equals("0")) {// 编辑时流程岗位变更或 添加是执行验证
				// 获取信息异常;1：存在匹配关系(不可新增);2：不存在匹配关系;3:添加成功
				String json = onlyMatchPost(flowPosId);
				if (json.equals("1")) {// 1：存在匹配关系(不可新增)
					outJsonString(getJsonStr(1));
					return null;
				}
			}
			if (isFlag.equals("0")) { // 新增
				// 验证该流程节点是否存在匹配关系 String 0 :
				// // 添加数据到流程岗位与实际岗位关联表
				isok = matchService.addPosEpsRel(flowPosId, posNums, type);
			} else if (isFlag.equals("1")) { // 编辑

				isok = matchService.updatePosEpsRel(flowPosId, posNums, type);
			}
		} catch (Exception e) {
			log.error(e);
			outJsonString(getJsonStr(0));
			return null;
		}
		if (isok) {
			outJsonString(getJsonStr(3));
			return "success";
		}
		return null;
	}

	/**
	 * 拼装JSON字符串
	 * 
	 * @param type
	 *            String 0 : 获取信息异常;1：存在匹配关系(不可新增);2：不存在匹配关系;3:添加成功
	 * @return String
	 */
	private String getJsonStr(int type) {
		return "{success:true,msg:" + type + "}";
	}

	/**
	 * 验证该流程节点是否存在匹配关系 String 1：存在匹配关系;2：不存在匹配关系0 : 获取信息异常
	 * 
	 * @param flowPosId
	 *            流程岗位ID
	 * @return
	 */
	private String onlyMatchPost(String flowPosId) {
		String josn = "";
		if (JecnCommon.isNullOrEmtryTrim(flowPosId)) {
			josn = "0";
			return null;
		}
		int count = matchService.onlyMatchPost(Long.valueOf(flowPosId.trim()));
		if (count > 0) {// 匹配关系已存在改岗位的匹配关系
			josn = "1";
		} else {
			josn = "2";
		}
		return josn;
	}

	/***************************************************************************
	 * 删除流程岗位与实际岗位关联表 删除 流程岗位与人员关联表
	 * 
	 * @return
	 * @throws NumberFormatException
	 * @throws DaoException
	 */
	public String deletePosEpsRel() {
		HttpServletRequest request = ServletActionContext.getRequest();
		// String mainId = request.getParameter("mainId");
		String flowPosId = request.getParameter("epsPosId");
		try {
			matchService.deletePosEpsData(Long.valueOf(flowPosId));
		} catch (Exception e) {
			log.error(" 删除流程岗位与实际岗位关联表 删除 流程岗位与人员关联表！", e);
			return "error";
		}
		return null;
	}

	/**
	 * 获取实际岗位未匹配结果集
	 * 
	 * @return
	 */
	public String showNoMatchTruePostResult() {
		log.info("获取未匹配岗位的结果集开始！");
		try {
			// 1:HR实际岗位；5：流程岗位；6基准岗位
			int type = 1;
			// 获取岗位匹配总数
			int total = postionMatchService
					.getNoMatchObjectCount(type, posName); // userSyncImportSf.getStatcionRowCount();
			// 获取未匹配岗位
			actuaList = postionMatchService.getActualPosBeanList(type, start,
					limit, posName);
			// 获取json字符串
			if (actuaList != null) {
				JSONArray jsonArray = JSONArray.fromObject(actuaList);
				outJsonPage(jsonArray.toString(), total);
			}
			log.info("获取未匹配岗位数据结束！");
		} catch (Exception ex) {
			log.error("获取未匹配岗位出现异常！", ex);
			return "error";
		}
		log.info("获取未匹配岗位的结果集結束！");
		return "success";
	}

	/**
	 * 获取获取未匹配的流程岗位结果集
	 * 
	 * @return
	 */
	public String showNoMatchProcessResult() {
		log.info("获取未匹配岗位的结果集开始！");
		try {
			// 1:HR实际岗位；5：流程岗位；6基准岗位
			int type = 5;
			// 获取岗位匹配总数
			int total = postionMatchService
					.getNoMatchObjectCount(type, posName); // userSyncImportSf.getStatcionRowCount();
			// 获取未匹配岗位
			actuaList = postionMatchService.getActualPosBeanList(type, start,
					limit, posName);
			// 获取json字符串
			if (actuaList != null) {
				JSONArray jsonArray = JSONArray.fromObject(actuaList);
				outJsonPage(jsonArray.toString(), total);
			}
			log.info("获取未匹配岗位数据结束！");
		} catch (Exception ex) {
			log.error("获取未匹配岗位出现异常！", ex);
			return "error";
		}
		log.info("获取未匹配岗位的结果集結束！");
		return "success";
	}

	/**
	 * 获取获取未匹配的基准岗位岗位结果集
	 * 
	 * @return
	 */
	public String showNoMatchBasePostResult() {
		log.info("获取未匹配岗位的结果集开始！");
		try {
			// 1:HR实际岗位；5：流程岗位；6基准岗位
			int type = 6;
			// 获取未匹配岗位
			// 获取岗位匹配总数
			int total = postionMatchService
					.getNoMatchObjectCount(type, posName); // userSyncImportSf.getStatcionRowCount();
			// 获取未匹配岗位
			actuaList = postionMatchService.getActualPosBeanList(type, start,
					limit, posName);
			// 获取json字符串
			if (actuaList != null) {
				JSONArray jsonArray = JSONArray.fromObject(actuaList);
				outJsonPage(jsonArray.toString(), total);
			}
			log.info("获取未匹配岗位数据结束！");
		} catch (Exception ex) {
			log.error("获取未匹配岗位出现异常！", ex);
			return "error";
		}
		log.info("获取未匹配岗位的结果集結束！");
		return "success";
	}

	public List<PosEprosRelBean> getListAll() {
		return listAll;
	}

	public void setListAll(List<PosEprosRelBean> listAll) {
		this.listAll = listAll;
	}

	public String getStringResult() {
		return stringResult;
	}

	public void setStringResult(String stringResult) {
		this.stringResult = stringResult;
	}

	public List<ActualPosBean> getActuaList() {
		return actuaList;
	}

	public void setActuaList(List<ActualPosBean> actuaList) {
		this.actuaList = actuaList;
	}

	public String getRadioType() {
		return radioType;
	}

	public void setRadioType(String radioType) {
		this.radioType = radioType;
	}

	public IUserMatchService getMatchService() {
		return matchService;
	}

	public void setMatchService(IUserMatchService matchService) {
		this.matchService = matchService;
	}

	public IPostionMatchService getPostionMatchService() {
		return postionMatchService;
	}

	public void setPostionMatchService(IPostionMatchService postionMatchService) {
		this.postionMatchService = postionMatchService;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

}
