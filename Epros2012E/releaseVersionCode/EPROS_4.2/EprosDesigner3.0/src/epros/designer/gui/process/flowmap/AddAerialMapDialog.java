package epros.designer.gui.process.flowmap;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.JecnProcessCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.tree.JecnTreeRenderer;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/*******************************************************************************
 * 
 * 新建鸟瞰图
 * 
 * @author 2018-05-15
 * 
 */
public class AddAerialMapDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(AddAerialMapDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();// 510, 660

	/** center面板 */
	private JecnPanel centerPanel = new JecnPanel();// /510, 505

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();// 500, 30

	/** 流程地图名称Lab */
	private JLabel flowMapLab = new JLabel(JecnProperties.getValue("flowMapNameC"));
	/** 流程地图名称Field */
	JTextField flowMapField = new JTextField();// 170, 20
	/** 流程地图名称必填*号 */
	private JLabel flowMapNameRequired = new JLabel(JecnProperties.getValue("required"));

	/** 流程地图编号Lab */
	private JLabel mapNumLab = new JLabel(JecnProperties.getValue("flowMapNumC"));
	/** 流程地图编号Field */
	JTextField mapNumField = new JTextField();// 170, 20

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 验证提示 */
	JLabel verfyLab = new JLabel();
	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	private JecnTreeNode selectNode = null;
	private JecnTree jTree = null;
	/** 面板宽 */
	private int dialogWidth = getWidthMin();
	/** 面板高 */
	private int dialogHeigh = 150;

	public AddAerialMapDialog(JecnTreeNode pNode, JecnTree jTree) {
		this.setTitle(JecnProperties.getValue("newAerialView"));
		this.selectNode = pNode;
		this.jTree = jTree;
		this.setTitle(JecnProperties.getValue("newFlowMap"));
		this.setSize(dialogWidth, dialogHeigh);
		this.setResizable(true);
		this.setModal(true);
		this.setLocationRelativeTo(null);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		flowMapNameRequired.setForeground(Color.red);
		verfyLab.setForeground(Color.red);

		initLayout();

		acitonInit();
	}

	private void acitonInit() {
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		this.dispose();
	}

	public void okButPerformed() {
		// 流程地图名称
		String flowName = flowMapField.getText().toString().trim();
		// 判断流程名称填写是否正确
		if (DrawCommon.isNullOrEmtryTrim(flowName)) {
			// 名称不能为空
			verfyLab.setText(JecnProperties.getValue("name") + JecnProperties.getValue("isNotEmpty"));
			return;
		} else if (!JecnUserCheckUtil.checkNameFileSpecialChar(flowName)) {
			verfyLab.setText(JecnProperties.getValue("name") + JecnProperties.getValue("notInputThis"));
			return;
		} else if (JecnUserCheckUtil.getTextLength(flowName) > 122) {
			// 不能超过122个字符或61个汉字
			verfyLab.setText(JecnProperties.getValue("name") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 流程编号
		if (DrawCommon.checkNameMaxLength(this.mapNumField.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("flowNum") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		try {
			if (ConnectionPool.getProcessAction().validateAddName(flowName, this.selectNode.getJecnTreeBean().getId(),
					0, JecnConstants.projectId)) {
				verfyLab.setText(JecnProperties.getValue("nameHaved"));
				return;
			}
		} catch (Exception e1) {
			log.error("AddAerialMapDialog okButPerformed is error", e1);
		}

		// 流程地图基本信息表
		JecnMainFlowT flowMapBean = new JecnMainFlowT();
		try {

			JecnFlowStructureT jecnFlowStructureT = JecnProcessCommon.createAerialProcessMap(flowName, 0L,
					this.selectNode, mapNumField.getText(), flowMapBean, null, null, this.jTree, null);
			// 添加到画图面板
			JecnFlowMapData totalMapData = JecnDesignerCommon.createFlowMapData(MapType.totalMap, jecnFlowStructureT
					.getFlowId());
			totalMapData.setName(flowName);
			totalMapData.setFlowId(jecnFlowStructureT.getFlowId());
			JecnDrawMainPanel.getMainPanel().addWolkflow(totalMapData);
			JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			drawDesktopPane.getTabbedTitlePanel().setTabIcon(
					(Icon) JecnTreeRenderer.class.getField(TreeNodeType.processMap.toString() + "NoPub").get(null));
			this.dispose();
		} catch (Exception e) {
			log.error("AddAerialMapDialog okButPerformed is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}
		this.setVisible(false);
	}

	protected void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(3, 3, 3, 3);
		// centerPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(3, 3, 0, 3), 0, 0);
		mainPanel.add(centerPanel, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				new Insets(0, 3, 0, 3), 0, 0);
		mainPanel.add(buttonPanel, c);

		// 添加中间面板
		centerPanel.setLayout(new GridBagLayout());
		int row = 0;

		// 名称
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		centerPanel.add(flowMapLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(flowMapField, c);
		c = new GridBagConstraints(3, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		centerPanel.add(flowMapNameRequired, c);
		row++;
		// 编号
		c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		centerPanel.add(mapNumLab, c);
		c = new GridBagConstraints(1, row, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(mapNumField, c);
		row++;

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);
	}

	public boolean isUpdate() {
		// 流程地图名称
		if (flowMapField.getText() != null && !"".equals(flowMapField.getText())) {
			return true;
		}
		// 编号
		if (mapNumField.getText() != null && !"".equals(mapNumField.getText())) {
			return true;
		}
		return false;
	}
}
