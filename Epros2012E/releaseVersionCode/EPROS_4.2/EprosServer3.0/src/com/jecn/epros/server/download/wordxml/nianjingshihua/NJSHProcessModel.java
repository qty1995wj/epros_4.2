package com.jecn.epros.server.download.wordxml.nianjingshihua;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;

public class NJSHProcessModel extends ProcessFileModel {
	/**
	 * 南京石化操作说明模版
	 * 
	 * @param processDownloadBean
	 * @param path
	 * @param flowChartDirection
	 */
	public NJSHProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, false);
		setDocStyle("、", textTitleStyle());
		/***** 段落空格换行属性bean 宋体 十三号 最小值16磅值 *******/
		newLineBean = new ParagraphBean(Align.left, new FontBean("宋体",
				Constants.shisanhao, false));
		newLineBean.setSpace(0f, 0f, vLine_atLeast16);
		getDocProperty().setNewTblWidth(17.49F);
	}

	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F,
			LineRuleType.AUTO);
	/**** 段落行距最小值16磅值 ********/
	private ParagraphLineRule vLine_atLeast16 = new ParagraphLineRule(16F,
			LineRuleType.AT_LEAST);

	/***** 段落空格换行属性bean *******/
	private ParagraphBean newLineBean;

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 重写创建文本项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	@Override
	protected void createTextItem(ProcessFileItem processFileItem,
			String... content) {
		super.createTextItem(processFileItem, content);
		processFileItem.getBelongTo().createParagraph("", newLineBean);

	}

	/*****
	 * 创建表格项
	 * 
	 * @param contentSect
	 * @param fs
	 * @param list
	 * @return
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem,
			float[] fs, List<String[]> list) {
		processFileItem.getBelongTo().createParagraph("", newLineBean);
		Table tab = super.createTableItem(processFileItem, fs, list);
		processFileItem.getBelongTo().createParagraph("", newLineBean);
		return tab;
	}

	/**
	 * 添加封面节点封皮内容
	 */
	private void addTitleSectContent() {
		// 公司名称
		String companyName = processDownloadBean.getCompanyName();
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		// 文件编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		// 版本
		String flowVersion = processDownloadBean.getFlowVersion();
		ParagraphBean pBean = null;
		// 宋体小三 加粗 居中 单倍行距
		pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaosan, false);
		pBean.setSpace(0f, 0f, vLine1);
		for (int i = 0; i < 4; i++) {
			getDocProperty().getTitleSect().createParagraph("");
		}
		getDocProperty().getTitleSect().createParagraph(companyName, pBean);
		for (int i = 0; i < 6; i++) {
			getDocProperty().getTitleSect().createParagraph("");
		}
		// 宋体 小一 加粗 居中 单倍行距
		pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoer, true);
		pBean.setSpace(0f, 0f, vLine1);
		getDocProperty().getTitleSect().createParagraph(flowName, pBean);
		// 宋体 四号 左对齐 1.5倍行距
		pBean = new ParagraphBean("宋体", Constants.sanhao, false);
		pBean.setSpace(0f, 0f, vLine1);
		pBean.setInd(3.7F, 0, 0, 0.25F);
		getDocProperty().getTitleSect().createParagraph("");
		getDocProperty().getTitleSect().createParagraph("文件编号：" + flowInputNum,
				pBean);
		getDocProperty().getTitleSect().createParagraph("");
		getDocProperty().getTitleSect().createParagraph("文件版号：" + flowVersion,
				pBean);
		getDocProperty().getTitleSect().createParagraph("");

		// 宋体 五号 左对齐 最小值16磅值
		pBean = new ParagraphBean("宋体", Constants.xiaosi, false);
		pBean.setSpace(0f, 0f, vLine_atLeast16);
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(1);
		List<String[]> rowData = new ArrayList<String[]>();
		// 评审人集合
		/** 评审人集合 0 评审人类型 1名称 2日期 */
		List<Object[]> peopleList = processDownloadBean.getPeopleList();
		String[] str = null;
		for (Object[] obj : peopleList) {
			str = new String[] { "" + obj[0], "" + obj[1], "日期：", "" + obj[2] };
			rowData.add(str);
		}
		JecnWordUtil.createTab(getDocProperty().getTitleSect(), tabBean,
				new float[] { 4.36F, 4.36F, 4.36F, 4.36F }, rowData, pBean);
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		String flowName = processDownloadBean.getFlowName();
		String flowVersion = processDownloadBean.getFlowVersion();
		String flowInputNum = processDownloadBean.getFlowInputNum();
		String flowIsPublic = getPubOrSec();
		Table table = null;
		// 宋体 小四 居左
		ParagraphBean pBean = new ParagraphBean(Align.left, "宋体",
				Constants.xiaosi, false);
		Header hdr = sect.createHeader(type);
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		List<String[]> rowData = new ArrayList<String[]>();
		rowData
				.add(new String[] { "", "文件名称：", flowName, "密级：", flowIsPublic });
		rowData.add(new String[] { "", "文件编号：", flowInputNum, "版本：",
				flowVersion });
		table = JecnWordUtil.createTab(hdr, tabBean, new float[] { 5.82F,
				2.82F, 3F, 2.82F, 3F }, rowData, pBean);
		table.getRow(0).setHeight(0.56F);
		table.getRow(1).setHeight(0.5F);
		// 宋体 十一号 居中加粗
		pBean = new ParagraphBean(Align.center, "宋体", Constants.shiyihao, true);
		table.setCellStyle(1, pBean, Valign.top);
		table.setCellStyle(3, pBean, Valign.top);
		hdr.createParagraph("", pBean);
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体",
				Constants.xiaosi, false);
		Footer ftr = sect.createFooter(type);
		ftr.createParagraph("公司机密,未经批准不得扩散", pBean);

	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		firstSect.setSectBean(getSectBean());
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		createCommhdrftr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		addTitleSectContent();
		createCommhdrftr(titleSect);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.center,
				new FontBean("宋体", Constants.xiaosi, false));
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();
		// 所有边框 1.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.34 厘米
		tblStyle.setTableLeftInd(0.01F);
		// 设置单元格内边距父
		tblStyle.setCellMargin(0, 0, 0, 0);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center,
				new FontBean("宋体", Constants.shiyihao, true));
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left,
				new FontBean("宋体", Constants.xiaosi, false));
		textContentStyle.setSpace(0.1F, 0.1F, vLine1);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left,
				new FontBean("宋体", Constants.shisanhao, true));
		textTitleStyle.setSpace(0f, 0f, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
