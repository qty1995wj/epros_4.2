
package com.jecn.webservice.cetc29;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "intfsServiceWSXML", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface intfsServiceWSXML {

	@WebMethod(operationName = "getModelDatasXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String getModelDatasXML(
			@WebParam(name = "modelcode", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String modelcode,
			@WebParam(name = "queryCondition", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String queryCondition,
			@WebParam(name = "queryField", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String queryField);

	@WebMethod(operationName = "importEditingDataXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String importEditingDataXML(
			@WebParam(name = "code", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String code,
			@WebParam(name = "xmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String xmlStr);

	@WebMethod(operationName = "getCodeInfoXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String getCodeInfoXML(
			@WebParam(name = "modelcode", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String modelcode,
			@WebParam(name = "codeField", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String codeField,
			@WebParam(name = "queryXmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String queryXmlStr);

	@WebMethod(operationName = "importDataProCodeAuditXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String importDataProCodeAuditXML(
			@WebParam(name = "modelcode", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String modelcode,
			@WebParam(name = "codeField", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String codeField,
			@WebParam(name = "xmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String xmlStr);

	@WebMethod(operationName = "produceCodeAvailXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String produceCodeAvailXML(
			@WebParam(name = "modelcode", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String modelcode,
			@WebParam(name = "codeField", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String codeField,
			@WebParam(name = "xmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String xmlStr);

	@WebMethod(operationName = "importDataProCodeAvailXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String importDataProCodeAvailXML(
			@WebParam(name = "modelcode", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String modelcode,
			@WebParam(name = "codeField", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String codeField,
			@WebParam(name = "xmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String xmlStr);

	@WebMethod(operationName = "importDataProCodeEditXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String importDataProCodeEditXML(
			@WebParam(name = "modelcode", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String modelcode,
			@WebParam(name = "codeField", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String codeField,
			@WebParam(name = "xmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String xmlStr);

	@WebMethod(operationName = "produceCodeAuditXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String produceCodeAuditXML(
			@WebParam(name = "modelcode", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String modelcode,
			@WebParam(name = "codeField", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String codeField,
			@WebParam(name = "xmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String xmlStr);

	@WebMethod(operationName = "importFlowDataXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String importFlowDataXML(
			@WebParam(name = "code", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String code,
			@WebParam(name = "xmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String xmlStr);

	@WebMethod(operationName = "importEffectDataXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String importEffectDataXML(
			@WebParam(name = "code", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String code,
			@WebParam(name = "xmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String xmlStr);

	@WebMethod(operationName = "getDicInfoXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String getDicInfoXML(
			@WebParam(name = "modelcode", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String modelcode,
			@WebParam(name = "queryXmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String queryXmlStr);

	@WebMethod(operationName = "produceCodeEditXML", action = "")
	@WebResult(name = "return", targetNamespace = "")
	public String produceCodeEditXML(
			@WebParam(name = "modelcode", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String modelcode,
			@WebParam(name = "codeField", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String codeField,
			@WebParam(name = "xmlStr", targetNamespace = "http://www.meritit.com/ws/IntfsServiceWSXML") String xmlStr);

}
