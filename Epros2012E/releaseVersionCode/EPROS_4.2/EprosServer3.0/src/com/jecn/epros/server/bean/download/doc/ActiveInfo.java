package com.jecn.epros.server.bean.download.doc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.util.JecnUtil;

public class ActiveInfo implements Info {

	private IBaseDao baseDao;
	/**
	 * 活动与风险的关联关系表 0活动id 1活动名称 2风险id 3风险名称
	 */
	private List<Object[]> activeRisks;
	/**
	 * 活动与风险的关联关系表 0活动id 1活动名称 2IT系统id 3it系统名称
	 */
	private List<Object[]> activeIts;
	private List<JecnRisk> risks;
	private List<JecnFlowSustainTool> its;

	private Map<Long, List<JecnRisk>> activeToRiskMap;
	private Map<Long, List<JecnFlowSustainTool>> activeToItMap;

	public ActiveInfo(IBaseDao baseDao, List<JecnRisk> risks, List<Object[]> activeRisks,
			List<JecnFlowSustainTool> its, List<Object[]> activeIts) {
		this.baseDao = baseDao;
		this.risks = risks;
		this.activeRisks = activeRisks;
		this.its = its;
		this.activeIts = activeIts;
	}

	@Override
	public void init() {
		activeToRiskMap = new HashMap<Long, List<JecnRisk>>();
		activeToItMap = new HashMap<Long, List<JecnFlowSustainTool>>();
		if (JecnUtil.isNotEmpty(activeRisks) && JecnUtil.isNotEmpty(risks)) {
			for (Object[] objs : activeRisks) {
				Long activeId = Long.valueOf(objs[0].toString());
				Long riskId = Long.valueOf(objs[2].toString());
				for (JecnRisk risk : risks) {
					if (riskId.equals(risk.getId())) {
						List<JecnRisk> list = activeToRiskMap.get(activeId);
						if (list == null) {
							list = new ArrayList<JecnRisk>();
						}
						list.add(risk);
						activeToRiskMap.put(activeId, list);
					}
				}
			}
		}
		if (JecnUtil.isNotEmpty(activeIts) && JecnUtil.isNotEmpty(its)) {
			for (Object[] objs : activeIts) {
				Long activeId = Long.valueOf(objs[0].toString());
				Long riskId = Long.valueOf(objs[2].toString());
				for (JecnFlowSustainTool it : its) {
					if (riskId.equals(it.getFlowSustainToolId())) {
						List<JecnFlowSustainTool> list = activeToItMap.get(activeId);
						if (list == null) {
							list = new ArrayList<JecnFlowSustainTool>();
						}
						list.add(it);
						activeToItMap.put(activeId, list);
					}
				}
			}
		}
	}

	public String getItName(Long artivityId) {
		List<JecnFlowSustainTool> list = activeToItMap.get(artivityId);
		if (JecnUtil.isEmpty(list)) {
			return "";
		}
		Set<String> names = new HashSet<String>();
		for (JecnFlowSustainTool o : list) {
			names.add(o.getFlowSustainToolShow());
		}
		return JecnUtil.concat(names, "/");
	}

	public String getRiskName(Long artivityId) {
		List<JecnRisk> list = activeToRiskMap.get(artivityId);
		if (JecnUtil.isEmpty(list)) {
			return "";
		}
		Set<String> names = new HashSet<String>();
		for (JecnRisk o : list) {
			names.add(o.getRiskName());
		}
		return JecnUtil.concat(names, "/");
	}
	
	public String getRiskNum(Long artivityId) {
		List<JecnRisk> list = activeToRiskMap.get(artivityId);
		if (JecnUtil.isEmpty(list)) {
			return "";
		}
		Set<String> names = new HashSet<String>();
		for (JecnRisk o : list) {
			names.add(o.getRiskCode());
		}
		return JecnUtil.concat(names, "/");
	}
}
