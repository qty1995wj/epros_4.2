package com.jecn.epros.server.service.reports.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.temp.TempApproveTimeHeadersBean;
import com.jecn.epros.server.bean.process.temp.TempTaskApproveTime;
import com.jecn.epros.server.bean.process.temp.TempTaskInfoBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IFlowOrgImageDao;
import com.jecn.epros.server.dao.popedom.IOrgAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.popedom.IPositionAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPositionFigInfoDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.process.IProcessKPIDao;
import com.jecn.epros.server.dao.process.IProcessRecordDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.dao.system.IProcessRuleTypeDao;
import com.jecn.epros.server.service.reports.IWebReportsService;
import com.jecn.epros.server.util.JecnResourceUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.process.ProcessRoleWebBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.AgingProcessBean;
import com.jecn.epros.server.webBean.reports.ProcessAccessBean;
import com.jecn.epros.server.webBean.reports.ProcessActivitiesBean;
import com.jecn.epros.server.webBean.reports.ProcessAnalysisBean;
import com.jecn.epros.server.webBean.reports.ProcessApplicationBean;
import com.jecn.epros.server.webBean.reports.ProcessApplicationDetailsBean;
import com.jecn.epros.server.webBean.reports.ProcessConstructionBean;
import com.jecn.epros.server.webBean.reports.ProcessDetailBean;
import com.jecn.epros.server.webBean.reports.ProcessDetailLevel;
import com.jecn.epros.server.webBean.reports.ProcessDetailWebBean;
import com.jecn.epros.server.webBean.reports.ProcessDetailWebListBean;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceBean;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceWebBean;
import com.jecn.epros.server.webBean.reports.ProcessInputOutputBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIFollowBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIWebBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIWebListBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIWebValueBean;
import com.jecn.epros.server.webBean.reports.ProcessRoleBean;
import com.jecn.epros.server.webBean.reports.ProcessScanOptimizeBean;
import com.jecn.epros.server.webBean.reports.ProcessScanWebBean;
import com.jecn.epros.server.webBean.reports.ProcessScanWebListBean;

@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class WebReportsServiceImpl extends AbsBaseService<JecnFlowStructureT, Long> implements IWebReportsService {
	private Logger log = Logger.getLogger(WebReportsServiceImpl.class);
	private IFlowStructureDao flowStructureDao;
	private IFileDao fileDao;
	private IOrgAccessPermissionsDao orgAccessPermissionsDao;
	private IPositionAccessPermissionsDao positionAccessPermissionsDao;
	private IProcessRuleTypeDao processRuleTypeDao;
	private IProcessBasicInfoDao processBasicDao;
	private IPersonDao userDao;
	private IFlowOrgImageDao flowOrgImageDao;
	/** 岗位基本信息 */
	private IPositionFigInfoDao positionFigInfoDao;
	private IJecnConfigItemDao configItemDao;
	private IJecnDocControlDao docControlDao;
	private IProcessKPIDao processKPIDao;
	private IProcessRecordDao processRecordDao;

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	public IFileDao getFileDao() {
		return fileDao;
	}

	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
	}

	public IOrgAccessPermissionsDao getOrgAccessPermissionsDao() {
		return orgAccessPermissionsDao;
	}

	public void setOrgAccessPermissionsDao(IOrgAccessPermissionsDao orgAccessPermissionsDao) {
		this.orgAccessPermissionsDao = orgAccessPermissionsDao;
	}

	public IPositionAccessPermissionsDao getPositionAccessPermissionsDao() {
		return positionAccessPermissionsDao;
	}

	public void setPositionAccessPermissionsDao(IPositionAccessPermissionsDao positionAccessPermissionsDao) {
		this.positionAccessPermissionsDao = positionAccessPermissionsDao;
	}

	public IProcessRuleTypeDao getProcessRuleTypeDao() {
		return processRuleTypeDao;
	}

	public void setProcessRuleTypeDao(IProcessRuleTypeDao processRuleTypeDao) {
		this.processRuleTypeDao = processRuleTypeDao;
	}

	public IProcessBasicInfoDao getProcessBasicDao() {
		return processBasicDao;
	}

	public void setProcessBasicDao(IProcessBasicInfoDao processBasicDao) {
		this.processBasicDao = processBasicDao;
	}

	public IPersonDao getUserDao() {
		return userDao;
	}

	public void setUserDao(IPersonDao userDao) {
		this.userDao = userDao;
	}

	public IFlowOrgImageDao getFlowOrgImageDao() {
		return flowOrgImageDao;
	}

	public void setFlowOrgImageDao(IFlowOrgImageDao flowOrgImageDao) {
		this.flowOrgImageDao = flowOrgImageDao;
	}

	public IPositionFigInfoDao getPositionFigInfoDao() {
		return positionFigInfoDao;
	}

	public void setPositionFigInfoDao(IPositionFigInfoDao positionFigInfoDao) {
		this.positionFigInfoDao = positionFigInfoDao;
	}

	public IJecnConfigItemDao getConfigItemDao() {
		return configItemDao;
	}

	public void setConfigItemDao(IJecnConfigItemDao configItemDao) {
		this.configItemDao = configItemDao;
	}

	public IJecnDocControlDao getDocControlDao() {
		return docControlDao;
	}

	public void setDocControlDao(IJecnDocControlDao docControlDao) {
		this.docControlDao = docControlDao;
	}

	public IProcessKPIDao getProcessKPIDao() {
		return processKPIDao;
	}

	public void setProcessKPIDao(IProcessKPIDao processKPIDao) {
		this.processKPIDao = processKPIDao;
	}

	public IProcessRecordDao getProcessRecordDao() {
		return processRecordDao;
	}

	public void setProcessRecordDao(IProcessRecordDao processRecordDao) {
		this.processRecordDao = processRecordDao;
	}

	/**
	 * 搜索流程时效性统计sql
	 * 
	 * @param resOrgId
	 * @param processId
	 * @param time
	 * @param updateType
	 * @param projectId
	 * @return
	 */
	public String getSqlAgingProcessImage(long resOrgId, long processId, long time, int updateType, long projectId) {
		String sql = "	select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
				+ " ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
				+ " ,t.pub_time" + " from jecn_flow_basic_info jfbi";
		if (resOrgId == -1) {
			sql = sql
					+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
		}
		sql = sql
				+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
				+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			String sqlSQLSERVER = "";
			if (processId != -1) {
				sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
						+ processId
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
			}
			if (resOrgId != -1) {
				if (processId != -1) {
					sqlSQLSERVER = sqlSQLSERVER + ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				} else {
					sqlSQLSERVER = sqlSQLSERVER + "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				}
			}
			sql = sqlSQLSERVER + sql;
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
						+ " where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
			}
			if (processId != -1) {
				sql = sql + ",MY_FLOW t";
			} else {
				sql = sql + " ,jecn_flow_structure t";
			}

		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
						+ resOrgId + ") org,jecn_flow_related_org jfro_t"
						+ "       where org.org_id=jfro_t.org_id) leadOrg";
			}
			if (processId != -1) {
				sql = sql
						+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
						+ processId + ") t";
			} else {
				sql = sql + " ,jecn_flow_structure t";
			}
		}
		sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
		if (resOrgId != -1) {
			sql = sql + "  and t.flow_id = leadOrg.flow_id";
		}
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			if (updateType == 1) {
				sql = sql + " and datediff(m,t.pub_time,getdate())>=" + time;
			} else if (updateType == 2) {
				sql = sql + " and datediff(m,t.pub_time,getdate())<" + time;
			}
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (updateType == 1) {
				sql = sql + " and months_between(sysdate,t.pub_time)>=" + time;
			} else if (updateType == 2) {
				sql = sql + " and months_between(sysdate,t.pub_time)<" + time;
			}
		}
		// sql = sql + "  order by t.pre_flow_id,t.sort_id,t.flow_id";
		return sql;
	}

	/**
	 * 查询流程时效性统计
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param time
	 *            1,2，，，，12 代表选取的 12个月中 一个
	 * @param updateType
	 *            0全部 ， 1未更新 ， 2已更新
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> findAgingProcess(long resOrgId, long processId, long time, int updateType,
			long projectId, int start, int limit) throws Exception {
		try {
			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {
				String withSql = "";
				String sql = "	select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
						+ " ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ " ,t.pub_time" + " from jecn_flow_basic_info jfbi";
				if (resOrgId == -1) {
					sql = sql
							+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
				}
				sql = sql
						+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
						+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id";

				if (processId != -1) {
					withSql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (resOrgId != -1) {
					if (processId != -1) {
						withSql = withSql
								+ ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					} else {
						withSql = withSql
								+ "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
							+ " where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
				}
				if (processId != -1) {
					sql = sql + ",MY_FLOW t";
				} else {
					sql = sql + " ,jecn_flow_structure t";
				}
				sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				if (updateType == 1) {
					sql = sql + " and datediff(m,t.pub_time,getdate())>=" + time;
				} else if (updateType == 2) {
					sql = sql + " and datediff(m,t.pub_time,getdate())<" + time;
				}
				if (processId != -1 || resOrgId != -1) {
					listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit);
				} else {
					listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit);
				}

			} else if (JecnCommon.isOracle()) {
				String sql = "	select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
						+ " ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ " ,t.pub_time" + " from jecn_flow_basic_info jfbi";
				if (resOrgId == -1) {
					sql = sql
							+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
				}
				sql = sql
						+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
						+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id";

				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ resOrgId + ") org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id) leadOrg";
				}
				if (processId != -1) {
					sql = sql
							+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
							+ processId + ") t";
				} else {
					sql = sql + " ,jecn_flow_structure t";
				}
				sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				if (updateType == 1) {
					sql = sql + " and months_between(sysdate,t.pub_time)>=" + time;
				} else if (updateType == 2) {
					sql = sql + " and months_between(sysdate,t.pub_time)<" + time;
				}
				sql = sql + "  order by t.pre_flow_id,t.sort_id,t.flow_id";
				listObj = flowStructureDao.listNativeSql(sql, start, limit);
			}
			if (listObj == null) {
				return null;
			}
			List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();

			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessWebBean processWebBean = new ProcessWebBean();
				// 流程ID
				processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
				// 流程名称
				if (obj[1] != null) {
					processWebBean.setFlowName(obj[1].toString());
				}
				// 流程编号
				if (obj[2] != null) {
					processWebBean.setFlowIdInput(obj[2].toString());
				}
				// 责任部门
				if (obj[3] != null && obj[4] != null) {
					processWebBean.setOrgId(Long.valueOf(obj[3].toString()));
					processWebBean.setOrgName(obj[4].toString());
				}
				// 责任人
				if (obj[5] != null && obj[6] != null && obj[7] != null) {
					processWebBean.setTypeResPeople(Integer.parseInt(obj[5].toString()));
					processWebBean.setResPeopleId(Long.valueOf(obj[6].toString()));
					processWebBean.setResPeopleName(obj[7].toString());
				}
				// 更新时间
				if (obj[8] != null) {
					// 发布时间
					Date updateTime = JecnCommon.getDateByString(obj[8].toString());
					processWebBean.setPubDate(JecnCommon.getStringbyDate(updateTime));
					// 现今时间
					// 更新状态
					if (JecnUtil.getMonthSpace(updateTime, new Date()) >= time) {
						processWebBean.setUpdateType(0);
					} else {
						processWebBean.setUpdateType(1);
					}
				}
				listProcessWebBean.add(processWebBean);
			}
			return listProcessWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public AgingProcessBean findAllAgingProcess(long resOrgId, long processId, long time, int updateType, long projectId)
			throws Exception {
		try {
			AgingProcessBean agingProcessBean = new AgingProcessBean();
			String sql = getSqlAgingProcessImage(resOrgId, processId, time, updateType, projectId);
			List<Object[]> listObj = flowStructureDao.listNativeSql(sql);
			List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
			/** 未更新次数 */
			int failedToUpdate = 0;
			/** 已更新次数 */
			int hasBeenUpdated = 0;
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessWebBean processWebBean = new ProcessWebBean();
				// 流程ID
				processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
				// 流程名称
				if (obj[1] != null) {
					processWebBean.setFlowName(obj[1].toString());
				}
				// 流程编号
				if (obj[2] != null) {
					processWebBean.setFlowIdInput(obj[2].toString());
				}
				// 责任部门
				if (obj[3] != null && obj[4] != null) {
					processWebBean.setOrgId(Long.valueOf(obj[3].toString()));
					processWebBean.setOrgName(obj[4].toString());
				}
				// 责任人
				if (obj[5] != null && obj[6] != null && obj[7] != null) {
					processWebBean.setTypeResPeople(Integer.parseInt(obj[5].toString()));
					processWebBean.setResPeopleId(Long.valueOf(obj[6].toString()));
					processWebBean.setResPeopleName(obj[7].toString());
				}
				// 更新时间
				if (obj[8] != null) {
					// 发布时间
					Date updateTime = JecnCommon.getDateByString(obj[8].toString());
					processWebBean.setPubDate(JecnCommon.getStringbyDate(updateTime));
					// 现今时间
					// 更新状态
					if (JecnUtil.getMonthSpace(updateTime, new Date()) >= time) {
						failedToUpdate++;
						processWebBean.setUpdateType(0);
					} else {
						hasBeenUpdated++;
						processWebBean.setUpdateType(1);
					}
				}
				listProcessWebBean.add(processWebBean);
			}
			agingProcessBean.setProcessWebList(listProcessWebBean);
			agingProcessBean.setFailedToUpdate(failedToUpdate);
			agingProcessBean.setHasBeenUpdated(hasBeenUpdated);
			return agingProcessBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 搜索流程时效性统计sql
	 * 
	 * @param resOrgId
	 * @param processId
	 * @param time
	 * @param updateType
	 * @param projectId
	 * @return
	 */
	public String getSqlAgingProcessImageCount(long resOrgId, long processId, long time, int updateType, long projectId) {
		String sql = " select count(t.flow_id) from jecn_flow_basic_info jfbi ";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			String sqlSQLSERVER = "";
			if (processId != -1) {
				sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
						+ processId
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
			}
			if (resOrgId != -1) {
				if (processId != -1) {
					sqlSQLSERVER = sqlSQLSERVER + ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				} else {
					sqlSQLSERVER = sqlSQLSERVER + "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				}
			}
			sql = sqlSQLSERVER + sql;
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
						+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
			}
			if (processId != -1) {
				sql = sql + ",MY_FLOW t";
			} else {
				sql = sql + ",jecn_flow_structure t";
			}
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
						+ resOrgId + ") org,jecn_flow_related_org jfro_t"
						+ "       where org.org_id=jfro_t.org_id) leadOrg";
			}
			if (processId != -1) {
				sql = sql
						+ ",(select jfs.* from jecn_flow_structure jfs  CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
						+ processId + ") t";
			} else {
				sql = sql + ",jecn_flow_structure t";
			}
		}
		sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
		if (resOrgId != -1) {
			sql = sql + "  and t.flow_id = leadOrg.flow_id";
		}
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			if (updateType == 1) {
				sql = sql + " and datediff(m,t.pub_time,getdate())>=" + time;
			} else if (updateType == 2) {
				sql = sql + " and datediff(m,t.pub_time,getdate())<" + time;
			}
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (updateType == 1) {
				sql = sql + " and months_between(sysdate,t.pub_time)>=" + time;
			} else if (updateType == 2) {
				sql = sql + " and months_between(sysdate,t.pub_time)<" + time;
			}
		}
		return sql;
	}

	/**
	 * 查询流程时效性统计分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param time
	 *            1,2，，，，12 代表选取的 12个月中 一个
	 * @param updateType
	 *            0全部 ， 1未更新 ， 2已更新
	 * @return
	 * @throws Exception
	 */
	public int findAgingProcessCount(long resOrgId, long processId, long time, int updateType, long projectId)
			throws Exception {
		try {
			String sql = getSqlAgingProcessImageCount(resOrgId, processId, time, updateType, projectId);
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public AgingProcessBean findAgingProcessImage(long resOrgId, long processId, long time, int updateType,
			long projectId) throws Exception {
		try {
			if (updateType == 1) {
				String sql = getSqlAgingProcessImageCount(resOrgId, processId, time, updateType, projectId);
				AgingProcessBean agingProcessBean = new AgingProcessBean();
				agingProcessBean.setFailedToUpdate(this.flowStructureDao.countAllByParamsNativeSql(sql));
				agingProcessBean.setHasBeenUpdated(0);
				return agingProcessBean;
			} else if (updateType == 2) {
				String sql = getSqlAgingProcessImageCount(resOrgId, processId, time, updateType, projectId);
				AgingProcessBean agingProcessBean = new AgingProcessBean();
				agingProcessBean.setFailedToUpdate(0);
				agingProcessBean.setHasBeenUpdated(this.flowStructureDao.countAllByParamsNativeSql(sql));
				return agingProcessBean;
			} else {
				String sql = getSqlAgingProcessImageCount(resOrgId, processId, time, updateType, projectId);
				String noUpateSql = "";
				if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					noUpateSql = sql + " and datediff(m,t.pub_time,getdate())>=" + time;
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
					noUpateSql = sql + " and months_between(sysdate,t.pub_time)>=" + time;
				}
				AgingProcessBean agingProcessBean = new AgingProcessBean();
				agingProcessBean.setFailedToUpdate(this.flowStructureDao.countAllByParamsNativeSql(noUpateSql));
				String updateSql = "";
				if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					updateSql = sql + " and datediff(m,t.pub_time,getdate())<" + time;
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
					updateSql = sql + " and months_between(sysdate,t.pub_time)<" + time;
				}
				agingProcessBean.setHasBeenUpdated(this.flowStructureDao.countAllByParamsNativeSql(updateSql));
				return agingProcessBean;
			}

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 搜索流程覆盖度统计sql
	 * 
	 * @param resOrgId
	 * @param processId
	 * @param time
	 * @param constructionType
	 *            0是全部 ，1是审批中 2是已发布 3是待建
	 * @param projectId
	 * @return
	 */
	public String getSqlProcessConstruction(long resOrgId, long processId, int constructionType, long projectId) {
		String sql = " select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
				+ "        ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
				+ "        ,t.pub_time,jtbn.state,jfs_process.flow_id as pub_flow_id,t.sort_id,t.pre_flow_id"
				+ "        from jecn_flow_basic_info_t jfbi";
		if (resOrgId == -1) {
			sql = sql
					+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org_t jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
		}
		sql = sql
				+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
				+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id"
				+ " left join jecn_flow_structure jfs_process on jfs_process.del_state=0 and jfs_process.flow_id=jfbi.flow_id"
				+ " left join jecn_task_bean_new jtbn on jtbn.task_type = 0" + " and jtbn.r_id = jfbi.flow_id"
				+ " and jtbn.is_lock = 1 and jtbn.state<>5";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			String sqlCommon = "";
			if (processId != -1) {
				sqlCommon = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JFT WHERE JFT.FLOW_ID = "
						+ processId
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
			}
			if (resOrgId != -1) {
				if (processId != -1) {
					sqlCommon = sqlCommon + ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID =" + resOrgId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				} else {
					sqlCommon = sqlCommon + "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				}
			}
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org_t jfro_t"
						+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
			}
			if (processId != -1) {
				sql = sql + ",MY_FLOW t";
			} else {
				sql = sql + ",jecn_flow_structure_t t";
			}
			sql = sql + " where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
			if (resOrgId != -1) {
				sql = sql + "  and t.flow_id = leadOrg.flow_id";
			}
			sql = sqlCommon + " select process.* from (" + sql + ") process";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
						+ resOrgId + ") org,jecn_flow_related_org_t jfro_t"
						+ "       where org.org_id=jfro_t.org_id) leadOrg";
			}
			if (processId != -1) {
				sql = sql
						+ ",(select jfs.* from jecn_flow_structure_t jfs  CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
						+ processId + ") t";
			} else {
				sql = sql + " ,jecn_flow_structure_t t";
			}
			sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
			if (resOrgId != -1) {
				sql = sql + "  and t.flow_id = leadOrg.flow_id";
			}
			sql = "select process.* from (" + sql + ") process";
		}
		// 全部
		if (constructionType == 0) {
			return sql;
		}// 审批中
		else if (constructionType == 1) {
			sql = sql + " where  process.pub_flow_id is null and process.state is not null";
		}// 发布
		else if (constructionType == 2) {
			sql = sql + " where  process.pub_flow_id is not null";
		}// 待建中
		else if (constructionType == 3) {
			sql = sql + " where  process.pub_flow_id is null and process.state is null";
		}
		sql = sql + " order by process.pre_flow_id,process.sort_id,process.flow_id";
		return sql;
	}

	/**
	 * 搜索流程覆盖度统计sql
	 * 
	 * @param resOrgId
	 * @param processId
	 * @param time
	 * @param constructionType
	 *            0是全部 ，1是审批中 2是已发布 3是待建
	 * @param projectId
	 * @return
	 */
	private String getSqlProcessConstructionCount(long resOrgId, long processId, int constructionType, long projectId) {
		String sql = " select t.flow_id,jfbi_t.flow_id as pub_flow_id,jtbn.state from jecn_flow_basic_info_t jfbi"
				+ " left join jecn_flow_basic_info jfbi_t on jfbi_t.flow_id = jfbi.flow_id"
				+ " left join jecn_task_bean_new jtbn on jtbn.task_type = 0" + " and jtbn.r_id = jfbi.flow_id"
				+ " and jtbn.is_lock = 1 and jtbn.state<>5";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			String sqlCommon = "";
			if (processId != -1) {
				sqlCommon = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JFT WHERE JFT.FLOW_ID = "
						+ processId
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
			}
			if (resOrgId != -1) {
				if (processId != -1) {
					sqlCommon = sqlCommon + ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID =" + resOrgId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				} else {
					sqlCommon = sqlCommon + "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				}
			}

			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org_t jfro_t"
						+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
			}
			if (processId != -1) {
				sql = sql + ",MY_FLOW t";
			} else {
				sql = sql + ",jecn_flow_structure_t t";
			}
			sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
			if (resOrgId != -1) {
				sql = sql + "  and t.flow_id = leadOrg.flow_id";
			}
			sql = sqlCommon + " select count(process.flow_id) from (" + sql + ") process";

		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
						+ resOrgId + ") org,jecn_flow_related_org_t jfro_t"
						+ "       where org.org_id=jfro_t.org_id) leadOrg";
			}
			if (processId != -1) {
				sql = sql
						+ ",(select jfs.* from jecn_flow_structure_t jfs where jfs.isflow=1 and jfs.del_state=0 CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
						+ processId + ") t";
			} else {
				sql = sql + ",jecn_flow_structure_t t";
			}
			sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
			if (resOrgId != -1) {
				sql = sql + "  and t.flow_id = leadOrg.flow_id";
			}
			sql = "select count(process.flow_id) from (" + sql + ") process";
		}
		// 全部
		if (constructionType == 0) {
			return sql;
		}// 审批中
		else if (constructionType == 1) {
			sql = sql + " where  process.pub_flow_id is null and process.state is not null";
		}// 发布
		else if (constructionType == 2) {
			sql = sql + " where  process.pub_flow_id is not null";
		}// 待建中
		else if (constructionType == 3) {
			sql = sql + " where  process.pub_flow_id is null and process.state is null";
		}
		return sql;
	}

	public ProcessConstructionBean findProcessConstructionImage(long resOrgId, long processId, int constructionType,
			long projectId) throws Exception {
		try {
			// 全部
			if (constructionType == 0) {
				// 审批中
				String sql = getSqlProcessConstructionCount(resOrgId, processId, constructionType, projectId);
				sql = sql + " where process.pub_flow_id is null and process.state is not null";
				ProcessConstructionBean processConstructionBean = new ProcessConstructionBean();
				processConstructionBean.setEaaCount(this.flowStructureDao.countAllByParamsNativeSql(sql));
				// 发布
				sql = getSqlProcessConstructionCount(resOrgId, processId, constructionType, projectId);
				sql = sql + " where process.pub_flow_id is not null";
				processConstructionBean.setReleasedCount(this.flowStructureDao.countAllByParamsNativeSql(sql));
				// 待建中
				sql = getSqlProcessConstructionCount(resOrgId, processId, constructionType, projectId);
				sql = sql + " where  process.pub_flow_id is null and process.state is null";
				processConstructionBean.setTbbCount(this.flowStructureDao.countAllByParamsNativeSql(sql));
				return processConstructionBean;
			}// 审批中
			else if (constructionType == 1) {
				String sql = getSqlProcessConstructionCount(resOrgId, processId, constructionType, projectId);

				ProcessConstructionBean processConstructionBean = new ProcessConstructionBean();
				processConstructionBean.setEaaCount(this.flowStructureDao.countAllByParamsNativeSql(sql));
				return processConstructionBean;
			}// 发布
			else if (constructionType == 2) {
				String sql = getSqlProcessConstructionCount(resOrgId, processId, constructionType, projectId);

				ProcessConstructionBean processConstructionBean = new ProcessConstructionBean();
				processConstructionBean.setReleasedCount(this.flowStructureDao.countAllByParamsNativeSql(sql));
				return processConstructionBean;
			}// 待建中
			else if (constructionType == 3) {
				String sql = getSqlProcessConstructionCount(resOrgId, processId, constructionType, projectId);

				ProcessConstructionBean processConstructionBean = new ProcessConstructionBean();
				processConstructionBean.setTbbCount(this.flowStructureDao.countAllByParamsNativeSql(sql));
				return processConstructionBean;
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return null;
	}

	/**
	 * 流程建设覆盖度
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param processUserID
	 *            流程责任人ID
	 * @param releaseDate
	 *            发布时间
	 * @param constructionType
	 *            建设状态 0是全部 ，1是审批中 2是已发布 3是待建
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> findProcessConstruction(long resOrgId, long processId, int constructionType,
			long projectId, int start, int limit) throws Exception {
		try {
			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {

				String sql = " select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
						+ "        ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ "        ,t.pub_time,jtbn.state,jfs_process.flow_id as pub_flow_id,t.sort_id,t.pre_flow_id"
						+ "        from jecn_flow_basic_info_t jfbi";
				if (resOrgId == -1) {
					sql = sql
							+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org_t jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
				}
				sql = sql
						+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
						+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id"
						+ " left join jecn_flow_structure jfs_process on jfs_process.del_state=0 and jfs_process.flow_id=jfbi.flow_id"
						+ " left join jecn_task_bean_new jtbn on jtbn.task_type = 0" + " and jtbn.r_id = jfbi.flow_id"
						+ " and jtbn.is_lock = 1 and jtbn.state<>5";
				String withSql = "";
				if (processId != -1) {
					withSql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (resOrgId != -1) {
					if (processId != -1) {
						withSql = withSql
								+ ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					} else {
						withSql = withSql
								+ "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org_t jfro_t"
							+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
				}
				if (processId != -1) {
					sql = sql + ",MY_FLOW t";
				} else {
					sql = sql + ",jecn_flow_structure_t t";
				}
				sql = sql + " where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				sql = " select process.* from (" + sql + ") process";
				// 审批中
				if (constructionType == 1) {
					sql = sql + " where  process.pub_flow_id is null and process.state is not null";
				}// 发布
				else if (constructionType == 2) {
					sql = sql + " where  process.pub_flow_id is not null";
				}// 待建中
				else if (constructionType == 3) {
					sql = sql + " where  process.pub_flow_id is null and process.state is null";
				}
				if (processId != -1 || resOrgId != -1) {
					listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit);
				} else {
					listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit);
				}

			} else if (JecnCommon.isOracle()) {
				String sql = " select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
						+ "        ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ "        ,t.pub_time,jtbn.state,jfs_process.flow_id as pub_flow_id,t.sort_id,t.pre_flow_id"
						+ "        from jecn_flow_basic_info_t jfbi";
				if (resOrgId == -1) {
					sql = sql
							+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org_t jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
				}
				sql = sql
						+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
						+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id"
						+ " left join jecn_flow_structure jfs_process on jfs_process.del_state=0 and jfs_process.flow_id=jfbi.flow_id"
						+ " left join jecn_task_bean_new jtbn on jtbn.task_type = 0" + " and jtbn.r_id = jfbi.flow_id"
						+ " and jtbn.is_lock = 1 and jtbn.state<>5";
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ resOrgId + ") org,jecn_flow_related_org_t jfro_t"
							+ "       where org.org_id=jfro_t.org_id) leadOrg";
				}
				if (processId != -1) {
					sql = sql
							+ ",(select jfs.* from jecn_flow_structure_t jfs  CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
							+ processId + ") t";
				} else {
					sql = sql + " ,jecn_flow_structure_t t";
				}
				sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				sql = "select process.* from (" + sql + ") process";
				// 审批中
				if (constructionType == 1) {
					sql = sql + " where  process.pub_flow_id is null and process.state is not null";
				}// 发布
				else if (constructionType == 2) {
					sql = sql + " where  process.pub_flow_id is not null";
				}// 待建中
				else if (constructionType == 3) {
					sql = sql + " where  process.pub_flow_id is null and process.state is null";
				}
				sql = sql + " order by process.pre_flow_id,process.sort_id,process.flow_id";

				listObj = flowStructureDao.listNativeSql(sql, start, limit);
			}
			if (listObj == null) {
				return null;
			}
			List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();

			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessWebBean processWebBean = new ProcessWebBean();
				// 流程ID
				processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
				// 流程名称
				if (obj[1] != null) {
					processWebBean.setFlowName(obj[1].toString());
				}
				// 流程编号
				if (obj[2] != null) {
					processWebBean.setFlowIdInput(obj[2].toString());
				}
				// 责任部门
				if (obj[3] != null && obj[4] != null) {
					processWebBean.setOrgId(Long.valueOf(obj[3].toString()));
					processWebBean.setOrgName(obj[4].toString());
				}
				// 责任人
				if (obj[5] != null && obj[6] != null && obj[7] != null) {
					processWebBean.setTypeResPeople(Integer.parseInt(obj[5].toString()));
					processWebBean.setResPeopleId(Long.valueOf(obj[6].toString()));
					processWebBean.setResPeopleName(obj[7].toString());
				}
				// 更新时间
				if (obj[8] != null) {
					// 发布时间
					Date updateTime = JecnCommon.getDateByString(obj[8].toString());
					processWebBean.setPubDate(JecnCommon.getStringbyDate(updateTime));
				}
				if (obj[10] != null) {
					// 发布
					processWebBean.setTypeByData(2);
				} else {
					// 发布状态
					if (obj[9] != null) {
						// 1是审批
						processWebBean.setTypeByData(1);
					} else {
						// 待建
						processWebBean.setTypeByData(0);
					}
				}
				listProcessWebBean.add(processWebBean);
			}
			return listProcessWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public ProcessConstructionBean findAllProcessConstruction(long resOrgId, long processId, int constructionType,
			long projectId) throws Exception {
		try {
			String sql = getSqlProcessConstruction(resOrgId, processId, constructionType, projectId);
			List<Object[]> listObj = flowStructureDao.listNativeSql(sql);
			List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
			ProcessConstructionBean processConstructionBean = new ProcessConstructionBean();
			/** 审批中个数 */
			int eaaCount = 0;
			/** 已发布个数 */
			int releasedCount = 0;
			/** 待建个数 */
			int tbbCount = 0;
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessWebBean processWebBean = new ProcessWebBean();
				// 流程ID
				processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
				// 流程名称
				if (obj[1] != null) {
					processWebBean.setFlowName(obj[1].toString());
				}
				// 流程编号
				if (obj[2] != null) {
					processWebBean.setFlowIdInput(obj[2].toString());
				}
				// 责任部门
				if (obj[3] != null && obj[4] != null) {
					processWebBean.setOrgId(Long.valueOf(obj[3].toString()));
					processWebBean.setOrgName(obj[4].toString());
				}
				// 责任人
				if (obj[5] != null && obj[6] != null && obj[7] != null) {
					processWebBean.setTypeResPeople(Integer.parseInt(obj[5].toString()));
					processWebBean.setResPeopleId(Long.valueOf(obj[6].toString()));
					processWebBean.setResPeopleName(obj[7].toString());
				}
				// 更新时间
				if (obj[8] != null) {
					// 发布时间
					String updateTime = JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[8].toString()));
					processWebBean.setPubDate(updateTime);
				}
				if (obj[10] != null) {
					releasedCount++;
					// 发布
					processWebBean.setTypeByData(2);
				} else {
					// 发布状态
					if (obj[9] != null) {
						// 1是审批
						eaaCount++;
						processWebBean.setTypeByData(1);
					} else {
						// 待建
						tbbCount++;
						processWebBean.setTypeByData(0);
					}
				}
				listProcessWebBean.add(processWebBean);
			}
			processConstructionBean.setEaaCount(eaaCount);
			processConstructionBean.setTbbCount(tbbCount);
			processConstructionBean.setReleasedCount(releasedCount);
			processConstructionBean.setProcessWebList(listProcessWebBean);
			return processConstructionBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 流程建设覆盖度分页数
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param resOrgId
	 *            责任部门ID
	 * @param processId
	 *            流程ID
	 * @param processUserID
	 *            流程责任人ID
	 * @param releaseDate
	 *            发布时间
	 * @param constructionType
	 *            建设状态 0是全部 ，1是审批中 2是已发布 3是待建
	 * @return
	 * @throws Exception
	 */
	public int findProcessConstructionCount(long resOrgId, long processId, int constructionType, long projectId)
			throws Exception {
		try {
			String sql = getSqlProcessConstructionCount(resOrgId, processId, constructionType, projectId);
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 流程分析统计(excel下载)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param timeType
	 *            时间类型 0是全部 ， 1是年，2是季，3是月，4是周，5是日
	 * @param processType
	 *            流程类别
	 * @param posId
	 *            岗位ID
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ProcessAnalysisBean> findAllProcessAnalysis(long processId, long timeType, long processType,
			long posId, long projectId) throws Exception {
		try {
			String sql = " select jfs.flow_id,jfd.quantity";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				sql = sqlSQLSERVER + sql;
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
			}
			sql = sql + ",jecn_flow_driver jfd";
			if (processType != -1) {
				sql = sql + ",jecn_flow_type jft";
			}
			sql = sql + ",jecn_flow_basic_info jfbi";
			sql = sql
					+ " where  jfs.flow_id=jfbi.flow_id and jfbi.driver_type=1 and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
					+ projectId;
			sql = sql + " and jfbi.flow_id = jfd.flow_id";
			if (timeType != -1) {
				sql = sql + " and jfd.quantity='" + timeType + "'";
			} else {
				sql = sql + " and jfd.quantity in ('1','2','3','4','5')";
			}
			if (processType != -1) {
				sql = sql + "  and jfbi.type_id=jft.type_id and jft.type_id=" + processType;
			}
			if (posId != -1) {
				sql = sql
						+ " and (jfs.flow_id in"
						+ "(select jfs_process.flow_id"
						+ "  from process_station_related psr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
						+ "  where psr.type='0' and psr.figure_position_id="
						+ posId
						+ " and psr.figure_flow_id=jfsi.figure_id"
						+ "  and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0)"
						+ "  or"
						+ "  jfs.flow_id in"
						+ "  (select jfs_process.flow_id"
						+ "         from process_station_related psr,jecn_position_group_r jpgr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
						+ "         where psr.type='1' and   psr.figure_position_id=jpgr.group_id and jpgr.figure_id="
						+ posId + " and psr.figure_flow_id=jfsi.figure_id"
						+ "         and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0))";
			}
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			List<ProcessAnalysisBean> listProcessAnalysisBean = new ArrayList<ProcessAnalysisBean>();
			for (int i = 1; i <= 5; i++) {
				ProcessAnalysisBean processAnalysisBean = new ProcessAnalysisBean();
				processAnalysisBean.setTimeType(String.valueOf(i));
				int count = 0;
				for (Object[] obj : listObj) {
					if (obj == null || obj[0] == null || obj[1] == null) {
						continue;
					}
					if (Integer.parseInt(obj[1].toString()) == i) {
						count++;
					}
				}
				processAnalysisBean.setCountTotal(count);
				listProcessAnalysisBean.add(processAnalysisBean);
			}
			return listProcessAnalysisBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessInputOutputBean> findAllProcessInputOutput(long processId, long resOrgId, long projectId)
			throws Exception {
		try {
			String sql = this.getSqlProcessInputOutputBean(processId, resOrgId, projectId);
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			sql = " select flow_jfs.flow_id,flow_jfs.flow_name,flow_jfsi.IMPL_TYPE,flow_jfsi.flow_id as common_flow_id"
					+ "            from jecn_flow_structure_image flow_jfsi,jecn_flow_structure flow_jfs"
					+ "            where flow_jfsi.IMPL_TYPE IN (1, 2) and flow_jfsi.figure_type in "
					+ JecnCommonSql.getImplString()
					+ " and flow_jfsi.link_flow_id=flow_jfs.flow_id and flow_jfs.del_state=0"
					+ "            and flow_jfsi.flow_id in (";
			sql = sql + " select t.flow_id from jecn_flow_basic_info jfbi";
			// 获得流程下的上下游流程
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (resOrgId != -1) {
					if (processId != -1) {
						sqlSQLSERVER = sqlSQLSERVER
								+ ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					} else {
						sqlSQLSERVER = sqlSQLSERVER
								+ "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				sql = sqlSQLSERVER + sql;
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
				}
				if (processId != -1) {
					sql = sql + ",MY_FLOW t";
				} else {
					sql = sql + ",JECN_FLOW_STRUCTURE t";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ resOrgId + ") org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id) leadOrg";
				}
				if (processId != -1) {
					sql = sql
							+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
							+ processId + ") t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}
			}
			sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
			if (resOrgId != -1) {
				sql = sql + "  and t.flow_id = leadOrg.flow_id";
			}
			sql = sql + ")";
			List<Object[]> listUpperLower = this.flowStructureDao.listNativeSql(sql);
			List<ProcessInputOutputBean> listResult = new ArrayList<ProcessInputOutputBean>();

			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessInputOutputBean processInputOutputBean = new ProcessInputOutputBean();
				// 流程ID
				processInputOutputBean.setProcessId(Long.valueOf(obj[0].toString()));
				// 流程名称
				if (obj[1] != null)
					processInputOutputBean.setProcessName(obj[1].toString());
				List<ProcessWebBean> listProcessWebBeanUpper = new ArrayList<ProcessWebBean>();
				List<ProcessWebBean> listProcessWebBeanLower = new ArrayList<ProcessWebBean>();
				this.getUpperLowerFlowByFlowId(processInputOutputBean.getProcessId(), listProcessWebBeanUpper,
						listProcessWebBeanLower, listUpperLower);
				processInputOutputBean.setUpstreamProcess(listProcessWebBeanUpper);
				processInputOutputBean.setUpstreamProcessCount(listProcessWebBeanUpper.size());
				processInputOutputBean.setDownstreamProcess(listProcessWebBeanLower);
				processInputOutputBean.setDownstreamProcessCount(listProcessWebBeanLower.size());
				listResult.add(processInputOutputBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 获得流程上下游流程
	 * 
	 * @param flowId
	 * @param listProcessWebBeanUpper
	 * @param listProcessWebBeanLower
	 * @param listUpperLower
	 */
	private void getUpperLowerFlowByFlowId(Long flowId, List<ProcessWebBean> listProcessWebBeanUpper,
			List<ProcessWebBean> listProcessWebBeanLower, List<Object[]> listUpperLower) {
		for (Object[] obj : listUpperLower) {
			if (obj == null || obj[0] == null || obj[2] == null || obj[3] == null) {
				continue;
			}
			if (flowId.toString().equals(obj[3].toString())) {
				ProcessWebBean processWebBean = new ProcessWebBean();
				processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
				if (obj[1] != null) {
					processWebBean.setFlowName(obj[1].toString());
				}
				// 上游流程
				if ("1".equals(obj[2].toString())) {
					listProcessWebBeanUpper.add(processWebBean);
				}// 下游流程
				else if ("2".equals(obj[2].toString())) {
					listProcessWebBeanLower.add(processWebBean);

				}
			}
		}
	}

	/**
	 * 查询流程角色数统计(excel下载)
	 * 
	 * @author fuzhh Feb 28, 2013
	 * @param processId
	 *            流程ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ProcessRoleBean> findAllProcessRole(long processId, long projectId) throws Exception {
		try {
			String sql = " select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,leadOrg.org_id,leadOrg.org_name,jfbi.type_res_people,jfbi.res_people_id,"
					+ "  case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
					+ "  ,(select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
					+ JecnCommonSql.getRoleString() + ") role_count";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				sql = sqlSQLSERVER + sql;
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
			}
			sql = sql + ",jecn_flow_basic_info jfbi"
					+ "       left join (select jfo.org_id,jfo.org_name,jfro.flow_id from"
					+ "       jecn_flow_org jfo,jecn_flow_related_org jfro where jfro.org_id ="
					+ "       jfo.org_id and jfo.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id"
					+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
					+ "       left join (select jfoi.figure_id,jfoi.figure_text from"
					+ "       jecn_flow_org_image jfoi,jecn_flow_org org where jfoi.org_id=org.org_id"
					+ "       and org.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
			sql = sql + " where  jfs.flow_id=jfbi.flow_id and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
					+ projectId + " order by jfs.pre_flow_id,jfs.sort_id,jfs.flow_id";
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			List<ProcessRoleBean> listProcessRoleBean = new ArrayList<ProcessRoleBean>();
			for (int i = 0; i <= 16; i++) {
				ProcessRoleBean processRoleBean = new ProcessRoleBean();
				processRoleBean.setRoleCount(i);
				List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
				for (Object[] obj : listObj) {
					if (obj == null || obj[0] == null || obj[8] == null) {
						continue;
					}
					if (i != 16) {
						if (i == Integer.parseInt(obj[8].toString())) {
							ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
							if (processWebBean != null)
								listProcessWebBean.add(processWebBean);
						}
					} else {
						if (Integer.parseInt(obj[8].toString()) > 15) {
							ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
							if (processWebBean != null)
								listProcessWebBean.add(processWebBean);
						}
					}
				}
				processRoleBean.setAllCount(listProcessWebBean.size());
				processRoleBean.setProcessWebList(listProcessWebBean);
				listProcessRoleBean.add(processRoleBean);
			}
			return listProcessRoleBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessRoleBean> findProcessRole(long processId, long projectId) throws Exception {
		try {
			String sql = " select jfs.flow_id,(select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
					+ JecnCommonSql.getRoleString() + ") role_count";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				sql = sqlSQLSERVER + sql;
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
			}
			sql = sql + " where jfs.isflow=1 and jfs.del_state=0 and jfs.projectid=" + projectId
					+ " order by jfs.pre_flow_id,jfs.sort_id,jfs.flow_id";
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			List<ProcessRoleBean> listProcessRoleBean = new ArrayList<ProcessRoleBean>();
			for (int i = 0; i <= 16; i++) {
				ProcessRoleBean processRoleBean = new ProcessRoleBean();
				processRoleBean.setRoleCount(i);
				int count = 0;
				for (Object[] obj : listObj) {
					if (obj == null || obj[0] == null || obj[1] == null) {
						continue;
					}
					if (i != 16) {
						if (i == Integer.parseInt(obj[1].toString())) {
							count++;
						}
					} else {
						if (Integer.parseInt(obj[1].toString()) > 15) {
							count++;
						}
					}
				}
				processRoleBean.setAllCount(count);
				listProcessRoleBean.add(processRoleBean);
			}
			return listProcessRoleBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessWebBean> findProcessRoleDetails(long processId, int typeNumber, long projectId, int start,
			int limit) throws Exception {
		try {
			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {
				String sql = " select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,leadOrg.org_id,leadOrg.org_name,jfbi.type_res_people,jfbi.res_people_id,"
						+ "  case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name";
				String withSql = "";
				if (processId != -1) {
					withSql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

				sql = sql + ",jecn_flow_basic_info jfbi"
						+ "       left join (select jfo.org_id,jfo.org_name,jfro.flow_id from"
						+ "       jecn_flow_org jfo,jecn_flow_related_org jfro where jfro.org_id ="
						+ "       jfo.org_id and jfo.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id"
						+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
						+ "       left join (select jfoi.figure_id,jfoi.figure_text from"
						+ "       jecn_flow_org_image jfoi,jecn_flow_org org where jfoi.org_id=org.org_id"
						+ "       and org.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				sql = sql + " where  jfs.flow_id=jfbi.flow_id and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
						+ projectId;
				if (typeNumber < 16) {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getRoleString() + ")=" + typeNumber;
				} else {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getRoleString() + ")>15";
				}
				if (processId != -1) {
					listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit);
				} else {
					listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit);
				}
			} else if (JecnCommon.isOracle()) {
				String sql = " select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,leadOrg.org_id,leadOrg.org_name,jfbi.type_res_people,jfbi.res_people_id,"
						+ "  case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name";
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
				sql = sql + ",jecn_flow_basic_info jfbi"
						+ "       left join (select jfo.org_id,jfo.org_name,jfro.flow_id from"
						+ "       jecn_flow_org jfo,jecn_flow_related_org jfro where jfro.org_id ="
						+ "       jfo.org_id and jfo.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id"
						+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
						+ "       left join (select jfoi.figure_id,jfoi.figure_text from"
						+ "       jecn_flow_org_image jfoi,jecn_flow_org org where jfoi.org_id=org.org_id"
						+ "       and org.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				sql = sql + " where  jfs.flow_id=jfbi.flow_id and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
						+ projectId;
				if (typeNumber < 16) {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getRoleString() + ")=" + typeNumber;
				} else {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getRoleString() + ")>15";
				}
				sql = sql + " order by jfs.pre_flow_id,jfs.sort_id,jfs.flow_id";
				listObj = this.flowStructureDao.listNativeSql(sql, start, limit);
			}
			if (listObj == null) {
				return null;
			}
			List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
				if (processWebBean != null)
					listProcessWebBean.add(processWebBean);
			}
			return listProcessWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int findProcessRoleCount(long processId, int typeNumber, long projectId) throws Exception {
		try {
			String sql = " select count(jfs.flow_id)";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";

				}
				sql = sqlSQLSERVER + sql;
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
			}
			sql = sql + " where jfs.isflow=1 and jfs.del_state=0 and jfs.projectid=" + projectId;
			if (typeNumber < 16) {
				sql = sql
						+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
						+ JecnCommonSql.getRoleString() + ")=" + typeNumber;
			} else {
				sql = sql
						+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
						+ JecnCommonSql.getRoleString() + ")>15";
			}
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessActivitiesBean> findAllProcessActivities(long processId, long projectId) throws Exception {
		try {
			String sql = " select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,leadOrg.org_id,leadOrg.org_name,jfbi.type_res_people,jfbi.res_people_id,"
					+ "  case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
					+ "  ,(select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
					+ JecnCommonSql.getActiveString() + ") role_count";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				sql = sqlSQLSERVER + sql;

				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
			}
			sql = sql + ",jecn_flow_basic_info jfbi"
					+ "       left join (select jfo.org_id,jfo.org_name,jfro.flow_id from"
					+ "       jecn_flow_org jfo,jecn_flow_related_org jfro where jfro.org_id ="
					+ "       jfo.org_id and jfo.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id"
					+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
					+ "       left join (select jfoi.figure_id,jfoi.figure_text from"
					+ "       jecn_flow_org_image jfoi,jecn_flow_org org where jfoi.org_id=org.org_id"
					+ "       and org.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
			sql = sql + " where  jfs.flow_id=jfbi.flow_id and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
					+ projectId + " order by jfs.pre_flow_id,jfs.sort_id,jfs.flow_id";
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			List<ProcessActivitiesBean> listProcessActivitiesBean = new ArrayList<ProcessActivitiesBean>();
			for (int i = 0; i <= 21; i++) {
				ProcessActivitiesBean arocessActivitiesBean = new ProcessActivitiesBean();
				arocessActivitiesBean.setActivitiesCount(i);
				List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
				for (Object[] obj : listObj) {
					if (obj == null || obj[0] == null || obj[8] == null) {
						continue;
					}
					if (i != 21) {
						int minVaule = i * 5;
						int maxVaule = i * 5 + 5 - 1;
						if (Integer.parseInt(obj[8].toString()) >= minVaule
								&& Integer.parseInt(obj[8].toString()) <= maxVaule) {
							ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
							if (processWebBean != null)
								listProcessWebBean.add(processWebBean);
						}
					} else {
						if (Integer.parseInt(obj[8].toString()) > 100) {
							ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
							if (processWebBean != null)
								listProcessWebBean.add(processWebBean);
						}
					}
				}
				arocessActivitiesBean.setAllCount(listProcessWebBean.size());
				arocessActivitiesBean.setProcessWebList(listProcessWebBean);
				listProcessActivitiesBean.add(arocessActivitiesBean);
			}
			return listProcessActivitiesBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessActivitiesBean> findProcessActivities(long processId, long projectId) throws Exception {
		try {
			String sql = " select jfs.flow_id,(select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
					+ JecnCommonSql.getActiveString() + ") role_count";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				sql = sqlSQLSERVER + sql;
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
			}
			sql = sql + " where jfs.isflow=1 and jfs.del_state=0 and jfs.projectid=" + projectId
					+ " order by jfs.pre_flow_id,jfs.sort_id,jfs.flow_id";
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			List<ProcessActivitiesBean> listProcessActivitiesBean = new ArrayList<ProcessActivitiesBean>();
			for (int i = 0; i <= 20; i++) {
				ProcessActivitiesBean arocessActivitiesBean = new ProcessActivitiesBean();
				arocessActivitiesBean.setActivitiesCount(i);
				int count = 0;
				for (Object[] obj : listObj) {
					if (obj == null || obj[0] == null || obj[1] == null) {
						continue;
					}

					if (i != 20) {
						int minVaule = i * 5;
						int maxVaule = i * 5 + 4;
						if (Integer.parseInt(obj[1].toString()) >= minVaule
								&& Integer.parseInt(obj[1].toString()) <= maxVaule) {
							count++;
						}
					} else {
						if (Integer.parseInt(obj[1].toString()) >= 100) {
							count++;
						}
					}
				}
				arocessActivitiesBean.setAllCount(count);
				listProcessActivitiesBean.add(arocessActivitiesBean);
			}
			return listProcessActivitiesBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessWebBean> findProcessActivitiesDetails(long processId, int startNum, int endNum, long projectId,
			int start, int limit) throws Exception {
		try {
			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {
				String sql = " select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,leadOrg.org_id,leadOrg.org_name,jfbi.type_res_people,jfbi.res_people_id,"
						+ "  case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name";
				String withSql = "";
				if (processId != -1) {
					withSql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

				sql = sql + ",jecn_flow_basic_info jfbi"
						+ "       left join (select jfo.org_id,jfo.org_name,jfro.flow_id from"
						+ "       jecn_flow_org jfo,jecn_flow_related_org jfro where jfro.org_id ="
						+ "       jfo.org_id and jfo.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id"
						+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
						+ "       left join (select jfoi.figure_id,jfoi.figure_text from"
						+ "       jecn_flow_org_image jfoi,jecn_flow_org org where jfoi.org_id=org.org_id"
						+ "       and org.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				sql = sql + " where  jfs.flow_id=jfbi.flow_id and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
						+ projectId;
				if (startNum < endNum) {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getActiveString() + ")>=" + startNum;
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getActiveString() + ")<=" + endNum;
				} else if (startNum == endNum) {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getActiveString() + ")=" + startNum;
				} else {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getActiveString() + ")>" + 99;
				}
				if (processId != -1) {
					listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit);
				} else {
					listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit);
				}

			} else if (JecnCommon.isOracle()) {
				String sql = " select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,leadOrg.org_id,leadOrg.org_name,jfbi.type_res_people,jfbi.res_people_id,"
						+ "  case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name";
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
				sql = sql + ",jecn_flow_basic_info jfbi"
						+ "       left join (select jfo.org_id,jfo.org_name,jfro.flow_id from"
						+ "       jecn_flow_org jfo,jecn_flow_related_org jfro where jfro.org_id ="
						+ "       jfo.org_id and jfo.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id"
						+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
						+ "       left join (select jfoi.figure_id,jfoi.figure_text from"
						+ "       jecn_flow_org_image jfoi,jecn_flow_org org where jfoi.org_id=org.org_id"
						+ "       and org.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				sql = sql + " where  jfs.flow_id=jfbi.flow_id and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
						+ projectId;
				if (startNum < endNum) {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getActiveString() + ")>=" + startNum;
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getActiveString() + ")<=" + endNum;
				} else if (startNum == endNum) {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getActiveString() + ")=" + startNum;
				} else {
					sql = sql
							+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
							+ JecnCommonSql.getActiveString() + ")>" + 99;
				}
				sql = sql + " order by jfs.pre_flow_id,jfs.sort_id,jfs.flow_id";
				listObj = flowStructureDao.listNativeSql(sql, start, limit);
			}
			if (listObj == null) {
				return null;
			}
			List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
				if (processWebBean != null)
					listProcessWebBean.add(processWebBean);
			}
			return listProcessWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int findProcessActivitiesCount(long processId, int startNum, int endNum, long projectId) throws Exception {
		try {
			String sql = " select count(jfs.flow_id)";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				sql = sqlSQLSERVER + sql;
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
			}
			sql = sql + " where jfs.isflow=1 and jfs.del_state=0 and jfs.projectid=" + projectId;
			if (startNum < endNum) {
				sql = sql
						+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
						+ JecnCommonSql.getActiveString() + ")>=" + startNum;
				sql = sql
						+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
						+ JecnCommonSql.getActiveString() + ")<=" + endNum;
			} else if (startNum == endNum) {
				sql = sql
						+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
						+ JecnCommonSql.getActiveString() + ")=" + startNum;
			} else {
				sql = sql
						+ "  and (select count(jfsi.figure_id) from jecn_flow_structure_image jfsi where jfsi.flow_id=jfs.flow_id and jfsi.figure_type in "
						+ JecnCommonSql.getActiveString() + ")>" + 99;
			}
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public ProcessApplicationDetailsBean findProcessApplicationDetails(long processId) throws Exception {
		try {
			JecnFlowStructure jecnFlowStructure = (JecnFlowStructure) flowStructureDao.getSession().get(
					JecnFlowStructure.class, processId);
			if (jecnFlowStructure == null) {
				return null;
			}
			// 流程基本信息
			ProcessWebBean processWebBean = new ProcessWebBean();
			processWebBean.setFlowId(jecnFlowStructure.getFlowId());
			processWebBean.setFlowName(jecnFlowStructure.getFlowName());
			String sql = "WITH FLOWTMP AS (SELECT  A.FLOW_ID, G.PEOPLE_ID, E.ORG_ID,B.TYPE,B.FIGURE_POSITION_ID"
					+ " ,D.FIGURE_ID ORG_FIGURE_ID,"
					+ " A.FIGURE_ID"
					+ " FROM JECN_FLOW_STRUCTURE_IMAGE A"
					+ " INNER JOIN PROCESS_STATION_RELATED B ON A.FIGURE_ID = B.FIGURE_FLOW_ID"
					+ " INNER JOIN JECN_FLOW_STRUCTURE C ON A.FLOW_ID = C.FLOW_ID"
					+ " INNER JOIN JECN_FLOW_ORG_IMAGE D ON D.FIGURE_ID = B.FIGURE_POSITION_ID"
					+ " INNER JOIN JECN_FLOW_ORG E ON E.ORG_ID = D.ORG_ID"
					+ " INNER JOIN JECN_USER_POSITION_RELATED F ON F.FIGURE_ID = D.FIGURE_ID"
					+ " INNER JOIN JECN_USER G ON G.PEOPLE_ID = F.PEOPLE_ID"
					+ " WHERE G.ISLOCK = 0"
					+ " AND C.ISFLOW = 1"
					+ " AND C.DEL_STATE = 0"
					+ " AND A.FIGURE_TYPE IN"
					+ JecnCommonSql.getRoleString()
					+ ")"
					+ "  ,TMP AS ("
					+ "        (SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP WHERE FLOWTMP.TYPE = '0'"
					+ "             AND FLOWTMP.FIGURE_POSITION_ID = FLOWTMP.ORG_FIGURE_ID"
					+ "          UNION SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP ,JECN_POSITION_GROUP_R JPGR"
					+ "            WHERE  FLOWTMP.TYPE = '1'"
					+ "                   AND FLOWTMP.FIGURE_POSITION_ID = JPGR.GROUP_ID"
					+ "                   AND JPGR.FIGURE_ID = FLOWTMP.ORG_FIGURE_ID)" + "    )" + ",TMP2 AS("
					+ " SELECT TMP.FIGURE_ID,COUNT(*)AS USER_COUNT FROM TMP GROUP BY TMP.FIGURE_ID" + ")"
					+ "select distinct jfs.figure_id,jfs.figure_text,jfs.flow_id,TMP2.USER_COUNT"
					+ " from jecn_flow_structure_image jfs left join TMP on TMP.flow_id= jfs.flow_id "
					+ " left join TMP2 ON jfs.FIGURE_ID=TMP2.FIGURE_ID where jfs.figure_type in "
					+ JecnCommonSql.getRoleString() + " and jfs.flow_id =" + processId;
			List<Object[]> listRoleObj = this.flowStructureDao.listNativeSql(sql);
			ProcessApplicationDetailsBean processApplicationDetailsBean = new ProcessApplicationDetailsBean();
			List<ProcessRoleWebBean> listProcessRoleWebBean = new ArrayList<ProcessRoleWebBean>();
			for (Object[] obj : listRoleObj) {
				if (obj == null || obj[0] == null || obj[2] == null) {
					continue;
				}

				ProcessRoleWebBean processRoleWebBean = new ProcessRoleWebBean();
				processRoleWebBean.setRoleId(Long.valueOf(obj[0].toString()));
				if (obj[1] != null) {
					processRoleWebBean.setRoleName(obj[1].toString());
				}
				if (obj[3] != null) {
					processRoleWebBean.setTotalUser(Integer.parseInt(obj[3].toString()));
				} else {
					processRoleWebBean.setTotalUser(0);
				}

				listProcessRoleWebBean.add(processRoleWebBean);
			}
			processApplicationDetailsBean.setListProcessRoleWebBean(listProcessRoleWebBean);
			processApplicationDetailsBean.setProcessWebBean(processWebBean);
			return processApplicationDetailsBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessApplicationBean> findProcessApplication(long processId, long resOrgId, long projectId,
			int start, int limit) throws Exception {
		try {
			String sql = "";
			String withSql = "WITH FLOWTMP AS (SELECT  A.FLOW_ID, G.PEOPLE_ID, E.ORG_ID,B.TYPE,B.FIGURE_POSITION_ID"
					+ " ,D.FIGURE_ID ORG_FIGURE_ID," + " A.FIGURE_ID" + " FROM JECN_FLOW_STRUCTURE_IMAGE A"
					+ " INNER JOIN PROCESS_STATION_RELATED B ON A.FIGURE_ID = B.FIGURE_FLOW_ID"
					+ " INNER JOIN JECN_FLOW_STRUCTURE C ON A.FLOW_ID = C.FLOW_ID"
					+ " INNER JOIN JECN_FLOW_ORG_IMAGE D ON D.FIGURE_ID = B.FIGURE_POSITION_ID"
					+ " INNER JOIN JECN_FLOW_ORG E ON E.ORG_ID = D.ORG_ID"
					+ " INNER JOIN JECN_USER_POSITION_RELATED F ON F.FIGURE_ID = D.FIGURE_ID"
					+ " INNER JOIN JECN_USER G ON G.PEOPLE_ID = F.PEOPLE_ID" + " WHERE G.ISLOCK = 0"
					+ " AND C.ISFLOW = 1" + " AND C.DEL_STATE = 0" + " AND A.FIGURE_TYPE IN"
					+ JecnCommonSql.getRoleString()
					+ ")"
					+ "  ,TMP AS "
					+ "        (SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP WHERE FLOWTMP.TYPE = '0'"
					+ "             AND FLOWTMP.FIGURE_POSITION_ID = FLOWTMP.ORG_FIGURE_ID"
					+ "          UNION SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP ,JECN_POSITION_GROUP_R JPGR"
					+ "            WHERE  FLOWTMP.TYPE = '1'"
					+ "                   AND FLOWTMP.FIGURE_POSITION_ID = JPGR.GROUP_ID"
					+ "                   AND JPGR.FIGURE_ID = FLOWTMP.ORG_FIGURE_ID)"
					+ ",TMP2 AS("
					+ "SELECT TMP.FLOW_ID,COUNT(*)AS USER_COUNT FROM TMP GROUP BY FLOW_ID"
					+ ")"
					+ ",TMP3 AS ("
					+ "SELECT T2.FLOW_ID, COUNT(T2.FLOW_ID) AS RULE_COUNT"
					+ "  FROM JECN_FLOW_STRUCTURE T2, JECN_FLOW_STRUCTURE_IMAGE FSIMAGE"
					+ " WHERE T2.FLOW_ID = FSIMAGE.FLOW_ID"
					+ "   AND T2.ISFLOW = 1"
					+ "   AND T2.DEL_STATE = 0"
					+ "   AND T2.PROJECTID = "
					+ projectId
					+ "   AND FSIMAGE.FIGURE_TYPE IN    "
					+ JecnCommonSql.getRoleString() + " GROUP BY (T2.FLOW_ID)" + ")";

			if (JecnCommon.isSQLServer()) {
				if (resOrgId != -1) {
					withSql += ",TMP6 AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID =" + resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM TMP6 INNER JOIN JECN_FLOW_ORG JFS ON TMP6.ORG_ID=JFS.PER_ORG_ID)";
					withSql += ",TMP4 AS(SELECT JFRO_T.FLOW_ID"
							+ " FROM JECN_FLOW_RELATED_ORG JFRO_T INNER JOIN TMP6 ON TMP6.ORG_ID = JFRO_T.ORG_ID" + ")";
				}
				if (processId != -1) {
					withSql += ",TMP5 AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM TMP5 INNER JOIN JECN_FLOW_STRUCTURE JFS ON TMP5.FLOW_ID=JFS.PRE_FLOW_ID)";
				}

			} else if (JecnCommon.isOracle()) {
				if (resOrgId != -1) {
					withSql += ",TMP4 AS(" + "   SELECT JFRO_T.FLOW_ID" + "   FROM (SELECT JFO.*"
							+ "          FROM JECN_FLOW_ORG JFO" + "         WHERE JFO.DEL_STATE = 0"
							+ "        CONNECT BY PRIOR JFO.ORG_ID = JFO.PER_ORG_ID"
							+ "         START WITH JFO.ORG_ID = " + resOrgId + ") ORG,"
							+ "       JECN_FLOW_RELATED_ORG JFRO_T" + "   WHERE ORG.ORG_ID = JFRO_T.ORG_ID" + ")";
				}
				if (processId != -1) {
					withSql += ",TMP5 AS("
							+ "  SELECT JFS.FLOW_ID FROM JECN_FLOW_STRUCTURE JFS CONNECT BY PRIOR JFS.FLOW_ID=JFS.PRE_FLOW_ID START WITH JFS.FLOW_ID= "
							+ processId + ")" + "";
				}
			}
			sql = " SELECT T3.FLOW_ID,T3.FLOW_NAME,TMP3.RULE_COUNT,TMP2.USER_COUNT" + "  FROM JECN_FLOW_STRUCTURE T3"
					+ "  LEFT JOIN TMP3 ON T3.FLOW_ID=TMP3.FLOW_ID" + "  LEFT JOIN TMP2 ON T3.FLOW_ID=TMP2.FLOW_ID";
			if (resOrgId != -1) {
				sql += "  LEFT JOIN TMP4 ON T3.FLOW_ID=TMP4.FLOW_ID";
			}
			if (processId != -1) {
				sql += "  LEFT JOIN TMP5 ON T3.FLOW_ID=TMP5.FLOW_ID";
			}
			sql += " WHERE T3.ISFLOW = 1" + "   AND T3.DEL_STATE = 0" + "   AND T3.PROJECTID = " + projectId;
			if (processId != -1) {
				sql += " AND t3.flow_id = TMP5.flow_id";
			}
			if (resOrgId != -1) {
				sql += " AND t3.flow_id = TMP4.flow_id";
			}

			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {
				listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit);
			} else if (JecnCommon.isOracle()) {
				sql += " order by t3.flow_id";
				sql = withSql + sql;
				listObj = this.flowStructureDao.listNativeSql(sql, start, limit);
			}
			List<ProcessApplicationBean> listResult = new ArrayList<ProcessApplicationBean>();
			if (listObj == null) {
				return listResult;
			}
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessApplicationBean processApplicationBean = new ProcessApplicationBean();
				processApplicationBean.setProcessId(Long.valueOf(obj[0].toString()));
				if (obj[1] != null) {
					processApplicationBean.setProcessName(obj[1].toString());
				}
				// 角色数
				if (obj[2] != null) {
					processApplicationBean.setRoleNum(Integer.parseInt(obj[2].toString()));
				}
				// 总人数
				if (obj[3] != null) {
					processApplicationBean.setPeNum(Integer.parseInt(obj[3].toString()));
				}
				listResult.add(processApplicationBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessApplicationDetailsBean> findProcessApplicationDetails(long processId, long resOrgId,
			long projectId) throws Exception {
		try {
			String sql = "WITH FLOWTMP AS (SELECT  A.FLOW_ID, G.PEOPLE_ID, E.ORG_ID,B.TYPE,B.FIGURE_POSITION_ID"
					+ " ,D.FIGURE_ID ORG_FIGURE_ID," + " A.FIGURE_ID" + " FROM JECN_FLOW_STRUCTURE_IMAGE A"
					+ " INNER JOIN PROCESS_STATION_RELATED B ON A.FIGURE_ID = B.FIGURE_FLOW_ID"
					+ " INNER JOIN JECN_FLOW_STRUCTURE C ON A.FLOW_ID = C.FLOW_ID"
					+ " INNER JOIN JECN_FLOW_ORG_IMAGE D ON D.FIGURE_ID = B.FIGURE_POSITION_ID"
					+ " INNER JOIN JECN_FLOW_ORG E ON E.ORG_ID = D.ORG_ID"
					+ " INNER JOIN JECN_USER_POSITION_RELATED F ON F.FIGURE_ID = D.FIGURE_ID"
					+ " INNER JOIN JECN_USER G ON G.PEOPLE_ID = F.PEOPLE_ID" + " WHERE G.ISLOCK = 0"
					+ " AND C.ISFLOW = 1" + " AND C.DEL_STATE = 0" + " AND A.FIGURE_TYPE IN"
					+ JecnCommonSql.getRoleString()
					+ ")"
					+ "  ,TMP AS ("
					+ "        (SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP WHERE FLOWTMP.TYPE = '0'"
					+ "             AND FLOWTMP.FIGURE_POSITION_ID = FLOWTMP.ORG_FIGURE_ID"
					+ "          UNION SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP ,JECN_POSITION_GROUP_R JPGR"
					+ "            WHERE  FLOWTMP.TYPE = '1'"
					+ "                   AND FLOWTMP.FIGURE_POSITION_ID = JPGR.GROUP_ID"
					+ "                   AND JPGR.FIGURE_ID = FLOWTMP.ORG_FIGURE_ID)"
					+ "    )"
					+ ",TMP2 AS("
					+ "SELECT TMP.FLOW_ID,COUNT(*)AS USER_COUNT FROM TMP GROUP BY FLOW_ID"
					+ ")"
					+ ",TMP3 AS ("
					+ "SELECT T2.FLOW_ID, COUNT(T2.FLOW_ID) AS RULE_COUNT"
					+ "  FROM JECN_FLOW_STRUCTURE T2, JECN_FLOW_STRUCTURE_IMAGE FSIMAGE"
					+ " WHERE T2.FLOW_ID = FSIMAGE.FLOW_ID"
					+ "   AND T2.ISFLOW = 1"
					+ "   AND T2.DEL_STATE = 0"
					+ "   AND T2.PROJECTID = "
					+ projectId
					+ "   AND FSIMAGE.FIGURE_TYPE IN    "
					+ JecnCommonSql.getRoleString() + " GROUP BY (T2.FLOW_ID)" + ")";

			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				if (resOrgId != -1) {
					sql += ",TMP6 AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID =" + resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM TMP6 INNER JOIN JECN_FLOW_ORG JFS ON TMP6.ORG_ID=JFS.PER_ORG_ID)";
					sql += ",TMP4 AS(SELECT JFRO_T.FLOW_ID"
							+ " FROM JECN_FLOW_RELATED_ORG JFRO_T INNER JOIN TMP6 ON TMP6.ORG_ID = JFRO_T.ORG_ID" + ")";
				}
				if (processId != -1) {
					sql += ",TMP5 AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM TMP5 INNER JOIN JECN_FLOW_STRUCTURE JFS ON TMP5.FLOW_ID=JFS.PRE_FLOW_ID)";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (resOrgId != -1) {
					sql += ",TMP4 AS(" + "   SELECT JFRO_T.FLOW_ID" + "   FROM (SELECT JFO.*"
							+ "          FROM JECN_FLOW_ORG JFO" + "         WHERE JFO.DEL_STATE = 0"
							+ "        CONNECT BY PRIOR JFO.ORG_ID = JFO.PER_ORG_ID"
							+ "         START WITH JFO.ORG_ID = " + resOrgId + ") ORG,"
							+ "       JECN_FLOW_RELATED_ORG JFRO_T" + "   WHERE ORG.ORG_ID = JFRO_T.ORG_ID" + ")";
				}
				if (processId != -1) {
					sql += ",TMP5 AS("
							+ "  SELECT JFS.FLOW_ID FROM JECN_FLOW_STRUCTURE JFS CONNECT BY PRIOR JFS.FLOW_ID=JFS.PRE_FLOW_ID START WITH JFS.FLOW_ID= "
							+ processId + ")" + "";
				}
			}
			sql += " SELECT T3.FLOW_ID,T3.FLOW_NAME" + "  FROM JECN_FLOW_STRUCTURE T3"
					+ "  LEFT JOIN TMP3 ON T3.FLOW_ID=TMP3.FLOW_ID" + "  LEFT JOIN TMP2 ON T3.FLOW_ID=TMP2.FLOW_ID";
			if (resOrgId != -1) {
				sql += "  LEFT JOIN TMP4 ON T3.FLOW_ID=TMP4.FLOW_ID";
			}
			if (processId != -1) {
				sql += "  LEFT JOIN TMP5 ON T3.FLOW_ID=TMP5.FLOW_ID";
			}
			sql += " WHERE T3.ISFLOW = 1" + "   AND T3.DEL_STATE = 0" + "   AND T3.PROJECTID = " + projectId;
			if (processId != -1) {
				sql += " AND t3.flow_id = TMP5.flow_id";
			}
			if (resOrgId != -1) {
				sql += " AND t3.flow_id = TMP4.flow_id";
			}
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			Set<Long> setIds = new HashSet<Long>();
			List<ProcessApplicationDetailsBean> listResult = new ArrayList<ProcessApplicationDetailsBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessApplicationDetailsBean processApplicationDetailsBean = new ProcessApplicationDetailsBean();
				ProcessWebBean processWebBean = new ProcessWebBean();
				processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
				if (obj[1] != null) {
					processWebBean.setFlowName(obj[1].toString());
				}
				processApplicationDetailsBean.setProcessWebBean(processWebBean);
				listResult.add(processApplicationDetailsBean);
				setIds.add(Long.valueOf(obj[0].toString()));
			}
			if (setIds.size() == 0) {
				return new ArrayList<ProcessApplicationDetailsBean>();
			}
			List<Object[]> listRoleObj = new ArrayList<Object[]>();

			sql = "WITH FLOWTMP AS (SELECT  A.FLOW_ID, G.PEOPLE_ID, E.ORG_ID,B.TYPE,B.FIGURE_POSITION_ID"
					+ " ,D.FIGURE_ID ORG_FIGURE_ID,"
					+ " A.FIGURE_ID"
					+ " FROM JECN_FLOW_STRUCTURE_IMAGE A"
					+ " INNER JOIN PROCESS_STATION_RELATED B ON A.FIGURE_ID = B.FIGURE_FLOW_ID"
					+ " INNER JOIN JECN_FLOW_STRUCTURE C ON A.FLOW_ID = C.FLOW_ID"
					+ " INNER JOIN JECN_FLOW_ORG_IMAGE D ON D.FIGURE_ID = B.FIGURE_POSITION_ID"
					+ " INNER JOIN JECN_FLOW_ORG E ON E.ORG_ID = D.ORG_ID"
					+ " INNER JOIN JECN_USER_POSITION_RELATED F ON F.FIGURE_ID = D.FIGURE_ID"
					+ " INNER JOIN JECN_USER G ON G.PEOPLE_ID = F.PEOPLE_ID"
					+ " WHERE G.ISLOCK = 0"
					+ " AND C.ISFLOW = 1"
					+ " AND C.DEL_STATE = 0"
					+ " AND A.FIGURE_TYPE IN"
					+ JecnCommonSql.getRoleString()
					+ ")"
					+ "  ,TMP AS ("
					+ "        (SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP WHERE FLOWTMP.TYPE = '0'"
					+ "             AND FLOWTMP.FIGURE_POSITION_ID = FLOWTMP.ORG_FIGURE_ID"
					+ "          UNION SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP ,JECN_POSITION_GROUP_R JPGR"
					+ "            WHERE  FLOWTMP.TYPE = '1'"
					+ "                   AND FLOWTMP.FIGURE_POSITION_ID = JPGR.GROUP_ID"
					+ "                   AND JPGR.FIGURE_ID = FLOWTMP.ORG_FIGURE_ID)" + "    )" + ",TMP2 AS("
					+ " SELECT TMP.FIGURE_ID,COUNT(*)AS USER_COUNT FROM TMP GROUP BY TMP.FIGURE_ID" + ")"
					+ "select distinct jfs.figure_id,jfs.figure_text,jfs.flow_id,TMP2.USER_COUNT"
					+ " from jecn_flow_structure_image jfs left join TMP on TMP.flow_id= jfs.flow_id "
					+ " left join TMP2 ON jfs.FIGURE_ID=TMP2.FIGURE_ID where jfs.figure_type in "
					+ JecnCommonSql.getRoleString() + " and jfs.flow_id in";
			List<String> listStr = JecnCommonSql.getSetSqls(sql, setIds);
			for (String str : listStr) {
				List<Object[]> list = this.flowStructureDao.listNativeSql(str);
				for (Object[] obj : list) {
					listRoleObj.add(obj);
				}
			}
			for (ProcessApplicationDetailsBean processApplicationDetailsBean : listResult) {
				List<ProcessRoleWebBean> listProcessRoleWebBean = new ArrayList<ProcessRoleWebBean>();
				Long flowId = processApplicationDetailsBean.getProcessWebBean().getFlowId();
				for (Object[] obj : listRoleObj) {
					if (obj == null || obj[0] == null || obj[2] == null) {
						continue;
					}

					if (flowId.toString().equals(obj[2].toString())) {
						ProcessRoleWebBean processRoleWebBean = new ProcessRoleWebBean();
						processRoleWebBean.setRoleId(Long.valueOf(obj[0].toString()));
						if (obj[1] != null) {
							processRoleWebBean.setRoleName(obj[1].toString());
						}
						if (obj[3] != null) {
							processRoleWebBean.setTotalUser(Integer.parseInt(obj[3].toString()));
						}
						listProcessRoleWebBean.add(processRoleWebBean);
					}
				}
				processApplicationDetailsBean.setListProcessRoleWebBean(listProcessRoleWebBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int findProcessApplicationCount(long processId, long resOrgId, long projectId) throws Exception {
		try {
			String sql = "WITH FLOWTMP AS (SELECT  A.FLOW_ID, G.PEOPLE_ID, E.ORG_ID,B.TYPE,B.FIGURE_POSITION_ID"
					+ " ,D.FIGURE_ID ORG_FIGURE_ID," + " A.FIGURE_ID" + " FROM JECN_FLOW_STRUCTURE_IMAGE A"
					+ " INNER JOIN PROCESS_STATION_RELATED B ON A.FIGURE_ID = B.FIGURE_FLOW_ID"
					+ " INNER JOIN JECN_FLOW_STRUCTURE C ON A.FLOW_ID = C.FLOW_ID"
					+ " INNER JOIN JECN_FLOW_ORG_IMAGE D ON D.FIGURE_ID = B.FIGURE_POSITION_ID"
					+ " INNER JOIN JECN_FLOW_ORG E ON E.ORG_ID = D.ORG_ID"
					+ " INNER JOIN JECN_USER_POSITION_RELATED F ON F.FIGURE_ID = D.FIGURE_ID"
					+ " INNER JOIN JECN_USER G ON G.PEOPLE_ID = F.PEOPLE_ID" + " WHERE G.ISLOCK = 0"
					+ " AND C.ISFLOW = 1" + " AND C.DEL_STATE = 0" + " AND A.FIGURE_TYPE IN"
					+ JecnCommonSql.getRoleString()
					+ ")"
					+ "  ,TMP AS ("
					+ "        (SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP WHERE FLOWTMP.TYPE = '0'"
					+ "             AND FLOWTMP.FIGURE_POSITION_ID = FLOWTMP.ORG_FIGURE_ID"
					+ "          UNION SELECT FLOWTMP.FLOW_ID,FLOWTMP.PEOPLE_ID,FLOWTMP.ORG_ID,FLOWTMP.FIGURE_ID FROM FLOWTMP ,JECN_POSITION_GROUP_R JPGR"
					+ "            WHERE  FLOWTMP.TYPE = '1'"
					+ "                   AND FLOWTMP.FIGURE_POSITION_ID = JPGR.GROUP_ID"
					+ "                   AND JPGR.FIGURE_ID = FLOWTMP.ORG_FIGURE_ID)"
					+ "    )"
					+ ",TMP2 AS("
					+ "SELECT TMP.FLOW_ID,COUNT(*)AS USER_COUNT FROM TMP GROUP BY FLOW_ID"
					+ ")"
					+ ",TMP3 AS ("
					+ "SELECT T2.FLOW_ID, COUNT(T2.FLOW_ID) AS RULE_COUNT"
					+ "  FROM JECN_FLOW_STRUCTURE T2, JECN_FLOW_STRUCTURE_IMAGE FSIMAGE"
					+ " WHERE T2.FLOW_ID = FSIMAGE.FLOW_ID"
					+ "   AND T2.ISFLOW = 1"
					+ "   AND T2.DEL_STATE = 0"
					+ "   AND T2.PROJECTID = "
					+ projectId
					+ "   AND FSIMAGE.FIGURE_TYPE IN"
					+ JecnCommonSql.getRoleString() + " GROUP BY (T2.FLOW_ID)" + ")";

			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				if (resOrgId != -1) {
					sql += ",TMP6 AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID =" + resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM TMP6 INNER JOIN JECN_FLOW_ORG JFS ON TMP6.ORG_ID=JFS.PER_ORG_ID)";
					sql += ",TMP4 AS(SELECT JFRO_T.FLOW_ID"
							+ " FROM JECN_FLOW_RELATED_ORG JFRO_T INNER JOIN TMP6 ON TMP6.ORG_ID = JFRO_T.ORG_ID" + ")";
				}
				if (processId != -1) {
					sql += ",TMP5 AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM TMP5 INNER JOIN JECN_FLOW_STRUCTURE JFS ON TMP5.FLOW_ID=JFS.PRE_FLOW_ID)";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (resOrgId != -1) {
					sql += ",TMP4 AS(" + "   SELECT JFRO_T.FLOW_ID" + "   FROM (SELECT JFO.*"
							+ "          FROM JECN_FLOW_ORG JFO" + "         WHERE JFO.DEL_STATE = 0"
							+ "        CONNECT BY PRIOR JFO.ORG_ID = JFO.PER_ORG_ID"
							+ "         START WITH JFO.ORG_ID = " + resOrgId + ") ORG,"
							+ "       JECN_FLOW_RELATED_ORG JFRO_T" + "   WHERE ORG.ORG_ID = JFRO_T.ORG_ID" + ")";
				}
				if (processId != -1) {
					sql += ",TMP5 AS("
							+ "  SELECT JFS.FLOW_ID FROM JECN_FLOW_STRUCTURE JFS CONNECT BY PRIOR JFS.FLOW_ID=JFS.PRE_FLOW_ID START WITH JFS.FLOW_ID= "
							+ processId + ")" + "";
				}
			}
			sql += " SELECT COUNT(*)" + "  FROM JECN_FLOW_STRUCTURE T3" + "  LEFT JOIN TMP3 ON T3.FLOW_ID=TMP3.FLOW_ID"
					+ "  LEFT JOIN TMP2 ON T3.FLOW_ID=TMP2.FLOW_ID";
			if (resOrgId != -1) {
				sql += "  LEFT JOIN TMP4 ON T3.FLOW_ID=TMP4.FLOW_ID";
			}
			if (processId != -1) {
				sql += "  LEFT JOIN TMP5 ON T3.FLOW_ID=TMP5.FLOW_ID";
			}
			sql += " WHERE T3.ISFLOW = 1" + "   AND T3.DEL_STATE = 0" + "   AND T3.PROJECTID = " + projectId;
			if (processId != -1) {
				sql += " AND t3.flow_id = TMP5.flow_id";
			}
			if (resOrgId != -1) {
				sql += " AND t3.flow_id = TMP4.flow_id";
			}
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 获得输入输出的sql
	 * 
	 * @param processId
	 * @param resOrgId
	 * @param projectId
	 * @return
	 */
	private String getSqlProcessInputOutputBean(long processId, long resOrgId, long projectId) {
		String sql = "	select t.flow_id,t.flow_name from jecn_flow_basic_info jfbi";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			String sqlSQLSERVER = "";
			if (processId != -1) {
				sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
						+ processId
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
			}
			if (resOrgId != -1) {
				if (processId != -1) {
					sqlSQLSERVER = sqlSQLSERVER + ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				} else {
					sqlSQLSERVER = sqlSQLSERVER + "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				}
			}
			sql = sqlSQLSERVER + sql;

			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
						+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
			}
			if (processId != -1) {
				sql = sql + ",MY_FLOW t";
			} else {
				sql = sql + ",JECN_FLOW_STRUCTURE t";
			}

		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
						+ resOrgId + ") org,jecn_flow_related_org jfro_t"
						+ "       where org.org_id=jfro_t.org_id) leadOrg";
			}
			if (processId != -1) {
				sql = sql
						+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
						+ processId + ") t";
			} else {
				sql = sql + ",jecn_flow_structure t";
			}
		}
		sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
		if (resOrgId != -1) {
			sql = sql + "  and t.flow_id = leadOrg.flow_id";
		}
		sql = sql + "  order by t.pre_flow_id,t.sort_id,t.flow_id";
		return sql;
	}

	@Override
	public List<ProcessInputOutputBean> findProcessInputOutput(long processId, long resOrgId, long projectId,
			int start, int limit) throws Exception {
		try {
			String sql = "";
			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {

				sql = "	select t.flow_id,t.flow_name from jecn_flow_basic_info jfbi";
				String withSql = "";
				if (processId != -1) {
					withSql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (resOrgId != -1) {
					if (processId != -1) {
						withSql = withSql
								+ ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					} else {
						withSql = withSql
								+ "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
				}
				if (processId != -1) {
					sql = sql + ",MY_FLOW t";
				} else {
					sql = sql + ",JECN_FLOW_STRUCTURE t";
				}

				sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				if (processId != -1 || resOrgId != -1) {
					listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit);
				} else {
					listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit);
				}

			} else if (JecnCommon.isOracle()) {

				sql = "	select t.flow_id,t.flow_name from jecn_flow_basic_info jfbi";
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ resOrgId + ") org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id) leadOrg";
				}
				if (processId != -1) {
					sql = sql
							+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
							+ processId + ") t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}
				sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				sql = sql + "  order by t.pre_flow_id,t.sort_id,t.flow_id";
				listObj = this.flowStructureDao.listNativeSql(sql, start, limit);

			}

			Set<Long> setFlowIds = new HashSet<Long>();
			for (Object[] obj : listObj) {
				if (obj != null && obj[0] != null)
					setFlowIds.add(Long.valueOf(obj[0].toString()));
			}
			if (setFlowIds.size() == 0) {
				return new ArrayList<ProcessInputOutputBean>();
			}
			// 获得流程下的上下游流程

			sql = "select flow_jfs.flow_id,flow_jfs.flow_name,flow_jfsi.linecolor,flow_jfsi.flow_id as common_flow_id"
					+ "            from jecn_flow_structure_image flow_jfsi,jecn_flow_structure flow_jfs"
					+ "            where flow_jfsi.linecolor in ('1','2') and flow_jfsi.figure_type in "
					+ JecnCommonSql.getImplString()
					+ " and flow_jfsi.link_flow_id=flow_jfs.flow_id and flow_jfs.del_state=0"
					+ "            and flow_jfsi.flow_id in";
			List<String> listSql = JecnCommonSql.getSetSqls(sql, setFlowIds);
			List<Object[]> listUpperLower = new ArrayList<Object[]>();
			for (String str : listSql) {
				List<Object[]> list = this.flowStructureDao.listNativeSql(str);
				for (Object[] obj : list) {
					listUpperLower.add(obj);
				}
			}
			List<ProcessInputOutputBean> listResult = new ArrayList<ProcessInputOutputBean>();

			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				ProcessInputOutputBean processInputOutputBean = new ProcessInputOutputBean();
				// 流程ID
				processInputOutputBean.setProcessId(Long.valueOf(obj[0].toString()));
				// 流程名称
				if (obj[1] != null)
					processInputOutputBean.setProcessName(obj[1].toString());
				List<ProcessWebBean> listProcessWebBeanUpper = new ArrayList<ProcessWebBean>();
				List<ProcessWebBean> listProcessWebBeanLower = new ArrayList<ProcessWebBean>();
				this.getUpperLowerFlowByFlowId(processInputOutputBean.getProcessId(), listProcessWebBeanUpper,
						listProcessWebBeanLower, listUpperLower);
				processInputOutputBean.setUpstreamProcess(listProcessWebBeanUpper);
				processInputOutputBean.setUpstreamProcessCount(listProcessWebBeanUpper.size());
				processInputOutputBean.setDownstreamProcess(listProcessWebBeanLower);
				processInputOutputBean.setDownstreamProcessCount(listProcessWebBeanLower.size());
				listResult.add(processInputOutputBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int findProcessInputOutputCount(long processId, long resOrgId, long projectId) throws Exception {
		try {
			String sql = "	select count (t.flow_id) from jecn_flow_basic_info jfbi";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (resOrgId != -1) {
					if (processId != -1) {
						sqlSQLSERVER = sqlSQLSERVER
								+ ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					} else {
						sqlSQLSERVER = sqlSQLSERVER
								+ "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				sql = sqlSQLSERVER + sql;
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
				}
				if (processId != -1) {
					sql = sql + ",MY_FLOW t";
				} else {
					sql = sql + ",JECN_FLOW_STRUCTURE t";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ resOrgId + ") org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id) leadOrg";
				}
				if (processId != -1) {
					sql = sql
							+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
							+ processId + ") t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}
			}
			sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
			if (resOrgId != -1) {
				sql = sql + "  and t.flow_id = leadOrg.flow_id";
			}
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessAnalysisBean> findAllProcessAnalysisExcel(long processId, long timeType, long processType,
			long posId, long projectId) throws Exception {
		try {
			String sql = this.getSqlProcessAnalysis(processId, timeType, processType, posId, projectId);
			List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
			List<ProcessAnalysisBean> listProcessAnalysisBean = new ArrayList<ProcessAnalysisBean>();
			for (int i = 1; i <= 5; i++) {
				ProcessAnalysisBean processAnalysisBean = new ProcessAnalysisBean();
				processAnalysisBean.setTimeType(String.valueOf(i));
				List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
				for (Object[] obj : listObj) {
					if (obj == null || obj[0] == null || obj[8] == null) {
						continue;
					}
					if (Integer.parseInt(obj[8].toString()) == i) {
						ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
						if (processWebBean != null)
							listProcessWebBean.add(processWebBean);
					}
				}
				processAnalysisBean.setCountTotal(listProcessWebBean.size());
				processAnalysisBean.setListProcessWebBean(listProcessWebBean);
				listProcessAnalysisBean.add(processAnalysisBean);
			}
			return listProcessAnalysisBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int findProcessAnalysisCount(long processId, long timeType, long processType, long posId, long projectId)
			throws Exception {
		try {
			String sql = " select count(jfs.flow_id)";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				sql = sqlSQLSERVER + sql;
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}
			}
			sql = sql + ",jecn_flow_driver jfd";
			if (processType != -1) {
				sql = sql + ",jecn_flow_type jft";
			}
			sql = sql + ",jecn_flow_basic_info jfbi";

			sql = sql
					+ " where  jfs.flow_id=jfbi.flow_id and jfbi.driver_type=1 and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
					+ projectId;
			sql = sql + " and jfbi.flow_id = jfd.flow_id ";
			if (timeType != -1) {
				sql = sql + " and jfd.quantity='" + timeType + "'";
			} else {
				sql = sql + " and jfd.quantity in ('1','2','3','4','5')";
			}
			if (processType != -1) {
				sql = sql + " and jfbi.type_id=jft.type_id  and jft.type_id=" + processType;
			}
			if (posId != -1) {
				sql = sql
						+ "(jfs.flow_id in"
						+ "(select jfs_process.flow_id"
						+ "  from process_station_related psr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
						+ "  where psr.type='0' and psr.figure_position_id="
						+ posId
						+ " and psr.figure_flow_id=jfsi.figure_id"
						+ "  and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0)"
						+ "  or"
						+ "  jfs.flow_id in"
						+ "  (select jfs_process.flow_id"
						+ "         from process_station_related psr,jecn_position_group_r jpgr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
						+ "         where psr.type='1' and   psr.figure_position_id=jpgr.group_id and jpgr.figure_id="
						+ posId + " and psr.figure_flow_id=jfsi.figure_id"
						+ "         and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0))";
			}

			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 流程分析报表SQl
	 * 
	 * @param processId
	 * @param timeType
	 * @param processType
	 * @param posId
	 * @param projectId
	 * @return
	 */
	private String getSqlProcessAnalysis(long processId, long timeType, long processType, long posId, long projectId) {
		String sql = " select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,leadOrg.org_id,leadOrg.org_name,jfbi.type_res_people,jfbi.res_people_id,"
				+ "  case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
				+ "  ,jfd.quantity,jft.type_name";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			String sqlSQLSERVER = "";
			if (processId != -1) {
				sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
						+ processId
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
			}
			sql = sqlSQLSERVER + sql;
			if (processId != -1) {
				sql = sql + " from MY_FLOW jfs";
			} else {
				sql = sql + " from jecn_flow_structure jfs";
			}

		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (processId != -1) {
				sql = sql
						+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
						+ processId + ") jfs";
			} else {
				sql = sql + " from jecn_flow_structure jfs";
			}
		}

		sql = sql + ",jecn_flow_driver jfd";
		if (processType != -1) {
			sql = sql + ",jecn_flow_type jft";
		}
		sql = sql + ",jecn_flow_basic_info jfbi" + "       left join (select jfo.org_id,jfo.org_name,jfro.flow_id from"
				+ "       jecn_flow_org jfo,jecn_flow_related_org jfro where jfro.org_id ="
				+ "       jfo.org_id and jfo.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id"
				+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
				+ "       left join (select jfoi.figure_id,jfoi.figure_text from"
				+ "       jecn_flow_org_image jfoi,jecn_flow_org org where jfoi.org_id=org.org_id"
				+ "       and org.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
		if (processType == -1) {
			sql = sql + " left join jecn_flow_type jft on jft.type_id=jfbi.type_id";
		}
		sql = sql
				+ " where  jfs.flow_id=jfbi.flow_id and jfbi.driver_type=1 and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
				+ projectId;
		sql = sql + " and jfbi.flow_id = jfd.flow_id ";
		if (timeType != -1) {
			sql = sql + " and jfd.quantity='" + timeType + "'";
		} else {
			sql = sql + " and jfd.quantity in ('1','2','3','4','5')";
		}
		if (processType != -1) {
			sql = sql + " and jfbi.type_id=jft.type_id and jft.type_id=" + processType;
		}
		if (posId != -1) {
			sql = sql
					+ "(jfs.flow_id in"
					+ "(select jfs_process.flow_id"
					+ "  from process_station_related psr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
					+ "  where psr.type='0' and psr.figure_position_id="
					+ posId
					+ " and psr.figure_flow_id=jfsi.figure_id"
					+ "  and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0)"
					+ "  or"
					+ "  jfs.flow_id in"
					+ "  (select jfs_process.flow_id"
					+ "         from process_station_related psr,jecn_position_group_r jpgr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
					+ "         where psr.type='1' and   psr.figure_position_id=jpgr.group_id and jpgr.figure_id="
					+ posId + " and psr.figure_flow_id=jfsi.figure_id"
					+ "         and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0))";
		}
		sql = sql + " order by jfs.pre_flow_id,jfs.sort_id,jfs.flow_id";
		return sql;
	}

	@Override
	public List<ProcessWebBean> findProcessAnalysis(long processId, long timeType, long processType, long posId,
			long projectId, int start, int limit) throws Exception {
		try {
			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {
				String sql = " select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,leadOrg.org_id,leadOrg.org_name,jfbi.type_res_people,jfbi.res_people_id,"
						+ "  case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ "  ,jfd.quantity,jft.type_name";
				String withSql = "";
				if (processId != -1) {
					withSql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (processId != -1) {
					sql = sql + " from MY_FLOW jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

				sql = sql + ",jecn_flow_driver jfd";
				if (processType != -1) {
					sql = sql + ",jecn_flow_type jft";
				}
				sql = sql + ",jecn_flow_basic_info jfbi"
						+ "       left join (select jfo.org_id,jfo.org_name,jfro.flow_id from"
						+ "       jecn_flow_org jfo,jecn_flow_related_org jfro where jfro.org_id ="
						+ "       jfo.org_id and jfo.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id"
						+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
						+ "       left join (select jfoi.figure_id,jfoi.figure_text from"
						+ "       jecn_flow_org_image jfoi,jecn_flow_org org where jfoi.org_id=org.org_id"
						+ "       and org.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				if (processType == -1) {
					sql = sql + " left join jecn_flow_type jft on jft.type_id=jfbi.type_id";
				}
				sql = sql
						+ " where  jfs.flow_id=jfbi.flow_id and jfbi.driver_type=1 and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
						+ projectId;
				sql = sql + " and jfbi.flow_id = jfd.flow_id ";
				if (timeType != -1) {
					sql = sql + " and jfd.quantity='" + timeType + "'";
				} else {
					sql = sql + " and jfd.quantity in ('1','2','3','4','5')";
				}
				if (processType != -1) {
					sql = sql + " and jfbi.type_id=jft.type_id and jft.type_id=" + processType;
				}
				if (posId != -1) {
					sql = sql
							+ "(jfs.flow_id in"
							+ "(select jfs_process.flow_id"
							+ "  from process_station_related psr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
							+ "  where psr.type='0' and psr.figure_position_id="
							+ posId
							+ " and psr.figure_flow_id=jfsi.figure_id"
							+ "  and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0)"
							+ "  or"
							+ "  jfs.flow_id in"
							+ "  (select jfs_process.flow_id"
							+ "         from process_station_related psr,jecn_position_group_r jpgr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
							+ "         where psr.type='1' and   psr.figure_position_id=jpgr.group_id and jpgr.figure_id="
							+ posId + " and psr.figure_flow_id=jfsi.figure_id"
							+ "         and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0))";
				}
				if (processId != -1) {
					listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit);
				} else {
					listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit);
				}

			} else if (JecnCommon.isOracle()) {

				String sql = " select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,leadOrg.org_id,leadOrg.org_name,jfbi.type_res_people,jfbi.res_people_id,"
						+ "  case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ "  ,jfd.quantity,jft.type_name";
				if (processId != -1) {
					sql = sql
							+ " from (select t.* from jecn_flow_structure t CONNECT BY PRIOR t.flow_id=t.pre_flow_id start with t.flow_id="
							+ processId + ") jfs";
				} else {
					sql = sql + " from jecn_flow_structure jfs";
				}

				sql = sql + ",jecn_flow_driver jfd";
				if (processType != -1) {
					sql = sql + ",jecn_flow_type jft";
				}
				sql = sql + ",jecn_flow_basic_info jfbi"
						+ "       left join (select jfo.org_id,jfo.org_name,jfro.flow_id from"
						+ "       jecn_flow_org jfo,jecn_flow_related_org jfro where jfro.org_id ="
						+ "       jfo.org_id and jfo.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id"
						+ "       left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
						+ "       left join (select jfoi.figure_id,jfoi.figure_text from"
						+ "       jecn_flow_org_image jfoi,jecn_flow_org org where jfoi.org_id=org.org_id"
						+ "       and org.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				if (processType == -1) {
					sql = sql + " left join jecn_flow_type jft on jft.type_id=jfbi.type_id";
				}
				sql = sql
						+ " where  jfs.flow_id=jfbi.flow_id and jfbi.driver_type=1 and  jfs.isflow=1 and jfs.del_state=0 and jfs.projectid="
						+ projectId;
				sql = sql + " and jfbi.flow_id = jfd.flow_id ";
				if (timeType != -1) {
					sql = sql + " and jfd.quantity='" + timeType + "'";
				} else {
					sql = sql + " and jfd.quantity in ('1','2','3','4','5')";
				}
				if (processType != -1) {
					sql = sql + " and jfbi.type_id=jft.type_id and jft.type_id=" + processType;
				}
				if (posId != -1) {
					sql = sql
							+ "(jfs.flow_id in"
							+ "(select jfs_process.flow_id"
							+ "  from process_station_related psr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
							+ "  where psr.type='0' and psr.figure_position_id="
							+ posId
							+ " and psr.figure_flow_id=jfsi.figure_id"
							+ "  and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0)"
							+ "  or"
							+ "  jfs.flow_id in"
							+ "  (select jfs_process.flow_id"
							+ "         from process_station_related psr,jecn_position_group_r jpgr,jecn_flow_structure_image jfsi,jecn_flow_structure jfs_process"
							+ "         where psr.type='1' and   psr.figure_position_id=jpgr.group_id and jpgr.figure_id="
							+ posId + " and psr.figure_flow_id=jfsi.figure_id"
							+ "         and jfsi.flow_id = jfs_process.flow_id and jfs_process.del_state=0))";
				}
				sql = sql + " order by jfs.pre_flow_id,jfs.sort_id,jfs.flow_id";
				listObj = this.flowStructureDao.listNativeSql(sql, start, limit);
			}
			if (listObj == null) {
				return null;
			}
			List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null || obj[8] == null) {
					continue;
				}
				ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
				if (processWebBean != null)
					listProcessWebBean.add(processWebBean);
			}

			return listProcessWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 流程访问数统计
	 * 
	 * @param processId
	 * @param resOrgId
	 * @param startTime
	 * @param endTime
	 * @param projectId
	 * @param userType
	 *            0是查看流程图，1是下载流程文件
	 * @return
	 */
	private String getSqlProcessAccess(long processId, long resOrgId, String startTime, String endTime, long projectId,
			int type) {
		String sql = "	select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
				+ "        ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
				+ ",(select count(jut.id) from jecn_user_total jut" + " where jut.user_total_type=" + type;
		if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = sql + "and datediff(day,?,jut.access_date)>=0 and datediff(day,jut.access_date,?)>=0";
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = sql
						+ " and to_char(jut.access_date,'yyyy-MM-dd')>=? and to_char(jut.access_date,'yyyy-MM-dd')<=?";
			}
		}
		sql = sql + " and jut.flow_id=t.flow_id) as count_total" + " from jecn_flow_basic_info jfbi";
		if (resOrgId == -1) {
			sql = sql
					+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
		}
		sql = sql
				+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
				+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			String sqlSQLSERVER = "";
			if (processId != -1) {
				sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
						+ processId
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
			}
			if (resOrgId != -1) {
				if (processId != -1) {
					sqlSQLSERVER = sqlSQLSERVER + ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				} else {
					sqlSQLSERVER = sqlSQLSERVER + "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				}
			}
			sql = sqlSQLSERVER + sql;
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
						+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
			}
			if (processId != -1) {
				sql = sql + ",MY_FLOW t";
			} else {
				sql = sql + " ,jecn_flow_structure t";
			}

		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
						+ resOrgId + ") org,jecn_flow_related_org jfro_t"
						+ "       where org.org_id=jfro_t.org_id) leadOrg";
			}
			if (processId != -1) {
				sql = sql
						+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
						+ processId + ") t";
			} else {
				sql = sql + ",jecn_flow_structure t";
			}
		}
		sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
		if (resOrgId != -1) {
			sql = sql + "  and t.flow_id = leadOrg.flow_id";
		}
		sql = sql + "  order by t.pre_flow_id,t.sort_id,t.flow_id";
		return sql;
	}

	@Override
	public List<ProcessAccessBean> findAllProcessAccess(long processId, long resOrgId, String startTime,
			String endTime, long projectId, int type) throws Exception {
		try {
			List<Object[]> listObj = null;
			String sql = this.getSqlProcessAccess(processId, resOrgId, startTime, endTime, projectId, type);
			if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
				if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Date sDate = dateFormat.parse(startTime);
					Date eDate = dateFormat.parse(endTime);
					listObj = this.flowStructureDao.listNativeSql(sql, sDate, eDate);
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
					listObj = this.flowStructureDao.listNativeSql(sql, JecnCommon.getStringbyDate(JecnCommon
							.getDateByString(startTime)), JecnCommon.getStringbyDate(JecnCommon
							.getDateByString(endTime)));
				}
			} else {
				listObj = this.flowStructureDao.listNativeSql(sql);
			}
			List<ProcessAccessBean> listProcessAccessBean = new ArrayList<ProcessAccessBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null || obj[8] == null) {
					continue;
				}
				ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
				if (processWebBean != null) {
					ProcessAccessBean processAccessBean = new ProcessAccessBean();
					processAccessBean.setProcessWebBean(processWebBean);
					processAccessBean.setAccessCount(Integer.parseInt(obj[8].toString()));
					listProcessAccessBean.add(processAccessBean);
				}
			}
			return listProcessAccessBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessAccessBean> findProcessAccess(long processId, long resOrgId, String startTime, String endTime,
			long projectId, int start, int limit, int type) throws Exception {
		try {
			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {

				String sql = "	select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
						+ "        ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ ",(select count(jut.id) from jecn_user_total jut" + " where jut.user_total_type=" + type;
				if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
					sql = sql + "and datediff(day,?,jut.access_date)>=0 and datediff(day,jut.access_date,?)>=0";
				}
				sql = sql + " and jut.flow_id=t.flow_id) as count_total" + " from jecn_flow_basic_info jfbi";
				if (resOrgId == -1) {
					sql = sql
							+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
				}
				sql = sql
						+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
						+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				String withSql = "";
				if (processId != -1) {
					withSql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (resOrgId != -1) {
					if (processId != -1) {
						withSql = withSql
								+ ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					} else {
						withSql = withSql
								+ "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
				}
				if (processId != -1) {
					sql = sql + ",MY_FLOW t";
				} else {
					sql = sql + " ,jecn_flow_structure t";
				}

				sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				if (processId != -1 || resOrgId != -1) {
					if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
						listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit, JecnCommon
								.getDateByString(startTime), JecnCommon.getDateByString(endTime));

					} else {
						listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit);
					}

				} else {
					if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
						listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit, JecnCommon
								.getDateByString(startTime), JecnCommon.getDateByString(endTime));
					} else {
						listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit);
					}
				}

			} else if (JecnCommon.isOracle()) {

				String sql = "	select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
						+ "        ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ ",(select count(jut.id) from jecn_user_total jut" + " where jut.user_total_type=" + type;
				if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
					sql = sql
							+ " and to_char(jut.access_date,'yyyy-MM-dd')>=? and to_char(jut.access_date,'yyyy-MM-dd')<=?";
				}
				sql = sql + " and jut.flow_id=t.flow_id) as count_total" + " from jecn_flow_basic_info jfbi";
				if (resOrgId == -1) {
					sql = sql
							+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
				}
				sql = sql
						+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
						+ " left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ resOrgId + ") org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id) leadOrg";
				}
				if (processId != -1) {
					sql = sql
							+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
							+ processId + ") t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}
				sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				sql = sql + "  order by t.pre_flow_id,t.sort_id,t.flow_id";
				if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
					listObj = this.flowStructureDao.listNativeSql(sql, start, limit, JecnCommon
							.getStringbyDate(JecnCommon.getDateByString(startTime)), JecnCommon
							.getStringbyDate(JecnCommon.getDateByString(endTime)));
				} else {
					listObj = this.flowStructureDao.listNativeSql(sql, start, limit);
				}
			}

			List<ProcessAccessBean> listProcessWebBean = new ArrayList<ProcessAccessBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null || obj[8] == null) {
					continue;
				}
				ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
				if (processWebBean != null) {
					ProcessAccessBean processAccessBean = new ProcessAccessBean();
					processAccessBean.setProcessWebBean(processWebBean);
					processAccessBean.setAccessCount(Integer.parseInt(obj[8].toString()));
					listProcessWebBean.add(processAccessBean);
				}
			}
			return listProcessWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int findProcessAccessCount(long processId, long resOrgId, String startTime, String endTime, long projectId,
			int type) throws Exception {
		try {
			String sql = " select count(t.flow_id) from jecn_flow_basic_info jfbi";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLSERVER = "";
				if (processId != -1) {
					sqlSQLSERVER = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (resOrgId != -1) {
					if (processId != -1) {
						sqlSQLSERVER = sqlSQLSERVER
								+ ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					} else {
						sqlSQLSERVER = sqlSQLSERVER
								+ "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				sql = sqlSQLSERVER + sql;
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
				}
				if (processId != -1) {
					sql = sql + ",MY_FLOW  t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}

			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ resOrgId + ") org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id) leadOrg";
				}
				if (processId != -1) {
					sql = sql
							+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
							+ processId + ") t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}
			}
			sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
			if (resOrgId != -1) {
				sql = sql + "  and t.flow_id = leadOrg.flow_id";
			}
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 流程访问人统计
	 * 
	 * @param userId
	 * @param processId
	 * @param startTime
	 * @param endTime
	 * @param resOrgId
	 * @param orgId
	 * @param posId
	 * @param projectId
	 * @return
	 */
	private String getSqlProcessInterviewer(long userId, long processId, String startTime, String endTime,
			long resOrgId, long orgId, long posId, long projectId, int type) {
		String sql = " select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
				+ " ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
				+ ",people.people_id,people.true_name,jut.access_date"
				+ " from jecn_user_total jut,jecn_user people,jecn_flow_basic_info jfbi ";
		if (resOrgId == -1) {
			sql = sql
					+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
		}
		sql = sql
				+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
				+ "         left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			String sqlSQLServer = "";
			if (processId != -1) {
				sqlSQLServer = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
						+ processId
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
			}
			if (resOrgId != -1) {
				if (processId != -1) {
					sqlSQLServer = sqlSQLServer + ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				} else {
					sqlSQLServer = sqlSQLServer + "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
							+ resOrgId + " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
				}
			}
			sql = sqlSQLServer + sql;
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
						+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
			}
			if (processId != -1) {
				sql = sql + ",MY_FLOW t";
			} else {
				sql = sql + ",jecn_flow_structure t";
			}
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			if (resOrgId != -1) {
				sql = sql
						+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
						+ resOrgId + ") org,jecn_flow_related_org jfro_t"
						+ "       where org.org_id=jfro_t.org_id) leadOrg";
			}
			if (processId != -1) {
				sql = sql
						+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
						+ processId + ") t";
			} else {
				sql = sql + ",jecn_flow_structure t";
			}

		}
		sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
		sql = sql + " and jfbi.flow_id=jut.flow_id and jut.people_id = people.people_id and jut.user_total_type="
				+ type;
		if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = sql + "and DATEDIFF(day,?,jut.access_date)>=0 and datediff(day,jut.access_date,?)>=0";
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = sql
						+ " and to_char(jut.access_date,'yyyy-MM-dd')>=? and  to_char(jut.access_date,'yyyy-MM-dd')<=?";
			}

		}
		if (resOrgId != -1) {
			sql = sql + "  and t.flow_id = leadOrg.flow_id";
		}
		if (orgId != -1) {
			sql = sql + " and jut.people_id in (";
			sql = sql + "select distinct ju.people_id"
					+ "        from jecn_flow_org_image pos,jecn_user_position_related jupr,jecn_user ju"
					+ "        where ju.islock=0 and ju.people_id=jupr.people_id and jupr.figure_id=pos.figure_id"
					+ "        and pos.org_id=" + orgId + ")";
		}
		if (posId != -1) {
			sql = sql + " and jut.people_id in (";
			sql = sql + "select ju.people_id" + "        from jecn_user_position_related jupr,jecn_user ju"
					+ "        where ju.islock=0 and ju.people_id=jupr.people_id and jupr.figure_id=" + posId + ")";
		}
		if (userId != -1) {
			sql = sql + " and jut.people_id =" + userId;
		}
		sql = sql + "  order by t.pre_flow_id,t.sort_id,t.flow_id";
		return sql;
	}

	@Override
	public List<ProcessAccessBean> findAllProcessInterviewer(long userId, long processId, String startTime,
			String endTime, long resOrgId, long orgId, long posId, long projectId, int type) throws Exception {
		try {
			List<Object[]> listObj = null;
			if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
				String sql = this.getSqlProcessInterviewer(userId, processId, startTime, endTime, resOrgId, orgId,
						posId, projectId, type);
				if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Date sDate = dateFormat.parse(startTime);
					Date eDate = dateFormat.parse(endTime);
					listObj = this.flowStructureDao.listNativeSql(sql, sDate, eDate);
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
					listObj = this.flowStructureDao.listNativeSql(sql, JecnCommon.getStringbyDate(JecnCommon
							.getDateByString(startTime)), JecnCommon.getStringbyDate(JecnCommon
							.getDateByString(endTime)));
				}
			} else {
				listObj = this.flowStructureDao.listNativeSql(this.getSqlProcessInterviewer(userId, processId,
						startTime, endTime, resOrgId, orgId, posId, projectId, type));
			}
			List<ProcessAccessBean> listProcessAccessBean = new ArrayList<ProcessAccessBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null || obj[8] == null || obj[10] == null) {
					continue;
				}
				ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
				if (processWebBean != null) {
					ProcessAccessBean processAccessBean = new ProcessAccessBean();
					processAccessBean.setProcessWebBean(processWebBean);
					processAccessBean.setAccessCount(Integer.parseInt(obj[8].toString()));
					// 访问人
					processAccessBean.setAccessId(Long.valueOf(obj[8].toString()));
					// 访问人名
					if (obj[9] != null)
						processAccessBean.setAccessName(obj[9].toString());
					processAccessBean.setAccessTime(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[10]
							.toString())));
					listProcessAccessBean.add(processAccessBean);
				}
			}
			return listProcessAccessBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<ProcessAccessBean> findProcessInterviewer(long userId, long processId, String startTime,
			String endTime, long resOrgId, long orgId, long posId, long projectId, int start, int limit, int type)
			throws Exception {
		try {
			List<Object[]> listObj = null;
			if (JecnCommon.isSQLServer()) {
				String sql = " select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
						+ " ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ ",people.people_id,people.true_name,jut.access_date"
						+ " from jecn_user_total jut,jecn_user people,jecn_flow_basic_info jfbi ";
				if (resOrgId == -1) {
					sql = sql
							+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
				}
				sql = sql
						+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
						+ "         left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				String withSql = "";
				if (processId != -1) {
					withSql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (resOrgId != -1) {
					if (processId != -1) {
						withSql = withSql
								+ ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					} else {
						withSql = withSql
								+ "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
				}
				if (processId != -1) {
					sql = sql + ",MY_FLOW t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}
				sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				sql = sql
						+ " and jfbi.flow_id=jut.flow_id and jut.people_id = people.people_id and jut.user_total_type="
						+ type;
				if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
					sql = sql + "and DATEDIFF(day,?,jut.access_date)>=0 and datediff(day,jut.access_date,?)>=0";

				}
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				if (orgId != -1) {
					sql = sql + " and jut.people_id in (";
					sql = sql
							+ "select distinct ju.people_id"
							+ "        from jecn_flow_org_image pos,jecn_user_position_related jupr,jecn_user ju"
							+ "        where ju.islock=0 and ju.people_id=jupr.people_id and jupr.figure_id=pos.figure_id"
							+ "        and pos.org_id=" + orgId + ")";
				}
				if (posId != -1) {
					sql = sql + " and jut.people_id in (";
					sql = sql + "select ju.people_id" + "        from jecn_user_position_related jupr,jecn_user ju"
							+ "        where ju.islock=0 and ju.people_id=jupr.people_id and jupr.figure_id=" + posId
							+ ")";
				}
				if (userId != -1) {
					sql = sql + " and jut.people_id =" + userId;
				}
				if (processId != -1 || resOrgId != -1) {
					if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
						listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit, JecnCommon
								.getDateByString(startTime), JecnCommon.getDateByString(endTime));

					} else {
						listObj = flowStructureDao.listSQLServerWith(withSql, sql, "flow_id", start, limit);
					}
				} else {
					if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
						listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit, JecnCommon
								.getDateByString(startTime), JecnCommon.getDateByString(endTime));
					} else {
						listObj = flowStructureDao.listSQLServerSelect(sql, "flow_id", start, limit);
					}
				}

			} else if (JecnCommon.isOracle()) {
				String sql = " select t.flow_id,t.flow_name,t.flow_id_input,leadOrg.org_id,leadOrg.org_name"
						+ " ,jfbi.type_res_people,jfbi.res_people_id,case when jfbi.type_res_people=0 then ju.true_name when jfbi.type_res_people=1 then pos.figure_text end as res_name"
						+ ",people.people_id,people.true_name,jut.access_date"
						+ " from jecn_user_total jut,jecn_user people,jecn_flow_basic_info jfbi ";
				if (resOrgId == -1) {
					sql = sql
							+ " left join (select jfo_t.org_id,jfo_t.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo_t where jfro.org_id=jfo_t.org_id and jfo_t.del_state=0) leadOrg on leadOrg.flow_id=jfbi.flow_id";
				}
				sql = sql
						+ " left join jecn_user ju on ju.isLock=0 and ju.people_id=jfbi.res_people_id"
						+ "         left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org jfo_tt,jecn_flow_org_image jfoi where jfoi.org_id=jfo_tt.org_id and jfo_tt.del_state=0) pos on pos.figure_id=jfbi.res_people_id";
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ resOrgId + ") org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id) leadOrg";
				}
				if (processId != -1) {
					sql = sql
							+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
							+ processId + ") t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}

				sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid="
						+ projectId;
				sql = sql
						+ " and jfbi.flow_id=jut.flow_id and jut.people_id = people.people_id and jut.user_total_type="
						+ type;
				if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
					sql = sql
							+ " and to_char(jut.access_date,'yyyy-MM-dd')>=? and  to_char(jut.access_date,'yyyy-MM-dd')<=?";

				}
				if (resOrgId != -1) {
					sql = sql + "  and t.flow_id = leadOrg.flow_id";
				}
				if (orgId != -1) {
					sql = sql + " and jut.people_id in (";
					sql = sql
							+ "select distinct ju.people_id"
							+ "        from jecn_flow_org_image pos,jecn_user_position_related jupr,jecn_user ju"
							+ "        where ju.islock=0 and ju.people_id=jupr.people_id and jupr.figure_id=pos.figure_id"
							+ "        and pos.org_id=" + orgId + ")";
				}
				if (posId != -1) {
					sql = sql + " and jut.people_id in (";
					sql = sql + "select ju.people_id" + "        from jecn_user_position_related jupr,jecn_user ju"
							+ "        where ju.islock=0 and ju.people_id=jupr.people_id and jupr.figure_id=" + posId
							+ ")";
				}
				if (userId != -1) {
					sql = sql + " and jut.people_id =" + userId;
				}
				sql = sql + "  order by t.pre_flow_id,t.sort_id,t.flow_id";
				if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
					listObj = this.flowStructureDao.listNativeSql(sql, start, limit, JecnCommon
							.getStringbyDate(JecnCommon.getDateByString(startTime)), JecnCommon
							.getStringbyDate(JecnCommon.getDateByString(endTime)));
				} else {
					listObj = flowStructureDao.listNativeSql(sql, start, limit);
				}

			}
			// if (StringUtils.isNotBlank(startTime)
			// && StringUtils.isNotBlank(endTime)) {
			// String sql = this.getSqlProcessInterviewer(userId, processId,
			// startTime, endTime, resOrgId, orgId, posId, projectId,
			// type);
			// if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			// SimpleDateFormat dateFormat = new SimpleDateFormat(
			// "yyyy-MM-dd");
			// Date sDate = dateFormat.parse(startTime);
			// Date eDate = dateFormat.parse(endTime);
			// listObj = this.flowStructureDao.listNativeSql(sql, start,
			// limit, sDate, eDate);
			// } else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			// listObj = this.flowStructureDao.listNativeSql(sql, start,
			// limit, JecnCommon.getStringbyDate(JecnCommon
			// .getDateByString(startTime)), JecnCommon
			// .getStringbyDate(JecnCommon
			// .getDateByString(endTime)));
			// }
			//
			// } else {
			// listObj = this.flowStructureDao.listNativeSql(this
			// .getSqlProcessInterviewer(userId, processId, startTime,
			// endTime, resOrgId, orgId, posId, projectId,
			// type), start, limit);
			// }
			if (listObj == null) {
				return null;
			}
			List<ProcessAccessBean> listProcessAccessBean = new ArrayList<ProcessAccessBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null || obj[8] == null || obj[10] == null) {
					continue;
				}
				ProcessWebBean processWebBean = JecnUtil.getProcessWebBeanByObjectCount(obj);
				if (processWebBean != null) {
					ProcessAccessBean processAccessBean = new ProcessAccessBean();
					processAccessBean.setProcessWebBean(processWebBean);
					processAccessBean.setAccessCount(Integer.parseInt(obj[8].toString()));
					// 访问人
					processAccessBean.setAccessId(Long.valueOf(obj[8].toString()));
					// 访问人名
					if (obj[9] != null)
						processAccessBean.setAccessName(obj[9].toString());
					processAccessBean.setAccessTime(JecnCommon.getStringbyDate(JecnCommon.getDateByString(obj[10]
							.toString())));
					listProcessAccessBean.add(processAccessBean);
				}
			}
			return listProcessAccessBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int findProcessInterviewerCount(long userId, long processId, String startTime, String endTime,
			long resOrgId, long orgId, long posId, long projectId, int type) throws Exception {
		try {
			String sql = " select count(t.flow_id) from jecn_user_total jut,jecn_user people,jecn_flow_basic_info jfbi ";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				String sqlSQLServer = "";
				if (processId != -1) {
					sqlSQLServer = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE JFT WHERE JFT.FLOW_ID = "
							+ processId
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.FLOW_ID=JFS.PRE_FLOW_ID)";
				}
				if (resOrgId != -1) {
					if (processId != -1) {
						sqlSQLServer = sqlSQLServer
								+ ",MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					} else {
						sqlSQLServer = sqlSQLServer
								+ "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JF WHERE JF.ORG_ID ="
								+ resOrgId
								+ " UNION ALL"
								+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)";
					}
				}
				sql = sqlSQLServer + sql;
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from MY_ORG org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id and org.del_state=0) leadOrg";
				}
				if (processId != -1) {
					sql = sql + ",MY_FLOW t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (resOrgId != -1) {
					sql = sql
							+ ",(select org.org_id,org.org_name,jfro_t.flow_id from (select jfo.* from jecn_flow_org jfo where jfo.del_state=0 CONNECT BY PRIOR jfo.org_id = jfo.per_org_id start with jfo.org_id="
							+ resOrgId + ") org,jecn_flow_related_org jfro_t"
							+ "       where org.org_id=jfro_t.org_id) leadOrg";
				}
				if (processId != -1) {
					sql = sql
							+ ",(select jfs.* from jecn_flow_structure jfs CONNECT BY PRIOR jfs.flow_id=jfs.pre_flow_id start with jfs.flow_id="
							+ processId + ") t";
				} else {
					sql = sql + ",jecn_flow_structure t";
				}

			}
			sql = sql + "  where jfbi.flow_id=t.flow_id and t.isflow=1 and t.del_state=0 and t.projectid=" + projectId;
			sql = sql + " and jfbi.flow_id=jut.flow_id and jut.people_id = people.people_id and jut.user_total_type="
					+ type;
			if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
				if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					sql = sql + "and DATEDIFF(day,?,jut.access_date)>=0 and datediff(day,jut.access_date,?)>=0";
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
					sql = sql
							+ " and to_char(jut.access_date,'yyyy-MM-dd')>=? and  to_char(jut.access_date,'yyyy-MM-dd')<=?";
				}
			}
			if (resOrgId != -1) {
				sql = sql + "  and t.flow_id = leadOrg.flow_id";
			}
			if (orgId != -1) {
				sql = sql + " and jut.people_id in (";
				sql = sql + "select distinct ju.people_id"
						+ "        from jecn_flow_org_image pos,jecn_user_position_related jupr,jecn_user ju"
						+ "        where ju.islock=0 and ju.people_id=jupr.people_id and jupr.figure_id=pos.figure_id"
						+ "        and pos.org_id=" + orgId + ")";
			}
			if (posId != -1) {
				sql = sql + " and jut.people_id in (";
				sql = sql + "select ju.people_id" + "        from jecn_user_position_related jupr,jecn_user ju"
						+ "        where ju.islock=0 and ju.people_id=jupr.people_id and jupr.figure_id=" + posId + ")";
			}
			if (userId != -1) {
				sql = sql + " and jut.people_id =" + userId;
			}
			if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
				return this.flowStructureDao.countAllByParamsNativeSql(sql, startTime, endTime);
			} else {
				return this.flowStructureDao.countAllByParamsNativeSql(sql);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 任务审批各个阶段用时记录（分页）
	 * 
	 * @param start
	 *            开始
	 * @param pageSize
	 *            分页条数
	 * @param taskType
	 *            任务类型
	 * @return List<TempTaskApproveTime> 任务审批时间集合
	 * @throws Exception
	 */
	@Override
	public List<TempTaskApproveTime> findTaskApproveTime(int start, int pageSize, int taskType) throws Exception {

		try {
			int end = start + pageSize;
			// 搜索条件(任务类型)
			String searchCondition = null;
			// 0：流程任务
			if (taskType == 0) {
				searchCondition = " and task_type=0 ";
			}
			// 1：文件任务
			else if (taskType == 1) {
				searchCondition = " and task_type=1 ";
			}
			// 2：制度模板任务，3：制度文件任务
			else if (taskType == 2 || taskType == 3) {
				searchCondition = " and (task_type=2 or task_type=3) ";
			}
			// 4：流程地图任务
			else if (taskType == 4) {
				searchCondition = " and task_type=4 ";
			}

			// 默认是Oracle数据库
			String sql = "with temp as"
					+ "  (select id,task_name,task_type,is_lock from jecn_task_bean_new where state=5 and is_lock=1 order by id )"
					+ "  select jtb.id as task_id," + "  jtb.task_name as task_name," + "  task_type as task_type,"
					+ "  jtfr.state as state," + "  jtfr.up_state as up_state,"
					+ "  to_char(jtfr.update_time,'YYYY-MM-DD hh24:mi:ss') as update_time,"
					+ "  jtfr.task_else_state as task_else_state," + "  jtfr.sort_id as sort_id"
					+ "  from  jecn_task_for_recode_new jtfr" + "  inner join  "
					+ "  (select id,task_name,task_type,is_lock from "
					+ "  (select id,task_name,task_type,is_lock,rownum rn " + "  from temp where rownum<=" + end
					+ "  and 1=1  " + searchCondition + "  )  where rn>" + start + ") jtb"
					+ "  on jtfr.task_id=jtb.id  " + "  order by sort_id";

			// 如果是SQLSERVER数据库
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "with temp as (" + "	select id,task_name,task_type from ("
						+ "	select id,task_name,task_type,row_number() over (order by id asc) as rownum"
						+ "	from jecn_task_bean_new  where" + "	state = 5" + "	and is_lock = 1" + searchCondition
						+ "	) as t" + "	where t.rownum>" + start + "  and t.rownum<=" + end + "	)"
						+ "	       select   jtb.id              as task_id,"
						+ "	                jtb.task_name       as task_name,"
						+ "	                jtb.task_type       as task_type,"
						+ "	                jtfr.state           as state,"
						+ "	                jtfr.up_state        as up_state,"
						+ "	                jtfr.update_time     as update_time,"
						+ "	                jtfr.task_else_state as task_else_state,"
						+ "	                jtfr.sort_id         as sort_id"
						+ "	           from jecn_task_for_recode_new jtfr,temp jtb "
						+ "	          where jtfr.task_id = jtb.id" + "	          order by sort_id";
			}

			return getApproveTimes(sql);
		} catch (Exception e) {

			log.error("获取任务审批各个阶段用时记录错误！", e);
			throw e;
		}
	}

	/**
	 * 获取任务的审批耗时
	 * 
	 * @param sql
	 */
	private List<TempTaskApproveTime> getApproveTimes(String sql) throws Exception {

		List<Object[]> listObj = this.flowStructureDao.listNativeSql(sql);
		// 存放所有的临时任务对象
		Map<Long, List<TempTaskInfoBean>> taskRecordMap = new HashMap<Long, List<TempTaskInfoBean>>();

		for (Object[] obj : listObj) {
			// 获取任务Id
			Long taskId = Long.parseLong(obj[0].toString());
			List<TempTaskInfoBean> taskInfoBeanList = null;
			// 如果已经存储过该Id下的集合，则继续向该集合内添加数据
			if (taskRecordMap.containsKey(taskId)) {
				taskInfoBeanList = taskRecordMap.get(taskId);
			}
			// 如果是第一次添加则实例化记录集合并添加到Map中
			if (taskInfoBeanList == null) {
				taskInfoBeanList = new ArrayList<TempTaskInfoBean>();
				taskRecordMap.put(taskId, taskInfoBeanList);
			}
			// 封装数据
			TempTaskInfoBean tempTaskInfoBean = new TempTaskInfoBean();
			String taskName = obj[1].toString();
			String taskType = obj[2].toString();
			Integer state = Integer.parseInt(obj[3].toString());
			Integer upState = Integer.parseInt(obj[4].toString());
			Date updateTime = JecnCommon.getDateTimeByString(obj[5].toString());
			Integer taskElseState = Integer.parseInt(obj[6].toString());

			// 加入TempTaskInfoBean 对象中
			tempTaskInfoBean.setTaskId(taskId);
			tempTaskInfoBean.setTaskName(taskName);
			tempTaskInfoBean.setTaskType(taskType);
			tempTaskInfoBean.setState(state);
			tempTaskInfoBean.setUpState(upState);
			tempTaskInfoBean.setUpdateTime(updateTime);
			tempTaskInfoBean.setTaskElseState(taskElseState);

			// 将临时任务对象加入list
			taskInfoBeanList.add(tempTaskInfoBean);
		}

		// 流程审批时间
		List<TempTaskApproveTime> taskApproveTimes = new ArrayList<TempTaskApproveTime>();
		for (Long taskId : taskRecordMap.keySet()) {
			// 得到审批时间
			TempTaskApproveTime taskApproveTime = getApproveTime(taskRecordMap.get(taskId));
			TempTaskInfoBean tempTaskInfoBean = taskRecordMap.get(taskId).get(0);
			taskApproveTime.setTaskId(taskId);
			taskApproveTime.setTaskName(tempTaskInfoBean.getTaskName());
			taskApproveTime.setTaskType(tempTaskInfoBean.getTaskType());
			if (taskApproveTime != null) {
				taskApproveTimes.add(taskApproveTime);
			}
		}

		return taskApproveTimes;
	}

	/**
	 * 获取单个任务的各阶段审批耗时
	 * 
	 * @author weidp
	 * @date 2014-07-15
	 * @param taskInfoBeanList
	 *            单个任务下的所有审批记录
	 * @return
	 */
	private TempTaskApproveTime getApproveTime(List<TempTaskInfoBean> taskInfoBeanList) {
		TempTaskApproveTime taskApproveTime = new TempTaskApproveTime();
		// 记录每个阶段的耗时<state,costTime>
		Map<Integer, Integer> stateMap = new HashMap<Integer, Integer>();
		for (TempTaskInfoBean tempTaskInfoBean : taskInfoBeanList) {
			Integer state = tempTaskInfoBean.getState();
			// 每一个state会统计一次耗时
			if (stateMap.containsKey(state)) {
				continue;
			}
			// 获取该阶段花费的时间
			int costTime = getStateCostTime(state, taskInfoBeanList);
			stateMap.put(state, costTime);
		}
		// 生成TempTaskApproveTime
		for (Integer state : stateMap.keySet()) {
			setTaskApproveTimeByState(taskApproveTime, state, stateMap.get(state));
		}
		return taskApproveTime;

	}

	/**
	 * 获取阶段所花费的时间
	 * 
	 * @author weidp
	 * @date 2014-07-15
	 * @param state
	 *            要统计时间的阶段
	 * @param taskInfoBeanList
	 *            该任务的所有记录
	 */
	private int getStateCostTime(Integer state, List<TempTaskInfoBean> taskInfoBeanList) {
		// 记录阶段耗时
		double costTime = 0;
		// 遍历该任务的所有记录
		for (int index = 0; index < taskInfoBeanList.size(); index++) {
			TempTaskInfoBean taskInfo = taskInfoBeanList.get(index);
			// 找到state为要查询的阶段时去获取他的耗时（同一阶段可能会执行N次）
			if (taskInfo.getState().intValue() == state.intValue() && taskInfo.getState() != 5) {
				Date startDate = taskInfo.getUpdateTime();
				// 获取阶段耗时，和阶段结束时在所有记录中的索引，下一次遍历将从结束时间的索引开始
				Object[] data = getUpStateUpdateTime(index, state, taskInfoBeanList);
				if (data[0] == null || data[1] == null) {
					log.error("错误的任务：" + taskInfo.getTaskName() + "\t ID:" + taskInfo.getTaskId());
					continue;
				}
				index = (Integer) data[0];
				Date endDate = (Date) data[1];
				// 累加该阶段耗时
				costTime += calculateCostTime(startDate, endDate);
			}
		}
		// 向上取整
		if (costTime > 0) {
			return (int) Math.ceil(costTime);
		}
		return (int) costTime;
	}

	/**
	 * 计算阶段耗时
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	private double calculateCostTime(Date startDate, Date endDate) {
		long startTime = startDate.getTime();
		long endTime = endDate.getTime();
		// 一天的毫秒值
		final double oneDayMillisecond = 86400000;
		if (endTime <= startTime) {
			return 0;
		}
		double approveTime = (endTime - startTime) / oneDayMillisecond;
		return approveTime;
	}

	/**
	 * 获取阶段的结束时间和该条记录的索引
	 * 
	 * @author weidp
	 * @date 2014-07-15
	 * @param index
	 *            开始查询的索引
	 * @param state
	 *            要查询的state
	 * @param taskInfoBeanList
	 *            该条任务的所有记录
	 * @return
	 */
	private Object[] getUpStateUpdateTime(Integer index, Integer state, List<TempTaskInfoBean> taskInfoBeanList) {
		// 从当前记录的下一条开始查询
		index++;
		Object[] data = new Object[2];
		for (; index < taskInfoBeanList.size(); index++) {
			TempTaskInfoBean taskInfo = taskInfoBeanList.get(index);
			log.info("state:" + state + "\t TaskState:" + taskInfo.getState() + "\t TaskUpstate:"
					+ taskInfo.getUpState());
			// 过滤撤回操作，记录upState=state的数据
			if (taskInfo.getUpState().intValue() == state.intValue() && taskInfo.getState() != taskInfo.getUpState()) {
				data[0] = index;
				data[1] = taskInfo.getUpdateTime();
				return data;
			}
		}
		return data;
	}

	/**
	 * 根据当前state获得设置对应的审批阶段的操作时间
	 * 
	 * @param taskApproveTime
	 *            任务审批时间
	 * @param curState
	 *            当前阶段
	 * @param approveDate
	 *            当前阶段审批花费的时间
	 * @return
	 * @throws Exception
	 */
	private void setTaskApproveTimeByState(TempTaskApproveTime taskApproveTime, Integer curState, Integer approveDate) {
		// 0 为拟稿人
		if (0 == curState) {
			taskApproveTime.setPeopleMake(approveDate);
		}
		// 1文控审核
		else if (1 == curState) {
			taskApproveTime.setDocumentControl(approveDate);
		}
		// 2部门审核
		else if (2 == curState) {
			taskApproveTime.setDepartment(approveDate);
		}
		// 3评审阶段
		else if (3 == curState) {
			taskApproveTime.setReviewPeople(approveDate);
		}
		// 4：批准阶段
		else if (4 == curState) {
			taskApproveTime.setApproval(approveDate);
		}
		// 5：完成
		else if (5 == curState) {
			return;
		}
		// 6：各业务体系审批阶段
		else if (6 == curState) {
			taskApproveTime.setEachBusiness(approveDate);
		}
		// 7：IT总监审批阶段
		else if (7 == curState) {
			taskApproveTime.setDirector(approveDate);
		}
		// 8:事业部经理
		else if (8 == curState) {
			taskApproveTime.setDepartmentManager(approveDate);
		}
		// 9：总经理
		else if (9 == curState) {
			taskApproveTime.setManager(approveDate);
		}
		// 10:整理意见
		else if (10 == curState) {
			taskApproveTime.setFinishingViews(approveDate);
		}

	}

	/**
	 * 根据任务类型获得该任务类型的任务数目
	 * 
	 * @param taskType
	 * @return
	 */
	public int findTaskApproveTimeCount(int taskType) throws Exception {

		try {
			// 搜索条件(任务类型)
			String searchCondition = null;
			// 0：流程任务
			if (taskType == 0) {
				searchCondition = " and a.task_type=0 ";
			}
			// 1：文件任务
			else if (taskType == 1) {
				searchCondition = " and a.task_type=1 ";
			}
			// 2：制度模板任务，3：制度文件任务
			else if (taskType == 2 || taskType == 3) {
				searchCondition = " and (a.task_type=2 or a.task_type=3) ";
			}
			// 4：流程地图任务
			else if (taskType == 4) {
				searchCondition = " and a.task_type=4 ";
			}
			String sql = "select count(distinct a.id)" + " from jecn_task_bean_new a"
					+ " inner join  jecn_task_for_recode_new b" + " on a.id=b.task_id" + "  where" + "  a.state = 5"
					+ "  and a.is_lock = 1 " + searchCondition;
			return this.flowStructureDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	/**
	 * 根据任务类型获得该任务类型的任务
	 * 
	 * @author huoyl
	 * @param taskType
	 *            任务类型
	 * @return List<TempTaskApproveTime> 任务集合
	 */
	public List<TempTaskApproveTime> findAllTaskApproveTime(int taskType) throws Exception {

		// 搜索条件(任务类型)
		String searchCondition = null;
		// 0：流程任务
		if (taskType == 0) {
			searchCondition = " and task_type=0 ";
		}
		// 1：文件任务
		else if (taskType == 1) {
			searchCondition = " and task_type=1 ";
		}
		// 2：制度模板任务，3：制度文件任务
		else if (taskType == 2 || taskType == 3) {
			searchCondition = " and (task_type=2 or task_type=3) ";
		}
		// 4：流程地图任务
		else if (taskType == 4) {
			searchCondition = " and task_type=4 ";
		}

		String sql = " select jtb.id as task_id," + " jtb.task_name as task_name," + " task_type as task_type,"
				+ " jtfr.state as state," + " jtfr.up_state as up_state," + " jtfr.update_time as update_time,"
				+ " jtfr.task_else_state as task_else_state" + " from  jecn_task_for_recode_new jtfr" + " inner join  "
				+ " (select id,task_name,task_type,is_lock " + " from jecn_task_bean_new where  state=5 and is_lock=1 "
				+ searchCondition + " ) jtb" + " on jtfr.task_id=jtb.id " + " order by jtfr.sort_id";

		// 如果是Oracle数据库
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "with temp as"
					+ " (select id,task_name,task_type,is_lock,task_else_state from jecn_task_bean_new  where state=5 and is_lock=1 order by id )"
					+ " select jtb.id as task_id," + " jtb.task_name as task_name," + " task_type as task_type,"
					+ " jtfr.state as state," + " jtfr.up_state as up_state,"
					+ " to_char(jtfr.update_time,'YYYY-MM-DD hh24:mi:ss') as update_time,"
					+ " jtfr.task_else_state as task_else_state" + " from  jecn_task_for_recode_new jtfr"
					+ " inner join  " + " (select id,task_name,task_type " + " from temp where " + " 1=1 "
					+ searchCondition + " ) jtb" + " on jtfr.task_id=jtb.id " + " order by sort_id";
		}
		return getApproveTimes(sql);
	}

	/**
	 * 根据任务类型获得该任务类型的表头
	 * 
	 * @author huoyl
	 * @param taskType
	 *            任务类型
	 * @return
	 */
	public String findTaskApproveTimeHeader(int taskType) throws Exception {

		List<Object[]> strList = getHeadersList(taskType);
		StringBuffer headersBuffer = new StringBuffer();

		headersBuffer.append("[");
		headersBuffer.append("new Ext.grid.RowNumberer(),");
		// 任务名称
		headersBuffer.append(JecnCommon.columnsJson(JecnUtil.getValue("taskName"), "taskName", "taskRenderer"));

		// 拟稿人阶段
		headersBuffer.append(",");
		headersBuffer.append(JecnCommon.columnsJson(JecnUtil.getValue("peopleMake"), "peopleMake", null));

		for (Object[] str : strList) {
			String mark = str[0].toString();
			String name = str[1].toString();

			// 文控审核人
			if (mark.equals(ConfigItemPartMapMark.taskAppDocCtrlUser.toString())) {
				headersBuffer.append(",");
				headersBuffer.append(JecnCommon.columnsJson(name, "documentControl", null));

			}
			// 部门审核人
			else if (mark.equals(ConfigItemPartMapMark.taskAppDeptUser.toString())) {
				headersBuffer.append(",");
				headersBuffer.append(JecnCommon.columnsJson(name, "department", null));

			}
			// 评审人会审
			else if (mark.equals(ConfigItemPartMapMark.taskAppReviewer.toString())) {
				headersBuffer.append(",");
				headersBuffer.append(JecnCommon.columnsJson(name, "reviewPeople", null));
				// 会审之后是整理意见
				headersBuffer.append(",");
				headersBuffer.append(JecnCommon
						.columnsJson(JecnUtil.getValue("finishingViews"), "finishingViews", null));

			}
			// 批准人审核
			else if (mark.equals(ConfigItemPartMapMark.taskAppPzhUser.toString())) {
				headersBuffer.append(",");
				headersBuffer.append(JecnCommon.columnsJson(name, "approval", null));

			}
			// 各业务体系负责人
			else if (mark.equals(ConfigItemPartMapMark.taskAppOprtUser.toString())) {
				headersBuffer.append(",");
				headersBuffer.append(JecnCommon.columnsJson(name, "eachBusiness", null));

			}
			// IT总监
			else if (mark.equals(ConfigItemPartMapMark.taskAppITUser.toString())) {
				headersBuffer.append(",");
				headersBuffer.append(JecnCommon.columnsJson(name, "director", null));

			}
			// 事业部经理
			else if (mark.equals(ConfigItemPartMapMark.taskAppDivisionManager.toString())) {
				headersBuffer.append(",");
				headersBuffer.append(JecnCommon.columnsJson(name, "departmentManager", null));

			}
			// 总经理
			else if (mark.equals(ConfigItemPartMapMark.taskAppGeneralManager.toString())) {
				headersBuffer.append(",");
				headersBuffer.append(JecnCommon.columnsJson(name, "manager", null));

			}
		}

		headersBuffer.append("]");
		return headersBuffer.toString();
	}

	/**
	 * 根据任务类型获得最新的表头列表
	 * 
	 * @author huoyl
	 * @param taskType
	 * @return
	 * @throws Exception
	 */
	private List<Object[]> getHeadersList(int taskType) throws Exception {

		int bigModel = -1;
		// 0：流程任务
		if (taskType == 0) {

			bigModel = 1;
		}
		// 1：文件任务
		else if (taskType == 1) {
			bigModel = 3;
		}
		// 2：制度模板任务，3：制度文件任务
		else if (taskType == 2 || taskType == 3) {
			bigModel = 2;
		}
		// 4：流程地图任务
		else if (taskType == 4) {
			bigModel = 0;
		}

		String sql = "select mark,name from jecn_config_item where" + " type_big_model=" + bigModel
				+ " and type_small_model=0 " + " and mark!='taskAppPubType' order by sort";

		List<Object[]> strList = this.flowStructureDao.listNativeSql(sql);

		return strList;

	}

	/**
	 * 根据任务类型获得表头的bean
	 * 
	 * @author user huoyl
	 * @param taskType
	 *            任务类型
	 * @return TempApproveTimeHeadersBean 表头
	 */
	public TempApproveTimeHeadersBean findApproveTimeHeadersBean(int taskType) throws Exception {

		// 表头
		List<String> headers = new ArrayList<String>();
		// 表头顺序
		List<String> order = new ArrayList<String>();
		// 表头bean
		TempApproveTimeHeadersBean headersBean = new TempApproveTimeHeadersBean();

		List<Object[]> strList = getHeadersList(taskType);
		// 添加拟稿人阶段
		headers.add(JecnUtil.getValue("peopleMake"));
		order.add("taskAppPeopleMake");

		for (Object[] str : strList) {
			String mark = str[0].toString();
			String name = str[1].toString();

			// 文控审核人
			if (mark.equals(ConfigItemPartMapMark.taskAppDocCtrlUser.toString())) {
				headers.add(name);
				order.add(mark);

			}
			// 部门审核人
			else if (mark.equals(ConfigItemPartMapMark.taskAppDeptUser.toString())) {

				headers.add(name);
				order.add(mark);

			}
			// 评审人会审
			else if (mark.equals(ConfigItemPartMapMark.taskAppReviewer.toString())) {

				headers.add(name);
				order.add(mark);
				// 整理意见
				headers.add(JecnUtil.getValue("finishingViews"));
				order.add("finishingViews");

			}
			// 批准人审核
			else if (mark.equals(ConfigItemPartMapMark.taskAppPzhUser.toString())) {

				headers.add(name);
				order.add(mark);

			}
			// 各业务体系负责人
			else if (mark.equals(ConfigItemPartMapMark.taskAppOprtUser.toString())) {
				headers.add(name);
				order.add(mark);

			}
			// IT总监
			else if (mark.equals(ConfigItemPartMapMark.taskAppITUser.toString())) {
				headers.add(name);
				order.add(mark);

			}
			// 事业部经理
			else if (mark.equals(ConfigItemPartMapMark.taskAppDivisionManager.toString())) {
				headers.add(name);
				order.add(mark);

			}
			// 总经理
			else if (mark.equals(ConfigItemPartMapMark.taskAppGeneralManager.toString())) {
				headers.add(name);
				order.add(mark);

			}
		}
		headersBean.setHeaders(headers);
		headersBean.setOrder(order);

		return headersBean;
	}

	/**
	 * 流程清单报表-获得所有的要下载的数据
	 */
	@Override
	public ProcessDetailBean findProcessDetailBean(Map<String, Object> param) throws Exception {
		List<Long> orgIds = (List<Long>) param.get("orgIds");
		List<Long> mapIds = (List<Long>) param.get("mapIds");
		String endTime = (String) param.get("endTime");
		Long projectId = (Long) param.get("projectId");
		String startTime = (String) param.get("startTime");
		int type = (Integer) param.get("type");
		boolean orgHasChild = (Boolean) param.get("orgHasChild");

		String dutyOrgIds = concatIds(orgIds);
		String processMapIds = concatIds(mapIds);
		// 如果没有选择流程地图或者流程图则 dutyOrgIds=-1 processMapIds=-1
		// 流程清单总数预览类
		ProcessDetailBean detailBean = new ProcessDetailBean();
		// 每一个部门或者流程架构下的所有流程清单信息ProcessDetailBean
		// 包含List<ProcessDetailWebListBean>
		List<ProcessDetailWebListBean> allDetailWebListBeans = detailBean.getAllDetailWebListBeans();

		boolean guardianPeopleIsShow = false;
		if (type == 0) {
			// 获得配置文件 是否显示流程监护人
			guardianPeopleIsShow = JecnConfigTool.flowGuardianPeopleIsShow();
		} else {
			guardianPeopleIsShow = JecnConfigTool.ruleGuardianPeopleIsShow();
		}

		// 设置流程清单报表的结束时间
		detailBean.setEndTime(endTime);
		detailBean.setGuardianPeopleIsShow(guardianPeopleIsShow);

		boolean orgExist = idExist(dutyOrgIds);
		boolean mapExist = idExist(processMapIds);
		boolean bothExist = orgExist && mapExist;

		// 所有选中的责任部门下的流程图的集合
		List<ProcessDetailWebBean> orgWebBeans = new ArrayList<ProcessDetailWebBean>();
		// 选中的架构下的流程图的集合
		List<ProcessDetailWebBean> mapWebBeans = new ArrayList<ProcessDetailWebBean>();
		// 保证精度
		String endTimeAccurate = endTime + " 23:59:59";
		// 如果部门和架构都存在时，架构只是作为部门查询的筛选条件，所以需要取交集
		Set<Long> orgFlowIds = new HashSet<Long>();
		Set<Long> mapFlowIds = new HashSet<Long>();
		if (orgExist) {
			List<Object[]> processInfoList = null;
			if (type == 0) {
				processInfoList = this.flowStructureDao.getDutyOrgProcessesInfo(dutyOrgIds, projectId, endTimeAccurate,
						orgHasChild);
			} else {
				// 选中的所有责任部门下的流程图详细信息
				processInfoList = this.flowStructureDao.getDutyOrgRulesInfo(dutyOrgIds, projectId, endTimeAccurate,
						orgHasChild);
			}
			// 封装所有流程图为对象
			for (Object[] processInfo : processInfoList) {
				ProcessDetailWebBean detailWebBean = getDutyOrgProcessWebBeanByObj(processInfo);
				orgWebBeans.add(detailWebBean);
				orgFlowIds.add(detailWebBean.getFlowId());
			}
		}

		if (mapExist) {
			// 选中的所有流程地图下的流程图
			String[] processMapIdArrs = processMapIds.split(",");
			List<Object[]> mapProcessInfoList = null;
			if (type == 0) {
				mapProcessInfoList = this.flowStructureDao.getProcessMapProcessesInfo(processMapIdArrs, projectId,
						endTimeAccurate);
			} else {
				mapProcessInfoList = this.flowStructureDao
						.getDirRulesInfo(processMapIdArrs, projectId, endTimeAccurate);
			}
			// 封装所有流程图为对象
			for (Object[] processInfo : mapProcessInfoList) {
				ProcessDetailWebBean detailWebBean = getProcessMapProcessWebBeanByObj(processInfo);
				mapWebBeans.add(detailWebBean);
				mapFlowIds.add(detailWebBean.getFlowId());
			}
		}

		if (bothExist) {
			/** 发布数量 */
			int pubCount = 0;
			/** 未发布数量 */
			int notPubCount = 0;
			/** 发布总数量 */
			int allCount = 0;
			// 取orgWebBeans和mapWebBeans中的交集
			orgFlowIds.retainAll(mapFlowIds);

			List<ProcessDetailWebBean> orgWebBeansTemp = new ArrayList<ProcessDetailWebBean>();
			for (ProcessDetailWebBean detail : orgWebBeans) {
				if (orgFlowIds.contains(detail.getFlowId())) {
					orgWebBeansTemp.add(detail);
					if (detail.getTypeByData() == 2) {
						pubCount++;
					} else {
						notPubCount++;
					}
				}
			}
			orgWebBeans = orgWebBeansTemp;
			for (ProcessDetailWebBean detail : orgWebBeans) {
				detail.setPubCount(pubCount);
				detail.setNotPubCount(notPubCount);
				detail.setAllCount(allCount);
			}
		}

		// 责任部门
		if (orgExist) {

			List<Object[]> idAndNames = queryIdAndNameList(dutyOrgIds, 0);
			// key为流程图/流程架构的id value为他们的名称
			Map<Long, String> dutyOrgInfoMap = new LinkedHashMap<Long, String>();
			for (Object[] arr : idAndNames) {
				dutyOrgInfoMap.put(Long.valueOf(arr[0].toString()), arr[1].toString());
			}

			// 选中的责任部门下的流程与它的所有父流程地图的map key 流程地图的id
			Map<Long, ProcessDetailWebBean> processMaps = new HashMap<Long, ProcessDetailWebBean>();

			// 选中的责任部门下的流程与它的所有父流程地图的map
			List<Object[]> processAndMapLists = null;
			if (type == 0) {
				processAndMapLists = this.flowStructureDao.getProcessDetailDutyOrgList(dutyOrgIds, projectId,
						endTimeAccurate, orgHasChild);
			} else {
				processAndMapLists = this.flowStructureDao.getRulesDetailDutyOrgList(dutyOrgIds, projectId,
						endTimeAccurate, orgHasChild);
			}
			for (Object[] processAndMapInfo : processAndMapLists) {
				ProcessDetailWebBean webBean = getProcessAndMaplWebBeanByObj(processAndMapInfo);
				if (webBean == null) {
					continue;
				} else {
					processMaps.put(webBean.getFlowId(), webBean);
				}
			}

			// 选中的责任部门下的流程图集合的map key是责任部门的id
			Map<Long, ProcessDetailWebListBean> allInfos = new LinkedHashMap<Long, ProcessDetailWebListBean>();
			// 创建每一个选择的流程地图的对象
			for (Object[] arr : idAndNames) {
				Long orgId = Long.valueOf(arr[0].toString());
				// 流程责任部门的部门id,name,以及责任部门下的流程图的bean
				ProcessDetailWebListBean webListBean = new ProcessDetailWebListBean();
				// 部门id
				webListBean.setId(orgId);
				// 部门name
				webListBean.setName(dutyOrgInfoMap.get(orgId));
				/** 0为责任部门 1为流程地图 */
				webListBean.setType(0);

				allInfos.put(orgId, webListBean);

			}

			// 迭代获得每一个ProcessDetailWebBean的levelList
			for (ProcessDetailWebBean webBean : orgWebBeans) {

				// 责任部门的id
				long orgId = webBean.getMapId();
				// 获得
				ProcessDetailWebListBean webListBean = allInfos.get(orgId);
				// 设置levelList
				List<ProcessDetailLevel> levels = new ArrayList<ProcessDetailLevel>();
				// 获得父节点名称和流程编号的集合
				findProcessDetailLevel(processMaps, webBean.getParentFlowId(), levels);

				webBean.setProcessDetailLevelList(levels);

				// 将某个流程清单加入责任部门bean中的流程清单的集合中
				webListBean.getAllDetailWebBeans().add(webBean);

			}

			// 循环所有的流程图的信息 设置ProcessDetailBean的maxlevel和发布流程、未发布流程、流程总数
			for (Entry<Long, ProcessDetailWebListBean> entry : allInfos.entrySet()) {
				ProcessDetailWebListBean webListBean = entry.getValue();
				List<ProcessDetailWebBean> allDetailWebBeans = webListBean.getAllDetailWebBeans();
				// 某一个流程地图下可能没有流程图，跳出继续循环
				if (allDetailWebBeans.size() == 0) {
					// 将webListBean加入
					allDetailWebListBeans.add(webListBean);
					continue;
				}
				int maxLevel = 0;
				// 循环其中一个部门下的所有流程图 获得levle的最大值
				for (ProcessDetailWebBean detailWebBean : allDetailWebBeans) {
					int level = detailWebBean.getProcessDetailLevelList().size();
					if (level > maxLevel) {
						maxLevel = level;
					}

				}
				// 每个责任部门下最深的深度
				webListBean.setMaxLevel(maxLevel);

				ProcessDetailWebBean processDetailWebBean = webListBean.getAllDetailWebBeans().get(0);
				// 发布数量,未发布数量,百分比
				if (processDetailWebBean != null) {
					int pubCount = processDetailWebBean.getPubCount();
					int notPubCount = processDetailWebBean.getNotPubCount();
					int allCount = pubCount + notPubCount;
					webListBean.setPubCount(pubCount);
					webListBean.setNotPubCount(notPubCount);
					webListBean.setAllCount(allCount);
					if (allCount >= pubCount && allCount > 0) {
						webListBean.setPubPercent((float) pubCount / allCount);
					} else {
						webListBean.setPubPercent(0);
					}

				}
				// 将每一个责任部门下流程图的集合bean加入list中
				allDetailWebListBeans.add(webListBean);
			}

		}

		if (!bothExist && mapExist) {

			// 选中的流程图的名称集合 key为id
			Map<Long, String> processMapMap = new LinkedHashMap<Long, String>();
			List<Object[]> idAndNames = queryIdAndNameList(processMapIds, type == 0 ? 1 : 2);
			for (Object[] arr : idAndNames) {
				processMapMap.put(Long.valueOf(arr[0].toString()), arr[1].toString());

			}

			// 流程图的map key为流程id value为流程清单bean
			Map<Long, ProcessDetailWebBean> processMaps = new HashMap<Long, ProcessDetailWebBean>();

			// 获得所有流程与父节点,将它们加入map
			List<Object[]> processAndMapLists = null;
			if (type == 0) {
				processAndMapLists = this.flowStructureDao.getProcessDetailProcessMapList(processMapIds, projectId);
			} else {
				processAndMapLists = this.flowStructureDao.getRuleDetailDirList(processMapIds, projectId);
			}

			for (Object[] processAndMapInfo : processAndMapLists) {
				ProcessDetailWebBean webBean = getProcessAndMaplWebBeanByObj(processAndMapInfo);
				if (webBean == null) {
					continue;
				} else {
					processMaps.put(webBean.getFlowId(), webBean);
				}
			}

			// 流程图的map key 流程架构的id value为该流程架构下的流程图的集合bean
			Map<Long, ProcessDetailWebListBean> allInfos = new LinkedHashMap<Long, ProcessDetailWebListBean>();
			// 创建每一个选择的流程地图的对象
			for (Object[] arr : idAndNames) {
				Long mapId = Long.valueOf(arr[0].toString());
				// 设置值
				ProcessDetailWebListBean webListBean = new ProcessDetailWebListBean();
				webListBean.setId(mapId);
				webListBean.setName(processMapMap.get(mapId));
				/** 0为责任部门 1为流程地图 */
				webListBean.setType(1);

				allInfos.put(mapId, webListBean);
			}

			// 迭代获得每一个ProcessDetailWebBean的levelList
			for (ProcessDetailWebBean webBean : mapWebBeans) {
				// 设置levelList
				List<ProcessDetailLevel> levels = new ArrayList<ProcessDetailLevel>();
				// 获得父节点名称和流程编号的集合
				findProcessDetailLevel(processMaps, webBean.getParentFlowId(), levels);
				webBean.setProcessDetailLevelList(levels);

				long mapId = webBean.getMapId();
				ProcessDetailWebListBean processDetailBean = allInfos.get(mapId);

				List<ProcessDetailWebBean> allDetailWebBeans = processDetailBean.getAllDetailWebBeans();
				allDetailWebBeans.add(webBean);

			}

			// 循环所有的流程图的信息 设置ProcessDetailBean的maxlevel和发布流程、未发布流程、流程总数
			for (Entry<Long, ProcessDetailWebListBean> entry : allInfos.entrySet()) {
				ProcessDetailWebListBean webListBean = entry.getValue();
				List<ProcessDetailWebBean> allDetailWebBeans = webListBean.getAllDetailWebBeans();
				// 有些流程架构下在选中的时间段内没有流程图
				if (0 == allDetailWebBeans.size()) {
					// 将webListBean加入
					allDetailWebListBeans.add(webListBean);
					continue;
				}
				int maxLevel = 0;
				// 循环其中一个部门下的所有流程图 获得levle的最大值
				for (ProcessDetailWebBean detailWebBean : allDetailWebBeans) {
					int level = detailWebBean.getProcessDetailLevelList().size();
					if (level > maxLevel) {
						maxLevel = level;
					}

				}
				// 每个责任部门下最深的深度
				webListBean.setMaxLevel(maxLevel);

				ProcessDetailWebBean processDetailWebBean = webListBean.getAllDetailWebBeans().get(0);
				// 发布数量,未发布数量,百分比
				if (processDetailWebBean != null) {
					int pubCount = processDetailWebBean.getPubCount();
					int notPubCount = processDetailWebBean.getNotPubCount();
					int allCount = pubCount + notPubCount;
					webListBean.setPubCount(pubCount);
					webListBean.setNotPubCount(notPubCount);
					webListBean.setAllCount(allCount);
					if (allCount >= pubCount && allCount > 0) {
						webListBean.setPubPercent((float) pubCount / allCount);
					} else {
						webListBean.setPubPercent(0);
					}
				}

				// 将webListBean加入
				allDetailWebListBeans.add(webListBean);
			}
		}
		return detailBean;
	}

	private boolean idExist(String ids) {
		return !JecnCommon.isNullOrEmtryTrim(ids) && !ids.contains("-1");
	}

	/**
	 * 查询责任部门下下的流程/制度的审视优化报表
	 * 
	 * @param dutyOrgIds责任部门ids
	 *            使用,分隔
	 * @param dutyOrgNames责任部门names
	 *            使用 \r\n分隔
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @param projectId
	 *            项目id
	 * @return
	 */
	@Override
	public ProcessScanOptimizeBean findProcessScanOptimizeBean(Map<String, Object> param) throws Exception {
		List<Long> orgIds = (List<Long>) param.get("orgIds");
		List<Long> mapIds = (List<Long>) param.get("mapIds");
		List<Long> dutyIds = (List<Long>) param.get("dutyIds");
		String endTime = (String) param.get("endTime");
		Long projectId = (Long) param.get("projectId");
		String startTime = (String) param.get("startTime");
		int type = (Integer) param.get("type");
		boolean orgHasChild = (Boolean) param.get("orgHasChild");

		String dutyOrgIds = concatIds(orgIds);
		String processMapIds = concatIds(mapIds);
		String processDutyIds = concatIds(dutyIds);
		// 责任部门与流程架构的公共信息
		// 开始月份的第一天
		String startTimeAccurate = getMonthFirstDay(startTime);
		// 结束月份的最后一天
		String endTimeAccurate = getMonthLastDay(endTime);

		Date startDate = JecnCommon.getDateByString(endTime);
		Date endDate = JecnCommon.getDateByString(endTime);
		Calendar calender = Calendar.getInstance();
		calender.setTime(startDate);
		calender.add(Calendar.MONTH, 1);
		// 下两月审视开始时间第一天
		String nextScanStartTime = getMonthFirstDay(JecnCommon.getStringbyDate(calender.getTime()));
		calender.clear();
		calender.setTime(endDate);
		calender.add(Calendar.MONTH, 2);
		// 下两月审视结束时间 最后一天
		String nextScanEndTime = getMonthLastDay(JecnCommon.getStringbyDate(calender.getTime()));// 这个月的最后一天

		// 流程审视优化统计bean
		ProcessScanOptimizeBean scanOptimizeBean = new ProcessScanOptimizeBean();
		scanOptimizeBean.setStartTime(startTime);
		scanOptimizeBean.setEndTime(endTime);

		// 获得配置文件 是否显示流程监护人
		boolean guardianPeopleIsShow = guardianPeopleIsShow(type);
		// 是否显示流程监护人
		scanOptimizeBean.setGuardianIsShow(guardianPeopleIsShow);

		boolean orgExist = idExist(dutyOrgIds);
		boolean mapExist = idExist(processMapIds);
		boolean bothExist = orgExist && mapExist;

		// 流程责任部门下的流程审视清单
		if (orgExist) {
			List<ProcessScanWebBean> processScanList = new ArrayList<ProcessScanWebBean>();
			List<Object[]> scanObjs = null;
			if (type == 0) {
				scanObjs = this.flowStructureDao.getAllProcessScanOptimizeList(dutyOrgIds, startTimeAccurate,
						endTimeAccurate, projectId, processMapIds, processDutyIds, orgHasChild);
			} else {
				scanObjs = this.flowStructureDao.getAllRulesScanOptimizeList(dutyOrgIds, startTimeAccurate,
						endTimeAccurate, projectId, processMapIds, processDutyIds, orgHasChild);
			}

			for (Object[] obj : scanObjs) {
				ProcessScanWebBean processScanBean = getProcessScanWebBeanByObj(obj);
				processScanList.add(processScanBean);
			}

			List<Object[]> idAndNames = queryIdAndNameList(dutyOrgIds, 0);
			// 流程审视bean的map key为部门id value为流程审视bean的集合
			Map<Long, ProcessScanWebListBean> processScanOptimizeMap = getProcessScanOptimizeBeanByWebBean(idAndNames,
					processScanList);
			List<Object[]> needScanObjs = null;
			// 获得下两月待审视的流程数量 0为责任部门的id 1为数量
			if (type == 0) {
				needScanObjs = this.flowStructureDao.getNextTwoMonthScanList(dutyOrgIds, nextScanStartTime,
						nextScanEndTime, projectId, processMapIds, processDutyIds, orgHasChild);
			} else {
				needScanObjs = this.flowStructureDao.getRuleNextTwoMonthScanList(dutyOrgIds, nextScanStartTime,
						nextScanEndTime, projectId, processMapIds, processDutyIds, orgHasChild);
			}

			// 0 为数量 1为部门id
			for (Object[] obj : needScanObjs) {
				if (obj[0] == null || obj[1] == null) {
					continue;
				}

				processScanOptimizeMap.get(Long.valueOf(obj[1].toString())).setNextTimeScanCount(
						Integer.valueOf(obj[0].toString()));
			}
			List<ProcessScanWebListBean> processScanOptimizeList = scanOptimizeBean.getAllScanWebListBeans();

			// 将map中的value添加到list中
			processScanOptimizeList.addAll(processScanOptimizeMap.values());

		}

		if (!bothExist && mapExist) {
			int nameType = 1;
			if (type == 2) {
				nameType = 2;
			}
			List<Object[]> idAndNames = queryIdAndNameList(processMapIds, nameType);
			List<ProcessScanWebBean> processScanList = new ArrayList<ProcessScanWebBean>();
			List<Object[]> scanObjs = null;
			if (type == 0) {
				scanObjs = this.flowStructureDao.getAllMapProcessScanOptimizeList(idAndNames, startTimeAccurate,
						endTimeAccurate, projectId, processDutyIds);
			} else {
				scanObjs = this.flowStructureDao.getRuleDirProcessScanOptimizeList(processMapIds, startTimeAccurate,
						endTimeAccurate, projectId, processDutyIds);
			}
			for (Object[] obj : scanObjs) {
				ProcessScanWebBean processScanBean = getProcessScanWebBeanByObj(obj);
				processScanList.add(processScanBean);
			}

			// 流程审视bean的map key为流程架构id value为流程审视bean的集合
			Map<Long, ProcessScanWebListBean> processScanOptimizeMap = getProcessScanOptimizeBeanByWebBean(idAndNames,
					processScanList);
			List<Object[]> needScanObjs = null;
			if (type == 0) {
				needScanObjs = this.flowStructureDao.getMapNextTwoMonthScanList(processMapIds, nextScanStartTime,
						nextScanEndTime, projectId, processDutyIds);
			} else {
				// 获得下两月待审视的流程数量 0为责任部门的id 1为数量
				needScanObjs = this.flowStructureDao.getRuleDirNextTwoMonthScanList(processMapIds, nextScanStartTime,
						nextScanEndTime, projectId, processDutyIds);
			}

			// 0 为数量 1为流程架构的id
			for (Object[] obj : needScanObjs) {
				if (obj[0] == null || obj[1] == null) {
					continue;
				}

				processScanOptimizeMap.get(Long.valueOf(obj[1].toString())).setNextTimeScanCount(
						Integer.valueOf(obj[0].toString()));
			}
			List<ProcessScanWebListBean> processScanOptimizeList = scanOptimizeBean.getAllScanWebListBeans();

			// 将map中的value添加到list中
			processScanOptimizeList.addAll(processScanOptimizeMap.values());

		}

		return scanOptimizeBean;
	}

	private String concatIds(List<Long> ids) {
		if (ids == null || ids.size() == 0) {
			return "";
		}
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < ids.size(); i++) {
			if (i == 0) {
				buf.append(ids.get(i));
			} else {
				buf.append("," + ids.get(i));
			}
		}
		return buf.toString();
	}

	private boolean guardianPeopleIsShow(int type) {
		if (type == 0) {
			return JecnConfigTool.flowGuardianPeopleIsShow();
		} else {
			return JecnConfigTool.ruleGuardianPeopleIsShow();
		}
	}

	/**
	 * 获得某个月份的第一天
	 * 
	 * @param startTime
	 *            字符串的日期
	 * @return 类似于2014-02-01 00:00:01
	 */
	private String getMonthFirstDay(String startTime) {
		// 2014-02-23
		startTime = startTime.substring(0, 8);
		startTime = startTime + "01 00:00:01";
		return startTime;
	}

	/**
	 * 
	 * @param endTime
	 *            字符串的日期
	 * @return 类似于2014-02-28 23:59:59
	 * @throws ParseException
	 */
	private String getMonthLastDay(String endTime) throws ParseException {
		// 2014-02-23
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(format.parse(endTime));
		int value = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		cal.set(Calendar.DAY_OF_MONTH, value);
		endTime = format.format(cal.getTime()) + " 23:59:59";

		return endTime;
	}

	/**
	 * 获得流程审视的map
	 * 
	 * @param orgIds
	 *            部门的ids 使用逗号分隔
	 * @param dutyOrgNames
	 *            部门的名称使用 \r\n分隔
	 * @param processScanList
	 *            流程审视的webBean
	 * @return
	 */
	private Map<Long, ProcessScanWebListBean> getProcessScanOptimizeBeanByWebBean(List<Object[]> idAndNames,
			List<ProcessScanWebBean> processScanList) {

		Map<Long, ProcessScanWebListBean> scanMap = new LinkedHashMap<Long, ProcessScanWebListBean>();
		for (Object[] arr : idAndNames) {
			ProcessScanWebListBean scanBean = new ProcessScanWebListBean();
			scanBean.setId(Long.valueOf(arr[0].toString()).longValue());
			scanBean.setName(arr[1].toString());
			scanMap.put(Long.valueOf(arr[0].toString()), scanBean);
		}

		// 将各部门下的流程，各个流程的审视记录分类
		for (ProcessScanWebBean scanWebBean : processScanList) {
			// 某个部门/流程架构
			long id = scanWebBean.getId();
			ProcessScanWebListBean scanWebList = scanMap.get(id);
			scanWebList.setAllScanCount(scanWebBean.getAllScanCount());
			scanWebList.setScanCount(scanWebBean.getScanCount());
			scanWebList.setScanPercent(scanWebBean.getScanPercent());

			// 将这个审视记录加入到ProcessScanWebListBean中
			addScanToProcessScanOptimizeBean(scanWebList, scanWebBean);
		}

		return scanMap;

	}

	private void addScanToProcessScanOptimizeBean(ProcessScanWebListBean scanWebListBean, ProcessScanWebBean scanWebBean) {
		// 某一个部门下的所有的审视信息（包含不同的流程）
		List<ProcessScanWebBean> allScanWebBeans = scanWebListBean.getAllScanWebBeans();
		long flowId = scanWebBean.getFlowId();
		String flowVersionId = scanWebBean.getVersionId();
		String orgName = scanWebBean.getGuardianOrgName();
		// 流程审视记录为空
		if (allScanWebBeans.size() == 0) {
			allScanWebBeans.add(scanWebBean);

		} else {// 流程审视记录不为空
			// 这条审视记录是否存在于该部门下的记录表中
			boolean isExist = false;
			for (ProcessScanWebBean sacnBean : allScanWebBeans) {
				String guardianOrgName = sacnBean.getGuardianOrgName();
				// 是同一个流程
				if (flowId == sacnBean.getFlowId()) {
					// 是同一个流程下的同一个版本
					if (!StringUtils.isEmpty(flowVersionId) && flowVersionId.equals(sacnBean.getVersionId())// 判断版本是否相同
							&& !StringUtils.isEmpty(guardianOrgName) && !guardianOrgName.contains(orgName))// 监护人的部门是否有重复

					{
						isExist = true;
						sacnBean.setGuardianOrgName(guardianOrgName + "/" + orgName);
					}
				}

			}

			if (!isExist) {
				allScanWebBeans.add(scanWebBean);
			}

		}

	}

	/***
	 * 将Object转换为流程审视优化webBean
	 * 
	 * @param Object
	 *            []
	 * @return ProcessScanWebBean
	 */
	private ProcessScanWebBean getProcessScanWebBeanByObj(Object[] obj) {
		ProcessScanWebBean processScanBean = new ProcessScanWebBean();

		// 需完成的月份（计划）
		if (obj[0] != null) {

			processScanBean.setNeedFinishDate(trimRemain(obj[0], 10));
		}
		// 需审视流程
		if (obj[1] != null && obj[2] != null) {
			String temp = obj[1].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setFlowId(Long.valueOf(temp));
			}

			processScanBean.setFlowName(obj[2].toString());
		}
		// 版本
		if (obj[3] != null) {

			processScanBean.setVersionId(obj[3].toString());
		}
		// 需审视流程发布时间
		if (obj[4] != null) {

			processScanBean.setNeedScanPubDate(trimRemain(obj[4], 10));
		}
		// 拟稿人
		if (obj[5] != null) {

			processScanBean.setDraftPerson(obj[5].toString());
		}
		// 责任部门 id 责任部门 name/流程架构id name
		if (obj[6] != null && obj[7] != null) {
			String temp = obj[6].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setId(Long.valueOf(temp));

			}

			processScanBean.setName(obj[7].toString());
		}
		// 流程责任人
		if (obj[8] != null) {
			processScanBean.setResPeopleName(obj[8].toString());
		}
		// 监护人所属部门
		if (obj[9] != null && obj[10] != null) {
			String temp = obj[9].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setGuardianOrgId(Long.valueOf(temp));
			}

			processScanBean.setGuardianOrgName(obj[10].toString());
		}
		// 监护人
		if (obj[11] != null && obj[12] != null) {
			String temp = obj[11].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setGuardianId(Long.valueOf(temp));
			}
			processScanBean.setGuardianName(obj[12].toString());

		}
		// 当前发布时间
		if (obj[13] != null) {

			processScanBean.setCurScanPubDate(trimRemain(obj[13], 10));
		}
		// 按时完成审视优化数量
		if (obj[14] != null) {
			String temp = obj[14].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setScanCount(Integer.valueOf(temp));
			}
		}
		// 需审视流程数量 所有的
		if (obj[15] != null) {
			String temp = obj[15].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setAllScanCount(Integer.valueOf(temp));
			}
		}
		// 审视优化及时完成率
		if (obj[16] != null) {
			String temp = obj[16].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setScanPercent(Integer.valueOf(temp));
			}
		}
		// 是否及时优化
		if (obj[17] != null) {
			String temp = obj[17].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setOptimization(Integer.valueOf(temp));
			}
		}

		// 责任部门
		if (obj[18] != null) {
			String temp = obj[18].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setDutyName(temp);
			}
		}

		// 变更说明
		if (obj[19] != null) {
			String temp = obj[19].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processScanBean.setModifyExplain(temp);
			}
		}
		if (obj[20] != null && obj[21] != null) {
			processScanBean.setTrueOrgId(Long.valueOf(obj[20].toString()));
			processScanBean.setTrueOrgName(obj[21].toString());
		}

		return processScanBean;
	}

	public String trimRemain(Object obj, int remainLen) {
		return JecnUtil.trimRemain(obj, remainLen);
	}

	/**
	 * 迭代循环获得父节点的名称和流程编号
	 * 
	 * @param processAndMapInfo
	 *            0流程id 1流程名称 2父节点 3流程编号 4是否是流程
	 */
	private ProcessDetailWebBean getProcessAndMaplWebBeanByObj(Object[] processAndMapInfo) {

		if (processAndMapInfo[0] == null || processAndMapInfo[1] == null || processAndMapInfo[2] == null
				|| processAndMapInfo[4] == null) {
			return null;
		}
		ProcessDetailWebBean webBean = new ProcessDetailWebBean();
		webBean.setFlowId(Long.parseLong(processAndMapInfo[0].toString()));
		webBean.setFlowName(processAndMapInfo[1].toString());
		webBean.setParentFlowId(Long.parseLong(processAndMapInfo[2].toString()));
		if (processAndMapInfo[3] != null) {
			webBean.setFlowIdInput(processAndMapInfo[3].toString());
		}
		// 是否为流程
		webBean.setIsFlow(Integer.valueOf(processAndMapInfo[4].toString()));
		return webBean;
	}

	/**
	 * 迭代循环获得父节点的名称和流程编号
	 * 
	 * @param processMaps
	 *            流程地图的集合
	 * @param parentId
	 *            父节点id
	 * @param levels
	 *            节点父节点的名称和流程编号的集合
	 */
	private void findProcessDetailLevel(Map<Long, ProcessDetailWebBean> processMaps, Long parentId,
			List<ProcessDetailLevel> levels) {

		ProcessDetailWebBean processDetailWebBean = processMaps.get(parentId);
		if (processDetailWebBean != null) {
			// 父节点如果是流程图不算级别
			if (0 == processDetailWebBean.getIsFlow())// 如果是流程地图
			{
				ProcessDetailLevel detailLeavel = new ProcessDetailLevel();
				detailLeavel.setLevelName(processDetailWebBean.getFlowName());
				detailLeavel.setLevelNum(processDetailWebBean.getFlowIdInput());
				levels.add(detailLeavel);
			}
			parentId = processDetailWebBean.getParentFlowId();
			findProcessDetailLevel(processMaps, parentId, levels);
		}

	}

	/**
	 * 通过对象获得责任人下的ProcessWebBean
	 * 
	 * @param obj
	 * @return
	 */
	private ProcessDetailWebBean getDutyOrgProcessWebBeanByObj(Object[] obj) {

		ProcessDetailWebBean processWebBean = new ProcessDetailWebBean();
		// 流程ID
		processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
		// 父节点ID
		if (obj[1] != null) {
			String temp = obj[1].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setParentFlowId(Long.valueOf(temp));
			}

		}

		// 流程名称
		if (obj[2] != null)
			processWebBean.setFlowName(obj[2].toString());
		// 流程编号
		if (obj[3] != null)
			processWebBean.setFlowIdInput(obj[3].toString());
		// 当前版本号
		if (obj[5] != null)
			processWebBean.setVersionId(obj[5].toString());
		// 责任部门
		if (obj[6] != null && obj[7] != null) {
			processWebBean.setOrgName(obj[6].toString());
			String temp = obj[7].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setOrgId(Long.valueOf(temp));
			}
		}
		// 责任人类型
		if (obj[8] != null && obj[9] != null && obj[10] != null) {

			// 责任人类型
			String temp = obj[8].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setTypeResPeople(Integer.parseInt(temp));
			}

			// 责任人ID
			temp = obj[9].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setResPeopleId(Long.valueOf(temp));
			}

			// 责任人名称
			processWebBean.setResPeopleName(obj[10].toString());
		}

		// 流程发布时间
		if (obj[4] != null) {
			String temp = obj[4].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(temp)));
			}
			// 发布
			processWebBean.setTypeByData(2);

		} else {
			// 未发布
			processWebBean.setTypeByData(1);

		}

		// 监护人ID
		if (obj[12] != null) {
			String temp = obj[12].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setGuardianId(Long.valueOf(temp));
			}

		}
		// 监护人名称
		if (obj[13] != null) {
			processWebBean.setGuardianName(obj[13].toString());
		} else {
			processWebBean.setGuardianName("");
		}
		// 拟稿人
		if (obj[14] != null) {
			processWebBean.setDraftPerson(obj[14].toString());
		} else {
			processWebBean.setDraftPerson("");
		}
		// 有效期
		if (obj[15] != null) {
			String temp = obj[15].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setExpiry(Integer.valueOf(temp));
			}

		}

		// 责任部门id
		if (obj[16] != null) {
			String temp = obj[16].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setMapId(Long.valueOf(temp));
			}

		}
		// 发布数量
		if (obj[17] != null) {
			String temp = obj[17].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setPubCount(Integer.valueOf(temp));
			}

		}
		// 未发布数量
		if (obj[18] != null) {
			String temp = obj[18].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setNotPubCount(Integer.valueOf(temp));
			}
		}

		// 是否及时优化 0空 ,1是 ,2否
		if (obj[19] != null) {
			String temp = obj[19].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setOptimization(Integer.valueOf(temp));
			}

		}

		// 下次审视时间
		if (obj[20] != null) {
			processWebBean.setNextScanData(JecnUtil.trimRemain(JecnUtil.nullToEmpty(obj[20]), 10));
		}

		if (obj[21] != null && obj[22] != null) {
			processWebBean.setTrueOrgId(Long.valueOf(obj[21].toString()));
			processWebBean.setTrueOrgName(obj[22].toString());
		}

		return processWebBean;
	}

	/**
	 * 通过对象获得ProcessWebBean
	 * 
	 * @param obj
	 * @return
	 */
	private ProcessDetailWebBean getProcessMapProcessWebBeanByObj(Object[] obj) {

		ProcessDetailWebBean processWebBean = new ProcessDetailWebBean();
		// 流程ID
		processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
		// 父节点ID
		if (obj[1] != null) {
			String temp = obj[1].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setParentFlowId(Long.valueOf(temp));

			}

		}

		// 流程名称
		if (obj[2] != null)
			processWebBean.setFlowName(obj[2].toString());
		// 流程编号
		if (obj[3] != null)
			processWebBean.setFlowIdInput(obj[3].toString());

		if (obj[4] != null) {
			String temp = obj[4].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setPubDate(JecnCommon.getStringbyDate(JecnCommon.getDateByString(temp)));
			}

			// 发布
			processWebBean.setTypeByData(2);

		} else {
			// 未发布
			processWebBean.setTypeByData(1);

		}

		// 当前版本号
		if (obj[5] != null)
			processWebBean.setVersionId(obj[5].toString());
		// 责任部门
		if (obj[6] != null && obj[7] != null) {
			processWebBean.setOrgName(obj[6].toString());
			String temp = obj[7].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setOrgId(Long.valueOf(temp));
			}

		}
		// 责任人类型
		if (obj[8] != null && obj[9] != null && obj[10] != null) {

			// 责任人类型
			String temp = obj[8].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setTypeResPeople(Integer.parseInt(temp));
			}

			// 责任人ID
			temp = obj[9].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setResPeopleId(Long.valueOf(temp));
			}
			// 责任人名称
			processWebBean.setResPeopleName(obj[10].toString());
		}

		// 监护人ID
		if (obj[12] != null) {
			String temp = obj[12].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setGuardianId(Long.valueOf(temp));
			}

		}
		// 监护人名称
		if (obj[13] != null) {
			processWebBean.setGuardianName(obj[13].toString());
		} else {
			processWebBean.setGuardianName("");
		}
		// 拟稿人
		if (obj[14] != null) {
			processWebBean.setDraftPerson(obj[14].toString());
		} else {
			processWebBean.setDraftPerson("");
		}
		// 有效期
		if (obj[15] != null) {
			String temp = obj[15].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setExpiry(Integer.valueOf(temp));
			}

		} else {
			processWebBean.setExpiry(0);

		}

		// 流程地图id
		if (obj[16] != null) {
			String temp = obj[16].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setMapId(Long.valueOf(temp));
			}

		}
		// 发布数量
		if (obj[17] != null) {
			String temp = obj[17].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setPubCount(Integer.valueOf(temp));
			}

		}
		// 未发布数量
		if (obj[18] != null) {
			String temp = obj[18].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setNotPubCount(Integer.valueOf(temp));
			}

		}

		if (obj[19] != null) {
			String temp = obj[19].toString();
			if (!JecnCommon.isNullOrEmtryTrim(temp)) {
				processWebBean.setOptimization(Integer.valueOf(temp));
			}
		}

		// 下次审视时间
		if (obj[20] != null) {
			processWebBean.setNextScanData(JecnUtil.trimRemain(JecnUtil.nullToEmpty(obj[20]), 10));
		}

		return processWebBean;
	}

	/**
	 * 流程kpi跟踪表数据源
	 */
	@Override
	public ProcessKPIFollowBean findProcessKPIFollowBean(String dutyOrgIds, String processMapIds, String startTime,
			String endTime, long projectId) throws Exception {

		// 流程拟制人与流程监护人判断
		boolean guardianPeopleIsShow = JecnConfigTool.flowGuardianPeopleIsShow();
		// 获得配置文件 是否显示流程监护人
		boolean fictionPeople = JecnConfigTool.fictionPeopleIsShow();
		// 指标来源是否显示支持一级指标是否显示相关度三属性是否显示
		boolean kpiPropertyIsShow = JecnConfigTool.kpiPropertyIsShowIsShow();

		// 流程kpi报表bean
		ProcessKPIFollowBean kpiFlowBean = new ProcessKPIFollowBean();
		kpiFlowBean.setKpiPropertyIsShow(kpiPropertyIsShow);
		kpiFlowBean.setGuardianPeopleIsShow(guardianPeopleIsShow);
		kpiFlowBean.setArtificialPersonIsShow(fictionPeople);

		List<JecnConfigItemBean> showConfig = configItemDao.selectShowSortConfigItemBeanByType(1, 16);
		kpiFlowBean.setConfigList(showConfig);

		// 选中流程责任人
		if (!JecnCommon.isNullOrEmtryTrim(dutyOrgIds) && !dutyOrgIds.contains("-1")) {
			// kpi
			List<Object[]> kpiObjects = this.flowStructureDao.getProcessKPIFollowList(dutyOrgIds, projectId, startTime,
					endTime);
			List<Object[]> idAndNames = queryIdAndNameList(dutyOrgIds, 0);
			getProcessKPIFollowBean(kpiFlowBean, idAndNames, kpiObjects);

		}
		// 选中流程架构
		if (!JecnCommon.isNullOrEmtryTrim(processMapIds) && !processMapIds.contains("-1")) {
			// 选中的流程架构下的数据源
			List<Object[]> kpiObjects = this.flowStructureDao.getMapProcessKPIFollowList(processMapIds, projectId,
					startTime, endTime);
			List<Object[]> idAndNames = queryIdAndNameList(processMapIds, 1);
			getProcessKPIFollowBean(kpiFlowBean, idAndNames, kpiObjects);

		}

		return kpiFlowBean;
	}

	/**
	 * 封装流程kpi报表
	 * 
	 * @author huoyl
	 * @param kpiFlowBean
	 *            ProcessKPIFollowBean 流程kpi报表bean
	 * @param ids
	 *            String 责任部门/流程架构的ids 使用,分隔
	 * @param names
	 *            String 责任部门/流程架构的names 使用\r\n分隔
	 * @param kpiObjects
	 *            List<Object[]> kpi数据源
	 */
	private void getProcessKPIFollowBean(ProcessKPIFollowBean kpiFlowBean, List<Object[]> idAndNames,
			List<Object[]> kpiObjects) {

		// 流程kpi责任部门下kpi的集合
		List<ProcessKPIWebListBean> kPIWebListBean = kpiFlowBean.getkPIWebListBean();

		Map<Long, ProcessKPIWebListBean> kpiWebListMaps = new LinkedHashMap<Long, ProcessKPIWebListBean>();
		for (Object[] arr : idAndNames) {
			// 责任部门的kpi
			ProcessKPIWebListBean bean = new ProcessKPIWebListBean();
			Long id = Long.valueOf(arr[0].toString());
			bean.setId(id);
			bean.setName(arr[1].toString());
			kpiWebListMaps.put(id, bean);
		}

		// 临时责任部门/流程架构ID
		long tmpId = 0l;
		// 临时KPI主键ID
		long tmpKpiId = 0l;
		// 临时KPI值主键ID
		long tmpKpiValueId = 0l;

		// 所有部门KPI数据

		// 部门下KPI数据
		ProcessKPIWebListBean webListBean = null;
		// 一个KPIbean
		ProcessKPIWebBean processKPIWebBean = null;

		for (Object[] kpiObject : kpiObjects) {
			String kpiPurpose = "";
			String kpiPoint = "";
			String kpiPeriod = "";
			String kpiExplain = "";
			String kpiDefinition = "";
			String kpiType = "";

			// 流程名称（或关键过程）
			String flowName = "";
			if (kpiObject[0] != null) {
				flowName = String.valueOf(kpiObject[0]);
			}
			// 流程责任部门
			String orgName = "";
			if (kpiObject[1] != null) {

				orgName = String.valueOf(kpiObject[1]);
			}

			// 流程责任人
			String resPeopleName = "";
			if (kpiObject[2] != null) {
				resPeopleName = String.valueOf(kpiObject[2]);
			}
			// 流程责任人是岗位时，岗位ID--此项留以后用户需要显示岗位对应人员时使用
			// 流程监护人
			String wardPeopleName = "";
			if (kpiObject[4] != null) {
				wardPeopleName = String.valueOf(kpiObject[4]);
			}

			// 流程拟制人
			String draftPersonName = "";
			if (kpiObject[5] != null) {
				draftPersonName = String.valueOf(kpiObject[5]);
			}

			// 流程KPI名称
			String kpiName = "";
			if (kpiObject[6] != null) {
				kpiName = String.valueOf(kpiObject[6]);
			}

			// 指标来源 0:二级指标 1:个人PBC指标 2:其他重要工作
			int kpiTargetType = -1;
			if (kpiObject[7] != null) {
				String kpiTargetTypeTemp = kpiObject[7].toString();
				if (!JecnCommon.isNullOrEmtryTrim(kpiTargetTypeTemp)) {
					kpiTargetType = Integer.valueOf(kpiTargetTypeTemp);
				}

			}

			// 支撑的一级指标
			String kpiFirstTarge = "";
			if (kpiObject[8] != null) {
				kpiFirstTarge = String.valueOf(kpiObject[8]);
			}

			// 相关度 0：强相关 1：弱相关 2：不相关
			int kpiRelevance = -1;
			if (kpiObject[9] != null) {
				String kpiRelevanceTemp = kpiObject[9].toString();
				if (!JecnCommon.isNullOrEmtryTrim(kpiRelevanceTemp)) {
					kpiRelevance = Integer.valueOf(kpiRelevanceTemp);
				}

			}

			// 流程KPI计算公式
			String kpiStatisticalMethods = "";
			if (kpiObject[10] != null) {
				kpiStatisticalMethods = String.valueOf(kpiObject[10]);
			}

			// 流程KPI目标值 KPI目标值比较符号 0：= 1：> 2：< 3:>= 4:<=
			int kpiTargetOperator = -1;
			if (kpiObject[11] != null) {
				String kpiTargetOperatorTemp = String.valueOf(kpiObject[11]);
				if (!JecnCommon.isNullOrEmtryTrim(kpiTargetOperatorTemp)) {
					kpiTargetOperator = Integer.valueOf(kpiTargetOperatorTemp);
				}

			}

			// 流程KPI目标值：值
			double kpiTarget = -1;
			if (kpiObject[12] != null) {
				String kpiTargetTemp = String.valueOf(kpiObject[12]);
				if (!JecnCommon.isNullOrEmtryTrim(kpiTargetTemp)) {
					kpiTarget = Double.valueOf(kpiTargetTemp).doubleValue();
				}

			}

			// 流程kpi目标值:KPI值单位名称(纵向单位) 0:百分比 1:季度 2:月 3:周 4:工作日 5:天 6:小时 7:分钟
			// 8:个 9:人 10:ppm 11:元、12：次
			int kpiVertical = -1;
			if (kpiObject[13] != null) {
				String kpiVerticalTemp = kpiObject[13].toString();
				if (!JecnCommon.isNullOrEmtryTrim(kpiVerticalTemp)) {
					kpiVertical = Integer.valueOf(kpiVerticalTemp);
				}

			}

			// 数据获取方式 0:人工 1：系统
			int kpiDataMethod = -1;
			if (kpiObject[14] != null) {
				String kpiDataMethodTemp = kpiObject[14].toString();
				if (!JecnCommon.isNullOrEmtryTrim(kpiDataMethodTemp)) {
					kpiDataMethod = Integer.valueOf(kpiDataMethodTemp);
				}

			}
			// IT系统
			String itSystem = "";
			if (kpiObject[15] != null) {
				itSystem = String.valueOf(kpiObject[15]);
			}

			// 数据提供者
			String kpiDataPeopleName = "";
			if (kpiObject[16] != null) {
				kpiDataPeopleName = String.valueOf(kpiObject[16]);
			}
			// 数据统计时间\频率 0：月 1：季度
			int kpiHorizontal = -1;
			if (kpiObject[17] != null) {
				String kpiHorizontalTemp = String.valueOf(kpiObject[17]);
				if (!JecnCommon.isNullOrEmtryTrim(kpiHorizontalTemp)) {
					kpiHorizontal = Integer.valueOf(kpiHorizontalTemp);
				}

			}
			// 选中的部门/流程架构的id
			long id = -1;
			if (kpiObject[18] != null) {
				String orgIdTemp = String.valueOf(kpiObject[18]);
				if (!JecnCommon.isNullOrEmtryTrim(orgIdTemp)) {
					id = Long.valueOf(orgIdTemp).longValue();
				}

			}
			// 选中的责任部门/流程架构的name
			String name = "";
			if (kpiObject[24] != null) {
				String nameTemp = String.valueOf(kpiObject[24]);
				if (!JecnCommon.isNullOrEmtryTrim(nameTemp)) {
					name = nameTemp;
				}

			}

			// 流程主键ID
			long flowId = -1;
			if (kpiObject[19] != null) {
				String flowIdTemp = String.valueOf(kpiObject[19]);
				if (!JecnCommon.isNullOrEmtryTrim(flowIdTemp)) {
					flowId = Long.valueOf(flowIdTemp).longValue();
				}

			}
			// KPI主键ID
			long kpiId = -1;
			if (kpiObject[20] != null) {
				String kpiIdTemp = String.valueOf(kpiObject[20]);
				if (!JecnCommon.isNullOrEmtryTrim(kpiIdTemp)) {
					kpiId = Long.valueOf(kpiIdTemp).longValue();
				}

			}
			// KPI值主键ID
			long kpiValueId = -1;
			if (kpiObject[21] != null) {
				String kpiValueIdTemp = String.valueOf(kpiObject[21]);
				if (!JecnCommon.isNullOrEmtryTrim(kpiValueIdTemp)) {
					kpiValueId = Long.valueOf(kpiValueIdTemp).longValue();
				}

			}
			// KPI值日期
			String kpiHorVlaue = "";
			if (kpiObject[22] != null) {
				String kpiHorVlaueTemp = kpiObject[22].toString();
				if (!JecnCommon.isNullOrEmtryTrim(kpiHorVlaueTemp)) {
					kpiHorVlaue = JecnUtil.DateToStr((Date) kpiObject[22], "yyyy-MM");
				}

			}
			// KPI值
			double kpiValue = -1;
			if (kpiObject[23] != null) {
				String kpiValueTemp = String.valueOf(kpiObject[23]);
				if (!JecnCommon.isNullOrEmtryTrim(kpiValueTemp)) {
					kpiValue = Double.valueOf(kpiValueTemp).doubleValue();
				}

			}

			if (kpiObject[25] != null) {
				kpiPurpose = kpiObject[25].toString();
			}
			if (kpiObject[26] != null) {
				kpiPoint = kpiObject[26].toString();
			}
			if (kpiObject[27] != null) {
				kpiPeriod = kpiObject[27].toString();
			}
			if (kpiObject[28] != null) {
				kpiExplain = kpiObject[28].toString();
			}
			if (kpiObject[29] != null) {
				kpiDefinition = kpiObject[29].toString();
			}

			if (kpiObject[30] != null) {
				kpiType = "0".equals(kpiObject[30].toString()) ? "结果性指标" : "过程性指标";
			}

			// sql通过排序后的数据：通过部门ID，流程ID，KPI主键ID，KPI值日期（只有按此排序情况下才能使用相邻比较不相等来判断是否重复）
			// 一个部门对应的KPI值都排列在一起
			if (tmpId != id) {// 部门不相等重新记录一个部门数据
				tmpId = id;
				tmpKpiId = 0l;
				tmpKpiValueId = 0l;

				webListBean = kpiWebListMaps.get(id);

				// 添加部门/流程架构名称 ID，名称
				webListBean.setId(id);
				webListBean.setName(name);
			}

			// sql通过排序后的数据：通过部门ID，流程ID，KPI主键ID，KPI值日期（只有按此排序情况下才能使用相邻比较不相等来判断是否重复）
			if (tmpKpiId != kpiId) {// KPI主键ID不相等重新记录一个KPI数据
				tmpKpiId = kpiId;
				tmpKpiValueId = 0l;

				processKPIWebBean = new ProcessKPIWebBean();
				webListBean.addProcessKPIWebBean(processKPIWebBean);

				// 添加KPI属性值
				// KPI主键ID
				processKPIWebBean.setKpiId(kpiId);
				// 流程ID
				processKPIWebBean.setFlowId(flowId);
				// 流程名称（或关键过程）
				processKPIWebBean.setFlowName(flowName);
				// 流程责任部门
				processKPIWebBean.setOrgName(orgName);
				// 流程责任人
				processKPIWebBean.setResPeopleName(resPeopleName);
				// 流程责任人是岗位时，岗位ID--此项留以后用户需要显示岗位对应人员时使用
				// 流程监护人
				processKPIWebBean.setWardPeopleName(wardPeopleName);
				// 流程拟制人
				processKPIWebBean.setDraftPersonName(draftPersonName);
				// 流程KPI名称
				processKPIWebBean.setKpiName(kpiName);
				// 指标来源 0:二级指标 1:个人PBC指标 2:其他重要工作
				processKPIWebBean.setKpiTargetType(JecnResourceUtil.getKpiTargetType(kpiTargetType));
				// 支撑的一级指标
				processKPIWebBean.setKpiFirstTarget(kpiFirstTarge);
				// 相关度 0：强相关 1：弱相关 2：不相关
				processKPIWebBean.setKpiRelevance(JecnResourceUtil.getKpiRelevance(kpiRelevance));
				// 流程KPI计算公式
				processKPIWebBean.setKpiStatisticalMethods(kpiStatisticalMethods);
				// 流程KPI目标值 KPI目标值比较符号 0：= 1：> 2：< 3:>= 4:<=
				processKPIWebBean.setKpiTargetOperator(JecnResourceUtil.getKpiTargetOperator(kpiTargetOperator));
				// 流程KPI目标值：值
				processKPIWebBean.setKpiTarget(kpiTarget);
				// 流程kpi目标值:KPI值单位名称(纵向单位) 0:百分比 1:季度 2:月 3:周 4:工作日 5:天 6:小时
				// 7:分钟 8:个 9:人 10:ppm 11:元、12：次
				processKPIWebBean.setKpiVertical(JecnResourceUtil.getKpiVertical(kpiVertical));
				// 数据获取方式 0:人工 1：系统
				processKPIWebBean.setKpiDataMethod(JecnResourceUtil.getKpiDataMethod(kpiDataMethod));
				// IT系统(一个KPI下对应的IT系统有多个)
				// 数据提供者
				processKPIWebBean.setKpiDataPeopleName(kpiDataPeopleName);
				// 数据统计时间\频率 0：月 1：季度
				processKPIWebBean.setKpiHorizontal(JecnResourceUtil.getKpiHorizontal(kpiHorizontal));

				processKPIWebBean.setKpiPurpose(kpiPurpose);
				processKPIWebBean.setKpiPoint(kpiPoint);
				processKPIWebBean.setKpiPeriod(kpiPeriod);
				processKPIWebBean.setKpiExplain(kpiExplain);
				processKPIWebBean.setKpiDefinition(kpiDefinition);
				processKPIWebBean.setKpiType(kpiType);

			}

			// IT系统(一个KPI下对应的IT系统有多个)
			if (itSystem != null && !"".equals(itSystem.trim())) {// 给定值不为空
				if (processKPIWebBean.getItSystem() == null || "".equals(processKPIWebBean.getItSystem().trim())) {
					processKPIWebBean.setItSystem(itSystem);
				} else {
					if (processKPIWebBean.getItSystem().indexOf(itSystem) == -1) {// 不存在值
						processKPIWebBean.setItSystem(processKPIWebBean.getItSystem() + "/" + itSystem);
					}
				}
			}

			// KPI值--------------
			// sql通过排序后的数据：通过部门ID，流程ID，KPI主键ID，KPI值日期（只有按此排序情况下才能使用相邻比较不相等来判断是否重复）
			if (kpiValueId != -1 && tmpKpiValueId != kpiValueId) {// 同一KPI下，KPI值主键不相同，就创建一个KPI值bean
				tmpKpiValueId = kpiValueId;
				// KPI值bean
				ProcessKPIWebValueBean kpiWebValueBean = new ProcessKPIWebValueBean();
				// KPI值主键ID
				kpiWebValueBean.setKpiValueId(kpiValueId);
				// KPI值
				kpiWebValueBean.setKpiValue(kpiValue);
				// KPI值日期
				kpiWebValueBean.setKpiHorVlaue(kpiHorVlaue);
				processKPIWebBean.addProcessKPIWebValueBean(kpiWebValueBean);
			} else {
				// 同一KPI值主键ID是因为IT系统有多个造成的
			}
		}
		// 将map中的value添加到list中
		kPIWebListBean.addAll(kpiWebListMaps.values());

	}

	@Override
	public ProcessExpiryMaintenanceBean findProcessExpiryMaintenanceBean(String dutyOrgIds, String processMapIds,
			long projectId, int type) throws Exception {
		ProcessExpiryMaintenanceBean processExpiryMaintenanceBean = new ProcessExpiryMaintenanceBean();
		processExpiryMaintenanceBean.setType(type);

		Set<Long> dutyOrgIdsSet = new HashSet<Long>();
		Set<Long> processMapIdsSet = new HashSet<Long>();
		if (StringUtils.isNotBlank(dutyOrgIds) && !dutyOrgIds.contains("-1")) {
			String[] ids = dutyOrgIds.split(",");
			for (String orgId : ids) {
				dutyOrgIdsSet.add(Long.valueOf(orgId));
			}
		}
		if (StringUtils.isNotBlank(processMapIds) && !processMapIds.contains("-1")) {
			String[] ids = processMapIds.split(",");
			for (String processId : ids) {
				processMapIdsSet.add(Long.valueOf(processId));
			}
		}

		List<Object[]> expiryMaintenanceObjects = this.flowStructureDao.getProcessExpiryMaintenanceDateList(
				dutyOrgIdsSet, processMapIdsSet, projectId, type);

		/** 流程拟制人是否显示 */
		boolean artificialPersonIsShow = artificialPersonIsShow(type);
		/** 流程监护人是否显示 */
		boolean guardianPeopleIsShow = guardianPeopleIsShow(type);

		processExpiryMaintenanceBean.setArtificialPersonIsShow(artificialPersonIsShow);
		processExpiryMaintenanceBean.setGuardianPeopleIsShow(guardianPeopleIsShow);

		processExpiryMaintenanceBean
				.setExpiryMaintenanceWebs(getProcessExpiryMaintenanceWebBeanListByObjs(expiryMaintenanceObjects));

		return processExpiryMaintenanceBean;
	}

	private boolean artificialPersonIsShow(int type) {
		if (type == 0) {
			return JecnConfigTool.fictionPeopleIsShow();
		} else {
			return JecnConfigTool.ruleFictionPeopleIsShow();
		}
	}

	/**
	 * 文控id 流程名称 流程编号 责任部门 流程责任人 流程监护人 拟稿人 发布日期 有效日期 下次审视需要完成时间
	 * 
	 * @param expiryMaintenanceObjects
	 * @return
	 */
	private List<ProcessExpiryMaintenanceWebBean> getProcessExpiryMaintenanceWebBeanListByObjs(
			List<Object[]> expiryMaintenanceObjects) {
		List<ProcessExpiryMaintenanceWebBean> webBeans = new ArrayList<ProcessExpiryMaintenanceWebBean>();

		for (Object[] obj : expiryMaintenanceObjects) {
			if (obj[0] == null || obj[1] == null || obj[2] == null || obj[8] == null || obj[9] == null
					|| obj[10] == null) {
				continue;
			}
			ProcessExpiryMaintenanceWebBean webBean = new ProcessExpiryMaintenanceWebBean();
			webBean.setDocId(obj[0].toString());
			webBean.setFlowId(obj[1].toString());
			webBean.setFlowName(obj[2].toString());
			// 流程编号
			if (obj[3] != null) {
				webBean.setFlowIdInput(obj[3].toString());
			}
			// 责任部门
			if (obj[4] != null) {
				webBean.setDutyOrgName(obj[4].toString());
			}
			// 流程责任人
			if (obj[5] != null) {
				webBean.setDutyPeopleName(obj[5].toString());
			}
			// 流程监护人
			if (obj[6] != null) {
				webBean.setGuardianName(obj[6].toString());
			}
			// 拟制人
			if (obj[7] != null) {
				webBean.setPeopleMakeName(obj[7].toString());
			}
			// 发布日期
			webBean.setPubDate(JecnUtil.trimRemain(obj[8], 10));
			// 有效期
			webBean.setExpiry(obj[9].toString());
			// 下次审视需要完成时间
			webBean.setNextScandDate(JecnUtil.trimRemain(obj[10], 10));
			// 流程版本号
			if (obj[11] != null && !"".equals(obj[11].toString())) {
				webBean.setVersionId(obj[11].toString());
			} else {
				webBean.setVersionId("");
			}
			webBeans.add(webBean);
		}

		return webBeans;

	}

	/**
	 * 查询id和name的集合
	 * 
	 * @param ids
	 *            使用，拼接
	 * @param type
	 *            0部门 1流程
	 * @return
	 */
	private List<Object[]> queryIdAndNameList(String ids, int type) {
		if (StringUtils.isBlank(ids)) {
			return new ArrayList<Object[]>();
		}
		String sql = "";
		String sqlIds = "(" + ids + ")";
		if (type == 0) {
			sql = "select org_id,ORG_NAME from JECN_FLOW_ORG where org_id in " + sqlIds + " order by org_id asc";
		} else if (type == 1) {
			sql = "select flow_id,flow_name from JECN_FLOW_STRUCTURE_T where flow_id in " + sqlIds
					+ " order by flow_id asc";
		} else if (type == 2) {
			sql = "select id,rule_name from jecn_rule_t where id in " + sqlIds + " order by id asc";
		}
		return flowStructureDao.listNativeSql(sql);
	}

}
