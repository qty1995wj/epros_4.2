/**
 * 
 */
package com.jecn.epros.server.bean.dataimport;

import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractBaseBean;

/**
 * 
 * 基准岗位Bean
 * @author cheshaowei
 *
 */
public class JecnBasePosBean extends AbstractBaseBean{
    
	
	/**基准岗位主键ID*/
	private String basePosId;
	/**基准岗位编号*/
	private String basePosNum;
	/**基准岗位名称*/
	private String basePosName;
	
	
	
	public String getBasePosId() {
		return basePosId;
	}
	public void setBasePosId(String basePosId) {
		this.basePosId = basePosId;
	}
	public String getBasePosNum() {
		return basePosNum;
	}
	public void setBasePosNum(String basePosNum) {
		this.basePosNum = basePosNum;
	}
	public String getBasePosName() {
		return basePosName;
	}
	public void setBasePosName(String basePosName) {
		this.basePosName = basePosName;
	}
	@Override
	public String toInfo() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
