package com.jecn.epros.server.dao.project.impl;

import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.project.IProjectDao;

/**
 * 项目管理Dao
 * @author 2012-05-23
 *
 */
public class ProjectDaoImpl extends AbsBaseDao<JecnProject, Long> implements IProjectDao {



}
