
package com.jecn.webservice.zhongsh.datainput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserTotalCountResult" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserTotalCountResult"
})
@XmlRootElement(name = "GetUserTotalCountResponse")
public class GetUserTotalCountResponse {

    @XmlElement(name = "GetUserTotalCountResult")
    protected int getUserTotalCountResult;

    /**
     * 获取getUserTotalCountResult属性的值。
     * 
     */
    public int getGetUserTotalCountResult() {
        return getUserTotalCountResult;
    }

    /**
     * 设置getUserTotalCountResult属性的值。
     * 
     */
    public void setGetUserTotalCountResult(int value) {
        this.getUserTotalCountResult = value;
    }

}
