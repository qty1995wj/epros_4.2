
package com.jecn.webservice.zhongsh;

/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

/**
 * This class was generated by Apache CXF 3.1.1
 * 2015-11-20T07:20:59.750+08:00
 * Generated source version: 3.1.1
 * 
 */
public final class GetUserByTokenServiceSoap_GetUserByTokenServiceSoap12_Client {

    private static final QName SERVICE_NAME = new QName("http://tempuri.org/", "GetUserByTokenService");

    private GetUserByTokenServiceSoap_GetUserByTokenServiceSoap12_Client() {
    }

    public static void main(String args[]) throws java.lang.Exception {
        URL wsdlURL = GetUserByTokenService.WSDL_LOCATION;
        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
            File wsdlFile = new File(args[0]);
            try {
                if (wsdlFile.exists()) {
                    wsdlURL = wsdlFile.toURI().toURL();
                } else {
                    wsdlURL = new URL(args[0]);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
      
        GetUserByTokenService ss = new GetUserByTokenService(wsdlURL, SERVICE_NAME);
        GetUserByTokenServiceSoap port = ss.getGetUserByTokenServiceSoap12();  
        
        {
        System.out.println("Invoking checkToken...");
        java.lang.String _checkToken_sToken = "0c62a879-3cc4-4f14-8218-4554e3e103e4";
        ArrayOfString _checkToken__return = port.checkToken(_checkToken_sToken);
        System.out.println("checkToken.result=" + _checkToken__return);


        }

        System.exit(0);
    }

}
