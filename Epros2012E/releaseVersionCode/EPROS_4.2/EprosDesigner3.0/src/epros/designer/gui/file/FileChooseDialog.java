package epros.designer.gui.file;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.ConfigCommon;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.common.ConfigCommon.ConfigCommonEnum;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;

public class FileChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(FileChooseDialog.class);

	private Long supportFileId = null;

	public FileChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		super(list, 2, jecnDialog);
		this.setTitle(JecnProperties.getValue("StandardDocument"));

		// 文件选择，根据配置文件节点ID，定位树节点
		allowExpandByConfigNode((HighEfficiencyFileTree) jTree);
	}

	public FileChooseDialog(List<JecnTreeBean> list) {
		super(list, 2);
		this.setTitle(JecnProperties.getValue("StandardDocument"));

		// 文件选择，根据配置文件节点ID，定位树节点
		allowExpandByConfigNode((HighEfficiencyFileTree) jTree);
	}

	public FileChooseDialog(List<JecnTreeBean> list, int isVisibleLab) {
		super(list, 2);
		if (isVisibleLab == 0) {
			this.getEmptyBtn().setVisible(false);
		}
		this.setTitle(JecnProperties.getValue("StandardDocument"));

		// 文件选择，根据配置文件节点ID，定位树节点
		allowExpandByConfigNode((HighEfficiencyFileTree) jTree);
	}

	public FileChooseDialog(List<JecnTreeBean> list, Long supportFileId) {
		super(list, 2);
		this.supportFileId = supportFileId;
		this.setTitle(JecnProperties.getValue("StandardDocument"));

		// 文件选择，根据配置文件节点ID，定位树节点
		allowExpandByConfigNode((HighEfficiencyFileTree) jTree);
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		List<JecnTreeBean> listTreeBean = null;
		if (DrawCommon.checkNameMaxLength(name)) {
			listTreeBean = new ArrayList<JecnTreeBean>();
		} else {
			listTreeBean = new ArrayList<JecnTreeBean>();
			try {
				List<JecnFileBeanT> list = null;
				if (this.getChooseType() == 19) {
					list = ConnectionPool.getFileAction().getFileDirsByName(name, JecnConstants.projectId);
				} else {
					list = ConnectionPool.getFileAction().getFilesByName(name, JecnConstants.projectId, null);
				}

				for (JecnFileBeanT fileBean : list) {
					JecnTreeBean treeBean = new JecnTreeBean();
					treeBean.setId(fileBean.getFileID());
					treeBean.setName(fileBean.getFileName());
					treeBean.setTreeNodeType(fileBean.getIsDir() == 0 ? TreeNodeType.fileDir : TreeNodeType.file);
					// 是图片
					if (getChooseType() == 17) {
						if (JecnUtil.isImage(fileBean.getFileName())) {
							listTreeBean.add(treeBean);
						}
					} else {
						listTreeBean.add(treeBean);
					}
				}

			} catch (Exception e) {
				log.error("FileChooseDialog searchByName is error", e);

			}
		}
		return listTreeBean;
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("fileNameC");
	}

	@Override
	public JecnTree getJecnTree() {
		// return new HighEfficiencyFileTree(this);
		// if (JecnConstants.isMysql) {
		// return new RoutineFileTree();
		// } else {
		// return new HighEfficiencyFileTree(1, this);
		// }

		HighEfficiencyFileTree jTree = new HighEfficiencyFileTree(1, this);
		return jTree;
	}

	/**
	 * 文件选择，根据配置文件节点ID，定位树节点
	 * 
	 * @param jTree
	 */
	private void allowExpandByConfigNode(HighEfficiencyFileTree jTree) {
		JecnTreeBean treeBean = getConfigCommonDir();
		if (treeBean == null) {
			return;
		}
		jTree.setAllowExpand(false);
		try {
			JecnTreeCommon.searchNode(treeBean, jTree);
		} catch (Exception e) {
			log.error("FileChooseDialog allowExpandByConfigNode is error！", e);
		}
		jTree.setAllowExpand(true);
	}

	protected void okBtnAction() {
		// 文件选择，记录当前选择的文件目录节点ID
		List<JecnTreeBean> listResults = getListJecnTreeBean();
		if (listResults.size() > 0) {
			Long pid = listResults.get(listResults.size() - 1).getPid();
			if (pid != null) {
				ConfigCommon.INSTANTCE
						.writeCommonConfigProperties(String.valueOf(pid), ConfigCommonEnum.relatedFileDir);
			}
		}
		super.okBtnAction();
	}

	private JecnTreeBean getConfigCommonDir() {
		Long fileId = -1L;
		if (supportFileId != null) {
			fileId = supportFileId;
		} else {
			String value = ConfigCommon.INSTANTCE.getConfigProperties(ConfigCommonEnum.relatedFileDir);
			if (StringUtils.isBlank(value)) {
				return null;
			}
			fileId = Long.valueOf(value);
		}
		JecnTreeBean treeBean = new JecnTreeBean();
		treeBean.setId(fileId);
		treeBean.setTreeNodeType(TreeNodeType.fileDir);
		return treeBean;
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		title.add(JecnProperties.getValue("actBaseNum"));
		return title;
	}

	@Override
	public int[] hiddenCols() {
		return new int[] { 0, 2 };
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {

		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				data.add(jecnTreeBean.getNumberId());
				content.add(data);
			}
		}
		return content;

	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		data.add(jecnTreeBean.getNumberId());
		return data;
	}

	/**
	 * 点击表格 定位树节点
	 */
	public void isClickTable() {
		int row = this.resultTable.getSelectedRow();
		if (row == -1) {
			return;
		}
		JecnTreeBean treeBean = getListJecnTreeBean().get(row);
		if (treeBean == null) {
			return;
		}
		treeBean.setTreeNodeType(TreeNodeType.file);
		try {
			HighEfficiencyFileTree jTree = ((HighEfficiencyFileTree) this.getjTree());
			jTree.setAllowExpand(false);
			JecnTreeCommon.searchNode(treeBean, jTree);
			jTree.setAllowExpand(true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("FileChooseDialog isClickTable is error！", e);
		}
	}
}
