package com.jecn.epros.server.action.designer.standard.impl;

import java.util.List;

import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.server.action.designer.standard.IStandardAction;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.standard.IStandardService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class StandardActionImpl implements IStandardAction {

	private IStandardService standardService;

	public IStandardService getStandardService() {
		return standardService;
	}

	public void setStandardService(IStandardService standardService) {
		this.standardService = standardService;
	}

	@Override
	public Long addStandard(StandardBean standardBean) throws Exception {
		return standardService.addStandard(standardBean);
	}

	@Override
	public List<JecnTreeBean> getChildStandards(Long pid, Long projectId) throws Exception {
		return standardService.getChildStandards(pid, projectId);
	}

	/**
	 * 角色权限下：标准权限选择
	 * 
	 * @param pid
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> getRoleAuthChildStandards(Long pid, Long projectId, Long peopleId) throws Exception {
		return standardService.getRoleAuthChildStandards(pid, projectId, peopleId);
	}

	@Override
	public List<JecnTreeBean> getAllStandard(Long projectId) throws Exception {
		return null;
	}

	@Override
	public void reName(String newName, Long id, Long updatePersonId) throws Exception {
		standardService.reName(newName, id, updatePersonId);
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception {
		standardService.updateSortNodes(list, pId, projectId, updatePersonId);
	}

	@Override
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		standardService.moveNodes(listIds, pId, updatePersonId, moveNodeType);

	}

	@Override
	public void deleteStandard(List<Long> listIds, Long projectId, Long updatePersonId) throws Exception {
		standardService.deleteStandard(listIds, projectId, updatePersonId);
	}

	@Override
	public void addRelationFlow(List<StandardBean> list, Long updatePersonId) throws Exception {
		standardService.addRelationFlow(list, updatePersonId);

	}

	@Override
	public void addStandardFile(List<StandardBean> list, String posIds, String orgIds, String posGroupIds,
			Long updatePersonId) throws Exception {
		standardService.addStandardFile(list, posIds, orgIds, posGroupIds, updatePersonId);

	}

	@Override
	public List<JecnTreeBean> searchByName(String name, Long projectId, Long peopelId) throws Exception {
		if (peopelId == null) {
			return standardService.searchByName(name, projectId);
		} else {
			return standardService.searchRoleAuthByName(name, projectId, peopelId);
		}
	}

	@Override
	public List<JecnTreeBean> getChildStandardDirs(Long pid, Long projectId) throws Exception {
		return standardService.getChildStandardDirs(pid, projectId);
	}

	@Override
	public List<JecnTreeBean> getAllStandardDir(Long pid, Long projectId) throws Exception {
		return standardService.getAllStandardDir(pid, projectId);
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long standardId, Long projectId) throws Exception {
		return standardService.getPnodes(standardId, projectId);
	}

	@Override
	public void updateStandardFile(Long id, Long fileId, String fileName, Long updatePersonId) throws Exception {
		standardService.updateStandardFile(id, fileId, fileName, updatePersonId);

	}

	@Override
	public void updateStandardProcess(Long id, Long processId, String name, TreeNodeType type, Long updatePersonId)
			throws Exception {
		int i = 2;
		if (type == TreeNodeType.process) {
		} else if (type == TreeNodeType.processMap) {
			i = 3;
		}
		standardService.updateStandardProcess(id, processId, name, i, updatePersonId);
	}

	@Override
	public List<JecnTreeBean> searchStandarFileByName(String name, Long projectId) throws Exception {
		return standardService.searchStandarFileByName(name, projectId);
	}

	@Override
	public void updateStanClauseRequire(Long id, String name, String context, Long updatePersonId) throws Exception {
		standardService.updateStanClauseRequire(id, name, context, updatePersonId);
	}

	@Override
	public StandardBean getStandardBean(Long id) throws Exception {
		StandardBean standardBean = standardService.get(id);
		standardBean.setFileContents(standardService.getStandardFileContents(id));
		return standardBean;
	}

	@Override
	public void updateStandardBean(StandardBean standardBean) throws Exception {
		standardService.updateStandardBean(standardBean);
	}

	@Override
	public List<JecnTreeBean> getStandardByFlowId(Long flowId) throws Exception {
		return standardService.getStandardByFlowId(flowId);
	}

	@Override
	public FileOpenBean openFileContent(Long id) throws Exception {
		return standardService.openFileContent(id);
	}

	@Override
	public ByteFileResult importData(long userId, byte[] b, Long id, String type, Long projectId, boolean CH) {
		return standardService.importData(userId, b, id, type, projectId, CH);
	}
}
