package epros.designer.gui.project;

import java.awt.Insets;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.project.JecnProject;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 新建项目名称
 * @author 2012-05-23
 *
 */

public class AddProjectNameDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(AddProjectNameDialog.class);
	
	private JecnProject jecnProject;
	public AddProjectNameDialog(ProjectManageDialog projectManageDialog){
		this.setLocationRelativeTo(null);
	}
	
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addProject");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		JecnProject jecnProject = new JecnProject();
		jecnProject.setProjectName(getName());
		jecnProject.setDelSate(Long.valueOf("0"));
		try {
			this.jecnProject=ConnectionPool.getProject().addProject(jecnProject);
			this.dispose();
		} catch (Exception e) {
			log.error("AddProjectNameDialog saveData is error",e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
			return ConnectionPool.getProject().validateAddName(name);
	}

	public JecnProject getJecnProject() {
		return jecnProject;
	}

	public void setJecnProject(JecnProject jecnProject) {
		this.jecnProject = jecnProject;
	}
	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}
}
