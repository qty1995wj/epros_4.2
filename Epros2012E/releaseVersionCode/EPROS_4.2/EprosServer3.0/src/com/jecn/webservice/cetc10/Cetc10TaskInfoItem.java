package com.jecn.webservice.cetc10;

import java.net.URL;

import org.apache.log4j.Logger;
import org.codehaus.xfire.client.Client;

import com.jecn.epros.server.action.web.login.ad.cetc10.JecnCetc10AfterItem;

/**
 * 十所代办信息发送到 webservice
 * 
 * 
 * @author ZXH
 * 
 */
public enum Cetc10TaskInfoItem {
	INSTANCE;

	static Logger logger = Logger.getLogger(Cetc10TaskInfoItem.class);

	public void taskSendInfoService(Cetc10TaskBean cetc10TaskBean) {
		try {
			// URL="http://188.88.100.87/ESB_Web_swietPortal_WebService/WebService.asmx?wsdl";
			// Client client = new Client(new
			// URL(JecnCetc10AfterItem.TASK_URL));

			// client.setProperty("username", JecnCetc10AfterItem.USER_NAME);
			// client.setProperty("password", JecnCetc10AfterItem.PASS_DORD);

			String userName = JecnCetc10AfterItem.getValue("username");
			String password = JecnCetc10AfterItem.getValue("password");
			// http://188.88.100.87/ESB_Web_swietPortal_DealWebService/WebService.asmx?wsdl
			String url = JecnCetc10AfterItem.getValue("taskUrl");
			Client client = new Client(new URL(url));
			logger.info("client创建连接成功!");
			// client.setProperty("username", "LXTest");
			// client.setProperty("password", "LXTest20160715");

			client.setProperty("username", userName);
			client.setProperty("password", password);
			logger.info("账号和密码：" + userName + " " + password);

			Object[] sendInfoParams = { cetc10TaskBean.getTitle(), cetc10TaskBean.getUrl(), cetc10TaskBean.getTime(),
					cetc10TaskBean.getSource(), cetc10TaskBean.getName(), cetc10TaskBean.getLoginName(),
					cetc10TaskBean.getIsProcessed(), cetc10TaskBean.getSummery(), cetc10TaskBean.getTaskid() };
			client.invoke("SendInfo", sendInfoParams);
			logger.info("发送代办成功!");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("发送代办失败!", e);
		}
	}

	public static void main(String[] args) {
		try {
			Client client = new Client(new URL(
					"http://188.88.100.87/ESB_Web_swietPortal_WebService/WebService.asmx?wsdl"));
			// client.setProperty("username", "EPROS");
			// client.setProperty("password", "EPROS20160715");
			//
			client.setProperty("username", "LXTest");
			client.setProperty("password", "LXTest20160715");
			//
			Object[] sendInfoParams = { "test", "1212http://pdm.swiet.com", "2016-07-15", "PDM系统", "task", "lixin_2@163.com",
					"false", "test", "100" };
			client.invoke("SendInfo", sendInfoParams);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
