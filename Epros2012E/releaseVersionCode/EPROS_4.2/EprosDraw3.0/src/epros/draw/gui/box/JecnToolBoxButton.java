package epros.draw.gui.box;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnToolBar;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程元素按钮
 * 
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolBoxButton extends JToggleButton implements ActionListener {
	/** 装载按钮容器 */
	private JecnToolBar toolBar = null;

	/** 流程元素库主面板 */
	private JecnToolBoxMainPanel toolBoxMainPanel = null;

	/** 按钮标识 */
	private MapElemType btnType = null;
	private ImageIcon btnIcon = null;
	/** 名称 */
	private String btnText = "";
	/** 按钮图标说明信息 */
	private String btnTextNote = "";

	/**
	 * 
	 * 构造函数
	 * 
	 * @param toolBoxMainPanel
	 *            JecnToolBoxMainPanel 流程元素库面板
	 * @param btnType
	 *            MapElemType 按钮标识
	 */
	public JecnToolBoxButton(JecnToolBoxMainPanel toolBoxMainPanel, MapElemType btnType) {
		if (toolBoxMainPanel == null || btnType == null) {
			JecnLogConstants.LOG_TOOLBOX_BUTTON.error("JecnToolBoxButton类构造函数：参数为null或“”.toolBoxMainPanel="
					+ toolBoxMainPanel + ";btnType=" + btnType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.toolBoxMainPanel = toolBoxMainPanel;
		this.btnType = btnType;
		initComponents();

	}

	/**
	 * 
	 * 初始化
	 * 
	 * @param text
	 * @param btnActionCommand
	 */
	private void initComponents() {
		// 实例化工具栏
		toolBar = new JecnToolBar();
		toolBar.add(this);

		// 不显示焦点状态
		this.setFocusPainted(false);

		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 事件
		this.addActionListener(this);

		// 名称
		btnText = toolBoxMainPanel.getResourceManager().getValue(btnType);
		// 按钮图标说明信息
		btnTextNote = toolBoxMainPanel.getResourceManager().getValue(btnType.toString() + "Note");
		// 图片
		btnIcon = JecnFileUtil.getToolBoxImageIcon(btnType.toString());
		// 提示信息
		setToolBoxButtonToolTipText();

		this.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent paramMouseEvent) {
				ToolTipManager.sharedInstance().setDismissDelay(3600 * 1000);
			}

			public void mouseExited(MouseEvent paramMouseEvent) {
				// 返回初始延迟值
				int intD = ToolTipManager.sharedInstance().getInitialDelay();
				// 取消工具提示的延迟值
				ToolTipManager.sharedInstance().setDismissDelay(intD);
			}
		});
		// 默认图片上内容下布局
		setButtonShowType();
	}

	/**
	 * 
	 * 提示信息
	 * 
	 */
	private void setToolBoxButtonToolTipText() {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append("<html><b>");
		sbuf.append(btnText);
		sbuf.append("</b><br><br>");
		sbuf.append("<div style= 'white-space:normal; display:block;width:200px; word-break:break-all'>");
		sbuf.append(btnTextNote);
		sbuf.append("</div><br><br></html>");
		setToolTipText(sbuf.toString());
	}

	void setButtonShowType() {
		JecnToolBoxFlowElemContainPanel flowElemContainPanel = toolBoxMainPanel.getFlowElemContainPanel();
		if (flowElemContainPanel == null) {
			return;
		}
		switch (flowElemContainPanel.getButtonShowType()) {
		case leftRight:// 图片左内容右
			leftRight();
			break;
		case topBotton: // 图片上内容下
			topButton();
			break;
		case titleText: // 显示详细信息
			getTitleBtnText();
			break;
		}
	}

	/**
	 * 
	 * 图上内容下布局
	 * 
	 */
	private void topButton() {
		this.removeAll();
		// 布局管理器
		this.setLayout(null);
		// 图片
		this.setIcon(btnIcon);
		// 设置内容
		this.setText(btnText);
		// 大小
		this.setPreferredSize(toolBoxMainPanel.getTopButtonBtnSize());
		// 最小大小
		this.setMinimumSize(toolBoxMainPanel.getTopButtonBtnSize());
		// 图片内容显示方式：居中
		this.setHorizontalAlignment(SwingConstants.CENTER);
		// 上下分布
		this.setHorizontalTextPosition(SwingConstants.CENTER);
		this.setVerticalTextPosition(SwingConstants.BOTTOM);
	}

	/**
	 * 
	 * 图左内容右布局
	 * 
	 */
	public void leftRight() {
		this.removeAll();
		// 布局管理器
		this.setLayout(null);
		// 图片
		this.setIcon(btnIcon);
		// 设置内容
		this.setText(btnText);
		// 大小
		this.setPreferredSize(toolBoxMainPanel.getLeftRightBtnSize());
		// 最小大小
		this.setMinimumSize(toolBoxMainPanel.getLeftRightBtnSize());
		// 图片内容显示方式：靠左
		this.setHorizontalAlignment(SwingConstants.LEFT);
		// 左右分布
		this.setHorizontalTextPosition(SwingConstants.RIGHT);
		this.setVerticalTextPosition(SwingConstants.CENTER);
	}

	/**
	 * 
	 * 详细说明布局
	 * 
	 */
	public void getTitleBtnText() {
		this.removeAll();
		this.setText(null);
		this.setIcon(null);

		// 布局管理器
		this.setLayout(new GridBagLayout());
		// 大小
		this.setPreferredSize(toolBoxMainPanel.getTitleTextBtnSize());
		// 最小大小
		this.setMinimumSize(toolBoxMainPanel.getTitleTextBtnSize());

		// 图片
		JLabel iconLabel = new JLabel(btnIcon);

		// 名称标签
		JLabel nameLabel = new JLabel(btnText);
		nameLabel.setHorizontalAlignment(SwingConstants.LEFT);
		// 名称字体加粗
		Font font = nameLabel.getFont();
		nameLabel.setFont(new Font(font.getName(), Font.BOLD, font.getSize()));
		// 详细内容
		JLabel noteLabel = new JLabel(btnTextNote);

		// 图片
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0(), 0, 0);
		this.add(iconLabel, c);
		// 名称
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				toolBoxMainPanel.getNoteInsets(), 0, 0);
		this.add(nameLabel, c);
		// 空闲
		// 详细内容
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				toolBoxMainPanel.getNoteInsets(), 0, 0);
		this.add(noteLabel, c);

	}

	/**
	 * 
	 * 获取按钮容器
	 * 
	 * @return
	 */
	public JecnToolBar getJToolBar() {
		return toolBar;
	}

	public void actionPerformed(ActionEvent e) {
		actionPerformedProcess(e);
	}

	/**
	 * 
	 * 选中按钮事件
	 * 
	 * @param e
	 */
	private void actionPerformedProcess(ActionEvent e) {
		if (this.isSelected()) {
			// 查看是否在编辑
			JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (workflow != null && !workflow.isEditFail()) {// 正在编辑且内容不正确
				toolBoxMainPanel.setSelectedBtnType(MapElemType.guide);
				return;
			}

			toolBoxMainPanel.setSelectedBtnType(this.btnType);
			// 清空选中的图形元素集合的所有显示点
			JecnWorkflowUtil.hideAllResizeHandle();
			// 拖动端点临时线
			JecnWorkflowUtil.disposeLinesAndEidtPoint();
			// 情况格式刷
			JecnWorkflowUtil.clearFormatPainter();
		}
	}

	public MapElemType getBtnType() {
		return btnType;
	}

}