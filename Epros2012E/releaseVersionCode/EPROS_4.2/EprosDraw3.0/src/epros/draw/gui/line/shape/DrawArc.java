/*
 * DrawArc.java
 *
 *  Created on 2010年3月8日, 上午10:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.line.shape;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 线段相交弧
 * 
 * @author ZHOUXY
 */
public class DrawArc extends JPanel {
	/** 弧形缩在线段 */
	private LineSegment lineSegment = null;
	/** 弧形中心点 */
	private Point centerPoint = null;
	/** 弧的相交竖线 */
	private LineSegment vLineSegment = null;

	/**
	 * 
	 * 
	 * @param point
	 * @param line
	 */
	public DrawArc(LineSegment lineSegment, LineSegment vLineSegment) {
		if (lineSegment == null) {
			JecnLogConstants.LOG_DRAW_ARC
					.error("DrawArc类构造函数：参数为null. lineSegment=" + lineSegment);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.setOpaque(false);
		this.lineSegment = lineSegment;
		this.vLineSegment = vLineSegment;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (lineSegment == null) {
			return;
		}

		int x = 0, h = this.getHeight() * 2, w = this.getWidth() - 1, y = 0;

		Graphics2D g2d = (Graphics2D) g;
		RenderingHints originalRenderingHints = g2d.getRenderingHints();
		// 指定RenderingHints
		g2d.setRenderingHints(JecnUIUtil.getRenderingHints());

		// Stroke oldStroke = g2d.getStroke();
		// RenderingHints oldRenderingHints = g2d.getRenderingHints();

		// 背景颜色
		g2d.setColor(JecnUIUtil.getDefaultBackgroundColor());
		// 填充覆盖指定矩形的圆弧或椭圆弧
		g2d.fillArc(x, y, w, h + 4, 0, 180);
		// 线条图元轮廓呈现属性
		g2d.setStroke(lineSegment.getBasicStroke());
		// 线条颜色
		g2d.setPaint(lineSegment.getFillColor());
		// 绘制一个覆盖指定矩形的圆弧或椭圆弧边框
		g2d.drawArc(x, y, w, h, 0, 180);

		Rectangle arcRect = this.getBounds();
		// 弧和竖线相交区域
		Rectangle rectangle = SwingUtilities.computeIntersection(arcRect.x,
				arcRect.y, arcRect.width, arcRect.height, vLineSegment
						.getBounds());

		Point point = SwingUtilities.convertPoint(JecnDrawMainPanel
				.getMainPanel().getWorkflow(), rectangle.x, rectangle.y, this);
		// rectangle.setLocation(point);
		rectangle.setBounds(point.x, point.y, rectangle.width / 2 + 1,
				rectangle.height);
		// 获取相交竖线的画笔
		g2d.setStroke(vLineSegment.getBasicStroke());
		// g2d.setColor(Color.red);
		// 填充相交区域
		g2d.fill(rectangle);
		// g2d.draw(rectangle);

		// 还原原始RenderingHints对象
		g2d.setRenderingHints(originalRenderingHints);
	}

	public Point getCenterPoint() {
		return centerPoint;
	}

	public void setCenterPoint(Point centerPoint) {
		this.centerPoint = centerPoint;
	}
}
