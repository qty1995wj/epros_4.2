package com.jecn.epros.util;

import com.jecn.epros.bean.*;
import com.jecn.epros.domain.TaskApproveBaseForm;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by thinkpad on 2017/1/3.
 */
public class TaskRPCUtils {
    public static BaseTaskParam toBaseTaskParam(String id, TaskApproveBaseForm baseForm) {
        BaseTaskParam baseTaskParam = new BaseTaskParam();
        baseTaskParam.setTaskId(id);
        baseTaskParam.setOpinion(baseForm.getOpinion());
        baseTaskParam.setTaskOperation(baseForm.getOperation());
        baseTaskParam.setTaskStage(baseForm.getState());
        return baseTaskParam;
    }
}
