package epros.draw.gui.top.button;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程元素按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolbarContentButton extends JButton {
	/** 装载按钮容器 */
	protected JToolBar toolBar = null;
	/** 按钮类型 */
	protected ToolBarElemType toolBarElemType = null;

	public JecnToolbarContentButton(String text, ToolBarElemType toolBarElemType) {

		this.toolBarElemType = toolBarElemType;

		initComponents(text, toolBarElemType);

	}

	private int linkIcon;

	public JecnToolbarContentButton(String text, ToolBarElemType toolBarElemType, int linkIcon) {
		this.toolBarElemType = toolBarElemType;
		initComponents(text, toolBarElemType);
		this.linkIcon = linkIcon;
	}

	public JToolBar getJToolBar() {
		return toolBar;
	}

	private void initComponents(String text, ToolBarElemType toolBarElemType) {
		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 图片内容显示方式：靠左
		this.setHorizontalAlignment(SwingConstants.LEFT);
		// 图片
		if (toolBarElemType != null) {
			// 设置图片
			this.setIcon(JecnFileUtil.getToolBarImageIcon(toolBarElemType.toString()));
		}
		// 设置内容
		if (text != null) {
			this.setText(text);
		}
		// 添加点击监听
		this.addActionListener(JecnDrawMainPanel.getMainPanel());
		// 不显示焦点状态
		this.setFocusPainted(false);

		toolBar = new JToolBar();
		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());
		toolBar.add(this);
	}

	public ToolBarElemType getToolBarElemType() {
		return toolBarElemType;
	}

}