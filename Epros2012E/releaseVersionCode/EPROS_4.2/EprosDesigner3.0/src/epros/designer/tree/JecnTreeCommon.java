package epros.designer.tree;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JTree;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.LoginBean;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.popedom.position.HighEfficiencyOrgPositionTree;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;

public class JecnTreeCommon {
	private static Logger log = Logger.getLogger(JecnTreeCommon.class);

	/**
	 * @author zhangchen May 2, 2012
	 * @description：获得树节点唯一标识
	 * @param node
	 * @return
	 */
	public static String getNodeUniqueIdentifier(JecnTreeNode node) {
		return getNodeUniqueIdentifier(node.getJecnTreeBean());
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：获得树节点唯一标识
	 * @param node
	 * @return
	 */
	public static String getNodeUniqueIdentifier(JecnTreeBean jecnTreeBean) {
		return getNodeUniqueIdentifier(jecnTreeBean.getId(), jecnTreeBean.getTreeNodeType());
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：获得树节点唯一标识
	 * @param node
	 * @return
	 */
	public static String getNodeUniqueIdentifier(Long id, TreeNodeType type) {
		return type.toString() + id.toString();
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：获得Tree节点 (递归)
	 * @param nodeUniqueIdentifier
	 * @param pNode
	 * @return
	 */
	private static JecnTreeNode getTreeNodeChild(String nodeUniqueIdentifier, JecnTreeNode pNode) {
		for (int i = 0; i < pNode.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) pNode.getChildAt(i);
			// node的唯一标识
			String nodeChildUniqueIdentifier = getNodeUniqueIdentifier(node);
			if (nodeUniqueIdentifier.equals(nodeChildUniqueIdentifier)) {
				return node;
			}
			JecnTreeNode childNode = getTreeNodeChild(nodeUniqueIdentifier, node);
			if (childNode != null) {
				return childNode;
			}
		}
		return null;
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：获得Tree节点,并判断节点存不存在 如果返回值为空,就是不存在,不为空就是存在
	 * @param jecnTreeBean
	 * @param jTree
	 * @return
	 */
	public static JecnTreeNode getTreeNode(JecnTreeBean jecnTreeBean, JTree jTree) {
		// node的唯一标识
		String nodeUniqueIdentifier = getNodeUniqueIdentifier(jecnTreeBean);
		return getTreeNode(nodeUniqueIdentifier, jTree);
	}

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：获得搜索的根节点
	 * @param jTree
	 * @param treeNodeType
	 * @return
	 */
	public static JecnTreeNode getSearchRootNode(JTree jTree, TreeNodeType treeNodeType) {
		// 获得根节点
		JecnTreeNode root = (JecnTreeNode) ((JecnTreeModel) jTree.getModel()).getRoot();
		if (treeNodeType.equals(root.getJecnTreeBean().getTreeNodeType())) {
			return root;
		}
		for (int i = 0; i < root.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) root.getChildAt(i);
			if (treeNodeType.equals(node.getJecnTreeBean().getTreeNodeType())) {
				return node;
			}
		}
		return null;
	}

	/**
	 * 根据 父ID 刷新 流程节点
	 * 
	 * @param flowId
	 *            流程ID
	 * @throws Exception
	 */
	public static void reloadProcessByPid(Long pId) throws Exception {
		JecnTreeBean treeBean = new JecnTreeBean();
		if (pId == 0) {
			JecnTreeNode flowRootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.processRoot, JecnProperties
					.getValue("process"));
			treeBean = flowRootNode.getJecnTreeBean();
		} else {
			// 根据流程ID 获取对应的父ID 封装成Tree对象
			JecnFlowStructureT parentT = ConnectionPool.getProcessAction().getJecnFlowStructureT(pId);
			treeBean.setId(parentT.getFlowId());
			treeBean.setName(parentT.getFlowName());
			treeBean.setTreeNodeType(parentT.getIsFlow() == 0 ? TreeNodeType.processMap : TreeNodeType.process);

		}

		// 获取 缓存中的树节点
		JecnTreeNode parentNode = JecnTreeCommon.getTreeNode(treeBean, JecnDesignerMainPanel.getDesignerMainPanel()
				.getTree());
		// 树节点如果存在 刷新
		if (parentNode != null) {
			Long id = parentNode.getJecnTreeBean().getId();
			try {
				List<JecnTreeBean> list = JecnDesignerCommon.getDesignerProcessTreeBeanList(id, JecnDesignerCommon
						.isOnlyViewDesignAuthor());
				JecnTreeCommon.refreshNode(parentNode, list, JecnDesignerMainPanel.getDesignerMainPanel().getTree());
			} catch (Exception ex) {
				log.error("refreshItemAction is error", ex);
			}
		}
	}

	/**
	 * 
	 * @author zhangchen Aug 1, 2012
	 * @description：获得子节点
	 * @param listAll
	 * @param pJecnTreeBean
	 * @return
	 * 
	 */
	private static List<JecnTreeBean> getChildData(List<JecnTreeBean> listAll, JecnTreeBean pJecnTreeBean) {
		List<JecnTreeBean> listChild = new ArrayList<JecnTreeBean>();
		if (pJecnTreeBean.isChildNode() == false) {
			return listChild;
		}
		for (JecnTreeBean jecnTreeBean : listAll) {
			if (jecnTreeBean.getPid().equals(pJecnTreeBean.getId())) {
				listChild.add(jecnTreeBean);
			}
		}
		return listChild;
	}

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：添加搜索子节点
	 * @param pNode
	 * @param list
	 * @param jTree
	 */
	public static void addChildNodesSearch(JecnTreeNode pNode, List<JecnTreeBean> list, JTree jTree) {
		JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
		// 从数据库获得的子节点
		List<JecnTreeBean> listChild = getChildData(list, pNode.getJecnTreeBean());
		pNode.removeAllChildren();
		jecnTreeModel.reload(pNode);
		// 树现在存在的
		for (int j = 0; j < listChild.size(); j++) {
			JecnTreeBean jecnTreeBeanData = listChild.get(j);
			boolean isExist = false;
			JecnTreeNode node = null;
			for (int i = 0; i < pNode.getChildCount(); i++) {
				node = (JecnTreeNode) pNode.getChildAt(i);
				if (node.getJecnTreeBean().getId().equals(jecnTreeBeanData.getId())) {
					node.setJecnTreeBean(jecnTreeBeanData);
					jecnTreeModel.insertNodeInto(node, pNode, j);
					isExist = true;
				}
			}
			if (!isExist) {
				node = new JecnTreeNode(jecnTreeBeanData);
				jecnTreeModel.insertNodeInto(node, pNode, j);
			}
			addChildNodesSearch(node, list, jTree);

		}
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：获得Tree节点
	 * @param nodeUniqueIdentifier
	 * @param jTree
	 * @return
	 */
	public static JecnTreeNode getTreeNode(String nodeUniqueIdentifier, JTree jTree) {
		// 获得树的model
		// 获得根节点
		JecnTreeNode root = (JecnTreeNode) ((JecnTreeModel) jTree.getModel()).getRoot();
		// root的唯一标识
		String rootUniqueIdentifier = getNodeUniqueIdentifier(root);
		if (nodeUniqueIdentifier.equals(rootUniqueIdentifier)) {
			return root;
		}
		return getTreeNodeChild(nodeUniqueIdentifier, root);
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：删除一个节点
	 * @param jTree
	 * @param node
	 */
	public static void removeNode(JTree jTree, JecnTreeNode node) {
		JecnTreeNode pNode = (JecnTreeNode) node.getParent();
		removeNode(jTree, pNode, node);
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：删除一个节点
	 * @param jTree
	 * @param pNode
	 * @param node
	 * @param jecnTreeModel
	 */
	public static void removeNode(JTree jTree, JecnTreeNode pNode, JecnTreeNode node) {
		// 父节点删除子节点
		pNode.remove(node);
		// 刷新节点
		((JecnTreeModel) jTree.getModel()).reload(pNode);
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：删除多个节点
	 * @param jTree
	 * @param list
	 */
	public static void removeNodes(JTree jTree, List<JecnTreeNode> list) {
		for (JecnTreeNode node : list) {
			JecnTreeNode pNode = (JecnTreeNode) node.getParent();
			removeNode(jTree, pNode, node);
			// 如果删除的不再有子节点，则把数据层改为false;
			if (pNode.getChildCount() == 0) {
				pNode.getJecnTreeBean().setChildNode(false);
			}
		}
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：选中节点
	 * @param jTree
	 * @param node
	 */
	public static void selectNode(JTree jTree, JecnTreeNode node) {
		TreePath treePath = new TreePath(((JecnTreeModel) jTree.getModel()).getPathToRoot(node));
		jTree.setSelectionPath(treePath);
		/** 滚动条显示在选中的节点位置上 */
		jTree.scrollPathToVisible(treePath);
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：tree节点重命名
	 * @param jTree
	 * @param node
	 * @param newName
	 */
	public static void reNameNode(JTree jTree, JecnTreeNode node, String newName) {
		node.getJecnTreeBean().setName(newName);
		node.setUserObject(JecnTreeCommon.getShowName(node.getJecnTreeBean()));
		reload(jTree, node);
	}

	/** 刷新 */
	public static void reload(JTree jTree, JecnTreeNode node) {
		((JecnTreeModel) jTree.getModel()).reload(node);
	}

	/**
	 * 节点排序
	 * 
	 * @param jTree
	 * @param list
	 * @param pNode
	 * @param listTreeNodeDrag
	 */
	public static void sortNodes(JTree jTree, JecnTreeNode pNode, List<JecnTreeDragBean> listJecnTreeDragBean) {
		if (pNode.getChildCount() > 0) {
			List<JecnTreeNode> list = new ArrayList<JecnTreeNode>();
			for (int i = 0; i < pNode.getChildCount(); i++) {
				list.add((JecnTreeNode) pNode.getChildAt(i));
			}
			pNode.removeAllChildren();
			JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
			jecnTreeModel.reload(pNode);
			for (JecnTreeDragBean jecnTreeDragBean : listJecnTreeDragBean) {
				// 排序的唯一标识
				String idUniqueIdentifier = jecnTreeDragBean.getTreeNodeType().toString()
						+ jecnTreeDragBean.getId().toString();
				for (JecnTreeNode node : list) {
					// node的唯一标识
					String nodeUniqueIdentifier = getNodeUniqueIdentifier(node);
					if (idUniqueIdentifier.equals(nodeUniqueIdentifier)) {
						int index = pNode.getChildCount();
						jecnTreeModel.insertNodeInto(node, pNode, index);
					}
				}
			}
		}
		selectNode(jTree, pNode);
	}

	/**
	 * 
	 * @author zhangchen May 2, 2012
	 * @description： tree节点删除,并选中父节点
	 * @param jTree
	 * @param node
	 * @param pNode
	 */
	public static void deleteNodeAndSelectPNode(JTree jTree, JecnTreeNode node, JecnTreeNode pNode) {
		pNode.remove(node);
		JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
		jecnTreeModel.reload(pNode);
		selectNode(jTree, pNode);
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：父节点添加单一节点
	 * @param jTree
	 * @param newNode
	 * @param pNode
	 */
	public static void addNode(JTree jTree, JecnTreeNode newNode, JecnTreeNode pNode) {
		if (pNode.isLeaf()) {
			pNode.getJecnTreeBean().setChildNode(true);
		}
		// 添加子节点
		((JecnTreeModel) jTree.getModel()).insertNodeInto(newNode, pNode, pNode.getChildCount());
		// 刷新节点
		((JecnTreeModel) jTree.getModel()).reload(pNode);
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:添加节点并选中节点
	 * @param jTree
	 * @param jecnTreeBean
	 * @param pNode
	 */
	public static void addNodeAndSelectNode(JTree jTree, JecnTreeBean jecnTreeBean, JecnTreeNode pNode) {
		JecnTreeNode node = new JecnTreeNode(jecnTreeBean);
		addNode(jTree, node, pNode);
		JecnTreeCommon.autoExpandNode(jTree, pNode);
		selectNode(jTree, node);
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：删除所有的子节点
	 * @param jecnTreeModel
	 * @param pNode
	 */
	public static void deleteAllChilds(JecnTreeModel jecnTreeModel, JecnTreeNode pNode) {
		if (pNode.getChildCount() == 0) {
			return;
		}
		pNode.removeAllChildren();
		jecnTreeModel.nodeChanged(pNode);
		jecnTreeModel.reload(pNode);
	}

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：搜索节点
	 * @param list
	 * @param jecnTreeBean
	 * @param jTree
	 * @param treeNodeType
	 *            根节点类型
	 */
	public static JecnTreeNode searchNode(List<JecnTreeBean> list, Long id, JTree jTree, TreeNodeType rootNodeType,
			TreeNodeType nodeType) {
		// 根节点
		JecnTreeNode searchRootNode = getSearchRootNode(jTree, rootNodeType);
		if (searchRootNode == null) {
			return null;
		}
		// 添加搜索节点
		addChildNodesSearch(searchRootNode, list, jTree);
		// 添加岗位组
		addPosGroupRootToOrgRoot(jTree, searchRootNode);
		// 获得树节点
		JecnTreeNode selectNode = JecnTreeCommon.getTreeNode(JecnTreeCommon.getNodeUniqueIdentifier(id, nodeType),
				jTree);
		if (selectNode != null) {
			JecnTreeCommon.selectNode(jTree, selectNode);
		}
		return selectNode;
	}

	/**
	 * 
	 * @desc:将岗位组添加到组织根节点公共方法
	 * 
	 * **/
	public static void addPosGroupRootToOrgRoot(JTree jTree, JecnTreeNode orgRootNode) {
		if (!orgRootNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.organizationRoot)) {
			return;
		}
		JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
		// 岗位组
		JecnTreeNode positionGroupRootNode = new JecnTreeNode(JecnTreeCommon.createTreeRoot(
				TreeNodeType.positionGroupRoot, JecnProperties.getValue("positionGroup")).getJecnTreeBean());
		// 将岗位组添加到
		jecnTreeModel.insertNodeInto(positionGroupRootNode, orgRootNode, 0);
	}

	/**
	 * @author yxw 2012-8-9
	 * @description:岗位搜索
	 * @param id
	 * @param type
	 * @param jTree
	 * @param orgId
	 */
	public static void searchNodePos(Long id, TreeNodeType type, JTree jTree, Long orgId) throws Exception {
		// 节点唯一标示
		String nodeUniqueIdentifier = JecnTreeCommon.getNodeUniqueIdentifier(id, type);
		// 判断节点是不是已经在数节点上
		JecnTreeNode node = JecnTreeCommon.getTreeNode(nodeUniqueIdentifier, jTree);
		if (node != null) {
			JecnTreeCommon.selectNode(jTree, node);
			return;
		}
		List<JecnTreeBean> list = ConnectionPool.getOrganizationAction().getPnodesPos(orgId, JecnConstants.projectId);
		TreeNodeType rootNodeType = TreeNodeType.organizationRoot;
		JecnTreeCommon.searchNode(list, id, jTree, rootNodeType, type);
	}

	/**
	 * @author yxw 2012-8-9
	 * @description:岗位搜索
	 * @param jecnTreeBean
	 * @param jTree
	 * @param orgId
	 */
	public static void searchNodePos(JecnTreeBean jecnTreeBean, JTree jTree) throws Exception {
		searchNodePos(jecnTreeBean.getId(), jecnTreeBean.getTreeNodeType(), jTree, jecnTreeBean.getPid());
	}

	/**
	 * @author yxw 2012-8-9
	 * @description:搜索节点
	 * @param jecnTreeBean
	 * @param jTree
	 * @throws Exception
	 */
	public static void searchNode(JecnTreeBean jecnTreeBean, JTree jTree) throws Exception {
		searchNode(jecnTreeBean.getId(), jecnTreeBean.getTreeNodeType(), jTree);
	}

	/**
	 * @author yxw 2012-8-9
	 * @description:搜索节点
	 * @param jecnTreeBean
	 * @param jTree
	 * @throws Exception
	 */
	public static JecnTreeNode getSearchNode(JecnTreeBean jecnTreeBean, JTree jTree) throws Exception {
		return searchNode(jecnTreeBean.getId(), jecnTreeBean.getTreeNodeType(), jTree);
	}

	/**
	 * @author yxw 2012-8-9
	 * @description:搜索节点
	 * @param id
	 * @param type
	 * @param jTree
	 * @throws Exception
	 */
	public static JecnTreeNode searchNode(Long id, TreeNodeType type, JTree jTree) throws Exception {
		// 节点唯一标示
		String nodeUniqueIdentifier = JecnTreeCommon.getNodeUniqueIdentifier(id, type);
		// 判断节点是不是已经在数节点上
		JecnTreeNode node = JecnTreeCommon.getTreeNode(nodeUniqueIdentifier, jTree);
		if (node != null) {
			JecnTreeCommon.selectNode(jTree, node);
			return node;
		}
		TreeNodeType rootNodeType = null;
		List<JecnTreeBean> list = null;
		switch (type) {
		case process:
		case processMap:
		case processFile:
			if (jTree.equals(JecnDesignerMainPanel.getDesignerMainPanel().getTreePanel().getTree())
					&& JecnDesignerCommon.isOnlyViewDesignAuthor()) {
				list = JecnDesignerCommon.getDesignerResultList(ConnectionPool.getProcessAction().getPnodes(id,
						JecnConstants.projectId));
			} else {
				list = ConnectionPool.getProcessAction().getPnodes(id, JecnConstants.projectId);
			}
			rootNodeType = TreeNodeType.processRoot;
			break;
		case ruleDir:
		case ruleModeFile:
		case ruleFile:
			list = ConnectionPool.getRuleAction().getPnodes(id, JecnConstants.projectId);
			rootNodeType = TreeNodeType.ruleRoot;
			break;
		case organization:
			list = ConnectionPool.getOrganizationAction().getPnodesOrg(id, JecnConstants.projectId);
			rootNodeType = TreeNodeType.organizationRoot;
			break;
		case standardDir:
		case standard:
		case standardClause:
		case standardClauseRequire:
			list = ConnectionPool.getStandardAction().getPnodes(id, JecnConstants.projectId);
			rootNodeType = TreeNodeType.standardRoot;
			break;
		case fileDir:
		case file:
			list = ConnectionPool.getFileAction().getPnodes(id, JecnConstants.projectId);
			rootNodeType = TreeNodeType.fileRoot;
			break;
		case termDefineDir:
		case termDefine:
			list = ConnectionPool.getTermDefinitionAction().getPnodes(id);
			rootNodeType = TreeNodeType.termDefineRoot;
			break;
		default:
			break;
		}
		if (list != null && rootNodeType != null) {
			return JecnTreeCommon.searchNode(list, id, jTree, rootNodeType, type);
		}
		return null;
	}

	/**
	 * 立邦-自动编号节点定位
	 * 
	 * @param id
	 * @param type
	 * @param jTree
	 * @param codeType
	 * @return
	 * @throws Exception
	 */
	public static JecnTreeNode searchNode(Long id, TreeNodeType type, JTree jTree, int codeType) throws Exception {
		// 节点唯一标示
		String nodeUniqueIdentifier = JecnTreeCommon.getNodeUniqueIdentifier(id, type);
		// 判断节点是不是已经在数节点上
		JecnTreeNode node = JecnTreeCommon.getTreeNode(nodeUniqueIdentifier, jTree);
		if (node != null) {
			JecnTreeCommon.selectNode(jTree, node);
			return node;
		}
		if (type != TreeNodeType.processCode) {
			return null;
		}
		TreeNodeType rootNodeType = null;
		List<JecnTreeBean> list = null;

		list = ConnectionPool.getAutoCodeAction().getPnodes(id, codeType);
		rootNodeType = TreeNodeType.processCodeRoot;
		if (list != null && rootNodeType != null) {
			return JecnTreeCommon.searchNode(list, id, jTree, rootNodeType, type);
		}
		return null;
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：添加树的子节点
	 * @param jTree
	 * @param list
	 * @param pNode
	 * @param jecnTreeModel
	 */
	private static void addTreeChildNode(JTree jTree, List<JecnTreeBean> list, JecnTreeNode pNode) {
		for (JecnTreeBean JecnTreeBean : list) {
			/** 通过JecnTreeBean获得NewTreeNode对象 */
			JecnTreeNode node = new JecnTreeNode(JecnTreeBean);
			addNode(jTree, node, pNode);
		}
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：添加树的子节点
	 * @param jTree
	 * @param list
	 * @param pNode
	 * @param jecnTreeModel
	 * @param listIds
	 */
	private static void addTreeChildMoveNode(JTree jTree, List<JecnTreeBean> list, JecnTreeNode pNode,
			JecnTreeModel jecnTreeModel, List<Long> listIds) {
		for (JecnTreeBean JecnTreeBean : list) {
			/** 通过JecnTreeBean获得NewTreeNode对象 */
			JecnTreeNode node = new JecnTreeNode(JecnTreeBean);
			if (!listIds.contains(node.getJecnTreeBean().getId())) {
				addNode(jTree, node, pNode);
			}
		}
	}

	/**
	 * @author yxw 2013-1-11
	 * @description:组织岗位移动专用
	 * @param jTree
	 * @param list
	 *            加载的节点集合
	 * @param pNode
	 * @param jecnTreeModel
	 * @param listIds
	 *            不需要加载的节点集合
	 */
	private static void addTreeChildMoveNodeOrg(JTree jTree, List<JecnTreeBean> list, JecnTreeNode pNode,
			JecnTreeModel jecnTreeModel, List<Long> listIds) {
		for (JecnTreeBean jecnTreeBean : list) {
			/** 通过JecnTreeBean获得NewTreeNode对象 */
			JecnTreeNode node = new JecnTreeNode(jecnTreeBean);
			if (!(listIds.contains(node.getJecnTreeBean().getId()) && (((JecnTree) jTree).getNodeType() == jecnTreeBean
					.getTreeNodeType()))) {
				addNode(jTree, node, pNode);
			}
		}
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：点击节点展开操作，加载子节点（一种类型节点）
	 * @param jTree
	 * @param list
	 * @param pNode
	 */
	public static void expansionTreeNode(JTree jTree, List<JecnTreeBean> list, JecnTreeNode pNode) {
		// 重新添加子节点
		addTreeChildNode(jTree, list, pNode);
		// 刷新节点
		((JecnTreeModel) jTree.getModel()).reload(pNode);
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：点击节点展开操作，加载子节点（一种类型节点）
	 * @param jTree
	 * @param list
	 * @param pNode
	 * @param listIds
	 */
	public static void expansionTreeMoveNode(JTree jTree, List<JecnTreeBean> list, JecnTreeNode pNode,
			List<Long> listIds) {
		JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
		// 重新添加子节点
		addTreeChildMoveNode(jTree, list, pNode, jecnTreeModel, listIds);
		// 刷新节点
		jecnTreeModel.reload(pNode);
	}

	/**
	 * 
	 * @author yxw 2013-1-11
	 * @description:点击节点展开操作，加载子节点（一种类型节点）组织移动专用
	 * @param jTree
	 * @param list
	 * @param pNode
	 * @param listIds
	 */
	public static void expansionTreeMoveNodeOrg(JTree jTree, List<JecnTreeBean> list, JecnTreeNode pNode,
			List<Long> listIds) {
		JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
		// 重新添加子节点
		addTreeChildMoveNodeOrg(jTree, list, pNode, jecnTreeModel, listIds);
		// 刷新节点
		jecnTreeModel.reload(pNode);
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：判断节点存不存在子节点中（递归）
	 * @param id
	 * @param pNode
	 * @return
	 */
	private static boolean isPerChild(Long id, JecnTreeNode pNode) {
		for (int i = 0; i < pNode.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) pNode.getChildAt(i);
			if (id.equals(node.getJecnTreeBean().getId())) {
				return true;
			}
			if (isPerChild(id, node)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：判断节点存不存在子节点中
	 * @param id
	 * @param pNode
	 * @return
	 */
	private static boolean isChild(Long id, JecnTreeNode pNode) {
		for (int i = 0; i < pNode.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) pNode.getChildAt(i);
			if (id.equals(node.getJecnTreeBean().getId())) {
				return true;
			}
			if (isPerChild(id, node)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：多选时,去掉子节点
	 * @param listNodes
	 * @param jTree
	 * @return
	 */
	public static List<JecnTreeNode> repeatChildNodes(List<JecnTreeNode> listNodes, JTree jTree) {
		List<JecnTreeNode> removeRepeatNodes = new ArrayList<JecnTreeNode>();
		for (JecnTreeNode node : listNodes) {
			boolean isChild = false;
			for (JecnTreeNode pNode : listNodes) {
				if (node.getJecnTreeBean().getId().equals(pNode.getJecnTreeBean().getId())) {
					continue;
				}
				isChild = isChild(node.getJecnTreeBean().getId(), pNode);
				if (isChild) {
					break;
				}
			}
			if (!isChild) {
				removeRepeatNodes.add(node);
			}
		}
		return removeRepeatNodes;
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：移动节点
	 * @param moveList
	 * @param pNode
	 * @param jTree
	 */
	public static void moveNodes(List<JecnTreeNode> moveList, JecnTreeNode pNode, JTree jTree) {
		// if (pNode.getChildCount() == 0) {
		// JecnTreeCommon.autoExpandNode(jTree, pNode);
		// }
		for (JecnTreeNode jecnTreeNode : moveList) {
			// 删除以前的节点位置
			removeNode(jTree, jecnTreeNode);
			// 添加新的节点位置
			addNode(jTree, jecnTreeNode, pNode);

		}
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：找到关键字节点（mysql）
	 * @param jTree
	 * @param name
	 * @return
	 */
	public static List<JecnTreeBean> findAllTreeNodes(JTree jTree, String name) {
		List<JecnTreeBean> listAll = new ArrayList<JecnTreeBean>();
		// 获得树的model
		JecnTreeModel treeMode = (JecnTreeModel) jTree.getModel();
		// 获得根节点
		JecnTreeNode root = (JecnTreeNode) treeMode.getRoot();
		if (root.getJecnTreeBean().getName().indexOf(name) != -1) {
			listAll.add(root.getJecnTreeBean());
		}
		for (int i = 0; i < root.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) root.getChildAt(i);
			if (node.getJecnTreeBean().getName().indexOf(name) != -1) {
				listAll.add(node.getJecnTreeBean());
				if (listAll.size() > 20) {
					return listAll;
				}
			}
			findAllTreeNodes(node, name, listAll, new ArrayList<JecnTreeNode>());
		}
		return listAll;
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：找到关键字节点（mysql 移动节点搜索）
	 * @param jTree
	 * @param name
	 * @param listDrag
	 * @return
	 */
	public static List<JecnTreeBean> findAllTreeNodesDrag(JTree jTree, String name, List<JecnTreeNode> listMoveNodes) {
		List<JecnTreeBean> listAll = new ArrayList<JecnTreeBean>();
		// 获得树的model
		JecnTreeModel treeMode = (JecnTreeModel) jTree.getModel();
		// 获得根节点
		JecnTreeNode root = (JecnTreeNode) treeMode.getRoot();
		if (root.getJecnTreeBean().getName().indexOf(name) != -1) {
			listAll.add(root.getJecnTreeBean());
		}
		for (int i = 0; i < root.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) root.getChildAt(i);
			if (isExistTreeNode(node, listMoveNodes)) {
				continue;
			}
			if (node.getJecnTreeBean().getName().indexOf(name) != -1) {
				listAll.add(node.getJecnTreeBean());
				if (listAll.size() > 20) {
					return listAll;
				}
			}
			findAllTreeNodes(node, name, listAll, listMoveNodes);
		}
		return listAll;
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：list集合存不存在tree节点
	 * @param node
	 * @param listDrag
	 * @return
	 */
	private static boolean isExistTreeNode(JecnTreeNode node, List<JecnTreeNode> listDrag) {
		for (JecnTreeNode jecnTreeNode : listDrag) {
			if (getNodeUniqueIdentifier(node).equals(getNodeUniqueIdentifier(jecnTreeNode))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：找到关键字节点递归
	 * @param pNode
	 * @param name
	 * @param listAll
	 * @param listDrag
	 */
	private static void findAllTreeNodes(JecnTreeNode pNode, String name, List<JecnTreeBean> listAll,
			List<JecnTreeNode> listMoveNodes) {
		for (int i = 0; i < pNode.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) pNode.getChildAt(i);
			if (isExistTreeNode(node, listMoveNodes)) {
				continue;
			}
			if (node.getJecnTreeBean().getName().indexOf(name) != -1) {
				listAll.add(node.getJecnTreeBean());
				if (listAll.size() > 20) {
					break;
				}
			}
			findAllTreeNodes(node, name, listAll, listMoveNodes);
		}
	}

	/**
	 * 刷新节点
	 * 
	 * @author zhangchen May 2, 2012
	 * @description：
	 * @param node
	 * @param listChild
	 * @param jTree
	 */
	public static void refreshNode(JecnTreeNode node, List<JecnTreeBean> listChild, JTree jTree) {
		JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
		// 删除子节点
		deleteAllChilds(jecnTreeModel, node);
		for (JecnTreeBean jecnTreeBean : listChild) {
			// 添加新节点
			addNode(jTree, new JecnTreeNode(jecnTreeBean), node);
		}
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：找到节点下所有的子节点（包括自己）
	 * @param jTree
	 * @param nodeUniqueIdentifier
	 * @return
	 */
	public static List<JecnTreeBean> findAllTreeNodesByNodeUniqueIdentifier(JTree jTree, String nodeUniqueIdentifier) {
		List<JecnTreeBean> listAll = new ArrayList<JecnTreeBean>();
		// 获得根节点
		JecnTreeNode jecnTreeNode = JecnTreeCommon.getTreeNode(nodeUniqueIdentifier, jTree);
		if (jecnTreeNode == null) {
			return listAll;
		}
		listAll.add(jecnTreeNode.getJecnTreeBean());
		for (int i = 0; i < jecnTreeNode.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) jecnTreeNode.getChildAt(i);
			listAll.add(node.getJecnTreeBean());
			findAllTreeNodesByNodeUniqueIdentifier(listAll, node);
		}
		return listAll;
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：找到节点下所有的子节点（递归）
	 * @param listAll
	 * @param jecnTreeNode
	 */
	private static void findAllTreeNodesByNodeUniqueIdentifier(List<JecnTreeBean> listAll, JecnTreeNode jecnTreeNode) {
		for (int i = 0; i < jecnTreeNode.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) jecnTreeNode.getChildAt(i);
			listAll.add(node.getJecnTreeBean());
			findAllTreeNodesByNodeUniqueIdentifier(listAll, node);
		}
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:一次加载list的所有
	 * @param list
	 * @param pNode
	 */
	public static void addNLevelNodes(List<JecnTreeBean> list, JecnTreeNode pNode) {
		for (JecnTreeBean jecnTreeBean : list) {
			if (pNode.getJecnTreeBean().getId().equals(jecnTreeBean.getPid())) {
				JecnTreeNode node = new JecnTreeNode(jecnTreeBean);
				pNode.add(node);
				if (jecnTreeBean.isChildNode()) {
					addNLevelNodes(list, node);
				}

			}
		}
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:一次加载list的所有
	 * @param list
	 * @param pNode
	 * @param listIds
	 */
	public static void addNLevelMoveNodes(List<JecnTreeBean> list, JecnTreeNode pNode, List<Long> listIds) {
		if (list == null) {
			return;
		}
		for (JecnTreeBean jecnTreeBean : list) {
			if (listIds.contains(jecnTreeBean.getId())) {
				continue;
			}
			if (pNode.getJecnTreeBean().getId().equals(jecnTreeBean.getPid())) {
				JecnTreeNode node = new JecnTreeNode(jecnTreeBean);
				pNode.add(node);
				addNLevelNodes(list, node);
			}
		}
	}

	public static void addNLevelMoveNodes(List<JecnTreeBean> list, JecnTreeNode pNode, List<Long> listIds,
			TreeNodeType nodeType) {
		if (list == null) {
			return;
		}
		for (JecnTreeBean jecnTreeBean : list) {
			if (listIds.contains(jecnTreeBean.getId()) && jecnTreeBean.getTreeNodeType() == nodeType) {
				continue;
			}
			if (pNode.getJecnTreeBean().getId().equals(jecnTreeBean.getPid())) {
				JecnTreeNode node = new JecnTreeNode(jecnTreeBean);
				pNode.add(node);
				addNLevelNodes(list, node);
			}
		}
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:判断节点是否展开
	 * @param jTree
	 * @param node
	 * @return
	 */
	public static boolean isExpandNode(JTree jTree, JecnTreeNode node) {
		JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
		TreePath treePath = new TreePath(jecnTreeModel.getPathToRoot(node));
		return isExpandNode(jTree, treePath);
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:判断节点是否展开
	 * @param jTree
	 * @param node
	 * @param treePath
	 * @return
	 */
	public static boolean isExpandNode(JTree jTree, TreePath treePath) {
		if (jTree.isExpanded(treePath)) {
			return true;
		}
		return false;
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:自动展开节点
	 * @param jTree
	 * @param node
	 */
	public static void autoExpandNode(JTree jTree, JecnTreeNode node) {
		if (node.isLeaf()) {
			return;
		}
		JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
		TreePath treePath = new TreePath(jecnTreeModel.getPathToRoot(node));
		if (!isExpandNode(jTree, treePath)) {
			jTree.expandPath(treePath);
		}
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:创建树的根节点
	 * @param treeNodeType
	 * @param rootName
	 * @return
	 */
	public static JecnTreeNode createTreeRoot(TreeNodeType treeNodeType, String rootName) {
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		jecnTreeBean.setTreeNodeType(treeNodeType);
		jecnTreeBean.setId(Long.valueOf(0));
		jecnTreeBean.setName(rootName);
		jecnTreeBean.setPid(Long.valueOf(0));
		jecnTreeBean.setNumberId("0");
		jecnTreeBean.setPname(rootName);
		jecnTreeBean.setChildNode(true);
		return new JecnTreeNode(jecnTreeBean);
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:新建时，判断是否重名
	 * @param pNode
	 * @param name
	 * @param treeNodeType
	 * @return 重名返回true 不重命名返回false
	 */
	public static boolean validateRepeatNameAdd(JecnTreeNode pNode, String name, TreeNodeType treeNodeType) {
		for (int i = 0; i < pNode.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) pNode.getChildAt(i);
			if (node.getJecnTreeBean().getTreeNodeType().equals(treeNodeType)
					&& name.equals(node.getJecnTreeBean().getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:编辑时，判断是否重名
	 * @param pNode
	 * @param name
	 * @param jecnTreeNode
	 * @return 重命时返回true,不重命返false
	 */
	public static boolean validateRepeatNameEidt(JecnTreeNode pNode, String name, JecnTreeNode jecnTreeNode) {
		for (int i = 0; i < pNode.getChildCount(); i++) {
			JecnTreeNode node = (JecnTreeNode) pNode.getChildAt(i);
			if (getNodeUniqueIdentifier(node).equals(getNodeUniqueIdentifier(jecnTreeNode))) {
				continue;
			}
			if (node.getJecnTreeBean().getTreeNodeType().equals(jecnTreeNode.getJecnTreeBean().getTreeNodeType())
					&& name.equals(node.getJecnTreeBean().getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:编辑时，判断是否重名
	 * @param name
	 * @param jecnTreeNode
	 * @return
	 */
	public static boolean validateRepeatNameEidt(String name, JecnTreeNode jecnTreeNode) {
		JecnTreeNode pNode = (JecnTreeNode) jecnTreeNode.getParent();
		return validateRepeatNameEidt(pNode, name, jecnTreeNode);
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:获得新节点的排序
	 * @param pNode
	 * @return
	 */
	public static int getMaxSort(JecnTreeNode pNode) {
		int sortId = -1;
		try {
			sortId = getMaxSort(pNode.getJecnTreeBean());
		} catch (Exception e) {
			log.error("", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("GettingNodeMaxSortException"));
			return -1;
		}
		if (sortId != -1) {
			return sortId;
		}
		return pNode.getChildCount();
	}

	public static int getMaxSort(JecnTreeBean treeBean) {
		try {
			return ConnectionPool.getTreeAction().getTreeChildMaxSortId(treeBean);
		} catch (Exception e) {
			log.error("", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("GettingNodeMaxSortException"));
			return -1;
		}
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:获得单一节点的所有子节点（包括自己）
	 * @param jecnTreeNode
	 * @param listIds
	 */
	private static void getSingleAllChildIds(JecnTreeNode jecnTreeNode, List<Long> listIds) {
		if (!listIds.contains(jecnTreeNode.getJecnTreeBean().getId())) {
			listIds.add(jecnTreeNode.getJecnTreeBean().getId());
		}
		for (int i = 0; i < jecnTreeNode.getChildCount(); i++) {
			JecnTreeNode childNode = (JecnTreeNode) jecnTreeNode.getChildAt(i);
			getSingleAllChildIds(childNode, listIds);
		}
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:获得所有节点的所有子节点（包括自己）
	 * @param list
	 * @return
	 */
	public static List<Long> getAllChildIds(List<JecnTreeNode> list) {
		List<Long> listIds = new ArrayList<Long>();
		for (JecnTreeNode jecnTreeNode : list) {
			getSingleAllChildIds(jecnTreeNode, listIds);
		}
		return listIds;
	}

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：新建制度获得制度模板ID
	 * @param selectNode
	 * @return
	 */
	public static Long getRuleModeId(JecnTreeNode selectNode) {
		if (selectNode.getJecnTreeBean().getModeId() != null) {
			return selectNode.getJecnTreeBean().getModeId();
		}
		JecnTreeNode pNode = (JecnTreeNode) selectNode.getParent();
		if (pNode != null) {
			if (TreeNodeType.ruleRoot.equals(pNode.getJecnTreeBean().getTreeNodeType())) {
				return null;
			}
			if (pNode.getJecnTreeBean().getModeId() != null) {
				return pNode.getJecnTreeBean().getModeId();
			} else {
				return getRuleModeId(pNode);
			}
		}
		return null;
	}

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：判断树节点有没有权限,并添加提示
	 * @param node
	 * @return true有权限 false 没权限
	 */
	public static boolean isAuthNodeAddTip(JecnTreeNode node, Component c) {
		int authType = isAuthNode(node);
		if (authType == 1 || authType == 2) {
			return true;
		}
		JecnLoading.stop();
		JecnOptionPane.showMessageDialog(c, JecnProperties.getValue("noPopedomOp"));
		return false;
	}

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：判断树节点有没有权限,不添加提示
	 * @param node
	 * @return true有权限 false 没权限
	 */
	public static boolean isAuthNode(JecnTreeNode node, Component c) {
		int authType = isAuthNode(node);
		if (authType == 1 || authType == 2) {
			return true;
		}
		JecnLoading.stop();
		return false;
	}

	/**
	 * @author yxw 2012-8-17
	 * @description:判断是否有操作权限
	 * @param id
	 * @param treeNodeType
	 * @param c
	 * @return
	 * @throws Exception
	 */
	public static boolean isAuth(Long id, TreeNodeType treeNodeType, Component c) {
		try {
			if (JecnConstants.loginBean == null) {
				JecnOptionPane.showMessageDialog(c, JecnProperties.getValue("noPopedomOp"));
				return false;
			}
			// 是不是管理员
			if (JecnConstants.loginBean.isAdmin()) {
				return true;
			}

			switch (treeNodeType) {
			case file:// 文件
				if (JecnConstants.loginBean.getSetFile() != null && JecnConstants.loginBean.getSetFile().size() > 0) {
					if (JecnConstants.loginBean.getSetFile().contains(id)) {
						return true;
					}
					if (ConnectionPool.getFileAction().isFileAuth(id, JecnConstants.projectId,
							JecnConstants.loginBean.getSetFile(), treeNodeType)) {
						return true;
					}
				}
				break;
			case processMap:// 流程地图
			case process: // 流程图
			case standardProcess: // 流程标准(以流程ID为判断标准)
			case standardProcessMap:// 流程地图标准(以流程地图ID为判断标准)
				if (JecnConstants.loginBean.getSetFlow() != null && JecnConstants.loginBean.getSetFlow().size() > 0) {
					if (JecnConstants.loginBean.getSetFlow().contains(id)) {
						return true;
					}
					if (ConnectionPool.getFileAction().isFileAuth(id, JecnConstants.projectId,
							JecnConstants.loginBean.getSetFlow(), treeNodeType)) {
						return true;
					}
				}
				break;
			case ruleFile:// 制度文件
			case ruleModeFile:// 制度模板文件
				if (JecnConstants.loginBean.getSetRule() != null && JecnConstants.loginBean.getSetRule().size() > 0) {
					if (JecnConstants.loginBean.getSetRule().contains(id)) {
						return true;
					}
					if (ConnectionPool.getFileAction().isFileAuth(id, JecnConstants.projectId,
							JecnConstants.loginBean.getSetRule(), treeNodeType)) {
						return true;
					}
				}
				break;
			case standard: // 文件标准
			case standardClause:// 标准条款
			case standardClauseRequire:// 条款要求
				if (JecnConstants.loginBean.getSetStandiad() != null
						&& JecnConstants.loginBean.getSetStandiad().size() > 0) {
					if (JecnConstants.loginBean.getSetStandiad().contains(id)) {
						return true;
					}
					if (ConnectionPool.getFileAction().isFileAuth(id, JecnConstants.projectId,
							JecnConstants.loginBean.getSetStandiad(), treeNodeType)) {
						return true;
					}
				}
				break;
			case riskPoint:// 风险点
				if (JecnConstants.loginBean.getSetRisk() != null && JecnConstants.loginBean.getSetRisk().size() > 0) {
					if (JecnConstants.loginBean.getSetRisk().contains(id)) {
						return true;
					}
					if (ConnectionPool.getFileAction().isFileAuth(id, JecnConstants.projectId,
							JecnConstants.loginBean.getSetRisk(), treeNodeType)) {
						return true;
					}
				}
				break;
			default:
				break;
			}
		} catch (Exception e) {
			log.error("JecnTreeCommon is error", e);
		}
		// 执行到此步表示没有权限操作
		JecnOptionPane.showMessageDialog(c, JecnProperties.getValue("noPopedomOp"));
		return false;
	}

	/**
	 * @author zhr 2018-10-25
	 * @description:判断是否有查阅权限 因为浏览端 树节点也没有 向上查找 部门 所以统一更改
	 * @param id
	 * @param treeNodeType
	 *            * 0是流程、1是文件、2是标准、3制度
	 * @param c
	 * @return
	 * @throws Exception
	 */
	public static boolean isAccessAuthority(Long id, TreeNodeType treeNodeType, Component c) {
		try {
			if (JecnConstants.loginBean == null) {
				JecnOptionPane.showMessageDialog(c, JecnProperties.getValue("noPopedomOp"));
				return false;
			}
			// 是不是管理员
			if (JecnConstants.loginBean.isAdmin()) {
				return true;
			}
			LookPopedomBean accessPermissions = null;
			switch (treeNodeType) {
			case file:// 文件
				accessPermissions = ConnectionPool.getOrganizationAction().getAccessPermissions(id, 1);
				break;
			case processMap:// 流程地图
				if (JecnConfigTool.isPubTotalMap()) {
					return true;
				}
				accessPermissions = ConnectionPool.getOrganizationAction().getAccessPermissions(id, 0);
				break;
			case process: // 流程图
			case standardProcess: // 流程标准(以流程ID为判断标准)
			case standardProcessMap:// 流程地图标准(以流程地图ID为判断标准)
				if (JecnConfigTool.isPubPartMap()) {
					return true;
				}
				accessPermissions = ConnectionPool.getOrganizationAction().getAccessPermissions(id, 0);
				break;
			case ruleFile:// 制度文件
			case ruleModeFile:// 制度模板文件
				accessPermissions = ConnectionPool.getOrganizationAction().getAccessPermissions(id, 3);
				break;
			case standard: // 文件标准
			case standardClause:// 标准条款
			case standardClauseRequire:// 条款要求
				accessPermissions = ConnectionPool.getOrganizationAction().getAccessPermissions(id, 2);
				break;
			default:
				break;
			}
			// 公开 后期加入 因为觉得 原先的需求不合适 公用版本.
			if (accessPermissions.getIsPublic() != 0) {
				return true;
			}

			// 获取人员 所在部门岗位岗位组 (部门中包含 本身部门和 父部门)
			AccessId accIds = ConnectionPool.getOrganizationAction().getPepoleOrgAndPosAndPosPosGroup(
					JecnConstants.getUserId(), JecnConstants.projectId);

			// 部门
			List<AccessId> orgAccessId = accIds.getOrgAccessId();
			for (AccessId accessId : orgAccessId) {
				for (JecnTreeBean org : accessPermissions.getOrgList()) {
					if (accessId.getAccessId().equals(org.getId())) {
						return true;
					}
				}
			}

			// 岗位 精确查询
			List<AccessId> posAccessId = accIds.getPosAccessId();
			for (AccessId accessId : posAccessId) {
				for (JecnTreeBean pos : accessPermissions.getPosList()) {
					if (accessId.getAccessId().equals(pos.getId())) {
						return true;
					}
				}
			}

			// 岗位组 精确查询
			List<AccessId> posGroupAccessId = accIds.getPosGroupAccessId();
			for (AccessId accessId : posGroupAccessId) {
				for (JecnTreeBean posGroup : accessPermissions.getPosGroupList()) {
					if (accessId.getAccessId().equals(posGroup.getId())) {
						return true;
					}
				}
			}

		} catch (Exception e) {
			log.error("JecnTreeCommon is error", e);
		}
		return false;
	}

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：判断多个树节点有没有权限
	 * @param nodeList
	 *            节点集合
	 * @return
	 */
	public static boolean isAuthNodes(List<JecnTreeNode> nodeList) {
		if (nodeList != null) {
			for (JecnTreeNode node : nodeList) {
				int authType = isAuthNode(node);
				if (authType == 0 || authType == 1) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：判断树节点有没有权限
	 * 
	 * @param node
	 * @return 0是没有权限，1是设计者权限但是是管理员制定节点 设计者不能重命名，删除和移动，2是操作权限
	 */
	public static int isAuthNode(JecnTreeNode node) {
		if (JecnConstants.loginBean == null) {
			return 0;
		}
		// 是不是管理员
		if (JecnConstants.loginBean.isAdmin()) {
			return 2;
		}

		// 权限集合
		Set<Long> setIds = null;
		// 根节点类型
		TreeNodeType treeNodeType = null;
		// 流程
		if (JecnTreeCommon.isProcessTreeNodeType(node.getJecnTreeBean().getTreeNodeType())) {
			treeNodeType = TreeNodeType.processRoot;
			setIds = JecnConstants.loginBean.getSetFlow();
		} else if (JecnTreeCommon.isFileTreeNodeType(node.getJecnTreeBean().getTreeNodeType())) {
			treeNodeType = TreeNodeType.fileRoot;
			setIds = JecnConstants.loginBean.getSetFile();
		} else if (JecnTreeCommon.isStandaidTreeNodeType(node.getJecnTreeBean().getTreeNodeType())) {
			treeNodeType = TreeNodeType.standardRoot;
			setIds = JecnConstants.loginBean.getSetStandiad();
		} else if (JecnTreeCommon.isRuleTreeNodeType(node.getJecnTreeBean().getTreeNodeType())) {
			treeNodeType = TreeNodeType.ruleRoot;
			setIds = JecnConstants.loginBean.getSetRule();
		} else if (JecnTreeCommon.isRiskTreeNodeType(node.getJecnTreeBean().getTreeNodeType())) { // 风险节点C
			treeNodeType = TreeNodeType.riskRoot;
			setIds = JecnConstants.loginBean.getSetRisk();
		} else if (JecnTreeCommon.isOrgTreeNodeType(node.getJecnTreeBean().getTreeNodeType())) { // 组织权限：暂时去掉
			treeNodeType = TreeNodeType.organizationRoot;
			setIds = JecnConstants.loginBean.getSetOrg();
		} else if (JecnTreeCommon.isPositionGroupTreeNodeType(node.getJecnTreeBean().getTreeNodeType())) { // 组织权限：暂时去掉
			treeNodeType = TreeNodeType.positionGroupRoot;
			setIds = JecnConstants.loginBean.getSetPosGroup();
		} else if (JecnTreeCommon.isTermTreeNodeType(node.getJecnTreeBean().getTreeNodeType())) { // 风险节点C
			treeNodeType = TreeNodeType.termDefineRoot;
			setIds = JecnConstants.loginBean.getSetTerm();
		}
		if (treeNodeType == null) {
			return 0;
		}
		if (setIds == null || setIds.size() == 0) {
			return 0;
		}
		// 虽然有该节点的权限，但是为了防止删除或者重命名该节点，所以返回1
		if (setIds.contains(node.getJecnTreeBean().getId())) {
			return 1;
		}
		// 如果父节点有设计权限，那么子节点也应当有设计权限
		if (isOperationAuthByNodePub(node, treeNodeType, setIds)) {
			return 2;
		}
		return 0;
	}

	/**
	 * 验证是否为设计者权限
	 * 
	 * @param node
	 * @return true 当前节点只为设计者权限,false 无权限
	 */
	public static boolean isDesigner(JecnTreeNode node) {
		if (node == null) {
			throw new NullPointerException("JecnTreeCommon isDesigner is error！");
		}
		return isDesigner(node.getJecnTreeBean().getTreeNodeType());
	}

	/**
	 * 验证是否为设计者权限
	 * 
	 * @param treeNodeType
	 * @return true 当前节点只为设计者权限,false 无权限
	 */
	public static boolean isDesigner(TreeNodeType treeNodeType) {
		boolean isDesigner = false;
		LoginBean loginBean = JecnConstants.loginBean;
		if (JecnDesignerCommon.isAuthorAdmin()) {
			return false;
		}
		switch (treeNodeType) {
		case process:
		case processMap:
		case processFile:
			if (!loginBean.isDesignProcessAdmin()) {// 不是系统管理员，也不是制度管理员 也不是二级管理员
				isDesigner = true;
			}
			break;
		case file:
			if (!loginBean.isDesignFileAdmin()) {// 不是系统管理员，也不是制度管理员 也不是二级管理员
				isDesigner = true;
			}
			break;
		case ruleFile:
		case ruleModeFile:
			if (!loginBean.isDesignRuleAdmin()) {// 不是系统管理员，也不是制度管理员 也不是二级管理员
				isDesigner = true;
				break;
			}
		}
		return isDesigner;
	}

	/**
	 * @author yxw 2012-8-14
	 * @description:判断是不是流程
	 * @param treeNodeType
	 * @return
	 */
	public static boolean isProcessTreeNodeType(TreeNodeType treeNodeType) {
		if (treeNodeType == TreeNodeType.processMap || treeNodeType == TreeNodeType.process
				|| treeNodeType == TreeNodeType.processFile) {
			return true;
		}
		return false;
	}

	/**
	 * @author yxw 2012-8-14
	 * @description:判断是不是标准
	 * @param treeNodeType
	 * @return
	 */
	public static boolean isStandaidTreeNodeType(TreeNodeType treeNodeType) {
		if (treeNodeType == TreeNodeType.standard || treeNodeType == TreeNodeType.standardProcess
				|| treeNodeType == TreeNodeType.standardProcessMap || treeNodeType == TreeNodeType.standardDir
				|| treeNodeType == TreeNodeType.standardClause || treeNodeType == TreeNodeType.standardClauseRequire) {
			return true;
		}
		return false;
	}

	/**
	 * @author yxw 2012-8-14
	 * @description:判断是不是制度
	 * @param treeNodeType
	 * @return
	 */
	public static boolean isRuleTreeNodeType(TreeNodeType treeNodeType) {
		if (treeNodeType == TreeNodeType.ruleDir || treeNodeType == TreeNodeType.ruleFile
				|| treeNodeType == TreeNodeType.ruleModeFile) {
			return true;
		}
		return false;
	}

	/**
	 * @author yxw 2012-8-14
	 * @description:判断是不是文件
	 * @param treeNodeType
	 * @return
	 */
	public static boolean isFileTreeNodeType(TreeNodeType treeNodeType) {
		if (treeNodeType == TreeNodeType.file || treeNodeType == TreeNodeType.fileDir) {
			return true;
		}
		return false;
	}

	/**
	 * 判断是不是风险
	 * 
	 * @author chehuanbo
	 * @param treeNodeType
	 *            节点类型
	 * @return boolean true:是风险节点 false:不是风险节点
	 * */
	public static boolean isRiskTreeNodeType(TreeNodeType treeNodeType) {
		if (treeNodeType == TreeNodeType.riskDir || treeNodeType == TreeNodeType.riskPoint) {
			return true;
		}
		return false;
	}

	public static boolean isOrgTreeNodeType(TreeNodeType treeNodeType) {
		if (treeNodeType == TreeNodeType.organization) {
			return true;
		}
		return false;
	}

	public static boolean isPositionGroupTreeNodeType(TreeNodeType treeNodeType) {
		if (treeNodeType == TreeNodeType.positionGroup || treeNodeType == TreeNodeType.positionGroupDir) {
			return true;
		}
		return false;
	}

	public static boolean isTermTreeNodeType(TreeNodeType treeNodeType) {
		if (treeNodeType == TreeNodeType.termDefine || treeNodeType == TreeNodeType.termDefineDir) {
			return true;
		}
		return false;
	}

	/**
	 * tree有没有节点的操作权限(递归算法)
	 * 
	 * @param node
	 * @return
	 */
	private static boolean isOperationAuthByNodePub(JecnTreeNode node, TreeNodeType treeNodeType, Set<Long> set) {
		JecnTreeNode pNode = (JecnTreeNode) node.getParent();
		if (pNode != null) {
			if (pNode.getJecnTreeBean().getTreeNodeType().equals(treeNodeType)) {
				if (set.contains(pNode.getJecnTreeBean().getId())) {
					return true;
				}
			} else {
				if (set.contains(pNode.getJecnTreeBean().getId())) {
					return true;
				}
				return isOperationAuthByNodePub(pNode, treeNodeType, set);
			}
		}
		return false;
	}

	/**
	 * @author yxw 2012-8-10
	 * @description:获得树选中的节点
	 * @param jTree
	 * @return
	 */
	public static List<JecnTreeNode> getSelectJecnTreeNodes(JTree jTree) {
		List<JecnTreeNode> listJecnTreeNode = new ArrayList<JecnTreeNode>();
		TreePath[] treePathsArr = jTree.getSelectionPaths();
		if (treePathsArr != null) {
			for (TreePath treePath : treePathsArr) {
				listJecnTreeNode.add((JecnTreeNode) treePath.getLastPathComponent());
			}
		}

		return listJecnTreeNode;
	}

	/**
	 * 
	 * 选中节点集合 (显示按钮，已验证过是否有节点权限，只需要验证当前节点是否为设计者或管理员即可:以为设计者对未发布的节点有更新和删除权限)
	 * 
	 * @author ZHANGXIAOHU
	 * @param listNode
	 *            选中节点集合
	 * @param nodeType
	 *            文件类型 0:流程；1：文件；2：制度
	 * @return true 有权限(设计者有权限执行删除或更新),false:没有权限
	 */
	public static boolean isValidateTreeNode(List<JecnTreeNode> listNode, int nodeType) {
		if (listNode == null) {
			throw new NullPointerException("JecnTreeCommon isValidateDelete is error！");
		}
		if (nodeValidate(nodeType)) {// true有权限(设计者有权限执行删除或更新),false:没有权限
			return true;
		}
		// 如果节点存在已发布的PRF（文件、流程、制度）文件 没有权限
		for (JecnTreeNode jecnTreeNode : listNode) {
			if (jecnTreeNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.fileDir)
					|| jecnTreeNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.ruleDir)) {
				continue;
			}
			if (jecnTreeNode.getJecnTreeBean().isPub()) {// 存在已发布的PRF文件,设计者没有权限执行删除或更新
				// 您没有权限操作!
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("noPopedomOp"));
				return false;
			}
		}
		return true;
	}

	/**
	 * 验证节点是否有管理员权限
	 * (显示按钮，已验证过是否有节点权限，只需要验证当前节点是否为设计者或管理员即可:以为设计者对未发布的节点有更新和删除权限)
	 * 
	 * @param nodeType
	 *            文件类型 0:流程；1：文件；2：制度
	 * @return true 有权限(设计者有权限执行删除或更新),false:没有权限
	 */
	public static boolean nodeValidate(int nodeType) {
		if (JecnDesignerCommon.isAuthorAdmin()) {// 如果是系统管理员
			return true;
		}
		// 是否存在权限 true(// 如果是管理员（文件、流程、制度）)
		boolean isPermissions = false;
		switch (nodeType) {
		case 0:// 流程/流程地图
			isPermissions = JecnConstants.loginBean.isDesignProcessAdmin();
			break;
		case 1: // 文件目录/文件
			isPermissions = JecnConstants.loginBean.isDesignFileAdmin();
			break;
		case 2:// 制度文件/制度目录/ 制度模板文件
			isPermissions = JecnConstants.loginBean.isDesignRuleAdmin();
			break;
		}
		return isPermissions;
	}

	/**
	 * 验证当前节点是否有管理员权限（当前节点下显示操作按钮，就有对该节点的操作权限，只需要在是设计者还是管理员）
	 * 
	 * @param treeNode
	 * @return true 当前节点为管理员权限
	 */
	public static boolean isNodeAdmin(JecnTreeNode treeNode, int nodeType) {
		if (nodeValidate(nodeType)) {
			return true;
		}
		return false;
	}

	/**
	 * 选中单个节点验证是否存在更新权限
	 * 
	 * @param treeNode
	 *            选中的树节点
	 * @param nodeType
	 *            节点类型 文件类型 0:流程；1：文件；2：制度
	 * @return true 有权限(设计者有权限执行删除或更新),false:没有权限
	 */
	public static boolean singleTreeNode(JecnTreeNode treeNode, int nodeType) {
		if (treeNode == null) {
			throw new NullPointerException("JecnTreeCommon singleTreeNode is error！");
		}
		if (nodeValidate(nodeType)) {// true有权限(设计者有权限执行删除或更新),false:没有权限
			return true;
		}
		if (treeNode.getJecnTreeBean().isPub()) {// 存在已发布的PRF文件,设计者没有权限执行删除或更新
			// 您没有权限操作!
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("noPopedomOp"));
			return false;
		}
		return true;
	}

	/**
	 * 验证当前节点是否可以执行发布，判断依据父节点未发布，责当前节点不能执行发布
	 * 
	 * @param workFlowNode
	 * @return true 当前节点可以执行发布
	 */
	public static boolean isPublic(JecnTreeNode workFlowNode) {
		if (workFlowNode == null) {
			throw new NullPointerException("JecnTreeCommon isPublic is error！workFlowNode=" + workFlowNode);
		}
		JecnTreeNode pNode = (JecnTreeNode) workFlowNode.getParent();
		if (pNode == null) {// 顶级节点不存在父节点 ()
			return false;
		}
		if (pNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.processRoot)) {// 父节点是流程根节点
			return true;
		}
		if (pNode.getJecnTreeBean().isPub()) {// 是否发布 true是发布，false是发布
			return true;
		}
		return false;
	}

	/**
	 * 展开所有树节点
	 * 
	 * @param tree
	 */
	public static void expandTree(RecycleTree tree) {
		JecnTreeNode root = (JecnTreeNode) tree.getModel().getRoot();
		expandAll(tree, new TreePath(root), true);
	}

	private static void expandAll(RecycleTree tree, TreePath parent, boolean expand) {
		// Traverse children
		JecnTreeNode node = (JecnTreeNode) parent.getLastPathComponent();
		if (node.getChildCount() >= 0) {
			for (Enumeration e = node.children(); e.hasMoreElements();) {
				JecnTreeNode n = (JecnTreeNode) e.nextElement();
				TreePath path = parent.pathByAddingChild(n);
				expandAll(tree, path, expand);
			}
		}
		// Expansion or collapse must be done bottom-up
		if (expand) {
			tree.expandPath(parent);
		} else {
			tree.collapsePath(parent);
		}
	}

	/**
	 * 在树节点显示的流程等的名称
	 * 
	 * @param jecnTreeBean
	 * @return
	 */
	public static String getShowName(JecnTreeBean jecnTreeBean) {
		String appState = "";
		if (jecnTreeBean.getApproveType() == 0) {
			appState = "(" + JecnProperties.getValue("approval") + ")";
		} else if (jecnTreeBean.getApproveType() == 2) {
			appState = "(" + JecnProperties.getValue("revocation") + ")";
		}
		String showName = appState + jecnTreeBean.getName();
		if (jecnTreeBean.getTreeNodeType() == null || DrawCommon.isNullOrEmtry(showName)) {
			return null;
		}
		// 如果是流程、架构而且编号不为空并且配置项显示编号
		if ((jecnTreeBean.getTreeNodeType().equals(TreeNodeType.process) || jecnTreeBean.getTreeNodeType().equals(
				TreeNodeType.processMap))
				&& !DrawCommon.isNullOrEmtry(jecnTreeBean.getNumberId()) && JecnConstants.isProcessShowNumber) {
			showName = jecnTreeBean.getNumberId() + " " + showName;
		}
		return showName;
	}

	public static String getShowName(String name, String num) {
		String showName = name;
		if (JecnConstants.isProcessShowNumber && num != null && !"".equals(num.trim())) {
			showName = num.trim() + " " + name;
		}
		return showName;
	}

	/**
	 * 通过数据库获得新节点的排序
	 * 
	 * @param pNode
	 * @return
	 * @throws Exception
	 */
	public static int getMaxSortFromDB(JecnTreeNode pNode) throws Exception {

		if (pNode == null || pNode.getJecnTreeBean() == null) {
			return 0;
		}
		int sortId = 0;
		JecnTreeBean jecnTreeBean = pNode.getJecnTreeBean();
		if (jecnTreeBean.getTreeNodeType() == TreeNodeType.file
				|| jecnTreeBean.getTreeNodeType() == TreeNodeType.fileDir
				|| jecnTreeBean.getTreeNodeType() == TreeNodeType.fileRoot) {

			sortId = ConnectionPool.getFileAction().getMaxSortId(jecnTreeBean.getId());
			sortId = sortId + 1;
		}

		return sortId;
	}

	/**
	 * 将JecnTreeBean转换为JecnTreeNode类型。并添加到rootNode。
	 * 
	 * @param rootNode
	 * @param bean
	 */
	public static void addChildNode(JecnTreeNode rootNode, JecnTreeBean bean) {
		if (bean == null || rootNode == null) {
			return;
		}
		JecnTreeNode node = new JecnTreeNode(bean);
		rootNode.add(node);
	}

	/**
	 * 从开始节点开始定位岗位
	 * 
	 * @param jecnTreeBean
	 * @param jTree
	 * @param startId
	 *            部门开始展示的节点
	 * @throws Exception
	 */
	public static void searchNodePos(JecnTreeBean jecnTreeBean, HighEfficiencyOrgPositionTree jTree, Long startId)
			throws Exception {

		// 节点唯一标示
		String nodeUniqueIdentifier = JecnTreeCommon.getNodeUniqueIdentifier(jecnTreeBean.getId(), jecnTreeBean
				.getTreeNodeType());
		// 判断节点是不是已经在数节点上
		JecnTreeNode node = JecnTreeCommon.getTreeNode(nodeUniqueIdentifier, jTree);
		if (node != null) {
			JecnTreeCommon.selectNode(jTree, node);
			return;
		}

		TreeNodeType rootNodeType = TreeNodeType.organizationRoot;

		// 根节点
		JecnTreeNode searchRootNode = getSearchRootNode(jTree, rootNodeType);
		if (searchRootNode == null) {
			return;
		}

		List<JecnTreeBean> list = ConnectionPool.getOrganizationAction().getPnodesPos(jecnTreeBean.getPid(),
				JecnConstants.projectId);

		// 开始的节点
		JecnTreeNode jecnTreeNode = null;

		// 在根节点的所有的子节点中找到开始显示的子节点
		int childCount = searchRootNode.getChildCount();
		JecnTreeNode eachNode = null;
		for (int i = 0; i < childCount; i++) {

			eachNode = (JecnTreeNode) searchRootNode.getChildAt(i);
			if (eachNode.getJecnTreeBean().getId().equals(startId)) {
				jecnTreeNode = eachNode;
				break;
			}
		}
		if (jecnTreeNode == null) {
			return;
		}
		// 添加搜索节点
		JecnTreeCommon.addChildNodesSearch(jecnTreeNode, list, jTree);
		// 获得树节点
		JecnTreeNode selectNode = getTreeNodeChild(JecnTreeCommon.getNodeUniqueIdentifier(jecnTreeBean.getId(),
				jecnTreeBean.getTreeNodeType()), searchRootNode);

		if (selectNode != null) {
			JecnTreeCommon.selectNode(jTree, selectNode);
		}

	}

	/**
	 * @author yxw 2012-8-9
	 * @description:搜索节点
	 * @param id
	 * @param type
	 * @param jTree
	 * @throws Exception
	 */
	public static JecnTreeNode searchNodeNoExpand(Long id, TreeNodeType type, JTree jTree) throws Exception {
		// 节点唯一标示
		String nodeUniqueIdentifier = JecnTreeCommon.getNodeUniqueIdentifier(id, type);
		// 判断节点是不是已经在数节点上
		JecnTreeNode node = JecnTreeCommon.getTreeNode(nodeUniqueIdentifier, jTree);
		if (node != null) {
			JecnTreeCommon.selectNode(jTree, node);
			return node;
		}
		TreeNodeType rootNodeType = null;
		List<JecnTreeBean> list = null;
		switch (type) {
		case process:
		case processMap:
			if (jTree.equals(JecnDesignerMainPanel.getDesignerMainPanel().getTreePanel().getTree())
					&& JecnDesignerCommon.isOnlyViewDesignAuthor()) {
				list = JecnDesignerCommon.getDesignerResultList(ConnectionPool.getProcessAction().getPnodes(id,
						JecnConstants.projectId));
			} else {
				list = ConnectionPool.getProcessAction().getPnodes(id, JecnConstants.projectId);
			}
			rootNodeType = TreeNodeType.processRoot;
			break;
		case ruleDir:
		case ruleModeFile:
		case ruleFile:
			list = ConnectionPool.getRuleAction().getPnodes(id, JecnConstants.projectId);
			rootNodeType = TreeNodeType.ruleRoot;
			break;
		case organization:
			list = ConnectionPool.getOrganizationAction().getPnodesOrg(id, JecnConstants.projectId);
			rootNodeType = TreeNodeType.organizationRoot;
			break;
		case standardDir:
		case standard:
		case standardClause:
		case standardClauseRequire:
			list = ConnectionPool.getStandardAction().getPnodes(id, JecnConstants.projectId);
			rootNodeType = TreeNodeType.standardRoot;
			break;
		case fileDir:
		case file:
			list = ConnectionPool.getFileAction().getPnodes(id, JecnConstants.projectId);
			rootNodeType = TreeNodeType.fileRoot;
			break;
		default:
			break;
		}
		if (list != null && rootNodeType != null) {
			return JecnTreeCommon.searchNode(list, id, jTree, rootNodeType, type);
		}
		return null;
	}

	/**
	 * 根据节点类型获得该节点设置的任务模板id,如果没有则迭代找到父节点的模板id
	 * 
	 * @param node
	 * @param type
	 *            :0 流程；1 文件； 2 制度；4 流程架构
	 * @param approveType
	 *            0:正常审批 ； 2 废止
	 * @return
	 */
	private static String getTaskTempletId(JecnTreeNode node, int approveType) {
		String taskTempletId = null;
		try {
			taskTempletId = ConnectionPool.getTaskRecordAction().getTempletIdByTempletType(
					node.getJecnTreeBean().getId(), getTemplateTypeByNodeType(node), approveType);
		} catch (Exception e) {
			log.error("", e);
		}
		if (taskTempletId != null) {
			return taskTempletId;
		}
		TreeNodeType rootType = null;
		Map<Long, String> relatedTempletMap = null;
		if (approveType == 0) {
			if (JecnDesignerCommon.isProcess(node.getJecnTreeBean().getTreeNodeType())) {
				rootType = TreeNodeType.processRoot;
				relatedTempletMap = JecnConstants.processRelatedTaskTemplet;
			} else if (TreeNodeType.processMap == node.getJecnTreeBean().getTreeNodeType()) {
				rootType = TreeNodeType.processRoot;
				relatedTempletMap = JecnConstants.processMapRelatedTaskTemplet;
			} else if (TreeNodeType.ruleFile == node.getJecnTreeBean().getTreeNodeType()
					|| TreeNodeType.ruleModeFile == node.getJecnTreeBean().getTreeNodeType()) {
				rootType = TreeNodeType.ruleRoot;
				relatedTempletMap = JecnConstants.ruleRelatedTaskTemplet;
			} else if (TreeNodeType.file == node.getJecnTreeBean().getTreeNodeType()) {
				rootType = TreeNodeType.fileRoot;
				relatedTempletMap = JecnConstants.fileRelatedTaskTemplet;
			}
		} else if (approveType == 2) {
			if (JecnDesignerCommon.isProcess(node.getJecnTreeBean().getTreeNodeType())) {
				rootType = TreeNodeType.processRoot;
				relatedTempletMap = JecnConstants.processRelatedAbolishTaskTemplet;
			} else if (TreeNodeType.processMap == node.getJecnTreeBean().getTreeNodeType()) {
				rootType = TreeNodeType.processRoot;
				relatedTempletMap = JecnConstants.processMapRelatedAbolishTaskTemplet;
			} else if (TreeNodeType.ruleFile == node.getJecnTreeBean().getTreeNodeType()
					|| TreeNodeType.ruleModeFile == node.getJecnTreeBean().getTreeNodeType()) {
				rootType = TreeNodeType.ruleRoot;
				relatedTempletMap = JecnConstants.ruleRelatedAbolishTaskTemplet;
			} else if (TreeNodeType.file == node.getJecnTreeBean().getTreeNodeType()) {
				rootType = TreeNodeType.fileRoot;
				relatedTempletMap = JecnConstants.fileRelatedAbolishTaskTemplet;
			}
		}

		if (rootType == null || relatedTempletMap == null) {
			return null;
		}
		taskTempletId = getTaskTempletId((JecnTreeNode) node.getParent(), relatedTempletMap, rootType);
		return taskTempletId;
	}

	public static List<TaskConfigItem> getTaskConfigItems(JecnTreeNode node, int approveType) {
		String taskTempletId = getTaskTempletId(node, approveType);
		try {
			List<TaskConfigItem> fullConfigs = ConnectionPool.getTaskRecordAction().listTaskConfigItemByTempletId(
					taskTempletId);
			return fullConfigs;
		} catch (Exception e) {
			log.error("", e);
		}
		return new ArrayList<TaskConfigItem>();
	}

	private static String getTaskTempletId(JecnTreeNode node, Map<Long, String> relatedTempletMap, TreeNodeType rootType) {
		String taskTempletId = relatedTempletMap.get(node.getJecnTreeBean().getId());
		if (taskTempletId != null && !"".equals(taskTempletId)) {
			return taskTempletId;
		}
		JecnTreeNode pNode = (JecnTreeNode) node.getParent();
		if (pNode != null) {
			if (rootType == pNode.getJecnTreeBean().getTreeNodeType()) {
				return null;
			}
			return getTaskTempletId(pNode, relatedTempletMap, rootType);
		}
		return null;

	}

	/**
	 * 根据节点类型获得该节点设置的任务模板id
	 * 
	 * @param node
	 * @return
	 */
	public static String getTaskTempletIdBySelf(JecnTreeNode node, int taskType) {
		Map<Long, String> relatedTempletMap = null;
		if (taskType == 0) {
			relatedTempletMap = JecnConstants.processRelatedTaskTemplet;
		} else if (taskType == 4) {
			relatedTempletMap = JecnConstants.processMapRelatedTaskTemplet;
		} else if (taskType == 2 || taskType == 3) {
			relatedTempletMap = JecnConstants.ruleRelatedTaskTemplet;
		} else if (taskType == 1) {
			relatedTempletMap = JecnConstants.fileRelatedTaskTemplet;
		}

		if (relatedTempletMap == null) {
			return null;
		}
		return relatedTempletMap.get(node.getJecnTreeBean().getId());
	}

	public static boolean canApprove(JecnTreeNode selectNode, int approveType) {
		List<TaskConfigItem> taskConfigItems = JecnTreeCommon.getTaskConfigItems(selectNode, approveType);
		if (taskConfigItems == null || taskConfigItems.size() == 0) {
			String template = "";
			if (approveType == 0) {
				template = JecnProperties.getValue("approvalTaskModel");
			} else if (approveType == 1) {
				template = JecnProperties.getValue("borrowTaskModel");
			} else if (approveType == 2) {
				template = JecnProperties.getValue("abolishTaskModel");
			}
			/*
			 * 请设置:任务模板
			 */
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("pleaseSetUp") + template);
			return false;
		}
		return true;
	}

	private static int getTemplateTypeByNodeType(JecnTreeNode selectNode) {
		TreeNodeType treeNodeType = selectNode.getJecnTreeBean().getTreeNodeType();
		switch (treeNodeType) {
		case process:
		case processFile:
			return 0;
		case processMap:
			return 4;
		case ruleFile:
		case ruleModeFile:
			return 2;
		case file:
			return 1;
		default:
			return -1;
		}
	}

	public static boolean canPub(JecnTreeNode selectNode) {
		return JecnTreeCommon.canApprove(selectNode, 0);
	}

	/**
	 * 获得当前面板对应的树节点
	 * 
	 * @return
	 */
	public static JecnTreeNode getWorkNode() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return null;
		}
		switch (workflow.getFlowMapData().getMapType()) {
		case partMap:// 流程图
			return JecnTreeCommon.getTreeNode(JecnTreeCommon.getNodeUniqueIdentifier(workflow.getFlowMapData()
					.getFlowId(), TreeNodeType.process), JecnDesignerMainPanel.getDesignerMainPanel().getTree());
		case totalMap:// 流程地图
		case totalMapRelation:// 集成关系图
			return JecnTreeCommon.getTreeNode(JecnTreeCommon.getNodeUniqueIdentifier(workflow.getFlowMapData()
					.getFlowId(), TreeNodeType.processMap), JecnDesignerMainPanel.getDesignerMainPanel().getTree());
		case orgMap:// 组织图
			return JecnTreeCommon.getTreeNode(JecnTreeCommon.getNodeUniqueIdentifier(workflow.getFlowMapData()
					.getFlowId(), TreeNodeType.organization), JecnDesignerMainPanel.getDesignerMainPanel().getTree());
		default:
			return null;
		}
	}

	/**
	 * 把流程架构和流程图标设置成更新状态
	 */
	public static void flushCurrentTreeNodeToUpdate() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || !desktopPane.getFlowMapData().isSave()) {
			return;
		}
		JecnTreeNode selectNode = JecnTreeCommon.getWorkNode();
		flushCurrentTreeNodeToUpdate(selectNode);
	}

	/**
	 * 把流程架构和流程图标设置成更新状态
	 */
	public static void flushCurrentTreeNodeToUpdate(JecnTreeNode selectNode) {
		if (selectNode != null && selectNode.getJecnTreeBean().isPub()) {// 未发布的流程不能够设置为更新
			selectNode.getJecnTreeBean().setUpdate(true);
			JecnTreeCommon.reload(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), selectNode);
			// 选中树节点
			JecnTreeCommon.selectNode(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), selectNode);
		}
	}
}
