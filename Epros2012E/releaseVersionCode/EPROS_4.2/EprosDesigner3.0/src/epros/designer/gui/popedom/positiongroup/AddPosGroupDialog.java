package epros.designer.gui.popedom.positiongroup;

import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnPositionGroup;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;

/**
 * 流程模板节点
 * 
 * @author fuzhh Apr 9, 2013
 * 
 */
public class AddPosGroupDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(AddPosGroupDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public AddPosGroupDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addPositionGroup");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("positionGroupC");
	}

	@Override
	public void saveData() {
		// 向岗位组表中添加数据
		JecnPositionGroup jecnPositionGroup = new JecnPositionGroup();
		// 项目ID
		jecnPositionGroup.setProjectId(JecnConstants.projectId);
		// 岗位组目录名称
		jecnPositionGroup.setName(getName());
		// 父ID
		jecnPositionGroup.setPerId(pNode.getJecnTreeBean().getId());
		// 岗位组目录 1：目录，0：岗位组
		jecnPositionGroup.setIsDir(0);
		// 排序ID
		jecnPositionGroup.setSortId(JecnTreeCommon.getMaxSort(pNode));
		// 创建人
		jecnPositionGroup.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 更新人
		jecnPositionGroup.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		jecnPositionGroup.setDataType(0); //岗位组 新建类型 0 本地新建
		try {
			// 执行数据库表的添加保存
			Long id = ConnectionPool.getPosGroup().addPositionGroup(jecnPositionGroup);
			// 向树节点添加岗位组名
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.positionGroup);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("AddPosGroupDialog saveData is error", e);
		}
	}

	protected void validateEnglishName() {// 验证英文名称
	}

	protected void addEnNamePanel(Insets insets) {
	}

	/**
	 * 验证名称
	 */
	public boolean validateName(String name, JLabel jLable) {
		return JecnValidateCommon.validateName(name, promptLab);
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name, TreeNodeType.positionGroup);
	}

}
