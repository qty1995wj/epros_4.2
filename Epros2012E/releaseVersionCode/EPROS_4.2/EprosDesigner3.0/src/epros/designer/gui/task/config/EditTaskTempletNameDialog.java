package epros.designer.gui.task.config;

import java.util.Date;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.TaskTemplet;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 重命名 项目名称
 * 
 * @author 2012-05-24
 * 
 */
public class EditTaskTempletNameDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(EditTaskTempletNameDialog.class);
	private TaskTemplet taskTemplet = null;

	public EditTaskTempletNameDialog(TaskTemplet taskTemplet) {
		this.setLocationRelativeTo(null);
		if (taskTemplet == null) {
			return;
		}
		this.taskTemplet = taskTemplet;
		this.setName(taskTemplet.getName());
		this.seteName(taskTemplet.geteName());
		this.setLocationRelativeTo(null);
	}


	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		taskTemplet.setName(getName());
		taskTemplet.setUpdatePeople(JecnConstants.loginBean.getJecnUser().getPeopleId());
		taskTemplet.seteName(geteName());
		taskTemplet.setUpdateTime(new Date());
		try {
			ConnectionPool.getTaskRecordAction().updateTaskTemplet(taskTemplet);
			this.dispose();
		} catch (Exception e) {
			log.error("EditTaskTempletNameDialog saveData is error", e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return ConnectionPool.getTaskRecordAction().validateUpdateName(name, taskTemplet.getId(),taskTemplet.getType());
	}

}
