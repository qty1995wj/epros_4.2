package com.jecn.epros.server.webBean.dataImport.match;

import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;

/**
 * 岗位临时表
 * 
 * @author zhangjie 20120208
 */
public class PosMatchBean extends AbstractBaseBean {
	/** 主键ID */
	private Long id;

	/** 基准岗位编码 */
	private String basePosNum;

	/** 基准岗位名称 */
	private String basePosName;

	/** HR实际部门编码 */
	private String deptNum;

	/** HR实际岗位编码 */
	private String actPosNum;

	/** HR实际岗位名称 */
	private String actPosName;

	// /**标识位 0：添加，1：更新，*/
	// private int posFlag;
	//	
	// /**错误信息*/
	// private String error;

	/** 存放人员岗位组合数据,即人员+关联岗位的数据 */
	private UserMatchBean userPosBean = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBasePosNum() {
		return basePosNum;
	}

	public void setBasePosNum(String basePosNum) {
		this.basePosNum = basePosNum;
	}

	public String getBasePosName() {
		return basePosName;
	}

	public void setBasePosName(String basePosName) {
		this.basePosName = basePosName;
	}

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public String getActPosNum() {
		return actPosNum;
	}

	public void setActPosNum(String actPosNum) {
		this.actPosNum = actPosNum;
	}

	public String getActPosName() {
		return actPosName;
	}

	public void setActPosName(String actPosName) {
		this.actPosName = actPosName;
	}

	public UserMatchBean getUserPosBean() {
		return userPosBean;
	}

	public void setUserPosBean(UserMatchBean userPosBean) {
		this.userPosBean = userPosBean;
	}

	@Override
	public String toInfo() {
		StringBuilder strBuf = new StringBuilder();
		// 基准岗位编号
		strBuf.append("基准岗位编号:");
		strBuf.append(this.getBasePosNum());
		strBuf.append(MatchConstant.EMTRY_SPACE);

		// 基准岗位名称
		strBuf.append("基准岗位名称:");
		strBuf.append(this.getBasePosName());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		// 岗位所属部门编号
		strBuf.append("岗位所属部门编号:");
		strBuf.append(this.getDeptNum());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		// 实际岗位编号
		strBuf.append("实际岗位编号:");
		strBuf.append(this.getActPosNum());
		strBuf.append(MatchConstant.EMTRY_SPACE);

		// 实际岗位名称
		strBuf.append("实际岗位名称:");
		strBuf.append(this.getActPosName());
		strBuf.append(MatchConstant.EMTRY_SPACE);

		// 错误信息
		strBuf.append("错误信息:");
		strBuf.append(this.getError());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		strBuf.append("\r\n");
		return strBuf.toString();
	}

}
