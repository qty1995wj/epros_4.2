package epros.designer.gui.common;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public abstract class OldJecnMoveChooseDialog extends JecnDialog {
	/** 搜索集 */
	private List<JecnTreeBean> searchListJecnTreeBean = new ArrayList<JecnTreeBean>();

	/** 结果 */
	private JecnTreeBean jecnTreeBean = null;

	public JecnTreeBean getJecnTreeBean() {
		return jecnTreeBean;
	}

	public void setJecnTreeBean(JecnTreeBean jecnTreeBean) {
		this.jecnTreeBean = jecnTreeBean;
	}

	private List<JecnTreeNode> listMoveNodes = null;

	public List<JecnTreeNode> getListMoveNodes() {
		return listMoveNodes;
	}

	public void setListMoveNodes(List<JecnTreeNode> listMoveNodes) {
		this.listMoveNodes = listMoveNodes;
	}

	private List<Long> listIds = new ArrayList<Long>();

	public List<Long> getListIds() {
		return listIds;
	}

	public void setListIds(List<Long> listIds) {
		this.listIds = listIds;
	}

	/** 主面板 */
	private JPanel mainPanel = null;

	/** 树面板 */
	private JScrollPane treePanel = null;
	/** 搜索面板 */
	private JPanel searchPanel = null;
	/** 结果面板 */
	private JPanel resultPanel = null;
	/** 搜索结果滚动面板 */
	private JScrollPane searchScrollPane = null;
	/** 已选结果滚动面板 */
	private JScrollPane resultScrollPane = null;

	/** 搜索名称 */
	private JLabel searchLabel = null;
	/** 搜索内容 */
	private JTextField searchText = null;

	/** 按钮面板，上面放四个按钮 */
	private JPanel buttonsPanel = null;
	/** 确定 */
	private JButton okBtn = null;
	/** 清空 */
	private JButton emptyBtn = null;
	/** 取消 */
	private JButton cancelBtn = null;

	/** 搜索table */
	private JecnTable resultTable = null;

	/** 结果table */
	private JecnTable searchTable = null;

	/** 是否操作 true是操作，false是没有 */
	private boolean isOperation = false;

	/** Tree 对象 */
	private JecnTree jTree = null;

	public boolean isOperation() {
		return isOperation;
	}

	public void setOperation(boolean isOperation) {
		this.isOperation = isOperation;
	}

	public OldJecnMoveChooseDialog(JecnTreeBean jecnTreeBean,
			List<JecnTreeNode> listMoveNodes) {
		this.jecnTreeBean = jecnTreeBean;
		this.listMoveNodes = listMoveNodes;
		for (JecnTreeNode jecnTreeNode : listMoveNodes) {
			listIds.add(jecnTreeNode.getJecnTreeBean().getId());
		}
		initCompotents();
		initLayout();
		buttonInit();
		this.setModal(true);
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:初始化组件
	 */
	public void initCompotents() {
		this.setSize(710, 510);
		this.setResizable(false);
		this.setModal(true);
		// 局中，设置大小后使用
		this.setLocationRelativeTo(null);
		this.setTitle(getDialogTitle());
		// 主面板
		this.mainPanel = new JPanel();

		// 树面板
		this.treePanel = new JScrollPane();
		Dimension dimension = new Dimension(200, 430);
		treePanel.setPreferredSize(dimension);
		treePanel.setMinimumSize(dimension);

		// 搜索面板
		this.searchPanel = new JPanel();
		dimension = new Dimension(475, 240);
		searchPanel.setPreferredSize(dimension);
		searchPanel.setMinimumSize(dimension);
		searchPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("search")));

		// 结果面板
		this.resultPanel = new JPanel();
		dimension = new Dimension(475, 190);
		resultPanel.setPreferredSize(dimension);
		resultPanel.setMinimumSize(dimension);
		resultPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("result")));

		// 搜索结果滚动面板设置大小
		this.searchScrollPane = new JScrollPane();
		dimension = new Dimension(468, 180);
		searchScrollPane.setPreferredSize(dimension);
		searchScrollPane.setMinimumSize(dimension);

		// 已选结果滚动面板设置大小
		this.resultScrollPane = new JScrollPane();
		dimension = new Dimension(468, 150);
		resultScrollPane.setPreferredSize(dimension);
		resultScrollPane.setMinimumSize(dimension);
		// 按钮面板
		this.buttonsPanel = new JPanel();
		dimension = new Dimension(460, 30);
		buttonsPanel.setPreferredSize(dimension);
		buttonsPanel.setMinimumSize(dimension);

		// 初始化搜索标签名称
		this.searchLabel = new JLabel(getSearchLabelName());
		this.searchText = new JTextField(60);

		// 设置各面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		searchPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:dialog布局方法
	 */
	public void initLayout() {
		// 主面板布局-开始
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(2, 2, 2, 2);
		c = new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
				GridBagConstraints.SOUTH, GridBagConstraints.NONE, insets, 0, 0);

		mainPanel.add(treePanel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(searchPanel, c);
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(resultPanel, c);
		c = new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(buttonsPanel, c);
		// 主面板布局-结束

		jTree = getJecnTree();
		// 把tree加入滚动面板
		treePanel.setViewportView(jTree);

		// 搜索面板布局,流布局 靠左
		searchPanel.setLayout(new FlowLayout());
		searchPanel.add(searchLabel);
		searchPanel.add(searchText);
		searchText.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				searchTextKeyReleased();
			}
		});

		searchTable = new ChooseTable(new ArrayList<JecnTreeBean>());
		searchTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					searchTableMousePressed();
				}
			}
		});
		searchScrollPane.setViewportView(searchTable);
		searchPanel.add(searchScrollPane);
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		if (jecnTreeBean != null) {
			list.add(jecnTreeBean);
		}
		resultTable = new ChooseTable(list);
		resultScrollPane.setViewportView(resultTable);
		resultPanel.add(resultScrollPane);

		// 设置按钮面板为流布局
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		// 把主面板增加到dialog
		this.getContentPane().add(mainPanel);
	}

	// 为按钮增加事件
	private void buttonInit() {
		this.okBtn = new JButton(JecnProperties.getValue("okBtn"));
		buttonsPanel.add(okBtn);
		okBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isOperation = true;
				closeDialog();
			}
		});

		this.emptyBtn = new JButton(JecnProperties.getValue("emptyBtn"));
		buttonsPanel.add(emptyBtn);
		emptyBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				emptyBtnAction();
			}
		});

		this.cancelBtn = new JButton(JecnProperties.getValue("cancelBtn"));
		buttonsPanel.add(cancelBtn);
		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeDialog();
			}
		});

	}

	/**
	 * 双击树选择
	 * 
	 * @param evt
	 */
	public void jTreeMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jTree1MousePressed
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			if (evt.getClickCount() == 2) {
				if (!jTree.isSelectionEmpty()) {
					javax.swing.tree.TreePath treePath = jTree
							.getSelectionPath();
					JecnTreeNode node = (JecnTreeNode) treePath
							.getLastPathComponent();
					// 清空
					resultTable.remoeAll();
					// 增加
					resultTable.addRow(convertRowData(node.getJecnTreeBean()));
					this.setJecnTreeBean(node.getJecnTreeBean());

				}
			}
		}

	}

	/**
	 * @author yxw 2012-5-4
	 * @description:关闭Dialog
	 */
	public void closeDialog() {
		this.dispose();
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:设置dialog标题
	 * @return
	 */
	public abstract String getDialogTitle();

	/**
	 * @author yxw 2012-5-9
	 * @description:搜索结果
	 */
	public void searchTextKeyReleased() {
		String name = searchText.getText().trim();
		if (!DrawCommon.isNullOrEmtryTrim(name)) {
			searchListJecnTreeBean = getSearchContent(name, jTree);
			searchTable.remoeAll();
			searchTable.addRows(getContent(searchListJecnTreeBean));
		}
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:获得搜索的结果
	 * @param name
	 * @return
	 */
	public abstract List<JecnTreeBean> getSearchContent(String name, JTree jTree);

	/**
	 * @author yxw 2012-5-4
	 * @description:
	 * @return 返回搜索标签的名称
	 */
	public abstract String getSearchLabelName();

	/**
	 * @author yxw 2012-5-4
	 * @description:设置
	 * @return
	 */
	public abstract JecnTree getJecnTree();

	/**
	 * @author yxw 2012-5-9
	 * @description:清空操作
	 */
	public void emptyBtnAction() {
		// 增加是否要清空
		resultTable.remoeAll();
		this.setJecnTreeBean(null);
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:双击Table
	 */
	private void searchTableMousePressed() {
		int row = searchTable.getSelectedRow();
		if (row != -1) {
			JecnTreeBean treeBean = this.searchListJecnTreeBean.get(row);
			// 清空
			resultTable.remoeAll();
			// 增加
			resultTable.addRow(convertRowData(treeBean));
			this.setJecnTreeBean(treeBean);
		}
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:获得搜索table的标题
	 * @return
	 */
	public abstract Vector<String> getTableTitle();

	// button抽角方法--结束
	class ChooseTable extends JecnTable {

		private List<JecnTreeBean> listJecnTreeBeans = null;

		public ChooseTable(List<JecnTreeBean> listJecnTreeBeans) {
			this.listJecnTreeBeans = listJecnTreeBeans;
		}

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(),
					getContent(listJecnTreeBeans));
		}

		@Override
		public boolean isSelectMutil() {
			return false;
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0 };
		}
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得Table的内容
	 * @param list
	 * @return
	 */
	private Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	/**
	 * @author yxw 2012-5-11
	 * @description:jecnTreeBean 转换成可以加到表格里的数据
	 * @param jecnTreeBean
	 * @return
	 */
	private Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

}
