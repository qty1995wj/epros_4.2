package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.task.TaskHistoryNew;
import com.jecn.epros.sqlprovider.server3.SqlCommon;

public class DocSqlProvider extends BaseSqlProvider {


    public String saveJecnTaskHistory(TaskHistoryNew historyNew) {

        String id = "";
        String idValue = "";
        if (isOracle()) {
            id = "id,";
            idValue = "#{id},";

        }
        String sql = "insert into JECN_TASK_HISTORY_NEW(" + id
                + "DRAFT_PERSON,RELATE_ID,VERSION_ID,MODIFY_EXPLAIN,TEST_RUN_NUMBER,PUBLISH_DATE,NUMBER_ID,START_TIME,END_TIME,APPROVE_COUNT,TYPE,FILE_PATH,FILE_CONTENT_ID,EXPIRY,TASK_ID,NEXT_SCAN_DATE)values("
                + idValue
                + "#{draftPerson} ,#{relateId} , #{versionId} , #{modifyExplain} ,#{testRunNumber} , #{publishDate} , #{numberId}, #{startTime} , #{endTime} , #{approveCount}, #{type} , #{filePath} , #{fileContentId}, #{expiry} , #{taskId} , #{newScanDate})";

        return sql;

    }


    public String fetchProcessMapNoPubFiles(Long flowId) {

        String sql = "SELECT DISTINCT T.FILE_ID as fileId, T.VERSION_ID as versionId" + "  FROM JECN_FILE_T T"
                + "  INNER JOIN (" + "             SELECT JMF.FILE_ID" + "               FROM JECN_MAIN_FLOW_T JMF"
                + "              WHERE JMF.FLOW_ID = " + flowId + "             UNION"
                + "             SELECT JFSI.LINK_FLOW_ID FILE_ID"
                + "               FROM JECN_FLOW_STRUCTURE_IMAGE_T JFSI" + "              WHERE JFSI.FIGURE_TYPE = "
                + SqlCommon.getIconString() + "                AND JFSI.FLOW_ID = " + flowId + "             UNION"
                + "             SELECT JR.FILE_ID" + "               FROM JECN_RULE_T JR,"
                + "                     JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
                + "              WHERE JR.IS_DIR = 2 AND JR.IS_FILE_LOCAL=0 "
                + "                AND JFSI.FIGURE_TYPE = " + SqlCommon.getSystemString()
                + "                AND JR.ID = JFSI.LINK_FLOW_ID" + "                AND JFSI.FLOW_ID = " + flowId
                + "             UNION" + "             SELECT RF.RULE_FILE_ID FILE_ID"
                + "               FROM JECN_RULE_T                 JR,"
                + "                     RULE_FILE_T                 RF,"
                + "                     RULE_TITLE_T                RT,"
                + "                     JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
                + "              WHERE RF.RULE_TITLE_ID = RT.ID" + "                AND RT.RULE_ID = JR.ID"
                + "                AND JR.IS_DIR = 1" + "                AND JFSI.FIGURE_TYPE = "
                + SqlCommon.getSystemString() + "                AND JR.ID = JFSI.LINK_FLOW_ID"
                + "                AND JFSI.FLOW_ID = " + flowId + "             UNION"
                + "             SELECT FT.FILE_ID"
                + "               FROM JECN_FIGURE_FILE_T FT, JECN_FLOW_STRUCTURE_IMAGE_T IT"
                + "              WHERE FT.FIGURE_ID = IT.FIGURE_ID" + "                AND IT.FLOW_ID = " + flowId
                + ") FILE_REF_TMP" + "    ON T.FILE_ID = FILE_REF_TMP.FILE_ID" + "  LEFT JOIN JECN_FILE JF"
                + "    ON T.FILE_ID = JF.FILE_ID"
                + " WHERE T.DEL_STATE = 0 AND (T.UPDATE_TIME > JF.UPDATE_TIME OR JF.UPDATE_TIME IS NULL)";

        return sql;

    }

    public String fetchProcessNoPubFiles(Long flowId) {

        String sql = "SELECT T.FILE_ID as fileId,T.VERSION_ID as versionId" + "  FROM JECN_FILE_T T" + " INNER JOIN ("
                + "             SELECT JFBI.FILE_ID" + "               FROM JECN_FLOW_BASIC_INFO_T JFBI"
                + "              WHERE JFBI.FLOW_ID = " + flowId + "             UNION"
                + "             SELECT JFSI.LINK_FLOW_ID FILE_ID"
                + "               FROM JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
                + "              WHERE JFSI.FIGURE_TYPE = 'IconFigure'" + "                AND JFSI.FLOW_ID = " + flowId
                + "             UNION" + "             SELECT JAF.FILE_S_ID FILE_ID"
                + "               FROM JECN_ACTIVITY_FILE_T        JAF,"
                + "                     JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
                + "              WHERE JAF.FIGURE_ID = JFSI.FIGURE_ID" + "                AND JFSI.FLOW_ID = " + flowId
                + "             UNION" + "             SELECT JMF.FILE_M_ID FILE_ID"
                + "               FROM JECN_MODE_FILE_T JMF, JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
                + "              WHERE JMF.FIGURE_ID = JFSI.FIGURE_ID" + "                AND JFSI.FLOW_ID = " + flowId
                + "             UNION" + "             SELECT JT.FILE_ID"
                + "               FROM JECN_TEMPLET_T              JT,"
                + "                     JECN_MODE_FILE_T            JMF,"
                + "                     JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
                + "              WHERE JT.MODE_FILE_ID = JMF.MODE_FILE_ID"
                + "                AND JMF.FIGURE_ID = JFSI.FIGURE_ID" + "                AND JFSI.FLOW_ID = " + flowId
                + "             UNION" + "             SELECT JR.FILE_ID"
                + "               FROM FLOW_RELATED_CRITERION_T FRC, JECN_RULE_T JR"
                + "              WHERE JR.IS_DIR = 2 AND JR.IS_FILE_LOCAL=0 "
                + "                AND JR.ID = FRC.CRITERION_CLASS_ID" + "                AND FRC.FLOW_ID = " + flowId
                + "             UNION" + "             SELECT RF.RULE_FILE_ID FILE_ID"
                + "               FROM FLOW_RELATED_CRITERION_T FRC,"
                + "                     JECN_RULE_T              JR,"
                + "                     RULE_TITLE_T             RT,"
                + "                     RULE_FILE_T              RF" + "              WHERE RF.RULE_TITLE_ID = RT.ID"
                + "                AND RT.RULE_ID = JR.ID" + "                AND JR.IS_DIR = 1"
                + "                AND JR.ID = FRC.CRITERION_CLASS_ID" + "                AND FRC.FLOW_ID = " + flowId
                + "             UNION" + "             SELECT FT.FILE_ID"
                + "               FROM JECN_FIGURE_FILE_T FT, JECN_FLOW_STRUCTURE_IMAGE_T IT"
                + "              WHERE FT.FIGURE_ID = IT.FIGURE_ID" + "                AND IT.FIGURE_TYPE = 'FileImage'"
                + "                AND IT.FLOW_ID = " + flowId + "             ) FILE_REF_TMP"
                + "    ON T.FILE_ID = FILE_REF_TMP.FILE_ID" + "    LEFT JOIN JECN_FILE JF"
                + "    ON T.FILE_ID = JF.FILE_ID"
                + "    AND (T.UPDATE_TIME > JF.UPDATE_TIME or JF.UPDATE_TIME is null)";

        return sql;

    }

    public String fetchRuleModeNoPubFiles(Long fileId) {

        String sql = "SELECT T.FILE_ID AS FILEID,T.VERSION_ID AS VERSIONID"
                + "                  FROM JECN_FILE_T T"
                + "                  LEFT JOIN JECN_FILE JF ON T.FILE_ID = JF.FILE_ID"
                + "                ,JECN_RULE_T JR,RULE_TITLE_T RT,RULE_FILE_T RF WHERE T.FILE_ID=RF.RULE_FILE_ID"
                + "                AND RF.RULE_TITLE_ID=RT.ID AND RT.RULE_ID=JR.ID AND JR.IS_DIR=1 AND JR.ID=" + fileId
                + " AND (T.UPDATE_TIME > JF.UPDATE_TIME or JF.UPDATE_TIME is null)";
        return sql;

    }

    public String fetchRuleFileNoPubFiles(Long id) {

        String sql = "SELECT DISTINCT RULE_FILE.FILE_ID AS FILEID,RULE_FILE.VERSION_ID AS VERSIONID FROM ("
                + " SELECT T.FILE_ID,JF.FILE_ID AS FILE_PUB_ID,T.VERSION_ID FROM JECN_FILE_T T"
                + "        LEFT JOIN JECN_FILE JF ON T.FILE_ID = JF.FILE_ID AND JF.DEL_STATE=0"
                + "       ,JECN_RULE_T JR WHERE T.DEL_STATE=0 AND T.FILE_ID = JR.FILE_ID AND JR.IS_DIR=2 AND JR.IS_FILE_LOCAL=0 AND JR.IS_FILE_LOCAL=0 AND JR.ID=" + id + ") RULE_FILE"
                + "       WHERE RULE_FILE.FILE_PUB_ID IS NULL";
        return sql;

    }

}
