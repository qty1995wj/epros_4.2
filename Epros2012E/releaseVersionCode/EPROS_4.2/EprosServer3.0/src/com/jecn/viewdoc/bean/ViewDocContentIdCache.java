package com.jecn.viewdoc.bean;

import java.io.Serializable;

public class ViewDocContentIdCache implements Serializable {

	private Long id;

	private Long relatedId;
	
	/** 文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3*/
	private Integer type;

	private Long contentId;

	private String cachePath;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getContentId() {
		return contentId;
	}

	public void setContentId(Long contentId) {
		this.contentId = contentId;
	}

	public String getCachePath() {
		return cachePath;
	}

	public void setCachePath(String cachePath) {
		this.cachePath = cachePath;
	}

}
