package com.jecn.epros.server.bean.process;

/**
 * 活动和角色的关系（设计器）
 * 
 * @author Administrator
 * 
 */
public class JecnToFromActive implements java.io.Serializable {

	/** 对应TO FROM JecnFlowStructureImageT的figureId 主键id**/
	private Long toFromFigureId=-1L;
	/** 对应活动JecnFlowStructureImageT的figureId **/
	private Long activeFigureId=-1L;
	/** 流程id**/
	private Long flowId;
	
	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public Long getToFromFigureId() {
		return toFromFigureId;
	}
	public void setToFromFigureId(Long toFromFigureId) {
		this.toFromFigureId = toFromFigureId;
	}
	public Long getActiveFigureId() {
		return activeFigureId;
	}
	public void setActiveFigureId(Long activeFigureId) {
		this.activeFigureId = activeFigureId;
	}
}
