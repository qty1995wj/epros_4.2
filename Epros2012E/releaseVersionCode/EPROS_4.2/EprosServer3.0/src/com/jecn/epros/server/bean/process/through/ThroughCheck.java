package com.jecn.epros.server.bean.process.through;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;

import com.jecn.epros.bean.checkout.CheckoutItem;
import com.jecn.epros.bean.checkout.CheckoutResult;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.service.process.buss.FullPathManager;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 十所特殊需求，子流程的统计是通过接口活动来获得的，不通过普通接口，因为活动可以填活动编号，活动编号可以比大小。
 * 普通客户使用普通的接口就可以
 * 
 * @author hyl
 * 
 */
public class ThroughCheck {
	private Long flowId;
	private IBaseDao baseDao;
	/**
	 * 上级流程
	 */
	private List<Long> aIds = new ArrayList<Long>();
	/**
	 * 下级流程
	 */
	private List<Long> bIds = new ArrayList<Long>();
	/**
	 * 子流程,过程接口流程
	 */
	private List<Long> cIds = new ArrayList<Long>();
	private FullPathManager manager;
	private List<JecnFlowStructureImageT> acts = null;
	// 活动对应的数字编号
	private Map<Long, Double> figureNumMap = new HashMap<Long, Double>();
	// 活动对应的活动bean
	private Map<Long, JecnFlowStructureImageT> figureImageMap = new HashMap<Long, JecnFlowStructureImageT>();
	// 活动链接(活动接口类型的活动)对应的活动，k对应的流程ID v对应的活动的集合
	private Map<Long, List<JecnFlowStructureImageT>> linkFiguresMap = new HashMap<Long, List<JecnFlowStructureImageT>>();

	public ThroughCheck(IBaseDao baseDao, Long flowId) {
		this.flowId = flowId;
		this.baseDao = baseDao;
	}

	public CheckoutResult check() {
		String sql = "select flow_jfs.flow_id,flow_jfs.flow_name,flow_jfsi.impl_type"
				+ "            from jecn_flow_structure_image_t flow_jfsi,jecn_flow_structure_t flow_jfs"
				+ "            where flow_jfsi.impl_type in ('1','2','3','4') and flow_jfsi.figure_type in "
				+ JecnCommonSql.getImplString()
				+ " and flow_jfsi.link_flow_id=flow_jfs.flow_id and flow_jfs.del_state=0"
				+ "            and flow_jfsi.flow_id =" + flowId;
		List<Object[]> data = baseDao.listNativeSql(sql);
		for (Object[] str : data) {
			Long id = Long.valueOf(str[0].toString());
			if ("1".equals(str[2].toString())) {
				aIds.add(id);
			} else if ("2".equals(str[2].toString())) {
				bIds.add(id);
			} else if ("3".equals(str[2].toString()) || "4".equals(str[2].toString())) {
				cIds.add(id);
			}
		}

		// 获得所有的活动（为了比编号大小）
		acts = getActs(this.flowId);
		toMap();

		manager = new FullPathManager(baseDao, 0);
		manager.add(flowId);
		manager.add(this.cIds);
		manager.generate();
		FlowInouts selfIns = new FlowInouts(baseDao, 0, this.flowId);
		FlowInouts selfOuts = new FlowInouts(baseDao, 1, this.flowId);
		// FlowInouts aIns = new FlowInouts(baseDao, 0, this.aIds);
		FlowInouts aOuts = new FlowInouts(baseDao, 1, this.aIds);
		FlowInouts bIns = new FlowInouts(baseDao, 0, this.bIds);
		FlowInouts bOuts = new FlowInouts(baseDao, 1, this.bIds);
		FlowInouts cIns = new FlowInouts(baseDao, 0, this.cIds);
		FlowInouts cOuts = new FlowInouts(baseDao, 1, this.cIds);
		ActivityInouts selfAcIns = new ActivityInouts(baseDao, 0, this.flowId);
		ActivityInouts selfAcOuts = new ActivityInouts(baseDao, 1, this.flowId);

		CheckoutItem item = null;
		List<CheckoutItem> ins = new ArrayList<CheckoutItem>();
		List<CheckoutItem> outs = new ArrayList<CheckoutItem>();
		CheckoutResult result = new CheckoutResult();
		result.setName(manager.getFullPath(flowId));
		result.setIns(ins);
		result.setOuts(outs);
		// 输入
		for (Object[] str : selfIns.getInouts()) {
			if (str[4] == null) {
				continue;
			}
			String name = str[4].toString();
			if (StringUtils.isBlank(name)) {
				continue;
			}
			item = new CheckoutItem();
			ins.add(item);
			item.setName(name);
			List<String> cellA = new ArrayList<String>();
			List<String> cellB = new ArrayList<String>();
			item.setCellA(cellA);
			item.setCellB(cellB);
			// 上级流程输出
			cellA.addAll(wrap(aOuts.getFlowNames(name), "上游流程输出"));

			// 下游流程的输入
			cellB.addAll(wrap(bOuts.getFlowNames(name), "下游流程的输入"));
			// 子流程输入 过程流程输入
			cellB.addAll(wrap(cIns.getFlowNames(name), "子流程输入"));
			// 本流程活动输入
			cellB.addAll(wrap(selfAcIns.getActivityNums(name), "活动"));
		}
		// 输出
		for (Object[] str : selfOuts.getInouts()) {
			if (str[4] == null) {
				continue;
			}
			String name = str[4].toString();
			if (StringUtils.isBlank(name)) {
				continue;
			}
			item = new CheckoutItem();
			outs.add(item);
			item.setName(name);
			List<String> cellA = new ArrayList<String>();
			List<String> cellB = new ArrayList<String>();
			item.setCellA(cellA);
			item.setCellB(cellB);
			// 下游流程输入
			cellA.addAll(wrap(bIns.getFlowNames(name), "下游流程输入"));

			// 本流程的输入
			cellB.addAll(wrap(selfIns.getFlowNames(name), "当前流程的输入"));
			// 子流程输出 过程流程的输出
			cellB.addAll(wrap(cOuts.getFlowNames(name), "子流程输出"));
			// 本流程活动输出
			cellB.addAll(wrap(selfAcOuts.getActivityNums(name), "活动"));
		}

		// 子流程
		List<CheckoutResult> sons = new ArrayList<CheckoutResult>();
		result.setSons(sons);
		if (JecnUtil.isNotEmpty(cIds)) {
			CheckoutResult son = null;
			List<JecnFlowStructureT> flows = getFlows(cIds);
			for (JecnFlowStructureT sonFlow : flows) {
				son = new CheckoutResult();
				List<CheckoutItem> sonInItems = new ArrayList<CheckoutItem>();
				List<CheckoutItem> sonOutItems = new ArrayList<CheckoutItem>();
				son.setName(manager.getFullPath(sonFlow.getFlowId()));
				son.setIns(sonInItems);
				son.setOuts(sonOutItems);
				sons.add(son);
				FlowInouts sonIns = new FlowInouts(baseDao, 0, sonFlow.getFlowId());
				CheckoutItem sonInItem = null;
				for (Object[] objs : sonIns.getInouts()) {
					if (objs[4] == null) {
						continue;
					}
					String name = objs[4].toString();
					if (StringUtils.isBlank(name)) {
						continue;
					}
					sonInItem = new CheckoutItem();
					sonInItems.add(sonInItem);
					sonInItem.setName(name);
					List<String> cellA = new ArrayList<String>();
					sonInItem.setCellA(cellA);
					// 本流程输入
					cellA.addAll(wrap(selfIns.getFlowNames(JecnUtil.nullToEmpty(objs[4])), "当前流程输入"));
					// 子流程、过程流程输出（编号小的）
					cellA
							.addAll(wrap(getMathSonInoutByOut(sonFlow.getFlowId(), JecnUtil.nullToEmpty(objs[4])),
									"子流程输出"));
					// 活动输出（编号小的）
					cellA.addAll(wrap(getMathActivityNumByUpOuts2(sonFlow.getFlowId(), JecnUtil.nullToEmpty(objs[4])),
							"活动"));
				}

				FlowInouts sonOuts = new FlowInouts(baseDao, 1, sonFlow.getFlowId());
				CheckoutItem sonOutItem = null;
				for (Object[] objs : sonOuts.getInouts()) {
					if (objs[4] == null) {
						continue;
					}
					String name = objs[4].toString();
					if (StringUtils.isBlank(name)) {
						continue;
					}
					sonOutItem = new CheckoutItem();
					sonOutItems.add(sonOutItem);
					sonOutItem.setName(name);
					List<String> cellA = new ArrayList<String>();
					sonOutItem.setCellA(cellA);
					// 本流程输出
					cellA.addAll(wrap(selfOuts.getFlowNames(objs[4].toString()), "当前流程输出"));
					// 子流程、过程流程输入（编号大的）
					cellA
							.addAll(wrap(getMathSonInoutByIn(sonFlow.getFlowId(), JecnUtil.nullToEmpty(objs[4])),
									"子流程输入"));
					// 活动输入（编号大的）
					cellA.addAll(wrap(getMathActivityNumByNextIns2(sonFlow.getFlowId(), objs[4].toString()), "活动"));
				}
			}
		}
		return result;
	}

	private List<String> getMathSonInoutByOut(Long sonFlowId, String name) {
		return getMathSonInout(sonFlowId, name, false, 1);
	}

	private List<String> getMathSonInoutByIn(Long sonFlowId, String name) {
		return getMathSonInout(sonFlowId, name, true, 0);
	}

	private List<String> getMathSonInout(Long sonFlowId, String name, boolean big, int type) {
		List<String> result = new ArrayList<String>();
		if (sonFlowId == null || StringUtils.isBlank(name)) {
			return result;
		}
		Set<Long> ids = getLinkIdsByFigureId(getBigOrSmallLinkFiguresByNum(sonFlowId, big));
		if (JecnUtil.isEmpty(ids)) {
			return result;
		}
		String sql = "select distinct b.flow_name from jecn_flow_in_out_t a"
				+ " inner join jecn_flow_structure_t b on a.flow_id=b.flow_id"
				+ " where a.name=? and a.type=? and b.flow_id <> ? and b.flow_id in " + JecnCommonSql.getIdsSet(ids);

		return baseDao.listObjectNativeSql(sql, "flow_name", Hibernate.STRING, name, type, sonFlowId);
	}

	private Set<Long> getLinkIdsByFigureId(Set<Long> ids) {
		Set<Long> result = new HashSet<Long>();
		if (JecnUtil.isEmpty(ids)) {
			return result;
		}
		for (Long id : ids) {
			JecnFlowStructureImageT image = figureImageMap.get(id);
			if (image != null && image.getLinkFlowId() != null) {
				result.add(image.getLinkFlowId());
			}
		}
		return result;
	}

	private Set<Long> getBigOrSmallLinkFiguresByNum(Long sonFlowId, boolean big) {
		Set<Long> result = new HashSet<Long>();
		List<JecnFlowStructureImageT> images = linkFiguresMap.get(sonFlowId);
		if (JecnUtil.isEmpty(images)) {
			return result;
		}
		Set<Double> nums = new HashSet<Double>();
		Set<Long> skip = new HashSet<Long>();
		for (JecnFlowStructureImageT image : images) {
			nums.add(figureNumMap.get(image.getFigureId()));
			skip.add(image.getFigureId());
		}

		for (Map.Entry<Long, Double> entry : this.figureNumMap.entrySet()) {
			if (skip.contains(entry.getKey())) {
				continue;
			}
			JecnFlowStructureImageT image = figureImageMap.get(entry.getKey());
			// 接口活动
			if (!"ActivityImplFigure".equals(image.getFigureType())
					|| (image.getImplType() != 3 && image.getImplType() != 4) || image.getLinkFlowId() == null) {// 跳过非子节点和过程节点的
				continue;
			}
			for (Double num : nums) {
				if (entry.getValue() == null) {
					continue;
				}
				if (big && entry.getValue() > num) {
					result.add(entry.getKey());
				} else if (!big && entry.getValue() < num) {
					result.add(entry.getKey());
				}
			}
		}
		return result;
	}

	private void toMap() {
		if (JecnUtil.isEmpty(this.acts)) {
			return;
		}
		for (JecnFlowStructureImageT image : this.acts) {
			Double d = toDouble(image.getActivityId());
			figureNumMap.put(image.getFigureId(), d);
			figureImageMap.put(image.getFigureId(), image);
			if ("ActivityImplFigure".equals(image.getFigureType())) {
				Long linkFlowId = image.getLinkFlowId();
				if (linkFlowId != null) {
					List<JecnFlowStructureImageT> list = linkFiguresMap.get(linkFlowId);
					if (list == null) {
						list = new ArrayList<JecnFlowStructureImageT>();
						linkFiguresMap.put(linkFlowId, list);
					}
					list.add(image);
				}
			}
		}
	}

	private List<JecnFlowStructureImageT> getActs(Long flowId) {
		String hql = "from JecnFlowStructureImageT where flowId=? and figureType in "
				+ JecnCommonSql.getOperationActiveString();
		return baseDao.listHql(hql, flowId);
	}

	private List<String> wrap(List<String> names, String type) {
		List<String> result = new ArrayList<String>();
		if (JecnUtil.isEmpty(names)) {
			return result;
		}
		for (String name : names) {
			if (StringUtils.isBlank(name)) {
				continue;
			}
			result.add(name + "(" + type + ")");
		}
		return result;
	}

	/**
	 * 获得子流程对应的活动的的上级活动输出，如果输出匹配，那么把上级活动编号返回
	 * 
	 * @param flowId2
	 * @return
	 */
	private List<String> getMathActivityNumByUpOuts(Long sonFlowId, String name) {
		String sql = "select jfsip.activity_id from jecn_flow_structure_image_t jfsi"
				+ "  inner join jecn_flow_structure_image_t line on jfsi.figure_id=line.end_figure  and jfsi.impl_type=4 and jfsi.link_flow_id=?"
				+ " and line.figure_type in ('ManhattanLine','CommonLine')"
				+ "  inner join jecn_flow_structure_image_t jfsip on jfsip.figure_id=line.start_figure and jfsip.figure_type in "
				+ JecnCommonSql.getActiveString()
				+ "  inner join jecn_figure_in_out_t jfiop on jfsip.figure_id=jfiop.figure_id and jfiop.type=1 and jfiop.name=? ";
		return baseDao.listObjectNativeSql(sql, "activity_id", Hibernate.STRING, sonFlowId, name);
	}

	/**
	 * 获得子流程对应的活动的的下级活动输入，如果输入匹配，那么把下级活动编号返回
	 * 
	 * @param flowId2
	 * @return
	 */
	private List<String> getMathActivityNumByNextIns(Long sonFlowId, String name) {
		String sql = "select jfsip.activity_id from jecn_flow_structure_image_t jfsi"
				+ "  inner join jecn_flow_structure_image_t line on jfsi.figure_id=line.start_figure and jfsi.impl_type=4 and jfsi.link_flow_id=?"
				+ " and line.figure_type in ('ManhattanLine','CommonLine')"
				+ "  inner join jecn_flow_structure_image_t jfsip on jfsip.figure_id=line.end_figure and jfsip.figure_type in "
				+ JecnCommonSql.getActiveString()
				+ "  inner join jecn_figure_in_out_t jfiop on jfsip.figure_id=jfiop.figure_id and jfiop.type=0 and jfiop.name=?";
		return baseDao.listObjectNativeSql(sql, "activity_id", Hibernate.STRING, sonFlowId, name);
	}

	/**
	 * 获得子流程、过程接口流程对应的活动的的上级活动输出(编号比当前接口活动编号小的)，如果输出匹配，那么把上级活动编号返回
	 * 
	 * @param flowId2
	 * @return
	 */
	private List<String> getMathActivityNumByUpOuts2(Long sonFlowId, String name) {
		List<String> result = new ArrayList<String>();
		if (StringUtils.isBlank(name)) {
			return result;
		}
		Set<Long> ids = getSonInoutFigureIds(sonFlowId, false);
		if (JecnUtil.isEmpty(ids)) {
			return result;
		}
		return getInoutMath(name, ids, 1);
	}

	/**
	 * 获得子流程、过程接口流程对应的活动的的下级活动输入(编号比当前接口活动编号大的)，如果输入匹配 ，那么把下级活动编号返回
	 * 
	 * @param flowId2
	 * @return
	 */
	private List<String> getMathActivityNumByNextIns2(Long sonFlowId, String name) {
		List<String> result = new ArrayList<String>();
		if (StringUtils.isBlank(name)) {
			return result;
		}
		Set<Long> ids = getSonInoutFigureIds(sonFlowId, true);
		if (JecnUtil.isEmpty(ids)) {
			return result;
		}
		return getInoutMath(name, ids, 0);
	}

	private List<String> getInoutMath(String name, Set<Long> ids, int type) {
		String sql = "select distinct b.activity_id as num from jecn_figure_in_out_t a"
				+ "  inner join jecn_flow_structure_image_t b on a.figure_id=b.figure_id"
				+ "  where a.flow_id=? and a.name=? and a.type=? and b.figure_id in" + JecnCommonSql.getIdsSet(ids);
		return this.baseDao.listObjectNativeSql(sql, "num", Hibernate.STRING, this.flowId, name, type);
	}

	// TODO
	private Set<Long> getSonInoutFigureIds(Long sonFlowId, boolean big) {
		List<JecnFlowStructureImageT> list = linkFiguresMap.get(sonFlowId);
		if (JecnUtil.isEmpty(list)) {
			return new HashSet<Long>();
		}
		// 找到多个接口活动对应的活动
		Set<Double> nums = new HashSet<Double>();
		Set<Long> skip = new HashSet<Long>();
		for (JecnFlowStructureImageT image : list) {
			if (image.getImplType() == 3 || image.getImplType() == 4) {
				skip.add(image.getFigureId());
				Double d = this.figureNumMap.get(image.getFigureId());
				if (d != null) {
					nums.add(d);
				}
			}
		}

		return getBigOrSmall(nums, skip, big);
	}

	private Set<Long> getBigOrSmall(Set<Double> nums, Set<Long> skip, boolean big) {
		Set<Long> result = new HashSet<Long>();
		if (JecnUtil.isEmpty(nums)) {
			return result;
		}
		for (Map.Entry<Long, Double> entry : this.figureNumMap.entrySet()) {
			if (skip.contains(entry.getKey())) {
				continue;
			}
			for (Double num : nums) {
				if (entry.getValue() == null) {
					continue;
				}
				if (big && entry.getValue() > num) {
					result.add(entry.getKey());
				} else if (!big && entry.getValue() < num) {
					result.add(entry.getKey());
				}
			}
		}
		return result;
	}

	private Double toDouble(String num) {
		if (num == null) {
			return null;
		}
		try {
			return Double.valueOf(num);
		} catch (Exception ignore) {
		}
		return null;
	}

	private List<JecnFlowStructureT> getFlows(List<Long> ids) {
		if (JecnUtil.isEmpty(ids)) {
			return null;
		}
		String hql = "from JecnFlowStructureT where flowId in " + JecnCommonSql.getIds(ids);
		return this.baseDao.listHql(hql);
	}

}
