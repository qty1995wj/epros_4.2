package com.jecn.epros.server.action.web.login.ad.libang;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;
import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.HttpRequestUtil;

/**
 * 立邦单点登录
 * 运行流程
 * 1、filter拦截请求，客户端通过client_id请求授权服务器的认证code，附上回调地址
 * 2、跳转到立邦的登录地址，输入用户名密码认证
 * 3、授权服务器通过回调地址返回code
 * 4、授权服务器验证完地址和code之后返回token
 * 5、客户端通过token向授权服务器换取用户名
 */
public class LiBangLoginAction extends JecnAbstractADLoginAction {

	public LiBangLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	public String login() {
		String uid = (String) loginAction.getRequest().getSession().getAttribute("uid");
		String user = (String) loginAction.getRequest().getSession().getAttribute("user");
		if (StringUtils.isNotBlank(uid) && StringUtils.isNotBlank(user)) {
			log.info("uid " + uid);
			log.info("user " + user);
			log.info("跳过权限验证");
			return loginByLoginDomainName(uid);
		}

		HttpServletRequest request = this.loginAction.getRequest();
		HttpServletResponse response = this.loginAction.getResponse();

		// 第二步获取code
		String code = request.getParameter("code");
		log.info("code:" + code);
		String oauthurl = LiBangAfterItem.MAIN_URL;
		try {
			// 执行第三步，用code去换取token
			String ssotoken = LiBangAfterItem.TOKEN_URL;
			String client_id = LiBangAfterItem.CLIENT_ID;
			String client_secret = LiBangAfterItem.CLIENT_SECRET;
			long time = System.currentTimeMillis();
			String tokenparam = "client_id=" + client_id + "&client_secret=" + client_secret + "&code=" + code
					+ "&grant_type=authorization_code&" + "oauth_timestamp=" + time + "&redirect_uri="
					+ LiBangAfterItem.LOGIN_URL;
			log.info("tokenparam:" + tokenparam);
			String token = getResult(ssotoken, tokenparam);
			log.info("token:" + token);
			if (StringUtils.isBlank(token) || token.indexOf("失败") != -1) {
				log.error("获得token异常：" + token);
				loginAction.setCustomRedirectURL(oauthurl + "/oauth_error.jsp?message=1");
				return LoginAction.REDIRECT;
			}

			// 执行第四步，用token去换取用户信息
			String ssoprofile = LiBangAfterItem.USERINFO_URL;
			log.info("ssoprofile:" + ssoprofile);
			String uidJson = getResult(ssoprofile, token);
			log.info("uidJson:" + uidJson);
			if (StringUtils.isBlank(uidJson) || uidJson.equals("null")) {
				log.error("获得用户信息异常");
				loginAction.setCustomRedirectURL(oauthurl + "/oauth_error.jsp?message=2");
				return LoginAction.REDIRECT;
			}

			// 解析json格式的用户信息
			JSONObject userObj = JSONObject.parseObject(uidJson);
			uid = userObj.getString("id");
			// String uid = "D010844";
			// 将用户信息存放到session中。应用系统当做用户登录成功。
			request.getSession().setAttribute("uid", uid);
			request.getSession().setAttribute("user", uid);
			log.info("uid:" + uid);
			return loginByLoginDomainName(uid);
		} catch (Exception e) {
			log.error(e);
		}
		loginAction.setCustomRedirectURL(oauthurl + "/oauth_error.jsp");
		return LoginAction.REDIRECT;
	}

	static class miTM implements javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}

	public static String getResult(String sendUrl, String param) throws Exception {
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		// javax.net.ssl.SSLContext sc =
		// javax.net.ssl.SSLContext.getInstance("SSL");
		javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSLv3");

		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		HostnameVerifier ignoreHostnameVerifier = new HostnameVerifier() {
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}
		};

		HttpsURLConnection.setDefaultHostnameVerifier(ignoreHostnameVerifier);

		DataOutputStream out = null;
		BufferedReader reader = null;
		String result = "";
		URL url = null;
		HttpsURLConnection conn = null;
		try {
			url = new URL(sendUrl);
			conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
			// 必须设置false，否则会自动redirect到重定向后的地址
			conn.setInstanceFollowRedirects(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestProperty("Charset", "UTF-8");
			conn.setRequestProperty("Connection", "Keep-Alive");

			conn.getOutputStream().write(param.getBytes());

			conn.connect();
			InputStream input = conn.getInputStream();
			reader = new BufferedReader(new InputStreamReader(input, "UTF-8"));
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			result = sb.toString();
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
			if (out != null) {
				out.close();
			}
			if (reader != null) {
				reader.close();
			}
		}
		return result;
	}

	/**
	 * 模拟请求
	 * 
	 * @param url
	 *            资源地址
	 * @param map
	 *            参数列表
	 * @param encoding
	 *            编码
	 * @return
	 * @throws Exception
	 */
	public static String send(String url, Map<String, String> map, String encoding) throws Exception {
		String body = "";

		// 创建httpclient对象
		CloseableHttpClient client = HttpClients.createDefault();
		// 创建post方式请求对象
		HttpPost httpPost = new HttpPost(url);

		// 装填参数
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		if (map != null) {
			for (Entry<String, String> entry : map.entrySet()) {
				nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			}
		}
		// 设置参数到请求对象中
		httpPost.setEntity(new UrlEncodedFormEntity(nvps, encoding));

		System.out.println("请求地址：" + url);
		System.out.println("请求参数：" + nvps.toString());

		// 设置header信息
		// 指定报文头【Content-type】、【User-Agent】
		httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
		httpPost.setHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

		// 执行请求操作，并拿到结果（同步阻塞）
		CloseableHttpResponse response = client.execute(httpPost);
		// 获取结果实体
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			// 按指定编码转换结果实体为String类型
			body = EntityUtils.toString(entity, encoding);
		}
		EntityUtils.consume(entity);
		// 释放链接
		response.close();
		return body;
	}

	public static String getIpAddr(HttpServletRequest request) throws Exception {
		String ip = request.getHeader("X-Real-IP");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return ip;
		}
		ip = request.getHeader("X-Forwarded-For");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			// 多次反向代理后会有多个IP值，第一个为真实IP。
			int index = ip.indexOf(',');
			if (index != -1) {
				return ip.substring(0, index);
			} else {
				return ip;
			}
		} else {
			return request.getRemoteAddr();
		}
	}

	
	/**
	 * @author angus
	 * @deprecated
	 * 	链接带有ticket   调用基建Api获取Ticket所对应的OpenID、Account，若该OpenID已经绑定了账号，则Account有值
	 * 
	 */
	public String WXlogin(){
		String ticket = loginAction.getRequest().getParameter("Ticket");
		String wx_login_url = LiBangAfterItem.WX_LOGIN_RUL;
		String rep = HttpRequestUtil.sendPost(wx_login_url, "{\"Ticket\":\""+ticket+"\"}");
		JSONObject json = JSONObject.parseObject(rep);
		JSONObject data;
		String account;
		if("0".equals(json.getString("Code"))){
			data = json.getJSONObject("Data");
			account = data.getString("Account");
		}else{
			log.error("立邦微信登录ticket失败"+json.getString("Msg"));
			return LoginAction.REDIRECT; 
		}
		
		
		return loginByLoginName(account);

	}
}
