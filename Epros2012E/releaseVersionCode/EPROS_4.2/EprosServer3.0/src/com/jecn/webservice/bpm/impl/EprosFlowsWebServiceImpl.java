package com.jecn.webservice.bpm.impl;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.EprosBPMVersion;
import com.jecn.epros.server.service.process.IEprosBPMVersionService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.webservice.bpm.IEprosFlowsWebService;

@WebService
public class EprosFlowsWebServiceImpl implements IEprosFlowsWebService {
	private static final Logger log = Logger.getLogger(EprosFlowsWebServiceImpl.class);

	@Override
	@WebMethod
	public List<EprosBPMVersion> findEprosBpmVersions(String flowId) {
		IEprosBPMVersionService bpmVersionService = (IEprosBPMVersionService) ApplicationContextUtil.getContext()
				.getBean("bpmVersionServiceImpl");
		try {
			return bpmVersionService.findEprosBpmVersions(flowId);
		} catch (Exception e) {
			log.error("BPM调用webservice接口异常！", e);
			return null;
		}
	}
}
