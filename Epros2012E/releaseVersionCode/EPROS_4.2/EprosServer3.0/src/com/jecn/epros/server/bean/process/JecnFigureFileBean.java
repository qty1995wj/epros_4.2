package com.jecn.epros.server.bean.process;

import java.io.Serializable;

/**
 * 流程地图元素关联附件真实表
 * 
 * 目前流程图的文档元素也使用此bean存储数据
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-4 时间：上午10:44:33
 */
public class JecnFigureFileBean implements Serializable {
	/** 主键ID */
	private String id;
	/** 关联地图元素ID */
	private String figureId;
	private String figureUUID;// 元素索引
	/** 关联附件ID */
	private String fileId;
	/** 关联地图元素类型 */
	private String figureType;
	private String UUID;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFigureId() {
		return figureId;
	}

	public void setFigureId(String figureId) {
		this.figureId = figureId;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFigureType() {
		return figureType;
	}

	public void setFigureType(String figureType) {
		this.figureType = figureType;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String UUID) {
		this.UUID = UUID;
	}
}
