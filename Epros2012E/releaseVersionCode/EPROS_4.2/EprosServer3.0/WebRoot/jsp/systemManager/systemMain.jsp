<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

	</head>

	<body>
		<div id="sysMain_id" style="width: 100%; height: 100%">
			<div id='sysMainLeft_id'
				style="width: 175px; height: 100%; margin-left: 2px; padding: 0px; background: #F2F2F2;">
				<ul id=sysMainUl_id>
				<s:if test="#application.isShowUserManager">
					<!-- 人员管理 -->
					<li
						id="${basePath}jsp/systemManager/personManageSearch.jsp;personManageGrid">
						<s:text name="personManage" />
					</li>
				</s:if>
				<s:if test="#application.isShowRoleManager">
					<!-- 角色管理 -->
					<li
						id="${basePath}jsp/systemManager/rolepersonManageSearch.jsp;roleManageGrid">
						<s:text name="roleManage" />
					</li>
				</s:if>
					
					<!--人员导入 -->
					<li
						id="${basePath}jsp/systemManager/import/importMain.jsp;personImport">
						<s:text name="personImport" />
					</li>
					<!-- 流程检错 -->
					<li id="${basePath}jsp/myhome/processCheck.jsp;processCheckGrid">
						<s:text name="processCheck" />
					</li>
				</ul>
			</div>
		</div>
	</body>
</html>
