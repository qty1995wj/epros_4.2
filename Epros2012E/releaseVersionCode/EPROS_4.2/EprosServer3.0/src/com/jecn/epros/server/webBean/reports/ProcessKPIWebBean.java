package com.jecn.epros.server.webBean.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 流程KPI跟踪表
 * 
 * 一KPI属性+其KPI值
 * 
 * @author huoyl
 * 
 */
public class ProcessKPIWebBean implements Serializable {
	/** 流程ID */
	private long flowId;
	/** 流程名称 */
	private String flowName;
	/** 责任部门名称 */
	private String orgName;

	/** 流程责任人类型： 0是人员，1是岗位 */
	private int typeResPeople=-1;
	/** 流程责任人Id */
	private Long resPeopleId;
	/** 流程责任人名称 */
	private String resPeopleName;

	/** 流程监护人ID */
	private Long wardPeopleId;
	/** 流程监护人名称 */
	private String wardPeopleName;

	/** 流程拟制人ID */
	private Long draftPersonId;
	/** 流程拟制人名称 */
	private String draftPersonName;

	/** 流程KPI id */
	private long kpiId;
	/** 流程KPI 名称 */
	private String kpiName;

	/** 指标来源 0:二级指标 1:个人PBC指标 2:其他重要工作 */
	private String kpiTargetType;
	/** 支撑的一级指标 */
	private String kpiFirstTarget;
	/** 相关度 0：强相关 1：弱相关 2：不相关 */
	private String kpiRelevance;
	/** 流程KPI计算公式 */
	private String kpiStatisticalMethods;

	/** 流程KPI目标值：KPI目标值比较符号 0：= 1：> 2：< 3:>= 4:<= */
	private String kpiTargetOperator;
	/** 流程KPI目标值：值 */
	private double kpiTarget=-1;
	/**
	 * 流程kpi目标值:KPI值单位名称(纵向单位) 0:百分比 1:季度 2:月 3:周 4:工作日 5:天 6:小时 7:分钟 8:个 9:人
	 * 10:ppm 11:元、12：次
	 */
	private String kpiVertical;

	/** 数据获取方式 0:人工 1：系统 */
	private String kpiDataMethod;
	/** IT系统 存在多个 */
	private String itSystem;
	/** 数据提供者 */
	private String kpiDataPeopleName;
	/** 数据统计时间\频率 0：月 1：季度 */
	private String kpiHorizontal;
	/** KPI对应的所有值集合 */
	private List<ProcessKPIWebValueBean> kpiValueList = new ArrayList<ProcessKPIWebValueBean>();

	private String kpiPurpose;//设置目的
	private String kpiPoint;//测量点
	private String kpiPeriod;//统计周期
	private String kpiExplain;//说明
	
	private String kpiDefinition;//kpi定义
	private String kpiType;//kpi类型
	public String getKpiType() {
		return kpiType;
	}

	public void setKpiType(String kpiType) {
		this.kpiType = kpiType;
	}

	public String getKpiDefinition() {
		return kpiDefinition;
	}

	public void setKpiDefinition(String kpiDefinition) {
		this.kpiDefinition = kpiDefinition;
	}

	public String getKpiPurpose() {
		return kpiPurpose;
	}

	public void setKpiPurpose(String kpiPurpose) {
		this.kpiPurpose = kpiPurpose;
	}

	public String getKpiPoint() {
		return kpiPoint;
	}

	public void setKpiPoint(String kpiPoint) {
		this.kpiPoint = kpiPoint;
	}

	public String getKpiPeriod() {
		return kpiPeriod;
	}

	public void setKpiPeriod(String kpiPeriod) {
		this.kpiPeriod = kpiPeriod;
	}

	public String getKpiExplain() {
		return kpiExplain;
	}

	public void setKpiExplain(String kpiExplain) {
		this.kpiExplain = kpiExplain;
	}

	public void setKpiValueList(List<ProcessKPIWebValueBean> kpiValueList) {
		this.kpiValueList = kpiValueList;
	}

	public long getFlowId() {
		return flowId;
	}

	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public int getTypeResPeople() {
		return typeResPeople;
	}

	public void setTypeResPeople(int typeResPeople) {
		this.typeResPeople = typeResPeople;
	}

	public Long getResPeopleId() {
		return resPeopleId;
	}

	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}

	public String getResPeopleName() {
		return resPeopleName;
	}

	public void setResPeopleName(String resPeopleName) {
		this.resPeopleName = resPeopleName;
	}

	public Long getWardPeopleId() {
		return wardPeopleId;
	}

	public void setWardPeopleId(Long wardPeopleId) {
		this.wardPeopleId = wardPeopleId;
	}

	public String getWardPeopleName() {
		return wardPeopleName;
	}

	public void setWardPeopleName(String wardPeopleName) {
		this.wardPeopleName = wardPeopleName;
	}

	public Long getDraftPersonId() {
		return draftPersonId;
	}

	public void setDraftPersonId(Long draftPersonId) {
		this.draftPersonId = draftPersonId;
	}

	public String getDraftPersonName() {
		return draftPersonName;
	}

	public void setDraftPersonName(String draftPersonName) {
		this.draftPersonName = draftPersonName;
	}

	public long getKpiId() {
		return kpiId;
	}

	public void setKpiId(long kpiId) {
		this.kpiId = kpiId;
	}

	public String getKpiName() {
		return kpiName;
	}

	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	public String getKpiTargetType() {
		return kpiTargetType;
	}

	public void setKpiTargetType(String kpiTargetType) {
		this.kpiTargetType = kpiTargetType;
	}

	public String getKpiFirstTarget() {
		return kpiFirstTarget;
	}

	public void setKpiFirstTarget(String kpiFirstTarget) {
		this.kpiFirstTarget = kpiFirstTarget;
	}

	public String getKpiRelevance() {
		return kpiRelevance;
	}

	public void setKpiRelevance(String kpiRelevance) {
		this.kpiRelevance = kpiRelevance;
	}

	public String getKpiStatisticalMethods() {
		return kpiStatisticalMethods;
	}

	public void setKpiStatisticalMethods(String kpiStatisticalMethods) {
		this.kpiStatisticalMethods = kpiStatisticalMethods;
	}

	public String getKpiTargetOperator() {
		return kpiTargetOperator;
	}

	public void setKpiTargetOperator(String kpiTargetOperator) {
		this.kpiTargetOperator = kpiTargetOperator;
	}

	public double getKpiTarget() {
		return kpiTarget;
	}

	public void setKpiTarget(double kpiTarget) {
		this.kpiTarget = kpiTarget;
	}

	public String getKpiVertical() {
		return kpiVertical;
	}

	public void setKpiVertical(String kpiVertical) {
		this.kpiVertical = kpiVertical;
	}

	public String getKpiDataMethod() {
		return kpiDataMethod;
	}

	public void setKpiDataMethod(String kpiDataMethod) {
		this.kpiDataMethod = kpiDataMethod;
	}

	public String getItSystem() {
		return itSystem;
	}

	public void setItSystem(String itSystem) {
		this.itSystem = itSystem;
	}

	public String getKpiDataPeopleName() {
		return kpiDataPeopleName;
	}

	public void setKpiDataPeopleName(String kpiDataPeopleName) {
		this.kpiDataPeopleName = kpiDataPeopleName;
	}

	public String getKpiHorizontal() {
		return kpiHorizontal;
	}

	public void setKpiHorizontal(String kpiHorizontal) {
		this.kpiHorizontal = kpiHorizontal;
	}

	public List<ProcessKPIWebValueBean> getKpiValueList() {
		return kpiValueList;
	}

	public void addProcessKPIWebValueBean(ProcessKPIWebValueBean kpiWebValueBean) {
		if (kpiWebValueBean == null) {
			return;
		}
		kpiValueList.add(kpiWebValueBean);
	}

	@Override
	public String toString() {
		return "ProcessKPIWebBean [draftPersonId=" + draftPersonId + ", draftPersonName=" + draftPersonName
				+ ", flowId=" + flowId + ", flowName=" + flowName + ", itSystem=" + itSystem + ", kpiDataMethod="
				+ kpiDataMethod + ", kpiDataPeopleName=" + kpiDataPeopleName + ", kpiDefinition=" + kpiDefinition
				+ ", kpiExplain=" + kpiExplain + ", kpiFirstTarget=" + kpiFirstTarget + ", kpiHorizontal="
				+ kpiHorizontal + ", kpiId=" + kpiId + ", kpiName=" + kpiName + ", kpiPeriod=" + kpiPeriod
				+ ", kpiPoint=" + kpiPoint + ", kpiPurpose=" + kpiPurpose + ", kpiRelevance=" + kpiRelevance
				+ ", kpiStatisticalMethods=" + kpiStatisticalMethods + ", kpiTarget=" + kpiTarget
				+ ", kpiTargetOperator=" + kpiTargetOperator + ", kpiTargetType=" + kpiTargetType + ", kpiType="
				+ kpiType + ", kpiValueList=" + kpiValueList + ", kpiVertical=" + kpiVertical + ", orgName=" + orgName
				+ ", resPeopleId=" + resPeopleId + ", resPeopleName=" + resPeopleName + ", typeResPeople="
				+ typeResPeople + ", wardPeopleId=" + wardPeopleId + ", wardPeopleName=" + wardPeopleName + "]";
	}
	
}
