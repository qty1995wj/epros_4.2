package com.jecn.epros.server.service.dataImport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 更新t_level t_path view_sort字段
 * 
 * @author user
 * 
 */
public class PathUpdater {

	private IBaseDao baseDao;

	public PathUpdater(IBaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public void updateOrgTLevelAndTPath() {
		Map<Long, SourceBean> idMaps = findOrgIdsMap();
		createSortId(idMaps);
		for (Long key : idMaps.keySet()) {
			PathBean pathBean = createPath(key, idMaps);
			if (pathBean == null) {
				continue;
			}
			updateTable(key, pathBean, "JECN_FLOW_ORG", "ORG_ID");
		}
	}
	
	public void updateFileTLevelAndTPath() {
		Map<Long, SourceBean> idMaps = findFileIdsMap();
		createSortId(idMaps);
		for (Long key : idMaps.keySet()) {
			PathBean pathBean = createPath(key, idMaps);
			if (pathBean == null) {
				continue;
			}
			updateTable(key, pathBean, "JECN_FILE_T", "FILE_ID");
		}
	}

	/**
	 * 岗位组更新T_PATH,T_LEVEL
	 */
	public void updateGroupLevelAndTPath() {
		Map<Long, SourceBean> idMaps = findGroupIdsMap();
		createSortId(idMaps);
		// 迭代
		for (Long key : idMaps.keySet()) {
			PathBean pathBean = createPath(key, idMaps);
			if (pathBean == null) {
				continue;
			}
			updateTable(key, pathBean, "JECN_POSITION_GROUP", "ID");
		}
	}

	// TODO
	// 由于同步之后部门表，岗位组表并没有给SORT_ID，所以只能新生成一遍SORT_ID，如果将来表中生成了SORT_ID那么就可以把这块代码删除
	private void createSortId(Map<Long, SourceBean> idMaps) {
		// key父节点 value为该节点下的节点当前的sort_id
		Map<Long, Integer> curSortId = new HashMap<Long, Integer>();
		for (Map.Entry<Long, SourceBean> entry : idMaps.entrySet()) {
			Long pid = entry.getValue().getPid();
			Integer value = curSortId.get(pid);
			if (value == null) {
				curSortId.put(pid, 1);
			} else {
				value++;
				curSortId.put(pid, value);
			}
			SourceBean sourceBean = entry.getValue();
			sourceBean.setSortId(Long.valueOf(curSortId.get(pid)));
		}
	}

	/**
	 * 更新表的path和level
	 * 
	 * @param keyId
	 *            主键id
	 * @param path
	 *            存放着path和level数据
	 * TODO SORT_ID
	 */
	private void updateTable(Long keyId, PathBean path, String tableName, String idName) {
		String sql = "update " + tableName + " set t_path='" + path.getPath() + "',t_level=" + path.getLevel()
				+ ",sort_id=" + path.getSortId() + ",view_sort='" + path.getViewSort() + "'" + " where " + idName + "="
				+ keyId;
		this.baseDao.execteNative(sql);
	}

	/**
	 * @return map key为子节点id value为父节点id
	 */
	private Map<Long, SourceBean> findGroupIdsMap() {
		Map<Long, SourceBean> idMaps = new HashMap<Long, SourceBean>();
		String sql = "SELECT ID,PER_ID,SORT_ID FROM JECN_POSITION_GROUP";
		List<Object[]> objs = this.baseDao.listNativeSql(sql);
		SourceBean bean = null;
		if (objs != null && objs.size() > 0) {
			for (Object[] obj : objs) {
				if (obj[0] == null || obj[1] == null) {
					continue;
				}
				bean = new SourceBean();
				bean.setId(Long.valueOf(obj[0].toString()));
				bean.setPid(Long.valueOf(obj[1].toString()));
				if (obj[2] != null) {
					bean.setSortId(Long.valueOf(obj[2].toString()));
				}
				idMaps.put(Long.valueOf(obj[0].toString()), bean);
			}
		}
		return idMaps;
	}

	private PathBean createPath(Long keyId, Map<Long, SourceBean> idMap) {
		String path = keyId + "-";
		String viewSort = getFullSortId(idMap.get(keyId).getSortId());
		// 如果不存在父节点那么level为0
		PathBean pathBean = recurrenceCreatePath(keyId, idMap, path, viewSort);
		if (pathBean == null || "".equals(pathBean.getPath() != null)) {
			return null;
		}
		// TODO
		// 由于同步之后部门表，岗位组表并没有给SORT_ID，所以只能新生成一遍SORT_ID，如果将来表中生成了SORT_ID那么久可以把这块代码删除
		pathBean.setSortId(idMap.get(keyId).getSortId());
		return pathBean;
	}

	private String getFullSortId(Long sortId) {
		int intSortId = sortId == null ? 0 : sortId.intValue();
		return JecnUtil.completeFourDigit(intSortId);
	}

	/**
	 * 迭代获得path 直到找到父节点为0的节点返回path
	 * 
	 * @return
	 */
	private PathBean recurrenceCreatePath(Long childId, Map<Long, SourceBean> idMap, String path, String viewSort) {
		Long parentId = idMap.get(childId).getPid();
		if (path != null && parentId != null && childId.longValue() != parentId.longValue()) {
			if (parentId.longValue() > 0) {
				path = parentId + "-" + path;
				viewSort = getFullSortId(idMap.get(parentId).getSortId()) + viewSort;
				return recurrenceCreatePath(parentId, idMap, path, viewSort);
			} else if (parentId == 0) {
				PathBean pathBean = new PathBean();
				pathBean.setPath(path);
				pathBean.setViewSort(viewSort);
				pathBean.setLevel(path.split("-").length - 1);
				return pathBean;
			}
		} else {
			return null;
		}
		return null;
	}

	/**
	 * @return map key为子节点id value为父节点id
	 */
	private Map<Long, SourceBean> findOrgIdsMap() {
		Map<Long, SourceBean> idMaps = new HashMap<Long, SourceBean>();
		String sql = "SELECT ORG_ID,PER_ORG_ID,SORT_ID FROM JECN_FLOW_ORG";
		List<Object[]> objs = this.baseDao.listNativeSql(sql);
		SourceBean bean = null;
		if (objs != null && objs.size() > 0) {
			for (Object[] obj : objs) {
				if (obj[0] == null || obj[1] == null) {
					continue;
				}
				bean = new SourceBean();
				bean.setId(Long.valueOf(obj[0].toString()));
				bean.setPid(Long.valueOf(obj[1].toString()));
				if (obj[2] != null) {
					bean.setSortId(Long.valueOf(obj[2].toString()));
				}
				idMaps.put(Long.valueOf(obj[0].toString()), bean);
			}
		}
		return idMaps;
	}
	
	private Map<Long, SourceBean> findFileIdsMap() {
		Map<Long, SourceBean> idMaps = new HashMap<Long, SourceBean>();
		String sql = "SELECT FILE_ID,PER_FILE_ID,SORT_ID FROM JECN_FILE_T WHERE NODE_TYPE = 1 and DEL_STATE = 0";
		List<Object[]> objs = this.baseDao.listNativeSql(sql);
		SourceBean bean = null;
		if (objs != null && objs.size() > 0) {
			for (Object[] obj : objs) {
				if (obj[0] == null || obj[1] == null) {
					continue;
				}
				bean = new SourceBean();
				bean.setId(Long.valueOf(obj[0].toString()));
				bean.setPid(Long.valueOf(obj[1].toString()));
				if (obj[2] != null) {
					bean.setSortId(Long.valueOf(obj[2].toString()));
				}
				idMaps.put(Long.valueOf(obj[0].toString()), bean);
			}
		}
		return idMaps;
	}

	class PathBean {

		/** 路径使用-连接 */
		private String path;
		/** 等级如果父节点为0的level为0 */
		private int level;
		private Long sortId;
		private String viewSort;

		public Long getSortId() {
			return sortId;
		}

		public void setSortId(Long sortId) {
			this.sortId = sortId;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public int getLevel() {
			return level;
		}

		public void setLevel(int level) {
			this.level = level;
		}

		public String getViewSort() {
			return viewSort;
		}

		public void setViewSort(String viewSort) {
			this.viewSort = viewSort;
		}

		@Override
		public String toString() {
			return " path:" + path + " level:" + level + " view_sort:" + viewSort;
		}
	}

	class SourceBean {
		private Long id;
		private Long pid;
		private Long sortId;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Long getPid() {
			return pid;
		}

		public void setPid(Long pid) {
			this.pid = pid;
		}

		public Long getSortId() {
			return sortId;
		}

		public void setSortId(Long sortId) {
			this.sortId = sortId;
		}
	}

}
