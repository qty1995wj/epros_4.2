package epros.designer.gui.choose;

import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;


public abstract class JecnCommonChooseTable extends JecnTable {

	@Override
	public JecnTableModel getTableModel() {
		return new JecnTableModel(getTableTitle(),
				getContent(null));
	}

	@Override
	public int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}
	public  void remoeAll(){
		//清空
		for (int index = this.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel)this.getModel()).removeRow(index);
        } 
	}
	public abstract Vector<String> getTableTitle();
	
	public abstract Vector<Vector<String>> getContent(List<JecnTreeBean> list);
}
