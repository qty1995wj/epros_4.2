/*
 * FromFigure.java
 *from元素
 * Created on 2008年12月3日, 下午5:17
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseCircle;

/**
 * from 元素
 * 
 * @author zhangxh
 * @date： 日期：Mar 22, 2012 时间：10:42:51 AM
 */
public class FromFigure extends BaseCircle {

	/** Creates a new instance of FromFigure */
	public FromFigure(JecnFigureData figureData) {
		super(figureData);
	}
}
