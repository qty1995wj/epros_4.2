package epros.designer.gui.file.fileMove;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.file.JecnFileCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class HighEfficiencyFileMoveTree extends JecnHighEfficiencyTree {
	private Logger log = Logger.getLogger(HighEfficiencyFileMoveTree.class);
	/** 当前项目根节点下的角色目录 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	
	public HighEfficiencyFileMoveTree(List<Long> listIds){
		super(listIds);
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(
			JecnHighEfficiencyTree jTree) {
		return new FileMoveTreeListener(this.getListIds(), jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getFileAction().getChildFileDirs(
					Long.valueOf(0), JecnConstants.projectId);
		} catch (Exception e) {
			log.error("HighEfficiencyFileMoveTree getTreeModel is error", e);
		}
		return JecnFileCommon.getTreeMoveModel(list, this.getListIds());
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		
	}

}
