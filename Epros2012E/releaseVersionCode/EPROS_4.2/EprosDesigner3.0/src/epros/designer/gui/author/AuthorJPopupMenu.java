package epros.designer.gui.author;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class AuthorJPopupMenu extends JPopupMenu {

	@Override
	public JMenuItem add(JMenuItem menuItem) {
		if (isAuthorMenu(menuItem.getActionCommand())) {
			super.add(menuItem);
		}
		return menuItem;
	}

	public enum AuthorEnum {
		// test, addTest;
		test(true, false, true), flow_addMenu(true, false, true);

		private boolean isAdmin;
		private boolean isDesigner;
		private boolean isSecondAdmin;

		private AuthorEnum(boolean isAdmin, boolean isSecondAdmin, boolean isDesigner) {
			this.isAdmin = isAdmin;
			this.isSecondAdmin = isSecondAdmin;
			this.isDesigner = isDesigner;
		}

		public boolean isAdmin() {
			return isAdmin;
		}

		public boolean isDesigner() {
			return isDesigner;
		}

		public boolean isSecondAdmin() {
			return isSecondAdmin;
		}

	}

	private boolean isAuthorMenu(String command) {
		for (AuthorEnum authorEnum : AuthorEnum.values()) {
			if (authorEnum.toString().equals(command)) {
				return true;
			}
		}
		return false;
	}
}
