package com.jecn.epros.service.home.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.home.*;
import com.jecn.epros.mapper.HomeMapper;
import com.jecn.epros.service.ParamUtils;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.home.HomeService;
import com.jecn.epros.util.Params;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 我参与的流程
 *
 * @author admin
 */
@Service
@Transactional
public class HomeServiceImpl implements HomeService {
    @Autowired
    private HomeMapper homeMapper;

    /**
     * 我的主页-》我参与的流程
     *
     * @param maps
     * @return
     */
    @Override
    public List<RoleRelatedFlowInfo> findMyJoinProcess(Map<String, Object> maps) {
        if (maps.get("peopleId") == null || maps.get("projectId") == null) {
            return null;
        }
        Paging paging = (Paging) maps.get(Params.PG);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "T.roleId");
        List<RoleRelatedFlowInfo> roleRelateFlows = homeMapper.findRoleRelateFlowByPostIds(maps);
        if (roleRelateFlows.isEmpty()) {
            return null;
        }
        List<Long> roleIds = new ArrayList<>();
        Set<Long> flowIds = new HashSet<>();
        for (RoleRelatedFlowInfo flowInfo : roleRelateFlows) {
            roleIds.add(flowInfo.getRoleId());
            flowIds.add(flowInfo.getFlowId());
        }

        if (roleIds.isEmpty()) {
            return null;
        }

        // 按岗位分组（岗位对应角色-》流程）
        return getResultByPostGroup(maps, roleRelateFlows, roleIds, flowIds);
    }

    /**
     * 我的主页-》我参与的流程 总条数
     *
     * @param maps
     * @return
     */
    @Override
    public int findMyJoinProcessCount(Map<String, Object> maps) {
        if (maps.get("peopleId") == null || maps.get("projectId") == null) {
            return 0;
        }
        int count = homeMapper.findRoleRelateFlowByPostIdsCount(maps);
        // 按岗位分组（岗位对应角色-》流程）
        return count;
    }

    /**
     * 按岗位分组获取流程相关信息
     *
     * @param maps            传入参数，projectId
     * @param roleRelateFlows
     * @param roleIds
     * @param flowIds
     * @return
     */
    private List<RoleRelatedFlowInfo> getResultByPostGroup(Map<String, Object> maps,
                                                           List<RoleRelatedFlowInfo> roleRelateFlows,
                                                           List<Long> roleIds, Set<Long> flowIds) {
        if (maps == null) {
            return null;
        }
        Long posId = Long.valueOf(maps.get("posId").toString());
        // 角色对应活动
        List<RoleRelatedActivity> listActive = null;
        // 相关制度
        List<JoinProcessRelatedFile> listRules = null;
        // 相关标准
        List<JoinProcessRelatedFile> listStans = null;
        // 相关风险
        List<JoinProcessRelatedFile> listRisks = null;
        if (roleIds.size() > 0) {
            listActive = homeMapper.findActiveByRoleListIds(ParamUtils.collection2Map(ParamUtils.DEFAULT_COLLECTION_KEY, roleIds));
            Map<String, Object> flowMap = ParamUtils.collection2Map(ParamUtils.DEFAULT_COLLECTION_KEY, flowIds);
            listRules = homeMapper.findFlowRelatedRules(flowMap);
            listStans = getRelatedStandards(flowMap);
            listRisks = homeMapper.findFlowRelatedRisks(flowMap);
        }
        // 返回结果集
        List<RoleRelatedFlowInfo> postRoleLists = null;
        // 岗位对应流程相关信息
        postRoleLists = getRoleRelatedFlowInfosByPostId(roleRelateFlows, listActive, listRules, listStans,
                listRisks, posId);
        return postRoleLists;
    }


    /**
     * 按岗位分组获取流程相关信息
     *
     * @param maps 传入参数，projectId
     * @return
     */
    public List<JoinProcess> getPostByUserId(Map<String, Object> maps) {
        if (maps == null) {
            return null;
        }
        // 人员对应岗位集合
        List<Map<String, Object>> postMaps = homeMapper.findPostNameByPeopleId((Long) maps.get("peopleId"));
        // 返回结果集
        List<JoinProcess> listResult = new ArrayList<JoinProcess>();
        JoinProcess joinProcess = null;
        for (Map<String, Object> map : postMaps) {
            if (map.get("FIGURE_ID") == null || map.get("FIGURE_TEXT") == null) {
                continue;
            }
            joinProcess = new JoinProcess();
            joinProcess.setPosId(valueOfLong(map.get("FIGURE_ID")));
            joinProcess.setPosName(map.get("FIGURE_TEXT").toString());
            listResult.add(joinProcess);
        }
        return listResult;
    }

    /**
     * 角色对应活动
     *
     * @param listActive
     * @return
     */
    private List<LinkResource> getActiveLinks(List<RoleRelatedActivity> listActive) {
        if (listActive == null) {
            return null;
        }

        List<LinkResource> activeLinks = new ArrayList<>();
        String number = "";
        for (RoleRelatedActivity active : listActive) {
            if (active.getActivityName() == null) {
                continue;
            }
            if (active.getActivityNum() != null) {
                number = "[" + active.getActivityNum() + "] ";
            }
            activeLinks.add(new LinkResource(active.getActivityFigureId(), number + active.getActivityName(), LinkResource.ResourceType.ACTIVITY));
            number = "";
        }
        return activeLinks;
    }

    private List<LinkResource> getRelatedLinks(List<JoinProcessRelatedFile> addRelatedFiles, LinkResource.ResourceType type) {
        if (addRelatedFiles == null || type == null) {
            return new ArrayList<>();
        }
        List<LinkResource> relatedLikns = new ArrayList<>();
        for (JoinProcessRelatedFile rule : addRelatedFiles) {
            relatedLikns.add(new LinkResource(rule.getId(), rule.getName(), type, TreeNodeUtils.NodeType.main));
        }
        return relatedLikns;
    }

    /**
     * 获取岗位参与的流程
     *
     * @param roleRelFlows
     * @param postId
     * @return
     */
    private List<RoleRelatedFlowInfo> getRoleRelatedFlowInfosByPostId(List<RoleRelatedFlowInfo> roleRelFlows,
                                                                      List<RoleRelatedActivity> listActive,
                                                                      List<JoinProcessRelatedFile> listRules,
                                                                      List<JoinProcessRelatedFile> listStandard,
                                                                      List<JoinProcessRelatedFile> listRisks, Long postId) {
        List<RoleRelatedFlowInfo> postRoleLists = new ArrayList<>();
        for (RoleRelatedFlowInfo flowInfo : roleRelFlows) {
            if (flowInfo.getPostId().toString().equals(postId.toString())) {
                // 添加参与流程的相关活动
                addRelatedActivitys(flowInfo, listActive);
                // 添加参与流程的相关制度
                flowInfo.setRelatedFiles(getAddRelatedFiles(flowInfo.getFlowId(), listRules, LinkResource.ResourceType.RULE));
                // 添加参与流程的相关标准
                flowInfo.getRelatedFiles().addAll(getAddRelatedFiles(flowInfo.getFlowId(), listStandard, LinkResource.ResourceType.STANDARD));
                // 添加参与流程的相关风险
                flowInfo.getRelatedFiles().addAll(getAddRelatedFiles(flowInfo.getFlowId(), listRisks, LinkResource.ResourceType.RISK));
                postRoleLists.add(flowInfo);
            }
        }
        return postRoleLists;
    }

    /**
     * 角色相关活动
     *
     * @param flowInfo
     * @param listActive
     */
    private void addRelatedActivitys(RoleRelatedFlowInfo flowInfo, List<RoleRelatedActivity> listActive) {
        if (listActive == null) {
            return;
        }
        List<RoleRelatedActivity> addListActive = new ArrayList<>();
        for (RoleRelatedActivity relatedActivity : listActive) {
            if (isEqualsObj(flowInfo.getRoleId(), relatedActivity.getRoleId())) {
                addListActive.add(relatedActivity);
            }
        }
        flowInfo.setActivityLikes(getActiveLinks(addListActive));
    }

    /**
     * 流程相关制度
     *
     * @param flowId
     * @param relatedFiles
     * @param type
     */
    private List<LinkResource> getAddRelatedFiles(Long flowId, List<JoinProcessRelatedFile> relatedFiles, LinkResource.ResourceType type) {
        if (relatedFiles == null) {
            return new ArrayList<>();
        }
        List<JoinProcessRelatedFile> addRelatedFiles = new ArrayList<>();
        for (JoinProcessRelatedFile relatedFile : relatedFiles) {
            if (isEqualsObj(flowId, relatedFile.getFlowId())) {
                addRelatedFiles.add(relatedFile);
            }
        }
        return getRelatedLinks(addRelatedFiles, type);
    }

    private boolean isEqualsObj(Object obj1, Object obj2) {
        if (obj1 == null || obj2 == null) {
            return false;
        }
        return obj1.toString().equals(obj2.toString());
    }

    private Long valueOfLong(Object object) {
        return Long.valueOf(object.toString());
    }

    private List<JoinProcessRelatedFile> getRelatedStandards(Map<String, Object> flowMap) {
        // 相关标准
        List<JoinProcessRelatedFile> flowRelatedStandards = homeMapper.findFlowRelatedStandards(flowMap);
        List<JoinProcessRelatedFile> activityRelatedStandards = homeMapper.findActivityRelatedStandards(flowMap);
        if (activityRelatedStandards == null || activityRelatedStandards.size() == 0) {
            return flowRelatedStandards;
        }
        if (flowRelatedStandards == null || flowRelatedStandards.size() == 0) {
            return activityRelatedStandards;
        }
        for (JoinProcessRelatedFile activityStandard : activityRelatedStandards) {// 活动标准
            for (JoinProcessRelatedFile flowStandard : flowRelatedStandards) {// 流程标准
                if (!isEqualsObj(flowStandard.getFlowId(), activityStandard)) {// 包含该标准
                    flowRelatedStandards.add(activityStandard);
                    break;
                }
            }
        }
        return flowRelatedStandards;
    }

    @Override
    public List<TopNumberData> findFlowVisitTopTen(Map<String, Object> map) {
        Paging paging = new Paging();
        paging.setPageNum(1);
        paging.setPageSize(10);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "TOTAL_COUNT");
        return homeMapper.findFlowVisitTopTen(map);
    }

    @Override
    public List<TopNumberData> findFlowProposeTopTen(Map<String, Object> map) {
        Paging paging = new Paging();
        paging.setPageNum(1);
        paging.setPageSize(10);
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "TOTAL_COUNT");
        return homeMapper.findFlowProposeTopTen(map);
    }
}
