package com.jecn.epros.server.service.autoCode.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;
import com.jecn.epros.server.bean.autoCode.AutoCodeRelatedBean;
import com.jecn.epros.server.bean.autoCode.ProcessCodeTree;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.dao.autoCode.IAutoCodeDao;
import com.jecn.epros.server.service.autoCode.AutoCodeUtil;
import com.jecn.epros.server.service.autoCode.IAutoCodeService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

@Transactional(rollbackFor = Exception.class)
public class AutoCodeServiceImpl implements IAutoCodeService {

	private IAutoCodeDao autoCodeDao;

	@Override
	public Long addPorcessCode(ProcessCodeTree codeTree) throws Exception {
		return autoCodeDao.addPorcessCode(codeTree);
	}

	@Override
	public void deleteProcessCode(List<Long> ids) throws Exception {
		autoCodeDao.deleteProcessCode(ids);
	}

	@Override
	public void updateName(Long id, String newName, Long updatePersonId) throws Exception {
		autoCodeDao.updateName(id, newName, updatePersonId);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getPnodes(Long id, int type) throws Exception {
		String path = autoCodeDao.getCodePath(id);
		if (StringUtils.isBlank(path)) {
			return new ArrayList<JecnTreeBean>();
		}
		Set<Long> set = JecnCommon.getPositionTreeIds(path, id);

		List<Object[]> lists = autoCodeDao.getPnodes(set, type);
		if (lists.isEmpty()) {
			return new ArrayList<JecnTreeBean>();
		}
		return getTreeBean(lists);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getChildProcessCodes(Long pId, int type) throws Exception {
		List<Object[]> lists = autoCodeDao.getChildProcessCodes(pId, type);
		if (lists.isEmpty()) {
			return new ArrayList<JecnTreeBean>();
		}

		return getTreeBean(lists);
	}

	private List<JecnTreeBean> getTreeBean(List<Object[]> lists) {
		List<JecnTreeBean> beans = new ArrayList<JecnTreeBean>();
		JecnTreeBean treeBean = null;
		// PC.Id, PC.NAME, PC.PANRENT_ID,PC.IS_DIR,
		// PC.SORT_ID,PC.T_PATH,PC.T_LEVEL,COUNT
		for (Object[] objects : lists) {
			treeBean = new JecnTreeBean();
			treeBean.setId(Long.valueOf(objects[0].toString()));
			treeBean.setName(objects[1].toString());
			treeBean.setPid(Long.valueOf(objects[2].toString()));
			treeBean.setTreeNodeType(TreeNodeType.processCode);
			treeBean.setT_Path(objects[5].toString());
			if (objects[7] != null) {
				treeBean.setChildNode(Integer.valueOf(objects[7].toString()) == 1 ? true : false);
			}
			beans.add(treeBean);
		}
		return beans;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public JecnTreeBean getTreeBeanByName(String name) throws Exception {
		List<Object[]> lists = autoCodeDao.getTreeBeanByName(name);
		if (lists.isEmpty()) {
			return null;
		}
		List<JecnTreeBean> beans = getTreeBean(lists);
		return beans.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public AutoCodeBean getAutoCodeBean(Long relatedId, int relatedType) throws Exception {
		String sql = "SELECT AC.GUID,AC.CODE_TOTAL,AC.CREATE_PERSON_ID, AC.CREATE_TIME,"
				+ " AC.COMPANY_CODE,AC.PROCESS_CODE,AC.PRODUCTION_LINE,AC.FILE_TYPE,AC.SMALL_CLASS,AC.PROCESS_LEVEL"
				+ "  FROM AUTO_CODE_TABLE AC" + " INNER JOIN AUTO_CODE_RELATED_TABLE ACRT"
				+ "    ON ACRT.RELATED_ID = " + relatedId + "   AND ACRT.RELATED_TYPE = " + relatedType
				+ "  AND ACRT.AUTO_CODE_ID = AC.GUID";
		List<AutoCodeBean> list = (List<AutoCodeBean>) this.autoCodeDao.listNativeAddEntity(sql, AutoCodeBean.class);
		if (list.size() == 0) {
			return null;
		}
		// 清除session，否則hibernate会自动执行evict保存
		autoCodeDao.getSession().clear();
		AutoCodeBean autoCodeBean = list.get(0);
		getAutoNum(autoCodeBean, relatedType);

		return autoCodeBean;
	}

	/**
	 * 更新编号 通用 流程架构、制度目录、文件目录
	 * 
	 * @param id
	 * @param codeBean
	 * @param relateType
	 * @param listChild
	 * @param listChildExist
	 * @param listExist
	 * @throws Exception
	 */
	private void autoCodeCommon(Long id, AutoCodeBean codeBean, int relateType, List<Object[]> listChild,
			List<Object[]> listChildExist, List<Object[]> listExist) throws Exception {
		String gzId = "";
		boolean isCreateNewrule = false;
		if (codeBean.getGuid() != null && !"".equals(codeBean.getGuid())) {
			gzId = codeBean.getGuid();
		} else {
			gzId = JecnCommon.getUUID();
			codeBean.setGuid(gzId);
			isCreateNewrule = true;
		}
		boolean isPubExist = false;
		boolean isPubUpdate = false;
		if (listExist.size() > 0) {
			isPubExist = true;
			if (!gzId.equals(listExist.get(0)[1].toString())) {
				isPubUpdate = true;
			}
		}
		this.updateAutoRelated(isPubExist, id, relateType, codeBean, isPubUpdate);

		// 更新流程编号
		updateMengNiuCode(id, relateType, codeBean);

		if (JecnConfigTool.isMengNiuLogin() || relateType == 4 || relateType == 2) {// 文件目录或者 蒙牛规则变更时，流水号保持当前默认，不自增
			codeBean.setCodeTotal(codeBean.getCodeTotal() > 0 ? codeBean.getCodeTotal() - 1 : codeBean.getCodeTotal());
		}
		// 更新主表
		if (isCreateNewrule) {
			codeBean.setCreateTime(new Date());
			autoCodeDao.save(codeBean);
		} else {
			String sql = "update AUTO_CODE_TABLE  set code_total=" + codeBean.getCodeTotal() + " where guid=?";
			autoCodeDao.execteNative(sql, codeBean.getGuid());
		}
	}

	private void updateMengNiuCode(Long relatedId, int relateType, AutoCodeBean codeBean) throws Exception {
		String resultCode = getAutoNum(codeBean, relateType);
		if (relateType == 0) {// 流程架构
			this.updateNumByRelatedType(1, relatedId, resultCode, codeBean.getCreatePersonId());
		} else if (relateType == 2) {// 制度目录
			this.updateNumByRelatedType(3, relatedId, resultCode, codeBean.getCreatePersonId());
		} else if (relateType == 4) {// 文件
			this.updateNumByRelatedType(5, relatedId, resultCode, codeBean.getCreatePersonId());
		}
	}

	private void updateChildCode(String gzId, int relateType, AutoCodeBean codeBean, List<Object[]> listChild,
			List<Object[]> listChildExist, List<Object[]> listExist) throws Exception {
		for (Object[] objs : listChild) {
			Long childId = Long.valueOf(objs[0].toString());
			boolean isExist = false;
			boolean isUpdate = false;
			for (Object[] obj : listChildExist) {
				if (childId.toString().equals(obj[0].toString())) {
					isExist = true;
					if (!gzId.equals(obj[1].toString())) {
						isUpdate = true;
					}
					break;
				}
			}
			if (!isExist || isUpdate) {
				if (relateType == 0) {// 流程架构
					this.updateNumByRelatedType(1, childId, getAutoNum(codeBean, relateType), codeBean
							.getCreatePersonId());
				} else if (relateType == 2) {// 制度目录
					this.updateNumByRelatedType(3, childId, getAutoNum(codeBean, relateType), codeBean
							.getCreatePersonId());
				} else if (relateType == 4) {// 文件
					this.updateNumByRelatedType(5, childId, getAutoNum(codeBean, relateType), codeBean
							.getCreatePersonId());
				}
			}
			if (relateType == 0) {// 流程架构
				this.updateAutoRelated(isExist, childId, 1, codeBean, isUpdate);
			} else if (relateType == 2) {// 制度目录
				this.updateAutoRelated(isExist, childId, 3, codeBean, isUpdate);
			} else if (relateType == 4) {// 文件
				this.updateAutoRelated(isExist, childId, 5, codeBean, isUpdate);
			}
		}
	}

	/**
	 * 更新编号 通用 流程、制度、文件
	 * 
	 * @param id
	 * @param codeBean
	 * @param relateType
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * @param listExist
	 * @throws Exception
	 */
	private void autoCodeCommonSingle(Long id, AutoCodeBean codeBean, int relateType, List<Object[]> listExist)
			throws Exception {
		String gzId = "";
		boolean isCreateNewrule = false;
		if (codeBean.getGuid() != null && !"".equals(codeBean.getGuid())) {
			gzId = codeBean.getGuid();
		} else {
			gzId = JecnCommon.getUUID();
			codeBean.setGuid(gzId);
			isCreateNewrule = true;
		}
		boolean isPubExist = false;
		boolean isPubUpdate = false;
		if (listExist.size() > 0) {
			isPubExist = true;
			if (!gzId.equals(listExist.get(0)[1].toString())) {
				isPubUpdate = true;
			}
		}
		this.updateAutoRelated(isPubExist, id, relateType, codeBean, isPubUpdate);

		if (!isPubExist || isPubUpdate) {
			if (relateType == 1) {// 流程
				this.updateNumByRelatedType(1, id, getAutoNum(codeBean, relateType), codeBean.getCreatePersonId());
			} else if (relateType == 3 || relateType == 2) {// 制度
				this.updateNumByRelatedType(3, id, getAutoNum(codeBean, relateType), codeBean.getCreatePersonId());
			} else if (relateType == 5) {// 文件
				this.updateNumByRelatedType(5, id, getAutoNum(codeBean, relateType), codeBean.getCreatePersonId());
			}
		}
		// 更新主表
		if (isCreateNewrule) {
			codeBean.setCreateTime(new Date());
			autoCodeDao.save(codeBean);
		} else {
			String sql = "update AUTO_CODE_TABLE  set code_total=" + codeBean.getCodeTotal() + " where guid=?";
			autoCodeDao.execteNative(sql, codeBean.getGuid());
		}
	}

	/**
	 * 流程地图
	 * 
	 * @param id
	 * @param codeBean
	 * @throws Exception
	 */
	private void autoCodeFlowMap(Long id, AutoCodeBean codeBean) throws Exception {
		String sql = " select t.flow_id,T.SORT_ID from jecn_flow_structure_t t where t.pre_flow_id=? and t.del_state=0 and t.data_type=0 and t.isflow=1 ORDER BY T.SORT_ID";
		List<Object[]> listChild = autoCodeDao.listNativeSql(sql, id);
		sql = "select t.flow_id,acrt.auto_code_id,acrt.guid from AUTO_CODE_RELATED_TABLE acrt,jecn_flow_structure_t t where"
				+ "     t.pre_flow_id=? and t.del_state=0 and t.data_type=0 and t.isflow=1"
				+ "     and t.flow_id = acrt.related_id and acrt.related_type=1";
		List<Object[]> listExistNum = autoCodeDao.listNativeSql(sql, id);

		sql = "select t.flow_id,acrt.auto_code_id,acrt.guid from AUTO_CODE_RELATED_TABLE acrt,jecn_flow_structure_t t where"
				+ "     t.flow_id=? and t.flow_id = acrt.related_id and acrt.related_type=0";
		List<Object[]> list = autoCodeDao.listNativeSql(sql, id);
		this.autoCodeCommon(id, codeBean, 0, listChild, listExistNum, list);
	}

	/**
	 * 流程
	 * 
	 * @param id
	 * @param codeBean
	 * @throws Exception
	 */
	private void autoCodeFlow(Long id, AutoCodeBean codeBean) throws Exception {
		String sql = "select t.flow_id,acrt.auto_code_id,acrt.guid from AUTO_CODE_RELATED_TABLE acrt,jecn_flow_structure_t t where"
				+ "     t.flow_id=? and t.flow_id = acrt.related_id and acrt.related_type=1";
		List<Object[]> listExist = autoCodeDao.listNativeSql(sql, id);
		this.autoCodeCommonSingle(id, codeBean, 1, listExist);
	}

	/**
	 * 制度目录
	 * 
	 * @param id
	 * @param codeBean
	 * @throws Exception
	 */
	private void autoCodeRuleDir(Long id, AutoCodeBean codeBean) throws Exception {
		String sql = "  select t.id,t.sort_id from jecn_rule_t t where t.per_id=? and (t.is_dir=1 or (t.is_dir=2 and t.is_file_local=1)) order by t.sort_id";
		List<Object[]> listChild = autoCodeDao.listNativeSql(sql, id);

		sql = "select t.id,acrt.auto_code_id,acrt.guid from AUTO_CODE_RELATED_TABLE acrt,jecn_rule_t t where"
				+ "              t.per_id=? and (t.is_dir=1 or (t.is_dir=2 and t.is_file_local=1))"
				+ "              and t.id = acrt.related_id and acrt.related_type=3";
		List<Object[]> listExistNum = autoCodeDao.listNativeSql(sql, id);

		sql = "select t.id,acrt.auto_code_id,acrt.guid from AUTO_CODE_RELATED_TABLE acrt,jecn_rule_t t where"
				+ "     t.id=? and t.id = acrt.related_id and acrt.related_type=2";
		List<Object[]> list = autoCodeDao.listNativeSql(sql, id);
		this.autoCodeCommon(id, codeBean, 2, listChild, listExistNum, list);
	}

	/**
	 * 制度
	 * 
	 * @param id
	 * @param codeBean
	 * @throws Exception
	 */
	private void autoCodeRuleFile(Long id, AutoCodeBean codeBean) throws Exception {
		String sql = "select t.id,acrt.auto_code_id,acrt.guid from AUTO_CODE_RELATED_TABLE acrt,jecn_rule_t t where"
				+ "     t.id=? and t.id = acrt.related_id and acrt.related_type=3";
		List<Object[]> listExist = autoCodeDao.listNativeSql(sql, id);
		this.autoCodeCommonSingle(id, codeBean, 3, listExist);
	}

	/**
	 * 制度目录
	 * 
	 * @param id
	 * @param codeBean
	 * @throws Exception
	 */
	private void autoCodeFileDir(Long id, AutoCodeBean codeBean) throws Exception {
		String sql = " select t.file_id,t.sort_id from jecn_file_t t where t.per_file_id=? and t.del_state=0 order by t.sort_id";
		List<Object[]> listChild = autoCodeDao.listNativeSql(sql, id);

		sql = "select t.file_id,acrt.auto_code_id,acrt.guid from AUTO_CODE_RELATED_TABLE acrt,jecn_file_t t where"
				+ "                       t.per_file_id=? and t.del_state=0"
				+ "                       and t.file_id = acrt.related_id and acrt.related_type=5";
		List<Object[]> listExistNum = autoCodeDao.listNativeSql(sql, id);
		sql = "select t.file_id,acrt.auto_code_id,acrt.guid from AUTO_CODE_RELATED_TABLE acrt,jecn_file_t t where"
				+ "     t.file_id=? and t.file_id = acrt.related_id and acrt.related_type=4";
		List<Object[]> list = autoCodeDao.listNativeSql(sql, id);
		this.autoCodeCommon(id, codeBean, 4, listChild, listExistNum, list);
	}

	/**
	 * 文件
	 * 
	 * @param id
	 * @param codeBean
	 * @throws Exception
	 */
	private void autoCodeFile(Long id, AutoCodeBean codeBean) throws Exception {
		String sql = "select t.file_id,acrt.auto_code_id,acrt.guid from AUTO_CODE_RELATED_TABLE acrt,jecn_file_t t where"
				+ "     t.file_id=? and t.file_id = acrt.related_id and acrt.related_type=5";
		List<Object[]> listExist = autoCodeDao.listNativeSql(sql, id);
		this.autoCodeCommonSingle(id, codeBean, 5, listExist);
	}

	/**
	 * 更新自动编号的规则
	 * 
	 * @param isExist
	 * @param relateId
	 * @param relateType
	 * @param codeBean
	 * @param isUpdate
	 */
	private void updateAutoRelated(boolean isExist, Long relateId, int relateType, AutoCodeBean codeBean,
			boolean isUpdate) {
		if (!isExist) {
			String sql = "insert into auto_code_related_table (guid,auto_code_id,related_type,related_id,create_person_id,create_time) values(?,?,?,?,?,?)";
			autoCodeDao.execteNative(sql, JecnCommon.getUUID(), codeBean.getGuid(), relateType, relateId, codeBean
					.getCreatePersonId(), new Date());
		} else {
			if (isUpdate) {
				String sql = "update auto_code_related_table set auto_code_id=?,create_person_id=?,create_time=? where related_type="
						+ relateType + " and related_id=" + relateId;
				autoCodeDao.execteNative(sql, codeBean.getGuid(), codeBean.getCreatePersonId(), new Date());
			}
		}
	}

	/**
	 * 获得自动编号
	 * 
	 * @param codeBean
	 * @return
	 */
	private String getAutoNum(AutoCodeBean codeBean, int relateType) {
		codeBean.setCodeTotal(codeBean.getCodeTotal() + 1);
		String resultCode = "";
		if (28 == JecnContants.otherLoginType) {// 立邦自动编号 编号规则
			resultCode = AutoCodeUtil.getLibangAutoNum(codeBean, relateType);
		} else if (37 == JecnContants.otherLoginType) {// 蒙牛自动编号编号规则
			resultCode = AutoCodeUtil.getMengNiuAutoNum(codeBean);
		} else if (41 == JecnContants.otherLoginType) {// 木林森
			resultCode = AutoCodeUtil.getMulinsenAutoNum(codeBean, relateType);
		}
		codeBean.setResultCode(resultCode);
		return resultCode;
	}

	/**
	 * 自动编号保存
	 * 
	 * @param codeBean
	 * @param relatedType
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * @throws Exception
	 */
	@Override
	public AutoCodeBean saveAutoCode(AutoCodeBean codeBean, Long id, int relatedType) throws HibernateException,
			Exception {
		String sql = "";
		List<Object[]> list = null;
		sql = "select t.guid,t.code_total" + " from auto_code_table t where 1=1";
		if (StringUtils.isNotBlank(codeBean.getCompanyCode())) {
			sql += " and t.company_code='" + codeBean.getCompanyCode() + "'";
		} else {
			sql += " and (t.company_code IS NULL or t.company_code ='')";
		}
		if (StringUtils.isNotBlank(codeBean.getFileType())) {
			sql += " and t.file_type='" + codeBean.getFileType() + "'";
		} else {
			sql += " and (t.file_type IS NULL or t.file_type= '')";
		}
		if (StringUtils.isNotBlank(codeBean.getProductionLine())) {
			sql += " and t.production_line='" + codeBean.getProductionLine() + "'";
		} else {
			sql += " and (t.production_line IS NULL or t.production_line = '')";
		}
		if (StringUtils.isNotBlank(codeBean.getProcessCode())) {
			sql += " and t.process_code='" + codeBean.getProcessCode() + "'";
		} else {
			sql += " and (t.process_code IS NULL or t.process_code ='')";
		}
		if (StringUtils.isNotBlank(codeBean.getProcessLevel())) {
			sql += " and t.process_level='" + codeBean.getProcessLevel() + "'";
		} else {
			sql += " and (t.process_level IS NULL or t.process_level = '')";
		}
		if (StringUtils.isNotBlank(codeBean.getSmallClass())) {
			sql += " and t.small_class='" + codeBean.getSmallClass() + "'";
		} else {
			sql += " and (t.small_class IS NULL or t.small_class ='')";
		}
		list = autoCodeDao.listNativeSql(sql);

		if (list.size() > 0) {
			Object[] obj = list.get(0);
			codeBean.setGuid(obj[0].toString());
			codeBean.setCodeTotal(Integer.parseInt(obj[1].toString()));
		}

		switch (relatedType) {
		case 0:
			this.autoCodeFlowMap(id, codeBean);
			break;
		case 1:
			this.autoCodeFlow(id, codeBean);
			break;
		case 2:
			this.autoCodeRuleDir(id, codeBean);
			break;
		case 3:
			this.autoCodeRuleFile(id, codeBean);
			break;
		case 4:
			this.autoCodeFileDir(id, codeBean);
			break;
		case 5:
			this.autoCodeFile(id, codeBean);
			break;
		}
		return codeBean;
	}

	@SuppressWarnings("unchecked")
	@Override
	public AutoCodeRelatedBean getAutoCodeRelatedBean(Long relatedId, int relatedType) throws Exception {
		String sql = "SELECT GUID,CODE_NUM,AUTO_CODE_ID,RELATED_TYPE,RELATED_ID,CREATE_PERSON_ID,CREATE_TIME FROM AUTO_CODE_RELATED_TABLE WHERE RELATED_ID ="
				+ relatedId + " AND RELATED_TYPE = " + relatedType;
		List<AutoCodeRelatedBean> list = (List<AutoCodeRelatedBean>) this.autoCodeDao.listNativeAddEntity(sql,
				AutoCodeRelatedBean.class);
		if (list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	/**
	 * 更新节点编号
	 * 
	 * @param relatedType
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板、4是制度文件（本地上传）、5是文件目录、6是文件
	 * @throws Exception
	 */
	private void updateNumByRelatedType(int relatedType, Long relatedId, String code, Long updatePeopleId)
			throws Exception {
		String sql = "";
		String sqlT = "";
		int type = 0;
		if (relatedType == 1) {
			sqlT = "UPDATE JECN_FLOW_STRUCTURE_T SET FLOW_ID_INPUT = ? WHERE FLOW_ID = ?";
			sql = "UPDATE JECN_FLOW_STRUCTURE SET FLOW_ID_INPUT = ? WHERE FLOW_ID = ?";
			type = 1;
		} else if (relatedType == 3) {
			sqlT = "UPDATE JECN_RULE_T SET RULE_NUMBER = ? WHERE ID = ?";
			sql = "UPDATE JECN_RULE SET RULE_NUMBER = ? WHERE ID = ?";
			type = 4;
		} else if (relatedType == 5) {
			sqlT = "UPDATE JECN_FILE_T SET DOC_ID = ? WHERE FILE_ID = ?";
			sql = "UPDATE JECN_FILE SET DOC_ID = ? WHERE FILE_ID = ?";
			type = 2;
		}
		if (StringUtils.isBlank(sql)) {
			return;
		}
		if (JecnConfigTool.isAllowedPubReName()) {// 重命名是否直接发布，不直接发布，编号不更新
			this.autoCodeDao.execteNative(sql, code, relatedId);
		}
		this.autoCodeDao.execteNative(sqlT, code, relatedId);
		// 记录日志: 1是流程，2是文件，3是角色,4是制度,5是标准
		JecnUtil.saveJecnJournal(relatedId, "", type, 10, updatePeopleId, this.autoCodeDao);
	}

	/**
	 * 根据文件类型、ID 更新对应编号规则下的最大流水号
	 * 
	 * @param relatedId
	 * @param relatedType
	 * @param codeTotal
	 * @throws Exception
	 */
	@Override
	public void updateAutoCodeTableByRelatedId(Long relatedId, int relatedType, int codeTotal) throws Exception {
		this.autoCodeDao.updateAutoCodeTableByRelatedId(relatedId, relatedType, codeTotal);
	}

	public IAutoCodeDao getAutoCodeDao() {
		return autoCodeDao;
	}

	public void setAutoCodeDao(IAutoCodeDao autoCodeDao) {
		this.autoCodeDao = autoCodeDao;
	}
}
