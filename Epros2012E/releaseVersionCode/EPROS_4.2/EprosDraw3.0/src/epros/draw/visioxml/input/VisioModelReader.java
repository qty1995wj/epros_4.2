package epros.draw.visioxml.input;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;

import visioxml.bean.ConnectProperty;
import visioxml.constant.Constants.LineRoute;
import visioxml.util.FileUtil;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.util.DrawCommon;

public class VisioModelReader {
	private static final Log log = LogFactory.getLog(VisioModelReader.class);
	/** visio导入关系配置 */
	private Map<String, String> configMap = null;
	/** visio图形和epros图形的关系集合 key=visioFigueID,value=eprosFigueID */
	private Map<String, String> relationMap = null;
	/** 解析帮助类 **/
	private VisioModelReaderHelper help = null;
	/*** epros 中间数据 **/
	private EPROSMiddleData data = null;

	private Document doc = null;;

	public VisioModelReader(String allFullPath) {
		doc = FileUtil.getDocumentFile(allFullPath);
		data = new EPROSMiddleData();
		configMap = FileUtil.getProcessInPutVisioModeConfig();
		help = new VisioModelReaderHelper(doc);

	}

	@SuppressWarnings("unchecked")
	public EPROSMiddleData read() {
		Element page = doc.getRootElement().element("Pages").element("Page");
		List<Element> shapes = page.element("Shapes").elements();
		if (shapes.size() == 0 || shapes == null) {
			return null;
		}
		// 面板高度
		double panelHeight = 0;
		Element pageProps = page.element("PageSheet").element("PageProps");
		panelHeight = VisioInputTools.feetToPx(pageProps.elementText("PageHeight"));
		// page
		conversionVisioPage(pageProps);
		// 横线竖线 分割线
		convertVisioVHLine(shapes, panelHeight);
		// 横向 竖向 分割线
		convertVisioVHDiviLine(shapes, panelHeight);
		// 图形
		convertVisioFigure(shapes, panelHeight);
		// 动态连接线
		if (page.element("Connects") != null) {
			List<Element> listConnect = page.element("Connects").elements();
			if (listConnect != null) {
				conversionVisioDynLine(shapes, listConnect, panelHeight);
			}
		}
		return data;
	}

	/**
	 * 解析page 面板数据
	 * 
	 * @param doc
	 */
	private void conversionVisioPage(Element pageProps) {
		// 面板高度
		double panelHeight = 0;
		// 面板宽度
		double panelWidth = 0;
		panelHeight = VisioInputTools.feetToPx(pageProps.elementText("PageHeight"));
		panelWidth = VisioInputTools.feetToPx(pageProps.elementText("PageWidth"));
		data.setPanelHeight(panelHeight);
		data.setPanelWidth(panelWidth);
	}

	/**
	 * 解析所有的连接线
	 * 
	 * @param shapes
	 * @param listConnect
	 * @param panelHeight
	 * @return
	 */
	private void conversionVisioDynLine(List<Element> shapes, List<Element> listConnect, double panelHeight) {
		List<JecnManhattanLineData> manLines = new ArrayList<JecnManhattanLineData>();
		// 存储连接线连接属性
		Map<String, ConnectProperty> beginX = new HashMap<String, ConnectProperty>();
		Map<String, ConnectProperty> endX = new HashMap<String, ConnectProperty>();
		// 初始化连接线属性
		help.initConnects(listConnect, beginX, endX);
		int size = shapes.size();
		for (int zIndex = 0; zIndex < size; zIndex++) {
			Element ele = shapes.get(zIndex);
			String nameU = getNameU(ele.attributeValue("Master"));
			// 动态连接线
			if (!VisioModelReaderHelper.EPROS_DYN_LINE.equals(nameU)) {
				continue;
			}
			// 图形的标识
			String figureType = getFigureType(nameU);
			int dynId = Integer.valueOf(ele.attributeValue("ID"));
			// 获取连接线的数据对象
			JecnManhattanLineData manhattanLineData = new JecnManhattanLineData(MapElemType.valueOf(figureType));
			// 设置图形层级
			manhattanLineData.setZOrderIndex(zIndex);

			// 获取 起点和终点的数据
			// 获取连接的图形的数据（4个点的位置（上，下，左，右））
			// 起点和起始图形的四个点进行匹配 相等的就是连接点
			ConnectProperty beg = beginX.get(dynId + "");
			ConnectProperty end = endX.get(dynId + "");
			// 设置连接线的开始连接图形
			if (beg == null || end == null) {
				continue;
			}
			manhattanLineData.setStartFigureUUID(getFigureUUID(beg.getToSheet()));
			// 设置连接线的结束连接图形
			manhattanLineData.setEndFigureUUID(getFigureUUID(end.getToSheet()));

			// 此处特殊处理visio直接导入的流程图连接线取交点
			if (beg.getLineRoute() == null || end.getLineRoute() == null) {
				Element xfrom1D = ele.element("XForm1D");
				String begx = xfrom1D.elementTextTrim("BeginX");
				String begY = xfrom1D.elementTextTrim("BeginY");
				String endx = xfrom1D.elementTextTrim("EndX");
				String endY = xfrom1D.elementTextTrim("EndY");
				if (beg.getLineRoute() == null) {
					Element shapeEle = VisioModelReaderHelper.mapShape.get(beg.getToSheet() + "");
					LineRoute lineRoute = getlineRoute(begx, begY, shapeEle.element("XForm"));
					beg.setLineRoute(lineRoute);
				}
				if (end.getLineRoute() == null) {
					Element shapeEle = VisioModelReaderHelper.mapShape.get(end.getToSheet() + "");
					LineRoute lineRoute = getlineRoute(endx, endY, shapeEle.element("XForm"));
					end.setLineRoute(lineRoute);
				}
				if (beg.getLineRoute() == null || end.getLineRoute() == null) {
					continue;
				}
			}
			// 设置连接线开始连接点
			manhattanLineData.setStartEditPointType(VisioInputTools.converLineRoute(beg.getLineRoute()));
			// 设置连接点结束连接点
			manhattanLineData.setEndEditPointType(VisioInputTools.converLineRoute(end.getLineRoute()));
			// 设置图形通用属性
			help.setGeneralFiguleAttr(manhattanLineData, ele, zIndex);
			// 设置图形文本
			help.setJecnFiguleText(manhattanLineData, ele, figureType);
			// 添加到连接线集合
			manLines.add(manhattanLineData);
		}
		data.setManLines(manLines);
	}

	/**
	 * 
	 * @param xfrom1D
	 * @param xfrom
	 * @return
	 */
	private LineRoute getlineRoute(String strX, String strY, Element xfrom) {
		double x = Double.valueOf(strX);
		double y = Double.valueOf(strY);
		double pinX = Double.valueOf(xfrom.elementTextTrim("PinX"));
		double pinY = Double.valueOf(xfrom.elementTextTrim("PinY"));

		double width = Double.valueOf(xfrom.elementTextTrim("Width"));
		double height = Double.valueOf(xfrom.elementTextTrim("Height"));
		// top x=pinx,pinY+(hright/2)
		// bottom x=pinx,pinY-(hright/2)；
		// left x=pinX-(width/2),y=pinY;
		// right x=pinX+(width/2),y=pinY
		double topX = pinX;
		double topY = pinY + (height / 2);
		double bottomX = pinX;
		double bottomY = pinY - (height / 2);
		double leftX = pinX - (width / 2);
		double leftY = pinY;
		double rightX = pinX + (width / 2);
		double rightY = pinY;
		DecimalFormat df = new DecimalFormat("######0.00");
		x = Double.valueOf(df.format(x));
		y = Double.valueOf(df.format(y));
		pinX = Double.valueOf(df.format(pinX));
		pinY = Double.valueOf(df.format(pinY));
		topX = Double.valueOf(df.format(topX));
		topY = Double.valueOf(df.format(topY));
		leftX = Double.valueOf(df.format(leftX));
		leftY = Double.valueOf(df.format(leftY));
		rightX = Double.valueOf(df.format(rightX));
		rightY = Double.valueOf(df.format(rightY));
		bottomX = Double.valueOf(df.format(bottomX));
		bottomY = Double.valueOf(df.format(bottomY));

		// log.error("x==" + x + ":" + y);
		// log.error("pin==" + pinX + ":" + pinY);
		// log.error("top==" + topX + ":" + topY);
		// log.error("bottom==" + bottomX + ":" + bottomY);
		// log.error("left==" + leftX + ":" + leftY);
		// log.error("right==" + rightX + ":" + rightY);
		if (x == topX && y == topY) {
			return LineRoute.TOP;
		} else if (x == bottomX && y == bottomY) {
			return LineRoute.BOTTOM;
		} else if (x == leftX && y == leftY) {
			return LineRoute.LEFT;
		} else if (x == rightX && y == rightY) {
			return LineRoute.RIGHT;
		}
		return null;
	}

	/**
	 * 解析横向竖向 分割线
	 * 
	 * @param shapes
	 * @param panelHeight
	 * @return
	 */
	private void convertVisioVHDiviLine(List<Element> shapes, double panelHeight) {
		List<JecnVHLineData> vhDiviLineList = new ArrayList<JecnVHLineData>();
		int size = shapes.size();
		JecnVHLineData vhLineData = null;
		for (int zIndex = 0; zIndex < size; zIndex++) {
			Element ele = shapes.get(zIndex);
			if (ele.attributeValue("Master") == null) {
				continue;
			}
			String nameU = getNameU(ele.attributeValue("Master"));
			nameU = nameU.replaceAll("\\.\\d+", "").trim();
			// 阶段分隔符号
			if (VisioModelReaderHelper.EPROS_VISIO_PHASE_LIST.equals(nameU)) {
				nameU = "EPROS_LINE_VERTICALLINE";
				vhLineData = new JecnVHLineData(MapElemType.valueOf(getFigureType(nameU)), LineDirectionEnum.vertical);
				Element xfrom = ele.element("XForm");
				double figureHeight = Double.valueOf(xfrom.elementText("Height"));
				int x = (int) VisioInputTools.changeX(xfrom.elementText("PinX"), figureHeight);
				vhLineData.setVhX(x + VisioModelReaderHelper.pinX);
				vhLineData.setVhY(0);
				vhDiviLineList.add(vhLineData);
				continue;
			}
			// 泳池 取X数据当作横分割线
			if (VisioModelReaderHelper.EPROS_VISIO_SWIMLANE.equals(nameU)) {
				nameU = "EPROS_LINE_PARALLELLINES";
				vhLineData = new JecnVHLineData(MapElemType.valueOf(getFigureType(nameU)), LineDirectionEnum.horizontal);
				Element xfrom = ele.element("XForm");
				double figureHeight = Double.valueOf(xfrom.elementText("Height"));
				int y = (int) VisioInputTools.changeY(xfrom.elementText("PinY"), panelHeight, figureHeight);
				vhLineData.setVhX(0);
				vhLineData.setVhY(y + 60);
				vhDiviLineList.add(vhLineData);
				continue;
			}
			// 横分割线、竖分割线
			if (!VisioModelReaderHelper.EPROS_DVICE_LINES.contains(nameU)) {
				continue;
			}
			String figureType = getFigureType(nameU);
			Element xform1D = ele.element("XForm1D");
			int begX = (int) VisioInputTools.changeX1D(xform1D.elementText("BeginX"));
			int begY = (int) VisioInputTools.changeY1D(xform1D.elementText("BeginY"), panelHeight);
			int endX = (int) VisioInputTools.changeX1D(xform1D.elementText("EndX"));
			int endY = (int) VisioInputTools.changeY1D(xform1D.elementText("EndY"), panelHeight);

			LineDirectionEnum lineDirectionEnum = null;
			if (begX == endX) {
				lineDirectionEnum = LineDirectionEnum.vertical;
			} else if (begY == endY) {
				lineDirectionEnum = LineDirectionEnum.horizontal;
			}
			vhLineData = new JecnVHLineData(MapElemType.valueOf(figureType), lineDirectionEnum);
			vhLineData.setZOrderIndex(zIndex);
			if (LineDirectionEnum.horizontal == lineDirectionEnum) {
				vhLineData.setVhX(0);
				vhLineData.setVhY(begY);
			} else if (LineDirectionEnum.vertical == lineDirectionEnum) {
				vhLineData.setVhX(begX);
				vhLineData.setVhY(0);
			}
			vhDiviLineList.add(vhLineData);
		}
		data.setVHLines(vhDiviLineList);
	}

	/**
	 * 解析Visio中的图形
	 * 
	 * @param shapes
	 * @param panelHeight
	 * @return
	 */
	private void convertVisioFigure(List<Element> shapes, double panelHeight) {
		List<JecnFigureData> figureDataLists = new ArrayList<JecnFigureData>();
		VisioModelReaderHelper.SWIMLANE = false;
		int size = shapes.size();
		for (int zIndex = 0; zIndex < size; zIndex++) {
			Element ele = shapes.get(zIndex);
			if (ele.attributeValue("Master") == null) {
				continue;
			}
			String nameU = getNameU(ele.attributeValue("Master"));
			// visio图形ID
			String visioId = ele.attributeValue("ID");
			// epros图形ID
			String flowElementUUID = DrawCommon.getUUID();
			nameU = nameU.replaceAll("\\.\\d+", "").trim();
			// 过滤掉动态连接线、横线、竖线、分割线、横分割线、竖分割线
			if (VisioModelReaderHelper.EPROS_DYN_LINE.equals(nameU)
					|| VisioModelReaderHelper.EPROS_LINE.contains(nameU)
					|| VisioModelReaderHelper.EPROS_DVICE_LINES.contains(nameU)
					// 阶段列表
					|| VisioModelReaderHelper.EPROS_VISIO_PHASE_LIST.equals(nameU)) {
				continue;
			}
			VisioModelReaderHelper.mapShape.put(visioId, ele);
			// 泳池对象解析为角色
			if (VisioModelReaderHelper.EPROS_VISIO_SWIMLANE.equals(nameU)) {
				VisioModelReaderHelper.SWIMLANE = true;
				nameU = "JECN_METRIC";
			}
			// cff 容器 解析为 名称文本框
			if (VisioModelReaderHelper.EPROS_VISIO_CFF_Container.equals(nameU)) {
				nameU = "JECN_MAPNAME_TEXT";
			}
			// 获取figuretype
			String figureType = getFigureType(nameU);
			if (figureType == null) {
				log.error("当前图形figureType IS null,nameU=" + nameU);
				continue;
			}
			// 创建图形数据对象
			JecnFigureData figureData = JecnCreateFlowElement.createFigureData(MapElemType.valueOf(figureType));
			if (figureData == null) {
				log.error("figureData== null,figureType=" + figureType);
				continue;
			}
			// 设置图形的UUID
			figureData.setFlowElementUUID(flowElementUUID);
			// 添加到关系集合
			putRelationMap(visioId, flowElementUUID);
			// 设置图形通用属性
			help.setGeneralFiguleAttr(figureData, ele, zIndex);
			// 设置图形文本
			help.setJecnFiguleText(figureData, ele, figureType);
			// 1D图形
			if (ele.element("XForm1D") != null) {
				// 设置特殊图形1D图形位置点
				help.setSpecialFiguleLocation(figureData, ele, figureType, panelHeight);
			} else {
				// 设置普通图形位置点
				help.setGeneralFiguleLocation(figureData, ele, panelHeight);
				// 存在泳道 所有图形偏移
				if (VisioModelReaderHelper.SWIMLANE) {
					double pinX = figureData.getFigureDataCloneable().getLeftUpXY().getX();
					if (MapElemType.RoleFigure == figureData.getMapElemType()) {
						pinX = pinX + 15;
					} else {
						pinX = pinX + VisioModelReaderHelper.pinX;
					}
					double pinY = figureData.getFigureDataCloneable().getLeftUpXY().getY();
					figureData.getFigureDataCloneable().getLeftUpXY().setXY(pinX, pinY);
				}
			}
			// 添加图形到集合
			figureDataLists.add(figureData);
		}
		data.setFigureDataLists(figureDataLists);
	}

	/**
	 * 添加到关系集合key=visioFigueID,value=eprosFigueID
	 * 
	 * @param visioFigueID
	 * @param eprosFigueID
	 */
	private void putRelationMap(String visioFigueID, String eprosFigueID) {
		if (relationMap == null) {
			relationMap = new HashMap<String, String>();
		}
		relationMap.put(visioFigueID, eprosFigueID);
	}

	/**
	 * 根据NameU获取FigureType
	 * 
	 * @param nameU
	 * @return
	 */
	private String getFigureType(String nameU) {
		return configMap.get(nameU);
	}

	/**
	 * 获取epros图形ID
	 * 
	 * @param visioFigueID
	 * @return
	 */
	private String getFigureUUID(int visioFigueID) {
		return relationMap.get(String.valueOf(visioFigueID));
	}

	/**
	 * 获取NameU 根据masterID
	 * 
	 * @param masterId
	 * @return
	 */
	private String getNameU(String masterId) {
		return help.getMasterMap().get(String.valueOf(masterId));
	}

	/**
	 * 解析isio中横线、竖线、分割线
	 * 
	 * @param shapes
	 * @param panelHeight
	 * @return
	 */
	private void convertVisioVHLine(List<Element> shapes, double panelHeight) {
		List<JecnBaseDivedingLineData> vhLines = new ArrayList<JecnBaseDivedingLineData>();
		int size = shapes.size();
		for (int zIndex = 0; zIndex < size; zIndex++) {
			Element ele = shapes.get(zIndex);
			String nameU = getNameU(ele.attributeValue("Master"));
			String figureType = getFigureType(nameU);
			if (!VisioModelReaderHelper.EPROS_LINE.contains(nameU)) {
				continue;
			}
			JecnBaseDivedingLineData divedingLineData = new JecnBaseDivedingLineData(MapElemType.valueOf(figureType));
			Element xform1D = ele.element("XForm1D");
			double begX = VisioInputTools.changeX1D(xform1D.elementText("BeginX"));
			double begY = VisioInputTools.changeY1D(xform1D.elementText("BeginY"), panelHeight);
			double endX = VisioInputTools.changeX1D(xform1D.elementText("EndX"));
			double endY = VisioInputTools.changeY1D(xform1D.elementText("EndY"), panelHeight);
			help.setGeneralFiguleAttr(divedingLineData, ele, zIndex);
			divedingLineData.getDiviLineCloneable().getStartXY().setXY(begX, begY);
			divedingLineData.getDiviLineCloneable().getEndXY().setXY(endX, endY);
			divedingLineData.setZOrderIndex(zIndex);
			vhLines.add(divedingLineData);
		}
		data.setDivedingLines(vhLines);
	}
}
