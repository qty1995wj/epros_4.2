package com.jecn.epros.server.util;

public class IntegerType {
	public int count = 1;// 总共级别
	public int countInt = 0;// 处于第几级别

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCountInt() {
		return countInt;
	}

	public void setCountInt(int countInt) {
		this.countInt = countInt;
	}
}
