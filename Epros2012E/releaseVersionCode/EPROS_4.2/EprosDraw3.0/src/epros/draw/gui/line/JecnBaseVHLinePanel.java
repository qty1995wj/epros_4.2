package epros.draw.gui.line;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.line.resize.JecnVHLineResizePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 横竖分割线父类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnBaseVHLinePanel extends JecnBaseLinePanel {
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel1 = null;
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel2 = null;
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel3 = null;
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel4 = null;
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel5 = null;
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel6 = null;
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel7 = null;
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel8 = null;
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel9 = null;
	/** 横竖分割线选中点 */
	private JecnVHLineResizePanel vHLineResizePanel10 = null;
	/** 分割线是否选中 */
	protected boolean showResizeHandle = false;
	/** 记录鼠标点击是鼠标的横坐标或纵坐标 */
	protected int oldXY = 0;

	public JecnBaseVHLinePanel(JecnVHLineData flowElementData) {
		super(flowElementData);
		initComponents();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void initComponents() {
		// 无边框
		this.setBorder(null);
		// 透明
		this.setOpaque(false);

		// 创建分割线选中点对象
		vHLineResizePanel1 = JecnVHLineResizePanel.createVHLineResizePanel();
		vHLineResizePanel2 = JecnVHLineResizePanel.createVHLineResizePanel();
		vHLineResizePanel3 = JecnVHLineResizePanel.createVHLineResizePanel();
		vHLineResizePanel4 = JecnVHLineResizePanel.createVHLineResizePanel();
		vHLineResizePanel5 = JecnVHLineResizePanel.createVHLineResizePanel();
		vHLineResizePanel6 = JecnVHLineResizePanel.createVHLineResizePanel();
		vHLineResizePanel7 = JecnVHLineResizePanel.createVHLineResizePanel();
		vHLineResizePanel8 = JecnVHLineResizePanel.createVHLineResizePanel();
		vHLineResizePanel9 = JecnVHLineResizePanel.createVHLineResizePanel();
		vHLineResizePanel10 = JecnVHLineResizePanel.createVHLineResizePanel();
	}

	/**
	 * 
	 * 设置位置点和大小
	 * 
	 */
	public void setVHLineBounds() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (desktopPane == null) {
			return;
		}
		switch (this.getLineDirectionEnum()) {
		case horizontal:// 水平
			this.setBounds(0, this.getFlowElementData().getHYInt(), desktopPane
					.getFlowMapData().getWorkflowWidth(), this
					.getVHLinePanelSize());
			break;
		case vertical:// 垂直
			this.setBounds(this.getFlowElementData().getVXInt(), 0, this
					.getVHLinePanelSize(), desktopPane.getFlowMapData()
					.getWorkflowHeight());
			break;
		}

		if (this.isShowResizeHandle()) {// 是否显示连接线显示点
			this.showParallelLinesResizeHandles();
		}
	}

	/**
	 * 
	 * 获取横竖分割线的宽或高
	 * 
	 * 目的能全部显示画的线条
	 * 
	 * @return int 横竖分割线的宽或高
	 */
	public int getVHLinePanelSize() {
		if (this.getFlowElementData().getBodyStyle() == 3) {// 双线
			return this.getDoubleLineSpace() + 2
					* this.getFlowElementData().getBodyWidth();
		} else {// 非双线
			return this.getFlowElementData().getBodyWidth();
		}
	}

	/**
	 * 
	 * 获取分割线getFlowElementData
	 * 
	 * @return LineDirectionEnum
	 */
	public LineDirectionEnum getLineDirectionEnum() {
		return this.getFlowElementData().getLineDirectionEnum();
	}

	public void paintComponent(Graphics g) {

		// 定义画笔
		Graphics2D g2d = (Graphics2D) g;
		// 备份
		Stroke oldStroke = g2d.getStroke();

		// 呈现和图像处理服务管理对象
		g2d.setRenderingHints(JecnUIUtil.getRenderingHints());
		// 线条颜色
		g2d.setColor(this.getFlowElementData().getBodyColor());

		// 线条样式
		BasicStroke lineBasicStroke = this.getFlowElementData()
				.getBodyBasicStroke(this.getFlowElementData().getBodyStyle(),
						this.getFlowElementData().getBodyWidth());
		g2d.setStroke(lineBasicStroke);

		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (desktopPane == null) {
			return;
		}
		int width = desktopPane.getFlowMapData().getWorkflowWidth();
		int height = desktopPane.getFlowMapData().getWorkflowHeight();
		// 画单线
		switch (getLineDirectionEnum()) {
		case horizontal:// 水平
			g2d.drawLine(0, 0, width, 0);
			break;
		case vertical:// 垂直
			g2d.drawLine(0, 0, 0, height);
			break;
		}

		// 双线条
		if (this.getFlowElementData().getBodyStyle() == 3) {
			// 双线间隔
			int line = this.getDoubleLineSpace()
					+ this.getFlowElementData().getBodyWidth();

			switch (getLineDirectionEnum()) {
			case horizontal:// 水平
				g2d.drawLine(0, line, width, line);
				break;
			case vertical:// 垂直
				g2d.drawLine(line, 0, line, height);
				break;
			}
		}

		// 还原备份
		g2d.setStroke(oldStroke);
	}

	public JecnVHLineData getFlowElementData() {
		return (JecnVHLineData) flowElementData;
	}

	@Override
	public JecnBaseFlowElementPanel clone() {
		return newInstance();
	}

	/**
	 * 实例化克隆对象
	 * 
	 */
	public JecnBaseVHLinePanel newInstance() {
		return new JecnBaseVHLinePanel(this.getFlowElementData().clone());
	}

	public JecnVHLineResizePanel getVHLineResizePanel1() {
		return vHLineResizePanel1;
	}

	public JecnVHLineResizePanel getVHLineResizePanel2() {
		return vHLineResizePanel2;
	}

	public JecnVHLineResizePanel getVHLineResizePanel3() {
		return vHLineResizePanel3;
	}

	public JecnVHLineResizePanel getVHLineResizePanel4() {
		return vHLineResizePanel4;
	}

	public JecnVHLineResizePanel getVHLineResizePanel5() {
		return vHLineResizePanel5;
	}

	public JecnVHLineResizePanel getVHLineResizePanel6() {
		return vHLineResizePanel6;
	}

	public JecnVHLineResizePanel getVHLineResizePanel7() {
		return vHLineResizePanel7;
	}

	public JecnVHLineResizePanel getVHLineResizePanel8() {
		return vHLineResizePanel8;
	}

	public JecnVHLineResizePanel getVHLineResizePanel9() {
		return vHLineResizePanel9;
	}

	public JecnVHLineResizePanel getVHLineResizePanel10() {
		return vHLineResizePanel10;
	}

	/**
	 * 点击分割线显示横竖风行显示点
	 * 
	 */
	public void showParallelLinesResizeHandles() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		// 画单线
		switch (getLineDirectionEnum()) {
		case horizontal:// 水平
			if (!JecnSystemStaticData.isShowParallelLines()) {// 是否显示横向分割线
				return;
			}
			break;
		case vertical:// 垂直
			if (!JecnSystemStaticData.isShowVerticalLine()) {// 是否显示竖向分割线
				return;
			}
			break;
		}

		if (showResizeHandle) {
			this.hideParallelLinesResizeHandles();
		}
		showResizeHandle = true;
		int oRder = this.getFlowElementData().getZOrderIndex();
		// 面板添加横竖分割线，设置横竖分割线层级
		workFlowAddVHLine(desktopPane, oRder);
		// 获取面板大小
		int workWidth = desktopPane.getWidth();
		int workHeight = desktopPane.getHeight();

		int resizeW = vHLineResizePanel1.getWidth();
		int resizeH = vHLineResizePanel1.getHeight();
		// 画单线
		switch (getLineDirectionEnum()) {
		case horizontal:// 水平
			reSizeHLineHandles(workWidth, resizeW);
			break;
		case vertical:// 垂直
			reSizeVLineHandles(workHeight, resizeH);
			break;
		}

		desktopPane.updateUI();
	}

	/**
	 * 面板上添加横竖分割线、设置分割线层级
	 * 
	 * @param desktopPane
	 *            JecnDrawDesktopPane画图面板
	 * @param oRder
	 *            分割线层级
	 */
	private void workFlowAddVHLine(JecnDrawDesktopPane desktopPane, int oRder) {
		desktopPane.add(vHLineResizePanel1);
		desktopPane.setLayer(vHLineResizePanel1, oRder + 1);

		desktopPane.add(vHLineResizePanel2);
		desktopPane.setLayer(vHLineResizePanel2, oRder + 1);

		desktopPane.add(vHLineResizePanel3);
		desktopPane.setLayer(vHLineResizePanel3, oRder + 1);

		desktopPane.add(vHLineResizePanel4);
		desktopPane.setLayer(vHLineResizePanel4, oRder + 1);

		desktopPane.add(vHLineResizePanel5);
		desktopPane.setLayer(vHLineResizePanel5, oRder + 1);

		desktopPane.add(vHLineResizePanel6);
		desktopPane.setLayer(vHLineResizePanel6, oRder + 1);

		desktopPane.add(vHLineResizePanel7);
		desktopPane.setLayer(vHLineResizePanel7, oRder + 1);

		desktopPane.add(vHLineResizePanel8);
		desktopPane.setLayer(vHLineResizePanel8, oRder + 1);

		desktopPane.add(vHLineResizePanel9);
		desktopPane.setLayer(vHLineResizePanel9, oRder + 1);

		desktopPane.add(vHLineResizePanel10);
		desktopPane.setLayer(vHLineResizePanel10, oRder + 1);
	}

	/**
	 * 设置横向分割线选中点
	 * 
	 * @param workWidth
	 *            int 面板宽度
	 */
	private void reSizeHLineHandles(int workWidth, int resizeW) {
		int vhSizeY = this.getLocation().y - resizeW / 2;
		vHLineResizePanel1.setLocation(0, vhSizeY);

		vHLineResizePanel2.setLocation(workWidth - 5, vhSizeY);

		vHLineResizePanel3.setLocation(workWidth / 9, vhSizeY);

		vHLineResizePanel4.setLocation(workWidth * 2 / 9, vhSizeY);

		vHLineResizePanel5.setLocation(workWidth * 3 / 9, vhSizeY);

		vHLineResizePanel6.setLocation(workWidth * 4 / 9, vhSizeY);

		vHLineResizePanel7.setLocation(workWidth * 5 / 9, vhSizeY);

		vHLineResizePanel8.setLocation(workWidth * 6 / 9, vhSizeY);

		vHLineResizePanel9.setLocation(workWidth * 7 / 9, vhSizeY);

		vHLineResizePanel10.setLocation(workWidth * 8 / 9, vhSizeY);
	}

	/**
	 * 设置竖向分割线选中点
	 * 
	 * @param workHeight
	 *            int面板高度
	 */
	private void reSizeVLineHandles(int workHeight, int resizeH) {
		int vhSizeX = this.getLocation().x - resizeH / 2;
		this.vHLineResizePanel1.setLocation(vhSizeX, 0);
		this.vHLineResizePanel2.setLocation(vhSizeX, workHeight / 9);

		this.vHLineResizePanel4.setLocation(vhSizeX, workHeight * 2 / 9);

		this.vHLineResizePanel5.setLocation(vhSizeX, workHeight * 3 / 9);
		this.vHLineResizePanel6.setLocation(vhSizeX, workHeight * 4 / 9);

		this.vHLineResizePanel7.setLocation(vhSizeX, workHeight * 5 / 9);

		this.vHLineResizePanel8.setLocation(vhSizeX, workHeight * 6 / 9);

		this.vHLineResizePanel9.setLocation(vhSizeX, workHeight * 7 / 9);

		this.vHLineResizePanel10.setLocation(vhSizeX, workHeight * 8 / 9);
		this.vHLineResizePanel3.setLocation(vhSizeX, workHeight - 5);
	}

	/**
	 * 面板删除横竖分割线选中点
	 * 
	 */
	public void hideParallelLinesResizeHandles() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (showResizeHandle) {
			showResizeHandle = false;
			desktopPane.remove(vHLineResizePanel1);
			if (vHLineResizePanel2 != null) {
				desktopPane.remove(vHLineResizePanel2);
			}
			if (vHLineResizePanel3 != null) {
				desktopPane.remove(vHLineResizePanel3);
			}
			if (vHLineResizePanel4 != null) {
				desktopPane.remove(vHLineResizePanel4);
			}
			if (vHLineResizePanel5 != null) {
				desktopPane.remove(vHLineResizePanel5);
			}
			if (vHLineResizePanel6 != null) {
				desktopPane.remove(vHLineResizePanel6);
			}
			if (vHLineResizePanel7 != null) {
				desktopPane.remove(vHLineResizePanel7);
			}
			if (vHLineResizePanel8 != null) {
				desktopPane.remove(vHLineResizePanel8);
			}
			if (vHLineResizePanel9 != null) {
				desktopPane.remove(vHLineResizePanel9);
			}
			if (vHLineResizePanel10 != null) {
				desktopPane.remove(vHLineResizePanel10);
			}
			desktopPane.updateUI();
		}
	}

	/**
	 * 点击横竖分割线判断是否点击分割线区域内
	 * 
	 * @param px
	 * @param py
	 * @return
	 */
	public boolean lineContainsPoint(int px, int py) {
		// 画单线
		switch (getLineDirectionEnum()) {
		case horizontal:// 水平
			return (Math.abs(getY() - py) < 5);
		case vertical:// 垂直
			return (Math.abs(getX() - px) < 5);
		}
		return false;
	}

	/**
	 * 面板变化，重置分割线
	 * 
	 */
	public void rePaintVHLinePanel() {
		if (showVHLine() != this.isVisible()) {
			this.setVisible(showVHLine());
			// 隐藏分割线显示点
			this.hideParallelLinesResizeHandles();
		}
	}

	protected boolean showVHLine() {
		return true;
	}

	public boolean isShowResizeHandle() {
		return showResizeHandle;
	}
}
