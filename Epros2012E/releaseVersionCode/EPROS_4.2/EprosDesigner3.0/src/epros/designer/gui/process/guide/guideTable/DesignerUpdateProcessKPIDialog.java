package epros.designer.gui.process.guide.guideTable;

import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;

import epros.designer.gui.process.guide.UpdateProcessKPIDialog;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 编辑流程KPI
 * 
 * @author 2012-07-12
 * 
 */
public class DesignerUpdateProcessKPIDialog extends UpdateProcessKPIDialog {
	private JecnFlowKpiNameT flowKpiNameT = null;
	public boolean isOperation = false;

	public DesignerUpdateProcessKPIDialog(JecnFlowKpiNameT flowKpiNameT) {
		super(flowKpiNameT);
		this.flowKpiNameT = flowKpiNameT;
	}

	@Override
	public String getKPITile() {
		return JecnProperties.getValue("editFlowKPI");
	}

	@Override
	public void saveData() {
		// KPI名称
		String kpiName = this.kpiNameField.getText().trim();
		// KPI 类型
		String kpiType = (String) this.kpiTypeCombox.getSelectedItem();
		// KPI 纵向单位名称
		String kpiLongitudinal = (String) this.longitudinalCombox
				.getSelectedItem();
		// kpi横向单位名称
		String kpiTransverse = (String) this.transverseCombox.getSelectedItem();
		// kpi定义
		String kpiDefined = this.kpiDefinedArea.getText();
		// kpi统计方法
		String kpiMethods = this.kpiMethodsArea.getText();
		// kpi目标值
		String kpiTarget = this.targetField.getText();

		// KPI数据Bean
		flowKpiNameT.setKpiName(kpiName);
		if (kpiType != null
				&& kpiType.equals(JecnProperties.getValue("effectTarget"))) {
			flowKpiNameT.setKpiType(0);
		} else if (kpiType != null
				&& kpiType.equals(JecnProperties.getValue("efficiencyTarget"))) {
			flowKpiNameT.setKpiType(1);
		}
		flowKpiNameT.setKpiHorizontal(kpiTransverse);
		flowKpiNameT.setKpiVertical(kpiLongitudinal);
		flowKpiNameT.setKpiDefinition(kpiDefined);
		flowKpiNameT.setKpiStatisticalMethods(kpiMethods);
		flowKpiNameT.setKpiTarget(kpiTarget);
		isOperation = true;
		this.dispose();
	}

}
