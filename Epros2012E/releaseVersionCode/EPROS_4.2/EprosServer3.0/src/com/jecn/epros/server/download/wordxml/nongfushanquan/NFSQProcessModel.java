package com.jecn.epros.server.download.wordxml.nongfushanquan;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ProjectSignBean;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;

import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 农夫山泉操作说明
 * 
 */
public class NFSQProcessModel extends ProcessFileModel {
	/** *** 段落属性特殊符号 **** */
	private ParagraphBean g_SignPpr;
	/** *** 段落属性特殊符号 下划线 **** */
	private ParagraphBean g_SignUnderLinePpr;
	/** *** 段落属性 蓝色 **** */
	private ParagraphBean g_BluePpr;

	public NFSQProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		getDocProperty().setFlowChartScaleValue(5.5F);
		getDocProperty().setNewTblWidth(17.5F);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		initDocumentStyle();
		setDocStyle(". ", textTitleStyle());

	}

	protected void initDocumentStyle() {

		// 初始化带符号段落
		g_SignPpr = new ParagraphBean(Align.left, JecnWordUtil.WUHAO);
		g_SignPpr.setSpace(0f, 0f, JecnWordUtil.lineRule18);
		g_SignPpr.setProjectSignBean(new ProjectSignBean(1));

		// 初始化带符号、下划线 段落
		g_SignUnderLinePpr = new ParagraphBean(Align.left, JecnWordUtil.WUHAO_B_U_BLUE);
		g_SignUnderLinePpr.setSpace(0f, 0f, JecnWordUtil.lineRule18);
		g_SignUnderLinePpr.setProjectSignBean(new ProjectSignBean(1));

		// 初始化带 蓝色 段落
		g_BluePpr = new ParagraphBean(Align.left, JecnWordUtil.WUHAO_B_BLUE);
		g_BluePpr.setSpace(0f, 0f, JecnWordUtil.lineRule18);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = new SectBean();
		sectBean.setStartNum(1);
		// 设置页边距
		sectBean.setPage(1.5F, 2, 0.55F, 1.8F, 0.78F, 0.9F);
		sectBean.setSize(21, 29.7F);
		sectBean.setDocGrid(new DocGrid(DocGridType.LINESANDCHARTS, 15.6F, 10.5F));
		firstSect.setSectBean(sectBean);

		// 创建页眉
		Header hdr = firstSect.createHeader(HeaderOrFooterType.first);
		// 添加页眉内容
		TableBean tabBean = new TableBean();
		tabBean.setBorder(1.5F);
		tabBean.setBorderInsideH(1);
		tabBean.setBorderInsideV(1);
		tabBean.setTableLeftInd(2.41F);
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tabBean.setCellMarginBean(marginBean);
		ParagraphBean sectTblTitleStyle = new ParagraphBean(Align.center, JecnWordUtil.XIAOSI_B);
		sectTblTitleStyle.setSpace(0f, 0f, JecnWordUtil.lineRule18);

		ParagraphBean sectTblContentStyle = new ParagraphBean(Align.left, JecnWordUtil.WUHAO_B);
		sectTblContentStyle.setSpace(0f, 0f, JecnWordUtil.lineRule18);

		List<String[]> rowData = new ArrayList<String[]>();
		// 添加数据
		rowData.add(new String[] {
				"文件名称",
				processDownloadBean.getFlowName(),
				JecnUtil.getValue("intensive"),
				"0".equals(processDownloadBean.getFlowIsPublic()) ? JecnUtil.getValue("secret") : JecnUtil
						.getValue("public") });
		rowData.add(new String[] { "文件层次", "", "文件编号", processDownloadBean.getFlowInputNum(), "分发号", "" });
		rowData.add(new String[] { "流程责任人", processDownloadBean.getResponFlow().getDutyUserName(), "审核", "", "复核", "",
				"批准", "" });
		rowData.add(new String[] { "归属部门", processDownloadBean.getResponFlow().getDutyOrgName(), "发布日期", "", "试行日期",
				"", "实施日期", "" });
		Table tbl = JecnWordUtil.createTab(hdr, tabBean, new float[] { 2.51F, 2.25F, 1.91F, 1.91F, 1.91F, 1.49F, 1.9F,
				1.98F }, rowData, sectTblContentStyle);
		tbl.getRow(0).getCell(0).setColspan(2); // 设置跨列
		tbl.getRow(0).setHeight(0.8F);
		tbl.setRowStyle(0, sectTblTitleStyle);
		// 在段落中添加图片
		Image img = new Image(getDocProperty().getLogoImage());
		// 设置图片绝对定位
		img.setPositionBean(new PositionBean(PositionType.absolute, 3.35F, 0, -104.4F, 0));
		// 设置图片宽高
		img.setSize(3.18F, 1.1F);
		tbl.getRow(0).getCell(0).getParagraph(0).appendGraphRun(img);
		tbl.getRow(0).getCell(1).setColspan(4); // 设置跨列

		// 设置第三列跨列
		tbl.getRow(1).getCell(3).setColspan(3);

		LineGraph line = new LineGraph(LineStyle.thickThin, 3);
		line.setFrom(-27F, 12.45F);
		line.setTo(513.2F, 12.45F);
		line.setPositionBean(new PositionBean());
		hdr.createParagraph(line);

		// 首页页脚
		firstSect.createFooter(HeaderOrFooterType.first).addTable(getCommonFtr(false));
		// 设置奇偶数页眉页脚
		ParagraphBean paragraphBean = new ParagraphBean(Align.center, "宋体", Constants.xiaosi, false);
		paragraphBean.setpStyle(PStyle.AF1);
		// 页眉
		Paragraph hdrP = new Paragraph(processDownloadBean.getFlowName(), paragraphBean);
		img = new Image(getDocProperty().getLogoImage());
		img.setPositionBean(new PositionBean(PositionType.absolute, -19.9F, 38.25F, 0, 0));
		img.setSize(3.18F, 1.1F);
		hdrP.appendGraphRun(img);

		// 页脚内容
		firstSect.createHeader(HeaderOrFooterType.odd).addParagraph(hdrP);
		firstSect.createFooter(HeaderOrFooterType.odd).addTable(getCommonFtr(false));
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = new SectBean();
		sectBean.setSize(29.7F, 21);
		sectBean.setPage(2, 0.55F, 1.8F, 1.5F, 0.78F, 0.9F);
		sectBean.setHorizontal(true);
		flowChartSect.setSectBean(sectBean);
		this.createCommftrhdr(flowChartSect, true);

	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = new SectBean();
		sectBean.setSize(21, 29.7F);
		// 设置页边距
		sectBean.setPage(1.5F, 2, 0.55F, 1.6F, 0.78F, 0.9F);
		secondSect.setSectBean(sectBean);
		this.createCommftrhdr(secondSect, false);

	}

	@Override
	protected void initTitleSect(Sect sect) {
	}

	// 创建通用的页眉页脚
	private void createCommftrhdr(Sect sect, boolean isChart) {
		ParagraphBean paragraphBean = new ParagraphBean(Align.center, "宋体", Constants.xiaosi, false);
		paragraphBean.setpStyle(PStyle.AF1);
		// 页眉内容
		Paragraph hdrP = new Paragraph(processDownloadBean.getFlowName(), paragraphBean);
		Image img = new Image(getDocProperty().getLogoImage());
		img.setPositionBean(new PositionBean(PositionType.absolute, -19.9F, 38.25F, 0, 0));
		img.setSize(3.18F, 1.1F);
		hdrP.appendGraphRun(img);

		// 页脚内容
		Table tableFtr = getCommonFtr(isChart);
		sect.createHeader(HeaderOrFooterType.odd).addParagraph(hdrP);
		sect.createFooter(HeaderOrFooterType.odd).addTable(tableFtr);
	}

	/**
	 * 创建通用页脚内容
	 * 
	 * @param isChart
	 * 
	 * @return
	 */
	private Table getCommonFtr(boolean isChart) {
		ParagraphLineRule vLine = new ParagraphLineRule(1F, LineRuleType.AUTO);
		ParagraphBean leftPBean = new ParagraphBean("宋体", Constants.xiaowu, false);
		ParagraphBean centerPBean = new ParagraphBean(Align.center, "宋体", Constants.xiaowu, false);
		ParagraphBean rightPBean = new ParagraphBean(Align.right, "宋体", Constants.xiaowu, false);
		leftPBean.setSpace(0.8F, 0F, vLine);
		centerPBean.setSpace(0.8F, 0F, vLine);
		rightPBean.setSpace(0.8F, 0F, vLine);
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 表格左缩进0.34 厘米
		tblStyle.setTableLeftInd(0.34F);
		// 表格内边距 厘米
		tblStyle.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		float[] cellWidths = { 8.2F, 1.5F, 8.2F };
		if (isChart) {
			cellWidths = new float[] { 13.5F, 1.5F, 13.5F };
		}
		List<String[]> rowData = new ArrayList<String[]>();
		String fileNumber = "文件编号" + processDownloadBean.getFlowInputNum();
		rowData.add(new String[] { fileNumber, "", "实施日期" });
		Table tab = new Table(tblStyle, cellWidths);
		tab.createTableRow(rowData, leftPBean);
		Paragraph centerP = tab.getRow(0).getCell(1).getParagraph(0);
		centerP.setParagraphBean(centerPBean);
		centerP.appendCurPageRun();
		centerP.appendTextRun("/");
		centerP.appendTotalPagesRun(0);

		// 设置右对齐
		Paragraph rightP = tab.getRow(0).getCell(2).getParagraph(0);
		rightP.setParagraphBean(rightPBean);
		return tab;
	}

	@Override
	protected void a03(ProcessFileItem processFileItem, String... content) {
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "名词", "解释" });
		for (int i = 0; i < content.length; i++) {
			if (content[i] != null) {
				String[] split = content[i].split("[：,:]");
				if (split.length == 2) {
					rowData.add(new String[] { split[0], split[1] });
				} else {
					rowData.add(new String[] { "", content[i] });
				}
			}

		}
		Table tbl = createTableItem(processFileItem, new float[] { 3, 14.5F }, rowData);
		tbl.setCellStyle(1, g_SignPpr);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), true);
		tbl.setRowBorder(0, "Bottom", 1.5F);
	}

	@Override
	protected void a07(ProcessFileItem processFileItem, List<KeyActivityBean> list) {
		createTitle(processFileItem);
		float[] width = new float[] { 1.5F, 2.5F, 2.5F, 7, 4 };
		// 活动编号
		// String hdbh = JecnUtil.getValue("activitynumbers");
		String hdbh = "编号";
		// 执行角色
		String executiveRole = JecnUtil.getValue("executiveRole");
		// 活动名称
		String hdmc = JecnUtil.getValue("activityTitle");
		// 活动说明
		String hdsm = JecnUtil.getValue("activityIndicatingThat");
		// 问题区域
		String ProblemArea = JecnUtil.getValue("problemAreas");
		// 关键成功因素
		String KeySuccessFactor = JecnUtil.getValue("keySuccessFactors");
		// 关键控制点
		String KeyControlPoint = JecnUtil.getValue("keyControlPoint");
		/** 问题区域 ** */
		List<KeyActivityBean> paList = new ArrayList<KeyActivityBean>();
		/** * 关键成功因素 **** */
		List<KeyActivityBean> kcpList = new ArrayList<KeyActivityBean>();
		/** * 关键控制点 **** */
		List<KeyActivityBean> ksfList = new ArrayList<KeyActivityBean>();
		/** * 关键控制点(合规) **** */
		List<KeyActivityBean> kcpAllowList = new ArrayList<KeyActivityBean>();
		for (KeyActivityBean keyActivityBean : processDownloadBean.getKeyActivityShowList()) {
			// 问题区域
			if ("1".equals(keyActivityBean.getActiveKey())) {
				paList.add(keyActivityBean);
			} // 关键成功因素
			else if ("2".equals(keyActivityBean.getActiveKey())) {
				kcpList.add(keyActivityBean);
			} // 关键控制点
			else if ("3".equals(keyActivityBean.getActiveKey())) {
				ksfList.add(keyActivityBean);
			}// 关键成功因素(合规)
			else if ("4".equals(keyActivityBean.getActiveKey())) {
				kcpAllowList.add(keyActivityBean);
			}

		}
		/**
		 * 没有数据的情况下直接返回
		 */
		if (paList.size() == 0 && kcpList.size() == 0 && ksfList.size() == 0) {
			processFileItem.getBelongTo().createParagraph(blank, getDocProperty().getTextContentStyle());
			return;
		}

		int count = 1;
		// 生成Table
		if (paList.size() != 0) {
			createKeyActiveTbl(processFileItem, width, new String[] { hdbh, executiveRole, hdmc, hdsm, ProblemArea },
					paList, count, "问题区域");
			count++;
		}
		if (kcpList.size() != 0) {
			createKeyActiveTbl(processFileItem, width,
					new String[] { hdbh, executiveRole, hdmc, hdsm, KeySuccessFactor }, kcpList, count, "关键成功因素");
			count++;
		}
		if (ksfList.size() != 0) {
			createKeyActiveTbl(processFileItem, width,
					new String[] { hdbh, executiveRole, hdmc, hdsm, KeyControlPoint }, ksfList, count, "关键控制点");
		}
		if (kcpAllowShow() && kcpAllowList.size() != 0) {
			createKeyActiveTbl(processFileItem, width,
					new String[] { hdbh, executiveRole, hdmc, hdsm, KeyControlPoint }, kcpAllowList, count,
					"关键合规点");
		}
	}

	/**
	 * 创建关键活动表格
	 * 
	 * @param processFileItem
	 * @param fs
	 * @param titles
	 * @param keyActiveList
	 * @param count
	 * @param title
	 */
	private void createKeyActiveTbl(ProcessFileItem processFileItem, float[] width, String[] titles,
			List<KeyActivityBean> keyActiveList, int count, String title) {
		/** 活动标题行段落 */
		ParagraphBean lv2Ppr = new ParagraphBean(Align.left, JecnWordUtil.XIAOSI_B);
		lv2Ppr.setInd(0.53F, 0, 0.9F, 0);
		lv2Ppr.setSpace(0.5F, 0.2F, JecnWordUtil.lineRule20);
		lv2Ppr.setpStyle(PStyle.LVL2);
		processFileItem.getBelongTo().createParagraph(title, lv2Ppr);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(titles);
		String[] dataRow;
		for (KeyActivityBean keyActive : keyActiveList) {
			dataRow = new String[5];
			dataRow[0] = JecnWordUtil.obj2String(keyActive.getActiveInfo()[1]);
			dataRow[1] = keyActive.getActivityNumber();
			dataRow[2] = keyActive.getActivityName();
			dataRow[3] = keyActive.getActivityShow();
			dataRow[4] = keyActive.getActivityShowControl();
			rowData.add(dataRow);
		}
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setCellStyle(3, g_SignPpr);
		tbl.setCellStyle(4, g_SignPpr);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), true);
		tbl.setRowStyle(0, Valign.center);
		tbl.setRowBorder(0, "Bottom", 1.5F);
		fillActiveData(tbl, keyActiveList);
	}

	/**
	 * 填充活动说明数据
	 * 
	 * @param tbl
	 * @param keyActiveList
	 */
	private void fillActiveData(Table tbl, List<KeyActivityBean> keyActiveList) {
		for (Object[] obj : processDownloadBean.getAllActivityShowList()) {
			for (int i = 0, j = 1; i < keyActiveList.size(); i++, j++) {
				// 主键相同
				if (obj[6].toString().equals(keyActiveList.get(i).getActiveInfo()[0].toString())) {
					appendActiveDetailInfo(tbl.getRow(j).getCell(3), obj, false);
				}
			}
		}
	}

	/**
	 * 追加活动的输入、输出、操作规范
	 * 
	 * @param processDownLoadBean
	 * @param tab
	 * @param obj
	 * @param flag
	 *            是否添加注意事项 true 添加
	 * @return
	 */
	private void appendActiveDetailInfo(TableCell tc, Object[] obj, boolean flag) {
		List<String> rule = getActiveRule(processDownloadBean.getOperationTemplateListNF(), obj[6]);
		if (flag && obj[8] != null && !"".equals(obj[8])) {
			if ("1".equals(obj[8].toString())) {
				tc.createParagraph("注意事项：", JecnWordUtil.WUHAO_B);
			} else if ("2".equals(obj[8].toString())) {
				tc.createParagraph("关键成功因素：", JecnWordUtil.WUHAO_B);
			} else if ("3".equals(obj[8].toString())) {
				tc.createParagraph("关键控制点：", JecnWordUtil.WUHAO_B);
			}
			tc.createParagraph(JecnWordUtil.getContents(obj[7]), g_SignPpr);
		}
		if (rule.size() > 0) {
			tc.createParagraph("需要遵循的操作规范：", JecnWordUtil.WUHAO_B);
			for (String str : rule) {
				tc.createParagraph(getName(str), g_SignUnderLinePpr);
			}
		}
		if (obj[4] != null && !"".equals(obj[4])) {
			tc.createParagraph(JecnUtil.getValue("input") + "：", JecnWordUtil.WUHAO_B);
			String[] str = JecnWordUtil.getContents(obj[4].toString());
			for (String string2 : str) {
				tc.createParagraph(getName(string2), g_SignUnderLinePpr);
			}
		}
		if (obj[5] != null && !"".equals(obj[5])) {
			tc.createParagraph(JecnUtil.getValue("output") + "：", JecnWordUtil.WUHAO_B);
			String[] str = JecnWordUtil.getContents(obj[5].toString());
			for (String string2 : str) {
				tc.createParagraph(getName(string2), g_SignUnderLinePpr);
			}
		}
		if (tc.getParagraphs().size() == 0) {
			tc.createParagraph("", g_SignPpr);
		}
	}

	private List<String> getActiveRule(List<Object[]> operationTemplateList, Object object) {
		List<String> result = new ArrayList<String>();
		for (Object[] objects : operationTemplateList) {
			if (objects[2].toString().equals(object.toString())) {
				result.add(objects[1].toString());
			}
		}
		return result;
	}

	@Override
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);
		// 角色名称
		String roleName = "部门/岗位";
		// 角色职责
		String roleResponsibility = "职责";
		// 标题数组
		rowData.add(0, new String[] { roleName, roleResponsibility });
		float[] width = new float[] { 4.69F, 12.81F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		// 宋体 五号加粗
		tbl.setCellStyle(0, new ParagraphBean("宋体", Constants.wuhao, true));
		// 设置第二列的样式
		tbl.setCellStyle(1, g_SignPpr);
		// 设置标题行样式
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), true);
		tbl.setRowStyle(0, Valign.center);
		tbl.setRowBorder(0, "Bottom", 1.5F);
	}

	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.getParagraphIndBean().setLeft(1.2F);
		newBean.getParagraphIndBean().setRight(0.3F);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	@Override
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {
		createTitle(processFileItem);
		// 活动编号
		// String activityNumber = JecnUtil.getValue("activitynumbers");
		String activityNumber = "编号";
		// 执行角色s
		String executiveRole = JecnUtil.getValue("executiveRole");
		// 活动名称
		String eventTitle = JecnUtil.getValue("activityTitle");
		// 活动说明
		String activityDescription = JecnUtil.getValue("activityIndicatingThat");
		float[] width = new float[] { 1.5F, 2.5F, 2.5F, 11 };

		List<Object[]> newRowData = new ArrayList<Object[]>();
		// 标题数组
		newRowData.add(new Object[] { activityNumber, executiveRole, eventTitle, activityDescription });
		// 添加数据 0:活动编号 1执行角色 2活动名称 3活动说明
		for (Object[] activeData : processDownloadBean.getAllActivityShowList()) {
			newRowData.add(new Object[] { activeData[0], activeData[1], activeData[2], activeData[3] });
		}
		Table tbl = createTableItem(processFileItem, width, JecnWordUtil.obj2String(newRowData));
		tbl.setCellStyle(3, g_SignPpr);
		// 追加输入、输出、操作规范 ,关键控制点
		for (int activeIndex = 0, tblIndex = 1; activeIndex < processDownloadBean.getAllActivityShowList().size(); activeIndex++, tblIndex++) {
			appendActiveDetailInfo(tbl.getRow(tblIndex).getCell(3), processDownloadBean.getAllActivityShowList().get(
					activeIndex), true);
		}
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), true);
		tbl.setRowBorder(0, "Bottom", 1.5F);
		tbl.setRowStyle(0, Valign.center);
		tbl.setCellStyle(0, getCenterPBean(), Valign.center);
		tbl.setCellStyle(1, getCenterPBean(), Valign.center);

		// 活动名称左对齐
		ParagraphBean pBean = getCenterPBean();
		pBean.setAlign(Align.left);
		tbl.setCellStyle(2, pBean, Valign.center);
		tbl.setCellStyle(1, pBean, Valign.center);
		tbl.setRowStyle(0, tblTitleStyle());

	}

	/**
	 * 
	 * 流程记录
	 * 
	 */
	@Override
	protected void a11(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);
		// 标题数组
		String[] titles = new String[] { "序号", "编号", "表单名称", "保管部门", "保存期限" };
		float[] width = new float[] { 1.25F, 3.75F, 7, 3, 2.5F };
		List<String[]> newRowData = new ArrayList<String[]>();
		for (int index = 0; index < rowData.size(); index++) {
			String[] curArray = rowData.get(index);
			newRowData.add(new String[] { index + 1 + "", curArray[0], getName(curArray[1]), "", "" });
		}
		newRowData.add(0, titles);
		createCommonTbl(processFileItem, width, newRowData);
	}

	/**
	 * 
	 * 操作规范
	 * 
	 */
	@Override
	protected void a12(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);
		List<String[]> newRowData = new ArrayList<String[]>();
		for (int index = 0; index < rowData.size(); index++) {
			// 文件编号 文件名称 对应的活动ID 责任部门
			String[] curr = rowData.get(index);

			String[] cA = new String[4];
			// 序号
			cA[0] = String.valueOf(index + 1);
			// 编号
			cA[1] = curr[0];
			// 名称
			cA[2] = getName(curr[1]);
			// 所属部门
			cA[3] = curr[3];

			newRowData.add(cA);
		}
		float[] width = new float[] { 1.25F, 3.75F, 9, 3.5F };
		String[] titles = new String[] { "序号", "编号", "操作规范名称", "归属部门" };
		newRowData.add(0, titles);
		createCommonTbl(processFileItem, width, newRowData);
	}

	/**
	 * 创建通用的表格（标题行为4列（"序号","编号","具体列名","归属部门"）
	 * 
	 * @param processFileItem
	 * @param rowData
	 */
	private Table createCommonTbl(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setCellStyle(0, getCenterPBean(), Valign.center);
		tbl.setCellStyle(2, g_BluePpr);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), true);
		tbl.setRowBorder(0, "Bottom", 1.5F);
		return tbl;
	}

	/**
	 * 相关流程
	 * 
	 * @param _count
	 * @param name
	 * @param relatedProcessMap
	 */
	@Override
	protected void a13(ProcessFileItem processFileItem, Map<String, List<RelatedProcessBean>> relatedProcessMap) {
		// 标题
		createTitle(processFileItem);
		List<String[]> newRowData = new ArrayList<String[]>();
		// 用来存放临时流程名称 去掉重复的
		Set<String> tempSet = new HashSet<String>();
		int index = 0;
		for (String key : relatedProcessMap.keySet()) {
			for (RelatedProcessBean relatedProcess : relatedProcessMap.get(key)) {
				if (tempSet.contains(relatedProcess.getFlowName())) {
					continue;
				}
				// 添加数据
				tempSet.add(relatedProcess.getFlowName());
				// 类型、编号、名称、责任部门
				index = index + 1;
				String flowName = "《" + relatedProcess.getFlowName() + "》";
				newRowData.add(new String[] { String.valueOf(index), relatedProcess.getFlowNumber(), flowName,
						relatedProcess.getDutyOrg() });
			}
		}

		// 流程编号
		String processNumber = JecnUtil.getValue("processNumber");
		// 流程名称
		String processName = JecnUtil.getValue("processName");
		// 标题数组
		String[] titles = new String[] { "序号", processNumber, processName, "归属部门" };
		newRowData.add(0, titles);
		float[] width = new float[] { 1.25F, 3.75F, 9, 3.5F };
		createCommonTbl(processFileItem, width, newRowData);

	}

	/**
	 * 
	 * 相关制度
	 * 
	 */
	@Override
	protected void a14(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		List<String[]> newRowData = new ArrayList<String[]>();
		for (int index = 0; index < rowData.size(); index++) {
			// 类型、编号、名称、责任部门
			String[] curr = rowData.get(index);

			String[] cA = new String[4];
			// 序号
			cA[0] = String.valueOf(index + 1);
			// 编号
			cA[1] = curr[1];
			// 名称
			String standName = curr[2];
			if ("2".equals(curr[0])) {// 0：目录 1：制度 2：制度文件
				cA[2] = getName(standName);
			} else {
				cA[2] = standName;
			}
			// 所属部门
			cA[3] = curr[3];

			newRowData.add(cA);
		}
		// 制度名称
		String name = JecnUtil.getValue("ruleName");
		// 标题数组
		String[] titles = new String[] { "序号", "编号", name, "归属部门" };
		newRowData.add(0, titles);
		float[] width = new float[] { 1.25F, 3.75F, 9, 3.5F };
		createCommonTbl(processFileItem, width, newRowData);
	}

	/**
	 * 
	 * 流程监控指标
	 * 
	 */
	@Override
	protected void a15(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);
		float[] width = new float[] { 3.8F, 8.2F, 2.5F, 3 };
		// 名称
		String name = JecnUtil.getValue("name");
		// 计算公式
		String computeFormula = "计算公式";
		// 统计方法
		String statisticFrequency = "统计频率";
		// 数据提供者
		String kpiDataPeopleName = JecnUtil.getValue("kpiDataPeopleName");
		for (String[] data : rowData) {
			String type = data[2];
			if (type != null) {
				data[2] = "0".equals(type.toString()) ? "月" : "季度";
			}
		}

		// 标题数组
		String[] titleStrNum = new String[] { name, computeFormula, statisticFrequency, kpiDataPeopleName };
		rowData.add(0, titleStrNum);
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), true);
		tbl.setRowBorder(0, "Bottom", 1.5F);
		tbl.setCellStyle(2, Valign.center);
		tbl.setCellStyle(3, Valign.center);
	}

	@Override
	protected void a20(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);
		float[] width = new float[] { 2.5F, 2.75F, 2.75F, 3.5F, 2.5F, 3.5F };
		// 记录名称
		String recordName = JecnUtil.getValue("recordName");
		// 保存责任人
		String saveResponsiblePersons = JecnUtil.getValue("saveResponsiblePersons");
		// 保存场所
		String savePlace = JecnUtil.getValue("savePlace");
		// 归档时间
		String filingTime = JecnUtil.getValue("filingTime");
		// 保存期限
		String expirationDate = JecnUtil.getValue("storageLife");
		// 到期处理方式
		String handlingDue = JecnUtil.getValue("treatmentDue");
		// 标题数组
		String[] title = new String[] { recordName, saveResponsiblePersons, savePlace, filingTime, expirationDate,
				handlingDue };
		rowData.add(0, title);
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), true);
		tbl.setRowBorder(0, "Bottom", 1.5F);
	}

	/**
	 * 
	 * 相关标准
	 * 
	 */
	@Override
	protected void a21(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);
		List<String[]> newRowData = new ArrayList<String[]>();
		for (int index = 0; index < rowData.size(); index++) {
			// 类型、编号、名称、责任部门
			String[] curr = rowData.get(index);

			String[] cA = new String[4];
			// 序号
			cA[0] = String.valueOf(index + 1);
			// 编号
			cA[1] = curr[1];
			// 名称
			String standName = curr[2];
			if ("1".equals(curr[0])) {// 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
				cA[2] = getName(standName);
			} else {
				cA[2] = standName;
			}
			// 所属部门
			cA[3] = curr[3];

			newRowData.add(cA);
		}
		// 标准名称
		String name = JecnUtil.getValue("standardName");
		// 标题数组
		String[] titles = new String[] { "序号", "编号", name, "归属部门" };
		newRowData.add(0, titles);
		float[] width = new float[] { 1.25F, 3.75F, 9, 3.5F };
		createCommonTbl(processFileItem, width, newRowData);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, JecnWordUtil.WUHAO);
		tblContentStyle.setSpace(0f, 0f, JecnWordUtil.lineRule18);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 1.5 磅
		tblStyle.setBorder(1.5F);
		tblStyle.setBorderInsideH(1);
		tblStyle.setBorderInsideV(1);
		// 表格左缩进0.34 厘米
		tblStyle.setTableLeftInd(0.34F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.20F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		// 表格标题行属性
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, JecnWordUtil.WUHAO_B);
		tblTitileStyle.setSpace(0f, 0f, JecnWordUtil.lineRule18);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		// 初始化普通段落 ****/
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, JecnWordUtil.XIAOSI);
		textContentStyle.setInd(0.7F, 0, 0, 0);
		textContentStyle.setSpace(0f, 0f, JecnWordUtil.lineRule20);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		// 初始化一级标题
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, JecnWordUtil.XIAOSI_B);
		textTitleStyle.setpStyle(PStyle.LVL1);
		// 段落缩进 厘米
		textTitleStyle.setInd(0, 0, 0.88F, 0);
		// 设置段落行间距
		textTitleStyle.setSpace(0.5F, 0.2F, JecnWordUtil.lineRule20);
		return textTitleStyle;
	}

	/**
	 * 
	 * 文件名称非空添加书名号处理
	 * 
	 * @param str
	 *            String
	 * @return String
	 */
	private String getName(String str) {
		// 名称
		str = removeFileExt(str);
		return (str == null || "".equals(str)) ? "" : "《" + str + "》";
	}

	/**
	 * 设置段落居中 表格中的序号列
	 * 
	 * @return
	 */
	private ParagraphBean getCenterPBean() {
		ParagraphBean centerBean = new ParagraphBean(Align.center, g_SignPpr.getFontBean().getFontFamily(), g_SignPpr
				.getFontBean().getFontSize(), false);
		return centerBean;
	}

	/**
	 * 去除掉文件后缀名
	 * 
	 * @param fileName
	 * @return
	 */
	private String removeFileExt(String fileName) {
		if (fileName.lastIndexOf(".") != -1) {
			fileName = fileName.substring(0, fileName.lastIndexOf("."));
		}
		return fileName;
	}
}
