package com.jecn.epros.server.action.designer.rule.impl;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.action.designer.rule.IRuleAction;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.integration.JecnRuleRiskBeanT;
import com.jecn.epros.server.bean.integration.JecnRuleStandardBeanT;
import com.jecn.epros.server.bean.rule.AddRuleFileData;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.JecnRuleData;
import com.jecn.epros.server.bean.rule.JecnRuleOperationCommon;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.rule.sync.SyncRules;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.vatti.buss.DominoOperation;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class RuleActionImpl implements IRuleAction {

	private IRuleService ruleService;

	public IRuleService getRuleService() {
		return ruleService;
	}

	public void setRuleService(IRuleService ruleService) {
		this.ruleService = ruleService;
	}

	@Override
	public Long addRule(RuleT rulet) throws Exception {
		return ruleService.addRuleDir(rulet);
	}

	@Override
	public List<JecnTreeBean> getChildRules(Long pid, Long projectId) throws Exception {
		return ruleService.getChildRules(pid, projectId);
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception {
		return ruleService.getRoleAuthChildFiles(pId, projectId, peopleId);
	}

	@Override
	public void reName(String newName, Long id, Long updatePersonId, int userType) throws Exception {
		ruleService.reName(newName, id, updatePersonId, userType);

	}

	@Override
	public List<JecnTreeBean> searchByName(String name, Long projectId, Long peopleId) throws Exception {
		if (peopleId == null) {
			return ruleService.searchByName(name, projectId);
		}
		return ruleService.searchRoleAuthByName(name, projectId, peopleId);
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception {
		ruleService.updateSortNodes(list, pId, projectId, updatePersonId);
	}

	@Override
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		ruleService.moveNodes(listIds, pId, updatePersonId, moveNodeType);

	}

	/**
	 * @author yxw 2012-6-7
	 * @description:删除制度
	 * @param listIds
	 * @throws Exception
	 */
	@Override
	public void deleteRule(List<Long> listIds, Long updatePersonId) throws Exception {
		ruleService.deleteRule(listIds, updatePersonId);
	}

	@Override
	public void deleteFalseRule(Set<Long> setIds, Long updatePersonId, Long projectId) throws Exception {
		ruleService.deleteFalseRule(setIds, updatePersonId, projectId);
	}

	@Override
	public void addRuleFile(AddRuleFileData ruleFileData) throws Exception {
		ruleService.addRuleFile(ruleFileData);

	}

	@Override
	public Long updateRule(JecnRuleData ruleData) throws Exception {
		return ruleService.updateRule(ruleData);
	}

	@Override
	public Long addRule(RuleT ruelBean, String posIds, String orgIds, Long modeId, Long updatePersonId)
			throws Exception {
		return ruleService.addRule(ruelBean, posIds, orgIds, modeId, updatePersonId);
	}

	@Override
	public List<JecnRuleOperationCommon> findRuleOperationShow(Long ruleId) throws Exception {
		return ruleService.findRuleOperationShow(ruleId, false);
	}

	@Override
	public List<JecnTreeBean> getChildRuleDirs(Long pid, Long projectId) throws Exception {
		return ruleService.getChildRuleDirs(pid, projectId);
	}

	@Override
	public List<JecnTreeBean> getAllRuleDir(Long projectId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RuleT getRuleT(Long id) throws Exception {
		return ruleService.getRuleT(id);
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long ruleId, Long projectId) throws Exception {

		return ruleService.getPnodes(ruleId, projectId);
	}

	@Override
	public void updateRuleFile(long id, long fileId, String fileName, Long updatePersonId) throws Exception {
		ruleService.updateRuleFile(id, fileId, fileName, updatePersonId);

	}

	@Override
	public void updateRuleMode(long ruleId, long modeId, Long updatePersonId, Long longFlag) throws Exception {
		ruleService.updateRuleMode(ruleId, modeId, updatePersonId, longFlag);
	}

	@Override
	public JecnCreateDoc getRuleFile(Long ruleId, boolean isPub) throws Exception {
		return ruleService.getRuleFile(ruleId, isPub);
	}

	@Override
	public void cancelRelease(Long id, TreeNodeType type, Long peopleId) throws Exception {
		ruleService.cancelRelease(id, type, peopleId);
		// 华帝登录 删除Domino平台中的数据
		if (JecnUtil.isVattiLogin()) {
			DominoOperation.deleteDataFromDomino(id, "Rule");
		}
	}

	@Override
	public void directRelease(Long id, TreeNodeType type, Long peopleId) throws Exception {
		ruleService.directRelease(id, type, peopleId);
		// 华帝登录 向Domino平台插入数据
		if (JecnUtil.isVattiLogin()) {
			DominoOperation.importDataToDomino(id, "Rule");
		}
	}

	@Override
	public boolean validateAddName(String name, Long pid) throws Exception {
		return ruleService.validateAddName(name, pid);
	}

	@Override
	public boolean validateAddNameDirRepeat(Long pid, String name) throws Exception {
		return ruleService.validateAddNameDirRepeat(pid, name);
	}

	@Override
	public String[] validateAddNames(List<String> names, Long pId) throws Exception {
		return ruleService.validateAddNames(names, pId);
	}

	@Override
	public List<JecnTreeBean> searchISNotRuleDirByName(String name, Long projectId) throws Exception {
		return ruleService.searchISNotRuleDirByName(name, projectId);
	}

	/**
	 * 制度相关风险
	 * 
	 * @param riskTList
	 *            制度相关风险集合
	 * @param ruleId
	 *            制度主键ID
	 */
	@Override
	public void editRuleRiskT(List<JecnRuleRiskBeanT> riskTList, Long ruleId) throws Exception {
		ruleService.editRuleRiskT(riskTList, ruleId);
	}

	/**
	 * 
	 * 制度相关标准
	 * 
	 * @param standardTList
	 *            制度相关标准集合
	 * @param ruleId
	 *            主动主键ID
	 */
	@Override
	public void editRuleStandardT(List<JecnRuleStandardBeanT> standardTList, Long ruleId) throws Exception {
		ruleService.editRuleStandardT(standardTList, ruleId);
	}

	@Override
	public Long updateLocalRuleFile(RuleT ruleT) throws Exception {
		return ruleService.updateLocalRuleFile(ruleT);
	}

	@Override
	public FileOpenBean g020OpenRuleFile(long id) throws Exception {
		return ruleService.g020OpenRuleFile(id);
	}

	@Override
	public List<JecnTreeBean> getRuleTreeBeanByFlowId(Long ruleId) throws Exception {
		return ruleService.getRuleTreeBeanByFlowId(ruleId);
	}

	@Override
	public List<JecnTreeBean> getRuleTreeBeanByRuleId(Long flowId) throws Exception {
		return ruleService.getRuleTreeBeanByRuleId(flowId);
	}

	@Override
	public List<JecnTreeBean> getStandardizedFiles(Long ruleId) throws Exception {
		return ruleService.getStandardizedFiles(ruleId);
	}

	@Override
	public List<G020RuleFileContent> getG020RuleFileContents(Long fileId) {
		return ruleService.getG020RuleFileContents(fileId);
	}

	@Override
	public void updateRecovery(long fileId, long versionId, long updatePersonId) throws Exception {
		ruleService.updateRecovery(fileId, versionId, updatePersonId);
	}

	@Override
	public void deleteVersion(List<Long> listIds, Long fileId, long userId) throws Exception {
		ruleService.deleteVersion(listIds, fileId, userId);
	}

	@Override
	public FileOpenBean openVersion(Long ruleId, Long versionId) throws Exception {
		return ruleService.openVersion(ruleId, versionId);
	}

	@Override
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pId) throws Exception {
		return ruleService.getRecycleJecnTreeBeanList(projectId, pId);
	}

	@Override
	public List<JecnTreeBean> getAllFilesByName(String name, Long projectId, Set<Long> ruleIds, boolean isAdmin)
			throws Exception {
		return ruleService.getAllFilesByName(name, projectId, ruleIds, isAdmin);
	}

	@Override
	public void getRecycleFileIds(List<Long> listIds, Long projectId) throws Exception {
		ruleService.getRecycleFileIds(listIds, projectId);
	}

	@Override
	public void updateRecycleFile(List<Long> ids, Long projectId) throws Exception {
		ruleService.updateRecycleFile(ids, projectId);
	}

	@Override
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception {
		return ruleService.getPnodesRecy(id, projectId);
	}

	@Override
	public Long createRelatedFileDirByPath(Long id, Long projectId, Long createPeopleId) throws Exception {
		return ruleService.createRelatedFileDirByPath(id, projectId, createPeopleId);
	}

	@Override
	public Long getRelatedFileId(Long ruleId) throws Exception {
		return ruleService.getRelatedFileId(ruleId);
	}

	@Override
	public boolean isNotAbolishOrNotDelete(Long id) throws Exception {
		return ruleService.isNotAbolishOrNotDelete(id);
	}

	@Override
	public String validateNamefullPath(String ruleName, Long id, int type) {
		return ruleService.validateNamefullPath(ruleName, id, type);
	}

	@Override
	public String[] validateNamefullPath(List<String> names, Long id, int type) {
		return ruleService.validateNamefullPath(names, id, type);
	}

	@Override
	public void batchReleaseNotRecord(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) throws Exception {
		ruleService.batchReleaseNotRecord(ids, treeNodeType, peopleId);

	}

	@Override
	public void batchCancelRelease(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) {
		ruleService.batchCancelRelease(ids, treeNodeType, peopleId);
	}

	@Override
	public void syncRules(SyncRules syncRule) throws Exception {
		ruleService.syncRules(syncRule);
	}

	@Override
	public List<JecnTreeBean> getChildRules(Long pid, Long projectId, int... delState) throws Exception {
		return ruleService.getChildRules(pid, projectId, delState);
	}
}
