package epros.draw.event;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.ToolTipManager;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.MapLineFigure;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnSelectFigureXY;
import epros.draw.gui.figure.unit.JecnTempFigureBean;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.TempDividingLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 流程元素键盘方向键事件处理
 * 
 * @author ZHANGXH
 * @date： 日期：Aug 7, 2012 时间：3:35:21 PM
 */
public class JecnKeyHandler extends KeyAdapter {
	/** 是否拖动 boolean true:拖动；false：未拖动 */
	private boolean mouseDragged = false;
	/** 释放标识：第一次进入或释放后都为true */
	private boolean releasedFlag = false;
	/** 键盘事件鼠标每次移动的位移 */
	private int moveDiff = 1;
	/** 当前画图面板 */
	private JecnDrawDesktopPane desktopPane = null;
	/** 获取选中的所有图形 */
	private List<JecnBaseFlowElementPanel> allElementList = null;

	private List<JecnBaseFigurePanel> allFigureList = null;

	/** 操作前数据 */
	private static JecnUndoRedoData undoData = null;
	/** 操作后数据 */
	private static JecnUndoRedoData redoData = null;
	/** 选中区域X方向最小值 */
	private int minX = 0;
	/** 选中区域Y方向最小值 */
	private int minY = 0;

	public JecnKeyHandler(JecnDrawDesktopPane desktopPane) {
		this.desktopPane = desktopPane;
	}

	/**
	 * 
	 * 释放事件
	 * 
	 */
	public void keyReleased(KeyEvent e) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		clearMouseWheelListener();
		keyReleased();
	}

	// 清除 鼠标滚轮事件
	private void clearMouseWheelListener() {
		for (MouseWheelListener mouseWheelListener : desktopPane.getMouseWheelListeners()) {
			desktopPane.removeMouseWheelListener(mouseWheelListener);
		}
	}

	/**
	 * 鼠标释放
	 * 
	 */
	public void keyReleased() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		if (mouseDragged && releasedFlag) {
			mouseDragged = false;
			releasedFlag = false;
			// 操作前数据
			redoData = new JecnUndoRedoData();
			// 记录撤销恢复数据
			redoData.recodeFlowElement(allElementList);
			workflow.getCurrentSelectElement();
			// 撤销恢复数据处理
			JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
			workflow.getUndoRedoManager().addEdit(undoProcess);

			// 重新设置角色移动
			workflow.removeAddRoleMove();

			allElementList.clear();
		}
	}

	/**
	 * 
	 * 按下事件
	 * 
	 */
	public void keyPressed(KeyEvent e) {
		if (desktopPane.getJecnTempFigurePanel() != null) {// 存在虚拟边框不允许拖动
			return;
		}

		// **************清除提示信息**************//
		// 解决活动提示信息消除不了情况
		ToolTipManager.sharedInstance().mouseExited(
				new MouseEvent(desktopPane, e.getID(), System.currentTimeMillis(), e.getModifiers(), 0, 0, 1, false));
		// **************清除提示信息**************//

		// **************清除快捷添加图形显示小三角形**************//
		// 清除快捷添加图形显示小三角形
		JecnDrawMainPanel.getMainPanel().interruptLinkFigure();
		// **************清除快捷添加图形显示小三角形**************//

		// 获取键盘key值
		int keyCode = e.getKeyCode();
		if (keyCode != KeyEvent.VK_UP && keyCode != KeyEvent.VK_DOWN && keyCode != KeyEvent.VK_LEFT
				&& keyCode != KeyEvent.VK_RIGHT && keyCode != KeyEvent.VK_CONTROL) {// 键盘按键不为方向键时return
			return;
		}
		// 释放标识：第一次进入或释放后都为true
		if (!releasedFlag) {
			// 鼠标移动前记录拖动前集合
			keyPressedPro();
			releasedFlag = true;

			// 操作前数据
			undoData = new JecnUndoRedoData();
			// 记录撤销恢复数据
			undoData.recodeFlowElement(allElementList);
			// 得到画板的选中区域的极值
			JecnSelectFigureXY figureXY = JecnWorkflowUtil.getSelectFigureXY(desktopPane.getCurrentSelectElement());
			// 选中区域X方向最小值
			minX = figureXY.getMinX();
			// 选中区域Y方向最小值
			minY = figureXY.getMinY();
		}

		// 获取当前倍数下移动像素点
		moveDiff = getMoveNewDiff();

		if (keyCode == KeyEvent.VK_UP) {// 键盘方向键上
			moveFigure(0, -moveDiff);
			e.consume();
		}
		if (keyCode == KeyEvent.VK_DOWN) {// 键盘方向键下
			moveFigure(0, moveDiff);
			e.consume();
		}
		if (keyCode == KeyEvent.VK_LEFT) {// 键盘方向键左
			moveFigure(-moveDiff, 0);
			e.consume();
		}
		if (keyCode == KeyEvent.VK_RIGHT) {// 键盘方向键右
			moveFigure(moveDiff, 0);
			e.consume();
		}
		if (keyCode == KeyEvent.VK_CONTROL) {// 键盘 CONTROL
			zoomIn(desktopPane);
			e.consume();
		}
	}

	/**
	 * 按住 ctrl 加滚轮 放大缩小 流程图
	 * 
	 * @param desktopPane
	 * @param e
	 */
	private void zoomIn(JecnDrawDesktopPane desktopPane) {
		clearMouseWheelListener();
		desktopPane.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				switch (e.getScrollType()) {
				case MouseWheelEvent.WHEEL_UNIT_SCROLL:
					if (e.getWheelRotation() == 1) {// 缩小
						zoomInWork(ToolBarElemType.oftenZoomSmall,  e);
					}
					if (e.getWheelRotation() == -1) {// 放大
						zoomInWork(ToolBarElemType.oftenZoomBig, e);
					}
					break;
				default:
					break;
				}

			}
		});

	}

	private void zoomInWork(ToolBarElemType toolBarElemType, MouseWheelEvent e) {
		if (desktopPane != null) {
			// 修复  释放 CTRL 键 依然可以 放大缩小 BUG
			boolean isCtrl =((e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0) ? true : false;
			if(!isCtrl){
				clearMouseWheelListener();
				return;
			}
			switch (toolBarElemType) {
			case oftenZoomBig: // 放大
				desktopPane.getWorkflowZoomProcess().zoomBtnActionProcess(0.25);
				break;
			case oftenZoomSmall: // 缩小
				desktopPane.getWorkflowZoomProcess().zoomBtnActionProcess(-0.25);
			default:
				break;
			}
		}
	}

	/**
	 * 
	 * 上、下、左、右移动选中图片
	 * 
	 * @param list
	 *            List<JecnBaseFlowElementPanel>选中的流程元素集合
	 * @param scrollX
	 * @param scrollY
	 */
	private void moveFigure(int scrollX, int scrollY) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (allElementList == null || allElementList.size() == 0) {
			releasedFlag = false;
			return;
		}
		if (minX + scrollX < 0 || minY + scrollY < 0) {// 拖动到边界处理
			// 鼠标释放 执行撤销恢复
			keyReleased();
			releasedFlag = false;
			return;
		} else {
			minX = minX + scrollX;
			minY = minY + scrollY;
		}

		// 图形虚拟坐标点
		Point temPoint = new Point(scrollX, scrollY);
		for (JecnBaseFlowElementPanel elementPanel : allElementList) {
			if (elementPanel instanceof JecnBaseFigurePanel) {
				JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) elementPanel;
				// 获取拖动时生成的虚拟图形
				JecnTempFigureBean tempFigureBean = curFigure.getTempFigureBean();
				if (isBound(curFigure.getLocation(), scrollX, scrollY)) {
					return;
				}
				// 如果是注释框
				if (curFigure instanceof CommentText) {
					CommentText commentText = (CommentText) curFigure;
					// 小线段位移算法
					commentText.isMove();
				}

				// 设置图形位置点和大小
				tempFigureBean.reSetTempFigureBean(new Point(curFigure.getLocation().x + scrollX, curFigure
						.getLocation().y
						+ scrollY), curFigure.getWidth(), curFigure.getHeight());
				// 拖动图形重设图形位置点、大小
				curFigure.moveFigureResetLocation();
			} else {
				if (elementPanel instanceof JecnBaseDividingLinePanel) {
					JecnBaseDividingLinePanel linePanel = (JecnBaseDividingLinePanel) elementPanel;
					if (isBound(linePanel.getStartPoint(), scrollX, scrollY)
							|| isBound(linePanel.getEndPoint(), scrollX, scrollY)) {
						return;
					}
					if (linePanel instanceof DividingLine) {
						// 获取分割线虚拟图形对象
						TempDividingLine tempDividing = linePanel.createTempLinePanel();
						tempDividing.setStartPoint(linePanel.getStartPoint());
						tempDividing.setEndPoint(linePanel.getEndPoint());
						// 拖动分割线端点到达边界处理
						tempDividing.draggedTempLinePoint(temPoint);
					} else {
						JecnTempFigureBean tempFigureBean = linePanel.getTempFigureBean();

						// 获取当前端点坐标
						Point startPoint = linePanel.getStartPoint();
						Point endPoint = linePanel.getEndPoint();
						// 位置点X坐标
						int localX = getMinIntValue(startPoint.x, endPoint.x);
						// 位置点Y坐标
						int localY = getMinIntValue(startPoint.y, endPoint.y);
						// 设置图形位置点和大小
						tempFigureBean.reSetTempFigureBean(new Point(localX + scrollX, localY + scrollY), linePanel
								.getWidth(), linePanel.getHeight());
					}

					// 拖动分割线重置分割线开始点和结束点，记录分割线原始大小
					linePanel.moveLineResetPoint();
				}
			}
		}
		mouseDragged = true;
		// 重画图形相关的连接线
		JecnDraggedFiguresPaintLine.paintLineByFigures(allFigureList);
		desktopPane.updateUI();
	}

	private int getMinIntValue(int value1, int value2) {
		return value1 < value2 ? value1 : value2;
	}

	/**
	 * 拖动图形位置点是否到达边界处理
	 * 
	 * @param localPoint
	 *            图形元素位置点
	 * @param scrollX
	 *            图形横向位移
	 * @param scrollY
	 *            图形纵向位移
	 * @return
	 */
	public boolean isBound(Point localPoint, int scrollX, int scrollY) {
		if (localPoint.x + scrollX > 0 && localPoint.y + scrollY < 0) {
			return true;
		} else if (localPoint.x + scrollX < 0 && localPoint.y + scrollY > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 根据放大缩小倍数获取移动像素点：
	 * 
	 * 0<=moveDiff<=1:1 <br>
	 * 1<moveDiff<=2:2 <br>
	 * 2<moveDiff<=3:3 <br>
	 * 3<moveDiff:4 <br>
	 * 
	 * @return 1,2,3,4
	 */
	private int getMoveNewDiff() {
		double scale = desktopPane.getWorkflowScale();
		if (scale == 1.0 || scale == 2.0 || scale == 3.0 || scale == 4.0) {
			return (int) scale;
		}
		return ((int) scale) + 1;
	}

	/**
	 * 鼠标移动前记录拖动前集合
	 * 
	 */
	private void keyPressedPro() {
		if (desktopPane == null) {
			return;
		}
		// 获取面板选中流程元素集合
		List<JecnBaseFlowElementPanel> listPanel = desktopPane.getCurrentSelectElement();

		// 记录选中的List的集合
		allFigureList = new ArrayList<JecnBaseFigurePanel>();
		// 记录所有分割线集合
		allElementList = new ArrayList<JecnBaseFlowElementPanel>();

		// 流程架构，拖动连线时，虚拟图形添加到选中元素集合
		List<JecnBaseFlowElementPanel> addSelectList = new ArrayList<JecnBaseFlowElementPanel>();
		for (JecnBaseFlowElementPanel flowElementPanel : listPanel) {
			if (flowElementPanel instanceof JecnBaseFigurePanel) {
				JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) flowElementPanel;
				allElementList.add(curFigure);
				// 记录所有图形元素
				allFigureList.add(curFigure);
			} else if (flowElementPanel instanceof JecnBaseDividingLinePanel) {
				allElementList.add((JecnBaseDividingLinePanel) flowElementPanel);
			} else {
				// 流程架构，连接线可不连接元素，移动
				addSelectList.addAll(addManLineFigure(allFigureList, flowElementPanel));
			}
		}

		// 添加选中元素集合, 去重复
		for (JecnBaseFlowElementPanel jecnBaseFlowElementPanel : addSelectList) {
			if (listPanel.contains(jecnBaseFlowElementPanel)) {
				continue;
			}
			listPanel.add(jecnBaseFlowElementPanel);
		}

		// 得到图形相关所有连接线
		Set<JecnBaseManhattanLinePanel> listBaseLines = JecnDraggedFiguresPaintLine.getLineListByFigure(allFigureList);

		if (listBaseLines == null || listBaseLines.size() == 0) {
			return;
		}
		for (JecnBaseManhattanLinePanel jecnBaseManhattanLinePanel : listBaseLines) {
			allElementList.add(jecnBaseManhattanLinePanel);
		}
	}

	/**
	 * 判断是流程架构，连接线可不连接图形存在
	 * 
	 * @param allElementList
	 * @param flowElementPanel
	 */
	public static List<JecnBaseFlowElementPanel> addManLineFigure(List<JecnBaseFigurePanel> allElementList,
			JecnBaseFlowElementPanel flowElementPanel) {
		List<JecnBaseFlowElementPanel> addSelectList = new ArrayList<JecnBaseFlowElementPanel>();
		if (!JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure()) {
			return addSelectList;
		}
		if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {
			JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) flowElementPanel;
			if (!(linePanel.getStartFigure() instanceof MapLineFigure && linePanel.getEndFigure() instanceof MapLineFigure)) {
				return addSelectList;
			}
			if (!allElementList.contains(linePanel.getStartFigure())) {
				allElementList.add(linePanel.getStartFigure());
				addSelectList.add(linePanel.getStartFigure());
			}
			if (!allElementList.contains(linePanel.getEndFigure())) {
				allElementList.add(linePanel.getEndFigure());
				addSelectList.add(linePanel.getEndFigure());
			}
		}
		return addSelectList;
	}

	public List<JecnBaseFlowElementPanel> getAllElementList() {
		return allElementList;
	}

	public void setAllElementList(List<JecnBaseFlowElementPanel> allElementList) {
		this.allElementList = allElementList;
	}

	public boolean isMouseDragged() {
		return mouseDragged;
	}

	public void setMouseDragged(boolean mouseDragged) {
		this.mouseDragged = mouseDragged;
	}

	public boolean isReleasedFlag() {
		return releasedFlag;
	}

	public void setReleasedFlag(boolean releasedFlag) {
		this.releasedFlag = releasedFlag;
	}
}
