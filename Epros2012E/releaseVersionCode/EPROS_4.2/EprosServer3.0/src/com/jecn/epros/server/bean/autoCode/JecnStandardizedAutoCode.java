package com.jecn.epros.server.bean.autoCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 自动编号- 编号规则
 * 
 * @author ZXH
 * 
 */
public class JecnStandardizedAutoCode implements Serializable {
	private String guid;
	/** 当前编码规则流水号计数器 */
	private int codeTotal = 0;
	private Date createTime;
	private Long createPersonId;
	/** 相关ID */
	private Long relatedId;

	private int relatedType;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public int getCodeTotal() {
		return codeTotal;
	}

	public void setCodeTotal(int codeTotal) {
		this.codeTotal = codeTotal;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public int getRelatedType() {
		return relatedType;
	}

	public void setRelatedType(int relatedType) {
		this.relatedType = relatedType;
	}
}
