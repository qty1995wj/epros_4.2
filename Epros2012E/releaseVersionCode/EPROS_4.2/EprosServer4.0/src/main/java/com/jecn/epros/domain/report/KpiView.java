package com.jecn.epros.domain.report;

import com.jecn.epros.domain.process.kpi.KPIValuesBean;

import java.util.List;

/**
 * 流程kpi跟踪表使用显示图
 *
 * @author user
 */
public class KpiView {

    private Long flowId;
    private String flowName;
    /**
     * 流程架构或者部门的id
     **/
    private Long id;
    /**
     * 流程架构或者部门的name
     **/
    private String name;
    /**
     * 0部门 1流程架构
     **/
    private Integer type;

    private Long kpiId;
    private String kpiName;
    private String kpiTarget;
    /**
     * kpi的值
     **/
    private List<KPIValuesBean> kpiValues;


    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getKpiId() {
        return kpiId;
    }

    public void setKpiId(Long kpiId) {
        this.kpiId = kpiId;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public String getKpiTarget() {
        return kpiTarget;
    }

    public void setKpiTarget(String kpiTarget) {
        this.kpiTarget = kpiTarget;
    }

    public List<KPIValuesBean> getKpiValues() {
        return kpiValues;
    }

    public void setKpiValues(List<KPIValuesBean> kpiValues) {
        this.kpiValues = kpiValues;
    }


}
