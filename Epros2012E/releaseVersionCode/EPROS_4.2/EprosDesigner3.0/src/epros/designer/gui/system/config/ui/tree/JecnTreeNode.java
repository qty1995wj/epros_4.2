package epros.designer.gui.system.config.ui.tree;

import javax.swing.tree.DefaultMutableTreeNode;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;

/**
 * 
 * 树节点
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTreeNode extends DefaultMutableTreeNode {
	/** 节点名称 */
	private String nodeName = null;
	/** 节点对应的数据对象 */
	private JecnConfigTypeDesgBean configTypeDesgBean;

	public JecnTreeNode(String name, JecnConfigTypeDesgBean configTypeDesgBean) {
		super(name);
		if (name == null || configTypeDesgBean == null) {
			throw new IllegalArgumentException("JecnTreeNode is error");
		}
		this.nodeName = name;
		this.configTypeDesgBean = configTypeDesgBean;
	}

	public JecnTreeNode(String nodeName) {
		super(nodeName);
		if (nodeName == null || "".equals(nodeName)) {
			throw new IllegalArgumentException("JecnTreeNode is error");
		}
		this.nodeName = nodeName;
	}

	public JecnConfigTypeDesgBean getConfigTypeDesgBean() {
		return configTypeDesgBean;
	}

	public String getNodeName() {
		return nodeName;
	}
}
