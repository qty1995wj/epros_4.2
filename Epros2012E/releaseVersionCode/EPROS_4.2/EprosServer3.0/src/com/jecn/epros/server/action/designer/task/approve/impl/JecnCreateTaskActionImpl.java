package com.jecn.epros.server.action.designer.task.approve.impl;

import com.jecn.epros.server.action.designer.task.approve.IJecnCreateTaskAction;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;

public class JecnCreateTaskActionImpl implements IJecnCreateTaskAction {

	private IJecnBaseTaskService abstractTaskService;

	/**
	 * 
	 * 创建文件任务
	 * 
	 */
	@Override
	public void createPRFTask(JecnTempCreateTaskBean tempCreateTaskBean)
			throws Exception {
		if (tempCreateTaskBean == null) {
			throw new IllegalArgumentException("创建任务失败！");
		}
		abstractTaskService.createPRFTask(tempCreateTaskBean);
	}

	public IJecnBaseTaskService getAbstractTaskService() {
		return abstractTaskService;
	}

	public void setAbstractTaskService(
			IJecnBaseTaskService abstractTaskService) {
		this.abstractTaskService = abstractTaskService;
	}
}
