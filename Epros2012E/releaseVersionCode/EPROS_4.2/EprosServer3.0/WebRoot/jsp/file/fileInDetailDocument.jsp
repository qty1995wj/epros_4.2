<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% response.setHeader("Pragma","No-cache"); response.setHeader("Cache-Control","no-cache");%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><s:text name="theControlSystemOfDetailedInformation" />
		</title>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
	</head>

	<body>
		<div id="div">
			<div id="fileInDetailDocument"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; float: left;width: 100%;">
				<table align="left" width="98%">
					<tr>
						<td width="140px;" class="td_dashed">
						    <img src="${basePath}images/common/triangle.gif" style="vertical-align:middle;">
							<!-- 版本号： -->
							<s:text name="versionNumberC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
							<s:property value="taskHistoryNew.versionId" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
						    <img src="${basePath}images/common/triangle.gif" style="vertical-align:middle;">
							<!-- 拟稿人： -->
							<s:text name="peopleMakeADraftC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
							<s:property value="taskHistoryNew.draftPerson" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
						    <img src="${basePath}images/common/triangle.gif" style="vertical-align:middle;">
							<!-- 开始时间： -->
							<s:text name="startTimeC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
							<s:property value="taskHistoryNew.startTime" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
						    <img src="${basePath}images/common/triangle.gif" style="vertical-align:middle;">
							<!-- 结束时间： -->
							<s:text name="endTimeC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
							<s:property value="taskHistoryNew.endTime" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
						    <img src="${basePath}images/common/triangle.gif" style="vertical-align:middle;">
							<!-- 返工次数： -->
							<s:text name="reworkNumberC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
							<s:property value="taskHistoryNew.approveCount" />
						</td>
					</tr>
					<s:iterator value="taskHistoryNew.listJecnTaskHistoryFollow">
						<tr>
							<td class="td_dashed">
							    <img src="${basePath}images/common/triangle.gif" style="vertical-align:middle;">
							    <s:property value="appellation" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
								<s:property value="name" />
							</td>
						</tr>
					</s:iterator>
					<tr>
						<td class="td_dashed">
						    <img src="${basePath}images/common/triangle.gif" style="vertical-align:middle;">
							<!-- 发布时间： -->
							<s:text name="releaseTimeC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
						   <s:date name="taskHistoryNew.publishDate" format="yyyy-MM-dd" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
						    <img src="${basePath}images/common/triangle.gif" style="vertical-align:middle;">
							<!-- 变更说明： -->
							<s:text name="changeThatC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
						
							<s:property value="taskHistoryNew.modifyExplain" escape="false"/>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>