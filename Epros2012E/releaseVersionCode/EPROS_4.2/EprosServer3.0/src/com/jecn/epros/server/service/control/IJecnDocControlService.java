package com.jecn.epros.server.service.control;

import java.util.List;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.IBaseService;

public interface IJecnDocControlService extends
		IBaseService<JecnTaskHistoryNew, Long> {
	/**
	 * 发布任务，创建文控版本信息日志
	 * 
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @param refId
	 *            关联ID
	 * @throws Exception
	 */
	public void saveJecnTaskHistoryNew(JecnTaskHistoryNew historyNew)
			throws Exception;

	/**
	 * 
	 * 编辑文控信息
	 * 
	 * @param JecnTaskHistoryNew
	 * @return true:版本号已存在
	 * @throws Exception
	 */
	public boolean updateJecnTaskHistoryNew(JecnTaskHistoryNew historyNew,
			String versionId) throws Exception;

	/**
	 * 删除文控信息
	 * 
	 * @param docId
	 *            文控主键ID
	 * @throws Exception
	 * @throws Exception
	 */
	public void deleteJecnTaskHistoryNew(List<Long> listDocId) throws Exception;

	/**
	 * 查阅文控信息
	 * 
	 * @param docId
	 *            文控主键ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @param refId
	 *            关联ID
	 * @throws Exception
	 */
	public FileOpenBean viewJecnTaskHistoryNew(Long docId,Long ruleId) throws Exception;

	/**
	 * 
	 * 根据流程ID获取文控信息版本号集合
	 * 
	 * @param flowId
	 *            流程ID
	 * @return List<String>文控信息版本号集合
	 * @throws Exception
	 */
	public List<String> findVersionListByFlowId(Long flowId, int type)
			throws Exception;

	/**
	 * 文件文控类型和关联ID获取文控信息版本记录
	 * 
	 * @param refId
	 *            关联ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @return List<JecnTaskHistoryNew>
	 * @throws Exception
	 */
	public List<JecnTaskHistoryNew> getJecnTaskHistoryNewList(Long refId,
			int type) throws Exception;

	/**
	 * 根据文控信息主键ID获取从表信息
	 * 
	 * @param historyId
	 *            文控主键ID
	 * @return 文控信息对应的各阶段审批人信息
	 * @throws Exception
	 */
	public List<JecnTaskHistoryFollow> getJecnTaskHistoryFollowList(
			Long historyId) throws Exception;

	/**
	 * 文件基本信息是否必填验证
	 * 
	 * @param flowId
	 *            流程ID
	 * @return List<String>
	 */
	public List<String> getErrorList(Long flowId,int languageType) throws Exception;

	public List<String> findVersionListByFlowIdNoPage(Long id, int type);
}
