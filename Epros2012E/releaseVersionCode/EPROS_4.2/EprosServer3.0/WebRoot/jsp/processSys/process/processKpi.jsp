<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="com.jecn.epros.server.webBean.WebLoginBean"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript"
			src="${basePath}js/processSys/processKPI.js">
	
</script>
		<script type="text/javascript"
			src="${basePath}js/My97DatePicker/WdatePicker.js">
	
</script>
<script type="text/javascript"
			src="${basePath}jquery/ajaxfileupload.js">
	
</script>
	</head>
	<body>
		<div id="div" class="div" style="height: 100%;overflow-y:scroll;" >
			<div id="search" class="divBorder">
				<div id="processKpiText" style="overflow: hidden;">
					<table width="100%">
						<tr>
							<td width="10%" height="25px" align="right">
								<!-- 流程名称： -->
								<s:text name="processNameC"></s:text>
							</td>
							<td width="40%" align="left">
								${tempProcessKpiBean.flowName }
							</td>

							<td></td>
							<td></td>
						</tr>
						<tr>
							<td align="right">
								<!-- KPI： -->
								<s:text name="KPIC"></s:text>
							</td>
							<td>
								<select id="kpiType" name="kpiType" size="1" style="width: 70%"
									onchange="kpiNameChange();">
									<option value="" selected>
										--
										<s:text name="pleaseSelect" />
										--
									</option>
									<s:iterator value="tempProcessKpiBean.listFlowKpiName"
										status="st1" id="flowKpiNames">
										<option value='<s:property value="kpiHorizontal"/>'>
											<s:property value="kpiName" />
										</option>
									</s:iterator>
								</select>

								<select id="kpiNameId" style="display: none; width: 90%">
									<option value="" selected>
										--
										<s:text name="systemManager.Select" />
										--
									</option>
									<s:iterator value="tempProcessKpiBean.kpiNameIds">
										<option>
											<s:property />
										</option>
									</s:iterator>
								</select>
							</td>

							<td width="10%" align="right">
								<!-- 时间段: -->
								<s:text name="timeC" />
							</td>
							<td width="40%">
								<s:textfield readonly="true" cssClass="Wdate" id="startTime"
									cssStyle="border-color: #7F9DB9;width:32%;" name="startTime"
									onclick="selectDate(true);" theme="simple">
									<s:param name="value">
										<s:date name="startTime"  format="yyyy-MM" />
									</s:param>
								</s:textfield>
								<select id="startKpiWeek" name="startKpiWeek"
									style="display: none;">
								</select>

								<select id="startKpiSeason" name="kpiSeason"
									style="display: none;">
								</select>
								-
								<s:textfield readonly="true" cssClass="Wdate" id="endTime"
									cssStyle="border-color: #7F9DB9;width:32%;"
									onclick="selectDate(false);" theme="simple" />
								<select id="endKpiWeek" name="endKpiWeek" style="display: none;">
								</select>

								<select id="endKpiSeason" name="endKpiSeason"
									style="display: none;">
								</select>
							</td>

						</tr>
					</table>
				</div>
				<div id="searchBut" align="center">
					<a style="cursor: pointer;" onclick="searchKPI();"><img
							src="${basePath}images/iParticipateInProcess/search.gif" /> </a>
					<a style="cursor: pointer;" onclick="cancelButton();"><img
							src="${basePath}images/iParticipateInProcess/reset.gif" /> </a>
				</div>
			</div>
			<div style="display: none;">
				<s:form name="form" id="form" action="" method="post" >
					<table style="margin: 0px; padding: 0px;display:none;">
						<tr>
							<td style="height: 10px;">
								<input  id="kpiHorType"
									name="searchKpiBean.kpiHorType" value="">
								<input  id="flowId" name="searchKpiBean.flowId"
									value="${tempProcessKpiBean.flowId}" />
								<input  id="flowIdDaoru" name="flowId"
									value="${tempProcessKpiBean.flowId}" />
								<input  id="startTime1"
									name="searchKpiBean.startTime" value="">
								<input  id="endTime1" name="searchKpiBean.endTime"
									value="">
								<input  id="kpiId" name="searchKpiBean.kpiId"
									value="">
							</td>
						</tr>
					</table>
				</s:form>
				
			</div>
			
			<div style="overflow-x:scroll;overflow-y:hidden;width:100%;display:none" id="flow_kpi" >
				<form action="">
					<table width="100%"  style="margin-right: 12px;margin-left: 12px;" border="0" cellspacing="0" cellpadding="0">
						<!-- 如果非管理员或者数据提供者则不显示 -->
						<tr id="kpiInputOutPut" style="visibility: hidden;">
							<td width="350px"  style="margin: 0px;  padding: 0px;">
								<div id="optionDiv">
									<s:form theme="simple" method="post" enctype="multipart/form-data">
										<s:file id="excelFilePath" name="upload" theme="simple" cssStyle="width:210px"/>
										<a href="#" onclick="KPIinput();"> <img
												src="${basePath}images/flowkpi/inpImg.gif"
												style="vertical-align: bottom;" />
										</a>
										<a href="#" onclick="KPIOutPut();"> <img
												src="${basePath}images/flowkpi/expImg.gif"
												style="vertical-align: bottom;" />
										</a>
									</s:form>
								</div>
							</td>
						</tr>
						<tr>
							<td width="350px"  valign="top">
								<div 
									style="margin: 0px;  padding: 0px;"
									id="kpiTableId">
								</div>
							</td>
							<td valign="top">
								<div
									id="kpiTableImage">
									<img id="chart" src="" >
								</div>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</body>
</html>