package com.jecn.epros.server.bean.process.temp;

/**
 * 流程KPI搜索数据Bean
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Jan 17, 2013 时间：2:18:06 PM
 */
public class TempSearchKpiBean {
	/** 流程ID */
	private Long flowId;
	/** KPI名称主表ID */
	private Long kpiId;
	/** 流程KPI名称 */
	private String kpiName;
	/** KPI主表横坐标值 */
	private String kpiHorType;
	/** KPI主表纵坐标值 */
	private String kpiVerType;
	/** KPI主表纵坐标值 */
	private String startTime;
	/** KPI主表纵坐标值 */
	private String endTime;

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getKpiId() {
		return kpiId;
	}

	public void setKpiId(Long kpiId) {
		this.kpiId = kpiId;
	}

	public String getKpiName() {
		return kpiName;
	}

	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getKpiHorType() {
		return kpiHorType;
	}

	public void setKpiHorType(String kpiHorType) {
		this.kpiHorType = kpiHorType;
	}

	public String getKpiVerType() {
		return kpiVerType;
	}

	public void setKpiVerType(String kpiVerType) {
		this.kpiVerType = kpiVerType;
	}
}
