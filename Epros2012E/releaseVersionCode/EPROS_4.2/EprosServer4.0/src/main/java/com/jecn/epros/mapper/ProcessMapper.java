package com.jecn.epros.mapper;

import com.jecn.epros.domain.Resource;
import com.jecn.epros.domain.common.BaseBean;
import com.jecn.epros.domain.inventory.FlowInventoryBean;
import com.jecn.epros.domain.process.*;
import com.jecn.epros.domain.process.inout.JecnFigureInoutBase;
import com.jecn.epros.domain.process.inout.JecnFlowInoutBean;
import com.jecn.epros.domain.process.kpi.FlowKPIBean;
import com.jecn.epros.domain.process.kpi.FlowKpiName;
import com.jecn.epros.domain.process.kpi.KPIValuesBean;
import com.jecn.epros.domain.process.processFile.FlowDriver;
import com.jecn.epros.domain.process.processFile.InterfaceDescription;
import com.jecn.epros.domain.process.processFile.JecnActiveOnLineBean;
import com.jecn.epros.domain.process.processFile.TermDefinition;
import com.jecn.epros.domain.process.relatedFile.*;
import com.jecn.epros.service.web.bean.FlowWebBen;
import com.jecn.epros.sqlprovider.ProcessSqlProvider;
import com.jecn.epros.sqlprovider.server3.SqlConstant;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface ProcessMapper {

    /**
     * 流程树 展开，系统管理员权限查询
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findChildFlowsAdminSql")
    @ResultMap("mapper.Process.processTreeBeans")
    public List<TreeNode> findChildFlowsAdmin(Map<String, Object> map);


    @Select("select * from JECN_FLOW_BASIC_INFO where FLOW_ID=#{flowId}")
    @ResultMap("mapper.Process.basicInfo")
    public FlowBasicInfo getProposeBasicInfo(Long flowId);

    /**
     * 获取流程所有活动的线上信息集合
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findItSystemByActiveId")
    public List<JecnActiveOnLineBean> findItSystemByActiveId(Map<String, Object> map);


    /**
     * 登陆人员的所在的岗位是否为该流程的责任人
     *
     * @param paramMap
     * @return
     */
    @Select("select count(1)"
            + "    from jecn_flow_basic_info t,Jecn_Flow_Org_Image jfoi,jecn_flow_org jfo,JECN_USER_POSITION_RELATED jupr"
            + "    where jupr.people_id=#{loginUserId} and t.flow_id=#{relatedId}  and jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 and"
            + "    t.type_res_people=1 and jfoi.figure_id = t.res_people_id")
    public int peoplePositionIsFlowResPeople(Map<String, Object> paramMap);

    public ProcessBaseInfoBean findBaseInfoBean(Map<String, Object> map);

    /**
     * 获取流程基本信息（编号，密级，名称）
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowStructureInfoSql")
    @ResultMap("mapper.Process.flowStructureWithTable")
    public FlowStructure findFlowStructureInfo(Map<String, Object> map);

    /**
     * 获取流程基本信息（编号，密级，名称）
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowBasicInfoSql")
    public Map<String, Object> findFlowBasicInfo(Map<String, Object> map);

    /**
     * 流程类别名称 TYPE_NAME
     *
     * @param typeId
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowTypeSql")
    public String findFlowType(Long typeId);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowResOrgSql")
    public Map<String, Object> findFlowResOrg(Map<String, Object> map);

    /**
     * 相关流程
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findRelatedProcessSql")
    @ResultMap("mapper.Process.relatedProcess")
    public List<RelatedProcess> findRelatedProcess(Map<String, Object> map);

    /**
     * 相关标准
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findRelatedStandardSql")
    @ResultMap("mapper.Process.relatedStandard")
    public List<RelatedStandard> findRelatedStandard(Map<String, Object> map);

    /**
     * 相关标准 文控
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findRelatedStandardHistorySql")
    @ResultMap("mapper.Process.relatedStandard")
    public List<RelatedStandard> findRelatedStandardHistory(Map<String, Object> map);


    /**
     * 相关制度
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findRelatedRuleSql")
    @ResultMap("mapper.Process.relatedRule")
    public List<RelatedRule> findRelatedRule(Map<String, Object> map);

    /**
     * 相关风险
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findRelatedRiskSql")
    @ResultMap("mapper.Process.relatedRisk")
    public List<RelatedRisk> findRelatedRisk(Map<String, Object> map);

    /**
     * 相关风险 文控
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findRelatedRiskHistorySql")
    @ResultMap("mapper.Process.relatedRisk")
    public List<RelatedRisk> findRelatedRiskHistory(Map<String, Object> map);

    /**
     * 获取流程对应的KPI 名称
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findKpiNamesSql")
    @ResultMap("mapper.Process.kpiBean")
    public List<FlowKPIBean> findKpiNames(Map<String, Object> map);

    /**
     * 流程KPI值
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findKpiValuesByDateSql")
    @ResultMap("mapper.Process.kpiValues")
    public List<KPIValuesBean> findKpiValuesByDate(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findKpiNameByIdSql")
    @ResultMap("mapper.Process.kpiName")
    public FlowKpiName findKpiNameById(Map<String, Object> map);

    @InsertProvider(type = ProcessSqlProvider.class, method = "insertKpiSql")
    public void insertKpi(Map<String, Object> map);

    @UpdateProvider(type = ProcessSqlProvider.class, method = "updateKpiSql")
    public void updateKpi(KPIValuesBean kpiValue);

    @Select("select * from JECN_FLOW_STRUCTURE_T where FLOW_ID=#{rid}")
    @ResultMap("mapper.Process.flowStructureTWithTable")
    public FlowStructureT getFlowStructureT(Long rid);

    //文控
    @Select("select * from JECN_FLOW_STRUCTURE_H where FLOW_ID=#{flowId} AND HISTORY_ID = #{historyId}")
    @ResultMap("mapper.Process.flowStructureTWithTable")
    public FlowStructureT getFlowStructureH(Map<String, Object> map);

    @Select("select * from JECN_FLOW_STRUCTURE where FLOW_ID=#{rid}")
    @ResultMap("mapper.Process.flowStructureWithTable")
    public FlowStructure getFlowStructure(Long rid);

    @Select("select * from JECN_FLOW_STRUCTURE_H where FLOW_ID=#{flowId} AND HISTORY_ID=#{historyId}")
    @ResultMap("mapper.Process.flowStructureWithTable")
    public FlowStructure getFlowStructureHistory(Map<String, Object> map);

    @Select("select * from JECN_FLOW_BASIC_INFO_T where FLOW_ID=#{flowId}")
    @ResultMap("mapper.Process.flowBasicInfoTWithTable")
    public FlowBasicInfoT getProcessBasicInfoT(Long flowId);

    //查询文控
    @Select("select * from JECN_FLOW_BASIC_INFO_H where FLOW_ID=#{flowId} AND HISTORY_ID=#{historyId}")
    @ResultMap("mapper.Process.flowBasicInfoTWithTable")
    public FlowBasicInfoT getProcessBasicInfoH(Map<String, Object> map);

    @Select("select * from JECN_MAIN_FLOW_T where FLOW_ID=#{flowId}")
    @ResultMap("mapper.Process.mainFlowTWithTable")
    public MainFlowT getMainFlowT(Long flowId);

    //文控
    @Select("select * from JECN_MAIN_FLOW_H where FLOW_ID=#{flowId} AND HISTORY_ID = #{historyId}")
    @ResultMap("mapper.Process.mainFlowTWithTable")
    public MainFlowT getMainFlowH(Map<String, Object> map);

    @Select("select * from JECN_FLOW_TYPE")
    @ResultMap("mapper.Process.flowTypeWithTable")
    public List<ProceeRuleTypeBean> fetchProcessRuleTyps();

    @Update("update JECN_FLOW_STRUCTURE_T set HISTORY_ID=#{historyId},UPDATE_DATE=#{updateDate},IS_PUBLIC=#{isPublic} WHERE FLOW_ID=#{flowId}")
    public void updateFlowStructureT(FlowStructureT structureT);

    @Delete("delete from jecn_line_segment where figure_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=#{flowId})")
    public void deleteLineSegment(Map<String, Object> paramMap);

    @Delete("DELETE FROM JECN_FIGURE_FILE WHERE EXISTS (SELECT 1 FROM jecn_flow_structure_image T WHERE T.FIGURE_ID =JECN_FIGURE_FILE.FIGURE_ID  AND T.FLOW_ID=#{flowId})")
    public void deleteFigureFile(Map<String, Object> paramMap);

    @Delete("delete from flow_related_criterion where flow_id=#{flowId}")
    public void deleteFlowRelatedCriterion(Map<String, Object> paramMap);

    @Delete("delete from jecn_flow_structure_image where flow_id=#{flowId}")
    public void deleteFlowStructureImage(Map<String, Object> paramMap);

    @Delete("delete from JECN_MAIN_FLOW where flow_id=#{flowId}")
    public void deleteMainFlow(Map<String, Object> paramMap);

    @Delete("delete from jecn_access_permissions where type=0 and RELATE_ID=#{flowId}")
    public void deleteAccessPermissions(Map<String, Object> paramMap);

    @Delete("delete from JECN_ORG_ACCESS_PERMISSIONS where type=0 and RELATE_ID=#{flowId}")
    public void deleteOrgAccessPermissions(Map<String, Object> paramMap);

    @Delete("delete from JECN_GROUP_PERMISSIONS where type=0 and RELATE_ID=#{flowId}")
    public void deleteGroupPermissions(Map<String, Object> paramMap);

    @Delete("delete from JECN_FLOW_STRUCTURE where flow_id=#{flowId}")
    public void deleteFlowStructure(Map<String, Object> paramMap);

    @Insert("insert into JECN_FIGURE_FILE(ID, FIGURE_ID, FILE_ID, FIGURE_TYPE) select ID, FIGURE_ID, FILE_ID, FIGURE_TYPE from JECN_FIGURE_FILE_T "
            + "where FIGURE_ID IN (SELECT T.FIGURE_ID FROM jecn_flow_structure_image_t T WHERE T.FLOW_ID=#{flowId})")
    public void insertFigureFile(Map<String, Object> paramMap);

    @Insert("insert into jecn_line_segment(ID,FIGURE_ID,START_X,START_Y,END_X,END_Y) "
            + "       select jlst.ID,jlst.FIGURE_ID,jlst.START_X,jlst.START_Y,jlst.END_X,jlst.END_Y"
            + "       from jecn_line_segment_t jlst,jecn_flow_structure_image_t jfsit"
            + "       where jlst.figure_id=jfsit.figure_id and jfsit.flow_id=#{flowId}")
    public void insertLineSegment(Map<String, Object> paramMap);

    @Insert("insert into jecn_flow_structure_image(FIGURE_ID, FLOW_ID, FIGURE_TYPE, FIGURE_TEXT, START_FIGURE, END_FIGURE, X_POINT, Y_POINT, WIDTH, HEIGHT, FONT_SIZE, BGCOLOR, FONTCOLOR, LINK_FLOW_ID, LINECOLOR, FONTPOSITION, HAVESHADOW, CIRCUMGYRATE, BODYLINE, BODYCOLOR, ISERECT, FIGURE_IMAGE_ID, Z_ORDER_INDEX, ACTIVITY_SHOW, ACTIVITY_ID, FONT_BODY, FRAME_BODY, FONT_TYPE, ROLE_RES, IS_3D_EFFECT, FILL_EFFECTS, LINE_THICKNESS, START_TIME, STOP_TIME, TIME_TYPE, SHADOW_COLOR,DIVIDING_X)"
            + " select FIGURE_ID, FLOW_ID, FIGURE_TYPE, FIGURE_TEXT, START_FIGURE, END_FIGURE, X_POINT, Y_POINT, WIDTH, HEIGHT, FONT_SIZE, BGCOLOR, FONTCOLOR, LINK_FLOW_ID, LINECOLOR, FONTPOSITION, HAVESHADOW, CIRCUMGYRATE, BODYLINE, BODYCOLOR, ISERECT, FIGURE_IMAGE_ID, Z_ORDER_INDEX, ACTIVITY_SHOW, ACTIVITY_ID, FONT_BODY, FRAME_BODY, FONT_TYPE, ROLE_RES, IS_3D_EFFECT, FILL_EFFECTS, LINE_THICKNESS, START_TIME, STOP_TIME, TIME_TYPE, SHADOW_COLOR,DIVIDING_X from jecn_flow_structure_image_t where flow_id=#{flowId}")
    public void insertFlowStructureImage(Map<String, Object> paramMap);

    @Insert("insert into JECN_MAIN_FLOW(FLOW_ID, FLOW_AIM, FLOW_AREA, FLOW_INPUT, FLOW_OUTPUT, FLOW_SHOW_STEP, FLOW_NOUN_DEFINE, FILE_ID)"
            + " select FLOW_ID, FLOW_AIM, FLOW_AREA, FLOW_INPUT, FLOW_OUTPUT, FLOW_SHOW_STEP, FLOW_NOUN_DEFINE,FILE_ID from JECN_MAIN_FLOW_T  where flow_id=#{flowId}")
    public void insertMainFlow(Map<String, Object> paramMap);

    @Insert("insert into jecn_access_permissions(ID, FIGURE_ID, RELATE_ID, TYPE) select ID, FIGURE_ID, RELATE_ID, TYPE from jecn_access_permissions_t  where type=0 and RELATE_ID=#{flowId}")
    public void insertAccessPermissions(Map<String, Object> paramMap);

    @Insert("insert into JECN_ORG_ACCESS_PERMISSIONS(ID, ORG_ID, RELATE_ID, TYPE) select ID, ORG_ID, RELATE_ID, TYPE from JECN_ORG_ACCESS_PERM_T where type=0 and RELATE_ID=#{flowId}")
    public void insertOrgAccessPermissions(Map<String, Object> paramMap);

    @Insert("insert into JECN_GROUP_PERMISSIONS(ID, POSTGROUP_ID, RELATE_ID, TYPE) select ID, POSTGROUP_ID, RELATE_ID, TYPE from JECN_GROUP_PERMISSIONS_T WHERE TYPE=#{type} AND RELATE_ID=#{flowId}")
    public void insertGroupPermissions(Map<String, Object> paramMap);

    @Insert("insert into JECN_FLOW_STRUCTURE(FLOW_ID, PROJECTID, FLOW_NAME, ISFLOW, PRE_FLOW_ID, FLOW_LEVEL, STR_WIDTH, STR_HEIGHT, FLOW_ID_INPUT, IS_PUBLIC, SORT_ID, DEL_STATE, PEOPLE_ID, CREATE_DATE, UPDATE_PEOPLE_ID, UPDATE_DATE, FILE_PATH, HISTORY_ID, ERROR_STATE,DATA_TYPE, OCCUPIER ,T_PATH,T_LEVEL )"
            + " select FLOW_ID, PROJECTID, FLOW_NAME, ISFLOW, PRE_FLOW_ID, FLOW_LEVEL, STR_WIDTH, STR_HEIGHT, FLOW_ID_INPUT, IS_PUBLIC, SORT_ID, DEL_STATE, PEOPLE_ID, CREATE_DATE, UPDATE_PEOPLE_ID, UPDATE_DATE, FILE_PATH, HISTORY_ID, ERROR_STATE,DATA_TYPE, OCCUPIER ,T_PATH,T_LEVEL from JECN_FLOW_STRUCTURE_T  where flow_id=#{flowId}")
    public void insertFlowStructure(Map<String, Object> paramMap);

    @Insert("insert into FLOW_RELATED_CRITERION (FLOW_CRITERON_ID, FLOW_ID, CRITERION_CLASS_ID)"
            + "SELECT FLOW_CRITERON_ID, FLOW_ID, CRITERION_CLASS_ID "
            + "FROM FLOW_RELATED_CRITERION_T WHERE FLOW_ID=#{flowId}")
    public void insertFlowRelatedCriterion(Map<String, Object> paramMap);

    @Delete("delete from jecn_templet" + "       where mode_file_id in"
            + "       (select jmf.mode_file_id from jecn_mode_file jmf,jecn_flow_structure_image jfsi where jmf.figure_id = jfsi.figure_id and jfsi.flow_id=#{flowId})")
    public void deleteTemplet(Map<String, Object> paramMap);

    @Delete("delete from jecn_mode_file where figure_id in"
            + "       (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=#{flowId})")
    public void deleteModeFile(Map<String, Object> paramMap);

    @Delete("delete from jecn_activity_file where figure_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=#{flowId})")
    public void deleteActivityFile(Map<String, Object> paramMap);

    @Delete("delete from jecn_ref_indicators where activity_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=#{flowId})")
    public void deleteRefIndicators(Map<String, Object> paramMap);

    @Delete("delete from process_station_related where figure_flow_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=#{flowId})")
    public void deleteProcessStationRelated(Map<String, Object> paramMap);

    @Delete("delete from JECN_ROLE_ACTIVE where figure_role_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=#{flowId})")
    public void deleteRoleActive(Map<String, Object> paramMap);

    @Delete("delete from JECN_ROLE_ACTIVE where figure_active_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=#{flowId})")
    public void deleteTempletTwo(Map<String, Object> paramMap);

    @Delete("DELETE FROM JECN_ACTIVE_ONLINE WHERE PROCESS_ID= #{flowId}")
    public void deleteActiveOnline(Map<String, Object> paramMap);

    @Delete("delete from JECN_CONTROL_POINT  where active_id in (select jfsi.figure_id  from jecn_flow_structure_image jfsi where jfsi.flow_id = #{flowId})")
    public void deleteControlPoint(Map<String, Object> paramMap);

    @Delete("DELETE FROM Jecn_Active_Standard WHERE EXISTS (SELECT 1 FROM JECN_FLOW_STRUCTURE_IMAGE I WHERE I.FIGURE_ID = ACTIVE_ID AND I.FLOW_ID=#{flowId})")
    public void deleteActiveStandard(Map<String, Object> paramMap);

    @Delete("delete from JECN_FLOW_STANDARD where flow_id=#{flowId}")
    public void deleteFlowStandard(Map<String, Object> paramMap);

    @Delete("delete from JECN_FLOW_RELATED_ORG where flow_id=#{flowId}")
    public void deleteFlowRelated(Map<String, Object> paramMap);

    @Delete("delete from JECN_FLOW_TOOL_RELATED where flow_id=#{flowId}")
    public void deleteFlowToolRelated(Map<String, Object> paramMap);

    @Delete("delete from JECN_FLOW_RECORD where flow_id=#{flowId}")
    public void deleteFlowRecord(Map<String, Object> paramMap);

    @DeleteProvider(type = ProcessSqlProvider.class, method = "deleteFlowKpi")
    public void deleteFlowKpi(Map<String, Object> paramMap);

    @Delete("DELETE FROM JECN_SUSTAIN_TOOL_CONN WHERE RELATED_TYPE=0 AND RELATED_ID IN(SELECT K.KPI_AND_ID FROM JECN_FLOW_KPI_NAME K where K.FLOW_ID=#{flowId})")
    public void deleteSustainToolConn(Map<String, Object> paramMap);

    @Delete("delete from JECN_FLOW_KPI_NAME where flow_id=#{flowId}")
    public void deleteFlowKpiName(Map<String, Object> paramMap);

    @Delete("delete from JECN_FLOW_DRIVER where flow_id=#{flowId}")
    public void deleteFlowDriver(Map<String, Object> paramMap);

    @Delete("delete from JECN_FLOW_BASIC_INFO where flow_id=#{flowId}")
    public void deleteFlowBasicInfo(Map<String, Object> paramMap);

    @Delete("delete from JECN_ORG_ACCESS_PERMISSIONS where type=0 and RELATE_ID=#{flowId}")
    public void deleteOrgAccessPermission(Map<String, Object> paramMap);

    @Delete("delete from JECN_GROUP_PERMISSIONS where type=0 and RELATE_ID=#{flowId}")
    public void deleteGroupPermission(Map<String, Object> paramMap);

    @Delete("delete from JECN_TO_FROM_ACTIVE where flow_id=#{flowId}")
    public void deleteToFromActive(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_MODE_FILE_T)
    public void insertModeFileT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_ACTIVITY_FILE_T)
    public void insertActivityFileT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_REF_INDICATORST)
    public void insertRefIndicatorst(Map<String, Object> paramMap);

    @Insert(SqlConstant.PROCESS_STATION_RELATED_T)
    public void insertProcessStationRelatedT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_LINE_SEGMENT_T)
    public void insertLineSegmentT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_ROLE_ACTIVE_T)
    public void insertRoleActiveT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_FLOW_STRUCTURE_T)
    public void insertFlowStructureT(Map<String, Object> paramMap);

    @Insert(SqlConstant.FLOW_BASIC_INFO_T)
    public void insertFlowBasicInfoT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_FLOW_STANDARD_T)
    public void insertFlowStandardT(Map<String, Object> paramMap);

    @Insert(SqlConstant.FLOW_RELATED_CRITERION_T)
    public void insertRelatedCriterionT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_FLOW_RELATED_ORG_T)
    public void insertFlowRelatedOrgT(Map<String, Object> paramMap);

    @Insert(SqlConstant.Jecn_Flow_Tool_Related_T)
    public void insertFlowToolRelatedT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_FLOW_RECORD_T)
    public void insertFlowRecordT(Map<String, Object> paramMap);

    @Insert("INSERT INTO JECN_FLOW_KPI_NAME"
            + " (KPI_AND_ID,FLOW_ID,KPI_NAME,KPI_TYPE,KPI_DEFINITION,KPI_STATISTICAL_METHODS,KPI_TARGET,KPI_HORIZONTAL,KPI_VERTICAL,CREAT_TIME,KPI_TARGET_OPERATOR,KPI_DATA_METHOD,KPI_DATA_PEOPEL_ID,KPI_RELEVANCE,KPI_TARGET_TYPE,FIRST_TARGER_ID,UPDATE_TIME,CREATE_PEOPLE_ID,UPDATE_PEOPLE_ID,KPI_PURPOSE,KPI_POINT,KPI_PERIOD,KPI_EXPLAIN)"
            + " SELECT KPI_AND_ID,FLOW_ID,KPI_NAME,KPI_TYPE,KPI_DEFINITION,KPI_STATISTICAL_METHODS,KPI_TARGET,KPI_HORIZONTAL,KPI_VERTICAL,CREAT_TIME,KPI_TARGET_OPERATOR,KPI_DATA_METHOD,KPI_DATA_PEOPEL_ID,KPI_RELEVANCE,KPI_TARGET_TYPE,FIRST_TARGER_ID,UPDATE_TIME,CREATE_PEOPLE_ID,UPDATE_PEOPLE_ID,KPI_PURPOSE,KPI_POINT,KPI_PERIOD,KPI_EXPLAIN"
            + " FROM JECN_FLOW_KPI_NAME_T WHERE FLOW_ID = #{flowId}")
    public void insertFlowKpiName(Map<String, Object> paramMap);

    @Insert("INSERT INTO JECN_SUSTAIN_TOOL_CONN (ID,SUSTAIN_TOOL_ID,RELATED_ID,RELATED_TYPE,CREAT_TIME,CREATE_PEOPLE_ID,UPDATE_TIME,UPDATE_PEOPLE_ID )"
            + " SELECT ID,SUSTAIN_TOOL_ID,RELATED_ID,RELATED_TYPE,CREAT_TIME,CREATE_PEOPLE_ID,UPDATE_TIME,UPDATE_PEOPLE_ID"
            + " FROM JECN_SUSTAIN_TOOL_CONN_T TC WHERE TC.RELATED_TYPE=0 AND TC.RELATED_ID IN(SELECT K.KPI_AND_ID FROM JECN_FLOW_KPI_NAME_T K where K.FLOW_ID=#{flowId})")
    public void insertSustainToolConn(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_FLOW_DRIVERT)
    public void insertFlowDriverT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_ACCESS_PERMISSIONS_T)
    public void insertAccessPermissionsT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_ORG_ACCESS_PERM_T)
    public void insertOrgAccessPermT(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_ORG_GROUP_PERM_T)
    public void insertOrgGroupPermT(Map<String, Object> paramMap);

    @Insert(SqlConstant.ACTIVE_ONLINE)
    public void insertActiveOnline(Map<String, Object> paramMap);

    @Insert(SqlConstant.ACTIVE_CONTROL_POINT)
    public void insertControlPoint(Map<String, Object> paramMap);

    @Insert(SqlConstant.ACTIVE_STANDARD)
    public void insertActiveStandard(Map<String, Object> paramMap);

    @Insert("insert into JECN_TO_FROM_ACTIVE(TO_FROM_FIGURE_ID, ACTIVE_FIGURE_ID, FLOW_ID) select TO_FROM_FIGURE_ID, ACTIVE_FIGURE_ID, FLOW_ID from JECN_TO_FROM_ACTIVE_T where flow_id=#{flowId}")
    public void insertToFromActive(Map<String, Object> paramMap);

    @Insert(SqlConstant.JECN_FLOW_STRUCTURE_IMAGE_T)
    public void insertFlowStructureImageT(Map<String, Object> paramMap);

    @SelectProvider(type = ProcessSqlProvider.class, method = "getRelateRuleByFlowMapId")
    public List<Map<String, Object>> getRelateRuleByFlowMapId(Long relateId);

    @SelectProvider(type = ProcessSqlProvider.class, method = "getRelateRuleByFlowId")
    public List<Map<String, Object>> getRelateRuleByFlowId(Long relateId);

    @InsertProvider(type = ProcessSqlProvider.class, method = "insertTemplet")
    public void insertTemplet(Map<String, Object> paramMap);

    /**
     * 流程基本信息
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "getBasicInfoSql")
    @ResultMap("mapper.Process.flowBasicInfoWithTable")
    public FlowBasicInfo getBasicInfo(Map<String, Object> map);

    /**
     * 查询是否存在文控
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "getBasicInfoHistoryCount")
    public int getBasicInfoCount(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowStructureByMapSql")
    @ResultMap("mapper.Process.flowStructureWithTable")
    public FlowStructure findFlowStructureByMap(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "getFlowDriverSql")
    @ResultMap("mapper.Process.flowDriver")
    public FlowDriver getFlowDriver(Map<String, Object> map);

    @Select("SELECT JU.TRUE_NAME FROM JECN_USER JU,JECN_FLOW_DRIVER_EMAIL_T " +
            "T WHERE T.PEOPLE_ID = JU.PEOPLE_ID AND T.RELATED_ID =#{flowId} AND JU.ISLOCK=0")
    public List<String> findTrname(Map<String, Object> map);

    /**
     * 流程责任部门
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "getFlowDutyOrgSql")
    public Map<String, Object> getProcessDutyOrg(Map<String, Object> map);

    /**
     * 活动明细，活动基本信息
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findActivityListByFlowIdSql")
    public List<Map<String, Object>> findActivityListByFlowId(Map<String, Object> map);

    /**
     * 流程文件角色信息
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findRoleListByFlowIdSql")
    public List<Map<String, Object>> findRoleListByFlowId(Map<String, Object> map);

    /**
     * 角色活动对应关系
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findRoleRelatedActiveByFlowIdSql")
    public List<Map<String, Object>> findRoleRelatedActiveByFlowId(Map<String, Object> map);

    /**
     * 活动输入
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findActivityInputByFlowIdSql")
    public List<Map<String, Object>> findActivityInputByFlowId(Map<String, Object> map);

    /**
     * 活动输出
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findActivityOutputByFlowIdSql")
    public List<Map<String, Object>> findActivityOutputByFlowId(Map<String, Object> map);

    /**
     * 活动操作规范
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findActivityOperateByFlowIdSql")
    public List<Map<String, Object>> findActivityOperateByFlowId(Map<String, Object> map);

    /**
     * 角色相关岗位组对应岗位
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "getRoleRelatedPostGroupSql")
    public List<Map<String, Object>> getRoleRelatedPostGroup(Map<String, Object> map);

    /**
     * 角色相关岗位
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "getRoleRelatedPostSql")
    public List<Map<String, Object>> getRoleRelatedPost(Map<String, Object> map);

    /**
     * 流程相关制度
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "getPorcessRelatedRuleSql")
    public List<Map<String, Object>> getPorcessRelatedRule(Map<String, Object> map);

    /**
     * 流程文控信息
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findDoucumentControlSql")
    public List<Map<String, Object>> findDoucumentControl(Map<String, Object> map);

    /**
     * 流程KPI
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowKpiSql")
    public List<Map<String, Object>> findFlowKpi(Map<String, Object> map);

    /**
     * 流程KPI 文控
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowKpiHistorySql")
    public List<Map<String, Object>> findFlowHistoryKpi(Map<String, Object> map);

    /**
     * 流程KPI支持工具
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowToolsSql")
    public List<Map<String, Object>> findFlowTools(Map<String, Object> map);

    /**
     * 流程KPI支持工具 文控
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowToolsHistorySql")
    public List<Map<String, Object>> findFlowHistoryTools(Map<String, Object> map);

    /**
     * 流程记录
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowRecordSql")
    public List<Map<String, Object>> findFlowRecord(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "fetchInventory")
    @ResultMap("mapper.Process.flowInventoryBean")
    public List<FlowInventoryBean> fetchInventory(Map<String, Object> paramMap);

    /**
     * 流程体系清单
     *
     * @param paramMap
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "fetchInventoryFlowSystem")
    @ResultMap("mapper.Process.processSystemInventory")
    public List<ProcessSystemInventory> fetchInventoryFlowSystem(Map<String, Object> paramMap);

    /**
     * 流程架构，流程文件
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findProcessMapFileSql")
    public Map<String, Object> findProcessMapFile(Map<String, Object> map);

    /**
     * 流程搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "flowSearch")
    @ResultMap("mapper.Process.flowSearchResultBean")
    public List<FlowSearchResultBean> searchFlow(Map<String, Object> map);

    /**
     * 流程搜索 总数
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "flowSearchTotal")
    @ResultType(Integer.class)
    public int flowSearchTotal(Map<String, Object> paramMap);

    /**
     * 流程架构搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "flowMapSearch")
    @ResultMap("mapper.Process.flowMapSearchResultBean")
    public List<FlowMapSearchResultBean> flowMapSearch(Map<String, Object> map);

    /**
     * 流程架构搜索 总数
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "flowMapSearchTotal")
    @ResultType(Integer.class)
    public int flowMapSearchTotal(Map<String, Object> map);

    /**
     * 流程架构-相关文件
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowMapRelatedFile")
    @ResultMap("mapper.Process.Resource")
    public List<Resource> findFlowMapRelatedFile(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findOperationModelSql")
    @ResultMap("mapper.Process.activeInputFiles")
    public List<FlowOperationModel> findOperationModel(Map<String, Object> map);

    @UpdateProvider(type = ProcessSqlProvider.class, method = "updateFlowBasicInfo")
    public void updateFlowBasicInfo(Map<String, Object> paramMap);

    @SelectProvider(type = ProcessSqlProvider.class, method = "fetchProcessesSupportResources")
    @ResultMap("mapper.Process.Resource")
    List<Resource> fetchProcessesSupportResources(Map<String, Object> paramMap);

    //文控
    @SelectProvider(type = ProcessSqlProvider.class, method = "fetchProcessesSupportHistoryResources")
    @ResultMap("mapper.Process.Resource")
    List<Resource> fetchProcessesSupportHistoryResources(Map<String, Object> paramMap);

    /**
     * 获得流程子级
     *
     * @param paramMap
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "getSubFlows")
    List<Map<String, Object>> getSubFlows(Map<String, Object> paramMap);

    /**
     * 获得架构卡基本信息
     *
     * @param paramMap
     * @return
     */
    @SelectProvider(type = ProcessSqlProvider.class, method = "getProcessArchitectureBaseSql")
    Map<String, Object> getProcessArchitectureBase(Map<String, Object> paramMap);

    @SelectProvider(type = ProcessSqlProvider.class, method = "getFlowKpiMapSql")
    Map<String, Object> getFlowKpiMap(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "fetchFlowSustainToolByKpiId")
    @ResultMap("mapper.Process.flowSustainTool")
    List<FlowSustainTool> fetchFlowSustainToolByKpiId(Map<String, Object> params);

    @Select("select * from JECN_FIRST_TARGER where id=#{id}")
    @ResultMap("mapper.Process.firstTarget")
    FirstTarget getFirstTarget(String id);

    @SelectProvider(type = ProcessSqlProvider.class, method = "getTermDefinesSql")
    List<TermDefinition> getTermDefines(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "getInterfaceDescriptionSql")
    List<InterfaceDescription> getInterfaceDescription(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "getFlowSustainToolSql")
    List<String> getFlowSustainTool(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findRelatedStandardFilesSql")
    List<StandardizedFile> findRelatedStandardFiles(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "fetchInventoryParent")
    @ResultMap("mapper.Process.flowInventoryBean")
    List<FlowInventoryBean> fetchInventoryParent(Map<String, Object> paramMap);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findFlowApplyOrgNamesSql")
    List<String> findFlowApplyOrgNames(Map<String, Object> map);

    //查询文控
    @SelectProvider(type = ProcessSqlProvider.class, method = "findHistoryId")
    Long findHistoryId(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findRelatedFilesSql")
    List<BaseBean> findRelatedFiles(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findMapRelatedFilesSql")
    List<BaseBean> findMapRelatedFiles(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findNewInout")
    @ResultMap("mapper.Process.findNewInout")
    List<JecnFlowInoutBean> findNewInout(Map<String, Object> param);

    @SelectProvider(type = ProcessSqlProvider.class, method = "fetchInouts")
    @ResultMap("mapper.Process.fetchInouts")
    List<JecnFigureInoutBase> fetchInouts(Map<String, Object> map);

    @SelectProvider(type = ProcessSqlProvider.class, method = "findInventedFlows")
    @ResultMap("mapper.SelectFile.selectFileTree")
    List<TreeNode> findInventedFlows(Map<String, Object> map);


    @SelectProvider(type = ProcessSqlProvider.class, method = "getProcessMapResources")
    @ResultMap("mapper.Process.FlowWebBens")
    List<FlowWebBen> getProcessMapResources();
}