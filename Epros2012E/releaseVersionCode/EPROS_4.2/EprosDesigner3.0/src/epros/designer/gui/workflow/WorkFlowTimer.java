package epros.designer.gui.workflow;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.Timer;

import epros.designer.util.ConnectionPool;
import epros.draw.event.JecnOftenActionProcess;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * @author yxw 2012-10-22
 * @description：自动保存定时器
 */
public class WorkFlowTimer implements ActionListener {

	private static WorkFlowTimer workFlowTimer;
	/** 1启用 0不启用 */
	private static int start = 0;
	/** true已经显示 false没有显示 */
	private static boolean showFlag = false;
	private Timer timer;

	public Timer getTimer() {
		return timer;
	}

	private WorkFlowTimer() {
		int sec = Integer.valueOf(ConnectionPool.getConfigAciton().selectValue(5, "basicAutoSaveTime")).intValue();
		start = Integer.valueOf(ConnectionPool.getConfigAciton().selectValue(5, "basicIsAutoSave")).intValue();
		timer = new Timer(1000 * 60 * sec, this);

	}

	public static WorkFlowTimer getWorkFlowTimer() {
		if (workFlowTimer == null) {
			workFlowTimer = new WorkFlowTimer();
		}
		return workFlowTimer;
	}

	/**
	 * @author yxw 2012-12-14
	 * @description:重新启动定时器
	 */
	public static void restart() {
		if (workFlowTimer == null) {
			getWorkFlowTimer();
		}
		if (WorkFlowTimer.start == 1 && JecnDrawMainPanel.getMainPanel().getWorkflow() != null) {
			getWorkFlowTimer().timer.restart();
		}

	}

	/**
	 * @author yxw 2012-12-14
	 * @description:停止定时器
	 */
	public static void stop() {
		getWorkFlowTimer().timer.stop();
	}

	/**
	 * @author yxw 2012-12-14
	 * @description:刷新定时器
	 * @param
	 */
	public static void refresh() {
		int sec = Integer.valueOf(ConnectionPool.getConfigAciton().selectValue(5, "basicAutoSaveTime")).intValue();
		getWorkFlowTimer().timer.setInitialDelay(1000 * 60 * sec);
		int s = Integer.valueOf(ConnectionPool.getConfigAciton().selectValue(5, "basicIsAutoSave")).intValue();
		if (s == 1) {
			WorkFlowTimer.start = 1;
			if (JecnDrawMainPanel.getMainPanel().getWorkflow() != null
					&& JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().isSave()) {
				restart();
			}

		} else {
			WorkFlowTimer.start = 0;
			getWorkFlowTimer().timer.stop();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (start == 1 && JecnDrawMainPanel.getMainPanel().getWorkflow() != null
				&& JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().isSave()) {
			stop();
			if (JecnSystemStaticData.getFrame() != null && JecnSystemStaticData.getFrame().isMinState()) {
				getWorkFlowTimer().timer.start();
				return;
			}

			// 获取所有属于epros的窗口
			Window[] eprosWindow = Window.getWindows();
			for (Window w : eprosWindow) {
				if (w.getOwner() != null && w.getOwner() instanceof JDialog) {
					// 有且存在一个Jdialog为显示状态时，不弹出是否保存对话框，重新开始计时
					if (w.getOwner().isVisible()) {
						getWorkFlowTimer().timer.start();
						return;
					}
				}
			}

//			// true已经显示 false没有显示
//			int option = JecnOptionPane.showConfirmDialog(JecnDrawMainPanel.getMainPanel(), JecnDrawMainPanel
//					.getMainPanel().getResourceManager().getValue("optionInfo"), null,
//					JecnOptionPane.YES_NO_CANCEL_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
//			if (option == JecnOptionPane.OK_OPTION) {
				JecnOftenActionProcess.saveWokfFlow(JecnDrawMainPanel.getMainPanel().getWorkflow());
//			} else {
//				getWorkFlowTimer().timer.start();
//			}

		}

	}
}
