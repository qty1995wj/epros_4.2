package com.jecn.epros.server.common;

import org.springframework.transaction.annotation.Transactional;




@Transactional
public abstract class AbsBaseService<M extends java.io.Serializable, PK extends java.io.Serializable>implements IBaseService<M, PK>  {

	    protected IBaseDao<M, PK> baseDao;
	    
	    
		@Override
	    public PK save(M model) {
	        
	        return baseDao.save(model);
	    }
	    
	    @Override
	    public void merge(M model) {
	        baseDao.merge(model);
	    }

	    @Override
	    public void saveOrUpdate(M model) {
	        baseDao.saveOrUpdate(model);
	    }

	    @Override
	    public void update(M model) {
	        baseDao.update(model);
	    }
	    
	    @Override
	    public void delete(PK id) {
	        baseDao.delete(id);
	    }

	    @Override
	    public void deleteObject(M model) {
	        baseDao.deleteObject(model);
	    }

	    @Override
	    public M get(PK id) {
	        return baseDao.get(id);
	    }
	    
	    @Override
	    public boolean exists(PK id)
	    {
	    	return get(id)!=null;
	    }
}
