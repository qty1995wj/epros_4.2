package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.action.web.login.ad.mengniu.MengNiuAfterItem;
import com.jecn.epros.server.common.HttpRequestUtil;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 基于Json同步数据
 * 
 * @author zxh
 * 
 */
public class ImportMengNiuUserByJson extends AbstractImportUser {
	protected final Log log = LogFactory.getLog(this.getClass());

	public ImportMengNiuUserByJson(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 获取部门数据集合
	 */
	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		// 执行读取Json数据 pageindex=1&pagesize=20
		int index = 1;
		List<DeptBean> result = new ArrayList<DeptBean>();

		String s = HttpRequestUtil.sendGet(MengNiuAfterItem.OA_ORG_SERVICE, "pageindex=" + String.valueOf(index)
				+ "&pagesize=1000");
		result.addAll(getDpetList(s));
		int total = Integer.valueOf(JSONObject.fromObject(s).getJSONObject("Data").getString("TotalCount"));
		while (index * 1000 < total) {
			index++;
			s = HttpRequestUtil.sendGet(MengNiuAfterItem.OA_ORG_SERVICE, "pageindex=" + String.valueOf(index)
					+ "&pagesize=1000");
			result.addAll(getDpetList(s));
		}

		return result;
	}

	/**
	 * 获取人员数据集合
	 */
	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		// 执行读取Json数据
		int index = 1;
		List<UserBean> result = new ArrayList<UserBean>();

//		int pageSize = 10000;
//		String s = HttpRequestUtil.sendGet(MengNiuAfterItem.OA_USER_POSTION_SERVICE, "pageindex="
//				+ String.valueOf(index) + "&pagesize=" + pageSize);
//		result.addAll(getUserList(s));
//		int total = Integer.valueOf(JSONObject.fromObject(s).getJSONObject("Data").getString("TotalCount"));
//
//		log.info("total = " + total);
//		while (index * pageSize < total) {
//			index++;
//			s = HttpRequestUtil.sendGet(MengNiuAfterItem.OA_USER_POSTION_SERVICE, "pageindex=" + String.valueOf(index)
//					+ "&pagesize=" + pageSize);
//			result.addAll(getUserList(s));
//		}
		String s = HttpRequestUtil.sendGet(MengNiuAfterItem.OA_USER_POSTION_SERVICE, "pageindex="
				+ String.valueOf(index) + "&pagesize=100000");
		result.addAll(getUserList(s));
		return result;
	}

	/**
	 * 把数据从json取出存储到bean对象中
	 * 
	 * @param result
	 */
	private List<String> syncDeptNums = new ArrayList<String>();
	static final String Dept_STATUS = "DeptStatus";

	private List<String> noSyncDeptNums = new ArrayList<String>();

	public List<DeptBean> getDpetList(String result) throws Exception {
		/** 部门 */
		List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
		DeptBean deptBean = null;
		JSONArray jsonArray = JSONObject.fromObject(result).getJSONObject("Data").getJSONArray("List");

		String strs[] = MengNiuAfterItem.NOT_SYNC_DEPTS.split(",");
		List<String> notSyncDept = new ArrayList<String>();
		for (String string : strs) {
			notSyncDept.add(string);
		}
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			deptBean = new DeptBean();
			if ("0".equals(jsonObject.getString(Dept_STATUS))) {
				// log.info("无效部门: " + jsonObject.toString());
				noSyncDeptNums.add(jsonObject.getString("DeptCode"));
				continue;
			}

			String deptNum = jsonObject.getString("DeptCode");
			String parentDeptCode = jsonObject.getString("ParentDeptCode");

			// 去除雅士利 的部
			if (parentDeptCode.startsWith("3_") || deptNum.startsWith("3_")) {
				noSyncDeptNums.add(deptBean.getDeptNum());
				continue;
			}

			deptNum = deptNum.substring(2, deptNum.length());
			parentDeptCode = parentDeptCode.substring(2, parentDeptCode.length());
			if (syncDeptNums.contains(deptNum)) {// 去掉重复部门
				continue;
			}

			if (notSyncDept.contains(deptNum.trim())) {// 去掉不存在的部门
				noSyncDeptNums.add(deptBean.getDeptNum());
				continue;
			}
			deptBean.setDeptNum(deptNum);
			deptBean.setDeptName(jsonObject.getString("DeptName_CN"));

			if (deptBean.getDeptNum().equals("1000000")) {
				deptBean.setPerDeptNum("0");
			} else {
				deptBean.setPerDeptNum(parentDeptCode);
			}

			syncDeptNums.add(deptBean.getDeptNum());
			deptBeanList.add(deptBean);
		}
		return deptBeanList;
	}

	private Map<String, String> syncExistsPosNums = new HashMap<String, String>();

	/**
	 * 把数据从json取出存储到bean对象中
	 * 
	 * @param result
	 */
	public List<UserBean> getUserList(String result) throws Exception {
		/** 人员 */
		List<UserBean> userBeanList = new ArrayList<UserBean>();
		UserBean userBean = null;

		JSONArray jsonArray = JSONObject.fromObject(result).getJSONObject("Data").getJSONArray("List");
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			String hrStatus = jsonObject.getString("HRStatus");
			// 在职状态 0离职 1在职
			if ("0".equals(hrStatus)) {
				// log.info("无效人员 : " + jsonObject.toString());
				continue;
			}
			String deptNum = jsonObject.getString("DeptID");
			String posNum = jsonObject.getString("Position");
			String posName = jsonObject.getString("PositionDescr");
			if ("null".equals(posNum)) {
				posNum = null;
			}
			if ("null".equals(deptNum)) {
				deptNum = null;
			}
			if ("null".equals(posName)) {
				posName = null;
			}
			if (StringUtils.isNotBlank(deptNum)) {
				deptNum = deptNum.substring(2, deptNum.length());
			}
			if (StringUtils.isBlank(deptNum) || StringUtils.isBlank(posNum) || StringUtils.isBlank(posName)
					|| (StringUtils.isNotBlank(deptNum) && !syncDeptNums.contains(deptNum))) {
				deptNum = "";
				posNum = "";
				posName = "";
			}
			String email = jsonObject.getString("MailAddress");
			String phone = jsonObject.getString("Phone");
			String loginName = jsonObject.getString("UserLoginID");
			if (loginName.startsWith("02")) {// 02开头用户，非蒙牛内部人员
				continue;
			}
			userBean = new UserBean();

			userBean.setUserNum(loginName);
			userBean.setTrueName(jsonObject.getString("UserName_CN"));
			// 岗位编号- 部门编号+ 岗位编号
			userBean.setPosNum(StringUtils.isNotBlank(posNum) ? deptNum + "_" + posNum : posNum);
			// 特殊情况处理：一个岗位编号出现多个岗位名称（VS系统没问题，OA中数据有问题）
			if (StringUtils.isNotBlank(posNum)) {
				userBean.setPosName(syncExistsPosNums.get(posNum) == null ? posName : syncExistsPosNums.get(posNum));
			} else {
				userBean.setPosName(posName);
			}
			userBean.setDeptNum(deptNum);
			userBean.setEmail("null".equals(email) ? "" : email);
			userBean.setEmailType(0);
			userBean.setPhone("null".equals(phone) ? "" : phone);

			if (!syncExistsPosNums.containsKey(posNum)) {// 
				syncExistsPosNums.put(posNum, userBean.getPosName());
			}
			userBeanList.add(userBean);
		}
		return userBeanList;
	}
}
