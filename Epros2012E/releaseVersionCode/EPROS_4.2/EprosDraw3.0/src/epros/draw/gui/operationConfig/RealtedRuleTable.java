package epros.draw.gui.operationConfig;

import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 相关制度
 * 
 * @author 2012-07-06
 * 
 */
public class RealtedRuleTable extends JTable {

	private Vector<Vector<String>> content;

	public RealtedRuleTable() {
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	public void setTableModel() {
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		columnModel.getColumn(3).setMinWidth(100);
		columnModel.getColumn(3).setMaxWidth(150);
	}

	public RealtedRuleTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		// 制度编号
		title.add("制度编号");
		// 制度名称
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("systemName"));
		// 类型
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("type"));
		return new RealtedRuleTableMode(getContent(), title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class RealtedRuleTableMode extends DefaultTableModel {
		public RealtedRuleTableMode(Vector<Vector<String>> data, Vector<String> title) {
			super(data, title);
		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	public Vector<Vector<String>> getContent() {
		return content;
	}

	public void setContent(Vector<Vector<String>> content) {
		this.content = content;
	}
}
