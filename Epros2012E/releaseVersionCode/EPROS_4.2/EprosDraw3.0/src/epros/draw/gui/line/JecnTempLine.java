package epros.draw.gui.line;

import java.awt.Color;
import java.awt.Point;

import javax.swing.JPanel;

/**
 * 临时线段
 * 
 * 临时线段是指在线段操作过程中临时使用的线段，当操作完成生成正式线段后此临时线段就清除
 * 
 * @author ZHANGXH
 */
public class JecnTempLine extends JPanel {
	/** 虚拟线段开始点 */
	private Point tempStartPt = null;
	/** 虚拟线段结束点 */
	private Point tempEndPt = null;
	/** 100% 状态开始点 */
	private Point originalStartPt = null;
	/** 100% 状态结束点 */
	private Point originalEndPt = null;

	/**
	 * 根据坐标点生成虚拟线段
	 * 
	 * @param startX
	 *            开始点的横坐标
	 * @param startY
	 *            开始点的纵坐标
	 * @param endX
	 *            结束点的横坐标
	 * @param endY
	 *            结束点的纵坐标
	 * @return
	 */
	public void drawTempLine(int startX, int startY, int endX, int endY) {
		// 虚线颜色红色
		this.setBackground(Color.RED);
		if (startX == endX) {
			// 获取当前比例下 线的大小和位置点
			this.setBounds(startX, Math.min(startY, endY), 1, Math.abs(startY
					- endY));
		}
		if (startY == endY) {
			// 获取当前比例下 线的大小和位置点
			this.setBounds(Math.min(startX, endX), endY, Math
					.abs(startX - endX), 1);
		}
	}

	public Point getTempStartPt() {
		return tempStartPt;
	}

	public void setTempStartPt(Point tempStartPt) {
		this.tempStartPt = tempStartPt;
	}

	public Point getTempEndPt() {
		return tempEndPt;
	}

	public void setTempEndPt(Point tempEndPt) {
		this.tempEndPt = tempEndPt;
	}

	public Point getOriginalStartPt() {
		return originalStartPt;
	}

	public void setOriginalStartPt(Point originalStartPt) {
		this.originalStartPt = originalStartPt;
	}

	public Point getOriginalEndPt() {
		return originalEndPt;
	}

	public void setOriginalEndPt(Point originalEndPt) {
		this.originalEndPt = originalEndPt;
	}
}
