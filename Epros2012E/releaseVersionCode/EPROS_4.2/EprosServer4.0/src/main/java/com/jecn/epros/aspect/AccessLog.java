package com.jecn.epros.aspect;

import io.swagger.annotations.ApiModelProperty;

public class AccessLog {

    @ApiModelProperty("访问者的 loginName")
    private String visitor;
    @ApiModelProperty("被访问的资源")
    private String uri;
    @ApiModelProperty("请求的参数 [url 问号之后的部分]")
    private String queryString;
    @ApiModelProperty("访问者 IP")
    private String remoteAddr;
    @ApiModelProperty("访问者主机名")
    private String remoteHost;
    @ApiModelProperty("请求的方式【GET,POST,DELETE,PATCH,OPTION】")
    private String method;
    @ApiModelProperty("访问者使用的操作系统")
    private String operatingSystem;
    @ApiModelProperty("访问者使用的浏览器")
    private String browser;
    @ApiModelProperty("访问的后台接口信息")
    private String classMethod;
    @ApiModelProperty("后台接口参数值")
    private String args;
    @ApiModelProperty("访问结果的状态码")
    private String statusCode;

    public String getVisitor() {
        return visitor;
    }

    public void setVisitor(String visitor) {
        this.visitor = visitor;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getClassMethod() {
        return classMethod;
    }

    public void setClassMethod(String classMethod) {
        this.classMethod = classMethod;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
