package com.jecn.epros.server.service.reports.excel;

import java.util.List;

import jxl.write.Label;
import jxl.write.WritableSheet;

import com.jecn.epros.server.webBean.process.ProcessRoleWebBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.ProcessApplicationDetailsBean;

/**
 * 
 * 流程应用人数统计
 * 
 * @author ZHOUXY
 * 
 */
public class ProcessAppUserCountDownExcelService extends BaseDownExcelService {
	private List<ProcessApplicationDetailsBean> appUserCountList = null;

	public ProcessAppUserCountDownExcelService() {
		this.mouldPath = getPrefixPath()
				+ "mould/processAppUserCountReport.xls";
		this.startDataRow = 2;
	}

	@Override
	protected boolean editExcle(WritableSheet dataSheet) {
		if (appUserCountList == null) {
			return true;
		}

		try {
			// 第一行
			this.addFirstRowData(dataSheet);

			// 数据行号
			int i = 0;
			// 第三行开始
			for (ProcessApplicationDetailsBean appUserBean : appUserCountList) {
				// 流程数据
				ProcessWebBean flowBean = appUserBean.getProcessWebBean();
				// 流程对应的角色和个数
				List<ProcessRoleWebBean> listProcessRoleWebBean = appUserBean
						.getListProcessRoleWebBean();

				if (flowBean == null) {
					continue;
				}

				if (listProcessRoleWebBean == null
						|| listProcessRoleWebBean.size() == 0) {
					int row = startDataRow + i;
					// 流程名称
					dataSheet
							.addCell(new Label(0, row, flowBean.getFlowName()));
					// 个数
					dataSheet.addCell(new Label(2, row, "0"));
					i++;
					continue;
				}

				// 待合并开始行号
				int startFirstRow = startDataRow + i;
				
				for (ProcessRoleWebBean bean : listProcessRoleWebBean) {
					int row = startDataRow + i;
					// 流程名称
					dataSheet
							.addCell(new Label(0, row, flowBean.getFlowName()));
					// 流程涉及角色
					dataSheet.addCell(new Label(1, row, bean.getRoleName()));
					// 个数
					dataSheet.addCell(new Label(2, row, String.valueOf(bean
							.getTotalUser())));
					i++;
				}
				// 合并单元格，参数格式（开始列，开始行，结束列，结束行）
				// 流程名称
				dataSheet.mergeCells(0, startFirstRow, 0, startDataRow + i - 1);
			}
			return true;
		} catch (Exception e) {
			log.error("ProcessAppUserCountDownExcelService类editExcle方法", e);
			return false;
		}
	}

	public List<ProcessApplicationDetailsBean> getAppUserCountList() {
		return appUserCountList;
	}

	public void setAppUserCountList(
			List<ProcessApplicationDetailsBean> appUserCountList) {
		this.appUserCountList = appUserCountList;
	}
}
