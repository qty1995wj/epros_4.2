package epros.designer.gui.process.guide.guideTable;

import java.util.List;
import java.util.Vector;

import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.process.JecnKPIFirstTargetBean;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;
/***
 * 一级指标数据显示Table
 * @Time 2014-10-17
 *
 */
public class KPIFirstTargetTable extends JecnTable{
	private List<JecnKPIFirstTargetBean> firstTargetList;
	
	public KPIFirstTargetTable(List<JecnKPIFirstTargetBean> firstTargetList){
		this.firstTargetList = firstTargetList;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		//设置第一列宽度为0
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		int[] cols1 = new int[] { 1 };
		//设置第一列宽度为0
		if (cols1 != null && cols1.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols1) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(100);
				tableColumn.setMaxWidth(100);
			}
		}
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}
	
	@Override
	public JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		//主键id
		title.add("id");
		//序号
		title.add(JecnProperties.getValue("sort"));
		//内容
		title.add(JecnProperties.getValue("content"));
		return new JecnTableModel(title,getContent(firstTargetList));
	}
	public Vector<Vector<String>> getContent(List<JecnKPIFirstTargetBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (int i = 0; i<list.size();i++) {
				JecnKPIFirstTargetBean jecnKPIFirstTargetBean = list.get(i);
				Vector<String> data = new Vector<String>();
				data.add(jecnKPIFirstTargetBean.getId().toString());
				data.add(String.valueOf(i+1));
				data.add(jecnKPIFirstTargetBean.getTargetContent());
				content.add(data);
			}
		}
		return content;
	}
	public void remoeAll() {
		// 清空
		for (int index = this.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) this.getModel()).removeRow(index);
		}
	}
	@Override
	public int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

}
