package com.jecn.epros.server.bean.rule;

import java.util.Date;

/**
 * 模板表(页面表)
 * 
 * @author lihongliang
 * 
 */
public class RuleModeBean implements java.io.Serializable {
	private Long id;
	private Long parentId;//父节点
    private String modeName;//模板名称
    private Integer modeType;//(0是目录，1是模板)
    private Date createDate;//创建时间
	private Date updateDate;//更新时间
	private Long createPeopleId;//创建人ID
	private Long updatePeopleId;//更新人ID
	private Integer sortId;        //排序
	
	
	public Integer getSortId() {
		return sortId;
	}
	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getModeName() {
		return modeName;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public void setModeName(String modeName) {
		this.modeName = modeName;
	}
	public Integer getModeType() {
		return modeType;
	}
	public void setModeType(Integer modeType) {
		this.modeType = modeType;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Long getCreatePeopleId() {
		return createPeopleId;
	}
	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}
	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}
	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}
}
