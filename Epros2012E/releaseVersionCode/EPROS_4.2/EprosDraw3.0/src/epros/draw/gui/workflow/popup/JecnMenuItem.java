package epros.draw.gui.workflow.popup;

import javax.swing.JMenuItem;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 菜单项
 * 
 * @author ZHOUXY
 * 
 */
public class JecnMenuItem extends JMenuItem {
	/** 菜单项类型 */
	protected ToolBarElemType toolBarElemType = null;
	/** 弹出菜单处理对象 */
	private JecnFlowElemPopupMenu flowElemPopupMenu = null;

	public JecnMenuItem(JecnFlowElemPopupMenu flowElemPopupMenu, ToolBarElemType toolBarElemType) {
		this.flowElemPopupMenu = flowElemPopupMenu;
		this.toolBarElemType = toolBarElemType;

		initComponents();
		initIconAndText();
	}

	public JecnMenuItem(JecnWorkflowPopupMenu workflowPopupMenu, ToolBarElemType toolBarElemType, String icon,
			String text) {
		this.toolBarElemType = toolBarElemType;
		initComponents();
		if (icon != null && text != null) {
			initIconAndText(icon, text);
		} else {
			initIconAndText();
		}
	}

	private void initComponents() {
		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 图片内容显示方式：靠左
		this.setHorizontalAlignment(SwingConstants.LEFT);

		// 添加点击监听
		this.addActionListener(JecnDrawMainPanel.getMainPanel());
		// 不显示焦点状态
		this.setFocusPainted(false);
	}

	private void initIconAndText() {
		// 图片
		if (toolBarElemType != null) {
			// 设置图片
			this.setIcon(JecnFileUtil.getToolBarImageIcon(toolBarElemType.toString()));
			// 设置内容
			this.setText(JecnResourceUtil.getJecnResourceUtil().getValue(toolBarElemType));
		}
	}

	private void initIconAndText(String icon, String text) {
		// 图片
		if (toolBarElemType != null) {
			// 设置图片
			this.setIcon(JecnFileUtil.getToolBarImageIcon(icon));
			// 设置内容
			this.setText(JecnResourceUtil.getJecnResourceUtil().getValue(text));
		}
	}

	public ToolBarElemType getToolBarElemType() {
		return toolBarElemType;
	}

	/**
	 * 
	 * 获取流程元素对象:当前需要改变的流程元素对象
	 * 
	 * @return JecnBaseFlowElementPanel 流程元素对象
	 */
	public JecnBaseFlowElementPanel getElementPanel() {
		return flowElemPopupMenu.getElementPanel();
	}

}