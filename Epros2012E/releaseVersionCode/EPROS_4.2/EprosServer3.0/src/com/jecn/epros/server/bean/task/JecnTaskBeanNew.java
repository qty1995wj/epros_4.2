package com.jecn.epros.server.bean.task;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 任务主表-1
 * 
 * @author xiaohu
 * 
 */
public class JecnTaskBeanNew implements java.io.Serializable {
	/** 主键ID */
	private Long id;
	/** 任务名称 */
	private String taskName;
	/**
	 * 0：拟稿人主导主导审批（默认） 1：文控审核人主导审批;
	 */
	private Integer isControlauditLead;
	/** 开始时间 */
	private Date startTime;
	/** 结束时间 */
	private Date endTime;
	/**
	 * 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段
	 * 8:事业部经理 9：总经理 10:整理意见
	 */
	private Integer state;
	/** State对应的阶段名称 用于邮件发送 ：不存入数据库 */
	private String stateMark;

	private String upStateMark;
	/**
	 * 上一个状态 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段
	 * 8:事业部经理 9：总经理 10:整理意见
	 */
	private Integer upState;
	/**
	 * 子操作 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
	 * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)） 10：交办人提交 11:编辑提交
	 * 12:编辑打回 13:撤回 14:反回 15：交给拟稿人整理（相当于打回整理---->拟稿人）
	 * 16：拟稿人整理完交给当前阶段（拟稿人------->当前阶段审批人）
	 */
	private Integer taskElseState;
	/** 任务说明 */
	private String taskDesc;
	/**
	 * 任务锁定：所有流程动作都不能做，只有解锁后才能操作 1：未锁定（默认） 0：锁定 也就是假删除状态
	 */
	private Integer isLock;
	/** 试运行周期 */
	private Date testRunTime;
	/** 版本号 */
	private String historyVistion;
	/**
	 * 0：试运行 1：是正式 2：是升级
	 */
	private Integer flowType;
	/** 间隔多少天发送试运行报告 */
	private Integer sendRunTimeValue;
	/**
	 * taskType=0时，表示流程ID，taskType=1时，表示文件ID，taskType=2时，表示制度模板ID,3:制度文件ID，4：
	 * 流程地图ID
	 */
	private Long rid;
	/** 文件名称 不存数据库 */
	private String rname;
	/** 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务 */
	private int taskType;
	/** 创建人 */
	private Long createPersonId;
	/** 创建人 名称不存数据库 */
	private String createPersonTemporaryName;
	/** 创建时间 */
	private Date createTime;
	/** 更新时间 */
	private Date updateTime;
	/** 源人 */
	private Long fromPeopleId;
	/** 源人 名称 不存数据库 */
	private String fromPeopleTemporaryName;
	/** 评审次数 */
	private int revirewCounts;
	/** 驳回次数 */
	private int approveNoCounts;
	/** 格式化后的结束时间:不存数据库 */
	private String endTimeStr;
	/** 格式化后开始时间 */
	private String startTimeStr;
	/** 实施日期,不存任务数据库 */
	private Date implementDate;
	/** 审批类型 0审批 1借阅 2废止 * */
	private Integer approveType;
	/**
	 * 表示任务相关的资源是否可编辑 1可编辑 其它不可 只有任务在拟稿人的时候可以编辑
	 */
	private Integer edit;
	/** 自定义输入项1 * */
	private String customInputItemOne;
	/** 自定义输入项2 * */
	private String customInputItemTwo;
	/** 自定义输入项3 * */
	private String customInputItemThree;

	/** 当前操作人对应的代办ID */
	private Long curPeopleTaskId;
	/** 人员ID对应的代办ID */
	private Map<Long, Long> handlerPeopleMap = new HashMap<Long, Long>();

	private Map<Long, Long> canclePeopleMap = new HashMap<Long, Long>();
 
	/** 万华 待办标题，责任人提交XXX流程，请审批 */
	private Long resPeopleId;

	public String getCustomInputItemOne() {
		return customInputItemOne;
	}

	public void setCustomInputItemOne(String customInputItemOne) {
		this.customInputItemOne = customInputItemOne;
	}

	public String getCustomInputItemTwo() {
		return customInputItemTwo;
	}

	public void setCustomInputItemTwo(String customInputItemTwo) {
		this.customInputItemTwo = customInputItemTwo;
	}

	public String getCustomInputItemThree() {
		return customInputItemThree;
	}

	public void setCustomInputItemThree(String customInputItemThree) {
		this.customInputItemThree = customInputItemThree;
	}

	public Integer getEdit() {
		return edit;
	}

	public void setEdit(Integer edit) {
		this.edit = edit;
	}

	public Date getImplementDate() {
		return implementDate;
	}

	public void setImplementDate(Date implementDate) {
		this.implementDate = implementDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Integer getIsControlauditLead() {
		return isControlauditLead;
	}

	public void setIsControlauditLead(Integer isControlauditLead) {
		this.isControlauditLead = isControlauditLead;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getUpState() {
		return upState;
	}

	public void setUpState(Integer upState) {
		this.upState = upState;
	}

	public Integer getTaskElseState() {
		return taskElseState;
	}

	public void setTaskElseState(Integer taskElseState) {
		this.taskElseState = taskElseState;
	}

	public String getTaskDesc() {
		return taskDesc;
	}

	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}

	public Integer getIsLock() {
		return isLock;
	}

	public void setIsLock(Integer isLock) {
		this.isLock = isLock;
	}

	public Date getTestRunTime() {
		return testRunTime;
	}

	public void setTestRunTime(Date testRunTime) {
		this.testRunTime = testRunTime;
	}

	public String getHistoryVistion() {
		return historyVistion;
	}

	public void setHistoryVistion(String historyVistion) {
		this.historyVistion = historyVistion;
	}

	public Integer getFlowType() {
		return flowType;
	}

	public void setFlowType(Integer flowType) {
		this.flowType = flowType;
	}

	public Integer getSendRunTimeValue() {
		return sendRunTimeValue;
	}

	public void setSendRunTimeValue(Integer sendRunTimeValue) {
		this.sendRunTimeValue = sendRunTimeValue;
	}

	public Long getRid() {
		return rid;
	}

	public void setRid(Long rid) {
		this.rid = rid;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getRevirewCounts() {
		return revirewCounts;
	}

	public void setRevirewCounts(int revirewCounts) {
		this.revirewCounts = revirewCounts;
	}

	public int getApproveNoCounts() {
		return approveNoCounts;
	}

	public void setApproveNoCounts(int approveNoCounts) {
		this.approveNoCounts = approveNoCounts;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getFromPeopleId() {
		return fromPeopleId;
	}

	public void setFromPeopleId(Long fromPeopleId) {
		this.fromPeopleId = fromPeopleId;
	}

	public String getCreatePersonTemporaryName() {
		return createPersonTemporaryName;
	}

	public void setCreatePersonTemporaryName(String createPersonTemporaryName) {
		this.createPersonTemporaryName = createPersonTemporaryName;
	}

	public String getFromPeopleTemporaryName() {
		return fromPeopleTemporaryName;
	}

	public void setFromPeopleTemporaryName(String fromPeopleTemporaryName) {
		this.fromPeopleTemporaryName = fromPeopleTemporaryName;
	}

	public String getEndTimeStr() {
		return endTimeStr;
	}

	public void setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
	}

	public String getStartTimeStr() {
		return startTimeStr;
	}

	public void setStartTimeStr(String startTimeStr) {
		this.startTimeStr = startTimeStr;
	}

	public String getStateMark() {
		return stateMark;
	}

	public void setStateMark(String stateMark) {
		this.stateMark = stateMark;
	}

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public Integer getApproveType() {
		return approveType;
	}

	public void setApproveType(Integer approveType) {
		this.approveType = approveType;
	}

	public String getUpStateMark() {
		return upStateMark;
	}

	public void setUpStateMark(String upStateMark) {
		this.upStateMark = upStateMark;
	}

	public Long getCurPeopleTaskId() {
		return curPeopleTaskId;
	}

	public void setCurPeopleTaskId(Long curPeopleTaskId) {
		this.curPeopleTaskId = curPeopleTaskId;
	}

	public Map<Long, Long> getHandlerPeopleMap() {
		return handlerPeopleMap;
	}

	public void setHandlerPeopleMap(Map<Long, Long> handlerPeopleMap) {
		this.handlerPeopleMap = handlerPeopleMap;
	}

	public Map<Long, Long> getCanclePeopleMap() {
		return canclePeopleMap;
	}

	public void setCanclePeopleMap(Map<Long, Long> canclePeopleMap) {
		this.canclePeopleMap = canclePeopleMap;
	}

	public Long getResPeopleId() {
		return resPeopleId;
	}

	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}
}
