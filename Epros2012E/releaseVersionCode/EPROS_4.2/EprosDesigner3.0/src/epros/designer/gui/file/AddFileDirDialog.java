package epros.designer.gui.file;

import java.awt.Insets;
import java.util.Arrays;
import java.util.Date;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;

/***
 * 创建文件目录
 * 
 * @author 2012-05-30
 * 
 */
public class AddFileDirDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(AddFileDirDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;

	public AddFileDirDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addDirI");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	@Override
	public void saveData() {
		// 向文件FileBean添加数据
		JecnFileBeanT fileBean = new JecnFileBeanT();
		// 文件名称
		fileBean.setFileName(getName());
		// 创建人
		fileBean.setPeopleID(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 更新人
		fileBean.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 父ID
		fileBean.setPerFileId(pNode.getJecnTreeBean().getId());
		// 是否是目录 0：目录，1：文件
		fileBean.setIsDir(0);
		// 密级：0是秘密,1是公开
		fileBean.setIsPublic(JecnConfigTool.isShowItem(ConfigItemPartMapMark.pubFile) ? 1L : 0L);
		// 隐藏 0:隐藏 1：显示
		int hide = 0;
		if ("0".equals(pNode.getJecnTreeBean().getId().toString())) {
			hide = 1;
		} else {
			hide = pNode.getJecnTreeBean().isPub() ? 1 : 0;
		}
		fileBean.setHide(hide);
		// 创建时间
		fileBean.setCreateTime(new Date());
		// 项目ID
		fileBean.setProjectId(JecnConstants.projectId);
		fileBean.setDelState(0);

		// 执行数据库表的添加保存
		try {
			// 排序
			fileBean.setSortId(JecnTreeCommon.getMaxSortFromDB(pNode));

			Long id = ConnectionPool.getFileAction().addFileDir(fileBean);
			// 向树节点添加文件 目录
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.fileDir);
			jecnTreeBean.setIsDelete(0);
			// 设置新添加的目录的hide值（用于显示/隐藏）
			boolean hideFlag = hide == 1 ? true : false;
			jecnTreeBean.setPub(hideFlag);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("AddFileDirDialog saveData is error", e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		try {
			String[] validateAddName = ConnectionPool.getFileAction().validateAddName(Arrays.asList(name),
					pNode.getJecnTreeBean().getId(), 0);
			if (validateAddName != null && validateAddName.length > 0) {
				return true;
			}
		} catch (Exception e) {
			log.error("AddFileDirDialog validateNodeRepeat is error", e);
		}
		return false;
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}
}
