package com.jecn.epros.email;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.EmailBasicInfo;

public class TaskDelayEmailBuilder extends DynamicContentEmailBuilder {

    @Override
    protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {
        return createEmail(user);
    }

    private EmailBasicInfo createEmail(JecnUser user) {

        // 管理员点击页面的链接直接打开任务管理页面
        String httpUrl = "/loginMail.action?accessType=mailOpenTaskManagement&mailPeopleId=" + user.getId();
        String subject = "任务审批人员缺失提醒！";
        String content = "当前审批任务中有审批人员缺失，可能导致审批不能进行下去，请及时替换审批人员！";
        EmailContentBuilder builder = new EmailContentBuilder(getTip());
        builder.setContent(content);
        builder.setResourceUrl(httpUrl);
        EmailBasicInfo emailBasicInfo = new EmailBasicInfo();
        emailBasicInfo.setSubject(subject);
        emailBasicInfo.setContent(builder.buildContent());
        emailBasicInfo.addRecipients(user);
        return emailBasicInfo;

    }


}
