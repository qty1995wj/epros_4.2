package com.jecn.epros.server.dao.dataImport.excelorDB;

import java.util.List;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpOriDept;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;

/**
 * 人员同步数据校验DAO
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-5-29 时间：上午10:46:15
 */
public interface IUserDataCheckDao extends IBaseDao<DeptBean, Long> {
	/**
	 * 
	 * 人员数据行间校验，是否存在重复数据
	 */
	List<JecnTmpPosAndUserBean> userSameDataCheck();

	/**
	 * 人员登录名称相同数据验证 登录名称相同，存在岗位编号为空
	 * 
	 * @return
	 */
	List<JecnTmpPosAndUserBean> loginNameSameAndPosNullCheck();

	/**
	 * 一人多岗位清空，岗位编号相同 异常
	 * 
	 * @return
	 */
	List<JecnTmpPosAndUserBean> sameLoginNameAndSamePos();

	/**
	 * 一人多岗位情况 ，真实姓名不同
	 * 
	 * @return
	 */
	List<JecnTmpPosAndUserBean> trueNameNoSameCheck();

	/**
	 * 一人多岗位情况 ，邮箱不同
	 * 
	 * @return
	 */
	List<JecnTmpPosAndUserBean> emailNoSameCheck();

	/**
	 * 一人多岗位情况 ，联系方式不同
	 * 
	 * @return
	 */
	List<JecnTmpPosAndUserBean> phoneNoSameCheck();

	/**
	 * 部门行间校验，部门编号不唯一
	 * 
	 * @return List<JecnTmpOriDept>
	 */
	List<JecnTmpOriDept> hasSameDeptNum();

	/**
	 * 同一部门下部门名称不能重复
	 * 
	 * @return
	 */
	List<JecnTmpOriDept> hasSameDeptName();

	/**
	 * 部门内不能出现重复岗位
	 * 
	 * @return List<JecnTmpPosAndUserBean>
	 */
	List<JecnTmpPosAndUserBean> deptHashSamePosName();

	/**
	 * 岗位编号不唯一
	 * 
	 * @return List<JecnTmpPosAndUserBean>
	 */
	List<JecnTmpPosAndUserBean> posNumNotOnly();

	/**
	 * 岗位所属部门编号在部门集合中不存在
	 * 
	 * @return
	 * @return List<JecnTmpPosAndUserBean>
	 */
	List<JecnTmpPosAndUserBean> posNumRefDeptNumNoExists();

	/**
	 * 岗位编号相同，岗位名称必须相同
	 * 
	 * @return
	 */
	List<JecnTmpPosAndUserBean> posNameNotSameList();

	List<JecnTmpPosAndUserBean> posNumSameAndDeptSameList();
}
