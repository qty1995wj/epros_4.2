package epros.designer.gui.process.roleDetail;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;

/*******************************************************************************
 * 图形：添加角色
 * 
 * @author 2012-08-15
 * 
 */
public class RoleDetailDialog extends JecnDialog {

	private RoleMainPanel mainPanel;

	/** 是否占击确定 */
	private boolean isOk = false;
	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	public RoleDetailDialog(JecnBaseRoleFigure baseRoleFigure) {
		mainPanel = new RoleMainPanel(baseRoleFigure);
		initComponents();
		initLayout();
		initEvent();
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelPerformed();
				return true;
			}
		});
	}

	private void initEvent() {
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelPerformed();
			}
		});
	}

	protected void okButPerformed() {
		if (!mainPanel.isUpdate()) {
			this.dispose();
			return;
		}
		mainPanel.saveData();
		isOk = true;
		this.dispose();
	}

	private void initLayout() {
		this.getContentPane().add(mainPanel);
		mainPanel.getButPanel().add(okBut);
		mainPanel.getButPanel().add(cancelBut);
	}

	private void initComponents() {
		if (mainPanel.getJecnDesignerData() != null && mainPanel.getJecnDesignerData().getModeType() == ModeType.none) { // 流程
			this.setSize(600, 300);
		} else {
			this.setSize(600, 200);
		}
		this.setTitle(JecnProperties.getValue("addRole"));
		this.setResizable(true);
		this.setModal(true);
		this.setLocationRelativeTo(null);

	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelPerformed() {
		if (!mainPanel.isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int tipAction = dialogCloseBeforeMsgTipAction();
			if (2 == tipAction) {// 信息提示：true：需要执行关闭
				this.okButPerformed();
			} else if (1 == tipAction) {
				this.setVisible(false);
			}
		}
	}

	public boolean isOk() {
		return isOk;
	}

	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}
}
