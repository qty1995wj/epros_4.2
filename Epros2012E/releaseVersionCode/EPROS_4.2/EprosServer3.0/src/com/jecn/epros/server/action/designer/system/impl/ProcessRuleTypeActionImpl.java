package com.jecn.epros.server.action.designer.system.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.system.IProcessRuleTypeAction;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.service.system.IProcessRuleTypeService;

public class ProcessRuleTypeActionImpl implements IProcessRuleTypeAction {
	
	private IProcessRuleTypeService processRuleTypeService;

	public IProcessRuleTypeService getProcessRuleTypeService() {
		return processRuleTypeService;
	}

	public void setProcessRuleTypeService(
			IProcessRuleTypeService processRuleTypeService) {
		this.processRuleTypeService = processRuleTypeService;
	}

	@Override
	public void delteFlowArributeType(List<Long> typeIds) throws Exception {
		processRuleTypeService.delteFlowArributeType(typeIds);
	}

	@Override
	public List<ProceeRuleTypeBean> getFlowAttributeType() throws Exception {
		return processRuleTypeService.getFlowAttributeType();
	}

	@Override
	public void updateFlowType(ProceeRuleTypeBean proceeRuleTypeBean)
			throws Exception {
		processRuleTypeService.updateFlowType(proceeRuleTypeBean);
	}

	@Override
	public Long addFlowType(ProceeRuleTypeBean proceeRuleTypeBean)
			throws Exception {
		return processRuleTypeService.save(proceeRuleTypeBean);
	}
	

}
