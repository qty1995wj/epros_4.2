package epros.designer.gui.process.activitiesProperty;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnRefIndicatorsT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 活动说明： 指标
 * 
 * @author 2012--07-27
 * 
 */
public class ActiveTargetPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(ActiveTargetPanel.class);
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel(450, 30);

	/** 指标表所在的滚动面板 */
	private JScrollPane activeTargetScrollPane = new JScrollPane();

	/** 指标表 */
	private JTable jecnTable = null;

	/** *添加按钮 */
	private JButton addBut = new JButton(JecnProperties.getValue("addBtn"));

	/** *删除按钮 */
	private JButton deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));

	/** *编辑按钮 */
	private JButton editBut = new JButton(JecnProperties.getValue("editBtn"));

	private JLabel targetLabel = new JLabel();

	/** 设置大小 */
	private Dimension dimension = null;
	/** 活动的数据 */
	private JecnActiveData jecnActiveData;
	/** 活动的指标数据 */
	private List<JecnRefIndicatorsT> listJecnRefIndicatorsT = new ArrayList<JecnRefIndicatorsT>();

	/** 父类对话框 */
	private JecnDialog jecnDialog;

	public ActiveTargetPanel(JecnActiveData jecnActiveData, JecnDialog jecnDialog) {
		this.jecnActiveData = jecnActiveData;
		this.jecnDialog = jecnDialog;
		if (JecnDesignerCommon.isShowEleAttribute()) {
			dimension = new Dimension(450, 80);
		} else {
			this.setSize(480, 350);
			dimension = new Dimension(450, 260);
		}

		targetLabel.setForeground(Color.red);
		initLaout();
		activeTargetScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		activeTargetScrollPane.setPreferredSize(dimension);
		activeTargetScrollPane.setMaximumSize(dimension);
		activeTargetScrollPane.setMinimumSize(dimension);

		// 添加
		addBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addButPerformed();
			}
		});
		// 编辑
		editBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editButPerformed();
			}
		});
		deleteBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}
		});
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData) {
		this.jecnActiveData = jecnActiveData;
		removeAllTable();
		Vector<Vector<String>> fileData = getContent();
		for (Vector<String> v : fileData) {
			((DefaultTableModel) jecnTable.getModel()).addRow(v);
		}
	}

	private void removeAllTable() {
		listJecnRefIndicatorsT = new ArrayList<JecnRefIndicatorsT>();
		for (int index = jecnTable.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) jecnTable.getModel()).removeRow(index);
		}
	}

	private void initLaout() {
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 1, 2);
		// 指标
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		jecnTable = new JTable(getTableModel());
		// 设置默认背景色
		jecnTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		jecnTable.getTableHeader().setReorderingAllowed(false);
		jecnTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel columnModel = jecnTable.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(0);
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		activeTargetScrollPane.setViewportView(jecnTable);
		this.add(activeTargetScrollPane, c);
		// buttonPanel
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel.add(addBut);
		buttonPanel.add(editBut);
		buttonPanel.add(deleteBut);
		buttonPanel.add(targetLabel);

	}

	/***************************************************************************
	 * t添加
	 * 
	 */
	private void addButPerformed() {
		AddActiveTargetDialog addActiveTargetDialog = new AddActiveTargetDialog(jecnDialog);
		addActiveTargetDialog.setVisible(true);
		if (addActiveTargetDialog.isOperation()) {
			addActiveTargetDialog.getJecnRefIndicatorsT().setActivityId(jecnActiveData.getFlowElementId());
			listJecnRefIndicatorsT.add(addActiveTargetDialog.getJecnRefIndicatorsT());
			Vector<String> row = new Vector<String>();
			row.add(String.valueOf(addActiveTargetDialog.getJecnRefIndicatorsT().getId()));
			// 活动KPI(指标名称)
			row.add(addActiveTargetDialog.getJecnRefIndicatorsT().getIndicatorName());
			// 目标值(指标值)
			row.add(addActiveTargetDialog.getJecnRefIndicatorsT().getIndicatorValue());
			((JecnTableModel) jecnTable.getModel()).addRow(row);
		}
	}

	/**
	 * 编辑
	 * 
	 * @author Administrator
	 * 
	 */
	private void editButPerformed() {
		targetLabel.setText("");
		int rowCount = jecnTable.getSelectedRow();
		if (rowCount == -1) {
			targetLabel.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		JecnRefIndicatorsT jecnRefIndicatorsT = this.listJecnRefIndicatorsT.get(rowCount);

		EditActiveTargetDialog editActiveTargetDialog = new EditActiveTargetDialog(jecnDialog);
		editActiveTargetDialog.getActiveKPIField().setText(jecnRefIndicatorsT.getIndicatorName());
		editActiveTargetDialog.getActiveKPIValueField().setText(jecnRefIndicatorsT.getIndicatorValue());
		editActiveTargetDialog.setVisible(true);
		if (editActiveTargetDialog.isOperation()) {
			jecnRefIndicatorsT.setIndicatorName(editActiveTargetDialog.getJecnRefIndicatorsT().getIndicatorName());
			jecnRefIndicatorsT.setIndicatorValue(editActiveTargetDialog.getJecnRefIndicatorsT().getIndicatorValue());
			jecnTable.getModel().setValueAt(editActiveTargetDialog.getJecnRefIndicatorsT().getIndicatorName(),
					rowCount, 1);
			jecnTable.getModel().setValueAt(editActiveTargetDialog.getJecnRefIndicatorsT().getIndicatorValue(),
					rowCount, 2);
		}
	}

	/**
	 * 删除
	 * 
	 * @author fuzhh Oct 25, 2012
	 */
	private void deleteButPerformed() {
		targetLabel.setText("");
		int rowCount = jecnTable.getSelectedRow();
		if (rowCount == -1) {
			targetLabel.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		listJecnRefIndicatorsT.remove(rowCount);
		((DefaultTableModel) jecnTable.getModel()).removeRow(rowCount);
	}

	/***************************************************************************
	 * 初始化内容
	 * 
	 * @return
	 */
	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		try {
			if (jecnActiveData != null && jecnActiveData.getListRefIndicatorsT() != null) {
				for (JecnRefIndicatorsT jecnRefIndicatorsT : jecnActiveData.getListRefIndicatorsT()) {
					Vector<String> row = new Vector<String>();
					row.add(String.valueOf(jecnRefIndicatorsT.getId()));
					// 活动KPI(指标名称)
					row.add(jecnRefIndicatorsT.getIndicatorName());
					// 目标值(指标值)
					row.add(jecnRefIndicatorsT.getIndicatorValue());
					JecnRefIndicatorsT jecnRefIndicatorsTData = new JecnRefIndicatorsT();
					jecnRefIndicatorsTData.setId(jecnRefIndicatorsT.getId());
					jecnRefIndicatorsTData.setActivityId(jecnRefIndicatorsT.getActivityId());
					jecnRefIndicatorsTData.setIndicatorName(jecnRefIndicatorsT.getIndicatorName());
					jecnRefIndicatorsTData.setIndicatorValue(jecnRefIndicatorsT.getIndicatorValue());
					this.listJecnRefIndicatorsT.add(jecnRefIndicatorsTData);
					vs.add(row);
				}
			}

		} catch (Exception e) {
			log.error("ActiveTargetPanel getContent is error", e);
		}
		return vs;
	}

	/**
	 * 生成table的Model
	 * 
	 * @return
	 */
	private JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		title.add(JecnProperties.getValue("activeKPI"));
		title.add(JecnProperties.getValue("activeKPIValue"));
		return new JecnTableModel(title, getContent());
	}

	public List<JecnRefIndicatorsT> getListJecnRefIndicatorsT() {
		return listJecnRefIndicatorsT;
	}

	public void setListJecnRefIndicatorsT(List<JecnRefIndicatorsT> listJecnRefIndicatorsT) {
		this.listJecnRefIndicatorsT = listJecnRefIndicatorsT;
	}

	public boolean isUpdate() {
		if (listJecnRefIndicatorsT.size() != jecnActiveData.getListRefIndicatorsT().size()) {
			return true;
		}
		for (JecnRefIndicatorsT newIndicatorsT : listJecnRefIndicatorsT) {
			boolean isExist = false;
			for (JecnRefIndicatorsT oldIndicatorsT : jecnActiveData.getListRefIndicatorsT()) {
				if (DrawCommon.checkLongSame(newIndicatorsT.getId(), oldIndicatorsT.getId())
						&& DrawCommon.checkStringSame(newIndicatorsT.getIndicatorName(), oldIndicatorsT
								.getIndicatorName())
						&& DrawCommon.checkStringSame(newIndicatorsT.getIndicatorValue(), oldIndicatorsT
								.getIndicatorValue())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		return false;
	}
}
