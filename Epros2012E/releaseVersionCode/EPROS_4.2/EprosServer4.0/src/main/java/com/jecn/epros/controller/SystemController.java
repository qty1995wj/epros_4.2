package com.jecn.epros.controller;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.system.SyncConfig;
import com.jecn.epros.configuration.rpc.RPCServiceConfiguration;
import com.jecn.epros.configuration.runner.ConfigLoadRunner;
import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.ViewResultWithMsg;
import com.jecn.epros.domain.ViewRoleAuth;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.ProcessBaseInfoBean;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.system.*;
import com.jecn.epros.domain.system.Dictionary;
import com.jecn.epros.domain.user.TreeNodeAccess;
import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.*;
import com.jecn.epros.service.config.ConfigService;
import com.jecn.epros.service.process.ProcessService;
import com.jecn.epros.service.system.DictionaryService;
import com.jecn.epros.service.system.SystemService;
import com.jecn.epros.service.user.UserService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.service.web.JecnIflytekSyncUserEnum;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.*;
import com.jecn.epros.util.excel.ExcelUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

@RestController
public class SystemController {

    @Autowired
    private SystemService systemService;
    @Autowired
    private IUserImportRPCService userImportRPCService;
    @Autowired
    private IOpenServiceInterface openServiceInterface;
    @Autowired
    private UserService userService;
    @Autowired
    private IDocRPCService docRPCService;
    @Autowired
    private IProcessRPCService processRPCService;
    @Autowired
    private DictionaryService dictionaryService;

    @Autowired
    private IMixRPCService mixRPCService;

    @Autowired
    private ConfigService configService;
    @Autowired
    private ProcessService processService;

    /**
     * 人员管理
     *
     * @param userWebSearchBean
     * @param paging
     * @return
     */
    @PreAuthorize("hasAuthority('userManage')")
    @RequestMapping(value = "/system/users", method = RequestMethod.GET)
    public ResponseEntity<UserManageBean> userManage(
            @ModelAttribute UserWebSearchBean userWebSearchBean, @ModelAttribute Paging paging) {
        UserManageBean managerBean = new UserManageBean();
        //科大讯飞 查询人员需要 根据真实姓名查询 登录名后 根据登录名查询具体人员
        userWebSearchBean.setIfLoginName(IfytekCommon.getIflytekUserLoginName(userWebSearchBean.getTrueName()));
        int total = systemService.getTotalUser(userWebSearchBean);
        managerBean.setTotal(total);
        if (total > 0) {
            List<UserWebInfoBean> users = systemService.fetchUserInfos(userWebSearchBean, paging);
            managerBean.setData(users);
        }
        return ResponseEntity.ok(managerBean);
    }

    @ApiOperation(value = "系统-获取功能权限/默认权限")
    @RequestMapping(value = "/system/users/default_authorities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNodeAccess>> getDefaultWebAccess() {
        List<TreeNodeAccess> list = userService.getDefaultWebAccess();
        return ResponseEntity.ok(list);
    }

    @ApiOperation(value = "系统-更新功能权限/默认权限")
    @RequestMapping(value = "/system/users/default_authorities", method = RequestMethod.POST)
    public ResponseEntity<?> updateDefaultWebAccess() {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        paramMap.put("menusList", new ArrayList<String>());
        userService.updateDefaultWebAccess(paramMap);
        return ResponseEntity.ok(null);
    }

    /**
     * 角色管理 设计分布图
     *
     * @param roleName
     * @param paging
     * @return
     */
    @PreAuthorize("hasAuthority('roleManage')")
    @ApiOperation(value = "角色管理 设计分布图")
    @RequestMapping(value = "/system/roles", method = RequestMethod.GET)
    public ResponseEntity<?> roleManage(String orgName, String roleId, String roleName,
                                        boolean isDownload, HttpServletRequest request, @ModelAttribute Paging paging) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("orgName", orgName);
        paramMap.put("roleId", roleId);
        paramMap.put("roleName", roleName);
        paramMap.put("isDownLoad", isDownload);
        String inventoryName = JecnProperties.getValue("designMap") + ".xls";
        //查询全部系统角色
        //List<RoleWebInfoBean> roles = systemService.fetchUserRoles(" ");
        Map<String, String> roleMap = openServiceInterface.findSytemKeyAndUserNumber();
        //查询设计分布图报表
        RoleManageBean managerBean = systemService.fetchUserRoleInfos(paramMap, paging);
        if (roleMap != null) {
            boolean keyType = Boolean.valueOf(roleMap.get("KEY_TYPE").toString());//秘钥类型国际化
            roleMap.put("KEY_TYPE_NAME", keyType ? JecnProperties.getValue("temporary") : JecnProperties.getValue("forever"));
        }
        managerBean.setRoleMap(roleMap);
        if (StringUtils.isNotBlank(inventoryName) && isDownload) {
            Attachment attachment = new Attachment();
            attachment.setContent(ExcelUtils.inventoryToByteArray(managerBean.getRoleData()));
            attachment.setName(inventoryName);
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        }
        return ResponseEntity.ok(managerBean);
    }

    /**
     * 浏览角色
     *
     * @param roleSearchName
     * @param paging
     * @return
     */
    @PreAuthorize("hasAuthority('viewRole')")
    @RequestMapping(value = "/system/viewRoles", method = RequestMethod.GET)
    public ResponseEntity<ViewRoleManageBean> viewRoleManage(
            @RequestParam String roleSearchName, @ModelAttribute Paging paging) {
        ViewRoleManageBean managerBean = new ViewRoleManageBean();
        int total = systemService.getTotalViewRole(roleSearchName);
        managerBean.setTotal(total);
        if (total > 0) {
            List<ViewRoleDetailBean> roles = systemService.fetchViewRoles(roleSearchName, paging);
            managerBean.setData(roles);
        }
        return ResponseEntity.ok(managerBean);
    }

    @ApiOperation(value = "获取角色权限")
    @PreAuthorize("hasAuthority('viewRole')")
    @RequestMapping(value = "/system/viewRoleAuthList", method = RequestMethod.GET)
    public ResponseEntity<List<TreeNode>> viewRoleAuthList(@RequestParam String roleId) {
        List<TreeNode> list = systemService.getViewRoleAuthList(roleId);
        return ResponseEntity.ok(list);
    }

    @ApiOperation(value = "保存角色权限")
    @PreAuthorize("hasAuthority('viewRole')")
    @RequestMapping(value = "/system/saveViewRoleAuthList", method = RequestMethod.POST)
    public ResponseEntity<ViewResultWithMsg> saveViewRoleAuthList(@RequestBody ViewRoleAuth viewRoleAuth) {
        systemService.saveViewRoleAuthList(viewRoleAuth);
        ViewResultWithMsg viewResultWithMsg = new ViewResultWithMsg(true, JecnProperties.getValue("SaveSuccess"));
        return ResponseEntity.ok(viewResultWithMsg);
    }

    /**
     * 流程检错
     *
     * @param type
     * @param paging
     * @return
     */
    @PreAuthorize("hasAuthority('ferror')")
    @RequestMapping(value = "/system/processes/errors", method = RequestMethod.GET)
    public ResponseEntity<ProcessCheckBean> processCheckResult(
            @RequestParam Integer type, @ModelAttribute Paging paging) {
        long projectId = AuthenticatedUserUtil.getProjectId();
        ProcessCheckBean checkBean = new ProcessCheckBean();
        int total = systemService.getProcessCheckCount(type, projectId);
        checkBean.setTotal(total);
        if (total > 0) {
            // 获取流程检错信息
            List<ProcessBaseInfoBean> errorProcesses = systemService.fetchProcessCheckList(type, projectId, paging);
            checkBean.setData(errorProcesses);
        }
        return ResponseEntity.ok(checkBean);
    }


    @PreAuthorize("hasAuthority('sync')")
    @ApiOperation(value = "模板下载")
    @RequestMapping(value = "/system/data/template", method = RequestMethod.GET)
    public ResponseEntity<?> downloadDataTemplate(HttpServletRequest request) {
        ByteFileResult result = userImportRPCService.downUserTemplet();
        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(result));
    }

    @PreAuthorize("hasAuthority('sync')")
    @ApiOperation(value = "导出错误数据")
    @RequestMapping(value = "/system/data/errors", method = RequestMethod.GET)
    public ResponseEntity<?> downloadDataErrors(HttpServletRequest request) {
        ByteFileResult result = userImportRPCService.downUserErrorData();
        if (result.isSuccess()) {
            return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(result));
        } else {
            return ResponseEntity.ok(result);
        }
    }

    @PreAuthorize("hasAuthority('sync')")
    @ApiOperation(value = "导出变更数据")
    @RequestMapping(value = "/system/data/changes", method = RequestMethod.GET)
    public ResponseEntity<?> downloadDataChanges(HttpServletRequest request) {
        ByteFileResult result = userImportRPCService.downUserChangeData();
        if (result.isSuccess()) {
            return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(result));
        } else {
            return ResponseEntity.ok(result);
        }
    }

    @PreAuthorize("hasAuthority('sync')")
    @ApiOperation(value = "人员导出")
    @RequestMapping(value = "/system/data", method = RequestMethod.GET)
    public ResponseEntity<?> downloadData(HttpServletRequest request) {
        ByteFileResult result = userImportRPCService.downUserData();
        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(result));
    }

    @PreAuthorize("hasAuthority('sync')")
    @ApiOperation(value = "人员导入excel方式")
    @RequestMapping(value = "/system/data/xls", method = RequestMethod.POST)
    public ResponseEntity<?> importData(@RequestParam(value = "file", required = true) MultipartFile mfile) {

        ByteFile file = new ByteFile();
        file.setFileName(mfile.getName());
        try {
            file.setBytes(mfile.getBytes());
        } catch (IOException e) {
            throw new EprosBussinessException("通过excel方式人员导入，获得上传的文件异常", e);
        }
        MessageResult result = userImportRPCService.importUserData(file);
        return ResponseEntity.ok(result);
    }

    @PreAuthorize("hasAuthority('sync')")
    @ApiOperation(value = "人员db同步")
    @RequestMapping(value = "/system/data/db", method = RequestMethod.POST)
    public ResponseEntity<?> importDataByDB() {
        MessageResult result = userImportRPCService.importUserDataByDB();
        return ResponseEntity.ok(result);
    }

    @PreAuthorize("hasAuthority('sync')")
    @ApiOperation(value = "人员db同步配置项保存")
    @RequestMapping(value = "/system/data/conf", method = RequestMethod.POST)
    public ResponseEntity<?> saveSyncConfig(@RequestBody DBSyncConfig config) {
        if (config.getIntervalDay() == null || config.getIsAuto() == null || StringUtils.isBlank(config.getStartTime())) {
            throw new EprosBussinessException("SystemController saveSyncConfig is error");
        }
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", config.getStartTime());
        params.put("intervalDay", config.getIntervalDay());
        params.put("isAuto", config.getIsAuto());
        MessageResult result = userImportRPCService.saveSyncConfig(params);
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "下载操作说明")
    @RequestMapping(value = "/downloadDoc", method = RequestMethod.GET)
    public ResponseEntity<?> downloadDoc(@RequestParam("id") String id, @RequestParam("type") String type, @RequestParam("publish") int publish, @RequestParam("fileType") String fileType, @RequestParam(required = false) Long historyId, HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        // process processMap rule
        params.put("type", type);
        params.put("isPub", publish == 1);
        params.put("curPeopleId", AuthenticatedUserUtil.getPeopleId());
        params.put("historyId", historyId);
        params.put("fileType", fileType);
        ByteFile byteFile = docRPCService.downloadDoc(params);
        if (byteFile == null) {
            throw new EprosBussinessException("SystemController downloadDoc is error id:" + id + "    type:" + type);
        }
        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(byteFile));
    }

    @ApiOperation(value = "下载操作说明(含附件)")
    @RequestMapping(value = "/downloadDocEnclosure", method = RequestMethod.GET)
    public ResponseEntity<?> downLoadDocContainEnclosure(String id, String type,
                                                         String isPub, HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("type", type);
        params.put("isPub", isPub == null ? true : "1".equals(isPub));
        params.put("curPeopleId", AuthenticatedUserUtil.getPeopleId());
        ByteFile byteFile = null;
        try {
            byteFile = docRPCService.downLoadDocContainEnclosure(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (byteFile == null) {
            throw new EprosBussinessException("SystemController downLoadDocContainEnclosure is error id:" + id + "    type:" + type);
        }
        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(byteFile));
    }

    @PreAuthorize("hasAuthority('sync')")
    @ApiOperation(value = "获得同步类型")
    @RequestMapping(value = "/system/sync_config_url", method = RequestMethod.GET)
    public ResponseEntity<SyncConfig> syncConfigUrl() {
        SyncConfig config = userImportRPCService.getSyncConfig();
        return ResponseEntity.ok(config);
    }

    @PreAuthorize("hasAuthority('expiryManage')")
    @ApiOperation(value = "流程有效期数据下载")
    @RequestMapping(value = "/system/expiry_download", method = RequestMethod.GET)
    public ResponseEntity<?> expiryDownload(String orgIds, String mapIds, Integer type, HttpServletRequest request) {
        ByteFile byteFile = processRPCService.downloadExpiryMaintenance(orgIds, mapIds, AuthenticatedUserUtil.getProjectId(), type);
        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(byteFile));
    }

    @PreAuthorize("hasAuthority('expiryManage')")
    @ApiOperation(value = "流程有效期数据导入")
    @RequestMapping(value = "/system/expiry_import", method = RequestMethod.POST)
    public ResponseEntity<?> expiryImport(@RequestParam(value = "file", required = true) MultipartFile mfile, Integer type) {
        MessageResult messageResult = processRPCService.importExpiryMaintenance(Utils.multipartFileToByteFile(mfile), AuthenticatedUserUtil.getPeopleId(), type);
        return ResponseEntity.ok(messageResult);
    }

    @PreAuthorize("hasAuthority('expiryManage')")
    @ApiOperation(value = "导出流程有效期错误数据")
    @RequestMapping(value = "/system/expiry_download_error_data", method = RequestMethod.GET)
    public ResponseEntity<?> expiryDownloadErrorData(HttpServletRequest request, Integer type) {
        ByteFileResult byteFileResult = processRPCService.downloadExpiryMaintenanceErrorData(AuthenticatedUserUtil.getPeopleId(), type);
        if (!byteFileResult.isSuccess()) {
            return ResponseEntity.ok(byteFileResult);
        }
        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(byteFileResult));
    }

    @ApiOperation(value = "获取数据字典列表")
    @PreAuthorize("hasAuthority('dictionaryManage')")
    @RequestMapping(value = "/system/dictionary_list", method = RequestMethod.GET)
    public ResponseEntity<DictionaryManage> dictionaryList() {
        DictionaryManage dm = new DictionaryManage();
        //获取总共的字典项目
        dm.setTransName(dictionaryService.getDictionaryItems());
        dm.setOldData(dictionaryService.getDictionaryAll());
        return ResponseEntity.ok(dm);
    }

    @ApiOperation(value = "获取数据字典单项列表")
    @PreAuthorize("hasAuthority('dictionaryManage')")
    @RequestMapping(value = "/system/dictionary_list/{name}", method = RequestMethod.GET)
    public ResponseEntity<List<Dictionary>> dictionaryList(@PathVariable String name) {
        List<Dictionary> list = dictionaryService.getDictionaryByName(DictionaryEnum.valueOf(name));
        return ResponseEntity.ok(list);
    }

    @ApiOperation(value = "保存数据字典列表")
    @PreAuthorize("hasAuthority('dictionaryManage')")
    @RequestMapping(value = "/system/save_dictionary_list", method = RequestMethod.POST)
    public ResponseEntity<MessageResult> saveDictionaryList(@RequestBody(required = true) DictionaryManage dictionaryManage) {
        MessageResult messageResult = new MessageResult();
        dictionaryService.saveDictionary(dictionaryManage);
        //刷新字典项
        Utils.flushDictinary(dictionaryService.fetchAllDictionary());
        try {
            mixRPCService.syncDictinaryConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        messageResult.setSuccess(true);
        messageResult.setResultMessage(JecnProperties.getValue("SaveSuccess"));
        return ResponseEntity.ok(messageResult);
    }

    @ApiOperation(value = "用户设置设置国际化语言")
    @RequestMapping(value = "/system/settingUserLanguage/{type}", method = RequestMethod.GET)
    public ResponseEntity<?> settingUserLanguage(@PathVariable int type) {
        //type 0中文 1英文
        ViewResultWithMsg result = JecnProperties.settingUserLanguage(type);
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "添加编辑自定义浏览角色以及相关信息")
    @PreAuthorize("hasAuthority('viewRole')")
    @RequestMapping(value = "/system/addRole", method = RequestMethod.POST)
    public ResponseEntity<?> addRole(@RequestBody(required = true) ViewRoleDetailBean role) {
        MessageResult result = systemService.addRole(role);
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "获取自定义浏览角色相关信息")
    @RequestMapping(value = "/system/findRolesInfo", method = RequestMethod.POST)
    public ResponseEntity<?> findRolesInfo(@RequestBody ViewRoleDetailBean role) {
        List<Map<String, Object>> maps = systemService.findRolesInfo(role);
        return ResponseEntity.ok(maps);
    }

    @ApiOperation(value = "删除自定义角色")
    @PreAuthorize("hasAuthority('viewRole')")
    @RequestMapping(value = "/system/deleteRole", method = RequestMethod.POST)
    public ResponseEntity<?> deleteRole(@RequestBody ViewRoleDetailBean role) {
        MessageResult result = systemService.deleteRole(role);
        return ResponseEntity.ok(result);
    }

//    @ApiOperation(value = "下载设计器安装包")
//    @RequestMapping(value = "/system/downLoadEprosDesigner", method = RequestMethod.GET)
//    public ResponseEntity<?> downLoadEprosDesigner(HttpServletRequest request) {
////        ByteFileResult byteFile = openServiceInterface.downLoadEprosDesigner();
////        if (byteFile == null) {
////            throw new EprosBussinessException("SystemController downLoadEprosDesigner is error : FILE IS NULL!");
////        }
////        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(byteFile));
//    }

    @ApiOperation(value = "通知刷新配置项")
    @RequestMapping(value = "/system/sync_system_config", method = RequestMethod.POST)
    public void syncSystemConfig(@RequestParam(required = false) String ids) {
        if (StringUtils.isNotBlank(ids)) {
            if (!ids.startsWith("(") && !ids.endsWith(")")) {
                ids = "(" + ids + ")";
            }
            List<ConfigItemBean> configItemBeans = configService.fetchConfig(ids);
            for (ConfigItemBean config : configItemBeans) {
                ConfigLoadRunner.ConfigMap.insertOrUpdateConfig(config);
            }
        } else {
            List<ConfigItemBean> configItemBeans = configService.fetchAllConfig();
            ConfigLoadRunner.ConfigMap.initSystemConfig(configItemBeans);
        }
    }

    @ApiOperation(value = "旧服务地址")
    @RequestMapping(value = "/system/old_server", method = RequestMethod.GET)
    public ResponseEntity<MessageResult> getOldServer() {
        MessageResult msg = new MessageResult();
        msg.setSuccess(true);
        msg.setResultMessage(RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL);
        return ResponseEntity.ok(msg);
    }
}
