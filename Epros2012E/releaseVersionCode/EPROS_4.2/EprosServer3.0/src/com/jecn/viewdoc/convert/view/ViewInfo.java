package com.jecn.viewdoc.convert.view;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.standard.IStandardService;
import com.jecn.viewdoc.service.IViewDocService;

public class ViewInfo {
	private IFileService fileService;
	private IRuleService ruleService;
	private IStandardService standardService;
	private IFlowStructureService flowStructureService;
	private IViewDocService viewDocService;
	private boolean isApp;
	/**
	 * 在线预览类型：文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3 ;
	 * 4、流程（文件）已发布，5：流程（文件）未发布;6 :标准附件; 7:流程图/架构图图片 已发布; 8:流程图/架构图图片 未发布
	 * 9：流程文件word 已发布;10：流程文件word 未发布;11:制度模板 已发布 ；12：制度模板未发布
	 **/
	private int type;
	private boolean isPub;
	private Long relatedId;
	private String relatedName;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private IWebProcessService webProcessService;

	private Long userId;

	private Long hId;

	private boolean isRuleFileLocal;

	public IFileService getFileService() {
		return fileService;
	}

	public void setFileService(IFileService fileService) {
		this.fileService = fileService;
	}

	public IRuleService getRuleService() {
		return ruleService;
	}

	public void setRuleService(IRuleService ruleService) {
		this.ruleService = ruleService;
	}

	public IStandardService getStandardService() {
		return standardService;
	}

	public void setStandardService(IStandardService standardService) {
		this.standardService = standardService;
	}

	public IFlowStructureService getFlowStructureService() {
		return flowStructureService;
	}

	public void setFlowStructureService(IFlowStructureService flowStructureService) {
		this.flowStructureService = flowStructureService;
	}

	public IViewDocService getViewDocService() {
		return viewDocService;
	}

	public void setViewDocService(IViewDocService viewDocService) {
		this.viewDocService = viewDocService;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public String getRelatedName() {
		return relatedName;
	}

	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public boolean isApp() {
		return isApp;
	}

	public void setApp(boolean isApp) {
		this.isApp = isApp;
	}

	public boolean isPub() {
		return isPub;
	}

	public void setPub(boolean isPub) {
		this.isPub = isPub;
	}

	public IWebProcessService getWebProcessService() {
		return webProcessService;
	}

	public void setWebProcessService(IWebProcessService webProcessService) {
		this.webProcessService = webProcessService;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long gethId() {
		return hId;
	}

	public void sethId(Long hId) {
		this.hId = hId;
	}

	public boolean isRuleFileLocal() {
		return isRuleFileLocal;
	}

	public void setRuleFileLocal(boolean isRuleFileLocal) {
		this.isRuleFileLocal = isRuleFileLocal;
	}

}
