/*
 * JecnTableSingleColumnEditModel.java
 *
 * Created on 2010年7月15日, 上午9:35
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package epros.designer.gui.task.buss;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

/**
 * 单列可以双击编辑的Tabel的model
 * 
 * @author Administrator
 */
public class JecnTableSingleColumnEditModel extends DefaultTableModel {
	int columnEdit = 0;

	/** Creates a new instance of JecnTableSingleColumnEditModel */
	public JecnTableSingleColumnEditModel(Vector data, Vector columnNames,
			int columnEdit) {
		super(data, columnNames);
		this.columnEdit = columnEdit;
	}

	public JecnTableSingleColumnEditModel(Object[][] cellData,
			String[] columnNames, int columnEdit) {
		super(cellData, columnNames);
		this.columnEdit = columnEdit;
	}

	public boolean isCellEditable(int row, int column) {
		if (column != columnEdit) {
			return false;
		}
		return true;
	}
}
