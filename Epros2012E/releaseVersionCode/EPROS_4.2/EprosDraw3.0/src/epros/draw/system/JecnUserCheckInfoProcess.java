package epros.draw.system;

import java.util.Properties;

import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 读取本地用户提示信息,系统配置信息
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUserCheckInfoProcess {
	/**
	 * 
	 * 加载本地资源
	 * 
	 */
	public static void start() {
		// 初始化用户提示信息
		initUserCHeckInfoData();
		// 读取设置文件中内容
		readSetFile();
		// 公司名称、是否使用logo标识 、是否显示活动说明中输入输出等
		JecnFileUtil.readCompName();
	}

	/**
	 * 
	 * 初始化用户提示信息
	 * 
	 */
	public static void initUserCHeckInfoData() {
		// 不能超过122个字符或61个汉字
		String nameLengthInfo = JecnResourceUtil.getJecnResourceUtil().getValue("nameLengthInfo");
		// 不能超过1200个字符或600个汉字
		String noteLengthInfo = JecnResourceUtil.getJecnResourceUtil().getValue("noteLengthInfo");
		// 不能包括任意字符之一: \ / : * ? < > | # %
		String nameErrorCharInfo = JecnResourceUtil.getJecnResourceUtil().getValue("nameErrorCharInfo");
		// 不能为空
		String nameNotNull = JecnResourceUtil.getJecnResourceUtil().getValue("notNull");
		// 文件名称长度不能超过211
		String fileLengthInfo = JecnResourceUtil.getJecnResourceUtil().getValue("fileLengthInfo");

		JecnUserCheckInfoData.setNameLengthInfo(nameLengthInfo);
		JecnUserCheckInfoData.setNoteLengthInfo(noteLengthInfo);
		JecnUserCheckInfoData.setNameErrorCharInfo(nameErrorCharInfo);
		JecnUserCheckInfoData.setNameNotNull(nameNotNull);
		JecnUserCheckInfoData.setFileLengthInfo(fileLengthInfo);
	}

	/**
	 * 
	 * 读取设置文件中内容
	 * 
	 */
	private static void readSetFile() {
		Properties setFile = JecnFileUtil.readProperties("setFile.properties");

		String formatAutoWidth = setFile.getProperty("formatAutoWidth");
		String formatAutoHeigth = setFile.getProperty("formatAutoHeigth");

		JecnSystemStaticData.setWorkflowResizeWidth(Integer.parseInt(formatAutoWidth));
		JecnSystemStaticData.setWorkflowResizeHeight(Integer.parseInt(formatAutoHeigth));
		setFile = null;
	}

	/**
	 * 
	 * 读取设置文件中内容
	 * 
	 */
	public static void writeSetFile() {
		// 文件路径
		String path = "setFile.properties";
		// 读文件
		Properties setFile = JecnFileUtil.readProperties(path);

		String width = String.valueOf(JecnSystemStaticData.getWorkflowResizeWidth());
		String height = String.valueOf(JecnSystemStaticData.getWorkflowResizeHeight());

		setFile.setProperty("formatAutoWidth", width);
		setFile.setProperty("formatAutoHeigth", height);
		// 写文件
		JecnFileUtil.writeProperties(path, setFile, null);

		setFile = null;
	}
}
