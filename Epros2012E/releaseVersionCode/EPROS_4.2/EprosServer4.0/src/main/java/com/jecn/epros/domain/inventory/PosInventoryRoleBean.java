package com.jecn.epros.domain.inventory;

import java.util.List;

public class PosInventoryRoleBean {
    private Long roleId;
    private String roleName;
    private String roleRes;
    private Long flowId;
    private String flowName;
    private List<FileInventoryCommonBean> listFileInventoryCommonBean;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleRes() {
        return roleRes;
    }

    public void setRoleRes(String roleRes) {
        this.roleRes = roleRes;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public List<FileInventoryCommonBean> getListFileInventoryCommonBean() {
        return listFileInventoryCommonBean;
    }

    public void setListFileInventoryCommonBean(List<FileInventoryCommonBean> listFileInventoryCommonBean) {
        this.listFileInventoryCommonBean = listFileInventoryCommonBean;
    }
}
