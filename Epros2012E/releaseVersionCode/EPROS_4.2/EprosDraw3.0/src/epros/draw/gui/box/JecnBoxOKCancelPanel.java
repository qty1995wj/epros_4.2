package epros.draw.gui.box;

import java.awt.Color;
import java.awt.GridBagConstraints;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnActionCommandConstants;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.swing.JecnOKCancelPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 确认取消按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnBoxOKCancelPanel extends JecnPanel {
	/** 资源文件管理对象 */
	private JecnResourceUtil resourceManager = null;
	/** 确定 */
	private JButton okBtn = null;
	/** 取消 */
	private JButton cancelBtn = null;
	/** 空闲区域 */
	private JLabel freeLabel = null;

	/** 非管理员提示 */
	private JLabel tipTitle;

	public JecnBoxOKCancelPanel() {

		initComponents();
		initBtnPanelLayout();
	}

	private void initComponents() {
		// 资源文件管理对象
		resourceManager = JecnResourceUtil.getJecnResourceUtil();

		// 空闲区域
		freeLabel = new JLabel();
		// 确定
		okBtn = new JButton(resourceManager.getValue(JecnActionCommandConstants.OK_BUTTON));
		okBtn.setActionCommand(JecnActionCommandConstants.OK_BUTTON);
		// 取消
		cancelBtn = new JButton(resourceManager.getValue(JecnActionCommandConstants.CANCEL_BUTTON));
		cancelBtn.setActionCommand(JecnActionCommandConstants.CANCEL_BUTTON);

		tipTitle = new JLabel(resourceManager.getValue("toAddSymbol"));
		freeLabel.setOpaque(false);
		freeLabel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		freeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		freeLabel.setForeground(Color.red);
	}

	/**
	 * 
	 * 按钮区域
	 * 
	 */
	private void initBtnPanelLayout() {

		// 空闲区域
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.SOUTHEAST,
				GridBagConstraints.BOTH, JecnUIUtil.getInsets0(), 0, 0);
		this.add(freeLabel, c);

		if (JecnDesignerProcess.getDesignerProcess().isAdmin()) {
			// 确定
			c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0555(), 0, 0);
			this.add(okBtn, c);
		}else{
			// 设置验证提示文字颜色
			tipTitle.setForeground(Color.red);
			c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0555(), 0, 0);
			this.add(tipTitle, c);
		}

		// 取消
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0555(), 0, 0);
		this.add(cancelBtn, c);

	}

	public JButton getOkBtn() {
		return okBtn;
	}

	public JButton getCancelBtn() {
		return cancelBtn;
	}

	public JLabel getFreeLabel() {
		return freeLabel;
	}
}
