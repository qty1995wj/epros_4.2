package com.jecn.epros.server.dao.file.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.file.JecnFileBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.JecnSqlConstant;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;

public class FileDaoImpl extends AbsBaseDao<JecnFileBeanT, Long> implements IFileDao {

	@Override
	public JecnFileBean findJecnFileBeanById(Long fileId) throws Exception {
		String hql = "from JecnFileBean where fileID=? and delState=0";
		return this.getObjectHql(hql, fileId);
	}

	@Override
	public void deleteFiles(Set<Long> setFileIds, Set<Long> setDirIds) throws Exception {
		/** 需要删除的本地文件路径 */
		Set<String> listStrDelFilePaths = new HashSet<String>();
		// 删除文件
		if (setFileIds.size() > 0) {
			// 公司logo
			String hql = "update JecnFlowStructureImageT set linkFlowId =-1 where figureType = "
					+ JecnCommonSql.getIconString() + " and linkFlowId in";
			List<String> list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 制度 start
			// 更新制度图标
			hql = "update JecnFlowStructureImageT set linkFlowId =-1 where figureType = "
					+ JecnCommonSql.getSystemString()
					+ " and linkFlowId in (select id from RuleT where isDir=2 and fileId in";
			list = JecnCommonSql.getSetSqlAddBracket(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 删除制度文件-设计器
			hql = "delete from RuleT where isDir=2 and fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 删除制度文件-浏览端
			hql = "delete from Rule where isDir=2 and fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 删除标准制度-设计器
			hql = "delete from RuleFileT where ruleFileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 删除标准制度-浏览端
			hql = "delete from RuleFile where ruleFileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 制度 end
			// 标准 start
			hql = "delete from StandardBean where stanType =1 and relationId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 标准 end

			// 流程 start
			// 获得JecnMainFlow通过fileId
			hql = "update JecnMainFlow set fileId=-1 where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			hql = "update JecnMainFlowT set fileId=-1 where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 获得JecnFlowBasicInfoT通过fileId-设计器
			hql = "update JecnFlowBasicInfoT set fileId=-1 where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 获得JecnFlowBasicInfo通过fileId-浏览端
			hql = "update JecnFlowBasicInfo set fileId=-1 where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}

			// 更新JecnPositionFigInfo里的岗位说明书附件信息
			hql = "update JecnPositionFigInfo set fileId=-1 where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}

			// 流程活动样例-浏览端
			hql = "delete from JecnTemplet where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 流程活动样例-设计器
			hql = "delete from JecnTempletT where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 流程输出模板-设计器
			hql = "delete from JecnModeFileT where fileMId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 流程输出模板-浏览端
			hql = "delete from JecnModeFile where fileMId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 流程输入、操作规范模板-设计器
			hql = "delete from JecnActivityFileT where fileSId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 流程输入、操作规范模板-浏览端
			hql = "delete from JecnActivityFile where fileSId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 流程 end

			// 删除查阅权限
			JecnUtil.deleteViewAuth(setFileIds, 1, this);
			// 删除文控信息
			JecnUtil.deleteRecord(setFileIds, 1, this);
			// 删除任务
			JecnUtil.deleteTask(setFileIds, 1, this);
			// 删除本地文件的版本文件
			hql = "select filePath from JecnFileContent where isVersionLocal=0 and fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				List<String> listFilePaths = this.listHql(str);
				for (String filePath : listFilePaths) {
					listStrDelFilePaths.add(filePath);
				}
			}
			// 删除数据库内容
			hql = "delete from JecnFileContent where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 文件
			hql = "delete from JecnFileBean where fileID in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 文件
			hql = "delete from JecnFileBeanT where fileID in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}

			// 制度相关标准化文件
			hql = "delete from RuleStandardizedFile where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}

			hql = "delete from RuleStandardizedFileT where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}

			hql = "delete from FlowStandardizedFile where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}

			// 流程相关标准化文件
			hql = "delete from FlowStandardizedFileT where fileId in";
			list = JecnCommonSql.getSetSqls(hql, setFileIds);
			for (String str : list) {
				execteHql(str);
			}
			// 删除收藏
			String sql = "delete from jecn_my_star where type=4 and related_id in";
			list = JecnCommonSql.getSetSqls(sql, setFileIds);
			for (String str : list) {
				execteNative(str);
			}

			// 删除合理化建议 0流程，1制度，2流程架构，3文件，4风险，5标准
			JecnDaoUtil.deleteProposeByRelatedIds(setFileIds, 3, this);
		}
		// 删除目录
		if (setDirIds.size() > 0) {
			// 文件
			String hql = "delete from JecnFileBean where fileID in";
			List<String> list = JecnCommonSql.getSetSqls(hql, setDirIds);
			for (String str : list) {
				execteHql(str);
			}
			// 设计器权限
			JecnUtil.deleteDesignAuth(setDirIds, 1, this);
			/** 文件 */
			hql = "delete from JecnFileBeanT where fileID in";
			list = JecnCommonSql.getSetSqls(hql, setDirIds);
			for (String str : list) {
				execteHql(str);
			}
		}
		// 删除本地文件
		for (String filePath : listStrDelFilePaths) {
			JecnFinal.deleteFile(filePath);
		}

	}

	@Override
	public void deleteFiles(Long projectId) throws Exception {
		String sql = "SELECT JF.FILE_ID,JF.IS_DIR FROM JECN_FILE_T JF WHERE JF.PROJECT_ID=?";
		List<Object[]> listObjects = this.listNativeSql(sql, projectId);
		// 待删除的文件
		Set<Long> setFileIds = new HashSet<Long>();
		// 待删除的目录
		Set<Long> setDirIds = new HashSet<Long>();
		// 带删除的文件并且保存在本地不是保存在的数据库的文件集合
		for (Object[] obj : listObjects) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			if ("1".equals(obj[1].toString())) {
				setFileIds.add(Long.valueOf(obj[0].toString()));
			} else {
				setDirIds.add(Long.valueOf(obj[0].toString()));
			}
		}
		this.deleteFiles(setFileIds, setDirIds);
	}

	@Override
	public void releaseFiles(List<Long> listFileIds) throws Exception {
		this.revokeFiles(listFileIds);
		String sql = "update FILE_CONTENT set type=1 where id in (select t.VERSION_ID from jecn_file_t t where t.file_id in";
		List<String> listStr = JecnCommonSql.getListSqlAddBracket(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 查阅岗位
		sql = "insert into JECN_ACCESS_PERMISSIONS (ID, FIGURE_ID, RELATE_ID, TYPE,ACCESS_TYPE) "
				+ "SELECT ID, FIGURE_ID, RELATE_ID, TYPE,ACCESS_TYPE FROM JECN_ACCESS_PERMISSIONS_T WHERE type=1 and RELATE_ID in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 查阅部门
		sql = "insert into JECN_ORG_ACCESS_PERMISSIONS (ID, ORG_ID, RELATE_ID, TYPE,ACCESS_TYPE)"
				+ "SELECT ID, ORG_ID, RELATE_ID, TYPE,ACCESS_TYPE FROM JECN_ORG_ACCESS_PERM_T WHERE type=1 and RELATE_ID in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 将岗位组查阅权限从临时表插入到正式表
		sql = "insert into JECN_GROUP_PERMISSIONS(ID, POSTGROUP_ID, RELATE_ID, TYPE,ACCESS_TYPE) "
				+ " select ID, POSTGROUP_ID, RELATE_ID, TYPE,ACCESS_TYPE from JECN_GROUP_PERMISSIONS_T WHERE TYPE=1 AND RELATE_ID in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 文件相关标准
		sql = "insert into  FILE_RELATED_STANDARD(ID, STANDARD_ID, FILE_ID) "
				+ " select ID, STANDARD_ID, FILE_ID from FILE_RELATED_STANDARD_T WHERE FILE_ID in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 文件相关风险
		sql = "insert into  FILE_RELATED_RISK(ID, RISK_ID, FILE_ID) "
				+ " select ID, RISK_ID, FILE_ID from FILE_RELATED_RISK_T WHERE FILE_ID in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}

		Date pubTime = new Date();
		// 更新临时表的更新日期和发布日期
		sql = "update jecn_file_t set update_time=?,pub_time=? where file_id in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str, pubTime, pubTime);
		}

		// 插入正式表
		sql = JecnSqlConstant.JECN_FILE + " in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}

	}

	@Override
	public void revokeFiles(List<Long> listFileIds) throws Exception {
		// 删除正式表
		String sql = "delete from jecn_file where file_id in";
		List<String> listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 删除岗位查阅权限
		sql = "delete from jecn_access_permissions where type=1 and relate_id in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 删除部门查阅权限
		sql = "delete from jecn_org_access_permissions where type=1 and relate_id in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 删除岗位组查阅权限
		sql = "delete from jecn_group_permissions where type=1 and relate_id in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 删除文件相关标准
		sql = "delete from FILE_RELATED_STANDARD where FILE_ID in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
		// 删除文件相关风险
		sql = "delete from FILE_RELATED_RISK where FILE_ID in";
		listStr = JecnCommonSql.getListSqls(sql, listFileIds);
		for (String str : listStr) {
			this.execteNative(str);
		}
	}

	@Override
	public List<Long> getNoPubFiles(Set<Long> setFileIds) throws Exception {
		String sql = "select fileID from JecnFileBean where delState=0 and fileID in";
		List<Long> listResultIds = new ArrayList<Long>();
		Set<Long> setPubFileIds = new HashSet<Long>();
		List<String> listStr = JecnCommonSql.getSetSqls(sql, setFileIds);
		for (String str : listStr) {
			List<Long> listIds = this.listHql(str);
			for (Long id : listIds) {
				setPubFileIds.add(id);
			}
		}
		for (Long fileId : setFileIds) {
			boolean isExist = false;
			for (Long pubFileIds : setPubFileIds) {
				if (fileId.equals(pubFileIds)) {
					isExist = true;
					setPubFileIds.remove(pubFileIds);
					break;
				}
			}
			if (!isExist) {
				listResultIds.add(fileId);
			}
		}
		return listResultIds;
	}

	@Override
	public JecnFileBeanT findJecnFileBeanTById(Long fileId) throws Exception {
		String hql = "from JecnFileBeanT where fileID=? and delState=0";
		return this.getObjectHql(hql, fileId);
	}

	@Override
	public List<JecnTaskHistoryNew> getTaskHistoryNewListById(long fileId) throws Exception {
		// 通过ID找到制度标题
		String hql = "from JecnTaskHistoryNew where relateId=? and type=1 order by publishDate desc";
		return this.listHql(hql, fileId);
	}

	@Override
	public List<Object[]> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) {
		String convertHide = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			convertHide = "convert(varchar(2),t.hide) ";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			convertHide = "to_char(t.hide) ";
		} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
			convertHide = "convert(t.hide,char(2)) ";
		}
		String sql = "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
				+ "case when jft.file_id is null then 0 else 1 end as count," + "t.CREATE_TIME,t.savetype,t.doc_id"
				+ ", CASE WHEN t.is_dir=0 THEN " + convertHide
				+ " WHEN jf.file_id IS NULL THEN '0' ELSE '1' END AS is_pub,t.flow_id,t.sort_id"
				+ " from jecn_file_t t" + " LEFT JOIN jecn_file jf ON t.file_id = jf.file_id"
				+ " LEFT JOIN jecn_file_t jft on t.file_id = jft.per_file_id and jft.del_state=0";
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(1, peopleId, projectId);
		sql = sql
				+ " where t.per_file_id=? and t.project_id=? and t.del_state = 0 order by t.per_file_id,t.sort_id,t.file_id";
		return this.listNativeSql(sql, pId, projectId);
	}

	@Override
	public List<Object[]> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		String sql = "select t.FILE_ID," + " t.FILE_NAME," + " t.FILE_PATH," + " t.PEOPLE_ID," + " t.PER_FILE_ID,"
				+ " t.CREATE_TIME," + " t.UPDATE_TIME," + " t.IS_DIR," + " t.ORG_ID," + " t.DOC_ID," + " t.IS_PUBLIC,"
				+ " t.FLOW_ID," + " t.SORT_ID," + " t.SAVETYPE," + " t.UPDATE_PERSON_ID," + " t.PROJECT_ID,"
				+ " t.VERSION_ID," + " t.HISTORY_ID," + " to.org_name, " + " t.hide," + " t.del_state"
				+ " from jecn_file_t t" + " left join jecn_flow_org jfo on jfo.org_id = t.org_id and jfo.del_state=0";
		// 拼装角色权限sql
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthSqlCommon(2, peopleId, projectId);
		sql = sql + " where t.is_dir=1 and t.project_id=? and t.file_name like ? and t.del_state = 0"
				+ " order by t.per_file_id,t.sort_id,t.file_id";
		return this.listNativeSql(sql, 0, 20, projectId, "%" + name + "%");
	}
}
