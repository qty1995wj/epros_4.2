package com.jecn.epros.server.dao.dataImport.match;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.JecnUserInfo;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.dataImport.match.ActualDeptBean;
import com.jecn.epros.server.webBean.dataImport.match.ActualPosBean;
import com.jecn.epros.server.webBean.dataImport.match.ActualUserBean;
import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.PosEprosRelBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;

public interface IMatchUserImportDao extends IBaseDao<DeptMatchBean, Long> {
	/**
	 * 
	 * 清除临时库、入库临时库
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	void deleteAndInsertTmpJecnIOTable(BaseBean baseBean) throws Exception;

	/**
	 * 
	 * 通用部门临时表(JECN_SANFU_DEPARTMENT)：同一部门下部门名称不能重复
	 * 
	 * @return
	 * @throws Exception
	 */
	List<DeptMatchBean> checkDeptNameSame() throws Exception;

	/**
	 * 
	 * 待导入部门、岗位以及人员中在各自真实库中存在的数据， 更新其操作标识设置为1
	 * 
	 * @throws Exception
	 */
	void updateProFlag(Long curProjectId) throws Exception;

	/**
	 * 获取岗位变更的信息
	 * 
	 * @param this.getSession()
	 * @return List<Object[]>userList :0:岗位编号，1：登录名称
	 */
	List<Object[]> findJecnUserObject();

	/**
	 * 
	 * 根据部门临时表中标识获取插入的部门
	 * 
	 * @param this.getSession()
	 * @throws Exception
	 */
	void insertDept(Long curProjectId) throws Exception;

	/**
	 * 
	 * 插入部门临时表中标识是插入的岗位
	 * 
	 * @param this.getSession()
	 *            Session
	 * @throws Exception
	 */
	void insertPos(Long curProjectId) throws Exception;

	/**
	 * 
	 * 插入通用人员临时表中标识是插入的人员
	 * 
	 * @param this.getSession()
	 *            Session
	 * @throws Exception
	 */
	void insertUser() throws Exception;

	/**
	 * 获取要 添加 的人员临时表数据
	 * 
	 * @return
	 * @throws Exception
	 */
	List<DeptMatchBean> selectDeptBeanList(int flag) throws Exception;

	/**
	 * 
	 * 查询实际部门表(JECN_SANFU_ACTUAL_DEPT)中的数据
	 * 
	 * @return
	 * @throws Exception
	 */
	List<ActualDeptBean> selectActDept() throws Exception;

	/**
	 * 将部门临时表中标识为 1：更新 的数据插入到人员实际表中 将更新后的部门实际表信息 插入数据库
	 * 
	 * @return
	 * @throws Exception
	 */
	void updateActDeptUpList(List<ActualDeptBean> listUserInfo)
			throws Exception;

	/**
	 * 获取要 添加 的人员临时表数据
	 * 
	 * @return
	 * @throws Exception
	 */
	List<PosMatchBean> selectPosBeanList(int flag) throws Exception;

	/**
	 * 
	 * 查询实际岗位表(JECN_SANFU_ACTUAL_POS)中的数据
	 * 
	 * @return
	 * @throws Exception
	 */
	List<ActualPosBean> selectActPos() throws Exception;

	/**
	 * 将岗位临时表中标识为 1：更新 的数据插入到人员实际表中 将更新后的岗位实际表信息 插入数据库
	 * 
	 * @return
	 * @throws Exception
	 */
	void updateActPosUpList(List<ActualPosBean> listUserInfo) throws Exception;

	/**
	 * 获取要 添加 的人员临时表数据或待更新记录
	 * 
	 * @param flag
	 *            0:添加，1：更新
	 * @return
	 * @throws Exception
	 */
	List<UserMatchBean> selectUserBeanList(int flag) throws Exception;

	/**
	 * 
	 * 查询实际人员表(JECN_SANFU_ACTUAL_PERSON)中的数据
	 * 
	 * @return
	 * @throws Exception
	 */
	List<ActualUserBean> selectActUser() throws Exception;

	/**
	 * 将人员临时表中标识为 1：更新 的数据插入到人员实际表中 将更新后的人员实际表信息 插入数据库
	 * 
	 * @return
	 * @throws Exception
	 */
	void updateActUserUpList(List<ActualUserBean> listUserInfo)
			throws Exception;

	/**
	 * 
	 * 删除部门实际表：在部门临时表中不存在的数据信息
	 * 
	 */
	void deleteActDeptByDeptNum();

	/**
	 * 
	 * 删除岗位实际表：在岗位临时表中不存在的数据信息
	 * 
	 */
	void deleteActPosByActNum();

	/**
	 * 
	 * 删除人员实际表：在人员临时表中不存在的数据信息
	 * 
	 */
	void deleteActUserByLogiName();

	/**
	 * 根据 实际岗位表数据在岗位临时表中不存在的岗位编码 查询流程岗位与实际岗位关联表数据
	 * 
	 * @return
	 * @throws Exception
	 */
	List<Long> getFlowPosIdList() throws Exception;

	/**
	 * 根据岗位ID删除人员岗位关联信息
	 * 
	 * @throws Exception
	 */
	void deletePosRelUser() throws Exception;

	/**
	 * 
	 * 更新流程人员表：在人员实际表中不存在的数据 给予锁定状态,锁定 ISLOCK 0:解锁 1：锁定
	 * 
	 * @throws Exception
	 */
	void udateJecnUserIsLock() throws Exception;

	/***************************************************************************
	 * 删除流程岗位与实际岗位关联表
	 * 
	 * @param this.getSession()
	 * @throws Exception
	 */
	void deleteEpsActPos() throws Exception;

	/**
	 * 
	 * 查询流程岗位与实际岗位关联表（JECN_POSITION_EPROS_REL）中的数据
	 * 
	 * @return
	 * @throws Exception
	 */
	List<PosEprosRelBean> selectPosUserRel() throws Exception;


	/**
	 * 根据人员临时表获取待更新的人员信息
	 * 
	 * @param this.getSession()
	 * @return
	 * @throws Exception
	 */
	List<JecnUser> findJecnUser() throws Exception;

	/**
	 * 流程人员岗位匹配表
	 * 
	 * @return
	 * @throws Exception
	 */
	void insertJecnUserPos(List<JecnUserInfo> listUserInfo) throws Exception;

	/**
	 * 
	 * 获取通用部门临时表(TMP_JECN_IO_DEPT)项目ID集合
	 * 
	 * @return
	 * @throws Exception
	 */
	List<Long> selectProjectId() throws Exception;

	/***************************************************************************
	 * 添加 流程岗位与实际岗位关联表数据 JECN_POSITION_EPROS_REL
	 * 
	 * @param posEprosRelBean
	 * @return
	 * @throws Exception
	 */
	boolean addNewPosEpsData(List<PosEprosRelBean> posEprosRelList,
			List<JecnUserInfo> addJecnUserInfoList) throws Exception;

	/**
	 * 获取已存在的岗位Id集合
	 * 
	 * @param peopleIds
	 * @return
	 * @throws Exception
	 */
	List<Object[]> isExitsUserPos() throws Exception;

	/**
	 * 获取基准岗位对应岗位下的人员登录名称
	 * 
	 * @param baseNum
	 * @return
	 * @throws Exception
	 */
	List<Object> getLoginNameByBaseNum(List<String> posNumList, int type)
			throws Exception;

	/**
	 * 根据登录名称获取登录人的主键ID
	 * 
	 * @param listNames
	 * @return
	 * @throws Exception
	 */
	List<Long> getPeopleIds(List<String> listNames) throws Exception;

	/***************************************************************************
	 * 删除 流程岗位与实际岗位关联表数据 JECN_POSITION_EPROS_REL 删除 流程人员与岗位管理表
	 * JECN_USER_POSITION_RELATED
	 * 
	 * @param posEprosRelBean
	 * @return
	 * @throws Exception
	 */
	void deletePosEpsData(Long FlowPosID) throws Exception;

	/***************************************************************************
	 * 获取岗位匹配总数
	 * 
	 * @return
	 * @throws Exception
	 */
	int getPosEpsCount() throws Exception;

	/**
	 * 
	 * 获得页的条数
	 * 
	 * @param pageSize
	 * @param startRow
	 * @return
	 * @throws Exception
	 */
	List<Long> getJecnTempPositionBean(int pageSize, int start)
			throws Exception;

	/**
	 * 根据岗位ID获取
	 * 
	 * @param posIds
	 * @return
	 * @throws Exception
	 */
	List<PosEprosRelBean> getPosEpsListInf(List<Long> posIds) throws Exception;

	/**
	 * 根据岗位ID获取岗位名称
	 * 
	 * @param posList
	 * @return
	 * @throws Exception
	 */
	List<Object[]> getPosNameList(List<Long> posList) throws Exception;

	/**
	 * 根据匹配岗位的编码、ID查找对应的岗位名称
	 * 
	 * @param this.getSession()
	 * @return List<Object[]> objecPosNametList
	 */
	List<Object[]> selectActEpsNames(Long curProjectId) throws Exception;

	/***************************************************************************
	 * 根据流程岗位ID 查询流程岗位与实际岗位关联表数据
	 * 
	 * @param stationId
	 * @return
	 * @throws Exception
	 */
	List<String> findPosEprosRelList(Long stationId) throws Exception;

	/**
	 * 查询 数据库中 要更新删除的数据
	 * 
	 * @param stationId
	 * @param strNumList
	 * @return
	 * @throws Exception
	 */
	List<PosEprosRelBean> findRelByJspNum(Long stationId,
			List<String> strNumList) throws Exception;

	/***************************************************************************
	 * 删除 编辑页面中没有的数据库中的数据
	 * 
	 * @param deleteNumList
	 * @throws Exception
	 */
	void deleteRelNum(List<PosEprosRelBean> deleteNumList,
			List<Long> deletePeopleIds, Long flowPosId) throws Exception;

	/**
	 * 根据搜索 条件获取匹配结果
	 * 
	 * @param psoName
	 *            流程中岗位名称
	 * @return
	 * @throws Exception
	 */
	int getSearchPosCount(String psoName) throws Exception;

	/**
	 * 分页查询获取岗位匹配数据
	 * 
	 * @param pageSize
	 * @param startRow
	 * @param sql
	 * @param args
	 * @return
	 * @throws Exception
	 */
	List<Long> getPosEpsListInfBySearch(int pageSize, int startRow, String sql,
			Object[] args) throws Exception;

	/**
	 * 根据岗位编号获取岗位名称 获取数据库驱动名称
	 * 
	 * @param listNames
	 * @return
	 * @throws Exception
	 */
	List<Object[]> getPosRelList(List<Long> posIdList) throws Exception;

	/**
	 * 根据岗位编号获取岗位名称 获取数据库驱动名称
	 * 
	 * @param listNames
	 * @return
	 * @throws Exception
	 */
	List<Object[]> getPosNamesByNum(List<String> listPosNums) throws Exception;

	/**
	 * 根据基准岗位编号获取基准岗位名称
	 * 
	 * @param listPosBaseNums
	 * @return
	 * @throws Exception
	 */
	List<Object[]> getPosNamesByBaseNum(List<String> listPosBaseNums)
			throws Exception;

	/**
	 * 获取未匹配的岗位结果
	 * 
	 * @param pageSize
	 * @param startRow
	 * @return
	 * @throws Exception
	 */
	List<Object[]> getActualPosBeanList(int pageSize, int start, String posName)
			throws Exception;

	/**
	 * 获取未匹配的岗位结果
	 * 
	 * @return
	 * @throws Exception
	 */
	List<Object[]> getNoMatchObject(int type, int pageSize, int start,
			String posName) throws Exception;

	/**
	 * 
	 * 获取未匹配的岗位或基准岗位或流程岗位
	 * 
	 * @param type
	 *            1:HR实际岗位；5：流程岗位；6基准岗位
	 * @return
	 * @throws Exception
	 */
	int getNoMatchObjectCount(int type, String posName) throws Exception;

	/***************************************************************************
	 * 删除部门，岗位，人员实际表中原有的数据
	 * 
	 * @throws Exception
	 */
	// /***
	void deleteActualMatchTableData() throws Exception;

	/***************************************************************************
	 * 将部门，岗位，人员临时表中的数据添加到部门，岗位，人员实际表中
	 * 
	 * @throws Exception
	 */
	// /***
	void insertActualMatchTableData() throws Exception;

}
