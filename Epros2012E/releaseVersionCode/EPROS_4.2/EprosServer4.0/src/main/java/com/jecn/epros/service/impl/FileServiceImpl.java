package com.jecn.epros.service.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.*;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.file.*;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.ProcessAttribute;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.report.InventoryBaseInfo;
import com.jecn.epros.mapper.FileMapper;
import com.jecn.epros.mapper.InventoryMapper;
import com.jecn.epros.mapper.OrgMapper;
import com.jecn.epros.mapper.UserMapper;
import com.jecn.epros.service.FileService;
import com.jecn.epros.service.ServiceExpandTreeData;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.TreeNodeUtils.TreeType;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant.TYPEAUTH;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.Utils;
import com.jecn.epros.util.excel.ExcelUtils;
import com.jecn.epros.util.excel.InventoryStyleManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class FileServiceImpl implements FileService {

    @Autowired
    private FileMapper fileMapper;
    @Autowired
    private InventoryMapper inventoryMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrgMapper orgMapper;

    @Override
    public Attachment fetchAttachmentById(Long id) {
        return fileMapper.fetchAttachmentById(id);
    }

    @Override
    public List<TreeNode> findChildFiles(Map<String, Object> map) {
        map.put("typeAuth", TYPEAUTH.FILE);
        List<TreeNode> treeBeans = fileMapper.findChildFiles(map);
        treeBeans = ServiceExpandTreeData.findExpandNodesByDataList(treeBeans, TreeType.file);
        return TreeNodeUtils.setNodeType(treeBeans, TreeType.file);
    }

    @Override
    public FileBaseInfoBean findBaseInfoBean(Map<String, Object> map) {
        FileBaseInfoBean baseInfo = fileMapper.findBaseInfo(map);
        IfytekCommon.getIflytekUserName(baseInfo);
        //判断是否显示专员关键字
        if (ConfigUtils.valueIsZeroThenTrue("isShowFileCommissioner")) {
            baseInfo.setCommissionerName("hide");
        }
        if (ConfigUtils.valueIsZeroThenTrue("isShowFileKeyWord")) {
            baseInfo.setKeyWord("hide");
        }
        if (ConfigUtils.valueIsZeroThenTrue("isShowFileResPeople")) {
            baseInfo.setResPeopleName("hide");
        } else {
            baseInfo.setResPeopleName(getResPeopleName(baseInfo.getResPeopleId(), baseInfo.getTypeResPeople()));
            ;
        }
        baseInfo.setPub(StringUtils.isBlank(map.get("isPub").toString()) ? true : false);
        return baseInfo;
    }

    /**
     * 获取文件责任人
     *
     * @param id
     * @param type
     * @return
     */
    private String getResPeopleName(Long id, Integer type) {
        LinkResource linkResource = null;
        String trueName = " ";//默认属性 隐藏 责任人
        if (id != null) {
            if (type != null && type.intValue() == 0) {
                trueName = IfytekCommon.getIflytekUserName(userMapper.findUserNameById(id), 0);
            } else {// 责任人为岗位
                trueName = orgMapper.findPostNameById(id);
            }
        }
        return trueName;
    }

    @Override
    public List<SearchFileResultBean> searchFile(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "t.pub_time desc");
        return fileMapper.searchFile(map);
    }

    @Override
    public int searchFileTotal(Map<String, Object> map) {
        return fileMapper.searchFileTotal(map);
    }

    @Override
    public List<FileUseDetailBean> findFileUseDetail(Map<String, Object> map) {
        return fileMapper.findFileUseDetail(map);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Inventory getInventory(Map<String, Object> paramMap) {

        Boolean isPaging = (Boolean) paramMap.get("isPaging");

        List<FileDetailWebBean> fileAndDirs = new ArrayList<>();
        if (isPaging) {
            // 查询分页的流程
            Paging paging = (Paging) paramMap.get("paging");
            // 通过view_sort分页查询流程
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "view_sort");
            List<String> paths = inventoryMapper.fetchPaths(paramMap);
            if (paths != null && paths.size() > 0) {
                // 将查询到的流程和他的所有父节点都集中处理为(1,2,3)类型
                String ids = Utils.convertPathCollectToSqlIn(paths);
                paramMap.put("ids", ids);
                // 从数据库获得清单项的详细信息并排序
                fileAndDirs = fileMapper.fetchInventory(paramMap);
            }
        } else {
            fileAndDirs.addAll(fileMapper.fetchInventoryParent(paramMap));
            // 获得清单数据,经过view_sort排序的数据
            fileAndDirs.addAll(fileMapper.fetchInventory(paramMap));
        }
        //根据部门ID  查询部门的父类部门
        for (FileDetailWebBean fileBean : fileAndDirs) {
            if (fileBean.getResponsibleDeptId() != null) {
                fileBean.setDutyOrgList(orgMapper.findOrgPaterStrByOrgId(Long.valueOf(fileBean.getResponsibleDeptId())));
            }
        }
        // 查询流程的最高的级别
        InventoryBaseInfo baseInfo = inventoryMapper.getInventoryBaseInfo(paramMap);
        // 初始化清单
        Inventory inventory = new Inventory(baseInfo);

        Map<Long, FileDirCommonBean> dirs = new HashMap<>();
        List<FileDetailWebBean> files = new ArrayList<>();
        for (FileDetailWebBean bean : fileAndDirs) {
            if (bean.getIsDir() == 0) {
                dirs.put(bean.getFileId(), new FileDirCommonBean(bean.getFileId(), bean.getFileName(), bean.getDocNumber(), bean.getLevel()));
            } else {
                files.add(bean);
            }
        }
        IfytekCommon.ifytekCommonGetFileUserName(files);
        // 获取清单数据实体
        InventoryTable inventoryTable = getInventoryTable(isPaging, dirs, files, inventory.getStartLevel(), inventory.getMaxLevel(), paramMap);
        inventory.setData(inventoryTable);
        inventory.setStyleMap(InventoryStyleManager.getFileInventoryStyles(inventoryTable.getTitileContents()));

        return inventory;
    }


    private InventoryTable getInventoryTable(boolean isPaging, Map<Long, FileDirCommonBean> maps,
                                             List<FileDetailWebBean> files, int minLv, int maxLv, Map<String, Object> paramMap) {
        InventoryTable data = new InventoryTable();

        if (ConfigUtils.inventoryShowDirEnd()) {
            // 添加标题行的数据
            data.addContentRow(ExcelUtils.getMengNiuInventoryTitleNoNum(minLv, maxLv, InventoryStyleManager.getMengNiuFileTitles()));
        } else {
            // 添加标题行的数据
            data.addContentRow(ExcelUtils.getInventoryTitleNoNum(minLv, maxLv, InventoryStyleManager.getFileTitles()));
        }
        for (FileDetailWebBean file : files) {
            List<InventoryCell> contentRow = new ArrayList<InventoryCell>();
            if (ConfigUtils.inventoryShowDirEnd()) {
                // 添加流程的相关数据
                contentRow.addAll(getContentCells(file));
                // 添加自动生成的级别数据
                contentRow.addAll(ExcelUtils.getLevelsNoNum(maps, minLv, maxLv, file.getPath()));
            } else {
                // 添加自动生成的级别数据
                contentRow.addAll(ExcelUtils.getLevelsNoNum(maps, minLv, maxLv, file.getPath()));
                // 添加流程的相关数据
                contentRow.addAll(getContentCells(file));
            }
            data.addContentRow(contentRow);
        }
        return data;
    }


    private List<InventoryCell> getContentCells(FileDetailWebBean file) {

        List<InventoryCell> content = new ArrayList<>();

        InventoryCell cell = new InventoryCell();
        int pubState = file.getPubState();
        if (pubState == 0) {
            cell.setStatus(InventoryTable.Status.notPub);
        } else if (pubState == 1) {
            cell.setStatus(InventoryTable.Status.pub);
        } else if (pubState == 2) {
            cell.setStatus(InventoryTable.Status.approve);
        }
        cell.setText(file.getFileName());
        if (pubState == 1) {
            LinkResource link = new LinkResource(file.getFileId(), file.getFileName(), ResourceType.FILE, TreeNodeUtils.NodeType.inventory);

            if (cell.getLinks() == null) {
                cell.setLinks(new ArrayList<LinkResource>());
            }
            List<LinkResource> links = cell.getLinks();
            links.add(link);
        }
        content.add(cell);

        cell = new InventoryCell();
        cell.setText(file.getDocNumber());
        content.add(cell);

        // 文件使用情况
        List<FileDetailRelatedWebBean> relatedList = file.getRelatedList();
        List<LinkResource> relatedMap = new ArrayList<LinkResource>();
        List<LinkResource> relatedProcess = new ArrayList<LinkResource>();
        List<LinkResource> relatedRule = new ArrayList<LinkResource>();
        List<LinkResource> relatedStandard = new ArrayList<LinkResource>();
        List<LinkResource> relatedOrg = new ArrayList<LinkResource>();
        List<LinkResource> relatedPos = new ArrayList<LinkResource>();
        List<LinkResource> relatedRisk = new ArrayList<LinkResource>();
        for (FileDetailRelatedWebBean bean : relatedList) {
            /** 0是流程架构、1是流程、2是制度、3标准、4是组织 、5 岗位 */
            ResourceType type = null;
            if (bean.getRelatedType() == 0) {
                type = ResourceType.PROCESS_MAP;
                relatedMap.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type));
            } else if (bean.getRelatedType() == 1) {
                type = ResourceType.PROCESS;
                relatedProcess.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type));
            } else if (bean.getRelatedType() == 2) {
                type = ResourceType.RULE;
                relatedRule.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type, TreeNodeUtils.NodeType.file));
            } else if (bean.getRelatedType() == 3) {
                type = ResourceType.STANDARD;
                relatedStandard.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type));
            } else if (bean.getRelatedType() == 4) {
                type = ResourceType.ORGANIZATION;
                relatedOrg.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type));
            } else if (bean.getRelatedType() == 5) {
                type = ResourceType.POSITION;
                relatedPos.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type));
            } else if (bean.getRelatedType() == 6) {
                type = ResourceType.RISK;
                relatedRisk.add(new LinkResource(bean.getRelatedId(), bean.getRelatedName(), type));
            }
        }

        List<LinkResource> relateds = new ArrayList<>();
        relateds.addAll(relatedMap);
        relateds.addAll(relatedProcess);
        relateds.addAll(relatedRule);
        relateds.addAll(relatedStandard);
        relateds.addAll(relatedOrg);
        relateds.addAll(relatedPos);
        relateds.addAll(relatedRisk);

        cell = new InventoryCell();
        cell.setLinks(relateds);
        content.add(cell);
        // 责任部门
        String orgName = Common.getOrgFullPathStr(file.getResponsibleDeptName(), file.getDutyOrgList());
        cell = new InventoryCell();
        cell.setText(orgName);
        content.add(cell);

        // 责任人
        if (!ConfigUtils.inventoryShowDirEnd()) {
            LinkResource link = new LinkResource(file.getResponsibleId(), file.getResponsibleName(), file.getResponsibleType() == 0 ? ResourceType.PERSON : ResourceType.POSITION, TreeNodeUtils.NodeType.inventory);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell = new InventoryCell();
            cell.setLinks(links);
            content.add(cell);
        }

        // 创建人
        cell = new InventoryCell();
        cell.setText(file.getCreator());
        content.add(cell);
        // 创建时间
        cell = new InventoryCell();
        cell.setText(file.getCreateTime() != null ? Common.getStringbyDateHM(file.getCreateTime()) : "");
        content.add(cell);
        // 更新时间
        cell = new InventoryCell();
        cell.setText(file.getUpdateTime() != null ? Common.getStringbyDateHM(file.getUpdateTime()) : "");
        content.add(cell);


        return content;
    }


}
