package com.jecn.epros.server.action.designer.process.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.bean.checkout.CheckoutResult;
import com.jecn.epros.bean.checkout.CheckoutRoleItem;
import com.jecn.epros.bean.checkout.CheckoutRoleResult;
import com.jecn.epros.server.action.designer.process.IFlowStructureAcion;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.download.ProcessDownloadBaseBean;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.JecnFileContent;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT2;
import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.bean.process.JecnFlowFigure;
import com.jecn.epros.server.bean.process.JecnFlowFigureNow;
import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.bean.process.JecnFlowRecordT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnKPIFirstTargetBean;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.process.JecnSaveProcessData;
import com.jecn.epros.server.bean.process.ProcessArchitectureDescriptionBean;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessData;
import com.jecn.epros.server.bean.process.ProcessFileNodeData;
import com.jecn.epros.server.bean.process.ProcessInfoBean;
import com.jecn.epros.server.bean.process.ProcessInterface;
import com.jecn.epros.server.bean.process.ProcessMapData;
import com.jecn.epros.server.bean.process.ProcessMapRelatedFileData;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutData;
import com.jecn.epros.server.bean.process.temp.TmpKpiShowValues;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.PubSourceData;
import com.jecn.epros.server.bean.system.PubSourceSave;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.service.process.IFlowPKIService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.IProcessFigureAttributeService;
import com.jecn.epros.server.service.vatti.buss.DominoOperation;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/**
 * @author Administrator
 * 
 */
public class FlowStructureAcionImpl implements IFlowStructureAcion {

	private IFlowStructureService flowStructureService;
	private IProcessFigureAttributeService processFigureAttributeService;
	private IFlowPKIService flowPKIServiceImpl;

	public void setProcessFigureAttributeService(IProcessFigureAttributeService processFigureAttributeService) {
		this.processFigureAttributeService = processFigureAttributeService;
	}

	public IFlowStructureService getFlowStructureService() {
		return flowStructureService;
	}

	public void setFlowStructureService(IFlowStructureService flowStructureService) {
		this.flowStructureService = flowStructureService;
	}

	@Override
	public JecnFlowStructureT addFlow(JecnSaveProcessData processData) throws Exception {
		return flowStructureService.addFlow(processData);
	}

	@Override
	public JecnFlowStructureT addFlowMap(JecnFlowStructureT jecnFlowStructureT, JecnMainFlowT jecnMainFlowT,
			String posIds, String orgIds, String posGroupIds, Long updatePersionId) throws Exception {
		return flowStructureService.addFlowMap(jecnFlowStructureT, jecnMainFlowT, posIds, orgIds, posGroupIds,
				updatePersionId);
	}

	@Override
	public Long addKPI(JecnFlowKpiNameT jecnFlowKpiNameT, Long updatePeopleId) throws Exception {
		return flowStructureService.addKPI(jecnFlowKpiNameT, updatePeopleId);
	}

	@Override
	public void addKPIVaule(JecnFlowKpi jecnFlowKpi, Long updatePeopleId, Long flowId) throws Exception {
		flowStructureService.addKPIVaule(jecnFlowKpi, updatePeopleId, flowId);
	}

	@Override
	public void addKPIVaules(Long kpiId, List<JecnFlowKpi> jecnFlowKpiList, Long updatePeopleId, Long flowId)
			throws Exception {
		flowStructureService.addKPIVaules(kpiId, jecnFlowKpiList, updatePeopleId, flowId);
	}

	@Override
	public void deleteFlows(List<Long> listIds, Long updatePersonId, Long projectId) throws Exception {
		flowStructureService.deleteFlows(listIds, updatePersonId, projectId);
	}

	@Override
	public List<JecnTreeBean> getAllFlows(Long projectId) throws Exception {
		return flowStructureService.getAllFlows(projectId);
	}

	@Override
	public List<JecnTreeBean> getChildFlows(Long flowMapId, Long projectId, boolean isContainsDelNode) throws Exception {
		return flowStructureService.getChildFlows(flowMapId, projectId, isContainsDelNode);
	}

	@Override
	public ProcessAttributeBean getFlowAttribute(Long flowId) throws Exception {
		return flowStructureService.getFlowAttribute(flowId);
	}

	@Override
	public JecnMainFlowT getFlowMapInfo(Long id) throws Exception {
		return flowStructureService.getFlowMapInfo(id);
	}

	@Override
	public List<JecnFlowKpi> getJecnFlowKpiListByKPIId(Long flowKpiId) throws Exception {
		return flowStructureService.getJecnFlowKpiListByKPIId(flowKpiId);
	}

	@Override
	public List<JecnFlowKpiNameT> getJecnFlowKpiNameTList(Long flowId) throws Exception {

		return flowStructureService.getJecnFlowKpiNameTList(flowId);
	}

	@Override
	public List<JecnFlowRecordT> getJecnFlowRecordTListByFlowId(Long flowId) throws Exception {

		return flowStructureService.getJecnFlowRecordTListByFlowId(flowId);
	}

	@Override
	public List<RuleT> getRuleTByFlowId(Long flowId) throws Exception {

		return flowStructureService.getRuleTByFlowId(flowId);
	}

	@Override
	public List<StandardBean> getStandardBeanByFlowId(Long flowId) throws Exception {

		return flowStructureService.getStandardBeanByFlowId(flowId);
	}

	@Override
	public void moveEdit(Long id, Long updatePersonId) throws Exception {
		flowStructureService.moveEdit(id, updatePersonId);
	}

	@Override
	public void moveFlows(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		flowStructureService.moveFlows(listIds, pId, updatePersonId, moveNodeType);
	}

	@Override
	public void reName(Long id, String newName, Long updatePersonId, int userType) throws Exception {
		try {
			flowStructureService.updateName(id, newName, updatePersonId, userType);
		} catch (Exception e) {
			throw e;
		}

	}

	@Override
	public List<JecnTreeBean> searchFlowByName(String name, Long projectId, int type, Long peopleId) throws Exception {
		if (peopleId == null) {
			return flowStructureService.searchFlowByName(name, projectId, type);
		}
		return flowStructureService.searchRoleAuthByName(name, projectId, type, peopleId);
	}

	/**
	 * 更新流程属性
	 * 
	 * @param processInfoBean
	 * @throws Exception
	 */
	public void updateFlowAttribute(ProcessInfoBean processInfoBean) throws Exception {
		flowStructureService.updateFlowAttribute(processInfoBean);
	}

	@Override
	public void updateFlowBaseInformation(Long flowId, JecnFlowStructureT jecnFlowStructureT,
			JecnFlowBasicInfoT jecnFlowBasicInfoT, String orgIds, String posIds, String supportToolIds,
			Long updatePeopleId, String posGroupIds) throws Exception {
		flowStructureService.updateFlowBaseInformation(flowId, jecnFlowStructureT, jecnFlowBasicInfoT, orgIds, posIds,
				supportToolIds, updatePeopleId, posGroupIds);
	}

	@Override
	public void updateFlowMap(JecnMainFlowT jecnMainFlowT, Long updatePersonId, String flowNum) throws Exception {
		flowStructureService.updateFlowMap(jecnMainFlowT, updatePersonId, flowNum);
	}

	@Override
	public void updateFlowOperation(Long flowId, JecnFlowBasicInfoT jecnFlowBasicInfoT,
			JecnFlowDriverT jecnFlowDriverT, List<JecnFlowRecordT> listJecnFlowRecordT, Long updatePeopleId)
			throws Exception {
		flowStructureService.updateFlowOperation(flowId, jecnFlowBasicInfoT, jecnFlowDriverT, listJecnFlowRecordT,
				updatePeopleId);
	}

	@Override
	public void updateFlowRelateRuleT(Long flowId, String ruleIds, String standardIds, Long updatePeopleId)
			throws Exception {
		flowStructureService.updateFlowRelateRuleT(flowId, ruleIds, standardIds, updatePeopleId);
	}

	@Override
	public void updateKPI(JecnFlowKpiNameT jecnFlowKpiNameT, Long updatePeopleId) throws Exception {
		flowStructureService.updateKPI(jecnFlowKpiNameT, updatePeopleId);
	}

	@Override
	public void updateKPIVaule(JecnFlowKpi jecnFlowKpi, Long updatePeopleId, Long flowId) throws Exception {
		flowStructureService.updateKPIVaule(jecnFlowKpi, updatePeopleId, flowId);
	}

	@Override
	public void updateSortFlows(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception {
		flowStructureService.updateSortFlows(list, pId, projectId, updatePersonId);
	}

	@Override
	public void deleteKPIValues(List<Long> listIds, Long updatePeopleId, Long flowId) throws Exception {
		flowStructureService.deleteKPIValues(listIds, updatePeopleId, flowId);
	}

	@Override
	public void deleteKPIs(List<Long> listIds, Long updatePeopleId, Long flowId) throws Exception {
		flowStructureService.deleteKPIs(listIds, updatePeopleId, flowId);
	}

	@Override
	public JecnFlowBasicInfoT getJecnFlowBasicInfoT(Long flowId) throws Exception {
		return flowStructureService.getJecnFlowBasicInfoT(flowId);
	}

	@Override
	public JecnFlowStructureT getJecnFlowStructureT(Long flowId) throws Exception {
		return flowStructureService.getJecnFlowStructureT(flowId);
	}

	@Override
	public void saveProcessMap(ProcessMapData mapData, byte[] flowImgeBytes, Long updatePersionId) throws Exception {
		flowStructureService.saveProcessMap(mapData, flowImgeBytes, updatePersionId);

	}

	@Override
	public void saveProcess(ProcessData processData, byte[] flowImgeBytes, Long updatePersionId) throws Exception {
		flowStructureService.saveProcess(processData, flowImgeBytes, updatePersionId);

	}

	@Override
	public ProcessOpenMapData getProcessMapData(long id, long projectId) throws Exception {
		return flowStructureService.getProcessMapData(id, projectId);
	}

	@Override
	public ProcessOpenData getProcessData(long id, long projectId) throws Exception {
		ProcessOpenData processOpenData = flowStructureService.getProcessData(id, projectId);
		return processOpenData;
	}

	@Override
	public JecnFlowDriverT getJecnFlowDriverT(Long flowId) throws Exception {
		return flowStructureService.getJecnFlowDriverT(flowId);
	}

	@Override
	public List<JecnFlowFigureNow> getAllFigureAttribute() throws Exception {
		return processFigureAttributeService.getAll();
	}

	@Override
	public List<JecnFlowFigure> getAllFigureAttributeDefault() throws Exception {
		return processFigureAttributeService.getAllDefault();
	}

	@Override
	public void updateFigureAttribute(List<JecnFlowFigureNow> jecnFlowFigureNowList) throws Exception {
		processFigureAttributeService.updateAll(jecnFlowFigureNowList);

	}

	@Override
	public List<JecnTreeBean> getPnodes(Long flowId, Long projectId) throws Exception {
		return flowStructureService.getPnodes(flowId, projectId);
	}

	@Override
	public boolean validateAddName(String name, long pid, long isFlow, Long projectId) throws Exception {
		return flowStructureService.validateAddName(name, pid, isFlow, projectId);

	}

	public boolean validateUpdateName(String name, long id, long pid, long isFlow, Long projectId) throws Exception {
		return flowStructureService.validateUpdateName(name, id, pid, isFlow, projectId);

	}

	@Override
	public List<JecnTreeBean> getParentsContainSelf(Long projectId, Long id) throws Exception {
		return flowStructureService.getParentsContainSelf(projectId, id);
	}

	@Override
	public boolean updateProcessCanOpen(Long userId, Long processId) throws Exception {
		return flowStructureService.updateProcessCanOpen(userId, processId);
	}

	@Override
	public List<Object[]> getFlowOeprationFilesByFlowIdDesign(Long flowId) throws Exception {
		return flowStructureService.getFlowOeprationFilesByFlowIdDesign(flowId);
	}

	@Override
	public List<Object[]> getFlowRelateByFlowIdDesign(Long flowId) throws Exception {
		return flowStructureService.getFlowRelateByFlowIdDesign(flowId);
	}

	@Override
	public JecnCreateDoc getFlowFile(Long flowId, TreeNodeType treeNodeType, boolean isPub) throws Exception {
		return flowStructureService.getFlowFile(flowId, treeNodeType, isPub);
	}

	@Override
	public void cancelRelease(Long id, TreeNodeType type, Long peopleId) throws Exception {
		flowStructureService.cancelRelease(id, type, peopleId);
		// 华帝 取消发布 删除Domino平台数据
		if (JecnUtil.isVattiLogin()) {
			DominoOperation.deleteDataFromDomino(id, "Flow");
		}
	}

	@Override
	public void directRelease(Long id, TreeNodeType type, Long peopleId) throws Exception {
		flowStructureService.directRelease(id, type, peopleId);
		// 华帝登录 写入数据至Domino平台
		if (JecnUtil.isVattiLogin()) {
			DominoOperation.importDataToDomino(id, "Flow");
		}
	}

	@Override
	public List<JecnTreeBean> getChildProcessMap(Long flowMapId, Long projectId) throws Exception {
		return flowStructureService.getChildProcessMap(flowMapId, projectId);
	}

	/**
	 * 更新节点
	 * 
	 * @param flowStructureT
	 * @throws Exception
	 */
	@Override
	public void updateJecnFlowStructureT(JecnFlowStructureT flowStructureT) throws Exception {
		flowStructureService.update(flowStructureT);
	}

	/**
	 * 根据流程ID查询对应风险
	 * 
	 * @author fuzhh 2013-11-27
	 * @param flowId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnRisk> getRiskByFlowId(Long flowId, boolean isPub) throws Exception {
		return flowStructureService.getRiskByFlowId(flowId, isPub);
	}

	/**
	 * 根据风险ID集合查询对应风险
	 * 
	 * @author fuzhh 2013-11-27
	 * @param flowId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnRisk> getRiskByRiskIds(Set<Long> riskIds) throws Exception {
		return flowStructureService.getRiskByRiskIds(riskIds);
	}

	@Override
	public JecnFlowBasicInfoT2 getJecnFlowBasicInfoT2(Long flowId) throws Exception {
		return flowStructureService.getJecnFlowBasicInfoT2(flowId);
	}

	public IFlowPKIService getFlowPKIServiceImpl() {
		return flowPKIServiceImpl;
	}

	public void setFlowPKIServiceImpl(IFlowPKIService flowPKIServiceImpl) {
		this.flowPKIServiceImpl = flowPKIServiceImpl;
	}

	@Override
	public String addFirstTarget(List<JecnKPIFirstTargetBean> list) throws Exception {
		return flowPKIServiceImpl.addFirstTarget(list);
	}

	@Override
	public List<JecnKPIFirstTargetBean> getAllFirstTarget() throws Exception {
		return flowPKIServiceImpl.getAllFirstTarget();
	}

	@Override
	public List<JecnFlowKpiNameT> getFlowKpiNameTList(Long flowId) throws Exception {
		return flowStructureService.getFlowKpiNameTList(flowId);
	}

	/**
	 * 获取单个流程的操作说明
	 * 
	 * @author fuzhh 2013-9-5
	 * @param flowId
	 * @param isPub
	 * @param isDownFile
	 * @return
	 */
	@Override
	public byte[] getProcessFile(Long flowId, boolean isPub, Long peopleId, Long projectId, boolean isAdmin)
			throws Exception {
		return flowStructureService.getProcessFile(flowId, isPub, peopleId, projectId, isAdmin);
	}

	/**
	 * 获取单个流程附件集合
	 */
	@Override
	public Map<EXCESS, List<FileOpenBean>> getProcessExcessFile(Long flowId, boolean isPub, Long peopleId,
			Long projectId, boolean isAdmin) throws Exception {
		return flowStructureService.getProcessExcessFile(flowId, isPub, peopleId, projectId, isAdmin);
	}

	/**
	 * 推送流程图到第三方系统中
	 * 
	 * @param flowId
	 *            :流程编号 sySncType:公司类型 path 选择的路径
	 * @return 0:同步失败 1： 同步成功 2：没有开始活动，同步失败
	 * */
	public int sysNcFlowProcess(Long flowId, int sySncType, String path) throws Exception {
		return flowStructureService.sysNcFlowProcess(flowId, sySncType, path);
	}

	@Override
	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId, Set<Long> processIds, boolean isAdmin)
			throws Exception {
		// TODO Auto-generated method stub
		return flowStructureService.getDelAuthorityByName(name, projectId, processIds, isAdmin);
	}

	@Override
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long id, Long projectId) throws Exception {
		// TODO Auto-generated method stub
		return flowStructureService.getRecycleJecnTreeBeanList(id, projectId);
	}

	@Override
	public void recoverCur(List<Long> recycleNodesIdList, Long projectId) throws Exception {
		flowStructureService.updateRecoverCur(recycleNodesIdList, projectId);
	}

	@Override
	public void recoverCurAndChild(List<Long> recycleNodesIdList, Long projectId) throws Exception {

		flowStructureService.updateRecoverCurAndChild(recycleNodesIdList, projectId);

	}

	@Override
	public void trueDelete(List<Long> listIds, Long projectId, long userId) throws Exception {

		flowStructureService.deleteIdsFlow(listIds, projectId, userId);

	}

	@Override
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception {
		// TODO Auto-generated method stub
		return flowStructureService.getPnodesRecy(id, projectId);
	}

	@Override
	public TmpKpiShowValues getKpiShowValues(Long flowId) throws Exception {
		return flowStructureService.getKpiShowValues(flowId);
	}

	/**
	 * 根据配置获取KPI前几列数据
	 * 
	 * @param flowKpiName
	 * @param configItemBeans
	 * @return
	 */
	@Override
	public List<String> getConfigKpiRowValues(JecnFlowKpiName flowKpiName, List<JecnConfigItemBean> configItemBeans) {
		return flowStructureService.getConfigKpiRowValues(flowKpiName, configItemBeans);
	}

	@Override
	public List<String> getDesignAuthFlows(Long peopleId, Long projectId) throws Exception {
		// TODO Auto-generated method stub
		return flowStructureService.getDesignAuthFlows(peopleId, projectId);
	}

	/**
	 * 安码BPM接口，插入流程版本发布记录
	 * 
	 * @param flowId
	 * @param version
	 * @return 0:插入成功，-1，插入失败；1：已存在当前版本，不允许重复推送
	 * @throws Exception
	 */
	@Override
	public int processPushUltimus(Long flowId, String flowName, String version) throws Exception {
		return flowStructureService.processPushUltimus(flowId, flowName, version);
	}

	@Override
	public String getFlowOccupierUserNameById(Long flowId, Long curPeopleId) throws Exception {
		return flowStructureService.getFlowOccupierUserNameById(flowId, curPeopleId);
	}

	@Override
	public void batchReleaseNotRecord(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) throws Exception {
		flowStructureService.batchReleaseNotRecord(ids, treeNodeType, peopleId);
	}

	@Override
	public void batchCancelRelease(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) throws Exception {
		flowStructureService.batchCancelRelease(ids, treeNodeType, peopleId);
	}

	@Override
	public void updateFlowOperation(Long flowId, JecnFlowBasicInfoT flowBasicInfoT, Map<String, Object> resultMap,
			Long peopleId) throws Exception {
		flowStructureService.updateFlowOperation(flowId, flowBasicInfoT, resultMap, peopleId);
	}

	@Override
	public ProcessArchitectureDescriptionBean getProcessArchitectureDescriptionBean(Long flowId) throws Exception {
		return flowStructureService.getProcessArchitectureDescriptionBean(flowId);
	}

	@Override
	public void updateFlowMap(ProcessArchitectureDescriptionBean processArchitectureDescriptionBean) throws Exception {
		flowStructureService.updateFlowMap(processArchitectureDescriptionBean);

	}

	@Override
	public List<ProcessInterface> findFlowInterfaces(Long id, boolean isPub) throws Exception {
		return flowStructureService.findFlowInterfaces(id, isPub);
	}

	@Override
	public Long getRelatedFileId(Long flowId) throws Exception {
		return flowStructureService.getRelatedFileId(flowId);
	}

	@Override
	public Long createRelatedFileDirByPath(Long id, Long projectId, Long createPeopleId) throws Exception {
		return flowStructureService.createRelatedFileDirByPath(id, projectId, createPeopleId);
	}

	@Override
	public FileOpenBean openFileContent(Long id) throws Exception {
		return flowStructureService.openFileContent(id);
	}

	@Override
	public List<JecnTreeBean> getStandardizedFiles(Long id) throws Exception {
		return flowStructureService.getStandardizedFiles(id);
	}

	@Override
	public List<JecnTreeBean> getRelatedRiskList(Long flowId) throws Exception {
		return flowStructureService.getRelatedRiskList(flowId);
	}

	@Override
	public JecnFlowStructureT createProcessFile(ProcessFileNodeData processFileData) throws Exception {
		return flowStructureService.createProcessFile(processFileData);
	}

	@Override
	public JecnFlowStructureT updateProcessFile(ProcessFileNodeData processFileData) throws Exception {
		return flowStructureService.updateProcessFile(processFileData);
	}

	@Override
	public void updateProcessMapRelatedFiles(ProcessMapRelatedFileData relatedFileData) throws Exception {
		flowStructureService.updateProcessMapRelatedFiles(relatedFileData);
	}

	@Override
	public List<JecnTreeBean> getProcessApplyOrgs(Long id) throws Exception {
		return flowStructureService.getProcessApplyOrgs(id);
	}

	@Override
	public List<JecnTreeBean> getDirverEmailPeopleByFlowId(Long flowId) throws Exception {
		return flowStructureService.getDirverEmailPeopleByFlowId(flowId);
	}

	@Override
	public Map<Long, String> getNodeParentById(List<Long> ids, boolean isPub, TreeNodeType nodeType) throws Exception {
		return flowStructureService.getNodeParentById(ids, isPub, nodeType);
	}

	@Override
	public ProcessDownloadBaseBean getProcessDownloadBean(Long flowId, boolean isPub, String mark) throws Exception {
		return flowStructureService.getProcessDownloadBean(flowId, isPub, mark);
	}

	@Override
	public boolean isNotAbolishOrNotDelete(Long id) throws Exception {
		return flowStructureService.isNotAbolishOrNotDelete(id);
	}

	@Override
	public void addFlowFileHeadInfo(FlowFileHeadData flowFileHeadData) {
		flowStructureService.addFlowFileHeadInfo(flowFileHeadData);
	}

	@Override
	public FlowFileHeadData findFlowFileHeadInfo(Long selectId) {
		// TODO Auto-generated method stub
		return flowStructureService.findFlowFileHeadInfo(selectId);
	}

	@Override
	public List<Object[]> getFlowElementById(List ids) {
		return flowStructureService.getFlowElementById(ids);
	}

	@Override
	public List<Object[]> getFlowElementByLinkId(List ids) {
		return flowStructureService.getFlowElementByLinkId(ids);
	}

	@Override
	public void reNameFlowFigureText(Long flowLinkId, String flowName) {
		// TODO Auto-generated method stub
		flowStructureService.reNameFlowFigureText(flowLinkId, flowName);
	}

	public List<FlowFileHeadData> findFlowFileHeadInfoVerification(Long id) {
		// TODO Auto-generated method stub
		return flowStructureService.findFlowFileHeadInfoVerification(id);
	}

	@Override
	public PubSourceData getPubSourceData(Long rId, int rType) throws Exception {
		return flowStructureService.getPubSourceData(rId, rType);
	}

	@Override
	public void saveOrUpdatePubCopyNote(PubSourceSave pubNoteData) throws Exception {
		flowStructureService.saveOrUpdatePubCopyNote(pubNoteData);
	}

	@Override
	public void sendPubCopyNoteEmails(PubSourceSave pubNoteData) throws Exception {
		flowStructureService.sendPubCopyNoteEmails(pubNoteData);
	}

	@Override
	public void replaceAlllElementAttrs(List<JecnFlowFigureNow> list) {
		flowStructureService.replaceAlllElementAttrs(list);
	}

	@Override
	public boolean belowFlowExistFile(List<Long> listIds) throws Exception {
		return flowStructureService.belowFlowExistFile(listIds);
	}

	@Override
	public List<Long> getRelatedFileIds(List<Long> listIds) throws Exception {
		return flowStructureService.getRelatedFileIds(listIds);
	}

	@Override
	public boolean belowRuleExistFile(List<Long> listIds) throws Exception {
		return flowStructureService.belowRuleExistFile(listIds);
	}

	@Override
	public String validateNamefullPath(String flowName, long id, long i, Long projectId) {
		return flowStructureService.validateNamefullPath(flowName, id, i, projectId);
	}

	@Override
	public void copyNodeDatasHandle(List<Long> copyIds, Long copyToId, int option, Long peopleId) throws Exception {
		flowStructureService.copyNodeDatasHandle(copyIds, copyToId, option, peopleId);
	}

	@Override
	public List<JecnFlowInoutData> getFlowInoutsByType(int type, Long flowId) throws Exception {
		return flowStructureService.getFlowInoutsByType(type, flowId, false);
	}

	@Override
	public List<CheckoutResult> checkout(Long id) {
		return flowStructureService.checkout(id);
	}

	@Override
	public List<CheckoutRoleResult> checkoutRole(Long id) {
		return flowStructureService.checkoutRole(id);
	}

	@Override
	public void updateActivityPosRelation(Long flowId, List<CheckoutRoleItem> result) {
		flowStructureService.updateActivityPosRelation(flowId, result);
	}

	@Override
	public void mnInit(int type) throws Exception {
		flowStructureService.mnInit(type);
	}

	@Override
	public void transToProcess(List<Long> nodes, Long peopleId) throws Exception {
		flowStructureService.transToProcess(nodes, peopleId);
	}

	@Override
	public List<JecnFileContent> getJecnFlowFileContents(Long id) throws Exception {
		return flowStructureService.getJecnFlowFileContents(id);
	}

	@Override
	public List<JecnTreeBean> getChildFlows(List<Long> pids) throws Exception {
		return flowStructureService.getChildFlows(pids);
	}

	@Override
	public List<JecnTreeBean> fetchEntryLimit(String name, int count) {
		return flowStructureService.fetchEntryLimit(name, count);
	}
}
