package com.jecn.epros.server.download.wordxml.shidaixincai;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import wordxml.constant.Constants;
import wordxml.constant.Fonts;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.bean.paragraph.GroupBean.Hanchor;
import wordxml.element.bean.paragraph.GroupBean.Vanchor;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.AntiFake;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 时代新材操作说明模版
 * 
 * @author hyl
 * 
 */
public class SDXCProcessModel extends ProcessFileModel {
	/*** 段落行距 1.5倍行距 *****/
	private final ParagraphLineRule vLine2 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);
	/*** 段落行距 单倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距最小值12磅 *****/
	private final ParagraphLineRule leastVline20 = new ParagraphLineRule(20F, LineRuleType.AT_LEAST);

	public SDXCProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(17F);
		getDocProperty().setNewRowHeight(0.6F);
		getDocProperty().setFlowChartScaleValue(6F);
		setDocStyle("、", textTitleStyle());
	}

	protected void a35(ProcessFileItem processFileItem, List<FileWebBean> relatedFiles) {
		createTitle(processFileItem);
		// 文件名称
		String name = JecnUtil.getValue("fileName");
		// 文件编码
		String fileCode = JecnUtil.getValue("fileCode");

		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { name, fileCode });
		for (FileWebBean file : relatedFiles) {
			rowData.add(new String[] { file.getFileName(), file.getFileNum() });
		}
		float[] width = new float[] { 7F, 7F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow());

	}

	/**
	 * 流程文控信息
	 * 
	 * @param name
	 * @param documentList
	 * @param contentSect
	 */
	protected void a24(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 版本号
		String version = JecnUtil.getValue("versionNumber");
		// 变更说明
		String descOfChange = JecnUtil.getValue("changeThat");
		// 发布日期
		String releaseDate = JecnUtil.getValue("releaseDate");
		// 流程拟稿人
		String artificialPerson = "拟稿人";
		rowData.add(0, new String[] { version, descOfChange, releaseDate, artificialPerson });
		float[] width = new float[] { 2, 10, 2.5F, 2.5F };
		Table tbl = createTableItem(processFileItem, width, getVersionData());
		ParagraphBean tblContentStyle = tblContentStyle();
		tblContentStyle.setAlign(Align.center);
		tblContentStyle.getFontBean().setB(true);
		tbl.setCellStyle(0, tblContentStyle);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	private List<String[]> getVersionData() {
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		List<String[]> data = new ArrayList<String[]>();
		data.add(new String[] { "版本号", "变更说明", "发布日期", "拟稿人" });
		if (historys != null && !historys.isEmpty()) {
			for (JecnTaskHistoryNew h : historys) {
				data.add(new String[] { h.getVersionId(), nullToEmpty(h.getModifyExplain()),
						JecnUtil.DateToStr(h.getPublishDate(), "yyyy-MM-dd"), nullToEmpty(h.getDraftPerson()) });
			}
		}
		return data;
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	protected void a05(ProcessFileItem processFileItem, String... content) {
		createTitle(processFileItem);
		List<String[]> data = new ArrayList<String[]>();
		data.add(new String[] { "供应商", "输入" });
		data.add(new String[] { "", nullToEmpty(processDownloadBean.getFlowInput()) });
		Table tbl = createTableItem(processFileItem, new float[] { 8.77F, 8.77F }, data);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	protected void a06(ProcessFileItem processFileItem, String... content) {
		createTitle(processFileItem);
		List<String[]> data = new ArrayList<String[]>();
		data.add(new String[] { "输出", "客户" });
		data.add(new String[] { nullToEmpty(processDownloadBean.getFlowOutput()), "" });
		Table tbl = createTableItem(processFileItem, new float[] { 8.77F, 8.77F }, data);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 相关风险
	 * 
	 * @param _count
	 * @param name
	 * @param rilsList
	 */
	protected void a25(ProcessFileItem processFileItem, List<JecnRisk> riskList) {
		if (riskList == null || riskList.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "风险事件描述 ", "机会描述 " });
		for (JecnRisk risk : riskList) {
			rowData.add(new String[] { risk.getRiskName(), "" });
		}
		float[] width = new float[] { 6F, 8F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}


	/**
	 * 流程记录
	 * 
	 * @param _count
	 * @param name
	 * @param processRecordList
	 */
	protected void a11(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 文件编号
		String fileNumber = JecnUtil.getValue("fileNumber");
		// 文件名称
		String fileName = JecnUtil.getValue("fileName");
		// 标题行
		rowData.add(0, new String[] { fileName, fileNumber });
		float[] width = new float[] { 7F, 7F };
		JecnUtil.swapColumn(rowData, 0, 1);
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		float[] width = null;
		if (JecnContants.showPosNameBox) {
			width = new float[] { 3.5F, 3.5F, 7.5F };
		} else if (!JecnContants.showPosNameBox) {
			width = new float[] { 5.0F, 9.5F };
		}
		List<String[]> rowData2 = new ArrayList<String[]>();
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = "对应职位/岗位";
		// 角色职责
		String roleResponsibility = "角色职责";
		if (JecnContants.showPosNameBox) {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility, positionName });
		} else {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility });
		}
		for (String[] str : rowData) {
			if (JecnContants.showPosNameBox) {
				rowData2.add(new String[] { str[0], str[2], str[1] });
			} else {
				rowData2.add(new String[] { str[0], str[2] });
			}
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), getDocProperty()
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 相关流程
	 * 
	 * @param _count
	 * @param name
	 * @param relatedProcessMap
	 */
	protected void a13(ProcessFileItem processFileItem, Map<String, List<RelatedProcessBean>> relatedProcessMap) {
		if (relatedProcessMap == null || relatedProcessMap.isEmpty() || relatedProcessMap.keySet().size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 类型
		String type = "关联类型";
		// 流程编号
		String processNumber = JecnUtil.getValue("processNumber");
		// 流程名称
		String processName = JecnUtil.getValue("processName");
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { processName, processNumber, type });
		for (String key : relatedProcessMap.keySet()) {
			for (RelatedProcessBean relatedProcess : relatedProcessMap.get(key)) {
				// 添加数据
				rowData.add(new String[] { relatedProcess.getFlowName(), relatedProcess.getFlowNumber(),
						getRelatedProcessType(key) });
			}
		}
		float[] width = new float[] { 6F, 6, 4 };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getSectBean();
		titleSect.setSectBean(sectBean);
		addTitleSectContent(titleSect);
		createCommfhdr(titleSect);
	}

	/**
	 * 添加封面节点数据
	 * 
	 * @param titleSect
	 */
	private void addTitleSectContent(Sect titleSect) {
		titleSect.createMutilEmptyParagraph(13);
		createProcessName(titleSect);
		createProcessNum(titleSect);
		createProcessVersion(titleSect);
		titleSect.createMutilEmptyParagraph(5);
		createDutyTable(titleSect);
	}

	private void createDutyTable(Sect titleSect) {
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "责任人", nullToEmpty(processDownloadBean.getFlowAttr().getDutyUserName()) });
		rowData.add(new String[] { "责任部门", nullToEmpty(processDownloadBean.getFlowAttr().getDutyOrgName()) });
		rowData.add(new String[] { "实施日期", nullToEmpty(processDownloadBean.getPubDate("yyyy-MM-dd")) });
		Table table = createTableItem(titleSect, new float[] { 5.61F, 11.89F }, rowData);

		ParagraphBean paragraphBean = new ParagraphBean(Align.center, Fonts.SONG, Constants.wuhao, true);
		table.setCellStyle(0, paragraphBean);
		paragraphBean = new ParagraphBean(Align.center, Fonts.SONG, Constants.wuhao, false);
		table.setCellStyle(1, paragraphBean);
	}

	private void createProcessVersion(Sect titleSect) {
		ParagraphBean p = getPB();
		Paragraph pp = new Paragraph("文件版本：" + nullToEmpty(processDownloadBean.getVersion()), p);
		titleSect.addParagraph(pp);
	}

	protected AntiFake getAntiFake() {
		AntiFake fake = null;
//		IBaseDao baseDao = processDownloadBean.getBaseDao();
//		Long flowId = processDownloadBean.getFlowStructure() != null ? processDownloadBean.getFlowStructure()
//				.getFlowId() : processDownloadBean.getFlowStructureT().getFlowId();
//		JecnFlowStructure flow = (JecnFlowStructure) baseDao.getSession().get(JecnFlowStructure.class, flowId);
//		if (flow != null) {
//			fake = AntiFake.createTextAntiFake("受控", new AntiFakeBean("宋体", Constants.wuhao));
//		}
		return fake;
	}

	private void createProcessNum(Sect titleSect) {
		ParagraphBean p = getPB();
		Paragraph pp = new Paragraph("文件编号：" + nullToEmpty(processDownloadBean.getFlowInputNum()), p);
		titleSect.addParagraph(pp);
	}

	private ParagraphBean getPB() {
		ParagraphBean p = new ParagraphBean(Align.left, Fonts.SONG, Constants.sihao, false);
		p.setInd(0F, 0F, 0, 3);
		p.setSpace(0F, 0F, 0, new ParagraphLineRule(1.5F, LineRuleType.AUTO));
		return p;
	}

	private void createProcessName(Sect titleSect) {
		String flowName = nullToEmpty(processDownloadBean.getFlowName());
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoer, true);
		ParagraphLineRule vLine1 = new ParagraphLineRule(34F, LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph paragraph = new Paragraph(flowName, pBean);

		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 17, GroupH.exact, 2.7).setHorizontal(2.0, Hanchor.page, 0)
				.setVertical(8.04, Vanchor.page, 0).lock(true);
		group.addParagraph(paragraph);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		createCommfhdr(firstSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = new SectBean();
		// 设置页边距
		sectBean.setPage(2F, 1.76F, 1.76F, 1.5F, 0.5F, 0.5F);
		sectBean.setSize(29.7F, 21F);
		flowChartSect.setSectBean(sectBean);
		createCommfdr(flowChartSect);
		Header header = flowChartSect.createHeader(HeaderOrFooterType.odd);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 * @param marginRight
	 */
	private void createCommhdr(Sect sect, float left, float center, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.left, "宋体", Constants.sihao, true);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		TableBean tableBean = new TableBean();
		tableBean.setTableLeftInd(0.2F);
		Table table = header.createTable(tableBean, new float[] { left, center, right });
		TableRow first = table.createTableRow(new String[] { "", "文件号：" + processDownloadBean.getFlowInputNum(),
				"版本：" + nullToEmpty(processDownloadBean.getVersion()) }, pBean);
		TableRow second = table.createTableRow(new String[] { "", "文件名：" + processDownloadBean.getFlowName(), "页码：" },
				pBean);
		// 页码
		Paragraph page = second.getCell(2).getParagraph(0);
		page.appendCurPageRun();
		page.appendTextRun("/");
		page.appendTotalPagesRun();
		first.getCell(0).setRowspan(2);

		// 图片
		Image logoImage = super.getImageByClass(4.46F, 1.63F, "1.png");
		PositionBean positionBean = new PositionBean(PositionType.relative, -15F, 0F, 0F, 0F);
		logoImage.setPositionBean(positionBean);
		first.getCell(0).addParagraph(new Paragraph(logoImage));
	}

	private void createCommfdr(Sect sect) {
		// 页脚
		Footer footer = sect.createFooter(HeaderOrFooterType.odd);
		Image imageByClassPath = super.getImageByClass(6.68F, 0.77F, "2.png");
		footer.createParagraph(imageByClassPath, new ParagraphBean(Align.right));
	}

	protected void afterHandle() {
		List<Header> headers = getDocProperty().getFlowChartSect().getHeaders();
		System.out.println(headers);
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 5.75F, 9.09F, 4.16F);
		createCommfdr(sect);
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Fonts.SONG, Constants.xiaosi, false);
		textContentStyle.setInd(1.5F, 0F, 0F, 0F);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(12F, LineRuleType.AT_LEAST));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Fonts.SONG, Constants.xiaosi, true);
		textTitleStyle.setInd(0.75F, 0F, 0F, 0F);
		textTitleStyle.setSpace(0.5F, 0.2F, leastVline20);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		tblStyle.setTableAlign(Align.center);
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

}
