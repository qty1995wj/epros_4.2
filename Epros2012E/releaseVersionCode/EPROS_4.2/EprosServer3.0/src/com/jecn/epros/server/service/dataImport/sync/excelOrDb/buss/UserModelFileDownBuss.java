package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;

/**
 * 
 * 人员模板下载
 * 
 * @author Administrator
 * 
 */
public class UserModelFileDownBuss extends AbstractFileDownBuss {

	/**
	 * 
	 * 导出文件路径
	 * 
	 * @return String 文件路径
	 */
	public String getOutFilepath() {
		return SyncTool.getExcelModelPath();
	}

	/**
	 * 
	 * 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	protected String getOutFileName() {
		return SyncConstant.IMPOT_EXCEL_MODEL_FILE_NAME;
	}
}
