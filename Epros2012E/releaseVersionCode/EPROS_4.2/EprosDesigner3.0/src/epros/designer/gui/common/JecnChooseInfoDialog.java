package epros.designer.gui.common;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTree;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/***
 * 数据选择框，无搜索，我结果集选择框
 * 2013-11-26
 *
 */
public abstract class JecnChooseInfoDialog extends JecnDialog {
	/** 主面板 */
	private JPanel mainPanel =  new JPanel();

	/** 树面板 */
	private JScrollPane treePanel = new JScrollPane();
	/** 结果面板 */
	private JPanel resultPanel = new JPanel();
	/** 已选结果滚动面板 */
	private JScrollPane resultScrollPane =  new JScrollPane();

	/** 按钮面板，上面放四个按钮 */
	private JPanel buttonsPanel = new JPanel();
	/** 确定 */
	private JButton okBtn =  new JButton(JecnProperties.getValue("okBtn"));
	/** 取消 */
	private JButton cancelBtn = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 结果table */
	protected JecnTable resultTable = new ChooseResultTable();

	/** Tree 对象 */
	protected JecnTree jTree = getJecnTree();
	/**表格行高*/
	protected int oldRowHeight;
	/*** 验证提示 */
	protected JLabel verfyLab = new JLabel();
	public JecnChooseInfoDialog(){
		initCompotents();
		initLayout();
	}
	/**
	 * @description:初始化组件
	 */
	public void initCompotents() {
		// 节点移动
		this.setTitle(getChooseTitle());
		this.setSize(710, 510);
		this.setResizable(true);
		this.setModal(true);
		// 局中，设置大小后使用
		this.setLocationRelativeTo(null);
		Dimension dimension = new Dimension(200, 430);
		treePanel.setPreferredSize(dimension);
		treePanel.setMinimumSize(dimension);
		treePanel.setBorder(BorderFactory.createTitledBorder(getTreeTitle()));
		// 结果面板
		dimension = new Dimension(475, 430);
		resultPanel.setPreferredSize(dimension);
		resultPanel.setMinimumSize(dimension);
		resultPanel.setBorder(BorderFactory.createTitledBorder(getResultTitle()));

		// 已选结果滚动面板设置大小
		dimension = new Dimension(468, 400);
		resultScrollPane.setPreferredSize(dimension);
		resultScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setMinimumSize(dimension);
		// 按钮面板
		dimension = new Dimension(460, 30);
		buttonsPanel.setPreferredSize(dimension);
		buttonsPanel.setMinimumSize(dimension);
		// 设置各面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		verfyLab.setForeground(Color.red);
		
		okBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okBtnAction();
			}
		});

		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelBtnAction();
			}
		});
		jTree.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				jTreeMousePressed(evt);
			}
		});
		//原始行高
		oldRowHeight = resultTable.getRowHeight();
		resultTable.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				controlTarTablemousePressed(evt);
			}
		});
	}
	/**
	 * @description:dialog布局方法
	 */
	public void initLayout() {
		// 主面板布局-开始
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(2, 2, 2, 2);
		c = new GridBagConstraints(0, 0, 1, 2, 0.0, 1.0,
				GridBagConstraints.SOUTH, GridBagConstraints.VERTICAL, insets, 0, 0);

		mainPanel.add(treePanel, c);
		
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(resultPanel, c);
		resultPanel.setLayout(new GridBagLayout());
		resultScrollPane.setViewportView(resultTable);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0, 0);
		resultPanel.add(resultScrollPane, c);
		
		c = new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		mainPanel.add(buttonsPanel, c);
		// 主面板布局-结束
		// 把tree加入滚动面板
		treePanel.setViewportView(jTree);

		// 设置按钮面板为流布局
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonsPanel.add(verfyLab);
		buttonsPanel.add(okBtn);
		buttonsPanel.add(cancelBtn);
		// 把主面板增加到dialog
		this.getContentPane().add(mainPanel);
	}
	/***
	 * 窗体名称
	 * @return
	 */
	public abstract String getChooseTitle();
	/***
	 * tree树名称
	 * @return
	 */
	public abstract String getTreeTitle();
	/***
	 * 结果集面板名称
	 */
	public abstract String getResultTitle();
	/**
	 * @description:设置
	 * @return
	 */
	public abstract JecnTree getJecnTree();
	// button抽角方法--结束
	class ChooseResultTable extends JecnTable {

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent());
		}

		@Override
		public boolean isSelectMutil() {
			return false;
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0,1,2,4 };
		}
	}
	/**
	 * @description:获得table的标题
	 * @return
	 */
	public abstract Vector<String> getTableTitle();
	/**
	 * @description:获得Table的内容
	 * @return
	 */
	public abstract Vector<Vector<String>> getContent();
	/***
	 * 取消
	 */
	private void cancelBtnAction(){
		this.dispose();
	}
	/***
	 * 确定
	 */
	private void okBtnAction(){
		okButPerformed();
	}
	public abstract void okButPerformed();
	
	public abstract void jTreeMousePressed(MouseEvent evt);
	
	public abstract void controlTargetTablemousePressed(MouseEvent evt);
	private void controlTarTablemousePressed(MouseEvent evt){
		controlTargetTablemousePressed(evt);
	}
	
	
	public JecnTable getResultTable() {
		return resultTable;
	}
	public void setResultTable(JecnTable resultTable) {
		this.resultTable = resultTable;
	}
	public JecnTree getjTree() {
		return jTree;
	}
	public void setjTree(JecnTree jTree) {
		this.jTree = jTree;
	}
	public int getOldRowHeight() {
		return oldRowHeight;
	}
	public void setOldRowHeight(int oldRowHeight) {
		this.oldRowHeight = oldRowHeight;
	}
	
}
