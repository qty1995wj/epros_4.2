package epros.draw.gui.workflow.popup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.gui.designer.JecnDesignerToolData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.dialog.AddTotalMapDescDialog;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 右键点击流程元素弹出菜单处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElemPopupMenu {

	/** 右键点击流程元素弹出菜单 */
	private JPopupMenu flowElemPopupMenu = null;

	/** 活动明细 */
	private JecnMenuItem roundRectMenuItem = null;
	/** 添加角色 */
	private JecnMenuItem roleMenuItem = null;
	/** 自动编号 */
	private JecnMenuItem antoNumMenuItem = null;

	/** 剪切 */
	private JecnMenuItem cutMenuItem = null;
	/** 复制 */
	private JecnMenuItem copyMenuItem = null;
	/** 粘贴 */
	private JecnMenuItem pasteMenuItem = null;
	/** 删除 */
	private JecnMenuItem deleteMenuItem = null;
	/** 截图 */
	private JecnMenuItem screenshotMenuItem = null;

	/** 置于顶层 */
	private JecnMenuItem frontLayerMenuItem = null;

	/** 置于底层 */
	private JecnMenuItem backLayerMenuItem = null;
	/** 上移一层 */
	private JecnMenuItem upLayerMenuItem = null;
	/** 下移一层 */
	private JecnMenuItem downLayerMenuItem = null;

	/** 图形维护 */
	private JecnMenuItem flowElemAttrMenuItem = null;
	/** 元素自动对齐 */
	// private JecnMenuItem flowElemAlignMenuItem = null;
	/** 新建 模型片段 */
	private JecnMenuItem templateMenuItem = null;

	/** 描述 */
	private JecnMenuItem totalMapInstructItem = null;

	/** 流程元素对象 */
	private JecnBaseFlowElementPanel elementPanel = null;
	/** 画图面板类型 */
	private MapType mapType = null;

	public JecnFlowElemPopupMenu(MapType mapType) {
		this.mapType = mapType;
		initComponent();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponent() {

		// 右键点击流程元素弹出菜单
		flowElemPopupMenu = new JPopupMenu();

		// 活动明细
		roundRectMenuItem = new JecnMenuItem(this, ToolBarElemType.activeDetails);
		roundRectMenuItem.setEnabled(false);
		// 添加角色
		roleMenuItem = new JecnMenuItem(this, ToolBarElemType.roleDetails);
		roleMenuItem.setEnabled(false);
		// 自动编号
		antoNumMenuItem = new JecnMenuItem(this, ToolBarElemType.formatAntoNum);
		antoNumMenuItem.setEnabled(false);

		// 剪切
		cutMenuItem = new JecnMenuItem(this, ToolBarElemType.editCut);
		// 复制
		copyMenuItem = new JecnMenuItem(this, ToolBarElemType.editCopy);
		// 粘贴
		pasteMenuItem = new JecnMenuItem(this, ToolBarElemType.editPaste);
		// 删除
		deleteMenuItem = new JecnMenuItem(this, ToolBarElemType.editDelete);
		// 截图
		screenshotMenuItem = new JecnMenuItem(this, ToolBarElemType.editIconshot);

		// 置于顶层
		frontLayerMenuItem = new JecnMenuItem(this, ToolBarElemType.editFrontLayer);
		// 置于底层
		backLayerMenuItem = new JecnMenuItem(this, ToolBarElemType.editBackLayer);
		// 上移一层
		upLayerMenuItem = new JecnMenuItem(this, ToolBarElemType.editUpLayer);
		// 下移一层
		downLayerMenuItem = new JecnMenuItem(this, ToolBarElemType.editDownLayer);

		// 元素属性
		flowElemAttrMenuItem = new JecnMenuItem(this, ToolBarElemType.flowElemAttr);
		// 模板片段
		templateMenuItem = new JecnMenuItem(this, ToolBarElemType.newTemplate);

		// 描述
		totalMapInstructItem = new JecnMenuItem(this, ToolBarElemType.mapFigureDesc);
		totalMapInstructItem.setOpaque(false);
		totalMapInstructItem.setEnabled(false);
		/**
		 * 描述
		 */
		totalMapInstructItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AddTotalMapDescDialog totalMapDescDialog = new AddTotalMapDescDialog((JecnBaseFigurePanel) elementPanel);
				totalMapDescDialog.setVisible(true);
			}
		});

		// 元素自动对齐
		// flowElemAlignMenuItem = new JecnMenuItem(this,
		// ToolBarElemType.flowElemAlign);
		// 弹出菜单背景颜色
		flowElemPopupMenu.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 剪切 Ctrl+X
		cutMenuItem.setText(cutMenuItem.getText() + " Ctrl+X");
		// 复制 Ctrl+C
		copyMenuItem.setText(copyMenuItem.getText() + " Ctrl+C");
		// 粘贴 Ctrl+V
		pasteMenuItem.setText(pasteMenuItem.getText() + " Ctrl+V");
		// 删除 Delete
		deleteMenuItem.setText(deleteMenuItem.getText() + " Delete");
		// 截图 Ctrl+G
		screenshotMenuItem.setText(screenshotMenuItem.getText() + " Ctrl+G");

	}

	/**
	 * 
	 * 添加菜单项
	 * 
	 */
	private void initLayout() {
		if (mapType == null) { // 画图面板类型
			return;
		}
		switch (mapType) {// 添加菜单项
		case totalMap: // 流程地图
		case totalMapRelation:// 集成关系图
			initTotalMapMenuItems();
			// 插入线
			flowElemPopupMenu.addSeparator();
			// 描述
			flowElemPopupMenu.add(totalMapInstructItem);
			break;
		case partMap: // 流程图
			initPartMapMenuItems();
			break;
		case orgMap: // 组织图
			initOrgMapMenuItems();
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 
	 * 流程图菜单项
	 * 
	 */
	private void initPartMapMenuItems() {
		// 活动明细
		flowElemPopupMenu.add(roundRectMenuItem);
		// 自动编号
		flowElemPopupMenu.add(antoNumMenuItem);
		if (EprosType.eprosDraw == JecnSystemStaticData.getEprosType()) {// 画图面板
			// 插入线
			flowElemPopupMenu.addSeparator();
			// 添加角色
			flowElemPopupMenu.add(roleMenuItem);
		}
		// flowElemPopupMenu.add(new JecnReplaceActivePopupMenu());
		// 插入线
		flowElemPopupMenu.addSeparator();

		// 共用菜单项
		initTotalMapMenuItems();
	}

	/**
	 * 
	 * 流程地图菜单项
	 * 
	 */
	public void initTotalMapMenuItems() {

		// 剪切
		flowElemPopupMenu.add(cutMenuItem);
		// 复制
		flowElemPopupMenu.add(copyMenuItem);
		// 粘贴
		flowElemPopupMenu.add(pasteMenuItem);
		// 删除
		flowElemPopupMenu.add(deleteMenuItem);
		// 截图
		flowElemPopupMenu.add(screenshotMenuItem);
		flowElemPopupMenu.addSeparator();

		// 置于顶层
		flowElemPopupMenu.add(frontLayerMenuItem);
		// 置于底层
		flowElemPopupMenu.add(backLayerMenuItem);
		// 上移一层
		flowElemPopupMenu.add(upLayerMenuItem);
		// 下移一层
		flowElemPopupMenu.add(downLayerMenuItem);
		flowElemPopupMenu.addSeparator();

		// 模板片段
		flowElemPopupMenu.add(templateMenuItem);
		flowElemPopupMenu.addSeparator();
		// 元素属性
		flowElemPopupMenu.add(flowElemAttrMenuItem);
	}

	/**
	 * 
	 * 组织图菜单项
	 * 
	 */
	private void initOrgMapMenuItems() {
		// 设计器
		initDesMenuItems();
		// 剪切
		flowElemPopupMenu.add(cutMenuItem);
		// 复制
		flowElemPopupMenu.add(copyMenuItem);
		// 粘贴
		flowElemPopupMenu.add(pasteMenuItem);
		// 删除
		flowElemPopupMenu.add(deleteMenuItem);
		// 截图
		flowElemPopupMenu.add(screenshotMenuItem);
		flowElemPopupMenu.addSeparator();

		// 置于顶层
		flowElemPopupMenu.add(frontLayerMenuItem);
		// 置于底层
		flowElemPopupMenu.add(backLayerMenuItem);
		// 上移一层
		flowElemPopupMenu.add(upLayerMenuItem);
		// 下移一层
		flowElemPopupMenu.add(downLayerMenuItem);
		flowElemPopupMenu.addSeparator();
		// 元素属性
		flowElemPopupMenu.add(flowElemAttrMenuItem);
		// TODO
		// 元素自动对齐
		// flowElemPopupMenu.add(flowElemAlignMenuItem);
	}

	/**
	 * 
	 * 添加设计器的菜单项
	 * 
	 */
	private void initDesMenuItems() {
		if (mapType == null) { // 画图面板类型
			return;
		}
		switch (mapType) { // 添加菜单项
		case totalMap: // 流程地图
			addMenuItemList(JecnDesignerToolData.getTotalMapFlowElemMenuItemlist());
			break;
		case partMap: // 流程图
			addMenuItemList(JecnDesignerToolData.getPartMapFlowElemMenuItemlist());
			break;
		case orgMap: // 组织图
			addMenuItemList(JecnDesignerToolData.getOrgMapFlowElemMenuItemlist());
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 添加菜单项
	 * 
	 */
	private void addMenuItemList(List<JMenuItem> menuItemlist) {
		if (menuItemlist == null || menuItemlist.size() == 0) {
			return;
		}
		for (JMenuItem menuItem : menuItemlist) {
			flowElemPopupMenu.add(menuItem);
		}
		// 插入线
		flowElemPopupMenu.addSeparator();
	}

	public JPopupMenu getFlowElemPopupMenu() {
		return flowElemPopupMenu;
	}

	public JecnBaseFlowElementPanel getElementPanel() {
		return elementPanel;
	}

	public void setElementPanel(JecnBaseFlowElementPanel elementPanel) {

		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 当前面板选中行
		List<JecnBaseFlowElementPanel> currentSelectElement = workflow.getCurrentSelectElement();

		if (currentSelectElement.size() == 0) {
			return;
		} else {
			this.elementPanel = elementPanel;
		}
	}

	public JecnMenuItem getTemplateMenuItem() {
		return templateMenuItem;
	}

	public JecnMenuItem getRoundRectMenuItem() {
		return roundRectMenuItem;
	}

	public JecnMenuItem getAntoNumMenuItem() {
		return antoNumMenuItem;
	}

	public JecnMenuItem getCutMenuItem() {
		return cutMenuItem;
	}

	public JecnMenuItem getCopyMenuItem() {
		return copyMenuItem;
	}

	public JecnMenuItem getPasteMenuItem() {
		return pasteMenuItem;
	}

	public JecnMenuItem getScreenshotMenuItem() {
		return screenshotMenuItem;
	}

	public void setEnableRoleItem(boolean b) {
		this.roleMenuItem.setEnabled(b);
	}

	public JecnMenuItem getRoleMenuItem() {
		return roleMenuItem;
	}

	public JecnMenuItem getTotalMapInstructItem() {
		return totalMapInstructItem;
	}

	public JecnMenuItem getDeleteMenuItem() {
		return deleteMenuItem;
	}

	public JecnMenuItem getFrontLayerMenuItem() {
		return frontLayerMenuItem;
	}

	public JecnMenuItem getBackLayerMenuItem() {
		return backLayerMenuItem;
	}

	public JecnMenuItem getUpLayerMenuItem() {
		return upLayerMenuItem;
	}

	public JecnMenuItem getDownLayerMenuItem() {
		return downLayerMenuItem;
	}

	public JecnMenuItem getFlowElemAttrMenuItem() {
		return flowElemAttrMenuItem;
	}

	public MapType getMapType() {
		return mapType;
	}
}
