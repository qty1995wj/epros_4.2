package com.jecn.viewdoc.service.impl;

import javax.annotation.Resource;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.viewdoc.bean.ViewDocContentIdCache;
import com.jecn.viewdoc.dao.IViewDocDao;
import com.jecn.viewdoc.service.IViewDocService;

@Transactional(rollbackFor = Exception.class)
public class ViewDocServiceImpl implements IViewDocService {

	@Resource
	private IViewDocDao viewDocDao;

	@Override
	public void save(ViewDocContentIdCache cache) {
		ViewDocContentIdCache temp = this.getCache(cache.getRelatedId(), cache.getType());
		if(temp!=null){
			viewDocDao.update(cache);
		}else{
			viewDocDao.save(cache);
		}
		
	}

	@Override
	public ViewDocContentIdCache getCache(Long relatedId, int dataType) {
		return viewDocDao.getCache(relatedId, dataType);
	}

	@Override
	public void update(ViewDocContentIdCache cache) {
		viewDocDao.update(cache);
	}

}
