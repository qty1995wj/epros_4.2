package com.jecn.epros.server.download.wordxml.lianhedianzi;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 联合电子操作说明下载
 * 
 * @author admin
 * 
 */
public class LHDZProcessModel extends ProcessFileModel {
	/*** 段落行距单倍倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F,
			LineRuleType.AUTO);

	public LHDZProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		getDocProperty().setNewTblWidth(15.1F);
		getDocProperty().setNewRowHeight(0.82F);
		getDocProperty().setFlowChartScaleValue(11F);
		// 设置默认的英文字体
		setDefAscii("Arial");
		setDocStyle(".	", textTitleStyle());
		setDocStartNum(0);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(2.54F, 3.17F, 2.54F, 3.17F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	// 重写表格设置垂直居中
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem,
			float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (int i = 0; i < width.length; i++) {
			tab.setCellStyle(i, Valign.center);
		}
		return tab;
	}

	@Override
	protected void a01(ProcessFileItem processFileItem, String... content) {
		processFileItem.setItemTitle("内容");
		createTitle(processFileItem);
		// 活动的内容
		List<String[]> rowData = new ArrayList<String[]>();
		rowData
				.add(new String[] { "目的", processDownloadBean.getFlowPurpose() });
		rowData
				.add(new String[] { "裁减",
						processDownloadBean.getApplicability() });
		rowData.add(new String[] { "输入", processDownloadBean.getFlowInput() });
		rowData.add(new String[] { "入口条件",
				processDownloadBean.getNoutGlossary() });
		rowData.add(new String[] { "活动", "任务", "负责人" });
		/** 活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出 （长春轨道加入 6指标） */
		// 活动编号：活动名称
		// 活动说明
		for (Object[] obj : processDownloadBean.getAllActivityShowList()) {
			String activeNumber = ("".equals(JecnWordUtil.obj2String(obj[0]))) ? ""
					: JecnWordUtil.obj2String(obj[0]) + "：";
			rowData.add(new String[] { "",
					activeNumber + JecnWordUtil.obj2String(obj[2]),
					JecnWordUtil.obj2String(obj[1]) });
		}
		rowData.add(new String[] { "输出", processDownloadBean.getFlowOutput() });
		rowData.add(new String[] { "出口条件",
				processDownloadBean.getFlowCustomFour() });
		rowData.add(new String[] { "角色",
				processDownloadBean.getFlowSupplement() });
		rowData
				.add(new String[] { "说明",
						processDownloadBean.getFlowCustomOne() });
		rowData
				.add(new String[] { "工具",
						processDownloadBean.getFlowCustomTwo() });
		rowData.add(new String[] { "支持资源",
				processDownloadBean.getFlowCustomThree() });

		Table tbl = createTableItem(processFileItem, new float[] { 2.22F,
				9.84F, 3 }, rowData);
		// 遍历活动设置活动说明活动开始行数是第五行
		for (int i = 0; i < processDownloadBean.getAllActivityShowList().size(); i++) {
			Object[] obj = processDownloadBean.getAllActivityShowList().get(i);
			if (StringUtils.isNotBlank(obj[3] + "")) {
				tbl.getRow(i + 5).getCell(1).createParagraph(
						JecnWordUtil.getContents(obj[3]),
						getDocProperty().getTblContentStyle());
			}
		}
		// 设置活动跨行
		tbl.getRow(4).getCell(0).setRowspan(
				processDownloadBean.getAllActivityShowList().size() + 1);
		// 设置跨列
		for (int i = 0; i < tbl.getRowCount(); i++) {
			if (i < 4
					|| i > (4 + processDownloadBean.getAllActivityShowList()
							.size())) {
				tbl.getRow(i).getCell(1).setColspan(2);
			}
		}
		tbl.setRowStyle(4, getDocProperty().getTblTitleStyle());
		ParagraphBean tblTitileStyle = new ParagraphBean("宋体", Constants.wuhao,
				true);
		tbl.setCellStyle(0, tblTitileStyle, Valign.center);
		tbl.setCellStyle(2, new ParagraphBean(Align.center, "宋体",
				Constants.wuhao, false), Valign.center, 4);
		// 设置垂直居中
		for (int i = 0; i < 2; i++) {
			tbl.setCellStyle(i, Valign.center);
		}
	}

	@Override
	protected void a02(ProcessFileItem processFileItem, String... content) {
	}

	@Override
	protected void a05(ProcessFileItem processFileItem, String... content) {
	}

	@Override
	protected void a06(ProcessFileItem processFileItem, String... content) {
	}

	@Override
	protected void a07(ProcessFileItem processFileItem,
			List<KeyActivityBean> activeList) {
	}

	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	@Override
	protected void a10(ProcessFileItem processFileItem, String[] title,
			List<String[]> rowData) {
	}

	@Override
	protected void a03(ProcessFileItem processFileItem, String... content) {
	}

	@Override
	protected void a30(ProcessFileItem processFileItem, String... content) {
	}

	/**
	 * 参考文件对应流程记录
	 * 
	 * @param _count
	 * @param name
	 * @param processRecordList
	 */
	protected void a11(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		// 文件编号
		String fileNumber = JecnUtil.getValue("fileNumber");
		// 文件名称
		String fileName = JecnUtil.getValue("fileName");
		// 标题行
		rowData.add(0, new String[] { fileNumber, fileName });
		float[] width = new float[] { 2.28F, 12.8F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle());
		tbl.setCellStyle(0, Valign.center);
		tbl.setCellStyle(1, Valign.center);

	}

	@Override
	protected void a19(ProcessFileItem processFileItem, String... content) {
	}

	/**
	 * 记录 对应纪录保存
	 */
	@Override
	protected void a20(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 记录保存数据 0 记录名称 1保存责任人 2保存场所 3归档时间 4保存期限 5到期处理方式
		createTitle(processFileItem);
		List<String[]> newRowData = new ArrayList<String[]>();
		newRowData.add(new String[] { "记录和编号", "保存于", "最少保存期", "归档索引" });
		String[] newData = null;
		for (String[] oldData : rowData) {
			newData = new String[] { oldData[0], oldData[2], oldData[4], "" };
			newRowData.add(newData);
		}
		Table tbl = createTableItem(processFileItem, new float[] { 8.13F,
				2.22F, 2.54F, 2.14F }, newRowData);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle());
		// 设置垂直居中
		for (int i = 0; i < 4; i++) {
			tbl.setCellStyle(i, Valign.center);
		}

	}

	@Override
	protected void a27(ProcessFileItem processFileItem, String... content) {
	}

	@Override
	protected void a28(ProcessFileItem processFileItem, String... content) {
	}

	@Override
	protected void a29(ProcessFileItem processFileItem, String... content) {
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		// 文件编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		// 文件版本
		String flowVersion = processDownloadBean.getFlowVersion();
		// 批准日期
		String releaseDate = processDownloadBean.getReleaseDate();
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		List<String[]> rowData = new ArrayList<String[]>();
		/**** 设置表格样式 *****/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		rowData.add(new String[] { "", "工作手册", "" });
		rowData.add(new String[] { "", flowName, "" });

		/**** 宋体 十五号 加粗 *******/
		ParagraphBean centerCellBean = new ParagraphBean(Align.center, "宋体",
				Constants.shiwuhao, true);
		centerCellBean.setSpace(0F, 0F, vLine1);
		/***** 页眉 ********/
		Header hdr = sect.createHeader(type);
		Table table = JecnWordUtil.createTab(hdr, tabBean, new float[] { 2.75F,
				7.6F, 4.68F }, rowData, centerCellBean);
		// 设置第一个单元格设置公司logo图片数据
		Paragraph p = table.getRow(0).getCell(0).getParagraph(0);
		Image image = new Image(getDocProperty().getLogoImage());
		image.setSize(2.28F, 0.66F);
		p.appendGraphRun(image);
		// 宋体 10.5 居左
		ParagraphBean rightCellBean = new ParagraphBean(Align.left, "宋体",
				Constants.shidianwuhao, false);
		rightCellBean.setSpace(0F, 0F, vLine1);
		table.getRow(0).getCell(1).createParagraph("", centerCellBean);
		table.getRow(1).getCell(1).createParagraph("", centerCellBean);

		// 获取第一行第三个单元格
		TableCell tc = table.getRow(0).getCell(2);
		p = tc.getParagraph(0);
		p.appendTextRun("编号:" + flowInputNum);
		tc.createParagraph("");
		p = tc.createParagraph("文件版本：" + flowVersion);
		tc.createParagraph("");
		p = tc.createParagraph("批准日期：" + releaseDate);
		tc.createParagraph("");
		p = tc.createParagraph("页数：第");
		p.appendCurPageRun();
		p.appendTextRun("页 共");
		p.appendTotalPagesRun(0);
		p.appendTextRun("页 ");
		tc.createParagraph("");
		table.getRow(0).getCell(0).setRowspan(2);
		table.getRow(0).getCell(2).setRowspan(2);
		/**** 设置列居中 *****/
		table.setCellStyle(0, Valign.center);
		table.setCellStyle(1, Valign.center);
		table.setCellStyle(2, Valign.center);
		table.setCellStyle(2, rightCellBean);

		table.getRow(0).setHeight(1.3F);
		table.getRow(1).setHeight(2.06F);
		hdr.createParagraph("", getDocProperty().getTblContentStyle());
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		String companyName = processDownloadBean.getCompanyName();
		// 黑体 五号 居中
		ParagraphBean pBean = new ParagraphBean(Align.left, "宋体",
				Constants.jiuhao, false);
		Footer ftr = sect.createFooter(type);
		ftr.createParagraph(companyName + "												"
				+ processDownloadBean.getFlowInputNum(), pBean);
		pBean = new ParagraphBean(Align.left, "Arial", Constants.jiuhao, false);
		ftr.createParagraph("United Automotive Electronic System Co., Ltd.",
				pBean);

	}

	/**
	 * 流程图页脚
	 * 
	 * @param sect
	 * @param type
	 */
	private void createCommftr_flowChartSect(Sect sect, HeaderOrFooterType type) {
		String companyName = processDownloadBean.getCompanyName();
		// 黑体 五号 居中
		ParagraphBean pBean = new ParagraphBean(Align.left, "宋体",
				Constants.jiuhao, false);
		Footer ftr = sect.createFooter(type);
		ftr.createParagraph(companyName + "																								"
				+ processDownloadBean.getFlowInputNum(), pBean);
		pBean = new ParagraphBean(Align.left, "Arial", Constants.jiuhao, false);
		ftr.createParagraph("United Automotive Electronic System Co., Ltd.",
				pBean);

	}

	@Override
	protected void initTitleSect(Sect titleSect) {
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		firstSect.createParagraph("历史修改记录", getDocProperty()
				.getTextTitleStyle());
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "版本", "批准日期", "编写", "修改内容" });
		/** 流程文控信息 0版本号 1变更说明 2发布日期 3流程拟制人 4审批人 */
		for (Object[] data : processDownloadBean.getDocumentList()) {
			rowData.add(JecnWordUtil.obj2String(new Object[] { data[0],
					JecnUtil.valueToDateStr(data[2]), data[3], data[1] }));
		}
		Table tbl = JecnWordUtil.createTab(firstSect, getDocProperty()
				.getTblStyle(), new float[] { 3.76F, 3.73F, 3.76F, 3.76F },
				rowData, getDocProperty().getTblContentStyle());
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), Valign.center);
		for (TableRow tr : tbl.getRows()) {
			tr.setHeight(1.11F);
		}
		for (int i = 0; i < 4; i++) {
			tbl.setCellStyle(i, Valign.center);
		}
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		createCommhdr(flowChartSect, HeaderOrFooterType.odd);
		createCommftr_flowChartSect(flowChartSect, HeaderOrFooterType.odd);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.center, "宋体",
				Constants.wuhao, false);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setTableAlign(Align.center);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体",
				Constants.wuhao, true);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, "宋体",
				Constants.wuhao, false);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, "宋体",
				Constants.xiaosi, true);
		textTitleStyle.setInd(0, 0, 0.75F, 0);
		textTitleStyle.setSpace(1F, 1F, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
