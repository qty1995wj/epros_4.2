package com.jecn.epros.server.action.designer.tree;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public interface IJecnTreeAction {
	/**
	 * 获取树节点的子节点sort最大值
	 * 
	 * @param treeBean
	 * @return
	 * @throws Exception
	 */
	public int getTreeChildMaxSortId(JecnTreeBean treeBean) throws Exception;
}
