package com.jecn.epros.server.download.wordxml.huadi;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;

/**
 * 华帝操作说明下载
 * 
 * @author admin
 * 
 */
public class HDProcessModel extends ProcessFileModel {
	/*** 段落行距单倍倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F,
			LineRuleType.AUTO);
	/*** 段落行距最小值22.5磅 *****/
	private ParagraphLineRule vLineFix22 = new ParagraphLineRule(22.5F,
			LineRuleType.AT_LEAST);
	/**** 段落行距最小值16磅值 **********/
	private ParagraphLineRule vLineFix16 = new ParagraphLineRule(16F,
			LineRuleType.AT_LEAST);
	/***** 段落空格换行属性bean *******/
	private ParagraphBean newLineBean;

	public HDProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, false);
		setDocStyle("、", textTitleStyle());
		/***** 段落空格换行属性bean 宋体 小四 最小值16磅值 *******/
		newLineBean = new ParagraphBean(Align.left, new FontBean("宋体",
				Constants.xiaosi, false));
		newLineBean.setSpace(0f, 0f, vLineFix22);
		getDocProperty().setNewTblWidth(17.49F);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.5F, 2F, 1.5F, 1.7F, 1F, 1.2F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/*****
	 * 创建表格项
	 * 
	 * @param contentSect
	 * @param fs
	 * @param list
	 * @return
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem,
			float[] fs, List<String[]> list) {
		processFileItem.getBelongTo().createParagraph("", newLineBean);
		Table tab = super.createTableItem(processFileItem, fs, list);
		processFileItem.getBelongTo().createParagraph("", newLineBean);
		return tab;
	}

	/**
	 * 创建文本项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	@Override
	protected void createTextItem(ProcessFileItem processFileItem,
			String... content) {
		super.createTextItem(processFileItem, content);
		processFileItem.getBelongTo().createParagraph("", newLineBean);
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent() {
		// 编制部门
		String orgName = processDownloadBean.getOrgName();
		// 文件编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		// 文件版本
		String flowVersion = processDownloadBean.getFlowVersion();
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		// 公司名称
		String companyName = processDownloadBean.getCompanyName();
		// String companyName = "北京杰成合力科技有限公司";
		// 宋体 粗号 居右对齐 单倍行距
		ParagraphBean pBean = new ParagraphBean(Align.right, "宋体",
				Constants.chuhao, false);
		pBean.setSpace(0f, 0f, vLine1);
		getDocProperty().getTitleSect().createParagraph("Q/HDG", pBean);

		// 宋体 一号 居中对齐 单倍行距
		pBean = new ParagraphBean(Align.center, "宋体", Constants.yihao, true);
		pBean.setSpace(0f, 0f, vLine1);
		getDocProperty().getTitleSect().createParagraph(companyName, pBean);
		// 单线条
		LineGraph line = new LineGraph(LineStyle.single, 1.25F);
		line.setFrom(0.75F, 1.15F);
		line.setTo(523.2F, 1.15F);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		line.setPositionBean(positionBean);
		getDocProperty().getTitleSect().createParagraph(line);
		// 宋体 小一 加粗 单倍行距
		pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoyi, true);
		pBean.setSpace(0f, 0f, vLine1);
		for (int i = 0; i < 6; i++) {
			getDocProperty().getTitleSect().createParagraph("");
		}
		getDocProperty().getTitleSect().createParagraph(flowName, pBean);
		// 宋体 三号 单倍行距
		pBean = new ParagraphBean(Align.left, "宋体", Constants.sanhao, false);
		pBean.setSpace(0f, 0f, vLine1);
		for (int i = 0; i < 4; i++) {
			getDocProperty().getTitleSect().createParagraph("");
		}
		getDocProperty().getTitleSect().createParagraph("				编制部门：" + orgName,
				pBean);
		getDocProperty().getTitleSect().createParagraph("");
		getDocProperty().getTitleSect().createParagraph(
				"				文件编号：" + flowInputNum, pBean);
		getDocProperty().getTitleSect().createParagraph("");
		getDocProperty().getTitleSect().createParagraph(
				"				文件版号：" + flowVersion, pBean);
		getDocProperty().getTitleSect().createParagraph("");
		TableBean tabBean = new TableBean();
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		// 宋体 小四 居左 最小值 16磅值
		pBean = new ParagraphBean("宋体", Constants.xiaosi, false);
		pBean.setSpace(0f, 0f, vLineFix16);
		List<String[]> rowData = new ArrayList<String[]>();
		/** 评审人集合 0 评审人类型 1名称 2日期 */
		List<Object[]> peopleList = processDownloadBean.getPeopleList();
		String[] str = null;
		for (Object[] obj : peopleList) {
			str = new String[] { "" + obj[0], "" + obj[1], "日期：", "" + obj[2] };
			rowData.add(str);
		}
		Table tab = JecnWordUtil.createTab(getDocProperty().getTitleSect(),
				tabBean, new float[] { 4.37F, 4.37F, 4.37F, 4.37F }, rowData,
				pBean);
		/***** 设置表格行高0.56 ******/
		for (int i = 0; i < tab.getRowCount(); i++) {
			tab.getRow(i).setHeight(0.56F);
		}
	}

	/**
	 * 添加第一节前面的数据
	 */
	private void addFirstSectContent() {
		// 文件编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		// 文件版本
		String flowVersion = processDownloadBean.getFlowVersion();
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		// 公司名称
		String companyName = processDownloadBean.getCompanyName();
		// 宋体 粗号 居右对齐 单倍行距
		ParagraphBean pBean = new ParagraphBean(Align.right, "宋体",
				Constants.chuhao, false);
		pBean.setSpace(0f, 0f, vLine1);
		// 宋体 三号 居中对齐 单倍行距
		pBean = new ParagraphBean(Align.center, "宋体", Constants.sanhao, true);
		pBean.setSpace(0f, 0f, vLine1);
		getDocProperty().getFirstSect().createParagraph(companyName, pBean);
		// 宋体十一号 居右 单倍行距
		pBean = new ParagraphBean(Align.right, "宋体", Constants.shiyihao, true);
		pBean.setSpace(0f, 0f, vLine1);
		getDocProperty().getFirstSect().createParagraph(
				flowInputNum + " " + flowVersion, pBean);
		// 宋体 二号 居中 单倍行距
		pBean = new ParagraphBean(Align.center, "宋体", Constants.erhao, true);
		pBean.setSpace(0f, 0f, vLine1);
		getDocProperty().getFirstSect().createParagraph(flowName, pBean);

		// 单线条
		LineGraph line = new LineGraph(LineStyle.single, 1F);
		line.setFrom(1.15F, 1.15F);
		line.setTo(516.95F, 1.15F);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		line.setPositionBean(positionBean);
		getDocProperty().getFirstSect().createParagraph(line);

	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);

	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		String flowInputNum = processDownloadBean.getFlowInputNum();
		String flowVersion = processDownloadBean.getFlowVersion();
		// 宋体 11 加粗 居中
		ParagraphBean pBean = new ParagraphBean("宋体", 22, true);
		pBean.setAlign(Align.center);
		Header hdr = sect.createHeader(type);
		hdr.createParagraph(flowInputNum + " " + flowVersion, pBean);

	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// Arial 小五号 居中
		ParagraphBean pBean = new ParagraphBean("宋体", Constants.xiaosi, false);
		pBean.setAlign(Align.center);
		Footer ftr = sect.createFooter(type);
		ftr.createParagraph("公司机密,未经批准不得扩散", pBean);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		firstSect.setSectBean(getSectBean());
		addFirstSectContent();
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		// 添加封皮节点数据
		addTitleSectContent();
		createCommftr(titleSect, HeaderOrFooterType.first);
		createCommhdr(titleSect, HeaderOrFooterType.first);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体",
				Constants.xiaosi, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0, 0, 0, 0);
		// 表格左缩进0.01厘米
		tblStyle.setTableLeftInd(0.01F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体",
				Constants.xiaosi, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体",
				Constants.xiaosi, false);
		textContentStyle.setSpace(0f, 0f, vLineFix22);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("宋体",
				Constants.xiaosi, true);
		textTitleStyle.setSpace(0F, 0F, vLineFix22);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
