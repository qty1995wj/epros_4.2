package epros.draw.gui.workflow.aid;

import java.awt.Dimension;

import javax.swing.JButton;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 
 * 快捷添加图形的图形按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnShortcutLinkFigureButton extends JButton {

	/** 快捷面板 */
	private JecnShortcutLinkFigurePanel linkPanel = null;
	/** 按钮类型 */
	private MapElemType mapElemType = null;

	public JecnShortcutLinkFigureButton(MapElemType mapElemType,
			JecnShortcutLinkFigurePanel linkPanel) {
		if (mapElemType == null || linkPanel == null) {
			// TODO
		}
		this.mapElemType = mapElemType;
		this.linkPanel = linkPanel;

		// 初始化组件
		initComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 图片
		this.setIcon(JecnFileUtil.getToolBoxImageIcon(mapElemType.toString()));
		// 大小
		Dimension btnSize = new Dimension(linkPanel.getBtnWidth(), linkPanel
				.getBtnHeight());
		this.setPreferredSize(btnSize);
		this.setMaximumSize(btnSize);
		this.setMinimumSize(btnSize);

		// 不绘制内容区域
		this.setContentAreaFilled(false);
		this.setContentAreaFilled(false);
		this.setContentAreaFilled(false);

		// 背景
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 事件
		this.addMouseListener(linkPanel.getLinkFigureMainProcess());
		this.addActionListener(linkPanel.getLinkFigureMainProcess());
	}

	public MapElemType getMapElemType() {
		return mapElemType;
	}

}
