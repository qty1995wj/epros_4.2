package epros.draw.event;

import java.awt.event.ActionEvent;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.SwingWorker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.shape.DateFreeText;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.JecnFlowExport;
import epros.draw.gui.top.toolbar.io.JecnSaveFlowImageJDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 工具栏常规动作处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnOftenActionProcess extends JecnAbstractActionProcess {
	private static final Log log = LogFactory.getLog(JecnOftenActionProcess.class);

	protected void actionPerformed(ActionEvent e, ToolBarElemType toolBarElemType) {
		if (toolBarElemType == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		switch (toolBarElemType) {
		case oftenSave: // 保存
			// 判断主界面是否存在
			saveWokfFlow(workflow);
			break;
		case oftenSaveAS: // 另存为
			// 判断界面是否存在
			// 重置层级
			workflow.getGraphicsLevel().saveResetLayer();
			JecnFlowExport flowExport = new JecnFlowExport();
			String showName = workflow.getFlowMapData().getShowName();
			boolean isSave = flowExport.initialize(workflow, showName, true);
			// if (isSave) {
			// // 保存成功，更新面板数据中保存状态
			// saveWokfFlow(workflow);
			// }
			if (isSave) {
				// 重置面板时间输入框
				resetDateText();
			}
			break;
		case oftenSaveImage: // 导出为图片
			// 判断主界面是否存在
			// 角色移动
			if (JecnSystemStaticData.isShowRoleMove()) {
				// 面板
				JecnDrawMainPanel.getMainPanel().getToolbar().getFormatToolBarItemPanel().getRoleMoveCheckBox()
						.setSelected(false);
				JecnSystemStaticData.setShowRoleMove(false);
				// 移除角色移动
				workflow.removeJecnRoleMobile();
				workflow.repaint();
			}
			// 清空选中点
			JecnWorkflowUtil.hideAllResizeHandle();
			if (workflow.getTipPanel() != null) {
				workflow.getTipPanel().setVisible(false);
			}
			JecnSaveFlowImageJDialog saveFlowImageJDialog = new JecnSaveFlowImageJDialog();
			saveFlowImageJDialog.setVisible(true);
			if (workflow.getTipPanel() != null) {
				workflow.getTipPanel().setVisible(true);
			}
			break;
		case oftenZoomLabel: // 当前比例
			zommComboBoxActionPerformed(e);
			break;
		case oftenZoomBig: // 放大
			workflow.getWorkflowZoomProcess().zoomBtnActionProcess(0.25);
			break;
		case oftenZoomSmall: // 缩小
			workflow.getWorkflowZoomProcess().zoomBtnActionProcess(-0.25);
			break;
		case oftenOriginal: // 原始视图
			workflow.getWorkflowZoomProcess().zoomByNewWorkflowScale(1.0);
			break;
		case oftenUndo: // 撤销
			if (!workflow.getAidCompCollection().getEditScrollPane().isVisible()) {
				workflow.getUndoRedoManager().undo();
			}
			break;
		case oftenRedo: // 恢复
			if (!workflow.getAidCompCollection().getEditScrollPane().isVisible()) {
				workflow.getUndoRedoManager().redo();
			}
			break;
		}
	}

	/**
	 * 
	 * 保存当前画图面板
	 * 
	 */
	private static boolean saveToLocal(JecnDrawDesktopPane desktopPane) {
		boolean isSave = false;
		// 判断界面是否存在
		if (desktopPane == null) {
			return false;
		}
		// 重置层级
		desktopPane.getGraphicsLevel().saveLocalResetLayer();

		JecnFlowExport flowExport = new JecnFlowExport();
		String flowName = desktopPane.getFlowMapData().getName();
		// 判断界面上的路径是否存在
		if (desktopPane.getFlowFilePath() != null) {
			WorkTask task = new WorkTask();
			task.setDesktopPane(desktopPane);
			task.setFlowExport(flowExport);
			String error = JecnLoading.start(task);
			if (!DrawCommon.isNullOrEmtryTrim(error) && !"0".equals(error)) {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
						.getJecnResourceUtil().getValue("saveFailed"));
				return false;
			} else {
				try {
					isSave = task.get();
				} catch (Exception e) {
					log.error("", e);
				}
			}
		} else {
			isSave = flowExport.initialize(desktopPane, flowName, true);
		}
		if (isSave) {
			// 重置面板时间输入框
			resetDateText();
		}
		return isSave;
	}

	/**
	 * 画图面板保存处理
	 * 
	 */
	public static boolean saveWokfFlow(JecnDrawDesktopPane desktopPane) {
		// 获取面板保存后标识：true保存成功返回boolean true
		boolean isSave = false;
		if (desktopPane == null) {
			return false;
		}
		// 清除 拖动添加图形等操作产生的临时数据，流程元素库选中指针状态
		JecnWorkflowUtil.clearStatesAndSelected();

		if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
			isSave = JecnDesignerProcess.getDesignerProcess().processSaveMap();
		} else {
			// 画图工具保存
			isSave = saveToLocal(desktopPane);
		}
		if (isSave) {// 保存成功，清除撤销恢复集合
			desktopPane.getUndoRedoManager().cleanRedoAndUndo();
		}
		return isSave;
	}

	/**
	 * 重置面板上 时间输入框的时间
	 */
	private static void resetDateText() {
		// 循环面板上的所有图形
		for (JecnBaseFlowElementPanel baseFlowElementPanel : JecnDrawMainPanel.getMainPanel().getWorkflow()
				.getPanelList()) {
			// 判断图形是否属于时间输入框
			if (baseFlowElementPanel instanceof DateFreeText) {
				// 根据跟定日期转换字符串日期格式yyyy-MM-dd
				String strDate = JecnWorkflowUtil.getStringbyDate(new Date());
				// 给时间数据框赋值
				baseFlowElementPanel.getFlowElementData().setFigureText(strDate);
			}

		}
	}

	/**
	 * 
	 * 放大缩小下拉框动作事件
	 * 
	 * @param e
	 */
	private void zommComboBoxActionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JComboBox) {
			JComboBox comboBox = (JComboBox) e.getSource();

			if (comboBox.getSelectedItem() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {// 当前选中为NULL,返回
				return;
			}

			// 获取手动输入或选择项的值，转化成放大缩小倍数
			double newWorkflowScale = JecnWorkflowUtil.convertWorkflowSpace(comboBox.getSelectedItem().toString());
			// 执行放大缩小
			JecnDrawMainPanel.getMainPanel().getWorkflow().getWorkflowZoomProcess().zoomByNewWorkflowScale(
					newWorkflowScale);
			// 删除虚框
			JecnTabbedEventProcess.removeTempFigure();
		}
	}
}

/**
 * @author yxw 2012-8-10
 * @description：面板保存进度控制线程
 */
class WorkTask extends SwingWorker<Boolean, Void> {
	private JecnDrawDesktopPane desktopPane;
	private JecnFlowExport flowExport;

	@Override
	protected Boolean doInBackground() throws Exception {
		try {
			boolean isSave = true;
			int num = flowExport.saveFlow(desktopPane.getFlowMapData(), desktopPane.getFlowFilePath(), false);
			if (num != 0) {
				isSave = false;
			}
			JecnLoading.setError(String.valueOf(num));
			return isSave;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void done() {
		JecnLoading.stop();
	}

	public JecnDrawDesktopPane getDesktopPane() {
		return desktopPane;
	}

	public void setDesktopPane(JecnDrawDesktopPane desktopPane) {
		this.desktopPane = desktopPane;
	}

	public JecnFlowExport getFlowExport() {
		return flowExport;
	}

	public void setFlowExport(JecnFlowExport flowExport) {
		this.flowExport = flowExport;
	}

}
