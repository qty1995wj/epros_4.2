package com.jecn.epros.server.service.tree.data;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.write.WritableSheet;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

import com.jecn.epros.server.bean.integration.JecnControlTarget;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.tree.data.Node;
import com.jecn.epros.server.common.IBaseDao;

public class RiskNodeImportService extends AbsNodeImportService<JecnRisk> {

	private static final String POINT_SPLIT = "；|;";

	public RiskNodeImportService(Long peopleId, Long pid, File file, Long projectId, IBaseDao baseDao, boolean CH) {
		super(peopleId, pid, file, projectId, baseDao, CH);
	}

	protected JecnRisk getData(Sheet sheet, int line) {
		String name = getCellText(sheet, line, contentStartIndex + 1);
		JecnRisk data = getBaseData(name, 1);
		data.setRiskCode(getCellText(sheet, line, contentStartIndex));
		data.setStandardControl(getCellText(sheet, line, contentStartIndex + 3));
		String point = getCellText(sheet, line, contentStartIndex + 2);
		if (StringUtils.isNotBlank(point)) {
			String[] arr = point.split(POINT_SPLIT);
			List<JecnControlTarget> points = new ArrayList<JecnControlTarget>();
			data.setListControlTarget(points);
			for (String a : arr) {
				if (StringUtils.isBlank(a)) {
					continue;
				}
				JecnControlTarget target = new JecnControlTarget();
				points.add(target);
				target.setCreateTime(now);
				target.setUpdateTime(now);
				target.setCreatePersonId(peopleId);
				target.setUpdatePersonId(peopleId);
				target.setDescription(a);
			}
		}

		return data;
	}

	protected boolean sheetNameIsDir() {
		return true;
	}

	protected JecnRisk getBaseData(String name, int type) {
		JecnRisk bean = new JecnRisk();
		bean.setRiskName(name);
		bean.setIsDir(type);
		bean.setProjectId(projectId);
		bean.setCreatePersonId(peopleId);
		bean.setUpdatePersonId(peopleId);
		bean.setUpdateTime(now);
		bean.setCreateTime(now);
		bean.setSort(1);
		bean.setGrade(1);
		bean.setRiskType(1);
		return bean;
	}

	@Override
	protected void initRootNode(Node<JecnRisk> pNode) {
		JecnRisk bean = (JecnRisk) baseDao.getObjectHql("from JecnRisk where id=?", pid);
		if (StringUtils.isBlank(bean.gettPath()) || StringUtils.isBlank(bean.getViewSort())) {
			throw new IllegalArgumentException("当前选中的节点的t_path或者view_sort为空。t_path:" + bean.gettPath() + " view_sort:"
					+ bean.getViewSort());
		}
		pNode.setDbId(bean.getId());
		pNode.setLevel(bean.gettLevel());
		pNode.setPath(bean.gettPath());
		pNode.setViewSort(bean.getViewSort());
		pNode.setData(bean);
	}

	@Override
	protected void analysis(Sheet sheet, WritableSheet wsheet, int sheetIndex) {
		// 读取单元格确定列标题是否和模板一致
		// 风险编号 风险点描述 控制目标 标准化控制
		// 读取宽度
		contentStartIndex = getContentIndex(sheet, "风险编号");
		contentEndIndex = Math.max(sheet.getColumns(), 0);
		clearCol(wsheet, contentEndIndex + 1);
		if (contentStartIndex == -1) {
			writeMsg(wsheet, getColumnNotExistTip("风险编号"));
			errors[sheetIndex] = true;
			return;
		}
		if (columnNotExist(sheet, wsheet, sheetIndex, contentStartIndex + 1, "风险点描述")) {
			return;
		}
		if (columnNotExist(sheet, wsheet, sheetIndex, contentStartIndex + 2, "控制目标")) {
			return;
		}
		if (columnNotExist(sheet, wsheet, sheetIndex, contentStartIndex + 3, "标准化控制")) {
			return;
		}
		contentEndIndex = contentStartIndex + 3;
		clearCol(wsheet, contentEndIndex + 1);
		dirStartIndex = 0;
		dirEndIndex = contentStartIndex - 1;
	}

	@Override
	protected void save(Node<JecnRisk> node) {
		Session s = baseDao.getSession();
		JecnRisk data = node.getData();
		data.setParentId(node.getParent().getDbId());
		s.save(data);

		List<JecnControlTarget> targets = data.getListControlTarget();
		if (targets != null && targets.size() > 0) {
			int sortId = 1;
			for (JecnControlTarget target : targets) {
				target.setRiskId(data.getId());
				target.setRiskNum(data.getRiskCode());
				target.setSort(sortId++);
				s.save(target);
			}
		}

		node.setDbId(data.getId());

		data.settLevel(node.getLevel());
		data.settPath(node.getPath());
		data.setViewSort(node.getViewSort());
		data.setSort(node.getSort().intValue());
		s.update(data);
	}

	@Override
	protected int getReadStartLine() {
		return 1;
	}

	@Override
	protected long getRootChildsMaxSortId() {
		String sql = "select max(a.sort) from jecn_risk a where a.parent_id = ?";
		int c = baseDao.countAllByParamsNativeSql(sql, pid);
		return c;
	}

	@Override
	protected boolean validateData(Sheet sheet, WritableSheet wsheet, boolean isDir, JecnRisk bean, int line) {
		// 目录长度 61个汉字
		// 风险编号 61个汉字
		// 风险点描述 600个汉字
		// 标准化控制 600个汉字
		// 控制目标 600个汉字
		boolean result = true;
		if (isDir && validateLenAndTipIfNotPass(wsheet, "目录", bean.getRiskName(), 61, line)) {
			result = false;
		} else {
			if (validateLenAndTipIfNotPass(wsheet, "风险编号", bean.getRiskCode(), 61, line)) {
				result = false;
			}
			if (validateLenAndTipIfNotPass(wsheet, "风险点描述", bean.getRiskName(), 600, line)) {
				result = false;
			}
			if (validateLenAndTipIfNotPass(wsheet, "标准化控制", bean.getStandardControl(), 600, line)) {
				result = false;
			}
			List<JecnControlTarget> targets = bean.getListControlTarget();
			if (targets != null && targets.size() > 0) {
				for (JecnControlTarget target : targets) {
					if (validateLenAndTipIfNotPass(wsheet, "控制目标", target.getDescription(), 600, line)) {
						break;
					}
				}
			}
		}
		return result;
	}
}
