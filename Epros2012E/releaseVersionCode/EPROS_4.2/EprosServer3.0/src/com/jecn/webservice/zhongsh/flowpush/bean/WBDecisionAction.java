package com.jecn.webservice.zhongsh.flowpush.bean;

public class WBDecisionAction {
	private String guid;
	private String actionGUID;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getActionGUID() {
		return actionGUID;
	}

	public void setActionGUID(String actionGUID) {
		this.actionGUID = actionGUID;
	}

}
