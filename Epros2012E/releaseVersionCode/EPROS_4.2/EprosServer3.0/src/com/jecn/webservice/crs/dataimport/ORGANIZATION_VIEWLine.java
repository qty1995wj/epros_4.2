/**
 * ORGANIZATION_VIEWLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crs.dataimport;

public class ORGANIZATION_VIEWLine  implements java.io.Serializable {
    private java.math.BigDecimal c_oid_orgunit;

    private java.lang.String c_name;

    private java.lang.String c_code;

    private java.lang.String c_category;

    private java.lang.String c_economictype;

    private java.lang.String c_phonenumber;

    private java.lang.String c_fax;

    private java.lang.String c_address;

    private java.lang.String c_postalcode;

    private java.lang.String c_email;

    private java.lang.String c_web;

    private java.lang.String c_introduction;

    private java.lang.String c_deptduty;

    private java.lang.String c_remark;

    private java.lang.String c_status;

    private java.util.Date c_validdate;

    private java.math.BigDecimal c_level;

    private java.util.Date c_begindate;

    private java.util.Date c_enddate;

    private java.lang.String c_businesstype;

    private java.lang.String c_lay;

    private java.lang.String c_businessarea;

    private java.lang.String c_othername;

    private java.math.BigDecimal c_parentunitid;

    private java.math.BigDecimal c_order;

    private java.math.BigDecimal c_structure;

    public ORGANIZATION_VIEWLine() {
    }

    public ORGANIZATION_VIEWLine(
           java.math.BigDecimal c_oid_orgunit,
           java.lang.String c_name,
           java.lang.String c_code,
           java.lang.String c_category,
           java.lang.String c_economictype,
           java.lang.String c_phonenumber,
           java.lang.String c_fax,
           java.lang.String c_address,
           java.lang.String c_postalcode,
           java.lang.String c_email,
           java.lang.String c_web,
           java.lang.String c_introduction,
           java.lang.String c_deptduty,
           java.lang.String c_remark,
           java.lang.String c_status,
           java.util.Date c_validdate,
           java.math.BigDecimal c_level,
           java.util.Date c_begindate,
           java.util.Date c_enddate,
           java.lang.String c_businesstype,
           java.lang.String c_lay,
           java.lang.String c_businessarea,
           java.lang.String c_othername,
           java.math.BigDecimal c_parentunitid,
           java.math.BigDecimal c_order,
           java.math.BigDecimal c_structure) {
           this.c_oid_orgunit = c_oid_orgunit;
           this.c_name = c_name;
           this.c_code = c_code;
           this.c_category = c_category;
           this.c_economictype = c_economictype;
           this.c_phonenumber = c_phonenumber;
           this.c_fax = c_fax;
           this.c_address = c_address;
           this.c_postalcode = c_postalcode;
           this.c_email = c_email;
           this.c_web = c_web;
           this.c_introduction = c_introduction;
           this.c_deptduty = c_deptduty;
           this.c_remark = c_remark;
           this.c_status = c_status;
           this.c_validdate = c_validdate;
           this.c_level = c_level;
           this.c_begindate = c_begindate;
           this.c_enddate = c_enddate;
           this.c_businesstype = c_businesstype;
           this.c_lay = c_lay;
           this.c_businessarea = c_businessarea;
           this.c_othername = c_othername;
           this.c_parentunitid = c_parentunitid;
           this.c_order = c_order;
           this.c_structure = c_structure;
    }


    /**
     * Gets the c_oid_orgunit value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_oid_orgunit
     */
    public java.math.BigDecimal getC_oid_orgunit() {
        return c_oid_orgunit;
    }


    /**
     * Sets the c_oid_orgunit value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_oid_orgunit
     */
    public void setC_oid_orgunit(java.math.BigDecimal c_oid_orgunit) {
        this.c_oid_orgunit = c_oid_orgunit;
    }


    /**
     * Gets the c_name value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_name
     */
    public java.lang.String getC_name() {
        return c_name;
    }


    /**
     * Sets the c_name value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_name
     */
    public void setC_name(java.lang.String c_name) {
        this.c_name = c_name;
    }


    /**
     * Gets the c_code value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_code
     */
    public java.lang.String getC_code() {
        return c_code;
    }


    /**
     * Sets the c_code value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_code
     */
    public void setC_code(java.lang.String c_code) {
        this.c_code = c_code;
    }


    /**
     * Gets the c_category value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_category
     */
    public java.lang.String getC_category() {
        return c_category;
    }


    /**
     * Sets the c_category value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_category
     */
    public void setC_category(java.lang.String c_category) {
        this.c_category = c_category;
    }


    /**
     * Gets the c_economictype value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_economictype
     */
    public java.lang.String getC_economictype() {
        return c_economictype;
    }


    /**
     * Sets the c_economictype value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_economictype
     */
    public void setC_economictype(java.lang.String c_economictype) {
        this.c_economictype = c_economictype;
    }


    /**
     * Gets the c_phonenumber value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_phonenumber
     */
    public java.lang.String getC_phonenumber() {
        return c_phonenumber;
    }


    /**
     * Sets the c_phonenumber value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_phonenumber
     */
    public void setC_phonenumber(java.lang.String c_phonenumber) {
        this.c_phonenumber = c_phonenumber;
    }


    /**
     * Gets the c_fax value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_fax
     */
    public java.lang.String getC_fax() {
        return c_fax;
    }


    /**
     * Sets the c_fax value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_fax
     */
    public void setC_fax(java.lang.String c_fax) {
        this.c_fax = c_fax;
    }


    /**
     * Gets the c_address value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_address
     */
    public java.lang.String getC_address() {
        return c_address;
    }


    /**
     * Sets the c_address value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_address
     */
    public void setC_address(java.lang.String c_address) {
        this.c_address = c_address;
    }


    /**
     * Gets the c_postalcode value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_postalcode
     */
    public java.lang.String getC_postalcode() {
        return c_postalcode;
    }


    /**
     * Sets the c_postalcode value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_postalcode
     */
    public void setC_postalcode(java.lang.String c_postalcode) {
        this.c_postalcode = c_postalcode;
    }


    /**
     * Gets the c_email value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_email
     */
    public java.lang.String getC_email() {
        return c_email;
    }


    /**
     * Sets the c_email value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_email
     */
    public void setC_email(java.lang.String c_email) {
        this.c_email = c_email;
    }


    /**
     * Gets the c_web value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_web
     */
    public java.lang.String getC_web() {
        return c_web;
    }


    /**
     * Sets the c_web value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_web
     */
    public void setC_web(java.lang.String c_web) {
        this.c_web = c_web;
    }


    /**
     * Gets the c_introduction value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_introduction
     */
    public java.lang.String getC_introduction() {
        return c_introduction;
    }


    /**
     * Sets the c_introduction value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_introduction
     */
    public void setC_introduction(java.lang.String c_introduction) {
        this.c_introduction = c_introduction;
    }


    /**
     * Gets the c_deptduty value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_deptduty
     */
    public java.lang.String getC_deptduty() {
        return c_deptduty;
    }


    /**
     * Sets the c_deptduty value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_deptduty
     */
    public void setC_deptduty(java.lang.String c_deptduty) {
        this.c_deptduty = c_deptduty;
    }


    /**
     * Gets the c_remark value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_remark
     */
    public java.lang.String getC_remark() {
        return c_remark;
    }


    /**
     * Sets the c_remark value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_remark
     */
    public void setC_remark(java.lang.String c_remark) {
        this.c_remark = c_remark;
    }


    /**
     * Gets the c_status value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_status
     */
    public java.lang.String getC_status() {
        return c_status;
    }


    /**
     * Sets the c_status value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_status
     */
    public void setC_status(java.lang.String c_status) {
        this.c_status = c_status;
    }


    /**
     * Gets the c_validdate value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_validdate
     */
    public java.util.Date getC_validdate() {
        return c_validdate;
    }


    /**
     * Sets the c_validdate value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_validdate
     */
    public void setC_validdate(java.util.Date c_validdate) {
        this.c_validdate = c_validdate;
    }


    /**
     * Gets the c_level value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_level
     */
    public java.math.BigDecimal getC_level() {
        return c_level;
    }


    /**
     * Sets the c_level value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_level
     */
    public void setC_level(java.math.BigDecimal c_level) {
        this.c_level = c_level;
    }


    /**
     * Gets the c_begindate value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_begindate
     */
    public java.util.Date getC_begindate() {
        return c_begindate;
    }


    /**
     * Sets the c_begindate value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_begindate
     */
    public void setC_begindate(java.util.Date c_begindate) {
        this.c_begindate = c_begindate;
    }


    /**
     * Gets the c_enddate value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_enddate
     */
    public java.util.Date getC_enddate() {
        return c_enddate;
    }


    /**
     * Sets the c_enddate value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_enddate
     */
    public void setC_enddate(java.util.Date c_enddate) {
        this.c_enddate = c_enddate;
    }


    /**
     * Gets the c_businesstype value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_businesstype
     */
    public java.lang.String getC_businesstype() {
        return c_businesstype;
    }


    /**
     * Sets the c_businesstype value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_businesstype
     */
    public void setC_businesstype(java.lang.String c_businesstype) {
        this.c_businesstype = c_businesstype;
    }


    /**
     * Gets the c_lay value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_lay
     */
    public java.lang.String getC_lay() {
        return c_lay;
    }


    /**
     * Sets the c_lay value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_lay
     */
    public void setC_lay(java.lang.String c_lay) {
        this.c_lay = c_lay;
    }


    /**
     * Gets the c_businessarea value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_businessarea
     */
    public java.lang.String getC_businessarea() {
        return c_businessarea;
    }


    /**
     * Sets the c_businessarea value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_businessarea
     */
    public void setC_businessarea(java.lang.String c_businessarea) {
        this.c_businessarea = c_businessarea;
    }


    /**
     * Gets the c_othername value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_othername
     */
    public java.lang.String getC_othername() {
        return c_othername;
    }


    /**
     * Sets the c_othername value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_othername
     */
    public void setC_othername(java.lang.String c_othername) {
        this.c_othername = c_othername;
    }


    /**
     * Gets the c_parentunitid value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_parentunitid
     */
    public java.math.BigDecimal getC_parentunitid() {
        return c_parentunitid;
    }


    /**
     * Sets the c_parentunitid value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_parentunitid
     */
    public void setC_parentunitid(java.math.BigDecimal c_parentunitid) {
        this.c_parentunitid = c_parentunitid;
    }


    /**
     * Gets the c_order value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_order
     */
    public java.math.BigDecimal getC_order() {
        return c_order;
    }


    /**
     * Sets the c_order value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_order
     */
    public void setC_order(java.math.BigDecimal c_order) {
        this.c_order = c_order;
    }


    /**
     * Gets the c_structure value for this ORGANIZATION_VIEWLine.
     * 
     * @return c_structure
     */
    public java.math.BigDecimal getC_structure() {
        return c_structure;
    }


    /**
     * Sets the c_structure value for this ORGANIZATION_VIEWLine.
     * 
     * @param c_structure
     */
    public void setC_structure(java.math.BigDecimal c_structure) {
        this.c_structure = c_structure;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ORGANIZATION_VIEWLine)) return false;
        ORGANIZATION_VIEWLine other = (ORGANIZATION_VIEWLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.c_oid_orgunit==null && other.getC_oid_orgunit()==null) || 
             (this.c_oid_orgunit!=null &&
              this.c_oid_orgunit.equals(other.getC_oid_orgunit()))) &&
            ((this.c_name==null && other.getC_name()==null) || 
             (this.c_name!=null &&
              this.c_name.equals(other.getC_name()))) &&
            ((this.c_code==null && other.getC_code()==null) || 
             (this.c_code!=null &&
              this.c_code.equals(other.getC_code()))) &&
            ((this.c_category==null && other.getC_category()==null) || 
             (this.c_category!=null &&
              this.c_category.equals(other.getC_category()))) &&
            ((this.c_economictype==null && other.getC_economictype()==null) || 
             (this.c_economictype!=null &&
              this.c_economictype.equals(other.getC_economictype()))) &&
            ((this.c_phonenumber==null && other.getC_phonenumber()==null) || 
             (this.c_phonenumber!=null &&
              this.c_phonenumber.equals(other.getC_phonenumber()))) &&
            ((this.c_fax==null && other.getC_fax()==null) || 
             (this.c_fax!=null &&
              this.c_fax.equals(other.getC_fax()))) &&
            ((this.c_address==null && other.getC_address()==null) || 
             (this.c_address!=null &&
              this.c_address.equals(other.getC_address()))) &&
            ((this.c_postalcode==null && other.getC_postalcode()==null) || 
             (this.c_postalcode!=null &&
              this.c_postalcode.equals(other.getC_postalcode()))) &&
            ((this.c_email==null && other.getC_email()==null) || 
             (this.c_email!=null &&
              this.c_email.equals(other.getC_email()))) &&
            ((this.c_web==null && other.getC_web()==null) || 
             (this.c_web!=null &&
              this.c_web.equals(other.getC_web()))) &&
            ((this.c_introduction==null && other.getC_introduction()==null) || 
             (this.c_introduction!=null &&
              this.c_introduction.equals(other.getC_introduction()))) &&
            ((this.c_deptduty==null && other.getC_deptduty()==null) || 
             (this.c_deptduty!=null &&
              this.c_deptduty.equals(other.getC_deptduty()))) &&
            ((this.c_remark==null && other.getC_remark()==null) || 
             (this.c_remark!=null &&
              this.c_remark.equals(other.getC_remark()))) &&
            ((this.c_status==null && other.getC_status()==null) || 
             (this.c_status!=null &&
              this.c_status.equals(other.getC_status()))) &&
            ((this.c_validdate==null && other.getC_validdate()==null) || 
             (this.c_validdate!=null &&
              this.c_validdate.equals(other.getC_validdate()))) &&
            ((this.c_level==null && other.getC_level()==null) || 
             (this.c_level!=null &&
              this.c_level.equals(other.getC_level()))) &&
            ((this.c_begindate==null && other.getC_begindate()==null) || 
             (this.c_begindate!=null &&
              this.c_begindate.equals(other.getC_begindate()))) &&
            ((this.c_enddate==null && other.getC_enddate()==null) || 
             (this.c_enddate!=null &&
              this.c_enddate.equals(other.getC_enddate()))) &&
            ((this.c_businesstype==null && other.getC_businesstype()==null) || 
             (this.c_businesstype!=null &&
              this.c_businesstype.equals(other.getC_businesstype()))) &&
            ((this.c_lay==null && other.getC_lay()==null) || 
             (this.c_lay!=null &&
              this.c_lay.equals(other.getC_lay()))) &&
            ((this.c_businessarea==null && other.getC_businessarea()==null) || 
             (this.c_businessarea!=null &&
              this.c_businessarea.equals(other.getC_businessarea()))) &&
            ((this.c_othername==null && other.getC_othername()==null) || 
             (this.c_othername!=null &&
              this.c_othername.equals(other.getC_othername()))) &&
            ((this.c_parentunitid==null && other.getC_parentunitid()==null) || 
             (this.c_parentunitid!=null &&
              this.c_parentunitid.equals(other.getC_parentunitid()))) &&
            ((this.c_order==null && other.getC_order()==null) || 
             (this.c_order!=null &&
              this.c_order.equals(other.getC_order()))) &&
            ((this.c_structure==null && other.getC_structure()==null) || 
             (this.c_structure!=null &&
              this.c_structure.equals(other.getC_structure())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getC_oid_orgunit() != null) {
            _hashCode += getC_oid_orgunit().hashCode();
        }
        if (getC_name() != null) {
            _hashCode += getC_name().hashCode();
        }
        if (getC_code() != null) {
            _hashCode += getC_code().hashCode();
        }
        if (getC_category() != null) {
            _hashCode += getC_category().hashCode();
        }
        if (getC_economictype() != null) {
            _hashCode += getC_economictype().hashCode();
        }
        if (getC_phonenumber() != null) {
            _hashCode += getC_phonenumber().hashCode();
        }
        if (getC_fax() != null) {
            _hashCode += getC_fax().hashCode();
        }
        if (getC_address() != null) {
            _hashCode += getC_address().hashCode();
        }
        if (getC_postalcode() != null) {
            _hashCode += getC_postalcode().hashCode();
        }
        if (getC_email() != null) {
            _hashCode += getC_email().hashCode();
        }
        if (getC_web() != null) {
            _hashCode += getC_web().hashCode();
        }
        if (getC_introduction() != null) {
            _hashCode += getC_introduction().hashCode();
        }
        if (getC_deptduty() != null) {
            _hashCode += getC_deptduty().hashCode();
        }
        if (getC_remark() != null) {
            _hashCode += getC_remark().hashCode();
        }
        if (getC_status() != null) {
            _hashCode += getC_status().hashCode();
        }
        if (getC_validdate() != null) {
            _hashCode += getC_validdate().hashCode();
        }
        if (getC_level() != null) {
            _hashCode += getC_level().hashCode();
        }
        if (getC_begindate() != null) {
            _hashCode += getC_begindate().hashCode();
        }
        if (getC_enddate() != null) {
            _hashCode += getC_enddate().hashCode();
        }
        if (getC_businesstype() != null) {
            _hashCode += getC_businesstype().hashCode();
        }
        if (getC_lay() != null) {
            _hashCode += getC_lay().hashCode();
        }
        if (getC_businessarea() != null) {
            _hashCode += getC_businessarea().hashCode();
        }
        if (getC_othername() != null) {
            _hashCode += getC_othername().hashCode();
        }
        if (getC_parentunitid() != null) {
            _hashCode += getC_parentunitid().hashCode();
        }
        if (getC_order() != null) {
            _hashCode += getC_order().hashCode();
        }
        if (getC_structure() != null) {
            _hashCode += getC_structure().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ORGANIZATION_VIEWLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://hr.publisher.mdm", "ORGANIZATION_VIEWLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_oid_orgunit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_oid_orgunit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_category");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_category"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_economictype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_economictype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_phonenumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_phonenumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_fax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_fax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_postalcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_postalcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_email");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_web");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_web"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_introduction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_introduction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_deptduty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_deptduty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_remark");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_remark"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_validdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_validdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_level");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_level"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_begindate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_begindate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_enddate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_enddate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_businesstype");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_businesstype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_lay");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_lay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_businessarea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_businessarea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_othername");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_othername"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_parentunitid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_parentunitid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_order");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_order"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_structure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_structure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
