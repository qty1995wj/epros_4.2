package com.jecn.epros.server.action.web.dataImport.sync.excelorDB;

import java.io.InputStream;

import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.UserDataFileDownBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;

/**
 * 标准版人员同步Excel下载处理
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Mar 1, 2013 时间：10:52:37 AM
 */
public class UserDataFileDownAction extends BaseAction {
	/** 对应的业务处理类 */
	private UserDataFileDownBuss userDataFileDownBuss = null;

	/**
	 * 
	 * 对外接口，导出人员数据（excel方式）
	 * 
	 * @return
	 */
	public String downUserDataExcel() {

		// 保存数据到本地
		boolean ret = userDataFileDownBuss.downUserDataExcel();
		if (!ret) {
			ResultHtml(SyncErrorInfo.USER_FILE_DOWN_FAIL,
					SyncConstant.HELF_DATA_INPUT_ACTION);
			return null;
		}
		return SyncConstant.RESULT_SUCCESS;
	}

	/**
	 * 
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() {
		return userDataFileDownBuss.getInputStream();
	}

	/**
	 * 
	 * 对于配置中的 ${fileName}, 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	public String getFileName() {
		return userDataFileDownBuss.getFileName();
	}

	public UserDataFileDownBuss getUserDataFileDownBuss() {
		return userDataFileDownBuss;
	}

	public void setUserDataFileDownBuss(
			UserDataFileDownBuss userDataFileDownBuss) {
		this.userDataFileDownBuss = userDataFileDownBuss;
	}

}
