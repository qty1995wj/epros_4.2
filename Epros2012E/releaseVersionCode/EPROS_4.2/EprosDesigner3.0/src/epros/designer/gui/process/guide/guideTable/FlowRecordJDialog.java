package epros.designer.gui.process.guide.guideTable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowRecordT;

import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.operationConfig.OperationConfigUtil;
import epros.draw.gui.swing.JecnOKCancelPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 记录保存Table
 * 
 * @author fuzhh Jan 6, 2013
 * 
 */
public class FlowRecordJDialog extends JecnDialog implements ActionListener,
		CaretListener {
	private static Logger log = Logger.getLogger(FlowRecordJDialog.class);
	/** 主panel */
	private JPanel mainPan;
	/** 数据输入界面 */
	private JPanel dataPan;
	/** 记录名称 */
	private JLabel recordNameLab;
	private JTextField recordNameText;
	private JLabel recordNameError;
	private JLabel querLabel;

	
	/** 编号 */
	private JLabel docIdLab;
	private JTextField docIdText;
	private JLabel docIdError;
	
	/** 移交责任人 */
	private JLabel transferPeopleLab;
	private JTextField transferPeopleText;
	private JLabel transferPeopleError;
	
	/** 保存责任人 */
	private JLabel savePeopleLab;
	private JTextField savePeopleText;
	private JLabel savePeopleError;
	/** 保存场所 */
	private JLabel savePlaceLab;
	private JTextField savePlaceText;
	private JLabel savePlaceError;
	/** 归档时间 */
	private JLabel filingTimeLab;
	private JTextField filingTimeText;
	private JLabel filingTimeError;
	/** 保存期限 */
	private JLabel storageLifeLab;
	private JTextField storageLifeText;
	private JLabel storageLifeError;
	/** 到期处理方式 */
	private JLabel treatmentDueLab;
	private JTextArea treatmentDueText;
	private JLabel treatmentDueError;
	private JScrollPane treatmentDueScrollPane;
	/** 确定取消面板 */
	private JecnOKCancelPanel jecnOKCancelPanel;

	private boolean isRecordName = true;
	private boolean isSavePeople = true;
	private boolean isDocId = true;
	private boolean isTransferPeople = true;
	private boolean isSavePlace = true;
	private boolean isFilingTime = true;
	private boolean isStorageLife = true;
	private boolean isTreatmentDue = true;

	/** 记录保存数据 */
	private JecnFlowRecordT flowRecordT;
	/** 是否执行确定按钮 */
	public boolean isSave = false;
	/** 动作标识 true 为保存 false 为编辑 */
	public boolean flag;

	public FlowRecordJDialog(JecnFlowRecordT flowRecordT, boolean flag) {
		if (flowRecordT == null) {
			return;
		}
		this.flowRecordT = flowRecordT;
		this.flag = flag;

		initComponents();
		initLayout();

		if (!flag) {
			initLayoutData();
		}
	}

	private void initComponents() {
		this.setTitle(JecnProperties.getValue("recordsFor"));
		this.setSize(400, 400);
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setResizable(true);

		// 主panel
		mainPan = new JPanel();
		mainPan.setLayout(new BorderLayout(0, 0));
		mainPan.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 数据输入界面
		dataPan = new JPanel();
		dataPan.setLayout(new GridBagLayout());
		dataPan.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 记录名称
		recordNameLab = new JLabel(JecnProperties.getValue("recordNameC"));
		recordNameText = new JTextField();
		recordNameText.addCaretListener(this);
		querLabel = new JLabel("*");
		querLabel.setForeground(Color.red);
		recordNameError = new JLabel();
		recordNameError.setForeground(Color.red);
		
		
		// 编号
		docIdLab = new JLabel(JecnProperties
				.getValue("numC"));
		docIdText = new JTextField();
		docIdText.addCaretListener(this);
		docIdError = new JLabel();
		docIdError.setForeground(Color.red);
		
		if(!JecnConfigTool.isShowRecordTransferAndNum()){
			docIdLab.setVisible(false);
			docIdText.setVisible(false);
			docIdError.setVisible(false);
		}
		
		// 移交责任人
		transferPeopleLab = new JLabel(JecnProperties
				.getValue("transferPeopleC"));
		transferPeopleText = new JTextField();
		transferPeopleText.addCaretListener(this);
		transferPeopleError = new JLabel();
		transferPeopleError.setForeground(Color.red);
		
		if(!JecnConfigTool.isShowRecordTransferAndNum()){
			transferPeopleLab.setVisible(false);
			transferPeopleText.setVisible(false);
			transferPeopleError.setVisible(false);
		}

		// 保存责任人
		savePeopleLab = new JLabel(JecnProperties
				.getValue("saveResponsiblePersonsC"));
		savePeopleText = new JTextField();
		savePeopleText.addCaretListener(this);
		savePeopleError = new JLabel();
		savePeopleError.setForeground(Color.red);

		// 保存场所
		savePlaceLab = new JLabel(JecnProperties.getValue("savePlaceC"));
		savePlaceText = new JTextField();
		savePlaceText.addCaretListener(this);
		savePlaceError = new JLabel();
		savePlaceError.setForeground(Color.red);

		// 归档时间
		filingTimeLab = new JLabel(JecnProperties.getValue("filingTimeC"));
		filingTimeText = new JTextField();
		filingTimeText.addCaretListener(this);
		// filingTimeText.setEditable(false);

		filingTimeError = new JLabel();
		filingTimeError.setForeground(Color.red);
		// 保存期限
		storageLifeLab = new JLabel(JecnProperties.getValue("storageLifeC"));
		storageLifeText = new JTextField();
		storageLifeText.addCaretListener(this);
		storageLifeError = new JLabel();
		storageLifeError.setForeground(Color.red);
		// 到期处理方式
		treatmentDueLab = new JLabel(JecnProperties.getValue("treatmentDueC"));
		treatmentDueText = new JTextArea();
		treatmentDueText.setColumns(5);
		treatmentDueText.setWrapStyleWord(true);
		treatmentDueText.setLineWrap(true);
		treatmentDueText.addCaretListener(this);
		treatmentDueScrollPane = new JScrollPane(treatmentDueText);

		treatmentDueError = new JLabel();
		treatmentDueError.setForeground(Color.red);

		// 确定取消面板
		jecnOKCancelPanel = new JecnOKCancelPanel();
		// 确定按钮事件
		jecnOKCancelPanel.getOkBtn().addActionListener(this);
		jecnOKCancelPanel.getOkBtn().setName("okBut");
		// 取消按钮事件
		jecnOKCancelPanel.getCancelBtn().addActionListener(this);
		jecnOKCancelPanel.getCancelBtn().setName("cancelBtn");
	}

	public void initLayout() {
		Insets insetsLeft = new Insets(5, 5, 0, 0);
		Insets insetsRight = new Insets(5, 5, 0, 5);
        
		int y=0;
		// 记录名称
		GridBagConstraints c = new GridBagConstraints(0, y, 1, 1, 0, 0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLeft,
				0, 0);
		dataPan.add(recordNameLab, c);
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsRight, 0, 0);
		dataPan.add(recordNameText, c);
		c = new GridBagConstraints(2, y, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,
						0, 0, 5), 0, 0);
		dataPan.add(querLabel, c);
		y++;
		// 错误提示
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		dataPan.add(recordNameError, c);

	    y++;
		// 编号
		c = new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insetsLeft, 0, 0);
		dataPan.add(docIdLab, c);
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsRight, 0, 0);
		dataPan.add(docIdText, c);
		y++;
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		dataPan.add(docIdError, c);
		y++;
		// 移交责任人
		c = new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insetsLeft, 0, 0);
		dataPan.add(transferPeopleLab, c);
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsRight, 0, 0);
		dataPan.add(transferPeopleText, c);
		y++;
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		dataPan.add(transferPeopleError, c);
		y++;
		// 保存责任人
		c = new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insetsLeft, 0, 0);
		dataPan.add(savePeopleLab, c);
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsRight, 0, 0);
		dataPan.add(savePeopleText, c);
		y++;
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		dataPan.add(savePeopleError, c);
        y++;
		// 保存场所
		c = new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insetsLeft, 0, 0);
		dataPan.add(savePlaceLab, c);
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsRight, 0, 0);
		dataPan.add(savePlaceText, c);
		y++;
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		dataPan.add(savePlaceError, c);
        y++;
		// 归档时间
		c = new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insetsLeft, 0, 0);
		dataPan.add(filingTimeLab, c);
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsRight, 0, 0);
		dataPan.add(filingTimeText, c);
		y++;
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		dataPan.add(filingTimeError, c);
        y++;
		// 保存期限
		c = new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insetsLeft, 0, 0);
		dataPan.add(storageLifeLab, c);
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsRight, 0, 0);
		dataPan.add(storageLifeText, c);
		y++;
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		dataPan.add(storageLifeError, c);
        y++;
		// 到期处理方式
		c = new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insetsLeft, 0, 0);
		dataPan.add(treatmentDueLab, c);
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 1.0,
				GridBagConstraints.EAST, GridBagConstraints.BOTH, insetsRight,
				0, 0);
		dataPan.add(treatmentDueScrollPane, c);
		y++;
		c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0555(), 0, 0);
		dataPan.add(treatmentDueError, c);

		// 数据panel添加到主panel
		mainPan.add(dataPan, BorderLayout.CENTER);
		// 确定面板添加到主panel
		mainPan.add(jecnOKCancelPanel, BorderLayout.SOUTH);

		this.getContentPane().add(mainPan);
	}

	/**
	 * 如果为编辑初始化数据
	 * 
	 * @author fuzhh Nov 5, 2012
	 */
	public void initLayoutData() {
		// 记录名称
		recordNameText.setText(flowRecordT.getRecordName());
		docIdText.setText(flowRecordT.getDocId());
		transferPeopleText.setText(flowRecordT.getRecordTransferPeople());
		// 保存责任人
		savePeopleText.setText(flowRecordT.getRecordSavePeople());
		// 保存场所
		savePlaceText.setText(flowRecordT.getSaveLaction());
		// 归档时间
		filingTimeText.setText(flowRecordT.getFileTime());
		// 保存期限
		storageLifeText.setText(flowRecordT.getSaveTime());
		// 到期处理方式
		treatmentDueText.setText(flowRecordT.getRecordApproach());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton but = (JButton) e.getSource();
			String butName = but.getName();
			if ("okBut".equals(butName)) { // 确定事件
				caretUpdate(null);
				if (isRecordName && isDocId && isTransferPeople && isSavePeople && isSavePlace && isFilingTime
						&& isStorageLife && isTreatmentDue) {
					// 记录名称
					String recordName = recordNameText.getText();
					flowRecordT.setRecordName(recordName);
					// docId
					String docId = docIdText.getText();
					flowRecordT.setDocId(docId);
					// 移交责任人
					String transferPeople = transferPeopleText.getText();
					flowRecordT.setRecordTransferPeople(transferPeople);
					// 保存责任人
					String savePeople = savePeopleText.getText();
					flowRecordT.setRecordSavePeople(savePeople);
					// 保存场所
					String savePlace = savePlaceText.getText();
					flowRecordT.setSaveLaction(savePlace);
					// 归档时间
					String filingTime = filingTimeText.getText();
					flowRecordT.setFileTime(filingTime);
					// 保存期限
					String storageLife = storageLifeText.getText();
					flowRecordT.setSaveTime(storageLife);
					// 到期处理方式
					String treatmentDue = treatmentDueText.getText();
					flowRecordT.setRecordApproach(treatmentDue);
					isSave = true;
					this.dispose();
				}
			} else if ("cancelBtn".equals(butName)) { // 取消事件
				this.dispose();
			}
		}
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		// 记录名称
		isRecordName = OperationConfigUtil.checkInOutIsNullContent(
				recordNameText.getText(), recordNameError);
		// 文控id
		isDocId = OperationConfigUtil.checkInOutContent(docIdText
				.getText(), docIdError);
		// 移交责任人
		isTransferPeople = OperationConfigUtil.checkInOutContent(transferPeopleText
				.getText(), transferPeopleError);
		// 保存责任人
		isSavePeople = OperationConfigUtil.checkInOutContent(savePeopleText
				.getText(), savePeopleError);
		// 保存场所
		isSavePlace = OperationConfigUtil.checkInOutContent(savePlaceText
				.getText(), savePlaceError);
		// 归档时间
		isFilingTime = OperationConfigUtil.checkInOutContent(filingTimeText
				.getText(), filingTimeError);
		// 保存期限
		isStorageLife = OperationConfigUtil.checkInOutContent(storageLifeText
				.getText(), storageLifeError);
		// 到期处理方式
		isTreatmentDue = OperationConfigUtil.checkInOutContent(treatmentDueText
				.getText(), treatmentDueError);
	}
}
