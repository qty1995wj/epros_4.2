package com.jecn.epros.server.action.designer.process.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.process.IProcessTemplateAction;
import com.jecn.epros.server.bean.process.JecnProcessTemplateBean;
import com.jecn.epros.server.bean.process.JecnProcessTemplateFollowBean;
import com.jecn.epros.server.service.process.IProcessTemplateService;

/**
 * @author yxw 2012-8-10
 * @description：模型
 */
public class ProcessTemplateActionImpl implements IProcessTemplateAction {

	private IProcessTemplateService processTemplateService;

	public void setProcessTemplateService(IProcessTemplateService processTemplateService) {
		this.processTemplateService = processTemplateService;
	}

	@Override
	public JecnProcessTemplateBean saveTemplate(JecnProcessTemplateBean processTemplateBean,
			JecnProcessTemplateFollowBean processTemplateFollowBean) throws Exception {
		return processTemplateService.saveTemplate(processTemplateBean, processTemplateFollowBean);
	}

	@Override
	public void deleteTemplate(JecnProcessTemplateBean processTemplateBean, long id) throws Exception {
		processTemplateService.deleteTemplate(processTemplateBean, id);
	}

	@Override
	public void updateTemplate(long id, String name, Long peopleId) throws Exception {
		processTemplateService.updateTemplate(id, name, peopleId);
	}

	@Override
	public List<JecnProcessTemplateBean> getJecnProcessTemplateBeanList() throws Exception {
		List<JecnProcessTemplateBean> list = processTemplateService.getJecnProcessTemplateBeanList();
		for (JecnProcessTemplateBean jecnProcessTemplateBean : list) {
			jecnProcessTemplateBean.setContent(null);
			if (jecnProcessTemplateBean.getListJecnProcessTemplateFollowBean() != null) {
				for (JecnProcessTemplateFollowBean jecnProcessTemplateFollowBean : jecnProcessTemplateBean
						.getListJecnProcessTemplateFollowBean()) {
					jecnProcessTemplateFollowBean.setImageContent(null);
				}
			}
		}
		return list;
	}

}
