/*
 * Triangle.java
 *三角
 * Created on 2008年11月3日, 下午2:13
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnPaintEnum;

/**
 * @date： 日期：Mar 21, 2012 时间：3:36:28 PM
 * 
 * 
 * @author Administrator
 */
public class Triangle extends JecnBaseFigurePanel {
	protected int x1;
	protected int y1;
	protected int x2;
	protected int y2;
	protected int x3;
	protected int y3; // 定义坐标属性

	public Triangle(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = userWidth / 2;
			y1 = 0;
			x2 = userWidth;
			y2 = userHeight;
			x3 = 0;
			y3 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = userWidth;
			y1 = userHeight / 2;
			x2 = 0;
			y2 = userHeight;
			x3 = 0;
			y3 = 0;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = userWidth / 2;
			y1 = userHeight;
			x2 = 0;
			y2 = 0;
			x3 = userWidth;
			y3 = 0;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = userHeight / 2;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paintNoneFigure(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, 0, 0,
				0, y1, y2, y3, 0, 0, 0, userWidth, userHeight);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintTriangleTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintTriangleTop(g2d, linevalue);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	protected void paintTriangleTop(Graphics2D g2d, int shadowCount) {
		// 顶层 原图，榜值均为零
		JecnPaintEnum.INTANCE.paintHandTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, 0, 0, 0,
				y1, y2, y3, 0, 0, 0, shadowCount, shadowCount, userWidth, userHeight);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DTriangleLow(g2d, 0);
	}

	/**
	 * 既有阴影也有3D
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            缩进间距
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DTriangleLow(g2d, linevalue);
	}

	/**
	 * 3D图形底层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 */
	protected void paint3DTriangleLow(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DHandLow(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, 0, 0,
				0, y1, y2, y3, 0, 0, 0, indent3Dvalue, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DTriangleLine(g2d, 0);
	}

	/**
	 * 3D效果设置线条 底层边框
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 */
	protected void paint3DTriangleLine(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DInputHandLine(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3,
				0, 0, 0, y1, y2, y3, 0, 0, 0, indent3Dvalue, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 3D底层边框 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DTriangleLine(g2d, linevalue);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, 0, 0,
				0, y1, y2, y3, 0, 0, 0, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DAndShadowsPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2,
				x3, 0, 0, 0, y1, y2, y3, 0, 0, 0, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	protected void paintShadows(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount }, new int[] {
					y1 + shadowCount, y2 + shadowCount, y3 + shadowCount }, 3);
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount }, new int[] {
					y1 + shadowCount, y2, y3 + shadowCount }, 3);

		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount }, new int[] {
					y1 + shadowCount, y2 + shadowCount, y3 + shadowCount }, 3);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount / 2, x3 + shadowCount / 2 }, new int[] {
					y1 + shadowCount, y2 + shadowCount, y3 + shadowCount / 2 }, 3);
		}
	}
}
