package com.jecn.epros.server.download.wordxml;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import wordxml.constant.Constants;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineType;
import wordxml.element.bean.common.LineBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.IBody;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

public class JecnWordUtil {

	private static final Log log = LogFactory.getLog(JecnWordUtil.class);

	// 小四 宋体
	public static final FontBean XIAOSI;
	// 小四 宋体 加粗
	public static final FontBean XIAOSI_B;
	// 五号 宋体
	public static final FontBean WUHAO;
	// 五号 宋体 加粗
	public static final FontBean WUHAO_B;
	// 五号 宋体 蓝色
	public static final FontBean WUHAO_B_BLUE;
	// 五号 宋体 蓝色 下划线
	public static final FontBean WUHAO_B_U_BLUE;

	public static final ParagraphLineRule lineRule20 = new ParagraphLineRule(20, LineRuleType.EXACT);
	public static final ParagraphLineRule lineRule18 = new ParagraphLineRule(18, LineRuleType.EXACT);

	static {
		XIAOSI = new FontBean("宋体", Constants.xiaosi, false);
		XIAOSI_B = new FontBean("宋体", Constants.xiaosi, true);
		WUHAO = new FontBean("宋体", Constants.wuhao, false);
		WUHAO_B = new FontBean("宋体", Constants.wuhao, true);
		WUHAO_B_BLUE = new FontBean("宋体", Constants.wuhao, true);
		WUHAO_B_BLUE.setFontColor(Color.BLUE);
		WUHAO_B_U_BLUE = new FontBean("宋体", Constants.wuhao, true);
		WUHAO_B_U_BLUE.setUnderlineBean(new LineBean(LineType.SINGLE, Color.BLUE));
		WUHAO_B_U_BLUE.setFontColor(Color.BLUE);
	}

	/**
	 * 按照换行符截取字符串数据
	 * 
	 * @param content
	 * @return
	 */
	public static String[] getContents(Object content) {
		if (content == null) {
			return new String[] { "" };
		}
		return content.toString().split("\n");
	}

	/**
	 * 创建表格
	 * 
	 * @param sect
	 *            所属节
	 * @param tabBean
	 *            表格样式
	 * @param widths
	 *            表格宽度
	 * @param rowData
	 *            行数据
	 */
	public static Table createTab(IBody body, TableBean tabBean, float[] width, List<String[]> rowData,
			ParagraphBean pBean) {
		// 创建表格
		Table tbl = body.createTable(tabBean, width);
		tbl.createTableRow(rowData, pBean);
		return tbl;
	}

	/**
	 * 创建表格中的表格
	 * 
	 * @param paragraph
	 */
	public static Table createCellTable(Paragraph paragraph, List<String[]> rowData, TableBean cellBean,
			float[] cellWidths, ParagraphBean tblPBean) {
		Table tab = paragraph.createTable(cellBean, cellWidths);
		tab.createTableRow(rowData, tblPBean);
		return tab;
	}

	/**
	 * 重设表格宽度
	 * 
	 * @param oldWidth
	 */
	public static void resizeTblWidth(float[] oldWidth, float newTblWidth) {
		float old = 0;
		for (float f : oldWidth) {
			old += f;
		}
		// 获取缩放比例
		float scale = Float.valueOf(String.format("%.2f", newTblWidth / old));
		for (int i = 0; i < oldWidth.length; i++) {
			oldWidth[i] = oldWidth[i] * scale;
		}
	}

	/**
	 * 转换Obj数组为String数组
	 * 
	 * @param obj
	 * @return
	 */
	public static String[] obj2String(Object[] obj) {
		if (obj == null) {
			return new String[] { "" };
		}
		String[] strArr = new String[obj.length];
		for (int i = 0; i < obj.length; i++) {
			strArr[i] = obj2String(obj[i]);
		}
		return strArr;
	}

	/**
	 * 转换Obj数组为String数组
	 * 
	 * @param obj
	 * @return
	 */
	public static List<String[]> obj2String(List<Object[]> objList) {
		if (objList == null) {
			return new ArrayList<String[]>();
		}
		List<String[]> strList = new ArrayList<String[]>();
		for (Object[] objArr : objList) {
			strList.add(obj2String(objArr));
		}
		return strList;
	}

	/**
	 * 转换Obj为String
	 * 
	 * @param obj
	 * @return
	 */
	public static String obj2String(Object obj) {
		return obj == null ? "" : obj.toString();
	}
}
