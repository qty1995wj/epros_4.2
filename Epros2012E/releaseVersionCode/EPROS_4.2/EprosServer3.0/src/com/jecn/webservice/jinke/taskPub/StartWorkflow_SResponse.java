/**
 * StartWorkflow_SResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class StartWorkflow_SResponse  implements java.io.Serializable {
    private com.jecn.webservice.jinke.taskPub.BPMServiceResult startWorkflow_SResult;

    public StartWorkflow_SResponse() {
    }

    public StartWorkflow_SResponse(
           com.jecn.webservice.jinke.taskPub.BPMServiceResult startWorkflow_SResult) {
           this.startWorkflow_SResult = startWorkflow_SResult;
    }


    /**
     * Gets the startWorkflow_SResult value for this StartWorkflow_SResponse.
     * 
     * @return startWorkflow_SResult
     */
    public com.jecn.webservice.jinke.taskPub.BPMServiceResult getStartWorkflow_SResult() {
        return startWorkflow_SResult;
    }


    /**
     * Sets the startWorkflow_SResult value for this StartWorkflow_SResponse.
     * 
     * @param startWorkflow_SResult
     */
    public void setStartWorkflow_SResult(com.jecn.webservice.jinke.taskPub.BPMServiceResult startWorkflow_SResult) {
        this.startWorkflow_SResult = startWorkflow_SResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StartWorkflow_SResponse)) return false;
        StartWorkflow_SResponse other = (StartWorkflow_SResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.startWorkflow_SResult==null && other.getStartWorkflow_SResult()==null) || 
             (this.startWorkflow_SResult!=null &&
              this.startWorkflow_SResult.equals(other.getStartWorkflow_SResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStartWorkflow_SResult() != null) {
            _hashCode += getStartWorkflow_SResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StartWorkflow_SResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">StartWorkflow_SResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startWorkflow_SResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "StartWorkflow_SResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "BPMServiceResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
