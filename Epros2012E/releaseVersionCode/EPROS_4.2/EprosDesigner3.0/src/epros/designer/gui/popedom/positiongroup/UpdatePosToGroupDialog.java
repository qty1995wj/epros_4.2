package epros.designer.gui.popedom.positiongroup;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.popedom.position.HighEfficiencyOrgPositionTree;
import epros.designer.gui.system.table.JecnTablePaging;
import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

public class UpdatePosToGroupDialog extends JecnDialog {
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();
	private JecnPosGroupSearchPanelPage jecnPosGroupSearchPanelPage;
	private JecnPosGroupResultPanelPage jecnPosGroupResultPanelPage;
	/** 删除选中 */
	private JButton closeBtn = new JButton(JecnProperties.getValue("closeBtn"));

	/** 按钮面板 */
	private JecnPanel buttonMainPanel = new JecnPanel();
	private Long groupId;

	/** 树滚动面板+快速查询面板的容器· */
	private JecnSplitPane splitPane = null;
	/** 树面板 */
	private JScrollPane treePanel = null;
	/** Tree 对象 */
	protected JecnTree jTree = null;

	private JecnPanel rightPanel = new JecnPanel();

	private JecnPanel centerPanel = new JecnPanel();

	public UpdatePosToGroupDialog(Long groupId) {
		this.groupId = groupId;
		this.setSize(800, 600);
		this.setResizable(false);
		this.setTitle(JecnProperties.getValue("connPosition"));
		this.setModal(true);
		this.setLocationRelativeTo(null);
		mainPanel.setLayout(new BorderLayout());
		this.getContentPane().add(mainPanel);
		initCom();
	}

	public void initCom() {
		initTree();
		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnPosGroupSearchPanelPage = new JecnPosGroupSearchPanelPage(0, centerPanel, new Insets(0, 0, 0, 0), this,
				groupId);
		jecnPosGroupResultPanelPage = new JecnPosGroupResultPanelPage(1, centerPanel, new Insets(0, 0, 0, 0), this,
				groupId);
		GridBagConstraints c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
				GridBagConstraints.NONE, new Insets(3, 3, 3, 3), 0, 0);
		centerPanel.add(buttonMainPanel, c);
		initButton();
		jecnPosGroupSearchPanelPage.setResultTable(jecnPosGroupResultPanelPage.resultTable);
	}

	private void initTree() {
		// 主面板
		rightPanel = new JecnPanel();
		rightPanel.setLayout(new BorderLayout());
		rightPanel.setBorder(null);
		rightPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// Tree 对象
		jTree = new HighEfficiencyOrgPositionTree();
		// 树面板
		treePanel = new JScrollPane();
		treePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("pleaseSelect")));
		treePanel.setViewportView(jTree);
		// 右侧面板
		// 树滚动面板+快速查询面板的容器
		splitPane = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, rightPanel, treePanel, centerPanel);
		// 左:右=3:7
		splitPane.setDividerLocation(0.3);
		// 当窗体变大时，右边面板获取空间,左面板不变
		splitPane.setResizeWeight(0.5);
		treePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.add(splitPane);
	}

	public void initButton() {
		buttonMainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonMainPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonMainPanel.add(closeBtn);
		closeBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				UpdatePosToGroupDialog.this.setVisible(false);
			}

		});
		jTree.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				jTreeMousePressed(evt);
			}
		});

	}

	public JecnTree getjTree() {
		return jTree;
	}

	public void setjTree(JecnTree jTree) {
		this.jTree = jTree;
	}

	/**
	 * 双击树选择
	 * 
	 * @param evt
	 */
	public void jTreeMousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jTree1MousePressed
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			if (evt.getClickCount() == 2) {
				if (!jTree.isSelectionEmpty()) {
					javax.swing.tree.TreePath treePath = jTree.getSelectionPath();
					JecnTreeNode node = (JecnTreeNode) treePath.getLastPathComponent();
					JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
					Set<Long> selectIds = new HashSet<Long>();
					selectIds.add(jecnTreeBean.getId());
					if (selectIds.size() > 0) {
						for (JecnTreeBean bean : jecnPosGroupResultPanelPage.resultTable.getList()) {
							if (selectIds.contains(bean.getId())) {
								jecnPosGroupResultPanelPage.resultTable.selectTableRows(bean.getId());
								selectIds.remove(bean.getId());
							}
						}
						try {
							// 执行数据库表的添加保存
							if (selectIds.size() > 0) {
								ConnectionPool.getPosGroup().AddSelectPoss(selectIds, groupId);
								jecnPosGroupResultPanelPage.resultTable.refreshTable("home");
							}
						} catch (Exception e) {
							log.error("JecnPosGroupSearchPanelPage method onlySelectBtnPerformed  error", e);
						}
					}
				}

			}
		}

	}

}
