package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

/**
 * 
 * xml方式导入对应的bean类
 * 
 * @author ZXH（迁移）
 * 
 */
public class XMLConfigBean extends AbstractConfigBean {
	/** 源xml文件路径 */
	private String srcXmlFilePath = null;

	public String getSrcXmlFilePath() {
		return srcXmlFilePath;
	}

	public void setSrcXmlFilePath(String srcXmlFilePath) {
		this.srcXmlFilePath = srcXmlFilePath;
	}

}
