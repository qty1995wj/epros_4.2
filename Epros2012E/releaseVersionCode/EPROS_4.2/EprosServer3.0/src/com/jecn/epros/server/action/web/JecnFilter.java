package com.jecn.epros.server.action.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 
 * 访问除login.jsp之外jsp页面情况，必须先判断是否已经登录（session存在），存在才访问后台action
 * 
 * @author yuxw
 *
 */
public class JecnFilter implements Filter {

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse rep = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		String url = req.getRequestURI();
		if (url.indexOf("login.jsp") > 0||url.indexOf("loginRedirect.jsp")>0||url.indexOf("aboutDaliog.jsp") > 0) {//是给定这些.jsp直接让进入后台
			chain.doFilter(request, response);
		} else if (session.getAttribute("webLoginBean") != null) {
			chain.doFilter(request, response);
		} else {
			rep.getWriter().write("<script language='javascript'> location.href = '"+req.getContextPath()+"/login.jsp';</script>");
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
