package com.jecn.epros.sqlprovider.server3;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Final {
    public static Logger log = Logger.getLogger(Final.class);

    public static final String NOPUBLIC = "noPublic";
    /**
     * 选择文件已删除！
     */
    public static final String IS_DELETE = "isDelete";
    /**
     * 文件存放的根目录*
     */
    public static final String FILE_DIR = "eprosFileLibrary/";
    /**
     * 保存流程或流程地图时候 保存流程图或流程地图的根目录*
     */
    public static final String FLOW_CHART = "flowChart/";
    /**
     * 发布流程或流程地图时候 发布流程或流程地图的程序文件*
     */
    public static final String FLOW_FILE_RELEASE = "flowFileRelease/";
    /**
     * 发布制度时候，发布制度的程序文件*
     */
    public static final String RULE_FILE_RELEASE = "ruleFileRelease/";
    /**
     * 文件存放的根目录*
     */
    public static final String FILE_LIBRARY = "fileLibrary/";
    /**
     * 存放临时文件夹
     */
    public static final String TEMP_DIR = "templet_dir/";
    /**
     * 流程或流程地图发布生成的图片目录
     */
    public static final String PUB_FLOW_CHART = "flowChartRelease/";
    /**
     * 人员变更excel数据导出 zhangft 2013-6-5
     */
    public static final String PERSONNEL_CHANGES_DATA = "personnelChangesData/";
    /**
     * 报表错误数据目录
     */
    public static final String REPORT_ERROR_DIR = "reportErrorData/";
    /** */


    /**
     * 获取文件临时目录
     *
     * @return
     * @author fuzhh Nov 8, 2012
     */
    public static String saveTempFilePath() {
        return FILE_DIR + TEMP_DIR + Common.getUUID() + ".doc";
    }

    /**
     * 获取文件临时目录
     *
     * @return
     * @author fuzhh Nov 8, 2012
     */
    public static String saveXmlFilePath() {
        return FILE_DIR + TEMP_DIR + Common.getUUID() + ".xml";
    }

    /**
     * 获取文件临时目录
     *
     * @return
     * @author fuzhh Nov 8, 2012
     */
    public static String saveExcelTempFilePath() {
        return FILE_DIR + TEMP_DIR + Common.getUUID() + ".xls";
    }

    /**
     * @param filePath
     * @return
     * @author yxw 2012-11-13
     * @description:返回临时文件全路径
     */
    public static String getAllTempPath(String filePath) {
        return Contants.jecn_path + filePath;
    }


    /**
     * @return filePath
     * @author zhangft 2013-6-5
     * @description: 返回人员数据变更xml目录
     */
    public static String savePersonnelChangesDataPath() {
        return FILE_DIR + PERSONNEL_CHANGES_DATA;
    }

    /**
     * @author zhangchen Nov 1, 2012
     * @description：启动服务时候，生成本地文件库的根目录
     */
    public static void startService() {
        String rootPath = Contants.jecn_path + FILE_DIR;
        // 创建根目录
        File file = new File(rootPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        // 创建流程图或流程地图保存的根目录
        file = new File(rootPath + FLOW_CHART);
        if (!file.exists()) {
            file.mkdirs();
        }
        // 创建流程图或流程地图发布的根目录
        file = new File(rootPath + PUB_FLOW_CHART);
        if (!file.exists()) {
            file.mkdirs();
        }
        // 创建流程或流程地图发布的程序文件
        file = new File(rootPath + FLOW_FILE_RELEASE);
        if (!file.exists()) {
            file.mkdirs();
        }
        // 创建制度发布的程序文件
        file = new File(rootPath + RULE_FILE_RELEASE);
        if (!file.exists()) {
            file.mkdirs();
        }
        // 创建文件库
        file = new File(rootPath + FILE_LIBRARY);
        if (!file.exists()) {
            file.mkdirs();
        }
        // 临时文件库
        file = new File(rootPath + TEMP_DIR);
        if (!file.exists()) {
            file.mkdirs();
        }
        // 人员变更数据导出文件库 zhangft 2013-6-5
        file = new File(rootPath + PERSONNEL_CHANGES_DATA);
        if (!file.exists()) {
            file.mkdirs();
        }

        //报表错误数据目录
        file = new File(rootPath + REPORT_ERROR_DIR);
        if (!file.exists()) {
            file.mkdirs();
        }

    }

    /**
     * @param date
     * @return
     * @author zhangchen May 30, 2012
     * @description：通过日期获得字符串
     */
    public static String getStringbyDate(Date date) {
        return new SimpleDateFormat("yyyyMM").format(date);
    }

    /***************************************************************************
     * @author zhangchen Nov 1, 2012
     * @description：获得文件全路径
     * @param rootPathType
     *            文件保存类型的根目录
     * @param createDate
     *            文件创建的时间
     * @param suffixName
     *            文件的后缀名
     * @return
     */
    public static String saveFilePath(String rootPathType, Date createDate,
                                      String suffixName) {
        File file = new File(Contants.jecn_path + FILE_DIR + rootPathType
                + getStringbyDate(createDate));
        if (!file.exists()) {
            file.mkdir();
        }
        return FILE_DIR + rootPathType + getStringbyDate(createDate) + "/"
                + Common.getUUID() + suffixName;
    }

    /**
     * @param fileName
     * @param fileId
     * @return
     * @author zhangchen May 30, 2012
     * @description：获得文件的后缀名
     */
    public static String getSuffixName(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."), fileName.length());
    }

    /**
     * @param bytes
     * @return
     * @throws IOException
     * @author zhangchen May 30, 2012
     * @description：保存流程地图和流程图
     */
    public static String getImagePathFileAndSave(byte[] bytes)
            throws IOException {
        String filePath = Final
                .saveFilePath(FLOW_CHART, new Date(), ".jpg");
        Final.createFile(filePath, bytes);
        return filePath;
    }

    /**
     * 保存分页流程图
     *
     * @param byteList
     * @return
     * @throws IOException
     * @author fuzhh 2014-2-21
     */
    public static void savePagingImage(List<byte[]> byteList, String fileName)
            throws IOException {
        int i = 1;
        for (byte[] data : byteList) {
            String filePath = Final.savePagingImagePath(FLOW_CHART,
                    new Date(), ".jpg", fileName, i);
            Final.createFile(filePath, data);
            i++;
        }
    }

    /**
     * 获取图片分页路径
     *
     * @param rootPathType 文件保存类型的根目录
     * @param createDate   文件创建的时间
     * @param suffixName   文件的后缀名
     * @return
     * @author fuzhh 2014-2-21
     */
    public static String savePagingImagePath(String rootPathType,
                                             Date createDate, String suffixName, String fileName, int i) {
        File file = new File(Contants.jecn_path + FILE_DIR + rootPathType
                + getStringbyDate(createDate) + "/" + fileName);
        if (!file.exists()) {
            file.mkdir();
        }
        return FILE_DIR + rootPathType + getStringbyDate(createDate) + "/"
                + fileName + "/" + i + suffixName;
    }

    /**
     * @param bytes
     * @return
     * @throws IOException
     * @author zhangchen May 30, 2012
     * @description：保存流程地图和流程图
     */
    public static String getImagePathByPub(byte[] bytes) throws IOException {
        String filePath = Final.saveFilePath(PUB_FLOW_CHART, new Date(),
                ".jpg");
        Final.createFile(filePath, bytes);
        return filePath;
    }

    /**
     * @param filePath
     * @param bytes
     * @throws IOException
     * @author zhangchen Nov 1, 2012
     * @description：创建文件的路径
     */
    public static void createFile(String filePath, byte[] bytes)
            throws IOException {
        File file = new File(Contants.jecn_path + filePath);

        FileOutputStream out = null;
        try {
            file.createNewFile();
            out = new FileOutputStream(file, false);
            out.write(bytes);
            out.flush();
        } catch (IOException ex) {
            log.error("写出异常", ex);
            throw ex;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                log.error("关闭输出流异常", ex);
            }
        }
    }

    /**
     * @param filePath
     * @param bytes
     * @throws IOException
     * @author zhangchen Nov 1, 2012
     * @description：创建文件的路径
     */
    public static void updateFile(String filePath, String fileOldPath,
                                  byte[] bytes) throws IOException {
        deleteFile(fileOldPath);
        createFile(filePath, bytes);
    }

    /**
     * @param filePath
     * @author zhangchen Nov 1, 2012
     * @description：删除文件
     */
    public static void deleteFile(String filePath) {
        if (filePath == null || "".equals(filePath)) {
            log.info("JecnFinal中方法deleteFile：文件丢失");
            return;
        }
        File file = new File(Contants.jecn_path + filePath);
        if (file.exists()) {
            file.delete();
        } else {
            log.info("JecnFinal中方法deleteFile：文件丢失");
        }
    }

    /**
     * 删除文件夹
     *
     * @param path
     * @author fuzhh 2014-2-24
     */
    public static boolean doDelete(String path) {
        File dir = new File(Contants.jecn_path + path);
        if (!dir.exists()) {
            return false;
        }
        File[] subs = dir.listFiles();
        for (File sub : subs) {
            sub.delete();
        }
        return dir.delete();
    }


}
