package com.jecn.epros.server.service.autoCode.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.jfree.util.Log;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.autoCode.AutoCodeNodeData;
import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.dao.autoCode.IAutoCodeDao;
import com.jecn.epros.server.service.autoCode.AutoCodeEnum;
import com.jecn.epros.server.service.autoCode.IStandardizedAutoCodeService;

@Transactional
public class StandardizedAutoCodeServiceImpl implements IStandardizedAutoCodeService {

	private IAutoCodeDao autoCodeDao;

	/**
	 * 标准版-自动编号
	 * 
	 * @param id
	 *            编号的文件ID
	 * @param relatedType
	 *            文件类型： 0:流程、1：文件、2：制度
	 * @throws Exception
	 */
	@Override
	public AutoCodeResultData getStandardizedAutoCode(AutoCodeNodeData codeNodeData) throws Exception {
		Long nodeId = JecnCommonSql.specialAutoCodeNodeHandle(codeNodeData.getNodeId(), codeNodeData.getNodeType());
		codeNodeData.setNodeId(nodeId);
		// 1、获取父节点编号类型
		List<Object[]> parentCodeObjects = getParentCode(codeNodeData.getNodeId(), codeNodeData.getNodeType());

		String errorString = null;
		List<String> parentCodes = new ArrayList<String>();

		if (parentCodeObjects != null) {
			// 2、验证父节点编号是否存在
			for (Object[] objects : parentCodeObjects) {
				if (objects[2] == null || StringUtils.isBlank(objects[2].toString())) {
					errorString += "《" + objects[1].toString() + "》";
				} else {
					parentCodes.add(objects[2].toString());
				}
			}
		}

		AutoCodeResultData codeData = new AutoCodeResultData();
		// 父节点不存在编号
		String error = null;
		if (StringUtils.isNotBlank(errorString)) {
			error = "上级节点" + errorString + "不存在编号，请添加!";
			codeData.setError(error);
			return codeData;
		}

		// 3、获取编号流水号
		Integer codeTotal = getAutoCodeTotal(codeNodeData.getNodeId(), codeNodeData.getNodeType());

		codeData.setCodeTotal(codeTotal);
		if (codeNodeData.getNodeType() == 1) {// 维护支撑文件节点
			codeNodeData.setStandardizedFileCode(standardizedCodeByFileId(codeNodeData.getNodeId()));
		}
		// 4、根据规则生成编号
		AutoCodeEnum.INSTANCE.getAutoCodeBy(parentCodes, codeData, codeNodeData);

		return codeData;
	}

	/**
	 * 编号更新
	 * 
	 * @param relatedId
	 * @param relatedType
	 * @param codeTotal
	 */
	@Override
	public int updateStandardizedAutoCode(Long relatedId, int relatedType, int codeTotal) throws Exception {
		return autoCodeDao.updateStandardizedAutoCode(relatedId, relatedType, codeTotal);
	}

	private List<Object[]> getParentCode(Long nodeId, int nodeType) throws Exception {
		if (isNotSelectParentCode()) {// 不需要验证父节点编号的登录类型
			return null;
		}
		if (nodeType == 0) {
			return autoCodeDao.listNativeSql(getFlowParentCode(nodeId));
		}
		return null;
	}

	/**
	 * 不需要验证父节点编号
	 * 
	 * @return
	 */
	private boolean isNotSelectParentCode() {
		// 株洲时代电气
		return JecnContants.otherLoginType == 23 || JecnContants.otherLoginType == 39;
	}

	private String getFlowParentCode(Long flowId) {
		return "SELECT PARENT.FLOW_ID, PARENT.FLOW_NAME, PARENT.FLOW_ID_INPUT" + "  FROM JECN_FLOW_STRUCTURE_T T"
				+ "  LEFT JOIN JECN_FLOW_STRUCTURE_T PARENT ON " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT")
				+ " WHERE T.FLOW_ID = " + flowId + " ORDER BY PARENT.T_PATH";
	}

	/**
	 * 
	 * 根据关联类型和关联ID获取对应规则下，编号流水号的最大值
	 * 
	 * @param relatedId
	 * @param relatedType
	 * @return 编号的最大流水号
	 */
	@Override
	public Integer getAutoCodeTotal(Long relatedId, int relatedType) throws Exception {
		Integer codeTotal = null;
		String sql = "SELECT T.CODE_TOTAL FROM JECN_STANDARDIZED_AUTO_COD T WHERE T.RELATED_ID = " + relatedId
				+ " AND RELATED_TYPE = " + relatedType;

		List<Integer> listIds = autoCodeDao.listObjectNativeSql(sql, "CODE_TOTAL", Hibernate.INTEGER);
		if (listIds.size() == 1) {
			codeTotal = listIds.get(0);
		}
		if (codeTotal == null) {
			codeTotal = 0;
			// 存储当前编号默认codeTotal = 0;
			sql = "INSERT INTO JECN_STANDARDIZED_AUTO_COD (GUID,RELATED_ID,CODE_TOTAL,RELATED_TYPE,CREATE_TIME) VALUES (?,?,?,?,?)";
			this.autoCodeDao.execteNative(sql, JecnCommon.getUUID(), relatedId, codeTotal, relatedType, new Date());
		}
		return codeTotal;
	}

	public void setAutoCodeDao(IAutoCodeDao autoCodeDao) {
		this.autoCodeDao = autoCodeDao;
	}

	/**
	 * 获取维护支撑文件对应的文件编号
	 * 
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	private String standardizedCodeByFileId(Long fileId) throws Exception {
		String sql = "SELECT CASE WHEN JF.FLOW_ID IS NOT NULL THEN       JFS.FLOW_ID_INPUT"
				+ "         WHEN JF.RULE_ID IS NOT NULL THEN" + "          JR.RULE_NUMBER" + "       END DIR_CODE"
				+ "  FROM JECN_FILE_T JF" + "  LEFT JOIN JECN_FLOW_STRUCTURE_T JFS" + "    ON JF.FLOW_ID = JFS.FLOW_ID"
				+ "  LEFT JOIN JECN_RULE_T JR" + "    ON JR.ID = JF.RULE_ID" + " WHERE JF.IS_DIR = 0"
				+ "   AND JF.FILE_ID = ?";
		return autoCodeDao.getObjectNativeSql(sql, fileId);
	}

	/**
	 * 更新节点编号 暂时只有 流程有重置编号功能
	 * 
	 * @param relatedType
	 * @param relatedId
	 * @param code
	 * @param updatePeopleId
	 * @throws Exception
	 */
	private void updateNumByRelatedType(int relatedType, String spliceFieldStr, String code, Long updatePeopleId)
			throws Exception {
		String sql = "";
		String sqlT = "";
		if (relatedType == 0) {
			sqlT = "UPDATE JECN_FLOW_STRUCTURE_T SET " + spliceFieldStr;
			sql = "UPDATE JECN_FLOW_STRUCTURE SET " + spliceFieldStr;
		} else if (relatedType == 2) {
			sqlT = "UPDATE JECN_RULE_T SET " + spliceFieldStr;
			sql = "UPDATE JECN_RULE SET " + spliceFieldStr;
		} else if (relatedType == 1) {
			sqlT = "UPDATE JECN_FILE_T SET " + spliceFieldStr;
			sql = "UPDATE JECN_FILE SET " + spliceFieldStr;
		}
		if (StringUtils.isBlank(sql)) {
			return;
		}
		if (JecnConfigTool.isAllowedPubReName()) {// 重命名是否直接发布，不直接发布，编号不更新
			this.autoCodeDao.execteNative(sql);
		}
		this.autoCodeDao.execteNative(sqlT);
	}

	/**
	 * 查询要更新的 树节点
	 * 
	 * @param relatedType
	 * @param relatedId
	 * @param code
	 * @param updatePeopleId
	 * @throws Exception
	 */
	private List<Long> findChildrenNodes(int relatedType, Long relatedId) throws Exception {
		String sqlT = "";
		if (relatedType == 0) {
			sqlT = "select flow.flow_id from jecn_flow_structure_t flow where flow.pre_flow_id =? and flow.isflow = 1  and flow.del_state = 0 order by flow.view_sort";
		} else if (relatedType == 2) {
			sqlT = "UPDATE JECN_RULE_T SET RULE_NUMBER = ? WHERE PER_ID = ?";
		} else if (relatedType == 1) {
			sqlT = "UPDATE JECN_FILE_T SET DOC_ID = ? WHERE PER_FILE_ID = ?";
		}
		if (StringUtils.isBlank(sqlT)) {
			return null;
		}
		return autoCodeDao.listNativeSql(sqlT, relatedId);
	}

	/**
	 * 重置编号
	 */
	@Override
	public boolean reNumItemItemAction(AutoCodeNodeData nodeData, long userId) {
		// 3、获取编号流水号
		try {
			Integer codeTotal = getAutoCodeTotal(nodeData.getNodeId(), nodeData.getNodeType());
			AutoCodeResultData codeData = new AutoCodeResultData();
			codeData.setCodeTotal(codeTotal);
			if (nodeData.getNodeType() == 1) {// 维护支撑文件节点
				nodeData.setStandardizedFileCode(standardizedCodeByFileId(nodeData.getNodeId()));
			}
			// 4、根据规则生成编号
			List<String> parentCodes = new ArrayList<String>();
			parentCodes.add(nodeData.getNodeCode());
			List<Long> findNodes = findChildrenNodes(nodeData.getNodeType(), nodeData.getNodeId());// 查询子节点
			if (findNodes == null || findNodes.size() <= 0) {
				return false;
			}
			String spliceField = spliceField(findNodes, parentCodes, codeData, nodeData); // 拼接更新字段
			if (StringUtils.isNotBlank(spliceField)) {
				updateNumByRelatedType(nodeData.getNodeType(), spliceField, codeData.getCode(), userId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.error("StandardizedAutoCodeServiceImpl reNumItemItemAction is error 重置编号异常");
		}

		return true;
	}

	/**
	 * 拼接更新字段
	 * 
	 * @param nodes
	 * @param parentCodes
	 * @param codeData
	 * @param nodeData
	 * @return
	 */
	private String spliceField(List<Long> nodes, List<String> parentCodes, AutoCodeResultData codeData,
			AutoCodeNodeData nodeData) {
		String field = "";
		String codeStr = "";
		if (nodeData.getNodeType() == 0) {
			field = "flow_id";
			codeStr = "flow_id_input";
		} else if (nodeData.getNodeType() == 1) {
			field = "id";
			codeStr = "RULE_NUMBER";
		} else {
			field = "file_id";
			codeStr = "DOC_ID";
		}
		StringBuilder str = new StringBuilder();
		str.append(codeStr + " =  case " + field);
		if (nodes != null && nodes.size() > 0) {
			for (Object object : nodes) {
				AutoCodeEnum.INSTANCE.getAutoCodeBy(parentCodes, codeData, nodeData);
				str.append(" when " + object.toString() + " then '" + codeData.getCode() + "'");
			}
			str.append(" end  where " + field + " in" + JecnCommonSql.getIds(nodes));
		}
		return str.toString();
	}

}
