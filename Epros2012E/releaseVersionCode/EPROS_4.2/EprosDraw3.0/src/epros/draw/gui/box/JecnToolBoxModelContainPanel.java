package epros.draw.gui.box;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 模型面板，存放标题或片段图标的容器
 * 
 * @author ZHOUXY
 * 
 */
class JecnToolBoxModelContainPanel extends JecnToolBoxAbstractContainPanel {
	public JecnToolBoxModelContainPanel(JecnToolBoxMainPanel toolBoxMainPanel) {
		super(toolBoxMainPanel);
		initComponent();
	}

	private void initComponent() {
		// 模型标题名称
		nameLabel.setText(toolBoxMainPanel.getResourceManager().getValue(
				"toolboxModel"));
		// 模型图标按钮显示布局类型：图左内容右/图上内容上/图左内容右上详细内容右下
		typeBtn.setIcon(JecnFileUtil.getToolBoxImageIcon("down"));
		// 大小
		Dimension flowElemModelSize = new Dimension(
				JecnUIUtil.getBoxBtnWidth(), 268);
		btnsPanel.setPreferredSize(flowElemModelSize);
	}

	public void actionPerformed(ActionEvent e) {

		if (toolBoxMainPanel.getCurrShowMapType() == null
				|| toolBoxMainPanel.getCurrShowMapType() == MapType.none) {// 不是流程图/流程地图/组织图
			return;
		}
		if (e.getSource() == typeBtn) {// 点击显示类型
			// 按钮显示类型动作事件
			typeBtnActionPerformed();
		}
	}

	/**
	 * 
	 * 按钮显示类型动作事件
	 * 
	 */
	private void typeBtnActionPerformed() {
		if (coverButtonPopupMenu == null) {
			coverButtonPopupMenu = new JecnCoverButtonPopupMenu(this);
		}
		coverButtonPopupMenu.getPopupMenu().show(typeBtn, 0,
				titlePanel.getHeight());
	}
}
