package com.jecn.epros.server.dao.tree.impl;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.tree.IJecnTreeDao;

public class JecnTreeDaoImpl extends AbsBaseDao<String, String> implements IJecnTreeDao {
	/**
	 * 获取树节点的子节点sort最大值
	 * 
	 * @param treeBean
	 * @return sort+1
	 * @throws Exception
	 */
	public int getTreeChildMaxSortId(JecnTreeBean treeBean) throws Exception {
		if (treeBean == null) {
			return -1;
		}
		String sql = "";
		switch (treeBean.getTreeNodeType()) {
		case process:
		case processMap:
		case processRoot:
			sql = " SELECT MAX(T.SORT_ID) FROM JECN_FLOW_STRUCTURE_T  T WHERE T.PRE_FLOW_ID = ?";
			break;
		case riskDir:
		case riskRoot:
			sql = " SELECT MAX(T.SORT) FROM JECN_RISK  T WHERE T.PARENT_ID = ?";
			break;
		case ruleDir:
		case ruleRoot:
			sql = " SELECT MAX(T.SORT_ID) FROM JECN_RULE_T  T WHERE T.PER_ID = ?";
			break;
		case fileDir:
		case fileRoot:
			sql = " SELECT MAX(T.SORT_ID) FROM JECN_FILE_T  T WHERE T.PER_FILE_ID = ?";
			break;
		case standardDir:
		case standardRoot:
		case standardClause:
			sql = " SELECT MAX(T.SORT_ID) FROM JECN_CRITERION_CLASSES  T WHERE T.PRE_CRITERION_CLASS_ID = ?";
			break;
		case organizationRoot:
		case organization:
			sql = " SELECT MAX(T.SORT_ID) FROM JECN_FLOW_ORG  T WHERE T.PER_ORG_ID = ?";
			break;
		case positionGroup:
			sql = "  SELECT MAX(T.SORT_ID) FROM JECN_POSITION_GROUP T WHERE T.PER_ID = ?";
			break;
		case processCode:
			sql = "  SELECT MAX(PC.SORT_ID) FROM PROCESS_CODE_TREE PC WHERE PC.PARENT_ID=?";
			break;
		default:
			return -1;
		}
		int maxSortId = countAllByParamsNativeSql(sql, treeBean.getId());
		// 返回 新建的sortId
		return maxSortId + 1;
	}
}
