package com.jecn.webservice.common;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import com.jecn.webservice.common.CompanyMenum.CompanyType;

/**
 * 协作框、To跳转符特殊处理方法
 * 
 * @author chehuanbo
 * @date 2014-01-19
 * 
 */
public abstract class SpecialTreatment {

	private static final Logger log = Logger.getLogger(SpecialTreatment.class);
    
    /**公司类型*/
	protected CompanyType companyType=CompanyType.xinghan;
	
	/**
	 * 所有的流程元素集合
	 * 
	 */
	private List<Object[]> flowImageAllList;

	/**
	 * 
	 * 处理所有的流程协作框、To跳转
	 * 
	 * @author chehuanbo
	 * 
	*/
	public void findDottedRectAll() {

		log.info("流程推送特殊处理开始！");
		// 找到所有的流程协作框并且存到协作框临时集合中
		for (int i = 0; i < flowImageAllList.size(); i++) {

			Object[] objDottedRect = flowImageAllList.get(i);
			if (objDottedRect[2] == null) {
				continue;
			}
			if ("DottedRect".equals(objDottedRect[2].toString())) {// DottedRect（协作框）
				dottedRectcl(objDottedRect);
			} else if ("ToFigure".equals(objDottedRect[2].toString())) { // To跳转
				toFigure(objDottedRect);
			}
			if(companyType.equals(CompanyType.bps)){
				if("AndFigure".equals(objDottedRect[2].toString())){
					andHandel(objDottedRect);
				}
			}
		}
		log.info("流程推送特殊处理结束！");
	}

	/**
	 * 协作框特殊处理方法
	 * 
	 * 描述：主要用来查找协作框内的所有活动，保留协作框内的第一个活动，并将连接线连接到第一个活动上
	 * 
	 * @author chehuanbo
	 * 
	 * @param activeList
	 *            协作框内活动临时集合 objDottedRect 协作框
	 * */
	private void dottedRectcl(Object[] objDottedRect) {

		try {
			// 协作框内活动临时集合
			List<Object[]> activeList = new ArrayList<Object[]>();
			MyRectangle rect = new MyRectangle(toInt(objDottedRect[6]),
					toInt(objDottedRect[7]), toInt(objDottedRect[8]),
					toInt(objDottedRect[9]));
			// 根据流程协作框查找协作框内所有的活动
			for (Object[] objActive : flowImageAllList) {
				if (!"DottedRect".equals(objActive[2].toString())) {
					// objActive 代表协作框内的元素（例如：活动）
					boolean flag = rect.contains(toInt(objActive[6]),
							toInt(objActive[7]), toInt(objActive[8]),
							toInt(objActive[9]));
					if (flag) { // true 属于当前协作框
						// 将协作框内的元素添加到临时集合中
						activeList.add(objActive);
					}
				}
			}

			if (activeList.size() > 0) {

				// 查找指向当前元素的连接线
				// objLink 代表当前元素的连接线 从所有元素中查找
				for (int i=0;i<flowImageAllList.size();i++) {
					Object[] objLink =flowImageAllList.get(i);
					if (objLink[2] == null) {
						continue;
					}
					// 不是连接线或者是箭头连接线的时候
					if (!"CommonLine".equals(objLink[2].toString())
							&& !"ManhattanLine".equals(objLink[2].toString())) {
						continue;
					}
					// 连接线的起始元素为当前协作框
					if (Integer.parseInt(objLink[4].toString()) == Integer
							.parseInt(objDottedRect[1].toString())) {
						// 更改当前连接线的起始元素
						Object[] objActiveOne = activeList.get(0);
						// 将连接线的起始连接元素改为协作框内的第一个元素
						objLink[4] = objActiveOne[1];
					} else if (Integer.parseInt(objLink[5].toString()) == Integer
							.parseInt(objDottedRect[1].toString())) { // 连接线的结束元素为当前协作框
						// 更改当前连接线的结束元素
						// 获得协作框内的第一个元素
						Object[] objActiveOne = activeList.get(0);
						// 将连接线的结束连接元素改为协作框内的第一个元素
						objLink[5] = objActiveOne[1];
					} else {
						// 连接线连接在协作框内的活动上
						for (Object[] objActive : activeList) {

							// 连接线的起始元素为当前活动
							if (Integer.parseInt(objLink[4].toString()) == Integer
									.parseInt(objActive[1].toString())) {
									// 更改当前连接线的起始元素
									// 获得协作框内的第一个元素
									Object[] objActiveOne = activeList.get(0);
									// 将连接线的起始连接元素改为协作框内的第一个元素
									objLink[4] = objActiveOne[1];
							} else if (Integer.parseInt(objLink[5].toString()) == Integer
									.parseInt(objActive[1].toString())) { // 连接线的结束元素为当前活动
								// 更改当前连接线的结束元素
								// 获得协作框内的第一个元素
								Object[] objActiveOne = activeList.get(0);
								// 将连接线的结束连接元素改为协作框内的第一个元素
								objLink[5] = objActiveOne[1];
							}
						}
					}
				}

				// 将活动临时集合第一个元素移除，需要用来充当协作框
				activeList.remove(0);
				// 将当前协作框内多余的活动删掉
				for (Object[] objects : activeList) {
					for (int j = 0; j < flowImageAllList.size(); j++) {
						Object[] flowImage = flowImageAllList.get(j);
						if (Integer.parseInt(objects[1].toString()) == Integer
								.parseInt(flowImage[1].toString())) {
							flowImageAllList.remove(flowImage);
						}
					}
				}
			}
			// 将协作框移除
			flowImageAllList.remove(objDottedRect);
		} catch (NumberFormatException e) {
			log.error("执行协作框特殊处理方法出现异常！BpsProSyncServiceImpl dottedRectcl()",
							e);
		}
	}

	/**
	 * 
	 * @Title: toFigure 
	 * @Description: To跳转符特殊处理方法
	 * @param @param objDottedRect
	 * @return void 
	 * @author cheshaowei
	 * @throws
	 * 
	 */
	private void toFigure(Object[] objDottedRect) {
		try {
			if (objDottedRect[3] == null) {
				return;
			}
			String toFigureString = objDottedRect[3].toString();
			// 截取To 对应的活动编号
			if (toFigureString.indexOf("To") != -1) {
				toFigureString = toFigureString.substring(2,
						toFigureString.length()).trim();
			} else {
				toFigureString = toFigureString.substring(0,
						toFigureString.length()).trim();
			}
			for (Object[] objToFigure : flowImageAllList) {
				// 查找To对应的活动
				if (objToFigure[10] != null
						&& objToFigure[10].toString().equals(toFigureString)) {
					for (int g = 0; g < flowImageAllList.size(); g++) {
						Object[] objTolink = flowImageAllList.get(g);
						// 不是连接线或者不是箭头连接线的时候
						if (!"CommonLine".equals(objTolink[2].toString())
								&& !"ManhattanLine".equals(objTolink[2]
										.toString())) {
							continue;
						}
						//查找到TO跳转符对应的活动，并且将连接线连接到TO跳转符对应的活动上
						if (objTolink[5] != null && objDottedRect[1] != null) {
							if (Integer.parseInt(objTolink[5].toString()) == Integer
									.parseInt(objDottedRect[1].toString())) {
//								if(companyType.equals(CompanyType.bps)){ //普元特殊处理
//									if("FlowFigureStart".equals(objToFigure[2].toString())){
//										objTolink[5]=findMinActive()[1];
//									}else{
										objTolink[5] = objToFigure[1];
//									}
//								}
								return;
							}
						}
					}
				}
			}
		} catch (NumberFormatException e) {
			log.error("执行To跳转符特殊处理方法出现异常！BpsProSyncServiceImpl toFigure()", e);
		}
	}
    
	
	/**
	 * 
	 * @Title: findMinActive 
	 * @Description: 查找当前流程图中最小的活动,如果活动编号类型不是Number,不做处理
	 * @param 
	 * @return void 
	 * @author cheshaowei
	 * @throws
	 * 
	 */
	protected Object[] findMinActive(){
		// 查找最小活动编号
		int minValue = Integer.MAX_VALUE;
		Object[] object = null;
		for (Object[] objActive : flowImageAllList) {
			String activeNum =null;
			if(objActive[10] != null){
				activeNum = objActive[10].toString();
			}else{
				continue;
			}
			// 如果活动编号不是number类型
			if (!NumberUtils.isNumber(activeNum)) {
				return null;
			} else {
				if (Integer.parseInt(activeNum) < minValue) {
					minValue = Integer.parseInt(activeNum);
					object = objActive;
				}
			}
		}
		return object;
	}
	
	
	/**
	 * And元素特殊处理
	 * 
	 * @author chehuanbo
	 * @date 2014-01-19
	 * 
	 * **/
	public void andHandel(Object[] objAnd) {

		if (objAnd == null||objAnd[1]==null) {
			return;
		}

		String toStart = null;
		// 查找结束元素为当前元素的连接线，并将当前连接线移除
		for(int i = 0; i < flowImageAllList.size(); i++){
			Object[] obj = flowImageAllList.get(i);
			// 首先查找结束元素为当前and元素的连接线
			if (toInt(obj[5]) == toInt(objAnd[1])) {
				if (obj[4] != null) {
					toStart = obj[4].toString();
				}
				flowImageAllList.remove(obj);
				break;
			}
		}

		// 更改连接线的其实元素
		for (int i = 0; i < flowImageAllList.size(); i++) {
			Object[] object = flowImageAllList.get(i);
			if (object != null&&object[5]!=null) {
				// 修改连接线的结束元素为当前And符的
				if (toInt(object[4]) == toInt(objAnd[1])) {
					object[4] = toStart;
				}
			}
		}
	}

	/**
	 * 查找与返入或者返出最近的元素
	 * 
	 * @param lineAngle
	 *            旋转角度 figureList 流程元素
	 * 
	 **/
	public Object[] rArrowhead(Object[] RightOrRev, double lineAngle,
			List<Object[]> figureList) {

		int fx = toInt(RightOrRev[6]);
		int fy = toInt(RightOrRev[7]);

		int lineW = toInt(RightOrRev[8]);
		int lineH = toInt(RightOrRev[9]);

		// 返出小线段坐标
		Point linePoint = null;
		if (lineAngle == 90.0) {
			linePoint = new Point(lineW + fx, lineH / 2 + fy);
		} else if (lineAngle == 180.0) {
			linePoint = new Point(lineW / 2 + fx, lineH + fy);
		} else if (lineAngle == 270.0) {
			linePoint = new Point(fx, lineH / 2 + fy);
		} else {// 0.0 360.0
			linePoint = new Point(lineW / 2 + fx, fy);
		}

		// 比较过的最小距离值
		int lastValue = Integer.MAX_VALUE;
		// 比较过的最小距离对应图形数据
		Object[] lastArray = null;
		// 当前图形对象
		Rectangle rect = new Rectangle();

		for (Object[] objArray : figureList) {
			// 不是连接线或者是箭头连接线的时候
			if ("CommonLine".equals(objArray[2].toString())
					|| "ManhattanLine".equals(objArray[2].toString())
					|| "RightArrowhead".equals(objArray[2].toString())
					|| "ReverseArrowhead".equals(objArray[2].toString())) {
				continue;
			}
			// 修改属性
			rect.setBounds(toInt(objArray[6]), toInt(objArray[7]),
					toInt(objArray[8]), toInt(objArray[9]));
			if (rect.contains(linePoint)) {// 线段点在图形内时，返回此图形数据 1
				return objArray;
			}

			// 返出X,Y坐标
			int lineX = linePoint.x;
			int lineY = linePoint.y;

			// 图形原点坐标以及其对角线对应点坐标
			int rectX = rect.x;
			int rectY = rect.y;
			int rectW = rect.x + rect.width;
			int rectH = rect.y + rect.height;

			int value = Integer.MAX_VALUE;
			// 开始查找最小值
			if (lineY < rectY || lineY > rectH) {// 返出在图形南北面 2
				if (lineX < rectX || lineX > rectW) {// 四个对角面
					int tmpX = Math.abs(lineX - rectX);
					int tmpY = Math.abs(lineY - rectY);
					if (tmpX <= tmpY) {
						value = tmpX;
					} else {
						value = tmpY;
					}
				} else {// 返出在图形南北面方情况 3
					value = Math.abs(lineY - rectY);
				}
			} else { // 返出在图形东西面上方情况 4
				value = Math.abs(lineX - rectX);
			}
			if (lastValue > value) {// 上一次最小距离大于当前最小距离，替换成当前对象
				lastValue = value;
				lastArray = objArray;
			}
		}
		return lastArray;
	}

	class MyRectangle {
		private int x;
		private int y;
		private int width;
		private int height;

		MyRectangle(int X, int Y, int W, int H) {
			this.x = X;
			this.y = Y;
			this.width = W;
			this.height = H;
		}

		boolean contains(int X, int Y, int W, int H) {
			int w = this.width;
			int h = this.height;
			if ((w | h | W | H) < 0) {
				return false;
			}
			int x = this.x;
			int y = this.y;
			if (X < x || Y < y) {
				return false;
			}

			w += x;
			W += X;
			if (W <= X) {
				if (w >= x || W > w)
					return false;
			} else {
				if (w >= x && W > w)
					return false;
			}
			h += y;
			H += Y;
			if (H <= Y) {
				if (h >= y || H > h)
					return false;
			} else {
				if (h >= y && H > h)
					return false;
			}
			return true;
		}
	}

	private int toInt(Object obj) {
		if (obj == null) {
			return -1;
		}
		return Integer.valueOf(String.valueOf(obj));
	}

	public List<Object[]> getFlowImageAllList() {
		return flowImageAllList;
	}

	public void setFlowImageAllList(List<Object[]> flowImageAllList) {
		this.flowImageAllList = flowImageAllList;
	}

	

}
