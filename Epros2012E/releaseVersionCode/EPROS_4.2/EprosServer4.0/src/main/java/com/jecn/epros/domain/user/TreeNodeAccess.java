package com.jecn.epros.domain.user;

import com.jecn.epros.domain.process.TreeNode;

public class TreeNodeAccess extends TreeNode {
	/**
	 * 0是不存在，1是存在
	 */
	private int existType;
	
	/**节点唯一表示*/
	private String code;
	
	/**父节点唯一表示*/
	private String pId;
	
	private int sortId;

	public int getSortId() {
		return sortId;
	}

	public void setSortId(int sortId) {
		this.sortId = sortId;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public int getExistType() {
		return existType;
	}

	public void setExistType(int existType) {
		this.existType = existType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
