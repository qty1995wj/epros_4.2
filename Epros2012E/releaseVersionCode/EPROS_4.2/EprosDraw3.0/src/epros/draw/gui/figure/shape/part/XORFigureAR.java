package epros.draw.gui.figure.shape.part;

import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseCircle;

public class XORFigureAR extends BaseCircle {

	public XORFigureAR(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintCircleTop(Graphics2D g2d, int shadowCount) {
		// 顶层 原图，榜值均为零
		g2d.fillOval(x, y, userWidth - shadowCount, userHeight - shadowCount);
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawOval(x, y, userWidth - shadowCount, userHeight - shadowCount);
		drawLine(g2d);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		super.paintNoneFigure(g2d);
		drawLine(g2d);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	protected void paint3DPartTop(Graphics2D g2d) {
		g2d.fillOval(x + 4, y + 4, userWidth - 4 - 4, userHeight - 4 - 4);

		g2d.setColor(this.getFlowElementData().getBodyColor());
		drawLine(g2d);
	}

	private void drawLine(Graphics2D g2d) {
		g2d.drawLine(this.getWidth() / 4, this.getHeight() / 4, this.getWidth() * 3 / 4, this.getHeight() * 3 / 4);
		g2d.drawLine(this.getWidth() * 3 / 4, this.getHeight() / 4, this.getWidth() / 4, this.getHeight() * 3 / 4);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	protected void paint3DAndShadowsPartTop(Graphics2D g2d) {
		g2d.fillOval(x + 4, y + 4, userWidth - 4 - 4, userHeight - 4 - 4);
		g2d.setColor(this.getFlowElementData().getBodyColor());
		drawLine(g2d);
	}
}
