package com.jecn.epros.server.download.wordxml.mulinsen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;

import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;

/**
 * 木林森
 * 
 * @author jecn
 * 
 */
public class MLSProcessModel extends ProcessFileModel {
	/**
	 * 0name 1posname 2orgname
	 */
	private final Map<Long, List<String[]>> approves = new HashMap<Long, List<String[]>>();

	public MLSProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(16.24F);
		getDocProperty().setNewRowHeight(0.82F);
		getDocProperty().setFlowChartScaleValue(6F);
		getDocProperty().setTblTitleShadow(true);
		getDocProperty().setFlowChartDirection(true);
		setDocStyle("、", textTitleStyle());
		setDefAscii("黑体");
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		Header header = firstSect.createHeader(HeaderOrFooterType.first);
		createTitleHeader(header);
		Header header2 = firstSect.createHeader(HeaderOrFooterType.even);
		createCommonHeader(header2);
		Header header3 = firstSect.createHeader(HeaderOrFooterType.odd);
		createCommonHeader(header3);

		createCommonFooter(firstSect);
	}

	private void createCommonhf(Sect sect) {
		createCommonFooter(sect);
		createCommonHeader(sect);
	}

	private void createCommonHeader(Sect sect) {
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		createCommonHeader(header);
	}

	private void createCommonFooter(Sect sect) {
		Footer footer = sect.createFooter(HeaderOrFooterType.first);
		createFooter(footer);

		footer = sect.createFooter(HeaderOrFooterType.odd);
		createFooter(footer);

		footer = sect.createFooter(HeaderOrFooterType.even);
		createFooter(footer);
	}

	private void createFooter(Footer footer) {
		Paragraph p;
		p = footer.createParagraph("", new ParagraphBean(Align.center, "Times New Roman", Constants.xiaowu, false));
		p.appendCurPageRun();
		p.appendTextRun("/");
		p.appendTotalPagesRun();
	}

	private void createCommonHeader(Header header) {
		Image logoImage = (Image) super.getLogoImage(4.23F, 0.9F);
		logoImage.setPositionBean(new PositionBean(PositionType.absolute, 1F, 0F, 1F, 0F));
		Paragraph paragraph = header.addParagraph(new Paragraph(logoImage, new ParagraphBean(Align.right, "宋体",
				Constants.shiyihao, false)));
		// paragraph.appendTextRun(processDownloadBean.getFlowName());
		// header.addParagraph(new Paragraph("", new
		// ParagraphBean(PStyle.AF1)));
		ParagraphBean pBean = new ParagraphBean(Align.right, "宋体", Constants.shiyihao, false);
		pBean.setpStyle(PStyle.AF1);
		Paragraph nameP = new Paragraph(processDownloadBean.getFlowName(), pBean);
		header.addParagraph(nameP);
	}

	private void createTitleHeader(Header header) {
		Table table = header.createTable(tblStyle(), new float[] { 6.32F, 2.71F, 3.92F, 2.41F, 2.78F });
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		table.createTableRow(new String[] { "", "文件编号", nullToEmpty(processDownloadBean.getFlowInputNum()) }, pBean);
		table.createTableRow(new String[] { "", "当前版本", nullToEmpty(processDownloadBean.getVersion()), "页    数", "" },
				pBean);

		table.createTableRow(
				new String[] { "", "文件类型", "流程说明文件", "更新日期", processDownloadBean.getPubDate("yyyy-MM-dd") }, pBean);

		table.createTableRow(new String[] { "文件名称", "流程责任人", nullToEmpty(getDutyPos()), "密    级", getSecretLevel() },
				pBean);
		table.createTableRow(new String[] { processDownloadBean.getFlowName(), "适用范围",
				nullToEmpty(processDownloadBean.getApplicability()) }, pBean);

		TableCell logoCell = table.getRow(0).getCell(0);
		Image logoImage = (Image) super.getLogoImage(4.74F, 1.01F);
		logoImage.setPositionBean(new PositionBean(PositionType.absolute, 2F, 0F, 2F, 0F));
		logoCell.addParagraph(new Paragraph(logoImage));

		table.getRow(0).getCell(2).setColspan(3);
		table.getRow(4).getCell(2).setColspan(3);

		table.getRow(0).getCell(0).setVmergeBeg(true);
		table.getRow(1).getCell(0).setVmerge(true);
		table.getRow(2).getCell(0).setVmerge(true);

		table.getRow(4).getCell(0).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.center, "宋体", Constants.sihao, true));

		table.getRow(1).getCell(4).getParagraph(0).appendTotalPagesRun();
		table.setCellValignAndHeight(Valign.center, 0.7F);

		// table.setColBackGround(1, 3);

		header.addParagraph(new Paragraph("", new ParagraphBean(PStyle.AF1)));

	}

	private String getDutyPos() {
		String result = null;
		int dutyType = processDownloadBean.getFlowAttr().getDutyUserType();
		if (dutyType == 1) {
			result = processDownloadBean.getFlowAttr().getDutyUserName();
		} else {
			result = processDownloadBean.getFlowAttr().getDutuUserPosName();
		}
		return JecnUtil.nullToEmpty(result);
	}

	private String getSecretLevel() {
		Integer level = processDownloadBean.getFlowStructure() != null ? processDownloadBean.getFlowStructure()
				.getConfidentialityLevel() : processDownloadBean.getFlowStructureT().getConfidentialityLevel();
		if (level == null) {
			return "";
		}
		JecnDictionary dictinary = JecnConfigTool.getDictinary(DictionaryEnum.JECN_SECURITY_LEVEL, level);
		if (dictinary == null) {
			return "";
		}
		return JecnUtil.nullToEmpty(dictinary.getValue());
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getCharSectBean();
		flowChartSect.setSectBean(sectBean);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		createCommonhf(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getSectBean();
		titleSect.setSectBean(sectBean);
	}

	/**
	 * TODO
	 */
	protected void afterHandle() {

		Sect sect = docProperty.getSecondSect();
		sect.breakPage();
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaosi, false);
		Table table = sect.createTable(tblStyle(), new float[] { 14.92F });
		table.createTableRow(new String[] { "文控管理" }, new ParagraphBean(Align.center, "宋体", Constants.xiaosi, true));
		table.setRowStyle(0, new ParagraphBean(Align.center, "宋体", Constants.xiaosi, true), true, true);
		table.setValign(Valign.center);

		table = sect.createTable(tblStyle(), new float[] { 2.69F, 3F, 4.17F, 1.49F, 3.56F });
		JecnTaskHistoryNew latestHistoryNew = processDownloadBean.getLatestHistoryNew();
		if (latestHistoryNew != null) {
			List<JecnTaskHistoryFollow> fllows = latestHistoryNew.getListJecnTaskHistoryFollow();
			Set<Long> draftPeopleIds = new HashSet<Long>();
			Set<Long> reviewPeopleIds = new HashSet<Long>();
			Set<Long> approveIds = new HashSet<Long>();
			// 审核阶段
			String reviewPeople = "";
			// 批准人
			String approvalPeople = "";
			String authorPeople = latestHistoryNew == null ? "" : nullToEmpty(latestHistoryNew.getDraftPerson());
			for (JecnTaskHistoryFollow p : fllows) {
				Integer state = p.getStageMark();
				if (state == null) {
					continue;
				}
				if (state == 14) {
					p.getId();
					reviewPeopleIds = getIds(p.getApproveId());
					reviewPeople = p.getName();
				} else if (state == 4) {
					approveIds = getIds(p.getApproveId());
					approvalPeople = p.getName();
				} else if (state == 0) {
					draftPeopleIds = getIds(p.getApproveId());
					authorPeople = p.getName();
				}
			}

			Set<Long> ids = new HashSet<Long>();
			ids.addAll(draftPeopleIds);
			ids.addAll(reviewPeopleIds);
			ids.addAll(approveIds);
			// 搞这么复杂只是为了少查几遍
			initApproveOrgAndPos(ids);

			if (draftPeopleIds.size() > 0) {
				for (Long id : draftPeopleIds) {
					table.createTableRow(new String[] { "", "编制", getPeopleNames(id), "岗位", getPosNames(id) }, pBean);
					break;
				}
			} else {
				table.createTableRow(new String[] { "", "编制", authorPeople, "岗位", "" }, pBean);
			}

			if (reviewPeopleIds.size() > 0) {
				for (Long id : reviewPeopleIds) {
					table.createTableRow(new String[] { "", "审核", getPeopleNames(id), "岗位", getPosNames(id) }, pBean);
				}
			} else {
				table.createTableRow(new String[] { "", "审核", reviewPeople, "岗位", "" }, pBean);
			}

			if (approveIds.size() > 0) {
				for (Long id : approveIds) {
					table.createTableRow(new String[] { "", "批准", getPeopleNames(id), "岗位", getPosNames(id) }, pBean);
					break;
				}
			} else {
				table.createTableRow(new String[] { "", "批准", approvalPeople, "岗位", "" }, pBean);
			}

			int reviewCount = reviewPeopleIds.size();
			if (reviewCount == 0) {
				reviewCount = 1;
			}
			int approvePeopleLineCount = reviewCount + 2;
			TableCell cell = table.getRow(0).getCell(0);
			cell.setVmergeBeg(true);
			cell.setRowspan(approvePeopleLineCount);
			cell.addParagraph(new Paragraph("审批控制", pBean));
		}
		table.setValign(Valign.center);

		table = sect.createTable(tblStyle(), new float[] { 2.69F, 3F, 9.22F });
		table.createTableRow(new String[] { "发布管理", "生效日期 ", processDownloadBean.getPubDate("yyyy-MM-dd") }, pBean);
		table
				.createTableRow(new String[] { "", "解释岗位", processDownloadBean.getFlowAttr().getDutuUserPosName() },
						pBean);
		table.getRow(0).getCell(0).setRowspan(2);
		table.setValign(Valign.center);

		sect.createMutilEmptyParagraph(3);

		table = sect.createTable(tblStyle(), new float[] { 1.69F, 1.75F, 6.75F, 2F, 2.73F });
		table.createTableRow(new String[] { "文件修订履历" }, new ParagraphBean(Align.center, "宋体", Constants.xiaosi, true));
		table.createTableRow(new String[] { "序号", "版本", "修订内容", "修订人", "修订日期" }, new ParagraphBean(Align.center, "宋体",
				Constants.xiaosi, true));
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		int index = 1;
		if (historys != null && historys.size() > 0) {
			for (JecnTaskHistoryNew history : historys) {
				table.createTableRow(new String[] { String.valueOf(index++), history.getVersionId(),
						history.getModifyExplain(), history.getDraftPerson(),
						JecnUtil.DateToStr(history.getPublishDate(), "yyyy-MM-dd") }, pBean);
			}
		}
		table.setValign(Valign.center);
		// 置灰
		table.getRow(0).getCell(0).setColspan(5);
		table.setRowStyle(0, pBean, true, true);
	}

	private String getPosNames(Long id) {
		List<String[]> ls = approves.get(id);
		StringBuffer buf = new StringBuffer();
		for (String[] arr : ls) {
			buf.append(JecnUtil.nullToEmpty(arr[1]));
			buf.append("/");
		}
		String result = buf.toString();
		result = result.replaceAll("/+", "/");
		if (result.endsWith("/")) {
			result = result.substring(0, result.length() - 1);
		}
		return result;
	}

	private String getPeopleNames(Long id) {
		return approves.get(id).get(0)[0];
	}

	private Set<Long> getIds(String idStr) {
		Set<Long> ids = new HashSet<Long>();

		if (StringUtils.isNotBlank(idStr)) {
			String[] idArray = idStr.split(",");
			for (String id : idArray) {
				if (StringUtils.isNotBlank(id)) {
					ids.add(Long.valueOf(id));
				}
			}
		}
		return ids;

	}

	private void initApproveOrgAndPos(Set<Long> idSet) {
		if (idSet == null || idSet.size() == 0) {
			return;
		}
		String sql = "	select ju.people_id,ju.TRUE_NAME,jfoi.FIGURE_TEXT,jfo.ORG_NAME from JECN_USER ju"
				+ "	left join JECN_USER_POSITION_RELATED jupr on ju.people_id=jupr.people_id"
				+ "	left join JECN_FLOW_ORG_IMAGE jfoi on jfoi.figure_id=jupr.figure_id"
				+ "	left join JECN_FLOW_ORG jfo on jfoi.ORG_ID=jfo.ORG_ID" + "  where ju.people_id in"
				+ JecnCommonSql.getIdsSet(idSet);
		List<Object[]> objs = processDownloadBean.getBaseDao().listNativeSql(sql);
		if (objs.size() > 0) {
			for (Object[] obj : objs) {
				putOrElseUpdate(Long.valueOf(obj[0].toString()), new String[] { JecnUtil.nullToEmpty(obj[1]),
						JecnUtil.nullToEmpty((obj[2])), JecnUtil.nullToEmpty(obj[3]) });
			}
		}
	}

	private void putOrElseUpdate(Long peopleId, String[] info) {
		List<String[]> list = approves.get(peopleId);
		if (list == null) {
			approves.put(peopleId, new ArrayList<String[]>());
		}
		approves.get(peopleId).add(info);
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, new ParagraphLineRule(20F, LineRuleType.EXACT));
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	@Override
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {
		swap(titles, 1, 2);
		swap(rowData, 1, 2);
		swap(titles, 2, 3);
		swap(rowData, 2, 3);
		// 切换顺序
		super.a10(processFileItem, titles, rowData);
	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		word.paragraphStyleKpi(processFileItem, oldRowData);
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	@Override
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		float[] width = null;
		if (JecnContants.showPosNameBox) {
			width = new float[] { 3.5F, 3.5F, 7.5F };
		} else if (!JecnContants.showPosNameBox) {
			width = new float[] { 5.0F, 9.5F };
		}
		List<String[]> rowData2 = new ArrayList<String[]>();
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = "对应职位/岗位";
		// 角色职责
		String roleResponsibility = "职责";
		if (JecnContants.showPosNameBox) {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility, positionName });
		} else {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility });
		}
		for (String[] str : rowData) {
			if (JecnContants.showPosNameBox) {
				rowData2.add(new String[] { str[0], str[2], str[1] });
			} else {
				rowData2.add(new String[] { str[0], str[2] });
			}
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), getDocProperty()
				.isTblTitleCrossPageBreaks());
	}

	private void swap(List<String[]> rowData, int i, int j) {
		for (String[] arr : rowData) {
			swap(arr, i, j);
		}
	}

	private void swap(String[] arr, int i, int j) {
		String temp = "";
		temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	/**
	 * 关键活动
	 * 
	 * @param _count
	 * @param name
	 * @param keyActivityShowList
	 */
	protected void a07(ProcessFileItem processFileItem, List<KeyActivityBean> activeList) {
		if (activeList == null || activeList.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		List<String[]> dataList = new ArrayList<String[]>();
		// 标题行
		dataList.add(new String[] { "活动类型", "活动名称", "具体内容" });

		List<KeyActivityBean> pas = new ArrayList<KeyActivityBean>();
		List<KeyActivityBean> ksfs = new ArrayList<KeyActivityBean>();
		List<KeyActivityBean> kcps = new ArrayList<KeyActivityBean>();
		List<KeyActivityBean> kcpAllows = new ArrayList<KeyActivityBean>();
		boolean kcpAllowShow = kcpAllowShow();
		if (activeList != null) {
			for (KeyActivityBean keyActive : activeList) {
				if ("1".equals(keyActive.getActiveKey())) {
					pas.add(keyActive);
				} else if ("2".equals(keyActive.getActiveKey())) {
					ksfs.add(keyActive);
				} else if ("3".equals(keyActive.getActiveKey())) {
					kcps.add(keyActive);
				} else if ("4".equals(keyActive.getActiveKey())) {
					kcpAllows.add(keyActive);
				}
			}
			fileData(dataList, pas, "PA（问题区域）");
			fileData(dataList, ksfs, "KSF（关键成功因素）");
			fileData(dataList, kcps, "KCP（核心控制点）");
			if (kcpAllowShow) {
				fileData(dataList, kcpAllows, "KCP（核心控制点，合规）");
			}
		}
		float[] width = new float[] { 4.11F, 2.5F, 9.74F };
		Table tbl = createTableItem(processFileItem, width, dataList);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
		tbl.setCellStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow());

		int paIndex = 1;
		int ksfIndex = paIndex + pas.size();
		int kcpIndex = ksfIndex + ksfs.size();
		int kcpAllowIndex = kcpIndex + kcps.size();

		int maxIndex = tbl.getRowCount() - 1;

		// 合并单元格
		if (maxIndex >= paIndex) {
			tbl.getRow(paIndex).getCell(0).setRowspan(pas.size());
		}
		if (maxIndex >= ksfIndex) {
			tbl.getRow(ksfIndex).getCell(0).setRowspan(ksfs.size());
		}
		if (maxIndex >= kcpIndex) {
			tbl.getRow(kcpIndex).getCell(0).setRowspan(kcps.size());
		}
		if (kcpAllowShow && maxIndex >= kcpAllowIndex) {
			tbl.getRow(kcpAllowIndex).getCell(0).setRowspan(kcpAllows.size());
		}
	}

	private void fileData(List<String[]> dataList, List<KeyActivityBean> pas, String name) {
		int index = 0;
		for (KeyActivityBean a : pas) {
			String[] rowData = new String[3];
			if (index == 0) {
				rowData[0] = name;
			} else {
				rowData[0] = "";
			}
			rowData[1] = JecnUtil.nullToEmpty(a.getActivityName());
			rowData[2] = JecnUtil.nullToEmpty(a.getActivityShowControl());
			dataList.add(rowData);
			index++;
		}
	}

	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> dataRow) {
		Table tab = super.createTableItem(processFileItem, width, dataRow);
		tab.setValign(Valign.center);
		return tab;
	}

	/****
	 * 输入和输出
	 * 
	 * @param titleName
	 * @param inputorout
	 */
	protected void a32(ProcessFileItem processFileItem, String[] input, String[] output) {
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "输入", processDownloadBean.getFlowInput() });
		rowData.add(new String[] { "输出", processDownloadBean.getFlowOutput() });
		float[] width = new float[] { 2.71F, 13.53F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setCellStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow());
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 15.6F));
		// 设置页边距
		sectBean.setPage(3.02F, 1F, 1.27F, 1F, 1.25F, 1F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getCharSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 15.6F));
		// 设置页边距
		sectBean.setPage(3.02F, 1F, 1.27F, 1F, 1.25F, 1F);
		sectBean.setSize(29.7F, 21F);
		return sectBean;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, new FontBean("微软雅黑", 18, false));
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		/** 默认表格样式 ****/
		TableBean tblStyle = new TableBean();// 
		// 所有边框 1.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.01 厘米
		// tblStyle.setTableLeftInd(0.01F);
		tblStyle.setTableAlign(Align.center);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, new FontBean("微软雅黑", 18, false));
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.shiyihao, false));
		// 2字符转厘米 0.4240284
		textContentStyle.setInd(0, 0, 0, 0.8480568f);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(1.5F, LineRuleType.AUTO));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.shiyihao, true));
		textTitleStyle.setInd(0F, 0, 0F, 0.4F); // 段落缩进 厘米
		textTitleStyle.setSpace(0F, 0F, new ParagraphLineRule(1.5F, LineRuleType.AUTO)); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

}
