package com.jecn.epros.server.download.excel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.process.ProcessSystemListBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 流程地图 流程清单Excel导出
 * 
 * @author ZHANGXH
 */
public class ProcessMapTemplateDownload {
	private static boolean isGuardShow;
	private static final Logger log = Logger
			.getLogger(ProcessMapTemplateDownload.class);

	/**
	 * 流程清单
	 * 
	 * @param processSystemListBean
	 * @return
	 */
	public static byte[] createExcelile(
			ProcessSystemListBean processSystemListBean, int depth)
			throws Exception {
		WritableWorkbook wbookData = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path
					+ JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			File file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);
			// Sheet1名字 流程清单
			WritableSheet wsheet = wbookData.createSheet(JecnUtil
					.getValue("processList"), 0);
			// 定义标题颜色
			wbookData.setColourRGB(Colour.YELLOW, 255, 255, 0);
			wbookData.setColourRGB(Colour.GREEN, 0, 128, 0);
			wbookData.setColourRGB(Colour.RED, 255, 0, 0);
			wbookData.setColourRGB(Colour.BROWN, 255, 153, 0);

			// 表样式
			WritableCellFormat wformat = new WritableCellFormat();
			wformat.setBorder(Border.ALL, BorderLineStyle.THIN);
			// 标题样式
			WritableCellFormat yellowTitle = getCellFormat(true, true,
					Colour.BLACK);
			yellowTitle.setBackground(Colour.YELLOW);

			WritableCellFormat brownTitle = getCellFormat(true, true,
					Colour.BLACK);
			brownTitle.setBackground(Colour.BROWN);

			wsheet.setRowView(0, 440);
			wsheet.setRowView(1, 440);
			// 获取总的级别个数
			int levelTotal = processSystemListBean.getLevelTotal();
			// 添加计数列
			wsheet.addCell(new Label(0, 1, "", yellowTitle));
			wsheet.setColumnView(0, 3);
			// 级别标题设置
			int count = 1;
			for (int i = 0; i < levelTotal; i++) {// 创建级别标题
				if (i >= depth) {
					// 级名称
					wsheet.addCell(new Label(count, 1, i
							+ JecnUtil.getValue("levelName"), yellowTitle));
					wsheet.setColumnView(count, 17);
					count++;
					// 级编号
					wsheet.addCell(new Label(count, 1, i
							+ JecnUtil.getValue("levelNumber"), yellowTitle));
					wsheet.setColumnView(count, 17);
					count++;
				}
			}
			if ("1".equals(JecnContants.getValue("guardianPeople"))) {
				isGuardShow = true;
			}

			// 创建除级别外的标题 流程名称
			wsheet.addCell(new Label(count, 1,
					JecnUtil.getValue("processName"), brownTitle));
			wsheet.setColumnView(count, 24);
			// 流程编号
			int num = count + 1;
			wsheet.addCell(new Label(num, 1,
					JecnUtil.getValue("processNumber"), brownTitle));
			wsheet.setColumnView(num, 24);
			// 发布状态
			num = num + 1;
			wsheet.addCell(new Label(num, 1, JecnUtil.getValue("pubState"),
					yellowTitle));
			wsheet.setColumnView(num, 12);
			// 责任部门
			num = num + 1;
			wsheet.addCell(new Label(num, 1, JecnUtil
					.getValue("responsibilityDepartment"), yellowTitle));
			wsheet.setColumnView(num, 24);
			// 流程责任人
			num = num + 1;
			wsheet.addCell(new Label(num, 1, JecnUtil
					.getValue("processResponsiblePersons"), yellowTitle));
			wsheet.setColumnView(num, 13);
			if (isGuardShow) {
				// 流程监护人
				num = num + 1;
				wsheet.addCell(new Label(num, 1, JecnUtil
						.getValue("processTheGuardian"), yellowTitle));
				wsheet.setColumnView(num, 13);
			}
			// 拟稿人
			num = num + 1;
			wsheet.addCell(new Label(num, 1, JecnUtil
					.getValue("peopleMakeADraft"), yellowTitle));
			wsheet.setColumnView(num, 12);
			// 版本号
			num = num + 1;
			wsheet.addCell(new Label(num, 1,
					JecnUtil.getValue("versionNumber"), yellowTitle));
			wsheet.setColumnView(num, 12);
			// 发布日期
			num = num + 1;
			wsheet.addCell(new Label(num, 1, JecnUtil.getValue("releaseDate"),
					yellowTitle));
			wsheet.setColumnView(num, 16);
			// 有效日期
			num = num + 1;
			wsheet.addCell(new Label(num, 1, JecnUtil.getValue("validity"),
					yellowTitle));
			wsheet.setColumnView(num, 12);
			// 是否及时优化
			num = num + 1;
			wsheet.addCell(new Label(num, 1, JecnUtil
					.getValue("theTimelyOptimization"), yellowTitle));
			wsheet.setColumnView(num, 15);
			// 下次审视需完成时间
			num = num + 1;
			wsheet.addCell(new Label(num, 1, JecnUtil
					.getValue("nextViewNeedFinishTime"), yellowTitle));
			wsheet.setColumnView(num, 22);
			// 添加数据
			addSheetData(processSystemListBean, wsheet, depth);
			// 写入Exel工作表
			wbookData.write();
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			// 把一个文件转化为字节
			byte[] tempDate = JecnUtil.getByte(file);
			if (file.exists()) {
				file.delete();
			}
			return tempDate;
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("关闭WritableWorkbook异常", e);
				}
			}
		}

		return null;
	}

	/**
	 * 获取级别信息
	 * 
	 * @author weidp
	 * @date 2014-10-30 下午01:38:15
	 * @param processWebBean
	 * @param processMapList
	 * @param infoList
	 *            存储级别信息
	 */
	public static void getFlowLevelInfo(ProcessWebBean processWebBean,
			List<ProcessWebBean> processMapList, List<Object[]> infoList) {
		// 获取父Id
		long pId = processWebBean.getParentFlowId();
		for (ProcessWebBean processMap : processMapList) {
			if (processMap.getFlowId() == pId) {
				// 父节点为流程图的情况下继续找,知道找到所属流程地图
				if (processMap.getIsflow() != 0) {
					getFlowLevelInfo(processMap, processMapList, infoList);
				} else {
					// 存储找到的流程地图信息（级别，流程地图名称，流程地图编号）
					infoList.add(new Object[] { processMap.getLevel(),
							processMap.getFlowName(),
							processMap.getFlowIdInput() });
					// 继续递归
					getFlowLevelInfo(processMap, processMapList, infoList);
				}
				break;
			}
		}

	}

	/**
	 * 添加到处数据
	 * 
	 * @param processSystemListBean
	 * @param wsheet
	 * @param wformatContent
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private static void addSheetData(
			ProcessSystemListBean processSystemListBean, WritableSheet wsheet,
			int depth) throws RowsExceededException, WriteException {

		/*** 模板单元格样式 start *******/
		// 正文字体
		WritableCellFormat normalCell = getCellFormat(false, true, Colour.BLACK);
		// 红色正文
		WritableCellFormat redCell = getCellFormat(false, true, Colour.RED);
		// 绿色正文
		WritableCellFormat greenCell = getCellFormat(false, true, Colour.GREEN);
		// 褐色正文
		WritableCellFormat brownCell = getCellFormat(false, true, Colour.BROWN);
		// 总计
		WritableCellFormat countCell = getCellFormat(false, false, Colour.BLACK);
		// 总计 未发布
		WritableCellFormat countRedCell = getCellFormat(false, false,
				Colour.RED);
		// 总计 已发布
		WritableCellFormat countGreenCell = getCellFormat(false, false,
				Colour.GREEN);
		// 总计 未审批
		WritableCellFormat countBrownCell = getCellFormat(false, false,
				Colour.BROWN);
		/*** 模板单元格样式 end *******/

		// 获取流程统计数据集合
		List<ProcessWebBean> listProcessWebBean = processSystemListBean
				.getListProcessWebBean();
		// 流程级别总数
		int totalLevel = processSystemListBean.getLevelTotal();
		ProcessWebBean processWebBean = null;
		// 内容行
		int rowNumber = 2;
		for (int i = 0; i < listProcessWebBean.size(); i++) {
			processWebBean = listProcessWebBean.get(i);
			// 跳过流程地图
			if (processWebBean.getIsflow() == 0) {
				continue;
			}
			// 初始化行高 和第一列的值（序列）
			wsheet.setRowView(rowNumber, 360);
			wsheet.addCell(new Label(0, rowNumber, rowNumber - 1 + "",
					normalCell));

			// 递归得到流程所属的流程地图信息
			List<Object[]> processMapInfo = new ArrayList<Object[]>();
			getFlowLevelInfo(processWebBean, processSystemListBean
					.getListProcessWebBean(), processMapInfo);
			// 获取最小级别
			int minLevel = Integer.valueOf(processMapInfo.get(processMapInfo
					.size() - 1)[0].toString());
			// 追加级别信息至JSON
			for (int index = 0; index < processMapInfo.size()
					|| index < totalLevel - depth; index++) {
				// 没有的级别赋空值
				if (index >= processMapInfo.size()) {
					wsheet.addCell(new Label(index * 2 + 1, rowNumber, "",
							normalCell));
					wsheet.addCell(new Label(index * 2 + 2, rowNumber, "",
							normalCell));
				} else {
					// 从最大级别开始填值
					Object[] objects = processMapInfo.get(index);
					wsheet.addCell(new Label((Integer.valueOf(objects[0]
							.toString()) - minLevel) * 2 + 1, rowNumber,
							objects[1].toString(), normalCell));
					wsheet.addCell(new Label((Integer.valueOf(objects[0]
							.toString()) - minLevel) * 2 + 2, rowNumber,
							objects[2].toString(), normalCell));
				}
			}
			int num = (totalLevel - minLevel) * 2 + 1;
			// 发布状态
			switch (processWebBean.getTypeByData()) {
			case 0:// 未发布
				// 流程名称
				wsheet.addCell(new Label(num, rowNumber, processWebBean
						.getFlowName(), redCell));
				break;
			case 1:// 待审批
				// 流程名称
				wsheet.addCell(new Label(num, rowNumber, processWebBean
						.getFlowName(), brownCell));
				break;
			case 2:// 已发布
				// 流程名称
				wsheet.addCell(new Label(num, rowNumber, processWebBean
						.getFlowName(), greenCell));
				break;
			}
			num++;
			// 流程编号
			wsheet.addCell(new Label(num, rowNumber, processWebBean
					.getFlowIdInput(), normalCell));
			num++;
			// 发布状态
			switch (processWebBean.getTypeByData()) {
			case 0:
				wsheet.addCell(new Label(num, rowNumber, JecnUtil
						.getValue("hasNotPub"), normalCell));
				break;
			case 1:
				wsheet.addCell(new Label(num, rowNumber, JecnUtil
						.getValue("waitForApprove"), normalCell));
				break;
			case 2:
				wsheet.addCell(new Label(num, rowNumber, JecnUtil
						.getValue("hasPub"), normalCell));
				break;
			default:
				wsheet.addCell(new Label(num, rowNumber, "", normalCell));
				break;
			}
			num++;
			// 责任部门
			wsheet.addCell(new Label(num, rowNumber, processWebBean
					.getOrgName(), normalCell));
			num++;
			// 流程责任人
			wsheet.addCell(new Label(num, rowNumber, processWebBean
					.getResPeopleName(), normalCell));
			if (isGuardShow) {
				num++;
				// 流程监护人
				wsheet.addCell(new Label(num, rowNumber, processWebBean
						.getGuardianName(), normalCell));
			}
			// 拟稿人
			num++;
			wsheet.addCell(new Label(num, rowNumber, processWebBean
					.getDraftPerson(), normalCell));
			// 版本号
			num++;
			wsheet.addCell(new Label(num, rowNumber, processWebBean
					.getVersionId(), normalCell));
			// 发布日期
			num++;
			wsheet.addCell(new Label(num, rowNumber, processWebBean
					.getPubDate(), normalCell));
			// 有效日期
			String expiry = "";
			if (processWebBean.getTypeByData() == 2) {
				if (processWebBean.getExpiry() == 0) {
					// 永久
					expiry = JecnUtil.getValue("permanent");
				} else {
					expiry = String.valueOf(processWebBean.getExpiry());
				}
			}
			num++;
			wsheet.addCell(new Label(num, rowNumber, expiry, normalCell));
			// 是否及时优化
			String optimization = "";
			if (processWebBean.getOptimization() == 1) {
				// 是
				optimization = JecnUtil.getValue("is");
			} else if (processWebBean.getOptimization() == 2) {
				// 否
				optimization = JecnUtil.getValue("no");
			}
			num++;
			wsheet.addCell(new Label(num, rowNumber, optimization, normalCell));
			// 需要审视及优化时间
			num++;
			if (processWebBean.getExpiry() == 0) {
				wsheet.addCell(new Label(num, rowNumber, "", normalCell));
			} else {
				wsheet.addCell(new Label(num, rowNumber, processWebBean
						.getNextScanDate(), normalCell));
			}
			rowNumber++;
		}

		wsheet.setRowView(0, 360);
		// 版本号 总计
		wsheet.addCell(new Label(1, 0, JecnUtil.getValue("total") + "("
				+ processSystemListBean.getFlowTotal() + ")", countCell));
		// 发布日期 待建流程
		wsheet.addCell(new Label(2, 0, JecnUtil
				.getValue("generationToBuildProcess")
				+ "(" + processSystemListBean.getNoPubFlowTotal() + ")",
				countRedCell));
		// 有效日期 待审批流程
		wsheet.addCell(new Label(3, 0, JecnUtil.getValue("forApprovalProcess")
				+ "(" + processSystemListBean.getApproveFlowTotal() + ")",
				countBrownCell));
		// 是否及时优化 发布流程
		wsheet.addCell(new Label(4, 0, JecnUtil.getValue("releaseProcess")
				+ "(" + processSystemListBean.getPubFlowTotal() + ")",
				countGreenCell));
	}

	/**
	 * 获取单元格样式
	 * 
	 * @author weidp
	 * @date 2014-10-30 下午05:32:55
	 * @param bold
	 *            true 加粗
	 * @param colour
	 * @return
	 * @throws WriteException
	 */
	private static WritableCellFormat getCellFormat(boolean bold,
			boolean hasBorder, Colour colour) throws WriteException {
		// 定义字体
		WritableFont font = new WritableFont(WritableFont.createFont("宋体"), 11,
				bold ? WritableFont.BOLD : WritableFont.NO_BOLD, false,
				UnderlineStyle.NO_UNDERLINE, colour);
		WritableCellFormat wformatContent = new WritableCellFormat(font);
		// 垂直水平居中
		wformatContent.setAlignment(Alignment.CENTRE);
		wformatContent.setVerticalAlignment(VerticalAlignment.CENTRE);
		// 设置自动换行
		wformatContent.setWrap(true);
		if (hasBorder) {
			// 边框样式
			wformatContent.setBorder(Border.ALL, BorderLineStyle.THIN);
		}
		return wformatContent;
	}
}
