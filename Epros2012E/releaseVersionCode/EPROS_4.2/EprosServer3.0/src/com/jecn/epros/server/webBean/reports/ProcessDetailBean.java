package com.jecn.epros.server.webBean.reports;

import java.util.ArrayList;
import java.util.List;

/**流程清单类*/
public class ProcessDetailBean {
	
	/** 责任部门或者流程地图下的流程清单数量详细信息类*/
	private List<ProcessDetailWebListBean> allDetailWebListBeans=new ArrayList<ProcessDetailWebListBean>();
	/** 截止时间*/
	private String endTime;
	/** 流程监护人是否显示*/
    private boolean guardianPeopleIsShow;
    private int type = 0;
    
	public boolean isGuardianPeopleIsShow() {
		return guardianPeopleIsShow;
	}
	public void setGuardianPeopleIsShow(boolean guardianPeopleIsShow) {
		this.guardianPeopleIsShow = guardianPeopleIsShow;
	}
	
	public List<ProcessDetailWebListBean> getAllDetailWebListBeans() {
		return allDetailWebListBeans;
	}
	public void setAllDetailWebListBeans(
			List<ProcessDetailWebListBean> allDetailWebListBeans) {
		this.allDetailWebListBeans = allDetailWebListBeans;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
}
	
	