package com.jecn.epros.server.bean.download;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.file.FileWebBean;

/**
 * 流程操作说明数据Bean
 * 
 * @author fuzhh Nov 7, 2012
 */
public class ProcessDownloadBaseBean implements Serializable {

	/** 流程记录 0 文件编号 1 文件名称 */
	private List<Object[]> processRecordList = new ArrayList<Object[]>();
	/** 操作规范 0 文件编号 1 文件名称 2 对应的活动编号 */
	private List<Object[]> operationTemplateList = new ArrayList<Object[]>();
	/** 农夫山泉-活动说明显示操作规范 操作规范 0 文件编号 1 文件名称 2 对应的活动编号 */
	private List<Object[]> operationTemplateListNF = new ArrayList<Object[]>();
	/** 相关文件 **/
	private List<FileWebBean> relatedFiles = new ArrayList<FileWebBean>();

	public void addOperationTemplateListNF(Object[] objects) {
		if (objects == null || objects.length == 0) {
			return;
		}
		this.operationTemplateListNF.add(objects);
	}

	public List<Object[]> getProcessRecordList() {
		return processRecordList;
	}

	public void setProcessRecordList(List<Object[]> processRecordList) {
		this.processRecordList = processRecordList;
	}

	public List<Object[]> getOperationTemplateList() {
		return operationTemplateList;
	}

	public void setOperationTemplateList(List<Object[]> operationTemplateList) {
		this.operationTemplateList = operationTemplateList;
	}

	public List<Object[]> getOperationTemplateListNF() {
		return operationTemplateListNF;
	}

	public void setOperationTemplateListNF(List<Object[]> operationTemplateListNF) {
		this.operationTemplateListNF = operationTemplateListNF;
	}

	public List<FileWebBean> getRelatedFiles() {
		return relatedFiles;
	}

	public void setRelatedFiles(List<FileWebBean> relatedFiles) {
		this.relatedFiles = relatedFiles;
	}
}
