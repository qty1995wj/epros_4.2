package com.jecn.epros.bean;

public class TaskTwoApproveParam extends BaseTaskParam {
	
	/** 二次评审人IDS */
	private String newViewerIds;
	
	public String getNewViewerIds() {
		return newViewerIds;
	}
	public void setNewViewerIds(String newViewerIds) {
		this.newViewerIds = newViewerIds;
	}

	
}
