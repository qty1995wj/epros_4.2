/**
 * UUVServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cms.designerSSO;

import com.jecn.epros.server.action.web.login.ad.cms.JecnCMSAfterItem;

public class UUVServiceLocator extends org.apache.axis.client.Service implements com.jecn.webservice.cms.designerSSO.UUVService {

    public UUVServiceLocator() {
    }


    public UUVServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public UUVServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for UUVServiceSoap
    private java.lang.String UUVServiceSoap_address = JecnCMSAfterItem.getValue("DESIGNER_SERVICE");

    public java.lang.String getUUVServiceSoapAddress() {
        return UUVServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String UUVServiceSoapWSDDServiceName = "UUVServiceSoap";

    public java.lang.String getUUVServiceSoapWSDDServiceName() {
        return UUVServiceSoapWSDDServiceName;
    }

    public void setUUVServiceSoapWSDDServiceName(java.lang.String name) {
        UUVServiceSoapWSDDServiceName = name;
    }

    public com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType getUUVServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(UUVServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getUUVServiceSoap(endpoint);
    }

    public com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType getUUVServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.jecn.webservice.cms.designerSSO.UUVServiceSoap_BindingStub _stub = new com.jecn.webservice.cms.designerSSO.UUVServiceSoap_BindingStub(portAddress, this);
            _stub.setPortName(getUUVServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setUUVServiceSoapEndpointAddress(java.lang.String address) {
        UUVServiceSoap_address = address;
    }


    // Use to get a proxy class for UUVServiceSoap12
    private java.lang.String UUVServiceSoap12_address = JecnCMSAfterItem.getValue("DESIGNER_SERVICE");

    public java.lang.String getUUVServiceSoap12Address() {
        return UUVServiceSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String UUVServiceSoap12WSDDServiceName = "UUVServiceSoap12";

    public java.lang.String getUUVServiceSoap12WSDDServiceName() {
        return UUVServiceSoap12WSDDServiceName;
    }

    public void setUUVServiceSoap12WSDDServiceName(java.lang.String name) {
        UUVServiceSoap12WSDDServiceName = name;
    }

    public com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType getUUVServiceSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(UUVServiceSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getUUVServiceSoap12(endpoint);
    }

    public com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType getUUVServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.jecn.webservice.cms.designerSSO.UUVServiceSoap12Stub _stub = new com.jecn.webservice.cms.designerSSO.UUVServiceSoap12Stub(portAddress, this);
            _stub.setPortName(getUUVServiceSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setUUVServiceSoap12EndpointAddress(java.lang.String address) {
        UUVServiceSoap12_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.jecn.webservice.cms.designerSSO.UUVServiceSoap_BindingStub _stub = new com.jecn.webservice.cms.designerSSO.UUVServiceSoap_BindingStub(new java.net.URL(UUVServiceSoap_address), this);
                _stub.setPortName(getUUVServiceSoapWSDDServiceName());
                return _stub;
            }
            if (com.jecn.webservice.cms.designerSSO.UUVServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.jecn.webservice.cms.designerSSO.UUVServiceSoap12Stub _stub = new com.jecn.webservice.cms.designerSSO.UUVServiceSoap12Stub(new java.net.URL(UUVServiceSoap12_address), this);
                _stub.setPortName(getUUVServiceSoap12WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("UUVServiceSoap".equals(inputPortName)) {
            return getUUVServiceSoap();
        }
        else if ("UUVServiceSoap12".equals(inputPortName)) {
            return getUUVServiceSoap12();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://cmschina.com.cn/", "UUVService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://cmschina.com.cn/", "UUVServiceSoap"));
            ports.add(new javax.xml.namespace.QName("http://cmschina.com.cn/", "UUVServiceSoap12"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("UUVServiceSoap".equals(portName)) {
            setUUVServiceSoapEndpointAddress(address);
        }
        else 
if ("UUVServiceSoap12".equals(portName)) {
            setUUVServiceSoap12EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
