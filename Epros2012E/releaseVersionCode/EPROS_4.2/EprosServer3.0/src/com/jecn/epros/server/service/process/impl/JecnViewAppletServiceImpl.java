package com.jecn.epros.server.service.process.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;

import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnLineSegmentDep;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnLineSegment;
import com.jecn.epros.server.bean.process.JecnLineSegmentT;
import com.jecn.epros.server.bean.process.JecnMapLineSegment;
import com.jecn.epros.server.bean.process.JecnMapLineSegmentT;
import com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT;
import com.jecn.epros.server.bean.system.JecnUserTotal;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.popedom.IPositoinGroupDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IJecnUserTotalDao;
import com.jecn.epros.server.service.process.IViewAppletService;
import com.jecn.epros.server.service.process.util.SvgDataTransformUtil;
import com.jecn.svg.SvgPanelBuilder;
import com.jecn.svg.SvgSpecialGraphBuilder;
import com.jecn.svg.dataModel.SvgPanel;
import com.jecn.svg.dataModel.SvgPanel.PanelType;
import com.jecn.svg.dataModel.epros.SvgEprosGraphData;
import com.jecn.svg.dataModel.graphs.SvgLinkIcon;
import com.jecn.svg.dataModel.graphs.SvgLinkIcon.IconName;

/**
 * 
 * 浏览器applet对应的处理层
 * 
 * @author ZHOUXY
 * 
 */
public class JecnViewAppletServiceImpl extends AbsBaseService<JecnFlowStructureT, Long> implements IViewAppletService {
	private final Logger log = Logger.getLogger(JecnViewAppletServiceImpl.class);
	private IFlowStructureDao flowStructureDao;
	/** 记录查阅流程图 下载操作说明 */
	private IJecnUserTotalDao userTotalDao;
	/** 岗位组操作Dao */
	private IPositoinGroupDao positionGroupDao;

	@Override
	public List<Object[]> getPositionsByPosGroupId(long posGroupId) throws Exception {
		return positionGroupDao.getPositionsByPosGroupId(posGroupId);
	}

	/**
	 * 
	 * 查询流程地图数据
	 * 
	 * @param processId
	 *            流程地图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	public List<List<Object[]>> getTotalMapT(Long processId, Long peopleId, Long projectId) throws Exception {

		try {
			List<List<Object[]>> listResult = new ArrayList<List<Object[]>>();

			// 查询图形元素属性集合sql查询图形元素属性集合sql
			List<Object[]> list = getProcessMapObjectList("_T", processId);
			listResult.add(list);

			// 查询连接线对应小线段sql
			String sql = getLinementSql();
			// 连接线对应小线段
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 参与的图形
			listResult.add(getParticipationElementT(processId, peopleId, projectId));

			// 名称 3
			sql = "SELECT F.FLOW_ID, F.FLOW_NAME,FILE_PATH,F.PRE_FLOW_ID,F.ISFLOW  FROM JECN_FLOW_STRUCTURE_T F WHERE F.FLOW_ID=?";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 流程地图相关附件 4
			sql = "SELECT FF.ID, FF.FIGURE_ID, FF.FILE_ID, FF.FIGURE_TYPE, FT.FILE_NAME"
					+ "  FROM JECN_FIGURE_FILE_T FF, JECN_FILE_T FT"
					+ " WHERE FF.FILE_ID = FT.FILE_ID AND FT.DEL_STATE=0" + "   AND EXISTS (SELECT 1"
					+ "          FROM JECN_FLOW_STRUCTURE_IMAGE_T IT" + "         WHERE FF.FIGURE_ID = IT.FIGURE_ID"
					+ "           AND IT.FLOW_ID = ?)";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	private List<Object[]> getParticipationElementT(Long processId, Long peopleId, Long projectId) {
		String sql = "";
		List<Object[]> list = null;
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "with  TMP as(select ps.figure_flow_id " + "    from jecn_flow_org              jfo, "
					+ "         jecn_flow_org_image        jfoi, " + "         PROCESS_STATION_RELATED_T    ps, "
					+ "         jecn_user_position_related jupr " + "   where ps.figure_position_id = "
					+ "         jfoi.figure_id " + "     and ps.type = '0' " + "     and jfoi.org_id = jfo.org_id "
					+ "     and jfo.del_state = 0 " + "     and jfoi.figure_id = " + "         jupr.figure_id "
					+ "     and jupr.people_id = ? " + " UNION " + "select ps.figure_flow_id "
					+ "    from jecn_flow_org              jfo, " + "         jecn_flow_org_image        jfoi, "
					+ "         PROCESS_STATION_RELATED_T    ps, " + "         jecn_position_group_r      jpgr, "
					+ "         jecn_user_position_related jupr " + "   where ps.figure_position_id = "
					+ "         jpgr.group_id " + "     and ps.type = '1' " + "     and jpgr.figure_id = "
					+ "         jfoi.figure_id " + "     and jfoi.org_id = jfo.org_id " + "     and jfo.del_state = 0 "
					+ "     and jfoi.figure_id = " + "         jupr.figure_id " + "     and jupr.people_id = ?), "
					+ "MY_FLOW AS (SELECT * " + "    FROM JECN_FLOW_STRUCTURE_T JF " + "   WHERE JF.FLOW_ID in "
					+ "         (select jfsi.flow_id " + "            from jecn_flow_structure_image_t jfsi, "
					+ "JECN_FLOW_STRUCTURE_T       jfs, " + "TMP " + "           where jfsi.flow_id = jfs.flow_id "
					+ "             and jfs.del_state = 0 " + "             and jfs.projectid = ? "
					+ "             and jfsi.figure_id in (TMP.figure_flow_id)) " + "     UNION ALL "
					+ "  SELECT JFS.* " + "    FROM MY_FLOW "
					+ "   INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.Pre_Flow_Id = " + "  JFS.Flow_Id) "
					+ "select distinct jfsi_t.figure_id, jfsi_t.figure_type "
					+ "          from jecn_flow_structure_image_t jfsi_t " + "         where jfsi_t.figure_type <> "
					+ JecnCommonSql.getSystemString() + "           and jfsi_t.figure_type <> "
					+ JecnCommonSql.getIconString() + "           and jfsi_t.flow_id = ? "
					+ "           and jfsi_t.Link_Flow_Id in (select flow_id from MY_FLOW)";
			list = flowStructureDao.listNativeSql(sql, peopleId, peopleId, projectId, processId);
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {

			sql = "with roles as  ((select ps.figure_flow_id "
					+ "                                from jecn_flow_org              jfo, "
					+ "                                     jecn_flow_org_image        jfoi, "
					+ "                                     process_station_related_t    ps, "
					+ "                                     jecn_user_position_related jupr "
					+ "                               where ps.figure_position_id = jfoi.figure_id "
					+ "                                 and ps.type = '0' "
					+ "                                 and jfoi.org_id = jfo.org_id "
					+ "                                 and jfo.del_state = 0 "
					+ "                                 and jfoi.figure_id = jupr.figure_id "
					+ "                                 and jupr.people_id ="
					+ peopleId
					+ ")"
					+ "                                 union "
					+ "                            (select ps.figure_flow_id "
					+ "                                from jecn_flow_org              jfo, "
					+ "                                     jecn_flow_org_image        jfoi, "
					+ "                                     process_station_related_t    ps, "
					+ "                                     jecn_position_group_r      jpgr, "
					+ "                                     jecn_user_position_related jupr "
					+ "                               where ps.figure_position_id = jpgr.group_id "
					+ "                                 and ps.type = '1' "
					+ "                                 and jpgr.figure_id = jfoi.figure_id "
					+ "                                 and jfoi.org_id = jfo.org_id "
					+ "                                 and jfo.del_state = 0 "
					+ "                                 and jfoi.figure_id = jupr.figure_id "
					+ "                                 and jupr.people_id = "
					+ peopleId
					+ ")), "
					+ "  flowIds as( select distinct jfsi.flow_id "
					+ "                       from jecn_flow_structure_image_t jfsi, "
					+ "                            jecn_flow_structure_t       jfs, "
					+ "                            roles                     rs "
					+ "                      where jfsi.flow_id = jfs.flow_id and jfsi.figure_id=rs.figure_flow_id "
					+ "                        and jfs.del_state = 0 "
					+ "                        and jfs.projectid = "
					+ projectId
					+ "), "
					+ "   jfsFlowIds  as (select jfs_t.flow_id "
					+ "          from jecn_flow_structure_t jfs_t "
					+ "        CONNECT BY PRIOR jfs_t.pre_flow_id = jfs_t.flow_id "
					+ "         start with jfs_t.flow_id in (select flow_id from flowIds)) "
					+ "      select distinct jfsi_t.figure_id, jfsi_t.figure_type "
					+ "  from jecn_flow_structure_image_t jfsi_t,jfsFlowIds fs "
					+ " where jfsi_t.figure_type <> "
					+ JecnCommonSql.getSystemString()
					+ "   and jfsi_t.figure_type <> "
					+ JecnCommonSql.getIconString()
					+ "   and jfsi_t.flow_id =? " + "   and jfsi_t.Link_Flow_Id=fs.flow_id ";
			list = flowStructureDao.listNativeSql(sql, processId);
		}
		return list;
	}

	private List<Object[]> getParticipationElementH(Long processId, Long peopleId, Long projectId, Long historyId) {
		String sql = "";
		List<Object[]> list = null;
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "with  TMP as(select ps.figure_flow_id " + "    from jecn_flow_org              jfo, "
					+ "         jecn_flow_org_image        jfoi, " + "         PROCESS_STATION_RELATED_T    ps, "
					+ "         jecn_user_position_related jupr " + "   where ps.figure_position_id = "
					+ "         jfoi.figure_id " + "     and ps.type = '0' " + "     and jfoi.org_id = jfo.org_id "
					+ "     and jfo.del_state = 0 " + "     and jfoi.figure_id = " + "         jupr.figure_id "
					+ "     and jupr.people_id = ? " + " UNION " + "select ps.figure_flow_id "
					+ "    from jecn_flow_org              jfo, " + "         jecn_flow_org_image        jfoi, "
					+ "         PROCESS_STATION_RELATED_T    ps, " + "         jecn_position_group_r      jpgr, "
					+ "         jecn_user_position_related jupr " + "   where ps.figure_position_id = "
					+ "         jpgr.group_id " + "     and ps.type = '1' " + "     and jpgr.figure_id = "
					+ "         jfoi.figure_id " + "     and jfoi.org_id = jfo.org_id " + "     and jfo.del_state = 0 "
					+ "     and jfoi.figure_id = " + "         jupr.figure_id " + "     and jupr.people_id = ?), "
					+ "MY_FLOW AS (SELECT * " + "    FROM JECN_FLOW_STRUCTURE_T JF " + "   WHERE JF.FLOW_ID in "
					+ "         (select jfsi.flow_id " + "            from jecn_flow_structure_image_t jfsi, "
					+ "JECN_FLOW_STRUCTURE_T       jfs, " + "TMP " + "           where jfsi.flow_id = jfs.flow_id "
					+ "             and jfs.del_state = 0 " + "             and jfs.projectid = ? "
					+ "             and jfsi.figure_id in (TMP.figure_flow_id)) " + "     UNION ALL "
					+ "  SELECT JFS.* " + "    FROM MY_FLOW "
					+ "   INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.Pre_Flow_Id = " + "  JFS.Flow_Id) "
					+ "select distinct jfsi_t.figure_id, jfsi_t.figure_type "
					+ "          from jecn_flow_structure_image_t jfsi_t " + "         where jfsi_t.figure_type <> "
					+ JecnCommonSql.getSystemString() + "           and jfsi_t.figure_type <> "
					+ JecnCommonSql.getIconString() + "           and jfsi_t.flow_id = ? "
					+ "           and jfsi_t.Link_Flow_Id in (select flow_id from MY_FLOW)";
			list = flowStructureDao.listNativeSql(sql, peopleId, peopleId, projectId, processId);
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {

			sql = "with roles as" + " ((select ps.figure_flow_id" + "     from jecn_flow_org              jfo,"
					+ "          jecn_flow_org_image        jfoi," + "          process_station_related_H  ps,"
					+ "          jecn_user_position_related jupr" + "    where ps.figure_position_id = jfoi.figure_id"
					+ "      and ps.type = '0'" + "      and jfoi.org_id = jfo.org_id" + "      and jfo.del_state = 0"
					+ "      and jfoi.figure_id = jupr.figure_id" + "      and jupr.people_id ="
					+ peopleId
					+ "      AND PS.HISTORY_ID = "
					+ historyId
					+ "      ) union (select ps.figure_flow_id"
					+ "                                      from jecn_flow_org              jfo,"
					+ "                                           jecn_flow_org_image        jfoi,"
					+ "                                           process_station_related_H  ps,"
					+ "                                           jecn_position_group_r      jpgr,"
					+ "                                           jecn_user_position_related jupr"
					+ "                                     where ps.figure_position_id ="
					+ "                                           jpgr.group_id"
					+ "                                       and ps.type = '1'"
					+ "                                       and jpgr.figure_id = jfoi.figure_id"
					+ "                                       and jfoi.org_id = jfo.org_id"
					+ "                                       and jfo.del_state = 0"
					+ "                                       and jfoi.figure_id = jupr.figure_id"
					+ "                                       and jupr.people_id = "
					+ peopleId
					+ "                                       AND PS.HISTORY_ID = "
					+ historyId
					+ "                                       )),"
					+ "flowIds as"
					+ " (select distinct jfsi.flow_id"
					+ "    from jecn_flow_structure_image_H jfsi,"
					+ "         jecn_flow_structure_H       jfs,"
					+ "         roles                       rs"
					+ "   where jfsi.flow_id = jfs.flow_id"
					+ "     and jfsi.figure_id = rs.figure_flow_id"
					+ "     and jfs.del_state = 0"
					+ "     and jfs.projectid = "
					+ projectId
					+ "     AND JFS.HISTORY_ID = JFSI.HISTORY_ID"
					+ "     AND JFSI.HISTORY_ID = "
					+ historyId
					+ "     ),"
					+ "jfsFlowIds as"
					+ " (select jfs_t.flow_id"
					+ "    from jecn_flow_structure_H jfs_t"
					+ "  CONNECT BY PRIOR jfs_t.pre_flow_id = jfs_t.flow_id"
					+ "   start with jfs_t.flow_id in (select flow_id from flowIds))"
					+ "select distinct jfsi_t.figure_id, jfsi_t.figure_type"
					+ "  from jecn_flow_structure_image_H jfsi_t, jfsFlowIds fs"
					+ " where jfsi_t.figure_type <> "
					+ JecnCommonSql.getSystemString()
					+ "   and jfsi_t.figure_type <>  "
					+ JecnCommonSql.getIconString()
					+ "   and jfsi_t.flow_id = ?"
					+ "   and jfsi_t.Link_Flow_Id = fs.flow_id" + "   AND JFSI_T.HISTORY_ID =" + historyId;
			list = flowStructureDao.listNativeSql(sql, processId);
		}
		return list;
	}

	/**
	 * 
	 * 查询流程地图数据
	 * 
	 * @param processId
	 *            流程地图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	public List<List<Object[]>> getTotalMap(Long processId, Long peopleId, Long projectId) throws Exception {

		try {
			List<List<Object[]>> listResult = new ArrayList<List<Object[]>>();

			// 查询图形元素属性集合sql查询图形元素属性集合sql

			List<Object[]> list = getProcessMapObjectList("", processId);
			listResult.add(list);

			// 查询连接线对应小线段sql
			String sql = getLinementSql();
			// 连接线对应小线段
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 参与的元素
			listResult.add(getParticipationElement(processId, peopleId, projectId));
			// 名称 3
			sql = "SELECT F.FLOW_ID, F.FLOW_NAME,FILE_PATH,F.PRE_FLOW_ID,F.ISFLOW  FROM JECN_FLOW_STRUCTURE F WHERE F.FLOW_ID=?";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 流程地图相关附件 4
			sql = "SELECT FF.ID, FF.FIGURE_ID, FF.FILE_ID, FF.FIGURE_TYPE, FT.FILE_NAME"
					+ "  FROM JECN_FIGURE_FILE FF, JECN_FILE FT" + " WHERE FF.FILE_ID = FT.FILE_ID AND FT.DEL_STATE=0"
					+ "   AND EXISTS (SELECT 1" + "          FROM JECN_FLOW_STRUCTURE_IMAGE IT"
					+ "         WHERE FF.FIGURE_ID = IT.FIGURE_ID" + "           AND IT.FLOW_ID = ?)";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	/**
	 * 参与的图形
	 * 
	 * @param processId
	 * @param peopleId
	 * @param projectId
	 * @return List<Object[]> figure_id, figure_type
	 * @throws
	 */
	private List<Object[]> getParticipationElement(Long processId, Long peopleId, Long projectId) {
		String sql = "";
		List<Object[]> list = null;
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "with  TMP as(select ps.figure_flow_id " + "    from jecn_flow_org              jfo, "
					+ "         jecn_flow_org_image        jfoi, " + "         process_station_related    ps, "
					+ "         jecn_user_position_related jupr " + "   where ps.figure_position_id = "
					+ "         jfoi.figure_id " + "     and ps.type = '0' " + "     and jfoi.org_id = jfo.org_id "
					+ "     and jfo.del_state = 0 " + "     and jfoi.figure_id = " + "         jupr.figure_id "
					+ "     and jupr.people_id = ? " + " UNION " + "select ps.figure_flow_id "
					+ "    from jecn_flow_org              jfo, " + "         jecn_flow_org_image        jfoi, "
					+ "         process_station_related    ps, " + "         jecn_position_group_r      jpgr, "
					+ "         jecn_user_position_related jupr " + "   where ps.figure_position_id = "
					+ "         jpgr.group_id " + "     and ps.type = '1' " + "     and jpgr.figure_id = "
					+ "         jfoi.figure_id " + "     and jfoi.org_id = jfo.org_id " + "     and jfo.del_state = 0 "
					+ "     and jfoi.figure_id = " + "         jupr.figure_id " + "     and jupr.people_id = ?), "
					+ "MY_FLOW AS (SELECT * " + "    FROM JECN_FLOW_STRUCTURE JF " + "   WHERE JF.FLOW_ID in "
					+ "         (select jfsi.flow_id " + "            from jecn_flow_structure_image jfsi, "
					+ "jecn_flow_structure       jfs, " + "TMP " + "           where jfsi.flow_id = jfs.flow_id "
					+ "             and jfs.del_state = 0 " + "             and jfs.projectid = ? "
					+ "             and jfsi.figure_id in (TMP.figure_flow_id)) " + "     UNION ALL "
					+ "  SELECT JFS.* " + "    FROM MY_FLOW "
					+ "   INNER JOIN JECN_FLOW_STRUCTURE JFS ON MY_FLOW.Pre_Flow_Id = " + "  JFS.Flow_Id) "
					+ "select distinct jfsi_t.figure_id, jfsi_t.figure_type "
					+ "          from jecn_flow_structure_image jfsi_t " + "         where jfsi_t.figure_type <> "
					+ JecnCommonSql.getSystemString() + "           and jfsi_t.figure_type <> "
					+ JecnCommonSql.getIconString() + "           and jfsi_t.flow_id = ? "
					+ "           and jfsi_t.Link_Flow_Id in (select flow_id from MY_FLOW)";
			list = flowStructureDao.listNativeSql(sql, peopleId, peopleId, projectId, processId);
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "with roles as  ((select ps.figure_flow_id "
					+ "                                from jecn_flow_org              jfo, "
					+ "                                     jecn_flow_org_image        jfoi, "
					+ "                                     process_station_related    ps, "
					+ "                                     jecn_user_position_related jupr "
					+ "                               where ps.figure_position_id = jfoi.figure_id "
					+ "                                 and ps.type = '0' "
					+ "                                 and jfoi.org_id = jfo.org_id "
					+ "                                 and jfo.del_state = 0 "
					+ "                                 and jfoi.figure_id = jupr.figure_id "
					+ "                                 and jupr.people_id ="
					+ peopleId
					+ ")"
					+ "                                 union "
					+ "                            (select ps.figure_flow_id "
					+ "                                from jecn_flow_org              jfo, "
					+ "                                     jecn_flow_org_image        jfoi, "
					+ "                                     process_station_related    ps, "
					+ "                                     jecn_position_group_r      jpgr, "
					+ "                                     jecn_user_position_related jupr "
					+ "                               where ps.figure_position_id = jpgr.group_id "
					+ "                                 and ps.type = '1' "
					+ "                                 and jpgr.figure_id = jfoi.figure_id "
					+ "                                 and jfoi.org_id = jfo.org_id "
					+ "                                 and jfo.del_state = 0 "
					+ "                                 and jfoi.figure_id = jupr.figure_id "
					+ "                                 and jupr.people_id = "
					+ peopleId
					+ ")), "
					+ "  flowIds as( select distinct jfsi.flow_id "
					+ "                       from jecn_flow_structure_image jfsi, "
					+ "                            jecn_flow_structure       jfs, "
					+ "                            roles                     rs "
					+ "                      where jfsi.flow_id = jfs.flow_id and jfsi.figure_id=rs.figure_flow_id "
					+ "                        and jfs.del_state = 0 "
					+ "                        and jfs.projectid = "
					+ projectId
					+ "), "
					+ "   jfsFlowIds  as (select jfs_t.flow_id "
					+ "          from jecn_flow_structure jfs_t "
					+ "        CONNECT BY PRIOR jfs_t.pre_flow_id = jfs_t.flow_id "
					+ "         start with jfs_t.flow_id in (select flow_id from flowIds)) "
					+ "      select distinct jfsi_t.figure_id, jfsi_t.figure_type "
					+ "  from jecn_flow_structure_image jfsi_t,jfsFlowIds fs "
					+ " where jfsi_t.figure_type <> "
					+ JecnCommonSql.getSystemString()
					+ "   and jfsi_t.figure_type <> "
					+ JecnCommonSql.getIconString()
					+ "   and jfsi_t.flow_id =? " + "   and jfsi_t.Link_Flow_Id=fs.flow_id ";

			list = flowStructureDao.listNativeSql(sql, processId);
		}
		return list;
	}

	/**
	 * 
	 * 查询流程图数据
	 * 
	 * @param processId
	 *            流程图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	@Override
	public List<List<Object[]>> getPartMap(Long processId, Long peopleId, Long projectId) throws Exception {
		try {
			List<List<Object[]>> listResult = new ArrayList<List<Object[]>>();
			// 查询图形元素属性集合sql
			List<Object[]> list = getProcessObjectList("", processId);
			listResult.add(list);

			// 查询连接线对应小线段sql
			String sql = getLinementSql();
			// 连接线对应小线段
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 活动的输入和操作规范
			sql = "SELECT JAF.FIGURE_ID,JAF.FILE_TYPE,JF.FILE_ID,JF.FILE_NAME"
					+ "       FROM JECN_FLOW_STRUCTURE_IMAGE JFSI,JECN_ACTIVITY_FILE JAF,JECN_FILE JF"
					+ "       WHERE JAF.FIGURE_ID=JFSI.FIGURE_ID AND JAF.FILE_S_ID = JF.FILE_ID AND JF.DEL_STATE=0 AND JFSI.FLOW_ID = ?";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);
			// 活动的输出
			sql = "SELECT JMF.FIGURE_ID,JF.FILE_ID,JF.FILE_NAME FROM JECN_MODE_FILE JMF,JECN_FLOW_STRUCTURE_IMAGE JFSI,JECN_FILE JF"
					+ "       WHERE JMF.FIGURE_ID=JFSI.FIGURE_ID AND JMF.FILE_M_ID=JF.FILE_ID AND JF.DEL_STATE=0 AND JFSI.FLOW_ID=?";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);
			// 角色的岗位

			sql = "SELECT PS.FIGURE_FLOW_ID,PS.TYPE, " + "       PS.FIGURE_POSITION_ID,JFOI.FIGURE_TEXT,JFO.Org_Name "
					+ "    FROM JECN_FLOW_ORG              JFO, " + "         JECN_FLOW_ORG_IMAGE        JFOI, "
					+ "         PROCESS_STATION_RELATED    PS, " + " JECN_FLOW_STRUCTURE_IMAGE JFSI "
					+ "   WHERE  PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID " + " AND JFSI.FLOW_ID = ?"
					+ " AND PS.FIGURE_POSITION_ID =  JFOI.FIGURE_ID " + "     AND PS.TYPE = '0' "
					+ "     AND JFOI.ORG_ID = JFO.ORG_ID " + "     AND JFO.DEL_STATE = 0 " + " UNION "
					+ " SELECT PS.FIGURE_FLOW_ID ,PS.TYPE, " + " PS.FIGURE_POSITION_ID,JPG.NAME,NULL " + "    FROM "
					+ " JECN_FLOW_STRUCTURE_IMAGE JFSI, " + " JECN_POSITION_GROUP   JPG, "
					+ " JECN_FLOW_ORG              JFO, " + "  JECN_FLOW_ORG_IMAGE        JFOI, "
					+ "  PROCESS_STATION_RELATED    PS, " + "  JECN_POSITION_GROUP_R      JPGR "
					+ "  WHERE PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID AND JFSI.FLOW_ID = ? "
					+ " AND JPG.ID = JPGR.GROUP_ID " + " AND PS.FIGURE_POSITION_ID = JPGR.GROUP_ID "
					+ " AND PS.TYPE = '1' " + " AND JPGR.FIGURE_ID =  JFOI.FIGURE_ID "
					+ " AND JFOI.ORG_ID = JFO.ORG_ID " + "  AND JFO.DEL_STATE = 0";
			list = flowStructureDao.listNativeSql(sql, processId, processId);
			listResult.add(list);

			// 流程图名称以及横竖标识 5
			sql = "SELECT F.FLOW_NAME, T.IS_X_OR_Y,F.FILE_PATH,F.PRE_FLOW_ID,"
					+ "(SELECT PF.ISFLOW FROM JECN_FLOW_STRUCTURE PF WHERE PF.FLOW_ID = F.PRE_FLOW_ID) AS PRE_ISFLOW  "
					+ "FROM JECN_FLOW_STRUCTURE F, JECN_FLOW_BASIC_INFO T WHERE F.FLOW_ID=T.FLOW_ID AND F.FLOW_ID=?";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 流程活动控制点 6
			sql = "SELECT JCP.ID," + "       JCP.ACTIVE_ID," + "       JCP.CONTROL_CODE," + "       JCP.KEY_POINT,"
					+ "       JCP.FREQUENCY," + "       JCP.METHOD," + "       JCP.TYPE," + "       JCP.RISK_ID,"
					+ "       JCP.TARGET_ID," + "       JCP.CREATE_PERSON," + "       JCP.CREATE_TIME,"
					+ "       JCP.UPDATE_PERSON," + "       JCP.UPDATE_TIME," + "       JCP.NOTE"
					+ "  FROM JECN_CONTROL_POINT JCP, JECN_FLOW_STRUCTURE_IMAGE JFSI" + " WHERE JFSI.FIGURE_TYPE IN "
					+ JecnCommonSql.getActiveString() + "   AND JCP.ACTIVE_ID = JFSI.FIGURE_ID"
					+ "   AND JFSI.FLOW_ID = ?";

			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);
			// 流程活动相关标准 7
			sql = "SELECT TMP.ID, TMP.ACTIVE_ID, TMP.STANDARD_ID, TMP.CLAUSE_ID,TMP.CRITERION_CLASS_NAME,TMP.RELA_TYPE FROM (SELECT JAS.ID, JAS.ACTIVE_ID, JAS.STANDARD_ID, JAS.CLAUSE_ID,JCC.CRITERION_CLASS_NAME,JAS.RELA_TYPE"
					+ ",CASE WHEN JCC.STAN_TYPE=1 AND JF.FILE_ID IS NULL THEN '1' ELSE '0' END AS IS_DELETE"
					+ "  FROM JECN_ACTIVE_STANDARD JAS, JECN_FLOW_STRUCTURE_IMAGE JFSI,"
					+ " JECN_CRITERION_CLASSES JCC "
					+ " LEFT JOIN JECN_FILE JF ON JF.DEL_STATE=0 AND JCC.STAN_TYPE=1 AND JCC.RELATED_ID = JF.FILE_ID"
					+ " WHERE JCC.CRITERION_CLASS_ID=JAS.STANDARD_ID "
					+ " AND JAS.ACTIVE_ID = JFSI.FIGURE_ID "
					+ " AND JFSI.FIGURE_TYPE IN "
					+ JecnCommonSql.getActiveString()
					+ " AND JFSI.FLOW_ID = ?) TMP WHERE  TMP.IS_DELETE='0'";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 8 相关附件
			sql = "SELECT FF.ID, FF.FIGURE_ID, FF.FILE_ID, FF.FIGURE_TYPE, FT.FILE_NAME"
					+ "  FROM JECN_FIGURE_FILE FF, JECN_FILE FT" + " WHERE FF.FILE_ID = FT.FILE_ID AND FT.DEL_STATE=0"
					+ "   AND EXISTS (SELECT 1" + "          FROM JECN_FLOW_STRUCTURE_IMAGE IT"
					+ "         WHERE FF.FIGURE_ID = IT.FIGURE_ID" + "           AND IT.FLOW_ID = ?)";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 9、信息化
			list = getJecnActiveOnLineTBeanById(null, processId, false);
			listResult.add(list);

			// 增加访问记录
			saveJecnUserTotal(processId, peopleId, 0);
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private void saveJecnUserTotal(Long processId, Long peopleId, int type) {
		// 增加访问记录
		JecnUserTotal userTotal = new JecnUserTotal();
		userTotal.setAccessDate(new Date());
		userTotal.setFlowId(processId);
		userTotal.setPeopleId(peopleId);
		userTotal.setType(type);
		userTotal.setAccessFrom(0);
		userTotalDao.save(userTotal);
	}

	/**
	 * 根据流程ID获取流程图元素信息
	 * 
	 * @param isStrT
	 *            "_T" 临时表
	 * @param processId
	 * @return
	 */
	private List<Object[]> getProcessObjectList(String isStrT, Long processId) {
		// 查询图形元素属性集合sql size = 36
		String sql = "SELECT F.FIGURE_ID, F.FIGURE_TYPE,F.FIGURE_TEXT, F.START_FIGURE,"
				+ "F.END_FIGURE,F.X_POINT,F.Y_POINT, F.WIDTH, F.HEIGHT,F.FONT_SIZE,F.FONT_BODY,"
				+ "F.FONT_TYPE, F.BGCOLOR, F.FONTCOLOR," + " CASE wHEN F.FIGURE_TYPE in "
				+ JecnCommonSql.getImplString()
				+ " THEN FLOW.FLOW_ID "
				+ " WHEN F.FIGURE_TYPE="
				+ JecnCommonSql.getIconString()
				+ " THEN JF.VERSION_ID "
				+ "ELSE F.LINK_FLOW_ID END AS RELATE_ID ,"
				+ " F.LINECOLOR,F.FONTPOSITION,"
				+ "F.HAVESHADOW,F.CIRCUMGYRATE,F.BODYLINE, F.BODYCOLOR, F.Z_ORDER_INDEX, F.ACTIVITY_SHOW,F.ROLE_RES,"
				+ "F.ACTIVITY_ID,F.IS_3D_EFFECT,F.FILL_EFFECTS,F.LINE_THICKNESS,F.SHADOW_COLOR,FLOW.ISFLOW AS LINK_ISFLOW,JF.VERSION_ID,"
				+ "F.ISONLINE,F.TARGET_VALUE,F.STATUS_VALUE,F.IMPL_TYPE,TDL.INSTRUCTIONS,F.ACTIVITY_INPUT,F.ACTIVITY_OUTPUT,"
				+ "F.CUSTOM_ONE,F.ELE_SHAPE"
				+ " FROM JECN_FLOW_STRUCTURE_IMAGE"
				+ isStrT
				+ " F "
				+ " LEFT JOIN JECN_FLOW_STRUCTURE_T FLOW ON FLOW.DEL_STATE = 0"
				+ " AND FLOW.FLOW_ID = F.LINK_FLOW_ID"
				+ " AND F.FIGURE_TYPE IN "
				+ JecnCommonSql.getImplString()
				+ "  AND F.IMPL_TYPE IN (1, 2, 3, 4)"
				+ " LEFT JOIN JECN_FILE"
				+ isStrT
				+ " JF ON JF.FILE_ID = F.LINK_FLOW_ID AND JF.DEL_STATE=0"
				+ " AND F.FIGURE_TYPE ="
				+ JecnCommonSql.getIconString()
				+ " LEFT JOIN TERM_DEFINITION_LIBRARY TDL ON TDL.ID = F.LINK_FLOW_ID "
				+ " AND F.FIGURE_TYPE = "
				+ JecnCommonSql.getTermFigureAR() + " WHERE F.FLOW_ID = ?";
		return flowStructureDao.listNativeSql(sql, processId);
	}

	/**
	 * 根据流程ID获取流程图元素信息
	 * 
	 * @param isStrT
	 *            "_T" 临时表
	 * @param historyId
	 * @return
	 */
	private List<Object[]> getHistoryProcessObjectList(Long processId, Long historyId) {
		// 查询图形元素属性集合sql size = 36
		String sql = "SELECT F.FIGURE_ID, F.FIGURE_TYPE,F.FIGURE_TEXT, F.START_FIGURE,"
				+ "F.END_FIGURE,F.X_POINT,F.Y_POINT, F.WIDTH, F.HEIGHT,F.FONT_SIZE,F.FONT_BODY,"
				+ "F.FONT_TYPE, F.BGCOLOR, F.FONTCOLOR,"
				+ " CASE wHEN F.FIGURE_TYPE in "
				+ JecnCommonSql.getImplString()
				+ " THEN FLOW.FLOW_ID ELSE F.LINK_FLOW_ID END AS RELATE_ID ,"
				+ " F.LINECOLOR,F.FONTPOSITION,"
				+ "F.HAVESHADOW,F.CIRCUMGYRATE,F.BODYLINE, F.BODYCOLOR, F.Z_ORDER_INDEX, F.ACTIVITY_SHOW,F.ROLE_RES,"
				+ "F.ACTIVITY_ID,F.IS_3D_EFFECT,F.FILL_EFFECTS,F.LINE_THICKNESS,F.SHADOW_COLOR,FLOW.ISFLOW AS LINK_ISFLOW,F.LINK_FLOW_ID,"
				+ "F.ISONLINE,F.TARGET_VALUE,F.STATUS_VALUE,F.IMPL_TYPE,TDL.INSTRUCTIONS,F.ACTIVITY_INPUT,F.ACTIVITY_OUTPUT,"
				+ "F.CUSTOM_ONE,F.ELE_SHAPE" + " FROM JECN_FLOW_STRUCTURE_IMAGE_H" + " F "
				+ " LEFT JOIN JECN_FLOW_STRUCTURE_T FLOW ON FLOW.DEL_STATE = 0" + " AND FLOW.FLOW_ID = F.LINK_FLOW_ID"
				+ " AND F.FIGURE_TYPE IN " + JecnCommonSql.getImplString() + "" + "  AND F.IMPL_TYPE IN (1, 2, 3, 4)"
				+ " LEFT JOIN TERM_DEFINITION_LIBRARY TDL ON TDL.ID = F.LINK_FLOW_ID " + " AND F.FIGURE_TYPE = "
				+ JecnCommonSql.getTermFigureAR() + " WHERE F.FLOW_ID = ? AND F.HISTORY_ID = " + historyId;
		return flowStructureDao.listNativeSql(sql, processId);
	}

	private List<Object[]> getProcessMapObjectList(String isStrT, Long processId) {
		// 查询图形元素属性集合sql查询图形元素属性集合sql
		String sql = getProcessMapSelectSql()
				// + // FLOW.FLOW_ID 为空就是接口流程没有发布，就不用显示；JF.FILE_ID
				// // 为IconFigure的图片不存在
				// "LINE.START_X,LINE.START_Y,LINE.END_X,LINE.END_Y"
				+ " FROM JECN_FLOW_STRUCTURE_IMAGE" + isStrT + " F "
				+ " LEFT JOIN JECN_FLOW_STRUCTURE_T  FLOW ON FLOW.DEL_STATE = 0" + " AND FLOW.FLOW_ID = F.LINK_FLOW_ID"
				+ " AND F.FIGURE_TYPE IN " + JecnCommonSql.getLinkProcessString() + " LEFT JOIN JECN_FILE" + isStrT
				+ " JF ON JF.FILE_ID = F.LINK_FLOW_ID AND JF.DEL_STATE=0" + " AND F.FIGURE_TYPE ="
				+ JecnCommonSql.getIconString() + " LEFT JOIN JECN_RULE" + isStrT
				+ " JR ON JR.DEL_STATE=0 AND JR.ID = F.LINK_FLOW_ID AND F.FIGURE_TYPE ="
				+ JecnCommonSql.getSystemString() + " WHERE F.FLOW_ID = ?";

		return flowStructureDao.listNativeSql(sql, processId);
	}

	// 文控
	private List<Object[]> getProcessHistoryMapObjectList(String isStrT, Long processId, Long historyId) {
		// 查询图形元素属性集合sql查询图形元素属性集合sql
		String sql = "SELECT F.FIGURE_ID," + "       F.FIGURE_TYPE," + "       F.FIGURE_TEXT,"
				+ "       F.START_FIGURE," + "       F.END_FIGURE," + "       F.X_POINT," + "       F.Y_POINT,"
				+ "       F.WIDTH," + "       F.HEIGHT," + "       F.FONT_SIZE," + "       F.FONT_BODY,"
				+ "       F.FONT_TYPE," + "       F.BGCOLOR," + "       F.FONTCOLOR," + "       CASE"
				+ "         WHEN F.FIGURE_TYPE = "
				+ JecnCommonSql.getSystemString()
				+ " THEN"
				+ "          JR.ID"
				+ "         WHEN F.FIGURE_TYPE = "
				+ JecnCommonSql.getIconString()
				+ " THEN"
				+ "          F.LINK_FLOW_ID"
				+ "         ELSE"
				+ "          FLOW.flow_id"
				+ "       end as relate_id,"
				+ "       F.LINECOLOR,"
				+ "       F.FONTPOSITION,"
				+ "       F.HAVESHADOW,"
				+ "       F.CIRCUMGYRATE,"
				+ "       F.BODYLINE,"
				+ "       F.BODYCOLOR,"
				+ "       F.Z_ORDER_INDEX,"
				+ "       F.ACTIVITY_SHOW,"
				+ "       F.ROLE_RES,"
				+ "       F.ACTIVITY_ID,"
				+ "       F.IS_3D_EFFECT,"
				+ "       F.FILL_EFFECTS,"
				+ "       F.LINE_THICKNESS,"
				+ "       F.SHADOW_COLOR,"
				+ "       FLOW.ISFLOW AS LINK_ISFLOW,"
				+ "       JF.ID,"
				+ "       F.ISONLINE,"
				+ "       F.DIVIDING_X,F.ELE_SHAPE"
				+ "  FROM JECN_FLOW_STRUCTURE_IMAGE_H F"
				+ "  LEFT JOIN JECN_FLOW_STRUCTURE FLOW"
				+ "    ON FLOW.DEL_STATE = 0"
				+ "   AND FLOW.FLOW_ID = F.LINK_FLOW_ID"
				+ "   AND F.FIGURE_TYPE IN "
				+ JecnCommonSql.getLinkProcessString()
				+ "  LEFT JOIN FILE_CONTENT JF"
				+ "    ON JF.ID = F.LINK_FLOW_ID"
				+ "   AND F.FIGURE_TYPE = "
				+ JecnCommonSql.getIconString()
				+ " LEFT JOIN JECN_RULE JR ON JR.DEL_STATE= 0 AND JR.ID = F.LINK_FLOW_ID AND F.FIGURE_TYPE ="
				+ JecnCommonSql.getSystemString() + " WHERE F.HISTORY_ID = " + historyId + " and f.flow_id = ?";

		return flowStructureDao.listNativeSql(sql, processId);
	}

	@Override
	@SuppressWarnings("unchecked")
	public SvgPanel getSvgBeanT(Long processId, Long peopleId, Long projectId, Long historyId, int languageType)
			throws Exception {
		try {
			// List<JecnFlowStructureImageT> images =
			// getJecnFlowStructureImageTList(processId);
			List<Object[]> list = null;
			Set<Long> participationElementIds = null;
			if (historyId == null || historyId == 0) {
				list = getProcessObjectList("_T", processId);
				// 参与的元素
				participationElementIds = participationElementIds(getParticipationElementT(processId, peopleId,
						projectId));
			} else {
				list = getHistoryProcessObjectList(processId, historyId);
				participationElementIds = participationElementIds(getParticipationElementH(processId, peopleId,
						projectId, historyId));
			}
			// 连接线对应小线段
			List<JecnLineSegmentT> mLineData = (List<JecnLineSegmentT>) flowStructureDao.listNativeAddEntity(
					getLinementSvgSqlT(historyId), JecnLineSegmentT.class, processId);
			// 添加Link信息
			List<SvgLinkIcon> linkIcons = getSvgLinkIcons(processId, true, false, historyId);
			String sql = "";
			if (historyId == null || historyId == 0) {
				// 流程图名称以及横竖标识 5
				sql = "SELECT F.FLOW_NAME, T.IS_X_OR_Y,F.FILE_PATH,F.PRE_FLOW_ID,"
						+ "(SELECT PF.ISFLOW FROM JECN_FLOW_STRUCTURE_T PF WHERE PF.FLOW_ID = F.PRE_FLOW_ID) AS PRE_ISFLOW,F.FLOW_ID_INPUT  "
						+ "FROM JECN_FLOW_STRUCTURE_T F, JECN_FLOW_BASIC_INFO_T T WHERE F.FLOW_ID=T.FLOW_ID AND F.FLOW_ID=?";
			} else {
				sql = "SELECT F.FLOW_NAME, T.IS_X_OR_Y,F.FILE_PATH,F.PRE_FLOW_ID,"
						+ "(SELECT PF.ISFLOW FROM JECN_FLOW_STRUCTURE_H PF WHERE PF.FLOW_ID = F.PRE_FLOW_ID AND F.HISTORY_ID=PF.HISTORY_ID)"
						+ " AS PRE_ISFLOW,F.FLOW_ID_INPUT  "
						+ "FROM JECN_FLOW_STRUCTURE_H F, JECN_FLOW_BASIC_INFO_H T WHERE F.FLOW_ID=T.FLOW_ID  AND F.FLOW_ID=? AND  F.HISTORY_ID=T.HISTORY_ID AND F.HISTORY_ID="
						+ historyId;
			}
			List<Object[]> listNativeSql = flowStructureDao.listNativeSql(sql, processId);

			// 信息化
			List<Object[]> listJecnActiveOnLineTBean = getJecnActiveOnLineTBeanById(historyId, processId, false);
			// 0：角色元素ID，1：关联的岗位类型（0 岗位，1：岗位组），2：岗位/岗位组名称
			List<Object[]> listPosts = getRoleRelatedPositions(processId, "_T");

			List<SvgEprosGraphData> graphDataLists = SvgDataTransformUtil.INSTANCE.getGraphDataLists(list, processId);

			SvgPanel svgPanel = new SvgPanelBuilder(graphDataLists, SvgDataTransformUtil.INSTANCE
					.getLineDatasT(mLineData), linkIcons, listNativeSql, PanelType.FLOW, participationElementIds,
					listJecnActiveOnLineTBean, listPosts).buildSvgPanel(languageType);
			// 活动时间轴
			svgPanel.addGraph(SvgSpecialGraphBuilder.buildTimeLineSvgGraph(graphDataLists));
			return svgPanel;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private List<Object[]> getRoleRelatedPositions(Long flowId, String strT) {
		String sql = "SELECT JFSI.FIGURE_ID, PS.TYPE, JFOI.FIGURE_TEXT" + "  FROM JECN_FLOW_ORG             JFO,"
				+ "       JECN_FLOW_ORG_IMAGE       JFOI," + "       PROCESS_STATION_RELATED"
				+ strT
				+ "   PS,"
				+ "       JECN_FLOW_STRUCTURE_IMAGE"
				+ strT
				+ " JFSI"
				+ " WHERE PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID"
				+ "   AND JFSI.FLOW_ID = ?"
				+ "   AND PS.FIGURE_POSITION_ID = JFOI.FIGURE_ID"
				+ "   AND PS.TYPE = '0'"
				+ "   AND JFOI.ORG_ID = JFO.ORG_ID"
				+ "   AND JFO.DEL_STATE = 0"
				+ " UNION "
				+ "SELECT JFSI.FIGURE_ID, PS.TYPE, JPG.NAME"
				+ "  FROM JECN_FLOW_STRUCTURE_IMAGE"
				+ strT
				+ "  JFSI,"
				+ "       JECN_POSITION_GROUP       JPG,"
				+ "       PROCESS_STATION_RELATED"
				+ strT
				+ "    PS"
				+ " WHERE PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID"
				+ "   AND JFSI.FLOW_ID = ?"
				+ "   AND PS.FIGURE_POSITION_ID = JPG.ID" + "   AND PS.TYPE = '1'";
		// 0：角色元素ID，1：关联的岗位类型（0 岗位，1：岗位组），2：岗位/岗位组名称
		return flowStructureDao.listNativeSql(sql, flowId, flowId);
	}

	@Override
	@SuppressWarnings("unchecked")
	public SvgPanel getSvgBean(Long processId, Long peopleId, Long projectId, int languageType) throws Exception {
		try {
			List<Object[]> list = getProcessObjectList("", processId);
			// 连接线对应小线段
			List<JecnLineSegment> mLineData = (List<JecnLineSegment>) flowStructureDao.listNativeAddEntity(
					getLinementSvgSql(), JecnLineSegment.class, processId);

			// 参与的元素
			Set<Long> participationElementIds = participationElementIds(getParticipationElement(processId, peopleId,
					projectId));

			// 添加Link信息
			List<SvgLinkIcon> linkIcons = getSvgLinkIcons(processId, true, true, null);

			// 流程图名称以及横竖标识 5
			String sql = "SELECT F.FLOW_NAME, T.IS_X_OR_Y,F.FILE_PATH,F.PRE_FLOW_ID,"
					+ "(SELECT PF.ISFLOW FROM JECN_FLOW_STRUCTURE PF WHERE PF.FLOW_ID = F.PRE_FLOW_ID) AS PRE_ISFLOW,F.FLOW_ID_INPUT  "
					+ "FROM JECN_FLOW_STRUCTURE F, JECN_FLOW_BASIC_INFO T WHERE F.FLOW_ID=T.FLOW_ID AND F.FLOW_ID=?";
			List<Object[]> listNativeSql = flowStructureDao.listNativeSql(sql, processId);

			// 活动信息化
			List<Object[]> listJecnActiveOnLineTBean = getJecnActiveOnLineTBeanById(null, processId, true);
			// 增加访问记录
			saveJecnUserTotal(processId, peopleId, 0);

			List<SvgEprosGraphData> graphDataLists = SvgDataTransformUtil.INSTANCE.getGraphDataLists(list, processId);

			// 0：角色元素ID，1：关联的岗位类型（0 岗位，1：岗位组），2：岗位/岗位组名称
			List<Object[]> listPosts = getRoleRelatedPositions(processId, "");

			SvgPanel svgPanel = new SvgPanelBuilder(graphDataLists, SvgDataTransformUtil.INSTANCE
					.getLineDatas(mLineData), linkIcons, listNativeSql, PanelType.FLOW, participationElementIds,
					listJecnActiveOnLineTBean, listPosts).buildSvgPanel(languageType);

			// 活动时间轴
			svgPanel.addGraph(SvgSpecialGraphBuilder.buildTimeLineSvgGraph(graphDataLists));
			return svgPanel;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private Set<Long> participationElementIds(List<Object[]> participationList) {
		Set<Long> set = new HashSet<Long>();
		if (participationList == null) {
			return set;
		}
		for (Object[] objects : participationList) {
			set.add(Long.valueOf(objects[0].toString()));
		}
		return set;
	}

	@Override
	@SuppressWarnings("unchecked")
	public SvgPanel getSvgMapBeanT(Long processId, Long peopleId, Long projectId, Long historyId, int languageType)
			throws Exception {
		try {
			List<Object[]> list = null;
			Set<Long> participationElementIds = null;
			// 连接线对应小线段
			List<JecnLineSegmentT> mLineData = null;
			// 查询图形元素属性集合sql查询图形元素属性集合sql
			if (historyId == null || historyId == 0) {
				list = getProcessMapObjectList("_T", processId);
				// 参与的元素
				participationElementIds = participationElementIds(getParticipationElementT(processId, peopleId,
						projectId));
				mLineData = (List<JecnLineSegmentT>) flowStructureDao.listNativeAddEntity(getLinementSvgSqlT(null),
						JecnLineSegmentT.class, processId);
			} else {
				list = getProcessHistoryMapObjectList("_H", processId, historyId);
				// 参与的元素
				participationElementIds = participationElementIds(getParticipationElementH(processId, peopleId,
						projectId, historyId));
				// 连接线对应小线段
				mLineData = (List<JecnLineSegmentT>) flowStructureDao.listNativeAddEntity(getLinementSvgSqlH(),
						JecnLineSegmentT.class, processId, historyId);
			}

			// 添加Link信息
			List<SvgLinkIcon> linkIcons = getSvgLinkIcons(processId, false, false, historyId);
			// 流程图名称以及横竖标识 5
			// 名称 3
			String sql = "";
			if (historyId == null) {
				sql = "SELECT F.FLOW_NAME,0,FILE_PATH,F.PRE_FLOW_ID,0,F.FLOW_ID_INPUT"
						+ "  FROM JECN_FLOW_STRUCTURE_T F WHERE F.FLOW_ID=?";
			} else {
				sql = "SELECT F.FLOW_NAME,0,FILE_PATH,F.PRE_FLOW_ID,0,F.FLOW_ID_INPUT"
						+ "  FROM JECN_FLOW_STRUCTURE_H F WHERE F.FLOW_ID=? AND F.HISTORY_ID = " + historyId;
			}
			List<Object[]> listNativeSql = flowStructureDao.listNativeSql(sql, processId);
			return new SvgPanelBuilder(SvgDataTransformUtil.INSTANCE.getGraphDataLists(list, processId),
					SvgDataTransformUtil.INSTANCE.getLineDatasT(mLineData), linkIcons, listNativeSql,
					PanelType.FLOW_MAP, participationElementIds, new ArrayList<Object[]>(), new ArrayList<Object[]>())
					.buildSvgPanel(languageType);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public SvgPanel getSvgMapBean(Long processId, Long peopleId, Long projectId, int languageType) throws Exception {
		try {
			// 查询图形元素属性集合sql查询图形元素属性集合sql
			List<Object[]> list = getProcessMapObjectList("", processId);
			// 参与的元素
			Set<Long> participationElementIds = participationElementIds(getParticipationElement(processId, peopleId,
					projectId));
			// 连接线对应小线段
			List<JecnLineSegment> mLineData = (List<JecnLineSegment>) flowStructureDao.listNativeAddEntity(
					getLinementSvgSql(), JecnLineSegment.class, processId);
			// 添加Link信息
			List<SvgLinkIcon> linkIcons = getSvgLinkIcons(processId, false, false, null);
			// 流程图名称以及横竖标识 5
			// 名称 3
			String sql = "SELECT F.FLOW_NAME,0,FILE_PATH,F.PRE_FLOW_ID,0,F.FLOW_ID_INPUT  FROM JECN_FLOW_STRUCTURE F WHERE F.FLOW_ID=?";
			List<Object[]> listNativeSql = flowStructureDao.listNativeSql(sql, processId);

			return new SvgPanelBuilder(SvgDataTransformUtil.INSTANCE.getGraphDataLists(list, processId),
					SvgDataTransformUtil.INSTANCE.getLineDatas(mLineData), linkIcons, listNativeSql,
					PanelType.FLOW_MAP, participationElementIds, new ArrayList<Object[]>(), new ArrayList<Object[]>())
					.buildSvgPanel(languageType);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 
	 * 获取link 的相关文件
	 * 
	 * @param processId
	 * @param isPub
	 *            true:取真实表，false：去临时表
	 * @return
	 */
	private List<SvgLinkIcon> getSvgLinkIcons(Long processId, boolean isFlow, boolean isPub, Long historyId) {
		List<SvgLinkIcon> links = new ArrayList<SvgLinkIcon>();
		if (isFlow) {

			if (!JecnConfigTool.useNewInout()) {
				// 活动输入及操作规范
				links.addAll(getLinksOfActivityFile(processId, historyId, isPub));
				// 活动的输出
				links.addAll(getLinksOfActiveOut(processId, historyId, isPub));
			} else {
				// 新版活动的输出
				links.addAll(getTermLinksOfActiveOut(processId, historyId, isPub));
				// 新版活动输入及操作规范
				links.addAll(getTermLinksOfActivityFile(processId, historyId, isPub));
			}
			// 角色的岗位
			links.addAll(getLinksOfPositions(processId, historyId, isPub));
			// 流程活动控制点
			links.addAll(getLinksOfActiveControlPoints(processId, historyId, isPub));
			// 流程活动相关标准
			links.addAll(getLinksOfActiveStandardFiles(processId, historyId, isPub));
		}
		// 8 相关附件
		links.addAll(getLinksOfFigureFile(processId, historyId, isPub));

		return links;
	}

	private List<SvgLinkIcon> getLinksOfFigureFile(Long processId, Long historyId, boolean isPub) {
		String sql = "";
		if (historyId == null || historyId == 0) {
			String table_t = "_T";
			if (isPub) {
				table_t = "";
			}
			sql = "SELECT FF.FIGURE_ID,FT.VERSION_ID FILE_ID,FT.FILE_NAME  FROM JECN_FIGURE_FILE" + table_t
					+ " FF, JECN_FILE" + table_t + " FT" + " WHERE FF.FILE_ID = FT.FILE_ID AND FT.DEL_STATE=0"
					+ "   AND EXISTS (SELECT 1" + "          FROM JECN_FLOW_STRUCTURE_IMAGE" + table_t + " IT"
					+ "         WHERE FF.FIGURE_ID = IT.FIGURE_ID" + "           AND IT.FLOW_ID = ?)";

		} else {
			sql = "SELECT FF.FIGURE_ID, FC.ID, FC.FILE_NAME" + "  FROM JECN_FIGURE_FILE_H FF"
					+ " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE_H IT"
					+ "    ON FF.FIGURE_ID = IT.FIGURE_ID AND IT.FLOW_ID = ?" + "   AND IT.HISTORY_ID =" + historyId
					+ "   AND IT.HISTORY_ID = FF.HISTORY_ID" + " INNER JOIN FILE_CONTENT FC"
					+ "    ON FF.FILE_ID = FC.ID ";
		}
		List<Object[]> list = flowStructureDao.listNativeSql(sql, processId);
		List<SvgLinkIcon> linkList = new ArrayList<SvgLinkIcon>();
		for (Object[] figureFileT : list) {
			SvgLinkIcon link = new SvgLinkIcon();
			link.setFigureId(Long.valueOf(figureFileT[0].toString()));
			if (figureFileT[1] == null) {// 文件内容不存在
				continue;
			}
			link.setFileId(figureFileT[1].toString());
			link.setFileName(figureFileT[2].toString());
			link.setIconName(IconName.FILE);
			linkList.add(link);
		}
		return linkList;
	}

	/**
	 * @param processId
	 * @return
	 */
	private List<SvgLinkIcon> getLinksOfActiveStandardFiles(Long processId, Long historyId, boolean isPub) {
		String sql = "";
		if (historyId == null || historyId == 0) {
			String table_t = "_T";
			if (isPub) {
				table_t = "";
			}
			sql = "SELECT JAS.Active_Id,JAS.STANDARD_ID,JCC.CRITERION_CLASS_NAME " + "  FROM JECN_ACTIVE_STANDARD"
					+ table_t + " JAS, JECN_FLOW_STRUCTURE_IMAGE" + table_t + " JFSI," + " JECN_CRITERION_CLASSES JCC "
					+ " LEFT JOIN JECN_FILE" + table_t
					+ " JF ON JF.DEL_STATE=0 AND JCC.STAN_TYPE=1 AND JCC.RELATED_ID=JF.FILE_ID"
					+ " WHERE JCC.CRITERION_CLASS_ID=JAS.STANDARD_ID " + " AND JAS.ACTIVE_ID = JFSI.FIGURE_ID "
					+ " AND JFSI.FIGURE_TYPE IN " + JecnCommonSql.getActiveString()
					+ " AND NOT (JCC.STAN_TYPE = 1 AND JF.FILE_ID IS NULL)" + " AND JFSI.FLOW_ID = ?";
		} else {
			sql = "SELECT JAS.Active_Id,JAS.STANDARD_ID,JCC.CRITERION_CLASS_NAME "
					+ "  FROM JECN_ACTIVE_STANDARD_H JAS, JECN_FLOW_STRUCTURE_IMAGE_H JFSI,"
					+ " JECN_CRITERION_CLASSES JCC "
					+ " LEFT JOIN JECN_FILE JF ON JF.DEL_STATE=0 AND JCC.STAN_TYPE=1 AND JCC.RELATED_ID=JF.FILE_ID"
					+ " WHERE JCC.CRITERION_CLASS_ID=JAS.STANDARD_ID " + " AND JAS.ACTIVE_ID = JFSI.FIGURE_ID "
					+ " AND JFSI.FIGURE_TYPE IN " + JecnCommonSql.getActiveString()
					+ " AND NOT (JCC.STAN_TYPE = 1 AND JF.FILE_ID IS NULL)"
					+ "  AND JFSI.FLOW_ID = ?  AND JFSI.HISTORY_ID = " + historyId + "  AND JAS.HISTORY_ID = "
					+ historyId + "  " + " AND JFSI.HISTORY_ID = " + historyId;
		}
		List<Object[]> list = flowStructureDao.listNativeSql(sql, processId);
		List<SvgLinkIcon> linkList = new ArrayList<SvgLinkIcon>();
		SvgLinkIcon link = null;
		for (Object[] activeStandardT : list) {
			link = new SvgLinkIcon();
			link.setFigureId(Long.valueOf(activeStandardT[0].toString()));
			link.setFileId(activeStandardT[1].toString());
			link.setFileName(activeStandardT[2].toString());
			link.setIconName(IconName.ACTIVE_STANDARD);
			linkList.add(link);
		}
		return linkList;
	}

	private List<SvgLinkIcon> getLinksOfActiveControlPoints(Long processId, Long historyId, boolean isPub) {
		String sql = "";
		if (historyId == null || historyId == 0) {
			String table_t = "_T";
			if (isPub) {
				table_t = "";
			}
			sql = "SELECT  JCP.ACTIVE_ID,  JCP.KEY_POINT,JCP.CONTROL_CODE,JCP.RISK_ID" + "  FROM JECN_CONTROL_POINT"
					+ table_t + " JCP, JECN_FLOW_STRUCTURE_IMAGE" + table_t + " JFSI" + " WHERE JFSI.FIGURE_TYPE IN "
					+ JecnCommonSql.getActiveString() + "   AND JCP.ACTIVE_ID = JFSI.FIGURE_ID"
					+ "   AND JFSI.FLOW_ID = ?";
		} else {
			sql = "SELECT  JCP.ACTIVE_ID,  JCP.KEY_POINT,JCP.CONTROL_CODE,JCP.RISK_ID"
					+ "  FROM JECN_CONTROL_POINT_H JCP," + " JECN_FLOW_STRUCTURE_IMAGE_H JFSI"
					+ " WHERE JFSI.FIGURE_TYPE IN " + JecnCommonSql.getActiveString()
					+ "   AND JCP.ACTIVE_ID = JFSI.FIGURE_ID" + " AND JCP.HISTORY_ID=" + historyId
					+ "  AND JFSI.FLOW_ID = ?  AND JFSI.HISTORY_ID = " + historyId;
		}
		List<Object[]> list = flowStructureDao.listNativeSql(sql, processId);
		List<SvgLinkIcon> linkList = new ArrayList<SvgLinkIcon>();
		Set<String> hasKeyPoint = new HashSet<String>();
		for (Object[] controlPointT : list) {
			if ("0".equals(controlPointT[1].toString())) {
				hasKeyPoint.add(controlPointT[0].toString());
				break;
			}
		}
		SvgLinkIcon link = null;
		for (Object[] controlPointT : list) {
			link = new SvgLinkIcon();
			link.setFigureId(Long.valueOf(controlPointT[0].toString()));
			link.setFileName(controlPointT[2].toString());
			link.setFileId(controlPointT[3].toString());
			if (hasKeyPoint.contains(controlPointT[0].toString())) {
				link.setIconName(IconName.KEY_CONTROL_POINT);
			} else {
				link.setIconName(IconName.CONTROL_POINT);
			}
			linkList.add(link);
		}
		return linkList;
	}

	private List<SvgLinkIcon> getLinksOfPositions(Long processId, Long historyId, boolean isPub) {
		String sql = "";
		if (historyId == null || historyId == 0) {
			String table_t = "_T";
			if (isPub) {
				table_t = "";
			}
			sql = "SELECT PS.FIGURE_FLOW_ID,JFSI.ACTIVITY_ID " + "    FROM JECN_FLOW_ORG        JFO, "
					+ "         JECN_FLOW_ORG_IMAGE        JFOI, " + "         PROCESS_STATION_RELATED_T    PS, "
					+ " JECN_FLOW_STRUCTURE_IMAGE" + table_t + " JFSI "
					+ "   WHERE  PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID " + " AND JFSI.FLOW_ID = ?"
					+ " AND PS.FIGURE_POSITION_ID =  JFOI.FIGURE_ID " + "     AND PS.TYPE = '0' "
					+ "     AND JFOI.ORG_ID = JFO.ORG_ID " + "     AND JFO.DEL_STATE = 0 " + " UNION "
					+ "SELECT PS.FIGURE_FLOW_ID, JFSI.ACTIVITY_ID" + "  FROM JECN_FLOW_STRUCTURE_IMAGE" + table_t
					+ " JFSI," + "       JECN_POSITION_GROUP       JPG," + "       PROCESS_STATION_RELATED" + table_t
					+ "    PS" + " WHERE PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID" + "   AND JFSI.FLOW_ID = ?"
					+ "   AND PS.FIGURE_POSITION_ID = JPG.ID" + "   AND PS.TYPE = '1'";
		} else {
			sql = "SELECT PS.FIGURE_FLOW_ID, JFSI.ACTIVITY_ID" + "  FROM JECN_FLOW_ORG               JFO,"
					+ "       JECN_FLOW_ORG_IMAGE         JFOI," + "       PROCESS_STATION_RELATED_H   PS,"
					+ "       JECN_FLOW_STRUCTURE_IMAGE_H JFSI" + " WHERE PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID"
					+ "   AND JFSI.FLOW_ID = ? AND PS.HISTORY_ID = " + historyId
					+ "   AND JFSI.HISTORY_ID = PS.HISTORY_ID" + "   AND PS.FIGURE_POSITION_ID = JFOI.FIGURE_ID"
					+ "   AND PS.TYPE = '0'" + "   AND JFOI.ORG_ID = JFO.ORG_ID" + "   AND JFO.DEL_STATE = 0"
					+ " UNION " + " SELECT PS.FIGURE_FLOW_ID, JFSI.ACTIVITY_ID"
					+ "  FROM JECN_FLOW_STRUCTURE_IMAGE_H JFSI," + "       JECN_POSITION_GROUP         JPG,"
					+ "       PROCESS_STATION_RELATED_H   PS" + " WHERE PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID"
					+ "   AND JFSI.FLOW_ID = ? AND PS.HISTORY_ID = " + historyId
					+ "   AND JFSI.HISTORY_ID = PS.HISTORY_ID" + "   AND PS.FIGURE_POSITION_ID = JPG.ID"
					+ "   AND PS.TYPE = '1'";

		}
		List<Object[]> list = flowStructureDao.listNativeSql(sql, processId, processId);
		List<SvgLinkIcon> linkList = new ArrayList<SvgLinkIcon>();
		for (Object[] objects : list) {
			SvgLinkIcon link = new SvgLinkIcon();
			link.setFigureId(Long.valueOf(objects[0].toString()));
			if ("4".equals(objects[1])) {
				link.setIconName(IconName.KEY_POSITION);
			} else {
				link.setIconName(IconName.POSITION);
			}
			linkList.add(link);
		}
		return linkList;
	}

	private List<SvgLinkIcon> getLinksOfActiveOut(Long processId, Long historyId, boolean isPub) {
		List<Object[]> list = null;
		List<Long> outFigureIds = null;
		List<SvgLinkIcon> linkList = new ArrayList<SvgLinkIcon>();
		if (historyId == null || historyId == 0) {
			String table_t = "_T";
			if (isPub) {
				table_t = "";
			}
			String sql = "SELECT JMF.FIGURE_ID,JF.FILE_NAME,JF.VERSION_ID FILE_ID  FROM JECN_MODE_FILE"
					+ table_t
					+ " JMF,JECN_FLOW_STRUCTURE_IMAGE"
					+ table_t
					+ " JFSI,JECN_FILE"
					+ table_t
					+ " JF"
					+ "       WHERE JMF.FIGURE_ID=JFSI.FIGURE_ID AND JMF.FILE_M_ID=JF.FILE_ID AND JF.DEL_STATE=0 AND JFSI.FLOW_ID=?";
			list = flowStructureDao.listNativeSql(sql, processId);
			sql = "SELECT F.FIGURE_ID FROM JECN_FLOW_STRUCTURE_IMAGE" + table_t + " F WHERE F.FLOW_ID = " + processId
					+ " AND  F.ACTIVITY_OUTPUT" + JecnCommonSql.getNullByDB();
			outFigureIds = this.flowStructureDao.listObjectNativeSql(sql, "FIGURE_ID", Hibernate.LONG);
		} else {
			String sql = "SELECT JMF.FIGURE_ID, FC.FILE_NAME, FC.ID" + "  FROM JECN_MODE_FILE_H            JMF,"
					+ "       JECN_FLOW_STRUCTURE_IMAGE_H JFSI," + "       FILE_CONTENT                FC"
					+ " WHERE JMF.FIGURE_ID = JFSI.FIGURE_ID" + "   AND JMF.FILE_M_ID = FC.ID"
					+ "   AND JMF.HISTORY_ID = ?" + "   AND JFSI.HISTORY_ID = JMF.HISTORY_ID";
			list = flowStructureDao.listNativeSql(sql, historyId);
			sql = "SELECT F.FIGURE_ID FROM JECN_FLOW_STRUCTURE_IMAGE_H F WHERE F.HISTORY_ID = " + historyId
					+ " AND  F.ACTIVITY_OUTPUT" + JecnCommonSql.getNullByDB();
			outFigureIds = this.flowStructureDao.listObjectNativeSql(sql, "FIGURE_ID", Hibernate.LONG);
		}

		for (Object[] activeOut : list) {
			SvgLinkIcon link = new SvgLinkIcon();
			link.setFigureId(Long.valueOf(activeOut[0].toString()));
			link.setFileName(activeOut[1].toString());
			if (activeOut[2] == null) {// 文件内容不存在
				continue;
			}
			link.setFileId(activeOut[2].toString());
			link.setIconName(IconName.ACTIVE_OUT);
			linkList.add(link);
		}

		addTextFigureLink(outFigureIds, linkList, 1);
		return linkList;
	}

	private List<SvgLinkIcon> getTermLinksOfActiveOut(Long processId, Long historyId, boolean isPub) {
		List<Object[]> list = null;
		List<Object[]> sampleList = null;
		List<SvgLinkIcon> linkList = new ArrayList<SvgLinkIcon>();
		if (historyId == null || historyId == 0) {
			String table_t = "_T";
			if (isPub) {
				table_t = "";
			}
			String sql = "SELECT T.ID, T.FIGURE_ID, JF.FILE_NAME, JF.VERSION_ID FILE_ID, T.NAME"
					+ "  FROM JECN_FIGURE_IN_OUT" + table_t + "  T" + " INNER JOIN JECN_FLOW_STRUCTURE" + table_t
					+ "  JFL" + "    ON T.FLOW_ID = JFL.FLOW_ID   AND T.TYPE = 1" + "  LEFT JOIN JECN_FILE" + table_t
					+ "  JF" + "    ON T.FILE_ID = JF.FILE_ID  AND JF.DEL_STATE = 0" + " WHERE T.FLOW_ID = ?"
					+ " ORDER BY T.SORT_ID";
			list = flowStructureDao.listNativeSql(sql, processId);
			sql = "select sm.in_out_id,jf.version_id file_id,jf.file_name,sm.type" + "  from jecn_figure_in_out_sample"
					+ table_t + " sm" + " inner join jecn_file" + table_t + " jf" + "    on sm.file_id = jf.file_id"
					+ "   and jf.del_state = 0  and sm.flow_id = ?  ORDER BY sm.type";
			sampleList = this.flowStructureDao.listNativeSql(sql, processId);
		} else {
			String sql = "select t.id, t.FIGURE_ID, JF.FILE_NAME, JF.ID FILE_ID, t.name"
					+ "  from jecn_figure_in_out_h t" + " inner join jecn_flow_structure_h jfl"
					+ "    on t.flow_id = jfl.flow_id" + "   and t.history_id = jfl.history_id" + "   and t.type = 1"
					+ "  left join file_content jf" + "    on t.file_id = jf.file_id" + " where jfl.flow_id = ?"
					+ "   and jfl.history_id = ?" + " order by t.sort_id";
			list = flowStructureDao.listNativeSql(sql, processId, historyId);
			sql = "select sm.in_out_id,jf.version_id file_id,jf.file_name,sm.type"
					+ "  from jecn_figure_in_out_sample_h sm" + " inner join jecn_file jf"
					+ "    on sm.file_id = jf.file_id" + "   and jf.del_state = 0" + " where sm.flow_id = ?"
					+ " and sm.history_id = ? ORDER BY sm.type";
			sampleList = this.flowStructureDao.listNativeSql(sql, processId, historyId);
		}
		List<Long> outFigureIds = new ArrayList<Long>();
		SvgLinkIcon link = null;
		for (Object[] activeOut : list) {
			link = new SvgLinkIcon();
			link.setInoutId(activeOut[0].toString());
			link.setFigureId(Long.valueOf(activeOut[1].toString()));
			outFigureIds.add(link.getFigureId());
			link.setIconName(IconName.ACTIVE_OUT);
			if (activeOut[4] != null) {
				link.setName(activeOut[4].toString());
			}
			linkList.add(link);
		}

		for (Object[] activeOut : list) {
			link = new SvgLinkIcon();
			link.setInoutId(activeOut[0].toString());
			link.setFigureId(Long.valueOf(activeOut[1].toString()));
			if (outFigureIds.contains(link.getFigureId())) {
				continue;
			}
			link.setFileId("");
			link.setFileName("");
			link.setIconName(IconName.ACTIVE_OUT_TEXT);
			if (activeOut[4] != null) {
				link.setName(activeOut[4].toString());
			}
			linkList.add(link);
		}

		addSampleLink(sampleList, linkList, 1);
		return linkList;
	}

	private List<Object[]> getJecnActiveOnLineTBeanById(Long historyId, Long processId, boolean isPub) {
		String sql = "";
		if (historyId == null || historyId == 0) {
			String table_t = "_T";
			if (isPub) {
				table_t = "";
			}
			sql = "SELECT FSI.FIGURE_ID activeId, ST.FLOW_SUSTAIN_TOOL_DESCRIBE sysName"
					+ "  FROM JECN_FLOW_STRUCTURE_IMAGE" + table_t + " FSI" + "  LEFT JOIN JECN_ACTIVE_ONLINE"
					+ table_t + " ON_T" + "    ON ON_T.ACTIVE_ID = FSI.FIGURE_ID"
					+ "  LEFT JOIN JECN_FLOW_SUSTAIN_TOOL ST" + "    ON ON_T.TOOL_ID = ST.FLOW_SUSTAIN_TOOL_ID"
					+ " WHERE ST.FLOW_SUSTAIN_TOOL_DESCRIBE IS NOT NULL" + "   AND FSI.FLOW_ID = " + processId
					+ "   ORDER BY FSI.FIGURE_ID ";
		} else {
			sql = "SELECT FSI.FIGURE_ID activeId, ST.FLOW_SUSTAIN_TOOL_DESCRIBE sysName"
					+ "  FROM JECN_FLOW_STRUCTURE_IMAGE_H FSI" + "  LEFT JOIN JECN_ACTIVE_ONLINE_H" + " ON_T"
					+ "    ON ON_T.ACTIVE_ID = FSI.FIGURE_ID AND ON_T.HISTORY_ID = FSI.HISTORY_ID  "
					+ "  LEFT JOIN JECN_FLOW_SUSTAIN_TOOL ST" + "    ON ON_T.TOOL_ID = ST.FLOW_SUSTAIN_TOOL_ID"
					+ " WHERE ST.FLOW_SUSTAIN_TOOL_DESCRIBE IS NOT NULL" + "  AND FSI.FLOW_ID = " + processId
					+ "  AND FSI.HISTORY_ID = " + historyId + "   ORDER BY FSI.FIGURE_ID ";
		}
		return this.flowStructureDao.listNativeSql(sql);
	}

	/**
	 * 获取活动输入和操作规范的SVGLink
	 * 
	 * @param processId
	 * @return
	 */
	private List<SvgLinkIcon> getLinksOfActivityFile(Long processId, Long histroyId, boolean isPub) {
		List<Object[]> list = null;
		List<Long> inputFigureIds = null;
		if (histroyId == null || histroyId == 0) {
			String table_t = "_T";
			if (isPub) {
				table_t = "";
			}
			// 活动的输入和操作规范
			String sql = "SELECT JAF.FIGURE_ID,JAF.FILE_TYPE,JF.VERSION_ID FILE_ID,JF.FILE_NAME"
					+ "       FROM JECN_FLOW_STRUCTURE_IMAGE"
					+ table_t
					+ " JFSI,JECN_ACTIVITY_FILE"
					+ table_t
					+ " JAF,JECN_FILE"
					+ table_t
					+ " JF"
					+ "       WHERE JAF.FIGURE_ID=JFSI.FIGURE_ID AND JAF.FILE_S_ID = JF.FILE_ID AND JF.DEL_STATE=0 AND JFSI.FLOW_ID = ?";
			list = flowStructureDao.listNativeSql(sql, processId);

			sql = "SELECT F.FIGURE_ID FROM JECN_FLOW_STRUCTURE_IMAGE" + table_t + " F WHERE F.FLOW_ID = " + processId
					+ " AND F.ACTIVITY_INPUT" + JecnCommonSql.getNullByDB();
			inputFigureIds = this.flowStructureDao.listObjectNativeSql(sql, "FIGURE_ID", Hibernate.LONG);
		} else {
			String sql = "SELECT JAF.FIGURE_ID, JAF.FILE_TYPE, FC.ID FILE_ID, FC.FILE_NAME"
					+ "  FROM JECN_FLOW_STRUCTURE_IMAGE_H JFSI," + "       JECN_ACTIVITY_FILE_H        JAF,"
					+ "       FILE_CONTENT                FC" + " WHERE JAF.FIGURE_ID = JFSI.FIGURE_ID"
					+ "   AND JAF.FILE_S_ID = FC.ID" + "  AND JFSI.FLOW_ID = ? AND JFSI.HISTORY_ID = ? "
					+ "   AND JAF.HISTORY_ID = JFSI.HISTORY_ID" + "   AND JFSI.HISTORY_ID = JAF.HISTORY_ID";
			list = flowStructureDao.listNativeSql(sql, processId, histroyId);
			sql = "SELECT F.FIGURE_ID FROM JECN_FLOW_STRUCTURE_IMAGE_H F WHERE F.FLOW_ID =" + processId
					+ "  AND F.HISTORY_ID = " + histroyId + " AND F.ACTIVITY_INPUT" + JecnCommonSql.getNullByDB();
			inputFigureIds = this.flowStructureDao.listObjectNativeSql(sql, "FIGURE_ID", Hibernate.LONG);
		}

		List<SvgLinkIcon> linkList = new ArrayList<SvgLinkIcon>();
		List<SvgLinkIcon> linkInList = new ArrayList<SvgLinkIcon>();
		SvgLinkIcon link = null;
		for (Object[] activityFileT : list) {
			link = new SvgLinkIcon();
			link.setFigureId(Long.valueOf(activityFileT[0].toString()));
			if (activityFileT[1].toString().equals("0")) {
				link.setIconName(IconName.ACTIVE_IN);
			} else {
				link.setIconName(IconName.FILE);
			}
			if (activityFileT[2] == null) {// 文件内容不存在
				continue;
			}
			link.setFileId(activityFileT[2].toString());
			link.setFileName(activityFileT[3].toString());
			linkList.add(link);
			if (activityFileT[1].toString().equals("0")) {
				linkInList.add(link);
			}
		}

		addTextFigureLinkIn(inputFigureIds, linkList, linkInList);

		return linkList;
	}

	/**
	 * 获取新版活动输入和操作规范的SVGLink
	 * 
	 * @param processId
	 * @return
	 */
	private List<SvgLinkIcon> getTermLinksOfActivityFile(Long processId, Long histroyId, boolean isPub) {
		List<Object[]> list = null;
		List<Object[]> sampleList = null;
		if (histroyId == null || histroyId == 0) {
			String table_t = "_T";
			if (isPub) {
				table_t = "";
			}
			// 活动的输入和操作规范
			String sql = "select * from (SELECT '0' AS ID,JF.VERSION_ID FILE_ID, JF.FILE_NAME, JAF.FIGURE_ID, JAF.FILE_TYPE, '' name,  0 sort_id"
					+ "  FROM JECN_FLOW_STRUCTURE_IMAGE"
					+ table_t
					+ " JFSI, JECN_ACTIVITY_FILE"
					+ table_t
					+ " JAF, JECN_FILE"
					+ table_t
					+ " JF"
					+ " WHERE JAF.FIGURE_ID = JFSI.FIGURE_ID"
					+ "   AND JAF.FILE_S_ID = JF.FILE_ID"
					+ "   AND JF.DEL_STATE = 0"
					+ "   and jaf.file_type = 1"
					+ "   AND JFSI.FLOW_ID = ?"
					+ " union"
					+ " SELECT   INOUT.ID,"
					+ "       JF.VERSION_ID FILE_ID,"
					+ "       JF.FILE_NAME,"
					+ " INOUT.FIGURE_ID,"
					+ "       INOUT.TYPE,"
					+ "       INOUT.NAME,"
					+ "       INOUT.SORT_ID"
					+ "  FROM JECN_FLOW_STRUCTURE"
					+ table_t
					+ " JFSI"
					+ " INNER JOIN JECN_FIGURE_IN_OUT"
					+ table_t
					+ " INOUT"
					+ "    ON JFSI.FLOW_ID = INOUT.FLOW_ID"
					+ "   AND INOUT.TYPE = 0"
					+ "  LEFT JOIN JECN_FILE"
					+ table_t
					+ " JF"
					+ "    ON INOUT.FILE_ID = JF.FILE_ID"
					+ "   AND JF.DEL_STATE = 0" + " where jfsi.flow_id = ? )t order by t.sort_id";

			list = flowStructureDao.listNativeSql(sql, processId, processId);
			sql = "select sm.in_out_id,jf.version_id file_id,jf.file_name,sm.type" + "  from jecn_figure_in_out_sample"
					+ table_t + " sm" + " inner join jecn_file" + table_t + " jf" + "    on sm.file_id = jf.file_id"
					+ "   and jf.del_state = 0  and sm.flow_id = ?  and sm.type = 0";
			sampleList = this.flowStructureDao.listNativeSql(sql, processId);
		} else {
			String sql = "select * from ( SELECT '0' AS ID, JF.ID FILE_ID, JF.FILE_NAME,JAF.FIGURE_ID, JAF.FILE_TYPE, '' name,  0 sort_id"
					+ "  FROM JECN_FLOW_STRUCTURE_IMAGE_H JFSI,"
					+ "       JECN_ACTIVITY_FILE_H        JAF,"
					+ "       FILE_CONTENT                JF"
					+ " WHERE JAF.FIGURE_ID = JFSI.FIGURE_ID"
					+ "   AND JAF.FILE_S_ID = JF.id"
					+ "  and jaf.history_id = jfsi.history_id"
					+ "   and jaf.file_type = 1"
					+ "   and jaf.history_id = jfsi.history_id"
					+ "   AND JFSI.FLOW_ID = ?"
					+ "   AND jfsi.history_id = ?"
					+ "	union"
					+ "	select  INOUT.ID,JF.ID FILE_ID, JF.FILE_NAME,inout.FIGURE_ID, inout.TYPE, inout.name,  inout.sort_id"
					+ "  from Jecn_Flow_Structure_H JFSI"
					+ "   inner join  Jecn_Figure_In_Out_H  inout"
					+ "   on inout.history_id = jfsi.history_id"
					+ "   and jfsi.flow_id = ?"
					+ "   left join    FILE_CONTENT        jf on   inout.file_id = jf.file_id"
					+ " where jfsi.flow_id = inout.flow_id"
					+ "     AND inout.type = 0"
					+ "   AND jfsi.history_id = ? )t order by t.sort_id";
			list = flowStructureDao.listNativeSql(sql, processId, histroyId, processId, histroyId);
			sql = "select sm.in_out_id,jf.version_id file_id,jf.file_name,sm.type"
					+ "  from jecn_figure_in_out_sample_h sm" + " inner join jecn_file jf"
					+ "    on sm.file_id = jf.file_id" + "   and jf.del_state = 0" + " where sm.flow_id = ?"
					+ " and sm.history_id = ? and sm.type = 0";
			sampleList = this.flowStructureDao.listNativeSql(sql, processId, histroyId);

		}

		List<SvgLinkIcon> linkList = new ArrayList<SvgLinkIcon>();
		SvgLinkIcon link = null;
		List<Long> inFigureIds = new ArrayList<Long>();
		for (Object[] activityFileT : list) {
			link = new SvgLinkIcon();
			link.setInoutId(activityFileT[0].toString());
			link.setFigureId(Long.valueOf(activityFileT[3].toString()));
			inFigureIds.add(link.getFigureId());
			if (activityFileT[5] != null) {
				link.setName(activityFileT[5].toString());
			}
			if (activityFileT[4].toString().equals("0")) {// 输入
				link.setIconName(IconName.ACTIVE_IN);
			} else {
				link.setFileId(activityFileT[1].toString());
				link.setFileName(activityFileT[2].toString());
				link.setIconName(IconName.FILE);
			}

			linkList.add(link);
		}

		for (Object[] activityFileT : list) {
			link = new SvgLinkIcon();
			link.setInoutId(activityFileT[0].toString());
			link.setFigureId(Long.valueOf(activityFileT[3].toString()));

			if (inFigureIds.contains(link.getFigureId())) {// 存在模板
				continue;
			}
			inFigureIds.add(link.getFigureId());

			if (activityFileT[5] != null) {
				link.setName(activityFileT[5].toString());
			}
			link.setFileId("");
			link.setFileName("");
			link.setIconName(IconName.ACTIVE_IN_TEXT);

			linkList.add(link);
		}
		addSampleLink(sampleList, linkList, 0);
		return linkList;
	}

	private void addTextFigureLinkIn(List<Long> textFigureIds, List<SvgLinkIcon> linkList, List<SvgLinkIcon> linkInList) {
		if (textFigureIds.size() == 0 || !JecnConfigTool.isShowActiveTextPutLogo()) {
			return;
		}
		// 获取只存在文本输入的
		List<SvgLinkIcon> linkTextList = new ArrayList<SvgLinkIcon>();
		SvgLinkIcon link = null;
		for (Long figureId : textFigureIds) {
			boolean isExists = false;
			for (SvgLinkIcon svgLinkIcon : linkInList) {
				if (svgLinkIcon.getFigureId().toString().equals(figureId.toString())) {
					isExists = true;
					break;
				}
			}

			if (!isExists) {
				link = new SvgLinkIcon();
				link.setFigureId(figureId);
				link.setIconName(IconName.ACTIVE_IN);
				link.setFileId("");
				link.setFileName("");
				linkTextList.add(link);
			}
		}
		if (linkTextList.size() > 0) {
			linkList.addAll(linkTextList);
		}
	}

	private void addTextFigureLink(List<Long> textFigureIds, List<SvgLinkIcon> linkList, int type) {
		if (textFigureIds.size() == 0 || !JecnConfigTool.isShowActiveTextPutLogo()) {
			return;
		}
		// 获取只存在文本输入的
		List<SvgLinkIcon> linkTextList = new ArrayList<SvgLinkIcon>();
		SvgLinkIcon link = null;
		for (Long figureId : textFigureIds) {
			boolean isExists = false;
			for (SvgLinkIcon svgLinkIcon : linkList) {
				if (svgLinkIcon.getFigureId().toString().equals(figureId.toString())) {
					isExists = true;
					break;
				}
			}

			if (!isExists) {
				link = new SvgLinkIcon();
				link.setFigureId(figureId);
				if (type == 0) {
					link.setIconName(IconName.ACTIVE_IN);
				} else {
					link.setIconName(IconName.ACTIVE_OUT);
				}
				link.setFileId("");
				link.setFileName("");
				linkTextList.add(link);
			}
		}
		if (linkTextList.size() > 0) {
			linkList.addAll(linkTextList);
		}
	}

	/**
	 * 
	 * @param textFigureIds
	 * @param linkList
	 * @param type
	 *            0 ：输入；1 ：输出
	 */
	private void addSampleLink(List<Object[]> textFigureIds, List<SvgLinkIcon> linkList, int type) {
		for (SvgLinkIcon svgLinkIcon : linkList) {
			if (IconName.ACTIVE_IN_TEXT.equals(svgLinkIcon.getIconName())
					|| IconName.ACTIVE_OUT_TEXT.equals(svgLinkIcon.getIconName())) {
				continue;
			}
			List<JecnFigureInoutSampleT> smaList = new ArrayList<JecnFigureInoutSampleT>();
			for (Object[] data : textFigureIds) {
				String inoutId = data[0].toString();
				String fileId = data[1].toString();
				String fileName = data[2].toString();
				// 文件类型 0 是模板 1 是样例
				int fileType = Integer.valueOf(data[3].toString());
				// IconName.ACTIVE_IN_TEXT
				if (svgLinkIcon.getInoutId().equals(inoutId)) {
					JecnFigureInoutSampleT smap = new JecnFigureInoutSampleT();
					smap.setFileId(Long.valueOf(fileId));
					smap.setFileName(fileName);
					smap.setType(fileType);
					smap.setInoutId(inoutId);
					smaList.add(smap);
				}
			}
			svgLinkIcon.setSample(smaList);
		}
		// 拼接新的输入输出状态 linkList 因为输入输出 查询的是整个流程图的 需要 确定 属于哪个活动 活动的 输入还是输出
		// 所以单独提出方法重新赋值。
		newTermInoutType(linkList, type);
	}

	private void newTermInoutType(List<SvgLinkIcon> linkList, int type) {
		List<Long> ids = new ArrayList<Long>();
		for (SvgLinkIcon svgLinkIcon : linkList) {
			if (ids.contains(svgLinkIcon.getFigureId())) {
				continue;
			}
			if (svgLinkIcon.getSample() != null && !svgLinkIcon.getSample().isEmpty()) {
				for (JecnFigureInoutSampleT smapBean : svgLinkIcon.getSample()) {
					if (smapBean.getType() == 0) {
						Long fId = svgLinkIcon.getFigureId();
						for (SvgLinkIcon svgLinkIcon2 : linkList) {
							if (fId.equals(svgLinkIcon2.getFigureId())) {
								ids.add(svgLinkIcon2.getFigureId());
								if (type == 0 && IconName.ACTIVE_IN.equals(svgLinkIcon2.getIconName())) {// 0
									// ：输入；1
									// ：输出
									svgLinkIcon2.setIconName(IconName.ACTIVE_FILE_IN);
								} else if (type == 1 && IconName.ACTIVE_OUT.equals(svgLinkIcon2.getIconName())) {
									svgLinkIcon2.setIconName(IconName.ACTIVE_FILE_OUT);
								}
								svgLinkIcon2.setUseNewInout(true);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * 查询流程图数据
	 * 
	 * @param processId
	 *            流程图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	@Override
	public List<List<Object[]>> getPartMapT(Long processId, Long peopleId, Long projectId) throws Exception {
		try {
			List<List<Object[]>> listResult = new ArrayList<List<Object[]>>();
			// 查询图形元素属性集合sql查询图形元素属性集合sql
			List<Object[]> list = getProcessObjectList("_T", processId);
			listResult.add(list);

			// 查询连接线对应小线段sql
			String sql = getLinementSqlT();
			// 连接线对应小线段
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 活动的输入和操作规范
			sql = "SELECT JAF.FIGURE_ID,JAF.FILE_TYPE,JF.FILE_ID,JF.FILE_NAME"
					+ "       FROM JECN_FLOW_STRUCTURE_IMAGE_T JFSI,JECN_ACTIVITY_FILE_T JAF,JECN_FILE_T JF"
					+ "       WHERE JAF.FIGURE_ID=JFSI.FIGURE_ID AND JAF.FILE_S_ID = JF.FILE_ID AND JF.DEL_STATE=0 AND JFSI.FLOW_ID = ?";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);
			// 活动的输出
			sql = "SELECT JMF.FIGURE_ID,JF.FILE_ID,JF.FILE_NAME FROM JECN_MODE_FILE_T JMF,JECN_FLOW_STRUCTURE_IMAGE_T JFSI,JECN_FILE_T JF"
					+ "       WHERE JMF.FIGURE_ID=JFSI.FIGURE_ID AND JMF.FILE_M_ID=JF.FILE_ID AND JF.DEL_STATE=0 AND JFSI.FLOW_ID=?";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);
			// 角色的岗位
			sql = "SELECT PS.FIGURE_FLOW_ID,PS.TYPE, " + "       PS.FIGURE_POSITION_ID,JFOI.FIGURE_TEXT,JFO.Org_Name "
					+ "    FROM JECN_FLOW_ORG              JFO, " + "         JECN_FLOW_ORG_IMAGE        JFOI, "
					+ "         PROCESS_STATION_RELATED_T    PS, " + " JECN_FLOW_STRUCTURE_IMAGE_T JFSI "
					+ "   WHERE  PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID " + " AND JFSI.FLOW_ID = ?"
					+ " AND PS.FIGURE_POSITION_ID =  JFOI.FIGURE_ID " + "     AND PS.TYPE = '0' "
					+ "     AND JFOI.ORG_ID = JFO.ORG_ID " + "     AND JFO.DEL_STATE = 0 " + " UNION "
					+ " SELECT PS.FIGURE_FLOW_ID ,PS.TYPE, " + " PS.FIGURE_POSITION_ID,JPG.NAME,NULL " + "    FROM "
					+ " JECN_FLOW_STRUCTURE_IMAGE_T JFSI, " + " JECN_POSITION_GROUP   JPG, "
					+ " JECN_FLOW_ORG              JFO, " + "  JECN_FLOW_ORG_IMAGE        JFOI, "
					+ "  PROCESS_STATION_RELATED_T    PS, " + "  JECN_POSITION_GROUP_R      JPGR "
					+ "  WHERE PS.FIGURE_FLOW_ID = JFSI.FIGURE_ID AND JFSI.FLOW_ID = ? "
					+ " AND JPG.ID = JPGR.GROUP_ID " + " AND PS.FIGURE_POSITION_ID = JPGR.GROUP_ID "
					+ " AND PS.TYPE = '1' " + " AND JPGR.FIGURE_ID =  JFOI.FIGURE_ID "
					+ " AND JFOI.ORG_ID = JFO.ORG_ID " + "  AND JFO.DEL_STATE = 0";
			list = flowStructureDao.listNativeSql(sql, processId, processId);
			listResult.add(list);

			// 流程图名称以及横竖标识 5
			sql = "SELECT F.FLOW_NAME, T.IS_X_OR_Y,F.FILE_PATH,F.PRE_FLOW_ID,"
					+ "(SELECT PF.ISFLOW FROM JECN_FLOW_STRUCTURE PF WHERE PF.FLOW_ID = F.PRE_FLOW_ID) AS PRE_ISFLOW "
					+ "  FROM JECN_FLOW_STRUCTURE_T F, JECN_FLOW_BASIC_INFO_T T WHERE F.FLOW_ID=T.FLOW_ID AND F.FLOW_ID=?";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 流程活动控制点 6
			sql = "SELECT JCP.ID," + "       JCP.ACTIVE_ID," + "       JCP.CONTROL_CODE," + "       JCP.KEY_POINT,"
					+ "       JCP.FREQUENCY," + "       JCP.METHOD," + "       JCP.TYPE," + "       JCP.RISK_ID,"
					+ "       JCP.TARGET_ID," + "       JCP.CREATE_PERSON," + "       JCP.CREATE_TIME,"
					+ "       JCP.UPDATE_PERSON," + "       JCP.UPDATE_TIME," + "       JCP.NOTE"
					+ "  FROM JECN_CONTROL_POINT_T JCP, JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
					+ " WHERE JFSI.FIGURE_TYPE IN " + JecnCommonSql.getActiveString()
					+ "   AND JCP.ACTIVE_ID = JFSI.FIGURE_ID" + "   AND JFSI.FLOW_ID = ?";

			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);
			// 流程活动相关标准 7
			sql = "SELECT TMP.ID, TMP.ACTIVE_ID, TMP.STANDARD_ID,TMP.CLAUSE_ID,TMP.CRITERION_CLASS_NAME,TMP.RELA_TYPE"
					+ " FROM(SELECT JAS.ID, JAS.ACTIVE_ID, JAS.STANDARD_ID, JAS.CLAUSE_ID,JCC.CRITERION_CLASS_NAME,JAS.RELA_TYPE"
					+ ",CASE WHEN JCC.STAN_TYPE=1 AND JF.FILE_ID IS NULL THEN '1' ELSE '0' END AS IS_DELETE"
					+ "  FROM JECN_ACTIVE_STANDARD_T JAS, JECN_FLOW_STRUCTURE_IMAGE_T JFSI,"
					+ " JECN_CRITERION_CLASSES JCC "
					+ " LEFT JOIN JECN_FILE_T JF ON JF.DEL_STATE=0 AND JCC.STAN_TYPE=1 AND JCC.RELATED_ID=JF.FILE_ID"
					+ " WHERE JCC.CRITERION_CLASS_ID=JAS.STANDARD_ID " + " AND JAS.ACTIVE_ID = JFSI.FIGURE_ID "
					+ " AND JFSI.FIGURE_TYPE IN " + JecnCommonSql.getActiveString()
					+ " AND JFSI.FLOW_ID = ?) TMP where TMP.IS_DELETE='0'";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 8 相关附件
			sql = "SELECT FF.ID, FF.FIGURE_ID, FF.FILE_ID, FF.FIGURE_TYPE, FT.FILE_NAME"
					+ "  FROM JECN_FIGURE_FILE_T FF, JECN_FILE FT"
					+ " WHERE FF.FILE_ID = FT.FILE_ID AND FT.DEL_STATE=0" + "   AND EXISTS (SELECT 1"
					+ "          FROM JECN_FLOW_STRUCTURE_IMAGE_T IT" + "         WHERE FF.FIGURE_ID = IT.FIGURE_ID"
					+ "           AND IT.FLOW_ID = ?)";
			list = flowStructureDao.listNativeSql(sql, processId);
			listResult.add(list);

			// 9、信息化
			list = getJecnActiveOnLineTBeanById(null, processId, false);
			listResult.add(list);

			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 
	 * 查询组织图数据
	 * 
	 * @param processId
	 *            流程地图主表主键ID
	 * @param peopleId
	 *            人员ID
	 * @param projectId
	 *            当前项目ID
	 * @return List<List<Object[]>>
	 * @throws Exception
	 */
	@Override
	public List<List<Object[]>> getOrgMap(Long processId, Long peopleId, Long projectId) throws Exception {
		List<List<Object[]>> listResult = new ArrayList<List<Object[]>>();
		String sql = "SELECT F.FIGURE_ID, F.FIGURE_TYPE,F.FIGURE_TEXT, F.START_FIGURE,"
				+ "F.END_FIGURE,F.X_POINT,F.Y_POINT, F.WIDTH, F.HEIGHT,F.FONT_SIZE,F.FONT_BODY,"
				+ "F.FONT_TYPE, F.BGCOLOR, F.FONTCOLOR, F.LINK_ORG_ID, F.LINECOLOR,F.FONTPOSITION,"
				+ "F.HAVESHADOW,F.CIRCUMGYRATE,F.BODYLINE, F.BODYCOLOR, F.Z_ORDER_INDEX, null,null,"
				+ "null,F.IS_3D_EFFECT,F.FILL_EFFECTS,F.LINE_THICKNESS,F.SHADOW_COLOR,null,JF.FILE_ID"
				+ " FROM (SELECT * FROM JECN_FLOW_ORG_IMAGE TT WHERE TT.ORG_ID = ?) F "
				+ " LEFT JOIN JECN_FILE JF ON JF.FILE_ID = F.LINK_ORG_ID AND JF.DEL_STATE=0" + " AND F.FIGURE_TYPE ="
				+ JecnCommonSql.getIconString();
		List<Object[]> list = flowStructureDao.listNativeSql(sql, processId);
		listResult.add(list);
		sql = "SELECT LINE.FIGURE_ID, LINE. START_X, LINE.START_Y, LINE.END_X, LINE.END_Y"
				+ "  FROM JECN_FLOW_ORG_IMAGE F, JECN_LINE_SEGMENT_DEP LINE" + " WHERE LINE.FIGURE_ID = F.FIGURE_ID"
				+ "   AND F.FIGURE_TYPE IN " + JecnCommonSql.getLineString() + "   AND F.ORG_ID = ?";
		list = flowStructureDao.listNativeSql(sql, processId);
		listResult.add(list);
		return listResult;
	}

	@Override
	@SuppressWarnings("unchecked")
	public SvgPanel getSvgOrg(Long id, Long peopleId, long projectId, int languageType) {
		String sql = "SELECT F.*" + " FROM (SELECT * FROM JECN_FLOW_ORG_IMAGE TT WHERE TT.ORG_ID = ?) F "
				+ " LEFT JOIN JECN_FILE JF ON JF.FILE_ID = F.LINK_ORG_ID AND JF.DEL_STATE=0" + " AND F.FIGURE_TYPE ="
				+ JecnCommonSql.getIconString();
		List<JecnFlowOrgImage> images = (List<JecnFlowOrgImage>) flowStructureDao.listNativeAddEntity(sql,
				JecnFlowOrgImage.class, id);
		sql = "SELECT LINE.*" + "  FROM JECN_FLOW_ORG_IMAGE F, JECN_LINE_SEGMENT_DEP LINE"
				+ " WHERE LINE.FIGURE_ID = F.FIGURE_ID" + "   AND F.FIGURE_TYPE IN " + JecnCommonSql.getLineString()
				+ "   AND F.ORG_ID = ?";
		List<JecnLineSegmentDep> lines = (List<JecnLineSegmentDep>) flowStructureDao.listNativeAddEntity(sql,
				JecnLineSegmentDep.class, id);
		// 获取图标
		getOrgImageVersion(id, images);
		return new SvgPanelBuilder(SvgDataTransformUtil.INSTANCE.getGraphDataListsOrg(images),
				SvgDataTransformUtil.INSTANCE.getLineDatasOrg(lines), new ArrayList<SvgLinkIcon>(),
				new ArrayList<Object[]>(), PanelType.DEPT, new HashSet<Long>(), new ArrayList<Object[]>(),
				new ArrayList<Object[]>()).buildSvgPanel(languageType);
	}
	
	private void getOrgImageVersion(Long id,List<JecnFlowOrgImage> images){
		// 获取存在的logo图标
		String sql = "SELECT OI.FIGURE_ID, FT.VERSION_ID" +
			"  FROM JECN_FLOW_ORG O" + 
			" INNER JOIN JECN_FLOW_ORG_IMAGE OI ON O.ORG_ID = OI.ORG_ID" + 
			"                                  and oi.figure_type = 'IconFigure'" + 
			" INNER JOIN JECN_FILE_T FT ON FT.FILE_ID = OI.LINK_ORG_ID" + 
			" WHERE O.ORG_ID = ?";
		List<Object[]> list = flowStructureDao.listNativeSql(sql, id);
		
		if(list == null){
			return;
		}
		for (Object[] objects : list) {
			Long figureId= Long.valueOf(objects[0].toString());
			Long versionId= Long.valueOf(objects[1].toString());
			for (JecnFlowOrgImage orgImage : images) {
				if(orgImage.getFigureId().intValue() == figureId.intValue()){
					orgImage.setLinkOrgId(versionId);
				}
			}
		}
	}

	/**
	 * 流程架构- 集成关系图
	 * 
	 * @param processId
	 * @param peopleId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	@Override
	public SvgPanel getSvgIntegrationChartBeanT(Long processId, Long peopleId, Long projectId, Long historyId,
			int languageType) throws Exception {
		try {

			List<Object[]> list = null;
			if (historyId == null || historyId == 0) {
				// 查询图形元素属性集合sql查询图形元素属性集合sql
				list = getIntegrationChartObjectList("_T", processId);
			} else {
				// 查询图形元素属性集合sql查询图形元素属性集合sql
				list = getHistoryIntegrationChartObjectList(processId, historyId);
			}

			// 连接线对应小线段
			List<JecnLineSegmentT> mLineData = findIntegrationChartLineTById(processId, historyId);
			// 流程图名称以及横竖标识 5
			// 名称 3
			String sql = "";
			if (historyId == null || historyId == 0) {
				sql = "SELECT F.FLOW_NAME,0,FILE_PATH,F.PRE_FLOW_ID,0,F.FLOW_ID_INPUT  "
						+ "FROM JECN_FLOW_STRUCTURE_T F WHERE F.FLOW_ID=?";
			} else {
				sql = "SELECT F.FLOW_NAME,0,FILE_PATH,F.PRE_FLOW_ID,0,F.FLOW_ID_INPUT  "
						+ "FROM JECN_FLOW_STRUCTURE_H F WHERE F.FLOW_ID=? AND F.HISTORY_ID=" + historyId;
			}
			List<Object[]> listNativeSql = flowStructureDao.listNativeSql(sql, processId);
			return new SvgPanelBuilder(SvgDataTransformUtil.INSTANCE.getGraphDataLists(list, processId),
					SvgDataTransformUtil.INSTANCE.getLineDatasT(mLineData), new ArrayList<SvgLinkIcon>(),
					listNativeSql, PanelType.INTEGRATION_CHART, new HashSet<Long>(), new ArrayList<Object[]>(),
					new ArrayList<Object[]>()).buildSvgPanel(languageType);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public SvgPanel getSvgIntegrationChartBean(Long processId, Long peopleId, Long projectId, int languageType)
			throws Exception {
		try {
			// 查询图形元素属性集合sql查询图形元素属性集合sql
			List<Object[]> list = getIntegrationChartObjectList("", processId);
			// 连接线对应小线段
			List<JecnLineSegment> mLineData = findIntegrationChartLineById(processId);
			// 名称 3
			String sql = "SELECT F.FLOW_NAME,0,FILE_PATH,F.PRE_FLOW_ID,0,F.FLOW_ID_INPUT  FROM JECN_FLOW_STRUCTURE F WHERE F.FLOW_ID=?";
			List<Object[]> listNativeSql = flowStructureDao.listNativeSql(sql, processId);
			// 增加访问记录
			saveJecnUserTotal(processId, peopleId, 0);

			return new SvgPanelBuilder(SvgDataTransformUtil.INSTANCE.getGraphDataLists(list, processId),
					SvgDataTransformUtil.INSTANCE.getLineDatas(mLineData), new ArrayList<SvgLinkIcon>(), listNativeSql,
					PanelType.INTEGRATION_CHART, new HashSet<Long>(), new ArrayList<Object[]>(),
					new ArrayList<Object[]>()).buildSvgPanel(languageType);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 流程架构- 集成关系图-连接线
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	private List<JecnLineSegmentT> findIntegrationChartLineTById(Long flowId, Long historyId) throws Exception {
		if (historyId == null || historyId == 0) {
			String hql = "select a from JecnMapLineSegmentT as a,JecnMapRelatedStructureImageT as b where a.figureId = b.figureId and b.flowId=?";
			List<JecnMapLineSegmentT> jecnMapLineSegmentTs = flowStructureDao.listHql(hql, flowId);
			return getLineSegmentTByCopyProperties(jecnMapLineSegmentTs);
		} else {
			String sql = "select a.id,a.figure_id,a.start_x,a.start_y,a.end_x,a.end_y from JECN_MAP_RELATED_LINE_H a, JECN_MAP_STRUCTURE_IMAGE_H b"
					+ " where a.FIGURE_ID = b.FIGURE_ID"
					+ "   and b.flow_id = ?"
					+ "   and a.history_id = b.history_id" + "   and a.history_id = ?";
			List<Object[]> list = flowStructureDao.listNativeSql(sql, flowId, historyId);
			List<JecnLineSegmentT> listJecnLineSegmentT = new ArrayList<JecnLineSegmentT>();
			for (Object[] obj : list) {
				if (obj == null || obj[0] == null || obj[1] == null) {
					continue;
				}
				JecnLineSegmentT jecnLineSegmentT = new JecnLineSegmentT();
				jecnLineSegmentT.setId(Long.valueOf(obj[0].toString()));
				jecnLineSegmentT.setFigureId(Long.valueOf(obj[1].toString()));
				if (obj[2] != null) {
					jecnLineSegmentT.setStartX(Long.valueOf(obj[2].toString()));
				} else {
					jecnLineSegmentT.setStartX(null);
				}
				if (obj[3] != null) {
					jecnLineSegmentT.setStartY(Long.valueOf(obj[3].toString()));
				} else {
					jecnLineSegmentT.setStartY(null);
				}
				if (obj[4] != null) {
					jecnLineSegmentT.setEndX(Long.valueOf(obj[4].toString()));
				} else {
					jecnLineSegmentT.setEndX(null);
				}
				if (obj[5] != null) {
					jecnLineSegmentT.setEndY(Long.valueOf(obj[5].toString()));
				} else {
					jecnLineSegmentT.setEndY(null);
				}
				listJecnLineSegmentT.add(jecnLineSegmentT);
			}

			return listJecnLineSegmentT;
		}
	}

	/**
	 * 流程架构- 集成关系图-连接线
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	private List<JecnLineSegment> findIntegrationChartLineById(Long flowId) throws Exception {
		String hql = "select a from JecnMapLineSegment as a,JecnMapRelatedStructureImage as b where a.figureId = b.figureId and b.flowId=?";
		List<JecnMapLineSegment> jecnMapLineSegmentTs = flowStructureDao.listHql(hql, flowId);
		return getLineSegmentByCopyProperties(jecnMapLineSegmentTs);
	}

	private List<JecnLineSegmentT> getLineSegmentTByCopyProperties(List<JecnMapLineSegmentT> jecnMapLineSegmentTs)
			throws Exception {
		List<JecnLineSegmentT> listJecnLineSegmentT = new ArrayList<JecnLineSegmentT>();
		JecnLineSegmentT lineSegmentT = null;
		for (JecnMapLineSegmentT jecnMapLineSegmentT : jecnMapLineSegmentTs) {
			lineSegmentT = new JecnLineSegmentT();
			// 复制属性
			PropertyUtils.copyProperties(lineSegmentT, jecnMapLineSegmentT);
			listJecnLineSegmentT.add(lineSegmentT);
		}
		return listJecnLineSegmentT;
	}

	private List<JecnLineSegment> getLineSegmentByCopyProperties(List<JecnMapLineSegment> jecnMapLineSegmentTs)
			throws Exception {
		List<JecnLineSegment> listJecnLineSegmentT = new ArrayList<JecnLineSegment>();
		JecnLineSegment lineSegment = null;
		for (JecnMapLineSegment jecnMapLineSegmentT : jecnMapLineSegmentTs) {
			lineSegment = new JecnLineSegment();
			// 复制属性
			PropertyUtils.copyProperties(lineSegment, jecnMapLineSegmentT);
			listJecnLineSegmentT.add(lineSegment);
		}
		return listJecnLineSegmentT;
	}

	/**
	 * 流程架构- 集成关系图
	 * 
	 * @param isStrT
	 * @param processId
	 * @return
	 */
	private List<Object[]> getIntegrationChartObjectList(String isStrT, Long processId) {
		// 查询图形元素属性集合sql查询图形元素属性集合sql
		String sql = getProcessMapSelectSql()
				// + // FLOW.FLOW_ID 为空就是接口流程没有发布，就不用显示；JF.FILE_ID
				// // 为IconFigure的图片不存在
				// "LINE.START_X,LINE.START_Y,LINE.END_X,LINE.END_Y"
				+ " FROM (SELECT * FROM JECN_MAP_STRUCTURE_IMAGE" + isStrT + " TT WHERE TT.FLOW_ID = ?) F "
				+ " LEFT JOIN JECN_FLOW_STRUCTURE_T  FLOW ON FLOW.DEL_STATE = 0" + " AND FLOW.FLOW_ID = F.LINK_FLOW_ID"
				+ " AND F.FIGURE_TYPE <>" + JecnCommonSql.getSystemString() + " LEFT JOIN JECN_FILE" + isStrT
				+ " JF ON JF.FILE_ID = F.LINK_FLOW_ID AND JF.DEL_STATE=0" + " AND F.FIGURE_TYPE ="
				+ JecnCommonSql.getIconString();

		return flowStructureDao.listNativeSql(sql, processId);
	}

	private String getProcessMapSelectSql() {
		return "SELECT F.FIGURE_ID, F.FIGURE_TYPE,F.FIGURE_TEXT, F.START_FIGURE,"
				+ "F.END_FIGURE,F.X_POINT,F.Y_POINT, F.WIDTH, F.HEIGHT,F.FONT_SIZE,F.FONT_BODY,"
				+ "F.FONT_TYPE, F.BGCOLOR, F.FONTCOLOR, CASE WHEN F.FIGURE_TYPE=" + JecnCommonSql.getSystemString()
				+ " THEN F.LINK_FLOW_ID" + " WHEN F.FIGURE_TYPE=" + JecnCommonSql.getIconString()
				+ " THEN JF.VERSION_ID ELSE FLOW.flow_id end as relate_id, F.LINECOLOR,F.FONTPOSITION,"
				+ "F.HAVESHADOW,F.CIRCUMGYRATE,F.BODYLINE, F.BODYCOLOR, F.Z_ORDER_INDEX, F.ACTIVITY_SHOW,F.ROLE_RES,"
				+ "F.ACTIVITY_ID,F.IS_3D_EFFECT,F.FILL_EFFECTS,F.LINE_THICKNESS,F.SHADOW_COLOR,"
				+ "FLOW.ISFLOW AS LINK_ISFLOW,JF.VERSION_ID,F.ISONLINE,F.DIVIDING_X,F.ELE_SHAPE";
	}

	/**
	 * 流程架构- 集成关系图 文控
	 * 
	 * @param isStrT
	 * @param processId
	 * @return
	 */
	private List<Object[]> getHistoryIntegrationChartObjectList(Long processId, Long historyId) {
		// 查询图形元素属性集合sql查询图形元素属性集合sql
		String sql = "SELECT F.FIGURE_ID, F.FIGURE_TYPE,F.FIGURE_TEXT, F.START_FIGURE,"
				+ "F.END_FIGURE,F.X_POINT,F.Y_POINT, F.WIDTH, F.HEIGHT,F.FONT_SIZE,F.FONT_BODY,"
				+ "F.FONT_TYPE, F.BGCOLOR, F.FONTCOLOR, CASE WHEN F.FIGURE_TYPE="
				+ JecnCommonSql.getSystemString()
				+ " THEN F.LINK_FLOW_ID"
				+ " WHEN F.FIGURE_TYPE="
				+ JecnCommonSql.getIconString()
				+ " THEN F.LINK_FLOW_ID ELSE FLOW.flow_id end as relate_id, F.LINECOLOR,F.FONTPOSITION,"
				+ "F.HAVESHADOW,F.CIRCUMGYRATE,F.BODYLINE, F.BODYCOLOR, F.Z_ORDER_INDEX, F.ACTIVITY_SHOW,F.ROLE_RES,"
				+ "F.ACTIVITY_ID,F.IS_3D_EFFECT,F.FILL_EFFECTS,F.LINE_THICKNESS,F.SHADOW_COLOR,"
				+ "FLOW.ISFLOW AS LINK_ISFLOW,JF.ID,F.ISONLINE,F.DIVIDING_X,F.ELE_SHAPE"
				// + // FLOW.FLOW_ID 为空就是接口流程没有发布，就不用显示；JF.FILE_ID
				// // 为IconFigure的图片不存在
				// "LINE.START_X,LINE.START_Y,LINE.END_X,LINE.END_Y"
				+ " FROM (SELECT * FROM JECN_MAP_STRUCTURE_IMAGE_H TT WHERE TT.FLOW_ID = ? AND TT.HISTORY_ID = "
				+ historyId + ") F " + " LEFT JOIN JECN_FLOW_STRUCTURE_H  FLOW ON FLOW.DEL_STATE = 0"
				+ " AND FLOW.FLOW_ID = F.LINK_FLOW_ID AND FLOW.HISTORY_ID = F.HISTORY_ID " + " AND F.FIGURE_TYPE <>"
				+ JecnCommonSql.getSystemString() + " LEFT JOIN FILE_CONTENT" + " JF ON JF.ID = F.LINK_FLOW_ID "
				+ " AND F.FIGURE_TYPE =" + JecnCommonSql.getIconString();
		return flowStructureDao.listNativeSql(sql, processId);
	}

	/**
	 * 
	 * 查询连接线对应小线段sql
	 * 
	 * @return String sql
	 */
	private String getLinementSql() {
		// 查询连接线对应小线段sql
		String sql = "SELECT LINE.FIGURE_ID, LINE. START_X, LINE.START_Y, LINE.END_X, LINE.END_Y"
				+ "  FROM JECN_FLOW_STRUCTURE_IMAGE F, JECN_LINE_SEGMENT LINE" + " WHERE LINE.FIGURE_ID = F.FIGURE_ID"
				+ "   AND F.FIGURE_TYPE IN " + JecnCommonSql.getLineString() + "   AND F.FLOW_ID = ?";
		return sql;
	}

	/**
	 * 
	 * 查询连接线对应小线段sql
	 * 
	 * @return String sql
	 */
	private String getLinementSqlT() {
		// 查询连接线对应小线段sql
		String sql = "SELECT LINE.FIGURE_ID, LINE. START_X, LINE.START_Y, LINE.END_X, LINE.END_Y"
				+ "  FROM JECN_FLOW_STRUCTURE_IMAGE_T F, JECN_LINE_SEGMENT_T LINE"
				+ " WHERE LINE.FIGURE_ID = F.FIGURE_ID" + "   AND F.FIGURE_TYPE IN " + JecnCommonSql.getLineString()
				+ "   AND F.FLOW_ID = ?";
		return sql;
	}

	/**
	 * 
	 * 查询连接线对应小线段sql
	 * 
	 * @return String sql
	 */
	private String getLinementSvgSql() {
		// 查询连接线对应小线段sql
		String sql = "SELECT LINE.*" + "  FROM JECN_FLOW_STRUCTURE_IMAGE F, JECN_LINE_SEGMENT LINE"
				+ " WHERE LINE.FIGURE_ID = F.FIGURE_ID" + "   AND F.FIGURE_TYPE IN " + JecnCommonSql.getLineString()
				+ "   AND F.FLOW_ID = ?";
		return sql;
	}

	/**
	 * 
	 * 查询连接线对应小线段sql
	 * 
	 * @return String sql
	 */
	private String getLinementSvgSqlT(Long historyId) {
		String sql = "";
		if (historyId == null || historyId == 0) {
			// 查询连接线对应小线段sql
			sql = "SELECT LINE.*" + "  FROM JECN_FLOW_STRUCTURE_IMAGE_T F, JECN_LINE_SEGMENT_T LINE"
					+ " WHERE LINE.FIGURE_ID = F.FIGURE_ID" + "   AND F.FIGURE_TYPE IN "
					+ JecnCommonSql.getLineString() + "   AND F.FLOW_ID = ?";
		} else {
			sql = "SELECT LINE.*" + "  FROM JECN_FLOW_STRUCTURE_IMAGE_H F" + ", JECN_LINE_SEGMENT_H LINE"
					+ " WHERE LINE.FIGURE_ID = F.FIGURE_ID AND F.HISTORY_ID = LINE.HISTORY_ID"
					+ "   AND F.FIGURE_TYPE IN " + JecnCommonSql.getLineString()
					+ "  AND F.FLOW_ID = ? AND F.HISTORY_ID = " + historyId;
		}
		return sql;
	}

	/**
	 * 
	 * 查询连接线对应小线段sql
	 * 
	 * @return String sql
	 */
	private String getLinementSvgSqlH() {
		// 查询连接线对应小线段sql
		String sql = "SELECT LINE.*" + "  FROM JECN_FLOW_STRUCTURE_IMAGE_H F, JECN_LINE_SEGMENT_H LINE"
				+ " WHERE LINE.FIGURE_ID = F.FIGURE_ID"
				+ "  AND F.HISTORY_ID = LINE.HISTORY_ID   AND F.FIGURE_TYPE IN " + JecnCommonSql.getLineString()
				+ "   AND F.FLOW_ID = ? AND F.HISTORY_ID = ?";
		return sql;
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	public IJecnUserTotalDao getUserTotalDao() {
		return userTotalDao;
	}

	public void setUserTotalDao(IJecnUserTotalDao userTotalDao) {
		this.userTotalDao = userTotalDao;
	}

	public IPositoinGroupDao getPositionGroupDao() {
		return positionGroupDao;
	}

	public void setPositionGroupDao(IPositoinGroupDao positionGroupDao) {
		this.positionGroupDao = positionGroupDao;
	}

}
