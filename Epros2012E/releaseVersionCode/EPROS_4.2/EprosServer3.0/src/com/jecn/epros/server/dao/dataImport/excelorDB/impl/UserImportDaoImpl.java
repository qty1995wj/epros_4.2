package com.jecn.epros.server.dao.dataImport.excelorDB.impl;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.impl.SessionImpl;

import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.bean.dataimport.JecnBaseRelPosBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.dataImport.excelorDB.IUserImportDao;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncSqlConstant;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnPosGroup;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnPosGroupRelatedPos;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpOriDept;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.PosBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class UserImportDaoImpl extends AbsBaseDao<DeptBean, Long> implements IUserImportDao {
	private static final Logger log = Logger.getLogger(UserImportDaoImpl.class);

	/**
	 * 
	 * 获取通用部门临时表(TMP_JECN_IO_DEPT)项目ID集合
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> selectProjectId() throws Exception {
		log.info("查询上次导入项目ID：通用部门临时表(TMP_JECN_IO_DEPT)数据开始 ");
		// 执行查询
		Query query = this.getSession().createSQLQuery(SyncSqlConstant.SELECT_TMP_JECN_IO_DEPT_PROJECT_ID).addScalar(
				"PROJECTID", Hibernate.LONG);
		List<Long> ret = query.list();
		// 执行查询
		return ret;
	}

	/**
	 * 
	 * 清除临时库、入库临时库
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	@Override
	public void deleteAndInsertTmpJecnIOTable(BaseBean baseBean) throws Exception {
		if (baseBean == null || baseBean.isNull()) {
			return;
		}
		// 获取Session
		Session s = this.getSession();
		log.info("删除临时表数据并且添加数据开始 ");

		// 删除部门
		Query query = s.createSQLQuery(SyncSqlConstant.DELETE_TMP_JECN_IO_DEPT_SQL);
		query.executeUpdate();

		// 删除岗位
		query = s.createSQLQuery(SyncSqlConstant.DELETE_TMP_JECN_IO_POS_SQL);
		query.executeUpdate();

		// 删除人员
		query = s.createSQLQuery(SyncSqlConstant.DELETE_TMP_JECN_IO_USER);
		query.executeUpdate();

		// 删除岗位组
		query = s.createSQLQuery("DELETE FROM TMP_JECN_POS_GROUP");
		query.executeUpdate();

		// 删除岗位组相关岗位
		query = s.createSQLQuery("DELETE FROM TMP_JECN_POSGROUP_POS");
		query.executeUpdate();

		boolean isSyncUserOnly = JecnConfigTool.isSyncUserOnly();

		if (!isSyncUserOnly) {
			// 插入部门
			for (DeptBean deptBean : baseBean.getDeptBeanList()) {
				s.save(deptBean);
			}
			// 插入岗位
			for (PosBean posBean : baseBean.getPosBeanList()) {
				s.save(posBean);
			}

			// 插入岗位组
			for (JecnPosGroup posGroup : baseBean.getPosGroups()) {
				s.save(posGroup);
			}

			// 插入岗位组相关岗位
			for (JecnPosGroupRelatedPos groupRelatedPos : baseBean.getGroupRelatedPos()) {
				s.save(groupRelatedPos);
			}
		}

		// 插入人员
		for (UserBean userBean : baseBean.getUserBeanList()) {
			if (StringUtils.isBlank(userBean.getUserNum())) {
				continue;
			}
			s.save(userBean);
		}
	}

	/**
	 * 
	 * 清除原始数据临时卡，并添加新数据
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	@Override
	public void deleteAndInsertTmpOriJecnIOTable(BaseBean baseBean) throws Exception {
		if (baseBean == null || baseBean.isNull()) {
			return;
		}
		// 获取Session
		Session s = this.getSession();
		log.info("删除临时原始表数据并且添加数据开始 ");

		// 删除原始部门
		String sql = "delete from TMP_JECN_ORI_DEPT";
		Query query = s.createSQLQuery(sql);
		query.executeUpdate();

		// 删除原始人员
		sql = "delete from TMP_JECN_ORI_USER";
		query = s.createSQLQuery(sql);
		query.executeUpdate();

		// 删除岗位组

		// 加入计数器，记录存储顺序
		int sort = 0;
		// 插入部门
		for (DeptBean deptBean : baseBean.getDeptBeanList()) {
			JecnTmpOriDept dept = new JecnTmpOriDept();
			dept.setDeptNum(deptBean.getDeptNum());
			dept.setDeptName(deptBean.getDeptName());
			dept.setError(deptBean.getError());
			dept.setPerDeptNum(deptBean.getPerDeptNum());
			dept.setSort(sort);
			s.save(dept);
			sort++;
			dept = null;
		}
		sort = 0;
		log.info("插入原始人员岗位信息开始！" + System.currentTimeMillis());
		// 插入人员
		JecnTmpPosAndUserBean andUserBean = null;
		for (UserBean userBean : baseBean.getUserPosBeanList()) {
			andUserBean = new JecnTmpPosAndUserBean();
			if (StringUtils.isBlank(userBean.getUserNum())) {
				continue;
			}
			andUserBean.setUserNum(userBean.getUserNum());
			andUserBean.setTrueName(userBean.getTrueName());
			andUserBean.setPosNum(userBean.getPosNum());
			andUserBean.setPosName(userBean.getPosName());
			andUserBean.setDeptNum(userBean.getDeptNum());
			andUserBean.setEmail(userBean.getEmail());
			andUserBean.setEmailType(userBean.getEmailType());
			andUserBean.setError(userBean.getError());
			andUserBean.setPhone(userBean.getPhone());
			andUserBean.setSort(sort);
			andUserBean.setDomainName(userBean.getDomainName());
			s.save(andUserBean);
			sort++;
		}
		log.info("插入原始人员岗位信息结束！" + System.currentTimeMillis());
	}

	/**
	 * 
	 * 通用部门临时表(TMP_JECN_IO_DEPT)：同一部门下部门名称不能重复
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<DeptBean> checkDeptNameSame() throws Exception {
		log.info("同一部门下部门名称不能重复：通用部门临时表(TMP_JECN_IO_DEPT)数据开始 ");
		// 执行查询
		Query query = this.getSession().createSQLQuery(SyncSqlConstant.SELECT_DEPT_NAME_SAME_PDEPT).addEntity(
				DeptBean.class);
		return query.list();
	}

	/**
	 * 
	 * 待导入部门、岗位以及人员中在各自真实库中存在的数据， 更新其操作标识设置为1
	 * 
	 * @throws Exception
	 */
	@Override
	public void UpdateProFlag(Long projectId, boolean isNotExistsDept) throws Exception {
		log.info("4.与真实库数据做验证数据开始 ");

		// 获取Session以便数据库操作
		Session s = this.getSession();

		if (!isNotExistsDept) {
			// --------------- 执行更新
			// 部门
			Query query = s.createSQLQuery(SyncSqlConstant.UPDATE_PRO_FLAG_DEPT);
			query.setLong("PROJECTID", projectId);
			query.executeUpdate();
			// 岗位
			query = s.createSQLQuery(SyncSqlConstant.UPDATE_PRO_FLAG_POS);
			query.setLong("PROJECTID", projectId);
			query.executeUpdate();

			// 岗位组
			String sql = "";
			if (DBType.ORACLE.equals(getDBDType())) {
				sql = SyncSqlConstant.UPDATE_TMP_JECN_POS_GROUP_ORACLE;
			} else if (DBType.SQLSERVER.equals(getDBDType())) {
				sql = SyncSqlConstant.UPDATE_TMP_JECN_POS_GROUP_SQLSERVER;
			}
			query = s.createSQLQuery(sql);
			query.executeUpdate();

			if (DBType.ORACLE.equals(getDBDType())) {
				sql = SyncSqlConstant.UPDATE_TMP_JECN_POSGROUP_POS_ORACLE;
			} else if (DBType.SQLSERVER.equals(getDBDType())) {
				sql = SyncSqlConstant.UPDATE_TMP_JECN_POSGROUP_POS_SQLSERVER;
			}
			// 岗位组相关岗位
			query = s.createSQLQuery(sql);
			query.executeUpdate();

		}
		// 人员
		Query query = s.createSQLQuery(SyncSqlConstant.UPDATE_PRO_FLAG_USER);
		query.executeUpdate();

		// --------------- 执行更新
		log.info("4.与真实库数据做验证数据结束 ");
	}

	/**
	 * 
	 * 插入部门临时表中标识是插入的部门
	 * 
	 * @param s
	 * @throws Exception
	 */
	@Override
	public void insertDept(Long projectId) throws Exception {
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_JECN_FLOW_ORG_DEPT_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_JECN_FLOW_ORG_DEPT_SQLSERVER;
		} else {
			throw new SQLDataException("连接数据库不是oracle或者SQL server");
		}

		log.info("插入组织/部门表(JECN_FLOW_ORG)数据开始 ");
		Query query = this.getSession().createSQLQuery(sql);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
		log.info("插入组织/部门表(JECN_FLOW_ORG)数据结束 ");

	}

	/**
	 * 插入岗位组
	 * 
	 * @param projectId
	 * @throws Exception
	 */
	@Override
	public void insertPosGroup(Long projectId) throws Exception {
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_POSGROUP_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_POSGROUP_SQLSERVER;
		} else {
			throw new SQLDataException("连接数据库不是oracle或者SQL server");
		}

		log.info("插入岗位组数据开始 ");
		Query query = this.getSession().createSQLQuery(sql);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
		log.info("插入岗位组数据结束 ");

	}

	/**
	 * 插入岗位组和岗位关联关系
	 * 
	 * @param projectId
	 * @throws Exception
	 */
	@Override
	public void insertPosGroupRelatedPos() throws Exception {
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_POSGROUP_RELATED_POS_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_POSGROUP_RELATED_POS_SQLSERVER;
		} else {
			throw new SQLDataException("连接数据库不是oracle或者SQL server");
		}

		log.info("插入岗位组和岗位关联关系开始 ");
		this.execteNative(sql);
		log.info("插入岗位组和岗位关联关系结束 ");

	}

	/**
	 * 
	 * 插入部门临时表中标识是插入的岗位
	 * 
	 * @param s
	 *            StatelessSession
	 * @throws Exception
	 */
	@Override
	public void insertPos(Long projectId) throws Exception {
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_JECN_FLOW_ORG_IMAGE_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_JECN_FLOW_ORG_IMAGE_SQLSERVER;
		} else {
			throw new SQLDataException("连接数据库不是oracle或者SQL server");
		}

		log.info("插入岗位元素表(JECN_FLOW_ORG_IMAGE)数据开始 ");
		Query query = getSession().createSQLQuery(sql);
		// 元素类型
		query.setString("FIGURE_TYPE", "PositionFigure");
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		log.info("插入岗位元素表(JECN_FLOW_ORG_IMAGE)数据结束 ");
	}

	/**
	 * 
	 * 插入通用人员临时表中标识是插入的人员
	 * 
	 * @param s
	 *            StatelessSession
	 * @throws Exception
	 */
	@Override
	public void insertUser() throws Exception {

		// 插入人员临时表中标识是插入的人员
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_JECN_USER_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_JECN_USER_SQLSERVER;
		} else {
			throw new SQLDataException("连接数据库不是oracle或者SQL server");
		}

		log.info("插入真实人员表(JECN_USER)数据开始 ");
		Query query = this.getSession().createSQLQuery(sql);
		// 邮箱内网标识
		query.setInteger("EMAIL_INNER", SyncConstant.EMAIL_TYPE_INNER_INT);
		// 邮箱外网标识
		query.setInteger("EMAIL_OUTER", SyncConstant.EMAIL_TYPE_OUTER_INT);
		query.setString("PASSWORD", SyncConstant.PASSWORD);
		query.executeUpdate();
		log.info("插入真实人员表(JECN_USER)数据结束 ");

	}

	/**
	 * 
	 * 插入通用人员临时表中标识是插入的人员
	 * 
	 * @param s
	 *            StatelessSession
	 * @throws Exception
	 */
	@Override
	public void insertUserPosRef(Long projectId) throws Exception {
		// 插入人员临时表中标识是插入的人员
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_JECN_USER_POSITION_RELATED_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.INSERT_JECN_USER_POSITION_RELATED_SQLSERVER;
		} else {
			throw new SQLDataException("连接数据库不是oracle或者SQL server");
		}

		log.info("插入人员岗位关联表(JECN_USER_POSITION_RELATED)数据开始 ");

		Query query = this.getSession().createSQLQuery(sql);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		log.info("插入人员岗位关联表(JECN_USER_POSITION_RELATED)数据结束 ");

	}

	/**
	 * 
	 * 更新岗位变更，查询需要删除的部门为A，删除A下的岗位数据以及更新或删除岗位相关联的表中字段值
	 * 
	 * @param s
	 */
	@Override
	public void updatePosUpAndDeletePos(Long projectId) throws Exception {
		log.info("更新岗位变更，查询需要删除的部门为A，删除A下的岗位数据以及更新或删除岗位相关联的表中字段值开始 ");

		Session s = this.getSession();
		// 更新【变更岗位】对应的岗位数据
		// Query query = s
		// .createSQLQuery(SyncSqlConstant.DELETE_VIEW_FLOW_ORG_DEPT);
		// query.setLong("PROJECTID", projectId);
		// query.executeUpdate();

		// 查询需要删除的部门为A，删除A下的岗位数据以及更新或删除岗位相关联的表中字段值 start
		// 岗位基本属性（JECN_POSITION_FIG_INFO） 删除
		Query query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_POSITION_FIG_INFO_DEL_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 岗位与岗位组关系表（JECN_POSITION_GROUP_R） 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_POSITION_GROUP_R_DEL_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		/**
		 * *********************【流程基本信息表更新开始】***********************************
		 * ********
		 */
		// 流程基本信息（JECN_FLOW_BASIC_INFO） 责任人类型（TYPE_RES_PEOPLE）==0 更新
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_BASIC_INFO_DEL_DEPT);
		query.executeUpdate();
		// 流程基本信息临时表(JECN_FLOW_BASIC_INFO_T) 责任人类型（TYPE_RES_PEOPLE）==0 更新
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_BASIC_INFO_T_DEL_DEPT);
		query.executeUpdate();

		// 流程基本信息(JECN_FLOW_BASIC_INFO) 流程监护人 更新
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_BASC_FICTION_PEOPLE_ID);
		query.executeUpdate();

		// 流程基本信息(JECN_FLOW_BASIC_INFO) 流程拟制人 更新
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_BASC_WARD_PEOPLE_ID);
		query.executeUpdate();

		// 流程基本信息临时表(JECN_FLOW_BASIC_INFO_T) 流程监护人 更新
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_BASC_T_FICTION_PEOPLE_ID);
		query.executeUpdate();

		// 流程基本信息临时表(JECN_FLOW_BASIC_INFO_T) 流程拟制人 更新
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_BASC_T_WARD_PEOPLE_ID);
		query.executeUpdate();

		/**
		 * *********************【流程基本信息表更新结束】***********************************
		 * ********
		 */

		// 流程角色与岗位关联表（JECN_PROCESS_STATION_RELATED）0：岗位 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_PROCESS_STATION_RELATED_DEL_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 流程角色与岗位关联临时表（JECN_PROCESS_STATION_RELATED_T）0：岗位 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_PROCESS_STATION_RELATED_T_DEL_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 岗位与制度关联关系表（JECN_POSITION_REF_SYSTEM） 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_POSITION_REF_SYSTEM_DEL_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 岗位表（JECN_FLOW_ORG_IMAGE） 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_FLOW_ORG_IMAGE_DEL_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
		// 查询需要删除的部门为A，删除A下的岗位数据以及更新或删除岗位相关联的表中字段值 end
		log.info("更新岗位变更，查询需要删除的部门为A，删除A下的岗位数据以及更新或删除岗位相关联的表中字段值结束 ");
	}

	/**
	 * 
	 * 删除部门以及相关
	 * 
	 */
	@Override
	public void deleteDept(Long projectId) throws Exception {
		log.info("删除真实部门表中部门，此部门是在临时部门表中不存在开始 ");
		Session s = this.getSession();

		// 1,2,5表不存在
		// --------------- 执行删除、更新
		// 流程与部门关联表(JECN_VIEW_FLOW_ORG) 删除 1
		// Query query = s
		// .createSQLQuery(SyncSqlConstant.DELETE_VIEW_FLOW_ORG_DEPT);
		// query.setLong("PROJECTID", projectId);
		// query.executeUpdate();
		// 流程与部门关联临时表(JECN_VIEW_FLOW_ORG_T) 删除 2
		// Query query = s
		// .createSQLQuery(SyncSqlConstant.DELETE_VIEW_FLOW_ORG_T_DEPT);
		// query.setLong("PROJECTID", projectId);
		// query.executeUpdate();
		// 流程责任部门(JECN_FLOW_RELATED_ORG) 删除 3
		Query query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_FLOW_RELATED_ORG_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
		// 流程责任部门临时表(JECN_FLOW_RELATED_ORG_T) 删除 4
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_FLOW_RELATED_ORG_T_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 制度责任部门 设置为空 JECN_RULE_T
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.UPDATE_RULE_T_REF_DEPT_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.UPDATE_RULE_T_REF_DEPT_SQLSERVER;
		}
		query = s.createSQLQuery(sql);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
		// 制度责任部门 设置为空 JECN_RULE
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.UPDATE_RULE_REF_DEPT_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.UPDATE_RULE_REF_DEPT_SQLSERVER;
		}
		query = s.createSQLQuery(sql);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 部门查阅权限关联表（JECN_ORG_ACCESS_PERMISSIONS） 删除 5
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_ORG_ACCESS_PERMISSIONS);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
		// 部门查阅权限关联表临时表（JECN_ORG_ACCESS_PERM_T） 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_ORG_ACCESS_PERM_T);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 文件表(JECN_FILE) 更新：部门ID(ORG_ID)=NULL 6
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FILE_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 文件临时表(JECN_FILE_T) 更新：部门ID(ORG_ID)=NULL
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FILE_T_DEPT);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 部门表（JECN_FLOW_ORG） 删除 7 -
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_FLOW_ORG);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
		// --------------- 执行删除、更新
		log.info("删除真实部门表中部门，此部门是在临时部门表中不存在结束 ");
	}

	/**
	 * 
	 * 删除岗位以及相关 除了人员与岗位关联关系
	 * 
	 */
	@Override
	public void deletePos(Long projectId) throws Exception {
		log.info("删除真实岗位表中岗位，此岗位是在临时岗位表中不存在开始 ");
		Session s = this.getSession();
		// --------------- 执行删除、更新

		// 岗位与岗位组关系表（JECN_POSITION_GROUP_R） 删除
		Query query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_POSITION_GROUP_R);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 流程基本信息（JECN_FLOW_BASIC_INFO） 责任人类型（TYPE_RES_PEOPLE）==1 更新
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_BASIC_INFO_1);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 流程基本信息临时表(JECN_FLOW_BASIC_INFO_T) 责任人类型（TYPE_RES_PEOPLE）==1 更新
		query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_BASIC_INFO_T_1);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 流程角色与岗位关联表（JECN_PROCESS_STATION_RELATED）0：岗位 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_PROCESS_STATION_RELATED);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 流程角色与岗位关联临时表（JECN_PROCESS_STATION_RELATED_T）0：岗位 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_PROCESS_STATION_RELATED_T);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 岗位与制度关联关系表（JECN_POSITION_REF_SYSTEM） 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_POSITION_REF_SYSTEM);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 岗位查阅权限 JECN_ACCESS_PERMISSIONS 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_POS_ACCESS_PERM);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
		// 岗位查阅权限 JECN_ACCESS_PERMISSIONS_T 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_POS_ACCESS_PERM_T);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 岗位表（JECN_FLOW_ORG_IMAGE） 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_FLOW_ORG_IMAGE);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// 岗位基本属性（JECN_POSITION_FIG_INFO） 删除
		query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_POSITION_FIG_INFO);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
		// --------------- 执行删除、更新
		log.info("删除真实岗位表中岗位，此岗位是在临时岗位表中不存在结束 ");
	}

	/**
	 * 
	 * 删除人员以及相关 除了人员与岗位关联关系
	 * 
	 */
	@Override
	public void deleteUser(Long projectId) throws Exception {
		// Session s = this.getSession();
		// // 人员角色关联表（JECN_USER_ROLE）:删除 PEOPLE_ID=人员表主键ID
		// Query query =
		// s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_USER_ISLOCK);
		// query.executeUpdate();

		// // --------------- 执行删除、更新
		// log.info("删除人员以及相关 除了人员与岗位关联关系开始 ");
		// s = this.getSession();
		// // 人员角色关联表（JECN_USER_ROLE）:删除 PEOPLE_ID=人员表主键ID
		// query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_USER_ROLE);
		// query.executeUpdate();
		//
		// // 流程结构表（JECN_FLOW_STRUCTURE）:更新人员ID（PEOPLE_ID）字段为NULL
		// query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_STRUCTURE);
		// query.executeUpdate();
		//
		// // 流程结构临时表(JECN_FLOW_STRUCTURE_T):更新人员ID（PEOPLE_ID）字段为NULL
		// query =
		// s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FLOW_STRUCTURE_T);
		// query.executeUpdate();
		//
		// // 文件表(JECN_FILE) 更新:人员ID(PEOPLE_ID)=NULL
		// query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FILE);
		// query.executeUpdate();
		//
		// // 文件临时表表(JECN_FILE_T) 更新:人员ID(PEOPLE_ID)=NULL
		// query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_FILE_T);
		// query.executeUpdate();
		//
		// // 信息表(JECN_MESSAGE) 更新:人员ID(PEOPLE_ID)=NULL
		// query = s.createSQLQuery(SyncSqlConstant.UPDATE_JECN_MESSAGE_USER);
		// query.executeUpdate();
		//
		// // 人员表（JECN_USER）：删除
		// query = s.createSQLQuery(SyncSqlConstant.DELETE_JECN_USER);
		// query.executeUpdate();
		// log.info("删除人员以及相关 除了人员与岗位关联关系结束 ");
		// --------------- 执行删除、更新
	}

	/**
	 * 
	 * 删除人员与岗位关联关系以及相关表
	 * 
	 */
	@Override
	public void deleteUserPosRef(Long projectId) throws Exception {
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.DELETE_JECN_USER_POSITION_RELATED_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.DELETE_JECN_USER_POSITION_RELATED_SQLSERVER;
		} else {
			return;
		}
		// --------------- 执行删除、更新
		// --人员岗位关联表（JECN_USER_POSITION_RELATED）删除
		Query query = this.getSession().createSQLQuery(sql);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();

		// --------------- 执行删除、更新
	}

	/**
	 * 
	 * 更新部门名称、PID
	 * 
	 */
	@Override
	public void updateNameAndPidByDept(Long projectId) throws Exception {
		// 执行更新
		Query query = this.getSession().createSQLQuery(SyncSqlConstant.UPDATE_DEPT_PID_NAME);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
	}

	/**
	 * 
	 * 更新岗位名称、所属部门ID
	 * 
	 */
	@Override
	public void updateNameAndDeptIdByPos(Long projectId) throws Exception {
		// 执行更新
		Query query = getSession().createSQLQuery(SyncSqlConstant.UPDATE_POS_DEPTID_NAME);
		query.setLong("PROJECTID", projectId);
		query.executeUpdate();
	}

	/**
	 * 
	 * 更新人员真实姓名、邮箱地址、联系电话、内外网邮件标识(内网：0（inner）,外网：1（outer）)
	 * 
	 */
	@Override
	public void updateAttributesByUser(Long projectId) throws Exception {
		// 更新锁定状态为0
		// 执行更新
		Query query = getSession().createSQLQuery("	UPDATE JECN_USER SET ISLOCK = 0");
		query.executeUpdate();
		this.getSession().flush();

		query = getSession().createSQLQuery(SyncSqlConstant.UPDATE_JECN_USER_ISLOCK);
		query.executeUpdate();
		this.getSession().flush();

		String sql = "";
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = SyncSqlConstant.UPDATE_USER_NAME_EMAIL_PHONE_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = SyncSqlConstant.UPDATE_USER_NAME_EMAIL_PHONE_SQLSERVER;
		} else {
			return;
		}
		// 执行更新
		query = getSession().createSQLQuery(sql);
		query.executeUpdate();
		this.getSession().flush();

		if (DBType.ORACLE.equals(getDBDType())) {
			sql = "UPDATE JECN_USER JU SET JU.MANAGER_ID = (SELECT MAX(A.PEOPLE_ID) "
					+ "  FROM JECN_USER A, TMP_JECN_ORI_USER B   WHERE A.LOGIN_NAME = B.USER_LEADER_ID "
					+ " AND B.USER_NUM = JU.LOGIN_NAME) ";
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = "UPDATE JU SET JU.MANAGER_ID = (SELECT MAX(A.PEOPLE_ID) "
					+ "  FROM JECN_USER A, TMP_JECN_ORI_USER B   WHERE A.LOGIN_NAME = B.USER_LEADER_ID "
					+ " AND B.USER_NUM = JU.LOGIN_NAME) FROM JECN_USER JU ";
		}
		query = getSession().createSQLQuery(sql);
		query.executeUpdate();
	}

	/**
	 * 
	 * 获取数据库驱动名称
	 * 
	 * @return
	 */
	public DBType getDBDType() {
		// 获取驱动方言名称
		String dialectName = ((SessionImpl) getSession()).getFactory().getDialect().getClass().getName();

		if ("org.hibernate.dialect.OracleDialect".equals(dialectName)) {
			return DBType.ORACLE;
		} else if ("org.hibernate.dialect.MySQLDialect".equals(dialectName)) {
			return DBType.MYSQL;
		} else if ("org.hibernate.dialect.SQLServerDialect".equals(dialectName)) {
			return DBType.SQLSERVER;
		} else {
			return DBType.NONE;
		}
	}

	/**
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	@Override
	public List<JecnTmpOriDept> findAllDeptOri() throws Exception {
		String sql = "select t.*" + "from tmp_jecn_ori_dept t order by t.sort";
		return getSession().createSQLQuery(sql).addEntity(JecnTmpOriDept.class).list();
	}

	/**
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	@Override
	public List<JecnTmpPosAndUserBean> findAllUserAndPosOri() throws Exception {
		String sql = "select t.*" + "  from tmp_jecn_ori_user t" + " order by t.sort";

		return getSession().createSQLQuery(sql).addEntity(JecnTmpPosAndUserBean.class).list();
	}

	@Override
	public void updateFlowRoleFigureText(Map<String, List<Long>> roleMap, Long curProjectID) throws Exception {
		log.info("更新流程图中的角色信息开始");
		if (DBType.ORACLE.equals(getDBDType())) { // Oracle
			// 临时表
			String sql = SyncSqlConstant.UPDATE_JECN_FLOW_STRUCTURE_IMAGE_POS_T_ORACLE + "  and c.PROJECTID="
					+ curProjectID + ") where JFSI.FIGURE_ID in";
			List<String> sqlList = JecnCommonSql.getListSqls(sql, roleMap.get("posT"));
			for (String strSql : sqlList) {
				this.execteNative(strSql);
			}
			// 正式表
			sql = SyncSqlConstant.UPDATE_JECN_FLOW_STRUCTURE_IMAGE_POS_ORACLE + "  and c.PROJECTID=" + curProjectID
					+ ") where JFSI.FIGURE_ID in";
			sqlList = JecnCommonSql.getListSqls(sql, roleMap.get("pos"));
			for (String strSql : sqlList) {
				this.execteNative(strSql);
			}
		} else if (DBType.SQLSERVER.equals(getDBDType())) { // SQLServer
			// 临时表
			String sql = SyncSqlConstant.UPDATE_JECN_FLOW_STRUCTURE_IMAGE_POS_T_SQLSERVER + "  and c.PROJECTID="
					+ curProjectID + ")  FROM JECN_FLOW_STRUCTURE_IMAGE_T JFSI where JFSI.FIGURE_ID in";
			List<String> sqlList = JecnCommonSql.getListSqls(sql, roleMap.get("posT"));
			for (String strSql : sqlList) {
				this.execteNative(strSql);
			}
			// 正式表
			sql = SyncSqlConstant.UPDATE_JECN_FLOW_STRUCTURE_IMAGE_POS_SQLSERVER + "  and c.PROJECTID=" + curProjectID
					+ ") FROM JECN_FLOW_STRUCTURE_IMAGE JFSI where JFSI.FIGURE_ID in";
			sqlList = JecnCommonSql.getListSqls(sql, roleMap.get("pos"));
			for (String strSql : sqlList) {
				this.execteNative(strSql);
			}
		}
		// 获取Session以便数据库操作
		log.info("更新流程图中的角色信息结束");
	}

	@Override
	public void insertPositionFigInfo(Long currProjectID) throws Exception {
		Session s = this.getSession();
		Query query = s.createSQLQuery(SyncSqlConstant.INSERT_JECN_POSITION_FIG_INFO);
		query.setLong("PROJECTID", currProjectID);
		query.executeUpdate();
	}

	@Override
	public Map<String, List<Long>> getChangeRoles() throws Exception {
		Session s = this.getSession();
		// 临时表 -岗位
		Map<String, List<Long>> roleMap = new HashMap<String, List<Long>>();
		String sql = " SELECT A.FIGURE_ID FROM JECN_FLOW_STRUCTURE_IMAGE_T A,JECN_FLOW_ORG_IMAGE B,PROCESS_STATION_RELATED_T C,"
				+ "     (SELECT FIGURE_FLOW_ID FROM PROCESS_STATION_RELATED_T GROUP BY FIGURE_FLOW_ID HAVING COUNT(FIGURE_FLOW_ID)=1) D"
				+ " 	 WHERE A.FIGURE_ID=C.FIGURE_FLOW_ID AND B.FIGURE_ID=C.FIGURE_POSITION_ID AND C.TYPE=0"
				+ " 	 AND A.FIGURE_TYPE IN"
				+ JecnCommonSql.getRoleString()
				+ " 	 AND A.FIGURE_TEXT=B.FIGURE_TEXT"
				+ "      AND A.FIGURE_ID=D.FIGURE_FLOW_ID";
		List<Long> posList_T = s.createSQLQuery(sql).addScalar("FIGURE_ID", Hibernate.LONG).list();
		roleMap.put("posT", posList_T);

		// ////////////////////////正式表操作/////////////////////////////////

		// 正式表-岗位
		sql = " SELECT A.FIGURE_ID FROM JECN_FLOW_STRUCTURE_IMAGE A,JECN_FLOW_ORG_IMAGE B,PROCESS_STATION_RELATED C,"
				+ "     (SELECT FIGURE_FLOW_ID FROM PROCESS_STATION_RELATED GROUP BY FIGURE_FLOW_ID HAVING COUNT(FIGURE_FLOW_ID)=1) D"
				+ " 	 WHERE A.FIGURE_ID=C.FIGURE_FLOW_ID AND B.FIGURE_ID=C.FIGURE_POSITION_ID AND C.TYPE=0"
				+ " 	 AND A.FIGURE_TYPE IN" + JecnCommonSql.getRoleString() + " 	 AND A.FIGURE_TEXT=B.FIGURE_TEXT"
				+ "      AND A.FIGURE_ID=D.FIGURE_FLOW_ID";
		List<Long> posList = s.createSQLQuery(sql).addScalar("FIGURE_ID", Hibernate.LONG).list();
		roleMap.put("pos", posList);
		return roleMap;
	}

	/***
	 * 基准岗位同步
	 * 
	 * @author cheshaowei
	 * @param jecnBasePosBeanList
	 *            新添加的基准岗位集合
	 * 
	 * */
	@Override
	public void insertBasePosInfo(JecnBasePosBean basePosBean) throws Exception {
		this.getSession().save(basePosBean);
	}

	/**
	 * 获取所有的实际岗位信息（主键ID，岗位编号）
	 * 
	 * @author cheshaowei
	 * @date 15-03-17
	 * @Override
	 * */
	@SuppressWarnings("unchecked")
	public List<JecnFlowOrgImage> getFlowOrgImageList() throws Exception {
		Session session = this.getSession();
		if (session != null) {
			String sql = SyncSqlConstant.SELECT_POS_INFO;
			SQLQuery query = session.createSQLQuery(sql);
			List<Object[]> flowOrgImageList = query.list();
			return flowOrgImageList(flowOrgImageList);
		}
		return null;
	}

	private List<JecnFlowOrgImage> flowOrgImageList(List<Object[]> flowOrgImageList) {

		List<JecnFlowOrgImage> flowOrgImages = new ArrayList<JecnFlowOrgImage>();
		for (Object[] obj : flowOrgImageList) {
			if (obj[0] != null && obj[1] != null) {
				JecnFlowOrgImage flowOrgImage = new JecnFlowOrgImage();
				flowOrgImage.setFigureId(Long.parseLong(obj[0].toString()));
				flowOrgImage.setFigureNumberId(obj[1].toString());
				flowOrgImages.add(flowOrgImage);
			}
		}

		return flowOrgImages;
	}

	/**
	 * 查询所有的基准岗位信息
	 * 
	 * @return List<JecnBasePosBean> 查询到的所有基准岗位集合
	 * @author cheshaowei
	 * @date 15-03-19
	 * 
	 * **/
	public List<JecnBasePosBean> getJecnBasePosBeanList() throws Exception {

		String sql = SyncSqlConstant.SELECT_BASE_POS_INFO;
		List<JecnBasePosBean> basePosBeans = null;
		Session session = this.getSession();
		if (session != null) {
			SQLQuery query = session.createSQLQuery(sql);
			basePosBeans = query.addEntity(JecnBasePosBean.class).list();
		}
		return basePosBeans;
	}

	/**
	 * 删除基准岗位
	 * 
	 * @param baseId
	 *            基准岗位主键ID
	 * @author cheshaowei
	 * 
	 * */
	public void delBasePosInfo(String baseId) throws Exception {
		if (StringUtils.isBlank(baseId)) {
			return;
		}
		String sql = SyncSqlConstant.DEL_BASE_POS_INFO;
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = SyncSqlConstant.DEL_BASE_POS_INFO_SQL;
		}
		Session session = this.getSession();
		if (session != null) {
			SQLQuery query = session.createSQLQuery(sql);
			query.setParameter(0, baseId);
			query.executeUpdate();
		}
	}

	/**
	 * 插入基准岗位与岗位关系
	 * 
	 * @param jecnBaseRelPosBean
	 *            基准岗位与岗位关系封装Bean
	 * 
	 * @author cheshaowei
	 * 
	 * 
	 * */
	public void insertBaseRelPosInfo(JecnBaseRelPosBean jecnBaseRelPosBean) throws Exception {
		try {
			String sql = SyncSqlConstant.INSERT_BASE_REL_POS_INFO;
			Query query = this.getSession().createSQLQuery(sql).addEntity(JecnBaseRelPosBean.class);
			query.setParameter(0, jecnBaseRelPosBean.getId());
			query.setParameter(1, jecnBaseRelPosBean.getPosId());
			query.setParameter(2, jecnBaseRelPosBean.getBasePosId());
			query.executeUpdate();
		} catch (HibernateException e) {
			log.error("insertBaseRelPosInfo插入基准岗位与岗位关系出现异常！" + this.getClass() + "943", e);
			throw e;
		}
	}

	/**
	 * 岗位对应基准岗位变更，岗位组相关岗位集合：删除不存在的记录(岗位组关联基准岗位)
	 * 
	 * @throws Exception
	 */
	@Override
	public void deletePosChangeBasePos() throws Exception {
		String sql = "DELETE FROM JECN_POSITION_GROUP_R GR" + " WHERE NOT EXISTS (SELECT 1"
				+ "          FROM JECN_BASE_REL_POS BRP" + "          LEFT JOIN JECN_FLOW_ORG_IMAGE OI"
				+ "            ON BRP.POS_ID = OI.FIGURE_NUMBER_ID" + "          LEFT JOIN JECN_BASEPOS_REL_GROUP BRG"
				+ "            ON BRG.BASE_ID = BRP.BASE_ID" + "         WHERE BRG.GROUP_ID IS NOT NULL"
				+ "           AND GR.GROUP_ID = BRG.GROUP_ID" + "           AND GR.FIGURE_ID = OI.FIGURE_ID)"
				+ "   AND GR.STATE = 1";
		this.execteNative(sql);
	}

	/**
	 * 删除基准岗位与岗位的关系
	 * 
	 * @author cheshaowei
	 * **/
	public void delBasePosGroup(String baseId) throws Exception {

		try {
			String sql = "DELETE FROM JECN_BASEPOS_REL_GROUP " + " WHERE BASE_ID = ?";
			Session session = this.getSession();
			Query query = session.createSQLQuery(sql);
			query.setParameter(0, baseId);
			query.executeUpdate();
		} catch (HibernateException e) {
			log.error("删除基准岗位与岗位的关系出现异常!" + this.getClass() + "line 978", e);
		}

	}

	/**
	 * 添加岗位与岗位组的关系
	 * 
	 **/
	public void addGroupRelPos() throws Exception {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "INSERT INTO JECN_POSITION_GROUP_R(ID,GROUP_ID,FIGURE_ID,STATE)"
					+ "SELECT JECN_POSITION_GROUP_R_SQUENCE.NEXTVAL,";
		} else {
			sql = "INSERT INTO JECN_POSITION_GROUP_R(GROUP_ID,FIGURE_ID,STATE)" + "SELECT ";
		}
		sql = sql + " BRG.GROUP_ID GROUP_ID,JFOI.FIGURE_ID FIGURE_ID,  1 STATE " + "  FROM JECN_BASE_REL_POS BRP"
				+ "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI" + "    ON BRP.POS_ID = JFOI.FIGURE_NUMBER_ID"
				+ "  LEFT JOIN JECN_BASEPOS_REL_GROUP BRG" + "    ON BRP.BASE_ID = BRG.BASE_ID"
				+ "  LEFT JOIN JECN_POSITION_GROUP_R PGR" + "    ON BRG.GROUP_ID = PGR.GROUP_ID"
				+ "   AND PGR.STATE = 1" + "   AND JFOI.FIGURE_ID = PGR.FIGURE_ID" + " WHERE BRG.GROUP_ID IS NOT NULL"
				+ "   AND JFOI.FIGURE_ID IS NOT NULL" + "   AND PGR.STATE IS NULL";
		this.execteNative(sql);
	}

	/**
	 * 岗位组与岗位关系表中比基准岗位与岗位组多余的数据
	 * 
	 * */
	public void delGroupRelPos(String baseId) throws Exception {
		try {
			String sql = "DELETE FROM JECN_POSITION_GROUP_R JPG_R " + "WHERE JPG_R.FIGURE_ID IN " + "( "
					+ "SELECT JPGR.FIGURE_ID FROM JECN_POSITION_GROUP_R JPGR "
					+ "INNER JOIN JECN_BASEPOS_REL_GROUP JBRG " + "ON JBRG.GROUP_ID = JPGR.GROUP_ID "
					+ "AND JBRG.BASE_ID = ? " + "AND JPGR.STATE =1 " + "WHERE JPGR.FIGURE_ID NOT IN " + "       ( "
					+ "       SELECT JFOI.FIGURE_ID " + "          FROM JECN_BASE_REL_POS JBRP "
					+ "         INNER JOIN JECN_FLOW_ORG_IMAGE JFOI "
					+ "            ON JBRP.POS_ID = JFOI.FIGURE_NUMBER_ID " + "           AND JBRP.BASE_ID = ?) "
					+ ") " + "AND JPG_R.GROUP_ID IN ( " + "  SELECT JBRG.GROUP_ID FROM JECN_BASEPOS_REL_GROUP JBRG "
					+ "  WHERE JBRG.BASE_ID =? " + ")";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "DELETE JPG_R FROM JECN_POSITION_GROUP_R JPG_R " + "WHERE JPG_R.FIGURE_ID IN " + "( "
						+ "SELECT JPGR.FIGURE_ID FROM JECN_POSITION_GROUP_R JPGR "
						+ "INNER JOIN JECN_BASEPOS_REL_GROUP JBRG " + "ON JBRG.GROUP_ID = JPGR.GROUP_ID "
						+ "AND JBRG.BASE_ID = ? " + "AND JPGR.STATE =1 " + "WHERE JPGR.FIGURE_ID NOT IN " + "       ( "
						+ "       SELECT JFOI.FIGURE_ID " + "          FROM JECN_BASE_REL_POS JBRP "
						+ "         INNER JOIN JECN_FLOW_ORG_IMAGE JFOI "
						+ "            ON JBRP.POS_ID = JFOI.FIGURE_NUMBER_ID " + "           AND JBRP.BASE_ID = ?) "
						+ ") " + "AND JPG_R.GROUP_ID IN ( "
						+ "  SELECT JBRG.GROUP_ID FROM JECN_BASEPOS_REL_GROUP JBRG " + "  WHERE JBRG.BASE_ID =? " + ")";
			}
			Session session = this.getSession();
			Query query = session.createSQLQuery(sql);
			query.setParameter(0, baseId);
			query.setParameter(1, baseId);
			query.setParameter(2, baseId);
			query.executeUpdate();
		} catch (HibernateException e) {
			log.error("执行sql出现异常！" + this.getClass() + " line 1037", e);
		}
	}

	/**
	 * 删除岗位组与岗位关系
	 * 
	 * @author cheshaowei
	 * 
	 * */
	public void delJecnBasePosGroupR(String baseId) {

		try {
			String sql = "DELETE FROM JECN_POSITION_GROUP_R JPG_R " + "WHERE JPG_R.GROUP_ID IN( "
					+ "SELECT JPGR.GROUP_ID FROM JECN_POSITION_GROUP_R JPGR "
					+ "INNER JOIN JECN_BASEPOS_REL_GROUP JBRG " + "ON JBRG.GROUP_ID = JPGR.GROUP_ID "
					+ "AND JBRG.BASE_ID = ?" + ") " + "AND JPG_R.STATE = 1";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "DELETE JPG_R FROM JECN_POSITION_GROUP_R JPG_R " + "WHERE JPG_R.GROUP_ID IN( "
						+ "SELECT JPGR.GROUP_ID FROM JECN_POSITION_GROUP_R JPGR "
						+ "INNER JOIN JECN_BASEPOS_REL_GROUP JBRG " + "ON JBRG.GROUP_ID = JPGR.GROUP_ID "
						+ "AND JBRG.BASE_ID = ?" + ") " + "AND JPG_R.STATE = 1";
			}
			Session session = this.getSession();
			Query query = session.createSQLQuery(sql);
			query.setParameter(0, baseId);
			query.executeUpdate();
		} catch (HibernateException e) {
			log.error("删除基准岗位与岗位的关系出现异常!", e);
		}
	}

	/**
	 * 根据基准岗位编号删除基准岗位与岗位的关系
	 * 
	 * @param baseNum
	 *            基准岗位编号
	 * @author cheshaowei
	 * 
	 * */
	public void delBaseRelPosParmBaseNum(String baseNum) {

		try {
			Session session = null;
			String sql = "DELETE FROM JECN_BASE_REL_POS JBRP" + " WHERE JBRP.BASE_ID = ?";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "DELETE JBRP FROM JECN_BASE_REL_POS JBRP" + " WHERE JBRP.BASE_ID = ?";
			}
			session = this.getSession();
			if (session != null) {
				Query query = session.createSQLQuery(sql);
				query.setParameter(0, baseNum);
				query.executeUpdate();
			}
		} catch (HibernateException e) {
			log.error("根据基准岗位编号删除基准岗位与岗位的关系出现异常！" + this.getClass() + " line 967", e);
		}

	}

	/**
	 * 删除所有基准岗位
	 * 
	 * @author cheshaowei
	 * */
	public void deleteBaseRelPosInfo() {
		try {
			String sql = SyncSqlConstant.DEL_ALL_BASE_REL_POS_INFO;
			Session session = this.getSession();
			if (session != null) {
				session.createSQLQuery(sql).executeUpdate();
			}
		} catch (HibernateException e) {
			log.error("删除所有的基准岗位出现异常！" + this.getClass() + "line 956", e);
		}
	}

	/**
	 * 部门总数
	 * 
	 * @param projectId
	 * @return
	 */
	@Override
	public List<Object[]> getSyncDeptNums(Long projectId) {
		String sql = "SELECT (SELECT COUNT(*) FROM JECN_FLOW_ORG WHERE DEL_STATE = 0 AND PROJECTID = " + projectId
				+ ") TRUE_ORG_COUNT," + "       (SELECT COUNT(*) FROM TMP_JECN_IO_DEPT) TMP_ORG_COUNT";
		if (DBType.ORACLE.equals(getDBDType())) {
			sql += "  FROM DUAL";
		}
		return this.listNativeSql(sql);
	}

	/**
	 * 岗位总数
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getSyncUserNumsCount() throws Exception {
		String sql = "SELECT (SELECT COUNT(*) FROM JECN_USER WHERE ISLOCK = 0) TRUE_USER_COUNT,"
				+ "       (SELECT COUNT(distinct USER_NUM) FROM TMP_JECN_IO_USER) TMP_USER_COUNT";
		if (DBType.ORACLE.equals(getDBDType())) {
			sql += "  FROM DUAL";
		}
		return this.listNativeSql(sql);
	}

	/**
	 * 人员总数
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getSyncPosNumsCount(Long projectId) throws Exception {
		String sql = "SELECT (SELECT COUNT(*)" + "          FROM JECN_FLOW_ORG_IMAGE OI"
				+ "         INNER JOIN JECN_FLOW_ORG O" + "            ON O.ORG_ID = OI.ORG_ID"
				+ "          AND DEL_STATE = 0 AND PROJECTID = " + projectId
				+ "         WHERE OI.FIGURE_TYPE = 'PositionFigure') TRUE_USER_COUNT,"
				+ "       (SELECT COUNT(*) FROM TMP_JECN_IO_POS) TMP_USER_COUNT";
		if (DBType.ORACLE.equals(getDBDType())) {
			sql += "  FROM DUAL";
		}
		return this.listNativeSql(sql);
	}

	@Override
	public void updatePerOrgNum() {
		String sql = "update JECN_FLOW_ORG  SET PREORG_NUMBER =" + "       (SELECT D.P_DEPT_NUM"
				+ "          FROM TMP_JECN_IO_DEPT D"
				+ "         WHERE D.DEPT_NUM = ORG_NUMBER) WHERE ORG_NUMBER IS NOT NULL";
		this.execteNative(sql);
	}
}
