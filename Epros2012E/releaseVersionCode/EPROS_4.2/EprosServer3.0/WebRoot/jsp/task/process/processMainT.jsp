<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><s:text name="flowChartForDetails"></s:text></title>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
		<script type="text/javascript" src="${basePath}js/common.js"></script>
		<script type="text/javascript"
			src="${basePath}js/epros_${language}_${country}.js"></script>
		<script type="text/javascript"
			src="${basePath}ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="${basePath}ext/ext-all.js"></script>
		<script type="text/javascript" src="${basePath}jquery/jquery-1.6.2.js"></script>
        <script type="text/javascript" src="${basePath}js/firstPage.js" charset="UTF-8"></script>
        <script type="text/javascript">
			var basePath = "${basePath}";
		</script>
		<script type="text/javascript" src="${basePath}js/browserDetect.js" charset="UTF-8"></script>
		<script type="text/javascript">
		var versionType=${versionType};
Ext.onReady(function() {
	var processMainPanel = new Ext.TabPanel({
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				region : 'center',
				items : [{
					title : i_procesChart,// 流程图
					id : "processMap_id",
					autoLoad : "${basePath}"
							+ "jsp/processSys/"+processMapContainerJsp+"?isPub=false&mapType=process&mapID="
							+ "${param.flowId}"+"&mapName="+"${param.name}"
				}, {
					title : '流程属性',// 概况信息
					id : "processOvervicwInfo_id",
					autoLoad : "${basePath}"
							+ "processProfileInformation.action?isPub=false&flowId="
							+ "${param.flowId}"
				}, {
					title : '流程说明',// 操作说明
					id : "processOperateDes_id",
					autoLoad : {url:"${basePath}"
							+ "processFile.action?isPub=false&flowId="
							+ "${param.flowId}",
							scripts : true,
					           callback:function(){
						            setShowProcessFileDivWH('processFileDiv');
					           }
							}
				}, {
					title : i_operateMode,// 操作模板
					id : "processoperateMode_id",
					autoLoad : {url:"${basePath}"
							+ "processTemplate.action?isPub=false&flowId="
							+ "${param.flowId}",
							scripts : true,
							  callback:function(){
								setShowTaskProcessTemplate('processTemplateDiv');
							   }
							}
				},{
					title : i_relateFile,// 相关文件
					id : "relaProStanRule_id",
					autoLoad : { 
					        url :"${basePath}"
							+ "relatedProcessRuleStandard.action?isPub=false&flowId="
							+ "${param.flowId}",
								scripts : true,
							callback:function(){
								setShowTaskProcessTemplate('relatedProcessRuleStandardId');
							}
						}
				}],
				listeners : {

					'bodyresize' : function(p, width, height) {
						if (p.getActiveTab() == null) {
							return;
						}
						if (p.getActiveTab().getId() == 'processOperateDes_id') {
							setShowProcessFileDivWH('processFileDiv');
						} else if (p.getActiveTab().getId() == 'processoperateMode_id') {
							setShowProcessTemplate('processTemplateDiv');
						} else if (p.getActiveTab().getId()== 'relaProStanRule_id') {
							setShowTaskProcessTemplate('relatedProcessRuleStandardId');
						}
					},
					'tabchange' : function(tabPanel, panel) {
						if(typeof(panel) == "undefined"){
							return;
						}
						if (panel.getId() == 'processOperateDes_id') {
							setShowProcessFileDivWH('processFileDiv');
						} else if (panel.getId() == 'processoperateMode_id') {
							setShowProcessTemplate('processTemplateDiv');
							
						} else if (panel.getId() == 'relaProStanRule_id') {
							setShowTaskProcessTemplate('relatedProcessRuleStandardId');
						}

					}

					}
			});
	if(!versionType){
		processMainPanel.remove('relaProStanRule_id');
		}
	if(${isHiddenBDF}){
		processMainPanel.remove('processoperateMode_id');
		processMainPanel.remove('relaProStanRule_id');
	}
	var northPanle=new Ext.Panel({
		height:20,
		region : 'north',
		contentEl:'homePage_id'		
	});
	new Ext.Viewport({
		layout : 'border',
		items : [northPanle,processMainPanel]
	});
});

</script>
	</head>

	<body>
	    <div id="homePage_id" style="height: 25px; width: 100%; overflow: hidden;"
				align="right" class="gradient minWidth">
				<%@include file="/navigation.jsp"%>
		</div>
	</body>
</html>