package epros.designer.gui.integration.internalControl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnControlGuide;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;

/**
 * 内控指引知识库管理界面
 * 
 * @author Administrator
 * 
 */
public class ControlGuideManageDialog extends JecnDialog implements
		ActionListener {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger
			.getLogger(ControlGuideManageDialog.class);

	/** 主面板 */
	private JPanel mainPane = null;
	/** 树滚动面板+右侧面板的容器· */
	private JecnSplitPane splitPane = null;
	/** 树面板 */
	private JScrollPane treeScrollPane = null;
	/** 右侧面板 */
	private JPanel rightPane = null;
	/** 内容显示面板 */
	private JScrollPane contentShowPane = null;
	/** 关闭按钮面板 */
	private JPanel closePanel = null;
	/** 条款名称和名称内容Panel */
	private JPanel guideNameAndGuideNameTextPane = null;
	/** 树 */
	private JTree tree = null;
	/** 关闭按钮 */
	private JButton closeButton = null;
	/** 条款名称label */
	private JLabel guideNameLabel = null;
	/** 条款名称内容 */
	protected static JTextField guideNameText = null;
	/** 条款内容 */
	protected static JTextArea controlGuideContent = null;
	/** 按钮面板，上面放两个按钮，保存和恢复 */
	private JPanel buttonsPanel = null;
	/** 保存按钮 */
	public static JButton saveButton = null;
	/** 恢复按钮 */
	public static JButton recoverButton = null;
	/** 单选节点 */
	private JecnTreeNode selectNode = null;
	private HighEfficiencyJecnControlGuideTree highEfficiencyJecnControlGuideTree;
	/** 名称验证提示 */
	protected static JLabel promptLab = null;
	/** 内容验证提示 */
	protected static JLabel promptContent = null;
	/** 设置面板控件大小 */
	Dimension dimension = null;
	/** 必填项 *号提示 */
	private JLabel requiredMarkLab = new JLabel("*");

	protected static String oldTextName = "";
	protected static String oldTextContent = "";

	// *********************测试***************************
	// private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	// *********************测试***************************

	public ControlGuideManageDialog() {
		initComponents();
		initButton();
		initLayout();

		// 给树添加键盘选中监听
		tree.addTreeSelectionListener(new JecnTreeSelectionListener());
		guideNameText.getDocument().addDocumentListener(new DocumentLinstener());
		controlGuideContent.getDocument().addDocumentListener(
				new DocumentLinstener());
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				closeButtonPerformed();
				return false;
			}
		});

	}

	/**
	 * 初始化组件
	 */
	public void initComponents() {
		// Dialog标题
		this.setTitle(JecnProperties.getValue("controlDB"));
		// 模态
		this.setModal(true);
		// Dialog大小
		this.setSize(810, 510);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);
		// 主面板
		mainPane = new JPanel();
		// 树面板
		treeScrollPane = new JScrollPane();
		// 树滚动面板大小
		Dimension size = new Dimension(200, 430);
		treeScrollPane.setPreferredSize(size);
		treeScrollPane.setMinimumSize(size);

		requiredMarkLab.setForeground(Color.red);

		promptLab = new JLabel();
		promptLab.setForeground(Color.red);
		// 设置验证提示Label的大小
		dimension = new Dimension(400, 13);
		promptLab.setPreferredSize(dimension);
		promptLab.setMaximumSize(dimension);
		promptLab.setMinimumSize(dimension);
		promptContent = new JLabel();
		promptContent.setForeground(Color.red);
		dimension = new Dimension(200, 13);
		promptContent.setPreferredSize(dimension);
		promptContent.setMaximumSize(dimension);
		promptContent.setMinimumSize(dimension);

		// 右侧面板
		rightPane = new JPanel();
		// 树滚动面板+右侧面板的容器
		splitPane = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, mainPane,
				treeScrollPane, rightPane);
		// 按钮面板，保存，恢复
		buttonsPanel = new JPanel();
		// 条款名称label
		guideNameLabel = new JLabel(JecnProperties.getValue("nameC"));

		// 保存按钮
		saveButton = new JButton(JecnProperties.getValue("saveBut"));
		// 默认不可用
		saveButton.setEnabled(false);

		// 条款名称内容
		guideNameText = new JTextField(120);

		// 条款内容
		controlGuideContent = new JTextArea();
		controlGuideContent.setLineWrap(true);
		controlGuideContent.setWrapStyleWord(true);
		// 内容显示面板
		contentShowPane = new JScrollPane(controlGuideContent);

		controlGuideContent.setBorder(null);
		contentShowPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		contentShowPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		contentShowPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 树
		tree = initJecnTree();
		// 把树放入树面板中
		treeScrollPane.setViewportView(tree);

		// 关闭按钮面板
		closePanel = new JPanel();
		// 关闭按钮
		closeButton = new JButton(JecnProperties.getValue("closeBtn"));

		// 恢复默认按钮
		recoverButton = new JButton(JecnProperties.getValue("recyBtn"));
		// 默认不可用
		recoverButton.setEnabled(false);

		// 条款名称和名称内容Panel
		guideNameAndGuideNameTextPane = new JPanel();

		// 主面板布局
		mainPane.setLayout(new BorderLayout());
		// 右侧面板布局
		rightPane.setLayout(new BorderLayout(0, 2));
		// 树滚动面板
		treeScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), JecnProperties.getValue("controlDB")));
		// 右侧面板
		rightPane.setBorder(BorderFactory
				.createTitledBorder(BorderFactory.createEtchedBorder(),
						JecnProperties.getValue("clauseContent")));
		// 左:右=3:7
		splitPane.setDividerLocation(0.3);
		// 当窗体变大时，右边面板获取空间,左面板不变
		splitPane.setResizeWeight(0.0);
		splitPane.showBothPanel();
		guideNameText.setEditable(false);
		controlGuideContent.setEditable(false);

		guideNameText.setBorder(BorderFactory.createLineBorder(Color.gray));
		mainPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		guideNameText.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treeScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		rightPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		guideNameAndGuideNameTextPane.setBackground(JecnUIUtil
				.getDefaultBackgroundColor());
		contentShowPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		treeScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		closePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		controlGuideContent.setBackground(JecnUIUtil
				.getDefaultBackgroundColor());
	}

	class DocumentLinstener implements DocumentListener {

		@Override
		public void changedUpdate(DocumentEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			String newText = guideNameText.getText();
			String newContent = controlGuideContent.getText();
			validateName(newText, promptLab);
			validateContent(newContent, promptContent);
			if (!newText.equals(oldTextName)
					|| !newContent.equals(oldTextContent)) {
				saveButton.setEnabled(true);
				recoverButton.setEnabled(true);
			} else {
				saveButton.setEnabled(false);
				recoverButton.setEnabled(false);
			}
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			String newText = guideNameText.getText();
			String newContent = controlGuideContent.getText();
			validateName(newText, promptLab);
			validateContent(newContent, promptContent);
			if (!newText.equals(oldTextName)
					|| !newContent.equals(oldTextContent)) {
				saveButton.setEnabled(true);
				recoverButton.setEnabled(true);
			} else {
				saveButton.setEnabled(false);
				recoverButton.setEnabled(false);
			}
		}

	}

	/**
	 * 初始化Button
	 */
	private void initButton() {
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 关闭
				closeButtonPerformed();
			}
		});
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveButtonPerformed();
			}
		});
		recoverButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				recoverButtonPerformed();
			}
		});
	}

	/**
	 * 编辑条款
	 */
	private boolean saveButtonPerformed() {
		promptLab.setText("");
		promptContent.setText("");
		String strGuideNameText = guideNameText.getText();
		String strControlGuideContent = controlGuideContent.getText();

		if (!validateName(strGuideNameText, promptLab)) {
			return false;
		}
		if (!validateContent(strControlGuideContent, promptContent)) {
			return false;
		}
		try {
			if (selectNode == null) {
				return false;
			}
			if (validateNodeRepeat(strGuideNameText.trim())) {
				promptLab.setText(JecnProperties.getValue("nameHaved"));
				return false;
			}
			boolean flag = ConnectionPool
					.getControlGuideAction()
					.updateJecnControlGuide(
							selectNode.getJecnTreeBean().getId(),
							strGuideNameText,
							strControlGuideContent,
							JecnConstants.loginBean.getJecnUser().getPeopleId(),
							new Date());
			if (flag) {
				// 刷新节点
				JecnTreeCommon.reNameNode(tree, selectNode, strGuideNameText);
				saveButton.setEnabled(false);
				recoverButton.setEnabled(false);
				return true;
			}
		} catch (Exception e) {
			log.error("ControlGuideManageDialog saveButtonPerformed is error", e);
		}
		return false;
	}

	/**
	 * 编辑时，判断是否重名
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public boolean validateNodeRepeat(String name) throws Exception {
		return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
	}

	/**
	 * 验证输入的名称是否正确 名称不能为空 名称不能超过122个字符或61个汉字
	 * 
	 * @param name
	 * @param jLable
	 * @return
	 */
	public boolean validateName(String name, JLabel jLable) {
		return JecnValidateCommon.validateNameNoRestrict(name, jLable);
	}

	/**
	 * 判断给定参数是否超过1200个字符或600个汉字
	 * 
	 * @param content
	 * @param jLable
	 * @return
	 */
	public boolean validateContent(String content, JLabel jLable) {
		return JecnValidateCommon.validateContent(content, jLable);
	}

	/**
	 * 恢复按钮
	 */
	private void recoverButtonPerformed() {
		try {
			if (selectNode == null) {
				return;
			}
			// 根据节点ID查询条款信息
			JecnControlGuide jecnControlGuide = ConnectionPool
					.getControlGuideAction().getJecnControlGuideById(
							selectNode.getJecnTreeBean().getId());
			guideNameText.setText(jecnControlGuide.getName());
			controlGuideContent.setText(jecnControlGuide.getDescription());
			saveButton.setEnabled(false);
			recoverButton.setEnabled(false);
			// 刷新节点
			JecnTreeCommon.reNameNode(tree, selectNode, jecnControlGuide
					.getName());
			promptLab.setText("");
			promptContent.setText("");
		} catch (Exception e) {
			log.error("ControlGuideManageDialog recoverButtonPerformed is error", e);
		}
	}

	/**
	 * 布局
	 */
	public void initLayout() {
		// **************第一层布局**************//
		// 内容区
		mainPane.add(splitPane, BorderLayout.CENTER);
		// 关闭按钮面板
		mainPane.add(closePanel, BorderLayout.SOUTH);
		// 关闭按钮
		closePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		closePanel.add(closeButton);
		// **************第一层布局**************//

		// **************第二层布局**************//
		// 右侧面板，在上方放名称和名称内容面板
		rightPane.add(guideNameAndGuideNameTextPane, BorderLayout.NORTH);
		guideNameAndGuideNameTextPane.setLayout(new BorderLayout());
		// 名称Label放左边
		guideNameAndGuideNameTextPane.add(guideNameLabel, BorderLayout.WEST);
		// 名称内容放中间
		guideNameAndGuideNameTextPane.add(guideNameText, BorderLayout.CENTER);
		guideNameAndGuideNameTextPane.add(requiredMarkLab, BorderLayout.EAST);
		guideNameAndGuideNameTextPane.add(promptLab, BorderLayout.SOUTH);
		// 右侧面板，在下方放保存和恢复面板
		rightPane.add(buttonsPanel, BorderLayout.SOUTH);
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		promptContent.setLocation(0, 20);
		buttonsPanel.add(promptContent);
		// 将保存按钮放入按钮面板中
		buttonsPanel.add(saveButton);
		// 将恢复按钮放入按钮面板中
		buttonsPanel.add(recoverButton);
		// 右侧面板，在中间放内容面板
		rightPane.add(contentShowPane, BorderLayout.CENTER);
		// **************第二层布局**************//
		// 把主面板增加到dialog
		this.getContentPane().add(mainPane);
	}

	/**
	 * 初始化树结构
	 * 
	 * @return
	 */
	public JecnTree initJecnTree() {
		highEfficiencyJecnControlGuideTree = new HighEfficiencyJecnControlGuideTree(
				this);
		return highEfficiencyJecnControlGuideTree;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	/**
	 * 点击关闭按钮关闭窗口
	 */
	private void closeButtonPerformed() {
		//保存按钮置灰时，直接关闭
			if(!saveButton.isEnabled()){
				promptLab.setText("");
				this.dispose();
				return;
			}

		// 判断条款名称和内容是否更改
		boolean flag = isUpdate();
		if(flag){
			int topState=dialogCloseBeforeMsgTipAction();
			if(topState==2){
				if (!saveButtonPerformed()) {
					return;
				}
				this.dispose();
			}else if(topState==1){
				this.dispose();
			}
		}else{
			this.dispose();
		}
	}

	public JecnTreeNode getSelectNode() {
		return selectNode;
	}

	public void setSelectNode(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
	}

	/**
	 * 检测内控指引条款是否有所更改
	 */
	private boolean isUpdate() {
		// 条款名称
		String strGuideNameText = guideNameText.getText();
		// 条款内容
		String strControlGuideContent = controlGuideContent.getText();
		JecnControlGuide controlGuide = null;
		try {
			if (selectNode == null) {
				return false;
			}
			controlGuide = ConnectionPool.getControlGuideAction()
					.getJecnControlGuideById(
							selectNode.getJecnTreeBean().getId());
			if (!validateName(strGuideNameText, promptLab)
					&& strGuideNameText.equals(controlGuide.getName())) {
				return false;
			}
			if (!validateContent(strControlGuideContent, promptContent)
					&& strControlGuideContent.equals(controlGuide
							.getDescription())) {
				return false;
			}
			return true;
		} catch (Exception e) {
			log.error("ControlGuideManageDialog isUpdate is error！", e);
		}
		return false;
	}

	// 树节点切换，判断是目录还是节点
	private void isChanged(JecnTreeNode selectTreeNode,
			JecnControlGuide controlGuide) {
		if ("innerControlDir".equals(selectTreeNode.getJecnTreeBean()
				.getTreeNodeType().toString())) {
			guideNameText.setText("");
			guideNameText.setEditable(false);
			guideNameText.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			controlGuideContent.setText("");
			controlGuideContent.setEditable(false);
			controlGuideContent.setBackground(JecnUIUtil
					.getDefaultBackgroundColor());
			promptLab.setText("");
			saveButton.setEnabled(false);
			recoverButton.setEnabled(false);
		}
		if ("innerControlClause".equals(selectTreeNode.getJecnTreeBean()
				.getTreeNodeType().toString())) {
			guideNameText.setText(controlGuide.getName());
			guideNameText.setEditable(true);
			guideNameText.setBackground(JecnUIUtil.getWorkflowColor());
			controlGuideContent.setText(controlGuide.getDescription());
			controlGuideContent.setEditable(true);
			controlGuideContent.setBackground(new Color(255, 255, 255));
			oldTextName = controlGuide.getName();
			oldTextContent = controlGuide.getDescription();
			saveButton.setEnabled(false);
			recoverButton.setEnabled(false);
			setSelectNode(selectTreeNode);
		}

	}

	// 提示是否保存
	private int isSave() {

		int option = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("contentPrompt")
				+ JecnProperties.getValue("isSave"), null,
				JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);

		if (option == 0) {// YES
			boolean flag = saveButtonPerformed();
			// 判断条款名称 是否符合条件，如果符合返回1，不符合返回0
			if (flag == false) {
				return 0;
			} else {
				return 1;
			}
		} else {// No
			// 选不保存时返回2
			// recoverButtonPerformed();
			// saveButton.setEnabled(false);
			// recoverButton.setEnabled(false);
			promptLab.setText("");
			promptContent.setText("");
			return 2;
		}
	}

	/**
	 * 树结构选中监听
	 * 
	 * @author user
	 * 
	 */
	class JecnTreeSelectionListener implements TreeSelectionListener {

		@Override
		public void valueChanged(TreeSelectionEvent e) {
			// promptLab.setText("");
			// promptContent.setText("");
			Object obj = e.getSource();
			if (obj == null || !(obj instanceof JecnTree)) {
				return;
			}
			JTree tree = (JTree) obj;
			// 新树节点路径
			TreePath path = e.getNewLeadSelectionPath();
			// 选中上一次树节点路径
			TreePath oldpath = e.getOldLeadSelectionPath();
			if (path == null) {
				return;
			}
			tree.setSelectionPath(path);
			//获得当前选中节点
			JecnTreeNode selectTreeNode = (JecnTreeNode) path
					.getLastPathComponent();
			JecnControlGuide controlGuide = null;
			Long id = selectTreeNode.getJecnTreeBean().getId();
			
			if (oldpath == null && "innerControlDir".equals(selectTreeNode.getJecnTreeBean()
					.getTreeNodeType().toString())) {
				return;
			}else{
				try {
					controlGuide = ConnectionPool.getControlGuideAction()
							.getJecnControlGuideById(id);
				} catch (Exception e1) {
					log.error("valueChanged is error", e1);
					return;
				}
			}
			if (oldpath == null) {
				if (controlGuide != null && "innerControlClause".equals(selectTreeNode.getJecnTreeBean()
						.getTreeNodeType().toString())) {
					isChanged(selectTreeNode, controlGuide);
				}
				return;
			}

			//获得当前节点之前的节点
			JecnTreeNode selectOldTreeNode = (JecnTreeNode) oldpath
					.getLastPathComponent();
			// 保存按钮可用时调用保存方法
			if (saveButton.isEnabled()) {
				// 判断是否保存
				int count = isSave();
				// 弹出是否保存，选择是，但是没有达到条件要求，返回0，并把内容还原成修改后的
				if (count == 0) {
					//修改后的名称
					String newTextName = guideNameText.getText();
					//修改后的内容
					String newContent = controlGuideContent.getText();
					isChanged(selectOldTreeNode, controlGuide);
					tree.setSelectionPath(oldpath);
					guideNameText.setText(newTextName);
					controlGuideContent.setText(newContent);
					return;
				} else if (count == 2) {
					// 弹出是否保存，选择否
					recoverButtonPerformed();
					saveButton.setEnabled(false);
					recoverButton.setEnabled(false);
					isChanged(selectTreeNode, controlGuide);
					return;
				} else {
					// 修改成功
					isChanged(selectTreeNode, controlGuide);
					return;
				}
			}

			isChanged(selectTreeNode, controlGuide);

		}

	}

}

