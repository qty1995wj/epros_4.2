package epros.designer.gui.task.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.ui.property.table.JecnDefaultTableModel;
import epros.designer.gui.task.Constants;
import epros.designer.util.JecnProperties;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 表组件的滚动面板
 * 
 * @author ZHOUXY
 * 
 */
public class TaskConfigTableScrollPane extends JScrollPane {
	protected final Log log = LogFactory.getLog(TaskConfigTableScrollPane.class);
	/** 配置界面 */
	protected TaskConfigDialog dialog = null;
	/** table对象 */
	protected JTable table = null;
	/** table对应模型对象 */
	protected JecnDefaultTableModel tableModel = null;

	/** 显示的数据对象 */
	protected List<TaskConfigItem> showItemList = null;

	public TaskConfigTableScrollPane(TaskConfigDialog dialog) {
		if (dialog == null) {
			log.error("JecnTableScrollPane dialog is error");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.dialog = dialog;

		// 初始化组件
		initComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 滚动面板
		this.setOpaque(false);
		this.setBorder(JecnUIUtil.getTootBarBorder());
		// 背景颜色
		this.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 初始化表
		initTable();

		this.setViewportView(table);
	}

	/**
	 * 
	 * 初始化表
	 * 
	 * @param headerName
	 *            String
	 */
	protected void initTable() {
		// 表头
		String[] header = { "id", "config", JecnProperties.getValue("name"), JecnProperties.getValue("EnglishName"),
				JecnProperties.getValue("defaultNames"), JecnProperties.getValue("defaultApprover"),
				JecnProperties.getValue("whetherRequired") };
		// 创建table模型
		tableModel = new JecnDefaultTableModel(header);
		// 创建表
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		// 设置用户是否可以拖动列头，以重新排序各列,设置为false
		table.getTableHeader().setReorderingAllowed(true);
		// 设置表头背景颜色
		table.getTableHeader().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			for (int col : cols) {
				TableColumn tableColumn = table.getColumnModel().getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(5).setCellRenderer(render);
		table.getColumnModel().getColumn(5).setMinWidth(80);
		table.getColumnModel().getColumn(5).setMaxWidth(80);

		// 自定义表头UI
		table.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	public int[] gethiddenCols() {

		return new int[] { 0, 1 };
	}

	/**
	 * 
	 * 加载表数据
	 * 
	 * @param showItemList
	 *            List<TaskConfigItem>
	 */
	public void initData(List<TaskConfigItem> configItems) {

		showItemList = configItems;
		// 清除表行
		DefaultTableModel df = (DefaultTableModel) table.getModel();
		// 反向删除
		for (int i = table.getRowCount() - 1; i >= 0; i--) {
			df.removeRow(i);
		}

		// 添加表行
		Object[][] tableData = getTableData(configItems);

		if (tableData == null) {
			return;
		}
		tableModel.setData(tableData);

		table.repaint();
	}

	// /**
	// *
	// * 获取选中行中最后一列对象
	// *
	// * @return
	// */
	// public List<TaskConfigItem> getSelectedItemBeanList() {
	//
	// List<TaskConfigItem> iTemBeanList = new
	// ArrayList<TaskConfigItem>();
	//
	// // 获取选中序号集合
	// int[] selectedRows = getSelectedRows();
	//
	// for (int i = 0; i < selectedRows.length; i++) {
	// // 选中行序号
	// int selectedIndex = selectedRows[i];
	// // 获取最后一列的选中数据
	// Object obj = this.table.getValueAt(selectedIndex, getBeanIndex());
	// if (obj instanceof TaskConfigItem) {
	// iTemBeanList.add((TaskConfigItem) obj);
	// }
	// }
	// return iTemBeanList;
	// }

	/**
	 * 
	 * 重新设置选中行(重置)
	 * 
	 * @param selectedItemBeanList
	 *            List<TaskConfigItem> 选中集合
	 */
	public void reSetSelectedItems(List<TaskConfigItem> selectedItemBeanList) {
		if (selectedItemBeanList == null || selectedItemBeanList.size() == 0) {
			return;
		}

		for (int i = 0; i < table.getRowCount(); i++) {
			Object obj = this.table.getValueAt(i, 1);
			if (obj instanceof TaskConfigItem) {
				for (TaskConfigItem selItemBean : selectedItemBeanList) {
					if (selItemBean == obj) {
						// 选中行
						table.addRowSelectionInterval(i, i);
					}
				}
			}
		}
	}

	/**
	 * 根据给定的序号添加选中行,如果选中行超过界限选中边界行
	 * 
	 */
	public void setSelectedRowToTable(int[] selectedIndexs) {

		if (selectedIndexs == null || selectedIndexs.length == 0) {
			return;
		}
		// 表行数
		int rowCount = table.getRowCount();
		if (rowCount <= 0) {
			return;
		}
		int maxRowIndex = rowCount - 1;
		for (int i = 0; i < selectedIndexs.length; i++) {
			int selectedIndex = selectedIndexs[i];
			if (selectedIndex >= 0 && selectedIndex <= maxRowIndex) {
				// 选中行
				table.addRowSelectionInterval(selectedIndex, selectedIndex);
			} else if (selectedIndex < 0) {
				// 小于下限
				table.addRowSelectionInterval(0, 0);
			} else {
				// 大于上限
				table.addRowSelectionInterval(maxRowIndex, maxRowIndex);
			}
		}
	}

	/**
	 * 
	 * 获取选中行
	 * 
	 * @return int[]
	 */
	public int[] getSelectedRows() {
		return table.getSelectedRows();
	}

	/**
	 * 
	 * 上移选中行
	 * 
	 */
	public void upShowUItem() {
		upDownShowUItem(true);
	}

	/**
	 * 
	 * 下移选中行
	 * 
	 */
	public void downShowUItem() {
		upDownShowUItem(false);
	}

	/**
	 * 
	 * 编辑表第一列选中数据
	 * 
	 */
	public void editName(List<JecnUser> users, String approveStageName, int isEmpty) {

		int[] selecIndex = getSelectedRows();
		if (selecIndex == null || selecIndex.length != 1) {
			return;
		}
		int index = selecIndex[0];

		String appendApprovePeopleName = appendApprovePeopleName(users);
		String isMustSelectStr = JecnProperties.getValue("task_notEmptyBtn");
		if (isEmpty == 0) {
			isMustSelectStr = JecnProperties.getValue("task_emptyBtn");
		}

		TaskConfigItem config = (TaskConfigItem) table.getValueAt(index, 1);
		config.setUsers(users);
		config.setIsEmpty(isEmpty);
		config.setName(approveStageName);

		// 获取第一列的选中数据
		table.setValueAt(config, index, 1);
		table.setValueAt(approveStageName, index, 2);
		table.setValueAt(appendApprovePeopleName, index, 4);
		table.setValueAt(isMustSelectStr, index, 5);
		table.repaint();
	}

	// /**
	// *
	// * 给定名称是否有重复情况
	// *
	// * @param name
	// * String 给定名称
	// * @return boolean true:参数为空或选中行不为1或有相同行；false：没有重复名称
	// */
	// public boolean existsNameSame(String name) {
	// if (Tool.isNullOrEmtryTrim(name)) {
	// return true;
	// }
	//
	// // 获取选中行
	// int[] selecIndex = getSelectedRows();
	// if (selecIndex == null || selecIndex.length != 1) {
	// return true;
	// }
	// int index = selecIndex[0];
	//
	// for (int i = 0; i < table.getRowCount(); i++) {
	// if (index == i) {// 除去选中行不判断
	// continue;
	// }
	// // 选中行第一列对象
	// String rowName = String.valueOf(table.getValueAt(i, 0));
	//
	// if (name.trim().equals(rowName)) {// 名称出现相同
	// return true;
	// }
	//
	// }
	//
	// // 判断隐藏项是否有相同
	// JecnConfigTypeDesgBean configTypeDesgBean =
	// dialog.getPropertyPanel().getConfigTypeDesgBean();
	// if (configTypeDesgBean == null) {
	// return true;
	// }
	// List<TaskConfigItem> hiddItemBeanList =
	// getHiddItem(configTypeDesgBean.getTableItemList(), false);
	// for (TaskConfigItem itemBean : hiddItemBeanList) {
	// if (name.trim().equals(itemBean.getName())) {// 名称出现相同
	// return true;
	// }
	// }
	//
	// return false;
	// }

	/**
	 * 
	 * 获取表数据
	 * 
	 * @param showItemList
	 *            List<TaskConfigItem>
	 * 
	 * @return Object[][]
	 */
	protected Object[][] getTableData(List<TaskConfigItem> configs) {

		if (configs == null || configs.size() == 0) {
			return null;
		}
		// 创建数组
		Object[][] data = new Object[configs.size()][7];

		for (int i = 0; i < configs.size(); i++) {
			TaskConfigItem item = configs.get(i);
			data[i][0] = item.getId();
			data[i][1] = item;
			data[i][2] = item.getName();
			data[i][3] = item.getEnName();
			data[i][4] = item.getDefaultName();
			data[i][5] = appendApprovePeopleName(item.getUsers());
			data[i][6] = item.getIsEmpty() == 0 ? JecnProperties.getValue("task_emptyBtn"): JecnProperties.getValue("task_notEmptyBtn");
		}
		return data;
	}

	private String appendApprovePeopleName(List<JecnUser> users) {
		if (users == null || users.size() == 0) {
			return "";
		}
		String approvePeopleName = "";
		for (JecnUser user : users) {
			approvePeopleName += getSplitChar() + user.getTrueName();
		}
		if (approvePeopleName.startsWith(getSplitChar())) {
			approvePeopleName = approvePeopleName.replaceFirst(getSplitChar(), "");
		}

		return approvePeopleName;
	}

	private String getSplitChar() {
		return ",";
	}

	/**
	 * 
	 * 获取必填值
	 * 
	 * @param isEmpty
	 *            Integer
	 * @param notEmptyText
	 *            String
	 * @return String
	 */
	public String getNotEmpty(Integer isEmpty, String notEmptyText) {
		if (isEmpty == null) {
			return "";
		}
		return (isEmpty.intValue() == JecnConfigContents.ITEM_NOT_EMPTY) ? notEmptyText : "";
	}

	/**
	 * 
	 * 此方法适用于对应数据库表中value2字段是存放序号的数据
	 * 
	 * 给定参数按照序号排序
	 * 
	 * @param itemBeanList
	 *            List<TaskConfigItem>
	 */
	private void sortConfigItemBeanList(List<TaskConfigItem> itemBeanList) {

		if (itemBeanList == null || itemBeanList.size() == 0) {
			return;
		}

		Collections.sort(itemBeanList, new Comparator() {

			public int compare(Object r1, Object r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("TaskConfigTableScrollPane sortConfigItemBeanList is error");
				}

				// 获取序号
				Integer sort1 = ((TaskConfigItem) r1).getSort();
				Integer sort2 = ((TaskConfigItem) r2).getSort();

				if (sort1 == null || sort2 == null) {
					return 0;
				}
				int sortId1 = sort1.intValue();
				int sortId2 = sort2.intValue();

				if (sortId1 < sortId2) {
					return -1;
				} else if (sortId1 > sortId2) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	/**
	 * 
	 * 上下移动选中行
	 * 
	 * @param upFlag
	 *            boolean true：上移；false：下移
	 */
	private void upDownShowUItem(boolean upFlag) {
		// 获取选中显示项
		List<TaskConfigItem> selectedItemBeanList = this.getSelectedItemBeanList();
		// 选中行
		int[] selectedIndexs = null;
		if (upFlag) {// 上移
			selectedIndexs = upShowUItem(showItemList, selectedItemBeanList);
		} else {// 下移
			selectedIndexs = downShowUItem(showItemList, selectedItemBeanList);
		}

		// 重新加载表数据
		setDataByBean(showItemList);
		// 设置选中行
		setSelectedRowToTable(selectedIndexs);
	}

	/**
	 * 
	 * 给表赋值
	 * 
	 * @param itemList
	 *            List<JecnConfigItemBean>
	 */
	private void setDataByBean(List<TaskConfigItem> itemList) {
		this.showItemList = itemList;

		// 清除表行
		DefaultTableModel df = (DefaultTableModel) table.getModel();
		// 反向删除
		for (int i = table.getRowCount() - 1; i >= 0; i--) {
			df.removeRow(i);
		}

		// 添加表行
		Object[][] tableData = getTableData(itemList);

		if (tableData == null) {
			return;
		}
		tableModel.setData(tableData);

		table.repaint();
	}

	public List<TaskConfigItem> getSelectedItemBeanList() {

		List<TaskConfigItem> configs = new ArrayList<TaskConfigItem>();

		// 获取选中序号集合
		int[] selectedRows = getSelectedRows();

		for (int i = 0; i < selectedRows.length; i++) {
			// 选中行序号
			int selectedIndex = selectedRows[i];

			TaskConfigItem config = (TaskConfigItem) this.table.getValueAt(selectedIndex, 1);
			configs.add(config);
		}
		return configs;
	}

	/**
	 * 
	 * 上移显示项
	 * 
	 * @param showItemBeanList
	 *            List<TaskConfigItem>
	 * @param selectedItemBeanList
	 *            List<TaskConfigItem>
	 * 
	 */
	private int[] upShowUItem(List<TaskConfigItem> showItemBeanList, List<TaskConfigItem> selectedItemBeanList) {
		if (showItemBeanList == null || showItemBeanList.size() == 0 || selectedItemBeanList == null
				|| selectedItemBeanList.size() == 0) {
			return null;
		}
		// 存储下一次选中行序号
		int[] selecteIndexs = new int[selectedItemBeanList.size()];

		// ******************************************start
		// 上移
		// 被选中第一行到表第一行就不移动了
		int index = showItemBeanList.indexOf(selectedItemBeanList.get(0));
		if (index <= 0) {
			for (int k = 0; k < selectedItemBeanList.size(); k++) {
				selecteIndexs[k] = showItemBeanList.indexOf(selectedItemBeanList.get(k));
			}
			return selecteIndexs;
		}

		// ******************************************end

		for (int k = 0; k < selectedItemBeanList.size(); k++) {

			// 获取选中行对象
			TaskConfigItem selectedItemBean = selectedItemBeanList.get(k);

			// 当前选中行
			int indexCurr = showItemBeanList.indexOf(selectedItemBean);

			// 上一行数据对象序号
			int upIndex = indexCurr - 1;

			// 上一行的bean对象
			TaskConfigItem upRowTtemBean = showItemBeanList.get(upIndex);
			// 判断上一行是否是选中，是选中那说明逻辑错误，返回null
			if (selectedItemBeanList.contains(upRowTtemBean)) {
				// 是选中行
				return null;
			}
			// 上一行或下一行的序号
			int sortId = upRowTtemBean.getSort();
			// 选中序号
			int selectedSortId = selectedItemBean.getSort();

			upRowTtemBean.setSort(selectedSortId);
			selectedItemBean.setSort(sortId);

			// 重新排序
			// 按照序号排序
			showItemBeanList.remove(selectedItemBean);
			showItemBeanList.add(upIndex, selectedItemBean);

			selecteIndexs[k] = upIndex;
		}
		return selecteIndexs;
	}

	/**
	 * 
	 * 下移显示项
	 * 
	 * @param showItemBeanList
	 *            List<TaskConfigItem>
	 * @param selectedItemBeanList
	 *            List<TaskConfigItem>
	 * 
	 */
	private int[] downShowUItem(List<TaskConfigItem> showItemBeanList, List<TaskConfigItem> selectedItemBeanList) {
		if (showItemBeanList == null || showItemBeanList.size() == 0 || selectedItemBeanList == null
				|| selectedItemBeanList.size() == 0) {
			return null;
		}
		// 存储下一次选中行序号
		int[] selecteIndexs = new int[selectedItemBeanList.size()];

		// ******************************************start
		// 被选中第最后一行到表最后一行就不移动了
		int index = showItemBeanList.indexOf(selectedItemBeanList.get(selectedItemBeanList.size() - 1));
		if (index >= showItemBeanList.size() - 1) {
			for (int k = 0; k < selectedItemBeanList.size(); k++) {
				selecteIndexs[k] = showItemBeanList.indexOf(selectedItemBeanList.get(k));
			}
			return selecteIndexs;
		}
		// ******************************************end

		for (int k = selectedItemBeanList.size() - 1; k >= 0; k--) {

			// 获取选中行对象
			TaskConfigItem selectedItemBean = selectedItemBeanList.get(k);

			// 当前选中行
			int indexCurr = showItemBeanList.indexOf(selectedItemBean);

			// 上一行数据对象
			int upIndex = indexCurr + 1;

			// 上一行的bean对象
			TaskConfigItem upRowTtemBean = showItemBeanList.get(upIndex);
			// 判断上一行是否是选中，是选中那说明逻辑错误，返回null
			if (selectedItemBeanList.contains(upRowTtemBean)) {
				// 是选中行
				return null;
			}

			// 上一行或下一行的序号
			int sortId = upRowTtemBean.getSort();
			// 选中序号
			int selectedSortId = selectedItemBean.getSort();

			upRowTtemBean.setSort(selectedSortId);
			selectedItemBean.setSort(sortId);

			// 重新排序
			// 按照序号排序
			showItemBeanList.remove(selectedItemBean);
			showItemBeanList.add(upIndex, selectedItemBean);

			selecteIndexs[k] = upIndex;
		}
		return selecteIndexs;
	}

	public void delShowItem() {

		int[] selectedRows = this.getSelectedRows();

		if (selectedRows.length == 0) {
			return;
		}

		for (int index : selectedRows) {
			TaskConfigItem config = (TaskConfigItem) table.getValueAt(index, 1);
			// 设置为不显示
			config.setValue(Constants.ITEM_IS_NOT_SHOW);
		}

		List<TaskConfigItem> configs = getShowConfigItems(true);

		initData(configs);

		// 根据给定的序号添加选中行,如果选中行超过界限选中边界行
		this.setSelectedRowToTable(selectedRows);

	}

	public List<TaskConfigItem> getShowConfigItems(boolean isSort) {
		List<TaskConfigItem> showConfigs = new ArrayList<TaskConfigItem>();
		List<TaskConfigItem> configs = dialog.getConfigs();

		if (isSort) {
			Collections.sort(configs, new Comparator() {

				public int compare(Object r1, Object r2) {
					if (r1 == null || r2 == null) {
						throw new IllegalArgumentException("sortConfigItemBeanList is error");
					}

					// 获取序号
					Integer sort1 = ((TaskConfigItem) r1).getSort();
					Integer sort2 = ((TaskConfigItem) r2).getSort();

					if (sort1 == null || sort2 == null) {
						return 0;
					}
					int sortId1 = sort1.intValue();
					int sortId2 = sort2.intValue();

					if (sortId1 < sortId2) {
						return -1;
					} else if (sortId1 > sortId2) {
						return 1;
					} else {
						return 0;
					}
				}
			});
		}

		for (TaskConfigItem item : configs) {
			if (item.getMark().equals(ConfigItemPartMapMark.taskAppPubType.toString())) {
				continue;
			}
			if (item.getValue().equals(Constants.ITEM_IS_SHOW)) {
				showConfigs.add(item);
			}
		}
		return showConfigs;
	}

	public TaskConfigItem getApproveType() {

		List<TaskConfigItem> configs = dialog.getConfigs();
		if (configs == null || configs.size() == 0) {
			return null;
		}
		for (TaskConfigItem itemBean : configs) {
			if (itemBean.getMark().equals(ConfigItemPartMapMark.taskAppPubType.toString())) {
				return itemBean;
			}
		}

		throw new NullPointerException();

	}

	public List<TaskConfigItem> getHiddenConfigItems() {
		List<TaskConfigItem> showConfigs = new ArrayList<TaskConfigItem>();
		List<TaskConfigItem> configs = dialog.getConfigs();
		for (TaskConfigItem item : configs) {
			if (item.getMark().equals(ConfigItemPartMapMark.taskAppPubType.toString())) {
				continue;
			}
			if (item.getValue().equals(Constants.ITEM_IS_NOT_SHOW)) {
				showConfigs.add(item);
			}
		}
		return showConfigs;
	}

	public void initShowData() {
		initData(getShowConfigItems(true));
	}

	public void initHiddenData() {
		initData(getHiddenConfigItems());
	}

}
