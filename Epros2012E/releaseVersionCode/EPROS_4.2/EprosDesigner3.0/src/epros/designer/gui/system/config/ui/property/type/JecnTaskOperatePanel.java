package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;

import com.jecn.epros.server.common.JecnConfigContents;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnTableScrollPane;
import epros.designer.util.JecnProperties;

/**
 * 任务操作配置面板
 * 
 * @author ZHANGXIAOHU
 * 
 */
public class JecnTaskOperatePanel extends JecnAbstractPropertyBasePanel {
	public JecnTaskOperatePanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 表面板
		tableScrollPane = new JecnTableScrollPane(JecnProperties
				.getValue("task_TableHeaderName"), dialog);
		this.add(tableScrollPane, BorderLayout.CENTER);
	}

	@Override
	public boolean check() {
		return true;
	}

	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null
				|| !configTypeDesgBean.existsItemListData()) {
			return;
		}

		// 过滤隐藏项 TASK_OPERATE_ITEM_PUB = 7
		tableScrollPane.initData(tableScrollPane.getShowItem(configTypeDesgBean
				.getTableItemList(),
				JecnConfigContents.TYPE_SMALL_TASK_OPERATE_ITEM_PUB));
	}
}
