package com.jecn.epros.server.action.web.login.ad.nine999;

import java.net.URLDecoder;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.action.web.login.ad.dongh.JecnDesKeyTools;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.task.TaskSearchTempBean;

/**
 * 
 * 九新  单点
 * 
 * http://192.168.1.11:8080/login.action?token=32位随机+用户名+有效期+32位随机
 * 
 * @author Angus
 * 
 */
public class Nine999WebLoginAciton extends JecnAbstractADLoginAction {

	/** http中员工编号参数 */
	private static final String NUM_PARA = "EmployeeNumber";

//	static {
//		Nine999AfterItem.start();
//	}

	public Nine999WebLoginAciton(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 通过单点登录
	 * 
	 * @return String input;main;success;null
	 * 
	 */
	public String login() {
		try {
			return httpLogin();
		} catch (Exception e) {
			e.printStackTrace();
			return LoginAction.INPUT;
		}
	}

	/**
	 * HTTP 地址解析
	 * 
	 * @return String
	 * @date 2015-8-19 下午03:34:39
	 * @throws
	 */
	private String httpLogin() throws Exception {
		// http://172.20.29.55/login.action?LoginType=login_process&EmployeeNumber=19Z6eI+T99dfLpe0smyNCA==
		// request 获取连接传入 参数 ;URL 的加号 被转义为空格, 以下方式不会
		String queryString = "";
		queryString = loginAction.getRequest().getQueryString();
		if (StringUtils.isEmpty(queryString)) {
			return loginAction.loginGen();
		} 
		
		log.error("String = "+queryString);
		String[] params = queryString.split("\\&");
		for(String s:params){
			if( s.indexOf("token")>-1){
				queryString = s;
			}else if(s.indexOf("toke")>-1 ){
				queryString = s;
			}
			
		}
		queryString = URLDecoder.decode(queryString.substring(queryString.indexOf("=")+1));
		log.error("queryString = "+queryString);

		if (StringUtils.isBlank(queryString)) {// 单点登录名称为空，执行普通登录路径
			return loginAction.loginGen();
		}
		// 获取密钥解析对象
		JecnDesKeyTools tools = new JecnDesKeyTools();
		// 获取解密后用户
		String encodeNum = null;
		try {
			// 获取解密后用户
			encodeNum = tools.decode(queryString);
			log.error("解析后："+encodeNum);
		} catch (Exception e) {
			log.error("员工编号解密异常！");
			throw e;
		}
		
		String str = encodeNum.substring(14);
		String name = str.substring(0,str.length()-46);
		log.error("解析后名称："+name);
		return loginByLoginName(name);
	}

	/**
	 * 返回结果页
	 */
	public String returnPage() {
		return null;
	}


	/**
	 * 获得response
	 * 
	 * @return HttpServletResponse
	 */
	private HttpServletResponse getResponse() {
		return this.loginAction.getResponse();
	}

	/**
	 * 
	 * 用户名
	 * 
	 * @return
	 */
	private String getUserID() {
		return this.loginAction.getUserID();
	}

	/**
	 * 
	 * 密码
	 * 
	 * @return
	 */
	private String getUserPwd() {
		return this.loginAction.getUserPwd();
	}

	/**
	 * 
	 * 东航单点登录 标记本次验证是否需要登录 true为需要登录，false为不需要登
	 * 
	 * @return true为需要登录，false为不需要登
	 */
	private String getLogin() {
		return this.loginAction.getLogin();
	}

	/**
	 * 获得登陆bean
	 * 
	 * @param loginName
	 * @param password
	 * @param loginType
	 *            true为验证密码，false为不验证密码
	 * @return
	 * @throws Exception
	 */
	private WebLoginBean getWebLoginBean(String loginName, String password,
			boolean loginType) throws Exception {
		return this.loginAction.getPersonService().getWebLoginBean(loginName,
				password, loginType);
	}

	/**
	 * 我的任务获取登录人相关任务总数
	 * 
	 * @return List<MyTaskBean>登录人相关任务集合
	 * @param loginPeopleId
	 *            Long 登录
	 * @throws Exception
	 */
	private int getMyTaskCountBySearch(TaskSearchTempBean searchTempBean,
			Long loginPeopleId) throws Exception {
		return this.loginAction.getSearchService().getMyTaskCountBySearch(
				searchTempBean, loginPeopleId);
	}

	private long getPeopleId() {
		return this.loginAction.getPeopleId();
	}
}
