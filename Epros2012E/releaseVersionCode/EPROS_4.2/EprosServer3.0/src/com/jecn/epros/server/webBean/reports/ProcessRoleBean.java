package com.jecn.epros.server.webBean.reports;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 流程角色数统计
 * 
 * @author fuzhh Feb 28, 2013
 * 
 */
public class ProcessRoleBean {

	/** 角色数 0...16 当等于16时候 角色大于15*/
	private int roleCount;

	/** 合计 */
	private int allCount=0;

	/** 流程具体数据bean */
	private List<ProcessWebBean> processWebList = new ArrayList<ProcessWebBean>();

	public int getRoleCount() {
		return roleCount;
	}

	public void setRoleCount(int roleCount) {
		this.roleCount = roleCount;
	}

	public int getAllCount() {
		return allCount;
	}

	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}

	public List<ProcessWebBean> getProcessWebList() {
		return processWebList;
	}

	public void setProcessWebList(List<ProcessWebBean> processWebList) {
		this.processWebList = processWebList;
	}

}
