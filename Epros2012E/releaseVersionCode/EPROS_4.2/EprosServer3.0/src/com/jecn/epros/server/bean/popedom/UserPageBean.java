package com.jecn.epros.server.bean.popedom;

import java.util.List;

public class UserPageBean implements java.io.Serializable {
	private int totalCount;
	private int curPage;
	private int totalPage;
	private List<UserSearchBean> listUserSearchBean;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public List<UserSearchBean> getListUserSearchBean() {
		return listUserSearchBean;
	}

	public void setListUserSearchBean(List<UserSearchBean> listUserSearchBean) {
		this.listUserSearchBean = listUserSearchBean;
	}
}
