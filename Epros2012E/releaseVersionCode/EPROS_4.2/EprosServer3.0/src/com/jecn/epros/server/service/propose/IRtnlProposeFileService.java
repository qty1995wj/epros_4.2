package com.jecn.epros.server.service.propose;

import com.jecn.epros.server.bean.propose.JecnRtnlProposeFile;
import com.jecn.epros.server.common.IBaseService;

/**
 * 合理化建议附件Service 操作类
 * 2013-09-02
 *
 */
public interface IRtnlProposeFileService   extends IBaseService<JecnRtnlProposeFile, String>{

}
