package com.jecn.epros.server.service.process.history;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;

/**
 * 流程地图 架构
 * 
 * @author zhr
 * 
 */
public class JecnCommonProcessMapHistory {

	/**
	 * 发布流程地图基本信息(信息表包括流程地图关联附件ID) 架构 JECN_MAIN_FLOW_H
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnMainFlow(Long processId, Long historyId) {
		return "insert into JECN_MAIN_FLOW_H(FLOW_ID, FLOW_AIM, FLOW_AREA, FLOW_INPUT,"
				+ " FLOW_OUTPUT, FLOW_SHOW_STEP, FLOW_NOUN_DEFINE, FILE_ID,FLOW_DESCRIPTION,"
				+ "FLOW_START,FLOW_END,FLOW_KPI,TYPE_RES_PEOPLE,RES_PEOPLE_ID,HISTORY_ID,GUID,SUPPLIER,CUSTOMER,RISK_OPPORTUNITY)"
				+ " select mf.FLOW_ID, mf.FLOW_AIM, mf.FLOW_AREA, mf.FLOW_INPUT,"
				+ " mf.FLOW_OUTPUT, mf.FLOW_SHOW_STEP, mf.FLOW_NOUN_DEFINE,"
				+ "jf.version_id,mf.FLOW_DESCRIPTION,mf.FLOW_START,mf.FLOW_END,mf.FLOW_KPI,mf.TYPE_RES_PEOPLE,mf.RES_PEOPLE_ID,"
				+ historyId
				+ " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID()
				+ ",SUPPLIER,CUSTOMER,RISK_OPPORTUNITY  from JECN_MAIN_FLOW_T mf left join jecn_file_t jf on mf.file_id = jf.file_id where mf.flow_id="
				+ processId;
	}

	/**
	 * 集成关系图 线段 JECN_MAP_RELATED_LINE_H 添加线的线段的数据 架构
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnMapRelatedLineHistorySql(Long processId, Long historyId) {
		return "insert into JECN_MAP_RELATED_LINE_H(ID,FIGURE_ID,START_X,START_Y,END_X,END_Y,HISTORY_ID,GUID) "
				+ "select jlst.ID,jlst.FIGURE_ID,jlst.START_X,jlst.START_Y,jlst.END_X,jlst.END_Y," + historyId
				+ " HISTORY_ID," + JecnCommonSql.getSqlGUID()
				+ "  from JECN_MAP_RELATED_LINE_t jlst,JECN_MAP_STRUCTURE_IMAGE_T jfsit"
				+ "       where jlst.figure_id=jfsit.figure_id and jfsit.flow_id=" + processId;
	}

	/**
	 * 集成关系图 图形 JECN_MAP_STRUCTURE_IMAGE_H 架构 添加图形的数据
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnMapStructureImageHistorySql(Long processId, Long historyId) {
		return "insert into JECN_MAP_STRUCTURE_IMAGE_H "
				+ "(FIGURE_ID, FLOW_ID, FIGURE_TYPE, FIGURE_TEXT, "
				+ "START_FIGURE, END_FIGURE, X_POINT, Y_POINT, WIDTH, "
				+ "HEIGHT, FONT_SIZE, BGCOLOR, FONTCOLOR, LINK_FLOW_ID, "
				+ "LINECOLOR, FONTPOSITION, HAVESHADOW, CIRCUMGYRATE,"
				+ " BODYLINE, BODYCOLOR, ISERECT, FIGURE_IMAGE_ID, "
				+ "Z_ORDER_INDEX, ACTIVITY_SHOW, ACTIVITY_ID, FONT_BODY,"
				+ " FRAME_BODY, FONT_TYPE, ROLE_RES, IS_3D_EFFECT, "
				+ "FILL_EFFECTS, LINE_THICKNESS, START_TIME, STOP_TIME,"
				+ " TIME_TYPE, SHADOW_COLOR,INNER_CONTROL_RISK,STANDARD_CONDITIONS,"
				+ "ACTIVE_TYPE_ID,ISONLINE,DIVIDING_X,ACTIVITY_INPUT,ACTIVITY_OUTPUT,"
				+ "TARGET_VALUE,STATUS_VALUE,EXPLAIN,ELE_SHAPE,HISTORY_ID,GUID)"
				+ "SELECT 	IT.FIGURE_ID,"
				+ "         IT.FLOW_ID,"
				+ "         IT.FIGURE_TYPE,"
				+ "         IT.FIGURE_TEXT,"
				+ "         IT.START_FIGURE,"
				+ "         IT.END_FIGURE,"
				+ "         IT.X_POINT,"
				+ "         IT.Y_POINT,"
				+ "         IT.WIDTH,"
				+ "         IT.HEIGHT,"
				+ "         IT.FONT_SIZE,"
				+ "         IT. BGCOLOR,"
				+ "         IT.FONTCOLOR,"
				+ "         CASE"
				+ "           WHEN JF.VERSION_ID IS NULL THEN"
				+ "            IT.LINK_FLOW_ID"
				+ "           ELSE"
				+ "            JF.VERSION_ID"
				+ "         END AS LINK_FLOW_ID,"
				+ "         IT.LINECOLOR,"
				+ "         IT. FONTPOSITION,"
				+ "         IT.HAVESHADOW,"
				+ "         IT. CIRCUMGYRATE,"
				+ "         IT.BODYLINE,"
				+ "         IT. BODYCOLOR,"
				+ "         IT.ISERECT,"
				+ "         IT.FIGURE_IMAGE_ID,"
				+ "         IT.Z_ORDER_INDEX,"
				+ "         IT. ACTIVITY_SHOW,"
				+ "         IT.ACTIVITY_ID,"
				+ "         IT. FONT_BODY,"
				+ "         IT. FRAME_BODY,"
				+ "         IT. FONT_TYPE,"
				+ "         IT. ROLE_RES,"
				+ "         IT. IS_3D_EFFECT,"
				+ "         IT. FILL_EFFECTS,"
				+ "         IT. LINE_THICKNESS,"
				+ "         IT. START_TIME,"
				+ "         IT. STOP_TIME,"
				+ "         IT. TIME_TYPE,"
				+ "         IT.SHADOW_COLOR,"
				+ "         IT.INNER_CONTROL_RISK,"
				+ "         IT. STANDARD_CONDITIONS,"
				+ "         IT. ACTIVE_TYPE_ID,"
				+ "         IT.ISONLINE,"
				+ "         IT.DIVIDING_X,"
				+ "         IT. ACTIVITY_INPUT,"
				+ "         IT.ACTIVITY_OUTPUT,"
				+ "         IT. TARGET_VALUE,"
				+ "         IT. STATUS_VALUE,"
				+ "         IT. EXPLAIN,ELE_SHAPE,"
				+ historyId
				+ " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID()
				+ " FROM JECN_MAP_STRUCTURE_IMAGE_T IT LEFT JOIN JECN_FILE_T JF ON IT.LINK_FLOW_ID = JF.FILE_ID AND JF.DEL_STATE = 0"
				+ "AND IT.FIGURE_TYPE = " + JecnCommonSql.getIconString() + " WHERE IT.FLOW_ID=" + processId;
	}

	/**
	 * 记录流程(地图)架构文控
	 * 
	 * @param ruleId
	 * @param historyId
	 * @param baseDao
	 */
	public static void recordProcessMapHistory(Long processId, Long historyId, IBaseDao baseDao) {
		baseDao.execteNative(getJecnMainFlow(processId, historyId));// 发布流程地图基本信息(信息表包括流程地图关联附件ID)
		baseDao.execteNative(getJecnMapRelatedLineHistorySql(processId, historyId));// 集成关系图
																					// 线段
		baseDao.execteNative(getJecnMapStructureImageHistorySql(processId, historyId));// 集成关系图
																						// 图形
	}

}
