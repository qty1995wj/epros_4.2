package com.jecn.epros.email;

import com.jecn.epros.domain.email.EmailTip;
import org.apache.commons.lang3.StringUtils;

public class EmailContentBuilder {

    /**
     * 邮件信息换行
     */
    public static final String strBr = "<br>";

    /**
     * 内容
     */
    private String content;
    /**
     * 外部链接地址
     */
    private String resourceUrl;
    private EmailTip emailTip;

    public EmailContentBuilder(EmailTip emailTip) {
        this.emailTip = emailTip;
    }

    public String buildContent() {
        StringBuilder sb = new StringBuilder();
        sb.append(content);
        sb.append(strBr);
        sb.append(getTip());
        return sb.toString();
    }

    private String getTip() {
        // 存在需要打开的Epros资源链接时，写入固定的提示信息
        if (StringUtils.isNotBlank(resourceUrl)) {

            StringBuilder sb = new StringBuilder();


            String mailAddrTip = emailTip.getAddressTip();// 邮件地址提示
            String eprosResourceUrl = initEprosResourceUrl(emailTip.getAddress());// Epros资源链接地址

            if (StringUtils.isNotBlank(mailAddrTip)) {
                sb.append(mailAddrTip);
            }

            sb.append(eprosResourceUrl);
            sb.append(strBr);

            if (StringUtils.isNotBlank(emailTip.getPasswordTip())) {
                String mailPwdTip = emailTip.getPasswordTip();// 邮件密码提示
                sb.append(mailPwdTip);
                sb.append(strBr);
            }

            if (StringUtils.isNotBlank(emailTip.getInscribe())) {
                String mailEndContent = emailTip.getInscribe();// 邮件落款
                sb.append(mailEndContent);
                sb.append(strBr);
            }

            return sb.toString();
        } else {
            return "";
        }
    }

    /**
     * 初始化Epros资源链接地址
     *
     * @param eprosServerUrl
     * @return
     */
    private String initEprosResourceUrl(String eprosServerUrl) {
        return "<a href = '" + eprosServerUrl + resourceUrl + "'>"
                + eprosServerUrl + "</a>" + strBr;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public EmailTip getEmailTip() {
        return emailTip;
    }

    public void setEmailTip(EmailTip emailTip) {
        this.emailTip = emailTip;
    }

    public static String getStrbr() {
        return strBr;
    }


}
