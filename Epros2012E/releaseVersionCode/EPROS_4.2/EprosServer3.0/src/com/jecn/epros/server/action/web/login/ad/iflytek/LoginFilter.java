//package com.jecn.epros.server.action.web.login.ad.iflytek;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.log4j.Logger;
//
//import com.iflytek.common.util.sso.model.UserInfo;
//import com.iflytek.common.util.sso.tool.SSOClient;
//import com.iflytek.common.util.sso.tool.SSOConst;
//import com.jecn.epros.server.util.JecnPath;
//
///**
// * <p>
// * <code>LoginFilter</code>
// * </p>
// * 
// * @author jwsong
// * @version 2017/4/28 17:27
// * @since 1.0
// */
//public class LoginFilter extends HttpServlet implements Filter {
//
//	/**
//	 * 服务器根路径
//	 */
//	private static String serverRoot;
//
//	/**
//	 * 日志对象
//	 */
//	private Logger logger = Logger.getLogger(this.getClass());
//
//	@Override
//	public void init(FilterConfig filterConfig) throws ServletException {
//		serverRoot = JecnIflytekLoginItem.SSOProperties().getProperty("serverRoot");
//	}
//
//	@Override
//	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
//			throws IOException, ServletException {
//		try {
//			noFilterAction();
//			SSOConst sso = new SSOConst();
//			HttpServletRequest request = (HttpServletRequest) servletRequest;
//			HttpServletResponse response = (HttpServletResponse) servletResponse;
//
//			HttpSession session = request.getSession(true);
//
//			UserInfo userInfo = (UserInfo) session.getAttribute("CurrentUser");
//			// 物理路径 此处路径可以配置成常量
//			// String storePath = request.getServletContext().getRealPath("/") +
//			// "WEB-INF\\classes\\sso\\ssostore.jks";
//			String storePath = JecnPath.CLASS_PATH + "cfgFile/iflytek/ssostore.jks";
//			// D:/EPROS/EPROSV4.1/apache-tomcat-6.0.30/webapps/EprosServer3.0/WEB-INF/classes/cfgFile/iflytek/ssostore.jks
//			sso.setConfigFilePath("/cfgFile/iflytek/ssoconfig", storePath);
//
//			String url = request.getRequestURI() == null ? serverRoot : serverRoot + request.getRequestURI();
//
//			if (!url.contains("/login.action")) {
//				chain.doFilter(servletRequest, servletResponse);
//				return;
//			}
//			// // 无需拦截的请求 此处可在init-param中配置
//			// for (String noFilterUrl : list) {
//			// if (url.contains(noFilterUrl)) {
//			// chain.doFilter(servletRequest, servletResponse);
//			// return;
//			// }
//			// }
//
//			logger.info("url = " + url);
//			// 此处url 可处理拼接参数
//			if (null != userInfo) {
//				boolean flag = true;
//				try {
//					String address = request.getRemoteAddr();
//					String userId = userInfo.getUniqueID();
//					flag = SSOClient.isHasLogined(userId, address);
//				} catch (Exception e) {
//					logger.error("检查单点登录状态失败", e);
//				}
//				// 当前用户缓存存在，单点登录验证不通过，执行SSO登出重新跳转到登陆
//				if (!flag) {
//					logger.info("logoutstart");
//					SSOClient.Logout(request, response, url);
//					return;
//				} else {
//					chain.doFilter(servletRequest, servletResponse);
//					return;
//				}
//			} else {
//				// 设置本地SSO返回用户缓存
//				if (SSOClient.IsSSOResponse(request)) {
//					logger.info("SSO  request success!");
//					UserInfo user = SSOClient.GetUserInfo(request);
//					logger.info("user = " + user.getAccountName());
//					if (null == user) {
//						// 再次跳转到SSO
//						logger.info("SSOClient  GetUserInfo error!");
//						SSOClient.Login(response, url);
//						return;
//					}
//					// todo 设置本地用户信息session
//					session.setAttribute("CurrentUser", user);
//					chain.doFilter(servletRequest, servletResponse);
//				} else {
//					logger.info("SSO  request error!");
//					// ajax请求做其他处理 设置过期
//					if (request.getHeader("X-Requested-With") != null
//							&& "XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"))) {
//						response.setContentType("application/Json");
//						PrintWriter printWriter = response.getWriter();
//						printWriter.print("sessionTimeout");
//						printWriter.flush();
//						printWriter.close();
//						// 此处可跳转，可重写，按需处理
//					} else {
//						// 跳转到单点登陆SSO
//						SSOClient.Login(response, url);
//					}
//				}
//			}
//		} catch (Exception e) {
//			logger.error("", e);
//			try {
//				throw e;
//			} catch (Exception e1) {
//				e1.printStackTrace();
//			}
//		}
//	}
//
//	private static List<String> list = null;
//
//	private void noFilterAction() {
//		if (list == null) {
//			list = new ArrayList<String>();
//		}
//		list.add("/taskLogin.action");
//		list.add("/downloadFile.action");
//		list.add("/printFlowDoc.action");
//		list.add("/downloadRuleFile.action");
//		list.add("/downloadMapIconAction.action");
//		list.add("/viewDoc.action");
//		list.add("/loginTaskApprove.action");
//		list.add("/loginMail.action");
//		list.add("/loginPublicApprove.action");
//
//		list.add("/process.action");
//		list.add("/rule.action");
//		list.add("/file.action");
//		list.add("/getSyncConfig.action");
//	}
//
//}
