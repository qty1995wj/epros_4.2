package epros.designer.gui.standard;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class StandardTreeListener extends JecnTreeListener{
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(StandardTreeListener.class);

	public StandardTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}
	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if(!jTree.isAllowExpand()){
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = ConnectionPool.getStandardAction().getChildStandards(node.getJecnTreeBean().getId(),JecnConstants.projectId);
				if(list!=null&&((HighEfficiencyStandardTree)jTree).getTreeType()==1){
					for(int i=list.size()-1;i>=0;i--){
						JecnTreeBean treeBean=list.get(i);
						if(treeBean.getTreeNodeType()==TreeNodeType.standardProcess||treeBean.getTreeNodeType()==TreeNodeType.standardProcessMap){
							list.remove(treeBean);
						}
					}
				}
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("StandardTreeListener treeExpanded is error", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
		
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if(!jTree.isAllowExpand()){
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}
	
	
	}


