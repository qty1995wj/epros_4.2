package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseRoleFigure;

/**
 * 虚拟角色
 * 
 * @author ZHANGXH
 * @date： 日期：Mar 28, 2012 时间：4:53:02 PM
 */
public class VirtualRoleFigure extends JecnBaseRoleFigure {

	public VirtualRoleFigure(JecnFigureData figureData) {
		super(figureData);
	}

}
