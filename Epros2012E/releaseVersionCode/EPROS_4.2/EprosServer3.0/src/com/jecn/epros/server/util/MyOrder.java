package com.jecn.epros.server.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * <p>用于对特殊的序列进行排序</p>
 * 
 * @version 1.0
 * @author wangjh
 *
 */
public class MyOrder {	
	/**
	 * 测试用
	 * @param args
	 */
	public static void main(String[] args) {
	   List<String> list = new ArrayList<String>();
	   list.add("111c12cc");
	   list.add("12");
	   list.add("1a");
	   list.add("1b");
	   list.add("11");
	   list.add("2");
	   list.add("02");
	   list.add("11a");
	   list.add("1c");
	   list.add("1c11");
	   list.add("1c12a");
	   list.add("1");
	   list.add("");
	   list.add("0");
	   list.add("01");
	   list.add("01aa");
	   list.add("0b");
	   list.add("00");
	   list.add("11b");
	   list.add("01");
	   MyOrder mo = new MyOrder();
	   list = mo.order(list);
	}
	/**
	 * 进行排序的入口,返回一个String类型的List
	 * @param col
	 * @return  List<String>
	 */
	public List<String> order(Collection<String> col){
		//定义变量
		String resiveStr;
		List<String> returnLst;
		ArrayList<Nodes> list; //用于存储从集合中得到的数据,并进行整理
		Nodes node;
		if(col == null || col.size() < 1){
			return new ArrayList<String>();
		}
		//初始化
		list = new ArrayList<Nodes>();
		Iterator<?> it;
		returnLst = new ArrayList<String>();
		
		it = col.iterator();
		
		while(it.hasNext()){
			resiveStr = (String)it.next();
			node = new Nodes(resiveStr);
			list.add(node);
		}
		//进行排序
		list = (ArrayList<Nodes>)orderGo(list);
		
		//取出排序的结果
		it = list.iterator();
		while(it.hasNext()){
			returnLst.add(((Nodes)it.next()).getStrAll());
		}
		
		return returnLst;
	}
	
	/**
	 * 返回一个字符串类型的数组
	 * @param col
	 * @return
	 */
	public String[] getArrOrder(Collection<String> col){
		return (String[])order(col).toArray();
	}
	
	/**
	 * 进行排序的逻辑
	 * @param list
	 * @return List<Nodes>
	 */
	private List<Nodes> orderGo(List<Nodes> list){
		
		//排序
		Collections.sort(list);
		return list;
	}

}

/**
 * 对字符串进行处理的类
 * @author wangjh
 *
 */
class Nodes implements Comparable{
	private  String strAll;//存储原始字符串
	private  int length;//存储字符串的长度
	private  boolean isNum;//正在读取的是否是数字,初始化由第一位决定
	private  List<String> list = new ArrayList<String>();//保存字符串的片段

	/**
	 * 带参数的构造函数
	 * @param str
	 */
	public Nodes(String str){
		/*
		 * 设置值
		 */
		if(str == null){
			
			strAll = "";
		}else{
			this.strAll = str;
		}
        //初始化操作
		init();
				
	}
	/**
	 * 对数据进行初始化操作
	 */
	private void init(){
		if(this.strAll.length() == 0){
			this.length = 0;
			this.isNum = false;
		}else{
			this.length = this.strAll.length();
			getMidOrder();
		}
	}

	/**
	 * 得到字符串的全部段落
	 * @param str
	 * @return
	 */
	private void getMidOrder(){
		String str;
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < this.length; i++){
						
			str = this.strAll.substring(i, i+1);
			//设置初始值
			if(i == 0){
				this.isNum = isNumber(str);
			}
			
			if(isNumber(str) == this.isNum){
				sb.append(str);
			}else{
				this.list.add(sb.toString());
				sb = new StringBuffer(str);
				this.isNum = isNumber(str);
			}
		}
		this.list.add(sb.toString());
	}
	
	/**
	 * 判断是否是数字
	 * @param str
	 * @return
	 */
	private boolean isNumber(String str){
		return str.matches("[0-9]+");
	}
	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}
	
	/**
	 * @return the strAll
	 */
	public String getStrAll() {
		return strAll;
	}

	public int compareTo(Object o) {
		Nodes node = (Nodes) o;
		String thisStr;
		String thatStr;
		int intThisList = this.list.size();
		int intThatList = node.list.size();
		// 判断是否有为空,如果有空,则空优先级较高
		if (intThisList == 0 && intThatList == 0) {
			return 0;
		} else if (intThisList == 0 && intThatList != 0) {
			return -1;
		} else if (intThisList != 0 && intThatList == 0) {
			return 1;
		}

		//开始进行比较
		//add start
		Integer thisInt = null;
		Integer thatInt = null;
		//add end
		for (int i = 0; i < compareInt(intThisList,intThatList); i++) {
			thisStr = this.list.get(i);
			thatStr = node.list.get(i);
			if(thisStr.equals(thatStr)){
				continue;
			}
			
			if (isNumber(thisStr) && !isNumber(thatStr)) {
				return 1;
			} else if (!isNumber(thisStr) && isNumber(thatStr)) {
				return -1;
			} else {
				if (isNumber(thisStr)) {
								if(thisStr.length()>9 || thatStr.length()>9)
								{
									return compareStr(thisStr, thatStr);
								}
										//add Start
										thisInt = Integer.valueOf(thisStr);
										thatInt = Integer.valueOf(thatStr);
										if(thisInt.intValue() == thatInt.intValue()){
										  if(thisInt.intValue() != 0){
											if(thisStr.length() > thatStr.length()){
												return -1;
											}else if(thisStr.length() < thatStr.length()){
												return 1;
											}else{
												continue;
											}
										  }else{
											  if(thisStr.length() > thatStr.length()){
													return 1;
												}else if(thisStr.length() < thatStr.length()){
													return -1;
												}else{
													continue;
												}  
										  }
						                }
									 if(isConZero(thisStr,thatStr)){
											   if(conZeroCount(thisStr) == 0 && conZeroCount(thatStr) != 0){
												   return 1;
											   }
											   if(conZeroCount(thatStr) == 0 && conZeroCount(thisStr) != 0){
												   return -1;
											   }
											   if(conZeroCount(thisStr) != conZeroCount(thatStr)){
												   return compareStr(thisStr, thatStr);
											   }else{
												   return compareNum(thisStr, thatStr);
											   }
									 }
										//add end
					         //modify start
                   return compareNum(thisStr, thatStr);
                   //modify end
				} else {
                   return compareStr(thisStr, thatStr);
				}
			}
		}
		//循环完了仍然相等
		if(intThisList > intThatList){
			return 1;
		}else if(intThisList < intThatList){
			return -1;
		}else{
			return 0;
		}
	}
	/**
	 * 比较字符串返回较小的
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	private int compareInt(int a, int b){
		return a>=b?b:a;
	}
	/**
	 * 判断是否有以0开始
	 * @param str1
	 * @param str2
	 * @return
	 */
	private boolean isConZero(String str1, String str2){
		return str1.matches("(^0)[0-9]*")?true:(str2.matches("(^0)[0-9]*")?true:false);
	}
	/**
	 * 查看一个数字字符串的前面包含的0个数
	 * @param str
	 * @return
	 */
	private int conZeroCount(String str){
		String sub;
		if(str == null){
			return 0;
		}
		int returnInt = 0;
		for(int i = 0; i < str.length(); i++){
			sub = str.substring(i, i+1);
			if("0".equals(sub)){
				returnInt++;
			}else{
				break;
			}
		}
		return returnInt;
	}
	
	/**
	 * 比较数字字符串的大小
	 * @param str1
	 * @param str2
	 * @return
	 */
	private int compareNum(String str1, String str2){
		Integer thisInt = null;
		Integer thatInt = null;
		List<Integer> list = null;
		
		thisInt = Integer.valueOf(str1);
		thatInt = Integer.valueOf(str2);
		list = new ArrayList<Integer>();
        list.add(thisInt);
        list.add(thatInt);
        Collections.sort(list);
        if(list.get(0).toString().equals(String.valueOf(thisInt.intValue()))){
     	   return -1;
        }
        else{
     	   return 1;
        }
	}
	/**
	 * 对字母字符串进行排序
	 * @param str1
	 * @param str2
	 * @return
	 */
	private int compareStr(String str1, String str2){
		    List<String> strList = null;
		    
		    strList = new ArrayList<String>();
        strList.add(str1);
        strList.add(str2);
        Collections.sort(strList);
        if(str1.equals(strList.get(0))){
     	   return -1;
        }else{
     	   return 1;
        }
	}
	
}

