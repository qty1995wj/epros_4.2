package epros.draw.gui.top.toolbar.element;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemOftenColorData;

/**
 * 
 * 流程元素常用色
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementOftenColorPanel extends
		JecnAbstarctFlowElementPanel {

	/** 最近使用的填充颜色:标签 */
	private JLabel oftenFillColorLabel = null;
	/** 最近使用的填充颜色:布局靠左 */
	private JecnElementButtonPanel oftenFillColorPanelLeft = null;
	/** 最近使用的填充颜色:布局靠中间 */
	private JecnElementButtonPanel oftenFillColorPanelConter = null;
	/** 最近使用的填充颜色:布局靠右 */
	private JecnElementButtonPanel oftenFillColorPanelRight = null;

	/** 最近使用的线条颜色:标签 */
	private JLabel oftenBodyColorLabel = null;
	/** 最近使用的线条颜色:布局靠左 */
	private JecnElementButtonPanel oftenBodyColorPanelLeft = null;
	/** 最近使用的线条颜色:布局靠中间 */
	private JecnElementButtonPanel oftenBodyColorPanelConter = null;
	/** 最近使用的线条颜色:布局靠右 */
	private JecnElementButtonPanel oftenBodyColorPanelRight = null;

	/** 最近使用的字体颜色:标签 */
	private JLabel oftenFontColorLabel = null;
	/** 最近使用的字体颜色:布局靠左 */
	private JecnElementButtonPanel oftenFontColorPanelLeft = null;
	/** 最近使用的字体颜色:布局靠中间 */
	private JecnElementButtonPanel oftenFontColorPanelConter = null;
	/** 最近使用的字体颜色:布局靠右 */
	private JecnElementButtonPanel oftenFontColorPanelRight = null;
	/** 常用颜色数据对象 */
	private JecnSystemOftenColorData systemOftenColorData = null;

	public JecnFlowElementOftenColorPanel(JecnFlowElementPanel flowElementPanel) {
		super(flowElementPanel);
		init();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	protected void initComponents() {
		// 最近使用的填充颜色:标签
		oftenFillColorLabel = new JLabel(flowElementPanel.getResourceManager()
				.getValue("fillOftenColor"), SwingConstants.RIGHT);
		// 最近使用的填充颜色:布局靠左
		oftenFillColorPanelLeft = new JecnElementButtonPanel(flowElementPanel,
				flowElementPanel.getSizeOftenColorPanel(), "fillOftenColorLeft");
		// 最近使用的填充颜色:布局靠中间
		oftenFillColorPanelConter = new JecnElementButtonPanel(
				flowElementPanel, flowElementPanel.getSizeOftenColorPanel(),
				"fillOftenColorCenter");
		// 最近使用的填充颜色:布局靠右
		oftenFillColorPanelRight = new JecnElementButtonPanel(flowElementPanel,
				flowElementPanel.getSizeOftenColorPanel(),
				"fillOftenColorRight");

		// 最近使用的线条颜色:标签
		oftenBodyColorLabel = new JLabel(flowElementPanel.getResourceManager()
				.getValue("lineOftenColor"), SwingConstants.RIGHT);
		// 最近使用的线条颜色:布局靠左
		oftenBodyColorPanelLeft = new JecnElementButtonPanel(flowElementPanel,
				flowElementPanel.getSizeOftenColorPanel(), "lineOftenColorLeft");
		// 最近使用的线条颜色:布局靠中间
		oftenBodyColorPanelConter = new JecnElementButtonPanel(
				flowElementPanel, flowElementPanel.getSizeOftenColorPanel(),
				"lineOftenColorCenter");
		// 最近使用的线条颜色:布局靠右
		oftenBodyColorPanelRight = new JecnElementButtonPanel(flowElementPanel,
				flowElementPanel.getSizeOftenColorPanel(),
				"lineOftenColorRight");

		// 最近使用的字体颜色:标签
		oftenFontColorLabel = new JLabel(flowElementPanel.getResourceManager()
				.getValue("fontOftenColor"), SwingConstants.RIGHT);
		// 最近使用的字体颜色:布局靠左
		oftenFontColorPanelLeft = new JecnElementButtonPanel(flowElementPanel,
				flowElementPanel.getSizeOftenColorPanel(), "fontOftenColorLeft");
		// 最近使用的字体颜色:布局靠中间
		oftenFontColorPanelConter = new JecnElementButtonPanel(
				flowElementPanel, flowElementPanel.getSizeOftenColorPanel(),
				"fontOftenColorCenter");
		// 最近使用的字体颜色:布局靠右
		oftenFontColorPanelRight = new JecnElementButtonPanel(flowElementPanel,
				flowElementPanel.getSizeOftenColorPanel(),
				"fontOftenColorRight");
	}

	/**
	 * 
	 * 布局
	 * 
	 */
	protected void initLayout() {

		Insets insets = new Insets(5, 5, 5, 5);

		GridBagConstraints c;
		// ************************最近使用的填充颜色************************//
		// 最近使用的填充颜色:标签
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				flowElementPanel.getInsetsFirst(), 0, 0);
		this.add(oftenFillColorLabel, c);
		// 最近使用的填充颜色:布局靠左
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(oftenFillColorPanelLeft, c);
		// 最近使用的填充颜色:布局靠中间
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(oftenFillColorPanelConter, c);
		// 最近使用的填充颜色:布局靠右
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(oftenFillColorPanelRight, c);
		// ************************最近使用的填充颜色************************//

		// ************************最近使用的线条颜色************************//
		// 最近使用的线条颜色:标签 */
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				flowElementPanel.getInsetsFirst(), 0, 0);
		this.add(oftenBodyColorLabel, c);
		// 最近使用的线条颜色:布局靠左 */
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(oftenBodyColorPanelLeft, c);
		// 最近使用的线条颜色:布局靠中间 */
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(oftenBodyColorPanelConter, c);
		// 最近使用的线条颜色:布局靠右 */
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(oftenBodyColorPanelRight, c);
		// ************************最近使用的线条颜色************************//

		// ************************最近使用的字体颜色************************//
		// 最近使用的字体颜色:标签
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				flowElementPanel.getInsetsFirst(), 0, 0);
		this.add(oftenFontColorLabel, c);
		// 最近使用的字体颜色:布局靠左
		c = new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(oftenFontColorPanelLeft, c);
		// 最近使用的字体颜色:布局靠中间
		c = new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(oftenFontColorPanelConter, c);
		// 最近使用的字体颜色:布局靠右
		c = new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(oftenFontColorPanelRight, c);
		// ************************最近使用的字体颜色************************//
	}

	public void initData(JecnAbstractFlowElementData flowElementData) {
		// 获取本地配置文件中常用色
		systemOftenColorData = JecnSystemData.readOftenColorData();

		// 判断配置文件中是否有填充颜色
		if (systemOftenColorData.getFillColorList() != null
				&& systemOftenColorData.getFillColorList().size() == 8) {
			// 最近使用的填充颜色:布局靠左
			oftenFillColorPanelLeft.setBackground(systemOftenColorData
					.getFillColorList().get(0));
			// 最近使用的填充颜色:布局靠中间
			oftenFillColorPanelConter.setBackground(systemOftenColorData
					.getFillColorList().get(1));
			// 最近使用的填充颜色:布局靠右 */
			oftenFillColorPanelRight.setBackground(systemOftenColorData
					.getFillColorList().get(2));
		}

		// 判断配置文件中是否有线条颜色
		if (systemOftenColorData.getLineColorList() != null
				&& systemOftenColorData.getLineColorList().size() == 8) {
			// 最近使用的线条颜色:布局靠左
			oftenBodyColorPanelLeft.setBackground(systemOftenColorData
					.getLineColorList().get(0));
			// 最近使用的线条颜色:布局靠中间
			oftenBodyColorPanelConter.setBackground(systemOftenColorData
					.getLineColorList().get(1));
			// 最近使用的线条颜色:布局靠右
			oftenBodyColorPanelRight.setBackground(systemOftenColorData
					.getLineColorList().get(2));
		}

		// 判断配置文件中是否有字体颜色
		if (systemOftenColorData.getFontColorList() != null
				&& systemOftenColorData.getFontColorList().size() == 8) {
			// 最近使用的字体颜色:布局靠左
			oftenFontColorPanelLeft.setBackground(systemOftenColorData
					.getFontColorList().get(0));
			// 最近使用的字体颜色:布局靠中间
			oftenFontColorPanelConter.setBackground(systemOftenColorData
					.getFontColorList().get(1));
			// 最近使用的字体颜色:布局靠右
			oftenFontColorPanelRight.setBackground(systemOftenColorData
					.getFontColorList().get(2));
		}

	}

	/**
	 * 
	 * 常用颜色更新到本地文件
	 * 
	 * @return boolean true：更新成功或无更新 false：更新失败
	 * 
	 */
	public boolean writeOftenColorToPropertyFile() {
		return JecnSystemData.exportColorToPropertyFile();
	}

	/**
	 * 
	 * 选择颜色，并且移动上两次选择颜色
	 * 
	 * @param Color
	 *            当前选择颜色
	 * 
	 */
	public void setCurrOftenColor(ToolBarElemType toolBarElemType, Color color) {
		if (systemOftenColorData == null) {
			return;
		}
		systemOftenColorData.setUpdateFlag(true);
		// 修改List集合
		JecnElementAttributesUtil.alertColorList(systemOftenColorData,
				toolBarElemType, color);
		// 更新panel
		this.initData(null);
	}

	public JLabel getOftenFillColorLabel() {
		return oftenFillColorLabel;
	}

	public JecnElementButtonPanel getOftenFillColorPanelLeft() {
		return oftenFillColorPanelLeft;
	}

	public JecnElementButtonPanel getOftenFillColorPanelConter() {
		return oftenFillColorPanelConter;
	}

	public JecnElementButtonPanel getOftenFillColorPanelRight() {
		return oftenFillColorPanelRight;
	}

	public JLabel getOftenBodyColorLabel() {
		return oftenBodyColorLabel;
	}

	public JecnElementButtonPanel getOftenBodyColorPanelLeft() {
		return oftenBodyColorPanelLeft;
	}

	public JecnElementButtonPanel getOftenBodyColorPanelConter() {
		return oftenBodyColorPanelConter;
	}

	public JecnElementButtonPanel getOftenBodyColorPanelRight() {
		return oftenBodyColorPanelRight;
	}

	public JLabel getOftenFontColorLabel() {
		return oftenFontColorLabel;
	}

	public JecnElementButtonPanel getOftenFontColorPanelLeft() {
		return oftenFontColorPanelLeft;
	}

	public JecnElementButtonPanel getOftenFontColorPanelConter() {
		return oftenFontColorPanelConter;
	}

	public JecnElementButtonPanel getOftenFontColorPanelRight() {
		return oftenFontColorPanelRight;
	}

}
