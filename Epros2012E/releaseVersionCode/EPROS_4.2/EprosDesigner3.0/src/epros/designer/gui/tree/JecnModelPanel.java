package epros.designer.gui.tree;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.designer.gui.process.mode.FlowModelHighEfficiencyTree;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程模板面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnModelPanel extends JPanel {
	/** 树面板的容器：滚动面板 */
	private JScrollPane treeScrollPane = null;
	/** 树 */
	private FlowModelHighEfficiencyTree tree = null;

	public JecnModelPanel() {
		initComponents();
	}

	private void initComponents() {

		// 树面板的容器：滚动面板
		treeScrollPane = new JScrollPane();
		// 树
		tree = new FlowModelHighEfficiencyTree();

		// 透明
		this.setOpaque(false);
		// 无边框
		this.setBorder(JecnUIUtil.getTootBarBorder());
		this.setLayout(new BorderLayout());

		// 树面板的容器：滚动面板
		treeScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treeScrollPane.setBorder(null);

		// 树
		tree.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		tree.setBorder(null);

		// 树面板添加到滚动容器中
		treeScrollPane.setViewportView(tree);

		this.add(treeScrollPane, BorderLayout.CENTER);
	}

	public FlowModelHighEfficiencyTree getTree() {
		return tree;
	}
}
