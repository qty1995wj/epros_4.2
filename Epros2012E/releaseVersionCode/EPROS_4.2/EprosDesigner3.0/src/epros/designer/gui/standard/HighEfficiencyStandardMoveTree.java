package epros.designer.gui.standard;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class HighEfficiencyStandardMoveTree extends JecnHighEfficiencyTree {
	private Logger log = Logger.getLogger(HighEfficiencyStandardMoveTree.class);

	public HighEfficiencyStandardMoveTree(List<Long> listIds) {
		super(listIds);
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new StandardMoveTreeListener(this.getListIds(), jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		try {
			list = ConnectionPool.getStandardAction().getChildStandards(
					Long.valueOf(0), JecnConstants.projectId);
		} catch (Exception e) {
			log.error("HighEfficiencyStandardMoveTree  getTreeModel is error", e);
		}
		
		// 根节点 "标准"
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.standardRoot,JecnProperties.getValue("ISO_Standard"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, this.getListIds());
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
