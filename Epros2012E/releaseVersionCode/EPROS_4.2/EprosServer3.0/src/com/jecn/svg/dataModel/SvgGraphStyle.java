package com.jecn.svg.dataModel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SvgGraphStyle implements Cloneable {

	private String fill;
	private String stroke;
	private String strokeWidth;
	private String strokeDashArray;
	private String transform; 
	private SvgGraphStyle graph3dStyle;
	// 存在shadow时，图形的宽和高需多减掉3像素（为同设计器样式一致）。
	private String shadow;

	public String getShadow() {
		return shadow;
	}

	public void setShadow(String shadow) {
		this.shadow = shadow;
	}

	public SvgGraphStyle getGraph3dStyle() {
		return graph3dStyle;
	}

	public void setGraph3dStyle(SvgGraphStyle graph3dStyle) {
		this.graph3dStyle = graph3dStyle;
	}

	@JsonProperty("stroke-width")
	public String getStrokeWidth() {
		return strokeWidth;
	}

	@JsonProperty("stroke-dasharray")
	public String getStrokeDashArray() {
		return strokeDashArray;
	}

	public String getTransform() {
		return transform;
	}

	public void appendTransform(String transform) {
		this.transform = (this.transform == null ? transform : this.transform + transform);
	}

	public void clearTransform() {
		this.transform = null;
	}

	public void setStrokeDashArray(String strokeDashArray) {
		this.strokeDashArray = strokeDashArray;
	}

	public String getStroke() {
		return stroke;
	}

	public void setStroke(String stroke) {
		this.stroke = stroke;
	}

	public void setStrokeWidth(String strokeWidth) {
		this.strokeWidth = strokeWidth;
	}

	public String getFill() {
		return fill;
	}

	public void setFill(String fill) {
		this.fill = fill;
	}

	@Override
	public SvgGraphStyle clone() {
		try {
			SvgGraphStyle cloneObj=(SvgGraphStyle) super.clone();
			cloneObj.clearTransform();
			return cloneObj;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}
