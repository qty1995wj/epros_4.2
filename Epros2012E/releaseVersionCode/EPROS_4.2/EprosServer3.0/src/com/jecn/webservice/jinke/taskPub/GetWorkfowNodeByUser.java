/**
 * GetWorkfowNodeByUser.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetWorkfowNodeByUser  implements java.io.Serializable {
    private java.lang.String userCode;

    private boolean showFavorite;

    private boolean isMobile;

    private java.lang.String parentCode;

    private java.lang.String searchKey;

    public GetWorkfowNodeByUser() {
    }

    public GetWorkfowNodeByUser(
           java.lang.String userCode,
           boolean showFavorite,
           boolean isMobile,
           java.lang.String parentCode,
           java.lang.String searchKey) {
           this.userCode = userCode;
           this.showFavorite = showFavorite;
           this.isMobile = isMobile;
           this.parentCode = parentCode;
           this.searchKey = searchKey;
    }


    /**
     * Gets the userCode value for this GetWorkfowNodeByUser.
     * 
     * @return userCode
     */
    public java.lang.String getUserCode() {
        return userCode;
    }


    /**
     * Sets the userCode value for this GetWorkfowNodeByUser.
     * 
     * @param userCode
     */
    public void setUserCode(java.lang.String userCode) {
        this.userCode = userCode;
    }


    /**
     * Gets the showFavorite value for this GetWorkfowNodeByUser.
     * 
     * @return showFavorite
     */
    public boolean isShowFavorite() {
        return showFavorite;
    }


    /**
     * Sets the showFavorite value for this GetWorkfowNodeByUser.
     * 
     * @param showFavorite
     */
    public void setShowFavorite(boolean showFavorite) {
        this.showFavorite = showFavorite;
    }


    /**
     * Gets the isMobile value for this GetWorkfowNodeByUser.
     * 
     * @return isMobile
     */
    public boolean isIsMobile() {
        return isMobile;
    }


    /**
     * Sets the isMobile value for this GetWorkfowNodeByUser.
     * 
     * @param isMobile
     */
    public void setIsMobile(boolean isMobile) {
        this.isMobile = isMobile;
    }


    /**
     * Gets the parentCode value for this GetWorkfowNodeByUser.
     * 
     * @return parentCode
     */
    public java.lang.String getParentCode() {
        return parentCode;
    }


    /**
     * Sets the parentCode value for this GetWorkfowNodeByUser.
     * 
     * @param parentCode
     */
    public void setParentCode(java.lang.String parentCode) {
        this.parentCode = parentCode;
    }


    /**
     * Gets the searchKey value for this GetWorkfowNodeByUser.
     * 
     * @return searchKey
     */
    public java.lang.String getSearchKey() {
        return searchKey;
    }


    /**
     * Sets the searchKey value for this GetWorkfowNodeByUser.
     * 
     * @param searchKey
     */
    public void setSearchKey(java.lang.String searchKey) {
        this.searchKey = searchKey;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetWorkfowNodeByUser)) return false;
        GetWorkfowNodeByUser other = (GetWorkfowNodeByUser) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userCode==null && other.getUserCode()==null) || 
             (this.userCode!=null &&
              this.userCode.equals(other.getUserCode()))) &&
            this.showFavorite == other.isShowFavorite() &&
            this.isMobile == other.isIsMobile() &&
            ((this.parentCode==null && other.getParentCode()==null) || 
             (this.parentCode!=null &&
              this.parentCode.equals(other.getParentCode()))) &&
            ((this.searchKey==null && other.getSearchKey()==null) || 
             (this.searchKey!=null &&
              this.searchKey.equals(other.getSearchKey())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserCode() != null) {
            _hashCode += getUserCode().hashCode();
        }
        _hashCode += (isShowFavorite() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isIsMobile() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getParentCode() != null) {
            _hashCode += getParentCode().hashCode();
        }
        if (getSearchKey() != null) {
            _hashCode += getSearchKey().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetWorkfowNodeByUser.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetWorkfowNodeByUser"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "UserCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("showFavorite");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ShowFavorite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isMobile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "IsMobile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ParentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "SearchKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
