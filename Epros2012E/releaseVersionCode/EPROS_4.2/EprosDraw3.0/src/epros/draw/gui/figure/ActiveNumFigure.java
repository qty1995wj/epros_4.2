package epros.draw.gui.figure;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;

import epros.draw.util.DrawCommon;

/**
 * 信息化显示
 * 
 * @author admin
 * 
 */
public class ActiveNumFigure extends JPanel {
	private String text;

	private Font font = new Font("宋体", 1, 12);

	public ActiveNumFigure() {
	}

	public void initSize(String text) {
		if (StringUtils.isEmpty(text)) {
			return;
		}
		this.text = text;
		int strWidth = getFontStringWidth(text);
		this.setSize(strWidth + 5, 16);
	}

	private int getFontStringWidth(String text) {
		FontMetrics fm = this.getFontMetrics(this.getFont());
		return fm.stringWidth(text);
	}

	public void paintComponent(Graphics g) {
		if (DrawCommon.isNullOrEmtryTrim(text)) {
			return;
		}
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.setFont(font);
		g2d.drawString(text, 2, 14);
	}

	public Font getFont() {
		return font;
	}
}
