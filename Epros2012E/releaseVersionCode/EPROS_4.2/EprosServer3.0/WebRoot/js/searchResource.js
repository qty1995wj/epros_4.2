$(document).ready(function () {
    /* config Start */
    var config = {
        selectedClass: "list-group-item-success",
        keyword: $("#keyword"),
        lastKeyword: $("#lastSearchKeyword"),
        searchBtn: $("#searchBtn"),
        result: $("#result"),
        resultNum: $("#resultNum"),
        pagination: $(".pagination"),
        totalPage: 0,
        searchType: 0
    };
    /* config End */


    /* spinner Start */
    var spinner = {
        obj: new Spinner({
            lines: 12,
            length: 7,
            width: 4,
            radius: 10,
            scale: 1.0,
            corners: 1,
            color: '#BEBEBE',
            opacity: 0.25,
            rotate: 0,
            direction: 1,
            speed: 1,
            trail: 100,
            fps: 20,
            zIndex: 2e9,
            className: 'spinner',
            top: '50%',
            left: '50%',
            shadow: false,
            hwaccel: false,
            position: 'absolute'
        }),
        layer: $("#spinnerLayer"),
        show: function () {
            this.layer.css("display", "block");
            this.obj.spin(document.getElementById("spinner"));
        },
        hide: function () {
            this.obj.spin();
            this.layer.css("display", "none");
        }
    };
    /* spinner End */


    /* Actions Start*/
    config.keyword.focus();

    config.searchBtn.click(function (event) {
        var keyword = config.keyword.val();
        config.searchType = 0;
        $("#searchType a").each(function () {
            $(this).removeClass(config.selectedClass);
        });
        searchResourceNum(keyword, config.searchType);
        searchResource(keyword, config.searchType, 1);
    });

    $("#searchType a").on("click", function () {
        var a = $(this);
        if (a.hasClass(config.selectedClass)) {
            config.searchType = 0;
            a.removeClass(config.selectedClass);
        } else {
            $("#searchType a").each(function () {
                $(this).removeClass(config.selectedClass);
            });
            config.searchType = a.attr("type");
            a.addClass(config.selectedClass);
        }
        var lastKeyword = config.lastKeyword.val();
        searchResourceNum(lastKeyword, config.searchType);
        searchResource(lastKeyword, config.searchType, 1);
    });

    config.keyword.on("keypress", function (event) {
        if (event.keyCode == "13") {
            config.searchBtn.click();
        }
    });

    config.pagination.on("click", "li", function () {
        var li = $(this);
        var p = parseInt(li.attr("p"));
        if (p == undefined || p == 0 || p > config.totalPage) {
            return;
        }
        var liArray = '<li  p="' + (p - 1) + '"><a href="javascript:;" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
        if (config.totalPage <= 11) {
            for (var i = 1; i <= config.totalPage; i++) {
                liArray += '<li  class="' + (i == p ? "active" : "") + '" p="' + i + '"><a href="javascript:;">' + i + '</a></li>';
            }
        } else {
            if (p >= 7) {
                liArray += '<li p="1"><a href="javascript:;">1</a></li>';
                liArray += '<li p="2"><a href="javascript:;">2</a></li>';
                liArray += '<li ><span>...</span></li>';

                if (config.totalPage - p <= 8) {
                    for (var i = config.totalPage - 8; i <= config.totalPage; i++) {
                        liArray += '<li class="' + (i == p ? "active" : "") + '"  p="' + i + '"><a href="javascript:;">' + i + '</a></li>';
                    }
                } else {
                    for (var i = p - 2; i <= p + 2; i++) {
                        liArray += '<li class="' + (i == p ? "active" : "") + '"  p="' + i + '"><a href="javascript:;">' + i + '</a></li>';
                    }
                    liArray += '<li ><span>...</span></li>';
                    liArray += '<li p="' + (config.totalPage - 1) + '"><a href="javascript:;">' + (config.totalPage - 1) + '</a></li>';
                    liArray += '<li p="' + (config.totalPage) + '"><a href="javascript:;">' + config.totalPage + '</a></li>';
                }


            } else {
                for (var i = 1; i <= 8; i++) {
                    liArray += '<li  class="' + (i == p ? "active" : "") + '" p="' + i + '"><a href="javascript:;">' + i + '</a></li>';
                }
                liArray += '<li ><span>...</span></li>';
                liArray += '<li p="' + (config.totalPage - 1) + '"><a href="javascript:;">' + (config.totalPage - 1) + '</a></li>';
                liArray += '<li p="' + (config.totalPage) + '"><a href="javascript:;">' + config.totalPage + '</a></li>';
            }
        }

        liArray += '<li p="' + (p + 1) + '"><a href="javascript:;" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
        config.pagination.empty();
        config.pagination.append(liArray);
        searchResource(config.lastKeyword.val(), config.searchType, p);
    });
    /* Actions End*/


    /* searchFunctions Start */
    var searchResourceNum = function (keyword, searchType) {
        //console.log(keyword + "--" + searchType);

        $.ajax({
                url: "keyWordSearchPagination.action",
                type: "post",
                data: {
                    searchKey: keyword || "",
                    searchType: searchType || 0
                },
                success: function (searchResult) {
                    var result = eval("(" + searchResult + ")");

                    /*"fileCount":0,"flowCount":0,"flowMapCount":26,"riskCount":0,"ruleCount":0,"resultNum":1,"totalPage":10,"curPage":1*/
                    switch (parseInt(searchType)) {
                        case 1:
                            $("a[type='1'] span").html(result.flowMapCount);
                            break;
                        case 2:
                            $("a[type='2'] span").html(result.flowCount);
                            break;
                        case 3:
                            $("a[type='3'] span").html(result.fileCount);
                            break;
                        case 4:
                            $("a[type='4'] span").html(result.ruleCount);
                            break;
                        case 5:
                            $("a[type='5'] span").html(result.standardCount);
                            break;
                        case 6:
                            $("a[type='6'] span").html(result.riskCount);
                            break;
                        case 0:
                        default:
                            $("a[type='1'] span").html(result.flowMapCount);
                            $("a[type='2'] span").html(result.flowCount);
                            $("a[type='3'] span").html(result.fileCount);
                            $("a[type='4'] span").html(result.ruleCount);
                            $("a[type='5'] span").html(result.standardCount);
                            $("a[type='6'] span").html(result.riskCount);
                            break;

                    }
                    if (result.resultNum != 0) {
                        config.keyword.focus();
                    }
                    config.resultNum.html(result.resultNum);
                    config.totalPage = result.totalPage;
                    result.curPage = 1;
                    if (result.resultNum == 0) {
                        config.pagination.empty();
                        return;
                    }
                    var liArray = '<li  p="' + (result.curPage - 1) + '"><a href="javascript:;" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                    if (result.totalPage <= 11) {
                        for (var i = 1; i <= result.totalPage; i++) {
                            liArray += '<li  class="' + (i == result.curPage ? "active" : "") + '"  p="' + i + '"><a href="javascript:;">' + i + '</a></li>';
                        }
                    } else {
                        for (var i = 1; i <= 8; i++) {
                            liArray += '<li  class="' + (i == result.curPage ? "active" : "") + '" p="' + i + '"><a href="javascript:;">' + i + '</a></li>';
                        }
                        liArray += '<li ><span>...</span></li>';
                        liArray += '<li p="' + (result.totalPage - 1) + '"><a href="javascript:;">' + (result.totalPage - 1) + '</a></li>';
                        liArray += '<li p="' + (result.totalPage) + '"><a href="javascript:;">' + result.totalPage + '</a></li>';
                    }
                    liArray += '<li p="' + (result.curPage + 1) + '"><a href="javascript:;" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                    config.pagination.empty();
                    config.pagination.append(liArray);
                }
            }
        );
    };

    /* Test pagination Start*/
    // var testPagination = function (result) {
    //     var liArray = '<li class="' + (result.curPage == 1 ? "disabled" : "") + '" p="' + (result.curPage - 1) + '"><a href="javascript:;" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
    //     if (result.totalPage <= 11) {
    //         for (var i = 1; i <= result.totalPage; i++) {
    //             liArray += '<li  class="' + (i == result.curPage ? "active" : "") + '"  p="' + i + '"><a href="javascript:;">' + i + '</a></li>';
    //         }
    //     } else {
    //         if (result.curPage >= 7) {
    //             liArray += '<li p="1"><a href="javascript:;">1</a></li>';
    //             liArray += '<li p="2"><a href="javascript:;">2</a></li>';
    //             liArray += '<li ><span>...</span></li>';
    //
    //             for (var i = result.curPage - 2; i <= result.curPage + 2; i++) {
    //                 liArray += '<li class="' + (i == result.curPage ? "active" : "") + '"  p="' + i + '"><a href="javascript:;">' + i + '</a></li>';
    //             }
    //             if (i == result.totalPage - 2) {
    //                 liArray += '<li p="' + (result.totalPage - 2) + '"><a href="javascript:;">' + (result.totalPage - 2) + '</a></li>';
    //             } else {
    //                 liArray += '<li ><span>...</span></li>';
    //             }
    //             liArray += '<li p="' + (result.totalPage - 1) + '"><a href="javascript:;">' + (result.totalPage - 1) + '</a></li>';
    //             liArray += '<li p="' + (result.totalPage) + '"><a href="javascript:;">' + result.totalPage + '</a></li>';
    //         } else {
    //             for (var i = 1; i <= 8; i++) {
    //                 liArray += '<li  class="' + (i == result.curPage ? "active" : "") + '" p="' + i + '"><a href="javascript:;">' + i + '</a></li>';
    //             }
    //             liArray += '<li ><span>...</span></li>';
    //             liArray += '<li p="' + (result.totalPage - 1) + '"><a href="javascript:;">' + (result.totalPage - 1) + '</a></li>';
    //             liArray += '<li p="' + (result.totalPage) + '"><a href="javascript:;">' + result.totalPage + '</a></li>';
    //         }
    //     }
    //     liArray += '<li class="' + (result.curPage == result.totalPage ? "disabled" : "") + '" p="' + (result.curPage + 1) + '"><a href="javascript:;" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
    //     config.pagination.empty();
    //     config.pagination.append(liArray);
    // };
    //testPagination({curPage: 1, totalPage: 11});
    //testPagination({curPage: 1, totalPage: 80});
    // testPagination({curPage: 7, totalPage: 80});
    /* Test pagination End*/

    var searchResource = function (keyword, searchType, p) {
        console.log(keyword + "---" + searchType + "+++" + p);
        spinner.show();
        $.ajax({
            url: "keyWordSearch.action",
            type: "post",
            data: {
                searchKey: keyword || "",
                searchType: searchType || 0,
                curPage: p || 1
            },
            success: function (searchResult) {
                /*
                 [{"fileCount":0,"flowCount":0,"flowMapCount":26,"riskCount":0,"ruleCount":0,"searchResultLists":[{"realtedName"
                 :"样品阶段","relatedId":2,"relatedType":1,"updateDate":"2015-09-06"},{"realtedName":"武思超","relatedId":462
                 ,"relatedType":1,"updateDate":"2015-09-21"}]
                 */
                var result = eval("(" + searchResult + ")");

                config.lastKeyword.val(keyword);
                spinner.hide();


                /*
                 $("a[type='1'] span").html(result.flowMapCount);
                 $("a[type='2'] span").html(result.flowCount);
                 $("a[type='3'] span").html(result.fileCount);
                 $("a[type='4'] span").html(result.ruleCount);
                 $("a[type='5'] span").html(result.standardCount);
                 $("a[type='6'] span").html(result.riskCount);
                 */
                /*
                 if (result.resultNum != 0) {
                 config.keyword.focus();
                 }
                 /
                 config.resultNum.html(result.resultNum);
                 */
                config.result.empty();


                if (result.searchResultLists && result.searchResultLists.length) {
                    var resultTags = "";
                    $.each(result.searchResultLists, function (i, item) {
                        resultTags += (function (r) {
                            //"realtedName":"电子控制器平台项目管理","relatedId":39,"relatedType":1,"updateDate":"2016-03-15"
                            var resultTag = '<tr><td>';
                            resultTag += '<div style="margin:5px 0px -4px 0px;"><div class="row">';
                            resultTag += '<div class="col-xs-8" style="word-break: break-all;word-wrap: break-word;"><p style="font-size: 16px;"><a target="_blank" href="openResource.action?relatedId=' + r.relatedId + '&relatedType=' + r.relatedType + '">' + r.realtedName + '</a></p></div>';
                            resultTag += '<div class="col-xs-2"><p class="text-right text-muted" style="font-size: 14px;line-height:22px;">' + r.relatedTitle + '</p></div>';
                            resultTag += '<div class="col-xs-2"><p class="text-right text-muted" style="font-size: 14px;line-height:22px;">' + r.updateDate + '</p></div>';
                            resultTag += '</div></td></tr>';
                            return resultTag;
                        }(this));
                    });
                    config.result.append(resultTags);
                }
            },
            error: function (request) {
            }
        });

    }
    /* searchFunctions End */

    // 初次加载时，默认触发一次查询
    searchResourceNum();
    searchResource();
});
