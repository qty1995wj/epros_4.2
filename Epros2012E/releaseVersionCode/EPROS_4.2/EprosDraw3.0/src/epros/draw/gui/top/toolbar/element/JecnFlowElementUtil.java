package epros.draw.gui.top.toolbar.element;

import java.awt.GraphicsEnvironment;
import java.util.Vector;

import javax.swing.JComboBox;

import epros.draw.util.JecnResourceUtil;

public class JecnFlowElementUtil {
	/** 字体大小默认选项序号 */
	private static int fontSizeDefaultIndex = 0;
	/** 字体大小选项值 */
	private static Vector<Integer> fontSizeVector = null;

	/** 字体名称默认选项序号 */
	private static int fontNameDefaultIndex = 0;
	/** 字体名称选项值 */
	private static Vector<String> fontNameVector = null;

	/** 字体位置默认选项序号 */
	private static int fontPostionDefaultIndex = 0;
	/** 字体位置选项值 */
	private static Vector<String> fontPostionVector = null;

	/** 线条类型默认选项序号 */
	private static int bodyStyleDefaultIndex = 0;
	/** 线条类型选项值 */
	private static Vector<String> bodyStyleVector = null;

	/** 线条粗细选项值 */
	private static final Object[] bodyWidthArray = new Object[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	private static final Object[] elementShapeArray = new Object[] { "", "三角形", "四边形", "五边形", "六边形", "椭圆", "等腰梯形",
			"手动输入" };

	/**
	 * 
	 * 获取字体名称下拉框
	 * 
	 * @return JComboBox 字体名称下拉框
	 */
	public static JComboBox getfontNameComboBox() {
		return new JComboBox(JecnFlowElementUtil.getFontNameVector());
	}

	/**
	 * 
	 * 获取字体大小下拉框
	 * 
	 * @return JComboBox 字体大小下拉框
	 */
	public static JComboBox getFontSizeComboBox() {
		return new JComboBox(JecnFlowElementUtil.getFontSizeVector());
	}

	/**
	 * 
	 * 获取字体位置下拉框
	 * 
	 * @return JComboBox 字体大小下拉框
	 */
	public static JComboBox getFontPostionComboBox() {
		return new JComboBox(JecnFlowElementUtil.getFontPostionVector());
	}

	/**
	 * 
	 * 获取线条类型下拉框
	 * 
	 * @return JComboBox 线条类型下拉框
	 */
	public static JComboBox getBodyStyleComboBox() {
		return new JComboBox(JecnFlowElementUtil.getBodyStyleVector());
	}

	/**
	 * 
	 * 获取线条类型的选项值 0：实线；1：虚线；2：点线；3：双线
	 * 
	 * @return Vector<String>
	 */
	public static Vector<String> getBodyStyleVector() {
		if (bodyStyleVector == null) {
			bodyStyleVector = new Vector<String>();

			// 实线
			bodyStyleVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("bodyStyleSolidLine"));
			bodyStyleDefaultIndex = bodyStyleVector.size() - 1;
			// 虚线
			bodyStyleVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("bodyStyleVirtualLine"));
			// 点线
			bodyStyleVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("bodyStylePointLine"));
			// 双线
			bodyStyleVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("bodyStyleDoubleLine"));
		}

		return bodyStyleVector;
	}

	/**
	 * 
	 * 获取字体名称值的集合
	 * 
	 * @return Vector<String>
	 */
	public static Vector<String> getFontNameVector() {

		if (fontNameVector == null) {
			fontNameVector = new Vector<String>();
			// 默认字体
			String fontNameDefaultValue = JecnResourceUtil.getJecnResourceUtil().getValue("fontNameDefaultValue");

			// GraphicsDevice 对象和 Font 对象的集合
			GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
			// 获取字体系列名称的数组
			String[] fontNames = graphicsEnvironment.getAvailableFontFamilyNames();

			for (int i = 0; i < fontNames.length; i++) {
				String fontName = fontNames[i];
				if (fontNameDefaultValue.equals(fontName)) {// 是默认值
					fontNameDefaultIndex = i;
				}
				fontNameVector.add(fontName);
			}
		}

		return fontNameVector;
	}

	/**
	 * 
	 * 获取字体位置的选项值
	 * 
	 * @return Vector<String> 字体位置的选项值
	 */
	public static Vector<String> getFontPostionVector() {

		if (fontPostionVector == null) {
			fontPostionVector = new Vector<String>();
			// 左上 0
			fontPostionVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("fontPositionNorthWest"));
			// 正上 1
			fontPostionVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("fontPositionNorth"));
			// 右上 2
			fontPostionVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("fontPositionNorthEast"));
			// 左中 3
			fontPostionVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("fontPositionWest"));
			// 正中 4
			fontPostionVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("fontPositionCenter"));
			// 字体位置默认值
			fontPostionDefaultIndex = fontPostionVector.size() - 1;
			// 右中 5
			fontPostionVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("fontPositionEast"));
			// 左下
			fontPostionVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("fontPositionSouthWest"));
			// 正下
			fontPostionVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("fontPositionSouth"));
			// 右下
			fontPostionVector.add(JecnResourceUtil.getJecnResourceUtil().getValue("fontPositionSouthEast"));
		}

		return fontPostionVector;
	}

	/**
	 * 
	 * 获取字体大小值的集合
	 * 
	 * @return Vector
	 */
	public static Vector<Integer> getFontSizeVector() {
		if (fontSizeVector == null) {
			fontSizeVector = new Vector<Integer>();
			fontSizeVector.add(2);
			fontSizeVector.add(4);
			fontSizeVector.add(6);
			fontSizeVector.add(8);
			fontSizeVector.add(9);
			fontSizeVector.add(10);
			fontSizeVector.add(11);
			fontSizeVector.add(12);
			fontSizeDefaultIndex = fontSizeVector.size() - 1;
			fontSizeVector.add(14);
			fontSizeVector.add(16);

			for (int i = 18; i <= 60; i = i + 2) {
				fontSizeVector.add(i);
			}
		}
		return fontSizeVector;
	}

	public static int getFontSizeDefaultIndex() {
		return fontSizeDefaultIndex;
	}

	public static int getFontNameDefaultIndex() {
		return fontNameDefaultIndex;
	}

	public static int getFontPostionDefaultIndex() {
		return fontPostionDefaultIndex;
	}

	public static int getBodyStyleDefaultIndex() {
		return bodyStyleDefaultIndex;
	}

	public static Object[] getBodyWidthArray() {
		return bodyWidthArray;
	}

	public static Object[] getElementshapearray() {
		return elementShapeArray;
	}

}
