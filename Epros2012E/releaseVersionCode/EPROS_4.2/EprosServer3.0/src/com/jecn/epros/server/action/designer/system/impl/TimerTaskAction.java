package com.jecn.epros.server.action.designer.system.impl;

import org.apache.log4j.Logger;

import com.jecn.epros.server.service.file.IDownLoadCountService;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.process.IFlowPKIService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.service.system.ISystemSendEmailToAdmin;
import com.jecn.epros.server.service.task.app.IJecnTaskApproveDelayService;
import com.jecn.epros.server.service.task.app.IJecnTaskApproveTipService;
import com.jecn.epros.server.service.task.app.ITaskRunTestService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 
 * 晚上12点定时执行
 * 
 * @author Administrator
 * 
 */
public class TimerTaskAction {

	private Logger log = Logger.getLogger(TimerTaskAction.class);

	public TimerTaskAction() {
	}

	public void timerExecte() {
		// 清除设计器被占用的组织、人员、流程
		removeDesignerResouce();
		// 服务到期提醒
		mailKeyTip();
		if (JecnUtil.isPubShow()) {// 有浏览端情况下
			// 流程有效期提醒
			processValidEmailTip();
			// 制度有效期提醒
			ruleValidEmailTip();
			// 试运行邮件提醒
			testRunEmailTip();
			// 任务审批超时提醒
			taskApproveEmailTip();
			// 搁置任务提醒
			delayedTaskEmailTip();
			// 清空每日文件下载次数统计
			clearFileDownloadCount();
			// 删除KPI临时图片
			deleteKpiTempImage();
			// 收藏文件更新
			favoriteUpdateSendEmail();
			// 任务审批完成邮件提醒
			taskFinishEmailTip();
			// 流程、文件等发布完成邮件提醒
			pubEmailTip();
			// 手动触发的延迟发送邮件代码
			handlPubEmailTip();
		}
	}

	private void pubEmailTip() {
		try {
			IFlowStructureService flowService = (IFlowStructureService) ApplicationContextUtil
					.getContext().getBean("FlowStructureServiceImpl");
			flowService.pubEmailTip();
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private void handlPubEmailTip() {
		try {
			IFlowStructureService flowService = (IFlowStructureService) ApplicationContextUtil
					.getContext().getBean("FlowStructureServiceImpl");
			flowService.handPubSendEmail();
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private void taskFinishEmailTip() {
		try {
			IFlowStructureService flowService = (IFlowStructureService) ApplicationContextUtil
					.getContext().getBean("FlowStructureServiceImpl");
			flowService.taskFinishSendEmail();
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private void favoriteUpdateSendEmail() {
		try {
			IFlowStructureService flowService = (IFlowStructureService) ApplicationContextUtil
					.getContext().getBean("FlowStructureServiceImpl");
			flowService.favoriteUpdateSendEmail();
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/**
	 * 烽火按照驱动规则发送邮件
	 */
	private void flowDriverSendEmail() {
		try {
			IFlowStructureService flowService = (IFlowStructureService) ApplicationContextUtil
					.getContext().getBean("FlowStructureServiceImpl");
			flowService.sendFlowDriverEmail();
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/**
	 * 删除KPI临时图片
	 */
	private void deleteKpiTempImage() {
		try {
			// 流程kpi
			IFlowPKIService flowPKIService = (IFlowPKIService) ApplicationContextUtil
					.getContext().getBean("flowPKIServiceImpl");
			// 清除流程下的KPI的图片
			flowPKIService.deleteKpiTempImage();
		} catch (Exception e) {
			log.error("删除KPI临时图片异常", e);
		}
	}

	/**
	 *清空每日文件下载次数统计
	 */
	private void clearFileDownloadCount() {
		try {
			// 下载次数统计service
			IDownLoadCountService downLoadCountService = (IDownLoadCountService) ApplicationContextUtil
					.getContext().getBean("downLoadCountServiceImpl");

			// 清空每日文件下载次数统计
			downLoadCountService.clearDownLoadCountData();
		} catch (Exception e) {
			log.error("清空每日文件下载次数统计异常", e);
		}
	}

	/**
	 * 搁置任务邮件提醒
	 */
	private void delayedTaskEmailTip() {
		try {
			// 任务搁置提醒service
			IJecnTaskApproveDelayService taskApproveDelayService = (IJecnTaskApproveDelayService) ApplicationContextUtil
					.getContext().getBean("jecnTaskApproveDelayServiceImpl");
			// 任务搁置邮件提醒
			taskApproveDelayService.sendDelayEmail();
		} catch (Exception e) {
			log.error("发送搁置任务提醒邮件异常", e);
		}
	}

	/**
	 * 任务审批超时提醒
	 */
	private void taskApproveEmailTip() {
		// 任务审批超时邮件提醒
		try {
			IJecnTaskApproveTipService taskApproveTipService = (IJecnTaskApproveTipService) ApplicationContextUtil
					.getContext().getBean("jecnTaskApproveTipServiceImpl");
			taskApproveTipService.sendTipEmail();
		} catch (Exception e) {
			log.error("发送任务审批超时提醒邮件异常", e);
		}
	}

	/**
	 * 试运行邮件提醒
	 */
	private void testRunEmailTip() {
		// 发送试用行邮件
		try {
			ITaskRunTestService runTestService = (ITaskRunTestService) ApplicationContextUtil
					.getContext().getBean("runTestServiceImpl");
			runTestService.setEmailByDate();
		} catch (Exception e) {
			log.error("发送试运行邮件异常", e);
		}
	}

	/**
	 * 流程有效期提醒
	 */
	private void processValidEmailTip() {
		try {
			IWebProcessService webProcessService = (IWebProcessService) ApplicationContextUtil
					.getContext().getBean("WebProcessServiceImpl");
			// 流程有效期邮件提醒
			webProcessService.sendOrgEmail(0);
		} catch (Exception e) {
			log.error("发送流程有效期提醒邮件异常", e);
		}
	}

	/**
	 * 制度有效期提醒
	 */
	private void ruleValidEmailTip() {
		try {
			IWebProcessService webProcessService = (IWebProcessService) ApplicationContextUtil
					.getContext().getBean("WebProcessServiceImpl");
			webProcessService.sendOrgEmail(2);
		} catch (Exception e) {
			log.error("发送制度有效期提醒邮件异常", e);
		}
	}

	/**
	 * 清除被占用的系统资源
	 */
	private void removeDesignerResouce() {
		try {
			IPersonService personService = (IPersonService) ApplicationContextUtil
					.getContext().getBean("PersonServiceImpl");
			// 清除设计器中的被占用的组织、流程、人员
			personService.moveEditAll();
		} catch (Exception e) {
			log.error("清除设计器中被占用的组织、流程、人员异常", e);
		}
	}

	/**
	 * 
	 * 邮件发送给系统管理员通知系统到期
	 * 
	 */
	private void mailKeyTip() {
		try {
			// 邮件发送给系统管理员通知系统到期
			ISystemSendEmailToAdmin sendEmailToAdmin = (ISystemSendEmailToAdmin) ApplicationContextUtil
					.getContext().getBean("sendEmailToAdminImpl");
			// 服务监听，到期发送邮件提醒
			sendEmailToAdmin.systemServerSendEmail();
		} catch (Exception e) {
			log.error("发送系统到期提醒邮件异常！", e);
		}
	}

}
