package com.jecn.viewdoc.dao.impl;

import java.util.List;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.viewdoc.bean.ViewDocContentIdCache;
import com.jecn.viewdoc.dao.IViewDocDao;

public class ViewDocDaoImpl extends AbsBaseDao<ViewDocContentIdCache, Long> implements IViewDocDao {

	@Override
	public ViewDocContentIdCache getCache(Long relatedId, int dataType) {
		String sql = "SELECT * FROM  VIEW_DOC_CONTENT_ID_CACHE where RELATED_ID=? and TYPE=?";
		List<ViewDocContentIdCache> list = this.listNativeAddEntity(sql, relatedId, dataType);
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

}
