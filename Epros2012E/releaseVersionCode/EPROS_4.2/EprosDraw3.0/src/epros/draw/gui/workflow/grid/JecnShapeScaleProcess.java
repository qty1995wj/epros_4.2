package epros.draw.gui.workflow.grid;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.math.BigDecimal;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 刻度算法类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnShapeScaleProcess {
	/** 刻度面板宽 */
	private int width = 0;
	/** 刻度面板高 */
	private int height = 0;
	/** 当前单元格大小 */
	private double currCellSize;
	/** 放大倍数 */
	private double multiple = 1.0D;

	/** 行标题、列标题标志 */
	private RowColumnEnum rowColumnEnum;

	public JecnShapeScaleProcess(RowColumnEnum rowColumnEnum) {
		if (rowColumnEnum == null) {
			JecnLogConstants.LOG_SHAPE_GRID_PROCESS
					.error("JecnShapeScaleProcess类构造函数：参数为null.");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.rowColumnEnum = rowColumnEnum;
		// 获取默认单元格大小
		this.currCellSize = (JecnWorkflowConstant.CELL_SIZE / 16.0D);
	}

	/**
	 * 
	 * 画刻度
	 * 
	 * @param graphics
	 *            画笔
	 * @param backgroundColor
	 *            Color 背景颜色
	 * @param foregroundColor
	 *            Color前景颜色
	 */
	public void drawScale(Graphics graphics, Color backgroundColor,
			Color foregroundColor) {
		// 绘制画图面板上的列标题刻度
		Graphics2D graphics2D = (Graphics2D) graphics;

		RenderingHints oldRenderingHints = graphics2D.getRenderingHints();

		// 设置呈现和图像处理服务管理对象
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_OFF);

		int i = 0;
		int j = 0;
		// 矩形宽度
		int rectWidth = 0;
		// 矩形高度
		int rectHeight = 0;

		if (RowColumnEnum.COLUMN_FLAG == this.rowColumnEnum) {
			// 列标题
			// rectWidth = (int) (this.width * this.multiple);
			rectWidth = this.width;
			rectHeight = JecnWorkflowConstant.SCALE_PANEL_WIDTH;
		} else {
			// 行标题
			rectWidth = JecnWorkflowConstant.SCALE_PANEL_WIDTH;
			// rectHeight = (int) (this.height * this.multiple);
			rectHeight = this.height;
		}

		graphics2D.setColor(backgroundColor);
		graphics2D.fillRect(i, j, rectWidth, rectHeight);

		// 设置字体
		graphics2D.setFont(new Font("Arial", 0, 9));
		// 设置颜色
		graphics2D.setColor(foregroundColor);
		// 线条宽度
		graphics2D.setStroke(JecnUIUtil.getBasicStrokeOne());

		double d1 = 0.0D;
		double d2 = 0.0D;
		int n = 0;
		String str = null;
		int i1 = 0;
		if (this.multiple < 0.35D) {
			i1 = 4;
		} else if (this.multiple < 0.7D) {
			i1 = 2;
		} else {
			i1 = 1;
		}
		double d4 = this.currCellSize * this.multiple * i1;
		int i2 = 16;
		int i3 = i2 / 2;
		int i4 = i2 / 4;
		if (RowColumnEnum.COLUMN_FLAG == this.rowColumnEnum) {
			d1 = (int) (i / d4) * d4;
			d2 = ((int) ((i + rectWidth) / d4) + 1) * d4;
		} else {
			d1 = (int) (j / d4) * d4;
			d2 = ((int) ((j + rectHeight) / d4) + 1) * d4;
		}
		if (d1 == 0.0D) {
			str = "0 ";
			n = 10;
			if (RowColumnEnum.COLUMN_FLAG == this.rowColumnEnum) {
				graphics2D.drawLine(0, 17, 0, 18 - n - 1);
				graphics2D.drawString(str, 2, 10);
			} else {
				graphics2D.drawLine(17, 0, 18 - n - 1, 0);
			}
			str = null;
			d1 = d4;
		}
		int i5 = 0;
		double d5 = d1;
		while (d5 < d2) {
			i5++;
			int i6 = (int) d5;
			if (i5 % i2 == 0) {
				n = 10;
				str = this.sacete((double) (i5 / i2 * i1));
			} else if (i5 % i3 == 0) {
				n = 7;
				str = null;
			} else if ((i5 % i4 == 0)) {
				n = 5;
				str = null;
			} else {
				n = 3;
				str = null;
			}
			if (n != 0) {
				if (RowColumnEnum.COLUMN_FLAG == this.rowColumnEnum) {
					graphics2D.drawLine(i6, 17, i6, 18 - n - 1);
					if (str != null) {
						graphics2D.drawString(str, i6 + 2, 10);
					}
				} else {
					graphics2D.drawLine(17, i6, 18 - n - 1, i6);
					if (str != null) {
						AffineTransform localAffineTransform1 = graphics2D
								.getTransform();
						AffineTransform localAffineTransform2 = (AffineTransform) localAffineTransform1
								.clone();
						localAffineTransform2
								.rotate(-1.5707963D, 10.0D, i6 - 3);
						graphics2D.setTransform(localAffineTransform2);
						graphics2D.drawString(str, 10, i6 - 3);
						graphics2D.setTransform(localAffineTransform1);
					}
				}
			}
			d5 += d4;
		}
		graphics2D.setRenderingHints(oldRenderingHints);
	}

	private String sacete(double paramDouble) {

		String str = getString(paramDouble);
		if (str.endsWith(".00")) {
			str = str.substring(0, str.length() - 3);
		}
		return str;
	}

	private String getString(double paramDouble) {
		try {
			return getStringByBigDecimal(paramDouble, 15, 2).trim();
		} catch (Exception e) {
		}
		return String.valueOf(paramDouble);
	}

	private String getStringByBigDecimal(double paramDouble, int paramInt1,
			int paramInt2) throws Exception {
		// 不可变的、任意精度的有符号十进制数
		BigDecimal bigDecimal = new BigDecimal(paramDouble);
		// 返回一个 BigDecimal，其标度为指定值
		bigDecimal = bigDecimal.setScale(paramInt2, BigDecimal.ROUND_HALF_EVEN);
		String value = bigDecimal.toString();
		int i = paramInt1 + 1;
		String str2;
		if (i <= value.length()) {
			str2 = value.substring(0, i);
		} else {
			str2 = "";
			for (int j = 0; j < i - value.length(); j++) {
				str2 = str2 + " ";
			}
			str2 = str2 + value;
		}
		return str2;
	}

	/**
	 * 
	 * 刻度类型
	 * 
	 * @author Administrator
	 * 
	 */
	public enum RowColumnEnum {
		// 行标题
		ROW_FLAG,
		// 列标题
		COLUMN_FLAG
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public double getMultiple() {
		return multiple;
	}

	public void setMultiple(double multiple) {
		this.multiple = multiple;
	}
}
