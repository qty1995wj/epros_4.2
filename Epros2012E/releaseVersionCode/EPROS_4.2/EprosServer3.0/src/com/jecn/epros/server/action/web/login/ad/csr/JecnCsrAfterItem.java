package com.jecn.epros.server.action.web.login.ad.csr;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnPath;

/**
 * 南车株洲所配置文件读取类（EprosServer3.0/src/cfgFile/csr/CsrConfig.properties）
 * 
 */
public class JecnCsrAfterItem {

	private static Logger log = Logger.getLogger(JecnCsrAfterItem.class);

	// 单点验证URL地址
	public static String LOGIN_URL = null;
	public static String FireFox_URL = null;
	public static String FireFox_URL1 = null;

	/**
	 * 读取29所配置文件
	 * 
	 * @author weidp
	 * @date 2014-8-27 下午02:38:20
	 */
	public static void start() {

		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/csr/CsrConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			// 单点登录时代表登录名称的参数名
			String loginUrl = props.getProperty("LOGIN_URL");
			if (!JecnCommon.isNullOrEmtryTrim(loginUrl)) {
				JecnCsrAfterItem.LOGIN_URL = loginUrl;
			}
			FireFox_URL = props.getProperty("FireFox_URL");
			FireFox_URL1 = props.getProperty("FireFox_URL1");
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
			}
		}
	}
}
