package com.jecn.epros.server.action.web.login.ad.mulinsen;

/**
 * 木林森-OA请求
 * 
 * @author ZXH
 * @date 2018-01-25
 */
public class MulinsenOAParamInfo {
	private String hr_userid;
	private String action;
	private String content;
	private String redirect_url;

	private String sign;

	public String getHr_userid() {
		return hr_userid;
	}

	public void setHr_userid(String hrUserid) {
		hr_userid = hrUserid;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRedirect_url() {
		return redirect_url;
	}

	public void setRedirect_url(String redirectUrl) {
		redirect_url = redirectUrl;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
