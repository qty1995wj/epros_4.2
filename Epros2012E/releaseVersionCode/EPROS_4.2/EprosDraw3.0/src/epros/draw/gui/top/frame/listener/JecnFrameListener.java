package epros.draw.gui.top.frame.listener;

/**
 * 
 * 窗体监听类
 * 
 * @author ZHOUXY
 * 
 */
public interface JecnFrameListener {
	/** true：需要执行关闭 false：不要执行关闭 */
	public boolean FrameCloseBefore(JecnFrameEvent e);
}
