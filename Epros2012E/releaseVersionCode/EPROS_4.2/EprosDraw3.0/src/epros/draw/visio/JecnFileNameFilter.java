package epros.draw.visio;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import epros.draw.util.JecnResourceUtil;

/**
 * 接收传入的文件后缀名，过滤文件
 * 
 * @author Administrator
 * 
 */
public class JecnFileNameFilter extends FileFilter {
	private String[] endNameStr = null;

	public JecnFileNameFilter(String[] nameStr) {
		endNameStr = nameStr;
	}

	/**
	 * 
	 * 确认哪些显示在确认文件对话框中
	 * 
	 * 判断返回的是目录，还是文件（文件必须为传入的后缀名称）
	 * 
	 */
	public boolean accept(File f) {
		return f.isDirectory() || (f.isFile() && f.getName() != null && isEndName(f.getName()));
	}

	public boolean isEndName(String name) {
		for (String endName : endNameStr) {
			if (name.endsWith("." + endName)) {
				return true;
			}
		}
		return false;
	}

	public String getEndName() {
		StringBuffer description = new StringBuffer();
		description.append(JecnResourceUtil.getJecnResourceUtil().getValue("fileChooseTypeName") + "(");
		for (int i = 0; i < endNameStr.length; i++) {
			String endName = endNameStr[i];
			if (i + 1 != endNameStr.length) {
				description.append("*." + endName + ";");
			} else {
				description.append("*." + endName);
			}
		}
		description.append(")");
		return description.toString();
	}

	public String getDescription() {
		return getEndName();
	}
}
