package com.jecn.epros.server.connector.mengniu;

/**
 * 启动流程
 * 
 * @author xiaohu
 * 
 */
public class MengNiuStartWorkFlowInterface {
	/** 第三方流程实例编号 */
	private String EngineInstanceID;
	/** Ture存入OA的草稿箱中,Flase会发起流程 */
	private String IsDraft = "false";
	/** OA中流程唯一编码,集成双方约定好的编码 */
	private String ProcessCode = "EPROS";
	/** 流程发起人ID,唯一员工号 */
	private String SubmitUserID;

	private String ProxyUserID;

	private String FormData;
	/** 流程实例PC端查看地址 */
	private String InsPCViewUrl;

	/** 流程实例Mobile端查看地址 */
	private String InsMobileViewUrl;

	/** 流程名称 */
	private String InstanceTitle;

	public String getEngineInstanceID() {
		return EngineInstanceID;
	}

	public void setEngineInstanceID(String engineInstanceID) {
		EngineInstanceID = engineInstanceID;
	}

	public String getIsDraft() {
		return IsDraft;
	}

	public void setIsDraft(String isDraft) {
		IsDraft = isDraft;
	}

	public String getProcessCode() {
		return ProcessCode;
	}

	public void setProcessCode(String processCode) {
		ProcessCode = processCode;
	}

	public String getSubmitUserID() {
		return SubmitUserID;
	}

	public void setSubmitUserID(String submitUserID) {
		SubmitUserID = submitUserID;
	}

	public String getProxyUserID() {
		return ProxyUserID;
	}

	public void setProxyUserID(String proxyUserID) {
		ProxyUserID = proxyUserID;
	}

	public String getFormData() {
		return FormData;
	}

	public void setFormData(String formData) {
		FormData = formData;
	}

	public String getInsPCViewUrl() {
		return InsPCViewUrl;
	}

	public void setInsPCViewUrl(String insPCViewUrl) {
		InsPCViewUrl = insPCViewUrl;
	}

	public String getInsMobileViewUrl() {
		return InsMobileViewUrl;
	}

	public void setInsMobileViewUrl(String insMobileViewUrl) {
		InsMobileViewUrl = insMobileViewUrl;
	}

	public String toString() {
		return "EngineInstanceID = " + EngineInstanceID + "  InsPCViewUrl = " + InsPCViewUrl + "  InsMobileViewUrl = "
				+ InsMobileViewUrl;
	}

	public String getInstanceTitle() {
		return InstanceTitle;
	}

	public void setInstanceTitle(String instanceTitle) {
		InstanceTitle = instanceTitle;
	}

}
