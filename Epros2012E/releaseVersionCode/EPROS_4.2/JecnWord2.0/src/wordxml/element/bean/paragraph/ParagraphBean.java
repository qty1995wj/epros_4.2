package wordxml.element.bean.paragraph;

import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.common.BorderBean;

/**
 * 段落属性
 */

public class ParagraphBean {
	/** 对齐方式 默认左对齐 **/
	private Align align = Align.left;
	/**** 禁止在本段分页 默认false 可以在本段前分页 ********/
	private boolean keepLines = false;
	/**** 强制在本段前分页 默认false 不强制在本段前分页 *****/
	private boolean pageBreakBefore = false;
	/***** 自动段字 默认false 手动端字 *****************/
	private boolean suppressAutoHyphens = false;
	/*** 段落间距属性定义 **/
	private ParagraphSpaceBean paragraphSpaceBean = null;
	/*** 段落行距缩进属性定义 ****/
	private ParagraphRowIndBean paragraphRowIndBean = null;
	/***** 字体 ********/
	private FontBean fontBean = new FontBean();
	/****** 此属性用于定义项目符号使用 *******/
	private ProjectSignBean projectSignBean = null;
	/**** 段落边框 ****/
	private BorderBean borderBottomBean = null;
	/****** 段落风格 对应的几级标题 例如一级标题对应的是1 ********/
	private PStyle pStyle;

	public ParagraphBean() {

	}

	/*****
	 * 设置段落风格
	 * 
	 * @param String
	 *            pStyle 是模版中已经定义好的 一般不用此构造方法 设置了段落风格 其他样式失效
	 */
	public ParagraphBean(PStyle pStyle) {
		this.pStyle = pStyle;
	}

	public ParagraphBean(Align align) {
		this.setAlign(align);
	}

	public ParagraphBean(FontBean font) {
		this.fontBean = font;
	}

	/**
	 * 构造段落样式
	 * 
	 * @param fontFamily
	 *            字体
	 * @param fontSize
	 *            字号
	 * @param isBold
	 *            是否加粗
	 */
	public ParagraphBean(String fontFamily, int fontSize, boolean isBold) {
		fontBean.setFontFamily(fontFamily);
		fontBean.setFontSize(fontSize);
		fontBean.setB(isBold);
	}

	/**
	 * 构造段落样式
	 * 
	 * @param align
	 *            对齐方式
	 * @param fontFamily
	 *            字体
	 * @param fontSize
	 *            字号
	 * @param isBold
	 *            是否加粗
	 */
	public ParagraphBean(Align align, String fontFamily, int fontSize, boolean isBold) {
		this(fontFamily, fontSize, isBold);
		this.setAlign(align);
	}

	/**
	 * 构造段落样式
	 * 
	 * @param align
	 *            对齐方式
	 * @param fontSize
	 *            字号
	 * @param isBold
	 *            是否加粗
	 */
	public ParagraphBean(Align align, int fontSize, boolean isBold) {
		fontBean.setFontSize(fontSize);
		fontBean.setB(isBold);
		this.setAlign(align);
	}

	/**
	 * 构造段落样式
	 * 
	 * @param align
	 *            对齐方式
	 * @param fontFamily
	 *            字体
	 * @param fontSize
	 *            字号
	 * @param isBold
	 *            是否加粗
	 */
	public ParagraphBean(Align align, FontBean font) {
		this(font);
		this.setAlign(align);
	}

	/**
	 * 构造段落样式
	 * 
	 * @param fontSize
	 *            字号
	 */
	public ParagraphBean(int fontSize) {
		fontBean.setFontSize(fontSize);
	}

	/**
	 * 设置段落缩进 （hanging(悬挂缩进值)非0时，firstLine(首行缩进)无效） 单位：厘米
	 * 
	 * @author weidp
	 * @date 2014-10-21 下午05:58:38
	 * @param left
	 *            左缩进
	 * @param right
	 *            右缩进
	 * @param hanging
	 *            悬挂缩进
	 * @param firstLine
	 *            首行缩进
	 */
	public ParagraphBean setInd(float left, float right, float hanging, float firstLine) {
		if (paragraphRowIndBean == null) {
			paragraphRowIndBean = new ParagraphRowIndBean();
		}
		paragraphRowIndBean.setLeft(left);
		paragraphRowIndBean.setRight(right);
		paragraphRowIndBean.setHanging(hanging);
		paragraphRowIndBean.setFirstLine(firstLine);
		return this;
	}

	/**
	 * 设置间距
	 * 
	 * @author weidp
	 * @date 2014-10-21 下午06:07:46
	 * @param beforeLines
	 *            段前 （单位：行）
	 * @param afterLines
	 *            段后 （单位：行）
	 * @param ruleType
	 *            行距类型
	 */
	public ParagraphBean setSpace(Float beforeLines, Float afterLines, ParagraphLineRule lineRule) {
		if (paragraphSpaceBean == null) {
			paragraphSpaceBean = new ParagraphSpaceBean();
		}
		paragraphSpaceBean.setBeforeLines(beforeLines);
		paragraphSpaceBean.setAfterLines(afterLines);
		paragraphSpaceBean.setLineRule(lineRule);
		return this;
	}

	/**
	 * 设置间距
	 * 
	 * @param beforeLines
	 *            段前
	 * @param afterLines
	 *            段后
	 * @param type
	 *            段前段后单位类型 0行 1磅
	 * @param lineRule
	 *            行距类型
	 */
	public ParagraphBean setSpace(Float beforeLines, Float afterLines, int type, ParagraphLineRule lineRule) {
		if (paragraphSpaceBean == null) {
			paragraphSpaceBean = new ParagraphSpaceBean();
		}
		paragraphSpaceBean.setBeforeLines(beforeLines);
		paragraphSpaceBean.setAfterLines(afterLines);
		paragraphSpaceBean.setLineRule(lineRule);
		paragraphSpaceBean.setType(type);
		return this;
	}

	public Align getAlign() {
		return align;
	}

	public ParagraphBean setAlign(Align align) {
		this.align = align;
		return this;
	}

	public boolean isKeepLines() {
		return keepLines;
	}

	public ParagraphBean setKeepLines(boolean keepLines) {
		this.keepLines = keepLines;
		return this;
	}

	public boolean isPageBreakBefore() {
		return pageBreakBefore;
	}

	public ParagraphBean setPageBreakBefore(boolean pageBreakBefore) {
		this.pageBreakBefore = pageBreakBefore;
		return this;
	}

	public boolean isSuppressAutoHyphens() {
		return suppressAutoHyphens;
	}

	public ParagraphBean setSuppressAutoHyphens(boolean suppressAutoHyphens) {
		this.suppressAutoHyphens = suppressAutoHyphens;
		return this;
	}

	public FontBean getFontBean() {
		return fontBean;
	}

	public ParagraphBean setFontBean(FontBean fontBean) {
		this.fontBean = fontBean;
		return this;
	}

	public ParagraphRowIndBean getParagraphIndBean() {
		return paragraphRowIndBean;
	}

	public ParagraphBean setParagraphIndBean(ParagraphRowIndBean paragraphRowIndBean) {
		this.paragraphRowIndBean = paragraphRowIndBean;
		return this;
	}

	public ParagraphSpaceBean getParagraphSpaceBean() {
		return paragraphSpaceBean;
	}

	public ParagraphBean setParagraphSpaceBean(ParagraphSpaceBean paragraphSpaceBean) {
		this.paragraphSpaceBean = paragraphSpaceBean;
		return this;
	}

	public BorderBean getBorderBottomBean() {
		return borderBottomBean;
	}

	public ParagraphBean setBorderBottomBean(BorderBean borderBottomBean) {
		this.borderBottomBean = borderBottomBean;
		return this;
	}

	public ProjectSignBean getProjectSignBean() {
		return projectSignBean;
	}

	/**
	 * 设置特殊符号
	 * 
	 * @param projectSignBean
	 */
	public ParagraphBean setProjectSignBean(ProjectSignBean projectSignBean) {
		this.projectSignBean = projectSignBean;
		return this;
	}

	public ParagraphRowIndBean getParagraphRowIndBean() {
		return paragraphRowIndBean;
	}

	public ParagraphBean setParagraphRowIndBean(ParagraphRowIndBean paragraphRowIndBean) {
		this.paragraphRowIndBean = paragraphRowIndBean;
		return this;
	}

	public PStyle getpStyle() {
		return pStyle;
	}

	public ParagraphBean setpStyle(PStyle pStyle) {
		this.pStyle = pStyle;
		return this;
	}
}
