package epros.designer.gui.autoNum;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.autoCode.ProcessCodeTree;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.gui.file.AddFileDirDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 添加过程代码
 * 
 * @author ZXH
 * 
 */
public class AddProcessCodeDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(AddFileDirDialog.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;
	/** 0 流程，1 文件 ， 2 制度 */
	private int treeType;

	public AddProcessCodeDialog(JecnTreeNode pNode, JTree jTree, int treeType) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.treeType = treeType;
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addProcessCodeC");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		try {
			if (pNode == null) {
				return;
			}
			ProcessCodeTree codeTree = new ProcessCodeTree();
			JecnTreeBean treeBean = pNode.getJecnTreeBean();
			codeTree.setParentId(treeBean.getId());
			codeTree.setName(getName());
			codeTree.settPath(treeBean.getT_Path());
			codeTree.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
			codeTree.setSortId(JecnTreeCommon.getMaxSort(pNode));
			codeTree.setIsDir(0);
			codeTree.setType(treeType);
			Long id = ConnectionPool.getAutoCodeAction().addPorcessCode(codeTree);

			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.processCode);
			jecnTreeBean.setT_Path(codeTree.gettPath());
			jecnTreeBean.setIsDelete(0);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, "saveData is error！");
			log.error("saveData is error！", e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name, TreeNodeType.processCode);
	}
	protected void validateEnglishName() {// 验证英文名称
	}

	protected void addEnNamePanel(Insets insets) {
	}

}
