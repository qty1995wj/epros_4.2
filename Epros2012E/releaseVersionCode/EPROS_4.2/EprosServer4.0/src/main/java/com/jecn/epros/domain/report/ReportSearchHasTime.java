package com.jecn.epros.domain.report;

import io.swagger.annotations.ApiModelProperty;

public class ReportSearchHasTime extends ReportSearchParam {

    @ApiModelProperty("流程清单报表不需要传这个参数开始时间格式为yyyy-MM-dd")
    private String startTime;
    @ApiModelProperty("结束时间格式为yyyy-MM-dd和开始时间的间隔最大为5年")
    private String endTime;
    /**
     * 报表类型0流程 2制度
     */
    private int type = 0;
    /**
     * 责任部门是否包含子节点
     */
    private boolean orgHasChild = true;

    public boolean isOrgHasChild() {
        return orgHasChild;
    }

    public void setOrgHasChild(boolean orgHasChild) {
        this.orgHasChild = orgHasChild;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
