package epros.designer.gui.rule;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfoT;
import com.jecn.epros.server.bean.rule.JecnRuleData;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.AccessAuthorityCommon;
import epros.designer.gui.system.CategoryCommon;
import epros.designer.gui.system.CommonJTextField;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.gui.system.DepartMutilSelectCommon;
import epros.designer.gui.system.ExpiryCommon;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.gui.system.PersonliableSelectCommon;
import epros.designer.gui.system.ResDepartSelectCommon;
import epros.designer.gui.system.area.TextAreaFour;
import epros.designer.gui.system.area.TextAreaUnlimited;
import epros.designer.gui.system.field.JecnTipTextField;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.JecnTextFieldAndCalendarPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnResourceUtil;

/***
 * 制度基本信息
 * 
 * @author 2012-06-07
 * 
 */
public class RuleBaseInfoPanel extends JecnPanel {

	private static Logger log = Logger.getLogger(RuleBaseInfoPanel.class);
	private String colon = JecnResourceUtil.getLocale() == Locale.ENGLISH ? ":" : "：";
	/** 制度编号Lab */
	protected JLabel ruleNumLab = new JLabel(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.ruleCode) + colon);
	/** 制度编号Field */
	protected JTextField ruleNumField = new JTextField();

	/** 制度名称Lab */
	protected JLabel ruleNameLab = new JLabel(JecnProperties.getValue("ruleNameC"));
	/** 制度名称Field */
	protected JTextField ruleNameField = new JTextField();

	protected CommonJTextField ruleLinkText;
	/** 类别 */
	protected CategoryCommon categoryCommon = null;
	/** 流程责任人 */
	protected PersonliableSelectCommon personliableSelect = null;
	/** 制度拟制人 */
	protected PeopleSelectCommon fictionSelect = null;
	/** 流程责任部门 */
	protected ResDepartSelectCommon resDepartSelectCommon = null;
	/** 查阅权限 */
	protected AccessAuthorityCommon accessAuthorityCommon = null;
	/** 监护人 */
	protected PeopleSelectCommon guardianPeopleSelectCommon = null;

	/** 有效期 */
	protected ExpiryCommon expiryCommon = null;

	/** 业务范围 */
	protected TextAreaUnlimited businessScope;

	/** 组织范围 */
	protected TextAreaUnlimited otherScope;

	/**
	 * 目的
	 * */
	protected TextAreaUnlimited rulePurpose;
	/**
	 * 适用范围
	 */
	protected TextAreaUnlimited ruleApplicability;
	/**
	 * 文件编写部门
	 */
	protected TextAreaFour ruleRriteDept;
	/**
	 * 起草人
	 */
	protected TextAreaFour ruleDraftMan;
	/**
	 * 起草单位
	 */
	protected TextAreaFour ruleDraftingDept;
	/**
	 * 试行版文件
	 */
	protected TextAreaFour ruleTestRunFile;

	/** 组织范围 */

	protected DepartMutilSelectCommon orgScope;

	/** 换版说明 */

	protected TextAreaUnlimited changeVersionExplain;

	/** 生效日期Lab */
	protected JLabel effectiveTimeLab = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowEffectiveDate)
			+ colon);

	/** 生效日期 */
	protected JecnTextFieldAndCalendarPanel effectiveTime = null;

	/** 文件But */
	protected JButton fileBut = new JButton(JecnProperties.getValue("selectBtn"));
	/** 文件，只放一个对象 */
	protected List<JecnTreeBean> fileList = new ArrayList<JecnTreeBean>();

	public JecnDialog jDialog = null;

	protected RuleT ruleT;

	/** 专员 */
	PeopleSelectCommon commissionerPeopleSelectCommon = null;
	/** 关键字 */
	JecnTipTextField keyWordField = null;

	private List<JecnConfigItemBean> itemBeanList = null;

	private boolean isAdd = false;
	private Long pRuleId;

	public RuleT getRuleT() {
		return ruleT;
	}

	public void setRuleT(RuleT ruleT) {
		this.ruleT = ruleT;
	}

	public RuleBaseInfoPanel(Long ruleId, JecnDialog jDialog) {
		this.jDialog = jDialog;
		// 根据制度Id获取RuleT中的数据
		try {
			ruleT = ConnectionPool.getRuleAction().getRuleT(ruleId);
			// 可以判断是否被删除 或者将来的假删
			if (ruleT == null) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("ruleIsDel"));
				return;
			}
			if (ruleT != null) {
				// 制度编号
				this.ruleNumField.setText(ruleT.getRuleNumber());
				// 制度名称
				this.ruleNameField.setText(ruleT.getRuleName());
			}

		} catch (Exception e) {
			log.error("RuleBaseInfoPanel RuleBaseInfoPanel is error", e);
			return;
		}
		commonInit();
	}

	public RuleBaseInfoPanel(JecnDialog jDialog, Long pRuleId) {
		this.jDialog = jDialog;
		ruleT = new RuleT();
		ruleT.setExpiry(getRuleExpiry());
		this.pRuleId = pRuleId;
		isAdd = true;
		commonInit();
	}

	private Long getRuleExpiry() {
		// 获取流程配置-->流程属性中 有效期配置数据
		JecnConfigItemBean configItemBean = ConnectionPool.getConfigAciton().selectConfigItemBean(
				JecnConfigContents.TYPE_BIG_ITEM_ROLE, ConfigItemPartMapMark.ruleValidDefaultValue.toString());
		// 默认为永久
		if (configItemBean.getValue() != null && !"".equals(configItemBean.getValue())) {
			return Long.valueOf(configItemBean.getValue());
		}
		return 0L;
	}

	private void commonInit() {
		// 根据制度Id获取RuleT中的数据
		itemBeanList = JecnConfigTool.getRuleBasicConfigItems();
		initLayout();

		if (isAdd && JecnConfigTool.isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode)) {
			ruleNumField.setEditable(false);
		}
	}

	/***
	 * 布局
	 */
	protected void initLayout() {
		GridBagConstraints c = null;
		Insets insets = new Insets(4, 5, 3, 5);
		// 信息面板=========================布局
		this.setLayout(new GridBagLayout());
		int rows = 0;
		// 制度名称
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(ruleNameLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(ruleNameField, c);
		c = new GridBagConstraints(3, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(JecnUtil.MUST, c);
		// 判断是否为 制度关联链接文件 添加链接打开按钮
		if (ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 2) {
			openRuleUrl(rows, insets, c);
			rows++;
		}
		if (!isAdd) {
			ruleNameField.setEditable(false);
		}
		rows++;

		// 制度编号
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleCode)) {// 流程编号是否必填
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			this.add(ruleNumLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, insets, 0, 0);
			this.add(ruleNumField, c);
			if (!isAdd && ruleT.getIsDir() == 2 && ruleT.getIsFileLocal() != null
					&& ruleT.getIsFileLocal().intValue() == 0) {// 文件库关联的不允许修改编号
				ruleNumField.setEditable(false);
			} else {
				if (JecnConfigTool.isAutoNumber() && !JecnConfigTool.isMengNiuOperaType()) {
					ruleNumField.setEditable(false);
				}
			}
			if (isRequest(ConfigItemPartMapMark.ruleCode)) {// 流程编号是否必填
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, insets, 0, 0);
				this.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}

		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleType)) {// 制度类别
			this.categoryCommon = new CategoryCommon(rows, this, insets, ruleT.getTypeId(), jDialog,
					isRequest(ConfigItemPartMapMark.ruleType), JecnConfigTool
							.getConfigItemName(ConfigItemPartMapMark.ruleType)
							+ colon);
			rows++;
			this.categoryCommon.setVisible();
		}
		// 生效日期
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowEffectiveDate)) {
			effectiveTime = new JecnTextFieldAndCalendarPanel();
			if (ruleT.getEffectiveTime() != null) {
				String effectiveDate = JecnTool.getStringbyDate(ruleT.getEffectiveTime());
				effectiveTime.setText(effectiveDate);
				effectiveTime.setOldText(effectiveDate);
			}
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			this.add(effectiveTimeLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, insets, 0, 0);
			this.add(effectiveTime, c);
			if (isRequest(ConfigItemPartMapMark.isShowEffectiveDate)) {// 流程编号是否必填
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, insets, 0, 0);
				this.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}

		// 责任人
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleResPeople)) {
			this.personliableSelect = new PersonliableSelectCommon(rows, this, insets, ruleT.getAttchMentId(), ruleT
					.getTypeResPeople() == null ? 0 : ruleT.getTypeResPeople().intValue(), ruleT.getAttchMentName(),
					jDialog, 1, isRequest(ConfigItemPartMapMark.ruleResPeople), JecnConfigTool
							.getConfigItemName(ConfigItemPartMapMark.ruleResPeople)
							+ colon);
			rows++;
		}
		// 制度监护人
		if (JecnConfigTool.isShowRuleGuardianPeople()) {
			guardianPeopleSelectCommon = new PeopleSelectCommon(rows, this, insets, ruleT.getGuardianId(), ruleT
					.getGuardianName(), jDialog, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleGuardianPeople)
					+ colon, isRequest(ConfigItemPartMapMark.ruleGuardianPeople));
			rows++;
		}
		// 拟制人
		if (JecnConfigTool.isShowRuleFictionPeople()) {
			fictionSelect = new PeopleSelectCommon(rows, this, insets, ruleT.getFictionPeopleId(), ruleT
					.getFictionPeopleName(), jDialog, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleFictionPeople)
					+ colon, isRequest(ConfigItemPartMapMark.ruleFictionPeople));
			rows++;
		}
		// 责任部门
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleResDept)) {
			resDepartSelectCommon = new ResDepartSelectCommon(rows, this, insets, ruleT.getOrgId(), ruleT.getOrgName(),
					jDialog, isRequest(ConfigItemPartMapMark.ruleResDept), JecnConfigTool
							.getConfigItemName(ConfigItemPartMapMark.ruleResDept)
							+ colon);
		}
		rows++;
		// 专员
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowRuleCommissioner)) {
			this.commissionerPeopleSelectCommon = new PeopleSelectCommon(rows, this, insets, ruleT.getCommissionerId(),
					ruleT.getCommissionerName(), jDialog, JecnConfigTool
							.getConfigItemName(ConfigItemPartMapMark.isShowRuleCommissioner)
							+ colon, isRequest(ConfigItemPartMapMark.isShowRuleCommissioner));
		}
		rows++;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowRuleKeyWord)) {
			this.keyWordField = new JecnTipTextField(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.isShowRuleKeyWord)
					+ colon, ruleT.getKeyword(), JecnProperties.getValue("keyWord"));

		}
		if (isRequest(ConfigItemPartMapMark.isShowRuleKeyWord)) {
			c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			this.add(new CommonJlabelMustWrite(), c);
		}
		rows++;
		// 有效期
		this.expiryCommon = new ExpiryCommon(ruleT.getExpiry() == null ? "0" : ruleT.getExpiry().toString(), rows,
				this, insets, 1);
		rows++;

		// 业务范围
		if (JecnConfigTool.isShowRuleBusinessScope()) {
			this.businessScope = new TextAreaUnlimited(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleBusinessScope)
					+ colon, ruleT.getBusinessScope(), JecnProperties.getValue("businessRange"),
					isRequest(ConfigItemPartMapMark.ruleBusinessScope));
			rows++;
		}
		// 其他范围
		if (JecnConfigTool.isShowRuleOtherScope()) {
			this.otherScope = new TextAreaUnlimited(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleOtherScope)
					+ colon, ruleT.getOtherScope(), JecnProperties.getValue("otherRange"),
					isRequest(ConfigItemPartMapMark.ruleOtherScope));
			rows++;
		}
		// if (ruleT.getIsDir() != null && ruleT.getIsDir().intValue() == 1) {
		rows = addMengNiuAttrs(rows, insets);
		// }
		if (ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 2) {
			this.accessAuthorityCommon = new AccessAuthorityCommon(rows, this, insets, isAdd ? pRuleId : ruleT.getId(),
					3, jDialog, isAdd, false);
		} else {
			this.accessAuthorityCommon = new AccessAuthorityCommon(rows, this, insets, isAdd ? pRuleId : ruleT.getId(),
					3, jDialog, isAdd, true);
		}

	}

	private void openRuleUrl(int rows, Insets insets, GridBagConstraints c) {
		rows++;
		ruleLinkText = new CommonJTextField(rows, this, insets, JecnProperties.getValue("ruleLinkC"));
		ruleLinkText.getTextField().addFocusListener(
				new JTextFieldHintListener(JecnProperties.getValue("systemLinkSpecification"), ruleLinkText
						.getTextField()));
		ruleLinkText.getTextField().setText(ruleT.getRuleUrl());
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 3, 5, 5), 0, 0);
		JButton button = new JButton(JecnProperties.getValue("openBtn"));
		this.add(button, c);
		c = new GridBagConstraints(5, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 3, 5, 5), 0, 0);
		this.add(new CommonJlabelMustWrite(), c);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Pattern pattern = Pattern
						.compile("^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+$");
				if (!pattern.matcher(ruleLinkText.getTextValue()).matches()) {
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("linkFormatIsIncorrect"));
					return;
				}
				JecnUtil.runBroswer(ruleLinkText.getTextValue());
			}
		});
	}

	private int addMengNiuAttrs(int rows, Insets insets) {
		// 获取前言信息对象
		JecnRuleBaiscInfoT jecnRuleBaiscInfoT = ruleT.getJecnRuleBaiscInfoT();
		if (jecnRuleBaiscInfoT == null) {
			jecnRuleBaiscInfoT = new JecnRuleBaiscInfoT();
			ruleT.setJecnRuleBaiscInfoT(jecnRuleBaiscInfoT);
		}
		// 制度目的
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.rulePurpose)) {
			this.rulePurpose = new TextAreaUnlimited(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.rulePurpose)
					+ colon, jecnRuleBaiscInfoT.getPurpose(), JecnProperties.getValue("rulePurpose"),
					isRequest(ConfigItemPartMapMark.rulePurpose));
			rows++;
		}
		// 适用范围
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleApplicability)) {
			this.ruleApplicability = new TextAreaUnlimited(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleApplicability)
					+ colon, jecnRuleBaiscInfoT.getApplicability(), JecnProperties.getValue("ruleApplicability"),
					isRequest(ConfigItemPartMapMark.ruleApplicability));
			rows++;
		}
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleChangeVersionExplain)) {
			this.changeVersionExplain = new TextAreaUnlimited(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleChangeVersionExplain)
					+ colon, jecnRuleBaiscInfoT.getChangeVersionExplain(), JecnProperties
					.getValue("ruleChangeVersionExplain"), isRequest(ConfigItemPartMapMark.ruleChangeVersionExplain));
			rows++;
		}
		// 文件编写部门
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleRriteDept)) {
			this.ruleRriteDept = new TextAreaFour(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleRriteDept)
					+ colon, jecnRuleBaiscInfoT.getWriteDept(), JecnProperties.getValue("ruleRriteDept"),
					isRequest(ConfigItemPartMapMark.ruleRriteDept));
			rows++;
		}
		// 起草人
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleDraftMan)) {
			this.ruleDraftMan = new TextAreaFour(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleDraftMan)
					+ colon, jecnRuleBaiscInfoT.getDraftman(), JecnProperties.getValue("ruleDraftMan"),
					isRequest(ConfigItemPartMapMark.ruleDraftMan));
			rows++;
		}
		// 起草单位
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleDraftingDept)) {
			this.ruleDraftingDept = new TextAreaFour(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleDraftingDept)
					+ colon, jecnRuleBaiscInfoT.getDraftingUnit(), JecnProperties.getValue("ruleDraftingDept"),
					isRequest(ConfigItemPartMapMark.ruleDraftingDept));
			rows++;
		}
		// 试行版文件
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleTestRunFile)) {
			this.ruleTestRunFile = new TextAreaFour(rows, this, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleTestRunFile)
					+ colon, jecnRuleBaiscInfoT.getTestRunFile(), JecnProperties.getValue("ruleTestRunFile"),
					isRequest(ConfigItemPartMapMark.ruleTestRunFile));
			rows++;
		}
		// 部门范围
		if (JecnConfigTool.isShowRuleOrgScope()) {
			this.orgScope = new DepartMutilSelectCommon(rows, this, insets, ruleT.getOrgScopes(), jDialog,
					isRequest(ConfigItemPartMapMark.ruleOrgScope), JecnConfigTool
							.getConfigItemName(ConfigItemPartMapMark.ruleOrgScope)
							+ colon);
			rows++;
		}
		return rows;
	}

	/**
	 * 验证 大字符 组件 输入长度 true ：验证输入内容不能大于5000个汉字或10000字符 false : 通过验证
	 * 
	 * @return
	 */
	public boolean validationLength() {
		boolean isPass = false;

		// 业务范围
		if (businessScope != null) {
			isPass = JecnValidateCommon.validateAreaNotMaxPass(businessScope.getResultStr(), JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleBusinessScope));
			if (isPass) {
				return true;
			}
		}
		// 其他范围
		if (otherScope != null) {
			isPass = JecnValidateCommon.validateAreaNotMaxPass(otherScope.getResultStr(), JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleOtherScope));
			if (isPass) {
				return true;
			}
		}
		// 目的
		if (rulePurpose != null) {
			isPass = JecnValidateCommon.validateAreaNotMaxPass(rulePurpose.getResultStr(), JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.rulePurpose));
			if (isPass) {
				return true;
			}
		}
		// 适用范围
		if (ruleApplicability != null) {
			isPass = JecnValidateCommon.validateAreaNotMaxPass(ruleApplicability.getResultStr(), JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleApplicability));
			if (isPass) {
				return true;
			}
		}
		// 文件编写部门
		if (ruleRriteDept != null) {
			isPass = JecnValidateCommon.validateAreaNotPass(ruleRriteDept.getResultStr(), JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleRriteDept));
			if (isPass) {
				return true;
			}
		}
		// 起草人
		if (ruleDraftMan != null) {
			isPass = JecnValidateCommon.validateAreaNotPass(ruleDraftMan.getResultStr(), JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleDraftMan));
			if (isPass) {
				return true;
			}
		}
		// 起草单位
		if (ruleDraftingDept != null) {
			isPass = JecnValidateCommon.validateAreaNotPass(ruleDraftingDept.getResultStr(), JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleDraftingDept));
			if (isPass) {
				return true;
			}
		}
		// 试行版文件
		if (ruleTestRunFile != null) {
			isPass = JecnValidateCommon.validateAreaNotPass(ruleTestRunFile.getResultStr(), JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleTestRunFile));
			if (isPass) {
				return true;
			}
		}
		// 换版说明
		if (changeVersionExplain != null) {
			isPass = JecnValidateCommon.validateAreaNotMaxPass(changeVersionExplain.getResultStr(), JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.ruleChangeVersionExplain));
			if (isPass) {
				return true;
			}
		}

		return isPass;
	}

	public JecnRuleData getJecnRuleData() {
		ruleT.setRuleName(this.ruleNameField.getText().trim());
		// 保存制度基本信息
		ruleT.setRuleNumber(ruleNumField.getText());

		if (categoryCommon != null) {
			// 制度类别
			ruleT.setTypeId(categoryCommon.getTypeResultId());
		}
		if (personliableSelect != null) {
			// 制度责任人
			ruleT.setAttchMentId(personliableSelect.getSingleSelectCommon().getIdResult());
		}
		if (resDepartSelectCommon != null) {
			// 责任部门
			ruleT.setOrgId(resDepartSelectCommon.getIdResult());
		}
		// 有效期
		if (expiryCommon != null) {
			ruleT.setExpiry(Long.valueOf(expiryCommon.getValueResult()));
		}
		// 监护人
		if (guardianPeopleSelectCommon != null) {
			ruleT.setGuardianId(guardianPeopleSelectCommon.getIdResult());
		}
		if (fictionSelect != null) {
			ruleT.setFictionPeopleId(fictionSelect.getIdResult());
		}
		// 业务范围
		if (businessScope != null) {
			ruleT.setBusinessScope(businessScope.getResultStr());
		}
		// 其他范围
		if (otherScope != null) {
			ruleT.setOtherScope(otherScope.getResultStr());
		}
		// 组织范围
		if (orgScope != null) {
			ruleT.setOrgScopes(orgScope.getList());
		}
		// 目的
		if (rulePurpose != null) {
			ruleT.getJecnRuleBaiscInfoT().setPurpose(rulePurpose.getResultStr());
		}
		// 适用范围
		if (ruleApplicability != null) {
			ruleT.getJecnRuleBaiscInfoT().setApplicability(ruleApplicability.getResultStr());
		}
		// 文件编写部门
		if (ruleRriteDept != null) {
			ruleT.getJecnRuleBaiscInfoT().setWriteDept(ruleRriteDept.getResultStr());
		}
		// 起草人
		if (ruleDraftMan != null) {
			ruleT.getJecnRuleBaiscInfoT().setDraftman(ruleDraftMan.getResultStr());
		}
		// 起草单位
		if (ruleDraftingDept != null) {
			ruleT.getJecnRuleBaiscInfoT().setDraftingUnit(ruleDraftingDept.getResultStr());
		}
		// 试行版文件
		if (ruleTestRunFile != null) {
			ruleT.getJecnRuleBaiscInfoT().setTestRunFile(ruleTestRunFile.getResultStr());
		}
		// 换版说明
		if (changeVersionExplain != null) {
			ruleT.getJecnRuleBaiscInfoT().setChangeVersionExplain(changeVersionExplain.getResultStr());
		}

		if (commissionerPeopleSelectCommon != null) {
			ruleT.setCommissionerId(commissionerPeopleSelectCommon.getIdResult());
		}
		if (keyWordField != null) {
			ruleT.setKeyword(keyWordField.getResultStr());
		}
		// 制度责任人类型
		if (personliableSelect != null) {
			ruleT.setTypeResPeople(personliableSelect.getPeopleResType());
		}
		// 生效日期
		if (effectiveTime != null && StringUtils.isNotBlank(effectiveTime.getText())) {
			Date effectiveDate = JecnTool.getDateByString(effectiveTime.getText());
			ruleT.setEffectiveTime(effectiveDate);
		}
		// 密级
		ruleT.setIsPublic(Long.valueOf(accessAuthorityCommon.getPublicStatic()));
		// 保密级别
		ruleT.setConfidentialityLevel(accessAuthorityCommon.getConfidentialityLevelStatic());
		// 权限
		AccessId accId = new AccessId();
		// 部门权限
		accId.setOrgAccessId(accessAuthorityCommon.getOrgAccessIds());
		// 岗位权限
		accId.setPosAccessId(accessAuthorityCommon.getPosAccessIds());
		// 岗位组
		accId.setPosGroupAccessId(accessAuthorityCommon.getPosGroupAcessIds());

		ruleT.setUpdatePeopleId(JecnConstants.getUserId());
		// 更新制度信息
		JecnRuleData ruleData = new JecnRuleData();
		ruleData.setAccIds(accId);
		ruleData.setRuelBean(ruleT);
		ruleData.setUpdatePersonId(JecnConstants.getUserId());
		return ruleData;
	}

	/**
	 * 是否必填
	 * 
	 * @param mark
	 * @return
	 */
	protected boolean isRequest(ConfigItemPartMapMark mark) {
		for (JecnConfigItemBean itemBean : itemBeanList) {
			if (itemBean.getMark().equals(mark.toString()) && itemBean.isNotEmpty() && "1".equals(itemBean.getValue())) {
				return true;
			}
		}
		return false;
	}

	public boolean isUpdate() {
		String oldRuleNumber = "";
		if (ruleT == null) {
			return false;
		}
		if (ruleT.getRuleNumber() != null) {
			oldRuleNumber = ruleT.getRuleNumber();
		}
		if (!this.ruleNumField.getText().trim().equals(oldRuleNumber)) {
			return true;
		}
		if (categoryCommon != null && this.categoryCommon.isUpdate()) {
			return true;
		}
		if (personliableSelect != null && this.personliableSelect.getSingleSelectCommon().isUpdate()) {
			return true;
		}
		if (guardianPeopleSelectCommon != null && this.guardianPeopleSelectCommon != null) {
			if (guardianPeopleSelectCommon.isUpdate()) {
				return true;
			}
		}
		if (fictionSelect != null && this.fictionSelect != null) {
			if (fictionSelect.isUpdate()) {
				return true;
			}
		}
		if (resDepartSelectCommon != null && this.resDepartSelectCommon.isUpdate()) {
			return true;
		}
		if (accessAuthorityCommon != null && this.accessAuthorityCommon.isUpdate()) {
			return true;
		}
		if (this.expiryCommon != null) {
			if (expiryCommon.isUpdate()) {
				return true;
			}
		}
		if (this.businessScope != null) {
			if (businessScope.isUpdate()) {
				return true;
			}
		}
		if (this.otherScope != null) {
			if (otherScope.isUpdate()) {
				return true;
			}
		}
		if (rulePurpose != null) {
			if (rulePurpose.isUpdate()) {
				return true;
			}
		}
		if (this.orgScope != null) {
			if (orgScope.isUpdate()) {
				return true;
			}
		}
		if (this.changeVersionExplain != null) {
			if (changeVersionExplain.isUpdate()) {
				return true;
			}
		}
		if (this.ruleApplicability != null) {
			if (ruleApplicability.isUpdate()) {
				return true;
			}
		}
		if (this.ruleRriteDept != null) {
			if (ruleRriteDept.isUpdate()) {
				return true;
			}
		}
		if (this.ruleDraftMan != null) {
			if (ruleDraftMan.isUpdate()) {
				return true;
			}
		}
		if (this.ruleDraftingDept != null) {
			if (ruleDraftingDept.isUpdate()) {
				return true;
			}
		}
		if (this.ruleTestRunFile != null) {
			if (ruleTestRunFile.isUpdate()) {
				return true;
			}
		}// 专员
		if (commissionerPeopleSelectCommon != null) {
			if (this.commissionerPeopleSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 关键字
		if (this.keyWordField != null) {
			if (keyWordField.isUpdate()) {
				return true;
			}
		}
		// 生效日期
		if (this.effectiveTime != null) {
			if (effectiveTime.isUpdate()) {
				return true;
			}
		}
		return false;
	}

	public JTextField getRuleNameField() {
		return ruleNameField;
	}

	public JLabel getRuleNumLab() {
		return ruleNumLab;
	}

	public void setRuleNumLab(JLabel ruleNumLab) {
		this.ruleNumLab = ruleNumLab;
	}

	public JTextField getRuleNumField() {
		return ruleNumField;
	}

	public void setRuleNumField(JTextField ruleNumField) {
		this.ruleNumField = ruleNumField;
	}

	public JLabel getRuleNameLab() {
		return ruleNameLab;
	}

	public void setRuleNameLab(JLabel ruleNameLab) {
		this.ruleNameLab = ruleNameLab;
	}

	public JButton getFileBut() {
		return fileBut;
	}

	public void setFileBut(JButton fileBut) {
		this.fileBut = fileBut;
	}

	public List<JecnTreeBean> getFileList() {
		return fileList;
	}

	public void setFileList(List<JecnTreeBean> fileList) {
		this.fileList = fileList;
	}

	public void setRuleNameField(JTextField ruleNameField) {
		this.ruleNameField = ruleNameField;
	}

	class JTextFieldHintListener implements FocusListener {
		private String mHintText;
		private JTextField mTextField;

		public JTextFieldHintListener(String hintText, JTextField textField) {
			this.mHintText = hintText;
			this.mTextField = textField;
			textField.setForeground(Color.GRAY);
		}

		@Override
		public void focusGained(FocusEvent e) {
			String temp = mTextField.getText();
			if (temp.equals(mHintText)) {
				mTextField.setText("");
				mTextField.setForeground(Color.BLACK);
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			String temp = mTextField.getText();
			if (temp.equals("")) {
				mTextField.setForeground(Color.GRAY);
				mTextField.setText(mHintText);
			}
		}
	}
}
