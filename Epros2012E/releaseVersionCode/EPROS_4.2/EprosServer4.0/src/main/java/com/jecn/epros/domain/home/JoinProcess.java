package com.jecn.epros.domain.home;

import java.util.List;

/**
 * 参与的流程
 *
 * @author ZXH
 */
public class JoinProcess {
    /**
     * 岗位Id
     */
    private Long posId;
    /**
     * 岗位名称
     */
    private String posName;
    /**
     * 角色
     */
    private List<RoleRelatedFlowInfo> roleList;

    /**
     * 角色
     */
    private List<RoleRelatedFlowInfo> data;
    /**
     * 条数
     *
     * @return
     */
    private int total;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Long getPosId() {
        return posId;
    }

    public void setPosId(Long posId) {
        this.posId = posId;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public List<RoleRelatedFlowInfo> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<RoleRelatedFlowInfo> roleList) {
        this.roleList = roleList;
    }

    public List<RoleRelatedFlowInfo> getData() {
        return data;
    }

    public void setData(List<RoleRelatedFlowInfo> data) {
        this.data = data;
    }
}
