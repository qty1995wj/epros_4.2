package com.jecn.epros.server.action.web.login.ad.lianhedianzi;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class ADReader {
	private final Logger log = Logger.getLogger(ADReader.class);
	public static final String[] UNREAD_ORG;
	public static final String[] UNREAD_USER;
	/** 数据列与对应的set方法 关系 */
	private static HashMap<String, String> COLUMN_METHOD_REFERENCES;

	static {
		UNREAD_ORG = LianHeDianZiAfterItem.UNREADORG.split(",");
		UNREAD_USER = LianHeDianZiAfterItem.UNREADUSER.split(",");
		COLUMN_METHOD_REFERENCES = new HashMap<String, String>();
		String[] mappings = LianHeDianZiAfterItem.DATAMAPPINGS.split(",");
		for (String val : mappings) {
			String[] temp = val.split(":");
			COLUMN_METHOD_REFERENCES.put(temp[0], temp[1]);
		}
	}

	List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
	List<UserBean> userPosBeanList = new ArrayList<UserBean>();
	// ldap权限验证类
	LdapAuthVerifier ldapPrivilegeVerifier = LdapAuthVerifier.newInstance(LianHeDianZiAfterItem.DOMAIN
			+ LianHeDianZiAfterItem.USERNAME, LianHeDianZiAfterItem.PASSWORD, LianHeDianZiAfterItem.LDAP_URL);

	/**
	 * 获取人员同步数据
	 * 
	 * @param base
	 * @param filter
	 * @throws Exception
	 */
	public void searchInformation(String base, String filter) throws Exception {
		LdapContext ctx = this.ldapPrivilegeVerifier.verifyAndGetContext();
		SearchControls sc = new SearchControls();
		sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		sc.setReturningAttributes(LianHeDianZiAfterItem.DATACOLUMNS.split(","));
		NamingEnumeration ne = ctx.search(base, filter, sc);
		while (ne.hasMore()) {
			SearchResult sr = (SearchResult) ne.next();
			if (sr.getName() != null && sr.getName().toLowerCase().startsWith("ou")) {
				addUserData(sr);
			} else {
				addDeptData(sr.getName());
			}
		}
		ctx.close();
	}

	private void addUserData(SearchResult sr) throws NamingException {
		UserBean userBean = new UserBean();
		for (String unRead : UNREAD_USER) {
			if (sr.getName().toLowerCase().contains(unRead)) {
				return;
			}
		}
		Attributes at = sr.getAttributes();
		NamingEnumeration ane = at.getAll();
		while (ane.hasMore()) {
			Attribute attr = (Attribute) ane.next();
			initUserBean(userBean, attr);
		}
		if (sr.getName().split(",").length == 1) {
			userBean.setPosName(LianHeDianZiAfterItem.BASE);
			userBean.setPosNum(LianHeDianZiAfterItem.BASE);
			userBean.setDeptNum(LianHeDianZiAfterItem.BASE);
		} else {
			String[] userDN = sr.getName().split(",");
			String firstOU = "";
			if (LianHeDianZiAfterItem.SPECIALOU.equalsIgnoreCase(userDN[1]))
				firstOU = getValue(userDN[2]);
			else {
				firstOU = getValue(userDN[1]);
			}
			if (firstOU.endsWith("\"")) {
				firstOU = firstOU.substring(0, firstOU.length() - 1);
			}
			userBean.setPosName(firstOU);
			userBean.setPosNum(firstOU);
			userBean.setDeptNum(firstOU);
		}
		this.userPosBeanList.add(userBean);
	}

	private String getValue(String source) {
		if (source == null) {
			return "";
		}
		return source.split("=")[1];
	}

	private void initUserBean(UserBean userBean, Attribute attr) throws NamingException {
		try {
			if (COLUMN_METHOD_REFERENCES.get(attr.getID()) != null) {
				Method md = userBean.getClass().getDeclaredMethod(COLUMN_METHOD_REFERENCES.get(attr.getID()),
						String.class);
				md.invoke(userBean, attr.get().toString());
				userBean.setEmailType(0);
			}
		} catch (Exception e) {
			log.error("初始化UserBean时出现异常", e);
		}
	}

	/**
	 * 添加部门数据
	 * 
	 * @param name
	 * @return
	 */
	private void addDeptData(String name) {
		if (isBase(name)) {
			DeptBean dept = new DeptBean();
			dept.setDeptName(LianHeDianZiAfterItem.BASE);
			dept.setDeptNum(LianHeDianZiAfterItem.BASE);
			dept.setPerDeptNum("0");
			this.deptBeanList.add(dept);
			return;
		}

		for (String singerUnread : UNREAD_ORG) {
			if (name.toLowerCase().contains(singerUnread)) {
				return;
			}
		}

		String[] dn = name.split(",");
		DeptBean dept = new DeptBean();
		dept.setDeptName(getValue(dn[0]));
		dept.setDeptNum(getValue(dn[0]));

		if (name.split(",").length == 1)
			dept.setPerDeptNum(LianHeDianZiAfterItem.BASE);
		else {
			dept.setPerDeptNum(getValue(dn[1]));
		}
		this.deptBeanList.add(dept);
	}

	/**
	 * 根目录为空
	 * 
	 * @param name
	 * @return
	 */
	private boolean isBase(String name) {
		return StringUtils.isBlank(name);
	}

	public List<DeptBean> getDeptBeanList() {
		return this.deptBeanList;
	}

	public List<UserBean> getUserPosBeanList() {
		return this.userPosBeanList;
	}
}