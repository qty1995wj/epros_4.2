package com.jecn.epros.domain.process.relatedFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;

public class RelatedFlow {
    private Long id;
    private String name;
    private String resPeopleName;
    private String resOrgName;
    /**
     * 流程编号
     */
    private String flowCode;

    public LinkResource getLink() {
        return new LinkResource(id, name, ResourceType.PROCESS, TreeNodeUtils.NodeType.rule);
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResPeopleName() {
        return resPeopleName;
    }

    public void setResPeopleName(String resPeopleName) {
        this.resPeopleName = resPeopleName;
    }

    public String getResOrgName() {
        return resOrgName;
    }

    public void setResOrgName(String resOrgName) {
        this.resOrgName = resOrgName;
    }

    public String getFlowCode() {
        return flowCode;
    }

    public void setFlowCode(String flowCode) {
        this.flowCode = flowCode;
    }
}
