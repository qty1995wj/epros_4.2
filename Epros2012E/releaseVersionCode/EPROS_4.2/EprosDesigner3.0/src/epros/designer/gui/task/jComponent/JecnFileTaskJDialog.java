package epros.designer.gui.task.jComponent;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.action.designer.file.IFileAction;
import com.jecn.epros.server.action.designer.task.IJecnTaskRecordAction;
import com.jecn.epros.server.action.designer.task.approve.IJecnCreateTaskAction;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.task.buss.TaskApproveConstant;
import epros.designer.gui.task.buss.TaskCommon;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 文件任务审批
 * 
 * @author Administrator
 * @date： 日期：Nov 20, 2012 时间：9:55:00 AM
 */
public class JecnFileTaskJDialog extends JecnTaskJDialog {

	private Long fileId = null;
	/** 文件信息 */
	private JecnFileBeanT jecnFileBeanT = null;

	public JecnFileTaskJDialog(JecnTreeNode treeNode, JecnTree jTree,int approveType) {
		super(jTree, treeNode,approveType);
		// if ("file".equals(treeNode.getJecnTreeBean().getTreeNodeType()
		// .toString())) {
		// isUpload = false;
		// } else {
		// this.isUpload = true;
		// }
		this.fileId = treeNode.getJecnTreeBean().getId();
		init();
		// if (isUpload) {
		// initJtable();
		// }
	}

	/**
	 * 上传文件任务 废弃
	 * 
	 */
	private void initJtable() {
		Vector title = new Vector();
		title.add(TaskApproveConstant.fileNumber);
		title.add(TaskApproveConstant.fileName);
		title.add(TaskApproveConstant.filePath);
		title.add(TaskApproveConstant.fileId);
		Vector content = new Vector();
		jTableUploadFile.setModel(new JecnTableModel(0, title, content));
		jTableUploadFile.getSelectionModel().setSelectionMode(
				ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel columnModel = jTableUploadFile.getColumnModel();
		TableColumn column3 = columnModel.getColumn(3);
		column3.setMinWidth(0);
		column3.setMaxWidth(0);
	}

	/**
	 * 检查版本号存不存在
	 */
	protected boolean checkRevsion() {
		if (fileId != null && !"".equals(fileId)) {
			IJecnTaskRecordAction taskRecordAction = ConnectionPool
					.getTaskRecordAction();
			try {
				if (taskRecordAction.isExistVersionbyFlowId(fileId,
						this.jTextFieldVersion.getText(), 1)) {
					JecnOptionPane.showMessageDialog(this,
							TaskApproveConstant.docIdExist);
					return false;
				}
			} catch (Exception ex) {
				log.error("JecnFileTaskJDialog checkRevsion is error",ex);
				JecnOptionPane.showMessageDialog(null,
						TaskApproveConstant.access_server_error);
				return false;
			}
		}
		return true;
	}

	/**
	 * 初始化其他显示
	 */
	protected void initElseShow() {
		// 节点名称
		taskName = treeNode.getJecnTreeBean().getName();
		jTextFieldTaskName.setText(treeNode.getJecnTreeBean().getName());
		IFileAction fileAction = ConnectionPool.getFileAction();
		try {
			jecnFileBeanT = fileAction.getFilBeanById(fileId);
		} catch (Exception ex) {
			log.error("JecnFileTaskJDialog initElseShow is error",ex);
			JecnOptionPane.showMessageDialog(null,
					TaskApproveConstant.access_server_error);
			return;
		}
		// 版本号
		version = this.jTextFieldVersion.getText();
		// 变更说明
		changeDescription = this.jTextAreaChangeDescription.getText();
		if (jecnTaskApplication.getIsPublic() != 0
				|| jecnTaskApplication.getIsFlowNumber() != 0) {
			if (jecnFileBeanT != null) {
				if (jecnTaskApplication.getIsPublic() != 0) {// 0秘密；1公开
					if (jecnFileBeanT.getIsPublic() != null
							&& jecnFileBeanT.getIsPublic() == 0) {// 0秘密；1公开
						this.jComboBoxSecurityClassification
								.setSelectedIndex(0);
					} else if (jecnFileBeanT.getIsPublic() != null
							&& jecnFileBeanT.getIsPublic() != 0) {
						jComboBoxSecurityClassification.setSelectedIndex(1);
					}
				}
				if (jecnTaskApplication.getIsFlowNumber() != 0) {
					if (jecnFileBeanT.getDocId() != null) {
						this.jTextFieldNumber.setText(jecnFileBeanT.getDocId());
					}
				}
			}
		}
	}

	/**
	 * 检查必填项是否填写
	 * 
	 * @return true 验证通过,false :验证失败
	 */
	// public boolean isCheckSubmit() {
	// if (!super.isCheckSubmit()) {
	// return false;
	// }
	// if (isUpload) {// 上传文件任务
	// DefaultTableModel modeModel = (DefaultTableModel) jTableUploadFile
	// .getModel();
	//
	// int rowNum = jTableUploadFile.getSelectedRow();
	// // 如果表格处于编辑状态,责关闭编辑状态
	// if (jTableUploadFile.isEditing()) {
	// jTableUploadFile.getCellEditor().stopCellEditing();
	// }
	// if (rowNum != -1) {
	// if (jTableUploadFile.getCellEditor(rowNum, 0)
	// .getCellEditorValue() != null
	// && jTableUploadFile.getCellEditor(rowNum, 0)
	// .stopCellEditing()) {
	// modeModel.setValueAt(jTableUploadFile.getCellEditor(rowNum,
	// 0).getCellEditorValue(), rowNum, 0);
	// }
	// }
	//
	// if (jTableUploadFile.getRowCount() == 0) {
	// // 上传文件为空,请选择
	// JecnOptionPane.showMessageDialog(this,
	// TaskApproveConstant.uplode_file_is_null);
	// return false;
	// }
	// int rows = modeModel.getRowCount();
	// /** 判断table有没有文件名 */
	// List<String> listRepeat = new ArrayList<String>();
	// for (int i = 0; i < rows; i++) {
	// if (modeModel.getValueAt(i, 2) == null
	// || "".equals(modeModel.getValueAt(i, 2))) {
	// continue;
	// }
	// String fileName = modeModel.getValueAt(i, 1).toString();
	// /** 上传文件 */
	// String str = JecnUserCheckUtil.checkName(fileName);
	// if (!Tool.isNullOrEmtryTrim(str)) {
	// JecnOptionPane.showMessageDialog(this, str);
	// return false;
	// }
	// if (!listRepeat.contains(fileName)) {
	// listRepeat.add(fileName);
	// } else {
	// // 重复存在
	// JecnOptionPane.showMessageDialog(this, fileName
	// + TaskApproveConstant.repeat_xist);
	// return false;
	// }
	// if (TaskCommon.isDirExistFileName(treeNode, fileName, null)) {
	// // 已存在
	// JecnOptionPane.showMessageDialog(this, fileName
	// + TaskApproveConstant.is_exist
	// + treeNode.getJecnTreeBean().getName()
	// + TaskApproveConstant.is_exist);
	// return false;
	// }
	// }
	// }
	// return true;
	// }
	/**
	 * 更新文件的文件
	 * 
	 * @return List<JecnFileBeanT>
	 */
	private List<JecnFileBeanT> DealWithUploadFile() {
		// 上传文件任务(jecnFileBean为当前结点)

		// List<String> listRepeat = new ArrayList<String>();
		// List<String> fileNames = null;
		// IJecnCreateTaskAction jecnDesignerTask = ConnectionPool
		// .getDesignerTaskAction();
		if (jecnFileBeanT == null) {
			return null;
		}
		// 记录当前目录下文件，并验证同目录下输出已经存在待上传文件
		List<JecnFileBeanT> listFileBeanT = new ArrayList<JecnFileBeanT>();
		// if (isUpload) {
		// try {
		// // 获得处于上传文件任务中的文件，验证待上传文件是否存在任务中
		// fileNames = jecnDesignerTask.getUploadFileTaskFileNames(Long
		// .valueOf(treeNode.getJecnTreeBean().getId()));
		// } catch (Exception ex) {
		// JecnOptionPane.showMessageDialog(null,
		// TaskApproveConstant.access_server_error);
		// return null;
		// }
		// DefaultTableModel modeModel = (DefaultTableModel) jTableUploadFile
		// .getModel();
		// // 表格行数
		// int rows = modeModel.getRowCount();
		// // 文件节当前节点下sort 最大值
		// int maxSort = JecnTreeCommon.getMaxSort(treeNode);
		// for (int i = 0; i < rows; i++) {
		// if (modeModel.getValueAt(i, 2) == null
		// || "".equals(modeModel.getValueAt(i, 2))) {
		// continue;
		// }
		// String docId = "";
		// if (modeModel.getValueAt(i, 0) != null
		// && !"".equals(modeModel.getValueAt(i, 0))) {
		// docId = modeModel.getValueAt(i, 0).toString();
		// }
		// // 上传文件
		// String fileName = modeModel.getValueAt(i, 1).toString();
		// int validateResult = this.validateFile(listRepeat, fileName,
		// fileNames, treeNode);
		// if (validateResult == 0) {// 上传文件重名验证
		// // 重复存在
		// JecnOptionPane.showMessageDialog(this, fileName
		// + TaskApproveConstant.repeat_xist);
		// return null;
		// }
		// /** 同目录下此文件是否已存在 */
		// if (validateResult == 1) {
		// JecnOptionPane.showMessageDialog(this, "文件'" + fileName
		// + "'在目录中已存在!");
		// return null;
		// }
		// if (validateResult == 2) {// 该目录下已存在
		// JecnOptionPane.showMessageDialog(null, fileName
		// + TaskApproveConstant.this_dir_has_exist);
		// return null;
		// }
		// // 获取文件信息
		// JecnFileBeanT fileBeanT = new JecnFileBeanT();
		// fileBeanT.setFileName(fileName);
		// // 文件流
		// fileBeanT.setFileStream(TaskCommon.changeFileToByte(modeModel
		// .getValueAt(i, 2).toString()));
		// // 编号
		// fileBeanT.setDocId(docId);
		// // 密级
		// fileBeanT.setIsPublic(getIsPublic());
		// // 文件
		// fileBeanT.setIsDir(1);
		// // 在节点下的顺序
		// fileBeanT.setSortId(maxSort + i);
		// // 创建人
		// fileBeanT.setPeopleID(JecnConstants.loginBean.getJecnUser()
		// .getPeopleId());
		// // 项目ID
		// fileBeanT.setProjectId(JecnConstants.projectId);
		// // 父节点
		// fileBeanT.setPerFileId(treeNode.getJecnTreeBean().getId());
		// fileBeanT.setUpdatePersonId(JecnConstants.loginBean
		// .getJecnUser().getPeopleId());
		// listFileBeanT.add(fileBeanT);
		// }
		// } else {// 更新文件任务
		// 获取上传的文件名称
		// File file = new File(jTextFieldEditFile.getText());
		// String fileName = file.getName();
		/** 更新文件 */
		// String str = JecnUserCheckUtil.checkName(fileName);
		// if (!Tool.isNullOrEmtryTrim(str)) {
		// JecnOptionPane.showMessageDialog(this, str);
		// return null;
		// }
		// JecnTreeNode pNode = (JecnTreeNode) treeNode.getParent();
		// // String oldFileName = jecnFileBean.getFileName();
		//
		// // 同目录下面的文件是不是已经存在
		// if (TaskCommon.isDirExistFileName(pNode, fileName, treeNode
		// .getJecnTreeBean().getName())) {
		// JecnOptionPane.showMessageDialog(this,
		// TaskApproveConstant.this_dir_has_exist);
		// return null;
		// }
		// try {
		// if (jecnDesignerTask.checkFileExists(fileName, Long.valueOf(pNode
		// .getJecnTreeBean().getId()))) {
		// JecnOptionPane.showMessageDialog(this, "该文件在任务中已存在!");
		// return null;
		// }
		// } catch (Exception e) {
		// // 同目录下面的文件是不是已经存在
		// JecnOptionPane.showMessageDialog(null,
		// TaskApproveConstant.this_dir_has_exist);
		// return null;
		// }
		// jecnFileBeanT.setFileName(file.getName());
		// jecnFileBeanT.setFileStream(TaskCommon.changeFileToByteByFile(file));
		// 密级
		if (jecnTaskApplication.getIsPublic() != 0) {
			jecnFileBeanT.setIsPublic(getIsPublic());
		}
		// 编号
		if (this.jecnTaskApplication.getIsFlowNumber() != 0) {
			jecnFileBeanT.setDocId(jTextFieldNumber.getText());
		}
		// 记录待更新文件
		listFileBeanT.add(jecnFileBeanT);
		// }
		return listFileBeanT;
	}

	/**
	 * 上传文件上传文件验证
	 * 
	 * @param listRepeat
	 *            待上传文件集合
	 * @param fileName
	 *            待上传文件名称
	 * @param fileNames
	 *            处于上传文件任务中的文件
	 * @param node
	 *            选择文件目录节点
	 * @return
	 */
	private int validateFile(List<String> listRepeat, String fileName,
			List<String> fileNames, JecnTreeNode node) {
		// 验证本次待上传文件中有没有重名
		if (validateCurUpdateIsRename(listRepeat, fileName)) {
			return 0;
		}
		// 同目录下此文件是否已存在
		if (TaskCommon.isDirExistFileName(node, fileName, null)) {
			return 1;
		}
		// 点击文件目录验证上传文件是否存在
		for (String name : fileNames) {
			if (name.equals(fileName)) {
				return 2;
			}
		}
		return 3;
	}

	/**
	 * 验证本次从本地上传文件中有没有重名
	 * 
	 * @param fileNames
	 * @param fileName
	 */
	private boolean validateCurUpdateIsRename(List<String> listRepeat,
			String fileName) {
		if (!listRepeat.contains(fileName)) {
			listRepeat.add(fileName);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 确定
	 */
	protected boolean submitTask() {
		JecnTempCreateTaskBean createTaskBean = new JecnTempCreateTaskBean();
		// 记录更新或上传的文件
		List<JecnFileBeanT> listFileBeanT = DealWithUploadFile();
		if (listFileBeanT == null || listFileBeanT.size() == 0) {
			return false;
		}
		try {
			IJecnCreateTaskAction designerTaskAction = ConnectionPool
					.getDesignerTaskAction();
			// 组装待处理信息对象
			setJecnTempCreateTaskBean(createTaskBean);
			// 获取待上传文件获取待更新文件
			createTaskBean.setListFileBeanT(listFileBeanT);

			designerTaskAction.createPRFTask(createTaskBean);

			JecnOptionPane.showMessageDialog(this,
					TaskApproveConstant.create_app_sucess);
			List<JecnTreeBean> listNode = ConnectionPool.getFileAction()
					.getChildFiles(treeNode.getJecnTreeBean().getId(),
							JecnConstants.projectId);
			JecnTreeCommon.refreshNode(treeNode, listNode, jTree);
			JecnTreeCommon.autoExpandNode(jTree, treeNode);
		} catch (Exception ex) {
			log.error("JecnFileTaskJDialog submitTask is error",ex);
			JecnOptionPane.showMessageDialog(this,
					TaskApproveConstant.create_app_error);
			return false;
		}
		return true;
	}
}
