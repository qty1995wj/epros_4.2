package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.user.SearchInfo;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UserSqlProvider extends BaseSqlProvider {

    /**
     * 根据人员ID获取用户名
     *
     * @param peopleId
     * @return
     */
    public String findUserNameByIdSql(Long peopleId) {
        return "SELECT TRUE_NAME FROM JECN_USER WHERE PEOPLE_ID = " + peopleId;
    }

    public String findUsers(Map<String, Object> paramMap) {
        Set<Long> setPeopleId = (Set<Long>) paramMap.get("ids");
        return "select * from jecn_user where people_id in " + SqlCommon.getIdsSet(setPeopleId);

    }

    public String findMyResponsibilityProcess(Map<String, Object> map) {
        String sql = "SELECT  JU.TRUE_NAME trueName," +
                "       FS.FLOW_NAME flowName," +
                "       FS.FLOW_ID flowId," +
                "       FS.PUB_TIME pubTime," +
                "       FS.NODE_TYPE nodeType," +
                "       PFS.FLOW_ID architectureId," +
                "       PFS.FLOW_NAME architectureName " +
                "  FROM JECN_FLOW_BASIC_INFO JFBI" +
                " INNER JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON FS.FLOW_ID = JFBI.FLOW_ID" +
                "   AND FS.DEL_STATE = 0" +
                " INNER JOIN JECN_USER JU" +
                "    ON JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID" +
                "   AND JFBI.TYPE_RES_PEOPLE = 0" +
                "   AND JU.ISLOCK = 0" +
                "LEFT JOIN JECN_FLOW_STRUCTURE PFS" +
                "  ON FS.PRE_FLOW_ID = PFS.FLOW_ID" +
                "  AND PFS.DEL_STATE = 0" +
                "  AND PFS.ISFLOW = 0" +
                " WHERE JU.PEOPLE_ID = #{peopleId}" +
                " UNION " +
                "SELECT OI.FIGURE_TYPE trueName," +
                "       FS.FLOW_NAME flowName," +
                "       FS.FLOW_ID flowId," +
                "       FS.PUB_TIME pubTime," +
                "       FS.NODE_TYPE nodeType," +
                "       PFS.FLOW_ID architectureId," +
                "       PFS.FLOW_NAME architectureName " +
                "  FROM JECN_FLOW_BASIC_INFO JFBI" +
                " INNER JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON FS.FLOW_ID = JFBI.FLOW_ID" +
                "   AND FS.DEL_STATE = 0" +
                " INNER JOIN JECN_FLOW_ORG_IMAGE OI" +
                "    ON OI.FIGURE_ID = JFBI.RES_PEOPLE_ID" +
                "   AND JFBI.TYPE_RES_PEOPLE = 1" +
                " INNER JOIN JECN_USER_POSITION_RELATED JUPR" +
                "    ON OI.FIGURE_ID = JUPR.FIGURE_ID" +
                " INNER JOIN JECN_USER JU" +
                "    ON JU.PEOPLE_ID = JUPR.PEOPLE_ID" +
                "   AND JU.ISLOCK = 0" +
                "LEFT JOIN JECN_FLOW_STRUCTURE PFS" +
                "  ON FS.PRE_FLOW_ID = PFS.FLOW_ID" +
                "  AND PFS.DEL_STATE = 0" +
                "  AND PFS.ISFLOW = 0" +
                " WHERE JU.PEOPLE_ID = #{peopleId} order by pubTime desc";
        return sql;
    }

    public String findMyResponsibilityRule(Map<String, Object> map) {
        String sql = "SELECT JU.TRUE_NAME trueName," +
                "       jr.rule_name ruleName," +
                "       jr.id   id," +
                "       jr.PUB_TIME  pubTime," +
                "       jr.is_dir nodeType," +
                "       pjr.id   dirId," +
                "       pjr.rule_name dirName" +
                "  FROM Jecn_Rule jr" +
                " INNER JOIN JECN_USER JU" +
                "    ON JU.PEOPLE_ID = jr.RES_PEOPLE_ID" +
                "   AND jr.TYPE_RES_PEOPLE = 0" +
                "   AND JU.ISLOCK = 0" +
                "  LEFT JOIN JECN_RULE PJR" +
                "  ON  JR.PER_ID = PJR.ID" +
                "  AND PJR.IS_DIR = 0 " +
                " WHERE JU.PEOPLE_ID = #{peopleId} and jr.del_state = 0" +
                " UNION " +
                " SELECT OI.FIGURE_TYPE trueName," +
                "       jr.rule_name ruleName," +
                "       jr.id   id," +
                "       jr.PUB_TIME  pubTime," +
                "       jr.is_dir nodeType," +
                "       pjr.id   dirId," +
                "       pjr.rule_name dirName" +
                "  FROM Jecn_Rule jr" +
                " INNER JOIN JECN_FLOW_ORG_IMAGE OI" +
                "    ON OI.FIGURE_ID = JR.RES_PEOPLE_ID" +
                "   AND JR.TYPE_RES_PEOPLE = 1" +
                " INNER JOIN JECN_USER_POSITION_RELATED JUPR" +
                "    ON OI.FIGURE_ID = JUPR.FIGURE_ID" +
                " INNER JOIN JECN_USER JU" +
                "    ON JU.PEOPLE_ID = JUPR.PEOPLE_ID" +
                "   AND JU.ISLOCK = 0" +
                "  LEFT JOIN JECN_RULE PJR" +
                "  ON  JR.PER_ID = PJR.ID" +
                "  AND PJR.IS_DIR = 0 " +
                " WHERE JU.PEOPLE_ID = #{peopleId}  AND jr.DEL_STATE = 0" +
                " order by pubTime desc";
        return sql;
    }


    public static final String BASE_FLOW_SQL = "SELECT distinct" +
            "  jfs.flow_id as flow_id," +
            "  jfs.FLOW_NAME as flow_NAME," +
            "  jfs.flow_id_input as flow_id_input," +
            "  jfs.update_date as pub_time," +
            "  jfo.org_id as org_id," +
            "  jfo.org_name as org_name," +
            "  CASE" +
            "	WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN           JU.TRUE_NAME" +
            "	WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN           JFOI.FIGURE_TEXT" +
            "  END  as resPeople," +
            " 1 as is_public" +
            " FROM" +
            "  flows jfs" +
            "  left join JECN_TASK_HISTORY_NEW jthn on jfs.HISTORY_ID=jthn.ID" +
            "  left join JECN_FLOW_BASIC_INFO jfbi on jfs.flow_id=jfbi.flow_id" +
            "  left join JECN_FLOW_RELATED_ORG jfro on jfro.FLOW_ID=jfs.FLOW_ID" +
            "  left join JECN_FLOW_ORG jfo on jfo.org_id=jfro.org_id" +
            "  left join jecn_user u1 on jfbi.FICTION_PEOPLE_ID =u1.people_id" +
            "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFOI.FIGURE_ID = JFBI.RES_PEOPLE_ID   " +
            "  LEFT JOIN JECN_USER JU ON JU.ISLOCK = 0 AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID" +
            "  where 1=1 ";

    public static final String BASE_RULE_SQL = "	SELECT distinct" +
            "		jr.id as rule_id," +
            "		jr.rule_name as rule_name," +
            "		jr.rule_number as rule_number," +
            "		jrp.rule_name AS parentDir," +
            "		jfo.org_name AS org_name," +
            "		jr.update_date AS publishDate," +
            "       1 as is_public" +
            "	FROM" +
            "		rules jr" +
            "	LEFT JOIN jecn_rule jrp ON jr.per_id = jrp.id" +
            "	LEFT JOIN JECN_FLOW_ORG jfo ON jr.ORG_ID = jfo.org_id" +
            "	LEFT JOIN FLOW_RELATED_CRITERION frc ON frc.CRITERION_CLASS_ID = jr.id" +
            "   where jr.is_dir<>0 and jr.del_state=0 ";

    public static final String BASE_RISK_SQL = " select distinct " +
            " a.id as id," +
            " a.name as name," +
            " a.risk_code as risk_code," +
            " a.grade as grade" +
            " from risks a WHERE 1=1 ";

    public String getMyProcesses(Map<String, Object> paramMap) {
        String sql = getMyFlows(paramMap) + BASE_FLOW_SQL;
        Object name = paramMap.get("name");
        Object number = paramMap.get("number");
        if (name != null && StringUtils.isNotBlank(name.toString())) {
            sql = sql + " AND jfs.flow_name LIKE '%" + name.toString() + "%'";
        }
        if (number != null && StringUtils.isNotBlank(number.toString())) {
            sql = sql + " AND jfs.flow_name LIKE '%" + number.toString() + "%'";
        }
        return sql;
    }

    public String getMyRules(Map<String, Object> paramMap) {
        String sql = getMyFlows(paramMap) + " ,rules as (SELECT jr.* FROM flows"
                + " INNER JOIN flow_related_criterion jrc ON flows.flow_id = jrc.FLOW_ID"
                + " INNER JOIN jecn_rule jr ON jr.id = jrc.CRITERION_CLASS_ID and jr.is_dir<>0 ) "
                + BASE_RULE_SQL;
        Object name = paramMap.get("name");
        Object number = paramMap.get("number");
        if (name != null && StringUtils.isNotBlank(name.toString())) {
            sql = sql + " AND jr.rule_name LIKE '%" + name.toString() + "%'";
        }
        if (number != null && StringUtils.isNotBlank(number.toString())) {
            sql = sql + " AND jr.rule_number LIKE '%" + number.toString() + "%'";
        }
        return sql;
    }

    public String getMyRisks(Map<String, Object> paramMap) {
        String sql = getMyFlows(paramMap) +
                " ,risks as (select jr.* " +
                "  from jecn_risk jr" +
                "  inner join JECN_CONTROL_POINT jcp on jcp.risk_id=jr.id" +
                "  inner join jecn_flow_structure_image jfsi on jfsi.figure_id=jcp.active_id" +
                "  inner join flows on flows.flow_id=jfsi.flow_id where 1=1 ";
        sql += ") " + BASE_RISK_SQL;

        Object name = paramMap.get("name");
        Object number = paramMap.get("number");
        if (name != null && StringUtils.isNotBlank(name.toString())) {
            sql = sql + " AND a.name LIKE '%" + name.toString() + "%'";
        }
        if (number != null && StringUtils.isNotBlank(number.toString())) {
            sql = sql + " AND a.risk_code LIKE '%" + number.toString() + "%'";
        }
        return sql;
    }


    private String getMyFlows(Map<String, Object> paramMap) {
        String sql =
                "WITH flows AS" +
                        " (SELECT DISTINCT *" +
                        "    FROM (SELECT JFS.*, JFOI.FIGURE_ID AS POS_ID, JFO.ORG_ID" +
                        "            FROM JECN_FLOW_STRUCTURE_IMAGE JFSI" +
                        "           INNER JOIN JECN_FLOW_STRUCTURE JFS" +
                        "              ON JFSI.FLOW_ID = JFS.FLOW_ID" +
                        "             AND JFS.DEL_STATE = 0" +
                        "             AND JFS.PROJECTID = #{projectId}" +
                        "           INNER JOIN PROCESS_STATION_RELATED PRS" +
                        "              ON JFSI.FIGURE_ID = PRS.FIGURE_FLOW_ID" +
                        "             AND PRS.TYPE = '0'" +
                        "           INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" +
                        "              ON PRS.FIGURE_POSITION_ID = JFOI.FIGURE_ID" +
                        "           INNER JOIN JECN_FLOW_ORG JFO" +
                        "              ON JFOI.ORG_ID = JFO.ORG_ID" +
                        "             AND JFO.DEL_STATE = 0" +
                        "             AND JFO.PROJECTID = #{projectId}" +
                        "           INNER JOIN JECN_USER_POSITION_RELATED JUPR" +
                        "              ON JFOI.FIGURE_ID = JUPR.FIGURE_ID" +
                        "           INNER JOIN JECN_USER JU ON JU.PEOPLE_ID=JUPR.PEOPLE_ID" +
                        "             AND JU.LOGIN_NAME = #{username}" +
                        "          UNION" +
                        "          SELECT JFS.*, JFOI.FIGURE_ID AS POS_ID, JFO.ORG_ID" +
                        "            FROM JECN_FLOW_STRUCTURE_IMAGE JFSI" +
                        "           INNER JOIN JECN_FLOW_STRUCTURE JFS" +
                        "              ON JFSI.FLOW_ID = JFS.FLOW_ID" +
                        "             AND JFS.DEL_STATE = 0" +
                        "             AND JFS.PROJECTID = #{projectId}" +
                        "           INNER JOIN PROCESS_STATION_RELATED PRS" +
                        "              ON JFSI.FIGURE_ID = PRS.FIGURE_FLOW_ID" +
                        "             AND PRS.TYPE = '1'" +
                        "           INNER JOIN JECN_POSITION_GROUP_R JPGR" +
                        "              ON PRS.FIGURE_POSITION_ID = JPGR.GROUP_ID" +
                        "           INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" +
                        "              ON JPGR.FIGURE_ID = JFOI.FIGURE_ID" +
                        "           INNER JOIN JECN_FLOW_ORG JFO" +
                        "              ON JFOI.ORG_ID = JFO.ORG_ID" +
                        "             AND JFO.DEL_STATE = 0" +
                        "             AND JFO.PROJECTID = #{projectId}" +
                        "           INNER JOIN JECN_USER_POSITION_RELATED JUPR" +
                        "              ON JFOI.FIGURE_ID = JUPR.FIGURE_ID" +
                        "           INNER JOIN JECN_USER JU ON JU.PEOPLE_ID=JUPR.PEOPLE_ID" +
                        "             AND JU.LOGIN_NAME = #{username}" +
                        ") flows";
        sql += " )";
        return sql;
    }

    public String getMyResources(Map<String, Object> paramMap) {
        String sql = getMyFlows(paramMap)
                + " ,rules as (SELECT jr.* FROM flows"
                + " INNER JOIN flow_related_criterion jrc ON flows.flow_id = jrc.FLOW_ID"
                + " INNER JOIN jecn_rule jr ON jr.id = jrc.CRITERION_CLASS_ID and jr.is_dir<>0 )" +
                "  ,risks as (select jr.* " +
                "  from jecn_risk jr" +
                "  inner join JECN_CONTROL_POINT jcp on jcp.risk_id=jr.id" +
                "  inner join jecn_flow_structure_image jfsi on jfsi.figure_id=jcp.active_id" +
                "  inner join flows on flows.flow_id=jfsi.flow_id ) " +
                " select (select count(distinct flow_id) from flows)as processNum," +
                "        (select count(distinct id) from rules)as ruleNum," +
                "        (select count(distinct id) from risks)as riskNum";

        if (isOracle()) {
            sql += " from dual";
        }

        return sql;
    }

    /**
     * 获取用户 角色权限菜单  TYPE = 0：默认角色；1：admin；2：用户自定义角色
     *
     * @param
     * @return
     */
    public String getPermissionsMenuByIdSql(Map<String, Object> params) {
        List<String> list = (List<String>) params.get("roleIds");
        int languageType = params.get("languageType").toString() == null ? 0 : Integer.valueOf(params.get("languageType").toString());//语言类型
        String sql = "SELECT distinct PM.ID, PM.MODULE, PM.ROUTE,";
        if (languageType == 0) {
            sql += " PM.NAME";
        } else {
            sql += " PM.EN_NAME";
        }
        sql += "  NAME, PM.ICON, PM.SORTID,PM.PID ,PM.TYPE,PM.CONTENT_TYPE,PM.EN_NAME " +
                " FROM PERMISSIONS_MENU PM,ROLE_PERMISSIONS_MENU RPM " +
                " where RPM.MENU_ID = PM.ID AND rpm.ROLE_ID in" + SqlCommon.getStrs(list) + " order by SORTID";
        return sql;
    }

    /**
     * 获取用户 角色权限菜单  TYPE = 0：默认角色；1：admin；2：用户自定义角色
     *
     * @param peopleId
     * @return
     */
    public String getPermissionsMenuByPeopleIdSql(Long peopleId) {
        String sql = "SELECT PM.ID, PM.MODULE, PM.ROUTE, PM.NAME, PM.ICON, PM.SORTID,PM.TYPE,PM.CONTENT_TYPE" +
                "  FROM PERMISSIONS_MENU PM" +
                "  LEFT JOIN ROLE_PERMISSIONS_MENU RPM" +
                "    ON RPM.MENU_ID = PM.ID" +
                "  LEFT JOIN USER_ROLE_PERMISSIONS URP" +
                "    ON URP.ROLE_ID = RPM.ROLE_ID" +
                "   AND URP.PEOPLE_ID = " + peopleId +
                " WHERE RPM.MENU_ID IS NOT NULL ORDER BY PM.SORTID";
        return sql;
    }

    /**
     * 获取用户默认角色权限菜单  TYPE = 0：默认角色；1：admin；2：用户自定义角色
     *
     * @return
     */
    public String getDefaultPermissionsMenuSql() {
        String sql = "SELECT PM.ID, PM.MODULE, PM.ROUTE, PM.NAME, PM.ICON, PM.SORTID,PM.TYPE,PM.CONTENT_TYPE" +
                "  FROM PERMISSIONS_MENU PM" +
                "  LEFT JOIN ROLE_PERMISSIONS_MENU RPM" +
                "    ON RPM.MENU_ID = PM.ID" +
                "  LEFT JOIN ROLE_PERMISSIONS RP" +
                "    ON RP.ID = RPM.ROLE_ID" +
                "   AND RP.TYPE = 0 " +
                "  WHERE RP.ID IS NOT NULL ORDER BY PM.SORTID";
        return sql;
    }

    public String getMyStarsNumSql(Map<String, Object> params) {
        return "select count(*) from (" + getMyStarsSql(params) + ")a";
    }

    public String getVisitTopProcess(Map<String, Object> params) {
        String ids = params.get("ids").toString();
        String sql = "WITH flows AS (" +
                "		SELECT" +
                "			f.*" +
                "		FROM" +
                "			JECN_FLOW_STRUCTURE f where f.FLOW_ID in " + ids +
                "	)," +
                "	 month_data AS (" +
                "		SELECT" +
                "			f.flow_id AS id," +
                "			f.flow_name AS name," +
                SqlCommon.DateCastYearAndMonthStr("jol.OPERATOR_TIME") +
                "	AS month_time " +
                "		FROM" +
                "			JECN_OPR_LOG jol" +
                "		INNER JOIN flows f ON jol.RELATED_TYPE in (0,7)" +
                "		AND jol.RELATED_ID = f.flow_id" +
                "		WHERE" +
                "			jol.OPERATOR_TIME BETWEEN " + SqlCommon.getBetweenAndSql(params.get("startTime").toString()) +
                "		AND " + SqlCommon.getBetweenAndSql(params.get("endTime").toString()) +
                "	)," +
                "	 month_count AS (" +
                "		SELECT" +
                "			m.id," +
                "			m.name," +
                "			m.month_time," +
                "			COUNT (*) AS num" +
                "		FROM" +
                "			month_data m" +
                "		GROUP BY" +
                "			m.id," +
                "			m.name," +
                "			m.month_time" +
                "	)" +
                " SELECT" +
                "		c.id," +
                "		c.name," +
                "		c.month_time as month," +
                "		c.num" +
                "	FROM" +
                "		month_count c ";
        return sql;
    }

    public String getVisitTopProcessIds(Map<String, Object> params) {

        return getVisitTopProcessSql(params) + "	select c.id from month_count c order by c.value desc";
    }

    private String getVisitTopProcessSql(Map<String, Object> params) {
        String flowSql = SecuritySqlProvider.getAuthorityProcessesSql();
        String sql = "	WITH flows AS (" +
                "		SELECT" +
                "			jfs.*" +
                "		FROM" +
                " JECN_FLOW_STRUCTURE jfs" +
                " inner join (" + flowSql + ")a  on jfs.flow_id=a.flow_id" +
                "	)," +
                "	 month_data AS (" +
                "		SELECT" +
                "			f.flow_id AS id," +
                "			f.flow_name AS name," +
                SqlCommon.DateCastYearAndMonthStr("jol.OPERATOR_TIME") +
                "	 AS month_time " +
                "		FROM" +
                "			JECN_OPR_LOG jol" +
                "		INNER JOIN flows f ON jol.RELATED_TYPE = 0" +
                "		AND jol.RELATED_ID = f.flow_id" +
                "		WHERE" +
                "			jol.OPERATOR_TIME BETWEEN "
                + SqlCommon.getBetweenAndSql(params.get("startTime").toString())
                + " AND " + SqlCommon.getBetweenAndSql(params.get("endTime").toString()) +
                "	)," +
                "	 month_count AS (" +
                "		SELECT" +
                "			m.id," +
                "			m.name," +
                "			COUNT (*) AS value" +
                "		FROM" +
                "			month_data m" +
                "		GROUP BY" +
                "			m.id," +
                "			m.name" +
                "	)" +
                " select id,name,value from month_count order by value desc";
        return sql;
    }

    private String getVisitTopRuleSql(Map<String, Object> params) {
        String flowSql = SecuritySqlProvider.getAuthorityRuleSql();
        String sql = "	WITH flows AS (" +
                "		SELECT" +
                "			jfs.*" +
                "		FROM" +
                " JECN_RULE jfs" +
                " inner join (" + flowSql + ")a  on jfs.id=a.id" +
                "	)," +
                "	 month_data AS (" +
                "		SELECT" +
                "			f.id AS id," +
                "			f.rule_name AS name," +
                SqlCommon.DateCastYearAndMonthStr("jol.OPERATOR_TIME") +
                "	 AS month_time " +
                "		FROM" +
                "			JECN_OPR_LOG jol" +
                "		INNER JOIN flows f ON jol.RELATED_TYPE = 1" +
                "		AND jol.RELATED_ID = f.id" +
                "		WHERE" +
                "			jol.OPERATOR_TIME BETWEEN "
                + SqlCommon.getBetweenAndSql(params.get("startTime").toString())
                + " AND " + SqlCommon.getBetweenAndSql(params.get("endTime").toString()) +
                "	)," +
                "	 month_count AS (" +
                "		SELECT" +
                "			m.id," +
                "			m.name," +
                "			COUNT (*) AS value" +
                "		FROM" +
                "			month_data m" +
                "		GROUP BY" +
                "			m.id," +
                "			m.name" +
                "	)" +
                " select id,name,value from month_count order by value desc";
        return sql;
    }


    public String getProposeTopProcess(Map<String, Object> params) {

        String flowSql = SecuritySqlProvider.getAuthorityProcessesSql();
        String sql = "with flows as (" +
                "		select jfs.* from JECN_FLOW_STRUCTURE jfs" +
                " inner join (" + flowSql + ")a  on jfs.flow_id=a.flow_id" +
                "		)" +
                "		select f.id,f.name,f.value from (" +
                "		select f.flow_id as id,f.flow_name as name,count(*) as value from flows f" +
                "		inner join JECN_RTNL_PROPOSE jrp on f.FLOW_ID=jrp.RELATION_ID and jrp.RELATION_TYPE=0" +
                "		GROUP BY f.flow_id,f.flow_name" +
                "	)f order by f.value desc";
        return sql;
    }

    public String getProposeTopRule(Map<String, Object> params) {
        String flowSql = SecuritySqlProvider.getAuthorityRuleSql();
        String sql = "with flows as (" +
                "		select jfs.* from JECN_RULE jfs" +
                " inner join (" + flowSql + ")a  on jfs.id=a.id" +
                "		)" +
                "		select f.id,f.name,f.value from (" +
                "		select f.id as id,f.rule_name as name,count(*) as value from flows f" +
                "		inner join JECN_RTNL_PROPOSE jrp on f.id=jrp.RELATION_ID and jrp.RELATION_TYPE=1" +
                "		GROUP BY f.id,f.rule_name" +
                "	)f order by f.value desc";
        return sql;
    }

    public String getVisitTopProcessHistogram(Map<String, Object> params) {
        return getVisitTopProcessSql(params);
    }


    public String getVisitTopRuleHistogram(Map<String, Object> params) {
        return getVisitTopRuleSql(params);
    }

    public String listUserOrgAndPosInfos(Map<String, Object> params) {
        String sql = "	SELECT JFOI.FIGURE_ID,JFOI.FIGURE_TEXT,JFO.ORG_ID,JFO.ORG_NAME FROM JECN_USER_POSITION_RELATED JUPR" +
                "	INNER JOIN JECN_FLOW_ORG_IMAGE JFOI ON JUPR.FIGURE_ID=JFOI.FIGURE_ID" +
                "	INNER JOIN JECN_FLOW_ORG JFO ON JFOI.ORG_ID=JFO.ORG_ID" +
                "	WHERE JUPR.PEOPLE_ID=#{peopleId} ORDER BY JUPR.FIGURE_ID ASC";
        return sql;
    }

    public String validateUserPwd(Map<String, Object> params) {
        String sql = "select count(*) from jecn_user u where u.people_id=#{peopleId} and u.password=#{pwd} ";
        return sql;
    }

    public String getMyStarsSql(Map<String, Object> map) {
        String sql =
                "SELECT *" +
                        "  FROM (SELECT ST.ID," +
                        "               ST.USER_ID," +
                        "               CASE" +
                        "                 WHEN ST.TYPE = 0 THEN" +
                        "                  FS.FLOW_ID" +
                        "                 WHEN ST.TYPE = 1 THEN" +
                        "                  FS_M.Flow_Id" +
                        "                 WHEN ST.TYPE = 2 THEN" +
                        "                  JU.ID" +
                        "                 WHEN ST.TYPE = 3 THEN" +
                        "                  JR.ID" +
                        "                 WHEN ST.TYPE = 4 THEN" +
                        "                  JF.file_id" +
                        "                 WHEN ST.TYPE = 5 THEN" +
                        "                  JCC.CRITERION_CLASS_ID" +
                        "                 ELSE" +
                        "                  ST.RELATED_ID" +
                        "               END RELATED_ID," +
                        "               ST.TYPE," +
                        "               CASE" +
                        "                 WHEN ST.TYPE = 0 THEN" +
                        "                  FS.FLOW_NAME" +
                        "                 WHEN ST.TYPE = 1 THEN" +
                        "                  FS_M.FLOW_NAME" +
                        "                 WHEN ST.TYPE = 2 THEN" +
                        "                  JU.RULE_NAME" +
                        "                 WHEN ST.TYPE = 3 THEN" +
                        "                  JR.NAME" +
                        "                 WHEN ST.TYPE = 4 THEN" +
                        "                  JF.FILE_NAME" +
                        "                 WHEN ST.TYPE = 5 THEN" +
                        "                  JCC.CRITERION_CLASS_NAME" +
                        "                 ELSE" +
                        "                  ST.NAME" +
                        "               END NAME," +
                        "               ST.CREATE_DATE," +
                        "               ST.SORTED_DATE" +
                        "          FROM JECN_MY_STAR ST" +
                        "          LEFT JOIN JECN_FILE JF" +
                        "            ON JF.FILE_ID = ST.RELATED_ID" +
                        "           AND ST.TYPE = 4" +
                        "           AND JF.DEL_STATE = 0" +
                        "          LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                        "            ON FS.FLOW_ID = ST.RELATED_ID" +
                        "           AND ST.TYPE = 0" +
                        "           AND FS.DEL_STATE = 0" +
                        "          LEFT JOIN JECN_FLOW_STRUCTURE FS_M" +
                        "            ON FS_M.FLOW_ID = ST.RELATED_ID" +
                        "           AND ST.TYPE = 1" +
                        "           AND FS_M.DEL_STATE = 0" +
                        "          LEFT JOIN JECN_RULE JU" +
                        "            ON JU.ID = ST.RELATED_ID" +
                        "           AND ST.TYPE = 2" +
                        "           AND JU.DEL_STATE = 0" +
                        "          LEFT JOIN JECN_RISK JR" +
                        "            ON JR.ID = ST.RELATED_ID" +
                        "           AND ST.TYPE = 3" +
                        "          LEFT JOIN JECN_CRITERION_CLASSES JCC" +
                        "            ON JCC.CRITERION_CLASS_ID = ST.RELATED_ID" +
                        "           AND ST.TYPE = 5" +
                        "         WHERE USER_ID =  #{userId}) T" +
                        " WHERE T.RELATED_ID IS NOT NULL";
        if (map.get("search") == null) {
            return sql;
        }
        SearchInfo searchInfo = (SearchInfo) map.get("search");
        if (StringUtils.isNotBlank(searchInfo.getName())) {
            sql += " AND NAME LIKE '%" + searchInfo.getName() + "%'";
        }
        if (searchInfo.getType() != -1) {
            sql += " AND type = " + searchInfo.getType();
        }
        return sql;
    }

    /**
     * 获取部门鸟瞰图
     *
     * @param map
     * @return
     */
    public String findDeptAerialMapsByPeopleSql(Map<String, Object> map) {
        return "SELECT DISTINCT JFS.FLOW_ID flowId, JFS.FLOW_NAME flowName,JFS.PUB_TIME pubDate" +
                "  FROM JECN_USER JU" +
                " INNER JOIN JECN_USER_POSITION_RELATED JUPR" +
                "    ON JUPR.PEOPLE_ID = JU.PEOPLE_ID" +
                " INNER JOIN JECN_FLOW_ORG_IMAGE OI" +
                "    ON JUPR.FIGURE_ID = OI.FIGURE_ID" +
                " INNER JOIN JECN_FLOW_RELATED_ORG JFRO" +
                "    ON OI.ORG_ID = JFRO.ORG_ID" +
                " INNER JOIN JECN_FLOW_STRUCTURE JFS" +
                "    ON JFS.FLOW_ID = JFRO.FLOW_ID AND JFS.DEL_STATE = 0 AND JFS.NODE_TYPE = 2." +
                " WHERE JU.PEOPLE_ID = #{peopleId} ";
    }

    public String findByUsernameSql(String username) {
        return "select PEOPLE_ID as id,LOGIN_NAME as username,PASSWORD as password,ISLOCK as isLock,TRUE_NAME name"
                + " from JECN_USER JU "
                + " where upper(JU.LOGIN_NAME)= '" + username.toUpperCase() + "'";
    }
}
