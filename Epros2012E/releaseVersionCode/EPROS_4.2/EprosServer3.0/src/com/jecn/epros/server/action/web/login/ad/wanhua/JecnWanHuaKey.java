package com.jecn.epros.server.action.web.login.ad.wanhua;

/**
 * java秘钥、加密解密相关api
 */
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.commons.codec.binary.Base64;

public class JecnWanHuaKey {

	/**
	 * 解析从HTTP Header中获取的LTPAToken串并截取用户uid
	 * 
	 * @param ltpaToken
	 *            从HTTP Header中获取的LTPAToken串
	 * @return 用户uid
	 * @throws Exception
	 */
	public static String getUid(String ltpaToken) throws Exception {
		return getUidFromLtpaToken(getPlainLtpaToken(ltpaToken));
	}

	/**
	 * 从解析后的LTPAToken串中截取用户uid
	 * 
	 * @param ltpaToken
	 *            解析后的LTPAToken
	 * @return 用户uid
	 * @throws Exception
	 */
	private static String getUidFromLtpaToken(String plainLtpaToken) {
		if (plainLtpaToken.indexOf("uid=") == -1) {
			plainLtpaToken = plainLtpaToken.substring(plainLtpaToken.indexOf("UID=") + 4);
		} else {
			plainLtpaToken = plainLtpaToken.substring(plainLtpaToken.indexOf("uid=") + 4);
		}

		return plainLtpaToken.substring(0, plainLtpaToken.indexOf(","));
	}

	/**
	 * 使用LTPA 3DESKey加密串解析从HTTP Header中获取的LTPAToken串
	 * 
	 * @param ltpaToken
	 *            从HTTP Header中获取的LTPAToken串
	 * @return 解析后的LTPAToken串
	 * @throws Exception
	 */
	private static String getPlainLtpaToken(String ltpaToken) throws Exception {

		try {
			String ltpaPassword = JecnWanHuaAfterItem.getValue("ltpa.Password");
			String ltpa3DESKey = JecnWanHuaAfterItem.getValue("ltpa.3DESKey");
			byte[] secretKey = getSecretKey(ltpa3DESKey, ltpaPassword);
			// 使用加密key解密ltpa Cookie
			String ltpaPlaintext = new String(decryptLtpaToken(ltpaToken, secretKey));
			// System.out.println(ltpaPlaintext);
			return ltpaPlaintext;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while decryting LTPA token:" + e.getMessage());
		}
	}

	// 获得安全Key
	private static byte[] getSecretKey(String ltpa3DESKey, String password) throws NoSuchAlgorithmException,
			InvalidKeyException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {
		// 使用SHA获得key密码的hash值
		MessageDigest md = MessageDigest.getInstance("SHA");
		md.update(password.getBytes());
		byte[] hash3DES = new byte[24];
		System.arraycopy(md.digest(), 0, hash3DES, 0, 20);
		// 使用0替换后4个字节
		Arrays.fill(hash3DES, 20, 24, (byte) 0);
		// BASE64解码 ltpa3DESKey
		byte[] decode3DES = Base64.decodeBase64(ltpa3DESKey.getBytes());
		// 使用key密码hash值解密已Base64解码的ltpa3DESKey
		return decrypt(decode3DES, hash3DES);
	}

	// 解密LtpaToken
	private static byte[] decryptLtpaToken(String encryptedLtpaToken, byte[] key) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {
		// Base64解码LTPAToken
		final byte[] ltpaByteArray = Base64.decodeBase64(encryptedLtpaToken.getBytes());
		// 使用key解密已Base64解码的LTPAToken
		return decrypt(ltpaByteArray, key);
	}

	// DESede/ECB/PKC5Padding解方法
	private static byte[] decrypt(byte[] ciphertext, byte[] key) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {
		final Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
		final KeySpec keySpec = new DESedeKeySpec(key);
		final Key secretKey = SecretKeyFactory.getInstance("TripleDES").generateSecret(keySpec);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		return cipher.doFinal(ciphertext);
	}

}
