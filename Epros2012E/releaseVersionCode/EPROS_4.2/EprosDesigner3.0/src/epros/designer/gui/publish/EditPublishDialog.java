package epros.designer.gui.publish;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 编辑文控信息
 * 
 * @author ZHANGJIE
 * @date： 日期：2012-10-31
 */
public class EditPublishDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(EditPublishDialog.class);
	/** 主面板 */
	protected JecnPanel mainPanel = new JecnPanel();

	/** 头部面板 */
	protected JecnPanel topPanel = new JecnPanel();

	/** 信息面板 */
	protected JecnPanel infoPanel = new JecnPanel();

	/** 按钮面板 */
	protected JecnPanel butPanel = new JecnPanel();

	/** 流程名称显示Lab */
	protected JLabel flowNameLab = new JLabel();

	/** 历史记录下拉菜单 */
	JMenu historyMenu = new JMenu(JecnProperties.getValue("historyVersion"));

	protected JecnPanel menuPanel = new JecnPanel();

	/** 流程名称下的分割线 */
	protected JTextField namebutomField = new JTextField();

	/** 版本号Lab */
	protected JLabel versonNumLab = new JLabel(JecnProperties.getValue("versionNumC"));

	/** 版本号Field */
	protected JTextField versonNumField = new JTextField();

	public JTextField getVersonNumField() {
		return versonNumField;
	}

	public void setVersonNumField(JTextField versonNumField) {
		this.versonNumField = versonNumField;
	}

	/*
	*//** 拟稿人Lab */
	/*
	 * protected JLabel draftPeopleLab = new
	 * JLabel(JecnProperties.getValue("draftPeopleC"));
	 *//** 拟稿人Field */
	/*
	 * protected JTextField draftPeopleField = new JTextField();
	 */
	/** 文控审核人Lab */
	protected JLabel documentControlAuditLab = new JLabel();// "文控审核人："

	/** 文控审核人Field */
	protected JTextField documentControlAuditField = new JTextField();

	/** 部门审核人Lab */
	protected JLabel deptAuditLab = new JLabel();// "部门审核人："

	/** 部门审核人Field */
	protected JTextField deptAuditField = new JTextField();

	/** 评审人Lab */
	protected JLabel reviewersAuditLab = new JLabel();// "评审人："

	/** 评审人Field */
	protected JecnTextArea reviewersAuditField = new JecnTextArea();

	/** 评审人面板 */
	protected JScrollPane reviewersAuditScrollPane = new JScrollPane(reviewersAuditField);

	/** 批准人Lab */
	protected JLabel approvalAuditLab = new JLabel();// "批准人："

	/** 批准人Field */
	protected JTextField approvalAuditField = new JTextField();

	/** 各业务体系负责人Lab */
	protected JLabel operationAuditLab = new JLabel();// /"各业务体系负责人："

	/** 各业务体系负责人Field */
	protected JTextField operationAuditField = new JTextField();

	/** IT总监Lab */
	protected JLabel ITPeopleAuditLab = new JLabel();// "IT总监："

	/** IT总监Field */
	protected JTextField iTPeopleAuditField = new JTextField();

	/** 事业部经理Lab */
	protected JLabel divisionManagerLab = new JLabel();// "事业部经理："

	/** 事业部经理Field */
	protected JTextField divisionManagerField = new JTextField();

	/** 总经理Lab */
	protected JLabel generalManagerLab = new JLabel();// "总经理："

	/** 总经理Field */
	protected JTextField generalManagerField = new JTextField();

	/** 变更说明Lab */
	protected JLabel descofChangeLab = new JLabel(JecnProperties.getValue("descofChangeC"));

	/** 变更说明Field */
	protected JecnTextArea descofChangeField = new JecnTextArea();

	public JecnTextArea getDescofChangeField() {
		return descofChangeField;
	}

	public void setDescofChangeField(JecnTextArea descofChangeField) {
		this.descofChangeField = descofChangeField;
	}

	/** 变更说明面板 */
	protected JScrollPane descofChangeScrollPane = new JScrollPane(descofChangeField);

	/** 确定按钮 */
	protected JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消按钮 */
	protected JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 开始结束时间Panel */
	private JecnPanel timePanel = new JecnPanel();

	/** 发布日期，返工次数 Panel */
	private JecnPanel pubReNumPanel = new JecnPanel();

	/** 开始时间Lab */
	private JLabel startTimeLab = new JLabel(JecnProperties.getValue("stTimeC"));

	/** 开始时间Field */
	private JTextField startTimeField = new JTextField();

	/** 结束时间Lab */
	private JLabel endTimeLab = new JLabel(JecnProperties.getValue("eTimeC"));

	/** 结束时间Field */
	private JTextField endTimeField = new JTextField();

	/** 发布日期Lab */
	private JLabel publishLab = new JLabel(JecnProperties.getValue("publishDateC"));

	/** 发布日期Field */
	private JTextField publishField = new JTextField();

	/** 返工次数Lab */
	private JLabel reworkNumLab = new JLabel(JecnProperties.getValue("reworkNumC"));

	/** 返工次数JTextField */
	private JTextField reworkNumField = new JTextField();

	/** 文控历史版本版本号 */
	protected List<String> versList = new ArrayList<String>();

	protected JecnTaskHistoryNew historyNew = null;

	/** 文控审核人阶段集合 */
	protected List<Object> panelList = new ArrayList<Object>();
	/** 文控审核人阶段名称显示Lab集合 */
	private List<Object> labList = new ArrayList<Object>();

	/** 制度文控ID */
	private Long docId = null;

	/** 制度ID */
	private Long ruleId = null;
	/** 验证提示 */
	private JLabel verfyLab = new JLabel();

	private JecnTreeNode selectNode = null;

	protected boolean isOperation = false;

	// 文控信息类型 0流程图， 1 文件， 2 制度模板文件， 3：制度文件，4：流程地图
	private int eidtType = 0;
	// 获取审核人集合
	private List<JecnTaskHistoryFollow> listHisFollow = null;

	/** title */
	private String strTitle = "";
	/** nameTable */
	private String prfName;

	/** 实施日期Lab */
	private JLabel implementDateLab = new JLabel(JecnProperties.getValue("implementationDateC"));

	/** 实施日期JTextField */
	private JTextField implementDateField = new JTextField();

	public List<JecnTaskHistoryFollow> getListHisFollow() {
		return listHisFollow;
	}

	public void setListHisFollow(List<JecnTaskHistoryFollow> listHisFollow) {
		this.listHisFollow = listHisFollow;
	}

	public boolean isOperation() {
		return isOperation;
	}

	public void setOperation(boolean isOperation) {
		this.isOperation = isOperation;
	}

	// 冒号
	// private String chinaColon = JecnProperties.getValue("chinaColon");

	public EditPublishDialog(Long docId, JecnTaskHistoryNew historyNew, Long ruleId, int eidtType,
			JecnTreeNode selectNode) {
		this.historyNew = historyNew;
		this.docId = docId;
		this.ruleId = ruleId;
		// 0流程图， 1 文件， 2 制度模板文件， 3：制度文件，4：流程地图
		this.eidtType = eidtType;
		this.selectNode = selectNode;
		verfyLab.setForeground(Color.red);
		// 编辑的文控名称
		flowNameLab.setText(selectNode.getJecnTreeBean().getName());
		// 获取编辑标题
		initTitle();
		init();
	}

	/**
	 * 获取编辑标题
	 * 
	 */
	private void initTitle() {
		strTitle = JecnProperties.getValue("editHistoryInfo");
		switch (eidtType) {
		case 0:
			prfName = JecnProperties.getValue("flowNameC");
			break;
		case 1:
			prfName = JecnProperties.getValue("fileNameC");
			break;
		case 2:
			prfName = JecnProperties.getValue("ruleNameC");
			break;
		case 3:
			prfName = JecnProperties.getValue("ruleNameC");
			break;
		case 4:
			prfName = JecnProperties.getValue("processMapNameC");
			break;
		}
	}

	protected void init() {
		this.setSize(480, 600);
		this.setTitle(strTitle);
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setModal(true);

		// 根据流程ID获取文控信息版本号集合
		try {
			versList = ConnectionPool.getDocControlAction().findVersionListByFlowId(ruleId, eidtType);
			listHisFollow = ConnectionPool.getDocControlAction().getJecnTaskHistoryFollowList(docId);
		} catch (Exception e) {
			log.error("EditPublishDialog init is error", e);
		}
		Dimension sizemenu = new Dimension(100, 20);
		historyMenu.setPreferredSize(sizemenu);
		historyMenu.setMinimumSize(sizemenu);
		historyMenu.setMaximumSize(sizemenu);

		// 历史版本
		// 获取数据库中的版本信息
		// versList = ;
		for (int i = 0; i < versList.size(); i++) {
			JMenuItem menu = new JMenuItem(versList.get(i));
			historyMenu.add(menu);
		}
		JMenuBar menuBar = new JMenuBar();
		menuPanel.add(menuBar);
		menuBar.add(historyMenu);
		// 拟稿人
		// draftPeopleField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 开始时间显示
		startTimeField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 结束时间显示
		endTimeField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 发布日期显示
		publishField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 返工次数显示
		reworkNumField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		implementDateField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 评审人
		reviewersAuditField.setBorder(null);
		reviewersAuditScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		reviewersAuditScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		reviewersAuditScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 变更说明
		descofChangeField.setBorder(null);
		descofChangeField.setRows(4);
		descofChangeScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		descofChangeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		descofChangeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		historyMenu.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 分割线设置
		Dimension namebuttondim = new Dimension(120, 1);
		namebutomField.setPreferredSize(namebuttondim);
		namebutomField.setMaximumSize(namebuttondim);
		namebutomField.setMinimumSize(namebuttondim);

		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});

		// 文控审核人
		documentControlAuditLab.setVisible(false);
		documentControlAuditField.setVisible(false);
		// 部门审核人
		deptAuditLab.setVisible(false);
		deptAuditField.setVisible(false);
		// 评审人
		reviewersAuditLab.setVisible(false);
		reviewersAuditField.setVisible(false);
		// 批准人
		approvalAuditLab.setVisible(false);
		approvalAuditField.setVisible(false);
		// 各业务体系负责人
		operationAuditLab.setVisible(false);
		operationAuditField.setVisible(false);
		// IT总监
		ITPeopleAuditLab.setVisible(false);
		iTPeopleAuditField.setVisible(false);
		// 事业部经理
		divisionManagerLab.setVisible(false);
		divisionManagerField.setVisible(false);
		// 总经理
		generalManagerLab.setVisible(false);
		generalManagerField.setVisible(false);

		this.startTimeField.setEditable(false);
		this.endTimeField.setEditable(false);
		this.publishField.setEditable(false);
		this.reworkNumField.setEditable(false);
		this.implementDateField.setEditable(false);

		// 版本号显示
		this.versonNumField.setText(historyNew.getVersionId());
		// 拟稿人
		// this.draftPeopleField.setText(historyNew.getDraftPerson());
		// 变更说明显示
		this.descofChangeField.setText(historyNew.getModifyExplain());
		// 开始时间显示
		this.startTimeField.setText(historyNew.getStartTime());
		// 结束时间显示
		this.endTimeField.setText(historyNew.getEndTime());
		// 发布日期显示
		this.publishField.setText(String
				.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(historyNew.getPublishDate())));
		String implementDate = historyNew.getImplementDate() == null ? "" : String.valueOf(new SimpleDateFormat(
				"yyyy-MM-dd").format(historyNew.getImplementDate()));
		this.implementDateField.setText(implementDate);
		// 返工次数显示
		this.reworkNumField.setText(String.valueOf(historyNew.getApproveCount()));

		if (listHisFollow == null) {
			return;
		}
		// draftPeopleField.setEditable(false);
		initLayout();
	}

	/***************************************************************************
	 * 布局
	 * 
	 * @param args
	 */
	protected void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);
		// *********** 头部面板显示信息
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(topPanel, c);
		topPanel.setLayout(new GridBagLayout());
		// 流程名称 flowNameLab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(new JLabel(prfName), c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(flowNameLab, c);
		// 分割线
		c = new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		topPanel.add(namebutomField, c);

		// *********** 中心控件显示面板

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("checFileControl")));
		infoPanel.setLayout(new GridBagLayout());
		// 版本号
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(versonNumLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(versonNumField, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(menuPanel, c);
		/*
		 * // 拟稿人draftPeopleLab c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
		 * GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets, 0, 0);
		 * infoPanel.add(draftPeopleLab, c); c = new GridBagConstraints(1, 1, 2,
		 * 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
		 * GridBagConstraints.HORIZONTAL, insets, 0, 0);
		 * infoPanel.add(draftPeopleField, c);
		 */

		// 循环审核人阶段集合 Panel pubReNumPanel
		// c = new GridBagConstraints(0, 1, 3, 1, 1.0, 0.0,
		// GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
		// insets, 0, 0);
		// infoPanel.add(pubReNumPanel, c);
		// pubReNumPanel.setLayout(new GridBagLayout());

		// for (int i = 0; i < listHisFollow.size(); i++) {
		// JecnTaskHistoryFollow historyFollow = listHisFollow.get(i);
		// EditContrlContentPanel editContrlContentPanel = new
		// EditContrlContentPanel(
		// historyFollow);
		// c = new GridBagConstraints(0, i, 1, 1, 1.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
		// new Insets(0, 0, 0, 0), 0, 0);
		// pubReNumPanel.add(editContrlContentPanel, c);
		//
		// panelList.add(editContrlContentPanel);
		// }
		//		
		int count = 2;
		for (int i = 0; i < listHisFollow.size(); i++) {
			JecnTaskHistoryFollow historyFollow = listHisFollow.get(i);
			JLabel applationLabi = new JLabel();
			JTextField nameFieldi = new JTextField();

			applationLabi.setText(historyFollow.getAppellation(JecnResourceUtil.getLocale()));
			nameFieldi.setText(historyFollow.getName());
			c = new GridBagConstraints(0, count + i, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(applationLabi, c);
			c = new GridBagConstraints(1, count + i, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
					GridBagConstraints.HORIZONTAL, insets, 0, 0);
			infoPanel.add(nameFieldi, c);
			panelList.add(nameFieldi);
			labList.add(applationLabi);
		}

		// 变更说明
		c = new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		infoPanel.add(descofChangeLab, c);
		c = new GridBagConstraints(1, 15, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				insets, 0, 0);
		infoPanel.add(descofChangeScrollPane, c);
		// 开始时间===结束时间 Panel
		c = new GridBagConstraints(0, 16, 3, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		infoPanel.add(timePanel, c);
		timePanel.setLayout(new GridBagLayout());
		// 开始时间
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		timePanel.add(startTimeLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		timePanel.add(startTimeField, c);
		// 结束时间
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		timePanel.add(endTimeLab, c);
		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		timePanel.add(endTimeField, c);

		if (!JecnConfigTool.isHiddenImplDate()) {
			// 发布日期
			c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			timePanel.add(publishLab, c);
			c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, insets, 0, 0);
			timePanel.add(publishField, c);

			// 实施日期
			c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			timePanel.add(implementDateLab, c);
			c = new GridBagConstraints(3, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, insets, 0, 0);
			timePanel.add(implementDateField, c);
		}
		// 返工次数
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		timePanel.add(reworkNumLab, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		timePanel.add(reworkNumField, c);

		// ************ 按钮面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(butPanel, c);
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		butPanel.add(verfyLab);
		butPanel.add(okBut);
		butPanel.add(cancelBut);

		this.getContentPane().add(new JScrollPane(mainPanel));
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		this.dispose();
	}

	/***************************************************************************
	 * 确定
	 */
	protected void okButPerformed() {
		verfyLab.setText("");
		// 变更说明
		String modifyExplain = this.descofChangeField.getText();
		// 循环取得审核人信息
		// for (int i = 0; i < listHisFollow.size(); i++) {
		// JecnTaskHistoryFollow taskHistoryFollow = listHisFollow.get(i);
		// EditContrlContentPanel editContrlContentPanel =
		// (EditContrlContentPanel) this.panelList
		// .get(i);
		// taskHistoryFollow.setName(String.valueOf(editContrlContentPanel
		// .getTitleName().getText()));
		// // listHistroyFlow.add(taskHistoryFollow);
		// }

		// 版本号 versonNumField
		String versonNum = versonNumField.getText().trim();
		if (DrawCommon.isNullOrEmtryTrim(versonNum)) {
			String str = JecnUserCheckUtil.checkNullName(versonNum);
			verfyLab.setText(versonNumLab.getText() + str);
			return;
		}
		// 验证版本号不能重复
		for (String str : versList) {
			if (str.equals(versonNum)) {
				if (historyNew.getVersionId().equals(versonNum)) {
				} else {
					verfyLab.setText(JecnProperties.getValue("versionNumOnly"));
					return;
				}
			}
		}
		if (DrawCommon.checkNameMaxLength(versonNum)) {// 名称不能大于61个汉字或122字母
			verfyLab.setText(versonNumLab.getText().substring(0, versonNumLab.getText().length() - 1)
					+ JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 各个阶段审核人名称长度验证
		for (int i = 0; i < listHisFollow.size(); i++) {
			JecnTaskHistoryFollow taskHistoryFollow = listHisFollow.get(i);
			JTextField resultField = (JTextField) this.panelList.get(i);
			JLabel resultLab = (JLabel) this.labList.get(i);
			// 校验名称是否满足要求,验证（名称，长度）成功返回“”，否则返回相应提示信息 长度122
			String reulstStr = JecnUserCheckUtil.checkNullName(resultField.getText());
			if (!DrawCommon.isNullOrEmtryTrim(reulstStr)) {
				String tipInfo = resultLab.getText().endsWith("：") ? resultLab.getText().substring(0,
						resultLab.getText().length() - 1) : resultLab.getText();
				tipInfo += reulstStr;
				verfyLab.setText(tipInfo);
				return;
			}
			taskHistoryFollow.setName(String.valueOf(resultField.getText()));
			if(taskHistoryFollow.getStageMark() == 0){
				historyNew.setDraftPerson(taskHistoryFollow.getName());
			}
		}
		if (DrawCommon.checkNoteLength(modifyExplain)) {
			verfyLab.setText(JecnProperties.getValue("modifyExplain") + JecnProperties.getValue("lengthNotOut"));
			return;
		}

		historyNew.setModifyExplain(modifyExplain);
		isOperation = true;
		this.dispose();
	}
}
