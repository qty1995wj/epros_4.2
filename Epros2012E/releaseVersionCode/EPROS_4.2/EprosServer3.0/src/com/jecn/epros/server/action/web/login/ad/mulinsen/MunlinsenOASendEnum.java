package com.jecn.epros.server.action.web.login.ad.mulinsen;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.log4j.Logger;

import com.jecn.epros.server.email.buss.EmailMessageBean;
import com.jecn.epros.server.util.JecnUtil;

public enum MunlinsenOASendEnum {
	INSTANCE;
	private static Logger LOGGER = Logger.getLogger(MunlinsenOASendEnum.class);

	static {
		if (MulinsenAfterItem.OA_KEY == null) {
			MulinsenAfterItem.start();
		}
	}

	public String sendMessage(MulinsenOAParamInfo paramInfo) throws UnsupportedEncodingException {
		Map<String, String> jsonMap = new LinkedHashMap<String, String>();

		jsonMap.put("action", paramInfo.getAction());
		jsonMap.put("content", paramInfo.getContent());
		jsonMap.put("hr_userid", paramInfo.getHr_userid());
		jsonMap.put("redirect_url", paramInfo.getRedirect_url());

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : jsonMap.entrySet()) {
			sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		sb.append("key=").append(MulinsenAfterItem.OA_KEY);

		LOGGER.info("builder = " + sb.toString());
		// 获取请求的md5 串
		String md5Code = null;
		md5Code = JecnUtil.MD5(sb.toString().getBytes("UTF-8"));
		// MD5串转换 大写
		String sign = md5Code.toString().toUpperCase();
		paramInfo.setSign(sign);

		JSONObject jsonObject = JSONObject.fromObject(paramInfo);
		LOGGER.info("JSON = " + jsonObject.toString());
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-type", "application/json;charset=utf-8");
		try {
			// return HttpHelper.post(headers, jsonObject.toString(),
			// MulinsenAfterItem.OA_URL);
			return post(MulinsenAfterItem.OA_URL, jsonObject.toString(), headers);
		} catch (Exception e) {
			LOGGER.error("sendMessage error!", e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 邮件消息通知
	 * 
	 * @date 2018-5-9 ZXH
	 * @param mailToOA
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String sendMail(EmailMessageBean messageBean) throws UnsupportedEncodingException {

		MulinsenSendMailToOA mailToOA = new MulinsenSendMailToOA();
		mailToOA.setAppName(MulinsenAfterItem.APP_NAME);
		mailToOA.setContent("<HTML>" + messageBean.getContent() + "</HTML>");
		mailToOA.setSubject(messageBean.getSubject());
		mailToOA.setTo_employee_id(messageBean.getUser().getLoginName());

		Map<String, String> jsonMap = new LinkedHashMap<String, String>();

		String content = mailToOA.getContent();
		content = content.replace("<a ", "<a style=\"color:#00f;text-decoration:underline;\"  ");
		mailToOA.setContent(content);
		jsonMap.put("appName", mailToOA.getAppName());
		jsonMap.put("content", mailToOA.getContent());
		jsonMap.put("subject", mailToOA.getSubject());
		jsonMap.put("to_employee_id", mailToOA.getTo_employee_id());

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : jsonMap.entrySet()) {
			sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		sb.append("key=").append(MulinsenAfterItem.APP_KEY);

		LOGGER.info("builder = " + sb.toString());
		// 获取请求的md5 串
		String md5Code = null;
		md5Code = JecnUtil.MD5(sb.toString().getBytes("UTF-8"));
		// MD5串转换 大写
		String sign = md5Code.toString().toUpperCase();
		mailToOA.setSign(sign);

		JSONObject jsonObject = JSONObject.fromObject(mailToOA);
		LOGGER.info("JSON = " + jsonObject.toString());
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-type", "application/json;charset=utf-8");
		LOGGER.info("URL=" + MulinsenAfterItem.APP_URL);
		try {
			return post(MulinsenAfterItem.APP_URL, jsonObject.toString(), headers);
		} catch (Exception e) {
			LOGGER.error("sendMail error!", e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * post请求
	 * 
	 * @param url
	 *            请求地址
	 * @param requestBody
	 *            请求体
	 * @return 返回内容
	 * @throws ClientProtocolException
	 * @throws IOException
	 */

	private static CloseableHttpClient httpClient = null;
	static {
		// 获取一个socket的连接工厂，可以设置一些参数。
		ConnectionSocketFactory plainsf = PlainConnectionSocketFactory.getSocketFactory();
		LayeredConnectionSocketFactory sslsf = SSLConnectionSocketFactory.getSocketFactory();
		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create().register(
				"http", plainsf).register("https", sslsf).build();

		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);

		cm.setMaxTotal(1000); // 这个是最大连接数。如果并发量大于这个数，就会发生排队。
		cm.setDefaultMaxPerRoute(1000); // 设置每个路由的最大值。 默认只有一个路由。可以忽视这个设置。

		// 此处解释下MaxtTotal和DefaultMaxPerRoute的区别：
		// 1、MaxtTotal是整个池子的大小；
		// 2、DefaultMaxPerRoute是根据连接到的主机对MaxTotal的一个细分；比如：
		// MaxtTotal=400 DefaultMaxPerRoute=200
		// 而我只连接到http://sishuok.com时，到这个主机的并发最多只有200；而不是400；
		// 而我连接到http://sishuok.com 和
		// http://qq.com时，到每个主机的并发最多只有200；即加起来是400（但不能超过400）；所以起作用的设置是DefaultMaxPerRoute。

		// 根据上面的一堆配置，生成一个client对象。 这个对象是整个应用程序公用的。是唯一的对象。
		httpClient = HttpClients.custom().setConnectionManager(cm).build();
	}

	public static String post(String url, String requestBody, Map<String, String> header)
			throws ClientProtocolException, IOException {
		// 创建一个post请求对象
		HttpPost postRequest = new HttpPost(url);

		for (Entry<String, String> entry : header.entrySet()) {
			postRequest.addHeader(entry.getKey(), entry.getValue());
		}

		StringEntity input = new StringEntity(requestBody, Charset.forName("utf-8"));
		input.setContentType("application/json");
		postRequest.setEntity(input);
		CloseableHttpResponse response = httpClient.execute(postRequest);

		String str = null;
		HttpEntity entity = response.getEntity();
		if (entity != null) {
			InputStream instreams = entity.getContent();
			str = inputStream2String(instreams);
		}
		response.close();
		return str;
	}

	/**
	 * 从网络输入流中获取数据
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	private static String inputStream2String(InputStream in) throws IOException {
		StringBuffer out = new StringBuffer();
		byte[] b = new byte[4096];
		for (int n; (n = in.read(b)) != -1;) {
			out.append(new String(b, 0, n));
		}
		in.close();
		return out.toString();
	}

	public static void main(String[] args) {
		// 46690945C4FFA8EFF51E7D9F8973FDF3
		// 5E4CC93BA944BE300D1721DEDA45F61F
		String mes = "action=send_msg&content=代办测试1审批&hr_userid=145151&redirect_url=http://192.168.17.49:8080/loginMail.action?accessType=mailApprove&mailTaskId=1235&mailPeopleId=221262&isApp=false&isView=false&key=TqbydEiuY&2uHIqhT5#%V#4wfXO^c!Ss";
		// 获取请求的md5 串
		String md5Code = null;
		try {
			md5Code = JecnUtil.MD5(mes.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// MD5串转换 大写
		String sign = md5Code.toString().toUpperCase();
		System.out.println(sign);
	}
}
