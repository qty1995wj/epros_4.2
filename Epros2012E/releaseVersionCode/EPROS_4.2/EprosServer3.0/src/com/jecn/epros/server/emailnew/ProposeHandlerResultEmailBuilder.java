package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.propose.JecnRtnlPropose;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnUtil;

public class ProposeHandlerResultEmailBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

		Object[] data = getData();
		JecnUser handleUser = (JecnUser) data[0];
		JecnRtnlPropose rtnlPropose = (JecnRtnlPropose) data[1];
		int intflag = (Integer) data[2];

		return handleProposeSendEmail(handleUser, user, rtnlPropose, intflag);
	}

	/**
	 * 处理合理化建议之后给创建人发送邮件
	 * 
	 * @author hyl
	 * @param handleUser
	 *            合理化建议的处理人
	 * @param createUser
	 *            创建人
	 * @param intflag
	 *            操作 2采纳 3拒绝
	 * @param jecnRtnlPropose
	 *            合理化建议
	 */
	public EmailBasicInfo handleProposeSendEmail(JecnUser handleUser, JecnUser createUser, JecnRtnlPropose rtnlPropose,
			int intflag) {

		// 邮件标题
		String subject = JecnUtil.getValue("rationalizationproposals") + "-";
		if (intflag == 2) {
			subject += JecnUtil.getValue("acceptPropose");
		} else if (intflag == 3) {
			subject += JecnUtil.getValue("refusePropose");
		}

		StringBuffer strBuf = new StringBuffer();
		strBuf.append(JecnUtil.getValue("proposeBy"));
		strBuf.append("“");
		if (intflag == 2) {
			strBuf.append(JecnUtil.getValue("acceptPropose"));
		} else if (intflag == 3) {
			strBuf.append(JecnUtil.getValue("refusePropose"));
		}
		strBuf.append("”");

		strBuf.append("<br>");
		strBuf.append(JecnUtil.getValue("submitTimeC") + JecnCommon.getStringbyDate(rtnlPropose.getCreateTime()));
		strBuf.append("<br>");
		strBuf.append(JecnUtil.getValue("proposeSendPerson") + handleUser.getTrueName());
		strBuf.append("<br>");
		strBuf.append("合理化建议主题名称：" + rtnlPropose.getRelationName());
		strBuf.append("<br>");

		EmailContentBuilder builder = new EmailContentBuilder();
		builder.setContent(strBuf.toString());
		EmailBasicInfo baseInfo = new EmailBasicInfo();
		baseInfo.setContent(builder.buildContent());
		baseInfo.setSubject(subject);
		baseInfo.addRecipients(createUser);

		return baseInfo;

	}

	// /**
	// * 校验和封装邮件地址
	// *
	// * @author hyl
	// * @param user
	// * 收件人
	// * @return
	// */
	// private static SimpleUserEmailBean checkAndPackEmailAddress(JecnUser
	// user) {
	//
	// SimpleUserEmailBean simpleUserEmailBean = new SimpleUserEmailBean();
	// // 0:内外网；1：邮件地址;2:人员主键ID
	// if (user != null && !JecnCommon.isNullOrEmtryTrim(user.getEmail())
	// && !JecnCommon.isNullOrEmtryTrim(user.getEmailType())
	// && JecnCommon.checkMailFormat(user.getEmail())) {
	//
	// ToolEmail.setEmailInfo(user.getEmailType(), user.getEmail(),
	// simpleUserEmailBean);
	// return simpleUserEmailBean;
	//
	// } else {
	//
	// return null;
	// }
	//
	// }

}
