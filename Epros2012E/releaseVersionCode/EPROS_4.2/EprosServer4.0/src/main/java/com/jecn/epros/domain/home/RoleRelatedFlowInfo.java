package com.jecn.epros.domain.home;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.sqlprovider.server3.Common;

import java.util.List;

public class RoleRelatedFlowInfo {
    private Long roleId;
    private String roleName;
    /**
     * 角色职责
     */
    private String roleRes;
    private Long flowId;
    private String flowName;
    private Long postId;

    /**
     * 所属流程架构
     */
    private Long architectureId;
    private String architectureName;
    private int architectureType;


    /**
     * 角色对应活动集合
     */
    private List<LinkResource> activityLikes;
    /**
     * 流程相关文件（制度、标准、风险）
     */
    private List<LinkResource> relatedFiles;

    public LinkResource getProcessLink() {
        return new LinkResource(flowId, flowName, ResourceType.PROCESS);
    }

    public LinkResource getArchitectureLink() {
        return new LinkResource(architectureId, architectureName, architectureType == 1 ? ResourceType.PROCESS : ResourceType.PROCESS_MAP);
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @JsonIgnore
    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    @JsonIgnore
    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public List<LinkResource> getActivityLikes() {
        return activityLikes;
    }

    public void setActivityLikes(List<LinkResource> activityLikes) {
        this.activityLikes = activityLikes;
    }

    public List<LinkResource> getRelatedFiles() {
        return relatedFiles;
    }

    public void setRelatedFiles(List<LinkResource> relatedFiles) {
        this.relatedFiles = relatedFiles;
    }

    @JsonIgnore
    public Long getArchitectureId() {
        return architectureId;
    }

    public void setArchitectureId(Long architectureId) {
        this.architectureId = architectureId;
    }

    @JsonIgnore
    public String getArchitectureName() {
        return architectureName;
    }

    public void setArchitectureName(String architectureName) {
        this.architectureName = architectureName;
    }

    @JsonIgnore
    public int getArchitectureType() {
        return architectureType;
    }

    public void setArchitectureType(int architectureType) {
        this.architectureType = architectureType;
    }

    public String getRoleRes() {
        return Common.replaceWarp(roleRes);
    }

    public void setRoleRes(String roleRes) {
        this.roleRes = roleRes;
    }
}
