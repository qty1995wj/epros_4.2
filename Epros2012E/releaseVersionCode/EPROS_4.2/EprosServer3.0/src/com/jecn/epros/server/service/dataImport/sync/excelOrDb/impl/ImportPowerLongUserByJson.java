package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.action.web.login.ad.powerlong.JecnPowerLongAfterItem;
import com.jecn.epros.server.common.HttpRequest;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.PosBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 宝龙 - 人员同步基于Json同步数据
 * 
 * @author ZXH
 * 
 */
public class ImportPowerLongUserByJson extends AbstractImportUser {
	protected final Log log = LogFactory.getLog(this.getClass());

	public ImportPowerLongUserByJson(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 获取部门数据集合
	 */
	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		// 执行读取Json数据
		String result = HttpRequest.sendGet(JecnPowerLongAfterItem.getValue("SYNC_ORG"), null);
		return getDpetList(result);
	}

	/**
	 * 获取人员数据集合
	 */
	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		// 执行读取Json数据
		String result = HttpRequest.sendGet(JecnPowerLongAfterItem.getValue("SYNC_USER"), null);
		return getUserList(result);
	}

	private List<String> noSyncDeptNum = new ArrayList<String>();

	/**
	 * 把数据从json取出存储到bean对象中
	 * 
	 * @param result
	 */
	public List<DeptBean> getDpetList(String result) throws Exception {
		/** 部门 */
		List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
		DeptBean deptBean = null;
		String returnObj = JSONObject.fromObject(result).getString("data");
		log.info("获取部门数据成功！");
		JSONArray jsonArray = JSONArray.fromObject(returnObj);

		Object parentId = null;
		Object id = null;
		Object name = null;
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			parentId = jsonObject.get("parentId");
			name = jsonObject.get("name");
			id = jsonObject.get("id");
			if (parentId == null) {
				log.info(jsonObject);
				noSyncDeptNum.add(id.toString());
				continue;
			}
			if (id == null || name == null || parentId == null || "0".equals(id)) {
				log.error("error data : " + jsonObject);
				continue;
			}
			deptBean = new DeptBean();
			deptBean.setDeptNum(id.toString());
			deptBean.setDeptName(name.toString());
			deptBean.setPerDeptNum(parentId.toString());
			deptBeanList.add(deptBean);
		}
		return deptBeanList;
	}

	/**
	 * 把数据从json取出存储到bean对象中
	 * 
	 * @param result
	 */
	public List<UserBean> getUserList(String result) throws Exception {
		/** 人员 */
		List<UserBean> userBeanList = new ArrayList<UserBean>();

		String returnObj = JSONObject.fromObject(result).getString("data");
		JSONArray jsonArray = JSONArray.fromObject(returnObj);
		log.info("获取人员岗位数据成功！");
		// 获取岗位和部门的对应关系
		Map<String, PosBean> postMap = getPositionMap();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			Object username = jsonObject.get("username");
			Object realname = jsonObject.get("realname");
			Object email = jsonObject.get("email");
			Object statusId = jsonObject.get("statusId");
			if (username == null || realname == null || statusId == null) {
				continue;
			}
			if (!"1".equals(statusId.toString()) || "admin".equals(username.toString())) {// 用户状态：0：待启;1:正常;2:冻结;4:销户
				continue;
			}
			addUserPost(postMap, userBeanList, jsonObject, username.toString(), realname.toString(), email.toString());
		}
		return userBeanList;
	}

	private void addUserPost(Map<String, PosBean> postMap, List<UserBean> userBeanList, JSONObject jsonObject,
			String username, String realname, String email) {
		Object posList = jsonObject.get("posList");
		if (posList == null) {
			userBeanList.add(getUserBean(username, realname, email));
			return;
		}
		JSONArray jsonArray = JSONArray.fromObject(posList.toString());
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject postObject = (JSONObject) jsonArray.opt(i);
			Object object = postObject.get("sysPosId");
			if (object == null) {
				continue;
			}
			PosBean posBean = postMap.get(object.toString());
			if (posBean == null || StringUtils.isBlank(posBean.getPosName())
					|| StringUtils.isBlank(posBean.getDeptNum())) {
				continue;
			}
			if (noSyncDeptNum.contains(posBean.getDeptNum())) {
				continue;
			}
			UserBean userBean = getUserBean(username, realname, email);

			userBean.setDeptNum(posBean.getDeptNum());
			userBean.setPosName(posBean.getPosName());
			userBean.setPosNum(posBean.getPosNum());
			userBeanList.add(userBean);
		}
	}

	private UserBean getUserBean(String username, String realname, String email) {
		UserBean userBean = new UserBean();
		userBean.setUserNum(username);
		userBean.setTrueName(realname);
		userBean.setEmail(email);
		userBean.setEmailType(0);
		return userBean;
	}

	private Map<String, PosBean> getPositionMap() {
		String result = HttpRequest.sendGet(JecnPowerLongAfterItem.getValue("SYNC_POST"), null);
		String returnObj = JSONObject.fromObject(result).getString("data");

		JSONArray jsonArray = JSONArray.fromObject(returnObj);
		PosBean posBean = null;
		Map<String, PosBean> postionMap = new HashMap<String, PosBean>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			Object id = jsonObject.get("id");
			Object posNum = jsonObject.get("positionCd");
			Object posName = jsonObject.get("name");
			Object deptNum = jsonObject.get("orgId");
			if (id == null || posName == null || deptNum == null || posNum == null) {
				continue;
			}
			posBean = new PosBean();
			posBean.setPosNum(posNum.toString());
			posBean.setPosName(posName.toString());
			posBean.setDeptNum(deptNum.toString());
			postionMap.put(id.toString(), posBean);
		}
		return postionMap;
	}
}
