package epros.designer.gui.system;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;

public class FileEnclosureSelectCommon {
	private static Logger log = Logger.getLogger(FileEnclosureSelectCommon.class);
	/** 附件Lab */
	private JLabel mapAttchMentLab = new JLabel(JecnProperties.getValue("mapFile"));

	/** 附件Area */
	private JecnTextField attchMentFiled = new JecnTextField();

	/** 附件的ID */
	private Long fileId = null;

	/** 附件选择按钮 */
	private JButton attchMentBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 附件 */
	private List<JecnTreeBean> listFile = new ArrayList<JecnTreeBean>();

	private JecnDialog jecnDialog;

	public FileEnclosureSelectCommon(int rows, JecnPanel infoPanel, Insets insets, Long fileId, JecnDialog jecnDialog,
			boolean isRequest) {
		this.jecnDialog = jecnDialog;
		this.fileId = fileId;
		attchMentFiled.setEditable(false);
		attchMentFiled.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(mapAttchMentLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(attchMentFiled, c);
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		infoPanel.add(attchMentBut, c);

		if (isRequest) {
			c = new GridBagConstraints(4, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}
		// 附件
		if (fileId != null) {
			JecnTreeBean fileTreeBean = null;
			try {
				fileTreeBean = ConnectionPool.getFileAction().getJecnTreeBeanByIds(fileId, JecnConstants.projectId);
			} catch (Exception e) {
				JecnOptionPane.showMessageDialog(jecnDialog, JecnProperties.getValue("obtainFileObjError"));
				log.error("FileEnclosureSelectCommon is error", e);
			}
			if (fileTreeBean != null) {
				listFile.add(fileTreeBean);
				attchMentFiled.setText(fileTreeBean.getName());
			}
		}
		attchMentBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				attchMentButPerformed();
			}

		});
	}

	/**
	 * 是否更新
	 * 
	 * @return
	 */
	public boolean isUpdate() {
		if (fileId == null) {
			if (listFile.size() > 0) {
				return true;
			}
		} else {
			if (listFile.size() == 0) {
				return true;
			} else {
				if (!fileId.equals(listFile.get(0).getId())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 获得附件
	 * 
	 * @return
	 */
	public Long getResultFileId() {
		if (listFile.size() > 0) {
			return listFile.get(0).getId();
		}
		return null;
	}

	/**
	 * 附件选择
	 */
	private void attchMentButPerformed() {
		FileChooseDialog fileChooseDialog = new FileChooseDialog(listFile, jecnDialog);
		// 只能选择一个文件
		fileChooseDialog.setSelectMutil(false);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			if (listFile.size() > 0) {
				attchMentFiled.setText(listFile.get(0).getName());
			} else {
				attchMentFiled.setText("");
			}
		}
	}

}
