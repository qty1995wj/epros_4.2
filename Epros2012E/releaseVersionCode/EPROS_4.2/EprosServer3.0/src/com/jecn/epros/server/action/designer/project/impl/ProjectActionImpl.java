package com.jecn.epros.server.action.designer.project.impl;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.action.designer.project.IProjectAction;
import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.service.project.IProjectService;

/***
 * 项目管理Action
 * @author 2012-05-23
 *
 */
public class ProjectActionImpl implements IProjectAction {

	private IProjectService projectService;
	
	public IProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(IProjectService projectService) {
		this.projectService = projectService;
	}

	@Override
	public JecnProject addProject(JecnProject jecnProject) throws Exception {
		 projectService.save(jecnProject);
		 return jecnProject;
	}

	@Override
	public void deleteProject(List<Long> listIds) throws Exception {
		projectService.deleteProject(listIds);
	}

	@Override
	public List<JecnProject> getListProject() throws Exception {
		return projectService.getListProject();
	}

	@Override
	public void operProject(long proId) throws Exception {
		
	}

	@Override
	public void reProjectName(String newName, long id) throws Exception {
		projectService.reProjectName(newName, id);
	}
	
	@Override
	/***
	 * 根据项目ID获取项目信息
	 * @return
	 */
	public JecnProject getProjectById(long proId) throws Exception{
		return projectService.get(proId);
	}

	@Override
	public List<Object[]> getDelsProject() throws Exception {
		return projectService.getDelsProject();
	}

	@Override
	public void updateRecoverDelProject(List<Long> ids) throws Exception {
		projectService.updateRecoverDelProject(ids);
		
	}

	@Override
	public void deleteIdsProject(Set<Long> setIds) throws Exception {
		projectService.deleteIdsProject(setIds);
		
	}

	@Override
	public boolean validateAddName(String name) throws Exception {
		return projectService.validateAddName(name);
	}

	@Override
	public boolean validateUpdateName(String name, long id) throws Exception {
		return projectService.validateUpdateName(name, id);
	}

}
