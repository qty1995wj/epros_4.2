package com.jecn.epros.server.dao.task.design;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.common.IBaseDao;

/**
 * 任务审批信息验证处理类数据层接口
 * 
 * @author ZHANXIAOHU
 * @date： 日期：Oct 22, 2012 时间：3:24:11 PM
 */
public interface IJecnTaskRecordDao extends IBaseDao<JecnTaskBeanNew, Long> {
	/**
	 * 获得人员名称和人员主键记录的集合信息
	 * 
	 * @param set
	 *            人员主键集合
	 * @param projectId
	 *            当前项目ID
	 * @return 人员名称和人员主键记录的集合信息
	 * @throws Exception
	 */
	public List<Object[]> getJecnUserByUserIds(Set<Long> set, Long protectId)
			throws Exception;

	/**
	 * 判断userIds人员个数
	 * 
	 * @param userIds
	 *            人员岗位关联表主键ID集合（集合大于100 不能用此方法）
	 * @return
	 * @throws DaoException
	 */
	public int getPeopleIdCountByUserIds(Set<Long> userIds) throws Exception;

	/**
	 * 验证版本号是否存在
	 * 
	 * @param id
	 *            流程或制度ID
	 * @param version
	 *            版本号
	 * @param type
	 *            类型 0 流程，1文件，2制度
	 * @return List<Long> 版本号存在 集合大于0
	 * @throws DaoException
	 */
	public List<Long> isExistVersionbyFlowId(Long id, String version, int type)
			throws Exception;

	/**
	 * 是否处于任务中
	 * 
	 * @param id
	 * @param type
	 *            0为流程任务，1为文件任务，2为制度任务
	 * @return int >0PRF处于任务中
	 * @throws Exception
	 */
	public int isBeInTask(Long rid, int type) throws Exception;
	
	
}
