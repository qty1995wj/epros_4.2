package wordxml.element.bean.common;

import java.awt.Color;

import wordxml.constant.Constants.LineType;

/**
 * 线属性
 */
public class LineBean {
	/*** 下划线类型 默认实线 single ***/
	// single、thick、double、dotted、dashed、dot-dash
	private LineType type = LineType.SINGLE;// single
	/*** 下划线颜色 默认黑色 000000 ***/
	private Color color = Color.black; // "000000"

	public LineBean() {

	}

	public LineBean(LineType type, Color color) {
		this.type = type;
		this.color = color;
	}

	public LineType getType() {
		return type;
	}

	public void setType(LineType type) {
		this.type = type;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

}
