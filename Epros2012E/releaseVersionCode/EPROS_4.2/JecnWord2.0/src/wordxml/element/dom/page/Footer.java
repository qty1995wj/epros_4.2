package wordxml.element.dom.page;

import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.element.dom.IBody;

/**
 * 页眉页脚
 * 
 * @author admin
 * 
 */
public class Footer extends IBody {
	/** 所属节 */
	private Sect parent;
	/** 类型 */
	private HeaderOrFooterType type;

	public Footer(Sect sect, HeaderOrFooterType type) {
		this.parent = sect;
		this.type = type;
	}

	public Sect getParent() {
		return parent;
	}

	public void setParent(Sect parent) {
		this.parent = parent;
	}

	public HeaderOrFooterType getType() {
		return type;
	}

	public void setType(HeaderOrFooterType type) {
		if (type == null) {
			return;
		}
		if (type.equals(HeaderOrFooterType.first)) {
			parent.getSectBean().setTitlePg(true);
		}
		this.type = type;
	}

}
