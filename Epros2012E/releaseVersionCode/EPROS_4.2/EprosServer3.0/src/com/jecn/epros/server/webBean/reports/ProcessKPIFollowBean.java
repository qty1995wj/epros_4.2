package com.jecn.epros.server.webBean.reports;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

/**
 * 
 * KPI跟踪表
 * 
 * 流程kpi的总bean
 * 
 * @author huoyl
 * 
 */
public class ProcessKPIFollowBean {


	/** kpi跟踪表中所有的部门的集合 */
	private List<ProcessKPIWebListBean> kPIWebListBean = new ArrayList<ProcessKPIWebListBean>();
	/** 流程拟制人是否显示*/
	private boolean artificialPersonIsShow=false;
	/** 流程监护人是否显示*/
	private boolean guardianPeopleIsShow=false;
	/** 指标来源是否显示*/
	private boolean kpiPropertyIsShow=false;
	/** 显示的配置项**/
	private List<JecnConfigItemBean> configList=new ArrayList<JecnConfigItemBean>();

	public List<JecnConfigItemBean> getConfigList() {
		return configList;
	}

	public void setConfigList(List<JecnConfigItemBean> configList) {
		this.configList = configList;
	}

	public boolean isKpiPropertyIsShow() {
		return kpiPropertyIsShow;
	}

	public void setKpiPropertyIsShow(boolean kpiPropertyIsShow) {
		this.kpiPropertyIsShow = kpiPropertyIsShow;
	}

	public boolean isArtificialPersonIsShow() {
		return artificialPersonIsShow;
	}

	public void setArtificialPersonIsShow(boolean artificialPersonIsShow) {
		this.artificialPersonIsShow = artificialPersonIsShow;
	}

	public boolean isGuardianPeopleIsShow() {
		return guardianPeopleIsShow;
	}

	public void setGuardianPeopleIsShow(boolean guardianPeopleIsShow) {
		this.guardianPeopleIsShow = guardianPeopleIsShow;
	}


	public List<ProcessKPIWebListBean> getkPIWebListBean() {
		return kPIWebListBean;
	}

	public void setkPIWebListBean(List<ProcessKPIWebListBean> kPIWebListBean) {
		this.kPIWebListBean = kPIWebListBean;
	}

}
