package com.jecn.epros.server.dao.process.impl;

import java.io.InputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.lob.SerializableBlob;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT;
import com.jecn.epros.server.bean.integration.JecnControlPointT;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.FlowCriterionT;
import com.jecn.epros.server.bean.process.JecnActivityFileT;
import com.jecn.epros.server.bean.process.JecnFigureFileTBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfo;
import com.jecn.epros.server.bean.process.JecnFlowDriver;
import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.process.JecnModeFileT;
import com.jecn.epros.server.bean.process.JecnRefIndicatorsT;
import com.jecn.epros.server.bean.process.JecnRoleActiveT;
import com.jecn.epros.server.bean.process.JecnTempletT;
import com.jecn.epros.server.bean.process.JecnToFromActiveT;
import com.jecn.epros.server.bean.process.ProcessInterface;
import com.jecn.epros.server.bean.process.StandardFlowRelationTBean;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutBase;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutData;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.JecnSqlConstant;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.service.process.buss.FullPathManager;
import com.jecn.epros.server.service.process.buss.ProcessSqlConstant;
import com.jecn.epros.server.service.process.util.MyFlowSqlUtil;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnCommonSqlConstant.TYPEAUTH;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.process.ProcessBaseInfoBean;

public class FlowStructureDaoImpl extends AbsBaseDao<JecnFlowStructureT, Long> implements IFlowStructureDao {

	@Override
	public List<Object[]> findAllActiveInputAndOperationByFlowId(Long flowId, boolean isPub) throws Exception {
		if (!JecnConfigTool.useNewInout()) {
			String sql = "";
			if (isPub) {
				sql = "select t.file_id,t.figure_id,t.file_s_id,"
						+ "        jf.file_name,t.file_type,jf.doc_id,JFO.ORG_NAME,jsi.activity_input"
						+ "       from jecn_activity_file t,jecn_flow_structure_image jsi,jecn_file jf"
						+ "		  LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID = JF.ORG_ID"
						+ "        where t.figure_id=jsi.figure_id and t.file_s_id=jf.file_id and jf.del_state=0 and jsi.flow_id=?";
			} else {
				sql = "select t.file_id,t.figure_id,t.file_s_id,"
						+ "        jf.file_name,t.file_type,jf.doc_id,JFO.ORG_NAME,jsi.activity_input"
						+ "       from jecn_activity_file_t t,jecn_flow_structure_image_t jsi,jecn_file_t jf"
						+ "		  LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID = JF.ORG_ID"
						+ "        where t.figure_id=jsi.figure_id and t.file_s_id=jf.file_id and jf.del_state=0 and jsi.flow_id=?";
			}
			return this.listNativeSql(sql, flowId);
		} else {
			return findAllTermActiveInputAndOperationByFlowId(flowId, isPub);
		}
	}

	// 新版输入输出
	public List<Object[]> findAllTermActiveInputAndOperationByFlowId(Long flowId, boolean isPub) throws Exception {
		String idStr = JecnCommon.isOracle() ? "to_char(t.file_id)" : "cast(t.file_id as varchar)";
		String puStr = isPub ? "" : "_T";
		String sql = "select " + idStr + "," + "       t.figure_id," + "       t.file_s_id," + "       jf.file_name,"
				+ "       t.file_type," + "       jf.doc_id," + "       JFO.ORG_NAME," + "       jsi.activity_input"
				+ "  from jecn_activity_file" + puStr + "        t," + "       jecn_flow_structure_image" + puStr
				+ " jsi," + "       jecn_file" + puStr + "                 jf" + "  LEFT JOIN JECN_FLOW_ORG JFO"
				+ "    ON JFO.ORG_ID = JF.ORG_ID" + " where t.figure_id = jsi.figure_id"
				+ "   and t.file_s_id = jf.file_id" + "   and jf.del_state = 0" + "   and t.file_type = 1"
				+ "   and jsi.flow_id = ?" + " UNION " + "	select inout.id," + "       inout.figure_id,"
				+ "       smap.file_id file_s_id," + "       ft.file_name," + "       ft.file_type,"
				+ "       ft.doc_id," + "       jfo.org_name," + "       inout.explain" + "  from JECN_FIGURE_IN_OUT"
				+ puStr + " inout" + " inner join jecn_figure_in_out_sample" + puStr + " smap"
				+ "    on smap.in_out_id = inout.id" + " inner join jecn_file" + puStr + " ft"
				+ "    on ft.file_id = smap.file_id" + "   and ft.del_state = 0" + "  LEFT JOIN JECN_FLOW_ORG JFO"
				+ "    ON JFO.ORG_ID = ft.ORG_ID" + " where inout.flow_id = ?" + "   and smap.type = 0"
				+ "   and inout.type = 0";
		return this.listNativeSql(sql, flowId, flowId);
	}

	/**
	 * 
	 * 农夫山泉操作说明下载使用
	 * 
	 * @description：流程下所有的活动的输入和操作规范 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称
	 *                               4是文件类型（0是输入，1是操作手册） 5是文件编号
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> findAllActiveInputAndOperationByFlowIdNFSQ(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String sql = "select t.file_id,t.figure_id,t.file_s_id,jf.file_name,t.file_type,jf.doc_id ,JFO.ORG_NAME"
					+ "       from jecn_activity_file t,jecn_flow_structure_image jsi,jecn_file jf"
					+ " LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID=JF.ORG_ID"
					+ "        where t.figure_id=jsi.figure_id and t.file_s_id=jf.file_id and jf.del_state=0 and jsi.flow_id=?";
			return this.listNativeSql(sql, flowId);
		} else {
			String sql = "select t.file_id,t.figure_id,t.file_s_id,jf.file_name,t.file_type,jf.doc_id ,JFO.ORG_NAME"
					+ "       from jecn_activity_file_t t,jecn_flow_structure_image_t jsi,jecn_file_t jf"
					+ " LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID=JF.ORG_ID"
					+ "        where t.figure_id=jsi.figure_id and t.file_s_id=jf.file_id and jf.del_state=0 and jsi.flow_id=?";
			return this.listNativeSql(sql, flowId);

		}
	}

	@Override
	public List<Object[]> findAllActiveOutputByFlowId(Long flowId, boolean isPub) throws Exception {
		if (!JecnConfigTool.useNewInout()) {
			String sql = "";
			if (isPub) {
				sql = "select t.mode_file_id,t.figure_id,t.file_m_id,jf.file_name,jf.doc_id,jsi.activity_output"
						+ "       from jecn_mode_file t,jecn_flow_structure_image jsi,jecn_file jf"
						+ "       where t.figure_id=jsi.figure_id and t.file_m_id = jf.file_id and jf.del_state=0 and jsi.flow_id=?";
			} else {
				sql = "select t.mode_file_id,t.figure_id,t.file_m_id,jf.file_name,jf.doc_id,jsi.activity_output"
						+ "       from jecn_mode_file_t t,jecn_flow_structure_image_t jsi,jecn_file_t jf"
						+ "       where t.figure_id=jsi.figure_id and t.file_m_id = jf.file_id and jf.del_state=0 and jsi.flow_id=?";
			}
			return this.listNativeSql(sql, flowId);
		} else {
			return findAllTermActiveOutputByFlowId(flowId, isPub);
		}
	}

	// smap.type 0模板 1 样例
	public List<Object[]> findAllTermActiveOutputByFlowId(Long flowId, boolean isPub) throws Exception {
		String puStr = isPub ? "" : "_T";
		String sql = "	select inout.id," + "       inout.figure_id," + "       smap.file_id file_s_id,"
				+ "       ft.file_name," + "       ft.file_type," + "       ft.doc_id," + "       jfo.org_name,"
				+ "       inout.explain" + "  from JECN_FIGURE_IN_OUT" + puStr + " inout"
				+ " inner join jecn_figure_in_out_sample" + puStr + " smap" + "    on smap.in_out_id = inout.id"
				+ " inner join jecn_file" + puStr + " ft" + "    on ft.file_id = smap.file_id"
				+ "   and ft.del_state = 0" + "  LEFT JOIN JECN_FLOW_ORG JFO" + "    ON JFO.ORG_ID = ft.ORG_ID"
				+ " where inout.flow_id = ?" + "   and smap.type = 0" + "   and inout.type = 1";
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public List<JecnModeFileT> findJecnModeFileTByFlowId(Long flowId) throws Exception {
		String hql = "select a from JecnModeFileT as a,JecnFlowStructureImageT as b where a.figureId=b.figureId and b.flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
	public List<Object[]> findAllActiveTempletByFlowId(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String sql = "select t.id,t.mode_file_id,t.file_id,jf.file_name,jf.doc_id"
					+ "        from jecn_templet t,jecn_mode_file jm,jecn_flow_structure_image jsi,jecn_file jf"
					+ "        where t.mode_file_id=jm.mode_file_id and jm.figure_id=jsi.figure_id and t.file_id=jf.file_id and jf.del_state=0 and jsi.flow_id=?";
			return this.listNativeSql(sql, flowId);
		} else {
			String sql = "select t.id,t.mode_file_id,t.file_id,jf.file_name,jf.doc_id"
					+ "        from jecn_templet_t t,jecn_mode_file_t jm,jecn_flow_structure_image_t jsi,jecn_file_t jf"
					+ "        where t.mode_file_id=jm.mode_file_id and jm.figure_id=jsi.figure_id and t.file_id=jf.file_id and jf.del_state=0 and jsi.flow_id=?";
			return this.listNativeSql(sql, flowId);
		}
	}

	@Override
	public List<JecnRefIndicatorsT> findAllJecnRefIndicatorsTsByFlowIdT(Long flowId) throws Exception {
		String hql = "select a from JecnRefIndicatorsT as a,JecnFlowStructureImageT as b where a.activityId = b.figureId and b.flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
	public List<Object[]> findAllRoleAndActiveRelateByFlowId(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String hql = "select a.figureRoleId,a.figureActiveId from JecnRoleActive as a,JecnFlowStructureImage as b where a.figureRoleId = b.figureId and b.flowId=?";
			return this.listHql(hql, flowId);
		} else {
			String hql = "select a.figureRoleId,a.figureActiveId from JecnRoleActiveT as a,JecnFlowStructureImageT as b where a.figureRoleId = b.figureId and b.flowId=?";
			return this.listHql(hql, flowId);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JecnRoleActiveT> findAllJecnRoleActiveTByFlowIdT(Long flowId) throws Exception {
		String sql = "select jrat.*" + "        from JECN_ROLE_ACTIVE_T jrat"
				+ "        left join JECN_FLOW_STRUCTURE_IMAGE_T jfsit on jrat.FIGURE_ROLE_ID ="
				+ "                                                       jfsit.figure_id"
				+ "       where jfsit.flow_id =" + flowId;
		return this.getSession().createSQLQuery(sql).addEntity(JecnRoleActiveT.class).list();
	}

	@Override
	public List<Object[]> findAllRolePosByFlowIdT(Long flowId) throws Exception {
		String sql = "select t.flow_station_id,t.figure_flow_id,t.figure_position_id"
				+ "        ,case when t.type='0' then pos.figure_text"
				+ "        when t.type='1' then jpg.name"
				+ "        end as name ,t.type,T.FIGURE_UUID,T.UUID"
				+ "        from jecn_flow_structure_image_t jfsit,process_station_related_t t"
				+ "        left join (select jfoi.figure_id,jfoi.figure_text from jecn_flow_org_image jfoi"
				+ "        ,jecn_flow_org jfo where jfoi.org_id = jfo.org_id and jfo.del_state=0) pos on pos.figure_id=t.figure_position_id"
				+ "        left join jecn_position_group jpg on jpg.id=t.figure_position_id"
				+ "        where t.figure_flow_id = jfsit.figure_id and jfsit.flow_id=?";
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public void deleteFlows(Set<Long> setFlowIds) throws Exception {
		// 流程相关部门-设计器
		String hql = "delete from FlowOrgT where flowId in";
		List<String> list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程相关部门-浏览端
		hql = "delete from FlowOrg where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程相关制度-设计器
		hql = "delete from FlowCriterionT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程相关制度-浏览端
		hql = "delete from FlowCriterion where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程相关标准-设计器
		hql = "delete from StandardFlowRelationTBean where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程相关标准-浏览端
		hql = "delete from StandardFlowRelationBean where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程相关支持工具-设计器
		hql = "delete from JecnSustainToolRelatedT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程相关支持工具-浏览端
		hql = "delete from JecnSustainToolRelated where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 流程基本信息-设计器
		hql = "delete from JecnFlowBasicInfoT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程基本信息-浏览端
		hql = "delete from JecnFlowBasicInfo where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 流程相关启动规则-设计器
		hql = "delete from JecnFlowDriverT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 流程相关启动规则-浏览端
		hql = "delete from JecnFlowDriver where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 流程保持记录-设计器
		hql = "delete from JecnFlowRecordT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 流程保持记录-浏览端
		hql = "delete from JecnFlowRecord where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 标准相关流程
		hql = "delete from StandardBean where stanType =2 and relationId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// KPI的值
		hql = "delete from JecnFlowKpi where kpiAndId in (select kpiAndId from JecnFlowKpiNameT where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// kpi属性 IT系统与支持工具关联关系临时表
		hql = " delete  from JECN_SUSTAIN_TOOL_CONN_T  " + " where  related_type = 0" + "   and  related_id in"
				+ "       (select jfknt.KPI_AND_ID" + "          from jecn_flow_kpi_name_t jfknt"
				+ "         where jfknt.flow_Id in ";

		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteNative(str);
		}
		// kpi属性 IT系统与支持工具关联关系正式表
		hql = " delete  from JECN_SUSTAIN_TOOL_CONN " + " where  related_type = 0" + "   and  related_id in"
				+ "       (select jfkn.KPI_AND_ID" + "          from jecn_flow_kpi_name jfkn"
				+ "         where jfkn.flow_Id in  ";

		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteNative(str);
		}

		// KPI-设计器
		hql = "delete from JecnFlowKpiNameT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// KPI-浏览端
		hql = "delete from JecnFlowKpiName where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 删除关联图形的ID--浏览端
		hql = "update JecnFlowStructureImage set linkFlowId = -1  where figureType <> " + JecnCommonSql.getIconString()
				+ " and  figureType <> " + JecnCommonSql.getSystemString() + " and linkFlowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除关联图形的ID--设计器
		hql = "update JecnFlowStructureImageT set linkFlowId = -1  where figureType <> "
				+ JecnCommonSql.getIconString() + " and  figureType <> " + JecnCommonSql.getSystemString()
				+ " and linkFlowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中角色与岗位相关联-设计器
		hql = "delete from JecnFlowStationT where figureFlowId in (select figureId from JecnFlowStructureImageT where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 流程中角色与岗位相关联-浏览端
		hql = "delete from JecnFlowStation where figureFlowId in (select figureId from JecnFlowStructureImage where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 流程中活动样例-设计器
		hql = "delete from JecnTempletT where modeFileId in (select a.modeFileId from JecnModeFileT as a,"
				+ "JecnFlowStructureImageT as b where a.figureId = b.figureId and b.flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中活动样例-浏览端
		hql = "delete from JecnTemplet where modeFileId in (select a.modeFileId from JecnModeFile as a,"
				+ "JecnFlowStructureImage as b where a.figureId = b.figureId and b.flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中活动输出-设计器
		hql = "delete from JecnModeFileT where figureId in (select figureId from JecnFlowStructureImageT where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中活动输出-浏览端
		hql = "delete from JecnModeFile where figureId in (select figureId from JecnFlowStructureImage where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中活动输入和操作规范-设计器
		hql = "delete from JecnActivityFileT where figureId in (select figureId from JecnFlowStructureImageT where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中活动输入和操作规范-浏览端
		hql = "delete from JecnActivityFile where figureId in (select figureId from JecnFlowStructureImage where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 流程中活动的指标-设计器
		hql = "delete from JecnRefIndicatorsT where activityId in (select figureId from JecnFlowStructureImageT where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中活动的指标-浏览器
		hql = "delete from JecnRefIndicators where activityId in (select figureId from JecnFlowStructureImage where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中角色与活动的关系-设计器
		hql = "delete from JecnRoleActiveT where figureRoleId in (select figureId from JecnFlowStructureImageT where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		hql = "delete from JecnRoleActiveT where figureActiveId in (select figureId from JecnFlowStructureImageT where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中角色与活动的关系-浏览端
		hql = "delete from JecnRoleActive where figureRoleId in (select figureId from JecnFlowStructureImage where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		hql = "delete from JecnRoleActive where figureActiveId in (select figureId from JecnFlowStructureImage where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 删除元素和连接线
		deleteFigureAndLine(setFlowIds);
		// 查阅权限
		JecnUtil.deleteViewAuth(setFlowIds, 0, this);
		// 设计器权限
		JecnUtil.deleteDesignAuth(setFlowIds, 0, this);
		// 发布流程的记录
		JecnUtil.deleteRecord(setFlowIds, 0, this);
		// 任务
		JecnUtil.deleteTask(setFlowIds, 0, this);

		// 文件-所属流程
		hql = "update JecnFileBean set flowId=-1 where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除流程图图片的本地文件
		hql = "select filePath from JecnFlowStructureT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			List<String> listfilePath = JecnCommonSql.getSetSqls(str, setFlowIds);
			for (String filePath : listfilePath) {
				if (filePath != null && !"".equals(filePath)) {
					JecnFinal.deleteFile(filePath);
				}
			}
		}
		// 删除标准
		hql = "delete from StandardBean where stanType=2 and relationId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程对象-设计器
		hql = "delete from JecnFlowStructureT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程对象-浏览端
		hql = "delete from JecnFlowStructure where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		hql = "delete from FlowStandardizedFileT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		hql = "delete from JecnFlowRelatedRiskT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		hql = "delete from FlowStandardizedFile where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		hql = "delete from JecnFlowRelatedRisk where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		hql = "delete from FlowFileContent where relatedId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除收藏
		String sql = "delete from jecn_my_star where type=0 and related_id in";
		list = JecnCommonSql.getSetSqls(sql, setFlowIds);
		for (String str : list) {
			execteNative(str);
		}

		// 删除合理化建议 0流程，1制度，2流程架构，3文件，4风险，5标准
		JecnDaoUtil.deleteProposeByRelatedIds(setFlowIds, 0, this);

	}

	/**
	 * 删除元素和连接线
	 * 
	 * @param setFlowIds
	 */
	private void deleteFigureAndLine(Set<Long> setFlowIds) {
		// 流程中图形之间的线段-设计器
		String hql = "delete from JecnLineSegmentT where figureId in (select figureId from JecnFlowStructureImageT where flowId in";
		List<String> list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程中图形之间的线段-浏览端
		hql = "delete from JecnLineSegment where figureId in (select figureId from JecnFlowStructureImage where flowId in";
		list = JecnCommonSql.getSetSqlAddBracket(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程图标-设计器
		hql = "delete from JecnFlowStructureImageT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程图标-浏览端
		hql = "delete from JecnFlowStructureImage where flowId in";
		list = JecnCommonSql.getSetSqls(hql, setFlowIds);
		for (String str : list) {
			execteHql(str);
		}
	}

	@Override
	public void deleteMainFlows(Set<Long> listMainFlowIds) throws Exception {
		// 流程地图 基本信息
		String hql = "delete from JecnMainFlow where flowId in";
		List<String> list = JecnCommonSql.getSetSqls(hql, listMainFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		hql = "delete from JecnMainFlowT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, listMainFlowIds);
		for (String str : list) {
			execteHql(str);
		}

		// 删除关联图形的ID--浏览端
		hql = "update JecnFlowStructureImage set linkFlowId = -1  where figureType <> " + JecnCommonSql.getIconString()
				+ " and  figureType <> " + JecnCommonSql.getSystemString() + " and linkFlowId in";
		list = JecnCommonSql.getSetSqls(hql, listMainFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除关联图形的ID--设计器
		hql = "update JecnFlowStructureImageT set linkFlowId = -1  where figureType <> "
				+ JecnCommonSql.getIconString() + " and  figureType <> " + JecnCommonSql.getSystemString()
				+ " and linkFlowId in";
		list = JecnCommonSql.getSetSqls(hql, listMainFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除元素和连接线
		deleteFigureAndLine(listMainFlowIds);

		// 删除流程图图片的本地文件
		hql = "select filePath from JecnFlowStructureT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, listMainFlowIds);
		for (String str : list) {
			List<String> listfilePath = JecnCommonSql.getSetSqls(str, listMainFlowIds);
			for (String filePath : listfilePath) {
				if (filePath != null && !"".equals(filePath)) {
					JecnFinal.deleteFile(filePath);
				}
			}
		}
		// 查阅权限
		JecnUtil.deleteViewAuth(listMainFlowIds, 0, this);
		// 设计器权限
		JecnUtil.deleteDesignAuth(listMainFlowIds, 0, this);
		// 发布流程的记录
		JecnUtil.deleteRecord(listMainFlowIds, 4, this);
		// 任务
		JecnUtil.deleteTask(listMainFlowIds, 4, this);
		// 删除标准
		hql = "delete from StandardBean where stanType=3 and relationId in";
		list = JecnCommonSql.getSetSqls(hql, listMainFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除收藏
		String sql = "delete from jecn_my_star where type=1 and related_id in";
		list = JecnCommonSql.getSetSqls(sql, listMainFlowIds);
		for (String str : list) {
			execteNative(str);
		}

		// 删除合理化建议 0流程，1制度，2流程架构，3文件，4风险，5标准
		JecnDaoUtil.deleteProposeByRelatedIds(listMainFlowIds, 2, this);

		// 流程地图对象 设计器
		hql = "delete from JecnFlowStructureT where flowId in";
		list = JecnCommonSql.getSetSqls(hql, listMainFlowIds);
		for (String str : list) {
			execteHql(str);
		}
		// 流程地图对象 浏览端
		hql = "delete from JecnFlowStructure where flowId in";
		list = JecnCommonSql.getSetSqls(hql, listMainFlowIds);
		for (String str : list) {
			execteHql(str);
		}

	}

	@Override
	public void deleteFlowsByProjectId(Long projectId) throws Exception {
		String hql = "select flowId,isFlow from JecnFlowStructureT where projectId=?";
		List<Object[]> listObjects = this.listHql(hql, projectId);
		// 所有流程
		Set<Long> setFlowIds = new HashSet<Long>();
		// 所有流程地图
		Set<Long> listMainFlowIds = new HashSet<Long>();
		for (Object[] obj : listObjects) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			if ("0".equals(obj[1].toString())) {
				listMainFlowIds.add(Long.valueOf(obj[0].toString()));
			} else if ("1".equals(obj[1].toString())) {
				setFlowIds.add(Long.valueOf(obj[0].toString()));
			}
		}
		if (setFlowIds.size() > 0) {
			this.deleteFlows(setFlowIds);
		}
		if (listMainFlowIds.size() > 0) {
			this.deleteMainFlows(listMainFlowIds);
		}
	}

	@Override
	public List<JecnActivityFileT> findJecnActivityFileTByFlowId(Long flowId) throws Exception {
		String hql = "select a from JecnActivityFileT as a,JecnFlowStructureImageT as b where a.figureId=b.figureId and b.flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
	public List<JecnTempletT> findJecnTempletTByFlowId(Long flowId) throws Exception {
		String hql = "select a from JecnTempletT as a,JecnModeFileT as c,JecnFlowStructureImageT as b where a.modeFileId=c.modeFileId and c.figureId=b.figureId and b.flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
	public void revokeProcessRelease(Long processId) throws Exception {
		// 删除样例表的数据
		String sql = "delete from jecn_templet"
				+ "       where mode_file_id in"
				+ "       (select jmf.mode_file_id from jecn_mode_file jmf,jecn_flow_structure_image jfsi where jmf.figure_id = jfsi.figure_id and jfsi.flow_id=?)";
		this.execteNative(sql, processId);
		// 删除活动输出的数据
		sql = "delete from jecn_mode_file where figure_id in"
				+ "       (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=?)";
		this.execteNative(sql, processId);
		// 删除活动的输入和操作规范的数据
		sql = "delete from jecn_activity_file where figure_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=?)";
		this.execteNative(sql, processId);
		// 删除活动的指标的数据
		sql = "delete from jecn_ref_indicators where activity_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=?)";
		this.execteNative(sql, processId);
		// 删除角色的岗位的数据
		sql = "delete from process_station_related where figure_flow_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=?)";
		this.execteNative(sql, processId);
		// 删除线的线段的数据
		sql = "delete from jecn_line_segment where figure_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=?)";
		this.execteNative(sql, processId);
		// 删除角色和活动的关系数据
		sql = "delete from JECN_ROLE_ACTIVE where figure_role_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=?)";
		this.execteNative(sql, processId);
		sql = "delete from JECN_ROLE_ACTIVE where figure_active_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=?)";
		this.execteNative(sql, processId);
		/** *********** 【二期需求】 **************** */
		// 活动线上信息（活动与信息化关系临时表）
		sql = "DELETE FROM JECN_ACTIVE_ONLINE WHERE PROCESS_ID= ?";
		this.execteNative(sql, processId);

		// 活动相关控制点
		sql = "delete from JECN_CONTROL_POINT  where active_id in (select jfsi.figure_id  from jecn_flow_structure_image jfsi where jfsi.flow_id = ?)";
		this.execteNative(sql, processId);

		// 活动相关标准
		sql = "DELETE FROM Jecn_Active_Standard WHERE EXISTS (SELECT 1 FROM JECN_FLOW_STRUCTURE_IMAGE I WHERE I.FIGURE_ID = ACTIVE_ID AND I.FLOW_ID=?)";
		this.execteNative(sql, processId);
		// 删除流程图文档元素附件
		sql = "DELETE FROM JECN_FIGURE_FILE  WHERE EXISTS (SELECT 1 FROM jecn_flow_structure_image T WHERE T.FIGURE_ID = JECN_FIGURE_FILE.FIGURE_ID  AND T.FLOW_ID=?)";
		this.execteNative(sql, processId);
		// 删除图形的数据
		sql = "delete from jecn_flow_structure_image where flow_id=?";
		this.execteNative(sql, processId);
		// 删除相关标准的数据
		sql = "delete from JECN_FLOW_STANDARD where flow_id=?";
		this.execteNative(sql, processId);
		// 删除相关制度的数据
		sql = "delete from flow_related_criterion where flow_id=?";
		this.execteNative(sql, processId);
		// 删除责任部门
		sql = "delete from JECN_FLOW_RELATED_ORG where flow_id=?";
		this.execteNative(sql, processId);
		// 删除流程图相关支持工具
		sql = "delete from JECN_FLOW_TOOL_RELATED where flow_id=?";
		this.execteNative(sql, processId);
		// 删除流程记录(流程图操作说明记录保存表)
		sql = "delete from JECN_FLOW_RECORD where flow_id=?";
		this.execteNative(sql, processId);

		// 清除和KPI对应不上的数据
		if (JecnContants.dbType == DBType.ORACLE) {
			sql = "DELETE FROM JECN_FLOW_KPI V" + " WHERE EXISTS (SELECT 1 FROM JECN_FLOW_KPI_NAME K"
					+ "          LEFT JOIN JECN_FLOW_KPI_NAME_T KT ON K.KPI_AND_ID = KT.KPI_AND_ID"
					+ "         WHERE KT.KPI_AND_ID IS NULL AND V.KPI_AND_ID = K.KPI_AND_ID AND K.FLOW_ID = ?)";
		} else {
			sql = "DELETE V FROM JECN_FLOW_KPI V" + " WHERE EXISTS (SELECT 1 FROM JECN_FLOW_KPI_NAME K"
					+ "          LEFT JOIN JECN_FLOW_KPI_NAME_T KT ON K.KPI_AND_ID = KT.KPI_AND_ID"
					+ "         WHERE KT.KPI_AND_ID IS NULL AND V.KPI_AND_ID = K.KPI_AND_ID AND K.FLOW_ID = ?)";
		}
		this.execteNative(sql, processId);
		// 流程KPI与支持工具关系数据
		sql = "DELETE FROM JECN_SUSTAIN_TOOL_CONN WHERE RELATED_TYPE=0 AND RELATED_ID IN(SELECT K.KPI_AND_ID FROM JECN_FLOW_KPI_NAME K where K.FLOW_ID=?)";
		this.execteNative(sql, processId);
		// 删除流程KPI
		sql = "delete from JECN_FLOW_KPI_NAME where flow_id=?";
		this.execteNative(sql, processId);

		// 删除流程驱动规则(流程图时间驱动表)
		sql = "delete from JECN_FLOW_DRIVER where flow_id=?";
		this.execteNative(sql, processId);
		// 删除流程基本信息
		sql = "delete from JECN_FLOW_BASIC_INFO where flow_id=?";
		this.execteNative(sql, processId);
		// 删除岗位查阅权限
		sql = "delete from jecn_access_permissions where type=0 and RELATE_ID=?";
		this.execteNative(sql, processId);
		// 删除部门查阅权限
		sql = "delete from JECN_ORG_ACCESS_PERMISSIONS where type=0 and RELATE_ID=?";
		this.execteNative(sql, processId);
		// 删除岗位组查阅权限
		sql = "delete from JECN_GROUP_PERMISSIONS where type=0 and RELATE_ID=?";
		this.execteNative(sql, processId);

		// // 删除发布时生成的临时图片
		// JecnFlowStructure flowStructure =
		// this.findJecnFlowStructureById(processId);
		// if (flowStructure != null) {
		// JecnFinal.deleteFile(flowStructure.getFilePath());
		// }
		// 删除流程对象
		sql = "delete from JECN_FLOW_STRUCTURE where flow_id=?";
		this.execteNative(sql, processId);

		// 删除to from 和活动的关联关系
		sql = "delete from JECN_TO_FROM_ACTIVE where flow_id=?";
		this.execteNative(sql, processId);

		sql = "delete from FLOW_STANDARDIZED_FILE where FLOW_ID=?";
		this.execteNative(sql, processId);

		sql = "delete from JECN_FLOW_RELATED_RISK where FLOW_ID=?";
		this.execteNative(sql, processId);

		// 流程（文件） 内容
		sql = "DELETE FROM FLOW_FILE_CONTENT  WHERE  ID IN (SELECT FILE_CONTENT_ID FROM  JECN_FLOW_STRUCTURE  WHERE FLOW_ID =?)";
		this.execteNative(sql, processId);

		// 流程适用部门
		sql = "delete from JECN_FLOW_APPLY_ORG where  RELATED_ID=?";
		this.execteNative(sql, processId);

		// 流程驱动提醒人
		sql = "delete from JECN_FLOW_DRIVER_EMAIL where  RELATED_ID=?";
		this.execteNative(sql, processId);

		// -------------------新版输入输出
		// 和岗位岗位组关联关系表
		sql = "DELETE FROM JECN_FLOW_IN_OUT_POS WHERE FLOW_ID=?";
		this.execteNative(sql, processId);

		// 新版输入输出
		sql = "delete FROM JECN_FLOW_IN_OUT WHERE FLOW_ID=?";
		this.execteNative(sql, processId);

		// 样例的关联关系表
		sql = "DELETE FROM JECN_FIGURE_IN_OUT_SAMPLE WHERE FLOW_ID =?";
		this.execteNative(sql, processId);

		// 新版活动的输入输出表
		sql = " DELETE FROM JECN_FIGURE_IN_OUT WHERE FLOW_ID=?";
		this.execteNative(sql, processId);

		// -------------------新版输入输出结束

		// 驱动规则时间表
		sql = " DELETE FROM JECN_FLOW_DRIVER_TIME WHERE FLOW_ID=?";
		this.execteNative(sql, processId);
	}

	/**
	 * 发布流程
	 * 
	 */
	@Override
	public void releaseProcessData(Long processId) throws Exception {
		// 清除老版本的数据
		this.revokeProcessRelease(processId);
		// 1发布活动的样例
		String sql = "";
		if (JecnContants.dbType == DBType.ORACLE) {
			// 添加样例表的数据
			sql = "insert into jecn_templet (id,mode_file_id,file_id)"
					+ "       select JECN_TEMPLET_SQUENCE.Nextval,jt.mode_file_id,jt.file_id from jecn_templet_t jt"
					+ "       where jt.mode_file_id in"
					+ "       (select jmf.mode_file_id from jecn_mode_file_t jmf,jecn_flow_structure_image_t jfsi where jmf.figure_id = jfsi.figure_id and  jfsi.flow_id=?)";
			this.execteNative(sql, processId);
		} else {
			// 添加样例表的数据
			sql = "insert into jecn_templet (mode_file_id,file_id)"
					+ "       select jt.mode_file_id,jt.file_id from jecn_templet_t jt"
					+ "       where jt.mode_file_id in"
					+ "       (select jmf.mode_file_id from jecn_mode_file_t jmf,jecn_flow_structure_image_t jfsi where jmf.figure_id = jfsi.figure_id and  jfsi.flow_id=?)";
			this.execteNative(sql, processId);
		}
		// 添加活动输出的数据
		sql = JecnSqlConstant.JECN_MODE_FILE_T;
		this.execteNative(sql, processId);

		// 添加活动的输入和操作规范的数据
		sql = JecnSqlConstant.JECN_ACTIVITY_FILE_T;
		this.execteNative(sql, processId);

		// 添加活动的指标的数据
		sql = JecnSqlConstant.JECN_REF_INDICATORST;
		this.execteNative(sql, processId);

		// 添加角色的岗位的数据
		sql = JecnSqlConstant.PROCESS_STATION_RELATED_T;
		this.execteNative(sql, processId);

		// 添加线的线段的数据
		sql = JecnSqlConstant.JECN_LINE_SEGMENT_T;
		this.execteNative(sql, processId);

		// 角色活动关系表
		sql = JecnSqlConstant.JECN_ROLE_ACTIVE_T;
		this.execteNative(sql, processId);

		// 添加图形的数据
		sql = JecnSqlConstant.JECN_FLOW_STRUCTURE_IMAGE_T;
		this.execteNative(sql, processId);

		// 发布流程基本信息
		sql = JecnSqlConstant.FLOW_BASIC_INFO_T;
		this.execteNative(sql, processId);

		// 添加相关标准的数据
		sql = JecnSqlConstant.JECN_FLOW_STANDARD_T;
		this.execteNative(sql, processId);

		// 添加相关制度的数据
		sql = JecnSqlConstant.FLOW_RELATED_CRITERION_T;
		this.execteNative(sql, processId);

		// 发布流程责任部门
		sql = JecnSqlConstant.JECN_FLOW_RELATED_ORG_T;
		this.execteNative(sql, processId);

		// 发布流程支持工具
		sql = JecnSqlConstant.Jecn_Flow_Tool_Related_T;
		this.execteNative(sql, processId);

		// 发布流程保持记录
		sql = JecnSqlConstant.JECN_FLOW_RECORD_T;
		this.execteNative(sql, processId);

		// 发布流程KPI则基本信息
		sql = "INSERT INTO JECN_FLOW_KPI_NAME"
				+ " (KPI_AND_ID,FLOW_ID,KPI_NAME,KPI_TYPE,KPI_DEFINITION,KPI_STATISTICAL_METHODS,KPI_TARGET,KPI_HORIZONTAL,KPI_VERTICAL,CREAT_TIME,KPI_TARGET_OPERATOR,KPI_DATA_METHOD,KPI_DATA_PEOPEL_ID,KPI_RELEVANCE,KPI_TARGET_TYPE,FIRST_TARGER_ID,UPDATE_TIME,CREATE_PEOPLE_ID,UPDATE_PEOPLE_ID,KPI_PURPOSE,KPI_POINT,KPI_PERIOD,KPI_EXPLAIN)"
				+ " SELECT KPI_AND_ID,FLOW_ID,KPI_NAME,KPI_TYPE,KPI_DEFINITION,KPI_STATISTICAL_METHODS,KPI_TARGET,KPI_HORIZONTAL,KPI_VERTICAL,CREAT_TIME,KPI_TARGET_OPERATOR,KPI_DATA_METHOD,KPI_DATA_PEOPEL_ID,KPI_RELEVANCE,KPI_TARGET_TYPE,FIRST_TARGER_ID,UPDATE_TIME,CREATE_PEOPLE_ID,UPDATE_PEOPLE_ID,KPI_PURPOSE,KPI_POINT,KPI_PERIOD,KPI_EXPLAIN"
				+ " FROM JECN_FLOW_KPI_NAME_T WHERE FLOW_ID = ?";
		this.execteNative(sql, processId);
		// 流程KPI对应的支持工具关系数据
		sql = "INSERT INTO JECN_SUSTAIN_TOOL_CONN (ID,SUSTAIN_TOOL_ID,RELATED_ID,RELATED_TYPE,CREAT_TIME,CREATE_PEOPLE_ID,UPDATE_TIME,UPDATE_PEOPLE_ID )"
				+ " SELECT ID,SUSTAIN_TOOL_ID,RELATED_ID,RELATED_TYPE,CREAT_TIME,CREATE_PEOPLE_ID,UPDATE_TIME,UPDATE_PEOPLE_ID"
				+ " FROM JECN_SUSTAIN_TOOL_CONN_T TC WHERE TC.RELATED_TYPE=0 AND TC.RELATED_ID IN(SELECT K.KPI_AND_ID FROM JECN_FLOW_KPI_NAME_T K where K.FLOW_ID=?)";
		this.execteNative(sql, processId);

		// 发布流程驱动规则基本信息
		sql = JecnSqlConstant.JECN_FLOW_DRIVERT;
		this.execteNative(sql, processId);

		// 岗位查阅权限
		sql = JecnSqlConstant.JECN_ACCESS_PERMISSIONS_T;
		this.execteNative(sql, 0, processId);

		// 部门查阅权限
		sql = JecnSqlConstant.JECN_ORG_ACCESS_PERM_T;
		this.execteNative(sql, 0, processId);

		// 岗位组查阅权限
		this.execteNative(JecnSqlConstant.JECN_ORG_GROUP_PERM_T, 0, processId);
		/** ************ 【二期新增】 ********************** */
		// 活动线上信息
		sql = JecnSqlConstant.ACTIVE_ONLINE;
		this.execteNative(sql, processId);
		// 活动控制点
		sql = JecnSqlConstant.ACTIVE_CONTROL_POINT;
		this.execteNative(sql, processId);
		// 活动相关标准
		sql = JecnSqlConstant.ACTIVE_STANDARD;
		this.execteNative(sql, processId);
		// 发布流程地图元素附件表
		sql = "insert into JECN_FIGURE_FILE(ID, FIGURE_ID, FILE_ID, FIGURE_TYPE) select ID, FIGURE_ID, FILE_ID, FIGURE_TYPE from JECN_FIGURE_FILE_T "
				+ "where FIGURE_ID IN (SELECT T.FIGURE_ID FROM jecn_flow_structure_image_t T WHERE T.FLOW_ID=?)";
		this.execteNative(sql, processId);

		// 发布to from关联关系表
		sql = "insert into JECN_TO_FROM_ACTIVE(TO_FROM_FIGURE_ID, ACTIVE_FIGURE_ID, FLOW_ID) select TO_FROM_FIGURE_ID, ACTIVE_FIGURE_ID, FLOW_ID from JECN_TO_FROM_ACTIVE_T where flow_id=?";
		this.execteNative(sql, processId);

		// 流程相关风险
		sql = JecnSqlConstant.JECN_FLOW_RELATED_RISK;
		this.execteNative(sql, processId);
		// 流程相关标准化文件
		sql = JecnSqlConstant.FLOW_STANDARDIZED_FILE;
		this.execteNative(sql, processId);

		// 流程适用部门
		sql = JecnSqlConstant.FLOW_APPLY_ORG;
		this.execteNative(sql, processId);

		// 流程驱动提醒人
		sql = JecnSqlConstant.FLOW_DRIVER_EMAIL_PEOPLE;
		this.execteNative(sql, processId);

		// 发布流程对象
		sql = JecnSqlConstant.JECN_FLOW_STRUCTURE_T;
		this.execteNative(sql, processId);

		// 新版输入输出
		sql = JecnSqlConstant.JECN_FLOW_IN_OUT_T;
		this.execteNative(sql, processId);

		// 和岗位岗位组关联关系表
		sql = JecnSqlConstant.JECN_FLOW_IN_OUT_POS_T;
		this.execteNative(sql, processId);

		// 新版活动的输入输出表
		sql = JecnSqlConstant.JECN_FIGURE_IN_OUT_T;
		this.execteNative(sql, processId);

		// 样例的关联关系表
		sql = JecnSqlConstant.JECN_FIGURE_IN_OUT_SAMPLE_T;
		this.execteNative(sql, processId);

		// 驱动时间
		sql = JecnSqlConstant.JECN_FLOW_DRIVER_TIME_T;
		this.execteNative(sql, processId);
	}

	/**
	 * 
	 * 删除流程地图的数据
	 * 
	 * @param processId
	 *            Long 流程主键ID
	 * 
	 * @throws Exception
	 */
	@Override
	public void revokeProcessMapRelease(Long processId) throws Exception {
		// 删除线的线段的数据
		String sql = "delete from jecn_line_segment where figure_id in (select jfsi.figure_id from jecn_flow_structure_image jfsi where jfsi.flow_id=?)";
		this.execteNative(sql, processId);
		// 删除流程地图元素对应附件信息
		sql = "DELETE FROM JECN_FIGURE_FILE WHERE EXISTS (SELECT 1 FROM jecn_flow_structure_image T WHERE T.FIGURE_ID =JECN_FIGURE_FILE.FIGURE_ID  AND T.FLOW_ID=?)";
		this.execteNative(sql, processId);
		// 删除流程地图相关制度表
		sql = "delete from flow_related_criterion where flow_id=?";
		this.execteNative(sql, processId);
		// 删除图形元素的数据
		sql = "delete from jecn_flow_structure_image where flow_id=?";
		this.execteNative(sql, processId);
		// 删除流程地图基本信息(信息表包括流程地图关联附件ID)
		sql = "delete from JECN_MAIN_FLOW where flow_id=?";
		this.execteNative(sql, processId);

		// 删除流程地图的岗位查阅权限
		sql = "delete from jecn_access_permissions where type=0 and RELATE_ID=?";
		this.execteNative(sql, processId);
		// 删除流程地图的部门查阅权限
		sql = "delete from JECN_ORG_ACCESS_PERMISSIONS where type=0 and RELATE_ID=?";
		this.execteNative(sql, processId);
		// 删除岗位组查阅权限
		sql = "delete from JECN_GROUP_PERMISSIONS where type=0 and RELATE_ID=?";
		this.execteNative(sql, processId);

		// // 删除发布时生成的临时图片
		// JecnFlowStructure flowStructure =
		// this.findJecnFlowStructureById(processId);
		// if (flowStructure != null) {
		// JecnFinal.deleteFile(flowStructure.getFilePath());
		// }
		// 删除流程地图对象
		sql = "delete from JECN_FLOW_STRUCTURE where flow_id=?";
		this.execteNative(sql, processId);

		sql = "DELETE FROM JECN_FLOW_RELATED_ORG WHERE FLOW_ID = ?";
		this.execteNative(sql, processId);

		sql = "delete from FLOW_STANDARDIZED_FILE where FLOW_ID=?";
		this.execteNative(sql, processId);

		/************* 【集成关系图 start】 *************************/
		// 删除线的线段的数据
		sql = "delete from JECN_MAP_RELATED_LINE where figure_id in (select jfsi.figure_id from JECN_MAP_STRUCTURE_IMAGE jfsi where jfsi.flow_id=?)";
		this.execteNative(sql, processId);

		// 删除- 元素
		sql = "DELETE FROM JECN_MAP_STRUCTURE_IMAGE where flow_id=?";
		this.execteNative(sql, processId);

		/************* 【集成关系图 end】 *************************/
	}

	@Override
	public void releaseProcessMapData(Long processId) throws Exception {
		// 删除流程地图老版本的数据
		this.revokeProcessMapRelease(processId);
		// 发布流程地图元素附件表
		String sql = "insert into JECN_FIGURE_FILE(ID, FIGURE_ID, FILE_ID, FIGURE_TYPE) select ID, FIGURE_ID, FILE_ID, FIGURE_TYPE from JECN_FIGURE_FILE_T "
				+ "where FIGURE_ID IN (SELECT T.FIGURE_ID FROM jecn_flow_structure_image_t T WHERE T.FLOW_ID=?)";
		this.execteNative(sql, processId);
		// 添加线的线段的数据
		sql = "insert into jecn_line_segment(ID,FIGURE_ID,START_X,START_Y,END_X,END_Y) "
				+ "       select jlst.ID,jlst.FIGURE_ID,jlst.START_X,jlst.START_Y,jlst.END_X,jlst.END_Y"
				+ "       from jecn_line_segment_t jlst,jecn_flow_structure_image_t jfsit"
				+ "       where jlst.figure_id=jfsit.figure_id and jfsit.flow_id=?";
		this.execteNative(sql, processId);

		// 添加图形的数据
		sql = JecnSqlConstant.JECN_FLOW_STRUCTURE_IMAGE_T;
		this.execteNative(sql, processId);

		// 发布流程地图基本信息(信息表包括流程地图关联附件ID)
		sql = "insert into JECN_MAIN_FLOW(FLOW_ID, FLOW_AIM, FLOW_AREA, FLOW_INPUT, FLOW_OUTPUT, FLOW_SHOW_STEP, FLOW_NOUN_DEFINE, FILE_ID,FLOW_DESCRIPTION,FLOW_START,FLOW_END,FLOW_KPI,TYPE_RES_PEOPLE,RES_PEOPLE_ID,SUPPLIER,CUSTOMER,RISK_OPPORTUNITY)"
				+ " select FLOW_ID, FLOW_AIM, FLOW_AREA, FLOW_INPUT, FLOW_OUTPUT, FLOW_SHOW_STEP, FLOW_NOUN_DEFINE,FILE_ID,FLOW_DESCRIPTION,FLOW_START,FLOW_END,FLOW_KPI,TYPE_RES_PEOPLE,RES_PEOPLE_ID,SUPPLIER,CUSTOMER,RISK_OPPORTUNITY"
				+ " from JECN_MAIN_FLOW_T  where flow_id=?";
		this.execteNative(sql, processId);

		// 岗位查阅权限
		sql = "insert into jecn_access_permissions(ID, FIGURE_ID, RELATE_ID, TYPE,ACCESS_TYPE) select ID, FIGURE_ID, RELATE_ID, TYPE,ACCESS_TYPE from jecn_access_permissions_t  where type=0 and RELATE_ID=?";
		this.execteNative(sql, processId);

		// 部门查阅权限
		sql = "insert into JECN_ORG_ACCESS_PERMISSIONS(ID, ORG_ID, RELATE_ID, TYPE,ACCESS_TYPE) select ID, ORG_ID, RELATE_ID, TYPE,ACCESS_TYPE from JECN_ORG_ACCESS_PERM_T where type=0 and RELATE_ID=?";
		this.execteNative(sql, processId);

		// 岗位组查阅权限
		this.execteNative(JecnSqlConstant.JECN_ORG_GROUP_PERM_T, 0, processId);

		// 发布流程对象
		sql = JecnSqlConstant.JECN_FLOW_STRUCTURE_T;
		this.execteNative(sql, processId);

		// 添加相关制度的数据
		sql = JecnSqlConstant.FLOW_RELATED_CRITERION_T;
		this.execteNative(sql, processId);

		/********* 【集成关系图】 *********/
		// 添加线的线段的数据
		sql = "insert into JECN_MAP_RELATED_LINE(ID,FIGURE_ID,START_X,START_Y,END_X,END_Y) "
				+ "       select jlst.ID,jlst.FIGURE_ID,jlst.START_X,jlst.START_Y,jlst.END_X,jlst.END_Y"
				+ "       from JECN_MAP_RELATED_LINE_t jlst,JECN_MAP_STRUCTURE_IMAGE_T jfsit"
				+ "       where jlst.figure_id=jfsit.figure_id and jfsit.flow_id=?";
		this.execteNative(sql, processId);
		// 添加图形的数据
		sql = JecnSqlConstant.JECN_MAP_STRUCTURE_IMAGE_T;
		this.execteNative(sql, processId);

		// 发布流程责任部门
		sql = JecnSqlConstant.JECN_FLOW_RELATED_ORG_T;
		this.execteNative(sql, processId);

		// 流程相关标准化文件
		sql = JecnSqlConstant.FLOW_STANDARDIZED_FILE;
		this.execteNative(sql, processId);
	}

	@Override
	public List<Object[]> getRelateRuleByFlowId(Long flowId) throws Exception {
		// c.isDir 0是目录，1是制度,2是制度文件
		String sql = "select distinct t.id,t.is_dir,case when t.is_dir=2 and t.is_file_local=0 then (select jf.version_id from jecn_file_t jf where jf.file_id=t.file_id) else null end as version_id from jecn_rule_t t left join jecn_rule jr on t.id=jr.id where jr.id is null"
				+ "      and t.id in (select jr.id from jecn_rule_t jr,flow_related_criterion_t frc where jr.id=frc.criterion_class_id and frc.flow_id=?)";
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public List<Object[]> getRelateRuleByFlowMapId(Long flowId) throws Exception {
		String sql = "SELECT DISTINCT T.ID," + "                T.IS_DIR," + "                CASE"
				+ "                  WHEN T.IS_DIR = 2 THEN" + "                   (SELECT JF.VERSION_ID"
				+ "                      FROM JECN_FILE_T JF"
				+ "                     WHERE JF.FILE_ID = T.FILE_ID and jf.del_state=0)" + "                  ELSE"
				+ "                   NULL" + "                END AS VERSION_ID" + "  FROM JECN_RULE_T T"
				+ "  LEFT JOIN JECN_RULE JR ON T.ID = JR.ID" + " WHERE JR.ID IS NULL" + "   AND T.ID IN (SELECT JR.ID"
				+ "                  FROM JECN_RULE_T JR, JECN_FLOW_STRUCTURE_IMAGE_T JFSI"
				+ "                 WHERE JR.ID = JFSI.LINK_FLOW_ID" + "                   AND JFSI.FIGURE_TYPE ="
				+ JecnCommonSql.getSystemString() + "                   AND JFSI.FLOW_ID = ?)" + " UNION "
				+ "SELECT DISTINCT T.ID," + "                T.IS_DIR," + "                CASE"
				+ "                  WHEN T.IS_DIR = 2 THEN" + "                   (SELECT JF.VERSION_ID"
				+ "                      FROM JECN_FILE_T JF"
				+ "                     WHERE JF.FILE_ID = T.FILE_ID and jf.del_state=0)" + "                  ELSE"
				+ "                   NULL" + "                END AS VERSION_ID" + "  FROM JECN_RULE_T T"
				+ "  LEFT JOIN JECN_RULE JR ON T.ID = JR.ID" + " WHERE JR.ID IS NULL" + "   AND T.ID IN (SELECT JR.ID"
				+ "                  FROM JECN_RULE_T JR, FLOW_RELATED_CRITERION_T FRC"
				+ "                 WHERE JR.ID = FRC.CRITERION_CLASS_ID" + "                   AND FRC.FLOW_ID = ?)";
		return this.listNativeSql(sql, flowId, flowId);
	}

	@Override
	public List<FlowCriterionT> getFlowCriterionTByFlowId(Long flowId) throws Exception {
		String hql = "from FlowCriterionT where flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
	public List<StandardFlowRelationTBean> getStandardFlowRelationTBeanByFlowId(Long flowId) throws Exception {
		String hql = "from StandardFlowRelationTBean where flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
	public JecnFlowDriver getJecnFlowDriverByFlowId(Long flowId) throws Exception {
		return (JecnFlowDriver) this.getSession().get(JecnFlowDriver.class, flowId);
	}

	@Override
	public JecnFlowDriverT getJecnFlowDriverTByFlowId(Long flowId) throws Exception {
		return (JecnFlowDriverT) this.getSession().get(JecnFlowDriverT.class, flowId);
	}

	@Override
	public List<Object[]> getActiveShowByFlowId(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String hql = "select figureId,activityId,figureText,activityShow,lineColor,roleRes,activityInput,activityOutput,linkFlowId,figureType,targetValue,statusValue from JecnFlowStructureImage where  figureType in "
					+ JecnCommonSql.getOperationActiveString() + " and  flowId=?";
			return this.listHql(hql, flowId);
		} else {
			String hql = "select figureId,activityId,figureText,activityShow,lineColor,roleRes,activityInput,activityOutput,linkFlowId,figureType,targetValue,statusValue from JecnFlowStructureImageT where  figureType in "
					+ JecnCommonSql.getOperationActiveString() + " and  flowId=?";
			return this.listHql(hql, flowId);
		}
	}

	/**
	 * 流程获取操作模板相关活动
	 * 
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getProcessActiveShowByFlowId(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String hql = "select figureId,activityId,figureText,activityShow,lineColor,roleRes,activityInput,activityOutput,linkFlowId,figureType from JecnFlowStructureImage where  figureType in "
					+ JecnCommonSql.getActiveString() + " and  flowId=?";
			return this.listHql(hql, flowId);
		} else {
			String hql = "select figureId,activityId,figureText,activityShow,lineColor,roleRes,activityInput,activityOutput,linkFlowId,figureType from JecnFlowStructureImageT where  figureType in "
					+ JecnCommonSql.getActiveString() + " and  flowId=?";
			return this.listHql(hql, flowId);
		}
	}

	@Override
	public List<Object[]> getActiveByFlowId(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String hql = "select figureId,activityId,figureText,activityShow,lineColor,roleRes,innerControlRisk,standardConditions,linkFlowId,figureType,targetValue,statusValue from JecnFlowStructureImage where  figureType in "
					+ JecnCommonSql.getOperationActiveString() + " and  flowId=?";
			return this.listHql(hql, flowId);
		} else {
			String hql = "select figureId,activityId,figureText,activityShow,lineColor,roleRes,innerControlRisk,standardConditions,linkFlowId,figureType,targetValue,statusValue from JecnFlowStructureImageT where  figureType in "
					+ JecnCommonSql.getOperationActiveString() + " and  flowId=?";
			return this.listHql(hql, flowId);
		}
	}

	@Override
	public List<Object[]> getRoleShowByFlowId(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String hql = "select figureId,figureText,roleRes from JecnFlowStructureImage where  figureType in "
					+ JecnCommonSql.getRoleString() + " and  flowId=? order by poLongy";
			return this.listHql(hql, flowId);
		} else {
			String hql = "select figureId,figureText,roleRes from JecnFlowStructureImageT where  figureType in "
					+ JecnCommonSql.getRoleString() + " and  flowId=? order by poLongy";
			return this.listHql(hql, flowId);
		}
	}

	/**
	 * 巴德富角色不要客户
	 * 
	 * @date 2015-11-17 上午10:42:13
	 * @throws
	 */
	@Override
	public List<Object[]> getBadfRoleShowByFlowId(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String hql = "select figureId,figureText,roleRes from JecnFlowStructureImage where  figureType in "
					+ JecnCommonSql.getNoCoustomRoleString() + " and  flowId=? order by poLongy";
			return this.listHql(hql, flowId);
		} else {
			String hql = "select figureId,figureText,roleRes from JecnFlowStructureImageT where  figureType in "
					+ JecnCommonSql.getNoCoustomRoleString() + " and  flowId=? order by poLongy";
			return this.listHql(hql, flowId);
		}
	}

	@Override
	public List<Object[]> finaAllRoleAndPosRelates(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String sql = "select t.figure_id,jf.figure_id as figureId,jf.figure_text,jfo.org_id from JECN_FLOW_ORG_IMAGE jf,JECN_FLOW_ORG jfo,"
					+ "  jecn_flow_structure_image t,PROCESS_STATION_RELATED rs where jf.org_id=jfo.org_id and jfo.del_state=0"
					+ "  and jf.figure_type = "
					+ JecnCommonSql.getPosString()
					+ " and jf.figure_id = rs.figure_position_id and rs.type='0' and rs.figure_flow_id=t.figure_id and t.figure_type in "
					+ JecnCommonSql.getRoleString() + " and t.flow_id=?";
			return this.listNativeSql(sql, flowId);
		} else {
			String sql = "select t.figure_id,jf.figure_id as figureId,jf.figure_text,jfo.org_id from JECN_FLOW_ORG_IMAGE jf,JECN_FLOW_ORG jfo,"
					+ "  jecn_flow_structure_image_t t,PROCESS_STATION_RELATED_T rs where jf.org_id=jfo.org_id and jfo.del_state=0"
					+ "  and jf.figure_type = "
					+ JecnCommonSql.getPosString()
					+ " and jf.figure_id = rs.figure_position_id and rs.type='0' and rs.figure_flow_id=t.figure_id and t.figure_type in "
					+ JecnCommonSql.getRoleString() + " and t.flow_id=?";
			return this.listNativeSql(sql, flowId);
		}

	}

	@Override
	public List<Object[]> finaAllRoleAndPosGroupPosRelates(Long flowId, boolean isPub) throws Exception {
		String isStrPub = "";
		if (!isPub) {
			isStrPub = "_T";
		}
		String sql = "";
		if (JecnConfigTool.processFileShowPostionType()) {
			sql = "SELECT T.FIGURE_ID, PG.NAME" + "  FROM JECN_FLOW_STRUCTURE_IMAGE" + isStrPub + " T"
					+ " INNER JOIN PROCESS_STATION_RELATED" + isStrPub + " RS"
					+ "    ON RS.FIGURE_FLOW_ID = T.FIGURE_ID" + "   AND RS.TYPE = '1'"
					+ "  LEFT JOIN JECN_POSITION_GROUP PG" + "    ON PG.ID = RS.FIGURE_POSITION_ID"
					+ " WHERE T.FLOW_ID = ?";
		} else {
			sql = "select t.figure_id,jf.figure_id as pos_id,jf.figure_text from JECN_FLOW_ORG_IMAGE jf,JECN_FLOW_ORG jfo,"
					+ "  jecn_flow_structure_image"
					+ isStrPub
					+ " t,PROCESS_STATION_RELATED"
					+ isStrPub
					+ " rs,JECN_POSITION_GROUP_R jpg where jf.org_id=jfo.org_id and jfo.del_state=0"
					+ " and jf.figure_type = "
					+ JecnCommonSql.getPosString()
					+ " and jf.figure_id = jpg.figure_id and jpg.group_id = rs.figure_position_id and rs.type='1' and rs.figure_flow_id=t.figure_id and t.figure_type in "
					+ JecnCommonSql.getRoleString() + " and t.flow_id=?";
		}
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public List<Object[]> findAllRelateFlows(Long flowId, boolean isPub) throws Exception {
		String pub = "";
		if (!isPub) {
			pub = "_T";
		}
		String sql = "SELECT T.IMPL_TYPE," + "       JFS.Flow_Id_Input," + "       CASE"
				+ "         WHEN JFS.FLOW_ID IS NULL THEN" + "          T.FIGURE_TEXT" + "         ELSE"
				+ "          JFS.FLOW_NAME" + "       END FLOW_NAME," + "       T.LINK_FLOW_ID"
				+ "  FROM JECN_FLOW_STRUCTURE_IMAGE" + pub + " T" + "  LEFT JOIN JECN_FLOW_STRUCTURE" + pub + " JFS"
				+ "    ON T.LINK_FLOW_ID = JFS.FLOW_ID" + "   AND JFS.DEL_STATE = 0" + " WHERE T.FIGURE_TYPE IN "
				+ JecnCommonSql.getImplString() + "   AND T.IMPL_TYPE IN (1, 2, 3, 4)" + "   AND T.FLOW_ID = ?"
				+ " ORDER BY T.IMPL_TYPE,T.LINK_FLOW_ID";
		return this.listNativeSql(sql, flowId);
	}

	/**
	 * 农夫山泉操作说明下载专用
	 * 
	 * @description：流程下所有的相关流程 0是linecolor 1是流程编号 2是流程名称
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> findAllRelateFlowsNFSQ(Long flowId, boolean isPub) throws Exception {
		String sql = null;
		if (isPub) {
			sql = "SELECT distinct A.IMPL_TYPE,B.FLOW_ID_INPUT, B.FLOW_NAME, O.ORG_NAME"
					+ " FROM JECN_FLOW_STRUCTURE_IMAGE  A"
					+ " LEFT JOIN  JECN_FLOW_STRUCTURE  B ON  A.LINK_FLOW_ID = B.FLOW_ID"
					+ " LEFT JOIN JECN_FLOW_RELATED_ORG  FO ON FO.FLOW_ID=A.LINK_FLOW_ID"
					+ " LEFT JOIN JECN_FLOW_ORG O ON FO.ORG_ID=O.ORG_ID" + " WHERE A.IMPL_TYPE IN (1, 2, 3, 4)"
					+ "  AND A.FIGURE_TYPE IN " + JecnCommonSql.getImplString() + "  AND B.DEL_STATE = 0"
					+ "  AND A.FLOW_ID = ?" + "ORDER BY A.IMPL_TYPE";
		} else {
			sql = "SELECT distinct A.IMPL_TYPE,B.FLOW_ID_INPUT, B.FLOW_NAME,O.ORG_NAME"
					+ "  FROM JECN_FLOW_STRUCTURE_IMAGE_T  A"
					+ "  LEFT JOIN  JECN_FLOW_STRUCTURE_T  B ON  A.LINK_FLOW_ID = B.FLOW_ID"
					+ "  LEFT JOIN JECN_FLOW_RELATED_ORG_T  FO ON FO.FLOW_ID=A.LINK_FLOW_ID"
					+ "  LEFT JOIN JECN_FLOW_ORG O ON FO.ORG_ID=O.ORG_ID" + "  WHERE A.IMPL_TYPE IN (1, 2, 3, 4)"
					+ "   AND A.FIGURE_TYPE IN " + JecnCommonSql.getImplString() + "   AND B.DEL_STATE = 0"
					+ "   AND A.FLOW_ID = ?" + " ORDER BY A.IMPL_TYPE";
		}
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public List<Object[]> findAllRelateFlowsWeb(Long flowId, boolean isPub) throws Exception {
		String pub = "_T";
		if (isPub) {
			pub = "";
		}
		String sql = "select JFSI.LINK_FLOW_ID,"
				+ "CASE"
				+ "        WHEN T.FLOW_ID IS NULL THEN"
				+ "         JFSI.FIGURE_TEXT"
				+ "        ELSE"
				+ "         T.FLOW_NAME"
				+ "      END FLOW_NAME,"
				+ "t.flow_id_input,org.org_id"
				+ "         ,org.org_name,jfbi.type_res_people,jfbi.res_people_id"
				+ "            ,case when jfbi.type_res_people=0 then ju.true_name"
				+ "           when jfbi.type_res_people=1 then jfoi.figure_text"
				+ "              end as res_people"
				+ "        ,t.update_date,t.is_public,t.create_date,jfsi.IMPL_TYPE"
				+ " FROM JECN_FLOW_STRUCTURE_IMAGE"
				+ pub
				+ " JFSI"
				+ "   LEFT JOIN JECN_FLOW_STRUCTURE"
				+ pub
				+ " T"
				+ "   ON JFSI.LINK_FLOW_ID = T.FLOW_ID"
				+ "   AND T.DEL_STATE = 0"
				+ "  LEFT JOIN JECN_FLOW_BASIC_INFO"
				+ pub
				+ " JFBI"
				+ "    ON JFBI.FLOW_ID = T.FLOW_ID"
				+ "     left join (select jfro.org_id,jfo.org_name,jfro.flow_id from jecn_flow_related_org jfro,jecn_flow_org jfo where jfro.org_id=jfo.org_id and jfo.del_state=0) org on org.flow_id=jfbi.flow_id"
				+ " 	left join jecn_flow_org_image jfoi on jfoi.figure_id=jfbi.res_people_id"
				+ " 	left join jecn_user ju on ju.islock=0 and ju.people_id=jfbi.res_people_id"
				+ "     where jfsi.IMPL_TYPE IN (1, 2, 3, 4) AND jfsi.FIGURE_TYPE IN " + JecnCommonSql.getImplString()
				+ "           and jfsi.flow_id=?  ORDER BY jfsi.IMPL_TYPE";
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public Object[] findFlowMap(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String sql = "select t.flow_aim,t.flow_area,t.flow_noun_define,t.flow_input,t.flow_output,t.flow_show_step,(select jf.file_name from jecn_file jf where jf.file_id = t.file_id and jf.del_state=0) as fileName,t.file_id from Jecn_Main_Flow t where t.flow_Id=?";
			return this.getObjectNativeSql(sql, flowId);
		} else {
			String sql = "select t.flow_aim,t.flow_area,t.flow_noun_define,t.flow_input,t.flow_output,t.flow_show_step,(select jf.file_name from jecn_file_t jf where jf.file_id = t.file_id and jf.del_state=0) as fileName,t.file_id from Jecn_Main_Flow_t t where t.flow_Id=?";
			return this.getObjectNativeSql(sql, flowId);
		}
	}

	/**
	 * 查询流程下指标
	 * 
	 * @author fuzhh Apr 26, 2013
	 * @param flowId
	 *            流程ID
	 * @param isPub
	 *            true为正式表 false 为临时表
	 * @return 0主键ID ， 1指标名 ， 2指标值 , 3活动ID
	 * @throws Exception
	 */
	public List<Object[]> getRefIndicators(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String sql = "select jri.id,jri.indicator_name,jri.indicator_value,jri.activity_id"
					+ " from jecn_flow_structure_image jfs inner join JECN_REF_INDICATORS jri "
					+ " on jfs.figure_id = jri.activity_id where jfs.flow_id=?";
			return this.listNativeSql(sql, flowId);
		} else {
			String sql = "select jri.id,jri.indicator_name,jri.indicator_value,jri.activity_id"
					+ " from jecn_flow_structure_image_t jfs inner join JECN_REF_INDICATORST jri "
					+ " on jfs.figure_id = jri.activity_id where jfs.flow_id=?";
			return this.listNativeSql(sql, flowId);
		}
	}

	@Override
	public List<Long> findProcessMapNoPubFiles(Long flowId) throws Exception {
		return JecnUtil.findProcessMapNoPubFiles(flowId, this);
	}

	@Override
	public List<Long> findProcessNoPubFiles(Long flowId) throws Exception {
		return JecnUtil.findProcessNoPubFiles(flowId, this);
	}

	@Override
	public JecnFlowBasicInfo findJecnFlowBasicInfoById(Long flowId) throws Exception {
		return (JecnFlowBasicInfo) this.getSession().get(JecnFlowBasicInfo.class, flowId);
	}

	@Override
	public JecnFlowStructure findJecnFlowStructureById(Long flowId) throws Exception {
		return (JecnFlowStructure) this.getSession().get(JecnFlowStructure.class, flowId);
	}

	@Override
	public List<String> getListTools(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String hql = "select b.flowSustainToolShow from JecnSustainToolRelated as a,JecnFlowSustainTool as b where b.flowSustainToolId=a.flowSustainToolId and a.flowId=?";
			return this.listHql(hql, flowId);
		} else {
			String hql = "select b.flowSustainToolShow from JecnSustainToolRelatedT as a,JecnFlowSustainTool as b where b.flowSustainToolId=a.flowSustainToolId and a.flowId=?";
			return this.listHql(hql, flowId);
		}
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description： 角色明细数据 0是角色主键ID 1角色名称 2角色职责 3图形X点 4图形Y点
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRoleByFlowId(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String hql = "select figureId,figureText,roleRes,poLongx,poLongy from JecnFlowStructureImage where  figureType in "
					+ JecnCommonSql.getRoleString() + " and  flowId=? order by poLongy";
			return this.listHql(hql, flowId);
		} else {
			String hql = "select figureId,figureText,roleRes,poLongx,poLongy from JecnFlowStructureImageT where  figureType in "
					+ JecnCommonSql.getRoleString() + " and  flowId=? order by poLongy";
			return this.listHql(hql, flowId);
		}
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description： 获取流程是横向还是纵向 0横 1纵
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public String getFlowDirectionByFlowId(Long flowId, boolean isPub) throws Exception {
		if (isPub) {
			String sql = "select IS_X_OR_Y from jecn_flow_basic_info where FLOW_ID = " + flowId;
			List<Object> list = this.listNativeSql(sql, flowId);
			if (list != null && list.size() > 0) {
				return list.get(0).toString();
			}
		} else {
			String sql = "select IS_X_OR_Y from jecn_flow_basic_info_t where FLOW_ID = " + flowId;
			List<Object> list = this.listNativeSql(sql, flowId);
			if (list != null && list.size() > 0) {
				return list.get(0).toString();
			}
		}
		return null;
	}

	/**
	 * 获取流程关联文件
	 * 
	 * @author fuzhh 2013-9-3
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<FileOpenBean> getAssociatedFileByProcessId(Long flowId, boolean isPub) throws Exception {
		List<FileOpenBean> fileOpenBeanList = new ArrayList<FileOpenBean>();
		String fileSql = null;
		if (isPub) {
			fileSql = "with FILE_TMP as (" + "   select jf.file_id,jf.savetype,jf.file_name,jf.version_id"
					+ "   from jecn_activity_file        t," + "        jecn_flow_structure_image jsi,"
					+ "        jecn_file                 jf" + "  where t.figure_id = jsi.figure_id"
					+ "    and t.file_s_id = jf.file_id and jf.del_state=0" + "    and jsi.flow_id = "
					+ flowId
					+ " UNION"
					+ " select jf.file_id,jf.savetype,jf.file_name,jf.version_id"
					+ "   from jecn_mode_file t, jecn_flow_structure_image jsi, jecn_file jf"
					+ "  where t.figure_id = jsi.figure_id"
					+ "    and t.file_m_id = jf.file_id and jf.del_state=0"
					+ "    and jsi.flow_id = "
					+ flowId
					+ " UNION"
					+ " select jf.file_id,jf.savetype,jf.file_name,jf.version_id"
					+ "   from JECN_FLOW_STANDARD a, JECN_CRITERION_CLASSES b"
					+ "   left join jecn_file jf on jf.file_id = b.related_id and jf.del_state=0"
					+ "  where a.criterion_class_id = b.criterion_class_id"
					+ "    and b.stan_type = 1"
					+ "    and a.flow_id = "
					+ flowId
					+ " UNION"
					+ " select jf.file_id,jf.savetype,jf.file_name,jf.version_id"
					+ "   from flow_related_criterion t, jecn_rule jr"
					+ "   left join jecn_file jf on jf.file_id = jr.file_id and jf.del_state=0"
					+ "  where t.criterion_class_id = jr.id"
					+ "    and jr.is_dir = 2 and jr.is_file_local=0 "
					+ "    and t.flow_id = "
					+ flowId
					+ " UNION"
					+ " select jf.file_id"
					+ " from jecn_mode_file t, jecn_flow_structure_image jsi, jecn_file jf,JECN_TEMPLET jt"
					+ " where t.figure_id = jsi.figure_id"
					+ "   and jt.file_id = jf.file_id and jf.del_state=0"
					+ "   and jt.mode_file_id = t.mode_file_id"
					+ "   and jsi.flow_id = "
					+ flowId
					+ ")"
					+ "select ft.savetype, fc.file_id, fc.file_stream, fc.file_path, ft.file_name"
					+ "  from FILE_CONTENT fc" + "  right join FILE_TMP ft on ft.version_id = fc.id";
		} else {
			fileSql = "with FILE_TMP as (" + "   select jf.file_id,jf.savetype,jf.file_name,jf.version_id"
					+ "   from jecn_activity_file_t        t," + "        jecn_flow_structure_image_t jsi,"
					+ "        jecn_file_t                 jf" + "  where t.figure_id = jsi.figure_id"
					+ "    and t.file_s_id = jf.file_id and jf.del_state=0" + "    and jsi.flow_id = "
					+ flowId
					+ " UNION"
					+ " select jf.file_id,jf.savetype,jf.file_name,jf.version_id"
					+ "   from jecn_mode_file_t t, jecn_flow_structure_image_t jsi, jecn_file_t jf"
					+ "  where t.figure_id = jsi.figure_id"
					+ "    and t.file_m_id = jf.file_id and jf.del_state=0"
					+ "    and jsi.flow_id = "
					+ flowId
					+ " UNION"
					+ " select jf.file_id,jf.savetype,jf.file_name,jf.version_id"
					+ "   from JECN_FLOW_STANDARD_t a, JECN_CRITERION_CLASSES b"
					+ "   left join jecn_file_t jf on jf.file_id = b.related_id and jf.del_state=0"
					+ "  where a.criterion_class_id = b.criterion_class_id"
					+ "    and b.stan_type = 1"
					+ "    and a.flow_id = "
					+ flowId
					+ " UNION"
					+ " select jf.file_id,jf.savetype,jf.file_name,jf.version_id"
					+ "   from flow_related_criterion_t t, jecn_rule_t jr"
					+ "   left join jecn_file_t jf on jf.file_id = jr.file_id and jf.del_state=0"
					+ "  where t.criterion_class_id = jr.id"
					+ "    and jr.is_dir = 2 and jr.is_file_local=0"
					+ "    and t.flow_id = "
					+ flowId
					+ " UNION"
					+ " select jf.file_id"
					+ " from jecn_mode_file_t t, jecn_flow_structure_image_t jsi, jecn_file_t jf,JECN_TEMPLET_T jt"
					+ " where t.figure_id = jsi.figure_id"
					+ "   and jt.file_id = jf.file_id and jf.del_state=0"
					+ "   and jt.mode_file_id = t.mode_file_id"
					+ "   and jsi.flow_id = "
					+ flowId
					+ ")"
					+ "select ft.savetype, fc.file_id, fc.file_stream, fc.file_path, ft.file_name"
					+ "  from FILE_CONTENT fc" + "  right join FILE_TMP ft on ft.version_id = fc.id";

		}
		List<Object[]> fileObj = this.listNativeSql(fileSql);
		for (Object[] obj : fileObj) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[4] == null) {
				continue;
			}
			FileOpenBean fileOpenBean = new FileOpenBean();
			fileOpenBean.setId(Long.valueOf(obj[1].toString()));
			fileOpenBean.setName(obj[4].toString());
			/** 本地 */
			if ("0".equals(obj[0].toString())) {
				if (obj[3] == null) {
					continue;
				}
				fileOpenBean.setFileByte(JecnFinal.getBytesByFilePath(obj[3].toString()));

			} else {
				if (obj[2] == null) {
					continue;
				}
				SerializableBlob sb = (SerializableBlob) obj[2];
				Blob wrapBlob = sb.getWrappedBlob();
				byte[] content = new byte[(int) wrapBlob.length()];
				InputStream in = null;
				try {
					in = wrapBlob.getBinaryStream();
					in.read(content);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				fileOpenBean.setFileByte(content);
			}
			fileOpenBeanList.add(fileOpenBean);
		}
		return fileOpenBeanList;
	}

	/**
	 * 
	 * 获取流程关联文件ID
	 * 
	 * @author fuzhh 2013-9-3
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Long> getAssociatedFileIdByProcessId(Long flowId, Long projectId, Long peopleId, boolean isPub,
			boolean isAdmin) throws Exception {

		String fileSql = null;
		if (!isPub) {
			if (isAdmin) {
				fileSql = getFlowFileSql(flowId, projectId);
			} else {
				if (JecnContants.dbType.equals(DBType.ORACLE)) {
					fileSql = getAssociatedFileIdsOracle(flowId, projectId, peopleId);
				} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					fileSql = getAssociatedFileIdsSqlServer(flowId, projectId, peopleId);
				} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
					fileSql = getAssociatedFileIdsMySql(flowId, projectId);
				}
			}
		}
		List<Number> fileObj = this.listNativeSql(fileSql);
		List<Long> fileIdList = new ArrayList<Long>();
		for (Number number : fileObj) {
			if (number == null) {
				continue;
			}
			fileIdList.add(number.longValue());
		}
		return fileIdList;
	}

	private String getFlowFileSql(Long flowId, Long projectId) {
		String fileSql;
		fileSql = "select jf.file_id    from"
				+ "         jecn_activity_file_t t,"
				+ "         jecn_flow_structure_image_t jsi,"
				+ "         jecn_file_t jf"
				+ "   where t.figure_id = jsi.figure_id"
				+ "     and t.file_s_id = jf.file_id and jf.del_state=0"
				+ "     and jf.project_id = "
				+ projectId
				+ "     and jsi.flow_id = "
				+ flowId
				+ " UNION"
				+ "  select jf.file_id    from"
				+ "         jecn_mode_file_t t,"
				+ "         jecn_flow_structure_image_t jsi,"
				+ "         jecn_file_t jf"
				+ "   where t.figure_id = jsi.figure_id"
				+ "     and t.file_m_id = jf.file_id and jf.del_state=0"
				+ "     and jf.project_id = "
				+ projectId
				+ "     and jsi.flow_id = "
				+ flowId
				+ " union "
				+ " select jf.file_id from jecn_figure_in_out_t jfio  inner join jecn_figure_in_out_sample_t sam   on jfio.id = sam.in_out_id "
				+ " inner join jecn_file_t jf on sam.file_id=jf.file_id and jf.del_state=0    and sam.type=0 and jfio.flow_id="
				+ flowId;
		return fileSql;
	}

	/**
	 * 设计器 流程相关标准化文件
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @param isPub
	 * @param isAdmin
	 * @return
	 */
	@Override
	public List<Long> getAcessStandardizedFileIds(Long flowId, Long projectId, Long peopleId, boolean isPub,
			boolean isAdmin) {
		String sql = null;
		if (!isPub) {
			if (isAdmin) {
				sql = "SELECT JF.FILE_ID" + "  FROM FLOW_STANDARDIZED_FILE_T T, JECN_FILE_T JF"
						+ " WHERE T.FILE_ID = JF.FILE_ID" + "   AND JF.DEL_STATE = 0" + "   AND JF.PROJECT_ID = "
						+ projectId + "   AND T.FLOW_ID = " + flowId;
			} else {
				if (JecnContants.dbType.equals(DBType.ORACLE)) {
					sql = getAcessStandardizedFileIdsOracle(flowId, projectId, peopleId);
				} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					sql = getAcessStandardizedFileIdsSqlServer(flowId, projectId, peopleId);
				}
			}
		}
		return this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
	}

	/**
	 * 流程制度附件
	 */
	@Override
	public List<Long> getProcessRuleFile(Long flowId, Long projectId, Long peopleId, boolean isPub, boolean isAdmin)
			throws Exception {
		String fileSql = null;
		if (!isPub) {
			if (isAdmin) {
				fileSql = "select rl.file_id " + "  from FLOW_RELATED_CRITERION_T rf, JECN_RULE_T rl "
						+ " where rf.criterion_class_id = rl.id " + "   and rl.is_dir = 2 "
						+ "   and rl.is_file_local = '0' " + "   and rf.flow_id =  " + flowId
						+ "   and rl.project_id = " + projectId;

			} else {
				if (JecnContants.dbType.equals(DBType.ORACLE)) {
					fileSql = getRuleAssociatedFileIsOracle(flowId, projectId, peopleId);
				} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					fileSql = getRuleAssociatedFileIsSQLSERVER(flowId, projectId, peopleId);
				} else if (JecnContants.dbType.equals(DBType.MYSQL)) {

				}
			}
		}
		List<Number> fileObj = this.listNativeSql(fileSql);
		List<Long> fileIdList = new ArrayList<Long>();
		for (Number number : fileObj) {
			if (number == null) {
				continue;
			}
			fileIdList.add(number.longValue());
		}
		return fileIdList;
	}

	/**
	 * 制度文件查阅权限
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @return
	 */
	private String getRuleAssociatedFileIsOracle(Long flowId, Long projectId, Long peopleId) {
		String sql = "select jf.file_id " + "  from jecn_rule_t rl, flow_related_criterion_t rf, jecn_file_t jf "
				+ " where rl.id = rf.criterion_class_id " + "   and rl.file_id = jf.file_id "
				+ "   and rl.is_file_local=0  " + "   and jf.del_state = 0 " + "   and jf.file_id in "
				+ "       (SELECT JF.file_id " + "          FROM JECN_FILE_T JF " + "         where JF.project_id ="
				+ projectId + "        CONNECT BY PRIOR JF.File_Id = JF.Per_File_Id "
				+ "         START WITH JF.File_Id in " + "                    (select B.relate_id "
				+ "                       from jecn_user_role A, jecn_role_content B "
				+ "                      where A.role_id = B.role_id " + "                        and B.type = 1 "
				+ "                        and A.people_id = " + peopleId + "))   and jf.project_id = " + projectId
				+ " " + "   and rf.flow_id =" + flowId;
		return sql;
	}

	/**
	 * 流程制度文件查阅权限
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @return
	 */
	private String getRuleAssociatedFileIsSQLSERVER(Long flowId, Long projectId, Long peopleId) {
		String sql = "WITH MY_FILE AS (SELECT JF.File_id  " + "                         FROM JECN_FILE_T JF  "
				+ "                        WHERE jf.del_state = 0  " + "                          and JF.project_id ="
				+ projectId
				+ "                          and JF.File_Id in  "
				+ "                              (select B.relate_id  "
				+ "                                 from jecn_user_role A, jecn_role_content B  "
				+ "                                where A.role_id = B.role_id  "
				+ "                                  and B.type = 1  "
				+ "                                  and A.people_id = "
				+ peopleId
				+ ")  "
				+ "                       UNION ALL  "
				+ "                       SELECT JFF.File_id  "
				+ "                         FROM MY_FILE  "
				+ "                        INNER JOIN JECN_FILE_T JFF ON MY_FILE.FILE_ID =  "
				+ "                                                      JFF.Per_File_Id)  "
				+ "       select jf.file_id  "
				+ "         from jecn_rule_t rl, flow_related_criterion_t rf, jecn_file_t jf  "
				+ "        where rl.id = rf.criterion_class_id  "
				+ "          and rl.file_id = jf.file_id  "
				+ "          and rl.is_file_local=0  "
				+ "          and jf.del_state = 0  "
				+ "          and jf.file_id in (SELECT JF.file_id FROM MY_FILE JF)  "
				+ "          and jf.project_id ="
				+ projectId + "          and rf.flow_id =" + flowId;
		return sql;
	}

	/**
	 * 流程标准附件
	 */
	@Override
	public List<Long> getProcessStandFile(Long flowId, Long projectId, Long peopleId, boolean isPub, boolean isAdmin)
			throws Exception {
		String fileSql = null;
		if (!isPub) {
			if (isAdmin) {
				fileSql = "select stand.related_id " + "  from JECN_FLOW_STANDARD_T rs, JECN_CRITERION_CLASSES stand "
						+ " where stand.criterion_class_id = rs.criterion_class_id " + "   and stand.stan_type = '1' "
						+ "   and rs.flow_id = " + flowId + "   and stand.project_id = " + projectId;
			} else {
				if (JecnContants.dbType.equals(DBType.ORACLE)) {
					fileSql = getRuleStandFileIsOracle(flowId, projectId, peopleId);

				} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					fileSql = getRuleStandFileIsSQLSERVER(flowId, projectId, peopleId);
				} else if (JecnContants.dbType.equals(DBType.MYSQL)) {

				}
			}
		}
		List<Number> fileObj = this.listNativeSql(fileSql);
		List<Long> fileIdList = new ArrayList<Long>();
		for (Number number : fileObj) {
			if (number == null) {
				continue;
			}
			fileIdList.add(number.longValue());
		}
		return fileIdList;
	}

	/**
	 * 流程标准文件查阅权限
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @return
	 */
	private String getRuleStandFileIsOracle(Long flowId, Long projectId, Long peopleId) {
		String sql = "select jf.file_id" + "  from jecn_flow_standard_t   rs," + "       jecn_criterion_classes stand,"
				+ "       jecn_file_t            jf" + " where rs.criterion_class_id = stand.criterion_class_id"
				+ "   and stand.related_id = jf.file_id" + "   and jf.del_state = 0" + "   and jf.file_id in"
				+ "       (SELECT JF.file_id" + "          FROM JECN_FILE_T JF" + "         where JF.project_id = "
				+ projectId + "        CONNECT BY PRIOR JF.File_Id = JF.Per_File_Id"
				+ "         START WITH JF.File_Id in" + "                    (select B.relate_id"
				+ "                       from jecn_user_role A, jecn_role_content B"
				+ "                      where A.role_id = B.role_id" + "                        and B.type = 1"
				+ "                        and A.people_id = " + peopleId + "))" + "   and jf.project_id = "
				+ projectId + "   and rs.flow_id =" + flowId;

		return sql;
	}

	/**
	 * 流程标准文件查阅权限
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @return
	 */
	private String getRuleStandFileIsSQLSERVER(Long flowId, Long projectId, Long peopleId) {
		String sql = "WITH MY_FILE AS (SELECT JF.File_id " + "                          FROM JECN_FILE_T JF "
				+ "                         WHERE jf.del_state = 0  "
				+ "                           and JF.project_id ="
				+ projectId
				+ "                           and JF.File_Id in  "
				+ "                               (select B.relate_id  "
				+ "                                  from jecn_user_role A, jecn_role_content B  "
				+ "                                 where A.role_id = B.role_id  "
				+ "                                   and B.type = 1  "
				+ "                                   and A.people_id = "
				+ peopleId
				+ ")  "
				+ "                        UNION ALL  "
				+ "                        SELECT JFF.File_id  "
				+ "                          FROM MY_FILE  "
				+ "                         INNER JOIN JECN_FILE_T JFF ON MY_FILE.FILE_ID =  "
				+ "                                                       JFF.Per_File_Id)  "
				+ "        select jf.file_id  "
				+ "          from jecn_flow_standard_t   rs,  "
				+ "               jecn_criterion_classes stand,  "
				+ "               jecn_file_t            jf  "
				+ "         where rs.criterion_class_id = stand.criterion_class_id  "
				+ "           and stand.related_id = jf.file_id  "
				+ "           and jf.del_state = 0  "
				+ "           and jf.file_id in (SELECT JF.file_id FROM MY_FILE JF)  "
				+ "           and jf.project_id = " + projectId + "           and rs.flow_id = " + flowId;
		return sql;
	}

	/**
	 * 制度文件本地上传
	 */
	public List<Object[]> getProcessLocalRuleFile(Long flowId, Long projectId, Long peopleId, boolean isPub,
			boolean isAdmin) throws Exception {
		String fileSql = null;
		if (!isPub) {
			if (isAdmin) {
				fileSql = "SELECT G020.ID,G020.FILE_NAME, G020.FILE_STREAM " + "  FROM FLOW_RELATED_CRITERION_T RF, "
						+ "       JECN_RULE_T              RL, " + "       G020_RULE_FILE_CONTENT   G020 "
						+ " WHERE RF.CRITERION_CLASS_ID = RL.ID " + "   AND RL.ID = G020.RULE_ID "
						+ "   AND RL.IS_DIR = 2 " + "   AND RL.IS_FILE_LOCAL = 1 AND RL.FILE_ID = G020.ID "
						+ "   AND RF.FLOW_ID =  " + flowId + "   AND RL.PROJECT_ID = " + projectId;
			} else {
				if (JecnContants.dbType.equals(DBType.ORACLE)) {
					fileSql = getProcessLocalRuleFileOracle(flowId, projectId, peopleId);
				} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					fileSql = getProcessLocalRuleFileSqlServer(flowId, projectId, peopleId);
				} else if (JecnContants.dbType.equals(DBType.MYSQL)) {

				}
			}

		}
		return this.listNativeSql(fileSql);
	}

	private String getProcessLocalRuleFileOracle(Long flowId, Long projectId, Long peopleId) {
		String sql = "SELECT G020.ID, G020.FILE_NAME, G020.FILE_STREAM" + "     FROM FLOW_RELATED_CRITERION_T RF,"
				+ "          JECN_RULE_T              RL," + "          G020_RULE_FILE_CONTENT   G020"
				+ "    WHERE RF.CRITERION_CLASS_ID = RL.ID" + "      AND RL.ID = G020.RULE_ID"
				+ "      AND RL.IS_DIR = 2" + "      AND RL.IS_FILE_LOCAL = 1" + "      AND RL.ID IN"
				+ "          (SELECT RT.ID" + "             FROM JECN_RULE_T RT"
				+ "            WHERE RT.PROJECT_ID = 1" + "           CONNECT BY PRIOR RT.ID = RT.PER_ID"
				+ "            START WITH RT.ID IN (SELECT B.RELATE_ID"
				+ "                                   FROM JECN_USER_ROLE A, JECN_ROLE_CONTENT B"
				+ "                                  WHERE A.ROLE_ID = B.ROLE_ID"
				+ "                                    AND B.TYPE = 3"
				+ "                                    AND A.PEOPLE_ID = " + peopleId + "))"
				+ "      AND RF.FLOW_ID = " + flowId + "      AND RL.PROJECT_ID = " + projectId;

		return sql;
	}

	private String getProcessLocalRuleFileSqlServer(Long flowId, Long projectId, Long peopleId) {
		String sql = "WITH MY_LOCAL_RULE AS" + " (select B.relate_id ID"
				+ "    from jecn_user_role A, jecn_role_content B" + "   where A.role_id = B.role_id"
				+ "     and B.type = 1" + "     and A.people_id = " + peopleId + "  UNION ALL" + "  SELECT RT.ID"
				+ "    FROM MY_LOCAL_RULE" + "   INNER JOIN JECN_RULE_T RT" + "      ON MY_LOCAL_RULE.ID = RT.PER_ID)"
				+ " SELECT G020.ID, G020.FILE_NAME, G020.FILE_STREAM" + "  FROM FLOW_RELATED_CRITERION_T RF,"
				+ "       JECN_RULE_T              RL," + "       G020_RULE_FILE_CONTENT   G020"
				+ " INNER JOIN MY_LOCAL_RULE" + "    ON MY_LOCAL_RULE.ID = MY_LOCAL_RULE.ID"
				+ " WHERE RF.CRITERION_CLASS_ID = RL.ID" + "   AND RL.ID = G020.RULE_ID" + "   AND RL.IS_DIR = 2"
				+ "   AND RL.IS_FILE_LOCAL = 1" + "   AND RF.FLOW_ID = " + flowId + "   AND RL.PROJECT_ID = "
				+ projectId;
		return sql;
	}

	/**
	 * 按照权限查找文件
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @return
	 */
	private String getAssociatedFileIdsMySql(Long flowId, Long projectId) {
		String sql = "select jf.file_id    from" + "         jecn_activity_file_t t,"
				+ "         jecn_flow_structure_image_t jsi," + "         jecn_file_t jf"
				+ "   where t.figure_id = jsi.figure_id" + "     and t.file_s_id = jf.file_id and jf.del_state=0"
				+ "     and jf.project_id = " + projectId + "     and jsi.flow_id = " + flowId + " UNION"
				+ "  select jf.file_id    from" + "         jecn_mode_file_t t,"
				+ "         jecn_flow_structure_image_t jsi," + "         jecn_file_t jf"
				+ "   where t.figure_id = jsi.figure_id" + "     and t.file_m_id = jf.file_id and jf.del_state=0"
				+ "     and jf.project_id = " + projectId + "     and jsi.flow_id = " + flowId;
		return sql;
	}

	/**
	 * 按照权限查找文件
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @return
	 */
	private String getAssociatedFileIdsSqlServer(Long flowId, Long projectId, Long peopleId) {
		String sql = "WITH MY_FILE AS(SELECT JF.File_id" + "  FROM JECN_FILE_T JF"
				+ "  WHERE jf.del_state=0 and JF.File_Id in (select B.relate_id"
				+ "                        from jecn_user_role A, jecn_role_content B"
				+ "                       where A.role_id = B.role_id" + "                         and B.type = 1"
				+ "                         and A.people_id = " + peopleId + ")" + " select a.file_id from ( "
				+ getFlowFileSql(flowId, projectId) + ")a " + " INNER JOIN MY_FILE F ON A.FILE_ID=F.FILE_ID";
		return sql;
	}

	/**
	 * 按照权限查找文件
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @return
	 */
	private String getAssociatedFileIdsOracle(Long flowId, Long projectId, Long peopleId) {
		String sql = "select a.file_id from (" + getFlowFileSql(flowId, projectId) + ")a where a.file_id in "
				+ "(SELECT JF.file_id" + "          FROM JECN_FILE_T JF" + "         where JF.project_id = "
				+ projectId + "        CONNECT BY PRIOR JF.File_Id = JF.Per_File_Id"
				+ "         START WITH JF.File_Id in (select B.relate_id       from jecn_user_role A,"
				+ "                                          jecn_role_content B"
				+ "                                    where A.role_id = B.role_id"
				+ "                                      and B.type = 1"
				+ "                                      and A.people_id = " + peopleId + "))";
		return sql;
	}

	/**
	 * 标准化文件
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @return
	 */
	private String getAcessStandardizedFileIdsOracle(Long flowId, Long projectId, Long peopleId) {
		return "SELECT JF.FILE_ID" + "  FROM FLOW_STANDARDIZED_FILE_T T, JECN_FILE_T JF"
				+ " WHERE T.FILE_ID = JF.FILE_ID" + "   AND JF.DEL_STATE = 0" + "   AND JF.FILE_ID IN"
				+ "       (SELECT JF.FILE_ID" + "          FROM JECN_FILE_T JF" + "         WHERE JF.PROJECT_ID = "
				+ projectId + "" + "        CONNECT BY PRIOR JF.FILE_ID = JF.PER_FILE_ID"
				+ "         START WITH JF.FILE_ID IN" + "                    (SELECT B.RELATE_ID"
				+ "                       FROM JECN_USER_ROLE A, JECN_ROLE_CONTENT B"
				+ "                      WHERE A.ROLE_ID = B.ROLE_ID" + "                        AND B.TYPE = 1"
				+ "                        AND A.PEOPLE_ID = " + peopleId + "))" + "   AND JF.PROJECT_ID = 1"
				+ "   AND T.FLOW_ID = " + flowId;
	}

	private String getAcessStandardizedFileIdsSqlServer(Long flowId, Long projectId, Long peopleId) {
		return "WITH MY_FILE AS (SELECT JF.File_id" + "                          FROM JECN_FILE_T JF"
				+ "                         WHERE jf.del_state = 0" + "                           and JF.File_Id in"
				+ "                               (select B.relate_id"
				+ "                                  from jecn_user_role A, jecn_role_content B"
				+ "                                 where A.role_id = B.role_id"
				+ "                                   and B.type = 1"
				+ "                                   and A.people_id =  " + peopleId + ")"
				+ "                        UNION ALL" + "                        SELECT JFF.File_id"
				+ "                          FROM MY_FILE" + "                         INNER JOIN JECN_FILE_T JFF"
				+ "                            ON MY_FILE.FILE_ID = JFF.Per_File_Id)" + ""
				+ "        SELECT JF.FILE_ID" + "          FROM FLOW_STANDARDIZED_FILE_T T, MY_FILE JF"
				+ "         WHERE T.FILE_ID = JF.FILE_ID  AND T.FLOW_ID = " + flowId;

	}

	/**
	 * 获取流程检错信息
	 * 
	 * @author fuzhh 2013-9-9
	 * @param processCheckType
	 *            检错类型
	 * @return
	 * @throws Exception
	 */
	public List<ProcessBaseInfoBean> getProcessCheckList(long processCheckType, int start, int limit, Long projectId)
			throws Exception {
		// 流程检错结果集合
		List<ProcessBaseInfoBean> processCheckList = new ArrayList<ProcessBaseInfoBean>();
		String sql = "";
		if (processCheckType == -1) { // 全部
			sql = ProcessSqlConstant.notThoseResponsible(projectId) + "  union  "
					+ ProcessSqlConstant.notTheDepartment(projectId) + "  union  "
					+ ProcessSqlConstant.roleCorrPos(projectId) + " ORDER BY 1";
		} else if (processCheckType == 0) {// 流程责任人存在的数据
			sql = ProcessSqlConstant.notThoseResponsible(projectId) + " ORDER BY 1";
		} else if (processCheckType == 1) { // 责任部门不存在
			sql = ProcessSqlConstant.notTheDepartment(projectId) + " ORDER BY 1";
		} else if (processCheckType == 2) { // 角色没有对应岗位
			sql = ProcessSqlConstant.roleCorrPos(projectId) + " ORDER BY 1";
		}
		List<Object[]> objList = this.listNativeSql(sql, start, limit);
		for (Object[] obj : objList) {
			if (obj[0] == null || obj[1] == null) {
				continue;
			}
			ProcessBaseInfoBean processBaseInfoBean = new ProcessBaseInfoBean();
			processBaseInfoBean.setFlowId(Long.valueOf(obj[0].toString()));
			processBaseInfoBean.setFlowName(obj[1].toString());
			if (obj[2] != null) {
				processBaseInfoBean.setFlowIdInput(obj[2].toString());
			}
			processCheckList.add(processBaseInfoBean);
		}
		return processCheckList;
	}

	/**
	 * 获取流程检错信息大小
	 * 
	 * @author fuzhh 2013-9-9
	 * @param processCheckType
	 *            检错类型
	 * @return
	 * @throws Exception
	 */
	public int getProcessCheckCount(long processCheckType, Long projectId) throws Exception {
		String sql = "";
		if (processCheckType == -1) { // 全部
			sql = "select count(*) from (" + ProcessSqlConstant.notThoseResponsible(projectId) + "  union  "
					+ ProcessSqlConstant.notTheDepartment(projectId) + "  union  "
					+ ProcessSqlConstant.roleCorrPos(projectId) + ") countSum";
		} else if (processCheckType == 0) {// 流程责任人存在的数据
			sql = ProcessSqlConstant.notThoseResponsibleCount(projectId);
		} else if (processCheckType == 1) { // 责任部门不存在
			sql = ProcessSqlConstant.notTheDepartmentCount(projectId);
		} else if (processCheckType == 2) { // 角色没有对应岗位
			sql = ProcessSqlConstant.roleCorrPosCount(projectId);
		}
		return this.countAllByParamsNativeSql(sql);
	}

	/**
	 * 获取流程地图附件集合
	 * 
	 * @param flowId
	 * @return List<JecnFigureFileTBean>
	 */
	@Override
	public List<JecnFigureFileTBean> findJecnFigureFileTList(Long flowId) throws Exception {
		// String hql =
		// "select a from JecnFigureFileTBean as a,JecnFlowStructureImageT as b
		// "
		// + "where a.figureId=b.figureId and b.flowId=?";
		String sql = "SELECT FF.ID, FF.FIGURE_ID, FF.FILE_ID, FF.FIGURE_TYPE, FT.FILE_NAME,FF.FIGURE_UUID"
				+ "  FROM JECN_FIGURE_FILE_T FF, JECN_FILE_T FT" + " WHERE FF.FILE_ID = FT.FILE_ID"
				+ "   AND EXISTS (SELECT 1" + "          FROM JECN_FLOW_STRUCTURE_IMAGE_T IT"
				+ "         WHERE FF.FIGURE_ID = IT.FIGURE_ID" + "           AND IT.FLOW_ID = ?)";
		List<Object[]> list = this.listNativeSql(sql, flowId);

		List<JecnFigureFileTBean> fileTBeans = new ArrayList<JecnFigureFileTBean>();
		JecnFigureFileTBean figureFileTBean = null;
		for (Object[] objects : list) {
			if (objects[0] == null || objects[1] == null || objects[2] == null || objects[3] == null
					|| objects[4] == null) {
				continue;
			}
			figureFileTBean = new JecnFigureFileTBean();
			figureFileTBean.setId(objects[0].toString());
			// 元素ID
			figureFileTBean.setFigureId(Long.valueOf(objects[1].toString()));
			// 文件ID
			figureFileTBean.setFileId(Long.valueOf(objects[2].toString()));
			// 元素类型
			figureFileTBean.setFigureType(objects[3].toString());
			// 文件名称
			figureFileTBean.setFileName(objects[4].toString());
			if (objects[5] != null) {
				figureFileTBean.setFigureUUID(objects[5].toString());
			}
			fileTBeans.add(figureFileTBean);
		}
		return fileTBeans;
	}

	/**
	 * 获取流程所有活动的线上信息集合
	 * 
	 * @param flowId
	 * @return List<JecnActiveOnLineTBean>
	 * @throws Exception
	 */
	@Override
	public List<JecnActiveOnLineTBean> findActiveOnLineTBeans(Long flowId, boolean isPub) throws Exception {
		String strPub = null;
		if (isPub) {
			strPub = "";
		} else {
			strPub = "_T";
		}
		String sql = "SELECT OT.ID," + "       OT.PROCESS_ID," + "       OT.ACTIVE_ID," + "       OT.TOOL_ID,"
				+ "       OT.ONLINE_TIME,"
				+ "       ST.FLOW_SUSTAIN_TOOL_DESCRIBE,OT.INFORMATION_DESCRIPTION,OT.FIGURE_UUID,OT.LINK_URL"
				+ "  FROM JECN_ACTIVE_ONLINE" + strPub + " OT, JECN_FLOW_SUSTAIN_TOOL ST"
				+ " WHERE OT.TOOL_ID = ST.FLOW_SUSTAIN_TOOL_ID" + "   AND OT.PROCESS_ID =?";
		List<Object[]> list = this.listNativeSql(sql, flowId);
		List<JecnActiveOnLineTBean> listOnLineTBean = new ArrayList<JecnActiveOnLineTBean>();
		JecnActiveOnLineTBean activeOnLineTBean = null;
		for (Object[] objects : list) {
			if (objects[0] == null || objects[1] == null || objects[2] == null || objects[3] == null
					|| objects[4] == null || objects[5] == null) {
				return new ArrayList<JecnActiveOnLineTBean>();
			}
			activeOnLineTBean = new JecnActiveOnLineTBean();
			activeOnLineTBean.setId(objects[0].toString());
			activeOnLineTBean.setProcessId(Long.valueOf(objects[1].toString()));
			activeOnLineTBean.setActiveId(Long.valueOf(objects[2].toString()));
			activeOnLineTBean.setToolId(Long.valueOf(objects[3].toString()));
			activeOnLineTBean.setOnLineTime(JecnCommon.getDateByString(objects[4].toString()));
			activeOnLineTBean.setSysName(objects[5].toString());
			if (objects[6] != null) {
				activeOnLineTBean.setInformationDescription(objects[6].toString());
			} else {
				activeOnLineTBean.setInformationDescription("");
			}
			activeOnLineTBean.setFigureUUID(objects[7] == null ? null : objects[7].toString());
			
			activeOnLineTBean.setLinkUrl(objects[8] == null ? null : objects[8].toString());
			listOnLineTBean.add(activeOnLineTBean);
		}
		return listOnLineTBean;
	}

	/**
	 * 活动相关标准集合
	 * 
	 * @param flowId
	 *            流程主键ID
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnActiveStandardBeanT> findActiveStandardBeanTs(Long flowId) throws Exception {
		String sql = "SELECT AT.ID,AT.ACTIVE_ID," + "       AT.CLAUSE_ID, AT.RELA_TYPE,"
				+ "       AT.STANDARD_ID,CC.CRITERION_CLASS_NAME,AT.figure_uuid" + "  FROM JECN_ACTIVE_STANDARD_T      AT,"
				+ "       JECN_FLOW_STRUCTURE_IMAGE_T IT," + "       JECN_CRITERION_CLASSES      CC"
				+ " WHERE AT.STANDARD_ID = CC.CRITERION_CLASS_ID" + "   AND AT.ACTIVE_ID = IT.FIGURE_ID"
				+ "   AND IT.FLOW_ID = ?";
		List<Object[]> list = this.listNativeSql(sql, flowId);

		List<JecnActiveStandardBeanT> listStandardT = new ArrayList<JecnActiveStandardBeanT>();
		for (Object[] objects : list) {
			if (objects[0] == null || objects[1] == null || objects[3] == null || objects[4] == null
					|| objects[5] == null) {
				continue;
			}
			JecnActiveStandardBeanT standardBeanT = new JecnActiveStandardBeanT();
			standardBeanT.setId(objects[0].toString());
			standardBeanT.setActiveId(Long.valueOf(objects[1].toString()));
			if (objects[2] != null) {// 存在条款要求父节点ID
				standardBeanT.setClauseId(Long.valueOf(objects[2].toString()));
			}
			// 关联标准或条款要求主键 ID(标准表主键ID)
			standardBeanT.setRelaType(Integer.parseInt(objects[3].toString()));
			standardBeanT.setStandardId(Long.valueOf(objects[4].toString()));
			standardBeanT.setRelaName(objects[5].toString());
			standardBeanT.setFigureUUID(objects[6] == null ? null : objects[6].toString());
			listStandardT.add(standardBeanT);
		}
		return listStandardT;
	}

	/**
	 * 获取流程中活动相关控制点
	 * 
	 * @param flowId
	 * @return List<JecnControlPointT> 控制点集合
	 * @throws Exception
	 */
	@Override
	public List<JecnControlPointT> findControlPointTs(Long flowId) throws Exception {
		String sql = "SELECT CP.ID," + "       CP.ACTIVE_ID," + "       CP.CONTROL_CODE," + "       CP.KEY_POINT,"
				+ "       CP.FREQUENCY," + "       CP.METHOD," + "       CP.TYPE," + "       CP.RISK_ID,"
				+ "       CP.TARGET_ID," + "       CP.CREATE_PERSON," + "       CP.CREATE_TIME,"
				+ "       CP.UPDATE_PERSON," + "       CP.UPDATE_TIME," + "       CP.NOTE"
				+ "  FROM JECN_CONTROL_POINT_T CP, JECN_FLOW_STRUCTURE_IMAGE_T IT"
				+ " WHERE CP.ACTIVE_ID = IT.FIGURE_ID" + "   AND IT.FLOW_ID = ?";
		List<Object[]> list = this.listNativeSql(sql, flowId);
		List<JecnControlPointT> listConPoints = new ArrayList<JecnControlPointT>();
		for (Object[] objects : list) {
			if (objects[0] == null || objects[1] == null || objects[2] == null || objects[3] == null
					|| objects[4] == null || objects[6] == null || objects[7] == null || objects[8] == null
					|| objects[9] == null || objects[10] == null || objects[11] == null || objects[12] == null) {
				return new ArrayList<JecnControlPointT>();
			}

			JecnControlPointT controlPointT = new JecnControlPointT();
			controlPointT.setId(objects[0].toString());
			controlPointT.setActiveId(Long.valueOf(objects[1].toString()));
			controlPointT.setControlCode(objects[2].toString());
			controlPointT.setKeyPoint(Integer.parseInt(objects[3].toString()));
			controlPointT.setFrequency(Integer.parseInt(objects[4].toString()));
			controlPointT.setMethod(Integer.parseInt(objects[5].toString()));
			controlPointT.setType(Integer.parseInt(objects[6].toString()));
			controlPointT.setRiskId(Long.valueOf(objects[7].toString()));
			controlPointT.setTargetId(Long.valueOf(objects[8].toString()));
			controlPointT.setCreatePersonId(Long.valueOf(objects[9].toString()));
			controlPointT.setCreateTime(JecnCommon.getDateByString(objects[10].toString()));
			controlPointT.setUpdatePersonId(Long.valueOf(objects[11].toString()));
			controlPointT.setUpdateTime(JecnCommon.getDateByString(objects[12].toString()));
			if (objects[13] != null) {// 存在备注
				controlPointT.setNote(objects[13].toString());
			}
			listConPoints.add(controlPointT);
		}
		return listConPoints;
	}

	/**
	 * 获取活动对应的控制点相关信息
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 *             List<JecnControlPointT>
	 */
	@Override
	public List<JecnControlPointT> findListControlPointTAll(Long flowId) throws Exception {
		String sql = "SELECT CP.ID," + "       CP.ACTIVE_ID," + "       CP.CONTROL_CODE," + "       CP.KEY_POINT,"
				+ "       CP.FREQUENCY," + "       CP.METHOD," + "       CP.TYPE," + "       CP.RISK_ID,"
				+ "       CP.TARGET_ID," + "       CP.CREATE_PERSON," + "       CP.CREATE_TIME,"
				+ "       CP.UPDATE_PERSON," + "       CP.UPDATE_TIME," + "       CP.NOTE," + "       JR.RISK_CODE,"
				+ "       JC.DESCRIPTION,CP.figure_uuid" + "  FROM JECN_CONTROL_POINT_T        CP,"
				+ "       JECN_FLOW_STRUCTURE_IMAGE_T IT," + "       JECN_RISK                   JR,"
				+ "       JECN_CONTROLTARGET          JC" + " WHERE CP.ACTIVE_ID = IT.FIGURE_ID"
				+ "   AND CP.TARGET_ID = JC.ID" + "   AND JR.ID = JC.RISK_ID" + "   AND IT.FLOW_ID = ?";

		List<Object[]> list = this.listNativeSql(sql, flowId);
		List<JecnControlPointT> listConPoints = new ArrayList<JecnControlPointT>();
		for (Object[] objects : list) {
			if (objects[0] == null || objects[1] == null || objects[2] == null || objects[3] == null
					|| objects[4] == null || objects[6] == null || objects[7] == null || objects[8] == null
					|| objects[9] == null || objects[10] == null || objects[11] == null || objects[12] == null) {
				return new ArrayList<JecnControlPointT>();
			}

			JecnControlPointT controlPointT = new JecnControlPointT();
			controlPointT.setId(objects[0].toString());
			controlPointT.setActiveId(Long.valueOf(objects[1].toString()));
			controlPointT.setControlCode(objects[2].toString());
			controlPointT.setKeyPoint(Integer.parseInt(objects[3].toString()));
			controlPointT.setFrequency(Integer.parseInt(objects[4].toString()));
			controlPointT.setMethod(Integer.parseInt(objects[5].toString()));
			controlPointT.setType(Integer.parseInt(objects[6].toString()));
			controlPointT.setRiskId(Long.valueOf(objects[7].toString()));
			controlPointT.setTargetId(Long.valueOf(objects[8].toString()));
			controlPointT.setCreatePersonId(Long.valueOf(objects[9].toString()));
			controlPointT.setCreateTime(JecnCommon.getDateByString(objects[10].toString()));
			controlPointT.setUpdatePersonId(Long.valueOf(objects[11].toString()));
			controlPointT.setUpdateTime(JecnCommon.getDateByString(objects[12].toString()));
			if (objects[13] != null) {// 存在备注
				controlPointT.setNote(objects[13].toString());
			}
			if (objects[14] != null) {// 存在备注
				controlPointT.setRiskNum(objects[14].toString());
			}
			if (objects[15] != null) {// 存在备注
				controlPointT.setControlTarget(objects[15].toString());
			}
			controlPointT.setFigureUUID(objects[16] == null ? null : objects[16].toString());
			listConPoints.add(controlPointT);
		}
		return listConPoints;
	}

	/**
	 * 根据流程ID查询对应风险
	 * 
	 * @author fuzhh 2013-11-27
	 * @param flowId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnRisk> getRiskByFlowId(Long flowId, boolean isPub) throws Exception {
		String sql;
		if (isPub) {
			sql = "select distinct jr.*" + "  from JECN_RISK jr"
					+ " inner join JECN_CONTROL_POINT jcpt on jr.id = jcpt.Risk_Id"
					+ " inner join JECN_FLOW_STRUCTURE_IMAGE jfsit on jcpt.Active_Id =" + " jfsit.figure_id"
					+ " where jfsit.flow_id=" + flowId;
		} else {
			sql = "select distinct jr.*" + "  from JECN_RISK jr"
					+ " inner join JECN_CONTROL_POINT_T jcpt on jr.id = jcpt.Risk_Id"
					+ " inner join JECN_FLOW_STRUCTURE_IMAGE_T jfsit on jcpt.Active_Id =" + " jfsit.figure_id"
					+ " where jfsit.flow_id=" + flowId;
		}
		List<JecnRisk> jecnRiskList = this.getSession().createSQLQuery(sql).addEntity(JecnRisk.class).list();
		// 控制点
		// if (!jecnRiskList.isEmpty()) {
		// Set<Long> idSet = new HashSet<Long>();
		// for (JecnRisk risk : jecnRiskList) {
		// idSet.add(risk.getId());
		// }
		// String ids = JecnCommonSql.getIdsSet(idSet);
		// String hql = "from JecnControlPoint where riskId in " + ids;
		// List<JecnControlPoint> points = this.listHql(hql);
		// if(points!=null){
		// Map<Long, List<JecnControlPoint>> riksToPointMap = new HashMap<Long,
		// List<JecnControlPoint>>();
		// for (JecnControlPoint point : points) {
		// List<JecnControlPoint> list = riksToPointMap.get(point.getRiskId());
		// if (list == null) {
		// list = new ArrayList<JecnControlPoint>();
		// }
		// list.add(point);
		// riksToPointMap.put(point.getRiskId(), list);
		// }
		// for (JecnRisk risk : jecnRiskList) {
		//					
		// }
		// }
		// }
		return jecnRiskList;
	}

	/**
	 * 活动明细数据 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6流程ID
	 * 
	 * @author fuzhh 2013-12-4
	 * @param flowSet
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getActiveShowBySet(Set<Long> flowSet, boolean isPub) throws Exception {
		if (flowSet == null || flowSet.size() <= 0) {
			return new ArrayList<Object[]>();
		}
		String sql = "";
		if (isPub) {
			sql = "select figure_Id,activity_Id,figure_Text,activity_Show,lineColor,ROLE_RES,flow_id from JECN_FLOW_STRUCTURE_IMAGE where  figure_Type in "
					+ JecnCommonSql.getActiveString() + " and  flow_Id in ";
		} else {
			sql = "select figure_Id,activity_Id,figure_Text,activity_Show,lineColor,ROLE_RES from JECN_FLOW_STRUCTURE_IMAGE_T where  figure_Type in "
					+ JecnCommonSql.getActiveString() + " and  flow_Id in " + JecnCommonSql.getIdsSet(flowSet);
		}
		List<String> lsit = JecnCommonSql.getSetSqls(sql, flowSet);
		List<Object[]> objList = new ArrayList<Object[]>();
		for (String strSql : lsit) {
			objList.addAll(this.getSession().createSQLQuery(strSql).list());
		}
		return objList;
	}

	@Override
	public List<Object[]> getProcessRules(Set<Long> ids) throws Exception {
		StringBuffer sql = new StringBuffer(
				"select  frc.flow_id,r.id,r.rule_name,r.is_dir,jf.file_id  "
						+ " from FLOW_RELATED_CRITERION frc, JECN_RULE  r "
						+ " left join jecn_file jf on jf.del_state=0 and r.is_dir=2 and r.is_file_local=0 and r.FILE_ID=jf.file_id"
						+ " where frc.criterion_class_id=r.id and frc.flow_id in");
		List<String> listStr = JecnCommonSql.getSetSqls(sql.toString(), ids);
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (String str : listStr) {
			List<Object[]> listObj = this.listNativeSql(str);
			for (Object[] obj : listObj) {
				if (obj != null) {
					listResult.add(obj);
				}
			}
		}
		return listResult;
	}

	@Override
	public List<Object[]> getProcessStandards(Set<Long> ids) throws Exception {

		String sql = "select frc.flow_id,c.criterion_class_id,c.criterion_class_name,c.stan_type,JF.FILE_ID "
				+ " from  JECN_FLOW_STANDARD frc, JECN_CRITERION_CLASSES  c "
				+ " LEFT jOIN JECN_FILE JF ON JF.DEL_STATE=0 AND C.stan_type=1 and c.RELATED_ID=JF.FILE_ID"
				+ " where frc.criterion_class_id=c.criterion_class_id and frc.flow_id in";
		List<String> listStr = JecnCommonSql.getSetSqls(sql, ids);
		sql = "select si.flow_id, c.criterion_class_id, c.criterion_class_name,c.stan_type,JF.FILE_ID from "
				+ "            JECN_FLOW_STRUCTURE_IMAGE si," + " JECN_ACTIVE_STANDARD jas,"
				+ " JECN_CRITERION_CLASSES c "
				+ " LEFT jOIN JECN_FILE JF ON JF.DEL_STATE=0 AND C.stan_type=1 and c.RELATED_ID=JF.FILE_ID"
				+ " where jas.active_id = si.figure_id" + "   and si.figure_type in" + JecnCommonSql.getActiveString()
				+ "   and jas.standard_id = c.criterion_class_id" + "   and si.flow_id in";
		List<String> listStr2 = JecnCommonSql.getSetSqls(sql, ids);
		listStr.addAll(listStr2);
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (String str : listStr) {
			List<Object[]> listObj = this.listNativeSql(str);
			for (Object[] obj : listObj) {
				if (obj != null) {
					listResult.add(obj);
				}
			}
		}

		return listResult;
	}

	@Override
	public List<Object[]> getProcessRisks(Set<Long> ids) throws Exception {
		StringBuffer sql = new StringBuffer(
				" select f.flow_id, r.id,r.name,r.risk_code from  JECN_FLOW_BASIC_INFO  f,JECN_FLOW_STRUCTURE_IMAGE i,JECN_CONTROL_POINT p,JECN_RISK r where f.flow_id=i.flow_id and p.active_id=i.figure_id and p.risk_id=r.id and f.flow_id in");
		List<String> listStr = JecnCommonSql.getSetSqls(sql.toString(), ids);
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (String str : listStr) {
			List<Object[]> listObj = this.listNativeSql(str);
			for (Object[] obj : listObj) {
				if (obj != null) {
					listResult.add(obj);
				}
			}
		}
		return listResult;
	}

	/**
	 * 查询单个流程对应标准
	 * 
	 * @author fuzhh 2013-12-20
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowStandards(Long flowId, boolean isPub) throws Exception {
		String strPub = null;
		if (isPub) {
			strPub = "";
		} else {
			strPub = "_T";
		}
		String sql = "SELECT C.CRITERION_CLASS_ID," + "       C.CRITERION_CLASS_NAME," + "       C.RELATED_ID,"
				+ "       C.STAN_TYPE" + "  FROM JECN_FLOW_STANDARD"
				+ strPub
				+ " FRC, JECN_CRITERION_CLASSES C"
				+ " WHERE FRC.CRITERION_CLASS_ID = C.CRITERION_CLASS_ID"
				+ "   AND C.STAN_TYPE <> 1"
				+ "   AND FRC.FLOW_ID = ?"
				+ " UNION "
				+ "SELECT C.CRITERION_CLASS_ID,"
				+ "       C.CRITERION_CLASS_NAME,"
				+ "       C.RELATED_ID,"
				+ "       C.STAN_TYPE"
				+ "  FROM JECN_FLOW_STANDARD"
				+ strPub
				+ " FRC, JECN_CRITERION_CLASSES C, JECN_FILE"
				+ strPub
				+ " JF"
				+ " WHERE FRC.CRITERION_CLASS_ID = C.CRITERION_CLASS_ID"
				+ "   AND C.STAN_TYPE = 1"
				+ "   AND JF.FILE_ID = C.RELATED_ID"
				+ "   AND JF.DEL_STATE = 0"
				+ "   AND FRC.FLOW_ID = ?"
				+ " UNION "
				+ "SELECT C.CRITERION_CLASS_ID,"
				+ "       C.CRITERION_CLASS_NAME,"
				+ "       C.RELATED_ID,"
				+ "       C.STAN_TYPE"
				+ "  FROM JECN_ACTIVE_STANDARD"
				+ strPub
				+ "      JAS,"
				+ "       JECN_FLOW_STRUCTURE_IMAGE"
				+ strPub
				+ " SI,"
				+ "       JECN_CRITERION_CLASSES    C"
				+ " WHERE JAS.ACTIVE_ID = SI.FIGURE_ID"
				+ "   AND SI.FIGURE_TYPE IN ('Rhombus',"
				+ "                          'ITRhombus',"
				+ "                          'DataImage',"
				+ "                          'RoundRectWithLine',"
				+ "                          'ActiveFigureAR',"
				+ "                          'ExpertRhombusAR')"
				+ "   AND JAS.STANDARD_ID = C.CRITERION_CLASS_ID"
				+ "   AND SI.FLOW_ID = ?";
		return this.listNativeSql(sql, flowId, flowId, flowId);
	}

	/**
	 * 
	 * 农夫三泉操作说明下载
	 * 
	 * 查询单个流程对应标准
	 * 
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowStandardsNFSQ(Long flowId, boolean isPub) throws Exception {
		String sql = "";
		if (isPub) {
			sql = "SELECT C.STAN_TYPE,  CASE WHEN C.STAN_TYPE=1 THEN FL.DOC_ID ELSE NULL END AS STAND_NUM,"
					+ "  CASE WHEN C.STAN_TYPE=1 THEN FL.FILE_NAME ELSE C.CRITERION_CLASS_NAME END AS STAND_NAME,"
					+ "  CASE WHEN C.STAN_TYPE=1 THEN O1.ORG_NAME ELSE NULL END DUTYORG"
					+ " FROM JECN_FLOW_STANDARD FRC"
					+ "  LEFT JOIN  JECN_CRITERION_CLASSES C ON FRC.CRITERION_CLASS_ID = C.CRITERION_CLASS_ID"
					+ "  LEFT JOIN JECN_FILE FL ON FL.FILE_ID =C.RELATED_ID AND C.STAN_TYPE=1 and FL.del_state=0"
					+ "  LEFT JOIN JECN_FLOW_ORG O1 ON O1.ORG_ID=FL.ORG_ID" + " WHERE FRC.FLOW_ID = ?";
		} else {
			sql = "SELECT C.STAN_TYPE,  CASE WHEN C.STAN_TYPE=1 THEN FL.DOC_ID ELSE NULL END AS STAND_NUM,"
					+ "  CASE WHEN C.STAN_TYPE=1 THEN FL.FILE_NAME ELSE C.CRITERION_CLASS_NAME END AS STAND_NAME,"
					+ "  CASE WHEN C.STAN_TYPE=1 THEN O1.ORG_NAME ELSE NULL END DUTYORG"
					+ "  FROM JECN_FLOW_STANDARD_T FRC"
					+ "  LEFT JOIN  JECN_CRITERION_CLASSES C ON FRC.CRITERION_CLASS_ID = C.CRITERION_CLASS_ID"
					+ "  LEFT JOIN JECN_FILE_T FL ON FL.FILE_ID =C.RELATED_ID AND C.STAN_TYPE=1 and FL.del_state=0"
					+ "  LEFT JOIN JECN_FLOW_ORG O1 ON O1.ORG_ID=FL.ORG_ID" + "  WHERE FRC.FLOW_ID = ?";
		}
		return this.listNativeSql(sql, flowId);
	}

	/**
	 * 查询单个流程对应标准农夫山泉
	 * 
	 * @author fuzhh 2013-12-20
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getNongfushanquan_FlowStandards(Long flowId, boolean isPub) throws Exception {
		// 标准类型 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
		// 设计器关联标准的时候只能选择文件和 标准条款和条款要求 故sql只查询了文件的部门
		String sql = "";
		if (isPub) {
			sql = "select   " + "case when c.Stan_Type=1 then fl.doc_id else null end as stand_num,   "
					+ "case when c.Stan_Type=1 then fl.file_name else c.criterion_class_name end as stand_name,   "
					+ "case when c.Stan_Type=1 then o1.org_name else null end dutyOrg   "
					+ "from JECN_FLOW_STANDARD frc   "
					+ "left join  JECN_CRITERION_CLASSES c on frc.criterion_class_id = c.criterion_class_id   "
					+ "left join jecn_file fl on fl.file_id =c.related_id and c.Stan_Type=1 and fl.del_state=0"
					+ "left join jecn_flow_org o1 on o1.org_id=fl.org_id   " + "where frc.flow_id =" + flowId;
		} else {
			sql = "select  " + "case when c.Stan_Type=1 then fl.doc_id else null end as stand_num,  "
					+ "case when c.Stan_Type=1 then fl.file_name else c.criterion_class_name end as stand_name,  "
					+ "case when c.Stan_Type=1 then o1.org_name else null end dutyOrg  "
					+ "from JECN_FLOW_STANDARD_T frc  "
					+ "left join  JECN_CRITERION_CLASSES c on frc.criterion_class_id = c.criterion_class_id  "
					+ "left join jecn_file_t fl on fl.file_id =c.related_id and c.Stan_Type=1 and fl.del_state=0"
					+ "left join jecn_flow_org o1 on o1.org_id=fl.org_id  " + "where frc.flow_id =" + flowId;

		}
		List<Object[]> listObjOne = this.listNativeSql(sql);
		return listObjOne;
	}

	/**
	 * 查询选中的流程地图的父节点与子节点
	 * 
	 * @author huoyl
	 * @param mIds
	 *            流程地图id的集合
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @return list
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getProcessDetailProcessMapList(String mIds, long projectId) throws Exception {

		String sql = "";

		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "select jfst.flow_id,jfst.flow_name,jfst.pre_flow_id,jfst.flow_id_input,jfst.isflow"
					+ " from JECN_FLOW_STRUCTURE_T jfst" + " where  jfst.del_state = 0" + " and projectId=" + projectId
					+ " CONNECT BY  PRIOR  jfst.flow_id=jfst.pre_flow_id" + " START WITH jfst.flow_id in (" + mIds
					+ ")" + " union "
					+ "select jfst.flow_id,jfst.flow_name,jfst.pre_flow_id,jfst.flow_id_input,jfst.isflow"
					+ " from JECN_FLOW_STRUCTURE_T jfst" + " where  jfst.del_state = 0" + " and projectId=" + projectId
					+ " CONNECT BY  PRIOR  jfst.pre_flow_id=jfst.flow_id" + " START WITH jfst.flow_id in (" + mIds
					+ ")";

		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {

			sql = " WITH MY_FLOW_N AS" // 查找子节点
					+ " (SELECT JFT.flow_id,JFT.flow_name,JFT.pre_flow_id,JFT.flow_id_input,jft.isflow"
					+ " FROM JECN_FLOW_STRUCTURE_T JFT" + " WHERE JFT.Flow_Id in ("
					+ mIds
					+ ")"
					+ " UNION ALL SELECT jfs.flow_id,jfs.flow_name,jfs.pre_flow_id,jfs.flow_id_input,jfs.isflow"
					+ " FROM MY_FLOW_N"
					+ " INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW_N.Pre_Flow_Id= JFS.Flow_Id"
					+ " where  JFS.del_state = 0"
					+ "	and JFS.projectId="
					+ projectId
					+ " ),"
					+ " MY_FLOW_P AS" // 查找父节点
					+ "  (SELECT JFT.flow_id,JFT.flow_name,JFT.pre_flow_id,JFT.flow_id_input,jft.isflow"
					+ "   FROM JECN_FLOW_STRUCTURE_T JFT"
					+ "   WHERE JFT.Flow_Id in ("
					+ mIds
					+ ")"
					+ "   UNION ALL SELECT jfs.flow_id,jfs.flow_name,jfs.pre_flow_id,jfs.flow_id_input,jfs.isflow"
					+ "   FROM MY_FLOW_P"
					+ "  INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW_P.Flow_Id=JFS.Pre_Flow_Id"
					+ " where  JFS.del_state = 0"
					+ "					 and JFS.projectId="
					+ projectId
					+ ")"
					+ " SELECT flow_id,flow_name,pre_flow_id,flow_id_input,isflow FROM MY_FLOW_N"
					+ "	UNION "
					+ " SELECT flow_id,flow_name,pre_flow_id,flow_id_input,isflow FROM MY_FLOW_P";
		}

		return this.listNativeSql(sql);
	}

	/**
	 * 查询选中流程地图下的所有流程详细信息
	 * 
	 * @param orgId
	 *            流程地图id
	 * @param projectId
	 *            项目id
	 * @author huoyl
	 * @return
	 */
	@Override
	public List<Object[]> getProcessMapProcessesInfo(String[] processMapIds, long projectId, String endTime) {
		if (processMapIds == null || processMapIds.length == 0 || endTime == null || "".equals(endTime.trim())) {
			return new ArrayList();
		}

		// 查询详细信息
		String sql = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = " with t as(select * from ( ";
			// 添加一个sql就为true，以后sql都要拼装union
			boolean flag = false;
			// 所有流程架构所属的流程
			for (int i = 0; i < processMapIds.length; i++) {
				String processMapId = processMapIds[i];
				if (processMapId == null || "".equals(processMapId.trim())) {
					continue;
				}
				if (flag) {
					sql += " union ";
				}
				sql += " select " + processMapId + " as map, tmp.*  from JECN_FLOW_STRUCTURE_T tmp"
						+ " CONNECT BY PRIOR tmp.flow_id = tmp.pre_flow_id START WITH tmp.pre_flow_id = "
						+ processMapId;
				flag = true;
			}
			sql += " )tmp2 where tmp2.ISFLOW = 1 and tmp2.del_state = 0 and tmp2.create_date<=to_date('"
					+ endTime
					+ "', 'yyyy-mm-dd hh24:mi:ss')),"
					+ " t1  as (select max(h.publish_date) as last_publish_date,t.flow_id,t.map from t"
					+ "                 left join JECN_TASK_HISTORY_NEW h on t.flow_id =h.RELATE_ID and h.Type = 0"
					+ "                      and h.publish_date <=to_date('"
					+ endTime
					+ "','yyyy-mm-dd hh24:mi:ss')"
					+ "                group by t.map, t.flow_id ),"
					+ "t2 as (select case "
					+ "  WHEN x2.flow_id is null then null"
					+ " when t1.last_publish_date is null then x2.pub_time else t1.last_publish_date end as last_publish_date,"
					+ "   t1.flow_id,t1.map"
					+ "   from t1 left join JECN_FLOW_STRUCTURE x2 on x2.flow_id=t1.flow_id  "
					+ "and x2.pub_time <=to_date('"
					+ endTime
					+ "','yyyy-mm-dd hh24:mi:ss')),"

					// /////////

					+ "t3 as("

					+ " select t2.last_publish_date,"
					+ "        t2.flow_id,"
					+ "        t2.map,"
					+ "        (select min(publish_date) from"
					+ "        jecn_task_history_new jthn"
					+ "        where t2.flow_id=jthn.relate_id and jthn.type=0"
					+ "        and publish_date>t2.last_publish_date) as true_next_publish_date"
					+ "   from t2"

					+ ")"

					// //////////

					+ "select jfs.flow_id,"
					+ "              jfs.pre_flow_id,"
					+ "              jfs.flow_name,"
					+ "              jfs.flow_id_input,"
					+ "              t3.last_publish_date,"
					+ "              jt.version_id,"
					+ "              org.org_name,"
					+ "              org.org_id,"
					+ "              t4.type_res_people,"
					+ "              t4.res_people_id,"
					+ "              case when t4.type_res_people = 0 then"
					+ "                 (select ju.true_name"
					+ "                    from jecn_user ju"
					+ "                   where ju.people_id = t4.res_people_id)"
					+ "                when t4.type_res_people = 1 then"
					+ "                 (select jfoi.figure_text"
					+ "                    from jecn_flow_org_image jfoi, jecn_flow_org jfo"
					+ "                   where jfoi.org_id = jfo.org_id"
					+ "                     and jfo.del_state = 0"
					+ "                     and jfoi.figure_id = t4.res_people_id)"
					+ "              end as res_people,"
					+ "              jfst.flow_id as p_flow_id,"
					+ "              gju.people_id,"
					+ "              gju.TRUE_NAME,"
					+ "              jt.draft_person,"
					+ "              jt.expiry,"
					+ "              t3.map,"
					+ "              pubCount,"
					+ "              notPubCount,"
					+ "				 case "
					+ "     when"
					+ "     jt.expiry <> 0 and  t3.true_next_publish_date is not null"
					+ "     and to_date(to_char(jt.next_scan_date,'yyyy-mm-dd'),'yyyy-mm-dd') >= to_date(to_char(t3.true_next_publish_date,'yyyy-mm-dd'),'yyyy-mm-dd')"
					+ "     then 1 "
					+ "     when  jt.expiry <> 0 and  t3.true_next_publish_date is not null"
					+ "     and to_date(to_char(jt.next_scan_date,'yyyy-mm-dd'),'yyyy-mm-dd') < to_date(to_char(t3.true_next_publish_date,'yyyy-mm-dd'),'yyyy-mm-dd')"
					+ "     then 0 "
					+ "		when jt.expiry<>0 and"
					+ "		    to_char(jt.next_scan_date,"
					+ "		        'YYYY/MM/DD')< to_char(sysdate, 'YYYY/MM/DD') then"
					+ "		         0 "
					+ "		         else "
					+ "		         2   "
					+ "		       end as scan_ontime,"
					+ "       case when jt.expiry<>0 then"
					+ "       jt.next_scan_date   " // -------------
					+ "       end as next_scan_date,"
					+ "              org.org_id as true_org_id,"
					+ "              org.org_name as true_org_name"
					+ "         from t3"
					+ "         left join (select sum(case when t3.last_publish_date is not null then 1 else 0 end) as pubCount,"
					+ "                           sum(case when t3.last_publish_date is null then 1 else 0 end) as notPubCount,"
					+ "                           t3.map"
					+ "                    from t3 group by t3.map) count on t3.map = count.map"
					+ "         left join JECN_FLOW_STRUCTURE_T jfs on jfs.flow_id = t3.flow_id and jfs.projectid = "
					+ projectId + "         left join jecn_flow_basic_info_t t4 on t4.flow_id = jfs.flow_id"
					+ " left join jecn_task_history_new jt"
					+ " on jt.publish_date=t3.last_publish_date and jt.relate_id=t3.flow_id and jt.type=0"
					+ "         left join (select jfro.org_id, jfo.org_name, jfro.flow_id"
					+ "                      from jecn_flow_related_org_t jfro, jecn_flow_org jfo"
					+ "                     where jfro.org_id = jfo.org_id"
					+ "                       and jfo.del_state = 0) org on org.flow_id ="
					+ "                                                     jfs.flow_id"
					+ "         left join jecn_flow_structure jfst on jfst.flow_id = jfs.flow_id"
					+ "         left join jecn_user gju on gju.people_id = t4.Ward_People_Id"
					+ "        order by org.org_id,jfs.pre_flow_id,jfs.flow_id asc";

		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			// 所有的流程地图下的流程图
			StringBuffer tempBuf = new StringBuffer();
			// 从所有的流程图中获得数据
			StringBuffer allFlowBuf = new StringBuffer();

			tempBuf.append(" WITH ");
			allFlowBuf.append(" flow_temp as  (");
			for (int i = 0; i < processMapIds.length; i++) {
				String processMapId = processMapIds[i];
				String tempTable = "temp" + i;
				tempBuf.append(tempTable + " as" + " (SELECT " + processMapId + " AS MAP,JFT.*"
						+ "   FROM JECN_FLOW_STRUCTURE_T JFT" + "   WHERE JFT.Flow_Id = " + processMapId + ""
						+ "   UNION ALL SELECT " + processMapId + " AS MAP,JFS.*" + "   FROM " + tempTable
						+ "   INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON " + tempTable + ".Flow_Id=JFS.Pre_Flow_Id"
						+ "   WHERE jfs.create_date<= '" + endTime + "'" + "     AND jfs.del_state = 0),");

				allFlowBuf.append("select * from " + tempTable + " where " + tempTable + ".isflow=1");

				if (i != processMapIds.length - 1) {
					allFlowBuf.append(" union ");
				}

			}

			allFlowBuf
					.append(" ),t2 AS" + " (SELECT max(h.publish_date)AS last_publish_date,"
							+ "         flow_temp.flow_id," + "         flow_temp.map" + "  FROM flow_temp"
							+ "  LEFT JOIN JECN_TASK_HISTORY_NEW h ON flow_temp.flow_id = h.RELATE_ID"
							+ "  AND h.Type = 0" + "  AND h.publish_date <= '"
							+ endTime
							+ "'"
							+ "  GROUP BY flow_temp.map,"
							+ "           flow_temp.flow_id),"
							+ " t4 AS "
							+ "  (SELECT CASE"
							+ "  WHEN x2.flow_id is null then null"
							+ "              WHEN t2.last_publish_date IS NULL THEN x2.pub_time"
							+ "              ELSE t2.last_publish_date"
							+ "          END AS last_publish_date,"
							+ "         t2.flow_id,"
							+ "         t2.map"
							+ "   FROM t2"
							+ "   LEFT JOIN JECN_FLOW_STRUCTURE x2 ON x2.flow_id=t2.flow_id"
							+ "   AND x2.pub_time <='"
							+ endTime
							+ "'),"
							+ " t5 as"
							+ " (select t4.last_publish_date,"
							+ "        t4.flow_id,"
							+ "         t4.map"
							+ "    from t4"
							+ "    left join jecn_task_history_new next_scan_history"
							+ "      on t4.flow_id = next_scan_history.relate_id"
							+ "   and next_scan_history.publish_date > t4.last_publish_date"
							+ "   group by t4.flow_id, t4.map, t4.last_publish_date),"
							+ " t6 as("
							+ "  select  t5.last_publish_date,"
							+ "          t5.flow_id,"
							+ "          t5.map,"
							+ "          (select min(publish_date) from"
							+ "          jecn_task_history_new jthn"
							+ "          where t5.flow_id=jthn.relate_id and jthn.type=0"
							+ "          and publish_date>t5.last_publish_date) as true_next_publish_date"
							+ "     from t5"
							+ ")"
							+ "	SELECT jfs.flow_id,"
							+ "       jfs.pre_flow_id,"
							+ "       jfs.flow_name,"
							+ "       jfs.flow_id_input,"
							+ "       t6.last_publish_date,"
							+ "       jt.version_id,"
							+ "       org.org_name,"
							+ "       org.org_id,"
							+ "       t.type_res_people,"
							+ "       t.res_people_id,"
							+ "      CASE"
							+ "          WHEN t.type_res_people = 0 THEN"
							+ "                 (SELECT ju.true_name"
							+ "                  FROM jecn_user ju"
							+ "                  WHERE ju.people_id = t.res_people_id)"
							+ "          WHEN t.type_res_people = 1 THEN"
							+ "                  (SELECT jfoi.figure_text"
							+ "                  FROM jecn_flow_org_image jfoi,"
							+ "                                           jecn_flow_org jfo"
							+ "                  WHERE jfoi.org_id = jfo.org_id"
							+ "                    AND jfo.del_state = 0"
							+ "                   AND jfoi.figure_id = t.res_people_id)"
							+ "       END AS res_people,"
							+ "      jfst.flow_id AS p_flow_id,"
							+ "      gju.people_id,"
							+ "      gju.TRUE_NAME,"
							+ "      jt.draft_person,"
							+ "      jt.expiry,"
							+ "      t6.map,"
							+ "      pubCount,"
							+ "      notPubCount,"
							+ " case              " // --------------
							+ " when"
							+ "   jt.expiry <> 0 and  t6.true_next_publish_date is not null"
							+ "   and convert(varchar(10), jt.next_scan_date, 23)>= convert(varchar(10), t6.true_next_publish_date, 23)"
							+ "   then 1"
							+ " when  jt.expiry <> 0 and  t6.true_next_publish_date is not null"
							+ "   and convert(varchar(10), jt.next_scan_date, 23)< convert(varchar(10), t6.true_next_publish_date, 23)"
							+ "   then 0"
							+ " when jt.expiry<>0 and"
							+ " convert(varchar(10),jt.next_scan_date,"
							+ " 23)< convert(varchar(10),getdate(),23) then "
							+ "  0 "
							+ "  else "
							+ " 2   "
							+ "  end as scan_ontime,"
							+ "       case when jt.expiry<>0 then"
							+ " convert(varchar(10),jt.next_scan_date,23) " // -------------
							+ " end as next_scan_date,"
							+ "              org.org_id as true_org_id,"
							+ "              org.org_name as true_org_name"
							+ "	FROM t6"
							+ "	LEFT JOIN"
							+ "  (SELECT sum(CASE WHEN t6.last_publish_date IS NOT NULL THEN 1 ELSE 0 END) AS pubCount,"
							+ "         sum(CASE WHEN t6.last_publish_date IS NULL THEN 1 ELSE 0 END) AS notPubCount,"
							+ "          t6.map"
							+ "   FROM t6"
							+ "   GROUP BY t6.map) t3 ON t6.map = t3.map"
							+ "	INNER JOIN JECN_FLOW_STRUCTURE_T jfs ON jfs.flow_id=t6.flow_id"
							+ "	AND jfs.projectid="
							+ projectId
							+ "	LEFT JOIN jecn_flow_basic_info_t t ON t.flow_id = jfs.flow_id"
							+ "	LEFT JOIN jecn_task_history_new jt "
							+ " on jt.publish_date=t6.last_publish_date and jt.relate_id=t6.flow_id and jt.type=0"
							+ "	LEFT JOIN"
							+ "	  (SELECT jfro.org_id,"
							+ "	          jfo.org_name,"
							+ "	          jfro.flow_id"
							+ "	   FROM jecn_flow_related_org_t jfro,"
							+ "	        jecn_flow_org jfo"
							+ "   WHERE jfro.org_id = jfo.org_id"
							+ "     AND jfo.del_state = 0) org ON org.flow_id = jfs.flow_id"
							+ "	LEFT JOIN jecn_flow_structure jfst ON jfst.flow_id = jfs.flow_id"
							+ "	LEFT JOIN jecn_user gju ON gju.people_id = t.Ward_People_Id"
							+ " ORDER BY org.org_id,jfs.pre_flow_id,jfs.flow_id asc");

			sql = tempBuf.append(allFlowBuf.toString()).toString();

		}
		return this.listNativeSql(sql);
	}

	/**
	 * 选中的责任部门下的所有的流程图的详细信息
	 * 
	 * @param orgId
	 *            责任部门地图id
	 * @param projectId
	 *            项目id
	 * @param endTime
	 *            结束时间
	 * @author huoyl
	 * @return
	 */
	@Override
	public List<Object[]> getDutyOrgProcessesInfo(String orgId, long projectId, String endTime, boolean orgHasChild) {
		String sql = "";
		String orgSql = getDutyOrgSubSource(orgId, 0, orgHasChild);
		// if (JecnContants.dbType.equals(DBType.ORACLE)) {
		sql = "with t22 as" + " (select max(h.publish_date) as publish_date," + "         jfst.flow_id,"
				+ "         jfo.org_id," + "         jfo.org_name" + "    from jecn_flow_structure_t jfst"
				+ "    inner join jecn_flow_basic_info_t jfbit" + "      on jfst.flow_id = jfbit.flow_id"
				+ "    inner join "
				+ orgSql
				+ " jfo on jfo.flow_id = jfbit.flow_id"
				+ "    inner join jecn_task_history_new h"
				+ "      on jfst.flow_id = h.relate_id"
				+ "     and h.type = 0"
				+ "     and "
				+ lessEqualThan("h.publish_date", endTime)
				+ "   where jfst.del_state = 0"
				+ "     and "
				+ lessEqualThan("jfst.create_date", endTime)
				+ "     and jfst.projectid ="
				+ projectId
				+ "   group by jfst.flow_id, jfo.org_id, jfo.org_name),"
				+ "t3 as"
				+ " (select case"
				+ "  WHEN x2.flow_id is null then null"
				+ "           when t22.publish_date is null then"
				+ "            x2.pub_time"
				+ "           else"
				+ "            t22.publish_date"
				+ "         end as last_publish_date,"
				+ "         t22.flow_id,"
				+ "         t22.org_id,"
				+ "         t22.org_name"
				+ "    from t22"
				+ "    left join JECN_FLOW_STRUCTURE x2"
				+ "      on x2.flow_id = t22.flow_id"
				+ "     and "
				+ lessEqualThan("x2.pub_time", endTime)
				// /////////////
				+ "),t5 as("

				+ " select t3.last_publish_date,"
				+ "        t3.flow_id,"
				+ "        t3.org_id,"
				+ "        t3.org_name,"
				+ "        (select min(publish_date) from"
				+ "        jecn_task_history_new jthn"
				+ "        where t3.flow_id=jthn.relate_id and jthn.type=0"
				+ "        and publish_date>t3.last_publish_date) as true_next_publish_date"
				+ "   from t3"
				+ ")"
				// /////////////

				+ " select jfs.flow_id,"
				+ "       jfs.pre_flow_id,"
				+ "       jfs.flow_name,"
				+ "       jfs.flow_id_input,"
				+ "       t5.last_publish_date,"
				+ "       jt.version_id,"
				+ "       t5.org_name,"
				+ "       t5.org_id,"
				+ "       t4.type_res_people,"
				+ "       t4.res_people_id,"
				+ "       case"
				+ "         when t4.type_res_people = 0 then"
				+ "          (select ju.true_name"
				+ "             from jecn_user ju"
				+ "            where ju.people_id = t4.res_people_id)"
				+ "         when t4.type_res_people = 1 then"
				+ "          (select jfoi.figure_text"
				+ "             from jecn_flow_org_image jfoi, jecn_flow_org jfo"
				+ "            where jfoi.org_id = jfo.org_id"
				+ "              and jfo.del_state = 0"
				+ "              and jfoi.figure_id = t4.res_people_id)"
				+ "       end as res_people,"
				+ "       jfst.flow_id as p_flow_id,"
				+ "       gju.people_id,"
				+ "       gju.TRUE_NAME,"
				+ "       jt.draft_person,"
				+ "       jt.expiry,"
				+ "       t5.org_id,"
				+ "       pubCount,"
				+ "       notPubCount,"
				+ "       case " // ---------------
				+ "     when"
				+ "       jt.expiry <> 0 and  t5.true_next_publish_date is not null"
				+ "       and "
				+ bigEqual("jt.next_scan_date", "t5.true_next_publish_date")
				+ "       then 1"
				+ "     when  jt.expiry <> 0 and  t5.true_next_publish_date is not null"
				+ "       and "
				+ bigEqual("jt.next_scan_date", "t5.true_next_publish_date")
				+ "       then 0"
				+ "     when jt.expiry<>0 and "
				+ smallThanCurTime("jt.next_scan_date")
				+ " then"
				+ "       0 "
				+ "       else "
				+ "       2   "
				+ "       end as scan_ontime,"
				+ "       case when jt.expiry<>0 then"
				+ "       jt.next_scan_date   " // -----------------
				+ "       end as next_scan_date,"
				+ "       jfoo.org_id as true_org_id,"
				+ "       jfoo.org_name true_org_name"
				+ "  from t5"
				+ "  left join (select sum(case"
				+ "                          when t5.last_publish_date is not null then"
				+ "                           1"
				+ "                          else"
				+ "                           0"
				+ "                        end) as pubCount,"
				+ "                    sum(case"
				+ "                          when t5.last_publish_date is null then"
				+ "                           1"
				+ "                          else"
				+ "                           0"
				+ "                        end) as notPubCount,"
				+ "                    t5.org_id"
				+ "               from t5"
				+ "              group by t5.org_id) count"
				+ "    on t5.org_id = count.org_id"
				+ "  left join JECN_FLOW_STRUCTURE_T jfs"
				+ "    on jfs.flow_id = t5.flow_id"
				+ "   and jfs.projectid ="
				+ projectId
				+ "  left join jecn_flow_basic_info_t t4"
				+ "    on t4.flow_id = jfs.flow_id"
				+ "  left join jecn_task_history_new jt"
				+ "    on jt.publish_date = t5.last_publish_date"
				+ "   and jt.relate_id = t5.flow_id and jt.type=0"
				+ "  left join jecn_flow_structure jfst"
				+ "    on jfst.flow_id = jfs.flow_id"
				+ "  left join jecn_user gju"
				+ "    on gju.people_id = t4.Ward_People_Id"
				+ "  left join JECN_FLOW_RELATED_ORG zo on zo.flow_id = jfs.flow_id"
				+ "  left join jecn_flow_org jfoo on zo.org_id=jfoo.org_id"
				+ " order by t5.org_id,jfs.pre_flow_id,jfs.flow_id asc";

		// } else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
		// sql = " WITH t1 as" +
		// " (SELECT max(h.publish_date) as publish_date,jfst.flow_id,jfo.org_id,jfo.org_name"
		// + " FROM jecn_flow_structure_t jfst"
		// +
		// " inner JOIN jecn_flow_basic_info_t jfbit ON jfst.flow_id=jfbit.flow_id"
		// + " inner JOIN "
		// + orgSql
		// + " jfo ON jfo.flow_id=jfst.flow_id"
		// + " inner JOIN JECN_TASK_HISTORY_NEW h ON jfst.flow_id = h.RELATE_ID"
		// + " AND h.Type = 0"
		// + " AND h.publish_date <= '"
		// + endTime
		// + "'"
		// + "	where jfst.del_state = 0"
		// + "  AND jfst.create_date<='"
		// + endTime
		// + "'"
		// + "  AND jfst.projectId="
		// + projectId
		// + "  group by jfst.flow_id,jfo.org_id,jfo.org_name"
		// + "  ),"
		// + "	     t2 as ("
		// + "	SELECT CASE"
		// + "  WHEN x2.flow_id is null then null"
		// + "	              WHEN t1.publish_date IS NULL THEN x2.pub_time"
		// + "	              ELSE t1.publish_date"
		// + "	          END AS publish_date,"
		// + "	          t1.flow_id,"
		// + "	          t1.org_id,"
		// + "	          t1.org_name"
		// + "	   FROM t1"
		// + "	   LEFT JOIN JECN_FLOW_STRUCTURE x2 ON x2.flow_id=t1.flow_id"
		// + "	   AND x2.pub_time <='"
		// + endTime
		// + "'"
		// + "	),"
		// + "   t3 as "
		// + " (SELECT t2.publish_date AS last_publish_date, "
		// + "   t2.flow_id, "
		// + "  t2.org_id, "
		// + "  t2.org_name, "
		// + "  pubCount, "
		// + "  notPubCount "
		// + "    FROM t2 "
		// + "    LEFT JOIN (SELECT sum(CASE "
		// + "                          WHEN t2.publish_date IS NOT NULL THEN "
		// + "                            1 "
		// + "                           ELSE "
		// + "                           0 "
		// + "                         END) AS pubCount, "
		// + "                     sum(CASE "
		// + "                          WHEN t2.publish_date IS NULL THEN "
		// + "                            1 "
		// + "                           ELSE "
		// + "                            0 "
		// + "                        END) AS notPubCount, "
		// + "                     org_id "
		// + "                FROM t2 "
		// + "               GROUP BY org_id) tt "
		// + "      ON t2.org_id = tt.org_id "
		// + "   left join jecn_task_history_new next_scan_time  "
		// +
		// "     on t2.flow_id = next_scan_time.relate_id and  next_scan_time.publish_date > t2.publish_date"
		// + "   group by t2.publish_date,t2.flow_id, "
		// + "       t2.org_id, "
		// + "       t2.org_name, "
		// + "       pubCount, "
		// + "       notPubCount"
		// + " ),"
		// + " t4 as("
		// + "  select  t3.last_publish_date,"
		// + "          t3.flow_id,"
		// + "          t3.org_id,"
		// + "          t3.org_name,"
		// + "          t3.pubCount,"
		// + "          t3.notPubCount,"
		// + "          (select min(publish_date) from"
		// + "          jecn_task_history_new jthn"
		// + "          where t3.flow_id=jthn.relate_id and jthn.type=0"
		// +
		// "          and publish_date>t3.last_publish_date) as true_next_publish_date"
		// + "     from t3"
		// + ")"
		// + " SELECT jfs.flow_id,"
		// + "    jfs.pre_flow_id,"
		// + "   jfs.flow_name,"
		// + "   jfs.flow_id_input,"
		// + "   t4.last_publish_date,"
		// + "   jt.version_id,"
		// + "  t4.org_name,"
		// + "  t4.org_id,"
		// + "  t.type_res_people,"
		// + "  t.res_people_id,"
		// + "   CASE"
		// + "    WHEN t.type_res_people = 0 THEN"
		// + "          (SELECT ju.true_name"
		// + "           FROM jecn_user ju"
		// + "            WHERE ju.people_id = t.res_people_id)"
		// + "     WHEN t.type_res_people = 1 THEN"
		// + "           (SELECT jfoi.figure_text"
		// + "         FROM jecn_flow_org_image jfoi,"
		// + "                                   jecn_flow_org jfo"
		// + "         WHERE jfoi.org_id = jfo.org_id"
		// + "           AND jfo.del_state = 0"
		// + "              AND jfoi.figure_id = t.res_people_id)"
		// + "  END AS res_people,"
		// + " jfst.flow_id AS p_flow_id,"
		// + " gju.people_id,"
		// + "  gju.TRUE_NAME,"
		// + "  jt.draft_person,"
		// + "  jt.expiry,"
		// + "  t4.org_id,"
		// + "   pubCount,"
		// + "   notPubCount,"
		// + " case   " // -------------
		// + " when"
		// + "    jt.expiry <> 0 and  t4.true_next_publish_date is not null"
		// +
		// "    and convert(varchar(10), jt.next_scan_date, 23)>= convert(varchar(10), t4.true_next_publish_date, 23)"
		// + "    then 1"
		// + "  when  jt.expiry <> 0 and  t4.true_next_publish_date is not null"
		// +
		// "    and convert(varchar(10), jt.next_scan_date, 23)< convert(varchar(10), t4.true_next_publish_date, 23)"
		// + "    then 0"
		// + " when jt.expiry<>0 and"
		// + " convert(varchar(10),jt.next_scan_date,"
		// + " 23)< convert(varchar(10),getdate(),23) then"
		// + " 0 "
		// + " else "
		// + " 2   "
		// + " end as scan_ontime,"
		// + "       case when jt.expiry<>0 then"
		// + " convert(varchar(10),jt.next_scan_date,23)  " // -------------
		// + " end as next_scan_date,"
		// + "       jfoo.org_id as true_org_id,"
		// + "       jfoo.org_name true_org_name"
		// + " FROM JECN_FLOW_STRUCTURE_T jfs"
		// + " INNER JOIN t4 ON jfs.flow_id=t4.flow_id"
		// + " LEFT JOIN jecn_flow_basic_info_t t ON t.flow_id = jfs.flow_id"
		// + " left join jecn_task_history_new jt"
		// + "   on jt.publish_date = t4.last_publish_date"
		// + "   and jt.relate_id = t4.flow_id and jt.type=0"
		// + " LEFT JOIN jecn_flow_structure jfst ON jfst.flow_id = jfs.flow_id"
		// + " LEFT JOIN jecn_user gju ON gju.people_id = t.Ward_People_Id"
		// + "  left join JECN_FLOW_RELATED_ORG zo on zo.flow_id = jfs.flow_id"
		// + "  left join jecn_flow_org jfoo on zo.org_id=jfoo.org_id"
		// + " where jfs.del_state=0"
		// + " ORDER BY t4.org_id,jfs.pre_flow_id,jfs.flow_id asc";
		// }

		return this.listNativeSql(sql);
	}

	/**
	 * 选中的责任部门下的流程图与它的父节点基本信息
	 * 
	 * @param oIds
	 *            责任部门id
	 * @param projectId
	 *            项目id
	 * @param endTime
	 *            结束时间
	 */
	@Override
	public List<Object[]> getProcessDetailDutyOrgList(String oIds, long projectId, String endTime, boolean orgHasChild)
			throws Exception {
		String sql = "";
		String orgSql = getDutyOrgSubSource(oIds, 0, orgHasChild);

		sql = "	WITH org_flowid as" + " (SELECT DISTINCT jfst.flow_id" + "  FROM jecn_flow_structure_t jfst"
				+ "   LEFT JOIN jecn_flow_basic_info_t jfbit ON jfst.flow_id=jfbit.flow_id inner join " + orgSql
				+ " jfo ON jfo.flow_id=jfst.flow_id"
				+ " LEFT JOIN JECN_TASK_HISTORY_NEW h ON jfst.flow_id = h.RELATE_ID AND h.Type = 0 AND "
				+ lessEqualThan("h.publish_date", endTime) + "  WHERE 1=1 and "
				+ lessEqualThan("jfst.create_date", endTime) + "    AND jfst.del_state = 0" + "    AND jfo.projectid="
				+ projectId + ")," + " MY_FLOW AS" + "  (";

		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql += "select jfst.* from JECN_FLOW_STRUCTURE_T jfst where jfst.del_state = 0"
					+ " CONNECT BY PRIOR  jfst.pre_flow_id=jfst.flow_id"
					+ " START WITH jfst.flow_id  in (select flow_id from org_flowid)";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql += "SELECT JFT.*" + "  FROM JECN_FLOW_STRUCTURE_T JFT"
					+ "  WHERE JFT.Flow_Id in (select flow_id from org_flowid)" + "   UNION ALL SELECT JFS.*"
					+ "  FROM MY_FLOW" + "  INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.Pre_Flow_Id= JFS.Flow_Id";
		}

		sql += " )";
		sql += "select  jfst.flow_id,jfst.flow_name,jfst.pre_flow_id,jfst.flow_id_input,jfst.isflow from MY_FLOW jfst";

		return this.listNativeSql(sql);
	}

	/**
	 * 
	 * 责任部门下流程kpi跟踪表数据源
	 * 
	 * @param dutyOrgIds
	 *            String 部门ID集合（1,2,3,4...）
	 * @param projectId
	 *            long 主项目ID
	 * @param startTime
	 *            String 主项目ID（yyyy-MM-dd 00:00:00）
	 * @param endTime
	 *            String 主项目ID (yyyy-MM-dd 23:59:59)
	 * 
	 * @return List<Object[]>
	 * 
	 */
	@Override
	public List<Object[]> getProcessKPIFollowList(String dutyOrgIds, long projectId, String startTime, String endTime) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "SELECT F.FLOW_NAME," + "       ORG.ORG_NAME as DUTY_ORG_NAME,"
					+ "       CASE WHEN FI.TYPE_RES_PEOPLE=0 THEN U2.TRUE_NAME ELSE POS.FIGURE_TEXT END AS FLOW_ZR,"
					+ "       CASE WHEN FI.TYPE_RES_PEOPLE=1 THEN POS.FIGURE_ID END AS POS_ID,"
					+ "       U3.TRUE_NAME as wardPeopleName," + "       U4.TRUE_NAME as draftPersonName,"
					+ "       KPI.KPI_NAME," + "       KPI.KPI_TARGET_TYPE," + "       FT.TARGER_CONTENT,"
					+ "       KPI.KPI_RELEVANCE," + "       KPI.KPI_STATISTICAL_METHODS,"
					+ "       KPI.KPI_TARGET_OPERATOR," + "       KPI.KPI_TARGET," + "       KPI.KPI_VERTICAL,"
					+ "       KPI.KPI_DATA_METHOD," + "       ST.FLOW_SUSTAIN_TOOL_DESCRIBE as itSystem,"
					+ "       U.TRUE_NAME as kpiDataPeopleName,"
					+ "       KPI.KPI_HORIZONTAL,"
					+ "       ORG.ORG_ID,"
					+ "       F.FLOW_ID,"
					+ "       KPI.KPI_AND_ID AS KPIID,"
					+ "       KV.KPI_ID AS KPIVALUEID,"
					+ "       KV.KPI_HOR_VALUE,"
					+ "       KV.KPI_VALUE,"
					+ "       ORG.ORG_NAME,"// 选中的责任部门的名称
					+ "       KPI.KPI_PURPOSE," + "       KPI.KPI_POINT," + "       KPI.KPI_PERIOD,"
					+ "       KPI.KPI_EXPLAIN," + "       KPI.KPI_DEFINITION," + "       KPI.KPI_TYPE"
					+ "  FROM JECN_FLOW_KPI_NAME KPI" + "  LEFT JOIN JECN_FLOW_STRUCTURE F ON KPI.FLOW_ID = F.FLOW_ID"
					+ "                                 AND F.ISFLOW = 1"
					+ "  LEFT JOIN JECN_FLOW_RELATED_ORG ZO ON F.FLOW_ID = ZO.FLOW_ID"
					+ "  LEFT JOIN JECN_FLOW_ORG ORG ON ORG.ORG_ID = ZO.ORG_ID"
					+ "                             AND ORG.PROJECTID = "
					+ projectId
					+ "  LEFT JOIN JECN_FIRST_TARGER FT ON FT.ID = KPI.FIRST_TARGER_ID"
					+ "  LEFT JOIN JECN_SUSTAIN_TOOL_CONN_T KREF ON KREF.RELATED_TYPE = 0"
					+ "                                       AND KREF.RELATED_ID = KPI.KPI_AND_ID"
					+ "  LEFT JOIN JECN_FLOW_SUSTAIN_TOOL ST ON ST.FLOW_SUSTAIN_TOOL_ID ="
					+ "                                         KREF.SUSTAIN_TOOL_ID"
					+ "  LEFT JOIN JECN_USER U ON U.PEOPLE_ID = KPI.KPI_DATA_PEOPEL_ID"
					+ "  LEFT JOIN JECN_FLOW_BASIC_INFO FI ON FI.FLOW_ID = F.FLOW_ID"
					+ "  LEFT JOIN JECN_USER U2 ON FI.TYPE_RES_PEOPLE = 0"
					+ "                        AND FI.RES_PEOPLE_ID = U2.PEOPLE_ID"
					+ "  LEFT JOIN JECN_FLOW_ORG_IMAGE POS ON FI.TYPE_RES_PEOPLE = 1"
					+ "                                   AND FI.RES_PEOPLE_ID = POS.FIGURE_ID"
					+ "  LEFT JOIN JECN_USER U3 ON FI.TYPE_RES_PEOPLE = 0"
					+ "                        AND FI.WARD_PEOPLE_ID = U3.PEOPLE_ID"
					+ "  LEFT JOIN JECN_USER U4 ON FI.TYPE_RES_PEOPLE = 0"
					+ "                        AND FI.FICTION_PEOPLE_ID = U4.PEOPLE_ID"
					+ "  LEFT JOIN JECN_FLOW_KPI KV ON KV.KPI_AND_ID = KPI.KPI_AND_ID"
					+ " WHERE ORG.ORG_ID IN ("
					+ dutyOrgIds
					+ "  )  AND F.DEL_STATE=0 AND (( KV.KPI_HOR_VALUE BETWEEN"
					+ " TO_DATE((substr('"
					+ startTime
					+ "',0,7)||'-01'), 'YYYY-MM-DD') " // 开始日期的第一天
					+ " AND"
					+ " LAST_DAY(TO_DATE('"
					+ endTime
					+ "', 'YYYY-MM-DD')))" // 结束日期的最后一天
					+ " OR KV.KPI_ID IS NULL )"
					+ " ORDER BY ORG.ORG_ID,F.FLOW_ID, KV.KPI_AND_ID,KPI.KPI_AND_ID, KV.KPI_HOR_VALUE";

		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "SELECT F.FLOW_NAME," + "       ORG.ORG_NAME,"
					+ "       CASE WHEN FI.TYPE_RES_PEOPLE=0 THEN U2.TRUE_NAME ELSE POS.FIGURE_TEXT END AS FLOW_ZR,"
					+ "       CASE WHEN FI.TYPE_RES_PEOPLE=1 THEN POS.FIGURE_ID END AS POS_ID,"
					+ "       U3.TRUE_NAME as wardPeopleName," + "       U4.TRUE_NAME as draftPersonName,"
					+ "       KPI.KPI_NAME ," + "       KPI.KPI_TARGET_TYPE," + "       FT.TARGER_CONTENT,"
					+ "       KPI.KPI_RELEVANCE," + "       KPI.KPI_STATISTICAL_METHODS,"
					+ "       KPI.KPI_TARGET_OPERATOR," + "       KPI.KPI_TARGET," + "       KPI.KPI_VERTICAL,"
					+ "       KPI.KPI_DATA_METHOD," + "       ST.FLOW_SUSTAIN_TOOL_DESCRIBE,"
					+ "       U.TRUE_NAME as kpiDataPeopleName,"
					+ "       KPI.KPI_HORIZONTAL,"
					+ "       ORG.ORG_ID,"
					+ "       F.FLOW_ID,"
					+ "       KPI.KPI_AND_ID,"
					+ "       KV.KPI_ID,"
					+ "       KV.KPI_HOR_VALUE,"
					+ "       KV.KPI_VALUE,"
					+ "       ORG.ORG_NAME,"// 选中的责任部门的名称
					+ "       KPI.KPI_PURPOSE," + "       KPI.KPI_POINT," + "       KPI.KPI_PERIOD,"
					+ "       KPI.KPI_EXPLAIN," + "       KPI.KPI_DEFINITION," + "       KPI.KPI_TYPE"
					+ "  FROM JECN_FLOW_KPI_NAME KPI" + "  LEFT JOIN JECN_FLOW_STRUCTURE F ON KPI.FLOW_ID = F.FLOW_ID"
					+ "                                 AND F.ISFLOW = 1"
					+ "  LEFT JOIN JECN_FLOW_RELATED_ORG ZO ON F.FLOW_ID = ZO.FLOW_ID"
					+ "  LEFT JOIN JECN_FLOW_ORG ORG ON ORG.ORG_ID = ZO.ORG_ID"
					+ "                             AND ORG.PROJECTID = "
					+ projectId
					+ "  LEFT JOIN JECN_FIRST_TARGER FT ON FT.ID = KPI.FIRST_TARGER_ID"
					+ "  LEFT JOIN JECN_SUSTAIN_TOOL_CONN_T KREF ON KREF.RELATED_TYPE = 0"
					+ "                                       AND KREF.RELATED_ID = KPI.KPI_AND_ID"
					+ "  LEFT JOIN JECN_FLOW_SUSTAIN_TOOL ST ON ST.FLOW_SUSTAIN_TOOL_ID ="
					+ "                                         KREF.SUSTAIN_TOOL_ID"
					+ "  LEFT JOIN JECN_USER U ON U.PEOPLE_ID = KPI.KPI_DATA_PEOPEL_ID"
					+ "  LEFT JOIN JECN_FLOW_BASIC_INFO FI ON FI.FLOW_ID = F.FLOW_ID"
					+ "  LEFT JOIN JECN_USER U2 ON FI.TYPE_RES_PEOPLE = 0"
					+ "                        AND FI.RES_PEOPLE_ID = U2.PEOPLE_ID"
					+ "  LEFT JOIN JECN_FLOW_ORG_IMAGE POS ON FI.TYPE_RES_PEOPLE = 1"
					+ "                                   AND FI.RES_PEOPLE_ID = POS.FIGURE_ID"
					+ "  LEFT JOIN JECN_USER U3 ON FI.TYPE_RES_PEOPLE = 0"
					+ "                        AND FI.WARD_PEOPLE_ID = U3.PEOPLE_ID"
					+ "  LEFT JOIN JECN_USER U4 ON FI.TYPE_RES_PEOPLE = 0"
					+ "                        AND FI.FICTION_PEOPLE_ID = U4.PEOPLE_ID"
					+ "  LEFT JOIN JECN_FLOW_KPI KV ON KV.KPI_AND_ID = KPI.KPI_AND_ID"
					+ " WHERE ORG.ORG_ID IN ("
					+ dutyOrgIds
					+ ")"
					+ " AND (( (F.DEL_STATE=0 AND CONVERT(VARCHAR(7),KV.KPI_HOR_VALUE,21) BETWEEN"
					+ "       CONVERT(VARCHAR(7),'"
					+ startTime
					+ ",21') AND"
					+ "       CONVERT(VARCHAR(7),'"
					+ endTime
					+ ",21')))"
					+ " OR KV.KPI_ID IS NULL )"
					+ " ORDER BY ORG.ORG_ID,F.FLOW_ID, KV.KPI_AND_ID,KPI.KPI_AND_ID, KV.KPI_HOR_VALUE";
		}

		return this.listNativeSql(sql);
	}

	/**
	 * 
	 * @param dutyOrgIds
	 * @param type
	 * @param orgHasChild
	 * @return
	 */
	private String getDutyOrgSubSource(String dutyOrgIds, int type, boolean orgHasChild) {
		String sql = "";
		String table = "";
		if (orgHasChild) {
			table = getChildOrgs(dutyOrgIds);
		} else {
			table = "( select jfo.*,jfo.org_id as c_org_id from jecn_flow_org jfo where jfo.org_id in (" + dutyOrgIds
					+ ") )";
		}
		if (type == 0) {
			sql = "(select jfo.*, zo.flow_id from " + table + " jfo left join JECN_FLOW_RELATED_ORG zo"
					+ "    on jfo.c_org_id = zo.org_id)";
		} else {
			sql = "(select jfo.*, zo.id as rule_id from " + table + " jfo left join jecn_rule zo"
					+ "    on jfo.c_org_id = zo.org_id)";
		}
		return sql;
	}

	@Override
	public List<Object[]> getAllProcessScanOptimizeList(String dutyOrgIds, String startTime, String endTime,
			long projectId, String processMapIds, String dutyIds, boolean orgHasChild) throws Exception {
		String orgSql = getDutyOrgSubSource(dutyOrgIds, 0, orgHasChild);
		String sql = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "with tmp as(" // tmp所有有效期非0的发布数据
					+ " select d.RELATE_ID,"
					+ "        d.PUBLISH_DATE,"
					+ "        max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE"
					+ "   from (select a.PUBLISH_DATE,"
					+ "                a.RELATE_ID,"
					+ "                b.PUBLISH_DATE as last_PUBLISH_DATE,"
					+ "               b.RELATE_ID as last_RELATE_ID"
					+ "           from JECN_TASK_HISTORY_NEW a"
					+ "           left join JECN_TASK_HISTORY_NEW b on b.type = 0 and b.expiry<>0"
					+ "                                           and b.type = a.type"
					+ "                                            and b.relate_id = a.relate_id"
					+ "                                            and b.publish_date < a.publish_date"
					+ "          where a.type = 0"
					+ "            and b.publish_date is not null) d"
					+ "  group by d.PUBLISH_DATE, d.RELATE_ID"
					+ " union all" // 最新的的版本（肯定是未审视的）
					+ " select t4.relate_id, t4.publish_date, t4.last_publish_date"
					+ "    from ("
					+ "          select t2.relate_id, t2.publish_date, t2.last_publish_date"
					+ "            from (select t1.relate_id,"
					+ "                           max(t1.publish_date) as publish_date,"
					+ "                           max(t1.publish_date) as last_publish_date"
					+ "                     from jecn_task_history_new t1"
					+ "                    where t1.type = 0"
					+ "                    group by t1.relate_id) t2"
					+ "           inner join jecn_task_history_new t3"
					+ "              on t2.relate_id = t3.relate_id"
					+ "             and t2.publish_date = t3.publish_date"
					+ "             and t3.expiry <> 0"
					+ "             and t3.type = 0) t4"
					+ "  )"
					+ "  ,"
					+ "  tmp2 as("
					+ "     select x.next_scan_date AS last_expect_publish_date," // 需完成的月份（计划）
					+ "        f.flow_id, "
					+ "        f.flow_name, "// 需审视流程
					+ "         x.VERSION_ID, "// 版本
					+ "         tmp.last_PUBLISH_DATE, "// 需审视流程发布时间
					+ "         x.Draft_Person, "// 拟稿人
					+ "        org.org_name," // 责任部门
					+ "        org.org_id,"
					+ "        case when fi.Type_Res_People=0 then u.true_name else pos.figure_text end as flow_zr," // 流程责任人
					+ "        case when fi.Type_Res_People=1 then pos.figure_id end as pos_id, "// 流程责任人是岗位时，岗位ID
					+ "        fi.Ward_People_Id ,"// 监护人ID
					+ "         case when tmp.publish_date=tmp.last_PUBLISH_DATE then null else tmp.publish_date end as publish_date, "// 当前发布时间
					+ "         case when  tmp.publish_date=tmp.last_PUBLISH_DATE then 0"// 按时完成个数
					+ "        when x.next_scan_date <=to_date('"
					+ endTime
					+ "','yyyy-mm-dd hh24:mi:ss') and to_date(to_char(tmp.PUBLISH_DATE,'YYYY/MM/DD'),'YYYY/MM/DD')<=x.next_scan_date  then 1 else 0 end finishNum,"
					+ "        1 as totalnum,"
					+ "         tmp.RELATE_ID,"
					+ "     x.MODIFY_EXPLAIN"
					+ "     from tmp"
					+ "     left join JECN_TASK_HISTORY_NEW x on x.type = 0 and x.expiry<>0"
					+ "                                     and tmp.RELATE_ID = x.relate_id"
					+ "                                    and tmp.last_PUBLISH_DATE ="
					+ "                                       x.publish_date"
					+ "     inner join JECN_FLOW_STRUCTURE f on tmp.RELATE_ID=f.flow_id and f.isflow=1 and f.del_state=0"
					+ "     inner join "
					+ orgSql
					+ " org on org.flow_id=f.flow_id and org.PROJECTID="
					+ projectId
					+ "     left join JECN_FLOW_BASIC_INFO fi on fi.flow_id=f.flow_id "
					+ "     left join jecn_user u on fi.Type_Res_People=0 and fi.Res_People_Id=u.people_id"// 人员
					+ "     left join JECN_FLOW_ORG_IMAGE pos on fi.Type_Res_People=1 and fi.res_people_id=pos.figure_id"// 岗位
					+ "     where 1=1 ";
			if (StringUtils.isNotBlank(processMapIds)) {
				sql += " and f.flow_id in " + getChildFlows(processMapIds);
			}
			if (StringUtils.isNotBlank(dutyIds)) {
				sql += " and fi.res_people_id in (" + dutyIds + ")";
			}
			sql += "  and (x.next_scan_date between to_date('"
					+ startTime
					+ "','yyyy-mm-dd hh24:mi:ss') "
					+ "   and to_date('"
					+ endTime
					+ "','yyyy-mm-dd hh24:mi:ss'))"
					+ " ),"
					+ " tmp3 as ("
					+ "  select a.*,"
					+ "         b.finishCount,"
					+ "         b.totalCount,"
					+ "         trunc((b.finishCount / b.totalCount),2)* 100   as finishperce"
					+ "    from tmp2 a"
					+ "    left join (select sum(c.finishNum) as finishCount,"
					+ "                     sum(c.totalnum) as totalCount,"
					+ "                      c.org_id"
					+ "                 from tmp2 c"
					+ "                group by c.org_id) b on a.org_id = b.org_id)"
					+ " 	select distinct "
					+ "        tmp3.last_expect_publish_date, "// 需完成的月份（计划）
					+ "        tmp3.flow_id, "
					+ "        tmp3.flow_name, "// 需审视流程
					+ "        tmp3.VERSION_ID," // 版本
					+ "        tmp3.last_PUBLISH_DATE, "// 需审视流程发布时间
					+ "        tmp3.Draft_Person, "// 拟稿人
					+ "        tmp3.org_id," // 选中的责任部门id
					+ "        tmp3.org_name, "// 选中的责任部门name
					+ "        tmp3.flow_zr, "// 流程责任人
					+ "        jorg.org_id as g_id," // 监护人部门id
					+ "         jorg.org_name as g_name," // 监护人所属部门
					+ "         ju.people_id," // 监护人id
					+ "         ju.true_name," // 监护人
					+ "         tmp3.publish_date," // 当前发布时间
					+ "         tmp3.finishCount, "// 按时完成审视优化数量
					+ "         tmp3.totalCount, "// 需审视流程数量
					+ "         tmp3.finishperce, "// 审视优化及时完成率
					+ "	        case"
					+ "         when to_char(tmp3.last_expect_publish_date, 'YYYY/MM/DD') >="
					+ "               to_char(tmp3.publish_date, 'YYYY/MM/DD') and tmp3.publish_date is not null"
					+ "          then 1"
					+ "          when to_char(tmp3.last_expect_publish_date, 'YYYY/MM/DD') >="
					+ "               to_char(sysdate, 'YYYY/MM/DD') and tmp3.publish_date is null"
					+ "          then 2" // 空
					+ "          else 0" // 否
					+ "	         end as scan_ontime," // 是否及时优化
					+ "          tmp3.org_name," // 责任部门name
					+ "          tmp3.MODIFY_EXPLAIN," + "     jfoo.org_id as true_org_id,"
					+ "     jfoo.org_name as true_org_name" + "   from tmp3"
					+ "   left join jecn_user ju on tmp3.Ward_People_Id = ju.people_id"
					+ "   left join JECN_USER_POSITION_RELATED jref on jref.people_id ="
					+ "                                                ju.people_id"
					+ "   left join JECN_FLOW_ORG_IMAGE jpos on jref.figure_id = jpos.figure_id"
					+ "   left join JECN_FLOW_ORG jorg on jorg.org_id = jpos.org_id and jorg.PROJECTID=" + projectId
					+ "  left join JECN_FLOW_RELATED_ORG zo on zo.flow_id = tmp3.flow_id"
					+ "  left join jecn_flow_org jfoo on zo.org_id=jfoo.org_id"
					+ " order by tmp3.publish_date,tmp3.flow_id,tmp3.org_id asc";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "with tmp as(" // tmp所有有效期非0的发布数据
					+ " select d.RELATE_ID,"
					+ "        d.PUBLISH_DATE,"
					+ "        max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE"
					+ "   from (select a.PUBLISH_DATE,"
					+ "                a.RELATE_ID,"
					+ "                b.PUBLISH_DATE as last_PUBLISH_DATE,"
					+ "               b.RELATE_ID as last_RELATE_ID"
					+ "           from JECN_TASK_HISTORY_NEW a"
					+ "           left join JECN_TASK_HISTORY_NEW b on b.type = 0 and b.expiry<>0"
					+ "                                           and b.type = a.type"
					+ "                                            and b.relate_id = a.relate_id"
					+ "                                            and b.publish_date < a.publish_date"
					+ "          where a.type = 0"
					+ "            and b.publish_date is not null) d"
					+ "  group by d.PUBLISH_DATE, d.RELATE_ID"
					+ " union all"
					+ " select t4.relate_id, t4.publish_date, t4.last_publish_date"
					+ "    from ("
					+ "          select t2.relate_id, t2.publish_date, t2.last_publish_date"
					+ "            from (select t1.relate_id,"
					+ "                           max(t1.publish_date) as publish_date,"
					+ "                           max(t1.publish_date) as last_publish_date"
					+ "                     from jecn_task_history_new t1"
					+ "                    where t1.type = 0"
					+ "                    group by t1.relate_id) t2"
					+ "           inner join jecn_task_history_new t3"
					+ "              on t2.relate_id = t3.relate_id"
					+ "             and t2.publish_date = t3.publish_date"
					+ "             and t3.expiry <> 0"
					+ "             and t3.type = 0) t4"
					+ "  )"
					+ "  ,"
					+ "  tmp2 as("
					+ "     select x.next_scan_date AS last_expect_publish_date," // 需完成的月份（计划）
					+ "        f.flow_id, "
					+ "        f.flow_name, "// 需审视流程
					+ "         x.VERSION_ID, "// 版本
					+ "         tmp.last_PUBLISH_DATE, "// 需审视流程发布时间
					+ "         x.Draft_Person, "// 拟稿人
					+ "        org.org_name," // 责任部门
					+ "        org.org_id,"
					+ "        case when fi.Type_Res_People=0 then u.true_name else pos.figure_text end as flow_zr," // 流程责任人
					+ "        case when fi.Type_Res_People=1 then pos.figure_id end as pos_id, "// 流程责任人是岗位时，岗位ID
					+ "        fi.Ward_People_Id ,"// 监护人ID
					+ "         case when tmp.publish_date=tmp.last_PUBLISH_DATE then null else tmp.publish_date end as publish_date, "// 当前发布时间
					+ "         case when  tmp.publish_date=tmp.last_PUBLISH_DATE then 0"// 按时完成个数
					+ "        when dateadd(month,x.expiry,tmp.last_PUBLISH_DATE) <='"
					+ endTime
					+ "' and CONVERT(varchar(10), tmp.PUBLISH_DATE, 23)<=convert(varchar(10),x.next_scan_date,23)  then 1 else 0 end finishNum,"
					+ "        1 as totalnum,"
					+ "         tmp.RELATE_ID,"
					+ "         x.MODIFY_EXPLAIN "
					+ "     from tmp"
					+ "     left join JECN_TASK_HISTORY_NEW x on x.type = 0 and x.expiry<>0"
					+ "                                     and tmp.RELATE_ID = x.relate_id"
					+ "                                    and tmp.last_PUBLISH_DATE ="
					+ "                                       x.publish_date"
					+ "    inner join JECN_FLOW_STRUCTURE f on tmp.RELATE_ID=f.flow_id and f.isflow=1 and f.del_state=0"
					+ "    left join JECN_FLOW_RELATED_ORG zo on f.flow_id=zo.flow_id"
					+ "     left join JECN_FLOW_ORG org on org.org_id=zo.org_id and org.PROJECTID=1"
					+ "     left join JECN_FLOW_BASIC_INFO fi on fi.flow_id=f.flow_id "
					+ "     left join jecn_user u on fi.Type_Res_People=0 and fi.Res_People_Id=u.people_id"// 人员
					+ "     left join JECN_FLOW_ORG_IMAGE pos on fi.Type_Res_People=1 and fi.res_people_id=pos.figure_id"// 岗位
					+ "     where  org.org_id  in(" + dutyOrgIds + ") ";
			if (StringUtils.isNotBlank(processMapIds)) {
				sql += " and f.flow_id in " + getChildFlows(processMapIds);
			}
			if (StringUtils.isNotBlank(dutyIds)) {
				sql += " and fi.res_people_id in (" + dutyIds + ")";
			}
			sql += " and " + "    convert(varchar(10),x.next_scan_date,23) between '"
					+ startTime
					+ "'"
					+ "   and '"
					+ endTime
					+ "'"
					+ " ),"
					+ " tmp3 as ("
					+ "  select a.*,"
					+ "         b.finishCount,"
					+ "         b.totalCount,"
					+ "         b.finishCount*100/b.totalCount  AS finishperce"
					+ "    from tmp2 a"
					+ "    left join (select sum(c.finishNum) as finishCount,"
					+ "                     sum(c.totalnum) as totalCount,"
					+ "                      c.org_id"
					+ "                 from tmp2 c"
					+ "                group by c.org_id) b on a.org_id = b.org_id)"
					+ " 	select  "
					+ "        CONVERT(varchar(10),tmp3.last_expect_publish_date, 23) as last_expect_publish_date, "// 需完成的月份（计划）
					+ "        tmp3.flow_id, "
					+ "        tmp3.flow_name, "// 需审视流程
					+ "        tmp3.VERSION_ID," // 版本
					+ "        CONVERT(varchar(10),tmp3.last_PUBLISH_DATE, 23) as last_PUBLISH_DATE,"// 需审视流程发布时间
					+ "        tmp3.Draft_Person, "// 拟稿人
					+ "        tmp3.org_id," // 责任部门id
					+ "        tmp3.org_name, "// 责任部门
					+ "        tmp3.flow_zr, "// 流程责任人
					+ "        jorg.org_id as g_id," // 监护人部门id
					+ "         jorg.org_name as g_name," // 监护人所属部门
					+ "         ju.people_id," // 监护人id
					+ "         ju.true_name," // 监护人
					+ "         CONVERT(varchar(10), tmp3.publish_date, 23) as publish_date," // 当前发布时间
					+ "         tmp3.finishCount, "// 按时完成审视优化数量
					+ "         tmp3.totalCount, "// 需审视流程数量
					+ "         tmp3.finishperce, "// 审视优化及时完成率
					+ "         case when  CONVERT(varchar(10), tmp3.last_expect_publish_date, 23)"
					+ " >=CONVERT(varchar(10), tmp3.publish_date, 23) and tmp3.publish_date is not null"
					+ "	then 1"
					+ " when CONVERT(varchar(10),getdate(), 23)<=CONVERT(varchar(10), tmp3.last_expect_publish_date, 23) and tmp3.publish_date is null"
					+ " then 2"
					+ "	else 0"
					+ "	end as scan_ontime," // 是否及时优化 1为及时 0为不及时 2为空
					+ " tmp3.org_name," // 责任部门name
					+ " tmp3.MODIFY_EXPLAIN," + "     jfoo.org_id as true_org_id,"
					+ "     jfoo.org_name as true_org_name" + "   from tmp3"
					+ "   left join jecn_user ju on tmp3.Ward_People_Id = ju.people_id"
					+ "   left join JECN_USER_POSITION_RELATED jref on jref.people_id ="
					+ "                                                ju.people_id"
					+ "   left join JECN_FLOW_ORG_IMAGE jpos on jref.figure_id = jpos.figure_id"
					+ "   left join JECN_FLOW_ORG jorg on jorg.org_id = jpos.org_id and jorg.PROJECTID=" + projectId
					+ "  left join JECN_FLOW_RELATED_ORG zo on zo.flow_id = tmp3.flow_id"
					+ "  left join jecn_flow_org jfoo on zo.org_id=jfoo.org_id"
					+ " order by tmp3.publish_date,tmp3.flow_id,tmp3.org_id asc";

		}

		return this.listNativeSql(sql);
	}

	private String getChildOrgs(String orgIds) {
		String sql = "(SELECT p.*,c.org_id as c_org_id FROM" + "		jecn_flow_org c"
				+ "	INNER JOIN jecn_flow_org p ON c.T_PATH LIKE p.T_PATH " + JecnCommonSql.getConcatChar() + " '%'"
				+ "	AND c.T_PATH IS NOT NULL" + "	AND p.T_PATH IS NOT NULL" + "	AND p.org_id in (" + orgIds + "))";
		return sql;
	}

	private String getChildFlows(String processMapIds) {
		String sql = "(SELECT" + "		c.flow_id" + "	FROM" + "		JECN_FLOW_STRUCTURE_T c"
				+ "	INNER JOIN JECN_FLOW_STRUCTURE_T p ON c.T_PATH LIKE p.T_PATH " + JecnCommonSql.getConcatChar()
				+ " '%'" + "	AND c.T_PATH IS NOT NULL" + "	AND p.T_PATH IS NOT NULL" + "	AND c.ISFLOW=1"
				+ "	AND p.FLOW_ID in (" + processMapIds + "))";
		return sql;
	}

	private String getChildRules(String ruleIds) {
		String sql = "(SELECT" + "		c.id" + "	FROM" + "		jecn_rule_t c"
				+ "	INNER JOIN jecn_rule_t p ON c.T_PATH LIKE p.T_PATH " + JecnCommonSql.getConcatChar() + " '%'"
				+ "	AND c.T_PATH IS NOT NULL" + "	AND p.T_PATH IS NOT NULL" + "	AND c.is_dir in (2,3)"
				+ "   AND c.del_state=0" + "	AND p.ID in (" + ruleIds + "))";
		return sql;
	}

	/**
	 * 获得下两个月需要审视的流程的数量与部门id
	 * 
	 * @param dutyOrgIds
	 *            部门的ids
	 * @param projectId
	 *            项目id
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @return objs
	 */
	@Override
	public List<Object[]> getNextTwoMonthScanList(String dutyOrgIds, String nextScanStartTime, String nextScanEndTime,
			long projectId, String processMapIds, String dutyIds, boolean orgHasChild) throws Exception {
		String orgSql = getDutyOrgSubSource(dutyOrgIds, 0, orgHasChild);
		String sql = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "with tmp as("// 有多次发布记录的数据
					+ "	select d.RELATE_ID,"
					+ "	       d.PUBLISH_DATE,"
					+ "	       max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE"
					+ "	  from (select a.PUBLISH_DATE,"
					+ "	               a.RELATE_ID,"
					+ "	               b.PUBLISH_DATE as last_PUBLISH_DATE,"
					+ "	               b.RELATE_ID as last_RELATE_ID"
					+ "	          from JECN_TASK_HISTORY_NEW a"
					+ "	          left join JECN_TASK_HISTORY_NEW b on b.type = 0 and b.expiry<>0"
					+ "	                                           and b.type = a.type"
					+ "	                                           and b.relate_id = a.relate_id"
					+ "	                                           and b.publish_date <"
					+ "	                                               a.publish_date"
					+ "	         where a.type = 0"
					+ "	           and a.expiry <> 0"
					+ "	           and b.publish_date is not null) d"
					+ "	 group by d.PUBLISH_DATE, d.RELATE_ID"
					+ " union all"
					+ "	select t1.RELATE_ID,"// 有多次发布记录的数据时最新一次发布数据 只有一次发布的数据
					+ "	       MAX(T1.PUBLISH_DATE) AS PUBLISH_DATE,"
					+ "	       MAX(T1.PUBLISH_DATE) AS last_PUBLISH_DATE"
					+ "	  from JECN_TASK_HISTORY_NEW t1"
					+ "	 where t1.type = 0"
					+ "	   and t1.expiry <> 0"
					+ "	 group by t1.RELATE_ID, t1.type"
					+ "	having(count(*) >= 1)"
					+ "	 )"
					+ "	select sum(c.totalnum) as totalCount,c.org_id"
					+ "	  from (select f.flow_name, "// 需审视流程
					+ "               tmp.last_PUBLISH_DATE, "// 需审视流程发布时间
					+ "               org.org_name, "// 责任部门
					+ "           org.org_id,"
					+ "            tmp.publish_date, "// 当前发布时间
					+ "            1 as totalnum,"
					+ "         tmp.RELATE_ID"
					+ "     from tmp"
					+ "     left join JECN_TASK_HISTORY_NEW x on x.type = 0"
					+ "     and x.expiry <> 0"
					+ "     and tmp.RELATE_ID = x.relate_id"
					+ "      and tmp.last_PUBLISH_DATE ="
					+ "      x.publish_date"
					+ "     inner join JECN_FLOW_STRUCTURE f on tmp.RELATE_ID = f.flow_id"
					+ "                                  and f.isflow = 1 and f.del_state=0"
					+ "     inner join JECN_FLOW_BASIC_INFO fi      on fi.flow_id = f.flow_id"
					+ "     inner join "
					+ orgSql + " org on org.org_id = f.flow_id and org.PROJECTID =" + projectId + "    where 1=1 ";
			if (StringUtils.isNotBlank(processMapIds)) {
				sql += " and f.flow_id in " + getChildFlows(processMapIds);
			}
			if (StringUtils.isNotBlank(dutyIds)) {
				sql += " and fi.res_people_id in (" + dutyIds + ") ";
			}
			sql += " and (x.next_scan_date between" + "      to_date('" + nextScanStartTime
					+ "', 'yyyy-mm-dd hh24:mi:ss') and" + "       to_date('" + nextScanEndTime
					+ "', 'yyyy-mm-dd hh24:mi:ss'))) c" + " group by c.org_id, c.org_name";

		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {

			sql = "with tmp as("// 有多次发布记录的数据
					+ "	select d.RELATE_ID,"
					+ "	       d.PUBLISH_DATE,"
					+ "	       max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE"
					+ "	  from (select a.PUBLISH_DATE,"
					+ "	               a.RELATE_ID,"
					+ "	               b.PUBLISH_DATE as last_PUBLISH_DATE,"
					+ "	               b.RELATE_ID as last_RELATE_ID"
					+ "	          from JECN_TASK_HISTORY_NEW a"
					+ "	          left join JECN_TASK_HISTORY_NEW b on b.type = 0 and b.expiry<>0"
					+ "	                                           and b.type = a.type"
					+ "	                                           and b.relate_id = a.relate_id"
					+ "	                                           and b.publish_date <"
					+ "	                                               a.publish_date"
					+ "	         where a.type = 0"
					+ "	           and a.expiry <> 0"
					+ "	           and b.publish_date is not null) d"
					+ "	 group by d.PUBLISH_DATE, d.RELATE_ID"
					+ " union all"
					+ "	select t1.RELATE_ID,"// 有多次发布记录的数据时最新一次发布数据 只有一次发布的数据
					+ "	       MAX(T1.PUBLISH_DATE) AS PUBLISH_DATE,"
					+ "	       MAX(T1.PUBLISH_DATE) AS last_PUBLISH_DATE"
					+ "	  from JECN_TASK_HISTORY_NEW t1"
					+ "	 where t1.type = 0"
					+ "	   and t1.expiry <> 0"
					+ "	 group by t1.RELATE_ID, t1.type"
					+ "	having(count(*) >= 1)"
					+ "	 )"
					+ "	select sum(c.totalnum) as totalCount,c.org_id"
					+ "	  from (select f.flow_name, "// 需审视流程
					+ "               tmp.last_PUBLISH_DATE, "// 需审视流程发布时间
					+ "               org.org_name, "// 责任部门
					+ "           org.org_id,"
					+ "            tmp.publish_date, "// 当前发布时间
					+ "            1 as totalnum,"
					+ "         tmp.RELATE_ID"
					+ "     from tmp"
					+ "     left join JECN_TASK_HISTORY_NEW x on x.type = 0"
					+ "     and x.expiry <> 0"
					+ "     and tmp.RELATE_ID = x.relate_id"
					+ "      and tmp.last_PUBLISH_DATE ="
					+ "      x.publish_date"
					+ "     inner join JECN_FLOW_STRUCTURE f on tmp.RELATE_ID = f.flow_id"
					+ "                                  and f.isflow = 1 and f.del_state=0"
					+ "     inner join JECN_FLOW_BASIC_INFO fi      on fi.flow_id = f.flow_id"
					+ "     inner join "
					+ orgSql + " org on org.org_id = f.flow_id and org.PROJECTID =" + projectId + "    where 1=1 ";
			if (StringUtils.isNotBlank(processMapIds)) {
				sql += " and f.flow_id in " + getChildFlows(processMapIds);
			}
			if (StringUtils.isNotBlank(dutyIds)) {
				sql += " and fi.res_people_id in (" + dutyIds + ") ";
			}
			sql += "  and convert(varchar(10),x.next_scan_date,23) between" + "      '" + nextScanStartTime + "' and"
					+ "      '" + nextScanEndTime + "') c" + " group by c.org_id, c.org_name";
		}

		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getAllMapProcessScanOptimizeList(List<Object[]> idAndNames, String startTimeAccurate,
			String endTimeAccurate, long projectId, String dutyIds) {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			StringBuffer sqlBuf = new StringBuffer();

			// 获得选中下流程架构下未删除的所有的流程图
			int index = 0;
			for (Object[] arr : idAndNames) {
				if (index == 0) {
					sqlBuf.append("select jfs.*," + arr[0] + " as map_id ,'" + arr[1] + "' as map_name"
							+ " from jecn_flow_structure jfs" + " where jfs.del_state = 0 and jfs.isflow=1"
							+ " and jfs.projectid=" + projectId + " START WITH jfs.flow_id = " + arr[0]// 开始的子节点
							+ " CONNECT BY PRIOR jfs.flow_id = jfs.pre_flow_id ");// 子id与父id互换决定向下还是向上查找"

				} else {// 不是第一次添加union关键字
					sqlBuf.append(" union select jfs.*," + arr[0] + " as map_id ,'" + arr[1] + "' as map_name"
							+ " from jecn_flow_structure jfs" + " where jfs.del_state = 0 and jfs.isflow=1"
							+ " and jfs.projectid=" + projectId + " START WITH jfs.flow_id = " + arr[0]// 开始的子节点
							+ " CONNECT BY PRIOR jfs.flow_id = jfs.pre_flow_id ");// 子id与父id互换决定向下还是向上查找"
				}

				index++;
			}

			sql = "with tmp as"
					+ " (select d.RELATE_ID,"
					+ "         d.PUBLISH_DATE,"// 当前版本审视时间
					+ "         max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE"// 当前版本发布时间
					+ "    from (select a.PUBLISH_DATE," + "                 a.RELATE_ID,"
					+ "                 b.PUBLISH_DATE as last_PUBLISH_DATE,"
					+ "                 b.RELATE_ID    as last_RELATE_ID" + "            from JECN_TASK_HISTORY_NEW a"
					+ "            left join JECN_TASK_HISTORY_NEW b" + "              on b.type = 0"
					+ "             and b.expiry <> 0" + "             and b.type = a.type"
					+ "             and b.relate_id = a.relate_id" + "             and b.publish_date < a.publish_date"
					+ "           where a.type = 0" + "             and b.publish_date is not null) d"
					+ "   group by d.PUBLISH_DATE, d.RELATE_ID" + "  union all"
					+ " select t4.relate_id, t4.publish_date, t4.last_publish_date" + "    from ("
					+ "          select t2.relate_id, t2.publish_date, t2.last_publish_date"
					+ "            from (select t1.relate_id,"
					+ "                           max(t1.publish_date) as publish_date,"
					+ "                           max(t1.publish_date) as last_publish_date"
					+ "                     from jecn_task_history_new t1" + "                    where t1.type = 0"
					+ "                    group by t1.relate_id) t2"
					+ "           inner join jecn_task_history_new t3" + "              on t2.relate_id = t3.relate_id"
					+ "             and t2.publish_date = t3.publish_date" + "             and t3.expiry <> 0"
					+ "             and t3.type = 0) t4" + ")," + "tmp2 as"
					+ " (select x.next_scan_date AS last_expect_publish_date," + "         f.flow_id,"
					+ "         f.flow_name," + "         x.VERSION_ID," + "         tmp.last_PUBLISH_DATE,"
					+ "         x.Draft_Person," + "         f.map_id," + "         f.map_name," + "         case"
					+ "           when fi.Type_Res_People = 0 then" + "            u.true_name" + "           else"
					+ "            pos.figure_text" + "         end as flow_zr," + "         case"
					+ "           when fi.Type_Res_People = 1 then" + "            pos.figure_id"
					+ "         end as pos_id," + "         fi.Ward_People_Id," + "         case"
					+ "           when tmp.publish_date = tmp.last_PUBLISH_DATE then" + "            null"
					+ "           else" + "            tmp.publish_date" + "         end as publish_date,"
					+ "         case" + "           when tmp.publish_date = tmp.last_PUBLISH_DATE then"
					+ "            0" + "           when x.next_scan_date <=" + "                to_date('"
					+ endTimeAccurate
					+ "', 'yyyy-mm-dd hh24:mi:ss') and"
					+ "                to_date(to_char(tmp.PUBLISH_DATE, 'YYYY/MM/DD'), 'YYYY/MM/DD') <="
					+ "                x.next_scan_date then"
					+ "            1"
					+ "           else"
					+ "            0"
					+ "         end as finishNum,"
					+ "         1 as totalnum,"
					+ "         tmp.RELATE_ID,"
					+ "         x.MODIFY_EXPLAIN"
					+ "    from tmp"
					+ "    left join JECN_TASK_HISTORY_NEW x"
					+ "      on x.type = 0"
					+ "     and x.expiry <> 0"
					+ "     and tmp.RELATE_ID = x.relate_id"
					+ "     and tmp.last_PUBLISH_DATE = x.publish_date"
					+ "    inner join ( "
					+ sqlBuf.toString()// 选中的流程架构下所有的流程图
					+ " ) f  on tmp.RELATE_ID = f.flow_id"
					+ "    left join JECN_FLOW_BASIC_INFO fi"
					+ "      on fi.flow_id = f.flow_id"
					+ "    left join jecn_user u"
					+ "      on fi.Type_Res_People = 0"
					+ "     and fi.Res_People_Id = u.people_id"
					+ "    left join JECN_FLOW_ORG_IMAGE pos"
					+ "      on fi.Type_Res_People = 1"
					+ "     and fi.res_people_id = pos.figure_id" + "   where ";

			if (StringUtils.isNotBlank(dutyIds)) {
				sql += " fi.Res_People_Id in (" + dutyIds + ")" + " and";
			}
			sql += " x.next_scan_date between" + "         to_date('" + startTimeAccurate
					+ "', 'yyyy-mm-dd hh24:mi:ss') and" + "         to_date('" + endTimeAccurate
					+ "', 'yyyy-mm-dd hh24:mi:ss')) ," + " tmp3 as" + " (select " + "   a.last_expect_publish_date, "
					+ "   a.flow_id," + "   a.flow_name," + "   a.VERSION_ID," + "   a.last_PUBLISH_DATE,"
					+ "   a.Draft_Person," + "   a.map_id as process_map_id," + "   a.map_name as process_map_name,"
					+ "   a.flow_zr," + "   a.publish_date," + "   a.Ward_People_Id," + "         b.finishCount,"
					+ "         b.totalCount,"
					+ "         trunc((b.finishCount / b.totalCount), 2) * 100 as finishperce,"
					+ "         a.MODIFY_EXPLAIN " + "    from tmp2 a"
					+ "    left join (select sum(c.finishNum) as finishCount,"
					+ "                     sum(c.totalnum) as totalCount," + "                     c.map_id"
					+ "                from tmp2 c" + "               group by c.map_id) b"
					+ "      on a.map_id = b.map_id )" + " select distinct tmp3.last_expect_publish_date,"
					+ "                tmp3.flow_id," + "                tmp3.flow_name,"
					+ "                tmp3.VERSION_ID," + "                tmp3.last_PUBLISH_DATE,"
					+ "                tmp3.Draft_Person," + "                tmp3.process_map_id,"
					+ "                tmp3.process_map_name," + "                tmp3.flow_zr,"
					+ "                jorg.org_id as g_id," + "                jorg.org_name as g_name,"
					+ "                ju.people_id," + "                ju.true_name,"
					+ "                tmp3.publish_date," + "                tmp3.finishCount,"
					+ "                tmp3.totalCount," + "                tmp3.finishperce," + "                case"
					+ "                  when to_char(tmp3.last_expect_publish_date, 'YYYY/MM/DD') >="
					+ "                       to_char(tmp3.publish_date, 'YYYY/MM/DD') and"
					+ "                       tmp3.publish_date is not null then" + "                   1"
					+ "                  when to_char(tmp3.last_expect_publish_date, 'YYYY/MM/DD') >="
					+ "                       to_char(sysdate, 'YYYY/MM/DD') and"
					+ "                       tmp3.publish_date is null then" + "                   2"
					+ "                  else" + "                   0" + "                end as scan_ontime,"
					+ "                jfo.org_name," + "                tmp3.MODIFY_EXPLAIN,"
					+ "                jfo.org_id as true_org_id," + "                jfo.org_name as true_org_name"
					+ "  from tmp3" + "  left join jecn_user ju" + "    on tmp3.Ward_People_Id = ju.people_id"
					+ "  left join JECN_USER_POSITION_RELATED jref" + "    on jref.people_id = ju.people_id"
					+ "  left join JECN_FLOW_ORG_IMAGE jpos" + "    on jref.figure_id = jpos.figure_id"
					+ "  left join JECN_FLOW_ORG jorg" + "    on jorg.org_id = jpos.org_id"
					+ "   and jorg.PROJECTID = 1"
					+ " left join JECN_FLOW_RELATED_ORG jfro on  tmp3.flow_id=jfro.flow_id"
					+ " left join jecn_flow_org jfo on jfo.org_id=jfro.org_id"
					+ " order by  tmp3.publish_date,tmp3.flow_id,tmp3.process_map_id asc";

		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {

			// 选中的流程架构中的所有的流程图与流程地图的临时表
			StringBuffer sqlBuf = new StringBuffer();
			// 从选中的临时表中选出流程图
			StringBuffer selectFlowBuf = new StringBuffer();
			String withName = "flow";
			// 获得选中下流程架构下未删除的所有的流程图
			int index = 0;
			for (Object[] arr : idAndNames) {
				String flowTempName = withName + index;

				sqlBuf.append(", " + flowTempName + " AS" + "  (SELECT JFT.*," + arr[0] + " as map_id ," + "'" + arr[1]
						+ "' as map_name " + "   FROM JECN_FLOW_STRUCTURE JFT" + "   WHERE JFT.Flow_Id = " + arr[0]
						+ " and jft.del_state=0  and JFT.projectid=" + projectId + "  UNION ALL SELECT JFS.*, "
						+ arr[0] + " as map_id ," + "'" + arr[1] + "' as map_name " + "  FROM " + flowTempName + ""
						+ "  INNER JOIN JECN_FLOW_STRUCTURE JFS ON " + flowTempName + ".Flow_Id=JFS.Pre_Flow_Id"
						+ " and jfs.del_state=0 and JFS.projectid=" + projectId + ")");

				if (index == 0) {
					selectFlowBuf.append(" select * from " + flowTempName + " where isflow=1");
				} else {

					selectFlowBuf.append(" union all select * from " + flowTempName + " where isflow=1 ");
				}
				index++;
			}

			sql = "with tmp as" + " (select d.RELATE_ID," + "         d.PUBLISH_DATE,"
					+ "         max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE" + "    from (select a.PUBLISH_DATE,"
					+ "                 a.RELATE_ID," + "                 b.PUBLISH_DATE as last_PUBLISH_DATE,"
					+ "                 b.RELATE_ID    as last_RELATE_ID" + "            from JECN_TASK_HISTORY_NEW a"
					+ "            left join JECN_TASK_HISTORY_NEW b" + "              on b.type = 0"
					+ "             and b.expiry <> 0" + "             and b.type = a.type"
					+ "             and b.relate_id = a.relate_id" + "             and b.publish_date < a.publish_date"
					+ "           where a.type = 0" + "             and b.publish_date is not null) d"
					+ "   group by d.PUBLISH_DATE, d.RELATE_ID" + "  union all "
					+ " select t4.relate_id, t4.publish_date, t4.last_publish_date" + "    from ("
					+ "          select t2.relate_id, t2.publish_date, t2.last_publish_date"
					+ "            from (select t1.relate_id,"
					+ "                           max(t1.publish_date) as publish_date,"
					+ "                           max(t1.publish_date) as last_publish_date"
					+ "                     from jecn_task_history_new t1" + "                    where t1.type = 0"
					+ "                    group by t1.relate_id) t2"
					+ "           inner join jecn_task_history_new t3" + "              on t2.relate_id = t3.relate_id"
					+ "             and t2.publish_date = t3.publish_date" + "             and t3.expiry <> 0"
					+ "             and t3.type = 0) t4" + " )"
					+
					// 所有选中流程架构的流程图的临时表
					sqlBuf.toString()
					+ " ,tmp2 as"
					+ " (select"
					+ " dateadd(month,x.expiry,tmp.last_PUBLISH_DATE) AS last_expect_publish_date,"
					+ "         f.flow_id,"
					+ "         f.flow_name,"
					+ "         x.VERSION_ID,"
					+ "         tmp.last_PUBLISH_DATE,"
					+ "         x.Draft_Person,"
					+ "         f.map_id,"
					+ "         f.map_name,"
					+ "         case"
					+ "           when fi.Type_Res_People = 0 then"
					+ "            u.true_name"
					+ "           else"
					+ "            pos.figure_text"
					+ "         end as flow_zr,"
					+ "         case"
					+ "           when fi.Type_Res_People = 1 then"
					+ "            pos.figure_id"
					+ "         end as pos_id,"
					+ "         fi.Ward_People_Id,"
					+ "         case"
					+ "           when tmp.publish_date = tmp.last_PUBLISH_DATE then"
					+ "            null"
					+ "           else"
					+ "            tmp.publish_date"
					+ "         end as publish_date,"
					+ "         case"
					+ "          WHEN tmp.publish_date = tmp.last_PUBLISH_DATE "
					+ "          THEN 0"
					+ "           when"
					+ "             CONVERT(varchar(10), dateadd(month,x.expiry,tmp.last_PUBLISH_DATE) , 23) <="
					+ "              CONVERT(varchar(10), '"
					+ endTimeAccurate
					+ "' , 23)"
					+ "               and"
					+ "              CONVERT(varchar(10), tmp.PUBLISH_DATE, 23)<="
					+ "              CONVERT(varchar(10), dateadd(month,x.expiry,tmp.last_PUBLISH_DATE) , 23) "
					+ " and tmp.PUBLISH_DATE is not null"
					+ "            then"
					+ "            1"
					+ "           else"
					+ "            0"
					+ "         end as finishNum,"
					+ "         1 as totalnum,"
					+ "         tmp.RELATE_ID,"
					+ "         x.MODIFY_EXPLAIN"
					+ "    from tmp"
					+ "    left join JECN_TASK_HISTORY_NEW x"
					+ "      on x.type = 0"
					+ "     and x.expiry <> 0"
					+ "     and tmp.RELATE_ID = x.relate_id"
					+ "     and tmp.last_PUBLISH_DATE = x.publish_date"
					+ "   inner join ("
					+
					// 取出所有的选中的流程架构下的流程图
					selectFlowBuf.toString()
					+ ") f "
					+ "      on tmp.RELATE_ID = f.flow_id"
					+ "    left join JECN_FLOW_BASIC_INFO fi"
					+ "      on fi.flow_id = f.flow_id"
					+ "    left join jecn_user u"
					+ "      on fi.Type_Res_People = 0"
					+ "     and fi.Res_People_Id = u.people_id"
					+ "    left join JECN_FLOW_ORG_IMAGE pos"
					+ "      on fi.Type_Res_People = 1" + "     and fi.res_people_id = pos.figure_id" + "   where ";
			if (StringUtils.isNotBlank(dutyIds)) {
				sql += " fi.Res_People_Id in (" + dutyIds + ")" + " and";
			}
			sql += " CONVERT(varchar(10), dateadd(month,x.expiry,tmp.last_PUBLISH_DATE), 23) between"
					+ " CONVERT(varchar(10), '"
					+ startTimeAccurate
					+ "', 23)"
					+ "          and "
					+ " CONVERT(varchar(10), '"
					+ endTimeAccurate
					+ "', 23)"
					+ " ),tmp3 as"
					+ " (select a.last_expect_publish_date,"
					+ "         a.flow_id,"
					+ "         a.flow_name,"
					+ "         a.VERSION_ID,"
					+ "         a.last_PUBLISH_DATE,"
					+ "         a.Draft_Person,"
					+ "         a.map_id ,"
					+ "         a.map_name ,"
					+ "         a.flow_zr,"
					+ "         a.publish_date,"
					+ "         a.Ward_People_Id,"
					+ "         b.finishCount,"
					+ "         b.totalCount,"
					+ "         floor(b.finishCount*100/ b.totalCount) as finishperce,"
					+ "         a.MODIFY_EXPLAIN"
					+ "    from tmp2 a"
					+ "    left join (select sum(c.finishNum) as finishCount,"
					+ "                     sum(c.totalnum) as totalCount,"
					+ "                     c.map_id"
					+ "                from tmp2 c"
					+ "               group by c.map_id) b"
					+ "      on a.map_id = b.map_id)"
					+ " select  CONVERT(varchar(10), tmp3.last_expect_publish_date, 23) as last_expect_publish_date,"
					+ "                tmp3.flow_id,"
					+ "                tmp3.flow_name,"
					+ "                tmp3.VERSION_ID,"
					+ "                CONVERT(varchar(10), tmp3.last_PUBLISH_DATE, 23) as last_PUBLISH_DATE,"
					+ "                tmp3.Draft_Person,"
					+ "                tmp3.map_id,"
					+ "                tmp3.map_name,"
					+ "                tmp3.flow_zr,"
					+ "                jorg.org_id as g_id,"
					+ "                jorg.org_name as g_name,"
					+ "                ju.people_id,"
					+ "                ju.true_name,"
					+ "                CONVERT(varchar(10),tmp3.publish_date, 23) as publish_date,"
					+ "                tmp3.finishCount,"
					+ "                tmp3.totalCount,"
					+ "                tmp3.finishperce,"
					+ "                case"
					+ "                  when"
					+ "               CONVERT(varchar(10), tmp3.last_expect_publish_date, 23)"
					+ "                        >="
					+ "               CONVERT(varchar(10), tmp3.publish_date, 23)"
					+ "                        and"
					+ "               CONVERT(varchar(10), tmp3.publish_date, 23) is not null then"
					+ "                   1"
					+ "                  when"
					+ " CONVERT(varchar(10), tmp3.last_expect_publish_date, 23)"
					+ // 只要在当天完成就是有效，不管时分秒 所以varchar(10) 只保留年月日
					"                       >="
					+ " CONVERT(varchar(10), getdate(), 23)"
					+ "                        and"
					+ " CONVERT(varchar(10), tmp3.publish_date, 23)"
					+ "                        is null then"
					+ "                   2"
					+ "                  else"
					+ "                   0"
					+ "                end as scan_ontime,"
					+ "                jfo.org_name,"
					+ "         tmp3.MODIFY_EXPLAIN,"
					+ "                jfo.org_id as true_org_id,"
					+ "                jfo.org_name as true_org_name"
					+ "  from tmp3"
					+ "  left join jecn_user ju"
					+ "    on tmp3.Ward_People_Id = ju.people_id"
					+ "  left join JECN_USER_POSITION_RELATED jref"
					+ "    on jref.people_id = ju.people_id"
					+ "  left join JECN_FLOW_ORG_IMAGE jpos"
					+ "    on jref.figure_id = jpos.figure_id"
					+ "  left join JECN_FLOW_ORG jorg"
					+ "    on jorg.org_id = jpos.org_id"
					+ "   and jorg.PROJECTID ="
					+ projectId
					+ " left join JECN_FLOW_RELATED_ORG jfro on  tmp3.flow_id=jfro.flow_id"
					+ " left join jecn_flow_org jfo on jfo.org_id=jfro.org_id"
					+ " order by tmp3.publish_date, tmp3.flow_id,tmp3.map_id";
		}

		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getMapNextTwoMonthScanList(String processMapIds, String nextScanStartTime,
			String nextScanEndTime, long projectId, String dutyIds) {
		String sql = "";
		String[] processMapIdArr = processMapIds.split(",");
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			StringBuffer processBuf = new StringBuffer();
			// 选中的流程地图下的所有的流程图
			for (int i = 0; i < processMapIdArr.length; i++) {
				if (i == 0) {
					processBuf.append(" select jfs.*," + processMapIdArr[i] + " as map_id "
							+ "  from jecn_flow_structure jfs"
							+ " where jfs.del_state = 0 and jfs.isflow=1 and jfs.del_state = 0 and jfs.projectid=1"
							+ " START WITH jfs.flow_id = " + processMapIdArr[i] // 开始的子节点
							+ " CONNECT BY PRIOR jfs.flow_id = jfs.pre_flow_id"); // 子id与父id互换决定向下还是向上查找
				} else {
					processBuf.append(" union select jfs.*," + processMapIdArr[i] + " as map_id "
							+ "  from jecn_flow_structure jfs"
							+ " where jfs.del_state = 0 and jfs.isflow=1 and jfs.del_state = 0 and jfs.projectid=1"
							+ " START WITH jfs.flow_id = " + processMapIdArr[i] // 开始的子节点
							+ " CONNECT BY PRIOR jfs.flow_id = jfs.pre_flow_id"); // 子id与父id互换决定向下还是向上查找
				}
			}

			sql = "with tmp as" + " (select d.RELATE_ID," + "         d.PUBLISH_DATE,"
					+ "         max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE" + "    from (select a.PUBLISH_DATE,"
					+ "                 a.RELATE_ID," + "                 b.PUBLISH_DATE as last_PUBLISH_DATE,"
					+ "                 b.RELATE_ID    as last_RELATE_ID" + "            from JECN_TASK_HISTORY_NEW a"
					+ "            left join JECN_TASK_HISTORY_NEW b" + "              on b.type = 0"
					+ "             and b.expiry <> 0" + "             and b.type = a.type"
					+ "             and b.relate_id = a.relate_id" + "             and b.publish_date < a.publish_date"
					+ "           where a.type = 0" + "             and a.expiry <> 0"
					+ "             and b.publish_date is not null) d" + "   group by d.PUBLISH_DATE, d.RELATE_ID"
					+ "  union all" + "  select t1.RELATE_ID," + "         MAX(T1.PUBLISH_DATE) AS PUBLISH_DATE,"
					+ "         MAX(T1.PUBLISH_DATE) AS last_PUBLISH_DATE" + "    from JECN_TASK_HISTORY_NEW t1"
					+ "   where t1.type = 0" + "     and t1.expiry <> 0" + "   group by t1.RELATE_ID, t1.type"
					+ "  having(count(*) >= 1))" + "select sum(c.totalnum) as totalCount, c.map_id"
					+ "  from (select tmp.last_PUBLISH_DATE," + "               map.map_id,"
					+ "               tmp.publish_date," + "               1 as totalnum,"
					+ "               tmp.RELATE_ID" + "          from tmp"
					+ "          left join JECN_TASK_HISTORY_NEW x" + "            on x.type = 0"
					+ "           and x.expiry <> 0" + "           and tmp.RELATE_ID = x.relate_id"
					+ "           and tmp.last_PUBLISH_DATE = x.publish_date" + " inner join(" + processBuf.toString()
					+ " )map on map.flow_id=tmp.RELATE_ID" + "           and (x.next_scan_date between"
					+ "               to_date('" + nextScanStartTime + "', 'yyyy-mm-dd hh24:mi:ss') and"
					+ "               to_date('" + nextScanEndTime + "', 'yyyy-mm-dd hh24:mi:ss'))";
			if (StringUtils.isNotBlank(dutyIds)) {
				sql += " inner join JECN_FLOW_BASIC_INFO fi on fi.flow_id=map.flow_id and fi.res_people_id in ("
						+ dutyIds + ") ";
			}
			sql += ") c" + " group by c.map_id";

		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			// 选中的流程架构中的所有的流程图与流程地图的临时表
			StringBuffer sqlBuf = new StringBuffer();
			// 从选中的临时表中选出流程图
			StringBuffer selectFlowBuf = new StringBuffer();
			String withName = "flow";
			// 获得选中下流程架构下未删除的所有的流程图
			for (int i = 0; i < processMapIdArr.length; i++) {
				String flowTempName = withName + i;
				sqlBuf.append(", " + flowTempName + " AS" + "  (SELECT JFT.*," + processMapIdArr[i] + " as map_id "
						+ "   FROM JECN_FLOW_STRUCTURE JFT" + "   WHERE JFT.Flow_Id = " + processMapIdArr[i]
						+ " and jft.del_state=0 and JFT.projectid=" + projectId + "  UNION ALL SELECT JFS.*, "
						+ processMapIdArr[i] + " as map_id " + "  FROM " + flowTempName + ""
						+ "  INNER JOIN JECN_FLOW_STRUCTURE JFS ON " + flowTempName + ".Flow_Id=JFS.Pre_Flow_Id"
						+ " and jfs.del_state=0 and JFS.projectid=" + projectId + ")");

				if (i == 0) {

					selectFlowBuf.append(" select * from " + flowTempName + " where isflow=1");
				} else {

					selectFlowBuf.append(" union all select * from " + flowTempName + " where isflow=1 ");
				}

			}

			sql = "with tmp as" + " (select d.RELATE_ID," + "         d.PUBLISH_DATE,"
					+ "         max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE" + "    from (select a.PUBLISH_DATE,"
					+ "                 a.RELATE_ID," + "                 b.PUBLISH_DATE as last_PUBLISH_DATE,"
					+ "                 b.RELATE_ID    as last_RELATE_ID" + "            from JECN_TASK_HISTORY_NEW a"
					+ "            left join JECN_TASK_HISTORY_NEW b" + "              on b.type = 0"
					+ "             and b.expiry <> 0" + "             and b.type = a.type"
					+ "             and b.relate_id = a.relate_id" + "             and b.publish_date < a.publish_date"
					+ "           where a.type = 0" + "             and a.expiry <> 0"
					+ "             and b.publish_date is not null) d" + "   group by d.PUBLISH_DATE, d.RELATE_ID"
					+ "  union all" + "  select t1.RELATE_ID," + "         MAX(T1.PUBLISH_DATE) AS PUBLISH_DATE,"
					+ "         MAX(T1.PUBLISH_DATE) AS last_PUBLISH_DATE" + "    from JECN_TASK_HISTORY_NEW t1"
					+ "   where t1.type = 0" + "     and t1.expiry <> 0" + "   group by t1.RELATE_ID, t1.type"
					+ "  having(count(*) >= 1))"
					+ sqlBuf.toString()
					+ "select sum(c.totalnum) as totalCount, c.map_id"
					+ "  from (select tmp.last_PUBLISH_DATE,"
					+ "               map.map_id,"
					+ "               tmp.publish_date,"
					+ "               1 as totalnum,"
					+ "               tmp.RELATE_ID"
					+ "          from tmp"
					+ "          left join JECN_TASK_HISTORY_NEW x"
					+ "            on x.type = 0"
					+ "           and x.expiry <> 0"
					+ "           and tmp.RELATE_ID = x.relate_id"
					+ "           and tmp.last_PUBLISH_DATE = x.publish_date"
					+ " inner join("
					+ selectFlowBuf.toString()
					+ ")map on map.flow_id=tmp.RELATE_ID"
					+ "           and (dateadd(month,x.expiry,tmp.last_PUBLISH_DATE) between"
					+ "               convert(varchar(20),'"
					+ nextScanStartTime
					+ "',23) and"
					+ "               convert(varchar(20),'" + nextScanEndTime + "',23))";
			if (StringUtils.isNotBlank(dutyIds)) {
				sql += " inner join JECN_FLOW_BASIC_INFO fi on fi.flow_id=map.flow_id and fi.res_people_id in ("
						+ dutyIds + ")";
			}
			sql += ") c" + " group by c.map_id";

		}

		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getMapProcessKPIFollowList(String processMapIds, long projectId, String startTime,
			String endTime) {
		String sql = "";
		String[] processMapIdArr = processMapIds.split(",");
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			StringBuffer processBuf = new StringBuffer();
			// 选中的流程地图下的所有的流程图
			for (int i = 0; i < processMapIdArr.length; i++) {
				if (i == 0) {
					processBuf.append(" select jfs.*," + processMapIdArr[i] + " as map_id"
							+ "  from jecn_flow_structure jfs"
							+ " where jfs.del_state = 0 and jfs.isflow=1  and jfs.projectid=1"
							+ " START WITH jfs.flow_id = " + processMapIdArr[i] // 开始的子节点
							+ " CONNECT BY PRIOR jfs.flow_id = jfs.pre_flow_id"); // 子id与父id互换决定向下还是向上查找
				} else {
					processBuf.append(" union select jfs.*," + processMapIdArr[i] + " as map_id"
							+ "  from jecn_flow_structure jfs"
							+ " where jfs.del_state = 0 and jfs.isflow=1  and jfs.projectid=" + projectId
							+ " START WITH jfs.flow_id = " + processMapIdArr[i] // 开始的子节点
							+ " CONNECT BY PRIOR jfs.flow_id = jfs.pre_flow_id"); // 子id与父id互换决定向下还是向上查找
				}
			}

			sql = "SELECT F.FLOW_NAME," + "       ORG.ORG_NAME," + "       CASE"
					+ "         WHEN FI.TYPE_RES_PEOPLE = 0 THEN" + "          U2.TRUE_NAME" + "         ELSE"
					+ "          POS.FIGURE_TEXT" + "       END AS FLOW_ZR," + "       CASE"
					+ "         WHEN FI.TYPE_RES_PEOPLE = 1 THEN" + "          POS.FIGURE_ID" + "       END AS POS_ID,"
					+ "       U3.TRUE_NAME as wardPeopleName," + "       U4.TRUE_NAME as draftPersonName,"
					+ "       KPI.KPI_NAME," + "       KPI.KPI_TARGET_TYPE," + "       FT.TARGER_CONTENT,"
					+ "       KPI.KPI_RELEVANCE," + "       KPI.KPI_STATISTICAL_METHODS,"
					+ "       KPI.KPI_TARGET_OPERATOR," + "       KPI.KPI_TARGET," + "       KPI.KPI_VERTICAL,"
					+ "       KPI.KPI_DATA_METHOD," + "       ST.FLOW_SUSTAIN_TOOL_DESCRIBE as itSystem,"
					+ "       U.TRUE_NAME as kpiDataPeopleName," + "       KPI.KPI_HORIZONTAL," + "       F.MAP_ID,"
					+ "       F.FLOW_ID," + "       KPI.KPI_AND_ID AS KPIID," + "       KV.KPI_ID AS KPIVALUEID,"
					+ "       KV.KPI_HOR_VALUE," + "       KV.KPI_VALUE," + "       JF.FLOW_NAME AS MAP_NAME,"
					+ "       KPI.KPI_PURPOSE," + "       KPI.KPI_POINT," + "       KPI.KPI_PERIOD,"
					+ "       KPI.KPI_EXPLAIN," + "       KPI.KPI_DEFINITION," + "       KPI.KPI_TYPE"
					+ "  FROM JECN_FLOW_KPI_NAME KPI" + "  INNER JOIN" + "  ("
					+ processBuf.toString()
					+ // 选中的流程架构下的流程图
					"  )F"
					+ "    ON KPI.FLOW_ID = F.FLOW_ID"
					+ "   AND F.ISFLOW = 1"
					+ "  INNER JOIN JECN_FLOW_STRUCTURE JF ON F.MAP_ID=JF.FLOW_ID and jf.del_state=0 "
					+ "  LEFT JOIN JECN_FLOW_RELATED_ORG ZO"
					+ "    ON F.FLOW_ID = ZO.FLOW_ID"
					+ "  LEFT JOIN"
					+ "  JECN_FLOW_ORG ORG"
					+ "    ON ORG.ORG_ID = ZO.ORG_ID"
					+ "   AND ORG.PROJECTID = 1"
					+ "  LEFT JOIN JECN_FIRST_TARGER FT"
					+ "    ON FT.ID = KPI.FIRST_TARGER_ID"
					+ "  LEFT JOIN JECN_SUSTAIN_TOOL_CONN_T KREF"
					+ "    ON KREF.RELATED_TYPE = 0"
					+ "   AND KREF.RELATED_ID = KPI.KPI_AND_ID"
					+ "  LEFT JOIN JECN_FLOW_SUSTAIN_TOOL ST"
					+ "    ON ST.FLOW_SUSTAIN_TOOL_ID = KREF.SUSTAIN_TOOL_ID"
					+ "  LEFT JOIN JECN_USER U"
					+ "    ON U.PEOPLE_ID = KPI.KPI_DATA_PEOPEL_ID"
					+ "  LEFT JOIN JECN_FLOW_BASIC_INFO FI"
					+ "    ON FI.FLOW_ID = F.FLOW_ID"
					+ "  LEFT JOIN JECN_USER U2"
					+ "    ON FI.TYPE_RES_PEOPLE = 0"
					+ "   AND FI.RES_PEOPLE_ID = U2.PEOPLE_ID"
					+ "  LEFT JOIN JECN_FLOW_ORG_IMAGE POS"
					+ "    ON FI.TYPE_RES_PEOPLE = 1"
					+ "   AND FI.RES_PEOPLE_ID = POS.FIGURE_ID"
					+ "  LEFT JOIN JECN_USER U3"
					+ "    ON FI.TYPE_RES_PEOPLE = 0"
					+ "   AND FI.WARD_PEOPLE_ID = U3.PEOPLE_ID"
					+ "  LEFT JOIN JECN_USER U4"
					+ "    ON FI.TYPE_RES_PEOPLE = 0"
					+ "   AND FI.FICTION_PEOPLE_ID = U4.PEOPLE_ID"
					+ "  LEFT JOIN JECN_FLOW_KPI KV"
					+ "    ON KV.KPI_AND_ID = KPI.KPI_AND_ID"
					+ " WHERE  F.DEL_STATE = 0"
					+ "   AND (((KV.KPI_HOR_VALUE BETWEEN"
					+ " TO_DATE((substr('"
					+ startTime
					+ "',0,7)||'-01'), 'YYYY-MM-DD') "
					+ // 开始日期的第一天
					" AND"
					+ "       LAST_DAY(TO_DATE('"
					+ endTime
					+ "', 'YYYY-MM-DD')))) OR"
					+ // 结束日期的最后一天
					"       KV.KPI_ID IS NULL)"
					+ " ORDER BY  F.MAP_ID,F.FLOW_ID, KV.KPI_AND_ID, KPI.KPI_AND_ID, KV.KPI_HOR_VALUE";

		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {

			// 选中的流程架构中的所有的流程图与流程地图的临时表
			StringBuffer sqlBuf = new StringBuffer();
			// 从选中的临时表中选出流程图
			StringBuffer selectFlowBuf = new StringBuffer();
			String withName = "flow";
			// 获得选中下流程架构下未删除的所有的流程图
			for (int i = 0; i < processMapIdArr.length; i++) {
				String flowTempName = withName + i;

				if (i == 0) {

					sqlBuf.append("with " + flowTempName + " AS" + "  (SELECT JFT.*," + "'" + processMapIdArr[i]
							+ "' as map_id " + "   FROM JECN_FLOW_STRUCTURE_T JFT" + "   WHERE JFT.Flow_Id = "
							+ processMapIdArr[i] + " and jft.del_state=0  and JFT.projectid=" + projectId
							+ "  UNION ALL SELECT JFS.*, " + "'" + processMapIdArr[i] + "' as map_id " + "  FROM "
							+ flowTempName + "" + "  INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON " + flowTempName
							+ ".Flow_Id=JFS.Pre_Flow_Id" + " and jfs.del_state=0 and JFS.projectid=" + projectId + ")");

					selectFlowBuf.append(" select * from " + flowTempName + " where isflow=1");
				} else {

					sqlBuf.append(", " + flowTempName + " AS" + "  (SELECT JFT.*," + "'" + processMapIdArr[i]
							+ "' as map_id " + "   FROM JECN_FLOW_STRUCTURE_T JFT" + "   WHERE JFT.Flow_Id = "
							+ processMapIdArr[i] + " and jft.del_state=0 and JFT.projectid=" + projectId
							+ "  UNION ALL SELECT JFS.*, " + "'" + processMapIdArr[i] + "' as map_id " + "  FROM "
							+ flowTempName + "" + "  INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON " + flowTempName
							+ ".Flow_Id=JFS.Pre_Flow_Id" + " and jfs.del_state=0 and JFS.projectid=" + projectId + ")");

					selectFlowBuf.append(" union all select * from " + flowTempName + " where isflow=1 ");
				}

			}

			sql = sqlBuf.toString() + "       SELECT F.FLOW_NAME," + "       ORG.ORG_NAME," + "       CASE"
					+ "         WHEN FI.TYPE_RES_PEOPLE = 0 THEN" + "          U2.TRUE_NAME" + "         ELSE"
					+ "          POS.FIGURE_TEXT" + "       END AS FLOW_ZR," + "       CASE"
					+ "         WHEN FI.TYPE_RES_PEOPLE = 1 THEN" + "          POS.FIGURE_ID" + "       END AS POS_ID,"
					+ "       U3.TRUE_NAME as wardPeopleName," + "       U4.TRUE_NAME as draftPersonName,"
					+ "       KPI.KPI_NAME," + "       KPI.KPI_TARGET_TYPE," + "       FT.TARGER_CONTENT,"
					+ "       KPI.KPI_RELEVANCE,"
					+ "       KPI.KPI_STATISTICAL_METHODS,"
					+ "       KPI.KPI_TARGET_OPERATOR,"
					+ "       KPI.KPI_TARGET,"
					+ "       KPI.KPI_VERTICAL,"
					+ "       KPI.KPI_DATA_METHOD,"
					+ "       ST.FLOW_SUSTAIN_TOOL_DESCRIBE as itSystem,"
					+ "       U.TRUE_NAME as kpiDataPeopleName,"
					+ "       KPI.KPI_HORIZONTAL,"
					+ "       F.MAP_ID,"
					+ "       F.FLOW_ID,"
					+ "       KPI.KPI_AND_ID AS KPIID,"
					+ "       KV.KPI_ID AS KPIVALUEID,"
					+ "       KV.KPI_HOR_VALUE,"
					+ "       KV.KPI_VALUE,"
					+ "       JF.FLOW_NAME AS MAP_NAME,"
					+ "       KPI.KPI_PURPOSE,"
					+ "       KPI.KPI_POINT,"
					+ "       KPI.KPI_PERIOD,"
					+ "       KPI.KPI_EXPLAIN,"
					+ "       KPI.KPI_DEFINITION,"
					+ "       KPI.KPI_TYPE"
					+ "  FROM JECN_FLOW_KPI_NAME KPI"
					+ "  INNER JOIN"
					+ "  ("
					+ selectFlowBuf.toString()
					+ // 选中的流程架构下的流程图
					"  )F" + "    ON KPI.FLOW_ID = F.FLOW_ID" + "   AND F.ISFLOW = 1"
					+ "  INNER JOIN JECN_FLOW_STRUCTURE JF ON F.MAP_ID=JF.FLOW_ID and jf.del_state=0 "
					+ "  LEFT JOIN JECN_FLOW_RELATED_ORG ZO" + "    ON F.FLOW_ID = ZO.FLOW_ID" + "  LEFT JOIN"
					+ "  JECN_FLOW_ORG ORG" + "    ON ORG.ORG_ID = ZO.ORG_ID" + "   AND ORG.PROJECTID = 1"
					+ "  LEFT JOIN JECN_FIRST_TARGER FT" + "    ON FT.ID = KPI.FIRST_TARGER_ID"
					+ "  LEFT JOIN JECN_SUSTAIN_TOOL_CONN_T KREF" + "    ON KREF.RELATED_TYPE = 0"
					+ "   AND KREF.RELATED_ID = KPI.KPI_AND_ID" + "  LEFT JOIN JECN_FLOW_SUSTAIN_TOOL ST"
					+ "    ON ST.FLOW_SUSTAIN_TOOL_ID = KREF.SUSTAIN_TOOL_ID" + "  LEFT JOIN JECN_USER U"
					+ "    ON U.PEOPLE_ID = KPI.KPI_DATA_PEOPEL_ID" + "  LEFT JOIN JECN_FLOW_BASIC_INFO FI"
					+ "    ON FI.FLOW_ID = F.FLOW_ID" + "  LEFT JOIN JECN_USER U2" + "    ON FI.TYPE_RES_PEOPLE = 0"
					+ "   AND FI.RES_PEOPLE_ID = U2.PEOPLE_ID" + "  LEFT JOIN JECN_FLOW_ORG_IMAGE POS"
					+ "    ON FI.TYPE_RES_PEOPLE = 1" + "   AND FI.RES_PEOPLE_ID = POS.FIGURE_ID"
					+ "  LEFT JOIN JECN_USER U3" + "    ON FI.TYPE_RES_PEOPLE = 0"
					+ "   AND FI.WARD_PEOPLE_ID = U3.PEOPLE_ID" + "  LEFT JOIN JECN_USER U4"
					+ "    ON FI.TYPE_RES_PEOPLE = 0" + "   AND FI.FICTION_PEOPLE_ID = U4.PEOPLE_ID"
					+ "  LEFT JOIN JECN_FLOW_KPI KV" + "    ON KV.KPI_AND_ID = KPI.KPI_AND_ID"
					+ " WHERE  F.DEL_STATE = 0 "
					+ "   AND ((CONVERT(VARCHAR(7),KV.KPI_HOR_VALUE,21) BETWEEN"
					+ // 去除日，以月份为搜索条件
					"       CONVERT(VARCHAR(7),'" + startTime + ",21') AND" + "       CONVERT(VARCHAR(7),'" + endTime
					+ ",21')) OR" + "       KV.KPI_ID IS NULL)"
					+ " ORDER BY  F.MAP_ID,F.FLOW_ID, KV.KPI_AND_ID, KPI.KPI_AND_ID, KV.KPI_HOR_VALUE";

		}

		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getProcessExpiryMaintenanceDateList(Set<Long> dutyOrgIds, Set<Long> processMapIds,
			Long projectId, int type) throws Exception {
		String sql = "";
		boolean recursion = JecnConfigTool.expiryRecursionEnable();
		if (type == 0) {
			if (dutyOrgIds.size() > 0 || processMapIds.size() > 0) {
				sql = "with";
			}
			String sqlOrg = " my_flow_org as (select distinct jfro.flow_id from JECN_FLOW_RELATED_ORG jfro where jfro.org_id in"
					+ JecnCommonSql.getIdsSet(dutyOrgIds) + " )";
			if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (recursion) {
					sqlOrg = "my_flow_org as ("
							+ "     select distinct jfro.flow_id from JECN_FLOW_RELATED_ORG jfro where jfro.org_id in ("
							+ "     select jfo.org_id from jecn_flow_org jfo where jfo.del_state=0 start with jfo.org_id in "
							+ JecnCommonSql.getIdsSet(dutyOrgIds)
							+ "     connect by prior jfo.org_id= jfo.per_org_id))";
				}
				if (processMapIds.size() > 0) {
					sql = sql
							+ " my_flow_flow as("
							+ "     select distinct jfs.flow_id from jecn_flow_structure jfs  where jfs.isflow=1 and jfs.del_state=0"
							+ " start with jfs.flow_id in " + JecnCommonSql.getIdsSet(processMapIds)
							+ "     connect by prior jfs.flow_id = jfs.pre_flow_id)";
					if (dutyOrgIds.size() > 0) {
						sql = sql + "," + sqlOrg;
					}
				} else {
					if (dutyOrgIds.size() > 0) {
						sql = sql + " " + sqlOrg;
					}
				}

			} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				if (recursion) {
					sqlOrg = " my_flow_org AS" + "  (SELECT JFO.ORG_ID,JFRO.FLOW_ID" + "   FROM jecn_flow_org jfo"
							+ "   INNER JOIN JECN_FLOW_RELATED_ORG JFRO ON JFO.ORG_ID=JFRO.ORG_ID"
							+ "   WHERE JFO.ORG_ID IN " + JecnCommonSql.getIdsSet(dutyOrgIds)
							+ "   UNION ALL SELECT my_flow_org.flow_id,jf.org_id" + "   FROM my_flow_org"
							+ "   INNER JOIN jecn_flow_org JF ON my_flow_org.ORG_ID=JF.PER_ORG_ID" + ")";
				}
				if (processMapIds.size() > 0) {
					sql = sql
							+ " my_flow_flow as("
							+ "     select distinct jfs.flow_id from jecn_flow_structure jfs where jfs.flow_id in "
							+ JecnCommonSql.getIdsSet(processMapIds)
							+ " and jfs.del_state=0"
							+ " UNION ALL"
							+ " SELECT JFS.FLOW_ID FROM my_flow_flow INNER JOIN jecn_flow_structure JFS ON my_flow_flow.flow_id=jfs.pre_flow_id)";
					if (dutyOrgIds.size() > 0) {
						sql = sql + "," + sqlOrg;
					}
				} else {
					if (dutyOrgIds.size() > 0) {
						sql = sql + " " + sqlOrg;
					}
				}

			}

			sql = sql + "     SELECT " + "     JTHN.ID," + "     JT.FLOW_ID," + "     JT.FLOW_NAME,"
					+ "     JT.FLOW_ID_INPUT," + "     JFO.ORG_NAME,"
					+ "     CASE WHEN T.TYPE_RES_PEOPLE=1 THEN JFIO.FIGURE_TEXT"
					+ "     ELSE JU.TRUE_NAME END AS RES_PEOPLE," + "     WARD_PEOPLE.True_Name,"
					+ "     FICTION_PEOPLE.TRUE_NAME AS PEOPLE_MAKE,";

			if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = sql + "     JTHN.PUBLISH_DATE," + "     T.EXPIRY," + "     JTHN.NEXT_SCAN_DATE,JTHN.VERSION_ID";
			} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = sql + "     CONVERT(VARCHAR(12),JTHN.PUBLISH_DATE,23)," + "     T.EXPIRY,"
						+ "     CONVERT(VARCHAR(12),JTHN.NEXT_SCAN_DATE,23) as NEXT_SCAN_DATE,JTHN.VERSION_ID";
			}

			sql = sql
					+ "	FROM JECN_TASK_HISTORY_NEW JTHN "
					+ "	  INNER JOIN (SELECT max(j.id) as id, j.relate_id "
					+ "	               FROM JECN_TASK_HISTORY_NEW j where j.type=0 "
					+ "	              group by j.relate_id) J  ON J.ID = JTHN.ID "
					+ "	  INNER JOIN JECN_FLOW_STRUCTURE JT ON J.RELATE_ID = JT.FLOW_ID and jt.del_state=0 "
					+ "    LEFT JOIN JECN_FLOW_BASIC_INFO T ON T.FLOW_ID=JT.FLOW_ID"
					+ "    LEFT JOIN JECN_USER JU ON T.TYPE_RES_PEOPLE<>1 AND T.RES_PEOPLE_ID=JU.PEOPLE_ID AND JU.ISLOCK=0"
					+ "    LEFT JOIN JECN_FLOW_ORG_IMAGE JFIO ON T.TYPE_RES_PEOPLE=1 AND JFIO.FIGURE_ID=T.RES_PEOPLE_ID"
					+ "    LEFT JOIN JECN_FLOW_ORG ORG ON JFIO.ORG_ID=ORG.ORG_ID AND ORG.DEL_STATE=0"
					+ "    LEFT JOIN JECN_FLOW_RELATED_ORG JFOG ON JFOG.FLOW_ID=T.FLOW_ID"
					+ "    LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID =JFOG.ORG_ID AND JFO.DEL_STATE=0"
					+ "    LEFT JOIN JECN_USER WARD_PEOPLE ON WARD_PEOPLE.PEOPLE_ID = T.WARD_PEOPLE_ID AND WARD_PEOPLE.ISLOCK=0"
					+ "    LEFT JOIN JECN_USER FICTION_PEOPLE ON FICTION_PEOPLE.PEOPLE_ID = T.FICTION_PEOPLE_ID AND FICTION_PEOPLE.ISLOCK=0"
					+ "    where JT.PROJECTID=" + projectId + "    AND jt.isflow=1 AND jt.del_state=0 and jthn.type=0";
			if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = sql + " AND JTHN.Publish_Date>= to_date('2014-01-01 00:00:00','yyyy-mm-dd hh24:mi:ss')";
			} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = sql + " AND year(JTHN.Publish_Date)>=2014";
			}
			if (dutyOrgIds.size() > 0) {
				sql = sql + "    AND JT.FLOW_ID IN (SELECT FLOW_ID FROM my_flow_org)";
			}
			if (processMapIds.size() > 0) {
				sql = sql + "    AND JT.FLOW_ID IN (SELECT FLOW_ID FROM my_flow_flow where isflow=1)";
			}
			sql = sql + "    ORDER BY JFO.ORG_ID,JT.PRE_FLOW_ID,JT.FLOW_ID,JTHN.Publish_Date";
		} else {
			if (dutyOrgIds.size() > 0 || processMapIds.size() > 0) {
				sql = "with";
			}
			String sqlOrg = " my_flow_org as (select distinct jfro.id from jecn_rule jfro where jfro.del_State = 0 and jfro.org_id in "
					+ JecnCommonSql.getIdsSet(dutyOrgIds) + " )";
			if (JecnContants.dbType.equals(DBType.ORACLE)) {
				if (recursion) {
					sqlOrg = " my_flow_org as ("
							+ "     select distinct jfro.id from jecn_rule jfro where jfro.org_id in ("
							+ "     select jfo.org_id from jecn_flow_org jfo start with jfo.org_id in "
							+ JecnCommonSql.getIdsSet(dutyOrgIds)
							+ "     connect by prior jfo.org_id= jfo.per_org_id) AND jfro.Del_State = 0)";
				}
				if (processMapIds.size() > 0) {
					sql = sql
							+ " my_flow_flow as("
							+ "     select distinct jfs.id from jecn_rule jfs  where jfs.is_dir in (2,3)  AND jfro.Del_State = 0"
							+ " start with jfs.id in " + JecnCommonSql.getIdsSet(processMapIds)
							+ "     connect by prior jfs.id = jfs.per_id)";
					if (dutyOrgIds.size() > 0) {
						sql = sql + "," + sqlOrg;
					}
				} else {
					if (dutyOrgIds.size() > 0) {
						sql = sql + " " + sqlOrg;
					}
				}

			} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				if (recursion) {
					sqlOrg = " my_flow_org AS" + "  (SELECT JFO.ORG_ID,JFRO.id" + "   FROM jecn_flow_org jfo"
							+ "   INNER JOIN jecn_rule JFRO ON JFO.ORG_ID=JFRO.ORG_ID AND JFRO.DEL_STATE = 0 "
							+ "   WHERE JFO.ORG_ID IN " + JecnCommonSql.getIdsSet(dutyOrgIds)
							+ "   UNION ALL SELECT my_flow_org.id,jf.org_id" + "   FROM my_flow_org"
							+ "   INNER JOIN jecn_flow_org JF ON my_flow_org.ORG_ID=JF.PER_ORG_ID" + ")";
				}

				if (processMapIds.size() > 0) {
					sql = sql
							+ " my_flow_flow as("
							+ "     select distinct jfs.id from jecn_rule jfs where jfs.id in "
							+ JecnCommonSql.getIdsSet(processMapIds)
							+ " AND jfs.DEL_STATE = 0"
							+ " UNION ALL"
							+ " SELECT JFS.id FROM my_flow_flow INNER JOIN jecn_rule JFS ON my_flow_flow.id=jfs.per_id AND JFS.DEL_STATE = 0)";
					if (dutyOrgIds.size() > 0) {
						sql = sql + "," + sqlOrg;
					}
				} else {
					if (dutyOrgIds.size() > 0) {
						sql = sql + " " + sqlOrg;
					}
				}

			}

			sql = sql + "     SELECT " + "     JTHN.ID," + "     JT.id as rule_id," + "     JT.RULE_NAME,"
					+ "     JT.RULE_NUMBER," + "     JFO.ORG_NAME,"
					+ "     CASE WHEN JT.TYPE_RES_PEOPLE=1 THEN JFIO.FIGURE_TEXT"
					+ "     ELSE JU.TRUE_NAME END AS RES_PEOPLE," + "     WARD_PEOPLE.True_Name,"
					+ "     FICTION_PEOPLE.TRUE_NAME AS PEOPLE_MAKE,";

			if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = sql + "     JTHN.PUBLISH_DATE," + "     JT.EXPIRY," + "     JTHN.NEXT_SCAN_DATE,JTHN.VERSION_ID";
			} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = sql + "     CONVERT(VARCHAR(12),JTHN.PUBLISH_DATE,23)," + "     JT.EXPIRY,"
						+ "     CONVERT(VARCHAR(12),JTHN.NEXT_SCAN_DATE,23) as NEXT_SCAN_DATE,JTHN.VERSION_ID";
			}

			sql = sql
					+ "	FROM JECN_TASK_HISTORY_NEW JTHN "
					+ "	  INNER JOIN (SELECT max(j.id) as id, j.relate_id "
					+ "	               FROM JECN_TASK_HISTORY_NEW j where j.type in (2,3) "
					+ "	              group by j.relate_id) J  ON J.ID = JTHN.ID "
					+ "	  INNER JOIN jecn_rule JT ON J.RELATE_ID = JT.id "
					+ "    LEFT JOIN JECN_USER JU ON JT.TYPE_RES_PEOPLE<>1 AND JT.RES_PEOPLE_ID=JU.PEOPLE_ID AND JU.ISLOCK=0"
					+ "    LEFT JOIN JECN_FLOW_ORG_IMAGE JFIO ON JT.TYPE_RES_PEOPLE=1 AND JFIO.FIGURE_ID=JT.RES_PEOPLE_ID"
					+ "    LEFT JOIN JECN_FLOW_ORG ORG ON JFIO.ORG_ID=ORG.ORG_ID AND ORG.DEL_STATE=0"
					+ "    LEFT JOIN jecn_rule JFOG ON JFOG.id=JT.id AND JFOG.DEL_STATE = 0 "
					+ "    LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID =JFOG.ORG_ID AND JFO.DEL_STATE=0"
					+ "    LEFT JOIN JECN_USER WARD_PEOPLE ON WARD_PEOPLE.PEOPLE_ID = JT.WARD_PEOPLE_ID AND WARD_PEOPLE.ISLOCK=0"
					+ "    LEFT JOIN JECN_USER FICTION_PEOPLE ON FICTION_PEOPLE.PEOPLE_ID = JT.FICTION_PEOPLE_ID AND FICTION_PEOPLE.ISLOCK=0"
					+ "    where JT.PROJECT_ID=" + projectId
					+ " AND JT.DEL_STATE=0 AND JT.is_dir <> 0  and jthn.type in (2,3)";
			if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = sql + " AND JTHN.Publish_Date>= to_date('2014-01-01 00:00:00','yyyy-mm-dd hh24:mi:ss')";
			} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = sql + " AND year(JTHN.Publish_Date)>=2014";
			}
			if (dutyOrgIds.size() > 0) {
				sql = sql + "    AND JT.id IN (SELECT id FROM my_flow_org)";
			}
			if (processMapIds.size() > 0) {
				sql = sql + "    AND JT.id IN (SELECT id FROM my_flow_flow where is_dir in (2,3))";
			}
			sql = sql + "    ORDER BY JFO.ORG_ID,JT.per_id,JT.id,JTHN.Publish_Date";

		}
		return this.listNativeSql(sql);
	}

	@Override
	public List<JecnFlowSustainTool> getSustainTool(long relatedId, int type) throws Exception {

		String sql = "SELECT JFST.* FROM JECN_FLOW_SUSTAIN_TOOL JFST" + " INNER JOIN JECN_SUSTAIN_TOOL_CONN JSTC"
				+ " ON JFST.FLOW_SUSTAIN_TOOL_ID=JSTC.SUSTAIN_TOOL_ID" + " WHERE JSTC.RELATED_ID =?"
				+ " AND JSTC.RELATED_TYPE=?";
		return (List<JecnFlowSustainTool>) this.listNativeAddEntity(sql, JecnFlowSustainTool.class, relatedId, type);
	}

	@Override
	// 根据父节点ID获得流程或流程地图的子节点
	public List<Object[]> getChildFlows(Long flowMapId, Long projectId, boolean isAdmin) throws Exception {
		return this.listNativeSql(MyFlowSqlUtil.getFlowTreeAppend(isAdmin, flowMapId, projectId));
	}

	@Override
	// 根据父节点ID获得流程或流程地图的子节点
	public List<Object[]> getChildFlowsT(Long flowMapId, Long projectId, boolean isContainsDelNode) throws Exception {
		// 不包含删除的节点添加条件
		String delConditionOne = isContainsDelNode == false ? " and t.del_state=0 " : "";
		String delConditionTwo = isContainsDelNode == false ? " and sub.del_state=0 " : "";
		String sql = "select distinct t.flow_id,t.flow_name,t.pre_flow_id,t.isflow,t.flow_id_input,"
				+ "       case when js.flow_id is null then '0' else '1' end as is_pub,"
				+ "       case when sub.flow_id is null then 0 else 1 end as count,t.t_path,t.sort_id,T.NODE_TYPE,"
				+ "       case when t.pub_time<>t.update_date then 1 else 0 end as isUpdate"
				+ "       from jecn_flow_structure_t t"
				+ "       left join jecn_flow_structure js on t.flow_id = js.flow_id "
				+ "       left join jecn_flow_structure_t sub on sub.pre_flow_id = t.flow_id " + delConditionTwo
				+ "       where t.pre_flow_id=? AND t.DATA_TYPE=0 and t.projectid=? " + delConditionOne
				+ " order by t.pre_flow_id,t.sort_id,t.flow_id";
		return this.listNativeSql(sql, flowMapId, projectId);
	}

	@Override
	public List<JecnToFromActiveT> findToFromRelatedActiveT(long id) throws Exception {
		String hql = "from JecnToFromActiveT where flowId=?";
		List<JecnToFromActiveT> relateds = this.listHql(hql, id);
		return relateds;
	}

	/**
	 * 插入当前发布版本
	 * 
	 * @param flowId
	 * @param flowName
	 * @param version
	 *            发布的版本号
	 * @throws Exception
	 */
	@Override
	public void insertEPROSBPM(Long flowId, String flowName, String version) throws Exception {
		String updateTime = JecnCommon.getStringbyDateHMS(new Date());
		String sql = "INSERT INTO EPROS_BMP_VERSION(FLOW_ID,FLOW_NAME,VERSION,UPDATE_TIME) " + "VALUES(?,?,?,?)";
		this.execteNative(sql, flowId, flowName, version, updateTime);
	}

	/**
	 * 验证当前发布版本是否存在
	 * 
	 * @param flowId
	 * @param version
	 *            发布的版本号
	 * @return
	 * @throws Exception
	 */
	@Override
	public int validateEPROSBPM(Long flowId, String version) throws Exception {
		String sql = "SELECT COUNT(1) FROM  EPROS_BMP_VERSION WHERE FLOW_ID = ? AND VERSION = ?";
		return this.countAllByParamsNativeSql(sql, flowId, version);
	}

	@Override
	public List<TermDefinitionLibrary> getDefinitionsByFlowId(Long processId, boolean isPub) throws Exception {
		List<Object[]> objs = getTermDefineByFlowId(processId, isPub ? "" : "_T");
		List<TermDefinitionLibrary> definitions = new ArrayList<TermDefinitionLibrary>();
		TermDefinitionLibrary definition = null;
		for (Object[] obj : objs) {
			definition = new TermDefinitionLibrary();
			definitions.add(definition);
			definition.setId(Long.valueOf(obj[0].toString()));
			definition.setName(obj[2].toString());
			definition.setInstructions(obj[1] == null ? "" : obj[1].toString());
		}
		return definitions;
	}

	@Override
	public List<ProcessInterface> findFlowInterfaces(Long id, boolean isPub) throws Exception {
		String pub = isPub ? "" : "_t";
		String sql = " SELECT" + "	jfsi.flow_id," + "	jfsi.figure_id," + "	jfsi.figure_text," + "   jfsi.impl_type,"
				+ "   jfsi.impl_name," + "   jfsi.impl_note," + "   jfsi.process_requirements," + "	jfs.flow_id,"
				+ "	jfs.flow_name" + " FROM" + "	jecn_flow_structure_image" + pub + " jfsi"
				+ " INNER JOIN jecn_flow_structure_t" + " jfs ON jfsi.link_flow_id = jfs.flow_id and jfs.del_state=0"
				+ " WHERE" + "	jfsi.flow_id = ? and jfsi.impl_type in (1,2,3,4) order by jfsi.x_point,jfsi.y_point asc";
		List<ProcessInterface> interfaces = new ArrayList<ProcessInterface>();
		List<Object[]> objs = this.listNativeSql(sql, id);
		for (Object[] obj : objs) {
			ProcessInterface pInterface = new ProcessInterface();
			interfaces.add(pInterface);
			pInterface.setId(Long.valueOf(obj[1].toString()));
			pInterface.setName(obj[2].toString());
			pInterface.setImplType(Integer.valueOf(obj[3].toString()));
			pInterface.setImplName(objToString(obj[4]));
			pInterface.setImplNote(objToString(obj[5]));
			pInterface.setProcessRequirements(objToString(obj[6]));
			pInterface.setRid(Long.valueOf(obj[7].toString()));
			pInterface.setRname(obj[8].toString());
		}
		return interfaces;
	}

	private String objToString(Object obj) {
		return obj == null ? "" : obj.toString();
	}

	@Override
	public List<Object[]> getParents(Long flowId, boolean isPub) throws Exception {
		String concat = JecnContants.dbType.equals(DBType.ORACLE) ? "||" : "+";
		String pub = isPub ? "" : "_t";
		String sql = "select p.flow_id,p.FLOW_NAME from JECN_FLOW_STRUCTURE" + pub + " c"
				+ "	inner join JECN_FLOW_STRUCTURE" + pub + " p on c.T_PATH like p.T_PATH" + concat
				+ "'%' and c.FLOW_ID=? order by p.t_level asc";
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public List<Object[]> getTermDefineByFlowId(Long flowId, String isPub) throws Exception {
		String sql = "SELECT T.ID,T.INSTRUCTIONS,FST.FIGURE_TEXT" + "  FROM TERM_DEFINITION_LIBRARY T"
				+ "  LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE" + isPub + " FST" + "    ON T.ID = FST.LINK_FLOW_ID"
				+ "   AND FST.FIGURE_TYPE = 'TermFigureAR'" + "   AND FST.FLOW_ID = ?"
				+ " WHERE FST.FIGURE_ID IS NOT NULL";
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public Long getRelatedFileId(Long flowId) throws Exception {
		String sql = "SELECT FT.FILE_ID" + "  FROM JECN_FLOW_STRUCTURE_T T" + "  LEFT JOIN JECN_FILE_T FT"
				+ "    ON FT.FLOW_ID = T.FLOW_ID" + " WHERE FT.FLOW_ID IS NOT NULL" + "   AND FT.IS_DIR = 0"
				+ "   AND T.FLOW_ID = ?";
		Object obj = getObjectNativeSql(sql, flowId);
		return obj == null ? null : Long.valueOf(obj.toString());
	}

	/**
	 * 获取父节点对应的relatedFileId
	 * 
	 * @param path
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getParentRelatedFileByPath(Long id) throws Exception {
		String sql = "SELECT PARENT.FLOW_ID," + "       FT.FILE_ID,PARENT.FLOW_NAME,PARENT.SORT_ID"
				+ "  FROM JECN_FLOW_STRUCTURE_T T" + "  LEFT JOIN JECN_FLOW_STRUCTURE_T PARENT ON "
				+ JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT") + " LEFT JOIN JECN_FILE_T FT "
				+ " ON FT.FLOW_ID = PARENT.FLOW_ID" + "  AND FT.IS_DIR = 0 WHERE T.FLOW_ID = ?"
				+ " ORDER BY PARENT.T_PATH";
		return this.listNativeSql(sql, id);
	}

	/**
	 * 根据流程ID获取可维护的文件节点
	 * 
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getRelatedFileIdsByFlowIds(List<Long> ids) throws Exception {
		String sql = "  SELECT T.RELATED_FILE_ID FROM JECN_FLOW_STRUCTURE_T T WHERE T.FLOW_ID IN "
				+ JecnCommonSql.getIds(ids) + " AND T.RELATED_FILE_ID IS NOT NULL";
		return this.listObjectNativeSql(sql, "RELATED_FILE_ID", Hibernate.LONG);
	}

	/**
	 * 更新流程关联文件节点
	 * 
	 * @param flowId
	 * @param relatedFileId
	 * @throws Exception
	 */
	@Override
	public void updateParentRelatedFileId(Long flowId, Long relatedFileId) throws Exception {
		String sql = "UPDATE JECN_FLOW_STRUCTURE_T SET RELATED_FILE_ID = ? WHERE FLOW_ID =  ?";
		this.execteNative(sql, relatedFileId, flowId);
		sql = "UPDATE JECN_FLOW_STRUCTURE SET RELATED_FILE_ID = ? WHERE FLOW_ID =  ?";
		this.execteNative(sql, relatedFileId, flowId);
	}

	@Override
	public List<Object[]> getStandardizedFiles(Long flowId) throws Exception {
		String sql = "SELECT FRSF.ID, FRSF.FLOW_ID, FRSF.FILE_ID, FT.FILE_NAME ,FT.DOC_ID"
				+ "  FROM FLOW_STANDARDIZED_FILE_T FRSF" + " INNER JOIN JECN_FILE_T FT"
				+ "    ON FT.FILE_ID = FRSF.FILE_ID" + " WHERE FRSF.FLOW_ID = ?";
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public List<Object[]> getRelatedRiskList(Long flowId) throws Exception {
		String sql = "SELECT RRT.RISK_ID, JR.NAME" + "  FROM JECN_FLOW_RELATED_RISK_T RRT"
				+ " INNER JOIN JECN_FLOW_STRUCTURE_T FST" + "    ON FST.FLOW_ID = RRT.FLOW_ID"
				+ " INNER JOIN JECN_RISK JR" + "    ON JR.ID = RRT.RISK_ID" + " WHERE RRT.FLOW_ID = ?";
		return this.listNativeSql(sql, flowId);
	}

	@Override
	public List<Object[]> getFirstRoleUsers(Set<Long> flowIdSet) throws Exception {
		// 获得流程中第一个角色的岗位的人员信息
		String flowIds = JecnCommonSql.getIdsSet(flowIdSet);
		String sql = "WITH images AS (" + "	SELECT" + "		f.FLOW_ID," + "		MIN (f.Y_POINT) AS p" + "	FROM"
				+ "		JECN_FLOW_STRUCTURE_IMAGE f"
				+ "	INNER JOIN PROCESS_STATION_RELATED s ON f.FIGURE_ID = s.FIGURE_FLOW_ID" + " where f.flow_id in "
				+ flowIds
				+ " and f.figure_type in "
				+ JecnCommonSql.getOnlyRoleString()
				+ "	GROUP BY"
				+ "		f.FLOW_ID"
				+ " ),"
				+ " images2 AS ("
				+ "	SELECT"
				+ "		f2.*"
				+ "	FROM"
				+ "		images f1"
				+ "	INNER JOIN JECN_FLOW_STRUCTURE_IMAGE f2 ON f1.flow_id = f2.flow_id"
				+ "	AND f1.p = f2.Y_POINT"
				+ " ) "
				+ " select f1.flow_id,ju.TRUE_NAME,ju.EMAIL,ju.EMAIL_TYPE,ju.PEOPLE_ID from images2 f1"
				+ " inner join PROCESS_STATION_RELATED f2 on f1.figure_id=f2.FIGURE_FLOW_ID and f2.type=0"
				+ " inner join JECN_FLOW_ORG_IMAGE jfoi on jfoi.FIGURE_ID=f2.FIGURE_POSITION_ID"
				+ " inner join JECN_USER_POSITION_RELATED jupr on jupr.FIGURE_ID=jfoi.FIGURE_ID"
				+ " inner join JECN_USER ju on jupr.PEOPLE_ID=ju.PEOPLE_ID"
				+ " union "
				+ " select f1.flow_id,ju.TRUE_NAME,ju.EMAIL,ju.EMAIL_TYPE,ju.PEOPLE_ID from images2 f1"
				+ " inner join PROCESS_STATION_RELATED f2 on f1.figure_id=f2.FIGURE_FLOW_ID and f2.type=1"
				+ " inner join JECN_POSITION_GROUP_R jpgr on f2.FIGURE_POSITION_ID=jpgr.GROUP_ID"
				+ " inner join JECN_FLOW_ORG_IMAGE jfoi on jfoi.FIGURE_ID=jpgr.FIGURE_ID"
				+ " inner join JECN_USER_POSITION_RELATED jupr on jupr.FIGURE_ID=jfoi.FIGURE_ID"
				+ " inner join JECN_USER ju on jupr.PEOPLE_ID=ju.PEOPLE_ID";
		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> listFavoriteUpdateInfo() throws Exception {
		String sql = "	with logs AS"
				+ "	 (SELECT jol.*"
				+ "			FROM JECN_OPR_LOG jol"
				+ "		 INNER JOIN (SELECT MAX(operator_time) AS last_time,"
				+ "											 RELATED_ID,"
				+ "											 RELATED_type"
				+ "									FROM JECN_OPR_LOG"
				+ "								 GROUP BY RELATED_ID, RELATED_type) l"
				+ "				ON jol.operator_time = l.last_time"
				+ "			 AND jol.RELATED_ID = l.RELATED_ID"
				+ "			 AND jol.RELATED_type = l.RELATED_type),"
				+ "	sources as"
				+ "	  (select logs.RELATED_ID,"
				+ "					 jfs.flow_name   as related_name,"
				+ "					 0               as star_type,"
				+ "                    0               as task_type,"
				+ "                    jfs.pub_time    as PUB_TIME,"
				+ "					 jfs.history_id  as n,"
				+ "					 logs.history_id as o"
				+ "			from JECN_FLOW_STRUCTURE jfs"
				+ "		 inner join logs"
				+ "				on jfs.flow_id = logs.RELATED_ID"
				+ "			 and logs.RELATED_type = 0"
				+ "			 and jfs.del_state = 0"
				+ "		union"
				+ "		select logs.RELATED_ID,"
				+ "					 jfs.flow_name   as related_name,"
				+ "					 1               as star_type,"
				+ "                    4               as task_type,"
				+ "                    jfs.pub_time    as PUB_TIME,"
				+ "					 jfs.history_id  as n,"
				+ "					 logs.history_id as o"
				+ "			from JECN_FLOW_STRUCTURE jfs"
				+ "		 inner join logs"
				+ "				on jfs.flow_id = logs.RELATED_ID"
				+ "			 and logs.RELATED_type = 7"
				+ "			 and jfs.del_state = 0"
				+ "		union"
				+ "		select logs.RELATED_ID,"
				+ "					 jfs.file_name   as related_name,"
				+ "					 4               as star_type,"
				+ "                    1               as task_type,"
				+ "                    jfs.pub_time    as PUB_TIME,"
				+ "					 jfs.history_id  as n,"
				+ "					 logs.history_id as o"
				+ "			from JECN_FILE jfs"
				+ "		 inner join logs"
				+ "				on jfs.file_id = logs.RELATED_ID"
				+ "			 and logs.RELATED_type = 2"
				+ "			 and jfs.del_state = 0"
				+ "		UNION"
				+ "		select logs.RELATED_ID,"
				+ "					 jfs.rule_name   as related_name,"
				+ "					 2               as star_type,"
				+ "                    2               as task_type,"
				+ "                    jfs.pub_time    as PUB_TIME,"
				+ "					 jfs.history_id  as n,"
				+ "					 logs.history_id as o"
				+ "			from JECN_RULE jfs"
				+ "		 inner join logs"
				+ "				on jfs.id = logs.RELATED_ID"
				+ "			 and logs.RELATED_type = 1"
				+ "       and jfs.IS_DIR=1"
				+ "	UNION"
				+ "		select logs.RELATED_ID,"
				+ "					 jfs.rule_name   as related_name,"
				+ "					 2               as star_type,"
				+ "                    3               as task_type,"
				+ "                    jfs.pub_time    as PUB_TIME,"
				+ "					 jfs.history_id  as n,"
				+ "					 logs.history_id as o"
				+ "			from JECN_RULE jfs"
				+ "		 inner join logs"
				+ "				on jfs.id = logs.RELATED_ID"
				+ "			 and logs.RELATED_type = 1"
				+ "       and jfs.IS_DIR=2"
				+ "),"
				+ "	updates as"
				+ "	 ("
				+ "		SELECT r.related_id, r.related_name, r.star_type,r.task_type,r.pub_time"
				+ "			from (select r.related_id,"
				+ "										r.related_name,"
				+ "										r.star_type,"
				+ "                    r.task_type,"
				+ "                    r.pub_time,"
				+ "										case"
				+ "											when n is null and o is null then"
				+ "											 0"
				+ "											when n = o then"
				+ "											 0"
				+ "											else"
				+ "											 1"
				+ "										end as is_update"
				+ "							 from sources r) r"
				+ "		 where r.is_update = 1)"
				+ "	select u.related_id,u.related_name,u.star_type,u.task_type,u.pub_time,ju.PEOPLE_ID,ju.TRUE_NAME,ju.EMAIL,ju.EMAIL_TYPE"
				+ "		from updates u   "
				+ "		 INNER JOIN JECN_MY_STAR jms on u.RELATED_ID=jms.RELATED_ID and u.star_type=jms.type"
				+ "		 inner join JECN_USER ju on jms.user_id=ju.people_id and ju.islock=0";
		return this.listNativeSql(sql);
	}

	/**
	 * 获取流程（文件） 名称
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Override
	public Object[] getFlowFileContentName(Long id, String isPub) throws Exception {
		String sql = "SELECT T.FILE_NAME,T.ID" + "  FROM FLOW_FILE_CONTENT T" + " INNER JOIN JECN_FLOW_STRUCTURE"
				+ isPub + " FS" + "    ON T.RELATED_ID = FS.FLOW_ID" + "   AND FS.FILE_CONTENT_ID = T.ID"
				+ " WHERE FS.FLOW_ID = ?";
		List<Object[]> list = this.listNativeSql(sql, id);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public Object[] getFlowFileContent(Long id, String isPub) throws Exception {
		String sql = "SELECT T.FILE_NAME,T.ID,T.FILE_STREAM" + "  FROM FLOW_FILE_CONTENT T"
				+ " INNER JOIN JECN_FLOW_STRUCTURE" + isPub + " FS" + "    ON T.RELATED_ID = FS.FLOW_ID"
				+ "   AND FS.FILE_CONTENT_ID = T.ID" + " WHERE FS.FLOW_ID = ?";
		List<Object[]> list = this.listNativeSql(sql, id);
		if (list == null || list.size() == 0) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<Object[]> getPathNameByIds(Set<Long> ids) {
		String sql = "SELECT T.CRITERION_CLASS_ID, JCC.CRITERION_CLASS_NAME" + "  FROM jecn_criterion_classes T"
				+ "  LEFT JOIN jecn_criterion_classes JCC ON " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("JCC")
				+ "  AND T.CRITERION_CLASS_ID <> JCC.CRITERION_CLASS_ID" + " WHERE T.CRITERION_CLASS_ID IN "
				+ JecnCommonSql.getIdsSet(ids) + " ORDER BY T.CRITERION_CLASS_ID, JCC.T_PATH";
		return this.listNativeSql(sql);
	}

	/**
	 * 活动相关附件 (输入输出、操作规范、样例)ID
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getActivityFileByFlowId(Long flowId, WebLoginBean loginBean, String isPub, int accessType)
			throws Exception {
		String sql = "SELECT T.FILE_ID FROM (SELECT DISTINCT JAF.FILE_S_ID FILE_ID" + "  FROM JECN_ACTIVITY_FILE"
				+ isPub + " JAF, JECN_FLOW_STRUCTURE_IMAGE" + isPub + " JFSI" + " WHERE JAF.FIGURE_ID = JFSI.FIGURE_ID"
				+ "   AND JFSI.FLOW_ID = " + flowId + " UNION " + "SELECT DISTINCT JMF.FILE_M_ID FILE_ID"
				+ "  FROM JECN_MODE_FILE" + isPub + " JMF, JECN_FLOW_STRUCTURE_IMAGE" + isPub + " JFSI"
				+ " WHERE JMF.FIGURE_ID = JFSI.FIGURE_ID" + "   AND JFSI.FLOW_ID =  " + flowId + " UNION "
				+ " SELECT DISTINCT JFIO.FILE_ID FROM JECN_FIGURE_IN_OUT" + isPub
				+ " JFIO WHERE JFIO.FILE_ID IS NOT NULL AND JFIO.FLOW_ID= " + flowId + " ) T";
		sql += JecnCommonSqlTPath.INSTANCE.getAuthorFileIdsSql("FILE_ID", loginBean, isPub, accessType);
		return this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
	}

	/**
	 * 根据权限获取流程相关制度（文件）
	 * 
	 * @param flowId
	 * @param loginBean
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getRuleFileIdsByFlowId(Long flowId, WebLoginBean loginBean, String isPub, int accessType)
			throws Exception {
		String sql = "SELECT T.FILE_ID FROM (SELECT RL.FILE_ID" + "  FROM FLOW_RELATED_CRITERION" + isPub
				+ " RF, JECN_RULE" + isPub + " RL" + " WHERE RF.CRITERION_CLASS_ID = RL.ID" + "   AND RL.IS_DIR = 2"
				+ "   AND RL.IS_FILE_LOCAL = '0'" + "   AND RF.FLOW_ID = " + flowId + "   AND RL.PROJECT_ID = "
				+ loginBean.getProjectId() + ") T";
		sql += JecnCommonSqlTPath.INSTANCE.getAuthorFileIdsSql("FILE_ID", loginBean, isPub, accessType);
		return this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
	}

	/**
	 * 流程标准附件
	 */
	@Override
	public List<Long> getStandFileByFlowId(Long flowId, WebLoginBean loginBean, String isPub, int accessType)
			throws Exception {
		String sql = "SELECT T.FILE_ID FROM (SELECT STAND.RELATED_ID FILE_ID,STAND.CRITERION_CLASS_ID"
				+ "  FROM JECN_FLOW_STANDARD"
				+ isPub
				+ " RS, JECN_CRITERION_CLASSES STAND"
				+ " WHERE STAND.CRITERION_CLASS_ID = RS.CRITERION_CLASS_ID"
				+ "   AND STAND.STAN_TYPE = '1'"
				+ "   AND RS.FLOW_ID = "
				+ flowId
				+ "   AND STAND.PROJECT_ID ="
				+ loginBean.getProjectId()
				+ " UNION "
				+ "SELECT C.RELATED_ID FILE_ID, C.CRITERION_CLASS_ID"
				+ "  FROM JECN_ACTIVE_STANDARD      JAS,"
				+ "       JECN_FLOW_STRUCTURE_IMAGE"
				+ isPub
				+ " SI,"
				+ "       JECN_CRITERION_CLASSES    C"
				+ " WHERE JAS.ACTIVE_ID = SI.FIGURE_ID"
				+ "   AND SI.FIGURE_TYPE IN"
				+ JecnCommonSql.getActiveString()
				+ "   AND JAS.STANDARD_ID = C.CRITERION_CLASS_ID  AND C.STAN_TYPE = '1' "
				+ "   AND SI.FLOW_ID =  "
				+ flowId + ") T";
		sql += JecnCommonSqlTPath.INSTANCE.getAuthorIdsSqlByType("CRITERION_CLASS_ID", loginBean, isPub,
				TYPEAUTH.STANDARD, accessType);
		return this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
	}

	/**
	 * 制度文件本地上传
	 */
	@Override
	public List<Object[]> getLocalRuleFileByFlowId(Long flowId, WebLoginBean loginBean, String isPub, int accessType)
			throws Exception {
		String sql = "SELECT T.ID,T.FILE_NAME,T.FILE_STREAM FROM (SELECT G020.ID, G020.FILE_NAME, G020.FILE_STREAM,RL.ID AS RELATED_ID"
				+ "  FROM FLOW_RELATED_CRITERION"
				+ isPub
				+ " RF,"
				+ "       JECN_RULE"
				+ isPub
				+ "              RL,"
				+ "       G020_RULE_FILE_CONTENT G020"
				+ " WHERE RF.CRITERION_CLASS_ID = RL.ID"
				+ "   AND RL.ID = G020.RULE_ID"
				+ "   AND RL.IS_DIR = 2"
				+ "   AND RL.IS_FILE_LOCAL = 1"
				+ "   AND RL.FILE_ID = G020.ID"
				+ "   AND RF.FLOW_ID =  "
				+ flowId
				+ "   AND RL.PROJECT_ID = "
				+ loginBean.getProjectId() + " ) T";
		sql += JecnCommonSqlTPath.INSTANCE.getAuthorRuleIdsSql("ID", loginBean, isPub, accessType);
		return this.listNativeSql(sql);
	}

	@Override
	public List<Long> getStandardizedFileIds(Long flowId, WebLoginBean loginBean, String isPub, int accessType)
			throws Exception {
		String sql = "SELECT T.FILE_ID FROM (SELECT FRSF.FILE_ID" + "  FROM FLOW_STANDARDIZED_FILE" + isPub + " FRSF"
				+ " INNER JOIN JECN_FILE_T FT" + "    ON FT.FILE_ID = FRSF.FILE_ID" + " WHERE FRSF.FLOW_ID = " + flowId
				+ ") T";
		sql += JecnCommonSqlTPath.INSTANCE.getAuthorFileIdsSql("FILE_ID", loginBean, isPub, accessType);
		return this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
	}

	@Override
	public List<String> getApplyOrgs(Long flowId, boolean isPub) {
		String tableName = "JECN_FLOW_APPLY_ORG_T";
		if (isPub) {
			tableName = "JECN_FLOW_APPLY_ORG";
		}
		String sql = "select JFO.ORG_NAME from " + tableName + " JFAOT"
				+ " inner join JECN_FLOW_ORG JFO ON JFAOT.ORG_ID=JFO.ORG_ID AND JFAOT.RELATED_ID=" + flowId
				+ " order by JFO.ORG_ID ASC";
		return this.listObjectNativeSql(sql, "ORG_NAME", Hibernate.STRING);
	}

	@Override
	public List<Object[]> getDriverUsers(Set<Long> flowIdSet) {
		// 流程角色相关人员
		boolean roleSend = JecnConfigTool.isSendFlowDriverEmail();
		// 流程文件中的驱动规则选择的人员
		boolean processFileSend = JecnConfigTool.isSendFlowDriverEmailToTip();
		if (!roleSend && !processFileSend) {
			return new ArrayList<Object[]>();
		}
		String flowIds = JecnCommonSql.getIdsSet(flowIdSet);
		String subSql = "";
		if (processFileSend) {
			subSql += "	select JFDE.RELATED_ID AS FLOW_ID,JFDE.PEOPLE_ID from JECN_FLOW_DRIVER_EMAIL_T JFDE WHERE JFDE.RELATED_ID IN "
					+ flowIds;
		}
		if (roleSend && processFileSend) {
			subSql += " union ";
		}
		if (roleSend) {
			subSql += " select jfsi.flow_id, u.people_id" + " from jecn_flow_org               jfo,"
					+ "      jecn_flow_org_image         jfoi," + "      process_station_related_t   psrt,"
					+ "      jecn_flow_structure_image_t jfsi," + "      jecn_user                   u,"
					+ "      jecn_user_position_related  u_p" + " where psrt.type = '0'"
					+ "  and psrt.figure_position_id = jfoi.figure_id" + "  and jfoi.org_id = jfo.org_id"
					+ "  and jfo.del_state = 0" + "  and psrt.figure_flow_id = jfsi.figure_id"
					+ "  and jfsi.flow_id in "
					+ flowIds
					+ "  and u.people_id = u_p.people_id"
					+ "  and u.islock = 0"
					+ "  and u_p.figure_id = jfoi.figure_id"
					+ "  union "
					+ " select jfsi.flow_id, u.people_id"
					+ "  from jecn_flow_org               jfo,"
					+ "       jecn_flow_org_image         jfoi,"
					+ "       process_station_related_t   psrt,"
					+ "       jecn_flow_structure_image_t jfsi,"
					+ "       jecn_position_group_r       jpgr,"
					+ "       jecn_user                   u,"
					+ "       jecn_user_position_related  u_p"
					+ " where psrt.type = '1'"
					+ "   and psrt.figure_position_id = jpgr.group_id"
					+ "   and jpgr.figure_id = jfoi.figure_id"
					+ "   and jfoi.org_id = jfo.org_id"
					+ "   and jfo.del_state = 0"
					+ "   and psrt.figure_flow_id = jfsi.figure_id"
					+ "   and jfsi.flow_id in "
					+ flowIds
					+ "   and u.people_id = u_p.people_id"
					+ "   and u.islock = 0"
					+ "   and u_p.figure_id = jfoi.figure_id";
		}

		String sql = "	SELECT distinct f1.flow_id,ju.TRUE_NAME,ju.EMAIL,ju.EMAIL_TYPE,ju.PEOPLE_ID FROM JECN_USER JU INNER JOIN"
				+ "	(" + subSql + "	) F1 ON JU.PEOPLE_ID=F1.PEOPLE_ID";
		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getAllRulesScanOptimizeList(String dutyOrgIds, String startTime, String endTime,
			long projectId, String dirIds, String dutyIds, boolean orgHasChild) throws Exception {
		String orgSql = getDutyOrgSubSource(dutyOrgIds, 1, orgHasChild);
		String sql = "";
		String scanOnTime = "";
		String finishNum = "";
		String betweenAnd = "";
		String lastPubdate = "";
		String pubdate = "";
		String lastExpectDate = "";
		String finish = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			scanOnTime += "	        case when to_char(tmp3.last_expect_publish_date, 'YYYY/MM/DD') >= to_char(tmp3.publish_date, 'YYYY/MM/DD') and tmp3.publish_date is not null then 1"
					+ "  when to_char(tmp3.last_expect_publish_date, 'YYYY/MM/DD') >= to_char(sysdate, 'YYYY/MM/DD') and tmp3.publish_date is null then 2" // 空
					+ "  else 0" // 否
					+ "	 end as scan_ontime,"; // 是否及时优化

			finishNum += "         case when  tmp.publish_date=tmp.last_PUBLISH_DATE then 0"// 按时完成个数
					+ "        when x.next_scan_date <=to_date('"
					+ endTime
					+ "','yyyy-mm-dd hh24:mi:ss') and to_date(to_char(tmp.PUBLISH_DATE,'YYYY/MM/DD'),'YYYY/MM/DD')<=x.next_scan_date  then 1 else 0 end finishNum,";

			betweenAnd += "  and (x.next_scan_date between to_date('" + startTime + "','yyyy-mm-dd hh24:mi:ss') "
					+ "   and to_date('" + endTime + "','yyyy-mm-dd hh24:mi:ss'))" + " ),";

			lastPubdate += "        tmp3.last_PUBLISH_DATE as last_PUBLISH_DATE,";// 需审视流程发布时间

			pubdate += "          tmp3.publish_date as publish_date,"; // 当前发布时间

			lastExpectDate += "       tmp3.last_expect_publish_date as last_expect_publish_date, ";// 需完成的月份（计划）

			finish += "         trunc((b.finishCount / b.totalCount),2)* 100   as finishperce";

		} else {
			scanOnTime += "         case when  CONVERT(varchar(10), tmp3.last_expect_publish_date, 23) >=CONVERT(varchar(10), tmp3.publish_date, 23) and tmp3.publish_date is not null	then 1"
					+ " when CONVERT(varchar(10),getdate(), 23)<=CONVERT(varchar(10), tmp3.last_expect_publish_date, 23) and tmp3.publish_date is null then 2"
					+ "	else 0" + "	end as scan_ontime,"; // 是否及时优化 1为及时 0为不及时
			// 2为空

			finishNum += "         case when  tmp.publish_date=tmp.last_PUBLISH_DATE then 0"// 按时完成个数
					+ "        when dateadd(month,x.expiry,tmp.last_PUBLISH_DATE) <='"
					+ endTime
					+ "' and CONVERT(varchar(10), tmp.PUBLISH_DATE, 23)<=convert(varchar(10),x.next_scan_date,23)  then 1 else 0 end finishNum,";

			betweenAnd += " and " + "    convert(varchar(10),x.next_scan_date,23) between '" + startTime + "'"
					+ "   and '" + endTime + "'" + " ),";

			lastPubdate += "        CONVERT(varchar(10),tmp3.last_PUBLISH_DATE, 23) as last_PUBLISH_DATE,";// 需审视流程发布时间

			pubdate += "         CONVERT(varchar(10), tmp3.publish_date, 23) as publish_date,"; // 当前发布时间

			lastExpectDate += "        CONVERT(varchar(10),tmp3.last_expect_publish_date, 23) as last_expect_publish_date, ";// 需完成的月份（计划）

			finish += "         b.finishCount*100/b.totalCount  AS finishperce";
		}

		sql += "	WITH tmp AS (" + "		SELECT" + "			d.RELATE_ID," + "			d.PUBLISH_DATE,"
				+ "			MAX (d.last_PUBLISH_DATE) AS last_PUBLISH_DATE" + "		FROM" + "			(" + "				SELECT"
				+ "					a.PUBLISH_DATE," + "					a.RELATE_ID," + "					b.PUBLISH_DATE AS last_PUBLISH_DATE,"
				+ "					b.RELATE_ID AS last_RELATE_ID" + "				FROM" + "					JECN_TASK_HISTORY_NEW a"
				+ "				LEFT JOIN JECN_TASK_HISTORY_NEW b ON b.type in (2,3)" + "				AND b.expiry <> 0"
				+ "				AND b.type = a.type" + "				AND b.relate_id = a.relate_id"
				+ "				AND b.publish_date < a.publish_date" + "				WHERE" + "					a.type in (2,3)"
				+ "				AND b.publish_date IS NOT NULL" + "			) d" + "		GROUP BY" + "			d.PUBLISH_DATE,"
				+ "			d.RELATE_ID" + "		UNION ALL" + "			SELECT" + "				t4.relate_id," + "				t4.publish_date,"
				+ "				t4.last_publish_date" + "			FROM" + "				(" + "					SELECT" + "						t2.relate_id,"
				+ "						t2.publish_date," + "						t2.last_publish_date" + "					FROM" + "						(" + "							SELECT"
				+ "								t1.relate_id," + "								MAX (t1.publish_date) AS publish_date,"
				+ "								MAX (t1.publish_date) AS last_publish_date" + "							FROM"
				+ "								jecn_task_history_new t1" + "							WHERE" + "								t1.type in (2,3)" + "							GROUP BY"
				+ "								t1.relate_id" + "						) t2"
				+ "					INNER JOIN jecn_task_history_new t3 ON t2.relate_id = t3.relate_id"
				+ "					AND t2.publish_date = t3.publish_date" + "					AND t3.expiry <> 0"
				+ "					AND t3.type in (2,3)" + "				) t4" + "	)," + "	 tmp2 AS (" + "		SELECT"
				+ "			x.next_scan_date AS last_expect_publish_date," + "			f.id," + "			f.rule_name,"
				+ "			x.VERSION_ID," + "			tmp.last_PUBLISH_DATE," + "			x.Draft_Person," + "			org.org_name,"
				+ "			org.org_id," + "			CASE" + "		WHEN f.Type_Res_People = 0 THEN" + "			u.true_name" + "		ELSE"
				+ "			pos.figure_text" + "		END AS flow_zr," + "		CASE" + "	WHEN f.Type_Res_People = 1 THEN"
				+ "		pos.figure_id" + "	END AS pos_id," + "	 f.Ward_People_Id," + "	 CASE"
				+ "	WHEN tmp.publish_date = tmp.last_PUBLISH_DATE THEN" + "		NULL" + "	ELSE" + "		tmp.publish_date"
				+ "	END AS publish_date,"
				+ finishNum
				+ "	 1 AS totalnum,"
				+ "	 tmp.RELATE_ID,"
				+ "	 x.MODIFY_EXPLAIN,"
				+ "  org.org_id as true_org_id,"
				+ "  org.org_name as true_org_name"
				+ "	FROM"
				+ "		tmp"
				+ "	LEFT JOIN JECN_TASK_HISTORY_NEW x ON x.type in (2,3)"
				+ "	AND x.expiry <> 0"
				+ "	AND tmp.RELATE_ID = x.relate_id"
				+ "	AND tmp.last_PUBLISH_DATE = x.publish_date"
				+ "	INNER JOIN JECN_RULE f ON tmp.RELATE_ID = f.id"
				+ "	AND f.is_dir <> 0"
				+ "	inner JOIN "
				+ orgSql
				+ " org ON org.rule_id=f.id "
				+ "	AND org.PROJECTID = "
				+ projectId
				+ " left join jecn_flow_org jfoc on org.c_org_id=jfoc.org_id"
				+ "	LEFT JOIN jecn_user u ON f.Type_Res_People = 0"
				+ "	AND f.Res_People_Id = u.people_id"
				+ "	LEFT JOIN JECN_FLOW_ORG_IMAGE pos ON f.Type_Res_People = 1"
				+ "	AND f.res_people_id = pos.figure_id" + "	WHERE 1=1";
		if (StringUtils.isNotBlank(dirIds)) {
			sql += " and f.id in " + getChildRules(dirIds);
		}
		sql += betweenAnd + "	 tmp3 AS (" + "		SELECT" + "			a.*, b.finishCount," + "			b.totalCount," + finish
				+ "		FROM" + "			tmp2 a" + "		LEFT JOIN (" + "			SELECT" + "				SUM (c.finishNum) AS finishCount,"
				+ "				SUM (c.totalnum) AS totalCount," + "				c.org_id" + "			FROM" + "				tmp2 c" + "			GROUP BY"
				+ "				c.org_id" + "		) b ON a.org_id = b.org_id" + "	) SELECT" + lastExpectDate + "		tmp3.id,"
				+ "		tmp3.rule_name," + "		tmp3.VERSION_ID," + lastPubdate + "		tmp3.Draft_Person," + "		tmp3.org_id,"
				+ "		tmp3.org_name," + "		tmp3.flow_zr," + "		jorg.org_id AS g_id," + "		jorg.org_name AS g_name,"
				+ "		ju.people_id," + "		ju.true_name," + pubdate + "		tmp3.finishCount," + "		tmp3.totalCount,"
				+ "		tmp3.finishperce," + scanOnTime + "	 tmp3.org_name,"
				+ "	 tmp3.MODIFY_EXPLAIN,jfoc.org_id as true_org_id,jfoc.org_name as true_org_name" + "	FROM"
				+ "		tmp3" + "	LEFT JOIN jecn_user ju ON tmp3.Ward_People_Id = ju.people_id"
				+ "	LEFT JOIN JECN_USER_POSITION_RELATED jref ON jref.people_id = ju.people_id"
				+ "	LEFT JOIN JECN_FLOW_ORG_IMAGE jpos ON jref.figure_id = jpos.figure_id"
				+ "	LEFT JOIN JECN_FLOW_ORG jorg ON jorg.org_id = jpos.org_id" + "	AND jorg.PROJECTID = " + projectId
				+ " LEFT JOIN JECN_RULE JR ON JR.ID=tmp3.id" + " left join jecn_flow_org jfoc on jfoc.org_id=jr.org_id"
				+ "	ORDER BY tmp3.id,tmp3.publish_date,tmp3.org_id ASC";

		return this.listNativeSql(sql);
	}

	/**
	 * 获得下两个月需要审视的流程的数量与部门id
	 * 
	 * @param dutyOrgIds
	 *            部门的ids
	 * @param projectId
	 *            项目id
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @return objs
	 */
	@Override
	public List<Object[]> getRuleNextTwoMonthScanList(String dutyOrgIds, String nextScanStartTime,
			String nextScanEndTime, long projectId, String dirIds, String dutyIds, boolean hasChildOrg)
			throws Exception {
		String orgSql = getDutyOrgSubSource(dutyOrgIds, 1, hasChildOrg);
		String sql = "with tmp as("// 有多次发布记录的数据
				+ "	select d.RELATE_ID,"
				+ "	       d.PUBLISH_DATE,"
				+ "	       max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE"
				+ "	  from (select a.PUBLISH_DATE,"
				+ "	               a.RELATE_ID,"
				+ "	               b.PUBLISH_DATE as last_PUBLISH_DATE,"
				+ "	               b.RELATE_ID as last_RELATE_ID"
				+ "	          from JECN_TASK_HISTORY_NEW a"
				+ "	          left join JECN_TASK_HISTORY_NEW b on b.type in (2,3) and b.expiry<>0"
				+ "	                                           and b.type = a.type"
				+ "	                                           and b.relate_id = a.relate_id"
				+ "	                                           and b.publish_date <"
				+ "	                                               a.publish_date"
				+ "	         where a.type in (2,3) "
				+ "	           and a.expiry <> 0"
				+ "	           and b.publish_date is not null) d"
				+ "	 group by d.PUBLISH_DATE, d.RELATE_ID"
				+ " union all"
				+ "	select t1.RELATE_ID,"// 有多次发布记录的数据时最新一次发布数据 只有一次发布的数据
				+ "	       MAX(T1.PUBLISH_DATE) AS PUBLISH_DATE,"
				+ "	       MAX(T1.PUBLISH_DATE) AS last_PUBLISH_DATE"
				+ "	  from JECN_TASK_HISTORY_NEW t1"
				+ "	 where t1.type in (2,3) "
				+ "	   and t1.expiry <> 0"
				+ "	 group by t1.RELATE_ID, t1.type"
				+ "	having(count(*) >= 1)"
				+ "	 )"
				+ "	select sum(c.totalnum) as totalCount,c.org_id"
				+ "	  from (select f.rule_name, "// 需审视流程
				+ "               tmp.last_PUBLISH_DATE, "// 需审视流程发布时间
				+ "               org.org_name, "// 责任部门
				+ "           org.org_id,"
				+ "            tmp.publish_date, "// 当前发布时间
				+ "            1 as totalnum," + "         tmp.RELATE_ID"
				+ "     from tmp"
				+ "     left join JECN_TASK_HISTORY_NEW x on x.type in (2,3) "
				+ "     and x.expiry <> 0"
				+ "     and tmp.RELATE_ID = x.relate_id"
				+ "      and tmp.last_PUBLISH_DATE ="
				+ "      x.publish_date"
				+ "     inner join JECN_RULE f on tmp.RELATE_ID = f.id"
				+ "                                  and f.is_dir <> 0 " + "     inner join "
				+ orgSql
				+ " org on org.rule_id = f.id" + " and org.PROJECTID =" + projectId + "    where 1=1 ";
		if (StringUtils.isNotBlank(dirIds)) {
			sql += " and f.id in " + getChildRules(dirIds);
		}
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql += " and (x.next_scan_date between" + "      to_date('" + nextScanStartTime
					+ "', 'yyyy-mm-dd hh24:mi:ss') and" + "       to_date('" + nextScanEndTime
					+ "', 'yyyy-mm-dd hh24:mi:ss'))) c" + " group by c.org_id, c.org_name";
		} else {
			sql += "  and convert(varchar(10),x.next_scan_date,23) between" + "      '" + nextScanStartTime + "' and"
					+ "      '" + nextScanEndTime + "') c" + " group by c.org_id, c.org_name";
		}

		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getRuleDirProcessScanOptimizeList(String dirIds, String startTime, String endTime,
			long projectId, String dutyIds) {

		String sql = "";
		String scanOnTime = "";
		String finishNum = "";
		String betweenAnd = "";
		String lastPubdate = "";
		String pubdate = "";
		String lastExpectDate = "";
		String finish = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			scanOnTime += "	        case when to_char(tmp3.last_expect_publish_date, 'YYYY/MM/DD') >= to_char(tmp3.publish_date, 'YYYY/MM/DD') and tmp3.publish_date is not null then 1"
					+ "  when to_char(tmp3.last_expect_publish_date, 'YYYY/MM/DD') >= to_char(sysdate, 'YYYY/MM/DD') and tmp3.publish_date is null then 2" // 空
					+ "  else 0" // 否
					+ "	 end as scan_ontime,"; // 是否及时优化

			finishNum += "         case when  tmp.publish_date=tmp.last_PUBLISH_DATE then 0"// 按时完成个数
					+ "        when x.next_scan_date <=to_date('"
					+ endTime
					+ "','yyyy-mm-dd hh24:mi:ss') and to_date(to_char(tmp.PUBLISH_DATE,'YYYY/MM/DD'),'YYYY/MM/DD')<=x.next_scan_date  then 1 else 0 end finishNum,";

			betweenAnd += "  and (x.next_scan_date between to_date('" + startTime + "','yyyy-mm-dd hh24:mi:ss') "
					+ "   and to_date('" + endTime + "','yyyy-mm-dd hh24:mi:ss'))" + " ),";

			lastPubdate += "        tmp3.last_PUBLISH_DATE as last_PUBLISH_DATE,";// 需审视流程发布时间

			pubdate += "          tmp3.publish_date as publish_date,"; // 当前发布时间

			lastExpectDate += "       tmp3.last_expect_publish_date as last_expect_publish_date, ";// 需完成的月份（计划）

			finish += "         trunc((b.finishCount / b.totalCount),2)* 100   as finishperce";

		} else {
			scanOnTime += "         case when  CONVERT(varchar(10), tmp3.last_expect_publish_date, 23) >=CONVERT(varchar(10), tmp3.publish_date, 23) and tmp3.publish_date is not null	then 1"
					+ " when CONVERT(varchar(10),getdate(), 23)<=CONVERT(varchar(10), tmp3.last_expect_publish_date, 23) and tmp3.publish_date is null then 2"
					+ "	else 0" + "	end as scan_ontime,"; // 是否及时优化 1为及时 0为不及时
			// 2为空

			finishNum += "         case when  tmp.publish_date=tmp.last_PUBLISH_DATE then 0"// 按时完成个数
					+ "        when dateadd(month,x.expiry,tmp.last_PUBLISH_DATE) <='"
					+ endTime
					+ "' and CONVERT(varchar(10), tmp.PUBLISH_DATE, 23)<=convert(varchar(10),x.next_scan_date,23)  then 1 else 0 end finishNum,";

			betweenAnd += " and " + "    convert(varchar(10),x.next_scan_date,23) between '" + startTime + "'"
					+ "   and '" + endTime + "'" + " ),";

			lastPubdate += "        CONVERT(varchar(10),tmp3.last_PUBLISH_DATE, 23) as last_PUBLISH_DATE,";// 需审视流程发布时间

			pubdate += "         CONVERT(varchar(10), tmp3.publish_date, 23) as publish_date,"; // 当前发布时间

			lastExpectDate += "        CONVERT(varchar(10),tmp3.last_expect_publish_date, 23) as last_expect_publish_date, ";// 需完成的月份（计划）

			finish += "         b.finishCount*100/b.totalCount  AS finishperce";
		}

		sql += "	WITH tmp AS (" + "		SELECT" + "			d.RELATE_ID," + "			d.PUBLISH_DATE,"
				+ "			MAX (d.last_PUBLISH_DATE) AS last_PUBLISH_DATE" + "		FROM" + "			(" + "				SELECT"
				+ "					a.PUBLISH_DATE," + "					a.RELATE_ID," + "					b.PUBLISH_DATE AS last_PUBLISH_DATE,"
				+ "					b.RELATE_ID AS last_RELATE_ID" + "				FROM" + "					JECN_TASK_HISTORY_NEW a"
				+ "				LEFT JOIN JECN_TASK_HISTORY_NEW b ON b.type in (2,3)" + "				AND b.expiry <> 0"
				+ "				AND b.type = a.type" + "				AND b.relate_id = a.relate_id"
				+ "				AND b.publish_date < a.publish_date" + "				WHERE" + "					a.type in (2,3)"
				+ "				AND b.publish_date IS NOT NULL" + "			) d" + "		GROUP BY" + "			d.PUBLISH_DATE,"
				+ "			d.RELATE_ID" + "		UNION ALL" + "			SELECT" + "				t4.relate_id," + "				t4.publish_date,"
				+ "				t4.last_publish_date" + "			FROM" + "				(" + "					SELECT" + "						t2.relate_id,"
				+ "						t2.publish_date," + "						t2.last_publish_date" + "					FROM" + "						(" + "							SELECT"
				+ "								t1.relate_id," + "								MAX (t1.publish_date) AS publish_date,"
				+ "								MAX (t1.publish_date) AS last_publish_date" + "							FROM"
				+ "								jecn_task_history_new t1" + "							WHERE" + "								t1.type in (2,3)" + "							GROUP BY"
				+ "								t1.relate_id" + "						) t2"
				+ "					INNER JOIN jecn_task_history_new t3 ON t2.relate_id = t3.relate_id"
				+ "					AND t2.publish_date = t3.publish_date" + "					AND t3.expiry <> 0"
				+ "					AND t3.type in (2,3)" + "				) t4" + "	)," + "	 tmp2 AS (" + "		SELECT"
				+ "			x.next_scan_date AS last_expect_publish_date," + "			f.id," + "			f.rule_name,"
				+ "			x.VERSION_ID," + "			tmp.last_PUBLISH_DATE," + "			x.Draft_Person," + "			f.dir_name,"
				+ "			f.dir_id," + "			CASE" + "		WHEN f.Type_Res_People = 0 THEN" + "			u.true_name" + "		ELSE"
				+ "			pos.figure_text" + "		END AS flow_zr," + "		CASE" + "	WHEN f.Type_Res_People = 1 THEN"
				+ "		pos.figure_id" + "	END AS pos_id," + "	 f.Ward_People_Id," + "	 CASE"
				+ "	WHEN tmp.publish_date = tmp.last_PUBLISH_DATE THEN" + "		NULL" + "	ELSE" + "		tmp.publish_date"
				+ "	END AS publish_date,"
				+ finishNum
				+ "	 1 AS totalnum,"
				+ "	 tmp.RELATE_ID,"
				+ "	 x.MODIFY_EXPLAIN"
				+ "	FROM"
				+ "		tmp"
				+ "	LEFT JOIN JECN_TASK_HISTORY_NEW x ON x.type in (2,3)"
				+ "	AND x.expiry <> 0"
				+ "	AND tmp.RELATE_ID = x.relate_id"
				+ "	AND tmp.last_PUBLISH_DATE = x.publish_date"
				+ "	INNER JOIN ("
				+ "	  select p.id as dir_id,p.rule_name as dir_name,c.* from jecn_rule p"
				+ "	  left join jecn_rule c on p.t_path is not null and c.t_path is not null and c.is_dir in (1,2) and c.t_path like p.t_path"
				+ JecnCommonSql.getConcatChar()
				+ "'%'"
				+ "     where p.id in ("
				+ dirIds
				+ ")"
				+ ") f ON tmp.RELATE_ID = f.id"
				+ "	LEFT JOIN jecn_user u ON f.Type_Res_People = 0"
				+ "	AND f.Res_People_Id = u.people_id"
				+ "	LEFT JOIN JECN_FLOW_ORG_IMAGE pos ON f.Type_Res_People = 1"
				+ "	AND f.res_people_id = pos.figure_id" + "	WHERE 1=1 ";
		sql += betweenAnd + "	 tmp3 AS (" + "		SELECT" + "			a.*, b.finishCount," + "			b.totalCount," + finish
				+ "		FROM" + "			tmp2 a" + "		LEFT JOIN (" + "			SELECT" + "				SUM (c.finishNum) AS finishCount,"
				+ "				SUM (c.totalnum) AS totalCount," + "				c.dir_id" + "			FROM" + "				tmp2 c" + "			GROUP BY"
				+ "				c.dir_id" + "		) b ON a.dir_id = b.dir_id" + "	) SELECT" + lastExpectDate + "		tmp3.id,"
				+ "		tmp3.rule_name," + "		tmp3.VERSION_ID," + lastPubdate + "		tmp3.Draft_Person," + "		tmp3.dir_id,"
				+ "		tmp3.dir_name," + "		tmp3.flow_zr," + "		jorg.org_id AS g_id," + "		jorg.org_name AS g_name,"
				+ "		ju.people_id," + "		ju.true_name," + pubdate + "		tmp3.finishCount," + "		tmp3.totalCount,"
				+ "		tmp3.finishperce," + scanOnTime + "	 tmp3.dir_name," + "	 tmp3.MODIFY_EXPLAIN,"
				+ "		jfo.org_id as true_org_id,jfo.org_name AS true_org_name " + "	FROM" + "		tmp3"
				+ "	LEFT JOIN jecn_user ju ON tmp3.Ward_People_Id = ju.people_id"
				+ "	LEFT JOIN JECN_USER_POSITION_RELATED jref ON jref.people_id = ju.people_id"
				+ "	LEFT JOIN JECN_FLOW_ORG_IMAGE jpos ON jref.figure_id = jpos.figure_id"
				+ "	LEFT JOIN JECN_FLOW_ORG jorg ON jorg.org_id = jpos.org_id" + "	AND jorg.PROJECTID = " + projectId
				+ " left join jecn_rule_t jr on jr.id=tmp3.id" + " left join jecn_flow_org jfo on jfo.org_id=jr.org_id"
				+ "	ORDER BY" + "		tmp3.id,tmp3.publish_date,tmp3.dir_id ASC";

		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getRuleDirNextTwoMonthScanList(String dirIds, String nextScanStartTime,
			String nextScanEndTime, long projectId, String dutyIds) {

		String sql = "with tmp as("// 有多次发布记录的数据
				+ "	select d.RELATE_ID,"
				+ "	       d.PUBLISH_DATE,"
				+ "	       max(d.last_PUBLISH_DATE) as last_PUBLISH_DATE"
				+ "	  from (select a.PUBLISH_DATE,"
				+ "	               a.RELATE_ID,"
				+ "	               b.PUBLISH_DATE as last_PUBLISH_DATE,"
				+ "	               b.RELATE_ID as last_RELATE_ID"
				+ "	          from JECN_TASK_HISTORY_NEW a"
				+ "	          left join JECN_TASK_HISTORY_NEW b on b.type in (2,3) and b.expiry<>0"
				+ "	                                           and b.type = a.type"
				+ "	                                           and b.relate_id = a.relate_id"
				+ "	                                           and b.publish_date <"
				+ "	                                               a.publish_date"
				+ "	         where a.type in (2,3)"
				+ "	           and a.expiry <> 0"
				+ "	           and b.publish_date is not null) d"
				+ "	 group by d.PUBLISH_DATE, d.RELATE_ID"
				+ " union all"
				+ "	select t1.RELATE_ID,"// 有多次发布记录的数据时最新一次发布数据 只有一次发布的数据
				+ "	       MAX(T1.PUBLISH_DATE) AS PUBLISH_DATE,"
				+ "	       MAX(T1.PUBLISH_DATE) AS last_PUBLISH_DATE"
				+ "	  from JECN_TASK_HISTORY_NEW t1"
				+ "	 where t1.type in (2,3)"
				+ "	   and t1.expiry <> 0"
				+ "	 group by t1.RELATE_ID, t1.type"
				+ "	having(count(*) >= 1)"
				+ "	 )"
				+ "	select sum(c.totalnum) as totalCount,c.dir_id"
				+ "	  from (select f.rule_name, "// 需审视流程
				+ "            tmp.last_PUBLISH_DATE, "// 需审视流程发布时间
				+ "            f.dir_name, "// 责任部门
				+ "            f.dir_id,"
				+ "            tmp.publish_date, "// 当前发布时间
				+ "            1 as totalnum,"
				+ "         tmp.RELATE_ID"
				+ "     from tmp"
				+ "     left join JECN_TASK_HISTORY_NEW x on x.type in (2,3)"
				+ "     and x.expiry <> 0"
				+ "     and tmp.RELATE_ID = x.relate_id"
				+ "      and tmp.last_PUBLISH_DATE ="
				+ "      x.publish_date"
				+ "	INNER JOIN ("
				+ "	  select p.id as dir_id,p.rule_name as dir_name,c.* from jecn_rule p"
				+ "	  left join jecn_rule c on p.t_path is not null and c.t_path is not null and c.is_dir in (1,2) and c.t_path like p.t_path"
				+ JecnCommonSql.getConcatChar() + "'%'" + "     where p.id in (" + dirIds
				+ ")"
				+ ") f ON tmp.RELATE_ID = f.id" + "    where 1=1 ";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql += " and (x.next_scan_date between" + "      to_date('" + nextScanStartTime
					+ "', 'yyyy-mm-dd hh24:mi:ss') and" + "       to_date('" + nextScanEndTime
					+ "', 'yyyy-mm-dd hh24:mi:ss'))) c" + " group by c.dir_id";
		} else {
			sql += "  and convert(varchar(10),x.next_scan_date,23) between" + "      '" + nextScanStartTime + "' and"
					+ "      '" + nextScanEndTime + "') c" + " group by c.dir_id";
		}

		return this.listNativeSql(sql);

	}

	@Override
	public List<Object[]> finaAllRoleAndPosGroupRelates(Long flowId, boolean isPub) {
		if (isPub) {
			String sql = "select t.figure_id,jpg.id,jpg.name from "
					+ "  jecn_flow_structure_image t,PROCESS_STATION_RELATED rs,JECN_POSITION_GROUP jpg where jpg.id = rs.figure_position_id and rs.type='1' and rs.figure_flow_id=t.figure_id and t.figure_type in "
					+ JecnCommonSql.getRoleString() + " and t.flow_id=?";
			return this.listNativeSql(sql, flowId);
		} else {
			String sql = "select t.figure_id,jpg.id,jpg.name from "
					+ "  jecn_flow_structure_image_t t,PROCESS_STATION_RELATED_t rs,JECN_POSITION_GROUP jpg where jpg.id = rs.figure_position_id and rs.type='1' and rs.figure_flow_id=t.figure_id and t.figure_type in "
					+ JecnCommonSql.getRoleString() + " and t.flow_id=?";
			return this.listNativeSql(sql, flowId);
		}

	}

	@Override
	public List<Object[]> getActivityRisks(Long flowId, boolean isPub) {
		String sql;
		if (isPub) {
			sql = "select jfsit.FIGURE_ID,jfsit.FIGURE_TEXT,jr.ID,jr.NAME  from JECN_RISK jr"
					+ " inner join JECN_CONTROL_POINT jcpt on jr.id = jcpt.Risk_Id"
					+ " inner join JECN_FLOW_STRUCTURE_IMAGE jfsit on jcpt.Active_Id =" + " jfsit.figure_id"
					+ " where jfsit.flow_id=" + flowId;
		} else {
			sql = "select  jfsit.FIGURE_ID,jfsit.FIGURE_TEXT,jr.ID,jr.NAME  from JECN_RISK jr"
					+ " inner join JECN_CONTROL_POINT_T jcpt on jr.id = jcpt.Risk_Id"
					+ " inner join JECN_FLOW_STRUCTURE_IMAGE_T jfsit on jcpt.Active_Id =" + " jfsit.figure_id"
					+ " where jfsit.flow_id=" + flowId;
		}
		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getActivityIts(Long flowId, boolean isPub) {
		String pub = isPub ? "" : "_T";
		String sql = " select jfsi.figure_id,jfsi.figure_text,jfst.flow_sustain_tool_id,jfst.flow_sustain_tool_describe from jecn_flow_structure_image"
				+ pub
				+ " jfsi"
				+ " inner join JECN_ACTIVE_ONLINE"
				+ pub
				+ " jao on jfsi.figure_id=jao.ACTIVE_ID"
				+ " inner join jecn_flow_sustain_tool jfst on jao.tool_id=jfst.flow_sustain_tool_id"
				+ " where jfsi.flow_id=" + flowId;
		return this.listNativeSql(sql);
	}

	@Override
	public List<JecnFlowSustainTool> getFlowIts(Long flowId, boolean isPub) {
		String pub = isPub ? "" : "_T";
		String sql = " select jfst.* from jecn_flow_structure_image" + pub + " jfsi" + " inner join JECN_ACTIVE_ONLINE"
				+ pub + " jao on jfsi.figure_id=jao.ACTIVE_ID"
				+ " inner join jecn_flow_sustain_tool jfst on jao.tool_id=jfst.flow_sustain_tool_id"
				+ " where jfsi.flow_id=" + flowId;
		return this.getSession().createSQLQuery(sql).addEntity(JecnFlowSustainTool.class).list();
	}

	@Override
	public List<Long> getRelatedFileIds(List<Long> flowIds) throws Exception {
		String sql = "SELECT FT.FILE_ID" + "  FROM JECN_FLOW_STRUCTURE_T T" + "  INNER JOIN JECN_FILE_T FT"
				+ "    ON FT.FLOW_ID = T.FLOW_ID" + " WHERE FT.FLOW_ID IS NOT NULL" + "   AND FT.IS_DIR = 0"
				+ "   AND T.FLOW_ID in " + JecnCommonSql.getIds(flowIds);
		List<Long> ids = this.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
		return ids;
	}

	@Override
	public List<JecnTreeBean> getOrgAccessPermissions(Long relateId, int type, boolean isPub) throws Exception {
		String sql = "";
		if (isPub) {
			sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count ,joap.access_type"
					+ " from jecn_flow_org jfo,JECN_ORG_ACCESS_PERMISSIONS joap where jfo.org_id = joap.org_id and joap.relate_id=? and joap.type=? and jfo.del_state=0"
					+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
		} else {
			sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count,joap.access_type"
					+ " from jecn_flow_org jfo,JECN_ORG_ACCESS_PERM_T joap where jfo.org_id = joap.org_id and joap.relate_id=? and joap.type=? and jfo.del_state=0"
					+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
		}
		List<Object[]> listOrg = listNativeSql(sql, relateId, type);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : listOrg) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectOrgAccess(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	@Override
	public List<Object[]> getDutyOrgRulesInfo(String orgId, Long projectId, String endTime, boolean orgHasChild) {
		String sql = "";
		String orgSql = getDutyOrgSubSource(orgId, 1, orgHasChild);
		// if (JecnContants.dbType.equals(DBType.ORACLE)) {
		sql = "with t22 as" + " (select max(h.publish_date) as publish_date," + "         jfst.id as rule_id,"
				+ "             jfo.org_id," + "             jfo.org_name" + "    from jecn_rule_t jfst"
				+ "    inner join "
				+ orgSql
				+ " jfo on jfo.rule_id = jfst.id"
				+ "    inner join jecn_task_history_new h"
				+ "      on jfst.id = h.relate_id"
				+ "     and h.type in (2,3)"
				+ "     and "
				+ lessEqualThan("h.publish_date", endTime)
				+ "   where jfst.del_state = 0"
				+ "     and "
				+ lessEqualThan("jfst.create_date", endTime)
				+ "   group by jfst.id, jfo.org_id, jfo.org_name),"
				+ "t3 as"
				+ " (select case"
				+ "  WHEN x2.id is null then null"
				+ "           when t22.publish_date is null then"
				+ "            x2.pub_time"
				+ "           else"
				+ "            t22.publish_date"
				+ "         end as last_publish_date,"
				+ "         t22.rule_id,"
				+ "         t22.org_id,"
				+ "         t22.org_name"
				+ "    from t22"
				+ "    left join jecn_rule x2"
				+ "      on x2.id = t22.rule_id"
				+ "     and "
				+ lessEqualThan("x2.pub_time", endTime)
				+ "),"
				// /////////////
				+ "t5 as("

				+ " select t3.last_publish_date,"
				+ "        t3.rule_id,"
				+ "        t3.org_id,"
				+ "        t3.org_name,"
				+ "        (select min(publish_date) from"
				+ "        jecn_task_history_new jthn"
				+ "        where t3.rule_id=jthn.relate_id and jthn.type in (2,3)"
				+ "        and publish_date>t3.last_publish_date) as true_next_publish_date"
				+ "   from t3"
				+ ")"
				// /////////////

				+ " select jfs.id,"
				+ "       jfs.per_id,"
				+ "       jfs.rule_name,"
				+ "       jfs.rule_number,"
				+ "       t5.last_publish_date,"
				+ "       jt.version_id,"
				+ "       t5.org_name,"
				+ "       t5.org_id,"
				+ "       jfs.type_res_people,"
				+ "       jfs.res_people_id,"
				+ "       case"
				+ "         when jfs.type_res_people = 0 then"
				+ "          (select ju.true_name"
				+ "             from jecn_user ju"
				+ "            where ju.people_id = jfs.res_people_id)"
				+ "         when jfs.type_res_people = 1 then"
				+ "          (select jfoi.figure_text"
				+ "             from jecn_flow_org_image jfoi, jecn_flow_org jfo"
				+ "            where jfoi.org_id = jfo.org_id"
				+ "              and jfo.del_state = 0"
				+ "              and jfoi.figure_id = jfs.res_people_id)"
				+ "       end as res_people,"
				+ "       jfs.per_id as p_flow_id,"
				+ "       gju.people_id,"
				+ "       gju.TRUE_NAME,"
				+ "       jt.draft_person,"
				+ "       jt.expiry,"
				+ "       t5.org_id,"
				+ "       pubCount,"
				+ "       notPubCount,"
				+ "       case " // ---------------
				+ "     when"
				+ "       jt.expiry <> 0 and  t5.true_next_publish_date is not null and "
				+ bigEqual("jt.next_scan_date", "t5.true_next_publish_date")
				+ "       then 1"
				+ "     when  jt.expiry <> 0 and  t5.true_next_publish_date is not null and "
				+ small("jt.next_scan_date", "t5.true_next_publish_date")
				+ "       then 0"
				+ "     when jt.expiry<>0 and "
				+ smallThanCurTime("jt.next_scan_date")
				+ " then"
				+ "       0 "
				+ "       else "
				+ "       2   "
				+ "       end as scan_ontime,"
				+ "       case when jt.expiry<>0 then"
				+ "       jt.next_scan_date   " // -----------------
				+ "       end as next_scan_date,"
				+ "       jfoo.org_id as true_org_id,"
				+ "       jfoo.org_name true_org_name"
				+ "  from t5"
				+ "  left join (select sum(case"
				+ "                          when t5.last_publish_date is not null then"
				+ "                           1"
				+ "                          else"
				+ "                           0"
				+ "                        end) as pubCount,"
				+ "                    sum(case"
				+ "                          when t5.last_publish_date is null then"
				+ "                           1"
				+ "                          else"
				+ "                           0"
				+ "                        end) as notPubCount,"
				+ "                    t5.org_id"
				+ "               from t5"
				+ "              group by t5.org_id) count"
				+ "    on t5.org_id = count.org_id"
				+ "  left join jecn_rule_t jfs"
				+ "    on jfs.id = t5.rule_id"
				+ "  left join jecn_task_history_new jt"
				+ "    on jt.publish_date = t5.last_publish_date"
				+ "   and jt.relate_id = t5.rule_id and jt.type in (2,3)"
				// + "  left join jecn_rule jfst"
				// + "    on jfst.id = jfs.id"
				+ "  left join jecn_user gju"
				+ "    on gju.people_id = jfs.Ward_People_Id"
				+ "  left join jecn_flow_org jfoo on jfs.org_id=jfoo.org_id"
				+ " order by t5.org_id,jfs.per_id,jfs.id asc";

		// } else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
		// sql = " WITH t1 as" +
		// " (SELECT max(h.publish_date) as publish_date,jfst.flow_id,jfo.org_id,jfo.org_name"
		// + " FROM jecn_flow_structure_t jfst"
		// +
		// " inner JOIN jecn_flow_basic_info_t jfbit ON jfst.flow_id=jfbit.flow_id"
		// + " inner JOIN "
		// + orgSql
		// + " jfo ON jfo.flow_id=jfst.flow_id"
		// + " inner JOIN JECN_TASK_HISTORY_NEW h ON jfst.flow_id = h.RELATE_ID"
		// + " AND h.Type = 0"
		// + " AND h.publish_date <= '"
		// + endTime
		// + "'"
		// + "	where jfst.del_state = 0"
		// + "  AND jfst.create_date<='"
		// + endTime
		// + "'"
		// + "  AND jfst.projectId="
		// + projectId
		// + "  group by jfst.flow_id,jfo.org_id,jfo.org_name"
		// + "  ),"
		// + "	     t2 as ("
		// + "	SELECT CASE"
		// + "  WHEN x2.flow_id is null then null"
		// + "	              WHEN t1.publish_date IS NULL THEN x2.pub_time"
		// + "	              ELSE t1.publish_date"
		// + "	          END AS publish_date,"
		// + "	          t1.flow_id,"
		// + "	          t1.org_id,"
		// + "	          t1.org_name"
		// + "	   FROM t1"
		// + "	   LEFT JOIN JECN_FLOW_STRUCTURE x2 ON x2.flow_id=t1.flow_id"
		// + "	   AND x2.pub_time <='"
		// + endTime
		// + "'"
		// + "	),"
		// + "   t3 as "
		// + " (SELECT t2.publish_date AS last_publish_date, "
		// + "   t2.flow_id, "
		// + "  t2.org_id, "
		// + "  t2.org_name, "
		// + "  pubCount, "
		// + "  notPubCount "
		// + "    FROM t2 "
		// + "    LEFT JOIN (SELECT sum(CASE "
		// + "                          WHEN t2.publish_date IS NOT NULL THEN "
		// + "                            1 "
		// + "                           ELSE "
		// + "                           0 "
		// + "                         END) AS pubCount, "
		// + "                     sum(CASE "
		// + "                          WHEN t2.publish_date IS NULL THEN "
		// + "                            1 "
		// + "                           ELSE "
		// + "                            0 "
		// + "                        END) AS notPubCount, "
		// + "                     org_id "
		// + "                FROM t2 "
		// + "               GROUP BY org_id) tt "
		// + "      ON t2.org_id = tt.org_id "
		// + "   left join jecn_task_history_new next_scan_time  "
		// +
		// "     on t2.flow_id = next_scan_time.relate_id and  next_scan_time.publish_date > t2.publish_date"
		// + "   group by t2.publish_date,t2.flow_id, "
		// + "       t2.org_id, "
		// + "       t2.org_name, "
		// + "       pubCount, "
		// + "       notPubCount"
		// + " ),"
		// + " t4 as("
		// + "  select  t3.last_publish_date,"
		// + "          t3.flow_id,"
		// + "          t3.org_id,"
		// + "          t3.org_name,"
		// + "          t3.pubCount,"
		// + "          t3.notPubCount,"
		// + "          (select min(publish_date) from"
		// + "          jecn_task_history_new jthn"
		// + "          where t3.flow_id=jthn.relate_id and jthn.type=0"
		// +
		// "          and publish_date>t3.last_publish_date) as true_next_publish_date"
		// + "     from t3"
		// + ")"
		// + " SELECT jfs.flow_id,"
		// + "    jfs.pre_flow_id,"
		// + "   jfs.flow_name,"
		// + "   jfs.flow_id_input,"
		// + "   t4.last_publish_date,"
		// + "   jt.version_id,"
		// + "  t4.org_name,"
		// + "  t4.org_id,"
		// + "  t.type_res_people,"
		// + "  t.res_people_id,"
		// + "   CASE"
		// + "    WHEN t.type_res_people = 0 THEN"
		// + "          (SELECT ju.true_name"
		// + "           FROM jecn_user ju"
		// + "            WHERE ju.people_id = t.res_people_id)"
		// + "     WHEN t.type_res_people = 1 THEN"
		// + "           (SELECT jfoi.figure_text"
		// + "         FROM jecn_flow_org_image jfoi,"
		// + "                                   jecn_flow_org jfo"
		// + "         WHERE jfoi.org_id = jfo.org_id"
		// + "           AND jfo.del_state = 0"
		// + "              AND jfoi.figure_id = t.res_people_id)"
		// + "  END AS res_people,"
		// + " jfst.flow_id AS p_flow_id,"
		// + " gju.people_id,"
		// + "  gju.TRUE_NAME,"
		// + "  jt.draft_person,"
		// + "  jt.expiry,"
		// + "  t4.org_id,"
		// + "   pubCount,"
		// + "   notPubCount,"
		// + " case   " // -------------
		// + " when"
		// + "    jt.expiry <> 0 and  t4.true_next_publish_date is not null"
		// +
		// "    and convert(varchar(10), jt.next_scan_date, 23)>= convert(varchar(10), t4.true_next_publish_date, 23)"
		// + "    then 1"
		// + "  when  jt.expiry <> 0 and  t4.true_next_publish_date is not null"
		// +
		// "    and convert(varchar(10), jt.next_scan_date, 23)< convert(varchar(10), t4.true_next_publish_date, 23)"
		// + "    then 0"
		// + " when jt.expiry<>0 and"
		// + " convert(varchar(10),jt.next_scan_date,"
		// + " 23)< convert(varchar(10),getdate(),23) then"
		// + " 0 "
		// + " else "
		// + " 2   "
		// + " end as scan_ontime,"
		// + "       case when jt.expiry<>0 then"
		// + " convert(varchar(10),jt.next_scan_date,23)  " // -------------
		// + " end as next_scan_date,"
		// + "       jfoo.org_id as true_org_id,"
		// + "       jfoo.org_name true_org_name"
		// + " FROM JECN_FLOW_STRUCTURE_T jfs"
		// + " INNER JOIN t4 ON jfs.flow_id=t4.flow_id"
		// + " LEFT JOIN jecn_flow_basic_info_t t ON t.flow_id = jfs.flow_id"
		// + " left join jecn_task_history_new jt"
		// + "   on jt.publish_date = t4.last_publish_date"
		// + "   and jt.relate_id = t4.flow_id and jt.type=0"
		// + " LEFT JOIN jecn_flow_structure jfst ON jfst.flow_id = jfs.flow_id"
		// + " LEFT JOIN jecn_user gju ON gju.people_id = t.Ward_People_Id"
		// + "  left join JECN_FLOW_RELATED_ORG zo on zo.flow_id = jfs.flow_id"
		// + "  left join jecn_flow_org jfoo on zo.org_id=jfoo.org_id"
		// + " where jfs.del_state=0"
		// + " ORDER BY t4.org_id,jfs.pre_flow_id,jfs.flow_id asc";
		// }

		return this.listNativeSql(sql);
	}

	private String smallThanCurTime(String a) {
		String b = null;
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			b = "sysdate";
		} else {
			b = "getdate()";
		}
		return small(a, b);
	}

	private String small(String a, String b) {
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			// "       and to_date(to_char(jt.next_scan_date,'yyyy-mm-dd'),'yyyy-mm-dd') < to_date(to_char(t5.true_next_publish_date,'yyyy-mm-dd'),'yyyy-mm-dd')"
			return " to_date(to_char(" + a + ",'yyyy-mm-dd'),'yyyy-mm-dd') < to_date(to_char(" + b
					+ ",'yyyy-mm-dd'),'yyyy-mm-dd')";
		} else {
			// convert(varchar(10), jt.next_scan_date, 23)< convert(varchar(10),
			// t4.true_next_publish_date, 23)
			return " convert(varchar(10), " + a + ", 23)< convert(varchar(10), " + b + ", 23)";
		}

	}

	private String bigEqual(String a, String b) {
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			// "       and to_date(to_char(jt.next_scan_date,'yyyy-mm-dd'),'yyyy-mm-dd') >= to_date(to_char(t5.true_next_publish_date,'yyyy-mm-dd'),'yyyy-mm-dd')"
			return " to_date(to_char(" + a + ",'yyyy-mm-dd'),'yyyy-mm-dd') >= to_date(to_char(" + b
					+ ",'yyyy-mm-dd'),'yyyy-mm-dd')";
		} else {
			// convert(varchar(10), jt.next_scan_date, 23)>=
			// convert(varchar(10), t4.true_next_publish_date, 23)
			return " convert(varchar(10), " + a + ", 23)>= convert(varchar(10), " + b + ", 23)";
		}
	}

	private String lessEqualThan(String dbColumn, String time) {
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			// h.publish_date <="
			// + "         to_date('"
			// + endTime
			// + "', 'yyyy-mm-dd hh24:mi:ss')"
			return dbColumn + " <= to_date('" + time + "', 'yyyy-mm-dd hh24:mi:ss')";
		} else {
			// h.publish_date <= '"
			// + endTime
			// + "'"
			return dbColumn + " <= '" + time + "'";
		}
	}

	@Override
	public List<Object[]> getDirRulesInfo(String[] processMapIds, Long projectId, String endTime) {
		if (processMapIds == null || processMapIds.length == 0 || endTime == null || "".equals(endTime.trim())) {
			return new ArrayList();
		}

		// 查询详细信息
		String sql = "";
		sql = " with t as(select * from ( ";
		// 添加一个sql就为true，以后sql都要拼装union
		boolean flag = false;
		// 所有流程架构所属的流程
		for (int i = 0; i < processMapIds.length; i++) {
			String processMapId = processMapIds[i];
			if (processMapId == null || "".equals(processMapId.trim())) {
				continue;
			}
			if (flag) {
				sql += " union ";
			}
			sql += " select " + processMapId + " as map, tmp.*  from JECN_RULE_T P "
					+ " LEFT JOIN jecn_rule_t tmp ON TMP.T_PATH LIKE P.T_PATH" + JecnCommonSql.getConcatChar()
					+ "'%' WHERE p.ID=" + processMapId;
			flag = true;
		}
		sql += " )tmp2 where tmp2.is_dir <> 0 and tmp2.del_state = 0 and "
				+ lessEqualThan(" tmp2.create_date", endTime)
				+ " ),"
				+ " t1  as (select max(h.publish_date) as last_publish_date,t.id,t.map from t"
				+ "                 left join JECN_TASK_HISTORY_NEW h on t.id =h.RELATE_ID and h.Type in (2,3)"
				+ "                      and "
				+ lessEqualThan("h.publish_date", endTime)
				+ "                group by t.map, t.id ),"
				+ " t2 as (select case "
				+ "  WHEN x2.id is null then null"
				+ " when t1.last_publish_date is null then x2.pub_time else t1.last_publish_date end as last_publish_date,"
				+ "   t1.id,t1.map"
				+ "   from t1 left join jecn_rule x2 on x2.id=t1.id  "
				+ " and "
				+ lessEqualThan("x2.pub_time", endTime)

				// /////////

				+ "), t3 as("
				+ " select t2.last_publish_date,"
				+ "        t2.id,"
				+ "        t2.map,"
				+ "        (select min(publish_date) from"
				+ "        jecn_task_history_new jthn"
				+ "        where t2.id=jthn.relate_id and jthn.type in (2,3)"
				+ "        and publish_date>t2.last_publish_date) as true_next_publish_date"
				+ "   from t2"

				+ ")"

				// //////////

				+ "select jfs.id,"
				+ "              jfs.per_id,"
				+ "              jfs.rule_name,"
				+ "              jfs.rule_number,"
				+ "              t3.last_publish_date,"
				+ "              jt.version_id,"
				+ "              org.org_name,"
				+ "              org.org_id,"
				+ "              jfs.type_res_people,"
				+ "              jfs.res_people_id,"
				+ "              case when jfs.type_res_people = 0 then"
				+ "                 (select ju.true_name"
				+ "                    from jecn_user ju"
				+ "                   where ju.people_id = jfs.res_people_id)"
				+ "                when jfs.type_res_people = 1 then"
				+ "                 (select jfoi.figure_text"
				+ "                    from jecn_flow_org_image jfoi, jecn_flow_org jfo"
				+ "                   where jfoi.org_id = jfo.org_id"
				+ "                     and jfo.del_state = 0"
				+ "                     and jfoi.figure_id = jfs.res_people_id)"
				+ "              end as res_people,"
				+ "              jfst.id as p_id,"
				+ "              gju.people_id,"
				+ "              gju.TRUE_NAME,"
				+ "              jt.draft_person,"
				+ "              jt.expiry,"
				+ "              t3.map,"
				+ "              pubCount,"
				+ "              notPubCount,"
				+ "				 case "
				+ "     when"
				+ "     jt.expiry <> 0 and  t3.true_next_publish_date is not null"
				+ "     and "
				+ bigEqual("jt.next_scan_date", "t3.true_next_publish_date")
				+ "     then 1 "
				+ "     when  jt.expiry <> 0 and  t3.true_next_publish_date is not null and "
				+ small("jt.next_scan_date", "t3.true_next_publish_date")
				+ "     then 0 "
				+ "		when jt.expiry<>0 and"
				+ smallThanCurTime("jt.next_scan_date")
				+ " then"
				+ "		         0 "
				+ "		         else "
				+ "		         2   "
				+ "		       end as scan_ontime,"
				+ "       case when jt.expiry<>0 then"
				+ "       jt.next_scan_date   " // -------------
				+ "       end as next_scan_date,"
				+ "              org.org_id as true_org_id,"
				+ "              org.org_name as true_org_name"
				+ "         from t3"
				+ "         left join (select sum(case when t3.last_publish_date is not null then 1 else 0 end) as pubCount,"
				+ "                           sum(case when t3.last_publish_date is null then 1 else 0 end) as notPubCount,"
				+ "                           t3.map"
				+ "                    from t3 group by t3.map) count on t3.map = count.map"
				+ "         left join jecn_rule_t jfs on jfs.id = t3.id " + " left join jecn_task_history_new jt"
				+ " on jt.publish_date=t3.last_publish_date and jt.relate_id=t3.id and jt.type in (2,3)"
				+ " left join jecn_flow_org org on org.org_id = jfs.org_id"
				+ "         left join jecn_rule jfst on jfst.id = jfs.id"
				+ "         left join jecn_user gju on gju.people_id = jfs.Ward_People_Id"
				+ "        order by org.org_id,jfs.per_id,jfs.id asc";

		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getRulesDetailDutyOrgList(String dutyOrgIds, Long projectId, String endTime,
			boolean orgHasChild) {
		String sql = "";
		String orgSql = getDutyOrgSubSource(dutyOrgIds, 1, orgHasChild);

		sql = "	WITH org_flowid as" + " (SELECT DISTINCT jfst.id" + "  FROM jecn_rule_t jfst" + "   inner join "
				+ orgSql + " jfo ON jfo.rule_id=jfst.id"
				+ "   LEFT JOIN JECN_TASK_HISTORY_NEW h ON jfst.id = h.RELATE_ID" + "  AND h.Type in (2,3)" + "  AND "
				+ lessEqualThan("h.publish_date", endTime) + "  WHERE 1=1 and "
				+ lessEqualThan("jfst.create_date", endTime) + "    AND jfst.del_state = 0" + "    AND jfo.projectid="
				+ projectId + ")," + " MY_FLOW AS" + "  (";

		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql += "select jfst.* from jecn_rule_t jfst where jfst.del_state = 0"
					+ " CONNECT BY PRIOR  jfst.per_id=jfst.id" + " START WITH jfst.id  in (select id from org_flowid)";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql += "SELECT JFT.*" + "  FROM jecn_rule_t JFT" + "  WHERE JFT.id in (select id from org_flowid)"
					+ "   UNION ALL SELECT JFS.*" + "  FROM MY_FLOW"
					+ "  INNER JOIN jecn_rule_t JFS ON MY_FLOW.per_Id= JFS.Id";
		}

		sql += " )";
		sql += "select  jfst.id,jfst.rule_name,jfst.per_id,jfst.rule_number,jfst.is_dir from MY_FLOW jfst";

		return this.listNativeSql(sql);
	}

	@Override
	public List<Object[]> getRuleDetailDirList(String mIds, Long projectId) {
		String sql = "";

		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "select jfst.id,jfst.rule_name,jfst.per_id,jfst.rule_number,jfst.is_dir" + " from jecn_rule_t jfst"
					+ " where  jfst.del_state = 0" + " and project_Id=" + projectId
					+ " CONNECT BY  PRIOR  jfst.id=jfst.per_id" + " START WITH jfst.id in (" + mIds + ")" + " union "
					+ "select jfst.id,jfst.rule_name,jfst.per_id,jfst.rule_number,jfst.is_dir"
					+ " from jecn_rule_t jfst" + " where  jfst.del_state = 0" + " and project_Id=" + projectId
					+ " CONNECT BY  PRIOR  jfst.per_id=jfst.id" + " START WITH jfst.id in (" + mIds + ")";

		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {

			sql = " WITH MY_FLOW_N AS" // 查找子节点
					+ " (SELECT JFT.id,JFT.rule_name,JFT.per_id,JFT.rule_number,jft.is_dir"
					+ " FROM jecn_rule_t JFT"
					+ " WHERE JFT.id in ("
					+ mIds
					+ ")"
					+ " UNION ALL SELECT jfs.id,jfs.rule_name,jfs.per_id,jfs.rule_number,jfs.is_dir"
					+ " FROM MY_FLOW_N"
					+ " INNER JOIN jecn_rule_t JFS ON MY_FLOW_N.per_id= JFS.id"
					+ " where  JFS.del_state = 0"
					+ "	and JFS.project_Id="
					+ projectId
					+ " ),"
					+ " MY_FLOW_P AS" // 查找父节点
					+ "  (SELECT JFT.id,JFT.rule_name,JFT.per_id,JFT.rule_number,jft.is_dir"
					+ "   FROM jecn_rule_t JFT"
					+ "   WHERE JFT.id in ("
					+ mIds
					+ ")"
					+ "   UNION ALL SELECT jfs.id,jfs.rule_name,jfs.per_id,jfs.rule_number,jfs.is_dir"
					+ "   FROM MY_FLOW_P"
					+ "  INNER JOIN jecn_rule_t JFS ON MY_FLOW_P.id=JFS.per_id"
					+ " where  JFS.del_state = 0"
					+ "					 and JFS.project_Id="
					+ projectId
					+ ")"
					+ " SELECT id,rule_name,per_id,rule_number,is_dir FROM MY_FLOW_N"
					+ "	UNION "
					+ " SELECT id,rule_name,per_id,rule_number,is_dir FROM MY_FLOW_P";
		}

		return this.listNativeSql(sql);
	}

	@Override
	public List<JecnFlowInoutData> getFlowInoutsByType(int type, Long flowId, boolean isPub) {
		String t = isPub ? "" : "T";
		String _t = isPub ? "" : "_T";
		String hql = "from JecnFlowInout" + t + " where flowId=? and type=? order by sortId asc";
		List<? extends JecnFlowInoutBase> inouts = this.listHql(hql, flowId, type);
		String posSql = "";
		String groupSql = "";
		posSql = "select jfoi.figure_id, jfoi.figure_text" + "    from jecn_flow_in_out_pos" + _t + " jfi"
				+ "   inner join jecn_flow_org_image jfoi" + "      on jfi.r_id = jfoi.figure_id"
				+ "     and jfi.r_type = 0 and jfi.in_out_id=? order by jfoi.figure_id asc";
		groupSql = "select jpg.id, jpg.name" + "  from jecn_flow_in_out_pos" + _t + " jfi"
				+ " inner join jecn_position_group jpg" + "    on jfi.r_id = jpg.id"
				+ "   and jfi.r_type = 1 and jfi.in_out_id=? order by jpg.id asc";
		List<JecnFlowInoutData> result = new ArrayList<JecnFlowInoutData>();
		FullPathManager posManager = new FullPathManager(this, 6);
		FullPathManager groupManager = new FullPathManager(this, 7);
		for (JecnFlowInoutBase inout : inouts) {
			JecnFlowInoutData data = new JecnFlowInoutData();
			result.add(data);
			data.setInout(inout);
			List<JecnTreeBean> poses = getPosOrGroupTreeBeans(posSql, inout.getId());
			List<JecnTreeBean> groups = getPosOrGroupTreeBeans(groupSql, inout.getId());
			data.setPoses(poses);
			data.setPosGroups(groups);
			if (JecnUtil.isNotEmpty(poses)) {
				for (JecnTreeBean b : poses) {
					posManager.add(b.getId());
				}
			}
			if (JecnUtil.isNotEmpty(groups)) {
				for (JecnTreeBean b : groups) {
					groupManager.add(b.getId());
				}
			}
		}
		posManager.generate();
		groupManager.generate();
		for (JecnFlowInoutData inout : result) {
			List<JecnTreeBean> poses = inout.getPoses();
			List<JecnTreeBean> groups = inout.getPosGroups();
			if (JecnUtil.isNotEmpty(poses)) {
				for (JecnTreeBean b : poses) {
					b.setName(posManager.getFullPath(b.getId()));
				}
			}
			if (JecnUtil.isNotEmpty(groups)) {
				for (JecnTreeBean b : groups) {
					b.setName(groupManager.getFullPath(b.getId()));
				}
			}
		}

		return result;
	}

	private List<JecnTreeBean> getPosOrGroupTreeBeans(String sql, String inoutId) {
		List<Object[]> objs = this.listNativeSql(sql, inoutId);
		List<JecnTreeBean> treeBeans = new ArrayList<JecnTreeBean>();
		for (Object[] obj : objs) {
			JecnTreeBean treeBean = new JecnTreeBean();
			treeBean.setId(Long.valueOf(obj[0].toString()));
			treeBean.setName(obj[1].toString());
			treeBeans.add(treeBean);
		}
		return treeBeans;
	}

	@Override
	public List<Object[]> getChildFlowsT(List<Long> pids) throws Exception {
		String sql = "select distinct t.flow_id,t.flow_name,t.pre_flow_id,t.isflow,t.flow_id_input,"
				+ "       '0' is_pub," + "       0 count,t.t_path,t.sort_id,T.NODE_TYPE,"
				+ "       case when t.pub_time<>t.update_date then 1 else 0 end as isUpdate"
				+ "       from jecn_flow_structure_t t"
				+ "       left join jecn_flow_structure_t sub on sub.pre_flow_id = t.flow_id  and sub.del_state=0 "
				+ "       where t.del_state = 0 and t.pre_flow_id in " + JecnCommonSql.getIds(pids) + " AND t.DATA_TYPE=0 "
				+ " order by t.pre_flow_id,t.sort_id,t.flow_id";
		return this.listNativeSql(sql);
	}
}
