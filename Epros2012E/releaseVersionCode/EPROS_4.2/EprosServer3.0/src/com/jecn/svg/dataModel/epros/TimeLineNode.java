package com.jecn.svg.dataModel.epros;

public class TimeLineNode {

	private Long id;
	private Double value;
	/** 现状值 */
	private Double statusValue;
	private String number;
	private Long x;
	private Long y;
	private Long width;
	private Long height;
	/**是否连续的时间节点 */
	private boolean isContinuous;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Long getX() {
		return x;
	}

	public void setX(Long x) {
		this.x = x;
	}

	public Long getY() {
		return y;
	}

	public void setY(Long y) {
		this.y = y;
	}

	public Long getWidth() {
		return width;
	}

	public void setWidth(Long width) {
		this.width = width;
	}

	public Long getHeight() {
		return height;
	}

	public void setHeight(Long height) {
		this.height = height;
	}

	public Double getStatusValue() {
		return statusValue;
	}

	public void setStatusValue(Double statusValue) {
		this.statusValue = statusValue;
	}

	public boolean getIsContinuous() {
		return isContinuous;
	}

	public void setContinuous(boolean isContinuous) {
		this.isContinuous = isContinuous;
	}

}
