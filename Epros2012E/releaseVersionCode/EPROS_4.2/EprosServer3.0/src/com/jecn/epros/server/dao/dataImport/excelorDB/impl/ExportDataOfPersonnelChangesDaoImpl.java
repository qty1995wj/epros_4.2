package com.jecn.epros.server.dao.dataImport.excelorDB.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.dataImport.excelorDB.IExportDataOfPersonnelChangesDao;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncSqlConstantPersonnelChanges;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;

/**
 * 人员数据变更导出excel Dao层
 * 
 * @author zhangft 2013-5-29
 */
public class ExportDataOfPersonnelChangesDaoImpl extends AbsBaseDao<DeptBean, Long> implements IExportDataOfPersonnelChangesDao {

	private static final Logger log = Logger.getLogger(ExportDataOfPersonnelChangesDaoImpl.class);

	/**
	 * 删除岗位影响的岗位组 Object[] 1:关联岗位名称,0:岗位组名称，,2:岗位所属流程;3:是否发布 1:发布后
	 * 
	 */
	@Override
	public List<Object[]> getDeleteAffectedPositionGroupList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.DELETE_AFFECTED_POSITION_GROUP_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getDeleteUserFlowList() {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.DELETE_USER_FLOW_SQL);
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getDeleteUserImpactFileInfoList() {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.DELETE_USER_IMPACT_FILE_INFO_SQL);
		List<Object[]> objList = query.list();
		return objList;
	}

	/**
	 * 0是流程 ;1是文件; 2是标准 ;3制度；4：发布类型
	 */
	@Override
	public List<Object[]> getDeptAccessPermList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.DEPT_ACCESS_PERM_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getDutyDeptFileList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.DUTY_DEPT_FILE_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	/**
	 * --流程责任人 责任人类型(0：人员：1岗位;) 发布状态(发布前：0；发布后：1)
	 */
	@Override
	public List<Object[]> getFlowBasicInfoList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.FLOW_BASIC_INFO_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	/**
	 * 删除岗位，影响的岗位相关角色 Object[] 0:岗位名称；1：角色名称；2：流程名称；3：是否发布（标识1：已发布；0未发布）
	 * 
	 * @return List<Object[]>
	 */
	@Override
	public List<Object[]> getFlowRoleWithPositionList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.FLOW_ROLE_WITH_POSITION_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getPositionAccessPermList() {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.POSITION_ACCESS_PERM_SQL);
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getProcessTaskDeptList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.PROCESS_TASK_DEPT_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getAddDeptList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.ADD_DEPT_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getAddPosList() {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.ADD_POS_SQL);
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getAddUserList() {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.ADD_USER_SQL);
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getDeleteDeptList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.DELETE_DEPT_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getDeletePosList() {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.DELETE_POS_SQL);
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getDeleteUserList() {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.DELETE_USER_SQL);
		List<Object[]> objList = query.list();
		return objList;

	}

	@Override
	public List<Object[]> getUpdateDeptList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.UPDATE_DEPT_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getUpdatePosList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.UPDATE_POS_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getUpdateUserList() {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.UPDATE_USER_SQL);
		List<Object[]> objList = query.list();
		return objList;
	}

	@Override
	public List<Object[]> getDutyDeptRuleList(Long currProjectID) {
		Session session = this.getSession();
		Query query = session.createSQLQuery(SyncSqlConstantPersonnelChanges.DUTY_DEPT_RULE_SQL);
		// 设置参数
		query.setLong("PROJECTID", currProjectID);
		// 执行查询
		List<Object[]> objList = query.list();
		return objList;
	}

	/**
	 * 新增岗位组
	 * 
	 * @return
	 */
	@Override
	public List<Object[]> getAddPosGroupList() {
		String sql = "SELECT JPG.NUM, JPG.NUM, JPG.PARENT_NUM, JPG.IS_DIR" + "  FROM TMP_JECN_POS_GROUP JPG" + " WHERE JPG.PRO_FLAG = 0";
		return this.listNativeSql(sql);
	}

	/**
	 * 删除的岗位组
	 * 
	 * @return
	 */
	@Override
	public List<Object[]> getDeletePosGroupList() {
		String sql = "SELECT JPG.Code, JPG.NAME" + "  FROM JECN_POSITION_GROUP JPG" + " WHERE NOT EXISTS"
				+ " (SELECT 1 FROM TMP_JECN_POS_GROUP TG WHERE TG.NUM = JPG.CODE)" + "   AND JPG.DATA_TYPE = 1";
		return this.listNativeSql(sql);
	}

	/**
	 * 更新的岗位组
	 * 
	 * @return
	 */
	@Override
	public List<Object[]> getUpdatePosGroupList() {
		String sql =  "SELECT JPG.CODE, JPG.NAME, JPG.PARENT_CODE, TG.NUM, TG.NUM, TG.PARENT_NUM" +
			"  FROM JECN_POSITION_GROUP JPG" + 
			"  LEFT JOIN TMP_JECN_POS_GROUP TG" + 
			"    ON TG.NUM = JPG.CODE" + 
			"   AND TG.NAME <> JPG.NAME" + 
			"   AND TG.PRO_FLAG = 1" + 
			" WHERE JPG.DATA_TYPE = 1" + 
			"   AND TG.NUM IS NOT NULL" + 
			" UNION" + 
			" SELECT JPG.CODE, JPG.NAME, JPG.PARENT_CODE, TG.NUM, TG.NUM, TG.PARENT_NUM" + 
			"  FROM JECN_POSITION_GROUP JPG" + 
			"  LEFT JOIN TMP_JECN_POS_GROUP TG" + 
			"    ON TG.NUM = JPG.CODE" + 
			"   AND TG.PARENT_NUM <> JPG.PARENT_CODE" + 
			"   AND TG.PRO_FLAG = 1" + 
			" WHERE JPG.DATA_TYPE = 1" + 
			"   AND TG.NUM IS NOT NULL";
		return this.listNativeSql(sql);
	}
}
