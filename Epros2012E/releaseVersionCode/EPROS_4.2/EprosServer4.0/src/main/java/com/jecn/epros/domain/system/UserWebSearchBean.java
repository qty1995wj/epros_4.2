package com.jecn.epros.domain.system;

import java.util.List;

public class UserWebSearchBean {
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 真实姓名
     */
    private String trueName;
    /**
     * 所属部门
     */
    private long orgId = -1;
    /**
     * 所属部门名称
     */
    private String orgName;
    /**
     * 角色类型
     */
    private String roleType;
    /**
     * 岗位id
     */
    private long posId = -1;
    /**
     * 岗位名称
     */
    private String posName;
    /**
     * 项目ID
     */
    private Long projectId;

    /**
     * 科大讯飞 登录名
     */
    private List<String> ifLoginName;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public long getOrgId() {
        return orgId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public long getPosId() {
        return posId;
    }

    public void setPosId(long posId) {
        this.posId = posId;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public List<String> getIfLoginName() {
        return ifLoginName;
    }

    public void setIfLoginName(List<String> ifLoginName) {
        this.ifLoginName = ifLoginName;
    }
}
