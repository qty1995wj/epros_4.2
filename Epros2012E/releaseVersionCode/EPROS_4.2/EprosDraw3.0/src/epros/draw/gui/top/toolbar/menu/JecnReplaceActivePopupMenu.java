package epros.draw.gui.top.toolbar.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.gui.workflow.util.JecnRemoveFlowElementUnit;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 排列 右键菜单( 自动对齐、决策、活动、信息系统、IT决策、垂直居中、底端对齐)
 * 
 * @author ZXH
 * @date： 日期：12 30, 2017 时间：11:28:56 AM
 */
public class JecnReplaceActivePopupMenu extends JMenu implements ActionListener {
	/** 决策 */
	private JMenuItem rhombusMenu;
	/** 活动 */
	private JMenuItem roundRectWithLineMenu;
	/** 信息系统 */
	private JMenuItem dataImageMenu;
	/** IT决策 */
	private JMenuItem iTRhombusMenu;

	/** 评审 */
	private JMenuItem expertRhombusARMenu;

	/** 接口 (活动) */
	private JMenuItem implFileMenu;
	/** 子流程 (活动) */
	private JMenuItem subFlowFigureMenu;

	/** 接口 */
	private JMenuItem implMenu;
	/** 子流程 */
	private JMenuItem ovalSubFlowMenu;

	/** 接口 六边形 */
	private JMenuItem interfaceRightHexagonMenu;

	public JecnReplaceActivePopupMenu() {
		// JecnResourceUtil.getJecnResourceUtil().getValue("activityReplacement")
		this.setText(JecnResourceUtil.getJecnResourceUtil().getValue("elementReplacement"));// 活动置换
		this.setIcon(JecnFileUtil.getReplaceImageIcon("replaceEle"));
		// 决策
		rhombusMenu = new JMenuItem();
		rhombusMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("decision"));// 决策
		// 添加事件监听
		rhombusMenu.addActionListener(this);
		rhombusMenu.setName(MapElemType.Rhombus.toString());
		rhombusMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.Rhombus.toString()));
		// 活动
		roundRectWithLineMenu = new JMenuItem();
		roundRectWithLineMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("activity"));// 活动
		// 添加事件监听
		roundRectWithLineMenu.addActionListener(this);
		roundRectWithLineMenu.setName(MapElemType.RoundRectWithLine.toString());
		roundRectWithLineMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.RoundRectWithLine.toString()));

		// 信息系统
		dataImageMenu = new JMenuItem();
		dataImageMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("itSystem"));// 信息系统
		// 添加事件监听
		dataImageMenu.addActionListener(this);
		dataImageMenu.setName(MapElemType.DataImage.toString());
		dataImageMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.DataImage.toString()));

		// IT决策
		iTRhombusMenu = new JMenuItem();
		iTRhombusMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("itDecision"));// IT
																								// 决策
		// 添加事件监听
		iTRhombusMenu.addActionListener(this);
		iTRhombusMenu.setName(MapElemType.ITRhombus.toString());
		iTRhombusMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.ITRhombus.toString()));

		expertRhombusARMenu = new JMenuItem();
		expertRhombusARMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("ExpertRhombusARNote"));// 评审
		// 添加事件监听
		expertRhombusARMenu.addActionListener(this);
		expertRhombusARMenu.setName(MapElemType.ExpertRhombusAR.toString());
		expertRhombusARMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.ExpertRhombusAR.toString()));

		// 接口
		implMenu = new JMenuItem();
		implMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("ImplFigure"));
		// 添加事件监听
		implMenu.addActionListener(this);
		implMenu.setName(MapElemType.ImplFigure.toString());
		implMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.ImplFigure.toString()));

		// 子流程
		ovalSubFlowMenu = new JMenuItem();
		ovalSubFlowMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("OvalSubFlowFigure"));
		// 添加事件监听
		ovalSubFlowMenu.addActionListener(this);
		ovalSubFlowMenu.setName(MapElemType.OvalSubFlowFigure.toString());
		ovalSubFlowMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.OvalSubFlowFigure.toString()));

		// 接口 (活动)
		implFileMenu = new JMenuItem();
		implFileMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("ActivityImplFigure"));
		// 添加事件监听
		implFileMenu.addActionListener(this);
		implFileMenu.setName(MapElemType.ActivityImplFigure.toString());
		implFileMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.ActivityImplFigure.toString()));

		// 子流程(活动)
		subFlowFigureMenu = new JMenuItem();
		subFlowFigureMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("ActivityOvalSubFlowFigure"));
		// 添加事件监听
		subFlowFigureMenu.addActionListener(this);
		subFlowFigureMenu.setName(MapElemType.ActivityOvalSubFlowFigure.toString());
		subFlowFigureMenu.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.ActivityOvalSubFlowFigure.toString()));

		// 接口 六边形
		interfaceRightHexagonMenu = new JMenuItem();
		interfaceRightHexagonMenu.setText(JecnResourceUtil.getJecnResourceUtil().getValue("InterfaceRightHexagon"));
		// 添加事件监听
		interfaceRightHexagonMenu.addActionListener(this);
		interfaceRightHexagonMenu.setName(MapElemType.InterfaceRightHexagon.toString());
		interfaceRightHexagonMenu.setIcon(JecnFileUtil
				.getReplaceImageIcon(MapElemType.InterfaceRightHexagon.toString()));

		// 流程图显示节点
		List<MapElemType> showTypeList = JecnDesignerProcess.getDesignerProcess().getShowElementsType(0);
		if (showTypeList.contains(MapElemType.RoundRectWithLine)) {
			this.add(roundRectWithLineMenu);
		}
		if (showTypeList.contains(MapElemType.Rhombus)) {
			this.add(rhombusMenu);
		}
		if (showTypeList.contains(MapElemType.ITRhombus)) {
			this.add(iTRhombusMenu);
		}
		if (showTypeList.contains(MapElemType.ExpertRhombusAR)) {
			this.add(expertRhombusARMenu);
		}
		if (showTypeList.contains(MapElemType.DataImage)) {
			this.add(dataImageMenu);
		}
		if (showTypeList.contains(MapElemType.ImplFigure)) {
			this.add(implMenu);
		}
		if (showTypeList.contains(MapElemType.OvalSubFlowFigure)) {
			this.add(ovalSubFlowMenu);
		}
		if (showTypeList.contains(MapElemType.ActivityImplFigure)) {
			this.add(implFileMenu);
		}
		if (showTypeList.contains(MapElemType.ActivityOvalSubFlowFigure)) {
			this.add(subFlowFigureMenu);
		}
		if (showTypeList.contains(MapElemType.InterfaceRightHexagon)) {
			this.add(interfaceRightHexagonMenu);
		}
	}

	public void actionPerformed(ActionEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		List<JecnBaseFlowElementPanel> jecnBaseFlowElementPanel = desktopPane.getCurrentSelectElement();
		for (JecnBaseFlowElementPanel baseBean : jecnBaseFlowElementPanel) {
			MapElemType mapElemType = baseBean.getFlowElementData().getMapElemType();
			if (mapElemType != MapElemType.ActiveFigureAR && mapElemType != MapElemType.RoundRectWithLine
					&& mapElemType != MapElemType.Rhombus && mapElemType != MapElemType.DataImage
					&& mapElemType != MapElemType.ITRhombus && mapElemType != MapElemType.ActivityImplFigure
					&& mapElemType != MapElemType.ActivityOvalSubFlowFigure
					&& mapElemType != MapElemType.OvalSubFlowFigure && mapElemType != MapElemType.ImplFigure
					&& mapElemType != MapElemType.InterfaceRightHexagon) {
				JecnOptionPane.showMessageDialog(null, JecnResourceUtil.getJecnResourceUtil().getValue(mapElemType)
						+ " " + JecnResourceUtil.getJecnResourceUtil().getValue("nonReplaceable"));
				return;
			}
		}

		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		undoData.recodeFlowElement(jecnBaseFlowElementPanel);
		List<JecnBaseFlowElementPanel> jecnBaseOldFlowElementPanel = new ArrayList<JecnBaseFlowElementPanel>();

		if (e.getSource() instanceof JMenuItem) {
			// 重画图形相关的连接线
			List<JecnBaseFigurePanel> figureList = new ArrayList<JecnBaseFigurePanel>();
			for (JecnBaseFlowElementPanel selectFigurebean : jecnBaseFlowElementPanel) {
				// 获取JMenuItem
				JMenuItem menuItem = (JMenuItem) e.getSource();
				String menuName = menuItem.getName();
				// 创建活动
				JecnBaseFigurePanel newFigure = (JecnBaseFigurePanel) JecnCreateFlowElement
						.createFlowElementObj(MapElemType.valueOf(menuName));

				JecnBaseFigurePanel selectFigure = (JecnBaseFigurePanel) selectFigurebean;
				jecnBaseOldFlowElementPanel.add(newFigure);

				JecnRemoveFlowElementUnit.removeReplaceFigure(selectFigure);

				// 复制活动属性
				newFigure.getFlowElementData().setFigureArrts(selectFigure.getFlowElementData(),
						newFigure.getFlowElementData());

				JecnAddFlowElementUnit.addFigure(selectFigure.getLocation(), newFigure);

				// 设置连接线 开始图形和结束图形
				JecnReplaceProcessMapPopupMenu.repalceLineActive(selectFigure, newFigure);

				figureList.add(newFigure);
				JecnDesignerProcess.getDesignerProcess().addElemLink(newFigure, null);
			}
			JecnDraggedFiguresPaintLine.paintLineByFigures(figureList);
			// 清空虚拟图形
			desktopPane.updateUI();
			redoData.recodeFlowElement(jecnBaseOldFlowElementPanel);
			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createDeleteAndAdd(undoData, redoData);
			desktopPane.getUndoRedoManager().addEdit(unredoProcess);
		}

	}
}
