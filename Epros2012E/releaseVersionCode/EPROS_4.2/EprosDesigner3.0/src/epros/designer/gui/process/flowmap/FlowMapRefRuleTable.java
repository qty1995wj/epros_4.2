package epros.designer.gui.process.flowmap;

import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/***
 * 流程地图与制度关联表格
 * 2013-11-27
 *
 */
public class FlowMapRefRuleTable extends JTable{

	private Vector<Vector<Object>> listObj = new Vector<Vector<Object>>();
	public FlowMapRefRuleTable(Vector<Vector<Object>> listObj){
		this.listObj = listObj;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		//设置第一列宽度为0
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		TableColumnModel columnModel = this.getColumnModel();
		columnModel.getColumn(4).setMaxWidth(100);
		columnModel.getColumn(4).setMinWidth(100);
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		//鼠标移动到操作列形状变小手，移出变为默认形状
	}
	

	/** 选中table 当前行和列 */
	private int myRow = -10;

	private int myColumn = -10;
	
	/**
	 * 鼠标进入表格监听
	 * 
	 * @param e
	 */
	protected void mouseMovedProcess(MouseEvent e) {
		int row = this.rowAtPoint(e.getPoint());
		int column = this.columnAtPoint(e.getPoint());
		if (myRow == row && column == myColumn) {// 始终在指定的单元格移动，不执行重绘
			return;
		}
		myRow = this.rowAtPoint(e.getPoint());
		myColumn = this.columnAtPoint(e.getPoint());

		this.revalidate();
		this.repaint();
	}
	public FlowMapRefRuleTableModel getTableModel() {
		Vector<Object> title = new Vector<Object>();
		title.add("ruleid");
		title.add(JecnProperties.getValue("ruleName"));
		title.add("ruleFileID");
		title.add("ruleType");
		title.add(JecnProperties.getValue("operational"));
		return new FlowMapRefRuleTableModel(title,getContent(listObj));
	}
	protected Vector<Vector<Object>> getContent(Vector<Vector<Object>> list){
		return null;
	}
	class FlowMapRefRuleTableModel extends DefaultTableModel {
		public FlowMapRefRuleTableModel(Vector<Object> title,
				Vector<Vector<Object>> content) {
			this.setDataVector(content, title);
		}

		public void remoeAll() {
			// 清空
			for (int index = this.getRowCount() - 1; index >= 0; index--) {
				this.removeRow(index);
			}
		}
		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}
	public int getMyRow() {
		return myRow;
	}
	public void setMyRow(int myRow) {
		this.myRow = myRow;
	}
	public int getMyColumn() {
		return myColumn;
	}
	public void setMyColumn(int myColumn) {
		this.myColumn = myColumn;
	}
	public void remoeAll() {
		// 清空
		for (int index = this.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) this.getModel()).removeRow(index);
		}
	}
	public int[] gethiddenCols() {
		return new int[] {0,2,3};//0 
	}

	public boolean isSelectMutil() {
		return true;
	}
}
