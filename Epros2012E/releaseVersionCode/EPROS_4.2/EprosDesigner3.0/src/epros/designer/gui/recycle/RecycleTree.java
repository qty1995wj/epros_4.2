package epros.designer.gui.recycle;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreePath;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnRecycleMenu;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;

public abstract class RecycleTree extends JecnHighEfficiencyTree {

	private RecyclePanel recyclePanel;

	public RecyclePanel getRecyclePanel() {
		return recyclePanel;
	}

	public void setRecyclePanel(RecyclePanel recyclePanel) {
		this.recyclePanel = recyclePanel;
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	/**
	 * 获取对应的Menu菜单类
	 * 
	 * @return
	 */
	protected abstract JecnRecycleMenu getRecycleMenu();

	/**
	 * @author zhangchen Aug 1, 2012
	 * @description：判断多个树节点有没有权限
	 * @param nodeList
	 *            节点集合
	 * @return
	 */
	private boolean isAuthNodes(List<JecnTreeNode> nodeList) {
		if (nodeList != null) {
			for (JecnTreeNode node : nodeList) {
				int authType = JecnTreeCommon.isAuthNode(node);
				if (authType == 0) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/***************************************************************************
	 * 文件回收站 ：点击树节点的操作
	 * 
	 * @param evt
	 * @param jTree
	 * @param jecnManageDialog
	 */
	public void jTreeMousePressed(MouseEvent evt) {
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = this.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				// 最后选中的节点
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick.getLastPathComponent();
				// 选中节点的类型
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean().getTreeNodeType();
				// 收集选中节点的集合
				List<JecnTreeNode> listSelectNode = new ArrayList<JecnTreeNode>();
				// 收集选中节点路径的集合
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
						if (treeNodeType.equals(nodeSelect.getJecnTreeBean().getTreeNodeType())) {
							// 有没有删除和恢复的操作的权限
							// if(JecnTreeCommon.isAuthNode(nodeSelect)==2){
							listSelectNode.add(nodeSelect);
							listTreePaths.add(treePath);
							// }
						}
					}
				}
				if (listSelectNode.size() > 0) {
					// 获取树右键菜单
					JecnRecycleMenu menu = getRecycleMenu();

					if (listSelectNode.size() == 1) {
						this.setSelectionPath(listTreePaths.get(0));
						if (isAuthNodes(listSelectNode)) {
							menu.setSelectedNodes(listSelectNode);
							menu.displayMenus();
							menu.show(evt.getComponent(), evt.getX(), evt.getY());
						}
						return;
					}
					// 去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon.repeatChildNodes(listSelectNode, this);
					// 重新选中需要选中的节点，去掉子节点选中的状态
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
								if (node.getJecnTreeBean().getId().equals(nodeSelect.getJecnTreeBean().getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}
						if (treePaths.length == 1) {
							this.setSelectionPath(treePaths[0]);
						} else {
							this.setSelectionPaths(treePaths);
						}
						if (isAuthNodes(listRemoveChild)) {
							menu.setSelectedNodes(listSelectNode);
							menu.displayMenus();
							menu.show(evt.getComponent(), evt.getX(), evt.getY());
						}
					}
				}

			}
		}
	}
}
