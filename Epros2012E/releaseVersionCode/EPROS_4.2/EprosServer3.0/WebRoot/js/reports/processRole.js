function processRoleGrid() {
	processRoleGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
						
					}
				},
				baseParams:{
						processId : document.getElementById("processId_id").value,
						typeNumber : document.getElementById("typeNumber").value
				},
				proxy : new Ext.data.HttpProxy({
							url : "processRoleDetails.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "flowId",
									mapping : "flowId"
								}, {
									name : "flowName",
									mapping : "flowName"
								}, {
									name : "flowIdInput",
									mapping : "flowIdInput"
								}, {
									name : "resPeopleName",
									mapping : "resPeopleName"
								}, {
									name : "orgName",
									mapping : "orgName"
								}])

			});

	processRoleGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processRoleNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			}

	]);
	processRoleGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processRoleGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processRoleGridPanel = new Ext.grid.GridPanel({
				id : "processRoleGridPanel_id",
				renderTo : 'processRoleGrid_id',
				height :  document.documentElement.clientHeight-20,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processRoleGridStore,
				colModel : processRoleGridColumnModel,
				bbar : processRoleGridBbar
			})
}


// 加载流程角色数统计
function processRolePieCharts(){
	$("#sysGrid_id").children().remove();
	$("#sysGrid_id").load(
	 'processRole.action',{"processId":document.getElementById("processId_id").value},
	  function(){
		setReportsDivWidthHeight('sysGrid_id');
	});
}

// 流程角色重置
function processRoleReset(){
		document.getElementById("processName_id").value = "";
		document.getElementById("processId_id").value = "-1";
}


/**
 * 流程名称增加超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function processRoleNameRenderer(value, cellmeta, record){
	 return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['flowId']+"' >"+value+"</a>";
}

/**
 * 流程角色数详情链接
 **/
function findProcessRoleSub(num){
   var processId = document.getElementById("processId_id").value;
   window.open(basePath+"processRoleDetailsLink.action?processId="+processId+"&typeNumber="+num)
}

// 流程角色下载
function processRoleDownloadExcel(){
    //流程ID
	 var processId=document.getElementById("processId_id").value;
	 window.location.href="downloadProcessRole.action?processId="+processId;
}