package com.jecn.search.service;

import com.jecn.search.bean.JecnSearchResultBean;

public interface IJecnKeyWordSearchService {

	JecnSearchResultBean keyWordSearch(String searchName, int relatedType, int start, int pageSize) throws Exception;

	JecnSearchResultBean keyWordSearchTotal(String searchName, int relatedType) throws Exception;

}
