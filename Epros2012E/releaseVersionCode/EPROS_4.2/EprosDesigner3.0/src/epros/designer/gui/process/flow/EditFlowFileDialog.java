package epros.designer.gui.process.flow;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessFileNodeData;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnUtil;
import epros.draw.util.DrawCommon;

/*******************************************************************************
 * 流程文件
 * 
 * @author 2012-07-04
 * 
 */
public class EditFlowFileDialog extends BaseFlowFileDialog {
	public EditFlowFileDialog(JecnTreeNode selectNode) {
		super(selectNode, false);
	}

	protected void okButton() {
		TreeNodeType nodeType = selectNode.getJecnTreeBean().getTreeNodeType();
		if (nodeType == TreeNodeType.processFile && !isUpdate()) {// 没有更新
			this.dispose();
			return;
		}

		String errorText = isValidate();
		if (!DrawCommon.isNullOrEmtry(errorText)) {
			verfyLab.setText(errorText);
			return;
		}

		ProcessFileNodeData processFileData = new ProcessFileNodeData();
		try {
			commonProcessFileData(processFileData);

			JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(id);
			flowStructureT.setUpdatePeopleId(JecnUtil.getUserId());
			flowStructureT.setFlowName(filePropertyPanel.getFlowNameField().getText());
			flowStructureT.setFlowIdInput(processFileData.getInfoBean().getFlowNumber());
			processFileData.setFlowStructureT(flowStructureT);
			processFileData.getInfoBean().setUpdatePeopleId(JecnUtil.getUserId());
			ConnectionPool.getProcessAction().updateProcessFile(processFileData);

			this.selectNode.getJecnTreeBean().setNumberId(flowStructureT.getFlowIdInput());
			JecnTreeCommon.reNameNode(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), selectNode,
					filePropertyPanel.getFlowNameField().getText());
			JecnTreeCommon.flushCurrentTreeNodeToUpdate(selectNode);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("EditFlowFileDialog okButton is error！", e);
		}
		this.dispose();
	}

	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.dispose();
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButton();
			} else if (optionTig == 1) {
				this.dispose();
			}
		}
	}
}
