package epros.designer.gui.process.guide;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/***
 * 添加流程类别
 * 
 * @author 2012-07-04
 * 
 */
public class AddProcessTypeDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(AddProcessTypeDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(450, 300);

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel(430, 225);

	/** 滚动面板 */
	private JScrollPane processScrollPane = new JScrollPane();

	private ProcessTable processTable = null;

	/** 操作按钮面板 */
	private JecnPanel operaButPanel = new JecnPanel(420, 30);

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel(440, 30);

	/** 添加按钮 */
	private JButton addBut = new JButton(JecnProperties.getValue("addBtn"));

	/** 编辑 */
	private JButton editBut = new JButton(JecnProperties.getValue("editBtn"));

	/** 删除 */
	private JButton deleteBut = new JButton(JecnProperties
			.getValue("deleteBtn"));

	/** 关闭 */
	private JButton closeBut = new JButton(JecnProperties.getValue("closeBtn"));

	/** 设置大小 */
	Dimension dimension = null;

	protected List<ProceeRuleTypeBean> listTypeBean = new ArrayList<ProceeRuleTypeBean>();
	
	public AddProcessTypeDialog(List<ProceeRuleTypeBean> listTypeBean) {
		this.listTypeBean = listTypeBean;
		this.setSize(450, 300);
		this.setTitle(JecnProperties.getValue("typeDif"));
		this.setResizable(false);
		this.setModal(true);
		this.setLocationRelativeTo(null);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		processScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		operaButPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 流程滚动面板大小
		dimension = new Dimension(410, 180);
		processScrollPane.setPreferredSize(dimension);
		processScrollPane.setMaximumSize(dimension);
		processScrollPane.setMinimumSize(dimension);
		processScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		addBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				addButPerformed();
			}

		});

		editBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				editButPerformed();
			}

		});

		deleteBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}

		});

		closeBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}

		});

		initLayout();
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 类别
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(processScrollPane, c);
		// processScrollPane
		processTable = new ProcessTable();
		processScrollPane.setViewportView(processTable);

		// 操作按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(operaButPanel, c);
		operaButPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		operaButPanel.add(addBut);
		operaButPanel.add(editBut);
		operaButPanel.add(deleteBut);
		// 按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(closeBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 添加
	 */
	private void addButPerformed() {
		AddTypeDialog addTypeDialog = new AddTypeDialog();
		addTypeDialog.setVisible(true);
		if (addTypeDialog.isOperation) {
			Vector<String> v = new Vector<String>();
			v.add(String.valueOf(addTypeDialog.getTypeId()));
			v.add(addTypeDialog.getName());
			((DefaultTableModel) processTable.getModel()).addRow(v);
//				controlTargetArea.setText("");
//			processTable = new ProcessTable();
//			processScrollPane.setViewportView(processTable);
		}
	}

	/**
	 * 编辑
	 */
	@SuppressWarnings("unchecked")
	private void editButPerformed() {
		int selectRows = processTable.getSelectedRow();
		if (selectRows == -1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pleaseChooseEditType"));
			return;
		}
		Vector<Vector<String>> vectors = ((DefaultTableModel) processTable
				.getModel()).getDataVector();
		String typeName = null;
		Long typeId = null;
		for (int j = 0; j < vectors.size(); j++) {
			Vector<String> vector = vectors.get(selectRows);
			typeId = Long.valueOf(vector.get(0));
			typeName = vector.get(1);
		}
		ProceeRuleTypeBean ruleTypeBean = new ProceeRuleTypeBean();
		ruleTypeBean.setTypeId(typeId);
		ruleTypeBean.setTypeName(typeName);
		Long ruletypeId =Long.valueOf(processTable.getModel().getValueAt(selectRows, 0).toString());
		
		EditProcessTypeDialog editProcessTypeDialog = new EditProcessTypeDialog(ruletypeId,ruleTypeBean);
		editProcessTypeDialog.setVisible(true);
		if (editProcessTypeDialog.isOperation) {
//			processTable = new ProcessTable();
//			processScrollPane.setViewportView(processTable);
			processTable.getModel().setValueAt(editProcessTypeDialog.getName(),
					processTable.getSelectedRow(), 1);
		}
	}

	/**
	 * 删除
	 */
	private void deleteButPerformed() {
		int[] selectRows = processTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pleaseChooseDeleteType"));
			return;
		}
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		Vector<Vector<String>> vectors = ((DefaultTableModel) processTable
				.getModel()).getDataVector();
		List<Long> kpiIds = new ArrayList<Long>();
		for (int i = 0; i < selectRows.length; i++) {
			Vector<String> vector = vectors.get(selectRows[i]);
			kpiIds.add(Long.valueOf(vector.get(0)));
		}
		// 删除数据库中的流程类别数据
		try {
			ConnectionPool.getProcessRuleTypeAction().delteFlowArributeType(
					kpiIds);
		} catch (Exception e) {
			log.error("AddProcessTypeDialog deleteButPerformed is error",e);
		}

		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) processTable.getModel())
					.removeRow(selectRows[i]);
		}
	}

	/**
	 * 取消
	 */
	private void cancelButPerformed() {
		this.dispose();
	}

	class ProcessTable extends JTable {

		ProcessTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		}

		public Vector<Vector<String>> getContent() {
			Vector<Vector<String>> vs = new Vector<Vector<String>>();
			// 获取流程KPI值数据
			// 获取类型数据
			try {
				listTypeBean = ConnectionPool.getProcessRuleTypeAction()
						.getFlowAttributeType();
				for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
					Vector<String> row = new Vector<String>();
					row.add(String.valueOf(proceeRuleTypeBean.getTypeId()));
					row.add(proceeRuleTypeBean.getTypeName());
					vs.add(row);
				}
			} catch (Exception e1) {
				log.error("AddProcessTypeDialog ProcessTable is error",e1);
			}
			return vs;
		}

		public ProcessTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			// title.add("类别编号");
			title.add(JecnProperties.getValue("typeName"));
			return new ProcessTableMode(getContent(), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {

			return new int[] { 0 };
		}

		class ProcessTableMode extends DefaultTableModel {
			public ProcessTableMode(Vector<Vector<String>> data,
					Vector<String> title) {
				super(data, title);
			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return false;
			}
		}
	}
}
