package epros.draw.gui.workflow.tabbedPane;

import java.awt.Component;
import java.util.List;

import javax.swing.JPanel;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnCloneablePanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 角色移动实现类
 * 
 * @author fuzhh
 * 
 */
public class JecnRoleMobile {
	/** 角色移动 移动面板 */
	private JPanel roleMove = null;
	/** 面板 */
	private static JecnDrawDesktopPane workflow = null;

	public JecnRoleMobile(JecnDrawDesktopPane workflow) {
		// 获取面板
		this.workflow = workflow;
		// 初始化组件
		initComponent();
		// 设置位置点和大小
		initRoleBounds();
		// 添加角色图形到移动面板上
		reDrawFigure();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponent() {
		// 角色移动 移动面板
		roleMove = new JPanel();
		// 无边框
		roleMove.setBorder(null);
		// 无布局
		roleMove.setLayout(null);
		// 背景颜色
		roleMove.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	/**
	 * 
	 * 获取第一条横分割线（靠近原点的横分割线）
	 * 
	 */
	public static JecnBaseVHLinePanel getFristHLine(JecnDrawDesktopPane workflow) {

		// 第一条横竖分割线（靠近原点的分割线）
		JecnBaseVHLinePanel fristHLine = null;

		// 画图面板上所有横竖分割线
		List<JecnBaseVHLinePanel> vhLinePanelList = workflow.getVhLinePanelList();

		for (int i = 0; i < vhLinePanelList.size(); i++) {
			JecnBaseVHLinePanel vhLine = vhLinePanelList.get(i);

			if (LineDirectionEnum.horizontal != vhLine.getLineDirectionEnum()) {// 不是横分割线
				continue;
			}

			if (fristHLine == null) {
				fristHLine = vhLine;
			} else {
				if (fristHLine.getY() > vhLine.getY()) {
					fristHLine = vhLine;
				}
			}
		}

		return fristHLine;
	}

	/**
	 * 
	 * 获取第一条竖分割线（靠近原点的分割线）
	 * 
	 */
	public static JecnBaseVHLinePanel getFristVLine(JecnDrawDesktopPane workflow) {

		// 第一条横竖分割线（靠近原点的分割线）
		JecnBaseVHLinePanel fristVLine = null;

		// 画图面板上所有横竖分割线
		List<JecnBaseVHLinePanel> vhLinePanelList = workflow.getVhLinePanelList();

		for (int i = 0; i < vhLinePanelList.size(); i++) {
			JecnBaseVHLinePanel vhLine = vhLinePanelList.get(i);

			if (LineDirectionEnum.vertical != vhLine.getLineDirectionEnum()) {// 不是竖分割线
				continue;
			}

			if (fristVLine == null) {
				fristVLine = vhLine;
			} else {
				if (fristVLine.getX() > vhLine.getX()) {
					fristVLine = vhLine;
				}
			}
		}
		return fristVLine;
	}

	/**
	 * 
	 * 设置位置点和大小
	 * 
	 */
	private void initRoleBounds() {
		// 角色移动面板的宽
		int vhWidth = 150;
		// 角色移动面板的高
		int vhHeight = 150;
		// 位置点坐标
		int x = 0;
		int y = 0;

		// 保证画图面板大小大于0
		if (workflow.getWidth() == 0 || workflow.getHeight() == 0) {
			workflow.setSize(JecnUIUtil.getWorkflowSize());
		}

		if (workflow.getFlowMapData().isHFlag()) {// 横向
			// 第一条横竖分割线（靠近原点的分割线）
			JecnBaseVHLinePanel fristVLine = getFristVLine(workflow);

			if (fristVLine != null) {// 有竖分割线
				// 宽
				vhWidth = DrawCommon.convertDoubleToInt(fristVLine.getX());
				// 高=画图面板高（当前比例下）
				vhHeight = workflow.getHeight();

				// 位置点X坐标:
				x = workflow.getScrollPanle().getHorizontalScrollBar().getValue();

			} else {// 没有竖分割线

				// 宽
				vhWidth = DrawCommon.convertDoubleToInt(vhWidth * workflow.getWorkflowScale());
				// 高=画图面板高（当前比例下）
				vhHeight = workflow.getHeight();

				// 位置点X坐标:
				x = workflow.getScrollPanle().getHorizontalScrollBar().getValue();
			}

		} else {// 竖向

			// 第一条横竖分割线（靠近原点的分割线）
			JecnBaseVHLinePanel fristHLine = getFristHLine(workflow);

			if (fristHLine != null) {// 有横分割线

				// 宽=画图面板宽（当前比例下）
				vhWidth = workflow.getWidth();
				// 高
				vhHeight = DrawCommon.convertDoubleToInt(fristHLine.getY());
				// 位置点Y坐标
				y = workflow.getScrollPanle().getVerticalScrollBar().getValue();

			} else {// 没有横分割线

				// 宽=画图面板宽（当前比例下）
				vhWidth = workflow.getWidth();
				// 高
				vhHeight = DrawCommon.convertDoubleToInt(vhHeight * workflow.getWorkflowScale());
				// 位置点Y坐标
				y = workflow.getScrollPanle().getVerticalScrollBar().getValue();
			}

		}

		// 设置位置点和大小
		roleMove.setBounds(x, y, vhWidth, vhHeight);
	}

	/**
	 * 
	 * 添加角色图形到移动面板上
	 * 
	 */
	private void reDrawFigure() {
		for (JecnBaseFlowElementPanel baseFlowElement : workflow.getPanelList()) {
			if (!DrawCommon.isRole(baseFlowElement.getFlowElementData().getMapElemType())) {
				continue;
			}
			// 当前待添加到角色移动的图形
			if (baseFlowElement instanceof JecnBaseFigurePanel) {
				// 获取克隆值
				JecnBaseFigurePanel baseFigurePanel = (JecnBaseFigurePanel) baseFlowElement.clone();
				// 设置大小位置点
				baseFigurePanel.setSizeAndLocation(baseFlowElement.getLocation());
				// 清除图形事件
				baseFigurePanel.removeFlowElemPanelEvent();
				// 添加到panel
				roleMove.add(baseFigurePanel);

				if (JecnSystemStaticData.isEprosDesigner() && baseFigurePanel instanceof JecnBaseRoleFigure) {
					// 设计器用户退出
					JecnDesignerProcess.getDesignerProcess().addLinkToroleMobile((JecnBaseRoleFigure) baseFigurePanel);
				}
			}
		}

		for (Component c : workflow.getComponents()) {
			if (c instanceof JecnCloneablePanel) {
				JecnCloneablePanel source = (JecnCloneablePanel) c;
				if (checkContainsRoleMove(source)) {
					JecnCloneablePanel clone = (JecnCloneablePanel) source.clone();
					roleMove.add(clone);
				}
			}
		}
	}

	/**
	 * 
	 * 是否在角色移动区域
	 * 
	 * @param jecnPanel
	 *            JecnBaseFlowElementPanel
	 * @return boolean 在返回true；不在返回false
	 */
	private boolean checkContainsRoleMove(JecnPanel jecnPanel) {
		if (workflow.getFlowMapData().isHFlag()) {// 横向
			return (roleMove.getWidth() > jecnPanel.getX() + jecnPanel.getWidth()) ? true : false;
		} else {// 竖向
			return (roleMove.getHeight() > jecnPanel.getY() + jecnPanel.getHeight()) ? true : false;
		}
	}

	public JPanel getRoleMove() {
		return roleMove;
	}
}
