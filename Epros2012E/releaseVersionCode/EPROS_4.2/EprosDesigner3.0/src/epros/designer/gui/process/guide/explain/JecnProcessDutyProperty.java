package epros.designer.gui.process.guide.explain;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.process.ProcessAttributeBean;

import epros.designer.gui.process.guide.ProcessPropertyDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;

/**
 * 流程责任属性
 * 
 * @author user
 * 
 */
public class JecnProcessDutyProperty extends JecnFileDescriptionComponent {

	private Log log = LogFactory.getLog(JecnProcessDutyProperty.class);
	// *************************流程责任属性***************************************
	/** 流程责任属性Lab */
	protected JLabel flowPropertyLab = new JLabel();
	/** 流程责任属性按钮Button */
	protected JButton flowPropertyBut = new JButton(JecnProperties.getValue("select"));;
	/** 流程监护人 */
	protected JLabel guardianPeopleLab = new JLabel();
	/** 流程监护人Field */
	protected JSearchTextField guardianField = new JSearchTextField(300, 20);

	/** 流程责任人Lab */
	private JLabel mapAttchMentLab = new JLabel();
	/** 流程责任人Field */
	protected JSearchTextField attchMentField = new JSearchTextField(300, 20);

	/** 流程拟制人Lab */
	protected JLabel fictionPeopleLab = new JLabel();
	/** 流程拟制人Field */
	protected JSearchTextField fictionPeopleField = new JSearchTextField(300, 20);

	/** 责任部门Lab */
	private JLabel departLab = new JLabel();
	/** 责任部门Field */
	protected JSearchTextField departField = new JSearchTextField(300, 20);

	/** 数据 */
	private ProcessAttributeBean processAttributeBean;

	private JecnTreeNode selectNode;
	private Long flowId;

	public JecnProcessDutyProperty(int index, String name, JecnPanel contentPanel, boolean isRequest,
			JecnTreeNode selectNode, Long flowId, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowId = flowId;
		try {
			this.processAttributeBean = ConnectionPool.getProcessAction().getFlowAttribute(flowId);
		} catch (Exception e) {
			log.error("", e);
		}
		this.selectNode = selectNode;
		initComp();
		initLayout();
		initData();
	}

	private void initComp() {

		flowPropertyBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectFlowDutyProperty();
			}
		});

		guardianField.setEditable(false);
		attchMentField.setEditable(false);
		fictionPeopleField.setEditable(false);
		departField.setEditable(false);
		guardianField.setOpaque(false);
		attchMentField.setOpaque(false);
		fictionPeopleField.setOpaque(false);
		departField.setOpaque(false);

	}

	private void selectFlowDutyProperty() {
		ProcessPropertyDialog propertyDialog = new ProcessPropertyDialog(selectNode);
		propertyDialog.setVisible(true);
		if (JecnConfigTool.processGuardianShowType() != 0) {
			this.guardianField.setText(propertyDialog.getGuardianName());// 流程监护人
		}
		this.attchMentField.setText(propertyDialog.getResPeopleName());// 流程责任人
		if (JecnConfigTool.processIntendedPersonShowType() != 0) {
			this.fictionPeopleField.setText(propertyDialog.getFictionPeopleName());// 流程拟制人
		}
		this.departField.setText(propertyDialog.getResOrgName());// 流程部门
	}

	private void initData() {

		// 流程责任部门名称
		String departNames = processAttributeBean.getDutyOrgName();
		// 流程责任人名称
		String perpleNames = processAttributeBean.getDutyUserName();

		if (departNames != null && !"".equals(departNames)) {
			departField.setText(departNames);
		}
		if (perpleNames != null && !"".equals(perpleNames)) {
			attchMentField.setText(perpleNames);
		}

		if (JecnConfigTool.processGuardianShowType() != 0) {
			// 流程监护人名称
			String guardianNames = processAttributeBean.getGuardianName();
			if (guardianNames != null && !"".equals(guardianNames)) {
				guardianField.setText(guardianNames);
			}
		}

		if (JecnConfigTool.processIntendedPersonShowType() != 0) {
			// 流程拟制人名称
			String fictionPeopleNames = processAttributeBean.getFictionPeopleName();
			if (fictionPeopleNames != null && !"".equals(fictionPeopleNames)) {
				fictionPeopleField.setText(fictionPeopleNames);
			}

		}

	}

	private void initLayout() {
		titlePanel.add(flowPropertyBut);
		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBorder(BorderFactory.createTitledBorder(""));
		GridBagConstraints c = null;
		if (JecnConfigTool.processGuardianShowType() != 0) {
			c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			guardianPeopleLab.setText(JecnProperties.getValue("guardianPeopleC"));
			centerPanel.add(guardianPeopleLab, c);
			// driveTypeLab.setBorder(BorderFactory.createTitledBorder(""));
			// 流程监护人Field
			c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			centerPanel.add(guardianField, c);
		}
		// 流程责任人Lab
		mapAttchMentLab.setText(JecnProperties.getValue("mapAttchMentC"));
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		centerPanel.add(mapAttchMentLab, c);
		// 流程责任人Filed
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(attchMentField, c);
		if (JecnConfigTool.processIntendedPersonShowType() != 0) {
			// 流程拟制人Lab
			fictionPeopleLab.setText(JecnProperties.getValue("fictionPeopleC"));
			c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(fictionPeopleLab, c);
			// 流程拟制人Filed
			c = new GridBagConstraints(1, 2, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			centerPanel.add(fictionPeopleField, c);
		}
		// 流程责任部门Lab
		departLab.setText(JecnProperties.getValue("departC"));
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		centerPanel.add(departLab, c);
		// 流程责任部门Filed
		c = new GridBagConstraints(1, 3, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(departField, c);

	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
