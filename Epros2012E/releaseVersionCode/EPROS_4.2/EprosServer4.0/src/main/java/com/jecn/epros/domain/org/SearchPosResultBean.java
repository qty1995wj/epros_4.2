package com.jecn.epros.domain.org;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;

public class SearchPosResultBean {
    private Long figureId;
    private String figureName;
    private Long orgId;
    private String orgName;

    public LinkResource getLink() {
        return new LinkResource(figureId, figureName, ResourceType.POSITION);
    }

    public LinkResource getOrgLink() {
        return new LinkResource(orgId, orgName, ResourceType.ORGANIZATION);
    }

    @JsonIgnore
    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    @JsonIgnore
    public String getFigureName() {
        return figureName;
    }

    public void setFigureName(String figureName) {
        this.figureName = figureName;
    }

    @JsonIgnore
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    @JsonIgnore
    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
