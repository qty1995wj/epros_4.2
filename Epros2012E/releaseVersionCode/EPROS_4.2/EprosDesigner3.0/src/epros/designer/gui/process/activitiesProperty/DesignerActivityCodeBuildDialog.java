package epros.designer.gui.process.activitiesProperty;

import javax.swing.DefaultComboBoxModel;

import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.gui.workflow.dialog.JecnActivityCodeBuildDialog;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

/**
 * 设计器，活动自动编号重构,添加新规则
 * 
 *@author ZXH
 * @date 2016-7-1下午02:19:58
 */
public class DesignerActivityCodeBuildDialog extends JecnActivityCodeBuildDialog {

	public DesignerActivityCodeBuildDialog(JecnDrawDesktopPane workflow) {
		super(workflow);
	}

	public void initNumBox() {
		// JList存放的数据
		String[] strings = { "1,2,3,4,5,6,...", "01,02,03,04,05,06,07,08,09,10,11...",
				"001,002,003,004,005,006,007,008,009,010...", "010,020,030,040,050,060,070,080,090,100,110..." };

		// 系统配置
		String value = ConnectionPool.getConfigAciton().selectValueFromCache(ConfigItemPartMapMark.activeCodeSelect);
		int index = DrawCommon.isNullOrEmtryTrim(value) ? 1 : Integer.valueOf(value);
		// 初始化下拉框
		numberTypeJcomboBox.setModel(new DefaultComboBoxModel(strings));
		numberTypeJcomboBox.setSelectedIndex(index);
		numberTypeJcomboBox.addActionListener(this);
		numberTypeJcomboBox.setEnabled(false);
	}

	protected boolean isHiddenActiveType() {
		if(JecnConfigTool.isActivityNumTypeShow()){
			return true;
		}
		return false;
	}
	
	/**
	 * 编号规则
	 */
	protected void numsRule(JecnActiveData activeData, int indexType, Integer actCode, String activeText, int i) {
		super.numsRule(activeData, indexType, actCode, activeText, i);
		if (indexType == 3) {// 新增编号规则
			if (i < 9) {
				activeData.setActivityNum(activeText + "0" + actCode.toString() + "0");
			} else {
				activeData.setActivityNum(activeText + actCode.toString() + "0");
			}
		}
	}

}
