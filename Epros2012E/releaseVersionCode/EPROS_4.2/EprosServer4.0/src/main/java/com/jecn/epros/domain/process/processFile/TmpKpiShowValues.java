package com.jecn.epros.domain.process.processFile;

import com.jecn.epros.domain.process.kpi.FlowKpiName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TmpKpiShowValues implements Serializable {
    /**
     * KPI 动态标题
     */
    private List<String> kpiTitles;
    /**
     * KPI 动态行数据
     */
    private List<List<String>> kpiRowValues;
    /**
     * KPI 数据
     */
    private List<FlowKpiName> kpiNameList;

    public List<FlowKpiName> getKpiNameList() {
        if (kpiNameList == null) {
            kpiNameList = new ArrayList<FlowKpiName>();
        }
        return kpiNameList;
    }

    public void setKpiNameList(List<FlowKpiName> kpiNameList) {
        this.kpiNameList = kpiNameList;
    }

    public List<String> getKpiTitles() {
        if (kpiTitles == null) {
            kpiTitles = new ArrayList<String>();
        }
        return kpiTitles;
    }

    public void setKpiTitles(List<String> kpiTitles) {
        this.kpiTitles = kpiTitles;
    }

    public List<List<String>> getKpiRowValues() {
        if (kpiRowValues == null) {
            kpiRowValues = new ArrayList<List<String>>();
        }
        return kpiRowValues;
    }

    public void setKpiRowValues(List<List<String>> kpiRowValues) {
        this.kpiRowValues = kpiRowValues;
    }
}
