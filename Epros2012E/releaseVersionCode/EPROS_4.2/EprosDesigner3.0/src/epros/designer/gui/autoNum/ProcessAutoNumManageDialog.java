package epros.designer.gui.autoNum;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.List;

import javax.swing.BorderFactory;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.designer.util.JecnConfigTool;
import epros.draw.util.JecnResourceUtil;

/**
 * 流程、知道、文件自动编号管理页面
 * 
 * @author ZXH
 * 
 */
public class ProcessAutoNumManageDialog extends AutoNumManageDialog {

	/** 流程级别 */
	protected AreaScrollPanel processLevelScrollPanel;
	protected JecnConfigItemBean processLevelItem;

	public ProcessAutoNumManageDialog() {
		super(0);
	}

	public void initCompotents() {
		super.initCompotents();
		// 设置Dialog大小
		this.setSize(555, 430);

		processLevelScrollPanel = new AreaScrollPanel();
		processLevelScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				processLevelItem.getName(JecnResourceUtil.getLocale())));

	}

	protected void initData() {
		super.initData();
		processLevelItem = JecnConfigTool.getProcessLevel();
	}

	protected void initCompotentsValue() {
		super.initCompotentsValue();
		processLevelScrollPanel.getTextArea().setText(processLevelItem.getValue());
	}

	/**
	 * 除公司代码外，其他组件布局
	 */
	public void addOtherPanel(GridBagConstraints c, Insets insets) {
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(processLevelScrollPanel, c);
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(treePanel, c);
	}

	protected void getOkButtonItems(List<JecnConfigItemBean> itemBeans) {
		processLevelItem.setValue(processLevelScrollPanel.getTextArea().getText());
		itemBeans.add(companyItem);
		itemBeans.add(processLevelItem);
	}
}
