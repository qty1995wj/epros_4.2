package com.jecn.epros.server.download.customized.baoLong;

import java.awt.Color;
import java.awt.Point;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.download.HeaderDocBean;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.download.word.DownloadUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.table.RtfBorder;
import com.lowagie.text.rtf.table.RtfBorderGroup;
import com.lowagie.text.rtf.table.RtfCell;

/**
 * 操作说明下载
 * 
 * @author ZHANGXH
 */
public class ProcessDownloadWord {

	public static String createWordFile(
			ProcessDownloadBean processDownloadBean, String path)
			throws Exception {
		// 流程名称
		if (processDownloadBean.getFlowName() == null) {
			return "";
		}
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);
		String filePath = "";
		// 判断传入的路径是否为空
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			filePath = path;
		} else {
			filePath = JecnFinal.getAllTempPath(JecnFinal.saveTempFilePath());
		}
		FileOutputStream fileOutputStream = new FileOutputStream(filePath);
		RtfWriter2.getInstance(document, fileOutputStream);
		document.open();
		// 生成页眉
		HeaderDocBean headerDocBean = new HeaderDocBean();
		// 名称
		headerDocBean.setName(processDownloadBean.getFlowName());
		// 是否公开
		headerDocBean.setIsPublic(processDownloadBean.getFlowIsPublic());
		// 编号
		headerDocBean.setInputNum(processDownloadBean.getFlowInputNum());
		// 版本
		headerDocBean.setVersion(processDownloadBean.getFlowVersion());
		// 发布日期
		headerDocBean.setReleaseDate(processDownloadBean.getReleaseDate());
		// 生效日期
		headerDocBean.setEffectiveDate(processDownloadBean.getEffectiveDate());
		DownBlUtil.getHeaderDoc(document, headerDocBean);

		// 生成页脚
		DownBlUtil.getFooterDoc(document);
		String line = "\n\n\n\n";
		document.add(new Paragraph(line));

		int count = 1;
		for (JecnConfigItemBean JecnConfigItemBean : processDownloadBean
				.getJecnConfigItemBean()) {
			if ("0".equals(JecnConfigItemBean.getValue())) {// 是否显示
				continue;
			}

			String titleName = count + "、" + JecnConfigItemBean.getName();
			if ("a1".equals(JecnConfigItemBean.getMark())) {
				// 1、目的
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowPurpose = "";
				if (processDownloadBean.getFlowPurpose() != null
						&& !"".equals(processDownloadBean.getFlowPurpose())) {
					flowPurpose = processDownloadBean.getFlowPurpose();
				} else {
					flowPurpose = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowPurpose));
				document.add(new Paragraph());
				count++;
			} else if ("a2".equals(JecnConfigItemBean.getMark())) {
				// 2、适用范围
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String applicability = "";
				if (processDownloadBean.getApplicability() != null
						&& !"".equals(processDownloadBean.getApplicability())) {
					applicability = processDownloadBean.getApplicability();
				} else {
					applicability = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(applicability));
				document.add(new Paragraph());
				count++;
			} else if ("a3".equals(JecnConfigItemBean.getMark())) {
				// 3、术语定义
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String nuotglOssary = "";
				if (processDownloadBean.getNoutGlossary() != null
						&& !"".equals(processDownloadBean.getNoutGlossary())) {
					nuotglOssary = processDownloadBean.getNoutGlossary();
				} else {
					nuotglOssary = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(nuotglOssary));
				document.add(new Paragraph());
				count++;
			} else if ("a4".equals(JecnConfigItemBean.getMark())) {// 驱动规则
				// 0是事件驱动;1是事件驱动
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				if (processDownloadBean.getFlowDriver().getDriveType() != null) {
					title.setSpacingBefore(15f);
					Table table = new Table(2);
					table.setWidths(new int[] { 10, 50 });
					table.setAlignment(Table.ALIGN_LEFT);
					table.setWidth(100f);
					String driverType = JecnUtil.getValue("drivingType");
					String driverRules = JecnUtil.getValue("drivingRules");
					Cell driverTypeCell = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(driverType));// str1=驱动类型
					driverTypeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(driverTypeCell, 0, 0);

					Cell driverRulesCell = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(driverRules));// qdgz="驱动规则";
					driverRulesCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(driverRulesCell, 1, 0);

					String driveType = "";
					if ("1".equals(processDownloadBean.getFlowDriver()
							.getDriveType().toString())) {
						driveType = JecnUtil.getValue("timeDrive");

					} else {
						driveType = JecnUtil.getValue("eventDriven");
					}
					table.addCell(DownBlUtil.BODY_11_MIDDLE(driveType),
							new Point(0, 1));
					if ("1".equals(processDownloadBean.getFlowDriver()
							.getDriveType().toString())) {
						// 频率
						String frequency = JecnUtil.getValue("frequency");
						// 开始时间
						String startingTime = JecnUtil.getValue("startTime");
						// 结束时间
						String endingTime = JecnUtil.getValue("endTime");
						// 类型
						String type = JecnUtil.getValue("type");

						Cell frequencyCell = new Cell(DownBlUtil
								.BODY_BOLD_11_MIDDLE(frequency));
						frequencyCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(frequencyCell, 2, 0);

						Cell startingTimeCell = new Cell(DownBlUtil
								.BODY_BOLD_11_MIDDLE(startingTime));
						startingTimeCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(startingTimeCell, 3, 0);

						Cell endingTimeCell = new Cell(DownBlUtil
								.BODY_BOLD_11_MIDDLE(endingTime));
						endingTimeCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(endingTimeCell, 4, 0);

						Cell typeCell = new Cell(DownBlUtil
								.BODY_BOLD_11_MIDDLE(type));
						typeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(typeCell, 5, 0);
					}
					if ("1".equals(processDownloadBean.getFlowDriver()
							.getDriveType().toString())) {
						table.addCell(DownBlUtil.BODY_11_MIDDLE(DownloadUtil
								.repaceAllInfo(processDownloadBean
										.getFlowDriver().getDriveRules())),
								new Point(1, 1));
						if ("1".equals(processDownloadBean.getFlowDriver()
								.getDriveType().toString())) {
							String dateType = "";
							if ("1".equals(processDownloadBean.getFlowDriver()
									.getQuantity())) { // 日
								dateType = JecnUtil.getValue("day");
							} else if ("2".equals(processDownloadBean
									.getFlowDriver().getQuantity())) { // 周
								dateType = JecnUtil.getValue("weeks");
							} else if ("3".equals(processDownloadBean
									.getFlowDriver().getQuantity())) { // 月
								dateType = JecnUtil.getValue("month");
							} else if ("4".equals(processDownloadBean
									.getFlowDriver().getQuantity())) { // 季
								dateType = JecnUtil.getValue("season");
							} else if ("5".equals(processDownloadBean
									.getFlowDriver().getQuantity())) { // 年
								dateType = JecnUtil.getValue("years");
							}
							table.addCell(DownBlUtil
									.BODY_11_MIDDLE(processDownloadBean
											.getFlowDriver().getFrequency()),
									new Point(2, 1));
							table.addCell(DownBlUtil
									.BODY_11_MIDDLE(processDownloadBean
											.getFlowDriver().getStartTime()),
									new Point(3, 1));
							table.addCell(DownBlUtil
									.BODY_11_MIDDLE(processDownloadBean
											.getFlowDriver().getEndTime()),
									new Point(4, 1));
							table.addCell(DownBlUtil.BODY_11_MIDDLE(dateType),
									new Point(5, 1));
						}
					} else {
						table.addCell(DownBlUtil.BODY_11_MIDDLE(DownloadUtil
								.changeNetToWordStyle(processDownloadBean
										.getFlowDriver().getDriveRules())),
								new Point(1, 1));
					}
					title.add(table);
					document.add(title);
					document.add(new Paragraph());
				} else {
					document.add(title);
					document.add(DownBlUtil.BODY_11_MIDDLE(JecnUtil
							.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a5".equals(JecnConfigItemBean.getMark())) {
				// 流程输入
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowInput = "";
				if (processDownloadBean.getFlowInput() != null
						&& !"".equals(processDownloadBean.getFlowInput())) {
					flowInput = processDownloadBean.getFlowInput();
				} else {
					flowInput = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowInput));
				document.add(new Paragraph());
				count++;
			} else if ("a6".equals(JecnConfigItemBean.getMark())) {
				// 输出
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowOutput = "";
				if (processDownloadBean.getFlowOutput() != null
						&& !"".equals(processDownloadBean.getFlowOutput())) {
					flowOutput = processDownloadBean.getFlowOutput();
				} else {
					flowOutput = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowOutput));
				document.add(new Paragraph());
				count++;
			} else if ("a7".equals(JecnConfigItemBean.getMark())) {
				// 7关键活动
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				int paSize = 0;
				int ksfSize = 0;
				int kcpSize = 0;
				for (KeyActivityBean keyActivityBean : processDownloadBean
						.getKeyActivityShowList()) {
					if ("1".equals(keyActivityBean.getActiveKey())) {
						paSize++;
					} else if ("2".equals(keyActivityBean.getActiveKey())) {
						ksfSize++;
					} else if ("3".equals(keyActivityBean.getActiveKey())) {
						kcpSize++;
					}
				}
				if (paSize > 0 || ksfSize > 0 || kcpSize > 0) {
					Table table6 = null;
					table6 = new Table(5);
					table6.setWidths(new int[] { 15, 15, 25, 30, 20 });
					table6.setAlignment(Table.ALIGN_LEFT);
					table6.setWidth(100f);
					// 关键活动
					String gjhd = JecnUtil.getValue("keyActivities");
					// 活动编号
					String hdbh = JecnUtil.getValue("activitynumbers");
					// 活动名称
					String hdmc = JecnUtil.getValue("activityTitle");
					// 活动说明
					String hdsm = JecnUtil.getValue("activityIndicatingThat");
					// 关键说明
					String KeyDescription = JecnUtil.getValue("keyShow");
					// 问题区域
					String ProblemArea = JecnUtil.getValue("problemAreas");
					// 关键成功因素
					String KeySuccessFactor = JecnUtil
							.getValue("keySuccessFactors");
					// 关键控制点
					String KeyControlPoint = JecnUtil
							.getValue("keyControlPoint");
					Cell cell5_00 = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(gjhd));// 关键活动
					cell5_00.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_00, 0, 0);
					Cell cell5_01 = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(hdbh));// hdbh="活动编号";
					cell5_01.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_01, 0, 1);
					Cell cell5_02 = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(hdmc));// hdmc="活动名称";
					cell5_02.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_02, 0, 2);
					Cell cell5_03 = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(hdsm));// hdsm="活动说明";
					cell5_03.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_03, 0, 3);

					Cell cell5_04 = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(KeyDescription));
					cell5_04.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_04, 0, 4);

					Cell cell5_10 = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(ProblemArea));
					cell5_10.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_10, 1, 0);
					Cell cell5_20 = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(KeySuccessFactor));
					cell5_20.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (paSize > 0) {
						table6.addCell(cell5_20, paSize + 1, 0);
					}
					if (paSize == 0) {
						table6.addCell(cell5_20, 2, 0);
					}
					Cell cell5_30 = new Cell(DownBlUtil
							.BODY_BOLD_11_MIDDLE(KeyControlPoint));
					cell5_30.setHorizontalAlignment(Element.ALIGN_CENTER);
					// 设置关键控制点标题
					if (paSize > 0 && ksfSize > 0) {
						table6.addCell(cell5_30, paSize + ksfSize + 1, 0);
					} else if (paSize > 0) {
						table6.addCell(cell5_30, paSize + 2, 0);
					} else if (ksfSize > 0) {
						table6.addCell(cell5_30, ksfSize + 2, 0);
					} else if (paSize == 0 && ksfSize == 0) {
						table6.addCell(cell5_30, 3, 0);
					}
					int table6i = 1;
					for (KeyActivityBean keyActivityBean : processDownloadBean
							.getKeyActivityShowList()) {
						// 问题区域
						if ("1".equals(keyActivityBean.getActiveKey())) {
							String activityShow = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShow());
							String activityShowControl = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShowControl());
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(keyActivityBean
											.getActivityNumber()), new Point(
									table6i, 1));
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(keyActivityBean
											.getActivityName()), new Point(
									table6i, 2));
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(activityShow), new Point(
									table6i, 3));

							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(activityShowControl),
									new Point(table6i, 4));
							table6i++;
						}
					}
					if (paSize == 0) {
						table6i++;
					}
					for (KeyActivityBean keyActivityBean : processDownloadBean
							.getKeyActivityShowList()) {
						if ("2".equals(keyActivityBean.getActiveKey())) {
							String activityShow = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShow());
							String activityShowControl = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShowControl());
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(keyActivityBean
											.getActivityNumber()), new Point(
									table6i, 1));
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(keyActivityBean
											.getActivityName()), new Point(
									table6i, 2));
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(activityShow), new Point(
									table6i, 3));

							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(activityShowControl),
									new Point(table6i, 4));
							table6i++;
						}
					}
					if (ksfSize == 0) {
						table6i++;
					}
					for (KeyActivityBean keyActivityBean : processDownloadBean
							.getKeyActivityShowList()) {
						if ("3".equals(keyActivityBean.getActiveKey())) {
							String activityShow = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShow());
							String activityShowControl = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShowControl());
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(keyActivityBean
											.getActivityNumber()), new Point(
									table6i, 1));
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(keyActivityBean
											.getActivityName()), new Point(
									table6i, 2));
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(activityShow), new Point(
									table6i, 3));
							table6.addCell(DownBlUtil
									.BODY_11_MIDDLE(activityShowControl),
									new Point(table6i, 4));
							table6i++;
						}
					}
					if (kcpSize == 0) {
						table6i++;
					}
					if (paSize > 1) {
						cell5_10.setRowspan(paSize);
					}
					if (ksfSize > 1) {
						cell5_20.setRowspan(ksfSize);
					}
					if (kcpSize > 1) {
						cell5_30.setRowspan(kcpSize);
					}
					title.add(table6);
					document.add(title);
					document.add(new Paragraph());
				} else {
					document.add(title);
					document.add(DownBlUtil.BODY_11_MIDDLE(JecnUtil
							.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a8".equals(JecnConfigItemBean.getMark())) {
				// 8、角色职责
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getRoleList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 20, 80 };
					// 角色名称
					String roleName = JecnUtil.getValue("roleName");
					// 岗位名称
					String positionName = JecnUtil.getValue("positionName");
					// 角色职责
					String roleResponsibility = JecnUtil
							.getValue("responsibilities");
					List<String> roleTitleList = new ArrayList<String>();
					roleTitleList.add(roleName);
					roleTitleList.add(positionName);
					roleTitleList.add(roleResponsibility);
					Table table = DownBlUtil.getTable(tableWidth, tableInt,
							roleTitleList, processDownloadBean.getRoleList());
					document.add(table);
					document.add(new Paragraph());
				} else {
					document.add(DownBlUtil.BODY_11_MIDDLE(JecnUtil
							.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a9".equals(JecnConfigItemBean.getMark())) {
				// 9、流程图
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				document.add(new Paragraph());
				// 流程图片
				Image img = null;
				if (processDownloadBean.getImgPath() != null
						&& !"".equals(processDownloadBean.getImgPath())) {
					img = JecnFinal.getImage(processDownloadBean.getImgPath());
				}
				if (img != null) {
					// 图像宽度大小
					float imgWidth = img.getWidth();
					float imgHeight = img.getHeight();
					if (imgWidth == 0 && imgHeight == 0) {
						// 除数不能为零
						continue;
					}
					float docWidth = document.getPageSize().getWidth() - 160;
					float docHeight = document.getPageSize().getHeight() - 200;
					int widhtScale = (int) ((docWidth / imgWidth) * 100) + 1;
					int heightScale = (int) ((docHeight / imgHeight) * 100) + 1;
					int scale = 0;
					if (widhtScale > heightScale) {
						scale = heightScale;
					} else {
						scale = widhtScale;
					}
					// 图像放大缩小倍数
					img.scalePercent(scale);
					document.add(img);
					document.add(new Paragraph());
				}
				count++;
			} else if ("a10".equals(JecnConfigItemBean.getMark())) {
				// 活动明细
				createWordActive(titleName, document, processDownloadBean);
				count++;
			} else if ("a11".equals(JecnConfigItemBean.getMark())) {
				// 11、流程记录
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);

				if (processDownloadBean.getProcessRecordList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 15, 50 };
					// 文件编号
					String fileNumber = JecnUtil.getValue("fileNumber");
					// 文件名称
					String fileName = JecnUtil.getValue("fileName");
					List<String> titleList = new ArrayList<String>();
					titleList.add(fileNumber);
					titleList.add(fileName);
					Table table = DownBlUtil.getTable(tableWidth, tableInt,
							titleList, processDownloadBean
									.getProcessRecordList());
					document.add(table);
					document.add(new Paragraph());
				} else {
					document.add(DownBlUtil.BODY_11_MIDDLE(JecnUtil
							.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a12".equals(JecnConfigItemBean.getMark())) {
				// 11、流程记录
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);

				if (processDownloadBean.getOperationTemplateList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 15, 50 };
					// 文件编号
					String fileNumber = JecnUtil.getValue("fileNumber");
					// 文件名称
					String fileName = JecnUtil.getValue("fileName");
					List<String> titleList = new ArrayList<String>();
					titleList.add(fileNumber);
					titleList.add(fileName);
					Table table = DownBlUtil.getTable(tableWidth, tableInt,
							titleList, processDownloadBean
									.getOperationTemplateList());
					document.add(table);
					document.add(new Paragraph());
				} else {
					document.add(DownBlUtil.BODY_11_MIDDLE(JecnUtil
							.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a13".equals(JecnConfigItemBean.getMark())) {
				// 13、相关流程
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				int upSize = 0;
				int lowSize = 0;
				int supSize = 0;
				int implSize = 0;
				for (RelatedProcessBean relatedProcessBean : processDownloadBean
						.getRelatedProcessList()) {
					if ("1".equals(relatedProcessBean.getLinecolor())) {
						upSize++;
					} else if ("2".equals(relatedProcessBean.getLinecolor())) {
						lowSize++;
					} else if ("3".equals(relatedProcessBean.getLinecolor())) {
						implSize++;
					} else if ("4".equals(relatedProcessBean.getLinecolor())) {
						supSize++;
					}
				}
				// int upSize = processBaseInfoBean.getListUpperFlows().size();
				// int lowSize = processBaseInfoBean.getListLowerFlows().size();
				// int supSize = processBaseInfoBean.getListSubFlows().size();
				// int implSize = processBaseInfoBean.getListImplFlows().size();

				if (upSize > 0 || lowSize > 0 || supSize > 0 || implSize > 0) {
					Table table13 = null;
					table13 = new Table(3);
					table13.setWidths(new int[] { 15, 10, 50 });
					table13.setWidth(100f);

					RtfCell cellDotted13 = new RtfCell("Dotted border");
					cellDotted13.setBorders(new RtfBorderGroup(Rectangle.BOX,
							RtfBorder.BORDER_DOTTED, 1, new Color(0, 0, 0)));
					table13.addCell(DownloadUtil.contentTitle(""), new Point(0,
							0));
					// 流程名称
					String ProcessName = JecnUtil.getValue("processName");
					// 流程编号
					String ProcessId = JecnUtil.getValue("processNumber");
					table13.addCell(DownBlUtil.BODY_BOLD_11_MIDDLE(ProcessId),
							new Point(0, 1));
					table13.addCell(
							DownBlUtil.BODY_BOLD_11_MIDDLE(ProcessName),
							new Point(0, 2));
					int table13i = 1;
					if (upSize > 0) {
						// 上游流程
						String flowUpper = JecnUtil.getValue("upstreamProcess");
						Cell cell0_131 = new Cell(DownBlUtil
								.BODY_BOLD_11_MIDDLE(flowUpper));
						cell0_131.setHorizontalAlignment(Element.ALIGN_CENTER);
						table13.addCell(cell0_131, 1, 0);
						cell0_131.setRowspan(upSize);

						for (RelatedProcessBean relatedProcessBean : processDownloadBean
								.getRelatedProcessList()) {
							if ("1".equals(relatedProcessBean.getLinecolor())) {
								table13.addCell(relatedProcessBean
										.getFlowNumber(),
										new Point(table13i, 1));
								table13.addCell(DownBlUtil
										.BODY_11_MIDDLE(relatedProcessBean
												.getFlowName()), new Point(
										table13i, 2));
								table13i++;
							}
						}
					}
					if (lowSize > 0) {
						// 下游流程
						String flowLower = JecnUtil
								.getValue("downstreamProcess");
						Cell cell0_131 = new Cell(DownBlUtil
								.BODY_BOLD_11_MIDDLE(flowLower));
						cell0_131.setHorizontalAlignment(Element.ALIGN_CENTER);
						table13.addCell(cell0_131, upSize + 1, 0);
						cell0_131.setRowspan(lowSize);
						for (RelatedProcessBean relatedProcessBean : processDownloadBean
								.getRelatedProcessList()) {
							if ("2".equals(relatedProcessBean.getLinecolor())) {
								table13.addCell(relatedProcessBean
										.getFlowNumber(),
										new Point(table13i, 1));
								table13.addCell(DownBlUtil
										.BODY_11_MIDDLE(relatedProcessBean
												.getFlowName()), new Point(
										table13i, 2));
								table13i++;
							}
						}
					}
					if (supSize > 0) {
						// 子流程
						String flowLower = JecnUtil.getValue("childProcess");
						Cell cell0_131 = new Cell(DownBlUtil
								.BODY_BOLD_11_MIDDLE(flowLower));
						cell0_131.setHorizontalAlignment(Element.ALIGN_CENTER);
						table13.addCell(cell0_131, lowSize + upSize + 1, 0);
						cell0_131.setRowspan(supSize);
						for (RelatedProcessBean relatedProcessBean : processDownloadBean
								.getRelatedProcessList()) {
							if ("4".equals(relatedProcessBean.getLinecolor())) {
								table13.addCell(relatedProcessBean
										.getFlowNumber(),
										new Point(table13i, 1));
								table13.addCell(DownBlUtil
										.BODY_11_MIDDLE(relatedProcessBean
												.getFlowName()), new Point(
										table13i, 2));
								table13i++;
							}
						}
					}
					if (implSize > 0) {
						// 过程流程
						String flowLower = JecnUtil.getValue("processFlow");
						Cell cell0_131 = new Cell(DownBlUtil
								.BODY_BOLD_11_MIDDLE(flowLower));
						cell0_131.setHorizontalAlignment(Element.ALIGN_CENTER);
						table13.addCell(cell0_131, supSize + lowSize + upSize
								+ 1, 0);
						cell0_131.setRowspan(implSize);
						for (RelatedProcessBean relatedProcessBean : processDownloadBean
								.getRelatedProcessList()) {
							if ("3".equals(relatedProcessBean.getLinecolor())) {
								table13.addCell(relatedProcessBean
										.getFlowNumber(),
										new Point(table13i, 1));
								table13.addCell(DownBlUtil
										.BODY_11_MIDDLE(relatedProcessBean
												.getFlowName()), new Point(
										table13i, 2));
								table13i++;
							}
						}
					}
					document.add(table13);
					document.add(new Paragraph());
				} else {
					document.add(DownBlUtil.BODY_11_MIDDLE(JecnUtil
							.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a14".equals(JecnConfigItemBean.getMark())) {
				// 14、相关制度
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getRuleNameList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 80, 20 };
					// 制度名称
					String name = JecnUtil.getValue("ruleName");
					// 类型
					String type = JecnUtil.getValue("type");
					List<String> ruleTitleList = new ArrayList<String>();
					ruleTitleList.add(name);
					ruleTitleList.add(type);
					Table table = DownloadUtil.getTable(tableWidth, tableInt,
							ruleTitleList, processDownloadBean
									.getRuleNameList());
					document.add(table);
					document.add(new Paragraph());
				} else {
					document.add(DownloadUtil.getContent("\t"
							+ JecnUtil.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a15".equals(JecnConfigItemBean.getMark())) {
				// 15、流程关键测评指标板
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getFlowKpiList().size() > 0) {
					for (Object[] obj : processDownloadBean.getFlowKpiList()) {
						Object objStr = obj[1];
						if (objStr != null
								&& Integer.valueOf(objStr.toString()) == 1) {
							// 过程性指标
							obj[1] = JecnUtil.getValue("efficiencyIndex");
						} else if (objStr != null
								&& Integer.valueOf(objStr.toString()) == 0) {
							// 结果性指标
							obj[1] = JecnUtil.getValue("effectIndex");
						} else {
							// 无
							obj[1] = JecnUtil.getValue("none");
						}
					}
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 10, 25, 30, 15 };
					// 名称
					String name = JecnUtil.getValue("name");
					// 类型
					String type = JecnUtil.getValue("type");
					// KPI定义
					String KpiDefinition = JecnUtil.getValue("KPIDefinition");
					// 统计方法
					String statisticsMethod = JecnUtil
							.getValue("statisticalMethod");
					// 目标值
					String targetValue = JecnUtil.getValue("targetValue");
					List<String> kpiTitleList = new ArrayList<String>();
					kpiTitleList.add(name);
					kpiTitleList.add(type);
					kpiTitleList.add(KpiDefinition);
					kpiTitleList.add(statisticsMethod);
					kpiTitleList.add(targetValue);
					Table table = DownBlUtil.getTable(tableWidth, tableInt,
							kpiTitleList, processDownloadBean.getFlowKpiList());
					document.add(table);
					document.add(new Paragraph());
				} else {
					document.add(DownBlUtil.BODY_11_MIDDLE(JecnUtil
							.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a16".equals(JecnConfigItemBean.getMark())) {
				// 16、客户
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowCustom = "";
				if (processDownloadBean.getFlowCustom() != null
						&& !"".equals(processDownloadBean.getFlowCustom())) {
					flowCustom = processDownloadBean.getFlowCustom();
				} else {
					flowCustom = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowCustom));
				document.add(new Paragraph());
				count++;
			} else if ("a17".equals(JecnConfigItemBean.getMark())) {
				// 17、不适用范围
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String noApplicability = "";
				if (processDownloadBean.getNoApplicability() != null
						&& !"".equals(processDownloadBean.getNoApplicability())
						&& !"&nbsp".equals(processDownloadBean
								.getNoApplicability())) {
					noApplicability = processDownloadBean.getNoApplicability();
				} else {
					noApplicability = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(noApplicability));
				document.add(new Paragraph());
				count++;
			} else if ("a18".equals(JecnConfigItemBean.getMark())) {
				// 18、概况信息
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowSummarize = "";
				if (processDownloadBean.getFlowSummarize() != null
						&& !"".equals(processDownloadBean.getFlowSummarize())
						&& !"&nbsp".equals(processDownloadBean
								.getFlowSummarize())) {
					flowSummarize = processDownloadBean.getFlowSummarize();
				} else {
					flowSummarize = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowSummarize));
				document.add(new Paragraph());
				count++;
			} else if ("a19".equals(JecnConfigItemBean.getMark())) {
				// 19、补充说明
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowSupplement = "";
				if (processDownloadBean.getFlowSupplement() != null
						&& !"".equals(processDownloadBean.getFlowSupplement())
						&& !"&nbsp".equals(processDownloadBean
								.getFlowSupplement())) {
					flowSupplement = processDownloadBean.getFlowSupplement();
				} else {
					flowSupplement = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowSupplement));
				document.add(new Paragraph());
				count++;
			} else if ("a20".equals(JecnConfigItemBean.getMark())) {
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getFlowRecordList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 15, 20, 20, 30, 30, 30 };
					// 记录名称
					String recordName = JecnUtil.getValue("recordName");
					// 保存责任人
					String saveResponsiblePersons = JecnUtil
							.getValue("saveResponsiblePersons");
					// 保存场所
					String savePlace = JecnUtil.getValue("savePlace");
					// 归档时间
					String filingTime = JecnUtil.getValue("filingTime");
					// 保存期限
					String expirationDate = JecnUtil.getValue("storageLife");
					// 到期处理方式
					String handlingDue = JecnUtil.getValue("treatmentDue");
					List<String> recordTitleList = new ArrayList<String>();
					recordTitleList.add(recordName);
					recordTitleList.add(saveResponsiblePersons);
					recordTitleList.add(savePlace);
					recordTitleList.add(filingTime);
					recordTitleList.add(expirationDate);
					recordTitleList.add(handlingDue);
					Table table = DownBlUtil.getTable(tableWidth, tableInt,
							recordTitleList, processDownloadBean
									.getFlowRecordList());
					document.add(table);
					document.add(new Paragraph());
				} else {
					document.add(DownBlUtil.BODY_11_MIDDLE(JecnUtil
							.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a21".equals(JecnConfigItemBean.getMark())) { // 相关标准
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getStandardList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 80, 20 };
					// 标准名称
					String name = JecnUtil.getValue("standardName");
					// 类型
					String type = JecnUtil.getValue("type");
					List<String> standTitleList = new ArrayList<String>();
					standTitleList.add(name);
					standTitleList.add(type);
					Table table = DownloadUtil.getTable(tableWidth, tableInt,
							standTitleList, processDownloadBean
									.getStandardList());
					document.add(table);
					document.add(new Paragraph());
				} else {
					document.add(DownloadUtil.getContent("\t"
							+ JecnUtil.getValue("none")));
					document.add(new Paragraph());
				}
				count++;
			} else if ("a22".equals(JecnConfigItemBean.getMark())) { // 流程边界
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);

				title.setSpacingBefore(15f);
				Table table = new Table(2);
				table.setWidths(new int[] { 10, 50 });
				table.setAlignment(Table.ALIGN_LEFT);
				table.setWidth(100f);

				Cell startActive = new Cell(DownBlUtil
						.BODY_BOLD_11_MIDDLE("起始活动"));// 起始活动
				startActive.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(startActive, 0, 0);

				Cell endActiveCell = new Cell(DownBlUtil
						.BODY_BOLD_11_MIDDLE("终止活动"));// 终止活动
				endActiveCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(endActiveCell, 1, 0);

				if (processDownloadBean.getStartEndActive()[0] != null) {
					table.addCell(DownBlUtil.BODY_11_MIDDLE(processDownloadBean
							.getStartEndActive()[0].toString()),
							new Point(0, 1));
				}
				if (processDownloadBean.getStartEndActive()[1] != null) {
					table.addCell(DownBlUtil.BODY_11_MIDDLE(processDownloadBean
							.getStartEndActive()[1].toString()),
							new Point(1, 1));
				}
				document.add(table);
				document.add(new Paragraph());
				count++;
			} else if ("a23".equals(JecnConfigItemBean.getMark())) { // 流程责任属性
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);

				title.setSpacingBefore(15f);
				Table table = new Table(2);
				table.setWidths(new int[] { 10, 50 });
				table.setAlignment(Table.ALIGN_LEFT);
				table.setWidth(100f);

				Cell cell = new Cell(DownBlUtil.BODY_BOLD_11_MIDDLE("流程监护人"));// 流程监护人
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell, 0, 0);

				cell = new Cell(DownBlUtil.BODY_BOLD_11_MIDDLE("流程责任人"));// 流程责任人
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell, 1, 0);

				cell = new Cell(DownBlUtil.BODY_BOLD_11_MIDDLE("流程拟制人"));// 流程拟制人
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell, 2, 0);

				cell = new Cell(DownBlUtil.BODY_BOLD_11_MIDDLE("流程责任部门"));// 流程责任部门
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(cell, 3, 0);

				if (processDownloadBean.getResponFlow().getGuardianName() != null) {
					table.addCell(DownBlUtil.BODY_11_MIDDLE(processDownloadBean
							.getResponFlow().getGuardianName().toString()),
							new Point(0, 1));
				}

				if (processDownloadBean.getResponFlow().getDutyUserType() == 0
						&& processDownloadBean.getResponFlow()
								.getDutyUserName() != null) {
					table.addCell(DownBlUtil.BODY_11_MIDDLE(processDownloadBean
							.getResponFlow().getDutyUserName().toString()),
							new Point(1, 1));
				} else if (processDownloadBean.getResponFlow()
						.getDutyUserType() == 1
						&& processDownloadBean.getResponFlow()
								.getDutyUserName() != null) {
					table.addCell(DownBlUtil.BODY_11_MIDDLE(processDownloadBean
							.getResponFlow().getDutyUserName().toString()),
							new Point(1, 1));
				}

				// if (processDownloadBean.getResponFlow().getDutyUserType() ==
				// 0
				// && processDownloadBean.getResponFlow()
				// .getDutyUserName() != null) {
				// table.addCell(DownBlUtil.BODY_11_MIDDLE("人员："
				// + processDownloadBean.getResponFlow()
				// .getDutyUserName().toString()), new Point(
				// 1, 1));
				// } else if (processDownloadBean.getResponFlow()
				// .getDutyUserType() == 1
				// && processDownloadBean.getResponFlow()
				// .getDutyUserName() != null) {
				// table.addCell(DownBlUtil.BODY_11_MIDDLE("岗位："
				// + processDownloadBean.getResponFlow()
				// .getDutyUserName().toString()), new Point(
				// 1, 1));
				// }
				if (processDownloadBean.getResponFlow().getFictionPeopleName() != null) {
					table.addCell(DownBlUtil.BODY_11_MIDDLE(processDownloadBean
							.getResponFlow().getFictionPeopleName()),
							new Point(2, 1));
				}
				if (processDownloadBean.getResponFlow().getDutyOrgName() != null) {
					table
							.addCell(DownBlUtil
									.BODY_11_MIDDLE(processDownloadBean
											.getResponFlow().getDutyOrgName()),
									new Point(3, 1));
				}
				document.add(table);
				document.add(new Paragraph());
				count++;
			} else if ("a24".equals(JecnConfigItemBean.getMark())) { // 流程文控信息
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);

				float tableWidth = 100f;
				int[] tableInt = new int[] { 15, 20, 20, 30, 30 };
				// 版本号
				String version = "版本号";
				// 变更说明
				String changeDescription = "变更说明";
				// 发布日期
				String releaseDate = "发布日期";
				// 流程拟制人
				String artificialPerson = "流程拟制人";
				// 审批人
				String theApprover = "审批人";
				List<String> recordTitleList = new ArrayList<String>();
				recordTitleList.add(version);
				recordTitleList.add(changeDescription);
				recordTitleList.add(releaseDate);
				recordTitleList.add(artificialPerson);
				recordTitleList.add(theApprover);
				Table table = DownBlUtil.getTable(tableWidth, tableInt,
						recordTitleList, processDownloadBean.getDocumentList());
				document.add(table);
				document.add(new Paragraph());
				count++;
			}
			else if ("a25".equals(JecnConfigItemBean.getMark())) {// 相关风险
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				float tableWidth = 100f;
				int[] tableInt = new int[] { 15, 50 };
				// 风险编号
				String riskCode = JecnUtil.getValue("riskNum");
				// 风险描述
				String riskName = JecnUtil.getValue("riskName");
				List<String> titleList = new ArrayList<String>();
				titleList.add(riskCode);
				titleList.add(riskName);
				// 风险数据
				List<Object[]> list = new ArrayList<Object[]>();
				for (JecnRisk jecnRisk : processDownloadBean.getRilsList()) {
					String[] obj = new String[2];
					obj[0] = jecnRisk.getRiskCode();
					obj[1] = jecnRisk.getRiskName();
					list.add(obj);
				}
				Table table = DownBlUtil.getTable(tableWidth, tableInt,
						titleList, list);
				document.add(table);
				document.add(new Paragraph());
				count++;
			}else if ("a26".equals(JecnConfigItemBean.getMark())) {// 相关文件
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowRelatedFile = "";
				if (processDownloadBean.getFlowRelatedFile() != null
						&& !"".equals(processDownloadBean.getFlowRelatedFile())) {
					flowRelatedFile = processDownloadBean.getFlowRelatedFile();
				} else {
					flowRelatedFile =  JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowRelatedFile));
				document.add(new Paragraph());
				count++;
			} else if ("a27".equals(JecnConfigItemBean.getMark())) {// 自定义要素1
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowCustomOne = "";
				if (processDownloadBean.getFlowCustomOne() != null
						&& !"".equals(processDownloadBean.getFlowCustomOne())) {
					flowCustomOne = processDownloadBean.getFlowCustomOne();
				} else {
					flowCustomOne =  JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowCustomOne));
				document.add(new Paragraph());
				count++;
			} else if ("a28".equals(JecnConfigItemBean.getMark())) {// 自定义要素2
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowCustomTwo = "";
				if (processDownloadBean.getFlowCustomTwo() != null
						&& !"".equals(processDownloadBean.getFlowCustomTwo())) {
					flowCustomTwo = processDownloadBean.getFlowCustomTwo();
				} else {
					flowCustomTwo = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowCustomTwo));
				document.add(new Paragraph());
				count++;
			} else if ("a29".equals(JecnConfigItemBean.getMark())) {// 自定义要素3
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowCustomThree = "";
				if (processDownloadBean.getFlowCustomThree() != null
						&& !"".equals(processDownloadBean.getFlowCustomThree())) {
					flowCustomThree = processDownloadBean.getFlowCustomThree();
				} else {
					flowCustomThree = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowCustomThree));
				document.add(new Paragraph());
				count++;
			} else if ("a30".equals(JecnConfigItemBean.getMark())) {// 自定义要素4
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowCustomFore = "";
				if (processDownloadBean.getFlowCustomFour() != null
						&& !"".equals(processDownloadBean.getFlowCustomFour())) {
					flowCustomFore = processDownloadBean.getFlowCustomFour();
				} else {
					flowCustomFore =  JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowCustomFore));
				document.add(new Paragraph());
				count++;
			} else if ("a31".equals(JecnConfigItemBean.getMark())) {// 自定义要素5
				Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
				document.add(title);
				String flowCustomFive = "";
				if (processDownloadBean.getFlowCustomFive() != null
						&& !"".equals(processDownloadBean.getFlowCustomFive())) {
					flowCustomFive = processDownloadBean.getFlowCustomFive();
				} else {
					flowCustomFive = JecnUtil.getValue("none");
				}
				document.add(DownBlUtil.BODY_11_MIDDLE(flowCustomFive));
				document.add(new Paragraph());
				count++;
			}
		}
		document.close();
		fileOutputStream.close();
		return filePath;
	}

	/**
	 * 生成活动明细
	 * 
	 * @author fuzhh May 13, 2013
	 * @param titleName
	 * @param document
	 * @param processDownloadBean
	 * @throws Exception
	 */
	private static void createWordActive(String titleName, Document document,
			ProcessDownloadBean processDownloadBean) throws Exception {
		Paragraph title = DownBlUtil.BODY_BOLD_11_MIDDLE(titleName);
		document.add(title);
		if (processDownloadBean.getAllActivityShowList().size() > 0) {
			// if (processDownloadBean.getFlowFileType() == 1) {
			// for (Object[] obj : processDownloadBean
			// .getAllActivityShowList()) {
			// String activeNameNum = "\t";
			// if (obj[0] != null) {
			// activeNameNum += obj[0].toString();
			//
			// }
			// if (obj[2] != null) {
			// activeNameNum += obj[2].toString();
			// }
			// // 活动编号及名称
			// Paragraph parActiveNameNum = DownBlUtil
			// .BODY_BOLD_11_MIDDLE(activeNameNum);
			// document.add(parActiveNameNum);
			// // 执行角色:
			// String executiveRole = "\t"
			// + JecnUtil.getValue("executiveRoleC");
			// Paragraph parRole = DownBlUtil
			// .BODY_BOLD_11_MIDDLE(executiveRole);
			// // 角色名称
			// String roleName = JecnUtil.getValue("none");
			// if (obj[1] != null) {
			// roleName = obj[1].toString();
			// }
			// Paragraph parRoleName = DownBlUtil.BODY_11_MIDDLE(roleName);
			// parRole.add(parRoleName);
			// document.add(parRole);
			// // 活动说明:
			// String activeShow = "\t"
			// + JecnUtil.getValue("activityIndicatingThatC");
			// Paragraph activePar = DownBlUtil
			// .BODY_BOLD_11_MIDDLE(activeShow);
			// // 活动说明显示内容
			// String activeShowVal = JecnUtil.getValue("none");
			// if (obj[3] != null) {
			// activeShowVal = obj[3].toString();
			// }
			// Paragraph parActiveShow = DownBlUtil
			// .BODY_11_MIDDLE(activeShowVal);
			// activePar.add(parActiveShow);
			// document.add(activePar);
			// // 输入:
			// String inputName = "\t" + JecnUtil.getValue("inputC");
			// Paragraph parInput = DownBlUtil
			// .BODY_BOLD_11_MIDDLE(inputName);
			// // 输入数据
			// String inputVal = JecnUtil.getValue("none");
			// if (obj[4] != null && !"".equals(obj[4].toString())) {
			// inputVal = obj[4].toString();
			// }
			// inputVal = inputVal.replaceAll("\n", "|");
			// Paragraph parInputVal = DownBlUtil.BODY_11_MIDDLE(inputVal);
			// parInput.add(parInputVal);
			// document.add(parInput);
			// // 输出:
			// String outputName = "\t" + JecnUtil.getValue("outputC");
			// Paragraph parOutput = DownBlUtil
			// .BODY_BOLD_11_MIDDLE(outputName);
			// // 输出数据
			// String outputVal = JecnUtil.getValue("none");
			// if (obj[5] != null && !"".equals(obj[5].toString())) {
			// outputVal = obj[5].toString();
			// }
			// outputVal = outputVal.replaceAll("\n", "|");
			// Paragraph parOutVal = DownBlUtil.BODY_11_MIDDLE(outputVal);
			// parOutput.add(parOutVal);
			// document.add(parOutput);
			// String horizontalLine =
			// "-----------------------------------------------------------------------------------------------------------";
			// Paragraph parHorizontalLine = DownBlUtil
			// .BODY_BOLD_11_MIDDLE(horizontalLine);
			// document.add(parHorizontalLine);
			// }
			//
			// } else {
			float tableWidth = 100f;
			int[] tableInt = new int[] { 15, 20, 20, 30, 30, 30 };
			// 活动编号
			String activityNumber = JecnUtil.getValue("activitynumbers");
			// 执行角色
			String executiveRole = JecnUtil.getValue("executiveRole");
			// 活动名称
			String eventTitle = JecnUtil.getValue("activityTitle");
			// 活动说明
			String activityDescription = JecnUtil
					.getValue("activityIndicatingThat");
			// 输入
			String Input = JecnUtil.getValue("input");
			// 输出
			String Output = JecnUtil.getValue("output");
			List<String> activityTitleList = new ArrayList<String>();
			activityTitleList.add(activityNumber);
			activityTitleList.add(executiveRole);
			activityTitleList.add(eventTitle);
			activityTitleList.add(activityDescription);
			activityTitleList.add(Input);
			activityTitleList.add(Output);
			Table table = DownBlUtil.getTable(tableWidth, tableInt,
					activityTitleList, processDownloadBean
							.getAllActivityShowList());
			document.add(table);
			document.add(new Paragraph());
			// }
		} else {
			document.add(DownBlUtil.BODY_11_MIDDLE(JecnUtil.getValue("none")));
			document.add(new Paragraph());
		}

	}
}
