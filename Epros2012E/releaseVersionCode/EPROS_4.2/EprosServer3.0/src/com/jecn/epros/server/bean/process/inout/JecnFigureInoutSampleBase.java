package com.jecn.epros.server.bean.process.inout;

import java.io.Serializable;

/**
 * 样例和模板表
 * 
 * @author jecn
 * 
 */
public class JecnFigureInoutSampleBase implements Serializable {
	private String id;
	private Long flowId;
	/**
	 * 活动输入输出id
	 */
	private String inoutId;
	private Long fileId;
	/** 文件名称 不存数据库 */
	private String fileName;
	/**
	 * 0模板 1样例
	 */
	private int type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInoutId() {
		return inoutId;
	}

	public void setInoutId(String inoutId) {
		this.inoutId = inoutId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
