package com.jecn.epros.server.dao.process;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.common.IBaseDao;

public interface IProcessKPIDao extends IBaseDao<JecnFlowKpiNameT, Long> {
	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程下的KPI
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpiNameT> getJecnFlowKpiNameTList(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程下的KPI
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpiName> getJecnFlowKpiNameList(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：流程关键评测指标数据 0 名称 1 类型 2 KPI定义 3统计方法 4 目标值
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getKpiNameList(Long flowId, boolean isPub) throws Exception;

	/**
	 * 根据流程set集合 查询KPI
	 * 
	 * @author fuzhh 2013-12-4
	 * @param flowIdSet
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpiName> findFlowKpiBySet(Set<Long> flowIdSet) throws Exception;

	/**
	 * @description：获得流程下的KPI及相关数据
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowKpiNameTList(Long flowId, String pub) throws Exception;

	/**
	 * 农夫山泉操作说明KPI测评指标板数据
	 * 
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> nongfushanquan_getKpiNameList(Long flowId, boolean isPub) throws Exception;

	/**
	 * 获取KPIIT系统
	 * 
	 * @param flowId
	 * @param kpiAndId 
	 * @param isPub
	 * @return
	 */
	public List<Object[]> getKPIITSysTem(Long flowId, Long kpiAndId, boolean isPub);

}
