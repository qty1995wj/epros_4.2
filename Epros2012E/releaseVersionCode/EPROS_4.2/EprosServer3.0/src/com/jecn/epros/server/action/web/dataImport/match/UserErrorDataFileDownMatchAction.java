package com.jecn.epros.server.action.web.dataImport.match;

import java.io.File;
import java.io.InputStream;

import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.dataImport.match.buss.UserErrorDataFileDownMatchBuss;
import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;
import com.jecn.epros.server.service.dataImport.match.constant.MatchErrorInfo;

/**
 * 岗位匹配下载错误信息Excel格式
 * 
 * @author Administrator
 * @date： 日期：Feb 28, 2013 时间：2:25:10 PM
 */
public class UserErrorDataFileDownMatchAction extends BaseAction {
	/** 对应的业务处理类 */
	private UserErrorDataFileDownMatchBuss fileDownMatchBuss = null;

	/**
	 * 
	 * 执行下载
	 * 
	 * @return
	 * @throws Exception
	 */
	public String execute() {
		return checkFile();
	}

	/**
	 * 
	 * 判读文件是否存在
	 * 
	 * @return
	 */
	private String checkFile() {
		File file = new File(fileDownMatchBuss.getOutFilepath());
		if (file.exists()) {
			return MatchConstant.RESULT_SUCCESS;
		} else {
			outResultHtml(MatchErrorInfo.ERROR_FILE_NOT_EXISTS, null);
			return null;
		}
	}

	/**
	 * 
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() {
		return fileDownMatchBuss.getInputStream();
	}

	/**
	 * 
	 * 对于配置中的 ${fileName}, 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	public String getFileName() {
		return fileDownMatchBuss.getFileName();
	}

	public UserErrorDataFileDownMatchBuss getFileDownMatchBuss() {
		return fileDownMatchBuss;
	}

	public void setFileDownMatchBuss(
			UserErrorDataFileDownMatchBuss fileDownMatchBuss) {
		this.fileDownMatchBuss = fileDownMatchBuss;
	}

}
