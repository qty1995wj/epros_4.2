package epros.designer.gui.system.config.ui.right;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.main.JecnPropertyPanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 
 * 默认按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDefaultJButton extends JecnAbstractBaseJButton {

	public JecnDefaultJButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int result = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("whetherReduction"), JecnProperties.getValue("pointLab"), JOptionPane.YES_NO_OPTION);
		if (result == 1) {
			return;
		}
		// 属性面板
		JecnPropertyPanel propertyPanel = dialog.getPropertyPanel();
		propertyPanel.showDefaultProperty();
	}
}
