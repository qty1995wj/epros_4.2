package com.jecn.epros.sqlprovider;

import com.jecn.epros.security.JecnContants;
import com.jecn.epros.util.ConfigUtils;

import java.util.Map;

public class ProcessActiveSqlProvider extends BaseSqlProvider {

    /**
     * 活动基本信息
     *
     * @param map
     * @return
     */
    public String findFlowActiveBaseBean(Map<String, Object> map) {
        Long activeId = Long.valueOf(map.get("activeId").toString());
        String isPub = map.get("isPub").toString();
        Long historyId = strHistoryId(map);
        if (historyId != null && historyId != 0) {
            isPub = "_H";
        }
        String sql = "select jfsi.figure_id,jfsi.activity_id,jfsi.figure_text,jfsi.activity_show,jfsi.linecolor,jfsi.role_res,role.figure_text as role_name"
                + "  ,jat.type_name,jfsi.activity_input,jfsi.activity_output,jfs.flow_id,jfs.flow_name"
                + "  ,jfsi.time_type,jfsi.start_time,jfsi.stop_time"
                + "  ,jfsi.TARGET_VALUE,jfsi.STATUS_VALUE,jfsi.EXPLAIN,jfsi.CUSTOM_ONE" + "  from jecn_flow_structure_image" + isPub
                + " jfsi" + "  left join jecn_flow_structure" + isPub
                + " jfs on jfsi.flow_id = jfs.flow_id and jfs.del_state=0" + "  left join jecn_role_active" + isPub + " jra"
                + " on jfsi.figure_id = jra.figure_active_id" + "  left join jecn_flow_structure_image" + isPub
                + " role on jra.figure_role_id = role.figure_id"
                + "  left join jecn_active_type jat on jfsi.active_type_id = jat.type_id" + "  where jfsi.figure_id="
                + activeId;
        if (historyId != null && historyId != 0) {
            sql += " and jfsi.history_id = " + historyId + " and jfsi.history_id = jfs.history_id" +
                    "   and jfsi.history_id = jra.history_id" +
                    "   and jfsi.history_id = role.history_id";
        }
        return sql;
    }

    private Long strHistoryId(Map<String, Object> map) {
        if (map.get("historyId") == null || map.get("historyId").toString().equals("-1")) {
            return null;
        }
        return Long.valueOf(map.get("historyId").toString());
    }

    /**
     * 活动相关风险
     *
     * @param map
     * @return
     */
    public String findFlowActiveRelatedRiskControlPoints(Map<String, Object> map) {
        Long activeId = Long.valueOf(map.get("activeId").toString());
        String isPub = map.get("isPub").toString();
        Long historyId = strHistoryId(map);
        if (historyId != null && historyId != 0) {
            isPub = "_H";
        }
        String sql = "SELECT CP.CONTROL_CODE,JC.DESCRIPTION,JS.ID as RISK_ID,JS.RISK_CODE,JS.NAME AS RISK_NAME,"
                + "                  CP.FREQUENCY,CP.METHOD," + "                  CP.TYPE, CP.KEY_POINT"
                + "             FROM JECN_CONTROL_POINT" + isPub + "        CP,"
                + "                  JECN_RISK                   JS,"
                + "                  JECN_CONTROLTARGET          JC"
                + "            WHERE CP.TARGET_ID = JC.ID  AND JC.RISK_ID=JS.ID" + "            AND CP.ACTIVE_ID="
                + activeId;
        if (historyId != null && historyId != 0) {
            sql += " and CP.history_id = " + historyId;
        }
        return sql;
    }

    /**
     * 活动相关指标
     *
     * @param map
     * @return
     */
    public String findFlowActiveRelatedRefIndicators(Map<String, Object> map) {
        Long activeId = Long.valueOf(map.get("activeId").toString());
        String isPub = map.get("isPub").toString();
        if ("_T".equals(isPub) || "_t".equals(isPub)) {
            isPub = "t";
        }
        return "select t.indicator_name,t.indicator_value from jecn_ref_indicators" + isPub
                + " t where t.activity_id=" + activeId;
    }

    /**
     * 活动相关标准
     *
     * @param map
     * @return
     */
    public String findFlowActiveRelatedStandardBeans(Map<String, Object> map) {
        Long activeId = Long.valueOf(map.get("activeId").toString());
        String isPub = map.get("isPub").toString();
        Long historyId = strHistoryId(map);
        if (historyId != null && historyId != 0) {
            isPub = "_H";
        }
        String sql = "SELECT CC.CRITERION_CLASS_ID," +
                "                  CC.CRITERION_CLASS_NAME,CC.STAN_TYPE" +
                "             FROM JECN_ACTIVE_STANDARD" + isPub + "      AT," +
                "                  JECN_CRITERION_CLASSES      CC" +
                "             WHERE AT.STANDARD_ID = CC.CRITERION_CLASS_ID" +
                "              AND AT.ACTIVE_ID =" + activeId;
        if (historyId != null && historyId != 0) {
            sql += " AND AT.HISTORY_ID = " + historyId;
        }
        return sql;
    }

    /**
     * 活动信息化
     *
     * @param map
     * @return
     */
    public String findFlowActiveOnLineBeans(Map<String, Object> map) {
        Long activeId = Long.valueOf(map.get("activeId").toString());
        String isPub = map.get("isPub").toString();
        Long historyId = strHistoryId(map);
        if (historyId != null && historyId != 0) {
            isPub = "_H";
        }
        String sql = "SELECT ST.FLOW_SUSTAIN_TOOL_DESCRIBE,OT.ONLINE_TIME" +
                "  FROM JECN_ACTIVE_ONLINE" + isPub + " OT, JECN_FLOW_SUSTAIN_TOOL ST" +
                "  WHERE OT.TOOL_ID = ST.FLOW_SUSTAIN_TOOL_ID" +
                "  AND OT.ACTIVE_ID=" + activeId;
        if (historyId != null && historyId != 0) {
            sql += " AND OT.HISTORY_ID = " + historyId;
        }
        return sql;
    }


    /**
     * 活动操作规范
     *
     * @param map
     * @return
     */
    public String findActiveOperationFiles(Map<String, Object> map) {
        return findActiveInputOrOperationFileSql(map, 1);
    }

    private String findActiveInputOrOperationFileSql(Map<String, Object> map, int type) {
        Long activeId = Long.valueOf(map.get("activeId").toString());
        String isPub = map.get("isPub").toString();
        Long historyId = strHistoryId(map);
        if (historyId == null || historyId == 0) {
            return findActiveOperationFileSql(isPub, activeId, type);
        } else {
            return findHistoryActiveOperationFileSql(activeId, type) + " AND JAF.HISTORY_ID = #{historyId}";
        }
    }

    public String findNewActiveInFiles(Map<String, Object> map) {
        Long activeId = Long.valueOf(map.get("activeId").toString());
        String isPub = map.get("isPub").toString();
        Long historyId = strHistoryId(map);
        if (historyId == null || historyId == 0) {
            return findTermActiveOperationFileSql(isPub, activeId) + " order by inout.sort_id";
        } else {
            return findTermActiveOperationHistoryFileSql(activeId) + " AND inout.HISTORY_ID = #{historyId} order by inout.sort_id";
        }
    }

    private String findActiveOperationFileSql(String isPub, Long activeId, int type) {
        return "SELECT  FI.ID FILE_ID,JF.FILE_NAME  FROM" +
                "  JECN_ACTIVITY_FILE" + isPub + " JAF" +
                "  INNER JOIN JECN_FILE" + isPub + " JF ON JAF.FILE_S_ID = JF.FILE_ID AND JF.DEL_STATE=0" +
                "  INNER JOIN FILE_CONTENT FI ON FI.ID = JF.VERSION_ID " +
                "  WHERE JAF.FILE_TYPE=" + type + "  AND JAF.FIGURE_ID=" + activeId;
    }

    private String findTermActiveOperationFileSql(String isPub, Long activeId) {
        return " select jf.version_id FILE_ID, JF.File_Name,inout.EXPLAIN,inout.name,inout.ID " +
                "  from jecn_figure_in_out" + isPub + " inout" +
                "      left join jecn_figure_in_out_sample" + isPub + " sam" +
                "     on inout.id = sam.in_out_id" +
                "    and sam.type = 0" +
                " left join jecn_file" + isPub + " jf" +
                "    on sam.file_id = jf.file_id" +
                "   and jf.del_state = 0" +
                " where inout.type = 0" +
                "   AND inout.FIGURE_ID =" + activeId;
    }

    private String findTermActiveOperationHistoryFileSql(Long activeId) {
        return " select jf.version_id FILE_ID, JF.File_Name,inout.EXPLAIN,inout.name,inout.ID" +
                "  from jecn_figure_in_out_H inout" +
                "      left join jecn_figure_in_out_sample_h sam" +
                "     on inout.id = sam.in_out_id" +
                "    and sam.type = 0" +
                "     and inout.HISTORY_ID = sam.HISTORY_ID" +
                " left join jecn_file jf" +
                "    on sam.file_id = jf.file_id" +
                "   and jf.del_state = 0" +
                " where inout.type = 0" +
                "   AND inout.FIGURE_ID =" + activeId;
    }

    private String findHistoryActiveOperationFileSql(Long activeId, int type) {
        return "SELECT  FC.ID FILE_ID,FC.FILE_NAME  FROM" +
                "  JECN_ACTIVITY_FILE_H JAF" +
                "  INNER JOIN FILE_CONTENT FC ON JAF.FILE_S_ID = FC.ID" +
                "  WHERE JAF.FILE_TYPE=" + type + " AND JAF.FIGURE_ID=" + activeId;
    }

    /**
     * 活动输入
     *
     * @param map
     * @return
     */
    public String findActiveInFiles(Map<String, Object> map) {
        return findActiveInputOrOperationFileSql(map, 0);
    }

    /**
     * 活动输出
     *
     * @param map
     * @return
     */
    public String findActiveOutFiles(Map<String, Object> map) {
        Long historyId = strHistoryId(map);
        if (!ConfigUtils.useNewInout()) {
            if (historyId == null || historyId == 0) {
                return findActiveOutFileSql(map);
            } else {
                return findHistoryActiveOutFileSql(map);
            }
        }
        if (historyId == null || historyId == 0) {
            return findTermActiveOutFileSql(map);
        } else {
            return findTermActiveOutHistoryFileSql(map);
        }
    }

    private String findActiveOutFileSql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        return "SELECT MODE_FILE.*," +
                "       TEMPLET.FILE_ID   AS TEMPLETFILEID," +
                "       TEMPLET.FILE_NAME AS TEMPLETFILENAME" +
                "  FROM (SELECT FI.ID FILE_ID, JF.FILE_NAME, JMF.MODE_FILE_ID" +
                "          FROM JECN_MODE_FILE" + isPub + " JMF" +
                "         INNER JOIN JECN_FILE" + isPub + " JF" +
                "            ON JMF.FILE_M_ID = JF.FILE_ID" +
                "           AND JF.DEL_STATE = 0" +
                "           INNER JOIN FILE_CONTENT FI ON FI.ID = JF.VERSION_ID " +
                "         WHERE JMF.FIGURE_ID = #{activeId}) MODE_FILE" +
                "  LEFT JOIN (SELECT FI.ID FILE_ID, JF.FILE_NAME, JMF.MODE_FILE_ID" +
                "               FROM JECN_MODE_FILE" + isPub + " JMF" +
                "              INNER JOIN JECN_TEMPLET" + isPub + " JT" +
                "                 ON JT.MODE_FILE_ID = JMF.MODE_FILE_ID" +
                "              INNER JOIN JECN_FILE" + isPub + " JF" +
                "                 ON JT.FILE_ID = JF.FILE_ID" +
                "                AND JF.DEL_STATE = 0" +
                "           INNER JOIN FILE_CONTENT FI ON FI.ID = JF.VERSION_ID " +
                "              WHERE JMF.FIGURE_ID = #{activeId}) TEMPLET" +
                "    ON TEMPLET.MODE_FILE_ID = MODE_FILE.MODE_FILE_ID" +
                " ORDER BY MODE_FILE.MODE_FILE_ID";
    }

    private String findTermActiveOutFileSql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        return "select jf.version_id file_id," +
                "       jf.file_name  file_name," +
                "       jfm.version_id   TEMPLETFILEID," +
                "       jfm.file_name TEMPLETFILENAME," +
                "       inout.EXPLAIN,inout.name," +
                "       sam.type," +
                "       inout.id mode_file_id" +
                "  from jecn_figure_in_out" + isPub + " inout" +
                " left join jecn_file" + isPub + " jf" +
                "    on inout.file_id = jf.file_id" +
                "   and jf.del_state = 0" +
                "  left join jecn_figure_in_out_sample" + isPub + " sam" +
                "    on inout.id = sam.in_out_id" +
                "  left join jecn_file" + isPub + " jfm" +
                "    on sam.file_id = jfm.file_id" +
                "   and jfm.del_state = 0" +
                " where inout.type = 1" +
                "   and inout.figure_id =#{activeId} order by inout.sort_id";

    }

    private String findTermActiveOutHistoryFileSql(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        return "select jf.version_id file_id," +
                "       jf.file_name  file_name," +
                "       jfm.version_id   TEMPLETFILEID," +
                "       jfm.file_name TEMPLETFILENAME," +
                "       inout.EXPLAIN ,inout.name," +
                "       sam.type," +
                "       inout.id mode_file_id" +
                "  from jecn_figure_in_out_h inout" +
                " left join jecn_file jf" +
                "    on inout.file_id = jf.file_id" +
                "   and jf.del_state = 0" +
                "  left join jecn_figure_in_out_sample_h sam" +
                "    on inout.id = sam.in_out_id" +
                "  and inout.HISTORY_ID = sam.HISTORY_ID " +
                "  left join jecn_file jfm" +
                "    on sam.file_id = jfm.file_id" +
                "   and jfm.del_state = 0" +
                " where inout.type = 1" +
                "   and inout.figure_id = #{activeId}  and inout.HISTORY_ID = #{historyId} order by inout.sort_id";

    }


    private String findHistoryActiveOutFileSql(Map<String, Object> map) {
        return "SELECT MODE_FILE.*," +
                "       TEMPLET.FILE_ID   AS TEMPLETFILEID," +
                "       TEMPLET.FILE_NAME AS TEMPLETFILENAME" +
                "  FROM (SELECT JF.ID FILE_ID, JF.FILE_NAME, JMF.MODE_FILE_ID" +
                "          FROM JECN_MODE_FILE_H JMF" +
                "         INNER JOIN FILE_CONTENT JF" +
                "            ON JMF.FILE_M_ID = JF.ID" +
                "         WHERE JMF.FIGURE_ID = #{activeId} AND JMF.HISTORY_ID = #{historyId}) MODE_FILE" +
                "  LEFT JOIN (SELECT JF.ID FILE_ID, JF.FILE_NAME, JMF.MODE_FILE_ID" +
                "               FROM JECN_MODE_FILE_H JMF" +
                "              INNER JOIN JECN_TEMPLET_H JT" +
                "                 ON JT.MODE_FILE_ID = JMF.MODE_FILE_ID" +
                "              INNER JOIN FILE_CONTENT JF" +
                "                 ON JT.FILE_ID = JF.ID" +
                "              WHERE JMF.FIGURE_ID = #{activeId}  AND JMF.HISTORY_ID = #{historyId}" +
                "  AND  JMF.HISTORY_ID =JT.HISTORY_ID ) TEMPLET" +
                "    ON TEMPLET.MODE_FILE_ID = MODE_FILE.MODE_FILE_ID" +
                " ORDER BY MODE_FILE.MODE_FILE_ID";
    }
}
