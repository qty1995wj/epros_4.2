package com.jecn.epros.server.bean.file;

import java.io.Serializable;

public class FileRelatedStandardT implements Serializable{
	private String id;
	private Long fileId;
	private Long standardId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public Long getStandardId() {
		return standardId;
	}
	public void setStandardId(Long standardId) {
		this.standardId = standardId;
	}
}
