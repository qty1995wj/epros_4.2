package com.jecn.epros.service.home;

import com.jecn.epros.domain.home.JoinProcess;
import com.jecn.epros.domain.home.RoleRelatedFlowInfo;
import com.jecn.epros.domain.home.TopNumberData;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface HomeService {

    /**
     * 我的主页-》我参与的流程
     *
     * @param maps
     * @return
     */
    public List<RoleRelatedFlowInfo> findMyJoinProcess(Map<String, Object> maps);

    public int findMyJoinProcessCount(Map<String, Object> maps);

    /**
     * 流程访问Top10
     *
     * @param maps
     * @return
     */
    public List<TopNumberData> findFlowVisitTopTen(Map<String, Object> map);

    /**
     * 流程合理化建议Top10
     *
     * @param maps
     * @return
     */
    public List<TopNumberData> findFlowProposeTopTen(Map<String, Object> map);


    public List<JoinProcess> getPostByUserId(Map<String, Object> maps);
}
