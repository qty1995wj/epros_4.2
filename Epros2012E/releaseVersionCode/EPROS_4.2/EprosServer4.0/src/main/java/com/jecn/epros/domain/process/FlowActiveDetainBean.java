package com.jecn.epros.domain.process;

import com.jecn.epros.domain.file.FileBaseBean;
import com.jecn.epros.util.ConfigUtils;

import java.util.List;

public class FlowActiveDetainBean {
    /**
     * 基本信息
     */
    private FlowActiveBaseBean FlowActiveBaseBean;
    /**
     * 信息化
     */
    private List<FlowActiveOnLineBean> listFlowActiveOnLineBeans;
    /**
     * 指标
     */
    private List<FlowActiveRelatedRefIndicators> listFlowActiveRelatedRefIndicators;
    /**
     * 控制点
     */
    private List<FlowActiveRelatedRiskControlPoint> listFlowActiveRelatedRiskControlPoint;
    /**
     * 标准
     */
    private List<FlowActiveRelatedStandardBean> listFlowActiveRelatedStandardBean;
    /**
     * 操作规范文件
     */
    private List<FileBaseBean> listFileBaseBeanOperationFiles;
    /**
     * 输入规范文件
     */
    private List<FileBaseBean> listFileBaseBeanInFiles;

    /**
     * 输出规范文件
     */
    private List<FlowActiveOutBean> listFlowActiveOutBeans;

    private boolean showRiskAndStandard;

    private boolean  useNewInout ;

    public boolean isUseNewInout() {
        this.useNewInout = ConfigUtils.useNewInout();
        return useNewInout;
    }

    public void setUseNewInout(boolean useNewInout) {
        this.useNewInout = useNewInout;
    }

    public FlowActiveBaseBean getFlowActiveBaseBean() {
        return FlowActiveBaseBean;
    }

    public void setFlowActiveBaseBean(FlowActiveBaseBean flowActiveBaseBean) {
        FlowActiveBaseBean = flowActiveBaseBean;
    }

    public List<FlowActiveOnLineBean> getListFlowActiveOnLineBeans() {
        return listFlowActiveOnLineBeans;
    }

    public void setListFlowActiveOnLineBeans(List<FlowActiveOnLineBean> listFlowActiveOnLineBeans) {
        this.listFlowActiveOnLineBeans = listFlowActiveOnLineBeans;
    }

    public List<FlowActiveRelatedRefIndicators> getListFlowActiveRelatedRefIndicators() {
        return listFlowActiveRelatedRefIndicators;
    }

    public void setListFlowActiveRelatedRefIndicators(
            List<FlowActiveRelatedRefIndicators> listFlowActiveRelatedRefIndicators) {
        this.listFlowActiveRelatedRefIndicators = listFlowActiveRelatedRefIndicators;
    }

    public List<FlowActiveRelatedRiskControlPoint> getListFlowActiveRelatedRiskControlPoint() {
        return listFlowActiveRelatedRiskControlPoint;
    }

    public void setListFlowActiveRelatedRiskControlPoint(
            List<FlowActiveRelatedRiskControlPoint> listFlowActiveRelatedRiskControlPoint) {
        this.listFlowActiveRelatedRiskControlPoint = listFlowActiveRelatedRiskControlPoint;
    }

    public List<FlowActiveRelatedStandardBean> getListFlowActiveRelatedStandardBean() {
        return listFlowActiveRelatedStandardBean;
    }

    public void setListFlowActiveRelatedStandardBean(
            List<FlowActiveRelatedStandardBean> listFlowActiveRelatedStandardBean) {
        this.listFlowActiveRelatedStandardBean = listFlowActiveRelatedStandardBean;
    }

    public List<FileBaseBean> getListFileBaseBeanOperationFiles() {
        return listFileBaseBeanOperationFiles;
    }

    public void setListFileBaseBeanOperationFiles(List<FileBaseBean> listFileBaseBeanOperationFiles) {
        this.listFileBaseBeanOperationFiles = listFileBaseBeanOperationFiles;
    }

    public List<FileBaseBean> getListFileBaseBeanInFiles() {
        return listFileBaseBeanInFiles;
    }

    public void setListFileBaseBeanInFiles(List<FileBaseBean> listFileBaseBeanInFiles) {
        this.listFileBaseBeanInFiles = listFileBaseBeanInFiles;
    }

    public List<FlowActiveOutBean> getListFlowActiveOutBeans() {
        return listFlowActiveOutBeans;
    }

    public void setListFlowActiveOutBeans(List<FlowActiveOutBean> listFlowActiveOutBeans) {
        this.listFlowActiveOutBeans = listFlowActiveOutBeans;
    }

    public boolean isShowRiskAndStandard() {
        return showRiskAndStandard;
    }

    public void setShowRiskAndStandard(boolean showRiskAndStandard) {
        this.showRiskAndStandard = showRiskAndStandard;
    }


}
