package com.jecn.epros.server.service.system.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.system.IProcessRuleTypeDao;
import com.jecn.epros.server.service.system.IProcessRuleTypeService;

@Transactional
public class ProcessRuleTypeServiceImpl extends
		AbsBaseService<ProceeRuleTypeBean, Long> implements
		IProcessRuleTypeService {

	private IProcessRuleTypeDao processRuleTypeDao;

	public IProcessRuleTypeDao getProcessRuleTypeDao() {
		return processRuleTypeDao;
	}

	public void setProcessRuleTypeDao(IProcessRuleTypeDao processRuleTypeDao) {
		this.processRuleTypeDao = processRuleTypeDao;
		this.baseDao = processRuleTypeDao;
	}

	@Override
	public void delteFlowArributeType(List<Long> typeIds) throws Exception {
		String hql = "delete ProceeRuleTypeBean  where typeId in "
				+ JecnCommonSql.getIds(typeIds);
		processRuleTypeDao.execteHql(hql);
		hql = "update  JecnFlowBasicInfoT set typeId = -1 where typeId in"
				+ JecnCommonSql.getIds(typeIds);
		processRuleTypeDao.execteHql(hql);
		hql = "update  JecnFlowBasicInfo set typeId = -1 where typeId in"
				+ JecnCommonSql.getIds(typeIds);
		processRuleTypeDao.execteHql(hql);
		hql = "update  RuleT set typeId = -1 where typeId in"
				+ JecnCommonSql.getIds(typeIds);
		processRuleTypeDao.execteHql(hql);
		hql = "update  Rule set typeId = -1 where typeId in"
				+ JecnCommonSql.getIds(typeIds);
		processRuleTypeDao.execteHql(hql);
	}

	@Override
	public List<ProceeRuleTypeBean> getFlowAttributeType() throws Exception {
		String hql = "from ProceeRuleTypeBean";
		List<ProceeRuleTypeBean> listTypeBean = processRuleTypeDao.listHql(hql);
		return listTypeBean;
	}

	@Override
	public void updateFlowType(ProceeRuleTypeBean proceeRuleTypeBean)
			throws Exception {
		processRuleTypeDao.update(proceeRuleTypeBean);
	}
}
