package com.jecn.epros.server.service.vatti.buss;

import java.util.Set;
import org.apache.log4j.Logger;

/**
 * Domino操作类
 * 
 * @author weidp
 * @date： 日期：2014-5-4 时间：下午05:47:06
 */
public class DominoOperation {

	private static Logger log = Logger.getLogger(DominoOperation.class);

	/**
	 * 插入数据至Domino
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-27 时间：上午11:38:31
	 * @param relateId
	 */
	public static void importDataToDomino(Long id, String type) {
	}

	/**
	 * 按照类型和Id删除Domino中的一条数据
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-27 时间：下午03:31:39
	 * @param id
	 *            制度或流程Id
	 */
	public static void deleteDataFromDomino(Long id, String type) {
	}

	/**
	 * 删除多条Domino数据
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-28 时间：上午11:55:06
	 * @param ids
	 *            制度或流程Id集合
	 * @param type
	 */
	public static void deleteDataFromDomino(Set<Long> ids, String type) {

	}
}
