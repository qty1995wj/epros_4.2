package com.jecn.epros.server.email.bean;

/**
 * 邮件主表与附件的中间表
 */

public class JecnEmailRelatedAttachment implements java.io.Serializable {

	private String id;
	/** 附件表**/
	private String attachmentId;
	/** 邮件主表**/
	private String emailId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

}