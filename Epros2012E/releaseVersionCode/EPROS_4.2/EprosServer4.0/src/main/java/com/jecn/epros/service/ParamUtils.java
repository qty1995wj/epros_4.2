package com.jecn.epros.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ParamUtils {

    public static final String DEFAULT_COLLECTION_KEY = "collection";

    public static Map<String, Object> collection2Map(String key, Collection<?> collection) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(key, collection);
        return map;
    }

}
