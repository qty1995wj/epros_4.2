package com.jecn.epros.server.dao.task;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.task.JecnTaskApplicationNew;
import com.jecn.epros.server.bean.task.JecnTaskApprovePeopleConn;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskForRecodeNew;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.bean.task.temp.JecnTaskFileTemporary;
import com.jecn.epros.server.common.IBaseDao;

public interface IJecnAbstractTaskDao extends IBaseDao<JecnTaskBeanNew, Long> {

	/**
	 * 获得任务的审批记录
	 * 
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTaskForRecodeNew> getJecnTaskForRecodeNewListByTaskId(Long taskId) throws Exception;

	/**
	 * 获得当前任务的审批记录的条数
	 * 
	 * @param taskId
	 *            当前任务ID
	 * @return int 当前任务的审批记录总数
	 * @throws Exception
	 */
	public int getCurTaskRecordCount(Long taskId) throws Exception;

	/**
	 * 获得处于上传文件任务中的文件
	 * 
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	public List<String> getUploadFileTaskFileNames(Long fileId) throws Exception;

	/**
	 * 判断该文件是否存在
	 * 
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public boolean checkFileExists(String fileName, Long fileId) throws Exception;

	/**
	 * 通过userId集合找到peopleId
	 * 
	 * @param set
	 * @return
	 * @throws DaoException
	 */
	public List<Object[]> getPeopleIdsByUserIds(Set<Long> set) throws Exception;

	/**
	 * 验证任务当前节点审批人是否存在
	 * 
	 * @param taskId
	 *            任务主键Id
	 * @return
	 * @throws Exception
	 */
	public int findTaskStatePeople(Long taskId) throws Exception;

	/**
	 * 获得任务审批操作人
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param state
	 *            任务阶段
	 * @return List<Long> 当前任务阶段的审批人
	 * @throws Exception
	 */
	public List<Long> getJecnTaskApprovePeopleConnListByTaskIdAndState(long stageId) throws Exception;

	/**
	 * 获得任务的记录条数
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @return int任务的记录条数
	 * @throws Exception
	 */
	public int getSortRecord(Long taskId) throws Exception;

	/**
	 * 验证当前登录人是否为任务审批人
	 * 
	 * @param peopleId
	 *            登录人人员主键ID
	 * @param taskId
	 *            任务主键ID
	 * @return int >1 是当前任务审批人；0：不是当前任务审批人
	 * @throws DaoException
	 */
	public int getPeopleCountByLoginPeople(Long peopleId, Long taskId, int taskElseState, int approveCounts)
			throws Exception;

	/**
	 * 验证指定人员ID是否存在
	 * 
	 * @param peopleId
	 *            人员主键ID
	 * @return 1：存在，0 ：不存在
	 * @throws Exception
	 */
	public int isExitPeople(Long peopleId) throws Exception;

	/**
	 * 打开试用行报告
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnTaskFileTemporary downLoadTaskRunFile(Long id) throws Exception;

	/**
	 * 验证指定人员ID是否存在
	 * 
	 * @param peopleId
	 *            人员主键ID
	 * @return int：根据人员ID集合获取存在的人员(JECN_USER)数
	 * @throws Exception
	 */
	public int isExitPeoples(Set<Long> peopleIds) throws Exception;

	/**
	 * 获得任务显示项
	 * 
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	public JecnTaskApplicationNew getJecnTaskApplicationNew(Long taskId) throws Exception;

	/**
	 * 根据任务ID获取任务各阶段审批人信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @return 各阶段审批人集合 List<Object[]> 0：任务阶段；1：人员真实姓名2：人员主键ID;3:是否删除1:删除
	 * @throws Exception
	 */
	public List<Object[]> getListJecnTaskApprovePeopleConn(Long taskId) throws Exception;


	/**
	 * 获取任务各阶段信息表的数据
	 * 
	 * @author weidp
	 * @date 2014-7-3 下午01:48:51
	 * @param taskId
	 *            任务Id
	 * @return
	 */
	public List<JecnTaskStage> getJecnTaskStageList(Long taskId);

	/**
	 * 删除任务各阶段联系人数据
	 * 
	 * @author weidp
	 * @date 2014-7-4 下午12:33:47
	 * @param taskId
	 */
	public void deleteJecnTaskPeopleConnByTaskId(Long taskId);

	/**
	 * 删除任务各阶段联系人数据
	 * 
	 * @author weidp
	 * @date 2014-7-4 下午12:33:47
	 * @param taskId
	 */
	public void deleteJecnTaskPeopleConnByTaskIdAndStageId(Long stageId);

	
	/**
	 * 返回操作[获取除会审阶段之外的阶段联系人]
	 * @param stageId
	 * @return
	 */
	public JecnTaskApprovePeopleConn getTaskPeopleConnByStageId(long stageId);

	public JecnTaskForRecodeNew getJecnTaskForRecodeNew(Long recordId);

}
