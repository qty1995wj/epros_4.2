package com.jecn.epros.server.download.word;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.download.HeaderDocBean;
import com.jecn.epros.server.bean.download.ProcessMapDownloadBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnUtil;
import com.lowagie.text.Document;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.rtf.RtfWriter2;

/****
 * 流程地图下载
 * 
 * @author admin
 * 
 */
public class ProcessMapDownload {
	private static final Logger log = Logger
			.getLogger(ProcessMapDownload.class);

	public static String create(ProcessMapDownloadBean processMapDownloadBean)
			throws MalformedURLException, IOException, Exception {
		// 标题
		Document document = null;
		FileOutputStream fileOutputStream = null;
		String filePath = null;
		try {
			filePath = JecnFinal.getAllTempPath(JecnFinal.saveTempFilePath());
			fileOutputStream = new FileOutputStream(filePath);
			document = new Document(PageSize.A4, 80, 80, 80, 100);
			RtfWriter2.getInstance(document, fileOutputStream);
			document.open();
			// 生成页眉
			HeaderDocBean headerDocBean = new HeaderDocBean();
			// 名称
			headerDocBean.setName(processMapDownloadBean.getFlowName());
			// 是否公开
			headerDocBean.setIsPublic(processMapDownloadBean.getFlowIsPublic());
			// 编号
			headerDocBean.setInputNum(processMapDownloadBean.getFlowInputNum());
			// 版本
			headerDocBean.setVersion(processMapDownloadBean.getFlowVersion());
			// 页眉图片
			Image loginImage = JecnFinal.getImage("logo01.jpg");
			DownloadUtil.getHeaderDoc(document, headerDocBean, loginImage);
			// 生成页脚
			DownloadUtil.getFooterDoc(document);

			String line = "\n\n\n\n";
			document.add(new Paragraph(line));
			// 公司名称
			document.add(DownloadUtil.ST_X3_MIDDLE(processMapDownloadBean
					.getCompanyName()));

			String newline = "\n\n\n\n\n\n\n";
			document.add(new Paragraph(newline));

			// 流程名称
			document.add(DownloadUtil.ST_X2_BOLD_MIDDLE(processMapDownloadBean
					.getFlowName()));

			DownloadUtil.getReturnTable(document, processMapDownloadBean
					.getPeopleList(), processMapDownloadBean.getFlowInputNum(),
					processMapDownloadBean.getFlowVersion());

			int count = 1;
			for (JecnConfigItemBean JecnConfigItemBean : processMapDownloadBean
					.getJecnConfigItemBean()) {
				if ("0".equals(JecnConfigItemBean.getValue())) {// 是否显示
					continue;
				}
				String titleName = count + "、" + JecnConfigItemBean.getName();
				if ("processMapPurpose".equals(JecnConfigItemBean.getMark())) { // 目的
					Paragraph title = DownloadUtil.getParagraphTitle(titleName);
					document.add(title);
					String content = "";
					if (processMapDownloadBean.getFlowAim() != null
							&& !"".equals(processMapDownloadBean.getFlowAim())) {
						content = processMapDownloadBean.getFlowAim();
					} else {
						content = "\t" + JecnUtil.getValue("none");
					}
					document.add(DownloadUtil.getContent(content));
					document.add(new Paragraph());
					count++;
				} else if ("processMapScope".equals(JecnConfigItemBean
						.getMark())) {// 范围
					Paragraph title = DownloadUtil.getParagraphTitle(titleName);
					document.add(title);
					String content = "";
					if (processMapDownloadBean.getFlowArea() != null
							&& !"".equals(processMapDownloadBean.getFlowArea())) {
						content = processMapDownloadBean.getFlowArea();
					} else {
						content = "\t" + JecnUtil.getValue("none");
					}
					document.add(DownloadUtil.getContent(content));
					document.add(new Paragraph());
					count++;
				} else if ("processMapTerms".equals(JecnConfigItemBean
						.getMark())) {// 术语定义
					Paragraph title = DownloadUtil.getParagraphTitle(titleName);
					document.add(title);
					String content = "";
					if (processMapDownloadBean.getFlowNounDefine() != null
							&& !"".equals(processMapDownloadBean
									.getFlowNounDefine())) {
						content = processMapDownloadBean.getFlowNounDefine();
					} else {
						content = "\t" + JecnUtil.getValue("none");
					}
					document.add(DownloadUtil.getContent(content));
					document.add(new Paragraph());
					count++;
				} else if ("processMapInput".equals(JecnConfigItemBean
						.getMark())) {// 输入
					Paragraph title = DownloadUtil.getParagraphTitle(titleName);
					document.add(title);
					String content = "";
					if (processMapDownloadBean.getFlowInput() != null
							&& !""
									.equals(processMapDownloadBean
											.getFlowInput())) {
						content = processMapDownloadBean.getFlowInput();
					} else {
						content = "\t" + JecnUtil.getValue("none");
					}
					document.add(DownloadUtil.getContent(content));
					document.add(new Paragraph());
					count++;
				} else if ("processMapOutput".equals(JecnConfigItemBean
						.getMark())) {// 输出
					Paragraph title = DownloadUtil.getParagraphTitle(titleName);
					document.add(title);
					String content = "";
					if (processMapDownloadBean.getFlowOutput() != null
							&& !"".equals(processMapDownloadBean
									.getFlowOutput())) {
						content = processMapDownloadBean.getFlowOutput();
					} else {
						content = "\t" + JecnUtil.getValue("none");
					}
					document.add(DownloadUtil.getContent(content));
					document.add(new Paragraph());
					count++;
				} else if ("processMap".equals(JecnConfigItemBean.getMark())) {// 流程地图
					Paragraph title = DownloadUtil.getParagraphTitle(titleName);
					document.add(title);
					if (processMapDownloadBean.getImg() != null) {
						// 图像宽度大小
						float imgWidth = processMapDownloadBean.getImg()
								.getWidth();
						float imgHeight = processMapDownloadBean.getImg()
								.getHeight();
						if (imgWidth == 0 || imgHeight == 0) {
							// 除数不能为零
							continue;
						}
						float docWidth = document.getPageSize().getWidth() - 160;
						float docHeight = document.getPageSize().getHeight() - 200;
						int widhtScale = (int) ((docWidth / imgWidth) * 100) + 1;
						int heightScale = (int) ((docHeight / imgHeight) * 100) + 1;
						int scale = 0;
						if (widhtScale > heightScale) {
							scale = heightScale;
						} else {
							scale = widhtScale;
						}
						// 图像放大缩小倍数
						processMapDownloadBean.getImg().scalePercent(scale);
						document.add(processMapDownloadBean.getImg());
						document.add(new Paragraph());
					} else {
						String content = "\t"
								+ processMapDownloadBean.getFlowName();
						document.add(DownloadUtil.getContent(content));
						document.add(new Paragraph());
					}
					count++;
				} else if ("processMapInstructions".equals(JecnConfigItemBean
						.getMark())) {// 步骤说明
					Paragraph title = DownloadUtil.getParagraphTitle(titleName);
					document.add(title);
					String content = "";
					if (processMapDownloadBean.getFlowShowStep() != null
							&& !"".equals(processMapDownloadBean
									.getFlowShowStep())) {
						content = processMapDownloadBean.getFlowShowStep();
					} else {
						content = "\t" + JecnUtil.getValue("none");
					}
					document.add(DownloadUtil.getContent(content));
					document.add(new Paragraph());
					count++;
				} else if ("processMapAttachment".equals(JecnConfigItemBean
						.getMark())) {// 附件
					Paragraph title = DownloadUtil.getParagraphTitle(titleName);
					document.add(title);
					String content = "";
					if (processMapDownloadBean.getFileName() != null
							&& !"".equals(processMapDownloadBean.getFileName())) {
						content = processMapDownloadBean.getFileName();
					} else {
						content = "\t" + JecnUtil.getValue("none");
					}
					document.add(DownloadUtil.getContent(content));
					// document.add(new Paragraph());
					count++;
				}
			}
			document.close();
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (Exception ex) {
					log.error("关闭document", ex);
				}
			}

			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (Exception ex) {
					log.error("关闭fileOutputStream", ex);
				}
			}
		}
		return filePath;
	}
}