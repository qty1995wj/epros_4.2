package epros.designer.gui.file;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.JecnFileContent;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/***
 * 文件版本信息
 * 
 * @author 2012-05-30
 * 
 */
public class FileVersionDialog extends JPanel {

	private static Logger log = Logger.getLogger(FileVersionDialog.class);
	private JecnTreeNode selectNode = null;
	/** 按钮Panel */
	private JPanel buttonPanel = null;

	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = null;

	/** 删除 */
	private JButton delBut = null;

	/** 恢复 */
	private JButton rebackBut = null;

	/** 查看 */
	private JButton searchBut = null;

	/** 信息显示Table */
	private FileVersionTable fileVersionTable = null;
	/** 验证提示 */
	private JLabel verfyLab = new JLabel();

	public FileVersionTable getFileVersion() {
		return fileVersionTable;
	}

	public void setFileVersion(FileVersionTable fileVersion) {
		this.fileVersionTable = fileVersion;
	}

	/** 设置大小 */
	private Dimension dimension = null;

	private List<JecnFileContent> versionList = new ArrayList<JecnFileContent>();

	public List<JecnFileContent> getVersionList() {
		return versionList;
	}

	public void setVersionList(List<JecnFileContent> versionList) {
		this.versionList = versionList;
	}

	// 版本信息ID
	Long listId = null;
	// 文件Id
	Long fileId = null;
	// 保存要删除的id，传到后台删除数据
	List<Long> listIds = null;

	public List<Long> getListIds() {
		return listIds;
	}

	public Long getListId() {
		return listId;
	}

	public Long getFileId() {
		return fileId;
	}

	private JTree jTree = null;

	public FileVersionDialog(JTree jTree, JecnTreeNode selectNode) {
		this.jTree = jTree;
		this.selectNode = selectNode;
		// 获取文件ID
		fileId = selectNode.getJecnTreeBean().getId();
		verfyLab.setForeground(Color.red);
		initCompotents();
		initLayout();
		// 获取文件版本信息
		try {
			versionList = ConnectionPool.getFileAction().getJecnFileContents(fileId);
			// ConnectionPool.getRuleAction().getChildRuleDirs(pid, projectId)
		} catch (Exception e) {
			log.error("FileVersionDialog FileVersionDialog is error", e);
		}
		fileVersionTable = new FileVersionTable();
		resultScrollPane.setViewportView(fileVersionTable);
		// 选中一条文控信息 ，双击表格，打开文控编辑框
		fileVersionTable.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					searchButPerformed();
				}
			}
		});
	}

	/***
	 * 初始化组件
	 */
	private void initCompotents() {
		// 按钮Panel
		buttonPanel = new JPanel();

		// 结果滚动面板
		resultScrollPane = new JScrollPane();

		// 删除
		delBut = new JButton(JecnProperties.getValue("deleteBtn"));

		// 恢复
		rebackBut = new JButton(JecnProperties.getValue("recyBtn"));

		// 查看
		searchBut = new JButton(JecnProperties.getValue("searchBtn"));

		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setSize(600, 400);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		dimension = new Dimension(600, 320);
		resultScrollPane.setPreferredSize(dimension);
		resultScrollPane.setMaximumSize(dimension);
		resultScrollPane.setMinimumSize(dimension);

		// 删除
		delBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delButPerformed();
			}
		});
		// 恢复
		rebackBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rebackButPerformed();
			}
		});
		// 查看
		searchBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchButPerformed();
			}
		});

	}

	/***
	 * 布局
	 */
	private void initLayout() {
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);

		// 文件使用情况表格
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		this.add(resultScrollPane, c);
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(delBut);
		buttonPanel.add(rebackBut);
		buttonPanel.add(searchBut);

		if (JecnTreeCommon.isDesigner(selectNode)) {// 设计者不可以删除版本记录
			delBut.setEnabled(false);
		}
	}

	/***
	 * 删除
	 */
	private void delButPerformed() {
		verfyLab.setText("");
		int[] selectRows = fileVersionTable.getSelectedRows();
		if (selectRows.length == 0) {
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		// 保存要删除的id，传到后台删除数据
		listIds = new ArrayList<Long>();

		for (int i = selectRows.length - 1; i >= 0; i--) {
			// 版本信息ID
			listIds.add(Long.valueOf(fileVersionTable.getModel().getValueAt(selectRows[i], 0).toString()));
		}
		try {
			ConnectionPool.getFileAction().deleteVersion(listIds, fileId, JecnConstants.getUserId());
			verfyLab.setText(JecnProperties.getValue("deleteSuccess"));
		} catch (Exception e) {
			log.error("FileVersionDialog delButPerformed is error", e);
			verfyLab.setText(JecnProperties.getValue("delfailure"));
			return;
		}
		// 删除表格数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) fileVersionTable.getModel()).removeRow(selectRows[i]);
		}

	}

	/***
	 * 恢复
	 */
	private void rebackButPerformed() {
		verfyLab.setText("");

		int[] selectRows = fileVersionTable.getSelectedRows();
		if (selectRows.length != 1) {
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}

		boolean isDesign = JecnTreeCommon.isDesigner(selectNode);
		if (isDesign && !JecnTool.isBeInTask(selectNode)) {// 如果是设计者，并且处于任务中退出
			return;
		}
		// 版本信息ID
		listId = Long.valueOf(fileVersionTable.getModel().getValueAt(selectRows[0], 0).toString());
		try {
			String fileName = fileVersionTable.getModel().getValueAt(selectRows[0], 2).toString();
			ConnectionPool.getFileAction().updateRecovery(fileId, listId, JecnConstants.getUserId(),
					selectNode.getJecnTreeBean().isPub(), isDesign);
			selectNode.getJecnTreeBean().setUpdate(true);
			JecnTreeCommon.reNameNode(jTree, selectNode, fileName.toString());
			fileVersionTable = new FileVersionTable();
			resultScrollPane.setViewportView(fileVersionTable);
			verfyLab.setText(JecnProperties.getValue("recySuccess"));
		} catch (Exception e) {
			log.error("FileVersionDialog rebackButPerformed is error", e);
			verfyLab.setText(JecnProperties.getValue("recyLose"));
			return;
		}
	}

	/***
	 * 查看,双击表格查看版本信息
	 */
	private void searchButPerformed() {
		int[] selectRows = fileVersionTable.getSelectedRows();
		if (selectRows.length != 1) {
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		try {
			FileOpenBean fileOpenBean = ConnectionPool.getFileAction().openVersion(
					Long.valueOf(fileVersionTable.getModel().getValueAt(selectRows[0], 4).toString()),
					Long.valueOf(fileVersionTable.getModel().getValueAt(selectRows[0], 0).toString()));
			JecnUtil.openFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
		} catch (Exception e) {
			log.error("FileVersionDialog searchButPerformed is error！", e);
		}

	}

	/**
	 * 文件版本表格显示信息
	 * 
	 * @param list
	 * @return
	 */
	public Vector<Vector<Object>> updateTableContent(List<JecnFileContent> list) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		int conunt = 0;
		for (JecnFileContent jecnFileContent : list) {
			Vector<Object> data = new Vector<Object>();
			conunt++;
			data.add(jecnFileContent.getId());
			// 序号
			data.add(conunt);
			// 名称
			data.add(jecnFileContent.getFileName());
			// 日期
			data.add(jecnFileContent.getUpdateTime());
			// 文件ID
			data.add(jecnFileContent.getFileId());
			vector.add(data);
		}
		return vector;
	}

	class FileVersionTable extends JTable {

		FileVersionTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
			TableColumnModel columnModel = this.getColumnModel();
			TableColumn tableColumn = columnModel.getColumn(1);
			tableColumn.setMinWidth(60);
			tableColumn.setMaxWidth(60);
			DefaultTableCellRenderer render = new DefaultTableCellRenderer();
			render.setHorizontalAlignment(SwingConstants.CENTER);
			tableColumn.setCellRenderer(render);
			tableColumn = columnModel.getColumn(3);
			tableColumn.setMinWidth(150);
			tableColumn.setMaxWidth(150);
			tableColumn.setCellRenderer(render);
		}

		public FileVersionTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("id"));
			/*
			 * 序号
			 */
			title.add(JecnProperties.getValue("sort"));
			/*
			 * 名称
			 */
			title.add(JecnProperties.getValue("name"));
			/*
			 * 日期
			 */
			title.add(JecnProperties.getValue("date"));
			/*
			 * 文件Id
			 */
			title.add(JecnProperties.getValue("fileId"));
			return new FileVersionTableMode(updateTableContent(versionList), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {

			return new int[] { 0, 4 };
		}

		class FileVersionTableMode extends DefaultTableModel {
			public FileVersionTableMode(Vector<Vector<Object>> data, Vector<String> title) {
				super(data, title);

			}

			@Override
			public Class getColumnClass(int columnIndex) {
				return getValueAt(0, columnIndex).getClass();
			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return false;
			}

		}

	}
}
