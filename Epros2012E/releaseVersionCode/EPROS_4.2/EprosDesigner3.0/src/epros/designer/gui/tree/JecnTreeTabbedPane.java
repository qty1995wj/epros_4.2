package epros.designer.gui.tree;

import javax.swing.JTabbedPane;

import epros.draw.gui.ui.JecnWindowsTabbedPaneUI;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 资源管理树和流程模板树的容器
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTreeTabbedPane extends JTabbedPane {

	public JecnTreeTabbedPane() {
		initComponents();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void initComponents() {
		// 选项卡过多时出左右移动的
		this.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		this.setBorder(null);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setUI(new JecnWindowsTabbedPaneUI());
	}

}
