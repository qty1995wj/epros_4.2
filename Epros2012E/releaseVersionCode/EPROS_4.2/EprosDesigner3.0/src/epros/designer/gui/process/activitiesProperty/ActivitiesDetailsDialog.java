package epros.designer.gui.process.activitiesProperty;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import epros.designer.util.JecnProperties;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;

/*******************************************************************************
 * 活动明细
 * 
 * @author 2012-07-27
 * 
 */
public class ActivitiesDetailsDialog extends JecnDialog {
	private ActivitiesDetailsJpanel mainPanel;

	/** 确定 */
	protected JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	protected JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	/** 活动的数据 */
	protected JecnActiveData jecnActiveData;
	/** 点击的图形 */
	protected JecnBaseActiveFigure baseActiveFigure;

	public ActivitiesDetailsDialog() {
		initializationInterface();
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelPerformed();
				return false;
			}
		});
	}

	public ActivitiesDetailsDialog(JecnActiveData jecnActiveData, JecnBaseActiveFigure baseActiveFigure) {
		this.jecnActiveData = jecnActiveData;
		this.baseActiveFigure = baseActiveFigure;
		// 初始化界面
		initializationInterface();
		initLayout();
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelPerformed();
				return false;
			}
		});
	}

	private void initializationInterface() {
		// 设置窗体大小
		this.setSize(700, 550);
		this.setTitle(JecnProperties.getValue("activeDes"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		// this.setResizable(false);

		this.setModal(true);

		mainPanel = new ActivitiesDetailsJpanel(jecnActiveData, baseActiveFigure);

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
	}

	/***************************************************************************
	 * 确定
	 */
	protected void okButPerformed() {
		boolean result = mainPanel.saveData();
		if (!result) {
			return;
		}
		this.dispose();
	}

	/***************************************************************************
	 * 取消
	 */
	protected void cancelPerformed() {
		mainPanel.getJecnActiveData().getListFigureInTs();
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int tipAction = dialogCloseBeforeMsgTipAction();
			if (2 == tipAction) {// 信息提示：true：需要执行关闭
				// false：不要执行关闭
				// this.setVisible(false);
				this.okButPerformed();
			} else if (1 == tipAction) {
				this.setVisible(false);
			}
		}
	}

	public boolean isUpdate() {
		return mainPanel.isUpdate();
	}

	private void initLayout() {
		Container contentPane = this.getContentPane();

		mainPanel.getButtonPanel().add(okBut);
		mainPanel.getButtonPanel().add(cancelBut);

		contentPane.add(mainPanel.getButtonPanel(), BorderLayout.SOUTH);
		contentPane.add(mainPanel.getTabPanel(), BorderLayout.CENTER);
	}

}
