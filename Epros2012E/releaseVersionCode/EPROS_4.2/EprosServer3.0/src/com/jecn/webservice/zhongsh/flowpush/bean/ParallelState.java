package com.jecn.webservice.zhongsh.flowpush.bean;

public class ParallelState {
	private String guid;
	private String splitStateGUID;
	private String joinStateGUID;
	/** 并行 */
	private String parallelType = "ANY";
	private String condition = "";;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getSplitStateGUID() {
		return splitStateGUID;
	}

	public void setSplitStateGUID(String splitStateGUID) {
		this.splitStateGUID = splitStateGUID;
	}

	public String getJoinStateGUID() {
		return joinStateGUID;
	}

	public void setJoinStateGUID(String joinStateGUID) {
		this.joinStateGUID = joinStateGUID;
	}

	public String getParallelType() {
		return parallelType;
	}

	public void setParallelType(String parallelType) {
		this.parallelType = parallelType;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}
}
