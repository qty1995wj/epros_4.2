package epros.designer.gui.system.listener;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class SelectChangeEvent {
	private List<JecnTreeBean> result;

	public SelectChangeEvent(List<JecnTreeBean> result) {
		this.result = result;
	}

	public List<JecnTreeBean> getResult() {
		return result;
	}

	public void setResult(List<JecnTreeBean> result) {
		this.result = result;
	}

}
