package epros.designer.gui.popedom.role;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;

public class HighEfficiencyRoleTree extends JecnHighEfficiencyTree {

	private static Logger log = Logger.getLogger(HighEfficiencyRoleTree.class);

	/** 当前项目根节点下的角色目录和角色 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/** 默认目录和角色 */
	private List<JecnTreeBean> listDefault = new ArrayList<JecnTreeBean>();

	/** 角色对话框 */
	private RoleManageDialog jecnManageDialog = null;

	public HighEfficiencyRoleTree(RoleManageDialog jecnManageDialog) {
		this.jecnManageDialog = jecnManageDialog;
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RoleTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			listDefault = JecnRoleCommon.getDefaultRoleTreeBeans();
			list = JecnRoleCommon.getRoleTreeBeans(0L);
		} catch (Exception e) {
			log.error("HighEfficiencyRoleTree getTreeModel is error！", e);
			return null;
		}
		return JecnRoleCommon.getTreeModel(listDefault, list);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		if (jecnManageDialog != null) {
			JecnRoleCommon.treeMousePressed(evt, this, jecnManageDialog);
		}
	}

}
