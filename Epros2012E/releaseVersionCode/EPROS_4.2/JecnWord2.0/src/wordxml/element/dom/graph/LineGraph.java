package wordxml.element.dom.graph;

import java.awt.Color;
import java.util.UUID;

import wordxml.constant.Constants.LineStyle;
import wordxml.element.bean.common.PositionBean;

/***
 * <w:pict> <v:line id="1"
 * style="position:absolute;left:0;text-align:left;z-index:1"
 * from="-12.3pt,69.75pt" to="479.9pt,69.75pt" strokecolor="red"
 * strokeweight="1pt"> <v:stroke linestyle="thickThin"/> </v:line> </w:pict>
 */
public class LineGraph extends WordGraph {

	/**** 布局定位bean ****/
	private PositionBean positionBean;
	/*** id ****/
	private final String id;
	/*** 线的颜色 *****/
	private Color strokecolor;
	/** 线条的粗细单位磅值 **/
	private double strokeweight;
	/** 线条类型 默认单线条 **/
	private LineStyle lineStyle = LineStyle.single;
	/*** 线的开始位置 距离顶部的距离 单位 磅值 ******/
	private float from_x;
	private float from_y;

	/*** 线的结束位置 距离顶部的距离 单位 磅值 ******/
	private float to_x;
	private float to_y;

	public LineGraph(LineStyle lineStyle, double strokeweight) {
		this.id = UUID.randomUUID().toString();
		this.lineStyle = lineStyle;
		this.strokeweight = strokeweight;
	}

	public String getId() {
		return id;
	}

	public PositionBean getPositionBean() {
		return positionBean;
	}

	public void setPositionBean(PositionBean positionBean) {
		this.positionBean = positionBean;
	}

	public Color getStrokecolor() {
		return strokecolor;
	}

	public void setStrokecolor(Color strokecolor) {
		this.strokecolor = strokecolor;
	}

	public double getStrokeweight() {
		return strokeweight;
	}

	public void setStrokeweight(double strokeweight) {
		this.strokeweight = strokeweight;
	}

	public LineStyle getLineStyle() {
		return lineStyle;
	}

	public void setLineStyle(LineStyle lineStyle) {
		this.lineStyle = lineStyle;
	}

	public float getFrom_x() {
		return from_x;
	}

	public float getFrom_y() {
		return from_y;
	}

	public float getTo_x() {
		return to_x;
	}

	public float getTo_y() {
		return to_y;
	}

	/**
	 * 设置From
	 * 
	 * @author weidp
	 * @date 2014-10-20 下午02:32:43
	 * @param x
	 * @param y
	 */
	public void setFrom(float x, float y) {
		this.from_x = x;
		this.from_y = y;
	}

	/**
	 * 设置To
	 * 
	 * @author weidp
	 * @date 2014-10-20 下午02:32:59
	 * @param x
	 * @param y
	 */
	public void setTo(float x, float y) {
		this.to_x = x;
		this.to_y = y;
	}
}
