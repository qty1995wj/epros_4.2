package com.jecn.epros.server.webBean.integration;

/**
 * 风险相关制度
 * @author ZHF
 *
 */
public class RiskRelatedRuleBean {

	/** 风险ID */
	private Long id;
	/** 制度ID */
	private Long ruleId;
	/** 制度名称 */
	private String ruleName;
	/** 制度编号 */
	private String ruleNumber;
	/** 部门 ID */
	private Long orgId;
	/** 责任部门 */
	private String orgName;
	/** 制度类别 */
	private String ruleType;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getRuleNumber() {
		return ruleNumber;
	}
	public void setRuleNumber(String ruleNumber) {
		this.ruleNumber = ruleNumber;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getRuleType() {
		return ruleType;
	}
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}
	public Long getRuleId() {
		return ruleId;
	}
	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	
}
