package epros.designer.gui.process.activitiesProperty;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.designer.gui.common.JecnTextField;
import epros.designer.util.JecnProperties;
import epros.draw.designer.JecnRefIndicatorsT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/*******************************************************************************
 * 活动明细 指标
 * 
 * @author 2012-08-17
 * 
 */
public abstract class ActiveTargetDialog extends JecnDialog implements
		CaretListener {

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();

	/** 按钮面板 */
	private JecnPanel butPanel = new JecnPanel();

	/** 添加活动KPILab */
	private JLabel activeKPILab = new JLabel(JecnProperties
			.getValue("activeKPIC"));
	/** 必填 **/
	private JLabel actRequiredLab = new JLabel(JecnProperties.getValue("required"));

	/** 活动KPIvalue */
	protected JecnTextField activeKPIField = new JecnTextField();

	/** 添加活动KPIValueLab */
	private JLabel activeKPIValueLab = new JLabel(JecnProperties
			.getValue("activeKPIValuC"));

	/** 活动KPIvalue */
	protected JecnTextField activeKPIValueField = new JecnTextField();
	/** 目标值必填* */
	private JLabel actValueRequiredLab = new JLabel(JecnProperties.getValue("required"));

	/** 验证信息 */
	protected JLabel activeTargetVerfLab = new JLabel();

	/** 确定按钮 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnProperties
			.getValue("cancelBtn"));

	/** true 是操作，false是没有操作 */
	protected boolean isOperation = false;

	/** 结果 */
	protected JecnRefIndicatorsT jecnRefIndicatorsT = null;

	protected boolean isActiveKPI = true;
	protected boolean isActiveKPIValue = true;

	public ActiveTargetDialog(JecnDialog jecnDialog) {
		super(jecnDialog, true);
		this.setSize(500, 145);
		this.setResizable(true);
		this.setTitle(getTargetTitle());
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());
		// activeKPIField.addCaretListener(this);
		// activeKPIValueField.addCaretListener(this);

		activeTargetVerfLab.setForeground(Color.red);
		actRequiredLab.setForeground(Color.red);
		actValueRequiredLab.setForeground(Color.red);

		initLayout();

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		// 确定

		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
	}

	/***************************************************************************
	 * 布局
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());
		// kpi
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(activeKPILab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(activeKPIField, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(actRequiredLab, c);

		// value
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(activeKPIValueLab, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(activeKPIValueField, c);
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(actValueRequiredLab, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(new JLabel(), c);
		// button
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		mainPanel.add(butPanel, c);
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// activeTargetVerfLab
		// activeTargetVerfLab.setText("阿凡达睡觉了发动机法律的手机分离司法局领导");
		butPanel.add(activeTargetVerfLab);
		butPanel.add(okBut);
		butPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);

	}

	@Override
	public void caretUpdate(CaretEvent e) {

	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelButPerformed() {
		// 关闭窗体
		this.dispose();
	}

	/***************************************************************************
	 * 确定
	 */
	protected abstract void okButPerformed();
//		// 验证活动KPI不能为空
//		if ("".equals(this.activeKPIField.getText())) {
//			this.activeTargetVerfLab.setText(JecnProperties
//					.getValue("activeKPINotnull"));
//			return;
//		} else if (Tool.checkNameMaxLength(this.activeKPIField.getText())) {
//			// 名称不能超过122个字符或61个汉字
//			this.activeTargetVerfLab.setText(JecnProperties.getValue(
//					"activeKPIC").substring(0,
//					JecnProperties.getValue("activeKPIC").length() - 1)
//					+ JecnProperties
//					.getValue("lengthNotBaiOut"));
//		}
//
//		// 验证目标值不能为空
//		if ("".equals(this.activeKPIValueField.getText())) {
//			this.activeTargetVerfLab.setText(JecnProperties
//					.getValue("activeKPIValueNotnull"));
//			return;
//		} else if (Tool.checkNameMaxLength(this.activeKPIValueField.getText())) {
//			// 名称不能超过122个字符或61个汉字
//			this.activeTargetVerfLab.setText(JecnProperties.getValue(
//					"activeKPIValuC").substring(0,
//					JecnProperties.getValue("activeKPIValuC").length() - 1)
//					+ JecnProperties.getValue("lengthNotBaiOut"));
//		}
//		this.activeTargetVerfLab.setText("");
//		jecnRefIndicatorsT = new JecnRefIndicatorsT();
//		jecnRefIndicatorsT.setIndicatorName(activeKPIField.getText());
//		jecnRefIndicatorsT.setIndicatorValue(activeKPIValueField.getText());
//		isOperation = true;
//		this.dispose();
//	}

	// 获取面板标题
	public abstract String getTargetTitle();

	public boolean isOperation() {
		return isOperation;
	}

	public void setOperation(boolean isOperation) {
		this.isOperation = isOperation;
	}

	public JecnRefIndicatorsT getJecnRefIndicatorsT() {
		return jecnRefIndicatorsT;
	}

	public void setJecnRefIndicatorsT(JecnRefIndicatorsT jecnRefIndicatorsT) {
		this.jecnRefIndicatorsT = jecnRefIndicatorsT;
	}

	public JecnTextField getActiveKPIField() {
		return activeKPIField;
	}

	public void setActiveKPIField(JecnTextField activeKPIField) {
		this.activeKPIField = activeKPIField;
	}

	public JecnTextField getActiveKPIValueField() {
		return activeKPIValueField;
	}

	public void setActiveKPIValueField(JecnTextField activeKPIValueField) {
		this.activeKPIValueField = activeKPIValueField;
	}

}
