//流程文档下载数统计
function processDocumentNumberGrid() {
	processDocumentNumberGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "processDocumentNumber.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "flowId",
								mapping : "processWebBean.flowId"
							}, {
								name : "flowName",
								mapping : "processWebBean.flowName"
							}, {
								name : "flowIdInput",
								mapping : "processWebBean.flowIdInput"
							}, {
								name : "resPeopleName",
								mapping : "processWebBean.resPeopleName"
							}, {
								name : "orgName",
								mapping : "processWebBean.orgName"
							},{
								name : "accessCount",
								mapping : "accessCount"
							}])

			});

	processDocumentNumberGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			},{
				header : i_downloadTimes,
				dataIndex : "accessCount",
				align : "center",
				menuDisabled : true
			}

	]);
	processDocumentNumberGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processDocumentNumberGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processDocumentNumberGridPanel = new Ext.grid.GridPanel({
				id : "processDocumentNumberGridPanel_id",
				renderTo : 'processDocumentNumberGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-165,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processDocumentNumberGridStore,
				colModel : processDocumentNumberGridColumnModel,
				bbar : processDocumentNumberGridBbar
			})
	reports_index_id='reports_processDocumentNumber_id';     		
}

/**
 * 流程文档下载数统计查询
 */
function processDocumentNumberGridSearch(){
	 //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_number_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_number_id").value;
	 //开始时间
	 var startTime=document.getElementById("startTime").value;
	  //结束时间
	 var endTime=document.getElementById("endTime").value;
	 // 结束时间大于开始时间
	 if (startTime > endTime) {
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip,i_startTimeIsLessEndTime);
		return;
	 }
	 if(resOrgId&&resOrgId!=""){
     	processDocumentNumberGridStore.setBaseParam("resOrgId", resOrgId);
     }
     if(processId&&processId!=""){
     	processDocumentNumberGridStore.setBaseParam("processId", processId);
     }
     processDocumentNumberGridStore.setBaseParam("startTime", startTime);
     processDocumentNumberGridStore.setBaseParam("endTime", endTime);
	 processDocumentNumberGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
				
}

/**
 * 流程文档下载数重置
 **/
function processDocumentNumberSearchReset(){
        document.getElementById("orgDutyName_number_id").value = "";
		document.getElementById("processName_number_id").value = "";
		document.getElementById("orgDutyId_number_id").value = "-1";
		document.getElementById("processId_number_id").value = "-1";
		document.getElementById("startTime").value = "";
		document.getElementById("endTime").value = "";
}

// 流程文档下载数下载
function processDocumentNumberDownloadExcel(){
     //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_number_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_number_id").value;
	 //开始时间
	 var startTime=document.getElementById("startTime").value;
	  //结束时间
	 var endTime=document.getElementById("endTime").value;
     window.location.href="downloadProcessDocumentNumber.action?resOrgId="+resOrgId+"&processId="+processId+"&startTime="+startTime+"&endTime="+endTime;
}