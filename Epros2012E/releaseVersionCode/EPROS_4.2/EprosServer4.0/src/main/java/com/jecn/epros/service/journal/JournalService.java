package com.jecn.epros.service.journal;

import com.jecn.epros.domain.journal.*;

import java.util.List;
import java.util.Map;

public interface JournalService {

    /**
     * 日志详情
     *
     * @param map
     * @return
     * @
     */
    public List<UserVisitsResult> getSearchLogDetailResultBeans(Map<String, Object> map);

    /**
     * 日志详情 总数
     *
     * @param map
     * @return
     * @
     */
    public int getSearchLogDetailResultBeanTotal(Map<String, Object> map);


    /**
     * 日志统计
     *
     * @param map
     * @return
     * @
     */
    public List<ResourceVisitsResult> getSearchLogStatisticsResultBeans(Map<String, Object> map);


    /**
     * 日志统计  总数
     *
     * @param map
     * @return
     * @
     */
    public int getSearchLogStatisticsResultBeanTotal(Map<String, Object> map);

    void addWebLog(Map<String, Object> map);

    List<SearchResultWebLogBean> searchNotice(Map<String, Object> map);

    int searchNoticeTotal(Map<String, Object> map);

    int getSearchLogOperateResultBeanTotal(Map<String, Object> map);

    List<UserOperateResult> getSearchLogOperateResultBeans(Map<String, Object> map);
}
