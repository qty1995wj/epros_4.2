package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

public class HessianConfigBean extends AbstractConfigBean {
	/** 端口 */
	private String port = null;
	/** 远程访问路径 */
	private String url = null;

	/** 人员对应的接口 */
	private String userIClass = null;
	/** 人员对应的接口的实现类 */
	private String userClass = null;
	/** 调用方法名称 */
	private String userMothed = null;

	/** 岗位对应的接口 */
	private String posIClass = null;
	/** 岗位对应的接口的实现类 */
	private String posClass = null;
	/** 调用方法名称 */
	private String posMothed = null;

	/** 部门对应的接口 */
	private String deptIClass = null;
	/** 部门对应的接口的实现类 */
	private String deptClass = null;
	/** 调用方法名称 */
	private String deptMothed = null;

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUserIClass() {
		return userIClass;
	}

	public void setUserIClass(String userIClass) {
		this.userIClass = userIClass;
	}

	public String getUserClass() {
		return userClass;
	}

	public void setUserClass(String userClass) {
		this.userClass = userClass;
	}

	public String getUserMothed() {
		return userMothed;
	}

	public void setUserMothed(String userMothed) {
		this.userMothed = userMothed;
	}

	public String getPosIClass() {
		return posIClass;
	}

	public void setPosIClass(String posIClass) {
		this.posIClass = posIClass;
	}

	public String getPosClass() {
		return posClass;
	}

	public void setPosClass(String posClass) {
		this.posClass = posClass;
	}

	public String getPosMothed() {
		return posMothed;
	}

	public void setPosMothed(String posMothed) {
		this.posMothed = posMothed;
	}

	public String getDeptIClass() {
		return deptIClass;
	}

	public void setDeptIClass(String deptIClass) {
		this.deptIClass = deptIClass;
	}

	public String getDeptClass() {
		return deptClass;
	}

	public void setDeptClass(String deptClass) {
		this.deptClass = deptClass;
	}

	public String getDeptMothed() {
		return deptMothed;
	}

	public void setDeptMothed(String deptMothed) {
		this.deptMothed = deptMothed;
	}

}
