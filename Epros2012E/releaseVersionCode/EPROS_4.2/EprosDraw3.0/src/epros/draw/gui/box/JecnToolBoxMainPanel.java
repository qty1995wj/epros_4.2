package epros.draw.gui.box;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.event.JecnTabbedEventProcess;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnAdjacentFigure;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程元素库
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolBoxMainPanel extends JPanel {
	/** 详细信息空闲设置 */
	private Insets noteInsets = null;
	/** 图左内容右按钮大小 */
	private Dimension leftRightBtnSize = null;
	/** 图上内容下按钮大小 */
	private Dimension topButtonBtnSize = null;
	/** 详细说明按钮大小 */
	private Dimension titleTextBtnSize = null;

	/** 资源文件管理对象 */
	private JecnResourceUtil resourceManager = null;

	/** 互斥组 */
	private ButtonGroup btnGroup = null;
	/** 流程图：指针 */
	private JecnToolBoxButton guideBtn = null;

	/** flowElemContainPanel&modelContainPanel的容器 */
	private JecnSplitPane splitPane = null;

	/** 流程元素库的容器 */
	private JecnToolBoxFlowElemContainPanel flowElemContainPanel = null;
	/** 模型的容器 */
	private JecnToolBoxModelContainPanel modelContainPanel = null;

	/** 流程图：流程元素面板 */
	private JecnToolBoxAbstractScrollPane partMapFlowElemScrollPane = null;
	/** 流程地图：流程元素面板 */
	private JecnToolBoxAbstractScrollPane totalMapFlowElemScrollPane = null;
	/** 组织图：流程元素面板 */
	private JecnToolBoxAbstractScrollPane orgMapFlowElemScrollPane = null;
	/** 流程图：模型面板 */
	private JecnTemplateScrollPane partMapModelScrollPane = null;
	/** 流程地图：模型面板 */
	private JecnTemplateScrollPane totalMapModelScrollPane = null;
	/** 组织图：模型面板 */
	private JecnTemplateScrollPane orgMapModelScrollPane = null;

	/** 当前显示的流程元素面板：流程图的库面板/流程地图库面板/组织图库面板 */
	private MapType currShowMapType = MapType.none;

	/** 选中流程元素图标 默认指针 */
	private MapElemType selectedBtnType = MapElemType.guide;

	public JecnToolBoxMainPanel() {
		// 初始化组件
		initComponent();
	}

	private void initComponent() {
		// 详细信息空闲设置
		noteInsets = new Insets(10, 14, 0, 0);
		// 图左内容右按钮大小
		leftRightBtnSize = new Dimension(JecnUIUtil.getBoxBtnWidth(), 35);
		// 图上内容下按钮大小
		topButtonBtnSize = new Dimension(80, 76);
		// 详细说明按钮大小
		titleTextBtnSize = new Dimension(250, 70);

		// 实例化资源文件管理对象
		resourceManager = JecnResourceUtil.getJecnResourceUtil();

		// 互斥组
		btnGroup = new ButtonGroup();

		// 流程元素库的容器
		flowElemContainPanel = new JecnToolBoxFlowElemContainPanel(this);
		// 模型的容器
		modelContainPanel = new JecnToolBoxModelContainPanel(this);

		// flowElemContainPanel&modelContainPanel的容器
		splitPane = new JecnSplitPane(JSplitPane.VERTICAL_SPLIT, this, flowElemContainPanel, modelContainPanel);
		// 指针
		guideBtn = new JecnToolBoxButton(this, MapElemType.guide);

		this.setLayout(new BorderLayout());
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setBorder(null);
		this.setOpaque(false);
		this.add(splitPane, BorderLayout.CENTER);

		// 上6下4
		splitPane.setResizeWeight(0.6);

		btnGroup.add(guideBtn);
	}

	/**
	 * 
	 * 初始化成指针选中
	 * 
	 */
	public void initGuide() {
		this.setSelectedBtnType(MapElemType.guide);
	}

	/**
	 * @param mapType
	 */
	public void showBoxPanel(MapType mapType) {
		// 是否显示流程设计向导（设计器专用）
		JecnDesignerProcess.getDesignerProcess().isEditFlowDesignGuide(mapType);

		if (mapType == null || currShowMapType.equals(mapType)) {
			return;
		}

		currShowMapType = mapType;

		switch (mapType) {
		case partMap:// 流程图
			flowElemContainPanel.addButtonToThis(getPartMapFlowElemScrollPane());
			// 流程图模板
			modelContainPanel.addButtonToThis(getPartMapModelScrollPane());
			break;
		case totalMap:// 流程地图
		case totalMapRelation: // 集成关系图
			flowElemContainPanel.addButtonToThis(getTotalMapFlowElemScrollPane());
			// 流程地图模板
			modelContainPanel.addButtonToThis(getTotalMapModelScrollPane());
			break;
		case orgMap:// 组织图
			flowElemContainPanel.addButtonToThis(getOrgMapFlowElemScrollPane());
			modelContainPanel.addButtonToThis(getOrgMapModelScrollPane());
			break;
		case none:
			flowElemContainPanel.removeBtnsPanel();
			modelContainPanel.removeBtnsPanel();
			break;
		default:
			break;
		}
		modelContainPanel.revalidate();
		flowElemContainPanel.revalidate();
		flowElemContainPanel.repaint();
		modelContainPanel.repaint();
	}

	/**
	 * @param mapType
	 */
	public void showModePanel(MapType mapType) {
		// 是否显示流程设计向导（设计器专用）
		JecnDesignerProcess.getDesignerProcess().isEditFlowDesignGuide(mapType);

		switch (mapType) {
		case partMap:// 流程图
			// 流程图模板
			modelContainPanel.addButtonToThis(getPartMapModelScrollPane());
			break;
		case totalMap:// 流程地图
			// 流程地图模板
			modelContainPanel.addButtonToThis(getTotalMapModelScrollPane());
			break;
		case orgMap:// 组织图
			modelContainPanel.addButtonToThis(getOrgMapModelScrollPane());
			break;
		case none:
			modelContainPanel.removeBtnsPanel();
			break;
		default:
			break;
		}
		modelContainPanel.revalidate();
		modelContainPanel.repaint();
	}

	/**
	 * 
	 * 获取流程元素当前滚动面板
	 * 
	 * @return JecnToolBoxAbstractScrollPane
	 */
	public JecnToolBoxAbstractScrollPane getCurrFlowElemScrollPane() {
		switch (currShowMapType) {
		case partMap:// 流程图
			return partMapFlowElemScrollPane;
		case totalMap:// 流程地图
			return totalMapFlowElemScrollPane;
		case orgMap:// 组织图
			return orgMapFlowElemScrollPane;
		case none:
		default:
			return null;
		}
	}

	/**
	 * 
	 * 切换流程元素面板按钮图标显示效果
	 * 
	 */
	public void coverFlowElemButtonShowType() {
		// 流程图
		if (this.partMapFlowElemScrollPane != null) {
			this.partMapFlowElemScrollPane.setButtonShowType();
		}
		// 流程地图
		if (this.totalMapFlowElemScrollPane != null) {
			this.totalMapFlowElemScrollPane.setButtonShowType();
		}
		// 组织图
		if (this.orgMapFlowElemScrollPane != null) {
			this.orgMapFlowElemScrollPane.setButtonShowType();
		}
	}

	/**
	 * 
	 * 获取模型当前滚动面板
	 * 
	 * @return JecnTemplateScrollPane
	 */
	public JecnTemplateScrollPane getCurrModelScrollPane() {
		switch (currShowMapType) {
		case partMap:// 流程图
			return this.partMapModelScrollPane;
		case totalMap:// 流程地图
			return this.totalMapModelScrollPane;
		case orgMap:// 组织图
			return this.orgMapModelScrollPane;
		case none:
		default:
			return null;
		}
	}

	/**
	 * 
	 * 切换模型面板按钮图标显示效果
	 * 
	 */
	public void coverTemplateButtonShowType() {
		if (this.partMapModelScrollPane != null) {// 流程图
			this.partMapModelScrollPane.setButtonShowType();
		}
		if (this.totalMapModelScrollPane != null) {// 流程地图
			this.totalMapModelScrollPane.setButtonShowType();
		}
		if (this.orgMapModelScrollPane != null) {// 组织图
			this.orgMapModelScrollPane.setButtonShowType();
		}
	}

	/**
	 * 
	 * 获取当前按钮面板
	 * 
	 * @return JecnToolBoxAbstractScrollPane 流程图/流程地图/组织图
	 */
	public JecnToolBoxAbstractScrollPane getToolBoxScrollPane() {
		JecnToolBoxAbstractScrollPane toolBoxScrollPane = null;
		switch (currShowMapType) {
		case partMap:// 流程图
			toolBoxScrollPane = partMapFlowElemScrollPane;
			break;
		case totalMap:// 流程地图
			toolBoxScrollPane = totalMapFlowElemScrollPane;
			break;
		case orgMap:// 组织图
			toolBoxScrollPane = orgMapFlowElemScrollPane;
			break;
		case none:
			break;
		default:
			break;
		}
		return toolBoxScrollPane;
	}

	/**
	 * 获取button类型
	 * 
	 * @param selectedBtnType
	 *            MapElemType 流程元素类型
	 */
	public void setSelectedBtnType(MapElemType selectedBtnType) {
		if (selectedBtnType == null) {
			JecnLogConstants.LOG_TOOLBOX_PANEL
					.error("JecnToolBoxMainPanel类setSelectedBtnType方法：selectedBtnType参数为null");
			this.selectedBtnType = MapElemType.guide;
		}
		this.selectedBtnType = selectedBtnType;
		// 点击连接线显示图形编辑点
		performedManhattanLineButton(selectedBtnType);
		// 删除鼠标拖动时生成的虚拟图形
		JecnTabbedEventProcess.removeTempFigure();
		// 单选时清楚 图形自动对齐连接线
		JecnAdjacentFigure.removeTempLine();
		// 键盘事件未执行完，切换选中元素，处理键盘释放事件
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow != null && workflow.getKeyHandler() != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().getKeyHandler().keyReleased();
		}
		if (MapElemType.guide == selectedBtnType) {// 是指针
			this.guideBtn.setSelected(true);
		}
	}

	/**
	 * 点击连接线（包含带箭头和不带箭头的连接线）
	 * 
	 * @param selectedBtnType
	 */
	private void performedManhattanLineButton(MapElemType selectedBtnType) {
		switch (selectedBtnType) {
		case ManhattanLine:// 显示所有图形编辑点（可添加连接线的图形）
		case CommonLine:// 显示所有图形编辑点（可添加连接线的图形）
			JecnPaintFigureUnit.showAllConnectPort();
			break;
		default:// 隐藏所有图形编辑点
			JecnPaintFigureUnit.hideAllConnectPort();
		}
	}

	public JecnResourceUtil getResourceManager() {
		return resourceManager;
	}

	public ButtonGroup getBtnGroup() {
		return btnGroup;
	}

	public JecnToolBoxFlowElemContainPanel getFlowElemContainPanel() {
		return flowElemContainPanel;
	}

	public JecnToolBoxModelContainPanel getModelContainPanel() {
		return modelContainPanel;
	}

	public Insets getNoteInsets() {
		return noteInsets;
	}

	public Dimension getLeftRightBtnSize() {
		return leftRightBtnSize;
	}

	public Dimension getTopButtonBtnSize() {
		return topButtonBtnSize;
	}

	public Dimension getTitleTextBtnSize() {
		return titleTextBtnSize;
	}

	public MapElemType getSelectedBtnType() {
		return selectedBtnType;
	}

	public JecnSplitPane getSplitPane() {
		return splitPane;
	}

	private JecnToolBoxAbstractScrollPane getPartMapFlowElemScrollPane() {
		if (partMapFlowElemScrollPane == null) {
			// 创建对象
			partMapFlowElemScrollPane = new JecnToolBoxPartScrollPane(this);
			// 设置按钮显示效果
			partMapFlowElemScrollPane.setButtonShowType();
		}
		return partMapFlowElemScrollPane;
	}

	private JecnToolBoxAbstractScrollPane getTotalMapFlowElemScrollPane() {
		if (totalMapFlowElemScrollPane == null) {
			// 创建对象
			totalMapFlowElemScrollPane = new JecnToolBoxTotalScrollPane(this);
			// 设置按钮显示效果
			totalMapFlowElemScrollPane.setButtonShowType();
		}
		return totalMapFlowElemScrollPane;
	}

	private JecnToolBoxAbstractScrollPane getOrgMapFlowElemScrollPane() {
		if (orgMapFlowElemScrollPane == null) {
			// 创建对象
			orgMapFlowElemScrollPane = new JecnToolBoxOrgScrollPane(this);
			// 设置按钮显示效果
			orgMapFlowElemScrollPane.setButtonShowType();
		}
		return orgMapFlowElemScrollPane;
	}

	public JecnTemplateScrollPane getPartMapModelScrollPane() {
		if (partMapModelScrollPane == null) {
			partMapModelScrollPane = new JecnTemplateScrollPane(MapType.partMap, this);
		}
		return partMapModelScrollPane;
	}

	public void setPartMapModelScrollPane(JecnTemplateScrollPane partMapModelScrollPane) {
		this.partMapModelScrollPane = partMapModelScrollPane;
	}

	MapType getCurrShowMapType() {
		return currShowMapType;
	}

	public JecnTemplateScrollPane getTotalMapModelScrollPane() {
		if (totalMapModelScrollPane == null) {
			totalMapModelScrollPane = new JecnTemplateScrollPane(MapType.totalMap, this);
		}
		return totalMapModelScrollPane;
	}

	public void setTotalMapModelScrollPane(JecnTemplateScrollPane totalMapModelScrollPane) {
		this.totalMapModelScrollPane = totalMapModelScrollPane;
	}

	public JecnTemplateScrollPane getOrgMapModelScrollPane() {
		if (orgMapModelScrollPane == null) {
			orgMapModelScrollPane = new JecnTemplateScrollPane(MapType.orgMap, this);
		}
		return orgMapModelScrollPane;
	}

	public void setOrgMapModelScrollPane(JecnTemplateScrollPane orgMapModelScrollPane) {
		this.orgMapModelScrollPane = orgMapModelScrollPane;
	}

	enum ButtonShowType {
		leftRight, // 图片左内容右
		topBotton, // 图片上内容下
		titleText
		// 显示详细信息
	}

}
