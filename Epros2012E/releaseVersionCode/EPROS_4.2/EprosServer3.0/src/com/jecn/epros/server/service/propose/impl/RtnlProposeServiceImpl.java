package com.jecn.epros.server.service.propose.impl;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import oracle.sql.BLOB;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.lob.SerializableBlob;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfo;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.propose.JecnFlowRtnlPropose;
import com.jecn.epros.server.bean.propose.JecnRtnlPropose;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeButtonShowConfig;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeCountDetail;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeFile;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.popedom.IOrganizationDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.propose.IRtnlProposeDao;
import com.jecn.epros.server.dao.propose.IRtnlProposeFileDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.propose.IRtnlProposeService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

/***
 * 合理化建议Service 操作类 2013-09-02
 * 
 */
@Transactional
public class RtnlProposeServiceImpl extends AbsBaseService<JecnRtnlPropose, String> implements IRtnlProposeService {

	private Logger log = Logger.getLogger(RtnlProposeServiceImpl.class);
	private IRtnlProposeDao rtnlProposeDao;
	private IRtnlProposeFileDao rtnlProposeFileDao;
	private IFlowStructureDao flowStructureDao;
	private IPersonDao personDao;
	private IJecnConfigItemDao jecnConfigItemDao;

	public IJecnConfigItemDao getJecnConfigItemDao() {
		return jecnConfigItemDao;
	}

	public void setJecnConfigItemDao(IJecnConfigItemDao jecnConfigItemDao) {
		this.jecnConfigItemDao = jecnConfigItemDao;
	}

	/** 组织 */
	private IOrganizationDao organizationDao;
	/** 制度 */
	private IRuleDao ruleDao;

	public IRuleDao getRuleDao() {
		return ruleDao;
	}

	public void setRuleDao(IRuleDao ruleDao) {
		this.ruleDao = ruleDao;
	}

	public IOrganizationDao getOrganizationDao() {
		return organizationDao;
	}

	public void setOrganizationDao(IOrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	public IPersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(IPersonDao personDao) {
		this.personDao = personDao;
	}

	public IRtnlProposeDao getRtnlProposeDao() {
		return rtnlProposeDao;
	}

	public void setRtnlProposeDao(IRtnlProposeDao rtnlProposeDao) {
		this.rtnlProposeDao = rtnlProposeDao;
	}

	public IRtnlProposeFileDao getRtnlProposeFileDao() {
		return rtnlProposeFileDao;
	}

	public void setRtnlProposeFileDao(IRtnlProposeFileDao rtnlProposeFileDao) {
		this.rtnlProposeFileDao = rtnlProposeFileDao;
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	@Override
	public JecnFlowStructure getFlowStructure(Long flowId) {
		try {
			return flowStructureDao.findJecnFlowStructureById(flowId);
		} catch (Exception e) {
			log.error("获取流程主表数据出错", e);
			return null;
		}
	}

	@Override
	public JecnFlowBasicInfo getJecnFlowBasicInfo(Long flowId) {
		try {
			// String sql =
			// "select * from JECN_FLOW_BASIC_INFO where FLOW_ID ="+flowId;
			return (JecnFlowBasicInfo) rtnlProposeDao.getSession().get(JecnFlowBasicInfo.class, flowId);
		} catch (Exception e) {
			log.error("获取流程基本表数据出错", e);
			return null;
		}
	}

	@Override
	public List<JecnFlowBasicInfo> getJecnFlowBasicInfoList() throws Exception {
		String hql = "from JecnFlowBasicInfo";
		return this.rtnlProposeDao.listHql(hql);
	}

	public List<JecnRtnlPropose> getListNavigationRtnlPropose(JecnFlowRtnlPropose flowRtnlPropose, int proposeState,
			int start, int limit) throws Exception {

		try {

			// 登陆人是否是系统管理员
			boolean isAdmin = flowRtnlPropose.isAdmin();
			// 登陆人ID
			Long peopleId = flowRtnlPropose.getLoginUserId();
			// 项目ID
			Long projectId = flowRtnlPropose.getProjectId();
			// 提交人ID
			Long submitPeopleId = flowRtnlPropose.getSubmitPeopleId();
			// 开始时间
			Date startTime = flowRtnlPropose.getDateStartTime();
			// 结束时间
			Date endTime = flowRtnlPropose.getDateEndTime();
			// 是否选中我的建议
			boolean isSelectedSubmit = flowRtnlPropose.isSelectedSubmit();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// 合理化建议数据集合
			String sql = "";
			if (!isAdmin) {
				// 建议是否公开
				if (!JecnContants.proPubOrSecRation) {
					sql = this.getMyAuthFileSql(peopleId, projectId, flowRtnlPropose.getRtnlType()) + " ";
				}
			}
			sql += " select pub.id,pub.RELATION_ID,pub.PROPOSE_CONTENT,pub.REPLY_CONTENT,pub.state,pub.REPLY_PERSON,pub.REPLY_TIME,pub.CREATE_PERSON,pub.CREATE_TIME";
			sql += ",pub.UPDATE_PERSON,pub.UPDATE_TIME,pub.RELATION_TYPE,juu.ISLOCK,ju.ISLOCK as create_is_lock,ju.true_name as crate_people_name,juu.true_name as replay_people_name";
			sql += " from (";
			if (flowRtnlPropose.getRtnlType() == 0) {

				sql += this.getFlowPubSql(flowRtnlPropose, peopleId, submitPeopleId, startTime, endTime,
						isSelectedSubmit, sdf, projectId, JecnContants.proPubOrSecRation, isAdmin, proposeState);
			} else if (flowRtnlPropose.getRtnlType() == 1) {
				sql += this.getRulePubSql(flowRtnlPropose, peopleId, submitPeopleId, startTime, endTime,
						isSelectedSubmit, sdf, projectId, JecnContants.proPubOrSecRation, isAdmin, proposeState);
			} else {
				sql += this.getFlowPubSql(flowRtnlPropose, peopleId, submitPeopleId, startTime, endTime,
						isSelectedSubmit, sdf, projectId, JecnContants.proPubOrSecRation, isAdmin, proposeState);
				sql += " union ";
				sql += this.getRulePubSql(flowRtnlPropose, peopleId, submitPeopleId, startTime, endTime,
						isSelectedSubmit, sdf, projectId, JecnContants.proPubOrSecRation, isAdmin, proposeState);
			}
			sql += ") pub";
			sql = sql + " left join jecn_user ju on pub.CREATE_PERSON=ju.PEOPLE_ID and ju.ISLOCK=0";
			sql = sql + " left join jecn_user juu on pub.REPLY_PERSON = juu.PEOPLE_ID and juu.ISLOCK=0";
			sql = sql + " order by pub.CREATE_TIME desc";
			List<Object[]> listObjects = rtnlProposeDao.listNativeSql(sql, start, limit);

			// 合理化建议数据集合
			List<JecnRtnlPropose> listPropose = new ArrayList<JecnRtnlPropose>();
			List<String> listIds = new ArrayList<String>();
			for (Object[] obj : listObjects) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				listIds.add(obj[0].toString());
				JecnRtnlPropose rtnlPropose = this.getJecnRtnlProposeByObjectNew(obj);

				listPropose.add(rtnlPropose);
			}
			if (listIds.size() > 0) {
				String sqlfile = "select rpf.id,rpf.propose_id,rpf.file_name from JECN_RTNL_PROPOSE rp,JECN_RTNL_PROPOSE_FILE rpf where rp.id = rpf.propose_id and rp.id in"
						+ JecnCommonSql.getStrs(listIds);
				List<Object[]> listFileObjts = rtnlProposeFileDao.listNativeSql(sqlfile);
				for (JecnRtnlPropose rtnlPropose : listPropose) {
					for (Object[] objfile : listFileObjts) {
						if (objfile[0] != null && objfile[1] != null && objfile[2] != null
								&& objfile[1].toString().equals(rtnlPropose.getId())) {
							rtnlPropose.setListFileIds(objfile[0].toString());
							rtnlPropose.setListFileNames(objfile[2].toString());
						}
					}

				}
			}
			return listPropose;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 获得合理化建议JecnRtnlPropose的bean
	 * 
	 * @param obj
	 * @return
	 */
	public JecnRtnlPropose getJecnRtnlProposeByObject(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[4] == null || obj[5] == null) {
			return null;
		}
		JecnRtnlPropose rtnlPropose = new JecnRtnlPropose();
		// 主键Id
		rtnlPropose.setId(obj[0].toString());
		// 关联ID
		if (obj[1] != null)
			rtnlPropose.setRelationId(Long.valueOf(obj[1].toString()));
		// 建议内容
		if (obj[2] != null)
			rtnlPropose.setProposeContent(obj[2].toString());
		// 回复内容
		if (obj[3] != null)
			rtnlPropose.setReplyContent(obj[3].toString());
		// 状态
		if (obj[4] != null)
			rtnlPropose.setState(Integer.valueOf(obj[4].toString()));
		// 编辑占用
		// if (obj[5] != null) {
		// rtnlPropose.setIsEdit(Integer.valueOf(obj[5].toString()));
		// }
		// 回复人
		if (obj[6] != null) {
			rtnlPropose.setReplyPersonId(Long.valueOf(obj[6].toString()));
		}
		// 回复日期
		if (obj[7] != null) {
			rtnlPropose.setReplyTime((Date) obj[7]);
		}
		// 创建人
		if (obj[8] != null) {
			rtnlPropose.setCreatePersonId(Long.valueOf(obj[8].toString()));
		}
		// 创建日期
		if (obj[9] != null) {
			rtnlPropose.setCreateTime((Date) obj[9]);
		}
		// 更新人
		if (obj[10] != null) {
			rtnlPropose.setUpdatePersonId(Long.valueOf(obj[10].toString()));
		}
		// 更新日期
		if (obj[11] != null) {
			rtnlPropose.setUpdateTime((Date) obj[11]);
		}
		// 合理化建议类型
		if (obj[12] != null) {
			rtnlPropose.setRtnlType(Integer.valueOf(obj[12].toString()));
		}
		// 流程责任人ID
		if (obj.length > 13) {
			if (obj[13] != null) {
				rtnlPropose.setReFlowPeopleId(Long.valueOf(obj[13].toString()));
			}
		}

		return rtnlPropose;
	}

	@Override
	public List<JecnFlowRtnlPropose> searchListRtnlPropose(List<JecnRtnlPropose> listAllStatePropose,
			JecnFlowRtnlPropose flowRtnlPropose) throws Exception {
		// 设计器系统管理员是否配置处理建议：0：默认都不选中，1：选中
		boolean proSystemRation = JecnContants.proSystemRation;
		// 设计器流程责任人是否配置处理建议：0：默认都不选中，1：选中
		boolean proFlowRefRation = JecnContants.proFlowRefRation;

		/** 在页面上显示的合理化建议集合 */
		List<JecnFlowRtnlPropose> showFlowProposeList = new ArrayList<JecnFlowRtnlPropose>();
		// 获取所有的合理化建议
		// 关联ID(流程ID)
		Set<Long> setFlowIds = new HashSet<Long>();
		// 关联ID(制度ID)
		Set<Long> setRuleIds = new HashSet<Long>();
		for (JecnRtnlPropose propose : listAllStatePropose) {
			if ("".equals(propose.getId())) {
				continue;
			}
			if (propose.getRtnlType() == 0) {
				setFlowIds.add(Long.valueOf(propose.getRelationId()));
			} else {
				setRuleIds.add(Long.valueOf(propose.getRelationId()));
			}

			// 合理化建议Bean
			JecnFlowRtnlPropose flowPropose = new JecnFlowRtnlPropose();
			flowPropose.setJecnRtnlPropose(propose);
			showFlowProposeList.add(flowPropose);
		}
		List<Object[]> listFlowResObj = new ArrayList<Object[]>();
		// 获得流程的责任人和责任部门
		if (setFlowIds.size() > 0) {
			String flowSql = "select jfs.flow_id,jfs.flow_name,jfs.flow_id_input,jfo.org_id,jfo.org_name,jfbi.type_res_people"
					+ "   ,case when jfbi.type_res_people=1 then jfoi.figure_id else ju.people_id end as res_id"
					+ "   ,case when jfbi.type_res_people=1 then jfoi.figure_text else ju.true_name end as res_name"
					+ "    from jecn_flow_structure jfs,jecn_flow_basic_info jfbi"
					+ "    left join jecn_flow_related_org jfro on jfro.flow_id = jfbi.flow_id"
					+ "    left join jecn_flow_org jfo on jfo.org_id = jfro.org_id "
					+ "    left join jecn_user ju on jfbi.type_res_people=0 and ju.people_id = jfbi.res_people_id and ju.islock=0"
					+ "    left join jecn_flow_org_image jfoi on jfbi.type_res_people=1 and jfoi.figure_id = jfbi.res_people_id"
					+ "    left join jecn_flow_org org on org.org_id = jfoi.org_id "
					+ "    where jfbi.flow_id=jfs.flow_id  and jfs.flow_id in" + JecnCommonSql.getIdsSet(setFlowIds);
			listFlowResObj = rtnlProposeDao.listNativeSql(flowSql);
		}

		// 获得制度的责任人和责任部门
		List<Object[]> listRuleResObj = new ArrayList<Object[]>();
		if (setRuleIds.size() > 0) {
			String ruleSql = "select jfbi.id,jfbi.rule_name,jfbi.rule_number,jfo.org_id,jfo.org_name,jfbi.type_res_people"
					+ "   ,case when jfbi.type_res_people=1 then jfoi.figure_id else ju.people_id end as res_id"
					+ "   ,case when jfbi.type_res_people=1 then jfoi.figure_text else ju.true_name end as res_name,jfbi.is_dir,jfbi.File_Id"
					+ "    from jecn_rule jfbi"
					+ "    left join jecn_flow_org jfo on jfo.org_id = jfbi.org_id "
					+ "    left join jecn_user ju on jfbi.type_res_people=0 and ju.people_id = jfbi.res_people_id and ju.islock=0"
					+ "    left join jecn_flow_org_image jfoi on jfbi.type_res_people=1 and jfoi.figure_id = jfbi.res_people_id"
					+ "    left join jecn_flow_org org on org.org_id = jfoi.org_id "
					+ "    where  jfbi.id in"
					+ JecnCommonSql.getIdsSet(setRuleIds);
			listRuleResObj = rtnlProposeDao.listNativeSql(ruleSql);
		}
		if (listFlowResObj.size() > 0 || listRuleResObj.size() > 0) {
			for (JecnFlowRtnlPropose flowPropose : showFlowProposeList) {
				// 制度
				if (flowPropose.getJecnRtnlPropose().getRtnlType() == 1) {
					for (Object[] objFlow : listRuleResObj) {
						if (objFlow == null || objFlow[0] == null) {
							continue;
						}
						if (objFlow[0].toString().equals(flowPropose.getJecnRtnlPropose().getRelationId().toString())) {

							/** 编号 */
							if (objFlow[2] != null) {
								flowPropose.setFlowNum(objFlow[2].toString());
							}
							/** 名称 */
							if (objFlow[1] != null) {
								flowPropose.setFlowName(objFlow[1].toString());
							}
							/** 责任部门ID */
							if (objFlow[3] != null) {
								flowPropose.setOrgId(Long.valueOf(objFlow[3].toString()));
							}
							/** 责任部门名称 */
							if (objFlow[4] != null) {
								flowPropose.setOrgName(objFlow[4].toString());
							}
							/** 责任人类型：0：人员，1：岗位 */
							int resPeopleType = 0;
							if (objFlow[5] != null) {
								resPeopleType = Integer.parseInt(objFlow[5].toString());
							}
							flowPropose.setResPeopleType(resPeopleType);
							/** 责任人ID */
							if (objFlow[6] != null) {
								flowPropose.setResFlowId(Long.valueOf(objFlow[6].toString()));
							}
							/** 责任人名称 */
							if (objFlow[7] != null) {
								flowPropose.setResFlowName(objFlow[7].toString());
							}
							/** 合理化建议搜索类型：0：流程；1：制度 */
							flowPropose.setRtnlType(1);
							if (objFlow[8] != null) {
								/** 制度类型：1：制度模板文件；2：制度文件；0：流程 */
								flowPropose.setRuleIsDir(Integer.parseInt(objFlow[8].toString()));
							}
							if (objFlow[9] != null) {
								/** 制度文件ID */
								flowPropose.setRuleFileId(Long.valueOf(objFlow[9].toString()));
							}
						}

					}
					// 流程
				} else if (flowPropose.getJecnRtnlPropose().getRtnlType() == 0) {
					for (Object[] objFlow : listFlowResObj) {
						if (objFlow == null || objFlow[0] == null) {
							continue;
						}
						if (objFlow[0].toString().equals(flowPropose.getJecnRtnlPropose().getRelationId().toString())) {

							/** 流程编号 */
							if (objFlow[2] != null) {
								flowPropose.setFlowNum(objFlow[2].toString());
							}
							/** 流程名称 */
							if (objFlow[1] != null) {
								flowPropose.setFlowName(objFlow[1].toString());
							}
							/** 责任部门ID */
							if (objFlow[3] != null) {
								flowPropose.setOrgId(Long.valueOf(objFlow[3].toString()));
							}
							/** 责任部门名称 */
							if (objFlow[4] != null) {
								flowPropose.setOrgName(objFlow[4].toString());
							}
							/** 流程责任人类型：0：人员，1：岗位 */
							int resPeopleType = 0;
							if (objFlow[5] != null) {
								resPeopleType = Integer.parseInt(objFlow[5].toString());
							}
							flowPropose.setResPeopleType(resPeopleType);
							/** 流程责任人ID */
							if (objFlow[6] != null) {
								flowPropose.setResFlowId(Long.valueOf(objFlow[6].toString()));
							}
							/** 流程责任人名称 */
							if (objFlow[7] != null) {
								flowPropose.setResFlowName(objFlow[7].toString());
							}
							/** 合理化建议搜索类型：0：流程；1：制度 */
							flowPropose.setRtnlType(0);
							/** 制度类型：1：制度模板文件；2：制度文件；0：流程 */
							flowPropose.setRuleIsDir(0);
							/** 制度文件ID */
							flowPropose.setRuleFileId(null);
						}

					}
				}
				// 是否是责任人
				boolean isResPeople = this.isRes(flowRtnlPropose.getLoginUserId(), flowPropose.getJecnRtnlPropose()
						.getRelationId(), flowPropose.getJecnRtnlPropose().getRtnlType());
				// 是否是创建人
				boolean isCreatePeople = false;
				if (flowPropose.getJecnRtnlPropose() != null
						&& flowPropose.getJecnRtnlPropose().getCreatePersonId() != null
						&& flowRtnlPropose.getSubmitPeopleId() != null
						&& flowPropose.getJecnRtnlPropose().getCreatePersonId()
								.equals(flowRtnlPropose.getLoginUserId())) {
					isCreatePeople = true;
				}

				// 封装案例bean
				JecnRtnlProposeButtonShowConfig jecnRtnlProposeButtonShowConfig = initProposeButtonConfig(
						flowRtnlPropose.getLoginUserId(), flowRtnlPropose.isAdmin(), isResPeople, isCreatePeople,
						proSystemRation, proFlowRefRation, flowPropose.getJecnRtnlPropose());
				flowPropose.getJecnRtnlPropose().setButtonShowConfig(jecnRtnlProposeButtonShowConfig);
			}
		}

		return showFlowProposeList;
	}

	private JecnRtnlProposeButtonShowConfig initProposeButtonConfig(Long loginUserId, boolean isAdmin,
			boolean isResPeople, boolean isCreatePeople, boolean proSystemRation, boolean proFlowRefRation,
			JecnRtnlPropose rtnlPropose) {

		JecnRtnlProposeButtonShowConfig jecnRtnlProposeButtonShowConfig = this.setProposeButton(loginUserId, isAdmin,
				isResPeople, isCreatePeople, proSystemRation, proFlowRefRation, rtnlPropose);

		return jecnRtnlProposeButtonShowConfig;
	}

	@Override
	public boolean deleteFlowPropose(String proposeId, Long flowId, int isEdit, int isCreator) throws Exception {
		try {
			String sql = "";
			if (isCreator == 0) {
				// 判断是是否是已读状态
				sql = "select count(*) from JECN_RTNL_PROPOSE where ID =? and STATE !=0";
				int countNum = rtnlProposeDao.countAllByParamsNativeSql(sql, proposeId);
				if (countNum == 1) {
					return false;
				}
			}
			// 删除建议表数据
			sql = "delete from JECN_RTNL_PROPOSE where ID = ?";
			this.rtnlProposeDao.execteNative(sql, proposeId);
			// 删除建议附件表数据
			sql = "delete from JECN_RTNL_PROPOSE_FILE where PROPOSE_ID  = ?";
			this.rtnlProposeFileDao.execteNative(sql, proposeId);
			return true;
		} catch (Exception e) {
			log.error("删除未读状态数据出错", e);
			return false;
		}
	}

	/**
	 * 获得单个流程或制度文件的合理化建议
	 * 
	 * @param reletedId
	 * @param loginpeopleId
	 * @param proposeState
	 * @param isCheckedMyPropose
	 * @param rtnlType
	 * @return
	 */
	private String getSingleFileSql(Long reletedId, Long loginpeopleId, int proposeState, int isCheckedMyPropose,
			int rtnlType, boolean isCount) {
		String sql = "";
		if (isCount) {
			sql = "select count(rt.id) from jecn_rtnl_propose rt";
		} else {
			sql = "select rt.id,rt.RELATION_ID,rt.PROPOSE_CONTENT,rt.REPLY_CONTENT,rt.state,rt.REPLY_PERSON,rt.REPLY_TIME,rt.CREATE_PERSON,rt.CREATE_TIME"
					+ ",rt.UPDATE_PERSON,rt.UPDATE_TIME,rt.RELATION_TYPE,juu.ISLOCK,ju.ISLOCK as create_is_lock,ju.true_name as crate_people_name,juu.true_name as replay_people_name"
					+ " from jecn_rtnl_propose rt";
			sql = sql + " left join jecn_user ju on rt.CREATE_PERSON=ju.PEOPLE_ID and ju.ISLOCK=0";
			sql = sql + " left join jecn_user juu on rt.REPLY_PERSON = juu.PEOPLE_ID and juu.ISLOCK=0";

		}
		// 流程
		if (rtnlType == 0) {
			sql = sql + ",jecn_flow_basic_info fb where rt.relation_id = fb.flow_id ";

		}// 制度
		else if (rtnlType == 1) {
			sql = sql + ",jecn_rule jr where rt.relation_id = jr.id ";
		}
		sql = sql + " and rt.RELATION_ID = " + reletedId + " and rt.RELATION_TYPE =" + rtnlType;
		if (proposeState != -1) {
			if (proposeState == 1) {
				sql = sql + " and rt.STATE in (1,2,3) ";
			} else {
				sql = sql + " and rt.STATE = " + proposeState;
			}

		}
		if (isCheckedMyPropose == 1) {
			sql = sql + " and rt.create_person = " + loginpeopleId;
		}

		if (!isCount) {
			sql = sql + " order by rt.CREATE_TIME desc";
		}
		return sql;
	}

	@Override
	public int getFlowProposeTotal(Long reletedId, Long loginpeopleId, int proposeState, int isCheckedMyPropose,
			int rtnlType) throws Exception {
		try {
			String sql = this.getSingleFileSql(reletedId, loginpeopleId, proposeState, isCheckedMyPropose, rtnlType,
					true);
			return rtnlProposeDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("获取不同状态下数据总数出错", e);
			return 0;
		}

	}

	/**
	 * 获得合理化建议JecnRtnlPropose的bean
	 * 
	 * @param obj
	 * @return
	 */
	public JecnRtnlPropose getJecnRtnlProposeByObjectNew(Object[] obj) {
		if (obj == null || obj[0] == null) {
			return null;
		}
		JecnRtnlPropose rtnlPropose = new JecnRtnlPropose();
		// 主键Id
		rtnlPropose.setId(obj[0].toString());
		// 关联ID
		if (obj[1] != null)
			rtnlPropose.setRelationId(Long.valueOf(obj[1].toString()));
		// 建议内容
		if (obj[2] != null) {
			rtnlPropose.setProposeContent(obj[2].toString());
			rtnlPropose.setProposeContent(rtnlPropose.getProposeContent().replace("\n", "<br>"));
		}
		// 回复内容
		if (obj[3] != null) {
			rtnlPropose.setReplyContent(obj[3].toString());
			rtnlPropose.setReplyContent(rtnlPropose.getReplyContent().replaceAll("\n", "<br>"));
		}
		// 状态
		if (obj[4] != null)
			rtnlPropose.setState(Integer.valueOf(obj[4].toString()));
		// 回复人
		if (obj[5] != null) {
			rtnlPropose.setReplyPersonId(Long.valueOf(obj[5].toString()));
		}
		// 回复日期
		if (obj[6] != null) {
			rtnlPropose.setReplyTime((Date) obj[6]);
		}
		// 创建人
		if (obj[7] != null) {
			rtnlPropose.setCreatePersonId(Long.valueOf(obj[7].toString()));
		}
		// 创建日期
		if (obj[8] != null) {
			rtnlPropose.setCreateTime((Date) obj[8]);
		}
		// 更新人
		if (obj[9] != null) {
			rtnlPropose.setUpdatePersonId(Long.valueOf(obj[9].toString()));
		}
		// 更新日期
		if (obj[10] != null) {
			rtnlPropose.setUpdateTime((Date) obj[10]);
		}
		// 合理化建议类型
		if (obj[11] != null) {
			rtnlPropose.setRtnlType(Integer.valueOf(obj[11].toString()));
		}
		// 回复人是否删除 回复人名称
		if (obj[12] != null && obj[14] != null) {
			rtnlPropose.setReplyPeopleState(Integer.parseInt(obj[12].toString()));

			rtnlPropose.setReplyName(obj[15].toString());

		} else {
			rtnlPropose.setReplyPeopleState(1);
		}
		// 创建人是否删除 创建人名称
		if (obj[13] != null) {
			rtnlPropose.setCreatePeopleState(Integer.parseInt(obj[13].toString()));

			rtnlPropose.setCreateUpPeopoleName(obj[14].toString());

		} else {
			rtnlPropose.setReplyPeopleState(1);
		}

		return rtnlPropose;
	}

	@Override
	public List<JecnRtnlPropose> getListFlowRtnlPropose(int proposeState, Long peopleId, Long relatedId,
			int isCheckedMyPropose, int rtnlType, boolean isAdmin, int start, int limit) throws Exception {
		try {
			// 合理化建议查询
			String sql = this.getSingleFileSql(relatedId, peopleId, proposeState, isCheckedMyPropose, rtnlType, false);
			List<Object[]> listObjects = rtnlProposeDao.listNativeSql(sql, start, limit);
			// 合理化建议关联文件查询
			String sqlfile = "select rpf.id,rpf.propose_id,rpf.file_name from JECN_RTNL_PROPOSE rp,JECN_RTNL_PROPOSE_FILE rpf where rp.id = rpf.propose_id ";
			sqlfile = sqlfile + " and rp.relation_id =? " + " and rp.RELATION_TYPE =?";
			List<Object[]> listFileObjts = rtnlProposeFileDao.listNativeSql(sqlfile, relatedId, rtnlType);

			// 设计器系统管理员是否配置处理建议：0：默认都不选中，1：选中
			boolean proSystemRation = JecnContants.proSystemRation;
			// 设计器流程责任人是否配置处理建议：0：默认都不选中，1：选中
			boolean proFlowRefRation = JecnContants.proFlowRefRation;

			// 合理化建议数据集合
			List<JecnRtnlPropose> listPropose = new ArrayList<JecnRtnlPropose>();
			for (Object[] obj : listObjects) {
				JecnRtnlPropose rtnlPropose = this.getJecnRtnlProposeByObjectNew(obj);
				if (obj[0] == null) {
					continue;
				}
				if (rtnlPropose != null && listFileObjts != null) {
					for (Object[] objfile : listFileObjts) {
						if (objfile[0] != null && objfile[1] != null && objfile[2] != null
								&& objfile[1].toString().equals(obj[0].toString())) {
							rtnlPropose.setListFileIds(objfile[0].toString());
							rtnlPropose.setListFileNames(objfile[2].toString());
						}
					}
					listPropose.add(rtnlPropose);
				}
				// 创建人是不是提意见
				boolean isCreatePeople = false;
				if (rtnlPropose.getCreatePersonId() != null && peopleId != null
						&& peopleId.equals(rtnlPropose.getCreatePersonId())) {
					isCreatePeople = true;
				}
				// 是否是责任人
				boolean isResPeople = this.isRes(peopleId, relatedId, rtnlType);
				JecnRtnlProposeButtonShowConfig jecnRtnlProposeButtonShowConfig = this.setProposeButton(peopleId,
						isAdmin, isResPeople, isCreatePeople, proSystemRation, proFlowRefRation, rtnlPropose);
				rtnlPropose.setButtonShowConfig(jecnRtnlProposeButtonShowConfig);
			}
			return listPropose;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 封装意见按钮
	 * 
	 * @param isAdmin
	 *            是不是管理员
	 * @param isResPeople
	 *            是不是责任人
	 * @param isCreatePeople
	 *            是不是提交人
	 * @param proSystemRation
	 *            是不是系统管理员可以处理意见
	 * @param proFlowRefRation
	 *            是不是管理员可以处理意见
	 * @param rtnlPropose
	 * @return
	 */
	private JecnRtnlProposeButtonShowConfig setProposeButton(Long LoginUserId, boolean isAdmin, boolean isResPeople,
			boolean isCreatePeople, boolean proSystemRation, boolean proFlowRefRation, JecnRtnlPropose rtnlPropose) {
		JecnRtnlProposeButtonShowConfig jecnRtnlProposeButtonShowConfig = new JecnRtnlProposeButtonShowConfig();
		boolean acceptIsShow = false;
		/** 采纳按钮是否置灰 */
		boolean acceptIsGray = false;

		/** 取消采纳(拒绝)按钮是否显示 */
		boolean cancelAcceptIsShow = false;
		/** 取消采纳(拒绝)按钮是否置灰 */
		boolean cancelAcceptIsGray = false;

		/** 回复按钮是否显示 */
		boolean replayIsShow = false;
		/** 回复按钮是否置灰 */
		boolean replayIsGray = false;

		/** 已读按钮是否显示 */
		boolean readIsShow = false;
		/** 已读按钮是否置灰 */
		boolean readIsGray = false;

		/** 编辑按钮是否显示 */
		boolean editIsShow = false;
		/** 编辑按钮是否置灰 */
		boolean editIsGray = false;

		/** 删除按钮是否置灰 */
		boolean delIsShow = false;
		/** 删除按钮是否置灰 */
		boolean delIsGray = false;
		// 如果系统管理员可以处理意见或流程责任人可以处理意见
		if ((proSystemRation && isAdmin) || (proFlowRefRation && isResPeople) || isCreatePeople) {
			// 未读
			switch (rtnlPropose.getState()) {
			case 0:// 未读
				if ((proSystemRation && isAdmin) || (proFlowRefRation && isResPeople)) {
					// 采纳显示，不置灰
					acceptIsShow = true;
					acceptIsGray = false;
					// 取消采纳显示，置灰
					cancelAcceptIsShow = true;
					cancelAcceptIsGray = false;
					// 回复显示，不置灰
					replayIsShow = true;
					replayIsGray = false;
					// 已读显示，不置灰
					readIsShow = true;
					readIsGray = false;
				}

				// 删除显示，不置灰 只有管理员与建议创建人才可以删除
				if (isAdmin || isCreatePeople) {
					delIsShow = true;
					delIsGray = false;
				}

				// 意见提交人才可以编辑
				if (isCreatePeople) {
					// 编辑显示，不置灰
					editIsShow = true;
					editIsGray = false;

				} else {
					// 编辑不显示，置灰
					editIsShow = false;
					editIsGray = true;

				}
				break;
			// 已读
			case 1:
				if ((proSystemRation && isAdmin) || (proFlowRefRation && isResPeople)) {
					// 采纳显示，不置灰
					acceptIsShow = true;
					acceptIsGray = false;
					// 取消采纳显示，置灰
					cancelAcceptIsShow = true;
					cancelAcceptIsGray = false;
					// 回复显示，不置灰
					replayIsShow = true;
					replayIsGray = false;
					// 已读显示，置灰
					readIsShow = true;
					readIsGray = true;
				}
				// 删除显示，不置灰
				// 删除显示，不置灰 只有管理员才可以删除
				if (isAdmin) {
					delIsShow = true;
					delIsGray = false;
				}

				// 是不是意见提交人
				if (isCreatePeople) {
					// 编辑显示，不置灰
					editIsShow = true;
					editIsGray = false;

				} else {
					// 编辑不显示，置灰
					editIsShow = false;
					editIsGray = true;

				}
				break;
			// 采纳
			case 2:
				if ((proSystemRation && isAdmin) || (proFlowRefRation && isResPeople)) {
					// 采纳显示，置灰
					acceptIsShow = true;
					acceptIsGray = true;
					// 取消采纳显示，不置灰
					cancelAcceptIsShow = true;
					cancelAcceptIsGray = false;
					// 回复显示，不置灰
					replayIsShow = true;
					replayIsGray = false;
					// 已读显示，置灰
					readIsShow = true;
					readIsGray = true;
				}
				// 删除显示，不置灰
				// 删除显示，不置灰 只有管理员才可以删除
				if (isAdmin) {
					delIsShow = true;
					delIsGray = false;
				}

				// 是不是意见提交人
				if (isCreatePeople) {
					// 编辑显示，不置灰
					editIsShow = true;
					editIsGray = true;

				} else {
					// 编辑不显示，置灰
					editIsShow = false;
					editIsGray = true;

				}
				break;
			// 拒绝
			case 3:
				if ((proSystemRation && isAdmin) || (proFlowRefRation && isResPeople)) {
					// 采纳显示，置灰
					acceptIsShow = true;
					acceptIsGray = false;
					// 取消采纳显示，不置灰
					cancelAcceptIsShow = true;
					cancelAcceptIsGray = true;
					// 回复显示，不置灰
					replayIsShow = true;
					replayIsGray = false;
					// 已读显示，置灰
					readIsShow = true;
					readIsGray = true;
				}
				// 删除显示，不置灰
				// 删除显示，不置灰 只有管理员才可以删除
				if (isAdmin) {
					delIsShow = true;
					delIsGray = false;
				}

				// 是不是意见提交人
				if (isCreatePeople) {
					// 编辑显示，不置灰
					editIsShow = true;
					editIsGray = false;

				} else {
					// 编辑不显示，置灰
					editIsShow = false;
					editIsGray = true;

				}
				break;
			default:
				break;
			}

		}

		jecnRtnlProposeButtonShowConfig.setAcceptIsGray(acceptIsGray);
		jecnRtnlProposeButtonShowConfig.setAcceptIsShow(acceptIsShow);
		jecnRtnlProposeButtonShowConfig.setCancelAcceptIsShow(cancelAcceptIsShow);
		jecnRtnlProposeButtonShowConfig.setCancelAcceptIsGray(cancelAcceptIsGray);
		jecnRtnlProposeButtonShowConfig.setDelIsGray(delIsGray);
		jecnRtnlProposeButtonShowConfig.setDelIsShow(delIsShow);
		jecnRtnlProposeButtonShowConfig.setEditIsGray(editIsGray);
		jecnRtnlProposeButtonShowConfig.setEditIsShow(editIsShow);
		jecnRtnlProposeButtonShowConfig.setReadIsGray(readIsGray);
		jecnRtnlProposeButtonShowConfig.setReadIsShow(readIsShow);
		jecnRtnlProposeButtonShowConfig.setReplayIsGray(replayIsGray);
		jecnRtnlProposeButtonShowConfig.setReplayIsShow(replayIsShow);

		// 是否有权限可以删除回复的信息
		boolean curPeopelIsCanDelAnswer = false;
		if (rtnlPropose.getReplyContent() != null && !"".equals(rtnlPropose.getReplyContent())) {

			// 是不是回复人
			if (rtnlPropose.getReplyPersonId() != null && LoginUserId != null
					&& rtnlPropose.getReplyPersonId().equals(LoginUserId)) {
				curPeopelIsCanDelAnswer = true;
			}
		}
		jecnRtnlProposeButtonShowConfig.setCurPeopelIsCanDelAnswer(curPeopelIsCanDelAnswer);

		return jecnRtnlProposeButtonShowConfig;
	}

	/**
	 * 获得我的权限文件集合
	 * 
	 * @param peopleId
	 * @param projectId
	 * @return
	 */
	private String getMyAuthFileSql(Long peopleId, Long projectId, int rtnlType) {
		String sql = "WITH MY_PERSON AS(" + " SELECT JFOM.FIGURE_ID,JFOM.ORG_ID FROM"
				+ "       JECN_USER_POSITION_RELATED JUPR"
				+ "       INNER JOIN  JECN_FLOW_ORG_IMAGE JFOM ON JUPR.FIGURE_ID=JFOM.FIGURE_ID"
				+ "       INNER JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID = JFOM.ORG_ID AND JFO.DEL_STATE=0"
				+ "       WHERE JUPR.PEOPLE_ID=" + peopleId + ")";
		// 我自己查阅权限下的部门
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = sql + ",ORG AS(SELECT JF.ORG_ID,JF.PER_ORG_ID FROM JECN_FLOW_ORG JF";
			sql = sql + " WHERE JF.ORG_ID in (SELECT ORG_ID FROM MY_PERSON)";
			sql = sql
					+ " UNION ALL"
					+ " SELECT JFS.ORG_ID,JFS.PER_ORG_ID FROM ORG INNER JOIN JECN_FLOW_ORG JFS ON org.ORG_ID=JFS.PER_ORG_ID)";
		} else {
			sql = sql + ",ORG AS(SELECT DISTINCT JFO.ORG_ID" + "    FROM JECN_FLOW_ORG JFO"
					+ "   START WITH JFO.ORG_ID IN (SELECT ORG_ID FROM MY_PERSON)"
					+ "  CONNECT BY PRIOR JFO.ORG_ID = JFO.PER_ORG_ID )";
		}
		// 流程文件权限
		String flowSQl = " ,MY_AUTH_FLOWS AS (" + "  SELECT  T.flow_id,T.pre_flow_id"
				+ "         FROM Jecn_Flow_Structure T"
				+ "         INNER JOIN JECN_ORG_ACCESS_PERMISSIONS JOAP ON JOAP.RELATE_ID = T.FLOW_ID"
				+ "         INNER JOIN ORG ON ORG.ORG_ID = JOAP.ORG_ID" + "         WHERE T.PROJECTID = "
				+ projectId
				+ " AND JOAP.TYPE = 0 AND T.ISFLOW=1"
				+ "       UNION"
				+ "        SELECT T.FLOW_ID,"
				+ "               T.PRE_FLOW_Id"
				+ "         FROM Jecn_Flow_Structure T"
				+ "         INNER JOIN JECN_ACCESS_PERMISSIONS JAP"
				+ "            ON JAP.RELATE_ID = T.FLOW_ID"
				+ "         WHERE T.ISFLOW=1 AND JAP.FIGURE_ID IN (SELECT FIGURE_ID FROM MY_PERSON)"
				+ "           AND T.PROJECTID = "
				+ projectId
				+ "           AND JAP.TYPE = 0"
				+ "        UNION"
				+ "        SELECT T.FLOW_ID,"
				+ "               T.PRE_FLOW_Id"
				+ "          FROM Jecn_Flow_Structure T"
				+ "         INNER JOIN JECN_GROUP_PERMISSIONS JGP"
				+ "            ON JGP.RELATE_ID = T.FLOW_ID"
				+ "         INNER JOIN JECN_POSITION_GROUP_R JPGR"
				+ "            ON JGP.POSTGROUP_ID = JPGR.GROUP_ID"
				+ "         INNER JOIN JECN_FLOW_ORG_IMAGE JFOI"
				+ "            ON JFOI.FIGURE_ID = JPGR.FIGURE_ID"
				+ "         WHERE T.ISFLOW=1"
				+ "           AND T.PROJECTID = "
				+ projectId
				+ "           AND JGP.TYPE = 0"
				+ "           AND JFOI.FIGURE_ID IN (SELECT FIGURE_ID FROM MY_PERSON)"
				+ "        UNION"
				+ "        SELECT T.FLOW_ID,"
				+ "               T.PRE_FLOW_ID"
				+ " FROM Jecn_Flow_Structure T"
				+ "         WHERE T.ISFLOW=1"
				+ "           AND T.PROJECTID = "
				+ projectId + "           AND T.IS_PUBLIC = 1";

		flowSQl = flowSQl + " UNION " + "select distinct flow.flow_id, flow.pre_flow_id"
				+ "  from jecn_flow_structure flow" + "  where (flow.flow_id in (select jfs.flow_id"
				+ "                           from jecn_flow_structure_image  jfsi,"
				+ "                                jecn_flow_structure        jfs,"
				+ "                                process_station_related    prs,"
				+ "                                jecn_flow_org              jfo,"
				+ "                                jecn_flow_org_image        jfoi,"
				+ "                                jecn_user_position_related jupr,"
				+ "                                jecn_position_group_r      jpgr"
				+ "                          where jupr.figure_id = jfoi.figure_id"
				+ "                            and jfoi.org_id = jfo.org_id"
				+ "                            and jfo.del_state = 0"
				+ "                            and jfo.projectid = " + projectId
				+ "                            and jfoi.figure_id = jpgr.figure_id"
				+ "                            and jpgr.group_id = prs.figure_position_id"
				+ "                            and prs.type = '1'"
				+ "                            and prs.figure_flow_id = jfsi.figure_id"
				+ "                            and jfsi.flow_id = jfs.flow_id"
				+ "                            and jfs.projectid = " + projectId
				+ "                            and jfs.del_state = 0"
				+ "                            and jupr.people_id = " + peopleId + ") or"
				+ "       flow.flow_id in (select jfs.flow_id"
				+ "                           from jecn_flow_structure_image  jfsi,"
				+ "                                jecn_flow_structure        jfs,"
				+ "                                process_station_related    prs,"
				+ "                                jecn_flow_org              jfo,"
				+ "                                jecn_flow_org_image        jfoi,"
				+ "                                jecn_user_position_related jupr"
				+ "                          where jupr.figure_id = jfoi.figure_id"
				+ "                            and jfoi.org_id = jfo.org_id"
				+ "                            and jfo.del_state = 0"
				+ "                            and jfo.projectid =" + projectId
				+ "                            and jfoi.figure_id = prs.figure_position_id"
				+ "                            and prs.type = '0'"
				+ "                            and prs.figure_flow_id = jfsi.figure_id"
				+ "                            and jfsi.flow_id = jfs.flow_id"
				+ "                            and jfs.projectid = " + projectId
				+ "                            and jfs.del_state = 0"
				+ "                            and jupr.people_id = " + peopleId + ")))";
		String ruleSQL = " ,MY_AUTH_RULES AS (" + "  SELECT T.ID,T.PER_ID" + "         FROM JECN_RULE T"
				+ "         INNER JOIN JECN_ORG_ACCESS_PERMISSIONS JOAP ON JOAP.RELATE_ID = T.ID"
				+ "         INNER JOIN ORG ON ORG.ORG_ID = JOAP.ORG_ID" + "         WHERE T.PROJECT_ID = "
				+ projectId
				+ " AND JOAP.TYPE = 3 AND T.IS_DIR IN (1,2)"
				+ "       UNION"
				+ "        SELECT T.ID,"
				+ "               T.Per_Id"
				+ "         FROM JECN_RULE T"
				+ "         INNER JOIN JECN_ACCESS_PERMISSIONS JAP"
				+ "            ON JAP.RELATE_ID = T.ID"
				+ "         WHERE T.IS_DIR IN (1,2)"
				+ "           AND JAP.FIGURE_ID IN (SELECT FIGURE_ID FROM MY_PERSON)"
				+ "           AND T.PROJECT_ID = "
				+ projectId
				+ "           AND JAP.TYPE = 3"
				+ "        UNION"
				+ "        SELECT T.ID,"
				+ "               T.Per_Id"
				+ "          FROM JECN_RULE T"
				+ "         INNER JOIN JECN_GROUP_PERMISSIONS JGP"
				+ "            ON JGP.RELATE_ID = T.ID"
				+ "         INNER JOIN JECN_POSITION_GROUP_R JPGR"
				+ "            ON JGP.POSTGROUP_ID = JPGR.GROUP_ID"
				+ "         INNER JOIN JECN_FLOW_ORG_IMAGE JFOI"
				+ "            ON JFOI.FIGURE_ID = JPGR.FIGURE_ID"
				+ "         WHERE T.IS_DIR IN (1,2)"
				+ "           AND T.PROJECT_ID = "
				+ projectId
				+ "           AND JGP.TYPE = 3"
				+ "           AND JFOI.FIGURE_ID IN (SELECT FIGURE_ID FROM MY_PERSON)"
				+ "        UNION"
				+ "        SELECT T.ID,"
				+ "               T.PER_ID"
				+ "          FROM JECN_RULE T"
				+ "         WHERE T.IS_DIR IN (1,2)"
				+ "           AND T.PROJECT_ID = "
				+ projectId
				+ "           AND T.IS_PUBLIC = 1)";
		// 制度文件权限
		if (rtnlType == 0) {
			sql = sql + flowSQl;
		} else if (rtnlType == 1) {
			// 我文件查阅权限
			sql = sql + ruleSQL;
		} else {
			sql = sql + flowSQl + ruleSQL;
		}

		return sql;
	}

	/**
	 * 活动流程合理化建议的SQL
	 * 
	 * @param flowRtnlPropose
	 * @param peopleId
	 * @param submitPeopleId
	 * @param startTime
	 * @param endTime
	 * @param isSelectedSubmit
	 * @param sdf
	 * @param projectId
	 * @param proPubOrSecRation
	 * @param isAdmin
	 */
	private String getFlowPubSql(JecnFlowRtnlPropose flowRtnlPropose, Long peopleId, Long submitPeopleId,
			Date startTime, Date endTime, boolean isSelectedSubmit, SimpleDateFormat sdf, Long projectId,
			boolean proPubOrSecRation, boolean isAdmin, int proposeState) {
		String flowSQl = "select t.* from jecn_rtnl_propose t,jecn_flow_structure flow,jecn_flow_basic_info jfs";
		// 责任部门
		if (flowRtnlPropose.getOrgId() != null) {
			flowSQl += "  left join JECN_FLOW_RELATED_ORG jfro on jfro.flow_id=jfs.flow_id";
			flowSQl += "  left join jecn_flow_org jfo on jfo.org_id=jfro.org_id";
		}
		// 责任人
		if (flowRtnlPropose.getResFlowId() != null) {
			flowSQl += "  Left join jecn_user ju on jfs.type_res_people=0 and jfs.res_people_id=ju.people_id";
			flowSQl += "  Left join jecn_user_position_related jupr on jfs.type_res_people=1 and jfs.res_people_id=jupr.figure_id";
			flowSQl += "  Left join jecn_user juu on juu.people_id = jupr.people_id";
		}
		flowSQl += "  where t.relation_id=jfs.flow_id and jfs.flow_id =flow.flow_id and flow.projectid=" + projectId;
		flowSQl += "  and t.relation_type=0";
		if (flowRtnlPropose.getFlowName() != null && !"".equals(flowRtnlPropose.getFlowName())) {
			flowSQl += "  and jfs.flow_name like '%" + flowRtnlPropose.getFlowName() + "%'";
		}
		if (flowRtnlPropose.getFlowNum() != null && !"".equals(flowRtnlPropose.getFlowNum())) {
			flowSQl += "  and flow.flow_id_input like '%" + flowRtnlPropose.getFlowNum() + "%'";
		}
		if (flowRtnlPropose.getOrgId() != null) {
			flowSQl += "  and jfro.org_id = " + flowRtnlPropose.getOrgId();
		}
		if (flowRtnlPropose.getResFlowId() != null) {
			flowSQl += "  and ((jfs.type_res_people=0 and ju.people_id=" + flowRtnlPropose.getResFlowId()
					+ ") or (jfs.type_res_people=1 and juu.people_id=" + flowRtnlPropose.getResFlowId() + "))";
		}

		if (isSelectedSubmit) {
			flowSQl += "  and t.create_person =" + peopleId;
		} else {
			if (submitPeopleId != null && submitPeopleId.intValue() != -1) {
				flowSQl += "  and t.create_person =" + submitPeopleId;
			}
		}
		if (proposeState != -1) {// 非全部
			if (proposeState == 1) {
				flowSQl += " and t.state in (1,2,3) ";
			} else {
				flowSQl += " and t.state=" + proposeState;
			}
		}
		if (startTime != null && !"".equals(startTime) && endTime != null && !"".equals(endTime)) {
			if (JecnCommon.isSQLServer()) {
				flowSQl += " and  DATEDIFF(day,'" + sdf.format(startTime)
						+ "',t.CREATE_TIME)>=0 and datediff(day,t.CREATE_TIME,'" + sdf.format(endTime) + "')>=0 ";
			} else if (JecnCommon.isOracle()) {
				flowSQl += " and t.CREATE_TIME between  to_date('" + sdf.format(startTime)
						+ "','yyyy-mm-dd hh24:mi:ss')  and to_date('" + sdf.format(endTime)
						+ "','yyyy-mm-dd hh24:mi:ss')";
			}
		}
		if (!isAdmin) {
			if (!proPubOrSecRation) {
				flowSQl += " and jfs.flow_id in (select flow_id from MY_AUTH_FLOWS)";
			}
		}
		return flowSQl;
	}

	/**
	 * 活动流程合理化建议的SQL
	 * 
	 * @param flowRtnlPropose
	 * @param peopleId
	 * @param submitPeopleId
	 * @param startTime
	 * @param endTime
	 * @param isSelectedSubmit
	 * @param sdf
	 * @param projectId
	 * @param proPubOrSecRation
	 * @param isAdmin
	 */
	private String getRulePubSql(JecnFlowRtnlPropose flowRtnlPropose, Long peopleId, Long submitPeopleId,
			Date startTime, Date endTime, boolean isSelectedSubmit, SimpleDateFormat sdf, Long projectId,
			boolean proPubOrSecRation, boolean isAdmin, int proposeState) {
		String ruleSQl = "select t.* from jecn_rtnl_propose t,jecn_rule jr";
		// 责任部门
		if (flowRtnlPropose.getOrgId() != null) {
			ruleSQl += "  left join jecn_flow_org jfo on jfo.org_id=jr.org_id";
		}
		// 责任人
		if (flowRtnlPropose.getResFlowId() != null) {
			ruleSQl += "  Left join jecn_user ju on jr.type_res_people=0 and jr.res_people_id=ju.people_id";
			ruleSQl += "  Left join jecn_user_position_related jupr on jr.type_res_people=1 and jr.res_people_id=jupr.figure_id";
			ruleSQl += "  Left join jecn_user juu on juu.people_id = jupr.people_id";
		}
		ruleSQl += "  where t.relation_id=jr.id and jr.project_id=" + projectId;
		ruleSQl += "  and t.relation_type=1";
		if (flowRtnlPropose.getFlowName() != null && !"".equals(flowRtnlPropose.getFlowName())) {
			ruleSQl += "  and jr.rule_name like '%" + flowRtnlPropose.getFlowName() + "%'";
		}
		if (flowRtnlPropose.getFlowNum() != null && !"".equals(flowRtnlPropose.getFlowNum())) {
			ruleSQl += "  and jr.rule_number like '%" + flowRtnlPropose.getFlowNum() + "%'";
		}
		if (flowRtnlPropose.getOrgId() != null) {
			ruleSQl += "  and jr.org_id = " + flowRtnlPropose.getOrgId();
		}
		if (flowRtnlPropose.getResFlowId() != null) {
			ruleSQl += "  and ((jr.type_res_people=0 and ju.people_id=" + flowRtnlPropose.getResFlowId()
					+ ")or(jr.type_res_people=1 and juu.people_id=" + flowRtnlPropose.getResFlowId() + "))";
		}

		if (isSelectedSubmit) {
			ruleSQl += "  and t.create_person =" + peopleId;
		} else {
			if (submitPeopleId != null && submitPeopleId.intValue() != -1) {
				ruleSQl += "  and t.create_person =" + submitPeopleId;
			}
		}
		if (proposeState != -1) {
			if (proposeState == 1) {// 已读包含已读的采纳的拒绝的
				ruleSQl += " and t.state in (1,2,3) ";
			} else {
				ruleSQl += " and t.state=" + proposeState;
			}
		}
		if (startTime != null && !"".equals(startTime) && endTime != null && !"".equals(endTime)) {
			if (JecnCommon.isSQLServer()) {
				ruleSQl += " and  DATEDIFF(day,'" + sdf.format(startTime)
						+ "',t.CREATE_TIME)>=0 and datediff(day,t.CREATE_TIME,'" + sdf.format(endTime) + "')>=0 ";
			} else if (JecnCommon.isOracle()) {
				ruleSQl += " and t.CREATE_TIME between  to_date('" + sdf.format(startTime)
						+ "','yyyy-mm-dd hh24:mi:ss')  and to_date('" + sdf.format(endTime)
						+ "','yyyy-mm-dd hh24:mi:ss')";
			}
		}
		if (!isAdmin) {
			if (!proPubOrSecRation) {
				ruleSQl += " and jr.id in (select id from MY_AUTH_RULES)";
			}
		}
		return ruleSQl;
	}

	/***
	 * 导航===获取不同状态下合理化建议 ====总数
	 * 
	 * @param flowRtnlPropose
	 *            导航搜索条件Bean
	 * @param flowId流程ID
	 * @param isResponsible是否是流程责任人
	 * @param peopleId登录人Id
	 * @param proposeState当前页显示的状态
	 * @param submitPeopleId合理化建议提交人ID
	 * @param startTime创建时间
	 * @param endTime创建时间
	 * @param isSelectedHandle界面我处理的建议是否选中
	 *            ： 0：否，1：是
	 * @param isSelectedSubmit界面我提交的建议是否选中
	 *            ： 0：否，1：是
	 * @param proFlowRefRation设计器流程责任人是否配置处理建议
	 *            ：0：默认都不选中，1：选中
	 * @param proPubOrSecRation密级
	 *            ：0：秘密，1：公开
	 * @param isAdmin
	 * @param listPosIds登录人员所在岗位集合
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getNavigationProposeTotal(JecnFlowRtnlPropose flowRtnlPropose, int proposeState) throws Exception {
		try {
			// 密级：false：秘密，true：公开
			boolean proPubOrSecRation = JecnContants.proPubOrSecRation;
			// 登陆人是否是系统管理员
			boolean isAdmin = flowRtnlPropose.isAdmin();
			// 登陆人ID
			Long peopleId = flowRtnlPropose.getLoginUserId();
			// 项目ID
			Long projectId = flowRtnlPropose.getProjectId();
			// 提交人ID
			Long submitPeopleId = flowRtnlPropose.getSubmitPeopleId();
			// 开始时间
			Date startTime = flowRtnlPropose.getDateStartTime();
			// 结束时间
			Date endTime = flowRtnlPropose.getDateEndTime();
			// 是否选中我的建议
			boolean isSelectedSubmit = flowRtnlPropose.isSelectedSubmit();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// 合理化建议数据集合
			String sql = "";
			if (!isAdmin) {
				// 如果不是公开的
				if (!proPubOrSecRation) {
					sql = this.getMyAuthFileSql(peopleId, projectId, flowRtnlPropose.getRtnlType()) + " ";
				}
			}
			sql += "select count(pub.id) from (";
			if (flowRtnlPropose.getRtnlType() == 0) {

				sql += this.getFlowPubSql(flowRtnlPropose, peopleId, submitPeopleId, startTime, endTime,
						isSelectedSubmit, sdf, projectId, proPubOrSecRation, isAdmin, proposeState);
			} else if (flowRtnlPropose.getRtnlType() == 1) {
				sql += this.getRulePubSql(flowRtnlPropose, peopleId, submitPeopleId, startTime, endTime,
						isSelectedSubmit, sdf, projectId, proPubOrSecRation, isAdmin, proposeState);
			} else {
				sql += this.getFlowPubSql(flowRtnlPropose, peopleId, submitPeopleId, startTime, endTime,
						isSelectedSubmit, sdf, projectId, proPubOrSecRation, isAdmin, proposeState);
				sql += " union ";
				sql += this.getRulePubSql(flowRtnlPropose, peopleId, submitPeopleId, startTime, endTime,
						isSelectedSubmit, sdf, projectId, proPubOrSecRation, isAdmin, proposeState);
			}
			sql += ") pub";
			return rtnlProposeDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("获取不同状态下数据总数出错", e);
			throw e;
		}
	}

	@Override
	public String updateProposeState(String proposeId, int proposeState, Long loginUserId, String strContent,
			String dateTime) throws Exception {
		try {
			JecnRtnlPropose rtnlPropose = this.rtnlProposeDao.get(proposeId);
			if (rtnlPropose != null) {
				if (strContent != null) {
					if (rtnlPropose.getState() == 2 || rtnlPropose.getState() == 1) {
						if (rtnlPropose.getState() != proposeState) {
							proposeState = rtnlPropose.getState();
						}
					}
				}
				int excuteCount = 0;
				String sql = "update JECN_RTNL_PROPOSE set STATE = " + proposeState;
				if (strContent != null) {
					sql = sql + " , REPLY_CONTENT = '" + strContent + "' ,REPLY_PERSON= " + loginUserId
							+ " ,REPLY_TIME = ? ";
					sql = sql + " , UPDATE_PERSON = " + loginUserId + " , UPDATE_TIME = ? where ID = '" + proposeId
							+ "'";
				} else {
					sql = sql + " , UPDATE_PERSON = " + loginUserId + " , UPDATE_TIME = ? where ID = '" + proposeId
							+ "'" + " and STATE != " + proposeState;
				}

				// if(rtnlPropose.getIsEdit()==1){
				// return "0";//编辑中
				// }
				// 1:修改成功 ;2:当前点击取消采纳/已读 时别人已取消采纳/已读 （不提示）;
				// 3:该建议被删除(操作失败，该建议已不存在)；4：当前点击采纳时已被别人采纳（操作失败，该建议已被采纳）；
				// 5：回复操作更新失败（操作失败）;6:逻辑错误或异常（后台逻辑错误，请联系管理员！）7：已被别人拒绝
				if (strContent != null) {
					excuteCount = this.rtnlProposeDao.execteNative(sql, new Date(), new Date());
				} else {
					excuteCount = this.rtnlProposeDao.execteNative(sql, new Date());
				}

				if (excuteCount == 1) {
					return "1";// 修改成功
				} else {
					if (strContent == null) {// 为空表示不是回复操作
						if (rtnlPropose.getState() == 2) {
							return "4";// 已被别人采纳
						} else if (rtnlPropose.getState() == 3) {// 已被别人拒绝
							return "7";
						} else if (rtnlPropose.getState() == 1) {
							return "2";// 状态更改，无法修改（取消采纳/已读 ）
						} else {
							log.error("逻辑错误，rtnlPropose.getState()=" + rtnlPropose.getState());
							return "6";
						}
					} else {// 回复操作，更新返回值0
						return "5";// 回复内容更新失败
					}
				}
			} else {
				return "3";// 该建议不存在，请刷新数据！
			}
		} catch (Exception e) {
			log.error("更新合理化建议状态出错", e);
			return "6";
		}
	}

	@Override
	public String updateAllUnReadToRead(Long relatedId, int rtnlType) throws Exception {
		try {

			String sql = "update JECN_RTNL_PROPOSE set STATE = 1 where relation_id =? and STATE=0 and relation_type=?";
			this.rtnlProposeDao.execteNative(sql, relatedId, rtnlType);
		} catch (Exception e) {
			log.error("更新合理化建议状态出错", e);
		}
		return "1";
	}

	@Override
	public List<JecnUser> getJecnUserByResId(List<Long> peopleId) throws Exception {

		try {
			if (peopleId.size() == 0) {
				return null;
			}
			String hql = "select * from jecn_user where PEOPLE_ID in " + JecnCommonSql.getIds(peopleId);
			List<Object[]> objs = personDao.listNativeSql(hql);
			List<JecnUser> listJecnUser = new ArrayList<JecnUser>();
			for (Object[] obj : objs) {
				if (obj[0] != null && obj[2] != null && obj[4] != null) {
					JecnUser jecnUser = new JecnUser();
					jecnUser.setPeopleId(Long.valueOf(obj[0].toString()));
					jecnUser.setLoginName(obj[2].toString());
					jecnUser.setTrueName(obj[4].toString());
					jecnUser.setIsLock(Long.valueOf(obj[7].toString()));
					listJecnUser.add(jecnUser);
				}
			}
			return listJecnUser;
		} catch (Exception e) {
			log.error("获取人员表数据出错", e);
			return null;
		}
	}

	@Override
	public String addRtnlPropose(JecnRtnlPropose rtnlPropose, JecnRtnlProposeFile jecnRtnlProposeFile, int inflag)
			throws Exception {
		if (inflag == 1) {// 1：更新，0：添加
			// 判断是否处于编辑中
			// if(rtnlPropose.getIsEdit()==1){
			// return "0";
			// }else{
			// 设置编辑中
			// rtnlPropose.setIsEdit(1);

			// 删除附件
			if (rtnlPropose.getIsDeleteFileId() == 1) {
				this.deleteProposeFileById(rtnlPropose.getEditProposeFileId());
			}
			this.rtnlProposeDao.update(rtnlPropose);
			this.rtnlProposeDao.getSession().flush();
			/** 上传文件 */
			// 判断上传附件是否为空
			if (jecnRtnlProposeFile != null && jecnRtnlProposeFile.getFileContants() != null) {
				// 判断数据库类型是sqlserver或者mysql
				if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
					jecnRtnlProposeFile.setFileStream(Hibernate.createBlob(jecnRtnlProposeFile.getFileContants()));
					if (jecnRtnlProposeFile.getId() != null) {
						rtnlProposeFileDao.getSession().update(jecnRtnlProposeFile);
					} else {
						jecnRtnlProposeFile.setId(JecnCommon.getUUID());
						rtnlProposeFileDao.getSession().save(jecnRtnlProposeFile);
					}

				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {// oracle
					jecnRtnlProposeFile.setFileStream(Hibernate.createBlob(new byte[1]));
					if (jecnRtnlProposeFile.getId() != null) {
						rtnlProposeFileDao.getSession().update(jecnRtnlProposeFile);
					} else {
						jecnRtnlProposeFile.setId(JecnCommon.getUUID());
						rtnlProposeFileDao.getSession().save(jecnRtnlProposeFile);
					}
					rtnlProposeFileDao.getSession().flush();
					rtnlProposeFileDao.getSession().refresh(jecnRtnlProposeFile, LockMode.UPGRADE);
					SerializableBlob sb = (SerializableBlob) jecnRtnlProposeFile.getFileStream();
					java.sql.Blob wrapBlob = sb.getWrappedBlob();
					BLOB blob = (BLOB) wrapBlob;
					OutputStream out = null;
					try {
						out = blob.getBinaryOutputStream();
						out.write(jecnRtnlProposeFile.getFileContants());
					} catch (Exception ex) {
						log.error("附件上传SQL异常", ex);
						throw ex;
					} finally {
						if (out != null) {
							out.close();
						}
					}
				}
			}
			// 还原编辑状态
			// rtnlPropose.setIsEdit(0);
			// this.rtnlProposeDao.update(rtnlPropose);
			// this.rtnlProposeDao.getSession().flush();
			// }
		} else {
			// rtnlPropose.setId(JecnCommon.getUUID());
			// if (rtnlPropose.getProposeContent().length() >= 1000 &&
			// rtnlPropose.getProposeContent().length() <= 2000) {
			// rtnlPropose.setProposeContent(StringUtils.rightPad(rtnlPropose.getProposeContent(),
			// 2008));
			// }
			this.rtnlProposeDao.save(rtnlPropose);
			this.rtnlProposeDao.getSession().flush();

			if (jecnRtnlProposeFile != null && jecnRtnlProposeFile.getFileContants() != null) {
				// 判断数据库类型是sqlserver或者mysql
				if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
					jecnRtnlProposeFile.setFileStream(Hibernate.createBlob(jecnRtnlProposeFile.getFileContants()));
					rtnlProposeFileDao.getSession().save(jecnRtnlProposeFile);
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {// oracle
					jecnRtnlProposeFile.setFileStream(Hibernate.createBlob(new byte[1]));
					rtnlProposeFileDao.getSession().save(jecnRtnlProposeFile);
					rtnlProposeFileDao.getSession().flush();
					rtnlProposeFileDao.getSession().refresh(jecnRtnlProposeFile, LockMode.UPGRADE);
					SerializableBlob sb = (SerializableBlob) jecnRtnlProposeFile.getFileStream();
					java.sql.Blob wrapBlob = sb.getWrappedBlob();
					BLOB blob = (BLOB) wrapBlob;
					OutputStream out = null;
					try {
						out = blob.getBinaryOutputStream();
						out.write(jecnRtnlProposeFile.getFileContants());
					} catch (Exception ex) {
						log.error("附件上传SQL异常", ex);
						return "2";
					} finally {
						if (out != null) {
							out.close();
						}
					}
				}
			}

			JecnUser createUser = personDao.get(rtnlPropose.getCreatePersonId());
			rtnlPropose.setCreateUpPeopoleName(createUser.getTrueName());
			// 获得制度或者流程的名称
			if (rtnlPropose.getRtnlType() == 0) {
				JecnFlowStructure flowStructure = flowStructureDao.findJecnFlowStructureById(rtnlPropose
						.getRelationId());
				rtnlPropose.setRelationName(flowStructure.getFlowName());
			} else if (rtnlPropose.getRtnlType() == 1) {
				Rule rule = ruleDao.getRuleById(rtnlPropose.getRelationId());
				rtnlPropose.setRelationName(rule.getRuleName());
			}

			// 提交合理化建议之后发送邮件
			submitProposeSendEmail(rtnlPropose);
		}
		return "1";
	}

	private String getflowEndDateListMarkVlalue(ConfigItemPartMapMark mark) {
		String value = null;
		for (JecnConfigItemBean config : JecnContants.flowEndDateList) {
			if (config.getMark().toString().equals(mark.toString())) {
				value = config.getValue();
				break;
			}

		}

		return value;
	}

	/**
	 * 提交合理化建议发送邮件给管理员或者责任人
	 * 
	 * @param rtnlPropose
	 */
	private void submitProposeSendEmail(JecnRtnlPropose rtnlPropose) {

		try {
			List<JecnUser> emailUsers = getCreateProposeEmailUser(rtnlPropose);
			if (!emailUsers.isEmpty()) {
				BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.PROPOSE_HANDLER);
				emailBuilder.setData(emailUsers, rtnlPropose);
				List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
				JecnUtil.saveEmail(buildEmail);
			}

			// TODO :EMAIL
		} catch (Exception e) {
			log.error("提交合理化建议发送邮件给管理员或者责任人异常", e);
		}
	}

	/**
	 * 提交合理化建议创建
	 * 
	 * @param rtnlPropose
	 * @return
	 * @throws Exception
	 */
	private List<JecnUser> getCreateProposeEmailUser(JecnRtnlPropose rtnlPropose) throws Exception {

		// 根据配置项配置是否发送邮件
		Set<Long> emailToPeopleId = new HashSet<Long>();
		// 创建合理化建议发送邮件给处理人发送邮件
		String value = getflowEndDateListMarkVlalue(ConfigItemPartMapMark.mailProposeToHandle);
		boolean isToHandle = false;

		if ("1".equals(value)) {
			isToHandle = true;
		}

		if (isToHandle && JecnContants.proSystemRation) {
			List<Object> adminEmailPeople = personDao.getAdminIds();
			for (Object obj : adminEmailPeople) {
				emailToPeopleId.add(Long.valueOf(obj.toString()));
			}
		}
		// 给责任人
		if (isToHandle && JecnContants.proFlowRefRation) {
			String sql = "";
			/** 合理化建议类型：0：流程；1：制度 */
			if (rtnlPropose.getRtnlType() == 0) {
				// 使用union起始执行其中的一个因为TYPE_RES_PEOPLE不是0就是1
				sql = "select  ju.people_id" + "                        from jecn_user ju"
						+ "                       inner join jecn_user_position_related jupr"
						+ "                          on jupr.people_id = ju.people_id"
						+ "                       inner join jecn_flow_basic_info t"
						+ "                          on t.type_res_people = 1"
						+ "                         and jupr.figure_id = t.res_people_id"
						+ "                         and t.flow_id = ?" + "                      union"
						+ "                      select ju.people_id" + "                        from jecn_user ju"
						+ "                       inner join jecn_flow_basic_info t"
						+ "                          on t.type_res_people = 0"
						+ "                         and ju.people_id = t.res_people_id"
						+ "                         and t.flow_id = ?";
			} else if (rtnlPropose.getRtnlType() == 1) {
				sql = " select  ju.people_id" + "                        from jecn_user ju"
						+ "                       inner join jecn_user_position_related jupr"
						+ "                          on jupr.people_id = ju.people_id"
						+ "                       inner join jecn_rule t"
						+ "                          on t.type_res_people = 1"
						+ "                         and jupr.figure_id = t.res_people_id"
						+ "                         and t.id = ?" + "                      union"
						+ "                      select ju.people_id" + "                        from jecn_user ju"
						+ "                       inner join jecn_rule t"
						+ "                          on t.type_res_people = 0"
						+ "                         and ju.people_id = t.res_people_id"
						+ "                         and t.id = ?";

			}

			List<Object> dutyObjs = personDao.listNativeSql(sql, rtnlPropose.getRelationId(), rtnlPropose
					.getRelationId());
			for (Object obj : dutyObjs) {
				emailToPeopleId.add(Long.valueOf(obj.toString()));
			}
		}
		if (emailToPeopleId.isEmpty()) {
			return new ArrayList<JecnUser>();
		}
		String hql = "from JecnUser where peopleId in " + JecnCommonSql.getIdsSet(emailToPeopleId);
		return personDao.listHql(hql);
	}

	@Override
	public JecnRtnlProposeFile getJecnRtnlProposeFile(String id) throws Exception {
		String sql = "from JecnRtnlProposeFile where proposeId = ?";
		return this.rtnlProposeFileDao.getObjectHql(sql, id);
	}

	@Override
	public JecnRtnlPropose getJecnRtnlPropose(String id) throws Exception {
		return this.rtnlProposeDao.get(id);
	}

	@Override
	public JecnRtnlProposeFile getPropoFileById(String fileId) throws Exception {
		JecnRtnlProposeFile proposeFile = this.rtnlProposeFileDao.get(fileId);
		// blob转换byte[]
		SerializableBlob sb = (SerializableBlob) proposeFile.getFileStream();
		Blob wrapBlob = sb.getWrappedBlob();
		byte[] content = new byte[(int) wrapBlob.length()];

		InputStream in = null;
		try {
			in = wrapBlob.getBinaryStream();
			in.read(content);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
					log.error("", e);
				}
			}
		}

		proposeFile.setFileContants(content);
		return proposeFile;
	}

	@Override
	public String updateJecnRtnlPropose(String id) throws Exception {
		String sql = "update jecn_rtnl_propose set REPLY_CONTENT = '',REPLY_PERSON = -1,REPLY_TIME = '' where id = '"
				+ id + "'";
		// 更新建议内容
		this.rtnlProposeDao.execteNative(sql);
		return "1";
	}

	@Override
	public void deleteProposeFileById(String fileId) throws Exception {
		String sql = "delete from jecn_rtnl_propose_file where id = '" + fileId + "'";
		this.rtnlProposeFileDao.execteNative(sql);
	}

	@Override
	public boolean eidtIsShowDiv(String proposeId, Long flowId, int state) throws Exception {
		if (proposeId != null) {
			String sql = "select count(*) from jecn_rtnl_propose where id ='" + proposeId + "' and state !=2";
			int count = this.rtnlProposeDao.countAllByParamsNativeSql(sql);
			if (count > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 制度基本信息
	 */
	@Override
	public Rule getRuleInfo(Long ruleId) throws Exception {
		return ruleDao.getRuleById(ruleId);
	}

	/**
	 * 获取制度基本信息表数据集合
	 * 
	 * @return
	 */
	@Override
	public List<Rule> getRuleInfoList() throws Exception {
		String hql = "from Rule";
		return this.rtnlProposeDao.listHql(hql);
	}

	/**
	 * 获得流程名称
	 * 
	 * @param flowRuleName输入的流程
	 *            、制度名称
	 * @param type
	 *            0：流程，1：制度
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> findJecnFlowStructureName(String flowRuleName, int type) throws Exception {
		// 获取项目ID
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");

		String sql = "";
		List<Object[]> objList = new ArrayList<Object[]>();
		if (type == 0) {// 流程
			sql = "select flowId,flowName from JecnFlowStructure where delState=0 and isFlow = 1 and projectId = "
					+ webLoginBean.getProjectId() + " and flowName like '%" + flowRuleName + "%'";
			objList = flowStructureDao.listHql(sql);
		} else if (type == 1) {// 制度
			sql = "select id,ruleName from Rule where isDir <> 0 and projectId =" + webLoginBean.getProjectId()
					+ " and ruleName like '%" + flowRuleName + "%'";
			objList = rtnlProposeDao.listHql(sql);
		}
		List<JecnFlowStructure> listFlow = new ArrayList<JecnFlowStructure>();
		for (Object[] obj : objList) {
			JecnFlowStructure flowStructure = new JecnFlowStructure();
			flowStructure.setFlowId(Long.valueOf(obj[0].toString()));
			flowStructure.setFlowName(obj[1].toString());
			listFlow.add(flowStructure);
		}
		return objList;
	}

	@Override
	public boolean isRes(Long loginUserId, Long relatedId, int rtnlType) throws Exception {
		// 登陆人是不是责任人
		boolean isResPeople = false;
		String sql = "";
		if (rtnlType == 0) {
			JecnFlowBasicInfo jecnFlowBasicInfo = this.getJecnFlowBasicInfo(relatedId);
			// 责任人是岗位
			if (jecnFlowBasicInfo.getTypeResPeople() != null && jecnFlowBasicInfo.getTypeResPeople() == 1) {
				sql = "select count(jupr.people_id)"
						+ "    from jecn_flow_basic_info t,Jecn_Flow_Org_Image jfoi,jecn_flow_org jfo,JECN_USER_POSITION_RELATED jupr"
						+ "    where jupr.people_id=? and t.flow_id=? and jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 and"
						+ "    t.type_res_people=1 and jfoi.figure_id = t.res_people_id";
				int count = rtnlProposeFileDao.countAllByParamsNativeSql(sql, loginUserId, relatedId);
				if (count > 0) {
					isResPeople = true;
				}

			} else {
				if (jecnFlowBasicInfo.getResPeopleId() != null && loginUserId != null
						&& loginUserId.equals(jecnFlowBasicInfo.getResPeopleId())) {
					isResPeople = true;
				}
			}
		} else if (rtnlType == 1) {
			Rule rule = this.getRuleInfo(relatedId);
			// 责任人是岗位
			if (rule.getTypeResPeople() != null && rule.getTypeResPeople() == 1) {
				sql = "select count(jupr.people_id)"
						+ "    from jecn_rule t,Jecn_Flow_Org_Image jfoi,jecn_flow_org jfo,JECN_USER_POSITION_RELATED jupr"
						+ "    where jupr.people_id=? and t.id=? and jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 and"
						+ "    t.type_res_people=1 and jfoi.figure_id = t.res_people_id";
				int count = rtnlProposeFileDao.countAllByParamsNativeSql(sql, loginUserId, relatedId);
				if (count > 0) {
					isResPeople = true;
				}

			} else {
				if (rule.getAttchMentId() != null && loginUserId != null && loginUserId.equals(rule.getAttchMentId())) {
					isResPeople = true;
				}
			}
		}
		return isResPeople;
	}

	@Override
	public JecnRtnlPropose getJecnRtnlPropose(String rtnlProposeId, JecnUser jecnUser, boolean isAdmin)
			throws Exception {
		JecnRtnlPropose rtnlPropose = this.rtnlProposeDao.get(rtnlProposeId);
		if (rtnlPropose != null) {

			// 设计器系统管理员是否配置处理建议：0：默认都不选中，1：选中
			boolean proSystemRation = JecnContants.proSystemRation;
			// 设计器流程责任人是否配置处理建议：0：默认都不选中，1：选中
			boolean proFlowRefRation = JecnContants.proFlowRefRation;

			// 是否是责任人
			boolean isResPeople = this.isRes(jecnUser.getPeopleId(), rtnlPropose.getRelationId(), rtnlPropose
					.getRtnlType());

			// 是否是创建人
			boolean isCreatePeople = false;
			if (rtnlPropose.getRelationId().equals(jecnUser.getPeopleId())) {
				isCreatePeople = true;
			}

			// 封装案例bean
			JecnRtnlProposeButtonShowConfig configButton = initProposeButtonConfig(jecnUser.getPeopleId(), isAdmin,
					isResPeople, isCreatePeople, proSystemRation, proFlowRefRation, rtnlPropose);
			rtnlPropose.setButtonShowConfig(configButton);

		}
		return rtnlPropose;
	}

	@Override
	public List<JecnRtnlProposeCountDetail> getProposeCreateCount(String personIds, String startTime, String endTime,
			int start, int limit) throws Exception {

		List<Object[]> objs = this.rtnlProposeDao
				.getProposeCreateCountList(personIds, startTime, endTime, start, limit);

		String startAndEndTime = startTime + "---" + endTime;
		List<JecnRtnlProposeCountDetail> proposeList = packageProposeCreate(objs, startAndEndTime);

		return proposeList;
	}

	private List<JecnRtnlProposeCountDetail> packageProposeCreate(List<Object[]> objs, String startAndEndTime) {
		List<JecnRtnlProposeCountDetail> proposeList = new ArrayList<JecnRtnlProposeCountDetail>();

		for (Object[] obj : objs) {

			if (obj[0] == null) {
				continue;
			}

			JecnRtnlProposeCountDetail proposeCountBean = new JecnRtnlProposeCountDetail();
			proposeCountBean.setStrDate(startAndEndTime);
			proposeCountBean.setName(obj[0].toString());

			// 总数
			if (obj[1] != null) {
				proposeCountBean.setTotal(Integer.valueOf(obj[1].toString()));
				if (obj[2] != null && Integer.valueOf(obj[1].toString()) > 0) {
					int total = Integer.valueOf(obj[1].toString());
					int accept = Integer.valueOf(obj[2].toString());
					int percent = accept * 100 / total;
					proposeCountBean.setAcceptPercent(percent);
					proposeCountBean.setAcceptPercentStr(percent + "%");
					proposeCountBean.setAcceptTotal(accept);
				} else {
					proposeCountBean.setAcceptTotal(0);
					proposeCountBean.setAcceptPercentStr("0%");
				}
			} else {
				proposeCountBean.setAcceptPercent(0);
				proposeCountBean.setAcceptPercentStr(0 + "%");
			}

			proposeList.add(proposeCountBean);
		}

		return proposeList;
	}

	@Override
	public int getProposeDetailCountNum(String peopleIds, String orgIds, String startTime, String endTime)
			throws Exception {

		return this.rtnlProposeDao.getProposeDetailNum(peopleIds, orgIds, startTime, endTime);
	}

	@Override
	public List<JecnRtnlProposeCountDetail> getProposeDetailCountList(String peopleIds, String orgIds,
			String startTime, String endTime, int start, int limit) throws Exception {

		List<Object[]> objList = this.rtnlProposeDao.getProposeDetailList(peopleIds, orgIds, startTime, endTime, start,
				limit);

		return packageProposeDetailList(objList);
	}

	private List<JecnRtnlProposeCountDetail> packageProposeDetailList(List<Object[]> objList) {
		List<JecnRtnlProposeCountDetail> detailList = new ArrayList<JecnRtnlProposeCountDetail>();
		for (Object[] obj : objList) {

			// ju.people_id," +
			// ju.true_name," +
			// jfo.org_id," +
			// jfo.org_name," +
			// jfs.flow_id," +
			// jfs.flow_name," +
			// jrp.id," +
			// jrp.propose_content," +
			// jrp.create_time" +
			//			
			if (obj[0] == null || obj[1] == null || obj[4] == null || obj[5] == null || obj[6] == null) {
				continue;
			}

			JecnRtnlProposeCountDetail detail = new JecnRtnlProposeCountDetail();

			detail.setCreateName(obj[1].toString());

			// 部门id,名称
			if (obj[2] != null && obj[3] != null) {
				detail.setOrgName(obj[3].toString());

			}

			detail.setRelateName(obj[5].toString());

			detail.setRtnlContent(obj[7].toString());

			if (obj[8] != null) {
				detail.setCreateTime(obj[8].toString());
			}

			detailList.add(detail);
		}
		return detailList;
	}

	@Override
	public List<JecnRtnlProposeCountDetail> getProposeOrgCount(String orgIds, String startTime, String endTime,
			int start, int limit) throws Exception {

		List<Object[]> objList = this.rtnlProposeDao.getProposeOrgCountList(orgIds, startTime, endTime, start, limit);

		String startAndEndTime = startTime + "---" + endTime;
		List<JecnRtnlProposeCountDetail> proposeList = packageProposeCreate(objList, startAndEndTime);

		return proposeList;
	}

	@Override
	public void handleProposeSendEmail(String proposeId, int flag) throws Exception {
		JecnRtnlPropose jecnRtnlPropose = this.getJecnRtnlPropose(proposeId);
		if (jecnRtnlPropose != null) {
			JecnUser createPeople = personDao.get(jecnRtnlPropose.getCreatePersonId());
			List<JecnUser> handlers = getProposeHandler(jecnRtnlPropose);
			if (createPeople != null && createPeople.getIsLock() == 0 && handlers.size() > 0) {
				jecnRtnlPropose.setRelationName(getTopicName(jecnRtnlPropose));
				jecnRtnlPropose.setCreateUpPeopoleName(createPeople.getTrueName());
				BaseEmailBuilder emailBuilder = EmailBuilderFactory
						.getEmailBuilder(EmailBuilderType.PROPOSE_HANDLER_RESULT);
				List<JecnUser> users = new ArrayList<JecnUser>();
				users.add(createPeople);
				emailBuilder.setData(users, handlers.get(0), jecnRtnlPropose, flag);
				List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
				JecnUtil.saveEmail(buildEmail);
			}
		}
	}

	@Override
	public void submitProposeSendEmail(String proposeId) throws Exception {
		JecnRtnlPropose jecnRtnlPropose = this.getJecnRtnlPropose(proposeId);
		if (jecnRtnlPropose != null) {
			JecnUser createPeople = personDao.get(jecnRtnlPropose.getCreatePersonId());
			List<JecnUser> emailUsers = getProposeHandler(jecnRtnlPropose);
			if (createPeople != null && emailUsers.size() > 0) {
				jecnRtnlPropose.setRelationName(getTopicName(jecnRtnlPropose));
				jecnRtnlPropose.setCreateUpPeopoleName(createPeople.getTrueName());
				BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.PROPOSE_HANDLER);
				emailBuilder.setData(emailUsers, jecnRtnlPropose);
				List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
				JecnUtil.saveEmail(buildEmail);
			}
		}
	}

	private String getTopicName(JecnRtnlPropose jecnRtnlPropose) {
		List<String> names = rtnlProposeDao.listObjectNativeSql(getRelated(jecnRtnlPropose), "name", Hibernate.STRING,
				jecnRtnlPropose.getRelationId(), jecnRtnlPropose.getRtnlType());
		return names.get(0);
	}

	public String getRelated(JecnRtnlPropose propose) {

		String sql = "select a.name from propose_topic a where a.related_id=" + propose.getRelationId()
				+ " and a.type=" + propose.getRtnlType();

//		/** 0流程，1制度，2流程架构，3文件，4风险，5标准 **/
//		String tableName = "";
//		String name = "";
//		String id = "";
//		int type = propose.getRtnlType();
//		if (type == 0) {
//			id = "FLOW_ID";
//			name = "FLOW_NAME";
//			tableName = "JECN_FLOW_STRUCTURE";
//		} else if (type == 1) {
//			id = "ID";
//			name = "RULE_NAME";
//			tableName = "JECN_RULE";
//		} else if (type == 2) {
//			id = "FLOW_ID";
//			name = "FLOW_NAME";
//			tableName = "JECN_FLOW_STRUCTURE";
//		} else if (type == 3) {
//			id = "FILE_ID";
//			name = "FILE_NAME";
//			tableName = "JECN_FILE";
//		} else if (type == 4) {
//			id = "ID";
//			name = "NAME";
//			tableName = "JECN_RISK";
//		} else if (type == 5) {
//			id = "CRITERION_CLASS_ID";
//			name = "CRITERION_CLASS_NAME";
//			tableName = "JECN_CRITERION_CLASSES";
//		}
//
//		String sql = " SELECT DISTINCT JFS." + name + " AS name" + " FROM " + tableName + " JFS" + " WHERE" + "	JFS."
//				+ id + " =" + propose.getRelationId();

		return sql;

	}

	private List<JecnUser> getProposeHandler(JecnRtnlPropose jecnRtnlPropose) {
		String sql = "select b.* from propose_topic a inner join jecn_user b on a.handle_people=b.people_id and b.islock=0 where a.related_id=? and a.type=?";
		List<JecnUser> emailUsers = (List<JecnUser>) rtnlProposeDao.listNativeAddEntity(sql, JecnUser.class,
				jecnRtnlPropose.getRelationId(), jecnRtnlPropose.getRtnlType());
		return emailUsers;
	}
}
