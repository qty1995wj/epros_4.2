package epros.designer.gui.process.activitiesProperty;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.integration.activity.JecnActiveTypeDialog;
import epros.designer.gui.system.CommonJTextField;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/*******************************************************************************
 * 活动明细：基本信息
 * 
 * @author 2012-07-27
 * 
 */
public class ActiveBaseInfoPanel extends JecnPanel implements CaretListener {
	private Logger log = Logger.getLogger(ActiveBaseInfoPanel.class);
	/** 基本信息 */
	protected JecnPanel basicInfoPanel = null;

	/** 活动信息 */
	protected JecnPanel activeAtrrPanel = null;
	/** 关键活动 */
	protected JecnPanel keyActivePanel = null;

	protected JecnPanel newPanel = null;

	/** 编号标签 */
	protected JLabel activeNumLabel = null;
	/** 编号输入框 */
	protected JTextField activeNumTextField = null;
	/** 编号提示信息框 */
	protected JecnUserInfoTextArea activeNumInfoTextArea = null;

	/** 名称标签 */
	protected JLabel activeNameLabel = null;
	/** 名称输入框 */
	protected JTextArea activeNameTextArea = null;
	/** 名称输入框容器 */
	protected JScrollPane activeNameScrollPane = null;
	/** 名称提示信息框 */
	protected JecnUserInfoTextArea activeNameInfoTextArea = null;

	/** 说明标签 */
	protected JLabel activeNoteLabel = null;
	/** 说明输入区域 */
	protected JTextArea activeNoteTextArea = null;
	/** 说明输入区域容器 */
	protected JScrollPane activeNoteScrollPane = null;
	/** 说明提示信息框 */
	protected JecnUserInfoTextArea activeNoteInfoTextArea = null;

	/** 选中的单选框对应的流程元素类型 */
	protected MapElemType selectedMapElemType = null;

	/** PA\KSF\KCP类型标签 */
	protected JLabel keyActiveTypeLabel = null;
	/** PA\KSF\KCP类型内容 */
	protected JLabel keyActiveTypeContLabel = null;
	/** PA\KSF\KCP标签 */
	protected JLabel keyActiveNoteLabel = null;
	/** PA\KSF\KCP输入区域 */
	protected JTextArea keyActiveNoteTextArea = null;
	/** PA\KSF\KCP输入区域容器 */
	protected JScrollPane keyActiveNoteScrollPane = null;
	/** PA\KSF\KCP输入区提示信息框 */
	protected JecnUserInfoTextArea keyActiveNoteInfoTextArea = null;

	/** 活动数据 */
	protected JecnActiveData jecnActiveData;
	/** 活动图形数据 */
	protected JecnBaseActiveFigure activeFigure;

	/** 对应内控矩阵风险点标签 INNER_CONTROL_RISK */
	protected JLabel innerControlRiskLabel = null;
	/** 对应内控矩阵风险点输入框 */
	protected JTextArea innerControlRiskTextArea = null;
	/** 对应内控矩阵风险点输入框容器 */
	protected JScrollPane innerControlRiskScrollPane = null;
	/** 对应内控矩阵风险点提示信息框 */
	protected JecnUserInfoTextArea innerControlRiskInfoTextArea = null;

	/** 对应标准条款标签 STANDARD_CONDITIONS */
	protected JLabel standardConditionsLabel = null;
	/** 对应标准条款输入框 */
	protected JTextArea standardConditionsTextArea = null;
	/** 对应标准条款输入框容器 */
	protected JScrollPane standardConditionsScrollPane = null;
	/** 对应标准条款提示信息框 */
	protected JecnUserInfoTextArea standardConditionsInfoTextArea = null;

	/** 三期需求新增 ** */
	/** 活动类别： */
	private JLabel activityTypeLable;
	/** 活动类别 */
	private JComboBox activityTypeBox;
	/** 活动类别编辑 */
	private JButton activiteEditBut;

	/** 记录活动类别集合 */
	private List<JecnActiveTypeBean> listActiveTypeBean;

	/** 爱普生-活动担当 */
	private CommonJTextField activeEventField = null;

	private JLabel errorEventLabel;

	/** 三期需求新增end ** */

	public ActiveBaseInfoPanel(JecnActiveData jecnActiveData, JecnBaseActiveFigure activeFigure) {
		this.jecnActiveData = jecnActiveData;
		this.activeFigure = activeFigure;
		initActiveKey();
		intComponents();
		initLayout();
		initData();
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData, JecnBaseActiveFigure activeFigure) {
		this.jecnActiveData = jecnActiveData;
		this.activeFigure = activeFigure;
		initActiveKey();
		initData();
	}

	void initActiveKey() {
		// 活动关联的关键活动类型，如果活动区域没有关键活动返回NULL
		selectedMapElemType = null;
		if ("1".equals(jecnActiveData.getActiveKeyType())) {
			selectedMapElemType = MapElemType.PAFigure;
		} else if ("2".equals(jecnActiveData.getActiveKeyType())) {
			selectedMapElemType = MapElemType.KSFFigure;
		} else if ("3".equals(jecnActiveData.getActiveKeyType())) {
			selectedMapElemType = MapElemType.KCPFigure;
		} else if ("4".equals(jecnActiveData.getActiveKeyType())) {
			selectedMapElemType = MapElemType.KCPFigureComp;
		}
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	protected void intComponents() {
		int width = 580;
		int height = 400;
		boolean isShowEleAttrs = JecnDesignerCommon.isShowEleAttribute();
		if (isShowEleAttrs) {
			// 基本信息
			basicInfoPanel = new JecnPanel();
		} else {
			// 基本信息
			basicInfoPanel = new JecnPanel(width, height);
		}

		// 活动信息
		activeAtrrPanel = new JecnPanel();
		// 关键活动
		keyActivePanel = new JecnPanel();

		newPanel = new JecnPanel();

		// 编号标签
		activeNumLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activeNumBlank"));
		// 编号输入框
		activeNumTextField = new JTextField();
		// 编号提示信息框
		activeNumInfoTextArea = new JecnUserInfoTextArea();

		// 名称标签
		activeNameLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activeNameBlank"));
		// 名称输入框
		activeNameTextArea = new JTextArea();
		// 名称输入框容器
		activeNameScrollPane = new JScrollPane(activeNameTextArea);
		// 名称提示信息框
		activeNameInfoTextArea = new JecnUserInfoTextArea();

		// 说明标签
		activeNoteLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activeOperateNote"));
		// 说明输入框
		activeNoteTextArea = new JTextArea();
		// 说明输入区域容器
		activeNoteScrollPane = new JScrollPane(activeNoteTextArea);
		// 说明提示信息框
		activeNoteInfoTextArea = new JecnUserInfoTextArea();

		// PA\KSF\KCP类型标签
		keyActiveTypeLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("keyActiveTypeBlank"));
		// PA\KSF\KCP类型内容
		keyActiveTypeContLabel = new JLabel();
		// PA\KSF\KCP标签
		keyActiveNoteLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activeImportNote"));
		// PA\KSF\KCP输入区域
		keyActiveNoteTextArea = new JTextArea();
		// PA\KSF\KCP输入区域容器
		keyActiveNoteScrollPane = new JScrollPane(keyActiveNoteTextArea);
		// PA/KSF/KCP提示信息框
		keyActiveNoteInfoTextArea = new JecnUserInfoTextArea();

		// 对应内控矩阵风险点标签
		innerControlRiskLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("innerControlRiskC"));
		// 对应内控矩阵风险点输入框
		innerControlRiskTextArea = new JTextArea();
		// 对应内控矩阵风险点输入框容器
		innerControlRiskScrollPane = new JScrollPane(innerControlRiskTextArea);
		// 对应内控矩阵风险点提示信息框
		innerControlRiskInfoTextArea = new JecnUserInfoTextArea();

		// 对应标准条款标签
		standardConditionsLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("standardConditionsC"));
		// 对应标准条款输入框
		standardConditionsTextArea = new JTextArea();
		// 对应标准条款输入框容器
		standardConditionsScrollPane = new JScrollPane(standardConditionsTextArea);
		// 对应标准条款提示信息框
		standardConditionsInfoTextArea = new JecnUserInfoTextArea();

		/** ******** 【第三需求新增线上，活动类别】 *********** */

		// newPanel = new JecnPanel();
		// 大小
		Dimension size = new Dimension(480, 404);
		Dimension preferredSize = new Dimension(600, 500);
		this.setSize(preferredSize);
		this.setMinimumSize(size);
		// 活动信息
		activeAtrrPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		activeAtrrPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnResourceUtil
				.getJecnResourceUtil().getValue("activeAtrrTitle")));
		// 名称输入框
		activeNameTextArea.setRows(1);
		activeNameTextArea.setLineWrap(true);
		activeNameTextArea.setWrapStyleWord(true);
		activeNameTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));
		activeNameTextArea.setBorder(null);

		// 名称输入区域容器
		activeNameScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		activeNameScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 关键活动
		keyActivePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		keyActivePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnResourceUtil
				.getJecnResourceUtil().getValue("keyActiveTitle")));

		// 说明输入区域容器
		activeNoteScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		activeNoteScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		activeNoteScrollPane.setBorder(null);
		// PA\KSF\KCP输入区域容器
		keyActiveNoteScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		keyActiveNoteScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		keyActiveNoteScrollPane.setBorder(null);

		// 说明输入区域
		activeNoteTextArea.setRows(2);
		activeNoteTextArea.setLineWrap(true);
		activeNoteTextArea.setWrapStyleWord(true);
		activeNoteTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));
		// PA\KSF\KCP输入区域
		keyActiveNoteTextArea.setRows(2);
		keyActiveNoteTextArea.setLineWrap(true);
		keyActiveNoteTextArea.setWrapStyleWord(true);
		keyActiveNoteTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));

		newPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		newPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), ""));

		// 对应内控矩阵风险点输入区域容器
		innerControlRiskScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// innerControlRiskScrollPane
		// .setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		innerControlRiskScrollPane.setBorder(null);

		// 对应标准条款输入区域容器
		standardConditionsScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		standardConditionsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		standardConditionsScrollPane.setBorder(null);

		// 对应内控矩阵风险点输入框
		innerControlRiskTextArea.setRows(1);
		innerControlRiskTextArea.setLineWrap(true);
		innerControlRiskTextArea.setWrapStyleWord(true);
		innerControlRiskTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));
		// 对应标准条款输入框
		standardConditionsTextArea.setRows(2);
		standardConditionsTextArea.setLineWrap(true);
		standardConditionsTextArea.setWrapStyleWord(true);
		standardConditionsTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));

		// 事件
		// 编号
		activeNumTextField.addCaretListener(this);
		// 名称
		activeNameTextArea.addCaretListener(this);
		// 说明
		activeNoteTextArea.addCaretListener(this);
		// PA/KSF/KCP
		keyActiveNoteTextArea.addCaretListener(this);
		// 对应内控矩阵风险点
		innerControlRiskTextArea.addCaretListener(this);
		// 对应标准条款
		standardConditionsTextArea.addCaretListener(this);
		if (JecnConstants.loginBean.getOtherLoginType() != 4) {
			newPanel.setVisible(false);
			innerControlRiskLabel.setVisible(false);
			innerControlRiskScrollPane.setVisible(false);
			standardConditionsLabel.setVisible(false);
			standardConditionsScrollPane.setVisible(false);
		}

		initComs3();
		// 是否存在浏览端：如果不存在浏览端，则隐藏活动类别
		if (!JecnConstants.isPubShow() || !JecnConfigTool.isShowActiveType()) {
			activityTypeLable.setVisible(false);
			activityTypeBox.setVisible(false);
			activiteEditBut.setVisible(false);
		}

		if (JecnConfigTool.isDRYLOperType()) {// 东软医疗，活动编号不允许手动输入
			activeNumTextField.setEnabled(false);
		}

	}

	/**
	 * 组件初始化
	 * 
	 */
	private void initComs3() {
		/** ******** 【第三需求新增线上，活动类别】 *********** */
		// 活动类别：
		activityTypeLable = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activeType"));
		activityTypeBox = new JComboBox();
		// 活动类别编辑按钮（编辑）
		activiteEditBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("activeEdit"));

		activityTypeBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		/**
		 * 活动类别 编辑按钮监听
		 * 
		 */
		activiteEditBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				activiteEditButListener();
			}
		});
	}

	/**
	 * 活动类别 编辑按钮监听
	 * 
	 */
	private void activiteEditButListener() {
		JecnActiveTypeDialog activeTypeDialog = new JecnActiveTypeDialog();
		// 活动类型集合
		activeTypeDialog.setVisible(true);
		addActiveComboxItem();
	}

	/**
	 * 
	 * 活动信息
	 * 
	 */
	protected void initLayout() {

		int h = 7;
		int v = 3;

		if (JecnDesignerCommon.isShowEleAttribute()) {
			h = 1;
		}
		/** 标签间距 */
		Insets insetsLabel = new Insets(h, 35, h, 3);
		/** 内容间距 */
		Insets insetsContent = new Insets(h, 3, h, 5);

		// 活动信息
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, new Insets(v, h, 0, h), 0, 0);
		basicInfoPanel.add(activeAtrrPanel, c);
		// 添加到活动信息面板上
		// 关键活动
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(
				v, h, v, h), 0, 0);
		basicInfoPanel.add(keyActivePanel, c);
		// newPanel
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(
				v, h, v, h), 0, 0);
		basicInfoPanel.add(newPanel, c);
		// //////////////// 活动信息//////////////////

		addActiveBaseInfo(insetsLabel, insetsContent);

		// ////////////////活动信息//////////////////

		// ////////////////关键活动//////////////////
		JecnPanel typePanel = new JecnPanel();
		// PA\KSF\KCP类型标签
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		typePanel.add(keyActiveTypeLabel, c);
		// PA\KSF\KCP类型内容
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		typePanel.add(keyActiveTypeContLabel, c);
		// 类型标签
		c = new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		keyActivePanel.add(typePanel, c);

		// PA\KSF\KCP标签,关键活动重要说明
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insetsLabel, 0, 0);
		keyActivePanel.add(keyActiveNoteLabel, c);
		// PA\KSF\KCP输入区域
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		keyActivePanel.add(keyActiveNoteScrollPane, c);
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.standKeyActivityInfoNotEmpty)
				&& selectedMapElemType != null) {
			c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(3, 1, 3, 1), 0, 0);
			keyActivePanel.add(new CommonJlabelMustWrite(), c);
		}
		// 说明信息提示
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		keyActivePanel.add(keyActiveNoteInfoTextArea, c);
		// ////////////////关键活动//////////////////

		// //////////////// newPanel对应内控矩阵风险点 对应标准条款//////////////////
		// 对应内控矩阵风险点标签
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		newPanel.add(innerControlRiskLabel, c);
		// 对应内控矩阵风险点输入框
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		newPanel.add(innerControlRiskScrollPane, c);
		// 对应内控矩阵风险点信息提示
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		newPanel.add(innerControlRiskInfoTextArea, c);

		// 对应标准条款标签
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		newPanel.add(standardConditionsLabel, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		newPanel.add(standardConditionsScrollPane, c);
		// 对应标准条款信息提示
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		newPanel.add(standardConditionsInfoTextArea, c);

		// ////////////////newPanel//////////////////

		// 基本信息
		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		this.add(basicInfoPanel, c);
	}

	private void addActiveBaseInfo(Insets insetsLabel, Insets insetsContent) {
		int rows = 0;
		// 编号标签
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
				GridBagConstraints.NONE, insetsLabel, 0, 0);
		activeAtrrPanel.add(activeNumLabel, c);
		// 编号输入框
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		activeAtrrPanel.add(activeNumTextField, c);
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.standActivityNumberNotEmpty)) {
			c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(3, 1, 3, 1), 0, 0);
			activeAtrrPanel.add(new CommonJlabelMustWrite(), c);
		}
		rows++;
		// 编号信息提示
		c = new GridBagConstraints(1, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		activeAtrrPanel.add(activeNumInfoTextArea, c);
		rows++;

		// 名称标签
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
				insetsLabel, 0, 0);
		activeAtrrPanel.add(activeNameLabel, c);
		// 名称输入框
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		activeAtrrPanel.add(activeNameScrollPane, c);
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.standActivityNameNotEmpty)) {
			c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(3, 1, 3, 1), 0, 0);
			activeAtrrPanel.add(new CommonJlabelMustWrite(), c);
		}
		rows++;
		// 名称信息提示
		c = new GridBagConstraints(1, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		activeAtrrPanel.add(activeNameInfoTextArea, c);
		rows++;

		/** 【插入三期需求，线上：活动类别】 */
		/** *** 【添加活动类别】 ****** */
		c = new GridBagConstraints(0, rows, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		activeAtrrPanel.add(activityTypeLable, c);
		c = new GridBagConstraints(1, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		activeAtrrPanel.add(activityTypeBox, c);
		if (JecnDesignerCommon.isAdmin()) {
			c = new GridBagConstraints(2, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insetsContent, 0, 0);
			activeAtrrPanel.add(activiteEditBut, c);
		}

		rows++;

		if (JecnConfigTool.isEpsonLogin()) {
			activeEventField = new CommonJTextField(rows, activeAtrrPanel, insetsLabel, JecnProperties
					.getValue("activityD"));
			rows++;
			errorEventLabel = new JLabel();
			errorEventLabel.setForeground(Color.red);
			c = new GridBagConstraints(1, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					new Insets(0, 0, 0, 0), 0, 0);
			activeAtrrPanel.add(errorEventLabel, c);
			rows++;
		}

		// 说明标签
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insetsLabel, 0, 0);
		activeAtrrPanel.add(activeNoteLabel, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		activeAtrrPanel.add(activeNoteScrollPane, c);
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.standActivityInfoNotEmpty)) {
			c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(3, 1, 3, 1), 0, 0);
			activeAtrrPanel.add(new CommonJlabelMustWrite(), c);
		}
		rows++;
		// 说明信息提示
		c = new GridBagConstraints(1, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		activeAtrrPanel.add(activeNoteInfoTextArea, c);
		rows++;

	}

	/**
	 * 
	 * 初始化数据
	 * 
	 */
	protected void initData() {
		// 编号
		this.activeNumTextField.setText(jecnActiveData.getActivityNum());

		// 名称
		this.activeNameTextArea.setText(jecnActiveData.getFigureText());
		// 说明
		this.activeNoteTextArea.setText(jecnActiveData.getAvtivityShow());

		if (selectedMapElemType != null) {// 关键活动存在情况

			// PA\KSF\KCP输入区域启用
			this.keyActiveNoteTextArea.setEnabled(true);

			// PA/KSF/KCP的说明
			this.keyActiveNoteTextArea.setText(jecnActiveData.getAvtivityShowAndControl());

			// 设置类型
			keyActiveTypeContLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue(selectedMapElemType));

			this.keyActivePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
					JecnResourceUtil.getJecnResourceUtil().getValue(selectedMapElemType + "Title")));

		} else {// 不存在关键活动
			// PA\KSF\KCP输入区域不启用
			this.keyActiveNoteTextArea.setEnabled(false);

			// 设置类型
			keyActiveTypeContLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue("noneFigure"));
			this.keyActiveNoteTextArea.setText("");
		}

		// 对应内控矩阵风险点
		this.innerControlRiskTextArea.setText(jecnActiveData.getInnerControlRisk());
		// 对应标准条款
		this.standardConditionsTextArea.setText(jecnActiveData.getStandardConditions());
		// 初始化活动类别
		addActiveComboxItem();

		if (activeEventField != null) {
			activeEventField.getTextField().setText(jecnActiveData.getCustomOne());
		}
	}

	/**
	 * 初始化获取活动类别集合
	 * 
	 */
	private void getActiveTypeBean() {
		try {
			listActiveTypeBean = ConnectionPool.getActivityAction().findJecnActiveTypeBeanList();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, JecnProperties.getValue("accessServerError"));
			return;
		}
	}

	/**
	 * 
	 * 输入框的插入字符事件
	 * 
	 */
	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	protected void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == activeNumTextField) {// 编号输入框
			// 编号长度超过122个字符或61个汉字
			checkTextFieldLength(activeNumInfoTextArea, activeNumTextField);
		} else if (e.getSource() == activeNameTextArea) {// 名称输入框
			// 名称长度超过122个字符或61个汉字
			checkTextFieldLength(activeNameInfoTextArea, activeNameTextArea);
		} else if (e.getSource() == keyActiveNoteTextArea) {// PA\KSF\KCP输入区域
			// 说明长度是否超过1200个字符或600个汉字
			checkTextAreaLength(keyActiveNoteInfoTextArea, keyActiveNoteTextArea);
		} else if (e.getSource() == innerControlRiskTextArea) {// 对应内控矩阵风险点
			// 对应内控矩阵风险点长度超过122个字符或61个汉字
			checkTextFieldLength(innerControlRiskInfoTextArea, innerControlRiskTextArea);
		} else if (e.getSource() == standardConditionsTextArea) {// 对应标准条款
			// 对应标准条款长度是否超过1200个字符或600个汉字
			checkTextAreaLength(standardConditionsInfoTextArea, standardConditionsTextArea);
		}
	}

	/**
	 * 
	 * 点击确认按钮提交前校验
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	protected boolean okBtnCheck() {
		boolean ret = true;
		// 编号
		ret = checkTextFieldLength(activeNumInfoTextArea, activeNumTextField);
		// 名称
		boolean nameRet = checkTextFieldLength(activeNameInfoTextArea, activeNameTextArea);
		if (!nameRet) {
			ret = nameRet;
		}
		// PA/KSF/KCP说明
		boolean keyActiveNoteRet = checkTextAreaLength(keyActiveNoteInfoTextArea, keyActiveNoteTextArea);
		if (!keyActiveNoteRet) {
			ret = keyActiveNoteRet;
		}

		// 对应内控矩阵风险点
		boolean innerControlRiskRet = checkTextFieldLength(innerControlRiskInfoTextArea, innerControlRiskTextArea);
		if (!innerControlRiskRet) {
			ret = innerControlRiskRet;
		}
		// 对应标准条款
		boolean standardConditionsRet = checkTextAreaLength(standardConditionsInfoTextArea, standardConditionsTextArea);
		if (!standardConditionsRet) {
			ret = standardConditionsRet;
		}

		if (JecnConfigTool.isEpsonLogin() && activeEventField.isValidateNotPass(errorEventLabel)) {
			ret = false;
		}

		return ret;
	}

	/**
	 * 确定按钮更新活动类型信息
	 * 
	 */
	public void setActiveTypeResult() {
		if (JecnConfigTool.isShowActiveType()) {
			int index = activityTypeBox.getSelectedIndex();
			if (index == 0) {
				jecnActiveData.setActiveTypeId(null);
				return;
			}
			index--;
			JecnActiveTypeBean activeTypeBean = listActiveTypeBean.get(index);
			jecnActiveData.setActiveTypeId(activeTypeBean.getTypeId());
		}
	}

	/**
	 * 
	 * 校验JTextField的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textField
	 *            JTextField
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextFieldLength(JecnUserInfoTextArea infoTextArea, JTextField textField) {
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNameLength(textField.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 校验JTextArea的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textArea
	 *            JTextArea
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextFieldLength(JecnUserInfoTextArea infoTextArea, JTextArea textArea) {
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNameLength(textArea.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 校验JTextArea的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textArea
	 *            JTextArea
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextAreaLength(JecnUserInfoTextArea infoTextArea, JTextArea textArea) {
		if (!textArea.isEnabled()) {// 未启用就不用校验
			return true;
		}
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNoteLength(textArea.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 活动类别 activityTypeBox
	 */
	protected void addActiveComboxItem() {
		getActiveTypeBean();
		try {
			if (listActiveTypeBean == null) {
				JOptionPane.showMessageDialog(this, "method addActiveComboxItem: "
						+ JecnProperties.getValue("accessServerError"));
				return;
			}
			// 动态读取活动类型
			// 清空活动类别
			activityTypeBox.removeAllItems();
			activityTypeBox.addItem("");
			if (jecnActiveData.getActiveTypeId() == null) {
				activityTypeBox.setSelectedIndex(0);
			}
			for (JecnActiveTypeBean activeTypeBean : listActiveTypeBean) {
				// 活动ID 与活动类别中的ID相等，设置该活动类别选中
				String selectItem = activeTypeBean.getTypeName();
				String selectIndex = activeTypeBean.getTypeId().toString();
				// 将活动类型显示到JCombox中
				activityTypeBox.addItem(selectItem);
				if (jecnActiveData.getActiveTypeId() != null
						&& selectIndex.equals(jecnActiveData.getActiveTypeId().toString())) {// 根据活动类型ID获取对应的类型名称
					activityTypeBox.setSelectedItem(selectItem);
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/**
	 * 
	 * 判断是否有更新，有返回true，没有返回false
	 * 
	 * @param newNum
	 * @param newName
	 * @param newNote
	 * @param newKeyActiveNote
	 * @return boolean 有返回true，没有返回false
	 */
	public boolean isUpdate() {

		// 编号
		String newNum = this.activeNumTextField.getText();
		// 名称
		String newName = this.activeNameTextArea.getText();
		// 说明
		String newNote = this.activeNoteTextArea.getText();
		// PA/KSF/KCP
		String newKeyActiveNote = this.keyActiveNoteTextArea.getText();
		// 对应内控矩阵风险点
		String innerControlRisk = this.innerControlRiskTextArea.getText();
		// 对应标准条款
		String standardConditions = this.standardConditionsTextArea.getText();

		if (!DrawCommon.checkStringSame(newNum, jecnActiveData.getActivityNum())
				|| !DrawCommon.checkStringSame(newName, jecnActiveData.getFigureText())
				|| !DrawCommon.checkStringSame(newNote, jecnActiveData.getAvtivityShow())
				|| (selectedMapElemType != null && !DrawCommon.checkStringSame(newKeyActiveNote, jecnActiveData
						.getAvtivityShowAndControl()))
				|| !DrawCommon.checkStringSame(innerControlRisk, jecnActiveData.getInnerControlRisk())
				|| !DrawCommon.checkStringSame(standardConditions, jecnActiveData.getStandardConditions())) {
			return true;
		}
		if (JecnConfigTool.isShowActiveType()) {
			int index = activityTypeBox.getSelectedIndex();
			if (index == 0) {
				if (jecnActiveData.getActiveTypeId() != null) {
					return true;
				} else {
					return false;
				}
			}
			index--;
			JecnActiveTypeBean activeTypeBean = listActiveTypeBean.get(index);
			if (!DrawCommon.checkLongSame(jecnActiveData.getActiveTypeId(), activeTypeBean.getTypeId())) {
				return true;
			}
		}

		if (activeEventField != null && activeEventField.isUpdate()) {
			return true;
		}
		return false;
	}

	public JTextField getActiveNumTextField() {
		return activeNumTextField;
	}

	public JTextArea getActiveNameTextArea() {
		return activeNameTextArea;
	}

	public JTextArea getActiveNoteTextArea() {
		return activeNoteTextArea;
	}

	public JTextArea getKeyActiveNoteTextArea() {
		return keyActiveNoteTextArea;
	}

	public JecnActiveData getJecnActiveData() {
		return jecnActiveData;
	}

	public String getCustonOne() {
		return activeEventField == null ? null : activeEventField.getTextValue();
	}

}
