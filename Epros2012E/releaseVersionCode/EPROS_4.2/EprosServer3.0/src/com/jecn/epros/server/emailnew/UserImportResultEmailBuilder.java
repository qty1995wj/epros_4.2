package com.jecn.epros.server.emailnew;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;

public class UserImportResultEmailBuilder extends FixedContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo() {
		Object[] data = getData();
		File attachment = (File) data[0];
		String attachmentName = data[1].toString();

		return createEmail(attachment, attachmentName);
	}

	public EmailBasicInfo createEmail(File attachment, String attachmentName) {

		// 内容 ：人员同步存在变更信息，请查阅！
		StringBuffer buf = new StringBuffer();
		buf.append(SyncConstant.IMPORT_MAIL_CONTENT1);
		if (JecnConfigTool.isDongRuanYiLiaoLoginType()) {
			buf.append("&nbsp;&nbsp;&nbsp;&nbsp;人员同步存在信息变更，变更信息为附件，请查阅！<BR><BR>");
		} else {
			buf.append(getContent2());
			buf.append(JecnConfigTool.getEmailPlatName());
		}
		buf.append(EmailContentBuilder.strBr);
		buf.append(EmailContentBuilder.strBr);
		buf.append("此邮件为系统自动发送，请不要直接回复，谢谢！");
		buf.append(EmailContentBuilder.strBr);
		buf.append(JecnCommon.getStringbyDateHMS(new Date()));

		// 人员变更信息
		String subject = SyncConstant.IMPORT_MAIL_SUBJET;

		// 附件
		EmailAttachment emailAttachment = new EmailAttachment();
		emailAttachment.setAttachment(attachment);
		emailAttachment.setAttachmentName(attachmentName);
		List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
		attachments.add(emailAttachment);

		EmailBasicInfo basicInfo = new EmailBasicInfo();
		basicInfo.setContent(buf.toString());
		basicInfo.setSubject(subject);
		basicInfo.setAttachments(attachments);

		return basicInfo;

	}

	private String getContent2() {
		String result = "&nbsp;&nbsp;&nbsp;&nbsp;" + JecnConfigTool.getEmailPlatName()
				+ "人员同步存在信息变更，变更信息为附件，请查阅！<BR><BR>";
		return result;
	}

}
