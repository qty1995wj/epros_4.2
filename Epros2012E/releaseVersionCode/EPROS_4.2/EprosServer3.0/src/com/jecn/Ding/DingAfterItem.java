package com.jecn.Ding;

import java.util.ResourceBundle;

/**
 * 
 * 钉钉配置信息
 * 
 */
public class DingAfterItem {

	private static ResourceBundle dingConfig;

	static {
		dingConfig = ResourceBundle.getBundle("cfgFile/Ding/DingConfig");
	}

	public static String getValue(String key) {
		return dingConfig.getString(key);
	}
}
