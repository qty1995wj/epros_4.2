package com.jecn.epros.domain.process;

public class ProceeRuleTypeBean implements java.io.Serializable {
    private Long typeId;//主键ID
    private String typeName;//类型名称

    public ProceeRuleTypeBean() {

    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

}
