package epros.designer.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Logger;

import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.integration.risk.RiskResourceTreeMenu;
import epros.designer.gui.system.Callback;
import epros.designer.tree.JecnTreeNode;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.ImportFlowFilter;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUserCheckUtil;

public class JecnDataImportUtil {
	private static Logger log = Logger.getLogger(JecnDataImportUtil.class);

	public static void importDataByType(final String type, final JecnTreeNode selectNode, Callback<Void> exec) {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFileManageDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);
		fileChoose.setEidtTextFiled(false);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("*.xls", "xls");
		fileChoose.setFileFilter(filter);
		fileChoose.setAcceptAllFileFilterUsed(false);

		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("updateFile"));
		int i = fileChoose.showSaveDialog(null);
		if (i == JFileChooser.APPROVE_OPTION) {
			final File file = fileChoose.getSelectedFile();
			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSaveFileManageDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			final JecnTreeBean treeBean = selectNode.getJecnTreeBean();

			SwingWorker<ByteFileResult, Void> submitTaskExcute = new SwingWorker<ByteFileResult, Void>() {
				@Override
				protected ByteFileResult doInBackground() throws Exception {
					ByteFileResult result = null;
					try {
						result = ConnectionPool.getStandardAction().importData(JecnConstants.getUserId(),
								JecnUtil.changeFileToByte(file.getPath()), treeBean.getId(), type,
								JecnConstants.projectId, JecnConfigTool.CH());
					} catch (Exception e) {
						log.error(e);
					}
					return result;
				}

				@Override
				public void done() {
					JecnLoading.stop();
				}
			};

			String error = JecnLoading.start(submitTaskExcute);
			try {
				final ByteFileResult result = submitTaskExcute.get();
				if (error == null && result != null) {
					if (result.isSuccess()) {
						exec.call(null);
						JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
								.getValue("importSuccess"));
					} else {// 弹出下载提示框 导入失败，有错误数据是否下载？
						int dialog = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("importFailDownloadData"), "",
								JecnOptionPane.YES_NO_OPTION);
						if (dialog == JecnOptionPane.YES_OPTION) {
							SwingWorker<Boolean, Void> ex = new SwingWorker<Boolean, Void>() {
								@Override
								protected Boolean doInBackground() throws Exception {
									return downloadErrorFile(result.getBytes());
								}

								@Override
								public void done() {
									JecnLoading.stop();
								}
							};
							String r = JecnLoading.start(ex);
							if (r != null || !ex.get()) {
								JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("downloadFail"));
							}
						}
					}
				} else {
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("importFail"));
				}
			} catch (Exception e) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("importFail"));
				log.error("", e);
			}
		}
	}

	private static boolean downloadErrorFile(byte[] b) throws Exception {
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;
		try {
			// 获取默认路径
			String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFlowDirectory();
			// 获取截取后正确的路径
			if (pathUrl != null && !"".equals(pathUrl)) {
				pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
			}
			JecnFileChooser fileChooser = new JecnFileChooser(pathUrl);
			// 移除系统给定的文件过滤器
			fileChooser.setAcceptAllFileFilterUsed(false);
			// 过滤文件
			fileChooser.setFileFilter(new ImportFlowFilter("xls"));
			// 文件名称获取当前面板名称
			fileChooser.setSelectedFile(new File("data"));
			int i = fileChooser.showSaveDialog(null);
			// 选择确认（yes、ok）后返回该值。
			if (i == JFileChooser.APPROVE_OPTION) {
				fileChooser.getTextFiled().getText();
				// 获取输出文件的路径
				String fileDirectory = fileChooser.getSelectedFile().getPath();
				String pathName = fileChooser.getSelectedFile().getName();
				String error = JecnUserCheckUtil.checkFileName(pathName);
				// 设置上传访问路径
				// 修改配置文件中的路径
				JecnSystemData.readCurrSystemFileIOData().setSaveFlowDirectory(fileDirectory);
				JecnSystemData.writeCurrSystemFileIOData();
				if (!"".equals(error)) {
					JecnOptionPane.showMessageDialog(null, error, "", JecnOptionPane.ERROR_MESSAGE);
					return false;
				}
				String xmlPath = fileDirectory;
				if (!fileDirectory.endsWith(".xls")) {
					xmlPath = fileDirectory + ".xls";
				}
				try {
					fos = new FileOutputStream(xmlPath);
				} catch (Exception e) {
					log.error("", e);
				}
				bos = new BufferedOutputStream(fos);
				bos.write(b);
				bos.flush();
				return true;
			}
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					log.error("", e);
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("", e);
				}
			}
		}
		return false;
	}

}
