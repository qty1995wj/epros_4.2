package epros.designer.gui.common;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.PubSourceData;
import com.jecn.epros.server.bean.system.PubSourceSave;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.system.DepartMutilSelectCommon;
import epros.designer.gui.system.PeopleMutilSelectCommon;
import epros.designer.gui.system.PosGroupMutilSelectCommon;
import epros.designer.gui.system.PosMutilSelectCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 */
public class JecnPubNoteDialog extends JecnDialog {
	Logger log = Logger.getLogger(JecnPubNoteDialog.class);
	/** 主面板 */
	private JecnPanel panel = new JecnPanel();
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();
	/** 主面板底部按钮面板，包括确定、取消按钮 */
	private JecnPanel botButtonPanel = new JecnPanel();

	// 发送提醒邮件
	private JButton tipBut = new JButton(JecnProperties.getValue("releaseReminderByEmail"));
	// 确定
	private JButton okBut = new JButton(JecnProperties.getValue("saveBut"));
	// 取消
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	private JecnTreeNode node;

	/** 部门多选 */
	DepartMutilSelectCommon departMutilSelectCommon = null;
	/** 岗位多选 */
	PosMutilSelectCommon posMutilSelectCommon = null;
	/** 岗位组多选 */
	PosGroupMutilSelectCommon posGroupMutilSelectCommon = null;
	/** 人员多选 **/
	PeopleMutilSelectCommon peopleMutilSelectCommon = null;

	private PubSourceData data;

	public JecnPubNoteDialog(JecnTreeNode node) {
		this.node = node;
		this.setSize(530, 430);
		this.setResizable(true);
		this.setModal(true);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);
		initData();
		initCompotents();
		initLayout();
	}

	private void initData() {
		int type = JecnUtil.getTypeByNode(node.getJecnTreeBean().getTreeNodeType());
		try {
			this.data = ConnectionPool.getProcessAction().getPubSourceData(node.getJecnTreeBean().getId(), type);
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
	}

	private void initLayout() {
		int rows = 0;
		Insets insets = new Insets(3, 5, 3, 5);
		departMutilSelectCommon = new DepartMutilSelectCommon(rows, mainPanel, insets, this.data.getOrgs(), this,
				false, JecnProperties.getValue("deptChoose"));
		rows++;
		// ================岗位组=======================================
		posGroupMutilSelectCommon = new PosGroupMutilSelectCommon(rows, mainPanel, insets, this.data.getPosGroups(),
				this, false, false, JecnProperties.getValue("positionGroupSelect"));
		rows++;
		// ============= 岗位=========================================
		posMutilSelectCommon = new PosMutilSelectCommon(rows, mainPanel, insets, this.data.getPoses(), this, false,
				JecnProperties.getValue("positionSelect"));
		rows++;
		// ================人员=======================================
		peopleMutilSelectCommon = new PeopleMutilSelectCommon(rows, mainPanel, insets, this.data.getPeoples(),
				JecnProperties.getValue("personChoose"), this, false);
		rows++;

		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0);
		panel.add(mainPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		panel.add(botButtonPanel, c);
		botButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		botButtonPanel.add(tipBut);
		botButtonPanel.add(okBut);
		botButtonPanel.add(cancelBut);
		this.setContentPane(panel);
		okBut.setVisible(false);
	}

	private void initCompotents() {
		this.setTitle(JecnProperties.getValue("releaseReminderByEmail"));
		Dimension dimension = new Dimension(380, 145);

		// 取消事件，关闭窗口
		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnPubNoteDialog.this.dispose();
			}
		});
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButAction();
			}
		});
		tipBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tipButAction();
			}
		});
	}

	protected void tipButAction() {
		try {
			// 保存
			PubSourceSave pubNoteData = getPubNoteData();
			ConnectionPool.getProcessAction().saveOrUpdatePubCopyNote(pubNoteData);
			ConnectionPool.getProcessAction().sendPubCopyNoteEmails(pubNoteData);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("operateSuccess"));
			JecnPubNoteDialog.this.dispose();
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private void okButAction() {
		try {
			ConnectionPool.getProcessAction().saveOrUpdatePubCopyNote(getPubNoteData());
			JecnPubNoteDialog.this.dispose();
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private PubSourceSave getPubNoteData() {
		PubSourceSave s = new PubSourceSave();
		s.setOrgs(departMutilSelectCommon.getResultIdsSet());
		s.setPoses(posMutilSelectCommon.getResultIdsSet());
		s.setPeoples(peopleMutilSelectCommon.getResultIdsSet());
		s.setGroups(posGroupMutilSelectCommon.getResultIdsSet());
		s.setPeopleId(JecnConstants.getUserId());
		List<JecnTreeBean> resources = new ArrayList<JecnTreeBean>();
		resources.add(node.getJecnTreeBean());
		s.setResources(resources);
		return s;
	}
}
