/*
 * JecnFlowRecordT.java
 *
 * Created on 2010��6��23��, ����2:39
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jecn.epros.server.bean.process;


/**
 * @author yxw 2012-11-5
 * @description：
 */

public class JecnFlowRecordT implements java.io.Serializable{
	/**主键ID*/
    private Long id;
    /**流程ID*/
    private Long flowId;
    /**记录名称*/
    private String recordName;
    /**保存责任人*/
    private String recordSavePeople;
    /**保存场所*/
    private String saveLaction;
    /**归档时间*/
    private String fileTime;
    /**保存期限*/
    private String saveTime;
    /**到期处理方式*/
    private String recordApproach;
    /**移交责任人**/
    private String recordTransferPeople;
    /**编号**/
    private String docId;
    
    /** Creates a new instance of JecnFlowRecordT */
    public JecnFlowRecordT() {
        
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public Long getFlowId() {
        return flowId;
    }
    
    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }
    
    public String getRecordName() {
        return recordName;
    }
    
    public void setRecordName(String recordName) {
        this.recordName = recordName;
    }
    
    public String getRecordSavePeople() {
        return recordSavePeople;
    }
    
    public void setRecordSavePeople(String recordSavePeople) {
        this.recordSavePeople = recordSavePeople;
    }
    
    public String getSaveLaction() {
        return saveLaction;
    }
    
    public void setSaveLaction(String saveLaction) {
        this.saveLaction = saveLaction;
    }
    
    public String getRecordApproach() {
        return recordApproach;
    }
    
    public void setRecordApproach(String recordApproach) {
        this.recordApproach = recordApproach;
    }
    
    public String getFileTime() {
        return fileTime;
    }
    
    public void setFileTime(String fileTime) {
        this.fileTime = fileTime;
    }
    
    public String getSaveTime() {
        return saveTime;
    }
    
    public void setSaveTime(String saveTime) {
        this.saveTime = saveTime;
    }

	public String getRecordTransferPeople() {
		return recordTransferPeople;
	}

	public void setRecordTransferPeople(String recordTransferPeople) {
		this.recordTransferPeople = recordTransferPeople;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}
    
}
