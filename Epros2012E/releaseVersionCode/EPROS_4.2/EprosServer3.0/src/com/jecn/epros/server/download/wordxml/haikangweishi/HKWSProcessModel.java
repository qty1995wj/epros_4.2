package com.jecn.epros.server.download.wordxml.haikangweishi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ParagraphSpaceBean;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.bean.paragraph.GroupBean.Hanchor;
import wordxml.element.bean.paragraph.GroupBean.Vanchor;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnResourceUtil;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 海康威视操作说明模版
 * 
 * @author hyl
 * 
 */
public class HKWSProcessModel extends ProcessFileModel {
	/*** 段落行距 1.5倍行距 *****/
	private final ParagraphLineRule vLine2 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);
	/*** 段落行距 单倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距最小值12磅 *****/
	private final ParagraphLineRule leastVline12 = new ParagraphLineRule(12F, LineRuleType.AT_LEAST);

	public HKWSProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(false);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(17.38F);
		getDocProperty().setNewRowHeight(0.7F);
		getDocProperty().setFlowChartScaleValue(7.5F);
		setDocStyle(" ", textTitleStyle());
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, new ParagraphLineRule(20F, LineRuleType.EXACT));
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		float[] width = null;
		if (JecnContants.showPosNameBox) {
			width = new float[] { 3.5F, 3.5F, 7.5F };
		} else if (!JecnContants.showPosNameBox) {
			width = new float[] { 5.0F, 9.5F };
		}
		List<String[]> rowData2 = new ArrayList<String[]>();
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = "对应职位/岗位";
		// 角色职责
		String roleResponsibility = "职责";
		if (JecnContants.showPosNameBox) {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility, positionName });
		} else {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility });
		}
		for (String[] str : rowData) {
			if (JecnContants.showPosNameBox) {
				rowData2.add(new String[] { str[0], str[2], str[1] });
			} else {
				rowData2.add(new String[] { str[0], str[2] });
			}
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), getDocProperty()
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {
		createTitle(processFileItem);
		// 活动编号
		String activityNumber = JecnUtil.getValue("activitynumbers");
		// 执行角色s
		String executiveRole = JecnUtil.getValue("executiveRole");
		// 活动名称
		String eventTitle = JecnUtil.getValue("activityTitle");
		// 活动说明
		String activityDescription = "活动内容";
		String input = "输入";
		String output = "输出";
		float[] width = new float[] { 1.19F, 2.5F, 2.5F, 5.25F, 2F, 2.04F };
		List<Object[]> newRowData = new ArrayList<Object[]>();
		// 标题数组
		newRowData.add(new Object[] { activityNumber, eventTitle, executiveRole, activityDescription, input, output });
		// 添加数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出
		for (Object[] activeData : processDownloadBean.getAllActivityShowList()) {
			newRowData.add(new Object[] { activeData[0], activeData[2], activeData[1], activeData[3], activeData[4],
					activeData[5] });
		}
		
		Table tbl = createTableItem(processFileItem, width, JecnWordUtil.obj2String(newRowData));
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());

	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		// KPI配置bean
		List<JecnConfigItemBean> configItemBeanList = processDownloadBean.getConfigKpiItemBean();
		createTitle(processFileItem);
		// 是否创建表格风格段落
		boolean flagTab = oldRowData.size() > 1 ? true : false;
		ParagraphBean tPbean = getDocProperty().getTextTitleStyle();

		ParagraphBean pBean = new ParagraphBean(tPbean.getFontBean());
		pBean.getFontBean().setB(false);
		if (tPbean.getParagraphIndBean() != null) {
			pBean.setInd(tPbean.getParagraphIndBean().getLeft(), tPbean.getParagraphIndBean().getRight(), tPbean
					.getParagraphIndBean().getHanging(), 0);
		}

		// 表格内容
		int count = 0;
		for (String[] row : oldRowData) { // 对应一条配置数据
			List<String[]> tableData = new ArrayList<String[]>();// 一个表格数据
			String[] rowContent = null;// 表格一行数据
			// boolean isExitKpiName = false;
			if (flagTab) {
				String number = "" + numberMark + "." + ++count;
				processFileItem.getBelongTo().createParagraph(number, pBean);
			}
			for (JecnConfigItemBean config : configItemBeanList) {
				if ("0".equals(config.getValue())) {
					continue;
				}
				rowContent = new String[2];
				rowContent[0] = config.getName();
				rowContent[1] = getKpiContent(row, config.getMark());
				tableData.add(rowContent);
			}
			Table tbl = createTableItem(processFileItem, new float[] { 4.25F, 11.15F }, tableData);
			tbl.setCellStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow());

		}

	}

	/**
	 * * 0 KPI的名称 1 类型 2 目标值 3 kpi定义 4 kpi统计方法 5 数据统计时间频率 6 KIP值单位名称 7 数据获取方式 8
	 * 相关度 9 指标来源 10 数据提供者名称 11 支撑的一级指标内容 12 目的 13 测量点 14 统计周期 15 说明
	 */
	private String getKpiContent(String[] row, String mark) {
		if ("kpiName".equals(mark)) {// KPI名称
			return row[0];
		} else if ("kpiType".equals(mark)) {// KPI类型
			return row[1];
		} else if ("kpiUtilName".equals(mark)) {// KPI单位名称
			return JecnResourceUtil.getKpiVertical(Integer.valueOf(row[6]));
		} else if ("kpiTargetValue".equals(mark)) {// KPI目标值
			return row[2];
		} else if ("kpiTimeAndFrequency".equals(mark)) {// 数据统计时间频率
			return JecnResourceUtil.getKpiHorizontal(Integer.valueOf(row[5]));
		} else if ("kpiDefined".equals(mark)) {// KPI定义
			return row[3];
		} else if ("kpiDesignFormulas".equals(mark)) {// 流程KPI计算方式
			return row[4];
		} else if ("kpiDataProvider".equals(mark)) {// 数据提供者
			return row[10];
		} else if ("kpiDataAccess".equals(mark)) {// 数据获取方式
			return JecnResourceUtil.getKpiDataMethod(Integer.valueOf(row[7]));
		} else if ("kpiIdSystem".equals(mark)) {// IT 系统
			return row[16];
		} else if ("kpiSourceIndex".equals(mark)) {// 指标来源
			return JecnResourceUtil.getKpiTargetType(Integer.valueOf(row[9]));
		} else if ("kpiRrelevancy".equals(mark)) {// 相关度
			return JecnResourceUtil.getKpiRelevance(Integer.valueOf(row[8]));
		} else if ("kpiSupportLevelIndicator".equals(mark)) {// 支持的一级指标
			return row[11];
		} else if ("kpiPurpose".equals(mark)) {// 设置目的
			return row[12];
		} else if ("kpiPoint".equals(mark)) {// 测量点
			return row[13];
		} else if ("kpiPeriod".equals(mark)) {// 统计周期
			return row[14];
		} else if ("kpiExplain".equals(mark)) {// 说明
			return row[15];
		}
		return "";
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.5F, 2F, 2.4F, 2F, 2.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(1F, 2F, 2.4F, 2F, 1F, 1.75F);
		titleSect.setSectBean(sectBean);
		addTitleSectContent();
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent() {

		Sect titleSect = docProperty.getTitleSect();
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 10.5F));
		// 设置页边距
		sectBean.setPage(1F, 2.5F, 2F, 1.5F, 0F, 0F);
		sectBean.setSize(21, 29.7F);
		titleSect.setSectBean(sectBean);

		Paragraph emptyParagraph = createEmptyParagraph();
		// bfw
		createBFW(titleSect);
		titleSect.addParagraph(emptyParagraph);
		// 公司名称
		createTitleComponyName(titleSect);
		titleSect.addParagraph(emptyParagraph);
		// Q/BFW.XXX/XXX/XXX_PXX
		standardVersion(titleSect);
		// 一条线
		createTopLine(titleSect);
		// 流程名称
		createProcessName(titleSect);
		// 密级 有效期等
		createSec(titleSect);
		// 发布行
		createPubLine(titleSect);
		// 一条线
		createBottomLine(titleSect);
	}

	private void createSec(Sect titleSect) {
		ParagraphBean pubBean = new ParagraphBean(Align.center, "黑体", Constants.wuhao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 6, GroupH.exact, 3.5).setHorizontal(7.5, Hanchor.page, 0)
				.setVertical(17, Vanchor.page, 0.32).lock(true);
		pubGroup.addParagraph(new Paragraph("密级级别：" + getPubOrSec(), pubBean));

		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		if (historys.size() > 0) {
			JecnTaskHistoryNew lastHistory = historys.get(0);
			Long expiry = lastHistory.getExpiry();
			Date publishDate = lastHistory.getPublishDate();
			pubGroup.addParagraph(new Paragraph("生效时间：" + JecnUtil.DateToStr(publishDate, "yyyy-MM-dd"), pubBean));
			pubGroup.addParagraph(new Paragraph("保密期：" + (expiry == 0 ? "长期" : expiry + "个月"), pubBean));
		}

	}

	private Paragraph createEmptyParagraph() {
		Paragraph paragraph = new Paragraph("", new ParagraphBean(Align.center, "黑体", Constants.sihao, false));
		return paragraph;
	}

	private void createPubLine(Sect titleSect) {

		String pubDateStr = nullToEmpty(processDownloadBean.getReleaseDate());
		if (pubDateStr.length() > 10) {
			pubDateStr = pubDateStr.substring(0, 10);
		}
		// 2016 - 05 - 01实施 黑体 14
		String nextMonthFirstDay = "";
		if (StringUtils.isNotBlank(pubDateStr)) {
			try {
				// 下一月第一天
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date pubDate = format.parse(pubDateStr);
				Calendar cal = Calendar.getInstance();
				cal.setTime(pubDate);
				cal.add(Calendar.MONTH, 1);
				cal.set(Calendar.DAY_OF_MONTH, 1);
				nextMonthFirstDay = format.format(cal.getTime());

			} catch (ParseException e) {
				log.error(e);
			}
		}

		// 2016 - 04 - 11发布 黑体 四号
		ParagraphBean pubBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(2.5, Hanchor.page, 0)
				.setVertical(25.04, Vanchor.page, 0.32).lock(true);
		pubGroup.addParagraph(new Paragraph(pubDateStr + "发布", pubBean));

		// 2016 - 04 - 11实施 黑体 四号
		ParagraphBean nextBean = new ParagraphBean(Align.right, "黑体", Constants.sihao, false);
		Group nextGroup = titleSect.createGroup();
		nextGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(12.5, Hanchor.page, 0)
				.setVertical(25.04, Vanchor.page, 0.32).lock(true);
		nextGroup.addParagraph(new Paragraph(nextMonthFirstDay + "实施", nextBean));
	}

	private void createProcessName(Sect titleSect) {
		String flowName = nullToEmpty(processDownloadBean.getFlowName());
		// 黑体 26
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		ParagraphLineRule vLine1 = new ParagraphLineRule(34F, LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph paragraph = new Paragraph(flowName, pBean);

		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 17, GroupH.exact, 2.7).setHorizontal(2.0, Hanchor.page, 0)
				.setVertical(11.27, Vanchor.page, 0).lock(true);
		group.addParagraph(paragraph);
	}

	private void createTopLine(Sect titleSect) {
		createLine(titleSect, 0.95F, 25F, 483F, 25F);
	}

	private void createBottomLine(Sect titleSect) {
		createLine(titleSect, 0.95F, 530F, 483F, 530F);
	}

	private void createLine(Sect titleSect, float startX, float startY, float endX, float endY) {
		LineGraph line = new LineGraph(LineStyle.single, 1.2F);
		line.setFrom(startX, startY);
		line.setTo(endX, endY);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		positionBean.setMarginRight(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);
	}

	private void standardVersion(Sect titleSect) {
		ParagraphBean pBean = new ParagraphBean(Align.right, "黑体", Constants.sihao, false);
		ParagraphLineRule vLine1 = new ParagraphLineRule(14, LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph vision = new Paragraph("Q/BFW.XXX/XXX/XXX_PXX", pBean);
		titleSect.addParagraph(vision);
	}

	private void createTitleComponyName(Sect titleSect) {

		/*** 最小值 分散对齐 *****/
		ParagraphLineRule vLine = new ParagraphLineRule(0F, LineRuleType.AT_LEAST);
		ParagraphBean pBean = new ParagraphBean(Align.distribute, "黑体", Constants.xiaoyi, false);
		pBean.setSpace(0f, 0f, vLine);
		titleSect.createParagraph("海康威视体系文件", pBean);

	}

	private void createBFW(Sect titleSect) {
		// Q/BFW 罗马 48号 加粗
		ParagraphBean pBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sishiba, true);
		Paragraph zcfcParagraph = new Paragraph("Q/BFW", pBean);
		titleSect.addParagraph(zcfcParagraph);
	}

	/**
	 * 添加首页内容
	 * 
	 * @param titleSect
	 */
	private void addLevelContent(Sect titleSect) {

		/***** 默认表格样式 **********/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0F, 0F, 0F, 0F));
		ParagraphBean cellBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);

		// 为了简单表格将由四个表格组合而成
		Table nameTable = titleSect.createTable(tabBean, new float[] { 2F, 14.88F });
		List<String[]> titleLine = new ArrayList<String[]>();
		titleLine.add(new String[] { "文件名称", processDownloadBean.getFlowName() });
		nameTable.createTableRow(titleLine, cellBean);
		nameTable.setCellValignAndHeight(Valign.center, 0.7F);

		Table versionTable = titleSect.createTable(tabBean, new float[] { 2F, 5.92F, 2.32F, 6.64F });
		List<String[]> versionLine = new ArrayList<String[]>();
		String pubTime = getPubTime();
		versionLine.add(new String[] { "版    本", processDownloadBean.getFlowVersion(), "生效日期", pubTime });
		versionTable.createTableRow(versionLine, cellBean);
		versionTable.setCellValignAndHeight(Valign.center, 0.7F);

		// 级别中应该是不显示t_level=0的以及自己本身
		String L1 = "";
		String L2 = "";
		String L3 = "";
		String L4 = "";
		List<Object[]> parents = processDownloadBean.getParents();
		int parentLen = parents.size();
		if (parentLen > 2) {
			L1 = parents.get(1)[1].toString();
		}
		if (parentLen > 3) {
			L2 = parents.get(2)[1].toString();
		}
		if (parentLen > 4) {
			L3 = parents.get(3)[1].toString();
		}
		if (parentLen > 5) {
			L4 = parents.get(4)[1].toString();
		}

		Table codeTable = titleSect.createTable(tabBean, new float[] { 2F, 5.92F, 0.85F, 1.48F, 6.64F });
		List<String[]> codeData = new ArrayList<String[]>();
		codeData.add(new String[] { "文件编码", processDownloadBean.getFlowInputNum(), "流程架构", "L1", L1 });
		codeData
				.add(new String[] { "拟 制 人", processDownloadBean.getResponFlow().getFictionPeopleName(), "", "L2", L2 });
		codeData.add(new String[] { "会审人", "", "", "L3", L3 });
		String approvePeople = getApprovePeople();
		codeData.add(new String[] { "审 核 人", approvePeople, "", "L4", L4 });
		codeTable.createTableRow(codeData, cellBean);
		// 合并流程架构单元格
		codeTable.getRow(0).getCell(2).setVmergeBeg(true);
		codeTable.getRow(0).getCell(2).setRowspan(5);
		codeTable.getRow(3).getCell(2).setVmerge(true);
		codeTable.setCellValignAndHeight(Valign.center, 0.7F);

		Table lastApproveTable = titleSect.createTable(tabBean, new float[] { 2F, 5.92F, 2.32F, 6.64F });
		lastApproveTable.createTableRow(new String[] { "批 准 人", "", "流程Owner", "" }, cellBean);

		Table scopeTable = titleSect.createTable(tabBean, new float[] { 2F, 5.92F, 2.32F, 6.64F });
		List<String[]> scopeLine = new ArrayList<String[]>();
		scopeLine.add(new String[] { "适用范围", processDownloadBean.getApplicability(), "流程角色", "" });
		scopeTable.createTableRow(scopeLine, cellBean);
		scopeTable.setCellValignAndHeight(Valign.center, 0.7F);

		// 修改样式去除表格内容加粗
		ParagraphBean noBlobBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		setTableParagraph(nameTable, new int[] { 0 }, new int[] { 1 }, noBlobBean);
		setTableParagraph(versionTable, new int[] { 0 }, new int[] { 1 }, noBlobBean);
		setTableParagraph(codeTable, new int[] { 0, 1, 2, 3 }, new int[] { 1, 4 }, noBlobBean);
		setTableParagraph(scopeTable, new int[] { 0 }, new int[] { 1 }, noBlobBean);

	}

	protected void setTableParagraph(Table table, int[] rows, int[] cells, ParagraphBean paragraphBean) {
		for (int row : rows) {
			TableRow tRow = table.getRow(row);
			for (int cell : cells) {
				TableCell tCell = tRow.getCell(cell);
				setParagraphBean(tCell.getParagraphs(), paragraphBean);
			}
		}
	}

	protected void setParagraphBean(Paragraph p, ParagraphBean pBean) {
		p.setParagraphBean(pBean);
	}

	protected void setParagraphBean(List<Paragraph> ps, ParagraphBean pBean) {
		for (Paragraph p : ps) {
			setParagraphBean(p, pBean);
		}
	}

	private String getApprovePeople() {
		if (processDownloadBean.getHistorys().size() == 0) {
			return "";
		}
		JecnTaskHistoryNew jecnTaskHistoryNew = processDownloadBean.getHistorys().get(0);
		StringBuffer buf = new StringBuffer();
		List<JecnTaskHistoryFollow> approvePeoples = jecnTaskHistoryNew.getListJecnTaskHistoryFollow();
		for (int i = 0; i < approvePeoples.size() - 1; i++) {
			if (approvePeoples.get(i).getName() == null || !"".equals(approvePeoples.get(i).getName())) {
				continue;
			}
			if (i == 0) {
				buf.append(approvePeoples.get(i).getName());
			} else {
				buf.append("/" + approvePeoples.get(i).getName());
			}
		}
		return buf.toString();
	}

	private String getPubTime() {
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		for (JecnTaskHistoryNew history : historys) {
			if (history != null) {
				return JecnUtil.DateToStr(history.getPublishDate(), "yyyy-MM-dd");
			}
		}
		return "";
	}

	/**
	 * 修改页
	 * 
	 * @param firstSect
	 */
	private void alterPage(Sect sect) {
		ParagraphBean paragraphBean = new ParagraphBean(Align.center, "黑体", Constants.sanhao, false);
		ParagraphSpaceBean paragraphSpaceBean = new ParagraphSpaceBean(1F, 1F, new ParagraphLineRule(1.25F,
				LineRuleType.AUTO));
		paragraphBean.setParagraphSpaceBean(paragraphSpaceBean);
		sect.createParagraph("修  改  页", paragraphBean);

		TableBean tableBean = new TableBean();
		tableBean.setTableAlign(Align.center);
		tableBean.setBorder(0.5F);
		tableBean.setCellMarginBean(new CellMarginBean(0F, 0F, 0F, 0F));
		Table historyTable = sect.createTable(tableBean, new float[] { 1.22F, 1.22F, 4.28F, 3.2F, 2.3F, 1.53F, 1.53F,
				1.53F });
		TableRow headerRow = historyTable.createTableRow(0.7F);
		headerRow.createCells(new String[] { "版本编号", "*变化状态", "变更内容", "变更原因", "实施日期", "变更人", "审核人", "批准人" },Valign.center);
		List<String[]> tableData = new ArrayList<String[]>();
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		if (historys.size() == 0) {
			tableData.add(new String[] { "", "", "", "", "", "", "", "", });
		} else {
			for (JecnTaskHistoryNew history : historys) {
				String approveName = "";
				for (int i = history.getListJecnTaskHistoryFollow().size() - 1; i >= 0; i--) {
					JecnTaskHistoryFollow approve = history.getListJecnTaskHistoryFollow().get(i);
					if (approve.getName() != null && !"".equals(approve.getName())) {
						approveName = approve.getName();
						break;
					}
				}
				tableData.add(new String[] { history.getVersionId(), "", "", history.getModifyExplain(),
						JecnUtil.DateToStr(history.getPublishDate(), "yyyy-MM-dd"), history.getDraftPerson(), "",
						approveName });
			}
		}
		historyTable.createTableRow(tableData, tblContentStyle());
		historyTable.setCellValignAndHeight(Valign.center, 0.7F);
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "黑体", Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		historyTable.setRowStyle(0, tblTitileStyle, true, true);
		sect.createParagraph("*变化状态：A——增加，M——修改，D——删除", new ParagraphBean(Align.left, "宋体", Constants.xiaowu, false));
	}

	/**
	 * 记录保存
	 */
	@Override
	protected void a20(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 记录名称
		String recordName = JecnUtil.getValue("recordName");
		// 保存责任人
		String saveResponsiblePersons = JecnUtil.getValue("saveResponsiblePersons");
		// 保存场所
		String savePlace = JecnUtil.getValue("savePlace");
		// 归档时间
		String filingTime = JecnUtil.getValue("filingTime");
		// 保存期限
		String expirationDate = JecnUtil.getValue("storageLife");
		// 到期处理方式
		String handlingDue = JecnUtil.getValue("treatmentDue");
		float[] width = new float[] { 2.12F, 2.83F, 2F, 2F, 2F, 2F, 2F, 2F, 1.5F };
		List<String[]> rowData2 = new ArrayList<String[]>();
		rowData2.add(0, new String[] { "编号", recordName, "移交责任人", saveResponsiblePersons, savePlace, filingTime,
				expirationDate, handlingDue });
		for (String[] row : rowData) {
			rowData2.add(new String[] { row[6], row[0], row[7], row[1], row[2], row[3], row[4], row[5] });
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		createCommfhdr(firstSect);
		alterPage(firstSect);
		firstSect.addParagraph(new Paragraph(""));
		addLevelContent(firstSect);
		firstSect.addParagraph(new Paragraph(true));
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setSize(29.7F, 21F);
		flowChartSect.setSectBean(sectBean);
		createCharfhdr(flowChartSect);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, float left, float center, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaowu, false);
		Header header = sect.createHeader(HeaderOrFooterType.odd);

		TableBean tableBean = new TableBean();
		tableBean.setBorderBottom(1F);
		tableBean.setTableLeftInd(0.2F);
		Table table = header.createTable(tableBean, new float[] { left, center, right });
		table.createTableRow(new String[] { "", processDownloadBean.getFlowName(), "文档密级：" + getPubOrSec() }, pBean);

		ParagraphBean paragraphBean = new ParagraphBean(Align.right, "黑体", Constants.xiaowu, false);
		table.getRow(0).getCell(2).getParagraph(0).setParagraphBean(paragraphBean);
		table.setCellValignAndHeight(Valign.bottom, 0.6F);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommfdr(Sect sect, float left, float center, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.xiaowu, false);
		Footer header = sect.createFooter(HeaderOrFooterType.odd);

		TableBean tableBean = new TableBean();
		tableBean.setBorderTop(1F);
		tableBean.setTableLeftInd(0.2F);
		// 3F,11F,3F
		Table table = header.createTable(tableBean, new float[] { left, center, right });
		table.createTableRow(new String[] { "", "海康威视保密信息，未经授权禁止扩散", "" }, pBean);

		table.getRow(0).getCell(0).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.left, "黑体", Constants.xiaowu, false));
		table.getRow(0).getCell(2).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.right, "黑体", Constants.xiaowu, false));
		Paragraph pageParagraph = table.getRow(0).getCell(2).getParagraph(0);
		pageParagraph.appendTextRun("第");
		pageParagraph.appendCurPageRun();
		pageParagraph.appendTextRun("页, 共");
		pageParagraph.appendTotalPagesRun();
		pageParagraph.appendTextRun("页");
		table.setCellValignAndHeight(Valign.top, 0.6F);
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 3F, 11F, 3F);
		createCommfdr(sect, 3F, 11F, 3F);
	}

	/***
	 * 创建流程图页眉页脚
	 * 
	 * @param sect
	 */
	private void createCharfhdr(Sect sect) {
		createCommhdr(sect, 3F, 19.7F, 3F);
		createCommfdr(sect, 3F, 19.7F, 3F);
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		textContentStyle.setInd(0F, 0F, 0F, 1F);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(12F, LineRuleType.AT_LEAST));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("黑体", Constants.xiaosi, true);
		textTitleStyle.setInd(0F, 0F, 0.76F, 0F);
		textTitleStyle.setSpace(1F, 1F, leastVline12);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.94 厘米
		tblStyle.setTableLeftInd(0F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

}
