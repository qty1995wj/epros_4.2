package com.jecn.epros.server.action.web.login.ad.fuyao;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.HttpRequest;

public class JecnFuYaoLoginAction extends JecnAbstractADLoginAction {

	public JecnFuYaoLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		
		String queryString = this.loginAction.getRequest().getQueryString();
		log.info("queryString " + queryString);
		
		// user_Password=a&user_Name=admin
		String password = this.loginAction.getRequest().getParameter("user_Password");
		String loginName = this.loginAction.getRequest().getParameter("user_Name");

		if (StringUtils.isNotBlank(password) && StringUtils.isNotBlank(loginName)) {
			this.loginAction.setPassword(password);
			this.loginAction.setLoginName(loginName);
			return this.loginAction.userLoginGen(true);
		}
		// 工号
		String userid = this.loginAction.getRequest().getParameter("uid");
		// 待验证的code
		String code = this.loginAction.getRequest().getParameter("code");
		String systemName = this.loginAction.getRequest().getParameter("system");

		log.info("userid = " + userid + " code = " + code);
		// 二次请求获取code
		String resultCode = HttpRequest.sendGet(FuYaoiAfterItem.getValue("SSOURL"), "uid=" + userid + "&system="
				+ systemName);
		log.info("resultCode = " + resultCode);
		if (StringUtils.isNotBlank(code) && code.equals(resultCode)) {
			return this.loginByLoginName(userid);
		}
		return LoginAction.INPUT;
	}

}
