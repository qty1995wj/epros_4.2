var flag = false;
function secBoard(type) {
	if (type == 0) {
		document.getElementById("basicInformation").style.display = "block";
		document.getElementById("examinationAndApprovalRecords").style.display = "none";
		document.getElementById("liBasicInformation").className = "basicInfo1";
		document.getElementById("liExaminationAndApprovalRecords").className = "taskRecord2";

	} else if (type == 1) {
		document.getElementById("examinationAndApprovalRecords").style.display = "block";
		document.getElementById("basicInformation").style.display = "none";
		document.getElementById("liBasicInformation").className = "basicInfo2";
		document.getElementById("liExaminationAndApprovalRecords").className = "taskRecord1";
	}
}
/**
 * 转批div 互换 flag=:true 转批
 */
function subconcessions(flag) {
	if (flag) {// 转批
		document.getElementById("subconcessionsDisplay").style.display = "";
		// document.getElementById("peopleSelectDiv").style.display="";
		document.getElementById("submitDisplay").style.display = "none";
	} else {// 取消
		document.getElementById("subconcessionsDisplay").style.display = "none";
		// document.getElementById("peopleSelectDiv").style.display="none";
		document.getElementById("submitDisplay").style.display = "";
	}
}
/**
 * 交办div 互换 flag=:true 交办
 */
function assigned(flag) {
	if (flag) {// 转批
		// document.getElementById("peopleSelectDiv").style.display="";
		document.getElementById("assignedDisplay").style.display = "";
		document.getElementById("submitDisplay").style.display = "none";
	} else {// 取消
		// document.getElementById("peopleSelectDiv").style.display="none";
		document.getElementById("assignedDisplay").style.display = "none";
		document.getElementById("submitDisplay").style.display = "";
	}
}
/**
 * 提交属性字符判断
 */
function checkStrLengthbyId(id, minLen, maxLen) {
	var str = document.getElementById(id).value;
	var lengthstr = getLength(str);
	// 输入长度越界，请重新输入！
	if (lengthstr > maxLen) {
		if (id == "taskDesc") {
			// 变更说明输入长度不能超过1200个字符或者600个汉字！
			Ext.MessageBox.alert(i_tip, i_taskDesc + i_inputLengthRange);
			return true;
		} else if (id == "opinion") {
			// 审批意见输入长度不能超过1200个字符或者600个汉字！
			Ext.MessageBox.alert(i_tip, i_opinion + i_inputLengthRange);
			return true;
		}
	} else if (lengthstr < minLen) {
		// 输入长度太小，请重新输入！
		Ext.MessageBox.alert(i_tip, i_InputLengthIsTooSmall);
		return true;
	}
	return false;
}
/**
 * 获取输入字符串长度
 */
function getLength(str) {
	return str.replace(/[^\x00-\xFF]/g, '**').length;
}
/**
 * 提交审批
 */
function taskSubmit(taskElseType, loadingImage, submitLoading) {
	if (taskElseType == 2 || taskElseType == 3) {
		if (assignedOrSubSubmit(taskElseType)) {// 转批或交办验证
			return;
		}
	}

	if (taskElseType == 4) {// 打回
		// 打回任务则重新审批，是否需要打回！
		Ext.MessageBox.confirm(i_tip, i_isNeedToFightBack, function(btn) {
			if (btn == 'no') {// 点击的是‘是’
					return;
				} else {
					if(isNotNull(trim(document.getElementById("opinion").value))){
						document.getElementById("opinion").value = i_repulse;
					}
					taskSunmitInfo(taskElseType, loadingImage, submitLoading);
				}
			});
	} else if (taskElseType == 14) {// 返回
		// 是否需要返回！
		Ext.MessageBox.confirm(i_tip, i_reCallToTask, function(btn) {
			if (btn == 'no') {// 点击的是‘是’
					return;
				} else {
					taskSunmitInfo(taskElseType, loadingImage, submitLoading);
				}
			});
	} else {// 通过
		if(isNotNull(trim(document.getElementById("opinion").value))){
			document.getElementById("opinion").value = i_pass;
		}
		taskSunmitInfo(taskElseType, loadingImage, submitLoading);
	}

}

/**
 * 任务提交信息处理
 * 
 */
function taskSunmitInfo(taskElseType, loadingImage, submitLoading) {
	// 获取审批意见
	var opinion = document.getElementById("opinion").value;
	// 当前任务阶段
	var state = document.getElementById("state").value;

	var isControlauditLead;
	if (document.getElementById("isControlauditLead") != null) {
		isControlauditLead = document.getElementById("isControlauditLead").value;
	}
	var newViewerIds;
	if (document.getElementById("selectPeopleId")) {
		newViewerIds = document.getElementById("selectPeopleId").value;
		if (taskElseType == 6) {// 二次评审，拟稿人提交
			if (newViewerIds == null || newViewerIds == '') {
				// 二次评审，评审人不能为空！
				Ext.MessageBox.alert(i_tip, i_reviewNotNull);
				return;
			}
		}
	}
	if (isNotNull(trim(opinion))) {// 审批意见为空
		// "审批意见不能为空,请填写意见!"
		Ext.MessageBox.alert(i_tip, i_TaskOpinionIsNotNul);
		return;
	} else {
		// document.getElementById("opinion").readOnly ='true';
	}
	if (checkStrLengthbyId("opinion", 0, 1200)) {// 提交属性字符判断
		// document.getElementById("opinion").readOnly ='';
		return;
	}
	var isControl = false;
	if (isControlauditLead == 1 && taskElseType == 1 && state == 1) {
		isControl = true;
	}
	if ((taskElseType == 7 || isControl) && checkSubmit(false, true)) {// 重新提交和文控主导验证
		return;
	}
	// 重新提交时验证流程图信息是否完善
	if (taskElseType == 7) {
		if (!checkDate()) {// 开始时间和结束时间验证
			return;
		}
		if (document.getElementById("errorState").value == 0) {// 流程信息未完善
			// "请完善流程信息！"
			Ext.MessageBox.alert(i_tip, i_PleaseFinishMessage);
			return;
		}
	}
	var taskDesc;
	if (document.getElementById("taskDesc") != null) {
		taskDesc = document.getElementById("taskDesc").value;
		if (taskDesc == null || taskDesc == '') {
			// 变更说明不能为空！
			Ext.MessageBox.alert(i_tip, i_TaskDescIsNotNul);
			return;
		} else {
			// document.getElementById("taskDesc").readOnly ='true';
		}
		if (checkStrLengthbyId("taskDesc", 0, 1200)) {// 提交属性字符判断
			return;
		}
	}
	// 任务类型
	var taskType = document.getElementById("taskType").value;
	// 任务类型
	var taskId = document.getElementById("taskId").value;
	// 转批或交办人ID
	var userAssignedId;
	var operationMes;
	if (document.getElementById("subPeopleId")
			&& document.getElementById("subPeopleId").value != '') {// 转批
		userAssignedId = document.getElementById("subPeopleId").value;
	} else if (document.getElementById("assignedPeopleId")) {// 交办
		userAssignedId = document.getElementById("assignedPeopleId").value;
	}

	// 提交时按钮状态
	loadingSubmit(loadingImage, submitLoading);

	// 是否存在类别
	if (taskType != -1 && document.getElementById("typeListId")) {// 文件任务没有类别
		document.getElementById("prfType").value = document
				.getElementById("typeListId").value;
		// 获取当前选中状态下的
		var selectIndex = document.getElementById("typeListId").selectedIndex;
		if (selectIndex != -1) {
			document.getElementById("prfTypeName").value = document
					.getElementById("typeListId").options[selectIndex].text;
		}
	}

	var prfPublic;
	if (document.getElementById("prfPublic")) {// 是否存在密级
		prfPublic = document.getElementById("prfPublic").selectedIndex;
	}
	// 获取审批页面变动信息
	operationMes = "submitMessage.taskId=" + taskId + "&taskType=" + taskType
			+ "&submitMessage.taskOperation=" + taskElseType
			+ "&submitMessage.userAssignedId=" + userAssignedId
			+ "&submitMessage.newViewerIds=" + newViewerIds + "&taskStage="
			+ state;
	if (prfPublic == 0 || prfPublic == 1) {
		operationMes = operationMes + "&submitMessage.strPublic=" + prfPublic;
	}
	form1.action = "taskOperation.action?eprosButtTask=1&" + operationMes;
	form1.submit();
}
/**
 * 转批和交办处理
 */
function assignedOrSubSubmit(taskElseType) {
	var assignedOrSubId;
	if (taskElseType == 2 && document.getElementById("assignedPeopleId")) {// 交办
		assignedOrSubId = document.getElementById("assignedPeopleId").value;
		if (idIsNotNull(assignedOrSubId)) {// 验证id是否存在 true:不存在
			// "交办人不能为空！"
			Ext.MessageBox.alert(i_tip, i_assignedPeopleIsNotNull);
			return true;
		}
	} else if (taskElseType == 3 && document.getElementById("subPeopleId")) {// 转批
		assignedOrSubId = document.getElementById("subPeopleId").value;
		if (idIsNotNull(assignedOrSubId)) {// 验证id是否存在 true:不存在
			// "转批人不能为空！"
			alert(i_SubPeopleIsNotNull);
			return true;
		}
	}

	// 转批或交办验证 待转批的人员或待交办的人员不能为当前人员 3:转批；2交办
	return checkToSamePeople(taskElseType);
}

/**
 * 任务管理，任务编辑
 */
function taskEdit(taskElseType) {
	if (taskElseType != 11 && taskElseType != 12) {
		return;
	}
	if (document.getElementById("auditName1")) {
		var auditName = document.getElementById("auditName1").value;
		if (isNotNull(auditName)) {// 拟稿人为空
			// 必须存在拟稿人！
			Ext.MessageBox.alert(i_tip, i_DrafterMustExist);
			return;
		}
	}
	// 当前任务阶段
	var state = document.getElementById("state").value;
	// 任务类型
	var taskType = document.getElementById("taskType").value;
	// 任务类型
	var taskId = document.getElementById("taskId").value;
	// 获取审批意见
	var opinion = document.getElementById("opinion").value;

	if (isNotNull(trim(opinion))) {// 审批意见为空
		// 审批意见不能为空,请填写意见！
		Ext.MessageBox.alert(i_tip, i_TaskOpinionIsNotNul);
		return;
	}
	if (checkStrLengthbyId("opinion", 0, 1200)) {// 提交属性字符判断
		return;
	}
	if (taskElseType == 11 && checkSubmit(true, false)) {// 任务管理，任务编辑提交或任务管理，
		// 人员信息验证
		return;
	}

	// 获取审批页面变动信息
	operationMes = "submitMessage.taskId=" + taskId + "&taskType=" + taskType
			+ "&submitMessage.taskOperation=" + taskElseType;

	form1.action = "taskOperation.action?" + operationMes;
	form1.submit();
}
/**
 * 获取审批人ID值
 */
function getAuditUserId(auditId) {
	if (document.getElementById(auditId) != null) {
		return document.getElementById(auditId).value;
	} else {
		return null;
	}
}
/**
 * 验证必填项是否为空 重新提交、文控审核人提交审批人验证 isDrafter true:任务编辑 isControl：true跳过拟稿人验证
 */
function checkSubmit(isDrafter, isControl) {
	// 所有用户的userId集合
	var userIdList = new Array();
	// 单个审批阶段用户集合
	var userList = new Array();
	// 是否存在评审人
	var isReview = false;
	// 判断评审人后面有没有审批人
	var isApprove = false;
	// 当前任务阶段
	var curState = document.getElementById("state").value;
	var arrValue = 0;

	// 拟稿人的id
	var createPeopleId;
	if (flag) {
		return true;
	}
	for (var i = 1; i < 11; i++) {// 循环审批阶段 目前最多9个审批阶段 1为拟稿阶段

		if (i == 1 && isControl) { // 跳过拟稿人阶段

			if (document.getElementById("auditPeopleId1") != null) {// 拟稿人存在
				createPeopleId = document.getElementById("auditPeopleId1").value;

			}
			continue;
		}

		// 审核人主键ID
		var auditPeopleId = "auditPeopleId" + i;
		// 审核阶段
		var state = "state" + i;
		// 是否必填 1：必填
		var isEmpty = "isEmpty" + i;
		if (document.getElementById(auditPeopleId) != null) {// Id不存在

			// 获取审批阶段值
			stateValue = document.getElementById(state).value;
			// 人员ID值
			idValue = document.getElementById(auditPeopleId).value;
			// 是否允许为空
			isEmptyValue = document.getElementById(isEmpty).value;
			// 当前阶段为整理意见时，评审人员不能为空！
			if (curState == 10 && stateValue == 3 && idValue == "") {
				Ext.MessageBox.alert(i_tip, i_reviewNotEmpty);
				return true;
			}

			if (isDrafter && i == 1) {// 拟稿人清空
				if (idIsNotNull(idValue)) {// 拟稿人不存在
					// 必须存在拟稿人！
					Ext.MessageBox.alert(i_tip, i_DrafterMustExist);
					return true;
				} else// 拟稿人存在
				{
					createPeopleId = idValue;
				}
			}

			if (idIsNotNull(idValue)) {// 必填项
				if (!isNotNull(isEmptyValue) && isEmptyValue == 1) {
					// 审批人不能为空！
					Ext.MessageBox.alert(i_tip, document
									.getElementById("auditLable" + i).innerHTML
									+ i_AuditNotNull);
					return true;
				} else if (curState == stateValue) {// 当前阶段审批人不能为空（编辑验证）
					// 当前审批阶段不能为空！
					Ext.MessageBox.alert(i_tip, i_nowStateNotNull);
					return true;
				} else {// ID不存在
					continue;
				}
			}

			if (i != null && i != "") {// 存在ID(不记录拟稿人)
				// 获取各阶段审核人的ID集合
				userList = idValue.split(',');
				// 添加用户的集合
				userIdList[arrValue] = userList;
				arrValue++;

				// 如果当前阶段是评审人会审阶段况且评审人会审阶段人员不为空
				if (stateValue == 3 && idValue != "") {
					// 将整理意见的人员加入列表
					userIdList[arrValue] = createPeopleId.split(',');
					arrValue++;
				}
			} else if (!isNotNull(isEmptyValue) && isEmptyValue == 1) {// 必填项
				// 审批人不能为空！
				Ext.MessageBox.alert(i_tip, i_AuditNotNull);
				return true;
			}

			if (stateValue == 3) {// 评审人状态
				// 存在评审人
				isReview = true;
				continue;
			}
			if (isReview) {
				// 评审人后存在审核人
				isApprove = true;
			}
		}
	}
	if (isReview && !isApprove) {// 评审人后面必须存在审批人！
		Ext.MessageBox.alert(i_tip, i_AfterReviewerIsNotAudit);
		return true;
	}
	if (userIdList == null || userIdList.length == 0) {// 必须存在审核人！
		Ext.MessageBox.alert(i_tip, i_mustExistAudit);
		return true;
	}
	return false;
}

/**
 * 验证值是否为空
 */
function isNotNull(obj) {
	if (obj == null || obj == '') {
		return true;
	}
	return false;
}

/**
 * 验证id是否存在 true:不存在
 */
function idIsNotNull(obj) {
	if (obj == null || obj == '' || obj == -1) {
		return true;
	}
	return false;
}
/**
 * 判断两个数组是否存在交集
 */
function checkArray(firstArrat, secondArray) {
	if (firstArrat instanceof Array && secondArray instanceof Array) {
		for (var i = 0; i < firstArrat.length; i++) {
			for (var j = 0; j < secondArray.length; j++) {
				if (firstArrat[i] === secondArray[j]) {
					return true;
				}
			}
		}
	}
}
/**
 * 
 * 验证时间
 */
function checkDate() {
	var startTime = document.getElementById("startTime").value;
	var endTime = document.getElementById("endTime").value;
	if (endTime == null || endTime == '') {
		// "结束时间不能为空！"
		Ext.MessageBox.alert(i_tip, i_endTimeIsNotNull);
		return false;
	}
	if (startTime == null || startTime == "") {
		// "开始时间不能为空！"
		Ext.MessageBox.alert(i_tip, i_startTimeIsNotNull);
		return false;
	}
	if (startTime > endTime) {// 结束时间大于开始时间
		if (endTime > new Date()) {
			// "结束时间应小于当前时间！"
			Ext.MessageBox.alert(i_tip, i_endTimeIsMustBigNowTime);
		} else {
			// "开始时间应小于结束时间！"
			Ext.MessageBox.alert(i_tip, i_startTimeIsLessEndTime);
		}
		return false;
	}
	return true;
}
/**
 * 关闭窗口
 * 
 */
function windowClose() {
	if (emailType == 1) {// // 未定义 是否为邮件登录类型 1：邮件登录，审批任务，0
		window.location.href = basePath + 'index.jsp?index=myhome';
		return;
	}
	window.opener = null;
	window.open('', '_self');
	window.close();
}

/**
 * 按钮显示状态
 */
function loadingSubmit(loadingImage, submitLoading) {
	if (loadingImage == null || submitLoading == null) {
		return;
	}
	var loadImage = 'loadingImage';
	var submitLoad = 'submitLoading';
	for (var i = 0; i < 20; i++) {// 12个操作状态 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回
		// 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
		// * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交；11：编辑
		var temploadingImage = loadImage + i;
		var tempsubmitLoading = submitLoad + i;
		if (loadExists(temploadingImage, tempsubmitLoading)) {// 存在节点
			document.getElementById(temploadingImage).style.display = "none";
		}
	}
	document.getElementById(submitLoading).style.display = "";
}
/**
 * 除点击的按钮外，其他按钮隐藏
 * 
 */
function loadExists(temploadingImage, tempsubmitLoading) {
	if (document.getElementById(temploadingImage)
			&& document.getElementById(temploadingImage)) {// 存在节点
		return true;
	}
	return false;
}
/**
 * 转批或交办验证 待转批的人员或待交办的人员不能为当前人员 3:转批；2交办
 */
function checkToSamePeople(taskEleseType) {
	if (!document.getElementById("curPeopleId")) {// 存在
		return false;
	}
	var curPeopleId = document.getElementById("curPeopleId").value;
	var assignedPeople;
	if (taskEleseType == 2 && document.getElementById("assignedPeopleId")) {// 交办
		assignedPeople = document.getElementById("assignedPeopleId").value;
		if (assignedPeople != null && assignedPeople == curPeopleId) {
			// 交办人不能为当前审批人！
			Ext.MessageBox.alert(i_tip, i_assignedIsNotCur);
			return true;
		}
	} else if (taskEleseType == 3 && document.getElementById("subPeopleId")) {// 转批
		assignedPeople = document.getElementById("subPeopleId").value;
		if (assignedPeople != null && assignedPeople == curPeopleId) {
			// 转批人不能为当前审批人！
			Ext.MessageBox.alert(i_tip, i_subPeopleIsNotCur);
			return true;
		}
	}
	return false;
}

/**
 * onload 加载 文本编辑框
 * 
 */
function setShowTaskPageWH(divId, num) {
	document.getElementById(divId).style.width = checkTaskWidth(num);
}

function checkTaskWidth(num) {
	// 屏幕可用区域
	var clientWidth = document.documentElement.clientWidth;
	return clientWidth / 2 - num + "px";
}

function taskC() {
	if (opener.location != null) {
		opener.location.href = '" + href + "';
		window.opener = null;
		window.open('', '_self');
		window.close();
	} else {
		window.location.href = basePath + 'index.jsp?index=myhome';
	}
}
function trim(str) { // 删除左右两端的空格
	return str.replace(/(^\s*)|(\s*$)/g, "");
}

/*
 * 清除人员替换人员选择框的内容
 */
function taskManagementReplacePeopleReset() {
	document.getElementById("fromPeopleId").value = "";
	document.getElementById("toPeopleId").value = "";
	document.getElementById("fromPeopleName").value = "";
	document.getElementById("toPeopleName").value = "";

}
/*
 * 替换审批人员
 */
function taskManagementReplacePeopleSubmit() {
	var fromPeopleId = document.getElementById("fromPeopleId").value;
	var toPeopleId = document.getElementById("toPeopleId").value;
	if (fromPeopleId == "" || toPeopleId == "") {
		Ext.Msg.alert(i_tip, i_approvePeopleIsNotNull);
		return;
	} else {
		if (fromPeopleId == toPeopleId) {
			Ext.Msg.alert(i_tip, i_approvePeopleIsNotSame);
			return;
		}

		// 确认执行对话框
		Ext.MessageBox.confirm(i_tip, "原审批人的未完成的任务都将替换给新审批人审批，确认继续？", 
		function(btn) {

			if (btn == 'yes') {

				Ext.Ajax.request({
					url : 'replaceApprovePeople.action',
					method : 'POST',
					timeout : 1000000,
					params : {
						fromPeopleId : fromPeopleId,
						toPeopleId : toPeopleId

					},
					// 调用成功
					success : function(response, options) {
						// 获取response中的数据
						var rs = response.responseText;
						if (rs == 'success') {
							// 清除选择的人员名称和id
							document.getElementById("fromPeopleId").value = "";
							document.getElementById("fromPeopleName").value = "";
							document.getElementById("toPeopleId").value = "";
							document.getElementById("toPeopleName").value = "";
							// 人员替换成功!
							Ext.Msg.alert(i_tip, i_approvePeopleReplaceSuccess);

						} else {
							// 有错误信息
							Ext.Msg.alert(i_tip, i_approvePeopleReplaceFail);
						}
					},
					// 失败调用
					failure : function(response, options) {
						Ext.Msg.alert(i_tip, i_approvePeopleReplaceFail);
					}
				});
			}
		});

	}
}

/**
 * 任务的人员选择是否可以多选
 * @param {} state 阶段
 * @return {Boolean}会审阶段以及自定义的阶段可以多选
 */
function taskUserMultipleSelect(state) {
	if (state == 3 || state == 11 || state == 12 || state == 13 || state == 14) {
		return true;
	}
	return false;
}