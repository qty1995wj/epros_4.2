package epros.designer.gui.integration.internalControl;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 新建条款模版
 * 
 * @author Administrator
 * 
 */
public abstract class JecnClauseDialog extends JecnDialog {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(JecnClauseDialog.class);

	/** 主面板 */
	private JecnPanel mainPanel = null;
	/** 名称显示面板 */
	private JecnPanel topPanel = null;
	/** 内容显示面板 */
	private JecnPanel centerPanel = null;
	/** 内容名称面板 */
	private JecnPanel textPanel = null;
	/** * 内容显示滚动面板 */
	private JScrollPane textScrollPane = null;
	/** 按钮面板 */
	private JPanel buttonPanel = null;
	/** 名称Lab */
	private JLabel nameLab = null;
	/** 名称填写框 */
	private JTextField nameTextField = null;
	/** 内容Label */
	private JLabel centerNameLab = null;
	/** 内容填写框 */
	private JecnTextArea centerText = null;
	/** 确定按钮 */
	private JButton okBut = null;
	/** 取消按钮 */
	private JButton cancelBut = null;
	/** 名称验证提示 */
	protected JLabel promptLab = null;
	/** 判断是否点击的确定按钮 */
	private boolean isOk = false;
	/** 设置面板控件大小 */
	Dimension dimension = null;
	/** 必填项 *号提示 */
	private JLabel requiredMarkLab = new JLabel("*");
	/** 内容验证提示 */
	protected static JLabel promptContent = null;

	public JecnClauseDialog() {
		initCompotents();
		initLayout();
	}

	/** 条款名称 */
	private String name = "";
	/** 条款内容 */
	private String content = "";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		this.nameTextField.setText(name);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		this.centerText.setText(content);
	}

	/** 初始化组件 */
	private void initCompotents() {
		mainPanel = new JecnPanel();
		topPanel = new JecnPanel();
		centerPanel = new JecnPanel();
		textPanel = new JecnPanel();

		buttonPanel = new JPanel();
		nameLab = new JLabel(JecnProperties.getValue("clauseNameC"));
		nameTextField = new JTextField();
		centerNameLab = new JLabel(JecnProperties.getValue("clauseContentC"));
		centerText = new JecnTextArea();
		textScrollPane = new JScrollPane(centerText);
		centerText.setLineWrap(true);
		centerText.setWrapStyleWord(true);
		centerText.setBorder(BorderFactory.createLineBorder(Color.gray));
		okBut = new JButton(JecnProperties.getValue("okBtn"));
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
		promptLab = new JLabel();
		promptContent = new JLabel();

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置名称显示面板的默认背景色
		topPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置内容显示面板的默认背景色
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置按钮面板的默认背景色
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置Dialog大小
		this.setSize(500, 450);
		// Dialog标题
		this.setTitle(getDialogTitle());
		// 设置Dialog的大小不被改变
		// this.setResizable(false);
		this.setModal(true);
		this.setLocationRelativeTo(null);

		// 设置主面板大小
		dimension = new Dimension(500, 420);
		mainPanel.setPreferredSize(dimension);
		mainPanel.setMaximumSize(dimension);
		mainPanel.setMinimumSize(dimension);

		// // 设置名称显示面板的大小
		// dimension = new Dimension(500, 400);
		// topPanel.setPreferredSize(dimension);
		// topPanel.setMaximumSize(dimension);
		// topPanel.setMinimumSize(dimension);

		// 设置内容显示面板大小
		// dimension = new Dimension(500, 340);
		// centerPanel.setPreferredSize(dimension);
		// centerPanel.setMaximumSize(dimension);
		// centerPanel.setMinimumSize(dimension);

		// 设置按钮面板的大小
		// dimension = new Dimension(450, 30);
		// buttonPanel.setPreferredSize(dimension);
		// buttonPanel.setMaximumSize(dimension);
		// buttonPanel.setMinimumSize(dimension);

		// 设置验证提示文字颜色
		promptLab.setForeground(Color.red);
		requiredMarkLab.setForeground(Color.red);
		promptContent.setForeground(Color.red);

		// 为确认按钮增加回车事件
		this.getRootPane().setDefaultButton(okBut);

		// 设置验证提示Label的大小
		// dimension = new Dimension(500, 13);
		// promptLab.setPreferredSize(dimension);
		// promptLab.setMaximumSize(dimension);
		// promptLab.setMinimumSize(dimension);
		// dimension = new Dimension(200, 13);
		// promptContent.setPreferredSize(dimension);
		// promptContent.setMaximumSize(dimension);
		// promptContent.setMinimumSize(dimension);

		// 确定按钮事件监听
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 取消按钮事件监听
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
	}

	/** 初始化布局 */
	private void initLayout() {
		// 主面板布局
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(topPanel, c);

		// c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
		// GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
		// 0, 0);
		// mainPanel.add(centerPanel, c);

		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(nameLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		topPanel.add(nameTextField, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,
						0, 5, 0), 0, 0);
		topPanel.add(requiredMarkLab, c);
		// c = new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		// topPanel.add(promptLab, c);

		/** ************************** */
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		topPanel.add(centerNameLab, c);
		// textPanel.add(centerNameLab);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		topPanel.add(textScrollPane, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.SOUTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 5, 15), 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// buttonPanel.setBorder(BorderFactory.createTitledBorder(""));
		buttonPanel.add(promptContent);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	private boolean validateName(String name, JLabel jLable) {
		String s = "";
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			s = JecnUserCheckInfoData.getNameNotNull();
		} else if (JecnUserCheckUtil.getTextLength(name) > 122) {
			// 名称不能超过122个字符或61个汉字
			s = JecnUserCheckInfoData.getNameLengthInfo();
		}
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			jLable.setText(JecnProperties.getValue("clauseNameC")+s);
			return false;
		}
		jLable.setText("");
		return true;
	}

	private boolean validateContent(String content, JLabel jLable) {
		String s = JecnUserCheckUtil.checkNoteLength(content);
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			jLable.setText(JecnProperties.getValue("clauseContentC")+s);
			return false;
		}
		jLable.setText("");
		return true;
	}

	// 确定事件
	protected void okButtonAction() {
		name = this.nameTextField.getText().trim();
		nameTextField.setText(name);
		content = this.centerText.getText();
		centerText.setText(content);

		// 验证名称是否正确
		if (!validateName(name, promptContent)) {
			return;
		}
		if (!validateContent(content, promptContent)) {
			return;
		}
		try {
			if (validateNodeRepeat(name)) {
				promptContent.setText(JecnProperties.getValue("nameHaved"));
				return;
			} else {
				promptContent.setText("");
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties
					.getValue("serverConnException"));
			log.error("JecnClauseDialog okButtonAction is error！", e);
			return;

		}
		saveData();
		isOk = true;
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:验证是否重名
	 * @return
	 */
	protected abstract boolean validateNodeRepeat(String name) throws Exception;

	/**
	 * @author yxw 2012-5-8
	 * @description:保存数据,并显示
	 */
	protected abstract void saveData();

	// 取消事件
	protected abstract void cancelButtonAction();

	/** 获取Dialog标题 */
	protected abstract String getDialogTitle();

	public JTextField getNameTextField() {
		return nameTextField;
	}

	public void setNameTextField(JTextField nameTextField) {
		this.nameTextField = nameTextField;
	}

	public JecnTextArea getCenterText() {
		return centerText;
	}

	public void setCenterText(JecnTextArea centerText) {
		this.centerText = centerText;
	}

	public boolean isOk() {
		return isOk;
	}

	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}

}
