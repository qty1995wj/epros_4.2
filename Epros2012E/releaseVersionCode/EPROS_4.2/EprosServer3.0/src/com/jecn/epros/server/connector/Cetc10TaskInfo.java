package com.jecn.epros.server.connector;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.webservice.cetc10.Cetc10TaskBean;
import com.jecn.webservice.cetc10.Cetc10TaskInfoItem;

public class Cetc10TaskInfo extends JecnBaseTaskSend {

	/**
	 * 根据任务信息，发送待办任务
	 * 
	 * @param forRecodeNew
	 * @param beanNew
	 * @param personDao
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) {
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			for (Long long1 : handlerPeopleIds) {
				myTaskBean = getMyTaskBeanByTaskInfo(beanNew, personDao);
				myTaskBean.setTaskApprovePeopleId(long1);
				myTaskBean.setIsProcessed("false");
				sendInfoToCetc10Service(getCetc10ToDOTasks(myTaskBean, personDao));
			}
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew, personDao);
			myTaskBean.setTaskApprovePeopleId(curPeopleId);
			myTaskBean.setTaskStage(beanNew.getUpState());
			myTaskBean.setIsProcessed("true");
			sendInfoToCetc10Service(getCetc10ToDOTasks(myTaskBean, personDao));

		} else {// 一条from的数据,一条to 的数据
			// 设置上一环节任务待办任务状态为false，表示未已审批；
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew, personDao);
			myTaskBean.setTaskApprovePeopleId(curPeopleId);
			myTaskBean.setIsProcessed("true");
			myTaskBean.setTaskStage(beanNew.getUpState());
			sendInfoToCetc10Service(getCetc10ToDOTasks(myTaskBean, personDao));

			for (Long long1 : handlerPeopleIds) {
				// 审批任务，发送下一环节任务待办
				myTaskBean = getMyTaskBeanByTaskInfo(beanNew, personDao);
				myTaskBean.setTaskApprovePeopleId(long1);
				myTaskBean.setIsProcessed("false");
				sendInfoToCetc10Service(getCetc10ToDOTasks(myTaskBean, personDao));
			}
		}
	}

	/**
	 * 发送消息
	 * 
	 * @param cetc10TaskBean
	 */
	public void sendInfoToCetc10Service(Cetc10TaskBean cetc10TaskBean) {
		log.info(cetc10TaskBean.toString());
		Cetc10TaskInfoItem.INSTANCE.taskSendInfoService(cetc10TaskBean);
	}

	/**
	 * 取消待办-发送通知
	 * 
	 * @param curPeopleId
	 * @param jecnTaskBeanNew
	 * @param jecnUser
	 * @param createUser
	 */
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew, JecnUser jecnUser, JecnUser createUser) {
		Cetc10TaskBean cetc10TaskBean = new Cetc10TaskBean();
		cetc10TaskBean.setIsProcessed("true");
		cetc10TaskBean.setLoginName(jecnUser.getEmail());
		cetc10TaskBean.setName(jecnUser.getTrueName());
		cetc10TaskBean.setTitle(jecnTaskBeanNew.getTaskName() + "(" + createUser.getTrueName() + ")");
		cetc10TaskBean.setSummery(jecnTaskBeanNew.getTaskDesc());
		cetc10TaskBean.setTaskid(jecnTaskBeanNew.getId() + "_" + curPeopleId + "_" + jecnTaskBeanNew.getState());
		cetc10TaskBean.setUrl(getMailUrl(jecnTaskBeanNew.getId(), jecnUser.getPeopleId(), false, false));
		cetc10TaskBean.setTime(JecnCommon.getStringbyDate(new Date()));
		sendInfoToCetc10Service(cetc10TaskBean);
	}

	/**
	 * 任务待办异常，取消待办任务
	 * 
	 * @param webLoginBean
	 * @param taskBeanNew
	 * @param createUser
	 * @throws Exception
	 */
	public void sendInfoByTaskError(JecnUser jecnUser, JecnTaskBeanNew taskBeanNew, JecnUser createUser)
			throws Exception {
		if (!JecnConfigTool.isCect10Login()) {
			return;
		}
		Cetc10TaskBean cetc10TaskBean = new Cetc10TaskBean();
		cetc10TaskBean.setIsProcessed("true");
		cetc10TaskBean.setLoginName(jecnUser.getEmail());
		cetc10TaskBean.setName(jecnUser.getTrueName());
		cetc10TaskBean.setTitle(taskBeanNew.getTaskName() + "(" + createUser.getTrueName() + ")");
		cetc10TaskBean.setTaskid(taskBeanNew.getId() + "_" + jecnUser.getPeopleId() + "_" + taskBeanNew.getUpState());
		cetc10TaskBean.setSummery(taskBeanNew.getTaskDesc());
		cetc10TaskBean.setUrl(getMailUrl(taskBeanNew.getId(), jecnUser.getPeopleId(), false, false));
		cetc10TaskBean.setTime(JecnCommon.getStringbyDate(new Date()));
		sendInfoToCetc10Service(cetc10TaskBean);
	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew taskBeanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		if (cancelPeoples == null || cancelPeoples.isEmpty()) {
			log.info("");
			return;
		}
		Set<Long> setPeopleIds = new HashSet<Long>();
		setPeopleIds.addAll(cancelPeoples);
		List<JecnUser> list = personDao.getJecnUserList(setPeopleIds);
		JecnUser createUser = personDao.get(taskBeanNew.getCreatePersonId());
		for (JecnUser user : list) {
			Cetc10TaskBean cetc10TaskBean = new Cetc10TaskBean();
			cetc10TaskBean.setIsProcessed("true");
			cetc10TaskBean.setLoginName(user.getEmail());
			cetc10TaskBean.setName(user.getTrueName());
			cetc10TaskBean.setTitle(taskBeanNew.getTaskName() + "(" + createUser.getTrueName() + ")");
			cetc10TaskBean.setTaskid(taskBeanNew.getId() + "_" + user.getPeopleId() + "_" + taskBeanNew.getState());
			cetc10TaskBean.setSummery(taskBeanNew.getTaskDesc());
			cetc10TaskBean.setUrl(getMailUrl(taskBeanNew.getId(), user.getPeopleId(), false, false));
			cetc10TaskBean.setTime(JecnCommon.getStringbyDate(new Date()));
			sendInfoToCetc10Service(cetc10TaskBean);
		}
	}
}
