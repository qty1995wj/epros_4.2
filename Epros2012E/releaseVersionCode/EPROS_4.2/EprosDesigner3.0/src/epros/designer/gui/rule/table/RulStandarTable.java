package epros.designer.gui.rule.table;

import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.rule.JecnStandarCommon;

import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;
/**
 * 制度操作说明===标准表单表格
 * 2013-11-22
 *
 */
public class RulStandarTable extends JTable {
	private List<JecnStandarCommon> listJecnStandarCommon;
	private  Long titleId;
	public RulStandarTable(List<JecnStandarCommon> listJecnStandarCommon,Long titleId){
		
		this.listJecnStandarCommon = listJecnStandarCommon;
		this.titleId = titleId;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}
	public RuleStandarTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("standardName"));
		title.add(JecnProperties.getValue("keyActiveType"));
		return new RuleStandarTableMode(updateTableContent(listJecnStandarCommon),
				title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class RuleStandarTableMode extends DefaultTableModel {
		public RuleStandarTableMode(Vector<Vector<Object>> data,
				Vector<String> title) {
			super(data, title);

		}

		@Override
		public Class getColumnClass(int columnIndex) {
			return getValueAt(0, columnIndex).getClass();
		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	public Vector<Vector<Object>> updateTableContent(List<JecnStandarCommon> list) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				JecnStandarCommon jecnStandarCommon = list.get(i);
				Vector<Object> data = new Vector<Object>();
				if (jecnStandarCommon != null) {
					//标准ID
					data.add(jecnStandarCommon.getStandarId());
					// 标准名称
					data.add(jecnStandarCommon.getStandarName());
					//标准类型 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
					String standarTypeStr = jecnStandarCommon.getStandaType();
					if("0".equals(standarTypeStr)){
						data.add(JecnProperties.getValue("dir"));
					}else if("1".equals(standarTypeStr)){
						data.add(JecnProperties.getValue("fileStandar"));
					}else if("2".equals(standarTypeStr)){
						data.add(JecnProperties.getValue("flowStandar"));
					}else if("3".equals(standarTypeStr)){
						data.add(JecnProperties.getValue("flowMapStandar"));
					}else if("4".equals(standarTypeStr)){
						data.add(JecnProperties.getValue("standarClause"));
					}else if("5".equals(standarTypeStr)){
						data.add(JecnProperties.getValue("stanClauseRequire"));
					}
					
					vector.add(data);
				}
			}
		}
		return vector;
	}
	
}
