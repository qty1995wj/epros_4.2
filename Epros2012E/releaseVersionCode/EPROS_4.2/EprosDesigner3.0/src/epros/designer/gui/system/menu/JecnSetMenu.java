package epros.designer.gui.system.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import epros.designer.gui.system.config.process.JecnConfigProcess;
import epros.designer.gui.system.config.ui.dialog.JecnFileConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnFlowConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnMailConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnPubSetConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnRuleConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnTotalMapConfigJDialog;
import epros.designer.util.JecnProperties;

/**
 * 
 * 配置按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSetMenu extends JPopupMenu implements ActionListener {

	/** 权限设置 */
	private JMenuItem pubSetButton = null;
	/** 制度配置 */
	private JMenuItem ruleTaskButton = null;
	/** 流程配置 */
	private JMenuItem processTaskButton = null;
	/** 流程地图配置 */
	private JMenuItem totalMapTaskButton = null;
	/** 文件配置 */
	private JMenuItem fileTaskButton = null;
	/** 邮箱配置 */
	private JMenuItem mailSetButton = null;
	
	
	public JecnSetMenu() {

		/** 权限设置 */
		pubSetButton = new JMenuItem();
		pubSetButton.setText(JecnProperties.getValue("setTitlePubName"));
		// 添加事件监听
		pubSetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnPubSetConfigJDialog pubDialog = new JecnPubSetConfigJDialog(JecnConfigProcess.getPubConfigData());
				pubDialog.setVisible(true);
			}
		});
		pubSetButton.setIcon(new ImageIcon("images/setingImages/eprosSet.gif"));
		
		
		/** 制度配置 */
		ruleTaskButton = new JMenuItem();
		ruleTaskButton.setText(JecnProperties.getValue("setTitleRuleName"));
		// 添加事件监听
		ruleTaskButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnRuleConfigJDialog dialogRule = new JecnRuleConfigJDialog(JecnConfigProcess.getRuleConfigData());
				dialogRule.setVisible(true);
			}
		});
		ruleTaskButton.setIcon(new ImageIcon("images/setingImages/ruleTaskConfig.gif"));
		
		
		/** 流程配置 */
		processTaskButton = new JMenuItem();
		processTaskButton.setText(JecnProperties.getValue("setTitlePartMapName"));
		// 添加事件监听
		processTaskButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnFlowConfigJDialog dialog = new JecnFlowConfigJDialog(JecnConfigProcess.getPartMapConfigData());
				dialog.setVisible(true);
			}
		});
		processTaskButton.setIcon(new ImageIcon("images/setingImages/processTaskConfig.gif"));
		
		
		/** 流程地图配置 */
		totalMapTaskButton = new JMenuItem();
		totalMapTaskButton.setText(JecnProperties.getValue("setTitleTotalName"));
		// 添加事件监听
		totalMapTaskButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnTotalMapConfigJDialog dialogMap = new JecnTotalMapConfigJDialog(JecnConfigProcess
						.getTotalMapConfigData());
				dialogMap.setVisible(true);
			}
		});
		totalMapTaskButton.setIcon(new ImageIcon("images/setingImages/totalMapTaskConfig.gif"));
		
		
		/** 文件配置 */
		fileTaskButton = new JMenuItem();
		fileTaskButton.setText(JecnProperties.getValue("setTitleFileName"));
		// 添加事件监听
		fileTaskButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnFileConfigJDialog dialogFile = new JecnFileConfigJDialog(JecnConfigProcess.getFileConfigData());
				dialogFile.setVisible(true);
			}
		});
		fileTaskButton.setIcon(new ImageIcon("images/setingImages/fileTaskConifg.gif"));
		
		/** 邮箱配置 */
		mailSetButton = new JMenuItem();
		mailSetButton.setText(JecnProperties.getValue("setTitleMailName"));
		// 添加事件监听
		mailSetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnMailConfigJDialog mailConfigJDialog = new JecnMailConfigJDialog(JecnConfigProcess.getMailConfigData());
				mailConfigJDialog.setVisible(true);
			}
		});
		mailSetButton.setIcon(new ImageIcon("images/setingImages/processOperation.gif"));
		
		
		this.add(processTaskButton);
		this.add(totalMapTaskButton);
		this.add(ruleTaskButton);
		this.add(fileTaskButton);
		this.add(mailSetButton);
		this.add(pubSetButton);
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
