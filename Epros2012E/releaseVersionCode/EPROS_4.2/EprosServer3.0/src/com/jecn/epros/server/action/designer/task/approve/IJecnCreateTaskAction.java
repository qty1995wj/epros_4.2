package com.jecn.epros.server.action.designer.task.approve;

import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;

public interface IJecnCreateTaskAction {

	/**
	 * 
	 * 创建流程、制度、文件 任务
	 * 
	 * @param tempCreateTaskBean
	 *            JecnTempCreateTaskBean提交的任务信息
	 * 
	 */
	public void createPRFTask(JecnTempCreateTaskBean tempCreateTaskBean)
			throws Exception;

}
