<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript"
			src="${basePath}js/reports/processValidityMaintenance.js"
			charset="UTF-8">
</script>
	</head>
	<body>
	
	<div style="height: 100%;overflow:auto;">
		<div id="div" class="div" style="min-height:160px;">
			<div id="search" class="divBorder" align="center">

				<table width="100%">
					<div align="right" style="margin-top: 4px">
						<!-- 开始日期: -->
						<s:text name="startTimeC" />
						<s:textfield readonly="true" cssClass="Wdate" id="startTime"
							cssStyle="border-color: #7F9DB9;" onclick="WdatePicker();"
							theme="simple" />
						<!-- 结束日期: -->
						<s:text name="endTimeC" />
						<s:textfield readonly="true" cssClass="Wdate" id="endTime"
							cssStyle="border-color: #7F9DB9;margin-right: 4px"
							onclick="WdatePicker();" theme="simple" />
					</div>

				</table>
				<table width="100%" style="margin-top: 4px">
					<tr>
						<td align="left" width="100px">
							<s:text name="responsibilityDepartmentC"></s:text>
						</td>

						<td>
							<div style="width: 100%" id="select_organization_id">
								&nbsp;
							</div>
						</td>
						<td align="right" width="240px">
							<div align="right">
								<span
									onclick='orgPosPerMultipleSelectHasLimit("organization","orgDutyName_access_id","orgDutyId_access_id","select_organization_id",20)'><img
										style="vertical-align: right; cursor: pointer;"
										src="${basePath}images/common/select.gif" /> </span>
								<span
									onclick='closeAllOrganization("orgDutyName_access_id","orgDutyId_access_id","select_organization_id")'><img
										style="vertical-align: right; margin-right: 4px; cursor: pointer;"
										src="${basePath}images/common/clear.gif" /> </span>
							</div>
						</td>
					</tr>

					<%--流程架构--%>
					<tr style="margin-top: 4px">
						<td align="left" width="100px" style="margin-left: 4px">
							<s:text name="processMapC"></s:text>

						</td>
						<td>
							<div id="select_processMap_id" style="width: 100%">
								&nbsp;
							</div>
						</td>

						<td align="right" width="240px">
							<div align="right">
								<span
									onclick='processAndProcessMapSelectHasLimit("processMap","processMapName_access_id","processMapId_access_id","select_processMap_id",20)'><img
										style="vertical-align: left; cursor: pointer;"
										src="${basePath}images/common/select.gif" /> </span>
								<span
									onclick='closeAllProcessMap("processMapName_access_id","processMapId_access_id","select_processMap_id")'><img
										style="vertical-align: left; margin-right: 4px; cursor: pointer;"
										src="${basePath}images/common/clear.gif" /> </span>
							</div>
						</td>

					</tr>
				</table>


			</div>
			<div align="center" style="padding-top: 10px;padding-bottom:10px">
				<a onclick="processScanOptimizeReportDownloadExcel()"
					style="cursor: pointer; color: #005EA7;" class="table"><s:text
						name="downLoadExcel" /> </a>
			</div>
		</div>
		<div>
			<form action="" id="processScanOptimizeForm" method="post">
				<!-- 责任部门ID -->
				<input name="dutyOrgIds" type="hidden" id="orgDutyId_access_id"
					value="-1" />
				<input name="dutyOrgNames" type="hidden" id="orgDutyName_access_id"
					value="" />
				<!-- 流程地图ID -->
				<input name="processMapIds" type="hidden"
					id="processMapId_access_id" value="-1" />
				<input name="processMapNames" type="hidden"
					id="processMapName_access_id" value="" />
				<!-- 开始时间 -->
				<input name="startTime" type="hidden" id="startTimeSub" value="" />
				<!-- 结束时间 -->
				<input name="endTime" type="hidden" id="endTimeSub" value="" />
			</form>
		</div>
   

        <div style="min-height:160px;">
		<!--有效期维护-->
		<div class="x-panel-header x-unselectable"
			style="-moz-user-select: none;">
			<span class="x-panel-header-text"><s:text
					name="expiryMaintenance"></s:text> </span>
		</div>
		<div class="div">
			<div id="search" class="divBorder" align="center">

				<table width="100%" style="margin-top: 4px">
					<tr>
						<td align="left" width="100px">
							<s:text name="responsibilityDepartmentC"></s:text>
						</td>

						<td>
							<div style="width: 100%" id="select_organization_validity_id">
								&nbsp;
							</div>
						</td>
						<td align="right" width="240px">
							<div align="right">
								<span
									onclick='orgPosPerMultipleSelectHasLimit("organization","orgDutyName_validity_id","orgDutyId_validity_id","select_organization_validity_id",20)'><img
										style="vertical-align: right; cursor: pointer;"
										src="${basePath}images/common/select.gif" /> </span>
								<span onclick='closeAllOrganization("orgDutyName_validity_id","orgDutyId_validity_id","select_organization_validity_id")'><img
										style="vertical-align: right; margin-right: 4px; cursor: pointer;"
										src="${basePath}images/common/clear.gif" /> </span>
							</div>
						</td>
					</tr>

					<%--流程名称--%>
					<tr style="margin-top: 4px">
						<td align="left" width="100px" style="margin-left: 4px">
							<s:text name="processNameC"></s:text>
						</td>
						<td>
							<div id="select_processAndMap_id" style="width: 100%">
								&nbsp;
							</div>
						</td>

						<td align="right" width="240px">
							<div align="right">
								<span
									onclick='processAndProcessMapSelectHasLimit("all","processMapName_validity_id","processMapId_validity_id","select_processAndMap_id",20)'><img
										style="vertical-align: left; cursor: pointer;"
										src="${basePath}images/common/select.gif" /> </span>
								<span onclick='closeAllProcessMap("processMapName_validity_id","processMapId_validity_id","select_processAndMap_id")'><img
										style="vertical-align: left; margin-right: 4px; cursor: pointer;"
										src="${basePath}images/common/clear.gif" /> </span>
							</div>
						</td>

					</tr>
				</table>


			</div>

			<div>

				<div align="center" style="">
					<input id="validityFile" name="upload" type="file" style="width: 150px;cursor: pointer;"/>
					<input id="validityFileButton" type="button" value="上传" onclick="processValidityImportExcel()" style="width:50px;cursor: pointer;">
					&nbsp;
					<a onclick="processValidityReportDownloadExcel()"
						style="cursor: pointer; color: #005EA7;" ><s:text
							name="downLoadExcel" /> </a> &nbsp;
					<a onclick="processValidityErrorDateExcel()"
						style="cursor: pointer; color: #005EA7;"><s:text
							name="exportErrorData" /> </a>
					

				</div>
			</div>
		</div>
		<div>
			<form action="" id="processValidityForm" method="post">
				<!-- 责任部门ID -->
				<input name="dutyOrgIds" type="hidden" id="orgDutyId_validity_id"
					value="-1" />
				<input name="dutyOrgNames" type="hidden"
					id="orgDutyName_validity_id" value="" />
				<!-- 流程,流程地图ID -->
				<input name="processMapIds" type="hidden"
					id="processMapId_validity_id" value="-1" />
				<input name="processMapNames" type="hidden"
					id="processMapName_validity_id" value="" />
			</form>
		</div>
			
		<div>
			<form id="importForm" enctype="multipart/form-data" style="display:none;" method="POST">  
			    <input id="validityFile" name="upload" type="file" >   
	    </div>
	</div>
	</div>
	</body>
</html>