package com.jecn.epros.domain.journal;

import java.util.List;

public class LogUserVisitsView {
    private int total;
    private List<UserVisitsResult> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<UserVisitsResult> getData() {
        return data;
    }

    public void setData(List<UserVisitsResult> data) {
        this.data = data;
    }
}
