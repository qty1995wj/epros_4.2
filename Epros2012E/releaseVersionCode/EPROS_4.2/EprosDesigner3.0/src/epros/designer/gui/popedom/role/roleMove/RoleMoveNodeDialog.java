package epros.designer.gui.popedom.role.roleMove;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.gui.popedom.role.JecnRoleCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 角色节点移动
 * 
 * @author 2012-06-20
 * 
 */
public class RoleMoveNodeDialog extends JecnMoveChooseDialog {
	private static Logger log = Logger.getLogger(RoleMoveNodeDialog.class);

	public RoleMoveNodeDialog(List<JecnTreeNode> listMoveNodes, JecnTree jTree) {
		super(listMoveNodes, jTree);
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyRoleMoveTree(getListIds());
		// if (JecnConstants.isMysql) {
		// return new RoutineRoleMoveTree(this.getListIds());
		// } else {
		// return new HighEfficiencyRoleMoveTree(getListIds());
		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		return title;
	}

	@Override
	public boolean savaData(List<Long> ids, Long pid) {
		try {
			ConnectionPool.getJecnRole().moveRoles(ids, pid, JecnConstants.getUserId());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("RoleMoveNodeDialog savaData is error", e);
			return false;
		}
	}

	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			List<JecnTreeBean> list = JecnRoleCommon.getRoleTreeBeans(pid);
			for (JecnTreeNode node : listMoveNodes) {
				for (JecnTreeBean tr : list) {
					if (node.getJecnTreeBean().getTreeNodeType() == tr.getTreeNodeType()
							&& node.getJecnTreeBean().getName().equals(tr.getName())) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeHaved") + "\""
								+ tr.getName() + "\"");
						return true;
					}

				}
			}
		} catch (Exception e) {
			log.error("RoleMoveNodeDialog validateName is error", e);
			return false;

		}
		return false;
	}

}
