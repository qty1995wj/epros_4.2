package epros.designer.tree;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.draw.util.JecnUIUtil;

/**
 * @author yxw 2012-4-28
 * @description：设置树节点的显示图标
 */
public class JecnTreeRenderer extends DefaultTreeCellRenderer {
	private static Logger log = Logger.getLogger(JecnTreeRenderer.class);
	public static ImageIcon project;// 项目
	public static ImageIcon processRoot;// 流程根节点
	public static ImageIcon processMap;// 流程地图
	public static ImageIcon processMapNoPub;// 流程地图未发布
	public static ImageIcon process;// 流程
	public static ImageIcon processNoPub;// 流程未发布
	public static ImageIcon processFileNoPub;// 流程未发布
	public static ImageIcon organizationRoot;// 组织根节点
	public static ImageIcon organization;// 组织
	public static ImageIcon position;// 岗位
	public static ImageIcon person;// 人员
	public static ImageIcon standardRoot;// 标准根节点
	public static ImageIcon standardDir;// 标准目录
	public static ImageIcon standard;// 标准
	public static ImageIcon standardProcess;// 标准流程
	public static ImageIcon standardProcessMap;// 标准流程地图
	public static ImageIcon standardProcessNoPub;// 标准流程
	public static ImageIcon standardProcessMapNoPub;// 标准流程地图
	public static ImageIcon standardClause;// 标准条款
	public static ImageIcon standardClauseRequire;// 标准条款
	public static ImageIcon ruleRoot;// 制度根节点
	public static ImageIcon ruleDir;// 制度目录
	public static ImageIcon ruleFile;// 制度文件
	public static ImageIcon ruleFileUpdate;// 制度文件更新
	public static ImageIcon ruleFileNoPub;// 制度文件未发布
	public static ImageIcon ruleModeFile;// 制度模板文件
	public static ImageIcon ruleModeFileUpdate;// 制度模板文件更新
	public static ImageIcon ruleModeFileNoPub;// 制度模板文件未发布

	public static ImageIcon ruleDirDel;// 制度目录删除
	public static ImageIcon ruleFileDel;// 制度文件删除
	public static ImageIcon ruleFileNoPubDel;// 制度文件未发布删除
	public static ImageIcon ruleModeFileDel;// 制度模板文件删除
	public static ImageIcon ruleModeFileNoPubDel;// 制度模板文件未发布删除

	public static ImageIcon fileRoot;// 文件根节点
	public static ImageIcon fileDir;// 文件目录
	public static ImageIcon fileDirNoDel;// 文件目录
	public static ImageIcon file;// 文件
	public static ImageIcon fileUpdate;// 文件更新
	public static ImageIcon fileNoPub;// 文件
	public static ImageIcon positionGroupRoot;// 岗位组根节点
	public static ImageIcon positionGroupDir;// 岗位组目录
	public static ImageIcon positionGroup;// 岗位组
	public static ImageIcon roleRoot; // 角色根节点
	public static ImageIcon roleDir;// 角色目录
	public static ImageIcon role;// 角色
	public static ImageIcon roleDefaultDir;// 默认角色目录
	public static ImageIcon roleSecondAdmin;// 二级管理节点
	public static ImageIcon roleDefault;// 默认角色节点
	public static ImageIcon toolRoot;// 支持工具根节点
	public static ImageIcon tool;// 支持工具
	public static ImageIcon ruleModeRoot;// 制度模板根节点
	public static ImageIcon ruleModeDir;// 制度模板目录
	public static ImageIcon ruleMode; // 制度模板
	public static ImageIcon processModeRoot; // 流程模板根节点
	public static ImageIcon processMapMode; // 流程地图模板
	public static ImageIcon processMode; // 流程图模板
	public static ImageIcon processModeFileRoot;// 流程模板文件根节点
	public static ImageIcon processModeFileDir; // 流程模板文件目录
	public static ImageIcon processModeFile; // 流程模板文件
	public static ImageIcon modeRoot;// 流程模板管理根节点
	public static ImageIcon riskRoot;// 风险根目录
	public static ImageIcon riskDir;// 风险目录
	public static ImageIcon riskPoint;// 风险点
	public static ImageIcon innerControlRoot;// 内控根目录
	public static ImageIcon innerControlDir;// 内控目录
	public static ImageIcon innerControlClause;// 内控条款
	public static ImageIcon posGroupRelPos;// 岗位组关联岗位图标

	public static ImageIcon organizationDel;// 岗位组删除的图标
	public static ImageIcon fileDirDel;// 文件目录被删除的图标
	public static ImageIcon fileDel;// 文件被删除的图标

	public static ImageIcon fileNoPubDel;// 未发布文件被删除的图标
	public static ImageIcon processMapDel;// 流程地图删除
	public static ImageIcon processMapNoPubDel;// 流程地图未发布删除
	public static ImageIcon processDel;// 流程删除
	public static ImageIcon processNoPubDel;// 流程未发布删除
	public static ImageIcon processUpdate;// 流程更新
	public static ImageIcon processMapUpdate;// 流程架构更新
	public static ImageIcon processCode;
	public static ImageIcon processCodeRoot;
	public static ImageIcon termDefineRoot;// 术语定义根目录
	public static ImageIcon termDefineDir;// 术语定义目录
	public static ImageIcon termDefine;// 术语定义

	public static ImageIcon processFile;

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
			boolean leaf, int row, boolean hasFocus) {
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
		// 改变节点不选择时的背景色
		super.setBackgroundNonSelectionColor(JecnUIUtil.getDefaultBackgroundColor());
		JecnTreeNode node = (JecnTreeNode) value;
		try {
			JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
			// 树节点类型的字符串值
			TreeNodeType treeNodeType = jecnTreeBean.getTreeNodeType();
			String treeNodeTypeString = jecnTreeBean.getTreeNodeType().toString();
			if ((treeNodeType == TreeNodeType.process || treeNodeType == TreeNodeType.processFile
					|| treeNodeType == TreeNodeType.processMap || treeNodeType == TreeNodeType.ruleFile
					|| treeNodeType == TreeNodeType.ruleModeFile || treeNodeType == TreeNodeType.file
					// || treeNodeType == TreeNodeType.ruleDir
					|| treeNodeType == TreeNodeType.standardProcess || treeNodeType == TreeNodeType.standardProcessMap)) {
				if (jecnTreeBean.getIsDelete() == 0) {
					if (jecnTreeBean.isPub()) {
						if (jecnTreeBean.isUpdate()
								&& (treeNodeType.equals(TreeNodeType.process) || treeNodeType
										.equals(TreeNodeType.processMap)|| treeNodeType
										.equals(TreeNodeType.file)|| treeNodeType
										.equals(TreeNodeType.ruleModeFile)|| treeNodeType
										.equals(TreeNodeType.ruleFile))) {
							if (null == JecnTreeRenderer.class.getField(treeNodeTypeString + "Update").get(null)) {
								JecnTreeRenderer.class.getField(treeNodeTypeString + "Update").set(treeNodeTypeString,
										new ImageIcon("images/treeNodeImages/" + treeNodeTypeString + "Update.gif"));
							}
							// 设置图标
							setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString + "Update").get(null));
						} else {
							if (null == JecnTreeRenderer.class.getField(treeNodeTypeString).get(null)) {
								JecnTreeRenderer.class.getField(treeNodeTypeString).set(treeNodeTypeString,
										new ImageIcon("images/treeNodeImages/" + treeNodeTypeString + ".gif"));
							}
							// 设置图标
							setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString).get(null));
						}
					} else {
						if (null == JecnTreeRenderer.class.getField(treeNodeTypeString + "NoPub").get(null)) {
							JecnTreeRenderer.class.getField(treeNodeTypeString + "NoPub").set(treeNodeTypeString,
									new ImageIcon("images/treeNodeImages/" + treeNodeTypeString + "NoPub.gif"));
						}
						// 设置图标
						setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString + "NoPub").get(null));
					}
				} else {
					if (jecnTreeBean.isPub()) {
						if (null == JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").get(null)) {
							JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").set(treeNodeTypeString,
									new ImageIcon("images/treeNodeImages/" + treeNodeTypeString + "Del.gif"));
						}
						// 设置图标
						setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").get(null));
					} else {
						if (null == JecnTreeRenderer.class.getField(treeNodeTypeString + "NoPubDel").get(null)) {
							JecnTreeRenderer.class.getField(treeNodeTypeString + "NoPubDel").set(treeNodeTypeString,
									new ImageIcon("images/treeNodeImages/" + treeNodeTypeString + "NoPubDel.gif"));
						}
						// 设置图标
						setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString + "NoPubDel").get(null));
					}
				}
			} else if (treeNodeType == TreeNodeType.organization) {// 组织文件显示：0：未删除；1：已删除
				if (jecnTreeBean.getIsDelete() == 1) {
					if (null == JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").get(null)) {
						JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").set(treeNodeTypeString,
								new ImageIcon("images/treeNodeImages/" + treeNodeTypeString + "Del.gif"));
					}
					// 设置图标
					setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").get(null));
				} else {
					if (null == JecnTreeRenderer.class.getField(treeNodeTypeString).get(null)) {
						JecnTreeRenderer.class.getField(treeNodeTypeString).set(
								treeNodeTypeString,
								new ImageIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString()
										+ ".gif"));
					}
					// 设置图标
					setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString).get(null));
				}

			} else if (treeNodeType == TreeNodeType.fileDir) {// 文件目录显示：0：未删除；1：已删除
				if (jecnTreeBean.getIsDelete() == 1) {
					if (null == JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").get(null)) {

						JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").set(treeNodeTypeString,
								new ImageIcon("images/treeNodeImages/" + treeNodeTypeString + "Del.gif"));

					}
					// 设置图标
					setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").get(null));
				} else {
					if (null == JecnTreeRenderer.class.getField(treeNodeTypeString).get(null)) {
						JecnTreeRenderer.class.getField(treeNodeTypeString).set(
								treeNodeTypeString,
								new ImageIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString()
										+ ".gif"));
					}
					// 设置图标
					setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString).get(null));
				}
			} else if (treeNodeType == TreeNodeType.ruleDir) {// 文件目录显示：0：未删除；1：已删除
				if (jecnTreeBean.getIsDelete() == 1) {
					if (null == JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").get(null)) {

						JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").set(treeNodeTypeString,
								new ImageIcon("images/treeNodeImages/" + treeNodeTypeString + "Del.gif"));

					}
					// 设置图标
					setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString + "Del").get(null));
				} else {
					if (null == JecnTreeRenderer.class.getField(treeNodeTypeString).get(null)) {
						JecnTreeRenderer.class.getField(treeNodeTypeString).set(
								treeNodeTypeString,
								new ImageIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString()
										+ ".gif"));
					}
					// 设置图标
					setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString).get(null));
				}
			} else if (TreeNodeType.roleSecondAdminDefaultDir == treeNodeType) {
				return this;
			} else if (TreeNodeType.processCodeRoot == treeNodeType) {
				return this;
			} else if (TreeNodeType.termDefineRoot == treeNodeType) {
				// 设置图标
				setIcon((Icon) new ImageIcon("images/treeNodeImages/" + TreeNodeType.termDefineRoot.toString() + ".gif"));
			} else if (TreeNodeType.termDefineDir == treeNodeType) {
				// 设置图标
				setIcon((Icon) new ImageIcon("images/treeNodeImages/" + "fileDir.gif"));
			} else if (TreeNodeType.termDefine == treeNodeType) {
				// 设置图标
				setIcon((Icon) new ImageIcon("images/treeNodeImages/" + TreeNodeType.termDefine.toString() + ".gif"));
			} else {
				if (null == JecnTreeRenderer.class.getField(treeNodeTypeString).get(null)) {
					JecnTreeRenderer.class.getField(treeNodeTypeString)
							.set(
									treeNodeTypeString,
									new ImageIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString()
											+ ".gif"));
				}
				// 设置图标
				setIcon((Icon) JecnTreeRenderer.class.getField(treeNodeTypeString).get(null));
			}

		} catch (Exception e) {
			log.error("JecnTreeRenderer is error", e);
		}
		setText(value.toString());
		return this;
	}
}
