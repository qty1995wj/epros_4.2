package com.jecn.epros.server.dao.task.search.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.dao.task.search.IJecnTaskSearchDao;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.epros.server.webBean.task.TaskSearchTempBean;

/**
 * 任务查询，。删除，显示全部记录DAO接口实现
 * 
 * @author ZHANGXH
 * @date： 日期：Nov 20, 2012 时间：3:06:12 PM
 */
public class JecnTaskSearchDaoImpl extends AbsBaseDao<JecnTaskBeanNew, Long> implements IJecnTaskSearchDao {
	/**
	 * 删除任务相关信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @throws Exception
	 */
	public void deleteTaskBean(Long taskId) throws Exception {
		// 删除任务主表
		this.delete(taskId);
		String hql = "";
		// 删除任务各阶段审批人记录表
		hql = "delete from JecnTaskApprovePeopleConn where stageId in(select id from JecnTaskStage where taskId =?)";
		this.execteHql(hql, taskId);
		// 删除任务各阶段信息表
		hql = "delete from JecnTaskStage where taskId=?";
		this.execteHql(hql, taskId);
		// 删除任务目标人表
		hql = "delete from JecnTaskPeopleNew where taskId =?";
		this.execteHql(hql, taskId);
		// 删除任务流传记录表
		hql = "delete from JecnTaskForRecodeNew where taskId =?";
		this.execteHql(hql, taskId);
		// 删除任务试运行文件表
		hql = "delete from JecnTaskTestRunFile where taskId =?";
		this.execteHql(hql, taskId);
		// 删除任务任务显示项表
		hql = "delete from JecnTaskApplicationNew where taskId =?";
		this.execteHql(hql, taskId);
	}

	/**
	 * 根据任务主键ID集合删除任务相关信息
	 * 
	 * @param taskIds
	 *            任务主键ID集合
	 * @throws Exception
	 */
	@Override
	public void deleteTaskByIds(Set<Long> taskIds) throws Exception {
		String hql = "";
		List<String> list = null;
		// 删除任务主表
		hql = "delete from JecnTaskBeanNew where id in";
		list = JecnCommonSql.getSetSqls(hql, taskIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除任务各阶段审批人记录表
		hql = "delete from JecnTaskApprovePeopleConn where taskId in";
		list = JecnCommonSql.getSetSqls(hql, taskIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除任务目标人表
		hql = "delete from JecnTaskPeopleNew where taskId in";
		list = JecnCommonSql.getSetSqls(hql, taskIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除任务流传记录表
		hql = "delete from JecnTaskForRecodeNew where taskId in";
		list = JecnCommonSql.getSetSqls(hql, taskIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除任务试运行文件表
		hql = "delete from JecnTaskTestRunFile where taskId in";
		list = JecnCommonSql.getSetSqls(hql, taskIds);
		for (String str : list) {
			execteHql(str);
		}
		// 删除任务任务显示项表
		hql = "delete from JecnTaskApplicationNew where taskId in";
		list = JecnCommonSql.getSetSqls(hql, taskIds);
		for (String str : list) {
			execteHql(str);
		}
	}

	/**
	 * 
	 * 假删除
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @throws Exception
	 */
	@Override
	public void falseDeleteTask(Long taskId) throws Exception {
		String sql = "update Jecn_Task_Bean_New set is_lock=0 where id = ?";
		this.execteNative(sql, taskId);

		// 厦门金旅待办删除
		if (!JecnConfigTool.isGoldenDragonLoginType()) {
			return;
		}
		sql = "DELETE TASK_APPROVAL_VIEW WHERE TASK_ID = ?";
		this.execteNative(sql, taskId);
	}

	@Override
	public void updateTaskApprovePeople(String fromPeopleId, String toPeopleId) throws Exception {
		
		
		//更新各阶段审批人表
		String sql="update jecn_task_people_conn_new  set approve_pid="+toPeopleId
		+" where id in("
		+" select jtpn.id from jecn_task_people_conn_new jtpn"
		+" inner join jecn_task_stage jts on jtpn.stage_id=jts.id"
		+" inner join jecn_task_bean_new jtbn on jts.task_id=jtbn.id"
		+" where "
		+" jtpn.approve_pid="+fromPeopleId
		+" and jtbn.state <>5"
		+" )";
		this.execteNative(sql);
		//更新当前阶段审批人表
		sql="update jecn_task_people_new  set approve_pid="+toPeopleId
		+" where approve_pid="+fromPeopleId;
		this.execteNative(sql);
		//更新任务主表 如果来源人是拟稿人
		sql=" update jecn_task_bean_new  set create_person_id="+toPeopleId
		+" where create_person_id="+fromPeopleId
		+" and state <>5 ";
		this.execteNative(sql);
		//更新任务主表 如果目标人是想要替换的人
		sql=" update jecn_task_bean_new  set from_person_id="+toPeopleId
		+" where from_person_id="+fromPeopleId
		+" and state <>5 ";
		this.execteNative(sql);
		
	}

	private String getSecondAdminTaskSqlByTaskType(Long roleId,Long peopleId,int type){
		
		String ownerSql=getSecondAdminOwnerTaskSql(peopleId,type);
		String permSql="";
		/** 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图 */
		switch (type) {
		case 0:
			permSql=getSecondAdminFlowTaskSql(roleId);
			break;
		case 1:
			permSql=getSecondAdminFileTaskSql(roleId);
			break;
		case 2:
		case 3:
			permSql=getSecondAdminRuleTaskSql(roleId);
			break;
		case 4:
			permSql=getSecondAdminFlowMapTaskSql(roleId);
			break;
		default:
			break;
		}
		String sql="select distinct a.* from ("+ownerSql+" union "+permSql+")a";
		
		return sql;
	}
	
	
	private String getSecondAdminTaskSql(Long roleId, Long peopleId){
		
		String  ownerSql=getSecondAdminOwnerTaskSql(peopleId,-1);
		
		String permSql="select a.* from ( "
		+ getSecondAdminFileTaskSql(roleId)
		+ " union "
		+ getSecondAdminFlowMapTaskSql(roleId)
		+ " union "
		+ getSecondAdminFlowTaskSql(roleId)
		+ " union "
		+ getSecondAdminRuleTaskSql(roleId)+")a ";

		String sql="select distinct a.* from ("+ownerSql+" union "+permSql+") a";
		
		return sql;
	
	}
	
	
	private String getSecondAdminOwnerTaskSql(Long peopleId, int type){
		
		String condition = type == -1 ? "" : " and a.task_type=" + type;
		String ownerSql = " select a.* from  jecn_task_bean_new a where a.is_lock=1 "
				+ condition
				+ "  AND EXISTS (SELECT 1 FROM JECN_TASK_FOR_RECODE_NEW FRN WHERE FRN.TASK_ID = a.ID AND FRN.TO_PERSON_ID = "
				+ peopleId + ")";
		
		return ownerSql;
	}
	
	
	private String getSecondAdminFlowTaskSql(Long roleId){
		
		String sql=" select a.*" +
		"  from jecn_task_bean_new a," + 
		"       jecn_flow_structure_t   c," + 
		"       jecn_flow_structure_t   p," + 
		"       jecn_role_content     r" + 
		" where c.t_path like p.t_path "+JecnCommonSql.getConcatChar()+"'%'" + 
		"   and p.flow_id = r.relate_id" + 
		"   and r.role_id = " + roleId +
//		"   and c.project_id=" + projectId +
		"   and r.type = 0" + 
		"   and c.del_state = 0" + 
		"   and p.del_state = 0" + 
		"   and a.r_id = c.flow_id" + 
		"   and a.is_lock=1 " +
		"   and a.task_type = 0";
		
		return sql;
		
		
	}
	private String getSecondAdminFlowMapTaskSql(Long roleId){
		
		String sql=" select a.*" +
		"  from jecn_task_bean_new a," + 
		"       jecn_flow_structure_t   c," + 
		"       jecn_flow_structure_t   p," + 
		"       jecn_role_content     r" + 
		" where c.t_path like p.t_path "+JecnCommonSql.getConcatChar()+"'%'" + 
		"   and p.flow_id = r.relate_id" + 
		"   and r.role_id = " + roleId +
//		"   and c.project_id=" + projectId +
		"   and r.type = 0" + 
		"   and c.del_state = 0" + 
		"   and p.del_state = 0" + 
		"   and a.r_id = c.flow_id" + 
		"   and a.is_lock=1 " +
		"   and a.task_type = 4";
		
		
		return sql;
		
		
	}
	private String getSecondAdminFileTaskSql(Long roleId){
		
		String sql=" select a.*" +
		"  from jecn_task_bean_new a," + 
		"       JECN_FILE_t             c," + 
		"       JECN_FILE_t             p," + 
		"       jecn_role_content     r" + 
		" where c.t_path like p.t_path "+JecnCommonSql.getConcatChar()+"'%'" + 
		"   and p.file_id = r.relate_id" + 
		"   and r.role_id = " + roleId +
//		"   and c.project_id=" + projectId +
		"   and r.type = 1" + 
		"   and c.del_state = 0" + 
		"   and p.del_state = 0" + 
		"   and a.r_id = c.file_id" + 
		"   and a.is_lock=1 " +
		"   and a.task_type = 1";
		
		
		return sql;
		
		
	}
	private String getSecondAdminRuleTaskSql(Long roleId){
		
		String sql=" select a.*" +
		"  from jecn_task_bean_new a," + 
		"       JECN_RULE_t             c," + 
		"       JECN_RULE_t             p," + 
		"       jecn_role_content     r" + 
		" where c.t_path like p.t_path "+JecnCommonSql.getConcatChar()+"'%'" + 
		"   and r.role_id = " + roleId +
//		"   and c.project_id=" + projectId +
		"   and r.type = 3" + 
		"   and a.r_id = c.id" + 
		"   and a.is_lock=1 " +
		"   and a.task_type in (2, 3)";
		
		return sql;
		
	}

	@Override
	public int countAllSecondAdminTaskNum(Long roleId,  Long peopleId,
			TaskSearchTempBean searchTempBean) throws Exception {
		
		String sql="";
		// 拼装查询条件
		List<Object> listArgs = new ArrayList<Object>();
		String searchSql = getSearchSql(searchTempBean,listArgs,roleId,peopleId);
		// 如果搁置任务
		if (searchTempBean != null && searchTempBean.isDelayTask()) {

			sql = " with tmp as(select distinct jts.task_id "
					+ " from jecn_task_stage jts where jts.is_selected_user=1 AND JTS.STAGE_MARK!=10"
					+ " and ( "
					+ " SELECT COUNT(JTPCN.id) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN"
					+ " ,jecn_user ju"
					+ " WHERE JTPCN.STAGE_ID=jts.id "
					+ "  and JTPCN.approve_pid=ju.people_id and ju.islock=0"
					+ " )=0 "
					+ " union "
					+ " select distinct jts.task_id "
					+ " from jecn_task_stage jts where jts.is_selected_user=1 AND JTS.STAGE_MARK=3"
					+ " and ( "
					+ " SELECT COUNT(distinct JTPCN.approve_pid) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN"
					+ " ,jecn_user ju"
					+ " WHERE JTPCN.STAGE_ID=jts.id "
					+ "  and JTPCN.approve_pid=ju.people_id and ju.islock=0"
					+ " )<>(select  COUNT(JTPCN.id) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN WHERE JTPCN.STAGE_ID=jts.id )"
					+ " union " // 最新的操作人没有或者没有岗位
					+ " select distinct jtbn.id" + " from jecn_task_bean_new jtbn"
					+ " left join jecn_task_people_new jtpn " + " on jtbn.id =jtpn.task_id"
					+ " left join jecn_user u " + " on jtpn.approve_pid =u.people_id"
					+ " where (jtpn.id is null or u.people_id is null or u.islock=1)" + "  and jtbn.state <> 5"
					+ "  and jtbn.is_lock = 1" + " )" + "  select  count(a.id) from (" + searchSql + ") a"
					+ "  inner join tmp on a.id=tmp.task_id"
					+ "  left join jecn_user u on a.create_person_id=u.people_id"
					+ "  where a.IS_LOCK=1 and a.State<>5 ";
		}else{
			sql = "select count(a.id) from (" + searchSql + ") a";
		}
		
		// 根据查询条件获取返回的总数
		return this.countAllByParamsNativeSql(sql, listArgs
				.toArray());
	}
	
	private String getSearchSql(TaskSearchTempBean searchTempBean, List<Object> listArgs ,Long roleId,Long peopleId) {
		
		
		String sql = "";
		if (searchTempBean != null) {
			if (searchTempBean.getTaskType() != -1) {// 任务类型
				sql = getSecondAdminTaskSqlByTaskType(roleId, peopleId, searchTempBean.getTaskType());
			}else{
				sql = getSecondAdminTaskSql(roleId, peopleId);
			}
			sql += " where 1=1 ";
			if (searchTempBean.getTaskStage() != -1) {// 任务阶段
				sql = sql + " and  A.state = ?";
				listArgs.add(searchTempBean.getTaskStage());
			}
			if (!JecnCommon.isNullOrEmtryTrim(searchTempBean.getTaskName())) {// 任务名称模糊查询
				sql = sql + " and A.task_name like ?";
				listArgs.add("%" + searchTempBean.getTaskName() + "%");
			}
			if (searchTempBean.getCreatePeopleId() != null) {// 创建人模糊查询
				sql = sql + " and A.CREATE_PERSON_ID = ?";
				listArgs.add(searchTempBean.getCreatePeopleId());
			}
			if (JecnTaskCommon.isRightTime(searchTempBean.getStartTime(), searchTempBean.getEndTime())) {// 时间段查询

				// 字符串转换成时间拼接时、分、秒
				searchTempBean.setStartTime(searchTempBean.getStartTime() + " 00:00:00");
				searchTempBean.setEndTime(searchTempBean.getEndTime() + " 23:59:59");
				// 字符串转换日期类型 开始时间
				Date sDate = JecnCommon.getDateTimeByString(searchTempBean.getStartTime());
				// 字符串转换日期类型 结束时间
				Date eDate = JecnCommon.getDateTimeByString(searchTempBean.getEndTime());
				sql = sql + " and A.UPDATE_TIME>=?" + " and A.UPDATE_TIME<=?";
				// 添加到数组
				listArgs.add(sDate);
				listArgs.add(eDate);
			}
		} else {
			sql = getSecondAdminTaskSql(roleId, peopleId);
		}
		return sql;
	}

	@Override
	public List<MyTaskBean> listSecondAdminList(TaskSearchTempBean searchTempBean, int start, int limit,
			Long curPeopleId, Long roleId) throws Exception {

		List<Object[]> objectList = null;
		String sql = "";
		// 拼装查询条件
		List<Object> listArgs = new ArrayList<Object>();
		// 拼装查询条件
		String searchSql = getSearchSql(searchTempBean, listArgs, roleId, curPeopleId);

		// 如果搁置任务
		if (searchTempBean != null && searchTempBean.isDelayTask()) {
			// 默认是Oracle数据库
			if (JecnCommon.isOracle()) {
				sql = "with tmp as(select distinct jts.task_id "
						+ " from jecn_task_stage jts where jts.is_selected_user=1 AND JTS.STAGE_MARK!=10"
						+ " and ( "
						+ " SELECT COUNT(JTPCN.id) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN"
						+ " ,jecn_user ju"
						+ " WHERE JTPCN.STAGE_ID=jts.id "
						+ "  and JTPCN.approve_pid=ju.people_id and ju.islock=0"
						+ " )=0 "
						+ " union "
						+ " select distinct jts.task_id "
						+ " from jecn_task_stage jts where jts.is_selected_user=1 AND JTS.STAGE_MARK=3"
						+ " and ( "
						+ " SELECT COUNT(distinct JTPCN.approve_pid) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN"
						+ " ,jecn_user ju"
						+ " WHERE JTPCN.STAGE_ID=jts.id "
						+ "  and JTPCN.approve_pid=ju.people_id and ju.islock=0"
						+ " )<>(select  COUNT(JTPCN.id) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN WHERE JTPCN.STAGE_ID=jts.id )"
						+ " union " // 当前操作人离职
						+ " select distinct jtbn.id" + " from jecn_task_bean_new jtbn"
						+ " left join jecn_task_people_new jtpn " + " on jtbn.id =jtpn.task_id"
						+ " left join jecn_user u " + " on jtpn.approve_pid =u.people_id"
						+ " where (jtpn.id is null or u.people_id is null or u.islock=1)" + "  and jtbn.state <> 5"
						+ "  and jtbn.is_lock = 1" + " )" + " select " + " a.id," + " a.task_name," + " a.state,"
						+ " a.task_else_state," + " a.task_type," + " u.true_name," + " a.create_person_id,"
						+ " a.update_time ," + " a.start_time," + " a.end_time," + " a.up_state,"
						+ " a.from_person_id," + " JTS.STAGE_NAME," + " jthn.publish_date" + "  from (" + searchSql
						+ ") a" + "  inner join tmp on a.id=tmp.task_id"
						+ "	INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
						+ "  left join jecn_user u on a.create_person_id=u.people_id"
						+ " left join jecn_task_history_new jthn on jthn.task_id=a.id"
						+ "  where a.IS_LOCK=1 and a.State<>5 ";

				// 如果是SQLServer 数据库
			} else if (JecnCommon.isSQLServer()) {
				sql = "select  a.id," + " a.task_name," + " a.state, " + " a.task_else_state, " + " a.task_type, "
						+ " u.true_name, " + " a.create_person_id, " + " a.update_time ,"
						+ " convert(varchar(100), a.start_time, 23) as start_time, "
						+ " convert(varchar(100), a.end_time, 23) as end_time, " + " a.up_state,"
						+ " a.from_person_id," + " JTS.STAGE_NAME," + " jthn.publish_date" + "  from ("
						+ searchSql
						+ ") a"
						+ " inner join (select jts.task_id "
						+ " from jecn_task_stage jts "
						+ " where jts.is_selected_user = 1 "
						+ " AND JTS.STAGE_MARK != 10 "
						+ " and (SELECT COUNT(JTPCN.id) "
						+ " FROM JECN_TASK_PEOPLE_CONN_NEW  JTPCN, "
						+ " jecn_user                  ju "
						+ " WHERE JTPCN.STAGE_ID = jts.id "
						+ " and JTPCN.approve_pid = ju.people_id "
						+ " and ju.islock = 0) = 0 "
						+ " union "
						+ " select jts.task_id "
						+ " from jecn_task_stage jts "
						+ " where jts.is_selected_user = 1 "
						+ " AND JTS.STAGE_MARK = 3 "
						+ " and (SELECT COUNT(distinct JTPCN.approve_pid) "
						+ " FROM JECN_TASK_PEOPLE_CONN_NEW  JTPCN, "
						+ " jecn_user                  ju "
						+ " WHERE JTPCN.STAGE_ID = jts.id "
						+ " and JTPCN.approve_pid = ju.people_id "
						+ " and ju.islock = 0) <> "
						+ " (select COUNT(JTPCN.id) "
						+ " FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN "
						+ " WHERE JTPCN.STAGE_ID = jts.id)"
						+ " union " // 最新的操作人没有或者没有岗位
						+ " select distinct jtbn.id"
						+ " from jecn_task_bean_new jtbn"
						+ " left join jecn_task_people_new jtpn "
						+ " on jtbn.id =jtpn.task_id"
						+ " left join jecn_user u "
						+ " on jtpn.approve_pid =u.people_id"
						+ " where (jtpn.id is null or u.people_id is null or u.islock=1)"
						+ "  and jtbn.state <> 5"
						+ "  and jtbn.is_lock = 1"
						+ " )"
						+ " tmp on a.id = tmp.task_id "
						+ "INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
						+ " left join jecn_user u on a.create_person_id = u.people_id "
						+ " left join jecn_task_history_new jthn on jthn.task_id=a.id "
						+ " where a.is_lock = 1 and a.state<>5 ";
			}
		} else {

			sql = "SELECT ALL_TASK.ID TASK_ID," + "               ALL_TASK.TASK_NAME,"
					+ "               ALL_TASK.STATE," + "               ALL_TASK.TASK_ELSE_STATE,"
					+ "               ALL_TASK.TASK_TYPE," + "               ALL_TASK.TRUE_NAME,"
					+ "               ALL_TASK.CREATE_PERSON_ID," + "               ALL_TASK.UPDATE_TIME,"
					+ "               ALL_TASK.START_TIME," + "               ALL_TASK.END_TIME,"
					+ "               ALL_TASK.UP_STATE," + "               ALL_TASK.FROM_PERSON_ID,"
					+ " CASE WHEN ALL_TASK.STAGE_NAME IS NULL " + " AND ALL_TASK.STATE = 5 THEN"
					+ "                 '完成'" + "                ELSE" + "                 ALL_TASK.STAGE_NAME"
					+ "              END AS STAGE_NAME,"
					+ getCaseResPeople()
					+ " ,ALL_TASK.publish_date"
					+ "          FROM (SELECT A.ID,"
					+ "                       A.TASK_NAME,"
					+ "                       A.STATE,"
					+ "                       A.TASK_ELSE_STATE,"
					+ "                       A.TASK_TYPE,"
					+ "                       C.TRUE_NAME,"
					+ "                       A.CREATE_PERSON_ID,"
					+ "                       A.UPDATE_TIME,"
					+ "                       A.START_TIME,"
					+ "                       A.END_TIME,"
					+ "                       A.UP_STATE,"
					+ "                       A.FROM_PERSON_ID,"
					+ "                       JTS.STAGE_NAME,"
					+ "                       A.R_ID,"
					+ " jthn.publish_date"
					+ "                  FROM ("
					+ searchSql
					+ ") A"
					+ "                 LEFT JOIN JECN_TASK_STAGE JTS"
					+ "                    ON A.ID = JTS.TASK_ID"
					+ "                   AND A.STATE = JTS.STAGE_MARK"
					+ "                  LEFT JOIN JECN_USER C"
					+ "                    ON A.CREATE_PERSON_ID = C.PEOPLE_ID"
					+ " left join jecn_task_history_new jthn on jthn.task_id=a.id"
					+ "                 WHERE A.IS_LOCK = 1" + ") ALL_TASK";

			// 添加流程责任人
			sql = sql + addResPeopleType();

			// 根据任务结果获取任务相关信息，责任人类型获取责任人
			sql = getTaskResPeoleSql(sql);
		}

		sql += " order by a.update_time desc";

		// 分页查询满足一定条件的数据集
		if (start == -1 || limit == -1) {
			objectList = this.listNativeSql(sql, listArgs.toArray());
		} else {
			objectList = this.listNativeSql(sql, start, limit, listArgs.toArray());
		}

		// 根据object数组获取MyTaskBean集合
		return getMyTaskBeanList(objectList, curPeopleId, roleId);
	}
	

	private List<MyTaskBean> getMyTaskBeanList(List<Object[]> objectList,
			Long loginPeopleId,Long roleId) throws Exception {
		if (objectList == null || loginPeopleId == null) {
			throw new NullPointerException(
					"getMyTaskBeanList获取任务记录异常！objectList == null || loginPeopleId == null");
		}
		
		// 记录登录人相关任务信息
		List<MyTaskBean> list = new ArrayList<MyTaskBean>();
		if(objectList.size()==0){
			return list;
		}
		
		Map<String, Boolean> taskHasPermMap = getTaskPermMap(objectList,roleId);
		
		for (Object[] objects : objectList) {
			// 我的任务信息
			MyTaskBean myTaskBean = new MyTaskBean();
			Long taskId=JecnTaskCommon.getlongByObject(objects[0]);
			// 0任务主键ID
			myTaskBean.setTaskId(taskId);
			// 1任务名称
			myTaskBean.setTaskName(objects[1].toString());
			// 2任务阶段
			myTaskBean.setTaskStage(JecnTaskCommon.getIntByObject(objects[2]));

			// 操作状态
			int taskEleType = JecnTaskCommon.getIntByObject(objects[3]);
	
			// 有权限
			if (taskHasPermMap.get(objects[0].toString())!=null&&taskHasPermMap.get(objects[0].toString())) {// 我的任务
				myTaskBean.setTaskElseState(2);
			} 

			// 4任务类型
			myTaskBean.setTaskType(JecnTaskCommon.getIntByObject(objects[4]));

			// 5创建人真实姓名
			if (objects[5] != null) {
				myTaskBean.setCreateName(objects[5].toString());
			}

			if (objects[7] != null) {
				myTaskBean.setUpdateTime(objects[7].toString());
			}
			if (objects[8] != null) {
				myTaskBean.setStartTime(JecnUtil.valueToDateStr(objects[8]));
			}
			if (objects[9] != null) {
				myTaskBean.setEndTime(JecnUtil.valueToDateStr(objects[9]));
			}
			if (objects[12] != null) {
				myTaskBean.setStageName(objects[12].toString());
			}
			if (objects[13] != null) {
				myTaskBean.setResPeopleName(objects[13].toString());
			}
			if (objects.length>14&&objects[14] != null) {
				myTaskBean.setPublishTime(JecnUtil.valueToDateStr(objects[14]));
			}
			list.add(myTaskBean);
		}
		return list;
	}

	/**
	 * 获得当前人员的任务的权限map
	 * @param objectList
	 * @param roleId
	 * @return
	 */
	private Map<String, Boolean> getTaskPermMap(List<Object[]> objectList, Long roleId)throws Exception {
		// 获得该人员的任务
		List<Long> taskIds = new ArrayList<Long>();
		for (Object[] obj : objectList) {
			taskIds.add(JecnTaskCommon.getlongByObject(obj[0]));
		}

		/** type 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图 */
		String relatedPath = "select" + "       a.id," + "       a.r_id," + "       a.task_type," + "       case"
				+ "         when a.task_type = 0 or a.task_type = 4 then" + "          (select f.t_path"
				+ "             from jecn_flow_structure_t f" + "            where a.r_id = f.flow_id)"
				+ "         when a.task_type = 1 then"
				+ "          (select f.t_path from jecn_file_t f where a.r_id = f.file_id)"
				+ "         when a.task_type = 2 or a.task_type = 3 then"
				+ "          (select f.t_path from jecn_rule_t f where a.r_id = f.id)" + "       end as t_path"
				+ "  from jecn_task_bean_new a" + " where a.id in " + JecnCommonSql.getIds(taskIds);

		Map<String, String[]> taskMap = new HashMap<String, String[]>();
		List<Object[]> relatedObjs = this.listNativeSql(relatedPath);
		for (Object[] obj : relatedObjs) {
			String taskId = obj[0].toString();
			String relateId = obj[1].toString();
			String type = obj[2].toString();
			String path = obj[3] == null ? "" : obj[3].toString();
			String[] value = { type, path };
			taskMap.put(taskId, value);
		}

		// 任务类型 0是流程、1是文件、2是标准、3是制度、4是风险、5：组织
		String permPath = "select" + "       a.relate_id," + "       a.type," + "       case"
				+ "         when a.type = 0  then" + "          (select f.t_path"
				+ "             from jecn_flow_structure_t f" + "            where a.relate_id = f.flow_id)"
				+ "         when a.type = 1 then"
				+ "          (select f.t_path from jecn_file_t f where a.relate_id = f.file_id)"
				+ "         when  a.type = 3 then"
				+ "          (select f.t_path from jecn_rule_t f where a.relate_id = f.id)" + "       end as t_path"
				+ "  from jecn_role_content a" + " where a.role_id =" + roleId;

		//
		List<String> flowPaths = new ArrayList<String>();
		List<String> filePaths = new ArrayList<String>();
		List<String> rulePaths = new ArrayList<String>();

		List<Object[]> permObjs = this.listNativeSql(permPath);
		// 管理员角色类型 0是流程、1是文件、2是标准、3是制度、4是风险、5：组织
		for (Object[] obj : permObjs) {
			String relateId = obj[0].toString();
			String type = obj[1].toString();
			String path = obj[2] == null ? "" : obj[2].toString();
			if ("0".equals(type)) {
				flowPaths.add(path);
			} else if ("1".equals(type)) {
				filePaths.add(path);
			} else if ("3".equals(type)) {
				rulePaths.add(path);
			}
		}

		Map<String, Boolean> taskHasPermMap = new HashMap<String, Boolean>();

		for (Entry<String, String[]> entry : taskMap.entrySet()) {
			String taskId = entry.getKey();
			String[] typeAndpath = entry.getValue();
			/**
			 * taskType=0时，表示流程ID，taskType=1时，表示文件ID，taskType=2时，表示制度模板ID,3:
			 * 制度文件ID，4： 流程地图ID
			 */
			String taskType = typeAndpath[0];
			String path = typeAndpath[1];
			List<String> authPaths = new ArrayList<String>();
			if ("0".equals(taskType) || "4".equals(taskType)) {
				authPaths = flowPaths;
			} else if ("1".equals(taskType)) {
				authPaths = filePaths;
			} else if ("2".equals(taskType) || "3".equals(taskType)) {
				authPaths = rulePaths;
			}

			if (chargeAuthByPath(authPaths, path)) {
				taskHasPermMap.put(taskId, true);
			} else {
				taskHasPermMap.put(taskId, false);
			}

		}
		return taskHasPermMap;
	}
	
	
	private boolean chargeAuthByPath(List<String> authPaths, String path) {
		for (String authPath : authPaths) {
			if (StringUtils.isNotBlank(path) && path.startsWith(authPath)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 查询责任人case判断
	 * @return
	 */
	private String getCaseResPeople(){
		String value = JecnCommonSql.getSqlValueDefault();
		return "               CASE" + 
		"                 WHEN TASK_TYPE = 0 THEN" + 
		value + "(FBIT.RES_PEOPLE_ID, -1)" + 
		"                 WHEN TASK_TYPE = 2 THEN" + 
		value + "(JRT.RES_PEOPLE_ID, -1)" + 
		"                 WHEN TASK_TYPE = 3 THEN" + 
		value + "(JRT.RES_PEOPLE_ID, -1)" + 
		"                 ELSE" + 
		"                  -1" + 
		"               END RES_PEOPLE_ID," + 
		"               CASE" + 
		"                 WHEN TASK_TYPE = 0 THEN" + 
		value + "(FBIT.TYPE_RES_PEOPLE, -1)" + 
		"                 WHEN TASK_TYPE = 2 THEN" + 
		value + "(JRT.TYPE_RES_PEOPLE, -1)" + 
		"                 WHEN TASK_TYPE = 3 THEN" + 
		value + "(JRT.TYPE_RES_PEOPLE, -1)" + 
		"                 ELSE" + 
		"                  -1" + 
		"               END TYPE_RES_PEOPLE";
	}
	private String addResPeopleType(){
		return " LEFT JOIN JECN_FLOW_BASIC_INFO_T FBIT" +
		"           ON ALL_TASK.R_ID = FBIT.FLOW_ID" + 
		"         LEFT JOIN JECN_RULE_T JRT" + 
		"           ON ALL_TASK.R_ID = JRT.ID" + 
		"         LEFT JOIN JECN_FILE_T JFT" + 
		"           ON ALL_TASK.R_ID = JFT.FILE_ID";
	}

	/**
	 * 任务中相关责任人查询
	 * @param sql
	 * @return
	 */
	private String getTaskResPeoleSql(String sql){
		String resSql = "SELECT TASK_ID," +
		"       TASK_NAME," + 
		"       STATE," + 
		"       TASK_ELSE_STATE," + 
		"       TASK_TYPE," + 
		"       A.TRUE_NAME," + 
		"       CREATE_PERSON_ID," + 
		"       UPDATE_TIME," + 
		JecnCommonSql.getSqlServerCONVERT("START_TIME") +
		"       ," + 
		JecnCommonSql.getSqlServerCONVERT("END_TIME") +
		"       ," + 
		"       FROM_PERSON_ID," + 
		"       UP_STATE," + 
		"       STAGE_NAME," + 
		"       CASE" + 
		"         WHEN TYPE_RES_PEOPLE = 0 THEN" + 
		"          JU.TRUE_NAME" + 
		"         WHEN TYPE_RES_PEOPLE = 1 THEN" + 
		"          OI.FIGURE_TEXT" + 
		"       END FLOW_RES_PEOPLE" + 
		" ,publish_date" +
		"  FROM (" + sql +
		") A" +
		"  LEFT JOIN JECN_USER JU" + 
		"    ON A.RES_PEOPLE_ID = JU.PEOPLE_ID" + 
		"  LEFT JOIN JECN_FLOW_ORG_IMAGE OI" + 
		"    ON A.RES_PEOPLE_ID = OI.FIGURE_ID" + 
		" WHERE 1 = 1";

		
		return resSql;
	}
	
}
