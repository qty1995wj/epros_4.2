<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div style="padding-left: 20px; width: 100%">
			<div id="excelImport_id" style="width: 100%;">
				<s:form name="form1" action="excelImport" method="post"
					enctype="multipart/form-data">
					<table>
						<tr style="width: 100%; height: 30px;">
							<td width="30%">
								<!-- 选择需要导入的文件： -->
								<s:text name="selectFileToImport" />
								<s:file id="excelFilePath" name="upload" theme="simple"></s:file>
								<!-- 人员导入 -->
								<span style="cursor: pointer;"><img onclick="excelSubmit();return false;" src="${basePath}images/dataImport/persionImport.gif" hspace="5" border="0" align="top"/></span>
							</td>
						</tr>
						<tr>
							<td align="left" style="padding-right: 60px;height: 60px;">
							    <!-- 模板下载 -->
								<a href="userModelFileDownAction.action" ><img src="${basePath}images/dataImport/downlodePersionMode.gif" hspace="5" border="0" align="middle"/></a>
								<!-- 导出错误数据 -->
								<a href="userErrorDataFileDownAction.action" ><img src="${basePath}images/dataImport/errorDataExp.gif" hspace="5" border="0" align="middle"/></a>
								<!-- 变更数据-->
								<a href="personnelChangesFileDownAction.action" ><img src="${basePath}images/dataImport/changeData.gif" hspace="5" border="0" align="middle"/></a>
								<!-- 人员导出 -->
								<a href="downUserDataExcel.action" ><img src="${basePath}images/dataImport/persionExp.gif" hspace="5" border="0" align="middle"/></a>
							</td>
						</tr>
					</table>
					<input id="setUserFilePath" type="hidden" />
					<input name="importType" value="excel" type="hidden" />
				</s:form>
			</div>

			<div>
				<br>
				<!-- 操作说明： -->
				<s:text name="operatingInstructions" />
				<br />
				<!-- 1.请先将人员导入模板下载,然后按照模板所提供的格式填写人员岗位信息. -->
				<s:text name="downloadModeOne" />
				<br />
				<!-- 2.浏览选择填好人员岗位信息的模板,通过人员导入操作将人员信息导入到系统中. -->
				<s:text name="downloadModeTwo" />
				<br />
				<!-- 3.人员导出可以将系统中已经存在的人员岗位信息导出至本地. -->
				<s:text name="downloadModeThree" />
			</div>
		</div>
	</body>
</html>
