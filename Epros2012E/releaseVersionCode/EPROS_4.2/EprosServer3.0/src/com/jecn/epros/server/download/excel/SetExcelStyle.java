package com.jecn.epros.server.download.excel;

import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.WritableCellFormat;
import jxl.write.WriteException;

import org.apache.log4j.Logger;

/**
 * 
 * 设置excel样式通用类，仅满足制度，标准，风险清单样式下载
 * 
 * @author chehuanbo
 * @since V3.06
 */
public class SetExcelStyle {

	private static final Logger log = Logger.getLogger(SetExcelStyle.class);

	/***
	 * 设置excel样式通用方法
	 * 
	 * @author chehuanbo
	 * @param
	 * @return WritableCellFormat 设置完成后的样式对象
	 * @since V3.06
	 */
	public static WritableCellFormat setExcelCellStyle(ExcelFont excelFont){
		try {
			// 表标题样式
			WritableCellFormat wformatTitle = new WritableCellFormat(excelFont.getWritableFont());
			// 设置单元格边框样式 Border.ALl:四周 BorderLineStyle.THIN：实线
			wformatTitle.setBorder(Border.ALL, BorderLineStyle.THIN);
			// 设置单元格水平方向
			wformatTitle.setAlignment(excelFont.getAlignment());
			// 设置单元格垂直方向
			wformatTitle.setVerticalAlignment(excelFont.getvAlignment());
			// 设置单元格字体大小
			wformatTitle.setFont(excelFont.getWritableFont());
			// 设置背景颜色
			wformatTitle.setBackground(excelFont.getColour());
			return wformatTitle;
		} catch (WriteException e) {
			log.error("设置excel样式出现异常，在SetExcelStyle:setExcelCellStyle()方法中", e);
			return null;
		}
	}
}


