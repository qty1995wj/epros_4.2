package com.jecn.epros.server.download.wordxml.badafu;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.FlowDriverBean;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.download.wordxml.EnumClass.FLOW_DERVER_RULE;
import com.jecn.epros.server.util.JecnResourceUtil;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 巴德富操作说明模版
 * 
 * @author admin
 * 
 */
public class BDFProcessModel extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距 1.5倍行距 *****/
	private ParagraphLineRule vLine2 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);
	/*** 段落行距最小值12磅 *****/
	private ParagraphLineRule leastVline12 = new ParagraphLineRule(12F, LineRuleType.AT_LEAST);
	/** 评审人集合 0 评审人类型 1名称 2日期3评审阶段标识 */
	List<Object[]> peopleList = processDownloadBean.getPeopleList();
	/** 评审人集合 NEW 0 阶段标识 1评审人类型 2评审人名称 3部门名称 4更新时间 5 排序 6用户ID */
	List<Object[]> peopleListNew = processDownloadBean.getPeopleListNew();

	public BDFProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(15.48F);
		getDocProperty().setNewRowHeight(0.8F);
		getDocProperty().setFlowChartScaleValue(5.8F);
		setDocStyle("、", textTitleStyle());
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 关键活动
	 * 
	 * @param _count
	 * @param name
	 * @param keyActivityShowList
	 */
	protected void a07(ProcessFileItem processFileItem, List<KeyActivityBean> activeList) {
		if (activeList == null || activeList.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 关键活动
		String active = "活动类型";
		// 活动名称
		String title = "活动名称";
		// 重要说明
		String keyExplain = "具体内容";

		List<String[]> dataList = new ArrayList<String[]>();
		// 标题行
		dataList.add(new String[] { active, title, keyExplain });
		boolean kcpAllowShow = kcpAllowShow();
		if (activeList != null) {
			for (KeyActivityBean keyActive : activeList) {
				if (!kcpAllowShow && "4".equals(keyActive.getActiveKey())) {
					continue;
				}
				String[] rowData = new String[3];
				if ("1".equals(keyActive.getActiveKey())) {
					rowData[0] = "PA（问题区域）";// 问题区域
				} else if ("2".equals(keyActive.getActiveKey())) {
					rowData[0] = "KSF（关键成功因素）";// 关键成功因素
				} else if ("3".equals(keyActive.getActiveKey())) {
					rowData[0] = "KCP（核心控制点）";// 关键控制点
				} else if ("4".equals(keyActive.getActiveKey())) {
					rowData[0] = "KCP（核心控制点，合规）";// 关键控制点
				}
				rowData[1] = keyActive.getActivityName();
				rowData[2] = keyActive.getActivityShowControl();
				dataList.add(rowData);
			}
		}
		float[] width = new float[] { 4.71F, 3.79F, 4.85F };
		Table tbl = createTableItem(processFileItem, width, dataList);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), getDocProperty()
				.isTblTitleCrossPageBreaks());
		tbl.setCellStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow());
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, new ParagraphLineRule(20F, LineRuleType.EXACT));
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}


	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = JecnUtil.getValue("positionName");
		// 角色职责
		String roleResponsibility = JecnUtil.getValue("responsibilities");
		if (JecnContants.showPosNameBox) {
			// 标题行
			rowData.add(0, new String[] { roleName, positionName, roleResponsibility });
		} else {
			List<String[]> newDataRows = new ArrayList<String[]>();
			String[] newData = null;
			for (String[] data : rowData) {
				newData = new String[] { data[0], data[2] };
				newDataRows.add(newData);
			}
			rowData.clear();
			rowData = newDataRows;
			// 标题行
			rowData.add(0, new String[] { roleName, roleResponsibility });
		}
		float[] width = null;
		if (JecnContants.showPosNameBox) {
			width = new float[] { 1.2F, 3.5F, 3.5F, 7.5F };
		} else if (!JecnContants.showPosNameBox) {
			width = new float[] { 1.2F, 5.0F, 9.0F };
		}
		List<String[]> rowData2 = new ArrayList<String[]>();
		int rowNum = 0;
		String[] tempStr = null;
		for (String[] str : rowData) {
			tempStr = new String[str.length + 1];
			tempStr[0] = numberMark + "." + String.valueOf(rowNum);
			if (rowNum == 0) {
				tempStr[0] = "序号";
			}
			for (int i = 0; i < str.length; i++) {
				tempStr[i + 1] = str[i];
			}
			rowData2.add(tempStr);
			rowNum++;
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), getDocProperty()
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {
		createTitle(processFileItem);
		// 活动编号
		String activityNumber = JecnUtil.getValue("activitynumbers");
		// 执行角色s
		String executiveRole = JecnUtil.getValue("executiveRole");
		// 活动名称
		String eventTitle = JecnUtil.getValue("activityTitle");
		// 活动说明
		String activityDescription = "具体活动说明";
		String input = "输入";
		String output = "输出";
		float[] width = new float[] { 1.19F, 2.5F, 2.5F, 5.25F, 2F, 2.04F };
		List<Object[]> newRowData = new ArrayList<Object[]>();
		// 标题数组
		newRowData.add(new Object[] { activityNumber, executiveRole, eventTitle, activityDescription, input, output });
		// 添加数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出
		for (Object[] activeData : processDownloadBean.getAllActivityShowList()) {
			newRowData.add(new Object[] { activeData[0], activeData[1], activeData[2], activeData[3], activeData[4],
					activeData[5] });
		}
		Table tbl = createTableItem(processFileItem, width, JecnWordUtil.obj2String(newRowData));
		TableBean tableBean = new TableBean();
		tableBean.setBorder(tbl.getTableBean().getBorderBottom().getBorderWidth());
		tableBean.setTableAlign(Align.left);
		tableBean.setTableLeftInd(0F);
		tbl.setTableBean(tableBean);

		ParagraphBean titleBean = tblTitleStyle();
		titleBean.getFontBean().setFontSize(Constants.xiaowu);
		titleBean.setAlign(Align.center);

		ParagraphBean contentBean = tblContentStyle();
		contentBean.getFontBean().setFontSize(Constants.xiaowu);

		// 编号居中
		ParagraphBean bhBean = tblContentStyle();
		bhBean.getFontBean().setFontSize(Constants.xiaowu);
		bhBean.setAlign(Align.center);
		for (int i = 0; i < tbl.getRowCount(); i++) {
			if (i == 0) {
				tbl.setRowStyle(0, titleBean, getDocProperty().isTblTitleShadow(), getDocProperty()
						.isTblTitleCrossPageBreaks());
				continue;
			}
			tbl.setRowStyle(i, contentBean);
		}
		tbl.setCellStyle(0, bhBean);

	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		// KPI配置bean
		List<JecnConfigItemBean> configItemBeanList = processDownloadBean.getConfigKpiItemBean();
		createTitle(processFileItem);
		// 是否创建表格风格段落
		boolean flagTab = oldRowData.size() > 1 ? true : false;
		ParagraphBean tPbean = getDocProperty().getTextTitleStyle();

		ParagraphBean pBean = new ParagraphBean(tPbean.getFontBean());
		pBean.getFontBean().setB(false);
		if (tPbean.getParagraphIndBean() != null) {
			pBean.setInd(tPbean.getParagraphIndBean().getLeft(), tPbean.getParagraphIndBean().getRight(), tPbean
					.getParagraphIndBean().getHanging(), 0);
		}

		// 表格内容
		int count = 0;
		for (String[] row : oldRowData) { // 对应一条配置数据
			List<String[]> tableData = new ArrayList<String[]>();// 一个表格数据
			String[] rowContent = null;// 表格一行数据
			// boolean isExitKpiName = false;
			if (flagTab) {
				String number = "" + numberMark + "." + ++count;
				processFileItem.getBelongTo().createParagraph(number, pBean);
			}
			for (JecnConfigItemBean config : configItemBeanList) {
				if ("0".equals(config.getValue())) {
					continue;
				}
				/*
				 * if (!isExitKpiName) { tableData.add(new String[] { "KPI名称",
				 * row[0] }); isExitKpiName = true; }
				 */
				rowContent = new String[2];
				rowContent[0] = config.getName();
				rowContent[1] = getKpiContent(row, config.getMark());
				tableData.add(rowContent);
			}
			Table tbl = createTableItem(processFileItem, new float[] { 4.25F, 11.15F }, tableData);
			tbl.setCellStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow());

		}

	}

	/**
	 * * 0 KPI的名称 1 类型 2 目标值 3 kpi定义 4 kpi统计方法 5 数据统计时间频率 6 KIP值单位名称 7 数据获取方式 8
	 * 相关度 9 指标来源 10 数据提供者名称 11 支撑的一级指标内容 12 目的 13 测量点 14 统计周期 15 说明
	 */
	private String getKpiContent(String[] row, String mark) {
		if ("kpiName".equals(mark)) {// KPI名称
			return row[0];
		} else if ("kpiType".equals(mark)) {// KPI类型
			return row[1];
		} else if ("kpiUtilName".equals(mark)) {// KPI单位名称
			return JecnResourceUtil.getKpiVertical(Integer.valueOf(row[6]));
		} else if ("kpiTargetValue".equals(mark)) {// KPI目标值
			return row[2];
		} else if ("kpiTimeAndFrequency".equals(mark)) {// 数据统计时间频率
			return JecnResourceUtil.getKpiHorizontal(Integer.valueOf(row[5]));
		} else if ("kpiDefined".equals(mark)) {// KPI定义
			return row[3];
		} else if ("kpiDesignFormulas".equals(mark)) {// 流程KPI计算方式
			return row[4];
		} else if ("kpiDataProvider".equals(mark)) {// 数据提供者
			return row[10];
		} else if ("kpiDataAccess".equals(mark)) {// 数据获取方式
			return JecnResourceUtil.getKpiDataMethod(Integer.valueOf(row[7]));
		} else if ("kpiIdSystem".equals(mark)) {// IT 系统
			return row[16];
		} else if ("kpiSourceIndex".equals(mark)) {// 指标来源
			return JecnResourceUtil.getKpiTargetType(Integer.valueOf(row[9]));
		} else if ("kpiRrelevancy".equals(mark)) {// 相关度
			return JecnResourceUtil.getKpiRelevance(Integer.valueOf(row[8]));
		} else if ("kpiSupportLevelIndicator".equals(mark)) {// 支持的一级指标
			return row[11];
		} else if ("kpiPurpose".equals(mark)) {// 设置目的
			return row[12];
		} else if ("kpiPoint".equals(mark)) {// 测量点
			return row[13];
		} else if ("kpiPeriod".equals(mark)) {// 统计周期
			return row[14];
		} else if ("kpiExplain".equals(mark)) {// 说明
			return row[15];
		}
		return "";
	}

	/****
	 * 输入和输出
	 * 
	 * @param titleName
	 * @param inputorout
	 */
	protected void a32(ProcessFileItem processFileItem, String[] input, String[] output) {
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "输入", processDownloadBean.getFlowInput() });
		rowData.add(new String[] { "输出", processDownloadBean.getFlowOutput() });
		float[] width = new float[] { 4.75F, 8.6F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setCellStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow());
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 3.17F, 2.54F, 3.17F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getSectBean();
		titleSect.setSectBean(sectBean);
		addTitleSectContent(titleSect);
	}

	/**
	 * 添加首页内容
	 * 
	 * @param titleSect
	 */
	private void addTitleSectContent(Sect titleSect) {
		// 宋体五号
		ParagraphBean imgPBean = new ParagraphBean("宋体", Constants.wuhao, false);
		imgPBean.setSpace(0F, 0F, vLine1);
		// logo图标
		Image logo = new Image(getDocProperty().getLogoImage());
		PositionBean positionBean = new PositionBean(PositionType.absolute, 3.75F, 0, -0.75F, 0);
		logo.setSize(4.99F, 1.8F);
		logo.setPositionBean(positionBean);
		Paragraph logpP = titleSect.createParagraph("", imgPBean);
		logpP.appendGraphRun(logo);

		// 换行
		for (int i = 0; i < 8; i++) {
			titleSect.createParagraph("", imgPBean);
		}
		// 流程名称 宋体 小一 加粗
		ParagraphBean flowNamePBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoyi, true);
		flowNamePBean.setSpace(0F, 0F, vLine1);
		titleSect.createParagraph(processDownloadBean.getFlowName(), flowNamePBean);

		// 换行
		for (int i = 0; i < 12; i++) {
			titleSect.createParagraph("", imgPBean);
		}

		/***** 默认表格样式 **********/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		/**** 表格默认段落样式 宋体小三 居中 ************/
		ParagraphBean tabPBean = new ParagraphBean(Align.center, "宋体", Constants.xiaosan, false);
		tabPBean.setSpace(0f, 0f, vLine1);
		List<String[]> rowData = new ArrayList<String[]>();
		// 流程属性中流程拟制人
		String flowDrafter = processDownloadBean.getResponFlow().getFictionPeopleName();
		// 对应的是自定义审批人2 12
		String flowOrgName = "";
		// 批准人审核 4
		String approve = "";
		if (peopleListNew != null && peopleListNew.size() != 0) {
			for (Object[] obj : peopleListNew) {
				/*
				 * if ("0".equals(String.valueOf(obj[0]))) { // flowDrafter =
				 * String.valueOf(obj[2]); } else
				 */if ("12".equals(String.valueOf(obj[0]))) {
					flowOrgName += String.valueOf(obj[2]) + "/";
				} else if ("4".equals(String.valueOf(obj[0]))) {
					approve = String.valueOf(obj[2]);
				}
			}
		}
		if ((peopleListNew == null || peopleListNew.size() == 0) && peopleList != null && peopleList.size() != 0) {
			for (Object[] obj : peopleList) {
				/*
				 * if ("0".equals(String.valueOf(obj[3]))) { // flowDrafter =
				 * String.valueOf(obj[1]); } else
				 */if ("12".equals(String.valueOf(obj[3]))) {
					flowOrgName += String.valueOf(obj[1]) + "/";
				} else if ("4".equals(String.valueOf(obj[3]))) {
					approve = String.valueOf(obj[1]);
				}
			}
		}

		if (flowOrgName.endsWith("/")) {
			flowOrgName = flowOrgName.substring(0, flowOrgName.length() - 1);
		}

		rowData.add(new String[] { "编  制", flowDrafter });
		rowData.add(new String[] { "审  核", flowOrgName });
		rowData.add(new String[] { "批  准", approve });
		JecnWordUtil.createTab(titleSect, tabBean, new float[] { 3.13F, 4.17F }, rowData, tabPBean);
		// 换行
		for (int i = 0; i < 21; i++) {
			titleSect.createParagraph("", imgPBean);
		}
		// 横线
		LineGraph line = new LineGraph(LineStyle.single, 1.5F);
		line.setFrom(0F, 0F);
		line.setTo(425F, 0F);
		titleSect.createParagraph(line, imgPBean);

		// 公司名称 宋体 五号 居中
		ParagraphBean compantNamePBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		titleSect.createParagraph(processDownloadBean.getCompanyName(), compantNamePBean);
	}

	/**
	 * 文控表格(返回文控表格高度)
	 * 
	 * @param firstSect
	 */
	private void addTextcontrolTab(Sect firstSect) {
		// 　存储过滤重复人员数据后的结果（累加部门）
		// 存储新结果，过滤掉阶段标识stageMark和peopleId 一样的重复的数据 同时清空部门数据
		List<Object[]> finalPeopleListNew = new ArrayList<Object[]>();
		for (Object[] objects : peopleListNew) {
			if (notExits(objects, finalPeopleListNew)) {
				// 清空部门信息
				Object[] obj = objects.clone();
				obj[3] = "";
				finalPeopleListNew.add(obj);
			}
		}
		// 循环便利原始数据和过滤之后的数据 追加部门
		for (Object[] objects : peopleListNew) {
			for (Object[] finalObj : finalPeopleListNew) {
				// 如果finalObj中 （stageMark 、 peopleId）字段值 和objects对象的（stageMark
				// 、peopleId）字段值 相等 则把 objects 的部门追加到 finalObj部门中
				if (finalObj[0].toString().equals(objects[0].toString())
						&& finalObj[6].toString().equals(objects[6].toString())) {
					if (objects[7] == null) {
						continue;
					}
					finalObj[3] = finalObj[3].toString().equals("") ? objects[7].toString() : finalObj[3].toString()
							+ "/" + objects[7].toString();
				}
			}
		}
		// 流程编号
		String fileCode = processDownloadBean.getFlowInputNum();
		// 流程版本
		String version = processDownloadBean.getFlowVersion();
		// 流程名称
		String fileName = processDownloadBean.getFlowName();
		// 密集
		String isPub = getPubOrSec();
		// 生效日期 ==发布日期
		String effectiveDate = processDownloadBean.getReleaseDate().replaceAll("\\w+\\d+\\:\\d+.+", "");
		// 表格
		Table tab = null;
		/***** 默认表格样式 **********/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		/**** 表格默认段落样式 宋体小居中 ************/
		ParagraphBean tabPBean = new ParagraphBean(Align.center, "宋体", Constants.xiaosi, false);
		tabPBean.setSpace(0f, 0f, vLine1);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "文控管理" });
		rowData.add(new String[] { "文件属性", "文件编码", fileCode, "版本", version });
		rowData.add(new String[] { "", "文件名称", fileName, "密级", isPub });

		// 动态添加审批环节 记录审批环节个数
		int shenheCount = 0;// 直接发布
		int rwshenheCount = 0;// 走任务
		/**** 添加评审人数据集合 此处分为两种 审批和直接发布 ******/
		if (finalPeopleListNew != null && finalPeopleListNew.size() != 0) {
			// 评审人集合 NEW 0 阶段标识 1评审人类型 2评审人名称 3部门名称 4更新时间 5 排序 6用户ID
			// 批准数据
			List<String[]> pzRowdata = new ArrayList<String[]>();
			for (Object[] obj : finalPeopleListNew) {
				if ("0".equals(String.valueOf(obj[0]))) {
					// 拟稿人 ==》流程属性中流程拟制人
					String flowDrafter = processDownloadBean.getResponFlow().getFictionPeopleName();
					String postName = processDownloadBean.getResponFlow().getFictionPeoplePosName();
					// rowData.add(new String[] { "审批控制",
					// "编制",String.valueOf(obj[2]), "岗位", String.valueOf(obj[3])
					// });
					rowData.add(new String[] { "审批控制", "编制", flowDrafter, "岗位", postName });
				}
				// 批准
				else if ("4".equals(String.valueOf(obj[0]))) {
					pzRowdata.add(new String[] { "", "批准", String.valueOf(obj[2]), "岗位", String.valueOf(obj[3]) });
				}
				// 审核
				else {
					rowData.add(new String[] { "", "审核", String.valueOf(obj[2]), "岗位", String.valueOf(obj[3]) });
					rwshenheCount++;
				}
			}
			rowData.addAll(pzRowdata);
		} else if (peopleList != null && peopleList.size() != 0) {
			// 评审人集合 0 评审人类型 1名称 2日期3评审阶段标识
			// [[拟稿人, 拟稿人, 2015-10-13 18:54:29, 0],
			// [文控审核人, 文控审核人, , 1],
			// [部门审核人, 部门审核人, , 2],
			// [评审人会审, 评审人, , 3],
			// [各业务体系负责人, 业务体系负责人, , 6],
			// [IT总监, IT中间, , 7],
			// [事业部经理, 事业部经理, , 8],
			// [总经理, 总经理, , 9],
			// [批准人审核, 批准人, , 4], null]

			// 批准数据
			List<String[]> pzRowdata = new ArrayList<String[]>();
			for (Object[] obj : peopleList) {
				if ("0".equals(String.valueOf(obj[3]))) {
					// 拟稿人 ==》流程属性中流程拟制人
					String flowDrafter = processDownloadBean.getResponFlow().getFictionPeopleName();
					String postName = processDownloadBean.getResponFlow().getFictionPeoplePosName();
					// rowData.add(new String[] { "审批控制", "编制",
					// String.valueOf(obj[1]), "岗位", "" });
					rowData.add(new String[] { "审批控制", "编制", flowDrafter, "岗位", postName });
				}
				// 批准
				else if ("4".equals(String.valueOf(obj[3]))) {
					pzRowdata.add(new String[] { "", "批准", String.valueOf(obj[1]), "岗位", "" });
				}
				// 审核
				else {
					rowData.add(new String[] { "", "审核", String.valueOf(obj[1]), "岗位", "" });
					shenheCount++;
				}
			}
			rowData.addAll(pzRowdata);
		}
		// 解释岗位 流程责任人岗位名称
		String explainJob = processDownloadBean.getResponFlow().getDutuUserPosName();
		rowData.add(new String[] { "发布管理", "生效日期", effectiveDate });
		rowData.add(new String[] { "", "解释岗位", explainJob });
		tab = JecnWordUtil.createTab(firstSect, tabBean, new float[] { 2.24F, 2.75F, 4.25F, 1.26F, 3.64F }, rowData,
				tabPBean);

		// 文控管理占5列
		tab.getRow(0).getCell(0).setColspan(5);
		tab.getRow(0).getCell(0).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.center, "宋体", Constants.xiaosan, true));
		// 文件属性占2行
		tab.getRow(1).getCell(0).setRowspan(2);
		// 审批控制跨行列
		if (finalPeopleListNew != null && finalPeopleListNew.size() != 0) {
			// 审批控制跨行（审批环节总数）
			tab.getRow(3).getCell(0).setRowspan(finalPeopleListNew.size());
			tab.getRow(4).getCell(1).setRowspan(rwshenheCount);
		} else if (peopleList != null && peopleList.size() != 0) {
			// 审批控制跨行（审批环节总数）
			tab.getRow(3).getCell(0).setRowspan(peopleList.size());
			tab.getRow(4).getCell(1).setRowspan(shenheCount);
		}
		int tabRowCount = tab.getRowCount();
		// 发布管理 占2行
		tab.getRow(tabRowCount - 2).getCell(0).setRowspan(2);
		// 生效日期 占3列
		tab.getRow(tabRowCount - 2).getCell(2).setColspan(3);
		// 解释岗位占3列
		tab.getRow(tabRowCount - 1).getCell(2).setColspan(3);

		// 设置全部垂直居中
		for (TableRow row : tab.getRows()) {
			row.setHeight(1F);
			for (TableCell cell : row.getCells()) {
				cell.setvAlign(Valign.center);
			}
		}
		tab.getRow(0).setHeight(0.74F);
	}

	/**
	 * 文件修订履历
	 * 
	 * @param firstSect
	 */
	private void addFileRevisedTab(Sect firstSect) {
		/** 流程文控信息 0版本号 1变更说明 2发布日期 3流程拟制人 4审批人 */
		List<Object[]> documentList = processDownloadBean.getDocumentList();
		// 表格
		Table tab = null;
		/***** 默认表格样式 **********/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		/**** 表格默认段落样式 宋体小居中 ************/
		ParagraphBean tabPBean = new ParagraphBean(Align.center, "宋体", Constants.xiaosi, false);
		tabPBean.setSpace(0f, 0f, vLine1);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "文件修订履历" });
		rowData.add(new String[] { "序号", "版本", "修订内容", "修订人", "修订日期" });
		// 添加文控发布记录
		if (documentList != null) {
			int i = 0;
			for (Object[] obj : documentList) {
				String version = String.valueOf(obj[0]);
				String content = String.valueOf(obj[1]);
				String revisePeople = String.valueOf(obj[3]);
				String reviseData = String.valueOf(obj[2]).replaceAll("\\w+\\d+\\:\\d+.+", "").replaceAll("\\-", "/");
				rowData.add(new String[] { ++i + "", version, content, revisePeople, reviseData });
			}
		}
		tab = JecnWordUtil.createTab(firstSect, tabBean, new float[] { 1.69F, 1.75F, 6.75F, 2F, 2.73F }, rowData,
				tabPBean);
		// 文件修订履历占5列
		tab.getRow(0).getCell(0).setColspan(5);
		tab.getRow(0).getCell(0).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.center, "宋体", Constants.xiaosi, true));
		// 设置第一行加粗
		List<TableCell> tc_b = tab.getRow(1).getCells();
		for (TableCell tc : tc_b) {
			tc.getParagraph(0).setParagraphBean(new ParagraphBean(Align.center, "宋体", Constants.xiaosi, true));
		}
		// 设置全部垂直居中
		for (TableRow row : tab.getRows()) {
			row.setHeight(1F);
			for (TableCell cell : row.getCells()) {
				cell.setvAlign(Valign.center);
			}
		}
	}

	/**
	 * 流程图前面的数据
	 */
	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		// 添加文控表格 计算表格所占高度
		addTextcontrolTab(firstSect);
		// 添加分页符号
		firstSect.addParagraph(new Paragraph(true));
		// 添加文件修订履历表格 计算表格所占高度
		addFileRevisedTab(firstSect);
		// 添加分页符号
		// firstSect.addParagraph(new Paragraph(true));
		// 流程名称
		// ParagraphBean pBean = new ParagraphBean(Align.center, "宋体",
		// Constants.sihao, true);
		// firstSect.createParagraph(processDownloadBean.getFlowName(), pBean);
		firstSect.createParagraph("");
		// 添加页眉
		createCommhdr(firstSect);

	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setSize(29.7F, 21F);
		flowChartSect.setSectBean(sectBean);
		// 添加页眉
		createCommhdr(flowChartSect);

	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		// 添加页眉
		createCommhdr(secondSect);
	}

	/**
	 * 表格内容段落样式 宋体 五号 最小值12磅值
	 */
	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.both, "宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, leastVline12);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.94 厘米
		tblStyle.setTableLeftInd(0F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

	/**
	 * 表格标题样式 宋体 五号 最小值12磅值
	 */
	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitleStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		tblTitleStyle.setSpace(0f, 0f, leastVline12);
		return tblTitleStyle;
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.right, "宋体", Constants.xiaowu, false);
		pBean.setpStyle(PStyle.AF1);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		header.createParagraph(processDownloadBean.getFlowName(), pBean);
	}

	/**
	 * 标题内容属性
	 */
	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体", Constants.xiaosi, false);
		textContentStyle.setInd(-0.5F, 0, 0, 0.75F);
		textContentStyle.setSpace(0f, 0f, vLine2);
		return textContentStyle;
	}

	/**
	 * 标题样式 宋体 小四 加粗 1.5倍数行距
	 */
	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("宋体", Constants.xiaosi, true);
		textTitleStyle.setSpace(0f, 0f, vLine2);
		// 悬挂缩进默认0.74 会导致序号 超过10了增加两倍故设置0.9
		textTitleStyle.setInd(-0.5F, 0, 0.9F, 0);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	/**
	 * 判断是否存在此审批数据（依据为数据的stageMark一致,peopleId一致)
	 * 
	 * @param objects
	 * @param finalPeopleListNew
	 * @return true不存在 false 存在
	 */
	private boolean notExits(Object[] objects, List<Object[]> finalPeopleListNew) {
		if (finalPeopleListNew.size() == 0) {
			return true;
		}
		// 便利finalPeopleListNew
		for (Object[] finalObj : finalPeopleListNew) {
			// 如果传入对象的 （stageMark 、 peopleId） 和集合中某一个对象的（stageMark 、 peopleId）
			// 相等 表示存在返回false
			if (finalObj[0].toString().equals(objects[0].toString())
					&& finalObj[6].toString().equals(objects[6].toString())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 相关标准
	 * 
	 * @param _count
	 * @param name
	 * @param standardList
	 */
	@Override
	protected void a21(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		List<String[]> rows = new ArrayList<String[]>();
		rows.add(0, new String[] { "文件名称" });
		for (String[] row : rowData) {
			rows.add(new String[] { row[0] });
		}
		createTitle(processFileItem);
		float[] width = new float[] { 17.5F };
		Table tbl = createTableItem(processFileItem, width, rows);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}
}
