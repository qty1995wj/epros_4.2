package epros.draw.gui.figure.shape.part;

import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.util.DrawCommon;

/**
 * 石勘院 角色元素
 * 
 *@author ZXH
 * @date 2016-8-27上午11:30:22
 */
public class RoleFigureAR extends RoleFigure {

	public RoleFigureAR(JecnFigureData figureData) {
		super(figureData);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	protected void paintRectTop(Graphics2D g2d, int shadowCount) {
		// 顶层 原图，榜值均为零
		g2d.fillRect(x, y, userWidth - shadowCount, userHeight - shadowCount);

		// 无填充
		paintNoneFigure(g2d);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawRect(x, y, userWidth, userHeight);

		// 设置虚线画笔
		g2d.setStroke(DrawCommon.getDottedLineStorke());

		g2d.drawLine(5, 0, 5, this.getHeight());
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	protected void paint3DPartTop(Graphics2D g2d) {
		g2d.fillRect(x + indent3Dvalue, y + indent3Dvalue, userWidth - indent3Dvalue - indent3Dvalue, userHeight
				- indent3Dvalue - indent3Dvalue);

		g2d.setColor(this.getFlowElementData().getBodyColor());
		// 设置虚线画笔
		g2d.setStroke(DrawCommon.getDottedLineStorke());

		g2d.drawLine(5, 0, 5, this.getHeight());
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	protected void paint3DAndShadowsPartTop(Graphics2D g2d) {
		g2d.fillRect(x + indent3Dvalue, y + indent3Dvalue, userWidth - 3 * indent3Dvalue, userHeight - 3
				* indent3Dvalue);
		g2d.setColor(this.getFlowElementData().getBodyColor());
		// 设置虚线画笔
		g2d.setStroke(DrawCommon.getDottedLineStorke());

		g2d.drawLine(5, 0, 5, this.getHeight());
	}
}
