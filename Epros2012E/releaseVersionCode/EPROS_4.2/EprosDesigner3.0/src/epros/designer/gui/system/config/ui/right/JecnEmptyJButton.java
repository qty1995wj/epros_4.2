package epros.designer.gui.system.config.ui.right;

import java.awt.event.ActionEvent;
import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;

import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;

/**
 * 
 * 非必填按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnEmptyJButton extends JecnAbstractBaseJButton {

	public JecnEmptyJButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// 获取当前选中的属性面板
		JecnAbstractPropertyBasePanel selectedPropContPanel = dialog
				.getSelectedPropContPanel();
		if (selectedPropContPanel == null
				|| selectedPropContPanel.getTableScrollPane() == null) {
			return;
		}

		// 获取选中序号集合
		int[] selectedRows = selectedPropContPanel.getTableScrollPane()
				.getSelectedRows();
		// 获取选中项
		List<JecnConfigItemBean> selectedItemBeanList = selectedPropContPanel
				.getTableScrollPane().getSelectedItemBeanList();

		if (selectedItemBeanList == null || selectedItemBeanList.size() == 0) {
			return;
		}

		// 查找到必填选中数据，修改成非必填
		for (JecnConfigItemBean itemBean : selectedItemBeanList) {
			// 必填改成非必填
			itemBean.setIsEmpty(JecnConfigContents.ITEM_EMPTY);
		}

		// 重新加载
		dialog.getPropertyPanel().reShowProperty();

		// 根据给定的序号添加选中行,如果选中行超过界限选中边界行
		selectedPropContPanel.getTableScrollPane().setSelectedRowToTable(
				selectedRows);
	}
}
