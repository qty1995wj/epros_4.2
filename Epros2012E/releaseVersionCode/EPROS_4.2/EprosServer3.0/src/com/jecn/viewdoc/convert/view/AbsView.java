package com.jecn.viewdoc.convert.view;

import java.io.File;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.standard.IStandardService;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.viewdoc.bean.ViewDocContentIdCache;
import com.jecn.viewdoc.convert.ViewDocsManager;
import com.jecn.viewdoc.convert.ViewDocsPathManager;
import com.jecn.viewdoc.service.IViewDocService;

/**
 * 制度、文件、流程（文件类型的）预览抽象类
 * 
 * @author hyl
 * 
 */
public abstract class AbsView implements View {
	protected static final Logger log = Logger.getLogger(View.class);
	/** type=0和1时为文件ID，2和3时为制度ID */
	protected Long relatedId;
	protected Map<String, String> fileInfo;
	protected IFileService fileService;
	protected IRuleService ruleService;
	protected IStandardService standardService;
	protected IFlowStructureService flowStructureService;
	protected IViewDocService viewDocService;
	/**
	 * 在线预览类型：文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3 ;
	 * 4、流程（文件）已发布，5：流程（文件）未发布;6 :标准附件; 7:流程图/架构图图片 已发布; 8:流程图/架构图图片 未发布
	 * 9：流程文件word 已发布;10：流程文件word 未发布
	 **/
	protected int type;
	/** true: 已发布 */
	protected boolean isPub;
	/** true：手机端 */
	protected boolean isApp;
	protected HttpServletRequest request;
	protected HttpServletResponse response;

	protected ViewInfo viewInfo;

	public AbsView(ViewInfo viewInfo) {
		this.fileService = viewInfo.getFileService();
		this.ruleService = viewInfo.getRuleService();
		this.standardService = viewInfo.getStandardService();
		this.flowStructureService = viewInfo.getFlowStructureService();
		this.type = viewInfo.getType();
		this.isPub = viewInfo.isPub();
		this.relatedId = viewInfo.getRelatedId();
		this.viewDocService = viewInfo.getViewDocService();
		this.isApp = viewInfo.isApp();
		this.request = viewInfo.getRequest();
		this.response = viewInfo.getResponse();
		this.viewInfo = viewInfo;
		try {
			fileInfo = getNameAndContentMaps();
		} catch (Exception e) {
			throw new ViewException(getDataError(), e);
		}
	}

	private String getDataError() {
		String result = "AbsView() 预览获取数据异常：" + toString();
		return result;
	}

	@Override
	public String toString() {
		return "AbsView [isApp=" + isApp + ", isPub=" + isPub + ", relatedId=" + relatedId + ", type=" + type + "]";
	}

	protected boolean isExists(ViewDocContentIdCache currentCache) throws Exception {
		File file = new File(getAbsoluteCachePath(currentCache.getCachePath()));
		return file.exists();
	}

	protected String getAbsoluteCachePath(String cachePath) {
		return JecnPath.APP_PATH + cachePath.substring(ViewDocsPathManager.RELAVTIVE_PATH.length(), cachePath.length());
	}

	protected String assembleFileNameAndDownloadURL(String file) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("?name=");
		try {
			buffer.append(URLEncoder.encode(fileInfo.get("FileName"), "UTF-8").replace("+", "%20"));

			buffer.append("&file=" + file);
			buffer.append("&");

			buffer.append("downloadURL=");
			/** 文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3 */
			buffer.append(ViewUtils.getDownloadFileURL(viewInfo));
		} catch (Exception e) {
			log.error("转码异常", e);
		}
		return buffer.toString();
	}

	protected ViewDocContentIdCache createCache() throws Exception {
		ViewDocContentIdCache cache = new ViewDocContentIdCache();
		cache.setRelatedId(relatedId);
		cache.setType(type);

		FileOpenBean fileOpenBean = getFileOpenBean();
		cache.setCachePath(getViewDocPath(fileOpenBean));
		cache.setContentId(fileOpenBean.getId());
		return cache;
	}

	protected String getViewDocPath(FileOpenBean fileOpenBean) throws Exception {
		String viewDocPath = null;
		// 在线预览类型：文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3 ;
		// * 4、流程（文件）已发布，5：流程（文件）未发布;6 :标准附件; 7:流程图/架构图图片 已发布; 8:流程图/架构图图片 未发布
		// * 9：流程文件word 已发布;10：流程文件word 未发布
		switch (type) {
		case 0:
		case 1:
			viewDocPath = ViewDocsManager.buildViewDoc(fileOpenBean.getFileByte(), fileOpenBean.getName(), viewInfo
					.isPub() ? "FILE" : "FILE_T");
			break;
		case 2:
		case 3:
		case 11:
		case 12:
			viewDocPath = ViewDocsManager.buildViewDoc(fileOpenBean.getFileByte(), fileOpenBean.getName(), viewInfo
					.isPub() ? "RULE" : "RULE_T");
			break;
		case 4:
		case 5:
			viewDocPath = ViewDocsManager.buildViewDoc(fileOpenBean.getFileByte(), fileOpenBean.getName(),
					"PROCESS_FILE");
			break;
		case 6:
			viewDocPath = ViewDocsManager.buildViewDoc(fileOpenBean.getFileByte(), fileOpenBean.getName(),
					"STANDARD_FILE");
			break;

		case 7:// 流程图
		case 8:
		case 9:// 流程文件
		case 10:
			viewDocPath = ViewDocsManager.buildViewDoc(fileOpenBean.getFileByte(), fileOpenBean.getName(), "PROCESS");
			break;
		default:
			break;
		}
		return viewDocPath;
	}

	/**
	 * 获取文件名称和文件内容主键ID
	 * 
	 * @param cache
	 * @param fileInfo
	 * @throws Exception
	 */
	protected FileOpenBean getFileOpenBean() throws Exception {
		FileOpenBean fileOpenBean = null;
		// 在线预览类型：文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3 ;
		// * 4、流程（文件）已发布，5：流程（文件）未发布;6 :标准附件; 7:流程图/架构图图片 已发布; 8:流程图/架构图图片 未发布
		// * 9：流程文件word 已发布;10：流程文件word 未发布
		switch (type) {
		case 0:
		case 1:
			fileOpenBean = fileService.openVersion(null, Long.valueOf(fileInfo.get("ContentId")));
			break;
		case 2:
		case 3:
			fileOpenBean = ruleService.g020OpenRuleFile(Long.valueOf(fileInfo.get("ContentId")));
			break;
		case 11:
		case 12:
			fileOpenBean = new FileOpenBean();
			try {
				JecnCreateDoc createDoc = ruleService.downloadRuleFile(relatedId, isPub);
				fileOpenBean.setName(getFileName());
				fileOpenBean.setFileByte(createDoc.getBytes());
				fileOpenBean.setId(relatedId);
			} catch (Exception e) {
				log.error("获取流程操作说明字节流错误！", e);
			}
			break;
		case 4:
		case 5:
			fileOpenBean = flowStructureService.openFileContent(Long.valueOf(fileInfo.get("ContentId")));
			break;
		case 6:
			fileOpenBean = standardService.openFileContent(Long.valueOf(fileInfo.get("ContentId")));
			break;
		case 7:
		case 8:// 流程图片
			fileOpenBean = getProcessImageFile();
			break;
		case 9:
		case 10:// 流程文件
			fileOpenBean = new FileOpenBean();
			try {
				JecnCreateDoc createDoc = viewInfo.getWebProcessService().printFlowDoc(relatedId, TreeNodeType.process,
						isPub, viewInfo.getUserId(), viewInfo.gethId());
				fileOpenBean.setName(getFileName());
				fileOpenBean.setFileByte(createDoc.getBytes());
				fileOpenBean.setId(relatedId);
			} catch (Exception e) {
				log.error("获取流程操作说明字节流错误！", e);
			}
			break;
		default:
			break;
		}

		return fileOpenBean;
	}

	private Map<String, String> getNameAndContentMaps() throws Exception {
		if (relatedId == null) {
			throw new IllegalArgumentException("在线预览获取参数非法！type = " + type + "relatedId=" + relatedId + "isPub="
					+ isPub);
		}
		Map<String, String> fileInfo = new HashMap<String, String>();
		fileInfo.put("FileName", viewInfo.getRelatedName());
		fileInfo.put("ContentId", String.valueOf(relatedId));
		return fileInfo;
	}

	private FileOpenBean getProcessImageFile() throws Exception {
		FileOpenBean fileOpenBean = new FileOpenBean();
		String mapIconPath = null;
		if (isPub) {
			JecnFlowStructure flowStructureT = flowStructureService.findJecnFlowStructureById(relatedId);
			mapIconPath = flowStructureT.getFilePath();
		} else {
			JecnFlowStructureT flowStructureT = flowStructureService.get(relatedId);
			mapIconPath = flowStructureT.getFilePath();
		}
		fileOpenBean.setName(viewInfo.getRelatedName());
		fileOpenBean.setId(relatedId);
		File file = new File(JecnContants.jecn_path + mapIconPath);
		if (!file.exists() || !file.isFile()) {// 判断文件是否是标准文件，是否存在
			log.info("不是文件或文件不存在。path=" + JecnContants.jecn_path + mapIconPath);
			return null;
		}
		fileOpenBean.setFileByte(JecnUtil.getByte(file));
		return fileOpenBean;
	}

	protected boolean isNewestCache(Long curContentId, Long cacheContentId) {
		return curContentId.longValue() == cacheContentId;
	}

	protected boolean checkFileViewable(String fileName) {
		String extension = FilenameUtils.getExtension(fileName);
		return allowViewExtension(extension);
	}

	public boolean allowViewExtension(String extension) {
		return allowViewExtensions().contains(extension.toLowerCase());
	}

	public boolean allowViewExtension() {
		return checkFileViewable(getFileName());
	}

	public String getFileName() {
		return fileInfo.get("FileName");
	}

}
