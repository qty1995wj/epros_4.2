package com.jecn.epros.domain.system;

import com.jecn.epros.domain.process.ProcessBaseInfoBean;

import java.util.List;

public class ProcessCheckBean {

    private int total = 0;
    private List<ProcessBaseInfoBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ProcessBaseInfoBean> getData() {
        return data;
    }

    public void setData(List<ProcessBaseInfoBean> data) {
        this.data = data;
    }

}
