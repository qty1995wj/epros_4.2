package epros.draw.util;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import epros.draw.gui.figure.shape.FreeText;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.system.data.JecnUserCheckInfoData;

/**
 * 
 * 
 * 用户输入校验工具类
 * 
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUserCheckUtil {

	/**
	 * 
	 * 校验名称是否满足要求,验证（名称不为空，字符正确，长度）成功返回“”，否则返回相应提示信息
	 * 
	 * @param name
	 *            String 名称
	 * @return String 验证（名称不为空，字符正确，长度）成功返回""，否则返回相应提示信息
	 */
	public static String checkName(String name) {
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			return JecnUserCheckInfoData.getNameNotNull();
		} else if (!checkNameFileSpecialChar(name)) {
			// 名称不能包括任意字符之一: \ / : * ? < > | # %
			return JecnUserCheckInfoData.getNameErrorCharInfo();
		} else if (getTextLength(name) > 122) {
			// 不能超过122个字符或61个汉字
			return JecnUserCheckInfoData.getNameLengthInfo();
		}
		return "";
	}

	/**
	 * 
	 * 校验名称是否满足要求,验证（名称不为空，字符正确，长度）成功返回“”，否则返回相应提示信息
	 * 
	 * @param name
	 *            String 名称
	 * @return String 验证（名称不为空，字符正确，长度）成功返回""，否则返回相应提示信息
	 */
	public static String checkNullName(String name) {
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			return JecnUserCheckInfoData.getNameNotNull();
		} else if (getTextLength(name) > 122) {
			// 不能超过122个字符或61个汉字
			return JecnUserCheckInfoData.getNameLengthInfo();
		}
		return "";
	}

	/**
	 * 输入内容允许为空 (长度是否在规定范围，是否包含特殊字符)成功返回""，否则返回相应提示信息
	 * 
	 * @param name
	 *            String 名称
	 * @return String 验证（字符正确，长度）成功返回""，否则返回相应提示信息
	 */
	public static String checkNameContainsNull(String name) {
		if (getTextLength(name) > 122) {
			// 不能超过122个字符或61个汉字
			return JecnUserCheckInfoData.getNameLengthInfo();
		}
		return "";
	}

	/**
	 * 
	 * 校验名称是否满足要求,验证（名称不为空，字符正确，长度）成功返回“”，否则返回相应提示信息
	 * 
	 * @param name
	 *            String 名称
	 * @return String 验证（名称不为空，字符正确，长度）成功返回""，否则返回相应提示信息
	 */
	public static String checkAreaName(String name) {
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			return JecnUserCheckInfoData.getNameNotNull();
		} else if (getTextLength(name) > 1200) {
			// 不能超过1200个字符或600个汉字
			return JecnUserCheckInfoData.getNoteLengthInfo();
		}
		return "";
	}

	/**
	 * 
	 * 校验名称是否满足要求,验证（名称不为空，字符正确，长度）成功返回“”，否则返回相应提示信息
	 * 
	 * @param name
	 *            String 名称
	 * @return String 验证（名称不为空，字符正确，长度）成功返回""，否则返回相应提示信息
	 */
	public static String checkAreaNote(String name) {

		if (getTextLength(name) > 1200) {
			// 不能超过1200个字符或600个汉字
			return JecnUserCheckInfoData.getNoteLengthInfo();
		}
		return "";
	}

	/**
	 * 
	 * 判断给定字符串长度是否大于给定长度
	 * 
	 * @param name
	 *            字符串
	 * @param length
	 *            长度
	 * @return boolean true：不大于；false：大于
	 */
	public static boolean checkLength(String name, int length) {
		if (getTextLength(name) > length) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 判断给定参数是否超过122个字符或61个汉字
	 * 
	 * @param name
	 *            String
	 * @return String 验证成功返回"",验证失败返回提示信息
	 */
	public static String checkNameLength(String name) {
		if (getTextLength(name) > 122) {
			// 不能超过122个字符或61个汉字
			return JecnUserCheckInfoData.getNameLengthInfo();
		}
		return "";
	}

	/**
	 * 
	 * 判断给定参数是否超过1200个字符或600个汉字
	 * 
	 * @param note
	 *            String
	 * @return String 验证成功返回"",验证失败返回提示信息
	 */
	public static String checkNoteLength(String note) {
		if (getTextLength(note) > 1200) {
			// 不能超过1200个字符或600个汉字
			return JecnUserCheckInfoData.getNoteLengthInfo();
		}
		return "";
	}

	/**
	 * 
	 * 判断名称是否合法
	 * 
	 * @param name
	 *            String
	 * @return boolean 含有
	 */
	public static boolean checkNameFileSpecialChar(String name) {
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			return false;
		} else {
			// 注册匹配模式(windows系统新建文件时不支持的特殊字符)
			Pattern pattern = Pattern.compile(JecnUserCheckInfoData.matchSpecialChar);
			// 创建匹配给定参数与此模式的匹配器
			Matcher matcher = pattern.matcher(name);
			// 开始匹配
			return matcher.matches();
		}
	}

	/**
	 * 
	 * 获取给定字符串的字节长度，汉字为2个字节，字母数字等为一个字节
	 * 
	 * 字符串为null或""时返回0
	 * 
	 * @param text
	 *            字符串
	 * @return int 返回字符串的字节长度
	 */
	public static int getTextLength(String text) {
		try {
			return (text != null) ? text.getBytes("GBK").length : 0;
		} catch (UnsupportedEncodingException e) {
			return (text != null) ? text.getBytes().length : 0;
		}
	}

	/**
	 * 
	 * 校验给定字符串是否满足输入最大长度
	 * 
	 * @param flowElementPanel
	 *            JecnBaseFlowElementPanel 流程元素
	 * @param name
	 *            String 需要校验长度的字符串
	 * @return String 提示信息
	 */
	public static String checkNameMaxLength(JecnBaseFlowElementPanel flowElementPanel, String name) {
		if (flowElementPanel == null || flowElementPanel instanceof FreeText) {
			return "";
		}
		if (flowElementPanel instanceof CommentText) {// 注释框、自由文本框
			return JecnUserCheckUtil.checkNoteLength(name);
		}

		return JecnUserCheckUtil.checkNameLength(name);
	}

	/**
	 * 
	 * 校验文件名称是否满足要求,验证（名称不为空，字符正确）成功返回“”，否则返回相应提示信息
	 * 
	 * @param name
	 *            String 名称
	 * @return String 验证（名称不为空，字符正确，长度）成功返回""，否则返回相应提示信息
	 */
	public static String checkFileName(String name) {
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			return JecnUserCheckInfoData.getNameNotNull();
		} else if (!checkNameFileSpecialChar(name)) {
			// 名称不能包括任意字符之一: \ / : * ? < > | # %
			return JecnUserCheckInfoData.getNameErrorCharInfo();
		} else if (name.length() > 210) {
			// 名称长度不能超过121
			return JecnUserCheckInfoData.getFileLengthInfo();
		}

		return "";
	}

	/**
	 * 
	 * 校验邮箱格式
	 * 
	 * @param mailAddr
	 *            String
	 * @return boolean 验证成功：true；验证失败：false
	 */
	public static boolean checkMailFormat(String mailAddr) {
		if (DrawCommon.isNullOrEmtry(mailAddr)) {
			return false;
		}
		// 邮箱格式不正确
		Pattern emailer = Pattern.compile(JecnUserCheckInfoData.EMAIL_FORMAT);
		if (!emailer.matcher(mailAddr).matches()) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 校验数字格式
	 * 
	 * @return boolean 验证成功：true；验证失败：false
	 */
	public static boolean checkNumberFormat(String num) {
		String reg = "^[0-9]+(.[0-9]+)?$";
		Pattern pattern = Pattern.compile(reg);
		// 创建匹配给定参数与此模式的匹配器
		Matcher matcher = pattern.matcher(num);
		// 开始匹配
		if (num != null && !"".equals(num) && !matcher.matches()) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 字符格式
	 * 
	 * @return boolean 验证成功：true；验证失败：false
	 */
	public static boolean checkTextFormat(String text) {
		String reg = "^[^0-9]+$";
		Pattern pattern = Pattern.compile(reg);
		// 创建匹配给定参数与此模式的匹配器
		Matcher matcher = pattern.matcher(text);
		// 开始匹配
		if (text != null && !"".equals(text) && !matcher.matches()) {
			return false;
		}
		return true;
	}

	/**
	 * 校验数字 是否是小数 或整数
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isBigDecimal(String value) {
		String reg = "^[0-9]*[1-9][0-9]*$";
		Pattern pattern = Pattern.compile(reg);
		// 创建匹配给定参数与此模式的匹配器
		Matcher matcher = pattern.matcher(value);
		// 开始匹配
		if (value != null && !"".equals(value) && !matcher.matches()) {
			return true;
		}
		return false;
	}

}
