package com.jecn.epros.server.bean.rule;

import java.io.Serializable;

public class JecnRuleBaiscInfo implements Serializable {
	private Long ruleId;
	private String businessScope;// 业务范围
	private String otherScope;// 其他范围

	// ---------------
	private String purpose;// 目的
	private String applicability;// 使用范围

	private String writeDept;// 文件编写部门
	private String draftman;// 起草人
	private String draftingUnit;// 起草单位
	private String testRunFile;// 试行版文件
	private String changeVersionExplain;// 换版说明

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String getWriteDept() {
		return writeDept;
	}

	public void setWriteDept(String writeDept) {
		this.writeDept = writeDept;
	}

	public String getDraftman() {
		return draftman;
	}

	public void setDraftman(String draftman) {
		this.draftman = draftman;
	}

	public String getDraftingUnit() {
		return draftingUnit;
	}

	public void setDraftingUnit(String draftingUnit) {
		this.draftingUnit = draftingUnit;
	}

	public String getTestRunFile() {
		return testRunFile;
	}

	public void setTestRunFile(String testRunFile) {
		this.testRunFile = testRunFile;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public String getBusinessScope() {
		return businessScope;
	}

	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}

	public String getOtherScope() {
		return otherScope;
	}

	public void setOtherScope(String otherScope) {
		this.otherScope = otherScope;
	}

	public String getChangeVersionExplain() {
		return changeVersionExplain;
	}

	public void setChangeVersionExplain(String changeVersionExplain) {
		this.changeVersionExplain = changeVersionExplain;
	}
}
