package com.jecn.epros.aspect;

import com.jecn.epros.domain.rule.RuleT;
import com.jecn.epros.service.journal.JournalService;
import com.jecn.epros.service.rule.RuleService;
import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hyl
 */
@Component
public class AccessDownloadRecord {

    private static final Logger logger = LoggerFactory.getLogger(AccessDownloadRecord.class);
    @Autowired
    private JournalService journalService;
    @Autowired
    private RuleService ruleService;

    private Map<String, LogType> visitUrls = null;
    // 下载操作说明，流程与制度使用同一个
    private String DOWNLOAD_DOC = "com.jecn.epros.controller.SystemController.downloadDoc";
    // 制度基本信息，制度模板文件访问制度基本信息不记录日志，因为访问操作说明记录过了
    private String RULE_BASE_INFO = "com.jecn.epros.controller.RuleController.findBaseInfo";

    public void recordAccessDownload(HttpServletRequest request, JoinPoint joinPoint) {

        String requestMethod = getRequestMethod(joinPoint);
        if (!needRecord(requestMethod)) {
            return;
        }
        Object[] args = joinPoint.getArgs();
        if (args == null || args.length == 0) {
            return;
        }
        String relatedId = args[0].toString();
        LogType logType = getVisitUrls().get(requestMethod);
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("relatedId", relatedId);
        try {
            if (logType != null) {
                paramMap.put("relatedType", logType.getLogType());
                paramMap.put("operatorType", logType.getOperator());
            } else {
                if (args.length < 2) {
                    return;
                }
                if (DOWNLOAD_DOC.equalsIgnoreCase(requestMethod)) {
                    String docType = args[1].toString();
                    String relatedType = "";
                    if (docType.equalsIgnoreCase("process")) {
                        relatedType = "0";
                    } else if (docType.equalsIgnoreCase("processMap")) {
                        relatedType = "7";
                    } else if (docType.equalsIgnoreCase("rule")) {
                        relatedType = "1";
                    }
                    paramMap.put("relatedType", relatedType);
                    paramMap.put("operatorType", "1");
                } else if (RULE_BASE_INFO.equalsIgnoreCase(requestMethod)) {
                    // 查询是否为制度模板文件
                    boolean isPub = Integer.valueOf(args[1].toString()) == 1 ? true : false;
                    RuleT rule = ruleService.getRuleInfo(Long.valueOf(relatedId), isPub);
                    if (rule == null || rule.getIsDir() == 1) {
                        return;
                    } else {
                        paramMap.put("relatedType", "1");
                        paramMap.put("operatorType", "0");
                    }
                }
            }
            journalService.addWebLog(paramMap);
        } catch (Exception e) {
            logger.error("记录文件访问日志异常，请求的方法为" + requestMethod + " id为" + relatedId, e);
        }
    }

    private boolean needRecord(String requestMethod) {
        Map<String, LogType> visitUrls = getVisitUrls();
        return visitUrls.containsKey(requestMethod);
    }


    private String getRequestMethod(JoinPoint joinPoint) {
        return joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName();
    }

    // 日志类型：0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7流程架构
    private Map<String, LogType> getVisitUrls() {
        if (visitUrls == null) {
            synchronized (this) {
                if (visitUrls == null) {
                    visitUrls = new HashMap<>();
                    // ------------------查阅---------------
                    // 打开流程图
                    visitUrls.put("com.jecn.epros.controller.ProcessController.getProcessChart", new LogType(0, 0));
                    // 流程架构
                    visitUrls.put("com.jecn.epros.controller.ArchitectureController.getChart", new LogType(7, 0));
                    // 制度操作说明显示
                    visitUrls.put("com.jecn.epros.controller.RuleController.findDocument", new LogType(1, 0));
                    // 制度详情
                    visitUrls.put(RULE_BASE_INFO, null);
                    // 文件详情
                    visitUrls.put("com.jecn.epros.controller.FileController.findBaseInfo", new LogType(2, 0));
                    // 标准详情
                    visitUrls.put("com.jecn.epros.controller.StandardController.findBaseInfoBean", new LogType(3, 0));
                    // 风险详情
                    visitUrls.put("com.jecn.epros.controller.RiskController.findBaseInfo", new LogType(4, 0));
                    // 组织图
                    visitUrls.put("com.jecn.epros.controller.OrgController.findBaseInfo", new LogType(5, 0));
                    // 岗位详细信息
                    visitUrls.put("com.jecn.epros.controller.PositionController.findPosBaseInfo", new LogType(6, 0));

                    // ------------------下载---------------
                    // 下载流程、制度的操作说明
                    visitUrls.put(DOWNLOAD_DOC, null);

                    return visitUrls;
                } else {
                    return visitUrls;
                }
            }
        } else {
            return visitUrls;
        }
    }

    private static class LogType {

        // 日志类型：0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7流程架构
        private String logType;
        //0是查阅，1是下载，2是流程架构/流程图/组织图下载
        private String operator;

        public LogType(String logType, String operator) {
            this.logType = logType;
            this.operator = operator;
        }

        public LogType(int logType, int operator) {
            this.logType = String.valueOf(logType);
            this.operator = String.valueOf(operator);
        }

        public String getLogType() {
            return logType;
        }

        public void setLogType(String logType) {
            this.logType = logType;
        }

        public String getOperator() {
            return operator;
        }

        public void setOperator(String operator) {
            this.operator = operator;
        }
    }

}
