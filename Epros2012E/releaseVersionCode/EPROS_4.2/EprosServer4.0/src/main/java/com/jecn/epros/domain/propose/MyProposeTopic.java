package com.jecn.epros.domain.propose;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的合理化建议主页的内容
 *
 * @author hyl
 */
public class MyProposeTopic {

    private Integer hasHandleNum = 0;
    private Integer notHandleNum = 0;
    private Integer selfHasHandleNum = 0;
    private Integer selfNotHandleNum = 0;

    /**
     * 我处理的合理化建议
     **/
    private List<ProposeTopic> hasHandle = new ArrayList<ProposeTopic>();
    /**
     * 需要我处理的合理化建议
     **/
    private List<ProposeTopic> notHandle = new ArrayList<ProposeTopic>();
    /**
     * 我自己提交的已处理的合理化建议
     **/
    private List<ProposeTopic> selfHasHandle = new ArrayList<ProposeTopic>();
    /**
     * 我自己提交的未处理的合理化建议
     **/
    private List<ProposeTopic> selfNotHandle = new ArrayList<ProposeTopic>();

    public Integer getHasHandleNum() {
        return hasHandleNum;
    }

    public void setHasHandleNum(Integer hasHandleNum) {
        this.hasHandleNum = hasHandleNum;
    }

    public Integer getNotHandleNum() {
        return notHandleNum;
    }

    public void setNotHandleNum(Integer notHandleNum) {
        this.notHandleNum = notHandleNum;
    }

    public Integer getSelfHasHandleNum() {
        return selfHasHandleNum;
    }

    public void setSelfHasHandleNum(Integer selfHasHandleNum) {
        this.selfHasHandleNum = selfHasHandleNum;
    }

    public Integer getSelfNotHandleNum() {
        return selfNotHandleNum;
    }

    public void setSelfNotHandleNum(Integer selfNotHandleNum) {
        this.selfNotHandleNum = selfNotHandleNum;
    }

    public List<ProposeTopic> getHasHandle() {
        return hasHandle;
    }

    public void setHasHandle(List<ProposeTopic> hasHandle) {
        this.hasHandle = hasHandle;
    }

    public List<ProposeTopic> getNotHandle() {
        return notHandle;
    }

    public void setNotHandle(List<ProposeTopic> notHandle) {
        this.notHandle = notHandle;
    }

    public List<ProposeTopic> getSelfHasHandle() {
        return selfHasHandle;
    }

    public void setSelfHasHandle(List<ProposeTopic> selfHasHandle) {
        this.selfHasHandle = selfHasHandle;
    }

    public List<ProposeTopic> getSelfNotHandle() {
        return selfNotHandle;
    }

    public void setSelfNotHandle(List<ProposeTopic> selfNotHandle) {
        this.selfNotHandle = selfNotHandle;
    }

}
