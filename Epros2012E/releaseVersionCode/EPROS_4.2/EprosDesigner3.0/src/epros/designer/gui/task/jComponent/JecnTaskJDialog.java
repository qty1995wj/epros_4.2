package epros.designer.gui.task.jComponent;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.action.designer.system.IJecnConfigItemAciton;
import com.jecn.epros.server.action.designer.task.IJecnTaskRecordAction;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnTaskApplicationNew;
import com.jecn.epros.server.bean.task.JecnTaskApprovePeopleConn;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.bean.task.JecnTaskTestRunFile;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.task.temp.JecnTempAccessBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.choose.DeptCompetecnTable;
import epros.designer.gui.choose.PosCompetenceTable;
import epros.designer.gui.choose.PosGroupCompetenceTable;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.popedom.organization.OrgChooseDialog;
import epros.designer.gui.popedom.position.PositionChooseDialog;
import epros.designer.gui.popedom.positiongroup.choose.PosGropChooseDialog;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.gui.system.PeopleMutilSelectCommon;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.gui.system.listener.SelectChangeEvent;
import epros.designer.gui.system.listener.SelectChangeListener;
import epros.designer.gui.task.buss.TaskApproveConstant;
import epros.designer.gui.task.buss.TaskCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.designer.util.JecnVersionUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.JecnTextFieldAndCalendarPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.system.JecnSystemData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 任务主窗体
 * 
 * @author ZHAGNXH
 * @date： 日期：Nov 13, 2012 时间：4:06:37 PM
 */
public abstract class JecnTaskJDialog extends JecnDialog {

	/** 主面板 */
	protected JecnPanel jPanelMain = new JecnPanel();
	/** 滚动条 */
	protected JScrollPane jScrollPaneMain = new JScrollPane();

	/** 信息面板 */
	protected JecnPanel infoPanel = new JecnPanel();
	/** 时间面板 */
	private JecnPanel timePanel = new JecnPanel();
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();
	/** 运行面板 */
	private JecnPanel runPanel = new JecnPanel();
	/** 确定按钮 */
	protected JButton jButtonSubmit = null;
	/** 取消按钮 */
	protected JButton jButtonCancel = null;

	/** *************************查阅权限************************************* */
	/** 部门查阅权限 */
	protected JLabel jLabelOrgAccess = null;
	/** 部门查阅权限的选择按钮 */
	protected JButton jButtonOrgAccess = null;
	/** 部门查阅权限 */
	// protected JTextArea jTextAreaOrgAccess = null;
	private DeptCompetecnTable depCompetenceTable = null;

	// 权限集合
	protected AccessId accId = new AccessId();
	/** 部门权限滚动面板 */
	private JScrollPane depCompetenceScrollPane = null;
	/** 部门 查阅权限 */
	/** 部门查阅权限集合 */
	protected List<JecnTreeBean> listOrgAccess = null;

	/** 岗位查阅权限 */
	protected JLabel jLabelPosAccess = null;
	/** 岗位查阅权限的选择按钮 */
	protected JButton jButtonPosAccess = null;
	/** 岗位查阅权限 */
	// protected JTextArea jTextAreaPosAccess = null;
	private PosCompetenceTable posCompetenceTable = null;

	/** 岗位权限滚动面板 */
	private JScrollPane posCompetenceScrollPane = null;
	/** 岗位查阅权限 */
	/* protected String accessPosId = ""; */
	/** 岗位查阅权限集合 */
	protected List<JecnTreeBean> listPosAccess = null;

	/** 岗位组查阅权限 */
	protected JLabel jLabelPosGroupAccess = null;
	/** 岗位组查阅权限的选择按钮 */
	protected JButton jButtonPosGroupAccess = null;
	/** 岗位组查阅权限 */
	// protected JTextArea jTextAreaPosAccess = null;
	private PosGroupCompetenceTable posGroupCompetenceTable = null;

	/** 岗位组权限滚动面板 */
	private JScrollPane posGroupCompetenceScrollPane = null;
	/** 岗位组查阅权限 */
	/* protected String accessPosGroupId = ""; */
	/** 岗位组查阅权限集合 */
	protected List<JecnTreeBean> listPosGroupAccess = null;

	/** *************************（流程制度）类别************************************* */
	/** 类别 */
	protected JLabel jLabelClassify = null;
	/** 添加类别按钮 */
	// protected JButton jButtonClassify = null;
	/** 类别 */
	protected JComboBox jComboBoxClassify = null;
	/** 所有类别数据 */
	// protected List<Long> comboIdList = new ArrayList<Long>();
	/** 流程类别 */
	protected List<ProceeRuleTypeBean> listTypeBean = new ArrayList<ProceeRuleTypeBean>();

	/** 流程类别名称 */
	protected String changeTypeName = null;
	/** 流程类别ID */
	protected String typeIds = null;

	/** *************************审核人阶段************************************* */
	/** 批准人 */
	protected String approveId = "";
	/** 文控审核人 */
	protected String auditId = "";
	/** 部门审核人 */
	protected String deptPeopleId = "";
	/** 评审人 */
	protected String reviewId = "";
	/** 各业务体系负责人 */
	protected String bussId = "";
	/** IT部总监 */
	protected String itId = "";
	/** 事业部经理 */
	protected String divisionManId = "";
	/** 总经理 */
	protected String generalManId = "";
	/** 文控历史版本版本号 */
	protected List<String> versList = new ArrayList<String>();

	/**
	 * *************************** 任务各阶段审批人
	 * ************************************************
	 */

	/** 任务名称 */
	protected JTextField jTextFieldTaskName = null;
	/** 任务名称 */
	protected JLabel jLabelTaskName = null;

	/** *************************页面显示信息************************************* */
	/** 联系方式 */
	protected JTextField jTextFieldContactWay = null;
	/** 联系方式 */
	protected JLabel jLabelContactWay = null;
	/** 开始时间 */
	protected JTextField jTextFieldStartTime = null;
	/** 开始时间 */
	protected JLabel jLabelStartTime = null;
	/** 结束时间 */
	protected JecnTextFieldAndCalendarPanel jTextFieldEndTime = null;
	/** 结束时间 */
	protected JLabel jLabelEndTime = null;
	/** 密级 */
	protected JComboBox jComboBoxSecurityClassification = null;
	/** 密级 */
	protected JLabel jLabelSecurityClassification = null;
	/** 版本号 */
	protected JTextField jTextFieldVersion = null;
	/** 版本号 */
	protected JLabel jLabelVersion = null;
	/** 编号 */
	protected JLabel jLabelNumber = null;
	/** 编号 */
	protected JTextField jTextFieldNumber = null;
	/** 术语定义 */
	protected JLabel jLabelDefinition = null;
	/** 术语定义 */
	protected JTextArea jTextAreaDefinition = null;
	/** 变更说明 */
	protected JLabel jLabelChangeDescription = null;
	/** 变更说明 */
	protected JTextArea jTextAreaChangeDescription = null;
	/** 自定义 输入项1 */
	protected JLabel customInputItemOne = null;
	/** 自定义 输入项1 */
	protected JTextArea customInputItemOneText = null;
	/** 自定义 输入项2 */
	protected JLabel customInputItemTwo = null;
	/** 自定义 输入项2 */
	protected JTextArea customInputItemTwoText = null;
	/** 自定义 输入项3 */
	protected JLabel customInputItemThree = null;
	/** 自定义 输入项3 */
	protected JTextArea customInputItemThreeText = null;

	/** 运行版本,试运行结束时间 */
	protected JComboBox jComboBoxRunVersion = null;
	/** 运行版本 */
	protected JLabel jLabelRunningVersion = null;
	/** 试运行结束时间 */
	protected JLabel jLabelEndOfTestRunTime = null;
	/** 试运行天数 */
	protected JecnTextFieldAndCalendarPanel jTextFieldRunTime = null;
	protected JTextField jTextFieldUploadATrialRunReport = null;
	/** 上传试运行报告 */
	/** 上传试运行报告 */
	protected JLabel jLabelUploadATrialRunReport = null;
	/** 上传试运行报告 */
	// private JPanel jPanelUploadATrialRunReport = null;
	/** 上传试运行报告任务 */
	protected JButton jButtonUploadATrialRunReport = null;
	/** 大文本域的大小 */
	/** 上传文件任务 上传按钮 */
	protected JButton jButtonUpload = null;
	/** 上传文件任务 删除 */
	protected JButton jButtonDel = null;
	/** 上传文件table */
	protected JTable jTableUploadFile;
	/** 更新文件 */
	protected JLabel jLabelEditFile;
	/** 更新文件 */
	protected JTextField jTextFieldEditFile;
	/** 更新文件 */
	protected JButton jButtonEditFile;
	/** 任务配置显示的审批人 */
	protected List<TaskConfigItem> appUserItemList;
	/** 流程图建设规范 */
	protected List<JecnConfigItemBean> showItemList;
	/** 流程文件信息 */
	protected List<JecnConfigItemBean> showFlowFileList;
	/** 是否显示编号 */
	// protected boolean isShowNumber = true;
	/** 是否自动编号 */
	// protected boolean isAutoNumber = false;
	/** 试运行 0是不显示，1是显示 */
	protected int isRunVersion = 0;

	/** 自定义输入项名称 **/
	protected String isCustomOneName = "";

	/** 自定义输入项名称 **/
	protected String isCustomTwoName = "";
	/** 自定义输入项名称 **/
	protected String isCustomThreeName = "";

	/** 试运行周期 默认值 */
	protected int runTimeValue = 0;
	/** 试运行发送报告周期 默认值 */
	protected int sendRunTimeValue = 0;
	/** 把评审人后面的审核人放在map中，判断评审人后面至少有一个审核人 */
	protected List<TaskStateEnum> listOrder = new ArrayList<TaskStateEnum>();
	/** 存放所有的审核人 */
	protected Map<String, String> mapString = new HashMap<String, String>();
	/** 提交任务的节点 */
	protected JecnTreeNode treeNode = null;
	/** 提交任务树 */
	protected JecnTree jTree;
	/** 上传任务的时候为true */
	// protected boolean isUpload = false;
	/** 界面显示对象 */
	protected JecnTaskApplicationNew jecnTaskApplication = new JecnTaskApplicationNew();
	/** 任务各阶段信息 */
	Map<TaskStateEnum, JecnTaskStage> jecnTaskStageMap = new HashMap<TaskStateEnum, JecnTaskStage>();
	/** 责任人List */
	protected List<JecnTreeBean> listPerson = new ArrayList<JecnTreeBean>();

	protected static Logger log = Logger.getLogger(JecnTaskJDialog.class);
	/** 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务 */
	protected int taskType;

	/** 任务名称必填项 */
	private JLabel taskNameRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 时间 */
	private JLabel timeRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 版本号 */
	private JLabel versionRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 上传试运行报告 */
	private JLabel uploadATrialRunReportLab = new JLabel(JecnProperties.getValue("required"));
	/** 术语定义 */
	private JLabel definitionRequiredLab = new JLabel(JecnProperties.getValue("required"));

	/** 变更说明 */
	private JLabel areaChangeDescriptionLab = new JLabel(JecnProperties.getValue("required"));

	/** 试运行结束时间必填项 */
	private JLabel testRunTimeLab = new JLabel(JecnProperties.getValue("required"));

	// private JLabel editNumRequLab = new JLabel("*");
	private Dimension dimension = null;

	/** 任务名称 */
	protected String taskName = "";
	/** 版本号 */
	protected String version = "";
	/** 术语定义 */
	protected String definition = "";
	/** 变更说明 */
	protected String changeDescription = "";
	/** 历史记录下拉菜单 */
	JMenu historyMenu = new JMenu(JecnProperties.getValue("historyVersion"));
	/** 菜单面板 */
	protected JecnPanel menuPanel = new JecnPanel();
	/** 拟稿人 */
	protected PeopleSelectCommon taskAppDrafterUserCommon;
	/** 文控 */
	protected PeopleSelectCommon taskAppDocCtrlUserCommon;
	protected PeopleSelectCommon taskAppDeptUserCommon;
	protected PeopleMutilSelectCommon taskAppReviewerCommon;
	protected PeopleSelectCommon taskAppPzhUserCommon;
	protected PeopleSelectCommon taskAppOprtUserCommon;
	protected PeopleSelectCommon taskAppITUserCommon;
	protected PeopleSelectCommon taskAppDivisionManagerCommon;
	protected PeopleSelectCommon taskAppGeneralManagerCommon;
	/** 自定义多选 */
	protected PeopleMutilSelectCommon mutilSelectCommon9;
	/** 自定义多选 */
	protected PeopleMutilSelectCommon mutilSelectCommon10;
	/** 自定义多选 */
	protected PeopleMutilSelectCommon mutilSelectCommon11;
	/** 自定义多选 */
	protected PeopleMutilSelectCommon mutilSelectCommon12;

	/** 自定义字段 审批人ID */
	protected String customApprovaId1 = "";
	protected String customApprovaId2 = "";
	protected String customApprovaId3 = "";
	protected String customApprovaId4 = "";
	/** 是否文控主导审核 */
	protected boolean isControl = false;

	/** 任务类型 0审批 1借阅 2废止 **/
	protected int approveType;
	/** 关键词 **/
	protected JLabel jLabelKeyword = null;
	protected JTextField jTextFieldKeyword = null;
	private JLabel jLabelKeywordRequest = new JLabel(JecnProperties.getValue("required"));

	/*	*//** 废止文件选择框 **/
	/*
	 * protected ProcessMultiSelectCommon abolishFlowsCommon; protected
	 * FileMultiSelectCommon abolishFilesCommon; protected RuleMultiSelectCommon
	 * abolishRulesCommon;
	 */

	/**
	 * 各阶段审核人 treeBean 集合 key:TaskStateEnum 阶段标识，value List<JecnTreeBean>
	 * 对应树上审批人节点
	 */
	private Map<TaskStateEnum, List<JecnTreeBean>> approvePersonTrees = new HashMap<TaskStateEnum, List<JecnTreeBean>>();

	private int infoPanelHeight = 400;
	private String draftId = "";

	public JecnTaskJDialog(JecnTree jTree, JecnTreeNode treeNode) {
		this(jTree, treeNode, 0);
	}

	public JecnTaskJDialog(JecnTree jTree, JecnTreeNode treeNode, int approveType) {
		this.approveType = approveType;
		this.jTree = jTree;
		this.treeNode = treeNode;
		this.setSize(700, 600);
		if (this.approveType == 0) {
			this.setTitle(TaskApproveConstant.submit_for_app);
		} else if (this.approveType == 2) {
			this.setTitle(JecnProperties.getValue("submitAbolish"));
		}
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setResizable(true);
		// 任务审批
		dimension = new Dimension(700, 500);
		jScrollPaneMain.setPreferredSize(dimension);
	}

	/**
	 * 提交审批，初始化信息
	 */
	protected void init() {
		initTaskData();
		// 获取任务类型
		initTaskType();
		/** 读取流程xml文件配置 */
		initXML();
		// 是否需要自动编号
		isAutoNumber();
		/** 读取任务配置 */
		readRequests();
		// 判断审核人顺序
		setCheckerOrder();
		/** 读取审批人默认值 */
		redTaskDefaultValue();
		initAppRove();
		/** 初始化控件 */
		initComponents();
		/** 初始化其他界面 */
		initElseShow();
		this.setLocationRelativeTo(null);
		if (this.listOrder.size() > 4) {
			infoPanelHeight = infoPanelHeight + (listOrder.size() - 4) * 40;
		}
		dimension = new Dimension(630, infoPanelHeight);
		infoPanel.setPreferredSize(dimension);
		jScrollPaneMain.setViewportView(infoPanel);

		if (JecnConfigTool.isAutoNumber()) {// 自动编号
			if (jTextFieldNumber != null) {
				jTextFieldNumber.setEditable(false);
			}
		}
	}

	protected void initTaskData() {

	}

	/**
	 * 废止状态隐藏
	 */
	private void initAppRove() {
		if (approveType == 2) {
			isRunVersion = 0;
			jecnTaskApplication.setIsAccess(0);
			jecnTaskApplication.setIsPublic(0);
			jecnTaskApplication.setFileType(0);
			jecnTaskApplication.setIsDefinition(0);
		}
	}

	/**
	 * 获取任务类型
	 * 
	 */
	private void initTaskType() {
		switch (treeNode.getJecnTreeBean().getTreeNodeType()) {
		case process:// 流程
			taskType = 0;
			break;
		case file:// 文件
		case fileDir:// 文件
			taskType = 1;
			break;
		case ruleModeFile:// 制度模板文件
			taskType = 2;
			break;
		case ruleFile:// 制度文件
			taskType = 3;
			break;
		case processMap:// 流程地图
			taskType = 4;
			break;
		default:
			break;
		}
	}

	/**
	 * 三幅百货配置 是否显示编号
	 * 
	 */
	protected void isAutoNumber() {
	}

	/**
	 * 获得目标人
	 * 
	 * @param 任务状态
	 * @return Set<Long>
	 */
	protected Set<Long> getToApprovePeople(int state) {
		Set<Long> sets = new HashSet<Long>();
		if (state == 1) {
			addSetIds(auditId, sets);
		} else if (state == 2) {
			addSetIds(deptPeopleId, sets);
		} else if (state == 3) {
			addSetIds(reviewId, sets);
		} else if (state == 4) {
			addSetIds(approveId, sets);
		} else if (state == 6) {
			addSetIds(bussId, sets);
		} else if (state == 7) {
			addSetIds(itId, sets);
		} else if (state == 8) {
			addSetIds(divisionManId, sets);
		} else if (state == 9) {
			addSetIds(generalManId, sets);
		} else if (state == 11) {
			addSetIds(customApprovaId1, sets);
		} else if (state == 12) {
			addSetIds(customApprovaId2, sets);
		} else if (state == 13) {
			addSetIds(customApprovaId3, sets);
		} else if (state == 14) {
			addSetIds(customApprovaId4, sets);
		}
		return sets;
	}

	protected void addSetIds(String approveIds, Set<Long> sets) {
		if (StringUtils.isBlank(approveIds)) {
			return;
		}
		String[] strArr = approveIds.split(",");
		for (String str : strArr) {
			if (StringUtils.isNotBlank(str)) {
				sets.add(Long.valueOf(str));
			}
		}
	}

	/**
	 * 审批人的集合
	 * 
	 * @return
	 */
	protected List<JecnTaskApprovePeopleConn> getlistJecnTaskApprovePeopleConn() {
		List<JecnTaskApprovePeopleConn> listJecnTaskApprovePeopleConn = new ArrayList<JecnTaskApprovePeopleConn>();
		JecnTaskApprovePeopleConn jecnTaskApprovePeopleConn = new JecnTaskApprovePeopleConn();
		jecnTaskApprovePeopleConn.setApprovePid(getJecnUser().getPeopleId());
		jecnTaskApprovePeopleConn.setStageId(0L);
		listJecnTaskApprovePeopleConn.add(jecnTaskApprovePeopleConn);
		/** 文控审核人 */
		if (StringUtils.isNotBlank(auditId) && jecnTaskStageMap.get(TaskStateEnum.a1).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(auditId, TaskStateEnum.a1));
		}
		/** 部门审核人 */
		if (StringUtils.isNotBlank(deptPeopleId) && jecnTaskStageMap.get(TaskStateEnum.a2).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(deptPeopleId, TaskStateEnum.a2));
		}
		/** 评审人 */
		if (StringUtils.isNotBlank(this.reviewId) && jecnTaskStageMap.get(TaskStateEnum.a3).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(reviewId, TaskStateEnum.a3));
		}
		/** 批准审核人 */
		if (StringUtils.isNotBlank(this.approveId) && jecnTaskStageMap.get(TaskStateEnum.a4).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(approveId, TaskStateEnum.a4));
		}
		/** 各个业务系统负责审核人 */
		if (StringUtils.isNotBlank(this.bussId) && jecnTaskStageMap.get(TaskStateEnum.a5).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(bussId, TaskStateEnum.a5));
		}
		/** IT总监责审核人 */
		if (StringUtils.isNotBlank(this.itId) && jecnTaskStageMap.get(TaskStateEnum.a6).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(itId, TaskStateEnum.a6));
		}
		// 添加自定义审批人
		if (StringUtils.isNotBlank(divisionManId) && jecnTaskStageMap.get(TaskStateEnum.a7).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(divisionManId, TaskStateEnum.a7));
		}
		if (StringUtils.isNotBlank(generalManId) && jecnTaskStageMap.get(TaskStateEnum.a8).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(generalManId, TaskStateEnum.a8));
		}
		if (StringUtils.isNotBlank(customApprovaId1) && jecnTaskStageMap.get(TaskStateEnum.a9).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(customApprovaId1, TaskStateEnum.a9));
		}
		if (StringUtils.isNotBlank(customApprovaId2) && jecnTaskStageMap.get(TaskStateEnum.a10).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(customApprovaId2, TaskStateEnum.a10));
		}
		if (StringUtils.isNotBlank(customApprovaId3) && jecnTaskStageMap.get(TaskStateEnum.a11).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(customApprovaId3, TaskStateEnum.a11));
		}
		if (StringUtils.isNotBlank(customApprovaId4) && jecnTaskStageMap.get(TaskStateEnum.a12).getIsShow() == 1) {// 0：不显示（默认）
			listJecnTaskApprovePeopleConn.addAll(getApprocePeopleConnBySplitStr(customApprovaId4, TaskStateEnum.a12));
		}
		return listJecnTaskApprovePeopleConn;
	}

	private List<JecnTaskApprovePeopleConn> getApprocePeopleConnBySplitStr(String approveIds, TaskStateEnum stateEnum) {
		String[] ids = approveIds.split(",");
		List<JecnTaskApprovePeopleConn> listJecnTaskApprovePeopleConn = new ArrayList<JecnTaskApprovePeopleConn>();
		JecnTaskApprovePeopleConn jecnTaskApprovePeopleConn = null;
		for (String id : ids) {
			if (StringUtils.isNotBlank(id)) {
				jecnTaskApprovePeopleConn = new JecnTaskApprovePeopleConn();
				jecnTaskApprovePeopleConn.setApprovePid(Long.valueOf(id));
				jecnTaskApprovePeopleConn.setStageId(jecnTaskStageMap.get(stateEnum).getStageMark().longValue());
				listJecnTaskApprovePeopleConn.add(jecnTaskApprovePeopleConn);
			}
		}
		return listJecnTaskApprovePeopleConn;
	}

	/**
	 * 获取人员信息
	 * 
	 * @return
	 */
	public JecnUser getJecnUser() {
		Long id = taskAppDrafterUserCommon.getIdResult();
		String name = taskAppDrafterUserCommon.getNameResult();
		if (id == null && id == 0) {// 创建任务失败 失败原因人员信息不存在
			log.error("JecnTaskJDialog getJecnUser is error!");
			return null;
		}

		JecnUser user = new JecnUser();
		user.setPeopleId(id);
		user.setTrueName(name);
		return user;
	}

	/**
	 * 获得任务的基本对象
	 * 
	 * @param rId
	 *            关联ID
	 * @param type
	 *            1:流程；2：文件；3制度；
	 * @return
	 */
	protected JecnTaskBeanNew getJecnTaskBeanNew(Long rId) {
		/** 开始时间 */
		Date startTimeDate = JecnTool.getDateByString(jTextFieldStartTime.getText());
		/** 结束时间 */
		Date endTimeDate = JecnTool.getDateByString(jTextFieldEndTime.getText());
		// 源操作时间
		Date updateDate = JecnTool.getDateByString(JecnTool.getStringbyDate(new Date()));
		/** 试运行周期 */
		Date runTime = null;
		if (this.isRunVersion != 0 && jComboBoxRunVersion.getSelectedIndex() == 0) {
			runTime = JecnTool.getDateByString(this.jTextFieldRunTime.getText());
		}

		JecnUser jecnUser = getJecnUser();
		if (jecnUser == null) {
			return null;
		}
		JecnTaskBeanNew jecnTaskBeanNew = new JecnTaskBeanNew();
		/** 任务名称 */
		jecnTaskBeanNew.setTaskName(this.jTextFieldTaskName.getText());
		/** 文件Id */
		jecnTaskBeanNew.setRid(rId);
		/** 任务类型 */
		jecnTaskBeanNew.setTaskType(taskType);
		/** 拟稿人 */
		jecnTaskBeanNew.setCreatePersonId(jecnUser.getPeopleId());
		/** 创建时间 */
		jecnTaskBeanNew.setCreateTime(new Date());
		/** 任务开始时间 */
		jecnTaskBeanNew.setStartTime(startTimeDate);
		/** 任务结束时间 */
		jecnTaskBeanNew.setEndTime(endTimeDate);
		/** 源操作时间 */
		jecnTaskBeanNew.setUpdateTime(updateDate);
		/** 设置任务处于解锁状态 */
		jecnTaskBeanNew.setIsLock(1);
		/** 驳回次数 */
		jecnTaskBeanNew.setApproveNoCounts(0);
		/** 评审次数 */
		jecnTaskBeanNew.setRevirewCounts(0);
		/** 任务说明 */
		jecnTaskBeanNew.setTaskDesc(this.jTextAreaChangeDescription.getText());
		/** 自定输入项1 **/
		jecnTaskBeanNew.setCustomInputItemOne(customInputItemOneText != null ? customInputItemOneText.getText() : "");
		/** 自定输入项2 **/
		jecnTaskBeanNew.setCustomInputItemTwo(customInputItemTwoText != null ? customInputItemTwoText.getText() : "");
		/** 自定输入项3 **/
		jecnTaskBeanNew.setCustomInputItemThree(customInputItemThreeText != null ? customInputItemThreeText.getText()
				: "");
		// 试运行发送报告周期 默认值
		jecnTaskBeanNew.setSendRunTimeValue(sendRunTimeValue);
		jecnTaskBeanNew.setState(0);
		/** 上一级任务状态 */
		jecnTaskBeanNew.setUpState(0);
		/**
		 * 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
		 * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）
		 */
		jecnTaskBeanNew.setTaskElseState(0);
		/** 版本号 */
		jecnTaskBeanNew.setHistoryVistion(this.jTextFieldVersion.getText());
		/** 试运行周期 */
		jecnTaskBeanNew.setTestRunTime(runTime);
		/**
		 * 0：拟稿人主导主导审批（默认） 1：文控审核人主导审批;
		 */
		jecnTaskBeanNew.setIsControlauditLead(jecnTaskApplication.getIsControlAuditLead());
		/** 源人 */
		jecnTaskBeanNew.setFromPeopleId(jecnUser.getPeopleId());
		if (this.isRunVersion != 0) {
			jecnTaskBeanNew.setFlowType(this.jComboBoxRunVersion.getSelectedIndex());// 版本类别
		} else {
			jecnTaskBeanNew.setFlowType(1);
		}
		// 废止状态
		jecnTaskBeanNew.setApproveType(approveType);
		return jecnTaskBeanNew;
	}

	/**
	 * 提交时保持默认审批人
	 * 
	 * @return OutputStream
	 */
	protected OutputStream writePropertTaskRequiredValue() {
		OutputStream fos = null;
		String url = TaskCommon.getPropertiesUrl(treeNode);
		if (DrawCommon.isNullOrEmtryTrim(url)) {// 路径不存在
			log.error("JecnTaskJDialog writePropertTaskRequiredValue is error！");
			return fos;
		}
		try {
			File file = new File(JecnTaskJDialog.class.getResource("/").toURI().getPath() + url);
			// 文件、制度、流程
			fos = new FileOutputStream(file);
			return fos;
		} catch (IOException ex) {
			JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
			log.error("JecnTaskJDialog writePropertTaskRequiredValue is error", ex);
			return null;
		} catch (URISyntaxException e) {
			log.error("JecnTaskJDialog writePropertTaskRequiredValue is error", e);
		}
		return fos;
	}

	/**
	 * 初始化 审批人默认文件
	 * 
	 * @return Properties
	 */
	protected Properties redTaskDefaultValueProperties() {
		Properties properties = new Properties();
		String url = TaskCommon.getPropertiesUrl(treeNode);
		if (DrawCommon.isNullOrEmtryTrim(url)) {// 路径不存在
			log.error("JecnTaskJDialog redTaskDefaultValueProperties is error！");
			return properties;
		}
		try {
			File file = new File(JecnTaskJDialog.class.getResource("/").toURI().getPath() + url);
			// 文件、制度、流程
			InputStream in = new FileInputStream(file);
			properties.load(in);
			in.close();
		} catch (IOException e) {
			JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
			log.error("JecnTaskJDialog redTaskDefaultValueProperties is error", e);
			return null;
		} catch (URISyntaxException e) {
			log.error("JecnTaskJDialog redTaskDefaultValueProperties is error", e);
		}

		return properties;
	}

	/**
	 * 读取审批人默认值
	 */
	private void redTaskDefaultValue() {
		if (JecnConfigTool.isFiberhome()) {
			return;
		}
		// 任务审批关联查询及验证接口
		IJecnTaskRecordAction taskRecordAction = ConnectionPool.getTaskRecordAction();

		// 初始化-人员信息
		Set<Long> sets = initApproveUser();
		List<Object[]> userList = new ArrayList<Object[]>();
		try {
			if (sets.size() > 0) {
				userList = taskRecordAction.getJecnUserByUserIds(sets, JecnConstants.projectId);
			}
		} catch (Exception ex) {
			log.error("JecnTaskJDialog redTaskDefaultValue is error !", ex);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			return;
		}
		// 通过用户ID，得到用户名称，并一一赋值
		if (userList != null && userList.size() > 0) {
			for (TaskStateEnum str : listOrder) {
				/** 文控审核人 */
				if (TaskStateEnum.a1.equals(str)) {
					auditId = getSplitPeopleIds(userList, auditId, str);
					if (this.jecnTaskApplication.getIsControlAuditLead() != 0) {
						break;
					}
				}/** 部门审核人 */
				else if (TaskStateEnum.a2.equals(str)) {
					deptPeopleId = getSplitPeopleIds(userList, deptPeopleId, str);
				}/** 评审人 */
				else if (TaskStateEnum.a3.equals(str)) {
					reviewId = getSplitPeopleIds(userList, reviewId, str);
				}/** 批准人审核 */
				else if (TaskStateEnum.a4.equals(str)) {
					approveId = getSplitPeopleIds(userList, approveId, str);
				}/** 各业务体系负责人 */
				else if (TaskStateEnum.a5.equals(str)) {
					bussId = getSplitPeopleIds(userList, bussId, str);
				}/** 批准人审核 */
				else if (TaskStateEnum.a6.equals(str)) {
					itId = getSplitPeopleIds(userList, itId, str);
				} else if (TaskStateEnum.a7.equals(str)) {// 事业部经理
					divisionManId = getSplitPeopleIds(userList, divisionManId, str);
				} else if (TaskStateEnum.a8.equals(str)) {// 总经理
					generalManId = getSplitPeopleIds(userList, generalManId, str);
				} else if (TaskStateEnum.a9.equals(str)) {// 自定义
					customApprovaId1 = getSplitPeopleIds(userList, customApprovaId1, str);
				} else if (TaskStateEnum.a10.equals(str)) {// 自定义
					customApprovaId2 = getSplitPeopleIds(userList, customApprovaId2, str);
				} else if (TaskStateEnum.a11.equals(str)) {// 自定义
					customApprovaId3 = getSplitPeopleIds(userList, customApprovaId3, str);
				} else if (TaskStateEnum.a12.equals(str)) {// 自定义
					customApprovaId4 = getSplitPeopleIds(userList, customApprovaId4, str);
				}
			}

		} else {
			// 不存在审核人（配置文件可能存在人员ID，但是真实人员可能全部被删除）
			itId = "";
			bussId = "";
			approveId = "";
			deptPeopleId = "";
			reviewId = "";
			auditId = "";
			generalManId = "";
			divisionManId = "";
		}
	}

	// 任务默认审批人显示顺序 (1、专员、责任人、部门领导;2、模板默认人员;3、本地文件存储)
	private boolean isTaskDefaultPeopleOrder = false;

	protected Set<Long> initApproveUser() {
		Set<Long> sets = new HashSet<Long>();
		Map<String, List<JecnUser>> markToApprovePeoples = initMarkToApprovePeoples();
		Properties properties = redTaskDefaultValueProperties();
		if (properties == null) {
			return sets;
		}

		isTaskDefaultPeopleOrder = JecnConfigTool.getTaskDefaultPeoplesOrder();
		// 只有是文控主导审批，以及到文控阶段的时候为true
		boolean isControlBreak = false;
		for (TaskStateEnum stateEnum : listOrder) {

			if (isControlBreak) {
				break;
			}
			switch (stateEnum) {
			case a1:
				this.auditId = getDefaultValueFromDbElseProperties("com.jecn.task.bean.recordAuditPeopleId",
						properties, markToApprovePeoples, ConfigItemPartMapMark.taskAppDocCtrlUser, stateEnum);

				if (auditId != null && StringUtils.isNotBlank(auditId)) {
					sets.add(Long.valueOf(auditId));
				}
				if (isControl) {// 文控审核人主导审批
					isControlBreak = true;
				}
				break;
			case a2:
				this.deptPeopleId = getDefaultValueFromDbElseProperties("com.jecn.task.bean.deptAuditPeopleIds",
						properties, markToApprovePeoples, ConfigItemPartMapMark.taskAppDeptUser, stateEnum);
				if (deptPeopleId != null && StringUtils.isNotBlank(deptPeopleId)) {
					sets.add(Long.valueOf(deptPeopleId));
				}
				break;
			case a3:
				this.reviewId = getDefaultValueFromDbElseProperties("com.jecn.task.bean.reviewPeopleId", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskAppReviewer, stateEnum);
				addSetIds(reviewId, sets);
				break;
			case a4:
				this.approveId = getDefaultValueFromDbElseProperties("com.jecn.task.bean.approvePeopleId", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskAppPzhUser, stateEnum);
				addSetIds(approveId, sets);
				break;
			case a5:
				this.bussId = getDefaultValueFromDbElseProperties("com.jecn.task.bean.operationPeopleId", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskAppOprtUser, stateEnum);
				addSetIds(bussId, sets);
				break;
			case a6:
				this.itId = getDefaultValueFromDbElseProperties("com.jecn.task.bean.itPeopleId", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskAppITUser, stateEnum);
				addSetIds(itId, sets);
				break;
			case a7:
				divisionManId = getDefaultValueFromDbElseProperties("com.jecn.task.bean.divManPeopleId", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskAppDivisionManager, stateEnum);
				addSetIds(divisionManId, sets);
				break;
			case a8:
				generalManId = getDefaultValueFromDbElseProperties("com.jecn.task.bean.genManPeopleIds", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskAppGeneralManager, stateEnum);
				addSetIds(generalManId, sets);
				break;
			case a9:
				customApprovaId1 = getDefaultValueFromDbElseProperties("customApprovaId1", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskCustomApproval1, stateEnum);
				addSetIds(customApprovaId1, sets);
				break;
			case a10:
				customApprovaId2 = getDefaultValueFromDbElseProperties("customApprovaId2", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskCustomApproval2, stateEnum);
				addSetIds(customApprovaId2, sets);
				break;
			case a11:
				customApprovaId3 = getDefaultValueFromDbElseProperties("customApprovaId3", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskCustomApproval3, stateEnum);
				addSetIds(customApprovaId3, sets);
				break;
			case a12:
				customApprovaId4 = getDefaultValueFromDbElseProperties("customApprovaId4", properties,
						markToApprovePeoples, ConfigItemPartMapMark.taskCustomApproval4, stateEnum);
				addSetIds(customApprovaId4, sets);
				break;
			default:
				break;
			}
		}
		return sets;
	}

	/**
	 * 获取默认审批人,先读取数据库配置的，如果没有再从本地读
	 * 
	 * @param key
	 * @param properties
	 * @param markToApprovePeoples
	 * @param mark
	 * @return
	 */
	private String getDefaultValueFromDbElseProperties(String key, Properties properties,
			Map<String, List<JecnUser>> markToApprovePeoples, ConfigItemPartMapMark mark, TaskStateEnum state) {
		String approveId = null;

		// 九新药业 读取审批人
		if (JecnConfigTool.isJiuXinLoginType()
				&& this.treeNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.process) {// 任务默认审批人显示顺序
			approveId = JecnDesignerTaskCommon.getDefaultsPeopleAppId(state, taskType, treeNode);
			return approveId;
		}
		// 1、模板
		approveId = getAuditPeopleByMark(mark, markToApprovePeoples);
		if (StringUtils.isNotBlank(approveId)) {
			return approveId;
		}
		// /2、、专员、责任人、部门领导;2、模板默认人员;3、本地文件存储)
		if (isTaskDefaultPeopleOrder) {// 任务默认审批人显示顺序
			approveId = JecnDesignerTaskCommon.getDefaultPeopleAppId(state, taskType, treeNode);
			if (StringUtils.isNotBlank(approveId)) {
				return approveId;
			}
		}

		// 3、本地默认存储
		approveId = properties.getProperty(key);
		return approveId;
	}

	private String getAuditPeopleByMark(ConfigItemPartMapMark approveMark,
			Map<String, List<JecnUser>> markToApprovePeoples) {
		if (approveMark == null || markToApprovePeoples == null) {
			return null;
		}
		List<JecnUser> list = markToApprovePeoples.get(approveMark.toString());
		String ids = "";
		if (list != null && list.size() > 0) {
			for (JecnUser user : list) {
				ids += "," + user.getPeopleId();
			}
			if (ids.startsWith(",")) {
				ids = ids.replaceFirst(",", "");
			}
		}

		return ids;
	}

	protected Map<String, List<JecnUser>> initMarkToApprovePeoples() {
		Map<String, List<JecnUser>> markToApprovePeople = new HashMap<String, List<JecnUser>>();
		for (TaskConfigItem config : appUserItemList) {
			markToApprovePeople.put(config.getMark(), config.getUsers());
		}
		return markToApprovePeople;
	}

	/**
	 * 读取流程xml文件配置
	 */
	protected void initXML() {
		IJecnConfigItemAciton configAciton = ConnectionPool.getConfigAciton();
		// 各阶段审批人显示项 typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置
		int typeBigModel = TaskCommon.typeBigModel(treeNode.getJecnTreeBean());
		// 查询任务审批环节显示数据
		appUserItemList = listApproveStage();
		// 查询所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
		showItemList = configAciton.selectTaskShowAllItems(typeBigModel);
		// 查询流程文件排序后显示数据 流程必填项
		// showFlowFileList = configAciton.selectShowFlowFile();
	}

	/**
	 * 查找模板
	 * 
	 * @return
	 */
	private List<TaskConfigItem> listApproveStage() {
		List<TaskConfigItem> configs = new ArrayList<TaskConfigItem>();
		try {
			List<TaskConfigItem> fullConfigs = JecnTreeCommon.getTaskConfigItems(treeNode, approveType);
			for (TaskConfigItem config : fullConfigs) {
				if (config.getMark().equals(ConfigItemPartMapMark.taskAppPubType.toString())) {
					configs.add(config);
					if (config.getValue().equals("1")) {// 文控主导审批
						isControl = true;
					}
				} else {
					if ("1".equals(config.getValue())) {
						configs.add(config);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return configs;
	}

	/** 初始化其他界面 */
	protected abstract void initElseShow();

	/** 初始化控件 */
	private void initComponents() {

		/** 上传试运行报告 */
		uploadATrialRunReportLab.setVisible(false);
		/** 术语定义 */
		definitionRequiredLab.setVisible(false);
		jPanelMain.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		// 初始化组件
		Insets insets = new Insets(3, 3, 3, 3);
		Insets insets5 = new Insets(5, 5, 5, 5);
		// ************************组件布局*****************************
		c = new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5,
				5, 5, 5), 0, 0);
		jPanelMain.add(jScrollPaneMain, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 任务名称：
		taskNameRequiredLab.setForeground(Color.red);
		jLabelTaskName = new JLabel(JecnProperties.getValue("taskNameTitle"));
		jTextFieldTaskName = new JTextField();
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(jLabelTaskName, c);
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(jTextFieldTaskName, c);
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(taskNameRequiredLab, c);

		// 初始化拟稿人
		JecnUser jecnUser = JecnConstants.loginBean.getJecnUser();
		JecnTreeBean drafter = new JecnTreeBean();
		drafter.setTreeNodeType(TreeNodeType.person);
		if (jecnUser != null) {
			drafter.setId(jecnUser.getPeopleId());
			drafter.setName(jecnUser.getTrueName());
		}
		taskAppDrafterUserCommon = new PeopleSelectCommon(1, infoPanel, insets, drafter, this, JecnProperties
				.getValue("draftPeopleC"), true);

		taskAppDrafterUserCommon.addSelectChangeListener(new SelectChangeListener() {
			@Override
			public void actionPerformed(SelectChangeEvent e) {
				List<JecnTreeBean> result = e.getResult();
				if (result != null && result.size() > 0 && jTextFieldContactWay != null) {
					// 查询人员信息
					Long id = result.get(0).getId();
					JecnUser user = ConnectionPool.getPersonAction().getJecnUserByPeopleId(id);
					if (user != null) {
						jTextFieldContactWay.setText(user.getPhoneStr());
					}
				} else if (jTextFieldContactWay != null) {
					jTextFieldContactWay.setText("");
				}
			}
		});
		if (this.jecnTaskApplication.getIsPhone() != 0) {
			// "联系方式:"
			jLabelContactWay = new JLabel(JecnProperties.getValue("phone"));
			jTextFieldContactWay = new JTextField();
			jTextFieldContactWay.setEditable(false);
			if (jecnUser != null && jecnUser.getPhoneStr() != null) {
				jTextFieldContactWay.setText(jecnUser.getPhoneStr());
			}
			c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			infoPanel.add(jLabelContactWay, c);
			c = new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			infoPanel.add(jTextFieldContactWay, c);
			if (this.jecnTaskApplication.getIsPhone() == 2) {
				c = new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			this.infoPanelHeight += 40;
		}

		// 开始时间,结束时间 Panel

		// 开始时间:
		jLabelStartTime = new JLabel(JecnProperties.getValue("startTime"));
		jTextFieldStartTime = new JTextField();
		jTextFieldStartTime.setEditable(false);
		jTextFieldStartTime.setText(JecnTool.getStringbyDate(new Date()));
		// "结束时间:改为预估结束时间:"
		jLabelEndTime = new JLabel(JecnProperties.getValue("predictEndTime"));
		jTextFieldEndTime = new JecnTextFieldAndCalendarPanel();
		// 当前日期加三天
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 3); // 加3天
		// 预估结束时间默认为三天后的时间
		jTextFieldEndTime.setText(JecnTool.getStringbyDate(cal.getTime()));
		timeRequiredLab.setForeground(Color.red);

		// 开始时间
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(jLabelStartTime, c);
		c = new GridBagConstraints(1, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 3), 0, 0);
		infoPanel.add(timePanel, c);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		timePanel.add(jTextFieldStartTime, c);
		// 结束时间
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		timePanel.add(jLabelEndTime, c);
		c = new GridBagConstraints(2, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(3, 3, 3, 0), 0, 0);
		timePanel.add(jTextFieldEndTime, c);
		c = new GridBagConstraints(3, 3, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(timeRequiredLab, c);

		if (this.jecnTaskApplication.getIsPublic() != 0) {
			// 密级
			jComboBoxSecurityClassification = new JComboBox();
			jComboBoxSecurityClassification.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED) {// 公开 隐藏
						hiddenOrShowAccessComponents();
					}
				}
			});
			jComboBoxSecurityClassification.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
					JecnProperties.getValue("secret"), JecnProperties.getValue("gongKai") }));
			// 密级：
			jLabelSecurityClassification = new JLabel(JecnProperties.getValue("security"));

			c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			infoPanel.add(jLabelSecurityClassification, c);
			c = new GridBagConstraints(1, 4, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			infoPanel.add(jComboBoxSecurityClassification, c);
			if (this.jecnTaskApplication.getIsPublic() == 2) {
				c = new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			this.infoPanelHeight += 40;

		}
		try {
			versList = ConnectionPool.getDocControlAction().findVersionListByFlowId(treeNode.getJecnTreeBean().getId(),
					taskType);
		} catch (Exception e) {
			log.error("JecnTaskJDialog initComponents is error！", e);
		}

		// 版本号
		jLabelVersion = new JLabel(JecnProperties.getValue("versionNumC"));
		jTextFieldVersion = new JTextField();
		initVersion();

		// 获得历史版本信息
		// 菜单项
		Dimension sizemenu = new Dimension(100, 20);
		historyMenu.setPreferredSize(sizemenu);
		historyMenu.setMinimumSize(sizemenu);
		historyMenu.setMaximumSize(sizemenu);
		historyMenu.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		for (int i = 0; i < versList.size(); i++) {
			JMenuItem menu = new JMenuItem(versList.get(i));
			menu.setPreferredSize(sizemenu);
			menu.setMinimumSize(sizemenu);
			menu.setMaximumSize(sizemenu);
			historyMenu.add(menu);
		}
		JMenuBar menuBar = new JMenuBar();
		menuPanel.add(menuBar);
		menuBar.add(historyMenu);

		versionRequiredLab = new JLabel(JecnProperties.getValue("required"));
		versionRequiredLab.setForeground(Color.red);
		if (approveType != 2) {
			c = new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			infoPanel.add(jLabelVersion, c);

			c = new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			infoPanel.add(JecnVersionUtil.getVersionTextPanel(jTextFieldVersion), c);
			if (JecnConfigTool.autoVersionNumber()
					&& JecnConfigTool.isShowItem(ConfigItemPartMapMark.enableSmallVersion)) {
				// 大版本 复选框
				JecnVersionUtil.getLargeVersionCheckBox().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						JecnVersionUtil.initVersion(jTextFieldVersion, treeNode.getJecnTreeBean(), taskType);
					}
				});
				// 小版本复选框
				JecnVersionUtil.getSmallVersionCheckBox().addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						JecnVersionUtil.initVersion(jTextFieldVersion, treeNode.getJecnTreeBean(), taskType);
					}
				});
			}
			c = new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			infoPanel.add(menuBar, c);

			c = new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			infoPanel.add(versionRequiredLab, c);
		}
		if (isRunVersion != 0) {
			// 运行版本,试运行结束时间
			jLabelRunningVersion = new JLabel(TaskApproveConstant.testRunVersion);
			jComboBoxRunVersion = new JComboBox();
			jComboBoxRunVersion.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
					JecnProperties.getValue("testRunVersionC"), TaskApproveConstant.official_version,
					TaskApproveConstant.upgreaded_version }));
			jComboBoxRunVersion.addItemListener(new java.awt.event.ItemListener() {

				public void itemStateChanged(java.awt.event.ItemEvent evt) {
					if (jComboBoxRunVersion.getSelectedIndex() == 0) {// 试运行
						jLabelUploadATrialRunReport.setVisible(false);
						jTextFieldUploadATrialRunReport.setVisible(false);
						jButtonUploadATrialRunReport.setVisible(false);
						uploadATrialRunReportLab.setVisible(false);
						testRunTimeLab.setVisible(true);
					} else {// 正式或升级版
						jLabelUploadATrialRunReport.setVisible(true);
						jTextFieldUploadATrialRunReport.setVisible(true);
						jButtonUploadATrialRunReport.setVisible(true);
						uploadATrialRunReportLab.setVisible(true);
						testRunTimeLab.setVisible(false);
					}
				}
			});
			jLabelEndOfTestRunTime = new JLabel(TaskApproveConstant.testRunEnd);
			jTextFieldRunTime = new JecnCalendarTextFieldPanel();
			this.infoPanelHeight += 40;

			if (isRunVersion != 0) {// 如果存在试运行，默认不显示上传试运行文件
				// 上传试运行报告
				jLabelUploadATrialRunReport = new JLabel(JecnProperties.getValue("uploadRunReport"));
				// jPanelUploadATrialRunReport.add(jLabelUploadATrialRunReport);
				jTextFieldUploadATrialRunReport = new JTextField();
				jTextFieldUploadATrialRunReport.setEditable(false);
				// jPanelUploadATrialRunReport.add(jTextFieldUploadATrialRunReport);
				jButtonUploadATrialRunReport = new JButton(JecnProperties.getValue("uploadBut"));
				uploadATrialRunReportLab.setForeground(Color.red);
				jButtonUploadATrialRunReport.addActionListener(new ActionListener() {

					public void actionPerformed(java.awt.event.ActionEvent evt) {
						// 获取默认路径
						String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFileManageDirectory();
						// 获取截取后正确的路径
						if (pathUrl != null && StringUtils.isNotBlank(pathUrl)) {
							pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
						}
						JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);
						fileChoose.setEidtTextFiled(false);

						// 可以同时上传多个文件
						fileChoose.setMultiSelectionEnabled(true);
						// 设置标题
						fileChoose.setDialogTitle(JecnProperties.getValue("fileUpload"));
						int i = fileChoose.showSaveDialog(JecnTaskJDialog.this);
						if (i == JFileChooser.APPROVE_OPTION) {
							// 获取文件路径
							String fileDirectory = fileChoose.getSelectedFile().getPath();
							// 修改配置文件中的路径
							JecnSystemData.readCurrSystemFileIOData().setSaveFileManageDirectory(fileDirectory);
							JecnSystemData.writeCurrSystemFileIOData();
							jTextFieldUploadATrialRunReport.setText(fileDirectory);
						}
					}
				});
				// 运行版本
				c = new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(jLabelRunningVersion, c);
				// 运行面板runPanel
				c = new GridBagConstraints(1, 6, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(runPanel, c);

				c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				runPanel.add(jComboBoxRunVersion, c);
				c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						insets, 0, 0);
				runPanel.add(jLabelEndOfTestRunTime, c);
				c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				runPanel.add(jTextFieldRunTime, c);

				testRunTimeLab.setForeground(Color.red);
				// 试运行结束时间必填项
				c = new GridBagConstraints(3, 6, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
						0, 0);
				infoPanel.add(testRunTimeLab, c);

				// 上传试运行报告
				c = new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(jLabelUploadATrialRunReport, c);
				c = new GridBagConstraints(1, 7, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(jTextFieldUploadATrialRunReport, c);
				c = new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(jButtonUploadATrialRunReport, c);
				c = new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(uploadATrialRunReportLab, c);
			}

		}

		if (this.jecnTaskApplication.getFileType() != 0) {
			// 类别：
			jLabelClassify = new JLabel(JecnProperties.getValue("flowTypeC"));
			jComboBoxClassify = new JComboBox();
			c = new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			infoPanel.add(jLabelClassify, c);
			c = new GridBagConstraints(1, 8, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			infoPanel.add(jComboBoxClassify, c);
			if (this.jecnTaskApplication.getFileType() == 2) {
				c = new GridBagConstraints(3, 8, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			this.infoPanelHeight += 40;
		}

		if (this.jecnTaskApplication.getIsFlowNumber() != 0) {//
			// 不是上传文件任务，并且显示编号
			// 流程编号：
			if (taskType == 0) {
				jLabelNumber = new JLabel(JecnProperties.getValue("flowNumC"));
			} else if (taskType == 1) {
				jLabelNumber = new JLabel(JecnProperties.getValue("fileNumC"));
			} else if (taskType == 2 || taskType == 3) {
				jLabelNumber = new JLabel(JecnProperties.getValue("ruleNumC"));
			} else if (taskType == 4) {
				jLabelNumber = new JLabel(JecnProperties.getValue("processMapNumC"));
			}
			jTextFieldNumber = new JTextField();
			c = new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			infoPanel.add(jLabelNumber, c);
			c = new GridBagConstraints(1, 9, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			infoPanel.add(jTextFieldNumber, c);

			if (numIsMustWrite()) {
				c = new GridBagConstraints(3, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			this.infoPanelHeight += 40;
		}

		JScrollPane sp = null;
		if (this.jecnTaskApplication.getIsAccess() != 0) {
			this.infoPanelHeight += 200;
			try {
				// 根据节点获取查阅权限对应的type type0是流程;1是文件2;是标准;3制度
				int type = TaskCommon.getViewType(treeNode);
				LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(
						treeNode.getJecnTreeBean().getId(), type);
				if (lookPopedomBean != null) {// 获取查阅权限
					// 部门权限集合
					listOrgAccess = lookPopedomBean.getOrgList();
					// 部门权限IDs
					if (listOrgAccess != null && listOrgAccess.size() > 0) {
						/* StringBuffer sbIds = new StringBuffer(); */
						StringBuffer sbNames = new StringBuffer();
						List<AccessId> orgAccIds = new ArrayList<AccessId>();
						for (JecnTreeBean o : listOrgAccess) {
							AccessId accId = new AccessId();
							accId.setAccessId(o.getId());
							orgAccIds.add(accId);
							/* sbIds.append(o.getId() + ","); */
							sbNames.append(o.getName() + "/");
							// listDept.add(o);
						}
						/* accessOrgId = sbIds.substring(0, sbIds.length() - 1); */
						accId.setOrgAccessId(orgAccIds);
					}
					// 岗位权限集合
					listPosAccess = lookPopedomBean.getPosList();
					// 岗位查阅权限ID集合
					if (listPosAccess != null && listPosAccess.size() > 0) {
						/* StringBuffer sbIds = new StringBuffer(); */
						StringBuffer sbNames = new StringBuffer();
						List<AccessId> posAccIds = new ArrayList<AccessId>();
						for (JecnTreeBean o : listPosAccess) {
							AccessId accId = new AccessId();
							accId.setAccessId(o.getId());
							posAccIds.add(accId);
							/* sbIds.append(o.getId() + ","); */
							sbNames.append(o.getName() + "/");
							// listDept.add(o);
						}
						/* accessPosId = sbIds.substring(0, sbIds.length() - 1); */
						accId.setPosAccessId(posAccIds);
					}
					// 岗位组权限集合
					listPosGroupAccess = lookPopedomBean.getPosGroupList();
					// 岗位组查阅权限ID集合
					if (listPosGroupAccess != null && listPosGroupAccess.size() > 0) {
						/* StringBuffer sbIds = new StringBuffer(); */
						StringBuffer sbNames = new StringBuffer();
						List<AccessId> posGAccIds = new ArrayList<AccessId>();
						for (JecnTreeBean o : listPosGroupAccess) {
							AccessId accId = new AccessId();
							accId.setAccessId(o.getId());
							posGAccIds.add(accId);
							/* sbIds.append(o.getId() + ","); */
							sbNames.append(o.getName() + "/");
						}
						/*
						 * accessPosGroupId = sbIds.substring(0, sbIds.length()
						 * - 1);
						 */
						accId.setPosGroupAccessId(posGAccIds);
					}
				}
			} catch (Exception e) {
				log.error("JecnTaskJDialog is error", e);
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			}
			jLabelOrgAccess = new JLabel(JecnProperties.getValue("departCompetenceC"));
			// 部门权限显示表格
			depCompetenceTable = new DeptCompetecnTable(listOrgAccess, isShowDownLoad());
			depCompetenceScrollPane = new JScrollPane();
			depCompetenceScrollPane.setViewportView(depCompetenceTable);
			depCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 选择
			jButtonOrgAccess = new JButton(JecnProperties.getValue("selectBtn"));
			jButtonOrgAccess.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					// 部门权限选择框
					OrgChooseDialog orgChooseDialog = new OrgChooseDialog(listOrgAccess,true);
					orgChooseDialog.setVisible(true);
					if (orgChooseDialog.isOperation()) {

						// 判断是否选择了部门
						if (listOrgAccess != null && listOrgAccess.size() > 0) {
							/* StringBuffer sbIds = new StringBuffer(); */
							StringBuffer sbNames = new StringBuffer();
							List<AccessId> orgAccIds = new ArrayList<AccessId>();
							for (JecnTreeBean o : listOrgAccess) {
								AccessId accId = new AccessId();
								accId.setAccessId(o.getId());
								orgAccIds.add(accId);
								/* sbIds.append(o.getId() + ","); */
								sbNames.append(o.getName() + "/");
							}
							/*
							 * accessOrgId = sbIds.substring(0, sbIds.length() -
							 * 1);
							 */
							accId.setOrgAccessId(orgAccIds);
							depCompetenceTable.remoeAll();
							for (int i = 0; i < listOrgAccess.size(); i++) {
								JecnTreeBean jecnTreeBean = listOrgAccess.get(i);
								Vector vec = new Vector();
								vec.add(0, jecnTreeBean.getId());
								vec.add(1, jecnTreeBean.getName());
								vec.add(2, new JCheckBox(JecnProperties.getValue("downLoadAccess"), false));
								((DefaultTableModel) depCompetenceTable.getModel()).addRow(vec);
							}
						} else {
							accId.setOrgAccessId(new ArrayList<AccessId>());
							depCompetenceTable.remoeAll();
						}
					}
				}
			});

			// 岗位权限
			jLabelPosAccess = new JLabel(JecnProperties.getValue("posCompetenceC"));
			// 岗位权限显示表格
			posCompetenceTable = new PosCompetenceTable(listPosAccess, isShowDownLoad());
			posCompetenceScrollPane = new JScrollPane();
			posCompetenceScrollPane.setViewportView(posCompetenceTable);
			posCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 选择按钮
			jButtonPosAccess = new JButton(JecnProperties.getValue("selectBtn"));
			jButtonPosAccess.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent evt) {
					// 岗位权限选择框
					PositionChooseDialog positionChooseDialog = new PositionChooseDialog(listPosAccess,true);
					positionChooseDialog.setVisible(true);
					if (positionChooseDialog.isOperation()) {
						if (listPosAccess != null && listPosAccess.size() > 0) {
							/* StringBuffer sbIds = new StringBuffer(); */
							StringBuffer sbNames = new StringBuffer();
							List<AccessId> posAccIds = new ArrayList<AccessId>();
							for (JecnTreeBean o : listPosAccess) {
								AccessId accId = new AccessId();
								accId.setAccessId(o.getId());
								posAccIds.add(accId);
								/* sbIds.append(o.getId() + ","); */
								sbNames.append(o.getName() + "/");
							}
							/*
							 * accessPosId = sbIds.substring(0, sbIds.length() -
							 * 1);
							 */
							accId.setPosAccessId(posAccIds);
							posCompetenceTable.remoeAll();
							for (int i = 0; i < listPosAccess.size(); i++) {
								JecnTreeBean jecnTreeBean = listPosAccess.get(i);
								Vector vec = new Vector();
								vec.add(0, jecnTreeBean.getId());
								vec.add(1, jecnTreeBean.getName());
								vec.add(2, new JCheckBox(JecnProperties.getValue("downLoadAccess"), false));
								((DefaultTableModel) posCompetenceTable.getModel()).addRow(vec);
							}
						} else {
							accId.setPosAccessId(new ArrayList<AccessId>());
							posCompetenceTable.remoeAll();
						}
					}

				}
			});
			// 岗位组权限posGroupCompetenceTable
			jLabelPosGroupAccess = new JLabel(JecnProperties.getValue("posCroupCompetenceC"));
			// 岗位权限显示表格
			posGroupCompetenceTable = new PosGroupCompetenceTable(listPosGroupAccess, isShowDownLoad());
			posGroupCompetenceScrollPane = new JScrollPane();
			posGroupCompetenceScrollPane.setViewportView(posGroupCompetenceTable);
			posGroupCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 选择按钮
			jButtonPosGroupAccess = new JButton(JecnProperties.getValue("selectBtn"));
			jButtonPosGroupAccess.addActionListener(new java.awt.event.ActionListener() {

				public void actionPerformed(java.awt.event.ActionEvent evt) {
					// 岗位权限选择框
					PosGropChooseDialog posGroupChooseDialog = new PosGropChooseDialog(listPosGroupAccess,true);
					posGroupChooseDialog.setVisible(true);
					if (posGroupChooseDialog.isOperation()) {
						if (listPosGroupAccess != null && listPosGroupAccess.size() > 0) {
							/* StringBuffer sbIds = new StringBuffer(); */
							StringBuffer sbNames = new StringBuffer();
							List<AccessId> posGAccIds = new ArrayList<AccessId>();
							for (JecnTreeBean o : listPosGroupAccess) {
								AccessId accId = new AccessId();
								accId.setAccessId(o.getId());
								posGAccIds.add(accId);
								/* sbIds.append(o.getId() + ","); */
								sbNames.append(o.getName() + "/");
							}
							/*
							 * accessPosGroupId = sbIds.substring(0,
							 * sbIds.length() - 1);
							 */
							accId.setPosGroupAccessId(posGAccIds);
							posGroupCompetenceTable.remoeAll();
							for (int i = 0; i < listPosGroupAccess.size(); i++) {
								JecnTreeBean jecnTreeBean = listPosGroupAccess.get(i);
								Vector vec = new Vector();
								vec.add(0, jecnTreeBean.getId());
								vec.add(1, jecnTreeBean.getName());
								vec.add(2, new JCheckBox(JecnProperties.getValue("downLoadAccess"), false));
								((DefaultTableModel) posGroupCompetenceTable.getModel()).addRow(vec);
							}
						} else {
							accId.setPosGroupAccessId(new ArrayList<AccessId>());
							posGroupCompetenceTable.remoeAll();
						}
					}

				}
			});

			// 部门权限布局显示
			c = new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(jLabelOrgAccess, c);
			c = new GridBagConstraints(1, 10, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			infoPanel.add(depCompetenceScrollPane, c);
			c = new GridBagConstraints(3, 10, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(jButtonOrgAccess, c);
			if (this.jecnTaskApplication.getIsAccess() == 2) {
				c = new GridBagConstraints(5, 10, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			// 岗位权限布局显示
			c = new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(jLabelPosAccess, c);
			c = new GridBagConstraints(1, 11, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			infoPanel.add(posCompetenceScrollPane, c);
			c = new GridBagConstraints(3, 11, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(jButtonPosAccess, c);
			if (this.jecnTaskApplication.getIsAccess() == 2) {
				c = new GridBagConstraints(5, 11, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			// 岗位组权限布局显示
			c = new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(jLabelPosGroupAccess, c);
			c = new GridBagConstraints(1, 12, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			infoPanel.add(posGroupCompetenceScrollPane, c);
			c = new GridBagConstraints(3, 12, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(jButtonPosGroupAccess, c);
			if (this.jecnTaskApplication.getIsAccess() == 2) {
				c = new GridBagConstraints(5, 12, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}

			hiddenOrShowAccessComponents();
		}

		if (this.jecnTaskApplication.getIsDefinition() != 0) {
			this.infoPanelHeight += 40;
			// 术语定义
			jLabelDefinition = new JLabel(JecnProperties.getValue("flowNameDefinC"));
			jTextAreaDefinition = new JTextArea();
			jTextAreaDefinition.setLineWrap(true);
			sp = new JScrollPane(jTextAreaDefinition);
			jTextAreaDefinition.setBorder(null);
			sp.setBorder(BorderFactory.createLineBorder(Color.gray));
			sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			c = new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(jLabelDefinition, c);
			c = new GridBagConstraints(1, 13, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			infoPanel.add(sp, c);
			c = new GridBagConstraints(3, 13, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			if (this.jecnTaskApplication.getIsDefinition() == 2) {
				c = new GridBagConstraints(3, 13, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			// infoPanel.add(definitionRequiredLab, c);
		}
		// TODO 流程审批人选项是否必填 1:必填；0：非必填
		if (appUserItemList == null && appUserItemList.size() == 0) {
			return;
		}
		String chinaColon = JecnProperties.getValue("chinaColon");
		int rows = 14;
		boolean isShow = true;
		for (int i = 0; i < listOrder.size(); i++) {// 根据审批顺序显示
			TaskStateEnum stateEnum = listOrder.get(i);
			JecnTaskStage taskStage = jecnTaskStageMap.get(stateEnum);
			int languageType = JecnResourceUtil.getLocale() == Locale.CHINESE ? 0 : 1;// 获取语言类型
			String stageName = taskStage.getStageName(languageType) + chinaColon;
			rows++;
			// 文控审核人
			if (TaskStateEnum.a1.equals(stateEnum) && taskStage.getIsShow() == 1) {
				taskAppDocCtrlUserCommon = new PeopleSelectCommon(rows, infoPanel, insets,
						getSelectTreeByMap(stateEnum), this, stageName, isRequest(taskStage.getIsEmpty()));
				disableButton(taskAppDocCtrlUserCommon);
				if (isControl) {
					isShow = false;
				}
			} // 部门审核人
			else if (isShow && TaskStateEnum.a2.equals(stateEnum) && taskStage.getIsShow() == 1) {
				taskAppDeptUserCommon = new PeopleSelectCommon(rows, infoPanel, insets, getSelectTreeByMap(stateEnum),
						this, stageName, isRequest(taskStage.getIsEmpty()));
				disableButton(taskAppDeptUserCommon);
			} else if (isShow && TaskStateEnum.a3.equals(stateEnum) && taskStage.getIsShow() == 1) {// 评审人
				taskAppReviewerCommon = new PeopleMutilSelectCommon(rows, infoPanel, insets,
						getMutilSelectTreeByMap(stateEnum), stageName, this, isRequest(taskStage.getIsEmpty()));
				disableMutilButton(taskAppReviewerCommon);
			} else if (isShow && TaskStateEnum.a4.equals(stateEnum) && taskStage.getIsShow() == 1) {// 批准人
				taskAppPzhUserCommon = new PeopleSelectCommon(rows, infoPanel, insets, getSelectTreeByMap(stateEnum),
						this, stageName, isRequest(taskStage.getIsEmpty()));
				disableButton(taskAppPzhUserCommon);
			} else if (isShow && TaskStateEnum.a5.equals(stateEnum) && taskStage.getIsShow() == 1) {// 各业务体系负责人
				taskAppOprtUserCommon = new PeopleSelectCommon(rows, infoPanel, insets, getSelectTreeByMap(stateEnum),
						this, stageName, isRequest(taskStage.getIsEmpty()));
				disableButton(taskAppOprtUserCommon);
			} else if (isShow && TaskStateEnum.a6.equals(stateEnum) && taskStage.getIsShow() == 1) {// IT总监
				taskAppITUserCommon = new PeopleSelectCommon(rows, infoPanel, insets, getSelectTreeByMap(stateEnum),
						this, stageName, isRequest(taskStage.getIsEmpty()));
				disableButton(taskAppITUserCommon);
			} else if (isShow && TaskStateEnum.a7.equals(stateEnum) && taskStage.getIsShow() == 1) {// 事业部经理
				taskAppDivisionManagerCommon = new PeopleSelectCommon(rows, infoPanel, insets,
						getSelectTreeByMap(stateEnum), this, stageName, isRequest(taskStage.getIsEmpty()));
				disableButton(taskAppDivisionManagerCommon);
			} else if (isShow && TaskStateEnum.a8.equals(stateEnum) && taskStage.getIsShow() == 1) {// 总经理
				taskAppGeneralManagerCommon = new PeopleSelectCommon(rows, infoPanel, insets,
						getSelectTreeByMap(stateEnum), this, stageName, isRequest(taskStage.getIsEmpty()));
				disableButton(taskAppGeneralManagerCommon);
			} else if (isShow && TaskStateEnum.a9.equals(stateEnum) && taskStage.getIsShow() == 1) {
				mutilSelectCommon9 = new PeopleMutilSelectCommon(rows, infoPanel, insets,
						getMutilSelectTreeByMap(stateEnum), stageName, this, isRequest(taskStage.getIsEmpty()));
				disableMutilButton(mutilSelectCommon9);
			} else if (isShow && TaskStateEnum.a10.equals(stateEnum) && taskStage.getIsShow() == 1) {
				mutilSelectCommon10 = new PeopleMutilSelectCommon(rows, infoPanel, insets,
						getMutilSelectTreeByMap(stateEnum), stageName, this, isRequest(taskStage.getIsEmpty()));
				disableMutilButton(mutilSelectCommon10);
			} else if (isShow && TaskStateEnum.a11.equals(stateEnum) && taskStage.getIsShow() == 1) {
				mutilSelectCommon11 = new PeopleMutilSelectCommon(rows, infoPanel, insets,
						getMutilSelectTreeByMap(stateEnum), stageName, this, isRequest(taskStage.getIsEmpty()));
				disableMutilButton(mutilSelectCommon11);
			} else if (isShow && TaskStateEnum.a12.equals(stateEnum) && taskStage.getIsShow() == 1) {
				mutilSelectCommon12 = new PeopleMutilSelectCommon(rows, infoPanel, insets,
						getMutilSelectTreeByMap(stateEnum), stageName, this, isRequest(taskStage.getIsEmpty()));
				disableMutilButton(mutilSelectCommon12);
			}
		}

		rows++;
		// 根据 各体系配置加载 自定义输入项
		rows = customInputItemComponent(sp, rows, insets, c);
		// 变更说明
		jLabelChangeDescription = new JLabel(JecnProperties.getValue("descofChangeC"));
		jTextAreaChangeDescription = new JTextArea();
		jTextAreaChangeDescription.setLineWrap(true);
		areaChangeDescriptionLab.setForeground(Color.red);
		sp = new JScrollPane(jTextAreaChangeDescription);
		jTextAreaChangeDescription.setBorder(null);
		sp.setBorder(BorderFactory.createLineBorder(Color.gray));
		sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		infoPanel.add(jLabelChangeDescription, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(sp, c);
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NORTHWEST,
				insets, 0, 0);
		infoPanel.add(areaChangeDescriptionLab, c);
		rows++;

		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets5, 0, 0);
		jPanelMain.add(buttonPanel, c);
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// 确定
		jButtonSubmit = new JButton(JecnProperties.getValue("okBtn"));
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets5, 0,
				0);
		buttonPanel.add(jButtonSubmit, c);
		// 取消
		jButtonCancel = new JButton(JecnProperties.getValue("cancelBtn"));
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets5, 0,
				0);
		buttonPanel.add(jButtonCancel, c);

		jButtonSubmit.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				excuteCode();
			}
		});
		jButtonCancel.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cancelCode();
			}
		});
		// this.add(jScrollPaneMain);
		// this.add(jLabelFooter);
		// this.add(jButtonSubmit);
		// this.add(jButtonCancel);

		this.getContentPane().add(jPanelMain);
		this.pack();

		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		if (isRunVersion != 0) {
			jLabelUploadATrialRunReport.setVisible(false);
			jTextFieldUploadATrialRunReport.setVisible(false);
			uploadATrialRunReportLab.setVisible(false);
			jButtonUploadATrialRunReport.setVisible(false);
		}
		/** 联系方式 */
		if (jTextFieldContactWay != null) {
			jTextFieldContactWay.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		}
		/** 开始时间 */
		jTextFieldStartTime.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	private boolean isShowDownLoad() {
		// 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
		switch (taskType) {
		case 0:
		case 4:
			return JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessDownLoad);
		case 1:
			return JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileDownLoad);
		case 2:
		case 3:
			return JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowRuleDownLoad);
		default:
			break;
		}
		return false;
	}

	// 九新药业隐藏 审批项的选择功能根据默认审批人提交审批
	private void disableButton(PeopleSelectCommon dig) {
		if (dig != null && JecnConfigTool.isJiuXinLoginType()
				&& treeNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.processMap) {
			dig.disableButton();
		}
	}

	// 九新药业隐藏 审批项的选择功能根据默认审批人提交审批
	private void disableMutilButton(PeopleMutilSelectCommon dig) {
		if (dig != null && JecnConfigTool.isJiuXinLoginType()
				&& treeNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.processMap) {
			dig.disableButton();
		}
	}

	/**
	 * 根据 各体系配置加载 自定义输入项
	 * 
	 * @param sp
	 * @param rows
	 * @param insets
	 * @param c
	 */
	protected int customInputItemComponent(JScrollPane sp, int rows, Insets insets, GridBagConstraints c) {

		// 自定义输入项1
		if (this.jecnTaskApplication.getIsCustomOne() != 0) {
			customInputItemOne = new JLabel(isCustomOneName);
			customInputItemOneText = new JTextArea();
			customInputItemOneText.setLineWrap(true);
			sp = new JScrollPane(customInputItemOneText);
			customInputItemOneText.setBorder(null);
			sp.setBorder(BorderFactory.createLineBorder(Color.gray));
			sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(customInputItemOne, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			infoPanel.add(sp, c);
			if (this.jecnTaskApplication.getIsCustomOne() == 2) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}

		// 自定义输入项2
		if (this.jecnTaskApplication.getIsCustomTwo() != 0) {
			customInputItemTwo = new JLabel(isCustomTwoName);
			customInputItemTwoText = new JTextArea();
			customInputItemTwoText.setLineWrap(true);
			sp = new JScrollPane(customInputItemTwoText);
			customInputItemTwoText.setBorder(null);
			sp.setBorder(BorderFactory.createLineBorder(Color.gray));
			sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(customInputItemTwo, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			infoPanel.add(sp, c);
			if (this.jecnTaskApplication.getIsCustomTwo() == 2) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
		// 自定义输入项3
		if (this.jecnTaskApplication.getIsCustomThree() != 0) {
			customInputItemThree = new JLabel(isCustomThreeName);
			customInputItemThreeText = new JTextArea();
			customInputItemThreeText.setLineWrap(true);
			sp = new JScrollPane(customInputItemThreeText);
			customInputItemThreeText.setBorder(null);
			sp.setBorder(BorderFactory.createLineBorder(Color.gray));
			sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(customInputItemThree, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			infoPanel.add(sp, c);
			if (this.jecnTaskApplication.getIsCustomThree() == 2) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, insets, 0, 0);
				infoPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
		return rows;
	}

	private void initVersion() {
		if (!JecnConfigTool.autoVersionNumber()) {
			if (!JecnConfigTool.isMengNiuOperaType()) {
				return;
			}
			String code = treeNode.getJecnTreeBean().getNumberId();
			if (StringUtils.isBlank(code)) {
				return;
			}
			String[] str = code.split("-");
			if (str.length > 2) {
				String version = str[str.length - 1];
				String year = str[str.length - 2];
				jTextFieldVersion.setText(year + "-" + version);
				jTextFieldVersion.setEditable(false);
			}
		} else {
			JecnVersionUtil.initVersion(jTextFieldVersion, treeNode.getJecnTreeBean(), taskType);
		}
	}

	/**
	 * 流程类别 jComboBoxClassify
	 */
	protected void addComboxItem() {
		if (jecnTaskApplication.getFileType() == 0) {
			return;
		}
		// 动态读取流程类型
		try {
			listTypeBean = ConnectionPool.getProcessRuleTypeAction().getFlowAttributeType();
			// 清空流程类别
			jComboBoxClassify.removeAllItems();
			jComboBoxClassify.addItem("");
			for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
				// 本流程对应的流程类型ID 与流程类别中的ID相等，设置该流程类别选中
				String selectItem = proceeRuleTypeBean.getTypeName();
				String selectIndex = proceeRuleTypeBean.getTypeId().toString();
				// 将流程类型显示到JCombox中
				jComboBoxClassify.addItem(selectItem);
				if (selectIndex.equals(typeIds)) {// 流程类别或制度类别存在
					jComboBoxClassify.setSelectedItem(selectItem);
				}
			}
		} catch (Exception e1) {
			log.error("JecnTaskJDialog addComboxItem is error", e1);
		}
	}

	/**
	 * 两个日期之间的天数
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	protected int daysOfTwoDate(Date beginDate, Date endDate) {
		int days = 0;// 两个日期之间的天数
		Calendar beginCalendar = Calendar.getInstance();
		Calendar endCalendar = Calendar.getInstance();

		beginCalendar.setTime(beginDate);
		endCalendar.setTime(endDate);
		// 计算天数
		while (beginCalendar.before(endCalendar)) {
			days++;
			beginCalendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		return days;
	}

	private void isChekApproveType() {
		jTextFieldVersion.setText(JecnCommon.getUUID());
	}

	/**
	 * 提交按钮
	 * 
	 */
	protected void excuteCode() {
		/**
		 * 检查是否是 废止 废止 下自动生成版本号
		 */
		if (approveType == 2) {
			isChekApproveType();
		}

		/** 检查必填项是否填写 */
		if (!isCheckSubmit()) {
			return;
		}

		/**
		 * 非必填项填写规范检查
		 */
		if (!isCheckNotMustSubmit()) {
			return;
		}

		/** 检查版本号存不存在 */
		if (!checkRevsion()) {
			return;
		}
		/** 检查填写的审批人合不合理 */
		if (!checkApprover()) {
			return;
		}
		jButtonSubmit.setEnabled(false);
		// 提交任务
		SubmitTaskExcute submitTaskExcute = new SubmitTaskExcute();
		String error = JecnLoading.start(submitTaskExcute);
		if (error != null) {
			jButtonSubmit.setEnabled(true);
			return;
		}
		try {
			String s = submitTaskExcute.get();
			if ("false".equals(s)) {
				jButtonSubmit.setEnabled(true);
				return;
			}

			// 如果现实编号，更新树节点编号
			if (this.jecnTaskApplication.getIsFlowNumber() != 0) {
				treeNode.getJecnTreeBean().setNumberId(jTextFieldNumber.getText());
			}
			treeNode.getJecnTreeBean().setApproveType(approveType);
			// 刷新节点
			JecnTreeCommon.reNameNode(jTree, treeNode, treeNode.getJecnTreeBean().getName());
		} catch (Exception e) {
			log.error("JecnTaskJDialog excuteCode is error.", e);
			jButtonSubmit.setEnabled(true);
		}
		/** 提交时保持默认值 */
		writeTeskRequiredValue();
		this.dispose();
	}

	/**
	 * @author yxw 2012-8-10
	 * @description：面板保存进度控制线程
	 */
	class SubmitTaskExcute extends SwingWorker<String, Void> {
		@Override
		protected String doInBackground() throws Exception {
			// 提交任务
			boolean falg = submitTask();
			if (falg) {
				return "true";
			} else {
				return "false";
			}
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	private void writeTeskRequiredValue() {
		if (JecnConfigTool.isFiberhome()) {
			return;
		}
		Properties prop = redTaskDefaultValueProperties();
		if (prop == null) {
			return;
		}
		try {
			// 文件、制度、流程
			OutputStream fos = writePropertTaskRequiredValue();
			for (TaskStateEnum stateEnum : listOrder) {
				switch (stateEnum) {
				case a1:
					/** 文控审核人 */
					prop.setProperty("com.jecn.task.bean.recordAuditPeopleId", this.auditId);
					if (this.jecnTaskApplication.getIsControlAuditLead() != 0) {
						break;
					}
					break;
				case a2:
					prop.setProperty("com.jecn.task.bean.deptAuditPeopleIds", this.deptPeopleId);
					break;
				case a3:
					prop.setProperty("com.jecn.task.bean.reviewPeopleId", this.reviewId);

					break;
				case a4:
					prop.setProperty("com.jecn.task.bean.approvePeopleId", this.approveId);

					break;
				case a5:
					prop.setProperty("com.jecn.task.bean.operationPeopleId", this.bussId);

					break;
				case a6:
					prop.setProperty("com.jecn.task.bean.itPeopleId", this.itId);
					break;
				case a7:
					prop.setProperty("com.jecn.task.bean.divManPeopleId", this.divisionManId);
					break;
				case a8:
					prop.setProperty("com.jecn.task.bean.genManPeopleIds", this.generalManId);
					break;
				case a9:
					prop.setProperty("customApprovaId1", this.customApprovaId1);
					break;
				case a10:
					prop.setProperty("customApprovaId2", this.customApprovaId2);
					break;
				case a11:
					prop.setProperty("customApprovaId3", this.customApprovaId3);
				case a12:
					prop.setProperty("customApprovaId4", this.customApprovaId4);
					break;
				default:
					break;
				}
			}
			prop.store(fos, null);
			fos.close();
		} catch (IOException ex) {
			log.error("JecnTaskJDialog writeTeskRequiredValue is error!", ex);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			return;
		}
	}

	/** 提交任务 */
	protected abstract boolean submitTask();

	/** 取消 */
	protected void cancelCode() {
		if (isJDialogChange()) {
			int flag = JecnOptionPane.showConfirmDialog(this, TaskApproveConstant.y_n_exit, TaskApproveConstant.prompt,
					JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
			if (JecnOptionPane.NO_OPTION == flag) {
				return;
			}
		}
		this.dispose();
	}

	private boolean numIsMustWrite() {
		JecnConfigItemBean showFlowConfig = null;
		// 流程基本配置
		IJecnConfigItemAciton configAciton = ConnectionPool.getConfigAciton();
		if (taskType == 0) {
			showFlowConfig = configAciton.selectConfigItemBean(1, "189");
		} else if (taskType == 4) {
			showFlowConfig = JecnConfigTool.getItemBean(ConfigItemPartMapMark.isShowProcessMapNum);
		}
		// 如果必填
		if (showFlowConfig != null && showFlowConfig.getIsEmpty() != null && 1 == showFlowConfig.getIsEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * 检查必填项是否填写
	 * 
	 * @return true 验证通过,false :验证失败
	 */
	protected boolean isCheckSubmit() {
		// 任务名称
		if (TaskCommon.checTextName(this, jTextFieldTaskName.getText(), jLabelTaskName)) {
			return false;
		}
		// 联系方式
		if (jecnTaskApplication.getIsPhone() == 2) {
			if ("".equals(this.jTextFieldContactWay.getText().trim())) {
				JecnOptionPane.showMessageDialog(this, JecnUtil.mustTip(jLabelContactWay));
				return false;
			}
		}

		/** 开始时间 */
		if ("".equals(this.jTextFieldStartTime.getText())) {
			JecnOptionPane.showMessageDialog(this, TaskApproveConstant.startTimeIsNotNull);
			return false;
		}
		/** 结束时间 */
		if ("".equals(this.jTextFieldEndTime.getText())) {
			JecnOptionPane.showMessageDialog(this, TaskApproveConstant.endTimeIsNotNull);
			return false;
		}
		/**
		 * 版本号发布类型  废止 没有版本号
		 */
		if (approveType != 2 && JecnConfigTool.autoVersionNumber() && JecnConfigTool.isShowItem(ConfigItemPartMapMark.enableSmallVersion)) {
			if ((JecnVersionUtil.getLargeVersionCheckBox() != null && !JecnVersionUtil.getLargeVersionCheckBox()
					.isSelected())
					&& (JecnVersionUtil.getSmallVersionCheckBox() != null && !JecnVersionUtil.getSmallVersionCheckBox()
							.isSelected())) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pleaseVersionNumber"));
				return false;

			}
		}
		/** 版本号 */
		if (TaskCommon.checTextName(this, jTextFieldVersion.getText(), jLabelVersion)) {
			return false;
		}

		// 试运行
		if (this.isRunVersion != 0) {// 选择试运行
			if (jComboBoxRunVersion.getSelectedIndex() == 0) {
				if ("".equals(this.jTextFieldRunTime.getText())) {
					JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("testRunTimeIsNotNull"));
					return false;
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				int days;
				try {
					days = daysOfTwoDate(sdf.parse(jTextFieldStartTime.getText()), sdf.parse(jTextFieldRunTime
							.getText()));
					if (days < runTimeValue) {
						JecnOptionPane.showMessageDialog(this, TaskApproveConstant.testRunTimeLess + runTimeValue
								+ TaskApproveConstant.day);
						return false;
					}
				} catch (Exception ex) {
					log.error("JecnTaskJDialog is error", ex);
					JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
					return false;
				}
			}
			if (jComboBoxRunVersion.getSelectedIndex() != 0) {
				if ("".equals(jTextFieldUploadATrialRunReport.getText())) {
					JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("reportIsNull"));
					return false;
				}
			}
		}
		// 开始时间不能大于结束时间
		if (JecnTool.getDateByString(jTextFieldEndTime.getText()).before(
				JecnTool.getDateByString(jTextFieldStartTime.getText()))) {
			JecnOptionPane.showMessageDialog(this, TaskApproveConstant.sTimeNotGreaterETime);
			return false;
		}

		// 权限范围（密级别）
		if (jecnTaskApplication.getIsPublic() == 2) {
			if ("".equals(this.jComboBoxSecurityClassification.getSelectedItem())) {
				JecnOptionPane.showMessageDialog(this, JecnUtil.mustTip(jLabelSecurityClassification));
				return false;
			}
		}

		// 流程类型
		if (jecnTaskApplication.getFileType() == 2) {
			if ("".equals(this.jComboBoxClassify.getSelectedItem()) || this.jComboBoxClassify.getSelectedItem() == null) {
				JecnOptionPane.showMessageDialog(this, JecnUtil.mustTip(jLabelClassify));
				return false;
			}
		}

		/** 编号 如果显示 */
		if (this.jecnTaskApplication.getIsFlowNumber() != 0 && jTextFieldNumber != null) {// 是否显示编号
			// 如果必填
			if (numIsMustWrite()) {
				if (TaskCommon.checTextName(this, jTextFieldNumber.getText(), jLabelNumber)) {
					return false;
				}
			}
			// 编号
			if (TaskCommon.checkNameContainsNull(this, jTextFieldNumber, jLabelNumber)) {
				return false;
			}
		}
		// 查阅权限
		if (jecnTaskApplication.getIsAccess() == 2) {
			if (accId != null && (accId.getOrgAccessId() == null || accId.getOrgAccessId().isEmpty())
					&& (accId.getPosAccessId() == null || accId.getPosAccessId().isEmpty())
					&& (accId.getPosGroupAccessId() == null || accId.getPosGroupAccessId().isEmpty())) {
				JecnOptionPane.showMessageDialog(this, JecnUtil.mustTip(JecnProperties.getValue("right")));
				return false;
			}
		}

		// 术语定义
		if (jecnTaskApplication.getIsDefinition() == 2) {
			if ("".equals(this.jTextAreaDefinition.getText().trim())) {
				JecnOptionPane.showMessageDialog(this, JecnUtil.mustTip(jLabelDefinition));
				return false;
			}
		}
		// 自定义输入项 1
		if (jecnTaskApplication.getIsCustomOne() == 2) {
			if ("".equals(this.customInputItemOneText.getText().trim())) {
				JecnOptionPane.showMessageDialog(this, JecnUtil.mustTip(isCustomOneName));
				return false;
			}
		}
		// 自定义输入项 2
		if (jecnTaskApplication.getIsCustomTwo() == 2) {
			if ("".equals(this.customInputItemTwoText.getText().trim())) {
				JecnOptionPane.showMessageDialog(this, JecnUtil.mustTip(isCustomTwoName));
				return false;
			}
		}
		// 自定义输入项 3
		if (jecnTaskApplication.getIsCustomThree() == 2) {
			if ("".equals(this.customInputItemThreeText.getText().trim())) {
				JecnOptionPane.showMessageDialog(this, JecnUtil.mustTip(isCustomThreeName));
				return false;
			}
		}
		// 变更说明
		if (TaskCommon.checkAreaName(this, jTextAreaChangeDescription, jLabelChangeDescription)) {
			return false;
		}
		return true;
	}

	/**
	 * 检查非必填项填写规范
	 * 
	 * @return true 验证通过,false :验证失败
	 */
	protected boolean isCheckNotMustSubmit() {

		// 流程地图的术语定义不可以超过1200个字符而流程图不做限制
		if (4 == taskType && null != jTextAreaDefinition && null != jLabelDefinition) {
			// 如果没有填写或者填写的都是空格则不验证
			if (DrawCommon.isNullOrEmtryTrim(jTextAreaDefinition.getText())) {
				return true;
			}
			if (TaskCommon.checkAreaName(this, jTextAreaDefinition, jLabelDefinition)) {
				return false;
			}

		}
		/***
		 * 自定义输入项一长度验证
		 */
		if (customInputItemOneText != null) {
			// 如果没有填写或者填写的都是空格则不验证
			if (DrawCommon.isNullOrEmtryTrim(customInputItemOneText.getText())) {
				return true;
			}
			if (TaskCommon.checkAreaName(this, customInputItemOneText, customInputItemOne)) {
				return false;
			}
		}
		/***
		 * 自定义输入项二长度验证
		 */
		if (customInputItemTwoText != null) {
			// 如果没有填写或者填写的都是空格则不验证
			if (DrawCommon.isNullOrEmtryTrim(customInputItemTwoText.getText())) {
				return true;
			}
			if (TaskCommon.checkAreaName(this, customInputItemTwoText, customInputItemTwo)) {
				return false;
			}
		}
		/***
		 * 自定义输入项三长度验证
		 */
		if (customInputItemThreeText != null) {
			// 如果没有填写或者填写的都是空格则不验证
			if (DrawCommon.isNullOrEmtryTrim(customInputItemThreeText.getText())) {
				return true;
			}
			if (TaskCommon.checkAreaName(this, customInputItemThreeText, customInputItemThree)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 检查界面是否修改，防止填写直接关闭
	 * 
	 * @return
	 */
	protected boolean isJDialogChange() {
		// 任务名称
		if (!jTextFieldTaskName.getText().equals(taskName)) {
			return true;
		}
		// 版本号
		if (!version.equals(this.jTextFieldVersion.getText())) {
			return true;
		}
		// 属于定义
		if (this.jecnTaskApplication.getIsDefinition() != 0) {
			if (!jTextAreaDefinition.getText().equals(definition)) {
				return true;
			}
		}
		// 变更说明
		if (!jTextAreaChangeDescription.getText().equals(changeDescription)) {
			return true;
		}
		return false;
	}

	/** 检查版本号存不存 */
	protected abstract boolean checkRevsion();

	/**
	 * 判断审核人顺序
	 * 
	 * @param jecnTaskConfigItemBeanList
	 * @param order
	 */
	private void setCheckerOrder() {
		int sort = 0;
		JecnTaskStage taskStage = new JecnTaskStage();
		taskStage.setIsEmpty(1);
		taskStage.setIsSelectedUser(1);
		taskStage.setSort(sort);
		/**
		 * 默认值不可更改 所以不加国际化
		 */
		taskStage.setStageName("拟稿人");
		taskStage.setEnName("Originator");
		taskStage.setStageMark(0);
		taskStage.setIsShow(1);
		jecnTaskStageMap.put(TaskStateEnum.a0, taskStage);
		for (TaskConfigItem itemBean : appUserItemList) {
			if ("taskAppPubType".equals(itemBean.getMark())) {
				if ("1".equals(itemBean.getValue())) {
					this.jecnTaskApplication.setIsControlAuditLead(1);
				}
			}
		}
		for (TaskConfigItem itemBean : appUserItemList) {
			String mark = itemBean.getMark();
			// 文控审核人
			taskStage = new JecnTaskStage();
			if ("taskAppDocCtrlUser".equals(mark)) {
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(1);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a1);
				}
				jecnTaskStageMap.put(TaskStateEnum.a1, taskStage);
			} else if ("taskAppDeptUser".equals(mark)) {// 部门审核人
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(2);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a2);
				}
				jecnTaskStageMap.put(TaskStateEnum.a2, taskStage);
			} else if ("taskAppReviewer".equals(mark)) {// 评审人
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(3);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a3);
				}
				jecnTaskStageMap.put(TaskStateEnum.a3, taskStage);

				sort++;
				/**
				 * 整理意见
				 */
				taskStage = new JecnTaskStage();
				taskStage.setIsEmpty(itemBean.getIsEmpty());
				taskStage.setSort(sort);
				// taskStage.setStageName(JecnProperties.getValue("finishingViews"));
				taskStage.setStageName("整理意见");
				taskStage.setEnName("Complete all reviews");
				taskStage.setStageMark(10);
				taskStage.setIsShow(0);
				jecnTaskStageMap.put(null, taskStage);
			} else if ("taskAppPzhUser".equals(mark)) {// 批准人
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(4);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a4);
				}
				jecnTaskStageMap.put(TaskStateEnum.a4, taskStage);
			} else if ("taskAppOprtUser".equals(mark)) {// 各业务体系负责人
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(6);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a5);
				}
				jecnTaskStageMap.put(TaskStateEnum.a5, taskStage);
			} else if ("taskAppITUser".equals(mark)) { // IT总监
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(7);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a6);
				}
				jecnTaskStageMap.put(TaskStateEnum.a6, taskStage);
			} else if ("taskAppDivisionManager".equals(mark)) {
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(8);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a7);
				}
				jecnTaskStageMap.put(TaskStateEnum.a7, taskStage);
			} else if ("taskAppGeneralManager".equals(mark)) {
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(9);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a8);
				}
				jecnTaskStageMap.put(TaskStateEnum.a8, taskStage);
			} else if (ConfigItemPartMapMark.taskCustomApproval1.toString().equals(mark)) {// 自定义
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(11);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a9);
				}
				jecnTaskStageMap.put(TaskStateEnum.a9, taskStage);
			} else if (ConfigItemPartMapMark.taskCustomApproval2.toString().equals(mark)) {
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(12);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a10);
				}
				jecnTaskStageMap.put(TaskStateEnum.a10, taskStage);
			} else if (ConfigItemPartMapMark.taskCustomApproval3.toString().equals(mark)) {
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(13);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a11);
				}
				jecnTaskStageMap.put(TaskStateEnum.a11, taskStage);
			} else if (ConfigItemPartMapMark.taskCustomApproval4.toString().equals(mark)) {
				sort++;
				setJecnTaskStageBaseInfo(taskStage, itemBean, sort);
				taskStage.setStageMark(14);
				if (taskStage.getIsShow() == 1) {
					listOrder.add(TaskStateEnum.a12);
				}
				jecnTaskStageMap.put(TaskStateEnum.a12, taskStage);
			}
		}
	}

	/**
	 * 为任务阶段信息赋值
	 * 
	 * @author weidp
	 * @date 2014-7-1 下午04:12:22
	 * @param taskStage
	 * @param itemBean
	 * @param afterDocShow
	 */
	private void setJecnTaskStageBaseInfo(JecnTaskStage taskStage, TaskConfigItem itemBean, Integer sort) {
		taskStage.setIsShow(Integer.valueOf(itemBean.getValue()));
		taskStage.setSort(sort);
		taskStage.setStageName(itemBean.getName());
		taskStage.setEnName(itemBean.getEnName());
		taskStage.setIsEmpty(itemBean.getIsEmpty());
	}

	/**
	 * 读取任务配置
	 */
	private void readRequests() {
		// 必填项读取
		if (appUserItemList == null || showItemList == null) {
			return;
		}
		/** 配置界面显示 */
		for (JecnConfigItemBean itemBean : showItemList) {
			String isShow = ((itemBean.getIsEmpty() != null && itemBean.getIsEmpty() == 0) || "0".equals(itemBean
					.getValue())) ? itemBean.getValue() : "2";
			if ("0".equals(isShow)) {// 不是显示项跳出当前循环
				continue;
			}
			if ("taskShowPublic".equals(itemBean.getMark())) {
				// 密级
				this.jecnTaskApplication.setIsPublic(Integer.parseInt(isShow));
			} else if ("taskShowFileType".equals(itemBean.getMark())) {
				// 文件类别
				this.jecnTaskApplication.setFileType(Integer.parseInt(isShow));
			} else if ("taskShowViewPerm".equals(itemBean.getMark())) {
				// 查阅权限
				this.jecnTaskApplication.setIsAccess(Integer.valueOf(isShow));
			} else if ("taskShowDefinition".equals(itemBean.getMark())) {
				// 术语定义
				this.jecnTaskApplication.setIsDefinition(Integer.valueOf(isShow));
			} else if ("taskShowPhone".equals(itemBean.getMark())) {
				// 联系方式
				this.jecnTaskApplication.setIsPhone(Integer.valueOf(isShow));
			} else if ("taskShowNumber".equals(itemBean.getMark())) {
				// 流程编号
				this.jecnTaskApplication.setIsFlowNumber(Integer.valueOf(isShow));
			} else if ("taskShowTestRun".equals(itemBean.getMark())) {
				// 试运行
				isRunVersion = Integer.valueOf(isShow);
			} else if ("taskShowRunCycle".equals(itemBean.getMark())) {
				if (isRunVersion != 0) {// 显示试运行
					// 试运行周期
					runTimeValue = Integer.valueOf(isShow);
				}
			} else if ("taskShowRunRealseCycle".equals(itemBean.getMark())) {
				if (isRunVersion != 0) {// 显示试运行
					// 试运行发送报告周期
					sendRunTimeValue = Integer.valueOf(isShow);
				}
			} else if ("isShowCustomOneInput".equals(itemBean.getMark())) {
				// 自定义输入项1
				this.jecnTaskApplication.setIsCustomOne(Integer.valueOf(isShow));
				isCustomOneName = itemBean.getName(JecnResourceUtil.getLocale());
			} else if ("isShowCustomTwoInput".equals(itemBean.getMark())) {
				// 自定义输入项2
				this.jecnTaskApplication.setIsCustomTwo(Integer.valueOf(isShow));
				isCustomTwoName = itemBean.getName(JecnResourceUtil.getLocale());
			} else if ("isShowCustomThreeInput".equals(itemBean.getMark())) {
				// 自定义输入项3
				this.jecnTaskApplication.setIsCustomThree(Integer.valueOf(isShow));
				isCustomThreeName = itemBean.getName(JecnResourceUtil.getLocale());
			}

		}
	}

	/**
	 * 检查相邻审批环节是否存在相同人员
	 * 
	 * @param auditPeopleId
	 *            上一审批环节审批人
	 * @param approverArr
	 *            当前审批环节审批人集合
	 * @param name
	 *            当前验证的审批环节阶段名称
	 * @param lableName
	 *            上一个审批环节阶段名称
	 * @return 0验证通过；1：存在相同审批人；2：上一个审批环节不存在审批人
	 */
	protected int checkSameName(String auditPeopleId, String[] approverArr, String name, String lableName) {
		if (DrawCommon.isNullOrEmtryTrim(auditPeopleId)) {// 人员不存在
			return 2;
		}
		// 存在相同的人员！
		String strSamePeople = JecnProperties.getValue("isExitsPeople");
		String[] upStrArr = auditPeopleId.split(",");
		for (String strApprover : approverArr) {
			for (String upStr : upStrArr) {
				if (strApprover.equals(upStr)) {
					// "与"
					JecnOptionPane.showMessageDialog(this, name + JecnProperties.getValue("and") + lableName
							+ strSamePeople);
					return 1;
				}
			}
		}
		return 0;
	}

	/**
	 * 通过审批标识符号获得阶段
	 * 
	 * @param approveSign
	 * @return
	 */
	protected int getState() {
		for (TaskStateEnum str : listOrder) {// 虚幻审批阶段，获取提交审批后的第一个审批阶段
			if (TaskStateEnum.a1.equals(str) && StringUtils.isNotBlank(auditId)) {
				return 1;
			} else if (TaskStateEnum.a2.equals(str) && StringUtils.isNotBlank(deptPeopleId)) {
				return 2;
			} else if (TaskStateEnum.a3.equals(str) && StringUtils.isNotBlank(reviewId)) {
				return 3;
			} else if (TaskStateEnum.a4.equals(str) && StringUtils.isNotBlank(approveId)) {
				return 4;
			} else if (TaskStateEnum.a5.equals(str) && StringUtils.isNotBlank(bussId)) {
				return 6;
			} else if (TaskStateEnum.a6.equals(str) && StringUtils.isNotBlank(itId)) {
				return 7;
			} else if (TaskStateEnum.a7.equals(str) && StringUtils.isNotBlank(divisionManId)) {
				return 8;
			} else if (TaskStateEnum.a8.equals(str) && StringUtils.isNotBlank(generalManId)) {
				return 9;
			} else if (TaskStateEnum.a9.equals(str) && StringUtils.isNotBlank(customApprovaId1)) {
				return 11;
			} else if (TaskStateEnum.a10.equals(str) && StringUtils.isNotBlank(customApprovaId2)) {
				return 12;
			} else if (TaskStateEnum.a11.equals(str) && StringUtils.isNotBlank(customApprovaId3)) {
				return 13;
			} else if (TaskStateEnum.a12.equals(str) && StringUtils.isNotBlank(customApprovaId4)) {
				return 14;
			}
		}
		return -1;
	}

	/**
	 * 验证各阶段审批环节必填项是否存在审批人
	 * 
	 * @return true 必填项无审批人
	 */
	protected boolean isMustWrite() {
		// 检验拟稿人是否存在
		draftId = taskAppDrafterUserCommon.getStringIdResult();
		if (TaskCommon.checTextNameIsNotNull(this, draftId, taskAppDrafterUserCommon.getJecnLab())) {
			return true;
		}
		for (Entry<TaskStateEnum, JecnTaskStage> entry : jecnTaskStageMap.entrySet()) {
			TaskStateEnum stateEnum = entry.getKey();
			JecnTaskStage stage = entry.getValue();
			if (TaskStateEnum.a1.equals(stateEnum)) {// 文控审核人
				if (taskAppDocCtrlUserCommon != null) {
					auditId = taskAppDocCtrlUserCommon.getStringIdResult();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, auditId, taskAppDocCtrlUserCommon.getJecnLab())) {
						return true;
					}
				}
			} else if (TaskStateEnum.a2.equals(stateEnum)) {// 部门审核人
				if (taskAppDeptUserCommon != null) {
					deptPeopleId = taskAppDeptUserCommon.getStringIdResult();
					// 部门审核人
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, deptPeopleId, taskAppDeptUserCommon.getJecnLab())) {
						return true;
					}
				}
			} else if (TaskStateEnum.a3.equals(stateEnum)) {// 评审人
				if (taskAppReviewerCommon != null) {
					reviewId = taskAppReviewerCommon.getResultIds();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checkAreaNameNotImput(this, reviewId, taskAppReviewerCommon.getJecnLab())) {// 如果必填，验证评审人
						return true;
					}
				}
			} else if (TaskStateEnum.a4.equals(stateEnum)) {// 批准人
				// 批准人
				if (taskAppPzhUserCommon != null) {
					approveId = taskAppPzhUserCommon.getStringIdResult();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, approveId, taskAppPzhUserCommon.getJecnLab())) {
						return true;
					}
				}
			} else if (TaskStateEnum.a5.equals(stateEnum)) {// 各业务体系负责人
				// 各业务体系负责人
				if (taskAppOprtUserCommon != null) {
					bussId = taskAppOprtUserCommon.getStringIdResult();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, bussId, taskAppOprtUserCommon.getJecnLab())) {
						return true;
					}
				}
			} else if (TaskStateEnum.a6.equals(stateEnum)) {// IT总监
				if (taskAppITUserCommon != null) {
					itId = taskAppITUserCommon.getStringIdResult();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, itId, taskAppITUserCommon.getJecnLab())) {
						return true;
					}
				}
			} else if (TaskStateEnum.a7.equals(stateEnum)) {// 事业部经理
				if (taskAppDivisionManagerCommon != null) {
					divisionManId = taskAppDivisionManagerCommon.getStringIdResult();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, divisionManId, taskAppDivisionManagerCommon
									.getJecnLab())) {
						return true;
					}
				}
			} else if (TaskStateEnum.a8.equals(stateEnum)) {// 总经理
				if (taskAppGeneralManagerCommon != null) {
					generalManId = taskAppGeneralManagerCommon.getStringIdResult();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, generalManId, taskAppGeneralManagerCommon
									.getJecnLab())) {
						return true;
					}
				}
			} else if (TaskStateEnum.a9.equals(stateEnum)) {// 自定义
				if (mutilSelectCommon9 != null) {
					customApprovaId1 = mutilSelectCommon9.getResultIds();
					if (stage.getIsEmpty() == 1
							&& TaskCommon
									.checTextNameIsNotNull(this, customApprovaId1, mutilSelectCommon9.getJecnLab())) {
						return true;
					}
				}
			} else if (TaskStateEnum.a10.equals(stateEnum)) {// 自定义
				if (mutilSelectCommon10 != null) {
					customApprovaId2 = mutilSelectCommon10.getResultIds();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, customApprovaId2, mutilSelectCommon10
									.getJecnLab())) {
						return true;
					}
				}
			} else if (TaskStateEnum.a11.equals(stateEnum)) {// 自定义
				if (mutilSelectCommon11 != null) {
					customApprovaId3 = mutilSelectCommon11.getResultIds();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, customApprovaId3, mutilSelectCommon11
									.getJecnLab())) {
						return true;
					}
				}

			} else if (TaskStateEnum.a12.equals(stateEnum)) {// 自定义
				if (mutilSelectCommon12 != null) {
					customApprovaId4 = mutilSelectCommon12.getResultIds();
					if (stage.getIsEmpty() == 1
							&& TaskCommon.checTextNameIsNotNull(this, customApprovaId4, mutilSelectCommon12
									.getJecnLab())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * 检查填写的审批人合不合理
	 */
	private boolean checkApprover() {
		if (isMustWrite()) {// 必填项验证失败
			return false;
		}
		if (checkAllState()) {// 各阶段审核人验证
			return false;
		}

		return true;
	}

	/**
	 * 
	 * 拟稿人是否存在岗位
	 * 
	 * @return true 拟稿人不存在
	 */
	private boolean isExistPos() {
		// 拟稿人是否存在岗位
		Long peopleId = null;
		JecnUser jecnUser = getJecnUser();
		if (jecnUser == null) {
			throw new NullPointerException(JecnProperties.getValue("drafterPost_err"));
		}
		peopleId = jecnUser.getPeopleId();

		try {
			boolean isExsitPos = ConnectionPool.getTaskRecordAction().isExsitPos(peopleId);
			if (!isExsitPos) {// True 存在岗位，false 拟稿人不存在岗位 退出
				// #拟稿人不存在岗位，请修改！
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("drafPeopleNotExit"));
				return true;
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return false;
	}

	private boolean checkUserPos(JecnUser jecnUser) {
		if (jecnUser == null) {
			throw new NullPointerException(JecnProperties.getValue("drafterPost_err"));
		}
		Long peopleId = jecnUser.getPeopleId();

		try {
			boolean isExsitPos = ConnectionPool.getTaskRecordAction().isExsitPos(peopleId);
			if (!isExsitPos) {// True 存在岗位，false 拟稿人不存在岗位 退出
				// #拟稿人不存在岗位，请修改！
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("drafPeopleNotExit"));
				return true;
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return false;
	}

	protected boolean checkAllState() {
		// 是否存在审批人
		boolean isExits = false;
		// 验证审批人是否存在
		for (int count = 0; count < listOrder.size(); count++) {
			TaskStateEnum stateEnum = listOrder.get(count);
			if (TaskStateEnum.a0.equals(stateEnum)) {
				continue;
			} else if (TaskStateEnum.a1.equals(stateEnum)) {// 文控审核人
				if (DrawCommon.isNullOrEmtryTrim(auditId)) {// 审核人不存在 退出当前循环
					continue;
				}
				isExits = true;
				if (this.jecnTaskApplication.getIsControlAuditLead() != 0) {
					return false;
				}
			} else if (TaskStateEnum.a2.equals(stateEnum)) {// 部门审核人
				// 部门审核人
				if (DrawCommon.isNullOrEmtryTrim(deptPeopleId)) {// 审核人不存在
					// 退出当前循环
					continue;
				}
				isExits = true;
			} else if (TaskStateEnum.a3.equals(stateEnum)) {// 评审人
				if (DrawCommon.isNullOrEmtryTrim(reviewId)) {// 评审人不存在 退出当前循环
					continue;
				}
				// 评审人后必须存在审核人
				if (!isNoAuditAfterReview(count)) {
					return true;
				}
				isExits = true;
			} else if (TaskStateEnum.a4.equals(stateEnum)) {// 批准人
				// 批准人
				if (DrawCommon.isNullOrEmtryTrim(approveId)) {// 审核人不存在 退出当前循环
					continue;
				}
				isExits = true;
			} else if (TaskStateEnum.a5.equals(stateEnum)) {// 各业务体系负责人
				// 各业务体系负责人
				if (DrawCommon.isNullOrEmtryTrim(bussId)) {// 审核人不存在 退出当前循环
					continue;
				}
				isExits = true;
			} else if (TaskStateEnum.a6.equals(stateEnum)) {// IT总监
				if (DrawCommon.isNullOrEmtryTrim(itId)) {// 审核人不存在 退出当前循环
					continue;
				}
				isExits = true;
			} else if (TaskStateEnum.a7.equals(stateEnum)) {// 事业部经理
				if (DrawCommon.isNullOrEmtryTrim(divisionManId)) {// 审核人不存在
					// 退出当前循环
					continue;
				}
				isExits = true;
			} else if (TaskStateEnum.a8.equals(stateEnum)) {// 总经理
				if (DrawCommon.isNullOrEmtryTrim(generalManId)) {// 审核人不存在
					// 退出当前循环
					continue;
				}
				isExits = true;
			} else if (TaskStateEnum.a9.equals(stateEnum)) {// 自定义
				if (DrawCommon.isNullOrEmtryTrim(customApprovaId1)) {// 审核人不存在
					// 退出当前循环
					continue;
				}
				isExits = true;
			} else if (TaskStateEnum.a10.equals(stateEnum)) {// 自定义
				if (DrawCommon.isNullOrEmtryTrim(customApprovaId2)) {// 审核人不存在
					// 退出当前循环
					continue;
				}
				isExits = true;
			} else if (TaskStateEnum.a11.equals(stateEnum)) {// 自定义
				if (DrawCommon.isNullOrEmtryTrim(customApprovaId3)) {// 审核人不存在
					// 退出当前循环
					continue;
				}
				isExits = true;
			} else if (TaskStateEnum.a12.equals(stateEnum)) {// 自定义
				if (DrawCommon.isNullOrEmtryTrim(customApprovaId4)) {// 审核人不存在
					// 退出当前循环
					continue;
				}
				isExits = true;
			}
		}
		if (!isExits) {// 必须存在审核人！
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("isMustExits"));
			return true;
		}
		return false;
	}

	/**
	 * 评审人后是否存在审批人
	 * 
	 * @param count
	 *            评审人所在节点
	 * @return true 评审人后存在审批人
	 */
	protected boolean isNoAuditAfterReview(int count) {
		// 评审人后是否存在审批人
		boolean isExistAudit = false;
		for (int i = count + 1; i < listOrder.size(); i++) {
			TaskStateEnum str = listOrder.get(i);
			if (TaskStateEnum.a1.equals(str)) {// 文控审核人
				if (StringUtils.isNotBlank(auditId)) {// 审核人不存在 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a2.equals(str)) {// 部门审核人
				// 部门审核人
				if (StringUtils.isNotBlank(deptPeopleId)) {// 审核人不存在
					// 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a4.equals(str)) {// 批准人
				// 批准人
				if (StringUtils.isNotBlank(approveId)) {// 审核人不存在 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a5.equals(str)) {// 各业务体系负责人
				// 各业务体系负责人
				if (StringUtils.isNotBlank(bussId)) {// 审核人不存在 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a6.equals(str)) {// IT总监
				if (StringUtils.isNotBlank(itId)) {// 审核人不存在 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a7.equals(str)) {// 事业部经理
				if (StringUtils.isNotBlank(divisionManId)) {// 审核人不存在
					// 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a8.equals(str)) {// 总经理
				if (StringUtils.isNotBlank(generalManId)) {// 审核人不存在
					// 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a8.equals(str)) {// 总经理
				if (StringUtils.isNotBlank(generalManId)) {// 审核人不存在
					// 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a9.equals(str)) {// 自定义
				if (StringUtils.isNotBlank(customApprovaId1)) {// 审核人不存在
					// 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a10.equals(str)) {// 自定义
				if (StringUtils.isNotBlank(customApprovaId2)) {// 审核人不存在
					// 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a11.equals(str)) {// 自定义
				if (StringUtils.isNotBlank(customApprovaId3)) {// 审核人不存在
					// 退出当前循环
					isExistAudit = true;
					break;
				}
			} else if (TaskStateEnum.a12.equals(str)) {// 自定义
				if (StringUtils.isNotBlank(customApprovaId4)) {// 审核人不存在
					// 退出当前循环
					isExistAudit = true;
					break;
				}
			}
		}
		if (!isExistAudit || count == listOrder.size()) {// 不存在审批人或评审人为最后审批人
			// 评审人后面必须存在审批环节！
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("isNoAuditAfterReview"));
			return isExistAudit;
		}
		return isExistAudit;
	}

	/**
	 * 获得上传试运行报告
	 * 
	 * @return
	 */
	protected JecnTaskTestRunFile getJecnTaskTestRunFile() {
		if (this.isRunVersion != 0 && jComboBoxRunVersion.getSelectedIndex() != 0) {
			if (jTextFieldUploadATrialRunReport != null) {
				if (jTextFieldUploadATrialRunReport != null) {
					File file = new File(jTextFieldUploadATrialRunReport.getText());
					if (file.exists()) {
						JecnTaskTestRunFile jecnTaskTestRunFile = new JecnTaskTestRunFile();
						jecnTaskTestRunFile.setFileName(file.getName());
						jecnTaskTestRunFile.setBytes(TaskCommon.changeFileToByteByFile(file));
						return jecnTaskTestRunFile;
					}
				}
			}
		}
		return null;
	}

	/**
	 * 获取密级
	 * 
	 * @return 0：秘密，1：公開
	 */
	public Long getIsPublic() {
		// 默认秘密
		Long isPublic = null;
		if (this.jecnTaskApplication.getIsPublic() == 0) {
			return 0L;
		}
		if (this.jComboBoxSecurityClassification.getSelectedIndex() == 0) {
			isPublic = 0L;
		} else if (jComboBoxSecurityClassification.getSelectedIndex() == 1) {
			isPublic = 1L;
		}
		return isPublic;
	}

	/**
	 * 获取提交审批信息
	 * 
	 * @param createTaskBean
	 *            JecnTempCreateTaskBean提交审批信息对象
	 */
	public void setJecnTempCreateTaskBean(JecnTempCreateTaskBean createTaskBean) {
		if (treeNode == null) {
			return;
		}
		Long refId = treeNode.getJecnTreeBean().getId();
		/** 任务基本对象 */
		JecnTaskBeanNew jecnTaskBeanNew = this.getJecnTaskBeanNew(refId);
		// 设置审批文件的名称
		jecnTaskBeanNew.setRname(treeNode.getJecnTreeBean().getName());
		/** 获得上传试运行报告 */
		JecnTaskTestRunFile jecnTaskTestRunFile = this.getJecnTaskTestRunFile();
		/** 审批人的集合 */
		List<JecnTaskApprovePeopleConn> jecnTaskApprovePeopleConnList = this.getlistJecnTaskApprovePeopleConn();
		/** 审批阶段集合 */
		List<JecnTaskStage> taskStageList = this.getJecnTaskStageList();
		/** 设置当前阶段名称 用于邮件发送 */
		this.setJecnTaskStateName(jecnTaskBeanNew, taskStageList);
		// 获得目标人
		int state = this.getState();
		Set<Long> listToPeople = this.getToApprovePeople(state);
		// 查阅权限对象
		JecnTempAccessBean accessBean = new JecnTempAccessBean();
		/*
		 * // 部门查阅权限 accessBean.setOrgIds(accessOrgId); // 岗位查阅权限
		 * accessBean.setPosIds(accessPosId); // 岗位组查阅权限
		 * accessBean.setPosGroupIds(accessPosGroupId);
		 */
		// 根据 查阅权限 ID 获取 是否具有下载权限\
		// 部门
		if (accId != null && accId.getOrgAccessId() != null && this.jecnTaskApplication.getIsAccess() != 0
				&& depCompetenceTable != null) {
			for (AccessId accid : accId.getOrgAccessId()) {
				accid.setDownLoad(depCompetenceTable.getIsDownLoadByTableId(accid.getAccessId()));
			}
		}
		// 岗位
		if (accId != null && accId.getPosAccessId() != null && this.jecnTaskApplication.getIsAccess() != 0
				&& posCompetenceTable != null) {
			for (AccessId accid : accId.getPosAccessId()) {
				accid.setDownLoad(posCompetenceTable.getIsDownLoadByTableId(accid.getAccessId()));
			}
		}
		// 岗位组
		if (accId != null && accId.getPosGroupAccessId() != null && this.jecnTaskApplication.getIsAccess() != 0
				&& posGroupCompetenceTable != null) {
			for (AccessId accid : accId.getPosGroupAccessId()) {
				accid.setDownLoad(posGroupCompetenceTable.getIsDownLoadByTableId(accid.getAccessId()));
			}
		}
		accessBean.setAccId(accId);
		// 关联ID
		accessBean.setRelateId(refId);
		// 是否保存 false 不执行保存
		accessBean.setSave(false);
		// 根据节点获取查阅权限对应的type type0是流程;1是文件2;是标准;3制度
		int accessType = TaskCommon.getViewType(treeNode);
		accessBean.setType(accessType);

		// 设置查阅权限对象
		createTaskBean.setAccessBean(accessBean);

		// 任务详细信息
		createTaskBean.setJecnTaskBeanNew(jecnTaskBeanNew);

		createTaskBean.setJecnTaskApplicationNew(jecnTaskApplication);
		// 任务各阶段负责人
		createTaskBean.setJecnTaskApprovePeopleConnList(jecnTaskApprovePeopleConnList);
		// 任务审批顺序
		createTaskBean.setJecnTaskStageList(taskStageList);
		// 试运行文件
		createTaskBean.setJecnTaskTestRunFile(jecnTaskTestRunFile);
		// 任务目标人
		createTaskBean.setListToPeople(listToPeople);
	}

	/**
	 * 设置当前阶段名称 用于邮件发送
	 * 
	 * @author weidp
	 * @date 2014-8-6 上午10:19:57
	 * @param taskBeanNew
	 * @param taskStageList
	 */
	private void setJecnTaskStateName(JecnTaskBeanNew taskBeanNew, List<JecnTaskStage> taskStageList) {
		for (JecnTaskStage jecnTaskStage : taskStageList) {
			if (taskBeanNew.getState() == jecnTaskStage.getStageMark()) {
				taskBeanNew.setStateMark(jecnTaskStage.getStageName());
				break;
			}
		}
	}

	/**
	 * 获取任务各阶段的审批人信息
	 * 
	 * @author weidp
	 * @date 2014-7-1 下午02:41:07
	 * @return
	 */
	protected List<JecnTaskStage> getJecnTaskStageList() {
		List<JecnTaskStage> taskStageList = new ArrayList<JecnTaskStage>();
		// taskStageList.add(jecnTaskStageMap.get(TaskStateEnum.a0));
		for (JecnTaskStage taskStage : jecnTaskStageMap.values()) {
			// 文控审核人
			if (taskStage.getStageMark() == 1) {
				// 设置是否选人
				if (!(DrawCommon.isNullOrEmtryTrim(auditId))) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}
			} else if (taskStage.getStageMark() == 2) {// 部门审核人
				// 设置是否选人
				if (StringUtils.isNotBlank(deptPeopleId)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}

			} else if (taskStage.getStageMark() == 3 || taskStage.getStageMark() == 10) {// 评审人会审
				// 设置是否选人
				if (StringUtils.isNotBlank(reviewId)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}

			} else if (taskStage.getStageMark() == 4) {// 批准人
				// 设置是否选人
				if (StringUtils.isNotBlank(approveId)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}

			} else if (taskStage.getStageMark() == 6) {// 各业务体系负责人
				// 设置是否选人
				if (StringUtils.isNotBlank(bussId)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}

			} else if (taskStage.getStageMark() == 7) { // IT总监
				// 设置是否选人
				if (StringUtils.isNotBlank(itId)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}

			} else if (taskStage.getStageMark() == 8) {// 事业部经理
				// 设置是否选人
				if (StringUtils.isNotBlank(divisionManId)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}

			} else if (taskStage.getStageMark() == 9) {// 总经理
				// 设置是否选人
				if (StringUtils.isNotBlank(generalManId)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}
			} else if (taskStage.getStageMark() == 11) {// 自定义
				// 设置是否选人
				if (StringUtils.isNotBlank(customApprovaId1)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}
			} else if (taskStage.getStageMark() == 12) {// 自定义
				// 设置是否选人
				if (StringUtils.isNotBlank(customApprovaId2)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}
			} else if (taskStage.getStageMark() == 13) {// 自定义
				// 设置是否选人
				if (StringUtils.isNotBlank(customApprovaId3)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}
			} else if (taskStage.getStageMark() == 14) {// 自定义
				// 设置是否选人
				if (StringUtils.isNotBlank(customApprovaId4)) {
					taskStage.setIsSelectedUser(1);
				} else {
					taskStage.setIsSelectedUser(0);
				}
			} else if (taskStage.getStageMark() == 10) { // 整理意见
				taskStage.setIsSelectedUser(1);
			}
			taskStageList.add(taskStage);
		}
		return taskStageList;
	}

	private boolean isRequest(Integer isEmpty) {
		return isEmpty == 1 ? true : false;
	}

	class JecnCalendarTextFieldPanel extends JecnTextFieldAndCalendarPanel {
		/**
		 * 
		 * 点击日期按钮或输入框弹出日历组件,选中一个日期确定处理
		 * 
		 */
		protected void eventProcess() {
			// 试运行可修改试运行时间，正式版和升级版不可修改试运行时间
			if (jComboBoxRunVersion.getSelectedIndex() != 0) {
				return;
			}
			super.eventProcess();
		}
	}

	/**
	 * 解析本地获取 peopleIds
	 * 
	 * @param userList
	 * @param approvePeopleId
	 * @param stateEnum
	 * @return
	 */
	private String getSplitPeopleIds(List<Object[]> userList, String approvePeopleId, TaskStateEnum stateEnum) {
		if (StringUtils.isBlank(approvePeopleId)) {
			return "";
		}
		List<JecnTreeBean> personTrees = new ArrayList<JecnTreeBean>();
		String[] strArr = approvePeopleId.split(",");
		// 获取存在的评审人
		StringBuffer newStrIds = new StringBuffer();
		JecnTreeBean bean = null;
		for (int i = 0; i < strArr.length; i++) {
			String treeName = TaskCommon.getTrueName(userList, strArr[i]);
			if (treeName != null && StringUtils.isNotBlank(treeName)) {
				if (newStrIds.length() > 0) {
					newStrIds.append(strArr[i]).append(",");
				} else {
					newStrIds.append(strArr[i]);
				}
				bean = new JecnTreeBean(Long.valueOf(strArr[i].toLowerCase()), treeName, TreeNodeType.person);
				personTrees.add(bean);
			}
		}

		approvePersonTrees.put(stateEnum, personTrees);
		return newStrIds.toString();
	}

	/**
	 * 获取 节点人员
	 * 
	 * @param stateEnum
	 * @return
	 */
	private JecnTreeBean getSelectTreeByMap(TaskStateEnum stateEnum) {
		List<JecnTreeBean> treeBeans = approvePersonTrees.get(stateEnum);
		if (treeBeans == null || treeBeans.size() == 0) {
			return null;
		}
		return treeBeans.get(0);
	}

	/**
	 * 审批节点多选
	 * 
	 * @param stateEnum
	 * @return
	 */
	private List<JecnTreeBean> getMutilSelectTreeByMap(TaskStateEnum stateEnum) {
		List<JecnTreeBean> treeBeans = approvePersonTrees.get(stateEnum);
		return treeBeans == null ? new ArrayList<JecnTreeBean>() : treeBeans;
	}

	public enum TaskStateEnum {
		a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13;
	}

	/**
	 * 显示 置灰 权限查阅框体
	 */
	public void hiddenOrShowAccessComponents() {
		if ((jLabelOrgAccess == null && jButtonOrgAccess == null && depCompetenceTable == null)
				|| (jLabelPosAccess == null && jButtonPosAccess == null && posCompetenceTable == null)
				|| (jLabelPosGroupAccess == null && jButtonPosGroupAccess == null && posGroupCompetenceTable == null)) {
			return;
		}
		boolean enabled = jComboBoxSecurityClassification == null ? true : !JecnConfigTool
				.pubDisableAuthSelect(jComboBoxSecurityClassification.getSelectedIndex());
		/** 部门查阅权限 */
		this.jLabelOrgAccess.setEnabled(enabled);
		/** 部门查阅权限的选择按钮 */
		this.jButtonOrgAccess.setEnabled(enabled);
		/** 部门查阅权限 */
		this.depCompetenceTable.setEnabled(enabled);

		/** 岗位查阅权限 */
		this.jLabelPosAccess.setEnabled(enabled);
		/** 岗位查阅权限的选择按钮 */
		this.jButtonPosAccess.setEnabled(enabled);
		/** 岗位查阅权限 */
		this.posCompetenceTable.setEnabled(enabled);

		/** 岗位组查阅权限 */
		this.jLabelPosGroupAccess.setEnabled(enabled);
		/** 岗位组查阅权限的选择按钮 */
		this.jButtonPosGroupAccess.setEnabled(enabled);
		/** 岗位组查阅权限 */
		this.posGroupCompetenceTable.setEnabled(enabled);

	}

}
