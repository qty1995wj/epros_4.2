package com.jecn.epros.server.action.web.timer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import jecntool.cmd.ResultBean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.convert.ReadUserConfigXml;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

/**
 * 定时器处理
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Jan 14, 2013 时间：1:47:59 PM
 */
public class UserAutoBuss {
	/** 加载log日志 */
	private final Log log = LogFactory.getLog(UserAutoBuss.class);
	/** timer定时器 */
	private Timer timer = null;
	/** 定时器时间数据 */
	private AbstractConfigBean autoBean = null;
	/** 定时器监听Action */
	private TimerAction timerAction;

	/**
	 * 定时器秒表开始
	 * 
	 */
	public void timerStart() {
		this.timerStop();
		timer = new Timer();
		if (autoBean == null) {
			throw new NullPointerException("执行定时器，获取初始化信息异常！autoBean==null");
		}
		// 开始时间
		String startTime = autoBean.getStartTime();
		// 间隔天数
		String invalDay = autoBean.getIntervalDay();
		// 获取给定的时间+间隔天数后的时间戳
		long newTime = getProTime(startTime, invalDay);

		if (newTime == -1) {
			return;
		}
		// 定时任务
		UserTimerTask userTimerTask = new UserTimerTask(timerAction);

		// 获取间隔时间
		long valLong = 24 * 60 * 60 * 1000 * Integer.parseInt(invalDay);

		// 计时监听执行给定任务
		// 任务、延迟执行时间、每次执行时间间隔
		timer.schedule(userTimerTask, newTime, valLong);
	}

	/**
	 * 取消定时器
	 * 
	 */
	public void timerStop() {
		if (timer != null) {
			timer.cancel();
		}
	}

	/**
	 * 
	 * 获取给定时间的时间戳
	 * 
	 * @param startTime
	 *            String 开始时间（HH:mm格式）
	 * @param valDayStr
	 *            String 间隔天数（正整数）
	 * @return long 时间戳
	 * @throws ParseException
	 * 
	 */
	private long getProTime(String startTime, String valDayStr) {

		// 开始时间 hh:mm
		if (JecnCommon.isNullOrEmtryTrim(startTime)
				|| !startTime
						.matches("^(0\\d{1}|1\\d{1}|2[0-3]):([0-5]\\d{1})$")) {
			return -1;
		}

		// 间隔天数
		if (JecnCommon.isNullOrEmtryTrim(valDayStr)
				|| !valDayStr.matches("^[1-9]\\d*$")) {// 1到9 数字
			return -1;
		}

		int valDay = Integer.parseInt(valDayStr);

		String space = " ";

		// 新建一个日期格式化类的对象，该对象可以按照指定模板格式化字符串
		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd" + space
				+ "HH:mm");
		// 当前日期格式的对象
		SimpleDateFormat cutFormat = new SimpleDateFormat("yyyy-MM-dd");
		// 获取当前日期字符串
		String cutYMD = cutFormat.format(new Date());

		// 拼裝日期
		String newDataStr = cutYMD + space + startTime;

		// 根据给定字符串时间生成Date对象
		Date newDate = null;
		try {
			newDate = newFormat.parse(newDataStr);
		} catch (ParseException e) {
			log.error("UserAutoSfBuss类getTime方法中转换时间出错：" + newDataStr, e);
			return -1;
		}
		// 新建一个日历对象
		Calendar calen = Calendar.getInstance();
		// 日历对象默认的日期为当前日期
		calen.setTime(newDate);

		// 当天给定时间的毫秒
		long cutTime = Calendar.getInstance().getTimeInMillis();

		// 将日历的"天"增加
		calen.add(Calendar.DAY_OF_YEAR, valDay);
		// 获取日历对象的时间，并赋给日期对象
		Date c = calen.getTime();

		// 下一次执行的毫秒数
		long newTime = c.getTime();

		// 执行任务前的延迟时间，单位是毫秒
		if (newTime < cutTime) {
			return -1;
		}
		long waitTime = newTime - cutTime;

		// 输出时间戳
		return waitTime;
	}

	public TimerAction getTimerAction() {
		return timerAction;
	}

	public void setTimerAction(TimerAction timerAction) {
		this.timerAction = timerAction;
	}

	public AbstractConfigBean getAutoBean() {
		return autoBean;
	}

	public void setAutoBean(AbstractConfigBean autoBean) {
		this.autoBean = autoBean;
	}

	public MessageResult saveParams(String startTime, String intervalDay, String iaAuto) {

		// hh:mm
		if (SyncTool.isNullOrEmtryTrim(startTime) || !startTime.matches("^(0\\d{1}|1\\d{1}|2[0-3]):([0-5]\\d{1})$")) {
			return new MessageResult(false,SyncErrorInfo.START_TIME_FORMAT_FAIL);
		}

		if (SyncTool.isNullOrEmtryTrim(intervalDay) || !intervalDay.matches("^[1-9]\\d*$")) {
			return new MessageResult(false,SyncErrorInfo.INTERVAL_DAY_FORMAT_FAIL);
		}

		if (SyncTool.isNullOrEmtryTrim(iaAuto) || !iaAuto.matches("^[01]$")) {
			return new MessageResult(false,SyncErrorInfo.AUTO_FORMAT_FAIL);
		}

		// 保存到配置文件
		AbstractConfigBean configBean=new AbstractConfigBean();
		configBean.setIaAuto(iaAuto);
		configBean.setIntervalDay(intervalDay);
		configBean.setStartTime(startTime);
		boolean ret = ReadUserConfigXml.writerConfigXml(configBean);
		if (!ret) {
			// 拼装返回页面
			return new MessageResult(false,SyncErrorInfo.SAVE_CONFIG_XML_FAIL);
		}
		
		MessageResult result=new MessageResult(true,"保存到配置文件成功");
		return result;
	}
}
