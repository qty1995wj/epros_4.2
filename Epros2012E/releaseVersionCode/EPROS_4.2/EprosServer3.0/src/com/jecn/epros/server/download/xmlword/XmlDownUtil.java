package com.jecn.epros.server.download.xmlword;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnFinal;

/**
 * 读取和操作xml节点类
 * 
 * @author fuzhh 2013-6-3
 * 
 */
public class XmlDownUtil {
	// private static final Logger log = Logger.getLogger(XmlDownUtil.class);
	/** 内容Title标识 */
	public static String titleStyle = "JecnTitle";
	/** 内容Content标识 */
	public static String contentStyle = "JecnContent";
	/** 竖向表格Title标识 */
	public static String highTabTitleStyle = "JecnHighTableTitle";
	/** 竖向表格Content标识 */
	public static String highTabContentStyle = "JecnHighTableContent";

	/** 横向表格title标识 */
	public static String wideTableTitle = "JecnWideTableTitle";
	/** 横向表格Content标识 */
	public static String wideTableContent = "JecnWideTableContent";
	/** 页面宽度（分辨率）图片大小用 */
	public static int docWidth = 490;
	/** 页面高度（分辨率）图片大小用 */
	public static int docHeight = 600;
	/** 页面横向宽度（分辨率）图片大小用 */
	public static int widthDocWidth = 780;
	/** 页面横向高度（分辨率）图片大小用 */
	public static int widthDocHeight = 370;

	/** 页面宽度（设置表格宽度用） */
	public static int pageWidth = 10000;

	/**
	 * 获取 xml Documet
	 * 
	 * @author fuzhh Apr 23, 2013
	 * @param path
	 *            文件路径
	 * @return
	 */
	public static Document getDocumentByXml(String path) {
		return JecnFinal.getDocumentByXml(path);
	}

	/**
	 * 获取根节点
	 * 
	 * @author fuzhh 2013-5-22
	 * @param document
	 * @return
	 */
	public static Element getRootElement(Document document) {
		if (document == null) {
			return null;
		}
		return document.getRootElement();
	}

	/**
	 * 获取当前节点下的子节点
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            当前节点
	 * @param nodeName
	 *            需要获取的节点名称
	 * @return
	 */
	public static Element getChildNodes(Element element, String nodeName) {
		if (element == null || nodeName == null) {
			return null;
		}
		return element.element(nodeName);
	}

	/**
	 * 获取传入节点的文字
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            节点的Element对象
	 * @return
	 */
	public static String getNodeValue(Element element) {
		if (element == null) {
			return null;
		}
		return element.getText();
	}

	/**
	 * 取得某节点下名为 nodeName 的所有字节点并进行遍历.
	 * 
	 * @author fuzhh 2013-5-22
	 * @param Element
	 *            当前节点的Element对象
	 * @param nodeName
	 *            节点名称
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Element> getNodesList(Element elemt, String nodeName) {
		if (elemt == null || nodeName == null) {
			return null;
		}
		List<Element> elementList = new ArrayList<Element>();
		List nodes = elemt.elements(nodeName);
		for (Iterator it = nodes.iterator(); it.hasNext();) {
			Element elm = (Element) it.next();
			elementList.add(elm);
		}
		return elementList;
	}

	/**
	 * 获取节点下的子节点
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            节点名称
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Element> getChildNodesList(Element nodeElement) {
		if (nodeElement == null) {
			return null;
		}
		List<Element> elementList = nodeElement.elements();
		return elementList;
	}

	/**
	 * 递归查询节点下所有的子节点
	 * 
	 * @author fuzhh 2013-5-23
	 * @param element
	 *            节点
	 * @param allEleList
	 *            节点下所有节点
	 * @return
	 */
	public static void getAllChildNodeList(Element nodeElement,
			List<Element> allEleList) {
		if (nodeElement == null || allEleList == null) {
			return;
		}
		List<Element> eleList = getChildNodesList(nodeElement);
		if (eleList.size() > 0) {
			for (Element ele : eleList) {
				allEleList.add(ele);
				getAllChildNodeList(ele, allEleList);
			}
		}
	}

	/**
	 * 递归查询节点下的某个节点的集合
	 * 
	 * @author fuzhh 2013-5-23
	 * @param element
	 *            节点
	 * @param allEleList
	 *            节点下所有节点
	 * @param nodeName
	 *            节点名称
	 * @return
	 */
	public static void getAllChildNodeList(Element nodeElement,
			List<Element> allEleList, String nodeName) {
		if (nodeElement == null || allEleList == null || nodeName == null) {
			return;
		}
		List<Element> eleList = getChildNodesList(nodeElement);
		if (eleList != null && eleList.size() > 0) {
			for (Element ele : eleList) {
				// System.out.println(ele.getQualifiedName());
				if (nodeName.equals(ele.getName())) {
					allEleList.add(ele);
				}
				getAllChildNodeList(ele, allEleList, nodeName);
			}
		}
	}

	/**
	 * 递归查询修改节点下的值
	 * 
	 * @author fuzhh 2013-5-23
	 * @param element
	 *            p节点
	 * @param updateVal
	 *            修改的值
	 * @return
	 */
	public static void updateNodeValueByNode(Element element, String updateVal) {
		if (element == null) {
			return;
		}
		if (updateVal == null) {
			updateVal = "";
		}
		// 存储所有节点的List
		List<Element> rNodeEleList = new ArrayList<Element>();
		getAllChildNodeList(element, rNodeEleList, "r");
		Element wrEle = null;
		if (rNodeEleList.size() == 0) {
			return;
		} else if (rNodeEleList.size() == 1) {
			wrEle = rNodeEleList.get(0);
		} else {
			for (int i = 0; i < rNodeEleList.size(); i++) {
				Element rEle = rNodeEleList.get(i);
				if (i == 0) {
					wrEle = rEle;
				} else {
					deleteNode(element, rEle);
				}
			}
		}
		List<Element> tNodeEleList = new ArrayList<Element>();
		getAllChildNodeList(wrEle, tNodeEleList, "t");
		Element wtEle = null;
		if (tNodeEleList.size() == 0) {
			wtEle = wrEle.addElement("w:t");
		} else if (tNodeEleList.size() == 1) {
			wtEle = tNodeEleList.get(0);
		} else {
			for (int i = 0; i < tNodeEleList.size(); i++) {
				Element tEle = tNodeEleList.get(i);
				if (i == 0) {
					wtEle = tEle;
				} else {
					deleteNode(wrEle, tEle);
				}
			}
		}
		wtEle.setText(updateVal);
	}

	/**
	 * 递归查询修改节点下的值
	 * 
	 * @author fuzhh 2013-5-23
	 * @param element
	 *            节点
	 * @param nodeName
	 *            需要修改的节点名称
	 * @param updateVal
	 *            修改后的值
	 * @return
	 */
	public static void updateImageNode(Element element,
			String nodeName, String updateVal) {
		if (element == null || nodeName == null) {
			return;
		}
		if (updateVal == null) {
			updateVal = "";
		}
		String imageSrc = "wordml://" + JecnCommon.getUUID() + ".png";
		List<Element> nodeList = new ArrayList<Element>();
		getAllChildNodeList(element, nodeList);
		if (nodeList.size() > 0) {
			for (Element ele : nodeList) {
				if (nodeName.equals(ele.getName())) {
					ele.setText(updateVal);
					updateNodeAttribute(ele, "name", imageSrc);
				} else if ("imagedata".equals(ele.getName())) {
					updateNodeAttribute(ele, "src", imageSrc);
				}
				updateNodeValue(ele, nodeName, updateVal);
			}
		}
	}

	/**
	 * 递归查询修改节点下的值
	 * 
	 * @author fuzhh 2013-5-23
	 * @param element
	 *            节点
	 * @param originalVal
	 *            节点下原有的值
	 * @param updateVal
	 *            修改后的值
	 * @return
	 */
	public static void updateNodeValue(Element element, String originalVal,
			String updateVal) {
		if (element == null || originalVal == null) {
			return;
		}
		if (updateVal == null) {
			updateVal = "";
		}
		List<Element> eleList = getChildNodesList(element);
		if (eleList.size() > 0) {
			for (Element ele : eleList) {
				if (originalVal.equals(getNodeValue(ele))) {
					ele.setText(updateVal);
					return;
				}
				updateNodeValue(ele, originalVal, updateVal);
			}
		}
	}

	/**
	 * 判断节点下是否存在value
	 * 
	 * @author fuzhh 2013-5-23
	 * @param element
	 *            节点
	 * @param nodeValue
	 *            value值
	 * @return true 存在 false不存在
	 */
	public static boolean getValueByEle(Element nodeElement, String nodeValue) {
		if (nodeElement == null || nodeValue == null) {
			return false;
		}
		List<Element> eleList = new ArrayList<Element>();
		getAllChildNodeList(nodeElement, eleList);
		String eleVal = "";
		for (Element ele : eleList) {
			if ("t".equals(ele.getName())) {
				if (nodeValue.equals(getNodeValue(ele).trim())) {
					return true;
				} else {
					eleVal += getNodeValue(ele);
				}
			}
		}
		if (nodeValue.equals(eleVal.trim())) {
			return true;
		}
		return false;
	}

	/**
	 * 复制节点到指定节点
	 * 
	 * @author fuzhh 2013-5-23
	 * @param insertNode
	 *            添加到的节点位置
	 * @param copyNode
	 *            需要复制的节点
	 * @return 节点
	 */
	public static Element copyNodes(Element insertNode, Element copyNode) {
		if (copyNode == null || insertNode == null) {
			return null;
		}
		Element copyEle = insertNode.addElement(copyNode.getQualifiedName());
		copyEle.appendContent(copyNode);
		// insertNode.appendContent(copyNode);
		return copyEle;
	}

	/**
	 * 复制节点到父节点
	 * 
	 * @author fuzhh 2013-5-27
	 * @param copyNode
	 *            需要复制的节点
	 * @return
	 */
	public static Element copyNodes(Element copyNode) {
		if (copyNode == null) {
			return null;
		}
		Element ele = copyNode.getParent();
		if (ele == null) {
			return null;
		}
		Element copyEle = ele.addElement(copyNode.getQualifiedName());
		copyEle.appendContent(copyNode);
		return copyEle;
	}

	/**
	 * 复制节点到自定义节点
	 * 
	 * @author fuzhh 2013-5-27
	 * @param copyNode
	 *            需要复制的节点
	 * @param nodeName
	 *            节点名称
	 * @return
	 */
	public static Element copyChildNodes(Element copyNode, String nodeName) {
		if (copyNode == null || JecnCommon.isNullOrEmtryTrim(nodeName)) {
			return null;
		}
		Element ele = copyNode.getParent();
		Element copyEle = ele.addElement(nodeName);
		copyEle.appendContent(copyNode);
		return copyEle;
	}

	/**
	 * 添加节点返回节点对象
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            节点Element对象
	 * @param nodeName
	 *            节点名称
	 * @return
	 */
	public static Element addNodes(Element element, String nodeName) {
		if (element == null || nodeName == null) {
			return null;
		}
		Element ageElm = element.addElement(nodeName);
		return ageElm;
	}

	/**
	 * 删除节点
	 * 
	 * @author fuzhh 2013-5-22
	 * @param parentElm
	 *            父节点
	 * @param chileElm
	 *            需要删除的节点
	 */
	public static void deleteNode(Element parentElm, Element chileElm) {
		if (parentElm == null || chileElm == null) {
			return;
		}
		parentElm.remove(chileElm);
	}

	/**
	 * 删除节点
	 * 
	 * @author fuzhh 2013-5-22
	 * @param parentElm
	 *            父节点
	 * @param chileElm
	 *            需要删除的节点
	 */
	public static void deleteNode(Element chileElm) {
		if (chileElm == null || chileElm.getParent() == null) {
			return;
		}
		chileElm.getParent().remove(chileElm);
	}

	/**
	 * 获取节点下的属性
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            节点名称
	 * @param attName
	 *            属性名称
	 * @return
	 */
	public static Attribute getNodeAtt(Element nodeElement, String attName) {
		if (nodeElement == null || attName == null) {
			return null;
		}
		Attribute attribute = nodeElement.attribute(attName);
		return attribute;
	}

	/**
	 * 获取节点下的属性值
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            节点名称
	 * @param attName
	 *            属性名称
	 * @return
	 */
	public static String getAttVal(Element nodeElement, String attName) {
		if (nodeElement == null || attName == null) {
			return null;
		}
		String attributeVal = nodeElement.attributeValue(attName);
		return attributeVal;
	}

	/**
	 * 修改节点下属性值
	 * 
	 * @author fuzhh 2013-5-27
	 * @param element
	 *            节点名称
	 * @param attName
	 *            属性名称
	 * @param updateAttVal
	 *            修改后值的名称
	 */
	public static void updateNodeAttribute(Element element, String attName,
			String updateAttVal) {
		Attribute att = getNodeAtt(element, attName);
		if (att == null) {
			return;
		}
		att.setText(updateAttVal);
	}

	/**
	 * 获取属性文字
	 * 
	 * @author fuzhh 2013-5-22
	 * @param attribute
	 *            属性
	 * @return
	 */
	public static String getAttValue(Attribute attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getText();
	}

	/**
	 * 获取 节点下的所有属性
	 * 
	 * @author fuzhh 2013-5-22
	 * @param ele
	 *            节点名称
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Attribute> getAttrubuteList(Element ele) {
		if (ele == null) {
			return null;
		}
		List<Attribute> attriList = new ArrayList<Attribute>();
		for (Iterator it = ele.attributeIterator(); it.hasNext();) {
			Attribute attribute = (Attribute) it.next();
			attriList.add(attribute);
		}
		return attriList;
	}

	/**
	 * 添加节点属性和设置节点值
	 * 
	 * @author fuzhh 2013-5-22
	 * @param ele
	 *            节点
	 * @param attName
	 *            节点名称
	 * @param attValue
	 *            节点值
	 */
	public static Element setEleAttrubute(Element ele, String attName,
			String attValue) {
		if (ele == null || attName == null) {
			return null;
		}
		if (attValue == null) {
			attValue = "";
		}
		Element attEle = ele.addAttribute(attName, attValue);
		return attEle;
	}

	/**
	 * 判断节点下是否存在此 值
	 * 
	 * @author fuzhh 2013-5-24
	 * @param pNodeEleList
	 * @param selNode
	 * @return
	 */
	public static Element getSelectEle(List<Element> pNodeEleList,
			String selNode) {
		if (pNodeEleList == null) {
			return null;
		}
		// 循环查找存在 JecnTitle 的节点
		for (Element ele : pNodeEleList) {
			boolean isEle = getValueByEle(ele, selNode);
			if (isEle) {
				return ele;
			}
		}
		return null;
	}

	/**
	 * 判断节点下是否存在此节点
	 * 
	 * @author fuzhh 2013-5-29
	 * @param selectEle
	 *            节点Ele对象
	 * @param nodeName
	 *            需要判断的节点名称
	 * @return
	 */
	public static boolean isExistNode(Element selectEle, String nodeName) {
		if (nodeName == null) {
			return false;
		}
		List<Element> eleList = new ArrayList<Element>();
		getAllChildNodeList(selectEle, eleList);
		for (Element ele : eleList) {
			if (nodeName.equals(ele.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 通过节点名称判断节点下是否存在节点
	 * 
	 * @author fuzhh 2013-5-29
	 * @param nodeEleList
	 *            需要判断的节点集合
	 * @param nodeName
	 *            节点名称
	 * @return
	 */
	public static Element getExistNode(List<Element> nodeEleList,
			String nodeName) {
		if (nodeEleList == null || nodeName == null) {
			return null;
		}
		// 循环查找存在 JecnTitle 的节点
		for (Element ele : nodeEleList) {
			boolean isEle = isExistNode(ele, nodeName);
			if (isEle) {
				return ele;
			}
		}
		return null;
	}

	/**
	 * 通过节点名称判断节点下是否存在多个节点
	 * 
	 * @author fuzhh 2013-5-29
	 * @param nodeEleList
	 *            需要判断的节点集合
	 * @param nodeNames
	 *            节点数组
	 * @return
	 */
	public static Element getExistNode(List<Element> nodeEleList,
			String[] nodeNames) {
		if (nodeEleList == null || nodeNames == null) {
			return null;
		}
		// 循环查找存在 JecnTitle 的节点
		for (Element ele : nodeEleList) {
			boolean isExist = true;
			for (String nodeName : nodeNames) {
				boolean isEle = isExistNode(ele, nodeName);
				if (!isEle) {
					isExist = false;
					break;
				}
			}
			if (isExist) {
				return ele;
			}
		}
		return null;
	}

	/**
	 * 通过节点名称判断节点下是否存在existNode节点不存在notNode 节点
	 * 
	 * @author fuzhh 2013-5-29
	 * @param nodeEleList
	 *            需要判断的节点集合
	 * @param existNode
	 *            存在的节点
	 * @param notNode
	 *            不存在的节点
	 * @return
	 */
	public static Element getExistNode(List<Element> nodeEleList,
			String existNode, String notNode) {
		if (nodeEleList == null) {
			return null;
		}
		// 循环查找存在 JecnTitle 的节点
		for (Element ele : nodeEleList) {
			boolean isExist = false;
			boolean isEle = isExistNode(ele, existNode);
			if (isEle) {
				boolean isExistEle = isExistNode(ele, notNode);
				if (!isExistEle) {
					isExist = true;
				}
			}
			if (isExist) {
				return ele;
			}
		}
		return null;
	}

	/**
	 * 判断是否为自定义图片
	 * 
	 * @param ele
	 * @return
	 */
	public static Element getImageExistNode(List<Element> nodeEleList,
			String[] nodeNames) {
		if (nodeEleList == null || nodeNames == null) {
			return null;
		}
		// 循环查找存在 JecnTitle 的节点
		for (Element ele : nodeEleList) {
			boolean isExist = true;
			for (String nodeName : nodeNames) {
				boolean isEle = isExistNode(ele, nodeName);
				if (!isEle) {
					isExist = false;
					break;
				}
			}
			if (isExist) {
				String attributeStr = getAttVal(ele, "epros");
				if (attributeStr == null) {
					return ele;
				}
				continue;
			}
		}
		return null;
	}

	/**
	 * 判断是否为自定义图片
	 * 
	 * @param ele
	 * @return
	 */
	public static Element getImageExistNode(List<Element> nodeEleList,
			String existNode, String notNode) {
		if (nodeEleList == null) {
			return null;
		}
		// 循环查找存在 JecnTitle 的节点
		for (Element ele : nodeEleList) {
			boolean isExist = false;
			boolean isEle = isExistNode(ele, existNode);
			if (isEle) {
				boolean isExistEle = isExistNode(ele, notNode);
				if (!isExistEle) {
					isExist = true;
				}
			}
			if (isExist) {
				String attributeStr = getAttVal(ele, "epros");
				if (attributeStr == null) {
					return ele;
				}
			}
		}
		return null;
	}

}
