package epros.designer.gui.process.mode;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

import epros.designer.gui.common.JecnSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class FlowModelNodeSortDialog extends JecnSortDialog {
	private static Logger log = Logger.getLogger(FlowModelNodeSortDialog.class);
	
	public FlowModelNodeSortDialog(JecnTreeNode selectNode,JecnTree jTree){
		super(selectNode, jTree);
	}

	@Override
	public List<JecnTreeBean> getChild() {
		try {
			return ConnectionPool.getProcessModeAction().getFlowModels(
					this.getSelectNode().getJecnTreeBean().getId());
		} catch (Exception e) {
			log.error("FlowModelNodeSortDialog getChild is error",e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public boolean saveData(List<JecnTreeDragBean> list) {
		try {
			ConnectionPool.getProcessModeAction().updateSortNodes(list, this.getSelectNode().getJecnTreeBean().getId());
			return true;
		} catch (Exception e) {
			log.error("FlowModelNodeSortDialog saveData is error",e);
			return false;
		}
	}

}
