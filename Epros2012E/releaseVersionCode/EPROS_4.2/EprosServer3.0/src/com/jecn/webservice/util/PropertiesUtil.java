package com.jecn.webservice.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.designer.update.impl.JecnUpdateFileActionImpl;

public class PropertiesUtil {
  
	 private static Logger log =Logger.getLogger(PropertiesUtil.class);
	/**
	 * 获取本地配置文件属性
	 * 
	 * @author chehuanbo
	 * */
	public static Properties getProperties() {
		// 属性对象
		Properties properties = new Properties();
		FileInputStream inputStream = null;
		// 路径
		String path = null;
		try {
			// 获取指定文件全路径
			String url = JecnUpdateFileActionImpl.class.getClassLoader()
			.getResource("webServiceInfo.properties").toString().substring(6);
	         // 转换空格
	         path = url.replaceAll("%20", " ");
			if (path == null) {
				return null;
			}
			// 读取文件
			inputStream = new FileInputStream(path);
			properties.load(inputStream);
		} catch (FileNotFoundException e) {
			log.error("指定文件找不到，文件路径：" + path,e);
			return null;
		} catch (IOException e) {
			log.error("读取文件出错，文件路径：" + path,e);
			return null;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("关闭FileInputStream流出错，文件路径："+ path,e);
					return null;
				}
			}
		}
		return properties;
	}
}
