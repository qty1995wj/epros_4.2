package epros.draw.event.flowElement;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.ReSizeFigureType;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.JecnFigureResizePanel;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 流程图形元素8个方向点拖动事件处理
 * 
 * @author ZHANGXH
 * @date： 日期：May 9, 2012 时间：5:02:02 PM
 */
public class JecnFlowResizeActionProcess {
	/** 记录鼠标点击时的位置 */
	private Point oldPoint = null;

	/** 操作前数据 */
	private JecnUndoRedoData undoData = null;
	/** 操作后数据 */
	private JecnUndoRedoData redoData = null;

	/** 记录拖动的图形 */
	private List<JecnBaseFigurePanel> curFigureList = new ArrayList<JecnBaseFigurePanel>();

	private int moveX = 0;
	private int moveY = 0;
	private int oldX;
	private int oldY;

	/**
	 * 鼠标点击事件处理
	 * 
	 * @param e
	 */
	public void mouseResizePressed(MouseEvent e) {
		if (e.getSource() == null || !(e.getSource() instanceof JecnFigureResizePanel)
				|| JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			JecnLogConstants.LOG_FLOW_RESIZE_PROCESS.error("JecnFlowResizeActionProcess点击事件参数为空！");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		if (((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0)
				&& ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0)) {
			// 左右键同时按住时,不设置按下位置点
		} else {
			// 记录鼠标点击时的位置
			oldPoint = e.getPoint();

			// *******************撤销恢复*******************//
			// 创建记录撤销恢复的流程元素数据的对象
			undoData = new JecnUndoRedoData();

			// 画图面板：选中流程元素,包括图形相关联的线
			List<JecnBaseFlowElementPanel> undoPanelList = JecnDraggedFiguresPaintLine
					.getSelectFlowElemRefLine(getRedoAndUndoPanelList());

			getMoveFigureOldXY((JecnFigureResizePanel) e.getSource());
			// 记录撤销恢复数据
			undoData.recodeFlowElement(undoPanelList);
		}
		// *******************撤销恢复*******************//
	}

	/**
	 * 
	 * 鼠标释放事件处理
	 * 
	 * @param e
	 */
	public void mouseResizeReleased(MouseEvent e) {
		if ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0 && e.getButton() == MouseEvent.BUTTON3) {// 右键释放且左键还处于按住状态
			return;
		}
		if ((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0 && e.getButton() == MouseEvent.BUTTON1) {// 左键释放且右键还处于按住状态
			return;
		}
		oldPoint = null;
		// 当前面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// *******************撤销恢复*******************//
		// 创建记录撤销恢复的流程元素数据的对象
		redoData = new JecnUndoRedoData();

		// 画图面板：选中流程元素,包括图形相关联的线
		List<JecnBaseFlowElementPanel> redoPanelList = JecnDraggedFiguresPaintLine
				.getSelectFlowElemRefLine(getRedoAndUndoPanelList());

		for (JecnBaseFlowElementPanel flowElementPanel : redoPanelList) {
			if (flowElementPanel instanceof JecnBaseFigurePanel) {
				resizeFigureSize(flowElementPanel);
			}
		}

		getMoveFigureXY((JecnFigureResizePanel) e.getSource());
		if (curFigureList.size() > 0) {// 存在选中图形集合
			// 根据拖动图形画连接线
			JecnDraggedFiguresPaintLine.paintLineByFigures(curFigureList);

			curFigureList.clear();
		}
		// 获取鼠标操作对象
		JecnFigureResizePanel resizePanel = (JecnFigureResizePanel) e.getSource();
		// 泳池特殊处理
		addReleasedResizeModelFigure(redoPanelList, resizePanel.getReSizeFigureType());
		// 记录撤销恢复数据
		redoData.recodeFlowElement(redoPanelList);

		JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);

		if (workflow != null) {
			workflow.getUndoRedoManager().addEdit(undoProcess);
		} else {
			// 此处记录撤销恢复，当前画图面板不能为NULL，为NULL说明处理逻辑有BUG
			JecnLogConstants.LOG_FLOW_RESIZE_PROCESS
					.error("JecnFlowResizeActionProcess类mouseResizeReleased方法：当前主面板中画图面板不能为NULL。"
							+ "JecnDrawMainPanel.getMainPanel().getWorkflow()=" + workflow);
		}
		// *******************撤销恢复*******************//
	}

	private void resizeFigureSize(JecnBaseFlowElementPanel flowElementPanel) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) flowElementPanel;
		// 原始状态下的size
		Dimension originalSize = null;
		if (workflow.getWorkflowScale() != 1.0) {
			originalSize = getOriginalSize(curFigure.getSize(), workflow.getWorkflowScale());
		} else {
			originalSize = curFigure.getSize();
		}

		// 重设置数据 获取100%状态下数据
		curFigure.getJecnFigureDataCloneable().reSetAttributes(curFigure.getLocation(), originalSize,
				workflow.getWorkflowScale());
		// 获取换算后的数据
		JecnFigureDataCloneable figureCloneble = curFigure.getZoomJecnFigureDataCloneable();
		// 设置图片位置点和大小
		curFigure.setBounds(figureCloneble.getX(), figureCloneble.getY(), figureCloneble.getWidth(), figureCloneble
				.getHeight());
	}

	/**
	 * 鼠标拖动事件处理
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseResizeDragged(MouseEvent e) {
		if (oldPoint == null || e.getSource() == null || !(e.getSource() instanceof JecnFigureResizePanel)
				|| JecnDrawMainPanel.getMainPanel() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			JecnLogConstants.LOG_FLOW_RESIZE_PROCESS.error("JecnFlowResizeActionProcess点击事件参数为空！");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		// 获取鼠标操作对象
		JecnFigureResizePanel resizePanel = (JecnFigureResizePanel) e.getSource();
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取面板选中的图形集合
		List<JecnBaseFlowElementPanel> listPanel = desktopPane.getCurrentSelectElement();
		if (listPanel.size() == 0) {
			return;
		}
		// 获取鼠标移动的位移
		int diffX = oldPoint.x - e.getX();
		int diffY = oldPoint.y - e.getY();

		resizePanel.setLocation(resizePanel.getLocation().x - diffX, resizePanel.getLocation().y - diffY);

		for (int i = 0; i < listPanel.size(); i++) {
			if (listPanel.get(i) instanceof JecnBaseFigurePanel) {
				JecnBaseFigurePanel tempFigure = (JecnBaseFigurePanel) listPanel.get(i);

				resizeFigureSize(tempFigure, resizePanel, diffX, diffY);

				// 添加图形到数组
				if (!curFigureList.contains(tempFigure)) {
					curFigureList.add(tempFigure);
				}
			}
		}

		if (curFigureList.size() > 0) {// 存在选中图形集合
			// 根据拖动图形画连接线
			JecnDraggedFiguresPaintLine.paintLineByFigures(curFigureList);
		}
	}

	/**
	 * 拖动图形8个方向点方法处理
	 * 
	 * @param curFigure
	 *            鼠标选中的当前图形
	 * @param resizeFigureSize
	 *            鼠标拖动的方向点
	 * @param diffX
	 * @param diffY
	 */
	private void resizeFigureSize(JecnBaseFigurePanel curFigure, JecnFigureResizePanel resizeFigureSize, int diffX,
			int diffY) {
		// 拖动图形编辑点重置图形位置点和大小
		reSizeFigurePanel(curFigure, resizeFigureSize, diffX, diffY);
		// 设置当前状态下显示点位置
		JecnPaintFigureUnit.reSetCurFigure(curFigure);
	}

	/**
	 * 拖动图形编辑点重置图形位置点和大小
	 * 
	 * @param curFigure
	 *            拖动的当前图形 JecnBaseFigurePanel
	 * @param resizeFigureSize
	 *            JecnFigureResizePanel拖动的图形显示点 图形八个选中点
	 * @param diffX
	 *            鼠标移动的横向位移
	 * @param diffY
	 *            鼠标移动的纵向位移
	 * @param localPoint
	 *            鼠标新的位置点
	 * @param size
	 *            图形新的size
	 */
	private void reSizeFigurePanel(JecnBaseFigurePanel curFigure, JecnFigureResizePanel resizeFigureSize, int diffX,
			int diffY) {
		// // 获取当前比例下数据对象
		// JecnFigureDataCloneable figureClonebleOld = curFigure
		// .getJecnFigureDataCloneable();

		// 是否点击泳池编辑区域
		boolean isModelText = false;

		ModelFigure modelFigure = null;
		int diviLineX = 0;
		if (curFigure instanceof ModelFigure) {
			modelFigure = (ModelFigure) curFigure;
			// boolean:True 点击的为泳池编辑区域；False 点击的不是泳池的编辑区域
			isModelText = modelFigure.isChick();
			diviLineX = modelFigure.getZoomDividingX();
		}
		// 获取拖动图形原始位置点X坐标
		int objPosX = curFigure.getX();
		// 获取拖动图形原始位置点Y坐标
		int objPosY = curFigure.getY();
		// 获取拖动图形原始宽度Y坐标
		int objWidth = curFigure.getWidth();
		// 获取拖动图形原始高度Y坐标
		int objHeight = curFigure.getHeight();
		// 图形最右侧X坐标值
		int rightX = objPosX + objWidth;
		// 图形最底Y坐标值
		int bottomY = objPosY + objHeight;

		// 图形显示的方向点标识 topLeft,
		ReSizeFigureType firgeSizeType = resizeFigureSize.getReSizeFigureType();
		switch (firgeSizeType) {
		case topLeft:// 上左 拖动左上显示点以下右为基准点（基准点位置不变） （拖动显示点到基准点的最小距离为10）
			// 位置点
			if (objWidth + diffX <= 10 && objHeight + diffY > 10) {
				curFigure.setSize(10, objHeight + diffY);
				curFigure.setLocation(rightX - 10, objPosY - diffY);
			}
			if (objWidth + diffX <= 10 && objHeight + diffY <= 10) {
				curFigure.setSize(10, 10);
				curFigure.setLocation(rightX - 10, bottomY - 10);
			}
			if (objWidth + diffX > 10 && objHeight + diffY <= 10) {
				curFigure.setSize(objWidth + diffX, 10);
				curFigure.setLocation(objPosX - diffX, bottomY - 10);
			}
			if (objWidth + diffX > 10 && objHeight + diffY > 10) {
				curFigure.setSize(objWidth + diffX, objHeight + diffY);
				curFigure.setLocation(objPosX - diffX, objPosY - diffY);
			}

			break;
		case topCenter:// 上中 拖动图形上方中间显示点，下方的显示点为基准点，不变 （拖动显示点到基准点的最小距离为10）
			if (objHeight + diffY <= 10) {
				curFigure.setSize(objWidth, 10);
				curFigure.setLocation(objPosX, bottomY - 10);
			} else {
				curFigure.setSize(objWidth, objHeight + diffY);
				curFigure.setLocation(objPosX, objPosY - diffY);
			}

			break;
		case topRight: // 上右 拖动图形上方右侧显示点，下方的左侧显示点为基准点不变 （拖动显示点到基准点的最小距离为10）
			if (isModelText) {// 如果是拖动泳池编辑区域返回
				curFigure.setLocation(objPosX, objPosY);
				curFigure.setSize(objWidth, objHeight);
				break;
			}
			if (objWidth - diffX <= 10 && objHeight + diffY > 10) {
				curFigure.setSize(10, objHeight + diffY);
				curFigure.setLocation(objPosX, objPosY - diffY);
			}
			if (objWidth - diffX <= 10 && objHeight + diffY <= 10) {
				curFigure.setSize(10, 10);
				curFigure.setLocation(objPosX, bottomY - 10);
			}
			if (objWidth - diffX > 10 && objHeight + diffY <= 10) {
				curFigure.setSize(objWidth - diffX, 10);
				curFigure.setLocation(objPosX, bottomY - 10);
			}
			if (objWidth - diffX > 10 && objHeight + diffY > 10) {
				curFigure.setSize(objWidth - diffX, objHeight + diffY);
				curFigure.setLocation(objPosX, objPosY - diffY);
			}

			break;
		case rightCenter:// 右中 拖动图形右侧中间显示点，左侧显示点为基准点不变 （拖动显示点到基准点的最小距离为10）
			curFigure.setLocation(objPosX, objPosY);
			if (isModelText) {
				if (diviLineX - diffX < 10 || diviLineX - diffX > modelFigure.getWidth() - 10) {
					return;
				}
				modelFigure.setZommDividingX(diviLineX - diffX);
				curFigure.setSize(objWidth, objHeight);
			} else {
				if (objWidth - diffX <= 10) {
					curFigure.setSize(10, objHeight);
				} else {
					curFigure.setSize(objWidth - diffX, objHeight);
				}
			}

			break;
		case bottomLRight: // 下右 拖动图形右侧下方显示点，上方左侧显示点为基准点不变 （拖动显示点到基准点的最小距离为10）
			curFigure.setLocation(objPosX, objPosY);
			if (isModelText) {// 如果是拖动泳池编辑区域返回
				curFigure.setSize(objWidth, objHeight);
				break;
			}
			if (objWidth - diffX <= 10 && objHeight - diffY > 10) {
				curFigure.setSize(10, objHeight - diffY);
			}
			if (objWidth - diffX <= 10 && objHeight - diffY <= 10) {
				curFigure.setSize(10, 10);
			}
			if (objWidth - diffX > 10 && objHeight - diffY <= 10) {
				curFigure.setSize(objWidth - diffX, 10);
			}
			if (objWidth - diffX > 10 && objHeight - diffY > 10) {
				curFigure.setSize(objWidth - diffX, objHeight - diffY);
			}
			break;
		case bottomLCenter:// 下中 拖动图形下方中间显示点，上方显示点为基准点不变 （拖动显示点到基准点的最小距离为10）
			curFigure.setLocation(objPosX, objPosY);
			if (objHeight - diffY <= 10) {
				curFigure.setSize(objWidth, 10);
			} else {
				curFigure.setSize(objWidth, objHeight - diffY);
			}
			break;
		case bottomLeft: // 下左 拖动图形下方左侧显示点，上方右侧显示点为基准点不变 （拖动显示点到基准点的最小距离为10）
			if (objWidth + diffX <= 10 && objHeight - diffY > 10) {
				curFigure.setSize(10, objHeight - diffY);
				curFigure.setLocation(rightX - 10, objPosY);
			}
			if (objWidth + diffX <= 10 && objHeight - diffY <= 10) {
				curFigure.setSize(10, 10);
				curFigure.setLocation(rightX - 10, objPosY);
			}
			if (objWidth + diffX > 10 && objHeight - diffY <= 10) {
				curFigure.setSize(objWidth + diffX, 10);
				curFigure.setLocation(objPosX - diffX, objPosY);
			}
			if (objWidth + diffX > 10 && objHeight - diffY > 10) {
				curFigure.setSize(objWidth + diffX, objHeight - diffY);
				curFigure.setLocation(objPosX - diffX, objPosY);
			}
			break;
		case leftCenter:// 左中 拖动图形左侧中间显示点，右侧显示点为基准点不变 （拖动显示点到基准点的最小距离为10）
			if (objWidth + diffX <= 10) {
				curFigure.setSize(10, objHeight);
				curFigure.setLocation(rightX - 10, objPosY);
			} else {
				curFigure.setSize(objWidth + diffX, objHeight);
				curFigure.setLocation(objPosX - diffX, objPosY);
			}
			break;
		}

		// 拖动后记录图形原始位置、大小编辑点（拖动连接线应用， 相对图形元素无关 图形元素100%状态下原始值为鼠标释放后计算
		// 这里只提供临时拖动画连接线应用）
		Dimension size = getOriginalSize(curFigure.getSize(), JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
		curFigure.getJecnFigureDataCloneable().reSetAttributes(curFigure.getLocation(), size,
				JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
	}

	/**
	 * 
	 * 获取原始大小(拖动图形八个方向点专用)
	 * 
	 * @param size
	 *            Dimension 图形大小
	 * @param multiple
	 *            double 放大缩小倍数
	 * @return Dimension 给定大小对应的原始大小
	 */
	private Dimension getOriginalSize(Dimension size, double multiple) {
		Dimension dimension = new Dimension();
		dimension.setSize(size.getWidth() / multiple, size.getHeight() / multiple);
		return dimension;
	}

	private void addReleasedResizeModelFigure(List<JecnBaseFlowElementPanel> redoPanelList,
			ReSizeFigureType firgeSizeType) {
		if (!JecnSystemStaticData.isShowModel()) {
			return;
		}

		List<JecnBaseFlowElementPanel> selectPanelList = JecnDrawMainPanel.getMainPanel().getWorkflow()
				.getCurrentSelectElement();
		JecnBaseFlowElementPanel standardModel = null;
		for (JecnBaseFlowElementPanel jecnBaseFlowElementPanel : selectPanelList) {
			if (jecnBaseFlowElementPanel instanceof ModelFigure) {// 存在泳池
				standardModel = jecnBaseFlowElementPanel;
			}
		}
		if (standardModel == null) {
			return;
		}

		boolean isBeforeMoveModel = true;

		List<JecnBaseFlowElementPanel> modelList = JecnDrawMainPanel.getMainPanel().getWorkflow().getModelPanelList();
		sortMoelFigureList(modelList);

		int size = modelList.size();
		int modelIndex = modelList.indexOf(standardModel);

		List<JecnBaseFigurePanel> baseFigurePanels = new ArrayList<JecnBaseFigurePanel>();
		for (int i = 0; i < size; i++) {
			JecnBaseFlowElementPanel modelFigure = modelList.get(i);
			if (selectPanelList.contains(modelFigure)) {
				isBeforeMoveModel = false;
				continue;
			}
			if (!isBeforeMoveModel && i - modelIndex < 1) {
				continue;
			}
			// 泳池内包含的元素
			List<JecnBaseFlowElementPanel> modelContainsEleList = modelContanisEleMap.get(modelFigure);
			resizeModelFigure(modelFigure, standardModel, firgeSizeType, isBeforeMoveModel, i - modelIndex,
					modelContainsEleList);
			resizeFigureSize(modelFigure);

			addModelContainsEles(baseFigurePanels, modelContainsEleList);
		}

		if (!baseFigurePanels.isEmpty()) {// 存在选中图形集合
			// 重画图形相关的连接线
			JecnDraggedFiguresPaintLine.paintLineByFigures(baseFigurePanels);
		}
	}

	private Map<JecnBaseFlowElementPanel, List<JecnBaseFlowElementPanel>> modelContanisEleMap = new HashMap<JecnBaseFlowElementPanel, List<JecnBaseFlowElementPanel>>();

	/**
	 * 获取锁定泳池后 选中泳池和泳池内包含元素
	 * 
	 * @param curSelectPanels
	 * @return
	 */
	private List<JecnBaseFlowElementPanel> getRedoAndUndoPanelList() {
		if (!JecnSystemStaticData.isShowModel()) {
			return JecnDrawMainPanel.getMainPanel().getWorkflow().getCurrentSelectElement();
		}
		List<JecnBaseFlowElementPanel> modelPanels = JecnDrawMainPanel.getMainPanel().getWorkflow().getModelPanelList();
		Set<JecnBaseFlowElementPanel> setPanels = new HashSet<JecnBaseFlowElementPanel>();
		for (JecnBaseFlowElementPanel flowElementPanel : modelPanels) {
			// 泳池内包含的元素
			List<JecnBaseFlowElementPanel> modelContainsEleList = JecnWorkflowUtil
					.selectModelFigureContainsEles(flowElementPanel);
			modelContanisEleMap.put(flowElementPanel, modelContainsEleList);
			setPanels.addAll(modelContainsEleList);
		}
		if (setPanels.isEmpty()) {
			return new ArrayList<JecnBaseFlowElementPanel>();
		}
		List<JecnBaseFlowElementPanel> resultPanels = new ArrayList<JecnBaseFlowElementPanel>();
		for (JecnBaseFlowElementPanel jecnBaseFlowElementPanel : setPanels) {
			resultPanels.add(jecnBaseFlowElementPanel);
		}
		return resultPanels;
	}

	private void addModelContainsEles(List<JecnBaseFigurePanel> baseFigurePanels,
			List<JecnBaseFlowElementPanel> modelContainsEleList) {
		for (JecnBaseFlowElementPanel figurePanel : modelContainsEleList) {
			if (baseFigurePanels.contains(figurePanel) || figurePanel instanceof ModelFigure) {
				continue;
			}
			baseFigurePanels.add((JecnBaseFigurePanel) figurePanel);
		}
	}

	private void sortMoelFigureList(List<JecnBaseFlowElementPanel> list) {
		if (list == null || list.size() == 0) {
			return;
		}

		Collections.sort(list, new Comparator<JecnBaseFlowElementPanel>() {
			public int compare(JecnBaseFlowElementPanel r1, JecnBaseFlowElementPanel r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("参数不合法");
				}
				return r1.getY() - r2.getY();
			}
		});
	}

	/**
	 * 拖动泳池方向点，面板泳池元素及泳池内包含元素位置及大小处理
	 * 
	 * @param curChangeModel
	 *            当前要处理的泳池元素
	 * @param standardModelFigure
	 *            拖动的泳池元素
	 * @param firgeSizeType
	 *            拖动泳池元素的方向
	 * @param isBeforeMoveModel
	 *            当前处理元素在面板上位置是否在拖动元素之前（按Y坐标排序）
	 * @param index
	 *            面板上坐标排序索引
	 * @param selectModelContainsEleList
	 *            当前处理的泳池所包含的元素集合
	 */
	private void resizeModelFigure(JecnBaseFlowElementPanel curChangeModel,
			JecnBaseFlowElementPanel standardModelFigure, ReSizeFigureType firgeSizeType, boolean isBeforeMoveModel,
			int index, List<JecnBaseFlowElementPanel> selectModelContainsEleList) {

		ModelFigure modelFigure = (ModelFigure) curChangeModel;
		ModelFigure standardModel = (ModelFigure) standardModelFigure;
		// true:拖动的是泳池分割线
		boolean isModelText = ((ModelFigure) standardModel).isChick();

		// 泳池内包含元素 应移动的位移
		int scrollX = 0;
		int scrollY = 0;
		switch (firgeSizeType) {
		case topLeft:
			if (isBeforeMoveModel && index == -1) {// 处理元素index-拖动index为-1的元素(泳池按照Y坐标排序，索引值为集合index)
				modelFigure.setLocation(modelFigure.getLocation().x - moveX, modelFigure.getLocation().y);
				modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight() - moveY);
			} else {
				modelFigure.setLocation(standardModel.getLocation().x, modelFigure.getLocation().y);
				modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight());
			}
			break;
		case topCenter:
			if (isBeforeMoveModel && index == -1) {// 处理元素index-拖动index为-1的元素(泳池按照Y坐标排序，索引值为集合index)
				modelFigure.setSize(standardModel.getWidth(), Math.abs(modelFigure.getLocation().y
						- standardModel.getLocation().y));
			}
			break;
		case topRight:
			if (isModelText) {
				return;
			}
			if (isBeforeMoveModel && index == -1) {// 处理元素index-拖动index为-1的元素(泳池按照Y坐标排序，索引值为集合index)
				modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight() - moveY);
			} else {
				modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight());
			}
			break;
		case rightCenter:
			if (isModelText) {
				modelFigure.setZommDividingX(standardModel.getZoomDividingX());
				modelFigure.repaint();
			} else {
				modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight());
			}
			break;
		case bottomLRight:
			if (isModelText) {
				return;
			}
			if (!isBeforeMoveModel) {
				scrollY = moveY;
				modelFigure.setLocation(modelFigure.getX(), modelFigure.getY() - moveY);
				modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight());
			} else {
				modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight());
			}
			break;
		case bottomLCenter:
			if (!isBeforeMoveModel) {// 拖动泳池 最近的上一个元素
				scrollY = moveY;
				modelFigure.setLocation(modelFigure.getLocation().x, modelFigure.getY() - moveY);
			}
			break;
		case bottomLeft:
			if (!isBeforeMoveModel) {
				scrollY = moveY;
				modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight());
				modelFigure.setLocation(standardModel.getX(), modelFigure.getY() - moveY);
			} else {
				modelFigure.setLocation(standardModel.getLocation().x, modelFigure.getLocation().y);
				modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight());
			}
			break;
		case leftCenter:
			modelFigure.setLocation(standardModel.getLocation().x, modelFigure.getLocation().y);
			modelFigure.setSize(standardModel.getWidth(), modelFigure.getHeight());
			break;
		}

		if (scrollX == 0 && scrollY == 0) {
			return;
		}

		// 处理泳池内包含元素 （Y坐标移动）
		for (JecnBaseFlowElementPanel elementPanel : selectModelContainsEleList) {
			if (elementPanel instanceof ModelFigure) {
				continue;
			}
			elementPanel.setLocation(elementPanel.getX() - scrollX, elementPanel.getY() - scrollY);
			resizeFigureSize(elementPanel);
		}
	}

	/**
	 * 锁定泳池 计算 泳池 坐标位置
	 * 
	 * @param resizePanel
	 */
	private void getMoveFigureOldXY(JecnFigureResizePanel resizePanel) {
		if (!JecnSystemStaticData.isShowModel()) {
			return;
		}
		JecnBaseFlowElementPanel baseFigurePanel = JecnDrawMainPanel.getMainPanel().getWorkflow()
				.getCurrentSelectElement().get(0);
		switch (resizePanel.getReSizeFigureType()) {
		case topLeft:
			oldX = baseFigurePanel.getX();
			oldY = baseFigurePanel.getY();
			break;
		case topCenter:
			oldY = baseFigurePanel.getY();
			break;
		case topRight:
			oldX = baseFigurePanel.getX();
			oldY = baseFigurePanel.getY();
			break;
		case rightCenter:
			oldX = baseFigurePanel.getX();
			oldY = baseFigurePanel.getY();
			break;
		case bottomLRight:
			oldX = baseFigurePanel.getX() + baseFigurePanel.getWidth();
			oldY = baseFigurePanel.getY() + baseFigurePanel.getHeight();
			break;
		case bottomLCenter:
			oldY = baseFigurePanel.getY() + baseFigurePanel.getHeight();
			break;
		case bottomLeft:
			oldX = baseFigurePanel.getX() + baseFigurePanel.getWidth();
			oldY = baseFigurePanel.getY() + baseFigurePanel.getHeight();
			break;
		case leftCenter:
			oldX = baseFigurePanel.getX();
			break;
		}
	}

	/**
	 * 锁定泳池-释放操作，计算泳池改变大小的相对位移
	 * 
	 * @param resizePanel
	 */
	private void getMoveFigureXY(JecnFigureResizePanel resizePanel) {
		if (!JecnSystemStaticData.isShowModel()) {
			return;
		}
		JecnBaseFlowElementPanel baseFigurePanel = JecnDrawMainPanel.getMainPanel().getWorkflow()
				.getCurrentSelectElement().get(0);
		switch (resizePanel.getReSizeFigureType()) {
		case topLeft:
			moveX = oldX - baseFigurePanel.getX();
			moveY = oldY - baseFigurePanel.getY();
			break;
		case topCenter:
			moveY = oldY - baseFigurePanel.getY();
			break;
		case topRight:
			moveY = oldY - baseFigurePanel.getY();
			break;
		case rightCenter:
			moveY = oldY - baseFigurePanel.getY();
			break;
		case bottomLRight:
			moveY = oldY - baseFigurePanel.getY() - baseFigurePanel.getHeight();
			break;
		case bottomLCenter:
			moveY = oldY - baseFigurePanel.getY() - baseFigurePanel.getHeight();
			break;
		case bottomLeft:
			moveY = oldY - baseFigurePanel.getY() - baseFigurePanel.getHeight();
			break;
		case leftCenter:
			moveX = oldX - baseFigurePanel.getX();
			break;
		}
	}
}
