package com.jecn.epros.domain.user;

/**
 * Created by user on 2017/3/13.
 * 收藏和通知搜索bean
 */
public class SearchInfo {

    private String name;
    private Integer type = -1;

    private String getLikeName() {
        return name + "%";
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
