package epros.designer.gui.process.roleDetail;

import epros.designer.gui.process.guide.DesignerProcessOperationIntrusDialog;
import epros.draw.designer.JecnFlowStationT;
import epros.draw.gui.figure.JecnBaseRoleFigure;

/*******************************************************************************
 * 操作说明：添加角色
 * 
 * @author 2012-08-15
 * 
 */
public class DesignerRoleDetailDialog extends RoleDetailDialog {

	// 角色数据
	private JecnBaseRoleFigure baseRoleFigure;
	// 操作说明
	private DesignerProcessOperationIntrusDialog processOperationIntrusDialog;

	public DesignerRoleDetailDialog(JecnBaseRoleFigure baseRoleFigure,
			DesignerProcessOperationIntrusDialog processOperationIntrusDialog) {
		super(baseRoleFigure);
		// errorLab
		// .setText(resourceManager
		// .getValue("theOperationInTheProcessOperationInstructionsWillSaveIrrevocable"));
		this.processOperationIntrusDialog = processOperationIntrusDialog;
		this.baseRoleFigure = baseRoleFigure;
	}

	/***************************************************************************
	 * 确定
	 */
	public void okButPerformed() {
		super.okButPerformed();
		// 角色职责
		for (int index = processOperationIntrusDialog.getRoleResponseTable()
				.getModel().getRowCount() - 1; index >= 0; index--) {
			String id = processOperationIntrusDialog.getRoleResponseTable()
					.getValueAt(index, 0).toString();
			if (baseRoleFigure.getFlowElementData().getFlowElementUUID()
					.equals(id)) {
				// 角色名称
				processOperationIntrusDialog.getRoleResponseTable().setValueAt(
						baseRoleFigure.getFlowElementData().getFigureText(),
						index, 1);
				// 岗位名称
				String PosName = "";
				for (JecnFlowStationT flowStationT : baseRoleFigure
						.getFlowElementData().getFlowStationList()) {
					if ("0".equals(flowStationT.getType())) {
						PosName += flowStationT.getStationName() + "/";
					}
				}
				if ("".equals(PosName)) {
					for (JecnFlowStationT flowStationT : baseRoleFigure
							.getFlowElementData().getFlowStationList()) {
						if ("1".equals(flowStationT.getType())) {
							PosName += flowStationT.getStationName() + "/";
						}
					}
				}
				if (!"".equals(PosName)) {
					PosName = PosName.substring(0, PosName.length() - 1);
				}
				processOperationIntrusDialog.getRoleResponseTable().setValueAt(
						PosName, index, 2);
				// 角色职责
				processOperationIntrusDialog.getRoleResponseTable().setValueAt(
						baseRoleFigure.getFlowElementData().getRoleRes(),
						index, 3);
			}
		}

		// 活动明细
		for (int index = processOperationIntrusDialog.getDrawActiveDescTable()
				.getModel().getRowCount() - 1; index >= 0; index--) {
			if (processOperationIntrusDialog.getDrawActiveDescTable()
					.getValueAt(index, 7) == null) {
				continue;
			}
			String id = processOperationIntrusDialog.getDrawActiveDescTable()
					.getValueAt(index, 7).toString();
			if (id.equals(baseRoleFigure.getFlowElementData()
					.getFlowElementUUID())) {
				processOperationIntrusDialog.getDrawActiveDescTable()
						.setValueAt(
								baseRoleFigure.getFlowElementData()
										.getFigureText(), index, 2);
			}

		}
	}
}
