package com.jecn.epros.server.action.designer.process.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.process.IFlowToolAction;
import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.process.IFlowToolService;

public class FlowToolActionImpl implements IFlowToolAction {

	private IFlowToolService flowToolServiceImpl;

	public IFlowToolService getFlowToolServiceImpl() {
		return flowToolServiceImpl;
	}

	public void setFlowToolServiceImpl(IFlowToolService flowToolServiceImpl) {
		this.flowToolServiceImpl = flowToolServiceImpl;
	}

	@Override
	public JecnFlowSustainTool getJecnSustainToolInfoById(Long id) throws Exception {
		return flowToolServiceImpl.get(id);
	}

	@Override
	public Long addSustainTool(JecnFlowSustainTool jecnFlowSustainTool, Long peopleId) throws Exception {
		return flowToolServiceImpl.saveTool(jecnFlowSustainTool, peopleId);
	}

	@Override
	public void updateSustainTool(JecnFlowSustainTool jecnFlowSustainTool, Long peopleId) throws Exception {
		flowToolServiceImpl.updateTool(jecnFlowSustainTool, peopleId);
	}

	@Override
	public void deleteSustainTools(List<Long> listIds, Long peopelId) throws Exception {
		flowToolServiceImpl.deleteSustainTools(listIds, peopelId);
	}

	@Override
	public List<JecnTreeBean> getChildSustainTools(Long pId) throws Exception {
		return flowToolServiceImpl.getChildSustainTools(pId);
	}

	@Override
	public List<JecnTreeBean> getSustainTools() throws Exception {
		return flowToolServiceImpl.getSustainTools();
	}

	@Override
	public List<JecnTreeBean> getSustainToolDirsByNameMove(String name, List<Long> listIds) throws Exception {
		return flowToolServiceImpl.getSustainToolDirsByNameMove(name, listIds);
	}

	@Override
	public List<JecnTreeBean> getSustainToolsByName(String name) throws Exception {
		return flowToolServiceImpl.getSustainToolsByName(name);
	}

	@Override
	public void moveSustainTools(List<Long> listIds, Long pId, Long peopleId) throws Exception {
		flowToolServiceImpl.moveSustainTools(listIds, pId, peopleId);
	}

	@Override
	public void updateSortSustainTools(List<JecnTreeDragBean> list, Long pId, Long peopleId) throws Exception {
		flowToolServiceImpl.updateSortSustainTools(list, pId, peopleId);
	}

	@Override
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid) throws Exception {
		return flowToolServiceImpl.validateRepeatNameEidt(newName, id, pid);
	}

	@Override
	public List<Long> getFlowToolIds(Long flowId) throws Exception {
		return flowToolServiceImpl.getFlowToolIds(flowId);
	}

	@Override
	public List<JecnTreeBean> getSustainToolsByIDs(List<Long> listIds) throws Exception {
		return flowToolServiceImpl.getSustainToolsByIDs(listIds);
	}

	public List<JecnTreeBean> getSustainToolsByFlowId(Long flowId) throws Exception {
		return flowToolServiceImpl.getSustainToolsByFlowId(flowId);
	}

	@Override
	public void addFlowToolRelated(List<Long> listIds, Long flowId) throws Exception {
		flowToolServiceImpl.addFlowToolRelated(listIds, flowId);
	}

	/***
	 * 根据关联ID、关联类型获取与支持工具关联临时表数据
	 * 
	 * @param relatedId关联ID
	 * @param relatedType
	 *            关联类型
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getSustainToolConnTList(Long relatedId, int relatedType) throws Exception {
		return flowToolServiceImpl.getSustainToolConnTList(relatedId, relatedType);
	}

	/***
	 * 添加"与支持工具关联临时表"的数据
	 * 
	 * @param listIds
	 * @param relatedId
	 * @param relatedType
	 * @throws Exception
	 */
	@Override
	public void addSustainToolConnTRelated(List<Long> listIds, Long relatedId, int relatedType) throws Exception {
		flowToolServiceImpl.addSustainToolConnTRelated(listIds, relatedId, relatedType);
	}

}
