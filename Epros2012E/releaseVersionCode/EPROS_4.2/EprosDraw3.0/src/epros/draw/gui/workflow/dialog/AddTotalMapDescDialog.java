package epros.draw.gui.workflow.dialog;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 流程地图元素添加说明
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-5 时间：下午02:16:11
 */
public class AddTotalMapDescDialog extends JecnDialog {
	/** 主面板 */
	private JecnPanel mainPanel;

	/** 添加说明：文本域 */
	private JTextArea jTextArea;

	/** 添加说明：标签 */
	private JLabel jLabel;
	/** 按钮面板 */
	private JecnPanel butPanel;
	/** 目的滚动面板 */
	private JScrollPane areaScroll;
	/** 确定 */
	private JButton okBut;
	/** 取消 */
	private JButton cancelBut;
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();
	/** 选中的流程地图元素 */
	private JecnBaseFigurePanel baseFigurePanel;
	/** 信息提示Label */
	private JLabel verfyLab = new JLabel();
	/**
	 * 验证是否输入描述的长度超过600汉字或者1200字符
	 */
	private boolean veryzjTextArea = false;

	public AddTotalMapDescDialog(JecnBaseFigurePanel baseFigurePanel) {
		this.baseFigurePanel = baseFigurePanel;
		init();
	}

	private void init() {

		this.setSize(430, 310);
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("mapFigureDesc"));
		this.setLocationRelativeTo(null);
		//
		this.setResizable(true);
		this.setModal(true);
		this.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);

		// 主面板
		mainPanel = new JecnPanel();
		// mainPanel.setSize(430, 310);
		// 添加说明：文本域
		jTextArea = new JTextArea();
		jTextArea.setLineWrap(true);
		jTextArea.setWrapStyleWord(true);
		jTextArea.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		jTextArea.setBorder(null);
		// 添加说明：标签
		jLabel = new JLabel();
		// 描述：
		jLabel.setText(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("descNote"));
		// 按钮面板
		butPanel = new JecnPanel();
		// 文本域滚动面板
		areaScroll = new JScrollPane(jTextArea);
		// areaScroll.setVisible(true);
		areaScroll.setBorder(BorderFactory.createLineBorder(Color.gray));
		areaScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		areaScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 设置按钮 确定
		okBut = new JButton(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("okBtn"));
		cancelBut = new JButton(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("cancelBtn"));

		/**
		 * 确定按钮
		 */
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButAction();
			}
		});
		/**
		 * 取消按钮
		 */
		cancelBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButAction();
			}
		});

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButAction();
				return false;
			}
		});
		/***********************************************************************
		 * 描述Area验证
		 */
		jTextArea.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent e) {
				// 判断当前输入的是否超出600个汉字或者1200字符
				if (DrawCommon.checkNoteLength(jTextArea.getText())) {
					verfyLab.setText(JecnUserCheckInfoData.getNoteLengthInfo());
					veryzjTextArea = true;
				} else {
					verfyLab.setText("");
					veryzjTextArea = false;
				}
			}
		});
		// 设置背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		butPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置字体颜色
		verfyLab.setForeground(Color.red);

		// 设置布局
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 2, 2);
		// infoPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		// 添加按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(butPanel, c);

		infoPanel.setLayout(new GridBagLayout());

		// infoPanel
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(jLabel, c);
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(areaScroll, c);

		// 设置布局
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// 添加错误信息提示Labl
		butPanel.add(verfyLab);
		// 添加按钮
		butPanel.add(okBut);
		butPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);

		// 初始化数据
		jTextArea.setText(baseFigurePanel.getFlowElementData().getAvtivityShow());
	}

	/**
	 * 确定按钮
	 * 
	 */
	private void okButAction() {
		if (baseFigurePanel == null) {
			return;
		}
		if (!isUpdate()) {// 没有更新
			this.dispose();
			return;
		}
		if (!DrawCommon.isInfoFigure(baseFigurePanel.getFlowElementData().getMapElemType())) {
			JecnOptionPane.showMessageDialog(this, "当前图形元素不存在添加说明！");
			this.dispose();
		}
		// 验证添加的描述说明是否超过600汉字或者1200字符
		if (veryzjTextArea) {
			return;
		}
		// 赋值
		baseFigurePanel.getFlowElementData().setAvtivityShow(jTextArea.getText());

		JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		this.dispose();
	}

	/**
	 * 取消
	 */
	protected void cancelButAction() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButAction();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	private boolean isUpdate() {
		if (!DrawCommon.checkStringSame(baseFigurePanel.getFlowElementData().getAvtivityShow(), jTextArea.getText())) {
			return true;
		}
		return false;
	}
}
