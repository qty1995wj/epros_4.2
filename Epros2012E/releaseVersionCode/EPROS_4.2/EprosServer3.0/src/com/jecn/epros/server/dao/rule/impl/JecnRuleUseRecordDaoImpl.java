package com.jecn.epros.server.dao.rule.impl;

import com.jecn.epros.server.bean.rule.JecnRuleUseRecord;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.rule.IJecnRuleUseRecordDao;
/**
 * 
 * 制度使用日志
 * 2013-12-02
 *
 */
public class JecnRuleUseRecordDaoImpl extends AbsBaseDao<JecnRuleUseRecord, String>  implements IJecnRuleUseRecordDao {

}
