/**
 * ISysNotifyTodoWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.lingnan.task;

public interface ISysNotifyTodoWebService extends java.rmi.Remote {
    public com.jecn.webservice.lingnan.task.NotifyTodoAppResult updateTodo(com.jecn.webservice.lingnan.task.NotifyTodoUpdateContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.lingnan.task.Exception;
    public com.jecn.webservice.lingnan.task.NotifyTodoAppResult setTodoDone(com.jecn.webservice.lingnan.task.NotifyTodoRemoveContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.lingnan.task.Exception;
    public com.jecn.webservice.lingnan.task.NotifyTodoAppResult getTodo(com.jecn.webservice.lingnan.task.NotifyTodoGetContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.lingnan.task.Exception;
    public com.jecn.webservice.lingnan.task.NotifyTodoAppResult getTodoCount(com.jecn.webservice.lingnan.task.NotifyTodoGetCountContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.lingnan.task.Exception;
    public com.jecn.webservice.lingnan.task.NotifyTodoAppResult deleteTodo(com.jecn.webservice.lingnan.task.NotifyTodoRemoveContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.lingnan.task.Exception;
    public com.jecn.webservice.lingnan.task.NotifyTodoAppResult sendTodo(com.jecn.webservice.lingnan.task.NotifyTodoSendContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.lingnan.task.Exception;
}
