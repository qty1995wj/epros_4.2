<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<script type="text/javascript" src="${basePath}js/common.js">
</script>
		<script type="text/javascript" src="${basePath}js/firstPage.js">
</script>
		<title><s:text name="fileBasicInformation" /></title>
		<script type="text/javascript">
window.onresize = function() {
	setShowFile('fileDivList');
}
</script>


	</head>

	<body onload="setShowFile('fileDivList')"
		style="height: 100%; margin: 0px; padding: 0px;">
	
		<s:if test="callType!=1">
		<div style="height: 25px; width: 100%; overflow: hidden;"
			align="right" class="gradient minWidth">
			<%@include file="/navigation.jsp"%>
		</div>
	</s:if>
		<table width="100%" style="table-layout: fixed;">
			<tr>
				<td>
					<table width="100%" align="left">
						<tr>
							<td style="font: bold 13px; width: 100px;">
								<!-- 基本信息 -->
								<s:text name="basicInformation" />
							</td>
							<td></td>
						</tr>
						<tr>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								<!-- 文件编号： -->
								<s:text name="fileNumberC" />
							</td>
							<td class="td_dashed">
								&nbsp;
								<s:property value="fileBean.docId" />
							</td>
						</tr>
						<tr>
							<td class="td_dashed">
								<!-- 文件名称： -->
								<s:text name="fileNameC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								&nbsp;
								<a style="cursor: pointer;"
									href="downloadFile.action?fileId=<s:property value='fileBean.fileID' />&isPub=${isPub}"
									target='_blank' class="table"> <s:property
										value="fileBean.fileName" /> </a>
							</td>
						</tr>
						<tr>
							<td class="td_dashed">
								<!-- 责任部门： -->
								<s:text name="responsibilityDepartmentC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								&nbsp;
								<a
									href="organization.action?orgId=<s:property value='fileBean.orgId'/>"
									target='_blank' style="cursor: pointer;" class="table"> <s:property
										value="fileBean.orgName" /> </a>
							</td>
						</tr>

						<tr>
							<td class="td_dashed">
								<!-- 所属流程： -->
								<s:text name="belongToProcessC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								&nbsp;
								<s:property value="fileBean.flowName" />
							</td>
						</tr>

						<tr>
							<td class="td_dashed">
								<!-- 创建时间： -->
								<s:text name="createTimeC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								&nbsp;
								<s:property value="fileBean.createStrTime" />
							</td>
						</tr>

						<tr>
							<td class="td_dashed">
								<!-- 更新时间： -->
								<s:text name="updateTimeC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								&nbsp;
								<s:property value="fileBean.updateStrTime" />
							</td>
						</tr>

						<tr>
							<td class="td_dashed">
								<!-- 创建人： -->
								<s:text name="createPeopleC" />
							</td>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_dashed">
								&nbsp;
								<s:property value="fileBean.createPeople" />
							</td>
						</tr>

						<tr>
							<td style="word-wrap: break-word; word-break: break-all;"
								class="td_padding">
								<!-- 密级： -->
								<s:text name="intensiveC" />
							</td>
							<td class="td_padding">
								&nbsp;
								<s:if test="fileBean.isPublic == 0">
									${secret}
								</s:if>
								<s:else>
									<s:property value="#application.public"/>
								</s:else>
							</td>
						</tr>
					</table>
					<hr style="border: 1px solid #E8E8E8;" />
				</td>
			</tr>


			<tr>
				<td>
					<table width="100%">
						<tr>
							<td style="font: bold 13px;">
								<!-- 文件使用情况 -->
								<s:text name="fileUse" />
							</td>
						</tr>
						<tr>
							<td>
								<div style="width: 100%; overflow-y: scroll;" id="fileDivList">
									<table width="100%" border="0" cellpadding="0" cellspacing="1"
										align="center"
										style="table-layout: fixed; background-color: #BFBFBF;">
										<tr class="FixedTitleRow" align="center">
											<td width="2%" class="gradient">
											</td>
											<td width="76%" height="25" class="gradient"
												style="word-break: break-all;">
												<!-- 名称 -->
												<s:text name="name" />
											</td>
											<td width="10%" height="25" class="gradient"
												style="word-break: break-all;">
												<!-- 密级-->
												<s:text name="intensive" />
											</td>
											<td width="10%" height="25" class="gradient"
												style="word-break: break-all;">
												<!-- 类型 -->
												<s:text name="type" />
											</td>
											<td width="2%" class="gradient">
											</td>
										</tr>
										<s:iterator value="fileBean.fileUseList" status="st">
											<input type="hidden" value="id" id="taskHistoryNewId" />
											<tr align="center" style="background-color: #ffffff"
												onmouseover="style.backgroundColor='#EFEFEF';"
												onmouseout="style.backgroundColor='#ffffff';" height="20">
												<td align="right" class="gradient">
													<s:property value="#st.index+1" />
												</td>
												<td class="t5" style="word-break: break-all;">
													<s:if test="typeName == 'processMap'">
														<a
															href="process.action?type=processMap&flowId=<s:property value='fileId'/>&isPub=${isPub}"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="fileName" /> </a>
													</s:if>
													<s:if test="typeName == 'process'">
														<a
															href="process.action?type=process&flowId=<s:property value='fileId'/>&isPub=${isPub}"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="fileName" /> </a>
													</s:if>
													<s:elseif test="typeName == 'standard'">
														<s:if test="stanType == 1">
															<a
																href="standard.action?standardType=1&standardId=<s:property value='id'/>"
																style="cursor: pointer;" target='_blank' class="table">
																<s:property value="fileName" /> </a>
														</s:if>
														<s:elseif test="stanType == 2">
															<a
																href="process.action?type=process&flowId=<s:property value='fileId'/>"
																target='_blank' style="cursor: pointer;" class="table">
																<s:property value="fileName" /> </a>
														</s:elseif>
														<s:elseif test="stanType == 3">
															<a
																href="process.action?type=processMap&flowId=<s:property value='fileId'/>"
																target='_blank' style="cursor: pointer;" class="table">
																<s:property value="fileName" /> </a>
														</s:elseif>
														<s:else>
															<s:property value="fileName" />
														</s:else>
													</s:elseif>
													<s:elseif test="typeName == 'ruleFile'">
														<a
															href="rule.action?type=ruleFile&ruleId=<s:property value='id'/>&isPub=${isPub}"
															style="cursor: pointer;" target='_blank' class="table">
															<s:property value="fileName" /> </a>
													</s:elseif>
													<s:elseif test="typeName == 'ruleModeFile'">
														<a
															href="rule.action?type=ruleModeFile&ruleId=<s:property value='fileId'/>&isPub=${isPub}"
															style="cursor: pointer;" target='_blank' class="table">
															<s:property value="fileName" /> </a>
													</s:elseif>
													<s:elseif test="typeName == 'organization'">
														<a
															href="organization.action?orgId=<s:property value='fileId'/>"
															style="cursor: pointer;" target='_blank' class="table">
															<s:property value="fileName" /> </a>
													</s:elseif>
												</td>
												<td class="t5" style="word-break: break-all;">
													<s:if test="isPublic == 0">
														${secret}
													</s:if>
													<s:elseif test="isPublic == 1">
														<s:property value="#application.public"/>
													</s:elseif>
												</td>
												<td class="t5" style="word-break: break-all;">
													<s:if test="typeName == 'processMap'">
														<s:text name="flowMap" />
													</s:if>
													<s:if test="typeName == 'process'">
														<s:text name="process" />
													</s:if>
													<s:elseif test="typeName == 'standard'">
														<s:text name="standard" />
													</s:elseif>
													<s:elseif test="typeName == 'ruleFile'">
														<s:text name="ruleFile" />
													</s:elseif>
													<s:elseif test="typeName == 'ruleModeFile'">
														<s:text name="ruleModeFile" />
													</s:elseif>
													<s:elseif test="typeName == 'organization'">
														<s:text name="organizationFile" />
													</s:elseif>
												</td>
												<td></td>
											</tr>
										</s:iterator>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
		<hr style="border: 1px solid #E8E8E8;" />
	</body>
</html>