package epros.designer.gui.common;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/***
 * 新建，更新名称Dialog
 * 
 * @author zhangjie 2012-05-04
 * 
 */
public abstract class JecnEditNameDialog extends JecnDialog {

	private static Logger log = Logger.getLogger(JecnEditNameDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 控件显示面板 */
	protected JPanel topPanel = null;

	/** 按钮面板 */
	protected JPanel buttonPanel = null;

	/** 名称Lab */
	private JLabel nameLab = null;

	/** 名称填写框 */
	protected JTextField nameTextField = null;

	/** 英文名称Lab */
	protected JLabel eNameLab = null;

	/** 英文名称填写框 */
	protected JTextField eNameTextField = null;

	/** 确定按钮 */
	private JButton okBut = null;

	/** 取消按钮 */
	private JButton cancelBut = null;

	/** 名称验证提示 */
	protected JLabel promptLab = null;

	/** 判断是否点击的确定按钮 */
	private boolean isOk = false;

	/** 岗位重命名标识 **/
	protected boolean postRename = false;

	/** 设置面板控件大小 */
	Dimension dimension = null;

	/** 必填项 *号提示 */
	private JLabel requiredMarkLab = new JLabel(JecnProperties.getValue("required"));

	/** 必填项 *号提示 */
	private JLabel requiredMarkLabEnglish = new JLabel(JecnProperties.getValue("required"));

	/** 设置统一部门下岗位名称相同 0:不允许 1：允许 */
	private Long allowSamePosName = null;

	private String name = "";

	private String eName = "";

	protected boolean englishFlag = true;

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
		this.eNameTextField.setText(eName);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		this.nameTextField.setText(name);
	}

	public JTextField geteNameTextField() {
		return eNameTextField;
	}

	public void seteNameTextField(JTextField eNameTextField) {
		this.eNameTextField = eNameTextField;
	}

	public JecnEditNameDialog() {
		initCompotents();
		initLayout();
		this.setModal(true);
	}

	/***
	 * 初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JPanel();

		// 控件显示面板
		topPanel = new JPanel();

		// 按钮面板
		buttonPanel = new JPanel();

		// 名称Lab
		nameLab = new JLabel();

		// 名称填写框
		nameTextField = new JTextField();

		// 名称Lab
		eNameLab = new JLabel();

		// 名称填写框
		eNameTextField = new JTextField();

		// 名称验证提示
		promptLab = new JLabel();

		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));

		// 取消按钮
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

		// 设置Dialog大小
		this.setSize(getWidthMin(), 160);

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置控件显示面板的默认背景色
		topPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置按钮面板的默认背景色
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置验证提示文字颜色
		promptLab.setForeground(Color.red);

		// 为确认按钮增加回车事件
		this.getRootPane().setDefaultButton(okBut);

		requiredMarkLab.setForeground(Color.red);
		requiredMarkLabEnglish.setForeground(Color.red);
		try {
			allowSamePosName = Long.valueOf(ConnectionPool.getConfigAciton().selectValue(5, "allowSamePosName"));
		} catch (Exception e) {
			log.error("JecnEditNameDialog initCompotents is error", e);
		}
	}

	/**
	 * 布局
	 */
	private void initLayout() {
		// 主面板布局
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(3, 5, 3, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(topPanel, c);
		// 控件显示面板 布局
		topPanel.setLayout(new GridBagLayout());
		// topPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 名称Lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(nameLab, c);
		// 名称填写框
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		topPanel.add(nameTextField, c);
		// *号提示
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(requiredMarkLab, c);

		addEnNamePanel(insets);
		addUpfilePanel(insets);

		JPanel infoPanel = new JPanel();
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 按钮面板 布局
		c = new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		infoPanel.add(promptLab);

		// 按钮面板 布局
		c = new GridBagConstraints(0, 5, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		addOtherBut();
		// 确定
		buttonPanel.add(okBut);
		// 取消
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);

		// 确定按钮事件监听
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 取消按钮事件监听
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
		// Dialog标题
		this.setTitle(getDialogTitle());

		// 设置名称Lab的值
		this.nameLab.setText(getNameLab());
		this.eNameLab.setText(JecnProperties.getValue("EnglishNameC"));

		/*
		 * // 设置验证提示Label的大小 dimension = new Dimension(200, 13);
		 * promptLab.setPreferredSize(dimension);
		 * promptLab.setMaximumSize(dimension);
		 * promptLab.setMinimumSize(dimension);
		 */

		// 设置名称显示框JTextField的大小
		dimension = new Dimension(240, 20);
		this.nameTextField.setPreferredSize(dimension);
		nameTextField.setMinimumSize(dimension);
		nameTextField.setMaximumSize(dimension);
		// 设置Dialog的大小不被改变
		this.setResizable(false);
	}

	protected void addOtherBut() {

	}

	// 获取Dialog标题
	public abstract String getDialogTitle();

	// 获取名称Lab
	public abstract String getNameLab();

	// 确定事件
	public void okButtonAction() {
		name = nameTextField.getText().trim();
		eName = eNameTextField.getText().trim();
		nameTextField.setText(name);
		eNameTextField.setText(eName);

		// 验证中文名称是否正确
		String css = JecnUserCheckUtil.checkName(name);
		if (!DrawCommon.isNullOrEmtryTrim(css)) {
			promptLab.setText(JecnProperties.getValue("name") + css);
			return;
		}
		validateEnglishName();// 验证英文名称 
		if (!englishFlag) { return; }

		try {
			if (postRename) {
				// V3.06添加允许同一部门下岗位名称相同
				if (Long.valueOf(allowSamePosName) == 0) { // 允许同一部门下岗位名称相同并且是岗位重命名
					if (validateNodeRepeat(name)) {
						promptLab.setText(JecnProperties.getValue("nameHaved"));
						return;
					} else {
						promptLab.setText("");
					}
				}
			} else {
				if (validateNodeRepeat(name)) {
					promptLab.setText(JecnProperties.getValue("nameHaved"));
					return;
				} else {
					promptLab.setText("");
				}
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			log.error("JecnEditNameDialog okButtonAction is error！", e);
			return;
		}
		saveData();
		isOk = true;
	}

	protected void validateEnglishName() {// 验证英文名称
		// 验证英文名称是否正确
		String ess = JecnUserCheckUtil.checkName(eName);
		if (!DrawCommon.isNullOrEmtryTrim(ess)) {
			promptLab.setText(JecnProperties.getValue("enName") + ess);
			this.englishFlag = false;
		} else {
			this.englishFlag = true;
		}
	}

	protected void addEnNamePanel(Insets insets) {
		// 英文名称Lab
		GridBagConstraints c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(eNameLab, c);
		// 英文名称填写框
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		topPanel.add(eNameTextField, c);
		/* if (JecnConfigTool.isYT()) { */
		// *号提示
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(requiredMarkLabEnglish, c);
		/* } */
	}

	protected void addUpfilePanel(Insets insets) {

	}

	public boolean validateName(String name, JLabel jLable) {
		return JecnValidateCommon.validateNameNoRestrict(name, promptLab);
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:验证是否重名
	 * @return
	 */
	public abstract boolean validateNodeRepeat(String name) throws Exception;

	/**
	 * @author yxw 2012-5-8
	 * @description:保存数据,并显示
	 */
	public abstract void saveData();

	// 取消事件
	public void cancelButtonAction() {
		isOk = false;
		this.dispose();
	}

	public JTextField getNameTextField() {
		return nameTextField;
	}

	public void setNameTextField(JTextField nameTextField) {
		this.nameTextField = nameTextField;
	}

	public boolean isOk() {
		return isOk;
	}

	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}

	public boolean isPostRename() {
		return postRename;
	}

	public void setPostRename(boolean postRename) {
		this.postRename = postRename;
	}

}
