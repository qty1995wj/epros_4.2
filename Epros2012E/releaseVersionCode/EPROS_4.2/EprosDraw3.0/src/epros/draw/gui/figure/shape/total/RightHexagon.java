/*
 * RightHexagon.java
 *箭头六边形》》
 * Created on 2008年11月5日, 下午3:17
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.total;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnPaintEnum;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 箭头六边形
 * 
 * @author zhagnxh
 * @date： 日期：Mar 23, 2012 时间：11:12:39 AM
 */
public class RightHexagon extends JecnBaseFigurePanel {
	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private int x5;
	private int y5;
	private int x6;
	private int y6;

	public RightHexagon(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	/**
	 * 画图
	 * 
	 */
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;

		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		int count = DrawCommon.convertDoubleToInt(19 * scale);
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth - count;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight / 2;
			x4 = userWidth - count;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight;
			x6 = count;
			y6 = userHeight / 2;

		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth / 2;
			y2 = count;
			x3 = userWidth;
			y3 = 0;
			x4 = userWidth;
			y4 = userHeight - count;
			x5 = userWidth / 2;
			y5 = userHeight;
			x6 = 0;
			y6 = userHeight - count;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = count;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth - count;
			y3 = userHeight / 2;
			x4 = userWidth;
			y4 = userHeight;
			x5 = count;
			y5 = userHeight;
			x6 = 0;
			y6 = userHeight / 2;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = count;
			x2 = userWidth / 2;
			y2 = 0;
			x3 = userWidth;
			y3 = count;
			x4 = userWidth;
			y4 = userHeight;
			x5 = userWidth / 2;
			y5 = userHeight - count;
			x6 = 0;
			y6 = userHeight;
		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paintNoneFigure(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4, x5, x6
				, y1, y2, y3, y4, y5, y6, userWidth, userHeight);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintRightHexagonTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintRightHexagonTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintRightHexagonTop(Graphics2D g2d, int shadowCountmin) {
		if (flowElementData.getCurrentAngle() == 0.0) {
			// 顶层 原图，榜值均为零
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin, x5,
					x6 - shadowCountmin }, new int[] { y1, y2, y3, y4 - shadowCountmin, y5 - shadowCountmin,
					y6 - shadowCountmin }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin, x5,
					x6 - shadowCountmin }, new int[] { y1, y2, y3, y4 - shadowCountmin, y5 - shadowCountmin,
					y6 - shadowCountmin }, 6);

		} else if (flowElementData.getCurrentAngle() == 90.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin, x5, x6 },
					new int[] { y1, y2 - shadowCountmin, y3, y4 - shadowCountmin, y5 - shadowCountmin,
							y6 - shadowCountmin }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin, x5, x6 },
					new int[] { y1, y2 - shadowCountmin, y3, y4 - shadowCountmin, y5 - shadowCountmin,
							y6 - shadowCountmin }, 6);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin,
					x5 - shadowCountmin, x6 }, new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmin,
					y5 - shadowCountmin, y6 - shadowCountmin }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin,
					x5 - shadowCountmin, x6 }, new int[] { y1, y2, y3 - shadowCountmin, y4 - shadowCountmin,
					y5 - shadowCountmin, y6 - shadowCountmin }, 6);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			g2d.fillPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin,
					x5 - shadowCountmin, x6 }, new int[] { y1, y2, y3, y4 - shadowCountmin, y5 - shadowCountmin, y6 },
					6);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawPolygon(new int[] { x1, x2 - shadowCountmin, x3 - shadowCountmin, x4 - shadowCountmin,
					x5 - shadowCountmin, x6 }, new int[] { y1, y2, y3, y4 - shadowCountmin, y5 - shadowCountmin, y6 },
					6);

		}
	}

	/**
	 * 3D效果底层填充 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DRightHexagonLow(g2d, 0);
	}

	/**
	 * 既有3D又有阴影效果
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DRightHexagonLow(g2d, 3);
	}

	/**
	 * 3D效果顶层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	private void paint3DRightHexagonLow(Graphics2D g2d, int indent3Dvaluemin) {
		JecnPaintEnum.INTANCE.paint3DHandLow(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvaluemin, indent3Dvaluemin,userWidth,userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DInputHandLine(g2d, 0);
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	private void paint3DInputHandLine(Graphics2D g2d, int indent3Dvaluemin) {
		JecnPaintEnum.INTANCE.paint3DInputHandLine(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3,
				x4, x5, x6, y1, y2, y3, y4, y5, y6,indent3Dvaluemin, indent3Dvaluemin,userWidth,userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DInputHandLine(g2d, 3);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue,userWidth,userHeight);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue,userWidth,userHeight);
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getShadowColor());
		if (flowElementData.getCurrentAngle() == 0.0) {
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount - 1, x4 + shadowCount / 2,
					x5 + shadowCount, x6 + shadowCount }, new int[] { y1 + shadowCount, y2 + shadowCount,
					y3 + shadowCount, y4 + shadowCount, y5 + shadowCount, y6 + shadowCount }, 6);

		} else if (flowElementData.getCurrentAngle() == 90.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount, x4 + shadowCount,
					x5 + shadowCount, x6 + shadowCount }, new int[] { y1 + shadowCount, y2 + shadowCount,
					y3 + shadowCount, y4 + shadowCount / 2, y5 + shadowCount - 1, y6 + shadowCount / 2 }, 6);
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount, x4 + shadowCount,
					x5 + shadowCount, x6 + shadowCount }, new int[] { y1 + shadowCount, y2 + shadowCount,
					y3 + shadowCount, y4 + shadowCount, y5 + shadowCount, y6 + shadowCount }, 6);
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			// 阴影
			g2d.fillPolygon(new int[] { x1 + shadowCount, x2 + shadowCount, x3 + shadowCount, x4 + shadowCount,
					x5 + shadowCount, x6 + shadowCount }, new int[] { y1 + shadowCount, y2 + shadowCount,
					y3 + shadowCount, y4 + shadowCount, y5 + shadowCount, y6 + shadowCount }, 6);
		}
	}
}
