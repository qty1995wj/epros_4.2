/**
 * 审批记录
 * @param {} divId
 */
function taskAppRecord(divId){
	


taskAppRecordStore = new Ext.data.Store({
			autoLoad : {params:{start:0,limit:10}},
			proxy : new Ext.data.HttpProxy({
						url : "taskAppRecord.action"
					}),
			reader:new Ext.data.JsonReader({totalProperty:"totalProperty",root:"root",id:"id"},
			 [
				{ name : "fromPeopleTemporaryName", mapping : "fromPeopleTemporaryName" }, 
				{ name : "opinion", mapping : "opinion" }, 
				{ name : "taskFixedDesc", mapping : "taskFixedDesc" }, 
				{ name : "state", mapping : "state" }, 
				{ name : "taskElseState", mapping : "taskElseState" }, 
				{ name : "toPeopleTemporaryName", mapping : "toPeopleTemporaryName" },
				{ name : "updateTime", mapping : "updateTime" }
			])

		});
		
taskAppRecordColumnModel = new Ext.grid.ColumnModel([new Ext.grid.RowNumberer(),
	    { header : i_approvePerson, dataIndex : "fromPeopleTemporaryName", align : "center", menuDisabled : true, hidden : true }, 
	    { header : i_appIdea, dataIndex : "opinion", align : "center", menuDisabled : true,sortable:true}, 
	    { header : i_appChange, dataIndex : "taskFixedDesc", align : "center", menuDisabled : true},
	    { header : i_stage, dataIndex : "state", align : "center", menuDisabled : true,sortable:true },
		{ header : i_operation, dataIndex : "taskElseState", align : "center", menuDisabled : true ,renderer:taskStageRenderer},
		{ header : i_targetPerson, dataIndex : "toPeopleTemporaryName", align : "center", menuDisabled : true,hidden : true },
		{ header : i_operationDate, align : "updateTime", menuDisabled : true}
]);
taskAppRecordBbar=new Ext.PagingToolbar(
{
	pageSize:10,
	store:taskAppRecordStore,
	displayInfo:true,
	displayMsg:"{0}--{1} "+i_twig+i_together+" {2}"+i_twig,
	emptyMsg:i_noData
});			

 
taskAppRecordGridPanel = new Ext.grid.GridPanel({
			renderTo:divId,
			height:400,
			width:'95%',
			viewConfig : {
				forceFit : true// 让列的宽度和grid一样。
			},
			frame : false,
			store : taskAppRecordStore,
			colModel : taskAppRecordColumnModel,
			bbar:taskAppRecordBbar
		})
	
}
