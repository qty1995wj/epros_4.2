package com.jecn.epros.server.action.rmi.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.bean.AuditPeopleBean;
import com.jecn.epros.bean.BaseTaskParam;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.temp.SimpleSubmitMessage;
import com.jecn.epros.server.bean.task.temp.TempAuditPeopleBean;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;

/**
 * 任务处理帮助类 可以提供task审批使用的service
 * 
 * @author user
 * 
 */
public class TaskHelper {

	private BaseTaskParam taskParam;
	private IJecnBaseTaskService taskService;
	private JecnTaskBeanNew taskBean;
	private Long curPeopleId;

	/** 提交审批 任务处理service接口 */
	private IJecnBaseTaskService flowTaskService;
	/** 提交审批 任务处理service接口 */
	private IJecnBaseTaskService fileTaskService;
	/** 提交审批 任务处理service接口 */
	private IJecnBaseTaskService ruleTaskService;

	public TaskHelper(Long curPeopleId, BaseTaskParam taskParam, JecnTaskBeanNew taskBean,
			IJecnBaseTaskService flowTaskService, IJecnBaseTaskService fileTaskService,
			IJecnBaseTaskService ruleTaskService) {
		this.curPeopleId = curPeopleId;
		this.taskParam = taskParam;
		this.flowTaskService = flowTaskService;
		this.fileTaskService = fileTaskService;
		this.ruleTaskService = ruleTaskService;
		this.taskBean = taskBean;
		this.taskService = initTaskService();
	}

	public SimpleSubmitMessage createSimpleSubmitMessage() throws Exception {
		SimpleSubmitMessage submitMessage = trans(taskParam);
		submitMessage.setCurPeopleId(String.valueOf(curPeopleId));
		submitMessage.setVersion4(true);
		return submitMessage;
	}

	/**
	 * 将BaseTaskParm转换为SimpleSubmitMessage
	 * 
	 * @param param
	 * @return
	 * @throws IntrospectionException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	private SimpleSubmitMessage trans(BaseTaskParam param) throws IntrospectionException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		SimpleSubmitMessage message = new SimpleSubmitMessage();
		Class<SimpleSubmitMessage> messageClazz = SimpleSubmitMessage.class;
		BeanInfo info = Introspector.getBeanInfo(param.getClass());
		PropertyDescriptor[] propertyDescriptors = info.getPropertyDescriptors();
		Method getter = null;
		Method putter = null;
		Method setter = null;
		Object obj = null;
		for (PropertyDescriptor pro : propertyDescriptors) {
			String fieldName = pro.getName();
			if (fieldName.equalsIgnoreCase("class")) {
				continue;
			}
			getter = pro.getReadMethod();
			setter = pro.getWriteMethod();
			obj = getter.invoke(param, new Object[] {});

			// 设置审批人属性
			if (setter.getName().equalsIgnoreCase("setListAuditPeople")) {
				initAuditList(obj, message);
			} else {
				putter = messageClazz.getMethod(setter.getName(), setter.getParameterTypes());
				putter.invoke(message, obj);
			}
		}
		return message;
	}

	/**
	 * 设置审批人
	 */
	private void initAuditList(Object obj, SimpleSubmitMessage message) throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException,
			IntrospectionException {
		message.setListAuditPeople(new ArrayList<TempAuditPeopleBean>());
		List<AuditPeopleBean> audits = (List<AuditPeopleBean>) obj;
		for (AuditPeopleBean audit : audits) {
			message.getListAuditPeople().add(trans(audit));
		}
	}

	/**
	 * 将AuditPeopleBean转换为TempAuditPeopleBean
	 * 
	 * @param param
	 * @return
	 * @throws IntrospectionException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	private TempAuditPeopleBean trans(AuditPeopleBean param) throws IntrospectionException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		TempAuditPeopleBean message = new TempAuditPeopleBean();
		Class<TempAuditPeopleBean> messageClazz = TempAuditPeopleBean.class;
		BeanInfo info = Introspector.getBeanInfo(param.getClass());
		PropertyDescriptor[] propertyDescriptors = info.getPropertyDescriptors();
		Method getter = null;
		Method setter = null;
		Method putter = null;
		Object obj = null;
		for (PropertyDescriptor pro : propertyDescriptors) {
			String fieldName = pro.getName();
			if (fieldName.equalsIgnoreCase("class")) {
				continue;
			}
			getter = pro.getReadMethod();
			setter = pro.getWriteMethod();
			obj = getter.invoke(param, new Object[] {});

			putter = messageClazz.getMethod(setter.getName(), setter.getParameterTypes());
			putter.invoke(message, obj);
		}
		return message;
	}

	/**
	 * 是否已经审批
	 * 
	 * @return
	 * @throws NumberFormatException
	 * @throws Exception
	 */
	public boolean isApprove() throws NumberFormatException, Exception {
		return taskService.hasApproveTask(Long.valueOf(taskParam.getTaskId()), curPeopleId, taskParam.getTaskStage());
	}

	private IJecnBaseTaskService initTaskService() {
		int taskType = taskBean.getTaskType();
		return getTaskService(taskType);
	}

	/**
	 * 获取任务类型
	 * 
	 * @return IJecnBaseTaskService
	 */
	private IJecnBaseTaskService getTaskService(int taskType) {
		if (taskType == -1) {
			return null;
		}
		switch (taskType) {
		case 0:
		case 4:
			return flowTaskService;
		case 2:
		case 3:
			return ruleTaskService;
		case 1:
			return fileTaskService;
		default:
			return null;
		}
	}

	public BaseTaskParam getTaskParam() {
		return taskParam;
	}

	public void setTaskParam(BaseTaskParam taskParam) {
		this.taskParam = taskParam;
	}

	public IJecnBaseTaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(IJecnBaseTaskService taskService) {
		this.taskService = taskService;
	}

	public JecnTaskBeanNew getTaskBean() {
		return taskBean;
	}

	public void setTaskBean(JecnTaskBeanNew taskBean) {
		this.taskBean = taskBean;
	}

	public Long getCurPeopleId() {
		return curPeopleId;
	}

	public void setCurPeopleId(Long curPeopleId) {
		this.curPeopleId = curPeopleId;
	}

}
