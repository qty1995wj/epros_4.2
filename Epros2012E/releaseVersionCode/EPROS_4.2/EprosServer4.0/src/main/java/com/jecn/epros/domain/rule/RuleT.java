package com.jecn.epros.domain.rule;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.util.ConfigUtils;

import java.util.Date;

public class RuleT {

    private Long id;
    private Long projectId;// 工程Id
    private Long perId;// 父节点
    private String ruleNumber;// 制度编号
    private String ruleName;// 制度名称
    private Long isPublic;// 密级(0秘密,1公开)
    private Long typeId;// 制度类别：制度文件的类别划分，类别可增删改查对应JecnFlowType主健
    private Integer isDir;// (0是目录，1是制度,2是制度文件)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date createDate;// 创建时间
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date updateDate;// 更新时间
    private Long createPeopleId;// 创建人ID
    private Long updatePeopleId;// 更新人ID
    private Long orgId;// 责任部门
    private Integer sortId;// 排序
    private Long fileId;// 制度文件Id
    /**
     * 运行状态类型
     */
    private Long testRunNumber;
    private Long historyId;// 文控ID
    private Long attchMentId;// 责任人ID
    private String attchMentName;// 责任人名称
    private Integer typeResPeople; // 责任人类型
    /**
     * 文件bytes 不存在数据库中，只是做文件内容传递
     */
    private byte[] fileBytes;
    private Integer isFileLocal;//g020 0是从文件库关联的1是从本地上传的
    private String tPath;//tpath 节点层级关系
    private int tLevel;// level 节点级别，默认0开始

    private Long expiry;// 有效期
    private Long guardianId;// 监护人ID
    private String guardianName;
    private String businessScope;// 业务范围
    private String otherScope;// 其他范围
    /**
     * 保密级别
     **/
    private Integer confidentialityLevel;
    private Date pubTime;
    /**
     * 制度链接
     **/
    private String ruleUrl;

    private Long commissionerId;

    private String keyWord;
    //生效日期
    private Date effectiveTime;

    private Long fictionPeopleId;

    public String getConfidentialityLevelStr() {
        return ConfigUtils.getConfidentialityLevel(confidentialityLevel);
    }

    //------------------
    
    public Long getFictionPeopleId() {
        return fictionPeopleId;
    }

    public void setFictionPeopleId(Long fictionPeopleId) {
        this.fictionPeopleId = fictionPeopleId;
    }

    public Date getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(Date effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    public String getRuleUrl() {
        return ruleUrl;
    }

    public void setRuleUrl(String ruleUrl) {
        this.ruleUrl = ruleUrl;
    }

    public Integer getConfidentialityLevel() {
        return confidentialityLevel;
    }

    public void setConfidentialityLevel(Integer confidentialityLevel) {
        this.confidentialityLevel = confidentialityLevel;
    }

    public Long getCommissionerId() {
        return commissionerId;
    }

    public void setCommissionerId(Long commissionerId) {
        this.commissionerId = commissionerId;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String gettPath() {
        return tPath;
    }

    public void settPath(String tPath) {
        this.tPath = tPath;
    }

    public int gettLevel() {
        return tLevel;
    }

    public void settLevel(int tLevel) {
        this.tLevel = tLevel;
    }

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    public Integer getIsFileLocal() {
        return isFileLocal;
    }

    public void setIsFileLocal(Integer isFileLocal) {
        this.isFileLocal = isFileLocal;
    }

    public Long getAttchMentId() {
        return attchMentId;
    }

    public void setAttchMentId(Long attchMentId) {
        this.attchMentId = attchMentId;
    }

    public String getAttchMentName() {
        return attchMentName;
    }

    public void setAttchMentName(String attchMentName) {
        this.attchMentName = attchMentName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }

    public String getRuleNumber() {
        return ruleNumber;
    }

    public void setRuleNumber(String ruleNumber) {
        this.ruleNumber = ruleNumber;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Long getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Long isPublic) {
        this.isPublic = isPublic;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Integer getIsDir() {
        return isDir;
    }

    public void setIsDir(Integer isDir) {
        this.isDir = isDir;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getCreatePeopleId() {
        return createPeopleId;
    }

    public void setCreatePeopleId(Long createPeopleId) {
        this.createPeopleId = createPeopleId;
    }

    public Long getUpdatePeopleId() {
        return updatePeopleId;
    }

    public void setUpdatePeopleId(Long updatePeopleId) {
        this.updatePeopleId = updatePeopleId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Integer getSortId() {
        return sortId;
    }

    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public Long getTestRunNumber() {
        return testRunNumber;
    }

    public void setTestRunNumber(Long testRunNumber) {
        this.testRunNumber = testRunNumber;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public Integer getTypeResPeople() {
        return typeResPeople;
    }

    public void setTypeResPeople(Integer typeResPeople) {
        this.typeResPeople = typeResPeople;
    }

    public Long getExpiry() {
        return expiry;
    }

    public void setExpiry(Long expiry) {
        this.expiry = expiry;
    }

    public Long getGuardianId() {
        return guardianId;
    }

    public void setGuardianId(Long guardianId) {
        this.guardianId = guardianId;
    }

    public String getGuardianName() {
        return guardianName;
    }

    public void setGuardianName(String guardianName) {
        this.guardianName = guardianName;
    }

    public String getBusinessScope() {
        return businessScope;
    }

    public void setBusinessScope(String businessScope) {
        this.businessScope = businessScope;
    }

    public String getOtherScope() {
        return otherScope;
    }

    public void setOtherScope(String otherScope) {
        this.otherScope = otherScope;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }
}
