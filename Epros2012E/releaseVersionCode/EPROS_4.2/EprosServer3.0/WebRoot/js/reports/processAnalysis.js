function processAnalysisGrid() {
	processAnalysisGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				baseParams:{
						processId : document.getElementById("processId_id").value,
						timeType : document.getElementById("timeType").value,
						posId : document.getElementById("posId").value,
						processType : document.getElementById("processType").value
				},
				proxy : new Ext.data.HttpProxy({
							url : "processAnalysisDetails.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "flowId",
								mapping : "flowId"
							}, {
								name : "flowName",
								mapping : "flowName"
							}, {
								name : "flowIdInput",
								mapping : "flowIdInput"
							}, {
								name : "resPeopleName",
								mapping : "resPeopleName"
							}, {
								name : "orgName",
								mapping : "orgName"
							}])

			});

	processAnalysisGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			}

	]);
	processAnalysisGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processAnalysisGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processAnalysisGridPanel = new Ext.grid.GridPanel({
				id : "processAnalysisGridPanel_id",
				renderTo : 'processAnalysisGrid_id',
				height :  document.documentElement.clientHeight-20,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processAnalysisGridStore,
				colModel : processAnalysisGridColumnModel,
				bbar : processAnalysisGridBbar
			})
}


// 加载流程分析统计
function processAnalysisPieCharts(){
	$("#sysGrid_id").children().remove();
	$("#sysGrid_id").load('processAnalysis.action',{
	"processId":document.getElementById("processId_analy_id").value,
	"timeType":document.getElementById("timeType_id").value,
	"posId":document.getElementById("participateInPostId_analy_id").value.split("_")[0],
	"processType":document.getElementById("processType_Id").value
	  },function(){
	     setProcessAnalysisWH('sysGrid_id');
	});
}

// 流程分析统计重置
function processAnalysisReset(){
		document.getElementById("processName_analy_id").value = "";
		document.getElementById("processId_analy_id").value = "-1";
		document.getElementById("participateInPostName_analy_id").value = "";
		document.getElementById("participateInPostId_analy_id").value = "-1";
		document.getElementById("timeType_id").options[0].selected=true;
		document.getElementById("processType_Id").options[0].selected=true;
}


/**
 * 流程名称增加超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function processNameRenderer(value, cellmeta, record){
	 return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['flowId']+"' >"+value+"</a>";
}

/**
 * 流程分析统计详情链接
 **/
function findProcessAnalysisSub(timeType){
   // 流程ID
   var processId = document.getElementById("processId_analy_id").value;
   // 岗位ID
   var posId = document.getElementById("participateInPostId_analy_id").value;
   // 流程类别ID
   var processType = document.getElementById("processType_Id").value;
   window.open(basePath+"processActivitiesDetailsLink.action?processId="+processId+"&timeType="+timeType+"&posId="+posId+"&processType="+processType);
}

// 流程分析统计下载
function processAnalysisDownloadExcel(){
   // 流程ID
   var processId = document.getElementById("processId_analy_id").value;
   // 岗位ID
   var posId = document.getElementById("participateInPostId_analy_id").value;
   // 流程类别ID
   var processType = document.getElementById("processType_Id").value;
   window.location.href="downloadProcessAnalysis.action?processId="+processId+"&posId="+posId+"&processType="+processType;
}