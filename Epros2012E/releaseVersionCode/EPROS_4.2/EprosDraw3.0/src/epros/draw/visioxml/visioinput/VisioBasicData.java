package epros.draw.visioxml.visioinput;

import java.util.List;
import java.util.Map;

import visioxml.bean.ConnectProperty;

/**
 * visio形状原始数据
 * 
 * @author admin
 * 
 */
class VisioBasicData {
	/** 面板宽度 **/
	private double panelWidth;
	/** 面板高度 */
	private double panelHeight;
	/** 图形集合 **/
	private List<VisioShape> shapeList;
	/** 动态连接线集合 */
	private List<VisioShape> manLine;
	/** 横竖分割线集合 */
	private List<VisioShape> vHLine;
	/** 横线、竖线、斜线 */
	private List<VisioShape> divedingLine;
	/*** 连接线开始连接的图形属性 ***/
	private Map<String, ConnectProperty> begConn;
	/*** 连接线结束连接的图形属性 ***/
	private Map<String, ConnectProperty> endConn;

	public double getPanelWidth() {
		return panelWidth;
	}

	public void setPanelWidth(double panelWidth) {
		this.panelWidth = panelWidth;
	}

	public double getPanelHeight() {
		return panelHeight;
	}

	public void setPanelHeight(double panelHeight) {
		this.panelHeight = panelHeight;
	}

	public List<VisioShape> getShapeList() {
		return shapeList;
	}

	public void setShapeList(List<VisioShape> shapeList) {
		this.shapeList = shapeList;
	}

	public List<VisioShape> getManLine() {
		return manLine;
	}

	public void setManLine(List<VisioShape> manLine) {
		this.manLine = manLine;
	}

	public List<VisioShape> getvHLine() {
		return vHLine;
	}

	public void setvHLine(List<VisioShape> vHLine) {
		this.vHLine = vHLine;
	}

	public List<VisioShape> getDivedingLine() {
		return divedingLine;
	}

	public void setDivedingLine(List<VisioShape> divedingLine) {
		this.divedingLine = divedingLine;
	}

	public Map<String, ConnectProperty> getBegConn() {
		return begConn;
	}

	public void setBegConn(Map<String, ConnectProperty> begConn) {
		this.begConn = begConn;
	}

	public Map<String, ConnectProperty> getEndConn() {
		return endConn;
	}

	public void setEndConn(Map<String, ConnectProperty> endConn) {
		this.endConn = endConn;
	}
}
