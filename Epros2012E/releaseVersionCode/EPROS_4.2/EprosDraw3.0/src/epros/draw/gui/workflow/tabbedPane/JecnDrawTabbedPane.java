package epros.draw.gui.workflow.tabbedPane;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.unit.JecnFlowElementCopyAndPaste;
import epros.draw.gui.ui.JecnWindowsTabbedPaneUI;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.popup.JecnCloseableTabbedPopupMenu;
import epros.draw.gui.workflow.util.JecnSyncTollBar;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 主界面分页面板
 * 
 * 主要提供创建多个流程图或流程地图
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDrawTabbedPane extends JTabbedPane implements ChangeListener, MouseListener {
	/** 当前画图面板表头面板 */
	private JecnTabbedTitlePanel tabbedTitlePanel;

	public JecnDrawTabbedPane() {
		initComponents();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void initComponents() {
		// 选项卡过多时出左右移动的
		this.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		this.setBorder(null);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 事件
		this.addChangeListener(this);
		this.addMouseListener(this);

		this.setUI(new JecnWindowsTabbedPaneUI());
	}

	/**
	 * 
	 * 添加流程图或流程地图
	 * 
	 * 要保证正常添加，保证参数不为null，MapType类型不为null或none
	 * 
	 * @param JecnFlowMapData
	 *            mapData 流程类型
	 * @return boolean 添加成功返回true，添加失败返回false
	 */
	public boolean addDrawPanelToTabble(JecnFlowMapData mapData) {
		if (mapData == null || mapData.getMapType() == null || mapData.getMapType() == MapType.none) {
			JecnLogConstants.LOG_DRAW_DESKTOP_PANE
					.error("JecnDrawTabbedPane类addDrawPanelToTabble方法：添加新选项卡时给定的参数数据不正确.mapData=" + mapData
							+ ";mapData.getMapType()=" + mapData.getMapType());
			return false;
		}

		// 创建当前选中页标题面板
		this.tabbedTitlePanel = new JecnTabbedTitlePanel(this, mapData);

		return true;
	}

	/**
	 * 
	 * 关闭选项卡
	 * 
	 * @param tabbedTitleButton
	 *            JecnTabbedTitleButton 选项卡的关闭按钮
	 */
	public void removeDrawPanelFromTabble(JecnTabbedTitleButton tabbedTitleButton) {
		if (tabbedTitleButton == null) {
			return;
		}
		// 获取当前画图面板对应的索引
		int closeTabNumber = tabbedTitleButton.getTabbedIndex();

		if (closeTabNumber >= 0 && closeTabNumber <= this.getTabCount() - 1) {// 索引在选项范围
			this.removeTabAt(closeTabNumber);
		}
	}

	/**
	 * 
	 * 返回此选项卡窗格当前选择的画图面板。如果当前没有选择选项卡，则返回 null。
	 * 
	 * @return JecnDrawDesktopPane 当前选择的画图面板
	 */
	public JecnDrawDesktopPane getSelectedDrawDesktopPane() {

		if (this.getSelectedComponent() != null && this.getSelectedComponent() instanceof JecnDrawScrollPane) {

			// 选中面板
			JecnDrawScrollPane selectedScrollPane = (JecnDrawScrollPane) this.getSelectedComponent();
			return selectedScrollPane.getDesktopPane();
		}
		return null;
	}

	/**
	 * 
	 * 切换标签触发事件
	 * 
	 */
	public void stateChanged(ChangeEvent e) {
		stateChangedProcess(e);
	}

	/**
	 * 
	 * 切换标签触发事件
	 * 
	 * 此事件执行原理：tabblepane添加第一个标签页时默认选中此标签页，因此会执行此事件；
	 * 当第二次以上添加标签页时，就不选中添加的新标签页，因此不会执行此事件； 当切换标签时，一定会执行此事件。
	 * 
	 */
	private void stateChangedProcess(ChangeEvent e) {
		// 获取当前选中选项卡，如果当前没有选择选项卡，则返回 null
		JecnDrawDesktopPane desktopPane = getSelectedDrawDesktopPane();
		MapType mapType = null;
		if (desktopPane != null) {// 没有选中选项卡
			// 获取流程类型
			mapType = desktopPane.getFlowMapData().getMapType();

			// 设置当前选中面板
			JecnDrawMainPanel.getMainPanel().setWorkflow(desktopPane);
			// 面板切换，同步处理
			JecnSyncTollBar.syncChangedProcess(desktopPane);

			// 角色移动:主要针对切换画图面板使用，新建流程图时在新建流程图事件中重新添加角色移动
			desktopPane.processRoleMobile();
			// 判断是否为设计器
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
				JecnDesignerProcess.getDesignerProcess().swicthWorkFlow(desktopPane.getFlowMapData().getFlowId(),
						mapType, desktopPane.getFlowMapData().getDesignerData().getModeType());

				// 自动保存定时器重启
				if (desktopPane.getFlowMapData().isSave()) {
					JecnDesignerProcess.getDesignerProcess().timerRestart();
				}

			}
		} else {
			mapType = MapType.none;
			// 设置当前选中面板
			JecnDrawMainPanel.getMainPanel().setWorkflow(null);
		}
		// 同步流程元素库面板
		JecnDrawMainPanel.getMainPanel().getBoxPanel().showBoxPanel(mapType);
		// 初始化剪切复制粘贴使用到的变量
		JecnFlowElementCopyAndPaste.initCountPative();
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
		// 双击
		if (e.getClickCount() == 2) {
			// 隐藏流程设计向导和资源管理器
			JecnDesignerProcess.getDesignerProcess().addTabbedPaneMouseLister(e);
		}
	}

	public void mouseReleased(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3) {// 右键标签页
			for (int i = 0; i < JecnDrawTabbedPane.this.getTabCount(); i++) {
				Rectangle rect = JecnDrawMainPanel.getMainPanel().getTabbedPane().getBoundsAt(i);
				if (rect.contains(e.getX(), e.getY())) {
					JecnDrawTabbedPane.this.setSelectedIndex(i);
					JecnCloseableTabbedPopupMenu closeableTabbed = new JecnCloseableTabbedPopupMenu();
					closeableTabbed.show(JecnDrawMainPanel.getMainPanel().getTabbedPane(), e.getX(), e.getY());
				}
			}
		}
	}

	public JecnTabbedTitlePanel getTabbedTitlePanel() {
		return tabbedTitlePanel;
	}
}
