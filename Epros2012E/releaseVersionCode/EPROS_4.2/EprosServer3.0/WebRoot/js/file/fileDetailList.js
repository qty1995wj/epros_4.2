function fileDetailListJson(fileId, isShow) {
	Ext.Ajax.request( {
		url : 'obtainFileDetailList.action',
		method : 'POST',
		timeout : 1000000,
		params : {
			fileId : fileId,
			showAll : isShow
		},
		success : function(response, options) {
			var json = Ext.util.JSON.decode(response.responseText);
			fileDetailListGrid(json);
			document.getElementById("fileId").value = fileId;
		},
		failure : function(response, options) {
			alert('系统错误请联系管理员！')
		}
	});
}

function fileDetailListGrid(json) {
	fileDetailListGridStore = new Ext.data.JsonStore( {
		autoLoad : false,
		data : json.dataList,
		fields : json.stores
	});

	fileDetailListGridColumnModel = new Ext.grid.ColumnModel( {
		defaults : {
			align : 'center',
			sortable : true,
			menuDisabled : false
		},
		columns : json.columns
	});

	fileDetailListGridPanel = new Ext.grid.GridPanel( {
		id : "fileDetailListGridPanel_id",
		renderTo : 'fileDetailListShow_id',
		height : document.documentElement.clientHeight - 150,
		autoScroll : true,//滚动条
		stripeRows : true,//显示行的分隔符
		columnLines : true,//显示列的分隔线
		enableColumnMove : false,
		viewConfig : {
			forceFit : true
		},
		frame : false,
		bodyStyle : 'border-left: 0px;',
		store : fileDetailListGridStore,
		colModel : fileDetailListGridColumnModel
	})
}

/**
 * 
 * 显示全部
 */
function showAllFileList() {
	var fileId = document.getElementById("fileId").value;
	fileDetailListGridPanel.destroy();
	fileDetailListJson(fileId, true, false);
}

/**
 * 
 *  下载
 */
function downloadFileList() {
	var fileId = document.getElementById("fileId").value;
	window.open("downLoadFileDetailList.action?fileId=" + fileId);
}