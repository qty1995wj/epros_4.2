package com.jecn.epros.server.action.web.file;

import java.io.File;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.standard.IStandardService;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.viewdoc.convert.view.ViewInfo;

/**
 * 所有下载
 * 
 * @author hyl
 * 
 */
public class FileDownload {
	private static final Logger log = Logger.getLogger(FileDownload.class);
	private static final String SUCCESS = "success";
	private IRuleService ruleService;
	private IStandardService standardService;
	private IFileService fileService;
	private IFlowStructureService flowStructureService;
	private IWebProcessService webProcessService;
	private int viewType;
	private boolean isRuleFileLocal;
	private long fileId;
	private boolean isPub;
	private Long historyId;
	private Long peopleId = null;

	/** 返回的数据 **/
	private byte[] downFileByte;
	private String resultPage;
	private String fileName;

	public FileDownload(ViewInfo viewInfo) {
		this.ruleService = viewInfo.getRuleService();
		this.standardService = viewInfo.getStandardService();
		this.fileService = viewInfo.getFileService();
		this.flowStructureService = viewInfo.getFlowStructureService();
		this.webProcessService = viewInfo.getWebProcessService();
		this.viewType = viewInfo.getType();
		this.isRuleFileLocal = viewInfo.isRuleFileLocal();
		this.fileId = viewInfo.getRelatedId();
		this.isPub = viewInfo.isPub();
		this.historyId = viewInfo.gethId();
	}

	public void createDownloadFile() {
		String result = null;
		if (isRuleFileLocal) {// 本地上传制度文件下载
			result = downloadRuleFileFromLocalResult();
		} else if (viewType == 4 || viewType == 5) {
			result = downloadProcessFile();
		} else if (viewType == 6) {// 标准附件
			result = downloadStandardFile();
		} else if (viewType == 7 || viewType == 8) {// 流程图片
			result = downloadProcessImage();
		} else if (viewType == 9 || viewType == 10) {// 流程文件
			result = downloadProcessWord();
		} else if (viewType == 11 || viewType == 12) {// 制度模板文件
			result = downloadRuleWord();
		} else {// 文件下载
			result = downloadFileAndResult();
		}
		this.resultPage = result;
	}

	private String downloadRuleWord() {
		if (fileId == -1) {
			log.error("下载流程（文件）文件id为-1");
			return "fileAbnormal";
		}
		try {
			JecnCreateDoc createDoc = ruleService.downloadRuleFile(fileId, !"false".equals(isPub));
			if (createDoc == null) {
				return "fileAbnormal";
			}
			downFileByte = createDoc.getBytes();
		} catch (Exception e) {
			log.error("", e);
			return "fileAbnormal";
		}
		return SUCCESS;
	}

	private String downloadFileAndResult() {
		try {
			FileOpenBean fileOpenBean = fileService.openVersion(null, fileId);
			if (fileOpenBean != null) {
				downFileByte = fileOpenBean.getFileByte();
			}
		} catch (Exception e) {
			log.error("下载文件异常", e);
			return "fileAbnormal";
		}
		return SUCCESS;
	}

	private String downloadRuleFileFromLocalResult() {
		if (fileId == -1) {
			log.error("下载制度文件内容时文件id为-1");
			return "fileAbnormal";
		}
		try {
			G020RuleFileContent fileContent = ruleService.getG020RuleFileContent(fileId);
			if (fileContent == null) {
				return "fileAbnormal";
			}
			downFileByte = JecnDaoUtil.blobToBytes(fileContent.getFileStream());
			fileName = fileContent.getFileName();
		} catch (Exception e) {
			log.error("", e);
			return "fileAbnormal";
		}
		return SUCCESS;
	}

	/**
	 * 流程（文件） 下载
	 * 
	 * @return
	 */
	private String downloadProcessFile() {
		if (fileId == -1) {
			log.error("下载流程（文件）文件id为-1");
			return "fileAbnormal";
		}
		FileOpenBean openBean = null;
		try {
			openBean = flowStructureService.openFileContent(fileId);
			if (openBean == null) {
				return "fileAbnormal";
			}
			downFileByte = openBean.getFileByte();
			fileName = openBean.getName();
		} catch (Exception e) {
			log.error("", e);
			return "fileAbnormal";
		}
		return SUCCESS;
	}

	/**
	 * 流程（文件） 下载
	 * 
	 * @return
	 */
	private String downloadStandardFile() {
		if (fileId == -1) {
			log.error("下载流程（文件）文件id为-1");
			return "fileAbnormal";
		}
		try {
			FileOpenBean openBean = standardService.openFileContent(fileId);
			if (openBean == null) {
				return "fileAbnormal";
			}
			downFileByte = openBean.getFileByte();
			fileName = openBean.getName();
		} catch (Exception e) {
			log.error("", e);
			return "fileAbnormal";
		}
		return SUCCESS;
	}

	private String downloadProcessImage() {
		if (fileId == -1) {
			log.error("下载流程（文件）文件id为-1");
			return "fileAbnormal";
		}
		try {
			String mapIconPath = null;
			if ("false".equals(isPub)) {
				JecnFlowStructureT flowStructureT = flowStructureService.get(fileId);
				mapIconPath = flowStructureT.getFilePath();
				fileName = flowStructureT.getFlowName() + ".png";
			} else {
				JecnFlowStructure flowStructureT = flowStructureService.findJecnFlowStructureById(fileId);
				mapIconPath = flowStructureT.getFilePath();
				fileName = flowStructureT.getFlowName() + ".png";
			}
			File file = new File(JecnContants.jecn_path + mapIconPath);
			if (!file.exists() || !file.isFile()) {// 判断文件是否是标准文件，是否存在
				log.info("不是文件或文件不存在。path=" + JecnContants.jecn_path + mapIconPath);
				return "fileAbnormal";
			}
			downFileByte = JecnUtil.getByte(file);
		} catch (Exception e) {
			log.error("", e);
			return "fileAbnormal";
		}
		return SUCCESS;
	}

	public ByteFileResult getFileResult() {
		ByteFileResult result = new ByteFileResult();
		if (this.downFileByte == null) {
			result.setSuccess(false);
			return result;
		}
		result.setFileName(fileName);
		result.setBytes(downFileByte);
		return result;
	}

	private String downloadProcessWord() {
		if (fileId == -1) {
			log.error("下载流程（文件）文件id为-1");
			return "fileAbnormal";
		}
		try {
			JecnCreateDoc createDoc = webProcessService.printFlowDoc(fileId, TreeNodeType.process, isPub, peopleId,
					historyId);
			if (createDoc == null) {
				return "fileAbnormal";
			}
			downFileByte = createDoc.getBytes();
			fileName = createDoc.getFileName();
		} catch (Exception e) {
			log.error("", e);
			return "fileAbnormal";
		}
		return SUCCESS;
	}

	public byte[] getDownFileByte() {
		return downFileByte;
	}

	public void setDownFileByte(byte[] downFileByte) {
		this.downFileByte = downFileByte;
	}

	public String getResultPage() {
		return resultPage;
	}

	public void setResultPage(String resultPage) {
		this.resultPage = resultPage;
	}

	public String getFileName() {
		return fileName;
	}

}
