package com.jecn.epros.server.action.web.login.ad.dongh;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.task.TaskSearchTempBean;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * 东航：单点登录
 * 
 * http://192.168.1.11:8080/login.action?UserID=jecn&UserPwd=amVjbg==&Login=true
 * 
 * @author zhouxy
 * 
 */
public class DonghWebLoginAciton extends JecnAbstractADLoginAction {

	/** http中员工编号参数 */
	private static final String NUM_PARA = "EmployeeNumber";

	static {
		JecnDonghAfterItem.start();
	}

	public DonghWebLoginAciton(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 通过单点登录
	 * 
	 * @return String input;main;success;null
	 * 
	 */
	public String login() {
		// WebLoginBean webLoginBean = null;
		// if ("true".equals(getLogin())) {// 需要单点登录
		// webLoginBean = adUserLogin();
		// if (webLoginBean == null) {
		// return LoginAction.NULL;
		// } else if (webLoginBean.isAdmin()) {
		// return LoginAction.MAIN;
		// } else {
		// return LoginAction.SUCCESS;
		// }
		// } else {// 普通登录
		try {
			return httpLogin();
		} catch (Exception e) {
			e.printStackTrace();
			return LoginAction.INPUT;
		}
	}

	/**
	 * HTTP 地址解析
	 * 
	 * @return String
	 * @date 2015-8-19 下午03:34:39
	 * @throws
	 */
	private String httpLogin() throws Exception {
		// http://172.20.29.55/login.action?LoginType=login_process&EmployeeNumber=19Z6eI+T99dfLpe0smyNCA==
		// request 获取连接传入 参数 ;URL 的加号 被转义为空格, 以下方式不会

		String queryString = loginAction.getRequest().getQueryString();
		if (StringUtils.isEmpty(queryString)) {
			return loginAction.loginGen();
		} 
		String[] str = queryString.split("&");
		queryString = str[1].substring(0);

		String em_Num = queryString.substring(NUM_PARA.length() + 1,
				queryString.length());
		if (StringUtils.isBlank(em_Num)) {// 单点登录名称为空，执行普通登录路径
			return loginAction.loginGen();
		}
		// EmployeeNumber = "8bjMpms/LDU=";
		// 获取密钥解析对象
		JecnDesKeyTools tools = new JecnDesKeyTools();
		// 获取解密后用户
		String encodeNum = null;
		try {
			// 获取解密后用户
			encodeNum = tools.decode(em_Num);
		} catch (Exception e) {
			log.error("员工编号解密异常！");
			throw e;
		}
		
		String result = loginByLoginName(encodeNum);
		if (result.equals(LoginAction.MAIN)
				|| result.equals(LoginAction.SUCCESS)) {
			// 登录类型 login_process:流程体系；login_file：知识库
			String loginType = loginAction.getRequest().getParameter("LoginType");
			if ("login_process".equals(loginType)) {
				return "login_processSys";
			} else {
				// 验证成功,返回知识库页面
				return "login_knowledge";
			}
		}
		return loginAction.loginGen();
	}

	/**
	 * 返回结果页
	 */
	public String returnPage() {
		return null;
	}

	/**
	 * AD域验证 单点登录
	 * 
	 * @author fuzhh Mar 26, 2013
	 */
	private WebLoginBean adUserLogin() {
		WebLoginBean webLoginBean = null;
		LoginInJson loginInJson = null;

		// 登录名称
		String loginName = this.getUserID();
		// 登录密码
		String password = this.getUserPwd();

		if (JecnCommon.isNullOrEmtryTrim(loginName)) {
			loginInJson = new LoginInJson();
			loginInJson.setResult("255");
			loginInJson.setMessage("登录名为空!");
			outJson(loginInJson);
			return null;
		}
		if (JecnCommon.isNullOrEmtryTrim(password)) {
			loginInJson = new LoginInJson();
			loginInJson.setResult("255");
			loginInJson.setMessage("登录密码为空!");
			outJson(loginInJson);
			return null;
		}

		// 验证用户名是否存在
		Object loginObj = ActionContext.getContext().getSession().get(
				JecnContants.SESSION_KEY);
		if (loginObj == null) {// 不存在
			loginInJson = new LoginInJson();
			// // 为空走AD域验证
			// loginInJson = singLeLoginInJson();
			//
			// if (!"0".equals(loginInJson.getResult())) {// AD域验证失败
			// outJson(loginInJson);
			// return null;
			// }
		} else {
			webLoginBean = (WebLoginBean) loginObj;
			loginInJson = new LoginInJson();
			loginInJson.setResult("0");
			loginInJson.setExpires(JecnSingleLoginUtil.getDate());
			outJson(loginInJson);
			return webLoginBean;

		}

		try {
			webLoginBean = getWebLoginBean(loginName, password, false);
			if (webLoginBean == null) {
				loginInJson.setResult("255");
				loginInJson.setMessage("没有获取到用户信息!");
				outJson(loginInJson);
				return null;
			}

			String ret = loginAction.addUserInfo(webLoginBean);
			if (LoginAction.MAIN.equals(ret) || LoginAction.SUCCESS.equals(ret)) {// 成功
				// 需要单点登录 AD域名验证
				outJson(loginInJson);
				return webLoginBean;
			} else {
				if (JecnContants.ERROR_NOT_PROJECT.equals(ret)) {// 主项目为空
					loginInJson.setResult("255");
					loginInJson.setMessage("主项目为空!");
					outJson(loginInJson);
				} else {// 其他错误
					loginInJson.setResult("1");
					loginInJson.setExpires(null);
					outJson(loginInJson);
				}
				return null;
			}
		} catch (Exception e) {
			log.error("", e);

			// 需要单点登录 AD域名验证
			loginInJson.setResult("255");
			loginInJson.setMessage("用户登录出错!");
			outJson(loginInJson);
			return null;
		}
	}

	/**
	 * 
	 * AD域验证
	 * 
	 * @return LoginInJson
	 */
	private LoginInJson singLeLoginInJson() {
		LoginInJson loginInJson = null;
		try {
			loginInJson = JecnSingleLoginUtil.singLeLogin(this.getUserID(),
					this.getUserPwd());
		} catch (Exception e) {
			if (loginInJson == null) {
				loginInJson = new LoginInJson();
			}
			loginInJson.setResult("255");
			loginInJson.setMessage("验证AD域名异常！");
			log.error("验证AD域名异常！", e);
		}
		return loginInJson;
	}

	private void outJson(LoginInJson loginInJson) {
		/* 设置格式为text/json */
		// getResponse().setContentType("text/json");
		/* 设置字符集为'UTF-8' */
		getResponse().setCharacterEncoding("UTF-8");
		String json = "{'Result':" + loginInJson.getResult() + ",'Message':'"
				+ loginInJson.getMessage() + "','Expires':'"
				+ loginInJson.getExpires() + "'}";
		JSONObject JSONArray = JSONObject.fromObject(json);
		try {
			getResponse().getWriter().print(JSONArray);
		} catch (IOException e) {
			log.error("输出json错误！！", e);
		}
	}

	/**
	 * 获得response
	 * 
	 * @return HttpServletResponse
	 */
	private HttpServletResponse getResponse() {
		return this.loginAction.getResponse();
	}

	/**
	 * 
	 * 用户名
	 * 
	 * @return
	 */
	private String getUserID() {
		return this.loginAction.getUserID();
	}

	/**
	 * 
	 * 密码
	 * 
	 * @return
	 */
	private String getUserPwd() {
		return this.loginAction.getUserPwd();
	}

	/**
	 * 
	 * 东航单点登录 标记本次验证是否需要登录 true为需要登录，false为不需要登
	 * 
	 * @return true为需要登录，false为不需要登
	 */
	private String getLogin() {
		return this.loginAction.getLogin();
	}

	/**
	 * 获得登陆bean
	 * 
	 * @param loginName
	 * @param password
	 * @param loginType
	 *            true为验证密码，false为不验证密码
	 * @return
	 * @throws Exception
	 */
	private WebLoginBean getWebLoginBean(String loginName, String password,
			boolean loginType) throws Exception {
		return this.loginAction.getPersonService().getWebLoginBean(loginName,
				password, loginType);
	}

	/**
	 * 我的任务获取登录人相关任务总数
	 * 
	 * @return List<MyTaskBean>登录人相关任务集合
	 * @param loginPeopleId
	 *            Long 登录
	 * @throws Exception
	 */
	private int getMyTaskCountBySearch(TaskSearchTempBean searchTempBean,
			Long loginPeopleId) throws Exception {
		return this.loginAction.getSearchService().getMyTaskCountBySearch(
				searchTempBean, loginPeopleId);
	}

	private long getPeopleId() {
		return this.loginAction.getPeopleId();
	}
}
