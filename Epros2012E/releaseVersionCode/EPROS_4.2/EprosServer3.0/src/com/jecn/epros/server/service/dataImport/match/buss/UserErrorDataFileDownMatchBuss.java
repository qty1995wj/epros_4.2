package com.jecn.epros.server.service.dataImport.match.buss;

import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;

/**
 * 岗位匹配下载错误信息Excel
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Feb 28, 2013 时间：5:03:01 PM
 */
public class UserErrorDataFileDownMatchBuss extends AbstractFileDownMatchBuss {
	/**
	 * 
	 * 导出文件路径
	 * 
	 * @return String 文件路径
	 */
	public String getOutFilepath() {
		return MatchTool.getOutPutExcelErrorDataPath();
	}

	/**
	 * 
	 * 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	protected String getOutFileName() {
		return MatchConstant.OUTPUT_EXCEL_ERROR_DATA_NAME;
	}

}
