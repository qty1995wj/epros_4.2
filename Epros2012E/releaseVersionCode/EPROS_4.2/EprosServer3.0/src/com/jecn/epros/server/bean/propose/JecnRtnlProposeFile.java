package com.jecn.epros.server.bean.propose;

import java.sql.Blob;
import java.util.Date;


/***
 * 建议附件表
 * 2013-09-02
 *
 */
public class JecnRtnlProposeFile implements java.io.Serializable {

	//主键ID
	private String id;
	//关联ID
	private String proposeId;
	//文件名称
	private String fileName;
	//文件内容
	private Blob fileStream;
	//创建人
	private Long createPersonId;
	//创建日期
	private Date createTime;
	//更新人
	private Long updatePersonId;
	//更新日期
	private Date updateTime;
	//文件
	private byte[] fileContants;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProposeId() {
		return proposeId;
	}
	public void setProposeId(String proposeId) {
		this.proposeId = proposeId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Blob getFileStream() {
		return fileStream;
	}
	public void setFileStream(Blob fileStream) {
		this.fileStream = fileStream;
	}
	public Long getCreatePersonId() {
		return createPersonId;
	}
	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getUpdatePersonId() {
		return updatePersonId;
	}
	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public byte[] getFileContants() {
		return fileContants;
	}
	public void setFileContants(byte[] fileContants) {
		this.fileContants = fileContants;
	}
}
