package epros.draw.unredo.data;

import java.util.ArrayList;
import java.util.List;

import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;

/**
 * 
 * 记录一次撤销恢复的数据数据类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUndoRedoData {
	/** 使用场合：添加且编辑（添加泳池、协作框、关键活动（PA、KSF、KCP）默认置底）添加图形备份记录 */
	private List<JecnUnRedoFigureData> addFigureDataCloneList = null;
	/** 图形备份对象记录 */
	private List<JecnUnRedoFigureData> figureDataCloneList = null;
	/** 连接线备份对象记录 */
	private List<JecnUnRedoManhattanLineData> manhattanLineDataCloneList = null;
	/** 横竖分割线 */
	private List<JecnUnRedoVHLineData> vhLineDataCloneList = null;
	/** 分割线 */
	private List<JecnUnRedoDividingLineData> dividingLineDataList = null;

	/** 使用场合 ： 添加永道 - 新增横竖风格像 */
	private List<JecnUnRedoVHLineData> addVHLineDataCloneList = null;

	/**
	 * 
	 * 记录流程元素以及其数据
	 * 
	 * @param flowElem
	 *            JecnBaseFlowElementPanel 流程元素
	 * 
	 */
	public void recodeFlowElement(JecnBaseFlowElementPanel flowElem) {
		if (flowElem == null) {
			return;
		}
		if (flowElem instanceof JecnBaseFigurePanel) {// 图形
			JecnUnRedoFigureData unRedoFigureData = new JecnUnRedoFigureData((JecnBaseFigurePanel) flowElem);
			this.addCloneFigureData(unRedoFigureData);
		} else if (flowElem instanceof JecnBaseManhattanLinePanel) {// 连接线
			JecnUnRedoManhattanLineData unRedoManhattanLineData = new JecnUnRedoManhattanLineData(
					(JecnBaseManhattanLinePanel) flowElem);
			this.addCloneManhattanLineData(unRedoManhattanLineData);
		} else if (flowElem instanceof JecnBaseVHLinePanel) {// 横竖分割线
			JecnUnRedoVHLineData unRedoVHLineData = new JecnUnRedoVHLineData((JecnBaseVHLinePanel) flowElem);
			this.addCloneVHLineData(unRedoVHLineData);
		} else if (flowElem instanceof JecnBaseDividingLinePanel) {// 分割线
			JecnUnRedoDividingLineData unRedoDividingLineData = new JecnUnRedoDividingLineData(
					(JecnBaseDividingLinePanel) flowElem);
			this.addCloneDividingLineData(unRedoDividingLineData);
		}
	}

	/**
	 * 
	 * 使用场合：添加且编辑（添加泳池默认置底），添加图形记录在redo数据对象中
	 * 
	 * 添加图形记录
	 * 
	 * @param flowElem
	 *            JecnBaseFlowElementPanel
	 */
	public void recodeFlowElementByAddEdit(JecnBaseFlowElementPanel flowElem) {
		if (flowElem == null) {
			return;
		}
		if (flowElem instanceof JecnBaseFigurePanel) {// 图形
			JecnUnRedoFigureData unRedoFigureData = new JecnUnRedoFigureData((JecnBaseFigurePanel) flowElem);

			if (this.addFigureDataCloneList == null) {
				this.addFigureDataCloneList = new ArrayList<JecnUnRedoFigureData>();
			}
			this.addFigureDataCloneList.add(unRedoFigureData);
		}
	}

	public void recodeVHLineByAddEdit(JecnBaseFlowElementPanel flowElem) {
		if (flowElem == null || !(flowElem instanceof JecnBaseVHLinePanel)) {
			return;
		}
		JecnUnRedoVHLineData unRedoFigureData = new JecnUnRedoVHLineData((JecnBaseVHLinePanel) flowElem);

		if (this.addVHLineDataCloneList == null) {
			this.addVHLineDataCloneList = new ArrayList<JecnUnRedoVHLineData>();
		}
		this.addVHLineDataCloneList.add(unRedoFigureData);
	}

	/**
	 * 
	 * 记录流程元素以及其数据
	 * 
	 * @param flowElemList
	 *            List<JecnBaseFlowElementPanel> 流程元素
	 * 
	 */
	public void recodeFlowElement(List<JecnBaseFlowElementPanel> flowElemList) {
		if (flowElemList == null) {
			return;
		}
		for (JecnBaseFlowElementPanel flowElement : flowElemList) {
			recodeFlowElement(flowElement);
		}
	}

	/**
	 * 
	 * 添加图形数据对象到List中
	 * 
	 * @param cloneDividingLineData
	 */
	private void addCloneFigureData(JecnUnRedoFigureData JecnUnRedoFigureData) {
		if (JecnUnRedoFigureData == null) {
			return;
		}
		if (this.figureDataCloneList == null) {
			this.figureDataCloneList = new ArrayList<JecnUnRedoFigureData>();
		}

		if (!this.figureDataCloneList.contains(JecnUnRedoFigureData)) {// 不包含
			this.figureDataCloneList.add(JecnUnRedoFigureData);
		}
	}

	/**
	 * 
	 * 添加图形数据对象到List中
	 * 
	 * @param cloneDividingLineData
	 */
	private void addCloneManhattanLineData(JecnUnRedoManhattanLineData cloneManhattanLineData) {
		if (cloneManhattanLineData == null) {
			return;
		}
		if (this.manhattanLineDataCloneList == null) {
			this.manhattanLineDataCloneList = new ArrayList<JecnUnRedoManhattanLineData>();
		}

		if (!this.manhattanLineDataCloneList.contains(cloneManhattanLineData)) {// 不包含
			this.manhattanLineDataCloneList.add(cloneManhattanLineData);
		}

	}

	/**
	 * 
	 * 添加CloneVHLineData数据对象到List中
	 * 
	 * @param vhLineDataCloneList
	 */
	private void addCloneVHLineData(JecnUnRedoVHLineData unRedoVHLineData) {
		if (unRedoVHLineData == null) {
			return;
		}
		if (this.vhLineDataCloneList == null) {
			this.vhLineDataCloneList = new ArrayList<JecnUnRedoVHLineData>();
		}
		if (!this.vhLineDataCloneList.contains(unRedoVHLineData)) {// 不包含
			this.vhLineDataCloneList.add(unRedoVHLineData);
		}

	}

	/**
	 * 
	 * 添加dividingLineData数据对象到分割线List中
	 * 
	 * @param dividingLineData
	 *            JecnUnRedoDividingLineData 分割线备份数据对象
	 */
	private void addCloneDividingLineData(JecnUnRedoDividingLineData dividingLineData) {
		if (dividingLineData == null) {
			return;
		}
		if (this.dividingLineDataList == null) {
			this.dividingLineDataList = new ArrayList<JecnUnRedoDividingLineData>();
		}

		if (!this.dividingLineDataList.contains(dividingLineData)) {// 不包含
			this.dividingLineDataList.add(dividingLineData);
		}
	}

	public List<JecnUnRedoFigureData> getFigureDataCloneList() {
		return figureDataCloneList;
	}

	public List<JecnUnRedoManhattanLineData> getManhattanLineDataCloneList() {
		return manhattanLineDataCloneList;
	}

	public List<JecnUnRedoVHLineData> getVhLineDataCloneList() {
		return vhLineDataCloneList;
	}

	public List<JecnUnRedoDividingLineData> getDividingLineDataList() {
		return dividingLineDataList;
	}

	public List<JecnUnRedoFigureData> getAddFigureDataCloneList() {
		return addFigureDataCloneList;
	}

	public List<JecnUnRedoVHLineData> getAddVHLineDataCloneList() {
		return addVHLineDataCloneList;
	}

}
