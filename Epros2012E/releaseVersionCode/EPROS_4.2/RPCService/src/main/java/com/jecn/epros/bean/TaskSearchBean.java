package com.jecn.epros.bean;


import java.io.Serializable;

public class TaskSearchBean implements Serializable{

    /***任务名称**/
    private String taskName;

    /***创建人Id**/
    private Long createPeopleId;

    /***任务阶段**/
    private int taskStage = -1;

    /***任务类型***/
    private int taskType = -1;

    /***开始时间**/
    private String startTime;

    /***结束时间**/
    private String endTime;

    /***搁置任务**/
    private boolean delayTask = false;

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Long getCreatePeopleId() {
        return createPeopleId;
    }

    public void setCreatePeopleId(Long createPeopleId) {
        this.createPeopleId = createPeopleId;
    }

    public int getTaskStage() {
        return taskStage;
    }

    public void setTaskStage(int taskStage) {
        this.taskStage = taskStage;
    }

    public int getTaskType() {
        return taskType;
    }

    public void setTaskType(int taskType) {
        this.taskType = taskType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isDelayTask() {
        return delayTask;
    }

    public void setDelayTask(boolean delayTask) {
        this.delayTask = delayTask;
    }

}
