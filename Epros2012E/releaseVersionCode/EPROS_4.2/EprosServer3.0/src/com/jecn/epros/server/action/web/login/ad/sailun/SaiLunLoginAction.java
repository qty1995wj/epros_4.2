package com.jecn.epros.server.action.web.login.ad.sailun;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

public class SaiLunLoginAction extends JecnAbstractADLoginAction {

	public SaiLunLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 赛轮单点登录入口
	 * 
	 * @desc 先验证是否存在用户名和密码，如果不存在走AD域验证
	 * 
	 * @author huoyl
	 * 
	 * @return String
	 */
	public String login() {
		try {

			// 单点登录名称
			String adLoginName = this.loginAction.getLoginName();
			// 单点登录密码
			String adPassWord = this.loginAction.getPassword();

			if ("admin".equals(adLoginName)) {
				return loginAction.userLoginGen(true);
			}
			// 直接传参登陆
			if (!StringUtils.isBlank(adLoginName) && !StringUtils.isBlank(adPassWord)) {
				if (!doDomainAuth()) {
					log.error("SaiLunLoginAction 域验证失败:" + adLoginName);
					return LoginAction.INPUT;
				}
				// 普通
				return loginAction.userLoginGen(false);
			} else {
				// 域登录
				return doDomainLogin();
			}

		} catch (Exception e) {
			log.error("SaiLunLoginAction 获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	/**
	 * 域验证
	 * 
	 * @param userName
	 * @param pwd
	 * @return
	 */
	private boolean doDomainAuth() {
		DirContext ctx = null;
		String account = loginAction.getLoginName(); // 设置访问账号
		String password = loginAction.getPassword(); // 设置账号密码
		log.info("用户名：" + account);
		log.info("密码：" + password);
		String host = null;
		String root = "389"; // root
		if (account != null) {
			String[] domainName = account.split("\\\\");
			if (domainName != null && domainName.length > 0) {
				// 域前缀
				String domain = domainName[0].toLowerCase();
				host = SaiLunAfterItem.map.get(domain);
				if (domain.equals("sailunjinyu")) {
					account = domainName[1] + "@" + "sailunjinyu.com";
				}
				log.info("域IP地址：" + host);
			} else {
				log.error("域IP地址为空！");
			}
		}
		log.info("用户名==" + account);
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.PROVIDER_URL, "ldap://" + host + ":" + root);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, account);
		env.put(Context.SECURITY_CREDENTIALS, password);
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		try {
			ctx = new InitialDirContext(env);
			log.info("SaiLunLoginAction AD域验证成功!");
		} catch (AuthenticationException e) {
			log.error("SaiLunLoginAction AD域验证失败！！！", e);
			return false;
		} catch (Exception e) {
			log.error("SaiLunLoginAction AD域验证异常！！！", e);
			return false;
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					log.error("SaiLunLoginAction InitialDirContext关闭异常...", e);
				}
			}
		}
		return true;
	}

	/**
	 * 域登录
	 * 
	 * @return
	 */
	private String doDomainLogin() {
		HttpServletRequest request = this.loginAction.getRequest();
		log.info("本地域用户：" + request.getRemoteUser());
		// 获取本地域信息
		String emoteUser = request.getRemoteUser();
		// 获取域信息的情况下 进行域登录
		if (StringUtils.isNotBlank(emoteUser)) {
			this.loginAction.setLoginName(emoteUser);
			// 不验证密码登录（域账号存在于Epros中时登陆成功）
			return this.loginAction.userLoginGen(false);
		} else {
			// 返回登录页面
			return LoginAction.INPUT;
		}
	}

}
