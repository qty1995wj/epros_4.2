package com.jecn.epros.server.webBean.file;

public class FileSearchBean {
	/**文件名称*/
	private String fileName;
	/**文件编号*/
	private String fileNum;
	/** 责任部门 */
	private long orgId ;
	/** 责任部门名称 */
	private String orgName;
	/** 查阅部门 */
	private long orgLookId ;
	/** 查阅部门名称 */
	private String orgLookName;
	/** 查阅岗位 */
	private long posLookId ;
	/** 查阅岗位名称 */
	private String posLookName;
	/** 密级 */
	private int secretLevel ;
	/**是否是管理员*/
	private boolean isAdmin;
	/**项目Id*/
	private Long projectId;
	/**登录人员Id*/
	private Long peopleId;
	public boolean isAdmin() {
		return isAdmin;
	}
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public Long getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileNum() {
		return fileNum;
	}
	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public long getOrgLookId() {
		return orgLookId;
	}
	public void setOrgLookId(long orgLookId) {
		this.orgLookId = orgLookId;
	}
	public String getOrgLookName() {
		return orgLookName;
	}
	public void setOrgLookName(String orgLookName) {
		this.orgLookName = orgLookName;
	}
	public long getPosLookId() {
		return posLookId;
	}
	public void setPosLookId(long posLookId) {
		this.posLookId = posLookId;
	}
	public String getPosLookName() {
		return posLookName;
	}
	public void setPosLookName(String posLookName) {
		this.posLookName = posLookName;
	}
	public int getSecretLevel() {
		return secretLevel;
	}
	public void setSecretLevel(int secretLevel) {
		this.secretLevel = secretLevel;
	}
}
