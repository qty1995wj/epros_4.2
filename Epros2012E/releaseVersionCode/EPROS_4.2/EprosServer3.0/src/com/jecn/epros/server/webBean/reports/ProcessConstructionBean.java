package com.jecn.epros.server.webBean.reports;

import java.util.List;

import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 流程建设覆盖度统计Bean
 * @author fuzhh Mar 4, 2013
 *
 */
public class ProcessConstructionBean {

	/** 审批中个数 */
	private int eaaCount=0;
	/** 已发布个数 */
	private int releasedCount=0;
	/** 待建个数 */
	private int tbbCount=0;
	/** 流程具体数据bean*/
	private List<ProcessWebBean> processWebList;
	
	public List<ProcessWebBean> getProcessWebList() {
		return processWebList;
	}

	public void setProcessWebList(List<ProcessWebBean> processWebList) {
		this.processWebList = processWebList;
	}

	public int getEaaCount() {
		return eaaCount;
	}

	public void setEaaCount(int eaaCount) {
		this.eaaCount = eaaCount;
	}

	public int getReleasedCount() {
		return releasedCount;
	}

	public void setReleasedCount(int releasedCount) {
		this.releasedCount = releasedCount;
	}

	public int getTbbCount() {
		return tbbCount;
	}

	public void setTbbCount(int tbbCount) {
		this.tbbCount = tbbCount;
	}
}
