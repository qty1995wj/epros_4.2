package com.jecn.svg.dataModel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SvgTextStyle {

	@JsonIgnore
	// 0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下
	public static final String[] textAnchors = new String[] { "left-top", "center-top", "right-top", "left-middle",
			"center-middle", "right-middle", "left-bottom", "center-bottom", "right-bottom" };

	private String family;
	private String anchor;
	private Integer size;
	private String weight;
	private String fill;

	@JsonProperty("font-family")
	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	@JsonProperty("text-anchor")
	public String getAnchor() {
		return anchor;
	}

	public void setAnchor(String anchor) {
		this.anchor = anchor;
	}

	@JsonProperty("font-size")
	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	@JsonProperty("font-weight")
	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getFill() {
		return fill;
	}

	public void setFill(String fill) {
		this.fill = fill;
	}
}
