package epros.designer.gui.publish;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 流程发布
 * 
 * @author Administrator 2012-10-23
 */
public class FlowPublishDialog extends BasePublishDialog {

	public FlowPublishDialog(JecnTreeNode treeNode) {
		super(treeNode);
		if (TreeNodeType.process != treeBean.getTreeNodeType()) {
			log.error("FlowPublishDialog ！　treeBean.getTreeNodeType()　＝"
					+ treeBean.getTreeNodeType());
			return;
		}
	}

	/**
	 * 初始化变量
	 * 
	 */
	public void initialize() {
		type = 0;
		prfName = JecnProperties.getValue("flowNameC");
		strTitle = JecnProperties.getValue("processPubI");
	}
}
