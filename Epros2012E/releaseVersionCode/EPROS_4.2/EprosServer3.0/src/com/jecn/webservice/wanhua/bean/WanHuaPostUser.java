package com.jecn.webservice.wanhua.bean;

public class WanHuaPostUser {
	/** 工号 */
	private String employeeNum;
	/** 系统用户名  = 邮箱 ： account@whchem.com */
	private String account;
	/** 全名 */
	private String fullName;
	/** 职位ID */
	private String posCode;
	/** 职位名称 */
	private String posName;
	private String orgId;
	/** 姓 */
	private String lastName;
	/** 名 */
	private String firstName;

	public String getEmployeeNum() {
		return employeeNum;
	}

	public void setEmployeeNum(String employeeNum) {
		this.employeeNum = employeeNum;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPosCode() {
		return posCode;
	}

	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

}
