package epros.designer.gui.system;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import epros.designer.gui.common.JecnTextArea;
import epros.draw.gui.swing.JecnPanel;

/**
 * 包含文本域的滚动面板
 * 
 * @author ZXH
 * @date 2017-2-7下午02:22:41
 */
public class CommonJScrollPaneTextArea {
	private JecnTextArea textArea = null;
	private JLabel label = null;
	private JScrollPane jScrollPane = null;

	public CommonJScrollPaneTextArea(int rows, JecnPanel infoPanel, Insets insets, String labName) {
		textArea = new JecnTextArea();
		textArea.setRows(4);
		jScrollPane = new JScrollPane(textArea);
		jScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		jScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		label = new JLabel(labName);
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(label, c);

		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(jScrollPane, c);
	}

	public JecnTextArea getTextArea() {
		return textArea;
	}
}
