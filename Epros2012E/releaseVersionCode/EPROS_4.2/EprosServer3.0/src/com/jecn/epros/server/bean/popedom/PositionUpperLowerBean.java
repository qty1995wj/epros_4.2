package com.jecn.epros.server.bean.popedom;

public class PositionUpperLowerBean implements java.io.Serializable{

	private Long id;//主键ID
	private Long leveType;//类型 0是上级、1是下属
	private Long positionId;//岗位ID
	private Long levePositionId;//上级或下级岗位ID 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLeveType() {
		return leveType;
	}
	public void setLeveType(Long leveType) {
		this.leveType = leveType;
	}
	public Long getPositionId() {
		return positionId;
	}
	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}
	public Long getLevePositionId() {
		return levePositionId;
	}
	public void setLevePositionId(Long levePositionId) {
		this.levePositionId = levePositionId;
	}
}
