<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div >
			<div id="search" class="divBorder" align="center">
				<div id="standardSystemSearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td width="10%" height="25px" align="right">
								<!-- 标准名称： -->
								<s:text name="standardNameC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%" id="standardName_id"
									maxLength="32" />
							</td>


							<td width="10%" align="right">
								<!-- 类型: -->
								<s:text name="typeC" />
							</td>
							<td width="35%" align="left">
								<select style="width: 80%" id="standardType_id">
									<!-- 全部 -->
									<option value="-1">
										<s:text name="all" />
									</option>
									<!-- 文件 -->
									<option value="1">
										<s:text name="file" />
									</option>
									<!-- 流程 -->
									<option value="2">
										<s:text name="flow" />
									</option>
									<!-- 流程地图 -->
									<option value="3">
										<s:text name="flowMap" />
									</option>
									<!-- 条款 -->
									<option value="4">
										<s:text name="clause" />
									</option>
									<!-- 条款要求-->
									<option value="5">
										<s:text name="clauseDemand" />
									</option>
								</select>
							</td>

							<td style="vertical-align: middle" width="10%">
								<a onClick="toggle(standardSystemSearchText)"
									style="cursor: pointer;" class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" />
								</span> </a>
							</td>
						</tr>
						<tr>
							<td align="right">
								<!-- 查阅部门: -->
								<s:text name="referToTheDepartmentC" />
							</td>
							<td align="left">
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="orgLookName_standard_id"
									onkeyup="search(orgLookName_standard_id,orgLookId_standard_id,search_standard_orgLookName,'org');"
									onblur="divNode(search_standard_orgLookName)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("organization","orgLookName_standard_id","orgLookId_standard_id")'>
									<img style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="search_standard_orgLookName"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td height="25px" align="right">
								<!-- 密级: -->
								<s:text name="intensiveC" />
							</td>
							<td align="left">
								<select style="width: 80%" id="secretLevel_id">
									<option value="-1">
										<s:text name="all" />
									</option>
									<option value="1">
										<s:property value="#application.public"/>
									</option>
									<option value="0">
										${secret}
									</option>
								</select>
							</td>

							<td></td>
						</tr>
						<tr>
							<td align="right" height="25px">
								<!-- 查阅岗位： -->
								<s:text name="referToThePostC"></s:text>
							</td>
							<td align="left">
								<input type="text" type="text"
									style="width: 63%; vertical-align: middle;"
									id="posLookName_standard_id"
									onkeyup="search(posLookName_standard_id,posLookId_standard_id,search_standard_posLookName,'pos');"
									onblur="divNode(search_standard_posLookName)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("position","posLookName_standard_id","posLookId_standard_id")'>
									<img style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="search_standard_posLookName"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td></td>
							<td></td>

							<td></td>
						</tr>
					</table>
					<!-- 查阅岗位隐藏域ID -->
					<input type="hidden" id="posLookId_standard_id" />
					<!-- 查阅岗位隐藏域ID -->
					<input type="hidden" id="orgLookId_standard_id" />
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="standardSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="standardSearchReset()" />
				</div>
			</div>

			<div id="standardGrid_id">

			</div>
		</div>
	</body>
</html>