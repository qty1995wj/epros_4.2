package com.jecn.webservice.jbshihua.util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang3.StringUtils;

import com.jecn.webservice.jbshihua.entity.DeptInfo;

@XmlRootElement(
    name = "DATAINFO"
)
public class HrDeptMsg {
    private String uuid;
    private String desc1;
    private String desc2;
    private String desc3;
    private String desc4;
    private String desc10;
    private String desc14;
    private String desc17;
    private String desc18;
    private String desc19;
    private String code;
    private String codeid;
    private String parentcode;

    public HrDeptMsg() {
    }

    @XmlElement(
        name = "UUID"
    )
    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @XmlElement(
        name = "DESC1"
    )
    public String getDesc1() {
        return this.desc1;
    }

    public void setDesc1(String desc1) {
        this.desc1 = desc1;
    }

    @XmlElement(
        name = "DESC2"
    )
    public String getDesc2() {
        return this.desc2;
    }

    public void setDesc2(String desc2) {
        this.desc2 = desc2;
    }

    @XmlElement(
        name = "DESC3"
    )
    public String getDesc3() {
        return this.desc3;
    }

    public void setDesc3(String desc3) {
        this.desc3 = desc3;
    }

    @XmlElement(
        name = "DESC4"
    )
    public String getDesc4() {
        return this.desc4;
    }

    public void setDesc4(String desc4) {
        this.desc4 = desc4;
    }

    @XmlElement(
        name = "DESC10"
    )
    public String getDesc10() {
        return this.desc10;
    }

    public void setDesc10(String desc10) {
        this.desc10 = desc10;
    }

    @XmlElement(
        name = "DESC14"
    )
    public String getDesc14() {
        return this.desc14;
    }

    public void setDesc14(String desc14) {
        this.desc14 = desc14;
    }

    @XmlElement(
        name = "DESC17"
    )
    public String getDesc17() {
        return this.desc17;
    }

    public void setDesc17(String desc17) {
        this.desc17 = desc17;
    }

    @XmlElement(
        name = "DESC18"
    )
    public String getDesc18() {
        return this.desc18;
    }

    public void setDesc18(String desc18) {
        this.desc18 = desc18;
    }

    @XmlElement(
        name = "DESC19"
    )
    public String getDesc19() {
        return this.desc19;
    }

    public void setDesc19(String desc19) {
        this.desc19 = desc19;
    }

    @XmlElement(
        name = "CODE"
    )
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlElement(
        name = "CODEID"
    )
    public String getCodeid() {
        return this.codeid;
    }

    public void setCodeid(String codeid) {
        this.codeid = codeid;
    }

    @XmlElement(
        name = "PARENTCODE"
    )
    public String getParentcode() {
        return this.parentcode;
    }

    public void setParentcode(String parentcode) {
        this.parentcode = parentcode;
    }

    public DeptInfo getDept() {
        DeptInfo dept = new DeptInfo();
        dept.setDeptCode(this.code);
        if (StringUtils.isNotBlank(this.desc19)) {
            dept.setDeptName(this.desc19);
        } else {
            dept.setDeptName(this.desc1);
        }

        dept.setDeptParentCode(this.getParentcode());
        dept.setDeptType("dr");
//        dept.setDeptStatus("终止".equals(this.desc17) ? 0 : 1);
        return dept;
    }
}
