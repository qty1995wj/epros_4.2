package epros.draw.gui.workflow.aid;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnToFromRelatedData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.LinkButton;
import epros.draw.util.JecnUIUtil;

public class JecnElemRelatedActivePanel extends JPanel {

	private JecnDrawDesktopPane desktopPane = null;
	private JPanel mainPanel = null;

	public JecnElemRelatedActivePanel(JecnDrawDesktopPane desktopPane) {
		if (desktopPane == null) {
			JecnLogConstants.LOG_DRAW_SCROLL_PANE.error("JecnElemRelatedActivePanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.desktopPane = desktopPane;

		initComponents();
	}

	private void initComponents() {
		mainPanel = new JecnPanel();
		this.add(mainPanel);
		desktopPane.add(this);
		// 布局
		mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 4, 4));
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 边框
		this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		// this.setVisible(false);
	}

	public void update(JecnBaseFlowElementPanel flowElementPanel) {
		// 先移除所有的组件
		mainPanel.removeAll();
		// 获得所有的活动
		JecnFlowMapData flowMapData = this.desktopPane.getFlowMapData();
		List<JecnFigureData> figureList = flowMapData.getFigureList();

		JecnToFromRelatedData flowElementData = (JecnToFromRelatedData) flowElementPanel.getFlowElementData();
		List<String> activeNumList = new ArrayList<String>();
		List<JecnActiveData> activeList = new ArrayList<JecnActiveData>();
		for (JecnFigureData figure : figureList) {
			if (figure instanceof JecnActiveData) {
				JecnActiveData active = (JecnActiveData) figure;
				String activeNum = active.getActivityNum();
				if (StringUtils.isNotBlank(activeNum)) {

					activeNumList.add(activeNum);
					activeList.add(active);
				}
			}
		}

		int numOfRow = 8;
		int numOfCol = activeNumList.size() % numOfRow == 0 ? activeNumList.size() / numOfRow
				: (activeNumList.size() / numOfRow) + 1;

		int maxWidth = 0;
		int height = numOfRow * 25;
		int width = 0;

		this.mainPanel.setLayout(new GridLayout(numOfRow, numOfCol));

		// 排序:按照活动编号升序排序
		sortJecnActiveDataList(activeList);

		// 获得最大字符最多的元素的长度
		for (String num : activeNumList) {
			if (num.length() > maxWidth) {
				maxWidth = num.length();
			}
		}

		width = numOfCol * maxWidth * 12;

		LinkButton elem = null;
		for (JecnActiveData active : activeList) {
			elem = new LinkButton(this, flowElementPanel, active);
			// 如果元素关联的是该活动那么修改颜色以示区别
			if (active.getFlowElementUUID().equals(flowElementData.getRelatedUUId())) {
				elem.setForeground(Color.RED);
				this.mainPanel.add(elem.getJToolBar(), 0);
			} else {
				this.mainPanel.add(elem.getJToolBar());
			}
		}

		this.setLocation(flowElementPanel.getX() + 10 + flowElementPanel.getWidth(), flowElementPanel.getY());
		this.setSize(width, height);
		this.updateUI();
		desktopPane.setLayer(this, desktopPane.getMaxZOrderIndex());
		this.setVisible(true);
	}

	private void sortJecnActiveDataList(List<JecnActiveData> list) {
		if (list == null || list.size() == 0) {
			return;
		}

		Collections.sort(list, new Comparator<JecnActiveData>() {
			public int compare(JecnActiveData r1, JecnActiveData r2) {
				if (r1 == null || r2 == null || StringUtils.isEmpty(r1.getActivityNum())
						|| StringUtils.isEmpty(r2.getActivityNum())) {
					throw new IllegalArgumentException("参数不合法");
				}
				return r1.getActivityNum().compareTo(r2.getActivityNum());
			}
		});
	}

	public void hiddenReleateActivePanel() {
		this.setVisible(false);
	}

	/**
	 * to、from元素字段关联编号
	 * 
	 * @param flowElementPanel
	 * @param active
	 */
	public void actionPerformed(JecnBaseFlowElementPanel flowElementPanel, JecnActiveData active) {
		JecnToFromRelatedData relatedData = (JecnToFromRelatedData) flowElementPanel.getFlowElementData();
		String showText = getShowText(relatedData);
		if (active.getFlowElementUUID().equals(relatedData.getRelatedUUId())) {
			relatedData.setRelatedId(null);
			relatedData.setRelatedUUId(null);
			flowElementPanel.getFlowElementData().setFigureText(showText);
		} else {
			relatedData.setRelatedUUId(active.getFlowElementUUID());
			flowElementPanel.getFlowElementData().setFigureText(showText + " " + active.getActivityNum());
		}

		flowElementPanel.updateUI();
		this.setVisible(false);
	}

	public void refreshRelatedText(JecnBaseFlowElementPanel flowElementPanel, JecnActiveData active) {
		JecnToFromRelatedData relatedData = (JecnToFromRelatedData) flowElementPanel.getFlowElementData();
		String showText = getShowText(relatedData);
		relatedData.setRelatedUUId(active.getFlowElementUUID());
		flowElementPanel.getFlowElementData().setFigureText(showText + " " + active.getActivityNum());
		flowElementPanel.updateUI();
	}

	private String getShowText(JecnToFromRelatedData relatedData) {
		String showText = "";
		if (relatedData.getMapElemType() == MapElemType.ToFigure) {
			showText = "To";
			// 创建from图形
		} else if (relatedData.getMapElemType() == MapElemType.FromFigure) {
			showText = "From";
			// 创建to图形
		}
		return showText;
	}
}
