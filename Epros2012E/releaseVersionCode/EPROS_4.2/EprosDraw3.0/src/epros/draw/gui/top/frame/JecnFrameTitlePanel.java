package epros.draw.gui.top.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 窗体标题栏:类似JFrame控件的标题栏面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFrameTitlePanel extends JecnFrameDropTitlePanel {
	/** 日志对象 */
	private final Log log = LogFactory.getLog(JecnFrameTitlePanel.class);

	/** Epros窗体标题 */
	private JLabel titleLabel = null;
	/** 标题内容 */
	private String title = "";
	/** 标题图片 */
	private Image icon;

	/** 最小化按钮 */
	private JButton minBtn = null;
	/** 最大化按钮 */
	private JButton resizeBtn = null;
	/** 关闭按钮 */
	private JButton exitBtn = null;

	/** 空闲面板 */
	private JPanel otherPanel = null;

	/** 最小化按钮图片：进入时显示 */
	private ImageIcon enterMinIcon = null;
	/** 最小化按钮图片 */
	private ImageIcon minIcon = null;

	/** 关闭按钮图片：进入时显示 */
	private ImageIcon enterExitIcon = null;
	/** 关闭按钮图片 */
	private ImageIcon exitIcon = null;

	/** 最大化按钮图片：进入时显示 */
	private ImageIcon enterResizeIcon = null;
	private ImageIcon enterResizeMaxIcon = null;
	/** 最大化按钮图片 */
	private ImageIcon resizeIcon = null;
	private ImageIcon resizeMaxIcon = null;

	public JecnFrameTitlePanel(JecnFrame frame) {
		super(frame);
		// 实例化子组件
		initComponents();
		// 布局子组件
		layoutComponents();
		// 初始化事件
		initMouseEvent();
	}

	/**
	 * 
	 * 实例化子组件
	 * 
	 */
	private void initComponents() {
		// **********************图片加载**********************//
		// 窗体默认图标
		icon = getDefaultImage();
		// 最小化按钮图片：进入时显示
		enterMinIcon = JecnFileUtil.getFrameBtnIcon("enterMinFrameBtn.gif");
		// 最小化按钮图片
		minIcon = JecnFileUtil.getFrameBtnIcon("minFrameBtn.jpg");

		// 关闭按钮图片：进入时显示
		enterExitIcon = JecnFileUtil.getFrameBtnIcon("enterExitFrameBtn.gif");
		// 关闭按钮图片
		exitIcon = JecnFileUtil.getFrameBtnIcon("exitFrameBtn.jpg");

		// 最大化按钮图片：进入时显示
		enterResizeIcon = JecnFileUtil.getFrameBtnIcon("enterResizeIcon.gif");
		enterResizeMaxIcon = JecnFileUtil
				.getFrameBtnIcon("enterMaxFrameBtn.gif");
		// 最大化按钮图片
		resizeIcon = JecnFileUtil.getFrameBtnIcon("resizeIcon.gif");
		resizeMaxIcon = JecnFileUtil.getFrameBtnIcon("maxFrameBtn.jpg");
		// **********************图片加载**********************//

		// Epros窗体标题吃哦
		titleLabel = new JLabel();
		// 最小化按钮
		minBtn = new JButton(minIcon);
		// 最大化按钮
		resizeBtn = new JButton(resizeMaxIcon);
		// 关闭按钮
		exitBtn = new JButton(exitIcon);
		// 空闲面板
		otherPanel = new JPanel();

		// 设置布局管理器
		this.setLayout(new GridBagLayout());
		// 大小
		Dimension size = new Dimension(135, 30);
		this.setPreferredSize(size);
		this.setMinimumSize(size);

		// 前景颜色
		Dimension titleLabelSize = new Dimension(300, 30);
		titleLabel.setPreferredSize(titleLabelSize);
		titleLabel.setMinimumSize(titleLabelSize);
		titleLabel.setForeground(JecnUIUtil.getFrameTtileNameColor());
		titleLabel.setFont(new Font(Font.DIALOG, Font.BOLD, 14));
		titleLabel.setHorizontalTextPosition(SwingConstants.RIGHT);
		titleLabel.setVerticalTextPosition(SwingConstants.CENTER);
		// 透明
		titleLabel.setOpaque(false);
		titleLabel.setBorder(null);

		// 透明
		otherPanel.setOpaque(false);
		otherPanel.setLayout(new BorderLayout());

		// 名称
		minBtn.setName("minBtn");
		resizeBtn.setName("maxBtn");
		exitBtn.setName("exitBtn");

		// 不让绘制内容区域
		minBtn.setContentAreaFilled(false);
		resizeBtn.setContentAreaFilled(false);
		exitBtn.setContentAreaFilled(false);

		// 设置按钮边框和标签之间的空白
		minBtn.setMargin(JecnUIUtil.getInsets0());
		resizeBtn.setMargin(JecnUIUtil.getInsets0());
		exitBtn.setMargin(JecnUIUtil.getInsets0());

		// 隐藏边框
		minBtn.setBorder(null);
		resizeBtn.setBorder(null);
		exitBtn.setBorder(null);
		minBtn.setBorderPainted(false);
		resizeBtn.setBorderPainted(false);
		exitBtn.setBorderPainted(false);

		// 隐藏选择焦点
		minBtn.setFocusable(false);
		resizeBtn.setFocusable(false);
		exitBtn.setFocusable(false);

		// 事件
		minBtn.addActionListener(this);
		resizeBtn.addActionListener(this);
		exitBtn.addActionListener(this);
	}

	/**
	 * 
	 * 初始化事件
	 * 
	 */
	private void initMouseEvent() {
		// 最小化按钮
		minBtn.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				minBtn.setIcon(enterMinIcon);
			}

			public void mouseExited(MouseEvent e) {
				minBtn.setIcon(minIcon);
			}
		});

		// 最大化按钮
		resizeBtn.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				if (!isMaxFlag()) {// 不是全屏
					resizeBtn.setIcon(enterResizeIcon);
				} else {
					resizeBtn.setIcon(enterResizeMaxIcon);
				}
			}

			public void mouseExited(MouseEvent e) {
				if (!isMaxFlag()) {// 不是全屏
					resizeBtnResizeIcon();
				} else {
					resizeBtnMaxIcon();
				}
			}
		});

		// 关闭按钮
		exitBtn.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				exitBtn.setIcon(enterExitIcon);
			}

			public void mouseExited(MouseEvent e) {
				exitBtn.setIcon(exitIcon);
			}
		});
	}

	/**
	 * 
	 * 布局子组件
	 * 
	 */
	private void layoutComponents() {

		// Epros窗体标题
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(2, 3, 0, 0), 0, 0);
		this.add(titleLabel, c);

		// 空闲面板
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(otherPanel, c);

		// 最小化按钮
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(minBtn, c);

		// 最大化按钮
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(resizeBtn, c);

		// 关闭按钮
		c = new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(exitBtn, c);
	}

	/**
	 * 
	 * 添加标题
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
		this.titleLabel.setText(title);
		this.repaint();
	}

	public String getTitle() {
		return title;
	}

	public Image getIcon() {
		if (icon == null) {
			return getDefaultImage();
		}
		return icon;
	}

	/**
	 * 
	 * 窗体图标
	 * 
	 * @param iconImage
	 */
	public void setIcon(Image iconImage) {
		this.icon = iconImage;
		if (iconImage != null) {
			titleLabel.setIcon(new ImageIcon(iconImage));
			this.validate();
			this.repaint();
		}
	}

	/**
	 * 
	 * 
	 * 设置是否支持用户调整大小
	 * 
	 * @param resizable
	 */
	public void setResizable(boolean resizable) {
		super.setResizable(resizable);
		resizeBtn.setVisible(resizable);
	}

	/**
	 * 
	 * 返回默认图标
	 * 
	 * @return
	 */
	private Image getDefaultImage() {
		return JecnFileUtil.getFrameBtnIcon("topicom.png").getImage();
	}

	JPanel getOtherPanel() {
		return otherPanel;
	}

	JButton getResizeBtn() {
		return resizeBtn;
	}

	void resizeBtnMaxIcon() {
		resizeBtn.setIcon(resizeMaxIcon);
	}

	void resizeBtnResizeIcon() {
		resizeBtn.setIcon(resizeIcon);
	}

	/**
	 * 
	 * 
	 * 是否显示最小按钮
	 * 
	 * @param resizable true：显示；false：不显示
	 */
	public void setMinBtnShow(boolean resizable) {
		minBtn.setVisible(resizable);
	}
	
	/**
	 * 
	 * 
	 * 是否显示关闭按钮
	 * 
	 * @param resizable true：显示；false：不显示
	 */
	public void setExitBtnShow(boolean resizable) {
		exitBtn.setVisible(resizable);
	}
}
