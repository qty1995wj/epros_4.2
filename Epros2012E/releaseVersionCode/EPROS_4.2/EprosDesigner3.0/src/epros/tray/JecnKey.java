package epros.tray;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import jecntool.cmd.JecnCmd;
import jecntool.cmd.ResultBean;

import org.apache.log4j.Logger;

import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;

public class JecnKey {
	private static Logger log = Logger.getLogger(JecnKey.class);
	/** 密钥 文件路径 */
	private static String keyFilePath = JecnFileUtil.getSyncPath("/")
			+ "server/jecn/EprosServer3.0/keylist.properties";
	/** 密钥,可自定义 */
	private static String key = "jecn";
	/** 密钥 关键字 */
	private static String keyName = "key.license";

	/**
	 * @author zhangchen May 17, 2012
	 * @description：是否存在密钥
	 * @return
	 */
	public static boolean isKeyExist() {
		File f = new File(keyFilePath);
		if (f.exists()) {
			return true;
		}
		log.info("JecnKey isKeyExist is error！");
		return false;
	}

	/***************************************************************************
	 * 获得加密密钥
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String getkeyEncrypt() {
		InputStream in = null;
		try {
			File file = new File(keyFilePath);
			Properties properties = new Properties();
			in = new FileInputStream(file);
			properties.load(in);
			String keyEncrypt = properties.getProperty(keyName);
			in.close();
			if (keyEncrypt == null) {
				return "";
			}
			return keyEncrypt;
		} catch (Exception ex) {

		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {

				}
			}
		}
		return "";
	}

	/**
	 * 获得解密的密钥
	 * 
	 * @param keyLicense
	 * @return
	 */
	public static String getKeyDecrypt(String keyEncrypt) {
		if (keyEncrypt == null || "".equals(keyEncrypt)) {
			return "";
		}
		return decrypt(keyEncrypt, key);
	}

	private static String base64_decode(String str) {

		sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();

		if (str == null) {
			return null;
		}

		try {

			return new String(decoder.decodeBuffer(str));

		} catch (IOException e) {
			log.error("", e);
			return null;

		}

	}

	private static final String strMD5(String s) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };

		try {

			byte[] strTemp = s.getBytes();

			MessageDigest mdTemp = MessageDigest.getInstance("MD5");

			mdTemp.update(strTemp);

			byte[] md = mdTemp.digest();

			int j = md.length;

			char str[] = new char[j * 2];

			int k = 0;

			for (int i = 0; i < j; i++) {

				byte byte0 = md[i];

				str[k++] = hexDigits[byte0 >>> 4 & 0xf];

				str[k++] = hexDigits[byte0 & 0xf];

			}

			return new String(str);

		} catch (Exception e) {
			log.error("", e);
		}

		return null;

	}

	private static String key(String txt, String encrypt_key) {

		encrypt_key = strMD5(encrypt_key);

		int ctr = 0;

		String tmp = "";

		for (int i = 0; i < txt.length(); i++) {

			ctr = (ctr == encrypt_key.length()) ? 0 : ctr;

			int c = txt.charAt(i) ^ encrypt_key.charAt(ctr);

			String x = "" + (char) c;

			tmp = tmp + x;

			ctr++;

		}

		return tmp;

	}

	/**
	 * 
	 * 解密算法
	 * 
	 */
	private static String decrypt(String cipherText, String key) {

		// base64解码

		cipherText = base64_decode(cipherText);

		cipherText = key(cipherText, key);

		String tmp = "";

		for (int i = 0; i < cipherText.length(); i++) {

			int c = cipherText.charAt(i) ^ cipherText.charAt(i + 1);

			String x = "" + (char) c;

			tmp += x;

			i++;

		}

		return tmp;

	}

	/**
	 * @author zhangchen Aug 3, 2012
	 * @description：获得本机的物理地址
	 * @return
	 * @throws IOException
	 */
	public static String getMacAddress() throws IOException {
		String os = System.getProperty("os.name");
		if (os.startsWith("Windows")) {
			ResultBean retBean = JecnCmd.getMacAddr();
			if (retBean.getResultState() == 0) {
				return retBean.getResultStr();
			} else {
				return "";
			}
		} else {
			throw new IOException("OS not supported : " + os);
		}
	}

	/**
	 * @author zhangchen May 17, 2012
	 * @description：注册的网卡是否匹配
	 * @return
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	public static boolean isRegisterSuccess() throws IOException,
			NoSuchAlgorithmException {
		// 新版注册码MAC地址验证
		String keyDecrypt = getKeyDecrypt(getkeyEncrypt());
		String str[] = keyDecrypt.split(",");
		String macString = str[0];
		String[] allMACAddress = getMacAddress().split(",");
		for (int i = 0; i < allMACAddress.length; i++) {
			if (allMACAddress[i].equals(macString)) {
				return true;
			}
		}
		log.info("JecnKey isRegisterSuccess is error！");
		return false;
	}

	/**
	 * @author zhangchen May 17, 2012
	 * @description：注册的网卡是否匹配
	 * @return
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	public static boolean isRegisterSuccess(String[] key) throws IOException,
			NoSuchAlgorithmException {
		String macString = key[0];
		String[] allMACAddress = getMacAddress().split(",");
		for (int i = 0; i < allMACAddress.length; i++) {
			if (allMACAddress[i].equals(macString)) {
				return true;
			}
		}
		log.info("JecnKey isRegisterSuccess is error！");
		return false;
	}

	/**
	 * 获取秘密解密后数组，验证时间是否过期
	 * 
	 * @param str
	 *            秘密解密后数组
	 * @return true 正确，false 密钥到期或密钥错误
	 */
	public static boolean isDateCheck(String[] str) {
		if (str == null || str.length < 4) {
			return false;
		}
		String dateStr = str[2];
		String days = str[3];
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		// 验证days 是否为数字
		String reg = "^\\d{4}$";
		if (!days.matches(reg)) {
			return false;
		}
		try {
			// 时间验证 获取秘密开始时间
			Date date = sdf.parse(dateStr);
			if (new Date().before(date)) {
				return false;
			} else {
				Calendar c = Calendar.getInstance();
				c.setTime(date); // 设置当前日期
				c.add(Calendar.DATE, Integer.parseInt(days)); // 日期加1
				Date dateTotal = c.getTime();
				return new Date().before(dateTotal);
			}
		} catch (ParseException e) {
			log.error("", e);
		}
		return false;
	}

	/**
	 * @author yxw 2012-4-27
	 * @description:试用是不是到期
	 * @return
	 */
	public static boolean isDatePass() {
		// 新版注册器验证
		return isDatePass(getkeyEncrypt());
	}

	/**
	 * @author yxw 2012-4-27
	 * @description:试用是不是到期
	 * @return
	 */
	private static boolean isDatePass(String keyEncrypt) {
		if ("".equals(keyEncrypt)) {
			return false;
		}
		String keyDecrypt = getKeyDecrypt(keyEncrypt);
		String[] str = keyDecrypt.split(",");
		/** 正式密钥 */
		if (str.length == 2) {
			return true;
		} else if (str.length == 4) {
			return isDateCheck(str);
		} else {
			log.info("JecnKey isDatePass is error！");
			return false;
		}
	}

	/**
	 * 获得设计器的用户数
	 * 
	 * @return
	 */
	public static int getDesignerUserInt() {
		String keyDecrypt = getKeyDecrypt(getkeyEncrypt());
		String str = keyDecrypt.split(",")[1];
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			return Integer.parseInt(str);
		}
		return 0;
	}

	/**
	 * @author yxw 2012-8-3
	 * @description:判断密钥是否正确
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public static boolean checkKey() {
		try {
			// 判断秘钥是否存在
			if (!JecnKey.isKeyExist()) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("keyDoesNotExist"));
				return false;
			} else if (!JecnKey.isRegisterSuccess()) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("keyIncorrectly"));
				return false;
			} else if (!JecnKey.isDatePass()) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("ontrialExpire"));
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * @author yxw 2013-7-18
	 * @description:上传秘钥
	 * @return
	 */
	public static boolean uploadKey() {
		JecnFileChooser fileChoose = new JecnFileChooser();
		fileChoose.setEidtTextFiled(false);

		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("uploadKey"));
		fileChoose.setFileFilter(new FileFilter() {

			@Override
			public boolean accept(File f) {
				return f.isDirectory()
						|| (f.isFile() && (f.getName().endsWith(".properties")));
			}

			@Override
			public String getDescription() {
				return null;
			}

		});
		int i = fileChoose.showOpenDialog(null);
		if (i == JFileChooser.APPROVE_OPTION) {
			File file = fileChoose.getSelectedFile();
			FileInputStream in = null;
			FileOutputStream out = null;
			try {
				in = new FileInputStream(file);
				out = new FileOutputStream(JecnTrayMain.class.getResource("/")
						.toString()
						+ "server/jecn/EprosServer3.0/keylist.properties");
				byte[] buffer = new byte[512];
				int count = 0;
				do {
					count = in.read(buffer);
					if (count != -1)
						out.write(buffer);

				} while (count != -1);
				in.close();
				out.flush();
				out.close();
				return true;
			} catch (Exception e) {
				return false;
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {
					}
				}
				if (out != null) {
					try {
						out.close();
					} catch (IOException e) {

					}
				}
			}
		} else {
			return false;
		}
	}

	public static boolean loadKeyList(String strKeyArea) {
		Properties props = new Properties();
		// 文件输出流
		FileOutputStream fos = null;
		try {

			File f = new File(keyFilePath);
			if (!f.exists()) {
				f.createNewFile();
			}

			// 获取配置文件serverCfg.properties
			props.setProperty("key.license", strKeyArea.trim());
			// 文件输出流
			fos = new FileOutputStream(keyFilePath);// 文件输出后存放路径
			// 将Properties集合保存到流中
			props.store(fos, "keylist.properties");
			fos.close();// 关闭流
		} catch (FileNotFoundException ex) {
			return false;
		} catch (IOException ex) {
			return false;
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				log
						.error(
								"ServerPortBuss writedListInfo FileOutputStream is error",
								e);
				return false;
			}
		}

		return true;
	}

}
