package com.jecn.epros.server.dao.rule;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.rule.JecnFlowCommon;
import com.jecn.epros.server.bean.rule.JecnRiskCommon;
import com.jecn.epros.server.bean.rule.JecnStandarCommon;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleContent;
import com.jecn.epros.server.bean.rule.RuleContentT;
import com.jecn.epros.server.bean.rule.RuleFile;
import com.jecn.epros.server.bean.rule.RuleFileT;
import com.jecn.epros.server.bean.rule.RuleHistory;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.WebLoginBean;

public interface RuleHistoryDao extends IBaseDao<RuleHistory, Long> {
}