package com.jecn.webservice.cms.designerSSO;

import com.jecn.epros.server.action.web.login.ad.cms.JecnCMSAfterItem;

public enum CMSDesignerServiceClient {
	INSTANCE;

	public String RserValidate(String userAcccount, String PassWord) throws Exception {
		try {
			String iASName = JecnCMSAfterItem.getValue("D_IASName");
			String strPublicKeyExponent = JecnCMSAfterItem.getValue("Exponent");
			String strPublicKeyModulus = JecnCMSAfterItem.getValue("Modulus");
			// 密码加密
			PassWord = RSAUtils.encryptByPublicKey(PassWord, RSAUtils.getPublicKey(strPublicKeyModulus,
					strPublicKeyExponent));
			UUVServiceLocator locator = new UUVServiceLocator();
			return locator.getUUVServiceSoap().userValidate(iASName, userAcccount, PassWord);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
