package epros.designer.gui.process.flowmap;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.ProcessMapRelatedFileData;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.process.flow.file.FlowRelatedRulePanel;
import epros.designer.gui.process.flow.file.FlowRelatedStandardizedFilePanel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;

/***
 * 流程关联文件 2013-11-27
 * 
 */
public class FlowMapRuleDialog extends JecnDialog {
	private Logger log = Logger.getLogger(FlowMapRuleDialog.class);
	/** 相关标准化文件 */
	protected FlowRelatedStandardizedFilePanel relatedStandardizedFile = null;
	/** 相关制度 */
	protected FlowRelatedRulePanel relatedRule = null;
	/** 选中树节点 */
	private JecnTreeNode selectNode;

	private JecnPanel buttonPanel = null;
	/** 确认按钮 */
	private JButton okButton;
	/** 取消 */
	private JButton cancelButton;

	protected JScrollPane ruleScrollPanel;
	protected JScrollPane standardizedFileScrollPanel;
	List<JecnTreeBean> relatedStandardizeds = null;

	protected JTabbedPane tabPanel = null;

	// 相关标准
	List<JecnTreeBean> relatedRules = null;

	public FlowMapRuleDialog(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
		if (selectNode == null) {
			return;
		}
		initData();
		initCompotents();
		initLayout();
	}

	private void initData() {
		Long id = null;
		if (selectNode != null) {
			id = selectNode.getJecnTreeBean().getId();
		}
		try {
			relatedStandardizeds = ConnectionPool.getProcessAction().getStandardizedFiles(id);
			relatedRules = ConnectionPool.getRuleAction().getRuleTreeBeanByFlowId(id);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("accessServerError"));
			log.error("FlowMapRuleDialog initData is error！", e);
			return;
		}
	}

	/**
	 * 初始化
	 */
	private void initCompotents() {
		// 设置窗体大小
		this.setSize(630, 500);
		// 相关风险
		this.setTitle(JecnProperties.getValue("relateFile"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);
		this.setModal(true);
		tabPanel = new JTabbedPane();

		relatedStandardizedFile = new FlowRelatedStandardizedFilePanel(relatedStandardizeds);
		relatedRule = new FlowRelatedRulePanel(relatedRules);

		ruleScrollPanel = new JScrollPane();
		ruleScrollPanel.setViewportView(relatedRule);

		// 标准化文件
		standardizedFileScrollPanel = new JScrollPane();
		standardizedFileScrollPanel.setViewportView(relatedStandardizedFile);

		tabPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		ruleScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		standardizedFileScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		buttonPanel = new JecnPanel();
		okButton = new JButton(JecnProperties.getValue("okBtn"));
		cancelButton = new JButton(JecnProperties.getValue("cancelBtn"));

		relatedRule.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		relatedRule.setBorder(BorderFactory.createEtchedBorder());
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonListener();
			}
		});

		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	private void initLayout() {
		this.getContentPane().add(tabPanel, BorderLayout.CENTER);

		int count = 0;
		tabPanel.add(standardizedFileScrollPanel, count);
		tabPanel.setTitleAt(count, JecnProperties.getValue("relevantStandardFile"));
		count++;
		tabPanel.add(ruleScrollPanel, count);
		tabPanel.setTitleAt(count, JecnProperties.getValue("relatedRule"));
		count++;

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * 确定按钮
	 * 
	 */
	private void okButtonListener() {
		if (relatedRule.isUpdate() || relatedStandardizedFile.isUpdate()) {
			try {
				ProcessMapRelatedFileData relatedFileData = new ProcessMapRelatedFileData();
				relatedFileData.setFlowId(selectNode.getJecnTreeBean().getId());
				relatedFileData.setRelatedRules(relatedRules);
				relatedFileData.setRelatedStandardizeds(relatedStandardizeds);
				relatedFileData.setUpdatePeopleId(JecnConstants.getUserId());
				ConnectionPool.getProcessAction().updateProcessMapRelatedFiles(relatedFileData);
				JecnTreeCommon.flushCurrentTreeNodeToUpdate(this.selectNode);
			} catch (Exception e) {
				log.error("okButtonListener is error");
			}
		}
		this.setVisible(false);
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!relatedRule.isUpdate() && !relatedStandardizedFile.isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButtonListener();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

}
