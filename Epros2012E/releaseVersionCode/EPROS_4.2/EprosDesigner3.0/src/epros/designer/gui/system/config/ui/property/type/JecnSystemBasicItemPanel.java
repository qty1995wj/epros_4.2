package epros.designer.gui.system.config.ui.property.type;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextField;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 系统设置其他配置
 * 
 * @author zhr
 * @date： 日期：2018-9-18
 */
public class JecnSystemBasicItemPanel extends JecnAbstractPropertyBasePanel implements ActionListener {

	/**
	 * 是否启用 自动版本号
	 */
	private JecnConfigCheckBox isEnableAutoVersionNumber = new JecnConfigCheckBox(JecnProperties
			.getValue("whetherNumber"));

	/**
	 * 是否启用 小版本
	 */
	private JecnConfigCheckBox isenableSmallVersion = new JecnConfigCheckBox(JecnProperties.getValue("isEnableSmallVersions"));

	/** 自动编号前缀Lab */
	private JLabel versionNumberIdLab = new JLabel(JecnProperties.getValue("versionIdentificationC"));
	/** 自动编号前缀Field */
	private JecnConfigTextField versionNumberIdField = new JecnConfigTextField();

	/** 初始化版本号Lab */
	private JLabel initVersionNumberLab = new JLabel(JecnProperties.getValue("InitVersionNumberC"));
	/** 初始化版本号Field */
	private JecnConfigTextField initVersionNumberField = new JecnConfigTextField();

	/**
	 * 流程 编辑权限 发布 未发布设置
	 */
	private JecnConfigCheckBox processCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("release"));

	private JecnConfigCheckBox processNotCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("notRelease"));

	/**
	 * 流程架构 编辑权限 发布 未发布设置
	 */
	private JecnConfigCheckBox processMapCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("release"));

	private JecnConfigCheckBox processMapNotCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("notRelease"));

	/**
	 * 是否有流程图查看权限
	 */
	private JecnConfigCheckBox isShowProcessAccess = new JecnConfigCheckBox(JecnProperties.getValue("task_isAutoSave"));

	public JecnSystemBasicItemPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
		initLayout();
		initEvent();
	}

	private void initComponents() {
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setLayout(new GridBagLayout());
		versionNumberIdField.setColumns(10);
		initVersionNumberField.setColumns(10);
		initVersionNumberField.setToolTipText(JecnProperties.getValue("isNumberInSystem"));
		versionNumberIdField.setToolTipText(JecnProperties.getValue("isTextInSystem"));

	}

	private void initEvent() {
		// 是否启用自动版本号
		isEnableAutoVersionNumber.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (isEnableAutoVersionNumber.isSelected()) {
					isEnableAutoVersionNumber.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
				} else {
					isEnableAutoVersionNumber.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
				}
				versionNumberIdField.setEnabled(isEnableAutoVersionNumber.isSelected());
				initVersionNumberField.setEnabled(isEnableAutoVersionNumber.isSelected());
				isenableSmallVersion.setEnabled(isEnableAutoVersionNumber.isSelected());

			}
		});

		isenableSmallVersion.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (initVersionNumberField != null && StringUtils.isNotBlank(initVersionNumberField.getText())) {
					if (isenableSmallVersion.isSelected()) {
						isenableSmallVersion.getItemBean().setValue(JecnConfigContents.ITEM_IS_SHOW);
					} else {
						isenableSmallVersion.getItemBean().setValue(JecnConfigContents.ITEM_IS_NOT_SHOW);
					}
				}
			}
		});

		versionNumberIdField.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent e) {
				if (checkInfo()) {
					versionNumberIdField.getItemBean().setValue(versionNumberIdField.getText().trim().toUpperCase());
				}
			}
		});

		initVersionNumberField.addCaretListener(new CaretListener() {

			@Override
			public void caretUpdate(CaretEvent e) {
				if (checkInfo()) {
					initVersionNumberField.getItemBean().setValue(initVersionNumberField.getText());
				}
			}
		});

		versionNumberIdField.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				int keyChar = e.getKeyChar();
				if (keyChar > KeyEvent.VK_9 || keyChar < KeyEvent.VK_0) {

				} else {
					e.consume();
				}
			}
		});

		initVersionNumberField.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				int keyChar = e.getKeyChar();
				if ((keyChar >= KeyEvent.VK_0 && keyChar <= KeyEvent.VK_9) || keyChar == 46) {

				} else {
					e.consume();
				}
			}
		});

	}

	private boolean checkInfo() {
		return (checkText() && checkNumber());
	}

	private boolean checkText() {
		boolean isShowBut = true;
		if (JecnUserCheckUtil.checkTextFormat(versionNumberIdField.getText().trim())
				&& versionNumberIdField.getText().trim().length() < 9
				&& StringUtils.isNotBlank(versionNumberIdField.getText().trim())) {
			dialog.getOkCancelJButtonPanel().updateBtnAndInfoText("");
		} else {
			dialog.getOkCancelJButtonPanel().updateBtnAndInfoText(JecnProperties.getValue("isTextInSystem"));
			isShowBut = false;
		}
		dialog.getOkCancelJButtonPanel().setEnabled(isShowBut);
		return isShowBut;
	}

	private boolean checkNumber() {
		boolean isShowBut = true;
		if (JecnUserCheckUtil.checkNumberFormat(initVersionNumberField.getText().trim())
				&& initVersionNumberField.getText().trim().length() < 9
				&& StringUtils.isNotBlank(initVersionNumberField.getText().trim())) {
			dialog.getOkCancelJButtonPanel().updateBtnAndInfoText("");
		} else {
			dialog.getOkCancelJButtonPanel().updateBtnAndInfoText(JecnProperties.getValue("isNumberInSystem"));
			isShowBut = false;
		}
		dialog.getOkCancelJButtonPanel().setEnabled(isShowBut);
		return isShowBut;
	}

	private void initLayout() {
		JecnPanel isAutoVersionNumber = getIsAutoVersionNumber();
		JecnPanel isProcessChexkBoxPanel = getProcessChexkBox();
		JecnPanel isProcessMapChexkBoxPanel = getProcessMapChexkBox();
		JecnPanel isShowProcessAccess = getProcessAccess();
		Insets insets = new Insets(5, 0, 5, 0);
		/* JecnPanel isAutoVersionNumberFieId = getIsAutoVersionNumberField(); */
		int gridy = 0;

		GridBagConstraints c = new GridBagConstraints(0, ++gridy, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		this.add(isAutoVersionNumber, c);

		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(isProcessChexkBoxPanel, c);

		c = new GridBagConstraints(1, gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(isProcessMapChexkBoxPanel, c);

		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(isShowProcessAccess, c);
		// 空闲区域
		c = new GridBagConstraints(0, ++gridy, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(new JLabel(), c);
	}

	/**
	 * 自动版本号 配置
	 * 
	 * @return
	 */
	private JecnPanel getIsAutoVersionNumber() {

		JecnPanel activityPanel = new JecnPanel();
		activityPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		activityPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("versionConfig")));
		activityPanel.add(isEnableAutoVersionNumber);
		activityPanel.add(versionNumberIdLab);
		activityPanel.add(versionNumberIdField);
		activityPanel.add(initVersionNumberLab);
		activityPanel.add(initVersionNumberField);
		activityPanel.add(isenableSmallVersion);
		return activityPanel;
	}

	/**
	 * 流程编辑权限设置
	 * 
	 * @return
	 */
	private JecnPanel getProcessChexkBox() {
		processCheckBox.addActionListener(this);
		processNotCheckBox.addActionListener(this);
		JecnPanel processPanel = new JecnPanel();
		processPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		processPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("whetherProcessEdit")));
		processPanel.add(processCheckBox);
		processPanel.add(processNotCheckBox);
		return processPanel;
	}

	private JecnPanel getProcessAccess() {
		isShowProcessAccess.addActionListener(this);
		JecnPanel processAccessPanel = new JecnPanel();
		processAccessPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		processAccessPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("doYouHaveEermission")));
		processAccessPanel.add(isShowProcessAccess);
		return processAccessPanel;
	}

	/**
	 * 流程架构编辑权限设置
	 * 
	 * @return
	 */
	private JecnPanel getProcessMapChexkBox() {
		processMapCheckBox.addActionListener(this);
		processMapNotCheckBox.addActionListener(this);
		JecnPanel processMapPanel = new JecnPanel();
		processMapPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		// 是否显示活动类别
		processMapPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("whetherProcessMapEdit")));
		processMapPanel.add(processMapCheckBox);
		processMapPanel.add(processMapNotCheckBox);
		return processMapPanel;
	}

	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 唯一标识
		String mark = null;
		String value = null;
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getTableItemList()) {// 非表数据
			// 唯一标识
			mark = itemBean.getMark();
			// 值
			value = itemBean.getValue();
			if (ConfigItemPartMapMark.autoVersionNumber.toString().equals(mark)) {
				initIsAutoVersionNumber(itemBean);
			} else if (ConfigItemPartMapMark.enableSmallVersion.toString().equals(mark)) {
				initIsEnableSmallVersion(itemBean);
			} else if (ConfigItemPartMapMark.initVersionNumber.toString().equals(mark)) {
				initIsInitVersionNumberField(itemBean);
			} else if (ConfigItemPartMapMark.versionNumberId.toString().equals(mark)) {
				initIsVersionNumberId(itemBean);
			} else if (ConfigItemPartMapMark.editAuthProcessRelease.toString().equals(mark)) {
				initIsProcessCheckBox(itemBean);
			} else if (ConfigItemPartMapMark.editAuthProcessNotRelease.toString().equals(mark)) {
				initIsProcessNotCheckBox(itemBean);
			} else if (ConfigItemPartMapMark.editAuthProcessMapRelease.toString().equals(mark)) {
				initIsProcessMapCheckBox(itemBean);
			} else if (ConfigItemPartMapMark.editAuthProcessMapNotRelease.toString().equals(mark)) {
				initIsProcessMapNotCheckBox(itemBean);
			} else if (ConfigItemPartMapMark.isShowProcessAccess.toString().equals(mark)) {
				initIsProcessAccessCheckBox(itemBean);
			}
		}
		versionNumberIdField.setEnabled(isEnableAutoVersionNumber.isSelected());
		initVersionNumberField.setEnabled(isEnableAutoVersionNumber.isSelected());
		isenableSmallVersion.setEnabled(isEnableAutoVersionNumber.isSelected());

	}

	/**
	 * @param itemBean
	 */
	private void initIsAutoVersionNumber(JecnConfigItemBean itemBean) {
		isEnableAutoVersionNumber.setItemBean(itemBean);
		if ("0".equals(itemBean.getValue())) {
			isEnableAutoVersionNumber.setSelected(false);
		} else if ("1".equals(itemBean.getValue())) {
			isEnableAutoVersionNumber.setSelected(true);
		}
	}

	/**
	 * @param itemBean
	 */
	private void initIsProcessCheckBox(JecnConfigItemBean itemBean) {
		processCheckBox.setItemBean(itemBean);
		if ("0".equals(itemBean.getValue())) {
			processCheckBox.setSelected(false);
		} else if ("1".equals(itemBean.getValue())) {
			processCheckBox.setSelected(true);
		}
	}

	/**
	 * @param itemBean
	 */
	private void initIsProcessNotCheckBox(JecnConfigItemBean itemBean) {
		processNotCheckBox.setItemBean(itemBean);
		if ("0".equals(itemBean.getValue())) {
			processNotCheckBox.setSelected(false);
		} else if ("1".equals(itemBean.getValue())) {
			processNotCheckBox.setSelected(true);
		}
	}

	/**
	 * @param itemBean
	 */
	private void initIsProcessMapCheckBox(JecnConfigItemBean itemBean) {
		processMapCheckBox.setItemBean(itemBean);
		if ("0".equals(itemBean.getValue())) {
			processMapCheckBox.setSelected(false);
		} else if ("1".equals(itemBean.getValue())) {
			processMapCheckBox.setSelected(true);
		}
	}

	/**
	 * @param itemBean
	 */
	private void initIsProcessMapNotCheckBox(JecnConfigItemBean itemBean) {
		processMapNotCheckBox.setItemBean(itemBean);
		if ("0".equals(itemBean.getValue())) {
			processMapNotCheckBox.setSelected(false);
		} else if ("1".equals(itemBean.getValue())) {
			processMapNotCheckBox.setSelected(true);
		}
	}

	/**
	 * @param itemBean
	 */
	private void initIsProcessAccessCheckBox(JecnConfigItemBean itemBean) {
		isShowProcessAccess.setItemBean(itemBean);
		if ("0".equals(itemBean.getValue())) {
			isShowProcessAccess.setSelected(false);
		} else if ("1".equals(itemBean.getValue())) {
			isShowProcessAccess.setSelected(true);
		}
	}

	/**
	 * @param itemBean
	 */
	private void initIsEnableSmallVersion(JecnConfigItemBean itemBean) {
		isenableSmallVersion.setItemBean(itemBean);
		if ("0".equals(itemBean.getValue())) {
			isenableSmallVersion.setSelected(false);
		} else if ("1".equals(itemBean.getValue())) {
			isenableSmallVersion.setSelected(true);
		}
	}

	/**
	 * 
	 * @param itemBean
	 */
	private void initIsVersionNumberId(JecnConfigItemBean itemBean) {
		versionNumberIdField.setItemBean(itemBean);
		versionNumberIdField.setText(itemBean.getValue());
	}

	/**
	 * 
	 * @param itemBean
	 */
	private void initIsInitVersionNumberField(JecnConfigItemBean itemBean) {
		initVersionNumberField.setItemBean(itemBean);
		initVersionNumberField.setText(itemBean.getValue());
	}

	@Override
	public boolean check() {
		return false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == processCheckBox) {
			processCheckBox.getItemBean().setValue(
					processCheckBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);
		}
		if (source == processNotCheckBox) {
			processNotCheckBox.getItemBean().setValue(
					processNotCheckBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);
		}
		if (source == processMapCheckBox) {
			processMapCheckBox.getItemBean().setValue(
					processMapCheckBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);

		}
		if (source == processMapNotCheckBox) {
			processMapNotCheckBox.getItemBean().setValue(
					processMapNotCheckBox.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);

		}
		if (source == isShowProcessAccess) {
			isShowProcessAccess.getItemBean().setValue(
					isShowProcessAccess.isSelected() ? JecnConfigContents.ITEM_IS_SHOW
							: JecnConfigContents.ITEM_IS_NOT_SHOW);

		}

	}
}
