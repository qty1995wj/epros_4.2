package com.jecn.webservice.xinghan.bean;

import java.util.List;

import org.apache.commons.lang.StringUtils;


/***** 星汉同步的流程属性 ******/
public class FlowInfo {
	/*** 流程类别ID ******/
	private long flowCatId;
	/***
	 * 流程类别序号 
	 * 默认值为了解决当流程类别编号为空，同步失败的情况 *
	 *****/
	private String flowCatNo = "-1";
	/**** 流程类别名称 *******/
	private String flowCatName;
	/*** 流程ID ***/
	private long flowId;
	/**
	 * 流程序号
	 * 默认值为了解决当流程编号为空，同步失败的情况
	 ***/
	private String flowNo = "-1";
	/**** 流程名称 *****/
	private String flowName;
	/***** 流程节点集合 *********/
	private List<FlowNode> flowNode;
	/***** 流程线条集合 *********/
	private List<FlowLink> flowLink;

	public String getFlowCatId() {
		if (flowCatId == 0) {
			return "";
		}
		return String.valueOf(flowCatId);
	}

	public void setFlowCatId(long flowCatId) {
		this.flowCatId = flowCatId;
	}

	public String getFlowCatNo() {
		
		return flowCatNo;
	}

	public void setFlowCatNo(String flowCatNo) {
		if(StringUtils.isBlank(flowCatNo)){
			return;
		}
		this.flowCatNo = flowCatNo;
	}

	public String getFlowCatName() {
		if (flowCatName == null) {
			return "";
		}
		return flowCatName;
	}

	public void setFlowCatName(String flowCatName) {
		this.flowCatName = flowCatName;
	}

	public String getFlowId() {
		if (flowId == 0) {
			return "";
		}
		return String.valueOf(flowId);
	}

	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}

	public String getFlowNo() {
		return flowNo;
	}

	public void setFlowNo(String flowNo) {
		if(StringUtils.isBlank(flowNo)){
			return;
		}
		this.flowNo = flowNo;
	}

	public String getFlowName() {
		if (flowName == null) {
			return "";
		}
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public List<FlowNode> getFlowNode() {
		return flowNode;
	}

	public void setFlowNode(List<FlowNode> flowNode) {
		this.flowNode = flowNode;
	}

	public List<FlowLink> getFlowLink() {
		return flowLink;
	}

	public void setFlowLink(List<FlowLink> flowLink) {
		this.flowLink = flowLink;
	}
}
