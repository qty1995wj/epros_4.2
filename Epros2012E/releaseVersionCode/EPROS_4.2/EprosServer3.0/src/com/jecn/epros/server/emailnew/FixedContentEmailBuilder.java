package com.jecn.epros.server.emailnew;

import java.util.ArrayList;
import java.util.List;

public abstract class FixedContentEmailBuilder extends BaseEmailBuilder {

	@Override
	public List<EmailBasicInfo> buildEmail() {
		List<EmailBasicInfo> emails = new ArrayList<EmailBasicInfo>();
		EmailBasicInfo info = getEmailBasicInfo();
		info.addRecipients(getUsers());
		emails.add(info);
		return emails;
	}

	protected abstract EmailBasicInfo getEmailBasicInfo();

}
