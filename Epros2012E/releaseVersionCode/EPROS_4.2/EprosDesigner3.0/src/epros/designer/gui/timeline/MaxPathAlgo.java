package epros.designer.gui.timeline;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

public class MaxPathAlgo {

	private List<JecnFigureData> panelList;
	private JecnActiveData startActive;
	private Map<String, JecnActiveData> activeMap = new HashMap<String, JecnActiveData>();
	private Map<String, JecnFigureData> dottedRectMap = new HashMap<String, JecnFigureData>();
	private Map<String, JecnActiveData> maxTimeCostActiveInDottedRect = new HashMap<String, JecnActiveData>();
	private List<JecnManhattanLineData> manhanttanLineList = new ArrayList<JecnManhattanLineData>();

	private List<List<String>> pathList = new ArrayList<List<String>>();
	private List<List<String>> removeList = new ArrayList<List<String>>();
	private List<String> maxPathList = null;
	private List<JecnActiveData> activeDataListOfMaxTimeCost = new ArrayList<JecnActiveData>();
	private BigDecimal maxTimeCost = new BigDecimal("0");

	public MaxPathAlgo() {
		initData();
		initPathList();
		initMaxCostPathList();
		initMaxCostActiveDataList();
	}

	/**
	 * 初始化最大耗时活动集合
	 */
	private void initMaxCostActiveDataList() {
		if (maxPathList == null) {
			return;
		}
		// 初始化最大路径活动集合
		for (String activeUUID : maxPathList) {
			if (activeMap.get(activeUUID) != null) {
				activeDataListOfMaxTimeCost.add(activeMap.get(activeUUID));
			}
		}
	}

	/**
	 * 初始化最大耗时活动数据集合
	 */
	private void initMaxCostPathList() {
		if (pathList.size() == 0) {
			return;
		}

		// 获取最大消耗时间，和对应的路径集合
		BigDecimal zero = new BigDecimal("0");

		for (List<String> path : pathList) {
			BigDecimal pathCost = new BigDecimal("0");
			int sort = path.size();
			for (int i = path.size() - 1; i >= 0; i--) {
				String figureUUID = path.get(i);

				if (isActive(figureUUID)) {
					JecnActiveData activeData = activeMap.get(figureUUID);
					sort--;
					if (activeData != null && activeData.getStatusValue() != null
							&& activeData.getStatusValue().compareTo(zero) != 0) {
						pathCost = pathCost.add(activeData.getStatusValue());
						activeData.setTimeLineSort(sort);
					} else {
						// 删除不存在时间轴的path
						path.remove(i);
					}
				} else if (isDottedRect(figureUUID)) {
					JecnActiveData activeData = maxTimeCostActiveInDottedRect.get(figureUUID);
					sort--;
					if (activeData != null && activeData.getStatusValue() != null
							&& activeData.getStatusValue().compareTo(zero) != 0) {
						pathCost = pathCost.add(activeData.getStatusValue());
						path.set(i, activeData.getFlowElementUUID());
						activeData.setTimeLineSort(sort);
					} else {
						path.remove(i);
					}
				}
			}
			if (maxTimeCost.compareTo(pathCost) == -1) {
				maxTimeCost = pathCost;
				maxPathList = path;
			}
		}
	}

	private boolean isDottedRect(String figureUUID) {
		return dottedRectMap.get(figureUUID) != null;
	}

	private boolean isActive(String figureUUID) {
		return activeMap.get(figureUUID) != null;
	}

	public List<JecnActiveData> getActiveDataListOfMaxTimeCost() {
		return activeDataListOfMaxTimeCost;
	}

	public BigDecimal getMaxTimeCostOfActive() {
		return maxTimeCost;
	}

	/**
	 * 获取当前画图面板中的活动及曼哈顿线数据
	 */
	private void initData() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		// 处理并行元素，保留办理时限最大值，并获取左右元素（不包含删除的并行元素和线段）

		// 获取起始元素以及保存所有活动的HashMap
		panelList = copyPanelList();
		// 初始化 记录活动元素是否有连接线
		initEndFigureList();
		List<JecnActiveData> deleteActiveList = new ArrayList<JecnActiveData>();
		for (JecnFigureData figureData : panelList) {
			JecnActiveData g020ActiveData = null;
			if (DrawCommon.isActive((figureData.getMapElemType()))) {
				g020ActiveData = (JecnActiveData) figureData;
			} else if (figureData.getMapElemType() == MapElemType.DottedRect) {
				g020ActiveData = getMaxCostActiveDataInDottedRect(figureData, deleteActiveList);
				dottedRectMap.put(figureData.getFlowElementUUID(), figureData);
				// 协作框对应最大办理时限的活动
				maxTimeCostActiveInDottedRect.put(figureData.getFlowElementUUID(), g020ActiveData);
			}
			if (g020ActiveData == null) {
				continue;
			}

			setStartActiveData(g020ActiveData);
			activeMap.put(g020ActiveData.getFlowElementUUID(), g020ActiveData);
		}

		// 复制 连接线
		copyManhantLineList();
		// 根据 删除活动集合 ，判断连接线是否删除
		deleteManLinesByDeleteActive(deleteActiveList);
	}

	private void deleteManLinesByDeleteActive(List<JecnActiveData> deleteActiveList) {
		if (endFigureList.size() == 0) {
			return;
		}
		for (JecnActiveData activeData : deleteActiveList) {
			List<JecnManhattanLineData> deleteManLines = new ArrayList<JecnManhattanLineData>();
			for (JecnManhattanLineData lineData : manhanttanLineList) {
				if (lineData.getStartFigureUUID().equals(String.valueOf(activeData.getFlowElementId()))
						|| lineData.getEndFigureUUID().equals(String.valueOf(activeData.getFlowElementId()))) {
					deleteManLines.add(lineData);
				}
			}
			manhanttanLineList.removeAll(deleteManLines);
		}
	}

	private List<String> endFigureList = new ArrayList<String>();

	private void initEndFigureList() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		List<JecnBaseLineData> lineList = desktopPane.getFlowMapData().getLineList();
		for (JecnBaseLineData jecnBaseLineData : lineList) {
			if (MapElemType.ManhattanLine == jecnBaseLineData.getMapElemType()) {
				JecnManhattanLineData lineData = (JecnManhattanLineData) jecnBaseLineData;
				endFigureList.add(lineData.getEndFigureUUID());
			}
		}
	}

	// parallel

	private void copyManhantLineList() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		List<JecnBaseLineData> panelList = desktopPane.getFlowMapData().getLineList();
		for (JecnBaseLineData baseLineData : panelList) {
			switch (baseLineData.getMapElemType()) {
			case ManhattanLine:
				manhanttanLineList.add((JecnManhattanLineData) baseLineData);
			}
		}
	}

	private List<JecnFigureData> copyPanelList() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		List<JecnFigureData> panelList = desktopPane.getFlowMapData().getFigureList();
		// 复制 集合
		List<JecnFigureData> copyPanelList = new ArrayList<JecnFigureData>();
		for (JecnFigureData figureData : panelList) {
			if (DrawCommon.isActive((figureData.getMapElemType()))) {
				copyPanelList.add(figureData);
			} else if (figureData.getMapElemType() == MapElemType.DottedRect) {
				copyPanelList.add(figureData);
			}
		}
		return copyPanelList;
	}

	/**
	 * 获取协作框里办理时限现状值最大的活动
	 * 
	 * @param figureData
	 * @return
	 */
	private JecnActiveData getMaxCostActiveDataInDottedRect(JecnFigureData figureData,
			List<JecnActiveData> deleteActiveList) {
		List<JecnActiveData> activesOfDottedRect = getTimeCostNotNullActivesInDottedRectRange(figureData);
		if (activesOfDottedRect.size() == 0) {
			return null;
		}
		JecnActiveData maxTimeCostActive = activesOfDottedRect.get(0);

		for (int i = 1; i <= activesOfDottedRect.size() - 1; i++) {
			if ((maxTimeCostActive == null || maxTimeCostActive.getStatusValue() == null)
					&& activesOfDottedRect.get(i).getStatusValue() != null) {
				maxTimeCostActive = activesOfDottedRect.get(i);
				continue;
			}
			if (activesOfDottedRect.get(i).getStatusValue() == null) {
				continue;
			}
			if (maxTimeCostActive.getStatusValue().compareTo(activesOfDottedRect.get(i).getStatusValue()) == -1) {
				maxTimeCostActive = activesOfDottedRect.get(i);
			}
		}

		// 删除泳池中相关活动
		deleteCostActive(activesOfDottedRect, deleteActiveList);
		return maxTimeCostActive;
	}

	private void deleteCostActive(List<JecnActiveData> activesOfDottedRect, List<JecnActiveData> deleteActiveList) {
		for (JecnActiveData jecnFigureData : activesOfDottedRect) {
			if (endFigureList.contains(String.valueOf(jecnFigureData.getFlowElementId()))) {
				activesOfDottedRect.remove(jecnFigureData);
				break;
			}
		}
		deleteActiveList.addAll(activesOfDottedRect);
	}

	/**
	 * 获取协作框中办理时限目标值不为空的活动集合
	 * 
	 * @param figureData
	 * @return
	 */
	private List<JecnActiveData> getTimeCostNotNullActivesInDottedRectRange(JecnFigureData figureData) {
		List<JecnActiveData> activesOfDottedRect = new ArrayList<JecnActiveData>();
		for (JecnFigureData data : panelList) {
			if (DrawCommon.isActive((data.getMapElemType()))) {
				if (DrawCommon.isActiveInDottedRectRange(data, figureData)) {
					activesOfDottedRect.add((JecnActiveData) data);
				}
			}
		}
		return activesOfDottedRect;
	}

	/**
	 * 设置开始活动
	 * 
	 * @param activeData
	 */
	private void setStartActiveData(JecnActiveData activeData) {
		if (activeData.getStatusValue() == null || activeData.getTargetValue() == null) {
			return;
		}
		if (startActive == null) {
			startActive = activeData;
		}
		if (startActive.getFigureDataCloneable().getDoubleX() > activeData.getFigureDataCloneable().getDoubleX()
				|| (startActive.getFigureDataCloneable().getDoubleX() == activeData.getFigureDataCloneable()
						.getDoubleX() && startActive.getFigureDataCloneable().getDoubleY() > activeData
						.getFigureDataCloneable().getDoubleY())) {
			startActive = activeData;
		}
	}

	public JecnActiveData getStartActive() {
		return startActive;
	}

	/**
	 * 获取不同路径下的最大耗时
	 * 
	 * @return
	 */
	private void initPathList() {
		if (check()) {
			return;
		}
		if (pathList.size() > 1000) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("inParallelCllaborationBox"));
			return;
		}
		for (JecnManhattanLineData mLine : manhanttanLineList) {
			String activeId = startActive.getFlowElementUUID();
			if (!endFigureList.contains(activeId)) {
				activeId = getDottedRectIdOrFigureId(startActive.getFlowElementUUID());
			}
			// 获取起点线（可能有多条），
			if (mLine.getStartFigureUUID().equals(activeId)) {
				// 起点线数据
				List<String> path = new ArrayList<String>();
				path.add(mLine.getStartFigureUUID());
				path.add(mLine.getEndFigureUUID());
				pathList.add(path);
				findSonPath(path, mLine.getEndFigureUUID());
			}
		}
		pathList.removeAll(removeList);

		// 重置路径，如果活动被连线，但是活动处于协作框内，则将连在活动上的线改为连在协作框上
		for (List<String> paths : pathList) {
			for (int i = 0; i <= paths.size() - 1; i++) {
				paths.set(i, getDottedRectIdOrFigureId(paths.get(i)));
			}
		}
	}

	/**
	 * 如果图形为活动并且在协作框内，则返回协作框的UUID
	 * 
	 * @param uuid
	 * @return
	 */
	private String getDottedRectIdOrFigureId(String uuid) {
		if (activeMap.get(uuid) != null) {
			JecnActiveData jecnActiveData = activeMap.get(uuid);
			for (JecnFigureData figureData : dottedRectMap.values()) {
				if (DrawCommon.isActiveInDottedRectRange(jecnActiveData, figureData)) {
					return figureData.getFlowElementUUID();
				}
			}
		}
		return uuid;
	}

	/**
	 * 递归获取所有路径
	 * 
	 * @param path
	 * @param startFigureUUID
	 */
	private void findSonPath(List<String> path, String startFigureUUID) {
		for (JecnManhattanLineData mLine : manhanttanLineList) {
			if (mLine.getStartFigureUUID().equals(startFigureUUID)) {
				if (path.contains(mLine.getStartFigureUUID().toString())
						&& path.contains(mLine.getEndFigureUUID().toString())) {
					continue;
				}
				List<String> newPath = new ArrayList<String>();
				for (String str : path) {
					newPath.add(str);
				}
				// 记录要回收的数据（例如：最终路径为1,2,3,4 ，则此处记录的数据可能为：1,2,3或1,2)
				removeList.add(path);
				// 起点线数据
				newPath.add(mLine.getEndFigureUUID());
				pathList.add(newPath);
				findSonPath(newPath, mLine.getEndFigureUUID());
			}
		}
	}

	/**
	 * 检查需要的数据是否完成
	 * 
	 * @return
	 */
	private boolean check() {
		if (startActive == null) {
			return true;
		}
		if (activeMap == null) {
			return true;
		}
		if (manhanttanLineList == null) {
			return true;
		}
		return false;
	}
}
