package epros.designer.gui.popedom.role.choose;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.file.FileManageDialog;
import epros.designer.gui.file.JecnFileCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

/**
 * 
 * 角色管理角色树
 * 
 * @author Administrator
 * 
 */
public class RoleManageHighEfficiencyFileTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(RoleManageHighEfficiencyFileTree.class);
	/** 所有文件 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	/** 1不支持双击打开文件 */
	private int flag = 0;

	/** 判断是0:文件管理框操作，还是1:文件选择框操作 */
	private int isFileMange = 0;
	/** 0:普通选择 1直选图片：过滤不是图片的文件不显示 */
	private int chooseType = 0;

	public int getChooseType() {
		return chooseType;
	}

	public void setChooseType(int chooseType) {
		this.chooseType = chooseType;
	}

	private FileManageDialog jecnManageDialog;
	private RoleManageFileChooseDialog fileChooseDialog;

	public RoleManageHighEfficiencyFileTree(int flag, RoleManageFileChooseDialog fileChooseDialog) {
		this.flag = flag;
		this.fileChooseDialog = fileChooseDialog;
		this.isFileMange = 1;
	}

	public RoleManageFileChooseDialog getFileChooseDialog() {
		return fileChooseDialog;
	}

	public void setFileChooseDialog(RoleManageFileChooseDialog fileChooseDialog) {
		this.fileChooseDialog = fileChooseDialog;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public int getIsFileMange() {
		return isFileMange;
	}

	public void setIsFileMange(int isFileMange) {
		this.isFileMange = isFileMange;
	}

	public RoleManageHighEfficiencyFileTree(FileManageDialog fileManageDialog) {
		this.jecnManageDialog = fileManageDialog;
		this.isFileMange = 0;
	}

	public RoleManageHighEfficiencyFileTree(int flag) {
		this.flag = flag;

	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RoleManageFileTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getFileAction().getRoleAuthChildFiles(0L, JecnConstants.projectId,
					JecnConstants.getUserId());
		} catch (Exception e) {
			log.error("RoleManageHighEfficiencyFileTree getTreeModel is error", e);
		}
		return JecnFileCommon.getFileTreeModel(list);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		// JecnFileCommon.treeMousePressed(evt, this, jecnManageDialog);
	}

}
