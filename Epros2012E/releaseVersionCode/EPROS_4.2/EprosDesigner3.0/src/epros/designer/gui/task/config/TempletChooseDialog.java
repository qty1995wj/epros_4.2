package epros.designer.gui.task.config;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/***
 * 模板选择框
 * 
 * 2012-10-25
 */
public class TempletChooseDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(TempletChooseDialog.class);
	private Long id;
	/** 0流程 1文件 2制度 **/
	private Integer type;
	private TempletPanel approvePanel;
	private TempletPanel borrowPanel;
	private TempletPanel desuetudePanel;
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(510, 360);// 510, 660
	/** center面板 */
	private JecnPanel centerPanel = new JecnPanel(510, 400);// /510, 505
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel(500, 30);// 500, 30

	/** 验证提示 */
	private JLabel verfyLab = new JLabel();
	/** 确定 */
	protected JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 面板宽 */
	private int dialogWidth = 560;
	/** 面板高 */
	private int dialogHeigh = 650;

	public TempletChooseDialog(Long id, Integer type) {
		this.setTitle(JecnProperties.getValue("taskModelSelect"));
		this.setSize(dialogWidth, dialogHeigh);
		this.setResizable(true);
		this.setModal(true);
		this.id = id;
		this.type = type;
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);
		initCompnonet();
		initAction();
		initLayout();

	}

	private void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(3, 3, 3, 3);
		JScrollPane scrollPanel = new JScrollPane();
		scrollPanel.setViewportView(centerPanel);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(3, 3, 0, 3), 0, 0);
		mainPanel.add(scrollPanel, c);

		centerPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(3, 3, 0, 3), 0, 0);
		centerPanel.add(approvePanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(3, 3, 0, 3), 0, 0);
		centerPanel.add(borrowPanel, c);
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(3, 3, 0, 3), 0, 0);
		centerPanel.add(desuetudePanel, c);

		c = new GridBagConstraints(0, 2, 3, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		this.setResizable(false);
		// 模态
		this.setModal(true);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setLocationRelativeTo(null);
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);

		borrowPanel.setVisible(false);
		this.setSize(dialogWidth, dialogHeigh - 100);
	}

	private void initCompnonet() {
		this.approvePanel = new TempletPanel(this.id, this.type, 0);
		this.borrowPanel = new TempletPanel(this.id, this.type, 1);
		this.desuetudePanel = new TempletPanel(this.id, this.type, 2);

		approvePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		borrowPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		desuetudePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	private void initAction() {
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveAction();
			}

		});

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

	}

	private void saveAction() {
		String approveId = approvePanel.getResult();
		String borrowId = borrowPanel.getResult();
		String desuetudeId = desuetudePanel.getResult();
		try {
			ConnectionPool.getTaskRecordAction().saveOrUpdatePrfRelated(id, type, approveId, borrowId, desuetudeId,
					JecnConstants.getUserId());
			if (approveId != null && approveId.length() != 0) {
				if (type == 0) {
					JecnConstants.processRelatedTaskTemplet.put(id, approveId);
				} else if (type == 1) {
					JecnConstants.fileRelatedTaskTemplet.put(id, approveId);
				} else if (type == 2 || type == 3) {
					JecnConstants.ruleRelatedTaskTemplet.put(id, approveId);
				} else if (type == 4) {
					JecnConstants.processMapRelatedTaskTemplet.put(id, approveId);
				}
			}
			if (desuetudeId != null && desuetudeId.length() != 0) {
				if (type == 0) {
					JecnConstants.processRelatedAbolishTaskTemplet.put(id, desuetudeId);
				} else if (type == 1) {
					JecnConstants.fileRelatedAbolishTaskTemplet.put(id, desuetudeId);
				} else if (type == 2 || type == 3) {
					JecnConstants.ruleRelatedAbolishTaskTemplet.put(id, desuetudeId);
				} else if (type == 4) {
					JecnConstants.processMapRelatedAbolishTaskTemplet.put(id, desuetudeId);
				}
			}
			// 关闭窗体
			this.dispose();
		} catch (Exception e) {
			log.error(e);
		}
	}

	private void cancelButPerformed() {
		// 关闭窗体
		this.dispose();
	}

}
