<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>

<script type="text/javascript">
</script>
</head>
<body>
	<div id="reports_id" style="width: 100%;height:100%">
		<div id='reportsLeft_id' style="width:175px;height:100%;margin-left:2px;padding:0px;background:#F2F2F2;">
				<ul id=reportsUl_id>
					<li id="${basePath}jsp/reports/processDetailListReport.jsp" style="padding-left:30px; text-align: left;"><s:text name="processDetailListReport"/></li><!-- 流程清单报表 -->					
					<li id="${basePath}jsp/reports/processScanOptimizeReport.jsp" style="padding-left:30px; text-align: left;"><s:text name="processScanOptimizeReport"/></li><!-- 流程审视优化统计表 -->					
					<li id="${basePath}jsp/reports/processKPIFollowReport.jsp" style="padding-left:30px; text-align: left;"><s:text name="processKPIFollowReport"/></li><!-- 流程审视优化统计表 -->					
					<li id="${basePath}jsp/reports/processRole.jsp;processRolePieCharts" style="padding-left:30px; text-align: left;"><s:text name="processRoleNumberStatistics"/></li><!-- 流程角色数统计 -->
					<li id="${basePath}jsp/reports/processActivities.jsp;processActivitiesPieCharts" style="padding-left:30px; text-align: left;"><s:text name="processActivitiesNumberStatistics"/></li><!-- 流程活动数统计 -->
					<!--<li id="${basePath}jsp/reports/processApplication.jsp;processApplicationGrid" style="padding-left:30px; text-align: left;"><s:text name="processApplicationNumberStatistics"/></li> 流程应用人数统计 -->			
					<li id="${basePath}jsp/reports/processInputOutput.jsp;processInputOutputGrid" style="padding-left:30px; text-align: left;"><s:text name="processInputOutputStatistics"/></li><!-- 流程输入输出统计 -->
					<li id="getAllReportsProcessType.action;processAnalysisPieCharts" style="padding-left:30px; text-align: left;" ><s:text name="processAnalysisStatistics"/></li><!-- 流程分析统计 -->
					<li id="${basePath}jsp/reports/processAccess.jsp;processAccessGrid" style="padding-left:30px; text-align: left;"><s:text name="processAccessNumberStatistics"/></li><!-- 流程访问数统计 -->
					<li id="${basePath}jsp/reports/processInterviewer.jsp;processInterviewerGrid" style="padding-left:30px; text-align: left;"><s:text name="processInterviewerStatistics"/></li><!-- 流程访问人统计 -->
					<li id="${basePath}jsp/reports/processDocumentNumber.jsp;processDocumentNumberGrid" style="padding-left:30px; text-align: left;"><s:text name="processDocumentDownloadNumberStatistics"/></li><!-- 流程文档下载数统计 -->
					<li id="${basePath}jsp/reports/processDocumentPeople.jsp;processDocumentPeopleGrid" style="padding-left:30px; text-align: left;"><s:text name="processDocumentDownloadPeopleStatistics"/></li><!-- 流程文档下载人统计 -->
					<!--<li id="${basePath}jsp/reports/taskApproveTime.jsp;taskRepository" style="padding-left:30px; text-align: left;"><s:text name="taskApproveTimeStatistics"/></li> 任务审批时间统计 -->		
				</ul>
		</div>
	</div>
</body>
</html>