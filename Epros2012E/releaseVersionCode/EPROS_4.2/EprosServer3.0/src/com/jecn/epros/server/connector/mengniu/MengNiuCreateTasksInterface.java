package com.jecn.epros.server.connector.mengniu;

import java.util.List;

/**
 * 创建任务
 * 
 * @author xiaohu
 * 
 */
public class MengNiuCreateTasksInterface {
	/** 1参数A模式 2参数B模式 */
	private int ParamMode = 2;

	/** 第三方流程实例编号 */
	private String EngineInstanceID;

	private String ProcessCode;

	private List<MengNiuTasksBean> Tasks;

	public int getParamMode() {
		return ParamMode;
	}

	public void setParamMode(int paramMode) {
		ParamMode = paramMode;
	}

	public List<MengNiuTasksBean> getTasks() {
		return Tasks;
	}

	public void setTasks(List<MengNiuTasksBean> tasks) {
		Tasks = tasks;
	}

	public String getEngineInstanceID() {
		return EngineInstanceID;
	}

	public void setEngineInstanceID(String engineInstanceID) {
		EngineInstanceID = engineInstanceID;
	}

	public String getProcessCode() {
		return ProcessCode;
	}

	public void setProcessCode(String processCode) {
		ProcessCode = processCode;
	}

}
