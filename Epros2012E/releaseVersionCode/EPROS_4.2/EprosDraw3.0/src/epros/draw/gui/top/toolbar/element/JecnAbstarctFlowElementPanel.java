package epros.draw.gui.top.toolbar.element;

import java.awt.event.ItemEvent;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnPanel;

/**
 * 
 * 流程元素的局部属性所在面板基类
 * 
 * 即填充、阴影、大小、字体样式、常用色、渐变色等许多模块面板的基类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnAbstarctFlowElementPanel extends JecnPanel {

	/** 流程元素面板 */
	protected JecnFlowElementPanel flowElementPanel = null;

	public JecnAbstarctFlowElementPanel(JecnFlowElementPanel flowElementPanel) {
		if (flowElementPanel == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_PANEL
					.error("JecnAbstarctFlowElementPanel类构造函数：参数为null。flowElementPanel="
							+ flowElementPanel);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.flowElementPanel = flowElementPanel;

	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	protected void init() {
		initComponents();
		initLayout();
	}

	/**
	 * 
	 * 选项事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
	}

	/** 创建组件以及属性 */
	protected abstract void initComponents();

	/** 布局 */
	protected abstract void initLayout();

	/** 初始化数据 */
	public abstract void initData(JecnAbstractFlowElementData flowElementData);
}
