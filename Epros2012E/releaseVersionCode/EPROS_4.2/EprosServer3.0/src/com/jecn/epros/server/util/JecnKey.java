package com.jecn.epros.server.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import jecntool.cmd.JecnCmd;
import jecntool.cmd.ResultBean;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import sun.misc.BASE64Decoder;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class JecnKey {
	/** 密钥 文件路径 */
	private static String keyFilePath = JecnPath.APP_PATH + "keylist.properties";
	/** 密钥 关键字 */
	private static String keyName = "key.license";

	private static String ALGORITHM = "AES";
	/** 获取解密后密钥文件 */
	public static String value = null;
	/** 密钥版本 P1，或P2 */
	private static final String KEY_TYPE = "P2";
	private static final Logger log = Logger.getLogger(JecnKey.class);

	/** 旧版本秘钥，不带二级管理员 */
	private static KeyEnmu keyEnmu = null;

	private static boolean test;

	/**
	 * @author yxw 2012-8-3
	 * @description:判断密钥是否正确
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public static boolean checkKey() throws Exception {
		if (!isKeyExist()) {// 密钥文件是否存在 tree:存在
			return false;
		}
		// 加密数据
		String str = getkeyEncrypt();
		// 获取AES key
		String key = getKey();
		// 解密 length = 53，测试版二级管理员
		value = decrypt(str, getSecretKey(key));
		if (value == null) {
			return false;
		}
		// 是否旧版秘钥判断
		keyEnmu = keyValidate();
		// 密钥版本验证
		if (!keyTypeCheck()) {
			return false;
		}
		test = isTestValidate();
		if (test) {// 试用
			if (isDatePass()) {// True：验证通过
				return true;
			}
		} else {// 正式
			if (isMacPass()) {// True：验证通过
				return true;
			}
		}
		return false;
	}

	/**
	 * 秘钥版本
	 * 
	 * 1、旧版、2：二极管理员、3：系统管理员
	 * 
	 * @return
	 */
	private static KeyEnmu keyValidate() {
		// 测试：旧版长度49，新版53;正式：新版70，旧版 66
		// 含系统管理员秘钥：测试：62，正式 78
		int strLen = value.length();
		if (strLen == 49 || strLen == 66) {
			return KeyEnmu.OLD_KEY;
		} else if (strLen == 53 || strLen == 70) {
			return KeyEnmu.PROCESS_ADMIN_KEY;
		}
		JecnContants.isAdminKey = true;
		return KeyEnmu.ADMIN_KEY;
	}

	enum KeyEnmu {
		OLD_KEY // 旧版秘钥 （设计用户）
		, PROCESS_ADMIN_KEY // 流程管理员秘钥(设计用户-含管理员，流程管理员)
		, ADMIN_KEY
		// 系统管理员秘钥（系统管理员、流程管理员、设计者）
	}

	/**
	 * 密钥版本验证
	 * 
	 * @return
	 */
	private static boolean keyTypeCheck() {
		// 密钥版本 P1，或P2 大版本为P2
		String keyType = null;
		if (keyEnmu == KeyEnmu.ADMIN_KEY) {
			keyType = value.substring(value.lastIndexOf(KEY_TYPE), value.length() - 18);
		} else {
			keyType = value.substring(value.lastIndexOf(KEY_TYPE), value.length() - 13);
		}
		if (keyType.equals(KEY_TYPE)) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为数字
	 * 
	 * @return
	 */
	public static boolean isTestValidate() {
		if (value == null) {
			return false;
		}

		if (KeyEnmu.ADMIN_KEY == keyEnmu) {
			return checkIsNumber(value.substring(0, 26));
		} else if (KeyEnmu.PROCESS_ADMIN_KEY == keyEnmu) {
			return checkIsNumber(value.substring(0, 22));
		} else if (KeyEnmu.OLD_KEY == keyEnmu) {// 旧版本秘钥
			return checkIsNumber(value.substring(0, 18));
		}
		return false;
	}

	/**
	 * 获得设计器的用户数
	 * 
	 * @return
	 */
	public static int getDesignerUserInt() {
		String keyDecrypt = null;
		if (isTestValidate()) {// 试用
			keyDecrypt = value.substring(8, 12);
		} else {// 正式
			keyDecrypt = value.substring(0, 4);
		}
		// 测试18 到22 为二级管理员总数 ;正式10 到14
		if (StringUtils.isNotBlank(keyDecrypt)) {
			return Integer.parseInt(keyDecrypt);
		}
		return 0;
	}

	public int getSecondAdminUserInt() {
		String keyDecrypt = null;
		// 测试18 到22 为二级管理员总数 ;正式10 到14
		if (KeyEnmu.OLD_KEY == keyEnmu) {// 旧版本秘钥不存在二级管理员
			return 0;
		}
		if (isTestValidate()) {// 试用
			keyDecrypt = value.substring(18, 22);
		} else {// 正式
			keyDecrypt = value.substring(10, 14);
		}
		if (StringUtils.isNotBlank(keyDecrypt)) {
			return Integer.parseInt(keyDecrypt);
		}
		return 0;
	}

	public int getAdminUserInt() {
		String keyDecrypt = null;
		// 测试18 到22 为二级管理员总数 ;正式10 到14
		if (KeyEnmu.ADMIN_KEY != keyEnmu) {// 旧版本秘钥不存在二级管理员
			return 0;
		}
		if (isTestValidate()) {// 试用
			keyDecrypt = value.substring(18, 22);
		} else {// 正式
			keyDecrypt = value.substring(10, 14);
		}
		if (StringUtils.isNotBlank(keyDecrypt)) {
			return Integer.parseInt(keyDecrypt);
		}
		return 0;
	}

	/**
	 * Tree;验证通过
	 * 
	 * @author yxw 2012-4-27
	 * @description:试用是不是到期
	 * @return
	 */
	private static boolean isDatePass() {
		// 获取日期
		String dateStr = value.substring(0, 8);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {
			// 时间验证 获取秘密到期时间
			Date date = sdf.parse(dateStr);
			// 测试此日期是否在指定日期之后
			if (new Date().after(date)) {// 是否到期
				return false;
			}
			return true;
		} catch (ParseException e) {
			log.error("", e);
		}
		return false;
	}

	/**
	 * MAC地址验证
	 * 
	 * @return
	 * @throws IOException
	 */
	private static boolean isMacPass() throws IOException {
		// MAC地址,旧版秘钥 10, 27;
		String macStr = "";
		int macStart = 10;
		int macEnd = 27;
		if (KeyEnmu.PROCESS_ADMIN_KEY == keyEnmu) {
			macStart = macStart + 4;
			macEnd = macEnd + 4;
		} else if (KeyEnmu.ADMIN_KEY == keyEnmu) {
			macStart = macStart + 4;
			macEnd = macEnd + 4;
		}
		macStr = value.substring(macStart, macEnd);
		String[] allMACAddress = getMacAddress().split(",");
		for (int i = 0; i < allMACAddress.length; i++) {
			if (allMACAddress[i].equals(macStr)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取AES key
	 * 
	 * @return
	 */
	private static String getKey() {
		InputStream in = null;
		try {
			File file = new File(JecnPath.APP_PATH + File.separator + "images" + File.separator + "word.gif");
			Properties properties = new Properties();
			in = new FileInputStream(file);
			properties.load(in);
			String keyEncrypt = properties.getProperty("key.word");

			if (keyEncrypt == null) {
				return "";
			}
			return keyEncrypt;
		} catch (Exception ex) {
		} finally {
			try {
				in.close();
			} catch (Exception e) {
			}

		}
		return "";
	}

	/**
	 * 判断是否为数字
	 * 
	 * @param curStr
	 *            传入的参数
	 * @return
	 */
	private static boolean checkIsNumber(String curStr) {
		if (curStr.matches("[0-9]*")) {
			return true;
		}
		return false;
	}

	/**
	 * @author zhangchen Aug 3, 2012
	 * @description：获得本机的物理地址
	 * @return
	 * @throws IOException
	 */
	private static String getMacAddress() throws IOException {
		String os = System.getProperty("os.name");
		if (os.startsWith("Windows")) {
			ResultBean retBean = JecnCmd.getMacAddr();
			if (retBean.getResultState() == 0) {
				return retBean.getResultStr();
			} else {
				throw new IOException("cmd process error");
			}
		} else {
			throw new IOException("OS not supported : " + os);
		}
	}

	/***************************************************************************
	 * 获得加密密钥
	 * 
	 * @return
	 * @throws Exception
	 */
	private static String getkeyEncrypt() {
		InputStream in = null;
		try {
			File file = new File(keyFilePath);
			Properties properties = new Properties();
			in = new FileInputStream(file);
			properties.load(in);
			String keyEncrypt = properties.getProperty(keyName);

			if (keyEncrypt == null) {
				return "";
			}
			return keyEncrypt;
		} catch (Exception ex) {
		} finally {
			try {
				in.close();
			} catch (Exception e) {

			}
		}
		return "";
	}

	/**
	 * @author zhangchen May 17, 2012
	 * @description：是否存在密钥
	 * @return
	 */
	private static boolean isKeyExist() {
		File f = new File(keyFilePath);
		if (f.exists()) {// 存在
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * 解密
	 * </p>
	 * 
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String data, String key) throws Exception {
		// 可逆
		byte[] byteData = hexStringToByte(data);
		byte[] decrData = decryptBASE64(new String(byteData));
		Key k = toKey(decode(key));
		byte[] raw = k.getEncoded();
		SecretKeySpec secretKeySpec = new SecretKeySpec(raw, ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
		return new String(cipher.doFinal(decrData));
	}

	/**
	 * <p>
	 * 生成密钥
	 * </p>
	 * 
	 * @param seed
	 *            密钥种子
	 * @return
	 * @throws Exception
	 */
	private static String getSecretKey(String seed) throws Exception {
		KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
		SecureRandom secureRandom;
		if (seed != null && !"".equals(seed)) {
			secureRandom = new SecureRandom(seed.getBytes());
		} else {
			secureRandom = new SecureRandom();
		}
		keyGenerator.init(128, secureRandom);
		SecretKey secretKey = keyGenerator.generateKey();
		return encode(secretKey.getEncoded());
	}

	private static final String HEX_MATCH = "0123456789ABCDEF";

	/**
	 * * 将16进制字符串转换成字节数组 *
	 * 
	 * @param str
	 *            String
	 * @return byte[]
	 */
	private static byte[] hexStringToByte(String str) {
		int len = (str.length() / 2);
		byte[] result = new byte[len];
		char[] charArray = str.toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (HEX_MATCH.indexOf(charArray[pos]) << 4 | HEX_MATCH.indexOf(charArray[pos + 1]));
		}
		return result;
	}

	/**
	 * <p>
	 * 二进制数据编码为BASE64字符串
	 * </p>
	 * 
	 * @param bytes
	 * @return
	 * @throws Exception
	 */
	private static String encode(byte[] bytes) throws Exception {
		return new String(Base64.encode(bytes));
	}

	/**
	 * <p>
	 * BASE64字符串解码为二进制数据
	 * </p>
	 * 
	 * @param base64
	 * @return
	 * @throws Exception
	 */
	public static byte[] decode(String base64) throws Exception {
		return Base64.decode(base64.getBytes());
	}

	/**
	 * <p>
	 * 转换密钥
	 * </p>
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private static Key toKey(byte[] key) throws Exception {
		SecretKey secretKey = new SecretKeySpec(key, ALGORITHM);
		return secretKey;
	}

	/**
	 * BASE64解密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private static byte[] decryptBASE64(String key) throws Exception {
		return (new BASE64Decoder()).decodeBuffer(key);
	}

	public static String getValue() {
		return value;
	}

	public static boolean isTest() {
		return test;
	}

	/**
	 * 解密key
	 */
	public static String decryptKey(String keyValue) {
		String str = new String(hexStringToByte(keyValue));
		return key(str);
	}

	private static String encrypt_key = "value";

	/**
	 * 可逆
	 * 
	 * @param txt
	 * @return
	 */
	private static String key(String txt) {
		int ctr = 0;
		String tmp = "";

		for (int i = 0; i < txt.length(); i++) {
			ctr = ctr == encrypt_key.length() ? 0 : ctr;

			int c = txt.charAt(i) ^ encrypt_key.charAt(ctr);

			String x = "" + (char) c;

			tmp = tmp + x;

			ctr++;
		}
		return tmp;
	}

	/**
	 * 返回服务端用户数
	 * 
	 * @return
	 */
	public static int getServerPeopleCounts() {
		if (JecnCommon.isNullOrEmtryTrim(value)) {
			return 0;
		}
		String serverPeopleCounts = "";
		if (isTestValidate()) {// 试用
			// 服务用户数获取
			serverPeopleCounts = value.substring(12, 18);
		} else {
			// 服务用户数获取
			serverPeopleCounts = value.substring(4, 10);
		}
		if (!checkIsNumber(serverPeopleCounts.trim())) {
			return -1;
		}
		return Integer.valueOf(serverPeopleCounts.trim());
	}
}
