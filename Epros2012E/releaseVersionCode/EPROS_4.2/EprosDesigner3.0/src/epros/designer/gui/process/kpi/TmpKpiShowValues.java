package epros.designer.gui.process.kpi;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;

public class TmpKpiShowValues {
	/** KPI 动态标题 */
	private List<JecnConfigItemBean> kpiTitles;
	/** KPI 动态行数据 */
	private List<List<String>> kpiRowValues;
	/** KPI 数据 */
	private List<JecnFlowKpiNameT> kpiNameList;

	public List<JecnFlowKpiNameT> getKpiNameList() {
		if (kpiNameList == null) {
			kpiNameList = new ArrayList<JecnFlowKpiNameT>();
		}
		return kpiNameList;
	}

	public void setKpiNameList(List<JecnFlowKpiNameT> kpiNameList) {
		this.kpiNameList = kpiNameList;
	}

	public List<JecnConfigItemBean> getKpiTitles() {
		if (kpiTitles == null) {
			kpiTitles = new ArrayList<JecnConfigItemBean>();
		}
		return kpiTitles;
	}

	public void setKpiTitles(List<JecnConfigItemBean> kpiTitles) {
		this.kpiTitles = kpiTitles;
	}

	public List<List<String>> getKpiRowValues() {
		if (kpiRowValues == null) {
			kpiRowValues = new ArrayList<List<String>>();
		}
		return kpiRowValues;
	}

	public void setKpiRowValues(List<List<String>> kpiRowValues) {
		this.kpiRowValues = kpiRowValues;
	}
}
