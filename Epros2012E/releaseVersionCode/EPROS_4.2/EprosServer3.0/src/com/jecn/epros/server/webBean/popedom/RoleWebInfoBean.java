package com.jecn.epros.server.webBean.popedom;
/**
 * @author yxw 2013-3-7
 * @description：角色列表bean
 */
public class RoleWebInfoBean {
	private Long roleId;//主键ID
	private String roleName;//角色名称
	private String roleRemark;//备注
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleRemark() {
		return roleRemark;
	}
	public void setRoleRemark(String roleRemark) {
		this.roleRemark = roleRemark;
	}
}
