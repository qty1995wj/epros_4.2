package com.jecn.epros.server.service.process.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnKPIFirstTargetBean;
import com.jecn.epros.server.bean.process.temp.TempProcessKpiBean;
import com.jecn.epros.server.bean.process.temp.TempSearchKpiBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IKPIFirstTargetDao;
import com.jecn.epros.server.dao.process.IProcessKPIValueDao;
import com.jecn.epros.server.service.process.IFlowPKIService;
import com.jecn.epros.server.service.process.buss.FlowJFreeChart;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 流程KPI服务端业务处理类
 * 
 * @author Administrator
 * @date： 日期：Jan 17, 2013 时间：10:40:25 AM
 */
@Transactional(rollbackFor = Exception.class)
public class FlowPKIServiceImpl extends AbsBaseService<JecnFlowKpiName, Long> implements IFlowPKIService {
	private final static Log log = LogFactory.getLog(FlowPKIServiceImpl.class);
	/** 流程KPI数据处理DAO */
	private IProcessKPIValueDao processKPIValueDao;
	/** 流程数据信息处理DAO */
	private IFlowStructureDao flowStructureDao;
	/** 设计器：KPI支撑的一级指标 */
	private IKPIFirstTargetDao kPIFirstTargetDao;

	/**
	 * 根据流程ID获取KPI集合
	 * 
	 * @param flowId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnFlowKpiName> getListJecnFlowKpiName(Long flowId) throws Exception {
		return processKPIValueDao.getListJecnFlowKpiName(flowId);
	}

	/**
	 * 获取流程对应的KPI信息
	 * 
	 * @param flowId
	 *            流程ID
	 * @return TempProcessKpiBean流程KPI信息
	 * @throws Exception
	 */
	@Override
	public TempProcessKpiBean geTempProcessKpiBean(Long flowId) throws Exception {
		TempProcessKpiBean tempProcessKpiBean = new TempProcessKpiBean();
		tempProcessKpiBean.setListFlowKpiName(processKPIValueDao.getListJecnFlowKpiName(flowId));
		List<Long> list = new ArrayList<Long>();
		for (JecnFlowKpiName kpiName : tempProcessKpiBean.getListFlowKpiName()) {
			list.add(kpiName.getKpiAndId());
		}
		tempProcessKpiBean.setKpiNameIds(list);
		// 获取流程信息
		JecnFlowStructure flowStructure = flowStructureDao.findJecnFlowStructureById(flowId);
		if (flowStructure == null) {
			throw new NullPointerException();
		}
		tempProcessKpiBean.setFlowId(flowId);
		tempProcessKpiBean.setFlowName(flowStructure.getFlowName());
		return tempProcessKpiBean;
	}

	/**
	 * 创建流程KPI曲线图
	 * 
	 * @param searchKpiBean
	 *            KPI曲线图生成条件
	 * @throws Exception
	 * 
	 */
	@Override
	public String createFlowKPIDiagram(TempSearchKpiBean searchKpiBean) throws Exception {
		if (searchKpiBean == null || searchKpiBean.getKpiId() == null) {
			throw new NullPointerException();
		}
		FlowJFreeChart freeChart = new FlowJFreeChart(searchKpiBean, processKPIValueDao);
		// 获取KPI名称对象
		JecnFlowKpiName jecnFlowKpiName = (JecnFlowKpiName) processKPIValueDao.getSession().get(JecnFlowKpiName.class,
				searchKpiBean.getKpiId());
		return freeChart.createFreeChart(jecnFlowKpiName);
	}

	/**
	 * 查询某段时间内的KPI值
	 * 
	 * @param searchKpiBean
	 * @throws Exception
	 * 
	 */

	public List<JecnFlowKpi> getFlowKPIValue(long kpiId, String StringStartDate, String StringEndDate) throws Exception {
		// yyyy-MM-dd
		Date startDate = null;
		Date endDate = null;
		if (startDate == null && endDate == null && !JecnCommon.isNullOrEmtryTrim(StringStartDate)
				&& !JecnCommon.isNullOrEmtryTrim(StringEndDate)) {
			startDate = JecnCommon.getDateByString(StringStartDate, "yyyy-MM");
			endDate = JecnCommon.getDateByString(StringEndDate, "yyyy-MM");
		}
		List<JecnFlowKpi> list = (List<JecnFlowKpi>) processKPIValueDao.findJecnFlowKpiList(kpiId, startDate, endDate);
		return list;
	}

	/**
	 * 获取所有 一级指标数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public JecnKPIFirstTargetBean getFirstTarget(String id) throws Exception {

		return kPIFirstTargetDao.getFirstTarget(id);
	}

	public JecnFlowKpi getFlowKPIValue(long kpiId) throws Exception {
		return processKPIValueDao.get(kpiId);
	}

	public JecnFlowKpi getFlowKPIValue(long kpiAndId, String kpiVlaue) throws Exception {
		return processKPIValueDao.getFlowKPIValue(kpiAndId, kpiVlaue);
	}

	public JecnFlowKpiName getFlowKPI(long kpiAndId) throws Exception {
		return processKPIValueDao.getJecnFlowKpiName(kpiAndId);
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	public IProcessKPIValueDao getProcessKPIValueDao() {
		return processKPIValueDao;
	}

	public void setProcessKPIValueDao(IProcessKPIValueDao processKPIValueDao) {
		this.processKPIValueDao = processKPIValueDao;
	}

	public IKPIFirstTargetDao getkPIFirstTargetDao() {
		return kPIFirstTargetDao;
	}

	public void setkPIFirstTargetDao(IKPIFirstTargetDao kPIFirstTargetDao) {
		this.kPIFirstTargetDao = kPIFirstTargetDao;
	}

	/***
	 * 添加一级指标
	 * 
	 * @throws Exception
	 */
	@Override
	public String addFirstTarget(List<JecnKPIFirstTargetBean> list) throws Exception {
		return kPIFirstTargetDao.addFirstTarget(list);
	}

	/**
	 * 获取所有 一级指标数据集合
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnKPIFirstTargetBean> getAllFirstTarget() throws Exception {
		List<JecnKPIFirstTargetBean> list = new ArrayList<JecnKPIFirstTargetBean>();
		List<Object[]> listObj = kPIFirstTargetDao.getAllFirstTarget();
		if (listObj == null || listObj.size() == 0) {
			return null;
		}
		for (Object[] obj : listObj) {
			JecnKPIFirstTargetBean kPIFirstTargetBean = new JecnKPIFirstTargetBean();
			// 主键ID
			kPIFirstTargetBean.setId(obj[0].toString());
			// 内容
			kPIFirstTargetBean.setTargetContent(obj[1].toString());
			// 创建时间
			kPIFirstTargetBean.setCreateTime((Date) obj[2]);
			// 更新时间
			kPIFirstTargetBean.setUpdateTime((Date) obj[3]);
			// 创建人
			kPIFirstTargetBean.setCreatePeopleId(Long.valueOf(obj[4].toString()));
			// 更新人
			kPIFirstTargetBean.setUpdatePeopleId(Long.valueOf(obj[5].toString()));
			list.add(kPIFirstTargetBean);
		}
		return list;
	}

	/**
	 * 定时任务删除所有的流程下的kpi的图片
	 * 
	 * @throws Exception
	 */
	@Override
	public void deleteKpiTempImage() throws Exception {

		String path = JecnPath.APP_PATH;
		// 获取存储临时文件的目录
		String tempFileDir = path + "images/tempImage/" + "PROCESS_KPI_VALUE";
		File tempKpiFile = new File(tempFileDir);
		if (tempKpiFile.exists() && tempKpiFile.isDirectory()) {
			File[] listFiles = tempKpiFile.listFiles();
			for (File temp : listFiles) {
				if (temp.exists()) {
					temp.delete();
				}
			}
		}

	}

	@Override
	public JecnCreateDoc exportFlowKpi(TempSearchKpiBean searchKpiBean) throws Exception {
		WritableWorkbook wbookData = null;
		ByteArrayOutputStream outputStream = null;
		try {
			// KPI类型 1季度 0 月份
			JecnFlowKpiName kpi = processKPIValueDao.getJecnFlowKpiName((Long.valueOf(searchKpiBean.getKpiId())));
			String kpiType = kpi.getKpiHorizontal();
			// 根据输入的时间来获取比较差数yyyy-MM-dd
			String StringStartDate = searchKpiBean.getStartTime();
			String StringEndDate = searchKpiBean.getEndTime();
			searchKpiBean.setFlowId(kpi.getFlowId());
			// 原始数据
			List<JecnFlowKpi> tmpListFlowKpi = getFlowKPIValue(searchKpiBean.getKpiId(), StringStartDate, StringEndDate);
			// 显示到前台的数据
			List<JecnFlowKpi> listFlowKpi = JecnUtil.kpiModelAddCount(tmpListFlowKpi, kpiType, StringStartDate,
					StringEndDate, searchKpiBean);

			outputStream = new ByteArrayOutputStream();
			wbookData = Workbook.createWorkbook(outputStream);
			// 创建工作表
			WritableSheet sheet = wbookData.createSheet("流程绩效指标", wbookData.getNumberOfSheets());
			// 填写列名
			fillColumnName(sheet);
			// 填写数据
			fillCell(sheet, listFlowKpi);
			// 写入工作表
			wbookData.write();
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			outputStream.flush();
			JecnCreateDoc createDoc = new JecnCreateDoc();
			createDoc.setBytes(outputStream.toByteArray());
			createDoc.setFileName("流程绩效指标.xls");
			return createDoc;
		} catch (Exception e) {
			log.error("导出KPI异常！searchKpiBean = " + searchKpiBean, e);
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					log.error(e);
				}
			}
			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error(e);
				}
			}
		}
		return null;
	}

	/**
	 * 填写列名
	 * 
	 * @param sheet
	 * @param table
	 * @param colNum
	 * @throws WriteException
	 * @throws WriteException
	 */
	private static void fillColumnName(WritableSheet sheet) {
		WritableFont font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);// 定义字体
		WritableCellFormat format = new WritableCellFormat(font);// 定义格式化对象

		try {
			format.setAlignment(Alignment.CENTRE);
			// 水平居中显示
			sheet.setColumnView(0, 15);// 设置列宽
			sheet.setColumnView(1, 15);// 设置列宽
			Label colTime = new Label(0, 0, JecnUtil.getValue("date"), format);
			Label colValue = new Label(1, 0, JecnUtil.getValue("kpiValue"), format);
			sheet.addCell(colTime);
			sheet.addCell(colValue);
		} catch (WriteException e) {
			log.error("填写列名异常", e);
		}
	}

	/**
	 * 填写数据
	 * 
	 * @param sheet
	 * @param talbe
	 * @param rowNum
	 * @param colNum
	 * @throws WriteException
	 */
	private void fillCell(WritableSheet sheet, List<JecnFlowKpi> list) {
		for (int i = 0; i < list.size(); i++) {
			JecnFlowKpi jecnFlowKpi = list.get(i);
			Label kpiHorVlaue = new Label(0, i + 1, JecnCommon.getStringbyDate(jecnFlowKpi.getKpiHorVlaue(), "yyyy-MM"));
			Label kpiValue = new Label(1, i + 1, jecnFlowKpi.getKpiValue());
			try {
				sheet.addCell(kpiHorVlaue);
				sheet.addCell(kpiValue);
			} catch (Exception e) {
				log.error("填写数据异常", e);
			}
		}
	}
}
