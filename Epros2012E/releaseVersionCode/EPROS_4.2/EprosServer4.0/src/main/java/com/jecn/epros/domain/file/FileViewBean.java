package com.jecn.epros.domain.file;

import java.util.List;

public class FileViewBean {
    private int total;
    private List<SearchFileResultBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<SearchFileResultBean> getData() {
        return data;
    }

    public void setData(List<SearchFileResultBean> data) {
        this.data = data;
    }
}
