
package com.jecn.webservice.addon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>RETURNMSG complex type�� Java �ࡣ
 * 
 * <p>����ģʽƬ��ָ�����ڴ����е�Ԥ�����ݡ�
 * 
 * <pre>
 * &lt;complexType name="RETURNMSG"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RESULT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MESSAGE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EMPLOYEELIST" type="{urn:DefaultNamespace}PROPS"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RETURNMSG", propOrder = {
    "result",
    "message",
    "employeelist"
})
public class RETURNMSG {

    @XmlElement(name = "RESULT", required = true)
    protected String result;
    @XmlElement(name = "MESSAGE", required = true)
    protected String message;
    @XmlElement(name = "EMPLOYEELIST", required = true, nillable = true)
    protected PROPS employeelist;

    /**
     * ��ȡresult���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESULT() {
        return result;
    }

    /**
     * ����result���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESULT(String value) {
        this.result = value;
    }

    /**
     * ��ȡmessage���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMESSAGE() {
        return message;
    }

    /**
     * ����message���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMESSAGE(String value) {
        this.message = value;
    }

    /**
     * ��ȡemployeelist���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link PROPS }
     *     
     */
    public PROPS getEMPLOYEELIST() {
        return employeelist;
    }

    /**
     * ����employeelist���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link PROPS }
     *     
     */
    public void setEMPLOYEELIST(PROPS value) {
        this.employeelist = value;
    }

}
