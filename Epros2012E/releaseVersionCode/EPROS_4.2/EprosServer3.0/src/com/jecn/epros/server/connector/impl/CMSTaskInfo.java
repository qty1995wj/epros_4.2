package com.jecn.epros.server.connector.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.action.web.login.ad.cms.JecnCMSAfterItem;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.webservice.cms.task.CMSTaskClient;
import com.jecn.webservice.cms.task.ProMessageOjbect;
import com.jecn.webservice.cms.task.bean.AddPendingTask;

/**
 * 招商证券-OA待办消息
 * 
 * @author ZXH
 * @date 2018-3-27
 */
public class CMSTaskInfo extends JecnBaseTaskSend {

	private IPersonDao personDao;

	private static String PROCESS_NAME = JecnCMSAfterItem.getValue("PROCESS_NAME");

	private static String SYS_NAME = JecnCMSAfterItem.getValue("TASK_SYS_NAME");

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			log.info("创建代办开始~");
			// 创建代办
			createTasks(myTaskBean, beanNew, handlerPeopleIds);
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 上一阶段改为已办
			JecnUser handlerUser = personDao.get(curPeopleId);
			changeStatusToHandled(myTaskBean, beanNew, handlerUser.getLoginName(), curPeopleId);
		} else {// 一条from的数据,一条to 的数据
			// 代办转已办,是否为多审判断
			// 设置上一环节任务待办任务状态为false，表示未已审批；
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			JecnUser handlerUser = personDao.get(curPeopleId);
			// 上一个阶段，处理任务，代办转已办
			changeStatusToHandled(myTaskBean, beanNew, handlerUser.getLoginName(), curPeopleId);

			if ((beanNew.getTaskElseState() == 2 || beanNew.getTaskElseState() == 3)
					|| beanNew.getState() != beanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				// 审批任务，发送下一环节任务待办
				createTasks(myTaskBean, beanNew, handlerPeopleIds);
			}
		}
	}

	/**
	 * 批量删除代办
	 */
	@Override
	public void sendInfoCancels(JecnTaskBeanNew taskBeanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {

		this.personDao = personDao;
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
		Set<Long> setCancelPeopleIds = new HashSet<Long>();
		setCancelPeopleIds.addAll(cancelPeoples);
		// 上一个阶段，代办删除
		List<JecnUser> users = personDao.getJecnUserList(setCancelPeopleIds);
		String stateMark = null;
		if (taskBeanNew.getTaskElseState() == 4 || taskBeanNew.getTaskElseState() == 13) {
			stateMark = taskBeanNew.getUpStateMark();
		} else {
			stateMark = taskBeanNew.getStateMark();
		}
		for (JecnUser user : users) {
			ProMessageOjbect messageOjbect = CMSTaskClient.INSTANCE.DeletePendingTask(getSN(taskBeanNew.getId(),
					taskBeanNew.getRid()), SYS_NAME, stateMark, user.getLoginName());
			System.out.println(messageOjbect.getMessage());
		}

		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		// 审批任务，发送下一环节任务待办
		createTasks(myTaskBean, taskBeanNew, handlerPeopleIds);

	}

	/**
	 * 代办转已办
	 */
	private void changeStatusToHandled(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, String handlerUser,
			Long curPeopleId) throws Exception {
		// 代办转已办接口
		ProMessageOjbect message = CMSTaskClient.INSTANCE.ChangeStatusToHandled(getSN(taskBeanNew.getId(), taskBeanNew
				.getRid()), SYS_NAME, taskBeanNew.getUpStateMark(), handlerUser, getMailUrl(myTaskBean.getTaskId(),
				curPeopleId, false, true));
		if (message.getResult() != 1) {
			throw new Exception(message.getMessage());
		}
	}

	/**
	 * 创建代办
	 * 
	 * @param myTaskBean
	 * @param taskBeanNew
	 * @throws Exception
	 */
	private void createTasks(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, Set<Long> handlerPeopleIds)
			throws Exception {
		JecnUser createUser = personDao.get(taskBeanNew.getCreatePersonId());

		for (Long handlerPeopleId : handlerPeopleIds) {
			AddPendingTask pendingTask = getPendingTask(myTaskBean, taskBeanNew, handlerPeopleId, createUser);
			log.info("pendingTask = " + pendingTask.toString());
			Object[] message = CMSTaskClient.INSTANCE.AddPendingTask(pendingTask);
			if (message == null || message.length == 0) {
				throw new Exception();
			}
		}
	}

	private AddPendingTask getPendingTask(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, Long handlerPeopleId,
			JecnUser createUser) {
		AddPendingTask pendingTask = new AddPendingTask();
		pendingTask.setSn(getSN(taskBeanNew.getId(), taskBeanNew.getRid()));
		pendingTask.setProcessName(PROCESS_NAME);
		// 阶段名称
		pendingTask.setActivityName(myTaskBean.getStageName());

		pendingTask.setOriginator(createUser.getLoginName());
		pendingTask.setOriginatorName(createUser.getTrueName());

		// pendingTask.setOriginatorDeptId(originatorDeptId);
		// 代办处理人
		JecnUser handlerUser = personDao.get(handlerPeopleId);
		pendingTask.setSendTo(handlerUser.getLoginName());
		pendingTask.setSubject(taskBeanNew.getTaskName());
		pendingTask.setUrl(getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, false));
		Calendar createTask = Calendar.getInstance();
		createTask.setTime(taskBeanNew.getCreateTime());
		pendingTask.setProcessBuildDate(createTask);

		Calendar updateTask = Calendar.getInstance();
		updateTask.setTime(taskBeanNew.getUpdateTime());
		pendingTask.setCreateDate(updateTask);

		pendingTask.setInComingSysName(SYS_NAME);
		return pendingTask;
	}

	private String getSN(Long taskId, Long rId) {
		return taskId + "_" + rId;
	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(getTaskName(beanNew) + "审批");
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setStageName(beanNew.getStateMark());
		return myTaskBean;
	}

	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew, JecnUser jecnUser, JecnUser createUser) {
		List<Long> cancelPeoples = new ArrayList<Long>();
		cancelPeoples.add(curPeopleId);
		try {
			sendInfoCancels(jecnTaskBeanNew, personDao, null, cancelPeoples, curPeopleId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
