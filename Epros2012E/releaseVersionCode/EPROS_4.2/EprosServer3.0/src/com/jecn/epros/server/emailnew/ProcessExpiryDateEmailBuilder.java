package com.jecn.epros.server.emailnew;

import com.jecn.epros.server.util.JecnUtil;

public class ProcessExpiryDateEmailBuilder extends FixedContentEmailBuilder {
	private int type = 0;

	@Override
	protected EmailBasicInfo getEmailBasicInfo() {
		EmailBasicInfo basicInfo = new EmailBasicInfo();
		Object[] data = getData();
		String flowName = data[0].toString();
		String endScanTime = data[1].toString();
		this.type = (Integer) data[2];
		String subject = handle(JecnUtil.getValue("flowExpiryOne"));
		String content = JecnUtil.getValue("flowExpiryTwo") + flowName 
		        + handle(JecnUtil.getValue("flowExpirySeven"))+ endScanTime 
		        + JecnUtil.getValue("flowExpiryEight") + flowName
				+ handle(JecnUtil.getValue("flowExpiryFive"));
		basicInfo.setSubject(subject);
		basicInfo.setContent(content);
		return basicInfo;
	}

	private String handle(String value) {
		if (this.type == 0) {
			return value;
		}
		return value.replaceAll("流程", "制度");
	}

}
