package epros.designer.gui.process.guide.explain;

import java.awt.Insets;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;

import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.DrawCommon;

/**
 * 流程文控信息
 * 
 * @author user
 * 
 */
public class JecnProcessHistory extends JecnFileDescriptionTable {

	private Logger log = Logger.getLogger(JecnRelatedFlow.class);
	private Long flowId;
	protected List<JecnTaskHistoryNew> listHistory;

	public JecnProcessHistory(int index, String name, JecnPanel contentPanel, boolean isRequest, Long flowId, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowId = flowId;
		try {
			listHistory = ConnectionPool.getDocControlAction().getJecnTaskHistoryNewList(flowId, 0);
		} catch (Exception e) {
			log.error("JecnProcessHistory is error", e);
		}

		super.initTable();
	}

	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> flowControl = new Vector<Vector<String>>();
		List<JecnTaskHistoryFollow> listHisFollow = null;
		for (JecnTaskHistoryNew taskHistoryNew : listHistory) {
			Vector<String> vec = new Vector<String>();
			// 流程ID
			vec.add(taskHistoryNew.getId().toString());
			// 版本号
			vec.add(taskHistoryNew.getVersionId());
			// 变更说明
			vec.add(taskHistoryNew.getModifyExplain());
			// 发布日期
			vec.add(taskHistoryNew.getPublishDate().toString());
			// 流程拟制人(拟稿人)
			vec.add(taskHistoryNew.getDraftPerson());
			// 审批人阶段名称
			try {
				String historyFollo = "";
				listHisFollow = ConnectionPool.getDocControlAction().getJecnTaskHistoryFollowList(
						taskHistoryNew.getId());
				for (JecnTaskHistoryFollow taskHistoryFollow : listHisFollow) {
					if (!DrawCommon.isNullOrEmtryTrim(taskHistoryFollow.getName())) {
						if ("".equals(historyFollo)) {
							historyFollo += taskHistoryFollow.getAppellation() + "：" + taskHistoryFollow.getName();
						} else {
							historyFollo += "\n" + taskHistoryFollow.getAppellation() + "："
									+ taskHistoryFollow.getName();
						}

					}
				}
				vec.add(historyFollo);
			} catch (Exception e) {
				log.error("JecnProcessHistory getContent is error", e);
			}
			flowControl.add(vec);
		}
		return flowControl;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void dbClickMethod() {
		// TODO Auto-generated method stub

	}

	@Override
	protected JecnTableModel getTableModel() {

		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("versionId"));// 版本号
		title.add(JecnProperties.getValue("modifyExplain"));// 变更说明
		title.add(JecnProperties.getValue("publishDate"));// 发布日期
		if (JecnConfigTool.isBLOperType()) {// 上海宝隆
			title.add(JecnProperties.getValue("fictionPeople"));// 流程拟制人
		} else {
			title.add(JecnProperties.getValue("drafterPeople"));// 拟稿人
		}
		title.add(JecnProperties.getValue("historyFollow"));// 审批人
		return new JecnTableModel(title, getContent());

	}

}
