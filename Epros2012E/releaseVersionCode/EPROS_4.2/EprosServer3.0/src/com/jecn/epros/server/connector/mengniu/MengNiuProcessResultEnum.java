package com.jecn.epros.server.connector.mengniu;

public enum MengNiuProcessResultEnum {
	agree("同意"), reject("拒绝"), countersign("加签"), redirect("转签"), return_("退回"), cancel("撤销"), terminate("终止"), lose(
			"失效");

	String value = null;

	MengNiuProcessResultEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
