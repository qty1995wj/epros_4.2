//全屏
function fullScreenMain(){
	var fullHeight = window.screen.availHeight;
	var mainArea = document.getElementById("showFrame");
	mainArea.height = fullHeight-300;
}

//左边树
function fullScreenLeft(){
	var fullHeight = window.screen.availHeight;
	var leftArea = document.getElementById("treeFrame");
	leftArea.height = fullHeight-350;
}

//右边主体
function fullScreenRight(){
	var fullHeight = window.screen.availHeight;
	var rightArea = document.getElementById("screenRight");
	rightArea.height = fullHeight-335;
}

//java app图全屏1
function fullScreenImg(){
	var fullHeight = window.screen.availHeight;
	var rightArea = document.getElementById("show");
	rightArea.style.height = fullHeight-355;
}

//java app图全屏2
function fullScreenImgFlow(){
	var fullHeight = window.screen.availHeight;
	if(document.getElementById("show")!=null){
		var rightArea = document.getElementById("show");
		rightArea.style.height = fullHeight-355;
	}
	if(document.getElementById("show2")!=null){
		var rightArea2 = document.getElementById("show2");
		rightArea2.style.height = fullHeight-355;
	}
	if(document.getElementById("show3")!=null){
		var rightArea3 = document.getElementById("show3");
		rightArea3.style.height = fullHeight-370;
	}
	if(document.getElementById("show4")!=null){
		var rightArea3 = document.getElementById("show4");
		rightArea3.style.height = fullHeight-390;
	}
}

function fullScreenContext(){
	var fullHeight = window.screen.availHeight;
	var rightArea = document.getElementById("contextJecnId");
	rightArea.style.height = fullHeight-365;
}

function fullScreenUpTable(){
	var fullHeight = window.screen.availHeight;
	var rightArea = document.getElementById("tableUpJecnId");
	rightArea.style.height = (fullHeight-440)/2;
}

function fullScreenDownTable(){
	var fullHeight = window.screen.availHeight;
	var rightArea = document.getElementById("tableDownJecnId");
	rightArea.style.height = (fullHeight-370)/2;
}