package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.AbstractSyncSpecialDataFilter;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;

/**
 * 石勘院数据同步过滤器
 * 
 * @author thinkpad
 * 
 */
public class ShiHuaSyncDataFilter extends AbstractSyncSpecialDataFilter {

	public ShiHuaSyncDataFilter(BaseBean baseBean) {
		super(baseBean);
	}

	@Override
	protected void initSpecialDataPath() {
		this.specialDataPath = JecnPath.CLASS_PATH + "cfgFile/shihua/shihuaSyncSpecialData.xls";
	}

	@Override
	protected void filterDeptDataBeforeCheck() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void filterUserPosDataBeforeCheck() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeUnusefulDataAfterCheck() {
		// TODO Auto-generated method stub

	}

}
