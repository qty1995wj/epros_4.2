package com.jecn.epros.server.action.web.login.ad.zhongsh;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.zhongruitongxin.ZhongRuiAfterItem;
import com.jecn.epros.server.util.JecnPath;

/**
 * 
 * 中睿通信单点登录配置信息
 * 
 */
public class ZhongSHAfterItem {

	private static Logger log = Logger.getLogger(ZhongRuiAfterItem.class);
	public static String EPROS_LOGIN_URL;
	public static final QName SERVICE_NAME = new QName("http://tempuri.org/",
			"GetUserByTokenService");

	public static URL SERVICE_URL = null;

	public static void start() {
		// 读取配置文件
		InputStream fis = null;
		Properties properties = new Properties();
		try {
			String url = JecnPath.CLASS_PATH
					+ "cfgFile/zhongshihua/zhongShiHua.properties";
			log.info("配置文件路径:" + url);
			fis = new FileInputStream(url);
			properties.load(fis);

			EPROS_LOGIN_URL = properties.getProperty("EPROS_LOGIN_URL");
			SERVICE_URL = new URL(properties.getProperty("SERVICE_URL"));
		} catch (Exception e) {
			log.error("解析中睿配置登录配置文件异常", e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					log.error(e);
				}
			}
		}
	}
}
