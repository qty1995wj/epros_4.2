<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div align="right" style="margin-top: 5px; margin-right: 25px;">
			<table style="margin-top: 3px;">
				<tr align="left">
					<td>
						<!--
						<img src="${basePath}images/allShow.gif" style="cursor: pointer;"
							target='_blank' class="table" onclick="showAllStandardList()" />
							images/common/downLoadExcel.gif
						-->
						<s:if test="#session.webLoginBean.isAdmin||#session.webLoginBean.isViewAdmin">
							<img src="${basePath}images/download.gif" style="cursor: pointer;"
								target='_blank' class="table" onclick="downloadStandardList()" />
						</s:if>
					</td>
				</tr>
			</table>
		</div>
		<div id="standardListShow_id">
		</div>
		<input type="hidden" id="standardId" />
	</body>
</html>
