package epros.designer.gui.common;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.gui.process.flow.RecylePathDialog;
import epros.designer.table.CheckBoxRenderer;
import epros.designer.table.CheckButtonEditor;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JecnSearchPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 回收站面板
 * 
 * @author 2012-08-08
 * 
 */
public abstract class JecnRecycleManageDialog extends JecnDialog {
	/** 主面板 */
	private JPanel mainPanel = null;
	/** table面板 */
	private JPanel tablePanel = null;
	/** table滚动面板 */
	private JScrollPane scrollPanel = null;
	/** table数据 */
	protected RecyleTable table = null;
	/** 底部面板 */
	private JPanel bottomPanel = null;
	/** 按钮面板 */
	private JPanel buttonPanel = null;
	/** 路径 */
	private JButton pathButton = null;
	/** 恢复 */
	private JButton recyButton = null;
	/** 删除 */
	private JButton deleteButton = null;
	/** 关闭 */
	private JButton closeButton = null;
	/** 复选框 */
	private JCheckBox selectAllBox = null;
	/** 验证提示 */
	protected JLabel verfyLab = new JLabel();

	/***搜索Panel */
	protected JecnSearchPanel searchPanel = new JecnSearchPanel();
	/**要删除的ID集合*/
	protected Set<Long> deleteIds = new HashSet<Long>();

	public JCheckBox getSelectAllBox() {
		return selectAllBox;
	}

	public void setSelectAllBox(JCheckBox selectAllBox) {
		this.selectAllBox = selectAllBox;
	}

	public JecnRecycleManageDialog() {
		this.setTitle(getRecycleTitle());
		this.setResizable(true);
		this.setModal(true);
		initCompotents();
		this.setLocationRelativeTo(null);
		// 设置居中
		verfyLab.setForeground(Color.red);
		initLayout();
		if (isViewPathButton()) {
			pathButton.setVisible(true);
		} else {
			pathButton.setVisible(false);
		}
	}

	private void initCompotents() {
		Dimension d = new Dimension(500, 500);
		this.setSize(d);
		mainPanel = new JecnPanel();
		d = new Dimension(480, 425);
		tablePanel = new JecnPanel();
		// tablePanel.setPreferredSize(d);
		// tablePanel.setMinimumSize(d);
		//
		scrollPanel = new JScrollPane();
		scrollPanel.setPreferredSize(d);
		scrollPanel.setMinimumSize(d);

		table = new RecyleTable(this);
		bottomPanel = new JecnPanel();
		// d = new Dimension(490, 40);
		// bottomPanel.setPreferredSize(d);
		// bottomPanel.setMinimumSize(d);

		buttonPanel = new JecnPanel();
		// d = new Dimension(420, 30);
		// buttonPanel.setPreferredSize(d);
		// buttonPanel.setMinimumSize(d);
		// 全选
		selectAllBox = new JCheckBox(JecnProperties.getValue("selectAll"));
		recyButton = new JButton(JecnProperties.getValue("recyBtn"));
		deleteButton = new JButton(JecnProperties.getValue("deleteBtn"));
		closeButton = new JButton(JecnProperties.getValue("closeBtn"));
		pathButton = new JButton(JecnProperties.getValue("recyclePath"));

		searchPanel.setVisible(false);

		scrollPanel.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		// 透明
		selectAllBox.setOpaque(false);

		selectAllBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				selectAllBoxAction();

			}
		});
		recyButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				recyButtonAction();
			}
		});
		pathButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pathButtonAction();
			}
		});
		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				deleteButtonAction();
			}
		});
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnRecycleManageDialog.this.dispose();
			}
		});

		searchPanel.addButtonActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				searchButPerformed();
				searchPanel.textFiledRequestFocusInWindow();
			}
		});
	}

	private void initLayout() {
		Insets insets = new Insets(5, 5, 1, 5);
		GridBagConstraints c = null;

		// 把table加入滚动面板
		scrollPanel.setViewportView(table);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				new Insets(10, 5, 1, 5), 0, 0);
		mainPanel.add(searchPanel, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(scrollPanel, c);
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);

		mainPanel.add(bottomPanel, c);
		// 复选框和按钮面板放在bottomPanel上
		// bottomPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		bottomPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);

		bottomPanel.add(selectAllBox, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);

		bottomPanel.add(new JLabel(), c);
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				new Insets(1, 1, 1, 1), 0, 0);

		bottomPanel.add(buttonPanel, c);
		// bottomPanel.add(selectAllBox);
		// bottomPanel.add(buttonPanel);
		// 按钮布局
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(pathButton);
		buttonPanel.add(recyButton);
		buttonPanel.add(deleteButton);
		buttonPanel.add(closeButton);

		this.getContentPane().add(mainPanel);

	}

	/** 获得面板标题 */
	public abstract String getRecycleTitle();

	/**
	 * 是否显示路径按钮
	 * 
	 * @return
	 */
	public abstract boolean isViewPathButton();

	/**
	 * 获得table的数据
	 */
	public abstract Object[][] getTableData();

	/**
	 * 
	 * 获取类型的名称
	 * 
	 * @return
	 */
	public abstract String getTableType();

	/**
	 * 显示路径
	 */
	private void pathButtonAction() {
		// int selectRow = table.getSelectedRow();
		// if (selectRow == -1) {
		// JOptionPane.showMessageDialog(null, JecnProperties
		// .getValue("chooseOneRow"));
		// return;
		// }
		// DefaultTableModel defaultTableModel = (DefaultTableModel) table
		// .getModel();
		// RecylePathDialog recyleFlowPathDialog = new RecylePathDialog(
		// Long.valueOf(defaultTableModel.getValueAt(selectRow, 2)
		// .toString()), Integer.parseInt(defaultTableModel
		// .getValueAt(selectRow, 1).toString()));
		// recyleFlowPathDialog.setVisible(true);
		verfyLab.setText("");
		// 要显示路径的数据所在行
		int i = 0;
		DefaultTableModel model = (DefaultTableModel) this.table.getModel();
		// 判断复选框是否有选中的，没有选中提示，请勾选复选框
		int countModel = model.getRowCount();
		// 被选中的行数
		int selectedNum = 0;
		for (int k = 0; k < countModel; k++) {
			if (((JCheckBox) model.getValueAt(k, 0)).isSelected()) {
				selectedNum++;
				i = k;
				continue;
			}
		}
		if (selectedNum == 0 || selectedNum >= 2) {
			verfyLab.setText(JecnProperties.getValue("chooseOneOperateData"));
			return;
		} else {
			DefaultTableModel defaultTableModel = (DefaultTableModel) table
					.getModel();
			RecylePathDialog recyleFlowPathDialog = new RecylePathDialog(Long
					.valueOf(defaultTableModel.getValueAt(i, 2).toString()),
					Integer.parseInt(defaultTableModel.getValueAt(i, 1)
							.toString()),0);
			recyleFlowPathDialog.setVisible(true);
		}
	}

	public void searchButPerformed() {
		// 清空表格
		((DefaultTableModel) table.getModel()).setRowCount(0);
		searchButAction();
	}

	public abstract void searchButAction();

	/**
	 * 全选操作
	 */
	private void selectAllBoxAction() {
		if (selectAllBox.isSelected()) {
			int row = table.getRowCount();
			DefaultTableModel defaultTableModel = (DefaultTableModel) table
					.getModel();
			for (int i = 0; i < row; i++) {
				((JCheckBox) defaultTableModel.getValueAt(i, 0))
						.setSelected(true);
			}
		} else {
			int row = table.getRowCount();
			DefaultTableModel defaultTableModel = (DefaultTableModel) table
					.getModel();
			for (int i = 0; i < row; i++) {
				((JCheckBox) defaultTableModel.getValueAt(i, 0))
						.setSelected(false);
			}
		}
		table.updateUI();
	}

	/**
	 * 删除数据库的数据
	 */
	public abstract boolean deleteData(List<Long> ListIds, Long curProjectId);

	/**
	 * 删除按钮
	 */
	private void deleteButtonAction() {
		verfyLab.setText("");
		int rows = table.getRowCount();
		List<Long> ids = new ArrayList<Long>();
		DefaultTableModel model = (DefaultTableModel) this.table.getModel();
		// 判断复选框是否有选中的，没有选中提示，请勾选复选框
		int countModel = model.getRowCount();
		// 选中的行数
		int selectedNum = 0;
		for (int i = 0; i < countModel; i++) {
			if (((JCheckBox) model.getValueAt(i, 0)).isSelected()) {
				selectedNum++;
				continue;
			}
		}
		if (selectedNum == 0) {
			verfyLab.setText(JecnProperties.getValue("chooseOneDeleteData"));
			return;
		}

		for (int i = 0; i < rows; i++) {
			if (((JCheckBox) model.getValueAt(i, 0)).isSelected()
					&& model.getValueAt(i, 1) != null
					&& !"".equals(model.getValueAt(i, 1).toString())) {
				Long id = Long.valueOf(model.getValueAt(i, 2).toString());
				ids.add(id);
			}
		}
		if (ids.size() == 0) {
			return;
		}
		// 确认提示框
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		if (!deleteData(ids, JecnConstants.getUserId())) {
			// 服务器异常
			return;
		}

		// 选中删除的流程的节点ID及子节点集合
		if(deleteIds!= null && deleteIds.size()>0){
			for (Long id : deleteIds) {
				for (int i = rows - 1; i >= 0; i--) {
					if (Long.valueOf(model.getValueAt(i, 2).toString()).equals(id)) {
						((DefaultTableModel) table.getModel()).removeRow(i);
						rows--;
					}
				}
			}
		}else{
			for (int i = rows - 1; i >= 0; i--) {
				if (((JCheckBox) model.getValueAt(i, 0)).isSelected()) {
					((DefaultTableModel) table.getModel()).removeRow(i);
				}
			}
		}
		
		
	}

	/**
	 * 恢复数据库的数据
	 */
	public abstract boolean recyData(List<Long> ids);

	/**
	 * 恢复按钮
	 */
	private void recyButtonAction() {
		verfyLab.setText("");
		// 要恢复的数据所在行
		int i = 0;
		DefaultTableModel model = (DefaultTableModel) this.table.getModel();
		// 判断复选框是否有选中的，没有选中提示，请勾选复选框
		int countModel = model.getRowCount();
		// 被选中的行数
		int selectedNum = 0;
		/**获取所有删除状态的数据Id集合*/
		Map<String,String> map = new HashMap<String,String>();
		for (int k = 0; k < countModel; k++) {
			if (((JCheckBox) model.getValueAt(k, 0)).isSelected()) {
				selectedNum++;
				i = k;
				map.put(String.valueOf(k), model.getValueAt(k, 2)
						.toString());
				continue;
			}
			map.put(String.valueOf(k), model.getValueAt(k, 2)
					.toString());
		}
		if (selectedNum == 0 || selectedNum >= 2) {
			verfyLab.setText(JecnProperties.getValue("choicePrompt"));
			return;
		} else {
			if(!isViewPathButton()){
				Long recyLong = Long.valueOf(((DefaultTableModel) table.getModel())
						.getValueAt(i, 2).toString());
				List<Long> listIds = new ArrayList<Long>();
				listIds.add(recyLong);
				if (!recyData(listIds)) {
					// 服务器异常
					return;
				} else {
					if (((JCheckBox) model.getValueAt(i, 0)).isSelected()) {
						((DefaultTableModel) table.getModel()).removeRow(i);
					}
				}
			}else{
				RecylePathDialog recyleFlowPathDialog = new RecylePathDialog(Long
						.valueOf(model.getValueAt(i, 2).toString()),
						Integer.parseInt(model.getValueAt(i, 1)
								.toString()),1);
				recyleFlowPathDialog.setVisible(true);
				if(!recyleFlowPathDialog.isOpersion()){
					return;
				}
				if (!recyData(recyleFlowPathDialog.getListIds())) {
					// 服务器异常
					return;
				} else {
					// 选中删除的流程的节点ID集合
					for (int k = countModel - 1; k >= 0; k--) {
						if(recyleFlowPathDialog.getListIds().contains(Long.valueOf(map.get(String.valueOf(k))))){
							((DefaultTableModel) table.getModel()).removeRow(k);
						}
					}
				}
			}
		}
	}

	public class RecyleTable extends JTable {
		public RecyleTable(JecnRecycleManageDialog recycleDialog) {
			this.setModel(new RecyleTableMode(getTableData(), this
					.getTableTitle()));
			TableColumnModel tableColumnModel = this.getColumnModel();
			TableColumn column0 = tableColumnModel.getColumn(0);
			column0.setCellEditor(new CheckButtonEditor(new JCheckBox(),recycleDialog,this));
			column0.setCellRenderer(new CheckBoxRenderer());
			column0.setMaxWidth(18);
			TableColumn column1 = tableColumnModel.getColumn(1);
			column1.setMinWidth(0);
			column1.setMaxWidth(0);
			column1.setWidth(0);
			column1.setPreferredWidth(0);
			TableColumn column2 = tableColumnModel.getColumn(2);
			column2.setMinWidth(0);
			column2.setMaxWidth(0);
			column2.setWidth(0);
			column2.setPreferredWidth(0);
			TableColumn column4 = tableColumnModel.getColumn(4);
			column4.setMaxWidth(80);
			column4.setMinWidth(80);
			TableColumn column5 = tableColumnModel.getColumn(5);
			column5.setMinWidth(0);
			column5.setMaxWidth(0);
			column5.setWidth(0);
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
			this.updateUI();
			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		}

		/**
		 * 获得Table的表头
		 * 
		 * @return
		 */
		private Object[] getTableTitle() {
			Object[] title = new Object[6];
			title[0] = ("");
			title[1] = ("");
			title[2] = "ID";// 主键Id
			title[3] = getTableType();
			title[4] = JecnProperties.getValue("type");
			title[5] = ("perId");// 父ID
			return title;
		}

	}

	class RecyleTableMode extends DefaultTableModel {
		public RecyleTableMode(Object[][] data, Object[] title) {
			super(data, title);
		}

		@Override
		public Class getColumnClass(int columnIndex) {
			if (columnIndex == 4) {
				return ImageIcon.class;
			} else {
				return super.getColumnClass(columnIndex);
			}
		}

		public boolean isCellEditable(int rowindex, int colindex) {
			if (colindex == 0) {
				return true;
			}
			return false;
		}

	}
}
