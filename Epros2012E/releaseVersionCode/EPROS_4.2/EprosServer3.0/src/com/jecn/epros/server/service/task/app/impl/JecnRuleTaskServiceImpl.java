package com.jecn.epros.server.service.task.app.impl;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.external.TaskParam;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.task.JecnTaskApplicationNew;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;
import com.jecn.epros.server.bean.task.temp.JecnTaskFileTemporary;
import com.jecn.epros.server.bean.task.temp.SimpleSubmitMessage;
import com.jecn.epros.server.bean.task.temp.SimpleTaskBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;

/**
 * 制度审批业务处理类
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 27, 2012 时间：10:56:52 AM
 */
@Transactional(rollbackFor = Exception.class)
public class JecnRuleTaskServiceImpl extends JecnCommonTaskService implements IJecnBaseTaskService {
	/**
	 * 获取制度
	 * 
	 * @param simpleTaskBean
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public void getRuleInfomation(SimpleTaskBean simpleTaskBean) throws Exception {
		JecnTaskBeanNew taskBeanNew = simpleTaskBean.getJecnTaskBeanNew();
		if (taskBeanNew == null) {//
			log.error("JecnRuleTaskServiceImpl类getFileInfomation方法中参数为空!");
			throw new IllegalArgumentException("参数不能为空!");
		}
		Long ruleId = taskBeanNew.getRid();
		RuleT ruleT = ruleDao.get(ruleId);
		if (ruleT == null) {
			log.error("JecnRuleTaskBS类getRuleInfomation方法中制度不存在!");
			throw new IllegalArgumentException("参数不能为空!");
		}
		// 制度密级
		if (ruleT.getIsPublic() != null) {
			simpleTaskBean.setStrPublic(ruleT.getIsPublic().toString());
		}
		// 制度阶段名称
		simpleTaskBean.setPrfName(ruleT.getRuleName());
		// 制度编号
		simpleTaskBean.setPrfNumber(ruleT.getRuleNumber());
		// 制度类别
		if (simpleTaskBean.getJecnTaskApplicationNew().getFileType() != 0 && ruleT.getTypeId() != null) {
			simpleTaskBean.setPrfType(ruleT.getTypeId().toString());
			// 获取文件类别名称
			simpleTaskBean.setPrfTypeName(getTypeName(ruleT.getTypeId(), simpleTaskBean.getListTypeBean()));
		}
		// 获取制度父节点
		if (ruleT.getPerId() != null) {
			RuleT preRuleT = ruleDao.get(ruleT.getPerId());
			if (preRuleT == null) {
				throw new IllegalArgumentException("制度任务getRuleInfomation获取制度父节点异常！");
			}
			simpleTaskBean.setPrfPreName(preRuleT.getRuleName());
		}
	}

	/**
	 * 制度审批变动
	 * 
	 * @param submitMessage
	 *            提交任务信息
	 * @param taskBeanNew
	 *            任务主表
	 * @throws Exception
	 */
	protected String updateAssembledRuleTaskFixed(SimpleSubmitMessage submitMessage, JecnTaskBeanNew taskBeanNew)
			throws Exception {
		Long rid = taskBeanNew.getRid();
		RuleT ruleT = ruleDao.get(rid);
		// 任务的显示项
		JecnTaskApplicationNew jecnTaskApplicationNew = abstractTaskDao.getJecnTaskApplicationNew(taskBeanNew.getId());
		// 密级 0秘密，1公开
		Long isPublic = ruleT.getIsPublic();
		Long typeId = ruleT.getTypeId();
		String taskFixedDesc = JecnTaskCommon.TASK_NOTHIN;
		// 文控审核和部门审核
		if (taskBeanNew.getUpState() == 1 || taskBeanNew.getUpState() == 2) {
			// 获取审批变动
			taskFixedDesc = assembledPrfTaskFixed(submitMessage, taskBeanNew, jecnTaskApplicationNew, typeId, isPublic);
		}
		if (!JecnCommon.isNullOrEmtryTrim(submitMessage.getStrPublic())) {// 更新密级
			ruleT.setIsPublic(Long.valueOf(submitMessage.getStrPublic().trim()));
			abstractTaskDao.getSession().update(ruleT);
			abstractTaskDao.getSession().flush();
		}
		return taskFixedDesc;
	}

	@Override
	public void createPRFTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * 打开试用行报告
	 * 
	 * @param id
	 *            任务主键ID
	 * @return JecnTaskFileTemporary 试运行文件
	 * @throws Exception
	 */
	@Override
	public JecnTaskFileTemporary downLoadTaskRunFile(Long id) throws Exception {
		return super.downLoadTaskRunFile(id);
	}

	/**
	 * 获取任务信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param taskOperation
	 *            我的任务点击操作 0审批;1重新提交;2整理意见;3任务管理，编辑任务
	 * @return SimpleTaskBean 页面获取的任务相关信息
	 * @throws Exception
	 */
	@Override
	// @Transactional(isolation=Isolation.SERIALIZABLE)
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public SimpleTaskBean getSimpleTaskBeanById(Long taskId, int taskOperation) throws Exception {
		return super.getSimpleTaskBeanById(taskId, taskOperation);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public boolean isApprovePeopleChecked(Long peopleId, Long taskId, int taskElseState, int approveCounts)
			throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 审批通过
	 * 
	 * @param submitMessage
	 *            审核人提交信息
	 * 
	 */

	@Override
	public void taskOperation(SimpleSubmitMessage submitMessage) throws Exception {
		super.taskOperation(submitMessage);
	}

	/**
	 * 当前任务，当前操作人是否已审批任务
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param curPeopleId
	 *            当前操作人主键ID
	 * @return true 当前操作人已审批，或无审批权限
	 * @throws Exception
	 */
	@Override
	public boolean hasApproveTask(Long taskId, Long curPeopleId, int taskOperation) throws Exception {
		return super.hasApproveTask(taskId, curPeopleId, taskOperation);
	}

	/**
	 * 检测任务是否搁置
	 * 
	 * @param simpleTaskBean
	 *            任务信息
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public SimpleTaskBean checkTaskIsDelay(SimpleTaskBean simpleTaskBean) throws Exception {
		return super.checkTaskIsDelay(simpleTaskBean);
	}

	@Override
	public void releaseTaskByWebService(Long taskId, List<JecnTaskHistoryFollow> taskHistorys) throws Exception {
		super.releaseTaskByWebService(taskId, taskHistorys);
	}
	
	@Override
	public MessageResult handleTaskExternal(TaskParam param) {
		return super.handleTaskExternal(param);
	}

}
