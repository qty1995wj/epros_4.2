package epros.designer.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import epros.designer.util.JecnProperties;
import epros.draw.gui.top.frame.JecnFrame;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * @author xiaobo 登录等待显示框 2014-07-08
 */
public class JecnD2Frame extends JFrame {

	/** 提示信息 */
	private String alertText = "";
	/** 滚动条的值 */
	private int processBarValue = 0;

	/** 图片显示面板 */
	private LoginImagePanel mainPanel = new LoginImagePanel();
	/** 图片路径 */
	private ImageIcon logoIm = new ImageIcon(
			"images/icons/startServicesD2.jpg");

	public JecnD2Frame() {
		init();
	}

	/***
	 * 
	 * 初始化
	 * 
	 */
	private void init() {
		this.setSize(logoIm.getIconWidth(), logoIm.getIconHeight());
		this.setResizable(true);
		// 去掉标题及边框
		this.setUndecorated(true);
		// 设置
		this.setIconImage(JecnFileUtil.getFrameBtnIcon("topicom.png")
				.getImage());
		this.setDefaultCloseOperation(JecnFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().add(mainPanel);
	}
	public void updateTipInfo(String alertText, int progress) {
		this.alertText = alertText;
		this.processBarValue = progress;
		this.mainPanel.updateTipInfo();
	}
	
	

	public int getProcessBarValue() {
		return processBarValue;
	}

	public void setVisible(boolean b) {
		if (!b) {
			this.mainPanel.stopTimer();
		}
		super.setVisible(b);
	}

	/**
	 * @desription:将图片添加到面板中,内部类
	 * */
	class LoginImagePanel extends JPanel {
		/**取消按钮背景图片*/
		ImageIcon extButtonImage = new ImageIcon("images/icons/extButton.jpg");
		/** 登录等待框的背景图片 */
		private Icon startFrameBackIcon = new ImageIcon(
				"images/icons/startServicesD2.jpg");
		/** 启动提示label */
		private JLabel showLab = new JLabel();
		/** 取消启动按钮 */
		private JButton extButton = new JButton(JecnProperties.getValue("D2CanCel"));
		private JProgressBar progressBar = new JProgressBar();
		/** 计时器 */
		private Timer timer = null;
		/** 判断字幕提示后面的点数 */
		private int flag = 0;

		LoginImagePanel() {
			init();
			initLayout();
			initEvent();
		}

		private void init() {
			this.setLayout(new BorderLayout());
			// 设置字体大小、粗细
			showLab.setFont(new Font(showLab.getFont().getName(), Font.BOLD,
							14));
			showLab.setPreferredSize(new Dimension(10, 130));
			showLab.setHorizontalAlignment(SwingConstants.LEFT);
			// 设置字体颜色
			showLab.setForeground(new Color(26, 127, 225));
			// 设置按钮大小
			extButton.setPreferredSize(new Dimension(extButtonImage.getIconWidth(),extButtonImage.getIconHeight()));
			extButton.setIcon(extButtonImage);
			//去掉按钮虚线
			extButton.setFocusPainted(false);
			//设置字体垂直居中
			extButton.setHorizontalTextPosition(SwingConstants.CENTER);
			progressBar.setOrientation(JProgressBar.HORIZONTAL);
			progressBar.setPreferredSize(new Dimension(350,13));
			extButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cancel();
				}
			});
		};

		/***
		 * 布局
		 */
		private void initLayout() {

			JPanel panel = new JPanel();
			panel.setOpaque(false);
			panel.setBorder(null);
			panel.setLayout(new GridBagLayout());
			this.add(panel, BorderLayout.SOUTH);
			// 设置提示字幕的位置
			Insets inset = new Insets(0, 10, 19, 0);
			Insets insetextProcessBar = new Insets(30, 10, 0, 0);
			Insets insetextButton = new Insets(0, 0, 10, 10);
			GridBagConstraints c = new GridBagConstraints(0, 0, 0, 0, 1.0, 1.0,
					GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
					inset, 0, 0);
			panel.add(showLab, c);
			// 设置滚动条的位置
			c = new GridBagConstraints(0, 1, 1, 0, 0.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.NONE,
					insetextProcessBar, 0, 0);
			panel.add(progressBar, c);

			// 设置取消启动按钮的位置
			c = new GridBagConstraints(0, 2, 2, 0, 0.0, 0.0,
					GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
					insetextButton, 0, 0);
			panel.add(extButton, c);
		}

		private void initEvent() {
			// 计时器,每隔250毫秒的时候刷新一次
			timer = new Timer(250, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					actionPerformedProcess(e);
				}
			});
			timer.start();
		}

		/**
		 * 停止计时器
		 * */
		private void stopTimer() {
			timer.stop();
		}

		private void updateTipInfo() {
			updateTipInfo(alertText, processBarValue);
		}

		private void updateTipInfo(String text, int progress) {
			if (progress >= 0 && progress <= 100) {
				progressBar.setValue(progress);
			}
			setShowLab(text);
		}

		private void setShowLab(String text) {
			showLab.setText((text == null) ? "" : text);
			showLab.revalidate();
			showLab.repaint();
		}

		/***
		 * 
		 * 追加字符提示后的6个点
		 * 
		 * */
		private void actionPerformedProcess(ActionEvent e) {
			StringBuffer buff = new StringBuffer();
			for (int i = 0; i < flag; i++) {
				buff.append(".");
			}
			setShowLab(alertText + buff.toString());
			if (flag == 6) {
				flag = 0;
			} else {
				flag++;
			}
		}

		/**
		 * 
		 * 绘制图片
		 * 
		 * **/
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2d = (Graphics2D) g;
			// 绘制背景图片
			startFrameBackIcon.paintIcon(this, g2d, 0, 0);
			// 绘制窗体底部线条
			JecnUIUtil.paintFrameBottomLine(this, g2d);
		}
	}

	/**
	 * 
	 * 退出
	 * 
	 * */
	private void cancel() {
		System.exit(0);
	}
	
	
//	public static void main(String args[]) {
//
//		try {
//			// 解决ABC输入法BUG：java Swing 里面的文本框在输入的时候会弹出一个“输入窗口”
//			System.setProperty("java.awt.im.style", "on-the-spot");
//			// 外观
//			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//		} catch (Exception e) {
//		}
//		// 1、 加载资源文件
//		JecnProperties.loadProperties();
//		// 2、 初始化画图面板必须设置
//		JecnInitDraw.initDraw();
//		// // 3、显示主面板
//		JecnD2Frame rd = new JecnD2Frame();
//		rd.setVisible(true);
//
//		// rd.setAlertText("tomcat服务正在启动中");
//
//	}
}
