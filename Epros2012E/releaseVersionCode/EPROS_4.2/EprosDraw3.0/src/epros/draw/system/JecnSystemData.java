package epros.draw.system;

import java.util.List;
import java.util.Map;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.JecnTemplateData;
import epros.draw.gui.swing.JecnToolBar;
import epros.draw.system.data.JecnDefaultFlowElementData;
import epros.draw.system.data.JecnSystemFileIOData;
import epros.draw.system.data.JecnSystemOftenColorData;

/**
 * 
 * 对外接口
 * 
 * 系统启动时需要加载的数据
 * 
 * @author Administrator
 * 
 */
public class JecnSystemData {

	/**
	 * 
	 * 读取保存路径、导出图像路径
	 * 
	 * @return JecnSystemFileIOData
	 */
	public static JecnSystemFileIOData readCurrSystemFileIOData() {
		return JecnSystemFileIOProcess.readSystemFileIOData();
	}

	/**
	 * 
	 * 写入保存路径、导出图像路径
	 * 
	 * @return boolean 更新成功：true 更新失败或无更新：false
	 */
	public static boolean writeCurrSystemFileIOData() {
		return JecnSystemFileIOProcess.writeSystemFileIODataToPropertyFile();
	}

	/**
	 * 
	 * 流程元素本地配置文件内容对象
	 * 
	 * @return List<JecnDefaultFlowElementData>
	 * 
	 */
	public static List<JecnDefaultFlowElementData> readCerrFlowElementDataList() {
		return JecnSystemFlowElementProcess.readCerrFlowElementDataList();
	}

	/**
	 * 
	 * 流程元素属性：写入到本地XML中
	 * 
	 * @return boolean 更新成功：true 更新失败或无更新：false
	 */
	public static boolean writeCurrFlowElementDataList() {
		return JecnSystemFlowElementProcess.writeCurrFlowElementDataList();
	}

	/**
	 * 
	 * 通过流程元素类型获取本地用户自定义的流程元素属性值
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return JecnDefaultFlowElementData 流程元素属性数据对象
	 */
	public static JecnDefaultFlowElementData getCurrFlowElementData(
			MapElemType mapElemType) {
		return JecnSystemFlowElementProcess.getCurrFlowElementData(mapElemType);
	}

	/**
	 * 
	 * 通过流程元素类型获取系统指定的默认元素属性值
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return JecnDefaultFlowElementData 流程元素属性数据对象
	 */
	public static JecnDefaultFlowElementData getSystemDefaultFlowElementData(
			MapElemType mapElemType) {
		return JecnSystemFlowElementProcess
				.getSystemDefaultFlowElementData(mapElemType);
	}

	/**
	 * 
	 * 创建流程元素对象时使用
	 * 
	 * 通过流程元素默认属性值初始化待创建的流程元素属性值
	 * 
	 * 
	 */
	public static void initCurrFlowElementData(
			JecnAbstractFlowElementData flowElementData) {
		JecnSystemFlowElementProcess.initCurrFlowElementData(flowElementData);
	}

	/**
	 * 
	 * 恢复流程元素系统属性时使用
	 * 
	 * 通过流程元素系统默认属性值恢复流程元素属性
	 * 
	 * 
	 */
	public static void initCurrFlowElementDataBySystemData(
			JecnAbstractFlowElementData flowElementData) {
		JecnSystemFlowElementProcess
				.initCurrFlowElementDataBySystemData(flowElementData);
	}

	/**
	 * 
	 * 获取常用颜色
	 * 
	 * @return JecnSystemOftenColorData
	 */
	public static JecnSystemOftenColorData readOftenColorData() {
		return JecnSystemOftenColorProcess.readOftenColorData();
	}

	/**
	 * 
	 * 更新常用颜色
	 * 
	 * @return boolean true:更新成功或无更新；false:更新失败
	 */
	public static boolean exportColorToPropertyFile() {
		return JecnSystemOftenColorProcess.exportColorToPropertyFile();
	}

	/**
	 * 
	 * 流程图：读取显示的不常用流程元素数据
	 * 
	 * @return List<MapElemType>
	 */
	public static List<MapElemType> readPartListByPropertyFile() {
		return JecnSystemToolBoxProcess.readPartListByPropertyFile();
	}

	/**
	 * 
	 * 流程地图：读取显示的不常用流程元素数据
	 * 
	 * @return List<MapElemType>
	 */
	public static List<MapElemType> readTotalListByPropertyFile() {
		return JecnSystemToolBoxProcess.readTotalListByPropertyFile();
	}

	/**
	 * 
	 * 组织图：读取显示的不常用流程元素数据
	 * 
	 * @return List<MapElemType>
	 */
	public static List<MapElemType> readOrgListByPropertyFile() {
		return JecnSystemToolBoxProcess.readOrgListByPropertyFile();
	}

	/**
	 * 
	 * 流程图：写入显示的不常用流程元素数据
	 * 
	 * @return boolean
	 * 
	 */
	public static boolean writePartMapByPropertyFile() {
		return JecnSystemToolBoxProcess.writePartMapByPropertyFile();
	}

	/**
	 * 
	 * 流程地图：写入显示的不常用流程元素数据
	 * 
	 * @return boolean
	 * 
	 */
	public static boolean writeTotalMapToPropertyFile() {
		return JecnSystemToolBoxProcess.writeTotalMapToPropertyFile();
	}

	/**
	 * 
	 * 组织图：写入显示的不常用流程元素数据
	 * 
	 * @return boolean
	 * 
	 */
	public static boolean writeOrgMapToPropertyFile() {
		return JecnSystemToolBoxProcess.writeOrgMapToPropertyFile();
	}

	/**
	 * 模板存储的数据
	 * 
	 * @return
	 */
	public static List<JecnTemplateData> getTemplateDataList() {
		return JecnTemplateDataProcess.getTemplateDataList();
	}

	/**
	 * 获取模板数据
	 * 
	 * @return 模板数据
	 */
	public static JecnTemplateData getTemplateData() {
		return JecnTemplateDataProcess.getTemplateData();
	}

	/**
	 * 设置模板数据
	 * 
	 * @param templateData
	 *            模板数据
	 */
	public static void setTemplateData(JecnTemplateData templateData) {
		JecnTemplateDataProcess.setTemplateData(templateData);
	}

	/**
	 * 获取模板数据对应的图形
	 * 
	 * @return
	 */
	public static Map<JecnTemplateData, JecnToolBar> getTemplateMap() {
		return JecnTemplateDataProcess.getTemplateMap();
	}

	/**
	 * 获取流程地图模板的个数
	 * 
	 * @return 流程地图模板个数
	 */
	public static int getTotalMapCount() {
		return JecnTemplateDataProcess.getTotalMapCount();
	}

	/**
	 * 设置流程地图模板的个数
	 * 
	 * @param totalMapCount
	 *            流程地图模板个数
	 */
	public static void setTotalMapCount(int totalMapCount) {
		JecnTemplateDataProcess.setTotalMapCount(totalMapCount);
	}

	/**
	 * 获取流程图模板个数
	 * 
	 * @return 流程图模板个数
	 */
	public static int getPartMapCount() {
		return JecnTemplateDataProcess.getPartMapCount();
	}

	/**
	 * 设置流程图模板个数
	 * 
	 * @param partMapCount
	 *            流程图模板个数
	 */
	public static void setPartMapCount(int partMapCount) {
		JecnTemplateDataProcess.setPartMapCount(partMapCount);
	}
}
