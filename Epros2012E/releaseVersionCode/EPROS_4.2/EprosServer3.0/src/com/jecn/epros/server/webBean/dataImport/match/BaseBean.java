package com.jecn.epros.server.webBean.dataImport.match;

import java.util.ArrayList;
import java.util.List;

/**
 * 部门 岗位 人员
 * 
 * @author Administrator
 * 
 */
public class BaseBean {
	/** 部门 */
	// private List<DeptBean> deptBeanList = null;
	private List<DeptMatchBean> deptBeanList = null;
	/** 岗位 */
	private List<PosMatchBean> posBeanList = null;
	// /** 人员 */
	// private List<UserBean> userBeanList = new ArrayList<UserBean>();
	/** 原始的人员 */
	private List<UserMatchBean> userPosBeanList = null;

	public BaseBean(List<DeptMatchBean> deptBeanList,
			List<PosMatchBean> posList, List<UserMatchBean> userPosBeanList) {

		// 部门
		if (deptBeanList == null) {
			this.deptBeanList = new ArrayList<DeptMatchBean>();
		} else {
			this.deptBeanList = deptBeanList;
		}
		// 岗位
		if (posList == null) {
			this.posBeanList = new ArrayList<PosMatchBean>();
		} else {
			this.posBeanList = posList;
		}
		// 人员
		if (userPosBeanList == null) {
			this.userPosBeanList = new ArrayList<UserMatchBean>();
		} else {
			this.userPosBeanList = userPosBeanList;
		}
	}

	// /**
	// *
	// * 分解人员和岗位数据
	// *
	// * @param userPosSheet
	// */
	// public void resolveUserPos() {
	//
	// if (userPosBeanList == null) {
	// return;
	// }
	//
	// // 记录已经存储过的岗位
	// Set<String> posNumSet = new HashSet<String>();
	// // 记录已经存储过的人员
	// List<UserBean> userBeanSet = new ArrayList<UserBean>();
	//
	// for (int i = 0; i < userPosBeanList.size(); i++) {
	// // 获取原始人员+岗位数据
	// UserBean userPosBean = userPosBeanList.get(i);
	// // 岗位
	// PosBean posBean = new PosBean();
	// // 人员
	// UserBean userBean = new UserBean();
	//
	// // 任职基准岗位编号
	// String basePosNum = userPosBean.getPosNum();
	// // 登录名称
	// String loginName = userPosBean.getLoginName();
	//
	// // 任职岗位编号
	// posBean.setBasePosNum(Tool.emtryToNull(basePosNum));
	// // // 任职岗位名称
	// // posBean.setPosName(Tool.emtryToNull(userPosBean.getPosName()));
	// // // 岗位所属部门编号
	// // posBean.setDeptNum(Tool.emtryToNull(userPosBean.getDeptNum()));
	// //存储拆分前的数据，为了记录错误信息
	// posBean.setUserPosBean(userPosBean);
	//
	// // 登录名称
	// userBean.setLoginName(Tool.emtryToNull(loginName));
	// // 真实姓名
	// userBean.setTrueName(Tool.emtryToNull(userPosBean.getTrueName()));
	// // 人员所在岗位编号
	// userBean.setPosNum(Tool.emtryToNull(basePosNum));
	// // 邮箱地址
	// userBean.setEmail(Tool.emtryToNull(userPosBean.getEmail()));
	//
	// // 邮箱、邮箱类型不等于空且是“0”，“1”
	// if (userPosBean.checkEmailType()) {
	// userBean.setEmailType(Integer.valueOf(userPosBean
	// .getEmailType()));
	// }
	// // 联系电话
	// userBean.setPhone(Tool.emtryToNull(userPosBean.getPhone()));
	// //存储拆分前的数据，为了记录错误信息
	// userBean.setUserPosBean(userPosBean);
	//			
	// // 判断不用存储标识
	// boolean flag = true;
	// // 岗位
	// // 不为空且已经存储过了就不用存储
	// if (!Tool.isNullOrEmtryTrim(basePosNum)) {
	// if (posNumSet.contains(basePosNum.trim())) {
	// flag = false;
	// }
	// } else {
	// flag = false;
	// }
	//
	// if (flag) {
	// posBeanList.add(posBean);
	// posNumSet.add(basePosNum);
	// }
	//
	// // 前提条件：不能出现两条以上岗位编号为空的数据
	// // 人员编号相同，岗位也相同情况不用添加
	// if (isUserBeanAddDB(userBeanSet, userBean)) {
	// userBeanList.add(userBean);
	// userBeanSet.add(userBean);
	// }
	// }
	// }

	/**
	 * 
	 * @return 部门、岗位以及人员集合其中一个为空返回true，否则返回false
	 */
	public boolean isNull() {
		return (deptBeanList == null || posBeanList == null || userPosBeanList == null) ? true
				: false;
	}

	/**
	 * 
	 * 判断岗位的所属部门编号是否在部门集合中存在
	 * 
	 * @param posBean
	 * @return boolean 存在返回true，不存在返回false
	 * 
	 */
	public boolean existsPosByDept(PosMatchBean posBean) {
		boolean ret = false;

		if (!isNull()) {
			for (DeptMatchBean deptBean : deptBeanList) {
				if (posBean.getDeptNum().equals(deptBean.getDeptNum())) {
					ret = true;
					break;
				}
			}
		}
		return ret;
	}

	/**
	 * 
	 * 判断人员的对应岗位编号是否在岗位集合中存在
	 * 
	 * @param userBean
	 * @return
	 */
	public boolean existsUserByPos(UserMatchBean userBean) {
		boolean ret = false;
		if (!isNull()) {
			for (PosMatchBean posBean : posBeanList) {
				if (userBean.getPosNum().equals(posBean.getActPosNum())) { // 人员中的岗位编码与岗位中的实际编码做对应
					ret = true;
				}
			}
		}
		return ret;
	}

	public List<DeptMatchBean> getDeptBeanList() {
		return deptBeanList;
	}

	public void setDeptBeanList(List<DeptMatchBean> deptBeanList) {
		this.deptBeanList = deptBeanList;
	}

	public List<PosMatchBean> getPosBeanList() {
		return posBeanList;
	}

	public void setPosBeanList(List<PosMatchBean> posBeanList) {
		this.posBeanList = posBeanList;
	}

	// public List<UserBean> getUserBeanList() {
	// return userBeanList;
	// }
	//
	// public void setUserBeanList(List<UserBean> userBeanList) {
	// this.userBeanList = userBeanList;
	// }

	public List<UserMatchBean> getUserPosBeanList() {
		return userPosBeanList;
	}

	public void setUserPosBeanList(List<UserMatchBean> userPosBeanList) {
		this.userPosBeanList = userPosBeanList;
	}

}
