package com.jecn.epros.server.action.web.login.ad.zhongdiantou;

import weaver.util.DES;

import com.jecn.epros.server.util.JecnPath;

/**
 * 
 * 中电投单点登录所需配置信息，在系统启动时需要启动项
 * 
 * @author ZHOUXY
 * 
 */
public class JecnZhongDTAfterItem {
	/** DES对象 */
	public static DES des = null;

	/**
	 * 
	 * 中电投单点登录 
	 * 读取本地文件数据，创建解密DES对象（DES对象的key=本地文件数据）
	 * 
	 */
	public static DES start() {
		if (des == null) {
			String url = JecnPath.CLASS_PATH
					+ "cfgFile/zhongdiantou/deskey.key";
			des = new DES();// 实例化一个对像
			des.getKey(url);// 生成密匙
		}
		return des;
	}
}
