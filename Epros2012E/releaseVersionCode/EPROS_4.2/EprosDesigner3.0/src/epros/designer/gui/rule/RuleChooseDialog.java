package epros.designer.gui.rule;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class RuleChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(RuleChooseDialog.class);
	/**选择框，搜索制度时，判断搜索的是否包含制度目录 0L：包含，1L不包含*/
	private Long serarchType= 0L;
	public RuleChooseDialog( List<JecnTreeBean> list) {
		super(list,5);
		this.setTitle(JecnProperties.getValue("ruleChoose"));
	}
	public RuleChooseDialog( List<JecnTreeBean> list,Long serarchType) {
		super(list,5);
		this.serarchType = serarchType;
		this.setTitle(JecnProperties.getValue("ruleChoose"));
	}
	public RuleChooseDialog( List<JecnTreeBean> list,JecnDialog jecnDialog) {
		super(list,5,jecnDialog);
		this.setTitle(JecnProperties.getValue("ruleChoose"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			List<JecnTreeBean> listJTree = null;
//			if(serarchType == 0L){
//				listJTree = ConnectionPool.getRuleAction().searchByName(name, JecnConstants.projectId);
//			}if(serarchType == 1L){
				listJTree = ConnectionPool.getRuleAction().searchISNotRuleDirByName(name, JecnConstants.projectId);
//			}
			return listJTree;
		} catch (Exception e) {
			log.error("RuleChooseDialog searchByName is error",e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("ruleNameC");//制度名称：
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyRuleTree();
//		if (JecnConstants.isMysql) {
//			return new RoutineRuleTree();
//		} else {
//			return new HighEfficiencyRuleTree();
//		}
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title=new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("ruleName"));//制度名称：
		return title;
	}

}
