package epros.draw.gui.top.toolbar;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.ManhattanLine;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.button.JecnColorSelectButton;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.gui.top.button.JecnToolbarGroupButton;
import epros.draw.gui.top.toolbar.element.JecnFlowElementUtil;
import epros.draw.gui.top.toolbar.menu.JecnAlignmentPopupMenu;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏大类：编辑类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnEidtToolBarItemPanel extends JecnAstractToolBarItemPanel implements ActionListener {

	/** 剪切 */
	private JecnToolbarContentButton cutBtn = null;
	/** 复制 */
	private JecnToolbarContentButton copyBtn = null;
	/** 粘贴 */
	private JecnToolbarContentButton pasteBtn = null;
	/** 删除 */
	private JecnToolbarContentButton deleteBtn = null;
	/** 格式刷 */
	private JecnToolbarContentButton formatPainterBtn = null;
	// //////////////////////字体////////////////////////
	/** 字体：字体名称 默认宋体 */
	private JComboBox fontNameComboBox = null;

	/** 字体：字体大小 ：5~60 流程图：12 流程地图：16 */
	private JComboBox fontSizeComboBox = null;
	/** 字体：字体大小 ：放大按钮 */
	private JecnToolbarContentButton fontSizeBigBtn = null;
	/** 字体：字体大小 ：缩小按钮 */
	private JecnToolbarContentButton fontSizeSmallBtn = null;

	/** 字体：字体颜色容器按钮 */
	private JecnToolbarContentButton fontColorBaseBtn = null;
	/** 字体：字体颜色 默认黑色 */
	private JecnColorSelectButton fontColorBtn = null;
	/** 字体：字体颜色选择按钮 */
	private JecnToolbarContentButton fontColorSelectBtn = null;

	/**
	 * 字体：字体位置：顶端居左、顶端居中、顶端居右、中间居左、中间居中、中间居右、底端居左、底端居中、底端居右 默认 中间居中
	 * 其中PA、KSF、KCP顶端居中 泳池：中间居左
	 */
	private JComboBox fontLocationComboBox = null;
	/** 字体：字体粗细：正常、加粗 默认正常 */
	private JecnToolbarContentButton fontFormBtn = null;
	// /** 字体：字排列方式 横排、竖排 */
	// private JecnToolbarContentButton fontAlignBtn = null;
	// //////////////////////字体////////////////////////

	// //////////////////////样式 start////////////////////////
	/** 样式：填充颜色容器 */
	private JecnToolbarContentButton styleFillColorBaseBtn = null;
	/** 样式：填充颜色 默认根据不同图形指定，线默认颜色黑色 （只针对图形） */
	private JecnColorSelectButton styleFillColorBtn = null;
	/** 样式：填充颜色选择按钮 （只针对图形） */
	private JecnToolbarContentButton styleFillColorSelectBtn = null;

	/** 样式：图形边框或线条颜色的容器 */
	private JecnToolbarContentButton styleLineColorBaseBtn = null;
	/** 样式：图形边框或线条颜色 */
	private JecnColorSelectButton styleLineColorBtn = null;
	/** 样式：图形边框或线条颜色选择按钮 */
	private JecnToolbarContentButton styleLineColorSelectBtn = null;

	/** 样式：图形边框或线条阴影容器 */
	private JecnToolbarContentButton styleShadowsBaseBtn = null;
	/** 样式：图形边框或线条阴影：有、无 默认无 */
	private JecnColorSelectButton styleShadowsBtn = null;
	/** 样式：图形边框或线条阴影选择按钮 */
	private JecnToolbarContentButton styleShadowsSelectedBtn = null;

	/** 样式：3D效果：有、无 默认无 */
	private JecnToolbarContentButton style3DBtn = null;

	/** 样式：线条类型标签 */
	private JLabel styleTypeLabel = null;
	/** 样式：线条类型：实线、虚线、点划线、双线 默认实线 */
	private JComboBox styleTypeComboBox = null;

	/** 样式：线条粗细标签 */
	private JLabel styleFormLabel = null;
	/** 样式：线条粗细：1~10 默认1 */
	private JComboBox bodyWidthComboBox = null;

	/** 双箭头连接线 */
	private JCheckBox twoArrowCheckBox = null;

	/** 隐藏编辑点 */
	private JCheckBox lineEditHiddenCheckBox = null;

	// //////////////////////样式 end////////////////////////

	/** 置于顶层 */
	private JecnToolbarContentButton frontLayerBtn = null;
	/** 置于底层 */
	private JecnToolbarContentButton backLayerBtn = null;
	/** 上移一层 */
	private JecnToolbarContentButton upLayerBtn = null;
	/** 下移一层 */
	private JecnToolbarContentButton downLayerBtn = null;

	/** 逆时针方向旋转 */
	private JecnToolbarContentButton clockWiseBtn = null;
	/** 顺时针方向旋转 */
	private JecnToolbarContentButton antiClockWiseBtn = null;

	/** 剪切、复制、粘贴、删除所在面板 */
	private JecnPanel pasteboardPanel = null;
	/** 字体所在面板 */
	private JecnPanel fontMainPanel = null;
	/** 样式所在面板 */
	private JecnPanel styleMainPanel = null;
	/** 层级所在面板 */
	private JecnPanel layerPanel = null;
	/** 旋转所在面板 */
	private JecnPanel rotationPanel = null;

	/** 排列：自动对齐、左对齐、水平居中、右对齐、顶端对齐、垂直居中、顶端对齐 */
	private JComboBox alignComboBox = null;
	private JecnPanel alignPanel = null;

	private JecnToolbarContentButton alignBut = null;

	public JecnEidtToolBarItemPanel(JecnToolBarPanel toolBarPanel) {
		super(toolBarPanel);
		initchildComponents();
	}

	@Override
	protected void initTitleType() {
		this.titleType = ToolBarElemType.editTitle;
	}

	/**
	 * 
	 * 
	 * 子组件实例化
	 * 
	 */
	@Override
	protected void createChildComponents() {
		JecnResourceUtil jecnResourceUtil = JecnResourceUtil.getJecnResourceUtil(); // 加载国际化
		// 剪切
		cutBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.editCut),
				ToolBarElemType.editCut);
		// 复制
		copyBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil()
				.getValue(ToolBarElemType.editCopy), ToolBarElemType.editCopy);
		// 粘贴
		pasteBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editPaste), ToolBarElemType.editPaste);
		// 删除
		deleteBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editDelete), ToolBarElemType.editDelete);
		// 格式刷
		formatPainterBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil()
				.getValue("formatPainter"), ToolBarElemType.formatPainter);

		// //////////////////////字体////////////////////////
		// 字体：字体名称 默认宋体
		fontNameComboBox = JecnFlowElementUtil.getfontNameComboBox();
		fontNameComboBox.addActionListener(this);
		fontNameComboBox.setActionCommand("fontType");
		// 字体：字体大小 ：5~60 流程图：12 流程地图：16
		fontSizeComboBox = JecnFlowElementUtil.getFontSizeComboBox();
		fontSizeComboBox.addActionListener(this);
		fontSizeComboBox.setActionCommand("fontSize");
		// 字体：字体大小 ：放大按钮
		fontSizeBigBtn = new JecnToolbarContentButton(null, ToolBarElemType.editFontSizeBig);
		fontSizeBigBtn.addActionListener(this);
		fontSizeBigBtn.setActionCommand("fontSizeBig");
		// 字体：字体大小 ：缩小按钮
		fontSizeSmallBtn = new JecnToolbarContentButton(null, ToolBarElemType.editfontSizeSmall);
		fontSizeSmallBtn.addActionListener(this);
		fontSizeSmallBtn.setActionCommand("fontSizeSmall");

		// 字体：字体颜色 默认黑色
		fontColorBtn = new JecnColorSelectButton(JecnResourceUtil.getJecnResourceUtil().getValue("editFontTitle"),
				ToolBarElemType.editfontOftenColor);
		// 字体：字体颜色选择按钮
		fontColorSelectBtn = new JecnToolbarContentButton(null, ToolBarElemType.editfontSelectColor);

		// 字体颜色容器
		fontColorBaseBtn = new JecnToolbarGroupButton(fontColorBtn, fontColorSelectBtn);

		// 字体颜色

		// 字体：字体位置：顶端居左、顶端居中、顶端居右、中间居左、中间居中、中间居右、底端居左、底端居中、底端居右 默认 中间居中
		// 其中PA、KSF、KCP顶端居中 泳池：中间居左
		fontLocationComboBox = JecnFlowElementUtil.getFontPostionComboBox();
		fontLocationComboBox.addActionListener(this);
		fontLocationComboBox.setActionCommand("fontPotision");
		// 字体：字体粗细：正常、加粗 默认正常
		fontFormBtn = new JecnToolbarContentButton(null, ToolBarElemType.editfontForm);
		fontFormBtn.addActionListener(this);
		fontFormBtn.setActionCommand("fontStyleRadio");
		// // 字体：字排列方式 横排、竖排
		// fontAlignBtn = new JecnToolbarContentButton(null,
		// ToolBarElemType.editFontAlign);
		// fontAlignBtn.addActionListener(this);
		// fontAlignBtn.setActionCommand("upRightRadio");
		// //////////////////////字体////////////////////////

		// //////////////////////样式 start////////////////////////
		// 样式：填充颜色 默认根据不同图形指定，线默认颜色黑色 （只针对图形）
		styleFillColorBtn = new JecnColorSelectButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editFill), ToolBarElemType.editFill);
		// 样式：填充颜色选择按钮 （只针对图形）
		styleFillColorSelectBtn = new JecnToolbarContentButton(null, ToolBarElemType.editFillSelectColor);
		// 填充颜色容器
		styleFillColorBaseBtn = new JecnToolbarGroupButton(styleFillColorBtn, styleFillColorSelectBtn);

		// 样式：图形边框或线条颜色
		styleLineColorBtn = new JecnColorSelectButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editLine), ToolBarElemType.editLine);
		// 样式：图形边框或线条颜色选择按钮
		styleLineColorSelectBtn = new JecnToolbarContentButton(null, ToolBarElemType.editLineSelectColor);
		// 图形边框或线条颜色容器
		styleLineColorBaseBtn = new JecnToolbarGroupButton(styleLineColorBtn, styleLineColorSelectBtn);
		styleLineColorBaseBtn.setSize(500, 200);
		// 样式：图形边框或线条阴影：有、无 默认无
		styleShadowsBtn = new JecnColorSelectButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editShadow), ToolBarElemType.editShadow);
		// 样式：图形边框或线条阴影选择按钮
		styleShadowsSelectedBtn = new JecnToolbarContentButton(null, ToolBarElemType.editShadowSelectColor);
		// 图形边框或线条阴影容器
		styleShadowsBaseBtn = new JecnToolbarGroupButton(styleShadowsBtn, styleShadowsSelectedBtn);

		// 样式：3D效果：有、无 默认无
		style3DBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.edit3D), ToolBarElemType.edit3D);

		// 样式：线条类型标签
		styleTypeLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.editLineType));

		// 样式：线条类型：实线、虚线、点划线、双线 默认实线
		styleTypeComboBox = JecnFlowElementUtil.getBodyStyleComboBox();
		styleTypeComboBox.addActionListener(this);
		styleTypeComboBox.setActionCommand("lineType");
		// 样式：线条粗细标签
		styleFormLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.editLineForm));
		// 样式：线条粗细：1~10 默认1
		bodyWidthComboBox = new JComboBox(JecnFlowElementUtil.getBodyWidthArray());
		bodyWidthComboBox.addActionListener(this);
		bodyWidthComboBox.setActionCommand("lineSize");

		twoArrowCheckBox = new JCheckBox(jecnResourceUtil.getValue("doubleArrow"));
		twoArrowCheckBox.setOpaque(false);
		twoArrowCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				itemStateAction();
			}
		});
		twoArrowCheckBox.setBackground(this.getBackground());
		twoArrowCheckBox.setActionCommand("twoArrowLine");

		// 连接线编辑点
		lineEditHiddenCheckBox = new JCheckBox(jecnResourceUtil.getValue("hideEditPoints"));
		lineEditHiddenCheckBox.setOpaque(false);
		lineEditHiddenCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				lineEditCheckBoxAction();
			}
		});
		lineEditHiddenCheckBox.setBackground(this.getBackground());
		lineEditHiddenCheckBox.setActionCommand("lineEditBox");
		// //////////////////////样式 end////////////////////////

		// 置于顶层
		frontLayerBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editFrontLayer), ToolBarElemType.editFrontLayer);
		// 置于底层
		backLayerBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editBackLayer), ToolBarElemType.editBackLayer);
		// 上移一层
		upLayerBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editUpLayer), ToolBarElemType.editUpLayer);
		// 下移一层
		downLayerBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editDownLayer), ToolBarElemType.editDownLayer);

		// 顺时针方向旋转
		antiClockWiseBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editClockWize), ToolBarElemType.editClockWize);
		// 逆时针方向旋转
		clockWiseBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.editAntiClockWize), ToolBarElemType.editAntiClockWize);

		alignBut = new JecnToolbarContentButton(jecnResourceUtil.getValue("array"), ToolBarElemType.editAlign);
		alignBut.addActionListener(this);
		alignBut.setActionCommand(ToolBarElemType.editAlign.toString());

		// 剪切、复制、粘贴、删除所在面板
		pasteboardPanel = new JecnPanel();
		// 字体所在面板
		fontMainPanel = new JecnPanel();
		// 样式所在面板
		styleMainPanel = new JecnPanel();
		// 层级所在面板
		layerPanel = new JecnPanel();
		// 旋转所在面板
		rotationPanel = new JecnPanel();
		// 排列
		alignPanel = new JecnPanel();
		// 边框
		// 剪切、复制、粘贴、删除所在面板
		pasteboardPanel.setBorder(JecnUIUtil.getTootBarBorder());
		// 字体所在面板
		fontMainPanel.setBorder(JecnUIUtil.getTootBarBorder());
		// 样式所在面板
		styleMainPanel.setBorder(JecnUIUtil.getTootBarBorder());
		// 层级所在面板
		layerPanel.setBorder(JecnUIUtil.getTootBarBorder());
		// 旋转所在面板
		rotationPanel.setBorder(JecnUIUtil.getTootBarBorder());
		alignPanel.setBorder(JecnUIUtil.getTootBarBorder());

		// 字体名称：默认宋体
		fontNameComboBox.setSelectedIndex(JecnFlowElementUtil.getFontNameDefaultIndex());
		Dimension fontNameComboBoxSize = new Dimension(75, 20);
		fontNameComboBox.setPreferredSize(fontNameComboBoxSize);
		fontNameComboBox.setMinimumSize(fontNameComboBoxSize);

		// 字体：字体大小 ：5~60 流程图：12 流程地图：16
		fontSizeComboBox.setSelectedIndex(JecnFlowElementUtil.getFontSizeDefaultIndex());
		Dimension fontSizeComboBoxSize = new Dimension(40, 20);
		fontSizeComboBox.setPreferredSize(fontSizeComboBoxSize);
		fontSizeComboBox.setMinimumSize(fontSizeComboBoxSize);

		// 字体位置：默认居中
		fontLocationComboBox.setSelectedIndex(JecnFlowElementUtil.getFontPostionDefaultIndex());

		// 样式：线条类型：实线、虚线、点划线、双线 默认实线
		styleTypeComboBox.setSelectedIndex(JecnFlowElementUtil.getBodyStyleDefaultIndex());
		Dimension styleTypeComboBoxSize = new Dimension(60, 20);
		styleTypeComboBox.setPreferredSize(styleTypeComboBoxSize);
		styleTypeComboBox.setMinimumSize(styleTypeComboBoxSize);

		// 样式：线条粗细：1~10 默认1
		bodyWidthComboBox.setPreferredSize(styleTypeComboBoxSize);
		bodyWidthComboBox.setMinimumSize(styleTypeComboBoxSize);

		// 提示信息
		// 剪切 Ctrl+X
		toolBarPanel.setToolBarBtnToolTipText(cutBtn, "Ctrl+X", "");
		// 复制 Ctrl+C
		toolBarPanel.setToolBarBtnToolTipText(copyBtn, "Ctrl+C", "");
		// 粘贴 Ctrl+V
		toolBarPanel.setToolBarBtnToolTipText(pasteBtn, "Ctrl+V", "");
		// 删除 Delete
		toolBarPanel.setToolBarBtnToolTipText(deleteBtn, "Delete", "");

		alignComboBox = JecnFlowElementUtil.getBodyStyleComboBox();
		alignComboBox.addActionListener(this);
		alignComboBox.setActionCommand("alignment");
		this.setBorder(null);
	}

	protected void lineEditCheckBoxAction() {
		getWorkflow().getFlowMapData().setHiddenLineEditPoint(lineEditHiddenCheckBox.isSelected());
		JecnSystemStaticData.setHiddenLineEditPoint(lineEditHiddenCheckBox.isSelected());
	}

	/**
	 * 
	 * 子组件布局
	 * 
	 */
	@Override
	protected void layoutChildComponents() {
		// 按钮组面板
		initBasePanel();
		// 剪切、复制、粘贴、删除所在面板下子组件
		initPasteboardPanel();
		// 字体所在面板下子组件
		initFontMainPanel();
		// 布局样式所在面板下子组件
		initStyleMainPanel();
		// 层级所在面板下子组件
		initLayerPanel();
		// 旋转所在面板下子组件
		initRotationPanel();
	}

	/**
	 * 
	 * 布局按钮组面板
	 * 
	 */
	private void initBasePanel() {

		// 剪切、复制、粘贴、删除所在面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets0(), 0, 0);
		this.add(pasteboardPanel, c);

		if (!JecnDesignerProcess.getDesignerProcess().isHiddenEditToolBarAttrs()) {
			// 字体所在面板
			c = new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets3(), 0, 0);
			this.add(fontMainPanel, c);

			// 样式所在面板
			c = new GridBagConstraints(2, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
					JecnUIUtil.getInsets3(), 0, 0);
			this.add(styleMainPanel, c);
		}

		// 层级所在面板
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets3(), 0, 0);
		this.add(layerPanel, c);

		// 旋转所在面板
		c = new GridBagConstraints(4, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets3(), 0, 0);
		this.add(rotationPanel, c);

		// 旋转所在面板
		c = new GridBagConstraints(5, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets3(), 0, 0);
		this.add(alignPanel, c);
	}

	/**
	 * 
	 * 剪切、复制、粘贴、删除所在面板下子组件
	 * 
	 */
	private void initPasteboardPanel() {
		// 剪切
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0(), 0, 0);
		pasteboardPanel.add(cutBtn.getJToolBar(), c);

		// 复制
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		pasteboardPanel.add(copyBtn.getJToolBar(), c);

		// 粘贴
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		pasteboardPanel.add(pasteBtn.getJToolBar(), c);

		// 删除
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		pasteboardPanel.add(deleteBtn.getJToolBar(), c);

		// 格式刷
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		pasteboardPanel.add(formatPainterBtn.getJToolBar(), c);

		// 空闲区域
		c = new GridBagConstraints(2, 2, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		pasteboardPanel.add(new JLabel(), c);
	}

	/**
	 * 
	 * 布局字体所有子组件
	 * 
	 */
	private void initFontMainPanel() {
		// 上半面板
		JecnPanel topFontPanel = initTopFontMainPanel();
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, JecnUIUtil.getInsets0(), 0, 0);
		fontMainPanel.add(topFontPanel, c);
		// 下半面板
		JecnPanel bottomFontPanel = initButtonFontMainPanel();
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		fontMainPanel.add(bottomFontPanel, c);
		// 空闲区域
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		fontMainPanel.add(new JLabel(), c);
	}

	/**
	 * 
	 * 字体面板的上半面板布局
	 * 
	 * @return
	 */
	private JecnPanel initTopFontMainPanel() {
		// 上半面板
		JecnPanel topFontPanel = new JecnPanel();

		// 字体名称 默认宋体
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, JecnUIUtil.getInsets0(), 0, 0);
		topFontPanel.add(fontNameComboBox, c);

		// 字体大小 ：5~60 流程图：12 流程地图：16
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		topFontPanel.add(fontSizeComboBox, c);

		// 放大字体按钮
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		topFontPanel.add(fontSizeBigBtn.getJToolBar(), c);

		// 缩小字体按钮
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		topFontPanel.add(fontSizeSmallBtn.getJToolBar(), c);

		return topFontPanel;
	}

	/**
	 * 
	 * 字体面板的下半面板布局
	 * 
	 * @return
	 */
	private JecnPanel initButtonFontMainPanel() {
		// 下半面板
		JecnPanel bottomFontPanel = new JecnPanel();

		// 字体位置
		GridBagConstraints c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0(), 0, 0);
		bottomFontPanel.add(fontLocationComboBox, c);
		// 字体粗细
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets3(), 0, 0);
		bottomFontPanel.add(fontFormBtn.getJToolBar(), c);

		// 字体颜色 默认黑色
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		bottomFontPanel.add(fontColorBaseBtn.getJToolBar(), c);

		// // 字排列方式
		// c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
		// .getInsets0(), 0, 0);
		// bottomFontPanel.add(fontAlignBtn.getJToolBar(), c);

		return bottomFontPanel;
	}

	/**
	 * 
	 * 布局样式所在面板子组件
	 * 
	 */
	private void initStyleMainPanel() {

		// 填充
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0(), 0, 0);
		styleMainPanel.add(styleFillColorBaseBtn.getJToolBar(), c);

		// 线条：图形边框或线条颜色
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		styleMainPanel.add(styleLineColorBaseBtn.getJToolBar(), c);

		// 线条类型
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		styleMainPanel.add(styleTypeLabel, c);
		// 线条类型
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets3(), 0, 0);
		styleMainPanel.add(styleTypeComboBox, c);

		// //////////////////////////

		// 阴影：图形边框或线条阴影
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		styleMainPanel.add(styleShadowsBaseBtn.getJToolBar(), c);

		// 3D效果
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		styleMainPanel.add(style3DBtn.getJToolBar(), c);

		// 线条粗细标签
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		styleMainPanel.add(styleFormLabel, c);

		// 线条粗细
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets3(), 0, 0);
		styleMainPanel.add(bodyWidthComboBox, c);

		// 显示双箭头
		c = new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		styleMainPanel.add(twoArrowCheckBox, c);

		c = new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		styleMainPanel.add(alignBut, c);

		// 隐藏编辑点
		c = new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		styleMainPanel.add(lineEditHiddenCheckBox, c);

		// 空闲区域
		c = new GridBagConstraints(0, 2, 4, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		styleMainPanel.add(new JLabel(), c);
	}

	/**
	 * 
	 * 层级所在面板下子组件
	 * 
	 */
	private void initLayerPanel() {
		// 上移一层
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0(), 0, 0);
		layerPanel.add(upLayerBtn.getJToolBar(), c);

		// 下移一层
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		layerPanel.add(downLayerBtn.getJToolBar(), c);

		// 置于顶层
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		layerPanel.add(frontLayerBtn.getJToolBar(), c);

		// 置于底层
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		layerPanel.add(backLayerBtn.getJToolBar(), c);

		// 空闲区域
		c = new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		layerPanel.add(new JLabel(), c);

	}

	/**
	 * 获取面板
	 * 
	 * @return 面板
	 */
	private JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	/**
	 * 
	 * 获取画图面板上所有流程元素集合
	 * 
	 * @return getAllFigureList 流程元素集合
	 */
	private List<JecnBaseFlowElementPanel> getCurrentSelectElementList() {
		if (getWorkflow() != null) {
			return getWorkflow().getCurrentSelectElement();
		}
		return null;
	}

	/**
	 * 
	 * 旋转所在面板下子组件
	 * 
	 */
	private void initRotationPanel() {
		// 逆时针方向旋转
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0(), 0, 0);
		rotationPanel.add(clockWiseBtn.getJToolBar(), c);

		// 顺时针方向旋转
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		rotationPanel.add(antiClockWiseBtn.getJToolBar(), c);

		// 空闲区域
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		rotationPanel.add(new JLabel(), c);
	}

	private void itemStateAction() {
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		if (getWorkflow() == null) {
			return;
		}
		List<JecnBaseFlowElementPanel> elementPanels = getWorkflow().getCurrentSelectElement();
		if (elementPanels.size() == 0) {
			return;
		}
		undoData.recodeFlowElement(elementPanels);
		for (JecnBaseFlowElementPanel jecnBaseFlowElementPanel : elementPanels) {
			if (jecnBaseFlowElementPanel instanceof ManhattanLine) {
				ManhattanLine line = (ManhattanLine) jecnBaseFlowElementPanel;
				line.getFlowElementData().setTwoArrow(twoArrowCheckBox.isSelected());
				line.reDrawArrow();
			}
		}
		// 记录操作后数据
		redoData.recodeFlowElement(elementPanels);

		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);

		getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
		// 刷新面板
		getWorkflow().repaint();
	}

	public void actionPerformed(ActionEvent e) {
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 创建记录撤销恢复的流程元素数据的对象
		JecnUndoRedoData redoData = new JecnUndoRedoData();

		// 元素属性是否有变更
		boolean isChange = false;
		// 判断点击的是否为JComboBox
		if (e.getSource() instanceof JComboBox) {
			if (getCurrentSelectElementList() == null) {
				return;
			}
			JComboBox comboBox = (JComboBox) e.getSource();
			String comboStr = comboBox.getActionCommand();
			if ("fontType".equals(comboStr)) { // 字体类型
				for (JecnBaseFlowElementPanel baseFlowElementPanel : getCurrentSelectElementList()) {
					String fontType = String.valueOf(comboBox.getSelectedItem());
					if (!baseFlowElementPanel.getFlowElementData().getFontName().equals(fontType)) {
						// 记录操作前数据
						undoData.recodeFlowElement(baseFlowElementPanel);
						JecnElementAttributesUtil.updateFontType(baseFlowElementPanel, fontType);
						// 记录操作后数据
						redoData.recodeFlowElement(baseFlowElementPanel);
						isChange = true;
					}
				}
			} else if ("fontSize".equals(comboStr)) { // 字体大小
				for (JecnBaseFlowElementPanel baseFlowElementPanel : getCurrentSelectElementList()) {
					int fontSize = Integer.valueOf(comboBox.getSelectedItem().toString()).intValue();
					if (baseFlowElementPanel.getFlowElementData().getFontSize() != fontSize) {
						// 记录操作前数据
						undoData.recodeFlowElement(baseFlowElementPanel);
						JecnElementAttributesUtil.updateFontSize(baseFlowElementPanel, fontSize);
						// 记录操作后数据
						redoData.recodeFlowElement(baseFlowElementPanel);
						isChange = true;
					}

				}
			} else if ("lineType".equals(comboStr)) {
				for (JecnBaseFlowElementPanel baseFlowElementPanel : getCurrentSelectElementList()) {
					int index = comboBox.getSelectedIndex();
					if (index < 0 || index >= comboBox.getItemCount()) {
						return;
					}
					if (baseFlowElementPanel.getFlowElementData().getBodyStyle() != index) {
						// 记录操作前数据
						undoData.recodeFlowElement(baseFlowElementPanel);
						// 修改线条类型
						JecnElementAttributesUtil.updateLineType(baseFlowElementPanel, index);
						// 记录操作后数据
						redoData.recodeFlowElement(baseFlowElementPanel);
						isChange = true;
					}
				}
			} else if ("lineSize".equals(comboStr)) { // 线条粗细
				for (JecnBaseFlowElementPanel baseFlowElementPanel : getCurrentSelectElementList()) {
					int index = comboBox.getSelectedIndex();
					if (index < 0 || index >= JecnFlowElementUtil.getBodyWidthArray().length) {
						return;
					}
					int valueInt = Integer.valueOf(JecnFlowElementUtil.getBodyWidthArray()[index].toString())
							.intValue();
					if (baseFlowElementPanel.getFlowElementData().getBodyWidth() != valueInt) {
						// 记录操作前数据
						undoData.recodeFlowElement(baseFlowElementPanel);
						// 修改线条粗细
						JecnElementAttributesUtil.updateLineWidth(baseFlowElementPanel, valueInt);
						// 记录操作后数据
						redoData.recodeFlowElement(baseFlowElementPanel);
						isChange = true;
						if (baseFlowElementPanel instanceof JecnBaseManhattanLinePanel) {
							JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) baseFlowElementPanel;
							// 当前面板下执行画线（放大缩小、改变线属性算法） 小线段数据层不变
							linePanel.paintZoomLine();
						} else if (baseFlowElementPanel instanceof HDividingLine
								|| baseFlowElementPanel instanceof VDividingLine) {
							JecnBaseDividingLinePanel dividingLinePanel = (JecnBaseDividingLinePanel) baseFlowElementPanel;
							// 设置横竖分割线位置点
							dividingLinePanel.setBounds(dividingLinePanel.getStartPoint(), dividingLinePanel
									.getEndPoint());
						} else if (baseFlowElementPanel instanceof JecnBaseVHLinePanel) {
							JecnBaseVHLinePanel baseVHLinePanel = (JecnBaseVHLinePanel) baseFlowElementPanel;
							baseVHLinePanel.setVHLineBounds();
						}
					}
				}
			} else if ("fontPotision".equals(comboStr)) { // 字体位置
				for (JecnBaseFlowElementPanel baseFlowElementPanel : getCurrentSelectElementList()) {
					int index = comboBox.getSelectedIndex();
					if (index < 0 || index >= comboBox.getItemCount()) {
						return;
					}
					if (baseFlowElementPanel.getFlowElementData().getFontPosition() != index) {
						// 记录操作前数据
						undoData.recodeFlowElement(baseFlowElementPanel);
						// 修改字体位置
						JecnElementAttributesUtil.updateFontPosition(baseFlowElementPanel, index);
						// 记录操作后数据
						redoData.recodeFlowElement(baseFlowElementPanel);
						isChange = true;
					}
				}
			}
		} else if (e.getSource() instanceof JecnToolbarContentButton) { // 判断是否为toolButton
			if (getCurrentSelectElementList() == null) {
				return;
			}
			JecnToolbarContentButton toolbarContentButton = (JecnToolbarContentButton) e.getSource();
			// 获取事件名称
			String toolButtonStr = toolbarContentButton.getActionCommand();

			if ("fontSizeBig".equals(toolButtonStr)) { // 字体放大
				fontSizeBtnProces(1);
			} else if ("fontSizeSmall".equals(toolButtonStr)) { // 字体缩小
				fontSizeBtnProces(-1);
			} else if ("upRightRadio".equals(toolButtonStr)) { // 字体的横排竖排
				for (JecnBaseFlowElementPanel baseFlowElementPanel : getCurrentSelectElementList()) {
					// 记录操作前数据
					// undoData.recodeFlowElement(baseFlowElementPanel);
					// 获取横竖标识
					boolean flag = baseFlowElementPanel.getFlowElementData().getFontUpRight();
					if (flag) {
						JecnElementAttributesUtil.updateFontUpRight(baseFlowElementPanel, false);
					} else {
						JecnElementAttributesUtil.updateFontUpRight(baseFlowElementPanel, true);
					}
					// 记录操作后数据
					// redoData.recodeFlowElement(baseFlowElementPanel);
				}
			} else if ("fontStyleRadio".equals(toolButtonStr)) { // 字体的加粗
				// 声明变量用来判断记录是否是全部选中状态
				int fontEditTypeBlod = 0;
				// 设置默认值为加粗状态
				int font = Font.BOLD;
				// 获取当前选中元素
				List<JecnBaseFlowElementPanel> jecnBaseFlowElementList = getCurrentSelectElementList();
				for (JecnBaseFlowElementPanel baseFlowElementPanel : jecnBaseFlowElementList) {
					// 获取字体是否加粗
					int fontStyle = baseFlowElementPanel.getFlowElementData().getFontStyle();
					// 判断选中字体中存在加粗
					if (font == fontStyle) {
						fontEditTypeBlod++;
					}
				}
				// 判断是否为全粗状态
				if (jecnBaseFlowElementList.size() == fontEditTypeBlod) {
					font = Font.PLAIN;
				}
				for (JecnBaseFlowElementPanel baseFlowElementPanel : jecnBaseFlowElementList) {// 更新操作
					// 判断当前状态是否和原有状态相同，相同：不执行加粗操作
					if (font == baseFlowElementPanel.getFlowElementData().getFontStyle()) {
						continue;
					}
					// 记录操作前数据
					undoData.recodeFlowElement(baseFlowElementPanel);
					JecnElementAttributesUtil.updateFontStyle(baseFlowElementPanel, font);
					// 记录操作后数据
					redoData.recodeFlowElement(baseFlowElementPanel);
					isChange = true; // 字体发生改变将isChange的值改为true;
				}
			} else if (ToolBarElemType.editAlign.toString().equals(toolButtonStr)) {
				JecnAlignmentPopupMenu popupMenu = new JecnAlignmentPopupMenu();
				popupMenu.show((JecnToolbarContentButton) e.getSource(), 0, 25);
			}
		}
		if (isChange) {
			// 记录这次操作的数据集合
			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
			if (getWorkflow() != null) {
				getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
			}
		}
		// 刷新面板
		getWorkflow().repaint();
	}

	/**
	 * 
	 * 字体放大缩小按钮处理方法
	 * 
	 * @param bc
	 *            int 放大或缩小的步长 放大：+1；缩小：-1
	 * @return boolean true：放大缩小成功；false：放大缩小失败
	 * 
	 */
	private boolean fontSizeBtnProces(int bc) {
		int index = fontSizeComboBox.getSelectedIndex();
		int newIndex = -10;
		Vector<Integer> fontSizeVector = JecnFlowElementUtil.getFontSizeVector();

		if (0 <= index && index < fontSizeVector.size()) {
			newIndex = index + bc;
			if (0 <= newIndex && newIndex < fontSizeVector.size()) {
				// 执行字体大小
				fontSizeComboBox.setSelectedIndex(newIndex);
				return true;
			}
		}

		return false;
	}

	public JComboBox getFontNameComboBox() {
		return fontNameComboBox;
	}

	public JComboBox getFontSizeComboBox() {
		return fontSizeComboBox;
	}

	public JComboBox getFontLocationComboBox() {
		return fontLocationComboBox;
	}

	public JComboBox getStyleTypeComboBox() {
		return styleTypeComboBox;
	}

	public JComboBox getBodyWidthComboBox() {
		return bodyWidthComboBox;
	}

	public JecnColorSelectButton getFontColorBtn() {
		return fontColorBtn;
	}

	public JecnColorSelectButton getStyleFillColorBtn() {
		return styleFillColorBtn;
	}

	public JecnColorSelectButton getStyleLineColorBtn() {
		return styleLineColorBtn;
	}

	public JecnColorSelectButton getStyleShadowsBtn() {
		return styleShadowsBtn;
	}

	public JecnToolbarContentButton getCutBtn() {
		return cutBtn;
	}

	public JecnToolbarContentButton getCopyBtn() {
		return copyBtn;
	}

	public JecnToolbarContentButton getPasteBtn() {
		return pasteBtn;
	}

	public JecnToolbarContentButton getDeleteBtn() {
		return deleteBtn;
	}

	public JCheckBox getTwoArrowCheckBox() {
		return twoArrowCheckBox;
	}

	public JecnToolbarContentButton getFormatPainterBtn() {
		return formatPainterBtn;
	}

	public JCheckBox getLineEditCheckBox() {
		return lineEditHiddenCheckBox;
	}
}
