
package com.jecn.webservice.cetc29;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "MasterDataServicePortType", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService")
@SOAPBinding(use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface MasterDataServicePortType {

	@WebMethod(operationName = "businessDataQuery", action = "")
	@WebResult(name = "out", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService")
	public String businessDataQuery(
			@WebParam(name = "in0", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in0,
			@WebParam(name = "in1", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in1,
			@WebParam(name = "in2", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in2,
			@WebParam(name = "in3", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in3);

	@WebMethod(operationName = "importMasterData_Audit", action = "")
	@WebResult(name = "out", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService")
	public String importMasterData_Audit(
			@WebParam(name = "in0", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in0,
			@WebParam(name = "in1", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in1,
			@WebParam(name = "in2", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in2,
			@WebParam(name = "in3", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in3);

	@WebMethod(operationName = "getAttachmentById", action = "")
	@WebResult(name = "out", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService")
	public ArrayOfString getAttachmentById(
			@WebParam(name = "in0", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in0);

	@WebMethod(operationName = "importMasterData", action = "")
	@WebResult(name = "out", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService")
	public String importMasterData(
			@WebParam(name = "in0", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in0,
			@WebParam(name = "in1", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in1,
			@WebParam(name = "in2", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in2,
			@WebParam(name = "in3", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in3);

	@WebMethod(operationName = "getMasterData", action = "")
	@WebResult(name = "out", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService")
	public String getMasterData(
			@WebParam(name = "in0", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in0,
			@WebParam(name = "in1", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in1,
			@WebParam(name = "in2", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in2,
			@WebParam(name = "in3", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in3,
			@WebParam(name = "in4", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in4);

	@WebMethod(operationName = "importMasterDataAttachment", action = "")
	@WebResult(name = "out", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService")
	public String importMasterDataAttachment(
			@WebParam(name = "in0", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in0,
			@WebParam(name = "in1", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in1,
			@WebParam(name = "in2", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in2,
			@WebParam(name = "in3", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in3,
			@WebParam(name = "in4", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in4);

	@WebMethod(operationName = "getCodeInfo", action = "")
	@WebResult(name = "out", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService")
	public String getCodeInfo(
			@WebParam(name = "in0", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in0,
			@WebParam(name = "in1", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in1,
			@WebParam(name = "in2", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in2,
			@WebParam(name = "in3", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in3);

	@WebMethod(operationName = "getTwoDimentionCode", action = "")
	@WebResult(name = "out", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService")
	public String getTwoDimentionCode(
			@WebParam(name = "in0", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in0,
			@WebParam(name = "in1", targetNamespace = "http://www.meritit.com/datamanage/MasterDataService") String in1);

}
