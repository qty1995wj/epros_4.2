package epros.designer.gui.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class ProcessChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(ProcessChooseDialog.class);
	/**
	 * type 0是所有，1是流程地图，2是流程
	 */
	private int type = 0;

	public ProcessChooseDialog(List<JecnTreeBean> list) {
		super(list, 3);
		this.setTitle(JecnProperties.getValue("processChoose"));

	}

	public ProcessChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		super(list, 3, jecnDialog);
		this.setTitle(JecnProperties.getValue("processChoose"));
	}

	public ProcessChooseDialog(List<JecnTreeBean> list, int chooseType) {
		super(list, chooseType);
		this.setTitle(JecnProperties.getValue("processChoose"));

	}

	public ProcessChooseDialog(List<JecnTreeBean> list, int chooseType, int type) {
		super(list, chooseType);
		this.type = type;
		this.setTitle(JecnProperties.getValue("processChoose"));

	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getProcessAction().searchFlowByName(name, JecnConstants.projectId, type,
					JecnDesignerCommon.isSecondAdmin() ? JecnConstants.getUserId() : null);
		} catch (Exception e) {
			log.error("ProcessChooseDialog searchByName is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public String getSearchLabelName() {

		return JecnProperties.getValue("flowNameC");// 流程名称：
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyProcessTree();
		// if (JecnConstants.isMysql) {
		// return new RoutineProcessTree();
		// } else {
		// return new HighEfficiencyProcessTree();
		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("flowName"));// 流程名称
		return title;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

}
