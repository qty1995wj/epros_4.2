package epros.designer.gui.popedom.positiongroup;

import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

public class BasePosRelatedPosTable extends JTable {

	public BasePosRelatedPosTable() {
		this.setModel(getTableTitle());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	/**
	 * 数据显示
	 * 
	 * @return
	 */
	public Vector<Vector<Object>> getTableData(List<Object[]> objList) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		for (Object[] obj : objList) {
			// 数据转换
			Vector<Object> v = new Vector<Object>();
			if (obj[0] != null && obj[1] != null && obj[2] != null) {
				v.add(obj[0].toString());
				v.add(obj[3].toString());
				v.add(obj[1].toString());
				v.add(obj[2].toString());
				vector.add(v);
				((DefaultTableModel) this.getModel()).addRow(v);
			}
		}
		return vector;

	}

	/**
	 * 分页查询结果
	 * 
	 * @author ZXH
	 * @date 2015-7-21 上午10:12:32
	 * @throws
	 */
	public void pageQueryResult(List<Object[]> objectList) {
		// 清空
		for (int index = this.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) this.getModel()).removeRow(index);
		}
		getTableData(objectList);
	}

	/**
	 * 多选
	 * 
	 * @return
	 */
	public boolean isSelectMutil() {
		return true;
	}

	/**
	 * 隐藏列
	 * 
	 * @return
	 */
	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	/**
	 * 获得Table的表头
	 * 
	 * @return
	 */
	private BasePosRelatedTableMode getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("personName"));
		title.add(JecnProperties.getValue("personInfoPosName"));
		title.add(JecnProperties.getValue("containOrg"));
		return new BasePosRelatedTableMode(null, title);
	}

}

class BasePosRelatedTableMode extends DefaultTableModel {
	public BasePosRelatedTableMode(Vector<Vector<Object>> data,
			Vector<String> title) {
		super(data, title);

	}

	public boolean isCellEditable(int rowindex, int colindex) {
		return false;
	}

}
