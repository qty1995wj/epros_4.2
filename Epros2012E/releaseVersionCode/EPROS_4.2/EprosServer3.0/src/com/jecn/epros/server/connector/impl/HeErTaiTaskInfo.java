package com.jecn.epros.server.connector.impl;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.jecn.epros.server.action.web.login.ad.heertai.HeTaiAfterItem;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.HttpRequestUtil;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;

/**
 * 和而泰代办
 * 
 * @author xiaohu
 * @date 2018-06-13
 * 
 */
public class HeErTaiTaskInfo extends JecnBaseTaskSend {
	private IPersonDao personDao;

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
			// 创建任务
			createTasks(myTaskBean);
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 审批完成，删除待办
			deleteToDoTask(myTaskBean, curPeopleId);
		} else {// 一条from的数据,一条to 的数据
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());
			// 上一个阶段，处理任务，代办待办删除
			deleteToDoTask(myTaskBean, curPeopleId);
			if ((beanNew.getTaskElseState() == 2 || beanNew.getTaskElseState() == 3)
					|| beanNew.getState() != beanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
				// 审批任务，发送下一环节任务待办
				createTasks(myTaskBean);
			}
		}
	}

	/**
	 * 删除待办
	 * 
	 * @param myTaskBean
	 * @param handlerPeopleId
	 * @throws Exception
	 */
	private void deleteToDoTask(MyTaskBean myTaskBean, Long handlerPeopleId) throws Exception {
		MultiValueMap<String, String> map = deleteTask(myTaskBean, handlerPeopleId);
		setTaskInfo(map, 2);
	}

	private void createTasks(MyTaskBean myTaskBean) throws Exception {
		for (Long handlerPeopleId : myTaskBean.getHandlerPeopleIds()) {
			MultiValueMap<String, String> map = createTaskToDo(myTaskBean, handlerPeopleId);
			setTaskInfo(map, 1);
		}
	}

	private MultiValueMap<String, String> createTaskToDo(MyTaskBean myTaskBean, Long handlerPeopleId) {
		JecnUser jecnUser = personDao.get(handlerPeopleId);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("syscode", "EPROS");
		map.add("flowid", getInstanceId(myTaskBean, handlerPeopleId));
		map.add("requestname", myTaskBean.getTaskName());
		map.add("workflowname", "待办任务");
		map.add("nodename", myTaskBean.getStageName());
		map.add("pcurl", getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, false));
		map.add("appurl", getMibileMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, false));
		// 创建人
		map.add("creator", myTaskBean.getCreatePeopleLoginName());
		map.add("createdatetime", myTaskBean.getCreateTime());
		map.add("receiver", jecnUser.getLoginName());
		map.add("receivedatetime", myTaskBean.getUpdateTime());
		return map;
	}

	private MultiValueMap<String, String> deleteTask(MyTaskBean myTaskBean, Long handlerPeopleId) {
		JecnUser jecnUser = personDao.get(handlerPeopleId);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("syscode", "EPROS");
		map.add("flowid", getInstanceId(myTaskBean, handlerPeopleId));
		map.add("userid", jecnUser.getLoginName());
		return map;
	}

	protected String getMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
		return "/loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId=" + loginId + "&isApp="
				+ isApp + "&isView=" + isView;
	}

	protected String getMibileMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
		return "/loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId=" + loginId + "&isApp="
				+ isApp + "&isView=" + isView;
	}

	private String setTaskInfo(MultiValueMap<String, String> map, int taskType) throws Exception {
		String result = null;
		switch (taskType) {
		case 1:// 创建待办
			result = HttpRequestUtil.sendPostRest(HeTaiAfterItem.getValue("receiveTodoUrl"), map);
			break;
		case 2:// 删除待办
			result = HttpRequestUtil.sendPostRest(HeTaiAfterItem.getValue("deleteUserUrl"), map);
			break;
		default:
			break;
		}
		if (StringUtils.isBlank(result)) {
			log.error("数据接口异常 ： taskType = " + taskType + "    " + map);
			throw new IllegalArgumentException("数据接口异常 ：" + result);
		}
		if (result.contains("operResult=0")) {
			log.error("数据接口异常 ：" + result);
			log.error("error taskType = " + taskType + "    " + map);
			throw new IllegalArgumentException("数据接口异常 ：" + result);
		}
		return result;
	}

	private String getInstanceId(MyTaskBean myTaskBean, Long curPeopleId) {
		return myTaskBean.getTaskId() + "_" + myTaskBean.getRid() + "_" + curPeopleId;
	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		JecnUser createUser = personDao.get(beanNew.getCreatePersonId());
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setCreateTime(JecnCommon.getStringbyDateHMS(beanNew.getCreateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(getTaskName(beanNew.getTaskName()));
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setRid(beanNew.getRid().toString());
		myTaskBean.setCreatePeopleLoginName(createUser.getLoginName());
		myTaskBean.setOperationType(beanNew.getTaskElseState());
		myTaskBean.setStageName(beanNew.getStateMark());
		myTaskBean.setCreatePeopleId(beanNew.getCreatePersonId());
		return myTaskBean;
	}

	private String getTaskName(String name) {
		return name.indexOf(".") != -1 ? name.substring(0, name.lastIndexOf(".")) : name;
	}

	/**
	 * 取消任务代办
	 */
	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew beanNew, JecnUser jecnUser, JecnUser createUser) {
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());
		try {
			deleteToDoTask(myTaskBean, jecnUser.getPeopleId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());

		// 批量删除待办
		deleteTasks(myTaskBean, cancelPeoples);

		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
		// 审批任务，发送下一环节任务待办
		createTasks(myTaskBean);
	}

	private void deleteTasks(MyTaskBean myTaskBean, List<Long> handlerPeopleIds) throws Exception {
		for (Long handlerPeopleId : handlerPeopleIds) {
			deleteToDoTask(myTaskBean, handlerPeopleId);
		}
	}

}
