package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.AbstractSyncSpecialDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class LiBangSyncDataFilter extends AbstractSyncSpecialDataFilter {

	public LiBangSyncDataFilter(BaseBean baseBean) {
		super(baseBean);
	}

	@Override
	protected void initSpecialDataPath() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void filterDeptDataBeforeCheck() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void filterUserPosDataBeforeCheck() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeUnusefulDataAfterCheck() {
		// 入库之前删除掉 【岗位所属部门编号在部门集合中不存在】的数据
		UserBean userBean = null;
		int size = syncData.getUserBeanList().size();
		for (int i = size - 1; i >= 0; i--) {
			userBean = syncData.getUserBeanList().get(i);
			if (SyncErrorInfo.POS_DEPT_NUM_NOT_EXISTS_DEPT_LIST.equals(userBean.getUserPosBean().getError())) {
				userBean.setPosNum(null);
				userBean.setPosName(null);
				userBean.setDeptNum(null);
				// syncData.getUserBeanList().remove(i);
			}
		}
	}

}
