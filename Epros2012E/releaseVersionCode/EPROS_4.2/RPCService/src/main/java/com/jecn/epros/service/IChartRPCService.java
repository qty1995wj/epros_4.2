package com.jecn.epros.service;

import com.jecn.epros.bean.ByteFileResult;

import java.util.List;
import java.util.Map;

public interface IChartRPCService {

    public String getChartJSON(Long id, String type, Long userId, Map<String, Object> options);

    public ByteFileResult downloadProcessDetailList(Map<String, Object> param);

    public ByteFileResult downloadProcessScanOptimize(Map<String, Object> param);

    public ByteFileResult downloadProcessKPIFollow(List<Long> orgIds, List<Long> mapIds, String startTime, String endTime, Long projectId);

}
