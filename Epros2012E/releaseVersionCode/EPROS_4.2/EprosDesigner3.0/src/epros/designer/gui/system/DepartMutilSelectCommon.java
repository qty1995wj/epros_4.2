package epros.designer.gui.system;

import java.awt.Insets;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.popedom.organization.OrgChooseDialog;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;

public class DepartMutilSelectCommon extends MutilSelectCommon {

	public DepartMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest, String labName) {
		super(rows, infoPanel, insets, list, labName, jecnDialog, isRequest);
	}
	
	public DepartMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest, String labName,boolean isDownLoad) {
		super(rows, infoPanel, insets, list, labName, jecnDialog, isRequest,isDownLoad);
	}

	@Override
	protected JecnSelectChooseDialog getJecnSelectChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		return new OrgChooseDialog(list, jecnDialog,true);
	}

}
