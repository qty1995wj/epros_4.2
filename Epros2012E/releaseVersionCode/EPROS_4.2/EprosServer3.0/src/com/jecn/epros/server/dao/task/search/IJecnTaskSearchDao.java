package com.jecn.epros.server.dao.task.search;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.epros.server.webBean.task.TaskSearchTempBean;

/**
 * 任务查询，。删除，显示全部记录DAO接口
 * 
 * @author ZHANGXH
 * @date： 日期：Nov 20, 2012 时间：3:06:12 PM
 */
public interface IJecnTaskSearchDao extends IBaseDao<JecnTaskBeanNew, Long> {
	/**
	 * 删除任务相关信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @throws Exception
	 */
	public void deleteTaskBean(Long taskId) throws Exception;

	/**
	 * 根据任务主键ID集合删除任务相关信息
	 * 
	 * @param taskIds
	 *            任务主键ID集合
	 * @throws Exception
	 */
	public void deleteTaskByIds(Set<Long> taskIds) throws Exception;

	/**
	 * 
	 * 假删除
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @throws Exception
	 */
	public void falseDeleteTask(Long taskId) throws Exception;
	
	/**
	 * 人员替换
	 * @param fromPeopleId 来源人id
	 * @param toPeopleId 替换人id
	 * @param taskIdSet 
	 */
	public void updateTaskApprovePeople(String fromPeopleId, String toPeopleId) throws Exception;

	public int countAllSecondAdminTaskNum(Long roleId, Long projectId, TaskSearchTempBean searchTempBean) throws Exception;

	public List<MyTaskBean> listSecondAdminList(TaskSearchTempBean searchTempBean, int start, int limit,
			Long curPeopleId,Long roleId) throws Exception;
    
}
