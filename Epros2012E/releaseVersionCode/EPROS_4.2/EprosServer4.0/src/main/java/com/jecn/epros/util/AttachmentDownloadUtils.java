package com.jecn.epros.util;

import com.jecn.epros.domain.Attachment;
import com.jecn.epros.service.DownloadResultMapUtils;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.net.URLEncoder;
import java.util.Map;

public class AttachmentDownloadUtils {

    private final static Logger log = LoggerFactory.getLogger(AttachmentDownloadUtils.class);

    public static ResponseEntity<?> newResponseEntity(HttpServletRequest request, Attachment attachment) {
        InputStreamResource isr = new InputStreamResource(new ByteArrayInputStream(attachment.getContent()));
        HttpHeaders headers = AttachmentDownloadUtils.getDownloadFileHttpHeadersByRequest(request,
                attachment.getName());

        return ResponseEntity.ok().headers(headers).body(isr);
    }

    public static Attachment resultMap2Attachment(Map<String, Object> resultMap) {
        Attachment attachment = new Attachment();
        attachment.setName(DownloadResultMapUtils.getFileName(resultMap));
        attachment.setContent(DownloadResultMapUtils.getContent(resultMap));
        return attachment;
    }

    private static HttpHeaders getDownloadFileHttpHeadersByRequest(HttpServletRequest request, String fileName) {
        UserAgent userAgent = getUserAgentByRequest(request);

        String encodedFileName = getEncodedFileNameByBrowser(fileName, userAgent.getBrowser().getGroup());

        HttpHeaders headers = new HttpHeaders();

        headers.setContentDispositionFormData("attachment", encodedFileName);
        // TODO
        // 标识此文件的类型（由于EPROS文件上传时未进行记录，因此默认标识为文件格式，以后如果记录了上传附件的路径，应当在此处修改默认设置）
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        return headers;
    }

    private static String getEncodedFileNameByBrowser(String fileName, Browser browser) {
        String encodedFileName = null;
        try {
            switch (browser) {
                case CHROME:
                case IE:
                case SAFARI:
                case FIREFOX:
                    encodedFileName = URLEncoder.encode(fileName, "utf-8").replaceAll("\\+", "%20");
                    break;
                default:
                    encodedFileName = MimeUtility.encodeText(fileName);
                    break;
            }
        } catch (Exception e) {
            log.error("字符编码转换异常", e);
        }
        return encodedFileName;
    }

    private static UserAgent getUserAgentByRequest(HttpServletRequest request) {
        String userAgentString = request.getHeader("User-Agent");
        return UserAgent.parseUserAgentString(userAgentString);
    }

}
