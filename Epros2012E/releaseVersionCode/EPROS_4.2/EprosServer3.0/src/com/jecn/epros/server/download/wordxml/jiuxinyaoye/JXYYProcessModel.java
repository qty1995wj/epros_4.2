package com.jecn.epros.server.download.wordxml.jiuxinyaoye;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import wordxml.constant.Constants;
import wordxml.constant.Fonts;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.WordGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfo;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlow;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.download.wordxml.approve.ApproveManager;
import com.jecn.epros.server.download.wordxml.approve.ApprovePeople;
import com.jecn.epros.server.download.wordxml.approve.ApproveState;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 九新药业
 * 
 * @author jecn
 * 
 */
public class JXYYProcessModel extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距 1.5倍行距 *****/
	private final ParagraphLineRule vLine1_5 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);
	private final String format = "yyyy年MM月dd日";

	/**
	 * 默认模版
	 * 
	 * @param processDownloadBean
	 * @param path
	 * @param flowChartDirection
	 */
	public JXYYProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(17.49F);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		getDocProperty().setFlowChartScaleValue(8.5F);
		setDocStyle("、", textTitleStyle());
		setDefAscii(Fonts.ROMAN);
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.5F, 2.22F, 1.5F, 1.32F, 1.1F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getCharSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.1F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommhdr(8.86F, 8.68F, 17.53F, sect, HeaderOrFooterType.odd);
		createCommftr(17.53F, sect, HeaderOrFooterType.odd);
	}

	private void createCharCommhdrftr(Sect sect) {
		createCommhdr(13.7F, 13F, 26.7F, sect, HeaderOrFooterType.odd);
		createCommftr(26.7F, sect, HeaderOrFooterType.odd);
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param h
	 * @param g
	 * @param f
	 * 
	 * @param sect
	 */
	private void createCommhdr(float f, float g, float h, Sect sect, HeaderOrFooterType type) {
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		WordGraph image = getImageByClass(8.66F, 1.17F, "1.png");
		header.addParagraph(new Paragraph(image));
		TableBean tableBean = new TableBean();
		tableBean.setTableAlign(Align.left);
		tableBean.setBorder(1F);
		tableBean.setBorderLeft(0F);
		tableBean.setBorderRight(0F);
		tableBean.setBorderTop(2.5F);
		tableBean.setTableAlign(Align.center);

		ParagraphBean pBean = new ParagraphBean(Align.left, "黑体", Constants.xiaosi, true);
		Table table = header.createTable(tableBean, new float[] { h });
		table.createTableRow(new String[] { "标题：" + processDownloadBean.getFlowName() }, pBean);
		table.setCellValignAndHeight(Valign.center, 1.07F);

		table = header.createTable(tableBean, new float[] { f, g });
		String version = processDownloadBean.getLatestHistoryNew() == null ? "" : processDownloadBean
				.getLatestHistoryNew().getVersionId();
		table.createTableRow(new String[] { "编号：" + nullToEmpty(processDownloadBean.getFlowInputNum()),
				"版本：第" + nullToEmpty(version) + "版" }, pBean);
		table.setCellValignAndHeight(Valign.center, 1.07F);

	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param f
	 * 
	 * @param sect
	 */
	private void createCommftr(float f, Sect sect, HeaderOrFooterType type) {
		Footer footer = sect.createFooter(type);
		TableBean tableBean = new TableBean();
		tableBean.setBorderTop(1F);
		Table table = footer.createTable(tableBean, new float[] { f });
		TableRow row = table.createTableRow();
		row.createCells(new String[] { "页码：" }, Valign.center, new ParagraphBean(Align.right, Fonts.SONG,
				Constants.wuhao, false));
		Paragraph paragraph = row.getCell(0).getParagraph(0);
		paragraph.appendCurPageRun().setFontBean(new FontBean(Fonts.ROMAN, Constants.wuhao, false));
		paragraph.appendTextRun("/").setFontBean(new FontBean(Fonts.ROMAN, Constants.wuhao, false));
		paragraph.appendTotalPagesRun().setFontBean(new FontBean(Fonts.ROMAN, Constants.wuhao, false));
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommhdrftr(firstSect);
		createContent(firstSect);
	}

	private void createContent(Sect sect) {
		sect.createParagraph("");
		TableBean tableBean = new TableBean();
		tableBean.setBorder(1F);
		tableBean.setBorderLeft(0F);
		tableBean.setBorderRight(0F);
		ParagraphBean pBean = new ParagraphBean(Align.center, Fonts.SONG, Constants.xiaosi, true);
		ParagraphBean leftBean = new ParagraphBean(Align.left, Fonts.SONG, Constants.xiaosi, true);
		Table table = sect.createTable(tableBean, new float[] { 4.54F, 3.75F, 4.59F, 4.66F });
		// 审批人-------------------------------------
		table.createTableRow(new String[] { "文件责任", "岗  位", "姓  名", "日  期" }, pBean);
		JecnTaskHistoryNew lastHistoryNew = processDownloadBean.getLatestHistoryNew();

		String pubTime = "";
		String nextTime = "";
		if (lastHistoryNew != null) {
			Date publishDate = lastHistoryNew.getPublishDate();
			pubTime = JecnUtil.DateToStr(publishDate, format);
			Calendar instance = Calendar.getInstance();
			instance.setTime(publishDate);
			instance.add(Calendar.YEAR, 1);
			nextTime = JecnUtil.DateToStr(instance.getTime(), format);
		}

		// 起草人为拟稿人，审核人为三级流程责任人及二级流程责任人，批准人为一级流程责任人。
		// 一级责任人是事业部经理 二级责任人是it总监 三级责任人是批准人
		Long f = getFictionPeopleId();
		Long l3 = getLevelDuty(3);
		Long l2 = getLevelDuty(2);
		Long l1 = getLevelDuty(1);

		Set<Long> peopleIds = new HashSet<Long>();
		if (f != null) {
			peopleIds.add(f);
		}
		if (l3 != null) {
			peopleIds.add(l3);
		}
		if (l2 != null) {
			peopleIds.add(l2);
		}
		if (l1 != null) {
			peopleIds.add(l1);
		}
		Map<Long, ApprovePeople> approvePeopleMap = ApproveManager.getApprovePeopleMap(this.processDownloadBean
				.getBaseDao(), peopleIds);

		ApproveState a = createState(approvePeopleMap, f);
		ApproveState b1 = createState(approvePeopleMap, l3);
		ApproveState b2 = createState(approvePeopleMap, l2);
		ApproveState c = createState(approvePeopleMap, l1);

		createApproveTable("起草人", sect, tableBean, pBean, a, pubTime, "流程梳理人");
		createApproveTable("审核人", sect, tableBean, pBean, b1, pubTime, "三级流程责任人", b2, "二级流程责任人");
		createApproveTable("批准人", sect, tableBean, pBean, c, pubTime, "一级流程责任人");

		// 发布日期-------------------------------------
		table = sect.createTable(tableBean, new float[] { 8.29F, 9.25F });

		table.createTableRow(new String[] { "生效日期：" + pubTime, "再审核日期：" + nextTime }, leftBean);
		table.setCellValignAndHeight(Valign.center, 1.07F);
		// 制订部门 -------------------------------------
		table = sect.createTable(tableBean, new float[] { 17.53F });
		TableRow row = table.createTableRow(new String[] { "制订部门：" }, leftBean);
		row.getCell(0).getParagraph(0).appendTextRun(nullToEmpty(processDownloadBean.getResponFlow().getDutyOrgName()),
				new FontBean(Fonts.SONG, Constants.xiaosi, false));
		table.setCellValignAndHeight(Valign.center, 1.07F);
		// 分发部门 -------------------------------------
		row = table.createTableRow(new String[] { "分发部门：" }, leftBean);
		row.getCell(0).getParagraph(0).appendTextRun(nullToEmpty(getPermOrgs()),
				new FontBean(Fonts.SONG, Constants.xiaowu, false));
		table.setCellValignAndHeight(Valign.center, 1.07F);
	}

	private ApproveState createState(Map<Long, ApprovePeople> approvePeopleMap, Long... ids) {
		if (ids == null || ids.length == 0) {
			return null;
		}
		ApproveState result = new ApproveState();
		List<ApprovePeople> peoples = new ArrayList<ApprovePeople>();
		result.setPeoples(peoples);
		for (Long id : ids) {
			if (id == null) {
				continue;
			}
			ApprovePeople approvePeople = approvePeopleMap.get(id);
			if (approvePeople != null) {
				peoples.add(approvePeople);
			}
		}
		return result;
	}

	private Long getFictionPeopleId() {
		Long result = null;
		if (processDownloadBean.isPub()) {
			JecnFlowBasicInfo basic = getFlowBase(processDownloadBean.getFlowId(), processDownloadBean.isPub());
			if (basic.getFictionPeopleId() != null) {
				result = basic.getFictionPeopleId();
			}
		} else {
			JecnFlowBasicInfoT basic = getFlowBase(processDownloadBean.getFlowId(), processDownloadBean.isPub());
			if (basic.getFictionPeopleId() != null) {
				result = basic.getFictionPeopleId();
			}
		}
		return result;
	}

	private Long getLevelDuty(int level) {
		Long result = null;
		String userId = null;
		if (processDownloadBean.isPub()) {
			userId = getLevelDutyPub(level);
		} else {
			userId = getLevelDutyNotPub(level);
		}
		if (StringUtils.isNotBlank(userId)) {
			result = Long.valueOf(userId);
		}
		return result;
	}

	private String getLevelDutyNotPub(int level) {
		boolean isPub = false;
		String tPath = getTPath();
		try {
			if (StringUtils.isNotBlank(tPath)) {
				String ids = tPath;
				String[] split = ids.split("-");
				if (split.length - 1 < level) {
					log.info("未找到对应的level" + level);
					return null;
				}
				Long id = Long.valueOf(split[level]);
				JecnFlowStructureT flow = getFlow(id, isPub);
				if (flow == null) {
					return null;
				}
				if (flow.getIsFlow() == 1) {
					JecnFlowBasicInfoT jecnFlowBasicInfoT = getFlowBase(id, isPub);
					if (jecnFlowBasicInfoT.getTypeResPeople() != null && jecnFlowBasicInfoT.getTypeResPeople() == 0
							&& jecnFlowBasicInfoT.getResPeopleId() != null) {
						return String.valueOf(jecnFlowBasicInfoT.getResPeopleId());
					}
				} else if (flow.getIsFlow() == 0) {
					JecnMainFlowT flowMapInfo = getMap(id, isPub);
					if (flowMapInfo.getTypeResPeople() != null && flowMapInfo.getTypeResPeople() == 0
							&& flowMapInfo.getResPeopleId() != null) {
						return String.valueOf(flowMapInfo.getResPeopleId());
					}
				}
			} else {
				log.info("对应的t_path为空");
			}
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	private String getLevelDutyPub(int level) {
		boolean isPub = true;
		String tPath = getTPath();
		try {
			if (StringUtils.isNotBlank(tPath)) {
				String ids = tPath;
				String[] split = ids.split("-");
				if (split.length - 1 < level) {
					log.info("未找到对应的level" + level);
					return null;
				}
				Long id = Long.valueOf(split[level]);
				JecnFlowStructure flow = getFlow(id, isPub);
				if (flow == null) {
					return null;
				}
				if (flow.getIsFlow() == 1) {
					JecnFlowBasicInfo jecnFlowBasicInfoT = getFlowBase(id, isPub);
					if (jecnFlowBasicInfoT.getTypeResPeople() != null && jecnFlowBasicInfoT.getTypeResPeople() == 0
							&& jecnFlowBasicInfoT.getResPeopleId() != null) {
						return String.valueOf(jecnFlowBasicInfoT.getResPeopleId());
					}
				} else if (flow.getIsFlow() == 0) {
					JecnMainFlow flowMapInfo = getMap(id, isPub);
					if (flowMapInfo.getTypeResPeople() != null && flowMapInfo.getTypeResPeople() == 0
							&& flowMapInfo.getResPeopleId() != null) {
						return String.valueOf(flowMapInfo.getResPeopleId());
					}
				}
			} else {
				log.info("对应的t_path为空");
			}
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	private String getTPath() {
		return processDownloadBean.getFlowStructure() != null ? processDownloadBean.getFlowStructure().gettPath()
				: processDownloadBean.getFlowStructureT().gettPath();
	}

	private <T> T getFlow(Long id, boolean isPub) {
		String pub = isPub ? "" : "T";
		String hql = "from JecnFlowStructure" + pub + " where flowId=?";
		return (T) processDownloadBean.getBaseDao().getObjectHql(hql, id);
	}

	private <T> T getFlowBase(Long id, boolean isPub) {
		String pub = isPub ? "" : "T";
		String hql = "from JecnFlowBasicInfo" + pub + " where flowId=?";
		return (T) processDownloadBean.getBaseDao().getObjectHql(hql, id);
	}

	private <T> T getMap(Long id, boolean isPub) {
		String pub = isPub ? "" : "T";
		String hql = "from JecnMainFlow" + pub + " where flowId=?";
		return (T) processDownloadBean.getBaseDao().getObjectHql(hql, id);
	}

	private String getPermOrgs() {
		try {
			List<JecnTreeBean> orgList = processDownloadBean.getPerms().getOrgList();
//			List<JecnTreeBean> orgList = getOrgAccessPermissions(processDownloadBean.getFlowId(), 0,
//					processDownloadBean.isPub());
			if (!JecnUtil.isEmpty(orgList)) {
				return concat(orgList);
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return "";
	}

	private List<JecnTreeBean> getOrgAccessPermissions(Long relateId, int type, boolean isPub) throws Exception {
		String sql = "";
		if (isPub) {
			sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count ,joap.access_type"
					+ " from jecn_flow_org jfo,JECN_ORG_ACCESS_PERMISSIONS joap where jfo.org_id = joap.org_id and joap.relate_id=? and joap.type=? and jfo.del_state=0"
					+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
		} else {
			sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,(select count(*) from jecn_flow_org where per_org_id=jfo.org_id ) as count,joap.access_type"
					+ " from jecn_flow_org jfo,JECN_ORG_ACCESS_PERM_T joap where jfo.org_id = joap.org_id and joap.relate_id=? and joap.type=? and jfo.del_state=0"
					+ " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
		}
		List<Object[]> listOrg = processDownloadBean.getBaseDao().listNativeSql(sql, relateId, type);
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : listOrg) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectOrgAccess(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	private Table createApproveTable(String state, Sect sect, TableBean tableBean, ParagraphBean pBean, ApproveState a,
			String pubTime, String posName) {
		Table table = sect.createTable(tableBean, new float[] { 4.54F, 3.75F, 4.59F, 4.66F });
		String peopleName = "";
		if (a != null && JecnUtil.isNotEmpty(a.getPeoples())) {
			peopleName = a.getPeoples().get(0).getName();
		}
		table.createTableRow(new String[] { state, posName, peopleName, pubTime }, pBean);
		table.setCellValignAndHeight(Valign.center, 1.07F);
		return table;
	}

	private Table createApproveTable(String state, Sect sect, TableBean tableBean, ParagraphBean pBean, ApproveState a,
			String pubTime, String posName, ApproveState b, String posNameB) {
		Table table = sect.createTable(tableBean, new float[] { 4.54F, 3.75F, 4.59F, 4.66F });
		String peopleName = "";
		String peopleNameB = "";
		if (a != null && JecnUtil.isNotEmpty(a.getPeoples())) {
			peopleName = a.getPeoples().get(0).getName();
		}
		if (b != null && JecnUtil.isNotEmpty(b.getPeoples())) {
			peopleNameB = b.getPeoples().get(0).getName();
		}
		table.createTableRow(new String[] { state, posName, JecnUtil.nullToEmpty(peopleName), pubTime }, pBean);
		table.createTableRow(new String[] { state, posNameB, JecnUtil.nullToEmpty(peopleNameB), pubTime }, pBean);
		table.setCellValignAndHeight(Valign.center, 1.07F);
		table.getRow(0).getCell(0).setRowspan(2);
		return table;
	}

	private Table createApproveTable(String state, Sect sect, TableBean tableBean, ParagraphBean pBean, ApproveState a,
			String pubTime) {
		Table table = sect.createTable(tableBean, new float[] { 4.54F, 3.75F, 4.59F, 4.66F });
		if (a != null && JecnUtil.isNotEmpty(a.getPeoples())) {
			List<ApprovePeople> peoples = a.getPeoples();
			for (int x = 0; x < peoples.size(); x++) {
				ApprovePeople p = peoples.get(x);
				String poes = "";
				List<String[]> posAndOrgs = p.getPosAndOrgs();
				if (JecnUtil.isNotEmpty(posAndOrgs)) {
					StringBuffer buf = new StringBuffer();
					for (int i = 0; i < posAndOrgs.size(); i++) {
						String[] posAndOrg = posAndOrgs.get(i);
						if (i == 0) {
							buf.append(nullToEmpty(posAndOrg[1]));
						} else {
							buf.append("/");
							buf.append(nullToEmpty(posAndOrg[1]));
						}
					}
					poes = buf.toString();
				}
				if (x > 1) {
					state = "";
				}
				TableRow row = table.createTableRow(new String[] { state, poes, p.getName(), pubTime }, pBean);
				table.getRow(0).getCell(0).setRowspan(peoples.size());
			}
			table.setCellValignAndHeight(Valign.center, 1.07F);
		} else {
			TableRow row = table.createTableRow(new String[] { state, "", "", "" }, pBean);
		}
		table.setCellValignAndHeight(Valign.center, 1.07F);
		return table;
	}

	private String getRolePosOrg() {
		String pub = processDownloadBean.isPub() ? "" : "_T";
		String sql = "SELECT distinct jfo.org_name FROM jecn_flow_structure_image" + pub + " jfsi"
				+ "  inner join PROCESS_STATION_RELATED" + pub
				+ " psr on jfsi.figure_id=psr.FIGURE_FLOW_ID and psr.type=0"
				+ "  inner join jecn_flow_org_image jfoi on jfoi.figure_id=psr.figure_position_id"
				+ "  inner join jecn_flow_org jfo on jfoi.org_id=jfo.org_id"
				+ "  where jfsi.figure_type='RoleFigure' and jfsi.flow_id=?";
		IBaseDao baseDao = processDownloadBean.getBaseDao();
		List<String> names = baseDao.listObjectNativeSql(sql, "org_name", Hibernate.STRING, processDownloadBean
				.getFlowId());
		return concat(names, "/");
	}

	private String getPosConcat(Long peopleId, Map<Long, List<String>> peoplePosMap) {
		List<String> pos = peoplePosMap.get(peopleId);
		String result = JecnUtil.concat(pos, "/");
		return result;
	}

	private Map<Long, String> getNameMap(List<Long> peopleIds) {
		Map<Long, String> result = new HashMap<Long, String>();
		if (JecnUtil.collectIsEmpty(peopleIds)) {
			return result;
		}
		String hql = "from JecnUser where peopleId in " + JecnCommonSql.getIds(peopleIds);
		IBaseDao baseDao = processDownloadBean.getBaseDao();
		List<JecnUser> users = baseDao.listHql(hql);
		for (JecnUser u : users) {
			result.put(u.getPeopleId(), u.getTrueName());
		}
		return result;
	}

	private Map<Long, List<String>> getPosMap(List<Long> peopleIds) {
		Map<Long, List<String>> result = new HashMap<Long, List<String>>();
		if (JecnUtil.collectIsEmpty(peopleIds)) {
			return result;
		}
		String sql = "select distinct ju.people_id, jfoi.figure_text" + "  from jecn_flow_org_image jfoi"
				+ " inner join jecn_user_position_related jupr" + "    on jfoi.figure_id = jupr.figure_id"
				+ " inner join jecn_user ju" + "    on ju.people_id = jupr.people_id" + " where ju.people_id in "
				+ JecnCommonSql.getIds(peopleIds);
		IBaseDao baseDao = processDownloadBean.getBaseDao();
		List<Object[]> objs = baseDao.listNativeSql(sql);
		Long peopleId = null;
		String posName = null;
		for (Object[] obj : objs) {
			peopleId = Long.valueOf(obj[0].toString());
			posName = nullToEmpty(obj[1]);
			if (result.containsKey(peopleId)) {
				List<String> pos = result.get(peopleId);
				pos.add(posName);
			} else {
				List<String> pos = new ArrayList<String>();
				pos.add(posName);
				result.put(peopleId, pos);
			}
		}
		return result;
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(2.0F, 1.76F, 1.5F, 1.76F, 1.1F, 1.75F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCharCommhdrftr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.wuhao, false));
		tblContentStyle.setSpace(0.5F, 0.2F, vLine1); // 设置段落行间距
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(1F);
		// 表格左缩进0.94 厘米
		tblStyle.setTableLeftInd(0F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, new FontBean("宋体", Constants.wuhao, true));
		tblTitileStyle.setSpace(0.5F, 0.2F, vLine1); // 设置段落行间距
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, new FontBean("宋体", Constants.wuhao, false));
		textContentStyle.setInd(0.52F, 0, 0, 0);
		textContentStyle.setSpace(0f, 0f, vLine1_5);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, new FontBean(Fonts.HEI, Constants.xiaosi, true));
		textTitleStyle.setInd(0, 0, 0.95F, 0); // 段落缩进 厘米
		textTitleStyle.setSpace(0.5F, 0.2F, vLine1_5); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
