package com.jecn.epros.server.service.control.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowApplyOrgT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT2;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfo2Dao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.process.IProcessKPIDao;
import com.jecn.epros.server.dao.process.IProcessRecordDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.rule.RuleHistoryDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.service.control.IJecnDocControlService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;

@Transactional(rollbackFor = Exception.class)
public class JecnDocControlServiceImpl extends AbsBaseService<JecnTaskHistoryNew, Long> implements
		IJecnDocControlService {
	private Logger logger = Logger.getLogger(JecnDocControlServiceImpl.class);

	private IJecnDocControlDao docControlDao;
	/** 流程信息处理接口 */
	private IFlowStructureDao flowStructureDao;

	private IRuleDao ruleDao;
	/**
	 * 制度文控
	 */
	private RuleHistoryDao ruleHistoryDao;
	private IFileDao fileDao;
	private IJecnConfigItemDao configItemDao;
	private IProcessBasicInfoDao processBasicDao;
	private IProcessKPIDao processKPIDao;
	private IProcessRecordDao processRecordDao;
	private IProcessBasicInfo2Dao processBasicDao2;
	private IPersonDao personDao;

	/**
	 * 发布记录文控，创建文控版本信息日志
	 * 
	 * @param historyNew
	 *            文控信息
	 * 
	 * @throws Exception
	 */
	public void saveJecnTaskHistoryNew(JecnTaskHistoryNew historyNew) throws Exception {
		long startTime = System.currentTimeMillis();
		logger.info("流程图发布，saveJecnTaskHistoryNew记录文控版本 start: " + startTime);
		JecnTaskCommon.saveJecnTaskHistoryNew(historyNew, docControlDao, flowStructureDao, ruleDao, ruleHistoryDao,
				fileDao, configItemDao, processBasicDao, processKPIDao, processRecordDao);
		logger.info("流程图发布，saveJecnTaskHistoryNew记录文控版本 end: " + (System.currentTimeMillis() - startTime));
		logger.info("流程图发布成功! " + (System.currentTimeMillis() - startTime));
	}

	/**
	 * 去除两组人员邮箱信息中id相同的信息
	 * 
	 * @param userEmail2
	 *            List<Object[]> [0]邮件类型 [1]邮件地址 [2]收件人 第一个人员邮箱组
	 * @param fixedEmail
	 *            List<Object[]> [0]邮件类型 [1]邮件地址 [2]收件人 第二个人员邮箱组
	 * @return
	 */
	private List<JecnUser> distinctEmailAddress(List<JecnUser> userEmail, List<JecnUser> fixedEmail) {
		Map<Long, JecnUser> emailMap = new HashMap<Long, JecnUser>();
		for (JecnUser user : userEmail) {
			emailMap.put(user.getPeopleId(), user);
		}
		for (JecnUser user : fixedEmail) {
			emailMap.put(user.getPeopleId(), user);
		}
		return new ArrayList<JecnUser>(emailMap.values());
	}

	/**
	 * 删除文控版本信息日志
	 * 
	 * @param docId
	 *            文控主键ID
	 * @throws Exception
	 * @throws Exception
	 */
	public void deleteJecnTaskHistoryNew(List<Long> listDocId) throws Exception {
		// 删除多条文控信息
		// 文控信息集合
		List<JecnTaskHistoryNew> historyNewList = new ArrayList<JecnTaskHistoryNew>();
		for (Long docId : listDocId) {
			historyNewList.add(docControlDao.get(docId));
		}
		// 不存在版本信息
		if (historyNewList.size() == 0) {
			return;
		}
		// 1 删除目录下文件， 删除文控信息
		// listId 文控信息主键ID
		List<Long> listId = new ArrayList<Long>();
		for (JecnTaskHistoryNew historyNew : historyNewList) {
			// 获取文件存储目录
			if (historyNew.getType() == 0 || historyNew.getType() == 4 || historyNew.getType() == 2) {
				// 拼装文件全目录
				String filePath = historyNew.getFilePath();
				JecnFinal.deleteFile(filePath);
			}
			listId.add(historyNew.getId());
		}
		// 2 根据文控主键ID和类型，关联ID获取 文件存储目录
		docControlDao.deleteJecnTaskHistoryNew(listId);
	}

	/**
	 * 
	 * 编辑文控信息
	 * 
	 * @param JecnTaskHistoryNew
	 * @return true:版本号已存在
	 * @throws Exception
	 */
	public boolean updateJecnTaskHistoryNew(JecnTaskHistoryNew historyNew, String versionId) throws Exception {
		if (historyNew == null || historyNew.getId() == null || versionId == null) {
			return false;
		}
		if (!historyNew.getVersionId().equals(versionId.trim())) {
			// 1 修改后的文控信息再当前文件版本记录内版本号必须唯一
			int count = docControlDao.getVersionIdCountById(historyNew.getVersionId());
			if (count > 0) {
				logger.error("版本号已存在!");
				return true;
			}
		}

		for (JecnTaskHistoryFollow historyFollow : historyNew.getListJecnTaskHistoryFollow()) {
			docControlDao.getSession().update(historyFollow);
		}
		// 2保存修改后的版本信息
		docControlDao.update(historyNew);
		return false;
	}

	/**
	 * 查阅文控信息
	 * 
	 * @param docId
	 *            文控主键ID
	 * @param refId
	 *            关联ID
	 * @throws Exception
	 */
	public FileOpenBean viewJecnTaskHistoryNew(Long docId, Long ruleId) throws Exception {
		JecnTaskHistoryNew historyNew = docControlDao.get(docId);
		if (historyNew == null || historyNew.getId() == null) {
			return null;
		}
		FileOpenBean fileOpenBean = null;
		// historyNew.getType(): 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
		if (historyNew.getType() == 1) {
			// 1、获取版本文件
			fileOpenBean = JecnDaoUtil.getFileOpenBean(docControlDao, historyNew.getFileContentId());
		} else if (historyNew.getType() == 3) {
			fileOpenBean = getRuleFileOpenBean(ruleId, historyNew.getFileContentId());
		} else {
			// 获取文件存储目录
			String filePath = historyNew.getFilePath();
			fileOpenBean = new FileOpenBean();
			fileOpenBean.setName(filePath);
			fileOpenBean.setFileByte(JecnFinal.getBytesByFilePath(filePath));
		}
		return fileOpenBean;
	}

	private FileOpenBean getRuleFileOpenBean(Long ruleId, Long contentId) throws Exception {
		RuleT ruleT = ruleDao.get(ruleId);
		FileOpenBean fileOpenBean = null;
		if (ruleT == null || ruleT.getIsFileLocal() == null) {
			return null;
		}
		if (ruleT.getIsFileLocal() == 0) {
			// 1、获取版本文件
			fileOpenBean = JecnDaoUtil.getFileOpenBean(ruleDao, contentId);
		} else {
			// 1、获取版本文件
			G020RuleFileContent fileContent = (G020RuleFileContent) ruleDao.getSession().get(G020RuleFileContent.class,
					contentId);
			if (fileContent.getIsVersionLocal() == 0) {
				fileOpenBean = new FileOpenBean();
				fileOpenBean.setName(fileContent.getFileName());
				fileOpenBean.setFileByte(JecnFinal.getBytesByFilePath(fileContent.getFilePath()));
			} else {
				// 根据制度文件的本地上传的附件id
				fileOpenBean = JecnDaoUtil.openRuleLocalFile(ruleDao, contentId);
			}
		}
		return fileOpenBean;
	}

	public IJecnDocControlDao getDocControlDao() {
		return docControlDao;
	}

	public void setDocControlDao(IJecnDocControlDao docControlDao) {
		this.docControlDao = docControlDao;
	}

	/**
	 * 
	 * 根据流程ID获取文控信息版本号集合
	 * 
	 * @param flowId
	 *            流程ID
	 * @param type
	 *            0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
	 * @return List<String>文控信息版本号集合
	 * @throws Exception
	 */
	public List<String> findVersionListByFlowId(Long flowId, int type) throws Exception {
		return docControlDao.findVersionListByFlowId(flowId, type);
	}

	/**
	 * 文件文控类型和关联ID获取文控信息版本记录
	 * 
	 * @param refId
	 *            关联ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @return List<JecnTaskHistoryNew>
	 * @throws Exception
	 */
	public List<JecnTaskHistoryNew> getJecnTaskHistoryNewList(Long refId, int type) throws Exception {
		return docControlDao.getJecnTaskHistoryNewList(refId, type);

	}

	public boolean isContentNotNull(String str) {
		if (str == null || "".equals(str.trim())) {
			return true;
		}
		return false;
	}

	/**
	 * 文件基本信息是否必填验证
	 * 
	 * @param flowId
	 *            流程ID
	 * @return List<String>
	 */
	@Override
	public List<String> getErrorList(Long flowId, int languageType) {
		List<String> errorList = new ArrayList<String>();
		List<JecnConfigItemBean> configItemBeanList = configItemDao.selectShowFlowFile();
		JecnFlowBasicInfoT flowBasicInfoT = processBasicDao.get(flowId);
		if (!JecnContants.isPubShow()) {// D2版本
			JecnFlowBasicInfoT2 flowBasicInfoT2 = processBasicDao2.get(flowId);
			if (flowBasicInfoT2 != null) {
				// 相关文件
				flowBasicInfoT.setFlowRelatedFile(flowBasicInfoT2.getFlowRelatedFile());
				// 自定义要素1
				flowBasicInfoT.setFlowCustomOne(flowBasicInfoT2.getFlowCustomOne());
				// 自定义要素2
				flowBasicInfoT.setFlowCustomTwo(flowBasicInfoT2.getFlowCustomTwo());
				// 自定义要素3
				flowBasicInfoT.setFlowCustomThree(flowBasicInfoT2.getFlowCustomThree());
				// 自定义要素4
				flowBasicInfoT.setFlowCustomFour(flowBasicInfoT2.getFlowCustomFour());
				// 自定义要素5
				flowBasicInfoT.setFlowCustomFive(flowBasicInfoT2.getFlowCustomFive());
			}
		}
		String strNull = languageType == 0 ? JecnUtil.getValue("isCannotNull") : "  Can't be empty!"; // web
		// 端
		// 暂未做国际化配置文件
		// 所以暂时以字符串的形式代替
		for (JecnConfigItemBean confiBean : configItemBeanList) {
			String error = null;
			if (confiBean.getIsEmpty() == null || confiBean.getIsEmpty() == 0) {
				continue;
			}
			if (confiBean.getMark().equals("a1")) { // 目的
				if (isContentNotNull(flowBasicInfoT.getFlowPurpose())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a2")) { // 适用范围
				boolean showOrgScope = JecnConfigTool.isShowOrgScope();
				if (showOrgScope) {
					String hql = "from JecnFlowApplyOrgT where relateId=?";
					List<JecnFlowApplyOrgT> listOld = processBasicDao.listHql(hql, flowId);
					if (!JecnUtil.isEmpty(listOld)) {
						continue;
					}
				}
				if (isContentNotNull(flowBasicInfoT.getApplicability())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a3")) { // 术语定义
				if (isContentNotNull(flowBasicInfoT.getNoutGlossary())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a4")) { // 驱动规则
				if (isContentNotNull(flowBasicInfoT.getDriveRules())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a5")) { // 输入
				if (isContentNotNull(flowBasicInfoT.getInput())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a6")) { // 输出
				if (isContentNotNull(flowBasicInfoT.getOuput())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a16")) { // 客户
				if (isContentNotNull(flowBasicInfoT.getFlowCustom())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a17")) { // 不适用范围
				if (isContentNotNull(flowBasicInfoT.getNoApplicability())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a18")) { // 概述
				if (isContentNotNull(flowBasicInfoT.getFlowSummarize())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a19")) { // 补充说明
				if (isContentNotNull(flowBasicInfoT.getFlowSupplement())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.a15.toString())) {// 流程关键测评指标板
				List<JecnFlowKpiNameT> list = processKPIDao.listHql("from JecnFlowKpiNameT where flowId=?", flowId);
				if (list == null || list.size() == 0) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a26")) { // 相关文件
				if (JecnCommon.isMySql()) {
					if (isContentNotNull(flowBasicInfoT.getFlowRelatedFile())) {
						error = confiBean.getName(languageType) + strNull;
					}
				}
			} else if (confiBean.getMark().equals("a27")) { // 自定义要素1
				if (isContentNotNull(flowBasicInfoT.getFlowCustomOne())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a28")) { // 自定义要素2
				if (isContentNotNull(flowBasicInfoT.getFlowCustomTwo())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a29")) { // 自定义要素3
				if (isContentNotNull(flowBasicInfoT.getFlowCustomThree())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a30")) { // 自定义要素4
				if (isContentNotNull(flowBasicInfoT.getFlowCustomFour())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a31")) { // 自定义要素5
				if (isContentNotNull(flowBasicInfoT.getFlowCustomFive())) {
					error = confiBean.getName(languageType) + strNull;
				}
			} else if (confiBean.getMark().equals("a32")) { // 输入和输出
				if (isContentNotNull(flowBasicInfoT.getInput())) {// 输入
					error = confiBean.getName(languageType) + strNull;
				}
				if (isContentNotNull(flowBasicInfoT.getOuput())) {// 输出
					error = confiBean.getName(languageType) + strNull;
				}
			}
			if (error != null) {
				errorList.add(error);
			}
		}
		return errorList;
	}

	public List<String> flowKPIErrorValidate(Long flowId) {
		List<JecnConfigItemBean> configList = configItemDao.selectConfigItemBeanByType(1, 16);
		return null;
	}

	/**
	 * 根据文控信息主键ID获取从表信息
	 * 
	 * @param historyId
	 *            文控主键ID
	 * @return 文控信息对应的各阶段审批人信息
	 * @throws Exception
	 */
	@Override
	public List<JecnTaskHistoryFollow> getJecnTaskHistoryFollowList(Long historyId) throws Exception {
		return docControlDao.getJecnTaskHistoryFollowList(historyId);
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	public IRuleDao getRuleDao() {
		return ruleDao;
	}

	public void setRuleDao(IRuleDao ruleDao) {
		this.ruleDao = ruleDao;
	}

	public IFileDao getFileDao() {
		return fileDao;
	}

	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
	}

	public IProcessBasicInfoDao getProcessBasicDao() {
		return processBasicDao;
	}

	public void setProcessBasicDao(IProcessBasicInfoDao processBasicDao) {
		this.processBasicDao = processBasicDao;
	}

	public IProcessKPIDao getProcessKPIDao() {
		return processKPIDao;
	}

	public void setProcessKPIDao(IProcessKPIDao processKPIDao) {
		this.processKPIDao = processKPIDao;
	}

	public IProcessRecordDao getProcessRecordDao() {
		return processRecordDao;
	}

	public void setProcessRecordDao(IProcessRecordDao processRecordDao) {
		this.processRecordDao = processRecordDao;
	}

	public IJecnConfigItemDao getConfigItemDao() {
		return configItemDao;
	}

	public void setConfigItemDao(IJecnConfigItemDao configItemDao) {
		this.configItemDao = configItemDao;
	}

	public IProcessBasicInfo2Dao getProcessBasicDao2() {
		return processBasicDao2;
	}

	public void setProcessBasicDao2(IProcessBasicInfo2Dao processBasicDao2) {
		this.processBasicDao2 = processBasicDao2;
	}

	public IPersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(IPersonDao personDao) {
		this.personDao = personDao;
	}

	@Override
	public List<String> findVersionListByFlowIdNoPage(Long id, int type) {
		return docControlDao.findVersionListByFlowIdNoPage(id, type);
	}

}
