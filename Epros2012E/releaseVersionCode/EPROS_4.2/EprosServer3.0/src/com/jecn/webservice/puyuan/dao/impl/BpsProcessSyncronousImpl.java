package com.jecn.webservice.puyuan.dao.impl;



import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.jecn.epros.server.bean.process.JecnFlowStructureImage;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.webservice.puyuan.dao.IBpsProcessSynchronous;


@Component
public class BpsProcessSyncronousImpl extends
		AbsBaseDao<JecnFlowStructureImage, Long> implements
		IBpsProcessSynchronous {

	Logger log = Logger.getLogger(BpsProcessSyncronousImpl.class);

	/***************************************************************************
	 * 
	 * 根据流程ID获取流程所属的流程架构以及当前要同步的流程信息
	 * 
	 * @author chehuanbo
	 * 
	 **************************************************************************/
	public List<Object[]> findAllByFlowMapId(Long flowId) {
          
	   String sql = "SELECT JFS.FLOW_ID,JFS.FLOW_NAME,JU.TRUE_NAME,JFO.ORG_NAME FROM JECN_FLOW_STRUCTURE JFS" +
	                " LEFT JOIN JECN_USER JU" + 
	                " ON JFS.PEOPLE_ID = JU.PEOPLE_ID" + 
	                " LEFT JOIN JECN_USER_POSITION_RELATED JUPR" + 
	                " ON JU.PEOPLE_ID = JUPR.PEOPLE_ID" + 
	                " LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI" + 
	                " ON  JUPR.FIGURE_ID = JFOI.FIGURE_ID" + 
	                " LEFT JOIN JECN_FLOW_ORG JFO" + 
	                " ON JFOI.ORG_ID = JFO.ORG_ID" + 
	                " WHERE JFS.FLOW_ID = ?";

		List<Object[]> objects = null;

		try {
			objects = this.listNativeSql(sql, flowId);
		} catch (Exception e) {
			log.error("根据流程ID获取流程信息出现异常! "
					+ "类：findAllByFlowMapId 方法findAllByFlowImageId 行：50", e);
		}
           
		return objects;
	}
	

	/**
	 * 
	 * 根据流程ID获取所有的流程元素
	 * 
	 * @author chehuanbo
	 * 
	 */
	public List<Object[]> findAllByFlowImageId(Long flowId) {

		String sql ="SELECT JFSIT.FLOW_ID," +
					"       JFSIT.FIGURE_ID," + 
					"       JFSIT.FIGURE_TYPE," + 
					"       JFSIT.FIGURE_TEXT," + 
					"       JFSIT.START_FIGURE," + 
					"       JFSIT.END_FIGURE," + 
					"       JFSIT.X_POINT," + 
					"       JFSIT.Y_POINT," + 
					"       JFSIT.WIDTH," + 
					"       JFSIT.HEIGHT," + 
					"       JFSIT.ACTIVITY_ID," + 
					"       JFSIT.CIRCUMGYRATE," + 
					"       JSI.FIGURE_TEXT," + 
					"       JSI.FIGURE_ID," +
					"       JFSIT.ACTIVITY_SHOW" +
					"  FROM JECN_FLOW_STRUCTURE_IMAGE JFSIT" + 
					"  LEFT JOIN JECN_ROLE_ACTIVE JRA" + 
					"  ON JRA.FIGURE_ACTIVE_ID = JFSIT.FIGURE_ID" + 
					"  LEFT  JOIN JECN_FLOW_STRUCTURE_IMAGE JSI" + 
					"  ON JSI.FIGURE_ID = JRA.FIGURE_ROLE_ID" + 
					"  WHERE JFSIT.FLOW_ID = ?" + 
					"   AND JFSIT.FIGURE_TYPE IN ('RoundRectWithLine'," + 
					"   'CommonLine'," + 
					"   'ManhattanLine'," + 
					"   'ReverseArrowhead'," + 
					"   'RightArrowhead'," + 
					"   'Rhombus'," + 
					"   'DottedRect'," + 
					"   'ImplFigure'," + 
					"   'OvalSubFlowFigure'," + 
					"   'DataImage'," + 
					"   'ToFigure'," + 
					"   'EndFigure'," + 
					"   'XORFigure'," + 
					"   'ORFigure'," + 
					"   'FlowFigureStart'," + 
					"   'FlowFigureStop'," + 
					"   'AndFigure'," + 
					"   'ITRhombus'," + 
					"   'EventFigure')";


		List<Object[]> objects = null;

		try {
			objects = this.listNativeSql(sql, flowId);
		} catch (Exception e) {
			log.error("根据流程ID获取所有的流程元素出现异常! 类：BpsProcessSyncronousImpl 方法findAllByFlowImageId 行：87",
							e);
		}

		return objects;
	}
	
	
	public void test1(){
		String sql ="SELECT JFSIT.FLOW_ID," +
		"       JFSIT.FIGURE_ID," + 
		"       JFSIT.FIGURE_TYPE," + 
		"       JFSIT.FIGURE_TEXT," + 
		"       JFSIT.START_FIGURE," + 
		"       JFSIT.END_FIGURE," + 
		"       JFSIT.X_POINT," + 
		"       JFSIT.Y_POINT," + 
		"       JFSIT.WIDTH," + 
		"       JFSIT.HEIGHT," + 
		"       JFSIT.ACTIVITY_ID," + 
		"       JFSIT.CIRCUMGYRATE," + 
		"       JSI.FIGURE_TEXT" + 
		"  FROM JECN_FLOW_STRUCTURE_IMAGE JFSIT" + 
		"  LEFT JOIN JECN_ROLE_ACTIVE JRA" + 
		"  ON JRA.FIGURE_ACTIVE_ID = JFSIT.FIGURE_ID" + 
		"  LEFT  JOIN JECN_FLOW_STRUCTURE_IMAGE JSI" + 
		"  ON JSI.FIGURE_ID = JRA.FIGURE_ROLE_ID" + 
		"  WHERE JFSIT.FLOW_ID = 50" + 
		"   AND JFSIT.FIGURE_TYPE IN ('RoundRectWithLine'," + 
		"   'CommonLine'," + 
		"   'ManhattanLine'," + 
		"   'ReverseArrowhead'," + 
		"   'RightArrowhead'," + 
		"   'Rhombus'," + 
		"   'DottedRect'," + 
		"   'ImplFigure'," + 
		"   'OvalSubFlowFigure'," + 
		"   'DataImage'," + 
		"   'ToFigure'," + 
		"   'EndFigure'," + 
		"   'XORFigure'," + 
		"   'ORFigure'," + 
		"   'FlowFigureStart'," + 
		"   'FlowFigureStop'," + 
		"   'AndFigure'," + 
		"   'ITRhombus'," + 
		"   'EventFigure')";
		System.out.println(sql);
	}
    

	/**
	 * 根据协作框，查找连接当前协作框的连接线
	 * 
	 * @author chehuanbo
	 * 
	 */
	public List<Object[]> findAllByDottedRectId(Long figureId) {

		String sql = "SELECT" + "   JFSIT.FLOW_ID," + "   JFSIT.FIGURE_ID,"
				+ "   JFSIT.FIGURE_TYPE," + "   JFSIT.FIGURE_TEXT,"
				+ "   JFSIT.START_FIGURE," + "   JFSIT.END_FIGURE,"
				+ "   JFSIT.X_POINT," + "   JFSIT.Y_POINT," + "   JFSIT.WIDTH,"
				+ "   JFSIT.HEIGHT" + " FROM JECN_FLOW_STRUCTURE_IMAGE JFSIT "
				+ " WHERE JFSI.END_FIGURE = ? OR JFSI.START_FIGURE = ?";

		List<Object[]> objects = null;

		try {
			objects = this.listHql(sql, figureId, figureId);
		} catch (Exception e) {
			log.error(
							"根据流程ID获取所有的流程元素出现异常! 类：BpsProcessSyncronousImpl 方法findAllByFlowImageId 行：63",
							e);
		}

		return objects;
	}



}
