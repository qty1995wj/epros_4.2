package com.jecn.epros.server.bean.popedom;

import java.io.Serializable;

public class UserAddReturnBean implements Serializable {
	/** 人员ID */
	private Long userId;
	/** 0:增加成功 1：角色人数已达上限;2:二级管理员数已达到上限; */
	private int type = 0;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String password;
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
