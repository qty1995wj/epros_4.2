package epros.designer.gui.system;

import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.process.flowtool.FlowToolChooseDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.top.dialog.JecnDialog;

public class SupportToolCommon extends SingleSelectCommon {
	public SupportToolCommon(int rows, JecnPanel infoPanel, Insets insets, JecnTreeBean jecnTreeBean,
			JecnDialog jecnDialog,boolean isRequest) {
		super(rows, infoPanel, insets, jecnTreeBean, jecnDialog, JecnProperties.getValue("flowToolC"),isRequest);
	}

	private static Logger log = Logger.getLogger(SupportToolCommon.class);

	@Override
	protected List<JecnTreeBean> searchName(String name, Long projectId) {
		List<JecnTreeBean> listTreeBean = new ArrayList<JecnTreeBean>();
		try {
			listTreeBean = ConnectionPool.getFlowTool().getSustainToolsByName(name);
		} catch (Exception e) {
			log.error("SupportToolCommon searchName is error", e);
		}
		return listTreeBean;
	}

	@Override
	protected void selectButPerformed(List<JecnTreeBean> list, JSearchTextField jecnField, Long id) {
		FlowToolChooseDialog flowToolChooseDialog = new FlowToolChooseDialog(list);
		flowToolChooseDialog.setSelectMutil(false);
		flowToolChooseDialog.setVisible(true);
		if (flowToolChooseDialog.isOperation()) {
			jecnField.setText(getNameResult());
		}
	}

}
