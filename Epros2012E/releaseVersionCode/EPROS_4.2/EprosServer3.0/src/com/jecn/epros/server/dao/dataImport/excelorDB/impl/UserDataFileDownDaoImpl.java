package com.jecn.epros.server.dao.dataImport.excelorDB.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.dataImport.excelorDB.IUserDataFileDownDao;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncSqlConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class UserDataFileDownDaoImpl extends AbsBaseDao<DeptBean, Long>
		implements IUserDataFileDownDao {
	private static final Logger log = Logger
			.getLogger(UserDataFileDownDaoImpl.class);

	/**
	 * 
	 * 获取部门所有数据（部门编号、部门名称、上级部门编号）
	 * 
	 */
	@Override
	public List<DeptBean> selectDeptList() throws Exception {
		List<DeptBean> deptList = new ArrayList<DeptBean>();
		// 获取Session以便左数据库操作
		Session s = this.getSession();
		log.debug("删部门表(JECN_FLOW_ORG)数据开始 ");

		Query query = s.createSQLQuery(SyncSqlConstant.SELECT_DEPT_ALL_DATA);
		query.setLong("PROJECTID", SyncTool.getProjectIDBySession());

		// 执行查询
		List<Object[]> objList = query.list();

		if (objList == null || objList.size() == 0) {
			return deptList;
		}
		for (Object[] obj : objList) {
			DeptBean deptBean = new DeptBean();
			// 部门编号
			deptBean.setDeptNum(SyncTool.nullToEmtryByObj(obj[0]));
			// 部门编号
			deptBean.setDeptName(SyncTool.nullToEmtryByObj(obj[1]));
			// 部门编号
			deptBean.setPerDeptNum(SyncTool.nullToEmtryByObj(obj[2]));

			deptList.add(deptBean);
		}
		log.debug("删除通用部门临时表(JECN_FLOW_ORG)数据结束 ");

		return deptList;
	}

	/**
	 * 
	 * 获取人员岗位以及他们之间关联关系数据
	 * 
	 */
	@Override
	public List<UserBean> selectUserPosList() throws Exception {
		// 获取Session以便左数据库操作
		Session s = this.getSession();

		List<UserBean> userList = new ArrayList<UserBean>();
		log.debug("删岗位表(JECN_USER)数据开始 ");

		Query query = s
				.createSQLQuery(SyncSqlConstant.SELECT_USER_POS_ALL_DATA);
		query.setLong("PROJECTID", SyncTool.getProjectIDBySession());
		// 执行查询
		List<Object[]> objList = query.list();

		if (objList == null || objList.size() == 0) {
			return userList;
		}
		for (Object[] obj : objList) {
			UserBean userBean = new UserBean();
			// 登录名称
			userBean.setUserNum(SyncTool.nullToEmtryByObj(obj[0]));
			// 真实姓名
			userBean.setTrueName(SyncTool.nullToEmtryByObj(obj[1]));
			// 任职岗位编号
			userBean.setPosNum(SyncTool.nullToEmtryByObj(obj[2]));
			// 任职岗位名称
			userBean.setPosName(SyncTool.nullToEmtryByObj(obj[3]));
			// 岗位所属部门编号
			userBean.setDeptNum(SyncTool.nullToEmtryByObj(obj[4]));
			// 邮箱地址
			userBean.setEmail(SyncTool.nullToEmtryByObj(obj[5]));
			// 内外网邮件标识(内网:0 外网:1)
			userBean.setEmailType(SyncTool.nullToEmtryIntByObj(obj[6]));
			// 联系电话
			userBean.setPhone(SyncTool.nullToEmtryByObj(obj[7]));

			userList.add(userBean);
		}

		log.debug("删除通用岗位临时表(JECN_USER)数据结束 ");

		return userList;

	}
}
