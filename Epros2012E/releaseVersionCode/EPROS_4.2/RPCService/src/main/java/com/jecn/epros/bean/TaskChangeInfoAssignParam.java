package com.jecn.epros.bean;

/**
 * 文控，部门审核人转批交办
 * @author user
 *
 */
public class TaskChangeInfoAssignParam extends TaskChangeInfoApproveParam {
	
	private String userAssionedId;

	public String getUserAssionedId() {
		return userAssionedId;
	}

	public void setUserAssionedId(String userAssionedId) {
		this.userAssionedId = userAssionedId;
	}
}
