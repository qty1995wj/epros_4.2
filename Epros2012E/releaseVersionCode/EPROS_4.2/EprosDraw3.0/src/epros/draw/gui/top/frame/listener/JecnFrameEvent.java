package epros.draw.gui.top.frame.listener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.error.ErrorInfoConstant;

/**
 * 
 * 窗体事件类
 * 
 * @author ZHOUXY
 *
 */
public class JecnFrameEvent {
	private final Log log = LogFactory.getLog(JecnFrameEvent.class);

	private Object source = null;

	public JecnFrameEvent(Object source) {
		if (source == null) {
			log.error("JecnFrameEvent类JecnFrameEvent构造函数参数为空 source=" + source);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.source = source;
	}

	public Object getSource() {
		return source;
	}
}
