package com.jecn.epros.server.service.popedom.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.file.JecnFileBean;
import com.jecn.epros.server.bean.log.FileDownloadLog;
import com.jecn.epros.server.bean.popedom.JecnRoleContent;
import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.popedom.UserRole;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.popedom.IJecnRoleDao;
import com.jecn.epros.server.service.popedom.IJecnRoleService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.popedom.RoleWebInfoBean;

@Transactional(rollbackFor = Exception.class)
public class JecnRoleServiceImpl extends AbsBaseService<JecnRoleInfo, Long> implements IJecnRoleService {

	private Logger log = Logger.getLogger(JecnRoleServiceImpl.class);
	private IJecnRoleDao jecnRoleDao;

	public IJecnRoleDao getJecnRoleDao() {
		return jecnRoleDao;
	}

	public void setJecnRoleDao(IJecnRoleDao jecnRoleDao) {
		this.jecnRoleDao = jecnRoleDao;
		this.baseDao = jecnRoleDao;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：角色子节点通用查询
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjects(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}

			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			if (obj.length > 4) {
				if ("1".equals(obj[3].toString())) {
					// 角色目录
					jecnTreeBean.setTreeNodeType(TreeNodeType.roleDir);
				} else if (obj[4] != null && JecnCommonSql.getStrSecondAdmin().equals(obj[4].toString())) {
					// 角色节点
					jecnTreeBean.setTreeNodeType(TreeNodeType.roleSecondAdmin);
				} else {
					// 角色节点
					jecnTreeBean.setTreeNodeType(TreeNodeType.role);
				}
				if (obj[4] != null) {
					jecnTreeBean.setNumberId(obj[4].toString());
				}

			} else {
				// 角色节点
				jecnTreeBean.setTreeNodeType(TreeNodeType.role);
			}

			// 角色节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 角色节点名称
			jecnTreeBean.setName(obj[1].toString());
			// 角色节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			// 角色节点父类名称
			jecnTreeBean.setPname("");
			if (obj.length > 5) {
				if (Integer.parseInt(obj[5].toString()) > 0) {
					// 角色节点是否有子节点
					jecnTreeBean.setChildNode(true);
				} else {
					jecnTreeBean.setChildNode(false);
				}
			} else {
				jecnTreeBean.setChildNode(false);
			}

			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getDefaultRoles(boolean isAdmin) throws Exception {
		try {
			String defaultAuthString = "";
			if (isAdmin) {
				defaultAuthString = JecnCommonSql.getDefaultAuthString();
			} else {
				defaultAuthString = JecnCommonSql.getSecondAdminDefaultAuthString();
			}
			// 获得默认角色的sql
			String sql = "select t.role_id,t.role_name,t.FILTER_ID from jecn_role_info t where t.filter_id in "
					+ defaultAuthString;
			List<Object[]> list = jecnRoleDao.listNativeSql(sql);
			return getResultTreeBean(TreeNodeType.roleDefault, list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private List<JecnTreeBean> getResultTreeBean(TreeNodeType nodeType, List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 角色默认节点
			jecnTreeBean.setTreeNodeType(nodeType);
			// 角色默认节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 角色默认节点名称
			jecnTreeBean.setName(obj[1].toString());
			// 角色默认节点父ID
			jecnTreeBean.setPid(Long.valueOf(0));
			// 角色默认节点父类名称
			jecnTreeBean.setPname("");
			// 角色默认节点是否有子节点
			if (obj.length > 3 && obj[3] != null) {
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
			if (obj[2] != null) {
				jecnTreeBean.setNumberId(obj[2].toString());
			}

			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	/**
	 * 获取二级管理员角色
	 */
	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getSecondAdminTreeBean(Long preId) throws Exception {
		List<Object[]> list = jecnRoleDao.getSecondAdminObjs(preId);
		return getResultTreeBean(TreeNodeType.roleSecondAdmin, list);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getChildRoles(Long id, Long projectId, Long secondAdminPeopleId) throws Exception {
		try {
			List<Object[]> list = null;
			String sql = "";
			if (id == 0 && secondAdminPeopleId != null) {// 二级管理员登陆，并且默认根节点为目录
				sql = "SELECT T.ROLE_ID, T.ROLE_NAME, 0 per_id, 1 is_dir, T.FILTER_ID, 1 isNode"
						+ "  FROM JECN_ROLE_INFO T" + " INNER JOIN JECN_USER_ROLE UR" + "    ON T.ROLE_ID = UR.ROLE_ID"
						+ "   AND UR.PEOPLE_ID = ?" + " WHERE T.PROJECT_ID = ? AND T.FILTER_ID IS NOT NULL"
						+ " ORDER BY T.PER_ID, T.SORT_ID, T.ROLE_ID";
				list = jecnRoleDao.listNativeSql(sql, secondAdminPeopleId, projectId);
			} else {// 首次加载或者tree展开
				// 通过父类Id和项目ID，获得role子节点(角色节点和角色目录)sql
				sql = "select t.role_id, t.role_name,t.per_id,t.is_dir,t.FILTER_ID,"
						+ "(select count(*) from jecn_role_info where per_id=t.role_id) from jecn_role_info t "
						+ "where t.project_id=?";
				if (secondAdminPeopleId != null || id == 0) {// 首次展开时或二级管理员节点展开是
					sql = sql + "  AND T.FILTER_ID IS NULL";
				}
				sql = sql + " and t.per_id=?" + " order by t.per_id,t.sort_id,t.role_id";

				list = jecnRoleDao.listNativeSql(sql, projectId, id);
			}
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getChildRoleDirs(Long id, Long projectId, Long secondAdminPeopleId) throws Exception {
		try {
			List<Object[]> list = null;
			String sql = "";
			if (id == 0 && secondAdminPeopleId != null) {// 二级管理员登陆，并且默认根节点为目录
				sql = "SELECT T.ROLE_ID, T.ROLE_NAME, 0 per_id, 1 is_dir, T.FILTER_ID, 1 isNode"
						+ "  FROM JECN_ROLE_INFO T" + " INNER JOIN JECN_USER_ROLE UR" + "    ON T.ROLE_ID = UR.ROLE_ID"
						+ "   AND UR.PEOPLE_ID = ?" + " ORDER BY T.PER_ID, T.SORT_ID, T.ROLE_ID";
				list = jecnRoleDao.listNativeSql(sql, secondAdminPeopleId);
			} else {
				sql = "select t.role_id, t.role_name,t.per_id,t.is_dir,t.FILTER_ID,"
						+ "(select count(*) from jecn_role_info where is_dir=1 and per_id=t.role_id) from jecn_role_info t "
						+ "where t.is_dir=1 " + " and t.per_id=?" + " order by t.per_id,t.sort_id,t.role_id";
				list = jecnRoleDao.listNativeSql(sql, id);
			}
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getAllRoles(Long projectId) throws Exception {
		try {
			// 通过父类Id和项目ID，获得role子节点(角色节点和角色目录)sql
			String sql = "select t.role_id, t.role_name,t.per_id,t.is_dir,t.FILTER_ID,"
					+ "(select count(*) from jecn_role_info where per_id=t.role_id) from jecn_role_info t "
					+ "where t.project_id=? order by t.per_id,t.sort_id,t.role_id";
			List<Object[]> list = jecnRoleDao.listNativeSql(sql, projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getAllRoleDirs(Long projectId) throws Exception {
		try {
			String sql = "select t.role_id, t.role_name,t.per_id,t.is_dir,t.FILTER_ID,"
					+ "(select count(*) from jecn_role_info where is_dir=1 and per_id=t.role_id) "
					+ "from jecn_role_info t where t.is_dir=1 and t.project_id=?"
					+ " order by t.per_id,t.sort_id,t.role_id";
			List<Object[]> list = jecnRoleDao.listNativeSql(sql, projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getRolesByName(Long projectId, String name) throws Exception {
		try {
			String sql = "select t.role_id, t.role_name,t.per_id,t.is_dir,t.FILTER_ID from jecn_role_info t where t.project_id=?"
					+ " and t.is_dir=0 and t.role_name like ?" + " order by t.per_id,t.sort_id,t.role_id";
			List<Object[]> list = jecnRoleDao.listNativeSql(sql, 0, 20, projectId, "%" + name + "%");
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getSecondAdminRolesByName(Long projectId, String name, Long peopleId) throws Exception {
		Long roleId = jecnRoleDao.getSecondAdminRoleId(peopleId);
		List<Object[]> list = jecnRoleDao.getSecondAdminRolesByName(projectId, roleId, name);
		return findByListObjects(list);
	}

	@Override
	public void updateSortRoles(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersionId)
			throws Exception {
		try {
			String hql = "from JecnRoleInfo where perId=? and projectId=?";
			List<JecnRoleInfo> listJecnRoleInfo = jecnRoleDao.listHql(hql, pId, projectId);
			List<JecnRoleInfo> listUpdate = new ArrayList<JecnRoleInfo>();
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				for (JecnRoleInfo jecnRoleInfo : listJecnRoleInfo) {
					if (jecnTreeDragBean.getId().equals(jecnRoleInfo.getRoleId())) {
						if (jecnRoleInfo.getSortId() == null
								|| (jecnTreeDragBean.getSortId() != jecnRoleInfo.getSortId().intValue())) {
							jecnRoleInfo.setSortId(jecnTreeDragBean.getSortId());
							listUpdate.add(jecnRoleInfo);
							break;
						}
					}
				}
			}

			for (JecnRoleInfo jecnRoleInfo : listUpdate) {
				jecnRoleInfo.setUpdatePersonId(updatePersionId);
				jecnRoleInfo.setUpdateTime(new Date());
				jecnRoleDao.update(jecnRoleInfo);
				// 添加日志
				JecnUtil.saveJecnJournal(jecnRoleInfo.getRoleId(), jecnRoleInfo.getRoleName(), 3, 5, jecnRoleInfo
						.getUpdatePersonId(), jecnRoleDao);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void moveRoles(List<Long> listIds, Long pId, Long updatePersionId) throws Exception {
		try {
			String hql = " update JecnRoleInfo set perId=?,updateTime=?,updatePersonId=? where roleId in "
					+ JecnCommonSql.getIds(listIds);
			jecnRoleDao.execteHql(hql, pId, new Date(), updatePersionId);
			// 添加日志
			for (Long id : listIds) {
				JecnUtil.saveJecnJournal(id, null, 3, 4, updatePersionId, jecnRoleDao);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void deleteRoles(List<Long> listIds, Long projectId, Long updatePersionId) throws Exception {
		try {
			Set<Long> setIds = new HashSet<Long>();
			String sql = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "WITH MY_JECN AS(SELECT * FROM jecn_role_info RT WHERE RT.ROLE_ID in"
						+ JecnCommonSql.getIds(listIds) + " UNION ALL"
						+ " SELECT RTT.* FROM MY_JECN INNER JOIN jecn_role_info RTT"
						+ " ON MY_JECN.ROLE_ID=RTT.per_id)" + " SELECT distinct ROLE_ID FROM MY_JECN";
				List<Object> list = jecnRoleDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select distinct t.ROLE_ID from jecn_role_info t"
						+ "       connect by prior t.ROLE_ID = t.per_id" + "       start with t.ROLE_ID in"
						+ JecnCommonSql.getIds(listIds);
				List<Object> list = jecnRoleDao.listNativeSql(sql);
				for (Object obj : list) {
					if (obj == null) {
						continue;
					}
					setIds.add(Long.valueOf(obj.toString()));
				}
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				sql = "select t.ROLE_ID,t.per_id from jecn_role_info t where t.project_id=?";
				List<Object[]> listAll = jecnRoleDao.listNativeSql(sql, projectId);
				setIds = JecnCommon.getAllChilds(listIds, listAll);
			}
			if (setIds.size() > 0) {
				// 添加日志
				JecnUtil.saveJecnJournals(setIds, 3, 3, updatePersionId, jecnRoleDao);
				jecnRoleDao.deleteRoles(setIds);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 封装JecnRoleContent对象
	 * 
	 * @param type
	 * @param listUpdate
	 * @param ids
	 */
	private void packageJecnRoleContent(Long roleId, int type, List<JecnRoleContent> listUpdate, String ids) {
		if (ids != null && !"".equals(ids)) {
			String[] idArr = ids.split(",");
			for (String str : idArr) {
				if (str != null && !"".equals(str)) {
					JecnRoleContent jecnRoleContent = new JecnRoleContent();
					jecnRoleContent.setRoleId(roleId);
					jecnRoleContent.setRelateId(Long.valueOf(str));
					jecnRoleContent.setType(type);
					listUpdate.add(jecnRoleContent);
				}
			}
		}
	}

	@Override
	public Long addRole(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId) throws Exception {
		try {
			jecnRoleInfo.setUpdateTime(new Date());
			jecnRoleInfo.setCreateTime(new Date());
			Long roleId = jecnRoleDao.save(jecnRoleInfo);
			List<JecnRoleContent> listUpdate = new ArrayList<JecnRoleContent>();
			// 遍历map 添加相关角色
			for (Entry<Integer, String> entry : mapIds.entrySet()) {
				this.packageJecnRoleContent(roleId, entry.getKey(), listUpdate, entry.getValue());
			}
			for (JecnRoleContent jecnRoleContent : listUpdate) {
				jecnRoleDao.getSession().save(jecnRoleContent);
			}
			// 添加日志
			JecnUtil.saveJecnJournal(jecnRoleInfo.getRoleId(), jecnRoleInfo.getRoleName(), 3, 1, jecnRoleInfo
					.getUpdatePersonId(), jecnRoleDao);
			return roleId;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * @author zhangchen May 2, 2012
	 * @description：处理自定义角色
	 * @param ids
	 *            已知参数 0是流程，1是文件，2是标准，3是制度对应的权限ID集合
	 * @param noNeedDelIds
	 *            未知参数
	 * @param listSave
	 *            未知参数
	 * @param listOld
	 *            已知参数 根据roleID 得到的关联表信息
	 * @param type
	 *            0是流程，1是文件，2是标准，3是制度
	 * @param roleId
	 */
	private void handleRole(String ids, List<JecnRoleContent> listOld, int type, Long roleId) {
		if (StringUtils.isNotBlank(ids)) {
			String[] idsArr = ids.split(",");
			for (String id : idsArr) {
				if (StringUtils.isNotBlank(id)) {
					boolean isExist = false;
					for (JecnRoleContent jecnRoleContent : listOld) {
						if (jecnRoleContent.getType() == null) {
							continue;
						}
						if (jecnRoleContent.getRelateId().toString().equals(id)
								&& jecnRoleContent.getType().intValue() == type
								&& roleId.equals(jecnRoleContent.getRoleId())) {
							isExist = true;
							listOld.remove(jecnRoleContent);
							break;
						}
					}
					if (!isExist) {
						JecnRoleContent jecnRoleContent = new JecnRoleContent();
						jecnRoleContent.setRelateId(Long.valueOf(id));
						jecnRoleContent.setRoleId(roleId);
						jecnRoleContent.setType(type);
						jecnRoleDao.getSession().save(jecnRoleContent);
					}
				}
			}
		}
	}

	@Override
	public Long updateRole(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception {
		try {
			jecnRoleInfo.setUpdateTime(new Date());
			jecnRoleInfo.setUpdatePersonId(updatePersionId);
			jecnRoleDao.update(jecnRoleInfo);
			String hql = "from JecnRoleContent where roleId=?";
			Long roleId = jecnRoleInfo.getRoleId();
			List<JecnRoleContent> listOld = jecnRoleDao.listHql(hql, roleId);
			// 遍历map 添加相关角色
			for (Entry<Integer, String> entry : mapIds.entrySet()) {
				this.handleRole(entry.getValue(), listOld, entry.getKey(), roleId);
			}

			// 删除数据库老数据
			for (JecnRoleContent jecnRoleContent : listOld) {
				jecnRoleDao.getSession().delete(jecnRoleContent);
			}
			// 添加日志
			JecnUtil.saveJecnJournal(jecnRoleInfo.getRoleId(), jecnRoleInfo.getRoleName(), 3, 2, jecnRoleInfo
					.getUpdatePersonId(), jecnRoleDao);
			return roleId;
		} catch (Exception e) {
			log.error("更新风险自定义角色出现异常" + this + "updateRole()方法中", e);
			throw e;
		}
	}

	@Override
	public void updateName(String newName, Long id, Long updatePersonId) throws Exception {
		try {
			JecnRoleInfo jecnRoleInfo = jecnRoleDao.get(id);
			jecnRoleInfo.setUpdatePersonId(updatePersonId);
			jecnRoleInfo.setUpdateTime(new Date());
			jecnRoleInfo.setRoleName(newName);
			jecnRoleDao.update(jecnRoleInfo);
			// 添加日志
			JecnUtil.saveJecnJournal(jecnRoleInfo.getRoleId(), jecnRoleInfo.getRoleName(), 3, 2, jecnRoleInfo
					.getUpdatePersonId(), jecnRoleDao, 4);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getRoleDirsByNameMove(Long projectId, String name, List<Long> listIds) throws Exception {
		try {
			String sql = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "WITH MY_JECN AS(SELECT * FROM jecn_role_info JFF WHERE JFF.role_id in"
						+ JecnCommonSql.getIds(listIds)
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_JECN INNER JOIN jecn_role_info JFS ON MY_JECN.role_id=JFS.per_id)"
						+ "    select t.role_id, t.role_name,t.per_id,t.is_dir"
						+ "                ,(select count(*) from jecn_role_info where per_id=t.role_id and is_dir=1)"
						+ "         from jecn_role_info t where t.project_id = "
						+ projectId
						+ "         and t.role_name like ? and t.role_id not in (select tt.role_id from jecn_role_info tt where tt.filter_id in "
						+ JecnCommonSql.getDefaultAuthString()
						+ " ) and t.is_dir=1 and t.role_id not in (select role_id from MY_JECN)";
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select t.role_id, t.role_name,t.per_id,t.is_dir"
						+ "                ,(select count(*) from jecn_role_info where per_id=t.role_id and is_dir=1)"
						+ "         from jecn_role_info t where t.project_id = "
						+ projectId
						+ "         and t.role_name like ? and t.role_id not in (select tt.role_id from jecn_role_info tt where tt.filter_id in "
						+ JecnCommonSql.getDefaultAuthString()
						+ " ) and t.is_dir=1 and t.role_id not in (select t.role_id from jecn_role_info t"
						+ " connect by prior t.role_id = t.per_id START WITH t.role_id in "
						+ JecnCommonSql.getIds(listIds) + " )";
			}
			List<Object[]> list = jecnRoleDao.listNativeSql(sql, 0, 20, "%" + name + "%");
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public JecnRoleInfo getJecnRoleInfoById(Long id) throws Exception {
		try {
			JecnRoleInfo jecnRoleInfo = jecnRoleDao.get(id);
			String sql = "SELECT a.ID,a.ROLE_ID,a.RELATE_ID,a.TYPE,"
					+ " CASE WHEN a.TYPE=0 THEN (SELECT b.FLOW_NAME FROM JECN_FLOW_STRUCTURE_T b WHERE b.FLOW_ID=a.RELATE_ID and b.DEL_STATE=0)"
					+ " WHEN a.TYPE=1 THEN (SELECT c.FILE_NAME FROM JECN_FILE_T c WHERE c.FILE_ID=a.RELATE_ID)"
					+ " WHEN a.TYPE=2 THEN (SELECT d.CRITERION_CLASS_NAME FROM JECN_CRITERION_CLASSES d WHERE d.CRITERION_CLASS_ID=a.RELATE_ID )"
					+ " WHEN a.TYPE=3 THEN (SELECT e.RULE_NAME FROM JECN_RULE_T e WHERE e.ID=a.RELATE_ID and e.DEL_STATE=0)"
					+ " WHEN a.TYPE=4 THEN (SELECT r.NAME FROM JECN_RISK r WHERE R.ID = a.RELATE_ID) "
					+ " WHEN a.TYPE=5 THEN (SELECT o.ORG_NAME FROM JECN_FLOW_ORG O WHERE O.ORG_ID = a.RELATE_ID) "
					+ "	WHEN a.TYPE=7 THEN (SELECT P.NAME FROM JECN_POSITION_GROUP P WHERE P.ID = a.RELATE_ID)"
					+ "  WHEN a.TYPE = 8 THEN (SELECT P.NAME FROM TERM_DEFINITION_LIBRARY P WHERE P.ID = a.RELATE_ID)"
					+ " END name " + " FROM JECN_ROLE_CONTENT a WHERE a.ROLE_ID=" + id;
			List<Object[]> listObjects = jecnRoleDao.listNativeSql(sql);
			List<JecnRoleContent> listJecnRoleContents = new ArrayList<JecnRoleContent>();
			for (Object[] obj : listObjects) {
				if (obj[0] != null && obj[1] != null && obj[2] != null && obj[3] != null) {
					JecnRoleContent jecnRoleContent = new JecnRoleContent();
					jecnRoleContent.setId(Long.valueOf(obj[0].toString()));
					jecnRoleContent.setRoleId(Long.valueOf(obj[1].toString()));
					jecnRoleContent.setRelateId(Long.valueOf(obj[2].toString()));
					jecnRoleContent.setType(Integer.valueOf(obj[3].toString()));
					String name = "";
					if (obj[4] != null && !"".equals(obj[4].toString())) {
						name = obj[4].toString();
					}
					jecnRoleContent.setrName(name);
					listJecnRoleContents.add(jecnRoleContent);
				}
			}
			jecnRoleInfo.setListJecnRoleContent(listJecnRoleContents);
			return jecnRoleInfo;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public int getChildCount(Long id, Long projectId) throws Exception {
		try {
			String hql = "select count(*) from JecnRoleInfo where perId=? and projectId=?";
			return jecnRoleDao.countAllByParamsHql(hql, id, projectId);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public boolean getRepeatNameEidt(String newName, Long id, Long pid, Long projectId) throws Exception {
		try {
			String hql = "select count(*) from JecnRoleInfo where roleName=? and perId=? and roleId<>? and isDir=0 and projectId=?";
			return jecnRoleDao.countAllByParamsHql(hql, newName, pid, id, projectId) > 0 ? true : false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<JecnTreeBean> getPnodes(Long id, Long projectId) throws Exception {
		try {
			String sql = "";
			if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
				sql = "WITH MY_JECN AS(SELECT * FROM jecn_role_info JFF WHERE JFF.role_id ="
						+ id
						+ " UNION ALL"
						+ " SELECT JFS.* FROM MY_JECN INNER JOIN jecn_role_info JFS ON MY_JECN.per_id=JFS.role_id)"
						+ " select t.role_id,t.role_name,t.per_id,t.is_dir,(select count(*) from jecn_role_info where per_id=t.role_id)"
						+ " from jecn_role_info t where t.project_id = " + projectId
						+ " and t.role_id not in (select tt.role_id from jecn_role_info tt where tt.filter_id in "
						+ JecnCommonSql.getDefaultAuthString()
						+ " ) and (t.per_id in (select role_id from MY_JECN) or t.per_id=0) and t.per_id<>" + id
						+ " order by t.per_id,t.sort_id,t.role_id";
				List<Object[]> list = jecnRoleDao.listNativeSql(sql);
				return findByListObjects(list);
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "select t.role_id, t.role_name,t.per_id,t.is_dir,(select count(*) from jecn_role_info where per_id=t.role_id)"
						+ " from jecn_role_info t where t.project_id = "
						+ projectId
						+ " and t.role_id not in (select tt.role_id from jecn_role_info tt where tt.filter_id in "
						+ JecnCommonSql.getDefaultAuthString()
						+ " )  and (t.per_id  in (select t.role_id from jecn_role_info t"
						+ " connect by prior t.per_id = t.role_id START WITH t.role_id = "
						+ id
						+ " ) or t.per_id=0) and t.per_id<>" + id + " order by t.per_id,t.sort_id,t.role_id";
				List<Object[]> list = jecnRoleDao.listNativeSql(sql);
				return findByListObjects(list);
			} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
				return this.getAllRoles(projectId);
			}
			return null;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public boolean isViewAuth(WebLoginBean webLoginBean, String requestName, HttpServletRequest request)
			throws Exception {
		try {

			if ("process".equals(requestName) || "processLook".equals(requestName)) {
				if ("public".equals(request.getParameter("reqType"))) {
					return true;
				}
				// 流程地图节点是否全部公开
				// 流程地图
				if ("processMap".equals(request.getParameter("type"))) {
					if (JecnContants.isMainFlowAdmin) {
						return true;
					}
					// 关键字
					if (request.getParameter("flowId") != null) {
						Long flowId = Long.valueOf(request.getParameter("flowId"));
						if (this.isAccessPermissionsFlow(flowId, webLoginBean.getListPosIds(), webLoginBean
								.getListOrgIds())) {
							return true;
						}
					}
					// 流程图
				} else if ("process".equals(request.getParameter("type"))) {

					// 流程节点是否全部公开
					if (JecnContants.isFlowAdmin) {
						return true;
					}

					if (StringUtils.isNotEmpty(request.getParameter("flowId"))) {
						Long flowId = Long.valueOf(request.getParameter("flowId"));
						if (this.isAccessPermissionsFlow(flowId, webLoginBean.getListPosIds(), webLoginBean
								.getListOrgIds())) {
							return true;
						}
					}
					return false;

				}

				return false;
			}// 组织
			else if ("organization".equals(requestName) || "organizationLook".equals(requestName)) {
				// 组织节点是否全部公开
				if (JecnContants.isOrgAdmin) {
					return true;
				}

				if (request.getParameter("orgId") != null) {
					Long orgId = Long.valueOf(request.getParameter("orgId"));
					if (webLoginBean.getListOrgIds().contains(orgId)) {
						return true;
					}
				}
				return false;
			}// 岗位
			else if ("orgSpecification".equals(requestName) || "stationLook".equals(requestName)) {
				// 岗位节点是否全部公开
				if (JecnContants.isStationAdmin) {
					return true;
				}
				if (request.getParameter("posId") != null) {
					Long posId = Long.valueOf(request.getParameter("posId"));
					if (webLoginBean.getListPosIds().contains(posId)) {
						return true;
					}
				}
				return false;
			}// 制度
			else if ("rule".equals(requestName)) {
				// 制度节点是否全部公开
				if (JecnContants.isSystemAdmin) {
					return true;
				}
				if (request.getParameter("ruleId") != null) {
					if ("false".equals(request.getParameter("isPub"))) {
						return true;
					}
					Long ruleId = Long.valueOf(request.getParameter("ruleId"));
					// public 和 secret
					if (request.getParameter("reqType") != null) {
						String reqType = request.getParameter("reqType").trim();
						if ("public".equals(reqType)) {
							if (request.getParameter("from") != null) {
								String from = request.getParameter("from");
								if (from.equals("process")) {
									if (JecnContants.sysPubItemMap.get(
											ConfigItemPartMapMark.permissionFlowRelateRule.toString()).equals("1")) {
										return true;
									}
								} else if (from.equals("processMap")) {
									if (JecnContants.sysPubItemMap.get(
											ConfigItemPartMapMark.permissionFlowMapRelateRule.toString()).equals("1")) {
										return true;
									}
								}
							}

						}
					}
					// 密级
					Rule rule = (Rule) this.jecnRoleDao.getSession().get(Rule.class, ruleId);
					if (rule != null && rule.getIsPublic() != null && rule.getIsPublic().intValue() == 1) {
						return true;
					} else {
						// 查阅权限（岗位）
						if (this.isAccessPermissions(ruleId, 3, webLoginBean.getListPosIds())) {
							return true;
						}
						// 查阅权限（部门）
						if (this.isAccessOrgPermissions(ruleId, 3, webLoginBean.getListOrgIds())) {
							return true;
						}
					}
				}
				return false;
			}// 标准
			else if ("standard".equals(requestName)) {
				// 标准节点是否全部公开
				if (JecnContants.isCriterionAdmin) {
					return true;
				}
				if (request.getParameter("standardId") != null) {
					if (request.getParameter("isPub") != null && request.getParameter("isPub").equals("false")) {
						return true;
					}
					Long standardId = Long.valueOf(request.getParameter("standardId"));
					if (request.getParameter("reqType") != null) {
						String reqType = request.getParameter("reqType").trim();
						if ("public".equals(reqType)) {
							if (request.getParameter("from") != null) {
								String from = request.getParameter("from");
								if (from.equals("process")) {
									if (JecnContants.sysPubItemMap.get(
											ConfigItemPartMapMark.permissionFlowRelateStandard.toString()).equals("1")) {
										return true;
									}
								} else if (from.equals("rule")) {
									if (JecnContants.sysPubItemMap.get(
											ConfigItemPartMapMark.permissionRuleRelateStandard.toString()).equals("1")) {
										return true;
									}
								}
							}
						}
					}
					if (standardId == 4L || standardId == 5L) {
						return true;
					}
					// 密级
					StandardBean standardBean = (StandardBean) this.jecnRoleDao.getSession().get(StandardBean.class,
							standardId);
					if (standardBean != null && standardBean.getIsPublic() != null
							&& standardBean.getIsPublic().intValue() == 1) {
						return true;
					} else {
						// 查阅权限（岗位）
						if (this.isAccessPermissions(standardId, 2, webLoginBean.getListPosIds())) {
							return true;
						}
						// 查阅权限（部门）
						if (this.isAccessOrgPermissions(standardId, 2, webLoginBean.getListOrgIds())) {
							return true;
						}
					}
				}
				return false;
			}// 文件 downloadFile文件下载getFileInformation文件详情
			else if ("downloadFile".equals(requestName) || "file".equals(requestName)) {
				// 记录查看和下载记录日志
				logFileDownload(webLoginBean, request);
				// 文件节点是否全部公开或者不需要验证权限 false为不需要 全称为isNeedPermission
				if (JecnContants.isFileAdmin || "false".equals(request.getParameter("isPerm"))) {
					return true;
				}
				if (request.getParameter("fileId") != null) {
					// isPub='false' 时，为任务审批，不需要判断文件权限
					if (request.getParameter("isPub") != null && request.getParameter("isPub").equals("false")) {
						return true;
					}
					Long fileId = Long.valueOf(request.getParameter("fileId"));
					if (request.getParameter("reqType") != null) {
						String reqType = request.getParameter("reqType").trim();
						if ("public".equals(reqType)) {
							if (request.getParameter("from") != null) {
								String from = request.getParameter("from");
								if (from.equals("process")) {
									if (JecnContants.sysPubItemMap.get(
											ConfigItemPartMapMark.permissionFlowRelateFile.toString()).equals("1")) {
										return true;
									}
								} else if (from.equals("processMap")) {
									if (JecnContants.sysPubItemMap.get(
											ConfigItemPartMapMark.permissionFlowMapRelateFile.toString()).equals("1")) {
										return true;
									}
								} else if (from.equals("rule")) {
									if (JecnContants.sysPubItemMap.get(
											ConfigItemPartMapMark.permissionRuleRelateFile.toString()).equals("1")) {
										return true;
									}
								}
							}
						}
					}
					// 密级
					JecnFileBean jecnFileBean = (JecnFileBean) this.jecnRoleDao.getSession().get(JecnFileBean.class,
							fileId);
					if (jecnFileBean != null && jecnFileBean.getIsPublic() != null
							&& jecnFileBean.getIsPublic().intValue() == 1) {
						return true;
					} else {
						// 查阅权限（岗位）
						if (this.isAccessPermissions(fileId, 1, webLoginBean.getListPosIds())) {
							return true;
						}
						// 查阅权限（部门）
						if (this.isAccessOrgPermissions(fileId, 1, webLoginBean.getListOrgIds())) {
							return true;
						}
					}
				}
				return false;
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return true;
	}

	/**
	 * 记录文件下载日志
	 */
	private void logFileDownload(WebLoginBean webLoginBean, HttpServletRequest request) {
		if (request.getParameter("fileId") == null) {
			return;
		}
		FileDownloadLog downloadLog = new FileDownloadLog();
		downloadLog.setFileId(Long.valueOf(request.getParameter("fileId").toString()));
		// 获得当前登录信息
		downloadLog.setPeopleId(webLoginBean.getJecnUser().getPeopleId());
		downloadLog.setCreateDate(new Date());
		// 0：文件；1：制度
		downloadLog.setLoadType(request.getParameter("from") != null
				&& "rule".equals(request.getParameter("from").toCharArray()) ? 1 : 0);
		jecnRoleDao.getSession().save(downloadLog);
	}

	/**
	 * 判断流程或流程地图是否有访问权限
	 * 
	 * @param flowId
	 * @param flowIdsStation
	 * @return
	 */
	private boolean isAccessPermissionsFlow(Long flowId, Set<Long> setStations, Set<Long> setOrgs) {
		try {
			JecnFlowStructure jecnFlowStructure = (JecnFlowStructure) this.jecnRoleDao.getSession().get(
					JecnFlowStructure.class, flowId);
			if (jecnFlowStructure != null && jecnFlowStructure.getIsPublic() != null
					&& jecnFlowStructure.getIsFlow() != null) {
				// 流程是公开
				if (jecnFlowStructure.getIsPublic().intValue() == 1) {
					return true;
				}
				// 是不是流程
				if (jecnFlowStructure.getIsFlow().intValue() == 1) {
					if (setStations.size() > 0) {
						String sql = "select count(jfs.flow_id) from jecn_flow_structure_image jfs"
								+ "       where jfs.flow_id=? and (jfs.figure_id in"
								+ "        (select psr.figure_flow_id from process_station_related psr where psr.type='0' and psr.figure_position_id in "
								+ JecnCommonSql.getIdsSet(setStations)
								+ ")"
								+ "       or jfs.figure_id in"
								+ "          (select psr.figure_flow_id from process_station_related psr,jecn_position_group_r jpgr"
								+ "                 where psr.type='1' and psr.figure_position_id=jpgr.group_id and jpgr.figure_id in "
								+ JecnCommonSql.getIdsSet(setStations) + "))";
						// 流程是否是岗位参与的流程
						if (jecnRoleDao.countAllByParamsNativeSql(sql, flowId) > 0) {
							return true;
						}
					}
				}
				// 查阅权限（岗位）
				if (this.isAccessPermissions(flowId, 0, setStations)) {
					return true;
				}
				// 查阅权限（部门）
				if (this.isAccessOrgPermissions(flowId, 0, setOrgs)) {
					return true;
				}
			}

		} catch (Exception e) {
			log.error("", e);
		}
		return false;
	}

	/**
	 * 是否有查阅权限(岗位)
	 * 
	 * @param relateId
	 * @param type
	 * @param listStations
	 *            岗位集合
	 * @return
	 */
	private boolean isAccessPermissions(Long relateId, int type, Set<Long> setStations) {
		try {
			if (setStations.size() > 0) {
				String sql = "SELECT COUNT(*)" + "  FROM (SELECT AP.FIGURE_ID"
						+ "          FROM JECN_ACCESS_PERMISSIONS AP"
						+ "         INNER JOIN JECN_FLOW_ORG_IMAGE FOI ON AP.FIGURE_ID ="
						+ "                                               FOI.FIGURE_ID"
						+ "         INNER JOIN JECN_FLOW_ORG FO ON FOI.ORG_ID = FO.ORG_ID"
						+ "                                    AND FO.DEL_STATE = 0"
						+ "                                    AND AP.TYPE = " + type
						+ "                                    AND AP.RELATE_ID = " + relateId + "        UNION "
						+ "        SELECT PGR.FIGURE_ID" + "          FROM JECN_GROUP_PERMISSIONS GP"
						+ "         INNER JOIN JECN_POSITION_GROUP_R PGR ON GP.POSTGROUP_ID ="
						+ "                                                 PGR.GROUP_ID"
						+ "                                             AND GP.TYPE = " + type
						+ "                                             AND GP.RELATE_ID = " + relateId
						+ ") TMP_FIGURE" + " WHERE TMP_FIGURE.FIGURE_ID IN " + JecnCommonSql.getIdsSet(setStations);
				if (this.jecnRoleDao.countAllByParamsNativeSql(sql) > 0) {
					return true;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("判断搜索查阅权限是否出错！", e);
		}

		return false;
	}

	/**
	 * 是否有查阅权限(部门)
	 * 
	 * @param relateId
	 * @param type
	 * @param listOrgs
	 * @return
	 */
	private boolean isAccessOrgPermissions(Long relateId, int type, Set<Long> setOrgs) {
		try {
			if (setOrgs.size() > 0) {
				if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
					String sql = "WITH MY_ORG AS(SELECT * FROM JECN_FLOW_ORG JFT WHERE JFT.ORG_ID in"
							+ " (select j.org_id from jecn_org_access_permissions j,jecn_flow_org f where "
							+ " j.org_id = f.org_id and f.del_state=0 and j.type=?" + " and j.relate_id=?)"
							+ " UNION ALL"
							+ " SELECT JFS.* FROM MY_ORG INNER JOIN JECN_FLOW_ORG JFS ON MY_ORG.ORG_ID=JFS.PER_ORG_ID)"
							+ " select count(*) from jecn_flow_org jfo " + " where jfo.del_state=0 and jfo.org_id in"
							+ JecnCommonSql.getIdsSet(setOrgs) + " and jfo.org_id in (select org_id from MY_ORG)";
					if (this.jecnRoleDao.countAllByParamsNativeSql(sql, type, relateId) > 0) {
						return true;
					}
				} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
					String sql = "select count(*) from jecn_flow_org jfo "
							+ " where jfo.del_state=0 and jfo.org_id in"
							+ JecnCommonSql.getIdsSet(setOrgs)
							+ " and jfo.org_id in (SELECT T.ORG_ID FROM JECN_FLOW_ORG T"
							+ " CONNECT BY PRIOR T.ORG_ID = T.Per_Org_Id"
							+ " START WITH T.ORG_ID in (select j.org_id from jecn_org_access_permissions j,jecn_flow_org f where "
							+ " j.org_id = f.org_id and f.del_state=0 and j.type=? and j.relate_id=? ))";
					if (this.jecnRoleDao.countAllByParamsNativeSql(sql, type, relateId) > 0) {
						return true;
					}
				}
			}

		} catch (Exception e) {
			log.error("判断搜索查阅权限是否出错！", e);
		}
		return false;
	}

	@Override
	public int getTotalRole(String roleName, String roleType) throws Exception {
		try {
			String sql = "select count(jri.role_id) from jecn_role_info jri where jri.is_dir=0";
			if (StringUtils.isNotBlank(roleName)) {
				sql = sql + " and jri.role_name like '%" + roleName + "%'";
			}
			if (StringUtils.isNotBlank(roleType)) {
				sql = sql + " and jri.filter_id ='" + roleType + "'";
			}
			return this.jecnRoleDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<RoleWebInfoBean> getRoleList(String roleName, String roleType, int start, int limit) throws Exception {
		try {
			String sql = "select jri.role_id,jri.role_name,jri.role_remark from jecn_role_info jri where jri.is_dir=0";
			if (StringUtils.isNotBlank(roleName)) {
				sql = sql + " and jri.role_name like '%" + roleName + "%'";
			}
			if (StringUtils.isNotBlank(roleType)) {
				sql = sql + " and jri.filter_id ='" + roleType + "'";
			}
			sql = sql + " order by jri.per_id,jri.sort_id,jri.role_id";
			List<Object[]> listObj = this.jecnRoleDao.listNativeSql(sql, start, limit);
			List<RoleWebInfoBean> listResult = new ArrayList<RoleWebInfoBean>();
			for (Object[] obj : listObj) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				RoleWebInfoBean roleWebInfoBean = new RoleWebInfoBean();
				roleWebInfoBean.setRoleId(Long.valueOf(obj[0].toString()));
				if (obj[1] != null) {
					roleWebInfoBean.setRoleName(obj[1].toString());
				}
				if (obj[2] != null) {
					roleWebInfoBean.setRoleRemark(obj[2].toString());
				}
				listResult.add(roleWebInfoBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public Long updateRoleSecondAdmin(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception {
		// // 0是流程、1是文件、2是标准、3是制度、4是风险、5组织； 6 二级管理员,7.岗位组
		String peopleIds = mapIds.get(6);
		mapIds.remove(6);
		Long roleId = this.updateRole(jecnRoleInfo, mapIds, updatePersionId);
		// 删除 历史关联的角色
		this.jecnRoleDao.deleteUserRoleByRoleId(roleId);
		addRoleUser(peopleIds, roleId);
		return roleId;
	}

	@Override
	public Long addRoleSecondAdmin(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception {
		// 0是流程、1是文件、2是标准、3是制度、4是风险、5组织； 6 二级管理员
		String peopleIds = mapIds.get(6);
		mapIds.remove(6);
		Long roleId = this.addRole(jecnRoleInfo, mapIds, updatePersionId);
		addRoleUser(peopleIds, roleId);
		return roleId;
	}

	/**
	 * 添加角色
	 * 
	 * @param peopleIds
	 * @param roleId
	 * @throws Exception
	 */
	private void addRoleUser(String peopleIds, Long roleId) throws Exception {
		if (StringUtils.isEmpty(peopleIds)) {
			return;
		}
		String[] strIds = peopleIds.split(",");
		if (JecnContants.isAdminKey) {
			int adminTotal = jecnRoleDao.getDesignerUserTotal(JecnCommonSql.getAdminAuthString());
			if ((adminTotal + strIds.length) > JecnContants.adminInt) {// 管理员总数注册总数
				throw new IllegalArgumentException("管理员总数已达上限！");
			}
		} else {
			int secondAdminTotal = jecnRoleDao.getDesignerUserTotal("('" + JecnCommonSql.getStrSecondAdmin() + "')");
			if ((secondAdminTotal + strIds.length) > JecnContants.secondInt) {// 二级管理员总数超出注册总数
				throw new IllegalArgumentException("二级管理员总数已达上限！");
			}
		}

		UserRole userRole = null;
		// 保存人员和角色关联表
		for (String string : strIds) {
			userRole = new UserRole();
			userRole.setPeopleId(Long.valueOf(string));
			userRole.setRoleId(roleId);
			jecnRoleDao.addUserRole(userRole);
		}
	}

	@Override
	public List<Object[]> selectUserRoleByRoleId(Long roleId) throws Exception {
		return jecnRoleDao.selectUserRoleByRoleId(roleId);
	}

	/**
	 * 秘秘钥到期，删除过期角色
	 * 
	 * @throws Exception
	 */
	@Override
	public void saveUserRoleLog() throws Exception {
		List<Long> listDesigners = null;
		List<Long> listAdmins = null;
		int delAdmins = 0;
		int delDesigners = 0;
		if (JecnContants.isAdminKey) {
			// （admin，design对应角色和人员关联表主键ID；总数 = 角色数）
			listDesigners = jecnRoleDao.getDesignerAuths("('" + JecnCommonSql.getStrDesignAdmin() + "')");
			// 二级管理员对应关联表主键ID集合
			listAdmins = jecnRoleDao.getDesignerAuths(JecnCommonSql.getAdminAuthString());
			delAdmins = listAdmins.size() - JecnContants.adminInt;
			delDesigners = listDesigners.size() - JecnContants.userInt;
		} else {
			// （admin，design对应角色和人员关联表主键ID；总数 = 角色数）
			listDesigners = jecnRoleDao.getDesignerAuths(JecnCommonSql.getDesignDefaultAuthString());
			// 二级管理员对应关联表主键ID集合
			listAdmins = jecnRoleDao.getDesignerAuths("('" + JecnCommonSql.getStrSecondAdmin() + "')");
			delAdmins = listAdmins.size() - JecnContants.secondInt;
			delDesigners = listDesigners.size() - JecnContants.userInt;
		}

		// 验证二级管理员总数是否超出注册秘钥中值
		List<Long> deleteUserRole = new ArrayList<Long>();
		if (delAdmins > 0) {
			for (int i = 0; i < delAdmins; i++) {
				deleteUserRole.add(listAdmins.get(i));
			}
		}

		if (delDesigners > 0) {
			for (int i = 0; i < delDesigners; i++) {
				deleteUserRole.add(listDesigners.get(i));
			}
		}

		if (deleteUserRole.isEmpty()) {
			return;
		}
		// 记录删除的角色记录
		jecnRoleDao.insertUserRoleLog(deleteUserRole);
		// 删除过期角色用户
		jecnRoleDao.deleteUserRoleByIds(deleteUserRole);
	}

	@Override
	public Long getSecondAdminRoleId(Long peopleId) throws Exception {
		return jecnRoleDao.getSecondAdminRoleId(peopleId);
	}

	/**
	 * 用户是否存在登录设计器的角色权限：true 存在
	 * 
	 * @param peopleId
	 * @return boolean
	 * @throws Exception
	 */
	@Override
	public boolean isRoleAuthDesigner(Long peopleId) throws Exception {
		int roleCount = this.jecnRoleDao.getRoleAuthDesignerCount(peopleId);
		return roleCount > 0 ? true : false;
	}
}
