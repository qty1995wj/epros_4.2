package com.jecn.epros.server.bean.system;

import java.io.Serializable;

/**
 * 版本信息Bean
 * @author fuzhh Apr 9, 2013
 *
 */
public class JecnVersionSys implements Serializable {
	
	/** 版本号 */
	private String version;
	/** 发布日期 */
	private String pubtime;
	/** 更新序号 */
	private String updateNum;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPubtime() {
		return pubtime;
	}

	public void setPubtime(String pubtime) {
		this.pubtime = pubtime;
	}

	public String getUpdateNum() {
		return updateNum;
	}

	public void setUpdateNum(String updateNum) {
		this.updateNum = updateNum;
	}
}
