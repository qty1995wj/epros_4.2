package com.jecn.epros.service.task;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.common.abolish.AbolishBean;
import com.jecn.epros.domain.task.*;

import java.util.List;
import java.util.Map;

public interface TaskService {

    public int getTaskCountBySearch(TaskSearchTempBean params, Long curPeopleId, Boolean isAdmin,
                                    Boolean isSecondAdmin);

    public List<MyTaskBean> fetchTasks(TaskSearchTempBean params, Paging paging, Long curPeopleId, Boolean isAdmin,
                                       Boolean isSecondAdmin);

    public void replaceApprovePeople(Long fromPeopleId, Long toPeopleId);

    public int getMyTaskCount(TaskSearchTempBean searchParam, Long peopleId);

    public List<MyTaskBean> fetchMyTasks(TaskSearchTempBean searchParam, Long loginPeopleId, Paging pagging);

    public SimpleTaskBean getSimpleTaskBean(Long taskId, ResourceType operation,String isApp);

    public void taskOperation(SimpleSubmitMessage submitMessage);

    public void markTaskToDelete(Long taskId);

    public boolean hasApproveTask(Long valueOf, Long curPeopleId, Integer taskStage);

    /**
     * 当当前处理人为拟稿人是判断是否离职
     *
     * @param taskId
     * @return boolean true 未离职 false 离职 @
     */
    public boolean createPeopleHandleIsNotLeaveOffice(String taskId);

    /**
     * 流程建设规范
     *
     * @param processId
     * @return true 没有错误 false有错误
     */
    public boolean flowNorms(Long processId);

    TaskTestRunFile downloadTaskRunFile(Long id);

    List<AbolishBean> findAbolish(Map<String ,Object> map);

    int findCount(Map<String ,Object> map);

    ResourceType getTaskBean(Long id);
}
