package epros.draw.data.figure;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.line.JecnBaseDivedingLineData;

/**
 * 注释框数据层
 * 
 * @author Administrator
 * @date： 日期：Dec 15, 2012 时间：2:33:32 PM
 */
public class JecnCommentLineData extends JecnFigureData {
	/** 注释框小线段数据层 */
	private JecnBaseDivedingLineData lineData = null;

	public JecnCommentLineData(MapElemType mapElemType) {
		super(mapElemType);
	}

	public JecnBaseDivedingLineData getLineData() {
		return lineData;
	}

	public void setLineData(JecnBaseDivedingLineData lineData) {
		this.lineData = lineData;
	}

	public double getStartX() {
		return lineData.getDiviLineCloneable().getStartXY().getX();
	}

	public double getStartY() {
		return lineData.getDiviLineCloneable().getStartXY().getY();
	}

	public double getEndX() {
		return lineData.getDiviLineCloneable().getEndXY().getX();
	}

	public double getEndY() {
		return lineData.getDiviLineCloneable().getEndXY().getY();
	}

	@Override
	/**
	 * 
	 * 实例化对象
	 * 
	 */
	public JecnCommentLineData newInstance() {
		return new JecnCommentLineData(mapElemType);
	}

	/**
	 * 
	 * 
	 * 复制流程元素属性
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(
			JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		if (!(srcFlowElementData instanceof JecnCommentLineData)
				|| !(destFlowElementData instanceof JecnCommentLineData)) {
			return;
		}

		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		JecnCommentLineData src = (JecnCommentLineData) srcFlowElementData;
		JecnCommentLineData dest = (JecnCommentLineData) destFlowElementData;
		dest.lineData = src.lineData.clone();
	}
}
