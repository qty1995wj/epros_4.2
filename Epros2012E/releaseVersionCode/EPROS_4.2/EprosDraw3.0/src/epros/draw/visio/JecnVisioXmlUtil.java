package epros.draw.visio;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * 获取visioXml数据包，根据Dom4J
 * 
 * @author fuzhh 2013-6-8
 * 
 */
public class JecnVisioXmlUtil {
	private static final Log log = LogFactory.getLog(JecnVisioXmlUtil.class);
	/**
	 * 获取 xml Documet
	 * 
	 * @author fuzhh Apr 23, 2013
	 * @param path
	 *            文件路径
	 * @return
	 */
	public static Document getDocumentByXml(String path) {
		SAXReader reader = new SAXReader();
		try {
			if (path == null) {
				return null;
			}
			Document doc = reader.read(new File(path));
			return doc;
		} catch (DocumentException e) {
			log.error("", e);
			return null;
		}
	}

	/**
	 * 获取根节点
	 * 
	 * @author fuzhh 2013-5-22
	 * @param document
	 * @return
	 */
	public static Element getRootElement(Document document) {
		if (document == null) {
			return null;
		}
		return document.getRootElement();
	}

	/**
	 * 获取当前节点下的子节点
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            当前节点
	 * @param nodeName
	 *            需要获取的节点名称
	 * @return
	 */
	public static Element getChildNodes(Element element, String nodeName) {
		if (element == null || nodeName == null) {
			return null;
		}
		return element.element(nodeName);
	}

	/**
	 * 获取传入节点的文字
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            节点的Element对象
	 * @return
	 */
	public static String getNodeValue(Element element) {
		if (element == null) {
			return null;
		}
		return element.getText();
	}

	/**
	 * 取得某节点下名为 nodeName 的所有子节点并进行遍历
	 * 
	 * @author fuzhh 2013-5-22
	 * @param Element
	 *            当前节点的Element对象
	 * @param nodeName
	 *            节点名称
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Element> getNodesList(Element elemt, String nodeName) {
		if (elemt == null || nodeName == null) {
			return null;
		}
		List<Element> elementList = new ArrayList<Element>();
		List nodes = elemt.elements(nodeName);
		for (Iterator it = nodes.iterator(); it.hasNext();) {
			Element elm = (Element) it.next();
			elementList.add(elm);
		}
		return elementList;
	}

	/**
	 * 获取节点下的子节点
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            节点名称
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Element> getChildNodesList(Element nodeElement) {
		if (nodeElement == null) {
			return null;
		}
		List<Element> elementList = nodeElement.elements();
		return elementList;
	}

	/**
	 * 递归查询节点下所有的子节点
	 * 
	 * @author fuzhh 2013-5-23
	 * @param element
	 *            节点
	 * @param allEleList
	 *            节点下所有节点
	 * @return
	 */
	public static void getAllChildNodeList(Element nodeElement,
			List<Element> allEleList) {
		if (nodeElement == null || allEleList == null) {
			return;
		}
		List<Element> eleList = getChildNodesList(nodeElement);
		if (eleList.size() > 0) {
			for (Element ele : eleList) {
				allEleList.add(ele);
				getAllChildNodeList(ele, allEleList);
			}
		}
	}

	/**
	 * 递归查询节点下的某个节点的集合
	 * 
	 * @author fuzhh 2013-5-23
	 * @param element
	 *            节点
	 * @param allEleList
	 *            节点下所有节点
	 * @param nodeName
	 *            节点名称
	 * @return
	 */
	public static void getAllChildNodeList(Element nodeElement,
			List<Element> allEleList, String nodeName) {
		if (nodeElement == null || allEleList == null || nodeName == null) {
			return;
		}
		List<Element> eleList = getChildNodesList(nodeElement);
		if (eleList != null && eleList.size() > 0) {
			for (Element ele : eleList) {
				if (nodeName.equals(ele.getName())) {
					allEleList.add(ele);
				}
				getAllChildNodeList(ele, allEleList, nodeName);
			}
		}
	}

	/**
	 * 获取节点下指点属性
	 * 
	 * @author fuzhh 2013-6-9
	 * @param nodeElement
	 *            节点名称
	 * @param attName
	 *            属性名称
	 * @return
	 */
	public static String getNodeAttValue(Element nodeElement, String attName) {
		return getAttValue(getNodeAtt(nodeElement, attName));
	}

	/**
	 * 获取节点下的属性
	 * 
	 * @author fuzhh 2013-5-22
	 * @param element
	 *            节点名称
	 * @param attName
	 *            属性名称
	 * @return
	 */
	public static Attribute getNodeAtt(Element nodeElement, String attName) {
		if (nodeElement == null || attName == null) {
			return null;
		}
		Attribute attribute = nodeElement.attribute(attName);
		return attribute;
	}

	/**
	 * 获取属性文字
	 * 
	 * @author fuzhh 2013-5-22
	 * @param attribute
	 *            属性
	 * @return
	 */
	public static String getAttValue(Attribute attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getText();
	}

	/**
	 * 获取 节点下的所有属性
	 * 
	 * @author fuzhh 2013-5-22
	 * @param ele
	 *            节点名称
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Attribute> getAttrubuteList(Element ele) {
		if (ele == null) {
			return null;
		}
		List<Attribute> attriList = new ArrayList<Attribute>();
		for (Iterator it = ele.attributeIterator(); it.hasNext();) {
			Attribute attribute = (Attribute) it.next();
			attriList.add(attribute);
		}
		return attriList;
	}

	/**
	 * 判断节点下是否存在此节点
	 * 
	 * @author fuzhh 2013-5-29
	 * @param selectEle
	 *            节点Ele对象
	 * @param nodeName
	 *            需要判断的节点名称
	 * @return
	 */
	public static boolean isExistNode(Element selectEle, String nodeName) {
		if (nodeName == null) {
			return false;
		}
		List<Element> eleList = new ArrayList<Element>();
		getAllChildNodeList(selectEle, eleList);
		for (Element ele : eleList) {
			if (nodeName.equals(ele.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 通过节点名称判断节点下是否存在节点
	 * 
	 * @author fuzhh 2013-5-29
	 * @param nodeEleList
	 *            需要判断的节点集合
	 * @param nodeName
	 *            节点名称
	 * @return
	 */
	public static Element getExistNode(List<Element> nodeEleList,
			String nodeName) {
		if (nodeEleList == null || nodeName == null) {
			return null;
		}
		// 循环查找存在 JecnTitle 的节点
		for (Element ele : nodeEleList) {
			boolean isEle = isExistNode(ele, nodeName);
			if (isEle) {
				return ele;
			}
		}
		return null;
	}

	/**
	 * 通过节点名称判断节点下是否存在多个节点
	 * 
	 * @author fuzhh 2013-5-29
	 * @param nodeEleList
	 *            需要判断的节点集合
	 * @param nodeNames
	 *            节点数组
	 * @return
	 */
	public static Element getExistNode(List<Element> nodeEleList,
			String[] nodeNames) {
		if (nodeEleList == null || nodeNames == null) {
			return null;
		}
		// 循环查找存在 JecnTitle 的节点
		for (Element ele : nodeEleList) {
			boolean isExist = true;
			for (String nodeName : nodeNames) {
				boolean isEle = isExistNode(ele, nodeName);
				if (!isEle) {
					isExist = false;
					break;
				}
			}
			if (isExist) {
				return ele;
			}
		}
		return null;
	}

	/**
	 * 通过节点名称判断节点下是否存在existNode节点不存在notNode 节点
	 * 
	 * @author fuzhh 2013-5-29
	 * @param nodeEleList
	 *            需要判断的节点集合
	 * @param existNode
	 *            存在的节点
	 * @param notNode
	 *            不存在的节点
	 * @return
	 */
	public static Element getExistNode(List<Element> nodeEleList,
			String existNode, String notNode) {
		if (nodeEleList == null) {
			return null;
		}
		// 循环查找存在 JecnTitle 的节点
		for (Element ele : nodeEleList) {
			boolean isExist = false;
			boolean isEle = isExistNode(ele, existNode);
			if (isEle) {
				boolean isExistEle = isExistNode(ele, notNode);
				if (!isExistEle) {
					isExist = true;
				}
			}
			if (isExist) {
				return ele;
			}
		}
		return null;
	}

}
