package com.jecn.webservice.zhongsh.flowpush.dao.impl;

import java.sql.Clob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.jecn.epros.server.bean.process.FlowFileInputi3GUIDBean;
import com.jecn.epros.server.bean.process.JecnFlowStructureImage;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.webservice.zhongsh.flowpush.bean.FlowElem;
import com.jecn.webservice.zhongsh.flowpush.dao.IShiHuaDao;

@Repository("shiHuaDao")
public class ShiHuaDaoImpl extends AbsBaseDao<JecnFlowStructureImage, Long> implements IShiHuaDao {

	/**
	 * 
	 * 根据流程ID获取所有的流程元素
	 * 
	 */
	@Override
	public List<FlowElem> findAllByFlowImageId(Long flowId) throws Exception {
		String sql ="SELECT JFSIT.FIGURE_ID," + 
					"       JFSIT.FIGURE_TYPE," + 
					"       JFSIT.FIGURE_TEXT," + 
					"       JFSIT.START_FIGURE," + 
					"       JFSIT.END_FIGURE," + 
					"       JFSIT.X_POINT," + 
					"       JFSIT.Y_POINT," + 
					"       JFSIT.WIDTH," + 
					"       JFSIT.HEIGHT," + 
					"       JFSIT.ACTIVITY_ID," + 
					"       JFSIT.ACTIVITY_SHOW," +
					"       JSI.FIGURE_TEXT," + 
					"       JRA.FIGURE_ROLE_ID," +
					"       JFSIT.CIRCUMGYRATE" +
					"  FROM JECN_FLOW_STRUCTURE_IMAGE JFSIT" + 
					"  LEFT JOIN JECN_ROLE_ACTIVE JRA" + 
					"  ON JRA.FIGURE_ACTIVE_ID = JFSIT.FIGURE_ID" + 
					"  LEFT  JOIN JECN_FLOW_STRUCTURE_IMAGE JSI" + 
					"  ON JSI.FIGURE_ID = JRA.FIGURE_ROLE_ID" + 
					"  WHERE JFSIT.FLOW_ID = ?";
		List<Object[]> objects = this.listNativeSql(sql, flowId);
		return getListFromObjs(objects);
	}

	private List<FlowElem> getListFromObjs(List<Object[]> objs) {
		List<FlowElem> elems = new ArrayList<FlowElem>();
		FlowElem elem = null;
		if (objs != null && objs.size() > 0) {
			for (Object[] obj : objs) {
				if (obj[0] == null || obj[1] == null || obj[5] == null || obj[6] == null) {
					continue;
				}
				elem = new FlowElem();
				elems.add(elem);
				if (obj[0] != null) {
					elem.setFigureId(Long.valueOf(obj[0].toString()));
				}
				if (obj[1] != null) {
					elem.setFigureType(obj[1].toString());
				}
				if (obj[2] != null) {
					elem.setFigureText(obj[2].toString());
				}
				if (obj[3] != null) {
					elem.setStartFigure(Long.valueOf(obj[3].toString()));
				}
				if (obj[4] != null) {
					elem.setEndFigure(Long.valueOf(obj[4].toString()));
				}
				if (isLineFigure(elem.getFigureType())) {// 如果是连线，获取连线连接元素的位置
					elem.setPoLongx(getSyncPositionXY(Integer.valueOf(obj[5].toString())));
					elem.setPoLongy(getSyncPositionXY(Integer.valueOf(obj[6].toString())));
				} else {//元素坐标
					elem.setPoLongx(Long.valueOf(obj[5].toString()));
					elem.setPoLongy(Long.valueOf(obj[6].toString()));
				}
				if (obj[7] != null) {
					elem.setWidth(Long.valueOf(obj[7].toString()));
				}
				if (obj[8] != null) {
					elem.setHeight(Long.valueOf(obj[8].toString()));
				}
				if (obj[9] != null) {
					elem.setActivityId(obj[9].toString());
				}
				if (obj[10] != null) {
					String activeShow = JecnCommonSql.clobToString((Clob) obj[10]);
					elem.setActivityShow(activeShow);
				}
				if (obj[11] != null) {
					elem.setRelatedRoleText(obj[11].toString());
				}
				if (obj[12] != null) {
					elem.setRelatedRoleId(Long.valueOf(obj[12].toString()));
				}
				if (obj[13] !=null){
					elem.setRevolve(Double.valueOf(obj[13].toString()));
				}
			}
		}
		return elems;
	}
	
	/**
	 * 
	 * i3系统位置坐标: 8:上、2：右、4：下、6：左
	 * 
	 * @param pXY
	 *            2:上、3：右、4：下、1：左
	 * @return
	 */
	private Long getSyncPositionXY(int pXY) {
		Long i3PositonXY = 0L;
		switch (pXY) {
		case 1:
			i3PositonXY = 6L;
			break;
		case 2:
			i3PositonXY = 8L;
			break;
		case 3:
			i3PositonXY = 2L;
			break;
		case 4:
			i3PositonXY = 4L;
			break;
		}
		return i3PositonXY;
	}
	
	/**
	 * 是否为连接线
	 * 
	 * @param figureType
	 * @return
	 */
	private boolean isLineFigure(String figureType) {
		return "ManhattanLine".equals(figureType) || "CommonLine".equals(figureType) ? true : false;
	}

	/**
	 * 获得流程的角色所关联的人员 key为角色的id value为人员的集合
	 */
	@Override
	public Map<Long, List<String>> findRoleRelatedPeopleId(Long flowId) throws Exception {
		String sql = "SELECT DISTINCT A.FIGURE_ID,A.PEOPLE_ID,A.Login_Name" + " FROM ("
				+ " SELECT JFSI.FIGURE_ID,JU.PEOPLE_ID,JU.Login_Name FROM JECN_FLOW_STRUCTURE_IMAGE JFSI"
				+ " LEFT JOIN PROCESS_STATION_RELATED PSR ON JFSI.FIGURE_ID=PSR.FIGURE_FLOW_ID AND PSR.TYPE=1"
				+ " LEFT JOIN JECN_POSITION_GROUP JPG ON JPG.ID=PSR.FIGURE_POSITION_ID"
				+ " LEFT JOIN JECN_POSITION_GROUP_R JPGR ON JPGR.GROUP_ID=JPG.ID"
				+ " LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFOI.FIGURE_ID=JPGR.FIGURE_ID"
				+ " LEFT JOIN JECN_USER_POSITION_RELATED JUPR ON JUPR.FIGURE_ID=JFOI.FIGURE_ID"
				+ " LEFT JOIN JECN_USER JU ON JU.PEOPLE_ID=JUPR.PEOPLE_ID"
				+ " WHERE JFSI.FIGURE_TYPE IN"+JecnCommonSql.getOnlyRoleString()+" AND JFSI.FLOW_ID=?" + " UNION"
				+ " SELECT JFSI.FIGURE_ID,JU.PEOPLE_ID,JU.Login_Name FROM JECN_FLOW_STRUCTURE_IMAGE JFSI"
				+ " LEFT JOIN PROCESS_STATION_RELATED PSR ON JFSI.FIGURE_ID=PSR.FIGURE_FLOW_ID AND PSR.TYPE=0"
				+ " LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFOI.FIGURE_ID=PSR.FIGURE_POSITION_ID"
				+ " LEFT JOIN JECN_USER_POSITION_RELATED JUPR ON JUPR.FIGURE_ID=JFOI.FIGURE_ID"
				+ " LEFT JOIN JECN_USER JU ON JU.PEOPLE_ID=JUPR.PEOPLE_ID"
				+ " WHERE JFSI.FIGURE_TYPE"+JecnCommonSql.getOnlyRoleString()+" AND JFSI.FLOW_ID=?" + ")A " 
				+ " WHERE A.LOGIN_NAME IS NOT NULL ORDER BY A.FIGURE_ID";
		List<Object[]> objs = this.listNativeSql(sql, flowId, flowId);
		return getMapFrom(objs);
	}

	private Map<Long, List<String>> getMapFrom(List<Object[]> objs) {
		Map<Long, List<String>> m = new HashMap<Long, List<String>>();
		Long figureId = -1L;
		for (Object[] obj : objs) {
			if (obj[0] != null) {
				figureId = Long.valueOf(obj[0].toString());
				if (!m.containsKey(figureId)) {
					m.put(figureId, new ArrayList<String>());
				}
				m.get(figureId).add(obj[2].toString());
			}
		}
		return m;
	}
	
	/**
	 * 根据流程ID获取流程基本信息
	 * @param flowId
	 * @return Object[]:0:流程名称；1：流程GUID
	 * @throws Exception
	 */
	@Override
	public Object[] getFlowInfoObjects(Long flowId) throws Exception {
		String sql = "SELECT T.FLOW_NAME, IG.GUID" + "  FROM JECN_FLOW_STRUCTURE T"
				+ "  LEFT JOIN FLOWFILE_INPUT_I3_GUID IG ON IG.FLOW_ID = T.FLOW_ID" + " WHERE T.FLOW_ID = ?";
		return this.getObjectNativeSql(sql, flowId);
	}

	@Override
	public void saveFlowFileInputi3GUIDBean(FlowFileInputi3GUIDBean inputi3guidBean) throws Exception {
		if (inputi3guidBean == null) {
			return;
		}
		this.getSession().save(inputi3guidBean);
	}
}
