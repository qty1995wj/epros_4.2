package com.jecn.epros.server.dao.standard;

import com.jecn.epros.server.bean.process.StandardFlowRelationTBean;
import com.jecn.epros.server.common.IBaseDao;

public interface IStandardFlowRelationDao extends IBaseDao<StandardFlowRelationTBean, Long> {

}
