package epros.designer.gui.task.config;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.task.TaskTemplet;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

public class TempletPanel extends JecnPanel {

	private static final long serialVersionUID = -8171984269044300497L;

	protected static Logger log = Logger.getLogger(TempletPanel.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(510, 190);// 510, 660
	/** center面板 */
	private JecnPanel centerPanel = new JecnPanel(510, 180);// /510, 505
	/** 顶部面板 */
	private JecnPanel templetPanel = new JecnPanel(500, 30);// 500, 30
	/** 模板选择 */
	private JLabel templetSelect = new JLabel(JecnProperties.getValue("modeSelect"));
	/** 下拉框选择 **/
	private JComboBox templetComboBox = new JComboBox();
	protected TempletConfigPanel taskEditPanel;

	/** 面板宽 */
	private int dialogWidth = 500;
	/** 面板高 */
	private int dialogHeigh = 500;

	protected int taskType = -1;
	/** 初始时候的模板id **/
	protected String oldSelectTempletId;
	/** 最新选中的模板id **/
	protected String curSelectTempletId;

	/** 任务模板下拉框数据 包含空选项 **/
	private List<TaskTemplet> templets = new ArrayList<TaskTemplet>();
	/** 选中的模板的任务配置项 **/
	private List<TaskConfigItem> configs = new ArrayList<TaskConfigItem>();
	/** 关联的文件id **/
	protected Long id;
	/** 0审批 1查阅 2废止 **/
	protected Integer templetType;
	protected String templetName;

	public TempletPanel(Long id, Integer taskType, Integer templetType) {
		this.id = id;
		this.taskType = taskType;
		this.templetType = templetType;
		if (templetType == 0) {
			templetName = JecnProperties.getValue("approvalModel");// 审批模板
		} else if (templetType == 1) {
			templetName = JecnProperties.getValue("borrowModel");// 借阅模板
		} else if (templetType == 2) {
			templetName = JecnProperties.getValue("abolishModel");// 废止模板
		}

		templetPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		initComponent();
		initData();
		initLayout();
		initAction();
	}

	public String getResult() {
		return this.curSelectTempletId;
	}

	public boolean isUpdate() {
		if (this.oldSelectTempletId == null && this.curSelectTempletId == null) {
			return false;
		}
		if (this.oldSelectTempletId != null && this.curSelectTempletId != null
				&& this.oldSelectTempletId.equals(this.curSelectTempletId)) {
			return false;
		}

		return true;
	}

	private void initComponent() {
		// 任务中间面板
		taskEditPanel = new TempletConfigPanel(this.configs);
	}

	public void initData() {
		try {
			this.curSelectTempletId = ConnectionPool.getTaskRecordAction().getTempletIdByTempletType(this.id,
					this.taskType, this.templetType);
			if (this.curSelectTempletId != null) {
				this.configs = ConnectionPool.getTaskRecordAction().listTaskConfigItemByTempletId(
						this.curSelectTempletId);
			}
		} catch (Exception e1) {
			log.error(e1);
		}
		try {
			templetComboBox.removeAllItems();
			templets.clear();
			// 空
			TaskTemplet emptyTemplet = new TaskTemplet();
			emptyTemplet.setName("    ");
			templets.add(emptyTemplet);
			// 该类型的模板 查询数据库
			templets.addAll(ConnectionPool.getTaskRecordAction().listTaskTempletsByType(taskType));

			// 由于重新添加下拉框节点会触发下拉框的选中action所以需要存放当前选中的模板id到临时值，之后再恢复值
			String tempCurSelectTempletId = this.curSelectTempletId;

			for (TaskTemplet templet : templets) {
				templetComboBox.addItem(templet.getName(JecnResourceUtil.getLocale() == Locale.CHINESE ? 0 : 1));
			}

			this.curSelectTempletId = tempCurSelectTempletId;
			int selectIndex = getSelectIndex(templets);
			if (selectIndex == 0) {
				buttonEditable(false);
			} else {
				buttonEditable(true);
			}
			updateTable(selectIndex);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
	}

	private void updateTable(int selectIndex) throws Exception {
		if (selectIndex == 0) {
			// 清空table
			this.configs.clear();
		} else {
			templetComboBox.setSelectedIndex(selectIndex);
			TaskTemplet taskTemplet = templets.get(selectIndex);
			this.configs = ConnectionPool.getTaskRecordAction().listTaskConfigItemByTempletId(taskTemplet.getId());
		}
		taskEditPanel.initData(this.configs);
	}

	/**
	 * @author yxw 2012-6-28
	 * @description:布局
	 */
	private void initLayout() {
		mainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), templetName));
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(
				3, 3, 0, 3), 0, 0);
		mainPanel.add(templetPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(3, 3, 0, 3), 0, 0);
		mainPanel.add(centerPanel, c);

		templetPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
				3, 3, 0, 3), 0, 0);
		templetPanel.add(templetSelect, c);

		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(3, 3, 0, 3), 0, 0);
		templetPanel.add(templetComboBox, c);

		centerPanel.setLayout(new GridLayout());
		centerPanel.add(taskEditPanel);
		this.add(mainPanel);
	}

	private void initAction() {

		templetComboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {

					int index = templetComboBox.getSelectedIndex();
					try {
						if (index == 0) {
							curSelectTempletId = null;
						} else {
							TaskTemplet taskTemplet = templets.get(index);
							curSelectTempletId = taskTemplet.getId();
						}
						if (index == 0) {
							buttonEditable(false);
						} else {
							buttonEditable(true);
						}
						updateTable(index);
					} catch (Exception e1) {
						log.error(e);
					}
				}
			}

		});

	}

	public void buttonEditable(boolean editable) {

	}

	/**
	 * 需要选中的下拉框的的index
	 * 
	 * @param templets
	 * @param selectTempletId
	 * @return
	 */
	private int getSelectIndex(List<TaskTemplet> templets) {
		int index = 0;
		int selectIndex = 0;
		for (TaskTemplet taskTemplet : templets) {
			if (taskTemplet.getId() != null && taskTemplet.getId().equals(this.curSelectTempletId)) {
				selectIndex = index;
				break;
			}
			index++;
		}
		return selectIndex;

	}

}
