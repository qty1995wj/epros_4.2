package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

public class XmlConfigBean {
	private Long userNumberId;
	private Long loginNameId;
	private Long passwordId;
	private Long trueNameId;
	private Long phoneId;
	private Long emailId;
	private Long positionId;
	private Long positionNameId;
	private Long deptId;
	private Long deptNameId;

	public Long getUserNumberId() {
		return userNumberId;
	}

	public void setUserNumberId(Long userNumberId) {
		this.userNumberId = userNumberId;
	}

	public Long getLoginNameId() {
		return loginNameId;
	}

	public void setLoginNameId(Long loginNameId) {
		this.loginNameId = loginNameId;
	}

	public Long getPasswordId() {
		return passwordId;
	}

	public void setPasswordId(Long passwordId) {
		this.passwordId = passwordId;
	}

	public Long getTrueNameId() {
		return trueNameId;
	}

	public void setTrueNameId(Long trueNameId) {
		this.trueNameId = trueNameId;
	}

	public Long getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(Long phoneId) {
		this.phoneId = phoneId;
	}

	public Long getEmailId() {
		return emailId;
	}

	public void setEmailId(Long emailId) {
		this.emailId = emailId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public Long getPositionNameId() {
		return positionNameId;
	}

	public void setPositionNameId(Long positionNameId) {
		this.positionNameId = positionNameId;
	}

	public Long getDeptNameId() {
		return deptNameId;
	}

	public void setDeptNameId(Long deptNameId) {
		this.deptNameId = deptNameId;
	}

}
