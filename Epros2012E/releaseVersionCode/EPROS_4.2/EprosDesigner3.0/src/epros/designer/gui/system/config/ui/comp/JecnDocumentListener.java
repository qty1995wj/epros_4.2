package epros.designer.gui.system.config.ui.comp;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 输入框事件
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDocumentListener implements DocumentListener {
	/** 输入框 */
	private JTextComponent textComp = null;
	/** 内网密码提示信息 */
	private JecnUserInfoTextArea infoTextArea = null;
	/** 主面板 */
	private JecnAbstractPropertyBasePanel mainPanel = null;

	public JecnDocumentListener(JecnAbstractPropertyBasePanel mailPanel,
			JTextComponent textComp, JecnUserInfoTextArea infoTextArea) {
		this.textComp = textComp;
		this.infoTextArea = infoTextArea;
		this.mainPanel = mailPanel;
	}

	public void insertUpdate(DocumentEvent e) {
		if (check()) {// 校验成功
			updateItemBeanData();
		}

		// 全部校验
		mainPanel.check();
	}

	public void removeUpdate(DocumentEvent e) {
		if (check()) {// 校验成功
			updateItemBeanData();
		}

		// 全部校验
		mainPanel.check();
	}

	public void changedUpdate(DocumentEvent e) {
	}

	/**
	 * 
	 * 更新数据层数据
	 * 
	 */
	private void updateItemBeanData() {
		if (textComp instanceof JecnConfigTextArea) {
			JecnConfigTextArea textArea = ((JecnConfigTextArea) textComp);
			String textAreaStr=textArea.getText();
			if(textAreaStr!=null&&!"".equals(textAreaStr)){
				textAreaStr=textAreaStr.trim();
			}
			textArea.getItemBean().setValue(textAreaStr);
		} else if (textComp instanceof JecnConfigTextField) {
			JecnConfigTextField textField = ((JecnConfigTextField) textComp);
			String textFieldStr=textField.getText();
			if(textFieldStr!=null&&!"".equals(textFieldStr)){
				textFieldStr=textFieldStr.trim();
			}
			textField.getItemBean().setValue(textFieldStr);
		} else if (textComp instanceof JecnConfigPasswordField) {
			JecnConfigPasswordField pwdTextField = ((JecnConfigPasswordField) textComp);
			String pwdTextFieldStr=new String(pwdTextField.getPassword());
			if(pwdTextFieldStr!=null&&!"".equals(pwdTextFieldStr)){
				pwdTextFieldStr=pwdTextFieldStr.trim();
			}
			pwdTextField.getItemBean().setValue(pwdTextFieldStr);
		}
	}

	/**
	 * 
	 * 验证是否合法
	 * 
	 * @return boolean 合法：true 不合法：false
	 */
	private boolean check() {
		// 错误信息
		String info = null;
		if (textComp instanceof JecnConfigTextArea) {// 不能超过1200个字符或600个汉字
			info = JecnUserCheckUtil.checkNoteLength(textComp.getText());
		} else if (textComp instanceof JecnConfigTextField) {// 名称不能超过122个字符或61个汉字
			info = JecnUserCheckUtil.checkNameLength(textComp.getText());
		}  else if (textComp instanceof JecnConfigPasswordField) {// 密码文本输入框名称不能超过122个字符或61个汉字
			info = JecnUserCheckUtil.checkNameLength(textComp.getText());
		} else {
			return false;
		}

		if (mainPanel.setErrorInfo(infoTextArea, info)) {// 有错误信息
			return false;
		} else {
			return true;
		}
	}

}