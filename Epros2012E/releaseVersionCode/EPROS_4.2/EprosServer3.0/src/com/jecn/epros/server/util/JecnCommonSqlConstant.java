package com.jecn.epros.server.util;

public class JecnCommonSqlConstant {
	public final static String ORG_TABLE_NAME = "JECN_FLOW_ORG";
	public final static String ORG_ID = "ORG_ID";
	public final static String ORG_PID = "PER_ORG_ID";

	/******* 文件表 ********/
	public final static String FILE_TABLE_NAME = "JECN_FILE";
	public final static String FILE_ID = "FILE_ID";
	public final static String FILE_PID = "PER_FILE_Id";

	/******** 流程表 *********/
	public final static String FLOW_TABLE_NAME = "JECN_FLOW_STRUCTURE";
	public final static String FLOW_ID = "FLOW_ID";
	public final static String FLOW_PID = "PRE_FLOW_ID";

	/******** 制度表 *********/
	public final static String RULE_TABLE_NAME = "JECN_RULE";
	public final static String RULE_ID = "ID";
	public final static String RULE_PID = "PER_ID";

	/******** 标准表 *********/
	public final static String STANDART_TABLE_NAME = "JECN_CRITERION_CLASSES";
	public final static String STANDART_ID = "CRITERION_CLASS_ID";
	public final static String STANDART_PID = "PRE_CRITERION_CLASS_ID";

	/*
	 * 0是流程 1是文件 2是标准 3制度
	 */
	public enum TYPEAUTH {
		FLOW(0), FILE(1), STANDARD(2), RULE(3);
		private int value;

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}

		private TYPEAUTH(int value) {
			this.value = value;
		}
	}
}
