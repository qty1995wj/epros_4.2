package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

public class AbstractConfigBean {
	/** 开始时间 */
	protected String startTime = "23:59";
	/** 间隔天数 */
	protected String intervalDay = "1";
	/** 手动或自动更新标识:手动：0；自动：1 */
	protected String iaAuto = "0";

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getIntervalDay() {
		return intervalDay;
	}

	public void setIntervalDay(String intervalDay) {
		this.intervalDay = intervalDay;
	}

	public String getIaAuto() {
		return iaAuto;
	}

	public void setIaAuto(String iaAuto) {
		this.iaAuto = iaAuto;
	}

}
