package com.jecn.epros.domain.process;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.util.HttpUtil;

/**
 * Created by admin on 2017/4/28.
 */
public class ProcessAttribute {
    private String title;
    /**
     * 带连接的内容
     */
    private LinkResource link;
    /**
     * 内容
     */
    private String text;

    private String hrefURL;


    public ProcessAttribute() {

    }

    public ProcessAttribute(String title, String text, LinkResource link) {
        this.title = title;
        this.text = text;
        this.link = link;
    }

    public String getHrefURL() {
        return hrefURL;
    }

    public void getDownLoadHrefURL(boolean isPub, Long contentId) {
        if (link == null || link.getType() != LinkResource.ResourceType.PROCESS_FILE) {
            return;
        }
        hrefURL = HttpUtil.getDownLoadFileHttp(contentId, true, "processFile");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LinkResource getLink() {
        return link;
    }

    public void setLink(LinkResource link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

