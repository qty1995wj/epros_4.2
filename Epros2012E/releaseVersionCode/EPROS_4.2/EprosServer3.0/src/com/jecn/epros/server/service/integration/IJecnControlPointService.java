package com.jecn.epros.server.service.integration;

import com.jecn.epros.server.bean.integration.JecnControlPoint;
import com.jecn.epros.server.common.IBaseService;

public interface IJecnControlPointService extends IBaseService<JecnControlPoint, String>  {

}
