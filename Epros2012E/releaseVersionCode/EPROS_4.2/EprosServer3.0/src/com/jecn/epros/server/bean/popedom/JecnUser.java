package com.jecn.epros.server.bean.popedom;

/**
 * 人员表
 * 
 * @author Administrator
 */
public class JecnUser implements java.io.Serializable {
	private Long peopleId;// 主键ID
	private String userNumber;// 员工编号
	private String loginName;// 登录名
	private String password;// 密码
	private String trueName;// 真实姓名
	private String email;// 邮箱
	private Long isLock;// 0是解锁，1是锁定
	private String macAddress;// 网卡地址 是否登录
	private Long id;// 临时数据库中主键
	private String emailType;// 邮件内外网,内网inner,外网outer
	private String phoneStr;// 电话
	private Long projectId;// 项目ID

	/** 域用户名 */
	private String domainName;
	/** 主管 **/
	private Long managerId;
	private String managerName;

	public JecnUser(Long peopleId, String trueName) {
		this.peopleId = peopleId;
		this.trueName = trueName;
	}

	public JecnUser() {
	}

	public Long getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getIsLock() {
		return isLock;
	}

	public void setIsLock(Long isLock) {
		this.isLock = isLock;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getPhoneStr() {
		return phoneStr;
	}

	public void setPhoneStr(String phoneStr) {
		this.phoneStr = phoneStr;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
}
