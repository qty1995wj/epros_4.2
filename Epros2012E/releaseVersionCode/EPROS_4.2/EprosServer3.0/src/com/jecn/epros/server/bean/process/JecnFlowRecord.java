/*
 * JecnFlowRecordT.java
 *
 * Created on 2010年6月23日, 下午2:39
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jecn.epros.server.bean.process;

/**
 * 
 * @author wanglikai
 */
public class JecnFlowRecord implements java.io.Serializable {
	private Long id;
	private Long flowId;
	private String recordName; // 记录名称
	private String recordSavePeople; // 保存责任人
	private String saveLaction; // 保存场所
	private String fileTime; // 归档时间
	private String saveTime; // 保存期限
	private String recordApproach; // 到期处理方式
	/**移交责任人**/
    private String recordTransferPeople;
    /**编号**/
    private String docId;

	/** Creates a new instance of JecnFlowRecordT */
	public JecnFlowRecord() {

	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getRecordName() {
		return recordName;
	}

	public void setRecordName(String recordName) {
		this.recordName = recordName;
	}

	public String getRecordSavePeople() {
		return recordSavePeople;
	}

	public void setRecordSavePeople(String recordSavePeople) {
		this.recordSavePeople = recordSavePeople;
	}

	public String getSaveLaction() {
		return saveLaction;
	}

	public void setSaveLaction(String saveLaction) {
		this.saveLaction = saveLaction;
	}

	public String getRecordApproach() {
		return recordApproach;
	}

	public void setRecordApproach(String recordApproach) {
		this.recordApproach = recordApproach;
	}

	public String getFileTime() {
		return fileTime;
	}

	public void setFileTime(String fileTime) {
		this.fileTime = fileTime;
	}

	public String getSaveTime() {
		return saveTime;
	}

	public void setSaveTime(String saveTime) {
		this.saveTime = saveTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRecordTransferPeople() {
		return recordTransferPeople;
	}

	public void setRecordTransferPeople(String recordTransferPeople) {
		this.recordTransferPeople = recordTransferPeople;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}
	

}
