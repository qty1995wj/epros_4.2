package com.jecn.epros.server.action.web.login.ad.nine999;

import java.util.ResourceBundle;

/**
 * 
 * 九新药业配置信息
 * 
 */
public class Nine999AfterItem {

	private static ResourceBundle nine999Config;

	static {
		nine999Config = ResourceBundle.getBundle("cfgFile/nine999/nine999ServerConfig");
	}

	public static String getValue(String key) {
		return nine999Config.getString(key);
	}
}
