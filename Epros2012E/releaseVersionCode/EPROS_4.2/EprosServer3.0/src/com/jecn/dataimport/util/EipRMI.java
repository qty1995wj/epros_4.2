package com.jecn.dataimport.util;

import java.util.List;

public interface EipRMI {
	/**
	 * 个人任务数据集合
	 * 
	 * @param userId
	 *            登录人员
	 * @return EipDataBean集合
	 */
	public List<EipDataBean> getEipDataBeanList(String userId) throws Exception;

	/**
	 * 所有任务数据集合
	 * 
	 * @return EipDataBean集合
	 */
	public List<EipDataBean> getEipAllDataBeanList() throws Exception;

	/**
	 * 待接办中心相应员工的流程总数
	 * 
	 * @param personId
	 *            员工工号
	 * @return 流程总数
	 */
	public Integer getFlowCount(String userId) throws Exception;

	/**
	 * 所有流程数据集合
	 * 
	 * @return EipDataBean集合
	 */
	public List<EipFlowBean> getEipAllFlowBeanList() throws Exception;

	/**
	 * 获得我的流程
	 * 
	 * @param loginName
	 *            员工工号
	 * @return
	 * @throws Exception
	 */
	public List<EipMyFlowBean> getEipMyFlowBeanList(String loginName)
			throws Exception;

}
