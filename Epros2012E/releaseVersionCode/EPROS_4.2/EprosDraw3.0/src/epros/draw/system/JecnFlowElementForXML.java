package epros.draw.system;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.system.data.JecnDefaultFlowElementData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnXmlUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 从XML配置文件中获取流程元素属性配置信息
 * 
 * 把流程元素属性配置信息写入XML配置文件中
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementForXML {

	/**
	 * 
	 * 获取当前配置信息，图形属性存储到JecnDefaultFigureData对象中
	 * 
	 * @return List<JecnDefaultFigureData>流程元素属性对象集合
	 */
	public static List<JecnDefaultFlowElementData> readCurrentModelByXML() {
		JecnFlowElementForXML flowElementForXML = new JecnFlowElementForXML();
		return flowElementForXML.readFlowElementXML(JecnFileUtil
				.getCurrentModelXMLPath());
	}

	/**
	 * 
	 * 获取系统默认的配置信息，图形属性存储到JecnDefaultFigureData对象中
	 * 
	 * @return List<JecnDefaultFigureData>流程元素属性对象集合
	 */
	public static List<JecnDefaultFlowElementData> readSystemInitModelByXML() {
		JecnFlowElementForXML flowElementForXML = new JecnFlowElementForXML();
		return flowElementForXML.readFlowElementXML(JecnFileUtil
				.getSystemInitModelXMLPath());
	}

	/**
	 * 
	 * 把当前修改的流程元素属性更新到XML配置文件中
	 * 
	 * @param list
	 *            List<JecnDefaultFigureData> 流程元素属性集合
	 * @return boolean true：正常处理保存 false：没有保存或者不能正常处理
	 * 
	 */
	public static boolean writeCurrentModelByXML(
			List<JecnDefaultFlowElementData> list) {
		JecnFlowElementForXML flowElementForXML = new JecnFlowElementForXML();
		return flowElementForXML.writeFlowElementXml(list, JecnFileUtil
				.getCurrentModelXMLPath());
	}

	/**
	 * 
	 * 把修改的图形属性保存到XML文件中
	 * 
	 * @param list
	 *            List<JecnDefaultFigureData> 流程元素图形属性值集合
	 * @param xmlFile
	 *            String XML文件名称
	 * @return boolean true：正常处理保存 false：没有保存或者不能正常处理
	 */
	private boolean writeFlowElementXml(List<JecnDefaultFlowElementData> list,
			String xmlFile) {

		if (list == null || list.size() == 0) {
			return false;
		}

		// 读取XML配置文件
		Document doc = JecnXmlUtil.getDocumentByXmlFile(xmlFile);
		if (doc == null) {
			return false;
		}

		// 有数据赋值成功标识标识
		boolean updateFlag = false;
		for (JecnDefaultFlowElementData figureData : list) {

			if (!figureData.isUpdateFlag()) {// 没有更新的
				continue;
			}
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.info("JecnFlowElementForXML类writeFlowElementXml方法：需要更新的流程元素属性。 图形节点名称="
							+ figureData.getMapElemType().toString());

			// 需要更新
			NodeList eleList = doc.getElementsByTagName(figureData
					.getMapElemType().toString());
			if (eleList.getLength() != 1) {
				JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
						.error("JecnFlowElementForXML类writeFlowElementXml方法：获取流程元素属性配置文件中图形节点出现多个。 图形节点名称="
								+ figureData.getMapElemType().toString());
				continue;
			}

			// 判断填充类型和填充颜色之间的一致性
			boolean retFillData = checkFillData(figureData);
			if (!retFillData) {
				return false;
			}

			// 判断有阴影情况下阴影颜色是否为空，为空返回false，否则返回true
			boolean retShadow = checkShadowData(figureData);
			if (!retShadow) {
				return false;
			}

			// 流程元素图形节点
			Element element = (Element) eleList.item(0);
			// 边框颜色:bodyColor
			element.setAttribute("bodyColor", DrawCommon
					.coverColorToString(figureData.getBodyColor()));
			// 边框类型:bodyStyle
			element.setAttribute("bodyStyle", String.valueOf(figureData
					.getBodyStyle()));
			// 图形宽:figureSizeX
			element.setAttribute("figureSizeX", String.valueOf(figureData
					.getFigureSizeX()));
			// 图形高:figureSizeY
			element.setAttribute("figureSizeY", String.valueOf(figureData
					.getFigureSizeY()));

			// 填充类型:fillType
			addFillTypeToXML(figureData.getFillType(), element);
			// 填充颜色:fillColor
			element.setAttribute("fillColor", DrawCommon
					.coverColorToString(figureData.getFillColor()));
			// 填充颜色2:changeColor
			element.setAttribute("changeColor", DrawCommon
					.coverColorToString(figureData.getChangeColor()));
			// 渐变方向类型:fillColorChangType
			addFillColorChangTypeToXML(figureData.getFillColorChangType(),
					element);

			// 字体颜色:fontColor
			element.setAttribute("fontColor", DrawCommon
					.coverColorToString(figureData.getFontColor()));
			// 字体位置:fontPosition
			element.setAttribute("fontPosition", String.valueOf(figureData
					.getFontPosition()));
			// 字体大小:fontSize
			element.setAttribute("fontSize", String.valueOf(figureData
					.getFontSize()));
			// 影:shadow
			element.setAttribute("shadow", String.valueOf(figureData
					.getShadow()));
			// 字体竖排:upRight
			element.setAttribute("upRight", String.valueOf(figureData
					.getUpRight()));
			// 字形样式:fontStyle
			element.setAttribute("fontStyle", String.valueOf(figureData
					.getFontStyle()));
			// 字体样式:fontName
			element.setAttribute("fontName", String.valueOf(figureData
					.getFontName()));
			// 阴影颜色:shadowColor
			element.setAttribute("shadowColor", DrawCommon
					.coverColorToString(figureData.getShadowColor()));
			// 线条粗细:bodyWidth
			element.setAttribute("bodyWidth", String.valueOf(figureData
					.getBodyWidth()));
			// 3D效果:flag3D
			element.setAttribute("flag3D", String.valueOf(figureData
					.getFlag3D()));

			// 是否显示编辑四个点:eidtPoint
			element.setAttribute("eidtPoint", String.valueOf(figureData
					.getEidtPoint()));

			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.info("JecnFlowElementForXML类writeFlowElementXml方法：更新成功。 图形节点名称="
							+ figureData.getMapElemType().toString());

			updateFlag = true;
		}

		if (!updateFlag) {//无更新时，直接返回
			return true;
		}

		// 保存到本地XML文件中
		boolean ret = JecnXmlUtil.writeXmlByDocument(xmlFile, doc);
		if (!ret) {
			return false;
		}

		return true;
	}

	/**
	 * 
	 * 获取图形属性配置文件内容
	 * 
	 * @param xmlFile
	 *            文件名称
	 * @return List<JecnDefaultFigureData>
	 */
	private List<JecnDefaultFlowElementData> readFlowElementXML(String xmlFile) {
		// 存储流程元素属性对象的集合
		List<JecnDefaultFlowElementData> list = new ArrayList<JecnDefaultFlowElementData>();

		// 读取XML配置文件
		Document doc = JecnXmlUtil.getDocumentByXmlFile(xmlFile);
		if (doc == null) {
			return list;
		}

		// 根节点
		Element root = doc.getDocumentElement();
		if (root == null) {
			return list;
		}

		// 获取图形节点集合
		NodeList nodeList = root.getChildNodes();

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			// 流程元素图形节点
			Element element = (Element) node;

			// 图形节点对应的图形名称标识
			MapElemType mapElemType = null;
			try {
				mapElemType = Enum.valueOf(MapElemType.class, element
						.getTagName());
			} catch (IllegalArgumentException ex) {
				JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
						.error("JecnFlowElementForXML类readFlowElementXML方法：配置文件中的图形节点在MapElemType枚举中没有记录。 图形名称="
								+ element.getTagName());
				continue;
			}

			// 图形数据对象
			JecnDefaultFlowElementData defaultFigureData = new JecnDefaultFlowElementData();

			// 边框颜色:bodyColor
			String bodyColor = element.getAttribute("bodyColor");
			defaultFigureData.setBodyColor(DrawCommon.covertStringToColor(bodyColor));

			// 边框类型:bodyStyle
			String bodyStyle = element.getAttribute("bodyStyle");
			defaultFigureData.setBodyStyle(DrawCommon.coverStringToInt(bodyStyle));

			// 图形宽:figureSizeX
			String figureSizeX = element.getAttribute("figureSizeX");
			defaultFigureData
					.setFigureSizeX(DrawCommon.coverStringToInt(figureSizeX));

			// 图形高:figureSizeY
			String figureSizeY = element.getAttribute("figureSizeY");
			defaultFigureData
					.setFigureSizeY(DrawCommon.coverStringToInt(figureSizeY));

			// 填充类型:fillType
			String fillType = element.getAttribute("fillType");
			addFillTypeByXML(fillType, defaultFigureData);

			// 填充颜色:fillColor
			String fillColor = element.getAttribute("fillColor");
			defaultFigureData.setFillColor(DrawCommon.covertStringToColor(fillColor));

			// 填充颜色2:changeColor
			String changeColor = element.getAttribute("changeColor");
			defaultFigureData.setChangeColor(DrawCommon
					.covertStringToColor(changeColor));

			// 判断填充类型和填充颜色之间的一致性
			boolean ret = checkFillData(defaultFigureData);
			if (!ret) {
				// 读取流程元素配置文件时，填充类型与颜色1和颜色2不一致
				throw new IllegalArgumentException(
						ErrorInfoConstant.FLOW_ELEMENT_FILL_FIAL_XML);
			}

			// 渐变方向类型:fillColorChangType
			String fillColorChangType = element
					.getAttribute("fillColorChangType");
			addFillColorChangTypeByXML(fillColorChangType, defaultFigureData);

			// 字体颜色:fontColor
			String fontColor = element.getAttribute("fontColor");
			defaultFigureData.setFontColor(DrawCommon.covertStringToColor(fontColor));

			// 字体位置:fontPosition
			String fontPosition = element.getAttribute("fontPosition");
			defaultFigureData.setFontPosition(DrawCommon
					.coverStringToInt(fontPosition));

			// 字体大小:fontSize
			String fontSize = element.getAttribute("fontSize");
			defaultFigureData.setFontSize(DrawCommon.coverStringToInt(fontSize));

			// 阴影:shadow
			String shadow = element.getAttribute("shadow");
			defaultFigureData.setShadow(DrawCommon.coverStringToInt(shadow));

			// 阴影颜色:shadowColor
			String shadowColor = element.getAttribute("shadowColor");
			defaultFigureData.setShadowColor(DrawCommon
					.covertStringToColor(shadowColor));

			// 判断有阴影情况下阴影颜色是否为空，为空返回false，否则返回true
			boolean retShadow = checkShadowData(defaultFigureData);
			if (!retShadow) {
				throw new IllegalArgumentException(
						ErrorInfoConstant.FLOW_ELEMENT_SHADOW_FIAL_XML);
			}

			// 字体竖排:upRight
			String upRight = element.getAttribute("upRight");
			defaultFigureData.setUpRight(DrawCommon.coverStringToInt(upRight));

			// 字形样式:fontStyle
			String fontStyle = element.getAttribute("fontStyle");
			defaultFigureData.setFontStyle(DrawCommon.coverStringToInt(fontStyle));

			// 字体样式:fontName
			String fontName = element.getAttribute("fontName");
			defaultFigureData.setFontName(trimString(fontName));

			// 线条粗细:bodyWidth
			String bodyWidth = element.getAttribute("bodyWidth");
			defaultFigureData.setBodyWidth(DrawCommon.coverStringToInt(bodyWidth));

			// 3D效果:flag3D
			String flag3D = element.getAttribute("flag3D");
			defaultFigureData.setFlag3D(coverStringToBoolean(flag3D));
			// 流程元素文字内容:figureText
			String figureText = element.getAttribute("figureText");
			defaultFigureData.setFigureText(figureText);

			// 是否显示编辑四个点:eidtPoint
			String eidtPoint = element.getAttribute("eidtPoint");
			defaultFigureData.setEidtPoint(coverStringToBoolean(eidtPoint));
			
			//是否支持双击：isSortAddFigure 
			String isDoubleClick = element.getAttribute("isDoubleClick");
			defaultFigureData.setDoubleClick(coverStringToBoolean(isDoubleClick));
			
			//是否支持快速添加图形：isSortAddFigure
			String isSortAddFigure = element.getAttribute("isSortAddFigure");
			defaultFigureData.setSortAddFigure(coverStringToBoolean(isSortAddFigure));
			// 添加图形类型
			defaultFigureData.setMapElemType(mapElemType);

			list.add(defaultFigureData);
		}
		return list;
	}

	/**
	 * 
	 * 获取前后去空后的字符串
	 * 
	 * @param eleStr
	 *            String 字符串
	 * @return String 前后去空后的字符串
	 */
	private String trimString(String eleStr) {
		if (DrawCommon.isNullOrEmtryTrim(eleStr)) {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.info("JecnFlowElementForXML类trimString方法：参数数据不正确。配置文件中数据有错误 eleStr="
							+ eleStr);
			return "";
		}
		return eleStr.trim();
	}

	/**
	 * 
	 * 布尔字符串转换成布尔值
	 * 
	 * @param eleStr
	 *            String 布尔字符串
	 * @return boolean
	 */
	private boolean coverStringToBoolean(String eleStr) {
		if (DrawCommon.isNullOrEmtryTrim(eleStr)) {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.info("JecnFlowElementForXML类trimString方法：参数数据不正确。配置文件中数据有错误 eleStr="
							+ eleStr);
			return false;
		}
		return Boolean.valueOf(eleStr);
	}

	/**
	 * 
	 * 添加填充颜色,如果没有指定的类型，那么添加无填充类型
	 * 
	 * @param fillType
	 *            String 填充类型
	 * @param defaultFigureData
	 *            JecnDefaultFigureData 流程元素数据对象
	 * 
	 */
	private void addFillTypeByXML(String fillType,
			JecnDefaultFlowElementData defaultFigureData) {
		if (DrawCommon.isNullOrEmtryTrim(fillType) || defaultFigureData == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.info("JecnFlowElementForXML类addFillTypeByXML方法：参数不能为NULL或空。配置文件中数据有错误 fillType="
							+ fillType
							+ ";defaultFigureData="
							+ defaultFigureData);
			defaultFigureData.setFillType(FillType.none);
		}

		if (FillType.none.toString().equals(fillType)) {// 无填充
			defaultFigureData.setFillType(FillType.none);
		} else if (FillType.one.toString().equals(fillType)) {// 单色填充
			defaultFigureData.setFillType(FillType.one);
		} else if (FillType.two.toString().equals(fillType)) {// 双色填充
			defaultFigureData.setFillType(FillType.two);
		} else {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.info("JecnFlowElementForXML类addFillTypeByXML方法：参数不是指定的填充类型。配置文件中数据有错误 fillType="
							+ fillType);
			defaultFigureData.setFillType(FillType.none);
		}
	}

	/**
	 * 
	 * 添加填充颜色,如果没有指定的类型，那么添加无填充类型
	 * 
	 * @param fillType
	 *            FillType 填充类型
	 * @param element
	 *            Element 流程元素节点
	 * 
	 */
	private void addFillTypeToXML(FillType fillType, Element element) {
		if (fillType == null || element == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.error("JecnFlowElementForXML类addFillTypeToXML方法：参数不能为NULL。 fillType="
							+ fillType + ";element=" + element);
			element.setAttribute("fillType", FillType.none.toString());
		}

		switch (fillType) {
		case none:// 无填充
			element.setAttribute("fillType", FillType.none.toString());
			break;
		case one:// 单色填充
			element.setAttribute("fillType", FillType.one.toString());
			break;
		case two:// 双色填充
			element.setAttribute("fillType", FillType.two.toString());
			break;
		default:
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.error("JecnFlowElementForXML类addFillTypeToXML方法：参数不是指定的填充类型。 fillType="
							+ fillType);
			element.setAttribute("fillType", FillType.none.toString());
			break;
		}
	}

	/**
	 * 
	 * 添加渐变色类型，如果fillColorChangType不是指定的渐变色类型，那么添加默认的渐变色类型
	 * 
	 * @param fillColorChangType
	 *            String 渐变色类型
	 * @param defaultFigureData
	 *            JecnDefaultFigureData 流程元素数据对象
	 * 
	 */
	private void addFillColorChangTypeByXML(String fillColorChangType,
			JecnDefaultFlowElementData defaultFigureData) {
		if (DrawCommon.isNullOrEmtryTrim(fillColorChangType)
				|| defaultFigureData == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.error("JecnFlowElementForXML类addFillColorChangTypeByXML方法：参数不能为NULL或空。配置文件中数据有错误 fillColorChangType="
							+ fillColorChangType
							+ ";defaultFigureData="
							+ defaultFigureData);
			defaultFigureData.setFillColorChangType(FillColorChangType.topTilt);
		}

		if (FillColorChangType.topTilt.toString().equals(fillColorChangType)) {// 1：向上倾斜
			defaultFigureData.setFillColorChangType(FillColorChangType.topTilt);
		} else if (FillColorChangType.bottom.toString().equals(
				fillColorChangType)) { // 2：向下倾斜
			defaultFigureData.setFillColorChangType(FillColorChangType.bottom);
		} else if (FillColorChangType.vertical.toString().equals(
				fillColorChangType)) { // 3：垂直
			defaultFigureData
					.setFillColorChangType(FillColorChangType.vertical);
		} else if (FillColorChangType.horizontal.toString().equals(
				fillColorChangType)) {// 4：水平
			defaultFigureData
					.setFillColorChangType(FillColorChangType.horizontal);
		} else {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.error("JecnFlowElementForXML类addFillColorChangTypeByXML方法：参数不是指定的渐变色类型。配置文件中数据有错误 fillColorChangType="
							+ fillColorChangType);
			defaultFigureData.setFillColorChangType(FillColorChangType.topTilt);
		}
	}

	/**
	 * 
	 * 添加渐变色类型，如果fillColorChangType不是指定的渐变色类型，那么添加默认的渐变色类型
	 * 
	 * @param fillColorChangType
	 *            FillColorChangType 渐变色类型
	 * @param element
	 *            Element 流程元素节点
	 * 
	 */
	private void addFillColorChangTypeToXML(
			FillColorChangType fillColorChangType, Element element) {

		if (fillColorChangType == null || element == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.error("JecnFlowElementForXML类addFillColorChangTypeToXML方法：参数不能为NULL或空。 fillColorChangType="
							+ fillColorChangType + ";element=" + element);
			element.setAttribute("fillColorChangType",
					FillColorChangType.topTilt.toString());
		}

		switch (fillColorChangType) {
		case topTilt:// 1：向上倾斜
			element.setAttribute("fillColorChangType",
					FillColorChangType.topTilt.toString());
			break;
		case bottom:// 2：向下倾斜
			element.setAttribute("fillColorChangType",
					FillColorChangType.bottom.toString());
			break;
		case vertical:// 3：垂直
			element.setAttribute("fillColorChangType",
					FillColorChangType.vertical.toString());
			break;
		case horizontal:// 4：水平
			element.setAttribute("fillColorChangType",
					FillColorChangType.horizontal.toString());
			break;
		default:
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.error("JecnFlowElementForXML类addFillColorChangTypeToXML方法：参数不是指定的渐变色类型。 fillColorChangType="
							+ fillColorChangType);
			element.setAttribute("fillColorChangType",
					FillColorChangType.topTilt.toString());
			break;

		}
	}

	/**
	 * 
	 * 判断填充类型和填充颜色之间的一致性。即FillType是单色时但是fillColor字段值不能为null；
	 * 
	 * FillType是双色时但是fillColor、changeColor字段值不能为null.
	 * 
	 * @param figureData
	 *            JecnDefaultFigureData 待保存的流程元素值
	 * @return boolean 校验正确返回true，校验失败返回false
	 */
	private boolean checkFillData(JecnDefaultFlowElementData figureData) {

		boolean ret = false;

		if (figureData == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.error("JecnFlowElementForXML类checkFillData方法：参数为null。 figureData="
							+ figureData);
			return ret;
		}

		switch (figureData.getFillType()) {
		case none:// 无填充
			ret = true;
			break;
		case one:// 单色填充
			if (figureData.getFillColor() != null) {
				ret = true;
			} else {
				JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
						.error("JecnFlowElementForXML类checkFillData方法：填充类型是单色，但是fillColor字段值为null。 figureData.fillColor="
								+ figureData.getFillColor());
			}
			break;
		case two:// 双色填充
			if (figureData.getFillColor() != null
					&& figureData.getChangeColor() != null) {
				ret = true;
			} else {
				JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
						.error("JecnFlowElementForXML类checkFillData方法：填充类型是双色，但是fillColor、ChangeColor字段值有为null的值。 figureData.fillColor="
								+ figureData.getFillColor()
								+ ";figureData.changeColor="
								+ figureData.getChangeColor());
			}
			break;
		default://
			break;
		}
		return ret;
	}

	/**
	 * 
	 * 判断有阴影情况下阴影颜色是否为空，为空返回false，否则返回true
	 * 
	 * @param figureData
	 *            JecnDefaultFigureData 待保存的流程元素值
	 * @return boolean 有阴影情况下阴影颜色是否为空，为空返回false，否则返回true
	 */
	private boolean checkShadowData(JecnDefaultFlowElementData figureData) {
		boolean ret = false;

		if (figureData == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.error("JecnFlowElementForXML类checkFillData方法：参数为null。 figureData="
							+ figureData);
			return ret;
		}

		// 0:无阴影 1：有阴影
		if (1 == figureData.getShadow() && figureData.getShadowColor() == null) {// 有阴影且阴影颜色为null
			JecnLogConstants.LOG_FLOW_ELEMENT_FORXML
					.error("JecnFlowElementForXML类checkShadowData方法：有阴影但阴影颜色为空。 figureData.shadowColor="
							+ figureData.getShadowColor());

		} else {
			ret = true;
		}

		return ret;
	}
}
