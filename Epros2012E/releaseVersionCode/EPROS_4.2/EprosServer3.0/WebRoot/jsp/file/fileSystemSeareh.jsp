<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="fileSystemSearehText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td width="10%" height="25px" align="right">
								<!-- 文件名称： -->
								<s:text name="fileNameC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%" id="fileName_id"
									maxLength="32" />
							</td>

							<td width="10%" align="right">
								<!-- 文件编号： -->
								<s:text name="fileNumberC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%" id="fileNum_id"
									maxLength="32" />
							</td>

							<td style="vertical-align: middle;" width="10%;" align="center">
								<a onClick="toggle(fileSystemSearehText)"
									style="cursor: pointer;" class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" />
								</span> </a>
							</td>
						</tr>

						<tr>
							<td height="25px" align="right">
								<!-- 密集: -->
								<s:text name="intensiveC" />
							</td>
							<td align="left">
								<select style="width: 80%" id="secretLevel_id">
									<option value="-1">
										<s:text name="all" />
									</option>
									<option value="1">
										<s:property value="#application.public"/>
									</option>
									<option value="0">
										${secret}
									</option>
								</select>
							</td>


							<td align="right">
								<!-- 责任部门: -->
								<s:text name="responsibilityDepartmentC" />
							</td>
							<td align="left">
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="orgDutyName_file_id"
									onkeyup="search(orgDutyName_file_id,orgDutyId_file_id,search_file_orgDutyName,'org');"
									onblur="divNode(search_file_orgDutyName)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("organization","orgDutyName_file_id","orgDutyId_file_id")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="search_file_orgDutyName"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td></td>
						</tr>
						<tr>

							<td height="25px" align="right">
								<!-- 查阅部门: -->
								<s:text name="referToTheDepartmentC" />
							</td>
							<td align="left">
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="orgLookName_file_id"
									onkeyup="search(orgLookName_file_id,orgLookId_file_id,search_file_orgLookName,'org');"
									onblur="divNode(search_file_orgLookName)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("organization","orgLookName_file_id","orgLookId_file_id")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="search_file_orgLookName"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td align="right">
								<!-- 查阅岗位: -->
								<s:text name="referToThePostC" />
							</td>
							<td align="left">
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="posLookName_file_id"
									onkeyup="search(posLookName_file_id,posLookId_file_id,search_file_posLookName,'pos');"
									onblur="divNode(search_file_posLookName)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("position","posLookName_file_id","posLookId_file_id")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="search_file_posLookName"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img src="${basePath}images/iParticipateInProcess/search.gif"
						style="cursor: pointer;" onclick="fileSearch()" />
					<img src="${basePath}images/iParticipateInProcess/reset.gif"
						style="cursor: pointer;" onclick="fileSearchReset()" />
				</div>
				<!-- 责任部门ID -->
				<input type="hidden" id="orgDutyId_file_id" />
				<!-- 查阅部门ID -->
				<input type="hidden" id="orgLookId_file_id" />
				<!-- 查阅岗位ID -->
				<input type="hidden" id="posLookId_file_id" />
			</div>

			<div id="fileGrid_id">

			</div>
		</div>
	</body>
</html>