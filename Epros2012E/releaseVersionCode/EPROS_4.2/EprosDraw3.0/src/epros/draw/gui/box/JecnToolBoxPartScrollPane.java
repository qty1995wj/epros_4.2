package epros.draw.gui.box;

import java.util.List;

import epros.draw.constant.JecnToolBoxConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.system.JecnSystemData;

/**
 * 
 * 流程图的流程元素库
 * 
 * @author ZHOUXY
 * 
 */
class JecnToolBoxPartScrollPane extends JecnToolBoxAbstractScrollPane {

	JecnToolBoxPartScrollPane(JecnToolBoxMainPanel toolBoxMainPanel) {
		super(toolBoxMainPanel);
	}

	@Override
	protected void initToolBoxfavrData() {
		// 常用线集合
		addOftenBtnToLineList(JecnToolBoxConstant.partMapFavrLineFlowType);
		// 常用图形集合
		addOftenBtnToFigureList(JecnToolBoxConstant.partMapFavrFiguresFlowType);
	}

	@Override
	protected void initToolBoxGenData() {
		// 获取本地配置文件中不常用图形配置
		List<MapElemType> partMapToolBoxDataList = JecnSystemData.readPartListByPropertyFile();

		// 不常用流程元素集合
		addPropertiesGenBtnToList(partMapToolBoxDataList);
	}

	/**
	 * 获取数据库中显示的元素集合
	 */
	protected void initShowToolBoxData() {
		List<MapElemType> list = JecnDesignerProcess.getDesignerProcess().getShowElementsType(0);
		if (list == null) {
			return;
		}
		addAllBtnToShowList(list);
	}

	@Override
	MapElemType[] getGenFigureTypeArray() {
		return JecnToolBoxConstant.partMapGenNoShowFigureType;
	}

	@Override
	MapElemType[] getGenLineTypeArray() {
		return JecnToolBoxConstant.partMapGenNoShowLineType;
	}
}
