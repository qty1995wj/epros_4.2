package com.jecn.epros.server.bean.task;

import java.io.Serializable;

/**
 * 流程文件制度关联模板
 * @author user
 *
 */
public class PrfRelatedTemplet implements Serializable{
	
	private String id;
	/** 流程、文件、制度主键*/
	private Long relatedId;
	/** 任务模板主键**/
	private String templetId;
	/** 任务模板主键**/
	private int type;
	/** 模板类型 0审批 1借阅 2废止**/
	private int templetType;
	
	
	
	
	public int getTempletType() {
		return templetType;
	}
	public void setTempletType(int templetType) {
		this.templetType = templetType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getRelatedId() {
		return relatedId;
	}
	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}
	public String getTempletId() {
		return templetId;
	}
	public void setTempletId(String templetId) {
		this.templetId = templetId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	

}
