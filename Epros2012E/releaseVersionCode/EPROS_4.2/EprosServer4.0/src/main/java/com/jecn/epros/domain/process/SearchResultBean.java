package com.jecn.epros.domain.process;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;

/**
 * 流程搜索返回结果
 *
 * @author ZXH
 */
public class SearchResultBean {
    private Long id;
    private String name;
    private String num;
    private String resPeopleName;
    private String resOrgName;
    /**
     * 0：秘密，1：公开
     */
    private int isPublic;
    /***/
    private String releaseDate;

    public LinkResource getLink() {
        return new LinkResource(id, name, ResourceType.PROCESS);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getResPeopleName() {
        return resPeopleName;
    }

    public void setResPeopleName(String resPeopleName) {
        this.resPeopleName = resPeopleName;
    }

    public String getResOrgName() {
        return resOrgName;
    }

    public void setResOrgName(String resOrgName) {
        this.resOrgName = resOrgName;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

}
