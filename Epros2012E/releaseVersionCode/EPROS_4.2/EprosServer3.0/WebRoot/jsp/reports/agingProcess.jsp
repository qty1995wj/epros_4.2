<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
    <!-- 流程时效性统计3.06废弃 -->
	<body>
		<div id="div" class="div" style="height: 99%;">
			<form action="" id="ruleForm">
				<div id="search" class="divBorder" align="center">
					<div id="agingProcessSearchText" style="overflow: hidden;"
						class="minWidth">
						<table width="100%">
							<tr>
								<td height="25px" width="10%;" align="right">
									<!-- 责任部门: -->
									<s:text name="responsibilityDepartmentC" />
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgDutyName_aging_id"
										onkeyup="search(orgDutyName_aging_id,orgDutyId_aging_id,search_aging_orgDutyName,'org');"
										onblur="divNode(search_aging_orgDutyName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgDutyName_aging_id","orgDutyId_aging_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_aging_orgDutyName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td width="10%;" align="right">
									<!-- 流程名称： -->
									<s:text name="processNameC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="processName_aging_id" maxLength="32" readonly="readonly"/>
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectProcessOrMaWindow("processName_aging_id","processId_aging_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
								</td>

								<td style="vertical-align: middle; width: 10%;" align="center">
									<a onClick="toggle(agingProcessSearchText)"
										style="cursor: pointer;" class="packUp"> <img id="packUp"
											src="${basePath}images/iParticipateInProcess/on.gif"
											align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
												name="packUp" /> </span> </a>
								</td>
							</tr>

							<tr>
								<td height="25px" align="right">
									<!-- 时间段(月): -->
									<s:text name="periodMonthC" />
								</td>
								<td align="left">
									<input type="text" style="width: 80%; vertical-align: middle;"
										id="timeType_id" value="12" maxLength="12" />
								</td>

								<td align="right">
									<!-- 状态: -->
									<s:text name="stateC" />
								</td>
								<td align="left">
									<select style="width: 80%" id="state_id">
										<option value="0">
											<!-- 全部 -->
											<s:text name="all" />
										</option>
										<option value="1">
											<!-- 未更新 -->
											<s:text name="failedToUpdate" />
										</option>
										<option value="2">
											<!-- 已更新 -->
											<s:text name="hasBeenUpdated" />
										</option>
									</select>
								</td>

								<td></td>
							</tr>
						</table>
					</div>

					<div id="searchBut" align="center">
						<img src="${basePath}images/iParticipateInProcess/search.gif"
							style="cursor: pointer;" onclick="agingProcessGridSearch()" />
						<img src="${basePath}images/iParticipateInProcess/reset.gif"
							style="cursor: pointer;" onclick="agingProcessSearchReset()" />
					</div>
				</div>
				<!-- 责任部门ID -->
				<input type="hidden" id="orgDutyId_aging_id" value="-1" />
				<!-- 流程ID -->
				<input type="hidden" id="processId_aging_id" value="-1" />
			</form>
			<table width="100%">
				<tr>
					<td width="30%">
						<div id="sysGrid_id" align="center">
						</div>
					</td>
					<td width="70%">
						<div id="agingProcessGrid_id" style="margin-right: 15px;"></div>
					</td>
				</tr>
				<tr>
					<td></td>
					<td align="center" style="padding-top: 5px;">
						<a onclick="agingProcessDownloadExcel()"
							style="cursor: pointer; color: #005EA7;" class="table"><s:text
								name="downLoadExcel" /> </a>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>