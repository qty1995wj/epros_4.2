package epros.draw.gui.operationConfig;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 活动说明表格
 * 
 * @author 2012-07-05
 * 
 */
public class ActiveDescTable extends JTable implements MouseListener {
	/** 界面初始化的数据 */
	private List<JecnActivityShowBean> listAllActivityShow;

	/**
	 * 界面初始化
	 * 
	 * @param listAllActivityShow
	 *            界面初始化的数据
	 */
	public ActiveDescTable(List<JecnActivityShowBean> listAllActivityShow) {
		this.listAllActivityShow = listAllActivityShow;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		this.addMouseListener(this);
	}

	/**
	 * 初始化JTable数据
	 * 
	 * @author fuzhh Aug 28, 2012
	 * @return
	 */
	public ActiveDescTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activitiesNumbers"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("executiveRole"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activityTitle"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activityIndicatingThat"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("input"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("output"));
		// 获取操作说明数据
		Vector<Vector<Object>> rowData = new Vector<Vector<Object>>();
		for (JecnActivityShowBean activityShowBean : listAllActivityShow) {
			Vector<Object> row = new Vector<Object>();
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getActivityNum());
			row.add(activityShowBean.getRoleName());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFigureText());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow());
			row.add("");
			row.add("");
			rowData.add(row);
		}
		return new ActiveDescTableMode(rowData, title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class ActiveDescTableMode extends DefaultTableModel {
		public ActiveDescTableMode(Vector<Vector<Object>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// 判断为左键双击
		if (e.getClickCount() == 2 && e.getButton() == 1) {
			int selectRow = this.getSelectedRow();
			// 判断是否为设计器
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
				ActivityDescriptionDialog activityDescriptionDialog = new ActivityDescriptionDialog(listAllActivityShow
						.get(selectRow));
				activityDescriptionDialog.setVisible(true);
			} else {
				// DrawActivityDescriptionDialog drawActivityDescriptionDialog =
				// new DrawActivityDescriptionDialog(
				// listAllActivityShow.get(selectRow), this, selectRow);
				// drawActivityDescriptionDialog.setVisible(true);
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}
}
