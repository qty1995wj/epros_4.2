package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseKeyActiveFigure;

/**
 * 
 * 关键控制点
 * 
 * @date： 日期：Mar 21, 2012 时间：10:40:29 AM
 * @author ZHAGNXH
 */
public class KCPFigure extends JecnBaseKeyActiveFigure {

	public KCPFigure(JecnFigureData keyActiveData) {
		super(keyActiveData);
	}

}
