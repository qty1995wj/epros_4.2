package epros.draw.event.flowElement;

import java.awt.event.MouseEvent;

import epros.draw.constant.JecnWorkflowConstant.ReSizeLineType;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.gui.line.JecnMoveLineUnit;
import epros.draw.gui.line.JecnMoveLineUnit.MoveEnum;
import epros.draw.gui.line.resize.JecnLineResizePanel;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUnRedoManhattanLineData;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 连接线鼠标处理 (鼠标点击、拖动小线段)
 * 
 * @author Administrator
 * @date： 日期：Apr 28, 2012 时间：11:27:58 AM
 */
public class JecnLineSegmentResizeProcess {
	/** 线段拖动处理类 */
	private JecnMoveLineUnit jecnMoveLineUnit = null;

	/** 鼠标拖动线段对象 */
	private LineSegment curLineSegment = null;
	/** 操作前数据 */
	private JecnUndoRedoData undoData = null;
	/** 操作后数据 */
	private JecnUndoRedoData redoData = null;
	/** 是否执行拖动 */
	private boolean isDragged = false;

	/**
	 * 鼠标点击连接线事件处理
	 * 
	 * @param e
	 *            MouseEvent鼠标事件
	 */
	public void drawLineMousePressed(MouseEvent e) {
		if (e.getSource() == null || JecnDrawMainPanel.getMainPanel() == null) {
			return;
		}
		if (e.getSource() instanceof JecnLineResizePanel) {
			// 获取鼠标点击对象
			JecnLineResizePanel lineResize = (JecnLineResizePanel) e
					.getSource();
			if (!ReSizeLineType.center.equals(lineResize.getReSizeLineType())) {// 点击线段选中点
				// 如果不是中心点返回
				return;
			}
			// 获取鼠标要拖动的线段对象
			curLineSegment = lineResize.getLineSegment();
			// 获取拖动线段处理类实例
			jecnMoveLineUnit = new JecnMoveLineUnit();
			// 获取鼠标当前坐标值作为线段拖动前的原始点
			jecnMoveLineUnit.setMouseOldPoint(e.getPoint());

			undoData = new JecnUndoRedoData();
			// 记录撤销恢复数据
			undoData.recodeFlowElement(curLineSegment.getBaseManhattanLine());
		}
	}

	/**
	 * 鼠标释放画线 走曼哈顿算法 鼠标释放处理
	 * 
	 * @param e
	 */
	public void drawLineMouseReleased(MouseEvent e) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null
				|| e.getSource() == null
				|| JecnDrawMainPanel.getMainPanel() == null
				|| jecnMoveLineUnit == null || curLineSegment == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		// 获取鼠标点击对象
		JecnLineResizePanel lineResize = (JecnLineResizePanel) e.getSource();
		if (!ReSizeLineType.center.equals(lineResize.getReSizeLineType())) {// 点击线段选中点
			// 如果不是中心点返回
			return;
		}
		if (!isDragged) {
			return;
		}
		isDragged = false;

		// 备份拖动前小线段集合
		jecnMoveLineUnit.getCurLineSegmentClone(curLineSegment);

		// 拖动线时 第一次点击鼠标的坐标
		jecnMoveLineUnit.setMouseOldPoint(null);
		// 清空面板上虚线
		JecnWorkflowUtil.disposeAllTempLine();

		// 释放添加连接线
		MoveEnum moveEnum = jecnMoveLineUnit.drawNewLine(curLineSegment);
		switch (moveEnum) {
		case success:// 执行成功
			redoData = new JecnUndoRedoData();
			// 记录撤销恢复数据
			redoData.recodeFlowElement(curLineSegment.getBaseManhattanLine());

			// 撤销恢复数据处理
			JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory
					.createEdit(undoData, redoData);
			// 添加撤销恢复动作数据
			desktopPane.getUndoRedoManager().addEdit(undoProcess);
			break;
		case rollback:// 执行失败，需要撤销恢复
			if (undoData.getManhattanLineDataCloneList() != null) {
				for (JecnUnRedoManhattanLineData lineData : undoData
						.getManhattanLineDataCloneList()) {
					// 把备份数据替换连接线的数据
					lineData.getManhattanLineData().setFlowElementAttributes(
							lineData.getManhattanLineDataClone());
					for (JecnLineSegmentData lineSegmentData : lineData
							.getManhattanLineData().getLineSegmentDataList()) {
						lineData.getManhattanLine().createLineSegmentLine(
								lineSegmentData, lineSegmentData.isEndLine());
					}

					// 添加连接线
					JecnAddFlowElementUnit.addDeleteUndoManLine(lineData
							.getManhattanLine());
					// 连接线执行放大缩小算法
					JecnDrawMainPanel.getMainPanel().getWorkflow()
							.getWorkflowZoomProcess().zoomManhattanLine(
									lineData.getManhattanLine());
					// 清空克隆连接线小线段临时数据：剪切->撤销->粘贴
					if (lineData.getManhattanLine().curLineSegmentsClone != null) {
						lineData.getManhattanLine().curLineSegmentsClone
								.clear();
					}
				}
			}
			break;
		}

		desktopPane.updateUI();

		clear();
	}

	/**
	 * 鼠标拖动连接线
	 * 
	 * @param e
	 */
	public void drawLineMouseDragged(MouseEvent e) {
		if (e.getSource() == null || JecnDrawMainPanel.getMainPanel() == null
				|| jecnMoveLineUnit == null || curLineSegment == null) {
			return;
		}
		if (e.getSource() instanceof JecnLineResizePanel) {
			// 获取鼠标点击对象
			JecnLineResizePanel lineResize = (JecnLineResizePanel) e
					.getSource();
			if (!ReSizeLineType.center.equals(lineResize.getReSizeLineType())) {// 点击线段选中点
				// 如果不是中心点返回
				return;
			}// 执行拖动
			isDragged = true;

			// 鼠标拖曳,虚拟绘图(baseLinePanel不规则)
			jecnMoveLineUnit.drawTempLine(e.getPoint(), curLineSegment);
		}
	}

	private void clear() {
		// 线段拖动处理类
		jecnMoveLineUnit = null;

		// 鼠标拖动线段对象
		curLineSegment = null;
		// 操作前数据
		undoData = null;
		// 操作后数据
		redoData = null;
		// 是否执行拖动
		isDragged = false;
	}
}
