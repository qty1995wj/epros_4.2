package com.jecn.epros.email;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.EmailBasicInfo;
import com.jecn.epros.domain.propose.Propose;
import com.jecn.epros.sqlprovider.server3.Common;

public class ProposeHandlerResultEmailBuilder extends DynamicContentEmailBuilder {


    @Override
    protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

        Object[] data = getData();
        JecnUser handleUser = (JecnUser) data[0];
        Propose rtnlPropose = (Propose) data[1];
        int intflag = (Integer) data[2];

        return handleProposeSendEmail(handleUser, user, rtnlPropose, intflag);
    }


    /**
     * 处理合理化建议之后给创建人发送邮件
     *
     * @param handleUser      合理化建议的处理人
     * @param createUser      创建人
     * @param intflag         操作 2采纳 3拒绝
     * @param jecnRtnlPropose 合理化建议
     * @author hyl
     */
    public EmailBasicInfo handleProposeSendEmail(JecnUser handleUser,
                                                 JecnUser createUser, Propose rtnlPropose, int intflag) {

        // 邮件标题
        String subject = "合理化建议" + "-";
        if (intflag == 2) {
            subject += "采纳";
        } else if (intflag == 3) {
            subject += "拒绝";
        }

        StringBuffer strBuf = new StringBuffer();
        strBuf.append("您好!您提交的合理化建议被");
        strBuf.append("“");
        if (intflag == 2) {
            strBuf.append("采纳");
        } else if (intflag == 3) {
            strBuf.append("拒绝");
        }
        strBuf.append("”");

        strBuf.append("<br>");
        strBuf.append("提交时间："
                + Common.getStringbyDate(rtnlPropose.getCreateTime()));
        strBuf.append("<br>");
        strBuf.append("提交人："
                + handleUser.getLoginName());
        strBuf.append("<br>");
        /** 合理化建议类型：0：流程；1：制度 */
        if (rtnlPropose.getType() == 0) {
            strBuf.append("流程名称："
                    + rtnlPropose.getRelationName());
        } else if (rtnlPropose.getType() == 1) {
            strBuf.append("制度名称："
                    + rtnlPropose.getRelationName());
        }
        strBuf.append("<br>");

        EmailContentBuilder builder = new EmailContentBuilder(getTip());
        builder.setContent(strBuf.toString());

        EmailBasicInfo baseInfo = new EmailBasicInfo();

        baseInfo.setContent(builder.buildContent());
        baseInfo.setSubject(subject);
        baseInfo.addRecipients(createUser);

        return baseInfo;

    }

//	/**
//	 * 校验和封装邮件地址
//	 * 
//	 * @author hyl
//	 * @param user
//	 *            收件人
//	 * @return
//	 */
//	private static SimpleUserEmailBean checkAndPackEmailAddress(JecnUser user) {
//
//		SimpleUserEmailBean simpleUserEmailBean = new SimpleUserEmailBean();
//		// 0:内外网；1：邮件地址;2:人员主键ID
//		if (user != null && !JecnCommon.isNullOrEmtryTrim(user.getEmail())
//				&& !JecnCommon.isNullOrEmtryTrim(user.getEmailType())
//				&& JecnCommon.checkMailFormat(user.getEmail())) {
//
//			ToolEmail.setEmailInfo(user.getEmailType(), user.getEmail(),
//					simpleUserEmailBean);
//			return simpleUserEmailBean;
//
//		} else {
//
//			return null;
//		}
//
//	}


}
