package epros.draw.gui.top.toolbar;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnPanel;

/**
 * 
 * 工具栏大类：如文件、编辑、格式、帮助等类的基类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnAstractToolBarItemPanel extends JecnPanel {
	/** 类型：文件、编辑、格式、帮助等 */
	protected ToolBarElemType titleType = null;
	/** 工具栏面板 */
	protected JecnToolBarPanel toolBarPanel = null;

	public JecnAstractToolBarItemPanel(JecnToolBarPanel toolBarPanel) {
		if (toolBarPanel == null) {
			JecnLogConstants.LOG_ASTRACT_TOOLBAR_ITEM_PANEL
					.error("JecnAstractToolBarItemPanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.toolBarPanel = toolBarPanel;
		initCompants();
	}

	private void initCompants() {
		// 初始化类型
		initTitleType();
	}

	public ToolBarElemType getTitleType() {
		return titleType;
	}

	/** 类型：文件、编辑、格式、帮助等 */
	protected abstract void initTitleType();

	/** 类型：文件、编辑、格式、帮助等 */
	protected void initchildComponents() {
		// 创建子组件
		createChildComponents();
		// 布局子组件
		layoutChildComponents();
	}

	/** 创建子组件 */
	protected abstract void createChildComponents();

	/** 布局子组件 */
	protected abstract void layoutChildComponents();

}
