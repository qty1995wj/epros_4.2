package com.jecn.epros.server.action.web.login.ad.csr;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import sun.misc.BASE64Decoder;
import ws.PortalSSOClient;
import ws.PortalSSOSoap;

import com.jecn.authentication.AuthenticationFirefoxRequestUtil;
import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 南车株洲 URL(http://127.0.0.1:8088/EprosServer3.0/login.action?SSO=
 * 'VFdwQmQwNTZWWGROVkdjOQ=='&RedirectUrl=1231)
 * 
 * @author ZXH
 * 
 */
public class CsrLoginAction extends JecnAbstractADLoginAction {

	private final String SSO_VALUE = "SSO";
	private final String REDIRECT_URL = "RedirectUrl";

	private final String TASK_ID = "mailTaskId";
	private final String RULE_ID = "mailRuleId";

	private final String USER_CODE = "usercode";

	public CsrLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		try {

			// 登录名称
			String name = this.loginAction.getLoginName();
			// 登录密码
			String password = this.loginAction.getPassword();

			// 1、普通登录:用户名密码验证
			if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(password)) {
				return validate(name, password);
			}

			if (!AuthenticationFirefoxRequestUtil.isFireFoxBrower(loginAction.getRequest())) {// 非火狐
				// 跳转到火狐，执行单点登录
				AuthenticationFirefoxRequestUtil.fireFoxBrowserRequest(getLoginUrl(loginAction), loginAction
						.getResponse(), JecnCsrAfterItem.FireFox_URL, JecnCsrAfterItem.FireFox_URL1);
				return LoginAction.INPUT;
			}

			String userName = this.loginAction.getRequest().getParameter("userName");
			if (StringUtils.isNotBlank(userName)) {
				loginAction.setLoginName(userName);
				// 不验证密码登录
				return loginAction.userLoginGen(false);
			}

			// 2、OA 单点登录，人员登录名

			String userCode = this.loginAction.getRequest().getQueryString();
			String parameterName = USER_CODE + "=";
			if (StringUtils.isNotBlank(userCode) && userCode.contains(parameterName)) {
				userCode = userCode.substring(userCode.lastIndexOf(USER_CODE + "=") + parameterName.length(), userCode
						.length());
				return oASSOLogin(decodeOAUserCode(userCode));
			}

			// /3、SSOLogin
			return ssoLogin();
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	private String decodeOAUserCode(String userCode) {
		try {
			userCode = new String(decode(userCode));
			userCode = new String(decode(userCode));
			userCode = new String(decode(userCode));
		} catch (IOException e) {
			e.printStackTrace();
			log.error("OA单点用户名解密异常！");
		}
		return userCode;
	}

	public static byte[] decode(String str) throws IOException {
		BASE64Decoder base64Decoder = new BASE64Decoder();
		return base64Decoder.decodeBuffer(str);
	}

	private String ssoLogin() {
		// 1、 取得Portal传递过来的SSO参数
		String queryString = loginAction.getRequest().getQueryString();
		if (StringUtils.isEmpty(queryString)) {// 正常登陆
			return loginAction.loginGen();
		}

		Map<String, String> queryMaps = getQueryMaps(queryString);
		if (queryMaps.size() == 0) {
			return loginAction.loginGen();
		}
		String strSSOValue = queryMaps.get(SSO_VALUE);
		log.info("strSSOValue = " + strSSOValue);
		if (StringUtils.isBlank(strSSOValue)) {
			return loginAction.loginGen();
		}
		log.info("JecnCsrAfterItem.LOGIN_URL" + JecnCsrAfterItem.LOGIN_URL);
		// 2、声明Portal SSO Web Servcie客户端对象
		PortalSSOClient client = new PortalSSOClient(JecnCsrAfterItem.LOGIN_URL);
		// 3、获取Portal SSO WebService
		PortalSSOSoap service = client.getPortalSSOSoap();
		// 调用Portal SSO WebService，进行用户身份的验证
		// WebService返回值不为空时，则SSO验证通过，返回值即为Portal当前用户
		// 在本业务系统对应的帐号
		// WebService返回值为空，则SSO验证失败

		String userName = service.checkSSO(strSSOValue).trim();
		// String userName = "admin";
		log.info(" SSO 验证 userName = " + userName);
		if (StringUtils.isNotEmpty(userName)) {
			String redirectUrl = queryMaps.get(REDIRECT_URL);
			log.info("redirectUrl = " + redirectUrl);
			loginAction.setLoginName(userName);
			if (StringUtils.isNotEmpty(redirectUrl)) {
				if (redirectUrl.contains("http://")) {
					// 不验证密码登录
					String result = loginAction.userLoginGen(false);
					redirectUrl = getRedirectUrl(queryMaps);
					if (StringUtils.isBlank(redirectUrl)) {
						return result;
					} else {
						loginAction.setCustomRedirectURL(redirectUrl + "&userName=" + userName);
						return LoginAction.REDIRECT;
					}
				}
			}
			// 不验证密码登录
			return loginAction.userLoginGen(false);
		}
		// 普通登录
		return LoginAction.INPUT;
	}

	private String oASSOLogin(String userCode) {
		log.info("OA获取usercode = " + userCode);
		loginAction.setLoginName(userCode);

		// 不验证密码登录
		String result = loginAction.userLoginGen(false);
		// 127.0.0.1:8080/EprosServer3.0/loginMail.action?accessType=mailOpenDominoRuleModelFile&mailRuleId=581&isPub=true&usercode=admin
		String mailRuleId = this.loginAction.getRequest().getParameter("mailRuleId");
		String accessType = this.loginAction.getRequest().getParameter("accessType");
		if (!LoginAction.INPUT.equals(accessType)) {
			loginAction.setAccessType(accessType);
			loginAction.setMailRuleId(mailRuleId);
			if (result.equals(LoginAction.INPUT)) {
				return "main";
			}
			return result;
		}
		return result;
	}

	private String validate(String name, String password) {
		String result = loginAction.userLoginGen(true);
		if (!LoginAction.INPUT.equals(result)) {
			if (!AuthenticationFirefoxRequestUtil.isFireFoxBrower(loginAction.getRequest())) {// 非火狐
				AuthenticationFirefoxRequestUtil.fireFoxBrowserRequest(getLoginUrl(loginAction), loginAction
						.getResponse(), JecnCsrAfterItem.FireFox_URL, JecnCsrAfterItem.FireFox_URL1);
				return LoginAction.INPUT;
			}
			return result;
		}
		return LoginAction.INPUT;
	}

	/**
	 * 邮件登录，浏览器判断
	 * 
	 * @param loginAction
	 */
	public static boolean isFireFox(LoginAction loginAction) {
		if (!AuthenticationFirefoxRequestUtil.isFireFoxBrower(loginAction.getRequest())) {// 非火狐
			AuthenticationFirefoxRequestUtil.fireFoxBrowserRequest(getLoginUrl(loginAction), loginAction.getResponse(),
					JecnCsrAfterItem.FireFox_URL, JecnCsrAfterItem.FireFox_URL1);
			return false;
		}
		return true;
	}

	private static String getLoginUrl(LoginAction loginAction) {
		StringBuffer url = loginAction.getRequest().getRequestURL();
		if (StringUtils.isNotBlank(loginAction.getLoginName()) && StringUtils.isNotBlank(loginAction.getPassword())) {// 普通登录
			url.append("?").append("userName=").append(loginAction.getLoginName());
		}
		if (StringUtils.isNotBlank(loginAction.getRequest().getQueryString())) {
			url.append("?").append(loginAction.getRequest().getQueryString());
		}
		log.info(JecnCsrAfterItem.FireFox_URL + " " + url.toString());
		return url.toString();
	}

	private String getRedirectUrl(Map<String, String> queryMaps) {
		String redirectUrl = queryMaps.get(REDIRECT_URL);
		String taskId = queryMaps.get(TASK_ID);
		String ruleId = queryMaps.get(RULE_ID);
		if (StringUtils.isNotBlank(taskId)) {
			return redirectUrl + "&" + TASK_ID + "=" + taskId;
		} else if (StringUtils.isNotBlank(ruleId)) {
			return redirectUrl + "&" + RULE_ID + "=" + ruleId;
		}
		return null;
	}

	private Map<String, String> getQueryMaps(String queryString) {
		if (StringUtils.isBlank(queryString)) {
			return null;
		}
		String[] strs = queryString.split("&");
		Map<String, String> maps = new HashMap<String, String>();
		for (String string : strs) {
			if (string.indexOf("SSO=") != -1) {
				String ssoValue = string.substring(string.lastIndexOf("SSO="), string.length());
				maps.put(SSO_VALUE, ssoValue.substring(4, ssoValue.length()));
			} else if (string.indexOf("RedirectUrl=") != -1) {
				String RedirectUrl = string.substring(string.lastIndexOf("RedirectUrl="), string.length());
				maps.put(REDIRECT_URL, RedirectUrl.substring(12, RedirectUrl.length()));
			} else if (string.indexOf("mailTaskId") != -1) {
				String mailTaskId = string.substring(string.lastIndexOf("mailTaskId="), string.length());
				maps.put(TASK_ID, mailTaskId.substring(11, mailTaskId.length()));
			} else if (string.indexOf("mailRuleId") != -1) {
				String mailRuleId = string.substring(string.lastIndexOf("mailRuleId="), string.length());
				maps.put(RULE_ID, mailRuleId.substring(11, mailRuleId.length()));
			}
		}
		return maps;
	}

	public static void main(String[] args) {
		String str = "1=1&SSO=7a007d10-2d84-4b6d-a4f1-6da21c785d29&RedirectUrl=http://127.0.0.1:8080/EprosServer3.0/loginMail.action?accessType=openApproveDomino&mailTaskId=810";
		String[] strs = str.split("&");
		Map<String, String> maps = new HashMap<String, String>();
		for (String string : strs) {
			if (string.indexOf("SSO=") != -1) {
				String ssoValue = string.substring(string.lastIndexOf("SSO="), string.length());
				maps.put("SSO", ssoValue.substring(4, ssoValue.length()));
			} else if (string.indexOf("RedirectUrl=") != -1) {
				String RedirectUrl = string.substring(string.lastIndexOf("RedirectUrl="), string.length());
				maps.put("RedirectUrl", RedirectUrl.substring(12, RedirectUrl.length()));
			} else if (string.indexOf("mailTaskId") != -1) {
				String mailTaskId = string.substring(string.lastIndexOf("mailTaskId="), string.length());
				maps.put("mailTaskId", mailTaskId.substring(11, mailTaskId.length()));
			} else if (string.indexOf("mailRuleId") != -1) {
				String mailRuleId = string.substring(string.lastIndexOf("mailRuleId="), string.length());
				maps.put("mailRuleId", mailRuleId.substring(11, mailRuleId.length()));
			}
		}
	}
	// private Map<String, String> getMaps(String queryString) {
	//
	// }

	// /**
	// * 邮件连接访问URL
	// *
	// * @param taskId
	// * @param accessType
	// * @param strUserId
	// * @return
	// */
	// private static String getMailFireFoxHttpUrl(String taskId, String
	// accessType, String strUserId) {
	// return JecnCsrAfterItem.FireFox_MAIL_URL + "?mailTaskId=" + taskId +
	// "&accessType=" + accessType
	// + "&strUserId=" + strUserId;
	// }

	// private static String getFireFoxHttpUrl(String strUserId) {
	// return JecnCsrAfterItem.FireFox_URL + "?strUserId=" + strUserId;
	// }
}
