package epros.draw.designer;

import java.util.ArrayList;
import java.util.List;

public class JecnModeFileT implements java.io.Serializable {

	private Long modeFileId;// 主键
	private Long figureId;// 活动元素ID
	private String modeName;// 文件名称
	private Long fileMId;// 输出的表单 文件ID
	private String fileNumber; // 文件编号
	private List<JecnTempletT> listJecnTempletT;
	/** 要删除的样例 */
	private List<Long> listDeleteTempletT;

	private String figureUUID;
	private String UUID;

	public String getFileNumber() {
		return fileNumber;
	}

	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}

	public Long getModeFileId() {
		return modeFileId;
	}

	public void setModeFileId(Long modeFileId) {
		this.modeFileId = modeFileId;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public String getModeName() {
		return modeName;
	}

	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	public Long getFileMId() {
		return fileMId;
	}

	public void setFileMId(Long fileMId) {
		this.fileMId = fileMId;
	}

	/**
	 * @return the listJecnTempletT
	 */
	public List<JecnTempletT> getListJecnTempletT() {
		return listJecnTempletT;
	}

	/**
	 * @param listJecnTempletT
	 *            the listJecnTempletT to set
	 */
	public void setListJecnTempletT(List<JecnTempletT> listJecnTempletT) {
		this.listJecnTempletT = listJecnTempletT;
	}

	public List<Long> getListDeleteTempletT() {
		return listDeleteTempletT;
	}

	public void setListDeleteTempletT(List<Long> listDeleteTempletT) {
		this.listDeleteTempletT = listDeleteTempletT;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String UUID) {
		this.UUID = UUID;
	}

	public JecnModeFileT clone() {
		JecnModeFileT clone = new JecnModeFileT();
		clone.setModeFileId(this.getModeFileId());
		clone.setFigureId(this.figureId);
		clone.setFigureUUID(figureUUID);
		clone.setModeName(this.modeName);
		clone.setFileMId(this.fileMId);
		clone.setFileNumber(this.fileNumber);
		if (listJecnTempletT != null && !listJecnTempletT.isEmpty()) {
			List<JecnTempletT> cloneTempletList = new ArrayList<JecnTempletT>();
			JecnTempletT cloneTemlet = null;
			for (JecnTempletT jecnTempletT : listJecnTempletT) {
				cloneTemlet = jecnTempletT.clone();
				cloneTempletList.add(cloneTemlet);
			}
			clone.setListJecnTempletT(cloneTempletList);
		}
		return clone;
	}
}
