package com.jecn.search.action;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.standard.IStandardService;
import com.jecn.search.bean.JecnSearchResultBean;
import com.jecn.search.service.IJecnKeyWordSearchService;

public class JecnKeyWordSearchAction extends BaseAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2270289175112858626L;

	@Resource
	private IJecnKeyWordSearchService keyWordSearchService;
	/** 制度service */
	@Resource(name = "RuleServiceImpl")
	private IRuleService ruleService;
	@Resource
	private IStandardService standardService;

	/** 搜索的关键字 */
	private String searchKey;
	/** 搜索类型 */
	private int searchType = 0;
	/** 每页显示条数 */
	private int pageSize = 10;

	/** 当前页 */
	private int curPage = 1;

	/** 关键搜索结果，URL地址 */
	private String resourceUrl;

	public String keyWordSearch() {
		try {
			JecnSearchResultBean resultBean = keyWordSearchService.keyWordSearch(searchKey, searchType, pageSize
					* (curPage - 1), pageSize);
			JSONObject json = JSONObject.fromObject(resultBean);
			outJsonString(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public String keyWordSearchPagination() {
		try {
			JecnSearchResultBean resultBean = keyWordSearchService.keyWordSearchTotal(searchKey, searchType);
			int resultNum = resultBean.getResultNum();
			int totalPage = resultNum % pageSize == 0 ? resultNum / pageSize : resultNum / pageSize + 1;
			resultBean.setTotalPage(totalPage);
			JSONObject json = JSONObject.fromObject(resultBean);
			outJsonString(json.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String openResource() {
		try {
			Object objId = this.getRequest().getParameter("relatedId");
			Object objType = this.getRequest().getParameter("relatedType");
			Long relatedId = objId == null ? -1L : Long.valueOf(objId.toString());
			// 1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
			int relatedType = objType == null ? -1 : Integer.valueOf(objType.toString());
			switch (relatedType) {
			case 1:
				resourceUrl = "process.action?type=processMap&flowId=" + relatedId;
				break;
			case 2:
				resourceUrl = "process.action?type=process&flowId=" + relatedId;
				break;
			case 3:
				resourceUrl = "file.action?visitType=infoFile&fileId=" + relatedId;
				break;
			case 4:
				Rule rule = ruleService.getRule(relatedId);
				String ruleType = rule.getIsDir() == 1 ? "ruleModeFile" : "ruleFile";
				resourceUrl = "rule.action?type=" + ruleType + "&ruleId=" + relatedId;
				break;
			case 5:
				StandardBean standardBean = standardService.get(relatedId);
				resourceUrl = "standard.action?standardType=" + standardBean.getStanType() + "&standardId=" + relatedId;
				break;
			case 6:
				resourceUrl = "getRiskById.action?riskDetailId=" + relatedId;
				break;
			default:
				resourceUrl = "";
				break;
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return SUCCESS;
	}

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public int getSearchType() {
		return searchType;
	}

	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	public String getResourceUrl() {
		return resourceUrl;
	}
}
