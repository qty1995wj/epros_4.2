package epros.designer.gui.process.guide.explain;

import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;

import org.apache.commons.lang.StringUtils;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnFigureInoutSampleT;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.swing.JecnPanel;

/**
 * 活动说明
 * 
 * @author hyl
 * 
 */
public class JecnActiveDesc extends JecnFileDescriptionTable {

	ProcessOperationDialog processOperationDialog;

	public JecnActiveDesc(int index, String name, boolean isRequest, JecnPanel contentPanel,
			ProcessOperationDialog processOperationDialog, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.processOperationDialog = processOperationDialog;
		initTable();
		table.setTableColumn(2, 120);
		table.setTableColumn(2, 120);
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

	@Override
	protected void dbClickMethod() {
		int selectRow = this.getTable().getSelectedRow();
		List<JecnActivityShowBean> listAllActivityShow = processOperationDialog.getFlowOperationBean()
				.getListAllActivityShow();
		JecnActiveDescDialog dialog = new JecnActiveDescDialog(listAllActivityShow.get(selectRow),
				processOperationDialog);
		dialog.setVisible(true);
		table.setRowSelectionInterval(selectRow, selectRow);
		table.updateUI();
	}

	@Override
	protected JecnTableModel getTableModel() {

		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("activitiesNumbers"));
		title.add(JecnProperties.getValue("executiveRole"));
		title.add(JecnProperties.getValue("activityTitle"));
		title.add(JecnProperties.getValue("activityIndicatingThat"));
		// 输入
		title.add(JecnProperties.getValue("input"));
		// 输出
		title.add(JecnProperties.getValue("output"));
		title.add("roleUIID");
		// 办理时限
		title.add(JecnProperties.getValue("timeLimit"));
		// 获取操作说明数据
		Vector<Vector<String>> rowData = new Vector<Vector<String>>();
		for (JecnActivityShowBean activityShowBean : processOperationDialog.getFlowOperationBean()
				.getListAllActivityShow()) {
			rowData.add(getRowData(activityShowBean));
		}
		return new JecnTableModel(title, rowData);

	}

	private Vector<String> getRowData(JecnActivityShowBean activityShowBean) {
		Vector<String> row = new Vector<String>();
		JecnActiveData activeData = activityShowBean.getActiveFigure().getFlowElementData();
		row.add(activeData.getFlowElementUUID());
		row.add(activeData.getActivityNum());
		row.add(activityShowBean.getRoleName());
		row.add(activeData.getFigureText());
		row.add(activeData.getAvtivityShow());
		// row.add(activeData.getActivityInput());
		row.add(getActiveFileInput(activityShowBean, activeData.getActivityInput()));
		// row.add(activeData.getActivityOutput());
		row.add(getActiveFileOutPut(activityShowBean, activeData.getActivityOutput()));
		row.add(activityShowBean.getRoleUIID());
		if (activeData.getStatusValue() != null && activeData.getTargetValue() != null) {
			row.add(activeData.getStatusValue() + "/" + activeData.getTargetValue());
		}
		return row;
	}

	/**
	 * 输入
	 * 
	 * @param activityShowBean
	 * @return
	 */
	private String getActiveFileInput(JecnActivityShowBean activityShowBean, String input) {
		JecnActiveData activeData = activityShowBean.getActiveFigure().getFlowElementData();
		if (JecnConfigTool.useNewInout()) {
			return getNewInout(activeData.getListFigureInTs());
		} else {
			return getOldIn(input, activeData.getListJecnActivityFileT());
		}
	}

	private String getNewInout(List<JecnFigureInoutT> inouts) {
		if (JecnUtil.isEmpty(inouts)) {
			return "";
		}
		StringBuilder b = new StringBuilder();
		for (JecnFigureInoutT ss : inouts) {
			if (StringUtils.isNotBlank(ss.getName())) {
				b.append(ss.getName());
				b.append(":");
			}
			if (ss.getListSampleT() != null && !ss.getListSampleT().isEmpty()) {
				for (JecnFigureInoutSampleT bean : ss.getListSampleT()) {
					if (StringUtils.isNotBlank(bean.getFileName())) {
						b.append(bean.getFileName());
						b.append(":");
					}
				}
			} else {
				b.append("");
			}
			if (StringUtils.isNotBlank(ss.getExplain())) {
				b.append(ss.getExplain());
				b.append("\n");
			}
		}
		String result = b.toString();
		if (StringUtils.isNotBlank(result) && result.endsWith("\n")) {
			result = result.substring(0, result.length() - 2);
		}
		return result;
	}

	private String getOldIn(String input, List<JecnActivityFileT> listJecnActivityFileT) {
		StringBuilder builder = new StringBuilder();
		for (JecnActivityFileT jecnActivityFileT : listJecnActivityFileT) {
			if (StringUtils.isNotBlank(jecnActivityFileT.getFileName())) {
				builder.append(jecnActivityFileT.getFileName()).append("/");
			}
		}
		if (builder.length() > 0 && StringUtils.isNotBlank(input)) {
			return builder.append(input).toString();
		}
		if (builder.length() > 0 && StringUtils.isBlank(input)) {
			return builder.substring(0, builder.length() - 1);
		}
		return input;
	}

	/**
	 * 输出
	 * 
	 * @param activityShowBean
	 * @return
	 */
	private String getActiveFileOutPut(JecnActivityShowBean activityShowBean, String outPut) {
		JecnActiveData activeData = activityShowBean.getActiveFigure().getFlowElementData();
		if (JecnConfigTool.useNewInout()) {
			return getNewInout(activeData.getListFigureOutTs());
		} else {
			return getOldOut(outPut, activeData.getListModeFileT());
		}
	}

	private String getOldOut(String outPut, List<JecnModeFileT> listModeFileT) {
		StringBuilder builder = new StringBuilder();
		for (JecnModeFileT jecnModeFileT : listModeFileT) {
			builder.append(jecnModeFileT.getModeName()).append("/");
		}
		if (builder.length() > 0 && StringUtils.isNotBlank(outPut)) {
			return builder.append(outPut).toString();
		} else if (builder.length() > 0 && StringUtils.isBlank(outPut)) {
			return builder.substring(0, builder.length() - 1);
		}
		return outPut;
	}

	@Override
	protected int[] gethiddenCols() {
		if (JecnConfigTool.showActInOutBox() && JecnConfigTool.showActTimeLine()) {
			return new int[] { 0, 7 };
		} else if (JecnConfigTool.showActInOutBox() && !JecnConfigTool.showActTimeLine()) {
			return new int[] { 0, 7, 8 };
		} else if (!JecnConfigTool.showActInOutBox() && JecnConfigTool.showActTimeLine()) {
			return new int[] { 0, 5, 6, 7 };
		} else {
			return new int[] { 0, 5, 6, 7, 8 };
		}
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

}
