package com.jecn.webservice.crrs.xc.dataimport;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 信息通达webService 接口数据获取 时代新材人员同步
 * 
 *@author admin
 * @date 2016-7-1上午10:06:07
 */
public class sdxcWebServiceItem {

	private static final Logger log = Logger.getLogger(sdxcWebServiceItem.class);

	private static final String USERNAME = "epros";
	private static final String PASSWORD = "epros.2017";
	// 可以筛选接口提供的数据字段
	private static final String FILTER = "";
	// 错误的岗位数据编号
	private static final BigDecimal ERROR_POS_NUM = new BigDecimal("0");

	private static final String ERROR_EMPLOYEE_STATUS = "2";

	public static List<Organization_viewLine> syncSDXCOrgViews() throws Exception {
		// 分页读取客户数据
		BigInteger pageNo = new BigInteger("1");
		ComCrrcEsbMdmHrInfoAdminLocator publisherLocator = new ComCrrcEsbMdmHrInfoAdminLocator();
		HRComPublisherPortType proxy = publisherLocator.getHRComPublisherHttpSoap11Endpoint();
		Organization_viewDataPackage pac;
		List<Organization_viewLine> allLines = new ArrayList<Organization_viewLine>();
		do {
			log.info(pageNo + "调用接口查询开始 ！");
			pac = proxy.queryOrganization_viewData(USERNAME, PASSWORD, FILTER, pageNo);
			if (pac.getDataList().length == 0) {
				log.info("获取部门数据为空！");
			}
			log.info(pageNo + "调用接口获取数据成功 ！");
			allLines.addAll(Arrays.asList(pac.getDataList()));
			pageNo = pageNo.add(new BigInteger("1"));
			log.info(pageNo + "syncOrgViews： 获取组织数据成功！");
		} while (pac.getHasMore());
		log.info("syncOrgViews： 获取组织数据成功！");
		return allLines;
	}

	public static List<Iam_empbaseinfoLine> syncSDXCUserPosViews() throws Exception {
		// 分页读取客户数据
		BigInteger pageNo = new BigInteger("1");
		ComCrrcEsbMdmHrInfoAdminLocator publisherLocator = new ComCrrcEsbMdmHrInfoAdminLocator();
		HRComPublisherPortType proxy = publisherLocator.getHRComPublisherHttpSoap11Endpoint();
		Iam_empbaseinfoDataPackage pac;
		List<Iam_empbaseinfoLine> allLines = new ArrayList<Iam_empbaseinfoLine>();
		do {
			log.info(pageNo + "调用接口查询开始 ！" + System.currentTimeMillis());
			pac = proxy.queryIam_empbaseinfoData(USERNAME, PASSWORD, FILTER, pageNo);
			log.info(pageNo + "syncOrgViews： 获取组织数据成功！" + System.currentTimeMillis());
			Iam_empbaseinfoLine[] dataList = pac.getDataList();
			for (int i = dataList.length - 1; i >= 0; i--) {
				if (isSDXCDirty(dataList[i])) {
					dataList[i] = null;
				}
			}
			allLines.addAll(Arrays.asList(dataList));
			pageNo = pageNo.add(new BigInteger("1"));
		} while (pac.getHasMore());
		log.info("syncOrgViews： 获取人员数据成功！");
		return allLines;
	}

	private static boolean isSDXCDirty(Iam_empbaseinfoLine userPOSVIEWLine) {
		return !userPOSVIEWLine.getC_employeestatus().equals(ERROR_EMPLOYEE_STATUS)
				|| userPOSVIEWLine.getC_jobid().compareTo(ERROR_POS_NUM) == 0;
	}
}
