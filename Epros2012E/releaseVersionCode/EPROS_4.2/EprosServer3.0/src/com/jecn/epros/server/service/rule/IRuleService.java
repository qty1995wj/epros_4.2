package com.jecn.epros.server.service.rule;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.integration.JecnRuleRiskBeanT;
import com.jecn.epros.server.bean.integration.JecnRuleStandardBeanT;
import com.jecn.epros.server.bean.rule.AddRuleFileData;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.JecnRuleData;
import com.jecn.epros.server.bean.rule.JecnRuleOperationCommon;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.rule.sync.SyncRule;
import com.jecn.epros.server.bean.rule.sync.SyncRules;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.PublicFileBean;
import com.jecn.epros.server.webBean.PublicSearchBean;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.rule.RuleFileWebBean;
import com.jecn.epros.server.webBean.rule.RuleInfoBean;
import com.jecn.epros.server.webBean.rule.RuleSearchBean;
import com.jecn.epros.server.webBean.rule.RuleSystemListBean;

public interface IRuleService extends IBaseService<RuleT, Long> {
	/**
	 * @author yxw 2012-6-7
	 * @description:增加制度或制度目录
	 * @param rule
	 *            制度bean
	 * @return 主键
	 * @throws Exception
	 */
	public Long addRuleDir(RuleT rulet) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:制度目录下制度节点
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildRules(Long pid, Long projectId) throws Exception;

	/**
	 * 
	 * @author yxw 2012-11-28
	 * @description:浏览端树加载 获取子节点
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<WebTreeBean> getChildRulesForWeb(Long pid, Long projectId, Long peopleId, boolean isAdmin)
			throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:所有制度集合
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllRule(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-9-7
	 * @description:根据PID查询出制度目录
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildRuleDirs(Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:重命名
	 * @param newName
	 * @param id
	 * @param updatePersonId
	 *            修改人
	 * @param userType
	 *            0：管理员
	 * @throws Exception
	 */
	public void reName(String newName, Long id, Long updatePersonId, int userType) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:根据名称搜索
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchByName(String name, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点排序
	 * @param list
	 * @param pId
	 * @throws Exception
	 */
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点移动
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:删除制度
	 * @param listIds
	 * @return int 0 删除异常，1删除成功；2：存在节点处于任务中
	 * @throws Exception
	 */
	public void deleteRule(List<Long> listIds, Long updatePersonId) throws Exception;

	public void deleteFalseRule(Set<Long> setIds, Long updatePersonId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:上传制度文件
	 * @param ruleId
	 * @param ids
	 * @throws Exception
	 */
	public void addRuleFile(AddRuleFileData ruleFileData) throws Exception;

	/**
	 * 更新制度信息
	 * 
	 * @param ruleData
	 * @return
	 * @throws Exception
	 */
	public Long updateRule(JecnRuleData ruleData) throws Exception;

	/**
	 * @author yxw 2012-6-8
	 * @description:新建制度信息
	 * @param ruelBean
	 * @param ruleContentList
	 * @param ruleFileList
	 * @throws Exception
	 */
	public Long addRule(RuleT ruelBean, String posIds, String orgIds, Long modeId, Long updatePersonId)
			throws Exception;

	/**
	 * @author zhangchen Oct 8, 2010
	 * @description： 根据制度ID获得制度操作说明信息
	 * @param ruleId
	 * @return
	 * @throws Exception
	 */
	public List<JecnRuleOperationCommon> findRuleOperationShow(Long ruleId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：制度搜索
	 * @param ruleId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodes(Long ruleId, Long projectId) throws Exception;

	public RuleT getRuleT(Long id) throws Exception;

	public Rule getRule(Long id) throws Exception;

	/**
	 * @author yxw 2012-10-23
	 * @description:更新制度文件
	 * @param id
	 * @param fileId
	 * @param fileName
	 * @throws Exception
	 */
	public void updateRuleFile(long id, long fileId, String fileName, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-10-25
	 * @description:设置制度模板
	 * @param ruleId
	 * @param modeId
	 * @throws Exception
	 */
	public void updateRuleMode(long ruleId, long modeId, Long updatePersonId, Long longFlag) throws Exception;

	/**
	 * 下载制度操作说明
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param ruleId
	 *            制度ID
	 * @param isPub
	 *            true是发布，false是不发布
	 * @return
	 * @throws Exception
	 */
	public JecnCreateDoc getRuleFile(Long ruleId, boolean isPub) throws Exception;

	/**
	 * @author yxw 2012-11-13
	 * @description:撤销发布
	 * @param id
	 *            制度ID
	 * @param type
	 *            节点类型
	 * @param peopleId
	 * @throws Exception
	 */
	public void cancelRelease(Long id, TreeNodeType type, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-11-13
	 * @description:直接发布
	 * @param id
	 *            制度ID
	 * @param type
	 *            节点类型
	 * @param peopleId
	 * @throws Exception
	 */
	public void directRelease(Long id, TreeNodeType type, Long peopleId) throws Exception;

	/**
	 * 搜索制度
	 * 
	 * @param ruleSearchBean
	 *            搜索条件
	 * @param start
	 *            开始行数
	 * @param limit
	 *            每页行数
	 * @return
	 * @throws Exception
	 */
	public List<RuleInfoBean> searchRule(RuleSearchBean ruleSearchBean, int start, int limit) throws Exception;

	/**
	 * @author yxw 2013-2-21
	 * @description:根据目录ID查试制度列表
	 * @param pId
	 *            目录ID
	 * @param start
	 *            开始行数
	 * @param limit
	 *            每页行数
	 * @return
	 * @throws Exception
	 */
	public List<RuleInfoBean> searchRuleList(Long pId, boolean isAdmin, Long projectId, Long peopleId, int start,
			int limit) throws Exception;

	/**
	 * 搜索制度
	 * 
	 * @param ruleSearchBean
	 *            搜索条件
	 * @return
	 * @throws Exception
	 */
	public int searchRule(RuleSearchBean ruleSearchBean) throws Exception;

	/**
	 * @author yxw 2013-2-21
	 * @description:根据制度目录ID查询制度总数
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	public int searchRuleListTotal(Long pId, boolean isAdmin, Long projectId, Long peopleId) throws Exception;

	/**
	 * 制度基本信息
	 * 
	 * @author fuzhh Jan 24, 2013
	 * @param ruleId
	 *            制度ID
	 * @param isPub
	 *            true 查询正式表，false查询临时表
	 * @return
	 * @throws Exception
	 */
	public RuleInfoBean getRuleInfoBean(Long ruleId, boolean isPub, Long peopleId) throws Exception;

	/**
	 * 文件文控类型和关联ID获取文控信息版本记录
	 * 
	 * @param refId
	 *            关联ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @return List<JecnTaskHistoryNew>
	 * @throws Exception
	 */
	public List<JecnTaskHistoryNew> getJecnTaskHistoryNewList(Long refId, int type) throws Exception;

	/**
	 * 通过id获取 制度文控信息
	 * 
	 * @author fuzhh Nov 29, 2012
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnTaskHistoryNew getJecnTaskHistoryNew(Long id) throws Exception;

	/**
	 * @author yxw 2012-12-12
	 * @description:制度模板文件新增验证重名
	 * @param name
	 *            名称
	 * @param pid
	 *            上级Id
	 * @return
	 * @throws Exception
	 */
	public boolean validateAddName(String name, Long pid) throws Exception;

	/**
	 * 制度文件内容
	 * 
	 * @param ruleId
	 *            制度ID
	 * @param isPub
	 *            true是发布，false是不发布
	 * @return
	 * @throws Exception
	 */
	public RuleFileWebBean getRuleFileBean(Long ruleId, boolean isPub, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:公共制度查询总数
	 * @param publicSearchBean搜索条件bean
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public int getTotalPublicRule(PublicSearchBean publicSearchBean, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:公共制度查询
	 * @param publicSearchBean搜索条件bean
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<PublicFileBean> getPublicRule(PublicSearchBean publicSearchBean, int start, int limit, Long projectId)
			throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的制度
	 * @param attenTionSearchBean
	 *            查询条件
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public int getTotalAttenTionRule(AttenTionSearchBean attenTionSearchBean, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的制度
	 * @param attenTionSearchBean
	 *            查询条件
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<RuleInfoBean> getAttenTionRule(AttenTionSearchBean attenTionSearchBean, int start, int limit,
			Long projectId) throws Exception;

	/***************************************************************************
	 * 验证上传的文件是否已经存在
	 * 
	 * @param names
	 * @param pId
	 *            父节点ID
	 * @return
	 * @throws Exception
	 */
	public String[] validateAddNames(List<String> names, Long pId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:根据名称搜索制度文件，制度模板文件
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchISNotRuleDirByName(String name, Long projectId) throws Exception;

	/**
	 * 制度操作说明下载
	 * 
	 * @author fuzhh Jan 22, 2013
	 * @param ruleId
	 * @return
	 * @throws Exception
	 */
	public JecnCreateDoc downloadRuleFile(long ruleId, boolean isp) throws Exception;

	/**
	 * 制度清单数据
	 * 
	 * @author fuzhh Feb 22, 2013
	 * @param ruleId
	 *            制度ID
	 * @return
	 * @throws Exception
	 */
	public RuleSystemListBean findRuleSystemListBean(long ruleId, long projectId, int ruleSysLevel, boolean showAll)
			throws Exception;

	/**
	 * 制度相关风险
	 * 
	 * @param riskTList
	 *            制度相关风险集合
	 * @param ruleId
	 *            制度主键ID
	 */
	void editRuleRiskT(List<JecnRuleRiskBeanT> riskTList, Long ruleId) throws Exception;

	/**
	 * 
	 * 制度相关标准
	 * 
	 * @param standardTList
	 *            制度相关标准集合
	 * @param ruleId
	 *            主动主键ID
	 */
	void editRuleStandardT(List<JecnRuleStandardBeanT> standardTList, Long ruleId) throws Exception;

	/***************************************************************************
	 * 记录制度下载日志
	 * 
	 * @param ruleId
	 * @param peopleId
	 * @throws Exception
	 */
	public void saveRuleUseRecord(Long ruleId, Long peopleId) throws Exception;

	/***************************************************************************
	 * 根据制度ID获得制度相关文件
	 * 
	 * @param ruleId
	 *            制度ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnRuleOperationCommon> getRuleRelateFile(Long ruleId) throws Exception;

	/***************************************************************************
	 * 获取当前节点所在级别
	 * 
	 * @param ruleId
	 * @return
	 * @throws Exception
	 */
	public int findRuleSysSevel(Long ruleId) throws Exception;

	/**
	 * 更新本地上传方式的制度文件
	 * 
	 * @param ruleT
	 * @throws Exception
	 */
	public Long updateLocalRuleFile(RuleT ruleT) throws Exception;

	/**
	 * 打开本地上传方式的制度文件
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public FileOpenBean g020OpenRuleFile(long id) throws Exception;

	/**
	 * 获得本地上传的制度文件的附件
	 * 
	 * @author hyl
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	public G020RuleFileContent getG020RuleFileContent(Long fileId) throws Exception;

	Map<String, String> getRuleNameAndContentId(long ruleId, boolean isPub) throws Exception;

	/**
	 * 根据制度id获得正式表关联的那个版本的文件内容
	 * 
	 * @param fileId
	 * @param isp
	 * @return
	 * @throws Exception
	 */
	public G020RuleFileContent getG020RuleFileContentByRuleId(Long fileId, boolean isp) throws Exception;

	List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception;

	List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception;

	/**
	 * 流程相关制度
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getRuleTreeBeanByFlowId(Long flowId) throws Exception;

	/**
	 * 制度相关制度
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getRuleTreeBeanByRuleId(Long flowId) throws Exception;

	List<JecnTreeBean> getStandardizedFiles(Long ruleId) throws Exception;

	Map<EXCESS, List<FileOpenBean>> ruleFilesDownLoad(Long ruleId, boolean isPub, Long peopleId) throws Exception;

	FileOpenBean openFile(Long fileId, boolean isPub) throws Exception;

	List<Object[]> getRuleByWebService(String loginName, int start, int pageSize) throws Exception;

	public List<G020RuleFileContent> getG020RuleFileContents(Long fileId);

	public void updateRecovery(long fileId, long versionId, long updatePersonId) throws Exception;

	public void deleteVersion(List<Long> listIds, Long fileId, long userId) throws Exception;

	public FileOpenBean openVersion(Long ruleId, Long versionId) throws Exception;

	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pId) throws Exception;

	public List<JecnTreeBean> getAllFilesByName(String name, Long projectId, Set<Long> ruleIds, boolean isAdmin)
			throws Exception;

	public void getRecycleFileIds(List<Long> listIds, Long projectId) throws Exception;

	public void updateRecycleFile(List<Long> ids, Long projectId) throws Exception;

	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception;

	Long createRelatedFileDirByPath(Long id, Long projectId, Long createPeopleId) throws Exception;

	Long getRelatedFileId(Long ruleId) throws Exception;

	public boolean isNotAbolishOrNotDelete(Long id) throws Exception;

	public boolean validateAddNameDirRepeat(Long pid, String name);

	public String validateNamefullPath(String ruleName, Long id, int type);

	public String[] validateNamefullPath(List<String> names, Long id, int type);

	public void batchReleaseNotRecord(List<Long> ids, TreeNodeType treeNodeType, Long peopleId);

	public void batchCancelRelease(List<Long> ids, TreeNodeType treeNodeType, Long peopleId);

	public void syncRules(SyncRules syncRule) throws Exception;

	public List<JecnTreeBean> getChildRules(Long pid, Long projectId, int... delState) throws Exception;
}
