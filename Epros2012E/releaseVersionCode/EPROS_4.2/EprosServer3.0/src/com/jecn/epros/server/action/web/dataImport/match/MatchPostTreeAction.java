package com.jecn.epros.server.action.web.dataImport.match;

import java.util.List;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.dataImport.match.IMatchPostTreeService;
import com.jecn.epros.server.service.dataImport.match.constant.TmpMatchTreePostBean;
import com.jecn.epros.server.webBean.WebTreeBean;

/**
 * 岗位匹配临时岗位树Action
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Apr 8, 2013 时间：10:07:33 AM
 */
public class MatchPostTreeAction extends BaseAction {
	private IMatchPostTreeService matchTreeService;
	/** 编号 */
	private String strNumber = "0";
	/** 名称搜索 */
	private String searchName = "";
	/** 树节点 */
	private String node;
	private static final Logger log = Logger
			.getLogger(MatchPostTreeAction.class);

	/**
	 * 获取HR岗位树结构
	 * 
	 * @return
	 */
	public String childsMatchPos() {
		try {
			List<WebTreeBean> listWebTreeBean = matchTreeService
					.getChildMatchOrgsAndPositon(strNumber, getProjectId());
			JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
			outJsonString(jsonArray.toString());
		} catch (Exception e) {
			log.error("", e);
		}

		return null;
	}

	/**
	 * 岗位选择查询数据
	 * 
	 * @author fuzhh Nov 30, 2012
	 * @return
	 */
	public String matchPosSearch() {
		try {
			List<TmpMatchTreePostBean> list = matchTreeService
					.searchPositionByName(searchName, getProjectId());
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("查询岗位数据错误！", e);
		}
		return null;
	}

	/**
	 * 基准岗位模糊查询
	 * 
	 * @return
	 */
	public String matchBaseSearch() {
		try {
			List<TmpMatchTreePostBean> list = matchTreeService
					.searchBaseNameByName(searchName);
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("查询岗位数据错误！", e);
		}
		return null;
	}

	/**
	 * 获取HR基准岗位树结构
	 * 
	 * @return
	 */
	public String childsMatchBasePos() {
		try {
			List<WebTreeBean> listWebTreeBean = matchTreeService
					.getChildMatchBasePos();
			JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
			outJsonString(jsonArray.toString());
		} catch (Exception e) {
			log.error("", e);
		}

		return null;
	}

	public List<JecnTreeBean> getChildOrgsAndPositon(Long id, Long projectId,
			boolean isLeaf) throws Exception {
		return null;

	}

	public IMatchPostTreeService getMatchTreeService() {
		return matchTreeService;
	}

	public void setMatchTreeService(IMatchPostTreeService matchTreeService) {
		this.matchTreeService = matchTreeService;
	}

	public String getStrNumber() {
		return strNumber;
	}

	public void setStrNumber(String strNumber) {
		this.strNumber = strNumber;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.strNumber = node.split("_")[0];
		this.node = node;
	}
}
