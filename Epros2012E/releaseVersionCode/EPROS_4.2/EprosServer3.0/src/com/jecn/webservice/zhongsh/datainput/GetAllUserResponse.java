
package com.jecn.webservice.zhongsh.datainput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAllUserResult" type="{http://tempuri.org/}ArrayOfUser" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAllUserResult"
})
@XmlRootElement(name = "GetAllUserResponse")
public class GetAllUserResponse {

    @XmlElement(name = "GetAllUserResult")
    protected ArrayOfUser getAllUserResult;

    /**
     * 获取getAllUserResult属性的值。
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfUser }
     *     
     */
    public ArrayOfUser getGetAllUserResult() {
        return getAllUserResult;
    }

    /**
     * 设置getAllUserResult属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfUser }
     *     
     */
    public void setGetAllUserResult(ArrayOfUser value) {
        this.getAllUserResult = value;
    }

}
