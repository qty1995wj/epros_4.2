package epros.designer.util;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;

import jecntool.cmd.JecnCmd;
import jecntool.cmd.ResultBean;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnPubNoteDialog;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.tree.JecnTreeNode;
import epros.draw.data.JecnFlowMapData;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;

public class JecnUtil {
	private static Logger log = Logger.getLogger(JecnUtil.class);
	/** 用于时间转换 */
	public static final DateFormat df = new SimpleDateFormat(JecnProperties.getValue("dateFormat"));
	public static final JLabel MUST = new CommonJlabelMustWrite();

	/**
	 * @author yxw 2012-5-9
	 * @description:通过字符串转化成Enum
	 * @param str
	 * @return
	 */
	public static TreeNodeType getTreeNodeType(String str) {
		try {
			return Enum.valueOf(TreeNodeType.class, str);
		} catch (Exception e) {
			log.error(str + ": JecnUtil getTreeNodeType is error");
			return null;
		}

	}

	public static int formatTitleTypeToInt(String str) {
		if (str.equals(JecnProperties.getValue("content"))) {
			return 0;
		} else if (str.equals(JecnProperties.getValue("fileForm"))) {
			return 1;
		} else if (str.equals(JecnProperties.getValue("flowForm"))) {
			return 2;
		} else if (str.equals(JecnProperties.getValue("flowMapForm"))) {
			return 3;
		} else if (str.equals(JecnProperties.getValue("standarForm"))) {
			return 4;
		} else if (str.equals(JecnProperties.getValue("riskForm"))) {
			return 5;
		} else if (str.equals(JecnProperties.getValue("ruleForm"))) {
			return 6;
		}
		return 0;
	}

	public static int formatRequiredTypeToInt(String str) {
		if (str.equals(JecnProperties.getValue("task_emptyBtn"))) {
			return 0;
		} else if (str.equals(JecnProperties.getValue("task_notEmptyBtn"))) {
			return 1;
		}
		return 0;
	}

	/**
	 * 把文件转换成byte数组
	 * 
	 * @param path
	 * @return
	 */
	public static byte[] changeFileToByte(String path) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(new File(path));
			byte[] content = new byte[fis.available()];
			fis.read(content);
			return content;
		} catch (IOException e) {
			log.error("JecnUtil changeFileToByte is error", e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					log.error("JecnUtil changeFileToByte is error");
				}
			}
		}
		return null;
	}

	/**
	 * @author yxw 2012-6-1
	 * @description:根据把文件路径，得到文件名称
	 * @param filePath
	 * @return
	 */
	public static String getFileName(String filePath) {
		String inString = ".*\\\\(.*\\..*)";
		Pattern pattern = Pattern.compile(inString);
		Matcher matcher = pattern.matcher(filePath);
		boolean b = matcher.matches();
		if (b) {

			return matcher.group(1);
		}
		return filePath;
	}

	/**
	 * @author yxw 2012-8-31
	 * @description:
	 * @param fileName
	 * @param content
	 * @return 返回下载的临时文件路径
	 */
	public static String downFile(String fileName, byte[] content) {
		if (null == content) {
			log.error("JecnUtil downFile is error  fileName=" + fileName);
			return null;
		}
		String fileDirectory = System.getProperty("user.dir") + "//tempFiles//";
		File fileDir = new File(fileDirectory);
		if (!fileDir.exists()) {
			fileDir.mkdir();
		} else {
			File[] delFile = fileDir.listFiles();
			for (int i = 0; i < delFile.length; i++) {
				delFile[i].delete();
			}
		}
		String filePath = fileDirectory + "\\" + fileName;
		File file = new File(filePath);
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			fos.write(content);
			fos.flush();
		} catch (Exception ex) {
			JecnOptionPane.showMessageDialog(null, ex.getLocalizedMessage());
			log.error("JecnUtil downFile is error", ex);
			return null;
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				log.error("JecnUtil downFile is error", e);
			}
		}
		return filePath;
	}

	/**
	 * @author yxw 2012-8-31
	 * @description:
	 * @param fileName
	 * @param content
	 * @return 返回下载的临时文件路径
	 */
	public static void downFileOpen(String filePath, byte[] content) {
		if (null == content) {
			log.error("JecnUtil downFileOpen is error!");
			return;
		}

		File file = new File(filePath);
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			fos.write(content);
			fos.flush();
		} catch (Exception ex) {
			log.error("JecnUtil downFileOpen is error", ex);
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}

			} catch (IOException e) {
				log.error("JecnUtil downFileOpen is error", e);
			}
		}
	}

	/**
	 * @author yxw 2012-5-31
	 * @description:文件打开
	 * @param fileName
	 * @param content
	 */
	public static void openFile(String fileName, byte[] content) {
		String fileDirectory = System.getProperty("user.dir") + "//tempFiles//";
		File fileDir = new File(fileDirectory);
		String filePath = fileDirectory + "\\" + fileName;
		if (content == null) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("fileHasDel"));
			return;
		}
		if (fileDir.exists()) {
			File[] delFile = fileDir.listFiles();
			for (int i = 0; i < delFile.length; i++) {
				delFile[i].delete();
			}
			File file = new File(filePath);
			if (file.exists()) {
				if (!file.renameTo(file)) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("fileOpened"));
				} else {
					openLocalFile(filePath);
				}
			} else {
				downFileOpen(filePath, content);
				if (null != filePath) {
					openLocalFile(filePath);
				}
			}
		} else {
			fileDir.mkdir();
			downFileOpen(filePath, content);
			if (null != filePath) {
				openLocalFile(filePath);
			}
		}

	}

	/**
	 * @author yxw 2012-8-31
	 * @description:本地文件打开
	 * @param filePath
	 */
	public static void openLocalFile(String filePath) {
		ResultBean retBean = JecnCmd.openFile(filePath);
		if (retBean.getResultState() == -1) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("openFileError"));
		}
	}

	/**
	 * @author yxw 2012-6-1
	 * @description:获取文件的扩展名
	 * @param fileName
	 * @return
	 */
	public static String getFileExtension(String fileName) {
		return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
	}

	/**
	 * @author yxw 2012-6-12
	 * @description:根据节点类型，转换成数据可以保存的int类型，用于设置查阅权限功能
	 * @param type
	 * @return
	 */
	public static int formatTreeNodeTypeForAccess(TreeNodeType type) {
		int i = 0;
		switch (type) {
		case process:

		case processMap:
			i = 0;
			break;
		case file:
			i = 1;
			break;
		case standard:
		case standardDir:
			i = 2;
			break;

		case ruleFile:
		case ruleDir:
			i = 3;
			break;
		default:
			break;
		}

		return i;
	}

	/**
	 * @author yxw 2012-8-27
	 * @description:获取MAC地址
	 * @return
	 */
	public static String getMacAddress() {
		try {
			return getLocalMac(InetAddress.getLocalHost());
		} catch (SocketException e) {
			log.error("getMacAddress error", e);
			e.printStackTrace();
		} catch (UnknownHostException e) {
			log.error("getMacAddress error", e);
			e.printStackTrace();
		}
		return null;
	}

	private static String getLocalMac(InetAddress ia) throws SocketException {
		// 获取网卡，获取地址
		byte[] mac = NetworkInterface.getByInetAddress(ia).getHardwareAddress();
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < mac.length; i++) {
			if (i != 0) {
				sb.append("-");
			}
			// 字节转换为整数
			int temp = mac[i] & 0xff;
			String str = Integer.toHexString(temp);
			if (str.length() == 1) {
				sb.append("0" + str);
			} else {
				sb.append(str);
			}
		}
		return sb.toString().toUpperCase();
	}

	/**
	 * >>>>>>> .merge-right.r35421
	 * 
	 * @author yxw 2012-7-11
	 * @description:Date 转换成 String 格式的时间
	 * @param date
	 * @return
	 */
	public static String formatDateToString(Date date) {
		if (null != date) {
			return df.format(date);
		}
		return null;
	}

	/**
	 * @author yxw 2012-8-30
	 * @description:图片过滤描述
	 * @return
	 */
	public static String getImageDescription() {
		return "gif,png,jpg,jpeg";
	}

	/**
	 * @author yxw 2012-8-30
	 * @description: 图片文件扩展名集合。
	 * @return
	 */
	public static String[] getImagetExtensions() {
		return new String[] { "gif", "png", "bmp", "jpg", "jpeg" };
	}

	public static boolean isImage(String name) {
		int i = name.lastIndexOf('.');
		String extension = name.substring(i + 1, name.length()).toLowerCase();
		return (getImageDescription().indexOf(extension) == -1) ? false : true;
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：用户密码加密
	 * @param password
	 * @return
	 */
	public static String getSecret(String password) {
		String value;
		char[] A1 = new char[40];
		char[] A2 = new char[40];
		int t1[] = { 6, 12, 5, 9, 10, 0, 13, 8, 15, 3, 14, 4, 2, 11, 1, 7 };
		char t3[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		int i = 0, j = 0, x, y, k;
		int key = 32;
		byte temp;
		A1 = password.toCharArray();
		k = password.length();
		while (j < k) {
			temp = (byte) A1[j];
			temp = (byte) ((temp) ^ key);
			A1[j] = (char) temp;
			x = temp / 16;
			y = temp % 16;
			A2[i++] = t3[t1[x]];
			A2[i++] = t3[t1[y]];
			j++;
		}
		A2[i] = '\0';
		value = String.valueOf(A2).trim();
		return (value);
	}

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：用户密码解密
	 * @param password
	 * @return
	 */

	public static String getDesecret(String password) {

		String value;
		char[] A1 = new char[40];
		char[] fname1 = new char[40];
		char[] A2 = new char[40];
		int t2[] = { 5, 14, 12, 9, 11, 2, 0, 15, 7, 3, 4, 13, 1, 6, 10, 8 };
		char t3[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

		int i = 0, j = 0, k, p, q;
		int key = 32;
		int temp;
		fname1 = password.toCharArray();
		k = password.length();
		while ((i < k) && (fname1[i] != '\0')) {
			A1[j] = fname1[i];
			p = 0;
			while ((p < 16) && (A1[j] != t3[p++]))
				;
			A2[j] = fname1[i + 1];
			q = 0;
			while ((q < 16) && (A2[j] != t3[q++]))
				;
			A1[j] = (char) (t2[p - 1] * 16 + t2[q - 1]);
			temp = A1[j];
			temp = temp ^ key;
			A1[j] = (char) temp;
			i = i + 2;
			j++;
		}
		A1[j] = 0;
		value = String.valueOf(A1).trim();
		return (value);
	}

	/**
	 * 根据节点类型返回节点对应的关联
	 * 
	 * @param nodeType
	 * @return 关联类型 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
	 */
	public static int getRelaType(TreeNodeType nodeType) {
		if (nodeType.toString().equals("standard")) {// 文件标准
			return 1;
		} else if (nodeType.toString().equals("standardClause")) {// 标准条款
			return 4;
		} else if (nodeType.toString().equals("standardClauseRequire")) {// 条款要求
			return 5;
		}
		return -1;
	}

	/**
	 * 根据标准类型返回标准文件类型名称
	 * 
	 * @param nodeType
	 * @return String标准类型名称
	 */
	public static String getTypeName(TreeNodeType nodeType) {
		if (nodeType == null) {
			return "";
		}
		if (nodeType.toString().equals("standard")) {// 文件标准
			return JecnProperties.getValue("fileStandar");
		} else if (nodeType.toString().equals("standardClause")) {// 标准条款
			return JecnProperties.getValue("standarClause");
		} else if (nodeType.toString().equals("standardClauseRequire")) {// 条款要求
			return JecnProperties.getValue("stanClauseRequire");
		}
		return "";
	}

	/**
	 * 
	 * 
	 * @param relatType
	 *            关联类型 '1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求'
	 * @return TreeNodeType
	 */
	public static TreeNodeType getNodeTypeByRelaType(int relatType) {
		TreeNodeType nodeType = null;
		switch (relatType) {
		case 1:
			nodeType = TreeNodeType.standard;
			break;
		case 4:
			nodeType = TreeNodeType.standardClause;
			break;
		case 5:
			nodeType = TreeNodeType.standardClauseRequire;
			break;
		}
		return nodeType;
	}

	// 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
	public static String FILE_STANDARD = JecnProperties.getValue("fileStandar");// "文件标准";
	public static String FLOW_STANDARD = JecnProperties.getValue("flowStandar");// "流程标准";
	public static String MAP_STANDARD = JecnProperties.getValue("flowMapStandar");// "流程地图标准";
	public static String CLAUSE_STANDARD = JecnProperties.getValue("standarClause");// "标准条款";
	public static String CLAUSE_REQUIRE = JecnProperties.getValue("stanClauseRequire");// "条款要求";

	/**
	 * 根据标准类型返回对象的类型名称
	 * 
	 * @param relaType
	 * @return
	 */
	public static String getStandardTypeName(int relaType) {
		switch (relaType) {
		case 1:
			return FILE_STANDARD;
		case 2:
			return FLOW_STANDARD;
		case 3:
			return MAP_STANDARD;
		case 4:
			return CLAUSE_STANDARD;
		case 5:
			return CLAUSE_REQUIRE;
		}
		return "";
	}

	/**
	 * 根据名称返回类型
	 * 
	 * @param name
	 * @return
	 */
	public static int getTypeByName(String name) {
		int type = 0;
		if (name.equals(FILE_STANDARD)) {
			type = 1;
		} else if (name.equals(FLOW_STANDARD)) {
			type = 2;
		} else if (name.equals(MAP_STANDARD)) {
			type = 3;
		} else if (name.equals(CLAUSE_STANDARD)) {
			type = 4;
		} else if (name.equals(CLAUSE_REQUIRE)) {
			type = 5;
		}
		return type;
	}

	/**
	 * 获取流程备份路径
	 * 
	 * @author weidp
	 * @param flowMap
	 * @date 2014-10-28 上午09:41:24
	 * @return
	 */
	public static String getFlowBakPath(JecnFlowMapData flowMap) {
		if (flowMap == null) {
			throw new IllegalArgumentException("flowMapData is null");
		}
		// 获取项目备份路径
		String flowBakPath = getProjectPath() + "\\saveFiles\\";

		switch (flowMap.getMapType()) {
		// 流程地图
		case totalMap:
			flowBakPath += "totalMap\\" + flowMap.getFlowId() + "\\";
			break;
		// 流程图
		case partMap:
			flowBakPath += "partMap\\" + flowMap.getFlowId() + "\\";
			break;
		// 组织图
		case orgMap:
			flowBakPath += "orgMap\\" + flowMap.getFlowId() + "\\";
			break;
		}
		return flowBakPath;
	}

	/**
	 * 获取项目根路径
	 * 
	 * @author weidp
	 * @date 2014-10-28 下午02:26:43
	 * @return
	 */
	public static String getProjectPath() {
		String projectPath = System.getProperty("user.dir").replaceAll("%20", "");
		return projectPath.replaceAll("/", "\\");
	}

	/**
	 * 格式化Object数据为字符串，并替换字符串内的内容只保留小数点后一位。 [e.g.] 6.5555d convert to '6.5'
	 * 
	 * @param val
	 * @return
	 */
	public static String convertDoubleToStringWithFormat(Object val) {
		if (val == null) {
			return null;
		}
		DecimalFormat format = new DecimalFormat("#.#");
		return format.format(Double.valueOf(val.toString())).replaceAll("\\.0+$", "");
	}

	/**
	 * list 转换成 string
	 * 
	 * @param list
	 * @return
	 */
	public static String getIds(List<JecnTreeBean> list) {
		String ids = "";
		for (JecnTreeBean jecnTreeBean : list) {
			ids += jecnTreeBean.getId() + ",";
		}
		if (!"".equals(ids)) {
			ids = ids.substring(0, ids.length() - 1);
		}
		return ids;
	}

	/**
	 * 检查两个字符串是否相同
	 * 
	 * @param newStr
	 * @param oldStr
	 * @return
	 */
	public static boolean isChangeString(String newStr, String oldStr) {
		newStr = newStr == null ? "" : newStr;
		oldStr = oldStr == null ? "" : oldStr;
		if (!oldStr.equals(newStr)) {
			return true;
		}
		return false;
	}

	/**
	 * 检查两个List<Long>是否相同
	 * 
	 * @param newList
	 * @param oldList
	 * @return
	 */
	public static boolean isChangeListLong(List<Long> newList, List<Long> oldList) {
		if (newList.size() != oldList.size()) {
			return true;
		}
		for (Long newId : newList) {
			boolean isExist = false;
			for (Long oldId : oldList) {
				if (newId.equals(oldId)) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}

		return false;
	}

	public static String getKpiFrom(int index) {
		String[] arr = new String[] { JecnProperties.getValue("twoLevelTarget"),
				JecnProperties.getValue("personPBCTarget"), JecnProperties.getValue("otherImpWork") };
		return arr[index];
	}

	public static String nullToEmpty(Object text) {
		if (text == null) {
			return "";
		}
		return text.toString().trim();
	}

	/**
	 * 在审批过程中非管理员不可以修改文件等，所以将修改菜单都隐藏
	 * 
	 * @param selectNode
	 * @param items
	 */
	public static void setMenuDisableThenInTaskExceptAuthorAdmin(JecnTreeNode selectNode, JComponent... items) {
		if (!JecnDesignerCommon.isAuthorAdmin() && !isBusinessAdmin(selectNode.getJecnTreeBean().getTreeNodeType())
				&& !selectNode.getJecnTreeBean().canEdit()) {
			for (JComponent item : items) {
				if (item == null) {
					continue;
				}
				item.setVisible(false);
			}
		}
	}

	private static boolean isBusinessAdmin(TreeNodeType treeNodeType) {
		if (treeNodeType == TreeNodeType.file || treeNodeType == TreeNodeType.fileDir) {
			return JecnConstants.loginBean.isDesignFileAdmin();
		} else if (treeNodeType == TreeNodeType.ruleDir || treeNodeType == TreeNodeType.ruleFile
				|| treeNodeType == TreeNodeType.ruleModeFile) {
			return JecnConstants.loginBean.isDesignRuleAdmin();
		} else if (treeNodeType == TreeNodeType.process || treeNodeType == TreeNodeType.processFile
				|| treeNodeType == TreeNodeType.processMap) {
			return JecnConstants.loginBean.isDesignProcessAdmin();
		}
		return false;
	}

	public static JMenuItem createPubNoteMenu(final JecnTreeNode node) {
		JMenuItem menu = new JMenuItem(JecnProperties.getValue("releaseReminder"));
		menu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnPubNoteDialog dialog = new JecnPubNoteDialog(node);
				dialog.setVisible(true);
			}
		});
		return menu;
	}

	public static int getTypeByNode(TreeNodeType treeNodeType) {
		String nodeStr = treeNodeType.toString();
		if (nodeStr.startsWith("process")) {
			return 0;
		} else if (nodeStr.startsWith("file")) {
			return 1;
		} else if (nodeStr.startsWith("rule")) {
			return 2;
		}
		throw new IllegalArgumentException("un suport type " + treeNodeType);
	}

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：创建文件的路径
	 * @param filePath
	 * @param bytes
	 * @throws IOException
	 */
	public static void createFile(String filePath, byte[] bytes) throws IOException {
		File file = new File(filePath);

		FileOutputStream out = null;
		try {
			file.createNewFile();
			out = new FileOutputStream(file, false);
			out.write(bytes);
			out.flush();
		} catch (IOException ex) {
			log.error("写出异常", ex);
			throw ex;
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				log.error("关闭输出流异常", ex);
			}
		}
	}

	public static boolean isNotEmpty(Collection<?> col) {
		return col != null && !col.isEmpty();
	}

	public static boolean isEmpty(Collection<?> col) {
		return !isNotEmpty(col);
	}

	public static String concat(Collection<? extends Object> cols, CharSequence delimiter) {
		if (cols == null || cols.isEmpty()) {
			return "";
		}
		StringBuilder buf = new StringBuilder();
		int size = cols.size();
		int index = 0;
		for (Object o : cols) {
			index++;
			if (o == null) {
				continue;
			}
			buf.append(o);
			if (index != size) {
				buf.append(delimiter);
			}
		}
		return buf.toString();
	}

	/**
	 * 使用默认浏览器打开链接
	 * 
	 * @param webSite
	 */
	public static void runBroswer(String webSite) {
		try {
			Desktop desktop = Desktop.getDesktop();
			if (desktop.isDesktopSupported() && desktop.isSupported(Desktop.Action.BROWSE)) {
				URI uri = new URI(webSite);
				desktop.browse(uri);
			} else {
				JecnOptionPane.showMessageDialog(null, "环境不支持");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			log.error("环境不支持", ex);
		} catch (URISyntaxException ex) {
			ex.printStackTrace();
			log.error("链接不正确", ex);
		}
	}

	/**
	 * URL打开 地址验证和浏览器跳转
	 * 
	 * @param textValue
	 */
	public static void openUrl(String textValue) {
		if (StringUtils.isBlank(textValue)) {
			return;
		}
		Pattern pattern = Pattern
				.compile("^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\/])+$");
		if (!pattern.matcher(textValue).matches()) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("systemLinkSpecification"));
			return;
		}
		JecnUtil.runBroswer(textValue);// 调用浏览器打开链接
	}

	public static <T> T nullToDeafult(T obj, T def) {
		if (obj == null) {
			return def;
		}
		return obj;
	}

	/**
	 * 九新要求除了管理员外其他人没有审批、发布等菜单
	 * 
	 * @param selectNode
	 * @param menus
	 */
	public static void setMenuDisableJiuxin(JecnTreeNode selectNode, JMenuItem... menus) {
		if (!JecnConfigTool.isJiuXinLoginType()) {
			return;
		}
		if (!JecnDesignerCommon.isAdmin()) {
			for (JMenuItem m : menus) {
				m.setVisible(false);
			}
		}
	}

	public static String getName(List<JecnDictionary> dics) {
		if (JecnUtil.isEmpty(dics)) {
			return "";
		}
		return getName(dics.get(0));
	}

	public static String getName(JecnDictionary dic) {
		if (JecnResourceUtil.CH) {
			return dic.getTransName();
		}
		return dic.getEnTransName();
	}

	public static Long getUserId() {
		return JecnConstants.getUserId();
	}

	/**
	 * 拼接冒号
	 * 
	 * @param name
	 * @return
	 */
	public static String concatColon(String name) {
		if (name.endsWith(JecnConfigContents.EN_COLON) || name.endsWith(JecnConfigContents.CH_COLON)) {
			return name;
		}
		return JecnResourceUtil.CH ? (name + JecnConfigContents.CH_COLON) : (name + JecnConfigContents.EN_COLON);
	}

	public static boolean isSame(Object one, Object two) {
		if (one == null && two == null) {
			return true;
		}
		if (one instanceof String && two instanceof String) {
			return nullToEmpty(one).trim().equals(nullToEmpty(two).trim());
		}
		if (one != null && two != null) {
			if (one == two) {
				return true;
			}
			return one.equals(two);
		}
		return false;
	}

	public static boolean isUpdate(Object one, Object two) {
		return !isSame(one, two);
	}

	public static String trimColon(String str) {
		return str.replace(":", "").replace("：", "");
	}

	public static String mustTip(boolean must, String name) {
		if (!must) {
			return "";
		}
		String split = JecnResourceUtil.EN ? " " : "";
		return name + split + JecnProperties.getValue("notNull");
	}

	public static String mustTipTrimColon(boolean must, String name) {
		return mustTip(must, trimColon(name));
	}

	public static void mustTip(JLabel sourceLab, JLabel verfyLab) {
		verfyLab.setText(mustTipTrimColon(true, sourceLab.getText()));
	}

	public static void mustTip(String name, JLabel verfyLab) {
		verfyLab.setText(mustTipTrimColon(true, name));
	}

	public static String mustTip(String name) {
		return mustTip(true, name);
	}

	public static String mustTip(JLabel label) {
		return mustTip(label.getText());
	}

}
