package epros.designer.tree;

import java.util.List;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/**
 * @author zhangchen Apr 28, 2012
 * @description：点击节点刷新生产子节点
 */
public abstract class JecnHighEfficiencyTree extends JecnTree {
	/**是否执行Tree的展开事件  true 展开 false 不展开 */
	private boolean  isAllowExpand=true;
	public boolean isAllowExpand() {
		return isAllowExpand;
	}

	public void setAllowExpand(boolean isAllowExpand) {
		this.isAllowExpand = isAllowExpand;
	}

	public JecnHighEfficiencyTree() {
		this.addTreeExpansionListener(getTreeExpansionListener(this));
	}
	
	public JecnHighEfficiencyTree(Long startId){
		super(startId);
		this.addTreeExpansionListener(getTreeExpansionListener(this));
	}
	
	public JecnHighEfficiencyTree(List<Long> listIds) {
		super(listIds);
		this.addTreeExpansionListener(getTreeExpansionListener(this));
	}
	
	public JecnHighEfficiencyTree(List<Long> listIds,TreeNodeType nodeType) {
		super(listIds,nodeType);
		this.addTreeExpansionListener(getTreeExpansionListener(this));
	}

	/**
	 * 
	 * @author zhangchen May 2, 2012
	 * @description：获得Tree展开的监听器
	 * @return
	 */
	public abstract JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree);

}
