package epros.draw.event.flowElement;

import java.awt.event.ActionEvent;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;
import epros.draw.gui.workflow.dialog.JecnActiveDetailsDialog;
import epros.draw.gui.workflow.dialog.JecnFlowElementAttrDialog;
import epros.draw.gui.workflow.dialog.JecnRoleDetailsDialog;
import epros.draw.gui.workflow.popup.JecnMenuItem;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 流程元素右键菜单处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElemPopupMenuProcess {

	public void menuItemActionPerformed(ActionEvent e,
			ToolBarElemType toolBarElemType) {
		if (toolBarElemType == null) {
			return;
		}
		switch (toolBarElemType) {
		case flowElemAttr:// 元素属性
			// 点击的菜单项
			JecnMenuItem flowElemRightMenuItem = (JecnMenuItem) e.getSource();
			if (flowElemRightMenuItem.getElementPanel() == null) {
				break;
			}
			JecnFlowElementAttrDialog dialog = new JecnFlowElementAttrDialog(
					flowElemRightMenuItem.getElementPanel());
			dialog.setVisible(true);
			break;
		case activeDetails:// 活动明细
			// 点击的菜单项
			JecnMenuItem activeDetailsMenuItem = (JecnMenuItem) e.getSource();

			if (activeDetailsMenuItem.getElementPanel() == null) {
				break;
			}
			// 点击的流程元素
			JecnBaseFlowElementPanel elementPanel = activeDetailsMenuItem
					.getElementPanel();

			if (elementPanel instanceof JecnBaseActiveFigure) {// 活动
				JecnBaseActiveFigure activeFigure = (JecnBaseActiveFigure) elementPanel;

				if (EprosType.eprosDesigner == JecnSystemStaticData
						.getEprosType()) {// 设计器
					JecnDesignerProcess.getDesignerProcess()
							.showActiveDetailsDialog(activeFigure);

				} else {// 画图工具
					JecnActiveDetailsDialog activeDetailsDialog = new JecnActiveDetailsDialog(
							activeFigure);
					activeDetailsDialog.setVisible(true);
				}

			}

			break;
		case roleDetails: // 角色明细
			if (EprosType.eprosDraw != JecnSystemStaticData.getEprosType()) {// 不是画图面板
				return;
			}
			// 点击的菜单项
			JecnMenuItem roleDetailsMenuItem = (JecnMenuItem) e.getSource();

			if (roleDetailsMenuItem.getElementPanel() == null) {
				break;
			}
			// 点击的流程元素
			JecnBaseFlowElementPanel tmpRoleFigure = roleDetailsMenuItem
					.getElementPanel();

			if (tmpRoleFigure instanceof JecnBaseRoleFigure) {// 角色
				JecnBaseRoleFigure roleFigure = (JecnBaseRoleFigure) tmpRoleFigure;
				JecnRoleDetailsDialog roleDetailsDialog = new JecnRoleDetailsDialog(
						roleFigure);
				roleDetailsDialog.setVisible(true);
			}
			break;
		case formatAntoNum: // 自动编号
			JecnElementAttributesUtil.formatAntoNum();
			break;
		}

	}
}
