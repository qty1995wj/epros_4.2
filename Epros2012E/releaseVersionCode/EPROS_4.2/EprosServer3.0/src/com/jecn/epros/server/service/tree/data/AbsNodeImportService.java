package com.jecn.epros.server.service.tree.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.server.bean.tree.data.BaseNode;
import com.jecn.epros.server.bean.tree.data.Node;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.util.JecnUtil;

public abstract class AbsNodeImportService<T> implements INodeImportService<T> {
	protected final Log log = LogFactory.getLog(this.getClass());
	/**
	 * 上传到的目标节点
	 */
	protected Long pid;
	protected File file;
	protected File errorFile;
	protected Long peopleId;
	protected List<List<Node<T>>> nodes;
	protected IBaseDao baseDao;
	protected Long projectId;
	protected int dirStartIndex = -1;
	protected int dirEndIndex = -1;
	protected int contentStartIndex = -1;
	protected int contentEndIndex = -1;
	protected Date now = new Date();
	protected Node<T> pNode;
	protected WritableCellFormat style = null;
	protected boolean[] errors;
	protected boolean CH;

	public AbsNodeImportService(Long peopleId, Long pid, File file, Long projectId, IBaseDao baseDao, boolean CH) {
		this.peopleId = peopleId;
		this.pid = pid;
		this.file = file;
		this.baseDao = baseDao;
		this.projectId = projectId;
		this.pNode = createRootNode(pid);
		this.CH = CH;
		// errorFile = JecnFinal.writeFileToTempDir(new byte[0]);
		style = getStyle();
	}

	private WritableCellFormat getStyle() {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 15, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.RED);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		try {
			tempCellFormat.setAlignment(Alignment.LEFT);
			tempCellFormat.setVerticalAlignment(VerticalAlignment.TOP);
		} catch (WriteException e) {
			log.error("", e);
		}
		return tempCellFormat;
	}

	private List<List<Node<T>>> createNodeByExcels() {
		// 读取本地excel数据
		FileInputStream inputStream = null;
		// 创建excel工作表
		Workbook rwb = null;
		WritableWorkbook wwb = null;
		try {
			// 读取本地excel数据
			inputStream = new FileInputStream(file);
			// 创建excel工作表
			rwb = Workbook.getWorkbook(inputStream);
			// 写出的文件
			wwb = Workbook.createWorkbook(file, rwb);
			Sheet[] sheets = rwb.getSheets();
			errors = new boolean[sheets.length];
			WritableSheet[] wsheets = wwb.getSheets();
			this.nodes = createNodeBySheets(sheets, wsheets);
			wwb.write();
			// 工作簿不关闭不会写出数据
			// workbook在调用close方法时，才向输出流中写入数据
			wwb.close();
			return nodes;
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("", e);
				}
			}
			if (rwb != null) {
				rwb.close();
			}
			if (wwb != null) {
				try {
					wwb.close();
				} catch (Exception e) {

				}
			}
		}
		throw new RuntimeException("导入数据异常");
	}

	protected String getColumnNotExistTip(String name) {
		if (CH) {
			return "标题'" + name + "'不存在";
		} else {
			return "the title '" + name + "' not exists";
		}
	}

	protected List<List<Node<T>>> createNodeBySheets(Sheet[] sheets, WritableSheet[] wsheets) {
		List<List<Node<T>>> result = new ArrayList<List<Node<T>>>();
		for (int i = 0; i < sheets.length; i++) {
			Sheet sheet = sheets[i];
			WritableSheet wsheet = wsheets[i];
			result.add(createNodeBySheet(sheet, wsheet, i));
		}
		// 设置父节点
		for (List<Node<T>> nodes : result) {// 第一层循环多个sheet
			for (Node<T> node : nodes) {// 第二层循环sheet中的目录或者节点
				pNode.add(node);
			}
		}
		initSortId(pNode);
		return result;
	}

	/**
	 * 将一个sheet中的数据拼接为一个树
	 * 
	 * @param sheet
	 * @param wsheet
	 * @param sheetIndex
	 * @return
	 */
	protected List<Node<T>> createNodeBySheet(Sheet sheet, WritableSheet wsheet, int sheetIndex) {
		analysis(sheet, wsheet, sheetIndex);
		wsheet.setColumnView(contentEndIndex + 1, 38);// 列宽
		List<Node<T>> result = new ArrayList<Node<T>>();
		if (!errors[sheetIndex]) {
			DirManager<Node<T>> dirManager = new DirManager<Node<T>>();
			int rows = sheet.getRows();
			Node<T> root = null;
			if (sheetNameIsDir()) {// 将sheet作为一个目录
				root = new BaseNode<T>();
				T data = getBaseData(sheet.getName(), 0);
				root.setData(data);
				result.add(root);
			}
			for (int line = getReadStartLine(); line < rows; line++) {
				createNodeAndAddToNodeTree(root, result, sheet, wsheet, dirManager, sheetIndex, line);
			}
		}
		if (errors[sheetIndex] && StringUtils.isBlank(getCellText(sheet, 0, contentEndIndex + 1))) {
			writeMsg(wsheet, 0, contentEndIndex + 1, name("错误原因", "error reason"));
		}
		return result;
	}

	private String name(String ch, String en) {
		if (CH) {
			return ch;
		}
		return en;
	}

	protected void clearCol(WritableSheet sheet, int c) {
		sheet.removeColumn(c);
	}

	/**
	 * sheet的名称是否可以作为一个目录
	 * 
	 * @return
	 */
	protected boolean sheetNameIsDir() {
		return false;
	}

	/**
	 * 生成一个树结构，如果root为空则创建的节点为root节点，如果不为空则将生成的节点添加到root下
	 * 
	 * @param root
	 * @param result
	 * @param sheet
	 * @param wsheet
	 * @param dirManager
	 * @param line
	 * @return
	 */
	protected Node<T> createNodeAndAddToNodeTree(Node<T> root, List<Node<T>> result, Sheet sheet, WritableSheet wsheet,
			DirManager<Node<T>> dirManager, int sheetIndex, int line) {
		boolean lineHasDir = lineHasDir(sheet, line);
		boolean lineHasContent = lineHasContent(sheet, line);
		if (!lineHasDir && !lineHasContent) {
			writeMsg(wsheet, line, name("没有目录或者内容", "no dir or content"));
			errors[sheetIndex] = true;
			return null;
		}
		// 从哪一列开始查找上级
		Integer indexFrom = null;
		T bean = null;
		Node<T> cur = new BaseNode<T>();
		if (lineHasDir) {
			ExcelNodeData dirData = getDirData(sheet, line);
			indexFrom = dirData.getColumn();
			bean = getBaseData(dirData.getName(), 0);
			cur.setData(bean);
			dirManager.put(dirData.getColumn(), cur);// 目录加入到目录管理器
		} else if (lineHasContent) {
			indexFrom = contentStartIndex;
			bean = getData(sheet, line);
			cur.setData(bean);
		}
		if (root != null) {
			root.add(cur);
		} else {
			Node<T> parent = dirManager.getParent(indexFrom);
			if (parent != null) {// 有上级
				parent.add(cur);
			} else {
				result.add(cur);
				log.warn("未找到上级，所以把当前级别当做顶级，加入到result中");
			}
		}
		if (!validateData(sheet, wsheet, lineHasDir, bean, line)) {
			errors[sheetIndex] = true;
		}
		return cur;
	}

	// --------------------
	protected abstract boolean validateData(Sheet sheet, WritableSheet wsheet, boolean lineHasDir, T bean, int line);

	/**
	 * 获得最后解析要保存的bean
	 * 
	 * @param sheet
	 * @param line
	 * @return
	 */
	protected abstract T getData(Sheet sheet, int line);

	/**
	 * 通过名称生成一个bean
	 * 
	 * @param name
	 * @param type
	 * @return
	 */
	protected abstract T getBaseData(String name, int type);

	/**
	 * 初始化选中节点的t_leval t_path 等值
	 * 
	 * @param pNode
	 */
	protected abstract void initRootNode(Node<T> pNode);

	/**
	 * 分解获得该Excel中的目录开始index，目录截止index，内容开始index，内容截止index
	 * 
	 * @param sheet
	 * @param wsheet
	 * @param sheetIndex
	 */
	protected abstract void analysis(Sheet sheet, WritableSheet wsheet, int sheetIndex);

	/**
	 * 最终保存单个数据，目录或者内容
	 * 
	 * @param node
	 */
	protected abstract void save(Node<T> node);

	/**
	 * 从第几行开始读取
	 * 
	 * @return
	 */
	protected abstract int getReadStartLine();

	/**
	 * 选中节点下最大的sortId
	 * 
	 * @return
	 */
	protected abstract long getRootChildsMaxSortId();

	// --------------------

	protected void writeMsg(WritableSheet wsheet, int line, int col, String msg) {
		String cellText = getCellText(wsheet, line, col);
		String text = msg;
		if (StringUtils.isNotBlank(cellText)) {
			text = cellText + "\n" + text;
		}
		try {
			wsheet.addCell(new Label(col, line, text, style));

		} catch (Exception e) {
			throw new RuntimeException("写出数据异常", e);
		}
	}

	protected void writeMsg(WritableSheet wsheet, int line, String msg) {
		writeMsg(wsheet, line, contentEndIndex + 1, msg);
	}

	protected void writeMsg(WritableSheet wsheet, String msg) {
		writeMsg(wsheet, 0, contentEndIndex + 1, msg);
	}

	@Override
	public ByteFileResult save() {
		// 产生数据及校验信息
		final List<List<Node<T>>> nodes = createNodeByExcels();
		ByteFileResult result = new ByteFileResult(true, "");
		boolean error = false;
		for (boolean hasError : errors) {
			if (hasError) {
				error = true;
			}
		}
		if (error) {
			result.setSuccess(false);
			try {
				result.setBytes(FileUtils.readFileToByteArray(file));
			} catch (Exception e) {
				log.error("", e);
			}
			deleteFile();
			return result;
		}
		for (List<Node<T>> n : nodes) {
			save(n);
		}
		deleteFile();
		return result;
	}

	private void deleteFile() {
		file.delete();
	}

	private void save(List<Node<T>> nodes) {
		for (Node<T> node : nodes) {
			save(node);
			if (node.hasChilds()) {
				save(node.getChilds());
			}
		}
	}

	protected boolean lineHasDir(Sheet sheet, int line) {
		if (!hasDir()) {
			return false;
		}
		for (int column = dirStartIndex; column <= dirEndIndex; column++) {
			String columnName = getCellText(sheet, line, column);
			if (StringUtils.isNotBlank(columnName)) {
				return true;
			}
		}
		return false;
	}

	private boolean hasDir() {
		if (dirEndIndex <= 0) {
			return false;
		}
		return true;
	}

	protected boolean lineHasContent(Sheet sheet, int line) {
		String columnName = getCellText(sheet, line, contentStartIndex);
		if (StringUtils.isNotBlank(columnName)) {
			return true;
		}
		return false;
	}

	protected ExcelNodeData getDirData(Sheet sheet, int line) {
		for (int column = dirStartIndex; column <= dirEndIndex; column++) {
			String columnName = getCellText(sheet, line, column);
			if (StringUtils.isNotBlank(columnName)) {
				return new ExcelNodeData(columnName, column);
			}
		}
		return null;
	}

	protected String getCellText(Sheet sheet, int line, int col) {
		try {
			return getCellText(sheet.getCell(col, line));
		} catch (Exception e) {
			log.error("", e);
			return "";
		}
	}

	protected void writeToCell(Sheet sheet, int line, int col, String msg) {

	}

	/**
	 * 
	 * 获取单元格内容
	 * 
	 * @param cell
	 * @return
	 */
	private String getCellText(Cell cell) {
		if (isNullObj(cell)) {
			return null;
		} else {
			String cellContent = cell.getContents();
			if (cellContent != null) {
				return cellContent.trim();
			}
			return null;
		}
	}

	protected Node<T> createRootNode(Long pid2) {
		Node<T> pNode = new BaseNode<T>();
		if (pid == 0L) {
			pNode.setDbId(0L);
			pNode.setLevel(0);
			pNode.setPath("");
			pNode.setViewSort("");
		} else {
			initRootNode(pNode);
		}
		return pNode;
	}

	/**
	 * 
	 * 给定参数是否为null
	 * 
	 * @param obj
	 *            Object
	 * @return
	 */
	protected boolean isNullObj(Object obj) {
		return (obj == null) ? true : false;
	}

	protected void initSortId(Node<T> pNode) {
		if (pNode.hasChilds()) {
			List<Node<T>> childs = pNode.getChilds();
			long sort = getRootChildsMaxSortId() + 1;
			for (Node<T> child : childs) {
				child.setSort(sort++);
				if (child.hasChilds()) {
					initChildSortId(child);
				}
			}
		}
	}

	private void initChildSortId(Node<T> pNode) {
		if (pNode.hasChilds()) {
			List<Node<T>> childs = pNode.getChilds();
			long sort = 1;
			for (Node<T> child : childs) {
				child.setSort(sort++);
				if (child.hasChilds()) {
					initChildSortId(child);
				}
			}
		}
	}

	protected int getContentIndex(Sheet sheet, String name) {
		int riskNumIndex = -1;
		int cellNum = sheet.getColumns() - 1;
		for (int col = 0; col <= cellNum; col++) {
			String cellText = getCellText(sheet, 0, col);
			if (cellText == null) {
				continue;
			}
			if (cellText.startsWith(name)) {
				riskNumIndex = col;
			}
		}
		return riskNumIndex;
	}

	protected boolean columnNotExist(Sheet sheet, WritableSheet wsheet, int sheetIndex, int col, String colName) {
		String cellText = getCellText(sheet, 0, col);
		if (StringUtils.isBlank(cellText) || !cellText.startsWith(colName)) {
			writeMsg(wsheet, getColumnNotExistTip(colName));
			errors[sheetIndex] = true;
			return true;
		}
		return false;
	}

	protected boolean validateLenAndTipIfNotPass(WritableSheet wsheet, String tipName, String validateField, int len,
			int line) {
		if (JecnUtil.lenThan(validateField, len)) {
			writeMsg(wsheet, line, "'" + tipName + "'"
					+ name("不能超过" + len + "个汉字", " can not greater than " + len + " character"));
			return true;
		}
		return false;
	}

	protected static class ExcelNodeData {
		private String name = null;
		private Integer column = null;

		public ExcelNodeData(String name, Integer column) {
			this.name = name;
			this.column = column;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Integer getColumn() {
			return column;
		}

		public void setColumn(Integer column) {
			this.column = column;
		}
	}

}
