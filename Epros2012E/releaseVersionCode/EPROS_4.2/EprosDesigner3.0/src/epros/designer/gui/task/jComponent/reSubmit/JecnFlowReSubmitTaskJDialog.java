package epros.designer.gui.task.jComponent.reSubmit;

import epros.designer.gui.task.jComponent.JecnFlowTaskJDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;

/**
 * 流程任务窗体
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 26, 2017 时间：7:07:02 PM
 */
public class JecnFlowReSubmitTaskJDialog extends JecnFlowTaskJDialog {
//
//	JecnTaskResubmitDesignerBean designerBean;
//
	public JecnFlowReSubmitTaskJDialog(JecnTree jTree, JecnTreeNode treeNode,int approveType) {
		super(jTree, treeNode,approveType);
	}
//
//	protected void init() {
//		super.init();
//
//		JecnTaskBeanNew beanNew = designerBean.getTaskBeanNew();
//		jTextFieldTaskName.setEditable(false);
//		jTextFieldVersion.setEditable(false);
//		jTextFieldVersion.setText(beanNew.getHistoryVistion());
//		jTextAreaChangeDescription.setText(beanNew.getTaskDesc());
//		jTextFieldEndTime.setText(JecnDesignerCommon.getStringbyDate(beanNew.getEndTime()));
//	}
//
//	protected void initTaskData() {
//		try {
//			designerBean = ConnectionPool.getTaskRecordAction().getReSubmitTaskBeanNew(
//					treeNode.getJecnTreeBean().getId(), treeNode.getJecnTreeBean().getTreeNodeType());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 初始化审批人
//	 */
//	protected Set<Long> initApproveUser() {
//		Set<Long> sets = new HashSet<Long>();
//		List<JecnTaskStateBean> approvePeopleConns = designerBean.getTaskStateBeans();
//		Properties properties = redTaskDefaultValueProperties();
//		if (properties == null) {
//			return sets;
//		}
//		// 只有是文控主导审批，以及到文控阶段的时候为true
//		boolean isControlBreak = false;
//		for (TaskStateEnum stateEnum : listOrder) {
//			if (isControlBreak) {
//				break;
//			}
//			switch (stateEnum) {
//			case a1:
//				this.auditId = getApproveIds(1, approvePeopleConns);
//
//				if (auditId != null && StringUtils.isNotBlank(auditId)) {
//					sets.add(Long.valueOf(auditId));
//				}
//				if (isControl) {// 文控审核人主导审批
//					isControlBreak = true;
//				}
//				break;
//			case a2:
//				this.deptPeopleId = getApproveIds(2, approvePeopleConns);
//				if (deptPeopleId != null && StringUtils.isNotBlank(deptPeopleId)) {
//					sets.add(Long.valueOf(deptPeopleId));
//				}
//				break;
//			case a3:
//				this.reviewId = getApproveIds(3, approvePeopleConns);
//				addSetIds(reviewId, sets);
//				break;
//			case a4:
//				this.approveId = getApproveIds(4, approvePeopleConns);
//				addSetIds(approveId, sets);
//				break;
//			case a5:
//				this.bussId = getApproveIds(6, approvePeopleConns);
//				addSetIds(bussId, sets);
//				break;
//			case a6:
//				this.itId = getApproveIds(7, approvePeopleConns);
//				addSetIds(itId, sets);
//				break;
//			case a7:
//				divisionManId = getApproveIds(8, approvePeopleConns);
//				addSetIds(divisionManId, sets);
//				break;
//			case a8:
//				generalManId = getApproveIds(9, approvePeopleConns);
//				addSetIds(generalManId, sets);
//				break;
//			case a9:
//				customApprovaId1 = getApproveIds(11, approvePeopleConns);
//				addSetIds(customApprovaId1, sets);
//				break;
//			case a10:
//				customApprovaId2 = getApproveIds(12, approvePeopleConns);
//				addSetIds(customApprovaId2, sets);
//				break;
//			case a11:
//				customApprovaId3 = getApproveIds(13, approvePeopleConns);
//				addSetIds(customApprovaId3, sets);
//				break;
//			case a12:
//				customApprovaId4 = getApproveIds(14, approvePeopleConns);
//				addSetIds(customApprovaId4, sets);
//				break;
//			default:
//				break;
//			}
//		}
//		return sets;
//	}
//
//	private String getApproveIds(int state, List<JecnTaskStateBean> approvePeopleConns) {
//		String ids = "";
//		for (JecnTaskStateBean jecnTaskStateBean : approvePeopleConns) {
//			if (jecnTaskStateBean.getState() == state) {
//				ids += "," + jecnTaskStateBean.getStatePeopleId();
//			}
//		}
//		if (ids.startsWith(",")) {
//			ids = ids.replaceFirst(",", "");
//		}
//		return ids;
//	}
//
//	/** 确定 */
//	protected boolean submitTask() {
//		if (designerBean == null) {
//			return false;
//		}
//		TaskReSubmitParam reSubmitParam = new TaskReSubmitParam();
//		List<AuditPeopleBean> listAuditPeople = getAuitPeoples(designerBean.getTaskBeanNew().getId());
//
//		reSubmitParam.setEndTime(new Date());
//		reSubmitParam.setOpinion("同意");
//		reSubmitParam.setTaskStage(0);
//		reSubmitParam.setImplementDate(new Date());
//		reSubmitParam.setTaskId(designerBean.getTaskBeanNew().getId().toString());
//		reSubmitParam.setTaskOperation("7");
//		reSubmitParam.setListAuditPeople(listAuditPeople);
//		reSubmitParam.setTaskDesc(jTextAreaChangeDescription.getText());
//		ConnectionPool.getTaskRPCService().resubmit(JecnConstants.getUserId(), reSubmitParam);
//		return true;
//	}
//
//	private List<AuditPeopleBean> getAuitPeoples(Long taskId) {
//		List<AuditPeopleBean> listAuditPeople = new ArrayList<AuditPeopleBean>();
//		/** 审批人的集合 */
//		List<JecnTaskApprovePeopleConn> jecnTaskApprovePeopleConnList = this.getlistJecnTaskApprovePeopleConn();
//		/** 审批阶段集合 */
//		List<JecnTaskStage> taskStageList = ConnectionPool.getTaskRecordAction().getJecnTaskStageList(taskId);
//
//		AuditPeopleBean auditPeopleBean = null;
//		if (jecnTaskApprovePeopleConnList != null) {// 任务各个环节的审批人
//			for (JecnTaskStage jecnTaskStage : taskStageList) {
//				auditPeopleBean = new AuditPeopleBean();
//				for (JecnTaskApprovePeopleConn jecnTaskApprovePeopleConn : jecnTaskApprovePeopleConnList) {
//					if (jecnTaskApprovePeopleConn.getStageId().intValue() == jecnTaskStage.getStageMark()) {
//						if (StringUtils.isNotBlank(auditPeopleBean.getAuditId())) {
//							auditPeopleBean.setAuditId(auditPeopleBean.getAuditId() + ","
//									+ jecnTaskApprovePeopleConn.getApprovePid());
//						} else {
//							auditPeopleBean.setAuditId(jecnTaskApprovePeopleConn.getApprovePid().toString());
//						}
//						auditPeopleBean.setState(jecnTaskStage.getStageMark());
//						auditPeopleBean.setStageId(jecnTaskStage.getId());
//					}
//				}
//				if (StringUtils.isNotBlank(auditPeopleBean.getAuditId())) {
//					listAuditPeople.add(auditPeopleBean);
//				}
//			}
//		}
//		return listAuditPeople;
//	}
}
