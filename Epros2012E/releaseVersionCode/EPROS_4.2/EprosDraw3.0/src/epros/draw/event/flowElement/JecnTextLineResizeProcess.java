package epros.draw.event.flowElement;

import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.SwingUtilities;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.gui.line.resize.JecnLineTextResizePanel;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.line.shape.TempDividingLine;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 拖动曼哈顿线端点事件处理类
 * 
 * @author ZHANGXH
 * @date： 日期：May 29, 2012 时间：4:33:15 PM
 */
public class JecnTextLineResizeProcess {

	/**
	 * 连接线鼠标点击事件处理
	 * 
	 * 点击连接线显示连接线选中点
	 * 
	 * @param e
	 */
	public void mouseResizePressed(MouseEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 主界面和面板是否存在
		if (JecnDrawMainPanel.getMainPanel() == null || desktopPane == null) {
			return;
		}
		if (((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0)
				&& ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0)) {
			// 左右键同时按住时,不设置按下位置点
		} else {

		}

	}

	/**
	 * 连接线脱线端点释放处理
	 * 
	 * @param e
	 */
	public void mouseResizeRelease(MouseEvent e) {
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();

		JecnLineTextResizePanel lineResizePanel = (JecnLineTextResizePanel) e.getSource();
		MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(lineResizePanel, e, drawDesktopPane);

		if (drawDesktopPane.activeLine != null) {
			drawDesktopPane.remove(drawDesktopPane.activeLine);
			// 删除虚拟连接线
			drawDesktopPane.activeLine = null;
			// 清空虚拟线段数组
			drawDesktopPane.getTempDividingList().clear();

			List<LineSegment> lineSegmentList = lineResizePanel.getTextPanel().getManhattanLine().getLineSegments();
			// 判断悬浮点属于那条线段
			LineSegment relatedLine = lineResizePanel.getTextPanel().getRelatedLine(lineSegmentList,
					desMouseEvent.getPoint());
			// 判断 textPoint 中心点是否在线段上，不在，则强制 移动到线段上
			reSizeTextLocation(relatedLine, lineResizePanel);

			lineResizePanel.getTextPanel().setRelatedLine(relatedLine);

			lineResizePanel.getTextPanel().reSetTextLocation(desMouseEvent.getPoint());
		}
		// 刷新面板
		drawDesktopPane.updateUI();
	}

	private void reSizeTextLocation(LineSegment relatedLine, JecnLineTextResizePanel lineResizePanel) {
		if (relatedLine == null) {
			return;
		}
		if (relatedLine.getLineSegmentData().getLineDirectionEnum() == LineDirectionEnum.horizontal) {// 横线
			lineResizePanel.setLocation(lineResizePanel.getX(), relatedLine.getY() - lineResizePanel.getHeight() / 2);
		} else {
			lineResizePanel.setLocation(relatedLine.getX() - lineResizePanel.getWidth() / 2, lineResizePanel.getY());
		}
	}

	/**
	 * 连接线鼠标拖动事件处理
	 * 
	 * 点击连接线显示连接线选中点
	 * 
	 * @param e
	 */
	public void mouseResizeDragged(MouseEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0 && e.getButton() == MouseEvent.BUTTON3) {// 鼠标左键按住同时拖动时右键，右键事件拖动不执行
			return;
		}

		if (e.getSource() instanceof JecnLineTextResizePanel) {
			JecnLineTextResizePanel lineResizePanel = (JecnLineTextResizePanel) e.getSource();
			MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(lineResizePanel, e, desktopPane);
			if (desktopPane.activeLine == null) {
				desktopPane.activeLine = new TempDividingLine(lineResizePanel.getZoomCenterPoint(), desMouseEvent
						.getPoint());
				// 添加图形画图面板
				desktopPane.add(desktopPane.activeLine);
				if (!desktopPane.getTempDividingList().contains(desktopPane.activeLine)) {
					desktopPane.getTempDividingList().add(desktopPane.activeLine);
				}
			} else {
				desktopPane.activeLine.setEndPoint(desMouseEvent.getPoint());
			}
			if (desktopPane.activeLine.getStartPoint() != null && desktopPane.activeLine.getEndPoint() != null) {
				// 画临时分割线 要时刻刷新面板
				desktopPane.updateUI();
			}
		}
	}
}
