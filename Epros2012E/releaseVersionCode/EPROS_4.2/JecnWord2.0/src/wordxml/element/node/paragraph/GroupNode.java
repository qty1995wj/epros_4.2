package wordxml.element.node.paragraph;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import wordxml.element.dom.IBodyElement;
import wordxml.element.dom.WordElementFactory;
import wordxml.element.dom.paragraph.Group;

/*********
 * 图文框节点操作
 * 
 * @author hyl
 * 
 */
public class GroupNode {
	private static final Log log = LogFactory.getLog(GroupNode.class);

	/**
	 * 创建一个图文框
	 * 
	 * @param group
	 * @param parentEle
	 */
	public static void createGroup(Group group, Element parentEle) {
		if (group == null || parentEle == null) {
			log.info("图文框为空");
			return;
		}
		Element gEl = parentEle.addElement("wx:pBdrGroup");
		// 实例化节中的内容
		for (IBodyElement bodyEle : group.getBodyElements()) {
			WordElementFactory.produceElement(bodyEle, gEl);
		}
	}

}
