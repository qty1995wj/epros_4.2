package com.jecn.epros.server.bean.system;

import java.io.Serializable;
import java.util.Date;

public class NightEvent implements Serializable {
	/** 手动触发的发布邮件提醒 eventType **/
	public static final int HAND_PUBLISH_SEND_EMAIL = 1;

	private String id;
	private String rId;
	private String rType;
	private String rSubType;
	private int eventType;
	private Date createDate;
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getrId() {
		return rId;
	}

	public void setrId(String rId) {
		this.rId = rId;
	}

	public String getrType() {
		return rType;
	}

	public void setrType(String rType) {
		this.rType = rType;
	}

	public String getrSubType() {
		return rSubType;
	}

	public void setrSubType(String rSubType) {
		this.rSubType = rSubType;
	}

	public int getEventType() {
		return eventType;
	}

	public void setEventType(int eventType) {
		this.eventType = eventType;
	}

}
