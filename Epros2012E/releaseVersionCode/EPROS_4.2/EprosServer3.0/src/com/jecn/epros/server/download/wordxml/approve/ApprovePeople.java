package com.jecn.epros.server.download.wordxml.approve;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.util.JecnUtil;

public class ApprovePeople {
	private Long id;
	private String name;
	/** 0岗位id 1岗位名称 2部门id 3部门名称 **/
	private List<String[]> posAndOrgs;
	private Date date;

	// 拼接岗位
	public List<String> getPoses() {
		List<String> result = new ArrayList<String>();
		if (JecnUtil.isEmpty(posAndOrgs)) {
			return result;
		}
		for (String[] arr : posAndOrgs) {
			if (StringUtils.isNotBlank(arr[0]) && StringUtils.isNotBlank(arr[1])) {
				result.add(arr[1]);
			}
		}
		return result;
	}

	// 拼接部门
	public List<String> getOrgs() {
		List<String> result = new ArrayList<String>();
		if (JecnUtil.isEmpty(posAndOrgs)) {
			return result;
		}
		for (String[] arr : posAndOrgs) {
			if (StringUtils.isNotBlank(arr[2]) && StringUtils.isNotBlank(arr[3])) {
				result.add(arr[3]);
			}
		}
		return result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String[]> getPosAndOrgs() {
		return posAndOrgs;
	}

	public void setPosAndOrgs(List<String[]> posAndOrgs) {
		this.posAndOrgs = posAndOrgs;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
