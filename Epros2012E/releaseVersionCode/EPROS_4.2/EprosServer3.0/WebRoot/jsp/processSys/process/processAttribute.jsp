<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div">
			<div id="processTitle">
				<table align="left" style="width: 100%;">
					<tr>
						<td width="100px;" valign="middle" class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 流程名称： -->
							<s:text name="processNameC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
							<s:property value="processAttribute.flowName" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 所属流程地图： -->
							<s:text name="subordinateProcessMapC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
							<s:property value="processAttribute.perFlowName" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 流程类别： -->
							<s:text name="processTypeC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
							<s:property value="processAttribute.flowTypeName" />
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 责任部门： -->
							<s:text name="responsibilityDepartmentC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
							<a
								href="organization.action?orgId=<s:property value='processAttribute.orgId'/>"
								target='_blank' style="cursor: pointer;" class="table"> <s:property
									value="processAttribute.orgName" /> </a>
						</td>
					</tr>
					<tr>
						<td class="td_dashed">
							<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
							<!-- 流程责任人： -->
							<s:text name="processResponsiblePersonsC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;"
							class="td_dashed">
							&nbsp;
							<s:property value="processAttribute.resPeopleName" />
						</td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>
