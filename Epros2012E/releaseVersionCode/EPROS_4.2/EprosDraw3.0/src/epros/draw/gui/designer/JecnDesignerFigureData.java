package epros.draw.gui.designer;

import java.io.Serializable;

/**
 * @author yxw 2012-8-15
 * @description：
 */
public class JecnDesignerFigureData implements Serializable {
	/** 元素链接id */
	private Long linkId;
	/** 关联类型 1是上游流程,2是下游流程,3是过程流程,4是子流程 */
	private String lineType;// 为数据据lineColor对应
	/** 岗位编号 */
	protected String posNumber;
	
	// 1是上游流程,2是下游流程,3是过程流程,4是子流程
	/** 0流程地图；1流程图 */
	private int isFlow = -1;

	public Long getLinkId() {
		return linkId;
	}

	public void setLinkId(Long linkId) {
		if (linkId != null && linkId < 1) {
			this.linkId = null;
		} else {
			this.linkId = linkId;
		}
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public int getIsFlow() {
		return isFlow;
	}

	public void setIsFlow(int isFlow) {
		this.isFlow = isFlow;
	}

	public String getPosNumber() {
		return posNumber;
	}

	public void setPosNumber(String posNumber) {
		this.posNumber = posNumber;
	}

	/**
	 * 
	 * 克隆属性值
	 * 
	 */
	public JecnDesignerFigureData clone() {
		JecnDesignerFigureData designerFigure = new JecnDesignerFigureData();
		designerFigure.setIsFlow(isFlow);
		designerFigure.setLineType(lineType);
		designerFigure.setLinkId(linkId);
		designerFigure.setPosNumber(posNumber);
		return designerFigure;
	}
}
