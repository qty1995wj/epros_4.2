package com.jecn.viewdoc.service;

import com.jecn.viewdoc.bean.ViewDocContentIdCache;


public interface IViewDocService {

	void save(ViewDocContentIdCache cache) throws Exception;

	ViewDocContentIdCache getCache(Long relatedId, int i) throws Exception;

	void update(ViewDocContentIdCache cache) throws Exception;

}
