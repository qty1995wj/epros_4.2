package com.jecn.webservice.jbshihua.util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.jecn.webservice.jbshihua.entity.UserInfo;

@XmlRootElement(name="DATAINFO")
public class HrUserMsg
{
  private String uuid;
  private String desc1;
  private String desc3;
  private String desc5;
  private String desc8;
  private String desc9;
  private String desc19;
  private String desc21;
  private String code;
  private String codeid;
  
  @XmlElement(name="UUID")
  public String getUuid()
  {
    return this.uuid;
  }
  
  public void setUuid(String uuid)
  {
    this.uuid = uuid;
  }
  
  @XmlElement(name="DESC1")
  public String getDesc1()
  {
    return this.desc1;
  }
  
  public void setDesc1(String desc1)
  {
    this.desc1 = desc1;
  }
  
  @XmlElement(name="DESC3")
  public String getDesc3()
  {
    return this.desc3;
  }
  
  public void setDesc3(String desc3)
  {
    this.desc3 = desc3;
  }
  
  @XmlElement(name="DESC5")
  public String getDesc5()
  {
    return this.desc5;
  }
  
  public void setDesc5(String desc5)
  {
    this.desc5 = desc5;
  }
  
  @XmlElement(name="DESC8")
  public String getDesc8()
  {
    return this.desc8;
  }
  
  public void setDesc8(String desc8)
  {
    this.desc8 = desc8;
  }
  
  @XmlElement(name="DESC9")
  public String getDesc9()
  {
    return this.desc9;
  }
  
  public void setDesc9(String desc9)
  {
    this.desc9 = desc9;
  }
  
  @XmlElement(name="DESC19")
  public String getDesc19()
  {
    return this.desc19;
  }
  
  public void setDesc19(String desc19)
  {
    this.desc19 = desc19;
  }
  
  @XmlElement(name="DESC21")
  public String getDesc21()
  {
    return this.desc21;
  }
  
  public void setDesc21(String desc21)
  {
    this.desc21 = desc21;
  }
  
  @XmlElement(name="CODE")
  public String getCode()
  {
    return this.code;
  }
  
  public void setCode(String code)
  {
    this.code = code;
  }
  
  @XmlElement(name="CODEID")
  public String getCodeid()
  {
    return this.codeid;
  }
  
  public void setCodeid(String codeid)
  {
    this.codeid = codeid;
  }
  
}
