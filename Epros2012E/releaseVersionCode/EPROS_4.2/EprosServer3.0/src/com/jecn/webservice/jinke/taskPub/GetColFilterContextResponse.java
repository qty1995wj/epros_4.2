/**
 * GetColFilterContextResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetColFilterContextResponse  implements java.io.Serializable {
    private java.lang.String getColFilterContextResult;

    public GetColFilterContextResponse() {
    }

    public GetColFilterContextResponse(
           java.lang.String getColFilterContextResult) {
           this.getColFilterContextResult = getColFilterContextResult;
    }


    /**
     * Gets the getColFilterContextResult value for this GetColFilterContextResponse.
     * 
     * @return getColFilterContextResult
     */
    public java.lang.String getGetColFilterContextResult() {
        return getColFilterContextResult;
    }


    /**
     * Sets the getColFilterContextResult value for this GetColFilterContextResponse.
     * 
     * @param getColFilterContextResult
     */
    public void setGetColFilterContextResult(java.lang.String getColFilterContextResult) {
        this.getColFilterContextResult = getColFilterContextResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetColFilterContextResponse)) return false;
        GetColFilterContextResponse other = (GetColFilterContextResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getColFilterContextResult==null && other.getGetColFilterContextResult()==null) || 
             (this.getColFilterContextResult!=null &&
              this.getColFilterContextResult.equals(other.getGetColFilterContextResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetColFilterContextResult() != null) {
            _hashCode += getGetColFilterContextResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetColFilterContextResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetColFilterContextResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getColFilterContextResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GetColFilterContextResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
