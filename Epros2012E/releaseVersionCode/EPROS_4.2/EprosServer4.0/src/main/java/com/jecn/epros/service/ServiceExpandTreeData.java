package com.jecn.epros.service;

import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.security.AuthorityNodesData;
import com.jecn.epros.security.AuthenticatedUserUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * tree节点展开权限判断
 * Created by ZXH on 2017/3/30.
 */
public class ServiceExpandTreeData {

    /**
     * 获取展开后有权限的节点
     *
     * @param dataList
     * @param type
     * @return
     */
    public static List<TreeNode> findExpandNodesByDataList(List<TreeNode> dataList, TreeNodeUtils.TreeType type) {
        Set<Long> expandIds = getExpandIdsByTypeAuth(type);
        if (expandIds == null) {
            return dataList;
        }
        for (int i = dataList.size() - 1; i >= 0; i--) {
            TreeNode treeNode = dataList.get(i);
            if (treeNode.getIsDir() == 0 && isPublicNode(type, treeNode.getIsDir())) {// 流程架构公开
                continue;
            } else if (treeNode.getIsDir() == 1 && isPublicNode(type, treeNode.getIsDir())) {
                continue;
            }
            if (!expandIds.contains(Long.parseLong(dataList.get(i).getId().toString()))) {
                dataList.remove(i);
            }
        }
        return dataList;
    }

    private static boolean isPublicNode(TreeNodeUtils.TreeType type, int isDir) {
        switch (type) {
            case process:
                if (isDir == 0) {
                    return AuthenticatedUserUtil.isFlowMapAdmin() || AuthenticatedUserUtil.isFlowAdmin();
                }
                return AuthenticatedUserUtil.isFlowAdmin();
            case rule:
                return AuthenticatedUserUtil.isRuleAdmin();
            case file:
                return AuthenticatedUserUtil.isFileAdmin();
            case standard:
                return AuthenticatedUserUtil.isStandardAdmin();
            case organization:
                return AuthenticatedUserUtil.isOrgAdmin();
            case position:
                return AuthenticatedUserUtil.isStationAdmin();
            default:
                return false;
        }
    }

    /**
     * 获取展开节点Ids
     *
     * @param type
     * @return
     */
    private static Set<Long> getExpandIdsByTypeAuth(TreeNodeUtils.TreeType type) {
        Set<Long> expandIds = null;
        AuthorityNodesData nodesData = AuthenticatedUserUtil.getNodesData();
        switch (type) {
            case process:
                expandIds = nodesData.getProcessNode() == null ? null : nodesData.getProcessNode().getExpandIds();
                break;
            case file:
                expandIds = nodesData.getFileNode() == null ? null : nodesData.getFileNode().getExpandIds();
                break;
            case rule:
                expandIds = nodesData.getRuleNode() == null ? null : nodesData.getRuleNode().getExpandIds();
                break;
            case standard:
                expandIds = nodesData.getStandardNode() == null ? null : nodesData.getStandardNode().getExpandIds();
                break;
        }
        return expandIds;
    }
}
