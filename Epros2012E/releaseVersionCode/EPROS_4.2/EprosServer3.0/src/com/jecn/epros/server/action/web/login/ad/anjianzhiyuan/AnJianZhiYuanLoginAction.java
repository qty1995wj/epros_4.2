package com.jecn.epros.server.action.web.login.ad.anjianzhiyuan;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

public class AnJianZhiYuanLoginAction extends JecnAbstractADLoginAction {

	public AnJianZhiYuanLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		try {
			// 过滤掉admin
			if ("admin".equals(loginAction.getLoginName())) {
				return loginAction.userLoginGen(true);
			}
			// 获取用户输入的Ldap账号
			String ldapAccount = loginAction.getLoginName();
			// 获取用户输入的密码
			String ldapPassword = loginAction.getDecodePwd(loginAction.getPassword());
			// 进行AD域验证
			int result = JecnAnJianZhiYuanAfterItem.doLdapAuthority(ldapAccount,ldapPassword);
			log.info("权限验证结果:" + result);
			// 验证成功
			if (result == 0) {
				// 不进行密码验证 登录
				return loginAction.userLoginGen(false);
			} else if (result == 1) {
				// 用户名或密码不正确！
				this.loginAction.addFieldError("loginMessage", loginAction
						.getText("nameOrPasswordError"));
				return LoginAction.INPUT;
			} else if (result == 2) {
				// 无法验证用户，请联系系统管理员!
				this.loginAction.addFieldError("loginMessage", loginAction
						.getText("anJianZhiYuanADConnectFailed"));
				return LoginAction.INPUT;
			}
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
		}
		return LoginAction.INPUT;
	}
}
