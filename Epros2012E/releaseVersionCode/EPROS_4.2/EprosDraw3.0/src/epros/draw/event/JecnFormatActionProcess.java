package epros.draw.event;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.List;

import javax.swing.JCheckBox;

import epros.draw.constant.JecnActionCommandConstants;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.unit.JecnSelectFigureXY;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;
import epros.draw.gui.top.toolbar.dialog.JecnFlowElementMainDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 工具栏格式动作处理类
 * 
 * @author Administrator
 * 
 */
public class JecnFormatActionProcess extends JecnAbstractActionProcess {

	protected void actionPerformed(ActionEvent paramActionEvent, ToolBarElemType toolBarElemType) {
		if (toolBarElemType == null) {
			return;
		}
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		switch (toolBarElemType) {
		case formatAttriSet:// 流程元素维护（模板维护）
			if (!JecnDesignerProcess.getDesignerProcess().checkAuth()) {// 设计器且不是管理员
				return;
			}

			if (desktopPane == null) {
				showFlowElementMainDialog();
				return;
			}

			// 记录操作前面板放大缩小倍数
			double oldWorkScale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
			// 设置面板放大缩小倍数为1.0
			desktopPane.setWorkflowScale(1.0);

			// 显示元素维护对话框
			JecnFlowElementMainDialog dialog = showFlowElementMainDialog();

			if (!dialog.isVisible() && oldWorkScale != 1.0) {// 关闭面板还原放大缩小倍数
				desktopPane.setWorkflowScale(oldWorkScale);
			}
			break;
		case formatAntoNum: // 自动编号
			if (desktopPane != null && desktopPane.getFlowMapData().getMapType() == MapType.partMap) {
				JecnElementAttributesUtil.formatAntoNum();
			}
			break;
		}
	}

	/**
	 * 
	 * 单选框选中事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() instanceof JCheckBox) {
			JCheckBox toobarFormatGrid = (JCheckBox) e.getSource();
			String actionCommand = toobarFormatGrid.getActionCommand();
			if (JecnActionCommandConstants.TOOBAR_FORMAT_GRID.equals(actionCommand)) {// 工具栏->格式->网格
				toobarFormatGridProcess(e);
			} else if (JecnActionCommandConstants.TOOBAR_FORMAT_PARALLELINES.equals(actionCommand)) {// 工具栏->格式->横分割线
				itemStateChangedParallelLines(e);
			} else if (JecnActionCommandConstants.TOOBAR_FORMAT_VERTICALINE.equals(actionCommand)) {// 工具栏->格式->竖分割线
				itemStateChangedVerticalLine(e);
			} else if ("roleMobile".equals(actionCommand)) { // 角色移动
				roleMobile();
			} else if (JecnActionCommandConstants.TOOBAR_FORMAT_MODEL.equals(actionCommand)) {
				megerModel();
				JecnSystemStaticData.setShowModel((e.getStateChange() == ItemEvent.SELECTED));
			}
		}
	}

	public static void megerModel() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		// 1、获取 所有泳池
		List<JecnBaseFlowElementPanel> listPanel = desktopPane.getModelPanelList();
		// 获取选中图形元素的最大XY坐标和最小XY坐标
		JecnSelectFigureXY figureXY = JecnWorkflowUtil.getSelectFigureXY(listPanel);
		desktopPane.getFlowMapData().setModelFigureXY(figureXY);
	}

	/**
	 * 角色移动
	 */
	public void roleMobile() {

		// 判断角色移动是否选中
		if (JecnDrawMainPanel.getMainPanel().getToolbar().getFormatToolBarItemPanel().getRoleMoveCheckBox()
				.isSelected()) {

			JecnSystemStaticData.setShowRoleMove(true);
		} else {
			JecnSystemStaticData.setShowRoleMove(false);
		}

		// 当前画图面板存在情况，同步角色移动显示/隐藏
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		// 角色移动
		if (JecnSystemStaticData.isShowRoleMove()) {
			// 添加角色移动
			workflow.createAddJecnRoleMobile();
		} else {
			// 移除角色移动
			workflow.removeJecnRoleMobile();
		}

		workflow.repaint();
	}

	/**
	 * 
	 * 工具栏->格式->网格处理对象
	 * 
	 * @param e
	 *            ItemEvent
	 */
	private void toobarFormatGridProcess(ItemEvent e) {
		// 修改全局变量
		JecnSystemStaticData.setShowGrid((e.getStateChange() == e.SELECTED));
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().repaint();
		}
	}

	/**
	 * 
	 * 工具栏->格式->横分割线
	 * 
	 * @param e
	 *            ItemEvent
	 */
	public void itemStateChangedParallelLines(ItemEvent e) {
		// 修改全局变量
		JecnSystemStaticData.setShowParallelLines((e.getStateChange() == e.SELECTED));
		// 当前选中
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow != null) {
			workflow.repaint();
		}
	}

	/**
	 * 
	 * 工具栏->格式->竖分割线
	 * 
	 * @param e
	 *            ItemEvent
	 */
	public void itemStateChangedVerticalLine(ItemEvent e) {
		// 修改全局变量
		JecnSystemStaticData.setShowVerticalLine((e.getStateChange() == e.SELECTED));
		// 当前选中
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow != null) {
			workflow.repaint();
		}
	}

	/**
	 * 
	 * 显示元素维护对话框
	 * 
	 */
	private JecnFlowElementMainDialog showFlowElementMainDialog() {
		JecnFlowElementMainDialog dialog = new JecnFlowElementMainDialog();
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
		return dialog;
	}
}
