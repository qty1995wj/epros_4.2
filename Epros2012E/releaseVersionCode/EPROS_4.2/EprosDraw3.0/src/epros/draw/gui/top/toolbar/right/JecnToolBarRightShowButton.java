package epros.draw.gui.top.toolbar.right;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 显示工具栏面板右面隐藏部分
 * 
 * @author ZHOUXY
 *
 */
public class JecnToolBarRightShowButton extends JButton {
	/** 装载按钮容器 */
	protected JToolBar toolBar = null;

	public JecnToolBarRightShowButton() {
		initComponent();
	}

	public JToolBar getJToolBar() {
		return toolBar;
	}

	private void initComponent() {
		toolBar = new JToolBar();

		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 图片内容显示方式：居中
		this.setHorizontalAlignment(SwingConstants.CENTER);
		// 不显示焦点状态
		this.setFocusPainted(false);

		toolBar.setOpaque(false);
		toolBar.setFloatable(false);
		toolBar.setLayout(new BorderLayout());
		toolBar.setBorder(null);

		toolBar.add(this);
	}
}
