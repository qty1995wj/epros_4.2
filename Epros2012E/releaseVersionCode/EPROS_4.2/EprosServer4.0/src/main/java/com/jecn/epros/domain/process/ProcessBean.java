package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.domain.org.Department;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class ProcessBean {

    private static final String[] LEVEL_NAMES = {"一级", "二级", "三级", "四级", "五级", "六级", "七级", "八级"};

    @ApiModelProperty(value = "主键", position = 0)
    private String id;

    @ApiModelProperty(value = "名称", position = 1)
    private String name;

    @ApiModelProperty(value = "编号", position = 2)
    private String number;

    @ApiModelProperty(value = "责任部门", position = 3)
    private Department department;

    @ApiModelProperty(value = "编制人", position = 4)
    private String writer;

    @ApiModelProperty(value = "责任人", position = 5)
    private String personInCharge;

    @ApiModelProperty(value = "版本号", position = 6)
    private String version;

    @ApiModelProperty(value = "发布日期", position = 7)
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    private Date publishDate;

    @ApiModelProperty(value = "级别", position = 8)
    private Integer level;

    @ApiModelProperty(value = "流程类型 0流程架构 1流程", position = 9)
    private String type;

    @ApiModelProperty(value = "文控ID", position = 10)
    private long historyId;

    @ApiModelProperty(value = "专员名称", position = 11)
    private String commissionerName;

    @ApiModelProperty(value = "关键字", position = 12)
    private String keyword;

    @ApiModelProperty(value = "供应商", position = 13)
    private String supplier;

    @ApiModelProperty(value = "客户", position = 14)
    private String customer;

    @ApiModelProperty(value = "风险与机会", position = 15)
    private String riskAndOpportunity;// 风险与机会

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getRiskAndOpportunity() {
        return riskAndOpportunity;
    }

    public void setRiskAndOpportunity(String riskAndOpportunity) {
        this.riskAndOpportunity = riskAndOpportunity;
    }


    public String getCommissionerName() {
        return commissionerName;
    }

    public void setCommissionerName(String commissionerName) {
        this.commissionerName = commissionerName;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(long historyId) {
        this.historyId = historyId;
    }

    public ProcessBean() {
    }

    public ProcessBean(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getLevel() {
        if (level != null) {
            return String.valueOf(level);
        }
        return null;
    }

    //----------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getPersonInCharge() {
        return personInCharge;
    }

    public void setPersonInCharge(String personInCharge) {
        this.personInCharge = personInCharge;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
