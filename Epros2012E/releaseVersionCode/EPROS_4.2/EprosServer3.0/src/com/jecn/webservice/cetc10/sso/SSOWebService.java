/**
 * SSOWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cetc10.sso;

public interface SSOWebService extends javax.xml.rpc.Service {
    public java.lang.String getSSOWebServiceSoapAddress();

    public com.jecn.webservice.cetc10.sso.SSOWebServiceSoap getSSOWebServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.cetc10.sso.SSOWebServiceSoap getSSOWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
