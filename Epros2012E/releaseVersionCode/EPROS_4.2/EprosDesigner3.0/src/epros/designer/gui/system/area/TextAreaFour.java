package epros.designer.gui.system.area;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.system.BasicComponent;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.util.JecnProperties;
import epros.draw.util.DrawCommon;

public class TextAreaFour implements BasicComponent {
	/** 流程输入Lab */
	private JLabel textAreaLab = new JLabel();
	/** 流程输入Field */
	private JecnTextArea textArea = new JecnTextArea();
	private JScrollPane textAreaScrollPane = new JScrollPane(textArea);

	private String text;

	/** 提示名称 */
	private String tipName;

	public TextAreaFour(int rows, JPanel infoPanel, Insets insets, String labName, String text, String tipName) {
		this.tipName = tipName;
		this.text = text == null ? "" : text;
		initLayout(rows, infoPanel, insets, labName, text);
	}

	private void initLayout(int rows, JPanel infoPanel, Insets insets, String labName, String text) {
		textAreaLab.setText(labName);
		textArea.setText(this.text);
		// 输入
		textArea.setBorder(null);
		textArea.setRows(4);
		textAreaScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		textAreaScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		textAreaScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// ==============流程输入==========================================
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(textAreaLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(textAreaScrollPane, c);
	}

	public TextAreaFour(int rows, JPanel infoPanel, Insets insets, String labName, String text, String tipName,
			boolean isRequest) {
		this.tipName = tipName;
		this.text = text == null ? "" : text;
		initLayout(rows, infoPanel, insets, labName, text);

		if (isRequest) {
			GridBagConstraints c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
					GridBagConstraints.NONE, insets, 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}
	}

	public boolean isUpdate() {
		if (!text.equals(textArea.getText())) {
			return true;
		}
		return false;
	}

	public String getResultStr() {
		return textArea.getText().trim();
	}

	public boolean isValidateNotPass(JLabel verfyLab) {
		if (DrawCommon.checkNoteLength(textArea.getText().toString())) {
			verfyLab.setText(tipName + JecnProperties.getValue("lengthNotOut"));
			return true;
		}
		return false;
	}

	@Override
	public boolean isRequest() {
		return false;
	}

	@Override
	public void setEnabled(boolean enable) {

	}
}
