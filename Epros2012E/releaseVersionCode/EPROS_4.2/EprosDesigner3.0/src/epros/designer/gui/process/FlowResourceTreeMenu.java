package epros.designer.gui.process;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.tree.DefaultTreeModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.autoCode.AutoCodeNodeData;
import com.jecn.epros.server.bean.popedom.DownloadPopedomBean;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnFlowFileDownloadPermDialog;
import epros.designer.gui.common.JecnLookSetDialog;
import epros.designer.gui.common.JecnDesignerCommon.CommonConfigEnum;
import epros.designer.gui.error.ErrorCommon;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.process.flow.AddFlowDialog;
import epros.designer.gui.process.flow.AddFlowFileDialog;
import epros.designer.gui.process.flow.file.FlowFileVersionDialog;
import epros.designer.gui.process.flowmap.AddAerialMapDialog;
import epros.designer.gui.process.flowmap.AddFlowMapDialog;
import epros.designer.gui.process.flowmap.FlowMapDesDialog;
import epros.designer.gui.process.flowmap.FlowMapRuleDialog;
import epros.designer.gui.process.flowmap.SettingProcessFileInfoDialog;
import epros.designer.gui.publish.FlowMapPublishDialog;
import epros.designer.gui.publish.FlowPublishDialog;
import epros.designer.gui.rule.RuleDoControlPanel;
import epros.designer.gui.system.ResourceSortDialog;
import epros.designer.gui.task.config.TempletChooseDialog;
import epros.designer.gui.task.jComponent.JecnFlowTaskJDialog;
import epros.designer.gui.task.jComponent.reSubmit.JecnFlowReSubmitTaskJDialog;
import epros.designer.service.process.JecnSelectProcessDownload;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.designer.util.JecnValidateCommon;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.ImportFlowFilter;
import epros.draw.gui.top.toolbar.io.JecnIOUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * @author yxw 2012-6-5
 * @description：流程右键菜单项
 */
public class FlowResourceTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(FlowResourceTreeMenu.class);

	/** 设置操作说明信息 */
	private JMenuItem settingProcessFileInfo = new JMenuItem(JecnProperties.getValue("settingsInstruction"),
			new ImageIcon("images/menuImage/newFlowMap.gif"));
	/** 新建流程地图 */
	private JMenuItem newFlowMapItem = new JMenuItem(JecnProperties.getValue("newFlowMap"), new ImageIcon(
			"images/menuImage/newFlowMap.gif"));
	/** 新建鸟瞰图 */
	private JMenuItem newAerialMapItem = new JMenuItem(JecnProperties.getValue("newBirdviewMap"), new ImageIcon(
			"images/menuImage/newFlowMap.gif"));
	/** 新建流程图 */
	private JMenuItem newFlowItem = new JMenuItem(JecnProperties.getValue("newProcess"), new ImageIcon(
			"images/menuImage/newFlow.gif"));
	/** 新建流程图 （文件） */
	private JMenuItem newFlowFileItem = new JMenuItem(JecnProperties.getValue("newProcessFile"), new ImageIcon(
			"images/menuImage/newFlowFile.gif"));
	/** 推送流程图 */
	private JMenuItem sysNcFlowItem = new JMenuItem(JecnProperties.getValue("sysNcFlow"), new ImageIcon(
			"images/menuImage/sysNcFlow.png"));

	/** 复制流程 */
	private JMenuItem copyFlowsItem = new JMenuItem(JecnProperties.getValue("CopyProcess"), new ImageIcon(
			"images/menuImage/sysNcFlow.png"));

	/** 相关文件 */
	private JMenuItem relFileItem = new JMenuItem(JecnProperties.getValue("relateFile"));
	/** 搜索 */
	private JMenuItem searchItem = new JMenuItem(JecnProperties.getValue("search"), new ImageIcon(
			"images/menuImage/search.gif"));
	/** 节点排序 */
	private JMenuItem nodeSortItem = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon(
			"images/menuImage/nodeSort.gif"));
	/** 节点移动 */
	private JMenuItem nodeMoveItem = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon(
			"images/menuImage/nodeMove.gif"));
	/** 刷新 */
	private JMenuItem refreshItem = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon(
			"images/menuImage/refresh.gif"));
	/** 发布 */
	private JMenuItem releaseItem = new JMenuItem(JecnProperties.getValue("release"), new ImageIcon(
			"images/menuImage/release.gif"));
	/** 撤销发布 */
	private JMenuItem cancelReleaseItem = new JMenuItem(JecnProperties.getValue("cancelRelease"), new ImageIcon(
			"images/menuImage/cancelRelease.gif"));
	/** 直接发布-不记录版本 */
	private JMenuItem directReleaseItem = new JMenuItem(JecnProperties.getValue("directRelease"), new ImageIcon(
			"images/menuImage/directRelease.gif"));
	/** 提交审批 */
	private JMenuItem submitAppItem = new JMenuItem(JecnProperties.getValue("submitApp"), new ImageIcon(
			"images/menuImage/submitApp.gif"));
	/** 重新提交 */
	private JMenuItem reSubmitAppItem = new JMenuItem(JecnProperties.getValue("resubmit"));
	/** 重命名 */
	private JMenuItem renameItem = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon(
			"images/menuImage/rename.gif"));

	/** 查阅权限(部门) 已与（岗位）合并为（查阅权限） */
	private JMenuItem lookItem = new JMenuItem(JecnProperties.getValue("lookPermissions"));
	// /** 查阅权限(岗位) */
	// private JMenuItem positionLookItem = new JMenuItem(
	// JecnProperties.getValue("positionLookI"), new ImageIcon(
	// "images/menuImage/positionLook.gif"));
	/** 流程架构描述 */
	private JMenuItem flowMapDesItem = new JMenuItem(JecnProperties.getValue("processArchitectureProperties"),
			new ImageIcon("images/menuImage/flowMapDes.gif"));
	/** 删除 */
	private JMenuItem deleteItem = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon(
			"images/menuImage/delete.gif"));

	/** 文控信息 */
	private JMenuItem historyItem = new JMenuItem(JecnProperties.getValue("historyRecord"), new ImageIcon(
			"images/menuImage/historyRecord.gif"));
	/** 操作说明 下载 */
	private JMenuItem downItem = new JMenuItem(JecnProperties.getValue("downloadOpDes"), new ImageIcon(
			"images/menuImage/downloadOpDes.gif"));
	/** 操作说明下载(带附件) */
	private JMenuItem downFileItem = new JMenuItem(JecnProperties.getValue("downloadOpDesFile"), new ImageIcon(
			"images/menuImage/downloadOpDesFile.gif"));

	/** 解除编辑 */
	private JMenuItem removeEditItem = new JMenuItem(JecnProperties.getValue("removeEdit"), new ImageIcon(
			"images/menuImage/removeEdit.gif"));

	/** 重置编号 */
	private JMenuItem reNumItem = new JMenuItem(JecnProperties.getValue("renumber"), new ImageIcon(
			"images/menuImage/renumber.gif"));

	/** 设计者权限：只显示设计权限节点/显示所有节点 */
	private JMenuItem designAccessMenu = new JMenuItem(JecnProperties.getValue("showJurisdictionNodes"));

	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> selectNodes = null;

	private JecnTree jTree = null;

	private Separator separatorOne = new Separator();
	private Separator separatorTwo = new Separator();
	private Separator separatorThree = new Separator();
	private Separator separatorFour = new Separator();

	/** 查阅权限(部门) 已与（岗位）合并为（查阅权限） */
	private JMenuItem flowFileDownItem = new JMenuItem(JecnProperties.getValue("processFileDownloadPermissions"));

	/** 任务配置 */
	/**
	 * 流程架构任务设置
	 */
	private JMenuItem mapTaskSet = new JMenuItem(JecnProperties.getValue("processMap_Task_Setting"));
	/**
	 * 流程任务 设置
	 */
	private JMenuItem processTaskSet = new JMenuItem(JecnProperties.getValue("process_Task_Setting"));
	/**
	 * 批量发布不记录文控
	 */
	private JMenuItem batchReleaseNotRecord = new JMenuItem(JecnProperties.getValue("batchReleaseNoRecord"));
	/**
	 * 批量撤销发布
	 */
	private JMenuItem batchCancelRelease = new JMenuItem(JecnProperties.getValue("cancelRelease"));

	/** 自动编号 */
	private JMenuItem autoCodeMenu = new JMenuItem(JecnProperties.getValue("autoNum"));

	/** 流程、架构 维护支持文件 */
	private JMenuItem architectureUploadFileMenu = new JMenuItem(JecnProperties.getValue("maintenanceSupportFile"));
	/**
	 * 提交废止
	 */
	private JMenuItem desuetudeItem = new JMenuItem(JecnProperties.getValue("submitAbolish"));
	/**
	 * 转换为流程图
	 */
	private JMenuItem transfromToProcessMenu = new JMenuItem(JecnProperties.getValue("conversionToFlowchart"));
	/** 发布提醒 **/
	private JMenuItem pubNoteItem = new JMenuItem();
	/**
	 * 流程贯通性检查
	 */
	private JMenuItem flowCheckout = new JMenuItem(JecnProperties.getValue("processContinuity"));
	/**
	 * 角色规范性检查
	 */
	private JMenuItem roleCheckout = new JMenuItem(JecnProperties.getValue("roleCheck"));
	private JMenuItem mnInitItem = new JMenuItem("蒙牛归档初始化");
	private JMenuItem transToProcessItem = new JMenuItem(JecnProperties.getValue("transToProcess"));// "转换为流程图"
	/** 流程文件版本信息情况 */
	private JMenuItem versionFile = new JMenuItem(JecnProperties.getValue("versionInfo"));// "版本信息"

	/** 生成APQC架构图 */
	private JMenuItem createAPQCMapItem = new JMenuItem(JecnProperties.getValue("GeneratingAPQC"), new ImageIcon(
			"images/menuImage/createOrgMap.gif"));

	/**
	 * @param jTree
	 * @param selectNodes
	 * @param selectMutil
	 *            0选择一个节点，1选择多个节点
	 */
	public FlowResourceTreeMenu(JecnTree jTree, List<JecnTreeNode> selectNodes, int selectMutil) {
		this.jTree = jTree;
		this.selectNodes = selectNodes;
		if (0 == selectMutil) {
			selectNode = selectNodes.get(0);
			pubNoteItem = JecnUtil.createPubNoteMenu(selectNode);
			pubNoteItem.setVisible(false);
		}
		if (selectNodes.size() == 1) {
			// 单选设置menu
			this.singleSelect();
		} else {
			// 多选设置menu
			this.mutilSelect();
		}
		this.add(transToProcessItem);
		this.add(mnInitItem);
		this.add(newFlowMapItem);// 新建流程地图
		if (JecnConfigTool.newAerialMap()) {// 新建鸟瞰图
			this.add(newAerialMapItem);
		}
		this.add(newFlowItem);// 新建流程图

		this.add(copyFlowsItem);

		if (selectNode != null && selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMap) {
			int authType = JecnTreeCommon.isAuthNode(selectNode);
			if (authType == 1 || authType == 2) {
				this.add(createAPQCMapItem);
			}
		}

		if (JecnConfigTool.isShowHeadInfo()
				&& (JecnDesignerCommon.isAdmin() || JecnDesignerCommon.isSecondAdmin() || JecnDesignerCommon
						.isProcessAdmin())) {
			this.add(settingProcessFileInfo);
		}

		if (!JecnConfigTool.isHiddenProcessFile()) {
			this.add(newFlowFileItem);
		}

		if (isProcessPush() && !isProcessFile()) { // 29所和中石化石勘院
			this.add(sysNcFlowItem); // 同步
		}
		this.add(relFileItem);
		this.add(separatorOne);

		this.add(renameItem);// 重命名
		this.add(nodeSortItem);// 节点排序
		this.add(nodeMoveItem);// 节点移动
		this.add(refreshItem);// 刷新
		this.add(deleteItem);// 删除
		this.add(separatorTwo);
		this.add(pubNoteItem);
		this.add(submitAppItem);// 提交审批
		// this.add(reSubmitAppItem);// 重新提交
		this.add(desuetudeItem);
		this.add(releaseItem);// 发布
		this.add(directReleaseItem);// 发布 不记录版本
		this.add(cancelReleaseItem);// 撤销发布
		this.add(mapTaskSet);
		this.add(processTaskSet);

		this.add(separatorThree);
		this.add(lookItem);// 查阅权限

		String flowFileDownMenuShow = ConnectionPool.getConfigAciton().selectValueFromCache(
				ConfigItemPartMapMark.flowFileDownMenuShow);
		if (!isProcessFile() && "1".equals(flowFileDownMenuShow) && selectNodes.size() == 1
				&& selectNodes.get(0).getJecnTreeBean().getId() != 0 && JecnDesignerCommon.isAdmin()) {
			this.add(flowFileDownItem);
		}

		// this.add(positionLookItem);// 查阅权限（岗位）
		this.add(separatorFour);
		if (JecnConfigTool.isFiberhome()) {// 暂时只有烽火有自动编号功能
			this.add(reNumItem);// 重置编号
		}
		if (JecnConstants.isPubShow()) {// 非D2版本
			this.add(removeEditItem);// 解除编辑
		}

		if (JecnConstants.isPubShow()) {
			this.add(historyItem);// 文控信息
		}
		// 如果存在历史版本
		if (selectNode != null && hasVersions()) {
			this.add(versionFile);
		}

		if (isProcess()) {
			this.add(downItem);// 操作说明下载
			this.add(downFileItem);// 操作说明下载(带附件)
		}

		this.add(flowMapDesItem);// 流程地图描述

		this.add(batchReleaseNotRecord);
		this.add(batchCancelRelease);

		if (JecnConfigTool.isShowAutoNumber()) {// 是否允许自动编号
			this.add(autoCodeMenu);
		}

		if (JecnConfigTool.isArchitectureUploadFile()) {// 是否允许维护上传文件
			this.add(architectureUploadFileMenu);
		}

		this.add(flowCheckout);
		this.add(roleCheckout);

		initDesigner();
		// 初始化事件
		menuItemActionInit();
	}

	private boolean hasVersions() {
		if (selectNode == null || selectNode.getJecnTreeBean().getId() == 0) {
			return false;
		}
		if (isProcessFile()) {
			return true;
		} else {
			try {
				return ConnectionPool.getProcessAction().getJecnFlowStructureT(selectNode.getJecnTreeBean().getId())
						.getFileContentId() != null;
			} catch (Exception e) {
				log.error("", e);
			}
		}
		return false;
	}

	/**
	 * 文件版本信息
	 */
	protected void versionFilePerformed() {
		if (selectNode == null) {
			return;
		}
		FlowFileVersionDialog versionFileDialog = new FlowFileVersionDialog(selectNode, jTree);
		versionFileDialog.setVisible(true);
	}

	/**
	 * 设计者权限显示
	 */
	private void initDesigner() {
		if (JecnDesignerCommon.isAdmin()) {
			return;
		}
		String value = JecnDesignerCommon.redCommonConfigProperties(CommonConfigEnum.designAccessMenu);
		if ("1".equals(value)) {
			/*
			 * 显示所有节点
			 */
			designAccessMenu.setText(JecnProperties.getValue("showNodes"));
		} else {
			/*
			 * 显示设计权限节点
			 */
			designAccessMenu.setText(JecnProperties.getValue("showJurisdictionNodes"));
		}
		this.add(designAccessMenu);
	}

	private void menuItemActionInit() {
		transToProcessItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				transToProcessItemAction();
			}
		});
		newFlowMapItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newFlowMapItemAction();
			}
		});
		/**
		 * 设置操作说明 下载文件 信息
		 */
		settingProcessFileInfo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				settingProcessFileInfo();
			}
		});

		newFlowItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newFlowItemAction();
			}
		});

		copyFlowsItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				copyFlowsItemAction();
			}
		});
		newFlowFileItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				newFlowFileItemAction();
			}
		});

		transfromToProcessMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				transfromToProcessAction();
			}
		});
		sysNcFlowItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				processPushSync();
			}
		});
		relFileItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				FlowMapRuleDialog flowMapRuleDialog = new FlowMapRuleDialog(selectNode);
				flowMapRuleDialog.setVisible(true);
			}
		});
		cancelReleaseItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 提示是否撤销发布
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("isCancelRelease"), null, JecnOptionPane.YES_NO_OPTION,
						JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					return;
				}
				try {
					ConnectionPool.getProcessAction().cancelRelease(selectNode.getJecnTreeBean().getId(),
							selectNode.getJecnTreeBean().getTreeNodeType(), JecnUtil.getUserId());
					selectNode.getJecnTreeBean().setPub(false);
					((DefaultTreeModel) jTree.getModel()).reload(selectNode);
					JecnDesignerCommon.changWorkTabIcon(selectNode, selectNode.getJecnTreeBean().getTreeNodeType()
							.toString()
							+ "NoPub");
				} catch (Exception e1) {
					log.error("cancelReleaseItem is error", e1);
				}
			}
		});
		directReleaseItem.addActionListener(new ActionListener() {// 发布，不记录文控
					@Override
					public void actionPerformed(ActionEvent e) {
						if (selectNode == null || JecnDesignerCommon.isOpenWorkFlow(selectNode)) {// 当前节点所对应的面板是否打开
							return;
						}
						JecnTreeBean treeBean = selectNode.getJecnTreeBean();
						if (treeBean.getTreeNodeType() == TreeNodeType.process
								&& JecnDesignerCommon.isOpenWorkFlow(selectNode)) {// 流程节点，流程图未打开情况
							return;
						}

						try {
							boolean isInTask = JecnTool.submitToTask(treeBean);
							if (isInTask) {
								return;
							}
						} catch (Exception e1) {
							log.error("releaseItemAction is error " + "treeBean.getTreeNodeType() = "
									+ treeBean.getTreeNodeType().toString(), e1);
						}

						// 双击树节点，面板切换到选中的树节点对应面板
						JecnDesignerCommon.checkFlowTreeTabPanel(selectNode.getJecnTreeBean().getId());
						JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
						if (!isEditAuth(desktopPane)) {// 判断面板有无编辑权限
							JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("noEditingRights"));
							return;
						}
						if (desktopPane != null && desktopPane.getFlowMapData().isSave()) {// 面板是否保存
							if (!JecnValidateCommon.isSaveMap(selectNode))
								return;
						}
						if (TreeNodeType.processMap == selectNode.getJecnTreeBean().getTreeNodeType()) {// 流程节点
							// 检查错误信息
							if (ErrorCommon.INSTANCE.checkflowMapControlError(selectNode.getJecnTreeBean().getId())) {
								return;
							}
						}
						if (TreeNodeType.process == selectNode.getJecnTreeBean().getTreeNodeType()) {// 流程节点
							// 检查错误信息
							if (ErrorCommon.INSTANCE.checkControlError()) {
								return;
							}
						}
						// 因为 验证错误信息 流程文件 和流程图 存在 一定的 功能性限制 所以单独提出来 只验证流程文件
						if (TreeNodeType.processFile == selectNode.getJecnTreeBean().getTreeNodeType()) {// 流程节点
							// 检查错误信息
							if (ErrorCommon.INSTANCE.checkControlErrorProcessFile(selectNode)) {
								return;
							}
						}
						// 提示是否不记录版本发布
						int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
								.getValue("isnotRemberPublish"), null, JecnOptionPane.YES_NO_OPTION,
								JecnOptionPane.INFORMATION_MESSAGE);
						if (option == JecnOptionPane.NO_OPTION) {
							return;
						}
						try {
							ConnectionPool.getProcessAction().directRelease(selectNode.getJecnTreeBean().getId(),
									selectNode.getJecnTreeBean().getTreeNodeType(), JecnUtil.getUserId());
							JecnDesignerCommon.changWorkTabIcon(selectNode, selectNode.getJecnTreeBean()
									.getTreeNodeType().toString());
							selectNode.getJecnTreeBean().setPub(true);
							selectNode.getJecnTreeBean().setUpdate(false);
							((DefaultTreeModel) jTree.getModel()).reload(selectNode);
							JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("publicSucees"));
						} catch (Exception e1) {
							log.error("directReleaseItem is error", e1);
						}
						// 发布成功
					}
				});
		nodeSortItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeSortItemAction();
			}
		});
		nodeMoveItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeMoveItemAction();
			}
		});
		refreshItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshItemAction();
			}
		});
		releaseItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				releaseItemAction();
			}
		});
		submitAppItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				submitAppItemAction(0);
			}
		});
		reSubmitAppItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				reSubmitAppItemAction();
			}
		});
		desuetudeItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				submitAppItemAction(2);
			}
		});
		renameItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				renameItemAction();
			}
		});

		lookItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lookItemAction();
			}
		});
		downItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downItemAction();
			}
		});
		downFileItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downFileItemAction();
			}
		});
		// positionLookItem.addActionListener(new ActionListener() {
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// positionLookItemAction();
		// }
		// });
		flowMapDesItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				flowMapDesItemAction();
			}
		});
		deleteItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteItemAction();
			}
		});
		removeEditItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				removeEditItemAction();
			}
		});
		historyItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				historyItemAction();
			}
		});
		reNumItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reNumItemItemAction();
			}
		});
		designAccessMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				designAccessMenu();
			}
		});

		flowFileDownItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				flowFileDownloadAction();

			}
		});

		mapTaskSet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				taskEditAction(4);
			}
		});
		processTaskSet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				taskEditAction(0);

			}
		});

		batchReleaseNotRecord.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BatchPubTask t = new BatchPubTask();
				JecnLoading.start(t);
			}
		});

		autoCodeMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				autoCodeAction();
			}
		});

		batchCancelRelease.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingWorker<Void, Void> t = new SwingWorker<Void, Void>() {

					@Override
					protected Void doInBackground() throws Exception {
						batchCancelReleaseAction();
						return null;
					}

					@Override
					public void done() {
						JecnLoading.stop();
					}

				};
				JecnLoading.start(t);
			}
		});
		architectureUploadFileMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				architectureUploadFileAction();
			}
		});
		versionFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				versionFilePerformed();

			}
		});

		newAerialMapItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newAerialMapAction();
			}
		});

		mnInitItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mnInit();
			}
		});

		createAPQCMapItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				createAPQCMapAction();
			}
		});
	}

	private void createAPQCMapAction() {
		// 1 、 首先判断面板是否打开
		if (JecnDesignerCommon.isOpenWorkFlowMap(selectNode)) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 是否有编辑权限
		if (desktopPane.getFlowMapData().isAuthSave() != 1 && desktopPane.getFlowMapData().isAuthSave() != 2) {
			JecnOptionPane.showMessageDialog(null, "没有编辑权限!");
			return;
		}
		if (!desktopPane.getFlowMapData().getFlowId().equals(selectNode.getJecnTreeBean().getId())) {// 流程图或流程地图节点
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("pleaseOpenProcess"));
			return;
		}

		JecnIOUtil.removeAllWorkflow();

		// 获取当前节点直属子节点
		try {
			List<JecnTreeBean> childTreeBeans = ConnectionPool.getProcessAction().getChildFlows(
					selectNode.getJecnTreeBean().getId(), JecnConstants.projectId, false);
			// 获取当前节点的直属子集
			List<JecnTreeBean> secondTreeBeans = ConnectionPool.getProcessAction().getChildFlows(
					getIdsByList(childTreeBeans));

			int valueX = 50;
			// 按照父节点 分组
			for (JecnTreeBean parentBean : childTreeBeans) {
				List<JecnTreeBean> nodeChilds = getTreeBeansMap(secondTreeBeans, parentBean);
				createAPQCMap(nodeChilds, parentBean, valueX);
				valueX += 160;
			}
			desktopPane.getCurrDrawPanelSizeByPanelList();
			JecnTreeCommon.flushCurrentTreeNodeToUpdate();

			// 更新面板显示状态（编辑权限）
			JecnPaintFigureUnit.addOpenTipPanel();

			// 面板保存标识为true
			desktopPane.getFlowMapData().setSave(true);

			desktopPane.updateUI();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Long> getIdsByList(List<JecnTreeBean> list) {
		List<Long> resultIds = new ArrayList<Long>();
		for (JecnTreeBean treeBean : list) {
			resultIds.add(treeBean.getId());
		}
		return resultIds;
	}

	private void createAPQCMap(List<JecnTreeBean> nodeChilds, JecnTreeBean parentBean, int valueX) {
		int valueY = 60;

		// 创建第一级
		addAPQCNode(parentBean, valueX, valueY);
		// 获取子集最大数组数
		int size = nodeChilds.size();

		// 创建子集
		for (int i = 0; i < size; i++) {
			addAPQCNode(nodeChilds.get(i), valueX, 100 + valueY + i * 80);
		}

	}

	private void addAPQCNode(JecnTreeBean bean, int valueX, int valueY) {
		JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(MapElemType.Pentagon);
		JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;
		baseFigure.getFlowElementData().setFigureText(bean.getName());
		baseFigure.getFlowElementData().getDesignerFigureData().setLinkId(bean.getId());
		JecnDesignerCommon.addElemLink(baseFigure, bean.getTreeNodeType() == TreeNodeType.processMap ? MapType.partMap
				: MapType.totalMap);
		Point point = new Point(valueX, valueY);
		JecnAddFlowElementUnit.addFigure(point, baseFigure);
	}

	private List<JecnTreeBean> getTreeBeansMap(List<JecnTreeBean> childs, JecnTreeBean jecnTreeBean) {
		if (childs == null) {
			return new ArrayList<JecnTreeBean>();
		}
		List<JecnTreeBean> result = new ArrayList<JecnTreeBean>();
		for (JecnTreeBean child : childs) {
			if (child.getPid().intValue() == jecnTreeBean.getId().intValue()) {
				result.add(child);
			}
		}
		// 按照sortId排序
		sortJecnActiveDataList(result);
		return result;
	}

	private void sortJecnActiveDataList(List<JecnTreeBean> list) {
		if (list == null || list.size() == 0) {
			return;
		}

		Collections.sort(list, new Comparator<JecnTreeBean>() {
			public int compare(JecnTreeBean r1, JecnTreeBean r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("参数不合法");
				}
				return r1.getSortId() - r2.getSortId();
			}
		});
	}

	protected void transToProcessItemAction() {
		// true已经显示 false没有显示
		// 转换之后无法恢复，是否继续？
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("transCannotRecover"), null,
				JecnOptionPane.YES_NO_CANCEL_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.OK_OPTION) {
			List<Long> nodes = new ArrayList<Long>();
			nodes.add(selectNode.getJecnTreeBean().getId());
			try {
				// TODO
				ConnectionPool.getProcessAction().transToProcess(nodes,
						JecnConstants.loginBean.getJecnUser().getPeopleId());
				JecnTreeBean jecnTreeBean = selectNode.getJecnTreeBean();
				jecnTreeBean.setTreeNodeType(TreeNodeType.process);
				jecnTreeBean.setNodeType(0);
				jecnTreeBean.setUpdate(true);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
				// 转换成功，双击节点打开流程图
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("transSuccessDoubleClick"));
			} catch (Exception e) {
				log.error("", e);
			}
		}
	}

	protected void mnInit() {
		try {
			ConnectionPool.getProcessAction().mnInit(0);
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), "归档初始化成功");
		} catch (Exception e) {
			log.error("", e);
		}
	}

	protected void copyFlowsItemAction() {
		JecnLoading.start(new SwingWorker<String, Void>() {
			@Override
			protected String doInBackground() throws Exception {
				return saveCopyNodes();
			}

			@Override
			public void done() {
				JecnLoading.stop();
			}
		});
	}

	public boolean validateNodeRepeat(String name, Long id, Long pId, TreeNodeType type) {
		try {
			// 流程地图
			if (type == TreeNodeType.processMap) {
				return ConnectionPool.getProcessAction().validateAddName(name, pId, 0, JecnConstants.projectId);
				// 流程图
			} else if (type == TreeNodeType.process) {
				return ConnectionPool.getProcessAction().validateAddName(name, pId, 1, JecnConstants.projectId);
			}

		} catch (Exception e1) {
			log.error("EditFlowNameDialog validateNodeRepeat is error", e1);
		}
		return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
	}

	private String saveCopyNodes() {
		// 复制当前节点到指定目录下
		JecnTreeBean selectBean = selectNode.getJecnTreeBean();
		boolean isFlow = selectBean.getTreeNodeType() == TreeNodeType.process;
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		ProcessChooseDialog processChooseDialog = new ProcessChooseDialog(list, isFlow ? 3 : 41, isFlow ? 2 : 1);
		processChooseDialog.setSelectMutil(false);
		processChooseDialog.setVisible(true);
		if (processChooseDialog.isOperation() && list.size() > 0) {// 点击确定
			if (validateNodeRepeat(selectNode.getJecnTreeBean().getName(), selectNode.getJecnTreeBean().getId(), list
					.get(0).getId(), selectNode.getJecnTreeBean().getTreeNodeType())) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("nameHaved"));
				return null;
			}
			int appType = list.get(0).getApproveType();
			int nodeType = list.get(0).getNodeType();
			if (nodeType == 2) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("NoCopyBirdView"));
				return "";
			}
			if (appType == 2) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("copyingAbolitionNode"));
				return "";
			}
			int authType = JecnTreeCommon.isAuthNode(new JecnTreeNode(list.get(0)));
			if (authType == 0) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("noPopedomOp"));
				return "";
			}
			// 提示是否包含子节点
			int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("doesNodes"), null, JecnOptionPane.YES_NO_CANCEL_OPTION,
					JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.CANCEL_OPTION) {
				return "";
			}
			try {
				List<Long> copyIds = new ArrayList<Long>();
				copyIds.add(selectBean.getId());
				Long copyToId = list.get(0).getId();
				ConnectionPool.getProcessAction().copyNodeDatasHandle(copyIds, copyToId, option, JecnUtil.getUserId());
				// 刷新节点
				List<JecnTreeBean> refreshNodes = JecnDesignerCommon.getDesignerProcessTreeBeanList(copyToId,
						JecnDesignerCommon.isOnlyViewDesignAuthor());
				JecnTreeNode refreshNode = JecnTreeCommon.getTreeNode(list.get(0), jTree);
				JecnTreeCommon.refreshNode(refreshNode, refreshNodes, jTree);
				return "sucess";
			} catch (Exception e) {
				log.info("FlowResourceTreeMenu saveCopyNodes is error", e);
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 新建鸟瞰图
	 */
	protected void newAerialMapAction() {
		AddAerialMapDialog addFlowMapDiaolog = new AddAerialMapDialog(selectNode, jTree);
		addFlowMapDiaolog.setVisible(true);
	}

	protected void reSubmitAppItemAction() {
		if (selectNode == null || JecnDesignerCommon.isOpenWorkFlow(selectNode)) {
			return;
		}

		JecnTreeBean treeBean = selectNode.getJecnTreeBean();
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane != null && desktopPane.getFlowMapData().isSave()) {// 面板是否保存
			if (!JecnValidateCommon.isSaveMap(selectNode))
				return;
		}
		if (treeBean.getTreeNodeType().equals(TreeNodeType.process)) {// 流程节点验证图形是否包含错误信息
			// 点击树节点，切换主面板
			JecnDesignerCommon.checkFlowTreeTabPanel(selectNode.getJecnTreeBean().getId());
			// 检查错误信息
			if (ErrorCommon.INSTANCE.checkControlError()) {
				return;
			}
		}
		if (!JecnTreeCommon.canApprove(selectNode, 0)) {
			return;
		}
		// 流程任务审批窗口
		JecnFlowReSubmitTaskJDialog flowReSubmitTaskJDialog = new JecnFlowReSubmitTaskJDialog(jTree, selectNode, 0);
		flowReSubmitTaskJDialog.setVisible(true);

	}

	private void architectureUploadFileAction() {
		try {
			// 获取 可维护的文件目录ID
			Long relatedFileId = JecnDesignerCommon.getArchitectureFileDirId(selectNode);
			// 3、 定位到文档管理
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(relatedFileId);
			jecnTreeBean.setName(selectNode.getJecnTreeBean().getName());
			// 将输入表格上的数据传到文件选择框中显示
			FileChooseDialog fileChooseDialog = new FileChooseDialog(new ArrayList<JecnTreeBean>(), relatedFileId);
			// 只能选择一个文件
			fileChooseDialog.setSelectMutil(true);
			fileChooseDialog.setVisible(true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void autoCodeAction() {
		JecnDesignerCommon.autoCodeAction(selectNode, null, false);
	}

	protected void taskEditAction(int taskType) {
		/*
		 * String curSelectTempletId =
		 * JecnTreeCommon.getTaskTempletIdBySelf(selectNode, taskType); //
		 * 弹出一个面板 TaskConfigSelectDialog taskEditDialog = new
		 * TaskConfigSelectDialog(selectNode, curSelectTempletId, taskType);
		 * taskEditDialog.setVisible(true);
		 */
		TempletChooseDialog taskEditDialog = new TempletChooseDialog(selectNode.getJecnTreeBean().getId(), taskType);
		taskEditDialog.setVisible(true);
	}

	private void flowFileDownloadAction() {

		try {
			DownloadPopedomBean popedomBean = ConnectionPool.getOrganizationAction().getFlowFileDownloadPermissions(
					selectNode.getJecnTreeBean().getId());

			JecnFlowFileDownloadPermDialog d = new JecnFlowFileDownloadPermDialog(popedomBean, selectNode);
			d.setVisible(true);
		} catch (Exception e) {
			log.error("flowFileDownloadAction is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}
	}

	private void designAccessMenu() {
		// JecnTreeCommon.removeNode(jTree, selectNode);
		// 执行配置文件
		JecnDesignerCommon.writeDesignerProperties();
		// 刷新节点
		refreshItemAction();
	}

	private void newFlowMapItemAction() {
		AddFlowMapDialog addFlowMapDiaolog = new AddFlowMapDialog(selectNode, jTree);
		addFlowMapDiaolog.setVisible(true);
	}

	/**
	 * 设置下载的操作说明的 标题以及 公司logo等相关信息
	 */
	private void settingProcessFileInfo() {
		SettingProcessFileInfoDialog settingProcessFileInfoDialog = new SettingProcessFileInfoDialog(selectNode
				.getJecnTreeBean().getId());
		settingProcessFileInfoDialog.setVisible(true);
	}

	/**
	 * 新建流程图
	 * 
	 */
	private void newFlowItemAction() {
		AddFlowDialog addFlowDialog = new AddFlowDialog(selectNode, jTree);
		addFlowDialog.setVisible(true);
		refreshItemAction(); // 新建后刷新树节点
	}

	private void newFlowFileItemAction() {
		AddFlowFileDialog addFlowDialog = new AddFlowFileDialog(selectNode, jTree);
		addFlowDialog.setVisible(true);
		refreshItemAction();// 新建后刷新树节点
	}

	private void transfromToProcessAction() {
		try {
			// true已经显示 false没有显示
			int option = JecnOptionPane.showConfirmDialog(this,
					JecnProperties.getValue("conversionProcessChartPrompt"), null, JecnOptionPane.YES_NO_CANCEL_OPTION,
					JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return;
			}

			JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(
					selectNode.getJecnTreeBean().getId());
			flowStructureT.setIsFlow(1L);
			// flowStructureT.setFileContentId(null);
			flowStructureT.setNodeType(0);
			ConnectionPool.getProcessAction().updateJecnFlowStructureT(flowStructureT);
			// 刷新树节点
			selectNode.getJecnTreeBean().setTreeNodeType(TreeNodeType.process);
			selectNode.getJecnTreeBean().setNodeType(0);
			// 更改打开面板的显示图标
			JecnDesignerCommon.changWorkTabIcon(selectNode, selectNode.getJecnTreeBean().getTreeNodeType().toString());
			((DefaultTreeModel) jTree.getModel()).reload(selectNode);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("transfromToProcessAction is error", e);
		}
	}

	private void nodeSortItemAction() {
		ResourceSortDialog rs = new ResourceSortDialog(selectNode, jTree);
		rs.setVisible(true);
	}

	private void nodeMoveItemAction() {
		if (!JecnDesignerCommon.validateRepeatNodesName(selectNodes)) {
			return;
		}
		JecnConstants.moveNodeType = selectNodes.get(0).getJecnTreeBean().getTreeNodeType();
		ProcessMoveChooseDialog pmcd = new ProcessMoveChooseDialog(selectNodes, jTree);
		pmcd.setVisible(true);
	}

	private void refreshItemAction() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			List<JecnTreeBean> list = JecnDesignerCommon.getDesignerProcessTreeBeanList(id, JecnDesignerCommon
					.isOnlyViewDesignAuthor());
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("refreshItemAction is error", ex);
		}
	}

	/**
	 * 面板是否有编辑权限
	 * 
	 * @return true 有编辑权限 false 无编辑权限
	 */
	private boolean isEditAuth(JecnDrawDesktopPane desktopPane) {
		if (selectNode.getJecnTreeBean().getTreeNodeType() != TreeNodeType.processFile) {
			int authSave = desktopPane.getFlowMapData().isAuthSave();
			return authSave == 4 ? false : true;
		}
		return true;
	}

	/**
	 * 发布
	 * 
	 */
	private void releaseItemAction() {
		if (selectNode == null) {// 验证节点所在面板是否已打开
			return;
		}

		JecnTreeBean treeBean = selectNode.getJecnTreeBean();
		if (treeBean.getTreeNodeType() == TreeNodeType.process && JecnDesignerCommon.isOpenWorkFlow(selectNode)) {// 流程节点，流程图未打开情况
			return;
		}
		try {
			boolean isInTask = JecnTool.submitToTask(treeBean);
			if (isInTask) {
				return;
			}
		} catch (Exception e) {
			log.error("releaseItemAction is error " + "treeBean.getTreeNodeType() = "
					+ treeBean.getTreeNodeType().toString(), e);
		}
		if (TreeNodeType.processMap == selectNode.getJecnTreeBean().getTreeNodeType()) { // 流程地图
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (!isEditAuth(desktopPane)) {// 判断面板有无编辑权限
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("noEditingRights"));
				return;
			}
			if (desktopPane != null && desktopPane.getFlowMapData().isSave()) {
				if (!JecnValidateCommon.isSaveMap(selectNode))
					return;
			}
			if (ErrorCommon.INSTANCE.checkflowMapControlError(selectNode.getJecnTreeBean().getId())) {
				return;
			}
			if (!JecnTreeCommon.canPub(selectNode)) {
				return;
			}
			FlowMapPublishDialog mapPublishDialog = new FlowMapPublishDialog(selectNode);
			mapPublishDialog.setVisible(true);
			if (mapPublishDialog.isOkBut()) {
				// 更改打开面板的显示图标
				JecnDesignerCommon.changWorkTabIcon(selectNode, selectNode.getJecnTreeBean().getTreeNodeType()
						.toString());
				selectNode.getJecnTreeBean().setPub(true);
				selectNode.getJecnTreeBean().setUpdate(false);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
				// 发布成功
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("publicSucees"));
			} else {
				return;
			}
		} else {
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (!isEditAuth(desktopPane)) {// 判断面板有无编辑权限
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("noEditingRights"));
				return;
			}
			if (desktopPane != null && desktopPane.getFlowMapData().isSave()) {// 面板是否保存
				if (!JecnValidateCommon.isSaveMap(selectNode))
					return;
			}
			// 点击树节点，切换主面板
			JecnDesignerCommon.checkFlowTreeTabPanel(selectNode.getJecnTreeBean().getId());
			// 检查错误信息 流程图
			if ((!JecnDesignerCommon.isProcessFile(selectNode.getJecnTreeBean().getTreeNodeType()) && ErrorCommon.INSTANCE
					.checkControlError())) {
				return;
			}
			// 流程文件
			if ((JecnDesignerCommon.isProcessFile(selectNode.getJecnTreeBean().getTreeNodeType()) && ErrorCommon.INSTANCE
					.checkControlErrorProcessFile(selectNode))) {
				return;
			}
			if (!JecnTreeCommon.canPub(selectNode)) {
				return;
			}
			FlowPublishDialog publishDialog = new FlowPublishDialog(selectNode);
			publishDialog.setVisible(true);
			if (publishDialog.isOkBut()) {
				JecnDesignerCommon.changWorkTabIcon(selectNode, selectNode.getJecnTreeBean().getTreeNodeType()
						.toString());
				selectNode.getJecnTreeBean().setPub(true);
				selectNode.getJecnTreeBean().setUpdate(false);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
				// 发布成功
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("publicSucees"));
			} else {
				return;
			}
		}
	}

	/**
	 * 提交审批
	 * 
	 * @param approveType
	 * 
	 */
	private void submitAppItemAction(int approveType) {
		if (selectNode == null) {
			return;
		}
		JecnTreeBean treeBean = selectNode.getJecnTreeBean();

		try {
			boolean isInTask = JecnTool.submitToTask(treeBean);
			if (isInTask) {
				return;
			}
		} catch (Exception e) {
			log.error("submitAppItemAction is error" + "treeBean.getTreeNodeType() = "
					+ treeBean.getTreeNodeType().toString(), e);
		}
		if (!JecnTreeCommon.canApprove(selectNode, approveType)) {
			return;
		}
		if (approveType == 0) {
			if (JecnDesignerCommon.isOpenWorkFlow(selectNode)) {
				return;
			}
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (!isEditAuth(desktopPane)) {// 判断面板有无编辑权限
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("noEditingRights"));
				return;
			}
			if (desktopPane != null && desktopPane.getFlowMapData().isSave()) {// 面板是否保存
				if (!JecnValidateCommon.isSaveMap(selectNode))
					return;
			}
			if (treeBean.getTreeNodeType().equals(TreeNodeType.process)) {// 流程节点验证图形是否包含错误信息
				// 点击树节点，切换主面板
				JecnDesignerCommon.checkFlowTreeTabPanel(selectNode.getJecnTreeBean().getId());
				// 检查错误信息
				if (ErrorCommon.INSTANCE.checkControlError()) {
					return;
				}
			}
			if (treeBean.getTreeNodeType().equals(TreeNodeType.processMap)) {// 流程
				// 地图节点验证图形是否包含错误信息
				if (ErrorCommon.INSTANCE.checkflowMapControlError(treeBean.getId())) {
					return;
				}
			}
			if (treeBean.getTreeNodeType().equals(TreeNodeType.processFile)) {// 流程
				// 文件节点验证图形是否包含错误信息
				if (ErrorCommon.INSTANCE.checkControlErrorProcessFile(selectNode)) {
					return;
				}
			}

		} else if (approveType == 2) {
			boolean isPub = treeBean.isPub(); // 是否发布
			if (!isPub) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("processUnpublished"));
				return;
			}
			int childCount = selectNode.getChildCount();
			if (childCount != 0) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("pleaseDeleteOrAbolishNodes"));
				return;
			}
		}
		// 流程任务审批窗口
		JecnFlowTaskJDialog flowTaskJDialog = new JecnFlowTaskJDialog(jTree, selectNode, approveType);
		flowTaskJDialog.setVisible(true);

	}

	private void renameItemAction() {
		// 重命名需要判断面板是否占用
		try {
			String occupierName = ConnectionPool.getProcessAction().getFlowOccupierUserNameById(
					selectNode.getJecnTreeBean().getId(), JecnConstants.loginBean.getJecnUser().getPeopleId());
			if (StringUtils.isNotBlank(occupierName)) {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("opened"));
				return;
			}
		} catch (Exception e) {
			log.error("renameItemAction is error", e);
		}
		EditFlowNameDialog editFlowNameDialog = new EditFlowNameDialog(selectNode, jTree);
		editFlowNameDialog.setVisible(true);
		JecnDesignerCommon.updateWorkTabTitleName(selectNode);
	}

	/**
	 * 文控信息
	 * 
	 */
	private void historyItemAction() {
		if (selectNode == null) {
			return;
		}
		if (TreeNodeType.processMap == selectNode.getJecnTreeBean().getTreeNodeType()) {
			RuleDoControlPanel ruleDoControlPanel = new RuleDoControlPanel(selectNode, selectNode.getJecnTreeBean()
					.getId(), 4);
			// 设置选中节点到窗体
			ruleDoControlPanel.setSelectNode(selectNode);
			ruleDoControlPanel.setVisible(true);
		} else if (JecnDesignerCommon.isProcess(selectNode.getJecnTreeBean().getTreeNodeType())) {
			RuleDoControlPanel ruleDoControlPanel = new RuleDoControlPanel(selectNode, selectNode.getJecnTreeBean()
					.getId(), 0);
			// 设置选中节点到窗体
			ruleDoControlPanel.setSelectNode(selectNode);
			ruleDoControlPanel.setVisible(true);
		}
	}

	private void lookItemAction() {
		try {

			LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(
					selectNode.getJecnTreeBean().getId(), 0);

			JecnLookSetDialog d = new JecnLookSetDialog(lookPopedomBean, selectNode, 0, true);
			d.setVisible(true);
			// List<JecnTreeBean> list = ConnectionPool.getOrganizationAction()
			// .getOrgAccessPermissions(
			// selectNode.getJecnTreeBean().getId(), 0);
			// JecnCommon.setDeptLook(selectNode, list, 0);
		} catch (Exception e) {
			log.error("lookItemAction is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}

	}

	private void downItemAction() {
		JecnSelectProcessDownload.processDownload(false, selectNode, selectNodes);
	}

	private void downFileItemAction() {
		JecnSelectProcessDownload.processDownload(true, selectNode, selectNodes);
	}

	private void flowMapDesItemAction() {
		// 权限验证
		if (JecnTreeCommon.isDesigner(selectNode) && !JecnTool.isBeInTask(selectNode)) {// 如果是设计者，并且处于任务中退出
			return;
		}
		// 流程地图描述
		FlowMapDesDialog flowMapDesDialog = new FlowMapDesDialog(jTree, selectNode);
		flowMapDesDialog.setVisible(true);
	}

	private void deleteItemAction() {
		if (selectNodes == null) {
			return;
		}
		if (selectNodes.size() == 1) {
			if (whetherThereIsAnAssociation(selectNodes)) {
				return;
			}
		}

		// 判断面板是否打开
		List<Long> listIds = new ArrayList<Long>();
		for (JecnTreeNode node : selectNodes) {
			listIds.add(node.getJecnTreeBean().getId());
			MapType mapType = MapType.partMap;
			if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMap) { // 流程地图
				mapType = MapType.totalMap;
			} else if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.process) { // 流程图
				mapType = MapType.partMap;
			}

			boolean isOpen = JecnDesignerCommon.isOpen(node.getJecnTreeBean().getId().intValue(), mapType);

			if (isOpen) {
				JecnOptionPane.showMessageDialog(this, "[" + node.getJecnTreeBean().getName() + "]"
						+ JecnProperties.getValue("alreadyOpenPleaseCloseInCarryingOutAfterDeleted"), null,
						JecnOptionPane.ERROR_MESSAGE);
				return;
			}
			// 调用判断流程面板方法
			boolean cIsopen = setFolderChildHideValue(node);

			if (cIsopen) {
				// 判断第二节点的面板是否打开
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("closeChildeNode"));
				return;
			}
		}
		if (listIds.size() == 0) {
			return;
		}
		try {
			if (!JecnConstants.isMysql) {
				if (!canDelOrAbolishFlow(listIds, JecnProperties.getValue("flowContinsFileCointinueDel"))) {
					return;
				}
				// 验证发布状态。type：0：流程；1：文件；2：制度
				if (!JecnTreeCommon.nodeValidate(0)) {
					boolean flowIsPub = ConnectionPool.getTaskRecordAction().idsIsPub(listIds, TreeNodeType.process);
					if (flowIsPub) {
						// 存在发布状态的节点，请查阅！
						JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
								.getValue("exitNodeIsPub"));
						return;
					}
				}
			}
			ConnectionPool.getProcessAction().deleteFlows(listIds, JecnConstants.getUserId(), JecnConstants.projectId);
		} catch (Exception e) {
			log.error("deleteItemAction is error", e);
			// 增加提示
		}
		/** 删除节点 */
		JecnTreeCommon.removeNodes(jTree, selectNodes);
	}

	private boolean canDelOrAbolishFlow(List<Long> listIds, String tip) {
		try {
			boolean hasFile = ConnectionPool.getProcessAction().belowFlowExistFile(listIds);
			String delTip = hasFile ? JecnProperties.getValue("flowContinsFileCointinueDel") : JecnProperties
					.getValue("isDelete");
			// 提示是否删除
			int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), delTip, null,
					JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return false;
			}
			boolean isInTask = ConnectionPool.getTaskRecordAction().idsIsInTask(listIds, TreeNodeType.process);
			if (isInTask) {
				// "节点下的流程处于审批中"
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("flowUnderTheNodeIsApproving"));
				return false;
			}
			if (hasFile) {
				List<Long> dirIds = ConnectionPool.getProcessAction().getRelatedFileIds(listIds);
				if (dirIds != null && dirIds.size() > 0) {
					boolean isFileInTask = ConnectionPool.getTaskRecordAction().idsIsInTask(dirIds, TreeNodeType.file);
					if (isFileInTask) {
						// 节点下的文件处于审批中
						JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
								.getValue("fileUnderTheNodeIsApproving"));
						return false;
					}
				}
			}
		} catch (Exception e1) {
			log.error("", e1);
		}
		return true;
	}

	/**
	 * 是否存在关联的流程
	 * 
	 * @param selectNodes2
	 * @return
	 */
	private boolean whetherThereIsAnAssociation(List<JecnTreeNode> selectNodes) {
		List ids = new ArrayList<Integer>();
		for (JecnTreeNode node : selectNodes) {
			ids.add(node.getJecnTreeBean().getId().intValue());
		}
		boolean isDel = false;
		List<Object[]> flowElement = ConnectionPool.getProcessAction().getFlowElementById(ids);
		if (!flowElement.isEmpty()) {
			StringBuilder perFlowName = new StringBuilder();
			String flowName = "";
			Iterator<JecnTreeNode> iterator = selectNodes.iterator();
			while (iterator.hasNext()) {
				JecnTreeNode node = iterator.next();
				perFlowName.delete(0, perFlowName.length());
				for (Object[] objects : flowElement) {
					if (node.getJecnTreeBean().getId().intValue() == Integer.valueOf(objects[2].toString())) {
						if (!perFlowName.toString().equals((String) objects[0].toString() + "-")) { // 去除重复名称
							perFlowName.append((String) objects[0].toString());
							perFlowName.append("-");
						}
					}
				}
				flowName = node.getJecnTreeBean().getName();
				perFlowName.deleteCharAt(perFlowName.length() - 1);
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), flowName
						+ JecnProperties.getValue("linkedWithProcess") + perFlowName
						+ JecnProperties.getValue("continueToDelete"), null, JecnOptionPane.YES_NO_OPTION,
						JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					iterator.remove();
					isDel = true;
					continue;
				} else {
					isDel = false;
					continue;
				}
			}

		}
		return isDel;
	}

	/**
	 * 判断打开的流程图中是否包含要删除的节点
	 * 
	 * @param node
	 *            (当前选中的节点)
	 */
	private boolean setFolderChildHideValue(JecnTreeNode node) {

		Enumeration e = null;
		MapType mapType = null;
		boolean flag = false;
		if (node.children() != null) {
			e = node.children(); // 获取子节点集合
		}
		while (e.hasMoreElements()) { // 判断是否为最后一个对象
			Object o = e.nextElement();
			JecnTreeNode treeNode = (JecnTreeNode) o;
			// 判断是流程图还是流程地图
			if (treeNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.processMap)) {
				mapType = MapType.totalMap;
			} else if (treeNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.process)) {
				mapType = MapType.partMap;
			}
			// 判断是否存在该面板
			flag = JecnDesignerCommon.isOpen(treeNode.getJecnTreeBean().getId().intValue(), mapType);
			if (flag) {
				return true;
			}
			// 如果存在子节点执行递归
			if (!treeNode.isLeaf()) { // 非叶子节点的情况下进行递归
				flag = setFolderChildHideValue(treeNode);
			}
		}
		return flag;
	}

	private void removeEditItemAction() {
		try {
			ConnectionPool.getProcessAction().moveEdit(selectNode.getJecnTreeBean().getId(), JecnConstants.getUserId());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("moveEditSuccess"));
		} catch (Exception e) {
			log.error("removeEditItemAction is error", e);
		}
	}

	/**
	 * 重置编号
	 */
	private void reNumItemItemAction() {
		JecnFlowStructureT jecnFlowStructureT;
		try {
			// 查询最新的流程架构编号
			jecnFlowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(
					selectNode.getJecnTreeBean().getId());
			if (StringUtils.isNotBlank(jecnFlowStructureT.getFlowIdInput())) {
				AutoCodeNodeData nodeData = new AutoCodeNodeData();
				nodeData.setNodeId(selectNode.getJecnTreeBean().getId());// 流程节点ID
				nodeData.setNodeCode(jecnFlowStructureT.getFlowIdInput());// 流程架构编号
				nodeData.setNodeType(0);// 节点类型
				boolean Identification = ConnectionPool.getAutoCodeAction().reNumItemItemAction(nodeData,
						JecnConstants.getUserId());
				if (Identification) {
					refreshItemAction();// 重置编号后刷新节点
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("RenumberingDone"));
				} else {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("NoSub-ProcessExisted"));
				}
			} else {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("PleaseAddNumberForNode"));
			}
		} catch (Exception e) {
			log.error("", e);
		}
	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		sysNcFlowItem.setVisible(false);// 流程同步
		desuetudeItem.setVisible(false);
		if (JecnTreeCommon.isAuthNodes(selectNodes)) {
			boolean isFlowMap = selectNodes.get(0).getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.processMap);

			// 只允许管理员编辑流程架构时，设计者菜单控制
			if (isFlowMap && !JecnDesignerCommon.isAdmin() && JecnConfigTool.isAdminEditFlowMapNode()) {
				return;
			}
			// 删除
			deleteItem.setVisible(true);
			// 移动
			nodeMoveItem.setVisible(JecnConfigTool.typeAllSame(selectNodes));
			if (JecnDesignerCommon.isAuthorAdmin() || JecnDesignerCommon.isProcessAdmin()) {
				batchReleaseNotRecord.setVisible(true);
				batchCancelRelease.setVisible(true);
				boolean batchReleaseNotRecordEnable = true;
				boolean batchCancelReleaseEnable = true;
				// 批量发布
				for (JecnTreeNode node : selectNodes) {
					JecnTreeNode p = (JecnTreeNode) node.getParent();
					if (p != null && p.getJecnTreeBean().getId() != 0 && !p.getJecnTreeBean().isPub()) {
						batchReleaseNotRecordEnable = false;
						break;
					}
				}
				// 撤销发布
				for (JecnTreeNode node : selectNodes) {
					if (!node.getJecnTreeBean().isPub()) {
						batchCancelReleaseEnable = false;
						break;
					}
				}
				batchReleaseNotRecord.setEnabled(batchReleaseNotRecordEnable);
				batchCancelRelease.setEnabled(batchCancelReleaseEnable);

			}

			// 操作说明下载提示
			downItem.setToolTipText(JecnProperties.getValue("flowMapDownLoadPlace"));
			downFileItem.setToolTipText(JecnProperties.getValue("flowMapDownLoadPlace"));
			// 改变下载Item文本
			downItem.setText(JecnProperties.getValue("downloadOpDes"));
			downFileItem.setText(JecnProperties.getValue("downloadOpDesFile"));
			// 下载操作说明
			downItem.setVisible(true);
			// 多选，将操作说明下载带附件隐藏掉
			if (!selectNodes.get(0).getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.processMap)) {
				// 下载操作说明(带附件)
				downFileItem.setVisible(true);
			}
		}
	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		// JecnTreeCommon.autoExpandNode(jTree, selectNode);
		// refreshItemAction();
		init();
		int authType;
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case processRoot:
			// 刷新
			refreshItem.setVisible(true);
			designAccessMenu.setVisible(true);
			if (JecnDesignerCommon.isAdmin()) {
				separatorOne.setVisible(true);
				// 新建流程地图
				newFlowMapItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);
				// 新建流程图
				newFlowItem.setVisible(true);
				// 新建流程(文件)
				newFlowFileItem.setVisible(true);
				newAerialMapItem.setVisible(true);
				copyFlowsItem.setVisible(false);
			}
			break;
		case processMap:
			// 刷新
			reNumItem.setVisible(true);
			refreshItem.setVisible(true);
			authType = JecnTreeCommon.isAuthNode(selectNode);
			if ((selectNode.getLevel() == 2 || selectNode.getLevel() == 3) && (authType == 1 || authType == 2)) {
				settingProcessFileInfo.setVisible(true);
			}
			// 只允许管理员编辑流程架构时，设计者菜单控制
			if (!JecnDesignerCommon.isAdmin() && JecnConfigTool.isAdminEditFlowMapNode()) {
				if (authType == 1 || authType == 2) {
					// 节点排序
					nodeSortItem.setVisible(true);
					// 新建流程
					newFlowItem.setVisible(true);
					newFlowFileItem.setVisible(true);
					if (JecnConfigTool.isShowProcessMapMenuTaskEdit()) {// true：显示流程任务设置
						processTaskSet.setVisible(true);
					}
				}
				return;
			}
			if (authType == 1 || authType == 2) {
				copyFlowsItem.setVisible(true);
				separatorOne.setVisible(true);
				// 新建流程地图
				newFlowMapItem.setVisible(true);
				// 维护支持文件
				architectureUploadFileMenu.setVisible(true);
				// 相关文件
				relFileItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
						.isDesignProcessAdmin()));
				// 新建流程图
				newFlowItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);
				// 查阅权限(部门)
				lookItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
						.isDesignProcessAdmin()));
				if (JecnConfigTool.isShowDes()) {
					// 流程地图描述
					flowMapDesItem.setVisible(true);
				}
				mapTaskSet.setVisible(true);

				if (JecnConfigTool.isShowProcessMapMenuTaskEdit()) {// true：显示流程任务设置
					processTaskSet.setVisible(true);
				}
				// 提交审批
				submitAppItem.setVisible(true);
				desuetudeItem.setVisible(true);
				reSubmitAppItem.setVisible(true);

				if (JecnTreeCommon.isPublic(selectNode)) {
					// 提交审批
					submitAppItem.setEnabled(true);
					desuetudeItem.setEnabled(true);
				} else {
					// 提交审批
					submitAppItem.setEnabled(false);
					desuetudeItem.setEnabled(false);
				}
				if (JecnTool.reSubmitToTask(selectNode.getJecnTreeBean())) {
					reSubmitAppItem.setEnabled(true);
				} else {
					reSubmitAppItem.setEnabled(false);
				}
				// 文控信息
				historyItem.setVisible(true);
				// 新建流程(文件)
				newFlowFileItem.setVisible(true);

				if (JecnDesignerCommon.isAuthorAdmin() || JecnConstants.loginBean.isDesignProcessAdmin()) {
					separatorFour.setVisible(true);
					// 发布
					releaseItem.setVisible(true);
					cancelReleaseItem.setVisible(true);
					// 撤销发布
					if (selectNode.getJecnTreeBean().isPub()) {
						cancelReleaseItem.setEnabled(true);
					} else {
						cancelReleaseItem.setEnabled(false);
					}
					// 直接发布-不记录版本
					directReleaseItem.setVisible(true);
					// 解除编辑
					removeEditItem.setVisible(true);
					if (JecnTreeCommon.isPublic(selectNode)) {
						// 发布
						releaseItem.setEnabled(true);
						// 直接发布-不记录版本
						directReleaseItem.setEnabled(true);
					} else {
						// 直接发布-不记录版本
						directReleaseItem.setEnabled(false);
						// 发布
						releaseItem.setEnabled(false);
					}
					if (selectNode.getJecnTreeBean().getNodeType() == 2) {
						// 部门鸟瞰图菜单处理
						deptAerialMapMenuHandel();
					}

					// checkout.setVisible(selectNode.getJecnTreeBean().getTreeNodeType()
					// == TreeNodeType.processMap
					// && JecnConfigTool.checkoutVisible());
				}
				if (authType == 2) {
					separatorTwo.setVisible(true);
					separatorFour.setVisible(true);
					// 节点移动
					nodeMoveItem.setVisible(true);
					// 重命名
					renameItem.setVisible(JecnDesignerCommon.isEditByTaskNode(selectNode, JecnConstants.loginBean
							.isDesignProcessAdmin()));
					// 删除
					deleteItem.setVisible(true);
					if (selectNode.getJecnTreeBean().isPub() && selectNode.getJecnTreeBean().getApproveType() == -1) {
						pubNoteItem.setVisible(true);
					}
				}
				// 权限判断，是否允许操作删除菜单
				if (!deleteMenuVisible()) {
					deleteItem.setVisible(false);
				}

				if (selectNode.getJecnTreeBean().getApproveType() == 0) {
					this.deleteItem.setVisible(false);
					this.releaseItem.setVisible(false);
					this.directReleaseItem.setVisible(false);
					// 提交审批
					submitAppItem.setVisible(false);
					desuetudeItem.setVisible(false);
					cancelReleaseItem.setVisible(false);
				}
				JecnUtil.setMenuDisableThenInTaskExceptAuthorAdmin(selectNode, renameItem, architectureUploadFileMenu,
						transfromToProcessMenu, nodeSortItem, autoCodeMenu, nodeMoveItem, relFileItem,
						createAPQCMapItem);
			}
			break;
		case process:
		case processFile:
			// 刷新
			refreshItem.setVisible(true);
			// 流程同步,已经发布的可以推送
			if (selectNode.getJecnTreeBean().isPub()) {
				sysNcFlowItem.setVisible(true);
			}
			authType = JecnTreeCommon.isAuthNode(selectNode);
			if (authType == 1 || authType == 2) {
				if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processFile) {
					transToProcessItem.setVisible(true);
					versionFile.setVisible(true);
				}
				copyFlowsItem.setVisible(true);
				autoCodeMenu.setVisible(true);
				// 新建流程图
				newFlowItem.setVisible(true);
				// 维护支撑文件
				architectureUploadFileMenu.setVisible(true);
				// 改变下载Item文本
				changeDownItemText(selectNode.getJecnTreeBean().getTreeNodeType());
				// 下载操作说明
				downItem.setVisible(true);
				// 下载操作说明(带附件)
				downFileItem.setVisible(true);
				// 提交审批
				submitAppItem.setVisible(true);
				desuetudeItem.setVisible(true);
				reSubmitAppItem.setVisible(true);
				processTaskSet.setVisible(true);
				if (JecnTool.reSubmitToTask(selectNode.getJecnTreeBean())) {
					reSubmitAppItem.setEnabled(true);
					// 提交审批
					submitAppItem.setEnabled(false);
				} else {
					if (JecnTreeCommon.isPublic(selectNode)) {
						// 提交审批
						submitAppItem.setEnabled(true);
						desuetudeItem.setEnabled(true);
					} else {
						// 提交审批
						submitAppItem.setEnabled(false);
						desuetudeItem.setEnabled(false);
					}
					reSubmitAppItem.setEnabled(false);
				}
				historyItem.setVisible(true);

				if (JecnDesignerCommon.isAuthorAdmin() || JecnConstants.loginBean.isDesignProcessAdmin()) {
					separatorFour.setVisible(true);
					// 撤销发布
					cancelReleaseItem.setVisible(true);
					// 撤销发布
					if (selectNode.getJecnTreeBean().isPub()) {
						cancelReleaseItem.setEnabled(true);
					} else {
						cancelReleaseItem.setEnabled(false);
					}
					// 直接发布--不记录版本
					directReleaseItem.setVisible(true);
					// 发布
					releaseItem.setVisible(true);
					// 解除编辑
					removeEditItem.setVisible(true);
					if (JecnTreeCommon.isPublic(selectNode)) {
						// 发布
						releaseItem.setEnabled(true);
						// 直接发布-不记录版本
						directReleaseItem.setEnabled(true);
					} else {
						// 直接发布-不记录版本
						directReleaseItem.setEnabled(false);
						// 发布
						releaseItem.setEnabled(false);
					}

					// flowCheckout.setVisible(selectNode.getJecnTreeBean().getTreeNodeType()
					// == TreeNodeType.process
					// && JecnConfigTool.checkoutVisible());
					// roleCheckout.setVisible(selectNode.getJecnTreeBean().getTreeNodeType()
					// == TreeNodeType.process
					// && JecnConfigTool.checkoutVisible());
				}
				if (authType == 2) {
					separatorTwo.setVisible(true);
					separatorFour.setVisible(true);
					// 重命名
					renameItem.setVisible(true);
					// 节点排序
					nodeSortItem.setVisible(true);
					// 节点移动
					nodeMoveItem.setVisible(true);
					// 删除
					deleteItem.setVisible(true);
					newFlowFileItem.setVisible(true);
				}

				// 权限判断，是否允许操作删除菜单
				if (!deleteMenuVisible()) {
					deleteItem.setVisible(false);
				}

				if (isProcessFile()) {
					newFlowItem.setVisible(false);
					// newFlowFileItem.setVisible(false);
					separatorOne.setVisible(false);
					transfromToProcessMenu.setVisible(true);
				} else {
					separatorOne.setVisible(true);
				}

				if (selectNode.getJecnTreeBean().getApproveType() == 0) {
					this.deleteItem.setVisible(false);
					this.releaseItem.setVisible(false);
					this.directReleaseItem.setVisible(false);
					// 提交审批
					submitAppItem.setVisible(false);
					desuetudeItem.setVisible(false);
					cancelReleaseItem.setVisible(false);
					transfromToProcessMenu.setVisible(false);
				}
				if (selectNode.getJecnTreeBean().isPub() && selectNode.getJecnTreeBean().getApproveType() == -1) {
					pubNoteItem.setVisible(true);
				}
				JecnUtil.setMenuDisableThenInTaskExceptAuthorAdmin(selectNode, renameItem, architectureUploadFileMenu,
						transfromToProcessMenu, nodeSortItem, autoCodeMenu, nodeMoveItem, relFileItem);
			}
			break;
		default:
			break;
		}
		JecnUtil.setMenuDisableJiuxin(selectNode, releaseItem, cancelReleaseItem, directReleaseItem, submitAppItem,
				desuetudeItem, mapTaskSet, processTaskSet, batchCancelRelease, batchReleaseNotRecord);
	}

	/**
	 * 部门鸟瞰图右键菜单
	 */
	private void deptAerialMapMenuHandel() {
		newAerialMapItem.setVisible(true);
		newFlowMapItem.setVisible(false);
		newFlowItem.setVisible(false);
		newFlowFileItem.setVisible(false);
		processTaskSet.setVisible(false);
		lookItem.setVisible(false);
		flowFileDownItem.setVisible(false);
		// relFileItem.setVisible(false);
		architectureUploadFileMenu.setVisible(false);
		pubNoteItem.setVisible(false);
		mnInitItem.setVisible(false);
	}

	/**
	 * 是否可以执行删除操作
	 * 
	 * @return
	 */
	private boolean deleteMenuVisible() {
		if (!(JecnDesignerCommon.isAuthorAdmin() || JecnConstants.loginBean.isDesignProcessAdmin())
				&& selectNode.getJecnTreeBean().isPub()) {// 非管理员且非发布的文件，可有删除权限
			return false;
		}
		return true;
	}

	/**
	 * 根据节点类型改变下载Item的文本
	 * 
	 * @author weidp
	 * @date 2014-10-31 下午04:03:28
	 * @param treeNodeType
	 */
	private void changeDownItemText(TreeNodeType treeNodeType) {
		if (treeNodeType.equals(TreeNodeType.processMap)) {
			downItem.setText(JecnProperties.getValue("downloadOpMapDesI"));
		} else {
			downItem.setText(JecnProperties.getValue("downloadOpDes"));
			downFileItem.setText(JecnProperties.getValue("downloadOpDesFile"));
		}
	}

	/**
	 * 初始化
	 */
	private void init() {
		transToProcessItem.setVisible(false);
		// 新建流程地图
		newFlowMapItem.setVisible(false);
		settingProcessFileInfo.setVisible(false);
		// 新建流程图
		newFlowItem.setVisible(false);
		// 流程同步
		sysNcFlowItem.setVisible(false);
		// 关联制度
		relFileItem.setVisible(false);
		// 搜索
		searchItem.setVisible(false);
		// 节点排序
		nodeSortItem.setVisible(false);
		// 节点移动
		nodeMoveItem.setVisible(false);
		// 刷新
		refreshItem.setVisible(false);
		// 发布
		releaseItem.setVisible(false);

		cancelReleaseItem.setVisible(false);
		directReleaseItem.setVisible(false);
		// 提交审批
		submitAppItem.setVisible(false);
		desuetudeItem.setVisible(false);
		// 查阅权限(部门)
		lookItem.setVisible(false);
		// 查阅权限(岗位)
		// positionLookItem.setVisible(false);
		// 重命名
		renameItem.setVisible(false);
		// 流程地图描述
		flowMapDesItem.setVisible(false);
		// 删除
		deleteItem.setVisible(false);
		downItem.setVisible(false);
		downFileItem.setVisible(false);
		// 解除编辑
		removeEditItem.setVisible(false);
		historyItem.setVisible(false);
		// 重置编号
		reNumItem.setVisible(false);

		separatorOne.setVisible(false);
		separatorTwo.setVisible(false);
		separatorThree.setVisible(false);
		separatorFour.setVisible(false);
		designAccessMenu.setVisible(false);
		batchReleaseNotRecord.setVisible(false);
		batchCancelRelease.setVisible(false);
		autoCodeMenu.setVisible(false);

		processTaskSet.setVisible(false);
		mapTaskSet.setVisible(false);
		architectureUploadFileMenu.setVisible(false);
		newFlowFileItem.setVisible(false);
		transfromToProcessMenu.setVisible(false);
		reSubmitAppItem.setVisible(false);
		newAerialMapItem.setVisible(false);
		pubNoteItem.setVisible(false);
		copyFlowsItem.setVisible(false);
		flowCheckout.setVisible(false);
		roleCheckout.setVisible(false);
		mnInitItem.setVisible(false);
		versionFile.setVisible(false);
	}

	/**
	 * 
	 * @ClassName: SysNcTask
	 * @Description: 流程推送面板控制线程
	 * @author cheshaowei
	 * @date 2015-4-13 下午03:32:39
	 */
	class SysNcTask extends SwingWorker<Void, Void> {
		@Override
		protected Void doInBackground() throws Exception {
			int syncType = 1;
			if (JecnConstants.loginBean.otherLoginType == 20) {
				syncType = 2;
			}
			try {
				if (syncType == 1) {
					FlowResourceTreeMenu.this.syncLoadFilexml(syncType);
				} else if (syncType == 2) {
					FlowResourceTreeMenu.this.sysNcFlowProcess(syncType, null);
				}
			} catch (Exception e) {
				log.error("SysNcTask is error", e);
			} finally {
				done();
			}
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	/**
	 * 星汉、普元 流程推送，下载xml到本地
	 * 
	 * @param syncType
	 * @throws Exception
	 */
	private void syncLoadFilexml(int syncType) throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		File file = new File(selectNode.getJecnTreeBean().getName());
		// 路径选择框
		JFileChooser fileChooser = new JFileChooser();
		// 移除系统给定的文件过滤器
		fileChooser.setAcceptAllFileFilterUsed(false);
		// 设置标题
		fileChooser.setDialogTitle(JecnProperties.getValue("pleaseChooseThePath"));
		// 设置选中文件
		fileChooser.setSelectedFile(file);
		fileChooser.setFileFilter(new ImportFlowFilter("*"));
		int i = fileChooser.showSaveDialog(null);
		// 选择确认（yes、ok）后返回该值。
		if (i == JFileChooser.APPROVE_OPTION) {
			// 获取输出文件的路径
			String fileDirectory = fileChooser.getSelectedFile().getPath();
			sysNcFlowProcess(syncType, fileDirectory);
		}
	}

	private void sysNcFlowProcess(int syncType, String fileDirectory) throws Exception {
		int ret = ConnectionPool.getProcessAction().sysNcFlowProcess(selectNode.getJecnTreeBean().getId(), syncType,
				fileDirectory);
		if (ret == 1) { // 同步成功
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("processSyncSucess"));
		} else if (ret == 0) {// 同步失败
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("processSysncFailure"));
		} else if (ret == 2) { // 没有开始活动，并且活动编号不是number类型
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("isExitStart"));
		} else if (ret == 3) {
			/*
			 * 没有结束活动
			 */
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("NoEndingActivity"));
		} else if (ret == 4) {
			/*
			 * 进入协作框的线只能有一条
			 */
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("cooperatingFramePrompt"));
		} else if (ret == 5) {
			/*
			 * 协作框出去的线只能有一条
			 */
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("cooperatingFramePromptOut"));
		}
	}

	/**
	 * 是否启用流程推送
	 * 
	 * @return
	 */
	private boolean isProcessPush() {
		if (JecnConstants.loginBean.otherLoginType == 11 || JecnConstants.loginBean.otherLoginType == 22
				|| JecnConstants.loginBean.otherLoginType == 20) { // 29所和中石化石勘院
			return true;
		}
		return false;
	}

	/**
	 * 安码BPM流程推送
	 * 
	 * @return
	 */
	private boolean isUltimus() {
		return JecnConstants.loginBean.otherLoginType == 22 ? true : false;
	}

	/**
	 * 流程推送
	 */
	private void processPushSync() {
		if (isUltimus()) {
			processPushUltimus();
		} else {
			processPushSystem();
		}
	}

	/**
	 * 安码BPM集成
	 */
	private void processPushUltimus() {
		JecnTreeBean treeBean = selectNode.getJecnTreeBean();
		try {
			JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(
					treeBean.getId());
			// 0:插入成功，-1，插入失败；1：已存在当前版本，不允许重复推送
			int result = ConnectionPool.getProcessAction().processPushUltimus(treeBean.getId(), treeBean.getName(),
					flowStructureT.getFlowIdInput());
			if (result == 1) {
				/*
				 * 当前版本已推送
				 */
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("currentVersionPush"));
			} else if (result == -1) {
				/*
				 * 流程推送失败
				 */
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("processPushFail"));
			} else {
				/*
				 * 流程推送成功
				 */
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("processPushSuccess"));
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, "流程推送失败！");
			log.error("processPushUltimus is error！ flowId = " + treeBean.getId(), e);
		}
	}

	/**
	 * 流程推送，到其它流程系统（星汉，普元，石化i3系统等）
	 */
	private void processPushSystem() {
		SysNcTask sysNcTask = new SysNcTask();
		JecnLoading.start(sysNcTask);
	}

	private void batchReleaseNotRecordAction() {
		// // 是否处于任务审批中
		for (JecnTreeNode selectNode : selectNodes) {
			if (JecnTool.isBeInTaskWithTipName(selectNode)) {
				return;
			}
		}

		// 提示是否发布
		int option = JecnOptionPane
				.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("isnotRemberPublish"),
						null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			List<Long> ids = new ArrayList<Long>();
			for (JecnTreeNode selectNode : selectNodes) {
				ids.add(selectNode.getJecnTreeBean().getId());
			}
			ConnectionPool.getProcessAction().batchReleaseNotRecord(ids,
					selectNodes.get(0).getJecnTreeBean().getTreeNodeType(), JecnUtil.getUserId());
			for (JecnTreeNode selectNode : selectNodes) {
				selectNode.getJecnTreeBean().setPub(true);
				selectNode.getJecnTreeBean().setUpdate(false);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
			}

			// 发布成功
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("publicSucees"));
		} catch (Exception e1) {
			log.error("batchReleaseNotRecordAction is error", e1);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}

	}

	class BatchPubTask extends SwingWorker<String, Void> {
		@Override
		protected String doInBackground() throws Exception {
			String s = null;
			batchReleaseNotRecordAction();
			return s;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	private void batchCancelReleaseAction() {
		// 是否处于任务审批中
		for (JecnTreeNode selectNode : selectNodes) {
			if (JecnTool.isBeInTaskWithTipName(selectNode)) {
				return;
			}
		}

		// 提示是否撤销发布
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("whetherRevokeRelease"), null, JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			List<Long> ids = new ArrayList<Long>();
			for (JecnTreeNode selectNode : selectNodes) {
				ids.add(selectNode.getJecnTreeBean().getId());
			}
			ConnectionPool.getProcessAction().batchCancelRelease(ids,
					selectNodes.get(0).getJecnTreeBean().getTreeNodeType(), JecnUtil.getUserId());
			for (JecnTreeNode selectNode : selectNodes) {
				selectNode.getJecnTreeBean().setPub(false);
				((DefaultTreeModel) jTree.getModel()).reload(selectNode);
			}

			// 发布成功
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("whetherReleaseSucess"));
		} catch (Exception e1) {
			log.error("batchCancelReleaseAction is error", e1);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}

	}

	private boolean isProcessFile() {
		if (selectNodes != null) {
			return selectNodes.get(0).getJecnTreeBean().getTreeNodeType() == TreeNodeType.processFile;
		}
		return selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processFile;
	}

	private boolean isProcess() {
		if (selectNodes != null) {
			return selectNodes.get(0).getJecnTreeBean().getTreeNodeType() == TreeNodeType.process;
		}
		return selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.process;
	}
}
