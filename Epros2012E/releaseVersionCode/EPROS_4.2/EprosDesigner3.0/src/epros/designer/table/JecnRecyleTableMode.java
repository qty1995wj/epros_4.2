package epros.designer.table;

import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
/**
 * 回收站TABLE的MODE
 * @author user
 *
 */
public class JecnRecyleTableMode extends DefaultTableModel{
	public JecnRecyleTableMode(Object[][] data, Object[] title) {
		super(data, title);
	}

	@Override
	public Class getColumnClass(int columnIndex) {
		if (columnIndex == 2) {
			return ImageIcon.class;
		} else {
			return super.getColumnClass(columnIndex);
		}
	}

	public boolean isCellEditable(int rowindex, int colindex) {
		if (colindex == 0) {
			return true;
		}
		return false;
	}
}
