package com.jecn.epros.server.service.task.app.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.google.gson.JsonObject;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.external.TaskParam;
import com.jecn.epros.bean.external.TaskPeople;
import com.jecn.epros.bean.external.TaskState;
import com.jecn.epros.server.action.web.login.ad.iflytek.JecnIflytekSyncUserEnum;
import com.jecn.epros.server.action.web.task.TaskIllegalArgumentException;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnAbolishBean;
import com.jecn.epros.server.bean.task.JecnTaskApplicationNew;
import com.jecn.epros.server.bean.task.JecnTaskApprovePeopleConn;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskForRecodeNew;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.task.JecnTaskPeopleNew;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;
import com.jecn.epros.server.bean.task.temp.JecnTaskFileTemporary;
import com.jecn.epros.server.bean.task.temp.JecnTempAccessBean;
import com.jecn.epros.server.bean.task.temp.SimpleSubmitMessage;
import com.jecn.epros.server.bean.task.temp.SimpleTaskBean;
import com.jecn.epros.server.bean.task.temp.TempAuditPeopleBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.connector.JecnSendTaskTOMainSystemFactory;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IOrgAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.popedom.IPositionAccessPermissionsDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.process.IProcessKPIDao;
import com.jecn.epros.server.dao.process.IProcessRecordDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.dao.task.IJecnAbstractTaskDao;
import com.jecn.epros.server.dao.task.design.IJecnTaskRecordDao;
import com.jecn.epros.server.download.word.ProcessDownDataUtil;
import com.jecn.epros.server.email.buss.TaskEmailParams;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/**
 * 任务审批，业务处理通用类
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Nov 27, 2012 时间：11:06:19 AM
 */
public class JecnCommonTaskService {

	protected Logger log = Logger.getLogger(JecnCommonTaskService.class);
	/** 任务DAO数据通用类 */
	protected IJecnAbstractTaskDao abstractTaskDao;
	/** 部门查阅权限处理DAO接口 */
	protected IOrgAccessPermissionsDao orgAccessPermissionsDao;
	/** 岗位查阅权限处理DAO接口 */
	protected IPositionAccessPermissionsDao positionAccessPermissionsDao;
	/** 人员对象DAO处理类 */
	protected IPersonDao personDao;
	/** 配置信息表（JECN_CONFIG_ITEM）操作类 */
	protected IJecnConfigItemDao configItemDao;
	/** 任务审批信息验证处理类数据层接口 */
	protected IJecnTaskRecordDao taskRecordDao;
	/** 文件DAO */
	protected IFileDao fileDao;
	/** 流程数据对象DAO */
	protected IFlowStructureDao structureDao;
	/** 流程属性对象DAO */
	protected IProcessBasicInfoDao basicInfoDao;
	/** 制度数据对象处理DAO */
	protected IRuleDao ruleDao;
	/** 文控信息处理类 */
	protected IJecnDocControlDao docControlDao;
	/** 流程KPI */
	protected IProcessKPIDao processKPIDao;
	/** 流程记录保存 */
	protected IProcessRecordDao processRecordDao;
	/** 必填項选择表 */
	protected JecnTaskApplicationNew jecnTaskApplicationNew = null;
	/** 分隔符 */
	private String STR_SPLIT = "";
	@Resource(name = "FlowStructureServiceImpl")
	private IFlowStructureService flowStructureService;

	/**
	 * 获取任务信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param taskOperation
	 *            我的任务点击操作 0审批;1重新提交;2整理意见;3任务管理，编辑任务
	 * @throws Exception
	 */
	protected SimpleTaskBean getSimpleTaskBeanById(Long taskId, int taskOperation) throws Exception {
		if (taskId == null) {
			log.error("JecnAbstractTaskServiceImpl类getSimpleTaskBeanById方法中参数为空!");
			throw new TaskIllegalArgumentException("参数不能为空!", -1);
		}
		// 任务相关信息
		SimpleTaskBean simpleTaskBean = new SimpleTaskBean();

		// 获取任务信息
		JecnTaskBeanNew taskBeanNew = getJecnTaskBeanNew(taskId);

		// 获取任务下的所有阶段信息
		List<JecnTaskStage> jecnTaskStageList = abstractTaskDao.getJecnTaskStageList(taskId);

		// 如果任务当前是完成阶段
		if (taskBeanNew.getState() == 5) {
			simpleTaskBean.setStageName(JecnTaskCommon.FINISH);
		} else {
			// 获取当前任务阶段状态名称
			JecnTaskStage curTaskStage = JecnTaskCommon.getCurTaskStage(jecnTaskStageList, taskBeanNew.getState());
			simpleTaskBean.setStageName(curTaskStage.getStageName());
		}

		// 获取任务显示项
		JecnTaskApplicationNew jecnTaskApplicationNew = getJecnTaskApplicationNew(taskId);
		simpleTaskBean.setJecnTaskApplicationNew(jecnTaskApplicationNew);

		// 创建人信息
		JecnUser jecnUser = getJecnUser(taskBeanNew.getCreatePersonId());
		// 开始时间转型
		taskBeanNew.setStartTimeStr(JecnCommon.getStringbyDate(taskBeanNew.getStartTime()));
		// 结束时间转型
		taskBeanNew.setEndTimeStr(JecnCommon.getStringbyDate(taskBeanNew.getEndTime()));
		if (jecnUser != null) {
			if (jecnUser.getIsLock() == 1) {
				// jecnUser.getTrueName() + (已离职)
				jecnUser.setTrueName(jecnUser.getTrueName() + JecnTaskCommon.LEAVE_OFFICE);
			}
			// 真实姓名
			taskBeanNew.setCreatePersonTemporaryName(jecnUser.getTrueName());
			// 电话
			simpleTaskBean.setPhone(jecnUser.getPhoneStr());
		}

		// 获取查阅权限
		accessPermissions(taskBeanNew.getRid(), taskBeanNew.getTaskType(), simpleTaskBean);
		// 设置任务信息
		simpleTaskBean.setJecnTaskBeanNew(taskBeanNew);
		// 0为流程任务，1为文件任务，2为制度任务
		switch (taskBeanNew.getTaskType()) {
		case 0:
		case 4:// 获取流程信息
			getFlowInfomation(simpleTaskBean);
			break;
		case 1:// 获取文件信息
			getFileInfomation(simpleTaskBean);
			break;
		case 2:// 制度
		case 3:// 获取制度
			getRuleInfomation(simpleTaskBean);
			break;
		default:
			break;
		}

		// 试运行报告
		if (taskBeanNew.getFlowType() != null && taskBeanNew.getFlowType() != 0) {
			// 获取试运行报告
			Object[] taskRunFile = getTaskRunFileName(taskId);
			if (taskRunFile != null) {
				// 试运行主键ID
				simpleTaskBean.setTaskRunFileId(taskRunFile[0].toString());
				// 试运行文件名称
				simpleTaskBean.setTaskRunFileName(taskRunFile[1].toString());
			}
		}

		// 任务说明换行显示
		if (taskBeanNew.getTaskDesc() != null && !"".equals(taskBeanNew.getTaskDesc())) {
			// 重新提交显示任务说明
			simpleTaskBean.setTaskDescStr(JecnTaskCommon.rebackReplaceString(taskBeanNew.getTaskDesc()));
		}

		// 获取页面中显示所需的Stage信息
		if (jecnTaskApplicationNew.getIsControlAuditLead() != 0) {
			// 是否处于文控之前（当前阶段为文控）
			boolean isStateBeforeDocControl = JecnTaskCommon.isStateBeforeDocControl(taskBeanNew.getState(),
					jecnTaskStageList);
			// 如果是在文控选人之前，去掉处于文控后面的Stage
			if ((taskOperation == 3 && isStateBeforeDocControl)
					|| (taskBeanNew.getState() != 1 && isStateBeforeDocControl)) {
				JecnTaskStage docControlTaskStage = JecnTaskCommon.getCurTaskStage(jecnTaskStageList, 1);
				for (int i = jecnTaskStageList.size() - 1; i >= 0; i--) {
					if (jecnTaskStageList.get(i).getSort() > docControlTaskStage.getSort()) {
						jecnTaskStageList.remove(i);
					}
				}
			}
		}

		// 获取页面显示所需的Bean
		getListAuditPeopleBean(taskBeanNew, jecnTaskStageList, simpleTaskBean);

		// 任务State集合
		List<Integer> orderList = getOrderList(jecnTaskStageList);

		// 打回整理验证 审批人必须存在评审人会审阶段后
		boolean isCallBack = JecnTaskCommon.isCallBack(orderList, taskBeanNew.getState(), simpleTaskBean
				.getListAuditPeopleBean());
		if (isCallBack) {// 存在打回整理状态 说明评审阶段已审批完成
			simpleTaskBean.setCallBack("1");// 打回整理状态
		} else {
			simpleTaskBean.setCallBack("0");
		}
		// 获取任务审批流转记录集合 需要所有的taskStage
		// 在文控主导审批时该集合是经过处理过的不完整的集合。（因为将文控之后的阶段都删除了）所以需要重新查询。
		jecnTaskStageList = abstractTaskDao.getJecnTaskStageList(taskId);
		// 获取任务审批流转记录集合
		List<JecnTaskForRecodeNew> listJecnTaskForRecodeNew = this.findJecnTaskForRecodeNewByTaskId(taskId, taskBeanNew
				.getTaskType(), jecnTaskStageList);
		simpleTaskBean.setListJecnTaskForRecodeNew(listJecnTaskForRecodeNew);

		for (TempAuditPeopleBean auditPeople : simpleTaskBean.getListAuditPeopleBean()) {
			if (taskBeanNew.getRevirewCounts() != 2 && taskBeanNew.getState() == 10 && auditPeople.getState() == 3) {// 记录评审人
				// 二次评审默认显示
				// 评审人IDs
				simpleTaskBean.setReviewIds(auditPeople.getAuditId());
				simpleTaskBean.setReviewNames(auditPeople.getAuditName());
			}
		}

		// 是否存在返回
		boolean isAppMesTrue = JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.taskOperationReBack.toString(),
				JecnTaskCommon.findBackItemBean(5, configItemDao));
		if (isAppMesTrue) {
			simpleTaskBean.getTaskOperationBean().setReBack(1);
		}
		simpleTaskBean.setShowOperationAssigned(JecnConfigTool.isShowOperationAssigned());
		simpleTaskBean.setShowOperationTransfer(JecnConfigTool.isShowOperationTransfer());
		return simpleTaskBean;
	}

	/**
	 * 审批通过
	 * 
	 * @param submitMessage
	 *            审核人提交信息
	 * 
	 */
	protected void taskOperation(SimpleSubmitMessage submitMessage) throws Exception {
		if (JecnCommon.isNullOrEmtryTrim(submitMessage.getTaskId())
				|| JecnCommon.isNullOrEmtryTrim(submitMessage.getTaskOperation())
				|| (!"11".equals(submitMessage.getTaskOperation()) && !"12".equals(submitMessage.getTaskOperation()) && JecnCommon
						.isNullOrEmtryTrim(submitMessage.getOpinion()))
				|| JecnCommon.isNullOrEmtryTrim(submitMessage.getCurPeopleId())) {
			log.error("任务提交审批时异常 方法taskOperation！" + "任务主键ID=" + submitMessage.getTaskId() + "taskOperation="
					+ submitMessage.getTaskOperation() + "option=" + submitMessage.getOpinion() + "任务审批操作人="
					+ submitMessage.getCurPeopleId());
			throw new TaskIllegalArgumentException("任务提交审批时异常 方法taskOperation！", -1);
		}
		validateCanApprove(submitMessage);

		// 任务主键ID
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		// 任务操作
		int taskElseState = Integer.valueOf(submitMessage.getTaskOperation()).intValue();
		// 获取任务主表信息
		JecnTaskBeanNew taskBeanNew = getJecnTaskBeanNew(taskId);
		if (taskBeanNew == null) {
			log.error("获取任务信息异常!");
			throw new TaskIllegalArgumentException("获取任务信息异常!", -1);
		}
		if (taskBeanNew.getState() == 5) {
			return;
		}

		if (taskElseState != 11 && taskElseState != 12) {
			if (taskBeanNew.getState() == 0 || taskBeanNew.getState() == 10) {// 拟稿人提交审批
				JecnUser jecnUser = getJecnUser(taskBeanNew.getCreatePersonId());
				if (jecnUser == null || jecnUser.getIsLock() == 1) {// 拟稿人离职
					throw new TaskIllegalArgumentException(JecnTaskCommon.STAFF_CHANGE, -1);
				}
			}
		}
		// 任务阶段信息
		List<JecnTaskStage> taskStageList = this.abstractTaskDao.getJecnTaskStageList(taskId);
		// 文控审核人主导审批
		if (taskBeanNew.getIsControlauditLead() == 1 && taskBeanNew.getState() == 1 && taskElseState == 1) {
			controlPass(submitMessage, taskBeanNew, taskStageList);
			return;
		}
		switch (taskElseState) {
		case 0:// 拟稿人提交审批
			break;
		case 1:// 通过
			approvePass(submitMessage, taskBeanNew, taskStageList);
			break;
		case 2:// 交办
			assignedTask(submitMessage, taskElseState, taskStageList);
			break;
		case 3:// 转批
			assignedTask(submitMessage, taskElseState, taskStageList);
			break;
		case 4:// 打回
			callBackTask(submitMessage, taskStageList);
			break;
		case 5:// 提交意见（提交审批意见(评审-------->拟稿人)
			submitTaskIdea(submitMessage, taskStageList);
			break;
		case 6:// 二次评审 拟稿人------>评审
			twoReviewTask(submitMessage, taskStageList);
			break;
		case 7:// 拟稿人重新提交审批
			reSubmitTask(submitMessage, taskStageList);
			break;
		case 8:// 完成
			break;
		case 9:// 打回整理意见(任务状态在评审人后的审核阶段------>拟稿人)）
			callBackArrangement(submitMessage, taskStageList);
			break;
		case 10:// 交办人提交
			assignedPeopleSubmitTask(submitMessage, taskStageList);
			break;
		case 11:// 编辑 (系统管理员编辑任务)
		case 12:// 编辑 (系统管理员编辑任务)
			editTask(submitMessage, taskBeanNew, taskStageList);
			break;
		case 13:// 撤回
			reCallTask(Long.valueOf(submitMessage.getCurPeopleId()), taskBeanNew, taskStageList);
			break;
		case 14:// 返回
			reBackTask(submitMessage, taskBeanNew, taskStageList);
			break;
		case 15:// 交给拟稿人整理（相当于打回整理---->拟稿人）
			backCollateToDraf(submitMessage, taskBeanNew, taskStageList);
			break;
		case 16:// 拟稿人整理完交给当前阶段（拟稿人------->当前阶段审批人）
			drafBackCollateSubmit(submitMessage, taskBeanNew, taskStageList);
			break;
		}
	}

	/**
	 * 拟稿人整理提交
	 * 
	 * @param submitMessage
	 * @param taskBeanNew
	 * @param taskStageList
	 * @throws Exception
	 */
	private void drafBackCollateSubmit(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {
		// 操作人ID
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		List<Long> ids = getJecnTaskApprovePeopleConnList(taskStageList, jecnTaskBeanNew.getState());
		if (ids == null || ids.size() == 0) {
			throw new TaskIllegalArgumentException(JecnTaskCommon.TASK_APPROVE_OR_PEOPLE_LIVE, 8);
		}
		Set<Long> peopleList = new HashSet<Long>(ids);
		/** 更新任务基本信息 */
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		jecnTaskBeanNew.setTaskElseState(16);
		jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
		setTaskEdit(jecnTaskBeanNew, submitMessage);
		// 更新任务
		abstractTaskDao.save(jecnTaskBeanNew);

		// 删除任务的所有审批人
		deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);
		// 记录当前阶段名称
		setMarkName(jecnTaskBeanNew, taskStageList);
		// 获取目标人集合 并保存流程记录
		updateToPeopleAndForRecord(peopleList, jecnTaskBeanNew, submitMessage.getOpinion(), JecnTaskCommon.TASK_NOTHIN,
				curPeopleId);

		sendTaskEmail(peopleList, jecnTaskBeanNew);
	}

	private List<Long> getJecnTaskApprovePeopleConnList(List<JecnTaskStage> taskStageList, Integer state)
			throws Exception {
		List<Long> listUserId = abstractTaskDao.getJecnTaskApprovePeopleConnListByTaskIdAndState(JecnTaskCommon
				.getCurTaskStage(taskStageList, state).getId());
		return listUserId;
	}

	/**
	 * 打回整理给拟稿人
	 * 
	 * @param submitMessage
	 * @param taskBeanNew
	 * @param taskStageList
	 * @throws Exception
	 */
	private void backCollateToDraf(SimpleSubmitMessage submitMessage, JecnTaskBeanNew taskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {
		if (submitMessage == null) {
			log.error("JecnAbstractTaskBS类callBackTask方法中参数为空!");
			throw new TaskIllegalArgumentException("参数不能为空!", -1);
		}
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		/** 任务基本信息 */
		JecnTaskBeanNew jecnTaskBeanNew = getJecnTaskBeanNew(taskId);
		// 拟稿人是否存在
		checkCreateUser(jecnTaskBeanNew.getCreatePersonId());
		// 操作人ID
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		// 审批意见
		String opinion = submitMessage.getOpinion();
		/** 更新任务基本信息 */
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		jecnTaskBeanNew.setTaskElseState(15);
		jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());

		// 记录当前阶段名称
		setMarkName(jecnTaskBeanNew, taskStageList);
		setTaskEdit(jecnTaskBeanNew, submitMessage);
		// 更新任务
		abstractTaskDao.update(jecnTaskBeanNew);
		// 待删除目标人集合
		List<Long> cancelPeoples = getCancelPeoples(taskBeanNew);
		// 删除任务的所有审批人
		deleteJecnTaskPeopleNew(taskId);
		// 记录排序号
		int sort = getCurSortRecordCount(taskId) + 1;

		updateTaskPeopleNew(jecnTaskBeanNew.getCreatePersonId(), taskId, jecnTaskBeanNew, opinion, "", sort);

		// 获取人员信息，发送邮件
		Set<Long> peopleList = new HashSet<Long>();
		peopleList.add(jecnTaskBeanNew.getCreatePersonId());
		// 获取人员信息，发送邮件
		sendTaskEmail(peopleList, jecnTaskBeanNew);

		Set<Long> handlerPeopleIds = new HashSet<Long>();
		handlerPeopleIds.add(jecnTaskBeanNew.getCreatePersonId());
		sendInfoCancels(jecnTaskBeanNew, personDao, handlerPeopleIds, cancelPeoples, curPeopleId);
	}

	private void setTaskEdit(JecnTaskBeanNew jecnTaskBeanNew, SimpleSubmitMessage submitMessage) {
		jecnTaskBeanNew.setEdit(0);
		if (jecnTaskBeanNew.getState() == 0 || jecnTaskBeanNew.getState() == 10 || jecnTaskBeanNew.getState() == 5) {
			jecnTaskBeanNew.setEdit(1);
		} else if (submitMessage != null) {
			int taskElseState = Integer.valueOf(submitMessage.getTaskOperation()).intValue();
			if (taskElseState == 15) {
				jecnTaskBeanNew.setEdit(1);
			}
		}
	}

	private void validateCanApprove(SimpleSubmitMessage submitMessage) {
		try {
			if ("11".equals(submitMessage.getTaskOperation()) || "12".equals(submitMessage.getTaskOperation())) {
				return;
			}
			if ("13".equals(submitMessage.getTaskOperation())) {
				String hql = "from JecnTaskBeanNew where id=?";
				JecnTaskBeanNew task = abstractTaskDao.getObjectHql(hql, Long.valueOf(submitMessage.getTaskId()));
				if (task == null || !Long.valueOf(submitMessage.getCurPeopleId()).equals(task.getFromPeopleId())) {
					throw new TaskIllegalArgumentException(JecnTaskCommon.TASK_APPROVE_OR_PEOPLE_LIVE, 8);
				}
			} else {
				// boolean hasApproveTask =
				// hasApproveTask(Long.valueOf(submitMessage.getTaskId()), Long
				// .valueOf(submitMessage.getCurPeopleId()),
				// submitMessage.getTaskStage());
				// if (hasApproveTask) {
				// throw new
				// TaskIllegalArgumentException(JecnTaskCommon.TASK_APPROVE_OR_PEOPLE_LIVE,
				// 8);
				// }
			}
		} catch (Exception e) {
			log.error("", e);
			throw new TaskIllegalArgumentException();
		}
	}

	/**
	 * 返回操作 从当前阶段返回至上一阶段，当前阶段操作人可执行
	 * 
	 * @param submitMessage
	 *            页面提交的信息
	 * @param taskBeanNew
	 *            任务
	 * @param taskStageList
	 *            任务对应的阶段信息
	 * @throws Exception
	 */
	private void reBackTask(SimpleSubmitMessage submitMessage, JecnTaskBeanNew taskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {
		if (submitMessage == null || taskBeanNew == null) {
			throw new IllegalArgumentException("返回获取提交信息异常！");
		}
		if (JecnCommon.isNullOrEmtryTrim(submitMessage.getCurPeopleId())) {
			throw new TaskIllegalArgumentException("参数非法，操作人不能为空！", -1);
		}
		// 当前操作人
		long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());

		// 任务操作 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段
		// 7：IT总监审批阶段
		// 8:事业部经理 9：总经理 10:整理意见
		int state = taskBeanNew.getState();

		// 获取任务的前一阶段
		JecnTaskStage taskStage = JecnTaskCommon.getPreTaskStage(taskStageList, state);
		int upState = taskStage.getStageMark();

		if (state == 0 || state == 3 || state == 10) {// 0：拟稿人阶段3：评审阶段10:整理意见无返回操作
			throw new TaskIllegalArgumentException("该阶段没有返回操作，state：" + state, -1);
		}
		taskBeanNew.setFromPeopleId(curPeopleId);
		// 操作 返回
		taskBeanNew.setTaskElseState(14);

		// 目标人
		Set<Long> toPeopleId = new HashSet<Long>();
		if (upState == 0 || upState == 10) {
			toPeopleId.add(taskBeanNew.getCreatePersonId());
		} else {
			toPeopleId = getApprovePeopleIdByState(taskStage.getId());
		}
		// 删除当前目标人
		deleteJecnTaskPeopleNew(curPeopleId, taskBeanNew);

		// 上次操作状态
		taskBeanNew.setState(upState);

		// 当前操作状态
		taskBeanNew.setUpState(state);

		// 记录当前阶段名称
		setMarkName(taskBeanNew, taskStageList);

		// 记录日志
		// 获取目标人集合 并保存流程记录
		updateToPeopleAndForRecord(toPeopleId, taskBeanNew, submitMessage.getOpinion(), JecnTaskCommon.TASK_NOTHIN,
				curPeopleId);

		setTaskEdit(taskBeanNew, submitMessage);
		this.abstractTaskDao.update(taskBeanNew);
		this.abstractTaskDao.getSession().flush();

		// 获取人员信息，发送邮件
		sendTaskEmail(toPeopleId, taskBeanNew);
	}

	private void setMarkName(JecnTaskBeanNew jecnTaskBeanNew, List<JecnTaskStage> taskStageList) {
		// 设置阶段名称 用于邮件发送
		if (jecnTaskBeanNew.getState() == 5) {
			jecnTaskBeanNew.setStateMark("完成");
		} else {
			jecnTaskBeanNew.setStateMark(JecnTaskCommon.getCurTaskStage(taskStageList, jecnTaskBeanNew.getState())
					.getStageName());
		}

		jecnTaskBeanNew.setUpStateMark(JecnTaskCommon.getCurTaskStage(taskStageList, jecnTaskBeanNew.getUpState())
				.getStageName());
	}

	/**
	 * 根据任务的state获取对应的来源人
	 * 
	 * @author weidp
	 * @date： 日期：2014-6-16 时间：上午09:29:43
	 * @param taskId
	 *            任务Id
	 * @param state
	 *            任务阶段
	 * @return -1 当前阶段不存在【需管理员指派阶段审批人】
	 * @throws Exception
	 */
	private Set<Long> getApprovePeopleIdByState(long stageId) throws Exception {
		Set<Long> ids = new HashSet<Long>();
		String hql = "from JecnTaskApprovePeopleConn where stageId=?";
		List<JecnTaskApprovePeopleConn> conns = this.abstractTaskDao.listHql(hql, stageId);
		if (conns == null || conns.size() == 0) {
			return ids;
		}
		for (JecnTaskApprovePeopleConn conn : conns) {
			ids.add(conn.getApprovePid());
		}
		return ids;
	}

	/**
	 * 特殊操作处理 撤回操作相当于恢复操作
	 * 
	 * @param taskElseType
	 *            最后一次提交的状态
	 * @param upState
	 *            上一阶段状态
	 * @return int页面对应操作 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回
	 *         5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审 拟稿人------>评审 7：拟稿人重新提交审批
	 *         8：完成 9:打回整理意 见(批准------>拟稿人)）10：交办人提交 11:编辑提交 12 编辑打回 13: 撤回 14:
	 *         反回 15：交给拟稿人整理（相当于打回整理---->拟稿人）
	 *         16：拟稿人整理完交给当前阶段（拟稿人------->当前阶段审批人）
	 */
	private int sepicalAssignReBack(int taskElseType, int upState) {
		int elseType = 1;
		switch (taskElseType) {
		case 2:// 交办
		case 3:// 转批
		case 4:// 打回
		case 5:// 提交意见（提交审批意见(评审-------->拟稿人)
		case 9:// 打回整理
		case 15:
			elseType = 1;
			break;

		case 10:// 交办人提交后点击撤销
			// 交办状态
			elseType = 2;
			break;
		case 6:// 二次评审
			// 撤回到整理意见阶段
			elseType = 5;
			break;
		case 7:// 重新提交
		case 0:// 重新提交
			// 撤回到打回
			elseType = 4;
			break;
		case 16:
			elseType = 15;
			break;
		}
		if (upState == 10) {// 整理意见阶段点击撤回
			// 设为整理意见操作
			elseType = 9;
		}

		if (taskElseType == 10) {
			// 页面显示为交办人的页面
			taskElseType = 2;
		}
		return elseType;
	}

	/**
	 * 特殊操作处理
	 * 
	 * @param taskElseType
	 *            最后一次提交的状态
	 * @param upState
	 *            上一阶段状态
	 * @return int页面对应操作
	 */
	private int sepicalOperReBack(int taskElseType, int upState) {
		if (upState == 0) {// 拟稿人提交任务
			// 返回为打回操作
			taskElseType = 4;
		} else if (upState == 10) {// 整理意见
			// 设为整理意见操作
			taskElseType = 9;
		}

		if (taskElseType == 10) {
			// 页面显示为交办人的页面
			taskElseType = 2;
		}
		return taskElseType;
	}

	/**
	 * 获取审批记录 当前任务审批最近一次提交记录
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @return JecnTaskForRecodeNew
	 */
	private JecnTaskForRecodeNew getNewestTaskForRecodeExceptEditOperate(Long taskId) {
		List<JecnTaskForRecodeNew> records = getTaskForRecodesExceptEditOperate(taskId);
		if (records.size() > 0) {
			return records.get(0);
		}
		return null;
	}

	private List<JecnTaskForRecodeNew> getTaskForRecodesExceptEditOperate(Long taskId) {
		List<JecnTaskForRecodeNew> result = new ArrayList<JecnTaskForRecodeNew>();
		List<JecnTaskForRecodeNew> records = getTaskRecordsDesc(taskId);
		for (JecnTaskForRecodeNew record : records) {
			if (record.getTaskElseState() != null && record.getTaskElseState() != 11 && record.getTaskElseState() != 12) {
				result.add(record);
			}
		}
		return result;
	}

	/**
	 * 撤回操作 （将任务从当前阶段撤回到上一阶段，只有上一阶段审批人可执行此操作）
	 * 
	 * @param curPeopleId
	 *            当前操作人
	 * @param taskBeanNew
	 *            任务
	 * @param taskStageList
	 *            任务对应的阶段信息
	 * @throws Exception
	 */
	private void reCallTask(long curPeopleId, JecnTaskBeanNew taskBeanNew, List<JecnTaskStage> taskStageList)
			throws Exception {
		if (curPeopleId == -1 || taskBeanNew == null) {
			throw new NullPointerException("撤销获取提交信息异常！");
		}
		if (!canReCallBack(taskBeanNew.getUpState())) {
			log.error("非法的撤回操作，上个阶段为：" + taskBeanNew.getUpState());
			throw new TaskIllegalArgumentException("CAN_NOT_RECALL", 9);
		}
		List<JecnTaskForRecodeNew> records = getTaskForRecodesExceptEditOperate(taskBeanNew.getId());
		// 获取审批记录 当前任务审批最近一次提交记录
		JecnTaskForRecodeNew newestRecord = records.get(0);
		// 如果是被转批人、被转交人、被整理人，是需要查找记录，找到谁给他的
		Long recordId = getRecordId(curPeopleId, taskBeanNew, records, newestRecord.getTaskElseState());
		// int taskElseType =
		// sepicalAssignReBack(newestRecord.getTaskElseState(),
		// newestRecord.getUpState());

		if (curPeopleId != taskBeanNew.getFromPeopleId() && curPeopleId != newestRecord.getFromPeopleId()) {// 当前操作人是否为上次提交源人
			throw new IllegalArgumentException("撤回操作参数非法！curPeopleId = " + curPeopleId
					+ " taskBeanNew.getFromPeopleId()=" + taskBeanNew.getFromPeopleId()
					+ " forRecodeNew.getFromPeopleId()=" + curPeopleId);
		}

		taskBeanNew.setUpdateTime(new Date());
		int curState = taskBeanNew.getUpState();
		taskBeanNew.setUpState(taskBeanNew.getState());
		// 当前操作人为任务源人，可执行撤回操作
		taskBeanNew.setState(curState);

		// 记录当前阶段名称
		setMarkName(taskBeanNew, taskStageList);
		// 相对页面操作
		// taskBeanNew.setTaskElseState(taskElseType);
		taskBeanNew.setTaskElseState(13);
		// 更新源人
		taskBeanNew.setFromPeopleId(curPeopleId);
		setTaskEdit(taskBeanNew, null);
		// 更新任务
		abstractTaskDao.getSession().update(taskBeanNew);
		abstractTaskDao.getSession().flush();

		List<Long> cancelPeoples = getCancelPeoples(taskBeanNew);
		// 删除所有操作人
		deleteJecnTaskPeopleNew(taskBeanNew.getId());
		abstractTaskDao.getSession().flush();

		// 谁操作，目标人就是谁
		Set<Long> setPeopleId = new HashSet<Long>();
		setPeopleId.add(curPeopleId);
		// abstractTaskDao.getSession().evict(taskBeanNew);
		// taskBeanNew.setTaskElseState(13);
		// 获取目标人集合 并保存流程记录
		updateToPeopleAndForRecord(setPeopleId, taskBeanNew, "", "", curPeopleId);
		if (recordId != null) {
			JecnTaskPeopleNew newestApprovePeople = getJecnTaskPeopleNewList(taskBeanNew.getId()).get(0);
			newestApprovePeople.setRecordId(recordId);
			abstractTaskDao.getSession().update(newestApprovePeople);
		}

		// 发送代办处理
		sendInfoCancels(taskBeanNew, personDao, setPeopleId, cancelPeoples, curPeopleId);

		// 获取人员信息，发送邮件
		// sendTaskEmail(setPeopleId, taskBeanNew);
	}

	private List<Long> getCancelPeoples(JecnTaskBeanNew taskBeanNew) {
		// 待删除目标人集合
		String hql = "SELECT approvePid,id from JecnTaskPeopleNew where taskId=?";
		List<Object[]> cancelObjs = abstractTaskDao.listHql(hql, taskBeanNew.getId());
		List<Long> cancelPeoples = new ArrayList<Long>();
		Long peopleId = null;
		for (Object[] obj : cancelObjs) {
			peopleId = Long.valueOf(obj[0].toString());
			cancelPeoples.add(peopleId);
			taskBeanNew.getCanclePeopleMap().put(peopleId, Long.valueOf(obj[1].toString()));
		}
		return cancelPeoples;
	}

	private Long getRecordId(long peopleId, JecnTaskBeanNew taskBeanNew, List<JecnTaskForRecodeNew> records,
			Integer taskElseState) {
		Long recordId = null;
		if (taskElseState == 16 || taskElseState == 10) {
			Integer whoGiveTaskTaskElseState = null;
			if (taskElseState == 16) {
				whoGiveTaskTaskElseState = 15;
			} else if (taskElseState == 10) {
				whoGiveTaskTaskElseState = 2;
			}
			recordId = getWhoGiveTaskRecord(peopleId, whoGiveTaskTaskElseState, records);
			if (recordId == null) {
				log.error("未找到该登录的人的任务是从日志哪来的，taskElseState:" + taskElseState + " 任务id:" + taskBeanNew.getId()
						+ " 当前人：" + peopleId);
				throw new TaskIllegalArgumentException();
			}
		} else if (taskElseState == 1) {
			for (JecnTaskForRecodeNew record : records) {
				if (peopleId == record.getToPeopleId()) {
					if (record.getTaskElseState() == 3) {// 是被转批来的
						recordId = record.getId();
						break;
					}
					break;
				}
			}
		}
		return recordId;
	}

	/**
	 * 找到是谁给他的这个任务
	 * 
	 * @param peopleId
	 * @param whoGiveHisTaskTaskElseState
	 * @param records
	 * @return
	 */
	private Long getWhoGiveTaskRecord(Long peopleId, Integer whoGiveHisTaskTaskElseState,
			List<JecnTaskForRecodeNew> records) {
		Long recordId = null;
		for (JecnTaskForRecodeNew record : records) {
			if (record.getTaskElseState().equals(whoGiveHisTaskTaskElseState)
					&& peopleId.equals(record.getToPeopleId())) {
				recordId = record.getId();
				break;
			}
		}
		return recordId;
	}

	/**
	 * 是否存在撤回功能
	 * 
	 * @param state
	 * @return
	 */
	private boolean canReCallBack(Integer state) {
		return state != 3 && !(state >= 11 && state <= 14);
	}

	/**
	 * 
	 * 暂时废弃， 目前拟稿人重新提交不能修改查阅权限，密级，类别，术语定义 PRF任务重新提交
	 * 
	 * @param submitMessage
	 *            重新提交信息
	 * @param taskBeanNew
	 *            任务主表信息
	 * @param typeId
	 *            文件类别
	 * @param isPublic
	 *            是否公开 0：秘密1：公开
	 * @throws Exception
	 */
	protected void reSubmitMethodCommon(SimpleSubmitMessage submitMessage, JecnTaskBeanNew taskBeanNew,
			JecnTaskApplicationNew jecnTaskApplicationNew, Long typeId, Long isPublic) throws Exception {
		if (submitMessage.getListAuditPeople() == null || submitMessage.getListAuditPeople().size() <= 1) {
			return;
		}
		// 获取审批变动
		String taskFixedDesc = assembledPrfTaskFixed(submitMessage, taskBeanNew, jecnTaskApplicationNew, typeId,
				isPublic);

		Long taskId = taskBeanNew.getId();
		/** 更新时间 */
		taskBeanNew.setUpdateTime(new Date());
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId().trim());
		taskBeanNew.setFromPeopleId(curPeopleId);
		taskBeanNew.setUpState(0);

		// 重新提交下级审批人
		TempAuditPeopleBean auditPeople = getReSubmitState(submitMessage.getListAuditPeople());
		if (auditPeople == null) {
			throw new NullPointerException("重新提交获取当前审批阶段审批人ID异常！reSubmitMethodCommon");
		}

		// 获取下个审批阶段(文控审核人的下一个阶段)
		int nextState = auditPeople.getState();
		// 获取目标人集合
		Set<Long> peopleList = new HashSet<Long>();

		// 解析字符串数组"," 逗号为分隔符
		String[] strIds = auditPeople.getAuditId().split(",");
		for (String string : strIds) {
			if (JecnCommon.isNullOrEmtryTrim(string)) {
				throw new NullPointerException("重新提交获取当前审批阶段审批人ID异常！reSubmitMethodCommon");
			}
			peopleList.add(Long.valueOf(string.trim()));
		}
		if (peopleList.size() == 0) {
			throw new TaskIllegalArgumentException("重新提交异常！", -1);
		}
		taskBeanNew.setState(nextState);
		// 设置操作状态 重新提交
		taskBeanNew.setTaskElseState(7);

		// 更新变更说明
		taskBeanNew.setTaskDesc(submitMessage.getTaskDesc());

		// 删除上次存在的各阶段审批人
		String hql = "delete from JecnTaskApprovePeopleConn where taskId = ?";
		abstractTaskDao.execteHql(hql, taskId);

		// 重新设置各阶段审批人
		for (TempAuditPeopleBean auditPeopleBean : submitMessage.getListAuditPeople()) {
			if (auditPeopleBean == null || JecnCommon.isNullOrEmtryTrim(auditPeopleBean.getAuditId())) {// 验证人员是否存在
				// ,拟稿人不读取，数组0为空
				continue;
			}
			JecnTaskApprovePeopleConn taskApprovePeopleConn = null;
			String[] strAuditIds = auditPeopleBean.getAuditId().split(",");
			for (int i = 0; i < strAuditIds.length; i++) {
				taskApprovePeopleConn = new JecnTaskApprovePeopleConn();
				String str = strAuditIds[i];
				taskApprovePeopleConn.setApprovePid(Long.valueOf(str.trim()));
			}
		}
		// 删除当前阶任务操作人
		deleteJecnTaskPeopleNew(curPeopleId, taskBeanNew);
		// 获取目标人集合 并保存流程记录
		updateToPeopleAndForRecord(peopleList, taskBeanNew, submitMessage.getOpinion(), taskFixedDesc, curPeopleId);

		// 获取人员信息，发送邮件
		if (peopleList.size() > 0) {
			sendTaskEmail(peopleList, taskBeanNew);
		}
	}

	/**
	 * 拟稿人重新提交
	 * 
	 * @param submitMessage
	 *            页面提交的信息
	 * @param taskStageList
	 *            任务对应的阶段信息
	 * @throws Exception
	 */
	protected void reSubmitTask(SimpleSubmitMessage submitMessage, List<JecnTaskStage> taskStageList) throws Exception {
		if (submitMessage == null || submitMessage.getListAuditPeople() == null) {
			throw new TaskIllegalArgumentException("重新提交各阶段审批人为空！", -1);
		}
		// 任务主键ID
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		/** 任务基本信息 */
		JecnTaskBeanNew jecnTaskBeanNew = getJecnTaskBeanNew(taskId);
		// PRF任务重新提交
		reSubmitMethodCommon(submitMessage, jecnTaskBeanNew, taskStageList);
	}

	/**
	 * 重新提交流程任务
	 * 
	 * @param simpleTaskBean
	 * @throws BsException
	 */
	protected void editFlowTask(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {

	}

	/**
	 * 重新提交文件任务
	 */
	protected void editFileTask(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {

	}

	/**
	 * 重新提交制度任务
	 */
	protected void editRuleTask(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {

	}

	/**
	 * 获取流程信息
	 * 
	 * @param simpleTaskBean
	 */
	protected void getFlowInfomation(SimpleTaskBean simpleTaskBean) throws Exception {
	}

	/**
	 * 获取文件信息
	 * 
	 * @param simpleTaskBean
	 */
	protected void getFileInfomation(SimpleTaskBean simpleTaskBean) throws Exception {
	}

	/**
	 * 获取制度
	 * 
	 * @param simpleTaskBean
	 */
	protected void getRuleInfomation(SimpleTaskBean simpleTaskBean) throws Exception {
	}

	/**
	 * 组装审批变动信息
	 * 
	 * @param isPublic
	 *            密级
	 * @param accessIds
	 *            查阅权限IDS
	 * @param typeId
	 *            文件类别
	 * @param flowId
	 *            流程ID
	 * @throws BsException
	 */
	protected String assembledPrfTaskFixed(SimpleSubmitMessage submitMessage, JecnTaskBeanNew taskBeanNew,
			JecnTaskApplicationNew jecnTaskApplicationNew, Long typeId, Long isPublic) throws Exception {
		if (submitMessage == null || taskBeanNew == null) {
			log.error("JecnFlowTaskBs类assembledFlowTaskFixed方法中参数为空!");
			throw new TaskIllegalArgumentException("参数不能为空!", -1);
		}
		// 密级
		String strPublic = submitMessage.getStrPublic();
		// 类别
		String strType = submitMessage.getPrfType();
		// 组装审批变动信息
		StringBuffer strBuffer = new StringBuffer();
		if (jecnTaskApplicationNew.getIsPublic() != 0) {// 存在密级
			getFixIsPublic(strPublic, strBuffer, isPublic);
		}
		// 显示类别
		if (jecnTaskApplicationNew.getFileType() != 0) {
			getFileType(submitMessage, strBuffer, typeId, strType);
		}

		// 存在查阅权限
		if (jecnTaskApplicationNew.getIsAccess() != 0) {
			getAccessPermiss(submitMessage, taskBeanNew.getTaskType(), taskBeanNew.getRid(), strBuffer);
		}
		if (strBuffer.length() == 0) {
			return JecnTaskCommon.TASK_NOTHIN;
		}
		return strBuffer.toString();
	}

	/**
	 * 获取当前任务所有的State
	 * 
	 * @param jecnTaskStageList
	 * @return
	 */
	protected List<Integer> getOrderList(List<JecnTaskStage> jecnTaskStageList) {
		List<Integer> orderList = new ArrayList<Integer>();
		for (JecnTaskStage taskStage : jecnTaskStageList) {
			orderList.add(taskStage.getStageMark());
		}
		return orderList;
	}

	/**
	 * 
	 * 根据获取的人员主键ID集合更新日志信息及目标操作人信息
	 * 
	 * @param setPeopleId
	 *            人员主键集合
	 * @param taskId
	 *            任务Id
	 * @param jecnTaskBeanNew
	 *            任务主表记录
	 * @param opinion
	 *            提交意见
	 * @param taskFixedDesc
	 *            审批变动
	 * @param listJecnTaskPeopleNew
	 *            目标操作人记录集合
	 * @param listJecnTaskForRecodeNew
	 *            日志信息集合
	 * @return Set<Long>目标操作人集合
	 */
	protected void updateToPeopleAndForRecord(Set<Long> setPeopleId, JecnTaskBeanNew jecnTaskBeanNew, String opinion,
			String taskFixedDesc, Long curPeopleId) throws Exception {
		// 获取目标人集合
		if (setPeopleId == null || setPeopleId.size() == 0 || jecnTaskBeanNew == null) {
			return;
		}
		Iterator<Long> it = setPeopleId.iterator();
		/** 记录排序号 */
		int sort = 0;

		Long taskId = jecnTaskBeanNew.getId();
		try {
			if (taskId == null) {
				sort = 1;
			} else {
				sort = abstractTaskDao.getCurTaskRecordCount(taskId) + 1;
			}

			while (it.hasNext()) {
				Long peopleAssignedId = it.next();
				// 目标人是否存在
				checkCreateUser(peopleAssignedId);
				// 根据获取的人员主键ID更新日志信息及目标操作人信息
				sort = updateTaskPeopleNew(peopleAssignedId, taskId, jecnTaskBeanNew, opinion, taskFixedDesc, sort);
				// 日志顺序++
				sort++;
			}

			// 代办处理
			if (!(jecnTaskBeanNew.getUpState() == 0 && jecnTaskBeanNew.getTaskElseState() == 0)) {// 非创建代办时
				sendTaskInfo(jecnTaskBeanNew, personDao, setPeopleId, curPeopleId);
			}
		} catch (TaskIllegalArgumentException ex) {
			throw ex;
		} catch (Exception e) {
			log.error("获取当前任务日志总数异常！", e);
			throw e;
		}
		return;
	}

	/**
	 * 更新流程信息
	 * 
	 * @param simpleTaskBean
	 * @param jecnTaskBeanNew
	 * @param jecnTaskForRecodeNewList
	 * @param jecnTaskPeopleNewList
	 * @return
	 * @throws Exception
	 */

	protected String updateAssembledFlowTaskFixed(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew)
			throws Exception {
		return "";
	}

	/**
	 * 更新文件信息
	 * 
	 * @param simpleTaskBean
	 * @param jecnTaskBeanNew
	 * @param jecnTaskForRecodeNewList
	 * @param jecnTaskPeopleNewList
	 * @return
	 * @throws Exception
	 */

	protected String updateAssembledFileTaskFixed(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew)
			throws Exception {
		return "";
	}

	/**
	 * 更新制度信息
	 * 
	 * @param simpleTaskBean
	 * @param jecnTaskBeanNew
	 * @param jecnTaskForRecodeNewList
	 * @param jecnTaskPeopleNewList
	 * @return
	 * @throws Exception
	 */

	protected String updateAssembledRuleTaskFixed(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew)
			throws Exception {
		return "";
	}

	/**
	 * 验证当前登录人是否为任务审批人
	 * 
	 * @param peopleId
	 *            登录人人员主键ID
	 * @param taskId
	 *            任务主键ID
	 * @return True 是当前任务审批人；false：不是当前任务审批人
	 * @throws Exception
	 */

	protected boolean isApprovePeopleChecked(Long peopleId, Long taskId, int taskElseState, int approveCounts)
			throws Exception {
		int count = abstractTaskDao.getPeopleCountByLoginPeople(peopleId, taskId, taskElseState, approveCounts);
		if (count == 1) {
			return true;
		}
		return false;
	}

	/**
	 * 打开试用行报告
	 * 
	 * @param id
	 *            任务主键ID
	 * @return JecnTaskFileTemporary 试运行文件
	 * @throws Exception
	 */

	protected JecnTaskFileTemporary downLoadTaskRunFile(Long id) throws Exception {
		return abstractTaskDao.downLoadTaskRunFile(id);
	}

	/**
	 * 更新查阅权限
	 * 
	 * @param accessBean
	 *            查阅权限对象
	 * @throws Exception
	 */
	protected void updateAccessPermissions(JecnTempAccessBean accessBean) throws Exception {
		/*
		 * // 部门查阅权限以逗号分隔‘,’ String orgIds = accessBean.getOrgIds(); //
		 * 岗位查阅权限以逗号分隔‘,’ String posIds = accessBean.getPosIds(); //
		 * 岗位组查阅权限以逗号分隔',' String posGroupIds = accessBean.getPosGroupIds();
		 */

		List<AccessId> orgIds = accessBean.getAccId().getOrgAccessId();

		List<AccessId> posIds = accessBean.getAccId().getPosAccessId();

		List<AccessId> posGroupIds = accessBean.getAccId().getPosGroupAccessId();

		// 0是流程，1是文件，2是标准，3是制度
		int type = accessBean.getType();
		// 关联ID
		Long relateId = accessBean.getRelateId();

		// 删除查阅权限

		// 1 岗位查阅权限
		String sqlPos = "DELETE FROM JECN_ACCESS_PERMISSIONS_T WHERE RELATE_ID=? AND TYPE = ?";
		orgAccessPermissionsDao.execteNative(sqlPos, relateId, type);

		// 2 部门查阅权限
		String sqlOrg = "DELETE FROM JECN_ORG_ACCESS_PERM_T WHERE RELATE_ID=? AND TYPE = ?";
		orgAccessPermissionsDao.execteNative(sqlOrg, relateId, type);
		// 岗位组查阅权限
		String sqlPosGroup = "DELETE FROM JECN_GROUP_PERMISSIONS_T WHERE RELATE_ID=? AND TYPE = ?";
		orgAccessPermissionsDao.execteNative(sqlPosGroup, relateId, type);

		// 添加部门查阅权限
		if (orgIds != null && !orgIds.isEmpty()) {
			for (AccessId orgIdObj : orgIds) {
				if (orgIdObj != null) {
					Long orgId = orgIdObj.getAccessId();
					int isDownload = orgIdObj.isDownLoad() ? 1 : 0;
					if (JecnCommon.isOracle()) {
						String sql = "insert into JECN_ORG_ACCESS_PERM_T (ID, ORG_ID, RELATE_ID, TYPE,ACCESS_TYPE) VALUES (JECN_PERMISSIONS_SQUENCE.NEXTVAL, ?, ?, ?,?)";
						orgAccessPermissionsDao.execteNative(sql, orgId, relateId, type, isDownload);
					} else if (JecnCommon.isSQLServer()) {
						String sql = "insert into JECN_ORG_ACCESS_PERM_T (ORG_ID, RELATE_ID, TYPE,ACCESS_TYPE) VALUES (?, ?, ?,?)";
						orgAccessPermissionsDao.execteNative(sql, orgId, relateId, type, isDownload);
					}

				}
			}
		}
		// 添加岗位查阅权限
		if (posIds != null && !posIds.isEmpty()) {
			for (AccessId posIdObj : posIds) {
				if (posIdObj != null) {
					Long posId = posIdObj.getAccessId();
					int isDownload = posIdObj.isDownLoad() ? 1 : 0;
					if (JecnCommon.isOracle()) {
						String sql = "insert into JECN_ACCESS_PERMISSIONS_T (ID, FIGURE_ID, RELATE_ID, TYPE,ACCESS_TYPE) VALUES (JECN_PERMISSIONS_SQUENCE.NEXTVAL, ?, ?, ?,?)";
						orgAccessPermissionsDao.execteNative(sql, posId, relateId, type, isDownload);
					} else if (JecnCommon.isSQLServer()) {
						String sql = "insert into JECN_ACCESS_PERMISSIONS_T (FIGURE_ID, RELATE_ID, TYPE,ACCESS_TYPE) VALUES (?, ?, ?,?)";
						orgAccessPermissionsDao.execteNative(sql, posId, relateId, type, isDownload);
					}

				}
			}
		}
		// 添加岗位组查阅权限
		if (posGroupIds != null && !posGroupIds.isEmpty()) {
			for (AccessId posGroupIdObj : posGroupIds) {
				if (posGroupIdObj != null) {
					Long posGroupId = posGroupIdObj.getAccessId();
					int isDownload = posGroupIdObj.isDownLoad() ? 1 : 0;
					if (JecnCommon.isOracle()) {
						String sql = "insert into JECN_GROUP_PERMISSIONS_T (ID, POSTGROUP_ID, RELATE_ID, TYPE,ACCESS_TYPE) VALUES (JECN_PERMISSIONS_SQUENCE.NEXTVAL, ?, ?, ?,?)";
						orgAccessPermissionsDao.execteNative(sql, posGroupId, relateId, type, isDownload);
					} else if (JecnCommon.isSQLServer()) {
						String sql = "insert into JECN_GROUP_PERMISSIONS_T (POSTGROUP_ID, RELATE_ID, TYPE,ACCESS_TYPE) VALUES (?, ?, ?,?)";
						orgAccessPermissionsDao.execteNative(sql, posGroupId, relateId, type, isDownload);
					}

				}
			}
		}

	}

	/**
	 * 更新查阅权限
	 * 
	 * @param accessBean
	 *            查阅权限对象
	 * @throws Exception
	 */
	protected void updateAccessPermissionsWeb(JecnTempAccessBean accessBean) throws Exception {
		// 部门查阅权限以逗号分隔‘,’
		String orgIds = accessBean.getOrgIds();
		// 岗位查阅权限以逗号分隔‘,’
		String posIds = accessBean.getPosIds();
		// 岗位组查阅权限以逗号分隔','
		String posGroupIds = accessBean.getPosGroupIds();
		// 0是流程，1是文件，2是标准，3是制度
		int type = accessBean.getType();
		// 关联ID
		Long relateId = accessBean.getRelateId();

		// 删除查阅权限

		// 1 岗位查阅权限
		String sqlPos = "DELETE FROM JECN_ACCESS_PERMISSIONS_T WHERE RELATE_ID=? AND TYPE = ?";
		orgAccessPermissionsDao.execteNative(sqlPos, relateId, type);

		// 2 部门查阅权限
		String sqlOrg = "DELETE FROM JECN_ORG_ACCESS_PERM_T WHERE RELATE_ID=? AND TYPE = ?";
		orgAccessPermissionsDao.execteNative(sqlOrg, relateId, type);
		// 岗位组查阅权限
		String sqlPosGroup = "DELETE FROM JECN_GROUP_PERMISSIONS_T WHERE RELATE_ID=? AND TYPE = ?";
		orgAccessPermissionsDao.execteNative(sqlPosGroup, relateId, type);

		// 添加部门查阅权限
		if (!JecnCommon.isNullOrEmtryTrim(orgIds)) {
			String[] orgIdsArr = orgIds.split(",");
			for (String orgIdStr : orgIdsArr) {
				if (!JecnCommon.isNullOrEmtryTrim(orgIdStr)) {
					Long orgId = Long.valueOf(orgIdStr.toString());
					if (JecnCommon.isOracle()) {
						String sql = "insert into JECN_ORG_ACCESS_PERM_T (ID, ORG_ID, RELATE_ID, TYPE) VALUES (JECN_PERMISSIONS_SQUENCE.NEXTVAL, ?, ?, ?)";
						orgAccessPermissionsDao.execteNative(sql, orgId, relateId, type);
					} else if (JecnCommon.isSQLServer()) {
						String sql = "insert into JECN_ORG_ACCESS_PERM_T (ORG_ID, RELATE_ID, TYPE) VALUES (?, ?, ?)";
						orgAccessPermissionsDao.execteNative(sql, orgId, relateId, type);
					}

				}
			}
		}
		// 添加岗位查阅权限
		if (!JecnCommon.isNullOrEmtryTrim(posIds)) {
			String[] posIdsArr = posIds.split(",");
			for (String posIdStr : posIdsArr) {
				if (!JecnCommon.isNullOrEmtryTrim(posIdStr)) {
					Long posId = Long.valueOf(posIdStr.toString());
					if (JecnCommon.isOracle()) {
						String sql = "insert into JECN_ACCESS_PERMISSIONS_T (ID, FIGURE_ID, RELATE_ID, TYPE) VALUES (JECN_PERMISSIONS_SQUENCE.NEXTVAL, ?, ?, ?)";
						orgAccessPermissionsDao.execteNative(sql, posId, relateId, type);
					} else if (JecnCommon.isSQLServer()) {
						String sql = "insert into JECN_ACCESS_PERMISSIONS_T (FIGURE_ID, RELATE_ID, TYPE) VALUES (?, ?, ?)";
						orgAccessPermissionsDao.execteNative(sql, posId, relateId, type);
					}

				}
			}
		}
		// 添加岗位组查阅权限
		if (!JecnCommon.isNullOrEmtryTrim(posGroupIds)) {
			String[] posGroupIdsArr = posGroupIds.split(",");
			for (String posGroupIdStr : posGroupIdsArr) {
				if (!JecnCommon.isNullOrEmtryTrim(posGroupIdStr)) {
					Long posGroupId = Long.valueOf(posGroupIdStr.toString());
					if (JecnCommon.isOracle()) {
						String sql = "insert into JECN_GROUP_PERMISSIONS_T (ID, POSTGROUP_ID, RELATE_ID, TYPE) VALUES (JECN_PERMISSIONS_SQUENCE.NEXTVAL, ?, ?, ?)";
						orgAccessPermissionsDao.execteNative(sql, posGroupId, relateId, type);
					} else if (JecnCommon.isSQLServer()) {
						String sql = "insert into JECN_GROUP_PERMISSIONS_T (POSTGROUP_ID, RELATE_ID, TYPE) VALUES (?, ?, ?)";
						orgAccessPermissionsDao.execteNative(sql, posGroupId, relateId, type);
					}

				}
			}
		}

	}

	/**
	 * 创建文件任务
	 * 
	 * @param tempCreateTaskBean
	 * @throws Exception
	 */
	protected void createFileTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {

	}

	/**
	 * 创建流程任务
	 * 
	 * @param tempCreateTaskBean
	 * @throws Exception
	 */
	protected void createFlowTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {

	}

	/**
	 * 创建制度任务
	 * 
	 * @param tempCreateTaskBean
	 * @throws Exception
	 */
	protected void createRuleTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {

	}

	// /**
	// * 任务审批完成获取相关文控信息
	// *
	// * @param taskHistoryNew
	// * @param taskBeanNew
	// * @throws Exception
	// */
	// protected void setFlowHistoryBean(JecnTaskHistoryNew taskHistoryNew,
	// JecnTaskBeanNew taskBeanNew) throws Exception {
	// if (taskHistoryNew == null || taskBeanNew == null) {
	// return;
	// }
	// // 任务驳回次数
	// taskHistoryNew.setApproveCount((long) taskBeanNew.getApproveNoCounts());
	// JecnUser user = personDao.get(taskBeanNew.getCreatePersonId());
	// // 拟稿人真实姓名
	// taskHistoryNew.setDraftPerson(user.getTrueName());
	// // 变更说明
	// taskHistoryNew.setModifyExplain(taskBeanNew.getTaskDesc());
	// // 设置关联ID
	// taskHistoryNew.setRelateId(taskBeanNew.getRid());
	// // 关联类型
	// taskHistoryNew.setType(taskBeanNew.getTaskType());
	// // 0：试运行 1：是正式 2：是升级
	// taskHistoryNew.setTestRunNumber(taskBeanNew.getFlowType());
	// // 开始时间
	// taskHistoryNew.setStartTime(JecnCommon.getStringbyDateHMS(taskBeanNew
	// .getStartTime()));
	// // 结束时间
	// taskHistoryNew.setEndTime(JecnCommon.getStringbyDateHMS(taskBeanNew
	// .getEndTime()));
	//
	// // Object[]0:任务阶段；1：当前阶段任务提交时间；2：人员真实姓名
	// List<Object[]> listObject = getForRecordInfo(taskBeanNew.getId());
	// // 根据任务类型获取对应的系统审批阶段配置项信息
	// List<JecnConfigItemBean> list = JecnTaskCommon
	// .getJecnConfigItemBeanList(taskBeanNew.getTaskType());
	//
	// List<String> listOrder = getOrderApproverList(taskBeanNew.getId());
	// // 任务审核人相关信息
	// List<JecnTaskHistoryFollow> listJecnTaskHistoryFollow = new
	// ArrayList<JecnTaskHistoryFollow>();
	// int count = 0;
	// for (String str : listOrder) {// 循环任务审批阶段
	// int curState = JecnTaskCommon.getState(str);
	// if (curState == -1) {
	// continue;
	// }
	// // 当前阶段对应系统配置的阶段名称
	// TempAuditPeopleBean auditPeopleBean = JecnTaskCommon
	// .getTempAuditPeopleBean(curState, list);
	// count++;
	// for (Object[] object : listObject) {
	// // 记录审核人信息
	// JecnTaskHistoryFollow historyFollow = new JecnTaskHistoryFollow();
	// if (JecnTaskCommon.isNullObj(object[0])
	// || JecnTaskCommon.isNullObj(object[1])
	// || JecnTaskCommon.isNullObj(object[2])) {// 信息异常
	// continue;
	// }
	// int cllState = Integer.valueOf(object[0].toString());
	// if (auditPeopleBean.getState() == curState
	// && cllState == curState) {
	// // 个审核阶段名称
	// historyFollow.setAppellation(auditPeopleBean
	// .getAuditLable());
	// // 各阶段审核人人员名称
	// historyFollow.setName(object[2].toString());
	// // 审批人提交日期
	// historyFollow.setApprovalTime((Date) object[2]);
	// }
	// historyFollow.setSort(count);
	// listJecnTaskHistoryFollow.add(historyFollow);
	// }
	// }
	// taskHistoryNew.setListJecnTaskHistoryFollow(listJecnTaskHistoryFollow);
	// }

	/**
	 * 当前任务，当前操作人是否已审批任务
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param curPeopleId
	 *            当前操作人主键ID
	 * @param taskState
	 *            我的任务操作人的操作状态 0审批;1重新提交;2整理意见;3任务管理，编辑任务
	 * @return true 当前操作人已审批，或无审批权限
	 * @throws Exception
	 */
	protected boolean hasApproveTask(Long taskId, Long curPeopleId, int taskState) throws Exception {
		String sql = "select count(t.id) from " + "jecn_task_bean_new t, jecn_task_people_new t_p"
				+ " where t.id = ? and t_p.task_id = t.id and t.state=? and t_p.approve_pid = ?";
		int count = abstractTaskDao.countAllByParamsNativeSql(sql, taskId, taskState, curPeopleId);
		if (count == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 获取文件类别ID对应的类别名称
	 * 
	 * @param typeId
	 * @param listTypeBean
	 * @return String 类别名称
	 */
	protected String getTypeName(Long typeId, List<ProceeRuleTypeBean> listTypeBean) {
		for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
			if (typeId.toString().equals(proceeRuleTypeBean.getTypeId().toString())) {
				return proceeRuleTypeBean.getTypeName();
			}
		}
		return null;
	}

	/**
	 * 获取变更后的类别ID
	 * 
	 * @param typeId
	 *            PRF文件类别ID
	 * @param tempTypeId
	 *            修改后的类别ID
	 * @return 返回修改后的类别ID
	 */
	protected Long getTypeId(Long typeId, String tempTypeId) {
		if (typeId == null || JecnCommon.isNullOrEmtryTrim(tempTypeId)) {
			return null;
		}
		Long tempId = Long.valueOf(tempTypeId.trim());
		if (typeId.equals(tempId)) {
			return null;
		}
		return tempId;
	}

	/**
	 * PRF任务重新提交
	 * 
	 * @param submitMessage
	 *            重新提交信息
	 * @param taskBeanNew
	 *            任务主表信息
	 * @throws Exception
	 */
	private void reSubmitMethodCommon(SimpleSubmitMessage submitMessage, JecnTaskBeanNew taskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {
		if (submitMessage.getListAuditPeople() == null || submitMessage.getListAuditPeople().size() <= 1) {
			return;
		}
		Long taskId = taskBeanNew.getId();
		/** 更新时间 */
		taskBeanNew.setUpdateTime(new Date());
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId().trim());
		taskBeanNew.setFromPeopleId(curPeopleId);
		if (submitMessage.getEndTime() != null) {
			taskBeanNew.setEndTime(submitMessage.getEndTime());
		}
		// 拟稿下的第一个阶段审批人
		TempAuditPeopleBean auditPeople = getReSubmitState(submitMessage.getListAuditPeople());
		if (auditPeople == null) {
			throw new NullPointerException("重新提交获取当前审批阶段审批人ID异常！reSubmitMethodCommon");
		}

		// 获取下个审批阶段(拟稿人的下一个阶段)
		int nextState = auditPeople.getState();
		// 获取目标人集合
		Set<Long> peopleList = new HashSet<Long>();

		// 解析字符串数组"," 逗号为分隔符
		String[] strIds = auditPeople.getAuditId().split(",");
		for (String string : strIds) {
			if (JecnCommon.isNullOrEmtryTrim(string)) {
				throw new NullPointerException("重新提交获取当前审批阶段审批人ID异常！reSubmitMethodCommon");
			}
			peopleList.add(Long.valueOf(string.trim()));
		}
		if (peopleList.size() == 0) {
			throw new TaskIllegalArgumentException("重新提交异常！", -1);
		}

		taskBeanNew.setTaskElseState(7);
		// 更新变更说明
		taskBeanNew.setTaskDesc(submitMessage.getTaskDesc());
		taskBeanNew.setCustomInputItemOne(submitMessage.getCustomInputItemOne());
		taskBeanNew.setCustomInputItemTwo(submitMessage.getCustomInputItemTwo());
		taskBeanNew.setCustomInputItemThree(submitMessage.getCustomInputItemThree());
		// 删除上次存在的各阶段审批人
		abstractTaskDao.deleteJecnTaskPeopleConnByTaskId(taskId);
		// JecnTaskStage的stage_mark的集合
		Set<Integer> stateSet = new HashSet<Integer>();
		// 重新设置各阶段审批人
		for (TempAuditPeopleBean auditPeopleBean : submitMessage.getListAuditPeople()) {
			if (auditPeopleBean == null || JecnCommon.isNullOrEmtryTrim(auditPeopleBean.getAuditId())) {// 验证人员是否存在
				// 拟稿人不读取，数组0为空
				continue;
			}
			stateSet.add(auditPeopleBean.getState());
			JecnTaskApprovePeopleConn taskApprovePeopleConn = null;
			String[] strAuditIds = auditPeopleBean.getAuditId().split(",");
			for (int i = 0; i < strAuditIds.length; i++) {
				String str = strAuditIds[i];
				if (StringUtils.isBlank(str)) {
					continue;
				}
				taskApprovePeopleConn = new JecnTaskApprovePeopleConn();
				taskApprovePeopleConn.setApprovePid(Long.valueOf(str.trim()));
				taskApprovePeopleConn.setStageId(auditPeopleBean.getStageId());
				abstractTaskDao.getSession().save(taskApprovePeopleConn);
			}
		}

		StringBuffer stageMarkBuf = new StringBuffer();
		for (Integer stageMark : stateSet) {
			if (stageMark == 3) {
				stageMarkBuf.append(10 + ",");
			}
			stageMarkBuf.append(stageMark + ",");
		}

		String stageMarks = stageMarkBuf.substring(0, stageMarkBuf.length() - 1);

		// 更新JecnTaskStage的isSelectedUser 为1
		String hql = "update JecnTaskStage set isSelectedUser=1 where stageMark in (" + stageMarks + ") and taskId=? ";
		abstractTaskDao.execteHql(hql, taskId);
		hql = "update JecnTaskStage set isSelectedUser=0 where stageMark not in (" + stageMarks + ") and taskId=?";
		abstractTaskDao.execteHql(hql, taskId);

		// 删除当前阶任务操作人
		deleteJecnTaskPeopleNew(curPeopleId, taskBeanNew);
		taskBeanNew.setUpState(0);
		// 设置任务状态
		taskBeanNew.setState(nextState);

		// 记录当前阶段名称
		setMarkName(taskBeanNew, taskStageList);

		// 获取目标人集合 并保存流程记录
		updateToPeopleAndForRecord(peopleList, taskBeanNew, submitMessage.getOpinion(), JecnTaskCommon.TASK_NOTHIN,
				curPeopleId);

		setTaskEdit(taskBeanNew, submitMessage);
		this.abstractTaskDao.update(taskBeanNew);
		this.abstractTaskDao.getSession().flush();

		// 获取人员信息，发送邮件
		if (peopleList.size() > 0) {
			sendTaskEmail(peopleList, taskBeanNew);
		}
	}

	/**
	 * 
	 * 重新提交获取第一阶段
	 * 
	 * @param listAuditPeople
	 * @return int 审批状态
	 */
	private TempAuditPeopleBean getReSubmitState(List<TempAuditPeopleBean> listAuditPeople) {
		if (listAuditPeople == null) {
			throw new NullPointerException("重新提交获取第一阶段异常！getReSubmitState");
		}
		for (TempAuditPeopleBean tempAuditPeopleBean : listAuditPeople) {
			if (tempAuditPeopleBean == null || tempAuditPeopleBean.getState() == 0) {
				continue;
			}
			if (tempAuditPeopleBean.getIsEmpty() == 1 && JecnCommon.isNullOrEmtryTrim(tempAuditPeopleBean.getAuditId())) {// 必填项，切不存在值
				// 异常！
				throw new NullPointerException("重新提交获取第一阶段中必填项不存在审批人！getReSubmitState");
			} else if (!JecnCommon.isNullOrEmtryTrim(tempAuditPeopleBean.getAuditId())) {
				return tempAuditPeopleBean;
			}
		}
		return null;
	}

	/**
	 * 交办人提交
	 * 
	 * @param submitMessage
	 *            页面提交的信息
	 * @throws Exception
	 */
	private void assignedPeopleSubmitTask(SimpleSubmitMessage submitMessage, List<JecnTaskStage> taskStageList)
			throws Exception {

		// 获取提交审批意见
		String opinion = submitMessage.getOpinion();
		// 任务主键ID
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		// 当前操作人
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		/** 任务基本信息 */
		JecnTaskBeanNew jecnTaskBeanNew = getJecnTaskBeanNew(taskId);
		// 目标原人 交办人提交，目标人为被交办人（执行交办操作的人）
		// 原代码
		// Long toPeopleId = jecnTaskBeanNew.getFromPeopleId();
		Long toPeopleId = getAssignFromPeople(curPeopleId, taskId);

		// 源人是否存在是否存在
		checkCreateUser(toPeopleId);
		/** 更新任务基本信息 */
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		// 交办人提交
		jecnTaskBeanNew.setTaskElseState(10);
		setTaskEdit(jecnTaskBeanNew, submitMessage);
		abstractTaskDao.update(jecnTaskBeanNew);

		// 更新目标人
		Set<Long> peopleList = new HashSet<Long>();
		peopleList.add(toPeopleId);
		// 删除当前操作目标人
		deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);
		// 记录当前阶段名称
		setMarkName(jecnTaskBeanNew, taskStageList);
		// 更新目标人信息和操作人流转记录
		updateToPeopleAndForRecord(peopleList, jecnTaskBeanNew, opinion, JecnTaskCommon.TASK_NOTHIN, curPeopleId);

	}

	private Long getAssignFromPeople(Long curPeopleId, Long taskId) throws Exception {
		Long recordId = null;
		// 查询当前审批人表
		List<JecnTaskPeopleNew> approvePeoples = getJecnTaskPeopleNewList(taskId);
		for (JecnTaskPeopleNew approve : approvePeoples) {
			if (curPeopleId.equals(approve.getApprovePid())) {
				recordId = approve.getRecordId();
				break;
			}
		}
		Long fromPeopleId = null;
		if (recordId == null) {
			JecnTaskBeanNew task = abstractTaskDao.get(taskId);
			Integer curState = task.getState();
			// 从日志中查询来源人
			List<JecnTaskForRecodeNew> records = getTaskRecordsDesc(taskId);
			for (JecnTaskForRecodeNew record : records) {
				if (record.getTaskElseState() == 7) {
					break;
				}
				if (record.getTaskElseState() == 2 && curState.equals(record.getState())) {
					if (curPeopleId.equals(record.getToPeopleId())) {
						fromPeopleId = record.getFromPeopleId();
						break;
					}
				}
			}
		} else {
			String hql = " from JecnTaskForRecodeNew where id=?";
			List<JecnTaskForRecodeNew> records = abstractTaskDao.listHql(hql, recordId);
			fromPeopleId = records.get(0).getFromPeopleId();
		}

		if (fromPeopleId == null) {
			log.error("getAssignFromPeople()未查找到来源人，任务taskId:" + taskId);
			throw new IllegalArgumentException("getAssignFromPeople()未查找到来源人，任务taskId:" + taskId);
		}

		return fromPeopleId;
	}

	private List<JecnTaskForRecodeNew> getTaskRecordsDesc(Long taskId) {
		String hql = " from JecnTaskForRecodeNew where taskId=? order by id desc";
		List<JecnTaskForRecodeNew> records = abstractTaskDao.listHql(hql, taskId);
		return records;
	}

	private boolean peopleIsExistInTaskApprove(Long peopleId, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		Long taskId = jecnTaskBeanNew.getId();
		String hql = "from JecnTaskPeopleNew where approvePid=? and taskId=?";
		List<JecnTaskPeopleNew> peoples = abstractTaskDao.listHql(hql, peopleId, taskId);
		if (peoples.size() > 0) {
			return true;
		}
		// 查询conn表，其中是否和当前审批人相同
		String sql = " select jtpcn.approve_pid from jecn_task_people_conn_new jtpcn"
				+ "  inner join jecn_task_stage jts on jtpcn.stage_id=jts.id and jts.stage_mark=? and jts.task_id=?";
		List<Object> objs = abstractTaskDao.listNativeSql(sql, jecnTaskBeanNew.getState(), taskId);
		for (Object obj : objs) {
			if (peopleId.equals(Long.valueOf(obj.toString()))) {
				return true;
			}
		}
		return false;
	}

	private void checkAssignPeopleIsExistInTaskApprove(Long peopleId, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		if (peopleIsExistInTaskApprove(peopleId, jecnTaskBeanNew)) {
			log.error("转批或者交办操作的时候，数据库中已经存在了该审批");
			throw new TaskIllegalArgumentException("审批人不能重复", 7);
		}
	}

	/**
	 * 二次评审 任务返回至评审阶段
	 * 
	 * @param submitMessage
	 *            页面提交的信息
	 * @param taskStageList
	 *            任务对应的阶段信息
	 * @throws Exception
	 */
	private void twoReviewTask(SimpleSubmitMessage submitMessage, List<JecnTaskStage> taskStageList) throws Exception {
		// 获取提交审批意见
		String opinion = submitMessage.getOpinion();
		// 二次评审人ID集合（字符串拼装）
		String newViewerIds = submitMessage.getNewViewerIds();
		if (JecnCommon.isNullOrEmtryTrim(newViewerIds)) {
			return;
		}
		// 任务主键ID
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		// 当前操作人
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		Set<Long> peopleList = new HashSet<Long>();
		// 任务信息
		JecnTaskBeanNew jecnTaskBeanNew = getJecnTaskBeanNew(taskId);
		/** 审批次数 */
		int revirewCounts = jecnTaskBeanNew.getRevirewCounts() + 1;
		jecnTaskBeanNew.setRevirewCounts(revirewCounts);
		/** 更新任务基本信息 */
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		jecnTaskBeanNew.setTaskElseState(6);
		jecnTaskBeanNew.setState(3);
		// 整理意见
		jecnTaskBeanNew.setUpState(10);
		setTaskEdit(jecnTaskBeanNew, submitMessage);
		abstractTaskDao.update(jecnTaskBeanNew);

		// 获取评审阶段的阶段信息
		JecnTaskStage taskStage = JecnTaskCommon.getCurTaskStage(taskStageList, 3);
		// 删除任务的人员关系表中的评审人信息
		abstractTaskDao.deleteJecnTaskPeopleConnByTaskIdAndStageId(taskStage.getId());
		String[] strArr = newViewerIds.split(",");
		Session s = abstractTaskDao.getSession();
		for (String str : strArr) {
			peopleList.add(Long.valueOf(str.trim()));
			JecnTaskApprovePeopleConn taskApprovePeopleConn = new JecnTaskApprovePeopleConn();
			taskApprovePeopleConn.setApprovePid(Long.valueOf(str.trim()));
			taskApprovePeopleConn.setStageId(taskStage.getId());
			s.save(taskApprovePeopleConn);
		}
		// 删除当前操作目标人
		deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);
		// 记录当前阶段名称
		setMarkName(jecnTaskBeanNew, taskStageList);
		// 更新目标人信息和操作人流转记录
		updateToPeopleAndForRecord(peopleList, jecnTaskBeanNew, opinion, JecnTaskCommon.TASK_NOTHIN, curPeopleId);
		// 更新JecnTaskStage 设置会审阶段的isSelectedUser设置为 1
		String sql = "update jecn_task_stage set is_selected_user=1 where stage_mark=3 and task_id=?";
		abstractTaskDao.execteNative(sql, taskId);

		// 获取人员信息，发送邮件
		if (peopleList.size() > 0) {
			sendTaskEmail(peopleList, jecnTaskBeanNew);
		}
	}

	/**
	 * 评审人提交意见 （存在其他评审人时继续评审，不存在时跳转至整理意见阶段）
	 * 
	 * @param submitMessage
	 *            页面提交的信息
	 * @param taskStageList
	 *            任务对应的阶段信息
	 * @throws Exception
	 */
	private void submitTaskIdea(SimpleSubmitMessage submitMessage, List<JecnTaskStage> taskStageList) throws Exception {
		// 操作人ID
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		// 审批意见
		String opinion = submitMessage.getOpinion();
		// 任务主键ID
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		// 审批变动
		String taskFixedDesc = JecnTaskCommon.TASK_NOTHIN;
		/** 任务基本信息 */
		JecnTaskBeanNew jecnTaskBeanNew = getJecnTaskBeanNew(taskId);
		// 获取目标人集合
		List<JecnTaskPeopleNew> listPeopleNew = getJecnTaskPeopleNewList(taskId);
		if (listPeopleNew.size() == 1) {// 只剩下一个评审人，提交后应到下一审批环节
			/** 更新任务基本信息 */
			jecnTaskBeanNew.setUpdateTime(new Date());
			jecnTaskBeanNew.setFromPeopleId(curPeopleId);
			// 评审人提交意见操作状态
			jecnTaskBeanNew.setTaskElseState(5);
			// 评审次数
			if (jecnTaskBeanNew.getRevirewCounts() != 2) {
				// 评审次数
				jecnTaskBeanNew.setRevirewCounts(1);
			}
			// 删除当前操作目标人
			deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);
			// 获取目标人集合
			Set<Long> setPeopleId = new HashSet<Long>();
			setPeopleId.add(jecnTaskBeanNew.getCreatePersonId());
			// 设置上次任务阶段
			jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
			// 下一个阶段为拟稿人 整理意见
			jecnTaskBeanNew.setState(10);

			// 记录当前阶段名称
			setMarkName(jecnTaskBeanNew, taskStageList);
			updateToPeopleAndForRecord(setPeopleId, jecnTaskBeanNew, opinion, taskFixedDesc, curPeopleId);

			setTaskEdit(jecnTaskBeanNew, submitMessage);
			abstractTaskDao.update(jecnTaskBeanNew);
			// 获取邮件发送的插进入
			Set<Long> peopleList = new HashSet<Long>();
			peopleList.add(jecnTaskBeanNew.getCreatePersonId());
			// 获取人员信息，发送邮件
			sendTaskEmail(peopleList, jecnTaskBeanNew);
		} else {// 存在其他审批人
			/** 更新任务基本信息 */
			jecnTaskBeanNew.setUpdateTime(new Date());
			// 设置上次任务阶段
			jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
			// 评审提交审批
			jecnTaskBeanNew.setTaskElseState(5);
			setTaskEdit(jecnTaskBeanNew, submitMessage);
			// 记录当前阶段名称
			setMarkName(jecnTaskBeanNew, taskStageList);
			// 更新当前任务
			abstractTaskDao.update(jecnTaskBeanNew);
			// 删除当前操作目标人
			deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);
			jecnTaskBeanNew.setFromPeopleId(curPeopleId);
			// 记录排序号 NOTHING:无变动
			int sort = abstractTaskDao.getCurTaskRecordCount(taskId) + 1;
			Set<Long> peoples = getRemainApprovePeoples(jecnTaskBeanNew.getId(), curPeopleId);
			for (Long toPeopleId : peoples) {
				sort++;
				// 获取任务日志信息
				JecnTaskForRecodeNew jecnTaskForRecodeNew = this.getJecnTaskForRecodeNew(jecnTaskBeanNew, toPeopleId,
						opinion, sort, JecnTaskCommon.TASK_NOTHIN);
				abstractTaskDao.getSession().save(jecnTaskForRecodeNew);
			}
			sendInfoCancel(curPeopleId, jecnTaskBeanNew);
		}
	}

	/**
	 * 取消任务待办
	 * 
	 * @param curPeopleId
	 * @param jecnTaskBeanNew
	 * @throws Exception
	 */
	private void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		if (!JecnConfigTool.sendTaskByInterface()) {
			return;
		}
		if (JecnConfigTool.isMengNiuLogin()) {
			// 获取目标人集合
			Set<Long> setPeopleId = new HashSet<Long>();
			setPeopleId.add(curPeopleId);
			// 发送代办信息
			sendTaskInfo(jecnTaskBeanNew, personDao, setPeopleId, curPeopleId);
		} else if (JecnConfigTool.isCect10Login()) {
			JecnUser jecnUser = personDao.get(curPeopleId);
			JecnUser createUser = personDao.get(jecnTaskBeanNew.getCreatePersonId());
			JecnSendTaskTOMainSystemFactory.INSTANCE.sendTaskCancel(curPeopleId, jecnTaskBeanNew, jecnUser, createUser);
		} else {
			List<Long> cancelPeoples = new ArrayList<Long>();
			cancelPeoples.add(curPeopleId);
			sendInfoCancels(jecnTaskBeanNew, personDao, null, cancelPeoples, curPeopleId);
		}
	}

	/**
	 * 获得任务目标人
	 * 
	 * @param taskId
	 * @return
	 * @throws DaoException
	 */
	private List<JecnTaskPeopleNew> getJecnTaskPeopleNewList(Long taskId) throws Exception {
		String hql = "from JecnTaskPeopleNew where taskId=? ";
		return abstractTaskDao.listHql(hql, taskId);
	}

	/**
	 * 获取各阶段审批人名称
	 * 
	 * @param taskBeanNew
	 *            当前任务对象
	 * @param orderList
	 *            审批顺序集合
	 * @param simpleTaskBean
	 *            审批页面获取的任务相关信息
	 * @throws Exception
	 */
	private void getListAuditPeopleBean(JecnTaskBeanNew taskBeanNew, List<JecnTaskStage> jecnTaskStage,
			SimpleTaskBean simpleTaskBean) throws Exception {
		// 根据任务类型获取对应的系统审批阶段配置项信息
		List<TempAuditPeopleBean> listAuditPeopleBean = getListAuditPeopleBean(taskBeanNew, jecnTaskStage);
		// 记录各阶段审批人、阶段名称
		simpleTaskBean.setListAuditPeopleBean(listAuditPeopleBean);
	}

	/**
	 * 获取审批,个阶段审核人信息
	 * 
	 * @param taskBeanNew
	 *            任务主表信息
	 * @param orderList
	 *            审批顺序
	 * @return List<TempAuditPeopleBean> 审批个阶段审核人信息
	 * @throws Exception
	 */
	private List<TempAuditPeopleBean> getListAuditPeopleBean(JecnTaskBeanNew taskBeanNew,
			List<JecnTaskStage> jecnTaskStageList) throws Exception {
		// 获取当前阶段审批人是否存在
		// boolean isCurApprovePeopleExists =
		// isStatePeople(taskBeanNew.getId());
		// new wdp
		// 根据任务类型获取对应的系统审批阶段配置项信息
		List<TempAuditPeopleBean> listAuditPeopleBean = JecnTaskCommon.getTempAuditPeopleBeanList(taskBeanNew,
				jecnTaskStageList);
		if (listAuditPeopleBean == null || listAuditPeopleBean.size() == 0) {
			return listAuditPeopleBean;
		}
		// 获取拟稿阶段
		TempAuditPeopleBean tempPeopleBean = listAuditPeopleBean.get(0);
		if (tempPeopleBean.getState() == 0) {// 拟稿状态 设置拟稿人名称
			tempPeopleBean.setAuditName(taskBeanNew.getCreatePersonTemporaryName());
			tempPeopleBean.setAuditId(taskBeanNew.getCreatePersonId().toString());
		}
		// 获取任务// 各阶段审批人集合 List<Object[]> 0：任务阶段；1：人员真实姓名2：人员主键ID;3:是否删除1:删除
		List<Object[]> listObject = abstractTaskDao.getListJecnTaskApprovePeopleConn(taskBeanNew.getId());

		for (Object[] objects : listObject) {
			int stageId = 0;
			if (objects[0] != null) {
				stageId = Integer.valueOf(objects[0].toString());
			}

			String userId = "";
			if (objects[2] != null) {
				userId = objects[2].toString();
			}

			int isLock = 0;
			if (objects[3] != null) {
				isLock = Integer.valueOf(objects[3].toString());
			}
			for (TempAuditPeopleBean auditPeopleBean : listAuditPeopleBean) {
				if (auditPeopleBean.getState() == 0) {// 跳过拟稿人阶段
					continue;
				}
				if (stageId == auditPeopleBean.getStageId()) {// 多个审批人
					String auditName = "";
					if (objects[1] != null) {
						auditName = objects[1].toString();
					}
					if (isLock == 1) {// 人员删除
						// auditName + (已离职)
						auditName = auditName + JecnTaskCommon.LEAVE_OFFICE;
						userId = null;
					}
					if (!JecnCommon.isNullOrEmtryTrim(auditPeopleBean.getAuditName())) {
						// 审核人ID
						auditPeopleBean.setAuditId(auditPeopleBean.getAuditId() + "," + userId);
						// 审核人名称
						auditPeopleBean.setAuditName(auditPeopleBean.getAuditName() + "" + auditName);
					} else {
						auditPeopleBean.setAuditName(auditName);
						auditPeopleBean.setAuditId(userId);
					}
				}
			}
		}
		return listAuditPeopleBean;
	}

	/**
	 * 获取任务审批流转记录集合
	 * 
	 * @param taskId
	 *            任务Id
	 * @param taskType
	 *            任务类型
	 * @param taskStageList
	 *            任务阶段集合
	 * @return 任务审批流转记录集合
	 * @throws Exception
	 */
	private List<JecnTaskForRecodeNew> findJecnTaskForRecodeNewByTaskId(Long taskId, int taskType,
			List<JecnTaskStage> taskStageList) throws Exception {
		// 获取审批人ID集合
		StringBuffer tempToPeopleIds = new StringBuffer();
		// 获取审批人名称集合
		StringBuffer tempToPeopleIdNames = new StringBuffer();
		// 记录所有的审批人
		List<JecnTaskForRecodeNew> tempAllList = new ArrayList<JecnTaskForRecodeNew>();
		// 获取审批记录
		List<JecnTaskForRecodeNew> listJecnTaskForRecodeNew = getJecnTaskForRecodeNewList(taskId);
		// 来源人
		Long fromPeopleId = null;
		// 文控信息
		JecnTaskForRecodeNew record = null;
		// 上个阶段
		Integer upState = null;
		// 更新时间
		Date updateTime = null;

		for (int i = 0; i < listJecnTaskForRecodeNew.size(); i++) {
			JecnTaskForRecodeNew jecnTaskForRecodeNew = listJecnTaskForRecodeNew.get(i);
			if (jecnTaskForRecodeNew.getState() == null) {
				continue;
			}
			if (jecnTaskForRecodeNew.getUpState() != 0) {// 不是拟稿状态
				// 当前审核阶段名称
				jecnTaskForRecodeNew.setUpStateName(JecnTaskCommon.getCurTaskStage(taskStageList,
						jecnTaskForRecodeNew.getUpState()).getStageName());
			}

			if (fromPeopleId == null) {
				fromPeopleId = jecnTaskForRecodeNew.getFromPeopleId();
				upState = jecnTaskForRecodeNew.getUpState();
				updateTime = jecnTaskForRecodeNew.getUpdateTime();
				record = jecnTaskForRecodeNew;
			} else {
				if (fromPeopleId.equals(jecnTaskForRecodeNew.getFromPeopleId())
						&& upState.equals(jecnTaskForRecodeNew.getUpState())
						&& updateTime.equals(jecnTaskForRecodeNew.getUpdateTime())) {
					// UN TO DO
				} else {
					// from人不同
					addTempList(record, tempToPeopleIds, tempToPeopleIdNames, tempAllList);
					// 初始化
					fromPeopleId = jecnTaskForRecodeNew.getFromPeopleId();
					upState = jecnTaskForRecodeNew.getUpState();
					updateTime = jecnTaskForRecodeNew.getUpdateTime();
					record = jecnTaskForRecodeNew;
					// 获取审批人ID集合
					tempToPeopleIds = new StringBuffer();
					// 获取审批人名称集合
					tempToPeopleIdNames = new StringBuffer();
				}
			}

			appendTempToPeopleIdAndName(tempToPeopleIds, tempToPeopleIdNames, jecnTaskForRecodeNew);

			if (i == listJecnTaskForRecodeNew.size() - 1) {
				addTempList(record, tempToPeopleIds, tempToPeopleIdNames, tempAllList);
			}
		}
		return tempAllList;
	}

	private void appendTempToPeopleIdAndName(StringBuffer tempToPeopleIds, StringBuffer tempToPeopleIdNames,
			JecnTaskForRecodeNew jecnTaskForRecodeNew) {
		tempToPeopleIds.append(jecnTaskForRecodeNew.getToPeopleId().toString()).append(",");
		// 获取评审人name集合
		tempToPeopleIdNames.append(jecnTaskForRecodeNew.getToPeopleTemporaryName()).append("/");
	}

	/**
	 * 添加组装后的评审阶段信息到临时日志集合
	 * 
	 * @param jecnTaskForRecodeNewTempory
	 *            记录组装后的日志记录
	 * @param tempToPeopleIds
	 *            评审阶段评审人ID集合
	 * @param tempToPeopleIdNames
	 *            评审阶段评审名称集合
	 * @param tempAllList
	 *            临时日志集合
	 */
	private void addTempList(JecnTaskForRecodeNew jecnTaskForRecodeNewTempory, StringBuffer tempToPeopleIds,
			StringBuffer tempToPeopleIdNames, List<JecnTaskForRecodeNew> tempAllList) {
		// 评审人结束
		if (jecnTaskForRecodeNewTempory != null) {
			if (tempToPeopleIds.length() > 1 && tempToPeopleIdNames.length() > 1) {
				String strPeopleIds = tempToPeopleIds.toString();
				strPeopleIds = strPeopleIds.substring(0, strPeopleIds.length() - 1);
				String strPeopleNames = tempToPeopleIdNames.toString();
				strPeopleNames = strPeopleNames.substring(0, strPeopleNames.length() - 1);
				jecnTaskForRecodeNewTempory.setTempToPeopleIds(strPeopleIds);
				jecnTaskForRecodeNewTempory.setToPeopleTemporaryName(strPeopleNames);
				tempAllList.add(jecnTaskForRecodeNewTempory);
			}
		}
	}

	/**
	 * 获取评审阶段审批人ID和名称
	 * 
	 * @param jecnTaskForRecodeNew
	 *            当前记录日志
	 * @param tempToPeopleIds
	 *            目标人ID集合
	 * @param tempToPeopleIdNames
	 *            目标人真实姓名集合
	 * @return true评审状态
	 */
	private boolean getReviewPeoples(JecnTaskForRecodeNew jecnTaskForRecodeNew, StringBuffer tempToPeopleIds,
			StringBuffer tempToPeopleIdNames) {
		// 获取评审人ID集合
		if (jecnTaskForRecodeNew.getToPeopleId() == null || jecnTaskForRecodeNew.getToPeopleTemporaryName() == null) {
			return false;
		}
		appendTempToPeopleIdAndName(tempToPeopleIds, tempToPeopleIdNames, jecnTaskForRecodeNew);
		return true;
	}

	/**
	 * 获得任务试用行报告的名称
	 * 
	 * @param taskId
	 *            任务主键ID的
	 * @return JecnTaskFileTemporary试运行对象
	 * @throws Exception
	 */
	private Object[] getTaskRunFileName(Long taskId) throws Exception {
		String hql = "select id,fileName from JecnTaskTestRunFile where taskId=?";
		List<Object[]> list = abstractTaskDao.listHql(hql, taskId);
		if (list.size() == 0 || list.get(0) == null) {
			return null;
		}
		Object[] obj = list.get(0);
		if (obj[0] == null || obj[1] == null) {
			return null;
		}
		return obj;
	}

	/**
	 * 设置类别
	 * 
	 * @param simpleTaskBean
	 */
	protected void setProceeRuleTypeBean(SimpleTaskBean simpleTaskBean) {
		if (simpleTaskBean == null) {
			return;
		}
		JecnTaskBeanNew taskBeanNew = simpleTaskBean.getJecnTaskBeanNew();
		if (taskBeanNew == null) {
			return;
		}
		List<ProceeRuleTypeBean> listTypeBean = null;
		// 当前审批阶段为文控阶段或部门阶段
		if (simpleTaskBean.getJecnTaskApplicationNew().getFileType() != 0) {
			listTypeBean = new ArrayList<ProceeRuleTypeBean>();
			ProceeRuleTypeBean bean = new ProceeRuleTypeBean();
			bean.setTypeId(-1L);
			bean.setTypeName("");
			listTypeBean.add(bean);
			listTypeBean.addAll(getProceeRuleTypeBean());
		}
		// 添加类别
		simpleTaskBean.setListTypeBean(listTypeBean);
	}

	/**
	 * 获取PRF；类别集合
	 * 
	 * @return
	 */
	private List<ProceeRuleTypeBean> getProceeRuleTypeBean() {
		String hql = "from ProceeRuleTypeBean";
		return abstractTaskDao.listHql(hql);
	}

	/**
	 * 获得任务的审批记录
	 * 
	 * @param taskId
	 * @return
	 * @throws DaoException
	 */
	private List<JecnTaskForRecodeNew> getJecnTaskForRecodeNewList(Long taskId) throws Exception {
		// 获取任务审批记录
		String hql = "from JecnTaskForRecodeNew where taskId =?  order by sortId";
		// 获取当前任务下所有日志记录
		List<JecnTaskForRecodeNew> list = abstractTaskDao.listHql(hql, taskId);
		// 记录所有日志下所有人员的人员ID集合
		Set<Long> setPeopleId = new HashSet<Long>();
		for (JecnTaskForRecodeNew taskRecord : list) {
			if (taskRecord.getCreatePersonId() != null) {
				setPeopleId.add(taskRecord.getCreatePersonId());
			}
			if (taskRecord.getFromPeopleId() != null) {
				setPeopleId.add(taskRecord.getFromPeopleId());
			}
			if (taskRecord.getToPeopleId() != null) {
				setPeopleId.add(taskRecord.getToPeopleId());
			}
			if (!JecnCommon.isNullOrEmtryTrim(taskRecord.getOpinion())) {// 评审意见换行符替换
				taskRecord.setOpinion(JecnTaskCommon.replaceAll(taskRecord.getOpinion()));
			}
		}
		if (setPeopleId.size() > 0) {
			hql = "select peopleId,trueName,isLock from JecnUser where peopleId in"
					+ JecnCommonSql.getIdsSet(setPeopleId);
			// Object[] 0:jecnUser主键ID；1：人员真实姓名;2:是否删除isLock=1已删除
			List<Object[]> listObj = abstractTaskDao.listHql(hql);
			for (JecnTaskForRecodeNew jecnTaskForRecodeNew : list) {
				// 通过人员id获得人员的姓名
				if (jecnTaskForRecodeNew.getCreatePersonId() != null) {
					jecnTaskForRecodeNew.setCreatePersonTemporaryName(JecnTaskCommon.getTrueNameByPeopleId(listObj,
							jecnTaskForRecodeNew.getCreatePersonId()));
				}
				if (jecnTaskForRecodeNew.getFromPeopleId() != null) {
					jecnTaskForRecodeNew.setFromPeopleTemporaryName(JecnTaskCommon.getTrueNameByPeopleId(listObj,
							jecnTaskForRecodeNew.getFromPeopleId()));
				}
				if (jecnTaskForRecodeNew.getToPeopleId() != null) {
					jecnTaskForRecodeNew.setToPeopleTemporaryName(JecnTaskCommon.getTrueNameByPeopleId(listObj,
							jecnTaskForRecodeNew.getToPeopleId()));
				}
			}
		}
		return list;
	}

	/**
	 * 根据任务主键ID获取任务信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @return JecnTaskBeanNew
	 */
	private JecnTaskBeanNew getJecnTaskBeanNew(Long taskId) {
		// 获取任务信息
		JecnTaskBeanNew taskBeanNew = abstractTaskDao.get(taskId);
		// 获得需要审批的文件的名称
		/**
		 * taskType=0时，表示流程ID，taskType=1时，表示文件ID，taskType=2时，表示制度模板ID,3:制度文件ID，4
		 * ： 流程地图ID
		 */
		long rid = taskBeanNew.getRid();
		if (0 == taskBeanNew.getTaskType() || 4 == taskBeanNew.getTaskType()) {
			JecnFlowStructureT jecnFlowStructureT = structureDao.get(rid);
			taskBeanNew.setRname(jecnFlowStructureT.getFlowName());
			if (taskBeanNew.getTaskType() == 0) {// 流程任务
				// 流程属性信息
				JecnFlowBasicInfoT jecnFlowBasicInfoT = basicInfoDao.get(rid);
				taskBeanNew.setResPeopleId(jecnFlowBasicInfoT.getResPeopleId());
			} else if (taskBeanNew.getTaskType() == 4) {
				// 流程地图属性信息
				JecnMainFlowT mainFlowT = (JecnMainFlowT) structureDao.getSession().get(JecnMainFlowT.class, rid);
				taskBeanNew.setResPeopleId(mainFlowT.getResPeopleId());
			}

		} else if (1 == taskBeanNew.getTaskType()) {
			JecnFileBeanT jecnFileBeanT = fileDao.get(rid);
			taskBeanNew.setRname(jecnFileBeanT.getFileName());
			taskBeanNew.setResPeopleId(jecnFileBeanT.getResPeopleId());
		} else if (2 == taskBeanNew.getTaskType() || 3 == taskBeanNew.getTaskType()) {
			RuleT ruleT = ruleDao.get(rid);
			taskBeanNew.setRname(ruleT.getRuleName());
			taskBeanNew.setResPeopleId(ruleT.getAttchMentId());
		}

		// 获得
		if (taskBeanNew == null) {
			log.error("JecnAbstractTaskServiceImpl中方法getJecnTaskBeanNew获取中任务信息为空!");
			throw new TaskIllegalArgumentException("任务信息不能为空!", -1);
		}
		return taskBeanNew;
	}

	/**
	 * 根据人员主键ID获取人员信息
	 * 
	 * @param peopleId
	 * @return JecnUser
	 */
	private JecnUser getJecnUser(Long peopleId) {
		// 创建人信息
		JecnUser jecnUser = personDao.get(peopleId);
		if (jecnUser == null) {
			log.error("JecnCommonTaskService中方法getJecnUser获取人员信息为空!参数peopleId=" + peopleId);
		}
		return jecnUser;
	}

	/**
	 * 设置PRF文件查阅权限
	 * 
	 * @param relateId
	 *            关联ID
	 * @param type
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @throws Exception
	 */
	private void accessPermissions(Long relateId, int type, SimpleTaskBean simpleTaskBean) throws Exception {
		if (relateId == null || simpleTaskBean == null) {
			throw new TaskIllegalArgumentException("方法accessPermissions relateId == null || simpleTaskBean == null", -1);
		}
		int accessType = -1;
		switch (type) {
		case 0:
		case 4:
			accessType = 0;
			break;
		case 1:// 文件
			accessType = 1;
			break;
		case 2:// 制度
		case 3:
			accessType = 3;
			break;
		default:
			break;
		}
		STR_SPLIT = "";
		StringBuffer orgBufIds = new StringBuffer();
		StringBuffer orgBufNames = new StringBuffer();
		// 部门权限IDs
		getOrgIds(orgBufIds, orgBufNames, relateId, accessType);

		// 添加部门查阅权限
		simpleTaskBean.setOrgIds(orgBufIds.toString());
		simpleTaskBean.setOrgNames(orgBufNames.toString());

		// 岗位查阅权限ID集合
		StringBuffer posBufIds = new StringBuffer();
		StringBuffer posBufNames = new StringBuffer();
		// 岗位查阅权限
		getPosIds(posBufIds, posBufNames, relateId, accessType);

		// 岗位部门查阅权限
		simpleTaskBean.setPosIds(posBufIds.toString());
		simpleTaskBean.setPosNames(posBufNames.toString());

		// 岗位组查阅权限ID集合
		StringBuffer groupBufIds = new StringBuffer();
		StringBuffer groupBufNames = new StringBuffer();
		// 岗位组查阅权限
		getGroupIds(groupBufIds, groupBufNames, relateId, accessType);
		// 岗位组部门查阅权限
		simpleTaskBean.setGroupIds(groupBufIds.toString());
		simpleTaskBean.setGroupNames(groupBufNames.toString());
	}

	/**
	 * 获取部门查阅权限
	 * 
	 * @param orgBufIds
	 *            部门查阅权限IDS (','分隔)
	 * @param orgBufNames
	 *            部门查阅权限NameS ('/'分隔)
	 * @param relateId
	 *            关联ID
	 * @param accessType
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @throws Exception
	 */
	private void getOrgIds(StringBuffer orgBufIds, StringBuffer orgBufNames, Long relateId, int accessType)
			throws Exception {
		// 部门查阅权限
		List<JecnTreeBean> orgList = orgAccessPermissionsDao.getOrgAccessPermissions(relateId, accessType, false);
		// 部门权限IDs
		if (orgList != null && orgList.size() > 0) {
			for (int i = 0; i < orgList.size(); i++) {
				JecnTreeBean orgTreeBean = orgList.get(i);
				if (i == orgList.size() - 1) {
					orgBufIds.append(orgTreeBean.getId());
					orgBufNames.append(orgTreeBean.getName());
				} else {
					orgBufIds.append(orgTreeBean.getId() + ",");
					orgBufNames.append(orgTreeBean.getName() + STR_SPLIT);
				}
			}
		}
	}

	/**
	 * 获取岗位查阅权限
	 * 
	 * @param posBufIds
	 *            岗位查阅权限IDS (','分隔)
	 * @param posBufNames
	 *            岗位查阅权限NameS ('/'分隔)
	 * @param relateId
	 *            关联ID
	 * @param accessType
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @throws Exception
	 */
	private void getPosIds(StringBuffer posBufIds, StringBuffer posBufNames, Long relateId, int accessType)
			throws Exception {
		// 部门查阅权限
		List<JecnTreeBean> orgList = positionAccessPermissionsDao.getPositionAccessPermissions(relateId, accessType,
				false);
		// 部门权限IDs
		if (orgList != null && orgList.size() > 0) {
			for (int i = 0; i < orgList.size(); i++) {
				JecnTreeBean orgTreeBean = orgList.get(i);
				if (i == orgList.size() - 1) {
					posBufIds.append(orgTreeBean.getId());
					posBufNames.append(orgTreeBean.getName());
				} else {
					posBufIds.append(orgTreeBean.getId() + ",");
					posBufNames.append(orgTreeBean.getName() + STR_SPLIT);
				}
			}
		}
	}

	/**
	 * @author xiaobo 获取岗位组查阅权限
	 * 
	 * @param groupBufIds
	 *            岗位查阅权限IDS (','分隔)
	 * @param groupBufNames
	 *            岗位查阅权限NameS ('/'分隔)
	 * @param relateId
	 *            关联ID
	 * @param accessType
	 *            类型 0是流程，1是文件，2是标准，3是制度
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private void getGroupIds(StringBuffer groupBufIds, StringBuffer groupBufNames, Long relateId, int accessType)
			throws Exception {
		// 部门查阅权限
		List<JecnTreeBean> groupList = positionAccessPermissionsDao.getPositionAccessPermissionsGroup(relateId,
				accessType, false);
		// 部门权限IDs
		if (groupList != null && groupList.size() > 0) {
			for (int i = 0; i < groupList.size(); i++) {
				JecnTreeBean groupTreeBean = groupList.get(i);
				if (i == groupList.size() - 1) {
					groupBufIds.append(groupTreeBean.getId());
					groupBufNames.append(groupTreeBean.getName());
				} else {
					groupBufIds.append(groupTreeBean.getId() + ",");
					groupBufNames.append(groupTreeBean.getName() + STR_SPLIT);
				}
			}
		}
	}

	/**
	 * 获取密级变动
	 * 
	 * @param strPublic
	 * @param strBuffer
	 * @param isPublic
	 */
	private void getFixIsPublic(String strPublic, StringBuffer strBuffer, Long isPublic) {
		if (!JecnCommon.isNullOrEmtryTrim(strPublic) && isPublic != null && !strPublic.equals(isPublic.toString())) {// 密级不相同
			if (isPublic == 0) {// 秘密
				// "密级由秘密更改为公开"
				strBuffer.append(JecnTaskCommon.CHANGE_SECRET_TO_PUBLIC).append("");
			} else {// 公开
				// "密级由公开更改为秘密"
				strBuffer.append(JecnTaskCommon.CHANGE_PUBLIC_TO_SECRET).append("");
			}
		}
	}

	/**
	 * 获取类别变动
	 * 
	 * @param submitMessage
	 * @param strBuffer
	 * @param typeId
	 * @param strType
	 */
	private void getFileType(SimpleSubmitMessage submitMessage, StringBuffer strBuffer, Long typeId, String strType) {
		if (JecnCommon.isNullOrEmtryTrim(submitMessage.getPrfTypeName())) {// 类别不存在，默认名称为‘无’
			submitMessage.setPrfTypeName(JecnTaskCommon.TASK_NOTHIN);
		}
		if (typeId == null && (!JecnCommon.isNullOrEmtryTrim(strType) && !"-1".equals(strType))) {
			// 获取变动信息
			strBuffer.append(
			// "类别由(" **************** ")更改为("
					JecnTaskCommon.TYPE_FROM + JecnTaskCommon.TASK_NOTHIN + JecnTaskCommon.TO_TYPE
							+ submitMessage.getPrfTypeName()).append(")");
		} else if (strType != null && typeId != null) {
			// 获取Prf类别
			List<ProceeRuleTypeBean> listTypeBean = null;
			if (!strType.equals(typeId.toString())) {
				// 当前流程类别
				String curTypeName = JecnTaskCommon.TASK_NOTHIN;
				listTypeBean = getProceeRuleTypeBean();
				if (listTypeBean == null || listTypeBean.size() == 0) {
					return;
				}
				for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
					if (typeId.equals(proceeRuleTypeBean.getTypeId())) {
						curTypeName = proceeRuleTypeBean.getTypeName();
						break;
					}
				}
				// 获取变动信息
				strBuffer.append(
				// "类别由(" curTypeName ")更改为("submitMessage.getPrfTypeName()
						JecnTaskCommon.TYPE_FROM + curTypeName + JecnTaskCommon.TO_TYPE
								+ submitMessage.getPrfTypeName()).append(")");
			}
		}
	}

	/**
	 * 获取查阅权限变动
	 * 
	 * @param submitMessage
	 *            页面提交信息
	 * @param taskType
	 *            任务类型
	 * @param rifId
	 *            关联ID
	 * @param strBuffer
	 *            返回变动信息
	 * @throws Exception
	 */
	private void getAccessPermiss(SimpleSubmitMessage submitMessage, int taskType, Long rifId, StringBuffer strBuffer)
			throws Exception {
		int accessType = -1;
		switch (taskType) {
		case 0:
		case 4:
			accessType = 0;
			break;
		case 1:// 文件
			accessType = 1;
			break;
		case 2:// 制度
		case 3:
			accessType = 3;
			break;
		default:
			break;
		}
		/** **************查阅权限变更************************ */
		// true 存在变动
		boolean isOrg = false;
		boolean isPos = false;
		boolean isGroup = false;
		// 部门查阅权限
		isOrg = orgOrPosAccessPermiss(submitMessage, accessType, rifId, strBuffer, 0);
		// 岗位查阅权限
		isPos = orgOrPosAccessPermiss(submitMessage, accessType, rifId, strBuffer, 1);
		// 岗位组查阅权限

		isGroup = orgOrPosAccessPermiss(submitMessage, accessType, rifId, strBuffer, 2);

		if (isOrg || isPos || isGroup) {// 是否存在变动
			JecnTempAccessBean accessBean = new JecnTempAccessBean();
			accessBean.setOrgIds(submitMessage.getOrgIds());
			accessBean.setPosIds(submitMessage.getPosIds());
			// 设置岗位组属性
			accessBean.setPosGroupIds(submitMessage.getGroupIds());
			accessBean.setRelateId(rifId);
			accessBean.setSave(false);
			accessBean.setType(accessType);
			// 更新查阅权限
			updateAccessPermissionsWeb(accessBean);
		}
	}

	/**
	 * 部门查阅权限
	 * 
	 * @param submitMessage
	 *            提交信息
	 * @param accessType
	 *            查阅权限类别
	 * @param relateId
	 *            关联ID
	 * @param stringBuffer
	 *            返回的错误信息
	 * @throws Exception
	 */
	private boolean orgOrPosAccessPermiss(SimpleSubmitMessage submitMessage, int accessType, Long relateId,
			StringBuffer strBuffer, int type) throws Exception {
		// 当前部门查阅权限
		List<Long> listOrgIds = new ArrayList<Long>();
		// StrorgIds 转List
		List<Long> listStrOrgIds = new ArrayList<Long>();
		STR_SPLIT = "/";
		String tempIDs = "";
		if (type == 0) {// 组织
			tempIDs = submitMessage.getOrgIds();
		} else if (type == 2) {
			tempIDs = submitMessage.getGroupIds();

		} else {
			tempIDs = submitMessage.getPosIds();
		}

		if (!JecnCommon.isNullOrEmtryTrim(tempIDs)) {
			// 把拼装的权限ID以数组获取
			String[] strOrg = tempIDs.split(",");
			for (int i = 0; i < strOrg.length; i++) {
				String string = strOrg[i];
				listStrOrgIds.add(Long.valueOf(string.trim()));
			}
		}
		StringBuffer orgBufIds = new StringBuffer();
		StringBuffer orgBufNames = new StringBuffer();
		if (type == 0) {// 部门
			// 部门权限IDs
			getOrgIds(orgBufIds, orgBufNames, relateId, accessType);
		} else if (type == 2) {
			// 岗位组权限IDs
			getGroupIds(orgBufIds, orgBufNames, relateId, accessType);
		} else {
			// 岗位权限IDs
			getPosIds(orgBufIds, orgBufNames, relateId, accessType);
		}

		if (orgBufIds.length() > 0) {
			// 把拼装的权限ID以数组获取
			String[] strOrg = orgBufIds.toString().split(",");
			for (int i = 0; i < strOrg.length; i++) {
				String string = strOrg[i];
				listOrgIds.add(Long.valueOf(string.trim()));
			}
		}
		// 验证两个Long型数组是否相同false 不同
		boolean isSameList = isSameList(listStrOrgIds, listOrgIds);
		if (!isSameList) {// 存在不同的数据集
			// 当前部门权限
			String orgCur = orgBufNames.toString();
			if (JecnCommon.isNullOrEmtryTrim(orgCur)) {
				orgCur = JecnTaskCommon.TASK_NOTHIN;
			}
			// 提交审批后查阅权限
			String orgStr = "";
			if (type == 0) {
				orgStr = submitMessage.getOrgNames();
			} else if (type == 2) {
				orgStr = submitMessage.getGroupNames();
			} else {
				orgStr = submitMessage.getPosNames();
			}
			if (JecnCommon.isNullOrEmtryTrim(orgStr)) {
				orgStr = JecnTaskCommon.TASK_NOTHIN;
			} else {
				orgStr = orgStr.replaceAll("", "/");
			}
			if (type == 0) {
				// "部门权限由(" ********** ")更改为("
				strBuffer.append(JecnTaskCommon.ROG_FROM + orgCur + JecnTaskCommon.TO_TYPE + orgStr + ")");

			} else if (type == 2) {
				// //"岗位组权限由(" ********** ")更改为("
				strBuffer.append(JecnTaskCommon.GRO_FROM + orgCur + JecnTaskCommon.TO_TYPE + orgStr + ")");

			} else {
				// //"部门权限由(" ********** ")更改为("
				strBuffer.append(JecnTaskCommon.POS_FROM + orgCur + JecnTaskCommon.TO_TYPE + orgStr + ")");
			}
			return true;
		}
		return false;
	}

	/**
	 * 验证两个Long型数组是否相同
	 * 
	 * @param list1
	 *            List<Long>
	 * @param list2
	 *            List<Long>
	 * @return true 相同，false 不同
	 */
	private boolean isSameList(List<Long> list1, List<Long> list2) {
		if (list1 == null || list2 == null) {
			throw new NullPointerException("数据处理异常！isSameList中数组不能为空");
		}
		if (list1.size() != list2.size()) {
			return false;
		} else {
			int count = 0;
			for (Long long1 : list1) {
				for (Long long2 : list2) {
					if (long1.equals(long2)) {
						count++;
					}
				}
			}
			if (count != list1.size()) {// 存在不同的数据
				return false;
			}
		}
		return true;

	}

	public enum TaskOperationEunm {
		reSubmit, // 重新提交
		viewTask, // 查看
		approve,
		// 审批
		auditControl
	}

	/**
	 * 获得审批记录
	 * 
	 * @param jecnTaskBeanNew
	 * @param toPeopleId
	 * @param opinion
	 * @param sort
	 * @param taskFixedDesc
	 * @return
	 */
	private JecnTaskForRecodeNew getJecnTaskForRecodeNew(JecnTaskBeanNew jecnTaskBeanNew, Long toPeopleId,
			String opinion, int sort, String taskFixedDesc) {
		/** 添加审批记录 */
		JecnTaskForRecodeNew jecnTaskForRecodeNew = new JecnTaskForRecodeNew();
		/** 驳回次数 */
		jecnTaskForRecodeNew.setApproveNoCounts(jecnTaskBeanNew.getApproveNoCounts());
		/** 评审次数 */
		jecnTaskForRecodeNew.setRevirewCounts(jecnTaskBeanNew.getRevirewCounts());
		/** 创建人 */
		jecnTaskForRecodeNew.setCreatePersonId(jecnTaskBeanNew.getCreatePersonId());
		/** 创建时间 */
		jecnTaskForRecodeNew.setCreateTime(jecnTaskBeanNew.getCreateTime());
		/** 开始时间 */
		jecnTaskForRecodeNew.setStartTime(jecnTaskBeanNew.getStartTime());
		/** 结束时间 */
		jecnTaskForRecodeNew.setEndTime(jecnTaskBeanNew.getEndTime());
		/** 源人(操作人) */
		jecnTaskForRecodeNew.setFromPeopleId(jecnTaskBeanNew.getFromPeopleId());
		/** 更新时间 */
		jecnTaskForRecodeNew.setUpdateTime(jecnTaskBeanNew.getUpdateTime());
		/** 目标人 */
		jecnTaskForRecodeNew.setToPeopleId(toPeopleId);
		/** 是否解锁 */
		jecnTaskForRecodeNew.setIsLock(jecnTaskBeanNew.getIsLock());
		/** 说明 */
		jecnTaskForRecodeNew.setOpinion(opinion);

		jecnTaskForRecodeNew.setSortId(sort);
		/** 当前阶段 */
		jecnTaskForRecodeNew.setState(jecnTaskBeanNew.getState());
		/** 上个阶段 */
		jecnTaskForRecodeNew.setUpState(jecnTaskBeanNew.getUpState());
		/** 当前操作状态 */
		jecnTaskForRecodeNew.setTaskElseState(jecnTaskBeanNew.getTaskElseState());
		/** 变更说明 */
		jecnTaskForRecodeNew.setTaskFixedDesc(taskFixedDesc);
		/** 任务Id */
		jecnTaskForRecodeNew.setTaskId(jecnTaskBeanNew.getId());
		return jecnTaskForRecodeNew;
	}

	/**
	 * 拟稿人是否存在验证
	 * 
	 * @param createPeopleId
	 *            拟稿人（创建人peopleId）
	 * @throws DaoException
	 */
	private void checkCreateUser(Long createPeopleId) throws Exception {
		if (createPeopleId == null) {
			log.error("JecnAbstractTaskBS类审批人为空方法中参数为空!");
			throw new TaskIllegalArgumentException(JecnTaskCommon.STAFF_CHANGE, -1);
		}
		// // 验证创建人是否离职或者没有岗位
		// Boolean createUserIsExist =
		// personDao.isPeopleAndPosAllExists(createPeopleId);
		// if (!createUserIsExist) {
		// log.error("JecnAbstractTaskBS类callBackTask方法中参数为空!");
		// throw new
		// TaskIllegalArgumentException(JecnTaskCommon.STAFF_TURNOVER_OR_NO_JOB,
		// 6);
		// }
	}

	/**
	 * 根据获取的人员主键ID更新日志信息及目标操作人信息
	 * 
	 * @param toPeopleId
	 *            目标人主键ID
	 * @param taskId
	 *            任务Id
	 * @param jecnTaskBeanNew
	 *            任务主表记录
	 * @param opinion
	 *            提交意见
	 * @param taskFixedDesc
	 *            审批变动
	 * @param listJecnTaskPeopleNew
	 *            目标操作人记录集合
	 * @param listJecnTaskForRecodeNew
	 *            日志信息集合
	 * @param sort
	 *            当前任务日志的数据记录
	 * @return sort
	 */
	private int updateTaskPeopleNew(Long toPeopleId, Long taskId, JecnTaskBeanNew jecnTaskBeanNew, String opinion,
			String taskFixedDesc, int sort) {
		// 记录当前操作人信息
		JecnTaskForRecodeNew jecnTaskForRecodeNew = this.getJecnTaskForRecodeNew(jecnTaskBeanNew, toPeopleId, opinion,
				sort, taskFixedDesc);
		Session s = abstractTaskDao.getSession();
		s.save(jecnTaskForRecodeNew);
		/** 更新目标人 */
		JecnTaskPeopleNew jecnTaskPeopleNew = new JecnTaskPeopleNew();
		jecnTaskPeopleNew.setApprovePid(toPeopleId);
		jecnTaskPeopleNew.setTaskId(taskId);
		if (jecnTaskBeanNew.getTaskElseState() == 2 || jecnTaskBeanNew.getTaskElseState() == 3
				|| jecnTaskBeanNew.getTaskElseState() == 15) {
			jecnTaskPeopleNew.setRecordId(jecnTaskForRecodeNew.getId());
		}
		s.save(jecnTaskPeopleNew);
		// 人员ID对应的代办ID
		jecnTaskBeanNew.getHandlerPeopleMap().put(toPeopleId, jecnTaskPeopleNew.getId());
		return sort;
	}

	/**
	 * 发送任务待办
	 * 
	 * @param jecnTaskForRecodeNew
	 * @param jecnTaskBeanNew
	 * @param personDao
	 * @throws Exception
	 */
	protected void sendTaskInfo(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		if (!JecnConfigTool.sendTaskByInterface() || beanNew.getTaskElseState() == 11
				|| beanNew.getTaskElseState() == 12 || beanNew.getTaskElseState() == 13) {// 非代办，并且非撤回操作
			return;
		}

		JecnSendTaskTOMainSystemFactory.INSTANCE.sendTask(beanNew, personDao, handlerPeopleIds, curPeopleId);
	}

	/**
	 * 撤回操作
	 * 
	 * @param beanNew
	 * @param personDao
	 * @param handlerPeopleIds
	 * @param cancelPeoples
	 * @throws Exception
	 */
	private void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		if (!JecnConfigTool.sendTaskByInterface()) {// 非代办，并且非撤回操作
			return;
		}
		JecnSendTaskTOMainSystemFactory.INSTANCE.sendInfoCancels(beanNew, personDao, handlerPeopleIds, cancelPeoples,
				curPeopleId);
	}

	/**
	 * 审批通过
	 * 
	 * @param submitMessage
	 *            页面提交的内容
	 * @param jecnTaskBeanNew
	 *            任务
	 * @param taskStageList
	 *            任务对应的阶段信息
	 * @throws Exception
	 */
	private void approvePass(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {

		// 是否为自定义阶段
		boolean isUserDefindState = isUserDefindState(jecnTaskBeanNew.getState());
		if (isUserDefindState) {
			approvePassByMuliPeople(submitMessage, jecnTaskBeanNew, taskStageList);
		} else {
			approvePassBySinglePeople(submitMessage, jecnTaskBeanNew, taskStageList);
		}

	}

	/**
	 * 是否为自定义阶段
	 * 
	 * @param state
	 * @return
	 */
	private boolean isUserDefindState(Integer state) {
		if (state >= 11 && state <= 14) {
			return true;
		}
		return false;
	}

	/**
	 * 多个审批人的阶段的通过
	 * 
	 * @param submitMessage
	 * @param jecnTaskBeanNew
	 * @param taskStageList
	 * @throws Exception
	 */
	private void approvePassByMuliPeople(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {

		// 操作人ID
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		// 审批意见
		String opinion = submitMessage.getOpinion();
		// 任务主键ID
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		// 审批变动
		String taskFixedDesc = JecnTaskCommon.TASK_NOTHIN;
		// 当前阶段
		int curState = jecnTaskBeanNew.getState();

		// 删除待审批人表中的当前人员
		deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);
		boolean isExistApprovePeoples = isExistApprovePeoples(taskId);
		// 是否存完成（没有下个阶段）
		boolean isFinished = curStateIsLastState(taskStageList, curState);
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		if (!isExistApprovePeoples && isFinished) {// 没有审批人，以及没有下个阶段，那么就是完成
			finishTaskMulti(submitMessage, jecnTaskBeanNew, taskStageList);
		} else {
			if (!isExistApprovePeoples) {// 本阶段只剩下当前个审批人，提交后应到下一审批环节
				jecnTaskBeanNew.setTaskElseState(1);
				// 下个阶段的审批人
				Set<Long> peoples = this.getNextUserIdsApprove(curState, taskStageList);
				// 设置上次任务阶段
				jecnTaskBeanNew.setUpState(curState);
				jecnTaskBeanNew.setState(JecnTaskCommon.getNextTaskStage(taskStageList, curState).getStageMark());
				// 记录当前阶段名称
				setMarkName(jecnTaskBeanNew, taskStageList);

				setTaskEdit(jecnTaskBeanNew, submitMessage);

				// 更新日志
				updateToPeopleAndForRecord(peoples, jecnTaskBeanNew, opinion, taskFixedDesc, curPeopleId);
				// 更新当前任务
				abstractTaskDao.update(jecnTaskBeanNew);
				abstractTaskDao.getSession().flush();
				abstractTaskDao.getSession().evict(jecnTaskBeanNew);

				// 发送邮件
				sendTaskEmail(peoples, jecnTaskBeanNew);
			} else {// 阶段不变仍然在当前阶段
				jecnTaskBeanNew.setUpState(curState);
				jecnTaskBeanNew.setState(curState);
				jecnTaskBeanNew.setUpdateTime(new Date());
				jecnTaskBeanNew.setTaskElseState(1);
				// 更新当前任务
				abstractTaskDao.update(jecnTaskBeanNew);
				abstractTaskDao.getSession().flush();
				abstractTaskDao.getSession().evict(jecnTaskBeanNew);

				int sort = abstractTaskDao.getCurTaskRecordCount(taskId);
				Set<Long> peoples = getRemainApprovePeoples(jecnTaskBeanNew.getId(), curPeopleId);
				for (Long toPeopleId : peoples) {
					sort++;
					// 获取任务日志信息
					JecnTaskForRecodeNew jecnTaskForRecodeNew = this.getJecnTaskForRecodeNew(jecnTaskBeanNew,
							toPeopleId, opinion, sort, JecnTaskCommon.TASK_NOTHIN);
					abstractTaskDao.getSession().save(jecnTaskForRecodeNew);
				}
				// 发送代办信息
				sendTaskInfo(jecnTaskBeanNew, personDao, null, curPeopleId);

			}
		}
	}

	/**
	 * 获得剩下的审批人
	 * 
	 * @param taskId
	 * @return
	 */
	private Set<Long> getRemainApprovePeoples(Long taskId, Long... exceptIds) {
		String sql = "select APPROVE_PID from JECN_TASK_PEOPLE_NEW where TASK_ID=?";
		List<Long> ids = abstractTaskDao.listObjectNativeSql(sql, "APPROVE_PID", Hibernate.LONG, taskId);
		if (exceptIds != null && exceptIds.length > 0) {
			for (Long id : exceptIds) {
				ids.remove(id);
			}
		}
		return new HashSet<Long>(ids);
	}

	private boolean isExistApprovePeoples(Long taskId) throws Exception {
		// 相当于查询当前阶段是否还有其它审批人员
		List<JecnTaskPeopleNew> listPeopleNew = getJecnTaskPeopleNewList(taskId);
		return listPeopleNew.size() > 0 ? true : false;
	}

	/**
	 * 当前阶段是否为最后一个阶段
	 * 
	 * @param taskStageList
	 * @param state
	 * @return
	 */
	private boolean curStateIsLastState(List<JecnTaskStage> taskStageList, Integer state) {
		return JecnTaskCommon.getNextTaskStage(taskStageList, state) == null ? true : false;
	}

	/**
	 * 单个审批人的阶段的通过
	 * 
	 * @param submitMessage
	 * @param jecnTaskBeanNew
	 * @param taskStageList
	 * @throws Exception
	 */
	private void approvePassBySinglePeople(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {
		// 操作人ID
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		// 审批意见
		String opinion = submitMessage.getOpinion();
		Long taskId = jecnTaskBeanNew.getId();
		// 审批变动
		String taskFixedDesc = JecnTaskCommon.TASK_NOTHIN;
		// 获得下一级审批人
		Set<Long> peopleList = new HashSet<Long>();
		if (jecnTaskBeanNew.getState() == 10 && jecnTaskBeanNew.getTaskElseState() == 9) {// 打回整理后，拟稿人提交操作
			int curState = 0;
			peopleList.add(jecnTaskBeanNew.getFromPeopleId());
			curState = jecnTaskBeanNew.getUpState();
			jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
			jecnTaskBeanNew.setState(curState);
			// 记录当前阶段名称
			setMarkName(jecnTaskBeanNew, taskStageList);
		} else {
			// 获得下一级审批人
			peopleList = this.getNextUserIdsApprove(jecnTaskBeanNew.getState(), taskStageList);
			if (peopleList.size() > 0) {
				JecnTaskStage taskStage = JecnTaskCommon.getNextTaskStage(taskStageList, jecnTaskBeanNew.getState());
				jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
				jecnTaskBeanNew.setState(taskStage.getStageMark());
				// 记录当前阶段名称
				setMarkName(jecnTaskBeanNew, taskStageList);
			}
		}
		if (peopleList.size() == 0) {// 审批完成 无下级审批人
			// 任务审批完成
			finishTask(submitMessage, jecnTaskBeanNew);
		} else {// 审批通过
			/** 更新任务基本信息 */
			jecnTaskBeanNew.setUpdateTime(new Date());
			jecnTaskBeanNew.setFromPeopleId(curPeopleId);
			jecnTaskBeanNew.setTaskElseState(1);
			// 删除当前阶任务操作人
			deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);
			taskFixedDesc = getTaskFixedDesc(submitMessage, jecnTaskBeanNew);

			// 记录当前阶段名称
			setMarkName(jecnTaskBeanNew, taskStageList);

			// 获取目标人集合 并保存流程记录
			updateToPeopleAndForRecord(peopleList, jecnTaskBeanNew, opinion, taskFixedDesc, curPeopleId);

			setTaskEdit(jecnTaskBeanNew, submitMessage);
			// 更新任务
			abstractTaskDao.save(jecnTaskBeanNew);

			// 获取人员信息，发送邮件
			if (peopleList != null && peopleList.size() > 0) {
				sendTaskEmail(peopleList, jecnTaskBeanNew);
			}
		}
	}

	/**
	 * 任务审批完成
	 * 
	 * @param submitMessage
	 *            提交的基本信息
	 * @param jecnTaskBeanNew
	 *            任务详细信息
	 * @throws Exception
	 */
	private void finishTask(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		// 操作人ID
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		// 审批意见
		String opinion = submitMessage.getOpinion();
		Long taskId = jecnTaskBeanNew.getId();
		// 记录当前操作
		jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
		// 获取审批变动结果
		String taskFixedDesc = getTaskFixedDesc(submitMessage, jecnTaskBeanNew);
		/** 更新任务基本信息 */
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		jecnTaskBeanNew.setTaskElseState(1);
		jecnTaskBeanNew.setImplementDate(submitMessage.getImplementDate() == null ? new Date() : submitMessage
				.getImplementDate());
		// 删除当前阶任务操作人
		deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);

		/** 记录排序号 */
		int sort = getCurSortRecordCount(taskId) + 1;
		/** 记录 */
		JecnTaskForRecodeNew jecnTaskForRecodeNew = this.getJecnTaskForRecodeNew(jecnTaskBeanNew, curPeopleId, opinion,
				sort, taskFixedDesc);
		// 记录任务流转记录信息
		abstractTaskDao.getSession().save(jecnTaskForRecodeNew);

		jecnTaskBeanNew.setState(5);
		// 更新任务
		abstractTaskDao.update(jecnTaskBeanNew);

		// 获取文控记录
		JecnTaskHistoryNew historyNew = getJecnTaskHistoryNew(jecnTaskBeanNew);
		if (historyNew == null) {
			log.error("finishTask 任务发布，获取流程记录异常！historyNew =null");
			throw new IllegalArgumentException("finishTask任务发布，获取流程记录异常！historyNew =null");
		}
		// 发送取消代办，给所有人
		List<JecnTaskHistoryFollow> peoples = new ArrayList<JecnTaskHistoryFollow>();
		peoples = historyNew.getListJecnTaskHistoryFollow();
		for (JecnTaskHistoryFollow peopleFollow : peoples) {
			String idString = peopleFollow.getApproveId();
			String ids[] = idString.split(",");
			for (int n = 0; n < ids.length; n++) {
				sendTaskInfo(jecnTaskBeanNew, personDao, null, Long.valueOf(ids[n]));
			}
		}
		// sendTaskInfo(jecnTaskBeanNew, personDao, null, curPeopleId);
		// 发布任务
		releaseTask(historyNew, jecnTaskBeanNew);
	}

	public void releaseTask(JecnTaskHistoryNew historyNew, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		int approveType = jecnTaskBeanNew.getApproveType(); // 获取 任务 类型 0 审批 1

		updateTaskApprovalView(Long.valueOf(jecnTaskBeanNew.getId()));
		if (approveType == 1) {// 借阅
		} else if (approveType == 2) {// 废止
			abolish(jecnTaskBeanNew.getRid(), jecnTaskBeanNew.getTaskType(), jecnTaskBeanNew.getId());
		} else {
			// 发布PRF文件记录文控信息
			JecnTaskCommon.saveJecnTaskHistoryNew(historyNew, docControlDao, structureDao, ruleDao, null, fileDao,
					configItemDao, basicInfoDao, processKPIDao, processRecordDao);
		}
	}

	private String findPdir(Long id, int type) throws Exception {
		List<Long> rIds = new ArrayList<Long>();
		rIds.add(id);
		String dir = "";
		Map<Long, String> nodeParentPath = null;
		switch (type) {
		case 0:
			nodeParentPath = flowStructureService.getNodeParentById(rIds, true, TreeNodeType.process);
			break;
		case 1:
			nodeParentPath = flowStructureService.getNodeParentById(rIds, true, TreeNodeType.file);
			break;
		case 2:
			nodeParentPath = flowStructureService.getNodeParentById(rIds, true, TreeNodeType.ruleFile);
			break;
		default:
			break;
		}
		for (Map.Entry<Long, String> entry : nodeParentPath.entrySet()) {
			dir += entry.getValue() == null ? " " : entry.getValue() + " ";
		}
		return dir;
	}

	/**
	 * 
	 * @param listObj
	 * @param mapStrs
	 * @param taskId
	 */
	private void InsertRevocationRecords(List<Object[]> listObj, Map<Long, String> mapStrs, Long taskId) {
		Date abolishDate = new Date();
		JecnAbolishBean aboBean = null;
		for (Object[] obj : listObj) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			aboBean = new JecnAbolishBean();
			String pathDir = "";
			if (obj[4] != null && !"".equals(obj[4].toString())) {
				pathDir = getParentName(obj[4].toString(), mapStrs, Long.valueOf(obj[0].toString()));
			}
			aboBean.setrId(Long.valueOf(obj[0].toString()));
			aboBean.setName(obj[1] == null ? "" : obj[1].toString());
			aboBean.setType(1);
			aboBean.setAbolishDate(abolishDate);
			aboBean.setPath(pathDir);
			aboBean.setTaskId(taskId);
			aboBean.setKeyWord(obj[3] == null ? "" : obj[3].toString());
			aboBean.setFileNum(obj[2] == null ? "" : obj[2].toString());
			this.InsertRevocationRecord(aboBean);
		}
	}

	/**
	 * 插入废止库
	 * 
	 * @param historyNew
	 * @param jecnTaskBeanNew
	 * @param PathDir
	 */
	private void InsertRevocationRecord(JecnAbolishBean jecnAbolishBean) {
		String sql = "insert into jecn_abolish (relationid_id,name,type,abolish_time,dir,task_id,KEYWORD,DOC_ID) "
				+ "values(?,?,?,?,?,?,?,?)";
		abstractTaskDao.execteNative(sql, jecnAbolishBean.getrId(), jecnAbolishBean.getName(), jecnAbolishBean
				.getType(), jecnAbolishBean.getAbolishDate(), jecnAbolishBean.getPath(), jecnAbolishBean.getTaskId(),
				jecnAbolishBean.getKeyWord(), jecnAbolishBean.getFileNum());// 0为文控ID
		// 暂不需要
	}

	private String getParentName(String path, Map<Long, String> parentNames, Long fileId) {
		String[] filePathArr = path.split("-");
		String pNames = "";
		if (filePathArr.length > 1) {
			for (String parentId : filePathArr) {
				if (parentId != null && !"".equals(parentId) && !fileId.toString().equals(parentId)) {
					pNames += parentNames.get(Long.valueOf(parentId)) + "/";
				}
			}
		}
		if (pNames != null && !"".equals(pNames)) {
			pNames = pNames.substring(0, pNames.length() - 1);
		}
		return pNames;
	}

	/**
	 * 废止 审批通过文件 以及相关支撑文件
	 * 
	 * @param rId
	 * @param type
	 * @param taskId
	 */

	private void abolish(Long rId, int type, Long taskId) {
		try {
			if (type == 4) {
				type = 0;
			} else if (type == 3) {
				type = 2;
			}
			String sql = "";
			Object[] obj = null;
			List<Long> rIds = new ArrayList<Long>();
			rIds.add(rId);
			JecnAbolishBean aboBean = new JecnAbolishBean(); // 封装废止对象
			aboBean.setrId(rId);
			aboBean.setType(type);
			aboBean.setAbolishDate(new Date());
			aboBean.setTaskId(taskId);
			switch (type) {
			case 0:
				// 查询流程 获取编号 关键字 是否是流程或架构
				sql = "select t.flow_name,t.isflow, t.keyword,t.flow_id_input from jecn_flow_structure_t"
						+ " t where t.flow_id = ?";
				obj = abstractTaskDao.getObjectNativeSql(sql, rId);
				if (obj != null && obj[1] != null) {
					aboBean.setName(obj[0] == null ? "" : obj[0].toString());
					aboBean.setFileNum(obj[3] == null ? "" : obj[3].toString());
					aboBean.setKeyWord(obj[2] == null ? "" : obj[2].toString());
					aboBean.setPath(findPdir(rId, type));
					int flowType = Integer.valueOf(obj[1].toString()); // 获取流程类型
					if (judgeAbolition("1", flowType == 1 ? "\'processAbolish\'" : "\'processMapAbolish\'")) { // 是否废止
						// 流程相关文件所以父级名称包含自己
						Map<Long, String> mapStrs = JecnDaoUtil.getNodeParentSqlByFlowRelateFiles(rId, abstractTaskDao);

						// 流程相关文件
						sql = "SELECT JF.FILE_ID,JF.FILE_NAME,JF.DOC_ID,JF.KEYWORD,JF.T_PATH FROM JECN_FILE_T JF WHERE JF.FLOW_ID = ? AND JF.IS_DIR=1 AND JF.DEL_STATE = 0";
						List<Object[]> listObj = abstractTaskDao.listNativeSql(sql, rId);

						this.InsertRevocationRecords(listObj, mapStrs, taskId); // 废止相关文件
						// abolish(getFlowSubordinateFiles(rIds), 1);//
						// 查找流程、架构下的相关文件
						// 废止相关文件以及目录
						sql = "update jecn_file_t set del_state=2 where flow_id=?";
						abstractTaskDao.execteNative(sql, rId);
						sql = "update jecn_file set del_state=2 where flow_id=?";
						abstractTaskDao.execteNative(sql, rId);
					}
					abolish(rIds, 0);
				}
				break;
			case 1:
				sql = "select t.file_name,t.keyword,t.doc_id from jecn_file_t t  where t.file_id = ? AND t.DEL_STATE = 0";
				obj = abstractTaskDao.getObjectNativeSql(sql, rId);
				if (obj != null && obj[0] != null) {
					aboBean.setName(obj[0] == null ? "" : obj[0].toString());
					aboBean.setFileNum(obj[2] == null ? "" : obj[2].toString());
					aboBean.setKeyWord(obj[1] == null ? "" : obj[1].toString());
					aboBean.setPath(findPdir(rId, type));
				}
				abolish(rIds, 1);
				break;
			case 2:
				sql = "select t.rule_name,t.keyword,t.rule_number from jecn_rule_t t  where t.id = ? AND t.DEL_STATE = 0";
				obj = abstractTaskDao.getObjectNativeSql(sql, rId);
				if (obj != null && obj[0] != null) {
					aboBean.setName(obj[0] == null ? "" : obj[0].toString());
					aboBean.setFileNum(obj[2] == null ? "" : obj[2].toString());
					aboBean.setKeyWord(obj[1] == null ? "" : obj[1].toString());
					aboBean.setPath(findPdir(rId, type));
				}
				if (judgeAbolition("2", "\'ruleAbolish\'")) {
					// 制度相关文件所以父级名称包含自己
					Map<Long, String> mapStrs = JecnDaoUtil.getNodeParentSqlByRuleRelateFiles(rId, abstractTaskDao);

					// 制度相关文件
					sql = "SELECT JF.FILE_ID,JF.FILE_NAME,JF.DOC_ID,JF.KEYWORD,JF.T_PATH FROM JECN_FILE_T JF WHERE JF.RULE_ID = ? AND JF.IS_DIR=1";
					List<Object[]> listObj = abstractTaskDao.listNativeSql(sql, rId);

					this.InsertRevocationRecords(listObj, mapStrs, taskId); // 废止相关文件
					// abolish(getFlowSubordinateFiles(rIds), 1);//
					// 废止相关文件以及目录
					sql = "update jecn_file_t set del_state=2 where RULE_ID=?";
					abstractTaskDao.execteNative(sql, rId);
					sql = "update jecn_file set del_state=2 where RULE_ID=?";
					abstractTaskDao.execteNative(sql, rId);
					/*
					 * if (taskType == 2) { // 制度模板
					 * abolitionRuleModelFile(rIds); } else { // 制度文件
					 * abolitionRuleStandardizedFile(rIds); }
					 */
				}
				abolish(rIds, 2);
				break;
			default:
				break;
			}
			// 插入废止记录
			InsertRevocationRecord(aboBean);

		} catch (Exception e) {
			log.error("文件废止异常...", e);
		}
	}

	/**
	 * 废止制度文件相关文件
	 * 
	 * @param ruleIds
	 */
	private void abolitionRuleStandardizedFile(List<Long> ruleIds) {
		String idStr = JecnCommonSql.getIds(ruleIds);
		/** 正式表 * */
		String sql = "update jecn_file jf" + "   set jf.del_state = 2" + " where jf.file_id in (select f.file_id"
				+ "                        from RULE_STANDARDIZED_FILE f"
				+ "                       inner join jecn_rule r" + "                          on f.related_id = r.id"
				+ "                      where r.id in" + idStr + "                     )";
		abstractTaskDao.execteNative(sql);
		/** 临时表 * */
		String sqlt = "update jecn_file_t jf" + "   set jf.del_state = 2" + " where jf.file_id in (select f.file_id"
				+ "                        from RULE_STANDARDIZED_FILE_T f"
				+ "                       inner join jecn_rule_t r"
				+ "                          on f.related_id = r.id" + "                      where r.id in" + idStr
				+ "                     )";
		abstractTaskDao.execteNative(sqlt);
	}

	/**
	 * 废止 制度模板相关文件
	 * 
	 * @param ruleIds
	 */
	private void abolitionRuleModelFile(List<Long> ruleIds) {
		String idStr = JecnCommonSql.getIds(ruleIds);

		/** 正式表 * */
		String sql = "update jecn_file jf" + "   set jf.del_state = 2" + " where jf.file_id in(select jf.file_id"
				+ "   from rule_title t, rule_file rf, jecn_rule jr, jecn_file jf" + "  where t.rule_id = jr.id"
				+ "    and t.id = rf.rule_title_id"
				+ "    and rf.rule_file_id = jf.file_id  and jf.del_state <> 2  and jr.id in " + idStr + ")";
		abstractTaskDao.execteNative(sql);

		/** 临时表 * */
		String sqlT = "update jecn_file_t jf" + "   set jf.del_state = 2" + " where jf.file_id in(select jf.file_id"
				+ "   from rule_title_T t, rule_file_T rf, jecn_rule_T jr, jecn_file_T jf"
				+ "  where t.rule_id = jr.id" + "    and t.id = rf.rule_title_id"
				+ "    and rf.rule_file_id = jf.file_id  and jf.del_state <> 2  and jr.id in " + idStr + ")";
		abstractTaskDao.execteNative(sqlT);
	}

	/**
	 * 判断是否废止相关文件
	 * 
	 * @param type
	 * @param mark
	 * @return
	 */
	private boolean judgeAbolition(String type, String mark) {
		type = mark == "\'processMapAbolish\'" ? "0" : type;
		String sql = "select value from jecn_config_item where TYPE_BIG_MODEL = " + type
				+ "  AND TYPE_SMALL_MODEL = 11 AND MARK =" + mark;
		SQLQuery sqlQuery = abstractTaskDao.getSQLQuery(sql);
		List<Object> list = sqlQuery.list();
		if ("1".equals(list.get(0))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 查询流程下的文件的ids，包括文件夹
	 * 
	 * @param ids
	 * @return
	 */
	private List<Long> getRuleFiles(List<Long> id, String tableName) {
		String idStr = JecnCommonSql.getIds(id);
		String sql = "	select C.FILE_ID from JECN_FILE_T C " + "	LEFT JOIN JECN_FILE_T P ON P.T_PATH LIKE C.T_PATH"
				+ JecnCommonSql.getConcatChar() + "'%'" + "	where C.FLOW_ID  =  " + idStr;
		List<Long> idsList = abstractTaskDao.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
		return new ArrayList<Long>(idsList);
	}

	/**
	 * 查询流程下的文件的ids，包括文件夹
	 * 
	 * @param ids
	 * @return
	 */
	private List<Long> getFlowSubordinateFiles(List<Long> id) {
		String idStr = JecnCommonSql.getIds(id);
		String sql = "	select C.FILE_ID from FLOW_STANDARDIZED_FILE_T A , JECN_FILE_T C "
				+ "	LEFT JOIN JECN_FILE_T P ON P.T_PATH LIKE C.T_PATH" + JecnCommonSql.getConcatChar() + "'%'"
				+ "	where A.File_Id = c.file_id  and A.FLOW_ID  =  " + idStr;
		List<Long> idsList = abstractTaskDao.listObjectNativeSql(sql, "FILE_ID", Hibernate.LONG);
		return new ArrayList<Long>(idsList);
	}

	private void abolish(List<Long> ids, Integer fileType) {
		if (ids == null || ids.size() == 0) {
			return;
		}
		String idStr = JecnCommonSql.getIds(ids);
		String sql = "";
		String sqlT = "";
		if (fileType == 0) {
			sql = "UPDATE JECN_FLOW_STRUCTURE SET DEL_STATE=2 WHERE FLOW_ID IN " + idStr;
			sqlT = "UPDATE JECN_FLOW_STRUCTURE_T SET DEL_STATE=2 WHERE FLOW_ID IN " + idStr;
		} else if (fileType == 1) {
			sql = "UPDATE JECN_FILE SET DEL_STATE=2 WHERE FILE_ID IN " + idStr;
			sqlT = "UPDATE JECN_FILE_T SET DEL_STATE=2 WHERE FILE_ID IN " + idStr;
		} else if (fileType == 2) {
			sql = "UPDATE JECN_RULE SET DEL_STATE=2 WHERE ID IN " + idStr;
			sqlT = "UPDATE JECN_RULE_T SET DEL_STATE=2 WHERE ID IN " + idStr;
		}
		abstractTaskDao.execteNative(sql);
		abstractTaskDao.execteNative(sqlT);
	}

	private void updateTaskApprovalView(Long taskId) {
		if (!JecnConfigTool.isGoldenDragonLoginType()) {
			return;
		}
		String sql = "UPDATE TASK_APPROVAL_VIEW SET RELEASE_STATE = 1 WHERE TASK_ID = ?";
		this.abstractTaskDao.execteNative(sql, taskId);
	}

	/**
	 * 任务审批完成
	 * 
	 * @param submitMessage
	 *            提交的基本信息
	 * @param jecnTaskBeanNew
	 *            任务详细信息
	 * @throws Exception
	 */
	private void finishTaskMulti(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {
		// 操作人ID
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		// 审批意见
		String opinion = submitMessage.getOpinion();
		Long taskId = jecnTaskBeanNew.getId();
		// 记录当前操作
		jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
		// 获取审批变动结果
		String taskFixedDesc = getTaskFixedDesc(submitMessage, jecnTaskBeanNew);
		/** 更新任务基本信息 */
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		jecnTaskBeanNew.setTaskElseState(1);
		setTaskEdit(jecnTaskBeanNew, submitMessage);
		// 更新任务
		abstractTaskDao.update(jecnTaskBeanNew);
		/** 记录排序号 */
		int sort = getCurSortRecordCount(taskId) + 1;
		/** 记录 */
		JecnTaskForRecodeNew jecnTaskForRecodeNew = this.getJecnTaskForRecodeNew(jecnTaskBeanNew, curPeopleId, opinion,
				sort, taskFixedDesc);
		// 记录任务流转记录信息
		abstractTaskDao.getSession().save(jecnTaskForRecodeNew);

		if (!isExistApprovePeoples(taskId)) {

			jecnTaskBeanNew.setState(5);
			abstractTaskDao.update(jecnTaskBeanNew);

			// 记录当前阶段名称
			setMarkName(jecnTaskBeanNew, taskStageList);

			// sendTaskInfo(jecnTaskBeanNew, personDao, null, curPeopleId);

			// 获取文控记录
			JecnTaskHistoryNew historyNew = getJecnTaskHistoryNew(jecnTaskBeanNew);
			if (historyNew == null) {
				log.error("finishTask 任务发布，获取流程记录异常！historyNew =null");
				throw new IllegalArgumentException("finishTask任务发布，获取流程记录异常！historyNew =null");
			}
			// 给所有人重置一下代办
			List<JecnTaskHistoryFollow> peoples = new ArrayList<JecnTaskHistoryFollow>();
			peoples = historyNew.getListJecnTaskHistoryFollow();
			for (JecnTaskHistoryFollow peopleFollow : peoples) {
				String idString = peopleFollow.getApproveId();
				String ids[] = idString.split(",");
				for (int n = 0; n < ids.length; n++) {
					sendTaskInfo(jecnTaskBeanNew, personDao, null, Long.valueOf(ids[n]));
				}
			}
			// 发布PRF文件记录文控信息
			JecnTaskCommon.saveJecnTaskHistoryNew(historyNew, docControlDao, structureDao, ruleDao, null, fileDao,
					configItemDao, basicInfoDao, processKPIDao, processRecordDao);
		}
	}

	/**
	 * 获得下一级审批人
	 * 
	 * @param taskId
	 * @param state
	 *            当前审批阶段
	 * @return
	 * @throws Exception
	 */
	private Set<Long> getNextUserIdsApprove(int state, List<JecnTaskStage> taskStageList) throws Exception {
		Set<Long> setUserId = new HashSet<Long>();

		// 任务审批带获取的下一个阶段
		long nextStageId = 0;
		JecnTaskStage taskStage = JecnTaskCommon.getNextTaskStage(taskStageList, state);
		if (taskStage == null) {
			return setUserId;
		}
		// 获取下个审批阶段
		nextStageId = taskStage.getId();
		List<Long> listUserId = abstractTaskDao.getJecnTaskApprovePeopleConnListByTaskIdAndState(nextStageId);
		for (Long userId : listUserId) {
			setUserId.add(userId);
		}
		if (nextStageId != -1 && setUserId.size() == 0) {// 存在下一个审核阶段
			// 人员离职或岗位变更
			throw new TaskIllegalArgumentException(JecnTaskCommon.STAFF_CHANGE, 6);
		}
		return setUserId;
	}

	/**
	 * 
	 * 编辑评审人，评审阶段无审批人，获取评审下一阶段-》整理意见阶段的
	 * 
	 * @param taskBeanNew
	 * @param setUserId
	 * @throws Exception
	 */
	private void getNextPeoples(JecnTaskBeanNew taskBeanNew, Set<Long> setUserId) throws Exception {
		// 整理意见阶段
		taskBeanNew.setState(10);

		taskBeanNew.setUpState(3);
		// 获得任务审批操作人 整理意见阶段审批人默认为任务创建人
		setUserId.add(taskBeanNew.getCreatePersonId());
	}

	/**
	 * 获得当前任务的审批记录的条数
	 * 
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	private int getCurSortRecordCount(Long taskId) throws Exception {
		return abstractTaskDao.getSortRecord(taskId);
	}

	/**
	 * 获取审批记录
	 * 
	 * @param taskBeanNew
	 * @return
	 */
	private JecnTaskHistoryNew getJecnTaskHistoryNew(JecnTaskBeanNew taskBeanNew) {
		if (taskBeanNew == null) {
			log.error("getJecnTaskHistoryNew 审批任务完成，发布相关文件异常！taskBeanNew = " + taskBeanNew);
			return null;
		}

		JecnTaskHistoryNew historyNew = getTaskHistoryNew(taskBeanNew);
		// 获取文控各阶段审批人信息
		List<JecnTaskHistoryFollow> listJecnTaskHistoryFollow = getJecnTaskHistoryFollowList(taskBeanNew.getId(),
				taskBeanNew.getTaskType());
		historyNew.setListJecnTaskHistoryFollow(listJecnTaskHistoryFollow);
		return historyNew;
	}

	public JecnTaskHistoryNew getTaskHistoryNew(JecnTaskBeanNew taskBeanNew) {
		JecnTaskHistoryNew historyNew = new JecnTaskHistoryNew();
		// 获取版本号
		historyNew.setVersionId(taskBeanNew.getHistoryVistion());
		JecnUser jecnUser = getJecnUser(taskBeanNew.getCreatePersonId());
		if (jecnUser == null) {
			log.error("getJecnTaskHistoryNew 审批任务完成，发布相关文件异常！jecnUser = " + jecnUser);
			return null;
		}
		// 拟稿人
		historyNew.setDraftPerson(jecnUser.getTrueName());
		historyNew.setApproveCount(Long.valueOf(taskBeanNew.getApproveNoCounts()));
		historyNew.setEndTime(JecnCommon.getStringbyDate(taskBeanNew.getEndTime()));
		historyNew.setStartTime(JecnCommon.getStringbyDate(taskBeanNew.getStartTime()));
		historyNew.setModifyExplain(taskBeanNew.getTaskDesc());
		historyNew.setPublishDate(new Date());
		historyNew.setTestRunNumber(taskBeanNew.getSendRunTimeValue());
		historyNew.setRelateId(taskBeanNew.getRid());
		historyNew.setType(taskBeanNew.getTaskType());
		historyNew.setTaskId(taskBeanNew.getId());
		historyNew.setImplementDate(taskBeanNew.getImplementDate());
		return historyNew;
	}

	/**
	 * 获取文控各阶段审批人信息
	 * 
	 * @param taskId
	 * @param taskType
	 * @return
	 */
	private List<JecnTaskHistoryFollow> getJecnTaskHistoryFollowList(Long taskId, int taskType) {
		// 获取任务审批日志记录
		List<JecnTaskForRecodeNew> listRecode = null;
		// 任务审批阶段集合
		List<JecnTaskStage> taskStageList = null;
		try {
			// 获取任务审批日志记录,从旧到新
			listRecode = abstractTaskDao.getJecnTaskForRecodeNewListByTaskId(taskId);
			// 任务审批顺序
			taskStageList = abstractTaskDao.getJecnTaskStageList(taskId);
		} catch (Exception e) {
			log.error("获取任务日志记录异常");
		}
		if (taskStageList == null || listRecode == null) {
			log.error("getJecnTaskHistoryFollowList 获取文控各阶段审批人信息异常！taskOrderNew = " + taskStageList + "listRecode = "
					+ listRecode);
			return null;
		}
		List<JecnTaskHistoryFollow> list = new ArrayList<JecnTaskHistoryFollow>();
		if (taskStageList.size() == 0) {
			return list;
		}
		Set<Long> set = null;

		int sort = 0;

		// 获取最近的提交时间
		Date lastDate = null;
		for (int i = 0; i < taskStageList.size(); i++) {
			int state = taskStageList.get(i).getStageMark();
			set = new HashSet<Long>();
			for (JecnTaskForRecodeNew forRecodeNew : listRecode) {
				if (forRecodeNew.getTaskElseState() == 7) {
					set.clear();
				}
				// 重新提交之后应该清除过期审批人信息
				if (forRecodeNew.getUpState() == state
						&& (forRecodeNew.getTaskElseState() != 11 && forRecodeNew.getTaskElseState() != 12 && forRecodeNew
								.getTaskElseState() != 13)) {// 编辑人员不记录与撤回不记录
					// 获取源人Id集合
					set.add(forRecodeNew.getFromPeopleId());
					// 获取最近的提交时间
					lastDate = forRecodeNew.getUpdateTime();
				}
			}
			if (set.size() > 0) {
				// 根据任务审批状态获取任务配置对于的mark唯一标识
				String mark = JecnTaskCommon.getItemMark(state);
				if (JecnCommon.isNullOrEmtryTrim(mark)) {
					log.error("JecnTaskCommon.getItemMark(state)异常！");
					return null;
				}
				String hql = "from JecnUser where peopleId in " + JecnCommonSql.getIdsSet(set);
				List<JecnUser> userList = personDao.listHql(hql);
				iflytekUsers(userList);
				JecnTaskHistoryFollow taskHistoryFollow = new JecnTaskHistoryFollow();
				// 任务审批阶段标识
				taskHistoryFollow.setStageMark(state);
				// 审核阶段名称
				taskHistoryFollow.setAppellation(taskStageList.get(i).getStageName());
				taskHistoryFollow.setEn_Appellation(taskStageList.get(i).getEnName());
				taskHistoryFollow.setSort(sort);
				// 记录当前阶段审批人名称
				StringBuffer buffUserName = new StringBuffer();
				StringBuffer buffUserId = new StringBuffer();
				for (int j = 0; j < userList.size(); j++) {
					JecnUser jecnUser = userList.get(j);
					if (j == userList.size() - 1) {
						buffUserName.append(jecnUser.getTrueName());
						buffUserId.append(jecnUser.getPeopleId());
					} else {
						buffUserName.append(jecnUser.getTrueName()).append("/");
						buffUserId.append(jecnUser.getPeopleId()).append(",");
					}
				}
				// 更新时间(操作时间)
				taskHistoryFollow.setApprovalTime(lastDate);
				taskHistoryFollow.setName(buffUserName.toString());
				taskHistoryFollow.setApproveId(buffUserId.toString());
				sort++;
				list.add(taskHistoryFollow);
			}
		}
		return list;
	}

	private void iflytekUsers(List<JecnUser> users) {
		if (!JecnConfigTool.isIflytekLogin()) {
			return;
		}
		List<String> loginNames = new ArrayList<String>();
		for (JecnUser jecnUser : users) {
			loginNames.add(jecnUser.getLoginName());
		}
		if (loginNames.isEmpty()) {
			return;
		}
		Map<String, String> userMap = JecnIflytekSyncUserEnum.INSTANCE.getMapUserName(loginNames);
		for (JecnUser jecnUser : users) {
			String trueName = userMap.get(jecnUser.getLoginName());
			jecnUser.setTrueName(StringUtils.isBlank(trueName) ? jecnUser.getTrueName() : trueName);
		}
	}

	/**
	 * 转批或交办
	 * 
	 * @param submitMessage
	 *            页面提交的信息
	 * @param taskOperation
	 *            用户操作： 2交办 3转批
	 * @param taskStageList
	 *            任务对应的阶段信息
	 * @throws Exception
	 */
	private void assignedTask(SimpleSubmitMessage submitMessage, int taskOperation, List<JecnTaskStage> taskStageList)
			throws Exception {
		if (JecnCommon.isNullOrEmtryTrim(submitMessage.getCurPeopleId())
				|| JecnCommon.isNullOrEmtryTrim(submitMessage.getUserAssignedId())) {
			throw new TaskIllegalArgumentException("转批或交办处理异常！", -1);
		}
		// 任务主键ID
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		/** 任务基本信息 */
		JecnTaskBeanNew jecnTaskBeanNew = getJecnTaskBeanNew(taskId);
		// 检验转批或者交办人是否已经存在于当前审批人中
		checkAssignPeopleIsExistInTaskApprove(Long.valueOf(submitMessage.getUserAssignedId()), jecnTaskBeanNew);

		// 操作人ID
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());

		/** 通过userId集合找到peopleId */
		/** 更新任务基本信息 */
		jecnTaskBeanNew.setUpdateTime(new Date());
		if (isUserDefindState(jecnTaskBeanNew.getState())) {// 自定义审批人阶段不能撤回
			jecnTaskBeanNew.setFromPeopleId(null);
		} else {
			jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		}
		jecnTaskBeanNew.setTaskElseState(taskOperation);
		jecnTaskBeanNew.setState(jecnTaskBeanNew.getState());
		jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
		setTaskEdit(jecnTaskBeanNew, submitMessage);
		abstractTaskDao.update(jecnTaskBeanNew);
		abstractTaskDao.getSession().flush();

		abstractTaskDao.getSession().evict(jecnTaskBeanNew);
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);// 日志中还需要来源人

		// 记录当前阶段名称
		setMarkName(jecnTaskBeanNew, taskStageList);
		// 转批或交办处理
		updateAssianedAndTransfer(taskId, submitMessage, jecnTaskBeanNew);
	}

	/**
	 * 转批或交办处理
	 * 
	 * @param taskId
	 * @param simpleSubmitMessage
	 * @param jecnTaskBeanNew
	 * @throws BsException
	 */
	private void updateAssianedAndTransfer(Long taskId, SimpleSubmitMessage submitMessage,
			JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		// 转批人
		Long userAssignedId = Long.valueOf(submitMessage.getUserAssignedId());
		// 验证人员是否存在
		isExitPeople(userAssignedId);
		// 审批意见
		String opinion = submitMessage.getOpinion();
		// 记录排序号
		int sort = getCurSortRecordCount(taskId) + 1;
		// 删除已经审批的目标人
		deleteJecnTaskPeopleNew(Long.valueOf(submitMessage.getCurPeopleId()), jecnTaskBeanNew);
		// 获取审批变动
		String taskFixedDesc = getTaskFixedDesc(submitMessage, jecnTaskBeanNew);

		Set<Long> handlerPeopleIds = new HashSet<Long>();
		handlerPeopleIds.add(userAssignedId);

		// 根据获取的人员主键ID更新日志信息及目标操作人信息
		updateTaskPeopleNew(userAssignedId, taskId, jecnTaskBeanNew, opinion, taskFixedDesc, sort);

		// TODO 发送代办信息
		sendTaskInfo(jecnTaskBeanNew, personDao, handlerPeopleIds, Long.valueOf(submitMessage.getCurPeopleId()));
		// 获取人员信息，发送邮件
		if (userAssignedId != null) {
			Set<Long> peopleList = new HashSet<Long>();
			peopleList.add(userAssignedId);
			// 获取人员信息，发送邮件
			sendTaskEmail(peopleList, jecnTaskBeanNew);
		}
	}

	/**
	 * 删除已经审批的目标人
	 * 
	 * @param taskId
	 *            任务主键ID
	 */
	private void deleteJecnTaskPeopleNew(Long curPeopleId, JecnTaskBeanNew taskBeanNew) {
		Long taskId = taskBeanNew.getId();
		// 记录删除的代办ID，集成代办需根据当前代办ID把代办转为已办
		String hql = "SELECT id from JecnTaskPeopleNew where taskId=? and approvePid=?";
		List<Long> curHandlerIds = abstractTaskDao.listHql(hql, taskId, curPeopleId);

		taskBeanNew.getCanclePeopleMap().put(curPeopleId, (curHandlerIds.get(0)));

		// 删除已经审批的目标人
		hql = "delete from JecnTaskPeopleNew where approvePid=? and taskId=?";
		abstractTaskDao.execteHql(hql, curPeopleId, taskId);
	}

	/**
	 * 验证人员是否存在
	 * 
	 * @param peopleId
	 * @throws DaoException
	 */
	private void isExitPeople(Long peopleId) throws Exception {
		if (peopleId == null) {
			return;
		}
		// 验证转批人或交办人是否存在
		int count = abstractTaskDao.isExitPeople(peopleId);
		if (count == 0) {
			log.error("JecnAbstractTaskBS类审批人为空方法中参数为空!");
			throw new TaskIllegalArgumentException(JecnTaskCommon.STAFF_TURNOVER_OR_NO_JOB, 6);
		}
	}

	/**
	 * 获取人员主键ID集合 发送邮件
	 * 
	 * @param peopleList
	 * @param jecnTaskBeanNew
	 * @throws Exception
	 */
	private void sendTaskEmail(Set<Long> peopleList, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		if (peopleList == null || peopleList.size() == 0) {
			return;
		}
		// 任务审批是否需要发送邮件
		boolean isTrue = JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.emailApprover.toString(), JecnTaskCommon
				.selectMessageAndEmailItemBean(configItemDao));
		if (isTrue) {// 发送邮件通知下级审批人
			// 获取任务创建人名称
			JecnUser createJecnUser = personDao.get(jecnTaskBeanNew.getCreatePersonId());
			jecnTaskBeanNew.setCreatePersonTemporaryName(createJecnUser.getTrueName());
			// Object[] 0:内外网；1：邮件地址；2：人员主键ID
			List<JecnUser> userObject = personDao.getJecnUserList(peopleList);
			if (!userObject.isEmpty()) {
				TaskEmailParams emailParams = null;
				if (jecnTaskBeanNew.getTaskType() == 0) {
					emailParams = ProcessDownDataUtil.getTaskEmailParams(false, jecnTaskBeanNew.getRid(), structureDao,
							basicInfoDao);
				}
				// 发送邮件 0为下个阶段审批人 1为拟稿人
				BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_APPROVE);
				emailBuilder.setData(userObject, jecnTaskBeanNew, 0, emailParams);
				List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
				JecnUtil.saveEmail(buildEmail);

			}
		}
		// 任务审批是否发送消息
		boolean isAppMesTrue = JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.mesApprover.toString(), JecnTaskCommon
				.selectMessageAndEmailItemBean(configItemDao));
		if (isAppMesTrue) {// 任务审批通过发送消息同事下级审批人
			JecnTaskCommon.createTaskMessage(peopleList, jecnTaskBeanNew.getTaskType(), jecnTaskBeanNew.getTaskName(),
					this.abstractTaskDao.getSession(), 1);
		}

		// 是否给拟稿人发送邮件
		boolean isDraf = JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.emailDraftPeople.toString(), JecnTaskCommon
				.selectMessageAndEmailItemBean(configItemDao));
		if (isDraf) {// 发送邮件给拟稿人
			peopleList.clear();
			peopleList.add(jecnTaskBeanNew.getCreatePersonId());
			// 获取任务创建人名称
			JecnUser createJecnUser = personDao.get(jecnTaskBeanNew.getCreatePersonId());
			jecnTaskBeanNew.setCreatePersonTemporaryName(createJecnUser.getTrueName());
			// Object[] 0:内外网；1：邮件地址；2：人员主键ID
			List<JecnUser> userObject = personDao.getJecnUserList(peopleList);
			if (!userObject.isEmpty()) {
				TaskEmailParams emailParams = null;
				if (jecnTaskBeanNew.getTaskType() == 0) {
					emailParams = ProcessDownDataUtil.getTaskEmailParams(false, jecnTaskBeanNew.getRid(), structureDao,
							basicInfoDao);
				}
				BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_APPROVE);
				// 发送邮件 0为下个阶段审批人 1为拟稿人
				emailBuilder.setData(userObject, jecnTaskBeanNew, 1, emailParams);
				List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
				JecnUtil.saveEmail(buildEmail);
			}

		}

		// 是否给拟稿人发送消息
		boolean isDrafMesTrue = JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.mesDraftPeople.toString(),
				JecnTaskCommon.selectMessageAndEmailItemBean(configItemDao));
		if (isDrafMesTrue) {// 任务审批通过发送消息同事下级审批人
			peopleList.clear();
			peopleList.add(jecnTaskBeanNew.getCreatePersonId());
			JecnTaskCommon.createTaskMessage(peopleList, jecnTaskBeanNew.getTaskType(), jecnTaskBeanNew.getTaskName(),
					this.abstractTaskDao.getSession(), 1);
		}
	}

	private void finishSendEmail(Set<Long> peopleList, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		// 获取任务创建人名称
		JecnUser createJecnUser = personDao.get(jecnTaskBeanNew.getCreatePersonId());
		jecnTaskBeanNew.setCreatePersonTemporaryName(createJecnUser.getTrueName());
		// Object[] 0:内外网；1：邮件地址；2：人员主键ID
		List<JecnUser> userObject = personDao.getJecnUserList(peopleList);
		if (!userObject.isEmpty()) {
			TaskEmailParams emailParams = null;
			if (jecnTaskBeanNew.getTaskType() == 0) {
				emailParams = ProcessDownDataUtil.getTaskEmailParams(true, jecnTaskBeanNew.getRid(), structureDao,
						basicInfoDao);
			}
			BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_FINISH);
			emailBuilder.setData(userObject, jecnTaskBeanNew, emailParams);
			List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
			JecnUtil.saveEmail(buildEmail);
		}

	}

	/**
	 * 打回操作
	 * 
	 * @param submitMessage
	 *            页面提交的信息
	 * @param taskStageList
	 *            任务对应的阶段信息
	 * @throws Exception
	 */
	private void callBackTask(SimpleSubmitMessage submitMessage, List<JecnTaskStage> taskStageList) throws Exception {
		if (submitMessage == null) {
			log.error("JecnAbstractTaskBS类callBackTask方法中参数为空!");
			throw new TaskIllegalArgumentException("参数不能为空!", -1);
		}
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		// 操作人ID
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		// 审批意见
		String opinion = submitMessage.getOpinion();
		// 审批变动
		String taskFixedDesc = JecnTaskCommon.TASK_NOTHIN;
		/** 任务基本信息 */
		JecnTaskBeanNew jecnTaskBeanNew = getJecnTaskBeanNew(taskId);
		int approveNoCounts = jecnTaskBeanNew.getApproveNoCounts() + 1;
		/** 打回次数 */
		jecnTaskBeanNew.setApproveNoCounts(approveNoCounts);
		/** 审批次数 */
		jecnTaskBeanNew.setRevirewCounts(0);
		/** 更新任务基本信息 */
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		// 打回
		jecnTaskBeanNew.setTaskElseState(4);
		// 拟稿人是否存在
		checkCreateUser(jecnTaskBeanNew.getCreatePersonId());

		// 记录当前阶段名称
		setMarkName(jecnTaskBeanNew, taskStageList);

		// 待删除目标人集合
		List<Long> cancelPeoples = getCancelPeoples(jecnTaskBeanNew);

		// 删除任务的所有审批人
		deleteJecnTaskPeopleNew(taskId);

		/** 更新目标人 */
		JecnTaskPeopleNew jecnTaskPeopleNew = new JecnTaskPeopleNew();
		jecnTaskPeopleNew.setApprovePid(jecnTaskBeanNew.getCreatePersonId());
		jecnTaskPeopleNew.setTaskId(taskId);

		abstractTaskDao.getSession().save(jecnTaskPeopleNew);

		// 人员ID对应的代办ID
		jecnTaskBeanNew.getHandlerPeopleMap().put(jecnTaskBeanNew.getCreatePersonId(), jecnTaskPeopleNew.getId());

		// 记录排序号
		int sort = getCurSortRecordCount(taskId) + 1;
		// 获取审批变动结果
		taskFixedDesc = getTaskFixedDesc(submitMessage, jecnTaskBeanNew);

		jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
		jecnTaskBeanNew.setState(0);

		// 任务审批流转记录
		JecnTaskForRecodeNew jecnTaskForRecodeNew = this.getJecnTaskForRecodeNew(jecnTaskBeanNew, jecnTaskBeanNew
				.getCreatePersonId(), opinion, sort, taskFixedDesc);
		// 保存任务流转记录
		abstractTaskDao.getSession().save(jecnTaskForRecodeNew);

		setTaskEdit(jecnTaskBeanNew, submitMessage);
		// 更新任务
		abstractTaskDao.update(jecnTaskBeanNew);

		// 获取人员信息，发送邮件
		if (jecnTaskBeanNew.getCreatePersonId() != null) {
			Set<Long> peopleList = new HashSet<Long>();
			peopleList.add(jecnTaskBeanNew.getCreatePersonId());
			// 获取人员信息，发送邮件
			sendTaskEmail(peopleList, jecnTaskBeanNew);
		}
		Set<Long> handlerPeopleIds = new HashSet<Long>();
		handlerPeopleIds.add(jecnTaskBeanNew.getCreatePersonId());
		sendInfoCancels(jecnTaskBeanNew, personDao, handlerPeopleIds, cancelPeoples, curPeopleId);
	}

	/**
	 * 删除该任务的所有审批人
	 * 
	 * @param taskId
	 *            任务主键ID
	 */
	private void deleteJecnTaskPeopleNew(Long taskId) {
		// 删除已经审批的目标人
		String hql = "delete from JecnTaskPeopleNew where taskId=?";
		abstractTaskDao.execteHql(hql, taskId);
	}

	/**
	 * 获取审批变动结果
	 * 
	 * @param simpleTaskBean
	 * @param jecnTaskBeanNew
	 * @return String 审批变动结果
	 * @throws Exception
	 */
	private String getTaskFixedDesc(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew)
			throws Exception {

		// 获取审批变动 非4.0版本同时是文控或者部门审核人提交
		if (!submitMessage.isVersion4() && (jecnTaskBeanNew.getUpState() == 1 || jecnTaskBeanNew.getUpState() == 2)) {
			switch (jecnTaskBeanNew.getTaskType()) {
			case 0:// 流程
			case 4:// 流程地图
				// 获取审批变动
				return updateAssembledFlowTaskFixed(submitMessage, jecnTaskBeanNew);
			case 1:// 文件
				// 获取审批变动
				return updateAssembledFileTaskFixed(submitMessage, jecnTaskBeanNew);
			case 2:// 制度模板文件
			case 3:// 制度文件
				// 获取审批变动
				return updateAssembledRuleTaskFixed(submitMessage, jecnTaskBeanNew);

			}
		}
		return JecnTaskCommon.TASK_NOTHIN;
	}

	/**
	 * 打回整理意见
	 * 
	 * @param submitMessage
	 *            页面提交的信息
	 * @param taskStageList
	 *            任务对应的阶段信息
	 * @throws Exception
	 */
	private void callBackArrangement(SimpleSubmitMessage submitMessage, List<JecnTaskStage> taskStageList)
			throws Exception {
		Long taskId = Long.valueOf(submitMessage.getTaskId());
		// 当前操作人
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		/** 任务基本信息 */
		JecnTaskBeanNew jecnTaskBeanNew = getJecnTaskBeanNew(taskId);
		// 拟稿人是否存在
		checkCreateUser(jecnTaskBeanNew.getCreatePersonId());
		// 更新任务基本信息
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setFromPeopleId(curPeopleId);
		jecnTaskBeanNew.setTaskElseState(9);
		jecnTaskBeanNew.setUpState(jecnTaskBeanNew.getState());
		// 整理意见
		jecnTaskBeanNew.setState(10);
		setTaskEdit(jecnTaskBeanNew, submitMessage);
		abstractTaskDao.update(jecnTaskBeanNew);

		// 记录当前阶段名称
		setMarkName(jecnTaskBeanNew, taskStageList);
		// 删除当前操作目标人
		deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);
		// 获取目标人集合
		Set<Long> setPeopleId = new HashSet<Long>();
		setPeopleId.add(jecnTaskBeanNew.getCreatePersonId());
		// 获取审批变动结果
		String taskFixedDesc = getTaskFixedDesc(submitMessage, jecnTaskBeanNew);
		updateToPeopleAndForRecord(setPeopleId, jecnTaskBeanNew, submitMessage.getOpinion(), taskFixedDesc, curPeopleId);
		// 获取人员信息，发送邮件
		if (jecnTaskBeanNew.getCreatePersonId() != null) {
			Set<Long> peopleList = new HashSet<Long>();
			peopleList.add(jecnTaskBeanNew.getCreatePersonId());
			// 获取人员信息，发送邮件
			sendTaskEmail(peopleList, jecnTaskBeanNew);
		}
	}

	/**
	 * 管理员编辑任务
	 * 
	 * @param submitMessage
	 *            页面提交的信息
	 * @param taskBeanNew
	 *            任务
	 * @throws Exception
	 */
	private void editTask(SimpleSubmitMessage submitMessage, JecnTaskBeanNew taskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {
		if (submitMessage == null || taskBeanNew == null || submitMessage.getListAuditPeople() == null) {
			throw new NullPointerException("编辑获取提交信息异常！");
		}
		// 获取任务主键ID
		Long taskId = taskBeanNew.getId();
		// 当前操作状态 11：提交；12：重新提交
		int curTaskElse = Integer.valueOf(submitMessage.getTaskOperation());
		// 当前操作人
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
		// 更新时间
		taskBeanNew.setUpdateTime(new Date());

		Long createPeopleId = null;
		if (submitMessage.getListAuditPeople().size() > 0) {// 获取创建人
			createPeopleId = Long.valueOf(submitMessage.getListAuditPeople().get(0).getAuditId().trim());
		}
		if (createPeopleId != null && !createPeopleId.equals(taskBeanNew.getCreatePersonId())) {
			// 验证人员是否存在
			isExitPeople(createPeopleId);
			// 更新拟稿人
			taskBeanNew.setCreatePersonId(createPeopleId);
		}
		// 目标人集合
		Set<Long> setPeopleIds = new HashSet<Long>();
		if (curTaskElse == 11) {// 任务管理 ,编辑任务提交
			// 获取待更新目标人和审批人
			changeAuditPeople(taskId, submitMessage.getListAuditPeople(), taskBeanNew, setPeopleIds);
			// 在转批或者交办时更新任务主表
			changeTaskFromPeopleAndTaskElseState(taskId, submitMessage.getListAuditPeople(), taskBeanNew);
		} else if (curTaskElse == 12 || taskBeanNew.getTaskElseState() == 4) {// 打回到拟稿人
			// 设置操作状态 缺少编辑状态
			if (createPeopleId != null) {
				int approveNoCounts = taskBeanNew.getApproveNoCounts() + 1;
				/** 打回次数 */
				taskBeanNew.setApproveNoCounts(approveNoCounts);
				/** 审批次数 */
				taskBeanNew.setRevirewCounts(0);
				// 打回
				taskBeanNew.setTaskElseState(4);
				taskBeanNew.setUpState(taskBeanNew.getState());
				taskBeanNew.setState(0);
				setPeopleIds.add(createPeopleId);
			}
		}
		setTaskEdit(taskBeanNew, submitMessage);
		// 更新任务主表
		abstractTaskDao.update(taskBeanNew);
		abstractTaskDao.getSession().flush();
		// 不更新剩余的操作
		abstractTaskDao.getSession().evict(taskBeanNew);

		// 更新操作人
		int oldTaskType = taskBeanNew.getTaskElseState();
		// 编辑状态(不保存，更新日志用)
		taskBeanNew.setFromPeopleId(curPeopleId);
		taskBeanNew.setTaskElseState(11);

		// 待删除目标人集合
		List<Long> cancelPeoples = getCancelPeoples(taskBeanNew);

		// 删除目标人
		String hql = "delete from JecnTaskPeopleNew where  taskId=?";
		abstractTaskDao.execteHql(hql, taskId);

		// 获取下个阶段审批人
		if (setPeopleIds.size() > 0) {
			// 根据获取的人员主键ID集合更新日志信息及目标操作人信息
			updateToPeopleAndForRecord(setPeopleIds, taskBeanNew, JecnUtil.nullToEmpty(submitMessage.getOpinion()),
					JecnTaskCommon.TASK_NOTHIN, curPeopleId);
		} else {// 当前阶段无审批人,说明已删除
			log.info("当前阶段审批人不存在！");
			int sort = 0;
			sort = abstractTaskDao.getCurTaskRecordCount(taskId) + 1;
			// 根据获取的人员主键ID更新日志信息及目标操作人信息
			// 记录当前操作人信息
			JecnTaskForRecodeNew jecnTaskForRecodeNew = this.getJecnTaskForRecodeNew(taskBeanNew, -1L, JecnUtil
					.nullToEmpty(submitMessage.getOpinion()), sort, JecnTaskCommon.TASK_NOTHIN);
			Session s = abstractTaskDao.getSession();
			s.save(jecnTaskForRecodeNew);
		}
		taskBeanNew.setTaskElseState(oldTaskType);

		// 记录当前阶段名称
		setMarkName(taskBeanNew, taskStageList);
		// 处理待办接口
		sendInfoCancels(taskBeanNew, personDao, setPeopleIds, cancelPeoples, curPeopleId);
	}

	/**
	 * 当taskElseState为转批或者交办时更新fromPeopleId和将taskElseState 改为通过
	 * 
	 * @author user huoyl
	 * @param taskId
	 *            任务id
	 * @param listTempAuditPeopleBean
	 *            页面提交过来的数据
	 * @param taskBeanNew
	 *            任务信息
	 */
	private void changeTaskFromPeopleAndTaskElseState(Long taskId, List<TempAuditPeopleBean> listTempAuditPeopleBean,
			JecnTaskBeanNew taskBeanNew) {
		// 操作
		int taskElseState = taskBeanNew.getTaskElseState();
		// 当前阶段
		int curState = taskBeanNew.getState();
		// 操作如果是转批或者交办
		if (taskElseState == 2 || taskElseState == 3) {
			List<TempAuditPeopleBean> listAudit = new ArrayList<TempAuditPeopleBean>();
			// 获得上个阶段的审批人
			for (TempAuditPeopleBean auditPeopleBean : listTempAuditPeopleBean) {
				if (auditPeopleBean == null) {
					continue;
				}

				if (auditPeopleBean.getState() == curState) {
					break;
				}
				listAudit.add(auditPeopleBean);

			}
			if (listAudit.size() > 0) {
				// 由于listAudit是当前阶段之前的阶段的集合，是按照审批顺序排好序的。所以取最后一条。
				TempAuditPeopleBean tempAuditPeopleBean = listAudit.get(listAudit.size() - 1);
				String auditId = tempAuditPeopleBean.getAuditId();
				if (StringUtils.isBlank(auditId)) {
					log.error("管理员编辑任务，操作室转批或者交办的时候上个阶段的审批人为空");
					return;
				}
				// 上个阶段是会审阶段，审批人员按照逗号分隔
				if (tempAuditPeopleBean.getState() == 3) {
					auditId = auditId.split(",")[0];
				}

				// 获得上个阶段操作人id
				// 当评审人会审阶段时审批人为多个所以取其中一个就行（会审人员没有撤回操作）其他阶段审批人只有一个
				taskElseState = 1;
				String sql = "update jecn_task_bean_new set from_person_id=?,task_else_state=? where id=? ";
				abstractTaskDao.execteNative(sql, auditId, taskElseState, taskId);
			}
		}
	}

	/**
	 * 获取待更新目标人和审批人
	 * 
	 * @param taskId
	 * @param listTempAuditPeopleBean
	 * @throws Exception
	 */
	private void changeAuditPeople(Long taskId, List<TempAuditPeopleBean> listTempAuditPeopleBean,
			JecnTaskBeanNew taskBeanNew, Set<Long> setPeopleIds) throws Exception {
		if (listTempAuditPeopleBean == null || listTempAuditPeopleBean.size() == 0 || taskId == null) {
			throw new NullPointerException("changeAuditPeople任务编辑获取待更新目标人和审批人异常！");
		}
		// 当前任务阶段
		int curState = taskBeanNew.getState();
		try {
			// 判断当前是否是整理意见阶段是的话判断评审人会审是否为空
			if (curState == 10) {
				// 当前阶段为整理意见阶段
				// 所以当前阶段为整理意见这个阶段页面没有审批人的数据，后台处理的时候不会将数据加入setPeopleIds。需要自己将拟稿人作为审批人加入。
				if (curState == 10) {
					setPeopleIds.add(taskBeanNew.getCreatePersonId());
				}
				// 是否有会审人
				boolean hasReviewPeople = false;
				for (TempAuditPeopleBean auditPeopleBean : listTempAuditPeopleBean) {
					if (auditPeopleBean == null) {
						continue;
					}
					if (auditPeopleBean.getState() == 3 && !JecnCommon.isNullOrEmtryTrim(auditPeopleBean.getAuditId())) {
						hasReviewPeople = true;
					}
				}
				if (!hasReviewPeople) {
					throw new TaskIllegalArgumentException("当前阶段为整理意见阶段，评审人会审阶段审批人不能为空", 2);
				}
			}
		} catch (Exception e) {
			throw e;
		}

		// 获取任务目标人集合
		String hql = "select approvePid from JecnTaskPeopleNew where taskId=?";
		List<Long> approveList = abstractTaskDao.listHql(hql, taskId);

		long curStageId = -1;
		List<Integer> stateList = new ArrayList<Integer>();

		// 获取完整的state数据
		for (TempAuditPeopleBean auditPeopleBean : listTempAuditPeopleBean) {
			if (auditPeopleBean != null && !JecnCommon.isNullOrEmtryTrim(auditPeopleBean.getAuditId())) {
				if (auditPeopleBean.getState() == curState) {
					curStageId = auditPeopleBean.getStageId();
				}
				// 会审
				if (auditPeopleBean.getState() == 3 && !JecnCommon.isNullOrEmtryTrim(auditPeopleBean.getAuditId())) {
					stateList.add(10);
				}
				stateList.add(auditPeopleBean.getState());
			}
		}

		// 更新此次提交的state的isSelectedUser为已选择状态
		if (stateList.size() > 0) {
			StringBuffer strBuf = new StringBuffer();
			for (Integer state : stateList) {
				strBuf.append(state + ",");
			}
			String stateStr = strBuf.substring(0, strBuf.length() - 1);

			hql = "update  JecnTaskStage set isSelectedUser=1 where taskId=? and stageMark in (" + stateStr + ")";
			abstractTaskDao.execteHql(hql, taskId);
			hql = "update  JecnTaskStage set isSelectedUser=0 where taskId=? and stageMark not in (" + stateStr + ")";
			abstractTaskDao.execteHql(hql, taskId);
		}

		// 获取当前阶段审批人
		hql = "select approvePid from JecnTaskApprovePeopleConn where stageId=?";
		List<Long> curAppIdList = abstractTaskDao.listHql(hql, curStageId);
		// 删除所有任务阶段
		abstractTaskDao.deleteJecnTaskPeopleConnByTaskId(taskId);

		// 修改后当前阶段审批人
		List<Long> editAuditPeople = new ArrayList<Long>();
		/** 当前审核阶段是否存在审核人 true:不存在；false 存在 */
		boolean isCurAudit = false;
		for (TempAuditPeopleBean auditPeopleBean : listTempAuditPeopleBean) {
			if (auditPeopleBean == null) {
				continue;
			}
			String auditId = auditPeopleBean.getAuditId();
			if (JecnTaskCommon.idIsNotNull(auditId)) {// 当前审批阶段不存在审核人
				if (auditPeopleBean.getIsEmpty() == 1) {// 必填项不存在值
					// 当前审批阶段审批人不能为空!
					throw new TaskIllegalArgumentException(JecnTaskCommon.CUR_APP_PEOPLE_NOT_NULL, -1);
				} else if (curState == auditPeopleBean.getState()) {
					// 当前阶段为非必填，编辑后删除当前审批阶段
					isCurAudit = true;
					continue;
				} else {
					continue;
				}
			}
			String[] str = auditId.split(",");
			for (int i = 0; i < str.length; i++) {
				JecnTaskApprovePeopleConn approvePeopleConn = new JecnTaskApprovePeopleConn();
				approvePeopleConn.setApprovePid(Long.valueOf(str[i].trim()));
				approvePeopleConn.setStageId(auditPeopleBean.getStageId());
				abstractTaskDao.getSession().save(approvePeopleConn);
				if (curState == auditPeopleBean.getState()) {
					editAuditPeople.add(approvePeopleConn.getApprovePid());
				}
			}
		}

		// 执行sql 获取任务各阶段审批人数据
		abstractTaskDao.getSession().flush();

		if (isCurAudit) {// 当前阶段为非必填，编辑后删除当前审批阶段
			getNextAuditPeople(taskBeanNew, listTempAuditPeopleBean, setPeopleIds);
		} else if (editAuditPeople.size() >= 1 && taskBeanNew.getState() == 3) {// 验证当前阶段审核人是否还能继续审批
			// 去掉已审批的记录 (编辑前审批人 集合 - 目标人集合 = 已审批的人集合) 3
			curAppIdList.removeAll(approveList);

			// 已经审批的人在编辑后当前的审批阶段存在的记录
			List<Long> sameList = new ArrayList<Long>();
			for (Long long1 : editAuditPeople) {
				for (Long long2 : curAppIdList) {
					if (long1.equals(long2)) {
						sameList.add(long1);
					}
				}
			}
			// 去掉编辑后已审批的人 3 /5/admin
			editAuditPeople.removeAll(sameList);

			// 删除不存在的目标人集合
			hql = "delete from JecnTaskPeopleNew where taskId=?";
			abstractTaskDao.execteHql(hql, taskId);

			setPeopleIds.addAll(editAuditPeople);
			if (setPeopleIds.size() == 0) {// 评审人不存在，当前阶段未审批完成，跳转到下一个审批阶段
				// 获得下一级审批人
				this.getNextPeoples(taskBeanNew, setPeopleIds);
			}
		} else {
			setPeopleIds.addAll(editAuditPeople);
		}
	}

	/**
	 * 编辑后删除当前审批阶段,获取下一阶段审批人(可填可不填情况)
	 * 
	 * @param taskId
	 * @throws Exception
	 */
	private void getNextAuditPeople(JecnTaskBeanNew taskBeanNew, List<TempAuditPeopleBean> listTempAuditPeopleBean,
			Set<Long> setPeopleIds) throws Exception {
		if (taskBeanNew == null || listTempAuditPeopleBean == null) {
			return;
		}
		// 当前任务阶段
		int curState = taskBeanNew.getState();
		// 是否存在当前审批阶段
		boolean curAuditState = false;
		// 获取下一阶段审批人
		int nextState = -1;
		for (TempAuditPeopleBean tempAuditPeopleBean : listTempAuditPeopleBean) {
			if (tempAuditPeopleBean == null) {
				continue;
			}
			if (curState == tempAuditPeopleBean.getState()) {//
				curAuditState = true;
			} else if (curAuditState) {// 如果
				int state = tempAuditPeopleBean.getState();
				// 更新审批状态
				taskBeanNew.setState(state);
				String auditIds = tempAuditPeopleBean.getAuditId();
				if (!JecnTaskCommon.idIsNotNull(auditIds)) {// 如果存在审核人，则当前阶段为当前审批阶段
					// 获取split‘，’ 后setPeopleIds集合
					getSplitAudits(setPeopleIds, auditIds);
					nextState = state;
					break;
				} else {
					nextState = -1;
				}
			}
		}
		if (nextState == -1) {// 找不到下一级审批人，任务审批完成
			// 更新审批状态
			taskBeanNew.setState(5);
		}
	}

	/**
	 * 获取split‘，’ 后setPeopleIds集合
	 * 
	 * @param setPeopleIds
	 *            返回解析后人员ID集合
	 * @param auditIds
	 *            ‘，’ 拼装的ID字符串
	 */
	private void getSplitAudits(Set<Long> setPeopleIds, String auditIds) {
		if (setPeopleIds == null || JecnCommon.isNullOrEmtryTrim(auditIds)) {
			return;
		}
		String[] strArray = auditIds.split(",");
		for (String string : strArray) {
			setPeopleIds.add(Long.valueOf(string.trim()));
		}
	}

	/**
	 * 文控审核主导审批
	 * 
	 * @param submitMessage
	 * @throws Exception
	 */
	private void controlPass(SimpleSubmitMessage submitMessage, JecnTaskBeanNew jecnTaskBeanNew,
			List<JecnTaskStage> taskStageList) throws Exception {
		if (submitMessage == null || jecnTaskBeanNew == null || submitMessage.getListAuditPeople() == null) {
			throw new NullPointerException("文控审核人主导审批异常！submitMessage==null || jecnTaskBeanNew==null");
		}
		// 任务不存在或已删除
		if (jecnTaskBeanNew == null || jecnTaskBeanNew.getId() == null) {
			// 任务审批异常
			throw new TaskIllegalArgumentException(JecnTaskCommon.APPROVE_ERROR, -1);
		}
		// 文控审核人操作前的审批顺序
		int stateCount = 0;
		boolean startLookUp = false;
		// 获取下个审批阶段(文控审核人的下一个阶段)
		int nextState = -1;
		for (TempAuditPeopleBean auditPeopleBean : submitMessage.getListAuditPeople()) {
			if (auditPeopleBean == null || JecnCommon.isNullOrEmtryTrim(auditPeopleBean.getAuditId())) {
				continue;
			}
			if (auditPeopleBean.getState() == 1) {
				startLookUp = true;
				continue;
			}
			if (startLookUp) {
				stateCount++;
				nextState = auditPeopleBean.getState();
				break;
			}
		}
		// 不存在其他阶段审批 文控审核人直接点击审批
		if (stateCount == 0) {
			// 文控主导不存在其他审核人 任务审批完成
			finishTask(submitMessage, jecnTaskBeanNew);
		} else {// 文控主导审批：文控审核人选择其他阶段审批人
			controlToOtherPeoples(submitMessage, nextState, jecnTaskBeanNew, taskStageList);
		}
	}

	/**
	 * 文控主导审批：文控审核人选择其他阶段审批人
	 * 
	 * @param simpleTaskBean
	 * @param submitMessage
	 * @param nextState
	 * @param jecnTaskBeanNew
	 * @param taskStageList
	 * @throws Exception
	 */
	/**
	 * @author weidp
	 * @date 2014-8-6 下午02:09:44
	 * @param submitMessage
	 * @param nextState
	 * @param jecnTaskBeanNew
	 * @param taskStageList
	 * @throws Exception
	 */
	private void controlToOtherPeoples(SimpleSubmitMessage submitMessage, int nextState,
			JecnTaskBeanNew jecnTaskBeanNew, List<JecnTaskStage> taskStageList) throws Exception {
		// 获取目标人集合
		Set<Long> peopleList = new HashSet<Long>();
		// 获取下一个阶段
		nextState = getControlNextStatePeople(nextState, submitMessage.getListAuditPeople(), taskStageList, peopleList);
		// // 当前操作人
		Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());

		if (peopleList != null && peopleList.size() > 0) {
			// 更新时间
			jecnTaskBeanNew.setUpdateTime(new Date());
			// 源操作人
			jecnTaskBeanNew.setFromPeopleId(curPeopleId);
			// 操作状态 1：通过
			jecnTaskBeanNew.setTaskElseState(1);
			// 记录上传提交状态 1:文控审核人
			jecnTaskBeanNew.setUpState(1);
			// 更新当前状态
			jecnTaskBeanNew.setState(nextState);
			// 记录当前阶段名称
			setMarkName(jecnTaskBeanNew, taskStageList);
			// 更新任务
			abstractTaskDao.update(jecnTaskBeanNew);
			// 设置文控主导选择的审批人
			saveJecnTaskApprovePeopleConn(submitMessage, jecnTaskBeanNew.getId(), taskStageList);

			// 删除当前阶任务操作人
			deleteJecnTaskPeopleNew(curPeopleId, jecnTaskBeanNew);
			// 获取审批变动结果
			String taskFixedDesc = getTaskFixedDesc(submitMessage, jecnTaskBeanNew);
			// 更新目标人信息和操作人流转记录
			updateToPeopleAndForRecord(peopleList, jecnTaskBeanNew, submitMessage.getOpinion(), taskFixedDesc,
					curPeopleId);

			// 获取人员信息，发送邮件
			if (peopleList.size() > 0) {
				// 获取人员信息，发送邮件
				sendTaskEmail(peopleList, jecnTaskBeanNew);
			}

		} else {
			// 文控主导未选择其他审核人（存在其他审核人阶段） 审批完成
			finishTask(submitMessage, jecnTaskBeanNew);
		}
	}

	/**
	 * 设置文控主导选择的审批人
	 * 
	 * @param simpleReSumbitMessage
	 *            文控主导审批提交选择的其他阶段审批人
	 * @param state
	 *            下一个审批阶段状态
	 * @param taskId
	 *            任务主键ID
	 * @param orderApprove
	 *            审批顺序
	 * @throws Exception
	 */
	private void saveJecnTaskApprovePeopleConn(SimpleSubmitMessage submitMessage, Long taskId,
			List<JecnTaskStage> taskStageList) throws Exception {
		List<TempAuditPeopleBean> listPList = submitMessage.getListAuditPeople();
		// 删除各阶段关系人的数据
		abstractTaskDao.deleteJecnTaskPeopleConnByTaskId(taskId);
		// 获取下一集操作人
		// 所有操作人集合
		if (listPList != null && listPList.size() > 0) {
			Session s = abstractTaskDao.getSession();
			for (TempAuditPeopleBean auditPeopleBean : listPList) {
				if (auditPeopleBean == null || JecnCommon.isNullOrEmtryTrim(auditPeopleBean.getAuditId())) {
					continue;
				}
				String strPeopleIds = auditPeopleBean.getAuditId();
				String[] str = strPeopleIds.split(",");
				// 获取当前阶段
				JecnTaskStage taskStage = JecnTaskCommon.getCurTaskStage(taskStageList, auditPeopleBean.getState());
				taskStage.setIsSelectedUser(1);// 设置已选择人
				for (String string : str) {
					JecnTaskApprovePeopleConn jecnTaskApprovePeopleConn = new JecnTaskApprovePeopleConn();
					// 当前阶段操作人（state）
					jecnTaskApprovePeopleConn.setApprovePid(Long.valueOf(string.trim()));
					// 阶段信息Id
					jecnTaskApprovePeopleConn.setStageId(taskStage.getId());
					s.save(jecnTaskApprovePeopleConn);
				}

				if (taskStage.getStageMark().intValue() == 3) {
					// 获取整理意见Stage
					JecnTaskStage stage = JecnTaskCommon.getCurTaskStage(taskStageList, 10);
					stage.setIsSelectedUser(1);

					JecnTaskApprovePeopleConn jecnTaskApprovePeopleConn = new JecnTaskApprovePeopleConn();
					// 当前阶段操作人（state）
					jecnTaskApprovePeopleConn.setApprovePid(Long.valueOf(listPList.get(0).getAuditId()));
					// 阶段信息Id
					jecnTaskApprovePeopleConn.setStageId(stage.getId());
					s.save(jecnTaskApprovePeopleConn);
				}
			}
		}
	}

	/**
	 * 
	 * 获取文控审核主导审批下一集审批人
	 * 
	 * @param nextStete
	 * @param simpleReSumbitMessage
	 * @return
	 * @throws Exception
	 */
	private int getControlNextStatePeople(int nextState, List<TempAuditPeopleBean> listAuditPeople,
			List<JecnTaskStage> taskStageList, Set<Long> auditPeopleIds) throws Exception {
		if (taskStageList == null || taskStageList.size() == 0 || listAuditPeople == null
				|| listAuditPeople.size() == 0 || nextState == -1) {
			return -1;
		}
		for (TempAuditPeopleBean auditPeopleBean : listAuditPeople) {
			if (auditPeopleBean == null) {// 数组第一个为拟稿人，页面请求不返回
				continue;
			}
			if (auditPeopleBean.getAuditId() == null) {
				continue;
			}
			if (nextState == auditPeopleBean.getState()) {
				// 获取审批人人员ID集合
				auditPeopleIds.addAll(getPeopleIds(auditPeopleBean.getAuditId()));
			}
		}
		if (nextState != -1 && auditPeopleIds.size() == 0) {
			// 获取下一个阶段的Stage
			JecnTaskStage taskStage = JecnTaskCommon.getNextTaskStage(taskStageList, nextState);
			if (taskStage == null) {
				nextState = -1;
			} else {
				nextState = taskStage.getStageMark();
			}
			return getControlNextStatePeople(nextState, listAuditPeople, taskStageList, auditPeopleIds);
		}
		return nextState;
	}

	/**
	 * 获取审批人人员ID集合
	 * 
	 * @param strPeopleIds
	 * @return
	 */
	private Set<Long> getPeopleIds(String strPeopleIds) {
		if (strPeopleIds == null && "".equals(strPeopleIds)) {
			return null;
		}
		Set<Long> peopleds = new HashSet<Long>();
		String[] str = strPeopleIds.split(",");
		for (String id : str) {
			if (!JecnCommon.isNullOrEmtryTrim(id)) {
				peopleds.add(Long.valueOf(id.trim()));
			}
		}
		return peopleds;
	}

	/**
	 * 获取任务显示项和必填项信息
	 * 
	 * @param taskId
	 * @return JecnTaskApplicationNew
	 * @throws Exception
	 */
	public JecnTaskApplicationNew getJecnTaskApplicationNew(Long taskId) throws Exception {
		jecnTaskApplicationNew = abstractTaskDao.getJecnTaskApplicationNew(taskId);
		return jecnTaskApplicationNew;
	}

	/**
	 * 检查是否搁置 如果搁置添加错误信息
	 * 
	 * @param simpleTaskBean
	 * @return
	 * @throws Exception
	 */
	public SimpleTaskBean checkTaskIsDelay(SimpleTaskBean simpleTaskBean) throws Exception {

		boolean isDelay = false;
		String delayReason = "";
		// 阶段名 审批人姓名 岗位名 是否锁定
		String sql = "select ts.stage_name,u.true_name,upr.figure_id,u.islock" + " from jecn_task_stage  ts"
				+ " left join JECN_TASK_PEOPLE_CONN_NEW tocn on ts.id=tocn.stage_id"
				+ " left join jecn_user u on tocn.approve_pid=u.people_id"
				+ " left join jecn_user_position_related upr on u.people_id=upr.people_id"
				+ " where ts.is_show=1 and ts.is_selected_user=1 and ts.task_id=? order by ts.sort";

		List<Object[]> objList = this.abstractTaskDao.listNativeSql(sql, simpleTaskBean.getTaskId());

		StringBuffer delayBuffer = new StringBuffer();
		for (Object[] obj : objList) {
			// 阶段名
			String stageName = obj[0].toString();
			// 审核人姓名为空 搁置
			if (obj[1] == null || (obj[3] != null && Integer.parseInt(obj[3].toString()) == 1)) {
				isDelay = true;
				// xxx:人员离职
				delayBuffer.append(stageName + "：" + JecnUtil.getValue("peopleLeave") + "<br/>");
			} else {
				if (obj[2] == null) {
					if (obj[1] != null) {
						isDelay = true;
						String name = obj[1].toString();
						delayBuffer.append(stageName + "：" + name + JecnUtil.getValue("peopleNoPos") + "<br/>");
					}
				}
			}
		}

		// 当前任务阶段所做的操作
		int taskElseState = simpleTaskBean.getJecnTaskBeanNew().getTaskElseState();
		boolean curPeopleIsNull = false;

		StringBuffer curStateBuf = new StringBuffer();
		// 当前任务为会审阶段或者当前阶段执行交办、转批操作时 查询当前任务操作人数据是否异常
		if (taskElseState == 2 || taskElseState == 3 || stateHasMultiPeople(simpleTaskBean)) {
			sql = "select jtpn.id,u.true_name,jupr.user_id from jecn_task_people_new jtpn" + "  inner join jecn_user u"
					+ "  on jtpn.approve_pid= u.people_id" + " left join jecn_user_position_related jupr"
					+ "  on jupr.people_id=jtpn.approve_pid" + "  where jtpn.task_id=?";
			List<Object[]> curApproveList = this.abstractTaskDao.listNativeSql(sql, simpleTaskBean.getTaskId());

			// 当前阶段无审批人数据
			if (curApproveList == null || curApproveList.size() == 0) {
				isDelay = true;
				curPeopleIsNull = true;
				// 交办人人员缺失！
				if (taskElseState == 2) {
					curStateBuf
							.append(JecnUtil.getValue("assignedPeople") + JecnUtil.getValue("peopleLeave") + "<br/>");
				}
				// 转批人人员缺失！
				else if (taskElseState == 3) {
					curStateBuf.append(JecnUtil.getValue("subconcessionsPeople") + JecnUtil.getValue("peopleLeave")
							+ "<br/>");
				}
				// 当前阶段为评审人会审阶段
				else if (stateHasMultiPeople(simpleTaskBean)) {
					if (delayBuffer.indexOf(simpleTaskBean.getStageName() + "：" + JecnUtil.getValue("peopleLeave")) != -1) {
						curPeopleIsNull = false;
					} else {
						curStateBuf.append(JecnUtil.getValue("peopleLeave") + "<br/>");
					}
				}
			} else {
				// 当前阶段存在审批人 判断有没有岗位
				for (Object[] obj : curApproveList) {
					// 姓名不为空，岗位为空
					if (obj[0] != null && obj[1] != null && obj[2] == null) {
						// 如果user_id 为空说明没有岗位
						curPeopleIsNull = true;
						isDelay = true;
						if (taskElseState == 2) {
							// 交办人xxx没有岗位
							curStateBuf.append(JecnUtil.getValue("assignedPeople") + obj[1].toString()
									+ JecnUtil.getValue("peopleNoPos"));
						} else if (taskElseState == 3) {
							// 转批人xxx没有岗位
							curStateBuf.append(JecnUtil.getValue("subconcessionsPeople") + obj[1].toString()
									+ JecnUtil.getValue("peopleNoPos"));
						}
					}
				}
			}

		}
		// 如果当前阶段审批人为空
		if (curPeopleIsNull && isDelay) {
			// 拼装错误提示
			delayBuffer.append(simpleTaskBean.getStageName() + "：" + curStateBuf.toString());
		}

		delayReason = delayBuffer.toString();
		simpleTaskBean.setDelay(isDelay);
		simpleTaskBean.setDelayReason(delayReason);
		return simpleTaskBean;
	}

	public JecnTaskBeanNew getTaskById(Long taskId) throws Exception {
		return abstractTaskDao.get(taskId);
	}

	public void releaseTaskByWebService(Long taskId, List<JecnTaskHistoryFollow> taskHistorys) throws Exception {
		JecnTaskBeanNew jecnTaskBeanNew = this.getJecnTaskBeanNew(taskId);
		if (jecnTaskBeanNew.getState() == 5) {
			log.info("releaseTaskByWebService() 任务状态已经是完成状态");
			return;
		}
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setTaskElseState(1);
		jecnTaskBeanNew.setState(5);
		jecnTaskBeanNew.setImplementDate(new Date());
		// 删除当前阶任务操作人
		String hql = "delete from JecnTaskPeopleNew where taskId=?";
		abstractTaskDao.execteHql(hql, taskId);
		// 更新任务
		abstractTaskDao.update(jecnTaskBeanNew);
		// 获取文控记录
		JecnTaskHistoryNew historyNew = getTaskHistoryNew(jecnTaskBeanNew);
		if (historyNew == null) {
			log.error("finishTask 任务发布，获取流程记录异常！historyNew =null");
			throw new IllegalArgumentException("finishTask任务发布，获取流程记录异常！historyNew =null");
		}
		historyNew.setListJecnTaskHistoryFollow(taskHistorys);
		// 发布任务
		releaseTask(historyNew, jecnTaskBeanNew);
	}

	/**
	 * 当前阶段是否含有多个审批人
	 * 
	 * @param simpleTaskBean
	 * @return
	 */
	private boolean stateHasMultiPeople(SimpleTaskBean simpleTaskBean) {
		Integer state = simpleTaskBean.getJecnTaskBeanNew().getState();
		// 会审阶段以及自定义阶段
		if (state == 3 || (state >= 9 && state <= 11)) {
			return true;
		}
		return false;
	}

	public MessageResult handleTaskExternal(TaskParam param) {
		log.info("handleTaskExternal请求的参数：" + JecnUtil.getJson(param));
		MessageResult result = new MessageResult();
		if (param.getTaskOp() == -1) {
			JecnTaskBeanNew taskBeanNew = abstractTaskDao.get(Long.valueOf(param.getTaskId()));
			if (taskBeanNew != null) {
				taskBeanNew.setUpdateTime(new Date());
				taskBeanNew.setIsLock(0);
				try {
					JecnUser users = personDao.getJecnUserByLoginName(param.getLoginName());
					if (users != null) {
						taskBeanNew.setFromPeopleId(users.getPeopleId());
					}
				} catch (Exception e) {
					log.error("", e);
					result = new MessageResult(false, "未找到当前处理人：" + param.getLoginName());
					return result;
				}
				abstractTaskDao.update(taskBeanNew);
				abstractTaskDao.getSession().flush();
			}
		} else if (param.getTaskOp() == 5) {
			try {
				releaseTaskByWebService(Long.valueOf(param.getTaskId()), toTaskFollow(param));
			} catch (Exception e) {
				log.error("", e);
				result = new MessageResult(false, "设置任务完成状态失败");
			}
		}
		return result;
	}

	private List<JecnTaskHistoryFollow> toTaskFollow(TaskParam param) {
		List<JecnTaskHistoryFollow> result = new ArrayList<JecnTaskHistoryFollow>();

		if (JecnConfigTool.isJinKe()) {

		} else {
			List<TaskState> states = param.getStates();
			JecnTaskHistoryFollow f = null;
			int i = 0;
			for (TaskState state : states) {
				f = new JecnTaskHistoryFollow();
				f.setAppellation(state.getStateName());
				f.setEn_Appellation(state.getStateNameEn());
				if (StringUtils.isBlank(f.getEn_Appellation())) {
					f.setEn_Appellation(f.getAppellation());
				}
				f.setSort(i++);
				f.setApprovalTime(getMaxApproveTime(state.getPeoples()));
				f.setApproveId(concatIds(state.getPeoples(), ","));
				f.setName(concatNames(state.getPeoples(), "/"));
				result.add(f);
			}
		}
		return result;
	}

	private String concatIds(List<TaskPeople> peoples, String split) {
		if (JecnUtil.isEmpty(peoples)) {
			return null;
		}
		List<String> temp = new ArrayList<String>();
		for (TaskPeople p : peoples) {
			try {
				JecnUser users = personDao.getJecnUserByLoginName(p.getLoginName());
				if (users != null) {
					temp.add(users.getPeopleId().toString());
				}
			} catch (Exception e) {
				log.error("", e);
			}
		}
		return JecnUtil.concat(temp, split);
	}

	private String concatNames(List<TaskPeople> peoples, String split) {
		if (JecnUtil.isEmpty(peoples)) {
			return null;
		}
		List<String> temp = new ArrayList<String>();
		for (TaskPeople p : peoples) {
			temp.add(p.getTrueName());
		}
		return JecnUtil.concat(temp, split);
	}

	private Date getMaxApproveTime(List<TaskPeople> peoples) {
		if (JecnUtil.isEmpty(peoples)) {
			return null;
		}
		Date max = peoples.get(0).getApproveTime();
		for (TaskPeople p : peoples) {
			if (max.getTime() < p.getApproveTime().getTime()) {
				max = p.getApproveTime();
			}
		}
		return max;
	}

	public IOrgAccessPermissionsDao getOrgAccessPermissionsDao() {
		return orgAccessPermissionsDao;
	}

	public void setOrgAccessPermissionsDao(IOrgAccessPermissionsDao orgAccessPermissionsDao) {
		this.orgAccessPermissionsDao = orgAccessPermissionsDao;
	}

	public IPositionAccessPermissionsDao getPositionAccessPermissionsDao() {
		return positionAccessPermissionsDao;
	}

	public void setPositionAccessPermissionsDao(IPositionAccessPermissionsDao positionAccessPermissionsDao) {
		this.positionAccessPermissionsDao = positionAccessPermissionsDao;
	}

	public IJecnConfigItemDao getConfigItemDao() {
		return configItemDao;
	}

	public void setConfigItemDao(IJecnConfigItemDao configItemDao) {
		this.configItemDao = configItemDao;
	}

	public IJecnTaskRecordDao getTaskRecordDao() {
		return taskRecordDao;
	}

	public void setTaskRecordDao(IJecnTaskRecordDao taskRecordDao) {
		this.taskRecordDao = taskRecordDao;
	}

	public IFileDao getFileDao() {
		return fileDao;
	}

	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
	}

	public IFlowStructureDao getStructureDao() {
		return structureDao;
	}

	public void setStructureDao(IFlowStructureDao structureDao) {
		this.structureDao = structureDao;
	}

	public IProcessBasicInfoDao getBasicInfoDao() {
		return basicInfoDao;
	}

	public void setBasicInfoDao(IProcessBasicInfoDao basicInfoDao) {
		this.basicInfoDao = basicInfoDao;
	}

	public IRuleDao getRuleDao() {
		return ruleDao;
	}

	public void setRuleDao(IRuleDao ruleDao) {
		this.ruleDao = ruleDao;
	}

	public IJecnAbstractTaskDao getAbstractTaskDao() {
		return abstractTaskDao;
	}

	public void setAbstractTaskDao(IJecnAbstractTaskDao abstractTaskDao) {
		this.abstractTaskDao = abstractTaskDao;
	}

	public IPersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(IPersonDao personDao) {
		this.personDao = personDao;
	}

	public IJecnDocControlDao getDocControlDao() {
		return docControlDao;
	}

	public void setDocControlDao(IJecnDocControlDao docControlDao) {
		this.docControlDao = docControlDao;
	}

	public IProcessKPIDao getProcessKPIDao() {
		return processKPIDao;
	}

	public void setProcessKPIDao(IProcessKPIDao processKPIDao) {
		this.processKPIDao = processKPIDao;
	}

	public IProcessRecordDao getProcessRecordDao() {
		return processRecordDao;
	}

	public void setProcessRecordDao(IProcessRecordDao processRecordDao) {
		this.processRecordDao = processRecordDao;
	}

}
