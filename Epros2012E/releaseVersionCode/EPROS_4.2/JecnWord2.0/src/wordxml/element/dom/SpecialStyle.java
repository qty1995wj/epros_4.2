package wordxml.element.dom;

import org.dom4j.Element;

import wordxml.constant.Constants.LineRuleType;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphRowIndBean;
import wordxml.element.bean.paragraph.ParagraphSpaceBean;
import wordxml.util.Tools;
import wordxml.util.Unit;

/**
 * 此类为了处理标题样式（style节点），和word 其他的没有关系
 * 
 * @author admin
 * 
 */
public class SpecialStyle {
	/**
	 * 设置段落行距和缩进
	 * 
	 * @param pPr
	 * @param indBean
	 * @param spaceBean
	 */
	static void setSpaceAndInd(Element pPr, ParagraphBean pBean) {
		ParagraphRowIndBean indBean = pBean.getParagraphIndBean();
		ParagraphSpaceBean spaceBean = pBean.getParagraphSpaceBean();
		if (indBean != null) {
			if (pPr.element("ind") != null) {
				pPr.remove(pPr.element("ind"));
			}
			Element ind = pPr.addElement("w:ind");
			// 厘米转缇 左缩进的值为 左缩进加上悬挂缩进的值。
			ind.addAttribute("w:left", Unit.convertCmToTwip(indBean.getLeft()
					+ indBean.getHanging()));
			if (indBean.getRight() != 0) {
				ind.addAttribute("w:right", Unit.convertCmToTwip(indBean
						.getRight()));
			}
			if (indBean.getHanging() != 0) {
				ind.addAttribute("w:hanging", Unit.convertCmToTwip(indBean
						.getHanging()));
			} else {
				ind.addAttribute("w:first-line", Unit.convertCmToTwip(indBean
						.getFirstLine()));
			}

		}
		if (spaceBean != null) {
			if (pPr.element("spacing") != null) {
				pPr.remove(pPr.element("spacing"));
			}
			Element spacing = pPr.addElement("w:spacing");
			spacing.addAttribute("w:line-rule", spaceBean.getRuleType()
					.getValue());
			// 间距Bean 磅转缇 *20 绝对值 最小值
			if (spaceBean.getRuleType() == LineRuleType.EXACT
					|| spaceBean.getRuleType() == LineRuleType.AT_LEAST) {
				spacing.addAttribute("w:line", Unit.convertPtToTwip(spaceBean
						.getLine()));
			} else if (spaceBean.getRuleType() == LineRuleType.AUTO) {
				// 设置为Auto时，值等于line*240
				spacing.addAttribute("w:line", String.valueOf(spaceBean
						.getLine() * 240));
			}
			if (spaceBean.getBeforeLines() == null) {
				spacing.addAttribute("w:before-autospacing", "on");
				spacing.addAttribute("w:before", "100");
			} else {
				// 行转缇 *100
				if (spaceBean.getBeforeLines() != 0 && spaceBean.getType() == 0) {
					spacing.addAttribute("w:before-lines", (int) (spaceBean.getBeforeLines() * 100) + "");
				} else if (spaceBean.getBeforeLines() != 0 && spaceBean.getType() == 1) {
					spacing.addAttribute("w:before", (int) (spaceBean.getBeforeLines() * 20) + "");
				}
			}
			if (spaceBean.getAfterLines() == null) {
				spacing.addAttribute("w:after-autospacing", "on");
				spacing.addAttribute("w:after", "100");
			} else {
				if (spaceBean.getAfterLines() != 0 && spaceBean.getType() == 0) {
					spacing.addAttribute("w:after-lines", (int) (spaceBean.getAfterLines() * 100) + "");
				} else if (spaceBean.getAfterLines() != 0 && spaceBean.getType() == 1) {
					spacing.addAttribute("w:after", (int) (spaceBean.getAfterLines() * 20) + "");
				}
			}

		}
	}

	/**
	 * 设置文本的属性
	 * 
	 * @param fontBean
	 */
	static void setFontAttr(Element rPr, FontBean fontBean) {
		if (rPr == null || fontBean == null) {
			return;
		}
		removeElement(rPr, "rFonts");
		// 字体 fareast 东亚字体
		rPr.addElement("w:rFonts").addAttribute("w:fareast",
				fontBean.getFontFamily());
		removeElement(rPr, "font");
		rPr.addElement("wx:font").addAttribute("wx:val",
				fontBean.getFontFamily());
		// 字体颜色
		removeElement(rPr, "color");
		rPr.addElement("w:color").addAttribute("w:val",
				Tools.convertColorToRGBStr(fontBean.getFontColor()));
		// 字号
		removeElement(rPr, "sz");
		rPr.addElement("w:sz").addAttribute("w:val",
				String.valueOf(fontBean.getFontSize()));
		removeElement(rPr, "sz-cs");
		rPr.addElement("w:sz-cs").addAttribute("w:val",
				String.valueOf(fontBean.getFontSize()));
		// 加粗
		removeElement(rPr, "b");
		removeElement(rPr, "b-cs");
		if (fontBean.isB()) {
			rPr.addElement("w:b");
			rPr.addElement("w:b-cs");
		}
		// 底纹
		if (fontBean.getShdBean() != null) {
			// <w:shd w:val="pct-15" w:color="auto" w:fill="FFFFFF"
			// wx:bgcolor="D9D9D9"/>
			removeElement(rPr, "shd");
			Element shd = rPr.addElement("w:shd");
			shd
					.addAttribute("w:val", fontBean.getShdBean().getVal()
							.getValue());
			shd.addAttribute("w:color", fontBean.getShdBean().getColor());
			shd.addAttribute("w:fill", Tools.convertColorToRGBStr(fontBean
					.getShdBean().getFill()));
		}
	}

	private static void removeElement(Element rPr, String ele) {
		if (rPr.element(ele) != null) {
			rPr.remove(rPr.element(ele));
		}
	}
}
