package com.jecn.epros.server.service.tree;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public interface IJecnTreeService {
	/**
	 * 获取树节点的子节点sort最大值
	 * 
	 * @param treeBean
	 * @return sort+1
	 * @throws Exception
	 */
	public int getTreeChildMaxSortId(JecnTreeBean treeBean) throws Exception;
}
