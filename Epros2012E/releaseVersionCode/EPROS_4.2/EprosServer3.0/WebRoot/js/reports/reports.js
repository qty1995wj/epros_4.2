var reports_index_id='';
function reports() {
	var reportsPanel = new Ext.Panel({
		renderTo : "reports_id",
		id:"reportsPanel_index_id",
		border : false,
		layout : 'border',
		height : Ext.getCmp("main-panel").getInnerHeight(),
		items : [{
			region : 'west',
			border : 0,
			title : '<div id="left_title_id" style="text-align:center;font-weight:bold">我的报表</div>',
			contentEl : 'reportsLeft_id',
			split : true,
			collapseMode : 'mini',
			width : 180
		}, {
			region : 'center',
			border : 0,
			id : 'reportsCenter_id',
			title : i_myReport,
			listeners :{
				'bodyresize':function(p,width,height){
					if(reports_index_id!=""){
						 if(reports_index_id=="reports_processApplication_id"&&Ext.getCmp('processApplicationGridPanel_id')!=null){
							Ext.getCmp('processApplicationGridPanel_id').setSize(width,height-120);
						}else if(reports_index_id=="reports_processInputOutput_id"&&Ext.getCmp('processInputOutputGridPanel_id')!=null){
							Ext.getCmp('processInputOutputGridPanel_id').setSize(width,height-120);
						}else if(reports_index_id=="reports_processAccess_id"&&Ext.getCmp('processAccessGridPanel_id')!=null){
							Ext.getCmp('processAccessGridPanel_id').setSize(width,height-145);
						}else if(reports_index_id=="reports_processInterviewer_id"&&Ext.getCmp('processInterviewerGridPanel_id')!=null){
							Ext.getCmp('processInterviewerGridPanel_id').setSize(width,height-175);
						}else if(reports_index_id=="reports_processDocumentNumber_id"&&Ext.getCmp('processDocumentNumberGridPanel_id')!=null){
							Ext.getCmp('processDocumentNumberGridPanel_id').setSize(width,height-140);
						}else if(reports_index_id=="reports_processDocumentPeople_id"&&Ext.getCmp('processDocumentPeopleGridPanel_id')!=null){
							Ext.getCmp('processDocumentPeopleGridPanel_id').setSize(width,height-170);
						}else if(reports_index_id="reports_taskApproveTime_id"&&Ext.getCmp('taskRepositoryTabPanel_id')!=null){
							Ext.getCmp('taskRepositoryTabPanel_id').setSize(width,height);
						}
					
					}
				}
			}	
		}]
	});
	var reportsSelectedLi;
	$("#reportsUl_id").children().bind("click", function(e) {
				$(reportsSelectedLi).css("background",
				"url(" + basePath + "images/common/libackground.jpg)");
		e.target.style.background = "url(" + basePath
				+ "images/common/libackgroundClicked.jpg)";
		reportsSelectedLi = e.target;
		Ext.getCmp("reportsCenter_id").setTitle(reportsSelectedLi.innerHTML);
		Ext.getCmp("reportsCenter_id").removeAll(true);
		Ext.getCmp("reportsCenter_id").load({
					url : reportsSelectedLi.attributes.id.value.split(';')[0]+'?timsStamp='+getTimeStamp(),
					scripts : true,
					callback : function(el, success, response) {
						if(reportsSelectedLi.attributes.id.value.split(';').length==2){
							eval(reportsSelectedLi.attributes.id.value.split(';')[1])();
						}
					}
				});
	});
}

