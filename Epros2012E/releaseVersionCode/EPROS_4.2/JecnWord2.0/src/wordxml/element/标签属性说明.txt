段落：Defines a paragraph
		element	1.pPr 
					 element 1.pStyle
					 		 2.shd 阴影  属性： val   required shdValues
					 		 				  color optional hexColorType
					 		 				  fill  optional hexColorType
					 		 				  bgcolor	optional
					 		  <w:shd w:val="pct-15" w:color="auto" w:fill="FFFFFF" wx:bgcolor="D9D9D9"/>
					 		  3.keepNext  onOffProperty  默认是on 保持与下一段落选项：防止本段和下之间的分页符
					 		  4.keepLines  onOffProperty  默认是on 代表保持在一起选项，禁止在本段中分页符
					 		  5.pageBreakBefore onOffProperty 代表分页符选项：强制本段前分页符
					 		  6.framePr framePrProperty 代表文本框和下拉框的属性
					 		  7.widowControl   onOffProperty 这个属性和打印有关 
					 		  8.pBdr  element:
					 		  				  1.top   borderProperty  属性：val   borderValues  （single、thick、double、dotted）
	 			              			  			   						color
	 			              			  			   						sz
	 			              			  			   						wx:bdrwidth
	 			              			  			   						shadow 阴影
					 		  	              2.left
					 		  	              3.bottom
					 		  	              4.right
					 		  	              5.between 
					 		  	              
					 		  	              
					 		 3.suppressAutoHyphens 自动段字  onOffProperty 默认是on
					 		 7.spacing 段落间距   
					 		 		      属性    1.before	optional twipsMeasureType
					 		 		        2.after optional twipsMeasureType
					 		 		        3.line  文本行之间的垂直距离 optional signedTwipsMeasureType
					 		 		        
					 		 		        3.before-lines  optional  decimalNumberType
					 		 		        4.after-lines  optional  decimalNumberType
					 		 
					 		 8.ind  段落缩进    属性： 
					 		 				1.left  optional  signedTwipsMeasureType  缇
					 		 				2.right
					 		 				3.hanging
					 		 				4.first-line
					 		 				
					 		 				5.left-chars  optional  decimalNumberType 字符数
					 		 				6.right-chars
					 		 				7.hanging-chars
					 		 				8.first-line-chars
					 		 9.suppressOverlap  指定不允许重叠    onOffProperty  默认是on
					 		 10.jc    属性val required   String  （left center right both  auto）		        
					 		 11.textDirection  段落中文本方向     属性：val   textDirectionValue	 必填	
					 		 12.textAlignment  文本垂直对齐方式  属性：val   textAlignmentValue 必填 （ top center baseline bottom  auto ）
					 		 13.outlineLvl  大纲级别
					 		 
					 		 14.rPr文本属性  element 
					 		 				 1.rStyle <w:rStyle val="1">  stringType
					 		 				 2.rFonts  3980
					 		 				 3.font
					 		 				 4.b  <w:b/>加粗  onOffProperty 默认是on 
					 		 				 5.b-cs   onOffProperty 默认是on
					 		 				 6.i <w:i/> 倾斜   onOffProperty 默认是on
					 		 				 7.i-cs  onOffProperty 默认是on
					 		 				 8.strike 绘制一条单线穿过文本  onOffProperty 默认是on
					 		 				 9.dstrike 绘制双线穿过文本  onOffProperty 默认是on
					 		 				 10.outline 显示每个字符内部和外部的边界   onOffProperty 默认是on
					 		 				 10.u 下划线 属性（  1.val  underlineValues optional、     2.color  ）  
					 		 				 	  <w:u w:val="dash" w:color="FF0000"/>
					 		 				 11.color <w:color w:val="auto"/>
					 		 				 12.spacing
					 		 				 12.w  <w:w val="1">   textScaleType1-600
					 		 				 13.sz 字体大小
					 		 				 14.sz-cs 字体大小
					 		 				 15.shd 底纹背景   shdProperty 属性    
					 		 				 <w:shd w:val="pct-15" w:color="auto" w:fill="FFFFFF" wx:bgcolor="D9D9D9"/>
					 		 				 						
					 		15.sectPr element 
					 						1.hdr 属性：type  required  hdrValue（even,odd,first）
					 							
					 						2.ftr 属性：type  required  ftrValue （even,odd,first）
					 						3.footnotePr  type：ftnEdnPropsElt  页脚属性
					 						4.endnotePr		type：ftnEdnPropsElt
					 						3.type
					 						4.pgSz   pageSzType 页面大小和方向
					 								属性  :w 宽度   required   twipsMeasureType
					 									 h  高度  required    twipsMeasureType
					 						5.pgMar 
					 								属性：top    设置页面的顶部边缘和第一行之间的距离。
					 									  right  设置页面的右边缘和右端之间的距离。
					 									  bottom 设置页面的底部边缘和底部最后一行之间的距离
					 									  left	  设置页面的左边缘和左端之间的距离
					 									  header 设置距离顶部的纸张边缘，头的顶部边缘
					 									  footer 设置距离从纸的底部边缘的页脚底部边缘
					 									  gutter 设置额外空间量到文件装订边缘。
					 						6.pgBorders 页面边框
					 									element：1.top  
					 												属性：val  borderValues  required
					 													  color
					 													  sz
					 											　2.left
					 											 3.bottom
					 											 4.right
					 					   7.lnNumType 指定行编号
					 		 	           7.pgNumType
					 		 	           8.titlePg 指定是否显示第一页
					 		 	           9.vAlign
					 		 	           10.docGrid 指定文档网格 
					 		 	           			   属性：type   （default无网格、lines行网格、lines-and-chars行和字符网格、snap-to-chars文字对齐字符网格）
					 		 	           			   	   line-pitch   行与行之间的行间距。每页行数的数量将被自动调整以适应线之间的空间
					 		 	           			   	   char-space   文件每行字符数
					 		 	           
					 		 	           	//行和字符
					 		 	           	<w:docGrid w:type="lines-and-chars" w:line-pitch="312" w:char-space="40960"/>   20磅对应38912字符数	
					 		 	           	
					 		 	            //指定行	
					 		 	            <w:docGrid w:type="lines" w:line-pitch="312" w:char-space="40960"/>   
					 		 	            
					 		 	            //文字对齐方式
					 		 	            <w:docGrid w:type="snap-to-chars" w:line-pitch="312" w:char-space="42106"/>	 
					 		 	            
					 		 	            //无
					 		 	            <w:docGrid w:line-pitch="312" w:char-space="42106"/>  
					 		 	           			   	   
					 		 	           			   	   
				2.r
		        3.hlink 超链接 
		        		 <w:hlink w:dest="img.jpg">
		        		 <w:hlink w:dest="E:\MyEclipse\jecn\JecnWord2.0\bin\沃尔夫we为.docx">
		        		 		<w:r></w:r>
		        		 </w:hlink>
		        4.subDoc 
		        
图片 ：Defines a picture. 	
	  pict
	  
	  element：1.binData  binDataType  属性 name  required  stringType
	  					     内容 base64Binary
			   2.movie   binDataType 
			   3.background 背景颜色    属性：1.bgcolor  stringType  optional
			   							  2.background  stringType  optional	
			   							  
			   4.v:shape  属性：
			   			  节点：v:imagedata   属性：src 
			   			  						  o:title	
			   5.o:OLEObject   属性
			   					  Type：
			   					  ProgID：
			   					  ShapeID：
			   					  DrawAspect：
			   					  ObjectID：	   							  			   							  			   							
表格    tbl

	 element： 
	 		1.tblPr  表格属性
	 			element：1.tblStyle  表格风格 stringProperty
	 				 	 2.tblpPr   属性：leftFromText  设置表左表边框和周围文字之间的距离 twipsMeasureType
	 				 	  				  rightFromText 设置表右表边框和周围文字之间的距离 twipsMeasureType
	 				 	  				  topFromText   设置顶部表格边框和周围文字之间的距离 twipsMeasureType
	 				 	  				  bottomFromText 设置底部表格边框和周围文字之间的距离 twipsMeasureType
	 				 	  				  
	 				 	  				  vertAnchor   定义这个的垂直固定 类型 vAnchorValue 	
	 				 	  				  horzAnchor   定义这个标的水平固定   类型  hAnchorValue
	 				 	  				  tblpXSpec   定义表格水平对齐方式  覆盖其他布局选项    类型：xAlignType (for example, center, left, or right,inside,outside)
	 				 	  				  tblpX       设置固定的水平距离  twipsMeasureType
	 				 	  				  tblpYSpec   定义表格垂直对齐方式   覆盖其他的布局选项 类型：yAlignType (for example, top ,bottom,center,inline,inside,outside)
	 				 	  				  tblpY       设置固定的垂直距离   twipsMeasureType
	 				 	3.tblOverlap  指定此表是否允许重叠，如果不指定，浮表将允许重叠     属性：val 类型 （ tableOverlapType ===》String）     值：Never
	 				    4.tblStyleRowBandSize   属性：val   必填    类型： integer
	 				    5.tblStyleColBandSize   属性：val   必填    类型： integer
	 				    6.tblW   表格宽度   属性 w  类型：decimalNumberType  
	 				    					 type 类型：string  （pct、dxa、auto、nil）
	 				    7.jc     表格对齐方式   属性：val  必填  （left、center、right、both）	
	 				    8.tblCellSpacing  定义单个单元格之间的间距 
	 				    			属性：w  类型：decimalNumberType
	 				    			     type 类型： type 类型：string  （pct、dxa、auto、nil）
	 				    9.tblInd    表格缩进
	 				    			属性：w  类型：decimalNumberType
	 				    			     type 类型： type 类型：string  （pct、dxa、auto、nil）
	 				    10.tblBorders  表格边框
	 				    				Element：1.top   属性： val 必填  String（single、thick、double、dotted、dashed、dot-dash）
	 				    									   color 
	 				    									   sz   类型： Integer  
	 				    									   wx:bdrwidth  类型： Integer  和sz一样
	 				    									   space
	 				    									   shadow 指定边框是否有阴影  onOffType（on、off）   
	 				    									
	 				    						 2.left
	 				    						 3.bottom
	 				    						 4.right
	 				    						 5.insideH  表格内部水平边框
	 				    						 6.insideV  表格累不垂直边框
	 				   11.shd     属性：val  必填   String (pct-5,pct-10,pct-15,pct-20)  766行
	 				   					color 
	 				   					fill
	 				   					wx:bgcolor 			
	 				    
	 				   12.tblLayout  指定表是否是固定宽度 ，否者采取布局    属性：type   值：Fixed   4722 	
	 				   13.tblCellMar 指定单元格的间距  
	 				   				  element ： top   
	 				   				  				属性：w  类型：decimalNumberType
	 				    			     			type 类型： type 类型：string  （pct、dxa、auto、nil）
	 				   				  			left 
	 				   				  			bottom
	 				   				  			right			 
	 				    					 
	 		2.tblGrid 表格各个列长度
	 				element :gridCol  属性：w  单位 twipsMeasureType
	 		
	 		3.tr  可选
	 			   element：trPr
	 			   				element：1.trHeight 行高   属性 val   twipsMeasureType
	 			   											 h-rule  类型 heightRuleType   （auto自动、exact指定的高度、at-least 至少为指定的高度）
	 			   						 2.tblHeader 指定是否是标题行    属性 val  onOffProperty  默认是on   <w:tblHeader/>
	 			   						 3.jc  对齐方式
	 			   						
	 			            tc
	 			              element ：tcPr  element：1.tcW  属性：w 
	 			              									   type（pct、dxa、auto）
	 			              						   2.gridSpan 所占单个格列数
	 			              						   3.hmerge  水平合并区域 属性 val （continue、restart）
	 			              						   4.vmerge  垂直合并区域  属性 val （continue、restart）
	 			              			  			   5.tcBorders element：1.top   borderProperty  属性：val   borderValues  （single、thick、double、dotted）
	 			              			  			   													 color
	 			              			  			   													 sz
	 			              			  			   													 wx:bdrwidth
	 			              			  			   													 shadow 阴影
	 			              			  			   						2.left
	 			              			  			   						3.bottom
	 			              			  			   						4.right
	 			              			  			   						5.insideH
	 			              			  			   						6.insideV
	 			              			  			   6.tcMar     element：1.top   tableWidthProperty 
	 			              			  			   							属性：w
	 			              			  			   								 type（pct、dxa、auto）
	 			              			  			   						2.left
	 			              			  			   						3.bottom
	 			              			  			   						4.right
	 			              			  			   7.vAlign  属性 val required （top、center、both、bottom）
body ==》1.sectPr ==============》1.cfChunk ====》1.lists
												  2.styles
												  3.fonts
								  2.（group）blockLevelChunkElts  =================》1.p
																					2.tbl
		2.（group）blockLevelElts 																				   
																					3.（group）runLevelElts========》1.proofErr
																													 2.permStart
																													 3.permEnd
																													 
																													 
																													 
		特殊的线条：w:pict==》v:line
		
					element：
							1.path
							2.formulas
							3.handles
							4.fill
							5.stroke
							
							6.imagedata
							7.shadow
							8.textbox
							9.textpath
							10.Behavior
							11.Sequence
							12.Action
							
				      属性：   1.id   string   required
				         2.style  string  optional
				         11.coordsize
				         12.strokecolor
				         13.strokeweight
				         14.filled
				         15.fillcolor
				         16.stroked
		
























	        
