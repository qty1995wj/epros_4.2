package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.bottom.JecnOKJButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextField;
import epros.designer.gui.system.config.ui.comp.JecnDocumentListener;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.RadioData;
import epros.designer.gui.system.config.ui.property.table.JecnThreeTableScrollPane;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 制度基本信息面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnBasicInfoRulePanel extends JecnAbstractPropertyBasePanel implements ActionListener {

	/** 其他项面板 */
	private JecnPanel otherPanel = null;

	/** 制度新建类型：标签 */
	private JLabel ruleCreateTypeLabel = null;
	/** 制度新建类型：文件 */
	private JecnConfigCheckBox fileCheckBox = null;
	/** 本地上传文件，复选框 */
	private JecnConfigCheckBox localFileCheckBox = null;
	/** 制度新建类型：模板 */
	private JecnConfigCheckBox modelCheckBox = null;

	/** 制度自动发布版本号：输入框 */
	private JecnConfigTextField autoPubVersionTextField = null;

	/** 制度责任人 */
	private RadioData ruleResData = null;
	private JecnConfigRadioButton peopleRadio;
	private JecnConfigRadioButton posRadio;

	/** 有效期Lab */
	private JLabel validityLab = new JLabel(JecnProperties.getValue("validMonthC"));

	/** 有效期Field */
	private JecnConfigTextField validityField = new JecnConfigTextField();
	/** 有效期提示信息 */
	private JecnUserInfoTextArea validityInfoArea = new JecnUserInfoTextArea();
	/** 有限期面板 */
	private JecnPanel validityPanel = new JecnPanel();
	/** 是否永久有效复选框 */
	private JecnConfigCheckBox validityCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("foreverC"));
	/** 是否可编辑有效复选框 */
	private JecnConfigCheckBox validityCheckBoxEdit = new JecnConfigCheckBox(JecnProperties.getValue("edit"));

	/** 制度文件类型 */
	private JLabel ruleFileTypeLabel = null;
	private JecnConfigTextField ruleFileTypeField = null;

	public JecnBasicInfoRulePanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		iniComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {

		// 表面板
		tableScrollPane = new JecnThreeTableScrollPane(dialog);

		// 其他项面板
		otherPanel = new JecnPanel();

		// 标签
		ruleCreateTypeLabel = new JLabel(JecnProperties.getValue("basicRuleCreateType"));
		// 制度文件关联知识库
		fileCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("ralationFile"));
		// 制度文件本地上传
		localFileCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("localUpload"));
		// 制度新建类型：模板
		modelCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("basicRuleCreateTypeModel"));

		// 制度自动发布版本号：输入框
		autoPubVersionTextField = new JecnConfigTextField();

		ruleResData = new RadioData(JecnProperties.getValue("ruleResTypeC"), JecnProperties.getValue("person"),
				JecnProperties.getValue("position"));
		peopleRadio = ruleResData.getRadioButton1();
		posRadio = ruleResData.getRadioButton2();
		posRadio.addActionListener(this);
		peopleRadio.addActionListener(this);
		// 其他项面板
		otherPanel.setOpaque(false);
		otherPanel.setBorder(null);

		fileCheckBox.setOpaque(false);
		fileCheckBox.setSelected(true);
		localFileCheckBox.setOpaque(false);
		localFileCheckBox.setSelected(true);
		modelCheckBox.setOpaque(false);
		modelCheckBox.setSelected(true);

		validityCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 有效期
		validityCheckBox.addActionListener(this);

		validityCheckBoxEdit.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		validityCheckBoxEdit.addActionListener(this);

		validityCheckBox.setSelected(false);
		validityField.setEditable(false);
		validityInfoArea.setForeground(Color.red);
		// 事件
		validityField.getDocument()
				.addDocumentListener(new JecnDocumentListener(this, validityField, validityInfoArea));
		validityPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 事件
		fileCheckBox.addActionListener(this);
		localFileCheckBox.addActionListener(this);
		modelCheckBox.addActionListener(this);

		fileCheckBox.setActionCommand("fileCheckBox");
		localFileCheckBox.setActionCommand("localFileCheckBox");
		modelCheckBox.setActionCommand("modelCheckBox");

		ruleFileTypeLabel = new JLabel(JecnProperties.getValue("ruleFileTypeC"));
		ruleFileTypeField = new JecnConfigTextField();
		// 事件
		ruleFileTypeField.getDocument().addDocumentListener(
				new JecnDocumentListener(this, ruleFileTypeField, validityInfoArea));
		ruleFileTypeField.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		autoPubVersionTextField.getDocument()
				.addDocumentListener(
						new JecnDocumentListener(this, autoPubVersionTextField, dialog.getOkCancelJButtonPanel()
								.getInfoLabel()));

	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		this.add(tableScrollPane, BorderLayout.CENTER);
		this.add(otherPanel, BorderLayout.SOUTH);

		Insets insets = new Insets(5, 5, 5, 5);

		int rowIndex = 0;
		// 标签
		GridBagConstraints c = new GridBagConstraints(0, rowIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(ruleCreateTypeLabel, c);
		// 文件
		c = new GridBagConstraints(1, rowIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		otherPanel.add(fileCheckBox, c);
		// 本地上传的文件
		c = new GridBagConstraints(2, rowIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		otherPanel.add(localFileCheckBox, c);
		// 模板
		c = new GridBagConstraints(3, rowIndex, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		otherPanel.add(modelCheckBox, c);
		rowIndex++;

		// 制度责任人类型Lab
		ruleResData.initLayout(otherPanel, insets, rowIndex);
		rowIndex++;

		// 有效期
		c = new GridBagConstraints(0, rowIndex, 4, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0);
		otherPanel.add(validityPanel, c);
		rowIndex++;

		// 有效期提示
		c = new GridBagConstraints(0, rowIndex, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 80, 0, 0), 0, 0);
		otherPanel.add(validityInfoArea, c);
		rowIndex++;

		JecnPanel ruleFileTypePanel = new JecnPanel();
		// 制度文件类型
		c = new GridBagConstraints(0, rowIndex, 4, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 0, 0, 0), 0, 0);
		otherPanel.add(ruleFileTypePanel, c);
		rowIndex++;
		// 空闲区域
		c = new GridBagConstraints(0, rowIndex, 4, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH,
				insets, 0, 0);
		otherPanel.add(new JLabel(), c);
		// 添加属性
		addValidityPanel();

		addRulePanel(ruleFileTypePanel);
	}

	private void addValidityPanel() {
		validityPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 8, 0, 0), 0, 0);
		validityPanel.add(validityLab, c);
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 5), 0, 0);
		validityPanel.add(validityField, c);

		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 0, 0, 5), 0, 0);
		validityPanel.add(validityCheckBox, c);

		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 0, 0, 5), 0, 0);
		validityPanel.add(validityCheckBoxEdit, c);
	}

	private void addRulePanel(JecnPanel jecnPanel) {
		jecnPanel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 8, 0, 0), 0, 0);
		jecnPanel.add(ruleFileTypeLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 5), 0, 0);
		jecnPanel.add(ruleFileTypeField, c);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {
			// 唯一标识
			String mark = itemBean.getMark();
			// 值
			String value = itemBean.getValue();
			if (ConfigItemPartMapMark.basicRuleCreateTypeFile.toString().equals(mark)) {// //制度新建类型：制度文件关联知识库（1：支持；0：不支持）
				fileCheckBox.setItemBeanAndValue(itemBean);
			} else if (ConfigItemPartMapMark.basicRuleCreateTypeLocalFile.toString().equals(mark)) {// 制度文件新建类型：制度文件本地上传（1：支持；0：不支持）
				localFileCheckBox.setItemBeanAndValue(itemBean);
			} else if (ConfigItemPartMapMark.basicRuleCreateTypeModel.toString().equals(mark)) {// 制度新建类型：制度（1：支持；0：不支持）
				modelCheckBox.setItemBeanAndValue(itemBean);
			} else if (ConfigItemPartMapMark.basicRuleAutoPubVersion.toString().equals(mark)) {// 制度自动发布版本号
				autoPubVersionTextField.setItemBean(itemBean);
				autoPubVersionTextField.setText((itemBean.getValue() == null) ? "" : itemBean.getValue());
			} else if (ConfigItemPartMapMark.basicRuleResPeople.toString().equals(mark)) {// 责任人类型：0:
				// 选择人
				// 1:选择岗位
				peopleRadio.setItemBean(itemBean);
				posRadio.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_FLOW_USER.equals(value)) {// 责任人类型是人
					peopleRadio.setSelected(true);
				} else {
					posRadio.setSelected(true);
				}
			} else if (ConfigItemPartMapMark.ruleValidDefaultValue.toString().equals(mark)) {// 有效期
				validityField.setItemBean(itemBean);
				if ("0".equals(value)) {
					validityCheckBox.setSelected(true);
					validityField.setEditable(false);
				} else {
					validityCheckBox.setSelected(false);
					validityField.setEditable(true);
					validityField.setText(value);
				}
			} else if (ConfigItemPartMapMark.ruleValidForever.toString().equals(mark)) {// 有效期
				validityCheckBox.setItemBean(itemBean);
				if ("0".equals(value)) {
					validityCheckBox.setSelected(true);
					validityField.setEditable(false);
				} else {
					validityCheckBox.setSelected(false);
					validityField.setEditable(true);
				}
			} else if (ConfigItemPartMapMark.uploadRuleFileType.toString().equals(mark)) {// 上传的制度文件
				ruleFileTypeField.setItemBean(itemBean);
				ruleFileTypeField.setText(value);
			} else if (ConfigItemPartMapMark.isShowRuleValidity.toString().equals(mark)) {// 制度有效期是否可以编辑
				validityCheckBoxEdit.setItemBean(itemBean);
				if ("0".equals(value)) {
					validityCheckBoxEdit.setSelected(false);
				} else {
					validityCheckBoxEdit.setSelected(true);
				}
			}
		}

		// 排序过滤隐藏项
		List<JecnConfigItemBean> showTableItemList = tableScrollPane.getShowItem(configTypeDesgBean.getTableItemList(),
				JecnConfigContents.TYPE_SMALL_ITEM_BASICINFO);
		// 显示表数据
		tableScrollPane.initData(showTableItemList);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == validityCheckBox) {// 1：选中；0不选中
				if (validityCheckBox.isSelected()) {// 选中 ：有效期永久
					validityField.setEditable(false);
					// 永久有效期值默认为：0
					validityCheckBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					validityField.setText("");
					validityField.getItemBean().setValue("0");
				} else {
					validityField.setEditable(true);
					validityCheckBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					// 不选中有效期永久且输入框中内容为空，设置JecnConfigItemBean中的有效期值为空
					if ("".equals(validityField.getText())) {
						validityField.getItemBean().setValue("9");
						validityField.setText("9");
					}
				}
			}else if (e.getSource() == validityCheckBoxEdit) {// 1：选中；0不选中
				if (validityCheckBoxEdit.isSelected()) {// 选中 ：可编辑
					validityCheckBoxEdit.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					validityCheckBoxEdit.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}  else {
				JecnConfigCheckBox checkBox = (JecnConfigCheckBox) e.getSource();
				if (checkBox.isSelected()) {// 选中 （1：支持；0：不支持）
					checkBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {// 未选中 （1：支持；0：不支持）
					checkBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}

		} else if (e.getSource() instanceof JecnConfigRadioButton) {
			// 责任人类型：0: 选择人 1:选择岗位
			JecnConfigRadioButton configRadioButton = (JecnConfigRadioButton) e.getSource();
			if (configRadioButton.isSelected()) {// 选中
				if (configRadioButton == peopleRadio) {// 人员
					peopleRadio.getItemBean().setValue(JecnConfigContents.ITEM_FLOW_USER);
				} else if (configRadioButton == posRadio) {// 岗位
					peopleRadio.getItemBean().setValue(JecnConfigContents.ITEM_FLOW_POS);
				}
			}
		}
	}

	/**
	 * 
	 * 
	 * @param boolean 校验成功：true ；失败：false
	 * 
	 */
	@Override
	public boolean check() {

		// 有效期验证
		boolean isValidity = getNumCheckInfo(validityField.getText());

		if (isValidity) {// 验证失败
			return false;
		}

		// 名称不能超过122个字符或61个汉字
		String info = JecnUserCheckUtil.checkNameLength(autoPubVersionTextField.getText());
		// 存在错误标识：true，无错：false
		boolean exsitsError = setErrorInfo(dialog.getOkCancelJButtonPanel().getInfoLabel(), info);

		JecnOKJButton btn = dialog.getOkCancelJButtonPanel().getOkJButton();
		if (exsitsError) {// 验证失败
			if (btn.isEnabled()) {
				btn.setEnabled(false);
			}
			return false;
		}

		if (!btn.isEnabled()) {// 验证成功
			btn.setEnabled(true);
		}
		return true;
	}

	/**
	 * 判断字符串是否为数字
	 * 
	 * @param strValidity
	 * @param isSelect
	 * @return true:存在错误信息
	 */
	private boolean getNumCheckInfo(String strValidity) {
		// 获取有效期
		String reg = "^[0-9]*$";
		String tipStr = "";
		// 有效期 值
		if (!validityCheckBox.isSelected()) {
			if (strValidity == null || "".equals(strValidity)) {
				tipStr = JecnProperties.getValue("validNotNullC");
			} else if (!strValidity.matches(reg)) {
				tipStr = JecnProperties.getValue("validIsNumC");
			} else if (strValidity.length() > 5 || Integer.parseInt(strValidity) > 9000) {// 如果输入的有效期大于9000则提示重新输入
				// “strValidity.length() >
				// 5”判断是为了避免Integer.parseInt(strValidity)报错， 不能超过9000个月
				tipStr = JecnProperties.getValue("validSuperfluousMonthC");
			} else if (Integer.parseInt(strValidity) <= 0) {
				tipStr = JecnProperties.getValue("validSuperfluousZeroC");
			} else {
				tipStr = "";
			}
			if (DrawCommon.isNullOrEmtry(tipStr)) {
				return false;
			}
			getInfoLable().setText(tipStr);
			return true;
		}
		return false;
	}

	private JecnUserInfoTextArea getInfoLable() {
		return dialog.getOkCancelJButtonPanel().getInfoLabel();
	}
}
