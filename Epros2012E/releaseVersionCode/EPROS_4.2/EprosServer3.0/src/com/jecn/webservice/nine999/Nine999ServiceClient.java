package com.jecn.webservice.nine999;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.xpath.DefaultXPath;

import com.alibaba.fastjson.JSONArray;
import com.jecn.epros.server.action.web.login.ad.nine999.Nine999AfterItem;

public class Nine999ServiceClient {

	private static Logger log = Logger.getLogger(Nine999ServiceClient.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		getDeptJson();
	}

	public static JSONArray getDeptJson() {
		return sendReq("dept");
	}
	
	public static JSONArray getPersonJson() {
		return sendReq("person");
	}

	public static JSONArray getPostJson() {
		return sendReq("post");
	}

	public static JSONArray sendReq(String type) { // type: dept post person
		JSONArray jsonArray = new JSONArray();
		String beginTime = "";
		String urlStr = Nine999AfterItem.getValue("HRURL");
		String paraXml = getAccInfoXml(type,beginTime);
		// System.out.println(paraXml);
		OutputStream out = null;
		String messageText = null;
		String respData = "";
		int count = 0;
		URL url;
		try {
			url = new URL(urlStr);

			HttpURLConnection con;
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestMethod("POST");
			con.setUseCaches(false);
			con.setConnectTimeout(1200000);
			con.setReadTimeout(1200000);
			con.setRequestProperty("Content-type", "text/xml; charset=UTF-8");
			con.setRequestProperty("Authorization", "Basic ZXByb3M6b3NiZXByb3M5OTk=");
			// con.setRequestProperty("WSS-Password Type", "PasswordText");

			// con.setRequestProperty("SOAPAction", soapAction);
			// con.setRequestProperty("Encoding", "UTF-8");
			out = con.getOutputStream();
			con.getOutputStream().write(paraXml.getBytes());
			out.flush();
			out.close();
			int code = con.getResponseCode();
			String tempString = null;
			StringBuffer sb1 = new StringBuffer();
			if (code == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				while ((tempString = reader.readLine()) != null) {
					sb1.append(tempString);
				}
				if (null != reader) {
					reader.close();
				}
			} else {
				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getErrorStream(), "UTF-8"));
				// 一次读入一行，直到读入null为文件结束
				while ((tempString = reader.readLine()) != null) {
					sb1.append(tempString);
				}
				if (null != reader) {
					reader.close();
				}
			}
			// 响应报文
			// String respData =
			// StringEscapeUtils.unescapeHtml4(sb1.toString());
			respData = sb1.toString();
			System.out.println(respData);
			// 通过read方法读取一个文件 转换成Document对象
			Document document = DocumentHelper.parseText(respData);
			// 获取根节点元素对象
			Element node = document.getRootElement();
			// 遍历所有的元素节点
			// listNodes(node);
			
			DefaultXPath xpath1 = new DefaultXPath("//soapenv:Envelope");
			xpath1.setNamespaceURIs(Collections.singletonMap("soapenv",
					"http://schemas.xmlsoap.org/soap/envelope/"));
			
			DefaultXPath xpath2 = new DefaultXPath("//ns1:getUpdatedElementsResponse");
			xpath2.setNamespaceURIs(Collections.singletonMap("ns1",
					"http://out.webservice.organization.sys.kmss.landray.com/"));


			
			Element ns = (Element) xpath1.selectNodes(node).get(0);
			System.out.println(ns.asXML());
			Element re = (Element) xpath2.selectNodes(ns).get(0);
			Element returnEle = re.element("return");
			Element message = returnEle.element("message");
			count = Integer.parseInt(returnEle.element("count").getText());
			beginTime = returnEle.element("timeStamp").getText();
			messageText = message.getText();
			// String a = node.element("message").getText();
			// return messageText;
		} catch (MalformedURLException e) {
			log.error("链接出错");
			e.printStackTrace();
		} catch (IOException e) {
			log.error("请求出错");
			e.printStackTrace();
		} catch (DocumentException e) {
			log.error("解析出错");
			log.error(respData);
			e.printStackTrace();
		}
		
		jsonArray = JSONArray.parseArray(messageText);
		
		while(count>=Integer.parseInt(Nine999AfterItem.getValue("maxCount"))){
			try {
				paraXml = getAccInfoXml(type,beginTime);
				url = new URL(urlStr);

				HttpURLConnection con;
				con = (HttpURLConnection) url.openConnection();
				con.setDoOutput(true);
				con.setDoInput(true);
				con.setRequestMethod("POST");
				con.setUseCaches(false);
				con.setConnectTimeout(1200000);
				con.setReadTimeout(1200000);
				con.setRequestProperty("Content-type", "text/xml; charset=UTF-8");
				con.setRequestProperty("Authorization", "Basic ZXByb3M6b3NiZXByb3M5OTk=");
				out = con.getOutputStream();
				con.getOutputStream().write(paraXml.getBytes());
				out.flush();
				out.close();
				int code = con.getResponseCode();
				String tempString = null;
				StringBuffer sb1 = new StringBuffer();
				if (code == HttpURLConnection.HTTP_OK) {
					BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
					while ((tempString = reader.readLine()) != null) {
						sb1.append(tempString);
					}
					if (null != reader) {
						reader.close();
					}
				} else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(con.getErrorStream(), "UTF-8"));
					// 一次读入一行，直到读入null为文件结束
					while ((tempString = reader.readLine()) != null) {
						sb1.append(tempString);
					}
					if (null != reader) {
						reader.close();
					}
				}
				// 响应报文
				// String respData =
				// StringEscapeUtils.unescapeHtml4(sb1.toString());
				respData = sb1.toString();
//				System.out.println(respData);
				// 通过read方法读取一个文件 转换成Document对象
				Document document = DocumentHelper.parseText(respData);
				// 获取根节点元素对象
				Element node = document.getRootElement();
				// 遍历所有的元素节点
				DefaultXPath xpath1 = new DefaultXPath("//soapenv:Envelope");
				xpath1.setNamespaceURIs(Collections.singletonMap("soapenv",
						"http://schemas.xmlsoap.org/soap/envelope/"));
				
				DefaultXPath xpath2 = new DefaultXPath("//ns1:getUpdatedElementsResponse");
				xpath2.setNamespaceURIs(Collections.singletonMap("ns1",
						"http://out.webservice.organization.sys.kmss.landray.com/"));


				
				Element ns = (Element) xpath1.selectNodes(node).get(0);
				System.out.println(ns.asXML());
				Element re = (Element) xpath2.selectNodes(ns).get(0);
				Element returnEle = re.element("return");
				Element message = returnEle.element("message");
				count = Integer.parseInt(returnEle.element("count").getText());
				beginTime = returnEle.element("timeStamp").getText();
				messageText = message.getText();
				// String a = node.element("message").getText();
				// return messageText;
			} catch (MalformedURLException e) {
				log.error("链接出错");
				e.printStackTrace();
			} catch (IOException e) {
				log.error("请求出错");
				e.printStackTrace();
			} catch (DocumentException e) {
				log.error("解析出错");
				log.error(respData);
				e.printStackTrace();
			}
			jsonArray.addAll(JSONArray.parseArray(messageText));
		}
		
		return jsonArray;
	}

	public static String getSoapHeader() {
		// 上面代码为从缓存中取到我们需求传递到认证头的数据 下面开始添加认证头
		StringBuffer soapHeader = new StringBuffer();

		soapHeader.append("<soapenv:Header>");
//		soapHeader.append("<tns:RequestSOAPHeader xmlns:tns=\"http://sys.webservice.client\">");
//		String user = "<tns:user>" + Nine999AfterItem.getValue("user") + "</tns:user>";
//		String password = "<tns:password>" + Nine999AfterItem.getValue("password") + "</tns:password>";
//		soapHeader.append(user);
//		soapHeader.append(password);
//		soapHeader.append("</tns:RequestSOAPHeader>");
		Date d = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String submitdate = format.format(d);
		soapHeader.append("<cux:SOAHeader>");
		soapHeader.append("<cux:APP_ID>100000015</cux:APP_ID>");
		soapHeader.append("<cux:APP_NAME>EPROS</cux:APP_NAME>");
		soapHeader.append("<cux:MOD_ID>500000001</cux:MOD_ID>");
		soapHeader.append("<cux:MOD_NAME>GET_OA_ORG</cux:MOD_NAME>");
		soapHeader.append("<cux:SUBMITDATE>" + submitdate + "</cux:SUBMITDATE>");
		soapHeader.append("</cux:SOAHeader>");
		
		soapHeader.append("</soapenv:Header>");
		return soapHeader.toString();
	}

	public static String getAccInfoXml(String type,String beginTime) {
		StringBuffer template = new StringBuffer();
		String header = getSoapHeader();
		String count = "<count>"+Nine999AfterItem.getValue("maxCount")+"</count>";
		template.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cux=\"http://xmlns.oracle.com/apps/cux/soaprovider/plsql/cux_10_ws_server_prg/\" xmlns:out=\"http://out.webservice.organization.sys.kmss.landray.com/\">");
		template.append(header);
		template.append("<soapenv:Body>");
		template.append("<out:getUpdatedElements>");
		template.append("<arg0>");
		template.append("<returnOrgType>[{\"type\":\"" + type + "\"}]</returnOrgType>");
		template.append("<beginTimeStamp>"+beginTime+"</beginTimeStamp>");
		template.append(count);
		template.append(" </arg0>");
		template.append(" </out:getUpdatedElements>");
		template.append(" </soapenv:Body>");
		return template.toString();
	}

	
}
