package epros.designer.util;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTreeNode;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.util.JecnResourceUtil;

public class JecnConfigTool {

	/**
	 * 流程支持工具显示状态 0是不显示，1是显示，2是显示和必填
	 * 
	 * @return
	 */
	public static int processSupportToolsShowType() {
		return getResultValue(ConfigItemPartMapMark.supportTools.toString());
	}

	/**
	 * 流程附件显示状态 0是不显示，1是显示，2是显示和必填
	 * 
	 * @return
	 */
	public static int processEnclosureShowType() {
		return getResultValue(ConfigItemPartMapMark.enclosure.toString());
	}

	/**
	 * 流程业务类型显示状态 0是不显示，1是显示，2是显示和必填
	 * 
	 * @return
	 */
	public static int processBussTypeShowType() {
		return getResultValue(ConfigItemPartMapMark.processShowBussType.toString());
	}

	/***
	 * 流程拟制人显示状态 0是不显示，1是显示，2是显示和必填
	 * 
	 * @return
	 */
	public static int processIntendedPersonShowType() {
		return getResultValue(ConfigItemPartMapMark.fictionPeople.toString());
	}

	/***
	 * 流程监护人显示状态 0是不显示，1是显示，2是显示和必填
	 * 
	 * @return
	 */
	public static int processGuardianShowType() {
		return getResultValue(ConfigItemPartMapMark.guardianPeople.toString());
	}

	/**
	 * 流程类别显示状态 0是不显示，1是显示，2是显示和必填
	 * 
	 * @return
	 */
	public static int processCategoryShowType() {
		return getResultValue("4");
	}

	/**
	 * 流程责任人类型 0是岗位 1是责任人
	 * 
	 * @return
	 */
	public static int processResponsiblePersonType() {
		return Integer.parseInt(ConnectionPool.getConfigAciton().selectValueFromCache(
				ConfigItemPartMapMark.basicDutyOfficer));
	}

	/**
	 * 制度责任人类型 0是岗位 1是责任人
	 * 
	 * @return
	 */
	public static int ruleResponsiblePersonType() {
		String resTypeStr = ConnectionPool.getConfigAciton().selectValue(2, "basicRuleResPeople");
		if ("1".equals(resTypeStr)) {
			return 1;
		}
		return 0;
	}

	/**
	 * 文件责任人类型 1是岗位0是责任人
	 * 
	 * @return
	 */
	public static int fileResponsiblePersonType() {
		String resTypeStr = ConnectionPool.getConfigAciton().selectValue(3, "basicFileResPeople");
		if ("1".equals(resTypeStr)) {
			return 1;
		}
		return 0;
	}

	/**
	 * 流程架构责任人类型 1是岗位 0是责任人
	 * 
	 * @return
	 */
	public static int processMapResponsiblePersonType() {
		String resTypeStr = ConnectionPool.getConfigAciton().selectValue(0, "isShowProcessMapOwnerType");
		if ("1".equals(resTypeStr)) {
			return 1;
		}
		return 0;
	}

	/**
	 * 流程责任人 是否必填
	 * 
	 * @return
	 */
	public static boolean isRequiredProcessResponsible() {
		JecnConfigItemBean itemBean = ConnectionPool.getConfigAciton().selectItemBeanByMark("3");
		return itemBean.getIsEmpty() == 1 ? true : false;
	}

	/**
	 * 流程客户 1是不可编辑，数据来源于流程图中流程客户图标的名称（多个随机），0是来至于流程客户数据库字段
	 * 
	 * @return
	 */
	public static int processCustomShowType() {
		return Integer.valueOf(ConnectionPool.getConfigAciton().selectValueFromCache(
				ConfigItemPartMapMark.readerFlowCustomer));
	}

	/**
	 * 是否自动编号
	 * 
	 * @return
	 */
	public static boolean isAutoNumber() {
		// 手动选择编号或自动生成编号
		return isShowAutoNumber() || isStandardizedAutoCode();
	}

	public static boolean isShowAutoNumber() {
		return isShowItem(ConfigItemPartMapMark.isShowAutoCode);
	}

	// 是否启用自动版本号
	public static boolean autoVersionNumber() {
		return isShowItem(ConfigItemPartMapMark.autoVersionNumber);
	}

	/**
	 * 流程有效期
	 * 
	 * @return
	 */
	public static String processValidityAllocation() {
		JecnConfigItemBean configItemBean = ConnectionPool.getConfigAciton().selectConfigItemBean(
				JecnConfigContents.TYPE_BIG_ITEM_PARTMAP, "processValidDefaultValue");
		// 有效期显示值
		return configItemBean.getValue();
	}

	/**
	 * false是不显示，true是显示
	 * 
	 * @return
	 */
	public static boolean isShowProcessMode() {
		return isShowItem(ConfigItemPartMapMark.isShowProcessMode);
	}

	/**
	 * 自动编号-公司代码
	 * 
	 * type : 0 流程，1 文件 ， 2 制度
	 * 
	 * @return
	 */
	public static JecnConfigItemBean getAutoComptyCode(int type) {
		if (type == 0) {
			return getItemBean(ConfigItemPartMapMark.auto_companyCode);
		} else if (type == 1) {
			return getItemBean(ConfigItemPartMapMark.file_auto_companyCode);
		} else {
			return getItemBean(ConfigItemPartMapMark.rule_auto_companyCode);
		}
	}

	/**
	 * 自动编号-生产线或小部门
	 * 
	 * @return
	 */
	public static JecnConfigItemBean getAutoProductionLine(int type) {
		if (type == 0) {
			return getItemBean(ConfigItemPartMapMark.auto_productionLine);
		} else if (type == 1) {
			return getItemBean(ConfigItemPartMapMark.file_auto_productionLine);
		} else {
			return getItemBean(ConfigItemPartMapMark.rule_auto_productionLine);
		}
	}

	/**
	 * 文件类型
	 * 
	 * @return
	 */
	public static JecnConfigItemBean getAutoFileType(int type) {
		if (type == 0) {
			return getItemBean(ConfigItemPartMapMark.auto_fileType);
		} else if (type == 1) {
			return getItemBean(ConfigItemPartMapMark.file_auto_fileType);
		} else {
			return getItemBean(ConfigItemPartMapMark.rule_auto_fileType);
		}
	}

	/**
	 * 小类别
	 * 
	 * @return
	 */
	public static JecnConfigItemBean getAutoSmallClass(int type) {
		if (type == 0) {
			return getItemBean(ConfigItemPartMapMark.auto_smallClass);
		} else if (type == 1) {
			return getItemBean(ConfigItemPartMapMark.file_auto_smallClass);
		} else {
			return getItemBean(ConfigItemPartMapMark.rule_auto_smallClass);
		}
	}

	/**
	 * 自动编号- 流程级别
	 * 
	 * @return
	 */
	public static JecnConfigItemBean getProcessLevel() {
		return getItemBean(ConfigItemPartMapMark.process_level);
	}

	public static JecnConfigItemBean getItemBean(ConfigItemPartMapMark mark) {
		return ConnectionPool.getConfigAciton().selectItemBeanByMark(mark.toString());
	}

	/**
	 * 更新
	 * 
	 * @param configItemList
	 * @param typeBigModel
	 */
	public static void updateItemBeans(List<JecnConfigItemBean> configItemList, int typeBigModel) {
		ConnectionPool.getConfigAciton().update(configItemList, typeBigModel);
	}

	public static void updateItemBeans(List<JecnConfigItemBean> configItemList) {
		ConnectionPool.getConfigAciton().update(configItemList);
	}

	/**
	 * 是否显示活动类别
	 * 
	 * @return
	 */
	public static boolean isShowActiveType() {
		return isShowItem(ConfigItemPartMapMark.activityType);
	}

	/**
	 * 显示状态 0是不显示，1是显示，2是显示和必填
	 * 
	 * @param mark
	 * @return
	 */
	public static int getResultValue(String mark) {
		JecnConfigItemBean itemBean = ConnectionPool.getConfigAciton().selectItemBeanByMark(mark);
		int result = 0;
		if (itemBean.getValue().equals("1")) {
			if (itemBean.isNotEmpty()) {
				result = 2;
			} else {
				result = 1;
			}
		}
		return result;
	}

	/**
	 * true:显示
	 * 
	 * @param mark
	 * @return
	 */
	public static boolean isShowItem(ConfigItemPartMapMark mark) {
		String value = ConnectionPool.getConfigAciton().selectValueFromCache(mark);
		return "1".equals(value) ? true : false;
	}

	/**
	 * 
	 * @param mark
	 */
	public static String getConfigItemValue(ConfigItemPartMapMark mark) {
		return getItemBean(mark).getValue();
	}

	/**
	 * 
	 * @param mark
	 */
	public static String getConfigItemName(ConfigItemPartMapMark mark) {
		return getItemBean(mark).getName(JecnResourceUtil.getLocale());
	}

	/**
	 * true：显示
	 * 
	 * @return
	 */
	public static boolean isShowRuleType() {
		return isShowItem(ConfigItemPartMapMark.ruleType);
	}

	/**
	 * 流程其它配置
	 * 
	 * @return
	 */
	public static List<JecnConfigItemBean> getProcessOtherItems() {
		return ConnectionPool.getConfigAciton().selectConfigItemBeanByType(1, 10);
	}

	/**
	 * 巴德富操作说明状态
	 * 
	 * @return
	 */
	public static boolean isBDFOperType() {
		return "16".equals(getConfigItemValue(ConfigItemPartMapMark.otherOperaType));
	}

	/**
	 * 立邦登录类型
	 * 
	 * @return
	 */
	public static boolean isLiBangLoginType() {
		return "28".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	/**
	 * 东软医疗
	 * 
	 * @return
	 */
	public static boolean isDongRuanYiLiaoLoginType() {
		return "30".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	/**
	 * 九新药业
	 * 
	 * @return
	 */
	public static boolean isJiuXinLoginType() {
		return "47".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	/**
	 * 招商证券
	 * 
	 * @return China Merchants Securities Co., Ltd
	 */
	public static boolean isNewOneType() {
		return "42".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	/**
	 * 制度右键菜单是否显示任务模板设置项
	 * 
	 * @return
	 */
	public static boolean isShowRuleMenuTaskEdit() {
		return isLiBangLoginType() ? false : true;
	}

	/**
	 * 是否显示 流程架构-流程任务设置
	 * 
	 * @return
	 */
	public static boolean isShowProcessMapMenuTaskEdit() {
		return isLiBangLoginType() ? false : true;
	}

	/**
	 * 是否显示流程架构-编号
	 * 
	 * @return true:显示，false 不显示
	 */
	public static boolean isShowProcessMapNum() {
		return isLiBangLoginType() ? false : true;
	}

	/**
	 * true:显示活动输入输出
	 * 
	 * @return
	 */
	public static boolean showActInOutBox() {
		return isShowItem(ConfigItemPartMapMark.showActInOutBox);
	}

	/**
	 * true：活动输入，输出 文本显示
	 * 
	 * @return
	 */
	public static boolean isShowInputText() {
		return isShowItem(ConfigItemPartMapMark.activityFileShow);
	}

	/**
	 * 时间驱动，时间配置
	 * 
	 * @return
	 */
	public static boolean isShowTimeDriverText() {
		String value = ConnectionPool.getConfigAciton().selectValue(6,
				ConfigItemPartMapMark.isShowDateFlowFile.toString());
		return JecnConfigContents.ITEM_IS_SHOW.equals(value) ? true : false;
	}

	/**
	 * 上海宝隆
	 * 
	 * @return
	 */
	public static boolean isBLOperType() {
		return "6".equals(getConfigItemValue(ConfigItemPartMapMark.otherOperaType));
	}

	/**
	 * 东软
	 * 
	 * @return
	 */

	public static String companyOperatorType = "";

	public static boolean isDRYLOperType() {
		if (companyOperatorType == null || "".equals(companyOperatorType)) {
			companyOperatorType = getConfigItemValue(ConfigItemPartMapMark.otherOperaType);
		}
		return "27".equals(companyOperatorType);
	}

	/**
	 * 是否显示 活动编号类别
	 * 
	 * @return
	 */
	public static boolean isActivityNumTypeShow() {
		String activityNumIsShow = getConfigItemValue(ConfigItemPartMapMark.activityNumIsShow);
		return "1".equals(activityNumIsShow);
	}

	/**
	 *判断流程文件中角色职责是否显示岗位
	 * 
	 * @return
	 */
	public static boolean isShowPosNameInRole() {
		boolean showPosNameBox = false;
		String value = ConnectionPool.getConfigAciton().selectValue(1, "showPosNameBox");
		if ("1".equals(value)) {
			showPosNameBox = true;
		}
		return showPosNameBox;
	}

	/**
	 * true：显示流程架构卡，false：显示流程架构描述
	 * 
	 * @return
	 */
	public static boolean isShowDes() {
		return isShowItem(ConfigItemPartMapMark.isShowDes);
	}

	/**
	 * 是否显示记录保存的移交责任人和编号
	 */
	public static boolean isShowRecordTransferAndNum() {
		return true;
	}

	/**
	 * true：只允许系统管理员编辑流程架构节点(增删改)
	 * 
	 * @return
	 */
	public static boolean isAdminEditFlowMapNode() {
		return !JecnDesignerCommon.isAuthorAdmin() && isShowItem(ConfigItemPartMapMark.isAdminEditProcessMap);
	}

	/**
	 * 活动时间配置-是否显示时间table
	 * 
	 * @return
	 */
	public static boolean isShowTimeText() {
		return isShowItem(ConfigItemPartMapMark.isShowDateActivity);
	}

	/**
	 * 流程架构是否允许维护文档管理
	 * 
	 * @return
	 */
	public static boolean isArchitectureUploadFile() {
		return isShowItem(ConfigItemPartMapMark.isArchitectureUploadFile);
	}

	private static int strSecurityLevel = -1;

	public static boolean flowSecurityLevel() {
		if (strSecurityLevel == -1 || JecnConstants.loginBean.isAdmin()) {
			strSecurityLevel = "1".equals(getConfigItemValue(ConfigItemPartMapMark.securityLevel)) ? 1 : 0;
		}
		if (strSecurityLevel == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 烽火通信
	 * 
	 * @return
	 */
	public static boolean isFiberhome() {
		return "19".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	/**
	 * 隐藏发布时间和实施时间
	 * 
	 * 株洲中车
	 * 
	 * @return
	 */
	public static boolean isHiddenImplDate() {
		return !isShowItem(ConfigItemPartMapMark.showHiddenImplDate);
	}

	/**
	 * 隐藏流程（文件）
	 * 
	 * @return
	 */
	public static boolean isHiddenProcessFile() {
		return !isShowItem(ConfigItemPartMapMark.showProcessFile);
	}

	/**
	 * 制度有效期默认值
	 * 
	 * @return
	 */
	private static String ruleValidityAllocationState = "-1";

	public static String ruleValidityAllocation() {
		if ("-1".equals(ruleValidityAllocationState) || JecnConstants.loginBean.isAdmin()) {
			JecnConfigItemBean configItemBean = ConnectionPool.getConfigAciton().selectConfigItemBean(
					JecnConfigContents.TYPE_BIG_ITEM_ROLE, "ruleValidDefaultValue");
			// 有效期显示值
			ruleValidityAllocationState = configItemBean.getValue();
		}
		return ruleValidityAllocationState;
	}

	/**
	 * 是否显示制度监护人
	 * 
	 * @return
	 */
	private static int showRuleGuardianPeopleState = -1;

	public static boolean isShowRuleGuardianPeople() {
		if (showRuleGuardianPeopleState == -1 || JecnConstants.loginBean.isAdmin()) {
			showRuleGuardianPeopleState = "1".equals(getConfigItemValue(ConfigItemPartMapMark.ruleGuardianPeople)) ? 1
					: 0;
		}
		if (showRuleGuardianPeopleState == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 是否显示制度业务范围
	 * 
	 * @return
	 */
	private static int showRuleBusinessScopeState = -1;

	public static boolean isShowRuleBusinessScope() {
		if (showRuleBusinessScopeState == -1 || JecnConstants.loginBean.isAdmin()) {
			showRuleBusinessScopeState = "1".equals(getConfigItemValue(ConfigItemPartMapMark.ruleBusinessScope)) ? 1
					: 0;
		}
		if (showRuleBusinessScopeState == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 是否显示制度其他范围
	 * 
	 * @return
	 */
	private static int showRuleOtherScopeState = -1;

	public static boolean isShowRuleOtherScope() {
		if (showRuleOtherScopeState == -1 || JecnConstants.loginBean.isAdmin()) {
			showRuleOtherScopeState = "1".equals(getConfigItemValue(ConfigItemPartMapMark.ruleOtherScope)) ? 1 : 0;
		}
		if (showRuleOtherScopeState == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 是否显示制度组织范围
	 * 
	 * @return
	 */
	private static int showRuleOrgScopeState = -1;

	public static boolean isShowRuleOrgScope() {
		if (showRuleOrgScopeState == -1 || JecnConstants.loginBean.isAdmin()) {
			showRuleOrgScopeState = "1".equals(getConfigItemValue(ConfigItemPartMapMark.ruleOrgScope)) ? 1 : 0;
		}
		if (showRuleOrgScopeState == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 是否显示部门的适用范围 雅阁原先要这个功能，然后又不要了
	 * 
	 * @return
	 */
	public static boolean isShowOrgScope() {
		return isShowItem(ConfigItemPartMapMark.showApplicabilitySeleteDept);
	}

	/**
	 * 雅阁
	 * 
	 * @return
	 */
	public static boolean isYG() {
		return "29".equals(getConfigItemValue(ConfigItemPartMapMark.otherOperaType));
	}

	/**
	 * 是否隐藏制度关联文件库菜单
	 * 
	 * @return
	 */
	public static boolean isHiddenRefRuleFileMenu() {
		return isMengNiuOperaType();
	}

	/**
	 * 蒙牛登录类型 37
	 * 
	 * @return
	 */
	public static boolean isMengNiuOperaType() {
		return "37".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	/**
	 * 是否预览文件自动编号
	 * 
	 * @return
	 */
	public static boolean isViewFileCode() {
		return isMengNiuOperaType();
	}

	public static List<JecnConfigItemBean> getRuleBasicConfigItems() {
		return ConnectionPool.getConfigAciton().selectConfigItemBeanByType(2, 2);
	}

	public static List<JecnConfigItemBean> getFileBasicConfigItems() {
		return ConnectionPool.getConfigAciton().selectConfigItemBeanByType(3, 2);
	}

	/**
	 * 是否必填
	 * 
	 * @param mark
	 * @return
	 */
	public static boolean isRequest(String mark) {
		return getResultValue(mark) == 2;
	}

	/**
	 * 搜索框，隐藏tree面板
	 * 
	 * @return
	 */
	public static boolean isHiddenSelectTreePanel() {
		return isShowItem(ConfigItemPartMapMark.isHiddenSelectTree);
	}

	/**
	 * 爱普生登录
	 * 
	 * @return
	 */
	public static boolean isEpsonLogin() {
		return "38".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	/**
	 * 科大讯飞登录
	 * 
	 * @return
	 */
	public static boolean isIflytekLogin() {
		return "40".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	/**
	 * 是否隐藏客户 属性 以及元素
	 * 
	 * @return
	 */
	public static boolean isShowProcessCustomer() {
		// 如果元素库中 没有添加客户元素 返回FALSE
		List<MapElemType> showTypeList = JecnDesignerProcess.getDesignerProcess().getShowElementsType(0);
		if (!showTypeList.contains(MapElemType.CustomFigure)) {
			return false;
		}
		return isShowItem(ConfigItemPartMapMark.isShowProcessCustomer);
	}

	/**
	 * 是否隐藏新建开始元素
	 * 
	 * @return
	 */
	public static boolean isShowBeginningEndElement() {
		return isShowItem(ConfigItemPartMapMark.isShowBeginningEndElement);
	}

	public static boolean isOpenPasswordValidate() {
		return false;
	}

	public static boolean showActTimeLine() {
		return isShowItem(ConfigItemPartMapMark.isShowActiveTimeValue);
	}

	/**
	 * 活动办理时限单位名称
	 * 
	 * @return
	 */
	public static String getActiveTimeLineUtil() {
		return getItemBean(ConfigItemPartMapMark.activeTimeLineUtil).getValue();
	}

	/**
	 * 制度维护 支撑文件
	 * 
	 * @return
	 */
	public static boolean isArchitectureRuleUploadFile() {
		return isShowItem(ConfigItemPartMapMark.isRuleUploadFile);
	}

	/**
	 * 自动创建的文件目录
	 * 
	 * @return
	 */
	public static boolean isAutoCreateFileDir() {
		return isArchitectureRuleUploadFile() || isArchitectureUploadFile();
	}

	/**
	 * 审批人默认显示顺序配置
	 * 
	 * @return
	 */
	public static boolean getTaskDefaultPeoplesOrder() {
		return isShowItem(ConfigItemPartMapMark.taskDefaultPeoplesOrder);
	}

	/**
	 * 上传文件-表格是否显示文件类型
	 * 
	 * @return
	 */
	public static boolean isShowFileTypeInTable() {
		String loginValue = getConfigItemValue(ConfigItemPartMapMark.otherLoginType);
		// 株洲中车 时代电气 -自动编号 显示文件类型
		return "23".equals(loginValue) || "39".equals(loginValue);
	}

	/**
	 * 标准版-自动编号
	 * 
	 * @return
	 */
	public static boolean isStandardizedAutoCode() {
		return isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode);
	}

	/**
	 * 新建鸟瞰图
	 * 
	 * @return
	 */
	public static boolean newAerialMap() {
		return isShowItem(ConfigItemPartMapMark.newAerialMap);
	}

	/**
	 * 是否显示流程架构的操作说明配置
	 * 
	 * @return
	 */
	public static boolean isShowHeadInfo() {
		return isShowItem(ConfigItemPartMapMark.isShowHeadInfo);
	}

	/**
	 * 元素属性-全局替换元素
	 * 
	 * @return
	 */
	public static boolean replaceAllElementAttribute() {
		return isShowItem(ConfigItemPartMapMark.replaceAllElementAttribute);
	}

	/**
	 * 隐藏制度体系
	 * 
	 * @return
	 */
	public static boolean isHiddenRule() {
		return isShowItem(ConfigItemPartMapMark.isHiddenSystem);
	}

	/**
	 * true ： 更新角色别名
	 * 
	 * @return
	 */
	public static boolean isUpdateRoleAlias() {
		return isShowItem(ConfigItemPartMapMark.isUpdateRoleAlias);
	}

	/**
	 * true ： 更新角色别名 是否仅首次更新
	 * 
	 * @return
	 */
	public static boolean isFirstUpdateRoleAlias() {
		JecnConfigItemBean itemBean = getItemBean(ConfigItemPartMapMark.isUpdateRoleAlias);
		return "2".equals(itemBean.getValue());
	}

	/**
	 * 上传的制度文件类型
	 * 
	 * @return
	 */
	public static String getUploadFileType() {
		return getItemBean(ConfigItemPartMapMark.uploadRuleFileType).getValue();
	}

	/**
	 * 裕同操作说明类型
	 * 
	 * @date 2018-5-10
	 * @return
	 */
	public static boolean isYUTOOperaType() {
		return "35".equals(getConfigItemValue(ConfigItemPartMapMark.otherOperaType));
	}

	/**
	 * 裕同
	 * 
	 * @return
	 */
	public static boolean isYT() {
		return "35".equals(getConfigItemValue(ConfigItemPartMapMark.otherOperaType));
	}

	public static String getItDesc() {
		if (JecnConfigTool.isYT()) {
			return JecnProperties.getValue("transactionCodeYT");
		}
		return JecnProperties.getValue("transactionCode");
	}

	/**
	 * 密级选择公开控制权限选择框是否可选
	 * 
	 * @param
	 * 
	 * @return true 不可选择 false可选择
	 */
	public static boolean pubDisableAuthSelect(int index) {
		if (index == 1 && isShowItem(ConfigItemPartMapMark.isShowAccessButton)) {// 根据配置选择公开的时候不让选择查阅权限
			return true;
		}
		return false;
	}

	/**
	 * 是否显示文本输入的logo
	 * 
	 * @return
	 */
	public static boolean isShowActiveTextPutLogo() {
		return isShowItem(ConfigItemPartMapMark.isShowActivtyTextInOutIcon);
	}

	/**
	 * 使用新版本的输入输出
	 * 
	 * @return
	 */
	public static boolean useNewInout() {
		return isShowItem(ConfigItemPartMapMark.useNewInout);
	}

	public static boolean isWanHuaLogin() {
		return "46".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	/**
	 * 流程贯通性检查是否显示
	 * 
	 * @return
	 */
	public static boolean checkoutVisible() {
		return useNewInout() && isShowItem(ConfigItemPartMapMark.a39) && isShowItem(ConfigItemPartMapMark.a40);
	}

	public static boolean isUseNewDrive() {
		return true;
	}

	/**
	 * 画图面板 - 编辑权限 流程发布
	 * 
	 * @return
	 */
	public static boolean editAuthProcessRelease() {
		return isShowItem(ConfigItemPartMapMark.editAuthProcessRelease);
	}

	/**
	 * 画图面板 - 编辑权限 流程未发布
	 * 
	 * @return
	 */
	public static boolean editAuthProcessNotRelease() {
		return isShowItem(ConfigItemPartMapMark.editAuthProcessNotRelease);
	}

	/**
	 * 画图面板 - 编辑权限 架构发布
	 * 
	 * @return
	 */
	public static boolean editAuthProcessMapRelease() {
		return isShowItem(ConfigItemPartMapMark.editAuthProcessMapRelease);
	}

	/**
	 * 画图面板 - 编辑权限 架构未发布
	 * 
	 * @return
	 */
	public static boolean editAuthProcessMapNotRelease() {
		return isShowItem(ConfigItemPartMapMark.editAuthProcessMapNotRelease);
	}

	public static boolean checkRoleEnable() {
		// return
		// "24".equals(getConfigItemValue(ConfigItemPartMapMark.otherOperaType));
		return false;
	}

	public static boolean isCetc10Login() {
		return "24".equals(getConfigItemValue(ConfigItemPartMapMark.otherLoginType));
	}

	public static boolean isShowProcessAccess() {
		return "1".equals(getConfigItemValue(ConfigItemPartMapMark.isShowProcessAccess));
	}

	public static int getProcessMapModelUpperLimit() {
		return Integer.valueOf(getConfigItemValue(ConfigItemPartMapMark.processMapModelUpperLimit));
	}

	public static int getProcessModelUpperLimit() {
		return Integer.valueOf(getConfigItemValue(ConfigItemPartMapMark.processModelUpperLimit));
	}

	public static String concatColon(String name) {
		return JecnUtil.concatColon(name);
	}

	public static boolean isFileNumMust() {
		return isMust(ConfigItemPartMapMark.isShowFileNum);
	}

	public static boolean isMust(ConfigItemPartMapMark mark) {
		JecnConfigItemBean itemBean = getItemBean(mark);
		return "1".equals(itemBean.getValue()) && itemBean.getIsEmpty() != null && 1 == itemBean.getIsEmpty();
	}

	public static boolean isShowGrid() {
		return isShowItem(ConfigItemPartMapMark.isShowGrid);
	}

	/**
	 * 系统配置中 流程是否公开
	 * 
	 * @return
	 */
	public static boolean isPubPartMap() {
		return isShowItem(ConfigItemPartMapMark.pubPartMap);
	}

	/**
	 * 系统配置中 流程架构是否公开
	 * 
	 * @return
	 */
	public static boolean isPubTotalMap() {
		return isShowItem(ConfigItemPartMapMark.pubTotalMap);
	}

	/**
	 * 元素链接显示位置：0 中上 ，1 右下 默认中上
	 * 
	 * @return
	 */
	public static String getElementLocation() {
		return getConfigItemValue(ConfigItemPartMapMark.elementLocation);
	}

	public static boolean CH() {
		return JecnResourceUtil.CH;
	}

	public static boolean EN() {
		return JecnResourceUtil.EN;
	}

	/**
	 * 制度拟制人
	 * 
	 * @return
	 */
	public static boolean isShowRuleFictionPeople() {
		return isShowItem(ConfigItemPartMapMark.ruleFictionPeople);
	}

	public static boolean typeAllSame(List<JecnTreeNode> selectNodes) {
		if (JecnUtil.isEmpty(selectNodes)) {
			return false;
		}
		JecnTreeNode node = selectNodes.get(0);
		int nodeType = node.getJecnTreeBean().getNodeType();
		TreeNodeType treeNodeType = node.getJecnTreeBean().getTreeNodeType();
		for (JecnTreeNode n : selectNodes) {
			if (nodeType != n.getJecnTreeBean().getNodeType() || treeNodeType != n.getJecnTreeBean().getTreeNodeType()) {
				return false;
			}
		}
		return true;
	}

}
