package epros.draw.data.figure;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;

/**
 * TO和From的数据bean
 * 
 * @author user
 * 
 */
public class JecnToFromRelatedData extends JecnFigureData {

	/** 关联的活动的id **/
	private Long relatedId;
	/** 关联的活动的uuid (保存后为relatedId) **/
	private String relatedUUId;

	public JecnToFromRelatedData(MapElemType mapElemType) {
		super(mapElemType);
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public String getRelatedUUId() {
		return relatedUUId;
	}

	public void setRelatedUUId(String relatedUUId) {
		this.relatedUUId = relatedUUId;
	}

	public JecnToFromRelatedData newInstance() {
		return new JecnToFromRelatedData(mapElemType);
	}

	public JecnToFromRelatedData clone() {
		return (JecnToFromRelatedData) this.currClone();
	}

	public void setFlowElementAttributes(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {
		if (!(srcFlowElementData instanceof JecnToFromRelatedData)
				|| !(destFlowElementData instanceof JecnToFromRelatedData)) {
			return;
		}
		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		JecnToFromRelatedData src = (JecnToFromRelatedData) srcFlowElementData;
		JecnToFromRelatedData dest = (JecnToFromRelatedData) destFlowElementData;
		dest.setRelatedId(src.getRelatedId());
		dest.setRelatedUUId(src.getRelatedUUId());
	}
}
