package epros.designer.gui.timeline;

import java.awt.Component;

import epros.draw.gui.workflow.JecnDrawMainPanel;

public enum TimeLimitLineBuilder {
	INSTANCE;

	/**
	 * 构建时间轴Panel
	 */
	public void buildTimeLinePanel() {
		clearTimeLimitPanel();
		TimeLimitLinePanel timeLimitPanel = new TimeLimitLinePanel();
		timeLimitPanel.repaint();
		JecnDrawMainPanel.getMainPanel().getWorkflow().add(timeLimitPanel);
		// 汇总
		JecnDrawMainPanel.getMainPanel().getWorkflow().add(timeLimitPanel.getLimitPanel());
	}

	/**
	 * 清除之前生成的时间轴Panel
	 */
	public void clearTimeLimitPanel() {
		Component c = getActiveTimeLimitPanel();
		if (c != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(c);
			TimeLimitLinePanel timeLimitPanel = (TimeLimitLinePanel) c;
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(timeLimitPanel.getLimitPanel());
		}
	}

	public TimeLimitLinePanel getActiveTimeLimitPanel() {
		Component[] c = JecnDrawMainPanel.getMainPanel().getWorkflow().getComponents();
		for (int i = c.length - 1; i >= 0; i--) {
			if (c[i] instanceof TimeLimitLinePanel) {
				return (TimeLimitLinePanel) c[i];
			}
		}
		return null;
	}
}
