package com.jecn.epros.server.webBean;
/**
 * @author yxw 2012-12-5
 * @description：公共文件搜索Bean
 */
public class PublicSearchBean {
	/**名称*/
	private String name;
	/**编号*/
	private String number;
	/**责任部门ID*/
	private Long orgId;
	/**责任部门名称*/
	private String orgName;
	/**密级**/
	private int secretLevel;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public int getSecretLevel() {
		return secretLevel;
	}
	public void setSecretLevel(int secretLevel) {
		this.secretLevel = secretLevel;
	}
	
}
