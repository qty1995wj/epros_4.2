package epros.draw.designer;

public class JecnFigureInoutSampleT {
	private String id;
	private Long flowId;
	/**
	 * 活动输入输出id
	 */
	private String inoutId;
	private Long fileId;
	/** 文件名称 不存数据库 */
	private String fileName;

	/**
	 * 0模板 1样例
	 */
	private int type;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInoutId() {
		return inoutId;
	}

	public void setInoutId(String inoutId) {
		this.inoutId = inoutId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public JecnFigureInoutSampleT clone() {
		JecnFigureInoutSampleT clone = new JecnFigureInoutSampleT();
		clone.setFileId(fileId);
		clone.setFlowId(flowId);
		clone.setFileName(fileName);
		clone.setId(id);
		clone.setInoutId(inoutId);
		clone.setType(type);
		return clone;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JecnFigureInoutSampleT) {
			JecnFigureInoutSampleT bean = (JecnFigureInoutSampleT) obj;
			if (this.inoutId != null && bean.getInoutId() != null) {
				if (!this.inoutId.equals(bean.getInoutId())) {
					return false;
				}
			}
			if (this.fileId != null && bean.getFileId() != null) {
				if (!this.fileId.equals(bean.getFileId())) {
					return false;
				}
			}
			if (this.type != bean.getType()) {
				return false;
			}
			return true;
		}

		return super.equals(obj);
	}
}
