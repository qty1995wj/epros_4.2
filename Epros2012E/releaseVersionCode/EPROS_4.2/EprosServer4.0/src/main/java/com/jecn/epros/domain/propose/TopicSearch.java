package com.jecn.epros.domain.propose;

import io.swagger.annotations.ApiModelProperty;

public class TopicSearch {

    // 主题搜索、类型搜索、处理人，提交人搜索
    private String name;
    @ApiModelProperty(value = "0流程，1制度，2流程架构，3文件，4风险，5标准 ")
    private Integer type = -1;
    private Long handlePeople;
    private Long createPeople;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getHandlePeople() {
        return handlePeople;
    }

    public void setHandlePeople(Long handlePeople) {
        this.handlePeople = handlePeople;
    }

    public Long getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Long createPeople) {
        this.createPeople = createPeople;
    }


}
