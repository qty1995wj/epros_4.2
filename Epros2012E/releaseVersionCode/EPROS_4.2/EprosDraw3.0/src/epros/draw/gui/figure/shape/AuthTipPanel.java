package epros.draw.gui.figure.shape;

import java.util.Locale;

import javax.swing.JEditorPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent.EventType;

import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnResourceUtil;

/**
 * 面板编辑权限提示
 * 
 *@author ZXH
 * @date 2016-5-26上午10:57:13
 */
public class AuthTipPanel extends ErrorPanel {

	public AuthTipPanel() {
		super();
	}

	public void initEditButton() {
		this.setLayout(null);
		this.add(MyeditPanel());
	}

	protected void editButAction(JEditorPane editBut) {
		JecnDesignerProcess.getDesignerProcess().editButAction(null);
		editBut.setVisible(false);
		JecnDrawMainPanel.getMainPanel().getWorkflow().remove(this);
		JecnDrawMainPanel.getMainPanel().getWorkflow().repaint();
		// 重新刷新占用 保存状态
		JecnPaintFigureUnit.addOpenTipPanel();

	}

	public JEditorPane MyeditPanel() {
		final JEditorPane deleterPane = new JEditorPane();
		deleterPane.setContentType("text/html");
		deleterPane.setText(" <a href='' style='color:red;font-weight:bold;'>"
				+ JecnResourceUtil.getJecnResourceUtil().getValue("edit") + "</a>");
		deleterPane.setEditable(false);
		if (JecnResourceUtil.getLocale() == Locale.CHINESE) {
			deleterPane.setBounds(310, 3, 60, 25);
		} else {
			deleterPane.setBounds(480, 3, 60, 25);
		}
		deleterPane.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() == EventType.ACTIVATED) {
					editButAction(deleterPane);
				}
			}
		});
		return deleterPane;
	}

}
