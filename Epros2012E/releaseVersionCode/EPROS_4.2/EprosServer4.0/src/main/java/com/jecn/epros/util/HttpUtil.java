package com.jecn.epros.util;

import com.jecn.epros.configuration.rpc.RPCServiceConfiguration;
import com.jecn.epros.domain.rule.Rule;
import com.jecn.epros.security.AuthenticatedUserUtil;
import org.springframework.context.annotation.Configuration;

/**
 * Created by admin on 2017/4/26.
 */
@Configuration
public class HttpUtil {
    private static String DOWN_LOAD_FILE_ACTION = "downloadFile.action?";

    private static String DB_SYNC_CONFIG = "getSyncConfig.action";
    /**
     * 流程文件
     */
    private static String DOWN_LOAD_PROCESS_DOC = "printFlowDoc.action?";

    private static String VIEW_PROCESS_DOC = "viewProcess.action?";
    /**
     * 制度文件
     */
    private static String DOWN_LOAD_RULE_DOC = "downloadRuleFile.action?";
    /**
     * 流程图
     */
    private static String DOWN_LOAD_IMAGE = "downloadMapIconAction.action?";

    /**
     * 流程图
     */
    private static String VIEW_PROCESS_IMAGE = "viewProcessIcon.action?";

    /**
     * 邮箱登录
     */
    private static String LOGIN_MAIL = "loginMail.action";

    public static String getDownLoadFileHttp(Long fileId, boolean isPub, String ruleLocal) {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + DOWN_LOAD_FILE_ACTION + "fileId=" + fileId + "&isPub=" + isPub + "&isPerm=false&downFileType=" + ruleLocal;
    }

    public static String getDownLoadFileHttp(Long fileId, boolean isPub, String ruleLocal, boolean isApp) {
        return getDownLoadFileHttp(fileId, isPub, ruleLocal) + (isApp == true ? "&isApp=" + isApp : "");
    }

    public static String getDownLoadHistoryFileHttp(Long fileId, boolean isPub, String ruleLocal, boolean isHistory) {
        return getDownLoadFileHttp(fileId, isPub, ruleLocal) + "&isHistory=" + isHistory;
    }

    public static String getSyncConfigUrl() {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + DB_SYNC_CONFIG;
    }

    /**
     * 查看制度
     *
     * @param id
     * @return
     */
    public static String getRule(String id, String isPub, String userCode) {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + LOGIN_MAIL + "?accessType=mailOpenDominoRuleModelFile&mailRuleId=" + id + "&isPub=" + isPub + "&userCode=" + userCode;
    }

    /**
     * 查看流程或架构详情
     *
     * @param id
     * @return
     */
    public static String getProcessDetail(String id, String isPub, String userCode) {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + LOGIN_MAIL + "?accessType=mailOpenDominoMap&mailFlowId=" + id + "&isPub=" + isPub + "&userCode=" + userCode;
    }

    /**
     * 制度文件链接
     *
     * @param rule
     * @param isPub
     * @return
     */
    public static String getRuleFileUrl(Rule rule, boolean isPub, boolean isApp) {
        return getDownLoadFileHttp(rule.getFileId(), isPub, rule.getIsFileLocal() == 1 ? "ruleLocal" : "") + (isApp == true ? "&isApp=" + isApp : "");
    }

    public static String getDownLoadRuleDocUrl(Long id, boolean isPub, boolean isApp) {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + DOWN_LOAD_RULE_DOC + "ruleId=" + id + "&isPub=" + isPub + (isApp == true ? "&isApp=" + isApp : "");
    }

    public static String getProcessDocUrl(Long id, boolean isPub, boolean isApp, Long historyId) {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + DOWN_LOAD_PROCESS_DOC + "flowId=" + id + "&isPub=" + isPub + (isApp == true ? "&isApp=" + isApp : "" + "&historyId=" + historyId);
    }

    public static String getViewProcessDocUrl(Long id, boolean isPub, boolean isApp) {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + VIEW_PROCESS_DOC + "flowId=" + id + "&relatedId=" + id + "&isPub=" + isPub + (isApp == true ? "&isApp=" + isApp : "" + "&userId=" + AuthenticatedUserUtil.getPrincipal().getId());
    }

    public static String getProcessChartUrl(Long id) {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + "processes/" + id + "/chart?opts=tree";
    }

    /**
     * 下载图片路径
     *
     * @param id
     * @param mapIconPath
     * @param isPub
     * @return
     */
    public static String getProcessImageUrl(Long id, String mapIconPath, String isPub, boolean isApp) {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + DOWN_LOAD_IMAGE + "flowId=" + id + "&mapIconPath=" + mapIconPath + "&isPub=" + isPub + "&isApp=" + isApp + "&userId=" + AuthenticatedUserUtil.getPrincipal().getId();
    }

    /**
     * 下载图片路径
     *
     * @param id
     * @param mapIconPath
     * @param isPub
     * @return
     */
    public static String getViewProcessImageUrl(Long id, String mapIconPath, String isPub, boolean isApp) {
        return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + VIEW_PROCESS_IMAGE + "relatedId=" + id + "&mapIconPath=" + mapIconPath + "&isPub=" + isPub + "&isApp=" + isApp;
    }
}
