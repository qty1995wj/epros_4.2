<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script type="text/javascript" src="${basePath}js/rtnlPropose.js"></script>
<script type="text/javascript" src="${basePath}js/rule/singleRuleRtnlPropose.js"></script>
<script type="text/javascript" src="${basePath}js/rule/ruleSys.js"></script>
<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
<script type="text/javascript">
	   var basePath = "${basePath}";
	   document.body.onresize = function() {
			setRtnlPropseWH('rtnlProposeShowContent');
	   }
</script>

</head>
<%-- 流程，制度合理化建议上边显示--%>
<body>

<div id="mainProposeId"  style="border: 1px;border-color: gray;height: 100%">
	<div id="topDivProposeId" align="center" style="margin-left: 15px; margin-right: 15px;">
		<table width="100%" style="table-layout:fixed; ">
			<tbody>
				<tr align="left">
					<td style="padding-right: 10px;padding-top: 10px;" class="wordHidden" >
					<s:if test="isFlowRtnl == 0">
					   <span style="font-weight:bold;font-size:18px;"  ><s:text name="processNameC" /></span>
					</s:if>
					<s:elseif test="isFlowRtnl == 1">
					   <span style="font-weight:bold;font-size:18px;"  ><s:text name="ruleNameC" /></span>
					</s:elseif>
					    <SPAN style="color:#6d0303;font-size:18px;font-weight:bold;"><s:property value="fileName" /></SPAN>
					</td>
					<td rowspan="2" align="right" style="vertical-align: middle;padding-right: 20px;width:180px;">
					<!-- 是否显示全部已读按钮 0是不显示，1是显示并置灰，2是显示 不置灰 -->
					<s:if test="isShowAllReadImg == 1">
						<img id="srcUnRead" src="${basePath}images/propose/allReadGray.gif" >
					</s:if>
					<s:elseif test="isShowAllReadImg == 2">
					    <img id="srcUnRead" src="${basePath}images/propose/allRead.gif" onclick="updateToRead()" style="cursor: pointer;">
					</s:elseif>
					
					<!-- 提交合理化建议 -->
					<!-- 流程 -->
					<img src="${basePath}images/propose/submitPropose.gif" style="cursor: pointer;" onclick="showDiv(0,0,'<s:property  value="flowId" />','<s:property value="fileNum" />','<s:property value="isAdiminLogin" />','<s:property value="isFlowRtnl" />','<s:property value="fileName" />','','-1',0);">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div id="centenDivProposeId" style="border-color: RGB(199, 199, 199);margin: 10px 10px 10px 15px;">
		<table width="100%" >
		<tr align="left">
		<td >
		<input type="hidden" id="hideStateValue" value="-1"/>
		<div style="width: 100%;text-align:center;margin:0 aoto;background-color: #f7f7f7;">
			<ul style="margin: 0 auto;position: relative;list-style-type:none;">
				
					<li id="liAllStatePropose" onclick="secProposeBoard(0,'<s:property  value="flowId" />','<s:property  value="currentPage" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />','<s:property  value="isFlowRtnl" />','<s:property value="fileName" />')" class="tabone1"><s:text name="all" />(<span id="allProId"><s:property value="allStateNum"/></span>)</li>
					<li id="liReadedPropose"  onclick="secProposeBoard(1,'<s:property  value="flowId" />','<s:property  value="currentPage" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />','<s:property  value="isFlowRtnl" />','<s:property value="fileName" />')" class="tabone2"><s:text name="readedPropose" />(<span id="readProId"><s:property value="readStateNum"/></span>)</li>
					<li id="liUnReadPropose"  onclick="secProposeBoard(2,'<s:property  value="flowId" />','<s:property  value="currentPage" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />','<s:property  value="isFlowRtnl" />','<s:property value="fileName" />')" class="tabone2"><s:text name="noReadPropose" />(<span id="unReadProId"><s:property value="unReadStateNum"/></span>)</li>
					<li id="liAcceptPropose"  onclick="secProposeBoard(3,'<s:property  value="flowId" />','<s:property  value="currentPage" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />','<s:property  value="isFlowRtnl" />','<s:property value="fileName" />')" class="tabone2"><s:text name="acceptPropose" />(<span id="acceptProId"><s:property value="acceptStateNum"/></span>)</li>
					<li id="liRefusePropose"  onclick="secProposeBoard(4,'<s:property  value="flowId" />','<s:property  value="currentPage" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />','<s:property  value="isFlowRtnl" />','<s:property value="fileName" />')" class="tabone2"><s:text name="refusePropose" />(<span id="refuseProId"><s:property value="refuseStateNum"/></span>)</li>
			</ul>
			<div align="right" style="display:inline;float:left;width: 56%;float: left; height: 25px; margin-top: 10px; margin-bottom: 5px;padding-top: 0px; border-top: gray 2px solid; border-bottom: 1px gray solid; border-right: 1px gray solid; padding-bottom: 5px;line-height:25px;list-style-image:none;list-style-type:none; background-color: #f7f7f7">
			      <input style="vertical-align: middle;" type="checkbox" name="myproposename" id="myProposeCheckboxId" onclick="selectMyPropose('<s:property  value="flowId" />','<s:property  value="isFlowRtnl" />')" />
			      <span style="vertical-align: middle;font-weight: bold;color: gray;padding-left:1px;padding-right:3px;"><s:text name="myPropose" /></span>
			</div>
		</div>
		</td>
		</tr>
		</table>
		<!-- tab标签内容显示-->
		<div id="allStateProposeId" ></div>
		<div id="readedProposeId"  style="display: none;"></div>
		<div id="unReadProposeId"  style="display: none;"></div>
		<div id="acceptProposeId"  style="display: none;"></div>
		<div id="refuseProposeId"  style="display: none;"></div>
     </div>

<!-- 点击提交建议按钮弹出窗体 start -->
			<div id="bg" class="bg"></div>
			<div id="show" class="show" style="display: none;overFlow-x: hidden; overFlow-y: hidden;">
				<form action="uploadRtnlProposeFile.action" method ="POST" enctype ="multipart/form-data" id="proposeFormId">
					<input type="hidden" id="flowId" name="flowId" value="<s:property  value="flowId" />"/>
					<input type="hidden" id="isFlowRtnl" name="isFlowRtnl" value="<s:property  value="isFlowRtnl" />"/>
					<input type="hidden" id="addOrUpdateId" name="addOrUpdateName" value="0"/>
					<textarea style="width: 100%;height: 285px;" name="fileProposeContent" id="fileProposeContentId"></textarea><br>
					
						<div style="height: 20px;">
							<span id="addUploadFileSpan" style="display:none;">
								<input type="file" name="upload" id="upfileProposeId" style="padding: 3px 0px 3px 3px;margin: 5px 0px 5px 0px;"/>
								<a href="#" class="flowPropose" id="hideAddFileInputId" onclick="hideAddFileInput()"><s:text name="delete" /></a>
							</span>
						</div>
						<span style="float:right;">
							<input type="button" value="<s:text name="addFilePropose" />" onclick="addFilePropose()" id="buttonAddFile" style="margin-top: 5px;">
							<input type="button" value="<s:text name="okBut" />" onclick="okUploadFile(0,'<s:property  value="flowId" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />')" style="width: 50px;margin-top: 5px;"/>
							<input type="button" value="<s:text name="cancelBut" />" onclick="hideDiv();" style="width: 50px;margin-top: 5px;"/>
						</span>
				</form>
			</div>
<!-- 点击提交建议按钮弹出窗体 end -->	

<!-- 点击编辑按钮弹出窗体 start -->
		<div id="eidtbg" class="bg"></div>
		<div id="eidtshow" class="show"
			style="overFlow-x: hidden; overFlow-y: hidden;">
			<form action="uploadRtnlProposeFile.action" method="POST"
				enctype="multipart/form-data" id="editproposeFormId">
				<input type="hidden" id="editrtnlProposeId"
					name="editrtnlProposeId"
					style="padding: 3px 0px 3px 3px; margin: 5px 0px 5px 0px;" />
				<input type="hidden" id="editaddOrUpdateId" name="addOrUpdateName"
					value="1" />
				<input type="hidden" name="isDeleteFileId" id="isDeleteFileId"
					value="0" />
				<input type="hidden" name="editProposeFileId"
					id="editProposeFileId" />
				<input type="hidden" id="isFlowRtnl" name="isFlowRtnl" value="<s:property  value="isFlowRtnl" />"/>
				<textarea style="width: 100%; height: 280px;"
					name="editfileProposeContent" id="editProposeContentId"></textarea>
				<br>
				<div style="height: 12px;">
					<span id="updateUploadFileNameId" style="display: none"> <span
						id="fileNameShowId"></span>&nbsp;&nbsp;<a class="flowPropose"
						href="#" onclick="deleteProposeFileU()" style="cursor: pointer;"
						id="uploadFileNameIdU"><s:text name="delete" />
					</a> </span>
				</div>
				<div style="height: 20px;">
					<span id="updateUploadFileSpan" style="display: none;"> <input
							type="file" name="editupload" id="editUpfileProposeId"
							style="padding: 3px 0px 3px 3px; margin: 5px 0px 5px 0px;" />
						<a href="#" class="flowPropose" id="hideUpdateFileInputId"
						onclick="hideUpdateFileInput();"><s:text name="delete" />
					</a> </span>
				</div>
				<span style="float: right; margin-bottom: 0px;"> <input
						type="button" value="上传附件" onclick="updateFilePropose()"
						id="buttonUpdateFile" style="margin-top: 5px;"> <input
						type="button" value="确定"
						onclick="editUploadFile('<s:property  value="flowId" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />')"
						style="width: 50px; margin-top: 5px;" /> <input type="button"
						value="取消" onclick="hidEditDiv();"
						style="width: 50px; margin-top: 5px;" /> </span>
			</form>
		</div>
<!-- 点击提交建议按钮弹出窗体 end -->
	
</div>

</body>
</html>