package com.jecn.epros.server.action.web.standard;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.service.standard.IStandardListService;
import com.jecn.epros.server.service.standard.IStandardService;
import com.jecn.epros.server.service.standard.buss.StandardListDownload;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.standard.StandardDetailBean;
import com.jecn.epros.server.webBean.standard.StandardInfoListBean;
import com.jecn.epros.server.webBean.standard.StandardRelatePRFBean;
import com.jecn.epros.server.webBean.standard.StandardSearchBean;
import com.jecn.epros.server.webBean.standard.StandardWebBean;
import com.jecn.epros.server.webBean.standard.StandardWebInfoBean;
import com.opensymphony.xwork2.ActionContext;

public class StandardAction extends BaseAction {
	/** 点击树加载传回的PID */
	private long node;
	/** 开始行数 */
	private int start;
	/** 每页显示条数 */
	private int limit;
	private Long standardId;
	/** 标准名称 */
	private String standardName;
	/** 标准类型 */
	private int standardType = -1;
	/** 查阅部门 */
	private long orgLookId = -1;
	/** 查阅部门名称 */
	private String orgLookName;
	/** 查阅岗位 */
	private long posLookId = -1;
	/** 查阅岗位名称 */
	private String posLookName;
	/** 密级 */
	private long secretLevel = -1;
	/** 上级目录 */
	private long perId = -1;
	/** 上级目录名称 */
	private String perName;
	/** 1加入tab 0弹出 */
	private int callType;
	/** 拼装数据源 */
	private String stores = "";
	/** 拼装表头 */
	private String columns = "";

	/** 标准详情 */
	private StandardDetailBean standardDetailBean;
	/** 标准清单 */
	private StandardWebInfoBean standardWebInfoBean;

	private IStandardService standardService;
	/** 标准清单Service方法 */
	private IStandardListService listStandService;
	/** 清单默认展示十条 true ：全部展示 */
	private boolean showAll = false;

	private static final Logger log = Logger.getLogger(StandardAction.class);

	/** 打开文件文件流 */
	private byte[] content = null;
	/** 文件名称 */
	private String fileName;

	/** 流程标准类型和ID */
	private Long relFlowId;
	private String mapType;
	private boolean ruleTypeIsShow;

	/**
	 * 标准清单
	 * 
	 * @return 修改人:chehuanbo 描述：
	 */
	public String getStandardInfoList() {
		try {
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
			standardWebInfoBean = listStandService
					.getStandardInfoList(standardId, webLoginBean.getProjectId(), showAll);

			// 数据
			StringBuffer storesBuf = new StringBuffer();
			storesBuf.append("[");

			for (int i = 0; i < standardWebInfoBean.getMaxLevel(); i++) {
				if (standardWebInfoBean.getCurLevel() <= i) {
					storesBuf.append(JecnCommon.storesJson(i + "level", i + "standName") + ",");
				}
			}
			storesBuf.append(JecnCommon.storesJson("standardId", "standardId") + ",");
			storesBuf.append(JecnCommon.storesJson("preStandId", "preStandId") + ",");
			storesBuf.append(JecnCommon.storesJson("standName", "standName") + ",");
			storesBuf.append(JecnCommon.storesJson("standType", "standType") + ",");
			storesBuf.append(JecnCommon.storesJson("standContent", "standContent") + ",");
			storesBuf.append(JecnCommon.storesJson("listStandardRefBean", "listStandardRefBean") + "]");
			stores = storesBuf.toString();

			// 表头
			StringBuffer columnsBuf = new StringBuffer();
			columnsBuf.append("[new Ext.grid.RowNumberer(),");
			for (int i = 0; i < standardWebInfoBean.getMaxLevel(); i++) {
				if (standardWebInfoBean.getCurLevel() <= i) {
					columnsBuf.append(JecnCommon.columnsJson(i + JecnUtil.getValue("level"), i + "level", null) + ",");
				}

			}
			// 标准名称
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("standardName"), "standName", null) + ",");
			// 条款对应要求
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("clauseToRequest"), "standContent", null) + ",");
			// 相关文件
			columnsBuf.append(JecnCommon
					.columnsJson(JecnUtil.getValue("activeRelateFile"), "listStandardRefBean", null)
					+ "]");
			columns = columnsBuf.toString();

			StringBuffer sb = new StringBuffer("[");
			StringBuffer standStr = null;
			for (int i = 0; i < standardWebInfoBean.getListInfoBean().size(); i++) {

				StandardInfoListBean standardBean = standardWebInfoBean.getListInfoBean().get(i);
				if (standardBean.getStandType() == 0) {
					continue;
				} else {
					sb.append("{");
					// 标准ID
					sb.append(JecnCommon.contentJson("standardId", standardBean.getStandardId()) + ",");
					findPerDir(standardBean, sb, standardWebInfoBean);
				}
				if (standardBean.getStandType() == 5) {// 条款要求
					for (StandardInfoListBean standardBean2 : standardWebInfoBean.getListInfoBean()) {
						if (standardBean2.getStandType() != 4) {
							continue;
						}
						// 判断
						if (standardBean.getPreStandId().longValue() == standardBean2.getStandardId()) {
							// 标准名称
							sb.append(JecnCommon.contentJson("standName", standardBean2.getHrefStandName()) + ",");
							break;
						}
					}
					// 条款要求
					sb.append(JecnCommon.contentJson("standContent", standardBean.getHrefStandContent()) + ",");
				} else {
					// 标准名称
					sb.append(JecnCommon.contentJson("standName", standardBean.getHrefStandName()) + ",");
				}
				List<StandardRelatePRFBean> relateList = standardBean.getListStandardRefBean();
				standStr = new StringBuffer();
				for (StandardRelatePRFBean relateBean : relateList) {
					if (relateBean.getRefType() == 1) {// 流程
						standStr.append(relateBean.getHrefFlow()).append("[" + JecnUtil.getValue("flow") + "]");
						standStr.append("<BR>");
					}
					if (relateBean.getRefType() == 2 || relateBean.getRefType() == 4) {// 制度
						standStr.append(relateBean.getHrefRule()).append("[" + JecnUtil.getValue("rule") + "]");
						standStr.append("<BR>");
					}
					if (relateBean.getRefType() == 3) {// 活动
						standStr.append(relateBean.getHrefFlow()).append("&nbsp;");
						standStr.append(relateBean.getHrefActive()).append("[" + JecnUtil.getValue("activities") + "]");
						standStr.append("<BR>");
					}
				}
				sb.append(JecnCommon.contentJson("listStandardRefBean", standStr.toString()));
				sb.append("},");
			}
			// 存在结果的情况下 截取最后一个逗号
			if (sb.toString().endsWith(",")) {
				sb.setLength(sb.length() - 1);
			}
			sb.append("]");
			// 待定
			outJsonString(JecnCommon.composeJson(stores, columns, sb.toString(), 0, 0, 0, 0));
		} catch (Exception e) {
			log.error("标准清单查询出错！", e);
		}
		return SUCCESS;
	}

	/**
	 * 递归查找
	 * 
	 * @author chehuanbo
	 * @date 2014-10-28
	 * @param standardBean
	 *            标准的基本信息bean
	 * @param standarSysStr
	 * @param ruleSysLevel
	 *            标准清单封装bean
	 * @since V3.06
	 * 
	 */
	public void findPerDir(StandardInfoListBean standardBean, StringBuffer standarSysStr,
			StandardWebInfoBean standardWebInfoBean) {
		try {
			StandardInfoListBean standardBean1 = standardBean;
			boolean isExit = false; // 判断是否找到父级目录 false:没有找到 true:找到 默认：false
			for (StandardInfoListBean standardBean2 : standardWebInfoBean.getListInfoBean()) {
				if (standardBean1.getPreStandId().longValue() == standardBean2.getStandardId()) {
					String levelName = standardBean2.getStandName(); // 标准名称
					// 如果是条款条款不给父目录赋值，因为条款要求的父目录是条款
					if (standardBean2.getStandType() != 4) {
						standarSysStr.append(JecnCommon.contentJson(standardBean2.getLevel() + "standName", levelName)
								+ ",");
					}
					standardBean1 = standardBean2;
					// 当找到父级目录时改变isExit的值。防止递归进入死循环
					isExit = true;
					break;
				}
			}

			if (standardBean1.getLevel() != standardWebInfoBean.getCurLevel() && isExit) {
				findPerDir(standardBean1, standarSysStr, standardWebInfoBean); // 递归查找
			}
		} catch (Exception e) {
			log.error("递归查找当前制度的上级目录的出现异常,RuleAction中的findPerDir()", e);
		}
	}

	/**
	 * 
	 * 标准清单下载
	 * 
	 * @return
	 */
	public String downLoadStandFile() {
		try {
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
			// 清单信息
			standardWebInfoBean = listStandService.getStandardInfoList(standardId, webLoginBean.getProjectId(), true);
			// 获取excel 字节数组
			content = StandardListDownload.createStandListDesc(standardWebInfoBean);

			// 标准清单
			fileName = JecnUtil.getValue("standardList") + ".xls";
		} catch (Exception e) {
			log.error("标准清单下载异常！", e);
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-12-3
	 * @description:树加载
	 * @return
	 */
	public String getChildStandards() {
		try {
			List<JecnTreeBean> list = standardService.getChildStandardsWeb(node, getProjectId(), getPeopleId(), this
					.isAdmin());
			if (list != null && list.size() > 0) {
				List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
				for (JecnTreeBean jecnTreeBean : list) {
					WebTreeBean webTreeBean = new WebTreeBean();
					webTreeBean.setId(jecnTreeBean.getId().toString());
					webTreeBean.setText(jecnTreeBean.getName());
					webTreeBean.setLeaf(!jecnTreeBean.isChildNode());
					webTreeBean.setType(jecnTreeBean.getTreeNodeType().toString());
					webTreeBean.setIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString() + ".gif");
					if (jecnTreeBean.getRelationId() != null) {
						webTreeBean.setRelationId(jecnTreeBean.getRelationId().toString());
					}
					listWebTreeBean.add(webTreeBean);

				}
				JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	public String getStandardList() {
		StandardSearchBean standardSearchBean = new StandardSearchBean();
		standardSearchBean.setStandardName(standardName);
		standardSearchBean.setStandardType(standardType);
		standardSearchBean.setOrgLookId(orgLookId);
		standardSearchBean.setOrgLookName(orgLookName);
		standardSearchBean.setPerId(perId);
		standardSearchBean.setPerName(perName);
		standardSearchBean.setPosLookId(posLookId);
		standardSearchBean.setPosLookName(posLookName);
		standardSearchBean.setSecretLevel(secretLevel);

		try {
			int total = standardService.getTotalStandardList(standardSearchBean, getPeopleId(), getProjectId(),
					isAdmin());
			List<StandardWebBean> list = standardService.getStandardList(standardSearchBean, start, limit,
					getPeopleId(), getProjectId(), isAdmin());
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);

		} catch (Exception e) {
			log.error("", e);
		}

		return null;
	}

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的标准
	 * @return
	 */
	public String attenTionStandard() {
		AttenTionSearchBean attenTionSearchBean = new AttenTionSearchBean();
		attenTionSearchBean.setName(standardName);
		attenTionSearchBean.setPosId(posLookId);
		attenTionSearchBean.setSecretLevel(secretLevel);
		attenTionSearchBean.setUserId(getPeopleId());
		try {
			int total = standardService.getTotalAttenTionStandard(attenTionSearchBean, getProjectId());
			List<StandardWebBean> list = standardService.getAttenTionStandard(attenTionSearchBean, start, limit,
					getProjectId());
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	public String standardListByPid() {
		try {
			int total = standardService.getTotalStandardListByPid(standardId, getPeopleId(), getProjectId(), isAdmin());
			if (total > 0) {
				List<StandardWebBean> listRuleInfoBean = standardService.getStandardListByPid(standardId, start, limit,
						getPeopleId(), getProjectId(), isAdmin());
				JSONArray jsonArray = JSONArray.fromObject(listRuleInfoBean);
				outJsonPage(jsonArray.toString(), total);
			} else {
				outJsonPage("[]", total);
			}

		} catch (Exception e) {
			log.error("双击标准目录查询标准列表错误！", e);
		}
		return null;
	}

	/**
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() throws Exception {
		// 获得文件
		if (content == null) {
			// 下载文件信息异常
			outResultHtml(this.getText("fileInformation"), "");
			throw new IllegalArgumentException("getInputStream方法中参数为空!");
		}
		return new ByteArrayInputStream(content);
	}

	/**
	 * @author yxw 2013-3-5
	 * @description:标准文件详情
	 * @return
	 */
	public String standardDetail() {
		try {

			standardDetailBean = standardService.getStandardDetail(standardId, standardType);
		} catch (Exception e) {
			log.error("获取制度内容出错", e);
		}
		return SUCCESS;
	}

	/**
	 * 双击树节点
	 * 
	 * @return
	 */
	public String standard() {
		ruleTypeIsShow = JecnUtil.ifValueIsOneReturnTrue("ruleType");
		if (standardType == 1 || standardType == 4 || standardType == 5) {
			try {
				standardDetailBean = standardService.getStandardDetail(standardId, standardType);
			} catch (Exception e) {
				log.error("标准详情出错");
			}
			if (standardDetailBean == null) {// 文件已删除!
				return JecnFinal.IS_DELETE;
			}
			return "standardDetail";
		} else if (standardType == 2) {
			if (!getStandardRelProcessObj()) {
				return JecnFinal.NOPUBLIC;
			}
			return "standardProcess";
		} else if (standardType == 3) {
			if (!getStandardRelProcessObj()) {
				return JecnFinal.NOPUBLIC;
			}
			return "standardProcessMap";
		}
		return null;
	}

	/**
	 * 获取流程标准。地图标准 节点类型和ID
	 */
	private boolean getStandardRelProcessObj() {
		try {
			Object[] objects = standardService.getRelFlowObject(standardId);
			if (objects == null) {
				log.error("标准流程未发布，或打开异常！");
				return false;
			}
			relFlowId = Long.valueOf(objects[0].toString());
			mapType = "1".equals(objects[1].toString()) ? "process" : "processMap";
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		}
		return true;
	}

	public long getNode() {
		return node;
	}

	public void setNode(long node) {
		this.node = node;
	}

	public IStandardService getStandardService() {
		return standardService;
	}

	public void setStandardService(IStandardService standardService) {
		this.standardService = standardService;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public int getStandardType() {
		return standardType;
	}

	public void setStandardType(int standardType) {
		this.standardType = standardType;
	}

	public long getOrgLookId() {
		return orgLookId;
	}

	public void setOrgLookId(long orgLookId) {
		this.orgLookId = orgLookId;
	}

	public String getOrgLookName() {
		return orgLookName;
	}

	public void setOrgLookName(String orgLookName) {
		this.orgLookName = orgLookName;
	}

	public long getPosLookId() {
		return posLookId;
	}

	public void setPosLookId(long posLookId) {
		this.posLookId = posLookId;
	}

	public String getPosLookName() {
		return posLookName;
	}

	public void setPosLookName(String posLookName) {
		this.posLookName = posLookName;
	}

	public long getSecretLevel() {
		return secretLevel;
	}

	public void setSecretLevel(long secretLevel) {
		this.secretLevel = secretLevel;
	}

	public long getPerId() {
		return perId;
	}

	public void setPerId(long perId) {
		this.perId = perId;
	}

	public String getPerName() {
		return perName;
	}

	public void setPerName(String perName) {
		this.perName = perName;
	}

	public Long getStandardId() {
		return standardId;
	}

	public void setStandardId(Long standardId) {
		this.standardId = standardId;
	}

	public StandardDetailBean getStandardDetailBean() {
		return standardDetailBean;
	}

	public void setStandardDetailBean(StandardDetailBean standardDetailBean) {
		this.standardDetailBean = standardDetailBean;
	}

	public int getCallType() {
		return callType;
	}

	public void setCallType(int callType) {
		this.callType = callType;
	}

	public IStandardListService getListStandService() {
		return listStandService;
	}

	public void setListStandService(IStandardListService listStandService) {
		this.listStandService = listStandService;
	}

	public boolean isShowAll() {
		return showAll;
	}

	public void setShowAll(boolean showAll) {
		this.showAll = showAll;
	}

	public String getFileName() {
		return JecnCommon.getDownFileNameByEncoding(getResponse(), fileName, "");
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMapType() {
		return mapType;
	}

	public Long getRelFlowId() {
		return relFlowId;
	}

	public boolean isRuleTypeIsShow() {
		return ruleTypeIsShow;
	}

	public void setRuleTypeIsShow(boolean ruleTypeIsShow) {
		this.ruleTypeIsShow = ruleTypeIsShow;
	}

}
