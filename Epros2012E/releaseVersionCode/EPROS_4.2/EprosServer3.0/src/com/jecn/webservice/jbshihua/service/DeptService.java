package com.jecn.webservice.jbshihua.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.webBean.dataImport.sync.JecnOrgView;

@Service
public interface DeptService extends IBaseService<JecnOrgView, String> {

	public int recevieOrg(List<JecnOrgView> depts) throws Exception;
	
	public int recevieOrgJB(List<JecnOrgView> depts) throws Exception;

	public List<JecnOrgView> getAllDepts() throws Exception;
}
