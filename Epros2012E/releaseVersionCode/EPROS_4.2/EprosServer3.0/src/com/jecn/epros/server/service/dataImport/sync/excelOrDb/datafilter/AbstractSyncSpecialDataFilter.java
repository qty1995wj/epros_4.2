package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter;

import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public abstract class AbstractSyncSpecialDataFilter extends AbstractSyncDataFilter {

	protected String specialDataPath;

	public AbstractSyncSpecialDataFilter(BaseBean baseBean) {
		super(baseBean);
		initSpecialDataPath();
	}

	@Override
	protected void addedSpecialDataToSyncData() {
		// 读取设计器固定数据
		SyncSpecialExcelDataReader specialDataReader = new SyncSpecialExcelDataReader(specialDataPath);
		specialDataReader.readExcelData();
		syncData.getDeptBeanList().addAll(specialDataReader.getDeptData());
		syncData.getUserPosBeanList().addAll(specialDataReader.getUserPosData());
		for (UserBean userBean : specialDataReader.getUserPosData()) {
			BaseBean.syncNotUpdateUsers.add(userBean.getUserNum());
		}
	}

	protected abstract void initSpecialDataPath();
}
