package com.jecn.epros.server.service.integration.impl;

import java.sql.Clob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.bean.integration.JecnControlTarget;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.integration.IJecnControlTargetDao;
import com.jecn.epros.server.dao.integration.IJecnRiskDao;
import com.jecn.epros.server.service.integration.IJecnRiskService;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.integration.RiskDataListBean;
import com.jecn.epros.server.webBean.integration.RiskDetailBean;
import com.jecn.epros.server.webBean.integration.RiskList;
import com.jecn.epros.server.webBean.integration.RiskRelatedProcessBean;
import com.jecn.epros.server.webBean.integration.RiskRelatedRuleBean;
import com.jecn.epros.server.webBean.integration.RiskSystemSearchBean;
import com.jecn.epros.server.webBean.integration.RiskWebBean;

/*******************************************************************************
 * 风险表操作类 2013-10-30
 * 
 */

@Transactional(rollbackFor = Exception.class)
public class JecnRiskServiceImpl extends AbsBaseService<JecnRisk, Long> implements IJecnRiskService {
	private Logger log = Logger.getLogger(JecnRiskServiceImpl.class);
	private IJecnRiskDao jecnRiskDao;
	private IJecnControlTargetDao jecnControlTargetDao;

	public IJecnRiskDao getJecnRiskDao() {
		return jecnRiskDao;
	}

	public void setJecnRiskDao(IJecnRiskDao jecnRiskDao) {
		this.jecnRiskDao = jecnRiskDao;
	}

	public IJecnControlTargetDao getJecnControlTargetDao() {
		return jecnControlTargetDao;
	}

	public void setJecnControlTargetDao(IJecnControlTargetDao jecnControlTargetDao) {
		this.jecnControlTargetDao = jecnControlTargetDao;
	}

	/**
	 * 更新tpath 和tlevel
	 * 
	 * @param jecnFlowStructureT
	 * @throws Exception
	 */
	private void updateTpathAndTLevel(JecnRisk jecnRisk) throws Exception {
		// 获取上级节点
		JecnRisk preRisk = jecnRiskDao.get(jecnRisk.getParentId());
		if (preRisk == null) {
			jecnRisk.settLevel(0);
			jecnRisk.settPath(jecnRisk.getId() + "-");
			jecnRisk.setViewSort(JecnUtil.concatViewSort("", jecnRisk.getSort()));
		} else {
			jecnRisk.settLevel(preRisk.gettLevel() + 1);
			jecnRisk.settPath(preRisk.gettPath() + jecnRisk.getId() + "-");
			jecnRisk.setViewSort(JecnUtil.concatViewSort(preRisk.getViewSort(), jecnRisk.getSort()));
		}
		// 更新
		jecnRiskDao.update(jecnRisk);
		jecnRiskDao.getSession().flush();
	}

	@Override
	public Long addJecnRiskOrDir(JecnRisk jecnRisk) throws Exception {
		// 新建风险
		jecnRiskDao.save(jecnRisk);
		// 更新tpath 和tlevel
		updateTpathAndTLevel(jecnRisk);
		// 控制目标
		if (jecnRisk.getListControlTarget() != null && jecnRisk.getListControlTarget().size() > 0) {
			for (JecnControlTarget jecnControlTarget : jecnRisk.getListControlTarget()) {
				if (jecnRisk.getId() != null) {
					jecnControlTarget.setRiskId(jecnRisk.getId());
					jecnControlTargetDao.save(jecnControlTarget);
				}
			}
		}
		// 内控指引知识库
		if (jecnRisk.getListControlGuide() != null) {
			for (JecnControlGuide jecnControlGuide : jecnRisk.getListControlGuide()) {
				jecnRiskDao.getSQLQuery(
						"insert into JECN_RISK_GUIDE (id,risk_id,guide_id) values('" + JecnCommon.getUUID() + "',"
								+ jecnRisk.getId() + "," + jecnControlGuide.getId() + ")").executeUpdate();
			}
		}
		JecnUtil.saveJecnJournal(jecnRisk.getId(), jecnRisk.getRiskName(), 9, 1, jecnRisk.getCreatePersonId(),
				jecnRiskDao);
		return jecnRisk.getId();
	}

	@Override
	public void reRiskName(String newName, Long riskId, Long updatePersonId) throws Exception {
		try {
			String hql = "update JecnRisk set riskName = '" + newName + "',updatePersonId=? where id = ? and isDir = 0";
			jecnRiskDao.execteHql(hql, updatePersonId, riskId);
			JecnUtil.saveJecnJournal(riskId, newName, 9, 2, updatePersonId, jecnRiskDao,4);
		} catch (Exception e) {
			log.error("风险重命名出错", e);
		}
	}

	@Override
	public int deleteRisk(List<Long> riskIds, Long peopleId) throws Exception {
		try {
			if (riskIds.size() > 0) {

				String sqlCommon = JecnCommonSql.getChildObjectsSqlGetSingleIdRisk(riskIds);

				List<Long> listIds = jecnRiskDao.listObjectNativeSql(sqlCommon, "id", Hibernate.LONG);
				if (listIds.size() > 0) {
					// 添加日志
					JecnUtil.saveJecnJournals(listIds, 9, 3, peopleId, jecnRiskDao);
					// 删除合理化建议 0流程，1制度，2流程架构，3文件，4风险，5标准
					JecnDaoUtil.deleteProposeByRelatedIds(listIds, 4, jecnRiskDao);
				}
				// 风险和风险控制目标
				String sql = "delete from JECN_CONTROLTARGET where RISK_ID in (" + sqlCommon + ")";
				jecnRiskDao.execteNative(sql);
				// JECN_CONTROL_POINT 风险与活动
				sql = "delete from JECN_CONTROL_POINT_T where RISK_ID in (" + sqlCommon + ")";
				jecnRiskDao.execteNative(sql);
				sql = "delete from JECN_CONTROL_POINT where RISK_ID in (" + sqlCommon + ")";
				jecnRiskDao.execteNative(sql);
				// 风险与制度
				sql = "delete from JECN_RULE_RISK_T where RISK_ID in (" + sqlCommon + ")";
				jecnRiskDao.execteNative(sql);
				sql = "delete from JECN_RULE_RISK where RISK_ID in (" + sqlCommon + ")";
				jecnRiskDao.execteNative(sql);
				// 风险与内控关联
				sql = "DELETE FROM JECN_RISK_GUIDE WHERE RISK_ID in (" + sqlCommon + ")";
				jecnRiskDao.execteNative(sql);
				// 删除风险表数据
				sql = "delete from Jecn_Risk where id in (" + sqlCommon + ")";
				jecnRiskDao.execteNative(sql);
				// 删除收藏
				sql = "delete from jecn_my_star where type=3 and related_id in (" + sqlCommon + ")";
				jecnRiskDao.execteNative(sql);

				return 1;
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return 0;
	}

	@Override
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		try {
			String t_path = "";
			int level = -1;
			if (pId.intValue() != 0) {
				JecnRisk jecnRisk = this.jecnRiskDao.get(pId);
				t_path = jecnRisk.gettPath();
				level = jecnRisk.gettLevel();
			}
			JecnCommon.updateNodesChildTreeBeans(listIds, pId, "", new Date(), updatePersonId, t_path, level,
					jecnRiskDao, moveNodeType, 1);
			// 添加日志
			for (Long id : listIds) {
				JecnUtil.saveJecnJournal(id, null, 9, 4, updatePersonId, jecnRiskDao);
			}
		} catch (Exception e) {
			log.error("风险节点移动出错", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getChildsRisk(Long pid, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.id,t.parent_id,t.risk_code,t.is_dir,t.grade,t.sort,"
					+ "t.standardcontrol,t.create_person,t.create_time,t.update_person,t.update_time,t.note,t.name, "
					+ " case when jr.id is null then 0 else 1 end as count " + " from JECN_RISK t"
					+ " LEFT JOIN JECN_RISK JR ON JR.parent_id=T.ID" + " where t.parent_id=" + pid
					+ " and  t.PROJECT_ID =? order by t.parent_id,t.sort,t.id";
			List<Object[]> list = jecnRiskDao.listNativeSql(sql, projectId);
			return getJecnTreeBeanByObject(list);
		} catch (Exception e) {
			log.error("获取子节点数据出错", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChildsRisk(Long pid, Long projectId, Long peopleId) throws Exception {
		List<Object[]> list = jecnRiskDao.getRoleAuthChildsRisk(pid, projectId, peopleId);
		return getJecnTreeBeanByObject(list);
	}

	private List<JecnTreeBean> getJecnTreeBeanByObject(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[3] == null || obj[5] == null || obj[12] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 风险ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// //风险描述/目录名称
			// if (obj[12] != null) {
			// jecnTreeBean.setName(obj[12].toString());
			// } else {
			// jecnTreeBean.setName("");
			// }
			// 风险节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[1].toString()));
			// 风险节点父类名称
			jecnTreeBean.setPname("");
			// 风险类型
			if ("0".equals(obj[3].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.riskDir);
				// 风险描述/目录名称
				if (!"".equals(obj[12])) {
					jecnTreeBean.setName(obj[12].toString());
				} else {
					jecnTreeBean.setName("");
				}
			} else if ("1".equals(obj[3].toString())) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.riskPoint);
				// 风险描述/目录名称
				if (obj[2] != null && !"".equals(obj[2]) && !"".equals(obj[12])) {
					jecnTreeBean.setContent(obj[12].toString());
					if (obj[12].toString().length() > 8) {
						obj[12] = obj[12].toString().substring(0, 8) + "...";
					}
					jecnTreeBean.setName(obj[2] + " " + obj[12]);
				} else {
					jecnTreeBean.setName("");
				}
			}
			// 风险编号
			if (obj[2] != null) {
				jecnTreeBean.setNumberId(obj[2].toString());
			} else {
				jecnTreeBean.setNumberId("");
			}
			// 是否发布
			jecnTreeBean.setPub(false);
			// 是否子节点
			if (Integer.parseInt(obj[13].toString()) > 0) {
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception {
		try {

			String pViewSort = "";
			if (pId.intValue() != 0) {
				String hql = "from JecnRisk where id=?";
				List<JecnRisk> data = jecnRiskDao.listHql(hql, pId);
				pViewSort = data.get(0).getViewSort();
			}

			String hql = "from JecnRisk where parentId=? ";
			List<JecnRisk> listJecnRisk = jecnRiskDao.listHql(hql, pId);
			List<JecnRisk> listUpdate = new ArrayList<JecnRisk>();
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				for (JecnRisk jecnRisk : listJecnRisk) {
					if (jecnTreeDragBean.getId().equals(jecnRisk.getId())) {
						if (jecnRisk.getSort() == null
								|| (jecnTreeDragBean.getSortId() != jecnRisk.getSort().intValue())) {
							jecnRisk.setSort(jecnTreeDragBean.getSortId());
							jecnRisk.setViewSort(JecnUtil.concatViewSort(pViewSort, jecnTreeDragBean.getSortId()));
							listUpdate.add(jecnRisk);
							break;
						}
					}
				}

			}

			for (JecnRisk jecnRisk : listUpdate) {
				jecnRiskDao.update(jecnRisk);
				JecnUtil.saveJecnJournal(jecnRisk.getId(), jecnRisk.getRiskName(), 9, 5, updatePersonId, jecnRiskDao);
			}
		} catch (Exception e) {
			log.error("风险节点排序出错", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getAllRisk() throws Exception {
		try {
			String sql = "select t.id,t.parent_id,t.risk_code,t.is_dir,t.grade,t.sort,t.standardcontrol,t.create_person,t.create_time,t.update_person,t.update_time,t.note,t.name, "
					+ "            (select count(*) from JECN_RISK where parent_id=t.id) as count "
					+ "            from JECN_RISK t " + "           order by t.parent_id,t.sort,t.id";

			List<Object[]> list = jecnRiskDao.listNativeSql(sql);
			return getJecnTreeBeanByObject(list);
		} catch (Exception e) {
			log.error("获取子节点数据出错", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long id) throws Exception {
		return null;
	}

	@Override
	public void addJecnControlTarget(List<JecnControlTarget> jecnControlTargetList) throws Exception {
		for (JecnControlTarget jecnControlTarget : jecnControlTargetList) {
			jecnControlTargetDao.save(jecnControlTarget);
		}
	}

	@Override
	public JecnRisk getJecnRiskById(Long id) throws Exception {
		String hql = "from JecnRisk where id = " + id;
		JecnRisk jecnRisk = jecnRiskDao.getObjectHql(hql);

		if (jecnRisk == null) {
			return null;
		}
		String sql = "select ct.id,ct.risk_id,ct.description,ct.sort,ct.create_person,ct.create_time,ct.update_person,ct.update_time,ct.note from Jecn_Controltarget ct where ct.risk_id = "
				+ id + " order by sort";
		List<Object[]> listObj = jecnControlTargetDao.listNativeSql(sql);

		// 控制目标
		List<JecnControlTarget> listControlTarget = new ArrayList<JecnControlTarget>();
		for (Object[] obj : listObj) {
			JecnControlTarget jecnControlTarget = new JecnControlTarget();
			if (obj[0] != null && !"".equals(obj[0])) {
				jecnControlTarget.setId(Long.valueOf(obj[0].toString()));
			}
			if (obj[1] != null && !"".equals(obj[1])) {
				jecnControlTarget.setRiskId(Long.valueOf(obj[1].toString()));
			}
			if (obj[2] != null && !"".equals(obj[2])) {
				jecnControlTarget.setDescription(obj[2].toString());
			}
			if (obj[3] != null && !"".equals(obj[3])) {
				jecnControlTarget.setSort(Integer.valueOf(obj[3].toString()));
			}
			if (obj[4] != null && !"".equals(obj[4])) {
				jecnControlTarget.setCreatePersonId(Long.valueOf(obj[4].toString()));
			}
			if (obj[5] != null && !"".equals(obj[5])) {
				jecnControlTarget.setCreateTime((Date) obj[5]);
			}
			if (obj[6] != null && !"".equals(obj[6])) {
				jecnControlTarget.setUpdatePersonId(Long.valueOf(obj[6].toString()));
			}
			if (obj[7] != null && !"".equals(obj[7])) {
				jecnControlTarget.setUpdateTime((Date) obj[7]);
			}
			if (obj[8] != null && !"".equals(obj[8])) {
				jecnControlTarget.setNote(obj[8].toString());
			}
			listControlTarget.add(jecnControlTarget);
		}
		sql = "select jcg.id,jcg.parent_id,jcg.name from JECN_CONTROL_GUIDE jcg,JECN_RISK_GUIDE jrg where jcg.id=jrg.guide_id and jrg.risk_id = "
				+ id;
		List<Object[]> list = jecnRiskDao.listNativeSql(sql);
		// 内控指引知识库
		List<JecnControlGuide> listControlGuide = new ArrayList<JecnControlGuide>();
		for (Object[] obj : list) {
			JecnControlGuide jecnControlGuide = new JecnControlGuide();
			if (obj[0] != null && !"".equals(obj[0])) {
				jecnControlGuide.setId(Long.valueOf(obj[0].toString()));
			}
			if (obj[1] != null && !"".equals(obj[1])) {
				jecnControlGuide.setParentId(Long.valueOf(obj[1].toString()));
			}
			if (obj[2] != null && !"".equals(obj[2])) {
				jecnControlGuide.setName(obj[2].toString());
			}
			listControlGuide.add(jecnControlGuide);
		}
		jecnRisk.setListControlTarget(listControlTarget);
		jecnRisk.setListControlGuide(listControlGuide);
		return jecnRisk;
	}

	@Override
	public Long EditJecnRiskProperty(JecnRisk jecnRisk) throws Exception {
		jecnRiskDao.update(jecnRisk);

		// 获取数据库中风险点相关的控制目标数据
		String hql = "from JecnControlTarget where riskId = " + jecnRisk.getId();
		List<JecnControlTarget> dataControlTargets = jecnControlTargetDao.listHql(hql);

		if (jecnRisk.getListControlTarget() != null) {
			for (JecnControlTarget jecnControlTarget : jecnRisk.getListControlTarget()) {
				boolean isExit = false;
				for (JecnControlTarget dataControlTarget : dataControlTargets) {
					if (jecnControlTarget.getId() != null && !"".equals(jecnControlTarget.getId())) {
						if (jecnControlTarget.getId().equals(dataControlTarget.getId())) {
							dataControlTargets.remove(dataControlTarget);
							isExit = true;
							// 与数据库中ID相同的进行更新
							jecnControlTargetDao.getSession().merge(jecnControlTarget);
							break;
						}
					}
				}
				if (!isExit) {
					// 没有主键ID的进行添加
					jecnControlTargetDao.save(jecnControlTarget);
				}
			}
		}
		// 删除数据库中的数据，条件：数据库中的不存在于最新数据中的
		for (JecnControlTarget dataControlTarget : dataControlTargets) {
			jecnControlTargetDao.delete(dataControlTarget.getId());
		}
		// 获取数据库中风险点相关的内控指引知识库数据
		String sql = "select jcg.id,jcg.parent_id,jcg.name,jcg.description from JECN_CONTROL_GUIDE jcg,JECN_RISK_GUIDE jrg where jcg.id=jrg.guide_id and jrg.risk_id ="
				+ jecnRisk.getId();
		List<Object[]> objectControlGuides = jecnControlTargetDao.listNativeSql(sql);
		List<JecnControlGuide> dataControlGuides = getControlGuideByObject(objectControlGuides);
		if (jecnRisk.getListControlGuide() != null) {
			for (JecnControlGuide jecnControlGuide : jecnRisk.getListControlGuide()) {
				boolean isExit = false;
				for (JecnControlGuide dataControlGuide : dataControlGuides) {
					if (jecnControlGuide.getId() != null && !"".equals(jecnControlGuide.getId())) {
						if (jecnControlGuide.getId().equals(dataControlGuide.getId())) {
							dataControlGuides.remove(dataControlGuide);
							isExit = true;
							// //与数据库中ID相同的进行更新
							// jecnRiskDao.getSQLQuery("update JECN_RISK_GUIDE
							// set guide_id ="+jecnControlGuide.getId()+" where
							// risk_id="+jecnRisk.getId()).executeUpdate();
							break;
						}
					}
				}
				if (!isExit) {
					// 没有主键ID的进行添加
					jecnRiskDao.getSQLQuery(
							"insert into JECN_RISK_GUIDE (id,risk_id,guide_id) values('" + JecnCommon.getUUID() + "',"
									+ jecnRisk.getId() + "," + jecnControlGuide.getId() + ")").executeUpdate();
				}
			}
		}
		// 删除数据库中的数据，条件：数据库中的不存在于最新数据中的
		for (JecnControlGuide dataControlGuide : dataControlGuides) {
			jecnRiskDao.getSQLQuery("delete from JECN_RISK_GUIDE where guide_id = " + dataControlGuide.getId())
					.executeUpdate();
		}
		JecnUtil.saveJecnJournal(jecnRisk.getId(), jecnRisk.getRiskName(), 9, 2, jecnRisk.getUpdatePersonId(),
				jecnRiskDao);
		return jecnRisk.getId();
	}

	private List<JecnControlGuide> getControlGuideByObject(List<Object[]> objectControlGuides) {
		List<JecnControlGuide> dataControlGuides = new ArrayList<JecnControlGuide>();
		for (Object[] obj : objectControlGuides) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}
			JecnControlGuide jecnControlGuide = new JecnControlGuide();
			jecnControlGuide.setId(Long.valueOf(obj[0].toString()));
			jecnControlGuide.setParentId(Long.valueOf(obj[1].toString()));
			jecnControlGuide.setName(obj[2].toString());
			dataControlGuides.add(jecnControlGuide);
		}
		return dataControlGuides;
	}

	@Override
	public List<JecnTreeBean> getChildsRiskDirs(Long pid, Long projectId) throws Exception {
		try {
			String sql = "select t.id,t.parent_id,t.risk_code,t.is_dir,t.grade,t.sort,t.standardcontrol,t.create_person,t.create_time,t.update_person,t.update_time,t.note,t.name, "
					+ " (select count(*) from JECN_RISK where parent_id=t.id) as count "
					+ " from JECN_RISK t where t.parent_id="
					+ pid
					+ " and is_dir = 0 and PROJECT_ID =? "
					+ " order by t.parent_id,t.sort,t.id";
			List<Object[]> list = jecnRiskDao.listNativeSql(sql, projectId);
			return getJecnTreeBeanByObject(list);
		} catch (Exception e) {
			log.error("获取子节点数据出错", e);
			throw e;
		}
	}

	@Override
	public List<JecnControlTarget> getAllControlTargetByRiskId(Long riskId) throws Exception {
		String sql = "select ct.id,ct.risk_id,ct.description,ct.sort," + "  ct.create_person,ct.create_time,"
				+ " ct.update_person,ct.update_time,ct.note,js.risk_code"
				+ "  from Jecn_Controltarget ct,jecn_risk js where ct.risk_id = js.id and ct.risk_id=?"
				+ " order by ct.sort";
		List<Object[]> list = jecnRiskDao.listNativeSql(sql, riskId);
		return getControlTargeByObject(list);
	}

	private List<JecnControlTarget> getControlTargeByObject(List<Object[]> list) {
		List<JecnControlTarget> listResult = new ArrayList<JecnControlTarget>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
				continue;
			}
			JecnControlTarget jecnControlTarget = new JecnControlTarget();
			// 控制目标ID
			jecnControlTarget.setId(Long.valueOf(obj[0].toString()));
			// 风险ID
			jecnControlTarget.setRiskId(Long.valueOf(obj[1].toString()));
			// 控制目标描述
			jecnControlTarget.setDescription(obj[2].toString());
			// 控制目标排序编号
			jecnControlTarget.setSort(Integer.valueOf(obj[3].toString()));
			// 创建人
			jecnControlTarget.setCreatePersonId(Long.valueOf(obj[4].toString()));
			// 创建时间
			jecnControlTarget.setCreateTime((Date) obj[5]);
			// 更新人
			jecnControlTarget.setUpdatePersonId(Long.valueOf(obj[6].toString()));
			// 更新时间
			jecnControlTarget.setUpdateTime((Date) obj[7]);
			// 风险编号
			if (obj[9] != null && !"".equals(obj[9])) {
				jecnControlTarget.setRiskNum(obj[9].toString());
			}
			listResult.add(jecnControlTarget);
		}
		return listResult;
	}

	@Override
	public List<JecnTreeBean> searchRiskByName(String name) throws Exception {
		try {
			String sql = "select t.id,t.parent_id,t.risk_code,t.is_dir,t.grade,t.sort,t.standardcontrol,t.create_person,t.create_time,t.update_person,t.update_time,t.note,t.name, "
					+ " (select count(*) from JECN_RISK where parent_id=t.id) as count "
					+ " from JECN_RISK t where t.name like ? and is_dir = 1 " + " order by t.id";
			List<Object[]> list = jecnRiskDao.listNativeSql(sql, "%" + name + "%");
			return getJecnTreeBeanByObject(list);
		} catch (Exception e) {
			log.error("根据风险点描述搜索数据出错", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchRoleAuthByName(String name, Long peopleId) throws Exception {
		List<Object[]> list = jecnRiskDao.searchRoleAuthByName(name, peopleId);
		return getJecnTreeBeanByObject(list);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JecnTreeBean> getJecnRiskGuide(Long riskId) throws Exception {
		String sql = "select jcg.id,jcg.parent_id,jcg.name,jcg.description from JECN_CONTROL_GUIDE jcg,JECN_RISK_GUIDE jrg where jcg.id=jrg.guide_id and jrg.risk_id = "
				+ riskId;
		List<Object[]> list = jecnRiskDao.getSQLQuery(sql).list();
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 主键ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 父ID
			jecnTreeBean.setPid(Long.valueOf(obj[1].toString()));
			// 内控指引知识库名称及详细条款
			jecnTreeBean.setName(obj[2].toString());
			// 内控指引知识库详细条款
			if (obj[3] != null) {
				jecnTreeBean.setContent(obj[3].toString());
			} else {
				jecnTreeBean.setContent("");
			}
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	/**
	 * 查询探索风险总数
	 */
	@Override
	public int getRiskTotal(RiskSystemSearchBean riskSystemSearchBean, Long projectId) throws Exception {
		try {
			String sql = "select count(distinct t.id)" + this.getSql(riskSystemSearchBean, projectId);
			return jecnRiskDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 探索通用查询条件
	 * 
	 * @param riskSystemSearchBean
	 * @return
	 */
	private String getSql(RiskSystemSearchBean riskSystemSearchBean, Long projectId) {
		String sql = "";
		if (riskSystemSearchBean.getOrgId() != null && riskSystemSearchBean.getOrgId() != -1) {
			sql = "from JECN_RISK t," + "JECN_CONTROLTARGET        jc," + "      JECN_CONTROL_POINT        jcp,"
					+ "      JECN_FLOW_STRUCTURE_IMAGE jfsi," + "      JECN_FLOW_STRUCTURE       jfs,"
					+ "      JECN_FLOW_RELATED_ORG     jfro," + "      JECN_FLOW_ORG             jfo"
					+ " where t.IS_DIR = 1 and t.project_id = " + projectId;
		} else {
			sql = "from JECN_RISK t where t.IS_DIR = 1 and t.project_id = " + projectId;
		}

		// 风险名称
		if (StringUtils.isNotBlank(riskSystemSearchBean.getName())) {
			String[] riskName = riskSystemSearchBean.getName().split(" ");
			String lsql = "";
			int namecount = 0;
			for (int f = 0; f < riskName.length; f++) {
				// 文件名称
				if (riskName[f].trim() != null && !"".equals(riskName[f].trim())) {
					if (namecount == 0) {
						lsql = lsql + "select name from JECN_RISK where name like '%" + riskName[f].trim() + "%' ";
					} else {
						lsql = lsql + " or name like '%" + riskName[f].trim() + "%'";
					}
					namecount++;
				}
			}
			sql = sql + " and t.name in(" + lsql + ") ";
		}

		// 风险编号
		if (StringUtils.isNotBlank(riskSystemSearchBean.getRiskCode())) {
			String[] riskNum = riskSystemSearchBean.getRiskCode().split(" ");
			String nsql = "";
			int numcount = 0;
			for (int f = 0; f < riskNum.length; f++) {
				if (riskNum[f].trim() != null && !"".equals(riskNum[f].trim())) {
					if (numcount == 0) {
						nsql = nsql + " select RISK_CODE from JECN_RISK where RISK_CODE like '%" + riskNum[f].trim()
								+ "%'";
					} else {
						nsql = nsql + " or RISK_CODE like '%" + riskNum[f].trim() + "%'";
					}
					numcount++;
				}
			}
			sql = sql + " and t.RISK_CODE in (" + nsql + ") ";
		}
		if (riskSystemSearchBean.getOrgId() != null && riskSystemSearchBean.getOrgId() != -1) {
			sql = sql + " and jc.risk_id = t.id" + "   and jcp.target_id = jc.id"
					+ "   and jcp.active_id = jfsi.figure_id" + "   and jfsi.flow_id = jfs.flow_id"
					+ "   and jfro.flow_id = jfs.flow_id" + "   and jfro.org_id = jfo.org_id" + " and jfo.org_id = "
					+ riskSystemSearchBean.getOrgId();
		}
		return sql;
	}

	/**
	 * 探索风险列表
	 */
	@Override
	public List<RiskWebBean> getRiskList(RiskSystemSearchBean riskSystemSearchBean, int start, int limit, Long projectId)
			throws Exception {
		try {
			String sql = "select distinct t.id,t.name,t.RISK_CODE,t.GRADE "
					+ this.getSql(riskSystemSearchBean, projectId);
			sql += " order by t.id";
			List<Object[]> list = jecnRiskDao.listNativeSql(sql, start, limit, projectId);
			List<RiskWebBean> listResult = new ArrayList<RiskWebBean>();
			for (Object[] object : list) {
				RiskWebBean riskWebBean = JecnUtil.getRiskWebBeanByObject(object);
				if (riskWebBean != null) {
					listResult.add(riskWebBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int riskNumIsRepeat(String riskNum, Long riskId) throws Exception {
		String sql = "";
		if (riskId == null) {
			sql = "select * from jecn_risk where RISK_CODE = ?";
		} else {
			sql = "select * from jecn_risk where RISK_CODE = ? and id not in (" + riskId + ")";
		}
		return this.jecnRiskDao.listNativeSql(sql, riskNum).size();
	}

	/**
	 * 根据制度ID获取制度关联风险信息
	 * 
	 * @param ruleId
	 *            制度主键ID
	 * @return List<JecnTreeBean>制度相关风险点集合
	 */
	@Override
	public List<JecnTreeBean> findJecnRuleRiskBeanTByRuleId(Long ruleId) throws Exception {
		String sql = "SELECT JR.ID, JR.NAME,JR.RISK_CODE" + "  FROM JECN_RULE_RISK_T RT, JECN_RISK JR"
				+ " WHERE RT.RISK_ID = JR.ID  AND RT.RULE_ID = ?";

		List<Object[]> listObjects = this.jecnRiskDao.listNativeSql(sql, ruleId);
		return JecnUtil.getJecnTreeBeanByObjectRisk(listObjects);
	}

	/**
	 * 根据风险主键ID查询基本信息
	 * 
	 * @author ZHF
	 */
	@Override
	public RiskDetailBean getJecnRiskByIdForWeb(Long id) throws Exception {
		try {
			// 详情
			Long riskId = null;
			String riskCode = null;
			String name = null;
			Long grade = null;
			String standardControl = "";
			String gradeStr = null;
			StringBuffer bufferName = new StringBuffer();
			String sql = "SELECT JR.ID, JR.RISK_CODE, JR.NAME, JR.GRADE, JR.STANDARDCONTROL, "
					+ "JCG.Description FROM JECN_RISK JR LEFT JOIN JECN_RISK_GUIDE JRG ON JR.ID "
					+ "= JRG.RISK_ID LEFT JOIN JECN_CONTROL_GUIDE JCG ON JRG.GUIDE_ID = JCG.ID " + "WHERE JR.ID = "
					+ id;
			List<Object[]> listObject = jecnRiskDao.listNativeSql(sql);
			int count = 0;
			int c = 0;
			RiskDetailBean detailBean = new RiskDetailBean();
			for (Object[] objects : listObject) {
				count++;
				riskId = Long.valueOf(objects[0].toString());
				riskCode = objects[1].toString();
				name = objects[2].toString();
				grade = Long.valueOf(objects[3].toString());
				if (Long.valueOf(objects[3].toString()) == 1) {
					gradeStr = JecnUtil.getValue("high");
					;
				}
				if (Long.valueOf(objects[3].toString()) == 2) {
					gradeStr = JecnUtil.getValue("In");
				}
				if (Long.valueOf(objects[3].toString()) == 3) {
					gradeStr = JecnUtil.getValue("Low");
				}
				if (objects[4] == null) {
					standardControl = null;
				} else {
					standardControl = objects[4].toString();
				}
				if (objects[5] == null) {
					bufferName.append("&nbsp;");
				} else {

					c++;
					if (count - 1 < listObject.size()) {

						bufferName.append(JecnUtil.getValue("inTheFirst") + c + JecnUtil.getValue("item") + "：")
								.append(objects[5].toString()).append("<br><br>");
					}
				}
			}

			// String ruleName = null;
			// String ruleNumber = null;
			// String orgName = null;
			// String ruleType = null;
			// // RiskRelatedRuleBean riskRelatedRuleBean = new
			// RiskRelatedRuleBean();
			// String sqlRule =
			// "select jru.rule_name, jru.rule_number, jft.type_name,
			// jfo.org_name"
			// + " from jecn_risk jr"
			// + " left join jecn_rule_risk_t jrr on jr.id = jrr.risk_id"
			// + " left join jecn_rule jru on jrr.rule_id = jru.id"
			// + " left join jecn_flow_type jft on jft.type_id = jru.type_id"
			// + " left join jecn_flow_org jfo on jru.org_id = jfo.org_id"
			// + " where jr.id = "+id;
			// List<Object[]> listObjectRule =
			// this.jecnRiskDao.listNativeSql(sqlRule);
			// for (Object[] objects : listObjectRule) {
			// ruleName = objects[0].toString();
			// ruleNumber = objects[1].toString();
			// ruleType = objects[2].toString();
			// orgName = objects[3].toString();
			// }

			detailBean.setRiskId(riskId);
			detailBean.setRiskCode(riskCode);
			detailBean.setName(name);
			detailBean.setGrade(grade);
			detailBean.setGradeStr(gradeStr);
			detailBean.setStandardControl(standardControl);
			detailBean.setControlGuide(bufferName.toString());
			// detailBean.setRuleName(ruleName);
			// detailBean.setRuleNumber(ruleNumber);
			// detailBean.setOrgName(orgName);
			// detailBean.setRuleType(ruleType);
			return detailBean;
		} catch (Exception e) {
			log.error("根据风险主键ID查询基本信息出错", e);
			throw e;
		}
	}

	/**
	 * 制度相关标准
	 * 
	 * @param ruleId
	 * @return List<JecnTreeBean>制度相关标准tree集合
	 * @throws Exception
	 */
	@Override
	public List<JecnTreeBean> findJecnRuleStandardBeanTByRuleId(Long ruleId) throws Exception {
		String sql = "SELECT B.CRITERION_CLASS_ID," + "       B.CRITERION_CLASS_NAME,"
				+ "       B.PRE_CRITERION_CLASS_ID," + "       B.STAN_TYPE," + "       B.RELATED_ID,"
				+ "       0 AS HASCHILD " + "  FROM JECN_RULE_STANDARD_T A, JECN_CRITERION_CLASSES B"
				+ " WHERE A.STANDARD_ID = B.CRITERION_CLASS_ID" + "   AND A.RULE_ID = ?";

		List<Object[]> listObjects = this.jecnRiskDao.listNativeSql(sql, ruleId);

		List<JecnTreeBean> standardsRelateds = new ArrayList<JecnTreeBean>();
		for (Object[] objects : listObjects) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectStandard(objects);
			if (jecnTreeBean == null) {
				continue;
			}
			standardsRelateds.add(jecnTreeBean);
		}
		return standardsRelateds;
	}

	/**
	 * 根据风险目录ID查询风险总数
	 */
	@Override
	public int getRiskTotalByPid(Long pid) throws Exception {
		List<Long> listIds = new ArrayList<Long>();
		listIds.add(pid);
		String sql = JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.riskDir);

		sql = "select count(jr.id) from (" + sql + ") jr where jr.is_dir = 1";
		try {
			return this.jecnRiskDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("JecnRiskServiceImpl根据风险目录ID查询风险总数出错：", e);
			throw e;
		}
	}

	/**
	 * 根据风险目录ID查询所有风险信息分页方法
	 */
	@Override
	public List<RiskWebBean> getRiskListByPid(Long pid, int start, int limit) throws Exception {
		List<Long> listIds = new ArrayList<Long>();
		listIds.add(pid);
		String sql = JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.riskDir);

		sql = "select jr.id,jr.risk_code,jr.name,jr.grade from jecn_risk jr inner join (" + sql
				+ ") risk on risk.id=jr.id where jr.is_dir = 1 order by jr.parent_id,jr.sort," + "jr.id";
		try {
			List<Object[]> list = this.jecnRiskDao.listNativeSql(sql, start, limit);
			List<RiskWebBean> listResult = new ArrayList<RiskWebBean>();
			for (Object[] object : list) {
				RiskWebBean riskWebBean = JecnUtil.getRiskWebBeanByObject(object);
				if (riskWebBean != null) {
					listResult.add(riskWebBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("JecnRiskServiceImpl根据风险目录ID查询所有风险信息分页方法出错：", e);
			throw e;
		}
	}

	/***************************************************************************
	 * 
	 * 根据风险目录ID获取当前目录下所有风险控制清单 由于原先的方法已经不能够满足当前清单显示要求，由此出现了此方法
	 * 
	 * @author chehuanbo
	 * @since V3.06
	 */
	public RiskList getRiskInventoryALL(Long id, Long projectId, int level, boolean isShowAll) throws Exception {
		// 风险清单数据bean
		RiskList rListBean = new RiskList();
		// 最大级别数
		int maxCount = 0;
		// 所有风险相关的数据
		List<Object[]> riskObjList = jecnRiskDao.getRiskInventoryALL(id, projectId, isShowAll);
		List<RiskDataListBean> listDataListBeans = new ArrayList<RiskDataListBean>();
		for (Object[] obj : riskObjList) {

			RiskDataListBean dataListBean = new RiskDataListBean();
			// 当前风险所在级别
			if (obj[0] != null && obj[3] != null) {
				int lev = Integer.valueOf(obj[0].toString());
				int isDir = Integer.valueOf(obj[3].toString());
				if (isDir == 0 && maxCount < lev) {
					maxCount = lev;
				}
				dataListBean.setLevel(lev);
			}
			// 风险等级
			if (obj[1] != null) {
				dataListBean.setRiskGrade(Integer.valueOf(obj[1].toString()));
			}
			// 风险ID
			if (obj[2] != null) {
				dataListBean.setRiskId(Long.valueOf(obj[2].toString()));
			}
			// 是否是目录
			if (obj[3] != null) {
				dataListBean.setIsDir(Integer.valueOf(obj[3].toString()));
			}
			// 风险编号
			if (obj[4] != null) {
				dataListBean.setRiskCode(obj[4].toString());
			}
			// 风险描述
			if (obj[5] != null) {
				dataListBean.setRiskName(obj[5].toString());
			}
			// 标准化控制
			if (obj[6] != null) {
				dataListBean.setStandardControl(obj[6].toString());
			}

			// 企业内部控制
			if (obj[8] != null && obj[9] != null) {
				dataListBean.setGuideVal(obj[8].toString() + " : " + obj[9].toString());
			}
			// 控制目标
			if (obj[11] != null) {
				dataListBean.setControlDescroption(obj[11].toString());
			}
			// 控制点编号
			if (obj[12] != null) {
				dataListBean.setControlNum(obj[12].toString());
			}
			// 控制活动描述
			if (obj[13] != null) {
				String activeShow = JecnCommonSql.clobToString((Clob) obj[13]);
				if (activeShow != null && !"".equals(activeShow)) {
					dataListBean.setActiveShow(activeShow.trim());
				}
			}
			// 活动编号
			if (obj[14] != null) {
				dataListBean.setActiveId(Long.valueOf(obj[14].toString()));
			}
			// 活动名称
			if (obj[15] != null) {
				dataListBean.setActiveName(obj[15].toString());
			}
			// 流程编号(ID)
			if (obj[16] != null) {
				dataListBean.setFlowId(Long.valueOf(obj[16].toString()));
			}
			// 流程名称
			if (obj[17] != null) {
				dataListBean.setFlowName(obj[17].toString());
			}
			// 制度编号、制度名称
			if (obj[18] != null && obj[19] != null) {
				dataListBean.setRuld(Long.valueOf(obj[18].toString()));
				dataListBean.setRuleName(obj[19].toString());
			}
			// 流程责任部门ID 和名称
			if (obj[20] != null && obj[21] != null) {
				// 责任部门ID
				dataListBean.setResOrgId(Long.valueOf(obj[20].toString()));
				// 责任部门名称
				dataListBean.setResOrgName(obj[21].toString());
			}
			// 制度责任部门ID 和名称
			if (obj[22] != null && obj[23] != null) {
				// 责任部门ID
				dataListBean.setRuleOrgId(Long.valueOf(obj[22].toString()));
				// 责任部门名称
				dataListBean.setRuleOrgName(obj[23].toString());
			}
			// 控制方法
			if (obj[24] != null) {
				dataListBean.setControlMethod(Integer.valueOf(obj[24].toString()));
			}
			// 控制类型
			if (obj[25] != null) {
				dataListBean.setControlType(Integer.valueOf(obj[25].toString()));
			}
			// 是否为关键控制点
			if (obj[26] != null) {
				dataListBean.setKeyControl(Integer.valueOf(obj[26].toString()));
			}
			// 控制频率
			if (obj[27] != null) {
				dataListBean.setControlFrequency(Integer.valueOf(obj[27].toString()));
			}
			// 父节点ID
			if (obj[28] != null) {
				dataListBean.setParentId(Long.valueOf(obj[28].toString()));
			}
			// 角色名称
			if (obj[29] != null) {
				dataListBean.setRoleName(obj[29].toString());
			}
			listDataListBeans.add(dataListBean);
		}
		// 级别总数 ： 循环处理默认0开始 size-1结束
		rListBean.setLevelTotal(maxCount + 1);
		List<RiskDataListBean> ResultBeans = new ArrayList<RiskDataListBean>();
		riskListSort(id, listDataListBeans, ResultBeans);
		rListBean.setDataListBeans(ResultBeans);
		return rListBean;
	}

	/**
	 * 流程清单数据排序
	 * 
	 * @author fuzhh 2013-12-12
	 * @param processSystemListBean
	 * @param sortProcessList
	 */
	private void riskListSort(Long riskId, List<RiskDataListBean> riskList, List<RiskDataListBean> riskSortList) {
		for (RiskDataListBean riskListBean : riskList) {
			if (riskId.equals(riskListBean.getRiskId())) {
				riskSortList.add(riskListBean);
				this.riskListChild(riskId, riskList, riskSortList);
			}
		}
	}

	/**
	 * 查找风险点子集
	 * 
	 * @param riskId
	 * @param riskList
	 * @param riskSortList
	 */
	private void riskListChild(Long riskId, List<RiskDataListBean> riskList, List<RiskDataListBean> riskSortList) {
		for (RiskDataListBean riskListBean : riskList) {
			if (riskId.equals(riskListBean.getParentId())) {
				riskSortList.add(riskListBean);
				if (riskListBean.getIsDir() == 0) {
					this.riskListChild(riskListBean.getRiskId(), riskList, riskSortList);
				}
			}
		}
	}

	/**
	 * 风险清单 节点处在的级别
	 * 
	 * @author fuzhh 2013-12-13
	 * @param id
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public int findRiskListLevel(Long id) throws Exception {
		return jecnRiskDao.findRiskListLevel(id);
	}

	/**
	 * 风险相关制度
	 */
	@Override
	public List<RiskRelatedRuleBean> getRiskRelatedRuleById(Long id) throws Exception {
		List<RiskRelatedRuleBean> list = new ArrayList<RiskRelatedRuleBean>();
		RiskRelatedRuleBean riskRelatedRuleBean = null;
		String sql = "select jr.id, jru.rule_name, jru.rule_number, jft.type_name, jfo.org_name, jrr.rule_id, jfo.org_id"
				+ "  from jecn_risk jr"
				+ "  left join jecn_rule_risk_t jrr on jr.id = jrr.risk_id"
				+ "  left join jecn_rule jru on jrr.rule_id = jru.id"
				+ "  left join jecn_flow_type jft on jft.type_id = jru.type_id"
				+ "  left join jecn_flow_org jfo on jru.org_id = jfo.org_id" + " where jrr.risk_id = " + id;
		List<Object[]> listObject = this.jecnRiskDao.listNativeSql(sql);
		for (Object[] objects : listObject) {
			riskRelatedRuleBean = new RiskRelatedRuleBean();
			if (objects[0] != null) {
				riskRelatedRuleBean.setId(Long.valueOf(objects[0].toString()));
			}
			if (objects[1] != null) {
				riskRelatedRuleBean.setRuleName(objects[1].toString());
			}
			if (objects[2] != null) {
				riskRelatedRuleBean.setRuleNumber(objects[2].toString());
			}
			if (objects[3] != null) {
				riskRelatedRuleBean.setRuleType(objects[3].toString());
			}
			if (objects[4] != null) {
				riskRelatedRuleBean.setOrgName(objects[4].toString());
			}
			if (objects[5] != null) {
				riskRelatedRuleBean.setRuleId(Long.valueOf(objects[5].toString()));
			}
			if (objects[6] != null) {
				riskRelatedRuleBean.setOrgId(Long.valueOf(objects[6].toString()));
			}
			list.add(riskRelatedRuleBean);
		}
		return list;
	}

	/**
	 * 风险相关流程
	 */
	@Override
	public List<RiskRelatedProcessBean> getRiskRelatedProcessBeanById(Long id) throws Exception {
		List<RiskRelatedProcessBean> list = new ArrayList<RiskRelatedProcessBean>();
		RiskRelatedProcessBean riskRelatedProcessBean = null;
		String sql = "select jc.description," + "       jcp.control_code," + "       jfsi.activity_show,"
				+ "       jfsi.figure_text," + "       jfs.flow_name," + "       jfo.org_name," + "       jcp.method,"
				+ "       jcp.type," + "       jcp.key_point,"
				+ "       jcp.frequency, jfo.org_id, jfsi.figure_id, jfs.flow_id" + "  from jecn_controltarget jc"
				+ "  left join jecn_control_point jcp on jc.id = jcp.target_id"
				+ "  left join jecn_flow_structure_image jfsi on jcp.active_id ="
				+ "                                              jfsi.figure_id"
				+ "  left join jecn_flow_structure jfs on jfsi.flow_id = jfs.flow_id"
				+ "  left join jecn_flow_related_org jfro on jfsi.flow_id = jfro.flow_id"
				+ "  left join jecn_flow_org jfo on jfro.org_id = jfo.org_id" + " where jc.risk_id = " + id;
		List<Object[]> listObject = this.jecnRiskDao.listNativeSql(sql);
		for (Object[] objects : listObject) {
			riskRelatedProcessBean = new RiskRelatedProcessBean();
			if (objects[0] != null) {
				riskRelatedProcessBean.setTargetDescription(objects[0].toString());
			}
			if (objects[1] != null) {
				riskRelatedProcessBean.setControlCode(objects[1].toString());
			}
			if (objects[2] != null) {
				Clob clob = (Clob) objects[2];
				riskRelatedProcessBean.setActiveShow(JecnCommon.ClobToString(clob));
			}
			if (objects[3] != null) {
				riskRelatedProcessBean.setFigureText(objects[3].toString());
			}
			if (objects[4] != null) {
				riskRelatedProcessBean.setFlowName(objects[4].toString());
			}
			if (objects[5] != null) {
				riskRelatedProcessBean.setOrgName(objects[5].toString());
			}
			if (objects[6] != null) {
				if ("0".equals(objects[6].toString())) {
					riskRelatedProcessBean.setStrMethod(JecnUtil.getValue("artificial"));
				} else if ("1".equals(objects[6].toString())) {
					riskRelatedProcessBean.setStrMethod(JecnUtil.getValue("autoSys"));
				} else {
					riskRelatedProcessBean.setStrMethod(" ");
				}
			}
			if (objects[7] != null) {
				if ("0".equals(objects[7].toString())) {
					riskRelatedProcessBean.setStrType(JecnUtil.getValue("preventive"));
				} else if ("1".equals(objects[7].toString())) {
					riskRelatedProcessBean.setStrType(JecnUtil.getValue("discover"));
				} else {
					riskRelatedProcessBean.setStrType(" ");
				}
			}
			if (objects[8] != null) {
				if ("0".equals(objects[8].toString())) {
					riskRelatedProcessBean.setKeyPoin(JecnUtil.getValue("is"));
				} else if ("1".equals(objects[8].toString())) {
					riskRelatedProcessBean.setKeyPoin(JecnUtil.getValue("no"));
				} else {
					riskRelatedProcessBean.setKeyPoin(" ");
				}
			}
			if (objects[9] != null) {
				if ("0".equals(objects[9].toString())) {
					riskRelatedProcessBean.setStrFrequency(JecnUtil.getValue("anyTime"));
				} else if ("1".equals(objects[9].toString())) {
					riskRelatedProcessBean.setStrFrequency(JecnUtil.getValue("day"));
				} else if ("2".equals(objects[9].toString())) {
					riskRelatedProcessBean.setStrFrequency(JecnUtil.getValue("weeks"));
				} else if ("3".equals(objects[9].toString())) {
					riskRelatedProcessBean.setStrFrequency(JecnUtil.getValue("month"));
				} else if ("4".equals(objects[9].toString())) {
					riskRelatedProcessBean.setStrFrequency(JecnUtil.getValue("seasons"));
				} else if ("5".equals(objects[9].toString())) {
					riskRelatedProcessBean.setStrFrequency(JecnUtil.getValue("year"));
				}
			}
			if (objects[10] != null) {
				riskRelatedProcessBean.setOrgId(Long.valueOf(objects[10].toString()));
			}
			if (objects[11] != null) {
				riskRelatedProcessBean.setActivityId(Long.valueOf(objects[11].toString()));
			}
			if (objects[12] != null) {
				riskRelatedProcessBean.setFlowId(Long.valueOf(objects[12].toString()));
			}
			list.add(riskRelatedProcessBean);
		}
		return list;
	}

	@Override
	public List<JecnTreeBean> getChooseDialogChildsRisk(Long pid, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.id," + "                t.parent_id," + "                t.risk_code,"
					+ "                t.is_dir," + "                t.grade," + "                t.sort,"
					+ "                t.standardcontrol," + "                t.create_person,"
					+ "                t.create_time," + "                t.update_person,"
					+ "                t.update_time," + "                t.note," + "                t.name,"
					+ "                (select count(*) from JECN_RISK where parent_id = t.id) as count"
					+ "  from JECN_RISK t, JECN_CONTROLTARGET jc" + " where 1 = 1"
					+ "   and (t.is_dir = 0 and t.PROJECT_ID = " + projectId + " and t.parent_id = " + pid + ")"
					+ "    or (t.is_dir = 1 and t.id = jc.risk_id and t.PROJECT_ID = " + projectId + " and"
					+ "       t.parent_id = " + pid + ")" + " order by t.parent_id, t.sort, t.id";
			List<Object[]> list = jecnRiskDao.listNativeSql(sql, projectId);
			return getJecnTreeBeanByObject(list);
		} catch (Exception e) {
			log.error("获取子节点数据出错", e);
			throw e;
		}
	}

}
