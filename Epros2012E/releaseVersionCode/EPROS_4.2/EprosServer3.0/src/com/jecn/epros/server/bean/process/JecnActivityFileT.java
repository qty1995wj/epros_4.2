package com.jecn.epros.server.bean.process;


public class JecnActivityFileT implements java.io.Serializable{
	private Long fileId;//主键ID
	private String figureUUID;// 索引
	private String fileName;// 文件名称（不存放在数据库）
	private Long fileType;//文件类型（0是输入，1是操作规范）
	private Long figureId;//活动FigureId
	private Long fileSId;//文件名称
	private String fileNumber; // 文件编号
	private String UUID;
	
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public JecnActivityFileT(){}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getFileType() {
		return fileType;
	}
	public void setFileType(Long fileType) {
		this.fileType = fileType;
	}
	public Long getFigureId() {
		return figureId;
	}
	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}
	public Long getFileSId() {
		return fileSId;
	}
	public void setFileSId(Long fileSId) {
		this.fileSId = fileSId;
	}
	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String UUID) {
		this.UUID = UUID;
	}
}
