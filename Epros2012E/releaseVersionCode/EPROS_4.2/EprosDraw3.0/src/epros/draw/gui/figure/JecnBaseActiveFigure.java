package epros.draw.gui.figure;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ToolTipManager;

import org.apache.commons.lang.StringUtils;

import sun.swing.SwingUtilities2;
import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.designer.JecnActiveOnLineTBean;
import epros.draw.designer.JecnActiveStandardBeanT;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnFigureInoutSampleT;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 活动、信息系统、决策、IT决策的父类
 * 
 * @author ZHANGXH
 * 
 */
public class JecnBaseActiveFigure extends JecnBaseFigurePanel {

	/** 信息化面板 */
	private InformatizationFigure informatizationFigure;

	/** 活动编号面板 */
	private ActiveNumFigure activeNumPanel;

	public ActiveNumFigure getActiveNumPanel() {
		return activeNumPanel;
	}

	public void paintComponent(Graphics g) {
		paintFigure(g);
		Graphics2D g2d = (Graphics2D) g;
		int textHeight = g2d.getFontMetrics(this.getCurScaleFont()).getHeight();
		// 活动编号
		addActivityNum(g2d, textHeight / 2 + 2);
	}

	public InformatizationFigure getInformatizationFigure() {
		return informatizationFigure;
	}

	/**
	 * 
	 * 构造函数
	 * 
	 * @param activeData
	 *            JecnFigureData JecnActiveData对象
	 */
	public JecnBaseActiveFigure(JecnFigureData activeData) {
		super(activeData);

		if (!(activeData instanceof JecnActiveData)) {// 不是JecnActiveData对象
			JecnLogConstants.LOG_JecnBaseActiveFigure.error("JecnBaseActiveFigure类构造函数：参数不合法");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FIAL);
		}

		initMouseEvent();
	}

	/**
	 * 
	 * 提示信息事件
	 * 
	 */
	public void initMouseEvent() {

		this.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent paramMouseEvent) {
				// 初始化
				setToolTipText(null);
				boolean avtiveNote = DrawCommon.isNullOrEmtryTrim(getAvtivityShow());
				boolean keyAvtiveNote = DrawCommon.isNullOrEmtryTrim(getAvtivityShowAndControl());
				boolean activityInputNote = DrawCommon.isNullOrEmtryTrim(getActivityInput());
				boolean activityOutputNote = DrawCommon.isNullOrEmtryTrim(getActivityOutput());

				JecnActiveData activeData = JecnBaseActiveFigure.this.getElementData();

				if (avtiveNote
						&& keyAvtiveNote
						&& activityInputNote
						&& activityOutputNote
						&& activeData.getListJecnActivityFileT().isEmpty()
						&& activeData.getListModeFileT().isEmpty()
						&& (activeData.isInNameNull() || (activeData.getListFigureInTs() == null || activeData
								.getListFigureInTs().isEmpty()))
						&& (activeData.isOutNameNull() || (activeData.getListFigureOutTs() == null || activeData
								.getListFigureOutTs().isEmpty()))
						&& (activeData.getListJecnActiveStandardBeanT() == null || activeData
								.getListJecnActiveStandardBeanT().isEmpty())) {// 如果存在活动说明或关键说明都为空
					return;
				}

				// 指定取消工具提示的延迟值 设置为一小时 3600秒
				ToolTipManager.sharedInstance().setDismissDelay(3600 * 1000);
				// 组织HTML格式字符串
				StringBuffer sbuf = new StringBuffer();
				// 是否启用新版本输入输出悬浮
				if (JecnDesignerProcess.getDesignerProcess().useNewInout()) {
					newInformationCue(sbuf, avtiveNote, keyAvtiveNote, activityInputNote, activityOutputNote,
							activeData);
				} else {
					oldInformationCue(sbuf, avtiveNote, keyAvtiveNote, activityInputNote, activityOutputNote,
							activeData);
				}
				// 注册要在工具提示中显示的文本。光标处于该组件上时显示该文本。如果 参数 为 null，则关闭此组件的工具提示
				setToolTipText(sbuf.toString());
			}

			public void mouseExited(MouseEvent paramMouseEvent) {
				// 返回初始延迟值
				int intD = ToolTipManager.sharedInstance().getInitialDelay();
				// 取消工具提示的延迟值
				ToolTipManager.sharedInstance().setDismissDelay(intD);
			}
		});
	}

	private void oldInformationCue(StringBuffer sbuf, boolean avtiveNote, boolean keyAvtiveNote,
			boolean activityInputNote, boolean activityOutputNote, JecnActiveData activeData) {

		sbuf.append("<html><div style= 'white-space:normal; display:block;min-width:200px; word-break:break-all'>");

		if (!activityInputNote || activeData.getListJecnActivityFileT().size() > 0) {// 输入说明
			sbuf.append("<br><b>");
			sbuf.append(JecnResourceUtil.getJecnResourceUtil().getValue("inLabC"));// 输入
			sbuf.append("</b><br>");
			// 说明
			sbuf.append(DrawCommon.getsubstring(getActivityInput()));

			if (activeData.getListJecnActivityFileT().size() > 0) {
				sbuf.append("<br>");
				for (JecnActivityFileT activityFile : activeData.getListJecnActivityFileT()) {
					if (activityFile.getFileType() == 0) {
						sbuf.append(activityFile.getFileName()).append(
								"  [ " + JecnResourceUtil.getJecnResourceUtil().getValue("mould") + "]").append("<br>");
					}
				}
			}
		}

		if (!activityOutputNote || activeData.getListModeFileT().size() > 0) {// 输出说明
			sbuf.append("<br><b>");
			sbuf.append(JecnResourceUtil.getJecnResourceUtil().getValue("outLabC"));
			sbuf.append("</b><br>");
			// 说明
			sbuf.append(DrawCommon.getsubstring(getActivityOutput()));
			if (activeData.getListModeFileT().size() > 0) {
				sbuf.append("<br>");
				for (JecnModeFileT modeFileT : activeData.getListModeFileT()) {
					sbuf.append(modeFileT.getModeName()).append(
							"  [" + JecnResourceUtil.getJecnResourceUtil().getValue("mould") + "]").append("<br>");
				}
			}
		}

		if (!avtiveNote) {// 活动说明
			sbuf.append("<br><b>");
			sbuf.append(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("activeNote"));
			sbuf.append("</b><br>");
			// 说明
			sbuf.append(DrawCommon.getsubstring(getAvtivityShow()));
			sbuf.append("<br><br>");
		}

		if (!keyAvtiveNote && getKeyActiveMapElemType() != null) {// 关键说明
			sbuf.append("<br><b>");
			sbuf.append(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("activeKeyNote"));
			sbuf.append("</b><br>");
			sbuf.append(DrawCommon.getsubstring(getAvtivityShowAndControl()));
			sbuf.append("<br><br>");
		}

		sbuf.append("</div><br></html>");

	}

	private void newInformationCue(StringBuffer sbuf, boolean avtiveNote, boolean keyAvtiveNote,
			boolean activityInputNote, boolean activityOutputNote, JecnActiveData activeData) {
		sbuf.append("<html><div style= 'white-space:normal; display:block;min-width:200px; word-break:break-all'>");
		String kong = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp;";
		if (activeData.getListFigureInTs() != null && activeData.getListFigureInTs().size() > 0) {// 输入说明
			if (!activeData.isInNameNull()) {
				sbuf.append("<br><b>");
				sbuf.append(JecnResourceUtil.getJecnResourceUtil().getValue("inLabC"));// 输入
				sbuf.append("</b>");
				if (activeData.getListFigureInTs().size() > 0) {
					for (JecnFigureInoutT activityFile : activeData.getListFigureInTs()) {
						if (StringUtils.isBlank(activityFile.getName()) && activityFile.getFileId() == null) {
							continue;
						}
						if (activityFile.getType() == 0) {
							sbuf.append("<br>" + kong);
							sbuf.append(activityFile.getName());
							if (activityFile.getListSampleT() != null) {
								for (JecnFigureInoutSampleT sample : activityFile.getListSampleT()) {
									if (sample.getType() == 0) {
										sbuf.append("<br>" + kong + kong);
										sbuf.append("  [" + JecnResourceUtil.getJecnResourceUtil().getValue("mould")
												+ "]  " + sample.getFileName());
									}
								}
							}
						}
					}
				}
			}
		}

		if (activeData.getListFigureOutTs() != null && activeData.getListFigureOutTs().size() > 0) {// 输出说明
			if (!activeData.isOutNameNull()) {
				sbuf.append("<br><b>");
				sbuf.append(JecnResourceUtil.getJecnResourceUtil().getValue("outLabC"));
				sbuf.append("</b>");
				if (activeData.getListFigureOutTs().size() > 0) {
					for (JecnFigureInoutT modeFileT : activeData.getListFigureOutTs()) {
						if (StringUtils.isBlank(modeFileT.getName())) {
							continue;
						}
						sbuf.append("<br>" + kong);
						sbuf.append(modeFileT.getName());
						if (modeFileT.getListSampleT() != null) {
							for (JecnFigureInoutSampleT sample : modeFileT.getListSampleT()) {
								if (sample.getType() == 0) {
									sbuf.append("<br>" + kong + kong);
									sbuf.append("  [" + JecnResourceUtil.getJecnResourceUtil().getValue("mould")
											+ "]  " + sample.getFileName());
								}
							}
							for (JecnFigureInoutSampleT sample : modeFileT.getListSampleT()) {
								if (sample.getType() == 1) {
									sbuf.append("<br>" + kong + kong);
									sbuf.append("  [" + JecnResourceUtil.getJecnResourceUtil().getValue("sample")
											+ "]  " + sample.getFileName());
								}
							}
						}
					}
				}
			}
		}
		StringBuffer str = new StringBuffer();
		if (activeData.getListJecnActivityFileT() != null && activeData.getListJecnActivityFileT().size() > 0) {// 操作规范
			boolean isShow = false;
			for (JecnActivityFileT actFile : activeData.getListJecnActivityFileT()) {
				if (actFile.getFileType() == 1) {
					isShow = true;
				} else {
					continue;
				}
			}
			if (isShow) {
				sbuf.append("<br><b>");
				sbuf.append(JecnResourceUtil.getJecnResourceUtil().getValue("actOperation"));
				sbuf.append("</b>");
				if (activeData.getListJecnActivityFileT().size() > 0) {
					sbuf.append("<br>" + kong);
					str = new StringBuffer();
					for (JecnActivityFileT actFile : activeData.getListJecnActivityFileT()) {
						if (actFile.getFileType() == 1) {
							if (StringUtils.isNotBlank(str.toString())) {
								str.append("<br>" + kong);
							}
							str.append(actFile.getFileName());
						}
					}
					sbuf.append(str);
				}
			}
		}

		if (activeData.getListJecnActiveStandardBeanT() != null
				&& activeData.getListJecnActiveStandardBeanT().size() > 0) {// 关联标准
			sbuf.append("<br><b>");
			sbuf.append(JecnResourceUtil.getJecnResourceUtil().getValue("refStand"));
			sbuf.append("</b>");
			if (activeData.getListJecnActiveStandardBeanT().size() > 0) {
				sbuf.append("<br>" + kong);
				str = new StringBuffer();
				for (JecnActiveStandardBeanT standardBeanT : activeData.getListJecnActiveStandardBeanT()) {
					if (StringUtils.isNotBlank(str.toString())) {
						str.append("<br>" + kong);
					}
					str.append(standardBeanT.getRelaName());
				}
				sbuf.append(str);
			}
		}

		if (!avtiveNote) {// 活动说明
			sbuf.append("<br><b>");
			sbuf.append(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("activeOperateNote"));
			sbuf.append("</b><br>" + kong);
			// 说明
			StringBuffer termSubstring = DrawCommon.getTermSubstring(getAvtivityShow(), kong);
			sbuf.append(termSubstring.delete(termSubstring.length() - 35, termSubstring.length()));
		}

		if (!keyAvtiveNote && getKeyActiveMapElemType() != null) {// 关键说明
			sbuf.append("<br><b>");
			sbuf.append(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("activeKeyNote"));
			sbuf.append("</b><br>" + kong);
			sbuf.append(DrawCommon.getTermSubstring(getAvtivityShowAndControl(), kong));
		}
		sbuf.append("</div><br></html>");

	}

	private String getName(String name) {
		if (StringUtils.isBlank(name)) {
			return "";
		}
		return name;
	}

	/**
	 * 
	 * 通过区域包含获取关键活动类型
	 * 
	 * @return MapElemType 有关键活动返回投关键活动类型，没有关键活动返回NULL
	 * 
	 */
	public MapElemType getKeyActiveMapElemType() {
		// 当前面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return null;
		}

		// 活动区域
		Rectangle activeFigureRectangle = this.getBounds();
		// 关键活动类型
		MapElemType activeKeyMapElemType = this.getElementData().getActiveKeyMapElemType();

		// 包含给定活动的所有关键活动
		List<JecnBaseKeyActiveFigure> keyActiveList = new ArrayList<JecnBaseKeyActiveFigure>();

		for (JecnBaseFlowElementPanel elemPanel : workflow.getPanelList()) {
			if (elemPanel instanceof JecnBaseKeyActiveFigure) {// 关键活动
				// 关键活动
				JecnBaseKeyActiveFigure keyActiveFigure = (JecnBaseKeyActiveFigure) elemPanel;
				// 关键活动区域
				Rectangle keyActiveRectangle = keyActiveFigure.getBounds();

				// 关键活动是否包含此活动
				boolean isContains = keyActiveRectangle.contains(activeFigureRectangle);

				if (isContains) {// 包含
					keyActiveList.add(keyActiveFigure);

					if (activeKeyMapElemType != null
							&& activeKeyMapElemType == keyActiveFigure.getFlowElementData().getMapElemType()) {// 如果类型相同
						return activeKeyMapElemType;
					}
				}
			}
		}

		if (keyActiveList.size() > 0) {
			return keyActiveList.get(0).getFlowElementData().getMapElemType();
		}

		return null;
	}

	/**
	 * 
	 * 关键活动类型：1为PA,2为KSF,3为KCP
	 * 
	 * @return 1为PA,2为KSF,3为KCP
	 */
	public String getActiveKeyType() {
		MapElemType mapType = this.getKeyActiveMapElemType();
		if (mapType == null) {
			return "";
		}
		switch (mapType) {
		case PAFigure:
			return "1";
		case KSFFigure:
			return "2";
		case KCPFigure:
			return "3";
		case KCPFigureComp:
			return "4";
		}
		return "";
	}

	public JecnActiveData getFlowElementData() {
		JecnActiveData activeData = (JecnActiveData) flowElementData;
		String activeKeyType = getActiveKeyType();
		activeData.setActiveKeyType(activeKeyType);
		return activeData;
	}

	public JecnActiveData getElementData() {
		JecnActiveData activeData = (JecnActiveData) flowElementData;
		return activeData;
	}

	/**
	 * 设置字的位置
	 * 
	 * @param g
	 * @param type
	 */
	protected void setFontPosition(Graphics2D g, int type) {
		g.setFont(this.getCurScaleFont());
		// 绘制文字
		String text = this.getFlowElementData().getFigureText();
		if (DrawCommon.isNullOrEmtryTrim(text) || g == null) {
			return;
		}
		int textLength = g.getFontMetrics(this.getCurScaleFont()).stringWidth(text);
		int textHeight = g.getFontMetrics(this.getCurScaleFont()).getHeight();
		// g.setColor(this.getFlowElementData().getFontColor());

		List<String> list = changeFontType(g, text, this.getCurScaleFont(), this.getWidth());

		for (int i = 0; i < list.size(); i++) {
			textLength = g.getFontMetrics(this.getCurScaleFont()).stringWidth(list.get(i));
			SwingUtilities2.drawString(this, g, list.get(i), this.getWidth() / 2 - textLength / 2,
					(this.getHeight() + 20) / 2 - (list.size() / 4) * textHeight + textHeight * i);
		}
	}

	/**
	 * 初始化线上模板
	 */
	protected void initInfoFigure() {
		if (this.getFlowElementData().getIsOnLine() == 1
				&& this.getFlowElementData().getListJecnActiveOnLineTBean().size() > 0) {
			String lineName = getInformatizationText();
			if (StringUtils.isEmpty(lineName)) {
				return;
			}
			if (informatizationFigure == null) {
				informatizationFigure = new InformatizationFigure();
				JecnDrawMainPanel.getMainPanel().getWorkflow().add(informatizationFigure);
			}
			informatizationFigure.initSize(lineName);
			informatizationFigure.setLocation(new Point(this.getLocation().x, this.getLocation().y
					- informatizationFigure.getHeight()));
		} else {
			if (informatizationFigure != null) {
				JecnDrawMainPanel.getMainPanel().getWorkflow().remove(informatizationFigure);
				informatizationFigure = null;
			}
		}
	}

	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		// 信息化
		initInfoFigure();
		initInfoActiveNumFigure();
	}

	/**
	 * 初始化编号面板
	 */
	protected void initInfoActiveNumFigure() {
		if (!JecnDesignerProcess.getDesignerProcess().isUserActiveByItem()) {
			return;
		}
		if (StringUtils.isNotBlank(this.getFlowElementData().getActivityNum())) {
			if (activeNumPanel == null) {
				activeNumPanel = new ActiveNumFigure();
				JecnDrawMainPanel.getMainPanel().getWorkflow().add(activeNumPanel);
			}
			activeNumPanel.initSize(this.getFlowElementData().getActivityNum());
			activeNumPanel.setLocation(new Point(this.getLocation().x + this.getWidth() - activeNumPanel.getWidth(),
					this.getLocation().y - activeNumPanel.getHeight()));
		} else {
			if (activeNumPanel != null) {
				JecnDrawMainPanel.getMainPanel().getWorkflow().remove(activeNumPanel);
				activeNumPanel = null;
			}
		}
	}

	private String getInformatizationText() {
		String lineName = "";
		for (JecnActiveOnLineTBean activeOnLineTBean : this.getFlowElementData().getListJecnActiveOnLineTBean()) {
			if (DrawCommon.isNullOrEmtryTrim(lineName)) {
				lineName = activeOnLineTBean.getSysName();
			} else {
				lineName = lineName + "/" + activeOnLineTBean.getSysName();
			}
		}
		return lineName;
	}

	/**
	 * 双重元素对象附属图形
	 */
	public void removeRelatedPanel() {
		super.removeRelatedPanel();
		if (activeNumPanel != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(activeNumPanel);
			activeNumPanel = null;
		}
		if (informatizationFigure != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(informatizationFigure);
			informatizationFigure = null;
		}
	}

	/**
	 * 活动创建克隆对象
	 * 
	 */
	public JecnBaseActiveFigure clone() {
		return (JecnBaseActiveFigure) this.currClone();
	}

	public String getAvtivityShow() {
		return this.getFlowElementData().getAvtivityShow();
	}

	public String getActivityInput() {
		return this.getFlowElementData().getActivityInput();
	}

	public String getActivityOutput() {
		return this.getFlowElementData().getActivityOutput();
	}

	public String getAvtivityShowAndControl() {
		return this.getFlowElementData().getAvtivityShowAndControl();
	}

	/**
	 * 添加编号
	 * 
	 * @param g2d
	 * @param textHeight
	 */
	protected void addActivityNum(Graphics2D g2d, int textHeight) {
		if (getFlowElementData().getActivityNum() != null) {
			int activityIdLength = g2d.getFontMetrics(this.getCurScaleFont()).stringWidth(
					getFlowElementData().getActivityNum().toString());
			if (flowElementData.getCurrentAngle() == 0.0) {
				g2d.drawString(getFlowElementData().getActivityNum().toString(), this.getWidth() / 2 - activityIdLength
						/ 2, textHeight);
			}
			if (flowElementData.getCurrentAngle() == 90.0) {
				g2d.drawString(getFlowElementData().getActivityNum().toString(),
						this.getWidth() - activityIdLength - 4, this.getHeight() / 2 + textHeight / 2 - 1);
			}
			if (flowElementData.getCurrentAngle() == 180.0) {
				g2d.drawString(getFlowElementData().getActivityNum().toString(), this.getWidth() / 2 + activityIdLength
						/ 2 - 3, this.getHeight() - textHeight / 2);
			}
			if (flowElementData.getCurrentAngle() == 270.0) {
				g2d.drawString(getFlowElementData().getActivityNum().toString(), activityIdLength / 2 + 3, this
						.getHeight() / 2 + 2);
			}
		}
	}
}
