package com.jecn.epros.domain.org;

import java.util.List;

public class PosViewBean {
    private int total;
    private List<SearchPosResultBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<SearchPosResultBean> getData() {
        return data;
    }

    public void setData(List<SearchPosResultBean> data) {
        this.data = data;
    }
}
