<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<% 
 response.setHeader("Pragma","No-cache");
 response.setHeader("Cache-Control","no-cache");
 response.setDateHeader("Expires", 0); 
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body>
		<div id="dbImport_id" align="left" style="padding-left: 20px;padding-top:10px;">
			<s:form name="dbform" action="" method="post">
				<table>
					<tr style="height: 30px;">
						<td colspan="2">
							<!-- 同步方式 -->
							<s:text name="sysMode" /><br/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<!-- 手动 -->
							<s:text name="handSys" />
							<input id="radio0" type="radio" name="abstractConfigBean.iaAuto"
								value="0" />
							<!-- 自动 -->
							<s:text name="autoSys" />
							<input id="radio1" type="radio" name="abstractConfigBean.iaAuto"
								value="1" />
							<input id="iaRadio" type="hidden"
								value="${abstractConfigBean.iaAuto }" />
						</td>
					</tr>
					<tr height="20"><td colspan="2"> </td></tr>
					<tr style="height: 30px;">
						<td colspan="2">
							<!-- 时间设置 -->
							<s:text name="timeSet" />
						</td>
					</tr>
					<tr>
						<td>
							<!-- 同步开始时间 -->
							<s:text name="sysStartTime"/>：
						</td>
						<td>
							<input id="startTime" name="abstractConfigBean.startTime"
								value="${abstractConfigBean.startTime }"  style="width:100px"/> (格式：13:01)
						</td>
					</tr>
					<tr>
						<td>
							<!-- 同步时间间隔(天)： -->
							<s:text name="sysInterval" />
						</td>
						<td>
							<input id="day" name="abstractConfigBean.intervalDay"
								value="${abstractConfigBean.intervalDay }" style="width:100px" />
						</td>
					</tr>
				</table>
				<br>
				<!-- 开始同步 -->
				<span id="loadingImage1" onclick="doImport('loadingImage1','submitLoading1');" style="cursor: pointer;" >
					<img src="${basePath}images/dataImport/startImport.gif" hspace="5" border="0" />
					&nbsp;&nbsp;
				</span>
				<span style="display: none;" id="submitLoading1"></span>
				<!--  保存设置 -->
				<span id="loadingImage2" onclick="checkAndSaveForm();" style="cursor: pointer;" > 
					<img  src="${basePath}images/dataImport/saveSet.gif" hspace="5" border="0"  />
				</span>	
				<!-- 导出错误数据 -->
				<a id="loadingImage3" href=<s:if test='importValue==3||importValue==6'>"errorFileDownMatchAction.action"</s:if>
					<s:else>  "userErrorDataFileDownAction.action"  </s:else>>
					<img src="${basePath}images/dataImport/expErrorData.gif" hspace="5" border="0" /></a>
			    <!-- 变更数据 标准版人员同步-->
			    <s:if test='importValue!=3&&importValue!=6'><a id="loadingImage4" href="personnelChangesFileDownAction.action"><img src="${basePath}images/dataImport/changeData.gif" hspace="5" border="0" /></a></s:if>
          		<input type="hidden" id="dbImportType" name="importType" value="<s:property value='importType'/>" />
          		<!-- 正在导入tip提示信息span -->	
				<span id="dbImportTip" style="display:none"><s:text name="sys_Loading" /></span>
			</s:form>
		</div>
	</body>
</html>
