package com.jecn.epros.domain.rule;

import com.jecn.epros.domain.process.processFile.BaseProcessFileBean;

import java.util.List;

/**
 * 制度操作说明
 *
 * @author admin
 */
public class RuleOperationFile {
    /**
     * 制度id
     */
    private Long id;
    private String name;

    /**
     * 各阶段文件内容
     */
    private List<BaseProcessFileBean> fileBeans = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BaseProcessFileBean> getFileBeans() {
        return fileBeans;
    }

    public void setFileBeans(List<BaseProcessFileBean> fileBeans) {
        this.fileBeans = fileBeans;
    }
}
