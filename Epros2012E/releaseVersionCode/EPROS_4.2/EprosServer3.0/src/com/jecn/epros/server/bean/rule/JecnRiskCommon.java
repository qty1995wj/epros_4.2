package com.jecn.epros.server.bean.rule;

/***
 * 风险表单
 * 2013-11-22
 *
 */
public class JecnRiskCommon  implements java.io.Serializable {
	private Long riskId;//风险ID
	private String riskNumber;//风险编号
	private String riskName;//风险名称
	private Long ruleTitleId;//制度标题ID
	public Long getRiskId() {
		return riskId;
	}
	public void setRiskId(Long riskId) {
		this.riskId = riskId;
	}
	public String getRiskNumber() {
		return riskNumber;
	}
	public void setRiskNumber(String riskNumber) {
		this.riskNumber = riskNumber;
	}
	public String getRiskName() {
		return riskName;
	}
	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}
	public Long getRuleTitleId() {
		return ruleTitleId;
	}
	public void setRuleTitleId(Long ruleTitleId) {
		this.ruleTitleId = ruleTitleId;
	}
	
	
}
