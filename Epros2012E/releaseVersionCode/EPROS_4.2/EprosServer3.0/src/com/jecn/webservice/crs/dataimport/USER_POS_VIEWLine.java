/**
 * USER_POS_VIEWLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crs.dataimport;

public class USER_POS_VIEWLine implements java.io.Serializable {
	private java.lang.String login_name;

	private java.lang.String true_name;

	private java.math.BigDecimal pos_num;

	private java.lang.String pos_name;

	private java.math.BigDecimal org_num;

	private java.lang.String email;

	private java.lang.String email_type;

	private java.lang.String phone_str;

	private java.lang.String c_employeestatus;

	public USER_POS_VIEWLine() {
	}

	public USER_POS_VIEWLine(java.lang.String login_name, java.lang.String true_name, java.math.BigDecimal pos_num,
			java.lang.String pos_name, java.math.BigDecimal org_num, java.lang.String email,
			java.lang.String email_type, java.lang.String phone_str, java.lang.String c_employeestatus) {
		this.login_name = login_name;
		this.true_name = true_name;
		this.pos_num = pos_num;
		this.pos_name = pos_name;
		this.org_num = org_num;
		this.email = email;
		this.email_type = email_type;
		this.phone_str = phone_str;
		this.c_employeestatus = c_employeestatus;
	}

	/**
	 * Gets the login_name value for this USER_POS_VIEWLine.
	 * 
	 * @return login_name
	 */
	public java.lang.String getLogin_name() {
		return login_name;
	}

	/**
	 * Sets the login_name value for this USER_POS_VIEWLine.
	 * 
	 * @param login_name
	 */
	public void setLogin_name(java.lang.String login_name) {
		this.login_name = login_name;
	}

	/**
	 * Gets the true_name value for this USER_POS_VIEWLine.
	 * 
	 * @return true_name
	 */
	public java.lang.String getTrue_name() {
		return true_name;
	}

	/**
	 * Sets the true_name value for this USER_POS_VIEWLine.
	 * 
	 * @param true_name
	 */
	public void setTrue_name(java.lang.String true_name) {
		this.true_name = true_name;
	}

	/**
	 * Gets the pos_num value for this USER_POS_VIEWLine.
	 * 
	 * @return pos_num
	 */
	public java.math.BigDecimal getPos_num() {
		return pos_num;
	}

	/**
	 * Sets the pos_num value for this USER_POS_VIEWLine.
	 * 
	 * @param pos_num
	 */
	public void setPos_num(java.math.BigDecimal pos_num) {
		this.pos_num = pos_num;
	}

	/**
	 * Gets the pos_name value for this USER_POS_VIEWLine.
	 * 
	 * @return pos_name
	 */
	public java.lang.String getPos_name() {
		return pos_name;
	}

	/**
	 * Sets the pos_name value for this USER_POS_VIEWLine.
	 * 
	 * @param pos_name
	 */
	public void setPos_name(java.lang.String pos_name) {
		this.pos_name = pos_name;
	}

	/**
	 * Gets the org_num value for this USER_POS_VIEWLine.
	 * 
	 * @return org_num
	 */
	public java.math.BigDecimal getOrg_num() {
		return org_num;
	}

	/**
	 * Sets the org_num value for this USER_POS_VIEWLine.
	 * 
	 * @param org_num
	 */
	public void setOrg_num(java.math.BigDecimal org_num) {
		this.org_num = org_num;
	}

	/**
	 * Gets the email value for this USER_POS_VIEWLine.
	 * 
	 * @return email
	 */
	public java.lang.String getEmail() {
		return email;
	}

	/**
	 * Sets the email value for this USER_POS_VIEWLine.
	 * 
	 * @param email
	 */
	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	/**
	 * Gets the email_type value for this USER_POS_VIEWLine.
	 * 
	 * @return email_type
	 */
	public java.lang.String getEmail_type() {
		return email_type;
	}

	/**
	 * Sets the email_type value for this USER_POS_VIEWLine.
	 * 
	 * @param email_type
	 */
	public void setEmail_type(java.lang.String email_type) {
		this.email_type = email_type;
	}

	/**
	 * Gets the phone_str value for this USER_POS_VIEWLine.
	 * 
	 * @return phone_str
	 */
	public java.lang.String getPhone_str() {
		return phone_str;
	}

	/**
	 * Sets the phone_str value for this USER_POS_VIEWLine.
	 * 
	 * @param phone_str
	 */
	public void setPhone_str(java.lang.String phone_str) {
		this.phone_str = phone_str;
	}

	/**
	 * Gets the c_employeestatus value for this USER_POS_VIEWLine.
	 * 
	 * @return c_employeestatus
	 */
	public java.lang.String getC_employeestatus() {
		return c_employeestatus;
	}

	/**
	 * Sets the c_employeestatus value for this USER_POS_VIEWLine.
	 * 
	 * @param c_employeestatus
	 */
	public void setC_employeestatus(java.lang.String c_employeestatus) {
		this.c_employeestatus = c_employeestatus;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof USER_POS_VIEWLine))
			return false;
		USER_POS_VIEWLine other = (USER_POS_VIEWLine) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.login_name == null && other.getLogin_name() == null) || (this.login_name != null && this.login_name
						.equals(other.getLogin_name())))
				&& ((this.true_name == null && other.getTrue_name() == null) || (this.true_name != null && this.true_name
						.equals(other.getTrue_name())))
				&& ((this.pos_num == null && other.getPos_num() == null) || (this.pos_num != null && this.pos_num
						.equals(other.getPos_num())))
				&& ((this.pos_name == null && other.getPos_name() == null) || (this.pos_name != null && this.pos_name
						.equals(other.getPos_name())))
				&& ((this.org_num == null && other.getOrg_num() == null) || (this.org_num != null && this.org_num
						.equals(other.getOrg_num())))
				&& ((this.email == null && other.getEmail() == null) || (this.email != null && this.email.equals(other
						.getEmail())))
				&& ((this.email_type == null && other.getEmail_type() == null) || (this.email_type != null && this.email_type
						.equals(other.getEmail_type())))
				&& ((this.phone_str == null && other.getPhone_str() == null) || (this.phone_str != null && this.phone_str
						.equals(other.getPhone_str())))
				&& ((this.c_employeestatus == null && other.getC_employeestatus() == null) || (this.c_employeestatus != null && this.c_employeestatus
						.equals(other.getC_employeestatus())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getLogin_name() != null) {
			_hashCode += getLogin_name().hashCode();
		}
		if (getTrue_name() != null) {
			_hashCode += getTrue_name().hashCode();
		}
		if (getPos_num() != null) {
			_hashCode += getPos_num().hashCode();
		}
		if (getPos_name() != null) {
			_hashCode += getPos_name().hashCode();
		}
		if (getOrg_num() != null) {
			_hashCode += getOrg_num().hashCode();
		}
		if (getEmail() != null) {
			_hashCode += getEmail().hashCode();
		}
		if (getEmail_type() != null) {
			_hashCode += getEmail_type().hashCode();
		}
		if (getPhone_str() != null) {
			_hashCode += getPhone_str().hashCode();
		}
		if (getC_employeestatus() != null) {
			_hashCode += getC_employeestatus().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			USER_POS_VIEWLine.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://hr.publisher.mdm", "USER_POS_VIEWLine"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("login_name");
		elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "login_name"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("true_name");
		elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "true_name"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("pos_num");
		elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "pos_num"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("pos_name");
		elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "pos_name"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("org_num");
		elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "org_num"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("email");
		elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "email"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("email_type");
		elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "email_type"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("phone_str");
		elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "phone_str"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("c_employeestatus");
		elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_employeestatus"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
			java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
