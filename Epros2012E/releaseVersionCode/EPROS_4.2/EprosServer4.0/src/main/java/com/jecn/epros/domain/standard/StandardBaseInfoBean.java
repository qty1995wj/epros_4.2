package com.jecn.epros.domain.standard;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.HttpUtil;
import com.jecn.epros.util.JecnProperties;

import java.util.List;

public class StandardBaseInfoBean {
    private String name;

    /**
     * 此值 stanType=1 ，文件ID，其他不用
     */
    private Long fileId;
    /**
     * 密级  此值 stanType=1
     */
    private long secretLevel;

    /**
     * 是否形成程序文件  此值 stanType=4
     */
    private long isProfile;

    /**
     * 内容
     */
    private String stanContent;
    /**
     * 所属条款   此值 stanType=4
     */
    private String clauseName;

    /**
     * 1文件标准，4标准条款,5条款要
     */
    private int stanType;

    private String fileNum;

    /**
     * 备注
     */
    private String note;

    private List<StandardFileContent> fileContents;


    private String delState; //状态



    public String getDelState() {
        return delState;
    }

    public void setDelState(String delState) {
        this.delState = delState;
    }

    public List<StandardFileContent> getFileContents() {
        return fileContents;
    }

    public void setFileContents(List<StandardFileContent> fileContents) {
        this.fileContents = fileContents;
    }

    public String getHrefURL() {
        return HttpUtil.getDownLoadFileHttp(fileId, true, "");
    }

    public String getPublicStr() {
        return ConfigUtils.getSecretByType(secretLevel);
    }

    public String getProfile() {
        if (isProfile == 0) {
            return JecnProperties.getValue("no");//否
        } else {
            return JecnProperties.getValue("yes");//是
        }
    }

    public LinkResource getFileViewLink() {
        return new LinkResource(fileId, name, LinkResource.ResourceType.FILE_VIEW);
    }

    public String getFileNum() {
        return fileNum;
    }

    public void setFileNum(String fileNum) {
        this.fileNum = fileNum;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSecretLevel() {
        return secretLevel;
    }

    public void setSecretLevel(long secretLevel) {
        this.secretLevel = secretLevel;
    }

    public long getIsProfile() {
        return isProfile;
    }

    public void setIsProfile(long isProfile) {
        this.isProfile = isProfile;
    }

    public String getStanContent() {
        return Common.replaceWarp(stanContent);
    }

    public void setStanContent(String stanContent) {
        this.stanContent = stanContent;
    }

    public String getClauseName() {
        return clauseName;
    }

    public void setClauseName(String clauseName) {
        this.clauseName = clauseName;
    }

    public int getStanType() {
        return stanType;
    }

    public void setStanType(int stanType) {
        this.stanType = stanType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
