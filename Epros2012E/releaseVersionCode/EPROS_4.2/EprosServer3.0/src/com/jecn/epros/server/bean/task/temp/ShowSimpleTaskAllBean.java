package com.jecn.epros.server.bean.task.temp;

import java.io.Serializable;

/**
 * 
 * @author zhagnxiaohu
 * 
 */
public class ShowSimpleTaskAllBean implements Serializable {
	/** 任务Id */
	private Long taskId;
	/** 任务名称 */
	private String taskName;
	/** 开始时间 */
	private String startTime;
	/** 结束时间 */
	private String endTime;
	/** 创建人Id */
	private Long createPeopleId;
	/** 创建人 */
	private String createPeopleName;
	/** 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段 */
	private int state;
	/** 0是锁定,1是解锁 */
	private int isLock;
	/**
	 * 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
	 * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交
	 */
	private Integer taskElseState;
	/** 1是可以删除，0不删除 */
	private int isDelete;
	/** 0流程任务，1上传文件任务，2制度 */
	private int taskType;
	/** 0不能查看，1可以查看 */
	private int isCanSee;

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public String getCreatePeopleName() {
		return createPeopleName;
	}

	public void setCreatePeopleName(String createPeopleName) {
		this.createPeopleName = createPeopleName;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getIsLock() {
		return isLock;
	}

	public void setIsLock(int isLock) {
		this.isLock = isLock;
	}

	public Integer getTaskElseState() {
		return taskElseState;
	}

	public void setTaskElseState(Integer taskElseState) {
		this.taskElseState = taskElseState;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public int getIsCanSee() {
		return isCanSee;
	}

	public void setIsCanSee(int isCanSee) {
		this.isCanSee = isCanSee;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

}
