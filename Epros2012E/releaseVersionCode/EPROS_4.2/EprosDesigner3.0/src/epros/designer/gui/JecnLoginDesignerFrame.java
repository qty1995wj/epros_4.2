package epros.designer.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.LoginBean;
import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.PrfRelatedTemplet;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.autoUpdate.VersionValidate;
import epros.designer.gui.common.ConfigCommon;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.common.ConfigCommon.ConfigCommonEnum;
import epros.designer.gui.popedom.person.PasswordUpdateDialog;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnLoginProperties;
import epros.designer.util.JecnLoginUtil;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.designer.JecnDesigner;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.frame.JecnFrame;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.system.JecnUserCheckInfoProcess;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/*******************************************************************************
 * 登录
 * 
 * @author 2012-08-01
 * 
 */
public class JecnLoginDesignerFrame extends JecnFrame {
	private static Logger log = Logger.getLogger(JecnLoginDesignerFrame.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(500, 300);

	/** 图片显示面板 */
	private JecnPanel topPanel = new JecnPanel(500, 170);

	/** 存放背景图片的Lab */
	private JLabel logoLab = new JLabel();

	ImageIcon logoImEn = new ImageIcon("images/icons/loginLogoEN.jpg");
	ImageIcon logoIm = new ImageIcon("images/icons/loginLogo.jpg");

	/** 信息显示面板 */
	private JecnPanel infoPanel = new JecnPanel(470, 210);

	/** 底部显示面板 * */
	private JecnPanel bottomPanel = new JecnPanel(470, 1);

	/** 按妞面板 * */
	private JecnPanel buttonPanel = new JecnPanel(210, 30);

	/** 用户名Lab */
	private JLabel userNameLab = new JLabel(JecnLoginProperties.getValue("loginNameC"));

	/** 用户名 Field */
	private JecnTextField userNameField = new JecnTextField(200, 20);

	/** 密码Lab */
	private JLabel pwdLab = new JLabel();

	/** 密码Field */
	private JPasswordField pwdField = new JPasswordField();

	/** 服务器 Lab */
	private JLabel serverLab = new JLabel(JecnLoginProperties.getValue("loginServer"));
	/** 服务器 */
	private JComboBox serverComBox = new JComboBox(new String[] {});

	/** 端口 Lab */
	private JLabel portLab = new JLabel();

	/** 端口号 */
	private JComboBox portComBox = new JComboBox(new String[] {});

	/** 语言 Lab */
	private JLabel languageLab = new JLabel();

	/** 语言选择下拉 */
	private JComboBox languageComBox = new JComboBox(new String[] {});

	/** *checkBoxPanel */
	private JecnPanel checkBoxPanel = new JecnPanel(200, 22);

	/** 记住密码CheckBox */
	private JCheckBox rembCheckBox = new JCheckBox(JecnLoginProperties.getValue("remPwd"));

	// /***自动登录CheckBox*/
	// private JCheckBox automaticLoginCheckBox = new JCheckBox("自动登录");
	// /**修改密码*/
	// private JButton updatePwdBtn = new JButton("密码修改");

	/** 登录 */
	private JButton loginBut = new JButton();

	/** 域名登录 */
	private JButton domainLogin = new JButton(JecnLoginProperties.getValue("domainLogin"));
	/** 退出 */
	private JButton exitBut = new JButton();

	/** bottom */
	private JLabel bottomLab = new JLabel();

	/** 设置大小 */
	Dimension dimension = null;

	String propertyFileName = null;//

	/** IP验证码 */
	private String test = "([1-9]|[1-9]\\d|1\\d{2}|2[0-1]\\d|22[0-3])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";
	/** 端口号验证 */
	private String portVerf = "^[0-9]*$";
	private Pattern portPattern = Pattern.compile(portVerf);
	private Pattern pattern = Pattern.compile(test);
	private Matcher matcher = null;

	/** 服务器Model */
	private ComboBoxModel serverModel = null;

	/** 端口Model */
	private ComboBoxModel portModel = null;

	/** 登录提示Lab */
	private JLabel loginLab = new JLabel();
	/** 读取配置文件的值 */
	Properties propsOld = null;
	// "resources/JecnConfig.properties";// resources/JecnConfig.properties

	/** 登录时判断，服务器格式是否正确 */
	private boolean isLogin = false;
	/** 登录时判断 端口号是否输入正确 */
	private boolean portIsLogin = false;

	/** 登录Bean* */
	private LoginBean loginBean = null;
	/** 登录类型 0输入密码登录 1 记住密码登录 在密码框有输入时成0;2：域用户登录 */
	private int loginType = 1;

	/** *用户名 */
	// 用户名
	String loginName = null;
	/** 密码 */
	// 密码
	String passward = null;

	// 设置连接的IP和端口
	String selServer = null;
	String selPort = null;

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public JecnLoginDesignerFrame() {
		// 隐藏标题
		// this.setUndecorated(true);
		this.setSize(500, 390);
		this.setResizable(false);
		this.setTitle(JecnLoginProperties.getValue("loginTitle"));
		// this.setModal(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JecnFrame.DISPOSE_ON_CLOSE);

		dimension = new Dimension(200, 20);
		serverComBox.setPreferredSize(dimension);
		serverComBox.setMaximumSize(dimension);
		serverComBox.setMinimumSize(dimension);
		portComBox.setPreferredSize(dimension);
		portComBox.setMaximumSize(dimension);
		portComBox.setMinimumSize(dimension);
		languageComBox.setPreferredSize(dimension);
		languageComBox.setMaximumSize(dimension);
		languageComBox.setMinimumSize(dimension);
		languageComBox.addItem("中文");
		languageComBox.addItem("English");

		dimension = new Dimension(210, 15);
		loginLab.setPreferredSize(dimension);
		loginLab.setMaximumSize(dimension);
		loginLab.setMinimumSize(dimension);
		loginLab.setForeground(Color.red);

		// 密码大小200, 20
		dimension = new Dimension(200, 20);
		pwdField.setPreferredSize(dimension);
		pwdField.setMaximumSize(dimension);
		pwdField.setMinimumSize(dimension);

		// 记住密码大小设置
		dimension = new Dimension(150, 20);
		rembCheckBox.setPreferredSize(dimension);
		rembCheckBox.setMaximumSize(dimension);
		rembCheckBox.setMinimumSize(dimension);
		// //自动登录大小设置
		// automaticLoginCheckBox.setPreferredSize(dimension);
		// automaticLoginCheckBox.setMaximumSize(dimension);
		// automaticLoginCheckBox.setMinimumSize(dimension);

		// 添加横线背景颜色
		bottomPanel.setBackground(new java.awt.Color(1, 1, 1));
		rembCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// automaticLoginCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 记住密码初始设置不选中
		rembCheckBox.setSelected(false);
		// //自动登录初始设置不选中
		// automaticLoginCheckBox.setSelected(false);
		serverComBox.setEditable(true);
		portComBox.setEditable(true);
		languageComBox.setEditable(true);
		pwdField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				if (JecnLoginDesignerFrame.this.isVisible()) {
					loginType = 0;
				}

			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				if (JecnLoginDesignerFrame.this.isVisible()) {
					loginType = 0;
				}
			}

			@Override
			public void changedUpdate(DocumentEvent e) {

			}
		});

		exitBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				exitButPerformed();
			}

		});
		loginBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				loginButPerformed();
			}

		});
		domainLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				domainLoginAction();
			}
		});

		// 服务器Combox按键事件
		serverComBox.getEditor().getEditorComponent().addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				serverIPAction();
			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});
		languageComBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					languageSetting();
				}
			}
		});

		// /端口号按键事件
		portComBox.getEditor().getEditorComponent().addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				portModelAction();

			}

			@Override
			public void focusGained(FocusEvent e) {

			}
		});

		// =========================== 读取服务器IP============================
		propsOld = new Properties();
		// 读取配置文件输入流
		InputStream in = null;
		FileInputStream fileInput = null;
		// 获得服务器 、端口
		try {
			// 获得配置文件地址
			propertyFileName = JecnLoginDesignerFrame.class.getResource("/").toURI().getPath()
					+ "resources/JecnConfig.properties";

			fileInput = new FileInputStream(propertyFileName);
			in = new BufferedInputStream(fileInput);
			propsOld.load(in);

			// serverComBox.addItem(JecnProperties.getConfigValue("serverIP1"));
			Enumeration<String> en = (Enumeration<String>) propsOld.propertyNames();
			String oldPwd = "";
			while (en.hasMoreElements()) {
				String key = en.nextElement();
				// 服务器IP
				if ("serverIP1".equals(key) || "serverIP2".equals(key) || "serverIP3".equals(key)) {
					serverComBox.addItem(propsOld.getProperty(key));
				}

				// 端口
				else if ("serverPort1".equals(key) || "serverPort2".equals(key) || "serverPort3".equals(key)) {
					portComBox.addItem(propsOld.getProperty(key));
				}
				// 登录名
				else if ("loginName".equals(key)) {
					userNameField.setText(propsOld.getProperty(key));
				}
				// 密码
				else if ("password".equals(key)) {
					if (propsOld.getProperty(key) != null) {
						oldPwd = propsOld.getProperty(key).toString();
					}

				}
				// 记住密码
				else if ("remPwd".equals(key)) {
					if (propsOld.getProperty(key) != null && !"".equals(propsOld.getProperty(key))) {
						if ("1".equals(propsOld.getProperty(key))) {
							rembCheckBox.setSelected(true);
						}
					}
				} else if ("language".equals(key)) {
					String language = propsOld.getProperty(key);
					if (StringUtils.isNotBlank(language)) {
						if ("1".equals(language)) {
							languageComBox.setSelectedIndex(1);
						} else {
							languageComBox.setSelectedIndex(0);
						}
					}
				}
			}
			if (rembCheckBox.isSelected()) {
				pwdField.setText(oldPwd);
			}
		} catch (Exception ex) {
			log.error("JecnLoginDesignerFrame is error", ex);
			return;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					log.error("JecnLoginDesignerFrame is error", e);
					return;
				}
			}
			if (fileInput != null) {
				try {
					fileInput.close();
				} catch (IOException e) {
					log.error("JecnLoginDesignerFrame is error", e);
					return;
				}
			}
		}
		/**
		 * 1.versionType =1 是D2单机版 2.隐藏ip地址和端口号 3.切换登录背景图片为D2
		 */
		if (JecnConstants.versionType == 1) {
			// 设置登录图片
			logoIm = new ImageIcon("images/icons/loginLogo-eps.jpg");
			logoLab.setIcon(logoIm);
			// 是否存在浏览端，如果不存在，则隐藏服务器和端口号，登录名称显示框置灰
			// 服务器隐藏
			serverLab.setVisible(false);
			serverComBox.setVisible(false);
			// 端口号隐藏
			portLab.setVisible(false);
			portComBox.setVisible(false);
			// 登录名称编辑框置灰
			userNameField.setEditable(false);
			userNameField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		}
		if (ConfigCommon.INSTANTCE.isDomainLogin()) {
			serverComBox.setEditable(false);
			portComBox.setEditable(false);
		}
		initLayout();

		initLanguageTest();
	}

	private void initLanguageTest() {
		JecnLoginProperties.initLanguageMaps(languageComBox.getSelectedIndex());
		// 根据国际化加载图片
		String isIflytekLogin = ConfigCommon.INSTANTCE.getConfigProperties(ConfigCommonEnum.isIflytekLogin);
		if (Boolean.valueOf(isIflytekLogin)) {
			logoImEn = new ImageIcon("images/icons/iBPMSloginLogoEN.jpg");
			logoIm = new ImageIcon("images/icons/iBPMSloginLogoZh.jpg");
		}
		logoLab.setIcon(languageComBox.getSelectedIndex() == 1 ? logoImEn : logoIm);
		userNameLab.setText(JecnLoginProperties.getValue("loginNameC"));
		pwdLab.setText(JecnLoginProperties.getValue("loginPwd"));
		serverLab.setText(JecnLoginProperties.getValue("loginServer"));
		languageLab.setText(JecnLoginProperties.getValue("languageC"));
		rembCheckBox.setText(JecnLoginProperties.getValue("remPwd"));
		portLab.setText(JecnLoginProperties.getValue("loginPort"));
		loginBut.setText(JecnLoginProperties.getValue("loginBut"));
		domainLogin.setText(JecnLoginProperties.getValue("domainLogin"));
		exitBut.setText(JecnLoginProperties.getValue("exitBut"));
		bottomLab.setText(JecnLoginProperties.getValue("loginDes"));
	}

	/***************************************************************************
	 * 服务器IP所在的JCombox的触发的事件
	 */
	private void serverIPAction() {
		boolean isReturn = false;
		// 获得编辑的服务器IP
		String strEditText = ((JTextField) serverComBox.getEditor().getEditorComponent()).getText();
		// 服务器下拉框的数据条数
		int serverNum = serverComBox.getItemCount();
		// 服务器IP
		String serverText = null;
		// 服务器不能为空
		if (DrawCommon.isNullOrEmtryTrim(strEditText)) {
			loginLab.setText(JecnLoginProperties.getValue("loginServerNo"));
			return;
		} else {
			loginLab.setText("");
		}
		// // IP格式验证
		// matcher = pattern.matcher(strEditText);
		// if (!matcher.matches()) {
		// loginLab.setText(JecnProperties.getValue("loginServerIP"));
		// isLogin = true;
		// return;
		// }
		// 判断serverComBox中是否存在正在编辑的IP
		for (int i = 0; i < serverNum; i++) {
			serverText = serverComBox.getItemAt(i).toString();
			if (strEditText.equals(serverText)) {
				isReturn = true;
			}
		}
		if (!isReturn) {
			// 删除最后一个IP
			serverComBox.removeItemAt(2);
			serverModel = serverComBox.getModel();
			newServerModel(strEditText);
		}
		isLogin = false;
	}

	public static PropertyResourceBundle resourceBundle;

	/**
	 * 设置语言
	 */
	public void languageSetting() {
		if (languageComBox.getSelectedIndex() == 0) {
			JecnResourceUtil.setLocale(Locale.CHINESE);
			JOptionPane.setDefaultLocale(Locale.CHINA);
		} else {
			JecnResourceUtil.setLocale(Locale.ENGLISH);
			JOptionPane.setDefaultLocale(Locale.ENGLISH);
		}
		initLanguageTest();
	}

	/***************************************************************************
	 * 重新显示服务器Combox
	 * 
	 * @param strEditText
	 */
	private void newServerModel(String strEditText) {
		String serverIP0 = serverModel.getElementAt(0).toString();
		String serverIP1 = serverModel.getElementAt(1).toString();
		serverComBox.addItem(strEditText);
		// 删除余下的IP
		serverComBox.removeItemAt(0);
		// 删除一个后，第二个值的索引变为0,所以应该删除索引为0的数据
		serverComBox.removeItemAt(0);
		// 从新加载
		serverComBox.addItem(serverIP0);
		serverComBox.addItem(serverIP1);
	}

	/***************************************************************************
	 * 端口所在的JCombox的触发事件
	 */
	private void portModelAction() {
		boolean isReturn = false;
		// 获得编辑的端口号
		String strEditPort = ((JTextField) portComBox.getEditor().getEditorComponent()).getText();

		// 端口号不能为空
		if (DrawCommon.isNullOrEmtryTrim(strEditPort)) {
			loginLab.setText(JecnLoginProperties.getValue("loginPortNo"));
			return;
		} else {
			loginLab.setText("");
		}
		// 端口号只能输入数字
		matcher = portPattern.matcher(strEditPort);
		if (!matcher.matches()) {
			loginLab.setText(JecnLoginProperties.getValue("portIsNum"));
			portIsLogin = true;
			return;
		}
		// 端口号下拉框的数据条数
		int serverNum = portComBox.getItemCount();
		// 端口号
		String portText = null;
		// 端口号验证
		// 判断serverComBox中是否存在正在编辑的端口号
		for (int i = 0; i < serverNum; i++) {
			portText = portComBox.getItemAt(i).toString();
			if (strEditPort.equals(portText)) {
				isReturn = true;
			}
		}
		if (!isReturn) {
			// 删除最后一个端口号
			portComBox.removeItemAt(2);
			portModel = portComBox.getModel();
			newPortModel(strEditPort);
		}
		portIsLogin = false;
	}

	/***************************************************************************
	 * 重新显示 端口Combox
	 * 
	 * @param strEditPort
	 */
	private void newPortModel(String strEditPort) {
		String port0 = portModel.getElementAt(0).toString();
		String port1 = portModel.getElementAt(1).toString();
		portComBox.addItem(strEditPort);
		// 删除余下的端口号
		portComBox.removeItemAt(0);
		// 删除一个后，第二个值的索引变为0,所以应该删除索引为0的数据
		portComBox.removeItemAt(0);
		// 从新加载
		portComBox.addItem(port0);
		portComBox.addItem(port1);
	}

	private void writeServerProperties() {
		// 获取服务器Combox的对象
		serverModel = serverComBox.getModel();
		// 获取端口Combox的对象
		portModel = portComBox.getModel();
		// 文件输出流
		FileOutputStream fos = null;
		// 获取配置文件serverCfg.properties
		String path = null;
		try {
			path = JecnLoginDesignerFrame.class.getResource("/").toURI().getPath() + "resources/JecnConfig.properties";

			// 服务器
			int ss = 0;
			for (int s = serverModel.getSize(); s > 0; s--) {

				propsOld.setProperty("serverIP" + s, serverModel.getElementAt(ss).toString());
				ss++;
			}
			// 端口号
			int pp = 0;
			for (int s = portModel.getSize(); s > 0; s--) {

				propsOld.setProperty("serverPort" + s, portModel.getElementAt(pp).toString());
				pp++;
			}
			// 重新赋值登录名
			propsOld.setProperty("loginName", this.userNameField.getText());
			// 重新赋值密码,密码加密
			propsOld.setProperty("password", JecnUtil.getSecret(new String(this.pwdField.getPassword())).trim());
			// 记住密码
			if (rembCheckBox.isSelected()) {
				propsOld.setProperty("remPwd", "1");
				propsOld.setProperty("password", JecnConstants.loginBean.getJecnUser().getPassword());
			} else {
				propsOld.setProperty("remPwd", "0");
				propsOld.setProperty("password", "");
			}
			propsOld.setProperty("language", languageComBox.getSelectedIndex() + "");
			fos = new FileOutputStream(path);
			// 将Properties集合保存到流中
			propsOld.store(fos, "JecnConfig.properties");
		} catch (Exception ex) {
			log.error("JecnLoginDesignerFrame is error", ex);
			return;
		} finally {
			if (fos != null) {
				try {
					// 关闭流
					fos.close();
				} catch (IOException e) {
					log.error("JecnLoginDesignerFrame is error", e);
					return;
				}
			}
		}
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets inset = new Insets(0, 4, 4, 4);
		Insets insetd = new Insets(4, 4, 0, 4);
		Insets insets = new Insets(4, 4, 4, 4);
		Insets insetb = new Insets(4, 4, 15, 4);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, inset,
				0, 0);
		mainPanel.add(topPanel, c);
		topPanel.add(logoLab);
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		// 用户名
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(userNameLab, c);
		c = new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(userNameField, c);
		// 密码
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(pwdLab, c);
		c = new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(pwdField, c);
		// 服务器
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(serverLab, c);
		c = new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(serverComBox, c);
		// 端口
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insetd, 0, 0);
		infoPanel.add(portLab, c);
		c = new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insetd, 0,
				0);
		infoPanel.add(portComBox, c);
		// 语言
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insetd, 0, 0);
		infoPanel.add(languageLab, c);
		c = new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insetd, 0,
				0);
		infoPanel.add(languageComBox, c);
		// 提示loginLab
		c = new GridBagConstraints(1, 6, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				new Insets(0, 13, 0, 4), 0, 0);
		infoPanel.add(loginLab, c);
		// CheckBoxPanel
		c = new GridBagConstraints(1, 5, 2, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 4), 0, 0);
		infoPanel.add(checkBoxPanel, c);
		checkBoxPanel.setLayout(new GridBagLayout());
		// 记住密码rembCheckBox
		c = new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				new Insets(0, 4, 0, 4), 0, 0);
		checkBoxPanel.add(rembCheckBox, c);
		// 密码修改
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 4, 0, 4), 0, 0);
		checkBoxPanel.add(new JLabel(), c);
		// 按钮buttonPanel
		c = new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, inset, 0, 0);
		infoPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		// buttonPanel.add(updatePwdBtn);
		if (ConfigCommon.INSTANTCE.isDomainLogin()) {
			buttonPanel.add(domainLogin);
		}
		buttonPanel.add(loginBut);
		buttonPanel.add(exitBut);
		// 线 bottomPanel
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		mainPanel.add(bottomPanel, c);
		// 底部
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insetb, 0,
				0);
		mainPanel.add(bottomLab, c);
		this.getContentPane().add(mainPanel);
		// 为登录按钮增加回车事件
		this.getRootPane().setDefaultButton(loginBut);

		checkBoxPanel.setVisible(ConfigCommon.INSTANTCE.remberPwdShow());

	}

	/***************************************************************************
	 * 登录
	 */
	private void loginButPerformed() {
		// login();
		LoginTask t = new LoginTask();
		// t.execute();
		JecnLoading.start(t);

	}

	/**
	 * 退出
	 */
	private void exitButPerformed() {
		System.exit(0);
	}

	/**
	 * @author yxw 2012-8-10
	 * @description：面板保存进度控制线程
	 */
	class LoginTask extends SwingWorker<Void, Void> {

		@Override
		protected Void doInBackground() throws Exception {
			try {
				login();
			} catch (Exception e) {
				log.error("JecnLoginDesignerFrame is error ", e);
				done();
				JecnOptionPane.showMessageDialog(JecnLoginDesignerFrame.this, JecnLoginProperties
						.getValue("serverConnException"));
			}
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	class DomainLoginTask extends SwingWorker<Void, Void> {

		@Override
		protected Void doInBackground() throws Exception {
			try {
				domainLogin();
			} catch (Exception e) {
				log.error("JecnLoginDesignerFrame is error", e);
				done();
				JecnOptionPane.showMessageDialog(JecnLoginDesignerFrame.this, JecnLoginProperties
						.getValue("serverConnException"));
			}
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	private void login() throws Exception {
		loginName = this.userNameField.getText().trim();
		passward = new String(this.pwdField.getPassword());
		userNameField.setText(loginName);
		if (DrawCommon.isNullOrEmtryTrim(loginName)) {
			loginLab.setText(JecnLoginProperties.getValue("loginName").trim()  +" "+  JecnLoginProperties.getValue("notNull"));
			return;
		} else if (JecnUserCheckUtil.getTextLength(loginName) > 122) {
			loginLab.setText(JecnLoginProperties.getValue("loginName").trim()
					 +" "+ JecnLoginProperties.getValue("nameLengthInfo"));
			return;
		} else {
			loginLab.setText("");
		}
		if (DrawCommon.isNullOrEmtryTrim(passward)) {
			loginLab.setText(JecnLoginProperties.getValue("loginPwdNo"));
			return;
		} else {
			loginLab.setText("");
		}

		/**
		 * 登录类型 0输入密码登录 1 记住密码登录 在密码框有输入时成0 根据登录类型判断 是否验证长度
		 */
		if (this.loginType == 0) {
			if (DrawCommon.checkPasswordMaxLength(passward)) {
				loginLab.setText(JecnLoginProperties.getValue("passwordIsEnough"));
				return;
			}
		}
		// 登录验证
		if (!loginValidate()) {
			return;
		}

		try {
			loginBean = ConnectionPool.getPersonAction().loginDesign(loginName, passward,
					JecnLoginUtil.getMacAddress(), loginType);
		} catch (Exception e) {
			log.error("JecnLoginDesignerFrame is error", e);
			throw e;
		}
		loginState();
	}

	/**
	 * 登录验证
	 * 
	 * @throws Exception
	 */
	private boolean loginValidate() throws Exception {
		// 服务器IP不能为空
		String strEditText = ((JTextField) serverComBox.getEditor().getEditorComponent()).getText();
		if (DrawCommon.isNullOrEmtryTrim(strEditText)) {
			loginLab.setText(JecnLoginProperties.getValue("loginServerNo"));
			return false;
		} else {
			loginLab.setText("");
		}
		// 服务器IP格式不正确
		if (isLogin) {
			loginLab.setText(JecnLoginProperties.getValue("loginServerIP"));
			return false;
		}

		// 端口号不能为空
		String strEditPort = ((JTextField) portComBox.getEditor().getEditorComponent()).getText();
		if (DrawCommon.isNullOrEmtryTrim(strEditPort)) {
			loginLab.setText(JecnLoginProperties.getValue("loginPortNo"));
			return false;
		} else {
			loginLab.setText("");
		}
		// /端口号只能输入数字
		if (portIsLogin) {
			loginLab.setText(JecnLoginProperties.getValue("portIsNum"));
			return false;
		}

		// 设置连接的IP和端口
		selServer = serverComBox.getSelectedItem().toString();
		selPort = portComBox.getSelectedItem().toString();
		ConnectionPool.setServerIp(selServer);
		ConnectionPool.setServerPort(selPort);
		ConnectionPool.actionToNull();

		// 验证版本更新
		if (!VersionValidate.validate(this)) {
			JecnOptionPane.showMessageDialog(this, JecnLoginProperties.getValue("versionUpdateException"));
			return false;
		}

		// mac地址
		String macAddress = JecnLoginUtil.getMacAddress();
		if (DrawCommon.isNullOrEmtryTrim(macAddress)) {
			loginLab.setText(JecnLoginProperties.getValue("noMac"));
			log.error("JecnLoginDesignerFrame is error!");
			return false;
		}

		return true;
	}

	/**
	 * 登录后提示信息处理
	 * 
	 * @throws Exception
	 */
	private void loginState() throws Exception {
		if (loginBean.getLoginState() == 0) {
			// 1、 加载资源文件
			JecnProperties.loadProperties();
			JecnUserCheckInfoProcess.initUserCHeckInfoData();
			// 2、 初始化画图面板必须设置
			JecnInitDraw.initDraw();
			// 登录成功
			JecnConstants.loginBean = loginBean;
			if ("123456".equals(passward)) {// 判断是否是默认密码，如果是，弹出提示框，修改后再登录
				// JecnOptionPane.showMessageDialog(JecnSystemStaticData
				// .getFrame(), "当前密码是系统默认密码，请修改后再登录！");

				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("DLPrompt"), null, JecnOptionPane.CANCEL_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.CANCEL_OPTION) {
					return;
				}

				// 密码修改界面
				PasswordUpdateDialog pwdUpdateDialog = new PasswordUpdateDialog(null);
				pwdUpdateDialog.setVisible(true);
				return;
			}
			JecnConstants.versionType = loginBean.getVersionType();
			// 0流程元素编辑点是否左上进右下出
			int isEditPortType = 0;

			List<JecnConfigItemBean> peopleItemList = ConnectionPool.getConfigAciton()
					.selectConfigItemBeanByType(1, 10);

			for (JecnConfigItemBean configItemBean : peopleItemList) {
				if (ConfigItemPartMapMark.otherEditPort.toString().equals(configItemBean.getMark())) {
					isEditPortType = Integer.valueOf(configItemBean.getValue());
				}
			}
			JecnSystemStaticData.setEditPortType(isEditPortType);
			JecnDesigner.isAdmin = loginBean.isAdmin();
			// 获取服务器COmbox的对象
			serverModel = serverComBox.getModel();
			// 获取端口Combox的对象
			portModel = portComBox.getModel();
			// 当前选中的数据

			// 当前选中的索引
			int indexServer = serverComBox.getSelectedIndex();
			int indexPort = portComBox.getSelectedIndex();

			// 删除选中的服务器和端口号
			serverComBox.removeItemAt(indexServer);
			portComBox.removeItemAt(indexPort);
			newServerModel(selServer);
			newPortModel(selPort);
			// 将serverIP和端口号Combox下的数据写入配置文件
			writeServerProperties();
			this.dispose();
			// 设项目ID
			String projectId = JecnProperties.getConfigValue("projectId");
			// 判断是否有值，如果没有值则设为主项目
			if (!DrawCommon.isNullOrEmtryTrim(projectId)) {
				JecnConstants.projectId = Long.valueOf(projectId);
				try {
					// 判断项目是否存在
					JecnProject jecnProject = ConnectionPool.getProject().getProjectById(JecnConstants.projectId);
					if (jecnProject == null) {
						// 如果不存在设为主项目
						JecnCurProject jecnCurProject = ConnectionPool.getCurProject().getJecnCurProjectById();
						if (jecnCurProject != null) {
							JecnConstants.projectId = jecnCurProject.getCurProjectId();
						} else {
							JecnConstants.projectId = null;
						}
					}
				} catch (Exception e) {
					// JecnOptionPane.showMessageDialog(this, JecnProperties
					// .getValue(""));
					log.error("JecnLoginDesignerFrame is error", e);
					throw e;
				}
			} else {
				try {
					// 获取主项目
					JecnCurProject jecnCurProject = ConnectionPool.getCurProject().getJecnCurProjectById();
					if (jecnCurProject != null) {
						JecnConstants.projectId = jecnCurProject.getCurProjectId();
					} else {
						JecnConstants.projectId = null;
					}

				} catch (Exception e) {
					log.error("JecnLoginDesignerFrame is error", e);
					throw e;
				}
			}
			// 修改配置文件
			addMaintainNodeResource(); // 加载设计器 节点 配置表
			addDataResource();
			JecnDesignerFrame.startDesigner();
			// 初始化一些配置项
			initDesignerConfigs();

			if (!JecnDesigner.isAdmin) {
				JecnConstants.getDesignAuthFlows = ConnectionPool.getProcessAction().getDesignAuthFlows(
						loginBean.getJecnUser().getPeopleId(), JecnConstants.projectId);
			}
		} else if (loginBean.getLoginState() == 1) {
			// 登录名称不存在
			loginLab.setText(JecnLoginProperties.getValue("loginNotName"));
		} else if (loginBean.getLoginState() == 2) {
			// 密码不正确
			loginLab.setText(JecnLoginProperties.getValue("loginIsPwd"));
			return;
		} else if (loginBean.getLoginState() == 3) {
			// 用户锁定
			loginLab.setText(JecnLoginProperties.getValue("locedName"));
		} else if (loginBean.getLoginState() == 4) {
			// 另一个用户已登录
			loginLab.setText(JecnLoginProperties.getValue("userLogined"));
		} else if (loginBean.getLoginState() == 5) {
			// 用户没有设计权限
			loginLab.setText(JecnLoginProperties.getValue("userPermissions"));
		}
	}

	/**
	 * 域名登录
	 * 
	 * @throws Exception
	 */
	private void domainLogin() throws Exception {
		String userName = System.getenv("USERNAME");
		log.info("用户= " + userName);
		log.info("域名= " + System.getenv("USERDOMAIN"));
		if (DrawCommon.isNullOrEmtryTrim(userName)) {// 域用户为空
			loginLab.setText(JecnLoginProperties.getValue("loginName").trim() +" "+ JecnLoginProperties.getValue("notNull"));
			return;
		} else {
			loginLab.setText("");
		}
		// 登录验证
		loginValidate();
		loginType = 2;
		try {
			loginBean = ConnectionPool.getPersonAction().loginDesign(userName, passward, JecnUtil.getMacAddress(),
					loginType);
		} catch (Exception e) {
			log.error("JecnLoginDesignerFrame is error", e);
			throw e;
		}
		loginState();
	}

	protected void domainLoginAction() {
		DomainLoginTask t = new DomainLoginTask();
		JecnLoading.start(t);
	}

	/**
	 * 初始化设计器配置项
	 */
	public void initDesignerConfigs() {
		String value = ConnectionPool.getConfigAciton().selectValue(1, "processShowNumber");
		JecnConstants.isProcessShowNumber = JecnConfigContents.ITEM_IS_SHOW.equals(value) ? true : false;

		value = ConnectionPool.getConfigAciton().selectValue(1, "visioImportAndExport");
		if (JecnConfigContents.ITEM_IS_SHOW.equals(value)) {
			JecnDrawMainPanel.getMainPanel().addVisioInOutMenus();
		}

		// 项目管理显示配置
		value = ConnectionPool.getConfigAciton().selectValue(6, ConfigItemPartMapMark.projectManagement.toString());
		JecnConstants.isShowProjectManagement = JecnConfigContents.ITEM_IS_SHOW.equals(value) ? true : false;
		if (!JecnConstants.isShowProjectManagement) {
			// 按钮工具栏，项目管理，根据系统配置显示
			JecnDesignerMainPanel.getDesignerMainPanel().getToolbarSetingButtonPanel().getProjectButton().setVisible(
					false);
		}
		// 初始化prf节点配置的任务模板
		initPrfRelatedTempletMap();

		// 文件支撑文件- 文件目录节点权限获取
		JecnDesignerCommon.addDesignerFileIdsByArchitectureNode();
		if (JecnConfigTool.isArchitectureUploadFile() && JecnConstants.loginBean.getSetFlow() != null
				&& JecnConstants.loginBean.getSetFlow().size() > 0) {
			try {
				List<Long> listIds = ConnectionPool.getFileAction().getFileAuthByFlowAutoCreateDir(
						JecnConstants.loginBean.getSetFlow());
				for (Long id : listIds) {
					JecnConstants.loginBean.getSetFile().add(id);
				}
			} catch (Exception e1) {
				log.error("getFileAuthByFlowAutoCreateDir erros", e1);
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			}
		}
	}

	private void initPrfRelatedTempletMap() {

		try {
			List<PrfRelatedTemplet> relatedTemplets = ConnectionPool.getTaskRecordAction().listPrfRelatedTemplet();
			for (PrfRelatedTemplet relatedTemplet : relatedTemplets) {
				/** 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务 */
				int type = relatedTemplet.getType();
				// 模板类型 0审批 1借阅 2废止
				int approveType = relatedTemplet.getTempletType();
				if (type == 0) {
					if (approveType == 0) {
						JecnConstants.processRelatedTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
								.getTempletId());
					} else if (approveType == 2) {
						JecnConstants.processRelatedAbolishTaskTemplet.put(relatedTemplet.getRelatedId(),
								relatedTemplet.getTempletId());
					}
					JecnConstants.processRelatedTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
							.getTempletId());
				} else if (type == 1) {
					if (approveType == 0) {
						JecnConstants.fileRelatedTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
								.getTempletId());
					} else if (approveType == 2) {
						JecnConstants.fileRelatedAbolishTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
								.getTempletId());
					}
					JecnConstants.fileRelatedTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
							.getTempletId());
				} else if (type == 2 || type == 3) {
					if (approveType == 0) {
						JecnConstants.ruleRelatedTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
								.getTempletId());
					} else if (approveType == 2) {
						JecnConstants.ruleRelatedAbolishTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
								.getTempletId());
					}
					JecnConstants.ruleRelatedTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
							.getTempletId());
				} else if (type == 4) {
					if (approveType == 0) {
						JecnConstants.processMapRelatedTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
								.getTempletId());
					} else if (approveType == 2) {
						JecnConstants.processMapRelatedAbolishTaskTemplet.put(relatedTemplet.getRelatedId(),
								relatedTemplet.getTempletId());
					}
					JecnConstants.processMapRelatedTaskTemplet.put(relatedTemplet.getRelatedId(), relatedTemplet
							.getTempletId());

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}

	}

	public static void main(String args[]) {

		try {
			// 解决ABC输入法BUG：java Swing 里面的文本框在输入的时候会弹出一个“输入窗口”
			System.setProperty("java.awt.im.style", "on-the-spot");
			// 外观
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
		}
		JecnLoginProperties.loadProperties();
		// 3、显示主面板
		JecnLoginDesignerFrame rd = new JecnLoginDesignerFrame();
		rd.setVisible(true);
	}

	/**
	 * 加载数据库配置
	 */
	private static void addDataResource() {
		try {

			List<JecnConfigItemBean> configs = ConnectionPool.getConfigAciton().selectConfigItemBeanByType(6, 4);
			// 修改密级
			for (JecnConfigItemBean config : configs) {
				if (ConfigItemPartMapMark.isPublic.toString().equals(config.getMark())) {
					JecnProperties.custOmResourceMap.put("gongKai", config.getName(JecnResourceUtil.getLocale()));
				} else if (ConfigItemPartMapMark.isSecret.toString().equals(config.getMark())) {
					JecnProperties.custOmResourceMap.put("secret", config.getName(JecnResourceUtil.getLocale()));
				} else if (ConfigItemPartMapMark.riskSysName.toString().equals(config.getMark())
						&& "1".equals(config.getValue()) ? true : false) {
					JecnProperties.custOmResourceMap.put("risk", config.getName(JecnResourceUtil.getLocale()));
				}
			}
		} catch (Exception e) {
			log.error("JecnLoginDesignerFrame addDataResource is error！", e);
		}
	}

	/**
	 * 加载 配置节点属性
	 */
	private static void addMaintainNodeResource() {
		List<Object[]> maintainNode = ConnectionPool.getConfigAciton().selectMaintainNode();
		int index = JecnResourceUtil.getLocale() == Locale.ENGLISH ? 3 : 1;
		if (maintainNode != null) {
			for (Object[] object : maintainNode) {
				JecnProperties.custOmResourceMap.put(object[0].toString(), object[index].toString());
			}
		}
	}

}
