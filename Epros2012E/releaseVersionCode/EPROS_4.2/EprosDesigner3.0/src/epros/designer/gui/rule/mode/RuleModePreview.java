package epros.designer.gui.rule.mode;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

public class RuleModePreview extends JecnDialog {

	/** 主面板 */
	private JPanel mainPanel = null;
	/** 滚动面板 */
	private JScrollPane scrollPane = null;
	/** 此面板存放标题 */
	private JPanel cpanel = null;

	/** 按钮面板 */
	private JPanel btnPanel = null;
	/** 取消按钮 */
	private JButton cancelBtn = null;

	public RuleModePreview(Vector<Vector<String>> vs) {
		initCompotents();
		initLayout(vs);

	}

	private void initCompotents() {
		this.setSize(800, 600);
		this.setResizable(true);
		this.setModal(true);

		this.mainPanel = new JPanel();
		this.scrollPane = new JScrollPane();
		this.cpanel = new JPanel();
		this.btnPanel = new JPanel();
		this.cancelBtn = new JButton(JecnProperties.getValue("closeBtn"));

		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);

		// 鼠标滚动大小
		scrollPane.getHorizontalScrollBar().setUnitIncrement(30);
		scrollPane.getVerticalScrollBar().setUnitIncrement(30);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		scrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		cpanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		btnPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		Dimension dimension = new Dimension(750, 35);
		this.btnPanel.setSize(dimension);
		this.btnPanel.setMinimumSize(dimension);
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelBtnAction();

			}
		});

	}

	private void initLayout(Vector<Vector<String>> vs) {

		this.mainPanel.setLayout(new BorderLayout());
		// 增加内容面板
		this.mainPanel.add(this.scrollPane, BorderLayout.CENTER);
		// 增加按钮面板
		this.mainPanel.add(this.btnPanel, BorderLayout.SOUTH);
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0);
		this.cpanel.setLayout(new GridBagLayout());

		this.btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		this.scrollPane.setViewportView(cpanel);
		this.btnPanel.add(this.cancelBtn);

		if (vs != null) {
			int i = 0;
			int j = 0;
			for (Vector<String> v : vs) {
				// 为title增加"、" 顿号
				String title = "";
				if (JecnResourceUtil.getLocale() == Locale.CHINESE) {//中文
					title = (j + 1) + JecnProperties.getValue("dunHao") + v.get(1);
				} else {
					title = (j + 1) + JecnProperties.getValue("dunHao") + v.get(2);
				}
				JPanel titlePanel = new JPanel();
				titlePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
				JButton button = new JButton(JecnProperties.getValue("selectBtn"));
				titlePanel.setLayout(new GridBagLayout());
				c = new GridBagConstraints(0, i, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
						new Insets(5, 5, 5, 5), 0, 0);
				this.cpanel.add(titlePanel, c);
				c = new GridBagConstraints(0, i, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(5, 5, 5, 5), 0, 0);
				titlePanel.add(new JLabel(title), c);
				c = new GridBagConstraints(1, i, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(5, 5, 5, 5), 0, 0);
				titlePanel.add(button, c);

				c = new GridBagConstraints(0, i + 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0);

				button.setVisible(false);
				if (JecnUtil.formatTitleTypeToInt(v.get(3)) == 0) {
					RuleModeTitleContentPanel tc = new RuleModeTitleContentPanel(title);
					tc.setBackground(JecnUIUtil.getDefaultBackgroundColor());
					this.cpanel.add(tc, c);
				} else if (JecnUtil.formatTitleTypeToInt(v.get(3)) == 1) {
					button.setVisible(true);
					RuleModeTitleFilePanel tf = new RuleModeTitleFilePanel(title);
					tf.setBackground(JecnUIUtil.getDefaultBackgroundColor());
					this.cpanel.add(tf, c);
				} else if (JecnUtil.formatTitleTypeToInt(v.get(3)) == 2) {
					RuleModeTitleProcessPanel tp = new RuleModeTitleProcessPanel(title);
					tp.setBackground(JecnUIUtil.getDefaultBackgroundColor());
					this.cpanel.add(tp, c);
				} else if (JecnUtil.formatTitleTypeToInt(v.get(3)) == 3) {
					RuleModeTitleProcessMapPanel tp = new RuleModeTitleProcessMapPanel(title);
					tp.setBackground(JecnUIUtil.getDefaultBackgroundColor());
					this.cpanel.add(tp, c);
				} else if (JecnUtil.formatTitleTypeToInt(v.get(3)) == 4) {
					RuleModeTitleStandarPanel tp = new RuleModeTitleStandarPanel(title);
					tp.setBackground(JecnUIUtil.getDefaultBackgroundColor());
					this.cpanel.add(tp, c);
				} else if (JecnUtil.formatTitleTypeToInt(v.get(3)) == 5) {
					RuleModeTitleRiskPanel tp = new RuleModeTitleRiskPanel(title);
					tp.setBackground(JecnUIUtil.getDefaultBackgroundColor());
					this.cpanel.add(tp, c);
				} else if (JecnUtil.formatTitleTypeToInt(v.get(3)) == 6) {
					RuleModeTitleRulePanel tp = new RuleModeTitleRulePanel(title);
						tp.setBackground(JecnUIUtil.getDefaultBackgroundColor());
						this.cpanel.add(tp, c);
				}

				i++;
				i++;
				j++;
			}
		}

		this.getContentPane().add(this.mainPanel);

	}

	private void cancelBtnAction() {
		this.dispose();
	}
}
