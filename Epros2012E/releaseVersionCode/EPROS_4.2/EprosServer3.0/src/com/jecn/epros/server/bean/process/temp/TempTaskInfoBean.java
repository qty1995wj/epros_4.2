package com.jecn.epros.server.bean.process.temp;

import java.util.Date;

public class TempTaskInfoBean {
	/**任务id*/
	private Long taskId;
	/**任务名称*/
	private String taskName;
	/**任务类型*/
	private String taskType;
	/**上个审批状态*/
	private Integer upState;
	/**审批状态*/
	private Integer state;
	/**操作日期*/
	private Date updateTime;
	/**审批顺序*//*
	private String order;*/
	/**操作*/
	private Integer taskElseState;

	
	public Integer getTaskElseState() {
		return taskElseState;
	}
	public void setTaskElseState(Integer taskElseState) {
		this.taskElseState = taskElseState;
	}
	/*public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}*/
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public Integer getUpState() {
		return upState;
	}
	public void setUpState(Integer upState) {
		this.upState = upState;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	 
	
	
	 

}
