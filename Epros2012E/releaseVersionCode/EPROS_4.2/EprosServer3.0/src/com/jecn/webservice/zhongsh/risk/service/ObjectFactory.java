
package com.jecn.webservice.zhongsh.risk.service;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.jecn.webservice.zhongsh.risk package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.jecn.webservice.zhongsh.risk
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UnitSearchSystemResponse }
     * 
     */
    public UnitSearchSystemResponse createUnitSearchSystemResponse() {
        return new UnitSearchSystemResponse();
    }

    /**
     * Create an instance of {@link SearchTaskCountResponse }
     * 
     */
    public SearchTaskCountResponse createSearchTaskCountResponse() {
        return new SearchTaskCountResponse();
    }

    /**
     * Create an instance of {@link SearchComplateTaskByOrganCodeResponse }
     * 
     */
    public SearchComplateTaskByOrganCodeResponse createSearchComplateTaskByOrganCodeResponse() {
        return new SearchComplateTaskByOrganCodeResponse();
    }

    /**
     * Create an instance of {@link SearchSystemByLevel }
     * 
     */
    public SearchSystemByLevel createSearchSystemByLevel() {
        return new SearchSystemByLevel();
    }

    /**
     * Create an instance of {@link GetDicBusinessCodeResponse }
     * 
     */
    public GetDicBusinessCodeResponse createGetDicBusinessCodeResponse() {
        return new GetDicBusinessCodeResponse();
    }

    /**
     * Create an instance of {@link SearchTask }
     * 
     */
    public SearchTask createSearchTask() {
        return new SearchTask();
    }

    /**
     * Create an instance of {@link SearchTaskListResponse }
     * 
     */
    public SearchTaskListResponse createSearchTaskListResponse() {
        return new SearchTaskListResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTask }
     * 
     */
    public ArrayOfTask createArrayOfTask() {
        return new ArrayOfTask();
    }

    /**
     * Create an instance of {@link SearchMessageResponse }
     * 
     */
    public SearchMessageResponse createSearchMessageResponse() {
        return new SearchMessageResponse();
    }

    /**
     * Create an instance of {@link SearchMessageAllResponse }
     * 
     */
    public SearchMessageAllResponse createSearchMessageAllResponse() {
        return new SearchMessageAllResponse();
    }

    /**
     * Create an instance of {@link SearchSystemByNameResponse }
     * 
     */
    public SearchSystemByNameResponse createSearchSystemByNameResponse() {
        return new SearchSystemByNameResponse();
    }

    /**
     * Create an instance of {@link SearchSystemByFileNo }
     * 
     */
    public SearchSystemByFileNo createSearchSystemByFileNo() {
        return new SearchSystemByFileNo();
    }

    /**
     * Create an instance of {@link SearchMessage }
     * 
     */
    public SearchMessage createSearchMessage() {
        return new SearchMessage();
    }

    /**
     * Create an instance of {@link HelloWorldResponse }
     * 
     */
    public HelloWorldResponse createHelloWorldResponse() {
        return new HelloWorldResponse();
    }

    /**
     * Create an instance of {@link SearchSystemOrdinanceByDocIdResponse }
     * 
     */
    public SearchSystemOrdinanceByDocIdResponse createSearchSystemOrdinanceByDocIdResponse() {
        return new SearchSystemOrdinanceByDocIdResponse();
    }

    /**
     * Create an instance of {@link SearchSystemByLevelResponse }
     * 
     */
    public SearchSystemByLevelResponse createSearchSystemByLevelResponse() {
        return new SearchSystemByLevelResponse();
    }

    /**
     * Create an instance of {@link SearchUpSystemList }
     * 
     */
    public SearchUpSystemList createSearchUpSystemList() {
        return new SearchUpSystemList();
    }

    /**
     * Create an instance of {@link SearchSystemByClassCode }
     * 
     */
    public SearchSystemByClassCode createSearchSystemByClassCode() {
        return new SearchSystemByClassCode();
    }

    /**
     * Create an instance of {@link SearchSystemByFileNoResponse }
     * 
     */
    public SearchSystemByFileNoResponse createSearchSystemByFileNoResponse() {
        return new SearchSystemByFileNoResponse();
    }

    /**
     * Create an instance of {@link SearchMessageAll }
     * 
     */
    public SearchMessageAll createSearchMessageAll() {
        return new SearchMessageAll();
    }

    /**
     * Create an instance of {@link SearchSmisSystemListResponse }
     * 
     */
    public SearchSmisSystemListResponse createSearchSmisSystemListResponse() {
        return new SearchSmisSystemListResponse();
    }

    /**
     * Create an instance of {@link SearchTaskResponse }
     * 
     */
    public SearchTaskResponse createSearchTaskResponse() {
        return new SearchTaskResponse();
    }

    /**
     * Create an instance of {@link SearchSmisSystemList }
     * 
     */
    public SearchSmisSystemList createSearchSmisSystemList() {
        return new SearchSmisSystemList();
    }

    /**
     * Create an instance of {@link HelloWorld }
     * 
     */
    public HelloWorld createHelloWorld() {
        return new HelloWorld();
    }

    /**
     * Create an instance of {@link SearchTaskByOrganCode }
     * 
     */
    public SearchTaskByOrganCode createSearchTaskByOrganCode() {
        return new SearchTaskByOrganCode();
    }

    /**
     * Create an instance of {@link SearchComplateTaskByOrganCode }
     * 
     */
    public SearchComplateTaskByOrganCode createSearchComplateTaskByOrganCode() {
        return new SearchComplateTaskByOrganCode();
    }

    /**
     * Create an instance of {@link UnitSearchSystem }
     * 
     */
    public UnitSearchSystem createUnitSearchSystem() {
        return new UnitSearchSystem();
    }

    /**
     * Create an instance of {@link SearchTaskList }
     * 
     */
    public SearchTaskList createSearchTaskList() {
        return new SearchTaskList();
    }

    /**
     * Create an instance of {@link SearchUpSystemListResponse }
     * 
     */
    public SearchUpSystemListResponse createSearchUpSystemListResponse() {
        return new SearchUpSystemListResponse();
    }

    /**
     * Create an instance of {@link SearchTaskCount }
     * 
     */
    public SearchTaskCount createSearchTaskCount() {
        return new SearchTaskCount();
    }

    /**
     * Create an instance of {@link SearchSystemOrdinanceByDocId }
     * 
     */
    public SearchSystemOrdinanceByDocId createSearchSystemOrdinanceByDocId() {
        return new SearchSystemOrdinanceByDocId();
    }

    /**
     * Create an instance of {@link SearchTaskByOrganCodeResponse }
     * 
     */
    public SearchTaskByOrganCodeResponse createSearchTaskByOrganCodeResponse() {
        return new SearchTaskByOrganCodeResponse();
    }

    /**
     * Create an instance of {@link SearchSystemByFileCode }
     * 
     */
    public SearchSystemByFileCode createSearchSystemByFileCode() {
        return new SearchSystemByFileCode();
    }

    /**
     * Create an instance of {@link GetDicBusinessCode }
     * 
     */
    public GetDicBusinessCode createGetDicBusinessCode() {
        return new GetDicBusinessCode();
    }

    /**
     * Create an instance of {@link SearchSystemByFileCodeResponse }
     * 
     */
    public SearchSystemByFileCodeResponse createSearchSystemByFileCodeResponse() {
        return new SearchSystemByFileCodeResponse();
    }

    /**
     * Create an instance of {@link SearchSystemByName }
     * 
     */
    public SearchSystemByName createSearchSystemByName() {
        return new SearchSystemByName();
    }

    /**
     * Create an instance of {@link SearchSystemByClassCodeResponse }
     * 
     */
    public SearchSystemByClassCodeResponse createSearchSystemByClassCodeResponse() {
        return new SearchSystemByClassCodeResponse();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

}
