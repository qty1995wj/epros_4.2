package com.jecn.epros.domain.file;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.process.ProcessAttribute;
import com.jecn.epros.mapper.UserMapper;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.HttpUtil;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.Map;

public class FileBaseInfoBean {
    private Long fileId;
    private String fileName;
    private Long flowId;
    private String flowName;
    private Long orgId;
    private String orgName;
    private String trueName;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date createDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date pubDate;
    private int isPublic;
    private boolean isPub;

    private Long historyId;
    /**
     * 保密级别
     **/
    private Integer confidentialityLevel;

    private boolean isShowLevel;

    private String docId;

    private Long commissionerId;//专员

    private String commissionerName;//专员名称

    private String keyWord;//关键字

    /** 责任人 **/
    private Long resPeopleId;
    /** 责任类型 **/
    private Integer typeResPeople;
    private String resPeopleName;


    public Long getResPeopleId() {
        return resPeopleId;
    }

    public void setResPeopleId(Long resPeopleId) {
        this.resPeopleId = resPeopleId;
    }

    public Integer getTypeResPeople() {
        return typeResPeople;
    }

    public void setTypeResPeople(Integer typeResPeople) {
        this.typeResPeople = typeResPeople;
    }

    public String getResPeopleName() {
        return resPeopleName;
    }

    public void setResPeopleName(String resPeopleName) {
        this.resPeopleName = resPeopleName;
    }

    public String getConfidentialityLevelStr() {
        return ConfigUtils.getConfidentialityLevel(confidentialityLevel);
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }
//------------


    public String getCommissionerName() {
        return commissionerName;
    }

    public void setCommissionerName(String commissionerName) {
        this.commissionerName = commissionerName;
    }

    public Long getCommissionerId() {
        return commissionerId;
    }

    public void setCommissionerId(Long commissionerId) {
        this.commissionerId = commissionerId;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public void setPub(boolean pub) {
        isPub = pub;
    }

    public String getHrefURL() {
        if (this.historyId == 0 || this.historyId == null) {
            return HttpUtil.getDownLoadFileHttp(fileId, isPub, "");
        } else {
            return HttpUtil.getDownLoadHistoryFileHttp(fileId, true, "", true);

        }
    }

    public String getPublicStr() {
        return ConfigUtils.getSecretByType(isPublic);
    }

    public LinkResource getFileViewLink() {
        return new LinkResource(fileId, fileName, ResourceType.FILE_VIEW);
    }

    public LinkResource getProcessLink() {
        return new LinkResource(flowId, flowName, ResourceType.PROCESS);
    }

    public LinkResource getOrgLink() {
        return new LinkResource(orgId, orgName, ResourceType.ORGANIZATION);
    }

    public LinkResource getCommLink() {
        return new LinkResource(commissionerId, commissionerName, ResourceType.PERSON);
    }

    public LinkResource getPeopleLink() {
        if(typeResPeople !=null) {
            return new LinkResource(resPeopleId, resPeopleName, typeResPeople == 0 ? ResourceType.PERSON : ResourceType.POSITION);
        }
        return new LinkResource(null, resPeopleName);
    }
    @JsonIgnore
    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    @JsonIgnore
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getFlowId() {
        return flowId;
    }

    @JsonIgnore
    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    @JsonIgnore
    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    @JsonIgnore
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    @JsonIgnore
    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public boolean getShowLevel() {
        isShowLevel = ConfigUtils.isShowItem(ConfigContents.ConfigItemPartMapMark.fileScurityLevel);
        return isShowLevel;
    }

}
