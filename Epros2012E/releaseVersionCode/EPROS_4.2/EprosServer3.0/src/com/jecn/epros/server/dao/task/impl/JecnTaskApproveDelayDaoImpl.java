package com.jecn.epros.server.dao.task.impl;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.task.IJecnTaskApproveDelayDao;

/**
 * 搁置任务提醒Dao
 * 
 * @author huoyl
 */
public class JecnTaskApproveDelayDaoImpl extends AbsBaseDao<JecnUser, Long>
		implements IJecnTaskApproveDelayDao {

	@Override
	public List<JecnUser> getAdminUserList() {
		String sql="select ju.* from jecn_user ju " //人员表
                   +" inner join JECN_USER_ROLE jur "//人员角色关联表
                   +" on ju.people_id=jur.people_id"
                   +" inner join JECN_ROLE_INFO jri "//角色表
                   +" on jur.role_id=jri.role_id"
                   +" where ju.islock=0 "//人员存在
                   +" and jri.filter_id='admin' "//角色为管理员
                   +" and ju.email is not null "//邮件地址不为空
                   +" and ju.email_type is not null ";//邮件类型不为空"
		
		return this.listNativeAddEntity(sql);
	}

	@Override
	public int getTaskDelayCount() {
		String delaySql = 
			"with tmp as" +
			" (select distinct jts.task_id" + 
			"    from jecn_task_stage jts" + 
			"   where jts.is_selected_user = 1" + 
			"     AND JTS.STAGE_MARK != 10" + 
			"     and (SELECT COUNT(JTPCN.id)" + 
			"            FROM JECN_TASK_PEOPLE_CONN_NEW  JTPCN," + 
			"                 jecn_user                  ju" + 
			"           WHERE JTPCN.STAGE_ID = jts.id" + 
			"             and JTPCN.approve_pid = ju.people_id" + 
			"             and ju.islock = 0) = 0" + 
			"  union" + 
			"  select distinct jts.task_id" + 
			"    from jecn_task_stage jts" + 
			"   where jts.is_selected_user = 1" + 
			"     AND JTS.STAGE_MARK = 3" + 
			"     and (SELECT COUNT(distinct JTPCN.approve_pid)" + 
			"            FROM JECN_TASK_PEOPLE_CONN_NEW  JTPCN," + 
			"                 jecn_user                  ju" + 
			"           WHERE JTPCN.STAGE_ID = jts.id" + 
			"             and JTPCN.approve_pid = ju.people_id" + 
			"             and ju.islock = 0) <>" + 
			"         (select COUNT(JTPCN.id)" + 
			"            FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN" + 
			"           WHERE JTPCN.STAGE_ID = jts.id)" + 
			"  union" + 
			"  select distinct jtbn.id" + 
			"    from jecn_task_bean_new jtbn" + 
			"    left join jecn_task_people_new jtpn" + 
			"      on jtbn.id = jtpn.task_id" + 
			"    left join jecn_user u" + 
			"      on jtpn.approve_pid = u.people_id" + 
			"   where (jtpn.id is null or u.islock = 1)" + 
			"     and jtbn.state <> 5" + 
			"     and jtbn.is_lock = 1)" + 
			"select count(a.id)" + 
			"  from jecn_task_bean_new a" + 
			" inner join tmp" + 
			"    on a.id = tmp.task_id" + 
			"  left join jecn_user u" + 
			"    on a.create_person_id = u.people_id" + 
			" where a.IS_LOCK = 1" + 
			"   and a.State <> 5";

		return this.countAllByParamsNativeSql(delaySql);
	}


}
