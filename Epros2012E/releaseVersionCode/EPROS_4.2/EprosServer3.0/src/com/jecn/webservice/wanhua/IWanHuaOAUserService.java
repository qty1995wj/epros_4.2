package com.jecn.webservice.wanhua;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.jecn.webservice.wanhua.bean.WanHuaPostUser;

/**
 * 万华- 人员组织同步接口
 * 
 * @author xiaohu
 * 
 */
@WebService(endpointInterface = "IWanHuaOAUserService")
public interface IWanHuaOAUserService {

	@WebMethod(operationName = "saveUsers")
	public String saveUsers(List<WanHuaPostUser> users);

}
