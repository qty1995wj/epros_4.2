package epros.draw.system;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import epros.draw.system.data.JecnSystemOftenColorData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 常用颜色处理对象
 * 
 * @author ZHOUXY
 * 
 */
class JecnSystemOftenColorProcess {
	/** 存储常用颜色数据对象 */
	private static JecnSystemOftenColorData systemOftenColorData = null;

	private static String getPath() {
		return "PropertyColorproperties.properties";
	}

	/**
	 * 读取常用颜色
	 * 
	 * @return
	 */
	static JecnSystemOftenColorData readOftenColorData() {
		if (systemOftenColorData == null) {
			// 存储常用颜色数据对象
			systemOftenColorData = new JecnSystemOftenColorData();
			// 读取配置文件数据
			importOftenColorByPropertiesFile();
		}
		return systemOftenColorData;
	}

	/**
	 * 
	 * 从Properties文件中读取常用颜色内容
	 * 
	 */
	private static void importOftenColorByPropertiesFile() {
		// 获取本地流程元素常用颜色
		Properties properties = JecnFileUtil.readProperties(getPath());
		if (properties != null) {
			List<Color> oftenFillColorList = new ArrayList<Color>();
			// 获取常用填充第一个颜色
			String oftenFillColor1 = properties.getProperty("oftenFillColor1");
			if (oftenFillColor1 != null) {
				oftenFillColorList.add(DrawCommon.getColorByString(oftenFillColor1));
			}
			// 获取常用填充第二个颜色
			String oftenFillColor2 = properties.getProperty("oftenFillColor2");
			if (oftenFillColor2 != null) {
				oftenFillColorList.add(DrawCommon.getColorByString(oftenFillColor2));
			}
			// 获取常用填充第三个颜色
			String oftenFillColor3 = properties.getProperty("oftenFillColor3");
			if(oftenFillColor3!=null){
				oftenFillColorList.add(DrawCommon.getColorByString(oftenFillColor3));
			}
			// 获取常用填充第四个颜色
			String oftenFillColor4 = properties.getProperty("oftenFillColor4");
			if(oftenFillColor4 != null){
				oftenFillColorList.add(DrawCommon.getColorByString(oftenFillColor4));
			}
			// 获取常用填充第五个颜色
			String oftenFillColor5 = properties.getProperty("oftenFillColor5");
			if(oftenFillColor5!=null){
				oftenFillColorList.add(DrawCommon.getColorByString(oftenFillColor5));
			}
			// 获取常用填充第六个颜色
			String oftenFillColor6 = properties.getProperty("oftenFillColor6");
			if(oftenFillColor6!=null){
				oftenFillColorList.add(DrawCommon.getColorByString(oftenFillColor6));
			}
			// 获取常用填充第七个颜色
			String oftenFillColor7 = properties.getProperty("oftenFillColor7");
			if(oftenFillColor7!=null){
				oftenFillColorList.add(DrawCommon.getColorByString(oftenFillColor7));
			}
			// 获取常用填充第八个颜色
			String oftenFillColor8 = properties.getProperty("oftenFillColor8");
			if(oftenFillColor8!=null){
				oftenFillColorList.add(DrawCommon.getColorByString(oftenFillColor8));
			}
			// 把List 的值 赋给fillColorList
			systemOftenColorData.setFillColorList(oftenFillColorList);
			
			

			List<Color> oftenShadowColorList = new ArrayList<Color>();
			// 获取常用阴影第一个颜色
			String oftenShadowColor1 = properties
					.getProperty("oftenShadowColor1");
			if(oftenShadowColor1!=null){
				oftenShadowColorList.add(DrawCommon.getColorByString(oftenShadowColor1));
			}
			// 获取常用阴影第二个颜色
			String oftenShadowColor2 = properties
					.getProperty("oftenShadowColor2");
			if(oftenShadowColor2!=null){
				oftenShadowColorList.add(DrawCommon.getColorByString(oftenShadowColor2));
			}
			// 获取常用阴影第三个颜色
			String oftenShadowColor3 = properties
					.getProperty("oftenShadowColor3");
			if(oftenShadowColor3!=null){
				oftenShadowColorList.add(DrawCommon.getColorByString(oftenShadowColor3));
			}
			// 获取常用阴影第四个颜色
			String oftenShadowColor4 = properties
					.getProperty("oftenShadowColor4");
			if(oftenShadowColor4!=null){
				oftenShadowColorList.add(DrawCommon.getColorByString(oftenShadowColor4));
			}
			// 获取常用阴影第五个颜色
			String oftenShadowColor5 = properties
					.getProperty("oftenShadowColor5");
			if(oftenShadowColor5!=null){
				oftenShadowColorList.add(DrawCommon.getColorByString(oftenShadowColor5));
			}
			// 获取常用阴影第六个颜色
			String oftenShadowColor6 = properties
					.getProperty("oftenShadowColor6");
			if(oftenShadowColor6!=null){
				oftenShadowColorList.add(DrawCommon.getColorByString(oftenShadowColor6));
			}
			// 获取常用阴影第七个颜色
			String oftenShadowColor7 = properties
					.getProperty("oftenShadowColor7");
			if(oftenShadowColor7!=null){
				oftenShadowColorList.add(DrawCommon.getColorByString(oftenShadowColor7));
			}
			// 获取常用阴影第八个颜色
			String oftenShadowColor8 = properties
					.getProperty("oftenShadowColor8");
			if(oftenShadowColor8!=null){
				oftenShadowColorList.add(DrawCommon.getColorByString(oftenShadowColor8));
			}
			// 把List 的值 赋给说shadowColorList
			systemOftenColorData.setShadowColorList(oftenShadowColorList);
			
			

			List<Color> oftenLineColorList = new ArrayList<Color>();
			// 获取常用线条第一个颜色
			String oftenLineColor1 = properties.getProperty("oftenLineColor1");
			if(oftenLineColor1!=null){
				oftenLineColorList.add(DrawCommon.getColorByString(oftenLineColor1));
			}
			// 获取常用线条第二个颜色
			String oftenLineColor2 = properties.getProperty("oftenLineColor2");
			if(oftenLineColor2!=null){
				oftenLineColorList.add(DrawCommon.getColorByString(oftenLineColor2));
			}
			// 获取常用线条第三个颜色
			String oftenLineColor3 = properties.getProperty("oftenLineColor3");
			if(oftenLineColor3!=null){
				oftenLineColorList.add(DrawCommon.getColorByString(oftenLineColor3));
			}
			// 获取常用线条第四个颜色
			String oftenLineColor4 = properties.getProperty("oftenLineColor4");
			if(oftenLineColor4!=null){
				oftenLineColorList.add(DrawCommon.getColorByString(oftenLineColor4));
			}
			// 获取常用线条第五个颜色
			String oftenLineColor5 = properties.getProperty("oftenLineColor5");
			if(oftenLineColor5!=null){
				oftenLineColorList.add(DrawCommon.getColorByString(oftenLineColor5));
			}
			// 获取常用线条第六个颜色
			String oftenLineColor6 = properties.getProperty("oftenLineColor6");
			if(oftenLineColor6!=null){
				oftenLineColorList.add(DrawCommon.getColorByString(oftenLineColor6));
			}
			// 获取常用线条第七个颜色
			String oftenLineColor7 = properties.getProperty("oftenLineColor7");
			if(oftenLineColor7!=null){
				oftenLineColorList.add(DrawCommon.getColorByString(oftenLineColor7));
			}
			// 获取常用线条第八个颜色
			String oftenLineColor8 = properties.getProperty("oftenLineColor8");
			if(oftenLineColor8!=null){
				oftenLineColorList.add(DrawCommon.getColorByString(oftenLineColor8));
			}
			// 把List 的值 赋给lineColorList
			systemOftenColorData.setLineColorList(oftenLineColorList);
			
			

			List<Color> oftenFontColorList = new ArrayList<Color>();
			// 获取常用字体第一个颜色
			String oftenFontColor1 = properties.getProperty("oftenFontColor1");
			if(oftenFontColor1!=null){
				oftenFontColorList.add(DrawCommon.getColorByString(oftenFontColor1));
			}
			// 获取常用字体第二个颜色
			String oftenFontColor2 = properties.getProperty("oftenFontColor2");
			if(oftenFontColor2!=null){
				oftenFontColorList.add(DrawCommon.getColorByString(oftenFontColor2));
			}
			// 获取常用字体第三个颜色
			String oftenFontColor3 = properties.getProperty("oftenFontColor3");
			if(oftenFontColor3!=null){
				oftenFontColorList.add(DrawCommon.getColorByString(oftenFontColor3));
			}
			// 获取常用字体第四个颜色
			String oftenFontColor4 = properties.getProperty("oftenFontColor4");
			if(oftenFontColor4!=null){
				oftenFontColorList.add(DrawCommon.getColorByString(oftenFontColor4));
			}
			
			// 获取常用字体第五个颜色
			String oftenFontColor5 = properties.getProperty("oftenFontColor5");
			if(oftenFontColor5!=null){
				oftenFontColorList.add(DrawCommon.getColorByString(oftenFontColor5));
			}
			// 获取常用字体第六个颜色
			String oftenFontColor6 = properties.getProperty("oftenFontColor6");
			if(oftenFontColor6!=null){
				oftenFontColorList.add(DrawCommon.getColorByString(oftenFontColor6));
			}
			// 获取常用字体第七个颜色
			String oftenFontColor7 = properties.getProperty("oftenFontColor7");
			if(oftenFontColor7!=null){
				oftenFontColorList.add(DrawCommon.getColorByString(oftenFontColor7));
			}
			// 获取常用字体第八个颜色
			String oftenFontColor8 = properties.getProperty("oftenFontColor8");
			if(oftenFontColor8!=null){
				oftenFontColorList.add(DrawCommon.getColorByString(oftenFontColor8));
			}
			// 把List 的值 赋给FontColorList
			systemOftenColorData.setFontColorList(oftenFontColorList);

		}
	}

	/**
	 * 
	 * 写入常用颜色
	 * 
	 * @return boolean 写入成功或无写入返回true；写入失败返回false
	 */
	static boolean exportColorToPropertyFile() {
		// 待写入的properties对象
		Properties properties = new Properties();

		// 常用颜色数据对象
		JecnSystemOftenColorData systemOftenColorData = readOftenColorData();
		if (!systemOftenColorData.isUpdateFlag()) {// 无更新
			return true;
		}

		// 常用填充颜色的List
		for (int i = 1; i <= systemOftenColorData.getFillColorList().size(); i++) {
			// 获取常用填充颜色
			Color color = systemOftenColorData.getFillColorList().get(i-1);
			properties.setProperty("oftenFillColor" + i, DrawCommon
					.coverColorToString(color));
		}

		// 常用阴影颜色List
		for (int i = 1; i <= systemOftenColorData.getShadowColorList().size(); i++) {
			// 获取常用填充颜色
			Color color = systemOftenColorData.getShadowColorList().get(i-1);
			properties.setProperty("oftenShadowColor" + i, DrawCommon
					.coverColorToString(color));
		}

		// 常用线条颜色List
		for (int i = 1; i <= systemOftenColorData.getLineColorList().size(); i++) {
			// 获取常用填充颜色
			Color color = systemOftenColorData.getLineColorList().get(i-1);
			properties.setProperty("oftenLineColor" + i, DrawCommon
					.coverColorToString(color));
		}

		// 常用字体颜色List
		for (int i = 1; i <= systemOftenColorData.getFontColorList().size(); i++) {
			// 获取常用填充颜色
			Color color = systemOftenColorData.getFontColorList().get(i-1);
			properties.setProperty("oftenFontColor" + i, DrawCommon
					.coverColorToString(color));
		}

		// 执行写入操作
		boolean ret = JecnFileUtil.writeProperties(getPath(), properties, null);
		return ret;
	}
}
