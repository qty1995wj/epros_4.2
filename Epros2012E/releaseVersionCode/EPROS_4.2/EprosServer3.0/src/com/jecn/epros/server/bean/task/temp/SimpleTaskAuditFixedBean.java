package com.jecn.epros.server.bean.task.temp;

/**
 * 任务审批变动
 * 
 * @author zhangxh
 * 
 * 
 */
public class SimpleTaskAuditFixedBean {
	/** 关联ID */
	private Long refId;
	/** 密级 */
	private Long isPublic;
	/** 文件类别 */
	private Long typeId;
	/** 查阅权限ID集合 */
	private String accessIds;
	/** 查阅权限名称 */
	private String accessNames;
	
	/**0:查阅权限无变动；1：有变动*/
	private int accessType;

	public int getAccessType() {
		return accessType;
	}

	public void setAccessType(int accessType) {
		this.accessType = accessType;
	}

	public Long getRefId() {
		return refId;
	}

	public void setRefId(Long refId) {
		this.refId = refId;
	}

	public Long getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Long isPublic) {
		this.isPublic = isPublic;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getAccessIds() {
		return accessIds;
	}

	public void setAccessIds(String accessIds) {
		this.accessIds = accessIds;
	}

	public String getAccessNames() {
		return accessNames;
	}

	public void setAccessNames(String accessNames) {
		this.accessNames = accessNames;
	}
}
