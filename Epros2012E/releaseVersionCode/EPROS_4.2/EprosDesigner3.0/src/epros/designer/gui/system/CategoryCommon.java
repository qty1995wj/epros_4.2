package epros.designer.gui.system;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.ConfigCommon;
import epros.designer.gui.common.ConfigCommon.ConfigCommonEnum;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnResourceUtil;

/**
 * 流程类别
 * 
 * @author zhangchen
 * 
 */
public class CategoryCommon {
	private static Logger log = Logger.getLogger(CategoryCommon.class);
	/** 流程类别Lab */
	private JLabel isCategoryLab = new JLabel(JecnProperties.getValue("categoriesC"));
	/** 流程类别ComboBox */
	private JComboBox isCategoryCombox = new JComboBox(new String[] {});
	/** 流程类别ID */
	private Long typeId = null;
	protected List<ProceeRuleTypeBean> listTypeBean = new ArrayList<ProceeRuleTypeBean>();
	private Component component;

	public CategoryCommon(int rows, JecnPanel infoPanel, Insets insets, Long typeId, Component jecnDialog,
			String labName) {
		this(rows, infoPanel, insets, typeId, jecnDialog);
		isCategoryLab.setText(labName);
	}

	public CategoryCommon(int rows, JecnPanel infoPanel, Insets insets, Long typeId, Component jecnDialog) {
		initComponent(rows, infoPanel, insets, typeId, jecnDialog);
	}

	private void initComponent(int rows, JecnPanel infoPanel, Insets insets, Long typeId, Component jecnDialog) {
		this.component = jecnDialog;
		Dimension dimension = new Dimension(300, 20);
		isCategoryCombox.setPreferredSize(dimension);
		isCategoryCombox.setMaximumSize(dimension);
		isCategoryCombox.setMinimumSize(dimension);
		this.typeId = typeId;
		addComboxItem();
		// 流程类别
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(isCategoryLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(isCategoryCombox, c);
	}

	public CategoryCommon(int rows, JecnPanel infoPanel, Insets insets, Long typeId, Component jecnDialog,
			boolean isRequest, String labName) {
		this(rows, infoPanel, insets, typeId, jecnDialog, isRequest);
		isCategoryLab.setText(labName);
	}

	public CategoryCommon(int rows, JecnPanel infoPanel, Insets insets, Long typeId, Component jecnDialog,
			boolean isRequest) {
		initComponent(rows, infoPanel, insets, typeId, jecnDialog);

		if (isRequest) {
			GridBagConstraints c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
					GridBagConstraints.NONE, insets, 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}
	}

	/**
	 * 初始化
	 */
	private void addComboxItem() {
		// 动态读取流程类型
		try {
			listTypeBean = ConnectionPool.getProcessRuleTypeAction().getFlowAttributeType();
			// 清空流程类别
			isCategoryCombox.removeAllItems();
			isCategoryCombox.addItem("");

			boolean mengNiuOperaType = JecnConfigTool.isMengNiuOperaType();
			// 获取本地默认类别
			String ruleTypeId = ConfigCommon.INSTANTCE.getConfigProperties(ConfigCommonEnum.typeId);

			for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
				// 本流程对应的流程类型ID 与流程类别中的ID相等，设置该流程类别选中
				String selectItem = proceeRuleTypeBean.getTypeName(JecnResourceUtil.getLocale() == Locale.CHINESE ? 0
						: 1);
				// 将流程类型显示到JCombox中
				isCategoryCombox.addItem(selectItem);
				if (typeId != null && typeId.equals(proceeRuleTypeBean.getTypeId())) {
					isCategoryCombox.setSelectedItem(selectItem);
				} else if (typeId == null && String.valueOf(proceeRuleTypeBean.getTypeId()).equals(ruleTypeId)
						&& mengNiuOperaType) {
					isCategoryCombox.setSelectedItem(selectItem);
				}
			}
		} catch (Exception e1) {
			JecnOptionPane.showMessageDialog(component, JecnProperties.getValue("obtainFlowTypeError"));
			log.error("CategoryCommon addComboxItem is error", e1);
		}
	}

	/**
	 * 类别是否更新
	 * 
	 * @return
	 */
	public boolean isUpdate() {
		Long newTypeId = getTypeResultId();
		if (newTypeId != null && typeId != null) {
			if (!newTypeId.equals(typeId)) {
				return true;
			}
		}
		if ((newTypeId == null && typeId != null) || (newTypeId != null && typeId == null)) {
			return true;
		}
		return false;
	}

	/**
	 * 返回结果
	 * 
	 * @return
	 */
	public Long getTypeResultId() {
		String typeName = (String) isCategoryCombox.getSelectedItem();
		for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
			if (JecnUtil.nullToEmpty(
					proceeRuleTypeBean.getTypeName(JecnResourceUtil.getLocale() == Locale.CHINESE ? 0 : 1)).equals(
					typeName)) {
				// 获取本地默认类别
				ConfigCommon.INSTANTCE.writeCommonConfigProperties(proceeRuleTypeBean.getTypeId().toString(),
						ConfigCommonEnum.typeId);
				return proceeRuleTypeBean.getTypeId();
			}
		}
		return null;
	}

	public void setVisible() {
		boolean isVisible = true;
		if ("0".equals(JecnConfigTool.getConfigItemValue(ConfigItemPartMapMark.ruleType))) {
			isVisible = false;
		}
		setVisible(isVisible);
	}

	public void setVisible(boolean isVisible) {
		isCategoryLab.setVisible(isVisible);
		isCategoryCombox.setVisible(isVisible);
	}

}
