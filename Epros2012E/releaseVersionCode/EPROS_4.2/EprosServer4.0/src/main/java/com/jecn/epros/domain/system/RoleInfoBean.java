package com.jecn.epros.domain.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.Resource;
import com.jecn.epros.domain.org.OrgBaseInfoBean;
import com.jecn.epros.service.TreeNodeUtils;

import java.util.List;

/**
 * @author zhr 2018-8-14
 * @description：设计权限分布图
 */
public class RoleInfoBean {
    //人员ID
    private Long userId;
    //人员名称
    private String userName;
    //所属部门
    private List<OrgBaseInfoBean> orgBean;
    //角色类型
    private List<RoleWebInfoBean> roleInfo;

    @JsonIgnore
    public LinkResource getUserLink() {
        return new LinkResource(userId, userName, LinkResource.ResourceType.PERSON, TreeNodeUtils.NodeType.auth);
    }


    public List<OrgBaseInfoBean> getOrgBean() {
        return orgBean;
    }

    public void setOrgBean(List<OrgBaseInfoBean> orgBean) {
        this.orgBean = orgBean;
    }

    public List<RoleWebInfoBean> getRoleInfo() {
        return roleInfo;
    }

    public void setRoleInfo(List<RoleWebInfoBean> roleInfo) {
        this.roleInfo = roleInfo;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
