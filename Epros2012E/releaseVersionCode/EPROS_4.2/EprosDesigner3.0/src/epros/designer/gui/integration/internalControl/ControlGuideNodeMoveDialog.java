package epros.designer.gui.integration.internalControl;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 内控指引知识库节点移动
 * 
 * @author Administrator
 * 
 */
public class ControlGuideNodeMoveDialog extends JecnMoveChooseDialog {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger
			.getLogger(ControlGuideNodeMoveDialog.class);

	/**
	 * 内控指引知识库节点移动构造方法
	 * 
	 * @param listMoveNodes
	 *            将要移动的节点集合
	 * @param moveTree
	 *            树
	 */
	public ControlGuideNodeMoveDialog(List<JecnTreeNode> listMoveNodes,
			JecnTree moveTree) {
		super(listMoveNodes, moveTree);
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyControlGuideMoveTree(getListIds());
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		return title;
	}

	@Override
	public boolean savaData(List<Long> ids, Long pid) {
//		if ("0".equals(pid.toString())) {
//			JecnOptionPane.showMessageDialog(this, JecnProperties
//					.getValue("notmovefileRoot"));
//			return false;
//		}
		try {
			ConnectionPool.getControlGuideAction().moveSortControlGuide(ids,
					pid, JecnConstants.getUserId());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(),
					JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("ControlGuideNodeMoveDialog savaData is error", e);
			return false;
		}
	}

	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			List<JecnTreeBean> list = ConnectionPool.getControlGuideAction()
					.getChildJecnControlGuides(pid, JecnConstants.projectId);
			for (JecnTreeNode node : listMoveNodes) {
				for (JecnTreeBean tr : list) {
					if (node.getJecnTreeBean().getTreeNodeType() == tr
							.getTreeNodeType()
							&& node.getJecnTreeBean().getName().equals(
									tr.getName())) {
						JecnOptionPane.showMessageDialog(this, JecnProperties
								.getValue("aimNodeHaved")
								+ "\"" + tr.getName() + "\"");
						return true;
					}
				}
			}
		} catch (Exception e) {
			log.error("ControlGuideNodeMoveDialog validateName is error", e);
			return false;
		}
		return false;
	}

}
