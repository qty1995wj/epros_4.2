package com.jecn.epros.server.bean.file;

import java.io.Serializable;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class FileUseInfoBean implements Serializable{
	private String   id;
	private String   name;
	private TreeNodeType   type;//0是流程地图，1是流程,2是组织,23是标准,4是制度
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TreeNodeType getType() {
		return type;
	}
	public void setType(TreeNodeType type) {
		this.type = type;
	}
}
