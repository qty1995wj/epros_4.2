package epros.designer.gui.popedom.role.choose;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
/**
 * @author yxw 2012-6-14
 * @description：
 */
public class HighEfficiencyRoleManageProcessTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(HighEfficiencyRoleManageProcessTree.class);
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RoleManageProcessTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.processRoot,JecnProperties.getValue("process"));
		try {
			list = JecnDesignerCommon.getRoleManageProcessTreeBeanList(0L,false);
			JecnTreeCommon.addNLevelNodes(list, rootNode);
		} catch (Exception e) {
			log.error("HighEfficiencyRoleManageProcessTree getTreeModel is error",e);
		}
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		
	}

}
