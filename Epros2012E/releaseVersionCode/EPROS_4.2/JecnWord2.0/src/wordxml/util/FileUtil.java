package wordxml.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.SAXReader;

public class FileUtil {
	private static final Log log = LogFactory.getLog(FileUtil.class);

	/**
	 * 
	 * 获取Properties文件内容
	 * 
	 * @param pFilePath
	 *            String Properties文件相对于项目路径
	 * @return Properties Properties对象或NULL
	 */
	public static Properties readProperties(String allPath) {
		// 配置文件路径
		if (Tools.isBlank(allPath)) {
			log.error("文件路径为空。文件路径=" + allPath);
			return null;
		}

		File file = new File(allPath);
		if (!file.exists() || !file.isFile()) {// 文件不存在或者不是文件
			log.error("此路径不是文件  。文件路径=" + allPath);
			return null;
		}

		// 属性对象
		Properties properties = new Properties();
		FileInputStream inputStream = null;
		try {
			// 读取文件
			inputStream = new FileInputStream(allPath);
			properties.load(inputStream);
			log.info("读取结束，文件路径：" + allPath);
		} catch (FileNotFoundException e) {
			log.error("指定文件找不到，文件路径：" + allPath, e);
			return null;
		} catch (IOException e) {
			log.error("读取文件出错，文件路径：" + allPath, e);
			return null;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("关闭FileInputStream流出错，文件路径：" + allPath, e);
					return null;
				}
			}
		}
		return properties;
	}

	/**
	 * 获取 Document
	 * 
	 * @param allPath
	 * @return
	 */
	public static Document getDocumentFile(final String allPath) {
		try {
			if (allPath == null || "".equals(allPath.trim())) {
				return null;
			}
			SAXReader reader = new SAXReader();
			File file = new File(allPath);
			if (file == null || !file.exists()) {
				log.info("Tools类getDocumentFile方法：找不到指定的文件。path=" + allPath);
				return null;
			}
			Document doc = reader.read(file);
			doc.normalize();
			return doc;
		} catch (Exception e) {
			log.error("获取document文档异常,方法名称：getDocumentFile", e);
			return null;

		}
	}

	/**
	 * 获取classPath中document.xml文件
	 * 
	 * @return
	 */
	public static Document getClassPathDocumentFile() {
		log.info("获取classPath模版文件");
		Document doc = null;
		InputStream in = FileUtil.class.getClassLoader().getResourceAsStream(
				"document.xml");
		BufferedReader buffReader=null;
		try {
			buffReader = new BufferedReader(
					new InputStreamReader(in,"utf-8"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		StringBuffer sb = new StringBuffer();
		String line = "";
		try {
			while ((line = buffReader.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
			doc = DocumentHelper.parseText(sb.toString());
		} catch (IOException e) {
			log.error("读取document.xml异常", e);
		} catch (DocumentException e) {
			log.error("解析document.xml构建dom异常", e);
		} catch (Exception e) {
			log.error("获取classPath模版文件异常", e);
		} finally {
			if (buffReader != null) {
				try {
					buffReader.close();
				} catch (IOException e) {
					log.error("关闭流异常", e);
				}
			}
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					log.error("关闭流异常", e);
				}
			}
		}
		log.info("获取classPath模版文件完成");
		return doc;
	}

	/**
	 * 写文件
	 * 
	 * @param sb
	 * @param filePath
	 */
	public static void writeFile(String sb, String filePath) {
		if (sb == null) {
			log.info("Tools类writeFile方法：sb is  null");
			return;
		}
		if (filePath == null) {
			log.info("Tools类writeFile方法：filePath is  null");
			return;
		}
		BufferedWriter writer = null;
		try { 
			writer = new BufferedWriter(new FileWriter(filePath));
			writer.write(sb);
			writer.flush();
		} catch (Exception e) {
			log.error("Tools类writeFile方法异常", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					log.error("Tools类writeFile关闭流方法异常", e);
				}
			}
		}
	}

	public static void main(String[] args) {
		String path = "E:\\MyEclipse\\jecn\\JecnWord2.0\\bin\\document.xml";
		Document doc = FileUtil.getDocumentFile(path);
	}
}
