package epros.designer.gui.task.config;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.task.TaskTemplet;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 任务编辑对话框
 * 
 * @author ZHOUXY
 * 
 */
public abstract class TaskConfigDialog extends JecnDialog {

	private static final long serialVersionUID = -8171984269044300497L;

	protected static Logger log = Logger.getLogger(TaskConfigDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(510, 560);// 510, 660
	/** center面板 */
	private JecnPanel centerPanel = new JecnPanel(510, 500);// /510, 505
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel(500, 30);// 500, 30
	/** 顶部面板 */
	private JecnPanel templetPanel = new JecnPanel(500, 30);// 500, 30

	/** 模板选择 */
	private JLabel templetSelect = new JLabel(JecnProperties.getValue("modeSelect"));
	/** 下拉框选择 **/
	private JComboBox templetComboBox = new JComboBox();
	/** 选择按钮 **/
	private JButton selectBtn = null;

	/** 验证提示 */
	private JLabel verfyLab = new JLabel();
	/** 确定 */
	protected JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	protected TaskConfigEditPanel taskEditPanel;

	/** 面板宽 */
	private int dialogWidth = 500;
	/** 面板高 */
	private int dialogHeigh = 500;

	protected int taskType = -1;
	protected String curSelectTempletId;
	private boolean isEidt;

	/** 任务模板下拉框数据 包含空选项 **/
	private List<TaskTemplet> templets = new ArrayList<TaskTemplet>();
	/** 选中的模板的任务配置项 **/
	private List<TaskConfigItem> configs = new ArrayList<TaskConfigItem>();

	public TaskConfigDialog(String templetId, int taskType, boolean isEdit) {
		this.curSelectTempletId = templetId;
		this.taskType = taskType;
		this.setTitle(JecnProperties.getValue("taskModelSelect"));
		this.setSize(this.getWidthMax(), dialogHeigh);
		this.setResizable(true);
		this.setModal(true);
		this.isEidt = isEdit;

		templetPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		initComponent();
		initData();
		initLayout();
		initAction();
	}

	private void initComponent() {

		verfyLab.setForeground(Color.RED);
		selectBtn = new JButton(getSelectButtonName());
		// 任务中间面板
		taskEditPanel = new TaskConfigEditPanel(this, isEidt);
	}

	public void initData() {

		try {
			templetComboBox.removeAllItems();
			templets.clear();
			// 空
			TaskTemplet emptyTemplet = new TaskTemplet();
			emptyTemplet.setName("    ");
			templets.add(emptyTemplet);
			// 该类型的模板 查询数据库
			templets.addAll(ConnectionPool.getTaskRecordAction().listTaskTempletsByType(taskType));

			// 由于重新添加下拉框节点会触发下拉框的选中action所以需要存放当前选中的模板id到临时值，之后再恢复值
			String tempCurSelectTempletId = this.curSelectTempletId;

			for (TaskTemplet templet : templets) {
				templetComboBox.addItem(templet.getName(JecnResourceUtil.getLocale() == Locale.CHINESE ? 0 : 1));
			}

			this.curSelectTempletId = tempCurSelectTempletId;
			int selectIndex = getSelectIndex(templets);
			if (selectIndex == 0) {
				buttonEditable(false);
			} else {
				buttonEditable(true);
			}
			updateTable(selectIndex);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}

	}

	private void updateTable(int selectIndex) throws Exception {
		if (selectIndex == 0) {
			// 清空table
			this.configs.clear();
		} else {
			templetComboBox.setSelectedIndex(selectIndex);
			TaskTemplet taskTemplet = templets.get(selectIndex);
			this.configs = ConnectionPool.getTaskRecordAction().listTaskConfigItemByTempletId(taskTemplet.getId());
		}
		taskEditPanel.initData();
	}

	/**
	 * @author yxw 2012-6-28
	 * @description:布局
	 */
	private void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(3, 3, 3, 3);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(
				3, 3, 0, 3), 0, 0);
		mainPanel.add(templetPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(3, 3, 0, 3), 0, 0);
		mainPanel.add(centerPanel, c);

		templetPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
				3, 3, 0, 3), 0, 0);
		templetPanel.add(templetSelect, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(3, 3, 0, 3), 0, 0);
		templetPanel.add(templetComboBox, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
				3, 3, 0, 3), 0, 0);
		templetPanel.add(selectBtn, c);

		centerPanel.setLayout(new GridLayout());
		centerPanel.add(taskEditPanel);
		c = new GridBagConstraints(0, 2, 3, 1, 1.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		this.setResizable(false);
		// 模态
		this.setModal(true);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setLocationRelativeTo(null);
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
	}

	private void initAction() {
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				saveAction();
			}
		});

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		selectBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				selectAction();
				;

			}
		});

		templetComboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {

					int index = templetComboBox.getSelectedIndex();
					try {
						if (index == 0) {
							curSelectTempletId = null;
						} else {
							TaskTemplet taskTemplet = templets.get(index);
							curSelectTempletId = taskTemplet.getId();
						}
						if (index == 0) {
							buttonEditable(false);
						} else {
							buttonEditable(true);
						}
						updateTable(index);
					} catch (Exception e1) {
						log.error(e);
					}
				}
			}

		});

	}

	public void buttonEditable(boolean editable) {

	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelButPerformed() {
		// 关闭窗体
		this.dispose();
	}

	protected void updateBtnAndInfo(String value) {
		verfyLab.setText(value);
	}

	abstract protected String getSelectButtonName();

	/**
	 * 需要选中的下拉框的的index
	 * 
	 * @param templets
	 * @param selectTempletId
	 * @return
	 */
	private int getSelectIndex(List<TaskTemplet> templets) {
		int index = 0;
		int selectIndex = 0;
		for (TaskTemplet taskTemplet : templets) {
			if (taskTemplet.getId() != null && taskTemplet.getId().equals(this.curSelectTempletId)) {
				selectIndex = index;
				break;
			}
			index++;
		}
		return selectIndex;

	}

	abstract protected void saveAction();

	abstract protected void selectAction();

	public JecnPanel getMainPanel() {
		return mainPanel;
	}

	public void setMainPanel(JecnPanel mainPanel) {
		this.mainPanel = mainPanel;
	}

	public JecnPanel getCenterPanel() {
		return centerPanel;
	}

	public void setCenterPanel(JecnPanel centerPanel) {
		this.centerPanel = centerPanel;
	}

	public JecnPanel getButtonPanel() {
		return buttonPanel;
	}

	public void setButtonPanel(JecnPanel buttonPanel) {
		this.buttonPanel = buttonPanel;
	}

	public JecnPanel getTempletPanel() {
		return templetPanel;
	}

	public void setTempletPanel(JecnPanel templetPanel) {
		this.templetPanel = templetPanel;
	}

	public JLabel getTempletSelect() {
		return templetSelect;
	}

	public void setTempletSelect(JLabel templetSelect) {
		this.templetSelect = templetSelect;
	}

	public JComboBox getTempletComboBox() {
		return templetComboBox;
	}

	public void setTempletComboBox(JComboBox templetComboBox) {
		this.templetComboBox = templetComboBox;
	}

	public JButton getSelectBtn() {
		return selectBtn;
	}

	public void setSelectBtn(JButton selectBtn) {
		this.selectBtn = selectBtn;
	}

	public JLabel getVerfyLab() {
		return verfyLab;
	}

	public void setVerfyLab(JLabel verfyLab) {
		this.verfyLab = verfyLab;
	}

	public JButton getOkBut() {
		return okBut;
	}

	public void setOkBut(JButton okBut) {
		this.okBut = okBut;
	}

	public JButton getCancelBut() {
		return cancelBut;
	}

	public void setCancelBut(JButton cancelBut) {
		this.cancelBut = cancelBut;
	}

	public TaskConfigEditPanel getTaskEditPanel() {
		return taskEditPanel;
	}

	public void setTaskEditPanel(TaskConfigEditPanel taskEditPanel) {
		this.taskEditPanel = taskEditPanel;
	}

	public int getDialogWidth() {
		return dialogWidth;
	}

	public void setDialogWidth(int dialogWidth) {
		this.dialogWidth = dialogWidth;
	}

	public int getDialogHeigh() {
		return dialogHeigh;
	}

	public void setDialogHeigh(int dialogHeigh) {
		this.dialogHeigh = dialogHeigh;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public String getCurSelectTempletId() {
		return curSelectTempletId;
	}

	public void setCurSelectTempletId(String curSelectTempletId) {
		this.curSelectTempletId = curSelectTempletId;
	}

	public boolean isEidt() {
		return isEidt;
	}

	public void setEidt(boolean isEidt) {
		this.isEidt = isEidt;
	}

	public List<TaskTemplet> getTemplets() {
		return templets;
	}

	public void setTemplets(List<TaskTemplet> templets) {
		this.templets = templets;
	}

	public List<TaskConfigItem> getConfigs() {
		return configs;
	}

	public void setConfigs(List<TaskConfigItem> configs) {
		this.configs = configs;
	}

}
