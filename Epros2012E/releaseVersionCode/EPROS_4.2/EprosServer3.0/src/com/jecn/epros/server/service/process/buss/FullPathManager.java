package com.jecn.epros.server.service.process.buss;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.util.JecnUtil;

public class FullPathManager {

	// 0 name 1 t_path
	private Map<Long, String[]> idToNameAndPath = new HashMap<Long, String[]>();
	private Set<Long> ids = new HashSet<Long>();
	private IBaseDao baseDao;
	/**
	 * 0 流程 1文件 2制度 3标准 4风险 5组织 6岗位 7岗位组
	 */
	private int type;

	public FullPathManager(IBaseDao baseDao, int type) {
		this.baseDao = baseDao;
		this.type = type;
	}

	public void add(Long id) {
		if (id == null) {
			return;
		}
		ids.add(id);
	}

	public void add(Collection<Long> ids) {
		if (JecnUtil.isEmpty(ids)) {
			return;
		}
		this.ids.addAll(ids);
	}

	public void generate() {
		if (JecnUtil.isEmpty(this.ids)) {
			return;
		}
		if (this.type == 6) {
			FullPathManager orgManager = new FullPathManager(this.baseDao, 5);
			Map<Long, Long> posToOrg = new HashMap<Long, Long>();
			List<Object[]> orgList = getOrgs(this.ids);
			for (Object[] obj : orgList) {
				Long posId = Long.valueOf(obj[0].toString());
				if (obj[2] != null) {
					Long orgId = Long.valueOf(obj[2].toString());
					posToOrg.put(posId, orgId);
					orgManager.add(orgId);
				}
				this.idToNameAndPath.put(posId, new String[] { obj[1].toString() });
			}
			orgManager.generate();
			for (Map.Entry<Long, String[]> entry : this.idToNameAndPath.entrySet()) {
				Long posId = entry.getKey();
				Long orgId = posToOrg.get(posId);
				if (orgId == null) {
					continue;
				}
				String fullPath = orgManager.getFullPath(orgId);
				if (StringUtils.isNotBlank(fullPath)) {
					entry.getValue()[0] = fullPath + "/" + entry.getValue()[0];
				}
			}
		} else {
			Map<Long, String[]> m = getIdNamTpath(this.ids);
			this.idToNameAndPath.putAll(m);
			Set<Long> pSet = new HashSet<Long>();
			for (Map.Entry<Long, String[]> entry : m.entrySet()) {
				String tPath = entry.getValue()[1];
				if (StringUtils.isNotBlank(tPath)) {
					String[] arr = tPath.split("-");
					for (String pIdStr : arr) {
						if (StringUtils.isNotBlank(pIdStr)) {
							pSet.add(Long.valueOf(pIdStr));
						}
					}
				}
			}

			if (JecnUtil.isNotEmpty(pSet)) {
				StringBuilder b = new StringBuilder();
				Map<Long, String[]> idNamTpath = getIdNamTpath(pSet);
				for (Entry<Long, String[]> entry : this.idToNameAndPath.entrySet()) {
					b.setLength(0);
					String tPath = entry.getValue()[1];
					if (StringUtils.isNotBlank(tPath)) {
						String[] arr = tPath.split("-");
						for (String pIdStr : arr) {
							if (StringUtils.isNotBlank(pIdStr)) {
								String[] nameTath = idNamTpath.get(Long.valueOf(pIdStr));
								if (nameTath != null) {
									b.append(nameTath[0]);
									b.append("/");
								}
							}
						}
						String name = b.toString();
						if (StringUtils.isNotBlank(name)) {
							if (name.endsWith("/")) {
								name = name.substring(0, name.length() - 1);
							}
							entry.getValue()[0] = name;
						}
					}
				}
			}
		}
	}

	private List<Object[]> getOrgs(Set<Long> posIds) {
		if (JecnUtil.isEmpty(posIds)) {
			return new ArrayList<Object[]>();
		}
		String sql = "select figure_id,figure_text,org_id from jecn_flow_org_image where figure_id in "
				+ JecnCommonSql.getIdsSet(posIds);
		return this.baseDao.listNativeSql(sql);
	}

	public String getFullPath(Long id) {
		String[] nameTpath = idToNameAndPath.get(id);
		if (nameTpath != null) {
			return nameTpath[0];
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	private Map<Long, String[]> getIdNamTpath(Set<Long> ids) {
		Map<Long, String[]> result = new HashMap<Long, String[]>();
		String sql = getSqlByType();
		List<String> sqls = JecnCommonSql.getListSqls(sql, new ArrayList<Long>(ids));
		for (String s : sqls) {
			List<Object[]> objs = baseDao.listNativeSql(s);
			for (Object[] obj : objs) {
				Long id = Long.valueOf(obj[0].toString());
				String name = obj[1].toString();
				String tPath = JecnUtil.nullToEmpty(obj[2]);
				result.put(id, new String[] { name, tPath });
			}
		}
		return result;
	}

	private String getSqlByType() {
		switch (this.type) {
		case 0:
			return "select flow_id,flow_name,t_path from jecn_flow_structure_t where flow_id in ";
		case 1:
			return "select file_id,file_name,t_path from jecn_file_t where file_id in ";
		case 5:
			return "select org_id,org_name,t_path from jecn_flow_org where org_id in ";
		case 7:
			return "select id,name,t_path from jecn_position_group where id in ";
		}

		return "";
	}

}
