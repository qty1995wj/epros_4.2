package com.jecn.epros.server.dao.popedom;

import java.util.Collection;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.IBaseDao;

public interface IFlowOrgImageDao extends IBaseDao<JecnFlowOrgImage, Long> {
	/**
	 * 获得人员在项目下参与的角色
	 * 
	 * @param peopleId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRoleNOGroupByList(Long peopleId, Long projectId) throws Exception;

	/**
	 * 获得人员在项目下参与的角色(岗位组)
	 * 
	 * @param peopleId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRoleGroupByList(Long peopleId, Long projectId) throws Exception;

	public List<JecnUser> listPosUsers(Collection<Long> ids) throws Exception;

	public List<JecnUser> listGroupUsers(Collection<Long> ids);
}
