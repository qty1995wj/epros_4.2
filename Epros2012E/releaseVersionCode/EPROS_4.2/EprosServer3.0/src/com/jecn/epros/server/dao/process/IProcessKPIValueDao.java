package com.jecn.epros.server.dao.process;

import java.util.Date;
import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.common.IBaseDao;

public interface IProcessKPIValueDao extends IBaseDao<JecnFlowKpi, Long> {
	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得KPI的值
	 * @param kpiId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpi> getJecnFlowKpiListBy(Long kpiId) throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得KPI的值
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpi> getJecnFlowKpiListByFlowId(Long flowId)
			throws Exception;

	public JecnFlowKpi getFlowKPIValue(long kpiAndId, String kpiVlaue)
			throws Exception;

	/**
	 * 根据流程ID获取KPI集合
	 * 
	 * @param flowId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpiName> getListJecnFlowKpiName(Long flowId)
			throws Exception;

	/**
	 * 获取单个KPI
	 */
	public JecnFlowKpiName getJecnFlowKpiName(Long kpiAndId) throws Exception;

	/**
	 * 时间段查询KPI的值
	 * 
	 * @param kpiId
	 *            KPI从表之间ID
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @return List<JecnFlowKpi>
	 * @throws Exception
	 */
	public List<JecnFlowKpi> findJecnFlowKpiList(Long kpiId, Date startTime,
			Date endTime) throws Exception;
}
