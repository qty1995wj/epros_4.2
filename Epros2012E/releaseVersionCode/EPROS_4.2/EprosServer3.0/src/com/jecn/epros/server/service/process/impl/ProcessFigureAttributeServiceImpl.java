package com.jecn.epros.server.service.process.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.process.JecnFlowFigure;
import com.jecn.epros.server.bean.process.JecnFlowFigureNow;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.dao.process.IProcessFigureAttributeDao;
import com.jecn.epros.server.service.process.IProcessFigureAttributeService;

/**
 * @author yxw 2012-7-27
 * @description：
 */
@Transactional(rollbackFor = Exception.class)
public class ProcessFigureAttributeServiceImpl extends
		AbsBaseService<JecnFlowFigureNow, Long> implements
		IProcessFigureAttributeService {
	private final static Log log = LogFactory
			.getLog(ProcessFigureAttributeServiceImpl.class);

	private IProcessFigureAttributeDao processFigureAttributeDao;

	public void setProcessFigureAttributeDao(
			IProcessFigureAttributeDao processFigureAttributeDao) {
		this.processFigureAttributeDao = processFigureAttributeDao;
		this.baseDao = processFigureAttributeDao;
	}

	@Override
	public List<JecnFlowFigureNow> getAll() throws Exception {
		return processFigureAttributeDao.listAll();
	}

	@Override
	public void updateAll(List<JecnFlowFigureNow> jecnFlowFigureNowList)
			throws Exception {
		try {
			if (jecnFlowFigureNowList != null) {
				for (JecnFlowFigureNow jecnFlowFigureNow : jecnFlowFigureNowList) {
//					processFigureAttributeDao.save(jecnFlowFigureNow);
					processFigureAttributeDao.update(jecnFlowFigureNow);
				}
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	@Override
	public List<JecnFlowFigure> getAllDefault() throws Exception {
		try {
			String hql = "from JecnFlowFigure";
			return processFigureAttributeDao.listHql(hql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

}
