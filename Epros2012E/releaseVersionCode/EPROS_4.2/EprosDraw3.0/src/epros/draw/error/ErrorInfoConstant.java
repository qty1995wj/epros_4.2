package epros.draw.error;

/**
 * 
 * 提示错误信息
 * 
 * @author ZHOUXY
 * 
 */
public class ErrorInfoConstant {
	public final static String PARA_OBJ_NULL = "参数为null或空值";
	public final static String PARA_OBJ_FAIL = "参数不正确";
	public final static String PARA_OBJ_FIAL = "参数不合法";
	public final static String FLOW_ELEMENT_FILL_FIAL_XML = "读取流程元素配置文件时，填充类型与颜色1和颜色2不一致";
	public final static String FLOW_ELEMENT_SHADOW_FIAL_XML = "读取流程元素配置文件时，有阴影情况下阴影颜色为空";
	public final static String SYSTEM_PROCES_ERROR = "系统处理逻辑错误，请联系管理员!";
	public final static String UPDATE_ERROR = "修改错误!";
}
