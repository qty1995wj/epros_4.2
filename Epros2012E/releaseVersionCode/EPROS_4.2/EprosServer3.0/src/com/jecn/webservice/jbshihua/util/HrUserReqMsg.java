package com.jecn.webservice.jbshihua.util;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DATAINFOS")
public class HrUserReqMsg
{
  private String uuid;
  private List<HrUserMsg> users;
  
  @XmlAttribute(name="uuid")
  public String getUuid()
  {
    return this.uuid;
  }
  
  public void setUuid(String uuid)
  {
    this.uuid = uuid;
  }
  
  @XmlElement(name="DATAINFO")
  public List<HrUserMsg> getUsers()
  {
    return this.users;
  }
  
  public void setUsers(List<HrUserMsg> users)
  {
    this.users = users;
  }
}
