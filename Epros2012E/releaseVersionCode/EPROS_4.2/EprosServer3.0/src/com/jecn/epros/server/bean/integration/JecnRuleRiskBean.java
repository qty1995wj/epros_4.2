package com.jecn.epros.server.bean.integration;

import java.io.Serializable;

/**
 * 
 * 制度与风险关系正式表
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-21 时间：下午02:07:31
 */
public class JecnRuleRiskBean implements Serializable {
	/** 主键ID */
	private String id;
	/** 制度ID */
	private Long ruleId;
	/** 风险主键ID */
	private Long riskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public Long getRiskId() {
		return riskId;
	}

	public void setRiskId(Long riskId) {
		this.riskId = riskId;
	}

}
