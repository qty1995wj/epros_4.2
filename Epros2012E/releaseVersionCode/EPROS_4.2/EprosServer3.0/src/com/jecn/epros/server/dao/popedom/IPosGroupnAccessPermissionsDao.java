package com.jecn.epros.server.dao.popedom;

import java.util.List;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.JecnPosGroupPermissionsT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface IPosGroupnAccessPermissionsDao extends IBaseDao<JecnPosGroupPermissionsT, Long>  {
	/***
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的岗位组权限
	 * @param relateId资源ID
	 * @param type0是流程1是文件2是标准3制度
	 * @param isPub 发布状态
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPosGroupAccessPermissions(Long relateId,int type,boolean isPub) throws Exception;
	
	/***
	 * 保存岗位组权限
	 * @param relateId
	 * @param type
	 * @param posIds
	 * @param posGroupType
	 * @param userType
	 * @param projectId
	 * @param nodeType
	 * @throws Exception
	 */
	public void  savaOrUpdate(Long relateId, int type, String posIds,int posGroupType,int userType,Long projectId,TreeNodeType nodeType) throws Exception;
	
	/***
	 * 保存岗位组权限
	 * @param relateId
	 * @param type
	 * @param posIds
	 * @param posGroupType
	 * @param userType
	 * @param projectId
	 * @param nodeType
	 * @throws Exception
	 */
	public void  savaOrUpdate(Long relateId, int type, List<AccessId> posIds,int posGroupType,int userType,Long projectId,TreeNodeType nodeType) throws Exception;
	/***
	 * 单个文件(流程地图、流程图、制度、文件)保存岗位组权限
	 * @param relateId
	 * @param type
	 * @param posGroupIds
	 * @param isSave true:新建，false:更新
	 * @throws Exception
	 */
	public void  savaOrUpdate(Long relateId, int type, String posGroupIds,boolean isSave) throws Exception;
	
	/***
	 * 单个文件(流程地图、流程图、制度、文件)保存岗位组权限
	 * @param relateId
	 * @param type
	 * @param posGroupIds
	 * @param isSave true:新建，false:更新
	 * @throws Exception
	 */
	public void  savaOrUpdate(Long relateId, int type, List<AccessId> Ids,boolean isSave) throws Exception;

	public void del(Long relateId, int type, String posGroupIds, int posGroupType, int userType, Long projectId,
			TreeNodeType nodeType)throws Exception;
	public void del(Long relateId, int type, List<AccessId> posGroupIds, int posGroupType, int userType, Long projectId,
			TreeNodeType nodeType)throws Exception;


	public void add(Long relateId, int type, String posGroupIds, int posGroupType, int userType, Long projectId,
			TreeNodeType nodeType)throws Exception;
			
	public void add(Long relateId, int type, List<AccessId> posGroupIds, int posGroupType, int userType, Long projectId,
					TreeNodeType nodeType)throws Exception;		
}
