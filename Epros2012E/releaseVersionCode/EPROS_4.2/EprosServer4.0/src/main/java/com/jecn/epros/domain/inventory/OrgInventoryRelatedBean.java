package com.jecn.epros.domain.inventory;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

public class OrgInventoryRelatedBean {
    /**
     * 流程ID
     */
    private Long flowId;
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程编号
     */
    private String flowIdInput;// 流程编号
    /**
     * 流程责任人
     */
    private String resPeople;
    /**
     * 监护人
     */
    private String guardianPeople;
    /**
     * 流程拟稿人
     */
    private String draftPeople;
    /**
     * 流程有效期
     */
    private Integer expiry;
    /**
     * 处于什么阶段 0是待建，1是审批，2是发布
     */
    private int typeByData;
    /**
     * 0未发布 ，1发布，2审批过程中
     * <p>
     * 蒙牛特有发布类型 0 新建 1新建发布 2换版发布
     **/
    private int pubState;
    /**
     * 发布日期
     */
    private Date pubTime;
    /**
     * 版本号
     */
    private String versionId;
    /**
     * 下次审视需完成的时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date nextScanDate;

    /**
     * 适用部门/责任部门 1责任/适用 2责任*
     * 已发布的流程并且有适用范围的责任/适用 已发布的没有适用范围的责任
     */
    private String orgType;

    /**
     * true 是已审视优化 false 是没有
     */
    public boolean isSurvey() {
        if (this.getExpiry() != null && this.getExpiry().intValue() > 0) {
            if (this.getNextScanDate() != null) {
                if (Common.compareDate(this.getNextScanDate(), new Date())) {
                    return false;
                }
            }

        }

        return true;
    }

    public String getOrgType() {
        if (StringUtils.isBlank(this.orgType)) {
            return "";
        }
        if ("1".equals(this.orgType)) {
            return JecnProperties.getValue("dutyScope");
        } else if ("2".equals(this.orgType)) {
            return JecnProperties.getValue("duty");
        }
        return "";
    }

    //------------------

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getFlowIdInput() {
        return flowIdInput;
    }

    public void setFlowIdInput(String flowIdInput) {
        this.flowIdInput = flowIdInput;
    }

    public String getResPeople() {
        return resPeople;
    }

    public void setResPeople(String resPeople) {
        this.resPeople = resPeople;
    }

    public String getGuardianPeople() {
        return guardianPeople;
    }

    public void setGuardianPeople(String guardianPeople) {
        this.guardianPeople = guardianPeople;
    }

    public String getDraftPeople() {
        return draftPeople;
    }

    public void setDraftPeople(String draftPeople) {
        this.draftPeople = draftPeople;
    }

    public Integer getExpiry() {
        return expiry;
    }

    public void setExpiry(Integer expiry) {
        this.expiry = expiry;
    }

    public int getTypeByData() {
        return typeByData;
    }

    public void setTypeByData(int typeByData) {
        this.typeByData = typeByData;
    }

    public int getPubState() {
        return pubState;
    }

    public void setPubState(int pubState) {
        this.pubState = pubState;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public Date getNextScanDate() {
        return nextScanDate;
    }

    public void setNextScanDate(Date nextScanDate) {
        this.nextScanDate = nextScanDate;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }
}
