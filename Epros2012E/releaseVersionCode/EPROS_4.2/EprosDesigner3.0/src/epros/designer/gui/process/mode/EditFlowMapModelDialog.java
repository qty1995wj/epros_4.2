package epros.designer.gui.process.mode;

import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;

/***
 * 流程模板(流程地图模板) 重命名
 * 
 * @author 2012-06-27
 * 
 */
public class EditFlowMapModelDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(EditFlowMapModelDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	public EditFlowMapModelDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		// 给名称显示框赋值
		this.setName(selectNode.getJecnTreeBean().getName());
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		try {
			ConnectionPool.getProcessModeAction().reName(this.getName(), selectNode.getJecnTreeBean().getId(), JecnConstants.loginBean.getJecnUser().getPeopleId());
		} catch (Exception e) {
			log.error("EditFlowMapModelDialog saveData is error");
		}
		// 重命名
		JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
		this.dispose();
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
	}
	
	@Override
	public boolean validateName(String name, JLabel jLable) {
		return JecnValidateCommon.validateName(name, promptLab);
	}
	protected void validateEnglishName() {// 验证英文名称
	}

	protected void addEnNamePanel(Insets insets) {
	}


}
