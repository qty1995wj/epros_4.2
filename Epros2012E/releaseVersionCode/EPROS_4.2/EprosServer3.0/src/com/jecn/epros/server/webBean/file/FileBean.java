package com.jecn.epros.server.webBean.file;

import java.util.Date;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class FileBean {
	// 文件ID
	private Long fileID;
	// 文件名称
	private String fileName;
	// 文件编号
	private String docId;
	// 创建人ID
	private Long createPeopleId;
	// 0是公开,1是秘密
	private Long isPublic;
	// 创建人
	private String createPeople;
	// 创建时间
	private Date createTime;
	// 创建时间
	private String createStrTime;
	// 更新人ID
	private Long updatePeopleId;
	// 更新人
	private String updatePeople;
	// 更新时间
	private Date updateTime;
	// 更新时间
	private String updateStrTime;
	// 责任部门ID
	private Long orgId;
	// 责任部门
	private String orgName;
	// 所属流程名称
	private String flowName;
	// 文件格式
	private String fileFormat;
	// 部门权限
	private List<JecnTreeBean> listOrg;
	// 岗位权限
	private List<JecnTreeBean> listPos;
	// 文件使用情况
	private List<FileUseBean> fileUseList;

	public Long getFileID() {
		return fileID;
	}

	public void setFileID(Long fileID) {
		this.fileID = fileID;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public Long getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Long isPublic) {
		this.isPublic = isPublic;
	}

	public String getCreatePeople() {
		return createPeople;
	}

	public void setCreatePeople(String createPeople) {
		this.createPeople = createPeople;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

	public String getUpdatePeople() {
		return updatePeople;
	}

	public void setUpdatePeople(String updatePeople) {
		this.updatePeople = updatePeople;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}


	public List<JecnTreeBean> getListOrg() {
		return listOrg;
	}

	public void setListOrg(List<JecnTreeBean> listOrg) {
		this.listOrg = listOrg;
	}

	public List<JecnTreeBean> getListPos() {
		return listPos;
	}

	public void setListPos(List<JecnTreeBean> listPos) {
		this.listPos = listPos;
	}

	public List<FileUseBean> getFileUseList() {
		return fileUseList;
	}

	public void setFileUseList(List<FileUseBean> fileUseList) {
		this.fileUseList = fileUseList;
	}

	public String getCreateStrTime() {
		return createStrTime;
	}

	public void setCreateStrTime(String createStrTime) {
		this.createStrTime = createStrTime;
	}

	public String getUpdateStrTime() {
		return updateStrTime;
	}

	public void setUpdateStrTime(String updateStrTime) {
		this.updateStrTime = updateStrTime;
	}
}
