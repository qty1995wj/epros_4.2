package com.jecn.epros.server.service.dataImport.match.impl;

import java.util.List;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.match.IMatchDataFactory;
import com.jecn.epros.server.service.dataImport.match.buss.AbstractImportUser;
import com.jecn.epros.server.service.dataImport.match.buss.ImportUserByDB;
import com.jecn.epros.server.service.dataImport.match.buss.ImportUserByXML;
import com.jecn.epros.server.service.dataImport.match.buss.ReadUserConfigXml;
import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;
import com.jecn.epros.server.service.dataImport.match.constant.MatchErrorInfo;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

public class MatchDataFactoryImpl implements IMatchDataFactory {
	/** 导入方式（excel、xml、hessian），默认是excel导入 */
	private ImportType importType = ImportType.excel;

	/**
	 * 
	 * 获取待导入数据
	 * 
	 * @param importType
	 *            导入方式（excel、xml、hessian），默认是excel导入
	 * @return BaseBean 待导入的部门、岗位以及人员
	 * @throws Exception
	 *             20120209
	 */
	public BaseBean getImportData(String importType) throws Exception {
		// 判断导入方式是否是给定的方式
		checkImpotType(importType);

		return readUserData();
	}

	/**
	 * 
	 * 获取配置文件之值
	 * 
	 * @param importType
	 *            String
	 * @return
	 */
	public AbstractConfigBean getInitTime(String importType) throws Exception {
		// 判断导入方式是否是给定的方式
		checkImpotType(importType);

		return ReadUserConfigXml.getAbstractConfigBean(this.importType);
	}

	/**
	 * 
	 * 读取数据，Hessian
	 * 
	 * @return
	 * @throws BsException
	 */
	private BaseBean readUserData() throws Exception {
		BaseBean baseBean = null;
		if (ImportType.hessian.equals(importType)) {// Hessian读取数据方式
			// baseBean = proHessian();
		} else if (ImportType.excel.equals(importType)) {// excel读取方式
			// baseBean = proExcel();
		} else if (ImportType.db.equals(importType)) {// 数据库读取方式
			baseBean = proDB();
		} else if (ImportType.xml.equals(importType)) {// xml读取数据方式
			baseBean = proXML();
		}
		return baseBean;
	}

	/**
	 * 
	 * 基于xml方式获取部门、岗位、人员数据
	 * 
	 * @return
	 * @throws Exception
	 */
	private BaseBean proXML() throws Exception {

		// 获取配置文件内容
		AbstractConfigBean configBean = ReadUserConfigXml
				.getAbstractConfigBean(importType);

		if (SyncTool.isNullObj(configBean)) {
			return null;
		}

		// XML方式读取用户数据对象
		ImportUserByXML xmlUser = new ImportUserByXML(configBean);

		// 获取部门、岗位、人员数据 存储到BaseBean对象中
		return createBaseBean(xmlUser);
	}

	/**
	 * 
	 * 基于db 提供视图方式获取部门、岗位、人员数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public BaseBean proDB() throws Exception {

		if (!ImportType.db.equals(importType)) {
			return null;
		}
		// 获取配置文件内容
		AbstractConfigBean configBean = ReadUserConfigXml
				.getAbstractConfigBean(importType);

		if (MatchTool.isNullObj(configBean)) {
			return null;
		}

		// db 方式读取部门、岗位、人员数据
		ImportUserByDB db = new ImportUserByDB(configBean);

		// 获取部门、岗位、人员数据 存储到BaseBean对象中
		return createBaseBean(db);
	}

	/**
	 * 
	 * 创建basebean对象
	 * 
	 * @param importUser
	 * @return
	 * @throws BsException
	 */
	private BaseBean createBaseBean(AbstractImportUser importUser)
			throws Exception {

		// 部门
		List<DeptMatchBean> deptList = importUser.getDeptBeanList();
		// 岗位
		List<PosMatchBean> posList = importUser.getPosBeanList();
		// 人员
		List<UserMatchBean> userList = importUser.getUserBeanList();

		// 获取部门、岗位、人员数据 存储到BaseBean对象中
		return new BaseBean(deptList, posList, userList);
	}

	/**
	 * 
	 * 判断导入方式是否是给定的方式
	 * 
	 * @param importType
	 */
	private void checkImpotType(String importType) {
		if (MatchConstant.IMPORT_TYPE_EXCEL.equals(importType)) {// excel
			this.importType = ImportType.excel;
		} else if (MatchConstant.IMPORT_TYPE_XML.equals(importType)) {// xml
			this.importType = ImportType.xml;
		} else if (MatchConstant.IMPORT_TYPE_HESSIAN.equals(importType)) {// hessian
			this.importType = ImportType.hessian;
		} else if (MatchConstant.IMPORT_TYPE_DB.equals(importType)) {// db
			this.importType = ImportType.db;
		} else {
			throw new IllegalArgumentException(MatchErrorInfo.IMPORT_TYPE_ERROR);
		}
	}

	/**
	 * 
	 * 外部获取数据方式
	 * 
	 */
	public enum ImportType {
		hessian, // 读取hessian接口 访问方式
		xml, // 读取xml中数据访问方式
		excel, // 读取excel访问方式
		db, // 读取数据库访问方式
		none
	}
}
