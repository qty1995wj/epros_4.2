package epros.designer.gui.system.config.ui.dialog;

import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.util.JecnProperties;

/**
 * 
 * 邮箱配置对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnMailConfigJDialog extends JecnAbtractBaseConfigDialog {
	public JecnMailConfigJDialog(JecnConfigDesgBean configBean) {
		super(configBean);
		this.setTitle(JecnProperties.getValue("setTitleMailName"));
	}
}
