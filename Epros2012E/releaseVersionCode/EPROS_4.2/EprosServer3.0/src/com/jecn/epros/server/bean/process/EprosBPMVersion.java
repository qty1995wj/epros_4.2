package com.jecn.epros.server.bean.process;

import java.io.Serializable;

public class EprosBPMVersion implements Serializable {
	private String flowId;
	private String flowName;
	private String updateTime;
	private String version;

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	// 注意这种情况下 我们需要重写equals和hashCode
	public boolean equals(Object object) {
		return true;
	}

	public int hashCode() {
		return 1;
	}

}
