package epros.designer.gui.integration.risk;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.bean.integration.JecnControlTarget;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnWindowsTabbedPaneUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/***
 * 新建风险(基本信息 控制目标) 2013-10-30
 * 
 */
public class AddRiskDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(AddRiskDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(600, 470);

	/** 风险基本信息面板 */
	private JScrollPane baseInfoPanel = new JScrollPane();

	/** 控制目标面板 */
	private JScrollPane controlTargetPanel = new JScrollPane();

	/** JTabbedPane */
	private JTabbedPane tabPanel = new JTabbedPane();

	/** 按钮面板 */
	private JPanel buttonPanel = new JecnPanel(600, 30);

	/*** 验证提示 */
	private JLabel verfyLab = new JLabel();

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 设置大小 */
	private Dimension dimension = null;

	// 基本信息
	private AddRiskBaseInfoPanel riskBaseInfoPanel = null;
	// 控制目标
	private AddRiskControlTargetPanel riskControlTargetPanel = null;

	public AddRiskDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		// 设置窗体大小
		this.setSize(670, 500);
		this.setTitle(JecnProperties.getValue("createRiskPoint"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);

		// 设置面板背景颜色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		tabPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		baseInfoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		controlTargetPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 添加ui
		tabPanel.setUI(new JecnWindowsTabbedPaneUI());
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});

		verfyLab.setForeground(Color.red);
		this.setModal(true);
		initLayout();
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelPerformed();
				return false;
			}
		});
	}

	/***
	 * 布局
	 */
	private void initLayout() {
		Container contentPane = this.getContentPane();
		tabPanel.add(baseInfoPanel, 0);
		tabPanel.setTitleAt(0, JecnProperties.getValue("actBaseInfo"));
		tabPanel.add(controlTargetPanel, 1);
		tabPanel.setTitleAt(1, JecnProperties.getValue("controlTarget"));

		// 风险基本信息
		riskBaseInfoPanel = new AddRiskBaseInfoPanel(selectNode, jTree);
		riskBaseInfoPanel.setVisible(true);
		baseInfoPanel.setViewportView(riskBaseInfoPanel);

		// 控制目标
		riskControlTargetPanel = new AddRiskControlTargetPanel(selectNode, jTree);
		riskControlTargetPanel.setVisible(true);
		controlTargetPanel.setViewportView(riskControlTargetPanel);

		// 按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		contentPane.add(buttonPanel, BorderLayout.SOUTH);
		contentPane.add(tabPanel, BorderLayout.CENTER);
	}

	/***
	 * 确定
	 */
	private void okButPerformed() {
		verfyLab.setText("");
		riskControlTargetPanel.controlTargetVerfy.setText("");
		// 判断控制目标是否保存
		int selectControTargetNum = riskControlTargetPanel.controlTargetTable.getSelectedRow();
		String textAreaTargetStr = riskControlTargetPanel.controlTargetArea.getText().trim();
		if (selectControTargetNum != -1) {
			String tableTargetStr = riskControlTargetPanel.controlTargetTable.getModel().getValueAt(
					selectControTargetNum, 2).toString();
			if (riskControlTargetPanel.isAddOrUpdate == 1 && !tableTargetStr.equals(textAreaTargetStr)) {
				if (textAreaTargetStr == null || "".equals(textAreaTargetStr)) {
					riskControlTargetPanel.controlTargetVerfy.setText(JecnProperties.getValue("controlTarget")
							+ JecnProperties.getValue("isNotEmpty"));
					return;
				}
				int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isSaveEditControlTarget"),
						null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.YES_OPTION) {
					riskControlTargetPanel.controlTargetTable.getModel().setValueAt(textAreaTargetStr,
							selectControTargetNum, 2);
				}
			}
		} else {
			if (textAreaTargetStr != null && !"".equals(textAreaTargetStr)) {
				int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isSaveAddControlTarget"),
						null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.YES_OPTION) {
					Vector<String> v = new Vector<String>();
					v.add("");
					v.add(String.valueOf(riskControlTargetPanel.controlTargetTable.getModel().getRowCount() + 1));
					v.add(riskControlTargetPanel.controlTargetArea.getText());
					riskControlTargetPanel.controlTargetTable.addRow(v);
					riskControlTargetPanel.controlTargetArea.setText("");
				}
			}

		}
		/****** ======================风险基本信息===================== */
		JecnRisk jecnRisk = new JecnRisk();
		jecnRisk.setIsDir(1);
		jecnRisk.setSort(JecnTreeCommon.getMaxSort(selectNode));
		// 风险编号
		String riskNum = riskBaseInfoPanel.riskCodeFiled.getText().trim();
		// 风险编号非空验证
		if (DrawCommon.isNullOrEmtryTrim(riskNum)) {
			verfyLab.setText(JecnProperties.getValue("riskNum") + JecnProperties.getValue("isNotEmpty"));
			return;
		}
		// 编号长度验证
		else if (DrawCommon.checkNameMaxLength(riskNum)) {
			verfyLab.setText(JecnProperties.getValue("riskNum") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		} else {
			// 验证风险编号是否重名
			try {
				int isRepeat = ConnectionPool.getJecnRiskAction().riskNumIsRepeat(riskNum, null);
				if (isRepeat == 0) {
					jecnRisk.setRiskCode(riskNum);
				} else {
					verfyLab.setText(JecnProperties.getValue("riskNum") + JecnProperties.getValue("repeatXist"));
					return;
				}

			} catch (Exception e) {
				log.error("AddRiskDialog okButPerformed is error", e);
			}

		}
		// 风险等级类别 1：高， 2：中， 3：低
		if (riskBaseInfoPanel.gradeCombox.getSelectedItem() != null
				&& !"".equals(riskBaseInfoPanel.gradeCombox.getSelectedItem())) {
			jecnRisk.setGrade(riskBaseInfoPanel.gradeCombox.getSelectedIndex());
		}
		// 风险点描述
		String riskDes = riskBaseInfoPanel.riskDescArea.getText().trim();
		// 风险点描述非空验证
		if (DrawCommon.isNullOrEmtryTrim(riskDes)) {
			verfyLab.setText(JecnProperties.getValue("riskPointDes") + JecnProperties.getValue("isNotEmpty"));
			return;
		}
		// 风险点描述长度验证
		if (riskDes != null && !"".equals(riskDes)) {
			if (DrawCommon.checkNoteLength(riskDes)) {
				verfyLab.setText(JecnProperties.getValue("riskPointDes") + JecnProperties.getValue("lengthNotOut"));
				return;
			} else {
				jecnRisk.setRiskName(riskDes);
			}
		}
		// 标准化控制
		String standardControl = riskBaseInfoPanel.standdardControlArea.getText().trim();
		// 标准化控制长度验证
		if (standardControl != null && !"".equals(standardControl)) {
			if (DrawCommon.checkNoteLength(standardControl)) {
				verfyLab.setText(JecnProperties.getValue("standardControl") + JecnProperties.getValue("lengthNotOut"));
				return;
			} else {
				jecnRisk.setStandardControl(standardControl);
			}
		}
		// 内控指引知识库

		// 创建人
		jecnRisk.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 创建时间
		jecnRisk.setCreateTime(new Date());
		// 更新人
		jecnRisk.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 更新时间
		jecnRisk.setUpdateTime(new Date());
		jecnRisk.setParentId(selectNode.getJecnTreeBean().getId());
		jecnRisk.setProjectId(JecnConstants.projectId);
		if (riskBaseInfoPanel.getRiskType() != null) {
			jecnRisk.setRiskType(Integer.valueOf(riskBaseInfoPanel.getRiskType().getKey().toString()));
		}

		Long id = null;
		try {
			/****** ======================控制目标===================== */
			// 获取控制目标表格中的数据
			Vector<Vector<String>> data = ((DefaultTableModel) riskControlTargetPanel.controlTargetTable.getModel())
					.getDataVector();
			List<JecnControlTarget> listControlTarget = new ArrayList<JecnControlTarget>();
			for (Vector<String> str : data) {
				JecnControlTarget jecnControlTarget = new JecnControlTarget();
				jecnControlTarget.setDescription(str.get(2));
				// 创建人
				jecnControlTarget.setCreatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
				// 创建时间
				jecnControlTarget.setCreateTime(new Date());
				// 更新人
				jecnControlTarget.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
				// 更新时间
				jecnControlTarget.setUpdateTime(new Date());
				jecnControlTarget.setSort(Integer.parseInt(str.get(1)));
				// 关联风险点主键ID
				// jecnControlTarget.setRiskId(id);
				listControlTarget.add(jecnControlTarget);
			}
			if (listControlTarget.size() > 0) {
				// ConnectionPool.getJecnRiskAction().addJecnControlTarget(listControlTarget);
				jecnRisk.setListControlTarget(listControlTarget);

			}

			/** ====================内控指引知识库================================= **/

			// 获取控制目标表格中的数据
			Vector<Vector<String>> dataControlGuide = ((DefaultTableModel) riskBaseInfoPanel.controlGuideTable
					.getModel()).getDataVector();
			List<JecnControlGuide> listControlGuide = new ArrayList<JecnControlGuide>();
			if (dataControlGuide.size() > 0) {
				for (Vector<String> str : dataControlGuide) {
					JecnControlGuide jecnControlGuide = new JecnControlGuide();
					jecnControlGuide.setId(Long.valueOf(str.get(0).toString()));
					jecnControlGuide.setName(str.get(1));
					listControlGuide.add(jecnControlGuide);
				}
			}
			if (listControlGuide.size() > 0) {
				jecnRisk.setListControlGuide(listControlGuide);
			}
			id = ConnectionPool.getJecnRiskAction().addJecnRiskOrDir(jecnRisk);
			// 向树节点添加风险点节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setContent(riskDes);
			if (riskDes.length() > 8) {
				riskDes = riskDes.substring(0, 8) + "...";
			}
			jecnTreeBean.setName(riskNum + " " + riskDes);
			jecnTreeBean.setPid(selectNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(selectNode.getJecnTreeBean().getName());

			jecnTreeBean.setTreeNodeType(TreeNodeType.riskPoint);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, selectNode);
		} catch (Exception e) {
			log.error("AddRiskDialog okButPerformed is error", e);
			return;
		}
		this.dispose();
	}

	private boolean isUpdate() {
		boolean isUpdate = false;
		String textAreaTargetStr = riskControlTargetPanel.controlTargetArea.getText().trim();
		String riskNum = riskBaseInfoPanel.riskCodeFiled.getText().trim();
		String riskDes = riskBaseInfoPanel.riskDescArea.getText().trim();
		String standardControl = riskBaseInfoPanel.standdardControlArea.getText().trim();
		if (!"".equals(textAreaTargetStr)) {
			isUpdate = true;
		}
		if (!"".equals(riskNum)) {
			isUpdate = true;
		}
		if (!"".equals(riskDes)) {
			isUpdate = true;
		}
		if (!"".equals(standardControl)) {
			isUpdate = true;
		}
		return isUpdate;
	}

	/**
	 * 取消
	 */
	protected void cancelPerformed() {
		if (!isUpdate()) {// 没有更新
			this.dispose();
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				this.dispose();
			}
		}
	}
}
