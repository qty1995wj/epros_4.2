package com.jecn.epros.server.bean.log;

import java.io.Serializable;

/**
 * 权限个人个性化记录表 （所选标识和记录表一致）
 * 
 * @author jecn
 * 
 */
public class JecnPersonalPermRecord implements Serializable {
	private String id;
	private Long userId;
	private Long rId;
	/**
	 * 0是流程，1是文件，2是标准，3是制度
	 */
	private Integer rType;
	/**
	 * 0覆盖 1添加 2删除
	 */
	private Integer handle;
	/**
	 * 1当前及子节点 0当前
	 */
	private Integer secret;
	private Integer org;
	private Integer pos;
	private Integer posGroup;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getrId() {
		return rId;
	}

	public void setrId(Long rId) {
		this.rId = rId;
	}

	public Integer getrType() {
		return rType;
	}

	public void setrType(Integer rType) {
		this.rType = rType;
	}

	public Integer getHandle() {
		return handle;
	}

	public void setHandle(Integer handle) {
		this.handle = handle;
	}

	public Integer getSecret() {
		return secret;
	}

	public void setSecret(Integer secret) {
		this.secret = secret;
	}

	public Integer getOrg() {
		return org;
	}

	public void setOrg(Integer org) {
		this.org = org;
	}

	public Integer getPos() {
		return pos;
	}

	public void setPos(Integer pos) {
		this.pos = pos;
	}

	public Integer getPosGroup() {
		return posGroup;
	}

	public void setPosGroup(Integer posGroup) {
		this.posGroup = posGroup;
	}

}
