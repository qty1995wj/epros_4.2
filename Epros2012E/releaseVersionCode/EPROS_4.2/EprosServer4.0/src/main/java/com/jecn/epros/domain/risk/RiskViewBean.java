package com.jecn.epros.domain.risk;

import java.util.List;

public class RiskViewBean {

    private int total;

    private List<SearchRiskResultBean> data;

    public List<SearchRiskResultBean> getData() {
        return data;
    }

    public void setData(List<SearchRiskResultBean> data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
