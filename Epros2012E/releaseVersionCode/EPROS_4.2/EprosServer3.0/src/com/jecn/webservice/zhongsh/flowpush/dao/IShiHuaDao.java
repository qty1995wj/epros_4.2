package com.jecn.webservice.zhongsh.flowpush.dao;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.process.FlowFileInputi3GUIDBean;
import com.jecn.webservice.zhongsh.flowpush.bean.FlowElem;

public interface IShiHuaDao {

	public List<FlowElem> findAllByFlowImageId(Long flowId) throws Exception;

	public Map<Long, List<String>> findRoleRelatedPeopleId(Long flowId) throws Exception;

	Object[] getFlowInfoObjects(Long flowId) throws Exception;

	void saveFlowFileInputi3GUIDBean(FlowFileInputi3GUIDBean inputi3guidBean) throws Exception;

}
