package com.jecn.epros.server.bean.rule;

import java.io.Serializable;
import java.util.Date;

public class JecnRuleUseRecord implements Serializable  {
	/**主键ID*/
	private String id;
	/**时间段*/
	private Date accessDate;
	/** 用户ID*/
	private Long peopleId;
	/**制度ID*/
	private Long ruleId;
	/**表的类型 0:查看流程统计 1:下载次数统计*/
	private int useType;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getAccessDate() {
		return accessDate;
	}
	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}
	public Long getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}
	public Long getRuleId() {
		return ruleId;
	}
	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}
	public int getUseType() {
		return useType;
	}
	public void setUseType(int useType) {
		this.useType = useType;
	}
	
}
