//各类型任务审批主要的方法
function taskRepository() {

	var taskRepositoryTabPanel = new Ext.TabPanel(
			{
				border : false,
				// resizeTabs:true,//True表示为自动调整各个标签页的宽度
				activeTab : 0,
				id : 'taskRepositoryTabPanel_id',
				height : Ext.getCmp("main-panel").getInnerHeight() - 20,
				renderTo : 'taskApproveTimeGrid_id',
				defaults : {
					border : false
				},
				items : [
					  
						{
							title : i_processTask,// 流程
							id : "processTask_id",
							autoLoad : {
								url : basePath + "jsp/reports/processTaskApproveTime.jsp",
								callback : function() {
									processApproveTime();
								}
							}
						},
						
						{
							title : i_processMapTask,// 流程地图
							id : "processMapTask_id",
							autoLoad : {
								url : basePath
										+ "jsp/reports/processMapTaskApproveTime.jsp",
								callback : function() {
									processMapApproveTime();
								}
							}
						},
						{
							title : i_ruleTask,// 制度
							id : "ruleTask_id",
							border:false,
							autoLoad : {
								url : basePath
										+ "jsp/reports/ruleTaskApproveTime.jsp",
								callback : function() {
									ruleApproveTime();
								}
							}
						}, {
							title : i_fileTask,// 文件
							border:false,
							id : "fileTask_id",
							autoLoad : {
								url : basePath + "jsp/reports/fileTaskApproveTime.jsp",
								callback : function() {
									fileApproveTime();
								}
							}
						} ],
				listeners : {
					'bodyresize' : function(p, width, height) {
						if (p.getActiveTab() == null) {
							return;
						}
						if (p.getActiveTab().getId() == 'processTask_id') {
							Ext.getCmp('processTaskApproveTimeGridPanel_id')
									.setSize(width, height-30);
						} else if (p.getActiveTab().getId() == 'processMapTask_id') {
							Ext.getCmp('processMapTaskApproveTimeGridPanel_id')
									.setSize(width, height-30);
						} else if (p.getActiveTab().getId() == 'ruleTask_id') {
							Ext.getCmp('ruleTaskApproveTimeGridPanel_id').setSize(
									width, height-30);
						} else if (p.getActiveTab().getId() == 'fileTask_id') {
							Ext.getCmp('fileTaskApproveTimeGridPanel_id')
									.setSize(width, height-30);
						} 
					}
					
				}

			});

}


//各种类型任务审批统计时间表(根据任务类型判断)
function processApproveTime(){
	Ext.Ajax.request( {
		url : 'taskApproveTimeHeaders.action',
		method : 'POST',
		timeout : 1000000,
		params : {
			taskType : 0
			
		},
		success : function(response, options) {
			// 获取response中的数据
	    var json = Ext.util.JSON.decode(response.responseText);
		processApproveTimeGrid(json,0);
       
	},
	// 失败调用
		failure : function(response, options) {
			alert(i_systemErrorReportToAdministrator);
		}
	});	
}

function processMapApproveTime(){
	Ext.Ajax.request( {
		url : 'taskApproveTimeHeaders.action',
		method : 'POST',
		timeout : 1000000,
		params : {
			taskType : 4
			
		},
		success : function(response, options) {
			// 获取response中的数据
	    var json = Ext.util.JSON.decode(response.responseText);
		processMapApproveTimeGrid(json,4);
       
	},
	// 失败调用
		failure : function(response, options) {
			alert(i_systemErrorReportToAdministrator);
		}
	});	
}

function ruleApproveTime(){
	Ext.Ajax.request( {
		url : 'taskApproveTimeHeaders.action',
		method : 'POST',
		timeout : 1000000,
		params : {
			taskType : 2
			
		},
		success : function(response, options) {
			// 获取response中的数据
	    var json = Ext.util.JSON.decode(response.responseText);
		ruleApproveTimeGrid(json,2);
       
	},
	// 失败调用
		failure : function(response, options) {
			alert(i_systemErrorReportToAdministrator);
		}
	});	
}

function fileApproveTime(){
	Ext.Ajax.request( {
		url : 'taskApproveTimeHeaders.action',
		method : 'POST',
		timeout : 1000000,
		params : {
			taskType : 1
			
		},
		success : function(response, options) {
			// 获取response中的数据
	    var json = Ext.util.JSON.decode(response.responseText);
		fileApproveTimeGrid(json,1);
       
	},
	// 失败调用
		failure : function(response, options) {
			alert(i_systemErrorReportToAdministrator);
		}
	});	
}

function processApproveTimeGrid(json,taskType)
{
		
		processTaskApproveTimeGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize,
						taskType:taskType
					}
				},
				baseParams : {
				taskType : taskType
				},
				proxy : new Ext.data.HttpProxy({
							url : "taskApproveTime.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "taskId",
								mapping : "taskId"
							}, {
								name : "taskName",
								mapping : "taskName"
							}, {
								name : "taskType",
								mapping : "taskType"
							}, {
								name : "peopleMake",
								mapping : "peopleMake"
							}, {
								name : "documentControl",
								mapping : "documentControl"
							}, {
								name : "department",
								mapping : "department"
							},{
								name : "reviewPeople",
								mapping : "reviewPeople"
							},{
								name : "approval",
								mapping : "approval"
							},{
								name : "eachBusiness",
								mapping : "eachBusiness"
							},{
								name : "director",
								mapping : "director"
							},{
								name : "departmentManager",
								mapping : "departmentManager"
							},{
								name : "manager",
								mapping : "manager"
							},{
								name : "finishingViews",
								mapping : "finishingViews"
							}])

			});
		
    //表头
	 processTaskApproveTimeGridColumnModel = new Ext.grid.ColumnModel(
		
		{
			defaults : {
			align : 'center',
			sortable : true,
			menuDisabled : false
		},
		columns : json.columns
	    }
		);
	
	processTaskApproveTimeGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processTaskApproveTimeGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});
	processTaskApproveTimeGridPanel = new Ext.grid.GridPanel({
				id : "processTaskApproveTimeGridPanel_id",
				renderTo : 'processTaskApproveTimeGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-90,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processTaskApproveTimeGridStore,
				colModel :  processTaskApproveTimeGridColumnModel,
				bbar : processTaskApproveTimeGridBbar
			})
		reports_index_id='reports_taskApproveTime_id'; 

}


function processMapApproveTimeGrid(json,taskType)
{
	    var taskType=taskType;
		processMapTaskApproveTimeGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize,
						taskType:4
					}
				},
				baseParams : {
				taskType : taskType
				},
				proxy : new Ext.data.HttpProxy({
							url : "taskApproveTime.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "taskId",
								mapping : "taskId"
							}, {
								name : "taskName",
								mapping : "taskName"
							}, {
								name : "taskType",
								mapping : "taskType"
							}, {
								name : "peopleMake",
								mapping : "peopleMake"
							}, {
								name : "documentControl",
								mapping : "documentControl"
							}, {
								name : "department",
								mapping : "department"
							},{
								name : "reviewPeople",
								mapping : "reviewPeople"
							},{
								name : "approval",
								mapping : "approval"
							},{
								name : "eachBusiness",
								mapping : "eachBusiness"
							},{
								name : "director",
								mapping : "director"
							},{
								name : "departmentManager",
								mapping : "departmentManager"
							},{
								name : "manager",
								mapping : "manager"
							},{
								name : "finishingViews",
								mapping : "finishingViews"
							}])

			});
	
	
    //表头
	 processMapTaskApproveTimeGridColumnModel = new Ext.grid.ColumnModel(
		
		{
			defaults : {
			align : 'center',
			sortable : true,
			menuDisabled : false
		},
		columns : json.columns
	    }
		);
	
	processMapTaskApproveTimeGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processMapTaskApproveTimeGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processMapTaskApproveTimeGridPanel = new Ext.grid.GridPanel({
				id : "processMapTaskApproveTimeGridPanel_id",
				renderTo : 'processMapTaskApproveTimeGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-90,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processMapTaskApproveTimeGridStore,
				colModel :  processMapTaskApproveTimeGridColumnModel,
				bbar : processMapTaskApproveTimeGridBbar
			})
		reports_index_id='reports_taskApproveTime_id'; 

}

function ruleApproveTimeGrid(json,taskType)
{
	
	
		ruleTaskApproveTimeGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize,
						taskType:taskType
					}
				},
				baseParams : {
				taskType : taskType
				},
				proxy : new Ext.data.HttpProxy({
							url : "taskApproveTime.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "taskId",
								mapping : "taskId"
							}, {
								name : "taskName",
								mapping : "taskName"
							}, {
								name : "taskType",
								mapping : "taskType"
							}, {
								name : "peopleMake",
								mapping : "peopleMake"
							}, {
								name : "documentControl",
								mapping : "documentControl"
							}, {
								name : "department",
								mapping : "department"
							},{
								name : "reviewPeople",
								mapping : "reviewPeople"
							},{
								name : "approval",
								mapping : "approval"
							},{
								name : "eachBusiness",
								mapping : "eachBusiness"
							},{
								name : "director",
								mapping : "director"
							},{
								name : "departmentManager",
								mapping : "departmentManager"
							},{
								name : "manager",
								mapping : "manager"
							},{
								name : "finishingViews",
								mapping : "finishingViews"
							}])

			});
	
	
    //表头
	 ruleTaskApproveTimeGridColumnModel = new Ext.grid.ColumnModel(
		
		
		{
			defaults : {
			align : 'center',
			sortable : true,
			menuDisabled : false
		},
		columns : json.columns
	    }
		);
	
	ruleTaskApproveTimeGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : ruleTaskApproveTimeGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	ruleTaskApproveTimeGridPanel = new Ext.grid.GridPanel({
				id : "ruleTaskApproveTimeGridPanel_id",
				renderTo : 'ruleTaskApproveTimeGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-90,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : ruleTaskApproveTimeGridStore,
				colModel : ruleTaskApproveTimeGridColumnModel,
				bbar : ruleTaskApproveTimeGridBbar
			})
		reports_index_id='reports_taskApproveTime_id'; 
	
}

function fileApproveTimeGrid(json,taskType)
{
	
	
		fileTaskApproveTimeGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize,
						taskType:taskType
					}
				},
				baseParams : {
				taskType : taskType
				},
				proxy : new Ext.data.HttpProxy({
							url : "taskApproveTime.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
								name : "taskId",
								mapping : "taskId"
							}, {
								name : "taskName",
								mapping : "taskName"
							}, {
								name : "taskType",
								mapping : "taskType"
							}, {
								name : "peopleMake",
								mapping : "peopleMake"
							}, {
								name : "documentControl",
								mapping : "documentControl"
							}, {
								name : "department",
								mapping : "department"
							},{
								name : "reviewPeople",
								mapping : "reviewPeople"
							},{
								name : "approval",
								mapping : "approval"
							},{
								name : "eachBusiness",
								mapping : "eachBusiness"
							},{
								name : "director",
								mapping : "director"
							},{
								name : "departmentManager",
								mapping : "departmentManager"
							},{
								name : "manager",
								mapping : "manager"
							},{
								name : "finishingViews",
								mapping : "finishingViews"
							}])
			});
	
    //表头
	 fileTaskApproveTimeGridColumnModel = new Ext.grid.ColumnModel(
		
		{
			defaults : {
			align : 'center',
			sortable : true,
			menuDisabled : false
		},
		columns : json.columns
	    }
		);
	
	fileTaskApproveTimeGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : fileTaskApproveTimeGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	fileTaskApproveTimeGridPanel = new Ext.grid.GridPanel({
				id : "fileTaskApproveTimeGridPanel_id",
				renderTo : 'fileTaskApproveTimeGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-90,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : fileTaskApproveTimeGridStore,
				colModel : fileTaskApproveTimeGridColumnModel,
				bbar : fileTaskApproveTimeGridBbar
			})
		reports_index_id='reports_taskApproveTime_id'; 

}

/**
 * 任务查看超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function taskRenderer(value, cellmeta, record){
	return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='taskLookUp.action?taskId="+record.data['taskId']+"&taskType="+record.data['taskType']+"' >"+value+"</a>";
}

/**
 * 任务审批时间下载统计
 */
function taskApproveTimeDownloadExcel(taskType){
    
     window.location.href="downloadTaskApproveTime.action?taskType="+taskType;
}



