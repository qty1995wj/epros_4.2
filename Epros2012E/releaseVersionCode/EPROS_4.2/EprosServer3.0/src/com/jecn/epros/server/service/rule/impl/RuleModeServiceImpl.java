package com.jecn.epros.server.service.rule.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.rule.RuleModeBean;
import com.jecn.epros.server.bean.rule.RuleModeTitleBean;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.JecnJournal;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.rule.IRuleModeDao;
import com.jecn.epros.server.service.rule.IRuleModeService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

@Transactional
public class RuleModeServiceImpl extends AbsBaseService<RuleModeBean, Long> implements IRuleModeService {

	private IRuleModeDao ruleModeDao;

	public IRuleModeDao getRuleModeDao() {
		return ruleModeDao;
	}

	public void setRuleModeDao(IRuleModeDao ruleModeDao) {
		this.ruleModeDao = ruleModeDao;
		this.baseDao = ruleModeDao;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：制度模板子节点通用查询
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjects(List<Object[]> list) throws Exception {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			if (obj.length > 3) {
				if ("1".equals(obj[3].toString())) {
					// 制度模板目录
					jecnTreeBean.setTreeNodeType(TreeNodeType.ruleMode);
				} else {
					// 制度模板节点
					jecnTreeBean.setTreeNodeType(TreeNodeType.ruleModeDir);
				}
			} else {
				// 制度模板节点
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleMode);
			}

			// 制度模板节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 制度模板节点名称
			jecnTreeBean.setName(obj[1].toString());
			// 制度模板节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			// 制度模板节点父类名称
			jecnTreeBean.setPname("");
			if (obj.length > 4) {
				if (Integer.parseInt(obj[4].toString()) > 0) {
					// 角色节点是否有子节点
					jecnTreeBean.setChildNode(true);
				} else {
					jecnTreeBean.setChildNode(false);
				}
			} else {
				jecnTreeBean.setChildNode(false);
			}
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	@Override
	public List<JecnTreeBean> getChildRuleMode(Long pId) throws Exception {
		String sql = "select t.id,t.mode_name,t.p_id,t.mode_type,(select count(*) from "
				+ "jecn_mode where p_id=t.id) as count from jecn_mode t where t.p_id = ? order by t.p_id,t.sort_id,t.id";
		List<Object[]> list = ruleModeDao.listNativeSql(sql, pId);
		return findByListObjects(list);
	}

	@Override
	public void updateSortRuleMode(List<JecnTreeDragBean> list, Long pId) throws Exception {
		String hql = "from RuleModeBean where parentId=?";
		List<RuleModeBean> listRuleModeBean = ruleModeDao.listHql(hql, pId);
		List<RuleModeBean> listUpdate = new ArrayList<RuleModeBean>();
		for (JecnTreeDragBean jecnTreeDragBean : list) {
			for (RuleModeBean ruleModeBean : listRuleModeBean) {
				if (jecnTreeDragBean.getId().equals(ruleModeBean.getId())) {
					if (ruleModeBean.getSortId() == null
							|| (jecnTreeDragBean.getSortId() != ruleModeBean.getSortId().intValue())) {
						ruleModeBean.setSortId(jecnTreeDragBean.getSortId());
						listUpdate.add(ruleModeBean);
						break;
					}
				}
			}
		}

		for (RuleModeBean ruleModeBean : listUpdate) {
			ruleModeDao.update(ruleModeBean);
			JecnUtil.saveJecnJournal(ruleModeBean.getId(), ruleModeBean.getModeName(), 19, 5, ruleModeBean
					.getCreatePeopleId(), ruleModeDao);
		}

	}

	@Override
	public List<JecnTreeBean> getAllNode() throws Exception {
		String sql = "select t.id,t.mode_name,t.p_id,t.mode_type,(select count(*) from "
				+ "jecn_mode where p_id=t.id) as count from jecn_mode t order by t.p_id,t.sort_id,t.id ";
		List<Object[]> list = ruleModeDao.listNativeSql(sql);
		return findByListObjects(list);
	}

	@Override
	public List<JecnTreeBean> getChildRuleModeDir(Long pId) throws Exception {
		String sql = "select t.id,t.mode_name,t.p_id,t.mode_type,(select count(*) from "
				+ "jecn_mode where p_id=t.id) as count from jecn_mode t where t.mode_type=0 and t.p_id = ? order by t.p_id,t.sort_id,t.id";
		List<Object[]> list = ruleModeDao.listNativeSql(sql, pId);
		return findByListObjects(list);
	}

	@Override
	public List<JecnTreeBean> getAllRuleModeDir() throws Exception {
		String sql = "select t.id,t.mode_name,t.p_id,t.mode_type,(select count(*) from "
				+ "jecn_mode where p_id=t.id) as count from jecn_mode t where t.mode_type=0 order by t.p_id,t.sort_id,t.id ";
		List<Object[]> list = ruleModeDao.listNativeSql(sql);
		return findByListObjects(list);
	}

	@Override
	public List<JecnTreeBean> getRuleModeDirsByNameMove(String name, List<Long> listIds) throws Exception {
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "WITH MY_JECN AS(SELECT * FROM jecn_mode JFF WHERE JFF.id in"
					+ JecnCommonSql.getIds(listIds)
					+ " UNION ALL"
					+ " SELECT JFS.* FROM MY_JECN INNER JOIN jecn_mode JFS ON MY_JECN.id=JFS.p_id)"
					+ " select t.id,t.mode_name,t.p_id,t.mode_type"
					+ " ,(select count(*) from jecn_mode where p_id=t.id and mode_type=0)"
					+ " from jecn_mode t where and t.mode_name like ? and t.mode_type=0 and t.id not in (select id from MY_JECN) order by t.p_id,t.sort_id,t.id";
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "select t.id,t.mode_name,t.p_id,t.mode_type,(select count(*) from jecn_mode where p_id=t.id and mode_type=0)"
					+ " from jecn_mode t where t.mode_name like ? and t.mode_type=0 and t.id not in (select jm.id from jecn_mode jm"
					+ " connect by prior jm.id = jm.p_id START WITH jm.id in "
					+ JecnCommonSql.getIds(listIds)
					+ " ) order by t.p_id,t.sort_id,t.id";
		}
		List<Object[]> list = ruleModeDao.listNativeSql(sql, 0, 20, "%" + name + "%");
		return findByListObjects(list);
	}

	@Override
	public void moveRuleModes(List<Long> listIds, Long pId, Long peopleId) throws Exception {
		String hql = " update RuleModeBean set parentId=? where id in " + JecnCommonSql.getIds(listIds);
		ruleModeDao.execteHql(hql, pId);
		JecnUtil.saveJecnJournals(listIds, 19, 4, peopleId, ruleModeDao);
	}

	@Override
	public Long addRuleMode(RuleModeBean ruleModeBean, List<RuleModeTitleBean> ruleModeTitleBean) throws Exception {
		ruleModeBean.setCreateDate(new Date());
		ruleModeBean.setUpdateDate(new Date());
		Long id = ruleModeDao.save(ruleModeBean);
		for (RuleModeTitleBean RuleModeTitleBean : ruleModeTitleBean) {
			RuleModeTitleBean.setModeId(id);
			ruleModeDao.getSession().save(RuleModeTitleBean);
		}
		JecnUtil.saveJecnJournal(ruleModeBean.getId(), ruleModeBean.getModeName(), 19, 1, ruleModeBean
				.getCreatePeopleId(), ruleModeDao);
		return id;
	}

	@Override
	public void updateName(String newName, Long id, Long updatePersonId) throws Exception {
		RuleModeBean ruleModeBean = ruleModeDao.get(id);
		ruleModeBean.setUpdatePeopleId(updatePersonId);
		ruleModeBean.setUpdateDate(new Date());
		ruleModeBean.setModeName(newName);
		ruleModeDao.update(ruleModeBean);
		JecnUtil.saveJecnJournal(ruleModeBean.getId(), ruleModeBean.getModeName(), 19, 2, updatePersonId, ruleModeDao,
				4);
	}

	@Override
	public void updateRuleMode(RuleModeBean ruleModeBean, List<RuleModeTitleBean> ruleModeTitleBeanList)
			throws Exception {
		ruleModeDao.update(ruleModeBean);
		List<RuleModeTitleBean> listRuleModeTitleBean = getRuleModeTitles(ruleModeBean.getId());
		List<RuleModeTitleBean> listUpdate = new ArrayList<RuleModeTitleBean>();
		/** 前台修改 */
		int sizeView = ruleModeTitleBeanList.size();
		/** 数据库存储 */
		int sizeData = listRuleModeTitleBean.size();
		if (sizeView >= sizeData) {
			List<RuleModeTitleBean> saveUpdate = new ArrayList<RuleModeTitleBean>();
			for (int i = 0; i < sizeView; i++) {
				RuleModeTitleBean viewBean = ruleModeTitleBeanList.get(i);
				if (i <= sizeData - 1) {
					RuleModeTitleBean dataBean = listRuleModeTitleBean.get(i);
					dataBean.setSortId(viewBean.getSortId());
					dataBean.setTitleName(viewBean.getTitleName());
					dataBean.setEnName(viewBean.getEnName());
					dataBean.setType(viewBean.getType());
					dataBean.setRequiredType(viewBean.getRequiredType());
					listUpdate.add(dataBean);
				} else {
					viewBean.setModeId(ruleModeBean.getId());
					viewBean.setId(null);
					saveUpdate.add(viewBean);
				}
			}
			if (listUpdate.size() > 0) {
				for (RuleModeTitleBean bean : listUpdate) {
					ruleModeDao.getSession().update(bean);
				}
			}
			if (saveUpdate.size() > 0) {
				for (RuleModeTitleBean bean : saveUpdate) {
					ruleModeDao.getSession().save(bean);
				}
			}
		} else if (sizeView == 0) {
			String hql = "delete from RuleModeTitleBean where  modeId = " + ruleModeBean.getId();
			ruleModeDao.execteHql(hql);
		} else {
			List<Long> listUpdateId = new ArrayList<Long>();
			for (int i = 0; i < sizeView; i++) {
				RuleModeTitleBean viewBean = ruleModeTitleBeanList.get(i);
				RuleModeTitleBean dataBean = listRuleModeTitleBean.get(i);
				dataBean.setSortId(viewBean.getSortId());
				dataBean.setTitleName(viewBean.getTitleName());
				dataBean.setEnName(viewBean.getEnName());
				dataBean.setType(viewBean.getType());
				dataBean.setRequiredType(viewBean.getRequiredType());
				listUpdate.add(dataBean);
				listUpdateId.add(dataBean.getId());
			}
			if (listUpdateId.size() > 0) {
				String hql = "delete from RuleModeTitleBean where id not in" + JecnCommonSql.getIds(listUpdateId)
						+ " and modeId = " + ruleModeBean.getId();
				ruleModeDao.execteHql(hql);
			}
			if (listUpdate.size() > 0) {
				for (RuleModeTitleBean bean : listUpdate) {
					ruleModeDao.getSession().update(bean);
				}
			}
		}
	}

	@Override
	public void deleteRuleMode(List<Long> ids, Long peopleId) throws Exception {
		Set<Long> setIds = new HashSet<Long>();
		String sql = "";
		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "WITH MY_JECN AS(SELECT * FROM jecn_mode JFF WHERE JFF.id in" + JecnCommonSql.getIds(ids)
					+ " UNION ALL" + " SELECT JFS.* FROM MY_JECN INNER JOIN jecn_mode JFS ON MY_JECN.id=JFS.p_id)"
					+ " select distinct id from MY_JECN";
			List<Object> list = ruleModeDao.listNativeSql(sql);
			for (Object obj : list) {
				if (obj == null) {
					continue;
				}
				setIds.add(Long.valueOf(obj.toString()));
			}
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "select distinct jm.id from jecn_mode jm" + " connect by prior jm.id = jm.p_id START WITH jm.id in "
					+ JecnCommonSql.getIds(ids);
			List<Object> list = ruleModeDao.listNativeSql(sql);
			for (Object obj : list) {
				if (obj == null) {
					continue;
				}
				setIds.add(Long.valueOf(obj.toString()));
			}
		} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
			sql = "select id,p_id from jecn_mode";
			List<Object[]> listAll = ruleModeDao.listNativeSql(sql);
			setIds = JecnCommon.getAllChilds(ids, listAll);
			// for (Long id : ids) {
			// setIds.add(id);
			// }
		}
		if (setIds.size() > 0) {
			JecnUtil.saveJecnJournals(ids, 19, 3, peopleId, ruleModeDao);
			sql = "from RuleT where templateId in";
			List<String> listSqls = JecnCommonSql.getSetSqls(sql, setIds);
			for (String hql : listSqls) {
				List<RuleT> listRuleT = ruleModeDao.listHql(hql);
				for (RuleT ruleT : listRuleT) {
					ruleT.setTemplateId(null);
					ruleModeDao.getSession().update(ruleT);
				}
			}
			// 删除制度模板标题表
			sql = "delete from RuleModeTitleBean where modeId in";
			listSqls = JecnCommonSql.getSetSqls(sql, setIds);
			for (String delSql : listSqls) {
				ruleModeDao.execteHql(delSql);
			}
			// 删除制度模板
			sql = "delete from RuleModeBean where id in";
			listSqls = JecnCommonSql.getSetSqls(sql, setIds);
			for (String delSql : listSqls) {
				ruleModeDao.execteHql(delSql);
			}
		}
	}

	@Override
	public List<JecnTreeBean> findRuleModeByName(String name) throws Exception {
		String sql = "select t.id,t.mode_name,t.p_id from jecn_mode t where t.mode_type=1 and t.mode_name like ?  order by t.p_id,t.sort_id,t.id";
		List<Object[]> list = ruleModeDao.listNativeSql(sql, 0, 20, "%" + name + "%");
		return findByListObjects(list);
	}

	@Override
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid) throws Exception {
		String hql = "select count(*) from RuleModeBean where modeName=? and parentId=? and id<>? and modeType=1";
		return ruleModeDao.countAllByParamsHql(hql, newName, pid, id) > 0 ? true : false;
	}

	@Override
	public List<RuleModeTitleBean> getRuleModeTitles(Long modeId) throws Exception {
		String hql = "from RuleModeTitleBean where modeId=? order by sortId";
		return ruleModeDao.listHql(hql, modeId);
	}

	@Override
	public Long addRuleModeDir(RuleModeBean ruleModeBean) throws Exception {
		ruleModeBean.setCreateDate(new Date());
		ruleModeBean.setUpdateDate(new Date());
		ruleModeDao.save(ruleModeBean);
		JecnUtil.saveJecnJournal(ruleModeBean.getId(), ruleModeBean.getModeName(), 19, 1, ruleModeBean
				.getUpdatePeopleId(), baseDao);
		return ruleModeBean.getId();
	}

}
