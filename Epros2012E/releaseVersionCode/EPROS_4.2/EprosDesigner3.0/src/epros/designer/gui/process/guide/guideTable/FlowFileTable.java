package epros.designer.gui.process.guide.guideTable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.gui.process.guide.DesignerProcessOperationIntrusDialog;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 文件表格
 * 
 * @author 2012-07-05
 * 
 */
public class FlowFileTable extends JTable {
	private Vector<Vector<String>> content;
	/** 活动的数据 */
	private List<JecnActivityShowBean> listAllActivityShow;

	/** 需要显示的类型 true 为 流程活动 ， false为 操作规范 */
	private boolean type;

	/** 文件选择框基于的界面 */
	private DesignerProcessOperationIntrusDialog processOperationIntrusDialog;

	public FlowFileTable(DesignerProcessOperationIntrusDialog processOperationIntrusDialog,
			List<JecnActivityShowBean> listAllActivityShow, boolean type) {
		this.processOperationIntrusDialog = processOperationIntrusDialog;
		this.listAllActivityShow = listAllActivityShow;
		this.type = type;
		setTableModel();
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
	}

	public void setTableModel() {
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		hiddenCols();
	}

	public FlowFileTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("fileNumbers"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("fileName"));
		title.add("type");
		title.add("figureUIID");
		return new FlowFileTableMode(getContent(), title);
	}

	public FlowFileTableMode getTableModel(Vector<Vector<String>> fileContent) {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("fileNumbers"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("fileName"));
		title.add("type");
		title.add("figureUIID");
		return new FlowFileTableMode(fileContent, title);
	}

	public void hiddenCols() {
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {
		return new int[] { 0, 3, 4 };
	}

	class FlowFileTableMode extends DefaultTableModel {
		public FlowFileTableMode(Vector<Vector<String>> vector, Vector<String> title) {
			super(vector, title);
		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	/**
	 * 初始化数据显示
	 * 
	 * @author fuzhh Oct 31, 2012
	 * @return
	 */
	public Vector<Vector<String>> getContent() {
		if (type) {
			// 流程记录
			Vector<Vector<String>> flowFileContent = new Vector<Vector<String>>();
			// 存储文件ID
			Set<Long> fileIdList = new HashSet<Long>();
			for (JecnActivityShowBean activityShowBean : listAllActivityShow) {
				// 输出
				for (JecnModeFileT modeFileT : activityShowBean.getActiveFigure().getFlowElementData()
						.getListModeFileT()) {
					if (!fileIdList.contains(modeFileT.getFileMId())) {
						Vector<String> flowFileContentStr = new Vector<String>();
						// 文件ID
						flowFileContentStr.add(String.valueOf(modeFileT.getFileMId()));
						// 文件编号
						flowFileContentStr.add(modeFileT.getFileNumber());
						// 文件名称
						flowFileContentStr.add(modeFileT.getModeName());
						// 类型输出
						flowFileContentStr.add("output");
						// 图形的UIID
						flowFileContentStr.add(activityShowBean.getActiveFigure().getFlowElementData()
								.getFlowElementUUID());
						flowFileContent.add(flowFileContentStr);
						fileIdList.add(modeFileT.getFileMId());
					}
				}
				// 输入 和 操作规范
				for (JecnActivityFileT activityFileT : activityShowBean.getActiveFigure().getFlowElementData()
						.getListJecnActivityFileT()) {
					// 判断为输入
					if (activityFileT.getFileType() != null && activityFileT.getFileType().intValue() == 0) {
						if (!fileIdList.contains(activityFileT.getFileSId())) {
							Vector<String> flowFileContentStr = new Vector<String>();
							// 文件ID
							flowFileContentStr.add(String.valueOf(activityFileT.getFileSId()));
							// 文件编号
							flowFileContentStr.add(activityFileT.getFileNumber());
							// 文件名称
							flowFileContentStr.add(activityFileT.getFileName());
							// 类型输入
							flowFileContentStr.add("input");
							// 图形的UIID
							flowFileContentStr.add(String.valueOf(activityShowBean.getActiveFigure()
									.getFlowElementData().getFlowElementId()));
							flowFileContent.add(flowFileContentStr);
							fileIdList.add(activityFileT.getFileSId());
						}
					}
				}
			}
			return flowFileContent;
		} else {
			// 操作规范
			Vector<Vector<String>> practicesContent = new Vector<Vector<String>>();
			// 存储文件ID
			Set<Long> fileIdList = new HashSet<Long>();
			for (JecnActivityShowBean activityShowBean : listAllActivityShow) {
				// 输入 和 操作规范
				for (JecnActivityFileT activityFileT : activityShowBean.getActiveFigure().getFlowElementData()
						.getListJecnActivityFileT()) {
					// 判断为操作规范
					if (activityFileT.getFileType() != null && activityFileT.getFileType().intValue() == 1) {
						if (!fileIdList.contains(activityFileT.getFileSId())) {
							Vector<String> practicesContentStr = new Vector<String>();
							// 文件ID
							practicesContentStr.add(String.valueOf(activityFileT.getFileSId()));
							// 文件编号
							practicesContentStr.add(activityFileT.getFileNumber());
							// 文件名称
							practicesContentStr.add(activityFileT.getFileName());
							// 类型操作规范
							practicesContentStr.add("practicesContent");
							// 图形的UIID
							practicesContentStr.add(String.valueOf(activityShowBean.getActiveFigure()
									.getFlowElementData().getFlowElementId()));

							practicesContent.add(practicesContentStr);
							fileIdList.add(activityFileT.getFileSId());
						}
					}
				}
			}
			return practicesContent;
		}
	}
}
