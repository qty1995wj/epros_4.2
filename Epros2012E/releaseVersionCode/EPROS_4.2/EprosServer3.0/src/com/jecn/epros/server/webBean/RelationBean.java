package com.jecn.epros.server.webBean;
/**
 * @author yxw 2012-11-26
 * @description：应用于只显示两列信息的表格
 */
public class RelationBean {
	/**主键*/
	private long id;
	/**名称1*/
	private String nameOne;
	/**名称2*/
	private String nameTwo;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNameOne() {
		return nameOne;
	}
	public void setNameOne(String nameOne) {
		this.nameOne = nameOne;
	}
	public String getNameTwo() {
		return nameTwo;
	}
	public void setNameTwo(String nameTwo) {
		this.nameTwo = nameTwo;
	}
	
}
