package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.file.FileContent;
import com.jecn.epros.domain.process.SearchBean;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.Contants;
import com.jecn.epros.util.Utils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class FileSqlProvider extends BaseSqlProvider {

    public String fetchAttachmentByIdSql(Long id) {
        return new SQL() {
            {
                SELECT("jff.FILE_NAME as name,fc.FILE_STREAM as content");
                FROM("JECN_FILE_T jff");
                INNER_JOIN("FILE_CONTENT fc on jff.FILE_ID = fc.ID");
                WHERE("jff.FILE_ID=#{id}");
            }
        }.toString();
    }

    public String findFileNameByIdSql(Map<String, Object> map) {
        Long id = (Long) map.get("id");
        String isPub = map.get("isPub").toString();
        return "SELECT FILE_NAME FROM JECN_FILE" + isPub + " WHERE FILE_ID = " + id;
    }

    public String saveFileContent(FileContent curContent) {

        String sql = "";
        String id = "";
        String value = "";
        if (isOracle()) {
            id = "id,";
            value = "#{id},";
        }
        sql = "insert into FILE_CONTENT(" + id + "FILE_ID," + "FILE_STREAM," + "FILE_PATH," + "UPDATE_TIME,"
                + "UPDATE_PEOPLE_ID," + "FILE_NAME," + "type," + "IS_VERSION_LOCAL)" + " values(" + value
                + "#{id},#{fileStream,jdbcType=BLOB},#{filePath},#{updateTime},#{updatePeopleId},#{fileName},#{type},#{isVersionLocal}"
                + ")";
        return sql;
    }

    /**
     * 树展开
     *
     * @param map
     * @return
     */
    public String findChildFiles(Map<String, Object> map) {
        Long pId = Long.valueOf(map.get("pid").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "";
        sql = sql + "select distinct t.file_id,t.file_name,t.per_file_id,t.is_dir,"
                + "case when jft.file_id is null then 0 else 1 end as count,t.sort_id" + " from jecn_file t"
                + " LEFT JOIN jecn_file jft on t.file_id = jft.per_file_id and jft.del_state=0";
        sql = sql + " where t.del_state=0 and t.per_file_id= " + pId + " and t.project_id=" + projectId
                + " and t.hide=1 order by t.per_file_id,t.sort_id,t.file_id";
        return sql;
    }

    private Long getHistoryId(Map<String, Object> map) {
        return Long.valueOf(map.get("historyId").toString());
    }

    /**
     * 文件基本信息
     *
     * @param map
     * @return
     */
    public String findBaseInfo(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        String isPub = map.get("isPub").toString();
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == null || historyId == 0) {
            sql = "select jf.version_id file_id,jf.file_name,jf.doc_id,jfs.flow_id,jfs.flow_name,jfo.org_id,jfo.org_name"
                    + "    ,ju.true_name,jf.create_time,jf.pub_time,jf.is_public,jf.CONFIDENTIALITY_LEVEL,jf.commissioner_id,jf.keyword,juf.true_name COMMISSIONER_NAME" +
                    " ,jf.res_people_id resPeopleId, jf.type_res_people typeResPeople" +
                    " from" + "    jecn_file" + isPub + " jf"
                    + "    left join jecn_flow_structure" + isPub + " jfs on jfs.flow_id = jf.flow_id and jfs.del_state=0"
                    + "    left join jecn_flow_org jfo on jfo.org_id = jf.org_id and jfo.del_state=0"
                    + "    left join jecn_user ju on ju.people_id = jf.project_id and ju.islock=0"
                    + "    left join jecn_user juf " +
                    "      on juf.people_id = jf.commissioner_id where jf.file_id=" + id;
        } else {
            sql = findHistoryBaseInfo(map);
        }
        return sql;
    }

    /**
     * 文件基本信息 文控
     *
     * @return
     */
    public String findHistoryBaseInfo(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        String sql = "select fi.id file_id," +
                "       jf.file_name," +
                "       jf.doc_id," +
                "       jfs.flow_id," +
                "       jfs.flow_name," +
                "       jfo.org_id," +
                "       jfo.org_name," +
                "       ju.true_name," +
                "       jf.create_time," +
                "       jf.pub_time," +
                "       jf.is_public," +
                "       jf.CONFIDENTIALITY_LEVEL," +
                "       jf.commissioner_id," +
                "       jf.keyword," +
                "       juf.true_name COMMISSIONER_NAME  ,jf.res_people_id resPeopleId, jf.type_res_people typeResPeople " +
                "  from jecn_file_h   jf" +
                "  inner join  file_content fi" +
                "  on jf.version_id = fi.id" +
                "  left join jecn_flow_structure" + isPub + " jfs" +
                "    on jfs.flow_id = jf.flow_id " +
                "   and jfs.del_state = 0" +
                "  left join jecn_flow_org jfo" +
                "    on jfo.org_id = jf.org_id" +
                "   and jfo.del_state = 0" +
                "  left join jecn_user ju" +
                "    on ju.people_id = jf.project_id" +
                "   and ju.islock = 0" +
                "  left join jecn_user juf " +
                "   on juf.people_id = jf.commissioner_id " +
                "   where jf.file_id = #{id}" +
                "   and jf.history_id = #{historyId}";
        return sql;
    }


    /**
     * 文件使用详情
     *
     * @return
     */
    public String findFileUseDetail(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        String condition = "";
        if (map.get("id") != null) {
            condition = " =" + map.get("id");
        } else if (map.get("ids") != null) {//ids格式为（1,2,3,4）
            condition = " in " + map.get("ids");
        }
        if (StringUtils.isNotBlank(isPub)) {// 文件任务审批，相关文件只显示相关风险和相关标准
            return getFileRelatedFiles(condition, isPub);
        }
        return "SELECT DISTINCT A.ID,A.NAME,A.TYPE FROM (SELECT JFS.FLOW_ID AS ID, JFS.FLOW_NAME AS NAME, CASE WHEN JFS.ISFLOW=0 THEN 0 ELSE 1 END AS TYPE"
                + "   FROM  jecn_flow_structure_image" + isPub + " jfsi,jecn_flow_structure" + isPub + " jfs"
                + "   WHERE jfsi.flow_id =jfs.flow_id and jfs.DEL_STATE=0 and jfsi.figure_type = 'IconFigure' and jfsi.link_flow_id  "
                + condition
                + " UNION " +
                "SELECT JMF.FLOW_ID AS ID,JFS.FLOW_NAME AS NAME,0 AS TYPE"
                + "  from jecn_main_flow" + isPub + " jmf,jecn_flow_structure" + isPub + " jfs"
                + "  where jmf.flow_id=jfs.flow_id and jfs.del_state=0 and jmf.file_id " + condition
                + " UNION "
                + "SELECT JFS.FLOW_ID AS ID, JFS.FLOW_NAME AS NAME, CASE WHEN JFS.ISFLOW=0 THEN 0 ELSE 1 END AS TYPE"
                + "   FROM JECN_FIGURE_FILE" + isPub + " JFF"
                + " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + isPub + " jfsi ON jff.figure_id = jfsi.figure_id"
                + " INNER JOIN JECN_FLOW_STRUCTURE" + isPub + " jfs ON jfs.flow_id = jfsi.flow_id"
                + "   WHERE jfs.DEL_STATE=0 and jff.FILE_ID " + condition
                + " UNION "
                + " SELECT JFBI.FLOW_ID AS ID,JFS.FLOW_NAME AS NAME,1 AS TYPE"
                + "  from jecn_flow_basic_info" + isPub + " jfbi,jecn_flow_structure" + isPub + " jfs"
                + "  where jfbi.flow_id=jfs.flow_id and jfs.del_state=0 and jfbi.file_id " + condition
                + " UNION "
                + " SELECT JFS.FLOW_ID AS ID,JFS.FLOW_NAME AS NAME,1 AS TYPE"
                + "  from jecn_flow_structure_image" + isPub + " jfsi,jecn_flow_structure" + isPub + " jfs,JECN_ACTIVITY_FILE" + isPub + " jaf"
                + "  where jaf.figure_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id and jfs.del_state=0 and jaf.file_s_id"
                + condition
                + " UNION "
                + " SELECT JFS.FLOW_ID AS ID,JFS.FLOW_NAME AS NAME,1 AS TYPE FROM"
                + "  jecn_flow_structure_image" + isPub + " jfsi,jecn_flow_structure" + isPub + " jfs,JECN_MODE_FILE" + isPub + " jmf"
                + "  where jmf.figure_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id and jfs.del_state=0 and jmf.file_m_id"
                + condition
                + " UNION "
                + "  SELECT JFS.FLOW_ID AS ID,JFS.FLOW_NAME AS NAME,1 AS TYPE"
                + "  from jecn_flow_structure_image" + isPub + " jfsi,jecn_flow_structure" + isPub + " jfs,jecn_mode_file" + isPub + " jmf,jecn_templet" + isPub + " jt"
                + "  where jt.mode_file_id=jmf.mode_file_id and jmf.figure_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id and jfs.del_state=0 and jt.file_id"
                + condition
                + " UNION " +
                "  SELECT JR.ID AS ID,JR.RULE_NAME AS NAME,2 AS TYPE"
                + "  from jecn_rule" + isPub + " jr where jr.del_state=0 and jr.is_dir=2 and jr.is_file_local=0 and jr.file_id" + condition
                + " UNION "
                + "  SELECT JR.ID AS ID,JR.RULE_NAME AS NAME,2 AS TYPE"
                + "  from jecn_rule" + isPub + " jr,rule_title" + isPub + " rt,rule_file" + isPub + " rf"
                + "  where rf.rule_title_id=rt.id and jr.del_state = 0 and rt.rule_id = jr.id and rf.rule_file_id" + condition
                + " UNION "
                + "  SELECT JCC.CRITERION_CLASS_ID AS ID,JCC.CRITERION_CLASS_NAME AS NAME,3 AS TYPE"
                + "  from jecn_criterion_classes jcc where jcc.stan_type=1 and jcc.related_id" + condition
                + " UNION "
                + "  SELECT JFO.ORG_ID AS ID,JFO.ORG_NAME AS NAME,4 AS TYPE"
                + "  from jecn_flow_org_image jfoi,jecn_flow_org jfo"
                + "  where jfoi.figure_type='IconFigure' and jfoi.org_id=jfo.org_id and jfo.del_state=0 and jfoi.link_org_id"
                + condition
                + " UNION " +
                "  SELECT PF.FIGURE_ID AS ID, FOI.FIGURE_TEXT AS NAME, 5 AS TYPE" +
                "  FROM JECN_POSITION_FIG_INFO PF" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE FOI" +
                "    ON FOI.FIGURE_ID = PF.FIGURE_ID" +
                " WHERE PF.FILE_ID " + condition +
                " UNION " +
                getFileRelatedFiles(condition, isPub) +
                " UNION" +
                " SELECT jfs.flow_id as id,jfs.flow_name as name,case when jfs.isflow=0 then 0 else 1 end as type " +
                "	FROM JECN_FLOW_STRUCTURE" + isPub + " jfs" +
                "	INNER JOIN FLOW_STANDARDIZED_FILE" + isPub + " A ON jfs.FLOW_ID=A.FLOW_ID and jfs.del_state = 0" +
                " where a.file_id " + condition +
                "        UNION" +
                "        SELECT DISTINCT JF.FLOW_ID AS ID," +
                "                        JF.FLOW_NAME AS NAME," +
                "                        CASE" +
                "                          WHEN JF.ISFLOW = 0 THEN" +
                "                           0" +
                "                          ELSE" +
                "                           1" +
                "                        END AS TYPE" +
                "          FROM JECN_FIGURE_IN_OUT" + isPub + " INOUT" +
                "         INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + isPub + " IMF" +
                "            ON INOUT.FIGURE_ID = IMF.FIGURE_ID" +
                "         INNER JOIN JECN_FLOW_STRUCTURE" + isPub + " JF" +
                "            ON JF.FLOW_ID = IMF.FLOW_ID" +
                "           AND JF.DEL_STATE = 0" +
                "           AND INOUT.FILE_ID  " + condition +
                "        UNION" +
                "        SELECT DISTINCT JF.FLOW_ID AS ID," +
                "                        JF.FLOW_NAME AS NAME," +
                "                        CASE" +
                "                          WHEN JF.ISFLOW = 0 THEN" +
                "                           0" +
                "                          ELSE" +
                "                           1" +
                "                        END AS TYPE" +
                "          FROM JECN_FIGURE_IN_OUT" + isPub + " INOUT" +
                "         INNER JOIN JECN_FIGURE_IN_OUT_SAMPLE" + isPub + " SAM" +
                "            ON INOUT.ID = SAM.IN_OUT_ID" +
                "         INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + isPub + " IMF" +
                "            ON INOUT.FIGURE_ID = IMF.FIGURE_ID" +
                "         INNER JOIN JECN_FLOW_STRUCTURE" + isPub + " JF" +
                "            ON JF.FLOW_ID = IMF.FLOW_ID" +
                "           AND JF.DEL_STATE = 0" +
                "         WHERE SAM.FILE_ID " + condition +
                " )A ORDER BY A.TYPE";
    }

    private String getFileRelatedFiles(String condition, String isPub) {
        return "	select jr.id as id,jr.name as name,6 as type from FILE_RELATED_RISK" + isPub + " a" +
                "	INNER join JECN_RISK jr on jr.id=a.risk_id" +
                " where a.file_id " + condition +
                " UNION " +
                "	select jcc.criterion_class_id as id,jcc.criterion_class_name as name,3 as type from FILE_RELATED_STANDARD" + isPub + " a" +
                "	inner join jecn_criterion_classes jcc on jcc.criterion_class_id=a.standard_id" +
                " where a.file_id " + condition;
    }


    /**
     * 文件搜索 总数
     *
     * @param map
     * @return
     */
    public String searchFileTotal(Map<String, Object> map) {
        return " select count(distinct t.file_id) from jecn_file t " + getFileSearchCommon(map);
    }

    public String searchFile(Map<String, Object> map) {
        return " select t.file_id,t.file_name,t.doc_id,t.org_id,org.org_name,t.is_public,t.pub_time from jecn_file t "
                + getFileSearchCommon(map);
    }

    /**
     * 文件搜索 公共部分
     *
     * @param map
     * @return
     */
    private String getFileSearchCommon(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchBean searchBean = (SearchBean) map.get("searchBean");
        boolean isAdmin = (boolean) map.get("isAdmin");
        String sql = " left join jecn_flow_org org on org.del_state=0 and org.org_id=t.org_id";
        if (!isAdmin && !Contants.isFileAdmin) {
            map.put("typeAuth", AuthSqlConstant.TYPEAUTH.FILE);
            sql = sql + "  inner join " + CommonSqlTPath.INSTANCE.getFileSearch(map)
                    + " on MY_AUTH_FILES.file_id=t.file_id";
        }
        sql = sql + " where t.del_state=0 and t.project_id=" + projectId + " and t.is_dir=1 and t.hide = 1";
        // 文件名称
        if (StringUtils.isNotBlank(searchBean.getName())) {
            String[] fileName = searchBean.getName().split(" ");
            String lsql = "";
            int namecount = 0;
            for (int f = 0; f < fileName.length; f++) {
                // 文件名称
                if (fileName[f].trim() != null && !"".equals(fileName[f].trim())) {
                    if (namecount == 0) {
                        lsql = lsql + "select file_name from jecn_file where file_name like '%" + fileName[f].trim()
                                + "%' ";
                    } else {
                        lsql = lsql + " or (file_name like '%" + fileName[f].trim() + "%'" + " or doc_id like '%" + fileName[f].trim() + "%'" + " or KEYWORD like '%" + fileName[f].trim() + "%')";
                    }
                    namecount++;
                }
            }
            sql = sql + " and t.file_name in (" + lsql + ") ";
        }

        // 责任部门
        if (searchBean.getOrgId() != null) {
            sql = sql + " and org.org_id =" + searchBean.getOrgId();
        }

        // 密级
        if (searchBean.getSecret() != -1) {
            sql = sql + " and t.is_public=" + searchBean.getSecret();
        }
        return sql;
    }


    public String fetchInventory(Map<String, Object> paramMap) {
        String pub = Utils.getPubByParams(paramMap);
        String sql = getFiles(paramMap);
        sql += " SELECT f.*,ju.true_name as creator,jfo.org_name,jfo.org_id," +
                "              case" +
                "         when f.type_res_people = 0 then" +
                "          u.true_name" +
                "         else" +
                "          pos.figure_text" +
                "       end res_name,ff.id,ff.name,ff.type, " +
                " case when jf.file_id is not null then 1" +
                " when jtbn.id is not null then 2 else 0 end as pub_state" +
                " FROM FILES f " +
                " LEFT JOIN JECN_USER JU ON F.PEOPLE_ID=JU.PEOPLE_ID " +
                " left join jecn_task_bean_new jtbn on jtbn.R_ID=f.file_id and jtbn.TASK_TYPE=1 and jtbn.state<>5 " +
                " left join jecn_file jf on jf.file_id=f.file_id and jf.del_state = 0"
                + " left join jecn_flow_org jfo on f.org_id=jfo.org_id" +
                "     left join jecn_user u" +
                "    on f.RES_PEOPLE_ID = u.people_id" +
                "   and u.islock = 0" +
                "   and f.type_res_people = 0" +
                "  left join jecn_flow_org_image pos" +
                "    on f.res_people_id = pos.figure_id" +
                "   and f.type_res_people = 1"
                + " left join ("
                + " SELECT distinct jfs.flow_id as id, jfs.flow_name as name, case when jfs.isflow=0 then 0 else 1 end as type,f.file_id"
                + "   FROM  jecn_flow_structure_image" + pub + " jfsi,jecn_flow_structure" + pub + " jfs " + ",files f "
                + "   WHERE jfsi.flow_id =jfs.flow_id and jfs.DEL_STATE=0 and jfsi.figure_type = 'IconFigure' and jfsi.link_flow_id  "
                + "=f.file_id"
                + " UNION " +
                "select distinct jmf.flow_id as id,jfs.flow_name as name,0 as type,f.file_id"
                + "  from jecn_main_flow" + pub + " jmf,jecn_flow_structure" + pub + " jfs" + ",files f "
                + "  where jmf.flow_id=jfs.flow_id and jfs.del_state=0 and jmf.file_id " + "=f.file_id"
                + " UNION "
                + "SELECT distinct jfs.flow_id as id, jfs.flow_name as name, case when jfs.isflow=0 then 0 else 1 end as type,f.file_id"
                + "   FROM JECN_FIGURE_FILE" + pub + " JFF INNER JOIN files f ON jff.FILE_ID =f.file_id"
                + " INNER JOIN jecn_flow_structure_image" + pub + " jfsi ON jff.figure_id = jfsi.figure_id"
                + " INNER JOIN jecn_flow_structure" + pub + " jfs ON jfs.flow_id = jfsi.flow_id"
                + "   WHERE jfs.DEL_STATE=0 "
                + " UNION "
                + " select distinct jfbi.flow_id as id,jfs.flow_name as name,1 as type,f.file_id"
                + "  from jecn_flow_basic_info" + pub + " jfbi,jecn_flow_structure" + pub + " jfs" + ",files f "
                + "  where jfbi.flow_id=jfs.flow_id and jfs.del_state=0 and jfbi.file_id " + "=f.file_id"
                + " UNION "
                + " select distinct jfs.flow_id as id,jfs.flow_name as name,1 as type,f.file_id"
                + "  from jecn_flow_structure_image" + pub + " jfsi,jecn_flow_structure" + pub + " jfs,jecn_activity_file" + pub + " jaf" + ",files f "
                + "  where jaf.figure_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id and jfs.del_state=0 and jaf.file_s_id"
                + "=f.file_id"
                + " UNION "
                + " select distinct jfs.flow_id as id,jfs.flow_name as name,1 as type,f.file_id from"
                + "  jecn_flow_structure_image" + pub + " jfsi,jecn_flow_structure" + pub + " jfs,jecn_mode_file" + pub + " jmf" + ",files f "
                + "  where jmf.figure_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id and jfs.del_state=0 and jmf.file_m_id"
                + "=f.file_id"
                + " UNION "
                + "  select distinct jfs.flow_id as id,jfs.flow_name as name,1 as type,f.file_id"
                + "  from jecn_flow_structure_image" + pub + " jfsi,jecn_flow_structure" + pub + " jfs,jecn_mode_file" + pub + " jmf,jecn_templet" + pub + " jt" + ",files f "
                + "  where jt.mode_file_id=jmf.mode_file_id and jmf.figure_id = jfsi.figure_id and jfsi.flow_id=jfs.flow_id and jfs.del_state=0 and jt.file_id"
                + "=f.file_id"
                + " UNION " +
                "  select distinct jr.id as id,jr.rule_name as name,2 as type,f.file_id"
                + "  from jecn_rule" + pub + " jr,files f  where jr.is_dir=2 and jr.del_state =0 and jr.is_file_local=0 and jr.file_id" + "=f.file_id"
                + " UNION "
                + "  select distinct jr.id as id,jr.rule_name as name,2 as type,f.file_id"
                + "  from jecn_rule" + pub + " jr,rule_title" + pub + " rt,rule_file" + pub + " rf" + ",files f "
                + "  where rf.rule_title_id=rt.id and jr.del_state =0 and rt.rule_id = jr.id and rf.rule_file_id" + "=f.file_id"
                + " UNION "
                + "  select distinct jcc.criterion_class_id as id,jcc.criterion_class_name as name,3 as type,f.file_id"
                + "  from jecn_criterion_classes jcc,files f where jcc.stan_type=1 and jcc.related_id" + "=f.file_id"
                + " UNION "
                + "  select distinct jfo.org_id as id,jfo.org_name as name,4 as type,f.file_id"
                + "  from jecn_flow_org_image jfoi,jecn_flow_org jfo" + ",files f "
                + "  where jfoi.figure_type='IconFigure' and jfoi.org_id=jfo.org_id and jfo.del_state=0 and jfoi.link_org_id"
                + "=f.file_id"
                + " UNION " +
                "  select distinct PF.FIGURE_ID as id, FOI.FIGURE_TEXT as name, 5 as type,f.file_id" +
                "  FROM JECN_POSITION_FIG_INFO PF "
                + " INNER JOIN files f ON PF.FILE_ID=f.file_id" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE FOI" +
                "    ON FOI.FIGURE_ID = PF.FIGURE_ID " +
                " UNION " +
                "	select distinct jr.id as id,jr.name as name,6 as type,f.file_id from FILE_RELATED_RISK" + pub + " a" +
                "	INNER JOIN files f on a.file_id=f.file_id" +
                "	INNER join JECN_RISK jr on jr.id=a.risk_id" +
                " UNION" +
                "	select distinct jcc.criterion_class_id as id,jcc.criterion_class_name as name,3 as type,f.file_id from FILE_RELATED_STANDARD" + pub + " a" +
                "	INNER JOIN files f on a.file_id=f.file_id" +
                "	inner join jecn_criterion_classes jcc on jcc.criterion_class_id=a.standard_id" +
                " UNION" +
                " SELECT distinct jfs.flow_id as id,jfs.flow_name as name,case when jfs.isflow=0 then 0 else 1 end as type,f.file_id " +
                "	FROM JECN_FLOW_STRUCTURE" + pub + " jfs" +
                "	INNER JOIN FLOW_STANDARDIZED_FILE" + pub + " A ON jfs.FLOW_ID=A.FLOW_ID AND JFS.DEL_STATE = 0" +
                "	INNER JOIN files f ON A.FILE_ID = f.FILE_ID" +
                ")ff on f.file_id=ff.file_id order by f.view_sort asc";
        return sql;
    }

    private String getFiles(Map<String, Object> paramMap) {
        String sql = "WITH FILES AS (" +
                InventorySqlProvider.getFiles(paramMap) +
                ")";
        return sql;
    }

    public String fetchInventoryParent(Map<String, Object> map) {
        String pub = Utils.getPubByParams(map);
        String sql = "select p.file_id,p.per_file_id,p.file_name,p.is_dir,p.t_path,p.t_level " +
                "	from jecn_file" + pub + " c" +
                "	inner join jecn_file" + pub + " p on c.file_id=#{id} and c.file_id<>p.file_id and c.t_path is not null and p.t_path is not null and c.t_path like p.t_path" + BaseSqlProvider.getConcatChar() + "'%'";
        return sql;
    }

    /**
     * 查询文件状态
     *
     * @param map
     * @return
     */
    public String judgmentFileExists(Map<String, Object> map) {
        String pub = Utils.getPubByParams(map);
        String sql = "select del_state from jecn_file" + pub + " t where  t.version_id = #{fileId}";
        return sql;
    }

}
