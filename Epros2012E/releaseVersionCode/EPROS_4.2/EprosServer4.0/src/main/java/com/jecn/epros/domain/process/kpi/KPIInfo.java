package com.jecn.epros.domain.process.kpi;

/**
 * 流程KPI 详细信息
 *
 * @author ZXH
 */
public class KPIInfo {
    private String title;
    private String value;
    /**
     * textarea文本域
     */
    private String XType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getXType() {
        return XType;
    }

    public void setXType(String xType) {
        XType = xType;
    }

}
