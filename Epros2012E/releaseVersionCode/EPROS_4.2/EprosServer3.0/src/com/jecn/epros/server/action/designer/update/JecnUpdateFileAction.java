package com.jecn.epros.server.action.designer.update;

import com.jecn.epros.server.bean.system.JecnVersionSys;

/**
 * 获取版本信息 接口
 * @author fuzhh Apr 9, 2013
 *
 */
public interface JecnUpdateFileAction {
	/** zip文件名称 */
	public static final String zipName="updateDesigner.zip";
	/** exe文件名称 */
	public static final String exeName = "designer.exe";
	
	/**
	 * 读取本地 配置文件
	 * @author fuzhh Apr 7, 2013
	 */
	public JecnVersionSys readProperties()throws Exception;
	
    /**
     * 读取更新的压缩包
     * @author fuzhh Apr 7, 2013
     * @return
     * @throws Exception
     */
	public byte[] readUpdateZip()throws Exception;
	
	 /**
     * 读取更新的exe文件
     * @author fuzhh Apr 7, 2013
     * @return
     * @throws Exception
     */
	public byte[] readUpdateExe()throws Exception;
	
}
