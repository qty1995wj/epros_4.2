package com.jecn.epros.server.action.web.login.ad.datangyidong;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.cetc29.JecnCetc29AfterItem;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnPath;

public class DaTangYiDongAfterItem {


	private static Logger log = Logger.getLogger(JecnCetc29AfterItem.class);

	// Ldap 服务器地址
	public static String LDAP_URL = null;
	// 域名 1
	public static String DOMAIN1 = null;
	// 域名 2
	public static String DOMAIN2= null;
	// 域名 3
	public static String DOMAIN3 = null;
	// 岗位匹配时使用的Email后缀
	public static String EMAIL = null;
	
	public static void start() {

		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH
				+ "cfgFile/datangyidong/daTangYiDongLdapServerConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			// Ldap 服务器地址
			String ldapURL = props.getProperty("ldapURL");
			if (!JecnCommon.isNullOrEmtryTrim(ldapURL)) {
				DaTangYiDongAfterItem.LDAP_URL = ldapURL;
			}

			// Ldap 域名
			DaTangYiDongAfterItem.DOMAIN1 =  props.getProperty("domain1");
			DaTangYiDongAfterItem.DOMAIN2 =  props.getProperty("domain2");
			DaTangYiDongAfterItem.DOMAIN3 =  props.getProperty("domain3");

			// Email 后缀
			String email = props.getProperty("email");
			if (!JecnCommon.isNullOrEmtryTrim(email)) {
				DaTangYiDongAfterItem.EMAIL = email;
			}

		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
			}
		}
	}
}
