package epros.draw.data.figure;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 
 * 泳池数据类
 * 
 * @author Administrator
 *
 */
public class JecnModeFigureData extends JecnFigureData {
	/** 记录泳池分割点X坐标(目前为横向图) */
	private int dividingX = 30;

	public JecnModeFigureData(MapElemType mapElemType) {
		super(mapElemType);
	}

	public int getDividingX() {
		return dividingX;
	}

	public void setDividingX(int dividingX) {
		this.dividingX = dividingX;
	}

	/**
	 * 
	 * 实例化对象
	 * 
	 */
	public JecnModeFigureData newInstance() {
		return new JecnModeFigureData(mapElemType);
	}

	@Override
	/**
	 * 
	 * 克隆对象
	 * 
	 */
	public JecnModeFigureData clone() {
		return (JecnModeFigureData) this.currClone();
	}

	/**
	 * 
	 * 
	 * 复制流程元素属性
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(
			JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		if (!(srcFlowElementData instanceof JecnModeFigureData)
				|| !(destFlowElementData instanceof JecnModeFigureData)) {
			return;
		}

		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		JecnModeFigureData src = (JecnModeFigureData) srcFlowElementData;
		JecnModeFigureData dest = (JecnModeFigureData) destFlowElementData;

		dest.setDividingX(src.getDividingX());
	}

	/**
	 * 设置当前面板放大缩小倍数下的泳池分割点值
	 * 
	 * @param zoomDividingX
	 *            当前面板下泳池分割点
	 */
	public void setZommDividingX(int zoomDividingX) {
		// 获取当前面板放大缩小倍数分割点X坐标
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();

		// 获取原始状态下
		int orgDividing = DrawCommon.convertDoubleToInt(zoomDividingX / scale);

		this.setDividingX(orgDividing);
	}
}
