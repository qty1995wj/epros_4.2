package com.jecn.webservice.wanhua.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebService;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.dataImport.sync.JecnOrgView;
import com.jecn.webservice.jbshihua.service.DeptService;
import com.jecn.webservice.wanhua.IWanHuaOADeptService;
import com.jecn.webservice.wanhua.bean.WanHuaDept;

@WebService
public class WanHuaOADeptServiceImpl implements IWanHuaOADeptService {
	private Logger log = Logger.getLogger(WanHuaOADeptServiceImpl.class);

	@Override
	@WebMethod
	public String saveDepts(List<WanHuaDept> depts) {
		Map<String, String> map = new HashMap<String, String>();
		if (depts == null || depts.size() == 0) {
			map.put("status", "false");
			map.put("result", "部门不能为空！");
			JSONArray jsonArray = JSONArray.fromObject(map);
			String json = jsonArray.toString();
			log.error(json);
			return json;
		}
		log.info("获取部门总数：" + depts.size());
		// 根据数据对象获取代保存的数据
		List<JecnOrgView> saveDepts = new ArrayList<JecnOrgView>();
		JecnOrgView deptInfo = null;
		for (WanHuaDept dept : depts) {
			deptInfo = new JecnOrgView();
			deptInfo.setOrgNum(dept.getOrgId());
			deptInfo.setOrgName(dept.getOrgName());
			deptInfo.setPerOrgNum(dept.getPOrgId());
			deptInfo.setOrgStatus("i".equals(dept.getChangeType().toLowerCase()) ? "0" : "2");

			saveDepts.add(deptInfo);
		}

		DeptService deptService = (DeptService) ApplicationContextUtil.getContext().getBean("DeptServiceImpl");

		int result = -1;
		try {
			// 获取已存在的部门
			List<JecnOrgView> existsDepts = deptService.getAllDepts();
			// 批量修改 数据状态
			if (existsDepts != null && existsDepts.size() > 0) {
				for (JecnOrgView existDept : existsDepts) {
					for (JecnOrgView saveDept : saveDepts) {
						if (existDept.getOrgNum().equals(saveDept.getOrgNum())) {
							// 部门更新
							saveDept.setOrgStatus("1");
						}
					}
				}
			}

			log.info("带存储部门总数：" + saveDepts.size());
			result = deptService.recevieOrg(saveDepts);
			map.put("status", 0 == result ? "success" : "false");
			map.put("result", "");
		} catch (Exception e) {
			map.put("status", 0 == result ? "success" : "false");
			map.put("result", e.getMessage());
			log.error(JSONArray.fromObject(map).toString());
			e.printStackTrace();
		}
		return JSONArray.fromObject(map).toString();
	}
}
