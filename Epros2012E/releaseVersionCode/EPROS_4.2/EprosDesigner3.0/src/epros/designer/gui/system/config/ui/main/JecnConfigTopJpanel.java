package epros.designer.gui.system.config.ui.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.draw.gui.swing.JecnPanel;

/**
 * 
 * 北面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigTopJpanel extends JecnPanel {
	/** 信息提示 */
	private JLabel infoLabel = null;

	public JecnConfigTopJpanel(JecnAbtractBaseConfigDialog dialog) {
		initComponents();
	}

	private void initComponents() {
		// 信息提示
		infoLabel = new JLabel();

		Dimension size = new Dimension(100, 25);
		this.setPreferredSize(size);
		this.setMinimumSize(size);

		this.setLayout(new BorderLayout());

		infoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		infoLabel.setVerticalAlignment(SwingConstants.CENTER);
		infoLabel.setBorder(null);
		infoLabel.setOpaque(false);
		// 设置字体
		infoLabel.setFont(new Font(Font.DIALOG, Font.ITALIC, 13));

		this.add(infoLabel);
	}

	public JLabel getInfoLabel() {
		return infoLabel;
	}

	public void setInfo(String text) {
		infoLabel.setText(text);
	}
}
