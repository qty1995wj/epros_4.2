package com.jecn.epros.domain.journal;

import com.jecn.epros.configuration.rpc.RPCServiceConfiguration;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.collections.map.HashedMap;

import java.util.Map;

public class EprosInterfaceMenuWebBean {
    /**
     * 菜单id
     */
    private String code;

    /**
     * 父节点菜单id
     */
    private String parentcode;
    /**
     * 菜单属性
     * 格式:
     * "properties": {
     * "fullPathName": { 全路径名称 以->隔开。
     * "en_US": "B2B系统",
     * "zh_CN": "B2B系统"
     * },
     * "url": "http://fhb2b.fiberhome.com/FHB2B/index.jsp"
     * },
     */
    private Map<String, Object> properties;

    /**
     * 菜单名称
     * "zh_CN": "B2B系统",
     * "en_US": "B2B系统"
     */
    private Map<String, Object> title;
    /**
     * 是否启用
     * enable：启用；disable：不启用。
     */
    private String status;
    /**
     * 是否启用
     * 节点是否隐藏。
     * true：隐藏；
     * false：显示。
     */
    private String hidden;
    /**
     * 节点类型默认是MENU
     */
    private String category;
    /**
     * 节点路径
     * 节点编码路径只到父级节点不含自己，以_隔开。例如S008_002_。是系根节点统编码，详见3系统编码表。
     */
    private String fullpath;
    /**
     * 节点顺序
     * 默认菜单的sortId
     */
    private String priority;
    /**
     * EPROS 继承接口的默认的系统编码 不作为返回数据
     */
    private final String systemCode = "S075";


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    public Map<String, Object> getTitle() {
        return title;
    }

    public void setTitle(Map<String, Object> title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFullpath() {
        return fullpath;
    }

    public void setFullpath(String fullpath) {
        this.fullpath = fullpath;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public String getParentcode() {
        return parentcode;
    }

    public void setParentcode(String parentcode) {
        this.parentcode = parentcode;
    }

    public void setSystemBean() {
        this.code = this.systemCode;
        Map<String, Object> properties = new HashedMap();
        Map<String, Object> fullPathName = new HashedMap();
        fullPathName.put("zh_CN", JecnProperties.getValueByType("processSystemPlatform", 0));
        fullPathName.put("en_US", JecnProperties.getValueByType("processSystemPlatform", 1));
        properties.put("fullPathName", fullPathName);
        properties.put("url", RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL+"login.action");
        this.properties = properties;
        Map<String, Object> name = new HashedMap();
        name.put("zh_CN", JecnProperties.getValueByType("processSystemPlatform", 0));
        name.put("en_US", JecnProperties.getValueByType("processSystemPlatform", 1));
        this.title = name;
        this.status = "enable";
        this.hidden = "false";
        this.category = "MENU";
        this.fullpath = this.systemCode;
        this.priority = "1";
    }

}
