package wordxml.util;

public class Unit {
	// 系统输出分辨率
	public static int dpi = 96;

	/**
	 * 转换厘米为缇 （1厘米约等于567缇）
	 * 
	 * @author weidp
	 * @date 2014-10-17 下午05:49:36
	 * @param value
	 * @return
	 */
	public static String convertCmToTwip(double cm) {
		return (int) (567 * cm) + "";
	}
	/**
	 * 
	 * @param twip
	 * @return
	 */
	public static float convertTwipToCM(int twip) {
		return (float) twip / 567;
	}

	/**
	 * 转换磅为缇 （1磅约等于20缇）
	 * 
	 * @author weidp
	 * @date 2014-10-21 上午09:26:14
	 * @param pt
	 * @return
	 */
	public static String convertPtToTwip(double pt) {
		return (int) (20 * pt) + "";
	}

	/**
	 * 像素转换为缇 1像素=(1/96)*1440 =15 Twip
	 * 
	 * @param pix
	 *            像素值
	 * @return
	 */
	private static int pixToTwip(int pix) {
		return pix * ((1 / dpi) * 1440);
	}

	/**
	 * 像素转换为磅 1 像素 = 0.75 磅
	 * 
	 * @param pix
	 * @return
	 */
	private static double pixToPt(int pix) {
		return pix * 0.75;
	}

	/**
	 * 磅值转换为缇
	 * 
	 * @param pt
	 *            磅值
	 * @return
	 */
	private static double ptToTwip(double pt) {
		return 0;
		// return (pt / 0.75 * ((1 / dpi) * 1440));
	}

	/**
	 * 磅值转换为英尺 一磅约等于七十二分之一英寸
	 * 
	 * @param pt磅
	 * @return
	 */
	private static double ptToFeet(double pt) {
		return pt * (1 / 72);
	}

	/**
	 * 磅值转换为 厘米
	 * 
	 * @param pt
	 *            磅
	 * @return
	 */
	private static double ptToCm(double pt) {
		double feet = pt * (1 / 72);
		return feet * 2.54;
	}

	/**
	 * 厘米转换为缇
	 * 
	 * @param cm
	 *            厘米
	 * @return
	 */
	private static double cmToTwip(double cm) {
		double pt = cmToPt(cm);
		return ptToTwip(pt);
	}

	/**
	 * 毫米转换为 缇
	 * 
	 * @param mm
	 * @return
	 */
	private static double mmToTwip(double mm) {
		double pt = cmToPt(mm / 10);
		return ptToTwip(pt);
	}

	/**
	 * 英寸转换为缇
	 * 
	 * @param feet
	 *            英寸
	 * @return
	 */
	private static double feetToTwip(double feet) {
		double cm = feetToCm(feet);
		return cmToTwip(cm);
	}

	/**
	 * 厘米转换为磅 1cm=28.346磅
	 * 
	 * @param cm
	 *            厘米
	 * @return
	 */
	public static double cmToPt(double cm) {
		return cm * 28.346;
	}

	/**
	 * 英寸转换为磅 一磅约等于七十二分之一英寸 也就是一英寸等于72磅
	 * 
	 * @param feet
	 *            英寸
	 * @return
	 */

	private static double feetToPt(double feet) {
		return feet * 72;
	}

	/**
	 * 英寸转换为厘米 一英寸约为2.54厘米
	 * 
	 * @param feet
	 * @return
	 */
	private static double feetToCm(double feet) {
		return feet * 2.54;
	}

	/**
	 * 行转换为磅
	 * 
	 * @param row行数
	 * @param fontSize
	 *            字体大小磅值
	 * @return
	 */
	private static double rowToPt(int row, int fontSize) {
		return row * fontSize;
	}

	public static void main(String[] args) {
		
		
		
		
		System.out.println(convertCmToTwip(3.97));
		System.out.println(convertCmToTwip(4.58));
		System.out.println(convertCmToTwip(3.43));
		System.out.println(convertCmToTwip(5.53));
	}
}
