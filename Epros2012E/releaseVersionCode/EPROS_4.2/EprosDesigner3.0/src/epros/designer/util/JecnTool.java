package epros.designer.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTreeNode;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class JecnTool {
	private static Logger log = Logger.getLogger(JecnTool.class);

	/**
	 * 删除字符串后面的字符 并在字符串的末尾截取count的长度
	 * 
	 * @param str
	 * @param count
	 * @return
	 */
	public static String dropChars(String str, int count) {
		int len = str.length();
		if (len >= count) {
			return str.substring(0, str.length() - count);
		}
		return str;
	}

	/**
	 * 字符串转换成时间格式
	 * 
	 * @param str
	 *            传入的时间格式字符串
	 * @return
	 */
	public static Date getDateByString(String str) {
		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return dateformat.parse(str);
		} catch (java.text.ParseException e) {
			log.error("JecnTool getDateByString is error", e);
		}
		return null;
	}

	/**
	 * 日期格式转换成时间格式字符串
	 * 
	 * @param date
	 *            传入的时间格式字符串
	 * @return
	 */
	public static String getStringbyDate(Date date) {
		return date == null ? null : new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	/**
	 * 验证阶段是否处于任务中
	 * 
	 * @param treeBean
	 * @return true:节点处于任务中
	 * @throws Exception
	 */
	public static boolean isInTask(JecnTreeBean treeBean) throws Exception {
		return isInTask(treeBean.getId(), treeBean.getTreeNodeType());
	}

	/**
	 * 验证阶段是否处于任务中
	 * 
	 * @param treeBean
	 * @return true:节点处于任务中
	 * @throws Exception
	 */
	public static boolean isInTask(Long id, TreeNodeType treeNodeType) throws Exception {
		boolean isInTask = ConnectionPool.getTaskRecordAction().isInTask(id, treeNodeType);
		if (isInTask) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("isInTask"));
			return true;
		}
		return false;
	}

	public static boolean isTaskIng(Long id, TreeNodeType treeNodeType) throws Exception {
		return ConnectionPool.getTaskRecordAction().isInTask(id, treeNodeType);
	}

	/**
	 * 验证是否可以创建任务
	 * 
	 * @param treeBean
	 * @return true:节点处于任务中
	 * @throws Exception
	 */
	public static boolean submitToTask(JecnTreeBean treeBean) throws Exception {
		boolean isInTask = ConnectionPool.getTaskRecordAction().submitToTask(treeBean.getId(),
				treeBean.getTreeNodeType());
		if (isInTask) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("isInTask"));
			return true;
		}
		return false;
	}

	/**
	 * 重新提交
	 * 
	 * @param treeBean
	 * @return
	 * @throws Exception
	 */
	public static boolean reSubmitToTask(JecnTreeBean treeBean) {
		try {
			return ConnectionPool.getTaskRecordAction().reSubmitToTask(treeBean.getId(), treeBean.getTreeNodeType());
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("isInTask"));
			e.printStackTrace();
			log.error("", e);
		}
		return false;
	}

	/**
	 * 判断是否处于任务审批中
	 * 
	 * @return boolean false 处于任务中
	 */
	public static boolean isBeInTask(JecnTreeNode selectNode) {
		if (selectNode == null) {
			throw new NullPointerException("JecnTool isBeInTask is error");
		}
		return isBeInTask(selectNode.getJecnTreeBean().getId(), selectNode.getJecnTreeBean().getTreeNodeType());
	}

	/**
	 * 判断是否处于任务审批中
	 * 
	 * @return boolean false 处于任务中
	 */
	public static boolean isBeInTask(Long id, TreeNodeType treeNodeType) {
		try {
			boolean isInTask = JecnTool.isInTask(id, treeNodeType);
			if (isInTask) {
				return false;
			}
		} catch (Exception e) {
			log.error("JecnTool isBeInTask is error" + "treeBean.getTreeNodeType() = " + treeNodeType.toString(), e);
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("accessServerError"));
			return false;
		}
		return true;
	}

	/**
	 * 批量删除，存在节点处于审批中验证
	 * 
	 * @param returnValue
	 * @return true 存在节点处于审批中
	 */
	public static boolean exitsNodeInTask(int returnValue) {
		if (returnValue == 2) {
			// 存在节点处于审批中，请查阅！
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("exitsNodeInTask"));
			return true;
		}
		return false;
	}

	/**
	 * 打开文件
	 * 
	 * @param fileId
	 * @return
	 */
	public static boolean openFile(Long fileId) {
		FileOpenBean fileOpenBean = null;
		try {
			fileOpenBean = ConnectionPool.getFileAction().openFile(fileId);
			if (fileOpenBean != null) {
				JecnUtil.openFile(fileOpenBean.getName(), fileOpenBean.getFileByte());
			} else {
				// 文件不存在！
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("fileNotExit"));
				return false;
			}
		} catch (Exception e) {
			log.error("JecnTool openFile is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("accessServerError"));
			return false;
		}
		return true;
	}

	public static boolean isBeInTaskWithTipName(JecnTreeNode selectNode) {
		if (selectNode == null) {
			throw new NullPointerException("JecnTool isBeInTask is error");
		}
		return isBeInTask(selectNode.getJecnTreeBean().getId(), selectNode.getJecnTreeBean().getName(), selectNode
				.getJecnTreeBean().getTreeNodeType());
	}

	private static boolean isBeInTask(Long id, String name, TreeNodeType treeNodeType) {

		try {
			boolean isInTask = ConnectionPool.getTaskRecordAction().isInTask(id, treeNodeType);
			if (isInTask) {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), name + "："
						+ JecnProperties.getValue("isInTask"));
				return true;
			}
		} catch (Exception e) {
			log.error("", e);
		}

		return false;

	}
}
