package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 
 * 风险点
 * 
 * @author Administrator
 * 
 */
public class RiskPointFigure extends JecnBaseFigurePanel {

	private int x1 = 0;
	private int x2 = 0;
	private int x3 = 0;
	private int x4 = 0;
	private int y1 = 0;
	private int y2 = 0;
	private int y3 = 0;
	private int y4 = 0;

	public RiskPointFigure(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		x1 = userWidth / 2;
		y1 = 0;
		x2 = userWidth;
		y2 = userHeight / 2;
		x3 = userWidth / 2;
		y3 = userHeight;
		x4 = 0;
		y4 = userHeight / 2;
		paintFigure(g);
	}

	/**
	 * 无填充色
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { x1, x2, x3, x4 }, new int[] { y1, y2, y3,
				y4 }, 4);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		g2d.setPaint(flowElementData.getShadowColor());
		g2d.fillPolygon(new int[] { x1, x2 - shadowCount, x3 + shadowCount,
				x4 + 2 * shadowCount }, new int[] { y1 + shadowCount,
				y2 + shadowCount, y3, y4 + shadowCount }, 4);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintActiveTop(Graphics2D g2d, int shadowCount) {
		g2d.fillPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount,
				x3 - shadowCount / 2, x4 }, new int[] { y1,
				y2 - shadowCount / 2, y3 - shadowCount, y4 - shadowCount }, 4);
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { x1 - shadowCount / 2, x2 - shadowCount,
				x3 - shadowCount / 2, x4 }, new int[] { y1,
				y2 - shadowCount / 2, y3 - shadowCount, y4 - shadowCount }, 4);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintActiveTop(g2d, 0);
	}

	/**
	 * 阴影 时顶层图像填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintActiveTop(g2d, shadowCount);
	}

	/** ************3D ************ */

	/**
	 * 3D效果 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DLowRhombus(g2d, 0);
	}

	/**
	 * 3D效果和阴影最下层
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DLowRhombus(g2d, linevalue);
	}

	/**
	 * 既有阴影又有3D 填充背景区域
	 */
	public void paint3DLowRhombus(Graphics2D g2d, int indent3Dvalue) {
		g2d.fillPolygon(new int[] { x1 - indent3Dvalue / 2, x2 - indent3Dvalue,
				x3 - indent3Dvalue / 2, x4 }, new int[] { y1,
				y2 - indent3Dvalue / 2, y3 - indent3Dvalue,
				y4 - indent3Dvalue / 2 }, 4);
		g2d.setStroke(flowElementData.getBasicStroke());
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DRhombusLine(g2d, 0);
	}

	private void paint3DRhombusLine(Graphics2D g2d, int indent3Dvalue) {
		g2d.drawPolygon(new int[] { x1 - indent3Dvalue / 2, x2 - indent3Dvalue,
				x3 - indent3Dvalue / 2, x4 }, new int[] { y1,
				y2 - indent3Dvalue / 2, y3 - indent3Dvalue,
				y4 - indent3Dvalue / 2 }, 4);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DRhombusLine(g2d, linevalue);
	}

	/**
	 * 顶层 颜色填充,渐变色区域 只有3D
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		g2d.fillPolygon(new int[] { x1, x2 - 2 * indent3Dvalue, x3,
				x4 + 2 * indent3Dvalue }, new int[] { y1 + indent3Dvalue, y2,
				y3 - indent3Dvalue, y4 }, 4);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		g2d.fillPolygon(new int[] { x1, x2 - 2 * indent3Dvalue, x3,
				x4 + 2 * indent3Dvalue }, new int[] { y1 + indent3Dvalue, y2,
				y3 - 2 * indent3Dvalue, y4 }, 4);
	}
}
