package com.jecn.epros.server.action.designer.popedom;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

public interface IJecnRoleAction {
	/**
	 * @author yxw 2012-4-27
	 * @description：树加载，获得默认角色
	 * @return
	 */
	public List<JecnTreeBean> getDefaultRoles(boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:树加载，通过父类Id和项目ID，获得role子节点(角色节点和角色目录)
	 * @param pId
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getChildRoles(Long pId, Long projectId, Long secondAdminPeopleId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description: 树加载，通过父类Id和项目ID，获得role子节点(角色目录)
	 * @param pId
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getChildRoleDirs(Long pId, Long projectId, Long secondAdminPeopleId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:树加载，通过项目ID，获得Role所有的子节点(角色节点和角色目录)
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getAllRoles(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:树加载，通过项目ID，获得Role所有的子节点(角色目录)
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getAllRoleDirs(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:table加载，通过项目Id，名称，搜索角色（不包括默认角色）
	 * @param projectId
	 * @param name
	 * @return
	 */
	public List<JecnTreeBean> getRolesByName(Long projectId, String name, Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:节点排序
	 * @param list
	 * @param pId排序节点的父节点ID
	 * @param projectId
	 */
	public void updateSortRoles(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersionId)
			throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:移动角色
	 * @param listIds
	 * @param pId
	 *            移动到新节点的Id
	 */
	public void moveRoles(List<Long> listIds, Long pId, Long updatePersionId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description: 删除节点
	 * @param listIds
	 */
	public void deleteRoles(List<Long> listIds, Long projectId, Long updatePersionId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:添加角色目录
	 * @param jecnRoleInfo
	 * @return
	 */
	public Long addRoleDir(JecnRoleInfo jecnRoleInfo, Long updatePersionId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:添加角色
	 * @param jecnRoleInfo
	 * @param processIds
	 * @param fileIds
	 * @param standardIds
	 * @param ruleIds
	 * @return
	 */
	public Long addRole(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:更新角色
	 * @param jecnRoleInfo
	 * @param mapIds
	 * @param updatePersionId
	 * @return
	 */
	public Long updateRole(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:角色重命名
	 * @param newName
	 * @param id
	 * @param updatePersionId
	 */
	public void reRoleName(String newName, Long id, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-4-27
	 * @description:table加载，移动节点，搜索除了移动节点所在目录下其他关键字name的角色目录
	 * @param projectId
	 * @param name
	 * @param listIds
	 * @return
	 */
	public List<JecnTreeBean> getRoleDirsByNameMove(Long projectId, String name, List<Long> listIds) throws Exception;

	/**
	 * @author yxw 2012-5-8
	 * @description:主键Id获得角色对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnRoleInfo getJecnRoleInfoById(Long id) throws Exception;

	/**
	 * @author yxw 2012-5-9
	 * @description:获得节点下的子节点的个数
	 * @param id
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public int getChildCount(Long id, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-10
	 * @description:角色更新时，判断是否与同一父ID下的角色是否重名
	 * @param newName
	 * @param id
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-14
	 * @description: 根据ID和项目ID递归上级节点，包括和一级的兄弟节点
	 * @param id
	 * @paramprojectId
	 * @return
	 */
	public List<JecnTreeBean> getPnodes(Long id, Long projectId) throws Exception;

	/**
	 * 获取二级管理员角色
	 * 
	 * @return
	 * @throws Exception
	 */
	List<JecnTreeBean> getSecondAdminTreeBean(Long preId) throws Exception;

	Long updateRoleSecondAdmin(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception;

	Long addRoleSecondAdmin(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception;

	List<Object[]> selectUserRoleByRoleId(Long roleId) throws Exception;

	Long getSecondAdminRoleId(Long peopleId) throws Exception;

	boolean isRoleAuthDesigner(Long peopleId) throws Exception;
}
