package epros.draw.gui.figure;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ToolTipManager;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 
 * 角色、虚拟角色、客户父类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnBaseRoleFigure extends BaseRect {

	public JecnBaseRoleFigure(JecnFigureData flowElementData) {
		super(flowElementData);
		if (!(flowElementData instanceof JecnRoleData)) {// 不是JecnRoleData对象
			JecnLogConstants.LOG_JecnBaseRoleFigure.error("JecnBaseRoleFigure类构造函数：参数不合法");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FIAL);
		}

		initMouseEvent();
	}

	public JecnRoleData getFlowElementData() {
		return (JecnRoleData) flowElementData;
	}

	/**
	 * 
	 * 提示信息事件
	 * 
	 */
	public void initMouseEvent() {

		this.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent paramMouseEvent) {
				// 初始化
				setToolTipText(null);

				// 岗位名称
				String posName = getFlowElementData().getPosName();
				String posGroupName = getFlowElementData().getPosGroupName();
				// 岗位职责
				String roleRes = getFlowElementData().getRoleRes();

				boolean posNameNote = DrawCommon.isNullOrEmtryTrim(posName);
				boolean roleResNote = DrawCommon.isNullOrEmtryTrim(roleRes);
				boolean posGroupNote = DrawCommon.isNullOrEmtryTrim(posGroupName);
				if (posNameNote && roleResNote && posGroupNote) {// 不存在悬浮
					return;
				}

				// 指定取消工具提示的延迟值 设置为一小时 3600秒
				ToolTipManager.sharedInstance().setDismissDelay(3600 * 1000);
				// 组织HTML格式字符串
				StringBuffer sbuf = new StringBuffer();
				sbuf
						.append("<html><div style= 'white-space:normal; display:block;min-width:200px; word-break:break-all'>");

				if (!posNameNote) {// 岗位名称
					sbuf.append("<br><b>");
					sbuf.append(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("posName"));
					sbuf.append("</b><br><br>");
					// 说明
					sbuf.append(DrawCommon.getsubstring(posName));
					sbuf.append("<br><br>");
				}

				if (!posGroupNote) {// 岗位名称
					sbuf.append("<br><b>");
					sbuf.append(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("posGroupName"));
					sbuf.append("</b><br><br>");
					// 说明
					sbuf.append(DrawCommon.getsubstring(posGroupName));
					sbuf.append("<br><br>");
				}

				if (!roleResNote) {// 角色职责
					sbuf.append("<br><b>");
					sbuf.append(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("roleRes"));
					sbuf.append("</b><br><br>");
					sbuf.append(DrawCommon.getsubstring(roleRes));
					sbuf.append("<br><br>");
				}

				sbuf.append("</div><br></html>");

				// 注册要在工具提示中显示的文本。光标处于该组件上时显示该文本。如果 参数 为 null，则关闭此组件的工具提示
				setToolTipText(sbuf.toString());
			}

			public void mouseExited(MouseEvent paramMouseEvent) {
				// 返回初始延迟值
				int intD = ToolTipManager.sharedInstance().getInitialDelay();
				// 取消工具提示的延迟值
				ToolTipManager.sharedInstance().setDismissDelay(intD);
			}
		});
	}
}
