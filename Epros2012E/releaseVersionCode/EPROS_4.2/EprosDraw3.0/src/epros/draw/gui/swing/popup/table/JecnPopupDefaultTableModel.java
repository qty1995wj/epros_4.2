package epros.draw.gui.swing.popup.table;

import javax.swing.table.DefaultTableModel;

/**
 * 
 * 表模型
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPopupDefaultTableModel extends DefaultTableModel {
	JecnPopupDefaultTableModel() {
	}
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}