package epros.draw.gui.top.dialog.listener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.error.ErrorInfoConstant;

/**
 * 
 * 对话框事件类
 * 
 * @author ZHOUXY
 *
 */
public class JecnDialogEvent {
	private final Log log = LogFactory.getLog(JecnDialogEvent.class);

	private Object source = null;

	public JecnDialogEvent(Object source) {
		if (source == null) {
			log.error("JecnFrameEvent类JecnFrameEvent构造函数参数为空 source=" + source);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.source = source;
	}

	public Object getSource() {
		return source;
	}
}
