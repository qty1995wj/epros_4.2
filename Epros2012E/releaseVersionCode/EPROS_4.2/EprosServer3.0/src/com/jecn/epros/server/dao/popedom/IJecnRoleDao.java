package com.jecn.epros.server.dao.popedom;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnRoleContent;
import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.popedom.UserRole;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnEnumUtil.AuthorName;

public interface IJecnRoleDao extends IBaseDao<JecnRoleInfo, Long> {

	/**
	 * 判断是存在默认角色
	 * 
	 * @param filterId
	 * @return
	 * @throws Exception
	 */
	public boolean isExistDefaultRole(String filterId, Long peopleId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：获得用户的默认权限
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public List<JecnRoleInfo> getDefaultRoleByPeopleId(Long peopleId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：获得自定义权限
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public List<JecnRoleContent> getJecnRoleContentsByPeopleId(Long peopleId) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除角色
	 * @param setIds
	 * @throws Exception
	 */
	public void deleteRoles(Set<Long> setIds) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除角色
	 * @param projectId
	 * @throws Exception
	 */
	public void deleteRoles(Long projectId) throws Exception;

	/**
	 * @author zhangchen Nov 2, 2012
	 * @description：获得流程管理员和流程设计专家的ID get(0)是流程管理员ID，get(1)是流程管理员
	 * @return
	 * @throws Exception
	 */
	public Map<Long, AuthorName> getAdminAndDesignId() throws Exception;

	/**
	 * 获取二级管理员对应角色ID
	 * 
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	Long getSecondAdminRoleId(Long peopleId) throws Exception;

	List<Object[]> getSecondAdminObjs(Long preId) throws Exception;

	void deleteUserRoleByRoleId(Long roleId) throws Exception;

	void addUserRole(UserRole userRole) throws Exception;

	/**
	 * 获取角色对应人员
	 * 
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	List<Object[]> selectUserRoleByRoleId(Long roleId) throws Exception;

	List<Long> getDesignerAuths(String mark) throws Exception;

	void deleteUserRoleByIds(List<Long> list) throws Exception;

	void insertUserRoleLog(List<Long> lists) throws Exception;

	List<Object[]> getSecondAdminRolesByName(Long projectId, Long roleId, String name) throws Exception;

	int getRoleAuthDesignerCount(Long peopleId) throws Exception;

	int getDesignerUserTotal(String mark) throws Exception;
}
