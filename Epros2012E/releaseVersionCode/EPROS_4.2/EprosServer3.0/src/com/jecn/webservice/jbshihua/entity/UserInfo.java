package com.jecn.webservice.jbshihua.entity;

import com.jecn.epros.server.webBean.dataImport.sync.JecnUserPosView;

public class UserInfo {
	private String userCode;
	private String realName;
	private String userDept;
	private String userPosNum;
	private String userPosName;
	private String phone;
	private String email;
	private String userStatus;

	public UserInfo(String userCode, String realName, String userDept, String userPosNum, String userPosName,
			String phone, String email, String userStatus) {
		super();
		this.userCode = userCode;
		this.realName = realName;
		this.userDept = userDept;
		this.userPosNum = userPosNum;
		this.userPosName = userPosName;
		this.phone = phone;
		this.email = email;
		this.userStatus = userStatus;
	}

	public UserInfo(String userCode, String userPosNum, String userPosName, String userStatus, String userDept) {
		super();
		this.userCode = userCode;
		this.userPosNum = userPosNum;
		this.userPosName = userPosName;
		this.userStatus = userStatus;
		this.userDept = userDept;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserDept() {
		return userDept;
	}

	public void setUserDept(String userDept) {
		this.userDept = userDept;
	}

	public String getUserPosNum() {
		return userPosNum;
	}

	public void setUserPosNum(String userPosNum) {
		this.userPosNum = userPosNum;
	}

	public String getUserPosName() {
		return userPosName;
	}

	public void setUserPosName(String userPosName) {
		this.userPosName = userPosName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public JecnUserPosView toUserViewBean() {
		return new JecnUserPosView(this.userCode, this.userCode, this.realName, this.userPosNum, this.userPosName,
				this.userDept, this.email, "0", this.phone, "", this.userStatus);
	}

}
