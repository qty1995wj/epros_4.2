package com.jecn.epros.server.connector.mengniu;

public class MengNiuTasksBean {
	/** 对应的第三方待办任务编号 - taskId */
	private String EngineTaskSN;
	/** 任务类型 1正常任务 2加签任务 3.转签任务 */
	private int TaskType;
	/** 流程发起人ID,唯一员工号 */
	private String HandlerUserID;

	private String ProxyUserID;

	private String ActName;

	/** OA中流程任务名称 */
	private String TaskName;

	/** OA中PC端的待办任务链接 */
	private String ProcessPCURL;

	/** OA中Mobile端的待办任务链接 */
	private String ProcessMobileURL;

	public String getEngineTaskSN() {
		return EngineTaskSN;
	}

	public void setEngineTaskSN(String engineTaskSN) {
		EngineTaskSN = engineTaskSN;
	}

	public int getTaskType() {
		return TaskType;
	}

	public void setTaskType(int taskType) {
		TaskType = taskType;
	}

	public String getHandlerUserID() {
		return HandlerUserID;
	}

	public void setHandlerUserID(String handlerUserID) {
		HandlerUserID = handlerUserID;
	}

	public String getProxyUserID() {
		return ProxyUserID;
	}

	public void setProxyUserID(String proxyUserID) {
		ProxyUserID = proxyUserID;
	}

	public String getActName() {
		return ActName;
	}

	public void setActName(String actName) {
		ActName = actName;
	}

	public String getTaskName() {
		return TaskName;
	}

	public void setTaskName(String taskName) {
		TaskName = taskName;
	}

	public String getProcessPCURL() {
		return ProcessPCURL;
	}

	public void setProcessPCURL(String processPCURL) {
		ProcessPCURL = processPCURL;
	}

	public String getProcessMobileURL() {
		return ProcessMobileURL;
	}

	public void setProcessMobileURL(String processMobileURL) {
		ProcessMobileURL = processMobileURL;
	}

	public String toString() {
		return "EngineTaskSN = " + EngineTaskSN + "  ProcessPCURL = " + ProcessPCURL + "  ProcessMobileURL = "
				+ ProcessMobileURL;
	}

}
