package com.jecn.epros.domain.system;

import java.util.List;

public class UserManageBean {

    private int total = 0;
    private List<UserWebInfoBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<UserWebInfoBean> getData() {
        return data;
    }

    public void setData(List<UserWebInfoBean> data) {
        this.data = data;
    }


}
