/**
 * GetReadWorkItemsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetReadWorkItemsResponse  implements java.io.Serializable {
    private com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getReadWorkItemsResult;

    public GetReadWorkItemsResponse() {
    }

    public GetReadWorkItemsResponse(
           com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getReadWorkItemsResult) {
           this.getReadWorkItemsResult = getReadWorkItemsResult;
    }


    /**
     * Gets the getReadWorkItemsResult value for this GetReadWorkItemsResponse.
     * 
     * @return getReadWorkItemsResult
     */
    public com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getGetReadWorkItemsResult() {
        return getReadWorkItemsResult;
    }


    /**
     * Sets the getReadWorkItemsResult value for this GetReadWorkItemsResponse.
     * 
     * @param getReadWorkItemsResult
     */
    public void setGetReadWorkItemsResult(com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getReadWorkItemsResult) {
        this.getReadWorkItemsResult = getReadWorkItemsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetReadWorkItemsResponse)) return false;
        GetReadWorkItemsResponse other = (GetReadWorkItemsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getReadWorkItemsResult==null && other.getGetReadWorkItemsResult()==null) || 
             (this.getReadWorkItemsResult!=null &&
              java.util.Arrays.equals(this.getReadWorkItemsResult, other.getGetReadWorkItemsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetReadWorkItemsResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetReadWorkItemsResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetReadWorkItemsResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetReadWorkItemsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetReadWorkItemsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getReadWorkItemsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GetReadWorkItemsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "CirculateItemViewModel"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "CirculateItemViewModel"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
