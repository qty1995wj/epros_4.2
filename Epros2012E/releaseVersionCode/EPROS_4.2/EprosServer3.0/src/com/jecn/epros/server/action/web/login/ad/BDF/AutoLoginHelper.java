package com.jecn.epros.server.action.web.login.ad.BDF;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * EKP自动登录帮助类，用于与其他系统做单点登录时，获取到已通过验证的关键字（通常是用户名）后，在EKP系统中做自动登录操作。
 * 
 * @author 苏轶 2010-3-5
 */
public class AutoLoginHelper {

	protected static final Logger log = Logger.getLogger(AutoLoginHelper.class);

	/**
	 * 根据关键字（通常是用户名）自动登录EKP，常用于与其他系统进行单点登录操作。
	 * 
	 * @param keyword
	 *            关键字（通常是用户名）
	 */
	public String doAutoLogin(String keyword, HttpSession session) {
		return doAutoLogin(keyword, null, session);
	}

	/**
	 * 根据关键字（通常是用户名）自动登录EKP，常用于与其他系统进行单点登录操作。
	 * 
	 * @param keyword
	 *            关键字
	 * @param type
	 *            关键字类型，可选值为keyword|id|username
	 * @return 用户名
	 */
	public String doAutoLogin(String keyword, String type, HttpSession session) {
		log.warn(" keyword = " + keyword);
		if (StringUtils.isBlank(keyword)) {
			return null;
		}
		session.setAttribute(BDFLoginAction.USER_NAME, keyword);
		return keyword;
	}

	/**
	 * 执行注销操作
	 * 
	 * @param request
	 * @param response
	 */
	public void doLogout(HttpServletRequest request, HttpServletResponse response) {
		Cookie namecookie = new Cookie("loginName", null);
		namecookie.setMaxAge(0);
		response.addCookie(namecookie);
		HttpSession session = request.getSession();
		session.invalidate();
	}
}
