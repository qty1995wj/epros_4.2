package epros.designer.gui.popedom.organization;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.system.ResourceSortDialog;
import epros.designer.service.process.JecnOrganization;
import epros.designer.service.process.JecnProcess;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * @author yxw 2012-6-5
 * @description：组织右键菜单
 */
public class OrgResourceTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(OrgResourceTreeMenu.class);
	/** 新建组织 */
	private JMenuItem newOrgItem = new JMenuItem(JecnProperties.getValue("newOrg"), new ImageIcon(
			"images/menuImage/newOrg.gif"));
	/** 新建岗位 */
	private JMenuItem newPositionItem = new JMenuItem(JecnProperties.getValue("newPosition"), new ImageIcon(
			"images/menuImage/newPosition.gif"));
	/** 搜索 */
	private JMenuItem searchItem = new JMenuItem(JecnProperties.getValue("search"), new ImageIcon(
			"images/menuImage/search.gif"));
	/** 生成组织图 */
	private JMenuItem createOrgMapItem = new JMenuItem(JecnProperties.getValue("createOrgMap"), new ImageIcon(
			"images/menuImage/createOrgMap.gif"));
	/** 节点排序 */
	private JMenuItem nodeSortItem = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon(
			"images/menuImage/nodeSort.gif"));
	/** 节点移动 */
	private JMenuItem nodeMoveItem = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon(
			"images/menuImage/nodeMove.gif"));
	/** 刷新 */
	private JMenuItem refreshItem = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon(
			"images/menuImage/refresh.gif"));
	/** 组织属性 */
	private JMenuItem orgDutyItem = new JMenuItem(JecnProperties.getValue("orgDuty"), new ImageIcon(
			"images/menuImage/orgDuty.gif"));
	/** 岗位属性 */
	private JMenuItem positionPropItem = new JMenuItem(JecnProperties.getValue("positionProp"), new ImageIcon(
			"images/menuImage/positionProp.gif"));
	/** 重命名 */
	private JMenuItem renameItem = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon(
			"images/menuImage/rename.gif"));
	/** 删除 */
	private JMenuItem deleteItem = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon(
			"images/menuImage/delete.gif"));
	/** 解除编辑 */
	private JMenuItem removeEditItem = new JMenuItem(JecnProperties.getValue("removeEdit"), new ImageIcon(
			"images/menuImage/removeEdit.gif"));
	private Separator separatorOne = new Separator();
	private Separator separatorTwo = new Separator();
	private JecnTree jTree = null;
	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 设置统一部门下岗位名称相同 0:不允许 1：允许 */
	private Long allowSamePosName = null;
	/** 选中节点集合 */
	private List<JecnTreeNode> selectNodes = null;

	public OrgResourceTreeMenu(JecnTree jTree, List<JecnTreeNode> selectNodes, int selectMutil) {
		this.jTree = jTree;
		this.selectNodes = selectNodes;
		if (selectNodes.size() > 0) {
			selectNode = selectNodes.get(0);
		}
		if (selectNodes.size() == 1) {
			// 单选设置menu
			this.singleSelect();
		} else {
			// 多选设置menu
			this.mutilSelect();
		}

		this.add(newOrgItem);// 新建组织
		this.add(newPositionItem);// 新建岗位
		this.add(separatorOne);

		this.add(createOrgMapItem);// 生成组织图
		this.add(renameItem);// 重命名
		this.add(nodeSortItem);// 节点排序
		this.add(nodeMoveItem);// 节点移动
		this.add(refreshItem);// 刷新
		this.add(deleteItem);// 删除
		this.add(separatorTwo);

		this.add(removeEditItem);// 解除编辑
		this.add(orgDutyItem);// 组织职责
		this.add(positionPropItem);// 岗位属性

		menuItemActionInit();

		try {
			allowSamePosName = Long.valueOf(ConnectionPool.getConfigAciton().selectValue(5, "allowSamePosName"));
		} catch (Exception e) {
			log.error("OrgResourceTreeMenu line:132 is error", e);
		}
	}

	public void menuItemActionInit() {
		newOrgItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newOrgItemAction();
			}
		});
		newPositionItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newPositionItemAction();
			}
		});
		createOrgMapItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				createOrgMapItemAction();
			}
		});
		nodeSortItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeSortItemAction();
			}
		});
		nodeMoveItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nodeMoveItemAction();

			}
		});
		refreshItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refreshItemAction();
			}
		});

		orgDutyItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				orgDutyItemAction();
			}
		});
		positionPropItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				positionPropItemAction();
			}
		});

		removeEditItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				removeEditItemAction();
			}
		});

		renameItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				renameItemAction();
			}
		});
		deleteItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteItemAction();
			}
		});
	}

	private void newOrgItemAction() {
		AddOrgDialog addOrgDialog = new AddOrgDialog(selectNode, jTree);
		addOrgDialog.setVisible(true);
	}

	private void newPositionItemAction() {
		AddPositionDialog addPositionDialog = new AddPositionDialog(selectNode, jTree);
		addPositionDialog.setVisible(true);

	}

	private void removeEditItemAction() {
		try {
			ConnectionPool.getOrganizationAction().moveEdit(selectNode.getJecnTreeBean().getId());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("moveEditSuccess"));
		} catch (Exception e) {
			log.error("OrgResourceTreeMenu removeEditItemAction is error", e);
		}
	}

	private void createOrgMapItemAction() {
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isCover"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		List<JecnTreeBean> list = refreshItemAction();
		try {
			JecnOrganization.createOrgMap(selectNode.getJecnTreeBean(), list);
		} catch (Exception e) {
			log.error("OrgResourceTreeMenu createOrgMapItemAction is error！", e);
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("generateOrganizationalChartAnomaly"));
			return;
		}
	}

	private void nodeSortItemAction() {
		ResourceSortDialog rs = new ResourceSortDialog(selectNode, jTree);
		rs.setVisible(true);
		if (rs.isFlag()) {
			JecnTreeModel jecnTreeModel = (JecnTreeModel) jTree.getModel();
			jecnTreeModel.reload(selectNode);

			// 如果是组织根节点执行
			if (selectNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.organizationRoot)) {
				// 排序后将岗位组重新插入
				JecnTreeCommon.addPosGroupRootToOrgRoot(jTree, selectNode);
			}
		}
	}

	private void nodeMoveItemAction() {
		// 选中的节点类型
		TreeNodeType nodeType = selectNodes.get(0).getJecnTreeBean().getTreeNodeType();
		// 不是岗位移动或者是岗位移动但是不允许相同的时候，执行验证
		if (nodeType != TreeNodeType.position || nodeType == TreeNodeType.position && allowSamePosName == 0) {
			if (!JecnDesignerCommon.validateRepeatNodesName(selectNodes)) {
				return;
			}
		}
		OrgPositionMoveChooseDialog opmc = new OrgPositionMoveChooseDialog(selectNodes, jTree);
		// 调用刷新方法，实现节点移动自动刷新节点
		refreshItemAction();
		opmc.setVisible(true);
	}

	private List<JecnTreeBean> refreshItemAction() {
		if (selectNode == null) {
			return null;
		}
		Long id = selectNode.getJecnTreeBean().getId();

		try {
			// 刷新组织
			List<JecnTreeBean> list = ConnectionPool.getOrganizationAction().getChildOrgsAndPositon(id,
					JecnConstants.projectId, true);
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
			// 刷新的是组织根节点时加载岗位组根节点
			if (selectNode.getJecnTreeBean().getTreeNodeType().equals(TreeNodeType.organizationRoot)) {
				// 调用插入岗位组方法将岗位组插入
				JecnTreeCommon.addPosGroupRootToOrgRoot(jTree, selectNode);
			}
			return list;
		} catch (Exception ex) {
			log.error("OrgResourceTreeMenu refreshItemAction is error", ex);
		}
		return null;
	}

	private void orgDutyItemAction() {
		// 组织职责
		OrgResponseDialog orgResponseDialog = new OrgResponseDialog(selectNode);
		orgResponseDialog.setVisible(true);
	}

	private void positionPropItemAction() {
		// 岗位属性
		PosPropertyDialog posPropertyDialog = new PosPropertyDialog(selectNode, jTree);
		posPropertyDialog.setVisible(true);
	}

	private void renameItemAction() {

		try {
			// 如果是组织重命名，需要判断面板是否占用
			if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.organization) {
				boolean isOpen = ConnectionPool.getOrganizationAction().updateOrgCanOpen(JecnConstants.getUserId(),
						selectNode.getJecnTreeBean().getId());
				if (!isOpen) {
					JecnOptionPane
							.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("opened"));
					return;
				}
			}
		} catch (Exception e) {
			log.error("OrgResourceTreeMenu renameItemAction is error", e);
		}
		EditOrgNameDialog editOrgNameDialog = new EditOrgNameDialog(selectNode, jTree);
		editOrgNameDialog.setVisible(true);

	}

	private void deleteItemAction() {
		List<Long> listIds = new ArrayList<Long>();
		if (JecnConstants.isMysql) {
			listIds = JecnTreeCommon.getAllChildIds(selectNodes);
		} else {
			for (JecnTreeNode node : selectNodes) {
				listIds.add(node.getJecnTreeBean().getId());
				if (JecnProcess.isOpen(node.getJecnTreeBean().getId().intValue(), JecnDesignerCommon
						.treeNodeTypeToMapType(node.getJecnTreeBean().getTreeNodeType()))) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), node.getJecnTreeBean().getName()
							+ JecnProperties.getValue("alreadyOpen"));
					return;
				}
			}
		}
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		try {
			if (TreeNodeType.organization.equals(selectNode.getJecnTreeBean().getTreeNodeType())) {
				for (JecnTreeNode node : selectNodes) {
					MapType mapType = MapType.orgMap;
					boolean isOpen = JecnDesignerCommon.isOpen(node.getJecnTreeBean().getId().intValue(), mapType);

					if (isOpen) {
						JecnOptionPane.showMessageDialog(null, "[" + node.getJecnTreeBean().getName() + "]"
								+ JecnProperties.getValue("alreadyOpenPleaseCloseInCarryingOutAfterDeleted"), null,
								JecnOptionPane.ERROR_MESSAGE);
						return;
					}
				}
				// 删除组织
				ConnectionPool.getOrganizationAction().deleteOrgs(listIds, JecnConstants.getUserId());
			} else if (TreeNodeType.position.equals(selectNode.getJecnTreeBean().getTreeNodeType())) {
				// 删除岗位
				ConnectionPool.getOrganizationAction().deletePosition(listIds);
			}
		} catch (Exception e) {
			log.error("OrgResourceTreeMenu deleteItemAction is error", e);
		}
		/** 删除节点 */
		JecnTreeCommon.removeNodes(jTree, selectNodes);

	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		if (JecnDesignerCommon.isAdmin()) {
			// 删除
			deleteItem.setVisible(true);
			// 移动
			nodeMoveItem.setVisible(true);
		}

	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		JecnTreeCommon.autoExpandNode(jTree, selectNode);
		init();
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case organizationRoot:
			if (JecnDesignerCommon.isAdmin()) {
				separatorOne.setVisible(true);
				// 新建组织
				newOrgItem.setVisible(true);
				// 搜索
				searchItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);

			}
			// 刷新
			refreshItem.setVisible(true);
			break;
		case organization:
			int authType = JecnTreeCommon.isAuthNode(selectNode);
			if (JecnDesignerCommon.isAdmin() || (authType == 1 || authType == 2)) {
				separatorOne.setVisible(true);
				separatorTwo.setVisible(true);
				// 新建组织
				newOrgItem.setVisible(true);
				// 新建岗位
				newPositionItem.setVisible(true);
				// 搜索
				searchItem.setVisible(true);
				// 重命名
				renameItem.setVisible(true);
				// 节点排序
				nodeSortItem.setVisible(true);
				// 节点移动
				nodeMoveItem.setVisible(true);
				// 删除
				deleteItem.setVisible(true);
				// 组织职责
				orgDutyItem.setVisible(true);
				// 生成组织图
				createOrgMapItem.setVisible(true);
				if (JecnConstants.isPubShow()) {// 非D2版本
					// 解除编辑
					removeEditItem.setVisible(true);
				}
			}
			// 刷新
			refreshItem.setVisible(true);
			break;
		case position:
			if (JecnDesignerCommon.isAdmin()) {
				separatorTwo.setVisible(true);
				// 重命名
				renameItem.setVisible(true);
				// 节点移动
				nodeMoveItem.setVisible(true);
				// 删除
				deleteItem.setVisible(true);
				// 岗位属性
				positionPropItem.setVisible(true);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 初始化
	 */
	private void init() {
		// 新建组织
		newOrgItem.setVisible(false);
		// 新建岗位
		newPositionItem.setVisible(false);
		// 节点移动
		nodeMoveItem.setVisible(false);
		// 组织职责
		orgDutyItem.setVisible(false);
		// 岗位属性
		positionPropItem.setVisible(false);
		// 重命名
		renameItem.setVisible(false);
		// 节点排序
		nodeSortItem.setVisible(false);
		// 删除
		deleteItem.setVisible(false);
		// 生成组织图
		createOrgMapItem.setVisible(false);
		// 刷新
		refreshItem.setVisible(false);
		separatorOne.setVisible(false);
		separatorTwo.setVisible(false);
		removeEditItem.setVisible(false);
	}
}
