package epros.draw.designer;

import java.util.Date;

/***
 * 控制点 2013-10-29
 * 
 */
public class JecnControlPointT implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** 主键ID */
	private String id;
	/** 关联活动ID */
	private Long activeId;
	/** 控制点编号 */
	private String controlCode;
	/** 是否是关键控制点：0：是，1：否 */
	private int keyPoint;
	/** 控制频率 0:随时 1:日 2: 周 3:月 4:季度 5:年度 */
	private int frequency;
	/** 控制方法 0:手动1:自动 */
	private int method;
	/** 控制类型 0:预防性1:发现性 */
	private int type;
	/** 风险ID */
	private Long riskId;
	/** 控制目标ID */
	private Long targetId;
	/*** 创建人 */
	private Long createPersonId;
	/*** 创建日期 */
	private Date createTime;
	/*** 更新人 */
	private Long updatePersonId;
	/*** 更新日期 */
	private Date updateTime;
	/** 备注 */
	private String note;
	/** 风险编号 */
	private String riskNum;
	/** 控制目标 */
	private String controlTarget;
	/** 是否删除当前控制点 1:删除 */
	private int isDelete = 0;

	/** 判断是添加还是更新 0：添加 2：更新 */
	private int addOrEdit = 0;
	private String figureUUID;

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public String getControlTarget() {
		return controlTarget;
	}

	public void setControlTarget(String controlTarget) {
		this.controlTarget = controlTarget;
	}

	public String getRiskNum() {
		return riskNum;
	}

	public void setRiskNum(String riskNum) {
		this.riskNum = riskNum;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public int getKeyPoint() {
		return keyPoint;
	}

	public void setKeyPoint(int keyPoint) {
		this.keyPoint = keyPoint;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public int getMethod() {
		return method;
	}

	public void setMethod(int method) {
		this.method = method;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Long getRiskId() {
		return riskId;
	}

	public void setRiskId(Long riskId) {
		this.riskId = riskId;
	}

	public Long getTargetId() {
		return targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getUpdatePersonId() {
		return updatePersonId;
	}

	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getControlCode() {
		return controlCode;
	}

	public void setControlCode(String controlCode) {
		this.controlCode = controlCode;
	}

	public int getAddOrEdit() {
		return addOrEdit;
	}

	public void setAddOrEdit(int addOrEdit) {
		this.addOrEdit = addOrEdit;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

	public JecnControlPointT clone() {
		JecnControlPointT controlPointT = new JecnControlPointT();
//		controlPointT.setId(this.getId());
		controlPointT.setActiveId(activeId);
		controlPointT.setControlCode(controlCode);
		controlPointT.setKeyPoint(keyPoint);
		controlPointT.setFrequency(frequency);
		controlPointT.setMethod(method);
		controlPointT.setType(type);
		controlPointT.setRiskId(riskId);
		controlPointT.setTargetId(targetId);
		controlPointT.setCreatePersonId(createPersonId);
		controlPointT.setCreateTime(createTime);
		controlPointT.setUpdatePersonId(updatePersonId);
		controlPointT.setUpdateTime(updateTime);
		controlPointT.setNote(note);
		controlPointT.setRiskNum(riskNum);
		controlPointT.setControlTarget(controlTarget);
		controlPointT.setIsDelete(isDelete);
		controlPointT.setFigureUUID(figureUUID);
		controlPointT.setAddOrEdit(addOrEdit);
		return controlPointT;
	}

}
