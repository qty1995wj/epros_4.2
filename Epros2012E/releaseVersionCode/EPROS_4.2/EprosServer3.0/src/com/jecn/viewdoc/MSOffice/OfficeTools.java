package com.jecn.viewdoc.MSOffice;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

public class OfficeTools {
	private static final int wdFormatPDF = 17;
	private static final int xlTypePDF = 0;
	private static final int ppSaveAsPDF = 32;

	// private static final int msoTrue = -1;
	// private static final int msofalse = 0;
	/**
	 * @return 操作成功与否的提示信息. 如果返回 -1, 表示找不到源文件, 或url.properties配置错误; 如果返回 0,
	 *         则表示操作成功; 返回1, 则表示转换失败
	 */
	public static int officeToPdf(OfficeToPDFInfo officeToPDFInfo, String mark) {
		String sourceFile = officeToPDFInfo.sourceUrl;
		String destFile = officeToPDFInfo.destUrl;
		File inputFile = new File(sourceFile);
		if (!inputFile.exists()) {
			return -1;// 找不到源文件, 则返回-1
		}
		// 如果目标路径不存在, 则新建该路径
		File outputFile = new File(destFile);
		if (!outputFile.getParentFile().exists()) {
			outputFile.getParentFile().mkdirs();
		}
		String extentionName = FilenameUtils.getExtension(sourceFile);
		if (extentionName.equalsIgnoreCase("ppt") || extentionName.equalsIgnoreCase("pptx")) {
			ppt2pdf(officeToPDFInfo.sourceUrl, officeToPDFInfo.destUrl);
		} else if (extentionName.equalsIgnoreCase("doc") || extentionName.equalsIgnoreCase("docx")) {
			doc2pdf(officeToPDFInfo.sourceUrl, officeToPDFInfo.destUrl);
		} else if (extentionName.equalsIgnoreCase("xls") || extentionName.equalsIgnoreCase("xlsx")) {
			excel2PDF(officeToPDFInfo.sourceUrl, officeToPDFInfo.destUrl);
		}

		// 给PDF加水印

		// 登录人员
		File waterT = new File(destFile + "w");

		try {
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(waterT));
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			PDFWaterMarkUtils.setWatermark(bos, destFile, format.format(cal.getTime()) + "生成", getLoginMark(mark));
			String fileName = outputFile.getName();
			outputFile.delete();
			waterT.renameTo(new File(destFile));
		} catch (Exception e) {
			Log.error("加水印失败");
		}

		return 0;
	}

	private static String getLoginMark(String mark) {
		if (StringUtils.isNotBlank(mark)) {
			return mark;
		}
		// 获取人员信息
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		if (webLoginBean == null) {
			throw new IllegalArgumentException("预览当前登录人为空");
		}
		JecnUser user = webLoginBean.getJecnUser();
		return user.getTrueName();
	}

	protected static boolean doc2pdf(String srcFilePath, String pdfFilePath) {
		ActiveXComponent app = null;
		Dispatch doc = null;
		try {
			ComThread.InitSTA();
			app = new ActiveXComponent("Word.Application");
			app.setProperty("Visible", false);
			Dispatch docs = app.getProperty("Documents").toDispatch();
			doc = Dispatch.invoke(docs, "Open", Dispatch.Method,
					new Object[] { srcFilePath, new Variant(false), new Variant(true),// 是否只读
							new Variant(false), new Variant("pwd") }, new int[1]).toDispatch();
			// Dispatch.put(doc, "Compatibility", false); //兼容性检查,为特定值false不正确
			Dispatch.put(doc, "RemovePersonalInformation", false);
			Dispatch.call(doc, "ExportAsFixedFormat", pdfFilePath, wdFormatPDF); // word保存为pdf格式宏，值为17
			return true; // set flag true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (doc != null) {
				Dispatch.call(doc, "Close", false);
			}
			if (app != null) {
				app.invoke("Quit", 0);
			}
			ComThread.Release();
		}
		return false;
	}

	protected static boolean ppt2pdf(String srcFilePath, String pdfFilePath) {
		ActiveXComponent app = null;
		Dispatch ppt = null;
		try {
			ComThread.InitSTA();
			app = new ActiveXComponent("PowerPoint.Application");
			Dispatch ppts = app.getProperty("Presentations").toDispatch();

			// 因POWER.EXE的发布规则为同步，所以设置为同步发布
			ppt = Dispatch.call(ppts, "Open", srcFilePath, true,// ReadOnly
					true,// Untitled指定文件是否有标题
					false// WithWindow指定文件是否可见
					).toDispatch();
			Dispatch.call(ppt, "SaveAs", pdfFilePath, ppSaveAsPDF); // ppSaveAsPDF为特定值32
			return true; // set flag true;
		} finally {
			if (ppt != null) {
				Dispatch.call(ppt, "Close");
			}
			if (app != null) {
				app.invoke("Quit");
			}
			ComThread.Release();
		}
	}

	public static boolean excel2PDF(String inputFile, String pdfFile) {
		// ActiveXComponent app = null;
		// Dispatch excel = null;
		// try {
		// ComThread.InitSTA();
		// app = new ActiveXComponent("Excel.Application");
		// app.setProperty("Visible", false);
		// Dispatch excels = app.getProperty("Workbooks").toDispatch();
		// excel = Dispatch.call(excels, "Open", inputFile, false,
		// true).toDispatch();
		// Dispatch.call(excel, "ExportAsFixedFormat", xlTypePDF, pdfFile);
		// return true;
		// } finally {
		// if (excel != null) {
		// Dispatch.call(excel, "Close");
		// }
		// if (app != null) {
		// app.invoke("Quit");
		// }
		// ComThread.Release();
		// }

		File sourcefile = new File(inputFile);
		try {
			StringBuffer htmlStr = new StringBuffer();
			htmlStr.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=GBK\" /><title>"
					+ sourcefile.getName() + "</title></head><body>");
			htmlStr.append(ExcelToHtml.getExcelInfo(sourcefile));
			htmlStr.append("</body></html>");
			ExcelToHtml.writeFile(htmlStr.toString(), pdfFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public static int officeToPdf(String inputPath, String outputPath) {
		// TODO Auto-generated method stub
		OfficeToPDFInfo officeToPDFInfo = new OfficeToPDFInfo();
		officeToPDFInfo.setDestUrl(outputPath);
		officeToPDFInfo.setSourceUrl(inputPath);

		return officeToPdf(officeToPDFInfo, null);
	}

	public static int officeToPdf(String inputPath, String outputPath, String mark) {
		// TODO Auto-generated method stub
		OfficeToPDFInfo officeToPDFInfo = new OfficeToPDFInfo();
		officeToPDFInfo.setDestUrl(outputPath);
		officeToPDFInfo.setSourceUrl(inputPath);

		return officeToPdf(officeToPDFInfo, mark);
	}

	/**
	 * XML word另存为
	 * 
	 * @param path
	 * @return
	 */
	public static String xmlWordSaveAsOfficeWord(String path) {
		if (!JecnConfigTool.isEnableOffice()) {
			return path;
		}
		File file = new File(path);
		String newPath = file.getParent() + File.separator + JecnCommon.getUUID() + ".doc";
		ActiveXComponent _app = new ActiveXComponent("Word.Application");
		_app.setProperty("Visible", Variant.VT_FALSE);

		Dispatch documents = _app.getProperty("Documents").toDispatch();

		// 打开FreeMarker生成的Word文档
		Dispatch doc = Dispatch.call(documents, "Open", path, Variant.VT_FALSE, Variant.VT_TRUE).toDispatch();
		// 另存为新的Word文档
		Dispatch.call(doc, "SaveAs", newPath, Variant.VT_FALSE, Variant.VT_TRUE);

		Dispatch.call(doc, "Close", Variant.VT_FALSE);
		_app.invoke("Quit", new Variant[] {});
		ComThread.Release();
		// 删除xml版本的word
		File xmlFile = new File(path);
		xmlFile.delete();
		return newPath;
	}
}
