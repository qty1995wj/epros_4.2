package epros.draw.gui.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.UIManager;

import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;

/**
 * 
 * 自定义UI
 * 
 * @author ZHOUXY
 * 
 */
public class JecnWindowsTabbedPaneUI extends WindowsTabbedPaneUI {
	boolean tabsOverlapBorder = UIManager
			.getBoolean("TabbedPane.tabsOverlapBorder");
	boolean contentOpaque = UIManager.getBoolean("TabbedPane.contentOpaque");
	Color selectedColor = UIManager.getColor("TabbedPane.selected");

	/**
	 * 
	 * 左边框
	 * 
	 */
	protected void paintContentBorderLeftEdge(Graphics g, int tabPlacement,
			int selectedIndex, int x, int y, int w, int h) {
	}

	/**
	 * 
	 * 下边框
	 * 
	 */
	protected void paintContentBorderBottomEdge(Graphics g, int tabPlacement,
			int selectedIndex, int x, int y, int w, int h) {
	}

	/**
	 * 
	 * 右边框
	 * 
	 */
	protected void paintContentBorderRightEdge(Graphics g, int tabPlacement,
			int selectedIndex, int x, int y, int w, int h) {
	}

	/**
	 * 
	 * 绘制边框
	 * 
	 */
	protected void paintContentBorder(Graphics g, int tabPlacement,
			int selectedIndex) {
		paintContentBorderJecn(g, tabPlacement, selectedIndex);
	}

	/**
	 * 
	 * 拷贝BasicTabbedPaneUI类paintContentBorder方法：目的为了执行BasicTabbedPaneUI类paintContentBorder方法
	 * 
	 * @param g
	 * @param tabPlacement
	 * @param selectedIndex
	 */
	protected void paintContentBorderJecn(Graphics g, int tabPlacement,
			int selectedIndex) {
		int width = tabPane.getWidth();
		int height = tabPane.getHeight();
		Insets insets = tabPane.getInsets();
		Insets tabAreaInsets = getTabAreaInsets(tabPlacement);

		int x = insets.left;
		int y = insets.top;
		int w = width - insets.right - insets.left;
		int h = height - insets.top - insets.bottom;

		switch (tabPlacement) {
		case LEFT:
			x += calculateTabAreaWidth(tabPlacement, runCount, maxTabWidth);
			if (tabsOverlapBorder) {
				x -= tabAreaInsets.right;
			}
			w -= (x - insets.left);
			break;
		case RIGHT:
			w -= calculateTabAreaWidth(tabPlacement, runCount, maxTabWidth);
			if (tabsOverlapBorder) {
				w += tabAreaInsets.left;
			}
			break;
		case BOTTOM:
			h -= calculateTabAreaHeight(tabPlacement, runCount, maxTabHeight);
			if (tabsOverlapBorder) {
				h += tabAreaInsets.top;
			}
			break;
		case TOP:
		default:
			y += calculateTabAreaHeight(tabPlacement, runCount, maxTabHeight);
			if (tabsOverlapBorder) {
				y -= tabAreaInsets.bottom;
			}
			h -= (y - insets.top);
		}

		if (tabPane.getTabCount() > 0 && (contentOpaque || tabPane.isOpaque())) {
			// Fill region behind content area
			Color color = UIManager.getColor("TabbedPane.contentAreaColor");
			if (color != null) {
				g.setColor(color);
			} else if (selectedColor == null || selectedIndex == -1) {
				g.setColor(tabPane.getBackground());
			} else {
				g.setColor(selectedColor);
			}
			g.fillRect(x, y, w, h);
		}

		paintContentBorderTopEdge(g, tabPlacement, selectedIndex, x, y, w, h);
		paintContentBorderLeftEdge(g, tabPlacement, selectedIndex, x, y, w, h);
		paintContentBorderBottomEdge(g, tabPlacement, selectedIndex, x, y, w, h);
		paintContentBorderRightEdge(g, tabPlacement, selectedIndex, x, y, w, h);
	}
}