package com.jecn.epros.sqlprovider;

import java.util.Map;

public class PermissionSqlProvider {

    public String getOrgAccessPermissions(Map<String, Object> paramMap) {

        Long relateId = (Long) paramMap.get("relateId");
        Integer accessType = (Integer) paramMap.get("accessType");
        Boolean isPub = (Boolean) paramMap.get("isPub");

        String sql = "";
        if (isPub) {
            sql = "SELECT JFO.ORG_ID,JFO.ORG_NAME,JFO.PER_ORG_ID,(SELECT COUNT(*) FROM JECN_FLOW_ORG WHERE PER_ORG_ID=JFO.ORG_ID ) AS COUNT"
                    + " FROM JECN_FLOW_ORG JFO,JECN_ORG_ACCESS_PERMISSIONS JOAP WHERE JFO.ORG_ID = JOAP.ORG_ID AND JOAP.RELATE_ID="
                    + relateId + " AND JOAP.TYPE=" + accessType + " AND JFO.DEL_STATE=0"
                    + " ORDER BY JFO.PER_ORG_ID,JFO.SORT_ID,JFO.ORG_ID";
        } else {
            sql = "SELECT JFO.ORG_ID,JFO.ORG_NAME,JFO.PER_ORG_ID,(SELECT COUNT(*) FROM JECN_FLOW_ORG WHERE PER_ORG_ID=JFO.ORG_ID ) AS COUNT"
                    + " FROM JECN_FLOW_ORG JFO,JECN_ORG_ACCESS_PERM_T JOAP WHERE JFO.ORG_ID = JOAP.ORG_ID AND JOAP.RELATE_ID="
                    + relateId + " AND JOAP.TYPE=" + accessType + " AND JFO.DEL_STATE=0"
                    + " ORDER BY JFO.PER_ORG_ID,JFO.SORT_ID,JFO.ORG_ID";
        }
        return sql;

    }

    public String getPositionAccessPermissions(Map<String, Object> paramMap) {

        Long relateId = (Long) paramMap.get("relateId");
        Integer accessType = (Integer) paramMap.get("accessType");
        Boolean isPub = (Boolean) paramMap.get("isPub");
        String sql = "";
        if (isPub) {
            sql = "SELECT T.FIGURE_ID,T.FIGURE_TEXT,T.ORG_ID,JFO.ORG_NAME"
                    + " FROM JECN_FLOW_ORG_IMAGE T,JECN_FLOW_ORG JFO,JECN_ACCESS_PERMISSIONS JAP WHERE T.ORG_ID=JFO.ORG_ID AND JFO.DEL_STATE=0"
                    + " AND T.FIGURE_ID = JAP.FIGURE_ID AND JAP.RELATE_ID=" + relateId + "AND JAP.TYPE=" + accessType
                    + " ORDER BY T.ORG_ID,T.SORT_ID";
        } else {
            // 流程
            sql = "SELECT T.FIGURE_ID,T.FIGURE_TEXT,T.ORG_ID,JFO.ORG_NAME"
                    + " FROM JECN_FLOW_ORG_IMAGE T,JECN_FLOW_ORG JFO,JECN_ACCESS_PERMISSIONS_T JAP WHERE T.ORG_ID=JFO.ORG_ID AND JFO.DEL_STATE=0"
                    + " AND T.FIGURE_ID = JAP.FIGURE_ID AND JAP.RELATE_ID=" + relateId + " AND JAP.TYPE=" + accessType
                    + " ORDER BY T.ORG_ID,T.SORT_ID";
        }
        return sql;
    }

    public String getPositionAccessPermissionsGroup(Map<String, Object> paramMap) {

        Long relateId = (Long) paramMap.get("relateId");
        Integer accessType = (Integer) paramMap.get("accessType");
        Boolean isPub = (Boolean) paramMap.get("isPub");
        String sql = "";
        // 判断是否是发布状态
        if (isPub) {
            sql = "SELECT JPG.ID,JPG.NAME,JPG.PER_ID,JPG.IS_DIR FROM JECN_POSITION_GROUP JPG,JECN_GROUP_PERMISSIONS JGP WHERE JPG.ID=JGP.POSTGROUP_ID AND  JGP.RELATE_ID="
                    + relateId + " AND TYPE=" + accessType;
        } else {
            sql = "SELECT JPG.ID,JPG.NAME,JPG.PER_ID,JPG.IS_DIR FROM  JECN_POSITION_GROUP JPG,JECN_GROUP_PERMISSIONS_T JGPT WHERE JPG.ID=JGPT.POSTGROUP_ID AND JGPT.RELATE_ID="
                    + relateId + " AND JGPT.TYPE=" + accessType;
        }
        return sql;
    }

    public String deletePosPermission(Map<String, Object> paramMap) {
        String pub = paramMap.get("pub").toString();
        String sql = "DELETE FROM JECN_ACCESS_PERMISSIONS" + pub + " WHERE RELATE_ID=#{relateId} AND TYPE = #{type}";

        return sql;
    }

    public String deleteOrgPermission(Map<String, Object> paramMap) {
        String pub = paramMap.get("pub").toString();
        String sql = "DELETE FROM JECN_ORG_ACCESS_PERM" + pub + " WHERE RELATE_ID=#{relateId} AND TYPE = #{type}";
        return sql;
    }

    public String deletePosGroupPermission(Map<String, Object> paramMap) {
        String pub = paramMap.get("pub").toString();
        String sql = "DELETE FROM JECN_GROUP_PERMISSIONS" + pub + " WHERE RELATE_ID=#{relateId} AND TYPE = #{type}";

        return sql;

    }

    public String insertOrgPerm(Map<String, Object> paramMap) {
        String pub = paramMap.get("pub").toString();

        String idCondition = "";
        String valueCondition = "";
        if (BaseSqlProvider.isOracle()) {
            idCondition = "id,";
            valueCondition = "JECN_PERMISSIONS_SQUENCE.NEXTVAL,";
        }
        String sql = "insert into JECN_ORG_ACCESS_PERM" + pub + " (" + idCondition + " ORG_ID, RELATE_ID, TYPE) VALUES (" + valueCondition + " #{figureId}, #{relateId}, #{type})";
        return sql;

    }

    public String insertPosPerm(Map<String, Object> paramMap) {
        String pub = paramMap.get("pub").toString();
        String idCondition = "";
        String valueCondition = "";
        if (BaseSqlProvider.isOracle()) {
            idCondition = "id,";
            valueCondition = "JECN_PERMISSIONS_SQUENCE.NEXTVAL,";
        }
        String sql = "insert into JECN_ACCESS_PERMISSIONS" + pub + " (" + idCondition + " FIGURE_ID, RELATE_ID, TYPE) VALUES (" + valueCondition + " #{figureId}, #{relateId}, #{type})";
        return sql;

    }

    public String insertPosGroupPerm(Map<String, Object> paramMap) {
        String pub = paramMap.get("pub").toString();
        String idCondition = "";
        String valueCondition = "";
        if (BaseSqlProvider.isOracle()) {
            idCondition = "id,";
            valueCondition = "JECN_PERMISSIONS_SQUENCE.NEXTVAL,";
        }
        String sql = "insert into JECN_GROUP_PERMISSIONS" + pub + " (" + idCondition + " POSTGROUP_ID, RELATE_ID, TYPE) VALUES (" + valueCondition + " #{figureId}, #{relateId}, #{type})";
        return sql;

    }

}
