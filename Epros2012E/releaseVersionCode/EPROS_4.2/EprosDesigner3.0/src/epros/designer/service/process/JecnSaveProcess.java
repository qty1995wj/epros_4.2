package epros.designer.service.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.ActivityUpdateData;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnLineSegmentT;
import com.jecn.epros.server.bean.process.JecnRoleActiveT;
import com.jecn.epros.server.bean.process.JecnToFromActiveT;
import com.jecn.epros.server.bean.process.ProcessFileImageFigureData;
import com.jecn.epros.server.bean.process.RoleFigureData;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.service.JecnDesignerImpl;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnImplData;
import epros.draw.data.figure.JecnModeFigureData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.data.figure.JecnToFromRelatedData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.designer.JecnActiveOnLineTBean;
import epros.draw.designer.JecnActiveStandardBeanT;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnControlPointT;
import epros.draw.designer.JecnFigureFileTBean;
import epros.draw.designer.JecnFigureInoutSampleT;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.designer.JecnFlowInoutPosT;
import epros.draw.designer.JecnFlowInoutT;
import epros.draw.designer.JecnFlowStationT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.designer.JecnRefIndicatorsT;
import epros.draw.designer.JecnTempletT;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;

/**
 * 保存图
 * 
 * @author fuzhh
 * 
 */
public class JecnSaveProcess {
	private static Logger log = Logger.getLogger(JecnDesignerImpl.class);

	/**
	 * 获取图形的填充颜色
	 * 
	 * @param figureData
	 * @return
	 */
	public static void getColorStr(JecnFigureData figureData, JecnFlowStructureImageT flowStructureImageT) {
		String backColor = "";
		// 图形填充色效果标识
		int colorChangType = 1;
		// 图行颜色的类型none为未填充,one为单色效果,two为双色效果 默认未填充单色效果
		if (figureData.getFillType() != null) {
			switch (figureData.getFillType()) {
			case none:
				// 图形填充色效果标识
				flowStructureImageT.setFillEffects(0);
				break;
			case one:
				// 图形填充色效果标识
				if (figureData.getFillColor() != null) {
					backColor = figureData.getFillColor().getRed() + "," + figureData.getFillColor().getGreen() + ","
							+ figureData.getFillColor().getBlue();
				}
				flowStructureImageT.setFillEffects(1);
				break;
			case two:
				switch (figureData.getFillColorChangType()) {
				case topTilt:
					colorChangType = 1;
					break;
				case bottom:
					colorChangType = 2;
					break;
				case vertical:
					colorChangType = 3;
					break;
				case horizontal:
					colorChangType = 4;
					break;
				}
				// 双色填充
				if (figureData.getFillColor() != null && figureData.getChangeColor() != null) {
					backColor = +figureData.getFillColor().getRed() + "," + figureData.getFillColor().getGreen() + ","
							+ figureData.getFillColor().getBlue() + "," + figureData.getChangeColor().getRed() + ","
							+ figureData.getChangeColor().getGreen() + "," + figureData.getChangeColor().getBlue()
							+ "," + colorChangType;
				}
				flowStructureImageT.setFillEffects(2);
				break;
			}
			flowStructureImageT.setBGColor(backColor);
		}
	}

	/**
	 * 图形数据转换为 JecnFlowStructureImageT数据
	 * 
	 * @param figureData
	 *            图形数据
	 * @param flowId
	 *            流程ID
	 * @param flowStructureImageT
	 *            存储的Bean
	 */
	public static void getFlowStructureImageTtoFigureData(JecnFigureData figureData, Long flowId,
			JecnFlowStructureImageT flowStructureImageT) {
		// 流程ID
		flowStructureImageT.setFlowId(flowId);
		if (figureData.getUUID() == null) {
			figureData.setUUID(DrawCommon.getUUID());
		}
		flowStructureImageT.setUUID(figureData.getUUID());
		// 图形唯一标示
		flowStructureImageT.setFigureImageId(figureData.getFlowElementUUID());
		// 线主键ID
		flowStructureImageT.setFigureId(figureData.getFlowElementId());
		// 图形的类型
		flowStructureImageT.setFigureType(figureData.getMapElemType().toString());
		// 元素形状，有此值的元素，可以切换形状
		flowStructureImageT.setEleShape(figureData.getEleShape() == null ? null : figureData.getEleShape().toString());

		if (figureData.getEleShape() == null && DrawCommon.isInfoFigure(figureData.getMapElemType())) {
			flowStructureImageT.setActivityShow(figureData.getAvtivityShow());
		}

		// 元素名称
		if (MapElemType.FreeText == figureData.getEleShape()) {
			flowStructureImageT.setActivityShow(figureData.getFigureText());
		} else {
			flowStructureImageT.setFigureText(figureData.getFigureText());
		}

		// 开始点
		flowStructureImageT.setPoLongx(Long.valueOf(figureData.getFigureDataCloneable().getX()));
		// 结束点
		flowStructureImageT.setPoLongy(Long.valueOf(figureData.getFigureDataCloneable().getY()));
		// 图形的宽度
		flowStructureImageT.setWidth(Long.valueOf(figureData.getFigureSizeX()));
		// 图形的高度
		flowStructureImageT.setHeight(Long.valueOf(figureData.getFigureSizeY()));
		// 字体大小
		flowStructureImageT.setFontSize(Long.valueOf(figureData.getFontSize()));
		// 图形层级
		flowStructureImageT.setOrderIndex(Long.valueOf(figureData.getZOrderIndex()));
		// 图形的链接id
		flowStructureImageT.setLinkFlowId(figureData.getDesignerFigureData().getLinkId());
		// 图形的填充颜色
		getColorStr(figureData, flowStructureImageT);

		// 字体颜色
		if (figureData.getFontColor() != null) {
			// 字体颜色
			String fontColorStr = String.valueOf(figureData.getFontColor().getRed() + ","
					+ figureData.getFontColor().getGreen() + "," + figureData.getFontColor().getBlue());
			flowStructureImageT.setFontColor(fontColorStr);
		}
		// 字体加粗
		flowStructureImageT.setFontBody(Long.valueOf(figureData.getFontStyle()));
		// 字体类型
		flowStructureImageT.setFontType(figureData.getFontName());
		// 字体位置
		flowStructureImageT.setFontPosition(String.valueOf(figureData.getFontPosition()));
		// 是否存在阴影
		if (figureData.isShadowsFlag()) {
			flowStructureImageT.setHavaShadow(1L);
		} else {
			flowStructureImageT.setHavaShadow(0L);
		}
		// 阴影颜色
		if (figureData.getShadowColor() != null) {
			// 阴影颜色
			String fontColorStr = String.valueOf(figureData.getShadowColor().getRed() + ","
					+ figureData.getShadowColor().getGreen() + "," + figureData.getShadowColor().getBlue());
			flowStructureImageT.setShadowColor(fontColorStr);
		}
		// 旋转角度
		flowStructureImageT.setCircumgyrate(String.valueOf(figureData.getCurrentAngle()));
		// 边框宽度
		flowStructureImageT.setLineThickness(Integer.valueOf(figureData.getBodyWidth()));
		// 边框样式
		flowStructureImageT.setBodyLine(String.valueOf(figureData.getBodyStyle()));
		// 边框颜色
		if (figureData.getBodyColor() != null) {
			flowStructureImageT.setBodyColor(String.valueOf(figureData.getBodyColor().getRed() + ","
					+ figureData.getBodyColor().getGreen() + "," + figureData.getBodyColor().getBlue()));
		}
		// 是否显示3D
		int is3Dshwo = 0;
		if (figureData.isFlag3D()) {
			is3Dshwo = 1;
		}
		flowStructureImageT.setIs3DEffect(is3Dshwo);

		// 连接ID
		Long linkFlowId = figureData.getDesignerFigureData().getLinkId();
		flowStructureImageT.setLinkFlowId(linkFlowId);
		// 连接类型
		String lineType = figureData.getDesignerFigureData().getLineType();
		// 接口类型
		flowStructureImageT.setImplType(StringUtils.isBlank(lineType) ? 0 : Integer.valueOf(lineType));

		// 判断是否为活动
		if (DrawCommon.isActive(figureData.getMapElemType())) {
			JecnActiveData activeData = (JecnActiveData) figureData;
			// 活动编号
			flowStructureImageT.setActivityId(activeData.getActivityNum());
			// 活动说明
			flowStructureImageT.setActivityShow(activeData.getAvtivityShow());
			// 关键活动
			flowStructureImageT.setRoleRes(activeData.getAvtivityShowAndControl());
			// 关键活动类型
			flowStructureImageT.setLineColor(activeData.getActiveKeyType());
			// 活动的类型
			flowStructureImageT.setTimeType(activeData.getAvtivityTimeType());
			// 活动的开始时间
			flowStructureImageT.setStartTime(activeData.getAvtivityStartTime());
			// 活动的结束时间
			flowStructureImageT.setStopTime(activeData.getAvtivityStopTime());
			// 活动类型ID
			flowStructureImageT.setActiveTypeId(activeData.getActiveTypeId());
			// 是否在线上
			flowStructureImageT.setIsOnLine(activeData.getIsOnLine());
			// 南京石化
			if (JecnConstants.loginBean.getOtherLoginType() == 4) {
				// 对应内控矩阵风险点
				flowStructureImageT.setInnerControlRisk(activeData.getInnerControlRisk());
				// 对应标准条款
				flowStructureImageT.setStandardConditions(activeData.getStandardConditions());
			}
			// TODO 添加活动输入和输出
			flowStructureImageT.setActivityInput(activeData.getActivityInput());
			flowStructureImageT.setActivityOutput(activeData.getActivityOutput());
			flowStructureImageT.setCustomOne(activeData.getCustomOne());

		} else if (DrawCommon.isRole(figureData.getMapElemType())) { // 判断是否为角色
			// 角色职责
			JecnRoleData rooleData = (JecnRoleData) figureData;
			flowStructureImageT.setRoleRes(rooleData.getRoleRes());
			// 主责岗位
			flowStructureImageT.setActivityId(rooleData.getActivityId());
		} else if (MapElemType.CommentText == figureData.getMapElemType()) {// 判断是否为注释框
			JecnCommentLineData commentData = (JecnCommentLineData) figureData;
			if (commentData.getLineData() == null) {
				return;
			}
			// 注释框线条开始X
			flowStructureImageT.setStartFigure(Long.valueOf((int) commentData.getStartX()));
			// 注释框线条开始Y
			flowStructureImageT.setEndFigure(Long.valueOf((int) commentData.getStartY()));
		} else if (MapElemType.IconFigure == figureData.getMapElemType()) { // 图标插入框
			flowStructureImageT.setLinkFlowId(figureData.getDesignerFigureData().getLinkId());
		} else if (MapElemType.ModelFigure == figureData.getMapElemType()) { // 泳池
			// 转换为泳池
			JecnModeFigureData modeFigureData = (JecnModeFigureData) figureData;
			// 记录泳池分割点X坐标
			flowStructureImageT.setDividingX(modeFigureData.getDividingX());
		}
	}

	/**
	 * 判断是否需要修改
	 * 
	 * @param figureData
	 *            图形的数据
	 * @param oldFigureData
	 *            内存的数据
	 * @param flowId
	 *            流程ID
	 * @param flowStructureImageT
	 *            需要修改的bean
	 * @return
	 */
	public static boolean isUpdateFlowElementData(JecnFigureData figureData, JecnFigureData oldFigureData, Long flowId,
			JecnFlowStructureImageT flowStructureImageT) {
		boolean isUpdate = false;
		if (figureData.getFillType() == oldFigureData.getFillType()) {
			if (figureData.getFillType() == FillType.one) {
				if (figureData.getFillColor() != null && oldFigureData.getFillColor() != null) {
					if (figureData.getFillColor().getRGB() != oldFigureData.getFillColor().getRGB()) {
						isUpdate = true;
					}
				} else if ((oldFigureData.getFillColor() == null && figureData.getFillColor() != null)
						|| (oldFigureData.getFillColor() != null && figureData.getFillColor() == null)) {
					isUpdate = true;
				}
			} else if (figureData.getFillType() == FillType.two) {
				if (figureData.getFillColor() != null && oldFigureData.getFillColor() != null) {
					if (figureData.getFillColor().getRGB() != oldFigureData.getFillColor().getRGB()) {
						isUpdate = true;
					}
				} else if ((oldFigureData.getFillColor() == null && figureData.getFillColor() != null)
						|| (oldFigureData.getFillColor() != null && figureData.getFillColor() == null)) {
					isUpdate = true;
				}

				if (figureData.getChangeColor() != null && oldFigureData.getChangeColor() != null) {
					if (figureData.getChangeColor().getRGB() != oldFigureData.getChangeColor().getRGB()) {
						isUpdate = true;
					}
				} else if ((oldFigureData.getChangeColor() != null && figureData.getChangeColor() != null && oldFigureData
						.getChangeColor().getRGB() != figureData.getChangeColor().getRGB())
						|| (oldFigureData.getChangeColor() != null && figureData.getChangeColor() == null)
						|| (figureData.getChangeColor() != null && oldFigureData.getChangeColor() == null)) {
					isUpdate = true;
				}
			}
		}
		if (isUpdateFigureText(figureData, oldFigureData)) {
			// 保存流程图的 提示是否修改流程名称的名称
			// isUpdate = isFigureTextUpdateLinkNodeText(figureData,
			// oldFigureData);
			isUpdate = isFigureTextUpdateLinkNodeText(figureData, oldFigureData);
		} else if (figureData.getFigureDataCloneable().getX() != oldFigureData.getFigureDataCloneable().getX()) {
			isUpdate = true;
		} else if (getIsUpdate(figureData.getDesignerFigureData().getLinkId(), oldFigureData.getDesignerFigureData()
				.getLinkId())) {
			isUpdate = true;
		} else if (getIsUpdate(figureData.getDesignerFigureData().getLineType(), oldFigureData.getDesignerFigureData()
				.getLineType())) {
			isUpdate = true;
		} else if (figureData.getFigureDataCloneable().getY() != oldFigureData.getFigureDataCloneable().getY()) {
			isUpdate = true;
		} else if (figureData.getFigureSizeX() != oldFigureData.getFigureSizeX()) {
			isUpdate = true;
		} else if (figureData.getFigureSizeY() != oldFigureData.getFigureSizeY()) {
			isUpdate = true;
		} else if (figureData.getFillColorChangType() != oldFigureData.getFillColorChangType()) {
			isUpdate = true;
		} else if (figureData.getFontSize() != oldFigureData.getFontSize()) {
			isUpdate = true;
		} else if (figureData.getZOrderIndex() != oldFigureData.getZOrderIndex()) {
			isUpdate = true;
		} else if (figureData.getFillType() != oldFigureData.getFillType()) {
			isUpdate = true;
		} else if (figureData.getFontStyle() != oldFigureData.getFontStyle()) {
			isUpdate = true;
		} else if (getIsUpdate(figureData.getFontName(), oldFigureData.getFontName())) {
			isUpdate = true;
		} else if (figureData.getFontPosition() != oldFigureData.getFontPosition()) {
			isUpdate = true;
		} else if (figureData.isShadowsFlag() != oldFigureData.isShadowsFlag()) {
			isUpdate = true;
		} else if ((figureData.getShadowColor() != null && oldFigureData.getShadowColor() != null && figureData
				.getShadowColor().getRGB() != oldFigureData.getShadowColor().getRGB())
				|| (figureData.getShadowColor() != null && oldFigureData.getShadowColor() == null)
				|| (oldFigureData.getShadowColor() != null && figureData.getShadowColor() == null)) {
			isUpdate = true;
		} else if ((figureData.getFontColor() != null && oldFigureData.getFontColor() != null && figureData
				.getFontColor().getRGB() != oldFigureData.getFontColor().getRGB())
				|| (figureData.getFontColor() == null && oldFigureData.getFontColor() != null)
				|| (figureData.getFontColor() != null && oldFigureData.getFontColor() == null)) { // 字体颜色
			isUpdate = true;
		} else if (figureData.getCurrentAngle() != oldFigureData.getCurrentAngle()) {
			isUpdate = true;
		} else if (figureData.getBodyWidth() != oldFigureData.getBodyWidth()) {
			isUpdate = true;
		} else if (figureData.getBodyStyle() != oldFigureData.getBodyStyle()) {
			isUpdate = true;
		} else if ((figureData.getBodyColor() != null && oldFigureData.getBodyColor() != null && figureData
				.getBodyColor().getRGB() != oldFigureData.getBodyColor().getRGB())
				|| (oldFigureData.getBodyColor() != null && figureData.getBodyColor() == null)
				|| (figureData.getBodyColor() != null && oldFigureData.getBodyColor() == null)) {
			isUpdate = true;
		} else if (figureData.isFlag3D() != oldFigureData.isFlag3D()) {
			isUpdate = true;
		} else if (DrawCommon.isActive(figureData.getMapElemType())
				&& DrawCommon.isActive(oldFigureData.getMapElemType())) {
			JecnActiveData activeData = (JecnActiveData) figureData;
			JecnActiveData oldActiveData = (JecnActiveData) oldFigureData;
			if (activeData.getActivityNum() != null
					&& !activeData.getActivityNum().equals(oldActiveData.getActivityNum())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getAvtivityShow(), oldActiveData.getAvtivityShow())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getAvtivityShowAndControl(), oldActiveData.getAvtivityShowAndControl())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getActiveKeyType(), oldActiveData.getActiveKeyType())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getAvtivityTimeType(), oldActiveData.getAvtivityTimeType())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getAvtivityStartTime(), oldActiveData.getAvtivityStartTime())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getAvtivityStopTime(), oldActiveData.getAvtivityStopTime())) {
			} else if (getIsUpdate(activeData.getActivityInput(), oldActiveData.getActivityInput())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getActivityOutput(), oldActiveData.getActivityOutput())) {
				isUpdate = true;
			} else if (JecnConstants.loginBean.getOtherLoginType() == 4) { // 南京石化
				if (getIsUpdate(activeData.getInnerControlRisk(), oldActiveData.getInnerControlRisk())) {
					isUpdate = true;
				} else if (getIsUpdate(activeData.getStandardConditions(), oldActiveData.getStandardConditions())) {
					isUpdate = true;
				}
			} else if (getIsUpdate(activeData.getIsOnLine(), oldActiveData.getIsOnLine())) {// 是否在线上
				/************ 三期需求验证 ******************/
				isUpdate = true;
			} else if (getIsUpdate(activeData.getActiveTypeId(), oldActiveData.getActiveTypeId())) {// 类别ID变化
				isUpdate = true;
			} else if (getIsUpdate(activeData.getTargetValue(), oldActiveData.getTargetValue())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getStatusValue(), oldActiveData.getStatusValue())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getExplain(), oldActiveData.getExplain())) {
				isUpdate = true;
			} else if (getIsUpdate(activeData.getCustomOne(), oldActiveData.getCustomOne())) {
				isUpdate = true;
			}
		} else if (MapElemType.CommentText == figureData.getMapElemType()
				&& MapElemType.CommentText == oldFigureData.getMapElemType()) {
			JecnCommentLineData commentData = (JecnCommentLineData) figureData;
			JecnCommentLineData oldCommentData = (JecnCommentLineData) oldFigureData;
			if (commentData.getStartX() != oldCommentData.getStartX()) {
				isUpdate = true;
			} else if (commentData.getStartY() != oldCommentData.getStartY()) {
				isUpdate = true;
			}
		} else if (MapElemType.IconFigure == figureData.getMapElemType()
				&& MapElemType.IconFigure == oldFigureData.getMapElemType()) {
			if ((figureData.getDesignerFigureData().getLinkId() != null && (!figureData.getDesignerFigureData()
					.getLinkId().equals(oldFigureData.getDesignerFigureData().getLinkId())))
					&& (oldFigureData.getDesignerFigureData().getLinkId() != null && !oldFigureData
							.getDesignerFigureData().getLinkId().equals(figureData.getDesignerFigureData().getLinkId()))) {
				isUpdate = true;
			}

		} else if (MapElemType.ModelFigure == figureData.getMapElemType()
				&& MapElemType.ModelFigure == oldFigureData.getMapElemType()) { // 泳池
			JecnModeFigureData modeFigureData = (JecnModeFigureData) figureData;
			JecnModeFigureData oldModeFigureData = (JecnModeFigureData) oldFigureData;
			if (oldModeFigureData.getDividingX() != modeFigureData.getDividingX()) {
				isUpdate = true;
			}
		} else if (DrawCommon.isImplFigure(figureData.getMapElemType())) {
			JecnImplData implData = (JecnImplData) figureData;
			JecnImplData oldImplData = (JecnImplData) oldFigureData;
			if (getIsUpdate(implData.getProcessRequirements(), oldImplData.getProcessRequirements())) {
				isUpdate = true;
			} else if (getIsUpdate(implData.getImplName(), oldImplData.getImplName())) {
				isUpdate = true;
			} else if (getIsUpdate(implData.getImplNote(), oldImplData.getImplNote())) {
				isUpdate = true;
			}
		} else if (figureData.getMapElemType() != oldFigureData.getMapElemType()) {
			isUpdate = true;
		}
		if (DrawCommon.isInfoFigure(figureData.getMapElemType())
				|| figureData.getMapElemType().equals(MapElemType.FileImage)) {//
			// 可设置连接的图形元素
			if (getIsUpdate(figureData.getAvtivityShow(), oldFigureData.getAvtivityShow())) {// 说明
				isUpdate = true;
			} else if (mapFileIsUpdate(figureData.getListFigureFileTBean(), oldFigureData.getListFigureFileTBean())) {
				isUpdate = true;
			}
		}

		if (isUpdate) {
			getFlowStructureImageTtoFigureData(figureData, flowId, flowStructureImageT);
		}
		return isUpdate;
	}

	private static boolean isUpdateFigureText(JecnFigureData figureData, JecnFigureData oldFigureData) {
		if (MapElemType.MapNameText == figureData.getMapElemType()) {
			return getIsUpdate(figureData.getFigureText(), oldFigureData.getOldNameText());
		} else {
			return getIsUpdate(figureData.getFigureText(), oldFigureData.getFigureText());
		}
	}

	/**
	 * 流程元素 更改名称 关联树节点是否修改名称
	 * 
	 * @return
	 */
	private static boolean isFigureTextUpdateLinkNodeText(JecnFigureData figureData, JecnFigureData oldFigureData) {
		boolean isUpdate = true;
		if (figureData.getMapElemType() == MapElemType.TermFigureAR) {
			return isUpdate;
		}
		if (MapElemType.IconFigure == figureData.getMapElemType()) {
			return isUpdate;
		}
		if (figureData.getDesignerFigureData().getLinkId() != null
				&& (figureData.getDesignerFigureData().getLinkId() == oldFigureData.getDesignerFigureData().getLinkId())) { // 判断是否存在关联元素
			try {
				String flowName = figureData.getFigureText();
				Long flowLinkId = figureData.getDesignerFigureData().getLinkId();
				/**
				 * 获取流程基本信息
				 */
				JecnFlowStructureT jecnFlowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(
						flowLinkId);
				if (jecnFlowStructureT == null) {
					return isUpdate;
				}
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
						.getValue("SyncProcessNode")
						+ "(" + jecnFlowStructureT.getFlowName() + ")?", null, JecnOptionPane.YES_NO_OPTION,
						JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.YES_OPTION) {

					/**
					 * 验证是否 同父级节点下存在 同名
					 */
					boolean validateUpdateName = ConnectionPool.getProcessAction().validateUpdateName(flowName,
							flowLinkId, jecnFlowStructureT.getPerFlowId(), jecnFlowStructureT.getIsFlow(),
							jecnFlowStructureT.getProjectId());
					if (validateUpdateName) {
						/**
						 * 验证重命名
						 */
						JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
								.getValue("nameHaved"));
						isUpdate = false;
					} else {
						/**
						 * 验证通过执行重命名操作
						 */
						ConnectionPool.getProcessAction().reName(flowLinkId, flowName,
								jecnFlowStructureT.getUpdatePeopleId(), JecnConstants.loginBean.isAdmin() ? 0 : 1);
						/**
						 * 更改 元素表中 所有关联本元素中 名称
						 */
						ConnectionPool.getProcessAction().reNameFlowFigureText(flowLinkId, flowName);
						JecnTreeCommon.reloadProcessByPid(jecnFlowStructureT.getPerFlowId());
					}
				}
			} catch (Exception e) {
				log.error("", e);
			}
		}
		return isUpdate;
	}

	/**
	 * 
	 * @param list
	 *            当前元素附件集合
	 * @param oldList
	 *            当前元素附件历史记录集合
	 * @return true:集合存在不同
	 */
	private static boolean mapFileIsUpdate(List<JecnFigureFileTBean> list, List<JecnFigureFileTBean> oldList) {
		if (list.size() != oldList.size()) {
			return true;
		}
		// true：存在
		boolean isExists = false;
		for (JecnFigureFileTBean jecnFigureFileTBean : list) {
			for (JecnFigureFileTBean oldFigureFileTBean : oldList) {
				if (jecnFigureFileTBean.getId() == null) {
					return true;
				} else if (jecnFigureFileTBean.getId().equals(oldFigureFileTBean.getId())) {
					isExists = true;
					break;
				}
			}
			if (!isExists) {// 老的集合中不存在，添加返回true
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断活动是否修改
	 * 
	 * @param figureData
	 *            图形的数据
	 * @param oldFigureData
	 *            内存的数据
	 * @param flowId
	 *            流程ID
	 * @param activityUpdateData
	 *            、 修改的活动Bean
	 * @return
	 */
	public static ActivityUpdateData isActiveUpdate(JecnFigureData figureData, JecnFigureData oldFigureData,
			Long flowId, ActivityUpdateData activityUpdateData) {
		// 转换为活动
		JecnActiveData activeData = (JecnActiveData) figureData;
		if (activeData.getListModeFileT() != null) {
			List<com.jecn.epros.server.bean.process.JecnModeFileT> listModeFileT = new ArrayList<com.jecn.epros.server.bean.process.JecnModeFileT>();
			activityUpdateData.setListModeFileT(listModeFileT);
			// 输出
			for (JecnModeFileT modeFileT : activeData.getListModeFileT()) {
				modeFileT.setFigureUUID(activeData.getUUID());
				// 没有主键ID代表为新加入的输入 添加到加入中
				activityUpdateData.getListModeFileT().add(getServerModeFileT(modeFileT));
			}
		}

		if (activeData.getListJecnActivityFileT() != null) {
			List<com.jecn.epros.server.bean.process.JecnActivityFileT> listJecnActivityFileT = new ArrayList<com.jecn.epros.server.bean.process.JecnActivityFileT>();
			activityUpdateData.setListJecnActivityFileT(listJecnActivityFileT);
			// 输入和操作规范
			for (JecnActivityFileT ActivityFileT : activeData.getListJecnActivityFileT()) {
				ActivityFileT.setFigureUUID(activeData.getUUID());
				activityUpdateData.getListJecnActivityFileT().add(getServerJecnActivityFileT(ActivityFileT));
			}
		}

		if (activeData.getListRefIndicatorsT() != null) {
			List<com.jecn.epros.server.bean.process.JecnRefIndicatorsT> listRefIndicatorsT = new ArrayList<com.jecn.epros.server.bean.process.JecnRefIndicatorsT>();
			activityUpdateData.setListRefIndicatorsT(listRefIndicatorsT);
			// 指标
			for (JecnRefIndicatorsT refIndicatorsT : activeData.getListRefIndicatorsT()) {
				refIndicatorsT.setFigureUUID(activeData.getUUID());
				// 没有主键ID代表为新加入的输入 添加到加入中
				activityUpdateData.getListRefIndicatorsT().add(getServerJecnRefIndicatorsT(refIndicatorsT));
			}
		}

		/***************** 【三期新增】 ********************/
		if (activeData.getListJecnActiveOnLineTBean() != null) {// 活动线上信息
			// 清空集合
			activityUpdateData.getListJecnActiveOnLineTBean().clear();
			for (JecnActiveOnLineTBean activeOnLineTBean : activeData.getListJecnActiveOnLineTBean()) {
				activeOnLineTBean.setFigureUUID(activeData.getUUID());
				activityUpdateData.getListJecnActiveOnLineTBean()
						.add(getServerJecnActiveOnLineTBean(activeOnLineTBean));
			}
		}
		if (activeData.getListJecnActiveStandardBeanT() != null) {// 活动相关标准
			activityUpdateData.getListJecnActiveStandardBeanT().clear();
			for (JecnActiveStandardBeanT activeStandardBeanT : activeData.getListJecnActiveStandardBeanT()) {
				activeStandardBeanT.setFigureUUID(activeData.getUUID());
				activityUpdateData.getListJecnActiveStandardBeanT().add(
						getServerJecnActiveStandardBeanT(activeStandardBeanT));
			}
		}

		// 活动控制点修改为一个活动对应一个多个控制点 所以需要做如下修改 C
		if (activeData.getJecnControlPointTList() != null) { // 控制点集合
			activityUpdateData.getListControlPoint().clear();// 先将原先的更新数据清空
			// 将activeData传回的要更新的控制点存放到activityUpdateData（更新集合）中
			for (JecnControlPointT controlPointT : activeData.getJecnControlPointTList()) {
				controlPointT.setFigureUUID(activeData.getUUID());
				// getServerJecnControlPointT()是单机版Bean和服务端Bean转换方法 转换后才能添加到集合中
				activityUpdateData.getListControlPoint().add(getServerJecnControlPointT(controlPointT));
			}
		}

		if (activeData.getListFigureInTs() != null) {
			// 将activeData传回的要更新的控制点存放到activityUpdateData（更新集合）中
			activityUpdateData.setListFigureInTs(switchJecnFigureInOutT(activeData.getListFigureInTs(), activeData
					.getUUID()));
		}

		if (activeData.getListFigureOutTs() != null) {
			// 将activeData传回的要更新的控制点存放到activityUpdateData（更新集合）中
			activityUpdateData.setListFigureOutTs(switchJecnFigureInOutT(activeData.getListFigureOutTs(), activeData
					.getUUID()));
		}

		activityUpdateData.setFigureId(figureData.getFlowElementId());
		activityUpdateData.setFigureUUID(figureData.getUUID());
		return activityUpdateData;
	}

	/**
	 * 文档数据获取
	 * 
	 * @param figureData
	 * @return
	 */
	public static ProcessFileImageFigureData isFileImageUpdate(JecnFigureData figureData) {
		ProcessFileImageFigureData fileImageFigureData = new ProcessFileImageFigureData();
		fileImageFigureData.setListJecnFigureFileTBean(JecnSaveProcess.switchLocalFigureFileTBean(figureData
				.getListFigureFileTBean(), figureData.getUUID()));
		return fileImageFigureData;
	}

	/**
	 * 判断角色是否修改
	 * 
	 * @param figureData
	 *            图形的数据
	 * @param oldFigureData
	 *            内存的数据
	 * @param flowId
	 *            流程ID
	 * @param roleFigureData
	 *            、 角色与岗位关联bean
	 * @return
	 */
	public static boolean isRoleUpdate(JecnFigureData figureData, JecnFigureData oldFigureData, Long flowId,
			RoleFigureData roleFigureData) {
		boolean isUpdate = isUpdateFlowElementData(figureData, oldFigureData, flowId, roleFigureData
				.getJecnFlowStructureImageT());
		// 转换为角色
		JecnRoleData roleData = (JecnRoleData) figureData;
		// 内存中的数据转换为角色
		JecnRoleData oldRoleData = (JecnRoleData) oldFigureData;
		List<com.jecn.epros.server.bean.process.JecnFlowStationT> listRolePositionInsert = new ArrayList<com.jecn.epros.server.bean.process.JecnFlowStationT>();
		roleFigureData.setListRolePositionInsert(listRolePositionInsert);

		if (oldRoleData.getFlowStationList() != null && oldRoleData.getFlowStationList().size() > 0) {
			for (JecnFlowStationT oldFlowStationT : oldRoleData.getFlowStationList()) {
				boolean isDelete = true;
				for (JecnFlowStationT flowStationT : roleData.getFlowStationList()) {
					if (StringUtils.isBlank(flowStationT.getUUID())) {
						continue;
					}
					if (flowStationT.getUUID().equals(oldFlowStationT.getUUID())) {
						isDelete = false;
						break;
					}
				}
				if (isDelete) {
					roleFigureData.getListRolePositionDelete().add(oldFlowStationT.getUUID());
					isUpdate = true;
				}
			}
		}

		if (roleData.getFlowStationList() != null) {
			for (JecnFlowStationT flowStationT : roleData.getFlowStationList()) {
				if (flowStationT.getUUID() == null) {
					flowStationT.setFigureUUID(figureData.getUUID());
					roleFigureData.getListRolePositionInsert().add(getServerJecnFlowStationT(flowStationT));
					isUpdate = true;
				}
			}
		}
		// 角色职责
		if ((roleData.getRoleRes() != null && !roleData.getRoleRes().equals(oldRoleData.getRoleRes()))
				|| (oldRoleData.getRoleRes() != null && !oldRoleData.getRoleRes().equals(roleData.getRoleRes()))) {
			isUpdate = true;
		}
		// 是否为主责岗位
		if ((oldRoleData.getActivityId() != null && !oldRoleData.getActivityId().equals(roleData.getActivityId()))
				|| (roleData.getActivityId() != null && !roleData.getActivityId().equals(oldRoleData.getActivityId()))) {
			roleFigureData.getJecnFlowStructureImageT().setActivityId(roleData.getActivityId());
			isUpdate = true;
		}
		if (isUpdate) {
			getFlowStructureImageTtoFigureData(figureData, flowId, roleFigureData.getJecnFlowStructureImageT());
		}
		return isUpdate;
	}

	/**
	 * 获取线的数据
	 * 
	 * @param lineData
	 *            线数据
	 * @param flowId
	 *            流程ID
	 * @param flowStructureImageT
	 *            存储的Bean
	 * @return
	 */
	public static JecnFlowStructureImageT getFlowStructureImageTtoLineData(JecnBaseLineData lineData, Long flowId,
			JecnFlowStructureImageT flowStructureImageT) {
		// 流程ID
		flowStructureImageT.setFlowId(flowId);
		if (lineData.getUUID() == null) {
			lineData.setUUID(DrawCommon.getUUID());
		}
		flowStructureImageT.setUUID(lineData.getUUID());
		// 线主键ID
		flowStructureImageT.setFigureId(lineData.getFlowElementId());
		// 线的类型
		flowStructureImageT.setFigureType(lineData.getMapElemType().toString());
		// 线的UIID
		flowStructureImageT.setFigureImageId(lineData.getFlowElementUUID());
		// 线条样式
		flowStructureImageT.setBodyLine(String.valueOf(lineData.getBodyStyle()));
		// 线的颜色
		flowStructureImageT.setLineColor(String.valueOf(lineData.getBodyColor().getRed() + ","
				+ lineData.getBodyColor().getGreen() + "," + lineData.getBodyColor().getBlue()));
		// 线条宽度
		flowStructureImageT.setLineThickness(Integer.valueOf(lineData.getBodyWidth()));
		// 横竖分割线
		if (lineData.getMapElemType() == MapElemType.ParallelLines
				|| lineData.getMapElemType() == MapElemType.VerticalLine) { // 横竖分割线
			// 获取分割线
			JecnVHLineData vHLineData = (JecnVHLineData) lineData;
			// X点
			flowStructureImageT.setPoLongx(Long.valueOf((int) vHLineData.getVhX()));
			// Y点
			flowStructureImageT.setPoLongy(Long.valueOf((int) vHLineData.getVhY()));
			// 分割线的层级
			flowStructureImageT.setOrderIndex(Long.valueOf(vHLineData.getZOrderIndex()));
		} else if (lineData.getMapElemType() == MapElemType.CommonLine
				|| lineData.getMapElemType() == MapElemType.ManhattanLine) { // 连接线
			JecnManhattanLineData manhattanLineData = (JecnManhattanLineData) lineData;
			// 线的开始图形的主键ID
			flowStructureImageT.setStartFigure(manhattanLineData.getStartId());
			// 线的结束图形的主键ID
			flowStructureImageT.setEndFigure(manhattanLineData.getEndId());
			if (manhattanLineData.getStartFigureData().getUUID() == null) {
				// 开始图形的UIID
				flowStructureImageT.setStartFigureUUID(manhattanLineData.getStartFigureUUID());
				// 结束图形的UIID
				flowStructureImageT.setEndFigureUUID(manhattanLineData.getEndFigureUUID());
			} else {
				// 开始图形的UIID
				flowStructureImageT.setStartFigureUUID(manhattanLineData.getStartFigureData().getUUID());
				// 结束图形的UIID
				flowStructureImageT.setEndFigureUUID(manhattanLineData.getEndFigureData().getUUID());
			}
			// 开始点的边界点
			int startLineType = DrawCommon.getLineType(manhattanLineData.getStartEditPointType());
			flowStructureImageT.setPoLongx(Long.valueOf(startLineType));
			// 结束点的边界点
			int endLineType = DrawCommon.getLineType(manhattanLineData.getEndEditPointType());
			flowStructureImageT.setPoLongy(Long.valueOf(endLineType));
			// 线的层级
			flowStructureImageT.setOrderIndex(Long.valueOf(manhattanLineData.getZOrderIndex()));
			// 线数据
			flowStructureImageT.setFigureText(manhattanLineData.getFigureText());
			// 字体颜色
			if (manhattanLineData.getFontColor() != null) {
				// 字体颜色
				String fontColorStr = String.valueOf(manhattanLineData.getFontColor().getRed() + ","
						+ manhattanLineData.getFontColor().getGreen() + ","
						+ manhattanLineData.getFontColor().getBlue());
				flowStructureImageT.setFontColor(fontColorStr);
			}
			// 字体加粗
			flowStructureImageT.setFontBody(Long.valueOf(manhattanLineData.getFontStyle()));
			// 字体类型
			flowStructureImageT.setFontType(manhattanLineData.getFontName());
			// 字体大小
			flowStructureImageT.setFontSize(Long.valueOf(manhattanLineData.getFontSize()));

			if (manhattanLineData.getTextPoint() != null) {
				flowStructureImageT.setWidth(manhattanLineData.getTextPoint().x + 0L);
				flowStructureImageT.setHeight(manhattanLineData.getTextPoint().y + 0L);
			}
			// 小线段的数据
			List<JecnLineSegmentT> listLineSegmet = new ArrayList<JecnLineSegmentT>();
			// 添加小线段开始
			for (JecnLineSegmentData lineSegmentData : manhattanLineData.getLineSegmentDataList()) {
				if (lineSegmentData.getUUID() == null) {
					JecnLineSegmentT jecnLineSegmentT = new JecnLineSegmentT();
					// 给小线段赋值
					jecnLineSegmentT.setFigureId(manhattanLineData.getFlowElementId());
					// 线条开始点X
					jecnLineSegmentT.setStartX(Long.valueOf((int) lineSegmentData.getLineSegmentData().getStartXY()
							.getX()));
					// 线条开始点Y
					jecnLineSegmentT.setStartY(Long.valueOf((int) lineSegmentData.getLineSegmentData().getStartXY()
							.getY()));
					// 线条结束点X
					jecnLineSegmentT
							.setEndX(Long.valueOf((int) lineSegmentData.getLineSegmentData().getEndXY().getX()));

					// 线条结束点Y
					jecnLineSegmentT
							.setEndY(Long.valueOf((int) lineSegmentData.getLineSegmentData().getEndXY().getY()));

					listLineSegmet.add(jecnLineSegmentT);
					jecnLineSegmentT.setFigureUUID(manhattanLineData.getUUID());
					lineSegmentData.setUUID(DrawCommon.getUUID());
					jecnLineSegmentT.setUUID(lineSegmentData.getUUID());
				}
			}
			// 添加小线段集合
			flowStructureImageT.setListLineSegmet(listLineSegmet);

			// 1:显示双箭头
			flowStructureImageT.setIsOnLine(manhattanLineData.isTwoArrow() ? 1 : 0);
		} else if (lineData.getMapElemType() == MapElemType.VDividingLine
				|| lineData.getMapElemType() == MapElemType.HDividingLine
				|| lineData.getMapElemType() == MapElemType.DividingLine) { // 竖线,横线,斜线（分割线）
			JecnBaseDivedingLineData baseDivedingLineData = (JecnBaseDivedingLineData) lineData;
			// 开始点的X
			flowStructureImageT.setStartFigure(Long.valueOf((int) baseDivedingLineData.getDiviLineCloneable()
					.getStartXInt()));
			// 开始点的Y
			flowStructureImageT.setEndFigure(Long.valueOf((int) baseDivedingLineData.getDiviLineCloneable()
					.getStartYInt()));
			// 结束点的X
			flowStructureImageT
					.setPoLongx(Long.valueOf((int) baseDivedingLineData.getDiviLineCloneable().getEndXInt()));
			// 结束点的Y
			flowStructureImageT
					.setPoLongy(Long.valueOf((int) baseDivedingLineData.getDiviLineCloneable().getEndYInt()));
			// 线条的颜色
			if (baseDivedingLineData.getBodyColor() != null) {
				flowStructureImageT.setBodyColor(String.valueOf(baseDivedingLineData.getBodyColor().getRed() + ","
						+ baseDivedingLineData.getBodyColor().getGreen() + ","
						+ baseDivedingLineData.getBodyColor().getBlue()));
			}
			// // 线条类型
			// flowStructureImageT.setBodyLine(String.valueOf(baseDivedingLineData
			// .getBodyStyle()));
			// 线的层级
			flowStructureImageT.setOrderIndex(Long.valueOf(baseDivedingLineData.getZOrderIndex()));
		}
		return flowStructureImageT;
	}

	/**
	 * 判断线是否需要修改
	 * 
	 * @param lineData
	 *            线数据
	 * @param oldLineData
	 *            内存中线的数据
	 * @param flowId
	 *            流程ID
	 * @param flowStructureImageT
	 *            存储的Bean
	 * @return
	 */
	public static boolean isUpdateFlowLineData(JecnBaseLineData lineData, JecnBaseLineData oldLineData, Long flowId,
			JecnFlowStructureImageT flowStructureImageT) {
		boolean isUpdate = false;
		// 线的UIID
		if ((oldLineData.getUUID() != null && !oldLineData.getUUID().equals(lineData.getUUID()))
				|| (lineData.getUUID() != null && !lineData.getUUID().equals(oldLineData.getUUID()))) {
			isUpdate = true;
		}
		// 横竖分割线
		if ((lineData.getMapElemType() == MapElemType.ParallelLines || lineData.getMapElemType() == MapElemType.VerticalLine)
				&& (oldLineData.getMapElemType() == MapElemType.ParallelLines || oldLineData.getMapElemType() == MapElemType.VerticalLine)) { // 横竖分割线
			// 获取分割线
			JecnVHLineData vHLineData = (JecnVHLineData) lineData;
			JecnVHLineData oldVHLineData = (JecnVHLineData) oldLineData;
			// X点
			if (vHLineData.getVhX() != oldVHLineData.getVhX()) {
				isUpdate = true;
			} else if (vHLineData.getVhY() != oldVHLineData.getVhY()) {// Y点
				isUpdate = true;
			} else if (vHLineData.getZOrderIndex() != oldVHLineData.getZOrderIndex()) {// 分割线的层级
				isUpdate = true;
			}
		} else if ((lineData.getMapElemType() == MapElemType.CommonLine || lineData.getMapElemType() == MapElemType.ManhattanLine)
				&& (oldLineData.getMapElemType() == MapElemType.CommonLine || oldLineData.getMapElemType() == MapElemType.ManhattanLine)) { // 连接线
			JecnManhattanLineData manhattanLineData = (JecnManhattanLineData) lineData;
			JecnManhattanLineData oldManhattanLineData = (JecnManhattanLineData) oldLineData;
			if (!manhattanLineData.getStartFigureUUID().equals(oldManhattanLineData.getStartFigureUUID())) {// 开始图形的UIID
				isUpdate = true;
			} else if (!manhattanLineData.getEndFigureUUID().equals(oldManhattanLineData.getEndFigureUUID())) {// 结束图形的UIID
				isUpdate = true;
			} else if (manhattanLineData.getStartEditPointType() != oldManhattanLineData.getStartEditPointType()) { // 开始点的边界点
				isUpdate = true;
			} else if (manhattanLineData.getEndEditPointType() != oldManhattanLineData.getEndEditPointType()) { // 结束点的边界点
				isUpdate = true;
			} else if ((manhattanLineData.getBodyColor() != null && oldManhattanLineData.getBodyColor() != null && manhattanLineData
					.getBodyColor().getRGB() != oldManhattanLineData.getBodyColor().getRGB())
					|| (manhattanLineData.getBodyColor() != null && oldManhattanLineData.getBodyColor() == null)
					|| (manhattanLineData.getBodyColor() == null && oldManhattanLineData.getBodyColor() != null)) {// 线的颜色
				isUpdate = true;
			} else if (manhattanLineData.getZOrderIndex() != oldManhattanLineData.getZOrderIndex()) {// 线的层级
				isUpdate = true;
			} else if (manhattanLineData.getBodyWidth() != oldManhattanLineData.getBodyWidth()) {// 线条宽度
				isUpdate = true;
			} else if (manhattanLineData.getFigureText() != null
					&& !manhattanLineData.getFigureText().equals(oldManhattanLineData.getFigureText())) {// 线数据
				isUpdate = true;
			} else if ((manhattanLineData.getFontName() != null && !manhattanLineData.getFontName().equals(
					oldManhattanLineData.getFontName()))
					|| (oldManhattanLineData.getFontName() != null && !oldManhattanLineData.getFontName().equals(
							manhattanLineData.getFontName()))) {// 字体类型
				isUpdate = true;
			} else if ((manhattanLineData.getFontColor() != null && oldManhattanLineData.getFontColor() != null && manhattanLineData
					.getFontColor().getRGB() != oldManhattanLineData.getFontColor().getRGB())
					|| (manhattanLineData.getFontColor() == null && oldManhattanLineData.getFontColor() != null)
					|| (manhattanLineData.getFontColor() != null && oldManhattanLineData.getFontColor() == null)) { // 字体颜色
				isUpdate = true;
			} else if (manhattanLineData.getFontStyle() != oldManhattanLineData.getFontStyle()) {// 字体加粗
				isUpdate = true;
			} else if (!manhattanLineData.getFontName().equals(oldManhattanLineData.getFontName())) {// 字体类型
				isUpdate = true;
			} else if (manhattanLineData.getBodyStyle() != manhattanLineData.getBodyStyle()) {// 线条类型
				isUpdate = true;
			} else if (manhattanLineData.getFontSize() != oldManhattanLineData.getFontSize()) {// 字体大小
				isUpdate = true;
			} else if (manhattanLineData.getTextPoint() != oldManhattanLineData.getTextPoint()) {// 字体大小
				isUpdate = true;
			} else if (manhattanLineData.isTwoArrow() != oldManhattanLineData.isTwoArrow()) {// 双箭头
				isUpdate = true;
			}
			// 添加小线段开始
			for (JecnLineSegmentData lineSegmentData : manhattanLineData.getLineSegmentDataList()) {
				if (lineSegmentData.getUUID() == null) {
					isUpdate = true;
				}
			}
		} else if (lineData.getMapElemType() == MapElemType.VDividingLine
				|| lineData.getMapElemType() == MapElemType.HDividingLine
				|| lineData.getMapElemType() == MapElemType.DividingLine) { // 竖线,横线,斜线（分割线）
			JecnBaseDivedingLineData baseDivedingLineData = (JecnBaseDivedingLineData) lineData;
			JecnBaseDivedingLineData oldBaseDivedingLineData = (JecnBaseDivedingLineData) oldLineData;
			// 开始点的X
			if (baseDivedingLineData.getDiviLineCloneable().getStartXInt() != oldBaseDivedingLineData
					.getDiviLineCloneable().getStartXInt()) {
				isUpdate = true;
			} else if (baseDivedingLineData.getDiviLineCloneable().getStartYInt() != oldBaseDivedingLineData
					.getDiviLineCloneable().getStartYInt()) {// 开始点的Y
				isUpdate = true;
			} else if (baseDivedingLineData.getDiviLineCloneable().getEndXInt() != oldBaseDivedingLineData
					.getDiviLineCloneable().getEndXInt()) {// 结束点的X
				isUpdate = true;
			} else if (baseDivedingLineData.getDiviLineCloneable().getEndYInt() != oldBaseDivedingLineData
					.getDiviLineCloneable().getEndYInt()) {// 结束点的Y
				isUpdate = true;
			} else if (baseDivedingLineData.getBodyColor().getRGB() != oldBaseDivedingLineData.getBodyColor().getRGB()) {// 线条的颜色
				isUpdate = true;
			} else if (baseDivedingLineData.getBodyStyle() != oldBaseDivedingLineData.getBodyStyle()) {// 线条类型
				isUpdate = true;
			} else if (baseDivedingLineData.getZOrderIndex() != oldBaseDivedingLineData.getZOrderIndex()) {// 线的层级
				isUpdate = true;
			}
		}
		if (isUpdate) {
			getFlowStructureImageTtoLineData(lineData, flowId, flowStructureImageT);
		}
		return isUpdate;
	}

	/**
	 * 获取角色活动关联
	 * 
	 * @param activeFigureDataList
	 *            活动集合
	 * @param roleFigureDataList
	 *            角色集合
	 * @param vHLineDataDataList
	 *            分割线集合
	 * @return 角色与活动关联集合
	 */
	public static List<JecnRoleActiveT> setActiveIdForRoleFigure(List<JecnFigureData> activeFigureDataList,
			List<JecnFigureData> roleFigureDataList, List<JecnBaseLineData> vHLineDataDataList) {
		if (getWorkflow() == null) {
			return null;
		}
		// 存储关联关系的List
		List<JecnRoleActiveT> roleActiveList = new ArrayList<JecnRoleActiveT>();
		// 循环角色
		for (JecnFigureData roleFigureData : roleFigureDataList) {
			if (getWorkflow().getFlowMapData().isHFlag()) { // 面板横向
				int oneVHLine = 0;
				int twoVHLine = getWorkflow().getHeight();

				boolean isContinue = false;

				for (JecnBaseLineData vhLinePanel : vHLineDataDataList) {
					// 获取分割线
					JecnVHLineData vHLineData = (JecnVHLineData) vhLinePanel;
					// 角色是否压线
					if (vHLineData.getVhY() >= roleFigureData.getFigureDataCloneable().getY()
							&& vHLineData.getVhY() <= roleFigureData.getFigureDataCloneable().getY()
									+ roleFigureData.getFigureDataCloneable().getHeight()) {
						isContinue = true;

					}

					// 循环判断小于角色位置点的分割线，并且离角色最近的一条
					if (oneVHLine < vHLineData.getVhY()
							&& vHLineData.getVhY() < roleFigureData.getFigureDataCloneable().getY()) {
						oneVHLine = (int) vHLineData.getVhY();
					}
					if (twoVHLine > vHLineData.getVhY()
							&& vHLineData.getVhY() > roleFigureData.getFigureDataCloneable().getY()) {
						twoVHLine = (int) vHLineData.getVhY();
					}
				}

				if (isContinue) {
					continue;
				}

				if (vHLineDataDataList.size() > 0) {
					// 数据不为默认值
					for (JecnFigureData activeFigure : activeFigureDataList) {
						// 活动是否压线
						if (oneVHLine >= activeFigure.getFigureDataCloneable().getY()
								&& oneVHLine <= activeFigure.getFigureDataCloneable().getY()
										+ activeFigure.getFigureDataCloneable().getHeight()) {
							continue;

						}
						// 活动是否压线
						if (twoVHLine >= activeFigure.getFigureDataCloneable().getY()
								&& twoVHLine <= activeFigure.getFigureDataCloneable().getY()
										+ activeFigure.getFigureDataCloneable().getHeight()) {
							continue;

						}
						if (activeFigure.getFigureDataCloneable().getY() < twoVHLine
								&& activeFigure.getFigureDataCloneable().getY() > oneVHLine) {
							JecnRoleActiveT roleActiveT = new JecnRoleActiveT();
							// 角色主键ID
							roleActiveT.setFigureRoleId(Long.valueOf(roleFigureData.getFlowElementId()));
							// 活动的主键ID
							roleActiveT.setFigureActiveId(Long.valueOf(activeFigure.getFlowElementId()));
							// 角色的UIID
							roleActiveT.setRoleImageId(roleFigureData.getUUID());
							// 活动的UIID
							roleActiveT.setActiveFigureId(activeFigure.getUUID());
							roleActiveList.add(roleActiveT);
						}
					}
				}
			} else { // 面板纵向
				int oneVHLine = 0;
				int twoVHLine = getWorkflow().getWidth();
				boolean isContinue = false;
				for (JecnBaseLineData vhLinePanel : vHLineDataDataList) {
					// 获取分割线
					JecnVHLineData vHLineData = (JecnVHLineData) vhLinePanel;

					// 角色是否压线
					if (vHLineData.getVhX() >= roleFigureData.getFigureDataCloneable().getX()
							&& vHLineData.getVhX() <= roleFigureData.getFigureDataCloneable().getX()
									+ roleFigureData.getFigureDataCloneable().getWidth()) {
						isContinue = true;

					}

					// 循环判断小于角色位置点的分割线，并且离角色最近的一条
					if (oneVHLine < vHLineData.getVhX()
							&& vHLineData.getVhX() < roleFigureData.getFigureDataCloneable().getX()) {
						oneVHLine = (int) vHLineData.getVhX();

					}
					if (twoVHLine > vHLineData.getVhX()
							&& vHLineData.getVhX() > roleFigureData.getFigureDataCloneable().getX()) {
						twoVHLine = (int) vHLineData.getVhX();
					}
				}
				if (isContinue) {
					continue;
				}
				if (vHLineDataDataList.size() > 0) {
					// 数据不为默认值
					for (JecnFigureData activeFigure : activeFigureDataList) {
						if (activeFigure.getFigureDataCloneable().getX() < twoVHLine
								&& activeFigure.getFigureDataCloneable().getX() > oneVHLine) {

							// 角色是否压线
							if (oneVHLine >= activeFigure.getFigureDataCloneable().getX()
									&& oneVHLine <= activeFigure.getFigureDataCloneable().getX()
											+ activeFigure.getFigureDataCloneable().getWidth()) {
								continue;

							}
							// 角色是否压线
							if (twoVHLine >= activeFigure.getFigureDataCloneable().getX()
									&& twoVHLine <= activeFigure.getFigureDataCloneable().getX()
											+ activeFigure.getFigureDataCloneable().getWidth()) {
								continue;

							}
							JecnRoleActiveT roleActiveT = new JecnRoleActiveT();
							// 角色主键ID
							roleActiveT.setFigureRoleId(Long.valueOf(roleFigureData.getFlowElementId()));
							// 活动的主键ID
							roleActiveT.setFigureActiveId(Long.valueOf(activeFigure.getFlowElementId()));
							// 角色的UIID
							roleActiveT.setRoleImageId(roleFigureData.getUUID());
							// 活动的UIID
							roleActiveT.setActiveFigureId(activeFigure.getUUID());
							roleActiveList.add(roleActiveT);
						}
					}
				}
			}
		}
		return roleActiveList;
	}

	/**
	 * 单机版JecnModeFileT集合转换为服务器的JecnModeFileT(服务器用)
	 * 
	 * @param modeFileTList
	 *            单机版中JecnModeFileT的List
	 * @return 服务器的JecnModeFileT
	 */
	public static List<com.jecn.epros.server.bean.process.JecnModeFileT> switchJecnModeFileT(
			List<JecnModeFileT> modeFileTList, String figureUUID) {
		// 服务器的List
		List<com.jecn.epros.server.bean.process.JecnModeFileT> serverModeFileTList = new ArrayList<com.jecn.epros.server.bean.process.JecnModeFileT>();
		if (modeFileTList != null) {
			for (JecnModeFileT modeFileT : modeFileTList) {
				modeFileT.setFigureUUID(figureUUID);
				serverModeFileTList.add(getServerModeFileT(modeFileT));
			}
		}
		return serverModeFileTList;
	}

	/**
	 * 单机版JecnModeFileT集合转换为服务器的JecnModeFileT(服务器用)
	 * 
	 * @param modeFileTList
	 *            单机版中JecnModeFileT的List
	 * @return 服务器的JecnModeFileT
	 */
	public static List<com.jecn.epros.server.bean.process.inout.JecnFigureInoutT> switchJecnFigureInOutT(
			List<JecnFigureInoutT> figureInOutTList, String figureUUID) {
		// 服务器的List
		List<com.jecn.epros.server.bean.process.inout.JecnFigureInoutT> serverFigureInoutTList = new ArrayList<com.jecn.epros.server.bean.process.inout.JecnFigureInoutT>();
		if (serverFigureInoutTList != null) {
			for (JecnFigureInoutT modeFileT : figureInOutTList) {
				modeFileT.setFigureUUID(figureUUID);
				serverFigureInoutTList.add(getServerFigureInoutT(modeFileT));
			}
		}
		return serverFigureInoutTList;
	}

	/**
	 * 输出转换(服务器用)
	 * 
	 * @param modeFileT
	 *            单机版数据Bean
	 * @return 服务器输出bean
	 */
	public static com.jecn.epros.server.bean.process.JecnModeFileT getServerModeFileT(JecnModeFileT modeFileT) {
		com.jecn.epros.server.bean.process.JecnModeFileT serverModeFile = new com.jecn.epros.server.bean.process.JecnModeFileT();
		serverModeFile.setModeFileId(modeFileT.getModeFileId());
		serverModeFile.setFigureId(modeFileT.getFigureId());
		serverModeFile.setModeName(modeFileT.getModeName());
		serverModeFile.setFileNumber(modeFileT.getFileNumber());
		serverModeFile.setFileMId(modeFileT.getFileMId());// 输出的表单
		if (StringUtils.isBlank(modeFileT.getUUID())) {
			modeFileT.setUUID(DrawCommon.getUUID());
		}
		serverModeFile.setUUID(modeFileT.getUUID());

		serverModeFile.setFigureUUID(modeFileT.getFigureUUID());
		// serverModeFile.setListDeleteTempletT(modeFileT.getListDeleteTempletT());
		// // 要删除的样例
		// 样例
		if (modeFileT.getListJecnTempletT() != null) {
			serverModeFile.setListJecnTempletT(switchJecnTempletT(modeFileT.getListJecnTempletT()));
		}
		return serverModeFile;
	}

	/**
	 * 输出转换(服务器用)
	 * 
	 * @param modeFileT
	 *            单机版数据Bean
	 * @return 服务器输出bean
	 */
	public static com.jecn.epros.server.bean.process.inout.JecnFigureInoutT getServerFigureInoutT(
			JecnFigureInoutT figureInoutT) {
		com.jecn.epros.server.bean.process.inout.JecnFigureInoutT serverFigureInout = new com.jecn.epros.server.bean.process.inout.JecnFigureInoutT();
		serverFigureInout.setId(figureInoutT.getId());
		serverFigureInout.setFlowId(figureInoutT.getFlowId());
		serverFigureInout.setFigureId(figureInoutT.getFigureId());
		serverFigureInout.setExplain(figureInoutT.getExplain());
		serverFigureInout.setFileId(figureInoutT.getFileId());
		serverFigureInout.setSortId(figureInoutT.getSortId());
		serverFigureInout.setName(figureInoutT.getName());// 输出的表单
		serverFigureInout.setType(figureInoutT.getType());
		// 样例
		if (figureInoutT.getListSampleT() != null) {
			serverFigureInout.setListSampleT(switchInoutSampleT(figureInoutT.getListSampleT()));
		}
		return serverFigureInout;
	}

	/**
	 * 服务器JecnModeFileT集合转换为单机版的JecnModeFileT(单机版用)
	 * 
	 * @param modeFileTList
	 *            服务器中JecnModeFileT的List
	 * @return 单机版的JecnModeFileT
	 */
	public static List<JecnModeFileT> switchServerJecnModeFileT(
			List<com.jecn.epros.server.bean.process.JecnModeFileT> serverModeFileTList) {
		// 服务器的List
		List<JecnModeFileT> modeFileList = new ArrayList<JecnModeFileT>();
		if (serverModeFileTList != null) {
			for (com.jecn.epros.server.bean.process.JecnModeFileT modeFileT : serverModeFileTList) {
				modeFileList.add(getModeFileT(modeFileT));
			}
		}
		return modeFileList;
	}

	/**
	 * 新版 活动输入输出，服务端转设计器
	 * 
	 * @param serverFigureInoutTList
	 * @return
	 */
	public static List<JecnFigureInoutT> switchServerJecnFigureInoutT(
			List<com.jecn.epros.server.bean.process.inout.JecnFigureInoutT> serverFigureInoutTList) {
		// 服务器的List
		List<JecnFigureInoutT> figureInoutTs = new ArrayList<JecnFigureInoutT>();
		if (serverFigureInoutTList != null) {
			for (com.jecn.epros.server.bean.process.inout.JecnFigureInoutT figureInoutT : serverFigureInoutTList) {
				figureInoutTs.add(getFigureInoutT(figureInoutT));
			}
		}
		return figureInoutTs;
	}

	public static List<JecnFlowInoutT> switchServerJecnFlowInoutT(
			List<com.jecn.epros.server.bean.process.inout.JecnFlowInoutT> serverFlowInoutTList) {
		// 服务器的List
		List<JecnFlowInoutT> figureInoutTs = new ArrayList<JecnFlowInoutT>();
		if (serverFlowInoutTList != null) {
			for (com.jecn.epros.server.bean.process.inout.JecnFlowInoutT flowInoutT : serverFlowInoutTList) {
				figureInoutTs.add(getFlowInoutT(flowInoutT));
			}
		}
		return figureInoutTs;
	}

	public static JecnFlowInoutT getFlowInoutT(com.jecn.epros.server.bean.process.inout.JecnFlowInoutT serverFlowInoutT) {
		JecnFlowInoutT flowInoutT = new JecnFlowInoutT();
		try {
			PropertyUtils.copyProperties(flowInoutT, serverFlowInoutT);
			// 相关角色
			if (serverFlowInoutT.getFlowInoutPosTs() != null) {
				for (com.jecn.epros.server.bean.process.inout.JecnFlowInoutPosT serverInoutPosT : serverFlowInoutT
						.getFlowInoutPosTs()) {
					JecnFlowInoutPosT inoutPosT = new JecnFlowInoutPosT();
					PropertyUtils.copyProperties(inoutPosT, serverInoutPosT);
					flowInoutT.getListInoutPos().add(inoutPosT);
				}
			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		}
		return flowInoutT;
	}

	public static JecnFigureInoutT getFigureInoutT(
			com.jecn.epros.server.bean.process.inout.JecnFigureInoutT serverFigureInoutT) {
		JecnFigureInoutT figureInoutT = new JecnFigureInoutT();
		try {
			PropertyUtils.copyProperties(figureInoutT, serverFigureInoutT);
			// 样例
			if (serverFigureInoutT.getListSampleT() != null) {
				List<JecnFigureInoutSampleT> listSampleT = new ArrayList<JecnFigureInoutSampleT>();
				for (com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT serverInoutSampleT : serverFigureInoutT
						.getListSampleT()) {
					JecnFigureInoutSampleT inoutSampleT = new JecnFigureInoutSampleT();
					PropertyUtils.copyProperties(inoutSampleT, serverInoutSampleT);
					listSampleT.add(inoutSampleT);
				}
				figureInoutT.setListSampleT(listSampleT);
			}
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		}
		return figureInoutT;
	}

	/**
	 * 获取单机版需要的输入Bean(单机版用)
	 * 
	 * @param modeFileT
	 *            服务器数据Bean
	 * @return 单机版输出bean
	 */
	public static JecnModeFileT getModeFileT(com.jecn.epros.server.bean.process.JecnModeFileT serverModeFileT) {
		JecnModeFileT modeFile = new JecnModeFileT();
		modeFile.setModeFileId(serverModeFileT.getModeFileId());
		modeFile.setFigureId(serverModeFileT.getFigureId());
		modeFile.setModeName(serverModeFileT.getModeName());
		modeFile.setFileNumber(serverModeFileT.getFileNumber());
		modeFile.setFileMId(serverModeFileT.getFileMId());// 输出的表单
		modeFile.setFigureUUID(serverModeFileT.getFigureUUID());
		modeFile.setUUID(serverModeFileT.getUUID());
		// 样例
		if (serverModeFileT.getListJecnTempletT() != null) {
			modeFile.setListJecnTempletT(switchServerJecnTempletT(serverModeFileT.getListJecnTempletT()));
		}

		return modeFile;
	}

	/**
	 * 单机版JecnTempletT集合转换为服务器的JecnTempletT(服务器用)
	 * 
	 * @param templetList
	 *            单机版样例的集合
	 * @return 服务器JecnTempletT
	 */
	public static List<com.jecn.epros.server.bean.process.JecnTempletT> switchJecnTempletT(
			List<JecnTempletT> templetList) {
		// 服务器的List
		List<com.jecn.epros.server.bean.process.JecnTempletT> serverTempletTList = new ArrayList<com.jecn.epros.server.bean.process.JecnTempletT>();
		if (templetList != null) {
			for (JecnTempletT templetT : templetList) {
				serverTempletTList.add(getServerTempletT(templetT));
			}
		}
		return serverTempletTList;
	}

	/**
	 * 单机版JecnTempletT集合转换为服务器的JecnTempletT(服务器用)
	 * 
	 * @param inoutSampleList
	 *            单机版样例的集合
	 * @return 服务器JecnTempletT
	 */
	public static List<com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT> switchInoutSampleT(
			List<JecnFigureInoutSampleT> inoutSampleList) {
		// 服务器的List
		List<com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT> serverTempletTList = new ArrayList<com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT>();
		if (inoutSampleList != null) {
			for (JecnFigureInoutSampleT inoutSampleT : inoutSampleList) {
				serverTempletTList.add(getServerSampleT(inoutSampleT));
			}
		}
		return serverTempletTList;
	}

	/**
	 * 获取服务器需要的样例(服务器用)
	 * 
	 * @param templetT
	 *            单机版样例的Bean
	 * @return 服务器的Bean
	 */
	public static com.jecn.epros.server.bean.process.JecnTempletT getServerTempletT(JecnTempletT templetT) {
		com.jecn.epros.server.bean.process.JecnTempletT serverTemplate = new com.jecn.epros.server.bean.process.JecnTempletT();
		// 主键ID
		serverTemplate.setId(templetT.getId());
		// 输出的模板主键ID
		serverTemplate.setModeFileId(templetT.getModeFileId());
		// 文件Id
		serverTemplate.setFileId(templetT.getFileId());
		// 文件名称
		serverTemplate.setFileName(templetT.getFileName());
		// 文件编号
		serverTemplate.setFileNumber(templetT.getFileName());
		serverTemplate.setModeUUID(templetT.getModeUUID());

		serverTemplate.setUUID(templetT.getUUID());
		return serverTemplate;
	}

	/**
	 * 获取服务器需要的样例(服务器用)
	 * 
	 * @param templetT
	 *            单机版样例的Bean
	 * @return 服务器的Bean
	 */
	public static com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT getServerSampleT(
			JecnFigureInoutSampleT inoutSampleT) {
		com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT serverInoutSampleT = new com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT();
		// 主键ID
		serverInoutSampleT.setId(inoutSampleT.getId());
		serverInoutSampleT.setFlowId(inoutSampleT.getFlowId());
		serverInoutSampleT.setFileId(inoutSampleT.getFileId());
		serverInoutSampleT.setType(inoutSampleT.getType());
		// 输出ID
		serverInoutSampleT.setInoutId(inoutSampleT.getInoutId());
		return serverInoutSampleT;
	}

	/**
	 * 服务器JecnTempletT集合转换为单机版的JecnTempletT(单机版用)
	 * 
	 * @param templetList
	 *            服务器样例的集合
	 * @return 单机版JecnTempletT
	 */
	public static List<JecnTempletT> switchServerJecnTempletT(
			List<com.jecn.epros.server.bean.process.JecnTempletT> serverTempletList) {
		// 服务器的List
		List<JecnTempletT> templetTList = new ArrayList<JecnTempletT>();
		if (serverTempletList != null) {
			for (com.jecn.epros.server.bean.process.JecnTempletT serverTempletT : serverTempletList) {
				templetTList.add(getTempletT(serverTempletT));
			}
		}
		return templetTList;
	}

	/**
	 * 获取单机版需要的样例(单机版用)
	 * 
	 * @param templetT
	 *            单机版样例的Bean
	 * @return 服务器的Bean
	 */
	public static JecnTempletT getTempletT(com.jecn.epros.server.bean.process.JecnTempletT serverTempletT) {
		JecnTempletT template = new JecnTempletT();
		// 主键ID
		template.setId(serverTempletT.getId());
		// 输出的模板主键ID
		template.setModeFileId(serverTempletT.getModeFileId());
		// 文件Id
		template.setFileId(serverTempletT.getFileId());
		// 文件名称
		template.setFileName(serverTempletT.getFileName());
		// 文件编号
		template.setFileNumber(serverTempletT.getFileNumber());

		template.setModeUUID(serverTempletT.getModeUUID());
		template.setUUID(serverTempletT.getUUID());
		return template;
	}

	/**
	 * 单机版JecnActivityFileT集合转换为服务器的JecnActivityFileT(服务器用)
	 * 
	 * @param activityFileList
	 *            单机版输出的集合
	 * @return 服务器JecnActivityFileT
	 */
	public static List<com.jecn.epros.server.bean.process.JecnActivityFileT> switchActivityFileT(
			List<JecnActivityFileT> activityFileList, String figureUUID) {
		// 服务器的List
		List<com.jecn.epros.server.bean.process.JecnActivityFileT> serverActivityFileTList = new ArrayList<com.jecn.epros.server.bean.process.JecnActivityFileT>();
		if (activityFileList != null) {
			for (JecnActivityFileT activityFileT : activityFileList) {
				activityFileT.setFigureUUID(figureUUID);
				serverActivityFileTList.add(getServerJecnActivityFileT(activityFileT));
			}
		}
		return serverActivityFileTList;
	}

	/**
	 * 获取服务器输入(服务器用)
	 * 
	 * @param activityFileT
	 *            单机版输入
	 * @return 服务器数据bean
	 */
	public static com.jecn.epros.server.bean.process.JecnActivityFileT getServerJecnActivityFileT(
			JecnActivityFileT activityFileT) {
		com.jecn.epros.server.bean.process.JecnActivityFileT serverActivityFile = new com.jecn.epros.server.bean.process.JecnActivityFileT();
		serverActivityFile.setFileId(activityFileT.getFileId());// 主键ID
		serverActivityFile.setFileName(activityFileT.getFileName());// 文件名称（不存放在数据库）
		serverActivityFile.setFileType(activityFileT.getFileType());// 文件类型（0是输入，1是操作规范）
		serverActivityFile.setFigureId(activityFileT.getFigureId());// 活动FigureId
		serverActivityFile.setFileSId(activityFileT.getFileSId());// 文件名称
		serverActivityFile.setFileNumber(activityFileT.getFileNumber()); // 文件编号
		if (StringUtils.isBlank(activityFileT.getUUID())) {
			activityFileT.setUUID(DrawCommon.getUUID());
		}
		serverActivityFile.setUUID(activityFileT.getUUID());
		serverActivityFile.setFigureUUID(activityFileT.getFigureUUID());
		return serverActivityFile;
	}

	/**
	 * 服务器JecnActivityFileT集合转换为单机版的JecnActivityFileT(单机版用)
	 * 
	 * @param activityFileList
	 *            服务器输出的集合
	 * @return 单机版JecnActivityFileT
	 */
	public static List<JecnActivityFileT> switchServerActivityFileT(
			List<com.jecn.epros.server.bean.process.JecnActivityFileT> serverActivityFileList) {
		// 服务器的List
		List<JecnActivityFileT> activityFileTList = new ArrayList<JecnActivityFileT>();
		if (serverActivityFileList != null) {
			for (com.jecn.epros.server.bean.process.JecnActivityFileT serverActivityFileT : serverActivityFileList) {
				activityFileTList.add(getServerJecnActivityFileT(serverActivityFileT));
			}
		}
		return activityFileTList;
	}

	/**
	 * 获取单机版输入(单机版用)
	 * 
	 * @param activityFileT
	 *            服务器输入
	 * @return 单机版数据bean
	 */
	public static JecnActivityFileT getServerJecnActivityFileT(
			com.jecn.epros.server.bean.process.JecnActivityFileT serverActivityFileT) {
		JecnActivityFileT activityFile = new JecnActivityFileT();
		activityFile.setFileId(serverActivityFileT.getFileId());// 主键ID
		activityFile.setFileName(serverActivityFileT.getFileName());// 文件名称（不存放在数据库）
		activityFile.setFileType(serverActivityFileT.getFileType());// 文件类型（0是输入，1是操作规范）
		activityFile.setFigureId(serverActivityFileT.getFigureId());// 活动FigureId
		activityFile.setFileSId(serverActivityFileT.getFileSId());// 文件名称
		activityFile.setFileNumber(serverActivityFileT.getFileNumber());// 文件编号
		activityFile.setUUID(serverActivityFileT.getUUID());
		activityFile.setFigureUUID(serverActivityFileT.getFigureUUID());
		return activityFile;
	}

	/**
	 * 服务器JecnFigureFileTBean集合转换为单机版的JecnFigureFileTBean(单机版用)
	 * 
	 * @param serverFigureFileTBeanList
	 *            服务器输出的集合
	 * @return 单机版figureFileTBeanList
	 */
	public static List<JecnFigureFileTBean> switchServerFigureFileTBean(
			List<com.jecn.epros.server.bean.process.JecnFigureFileTBean> serverFigureFileTBeanList) {
		// 服务器的List
		List<JecnFigureFileTBean> figureFileTBeanList = new ArrayList<JecnFigureFileTBean>();
		if (serverFigureFileTBeanList != null) {
			for (com.jecn.epros.server.bean.process.JecnFigureFileTBean serverFigureFileTBean : serverFigureFileTBeanList) {
				figureFileTBeanList.add(getServerFigureFileTBean(serverFigureFileTBean));
			}
		}
		return figureFileTBeanList;
	}

	/**
	 * 单机版获取流程地图附件（单机版专用）
	 * 
	 * @param figureFileTBean
	 *            服务器输入
	 * @return 单机版数据 JecnFigureFileTBean
	 */
	public static JecnFigureFileTBean getServerFigureFileTBean(
			com.jecn.epros.server.bean.process.JecnFigureFileTBean figureFileTBean) {
		JecnFigureFileTBean fileTBean = new JecnFigureFileTBean();
		fileTBean.setId(figureFileTBean.getId());
		fileTBean.setFigureId(figureFileTBean.getFigureId());
		fileTBean.setFileId(figureFileTBean.getFileId());
		fileTBean.setFigureType(figureFileTBean.getFigureType());
		fileTBean.setFileName(figureFileTBean.getFileName());
		fileTBean.setFigureUUID(figureFileTBean.getFigureUUID());
		return fileTBean;
	}

	/**
	 * 单机版JecnFigureFileTBean集合转换为服务器的JecnFigureFileTBean(服务版用)
	 * 
	 * @param serverFigureFileTBeanList
	 *            服务器输出的集合
	 * @return 单机版figureFileTBeanList
	 */
	public static List<com.jecn.epros.server.bean.process.JecnFigureFileTBean> switchLocalFigureFileTBean(
			List<JecnFigureFileTBean> localFigureFileTBeanList, String figureUUID) {
		// 服务器的List
		List<com.jecn.epros.server.bean.process.JecnFigureFileTBean> figureFileTBeanList = new ArrayList<com.jecn.epros.server.bean.process.JecnFigureFileTBean>();
		if (localFigureFileTBeanList != null) {
			for (JecnFigureFileTBean serverFigureFileTBean : localFigureFileTBeanList) {
				serverFigureFileTBean.setFigureUUID(figureUUID);
				figureFileTBeanList.add(getLocalFigureFileTBean(serverFigureFileTBean));
			}
		}
		return figureFileTBeanList;
	}

	/**
	 * 单机版获取流程地图附件（单机版专用）
	 * 
	 * @param figureFileTBean
	 *            服务器输入
	 * @return 单机版数据 JecnFigureFileTBean
	 */
	public static com.jecn.epros.server.bean.process.JecnFigureFileTBean getLocalFigureFileTBean(
			JecnFigureFileTBean figureFileTBean) {
		com.jecn.epros.server.bean.process.JecnFigureFileTBean fileTBean = new com.jecn.epros.server.bean.process.JecnFigureFileTBean();
		if (StringUtils.isBlank(figureFileTBean.getId())) {
			figureFileTBean.setId(DrawCommon.getUUID());
		}
		fileTBean.setId(figureFileTBean.getId());
		fileTBean.setFigureId(figureFileTBean.getFigureId());
		fileTBean.setFileId(figureFileTBean.getFileId());
		fileTBean.setFigureType(figureFileTBean.getFigureType());
		fileTBean.setFileName(figureFileTBean.getFileName());
		fileTBean.setFigureUUID(figureFileTBean.getFigureUUID());
		return fileTBean;
	}

	/**
	 * 获取服务器指标(服务器用)
	 * 
	 * @param activityFileList
	 *            单机版指标的List
	 * @return 服务器指标的List
	 */
	public static List<com.jecn.epros.server.bean.process.JecnRefIndicatorsT> switchJecnRefIndicatorsT(
			List<JecnRefIndicatorsT> activityFileList, String figureUUID) {
		// 服务器的List
		List<com.jecn.epros.server.bean.process.JecnRefIndicatorsT> serverRefIndicatorsTList = new ArrayList<com.jecn.epros.server.bean.process.JecnRefIndicatorsT>();
		if (activityFileList != null) {
			for (JecnRefIndicatorsT refIndicatorsT : activityFileList) {
				refIndicatorsT.setFigureUUID(figureUUID);
				serverRefIndicatorsTList.add(getServerJecnRefIndicatorsT(refIndicatorsT));
			}
		}
		return serverRefIndicatorsTList;
	}

	/**
	 * 获取服务器指标bean(服务器用)
	 * 
	 * @param refIndicatorsT
	 *            单机版指标Bean
	 * @return 服务器指标Bean
	 */
	public static com.jecn.epros.server.bean.process.JecnRefIndicatorsT getServerJecnRefIndicatorsT(
			JecnRefIndicatorsT refIndicatorsT) {
		com.jecn.epros.server.bean.process.JecnRefIndicatorsT serverRefIndicators = new com.jecn.epros.server.bean.process.JecnRefIndicatorsT();
		refIndicatorsT.setUUID(refIndicatorsT.getUUID());
		serverRefIndicators.setFigureUUID(refIndicatorsT.getFigureUUID());
		serverRefIndicators.setId(refIndicatorsT.getId());// 主键ID
		serverRefIndicators.setIndicatorName(refIndicatorsT.getIndicatorName());// 指标名
		serverRefIndicators.setIndicatorValue(refIndicatorsT.getIndicatorValue());// 指标值
		serverRefIndicators.setActivityId(refIndicatorsT.getActivityId());// 活动FigureID

		return serverRefIndicators;
	}

	/**
	 * 获取单机版指标(单机版用)
	 * 
	 * @param activityFileList
	 *            服务器指标的List
	 * @return 单机版指标的List
	 */
	public static List<JecnRefIndicatorsT> switchServerJecnRefIndicatorsT(
			List<com.jecn.epros.server.bean.process.JecnRefIndicatorsT> serverActivityFileList) {
		// 服务器的List
		List<JecnRefIndicatorsT> refIndicatorsTList = new ArrayList<JecnRefIndicatorsT>();
		if (serverActivityFileList != null) {
			for (com.jecn.epros.server.bean.process.JecnRefIndicatorsT serverRefIndicatorsT : serverActivityFileList) {
				refIndicatorsTList.add(getServerJecnRefIndicatorsT(serverRefIndicatorsT));
			}
		}
		return refIndicatorsTList;
	}

	/**
	 * 获取单机版指标bean(单机版用)
	 * 
	 * @param refIndicatorsT
	 *            服务器指标Bean
	 * @return 单机版指标Bean
	 */
	public static JecnRefIndicatorsT getServerJecnRefIndicatorsT(
			com.jecn.epros.server.bean.process.JecnRefIndicatorsT serverRefIndicatorsT) {
		JecnRefIndicatorsT refIndicators = new JecnRefIndicatorsT();
		refIndicators.setId(serverRefIndicatorsT.getId());// 主键ID
		refIndicators.setIndicatorName(serverRefIndicatorsT.getIndicatorName());// 指标名
		refIndicators.setIndicatorValue(serverRefIndicatorsT.getIndicatorValue());// 指标值
		refIndicators.setActivityId(serverRefIndicatorsT.getActivityId());// 活动FigureID

		return refIndicators;
	}

	/**
	 * 获取服务器岗位与角色关联(服务器用)
	 * 
	 * @param flowStationTList
	 *            单机版角色与岗位的关联
	 * @return 服务器角色与岗位的关联关系
	 */
	public static List<com.jecn.epros.server.bean.process.JecnFlowStationT> switchJecnFlowStationT(
			List<JecnFlowStationT> flowStationTList, String figureUUID) {
		// 服务器的List
		List<com.jecn.epros.server.bean.process.JecnFlowStationT> serverFlowStationTList = new ArrayList<com.jecn.epros.server.bean.process.JecnFlowStationT>();
		if (flowStationTList != null) {
			for (JecnFlowStationT flowStationT : flowStationTList) {
				flowStationT.setFigureUUID(figureUUID);
				serverFlowStationTList.add(getServerJecnFlowStationT(flowStationT));
			}
		}
		return serverFlowStationTList;
	}

	/**
	 * 获取服务器角色与岗位关联(服务器用)
	 * 
	 * @param refIndicatorsT
	 *            设计器的bean
	 * @return 服务器Bean
	 */
	public static com.jecn.epros.server.bean.process.JecnFlowStationT getServerJecnFlowStationT(
			JecnFlowStationT flowStationT) {
		com.jecn.epros.server.bean.process.JecnFlowStationT serverFlowStationT = new com.jecn.epros.server.bean.process.JecnFlowStationT();
		if (StringUtils.isBlank(flowStationT.getUUID())) {
			flowStationT.setUUID(DrawCommon.getUUID());
		}
		serverFlowStationT.setUUID(flowStationT.getUUID());
		serverFlowStationT.setFigureUUID(flowStationT.getFigureUUID());
		serverFlowStationT.setFlowStationId(flowStationT.getFlowStationId());// 主键ID
		serverFlowStationT.setFigureFlowId(flowStationT.getFigureFlowId());// 对应角色（JecnFlowStructureImageT）的figureId
		serverFlowStationT.setFigurePositionId(flowStationT.getFigurePositionId());// 对应岗位（JecnFlowOrgImage）的figureId或者对于岗位组的Id
		serverFlowStationT.setType(flowStationT.getType());// 岗位为0,岗位组为1
		serverFlowStationT.setStationName(flowStationT.getStationName());// 岗位或岗位组名称

		return serverFlowStationT;
	}

	/**
	 * 获取单机版岗位与角色关联(单机版用)
	 * 
	 * @param flowStationTList
	 *            单机版角色与岗位的关联
	 * @return 服务器角色与岗位的关联关系
	 */
	public static List<JecnFlowStationT> switchServerJecnFlowStationT(
			List<com.jecn.epros.server.bean.process.JecnFlowStationT> serverFlowStationTList) {
		// 服务器的List
		List<JecnFlowStationT> flowStationTList = new ArrayList<JecnFlowStationT>();
		if (serverFlowStationTList != null) {
			for (com.jecn.epros.server.bean.process.JecnFlowStationT serverFlowStationT : serverFlowStationTList) {
				flowStationTList.add(getJecnFlowStationT(serverFlowStationT));
			}
		}
		return flowStationTList;
	}

	/**
	 * 获取单机版角色与岗位关联(单机版用)
	 * 
	 * @param activeOnLineTBean
	 *            设计器的bean
	 * @return 服务器Bean
	 */
	public static JecnFlowStationT getJecnFlowStationT(
			com.jecn.epros.server.bean.process.JecnFlowStationT serverFlowStationT) {
		JecnFlowStationT flowStationT = new JecnFlowStationT();
		flowStationT.setFlowStationId(serverFlowStationT.getFlowStationId());// 主键ID
		flowStationT.setFigureFlowId(serverFlowStationT.getFigureFlowId());// 对应角色（JecnFlowStructureImageT）的figureId
		flowStationT.setFigurePositionId(serverFlowStationT.getFigurePositionId());// 对应岗位（JecnFlowOrgImage）的figureId或者对于岗位组的Id
		flowStationT.setType(serverFlowStationT.getType());// 岗位为0,岗位组为1
		flowStationT.setStationName(serverFlowStationT.getStationName());// 岗位或岗位组名称
		flowStationT.setFigureUUID(serverFlowStationT.getFigureUUID());
		flowStationT.setUUID(serverFlowStationT.getUUID());
		return flowStationT;
	}

	/**
	 * 获取单机版线上信息(单机版用)
	 * 
	 * @param serverOnLineTBean
	 *            设计器的bean
	 * @return 服务器Bean
	 */
	public static JecnActiveOnLineTBean getJecnActiveOnLineTBean(
			com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean serverOnLineTBean) {
		JecnActiveOnLineTBean activeOnLineTBean = new JecnActiveOnLineTBean();
		activeOnLineTBean.setId(serverOnLineTBean.getId());
		activeOnLineTBean.setActiveId(serverOnLineTBean.getActiveId());
		activeOnLineTBean.setOnLineTime(serverOnLineTBean.getOnLineTime());
		activeOnLineTBean.setProcessId(serverOnLineTBean.getProcessId());
		activeOnLineTBean.setSysName(serverOnLineTBean.getSysName());
		activeOnLineTBean.setToolId(serverOnLineTBean.getToolId());
		activeOnLineTBean.setInformationDescription(serverOnLineTBean.getInformationDescription());
		activeOnLineTBean.setFigureUUID(serverOnLineTBean.getFigureUUID());
		return activeOnLineTBean;
	}

	/**
	 * 获取服务线上信息(服务 保存)
	 * 
	 * @param activeOnLineTBean
	 *            设计器的bean
	 * @return 服务器Bean
	 */
	public static com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean getServerJecnActiveOnLineTBean(
			JecnActiveOnLineTBean activeOnLineTBean) {
		com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean serverOnLineTBean = new com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean();
		if (StringUtils.isBlank(activeOnLineTBean.getId())) {
			activeOnLineTBean.setId(DrawCommon.getUUID());
		}
		serverOnLineTBean.setId(activeOnLineTBean.getId());
		serverOnLineTBean.setFigureUUID(activeOnLineTBean.getFigureUUID());
		serverOnLineTBean.setActiveId(activeOnLineTBean.getActiveId());
		serverOnLineTBean.setOnLineTime(activeOnLineTBean.getOnLineTime());
		serverOnLineTBean.setProcessId(activeOnLineTBean.getProcessId());
		serverOnLineTBean.setSysName(activeOnLineTBean.getSysName());
		serverOnLineTBean.setInformationDescription(activeOnLineTBean.getInformationDescription());
		serverOnLineTBean.setToolId(activeOnLineTBean.getToolId());
		return serverOnLineTBean;
	}

	/**
	 * 获取单机版线上信息集合(单机版用)
	 * 
	 * @param serverJecnActiveOnLineTBeanList
	 *            服务数据库获取的线上信息集合
	 * @return 单机版线上信息集合 List<JecnActiveOnLineTBean>
	 */
	public static List<JecnActiveOnLineTBean> switchServerJecnActiveOnLineTBean(
			List<com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean> serverJecnActiveOnLineTBeanList) {
		// 服务器的List
		List<JecnActiveOnLineTBean> onLineTBeanList = new ArrayList<JecnActiveOnLineTBean>();
		if (serverJecnActiveOnLineTBeanList != null) {
			for (com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean serverOnLineT : serverJecnActiveOnLineTBeanList) {
				onLineTBeanList.add(getJecnActiveOnLineTBean(serverOnLineT));
			}
		}
		return onLineTBeanList;
	}

	/**
	 * 获取服务线上信息集合(单机版用)
	 * 
	 * @param serverJecnActiveOnLineTBeanList
	 *            服务数据库获取的线上信息集合
	 * @return 服务 线上信息集合 List<JecnActiveOnLineTBean>
	 */
	public static List<com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean> switchLocalJecnActiveOnLineTBean(
			List<JecnActiveOnLineTBean> localJecnActiveOnLineTBeanList, String figureUUID) {
		// 服务器的List
		List<com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean> onLineTBeanList = new ArrayList<com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean>();
		if (localJecnActiveOnLineTBeanList != null) {
			for (JecnActiveOnLineTBean serverOnLineT : localJecnActiveOnLineTBeanList) {
				serverOnLineT.setFigureUUID(figureUUID);
				onLineTBeanList.add(getServerJecnActiveOnLineTBean(serverOnLineT));
			}
		}
		return onLineTBeanList;
	}

	/**
	 * 获取单机版活动与标准关系表信息(单机版用)
	 * 
	 * @param serverJecnActiveStandardBeanT
	 *            设计器的bean
	 * @return 单机版Bean
	 */
	public static JecnActiveStandardBeanT getJecnActiveStandardBeanT(
			com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT serverJecnActiveStandardBeanT) {
		JecnActiveStandardBeanT standardBeanT = new JecnActiveStandardBeanT();
		standardBeanT.setId(serverJecnActiveStandardBeanT.getId());
		standardBeanT.setActiveId(serverJecnActiveStandardBeanT.getActiveId());
		standardBeanT.setClauseId(serverJecnActiveStandardBeanT.getClauseId());
		standardBeanT.setRelaName(serverJecnActiveStandardBeanT.getRelaName());
		standardBeanT.setRelaType(serverJecnActiveStandardBeanT.getRelaType());
		standardBeanT.setStandardId(serverJecnActiveStandardBeanT.getStandardId());
		standardBeanT.setFigureUUID(serverJecnActiveStandardBeanT.getFigureUUID());
		return standardBeanT;
	}

	/**
	 * 获取服务保存活动与标准关系表信息(保存专用)
	 * 
	 * @param localJecnActiveStandardBeanT
	 *            设计器的bean
	 * @return 服务Bean
	 */
	public static com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT getServerJecnActiveStandardBeanT(
			JecnActiveStandardBeanT localJecnActiveStandardBeanT) {
		com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT serverStandardBeanT = new com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT();
		if (StringUtils.isBlank(localJecnActiveStandardBeanT.getId())) {
			localJecnActiveStandardBeanT.setId(DrawCommon.getUUID());
		}
		serverStandardBeanT.setId(localJecnActiveStandardBeanT.getId());
		serverStandardBeanT.setFigureUUID(localJecnActiveStandardBeanT.getFigureUUID());
		serverStandardBeanT.setActiveId(localJecnActiveStandardBeanT.getActiveId());
		serverStandardBeanT.setClauseId(localJecnActiveStandardBeanT.getClauseId());
		serverStandardBeanT.setRelaName(localJecnActiveStandardBeanT.getRelaName());
		serverStandardBeanT.setRelaType(localJecnActiveStandardBeanT.getRelaType());
		serverStandardBeanT.setStandardId(localJecnActiveStandardBeanT.getStandardId());
		return serverStandardBeanT;
	}

	/**
	 * 获取单机活动和标准关联集合(单机版用)
	 * 
	 * @param serverJecnActiveStandardBeanTList
	 *            服务数据库获取的线上信息集合
	 * @return 单机版线上信息集合 List<JecnActiveOnLineTBean>
	 */
	public static List<JecnActiveStandardBeanT> switchServerJecnActiveStandardBeanT(
			List<com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT> serverJecnActiveStandardBeanTList) {
		// 服务器的List
		List<JecnActiveStandardBeanT> activeStandardBeanTList = new ArrayList<JecnActiveStandardBeanT>();
		if (serverJecnActiveStandardBeanTList != null) {
			for (com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT serverOnLineT : serverJecnActiveStandardBeanTList) {
				activeStandardBeanTList.add(getJecnActiveStandardBeanT(serverOnLineT));
			}
		}
		return activeStandardBeanTList;
	}

	/**
	 * 获取单机活动和控制点集合(单机版用) C
	 * 
	 * @param serverJecnActiveControlPointTList
	 *            服务数据库获取的线上信息集合
	 * @return 单机版线上信息集合 List<JecnActiveOnLineTBean>
	 */
	public static List<JecnControlPointT> serverJecnActiveControlPointTList(
			List<com.jecn.epros.server.bean.integration.JecnControlPointT> serverJecnActiveControlPointTList) {
		// 服务器的List
		List<JecnControlPointT> activeControlPoinBeanTList = new ArrayList<JecnControlPointT>();
		if (serverJecnActiveControlPointTList != null) {
			for (com.jecn.epros.server.bean.integration.JecnControlPointT jecnControlPointT : serverJecnActiveControlPointTList) {
				activeControlPoinBeanTList.add(getJecnControlPointT(jecnControlPointT));
			}
		}
		return activeControlPoinBeanTList;
	}

	/**
	 * 获取保存活动和标准关联集合(保存专用)
	 * 
	 * @param serverJecnActiveStandardBeanTList
	 *            服务数据库获取的线上信息集合
	 * @return 单机版线上信息集合 List<JecnActiveOnLineTBean>
	 */
	public static List<com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT> switchLocalJecnActiveStandardBeanT(
			List<JecnActiveStandardBeanT> localJecnActiveStandardBeanTList, String figureUUID) {
		// 服务器的List
		List<com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT> serverStandardBeanTList = new ArrayList<com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT>();
		if (localJecnActiveStandardBeanTList != null) {
			for (JecnActiveStandardBeanT localStandardBeanT : localJecnActiveStandardBeanTList) {
				localStandardBeanT.setFigureUUID(figureUUID);
				serverStandardBeanTList.add(getServerJecnActiveStandardBeanT(localStandardBeanT));
			}
		}
		return serverStandardBeanTList;
	}

	/**
	 * 获取保存活动和控制点集合(保存专用) C
	 * 
	 * @param serverJecnActiveStandardBeanTList
	 *            服务数据库获取的线上信息集合
	 * @return 单机版线上信息集合 List<JecnActiveOnLineTBean>
	 */
	public static List<com.jecn.epros.server.bean.integration.JecnControlPointT> swithLocalJecnActiveControlPoint(
			List<JecnControlPointT> listControlPointT, String figureUUID) {

		// 实例化List
		List<com.jecn.epros.server.bean.integration.JecnControlPointT> serverControlPointBeanTList = new ArrayList<com.jecn.epros.server.bean.integration.JecnControlPointT>();
		// 活动控制点修改为一个活动对应一个多个控制点 所以需要做如下修改 C
		if (listControlPointT != null) { // 控制点集合
			// 将activeData传回的要更新的控制点存放到activityUpdateData（更新集合）中
			for (JecnControlPointT controlPointT : listControlPointT) {
				// getServerJecnControlPointT()是单机版Bean和服务端Bean转换方法 转换后才能添加到集合中
				controlPointT.setFigureUUID(figureUUID);
				serverControlPointBeanTList.add(getServerJecnControlPointT(controlPointT));
			}
		}
		return serverControlPointBeanTList;
	}

	/**
	 * 获取单机版控制点信息(单机版用)
	 * 
	 * @param serverOnLineTBean
	 *            设计器的bean
	 * @return 单机版Bean
	 */
	public static JecnControlPointT getJecnControlPointT(
			com.jecn.epros.server.bean.integration.JecnControlPointT serverControlPointT) {
		JecnControlPointT controlPointT = new JecnControlPointT();
		if (controlPointT == null) {
			return null;
		}
		controlPointT.setId(serverControlPointT.getId());
		controlPointT.setActiveId(serverControlPointT.getActiveId());
		controlPointT.setControlCode(serverControlPointT.getControlCode());
		controlPointT.setCreatePersonId(serverControlPointT.getCreatePersonId());
		controlPointT.setCreateTime(serverControlPointT.getCreateTime());
		controlPointT.setFrequency(serverControlPointT.getFrequency());
		controlPointT.setKeyPoint(serverControlPointT.getKeyPoint());
		controlPointT.setMethod(serverControlPointT.getMethod());
		controlPointT.setNote(serverControlPointT.getNote());
		controlPointT.setRiskId(serverControlPointT.getRiskId());
		controlPointT.setTargetId(serverControlPointT.getTargetId());
		controlPointT.setType(serverControlPointT.getType());
		controlPointT.setUpdatePersonId(serverControlPointT.getUpdatePersonId());
		controlPointT.setUpdateTime(serverControlPointT.getUpdateTime());
		controlPointT.setControlTarget(serverControlPointT.getControlTarget());
		controlPointT.setRiskNum(serverControlPointT.getRiskNum());
		controlPointT.setIsDelete(serverControlPointT.getIsDelete());
		controlPointT.setFigureUUID(serverControlPointT.getFigureUUID());
		return controlPointT;
	}

	/**
	 * 获取服务控制点信息(保存用)
	 * 
	 * @param serverOnLineTBean
	 *            设计器的bean
	 * @return 单机版Bean
	 */
	public static com.jecn.epros.server.bean.integration.JecnControlPointT getServerJecnControlPointT(
			JecnControlPointT controlPointT) {
		com.jecn.epros.server.bean.integration.JecnControlPointT serverControlPointT = new com.jecn.epros.server.bean.integration.JecnControlPointT();
		if (controlPointT == null) {
			return null;
		}
		if (StringUtils.isBlank(controlPointT.getId())) {
			controlPointT.setId(DrawCommon.getUUID());
		}
		serverControlPointT.setId(controlPointT.getId());
		serverControlPointT.setFigureUUID(controlPointT.getFigureUUID());
		serverControlPointT.setActiveId(controlPointT.getActiveId());
		serverControlPointT.setControlCode(controlPointT.getControlCode());
		serverControlPointT.setCreatePersonId(controlPointT.getCreatePersonId());
		serverControlPointT.setCreateTime(controlPointT.getCreateTime());
		serverControlPointT.setFrequency(controlPointT.getFrequency());
		serverControlPointT.setKeyPoint(controlPointT.getKeyPoint());
		serverControlPointT.setMethod(controlPointT.getMethod());
		serverControlPointT.setNote(controlPointT.getNote());
		serverControlPointT.setRiskId(controlPointT.getRiskId());
		serverControlPointT.setTargetId(controlPointT.getTargetId());
		serverControlPointT.setType(controlPointT.getType());
		serverControlPointT.setUpdatePersonId(controlPointT.getUpdatePersonId());
		serverControlPointT.setUpdateTime(controlPointT.getUpdateTime());
		serverControlPointT.setControlTarget(controlPointT.getControlTarget());
		serverControlPointT.setRiskNum(controlPointT.getRiskNum());
		serverControlPointT.setIsDelete(controlPointT.getIsDelete());
		return serverControlPointT;
	}

	/**
	 * 判断 是否修改
	 * 
	 * @author fuzhh 2013-7-5
	 * @param str
	 * @param oldStr
	 * @return
	 */
	public static boolean getIsUpdate(String str, String oldStr) {
		if (DrawCommon.isNullOrEmtryTrim(oldStr) && DrawCommon.isNullOrEmtryTrim(str)) {
			return false;
		} else if ((str != null && !str.equals(oldStr)) || (oldStr != null && !oldStr.equals(str))) {
			return true;
		}
		return false;
	}

	public static boolean getIsUpdate(Object str, Object oldStr) {
		if ((str != null && !str.equals(oldStr)) || (oldStr != null && !oldStr.equals(str))) {
			return true;
		}
		return false;
	}

	/**
	 * 获取面板
	 * 
	 * @return 画图面板
	 */
	public static JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	public static List<JecnToFromActiveT> setActiveIdForToFromFigure(Long flowId,
			List<JecnFigureData> activeFigureDataList, List<JecnFigureData> toFromFigureDataList) {

		Map<String, JecnFigureData> activeMap = new HashMap<String, JecnFigureData>();
		for (JecnFigureData figureData : activeFigureDataList) {
			activeMap.put(figureData.getUUID(), figureData);
		}

		JecnFigureData activeData = null;
		JecnToFromActiveT figure = null;
		List<JecnToFromActiveT> resultList = new ArrayList<JecnToFromActiveT>();
		for (JecnFigureData figureData : toFromFigureDataList) {
			JecnToFromRelatedData relatedData = (JecnToFromRelatedData) figureData;
			if (relatedData.getUUID() == null) {
				continue;
			}
			activeData = activeMap.get(relatedData.getUUID());
			if (activeData != null) {
				figure = new JecnToFromActiveT();
				figure.setToFromFigureUUId(relatedData.getUUID());
				figure.setFlowId(flowId);
				figure.setActiveFigureUUId(relatedData.getRelatedUUId());

				// getFlowElementId 为-1，活动为新增
				if (activeData.getFlowElementId() != -1) {
					figure.setActiveFigureId(activeData.getFlowElementId());
				}
				if (relatedData.getFlowElementId() != -1) {
					figure.setToFromFigureId(relatedData.getFlowElementId());
				}
				resultList.add(figure);
			}
		}
		return resultList;
	}

}
