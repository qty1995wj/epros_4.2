function standardSysListJson(standardId, isShow) {
	Ext.Ajax.request( {
		url : 'getStandardInfoList.action',
		method : 'POST',
		timeout : 1000000,
		params : {
			standardId : standardId,
			showAll : isShow
		},
		success : function(response, options) {
			// 获取response中的数据
		var json = Ext.util.JSON.decode(response.responseText);
		standardListGrid(json);

		document.getElementById("standardId").value = standardId;
	},
	// 失败调用
		failure : function(response, options) {
			alert('系统错误请联系管理员！')
		}
	});
}

function standardListGrid(json) {
	standardListGridStore = new Ext.data.JsonStore( {
		autoLoad : false,
		data : json.dataList,
		fields : json.stores
	});

	standardListGridColumnModel = new Ext.grid.ColumnModel( {
		defaults : {
			align : 'center',
			sortable : true,
			menuDisabled : false
		},
		columns : json.columns
	});

	standardListGridPanel = new Ext.grid.GridPanel( {
		id : "standardListGridPanel_id",
		renderTo : 'standardListShow_id',
		height : document.documentElement.clientHeight - 150,
		autoScroll : true,//滚动条
		stripeRows : true,//显示行的分隔符
		columnLines : true,//显示列的分隔线
		enableColumnMove : false,
		viewConfig : {
			//		 让列的宽度和grid一样	
		forceFit : true
	},
	frame : false,
	bodyStyle:'border-left: 0px;',
	store : standardListGridStore,
	colModel : standardListGridColumnModel
	})
}

/**
 * 
 * 显示全部
 */
function showAllStandardList() {
	var standardId = document.getElementById("standardId").value;
	standardListGridPanel.destroy();
	standardSysListJson(standardId, true, false);
}

/**
 * 
 *  下载
 */
function downloadStandardList() {
	var standardId = document.getElementById("standardId").value;
	window.open("downLoadStandFile.action?standardId=" + standardId);
}