package epros.draw.visioxml;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import visioxml.VisioDocument;
import visioxml.bean.ConnectProperty;
import visioxml.bean.PageProperty;
import visioxml.shape.BaseShape;
import epros.draw.data.JecnFlowMapData;
import epros.draw.util.DrawCommon;

/**
 * visio导出工具类 所有图形 XY坐标 转换关系 X=X+偏移量 Y=maxY+偏移量-Y
 * 
 * @author admin
 * 
 */
public class VisioOutPut {
	private static final Log log = LogFactory.getLog(VisioOutPut.class);
	/** 画图面板数据 */
	private JecnFlowMapData jecnFlowMapData;

	public VisioOutPut(JecnFlowMapData flowMapData) {
		this.jecnFlowMapData = flowMapData;
	}

	/**
	 * 导出visio
	 * 
	 * @param fullAllPath
	 *            保存文件全路径
	 */
	public void export(String fullAllPath) {
		if (DrawCommon.isNullOrEmtryTrim(fullAllPath)) {
			log.info("export fullAllPath is not valid,fullAllPath" + fullAllPath);
			return;
		}
		// 转换Epros数据为Visio数据
		VisioElementBuilder builder = new VisioElementBuilder(jecnFlowMapData);
		List<BaseShape> visioShapeList = builder.getVisioShapeList();
		List<ConnectProperty> visioConnectList = builder.getVisioConnectList();
		PageProperty pageProperty = builder.getPageProperty();
		// 导出visio
		writeXml(fullAllPath, VisioDocument.build(visioShapeList, visioConnectList, pageProperty));
	}

	/**
	 * XMl格式化输出
	 */
	public static final void writeXml(String allPath, Document doc) {
		XMLWriter writer = null;
		try {
			OutputFormat format = OutputFormat.createPrettyPrint();
			format.setEncoding("utf-8");
			format.setTrimText(false);
			writer = new XMLWriter(new FileOutputStream(allPath), format);
			writer.write(doc);
			writer.flush();
		} catch (IOException e) {
			log.error("生成文件异常,allPath" + allPath, e);
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				log.error("生成文件关闭流异常,allPath=" + allPath, e);
			}
		}

	}

}
