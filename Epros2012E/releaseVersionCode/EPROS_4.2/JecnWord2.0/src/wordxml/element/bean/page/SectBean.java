package wordxml.element.bean.page;

/**
 * 此类的属性值在页面设置中查找 宽高默认为A4大小
 */
public class SectBean {
	/** 页面宽度单位厘米 **/
	private float width = 21;
	/** 页面高度单位厘米 **/
	private float height = 29.7F;

	/** 页面的顶部到第一行之间的距离 单位厘米 **/
	private float top = 2.54F;
	/** 底部边缘到底部最后一行的距离 单位厘米 **/
	private float bottom = 2.54F;
	/** 左边缘和左端之间(不含缩进)的距离单位厘米 **/
	private float left = 1.8F;
	/** 右边缘到右端之间(不含缩进)的距离 单位厘米 **/
	private float right = 2;
	/** 设置页眉到顶部边缘的距离 单位厘米 **/
	private float header = 0.78F;
	/** 设置页脚到底部边缘的距离 单位厘米 **/
	private float footer = 0.5F;
	/** 额外空间到文件装订边缘 单位厘米 **/
	private float gutter = 0;
	/** 指定是否显示首页页眉和首页页脚 默认不显示 **/
	private boolean titlePg = false;
	/** 文档网格设置 默认行网格、跨度15.6 */
	private DocGrid docGrid = DocGrid.getInstance();
	/***** 设置当前节页码从多少开始 *****/
	private int startNum = 0;
	/********当前节时候是横项显示**************/
	private boolean horizontal;
	
	public SectBean() {
	}

	/**
	 * 设置页边距
	 * 
	 * @param width
	 * @param height
	 * @param top
	 * @param right
	 * @param bottom
	 * @param left
	 * @param header
	 * @param footer
	 * @param gutter
	 */
	public SectBean(float top, float left, float bottom, float right,
			float gutter, float header, float footer) {
		this.top = top;
		this.left = left;
		this.bottom = bottom;
		this.right = right;
		this.gutter = gutter;
		this.header = header;
		this.footer = footer;
	}

	/****
	 * 8* 设置页面边距 上 左 下 右 头 脚
	 * 
	 * @param top
	 * @param left
	 * @param bottom
	 * @param right
	 * @param header
	 * @param footer
	 */
	public void setPage(float top, float left, float bottom, float right,
			float header, float footer) {
		setTop(top);
		setLeft(left);
		setBottom(bottom);
		setRight(right);
		setHeader(header);
		setFooter(footer);
	}

	/**
	 * 设置页面大小
	 * 
	 * @author weidp
	 * @date 2014-10-23 下午04:42:39
	 * @param width
	 * @param height
	 */
	public void setSize(float width, float height) {
		setWidth(width);
		setHeight(height);

	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getTop() {
		return top;
	}

	public void setTop(float top) {
		this.top = top;
	}

	public float getRight() {
		return right;
	}

	public void setRight(float right) {
		this.right = right;
	}

	public float getBottom() {
		return bottom;
	}

	public void setBottom(float bottom) {
		this.bottom = bottom;
	}

	public float getLeft() {
		return left;
	}

	public void setLeft(float left) {
		this.left = left;
	}

	public float getHeader() {
		return header;
	}

	public void setHeader(float header) {
		this.header = header;
	}

	public float getFooter() {
		return footer;
	}

	public void setFooter(float footer) {
		this.footer = footer;
	}

	public float getGutter() {
		return gutter;
	}

	public void setGutter(float gutter) {
		this.gutter = gutter;
	}

	public boolean isTitlePg() {
		return titlePg;
	}

	public void setTitlePg(boolean titlePg) {
		this.titlePg = titlePg;
	}

	public DocGrid getDocGrid() {
		return docGrid;
	}

	public void setDocGrid(DocGrid docGrid) {
		this.docGrid = docGrid;
	}

	public int getStartNum() {
		return startNum;
	}

	public void setStartNum(int startNum) {
		this.startNum = startNum;
	}

	public boolean isHorizontal() {
		return horizontal;
	}

	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
	}
}
