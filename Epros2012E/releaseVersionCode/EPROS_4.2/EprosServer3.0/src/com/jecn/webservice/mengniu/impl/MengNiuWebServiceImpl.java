package com.jecn.webservice.mengniu.impl;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.webservice.crs.task.RuleInfoData;
import com.jecn.webservice.crs.task.impl.CrsTaskWebServiceImpl;
import com.jecn.webservice.mengniu.IMengNiuWebService;

@WebService
public class MengNiuWebServiceImpl implements IMengNiuWebService {
	private Logger log = Logger.getLogger(MengNiuWebServiceImpl.class);

	@Override
	@WebMethod
	public String findRuleListsByUserCode(String userCode, int PNUM) {
		if (StringUtils.isBlank(userCode)) {
			return "登录名称不能为空！";
		}
		IRuleService ruleService = (IRuleService) ApplicationContextUtil.getContext().getBean("RuleServiceImpl");
		try {
			if (PNUM == 0) {
				PNUM = -1;
			}
			List<Object[]> ruleList = ruleService.getRuleByWebService(userCode, 0, PNUM);
			List<RuleInfoData> resultList = CrsTaskWebServiceImpl.getRuleListByObjectsList(ruleList);
			JSONArray jsonArray = JSONArray.fromObject(resultList);
			return jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" userCode = " + userCode + " PNUM = " + PNUM);
			return "根据登录名 " + userCode + " 获取制度数据异常！";
		}
	}
}
