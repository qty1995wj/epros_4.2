package com.jecn.epros.server.service.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.process.JecnSustainToolConnTBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;

/***
 * 支持工具处理层接口
 * 
 * @author 2012-05-14
 * 
 */
public interface IFlowToolService extends IBaseService<JecnFlowSustainTool, Long> {

	/**
	 * 树加载，获得所有支持工具
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getSustainTools() throws Exception;

	/**
	 * 树加载，通过父类Id，获得SustainTools子节点(支持工具节点和支持工具目录)
	 * 
	 * @param pId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildSustainTools(Long pId) throws Exception;

	/**
	 * 根据名称，搜索支持工具
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getSustainToolsByName(String name) throws Exception;

	/**
	 * 排序节点的父节点ID
	 * 
	 * @param list
	 * @param pId
	 * @throws Exception
	 */
	public void updateSortSustainTools(List<JecnTreeDragBean> list, Long pId, Long peopleId) throws Exception;

	/***
	 * 支持工具 节点移动
	 * 
	 * @param listIds
	 *            要移动的IDS
	 * @param pId
	 *            移动到的父ID
	 * @param peopleId
	 * @throws Exception
	 */
	public void moveSustainTools(List<Long> listIds, Long pId, Long peopleId) throws Exception;

	/***
	 * 删除节点
	 * 
	 * @param listIds
	 * @param peopelId
	 * @throws Exception
	 */
	public void deleteSustainTools(List<Long> listIds, Long peopelId) throws Exception;

	/**
	 * @description:table加载，移动节点，搜索除了移动节点所在目录下其他关键字name的支持工具目录
	 * @param name
	 * @param listIds
	 * @param peopleId
	 * @return
	 */
	public List<JecnTreeBean> getSustainToolDirsByNameMove(String name, List<Long> listIds) throws Exception;

	/**
	 * @description:支持工具更新时，判断是否与同一父ID下的支持工具是否重名
	 * @param newName
	 * @param id
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid) throws Exception;

	/***
	 * 根据流程Id查询支持工具ID集合
	 * 
	 * @param flowIds
	 * @return
	 * @throws Exception
	 */
	public List<Long> getFlowToolIds(Long flowId) throws Exception;

	/**
	 * 根据IDs，搜索支持工具
	 * 
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getSustainToolsByIDs(List<Long> listIds) throws Exception;

	/***
	 * 添加支持工具与流程关联关系表
	 * 
	 * @param listIds
	 * @param flowId
	 * @throws Exception
	 */
	public void addFlowToolRelated(List<Long> listIds, Long flowId) throws Exception;

	/***
	 * 根据关联ID、关联类型获取与支持工具关联临时表数据
	 * 
	 * @param relatedId关联ID
	 * @param relatedType
	 *            关联类型
	 * @return
	 * @throws Exception
	 */
	public List<Long> getSustainToolConnTList(Long relatedId, int relatedType) throws Exception;

	/***
	 * 添加"与支持工具关联临时表"的数据
	 * 
	 * @param listIds
	 * @param relatedId
	 * @param relatedType
	 * @throws Exception
	 */
	public void addSustainToolConnTRelated(List<Long> listIds, Long relatedId, int relatedType) throws Exception;

	public List<JecnTreeBean> getSustainToolsByFlowId(Long flowId) throws Exception;

	public Long saveTool(JecnFlowSustainTool jecnFlowSustainTool, Long peopleId) throws Exception;

	public void updateTool(JecnFlowSustainTool jecnFlowSustainTool, Long peopleId) throws Exception;
}
