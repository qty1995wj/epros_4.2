package com.jecn.epros.domain.process;

import com.jecn.epros.domain.LinkResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2017/4/28.
 */
public class ProcessBaseAttribute {
    private List<ProcessAttribute> list = new ArrayList<>();

    /**
     * 图片下载链接
     */
    private LinkResource imageLink;
    private LinkResource docLink;


    public List<ProcessAttribute> getList() {
        return list;
    }

    public void setList(List<ProcessAttribute> list) {
        this.list = list;
    }

    public LinkResource getImageLink() {
        return imageLink;
    }

    public void setImageLink(LinkResource imageLink) {
        this.imageLink = imageLink;
    }

    public LinkResource getDocLink() {
        return docLink;
    }

    public void setDocLink(LinkResource docLink) {
        this.docLink = docLink;
    }
}
