package epros.draw.gui.box;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.gui.box.JecnToolBoxMainPanel.ButtonShowType;
import epros.draw.gui.figure.JecnTemplateData;
import epros.draw.gui.figure.unit.JecnFlowTemplate;
import epros.draw.gui.swing.JecnToolBar;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.JecnSystemData;
import epros.draw.util.JecnUIUtil;

/**
 * 生成模板显示的ScrollPane
 * 
 * @author fuzhh
 * 
 */
public class JecnTemplateScrollPane extends JScrollPane implements
		MouseListener {

	/** 模板的panel */
	private JPanel templatePanel;
	/** 流程元素库主面板 */
	private JecnToolBoxMainPanel toolBoxMainPanel = null;
	/** 面板类型 */
	private MapType mapType;
	/** 按钮显示类型 */
	protected ButtonShowType buttonShowType = ButtonShowType.topBotton;

	public JecnTemplateScrollPane(MapType mapType,
			JecnToolBoxMainPanel toolBoxMainPanel) {
		if (toolBoxMainPanel == null) {
			return;
		}
		// 初始化panel
		templatePanel = new JPanel();
		this.toolBoxMainPanel = toolBoxMainPanel;
		this.mapType = mapType;
		this
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 设置背景颜色
		templatePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 边框
		templatePanel.setBorder(null);
		this.setBorder(null);

		// 模型数据集合
		List<JecnTemplateData> templatePartMapDataList = JecnFlowTemplate
				.getMapTemplate(mapType);

		if (templatePartMapDataList == null) {
			return;
		}
		getTemplatePanel(templatePartMapDataList);
		// 把panel 添加的 滚动面板上
		this.setViewportView(templatePanel);

		// 鼠标滚动大小
		this.getHorizontalScrollBar().setUnitIncrement(30);
		this.getVerticalScrollBar().setUnitIncrement(30);
	}

	/**
	 * 生成显示的button
	 * 
	 * @param templateDataList
	 * @param templateTitleDataList
	 * @param mapType
	 * @return
	 */
	private JPanel getTemplatePanel(List<JecnTemplateData> templateDataList) {
		// 设置panel的布局方式
		templatePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		for (JecnTemplateData templateData : templateDataList) {
			// 添加button
			getTemplateButton(templatePanel, templateData);
		}
		// 事件：和内容保持一致
		this.addComponentListener(new JecnScrollPaneComponentAdapter());
		templatePanel.addMouseListener(this);
		templatePanel.setBorder(null);
		return templatePanel;
	}

	/**
	 * 生成显示的button
	 * 
	 * @param templateType
	 *            模板和标题的标识 partMap为流程图模板，totalMap为流程地图模板，title标题
	 * @param templatePanel
	 *            需要添加的panel
	 * @param templateData
	 *            片段数据
	 */
	public void getTemplateButton(JPanel templatePanel,
			JecnTemplateData templateData) {
		// 生成button
		JecnTemplateBoxButton templateBoxButton = new JecnTemplateBoxButton(
				toolBoxMainPanel, templateData);
		templatePanel.add(templateBoxButton.getJToolBar());
		// 把模板数据添加到数据
		JecnSystemData.getTemplateDataList().add(templateData);
		// 把对应生成的按钮添加到数据中
		JecnSystemData.getTemplateMap().put(templateData,
				templateBoxButton.getJToolBar());
		// 添加到互斥组
		toolBoxMainPanel.getBtnGroup().add(templateBoxButton);
		// 如果是流程图
		if (mapType == MapType.partMap) {
			JecnSystemData
					.setPartMapCount(JecnSystemData.getPartMapCount() + 1);
		} else if (mapType == MapType.totalMap) { // 如果为流程地图
			JecnSystemData
					.setTotalMapCount(JecnSystemData.getTotalMapCount() + 1);
		}
	}

	/**
	 * 设置按钮显示
	 * 
	 * @author fuzhh Aug 29, 2012
	 */
	public void setButtonShowType() {
		Component[] toolBarStr = templatePanel.getComponents();
		for (Component component : toolBarStr) {
			if (component instanceof JecnToolBar) {
				JecnToolBar toolBar = (JecnToolBar) component;
				if (toolBar.getComponents().length > 0) {
					JecnTemplateBoxButton templateBoxButton = (JecnTemplateBoxButton) toolBar
							.getComponents()[0];
					templateBoxButton.setButtonShowType();
				}
			}
		}
	}

	/**
	 * 
	 * 获取ButtonShowType类型对应的按钮大小
	 * 
	 * @return Dimension 按钮大小
	 */
	Dimension getButtonShowTypeSize() {
		JecnToolBoxModelContainPanel modelContainPanel = toolBoxMainPanel
				.getModelContainPanel();
		Dimension dimension = null;
		switch (modelContainPanel.getButtonShowType()) {
		case leftRight: // 图片左内容右
			dimension = new Dimension(160, 65);
			break;
		case topBotton: // 图片上内容下
			dimension = new Dimension(105, 85);
			break;
		default:
			dimension = new Dimension(105, 85);
			break;
		}
		return dimension;
	}

	public void setButtonShowType(ButtonShowType buttonShowType) {
		this.buttonShowType = buttonShowType;
	}

	public ButtonShowType getButtonShowType() {
		return buttonShowType;
	}

	public MapType getMapType() {
		return mapType;
	}

	public JPanel getTemplatePanel() {
		return templatePanel;
	}

	public void setTemplatePanel(JPanel templatePanel) {
		this.templatePanel = templatePanel;
	}

	public void mouseReleased(MouseEvent e) {
		if (e.getSource() == templatePanel) {
			JecnDrawMainPanel.getMainPanel().getBoxPanel().initGuide();
		}
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

}
