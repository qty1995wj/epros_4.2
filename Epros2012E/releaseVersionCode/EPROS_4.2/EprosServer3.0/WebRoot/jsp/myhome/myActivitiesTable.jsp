<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div>
			<div id="myWorkActive_id_title"
				style="width: 100%; overflow-y: scroll;overflow-x:hidden;" align="center">
				<table width="97%" border="0" cellpadding="0" cellspacing="1"
					align="center" style="background-color: #BFBFBF" style="table-layout: fixed;">
					<tr class="FixedTitleRow" align="center">
						<td width="2%" class="gradient">
						</td>
						<td width="20%" height="32" class="gradient"
							style="word-break: break-all;">
							<!-- 活动名称 -->
							<s:text name="activityTitle" />
						</td>
						<td width="8%" height="32" class="gradient"
							style="word-break: break-all;">
							<!-- 活动编号 -->
							<s:text name="activitynumbers" />
						</td>
						<td width="20%" height="32" class="gradient"
							style="word-break: break-all;">
							<!-- 流程名称 -->
							<s:text name="processName" />
						</td>
						<td width="8%" height="32" class="gradient"
							style="word-break: break-all;">
							<!-- 流程编号 -->
							<s:text name="processNumber" />
						</td>
						<td width="20%" height="32" class="gradient"
							style="word-break: break-all;">
							<!-- 责任人 -->
							<s:text name="responsiblePersons" />
						</td>
						<td width="20%" height="32" class="gradient"
							style="word-break: break-all;">
							<!-- 责任部门 -->
							<s:text name="responsibilityDepartment" />
						</td>
						<td width="2%" class="gradient">
						</td>
					</tr>
				</table>
			</div>
			<div id="myWorkActive_id_List_div" style="width: 100%; overflow-y: scroll;overflow-x:hidden;"
				align="center">
				<s:set id="count" value="1" />
				<table width="97%" border="0" cellpadding="0" cellspacing="1"
					align="center" style="background-color: #BFBFBF" style="table-layout: fixed;">
					<s:iterator value="listProcessActiveWebBean" status="st">
						<tr align="center" style="background-color: #ffffff"
							onmouseover="style.backgroundColor='#EFEFEF';"
							onmouseout="style.backgroundColor='#ffffff';"
							height="20">
							<td align="right" class="gradient" style="word-break: break-all;" width="2%">
								<s:property value="#count" />
								<s:set id="count" value="#count+1"/>
							</td>
							<td class="t5" style="word-break: break-all;" width="20%">
								<a
									href="processActive.action?activeId=<s:property value='activeFigureId'/>"
									style="cursor: pointer;" target='_blank' class="table"> <s:property
										value="activeName" /> </a>
							</td>
							<td class="t5" style="word-break: break-all;" width="8%">
								<s:property value="activeId" />
							</td>
							<td class="t5" style="word-break: break-all;" width="20%">
								<a
									href="process.action?type=process&flowId=<s:property value='processWebBean.flowId'/>"
									target='_blank' style="cursor: pointer;" class="table"> <s:property
										value="processWebBean.flowName" /> </a>
							</td>
							<td class="t5" style="word-break: break-all;" width="8%">
								<s:property value="processWebBean.flowIdInput" />
							</td>
							<td class="t5" style="word-break: break-all;" width="20%">
								<s:property value="processWebBean.resPeopleName" />
							</td>
							<td class="t5" style="word-break: break-all;" width="20%">
								<a
									href="organization.action?orgId=<s:property value='processWebBean.orgId'/>"
									target='_blank' style="cursor: pointer;" class="table"> <s:property
										value="processWebBean.orgName" /> </a>
							</td>
							<td class="t5" style="word-break: break-all;" width="2%"></td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</div>
	</body>
</html>