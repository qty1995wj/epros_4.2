package epros.draw.gui.operationConfig;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnResourceUtil;

/*******************************************************************************
 * 活动明细
 * 
 * @author 2012-08-28
 * 
 */
public class DrawActivityDescriptionDialog extends JecnDialog {
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();

	/** 活动编号Lab */
	private JLabel actNumLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activitiesNumbers") + ":");

	/** 活动编号Field */
	private JTextField actNumField = new JecnTextField();

	/** 执行角色Lab */
	private JLabel executionRoleLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("executiveRole") + ":");

	/** 执行角色Field */
	private JTextField executionRoleField = new JecnTextField();

	/** 活动名称Lab */
	private JLabel actNameLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activityTitle") + ":");

	/** 活动名称Field */
	private JTextField actNameField = new JecnTextField();

	/** 活动说明Lab */
	private JLabel activityDesLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("activityIndicatingThat") + ":");

	/** 活动说明Area */
	private JTextArea activityDesArea = new JecnTextArea();

	private JScrollPane activityDesScrollPane = new JScrollPane(activityDesArea);

	/** 输入Lab */
	private JLabel actInLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("input") + ":");

	/** 输入Field */
	private JTextField actInField = new JecnTextField();

	/** 输出Lab */
	private JLabel actOutLab = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("output") + ":");

	/** 输出Field */
	private JTextField actOutField = new JecnTextField();

	/** 确定按钮 */
	private JButton confirmBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("confirm"));
	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("cancel"));

	/** 传入用来显示的数据 */
	private JecnActivityShowBean activityShowBean;

	/** 点击的table */
	private DrawActiveDescTable activeDescTable;
	/** 点击table的行数 */
	private int selectRow;

	public DrawActivityDescriptionDialog(JecnActivityShowBean activityShowBean, DrawActiveDescTable activeDescTable,
			int selectRow) {
		this.activityShowBean = activityShowBean;
		this.activeDescTable = activeDescTable;
		this.selectRow = selectRow;
		this.setSize(500, 350);
		this.setModal(true);
		this.setResizable(true);
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("activityIndicatingThat"));

		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());

		// 活动说明
		activityDesScrollPane.setBorder(null);
		activityDesScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		activityDesScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 设置活动说明滚动面板边框
		// activityDesScrollPane.setBorder(BorderFactory.createLineBorder(Color.lightGray));
		// 确定
		confirmBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				confirmButAction();
			}
		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButAction();
			}
		});
		initLayout();
		initializationData();
	}

	private void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);
		// infoPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 活动编号
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(actNumLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(actNumField, c);
		// 执行角色
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(executionRoleLab, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(executionRoleField, c);
		// 活动名称
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(actNameLab, c);
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(actNameField, c);
		// 活动说明
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(activityDesLab, c);
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(activityDesScrollPane, c);
		// 输入
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(actInLab, c);
		c = new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(actInField, c);
		// 输出
		c = new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(actOutLab, c);
		c = new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(actOutField, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(confirmBut);
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 初始化数据
	 * 
	 * @author fuzhh Aug 28, 2012
	 */
	private void initializationData() {
		// 活动编号Field
		actNumField.setText(activityShowBean.getActiveFigure().getFlowElementData().getActivityNum());
		// 执行角色Field
		executionRoleField.setText(activityShowBean.getRoleName());
		// 活动名称Field
		actNameField.setText(activityShowBean.getActiveFigure().getFlowElementData().getFigureText());
		// 活动说明Area
		activityDesArea.setText(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow());
		// 输入Field
		actInField.setText(activityShowBean.getInput());
		// 输出Field
		actOutField.setText(activityShowBean.getOutput());
	}

	/**
	 * 确定事件
	 * 
	 * @author fuzhh Oct 11, 2012
	 */
	private void confirmButAction() {
		// 活动编号
		activityShowBean.getActiveFigure().getFlowElementData().setActivityNum(actNumField.getText());
		// 角色名称
		activityShowBean.setRoleName(executionRoleField.getText());
		// 活动名称
		activityShowBean.getActiveFigure().getFlowElementData().setFigureText(actNameField.getText());
		// 活动说明
		activityShowBean.getActiveFigure().getFlowElementData().setAvtivityShow(activityDesArea.getText());
		// 输入Field
		activityShowBean.setInput(actInField.getText());
		// 输出Field
		activityShowBean.setOutput(actOutField.getText());

		activeDescTable.setValueAt(actNumField.getText(), selectRow, 1);

		activeDescTable.setValueAt(executionRoleField.getText(), selectRow, 2);

		activeDescTable.setValueAt(actNameField.getText(), selectRow, 3);

		activeDescTable.setValueAt(activityDesArea.getText(), selectRow, 4);

		activeDescTable.setValueAt(actInField.getText(), selectRow, 5);

		activeDescTable.setValueAt(actOutField.getText(), selectRow, 6);

		this.dispose();
	}

	private void cancelButAction() {
		this.dispose();
	}
}
