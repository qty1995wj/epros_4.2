//package com.jecn.epros.server.action.web.timer;
//
//import com.jecn.epros.server.action.web.timer.UserAutoBuss.SynType;
//
///**
// * 初始化定时器数据Bean
// * 
// * @author ZHANGXIAOHU
// * @date： 日期：Jan 14, 2013 时间：1:53:49 PM
// */
//public class AutoBean {
//	/** 开始时间hh:mm：执行定时器的时间 */
//	private String startTime;
//	/** 间隔天数：间隔多少天，在多少天之后的startTime执行定时器 */
//	private String invalDay;
//	/** 同步方式 */
//	private SynType synType;
//
//	public String getStartTime() {
//		return startTime;
//	}
//
//	public void setStartTime(String startTime) {
//		this.startTime = startTime;
//	}
//
//	public String getInvalDay() {
//		return invalDay;
//	}
//
//	public void setInvalDay(String invalDay) {
//		this.invalDay = invalDay;
//	}
//
//	public SynType getSynType() {
//		return synType;
//	}
//
//	public void setSynType(SynType synType) {
//		this.synType = synType;
//	}
//}
