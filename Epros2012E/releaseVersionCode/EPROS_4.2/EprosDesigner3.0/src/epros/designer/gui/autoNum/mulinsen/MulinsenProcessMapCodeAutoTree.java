package epros.designer.gui.autoNum.mulinsen;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.autoNum.AutoNumJecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

/**
 * 自动编号-模块代码tree结构
 * 
 * @author Administrator
 * 
 */
public class MulinsenProcessMapCodeAutoTree extends AutoNumJecnTree {

	public MulinsenProcessMapCodeAutoTree(int treeType) {
		super(treeType);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			// 根节点"流程模板管理"
			JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.processCodeRoot, "L3");
			List<JecnTreeBean> list = ConnectionPool.getAutoCodeAction().getChildProcessCodes(0L, 5);
			JecnTreeCommon.addNLevelNodes(list, rootNode);
			return new JecnTreeModel(rootNode);
		} catch (Exception e) {
			log.error("find process map code error!", e);
			return null;
		}
	}
}
