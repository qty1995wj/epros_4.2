package com.jecn.epros.server.bean.task;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

/**
 * 文控信息表从表 记录审核人名称和审核阶段名称
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Oct 30, 2012 时间：3:13:15 PM
 */
public class JecnTaskHistoryFollow implements Serializable {
	/** 主键ID */
	private Long id;
	/** 主键ID */
	private Long historyId;
	/** 审核阶段名称 */
	private String appellation;
	/** 审核阶段英文名称 */
	private String en_Appellation;
	
	/** 审核人名称 */
	private String name;
	/** 序号 */
	private int sort;
	/** 审批时间 */
	private Date approvalTime;
	/**
	 * 任务审批阶段标识 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 6：各业务体系审批阶段 7：IT总监审批阶段
	 */
	private Integer stageMark;
	/***
	 * 审批人的id，通过逗号分隔
	 */
	private String approveId;
	/** 所属人员对应部门 */
	private String approveOrgName;
	
	

	public String getEn_Appellation() {
		return en_Appellation;
	}

	public void setEn_Appellation(String enAppellation) {
		en_Appellation = enAppellation;
	}

	public String getApproveId() {
		return approveId;
	}

	public void setApproveId(String approveId) {
		this.approveId = approveId;
	}

	public Integer getStageMark() {
		return stageMark;
	}

	public void setStageMark(Integer stageMark) {
		this.stageMark = stageMark;
	}

	public Date getApprovalTime() {
		return approvalTime;
	}

	public void setApprovalTime(Date approvalTime) {
		this.approvalTime = approvalTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Long historyId) {
		this.historyId = historyId;
	}

	public String getAppellation() {
		return appellation;
	}

	public void setAppellation(String appellation) {
		this.appellation = appellation;
	}
	
	public String getAppellation(Locale type) {
		return type == Locale.ENGLISH ? en_Appellation : appellation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getApproveOrgName() {
		if (approveOrgName == null) {
			approveOrgName = "";
		}
		return approveOrgName;
	}

	public void setApproveOrgName(String approveOrgName) {
		this.approveOrgName = approveOrgName;
	}
}
