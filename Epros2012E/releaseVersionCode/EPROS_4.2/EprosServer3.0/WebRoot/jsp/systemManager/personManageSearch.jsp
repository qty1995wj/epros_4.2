<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%">
			<div id="search" class="divBorder" align="center">
				<div id="personManageSearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td width="10%" height="25px" align="right">
								<!-- 登录名称： -->
								<s:text name="lognNameC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%"
									id="personManage_loginName_id" maxLength="32" />
							</td>


							<td width="10%" align="right">
								<!-- 真实姓名: -->
								<s:text name="realNameC" />
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%"
									id="personManage_trueName_id" maxLength="32" />
							</td>

							<td style="vertical-align: middle;" width="10%;">
								<a onClick="toggle(personManageSearchText)"
									style="cursor: pointer;" class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" />
								</span> </a>
							</td>
						</tr>

						<tr>
							<td height="25px" align="right">
								<!-- 角色: -->
								<s:text name="roleC" />
							</td>
							<td align="left">
								<select style="width: 80%" id="personManage_role_id">
									<option value="">
										<s:text name="all" />
									</option>
									<option value="admin">
										<s:text name="sysAdminManager" />
									</option>
									<option value="design">
										<s:text name="designManager" />
									</option>
									<option value="viewAdmin">
										<s:text name="viewManager" />
									</option>
									<option value="isDesignFileAdmin">
										<s:text name="fileManager" />
									</option>
									<option value="isDesignProcessAdmin">
										<s:text name="processManager" />
									</option>
									<s:if test="#application.versionType">
										<option value="isDesignRuleAdmin">
											<s:text name="ruleManager" />
										</option>
									</s:if>
								</select>
							</td>


							<td align="right">
								<!-- 所属部门： -->
								<s:text name="subordinateDepartmentsC"></s:text>
							</td>
							<td align="left">
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="personManage_belongOrgName_id"
									onkeyup="search(personManage_belongOrgName_id,personManage_orgId_id,personManage_searchOrgName_id,'org');"
									onblur="divNode(personManage_searchOrgName_id)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("organization","personManage_belongOrgName_id","personManage_orgId_id")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br>
								<div id="personManage_searchOrgName_id"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td></td>
						</tr>

						<tr>
							<td height="25px" align="right">
								<!-- 任职岗位: -->
								<s:text name="officeholdingPositionC" />
							</td>
							<td align="left">
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="personManage_PosName_id"
									onkeyup="search(personManage_PosName_id,personManage_posId_id,personManageSearch_posNameId,'pos');"
									onblur="divNode(personManageSearch_posNameId)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("position","personManage_PosName_id","personManage_posId_id")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="personManageSearch_posNameId"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>
							<td></td>
							<td></td>

							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img src="${basePath}images/iParticipateInProcess/search.gif"
						style="cursor: pointer;" onclick="personManageGridSearch()" />
					<img src="${basePath}images/iParticipateInProcess/reset.gif"
						style="cursor: pointer;" onclick="personManageReset()" />
				</div>
				<!-- 所属部门ID -->
				<input type="hidden" id="personManage_orgId_id" value="-1" />
				<!-- 岗位ID -->
				<input type="hidden" id="personManage_posId_id" value="-1" />
			</div>

			<div id="personManageGrid_id">

			</div>
		</div>
	</body>
</html>