package com.jecn.epros.server.webBean.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

/**
 * 流程概况信息
 * 
 * @author Administrator
 * 
 */
public class ProcessMapBaseInfoBean implements Serializable {
	/** 流程ID */
	private Long flowMapId;
	/** 流程地图名称 */
	private String flowMapName;
	/** 流程地图编号 */
	private String flowMapIdInput;
	/** 密级 */
	private int isPublic;
	/** 岗位查阅权限 */
	private List<JecnTreeBean> posList;
	/** 部门查阅权限 */
	private List<JecnTreeBean> orgList;

	public Long getFlowMapId() {
		return flowMapId;
	}

	public void setFlowMapId(Long flowMapId) {
		this.flowMapId = flowMapId;
	}

	public String getFlowMapName() {
		return flowMapName;
	}

	public void setFlowMapName(String flowMapName) {
		this.flowMapName = flowMapName;
	}

	public String getFlowMapIdInput() {
		return flowMapIdInput;
	}

	public void setFlowMapIdInput(String flowMapIdInput) {
		this.flowMapIdInput = flowMapIdInput;
	}

	public int getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(int isPublic) {
		this.isPublic = isPublic;
	}

	public List<JecnTreeBean> getPosList() {
		return posList;
	}

	public void setPosList(List<JecnTreeBean> posList) {
		this.posList = posList;
	}

	public List<JecnTreeBean> getOrgList() {
		return orgList;
	}

	public void setOrgList(List<JecnTreeBean> orgList) {
		this.orgList = orgList;
	}
}
