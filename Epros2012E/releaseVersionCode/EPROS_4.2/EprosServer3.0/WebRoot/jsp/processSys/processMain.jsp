﻿﻿<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<!-- 流程图详情 -->
		<title><s:text name="flowChartForDetails" /></title>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
		<script type="text/javascript"
			src="${basePath}ext/adapter/ext/ext-base.js">
</script>
		<script type="text/javascript" src="${basePath}ext/ext-all.js">
</script>
		<s:if test="#session.country=='US'">
		</s:if>
		<s:else>
			<script type="text/javascript"
				src="${basePath}js/epros_${language}_${country}.js" charset="UTF-8">
</script>
			<script type="text/javascript"
				src="${basePath}ext/ext-lang-${language}_${country}.js"
				charset="UTF-8">
</script>
		</s:else>

		<script type="text/javascript" src="${basePath}js/common.js">
</script>
		<script type="text/javascript"
			src="${basePath}js/epros_${language}_${country}.js">
</script>
		<script type="text/javascript" src="${basePath}jquery/jquery-1.6.2.js">
</script>
		<script type="text/javascript" src="${basePath}js/firstPage.js"
			charset="UTF-8">
</script>
		<script type="text/javascript" src="${basePath}js/rtnlPropose.js">
</script>
<script type="text/javascript" src="${basePath}js/browserDetect.js" charset="UTF-8"></script>
		<script type="text/javascript">
var versionType = ${versionType};
var isHiddenSystem = ${isHiddenSystem};
Ext.onReady(function() {
		var processMainPanel = new Ext.TabPanel({
			border : false,
			// resizeTabs:true,//True表示为自动调整各个标签页的宽度
			activeTab : 0,
			region : 'center',
			items : [ {
				title : i_procesChart,// 流程图
				id : "processMap_id",
				autoLoad : "${basePath}"
						+ "jsp/processSys/"+ processMapContainerJsp +"?isPub=true&mapType=process&mapID="
						+ "${param.flowId}"+"&mapName="+"${param.name}"+"&target="+"${param.target}"
			},{
				title : '流程属性',// 概况信息
				id : "processOvervicwInfo_id",
				autoLoad : "${basePath}"
						+ "processProfileInformation.action?flowId="
						+ "${param.flowId}"
			}, {
				title : '流程说明',// 操作说明
				id : "processOperateDes_id",
				autoLoad : {url:"${basePath}"
						+ "processFile.action?flowId="
						+ "${param.flowId}",
						   scripts : true,
				callback:function(){
					setShowProcessFileDivWH('processFileDiv');
				 }
				}
			}, {
				title : i_operateMode,// 操作模板
				id : "processoperateMode_id",
				autoLoad : {url:"${basePath}"
						+ "processTemplate.action?flowId="
						+ "${param.flowId}",
						scripts : true,
						callback:function(){
							setShowProcessTemplate('processTemplateDiv');
						 }
						}
			},{
				title : i_relateFile,// 相关文件
				id : "relaProStanRule_id",
				autoLoad : {
				    url:"${basePath}"
						+ "relatedProcessRuleStandard.action?flowId="
						+ "${param.flowId}",
						scripts : true,
					callback:function(){
						setShowTaskProcessTemplate('relatedProcessRuleStandardId');
					}
				}
			}
<%--				, {--%>
<%--					title : i_property,// 属性--%>
<%--					id : "processProperty_id",--%>
<%--					autoLoad : "${basePath}"--%>
<%--							+ "processAttribute.action?flowId="--%>
<%--							+ "${param.flowId}"--%>
<%--				}--%>
			,{
				title : i_historyInfo,// 文控信息
				id : "historyInfo_id",
				autoLoad : {url:"${basePath}"
						+ "processHistoryNew.action?flowId="
						+ "${param.flowId}",
						scripts : true,
						callback:function(){
							setShowDocument('processDocumentDiv');
						}
				}
			},{
				title : "KPI",
				id : "processKPI_id",
				autoLoad :{
					url:"${basePath}"+ "flowKpiNames.action?flowId="+"${param.flowId}",
					scripts:true,
					callback:function(){
				       setKpiShowWH('flow_kpi');
					  }
					}
			}, {
				title : i_rationalization,// 合理化建议
				id : "rtnlPropose_id",
				autoLoad : {
					     url : "${basePath}"
							+ "allrtnlPropose.action?flowId="
							+ "${param.flowId}",
							scripts:true,
					     callback:function(){
							onloadPropose("${param.flowId}",true);
						}
				}
				}],
			listeners : {

				'bodyresize' : function(p, width, height) {
					if (p.getActiveTab() == null) {
						return;
					}
					if (p.getActiveTab().getId() == 'processOperateDes_id') {
						setShowProcessFileDivWH('processFileDiv');
					} else if (p.getActiveTab().getId() == 'processoperateMode_id') {
						setShowProcessTemplate('processTemplateDiv');
					} else if (p.getActiveTab().getId()== 'relaProStanRule_id') {
						setShowTaskProcessTemplate('relatedProcessRuleStandardId');
					}else if (p.getActiveTab().getId()== 'historyInfo_id') {
						setShowDocument('processDocumentDiv');
					}else if(p.getActiveTab().getId()== 'processKPI_id'){
						setKpiShowWH('flow_kpi');
					}
				},
				'tabchange' : function(tabPanel, panel) {
					if(typeof(panel) == "undefined"){
						return;
					}
					if (panel.getId() == 'processOperateDes_id') {
						setShowProcessFileDivWH('processFileDiv');
					} else if (panel.getId() == 'processoperateMode_id') {
						setShowProcessTemplate('processTemplateDiv');
						
					} else if (panel.getId() == 'relaProStanRule_id') {
						setShowTaskProcessTemplate('relatedProcessRuleStandardId');
					} else if (panel.getId() == 'historyInfo_id') {
						setShowDocument('processDocumentDiv');
					}

				}

				}
		});
	/*if(!${webLoginBean.isAdmin} && !${webLoginBean.isViewAdmin}){*/
		/*processMainPanel.remove('historyInfo_id');*/
	/*}*/
	
	if(!${isDocControlPermission}&&!${webLoginBean.isAdmin}&&!${webLoginBean.isViewAdmin}){
		processMainPanel.remove('historyInfo_id');
	}
	if(!versionType){
		processMainPanel.remove('relaProStanRule_id');
		processMainPanel.remove('processKPI_id');
	}
	if(${isHiddenBDF}){// 巴德富，隐藏
		processMainPanel.remove('processoperateMode_id');
		processMainPanel.remove('relaProStanRule_id');
	}
	var northPanle=new Ext.Panel({
		height:20,
		region : 'north',
		contentEl:'homePage_id'
	});
	new Ext.Viewport({
		autoScroll : false,
		layout : 'border',
		items : [northPanle,processMainPanel]
	});
});
var basePath = "${basePath}";
</script>
	</head>

	<body>
			<div id="homePage_id"  style="height: 25px; width: 100%; overflow: hidden;"
				align="right" class="gradient minWidth">
				<%@include file="/navigation.jsp"%>
			</div>
	</body>
</html>