package epros.draw.gui.figure.shape;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.BaseFlowLevelFigure;

/**
 * 
 * 流程等级图形 三级
 * 
 * @author ZHANGXH
 *
 */
public class FlowLevelFigureThree extends BaseFlowLevelFigure {

	public FlowLevelFigureThree(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
		intLevel = 3;
	}

}
