package epros.draw.gui.swing.popup.popup;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.ToolTipManager;

import epros.draw.gui.swing.popup.table.JecnPopupTableScrollPane;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 能拖动的弹出菜单
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPopupMenu extends JPopupMenu {

	/** 弹出框最小宽度 */
	private final int minWidth = 250;
	/** 弹出框最小高度 */
	private final int minHeight = 125;
	/** 是否支持拖动 true：允许拖动；false：禁止拖动 */
	private boolean isResizable = true;

	/** 主面板 */
	protected JPanel mainPanel = null;
	/** 表框拖动面板 */
	private JecnPopupResizeContentPanel borderPanel = null;

	public JecnPopupMenu() {
		initComponent();
		initLayouty();
	}

	private void initComponent() {
		// 主面板
		mainPanel = new JPanel();
		// 表框拖动面板
		borderPanel = new JecnPopupResizeContentPanel(this);

		this.setLayout(new BorderLayout());
		this.setBorderPainted(false);
		this.setBorder(null);
		this.setPopupSize(minWidth, minHeight);

		// 布局管理器
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(null);

		// 背景颜色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	private void initLayouty() {
		super.add(borderPanel);
		borderPanel.addMainPanl(mainPanel);
	}

	@Override
	public Component add(Component comp) {
		mainPanel.removeAll();
		mainPanel.add(comp);
		return comp;
	}

	@Override
	public void setPopupSize(Dimension d) {
		int newWidth;
		int newHeight;
		if (d.width < minWidth) {
			newWidth = minWidth;
		} else {
			newWidth = d.width;
		}
		if (d.height < minHeight) {
			newHeight = minHeight;
		} else {
			newHeight = d.height;
		}
		d.setSize(newWidth, newHeight);
		super.setPopupSize(d);
	}

	@Override
	public void setVisible(boolean b) {
		if (!b) {//这分支代码主要解决：在弹出菜单中显示提示信息后，切换软件后在回来，提示信息没有隐藏。
			if (mainPanel != null) {
				for (Component comp : mainPanel.getComponents()) {
					if (comp instanceof JecnPopupTableScrollPane) {
						// 注销提示
						JecnPopupTableScrollPane scrollPane = (JecnPopupTableScrollPane) comp;
						ToolTipManager.sharedInstance().unregisterComponent(
								scrollPane.getPopupTable());

						// 返回初始延迟值
						int intD = ToolTipManager.sharedInstance()
								.getInitialDelay();
						// 取消工具提示的延迟值
						ToolTipManager.sharedInstance().setDismissDelay(intD);

						// 禁止显示
						if (ToolTipManager.sharedInstance().isEnabled()) {
							ToolTipManager.sharedInstance().setEnabled(false);
						}
					}
				}
			}
			mainPanel = null;
			borderPanel = null;
			// 启动显示
			if (!ToolTipManager.sharedInstance().isEnabled()) {
				ToolTipManager.sharedInstance().setEnabled(true);
			}
		}

		super.setVisible(b);
	}

	public boolean isResizable() {
		return isResizable;
	}

	public void setResizable(boolean isResizable) {
		this.isResizable = isResizable;
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	public int getMinWidth() {
		return minWidth;
	}

	public int getMinHeight() {
		return minHeight;
	}

}
