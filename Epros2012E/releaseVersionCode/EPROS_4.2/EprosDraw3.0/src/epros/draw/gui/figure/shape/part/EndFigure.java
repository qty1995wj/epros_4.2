/*
 * EndFigure.java
 *结束元素
 * Created on 2008年12月3日, 下午4:44
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;

/**
 * 流程图的结束符号
 * 
 * @author Administrator
 */
public class EndFigure extends Triangle {

	public EndFigure(JecnFigureData figureData) {
		super(figureData);
	}

}
