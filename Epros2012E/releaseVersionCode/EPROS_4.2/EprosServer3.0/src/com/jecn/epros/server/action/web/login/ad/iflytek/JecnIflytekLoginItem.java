package com.jecn.epros.server.action.web.login.ad.iflytek;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.util.JecnPath;

public class JecnIflytekLoginItem {
	private static Logger log = Logger.getLogger(JecnIflytekLoginItem.class);

	public static String SYNC_APP_ID = null;
	public static String PS_ORG_SERVICE = null;
	public static String PS_EMPL_SERVICE = null;

	public static String PS_POSITION_SERVICE = null;

	public static String SERVER_ROOT = null;

	private static Properties ssoProperies = null;

	private static ResourceBundle iflytekConfig;

	static {
		iflytekConfig = ResourceBundle.getBundle("cfgFile/iflytek/iflytekConfig");
	}

	public static String getValue(String key) {
		return iflytekConfig.getString(key);
	}

	public static void start() {
		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/iflytek/iflytekConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			SYNC_APP_ID = props.getProperty("SYNC_APP_ID");

			PS_ORG_SERVICE = props.getProperty("PS_ORG_SERVICE");
			PS_EMPL_SERVICE = props.getProperty("PS_EMPL_SERVICE");
			PS_POSITION_SERVICE = props.getProperty("PS_POSITION_SERVICE");
			SyncConstant.PASSWORD = props.getProperty("DEFAULT_PASSWORD");
		} catch (FileNotFoundException e) {
			log.error("iflytekConfig.properties is not found", e);
		} catch (IOException e) {
			log.error("iflytekConfig.properties red error", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("iflytekConfig.properties close error", e1);
				}
			}
		}
	}

	public static Properties SSOProperties() {

		log.info("开始读取配置文件内容");

		if (ssoProperies != null) {
			return ssoProperies;
		}
		ssoProperies = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/iflytek/ssoconfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			ssoProperies.load(in);

		} catch (FileNotFoundException e) {
			log.error("ssoconfig.properties is not found", e);
		} catch (IOException e) {
			log.error("ssoconfig.properties red error", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("ssoconfig.properties close error", e1);
				}
			}
		}
		return ssoProperies;
	}
}
