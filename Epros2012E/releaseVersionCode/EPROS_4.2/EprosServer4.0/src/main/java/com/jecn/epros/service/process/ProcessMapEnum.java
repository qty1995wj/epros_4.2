package com.jecn.epros.service.process;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.process.processFile.ProcessMapFileExplain;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.sqlprovider.server3.ConfigContents.ConfigItemPartMapMark;

import java.util.List;
import java.util.Map;

public enum ProcessMapEnum {
    INSTANCE;

    /**
     * @param mark      文件说明唯一标识
     * @param title     文件说明title
     * @param paramMaps
     * @param map
     * @return
     */
    @SuppressWarnings("incomplete-switch")
    public ProcessMapFileExplain createMapProcessFile(List<ConfigItemBean> configItemBeanList,
                                                      Map<String, Object> processMap) {
        ProcessMapFileExplain mapFileExplain = new ProcessMapFileExplain();
        // T.FLOW_AIM,T.FLOW_AREA,T.FLOW_NOUN_DEFINE,T.FLOW_INPUT,T.FLOW_OUTPUT,T.FLOW_SHOW_STEP,JF.FILE_NAME,JF.FILE_ID
        for (ConfigItemBean itemBean : configItemBeanList) {
            switch (ConfigItemPartMapMark.valueOf(itemBean.getMark())) {
                case processMapPurpose:// 目的
                    mapFileExplain.setFlowAim(valueOfWarpString(processMap.get("FLOW_AIM")));
                    break;
                case processMapScope: // 适用范围
                    mapFileExplain.setFlowArea(valueOfWarpString(processMap.get("FLOW_AREA")));
                    break;
                case processMapTerms:// 术语定义
                    mapFileExplain.setFlowNounDefine(valueOfWarpString(processMap.get("FLOW_NOUN_DEFINE")));
                    break;
                case processMapInput:// 输入
                    mapFileExplain.setFlowInput(valueOfWarpString(processMap.get("FLOW_INPUT")));
                    break;
                case processMapOutput:// 输出
                    mapFileExplain.setFlowOutput(valueOfWarpString(processMap.get("FLOW_OUTPUT")));
                    break;
                case processMap:// 流程架构
                    break;
                case processMapInstructions:// 流程步骤说明
                    mapFileExplain.setFlowShowStep(valueOfWarpString(processMap.get("FLOW_SHOW_STEP")));
                    break;
                case processMapAttachment:// 附件
                    if (processMap.get("FILE_ID") == null) {
                        break;
                    }
                    LinkResource fileLink = new LinkResource(processMap.get("FILE_ID"),
                            processMap.get("FILE_NAME").toString(), ResourceType.FILE);
                    mapFileExplain.setFileLink(fileLink);
                    break;
            }
        }

        return mapFileExplain;
    }

    private String valueOfWarpString(Object obj) {
        return ProcessFileEnum.INSTANCE.valueOfWarpString(obj);
    }
}
