package com.jecn.epros.domain.org;

import java.util.List;

public class OrgViewBean {
    private int total;
    private List<SearchOrgResultBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<SearchOrgResultBean> getData() {
        return data;
    }

    public void setData(List<SearchOrgResultBean> data) {
        this.data = data;
    }
}
