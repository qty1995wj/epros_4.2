package com.jecn.epros.server.service.process;

import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;

/**
 * 活动明细 接口类
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-9 时间：下午06:20:56
 */
public interface IWebProcessActiveService extends IBaseService<String, String> {
	/**
	 * 点击活动，获取活动信息
	 * 
	 * @param activeId
	 *            活动主键ID
	 * 
	 * @param isPub
	 *            true:发布
	 * 
	 */
	ProcessActiveWebBean getProcessActiveWebBean(Long activeId, boolean isPub)
			throws Exception;

}
