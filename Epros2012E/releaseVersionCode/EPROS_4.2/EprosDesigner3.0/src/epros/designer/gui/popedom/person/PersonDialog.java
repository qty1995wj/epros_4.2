package epros.designer.gui.popedom.person;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.popedom.role.RoleChooseDialog;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/***
 * 添加用户
 * 
 * @author 2012-05-21
 * 
 */
public abstract class PersonDialog extends JecnDialog {
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 左侧信息面板 */
	private JPanel leftPanel = null;

	/** 右侧信息面板 */
	private JPanel rightPanel = null;

	/** 按钮面板 */
	private JPanel buttonPanel = null;

	/** 中心控件显示面板 */
	private JPanel centerPanel = null;

	/** 登陆名称Lab */
	private JLabel loginNameLab = null;

	/** 登陆名称TextField */
	protected JecnTextField loginNameField = null;

	/** 登录名称验证Lab */
	protected JLabel loginNamePrompt = null;

	/** 登录名称必填提示符 */
	private JLabel logNamRequirLab = null;

	/** 所在部门Lab */
	private JLabel departLab = null;

	/** 所在部门Field */
	protected JecnTextField departField = null;

	/** departLab */
	private JLabel departEmpLab = null;

	/** 系统角色Lab */
	private JLabel systemRoleLab = null;

	/** 系统角色Field */
	protected JTextArea systemRoleField = null;
	protected String systemRoleIds = null;
	/** systemLab */
	protected JLabel systemEmpLab = null;

	/** 邮箱地址Lab */
	private JLabel emailLab = null;

	/** 邮箱地址Field */
	protected JecnTextField emailField = null;
	/** 邮箱地址验证 */
	protected JLabel emailVerfyLab = new JLabel();

	/** 真实姓名Lab */
	private JLabel realNameLab = null;

	/** 真实姓名Field */
	protected JecnTextField realNameField = null;

	/** 真实姓名验证Lab */
	private JLabel realNamePrompt = null;

	/** 真实姓名必填提示符 */
	private JLabel realNamRequirLab = null;

	/** 任职岗位Lab */
	private JLabel posLab = null;

	/** 任职岗位Field */
	protected JTextArea posField = null;
	protected String positionIds = null;
	/** posEmpLab */
	private JLabel posEmpLab = null;

	/** 密码Lab */
	private JLabel pwdLab = null;

	/** 密码Field */
	protected JPasswordField pwdField = null;

	/** 密码验证Lab */
	private JLabel pwdPrompt = null;

	/** 密码必填提示符 */
	private JLabel pwdRequirLab = null;

	/** 联系电话Lab */
	private JLabel phoneLab = null;

	/** 联系电话Field */
	protected JecnTextField phoneField = null;
	/** 联系电话验证 **/
	protected JLabel phoneVerfyLab = new JLabel();

	/** 外网单选按钮 */
	protected JRadioButton outRadioBut = null;

	/** 内网单选按钮 */
	protected JRadioButton inRadioBut = null;
	protected String emailType = "outer";// 邮件内外网,内网inner,外网outer

	/** 解锁单选按钮 */
	protected JRadioButton unlockRadioBut = null;

	/** 锁定单选按钮 */
	protected JRadioButton lockRadioBut = null;
	protected Long isLock = 0L;// 0是解锁，1是锁定

	/** 确定按钮 */
	private JButton okBut = null;

	/** 取消按钮 */
	private JButton cancelBut = null;

	/** 系统选择按钮 */
	private JButton systemBut = null;

	/** 岗位选择按钮 */
	private JButton posBut = null;

	/** 系统及选择按钮面板 */
	private JPanel systemPanel = null;

	/** 岗位及选择按钮面板 */
	private JPanel posPanel = null;

	/** 项的内容大小 */
	Dimension dimension = null;

	protected PeopleSelectCommon selectCommon = null;
	protected JecnTreeBean managerTreeBean = new JecnTreeBean();

	protected List<JecnTreeBean> posList = new ArrayList<JecnTreeBean>();
	protected List<JecnTreeBean> roleList = new ArrayList<JecnTreeBean>();

	public PersonDialog() {
	}

	/***
	 * 初始化组件
	 */
	@SuppressWarnings("serial")
	protected void initComponents() {
		this.setResizable(true);
		this.setModal(true);
		this.setSize(660, 340);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);
		// 主面板
		mainPanel = new JPanel();

		// 左侧信息面板
		leftPanel = new JPanel();

		// 右侧信息面板
		rightPanel = new JPanel();

		// 按钮面板
		buttonPanel = new JPanel();

		// 空面板
		centerPanel = new JPanel();

		// 登陆名称Lab
		loginNameLab = new JLabel(JecnProperties.getValue("loginNameC"));

		// 登陆名称TextField
		loginNameField = new JecnTextField();

		// 登录名称验证Lab
		loginNamePrompt = new JLabel();
		loginNamePrompt.setForeground(Color.red);
		dimension = new Dimension(220, 15);
		loginNamePrompt.setPreferredSize(dimension);
		loginNamePrompt.setMaximumSize(dimension);
		loginNamePrompt.setMinimumSize(dimension);

		// 登录名称必填提示符
		logNamRequirLab = new JLabel();
		// 设置提示符内容及颜色
		logNamRequirLab.setText(JecnProperties.getValue("required"));
		logNamRequirLab.setForeground(Color.red);

		// 所在部门Lab
		departLab = new JLabel(JecnProperties.getValue("inDeptC"));

		// 所在部门Field
		departField = new JecnTextField();
		departField.setEnabled(false);
		departField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// departLab
		departEmpLab = new JLabel();
		dimension = new Dimension(220, 15);
		departEmpLab.setPreferredSize(dimension);
		departEmpLab.setMaximumSize(dimension);
		departEmpLab.setMinimumSize(dimension);

		// 系统角色Lab
		systemRoleLab = new JLabel(JecnProperties.getValue("sysRoleC"));

		// 系统角色Field
		systemRoleField = new JTextArea();
		systemRoleField.setEnabled(false);
		dimension = new Dimension(180, 25);
		systemRoleField.setPreferredSize(dimension);
		systemRoleField.setMaximumSize(dimension);
		systemRoleField.setMinimumSize(dimension);
		systemRoleField.setBorder(BorderFactory.createLineBorder(Color.gray));
		// systemLab
		systemEmpLab = new JLabel();
		dimension = new Dimension(220, 15);
		systemEmpLab.setPreferredSize(dimension);
		systemEmpLab.setMaximumSize(dimension);
		systemEmpLab.setMinimumSize(dimension);
		systemEmpLab.setForeground(Color.red);

		// 邮箱地址Lab
		emailLab = new JLabel(JecnProperties.getValue("emailAddrC"));

		// 邮箱地址Field
		emailField = new JecnTextField();

		// 真实姓名Lab
		realNameLab = new JLabel(JecnProperties.getValue("realNameC"));

		// 真实姓名Field
		realNameField = new JecnTextField();

		// 真实姓名验证Lab
		realNamePrompt = new JLabel();
		realNamePrompt.setForeground(Color.red);
		dimension = new Dimension(220, 15);
		realNamePrompt.setPreferredSize(dimension);
		realNamePrompt.setMaximumSize(dimension);
		realNamePrompt.setMinimumSize(dimension);

		// 真实姓名必填提示符
		realNamRequirLab = new JLabel();
		// 设置提示符内容及颜色
		realNamRequirLab.setText(JecnProperties.getValue("required"));
		realNamRequirLab.setForeground(Color.red);

		// 任职岗位Lab
		posLab = new JLabel(JecnProperties.getValue("officePosC"));

		// 任职岗位Field
		posField = new JTextArea();
		posField.setEnabled(false);
		dimension = new Dimension(180, 25);
		posField.setPreferredSize(dimension);
		posField.setMaximumSize(dimension);
		posField.setMinimumSize(dimension);
		posField.setBorder(BorderFactory.createLineBorder(Color.gray));
		// posEmpLab
		posEmpLab = new JLabel();
		dimension = new Dimension(220, 15);
		posEmpLab.setPreferredSize(dimension);
		posEmpLab.setMaximumSize(dimension);
		posEmpLab.setMinimumSize(dimension);

		// 密码Lab
		pwdLab = new JLabel(JecnProperties.getValue("loginPwd"));

		// 密码Field
		pwdField = new JPasswordField();

		// 密码验证Lab
		pwdPrompt = new JLabel();
		pwdPrompt.setForeground(Color.red);
		dimension = new Dimension(220, 15);
		pwdPrompt.setPreferredSize(dimension);
		pwdPrompt.setMaximumSize(dimension);
		pwdPrompt.setMinimumSize(dimension);

		// 密码必填提示符
		pwdRequirLab = new JLabel();
		pwdRequirLab.setText("*");
		pwdRequirLab.setForeground(Color.red);
		// 联系电话Lab
		phoneLab = new JLabel(JecnProperties.getValue("contactTelC"));

		// 联系电话Field
		phoneField = new JecnTextField();

		// 外网单选按钮
		outRadioBut = new JRadioButton(JecnProperties.getValue("external"));
		outRadioBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		outRadioBut.setSelected(true);

		// 内网单选按钮
		inRadioBut = new JRadioButton(JecnProperties.getValue("intranet"));
		inRadioBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 解锁单选按钮
		unlockRadioBut = new JRadioButton(JecnProperties.getValue("unlockC"));
		unlockRadioBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		unlockRadioBut.setSelected(true);

		// 锁定单选按钮
		lockRadioBut = new JRadioButton(JecnProperties.getValue("lockC"));
		lockRadioBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));

		// 取消按钮
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

		// 系统选择按钮
		systemBut = new JButton(JecnProperties.getValue("selectBtn"));
		// ImageIcon icon = new ImageIcon("images/icons/choose.gif");
		// systemBut.setIcon(icon);
		// dimension = new Dimension(37, 25);
		// systemBut.setPreferredSize(dimension);
		// systemBut.setMaximumSize(dimension);
		// systemBut.setMinimumSize(dimension);

		// 岗位选择按钮
		posBut = new JButton(JecnProperties.getValue("selectBtn"));
		// ImageIcon icons = new ImageIcon("images/icons/choose.gif");
		// posBut.setIcon(icons);
		// dimension = new Dimension(37, 25);
		// posBut.setPreferredSize(dimension);
		// posBut.setMaximumSize(dimension);
		// posBut.setMinimumSize(dimension);

		// 系统及选择按钮面板
		systemPanel = new JPanel();

		// 岗位及选择按钮面板
		posPanel = new JPanel();

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置左边面板背景颜色
		leftPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置右边面板背景颜色
		rightPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置按钮面板背景颜色
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置空面板背景颜色
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 系统及选择按钮面板
		systemPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 岗位及选择按钮面板
		posPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 系统及选择按钮面板
		dimension = new Dimension(220, 25);
		systemPanel.setPreferredSize(dimension);
		systemPanel.setMaximumSize(dimension);
		systemPanel.setMinimumSize(dimension);

		// 岗位及选择按钮面板
		dimension = new Dimension(220, 25);
		posPanel.setPreferredSize(dimension);
		posPanel.setMaximumSize(dimension);
		posPanel.setMinimumSize(dimension);

		// 内外网按钮实现单选
		ButtonGroup bg1 = new ButtonGroup();
		bg1.add(outRadioBut);
		bg1.add(inRadioBut);

		// 解锁锁定按钮实现单选
		ButtonGroup bg2 = new ButtonGroup();
		bg2.add(unlockRadioBut);
		bg2.add(lockRadioBut);

		// 确定按钮监听
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		// 取消按钮监听
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		dimension = new Dimension(220, 15);
		// 联系电话验证
		phoneVerfyLab.setForeground(Color.red);
		phoneVerfyLab.setPreferredSize(dimension);
		phoneVerfyLab.setMaximumSize(dimension);
		phoneVerfyLab.setMinimumSize(dimension);
		// 邮箱验证
		emailVerfyLab.setForeground(Color.red);
		emailVerfyLab.setPreferredSize(dimension);
		emailVerfyLab.setMaximumSize(dimension);
		emailVerfyLab.setMinimumSize(dimension);

		pwdField.setDocument(new PlainDocument() {// 密码空格限制
					@Override
					public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
						super.insertString(offs, str.trim(), a);
					}
				});
	}

	/**
	 * 布局
	 */
	protected void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);
		Insets insets2 = new Insets(1, 5, 1, 1);
		// 中心控件显示面板（leftPanel rightPanel）
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(centerPanel, c);
		// centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 15, 103,
		// 15));
		centerPanel.setBorder(BorderFactory.createTitledBorder(""));
		centerPanel.setLayout(new GridBagLayout());

		// 左侧面板 布局
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(leftPanel, c);
		// leftPanel.setBorder(BorderFactory.createTitledBorder(""));
		leftPanel.setLayout(new GridBagLayout());
		// 左侧===========================第一行================================
		// 登录名称Lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		leftPanel.add(loginNameLab, c);
		// 登录名称Field
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		leftPanel.add(loginNameField, c);
		// 登录名称必填提示符*
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		leftPanel.add(logNamRequirLab, c);
		// 左侧===========================第二行================================
		// 登录名称验证Lab
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets2, 0, 0);
		leftPanel.add(loginNamePrompt, c);

		// 左侧===========================第三 行================================
		// 真实姓名Lab
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		leftPanel.add(realNameLab, c);
		// 真实姓名Field
		c = new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		leftPanel.add(realNameField, c);

		// 真实姓名必填提示符
		c = new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		leftPanel.add(realNamRequirLab, c);
		// 左侧===========================第四 行================================
		// 真实名称验证Lab
		c = new GridBagConstraints(1, 3, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets2, 0, 0);
		leftPanel.add(realNamePrompt, c);
		// 左侧===========================第五 行================================

		// 密码
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		leftPanel.add(pwdLab, c);
		c = new GridBagConstraints(1, 4, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		leftPanel.add(pwdField, c);
		c = new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		leftPanel.add(pwdRequirLab, c);

		// 左侧===========================第六 行================================
		c = new GridBagConstraints(1, 5, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets2, 0, 0);
		leftPanel.add(pwdPrompt, c);
		// 左侧===========================第七 行================================
		c = new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		leftPanel.add(emailLab, c);
		c = new GridBagConstraints(1, 6, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		leftPanel.add(emailField, c);
		// 邮箱验证提示
		c = new GridBagConstraints(1, 7, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets2, 0, 0);
		leftPanel.add(emailVerfyLab, c);
		// 左侧===========================第 九行================================
		JecnPanel deptManagerPanel = new JecnPanel();
		deptManagerPanel.setLayout(new GridBagLayout());
		selectCommon = new PeopleSelectCommon(0, deptManagerPanel, new Insets(1, 4, 1, 1), managerTreeBean, this,
				JecnProperties.getValue("deptLeader"), false);

		selectCommon.getJecnField().setEnabled(false);
		selectCommon.getJecnField().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension = new Dimension(10, 25);
		selectCommon.getJecnField().setPreferredSize(dimension);
		selectCommon.getJecnField().setMaximumSize(dimension);
		selectCommon.getJecnField().setMinimumSize(dimension);

		c = new GridBagConstraints(0, 8, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets2, 0, 0);
		leftPanel.add(deptManagerPanel, c);

		// 右侧面板 布局
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(rightPanel, c);
		// rightPanel.setBorder(BorderFactory.createTitledBorder(""));
		rightPanel.setLayout(new GridBagLayout());
		// 右侧===========================第一行================================

		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		rightPanel.add(systemRoleLab, c);
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		rightPanel.add(systemPanel, c);
		systemPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets2, 0, 0);
		systemPanel.add(systemRoleField, c);
		c = new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets2, 0,
				0);
		systemPanel.add(systemBut, c);

		// 右侧===========================第二行================================

		c = new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets2, 0,
				0);
		rightPanel.add(systemEmpLab, c);
		// 右侧===========================第三行================================

		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		rightPanel.add(posLab, c);
		c = new GridBagConstraints(1, 2, 2, 1, 10.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		rightPanel.add(posPanel, c);

		posPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets2, 0, 0);
		posPanel.add(posField, c);
		c = new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets2, 0,
				0);
		posPanel.add(posBut, c);

		// 右侧===========================第四行================================
		c = new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets2, 0,
				0);
		rightPanel.add(departEmpLab, c);

		// 右侧===========================第五行================================
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		rightPanel.add(departLab, c);

		c = new GridBagConstraints(1, 4, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		rightPanel.add(departField, c);

		// c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		// rightPanel.add(posLab, c);
		// c = new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		// rightPanel.add(posPanel, c);
		//
		// posPanel.setLayout(new GridBagLayout());
		// c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets2, 0, 0);
		// posPanel.add(posField, c);
		// c = new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets2, 0, 0);
		// posPanel.add(posBut, c);

		// 右侧===========================第六行================================
		c = new GridBagConstraints(1, 5, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets2, 0,
				0);
		rightPanel.add(posEmpLab, c);

		// 右侧===========================第七行================================
		c = new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		rightPanel.add(phoneLab, c);
		c = new GridBagConstraints(1, 6, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		rightPanel.add(phoneField, c);
		// 联系电话验证
		c = new GridBagConstraints(1, 7, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets2, 0, 0);
		rightPanel.add(phoneVerfyLab, c);
		// 右侧===========================第八行================================
		c = new GridBagConstraints(1, 8, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		rightPanel.add(new JLabel(), c);

		// 左侧===========================第 九行================================
		c = new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		rightPanel.add(outRadioBut, c);
		c = new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		rightPanel.add(inRadioBut, c);
		// c = new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		// rightPanel.add(unlockRadioBut, c);
		// c = new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		// rightPanel.add(lockRadioBut, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.EAST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(new JLabel(), c);
		// 按钮面板 布局
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new GridBagLayout());

		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		buttonPanel.add(okBut, c);

		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		buttonPanel.add(cancelBut, c);

		this.getContentPane().add(mainPanel);
	}

	protected void actionInit() {

		systemBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RoleChooseDialog roleChooseDialog = new RoleChooseDialog(roleList);
				roleChooseDialog.setVisible(true);
				if (roleChooseDialog.isOperation()) {
					if (roleList != null && roleList.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : roleList) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						systemRoleIds = sbIds.substring(0, sbIds.length() - 1);
						systemRoleField.setText(sbNames.substring(0, sbNames.length() - 1));
					} else {
						systemRoleIds = null;
						systemRoleField.setText("");
					}
				}
			}
		});
		posBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				if (JecnDesignerCommon.isSecondAdmin()) {
					// FIXME
					Set<Long> ids = JecnConstants.loginBean.getSetOrg();
					Long startId = -1L;
					if (ids != null && ids.size() > 0) {
						for (Long id : ids) {
							startId = id;
							break;
						}
					}
					positionIds = JecnDesignerCommon.setPositionLook(posList, posField, PersonDialog.this, positionIds,
							startId);
				} else {
					positionIds = JecnDesignerCommon.setPositionLook(posList, posField, PersonDialog.this, positionIds);

				}
				StringBuffer s = new StringBuffer();
				if (posList != null && posList.size() > 0) {
					for (JecnTreeBean b : posList) {
						s.append(b.getPname()).append("/");
					}
					departField.setText(s.toString().substring(0, s.length() - 1));
				} else {
					departField.setText("");
				}

			}
		});
		unlockRadioBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isLock = 0L;

			}
		});

		lockRadioBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isLock = 1L;

			}
		});
		outRadioBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				emailType = "outer";

			}
		});
		inRadioBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				emailType = "inner";

			}
		});
	}

	/***
	 * 确定按钮事件
	 */
	private void okButPerformed() {
		loginNamePrompt.setText("");
		pwdPrompt.setText("");
		emailVerfyLab.setText("");
		phoneVerfyLab.setText("");

		String loginName = loginNameField.getText().trim();
		String realName = realNameField.getText().trim();
		String pwd = (new String(pwdField.getPassword())).trim();
		// 验证登录名称
		if (!JecnValidateCommon.validateNameNoRestrict(loginName, loginNamePrompt)) {
			return;
		}
		if (validateLoginName(loginName)) {
			loginNamePrompt.setText(JecnProperties.getValue("nameHaved"));
			return;
		}
		// 验证真实姓名
		if (!JecnValidateCommon.validateNameNoRestrict(realName, realNamePrompt)) {
			return;
		}
		// 验证密码
		if (DrawCommon.isNullOrEmtryTrim(pwd)) {
			pwdPrompt.setText(JecnProperties.getValue("loginPwdNo"));
			return;
		} else if (DrawCommon.checkPasswordMaxLength(pwd)) { //密码不能大于20个字符
			pwdPrompt.setText(JecnProperties.getValue("passwordIsEnough"));
			return;
		}
		Pattern pat = Pattern.compile("[A-Za-z0-9~`!@#$%^&*()\\[\\]_+-={}:\"|;'<>?,./\\\\]+");//密码只能输入字母、数字或特殊字符!
		//Pattern pat = Pattern.compile("^(?=.*[0-9].*)(?=.*[A-Z].*)(?=.*[a-z].*).{6,20}$");//必须包含 大小写字母 和数字
		if (!pat.matcher(pwd).matches()) {
			pwdPrompt.setText(JecnProperties.getValue("passwordNoChineseOn"));
			return;
		}
		// 邮件格式验证
		String email = emailField.getText();
		if (!DrawCommon.isNullOrEmtryTrim(email)) {
			String regexTele = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
			Pattern pattern = Pattern.compile(regexTele);
			Matcher matcher = pattern.matcher(email);
			boolean flag = matcher.matches();
			if (!flag) {
				emailVerfyLab.setText(JecnProperties.getValue("emailErrorTip"));
				return;
			}
		}

		if (!JecnUserCheckUtil.checkNameLength(email).equals("")) {
			emailVerfyLab.setText(JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 电话长度验证
		String phone = phoneField.getText();

		if (!JecnUserCheckUtil.checkNameLength(phone).equals("")) {
			phoneVerfyLab.setText(JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		saveData();
	}

	public abstract boolean validateLoginName(String loginName);

	// 数据保存
	public abstract void saveData();

	/***
	 * 取消按钮事件
	 */
	private void cancelButPerformed() {
		this.dispose();
	}

	class JecnTextField extends JTextField {

		public JecnTextField() {
			initComponents();
		}

		private void initComponents() {
			// 项的内容大小
			dimension = new Dimension(220, 25);
			this.setPreferredSize(dimension);
			this.setMinimumSize(dimension);
			this.setMaximumSize(dimension);
		}
	}

}
