package org.apache.struts2.dispatcher.ng.filter;

import java.io.IOException;
import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.security.auth.Subject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import waffle.servlet.AutoDisposableWindowsPrincipal;
import waffle.servlet.NegotiateRequestWrapper;
import waffle.servlet.WindowsPrincipal;
import waffle.servlet.spi.SecurityFilterProvider;
import waffle.servlet.spi.SecurityFilterProviderCollection;
import waffle.util.AuthorizationHeader;
import waffle.windows.auth.IWindowsAuthProvider;
import waffle.windows.auth.IWindowsIdentity;
import waffle.windows.auth.IWindowsImpersonationContext;
import waffle.windows.auth.PrincipalFormat;
import waffle.windows.auth.impl.WindowsAuthProviderImpl;

/**
 * 
 * 域凭证验证过滤器
 *
 */
public class JecnNegotiateSecurityFilter implements Filter {
	private Logger _log = LoggerFactory
			.getLogger(JecnNegotiateSecurityFilter.class);
	private PrincipalFormat _principalFormat = PrincipalFormat.fqn;
	private PrincipalFormat _roleFormat = PrincipalFormat.fqn;
	private SecurityFilterProviderCollection _providers = null;
	private IWindowsAuthProvider _auth;
	private boolean _allowGuestLogin = true;
	private boolean _impersonate = false;
	private static final String PRINCIPAL_SESSION_KEY = JecnNegotiateSecurityFilter.class
			.getName()
			+ ".PRINCIPAL";

	public JecnNegotiateSecurityFilter() {
		this._log.debug("[waffle.servlet.JecnNegotiateSecurityFilter] loaded");
	}

	public void destroy() {
		this._log.info("[waffle.servlet.JecnNegotiateSecurityFilter] stopped");
	}

	public void doFilter(ServletRequest paramServletRequest,
			ServletResponse paramServletResponse, FilterChain paramFilterChain)
			throws IOException, ServletException {
		paramServletRequest.setCharacterEncoding("utf-8");
		HttpServletRequest localHttpServletRequest = (HttpServletRequest) paramServletRequest;
		HttpServletResponse localHttpServletResponse = (HttpServletResponse) paramServletResponse;
		
		this._log.debug(localHttpServletRequest.getMethod() + " "
				+ localHttpServletRequest.getRequestURI() + ", contentlength: "
				+ localHttpServletRequest.getContentLength());
		if (doFilterPrincipal(localHttpServletRequest,
				localHttpServletResponse, paramFilterChain))
			return;
		AuthorizationHeader localAuthorizationHeader = new AuthorizationHeader(
				localHttpServletRequest);
		if (!localAuthorizationHeader.isNull()) {
			IWindowsIdentity localIWindowsIdentity = null;
			try {
				localIWindowsIdentity = this._providers.doFilter(
						localHttpServletRequest, localHttpServletResponse);
				if (localIWindowsIdentity == null)
					return;
			} catch (Exception localException) {
				this._log.warn("error logging in user: "
						+ localException.getMessage());
				sendUnauthorized(localHttpServletResponse, true);
				return;
			}
			IWindowsImpersonationContext localIWindowsImpersonationContext = null;
			try {
				if ((!this._allowGuestLogin)
						&& (localIWindowsIdentity.isGuest())) {
					this._log.warn("guest login disabled: "
							+ localIWindowsIdentity.getFqn());
					sendUnauthorized(localHttpServletResponse, true);
					return;
				}
				this._log.debug("logged in user: "
						+ localIWindowsIdentity.getFqn() + " ("
						+ localIWindowsIdentity.getSidString() + ")");
				HttpSession localHttpSession = localHttpServletRequest
						.getSession(true);
				if (localHttpSession == null)
					throw new ServletException("Expected HttpSession");
				Subject localSubject = (Subject) localHttpSession
						.getAttribute("javax.security.auth.subject");
				if (localSubject == null)
					localSubject = new Subject();
				Object localObject1 = null;
				if (this._impersonate)
					localObject1 = new AutoDisposableWindowsPrincipal(
							localIWindowsIdentity, this._principalFormat,
							this._roleFormat);
				else
					localObject1 = new WindowsPrincipal(localIWindowsIdentity,
							this._principalFormat, this._roleFormat);
				this._log.debug("roles: "
						+ ((WindowsPrincipal) localObject1).getRolesString());
				localSubject.getPrincipals().add((Principal) localObject1);
				localHttpSession.setAttribute("javax.security.auth.subject",
						localSubject);
				this._log.info("successfully logged in user: "
						+ localIWindowsIdentity.getFqn());
				localHttpServletRequest.getSession().setAttribute(
						PRINCIPAL_SESSION_KEY, localObject1);
				NegotiateRequestWrapper localNegotiateRequestWrapper = new NegotiateRequestWrapper(
						localHttpServletRequest,
						(WindowsPrincipal) localObject1);
				if (this._impersonate) {
					this._log.debug("impersonating user");
					localIWindowsImpersonationContext = localIWindowsIdentity
							.impersonate();
				}
				paramFilterChain.doFilter(localNegotiateRequestWrapper,
						localHttpServletResponse);
			} finally {
				if ((this._impersonate)
						&& (localIWindowsImpersonationContext != null)) {
					this._log.debug("terminating impersonation");
					localIWindowsImpersonationContext.revertToSelf();
				} else {
					localIWindowsIdentity.dispose();
				}
			}
			return;
		}
		this._log.debug("authorization required");
		sendUnauthorized(localHttpServletResponse, false);
	}

	private boolean doFilterPrincipal(
			HttpServletRequest paramHttpServletRequest,
			HttpServletResponse paramHttpServletResponse,
			FilterChain paramFilterChain) throws IOException, ServletException {
		Principal localPrincipal = paramHttpServletRequest.getUserPrincipal();
		Object localObject1;
		if (localPrincipal == null) {
			localObject1 = paramHttpServletRequest.getSession(false);
			if (localObject1 != null)
				localPrincipal = (Principal) ((HttpSession) localObject1)
						.getAttribute(PRINCIPAL_SESSION_KEY);
		}
		if (localPrincipal == null)
			return false;
		if (this._providers.isPrincipalException(paramHttpServletRequest))
			return false;
		if ((localPrincipal instanceof WindowsPrincipal)) {
			this._log.debug("previously authenticated Windows user: "
					+ localPrincipal.getName());
			localObject1 = (WindowsPrincipal) localPrincipal;
			if ((this._impersonate)
					&& (((WindowsPrincipal) localObject1).getIdentity() == null))
				return false;
			NegotiateRequestWrapper localNegotiateRequestWrapper = new NegotiateRequestWrapper(
					paramHttpServletRequest, (WindowsPrincipal) localObject1);
			IWindowsImpersonationContext localIWindowsImpersonationContext = null;
			if (this._impersonate) {
				this._log.debug("re-impersonating user");
				localIWindowsImpersonationContext = ((WindowsPrincipal) localObject1)
						.getIdentity().impersonate();
			}
			try {
				paramFilterChain.doFilter(localNegotiateRequestWrapper,
						paramHttpServletResponse);
			} finally {
				if ((this._impersonate)
						&& (localIWindowsImpersonationContext != null)) {
					this._log.debug("terminating impersonation");
					localIWindowsImpersonationContext.revertToSelf();
				}
			}
		} else {
			this._log.debug("previously authenticated user: "
					+ localPrincipal.getName());
			paramFilterChain.doFilter(paramHttpServletRequest,
					paramHttpServletResponse);
		}
		return true;
	}

	public void init(FilterConfig paramFilterConfig) throws ServletException {
		HashMap localHashMap = new HashMap();
		Object localObject1 = null;
		String[] arrayOfString = null;
		Object localObject2;
		Object localObject3;
		if (paramFilterConfig != null) {
			Enumeration localEnumeration = paramFilterConfig
					.getInitParameterNames();
			while (localEnumeration.hasMoreElements()) {
				localObject2 = (String) localEnumeration.nextElement();
				localObject3 = paramFilterConfig
						.getInitParameter((String) localObject2);
				this._log.debug((String) localObject2 + "="
						+ (String) localObject3);
				if (((String) localObject2).equals("principalFormat"))
					this._principalFormat = PrincipalFormat
							.valueOf((String) localObject3);
				else if (((String) localObject2).equals("roleFormat"))
					this._roleFormat = PrincipalFormat
							.valueOf((String) localObject3);
				else if (((String) localObject2).equals("allowGuestLogin"))
					this._allowGuestLogin = Boolean
							.parseBoolean((String) localObject3);
				else if (((String) localObject2).equals("impersonate"))
					this._impersonate = Boolean
							.parseBoolean((String) localObject3);
				else if (((String) localObject2)
						.equals("securityFilterProviders"))
					arrayOfString = ((String) localObject3).split("\\s+");
				else if (((String) localObject2).equals("authProvider"))
					localObject1 = localObject3;
				else
					localHashMap.put(localObject2, localObject3);
			}
		}
		if (localObject1 != null)
			try {
				this._auth = ((IWindowsAuthProvider) Class.forName(
						(String) localObject1).getConstructor(new Class[0])
						.newInstance(new Object[0]));
			} catch (Exception localException1) {
				this._log.error("error loading '" + localObject1 + "': "
						+ localException1.getMessage());
				throw new ServletException(localException1);
			}
		if (this._auth == null)
			this._auth = new WindowsAuthProviderImpl();
		if (arrayOfString != null)
			this._providers = new SecurityFilterProviderCollection(
					arrayOfString, this._auth);
		if (this._providers == null) {
			this._log.debug("initializing default security filter providers");
			this._providers = new SecurityFilterProviderCollection(this._auth);
		}
		Iterator localIterator = localHashMap.entrySet().iterator();
		while (localIterator.hasNext()) {
			localObject2 = (Map.Entry) localIterator.next();
			localObject3 = ((String) ((Map.Entry) localObject2).getKey())
					.split("/", 2);
			String[] object3 = (String[]) localObject3;
			if (object3.length == 2) {
				try {
					this._log.debug("setting " + object3[0] + ", " + object3[1]
							+ "="
							+ (String) ((Map.Entry) localObject2).getValue());
					SecurityFilterProvider localSecurityFilterProvider = this._providers
							.getByClassName(object3[0]);
					localSecurityFilterProvider.initParameter(object3[1],
							(String) ((Map.Entry) localObject2).getValue());
				} catch (ClassNotFoundException localClassNotFoundException) {
					this._log.error("invalid class: " + object3[0] + " in "
							+ (String) ((Map.Entry) localObject2).getKey());
					throw new ServletException(localClassNotFoundException);
				} catch (Exception localException2) {
					this._log
							.error(object3[0] + ": error setting '"
									+ object3[1] + "': "
									+ localException2.getMessage());
					throw new ServletException(localException2);
				}
			} else {
				this._log.error("Invalid parameter: "
						+ (String) ((Map.Entry) localObject2).getKey());
				throw new ServletException("Invalid parameter: "
						+ (String) ((Map.Entry) localObject2).getKey());
			}
		}
		this._log.info("[waffle.servlet.JecnNegotiateSecurityFilter] started");
	}

	public void setPrincipalFormat(String paramString) {
		this._principalFormat = PrincipalFormat.valueOf(paramString);
		this._log.info("principal format: " + this._principalFormat);
	}

	public PrincipalFormat getPrincipalFormat() {
		return this._principalFormat;
	}

	public void setRoleFormat(String paramString) {
		this._roleFormat = PrincipalFormat.valueOf(paramString);
		this._log.info("role format: " + this._roleFormat);
	}

	public PrincipalFormat getRoleFormat() {
		return this._roleFormat;
	}

	private void sendUnauthorized(HttpServletResponse paramHttpServletResponse,
			boolean paramBoolean) {
		try {
			this._providers.sendUnauthorized(paramHttpServletResponse);
			if (paramBoolean)
				paramHttpServletResponse.setHeader("Connection", "close");
			else
				paramHttpServletResponse.setHeader("Connection", "keep-alive");
			paramHttpServletResponse.sendError(401);
			paramHttpServletResponse.flushBuffer();
		} catch (IOException localIOException) {
			throw new RuntimeException(localIOException);
		}
	}

	public IWindowsAuthProvider getAuth() {
		return this._auth;
	}

	public void setAuth(IWindowsAuthProvider paramIWindowsAuthProvider) {
		this._auth = paramIWindowsAuthProvider;
	}

	public boolean isAllowGuestLogin() {
		return this._allowGuestLogin;
	}

	public void setImpersonate(boolean paramBoolean) {
		this._impersonate = paramBoolean;
	}

	public boolean isImpersonate() {
		return this._impersonate;
	}

	public SecurityFilterProviderCollection getProviders() {
		return this._providers;
	}
}