package com.jecn.epros.email;

public class EmailBuilderFactory {

    public enum EmailBuilderType {
        /**
         * 流程有效期到期提醒
         */
        PROCESS_EXPIRY_DATE,
        /**
         * 合理化建议处理提醒
         */
        PROPOSE_HANDLER,
        /**
         * 合理化建议处理结果反馈
         */
        PROPOSE_HANDLER_RESULT,
        /**
         * 服务器到期提醒
         */
        SERVER_STOP,
        /**
         * 流程审批发布提醒
         */
        PROCESS_TASK_PUBLISH,
        /**
         * 流程记录文控发布提醒
         */
        PROCESS_PUBLISH_RECORD_HISTORY,
        /**
         * 搁置任务提醒
         */
        TASK_DELAY,
        /**
         * 任务超时未审批提醒
         */
        TASK_APPROVE_DELAY,
        /**
         * 任务审批提醒
         */
        TASK_APPROVE,
        /**
         * 任务审批完成提醒
         */
        TASK_FINISH

    }

    public static BaseEmailBuilder getEmailBuilder(EmailBuilderType builderType) {
        BaseEmailBuilder builder = null;
        switch (builderType) {
            case PROCESS_EXPIRY_DATE:
                builder = new ProcessExpiryDateEmailBuilder();
                break;
            case PROPOSE_HANDLER:
                builder = new ProposeHandlerEmailBuilder();
                break;
            case PROPOSE_HANDLER_RESULT:
                builder = new ProposeHandlerResultEmailBuilder();
                break;
            case SERVER_STOP:
                builder = new ServerStopEmailBuilder();
                break;
            case PROCESS_TASK_PUBLISH:
                builder = new ProcessTaskPublishEmailBuilder();
                break;
            case PROCESS_PUBLISH_RECORD_HISTORY:
                builder = new ProcessPublishRecordHistoryEmailBuilder();
                break;
            case TASK_DELAY:
                builder = new TaskDelayEmailBuilder();
                break;
            case TASK_APPROVE_DELAY:
                builder = new TaskApproveDelayEmailBuilder();
                break;
            case TASK_APPROVE:
                builder = new TaskApproveEmailBuilder();
                break;
            case TASK_FINISH:
                builder = new TaskFinishEmailBuilder();
                break;
        }
        return builder;
    }

}
