package com.jecn.epros.server.dao.process;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.bean.process.FlowOrgT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT2;
import com.jecn.epros.server.common.IBaseDao;

public interface IProcessBasicInfoDao extends
		IBaseDao<JecnFlowBasicInfoT, Long> {

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程责任部门
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public Object[] getFlowRelateOrgIds(Long flowId, boolean isPub)
			throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程相关制度
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowRelateRules(Long flowId, boolean isPub)
			throws Exception;
	
	/**
	 * 
	 * 农夫三泉操作说明下载时候专用
	 * 
	 * @description：获得流程相关制度
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowRelateRulesNFSQ(Long flowId, boolean isPub)
			throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程相关制度
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowRelateRulesWeb(Long flowId, boolean isPub)
			throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程相关标准
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowRelateStandardBeans(Long flowId, boolean isPub)
			throws Exception;

	/**
	 * @author zhangchen Jul 13, 2012
	 * @description：获得流程责任部门
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public FlowOrgT getFlowOrgTByFlowId(Long flowId) throws Exception;

	/**
	 * 获取用户名称
	 * 
	 * @author fuzhh May 9, 2013
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String getUserName(Long userId) throws Exception;

	/**
	 * 获取岗位名称
	 * 
	 * @author fuzhh May 9, 2013
	 * @param posId
	 * @return
	 * @throws Exception
	 */
	public String getPosName(Long posId) throws Exception;
	
	/**
	 * 获取岗位名称
	 * @param userId 
	 * @return
	 * @throws Exception
	 */
	public List<String> getPosNameByUserId(Long userId) throws Exception;
	
	/**
	 * 获取流程自定义要素 处理D2 mysql大字段 
	 * @param flowId 流程ID
	 * @return
	 * @throws Exception
	 */
	public JecnFlowBasicInfoT2 getFlowBasicInfoT2(Long flowId) throws Exception;

	/**
	 * 获得该流程基本信息表临时表
	 * @param rid 流程id
	 * @return
	 */
	public JecnFlowBasicInfoT getFlowBasicInfoT(Long rid) throws Exception;

	/**
	 * 更新有效期
	 * @param flowId 流程id
	 * @param docId 文控id
	 * @param expiry 有效期
	 * @param nextScandDate 下次审视时间
	 * @param type 
	 * @throws Exception
	 */
	public void updateExpiry(Long flowId,Long docId, int expiry, String nextScandDate, int type)throws Exception;

	/**
	 * 有该流程的查阅权限的部门
	 * @param flowId 流程id
	 * @param isPub 是否发布
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowOrg> getFlowOrgPerm(Long flowId, boolean isPub)throws Exception;

	/**
	 * 流程相关标准化文件
	 * @param flowId
	 * @param isPub
	 * @return
	 */
	public List<String[]> getFlowRelateStandardFile(Long flowId, boolean isPub);

	public List<FlowFileHeadData> findFlowFileHeadInfo(Long flowId);



	
}
