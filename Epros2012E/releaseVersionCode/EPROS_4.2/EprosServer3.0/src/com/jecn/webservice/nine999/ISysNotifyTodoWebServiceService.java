/**
 * ISysNotifyTodoWebServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.nine999;

public interface ISysNotifyTodoWebServiceService extends javax.xml.rpc.Service {
    public java.lang.String getISysNotifyTodoWebServicePortAddress();

    public com.jecn.webservice.nine999.ISysNotifyTodoWebService getISysNotifyTodoWebServicePort() throws javax.xml.rpc.ServiceException;

    public com.jecn.webservice.nine999.ISysNotifyTodoWebService getISysNotifyTodoWebServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
