package com.jecn.epros.server.bean.process;

import java.util.Date;

/**
 * 一级指标Bean
 * @Time 2014-10-17
 *
 */
public class JecnKPIFirstTargetBean implements java.io.Serializable {
	/**主键ID*/
	private String id;
	/**内容*/
	private String targetContent;
	/**创建时间*/
	private Date createTime;
	/**更新时间*/
	private Date updateTime;
	/**创建人*/
	private Long createPeopleId;
	/**更新人*/
	private Long updatePeopleId;
	/**标识：0：普通状态；1：选中行为新添加的数据设置为1，只能选中一行*/
	private Integer sortId = 0; 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTargetContent() {
		return targetContent;
	}
	public void setTargetContent(String targetContent) {
		this.targetContent = targetContent;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Long getCreatePeopleId() {
		return createPeopleId;
	}
	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}
	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}
	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}
	public Integer getSortId() {
		return sortId;
	}
	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}
	
}
