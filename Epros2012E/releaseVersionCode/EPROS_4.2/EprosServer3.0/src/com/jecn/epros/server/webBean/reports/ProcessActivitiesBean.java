package com.jecn.epros.server.webBean.reports;

import java.util.List;

import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 活动数统计Bean
 * @author fuzhh Mar 4, 2013
 *
 */
public class ProcessActivitiesBean {

	/** 活动数 */
	private int activitiesCount;
	/** 合计 */
	private int allCount;
	/** 流程具体数据bean*/
	private List<ProcessWebBean> processWebList;

	public int getActivitiesCount() {
		return activitiesCount;
	}

	public void setActivitiesCount(int activitiesCount) {
		this.activitiesCount = activitiesCount;
	}

	public int getAllCount() {
		return allCount;
	}

	public void setAllCount(int allCount) {
		this.allCount = allCount;
	}

	public List<ProcessWebBean> getProcessWebList() {
		return processWebList;
	}

	public void setProcessWebList(List<ProcessWebBean> processWebList) {
		this.processWebList = processWebList;
	}
}
