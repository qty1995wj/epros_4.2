package epros.designer.gui.file;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.bean.file.FileAttributeBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.system.AccessAuthorityCommon;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.gui.system.PersonliableSelectCommon;
import epros.designer.gui.system.ResDepartSelectCommon;
import epros.designer.gui.system.combo.CheckBoxPo;
import epros.designer.gui.system.field.JecnTipTextField;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 添加文件
 * 
 * @author 2012-05-25
 * 
 */
public class AddFileDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(AddFileDialog.class);
	/** 主面板 */
	protected JecnPanel mainPanel = null;

	/** 添加文件面板 */
	private JecnPanel addFilePanel = null;

	/** 添加文件滚动面板 */
	private JScrollPane resultScrollPane = null;

	/** 添加文件按钮面板 */
	private JPanel buttonPanel = null;
	/** 保存面板按钮 */
	private JPanel saveButPanel = null;

	/** 添加文件Table */
	protected JTable addFileTable = null;

	/** 上传按钮 */
	private JButton uploadBut = null;

	/** 打开按钮 */
	private JButton openBut = null;

	/** 删除 */
	private JButton deleteBut = null;
	private JLabel tipLabel = new JLabel();
	/** 点击确定，底部面板验证提示显示Lab */
	private JLabel mainLab = new JLabel();
	/*** 底部面板(验证提示，确定取消按钮) */
	private JPanel mainLabPane = new JPanel();
	/** 底部验证提示所在滚动面板 **/
	private JScrollPane mainLabScroPane = null;

	/** 确定 */
	private JButton okBut = null;

	/** 取消 */
	private JButton cancelBut = null;

	/** 设置大小 */
	Dimension dimension = null;

	protected JecnTreeNode pNode = null;
	private JecnTree jTree = null;

	/** 流程责任部门 */
	ResDepartSelectCommon resDepartSelectCommon = null;
	/** 查阅权限 */
	AccessAuthorityCommon accessAuthorityCommon = null;

	/** 是不是东软 */
	private boolean isDRCompany = JecnConfigTool.isDRYLOperType();

	// 验证提示
	protected JLabel verfyLab = new JLabel();

	/** 责任人 */
	private PersonliableSelectCommon personliableSelect = null;
	/** 专员 */
	private PeopleSelectCommon commissionerPeopleSelectCommon = null;

	/** 关键字 */
	private JecnTipTextField keyWordField = null;

	private FileAttributeBean fileAttributeBean = null;
	private Vector<CheckBoxPo> checkBoxs = null;
	/** 自动编号 目录最大流水号 */
	protected int codeTotal = 0;
	/** 目录对应编号规则 */
	private String parentCode = null;
	/** 是否显示渲染列，文件类型 */
	protected boolean isShowFileTypeInTable;

	public AddFileDialog(JecnTreeNode node, JecnTree jTree) {
		this.pNode = node;
		this.jTree = jTree;
		// 设置窗体大小不可改变
		this.setResizable(true);
		this.setModal(true);
		this.setSize(getWidthMax(), 600);
		this.setLocationRelativeTo(null);
		this.setTitle(JecnProperties.getValue("uploadFile"));
		initData();
		initCompotents();
		initLayout();
		buttonActionInit();
	}

	private void initData() {
		try {
			fileAttributeBean = ConnectionPool.getFileAction()
					.getFileAttributeBeanById(pNode.getJecnTreeBean().getId());
			Long flowId = ConnectionPool.getFileAction().getFileRelatedFlowId(pNode.getJecnTreeBean().getId());
			if (flowId != null) {
				ProcessAttributeBean flowAttribute = ConnectionPool.getProcessAction().getFlowAttribute(flowId);
				if (flowAttribute != null) {
					fileAttributeBean.setResPeopleId(flowAttribute.getDutyUserId());
					fileAttributeBean.setRePeopleName(flowAttribute.getDutyUserName());
					fileAttributeBean.setResPeopleType(flowAttribute.getDutyUserType());
					fileAttributeBean.setCommissionerId(flowAttribute.getCommissionerId());
					fileAttributeBean.setCommissionerName(flowAttribute.getCommissionerName());
					fileAttributeBean.setKeyWord(flowAttribute.getKeyWord());
				}
			}
			isShowFileTypeInTable = JecnConfigTool.isShowFileTypeInTable();
			if (isShowFileTypeInTable) {
				checkBoxs = getListCheckBoxPo();
			}
			if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode)) {
				AutoCodeResultData resultData = JecnDesignerCommon.getStandardizedAutoCodeResultData(pNode
						.getJecnTreeBean().getId(), 1);
				if (resultData == null) {
					return;
				}
				// 获取目录编号
				parentCode = resultData.getDirCode();
				codeTotal = resultData.getCodeTotal();
			}
		} catch (Exception e) {
			log.error("AddFileDialog initData is error", e);
		}

	}

	/***
	 * 初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JecnPanel();
		// 添加文件面板
		addFilePanel = new JecnPanel(getWidthMax() - 10, 240);
		// 添加文件滚动面板
		resultScrollPane = new JScrollPane();
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 添加文件按钮面板
		buttonPanel = new JPanel();
		// 保存面板按钮
		saveButPanel = new JPanel();
		mainLabPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 上传按钮
		uploadBut = new JButton(JecnProperties.getValue("uploadBut"));
		// 打开按钮
		openBut = new JButton(JecnProperties.getValue("openBtn"));
		// 删除
		deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));
		// 设置名称验证提示文字颜色
		tipLabel.setForeground(Color.red);
		mainLab.setForeground(Color.red);
		verfyLab.setForeground(Color.red);
		// 确定
		okBut = new JButton(JecnProperties.getValue("okBtn"));
		// 取消
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
		// 设置主面板默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 添加文件面板
		addFilePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 滚动面板
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 添加文件按钮面板
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		saveButPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 底部面板
		mainLabPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 验证提示滚动设置tipLabScroPane
		mainLabScroPane = new JScrollPane(mainLab);
		mainLabScroPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainLabScroPane.setBorder(null);
		mainLabScroPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mainLabScroPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 底部验证提示所在滚动面板
		mainLab.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		initChildComponents();
	}

	protected void initChildComponents() {
	}

	/**
	 * 布局
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());

		GridBagConstraints c = null;

		Insets insets = new Insets(2, 2, 2, 2);

		int rows = 0;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileResPeople)) {
			// 文件责任人
			personliableSelect = new PersonliableSelectCommon(rows, mainPanel, insets, fileAttributeBean
					.getResPeopleId(), fileAttributeBean.getResPeopleType(), fileAttributeBean.getRePeopleName(), this,
					2, JecnConfigTool.isRequest("isShowFileResPeople"));
			rows++;
		}
		// 专员
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileCommissioner)) {
			this.commissionerPeopleSelectCommon = new PeopleSelectCommon(rows, mainPanel, insets, fileAttributeBean
					.getCommissionerId(), fileAttributeBean.getCommissionerName(), this, JecnProperties
					.getValue("commissionerC"), JecnConfigTool.isRequest("isShowFileCommissioner"));
			rows++;
		}
		// 关键字
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileKeyWord)) {
			this.keyWordField = new JecnTipTextField(rows, mainPanel, insets, JecnProperties.getValue("keyWordC"),
					fileAttributeBean.getKeyWord(), JecnProperties.getValue("keyWord"), JecnConfigTool
							.isRequest("isShowFileKeyWord"));
			rows++;
		}
		// 责任部门
		resDepartSelectCommon = new ResDepartSelectCommon(rows, mainPanel, insets, null, "", this, isDRCompany);
		rows++;
		accessAuthorityCommon = new AccessAuthorityCommon(rows, mainPanel, insets, pNode.getJecnTreeBean().getId(), 1,
				this, true, true);
		rows = accessAuthorityCommon.getRows();

		customPanel(rows, mainPanel, insets);
		rows++;

		// 添加文件面板============================布局
		c = new GridBagConstraints(0, rows, 4, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(addFilePanel, c);
		rows++;
		addFilePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("addFile")));
		addFilePanel.setLayout(new GridBagLayout());

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		addFileTable = new JecnAddFileTable();

		resultScrollPane.setViewportView(addFileTable);
		addFilePanel.add(resultScrollPane, c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		addFilePanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel.add(uploadBut);
		buttonPanel.add(openBut);
		buttonPanel.add(deleteBut);
		buttonPanel.add(tipLabel);

		c = new GridBagConstraints(0, rows, 4, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(mainLabPane, c);

		mainLabPane.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainLabPane.add(mainLabScroPane, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		mainLabPane.add(saveButPanel, c);
		saveButPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		saveButPanel.add(verfyLab);
		saveButPanel.add(okBut);
		saveButPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);
	}

	protected void customPanel(int rows, JecnPanel mainPanel, Insets insets) {

	}

	private void buttonActionInit() {
		uploadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				uploadButAction();
			}
		});
		openBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				openButAction();
			}
		});

		deleteBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteButAction();
			}
		});
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButAction();
			}
		});

		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}

		});
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});

	}

	private void uploadButAction() {
		if (isUploadFileValidate()) {
			return;
		}
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFileManageDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);
		fileChoose.setEidtTextFiled(false);

		// 可以同时上传多个文件
		fileChoose.setMultiSelectionEnabled(true);
		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("fileUpload"));
		int i = fileChoose.showSaveDialog(this);
		if (i == JFileChooser.APPROVE_OPTION) {
			// 选择的文件
			File[] files = fileChoose.getSelectedFiles();
			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSaveFileManageDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			Vector<Vector<Object>> vs = ((DefaultTableModel) addFileTable.getModel()).getDataVector();
			F: for (File f : files) {
				for (Vector t : vs) {// 判断是否已上传
					if (f.getPath().equals((String) t.get(2))) {
						continue F;
					}
				}
				Vector<Object> v = new Vector<Object>();
				addFileCode(v);
				v.add(JecnUtil.getFileName(f.getPath()));
				v.add(f.getPath());
				if (JecnConfigTool.isShowFileTypeInTable()) {
					v.add(checkBoxs.get(0));
				}
				((DefaultTableModel) addFileTable.getModel()).addRow(v);
			}
		}
	}

	protected boolean isUploadFileValidate() {
		return false;
	}

	protected void addFileCode(Vector<Object> v) {
		if (StringUtils.isNotBlank(parentCode)) {// 标准版文件编号
			v.add(parentCode + getCodeString(codeTotal));
			codeTotal++;
		} else {
			v.add("");
		}
	}

	private String getCodeString(int codeTotal) {
		return codeTotal < 10 ? "0" + codeTotal : codeTotal + "";
	}

	private void openButAction() {
		int[] selectRows = addFileTable.getSelectedRows();
		if (selectRows.length == 0) {
			tipLabel.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		for (int i = 0; i < selectRows.length; i++) {
			String filePath = (String) addFileTable.getValueAt(selectRows[i], 2);
			JecnUtil.openLocalFile(filePath);
		}
		tipLabel.setText("");
	}

	private void deleteButAction() {
		int[] selectRows = addFileTable.getSelectedRows();
		if (selectRows.length == 0) {
			tipLabel.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) addFileTable.getModel()).removeRow(selectRows[i]);
			if (StringUtils.isNotBlank(parentCode)) {
				codeTotal--;
			}
		}
		tipLabel.setText("");
	}

	private void okButAction() {
		UploadTask uploadTask = new UploadTask();
		JecnLoading.start(uploadTask);

	}

	protected void upload() throws Exception {
		verfyLab.setText("");
		// 关键字 长度验证
		if(this.keyWordField != null){
			if (this.keyWordField.isValidateNotPass(verfyLab)) {
				return;
			}
		}
		AddFileTableMode mode = (AddFileTableMode) addFileTable.getModel();
		if (mode.getDataVector().size() == 0) {
			tipLabel.setText(JecnProperties.getValue("pleaseChooseUploadFile"));
			return;
		}
		if (addFileTable.isEditing()) {
			addFileTable.getCellEditor().stopCellEditing();
		}

		Long relatedFlowId = null; // 流程关联ID
		if (JecnConfigTool.isArchitectureUploadFile()) {
			JecnFileBeanT fileBeanT = ConnectionPool.getFileAction().getFilBeanById(pNode.getJecnTreeBean().getId());
			relatedFlowId = fileBeanT.getFlowId();
		}

		Long relatedRuleId = null; // 制度 关联ID
		if (JecnConfigTool.isArchitectureRuleUploadFile()) {
			JecnFileBeanT fileBeanT = ConnectionPool.getFileAction().getFilBeanById(pNode.getJecnTreeBean().getId());
			relatedRuleId = fileBeanT.getRuleId();
		}

		int rows = addFileTable.getRowCount();
		JecnFileBeanT fileBean = null;
		List<JecnFileBeanT> list = new ArrayList<JecnFileBeanT>();
		// 用于验证文件是否重复
		List<String> names = new ArrayList<String>();
		// 获取父节点的hide值
		for (int i = 0; i < rows; i++) {
			if (JecnConfigTool.isFileNumMust()) {
				String num = (String) mode.getValueAt(i, 0);
				if (StringUtils.isBlank(num)) {
					tipLabel.setText(JecnProperties.getValue("fileNum") + "" + JecnProperties.getValue("isNotEmpty"));
					return;
				}
			}
			String s = JecnUserCheckUtil.checkNameLength((String) mode.getValueAt(i, 0));
			if (!DrawCommon.isNullOrEmtryTrim(s)) {
				tipLabel.setText(JecnProperties.getValue("fileNum") + s);
				return;
			}
			fileBean = new JecnFileBeanT();
			fileBean.setFileName((String) mode.getValueAt(i, 1));
			fileBean.setPeopleID(JecnConstants.loginBean.getJecnUser().getPeopleId());
			fileBean.setPerFileId(pNode.getJecnTreeBean().getId());
			fileBean.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
			fileBean.setIsDir(1);// 1是文件 0是目录
			fileBean.setOrgId(this.resDepartSelectCommon.getIdResult());
			fileBean.setDocId((String) mode.getValueAt(i, 0));
			fileBean.setHide(1); // 0 隐藏 1显示
			fileBean.setIsPublic(accessAuthorityCommon.getPublicStatic() == 0 ? 0L : 1L);
			fileBean.setSortId(JecnTreeCommon.getMaxSortFromDB(pNode) + i);
			fileBean.setProjectId(JecnConstants.projectId);
			fileBean.setFileStream(JecnUtil.changeFileToByte((String) mode.getValueAt(i, 2)));
			fileBean.setDelState(0);
			fileBean.setFlowId(relatedFlowId);
			fileBean.setRuleId(relatedRuleId);
			// 保密级别
			fileBean.setConfidentialityLevel(accessAuthorityCommon.getConfidentialityLevelStatic());
			if (this.personliableSelect != null) {
				fileBean.setResPeopleId(personliableSelect.getSingleSelectCommon().getIdResult());
				fileBean.setTypeResPeople(personliableSelect.getPeopleResType());
			}
			if (this.commissionerPeopleSelectCommon != null) {
				fileBean.setCommissionerId(commissionerPeopleSelectCommon.getIdResult());
			}
			if (this.keyWordField != null) {
				fileBean.setKeyword(keyWordField.getResultStr());
			}
			list.add(fileBean);
			names.add((String) mode.getValueAt(i, 1));
		}

		try {
			if (validateNamefullPath(names)) {
				return;
			}
			String[] ns = ConnectionPool.getFileAction().validateAddName(names, pNode.getJecnTreeBean().getId(), 1);
			if (ns != null && ns.length > 0) {
				StringBuffer tip = new StringBuffer();
				for (String n : ns) {
					if (tip.length() == 0) {
						tip.append(n).append(",");
					} else {
						tip.append("<br>");
						tip.append(n).append(",");
					}

				}
				String tipText = tip.toString().substring(0, tip.toString().length() - 1) + " "
						+ JecnProperties.getValue("haved");
				mainLab.setText("<html>" + tipText + "</html>");
				return;
			}
			AccessId accId = new AccessId();
			accId.setOrgAccessId(accessAuthorityCommon.getOrgAccessIds());
			accId.setPosAccessId(accessAuthorityCommon.getPosAccessIds());
			accId.setPosGroupAccessId(accessAuthorityCommon.getPosGroupAcessIds());
			ConnectionPool.getFileAction().addFiles(list, accId, false, codeTotal - 1);
			List<JecnTreeBean> listNode = ConnectionPool.getFileAction().getChildFiles(pNode.getJecnTreeBean().getId(),
					JecnConstants.projectId);
			JecnTreeCommon.refreshNode(pNode, listNode, jTree);
			JecnTreeCommon.autoExpandNode(jTree, pNode);
			tipLabel.setText("");
			this.setVisible(false);
		} catch (Exception e) {
			log.error("AddFileDialog upload is error！", e);
		}
	}

	/**
	 * 提示 文件库中是否有重名文件
	 * 
	 * @param names
	 * @return
	 */
	private boolean validateNamefullPath(List<String> names) {
		try {
			String[] ns = ConnectionPool.getFileAction()
					.validateNamefullPath(names, pNode.getJecnTreeBean().getId(), 1);
			if (ns != null && ns.length > 0) {
				StringBuffer tip = new StringBuffer();
				for (String n : ns) {
					if (tip.length() == 0) {
						tip.append(n).append(",");
					} else {
						tip.append("\n");
						tip.append(n).append(",");
					}
				}
				String tipText = tip.toString().substring(0, tip.toString().length() - 1) + " \n" +" "+JecnProperties.getValue("itExistsDirectories");
				// 是否删除提示框
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), tipText, null,
						JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					return true;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private boolean isUpdate() {
		if (resDepartSelectCommon.isUpdate()) {
			return true;
		}
		if (accessAuthorityCommon.isUpdate()) {
			return true;
		}
		if (((DefaultTableModel) addFileTable.getModel()).getDataVector().size() > 0) {
			return true;
		}

		return false;
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButAction();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	class JecnAddFileTable extends JTable {
		public JecnAddFileTable() {
			this.setModel(getTableModel());
			// 添加渲染
			addCellEditor(this);
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}
			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
			this.setRowHeight(20);
		}

		public AddFileTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("fileNum"));
			title.add(JecnProperties.getValue("fileName"));
			title.add(JecnProperties.getValue("fileSrc"));
			if (JecnConfigTool.isShowFileTypeInTable()) {
				title.add(JecnProperties.getValue("fileType"));
			}
			return new AddFileTableMode(null, title);
		}

		@Override
		public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
			super.changeSelection(rowIndex, columnIndex, toggle, extend);
			super.editCellAt(rowIndex, columnIndex, null);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {
			return new int[] {};
		}

	}

	/**
	 * 添加渲染列
	 * 
	 * @param fileTable
	 *@date 2017-8-23
	 *@author ZXH
	 */

	private FileTypeCombox typeCombox = null;

	protected void addCellEditor(JTable fileTable) {
		if (!JecnConfigTool.isShowFileTypeInTable()) {
			return;
		}
		TableColumnModel tcm = fileTable.getColumnModel();
		typeCombox = new FileTypeCombox(checkBoxs);
		tcm.getColumn(3).setCellEditor(new DefaultCellEditor(typeCombox));
		TableColumn tableColumn = tcm.getColumn(3);
		tableColumn.setMinWidth(80);
		tableColumn.setMaxWidth(80);
	}

	class FileTypeCombox extends JComboBox {
		public FileTypeCombox(Vector values) {
			super(values);
			this.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (addFileTable.getEditingRow() == -1) {
						return;
					}

					if (e.getStateChange() == ItemEvent.SELECTED) {
						addFileTypeTotal(FileTypeCombox.this.getSelectedItem());
					}
				}
			});
		}
	}

	/**
	 * 根据选中的文件类型索引获取编号
	 * 
	 * @param selectIndex
	 */
	private void addFileTypeTotal(Object obj) {
		if (obj == null) {
			return;
		}
		CheckBoxPo boxPo = (CheckBoxPo) obj;
		if (boxPo.value2 == null) {
			return;
		}
		JecnDictionary dictionary = (JecnDictionary) boxPo.value2;
		String fileType = dictionary.getParameter();
		if (StringUtils.isBlank(fileType)) {
			return;
		}
		String srcCode = addFileTable.getModel().getValueAt(addFileTable.getSelectedRow(), 0).toString();
		if (StringUtils.isBlank(srcCode)) {
			return;
		}
		// 根据文件类型重新设置编号
		reSetCodeByFileType(srcCode, fileType);
	}

	private void reSetCodeByFileType(String srcCode, String fileType) {
		String loginValue = JecnConfigTool.getConfigItemValue(ConfigItemPartMapMark.otherLoginType);
		if ("23".equals(loginValue)) {// 时代电气
			String endCode = srcCode.substring(srcCode.lastIndexOf(".") + 1, srcCode.length());
			endCode = endCode.substring(1, endCode.length());
			addFileTable.getModel().setValueAt(
					srcCode.substring(0, srcCode.lastIndexOf(".")) + "." + fileType + endCode,
					addFileTable.getSelectedRow(), 0);
		} else if ("39".equals(loginValue)) {// 时代新材
			// srcCode = TM 0012-03
			String[] arrs = srcCode.split(" ");
			if (arrs.length != 2) {
				return;
			}
			String endCode = arrs[1];
			addFileTable.getModel().setValueAt(fileType + " " + endCode, addFileTable.getSelectedRow(), 0);
		}
	}

	class AddFileTableMode extends DefaultTableModel {
		public AddFileTableMode(Vector<Vector<Object>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			if (colindex == 0 || colindex == 3) {
				return true;
			}
			return false;
		}

	}

	/**
	 * 动态获取文件类型
	 * 
	 * @return
	 */
	public Vector<CheckBoxPo> getListCheckBoxPo() {
		Vector<CheckBoxPo> model = new Vector<CheckBoxPo>();
		try {
			List<JecnDictionary> list = ConnectionPool.getFileAction().getFileTypeData();
			for (JecnDictionary dictionary : list) {
				model.add(new CheckBoxPo(dictionary.getCode().toString(), dictionary.getValue(), dictionary));
			}

		} catch (Exception e) {
			log.error("文件类型初始化出错！", e);
		}
		return model;
	}

	class UploadTask extends SwingWorker<Void, Void> {

		@Override
		protected Void doInBackground() throws Exception {
			try {
				upload();
			} catch (Exception e) {
				log.error("UploadTask is error", e);
				done();
				JecnOptionPane.showMessageDialog(AddFileDialog.this, JecnProperties.getValue("serverConnException"));
				// exitButPerformed();
			}
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}
}