package epros.draw.gui.swing.popup.popup;

import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 弹出菜单内容面板:改变弹出菜单大小的面板
 * 
 * 拖动弹出菜单边框改变大小
 * 
 * 根据鼠标进入弹出菜单的方向指定对应的鼠标类型，根据鼠标类型改变弹出菜单大小或位置点
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPopupResizeContentPanel extends JPanel implements
		MouseMotionListener, MouseListener {

	/** 边长度 */
	private final int panelHorn = 6;

	/** 弹出菜单 */
	private JecnPopupMenu popupMenu = null;
	/** 按下鼠标坐标 */
	private Point pressPoint;
	/** 拖动标识 true：拖动 false：未拖动 */
	protected boolean isDragged = false;

	public JecnPopupResizeContentPanel(JecnPopupMenu popupMenu) {
		if (popupMenu == null) {
			JecnLogConstants.LOG_JecnPopupMenu
					.error("JecnPopupResizeContentPanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.popupMenu = popupMenu;

		initComponent();
	}

	private void initComponent() {
		// 边框
		this.setBorder(null);
		// 布局管理器
		this.setLayout(new GridBagLayout());
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}

	/**
	 * 
	 * 添加主面板
	 * 
	 * @param mainPanel
	 *            JPanel主面板
	 */
	void addMainPanl(JPanel mainPanel) {
		if (mainPanel != null) {
			GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
					GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
					new Insets(3, 3, 3, 3), 0, 0);
			this.add(mainPanel, c);
		}
	}

	public void mouseMoved(MouseEvent e) {
		if (popupMenu == null) {
			return;
		}
		if (!popupMenu.isResizable()) {// 弹出菜单最大化或不支持改变弹出菜单大小
			return;
		}

		// 根据鼠标进入区域设置对应的鼠标类型
		if (e.getSource() instanceof JecnPopupResizeContentPanel) {
			JecnPopupResizeContentPanel resizePanel = (JecnPopupResizeContentPanel) e
					.getSource();
			// 获取鼠标进入面板的鼠标类型：上、下、左、右、左上、左下、右上、右下
			Cursor resizeCursor = getResizeCursor(e.getX(), e.getY());
			if (resizePanel.getCursor() != resizeCursor) {
				// 设置鼠标类型
				resizePanel.setCursor(resizeCursor);
			}
		}
	}

	public void mouseDragged(MouseEvent e) {
		// 拖动标识
		isDragged = true;

		if (!popupMenu.isResizable()) {// 弹出菜单最大化或不支持改变弹出菜单大小
			return;
		}

		if (e.getSource() instanceof JecnPopupResizeContentPanel) {
			JecnPopupResizeContentPanel resizePanel = (JecnPopupResizeContentPanel) e
					.getSource();

			// 改变弹出菜单的大小和位置点
			resizeJecnPopupMenu(e.getX(), e.getY(), resizePanel);
		}

	}

	public void mouseExited(MouseEvent e) {

		if (!isDragged) {// 不在拖动动作下离开才改变鼠标为默认类型
			if (e.getSource() instanceof JecnPopupResizeContentPanel) {
				JecnPopupResizeContentPanel resizePanel = (JecnPopupResizeContentPanel) e
						.getSource();
				resizePanel.setCursor(Cursor.getDefaultCursor());
			}
			// 出改变大小效果
//			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		}
	}

	public void mousePressed(MouseEvent e) {
		pressPoint = e.getPoint();

	}

	public void mouseClicked(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {
		// 出改变大小效果
//		this.setBackground(JecnUIUtil.getDialogBorderColor());
	}

	public void mouseReleased(MouseEvent e) {
		// 拖动标识
		isDragged = false;
	}

	private void resizeJecnPopupMenu(int x, int y,
			JecnPopupResizeContentPanel resizePanel) {
		if (pressPoint == null) {
			return;
		}

		double minWidth = 100;
		int minHeight = 25;

		Point point = popupMenu.getLocationOnScreen();

		// 新位置点
		int newX = 0;
		int newY = 0;

		if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.N_RESIZE_CURSOR)) {// 北面进入拖动
			// 偏移量
			int offsetY = y - pressPoint.y;
			int nH = popupMenu.getHeight() - offsetY;
			if (nH <= minHeight) {
				return;
			}

			// 改变大小
			newX = point.x;
			newY = point.y + offsetY;
			// popupMenu.setLocation(newX, newY);
			// popupMenu.setPopupSize(popupMenu.getWidth(), nH);
			this.setPopupMenuBounds(newX, newY, popupMenu.getWidth(), nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.E_RESIZE_CURSOR)) {// 东面进入拖动
			int offsetX = x - pressPoint.x;
			int nW = popupMenu.getWidth() + offsetX;
			if (nW < minWidth) {
				return;
			}
			pressPoint.x += offsetX;
			// 改变大小
			// popupMenu.setPopupSize(nW, popupMenu.getHeight());
			this.setPopupMenuSize(nW, popupMenu.getHeight());
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.S_RESIZE_CURSOR)) {// 南面进入拖动
			int offset = y - pressPoint.y;
			int nH = popupMenu.getHeight() + offset;
			if (nH <= minHeight) {
				return;
			}
			pressPoint.y += offset;

			// popupMenu.setPopupSize(popupMenu.getWidth(), nH);
			this.setPopupMenuSize(popupMenu.getWidth(), nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.W_RESIZE_CURSOR)) {// 西面进入拖动
			int offset = x - pressPoint.x;
			int nW = popupMenu.getWidth() - offset;
			if (nW < minWidth) {
				return;
			}

			newX = point.x + offset;
			newY = point.y;
			// popupMenu.setLocation(newX, newY);
			// popupMenu.setPopupSize(nW, popupMenu.getHeight());
			this.setPopupMenuBounds(newX, newY, nW, popupMenu.getHeight());
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR)) {// 东北面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;
			int nW = popupMenu.getWidth() + xoffset;
			int nH = popupMenu.getHeight() - yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}
			pressPoint.x += xoffset;

			newX = point.x;
			newY = point.y + yoffset;
			// popupMenu.setLocation(newX, newY);
			// popupMenu.setPopupSize(nW, nH);
			this.setPopupMenuBounds(newX, newY, nW, nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR)) {// 东南面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;

			int nW = popupMenu.getWidth() + xoffset;
			int nH = popupMenu.getHeight() + yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}

			pressPoint.x += xoffset;
			pressPoint.y += yoffset;

			// newX = point.x;
			// newY = point.y;
			// popupMenu.setLocation(newX, newY);
			// popupMenu.setPopupSize(nW, nH);
			this.setPopupMenuSize(nW, nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR)) {// 西南面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;

			int nW = popupMenu.getWidth() - xoffset;
			int nH = popupMenu.getHeight() + yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}

			pressPoint.y += yoffset;

			newX = point.x + xoffset;
			newY = point.y;
			// popupMenu.setLocation(newX, newY);
			// popupMenu.setPopupSize(nW, nH);
			this.setPopupMenuBounds(newX, newY, nW, nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR)) {// 西北面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;
			int nW = popupMenu.getWidth() - xoffset;
			int nH = popupMenu.getHeight() - yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}

			newX = point.x + xoffset;
			newY = point.y + yoffset;
			// popupMenu.setLocation(newX, newY);
			// popupMenu.setPopupSize(nW, nH);
			this.setPopupMenuBounds(newX, newY, nW, nH);
		}
	}

	/**
	 * 
	 * 获取鼠标进入面板的鼠标类型：上、下、左、右、左上、左下、右上、右下
	 * 
	 * 
	 * @param x
	 *            鼠标点击X坐标
	 * @param y
	 *            鼠标点击Y坐标
	 * @return Cursor 返回鼠标类型
	 */
	private Cursor getResizeCursor(int x, int y) {
		// 左边进入
		boolean left = isInLeft(x);
		// 右边进入
		boolean right = isInRight(x);
		// 上边进入
		boolean top = isInTop(y);
		// 下边进入
		boolean bottom = isInBottom(y);
		if (left) {
			if (top) {// 左上角
				return Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR);
			} else if (bottom) {// 左下角
				return Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
			} else {// 左边
				return Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR);
			}
		} else if (right) {
			if (top) {// 右上
				return Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR);
			} else if (bottom) {// 右下
				return Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR);
			} else {// 右边
				return Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
			}
		} else if (top) {// 上边
			return Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);
		} else if (bottom) {// 下边
			return Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR);
		} else {
			return Cursor.getDefaultCursor();
		}

	}

	/**
	 * 
	 * 左边进入
	 * 
	 * @param x
	 *            int 鼠标点击X坐标
	 * @return boolean true：左边进入 false：不是左边进入
	 */
	private boolean isInLeft(int x) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (x > rect.getX()-panelHorn && x < rect.getX() + panelHorn) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 右边进入
	 * 
	 * @param x
	 *            int 鼠标点击X坐标
	 * @return boolean true：右边进入 false：不是右边进入
	 */
	private boolean isInRight(int x) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (x > rect.getX() + rect.getWidth() - panelHorn
				&& x < rect.getX() + rect.getWidth()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 上边进入
	 * 
	 * @param y
	 *            int 鼠标点击Y坐标
	 * @return boolean true：上边进入 false：不是上边进入
	 */
	private boolean isInTop(int y) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (y > rect.getY()-panelHorn && y < rect.getY() + panelHorn) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 下边进入
	 * 
	 * @param y
	 *            int 鼠标点击Y坐标
	 * @return boolean true：下边进入 false：不是下边进入
	 */
	private boolean isInBottom(int y) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (y > rect.getY() + rect.getHeight() - panelHorn
				&& y < rect.getY() + rect.getHeight()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 
	 * 
	 * @param x
	 *            弹出菜单X坐标（基于屏幕）
	 * @param y
	 *            弹出菜单Y坐标（基于屏幕）
	 * @param width
	 *            弹出菜单宽
	 * @param height
	 *            弹出菜单高
	 */
	private void setPopupMenuBounds(int x, int y, int width, int height) {
		if (width >= popupMenu.getMinWidth()
				|| height >= popupMenu.getMinHeight()) {
			
			popupMenu.setPopupSize(width, height);
			
			// 弹出菜单位置点以及大小，其中位置点是基于屏幕而言
			if (x >= 0 && y >= 0) {
				popupMenu.setLocation(x, y);
			}
		} 
	}

	/**
	 * 
	 * 大小
	 * 
	 * @param width
	 *            弹出菜单宽
	 * @param height
	 *            弹出菜单高
	 */
	private void setPopupMenuSize(int width, int height) {
		if (width >= popupMenu.getMinWidth()
				|| height >= popupMenu.getMinHeight()) {
			popupMenu.setPopupSize(width, height);
		}
	}
}
