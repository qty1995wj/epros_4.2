package com.jecn.epros.service.rule;

import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.process.processFile.OperationFile;
import com.jecn.epros.domain.rule.RuleAttribute;
import com.jecn.epros.domain.rule.RuleRelatedFileBean;
import com.jecn.epros.domain.rule.RuleT;
import com.jecn.epros.domain.rule.SearchRuleResultBean;

import java.util.List;
import java.util.Map;

public interface RuleService {
    /**
     * 树节点展开
     *
     * @param map
     * @return
     */
    public List<TreeNode> findChildRules(Map<String, Object> map);

    /**
     * 概括信息
     *
     * @param map
     * @return
     */
    public List<RuleAttribute> findBaseInfoBean(Map<String, Object> map);

    /**
     * 相关文件
     *
     * @param map
     * @return
     */
    public RuleRelatedFileBean findReleateFiles(Map<String, Object> map);

    /**
     * 制度搜索
     *
     * @param map
     * @param searchBean
     * @return
     */
    public List<SearchRuleResultBean> searchRule(Map<String, Object> map);

    /**
     * 制度搜索 总数
     *
     * @param map
     * @return
     * @throws Exception
     */
    public int searchRuleTotal(Map<String, Object> map);

    public Inventory getInventory(Map<String, Object> map);

    /**
     * 制度操作说明
     *
     * @param map
     * @return
     * @throws Exception
     */
    OperationFile findRuleOperationFile(Map<String, Object> map);

    RuleT getRuleInfo(Long ruleId, boolean isPub);
}
