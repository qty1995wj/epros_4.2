package com.jecn.epros.server.bean.task;

import java.sql.Blob;

/**
 * 试运行文件
 * 
 * @author Administrator
 * 
 */
public class JecnTaskTestRunFile implements java.io.Serializable {
	private Long id;
	/** 任务主键Id */
	private Long taskId;
	/** 文件名称 */
	private String fileName;
	/** 文件内容 */
	private Blob fileStream;
	/** 文件bytes（不存在数据库中） */
	private byte[] bytes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Blob getFileStream() {
		return fileStream;
	}

	public void setFileStream(Blob fileStream) {
		this.fileStream = fileStream;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
}
