package com.jecn.epros.security;

import com.jecn.epros.sqlprovider.server3.Contants;

/**
 * 系统常量类以及系统启动时需要初始化数据类
 *
 * @author Administrator
 */
public class JecnContants {

    // //////////////权限////////////////
    /**
     * 浏览权限配置 流程地图 true是公开，false是密码
     */
    public static boolean isMainFlowAdmin = false;
    /**
     * 浏览权限配置 流程 true是公开，false是密码
     */
    public static boolean isFlowAdmin = false;
    /**
     * 浏览权限配置 组织 true是公开，false是密码
     */
    public static boolean isOrgAdmin = false;
    /**
     * 浏览权限配置 岗位 true是公开，false是密码
     */
    public static boolean isStationAdmin = false;
    /**
     * 浏览权限配置 标准 true是公开，false是密码
     */
    public static boolean isCriterionAdmin = false;
    /**
     * 浏览权限配置 制度 true是公开，false是密码
     */
    public static boolean isSystemAdmin = false;
    /**
     * 浏览权限配置 文件 true是公开，false是密码
     */
    public static boolean isFileAdmin = false;
    // //////////////权限////////////////
    /**
     * 流程 相关文件是否公开
     */
    public static boolean isOpenFlowRelatedFile = false;
    public static boolean isOpenFlowRelatedStandard = false;
    public static boolean isOpenFlowRelatedRule = false;

    /**
     * 架构相关文件是否公开
     */
    public static boolean isOpenArchitectureRelatedFile;
    public static boolean isOpenArchitectureRelatedRule;

    /**
     * 制度相关文件是否公开
     */
    public static boolean isOpenRuleRelatedFile;
    public static boolean isOpenRuleRelatedStandard;

    public static boolean isFlowAdmin() {
        // 是不是搜索所有流程节点
        return Contants.isFlowAdmin || isAdmin() ? true : false;
    }

    public static boolean isMainFlowAdmin() {
        // 是不是搜索所有流程节点
        return Contants.isMainFlowAdmin || isAdmin() ? true : false;
    }

    public static boolean isSystemAdmin() {
        // 是不是搜索所有流程节点
        return Contants.isSystemAdmin || isAdmin() ? true : false;
    }

    public static boolean isFileAdmin() {
        // 是不是搜索所有流程节点
        return Contants.isFileAdmin || isAdmin() ? true : false;
    }

    public static boolean isCriterionAdmin() {
        // 是不是搜索所有流程节点
        return Contants.isCriterionAdmin || isAdmin() ? true : false;
    }

    public static boolean isAdmin() {
        return AuthenticatedUserUtil.isAdmin();
    }
}
