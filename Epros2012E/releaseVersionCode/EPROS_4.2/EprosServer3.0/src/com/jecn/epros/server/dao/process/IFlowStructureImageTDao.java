package com.jecn.epros.server.dao.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnMapRelatedStructureImageT;
import com.jecn.epros.server.common.IBaseDao;

public interface IFlowStructureImageTDao extends IBaseDao<JecnFlowStructureImageT, Long> {
	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：通过流程ID获得所有图形元素
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowStructureImageT> findAllByFlowId(Long flowId) throws Exception;

	public void updateLinkedImageName(Long flowId, String showName) throws Exception;

	List<JecnMapRelatedStructureImageT> findMapRelatedStructureImageTById(Long flowId) throws Exception;
}
