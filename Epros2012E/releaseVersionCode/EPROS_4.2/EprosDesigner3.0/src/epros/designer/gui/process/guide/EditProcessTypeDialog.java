package epros.designer.gui.process.guide;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 编辑流程类别
 * @author 2012-07-19
 *
 */
public class EditProcessTypeDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(EditProcessTypeDialog.class);
	private ProceeRuleTypeBean ruleTypeBean = null;
	protected boolean isOperation = false;
	
	public boolean isOperation() {
		return isOperation;
	}

	public void setOperation(boolean isOperation) {
		this.isOperation = isOperation;
	}

	private Long typeId;
	public EditProcessTypeDialog(Long typeId,ProceeRuleTypeBean ruleTypeBean){
		this.typeId = typeId;
		this.ruleTypeBean = ruleTypeBean;
		this.setLocationRelativeTo(null);
		this.setName(ruleTypeBean.getTypeName());
		this.seteName(ruleTypeBean.getEnName());
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("editBtn");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		ruleTypeBean.setTypeName(this.getName());
		ruleTypeBean.setEnName(this.geteName());
		try {
			ConnectionPool.getProcessRuleTypeAction().updateFlowType(ruleTypeBean);
			isOperation = true;
			this.dispose();
		} catch (Exception e) {
			log.error("EditProcessTypeDialog saveData is error",e);
		}
		
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		List<ProceeRuleTypeBean> listProcessRuleTypeBean= new ArrayList<ProceeRuleTypeBean>();
		try {
			listProcessRuleTypeBean = ConnectionPool.getProcessRuleTypeAction().getFlowAttributeType();
			for(ProceeRuleTypeBean proruleTypeBean : listProcessRuleTypeBean){
				if(proruleTypeBean.getTypeName().equals(name)){
					if(proruleTypeBean.getTypeId().equals(typeId)){
						return false;
					}else{
						return true;
					}
				}
			}
		} catch (Exception e) {
			log.error("EditProcessTypeDialog validateNodeRepeat is error",e);
		}
		return false;
	}

}
