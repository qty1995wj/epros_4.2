package com.jecn.epros.server.dao.process;

import java.util.List;

import com.jecn.epros.server.bean.process.EprosBPMVersion;
import com.jecn.epros.server.common.IBaseDao;

public interface IEprosBPMVersionDao extends IBaseDao<String, String> {
	public List<EprosBPMVersion> findEprosBpmVersions(String flowId) throws Exception;
}
