package com.jecn.epros.server.service.system.impl;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.system.JecnElementsLibrary;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.service.system.IJecnConfigItemService;
import com.jecn.epros.server.service.system.JecnCofigItemSyscDB;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 配置信息表（JECN_CONFIG_ITEM）操作类
 *
 * @author ZHOUXY
 */
public class JecnConfigItemServiceImpl implements IJecnConfigItemService {

    private final Log log = LogFactory.getLog(JecnConfigItemServiceImpl.class);

    /**
     * Dao层对象
     */
    private IJecnConfigItemDao configDao = null;
    /**
     * 启动时设为true，用于暗码验证
     */
    private static boolean startType = true;

    private static boolean isAdminKey;

    /**
     * 查询流程图设置值
     * <p>
     * //	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
     *
     * @return List<JecnConfigItemBean> 流程图设置值
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> select(int typeBigModel) {
        if (startType) {// 启动时加载
            new Thread(new KeyWordThread(true)).start();
            startType = false;
        }
        return selectByTypeBigModel(typeBigModel);
    }

    /**
     * 反射停服务
     *
     * @param name
     * @param strMeth
     * @return
     * @throws Exception
     */
    public static void getStaticProperty(String name, String strMeth) throws Exception {
        // 获取构造函数(私有需用此方法)
        Constructor[] cts = Class.forName(name).getDeclaredConstructors();
        for (int i = 0; i < cts.length; i++) {
            cts[i].setAccessible(true);
            // cts[i].newInstance(null);
            Object obj = cts[i].newInstance();
            Method method = obj.getClass().getMethod(strMeth, int.class);
            method.invoke(obj, 0);
        }
    }

    /**
     * 反射获取类中指定方法
     *
     * @param name
     * @return
     * @throws Exception
     */
    private Object invokeMethod(String name, String strMethod) throws Exception {
        // 获取class
        Class<?> cl = Class.forName(name);
        // 创建对象
        Object obj = cl.newInstance();
        // 获取方法
        Method method = cl.getMethod(strMethod);
        return method.invoke(obj);//
    }

    /**
     * 类全名称获取 com.jecn.epros.server.util
     *
     * @return
     */
    private String getStrName() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("com.j");
        strBuff.append("ecn.e");
        strBuff.append("pro");
        strBuff.append("s.se");
        strBuff.append("rver.u");
        strBuff.append("til.");
        strBuff.append("Jec");
        strBuff.append("nKey");
        return strBuff.toString();
    }

    // com.jecn.epros.server.common
    private String getStrContantName() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("com.j");
        strBuff.append("ecn.e");
        strBuff.append("pro");
        strBuff.append("s.se");
        strBuff.append("rver.c");
        strBuff.append("ommon.J");
        strBuff.append("ecnCo");
        strBuff.append("ntants");
        return strBuff.toString();
    }

    private String getContantsStrKey() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("isAd");
        strBuff.append("minK");
        strBuff.append("ey");
        return strBuff.toString();
    }

    /**
     * 类方法名称获取
     *
     * @return
     */
    private String getStrKey() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("ge");
        strBuff.append("tVa");
        strBuff.append("lue");
        return strBuff.toString();
    }

    private String getStrIsTest() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("is");
        strBuff.append("Te");
        strBuff.append("st");
        return strBuff.toString();
    }

    private String getSecondCountStrKey() {
        // getSecondAdminUserInt
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("ge");
        strBuff.append("tSeco");
        strBuff.append("ndAdm");
        strBuff.append("inUse");
        strBuff.append("rInt");
        return strBuff.toString();
    }

    private String getDesignerUserInt() {
        // getSecondAdminUserInt
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("ge");
        strBuff.append("tDesi");
        strBuff.append("gnerU");
        strBuff.append("serI");
        strBuff.append("nt");
        return strBuff.toString();
    }

    /**
     * 试用，暗码验证
     *
     * @param text String 密钥
     * @return boolean 相等返回true；不相等返回false
     */
    private boolean DateDncrypt(String text) throws Exception {
        // 日期暗码验证
        boolean dateSame = testDateSame(text);
        // 试用版 服务端设计器用户数暗码验证
        boolean testDesigherSame = testServerValidate(text);
        if (dateSame && testDesigherSame) {// 全部验证通过，返回true
            return true;
        }
        return false;
    }

    /**
     * 试用， 日期暗码验证
     *
     * @param text
     * @return
     */
    private boolean testDateSame(String text) {
        // 201412110090111111ABADAABAAAABAAAAP2FFHJJBBBBBBHF
        if (text == null || "".equals(text)) {
            return false;
        }

        // 日期明码
        String datebefore = text.substring(0, 8);
        // 日期暗码 旧版：18, 34
        String dateAfter = null;

        if (text.length() == 49) {// 旧版密钥长度
            dateAfter = text.substring(18, 34);
        } else if (text.length() == 53) {// 二级管理员秘钥
            dateAfter = text.substring(22, 38);
        } else {// 62 新版秘钥
            dateAfter = text.substring(26, 42);
        }
        if (datebefore == null || "".equals(datebefore.trim()) || datebefore.length() * 2 != dateAfter.length()) { // 暗码是日期2倍
            return false;
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put("AA", "1");
        map.put("AB", "2");
        map.put("AC", "3");
        map.put("BA", "4");
        map.put("BB", "5");
        map.put("BC", "6");
        map.put("CA", "7");
        map.put("CB", "8");
        map.put("CC", "9");
        map.put("AD", "0");

        // 暗码对应日期
        StringBuffer buff = new StringBuffer();
        for (int i = 0; i < dateAfter.length(); i += 2) {
            String subStr = dateAfter.substring(i, i + 2);
            String value = map.get(subStr);
            buff.append(value);
        }

        boolean dateSame = false;
        if (datebefore.equals(buff.toString())) {
            dateSame = true;
        }
        return dateSame;
    }

    /**
     * MAC地址、用户数解密
     *
     * @param text String 密钥内容
     *             //	 * @param flaq
     *             String 版本标识
     * @return boolean true:验证成功；false：验证失败
     */
    private boolean McDncrypt(String text) throws Exception {
        // 9999F4-6D-04-44-44-4AAFBABCADADBABABABABABAAAD2DSFSDFSDLKJFSLD
        if (text == null || "".equals(text)) {
            return false;
        }
        // 服务器暗码验证
        // true:MAC比较相同
        boolean mcSame = macValidate(text);
        // 服务器用户数比较
        boolean serverSame = serverValidate(text);
        // 设计器用户数暗码验证
        boolean designerSame = designerValidate(text);

        if (mcSame && serverSame && designerSame) {// 全部验证通过返回true
            return true;
        }
        // ***************用户数部分 end***************//
        return false;
    }

    /**
     * 正式 服务端用户暗码验证
     *
     * @param text
     * @return
     */
    private boolean serverValidate(String text) {
        int length = text.length();
        // 设计器用户数 用户数暗码部分 2-5设计器用户数，6-11 服务器用户数
        String serverNum = null;
        if (isAdminKey) {
            serverNum = text.substring(length - 13, length - 7);
        } else {
            serverNum = text.substring(length - 8, length - 2);
        }
        // 服务用户数明码部分
        String userNum = text.substring(4, 10);
        // 设计器用户数比较
        return usrValidate(text, serverNum, userNum);
    }

    /**
     * 测试服务端用户数暗码验证
     *
     * @param text
     * @return true 验证通过
     */
    private boolean testServerValidate(String text) {
        int length = text.length();
        // 设计器用户数 用户数暗码部分 2-5设计器用户数，6-11 服务器用户数
        String serverNum = null;
        if (isAdminKey) {
            serverNum = text.substring(length - 13, length - 7);
        } else {
            serverNum = text.substring(length - 8, length - 2);
        }
        // 试用 服务用户数明码部分
        String userNum = text.substring(12, 18);
        // 设计器用户数比较
        return usrValidate(text, serverNum, userNum);
    }

    /**
     * 服务端 设计器用户数暗码验证
     *
     * @param text
     * @return
     */
    private boolean designerValidate(String text) {
        int length = text.length();
        // 设计器用户数 用户数暗码部分 2-5设计器用户数，6-11 服务器用户数
        String designNum = null;
        if (isAdminKey) {
            designNum = text.substring(length - 17, length - 13);
        } else {
            designNum = text.substring(length - 12, length - 8);
        }
        // 用户数明码部分
        String userNum = text.substring(0, 4);
        // 设计器用户数比较
        return usrValidate(text, designNum, userNum);
    }

    /**
     * @param text     密钥内容
     * @param darkNum  暗码
     * @param plainNum 明码
     * @return boolean true:验证通过
     */
    private boolean usrValidate(String text, String darkNum, String plainNum) {
        Map<String, String> map = new HashMap<String, String>();
        // ***************MAC地址部分 end***************//

        // ***************用户数部分 start***************//
        StringBuffer buff = new StringBuffer();

        map = new HashMap<String, String>();
        map.put("A", "0");
        map.put("B", "1");
        map.put("C", "2");
        map.put("D", "3");
        map.put("E", "4");
        map.put("F", "5");
        map.put("G", "6");
        map.put("H", "7");
        map.put("I", "8");
        map.put("J", "9");

        char[] c = darkNum.toCharArray();
        for (int i = 0; i < c.length; i++) {
            String tmp = map.get(String.valueOf(c[i]));
            buff.append(tmp);
        }
        // 获取暗码用户数
        String numStr = buff.toString();
        // 判断用户数是否相同
        if (plainNum.equals(numStr)) {// 用户数相同、MAC也相同
            return true;
        }
        return false;
    }

    private boolean macValidate(String text) {

        // 获取用户数、MAC地址、MAC地址暗码
        int length = text.length();
        String mcAll = null;

        if (isAdminKey) {
            mcAll = text.substring(0, length - 20);
        } else {
            mcAll = text.substring(0, length - 15);
        }

        // Mac地址明码部分 旧版本：10, 27，新版 14, 31
        String mcBefore = null;
        // MAC地址暗码部分
        String mcAfter = null;
        if (isAdminKey) {
            mcBefore = mcAll.substring(14, 31);
            mcAfter = mcAll.substring(31);
        } else if (length == 66) {// 旧版秘钥长度
            mcBefore = mcAll.substring(10, 27);
            mcAfter = mcAll.substring(27);
        } else {
            mcBefore = mcAll.substring(14, 31);
            mcAfter = mcAll.substring(31);
        }
        // ***************MAC地址部分 start***************//
        Map<String, String> map = new HashMap<String, String>();
        map.put("AA", "1");
        map.put("AB", "2");
        map.put("AC", "3");
        map.put("BA", "4");
        map.put("BB", "5");
        map.put("BC", "6");
        map.put("CA", "7");
        map.put("CB", "8");
        map.put("CC", "9");
        map.put("AD", "0");

        // Mac地址明码去掉"-"字符
        String strNew = mcBefore.replaceAll("-", "");
        if (strNew == null || "".equals(strNew.trim()) || strNew.length() * 2 != mcAfter.length()) {
            return false;
        }

        StringBuffer buff = new StringBuffer();
        int count = 0;
        // 加密后mac字符串循环
        for (int i = 0; i < strNew.length(); i++) {
            count++;
            String tmp2 = strNew.substring(i, i + 1);
            if (tmp2.matches("[a-zA-Z]")) {
                buff.append(mcAfter.substring(i * 2 + 1, i * 2 + 2));
            } else {
                String tmp = mcAfter.substring(i * 2, i * 2 + 2);
                String value = map.get(tmp);
                buff.append(value);
            }

            if (count == 2 && i < strNew.length() - 1) {
                buff.append("-");
                count = 0;
            }
        }
        if (mcBefore.equals(buff.toString())) {// 比较是否相等
            return true;
        }

        return false;
    }

    /**
     * 查询流程文件排序后显示数据
     *
     * @return List<JecnConfigItemBean> 流程文件排序后显示数据
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectShowFlowFile() {
        return configDao.selectShowFlowFile();
    }

    /**
     * 查询流程建设规范显示数据
     *
     * @return List<JecnConfigItemBean> 流程建设规范显示数据
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectShowPartMapStand() {
        return configDao.selectShowPartMapStand();
    }

    /**
     * 查询任务审批环节数据（带文控类型）
     * <p>
     * //	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
     *
     * @return List<JecnConfigItemBean> 任务审批环节数据（带文控类型）
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectShowTaskApp(int typeBigModel) {
        return configDao.selectShowTaskApp(typeBigModel);
    }

    /**
     * 查询任务审批环节数据（除去文控类型）
     * <p>
     * //	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
     *
     * @return List<JecnConfigItemBean> 任务审批环节显示数据
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectTaskAppTable(int typeBigModel) {
        return configDao.selectTaskAppTable(typeBigModel);
    }

    /**
     * 查询所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
     * <p>
     * //	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
     *
     * @return List<JecnConfigItemBean> 所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectTaskShowAllItems(int typeBigModel) {
        return configDao.selectTaskShowAllItems(typeBigModel);
    }

    /**
     * 查询所有表示公开秘密数据集合
     *
     * @return List<JecnConfigItemBean> 所有表示公开秘密数据集合
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectAllPub() {
        return configDao.selectAllPub();
    }

    /**
     * 修改JECN_CONFIG_ITEM表
     *
     * @param configItemList List<JecnConfigItemBean>
     *                       //	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
     * @return boolean true:修改成功；false：修改失败
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean update(List<JecnConfigItemBean> configItemList, int typeBigModel) {

        if (configItemList == null || configItemList.size() == 0) {
            return true;
        }

        // 获取数据库中最新数据
        List<JecnConfigItemBean> dbItemList = selectByTypeBigModel(typeBigModel);

        // 需要更新数据
        List<JecnConfigItemBean> updateItemList = new ArrayList<JecnConfigItemBean>();

        for (JecnConfigItemBean configBean : configItemList) {
            // 名称
            String newName = configBean.getName();
            // 排序
            Integer newSort = configBean.getSort();
            // 必填项
            Integer newIsEmpty = configBean.getIsEmpty();
            // 值
            String newValue = configBean.getValue();

            String newEnName = configBean.getEnName();
            String v1 = configBean.getV1();
            String v2 = configBean.getV2();
            log.info("v1" + v1);
            log.info("v2" + v2);
            // 数据验证
            if (newName == null || "".equals(newName.trim())) {
                log.error(" newName is error mark=" + configBean.getMark());
                return false;
            }
            if (newSort != null && newSort.intValue() < 1) {
                log.error("  newSort is error mark=" + configBean.getMark());
                return false;
            }
            if (newIsEmpty != null && newIsEmpty.intValue() != 0 && newIsEmpty.intValue() != 1) {
                log.error("  newIsEmpty is error mark=" + configBean.getMark());
                return false;
            }

            // 过滤没有改变数据
            for (JecnConfigItemBean dbBean : dbItemList) {
                if (configBean.getId() == dbBean.getId()) {// 主键相同
                    if (newName.equals(dbBean.getName()) && intSame(newSort, dbBean.getSort())
                            && intSame(newIsEmpty, dbBean.getIsEmpty()) && strSame(newValue, dbBean.getValue())
                            && strSame(newEnName, dbBean.getEnName()) && strSame(v1, dbBean.getV1())
                            && strSame(v2, dbBean.getV2())) {// name、sort、isEmpty、value都相同时不更新
                    } else {
                        log.info(configBean.info());
                        updateItemList.add(configBean);
                    }
                    dbItemList.remove(dbBean);
                    break;
                }
            }
        }

        if (updateItemList.size() == 0) {
            return true;
        }

        // 执行更新
        boolean ret = configDao.update(updateItemList);

        if (ret) {// 更新缓存
            for (JecnConfigItemBean itemBean : updateItemList) {
                JecnCofigItemSyscDB.updateConfigItemData(itemBean);

                // 更新缓存
                JecnContants.configMaps.put(itemBean.getMark(), itemBean);
            }
            flush4SystemConfig(updateItemList);
        }
        return ret;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(List<JecnConfigItemBean> updateItemList) {
        if (updateItemList.size() == 0) {
            return;
        }
        configDao.update(updateItemList);
        for (JecnConfigItemBean itemBean : updateItemList) {
            JecnCofigItemSyscDB.updateConfigItemData(itemBean);

            // 更新缓存
            JecnContants.configMaps.put(itemBean.getMark(), itemBean);
        }
        flush4SystemConfig(updateItemList);
    }

    /**
     * 刷新4.0系统中的配置项
     */
    private void flush4SystemConfig(List<JecnConfigItemBean> updateItemList) {
        List<Integer> idSet = new ArrayList<Integer>();
        for (JecnConfigItemBean config : updateItemList) {
            idSet.add(config.getId());
        }
        final String ids = "ids=" + getIds(idSet);
        new Thread(new Runnable() {
            @Override
            public void run() {
                java.net.HttpURLConnection httpConn = null;
                InputStream input = null;
                BufferedWriter writer = null;
                try {
                    String address = JecnConfigTool.getServer4SystemConfigAddress();
                    if (StringUtils.isBlank(address)) {
                        return;
                    }
                    // 创建URL对象
                    java.net.URL connURL = new java.net.URL(address);
                    // 打开URL连接
                    httpConn = (java.net.HttpURLConnection) connURL.openConnection();
                    // 设置通用属性
                    httpConn.setRequestProperty("Accept", "*/*");
                    httpConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
                    httpConn.setConnectTimeout(3000);
                    httpConn.setRequestMethod("POST");
                    httpConn.setRequestProperty("ContentType", " text/plain;charset=utf-8");
                    httpConn.setDoOutput(true);
                    httpConn.setRequestProperty("accept", "*/*");
                    // 建立实际的连接
                    httpConn.connect();
                    writer = new BufferedWriter(new OutputStreamWriter(httpConn.getOutputStream()));
                    writer.write(ids);
                    writer.flush();
                    input = httpConn.getInputStream();
                } catch (SocketTimeoutException e) {// 读取超时
                    // ignore
                    log.error("向4.0发送刷新配置请求异常，超时");
                } catch (Exception e) {
                    log.error("向4.0发送刷新配置请求异常", e);
                } finally {
                    if (input != null) {
                        try {
                            input.close();
                        } catch (IOException e) {
                            log.error("", e);
                        }
                    }
                    if (writer != null) {
                        try {
                            writer.close();
                        } catch (IOException e) {
                            log.error("", e);
                        }
                    }
                    if (httpConn != null) {
                        httpConn.disconnect();
                    }
                }
            }
        }).start();
    }

    private String getIds(List<Integer> list) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                b.append(list.get(i));
            } else {
                b.append("," + list.get(i));
            }
        }
        return b.toString();
    }

    /**
     * 通过大类下唯一标示获取value字段值
     * <p>
     * //	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
     * //	 * @param String
     * mark 大类下唯一标识
     *
     * @return String value字段值或NULL
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public String selectValue(int typeBigModel, String mark) {
        if ((mark == null || "".equals(mark.trim()))) {
            return null;
        }
        return configDao.selectValue(typeBigModel, mark);
    }

    /**
     * 通过大类下唯一标示获取数据
     * <p>
     * //	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
     * //	 * @param String
     * mark 大类下唯一标识
     *
     * @return String 一行数据字段值或NULL
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public JecnConfigItemBean selectConfigItemBean(int typeBigModel, String mark) {
        if ((mark == null || "".equals(mark.trim()))) {
            return null;
        }
        return configDao.selectConfigItemBean(typeBigModel, mark);
    }

    public IJecnConfigItemDao getConfigDao() {
        return configDao;
    }

    public void setConfigDao(IJecnConfigItemDao configDao) {
        this.configDao = configDao;
    }

    private boolean intSame(Integer newSort, Integer dbSort) {
        if (newSort == null && dbSort == null) {
            return true;
        } else if (newSort != null && dbSort != null && newSort.intValue() == dbSort.intValue()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean strSame(String newStr, String dbStr) {
        if (isNull(newStr) && isNull(dbStr)) {
            return true;
        } else if (!isNull(newStr) && !isNull(dbStr) && newStr.equals(dbStr)) {
            return true;
        } else {
            return false;
        }

    }

    private boolean isNull(String str) {
        return (str == null || "".equals(str)) ? true : false;
    }

    /**
     * 查询流程图设置值
     * <p>
     * //	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
     *
     * @return List<JecnConfigItemBean> 流程图设置值
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectByTypeBigModel(int typeBigModel) {
        return configDao.select(typeBigModel);
    }

    /**
     * 查询流程基本信息
     *
     * @return List<JecnConfigItemBean>查询流程基本信息
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectBaseInfoFlowFile() {
        return configDao.selectBaseInfoFlowFile();
    }

    /**
     * 获取邮箱配置项
     *
     * @return List<JecnConfigItemBean>邮箱配置项集合
     * @throws Exception
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectEmailConfigItem() {
        return configDao.selectEmailConfigItem();
    }

    /**
     * 获取系统配置项信息
     *
     * @param typeBigModel   int 0：流程地图 ,1：流程图 ,2：制度, 3：文件, 4：邮箱配置, 5：权限配置
     * @param typeSmallModel int 0：审批环节配置, 1：任务界面显示项配置, 2：基本配置, 3：流程图建设规范 ,4：流程文件,
     *                       5：邮箱配置,6：权限配置
     * @return List<JecnConfigItemBean> 系统配置项信息
     * @throws Exception
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectConfigItemBeanByType(int typeBigModel, int typeSmallModel) {
        return configDao.selectConfigItemBeanByType(typeBigModel, typeSmallModel);
    }

    /**
     * 查询流程地图文件排序后显示数据
     *
     * @return List<JecnConfigItemBean> 流程文件排序后显示数据
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectFlowMapShowFlowFile() {
        return configDao.selectFlowMapShowFlowFile();
    }

    class KeyWordThread implements Runnable {
        private boolean isRun = false;
        ;

        private KeyWordThread(boolean isRun) {
            this.isRun = isRun;
        }

        @Override
        public synchronized void run() {
            // 获取类名
            String name = getStrName();
            // 方法名
            String strMeth = getStrKey();

            try {
                Thread.sleep(10000);
                // 获取值
                String value = invokeMethod(name, strMeth).toString();
                String isTest = invokeMethod(name, getStrIsTest()).toString();

                isAdminKey = getAdminKey();
                if ("true".equals(isTest)) {// 试用日期+设计器用户数+服务器用户数+二级管理员个数
                    if (!DateDncrypt(value)) {
                        // 执行退出
                        getStaticProperty("jav" + "a.la" + "ng.Sy" + "stem", "ex" + "it");
                        return;
                    }
                } else {// 正式
                    // MAC地址
                    if (!McDncrypt(value)) {// 验证失败
                        // 执行退出
                        getStaticProperty("jav" + "a.la" + "ng.Sy" + "stem", "ex" + "it");
                        return;
                    }
                }
                setUserCount(name, value);

                // // 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
                // IJecnRoleService roleService = (IJecnRoleService)
                // ApplicationContextUtil.getContext().getBean(
                // "JecnRoleServiceImpl");
                // roleService.saveUserRoleLog();
            } catch (Exception e) {
                // 执行退出
                try {
                    getStaticProperty("jav" + "a.la" + "ng.Sy" + "stem", "ex" + "it");
                } catch (Exception e1) {
                }
                return;
            }
        }
    }

    /**
     * 获取密钥版本
     *
     * @return
     * @throws Exception
     */
    private boolean getAdminKey() throws Exception {
        String isAdminKey = invokeMethod(getStrContantName(), getContantsStrKey()).toString();
        return "true".equals(isAdminKey);
    }

    private void setUserCount(String name, String value) throws NumberFormatException, Exception {
        // 获取二级管理员总数
        int adminCount = Integer.valueOf(invokeMethod(name, getSecondCountStrKey()).toString());
        JecnContants.userInt = Integer.valueOf(invokeMethod(name, getDesignerUserInt()).toString());
        if (isAdminKey) {
            JecnContants.adminInt = adminCount;
        } else {
            JecnContants.secondInt = adminCount;
        }
    }

    /**
     * 获取小类对应的配置信息
     *
     * @param typeSmallModel 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
     *                       11:发布配置
     * @return List<JecnConfigItemBean>
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectSmallTypeItem(int typeSmallModel) {
        return configDao.selectSmallTypeItem(typeSmallModel);
    }

    /**
     * 合理化建议
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> getRationalizationProposal() {
        return configDao.selectConfigItemBeanByType(5, 9);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectShowSortConfigItemBeanByType(int typeBigModel, int typeSmallModel) {
        return configDao.selectShowSortConfigItemBeanByType(typeBigModel, typeSmallModel);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectTableConfigItemBeanByType(int typeBigModel, int typeSmallModel) {
        return configDao.selectTableConfigItemBeanByType(typeBigModel, typeSmallModel);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectKpiConfigItemBeanByTop(int top) {
        return configDao.selectKpiConfigItemBeanByTop(top);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<JecnConfigItemBean> selectAllItem() throws Exception {
        return configDao.selectAllItem();
    }

    @Override
    @Transactional
    public Map<DictionaryEnum, List<JecnDictionary>> initDictionaryMap() {
        try {
            String sql = "SELECT * FROM jecn_dictionary WHERE VALIDABLE = 'y'  order by name,code";
            List<JecnDictionary> dics = configDao.getSession().createSQLQuery(sql).addEntity(JecnDictionary.class)
                    .list();
            Map<DictionaryEnum, List<JecnDictionary>> dicMap = new ConcurrentHashMap<DictionaryEnum, List<JecnDictionary>>();
            DictionaryEnum dicEnum = null;
            for (JecnDictionary dic : dics) {
                dicEnum = DictionaryEnum.valueOf(dic.getName());
                if (dicMap.get(dicEnum) == null) {
                    dicMap.put(dicEnum, new ArrayList<JecnDictionary>());
                }
                dicMap.get(dicEnum).add(dic);
            }
            return dicMap;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<Object[]> selectMaintainNode() {
        return configDao.selectMaintainNode();
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public JecnElementsLibrary getElement(String mark) {
        String hql = "from JecnElementsLibrary where mark=?";
        return configDao.getObjectHql(hql, mark);
    }
}
