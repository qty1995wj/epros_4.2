package epros.draw.util;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JComponent;

import epros.draw.constant.JecnWorkflowConstant;

/**
 * 
 * 图片处理类
 * 
 * @author Administrator
 *
 */
public class JecnImageUtil {
	/**
	 * 获取image缓冲区
	 * 
	 * @param jcomponent
	 *            容器
	 * @param width
	 *            图片宽度
	 * @param height
	 *            图片高度
	 * @return
	 */
	public static  BufferedImage saveWorkflowDrawImage(JComponent jcomponent,
			int width, int height, double doX, double doY) {
		if (getJVMHeap() < 512) {
			if (width * height > JecnWorkflowConstant.imageSize) {
				return null;
			}
		}
		// image缓冲区
		BufferedImage bufferedImage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics graphics = bufferedImage.createGraphics();
		// 2D 画笔
		Graphics2D g2d = (Graphics2D) graphics;

		// 使用此 Graphics2D 中的 Transform 组合 AffineTransform 对象
		g2d.transform(AffineTransform.getScaleInstance(doX, doY));

		// 关闭双缓冲区绘制
		ArrayList<JComponent> compList = new ArrayList<JComponent>();
		closeDoubleBuffered(jcomponent, compList);

		// 绘制图
		jcomponent.paint(g2d);

		// 开启双缓冲区绘制
		openDoubleBuffered(compList);

		g2d.dispose();
		return bufferedImage;
	}
	
	/**
	 * 
	 * 允许使用双缓冲区绘制
	 * 
	 * @param compList
	 *            ArrayList<JComponent>
	 */
	public static  void openDoubleBuffered(ArrayList<JComponent> compList) {
		for (JComponent component : compList) {
			component.setDoubleBuffered(true);
		}
	}
	
	/**
	 * 
	 * 禁止使用双缓冲区绘制
	 * 
	 * @param component
	 *            JComponent
	 * @param compList
	 *            ArrayList<JComponent>
	 */
	public static  void closeDoubleBuffered(JComponent component,
			ArrayList<JComponent> compList) {
		if (component.isDoubleBuffered()) {
			compList.add(component);
			component.setDoubleBuffered(false);
		}
		for (int i = 0; i < component.getComponentCount(); i++) {
			Component c = component.getComponent(i);
			if (c instanceof JComponent) {
				closeDoubleBuffered((JComponent) c, compList);
			}
		}
	}
	
	
	/**
	 * 返回java虚拟机可用的最大内存数
	 * 
	 * @author fuzhh Sep 4, 2012
	 * @return
	 */
	public static int getJVMHeap() {
		int mb = 1024 * 1024;
		Runtime runtime = Runtime.getRuntime();
		// 返回空闲的内存
		// System.out.println("Free Memory:" + runtime.freeMemory() / mb + "M");
		// Java 虚拟机中的内存总量
		// System.out.println("Total Memory:" + runtime.totalMemory() / mb +
		// "M");
		// java虚拟机的最大内存数,如果内存本身没有限制，则返回值 Long.MAX_VALUE。
		return (int) (runtime.maxMemory() / mb);
	}
}
