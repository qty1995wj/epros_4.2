package com.jecn.epros.domain.journal;

import java.util.Map;

public class EprosInterfaceUserMenuWebBean {
    /**
     * 人员 菜单id
     */
    private String code;
    /**
     * 登录名称
     */
    private String username;

    /**
     * 系统编码
     */
    private final String fromsys = "S075";

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFromsys() {
        return fromsys;
    }
}
