package com.jecn.epros.server.bean.process;

import java.io.Serializable;

/**
 * 指标表(设计器)
 * 
 * @author Administrator
 */
public class JecnRefIndicatorsT implements Serializable {

	private Long id;// 主键ID
	private String indicatorName;// 指标名
	private String indicatorValue;// 指标值
	private Long activityId;// 活动FigureID

	private String figureUUID;
	private String UUID;

	public JecnRefIndicatorsT() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	public String getIndicatorName() {
		return indicatorName;
	}

	public void setIndicatorName(String indicatorName) {
		this.indicatorName = indicatorName;
	}

	public String getIndicatorValue() {
		return indicatorValue;
	}

	public void setIndicatorValue(String indicatorValue) {
		this.indicatorValue = indicatorValue;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String UUID) {
		this.UUID = UUID;
	}

}
