package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

public class StationAndPeopleRelate {
	private String figureIdNumber;
	private String peopleIdNumber;

	public String getFigureIdNumber() {
		return figureIdNumber;
	}

	public void setFigureIdNumber(String figureIdNumber) {
		this.figureIdNumber = figureIdNumber;
	}

	public String getPeopleIdNumber() {
		return peopleIdNumber;
	}

	public void setPeopleIdNumber(String peopleIdNumber) {
		this.peopleIdNumber = peopleIdNumber;
	}
}
