package epros.draw.gui.designer;

import java.util.List;

import javax.swing.JMenuItem;

/**
 * 
 * 设计器右键菜单项纪录类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDesignerToolData {

	// *********************** 画图面板右键菜单项 ***********************/
	/** 流程地图 */
	private static List<JMenuItem> totalMapWorkflowMenuItemlist = null;
	/** 流程图 */
	private static List<JMenuItem> partMapWorkflowMenuItemlist = null;
	/** 组织图 */
	private static List<JMenuItem> orgMapWorkflowMenuItemlist = null;

	// *********************** 画图面板右键菜单项 ***********************/

	// *********************** 流程元素右键菜单项 ***********************/
	/** 流程地图 */
	private static List<JMenuItem> totalMapFlowElemMenuItemlist = null;
	/** 流程图 */
	private static List<JMenuItem> partMapFlowElemMenuItemlist = null;
	/** 组织图 */
	private static List<JMenuItem> orgMapFlowElemMenuItemlist = null;

	// *********************** 流程元素右键菜单项 ***********************/

	public static List<JMenuItem> getTotalMapWorkflowMenuItemlist() {
		return totalMapWorkflowMenuItemlist;
	}

	public static void setTotalMapWorkflowMenuItemlist(
			List<JMenuItem> totalMapWorkflowMenuItemlist) {
		JecnDesignerToolData.totalMapWorkflowMenuItemlist = totalMapWorkflowMenuItemlist;
	}

	public static List<JMenuItem> getPartMapWorkflowMenuItemlist() {
		return partMapWorkflowMenuItemlist;
	}

	public static void setPartMapWorkflowMenuItemlist(
			List<JMenuItem> partMapWorkflowMenuItemlist) {
		JecnDesignerToolData.partMapWorkflowMenuItemlist = partMapWorkflowMenuItemlist;
	}

	public static List<JMenuItem> getOrgMapWorkflowMenuItemlist() {
		return orgMapWorkflowMenuItemlist;
	}

	public static void setOrgMapWorkflowMenuItemlist(
			List<JMenuItem> orgMapWorkflowMenuItemlist) {
		JecnDesignerToolData.orgMapWorkflowMenuItemlist = orgMapWorkflowMenuItemlist;
	}

	public static List<JMenuItem> getTotalMapFlowElemMenuItemlist() {
		return totalMapFlowElemMenuItemlist;
	}

	public static void setTotalMapFlowElemMenuItemlist(
			List<JMenuItem> totalMapFlowElemMenuItemlist) {
		JecnDesignerToolData.totalMapFlowElemMenuItemlist = totalMapFlowElemMenuItemlist;
	}

	public static List<JMenuItem> getPartMapFlowElemMenuItemlist() {
		return partMapFlowElemMenuItemlist;
	}

	public static void setPartMapFlowElemMenuItemlist(
			List<JMenuItem> partMapFlowElemMenuItemlist) {
		JecnDesignerToolData.partMapFlowElemMenuItemlist = partMapFlowElemMenuItemlist;
	}

	public static List<JMenuItem> getOrgMapFlowElemMenuItemlist() {
		return orgMapFlowElemMenuItemlist;
	}

	public static void setOrgMapFlowElemMenuItemlist(
			List<JMenuItem> orgMapFlowElemMenuItemlist) {
		JecnDesignerToolData.orgMapFlowElemMenuItemlist = orgMapFlowElemMenuItemlist;
	}

}
