package com.jecn.epros.server.email.buss;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 邮件信息
 * 
 * @author xiaohu
 * 
 */
public class SimpleUserEmailBean {
	/** 内网邮件地址 */
	private List<UserEmailBean> listInnerEmail = new ArrayList<UserEmailBean>();
	/** 外网邮件地址 */
	private List<UserEmailBean> listOutterEmail = new ArrayList<UserEmailBean>();
	/** 邮件主题 */
	private String emailSubject;
	/** 邮件正文 */
	private String emailContent;
	/** 邮件附件 */
	private File runTestFile = null;
	/** HTTP请求拼装 */
	private String httpUrl;
	/** http请求显示信息str */
	private String httpStr;
	/** 审批邮件正文 */
	private String taskContent;
	/** 发布邮件正文 */
	private String pubContent;
	/** 邮件地址提示 */
	private String mailAddrTip;
	/** 邮件密码提示 */
	private String mailPwdTip;
	/** 邮件落款 */
	private String mailEndContent;
	/** 邮件信息换行 */
	private String strBr = "<br>";

	/** 附件名称 */
	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	/**
	 * 获取邮件正文
	 * 
	 * @return
	 */
	public String getEmailContent() {
		if (JecnCommon.isNullOrEmtryTrim(taskContent)
				&& JecnCommon.isNullOrEmtryTrim(pubContent)) {
			return emailContent;
		}
		// 组装邮件正文信息
		String content = strBr + JecnUtil.valueToString(mailAddrTip)
				+ this.getHttpUrl() + getHttpStr()
				+ JecnUtil.valueToString(mailPwdTip) + strBr
				+ JecnUtil.valueToString(mailEndContent);
		if (JecnCommon.isNullOrEmtryTrim(taskContent)) {// 任务正文为空
			emailContent = pubContent + content;
		} else {// 发布提示
			emailContent = taskContent + content;
		}
		return emailContent;
	}

	public List<UserEmailBean> getListInnerEmail() {
		return listInnerEmail;
	}

	public void setListInnerEmail(List<UserEmailBean> listInnerEmail) {
		this.listInnerEmail = listInnerEmail;
	}

	public List<UserEmailBean> getListOutterEmail() {
		return listOutterEmail;
	}

	public void setListOutterEmail(List<UserEmailBean> listOutterEmail) {
		this.listOutterEmail = listOutterEmail;
	}

	public File getRunTestFile() {
		return runTestFile;
	}

	public void setRunTestFile(File runTestFile) {
		this.runTestFile = runTestFile;
	}

	/**
	 * 拼装HTTP请求
	 * 
	 * @return String
	 */
	public String getHttpUrl() {
		if (JecnCommon.isNullOrEmtryTrim(httpUrl)) {
			return "";
		}
		return "<a href = '" + httpStr + httpUrl + "'>";
	}

	public void setHttpUrl(String httpUrl) {
		this.httpUrl = httpUrl;
	}

	/**
	 * 显示请求连接地址
	 * 
	 * @return String
	 */
	public String getHttpStr() {
		if (JecnCommon.isNullOrEmtryTrim(httpUrl)) {// 不存在链接地址
			return "";
		}
		return httpStr + "</a>" + "<br>";
	}

	public void setHttpStr(String httpStr) {
		this.httpStr = httpStr;
	}

	public String getTaskContent() {
		return taskContent;
	}

	public void setTaskContent(String taskContent) {
		this.taskContent = taskContent;
	}

	public String getPubContent() {
		return pubContent;
	}

	public void setPubContent(String pubContent) {
		this.pubContent = pubContent;
	}

	public String getMailAddrTip() {
		return mailAddrTip;
	}

	public void setMailAddrTip(String mailAddrTip) {
		this.mailAddrTip = mailAddrTip;
	}

	public String getMailPwdTip() {
		return mailPwdTip;
	}

	public void setMailPwdTip(String mailPwdTip) {
		this.mailPwdTip = mailPwdTip;
	}

	public String getMailEndContent() {
		return mailEndContent;
	}

	public void setMailEndContent(String mailEndContent) {
		this.mailEndContent = mailEndContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

}
