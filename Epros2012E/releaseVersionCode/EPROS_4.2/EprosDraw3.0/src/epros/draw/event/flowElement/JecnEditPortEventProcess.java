package epros.draw.event.flowElement;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.JScrollBar;
import javax.swing.SwingUtilities;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.ReSizeLineType;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.gui.box.JecnToolBoxMainPanel;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.JecnEditPortPanel;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.figure.unit.SnapToGrid;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.resize.JecnManhattanLineResizePanel;
import epros.draw.gui.line.shape.JecnTempManLine;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.JecnSelectAreaPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;

/**
 * 
 * 编辑点事件类
 * 
 * 编辑点说明：
 * 
 * @author ZHANGXH
 * 
 */
public class JecnEditPortEventProcess {
	/** 横向滚动条 */
	private static JScrollBar hBar = null;
	/** 纵向滚动条 */
	private static JScrollBar vBar = null;
	/** 滚动条移动+50 */
	private static int setpNum = 50;

	/**
	 * 鼠标点击链接先进入图形编辑点区域时事件处理
	 * 
	 * @param e
	 */
	public void drawLineMouseEntered(MouseEvent e) {
		if (e.getSource() == null) {
			return;
		}
		if (e.getSource() instanceof JecnEditPortPanel) {
			JecnEditPortPanel editPort = (JecnEditPortPanel) e.getSource();
			JecnToolBoxMainPanel boxPanel = JecnDrawMainPanel.getMainPanel().getBoxPanel();
			switch (boxPanel.getSelectedBtnType()) {
			case ManhattanLine:
			case CommonLine:
				lineMouseEntered(editPort);
				break;
			default:
				break;
			}
		}

	}

	/**
	 * 添加连接线事件结束处理
	 * 
	 * @param e
	 */
	public void drawLineMouseExited(MouseEvent e) {
		if (e.getSource() == null) {
			return;
		}
		if (e.getSource() instanceof JecnEditPortPanel) {
			JecnEditPortPanel editPort = (JecnEditPortPanel) e.getSource();
			JecnToolBoxMainPanel boxPanel = JecnDrawMainPanel.getMainPanel().getBoxPanel();
			switch (boxPanel.getSelectedBtnType()) {
			case ManhattanLine:
			case CommonLine:
				lineMouseExited(editPort);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * 鼠标释放画线 走曼哈顿算法 鼠标释放处理
	 * 
	 * @param e
	 */
	public void drawLineMouseReleased(MouseEvent e) {

		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();

		if (e.getSource() == null || desktopPane == null) {
			return;
		}

		if (e.getSource() instanceof JecnEditPortPanel) {

			JecnEditPortPanel editPortPanel = (JecnEditPortPanel) e.getSource();

			// 是在拖动状态为false
			JecnManLineResizeProcess.setDragged(false);

			JecnBaseManhattanLinePanel linePanel = getJecnBaseManhattanLinePanel();
			if (linePanel == null || desktopPane.getTempLine() == null) {
				return;
			}
			// 获开始端口对象
			JecnEditPortPanel startEditPort = JecnDrawMainPanel.getMainPanel().getWorkflow().getTempLine()
					.getStartPort();

			// 获结束端口对象
			JecnEditPortPanel endEditPort = JecnDrawMainPanel.getMainPanel().getWorkflow().getTempLine().getEndPort();

			if (endEditPort == null || startEditPort == null) {
				// 清空拖动连接线生成的临时编辑点
				desktopPane.setTempEditPort(null);
				desktopPane.setTempLine(null);
				desktopPane.repaint();
				return;
			}
			if (startEditPort.equals(endEditPort)) {
				if (!JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure()) {
					// 清空临时线段
					desktopPane.getTempLine().disposeLines();
					// 清空拖动连接线生成的临时编辑点
					desktopPane.setTempEditPort(null);
					desktopPane.setTempLine(null);
					desktopPane.repaint();
					return;
				} else {
					Point newPoint = SwingUtilities.convertPoint(editPortPanel, e.getPoint(), desktopPane);
					JecnBaseFigurePanel endFigure = MapManLineCommon.INSTANCE.craeteFigurePanel(newPoint);
					// 根据Point相对位置获取结束端点位置
					EditPointType endType = desktopPane.getTempLine().getFlowElementData().getEndEditPointType();

					endEditPort = JecnEditPortPanel.createPortPanelByEditPointType(endFigure, endType);
					endEditPort.setLocation(newPoint);
				}
			}

			// **********圈内**********//
			// 鼠标在编辑上相对面板的坐标
			Point newPoint = SwingUtilities.convertPoint(editPortPanel, e.getPoint(), desktopPane);
			if (!endEditPort.getBounds().contains(newPoint)) {
				// 清空临时线段
				desktopPane.getTempLine().disposeLines();
				// 清空拖动连接线生成的临时编辑点
				desktopPane.setTempEditPort(null);
				desktopPane.setTempLine(null);
				desktopPane.repaint();
				return;
			}
			// **********圈内**********//

			// 设置连接线开始图形和结束图形
			linePanel.reSetEditFigure(startEditPort, endEditPort);
			// 添加连接线到画图面板
			JecnAddFlowElementUnit.addFlowElement(e.getPoint(), linePanel);

			// *******************撤销恢复*******************//
			// 操作后数据
			JecnUndoRedoData redoData = new JecnUndoRedoData();
			// 记录撤销恢复数据
			redoData.recodeFlowElement(linePanel);
			JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createAdd(redoData);
			desktopPane.getUndoRedoManager().addEdit(undoProcess);
			// 清空临时线段
			desktopPane.getTempLine().disposeLines();

			if (desktopPane.getFlowMapData().isHiddenLineEditPoint()) {
				JecnPaintFigureUnit.hideEditPortByList(desktopPane.getShowEditPointFigures());
			}

			// 清空拖动连接线生成的临时编辑点
			desktopPane.setTempEditPort(null);
			desktopPane.setTempLine(null);
			// *******************撤销恢复*******************//

		}
	}

	/**
	 * 画线结束编辑点恢复画线前状态
	 * 
	 * @param editPort
	 */
	public void lineMouseExited(JecnEditPortPanel editPort) {
		if (editPort == null) {
			return;
		}
		if (editPort.isAllowLink()) {// true:编辑点大小为14背景为红色；false:大小为12,背景为绿色
			editPort.exitStyle();
		}
	}

	/**
	 * 当鼠标进入端点区域时，端点放大2像素
	 * 
	 * @param editPort
	 *            JecnEditPortPanel
	 */
	public void lineMouseEntered(JecnEditPortPanel editPort) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		/** ****** 拖动连接线端点处理 start******* */
		if (dragLinePortEnterProcess(desktopPane, editPort)) {
			return;
		}
		/** ****** 拖动连接线端点处理 end******* */

		/** ****** 添加连接线处理 start******* */
		if (JecnWorkflowUtil.isPartMap(desktopPane)) {// 流程元素编辑点:1-左上进右下出（默认）;0-四个编辑点都能进出2:左进右出
			if (JecnSystemStaticData.isEditPortType() == 1) {
				if (isStartPort1(desktopPane, editPort) || isEndPort1(desktopPane, editPort)) {// 开始点进入或者结束点进入
					lineMouseEnteredProcess(editPort, desktopPane);
				}
				return;
			} else if (JecnSystemStaticData.isEditPortType() == 2) {// 左进右出
				if (isStartPort2(desktopPane, editPort) || isEndPort2(desktopPane, editPort)) {// 开始点进入或者结束点进入
					lineMouseEnteredProcess(editPort, desktopPane);
				}
				return;
			}
		}

		lineMouseEnteredProcess(editPort, desktopPane);
		/** ****** 添加连接线处理 end******* */
	}

	/**
	 * 鼠标拖动生成临时线段 拖拽事件处理
	 * 
	 * @param e
	 */
	public void drawLineMouseDragged(MouseEvent e) {
		if (e.getSource() == null || JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (e.getSource() instanceof JecnEditPortPanel) {
			// 是在拖动状态为true
			JecnManLineResizeProcess.setDragged(true);
			// 获取端口对象
			JecnEditPortPanel editPort = (JecnEditPortPanel) e.getSource();

			if (desktopPane.getTempLine() != null && desktopPane.getTempLine().getStartPort() != null) {// 已存在临时线段
				//
				// Point tempEndPoint = new Point(editPort.getX() + e.getX(),
				// editPort.getY() + e.getY());
				Point tempEndPoint = SnapToGrid.checkIsGrid(editPort.getX() + e.getX(), editPort.getY() + e.getY());
				// 设置线段开始点 ， 默认为原始大小
				desktopPane.getTempLine().setEndTempPoint(tempEndPoint);
				// 拖动连接线结束点面板横竖滚动条跟随移动
				draggedScrollPanel(tempEndPoint);

				if (!desktopPane.getTempLine().getEndPort().isAllowLink()) {
					drawTmpLine();
				}
				// 事件传递 (图形鼠标点击事件转换为面板鼠标点击事件)
				MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(editPort, e, desktopPane);
				JecnPaintFigureUnit.selectMoveInDesk(desMouseEvent, editPort.getBaseFigurePanel());
			}
		}
	}

	private void drawTmpLine() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 根据Point相对位置获取结束端点位置
		EditPointType endType = getEndPortType(desktopPane);
		if (JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure()) {
			EditPointType tmpType = MapManLineCommon.INSTANCE.getEditPortByFigures(desktopPane.getTempLine()
					.getOriginalPoint());
			if (tmpType != null) {
				endType = tmpType;
				// 设置线段结束
			}
		}

		// 设置数据层结束点
		desktopPane.getTempLine().getFlowElementData().setEndEditPointType(endType);
		// 清空临时线段
		desktopPane.getTempLine().disposeLines();
		// 拖动时生成虚拟连接线线段
		desktopPane.getTempLine().paintDraggedLinePort(
				desktopPane.getTempLine().getStartPort().getOriginalCenterPoint(),
				desktopPane.getTempLine().getStartPort().getEditPointType(),
				desktopPane.getTempLine().getOriginalPoint(), endType);

		desktopPane.getWorkflowZoomProcess().zoomManhattanLine(desktopPane.getTempLine());
	}

	/**
	 * 拖动连接线结束点面板横竖滚动条跟随移动
	 * 
	 * @param tempEndPoint
	 *            Point 连接线结束点
	 */
	public static void draggedScrollPanel(Point tempEndPoint) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (tempEndPoint == null || desktopPane == null) {
			return;
		}
		// 获取横向滚动条
		hBar = desktopPane.getScrollPanle().getHorizontalScrollBar();
		// 获取横向滚动条
		vBar = desktopPane.getScrollPanle().getVerticalScrollBar();
		// 横向滚动条的值
		int left = hBar.getValue();
		// 横向滚动条值 + 横向滚动条宽度
		int right = left
				+ DrawCommon.convertDoubleToInt(desktopPane.getScrollPanle().getViewportBorderBounds().getWidth());
		// 纵向滚动条的值
		int head = vBar.getValue();
		// 纵向滚动条值 + 纵向滚动条高度
		int bottom = vBar.getValue()
				+ DrawCommon.convertDoubleToInt(desktopPane.getScrollPanle().getViewportBorderBounds().getHeight());

		if (tempEndPoint.x >= right) {// 如果连接线结束点横坐标大于等于横向滚动条边界值，滚动条值右移50
			hBar.setValue(hBar.getValue() + setpNum);
		}
		if (tempEndPoint.x <= left) {// 如果连接线结束点横坐标小于或等于滚动条值，滚动条左移50
			hBar.setValue(hBar.getValue() - setpNum);
		}
		if (tempEndPoint.y >= bottom) {// 如果连接线结束点横坐标大于等于纵向向滚动条边界值，滚动条值下移50
			vBar.setValue(vBar.getValue() + setpNum);
		}
		if (tempEndPoint.y <= head) {// 如果连接线结束点横坐标大于等于横向滚动值，滚动条值上移50
			vBar.setValue(vBar.getValue() - setpNum);
		}
	}

	/**
	 * 当鼠标进入端点区域时，端点放大2像素
	 * 
	 * @param editPort
	 *            JecnEditPortPanel
	 * @param desktopPane
	 *            JecnDrawDesktopPane
	 * 
	 */
	private void lineMouseEnteredProcess(JecnEditPortPanel editPort, JecnDrawDesktopPane desktopPane) {

		// 设置编辑点位置和大小，背景色
		editPort.enterStyle();

		if (desktopPane.getStartPointType() != null) {
			// 面板拖动添加联系，不执行图形编辑点事件
			return;
		}

		if (desktopPane.getTempLine() != null) {
			// 设置连接线结束端口
			desktopPane.getTempLine().setEndPort(editPort);
			// 设置线段开始点 ， 默认为原始大小
			desktopPane.getTempLine().setEndTempPoint(editPort.getZoomCenterPoint());
			// 重画临时线段
			drawTmpLine();
			// if
			// (!desktopPane.tempDividingList.contains(desktopPane.tempLine))
			// {
			// // 添加虚拟线段到虚拟线段数组
			// desktopPane.tempDividingList.add(desktopPane.tempLine);
			// }
		} else {
			// 记录临时端点
			desktopPane.setTempEditPort(editPort);
		}
	}

	/**
	 * 
	 * 拖动连接线端点处理
	 * 
	 * @param desktopPane
	 *            JecnDrawDesktopPane
	 * @param editPort
	 *            JecnEditPortPanel
	 * @return boolean 是连接线端点拖动处理：true；其他：false
	 */
	private boolean dragLinePortEnterProcess(JecnDrawDesktopPane desktopPane, JecnEditPortPanel editPort) {
		if (!JecnManLineResizeProcess.isDragged()) {// 没有拖动
			return false;
		}
		// 当前拖动连接线端点
		JecnManhattanLineResizePanel draggedResizePanel = JecnManLineResizeProcess.getDraggedResizePanel();

		if (desktopPane.getTempLine() != null || desktopPane.getJecnTempManLine() == null || draggedResizePanel == null) {// 不是连接线端点拖动
			return false;
		}

		// 获取当前给定的连接线端点对应的图形的编辑点类型（此方法是为了解决通过点击连接线端点不能找到对应的图形编辑点类型）
		EditPointType editPointType = draggedResizePanel.getManhattanLine().getCurrEditPointType(draggedResizePanel);

		// 编辑点类型相同且是同一个图形,拖动不处理直接返回，解决拖动连接线端点和释放的编辑点是同一个时
		JecnBaseFigurePanel figure = draggedResizePanel.getManhattanLine().getCurrEditPointFigure(draggedResizePanel);
		if (editPort.getBaseFigurePanel() == figure && editPort.getEditPointType() == editPointType) {
			// 拖动线段端点记录临时端点
			desktopPane.setTempEditPort(null);
			return true;
		}
		if (JecnWorkflowUtil.isPartMap(desktopPane)) {// 流程元素编辑点:true-左上进右下出（默认）;false-四个编辑点都能进出
			if (dragLinePortEnter(editPort, draggedResizePanel.getReSizeLineType())) {
				// 设置编辑点位置和大小，背景色
				editPort.enterStyle();
				// 拖动线段端点记录临时端点
				desktopPane.setTempEditPort(editPort);
			} else {
				// 拖动线段端点记录临时端点
				desktopPane.setTempEditPort(null);
			}
		} else {
			// 设置编辑点位置和大小，背景色
			editPort.enterStyle();
			// 拖动线段端点记录临时端点
			desktopPane.setTempEditPort(editPort);
		}
		return true;
	}

	/**
	 * 拖动连接线,进入的点是否可 连接连线
	 * 
	 * @param editPort
	 * @return
	 */
	private boolean dragLinePortEnter(JecnEditPortPanel editPort, ReSizeLineType lineType) {
		if (JecnSystemStaticData.isEditPortType() == 0) {
			return true;
		} else if (JecnSystemStaticData.isEditPortType() == 1) {// 左上进，右下出
			if (!editPort.isStartPort()
					&& lineType == ReSizeLineType.end
					&& (editPort.getEditPointType().equals(EditPointType.left) || editPort.getEditPointType().equals(
							EditPointType.top))) {
				return true;
			} else if (editPort.isStartPort()
					&& lineType == ReSizeLineType.start
					&& (editPort.getEditPointType().equals(EditPointType.right) || editPort.getEditPointType().equals(
							EditPointType.bottom))) {
				return true;
			}
		} else if (JecnSystemStaticData.isEditPortType() == 2) {// 左进右出
			if (editPort.isStartPort() && lineType == ReSizeLineType.start
					&& editPort.getEditPointType().equals(EditPointType.right)) {
				return true;
			} else if (!editPort.isStartPort() && lineType == ReSizeLineType.end
					&& editPort.getEditPointType().equals(EditPointType.left)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 创建连接线临时线
	 * 
	 * @param desktopPane
	 */
	public static void creatTempLine(JecnDrawDesktopPane desktopPane) {
		JecnManhattanLineData flowElementData = new JecnManhattanLineData(MapElemType.CommonLine);
		// 线条为点线
		flowElementData.setBodyStyle(2);
		// 线宽为1像素
		flowElementData.setBodyWidth(1);
		flowElementData.setBodyColor(Color.red);
		flowElementData.setZOrderIndex(desktopPane.getMaxZOrderIndex() + 1);
		desktopPane.setTempLine(new JecnTempManLine(flowElementData));
	}

	/**
	 * 根据Point相对位置获取结束端点位置
	 * 
	 * @param desktopPane
	 * @return EditPointType 结束点位置
	 */
	private EditPointType getEndPortType(JecnDrawDesktopPane desktopPane) {
		if (desktopPane.getTempLine().getStartPort().equals(desktopPane.getTempLine().getEndPort())) {
			Point startPoint = desktopPane.getTempLine().getStartPort().getOriginalCenterPoint();
			Point endPoint = desktopPane.getTempLine().getEndTempPoint();
			if (startPoint.x < endPoint.x) {
				return EditPointType.left;
			} else {
				return EditPointType.right;
			}
		} else {
			return desktopPane.getTempLine().getEndPort().getEditPointType();
		}

	}

	/**
	 * 连接线画线鼠标点击
	 * 
	 * @param e
	 */
	public void drawLineMousePressed(MouseEvent e) {
		if (e.getSource() instanceof JecnEditPortPanel) {
			// 编辑端点
			JecnEditPortPanel editPort = (JecnEditPortPanel) e.getSource();
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (desktopPane.getTempLine() != null) {
				desktopPane.getTempLine().disposeLines();
				desktopPane.setTempLine(null);
			}
			if (desktopPane.getTempLine() == null && editPort.isAllowLink()) {// 不存在连接线临时线段且可连接情况下
				// 创建连接线临时线段对象
				creatTempLine(desktopPane);
				// 设置线段开始点
				desktopPane.getTempLine().setStartPort(desktopPane.getTempEditPort());
				// 设置线段结束
				desktopPane.getTempLine().setEndPort(editPort);
			}
			// 流程元素库面板
			JecnToolBoxMainPanel boxPanel = JecnDrawMainPanel.getMainPanel().getBoxPanel();

			switch (boxPanel.getSelectedBtnType()) {
			case ManhattanLine:
			case CommonLine:
				// 当鼠标进入端点区域时，端点放大2像素
				lineMousePressed(editPort);
				break;
			default:
				break;
			}
		}

	}

	/**
	 * 
	 * 按下动作
	 * 
	 * @param editPort
	 */
	private void lineMousePressed(JecnEditPortPanel editPort) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		// **************清除圈选框**************//
		JecnSelectAreaPanel selectPanel = desktopPane.getAidCompCollection().getSelecedAreaPanel();
		selectPanel.setVisibleFalse();
		// **************清除圈选框**************//
		desktopPane.repaint();
	}

	/**
	 * 获取连接线对象
	 * 
	 * @return
	 */
	public static JecnBaseManhattanLinePanel getJecnBaseManhattanLinePanel() {
		if (JecnDrawMainPanel.getMainPanel() == null) {
			return null;
		}
		// 获取box 选中对象
		JecnToolBoxMainPanel boxPanel = JecnDrawMainPanel.getMainPanel().getBoxPanel();
		if (MapElemType.ManhattanLine.equals(boxPanel.getSelectedBtnType())
				|| MapElemType.CommonLine.equals(boxPanel.getSelectedBtnType())) {
			// 获取线的数据对象
			return getManhattanLinePanelByMapElemType(boxPanel.getSelectedBtnType());
		}
		return null;
	}

	/**
	 * 通过MapElemType 类型创建连接线对象
	 * 
	 * @param mapElemType
	 *            ManhattanLine,CommonLine
	 * @return JecnBaseManhattanLinePanel
	 */
	public static JecnBaseManhattanLinePanel getManhattanLinePanelByMapElemType(MapElemType mapElemType) {
		if (mapElemType == null) {
			return null;
		}
		// 获取线的数据对象
		JecnBaseManhattanLinePanel manhattanLine = (JecnBaseManhattanLinePanel) JecnCreateFlowElement
				.getFlowEmelentByType(mapElemType);
		return manhattanLine;
	}

	/**
	 * 左进右出 开始点处理
	 * 
	 * @param desktopPane
	 * @param editPort
	 * @return
	 */
	private boolean isStartPort1(JecnDrawDesktopPane desktopPane, JecnEditPortPanel editPort) {
		return desktopPane.getTempLine() == null
				&& editPort.isStartPort()
				&& (EditPointType.right.equals(editPort.getEditPointType()) || EditPointType.bottom.equals(editPort
						.getEditPointType()));
	}

	private boolean isStartPort2(JecnDrawDesktopPane desktopPane, JecnEditPortPanel editPort) {
		return desktopPane.getTempLine() == null && editPort.isStartPort()
				&& EditPointType.right.equals(editPort.getEditPointType());
	}

	/**
	 * 左进右出 结束点处理
	 * 
	 * @param desktopPane
	 * @param editPort
	 * @return
	 */
	private boolean isEndPort1(JecnDrawDesktopPane desktopPane, JecnEditPortPanel editPort) {
		return desktopPane.getTempLine() != null
				&& !editPort.isStartPort()
				&& (EditPointType.left.equals(editPort.getEditPointType()) || EditPointType.top.equals(editPort
						.getEditPointType()));
	}

	private boolean isEndPort2(JecnDrawDesktopPane desktopPane, JecnEditPortPanel editPort) {
		return desktopPane.getTempLine() != null && !editPort.isStartPort()
				&& EditPointType.left.equals(editPort.getEditPointType());
	}
}
