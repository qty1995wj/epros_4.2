package com.jecn.epros.server.service.integration;

import java.util.List;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;
import com.jecn.epros.server.common.IBaseService;

/**
 * 活动类别service接口
 * 
 * @author Administrator
 * @date： 日期：2013-10-31 时间：上午10:25:41
 */
public interface IJecnActivityService extends
		IBaseService<JecnActiveTypeBean, Long> {
	/**
	 * 添加或更新活动类型
	 * 
	 * @param activeTypeBean
	 * @throws Exception
	 */
	Long addActivityType(JecnActiveTypeBean activeTypeBean) throws Exception;

	/**
	 * 
	 * 获取指定名称的的个数
	 * 
	 * @param name
	 * @return int名称个数
	 * @throws Exception
	 */
	int findCountByName(String name) throws Exception;

	/**
	 * 获取活动类别集合
	 * 
	 * @return List<JecnActiveTypeBean>
	 * @throws Exception
	 */
	List<JecnActiveTypeBean> findJecnActiveTypeBeanList() throws Exception;

	/**
	 * 根据活动类别ID 删除类别
	 * 
	 * @param typeIds
	 * @throws Exception
	 */
	void delteFlowArributeType(List<Long> typeIds) throws Exception;

}
