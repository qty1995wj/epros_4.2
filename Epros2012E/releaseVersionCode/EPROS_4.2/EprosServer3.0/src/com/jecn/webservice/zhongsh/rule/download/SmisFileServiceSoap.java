package com.jecn.webservice.zhongsh.rule.download;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.7.18
 * 2016-10-20T09:57:48.963+08:00
 * Generated source version: 2.7.18
 * 
 */
@WebService(targetNamespace = "http://tempuri.org/", name = "SmisFileServiceSoap")
@XmlSeeAlso({ObjectFactory.class})
public interface SmisFileServiceSoap {

    @WebMethod(operationName = "DeleteSystemDocument", action = "http://tempuri.org/DeleteSystemDocument")
    @RequestWrapper(localName = "DeleteSystemDocument", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DeleteSystemDocument")
    @ResponseWrapper(localName = "DeleteSystemDocumentResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DeleteSystemDocumentResponse")
    @WebResult(name = "DeleteSystemDocumentResult", targetNamespace = "http://tempuri.org/")
    public boolean deleteSystemDocument(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "documentId", targetNamespace = "http://tempuri.org/")
        java.lang.String documentId
    );

    @WebMethod(operationName = "GetDocumentBySystemId", action = "http://tempuri.org/GetDocumentBySystemId")
    @RequestWrapper(localName = "GetDocumentBySystemId", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.GetDocumentBySystemId")
    @ResponseWrapper(localName = "GetDocumentBySystemIdResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.GetDocumentBySystemIdResponse")
    @WebResult(name = "GetDocumentBySystemIdResult", targetNamespace = "http://tempuri.org/")
    public java.lang.String getDocumentBySystemId(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "systemId", targetNamespace = "http://tempuri.org/")
        java.lang.String systemId
    );

    @WebMethod(operationName = "UploadSystemDocument", action = "http://tempuri.org/UploadSystemDocument")
    @RequestWrapper(localName = "UploadSystemDocument", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.UploadSystemDocument")
    @ResponseWrapper(localName = "UploadSystemDocumentResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.UploadSystemDocumentResponse")
    @WebResult(name = "UploadSystemDocumentResult", targetNamespace = "http://tempuri.org/")
    public java.lang.String uploadSystemDocument(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "systemId", targetNamespace = "http://tempuri.org/")
        java.lang.String systemId,
        @WebParam(name = "smisFileSystem", targetNamespace = "http://tempuri.org/")
        byte[] smisFileSystem,
        @WebParam(name = "documentName", targetNamespace = "http://tempuri.org/")
        java.lang.String documentName,
        @WebParam(name = "documentSuffix", targetNamespace = "http://tempuri.org/")
        java.lang.String documentSuffix,
        @WebParam(name = "fileType", targetNamespace = "http://tempuri.org/")
        int fileType,
        @WebParam(name = "docSource", targetNamespace = "http://tempuri.org/")
        java.lang.String docSource
    );

    @WebMethod(operationName = "DownloadSystemDocumentBySystemId", action = "http://tempuri.org/DownloadSystemDocumentBySystemId")
    @RequestWrapper(localName = "DownloadSystemDocumentBySystemId", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DownloadSystemDocumentBySystemId")
    @ResponseWrapper(localName = "DownloadSystemDocumentBySystemIdResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DownloadSystemDocumentBySystemIdResponse")
    @WebResult(name = "DownloadSystemDocumentBySystemIdResult", targetNamespace = "http://tempuri.org/")
    public byte[] downloadSystemDocumentBySystemId(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "systemId", targetNamespace = "http://tempuri.org/")
        java.lang.String systemId
    );

    @WebMethod(operationName = "UploadSystem", action = "http://tempuri.org/UploadSystem")
    @RequestWrapper(localName = "UploadSystem", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.UploadSystem")
    @ResponseWrapper(localName = "UploadSystemResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.UploadSystemResponse")
    @WebResult(name = "UploadSystemResult", targetNamespace = "http://tempuri.org/")
    public java.lang.String uploadSystem(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "systemId", targetNamespace = "http://tempuri.org/")
        java.lang.String systemId,
        @WebParam(name = "smisFileSystem", targetNamespace = "http://tempuri.org/")
        byte[] smisFileSystem,
        @WebParam(name = "sytemFilePathName", targetNamespace = "http://tempuri.org/")
        java.lang.String sytemFilePathName,
        @WebParam(name = "fileType", targetNamespace = "http://tempuri.org/")
        int fileType
    );

    @WebMethod(operationName = "DownloadSystem", action = "http://tempuri.org/DownloadSystem")
    @RequestWrapper(localName = "DownloadSystem", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DownloadSystem")
    @ResponseWrapper(localName = "DownloadSystemResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DownloadSystemResponse")
    public void downloadSystem(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "docId", targetNamespace = "http://tempuri.org/")
        java.lang.String docId,
        @WebParam(name = "smisFileSystem", targetNamespace = "http://tempuri.org/")
        byte[] smisFileSystem,
        @WebParam(mode = WebParam.Mode.OUT, name = "DownloadSystemResult", targetNamespace = "http://tempuri.org/")
        javax.xml.ws.Holder<byte[]> downloadSystemResult,
        @WebParam(mode = WebParam.Mode.OUT, name = "documentName", targetNamespace = "http://tempuri.org/")
        javax.xml.ws.Holder<java.lang.String> documentName,
        @WebParam(mode = WebParam.Mode.OUT, name = "documentExtension", targetNamespace = "http://tempuri.org/")
        javax.xml.ws.Holder<java.lang.String> documentExtension,
        @WebParam(mode = WebParam.Mode.OUT, name = "docContentType", targetNamespace = "http://tempuri.org/")
        javax.xml.ws.Holder<java.lang.String> docContentType
    );

    @WebMethod(operationName = "DownloadSystemByDocId", action = "http://tempuri.org/DownloadSystemByDocId")
    @RequestWrapper(localName = "DownloadSystemByDocId", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DownloadSystemByDocId")
    @ResponseWrapper(localName = "DownloadSystemByDocIdResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DownloadSystemByDocIdResponse")
    @WebResult(name = "DownloadSystemByDocIdResult", targetNamespace = "http://tempuri.org/")
    public byte[] downloadSystemByDocId(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "docId", targetNamespace = "http://tempuri.org/")
        java.lang.String docId
    );

    @WebMethod(operationName = "UploadSystemToPdfQuick", action = "http://tempuri.org/UploadSystemToPdfQuick")
    @RequestWrapper(localName = "UploadSystemToPdfQuick", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.UploadSystemToPdfQuick")
    @ResponseWrapper(localName = "UploadSystemToPdfQuickResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.UploadSystemToPdfQuickResponse")
    @WebResult(name = "UploadSystemToPdfQuickResult", targetNamespace = "http://tempuri.org/")
    public java.lang.String uploadSystemToPdfQuick(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "systemId", targetNamespace = "http://tempuri.org/")
        java.lang.String systemId,
        @WebParam(name = "smisFileSystem", targetNamespace = "http://tempuri.org/")
        byte[] smisFileSystem,
        @WebParam(name = "documentName", targetNamespace = "http://tempuri.org/")
        java.lang.String documentName,
        @WebParam(name = "documentSuffix", targetNamespace = "http://tempuri.org/")
        java.lang.String documentSuffix,
        @WebParam(name = "fileType", targetNamespace = "http://tempuri.org/")
        int fileType,
        @WebParam(name = "generatePdf", targetNamespace = "http://tempuri.org/")
        boolean generatePdf,
        @WebParam(name = "generateOrdinance", targetNamespace = "http://tempuri.org/")
        boolean generateOrdinance,
        @WebParam(name = "docSource", targetNamespace = "http://tempuri.org/")
        java.lang.String docSource
    );

    @WebMethod(operationName = "HelloWorld", action = "http://tempuri.org/HelloWorld")
    @RequestWrapper(localName = "HelloWorld", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.HelloWorld")
    @ResponseWrapper(localName = "HelloWorldResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.HelloWorldResponse")
    @WebResult(name = "HelloWorldResult", targetNamespace = "http://tempuri.org/")
    public java.lang.String helloWorld();

    @WebMethod(operationName = "UploadSystemToPdf", action = "http://tempuri.org/UploadSystemToPdf")
    @RequestWrapper(localName = "UploadSystemToPdf", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.UploadSystemToPdf")
    @ResponseWrapper(localName = "UploadSystemToPdfResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.UploadSystemToPdfResponse")
    @WebResult(name = "UploadSystemToPdfResult", targetNamespace = "http://tempuri.org/")
    public java.lang.String uploadSystemToPdf(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "systemId", targetNamespace = "http://tempuri.org/")
        java.lang.String systemId,
        @WebParam(name = "smisFileSystem", targetNamespace = "http://tempuri.org/")
        byte[] smisFileSystem,
        @WebParam(name = "documentName", targetNamespace = "http://tempuri.org/")
        java.lang.String documentName,
        @WebParam(name = "documentSuffix", targetNamespace = "http://tempuri.org/")
        java.lang.String documentSuffix,
        @WebParam(name = "fileType", targetNamespace = "http://tempuri.org/")
        int fileType,
        @WebParam(name = "generatePdf", targetNamespace = "http://tempuri.org/")
        boolean generatePdf,
        @WebParam(name = "generateOrdinance", targetNamespace = "http://tempuri.org/")
        boolean generateOrdinance,
        @WebParam(name = "docSource", targetNamespace = "http://tempuri.org/")
        java.lang.String docSource
    );

    @WebMethod(operationName = "DeleteSystemDocumentBySystemId", action = "http://tempuri.org/DeleteSystemDocumentBySystemId")
    @RequestWrapper(localName = "DeleteSystemDocumentBySystemId", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DeleteSystemDocumentBySystemId")
    @ResponseWrapper(localName = "DeleteSystemDocumentBySystemIdResponse", targetNamespace = "http://tempuri.org/", className = "com.jecn.webservice.zhongsh.rule.download.DeleteSystemDocumentBySystemIdResponse")
    @WebResult(name = "DeleteSystemDocumentBySystemIdResult", targetNamespace = "http://tempuri.org/")
    public boolean deleteSystemDocumentBySystemId(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        java.lang.String password,
        @WebParam(name = "systemId", targetNamespace = "http://tempuri.org/")
        java.lang.String systemId
    );
}
