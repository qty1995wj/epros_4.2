/**
 * ORG_VIEWLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crs.dataimport;

public class ORG_VIEWLine  implements java.io.Serializable {
    private java.math.BigDecimal org_num;

    private java.lang.String org_name;

    private java.math.BigDecimal per_org_num;

    public ORG_VIEWLine() {
    }

    public ORG_VIEWLine(
           java.math.BigDecimal org_num,
           java.lang.String org_name,
           java.math.BigDecimal per_org_num) {
           this.org_num = org_num;
           this.org_name = org_name;
           this.per_org_num = per_org_num;
    }


    /**
     * Gets the org_num value for this ORG_VIEWLine.
     * 
     * @return org_num
     */
    public java.math.BigDecimal getOrg_num() {
        return org_num;
    }


    /**
     * Sets the org_num value for this ORG_VIEWLine.
     * 
     * @param org_num
     */
    public void setOrg_num(java.math.BigDecimal org_num) {
        this.org_num = org_num;
    }


    /**
     * Gets the org_name value for this ORG_VIEWLine.
     * 
     * @return org_name
     */
    public java.lang.String getOrg_name() {
        return org_name;
    }


    /**
     * Sets the org_name value for this ORG_VIEWLine.
     * 
     * @param org_name
     */
    public void setOrg_name(java.lang.String org_name) {
        this.org_name = org_name;
    }


    /**
     * Gets the per_org_num value for this ORG_VIEWLine.
     * 
     * @return per_org_num
     */
    public java.math.BigDecimal getPer_org_num() {
        return per_org_num;
    }


    /**
     * Sets the per_org_num value for this ORG_VIEWLine.
     * 
     * @param per_org_num
     */
    public void setPer_org_num(java.math.BigDecimal per_org_num) {
        this.per_org_num = per_org_num;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ORG_VIEWLine)) return false;
        ORG_VIEWLine other = (ORG_VIEWLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.org_num==null && other.getOrg_num()==null) || 
             (this.org_num!=null &&
              this.org_num.equals(other.getOrg_num()))) &&
            ((this.org_name==null && other.getOrg_name()==null) || 
             (this.org_name!=null &&
              this.org_name.equals(other.getOrg_name()))) &&
            ((this.per_org_num==null && other.getPer_org_num()==null) || 
             (this.per_org_num!=null &&
              this.per_org_num.equals(other.getPer_org_num())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOrg_num() != null) {
            _hashCode += getOrg_num().hashCode();
        }
        if (getOrg_name() != null) {
            _hashCode += getOrg_name().hashCode();
        }
        if (getPer_org_num() != null) {
            _hashCode += getPer_org_num().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ORG_VIEWLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://hr.publisher.mdm", "ORG_VIEWLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("org_num");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "org_num"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("org_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "org_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("per_org_num");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "per_org_num"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
