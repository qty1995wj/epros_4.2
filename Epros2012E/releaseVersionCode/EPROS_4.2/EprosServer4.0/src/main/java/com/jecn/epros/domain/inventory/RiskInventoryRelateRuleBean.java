package com.jecn.epros.domain.inventory;

import java.util.List;

/**
 * 风险与制度关联
 *
 * @author user sql
 */
public class RiskInventoryRelateRuleBean {
    //JecnRuleRiskBean
    private Long riskId;
    private List<FileInventoryCommonBean> listRule;

    public Long getRiskId() {
        return riskId;
    }

    public void setRiskId(Long riskId) {
        this.riskId = riskId;
    }

    public List<FileInventoryCommonBean> getListRule() {
        return listRule;
    }

    public void setListRule(List<FileInventoryCommonBean> listRule) {
        this.listRule = listRule;
    }

}
