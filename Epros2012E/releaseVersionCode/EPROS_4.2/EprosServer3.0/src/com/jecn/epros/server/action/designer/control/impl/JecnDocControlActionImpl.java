package com.jecn.epros.server.action.designer.control.impl;

import java.util.Date;
import java.util.List;

import com.jecn.epros.server.action.designer.control.IJecnDocControlAction;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.control.IJecnDocControlService;

public class JecnDocControlActionImpl implements IJecnDocControlAction {
	/** 文控信息service 接口 */
	private IJecnDocControlService docControlService;

	/**
	 * 发布任务，创建文控版本信息日志
	 * 
	 * @param docId
	 *            文控主键ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @param refId
	 *            关联ID
	 * @throws Exception
	 */
	public void saveJecnTaskHistoryNew(JecnTaskHistoryNew historyNew)
			throws Exception {
		Date now=new Date();
		//发布记录文控开始时间与结束时间应当取服务器时间
		historyNew.setStartTime(JecnCommon.getStringbyDate(now));
		historyNew.setEndTime(JecnCommon.getStringbyDate(now));
		docControlService.saveJecnTaskHistoryNew(historyNew);
	}

	/**
	 * 删除文控信息
	 * 
	 * @param docId
	 *            文控主键ID
	 * @throws Exception
	 * @throws Exception
	 */
	public void deleteJecnTaskHistoryNew(List<Long> docIds) throws Exception {
		docControlService.deleteJecnTaskHistoryNew(docIds);
	}

	/**
	 * 
	 * 编辑文控信息
	 * 
	 * @param JecnTaskHistoryNew
	 *            待修改的文控信息
	 * @return boolean True:版本号已存在
	 * @throws Exception
	 */
	public boolean updateJecnTaskHistoryNew(JecnTaskHistoryNew historyNew,
			String versionId) throws Exception {
		return docControlService
				.updateJecnTaskHistoryNew(historyNew, versionId);
	}

	/**
	 * 查阅文控信息
	 * 
	 * @param docId
	 *            文控主键ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @param refId
	 *            关联ID
	 * @throws Exception
	 */
	public FileOpenBean viewJecnTaskHistoryNew(Long docId,Long ruleId) throws Exception {
		return docControlService.viewJecnTaskHistoryNew(docId,ruleId);
	}

	public IJecnDocControlService getDocControlService() {
		return docControlService;
	}

	public void setDocControlService(IJecnDocControlService docControlService) {
		this.docControlService = docControlService;
	}

	/**
	 * 
	 * 根据流程ID获取文控信息版本号集合
	 * 
	 * @param flowId
	 *            流程ID
	 * @param type
	 *            0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
	 * @return List<String>文控信息版本号集合
	 * @throws Exception
	 */
	public List<String> findVersionListByFlowId(Long flowId, int type)
			throws Exception {
		return docControlService.findVersionListByFlowId(flowId, type);
	}

	/**
	 * 根据ID获取文控信息
	 * 
	 */
	public JecnTaskHistoryNew getJecnTaskHistoryNew(Long docId) {
		return docControlService.get(docId);
	}

	/**
	 * 文件文控类型和关联ID获取文控信息版本记录
	 * 
	 * @param refId
	 *            关联ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @return List<JecnTaskHistoryNew>
	 * @throws Exception
	 */
	public List<JecnTaskHistoryNew> getJecnTaskHistoryNewList(Long refId,
			int type) throws Exception {
		return docControlService.getJecnTaskHistoryNewList(refId, type);
	}

	/**
	 * 根据文控信息主键ID获取从表信息
	 * 
	 * @param historyId
	 *            文控主键ID
	 * @return 文控信息对应的各阶段审批人信息
	 * @throws Exception
	 */
	@Override
	public List<JecnTaskHistoryFollow> getJecnTaskHistoryFollowList(
			Long historyId) throws Exception {
		return docControlService.getJecnTaskHistoryFollowList(historyId);
	}

	/**
	 * 文件基本信息是否必填验证
	 * 
	 * @param flowId
	 *            流程ID
	 * @return List<String>
	 */
	@Override
	public List<String> getErrorList(Long flowId,int languageType) throws Exception {
		return docControlService.getErrorList(flowId,languageType);
	}

	@Override
	public List<String> findVersionListByFlowIdNoPage(Long id, int type) {
		return docControlService.findVersionListByFlowIdNoPage(id, type);
	}
}
