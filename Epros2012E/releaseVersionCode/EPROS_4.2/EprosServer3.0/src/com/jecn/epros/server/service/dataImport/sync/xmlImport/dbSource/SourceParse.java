package com.jecn.epros.server.service.dataImport.sync.xmlImport.dbSource;

import org.dom4j.Document;
import org.dom4j.DocumentException;

import com.jecn.epros.server.common.JecnFinal;

/**
 * 读取数据库连接的配置文件
 * 
 */
public class SourceParse {

	private static String driver;
	private static String url;
	private static String username;
	private static String password;

	/**
	 * 
	 * 解析XML文件
	 * 
	 * @param inputXml
	 *            xml配置文件路径
	 * @throws DocumentException
	 */
	public static void getDataSource(String inputXml) throws DocumentException {
		Document doc = JecnFinal.getDocumentByXml(inputXml);

		driver = doc.selectSingleNode("//database/driver/text()").getText();
		url = doc.selectSingleNode("//database/url/text()").getText();
		username = doc.selectSingleNode("//database/username/text()").getText();
		password = doc.selectSingleNode("//database/password/text()").getText();
	}

	/**
	 * 
	 * @return 获得数据库驱动
	 */
	public static String getDriver() {
		return driver;
	}

	/**
	 * 
	 * @return 获得数据库连接url
	 */
	public static String getUrl() {
		return url;
	}

	/**
	 * 
	 * @return 获得数据库连接用户名
	 */
	public static String getUsername() {
		return username;
	}

	/**
	 * 
	 * @return 获得数据库连接密码
	 */
	public static String getPassword() {
		return password;
	}

}
