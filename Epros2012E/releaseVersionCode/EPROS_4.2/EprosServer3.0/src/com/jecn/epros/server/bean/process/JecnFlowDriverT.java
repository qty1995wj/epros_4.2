package com.jecn.epros.server.bean.process;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.process.driver.JecnFlowDriverTimeT;

/**
 * 
 * 流程图时间驱动临时
 * 
 */
public class JecnFlowDriverT implements java.io.Serializable {
	private Long flowId;// 流程ID作为此表主键
	private String frequency;// 频率
	private String startTime;// 开始时间
	private String endTime;// 结束时间
	private String quantity;// 数量
	private Set<Long> driverEmailPeopleIds;// 邮件通知人
	/**
	 * 新版驱动规则的日期
	 */
	private List<JecnFlowDriverTimeT> times;

	public Set<Long> getDriverEmailPeopleIds() {
		return driverEmailPeopleIds;
	}

	public void setDriverEmailPeopleIds(Set<Long> driverEmailPeopleIds) {
		this.driverEmailPeopleIds = driverEmailPeopleIds;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public List<JecnFlowDriverTimeT> getTimes() {
		return times;
	}

	public void setTimes(List<JecnFlowDriverTimeT> times) {
		this.times = times;
	}

}
