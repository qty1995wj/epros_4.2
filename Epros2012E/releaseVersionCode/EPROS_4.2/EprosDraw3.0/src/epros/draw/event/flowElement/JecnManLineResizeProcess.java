package epros.draw.event.flowElement;

import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.ReSizeLineType;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.JecnEditPortPanel;
import epros.draw.gui.figure.unit.DraggedManLineUtil;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.resize.JecnManhattanLineResizePanel;
import epros.draw.gui.line.shape.JecnTempManLine;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnRemoveFlowElementUnit;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;

/**
 * 拖动曼哈顿线端点事件处理类
 * 
 * @author ZHANGXH
 * @date： 日期：May 29, 2012 时间：4:33:15 PM
 */
public class JecnManLineResizeProcess {
	// /** 拖动曼哈顿线端点，生成的虚拟曼哈顿线 */
	// private JecnTempManLine tempLine = null;
	/** 操作前数据 */
	private JecnUndoRedoData undoData = null;
	/** 操作后数据 */
	private JecnUndoRedoData redoData = null;
	/** 是否执行连接线端点拖动 true：执行拖动，false：未执行拖动 */
	private static boolean isDragged = false;
	/** 在点击连接线端点拖动时，draggedResizePanel对象为点击的连接线端点 */
	private static JecnManhattanLineResizePanel draggedResizePanel = null;

	/**
	 * 连接线鼠标点击事件处理
	 * 
	 * 点击连接线显示连接线选中点
	 * 
	 * @param e
	 */
	public void mouseResizePressed(MouseEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 主界面和面板是否存在
		if (JecnDrawMainPanel.getMainPanel() == null || desktopPane == null) {
			return;
		}
		if (desktopPane.getKeyHandler().isMouseDragged()) {// 鼠标事件不允许拖动
			return;
		}
		if (((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0)
				&& ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0)) {
			// 左右键同时按住时,不设置按下位置点
		} else {
			// 清空临时线段数据
			JecnWorkflowUtil.disposeLines();
			// 记录鼠标点击的点
			if (e.getSource() instanceof JecnManhattanLineResizePanel) {
				JecnManhattanLineResizePanel lineResizePanel = (JecnManhattanLineResizePanel) e.getSource();
				// 获取临时曼哈顿对象
				JecnManhattanLineData cloneLineData = lineResizePanel.getManhattanLine().clone().getFlowElementData();

				// 获取连接线克隆对象
				JecnBaseManhattanLinePanel cloneLinePanel = lineResizePanel.getManhattanLine().clone();
				// 创建虚拟线段
				JecnTempManLine tempLine = new JecnTempManLine(cloneLineData);
				// 临时线段记录拖动的曼哈顿线段对应的开始编辑点和结束编辑点
				tempLine.setStartFigure(cloneLinePanel.getStartFigure());
				tempLine.setEndFigure(cloneLinePanel.getEndFigure());

				// 流程架构图，添加连接线 ,设置拖动的端点
				MapManLineCommon.INSTANCE.setMapManLinePort(lineResizePanel, cloneLinePanel);
				// 点线
				tempLine.getFlowElementData().setBodyStyle(2);
				// 添加虚拟连接线到面板
				desktopPane.setJecnTempManLine(tempLine);
				draggedResizePanel = lineResizePanel;
				// 设置鼠标状态为连接线
				JecnDrawMainPanel.getMainPanel().getBoxPanel().setSelectedBtnType(MapElemType.CommonLine);
				undoData = new JecnUndoRedoData();
				// 记录撤销恢复数据
				undoData.recodeFlowElement(lineResizePanel.getManhattanLine());
			}
		}

	}

	/**
	 * 连接线脱线端点释放处理
	 * 
	 * @param e
	 */
	public void mouseResizeRelease(MouseEvent e) {

		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 主界面和面板是否存在
		if (desktopPane == null) {
			return;
		}
		if ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0 && e.getButton() == MouseEvent.BUTTON3) {// 右键释放且左键还处于按住状态
			return;
		}
		if ((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0 && e.getButton() == MouseEvent.BUTTON1) {// 左键释放且右键还处于按住状态
			return;
		}
		draggedResizePanel = null;

		// 面板图形显示编辑点
		JecnPaintFigureUnit.hideAllConnectPort();
		// 鼠标状态为指针
		JecnDrawMainPanel.getMainPanel().getBoxPanel().setSelectedBtnType(MapElemType.guide);

		JecnTempManLine tempLine = desktopPane.getJecnTempManLine();
		if (tempLine == null || !isDragged) {// 没有执行拖动
			// ***************5.右键菜单显示***************//
			// JecnFlowElementEventProcess.rightPopupMenuProcess(e);
			return;
		} else {
			isDragged = false;
		}

		JecnEditPortPanel startEditPanel = null;
		JecnEditPortPanel endEditPanel = null;

		if (e.getSource() instanceof JecnManhattanLineResizePanel) {
			JecnManhattanLineResizePanel lineResizePanel = (JecnManhattanLineResizePanel) e.getSource();

			// 拖动的线段临时点
			Point tmpPoint = tempLine.getEndTempPoint();
			/********* 【流程架构图 连接线脱离图形元素存在时，拖动连接线端点特殊处理】 **********/
			moveLineSetEditPort(e);
			mouseResizeReleaseMapLine(tmpPoint, lineResizePanel);

			if (desktopPane.getTempEditPort() != null) {

				// 流程架构图，拖动连接线端点处理
				MapManLineCommon.INSTANCE.mouseResizeReleaseMapLine(tmpPoint);
				// 画线结束编辑点恢复画线前状态
				new JecnEditPortEventProcess().lineMouseExited(desktopPane.getTempEditPort());

				switch (lineResizePanel.getReSizeLineType()) {
				case start:
					tempLine.setStartFigure(desktopPane.getTempEditPort().getBaseFigurePanel());
					startEditPanel = desktopPane.getTempEditPort();

					endEditPanel = tempLine.getEndFigure().getEditPortPanel(tempLine.getEndEditPointType(),
							tempLine.getEndFigure());

					break;
				case end:
					tempLine.setEndFigure(desktopPane.getTempEditPort().getBaseFigurePanel());

					startEditPanel = tempLine.getStartFigure().getEditPortPanel(tempLine.getStartEditPointType(),
							tempLine.getStartFigure());

					endEditPanel = desktopPane.getTempEditPort();
					break;

				default:
					break;
				}
				if (endEditPanel.equals(startEditPanel)) {// 开始点编辑点和结束点编辑点是否相同
					// 清空临时线段数据
					JecnWorkflowUtil.disposeLines();
					// 清空拖动连接线生成的临时编辑点
					desktopPane.setTempEditPort(null);
					return;
				}

				// **********圈内**********//
				// 鼠标在编辑上相对面板的坐标
				// Point newPoint = SwingUtilities.convertPoint(lineResizePanel,
				// e.getPoint(), desktopPane);
				if (!JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure()
						&& !startEditPanel.getBounds().contains(tmpPoint)
						&& !endEditPanel.getBounds().contains(tmpPoint)) {
					// 清空临时线段数据
					JecnWorkflowUtil.disposeLines();
					// 清空拖动连接线生成的临时编辑点
					desktopPane.setTempEditPort(null);
					desktopPane.repaint();
					return;
				}
				// **********圈内**********//

				// 删除曼哈顿线段，
				removeLineForDragged(lineResizePanel);

				// 设置曼哈顿线为未拖动状态，执行曼哈顿算法
				lineResizePanel.getManhattanLine().setDrraged(false);
				// 设置连接线开始图形和结束图形
				lineResizePanel.getManhattanLine().reSetEditFigure(startEditPanel, endEditPanel);
				if (!DrawCommon.isNullOrEmtryTrim(lineResizePanel.getManhattanLine().getFlowElementData()
						.getFigureText())) {// 是否存在连接线文本数据
					desktopPane.add(lineResizePanel.getManhattanLine().getTextPanel());
				}
				// 添加连接线到画图面板(// 重新生成新的曼哈顿线段)
				JecnAddFlowElementUnit.addManhattinLine(lineResizePanel.getManhattanLine());
				desktopPane.setTempEditPort(null);
				// 操作后数据
				redoData = new JecnUndoRedoData();
				// 记录撤销恢复数据
				redoData.recodeFlowElement(lineResizePanel.getManhattanLine());

				// 撤销恢复数据处理
				JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
				desktopPane.getUndoRedoManager().addEdit(undoProcess);
			}
			// 清空临时线段数据
			JecnWorkflowUtil.disposeLines();
			// 清空拖动连接线生成的临时编辑点
			desktopPane.setTempEditPort(null);
			desktopPane.setJecnTempManLine(null);
			draggedResizePanel = null;
		}
		// 刷新面板
		desktopPane.updateUI();
	}

	/**
	 * 移动连接线端点，判断临时端点是否允许为空
	 * 
	 * @param e
	 */
	private void moveLineSetEditPort(MouseEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 当前拖动连接线端点
		JecnManhattanLineResizePanel draggedResizePanel = (JecnManhattanLineResizePanel) e.getSource();

		if (desktopPane.getTempLine() != null || desktopPane.getJecnTempManLine() == null || draggedResizePanel == null) {// 不是连接线端点拖动
			return;
		}
		// 获取当前给定的连接线端点对应的图形的编辑点类型（此方法是为了解决通过点击连接线端点不能找到对应的图形编辑点类型）
		EditPointType editPointType = draggedResizePanel.getManhattanLine().getCurrEditPointType(draggedResizePanel);
		JecnEditPortPanel editPort = desktopPane.getTempEditPort();
		// 编辑点类型相同且是同一个图形,拖动不处理直接返回，解决拖动连接线端点和释放的编辑点是同一个时
		JecnBaseFigurePanel figure = draggedResizePanel.getManhattanLine().getCurrEditPointFigure(draggedResizePanel);
		if (editPort != null && editPort.getBaseFigurePanel() == figure && editPort.getEditPointType() == editPointType) {
			// 拖动线段端点记录临时端点
			desktopPane.setTempEditPort(null);
		}
	}

	private void mouseResizeReleaseMapLine(Point tmpPoint, JecnManhattanLineResizePanel lineResizePanel) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure()) {
			if (desktopPane.getTempEditPort() == null) {
				JecnBaseFigurePanel figure = MapManLineCommon.INSTANCE.craeteFigurePanel(tmpPoint);
				if (lineResizePanel.getReSizeLineType() == ReSizeLineType.start) {
					desktopPane.setTempEditPort(MapManLineCommon.INSTANCE.getEditPortPanel(lineResizePanel
							.getManhattanLine().getStartEditPointType(), figure));
				} else if (lineResizePanel.getReSizeLineType() == ReSizeLineType.end) {
					desktopPane.setTempEditPort(MapManLineCommon.INSTANCE.getEditPortPanel(lineResizePanel
							.getManhattanLine().getEndEditPointType(), figure));
				}
			}
		}
	}

	/**
	 * 拖动连接线点的，重新生成连线，需删除原有连线，重新生成
	 * 
	 * @param lineResizePanel
	 */
	private void removeLineForDragged(JecnManhattanLineResizePanel lineResizePanel) {
		// 删除曼哈顿线段，
		JecnRemoveFlowElementUnit.removeLine(lineResizePanel.getManhattanLine());
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		JecnBaseFigurePanel figurePanel = desktopPane.getTempEditPort().getBaseFigurePanel();
		if (figurePanel.getFlowElementData().getMapElemType() == MapElemType.MapLineFigure) {
			return;
		}
		if (lineResizePanel.getReSizeLineType() == ReSizeLineType.start) {// 移动开始点，删除开始点对应的虚拟元素
			if (lineResizePanel.getManhattanLine().getStartFigure().getFlowElementData().getMapElemType() == MapElemType.MapLineFigure) {
				JecnRemoveFlowElementUnit.removeFlowElement(lineResizePanel.getManhattanLine().getStartFigure());
			}
		} else if (lineResizePanel.getReSizeLineType() == ReSizeLineType.end) {
			if (lineResizePanel.getManhattanLine().getEndFigure().getFlowElementData().getMapElemType() == MapElemType.MapLineFigure) {
				JecnRemoveFlowElementUnit.removeFlowElement(lineResizePanel.getManhattanLine().getEndFigure());
			}
		}
	}

	/**
	 * 连接线鼠标拖动事件处理
	 * 
	 * 点击连接线显示连接线选中点
	 * 
	 * @param e
	 */
	public void mouseResizeDragged(MouseEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 主界面和面板是否存在
		if (desktopPane == null || desktopPane.getJecnTempManLine() == null) {
			return;
		}
		if ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0 && e.getButton() == MouseEvent.BUTTON3) {// 鼠标左键按住同时拖动时右键，右键事件拖动不执行
			return;
		}
		JecnTempManLine tempLine = desktopPane.getJecnTempManLine();

		if (e.getSource() instanceof JecnManhattanLineResizePanel) {
			JecnManhattanLineResizePanel lineResizePanel = (JecnManhattanLineResizePanel) e.getSource();
			if (!isDragged) {// 执行拖动唯一标识
				isDragged = true;
			}
			// 获取鼠标当前点坐标
			Point tempPoint = new Point(e.getX() + lineResizePanel.getLocation().x, e.getY()
					+ lineResizePanel.getLocation().y);

			// 拖动判断
			tempPoint = DraggedManLineUtil.draggLineEndPoint(lineResizePanel.getManhattanLine(), lineResizePanel
					.getReSizeLineType(), tempPoint);
			// 临时的另一个端点
			tempLine.setEndTempPoint(tempPoint);

			EditPointType tmpType = MapManLineCommon.INSTANCE.getEditPortByFigures(tempLine.getOriginalPoint());

			switch (lineResizePanel.getReSizeLineType()) {
			case start:// 拖动的是曼哈顿线的开始点
				if (JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure() && tmpType != null && desktopPane.getTempEditPort() != null) {
					tempLine.getFlowElementData().setStartEditPointType(tmpType);
					lineResizePanel.getManhattanLine().getFlowElementData().setStartEditPointType(tmpType);
					desktopPane.setTempEditPort(MapManLineCommon.INSTANCE.getEditPortPanel(tmpType, desktopPane
							.getTempEditPort().getBaseFigurePanel()));
				}
				// 路由算法画虚拟的曼哈顿线段
				tempLine.paintDraggedLinePort(tempLine.getOriginalPoint(), tempLine.getStartEditPointType(), tempLine
						.getOriginalEndPoint(), tempLine.getEndEditPointType());
				break;
			case end:// 拖动的是曼哈顿线的结束点
				if (JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure() && tmpType != null && desktopPane.getTempEditPort() != null) {
					tempLine.getFlowElementData().setEndEditPointType(tmpType);
					lineResizePanel.getManhattanLine().getFlowElementData().setEndEditPointType(tmpType);
					desktopPane.setTempEditPort(MapManLineCommon.INSTANCE.getEditPortPanel(tmpType, desktopPane
							.getTempEditPort().getBaseFigurePanel()));
				}
				// 路由算法画虚拟的曼哈顿线段
				tempLine.paintDraggedLinePort(tempLine.getOriginalStartPoint(), tempLine.getStartEditPointType(),
						tempLine.getOriginalPoint(), tempLine.getEndEditPointType());
				break;
			default:
				break;
			}
			JecnDrawMainPanel.getMainPanel().getWorkflow().getWorkflowZoomProcess().zoomManhattanLine(tempLine);
			// 拖动连接线结束点面板横竖滚动条跟随移动
			JecnEditPortEventProcess.draggedScrollPanel(tempPoint);
			JecnDrawMainPanel.getMainPanel().getWorkflow().updateUI();
		}
	}

	public static boolean isDragged() {
		return isDragged;
	}

	/**
	 * 首次添加连接线 设置连接线拖动状态
	 * 
	 * @param isDragged
	 */
	public static void setDragged(boolean isDragged) {
		JecnManLineResizeProcess.isDragged = isDragged;
		if (!isDragged) {
			draggedResizePanel = null;
		}
	}

	public static JecnManhattanLineResizePanel getDraggedResizePanel() {
		return draggedResizePanel;
	}

	public static void setDraggedResizePanel(JecnManhattanLineResizePanel draggedResizePanel) {
		JecnManLineResizeProcess.draggedResizePanel = draggedResizePanel;
	}
}
