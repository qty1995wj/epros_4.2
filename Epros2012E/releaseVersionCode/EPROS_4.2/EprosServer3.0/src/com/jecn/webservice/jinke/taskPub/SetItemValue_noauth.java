/**
 * SetItemValue_noauth.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class SetItemValue_noauth  implements java.io.Serializable {
    private java.lang.String userCode;

    private java.lang.String instanceId;

    private java.lang.String att_name;

    private java.lang.String att_url;

    public SetItemValue_noauth() {
    }

    public SetItemValue_noauth(
           java.lang.String userCode,
           java.lang.String instanceId,
           java.lang.String att_name,
           java.lang.String att_url) {
           this.userCode = userCode;
           this.instanceId = instanceId;
           this.att_name = att_name;
           this.att_url = att_url;
    }


    /**
     * Gets the userCode value for this SetItemValue_noauth.
     * 
     * @return userCode
     */
    public java.lang.String getUserCode() {
        return userCode;
    }


    /**
     * Sets the userCode value for this SetItemValue_noauth.
     * 
     * @param userCode
     */
    public void setUserCode(java.lang.String userCode) {
        this.userCode = userCode;
    }


    /**
     * Gets the instanceId value for this SetItemValue_noauth.
     * 
     * @return instanceId
     */
    public java.lang.String getInstanceId() {
        return instanceId;
    }


    /**
     * Sets the instanceId value for this SetItemValue_noauth.
     * 
     * @param instanceId
     */
    public void setInstanceId(java.lang.String instanceId) {
        this.instanceId = instanceId;
    }


    /**
     * Gets the att_name value for this SetItemValue_noauth.
     * 
     * @return att_name
     */
    public java.lang.String getAtt_name() {
        return att_name;
    }


    /**
     * Sets the att_name value for this SetItemValue_noauth.
     * 
     * @param att_name
     */
    public void setAtt_name(java.lang.String att_name) {
        this.att_name = att_name;
    }


    /**
     * Gets the att_url value for this SetItemValue_noauth.
     * 
     * @return att_url
     */
    public java.lang.String getAtt_url() {
        return att_url;
    }


    /**
     * Sets the att_url value for this SetItemValue_noauth.
     * 
     * @param att_url
     */
    public void setAtt_url(java.lang.String att_url) {
        this.att_url = att_url;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetItemValue_noauth)) return false;
        SetItemValue_noauth other = (SetItemValue_noauth) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userCode==null && other.getUserCode()==null) || 
             (this.userCode!=null &&
              this.userCode.equals(other.getUserCode()))) &&
            ((this.instanceId==null && other.getInstanceId()==null) || 
             (this.instanceId!=null &&
              this.instanceId.equals(other.getInstanceId()))) &&
            ((this.att_name==null && other.getAtt_name()==null) || 
             (this.att_name!=null &&
              this.att_name.equals(other.getAtt_name()))) &&
            ((this.att_url==null && other.getAtt_url()==null) || 
             (this.att_url!=null &&
              this.att_url.equals(other.getAtt_url())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserCode() != null) {
            _hashCode += getUserCode().hashCode();
        }
        if (getInstanceId() != null) {
            _hashCode += getInstanceId().hashCode();
        }
        if (getAtt_name() != null) {
            _hashCode += getAtt_name().hashCode();
        }
        if (getAtt_url() != null) {
            _hashCode += getAtt_url().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetItemValue_noauth.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">SetItemValue_noauth"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "userCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instanceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "instanceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("att_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "att_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("att_url");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "att_url"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
