package com.jecn.epros.domain.task;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 文控信息表从表 记录审核人名称和审核阶段名称
 *
 * @author ZHAGNXIAOHU
 * @date： 日期：Oct 30, 2012 时间：3:13:15 PM
 */
public class TaskHistoryFollow implements Serializable {
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 主键ID
     */
    private Long historyId;
    /**
     * 审核阶段名称
     */
    private String appellation;
    /**
     * 审核人名称
     */
    private String name;
    /**
     * 序号
     */
    private int sort;
    /**
     * 审批时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date approvalTime;
    /**
     * 任务审批阶段标识 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 6：各业务体系审批阶段 7：IT总监审批阶段
     */
    private Integer stageMark;

    public Integer getStageMark() {
        return stageMark;
    }

    public void setStageMark(Integer stageMark) {
        this.stageMark = stageMark;
    }

    public Date getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(Date approvalTime) {
        this.approvalTime = approvalTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public String getAppellation() {
        return appellation;
    }

    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
