package epros.draw.gui.figure.shape;

import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

public class PaintImplFigure {

	public JecnFigureData flowElementData = null;

	public PaintImplFigure(JecnBaseFigurePanel figurePanel) {
		flowElementData = figurePanel.getFlowElementData();
	}

	public void paintNoneFigure(Graphics2D g2d, int x1, int x2, int x3, int x4, int x5, int x6, int y1, int y2, int y3,
			int y4, int y5, int y6, int userWidth, int userHeight) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawPolygon(new int[] { x1, x2, x3, x4, x5, x6 }, new int[] { y1, y2, y3, y4, y5, y6 }, 6);
		g2d.drawRoundRect(0, 0, userWidth * 7 / 10, userHeight * 4 / 5, 15, 15);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintImplTop(Graphics2D g2d, int shadowCount, int type, int x1, int x2, int x3, int x4, int x5, int x6,
			int y1, int y2, int y3, int y4, int y5, int y6, int userWidth, int userHeight) {
		// 顶层 原图，榜值均为零
		if (type == 1) {
			// 填充六边形
			g2d.fillPolygon(
					new int[] { x1, x2, x3 - shadowCount, x4 - shadowCount, x5 - shadowCount, x6 - shadowCount },
					new int[] { y1, y2, y3, y4 - shadowCount, y5 - shadowCount, y6 - shadowCount }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			// 画六边形
			g2d.drawPolygon(new int[] { x1, x2 - shadowCount, x3 - shadowCount, x4 - shadowCount, x5 - shadowCount,
					x6 - shadowCount }, new int[] { y1 - shadowCount, y2, y3, y4 - shadowCount, y5 - shadowCount,
					y6 - shadowCount }, 6);
		} else {
			g2d.fillRoundRect(0, 0, userWidth * 7 / 10 - shadowCount, userHeight * 4 / 5 - shadowCount, 15, 15);
			g2d.setStroke(flowElementData.getBasicStroke());
			g2d.setPaint(flowElementData.getBodyColor());
			g2d.drawRoundRect(0, 0, userWidth * 7 / 10 - shadowCount, userHeight * 4 / 5 - shadowCount, 15, 15);
		}
	}

	/**
	 * 3D效果底层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时缩进值
	 * @param type
	 *            1:六边形
	 */
	public void paintImpl3DLow(Graphics2D g2d, int indent3Dvalue, int type, int x1, int x2, int x3, int x4, int x5,
			int x6, int y1, int y2, int y3, int y4, int y5, int y6, int userWidth, int userHeight) {
		if (type == 1) {// 六边形
			g2d.fillPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue, x4 - indent3Dvalue,
					x5 - indent3Dvalue, x6 - indent3Dvalue }, new int[] { y1 - indent3Dvalue, y2, y3,
					y4 - indent3Dvalue, y5 - indent3Dvalue, y6 - indent3Dvalue }, 6);
			g2d.setStroke(flowElementData.getBasicStroke());
		} else {// 矩形
			g2d.fillRoundRect(0, 0, userWidth * 7 / 10 - indent3Dvalue, userHeight * 4 / 5 - indent3Dvalue, 15, 15);
			g2d.setStroke(flowElementData.getBasicStroke());
		}
	}

	/**
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 * @param type
	 *            1:六边形
	 */
	public void paintImpl3DLine(Graphics2D g2d, int indent3Dvalue, int type, int x1, int x2, int x3, int x4, int x5,
			int x6, int y1, int y2, int y3, int y4, int y5, int y6, int userWidth, int userHeight) {
		if (type == 1) {// 六边形
			g2d.drawPolygon(new int[] { x1, x2 - indent3Dvalue, x3 - indent3Dvalue, x4 - indent3Dvalue,
					x5 - indent3Dvalue, x6 - indent3Dvalue }, new int[] { y1 - indent3Dvalue, y2, y3,
					y4 - indent3Dvalue, y5 - indent3Dvalue, y6 - indent3Dvalue }, 6);
		} else {// 矩形
			g2d.drawRoundRect(0, 0, userWidth * 7 / 10 - indent3Dvalue, userHeight * 4 / 5 - indent3Dvalue, 15, 15);
		}
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paintImpl3DPartTop(Graphics2D g2d, int type, int x1, int x2, int x3, int x4, int x5, int x6, int y1,
			int y2, int y3, int y4, int y5, int y6, int userWidth, int userHeight) {
		if (type == 1) {
			g2d.fillPolygon(new int[] { x1 + 5, x2 + 5, x3 - 5, x4 - 5, x5 - 5, x6 + 5 }, new int[] { y1, y2 + 5,
					y3 + 5, y4, y5 - 5, y6 - 5 }, 6);
		} else {
			g2d.fillRoundRect(5, 5, userWidth * 7 / 10 - 5 - 5, userHeight * 4 / 5 - 5 - 5, 15, 15);
		}
	}
}
