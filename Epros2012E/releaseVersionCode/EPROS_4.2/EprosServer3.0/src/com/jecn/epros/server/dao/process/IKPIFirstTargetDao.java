package com.jecn.epros.server.dao.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnKPIFirstTargetBean;
import com.jecn.epros.server.common.IBaseDao;
/***
 *  一级指标
 * @Time 2014-10-17S
 *
 */
public interface IKPIFirstTargetDao extends IBaseDao<JecnKPIFirstTargetBean, Long> {

	/***
	 * 添加一级指标
	 * @throws Exception
	 */
	public String addFirstTarget(List<JecnKPIFirstTargetBean> list) throws Exception;
	
	/**
	 * 获取所有  一级指标数据集合
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getAllFirstTarget() throws Exception;
	
	/**
	 * 获取 一级指标数据
	 * @return
	 * @throws Exception
	 */
	public JecnKPIFirstTargetBean getFirstTarget(String id) throws Exception;
}
