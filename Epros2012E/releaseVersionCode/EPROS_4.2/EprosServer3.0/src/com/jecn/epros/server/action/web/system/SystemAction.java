package com.jecn.epros.server.action.web.system;

import java.io.InputStream;

import javax.servlet.http.Cookie;

import org.apache.struts2.ServletActionContext;

import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnContants;

public class SystemAction extends BaseAction {
	
	/**
	 * 配置项的mark
	 */
	private String mark;
	
	/**
	 * @author yxw 2013-3-11
	 * @description:下载帮助文档
	 * @return
	 */
	public InputStream getDownloadHelp() {
		   return ServletActionContext.getServletContext().getResourceAsStream(
		     "/downLoad/help.chm");
		  }
	/**
	 * @author yxw 2013-3-11
	 * @description:下载32位jre
	 * @return
	 */
	public InputStream getDownloadJre32() {
		   return ServletActionContext.getServletContext().getResourceAsStream(
		     "/downLoad/jre-6u41-windows-i586.exe");
		  }
	/**
	 * @author yxw 2013-3-11
	 * @description:下载64位jre
	 * @return
	 */
	public InputStream getDownloadJre64() {
		   return ServletActionContext.getServletContext().getResourceAsStream(
		     "/downLoad/jre-6u41-windows-x64.exe");
		  }
	
	public void closeCompatibleTip() {
		try {
			Cookie comatibleTip = new Cookie("compatibleTipShow", "false");
			comatibleTip.setMaxAge(5*365*24*60*60);
			getResponse().addCookie(comatibleTip);
		} catch (Exception ex) {
			log.error("", ex);
		}
	}
	
	/**
	 * 根据mark获得value的值
	 */
	public void getConfigValueByMark(){
		if(mark==null){
			return;
		}
		String value=JecnContants.getValue(mark);
		outJsonString(value);
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}

}
