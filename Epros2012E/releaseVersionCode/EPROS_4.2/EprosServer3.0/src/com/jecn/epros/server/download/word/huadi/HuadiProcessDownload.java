package com.jecn.epros.server.download.word.huadi;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.download.HeaderDocBean;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.download.word.DownloadUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.graphic.RtfShape;
import com.lowagie.text.rtf.graphic.RtfShapePosition;
import com.lowagie.text.rtf.graphic.RtfShapeProperty;
import com.lowagie.text.rtf.table.RtfBorder;
import com.lowagie.text.rtf.table.RtfBorderGroup;
import com.lowagie.text.rtf.table.RtfCell;

/**
 * 华帝操作说明下载
 * 
 * @author ZHANGXH
 */
public class HuadiProcessDownload {

	public static String createWordFile(
			ProcessDownloadBean processDownloadBean, String path)
			throws Exception {
		// 流程名称
		if (processDownloadBean.getFlowName() == null) {
			return "";
		}
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);

		String filePath = "";
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			filePath = path;
		} else {
			filePath = JecnFinal.getAllTempPath(JecnFinal.saveTempFilePath());
		}

		FileOutputStream fileOutputStream = new FileOutputStream(filePath);
		RtfWriter2.getInstance(document, fileOutputStream);
		document.open();
		// 生成页眉
		HeaderDocBean headerDocBean = new HeaderDocBean();
		// 名称
		headerDocBean.setName(processDownloadBean.getFlowName());
		// 是否公开
		headerDocBean.setIsPublic(processDownloadBean.getFlowIsPublic());
		// 编号
		headerDocBean.setInputNum(processDownloadBean.getFlowInputNum());
		// 版本
		headerDocBean.setVersion(processDownloadBean.getFlowVersion());
		// 页眉图片
		Image loginImage = JecnFinal.getImage("logo01.jpg");
		// 生成页眉
		DownloadUtil.getHuadiHeaderDoc(document, headerDocBean, loginImage);
		// 生成页脚
		DownloadUtil.getFooterDoc(document);

		String hdgName = "Q/HDG";
		Paragraph paragraphName = DownloadUtil.ST_36(hdgName);
		// 设置右对齐
		paragraphName.setAlignment(Element.ALIGN_RIGHT);
		document.add(paragraphName);
		// 公司名称
		Paragraph companyName = DownloadUtil.ST_26(processDownloadBean
				.getCompanyName());
		companyName.setAlignment(Element.ALIGN_CENTER);
		document.add(companyName);

		// 添加分割线
		RtfShapePosition position = new RtfShapePosition(150, 0, 10400, 175);
		position.setXRelativePos(RtfShapePosition.POSITION_X_RELATIVE_MARGIN);
		position
				.setYRelativePos(RtfShapePosition.POSITION_Y_RELATIVE_PARAGRAPH);
		RtfShape shape = new RtfShape(RtfShape.SHAPE_RECTANGLE, position);
		RtfShapeProperty property = new RtfShapeProperty(
				RtfShapeProperty.PROPERTY_FILL_COLOR, Color.black);
		shape.setProperty(property);
		Paragraph p1 = new Paragraph(shape);
		document.add(p1);

		String newline = "\n\n\n\n\n\n\n";
		document.add(new Paragraph(newline));

		// 流程名称
		document.add(DownloadUtil.ST_24(processDownloadBean.getFlowName()));
		// 编制部门：
		String orgName = "";
		orgName = "\n\n\n\n\t\t\t"
				+ JecnUtil.getValue("preparationDepartmentC");
		if (!JecnCommon.isNullOrEmtryTrim(processDownloadBean.getOrgName())) {
			orgName += processDownloadBean.getOrgName();
		}
		document.add(DownloadUtil.ST_3(orgName));

		DownloadUtil.getReturnTable(document, processDownloadBean
				.getPeopleList(), processDownloadBean.getFlowInputNum(),
				processDownloadBean.getFlowVersion());
		// 流程
		DownloadUtil.huaDiTwoPage(document, processDownloadBean.getFlowName()
				+ JecnUtil.getValue("flow"), processDownloadBean
				.getCompanyName(), processDownloadBean.getFlowVersion(),
				processDownloadBean.getFlowInputNum());
		int count = 1;
		for (JecnConfigItemBean JecnConfigItemBean : processDownloadBean
				.getJecnConfigItemBean()) {
			if ("0".equals(JecnConfigItemBean.getValue())) {// 是否显示
				continue;
			}
			String titleName = count + "、" + JecnConfigItemBean.getName();
			if ("a1".equals(JecnConfigItemBean.getMark())) {
				// 1、目的
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowPurpose = "";
				if (processDownloadBean.getFlowPurpose() != null
						&& !"".equals(processDownloadBean.getFlowPurpose())) {
					flowPurpose = processDownloadBean.getFlowPurpose();
				} else {
					flowPurpose = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(flowPurpose));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a2".equals(JecnConfigItemBean.getMark())) {
				// 2、适用范围
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String applicability = "";
				if (processDownloadBean.getApplicability() != null
						&& !"".equals(processDownloadBean.getApplicability())) {
					applicability = processDownloadBean.getApplicability();
				} else {
					applicability = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(applicability));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a3".equals(JecnConfigItemBean.getMark())) {
				// 3、术语定义
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String nuotglOssary = "";
				if (processDownloadBean.getNoutGlossary() != null
						&& !"".equals(processDownloadBean.getNoutGlossary())) {
					nuotglOssary = processDownloadBean.getNoutGlossary();
				} else {
					nuotglOssary = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(nuotglOssary));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a4".equals(JecnConfigItemBean.getMark())) {// 驱动规则
				// 0是事件驱动;1是事件驱动
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				if (processDownloadBean.getFlowDriver().getDriveType() != null) {
					title.setSpacingBefore(15f);
					Table table = new Table(2);
					table.setWidths(new int[] { 10, 50 });
					table.setAlignment(Table.ALIGN_LEFT);
					table.setWidth(100f);
					String driverType = JecnUtil.getValue("drivingType");
					String driverRules = JecnUtil.getValue("drivingRules");
					Cell driverTypeCell = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(driverType));// str1=驱动类型
					driverTypeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(driverTypeCell, 0, 0);

					Cell driverRulesCell = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(driverRules));// qdgz="驱动规则";
					driverRulesCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(driverRulesCell, 1, 0);

					String driveType = "";
					if ("1".equals(processDownloadBean.getFlowDriver()
							.getDriveType().toString())) {
						driveType = JecnUtil.getValue("timeDrive");

					} else {
						driveType = JecnUtil.getValue("eventDriven");
					}
					table.addCell(DownloadUtil.huadi_ST_5_MIDDLE(driveType),
							new Point(0, 1));
					if ("1".equals(processDownloadBean.getFlowDriver()
							.getDriveType().toString())) {
						// 频率
						String frequency = JecnUtil.getValue("frequency");
						// 开始时间
						String startingTime = JecnUtil.getValue("startTime");
						// 结束时间
						String endingTime = JecnUtil.getValue("endTime");
						// 类型
						String type = JecnUtil.getValue("type");

						Cell frequencyCell = new Cell(DownloadUtil
								.huadi_ST_5_BOLD_MIDDLE(frequency));
						frequencyCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(frequencyCell, 2, 0);

						Cell startingTimeCell = new Cell(DownloadUtil
								.huadi_ST_5_BOLD_MIDDLE(startingTime));
						startingTimeCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(startingTimeCell, 3, 0);

						Cell endingTimeCell = new Cell(DownloadUtil
								.huadi_ST_5_BOLD_MIDDLE(endingTime));
						endingTimeCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(endingTimeCell, 4, 0);

						Cell typeCell = new Cell(DownloadUtil
								.huadi_ST_5_BOLD_MIDDLE(type));
						typeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(typeCell, 5, 0);
					}
					if ("1".equals(processDownloadBean.getFlowDriver()
							.getDriveType().toString())) {
						table.addCell(DownloadUtil
								.repaceAllInfo(processDownloadBean
										.getFlowDriver().getDriveRules()),
								new Point(1, 1));
						if ("1".equals(processDownloadBean.getFlowDriver()
								.getDriveType().toString())) {
							String dateType = "";
							if ("1".equals(processDownloadBean.getFlowDriver()
									.getQuantity())) { // 日
								dateType = JecnUtil.getValue("day");
							} else if ("2".equals(processDownloadBean
									.getFlowDriver().getQuantity())) { // 周
								dateType = JecnUtil.getValue("weeks");
							} else if ("3".equals(processDownloadBean
									.getFlowDriver().getQuantity())) { // 月
								dateType = JecnUtil.getValue("month");
							} else if ("4".equals(processDownloadBean
									.getFlowDriver().getQuantity())) { // 季
								dateType = JecnUtil.getValue("season");
							} else if ("5".equals(processDownloadBean
									.getFlowDriver().getQuantity())) { // 年
								dateType = JecnUtil.getValue("years");
							}
							table.addCell(DownloadUtil
									.huadi_ST_5_MIDDLE(processDownloadBean
											.getFlowDriver().getFrequency()),
									new Point(2, 1));
							table.addCell(DownloadUtil
									.huadi_ST_5_MIDDLE(processDownloadBean
											.getFlowDriver().getStartTime()),
									new Point(3, 1));
							table.addCell(DownloadUtil
									.huadi_ST_5_MIDDLE(processDownloadBean
											.getFlowDriver().getEndTime()),
									new Point(4, 1));
							table.addCell(DownloadUtil
									.huadi_ST_5_MIDDLE(dateType), new Point(5,
									1));
						}
					} else {
						table
								.addCell(
										DownloadUtil
												.huadi_ST_5_MIDDLE(DownloadUtil
														.changeNetToWordStyle(processDownloadBean
																.getFlowDriver()
																.getDriveRules())),
										new Point(1, 1));
					}
					title.add(table);
					document.add(title);
				} else {
					document.add(title);
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a5".equals(JecnConfigItemBean.getMark())) {
				// 流程输入
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowInput = "";
				if (processDownloadBean.getFlowInput() != null
						&& !"".equals(processDownloadBean.getFlowInput())) {
					flowInput = processDownloadBean.getFlowInput();
				} else {
					flowInput = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(flowInput));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a6".equals(JecnConfigItemBean.getMark())) {
				// 输出
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowOutput = "";
				if (processDownloadBean.getFlowOutput() != null
						&& !"".equals(processDownloadBean.getFlowOutput())) {
					flowOutput = processDownloadBean.getFlowOutput();
				} else {
					flowOutput = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(flowOutput));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a7".equals(JecnConfigItemBean.getMark())) {
				// 7关键活动
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				int paSize = 0;
				int ksfSize = 0;
				int kcpSize = 0;
				for (KeyActivityBean keyActivityBean : processDownloadBean
						.getKeyActivityShowList()) {
					if ("1".equals(keyActivityBean.getActiveKey())) {
						paSize++;
					} else if ("2".equals(keyActivityBean.getActiveKey())) {
						ksfSize++;
					} else if ("3".equals(keyActivityBean.getActiveKey())) {
						kcpSize++;
					}
				}
				if (paSize > 0 || ksfSize > 0 || kcpSize > 0) {
					Table table6 = null;
					table6 = new Table(5);
					table6.setWidths(new int[] { 15, 15, 25, 30, 20 });
					table6.setAlignment(Table.ALIGN_LEFT);
					table6.setWidth(100f);
					// 关键活动
					String gjhd = JecnUtil.getValue("keyActivities");
					// 活动编号
					String hdbh = JecnUtil.getValue("activitynumbers");
					// 活动名称
					String hdmc = JecnUtil.getValue("activityTitle");
					// 活动说明
					String hdsm = JecnUtil.getValue("activityIndicatingThat");
					// 关键说明
					String KeyDescription = JecnUtil.getValue("keyShow");
					// 问题区域
					String ProblemArea = JecnUtil.getValue("problemAreas");
					// 关键成功因素
					String KeySuccessFactor = JecnUtil
							.getValue("keySuccessFactors");
					// 关键控制点
					String KeyControlPoint = JecnUtil
							.getValue("keyControlPoint");
					
					Cell cell5_00 = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(gjhd));// 关键活动
					cell5_00.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_00, 0, 0);
					Cell cell5_01 = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(hdbh));// hdbh="活动编号";
					cell5_01.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_01, 0, 1);
					Cell cell5_02 = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(hdmc));// hdmc="活动名称";
					cell5_02.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_02, 0, 2);
					Cell cell5_03 = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(hdsm));// hdsm="活动说明";
					cell5_03.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_03, 0, 3);

					Cell cell5_04 = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(KeyDescription));
					cell5_04.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_04, 0, 4);

					Cell cell5_10 = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(ProblemArea));
					cell5_10.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_10, 1, 0);
					Cell cell5_20 = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(KeySuccessFactor));
					cell5_20.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (paSize > 0) {
						table6.addCell(cell5_20, paSize + 1, 0);
					}
					if (paSize == 0) {
						table6.addCell(cell5_20, 2, 0);
					}
					Cell cell5_30 = new Cell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(KeyControlPoint));
					cell5_30.setHorizontalAlignment(Element.ALIGN_CENTER);
					// 设置关键控制点标题
					if (paSize > 0 && ksfSize > 0) {
						table6.addCell(cell5_30, paSize + ksfSize + 1, 0);
					} else if (paSize > 0) {
						table6.addCell(cell5_30, paSize + 2, 0);
					} else if (ksfSize > 0) {
						table6.addCell(cell5_30, ksfSize + 2, 0);
					} else if (paSize == 0 && ksfSize == 0) {
						table6.addCell(cell5_30, 3, 0);
					}
					int table6i = 1;
					for (KeyActivityBean keyActivityBean : processDownloadBean
							.getKeyActivityShowList()) {
						// 问题区域
						if ("1".equals(keyActivityBean.getActiveKey())) {
							String activityShow = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShow());
							String activityShowControl = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShowControl());
							table6.addCell(keyActivityBean.getActivityNumber(),
									new Point(table6i, 1));
							table6.addCell(keyActivityBean.getActivityName(),
									new Point(table6i, 2));
							table6.addCell(activityShow, new Point(table6i, 3));

							table6.addCell(activityShowControl, new Point(
									table6i, 4));
							table6i++;
						}
					}
					if (paSize == 0) {
						table6i++;
					}
					for (KeyActivityBean keyActivityBean : processDownloadBean
							.getKeyActivityShowList()) {
						if ("2".equals(keyActivityBean.getActiveKey())) {
							String activityShow = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShow());
							String activityShowControl = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShowControl());
							table6.addCell(keyActivityBean.getActivityNumber(),
									new Point(table6i, 1));
							table6.addCell(keyActivityBean.getActivityName(),
									new Point(table6i, 2));
							table6.addCell(activityShow, new Point(table6i, 3));

							table6.addCell(activityShowControl, new Point(
									table6i, 4));
							table6i++;
						}
					}
					if (ksfSize == 0) {
						table6i++;
					}
					for (KeyActivityBean keyActivityBean : processDownloadBean
							.getKeyActivityShowList()) {
						if ("3".equals(keyActivityBean.getActiveKey())) {
							String activityShow = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShow());
							String activityShowControl = DownloadUtil
									.changeNetToWordStyle(keyActivityBean
											.getActivityShowControl());
							table6.addCell(keyActivityBean.getActivityNumber(),
									new Point(table6i, 1));
							table6.addCell(keyActivityBean.getActivityName(),
									new Point(table6i, 2));
							table6.addCell(activityShow, new Point(table6i, 3));
							table6.addCell(activityShowControl, new Point(
									table6i, 4));
							table6i++;
						}
					}
					if (kcpSize == 0) {
						table6i++;
					}
					if (paSize > 1) {
						cell5_10.setRowspan(paSize);
					}
					if (ksfSize > 1) {
						cell5_20.setRowspan(ksfSize);
					}
					if (kcpSize > 1) {
						cell5_30.setRowspan(kcpSize);
					}
					title.add(table6);
					document.add(title);
				} else {
					document.add(title);
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a8".equals(JecnConfigItemBean.getMark())) {
				// 8、角色职责
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getRoleList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 20, 80 };
					// 角色名称
					String roleName = JecnUtil.getValue("roleName");
					// 岗位名称
					String positionName = JecnUtil.getValue("positionName");
					// 角色职责
					String roleResponsibility = JecnUtil
							.getValue("responsibilities");
					List<String> roleTitleList = new ArrayList<String>();
					roleTitleList.add(roleName);
					roleTitleList.add(positionName);
					roleTitleList.add(roleResponsibility);
					Table table = DownloadUtil.getHuadiTable(tableWidth,
							tableInt, roleTitleList, processDownloadBean
									.getRoleList());
					document.add(table);
				} else {
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a9".equals(JecnConfigItemBean.getMark())) {
				// 9、流程图
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				// 流程图片
				// BufferedImage buffimg = null;
				// if (processDownloadBean.getImgPath() != null
				// && !"".equals(processDownloadBean.getImgPath())) {
				// File file = new File(JecnContants.jecn_path
				// + processDownloadBean.getImgPath());
				// if (file.exists()) {
				// buffimg = ImageIO.read(file);
				// // 获取图片的高度
				// int imgWidth = buffimg.getWidth();
				// // 获取图片的宽度
				// int imgHeight = buffimg.getHeight();
				// if (imgWidth > imgHeight) {
				// BufferedImage newImg = rotateImg(buffimg, 90);
				// buffimg = newImg;
				// }
				// }
				// }
				// if (buffimg != null) {
				// ByteArrayOutputStream imageStream = null;
				// try {
				// imageStream = new ByteArrayOutputStream();
				// ImageIO.write(buffimg, "jpg", imageStream);
				// byte[] tagInfo = imageStream.toByteArray();
				// Image iTextImg = Image.getInstance(tagInfo);
				// if (iTextImg != null) {
				// // 图像宽度大小
				// float imgWidth = iTextImg.getWidth();
				// float imgHeight = iTextImg.getHeight();
				// if (imgWidth != 0 && imgHeight != 0) {
				// int scale = 1;
				// // 获取页面宽度
				// float docWidth = document.getPageSize()
				// .getWidth() - 160;
				// // 获取页面高度
				// float docHeight = (int) document.getPageSize()
				// .getHeight() - 160;
				// int scaleWidth = (int) ((docWidth / imgWidth) * 100) + 1;
				// int scaleHeight = (int) ((docHeight / imgHeight) * 100) + 1;
				// if (scaleWidth > scaleHeight) {
				// scale = scaleHeight;
				// } else {
				// scale = scaleWidth;
				// }
				// // 图像放大缩小倍数
				// iTextImg.scalePercent(scale);
				// document.add(iTextImg);
				// document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				// document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				// }
				// }
				// } catch (Exception e) {
				// throw e;
				// } finally {
				// if (imageStream != null) {
				// imageStream.close();
				// }
				// }
				// }

				// // 流程图片
				Image img = null;
				if (processDownloadBean.getImgPath() != null
						&& !"".equals(processDownloadBean.getImgPath())) {
					img = JecnFinal.getImage(processDownloadBean.getImgPath());
				}
				if (img != null) {
					// 图像宽度大小
					float imgWidth = img.getWidth();
					float imgHeight = img.getHeight();
					if (imgWidth == 0 || imgHeight == 0) {
						// 除数不能为零
						continue;
					}
					float docWidth = document.getPageSize().getWidth() - 160;
					float docHeight = document.getPageSize().getHeight() - 200;
					int widhtScale = (int) ((docWidth / imgWidth) * 100) + 1;
					int heightScale = (int) ((docHeight / imgHeight) * 100) + 1;
					int scale = 0;
					if (widhtScale > heightScale) {
						scale = heightScale;
					} else {
						scale = widhtScale;
					}
					// 图像放大缩小倍数
					img.scalePercent(scale);
					document.add(img);
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				}
				count++;
			} else if ("a10".equals(JecnConfigItemBean.getMark())) {
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getAllActivityShowList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 15, 20, 20, 30, 30, 30 };
					if (JecnContants.otherOperaType == 7) { // 长春轨道 活动说明下载
						tableInt = new int[] { 20, 20, 20, 30, 30, 30, 20 };
					}
					// 活动编号
					String activityNumber = JecnUtil
							.getValue("activitynumbers");
					// 执行角色
					String executiveRole = JecnUtil.getValue("executiveRole");
					// 活动名称
					String eventTitle = JecnUtil.getValue("activityTitle");
					// 活动说明
					String activityDescription = JecnUtil
							.getValue("activityIndicatingThat");
					// 输入
					String Input = JecnUtil.getValue("input");
					// 输出
					String Output = JecnUtil.getValue("output");
					// 活动指标
					String activityIndicators = JecnUtil
							.getValue("activityIndicators");

					List<String> activityTitleList = new ArrayList<String>();
					activityTitleList.add(activityNumber);
					activityTitleList.add(executiveRole);
					activityTitleList.add(eventTitle);
					activityTitleList.add(activityDescription);
					activityTitleList.add(Input);
					activityTitleList.add(Output);
					if (JecnContants.otherOperaType == 7) { // 长春轨道 活动说明下载
						activityTitleList.add(activityIndicators);
					}
					Table table = DownloadUtil.getHuadiTable(tableWidth,
							tableInt, activityTitleList, processDownloadBean
									.getAllActivityShowList());
					document.add(table);
				} else {
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a11".equals(JecnConfigItemBean.getMark())) {
				// 11、流程记录
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);

				if (processDownloadBean.getProcessRecordList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 15, 50 };
					// 文件编号
					String fileNumber = JecnUtil.getValue("fileNumber");
					// 文件名称
					String fileName = JecnUtil.getValue("fileName");
					List<String> titleList = new ArrayList<String>();
					titleList.add(fileNumber);
					titleList.add(fileName);
					Table table = DownloadUtil.getHuadiTable(tableWidth,
							tableInt, titleList, processDownloadBean
									.getProcessRecordList());
					document.add(table);
				} else {
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a12".equals(JecnConfigItemBean.getMark())) {
				// 11、流程记录
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);

				if (processDownloadBean.getOperationTemplateList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 15, 50 };
					// 文件编号
					String fileNumber = JecnUtil.getValue("fileNumber");
					// 文件名称
					String fileName = JecnUtil.getValue("fileName");
					List<String> titleList = new ArrayList<String>();
					titleList.add(fileNumber);
					titleList.add(fileName);
					Table table = DownloadUtil.getHuadiTable(tableWidth,
							tableInt, titleList, processDownloadBean
									.getOperationTemplateList());
					document.add(table);
				} else {
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a13".equals(JecnConfigItemBean.getMark())) {
				// 13、相关流程
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				int upSize = 0;
				int lowSize = 0;
				int supSize = 0;
				int implSize = 0;
				for (RelatedProcessBean relatedProcessBean : processDownloadBean
						.getRelatedProcessList()) {
					if ("1".equals(relatedProcessBean.getLinecolor())) {
						upSize++;
					} else if ("2".equals(relatedProcessBean.getLinecolor())) {
						lowSize++;
					} else if ("3".equals(relatedProcessBean.getLinecolor())) {
						implSize++;
					} else if ("4".equals(relatedProcessBean.getLinecolor())) {
						supSize++;
					}
				}
				// int upSize = processBaseInfoBean.getListUpperFlows().size();
				// int lowSize = processBaseInfoBean.getListLowerFlows().size();
				// int supSize = processBaseInfoBean.getListSubFlows().size();
				// int implSize = processBaseInfoBean.getListImplFlows().size();

				if (upSize > 0 || lowSize > 0 || supSize > 0 || implSize > 0) {
					Table table13 = null;
					table13 = new Table(3);
					table13.setWidths(new int[] { 15, 10, 50 });
					table13.setWidth(100f);

					RtfCell cellDotted13 = new RtfCell("Dotted border");
					cellDotted13.setBorders(new RtfBorderGroup(Rectangle.BOX,
							RtfBorder.BORDER_DOTTED, 1, new Color(0, 0, 0)));
					table13.addCell(DownloadUtil.contentTitle(""), new Point(0,
							0));
					// 流程名称
					String ProcessName = JecnUtil.getValue("processName");
					// 流程编号
					String ProcessId = JecnUtil.getValue("processNumber");
					table13
							.addCell(DownloadUtil
									.huadi_ST_5_BOLD_MIDDLE(ProcessId),
									new Point(0, 1));
					table13.addCell(DownloadUtil
							.huadi_ST_5_BOLD_MIDDLE(ProcessName), new Point(0,
							2));
					int table13i = 1;
					if (upSize > 0) {
						// 上游流程
						String flowUpper = JecnUtil.getValue("upstreamProcess");
						Cell cell0_131 = new Cell(DownloadUtil
								.huadi_ST_5_BOLD_MIDDLE(flowUpper));
						cell0_131.setHorizontalAlignment(Element.ALIGN_CENTER);
						table13.addCell(cell0_131, 1, 0);
						cell0_131.setRowspan(upSize);

						for (RelatedProcessBean relatedProcessBean : processDownloadBean
								.getRelatedProcessList()) {
							if ("1".equals(relatedProcessBean.getLinecolor())) {
								table13.addCell(DownloadUtil
										.huadi_ST_5_MIDDLE(relatedProcessBean
												.getFlowNumber()), new Point(
										table13i, 1));
								table13.addCell(DownloadUtil
										.huadi_ST_5_MIDDLE(relatedProcessBean
												.getFlowName()), new Point(
										table13i, 2));
								table13i++;
							}
						}
					}
					if (lowSize > 0) {
						// 下游流程
						String flowLower = JecnUtil
								.getValue("downstreamProcess");
						Cell cell0_131 = new Cell(DownloadUtil
								.huadi_ST_5_BOLD_MIDDLE(flowLower));
						cell0_131.setHorizontalAlignment(Element.ALIGN_CENTER);
						table13.addCell(cell0_131, upSize + 1, 0);
						cell0_131.setRowspan(lowSize);
						for (RelatedProcessBean relatedProcessBean : processDownloadBean
								.getRelatedProcessList()) {
							if ("2".equals(relatedProcessBean.getLinecolor())) {
								table13.addCell(DownloadUtil
										.huadi_ST_5_MIDDLE(relatedProcessBean
												.getFlowNumber()), new Point(
										table13i, 1));
								table13.addCell(DownloadUtil
										.huadi_ST_5_MIDDLE(relatedProcessBean
												.getFlowName()), new Point(
										table13i, 2));
								table13i++;
							}
						}
					}
					if (supSize > 0) {
						// 子流程
						String flowLower = JecnUtil.getValue("childProcess");
						Cell cell0_131 = new Cell(DownloadUtil
								.huadi_ST_5_BOLD_MIDDLE(flowLower));
						cell0_131.setHorizontalAlignment(Element.ALIGN_CENTER);
						table13.addCell(cell0_131, lowSize + upSize + 1, 0);
						cell0_131.setRowspan(supSize);
						for (RelatedProcessBean relatedProcessBean : processDownloadBean
								.getRelatedProcessList()) {
							if ("4".equals(relatedProcessBean.getLinecolor())) {
								table13.addCell(DownloadUtil
										.huadi_ST_5_MIDDLE(relatedProcessBean
												.getFlowNumber()), new Point(
										table13i, 1));
								table13.addCell(DownloadUtil
										.huadi_ST_5_MIDDLE(relatedProcessBean
												.getFlowName()), new Point(
										table13i, 2));
								table13i++;
							}
						}
					}
					if (implSize > 0) {
						// 过程流程
						String flowLower = JecnUtil.getValue("processFlow");
						Cell cell0_131 = new Cell(DownloadUtil
								.huadi_ST_5_BOLD_MIDDLE(flowLower));
						cell0_131.setHorizontalAlignment(Element.ALIGN_CENTER);
						table13.addCell(cell0_131, supSize + lowSize + upSize
								+ 1, 0);
						cell0_131.setRowspan(implSize);
						for (RelatedProcessBean relatedProcessBean : processDownloadBean
								.getRelatedProcessList()) {
							if ("3".equals(relatedProcessBean.getLinecolor())) {
								table13.addCell(DownloadUtil
										.huadi_ST_5_MIDDLE(relatedProcessBean
												.getFlowNumber()), new Point(
										table13i, 1));
								table13.addCell(DownloadUtil
										.huadi_ST_5_MIDDLE(relatedProcessBean
												.getFlowName()), new Point(
										table13i, 2));
								table13i++;
							}
						}
					}
					document.add(table13);
				} else {
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a14".equals(JecnConfigItemBean.getMark())) {
				// 14、相关制度
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getRuleNameList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 80, 20 };
					// 制度名称
					String name = JecnUtil.getValue("ruleName");
					// 类型
					String type = JecnUtil.getValue("type");
					List<String> ruleTitleList = new ArrayList<String>();
					ruleTitleList.add(name);
					ruleTitleList.add(type);
					Table table = DownloadUtil.getHuadiTable(tableWidth,
							tableInt, ruleTitleList, processDownloadBean
									.getRuleNameList());
					document.add(table);
				} else {
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a15".equals(JecnConfigItemBean.getMark())) {
				// 15、流程关键测评指标板
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getFlowKpiList().size() > 0) {
					for (Object[] obj : processDownloadBean.getFlowKpiList()) {
						Object objStr = obj[1];
						if (objStr != null
								&& Integer.valueOf(objStr.toString()) == 1) {
							// 过程性指标
							obj[1] = JecnUtil.getValue("efficiencyIndex");
						} else if (objStr != null
								&& Integer.valueOf(objStr.toString()) == 0) {
							// 结果性指标
							obj[1] = JecnUtil.getValue("effectIndex");
						} else {
							// 无
							obj[1] = JecnUtil.getValue("none");
						}
					}
					float tableWidth = 100f;
					int[] tableInt = new int[] { 20, 10, 25, 30, 15 };
					// 名称
					String name = JecnUtil.getValue("name");
					// 类型
					String type = JecnUtil.getValue("type");
					// KPI定义
					String KpiDefinition = JecnUtil.getValue("KPIDefinition");
					// 统计方法
					String statisticsMethod = JecnUtil
							.getValue("statisticalMethod");
					// 目标值
					String targetValue = JecnUtil.getValue("targetValue");
					List<String> kpiTitleList = new ArrayList<String>();
					kpiTitleList.add(name);
					kpiTitleList.add(type);
					kpiTitleList.add(KpiDefinition);
					kpiTitleList.add(statisticsMethod);
					kpiTitleList.add(targetValue);
					Table table = DownloadUtil.getHuadiTable(tableWidth,
							tableInt, kpiTitleList, processDownloadBean
									.getFlowKpiList());
					document.add(table);
				} else {
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a16".equals(JecnConfigItemBean.getMark())) {
				// 16、客户
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowCustom = "";
				if (processDownloadBean.getFlowCustom() != null
						&& !"".equals(processDownloadBean.getFlowCustom())) {
					flowCustom = processDownloadBean.getFlowCustom();
				} else {
					flowCustom = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(flowCustom));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a17".equals(JecnConfigItemBean.getMark())) {
				// 17、不适用范围
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String noApplicability = "";
				if (processDownloadBean.getNoApplicability() != null
						&& !"".equals(processDownloadBean.getNoApplicability())
						&& !"&nbsp".equals(processDownloadBean
								.getNoApplicability())) {
					noApplicability = processDownloadBean.getNoApplicability();
				} else {
					noApplicability = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(noApplicability));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a18".equals(JecnConfigItemBean.getMark())) {
				// 18、概况信息
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowSummarize = "";
				if (processDownloadBean.getFlowSummarize() != null
						&& !"".equals(processDownloadBean.getFlowSummarize())
						&& !"&nbsp".equals(processDownloadBean
								.getFlowSummarize())) {
					flowSummarize = processDownloadBean.getFlowSummarize();
				} else {
					flowSummarize = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(flowSummarize));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a19".equals(JecnConfigItemBean.getMark())) {
				// 19、补充说明
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowSupplement = "";
				if (processDownloadBean.getFlowSupplement() != null
						&& !"".equals(processDownloadBean.getFlowSupplement())
						&& !"&nbsp".equals(processDownloadBean
								.getFlowSupplement())) {
					flowSupplement = processDownloadBean.getFlowSupplement();
				} else {
					flowSupplement = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(flowSupplement));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a20".equals(JecnConfigItemBean.getMark())) {
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getFlowRecordList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 15, 20, 20, 30, 30, 30 };
					// 记录名称
					String recordName = JecnUtil.getValue("recordName");
					// 保存责任人
					String saveResponsiblePersons = JecnUtil
							.getValue("saveResponsiblePersons");
					// 保存场所
					String savePlace = JecnUtil.getValue("savePlace");
					// 归档时间
					String filingTime = JecnUtil.getValue("filingTime");
					// 保存期限
					String expirationDate = JecnUtil.getValue("storageLife");
					// 到期处理方式
					String handlingDue = JecnUtil.getValue("treatmentDue");
					List<String> recordTitleList = new ArrayList<String>();
					recordTitleList.add(recordName);
					recordTitleList.add(saveResponsiblePersons);
					recordTitleList.add(savePlace);
					recordTitleList.add(filingTime);
					recordTitleList.add(expirationDate);
					recordTitleList.add(handlingDue);
					Table table = DownloadUtil.getHuadiTable(tableWidth,
							tableInt, recordTitleList, processDownloadBean
									.getFlowRecordList());
					document.add(table);
				} else {
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a21".equals(JecnConfigItemBean.getMark())) { // 相关标准
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getStandardList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 80, 20 };
					// 标准名称
					String name = JecnUtil.getValue("standardName");
					// 类型
					String type = JecnUtil.getValue("type");
					List<String> standTitleList = new ArrayList<String>();
					standTitleList.add(name);
					standTitleList.add(type);
					Table table = DownloadUtil.getHuadiTable(tableWidth,
							tableInt, standTitleList, processDownloadBean
									.getStandardList());
					document.add(table);
				} else {
					document.add(DownloadUtil.huadi_ST_12_MIDDLE(JecnUtil
							.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a25".equals(JecnConfigItemBean.getMark())) {// 相关风险
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				if (processDownloadBean.getRilsList().size() > 0) {
					float tableWidth = 100f;
					int[] tableInt = new int[] { 15, 50 };
					// 风险编号
					String riskCode = JecnUtil.getValue("riskNum");
					// 风险描述
					String riskName = JecnUtil.getValue("riskName");
					List<String> titleList = new ArrayList<String>();
					titleList.add(riskCode);
					titleList.add(riskName);
					// 风险数据
					List<Object[]> list = new ArrayList<Object[]>();
					for (JecnRisk jecnRisk : processDownloadBean.getRilsList()) {
						String[] obj = new String[2];
						obj[0] = jecnRisk.getRiskCode();
						obj[1] = jecnRisk.getRiskName();
						list.add(obj);
					}
					Table table = DownloadUtil.getHuadiTable(tableWidth, tableInt,
							titleList, list);
					document.add(table);
				} else {
					document.add(DownloadUtil.getContent(JecnUtil.getValue("none")));
				}
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a26".equals(JecnConfigItemBean.getMark())) {//  相关文件
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowRelatedFile = "";
				if (processDownloadBean.getFlowRelatedFile() != null
						&& !"".equals(processDownloadBean.getFlowRelatedFile())) {
					flowRelatedFile = processDownloadBean.getFlowRelatedFile();
				} else {
					flowRelatedFile =  JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.getContent(flowRelatedFile));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a27".equals(JecnConfigItemBean.getMark())) {// 自定义要素1
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowCustomOne = "";
				if (processDownloadBean.getFlowCustomOne() != null
						&& !"".equals(processDownloadBean.getFlowCustomOne())) {
					flowCustomOne = processDownloadBean.getFlowCustomOne();
				} else {
					flowCustomOne =  JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.getContent(flowCustomOne));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a28".equals(JecnConfigItemBean.getMark())) {// 自定义要素2
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowCustomTwo = "";
				if (processDownloadBean.getFlowCustomTwo() != null
						&& !"".equals(processDownloadBean.getFlowCustomTwo())) {
					flowCustomTwo = processDownloadBean.getFlowCustomTwo();
				} else {
					flowCustomTwo = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.getContent(flowCustomTwo));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a29".equals(JecnConfigItemBean.getMark())) {// 自定义要素3
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowCustomThree = "";
				if (processDownloadBean.getFlowCustomThree() != null
						&& !"".equals(processDownloadBean.getFlowCustomThree())) {
					flowCustomThree = processDownloadBean.getFlowCustomThree();
				} else {
					flowCustomThree = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.getContent(flowCustomThree));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a30".equals(JecnConfigItemBean.getMark())) {// 自定义要素4
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowCustomFore = "";
				if (processDownloadBean.getFlowCustomFour() != null
						&& !"".equals(processDownloadBean.getFlowCustomFour())) {
					flowCustomFore = processDownloadBean.getFlowCustomFour();
				} else {
					flowCustomFore =  JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.getContent(flowCustomFore));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			} else if ("a31".equals(JecnConfigItemBean.getMark())) {// 自定义要素5
				Paragraph title = DownloadUtil.huadi_BODY_12_MIDDLE(titleName);
				document.add(title);
				String flowCustomFive = "";
				if (processDownloadBean.getFlowCustomFive() != null
						&& !"".equals(processDownloadBean.getFlowCustomFive())) {
					flowCustomFive = processDownloadBean.getFlowCustomFive();
				} else {
					flowCustomFive = JecnUtil.getValue("none");
				}
				document.add(DownloadUtil.getContent(flowCustomFive));
				document.add(DownloadUtil.huadi_ST_12_MIDDLE(""));
				count++;
			}
		}
		document.close();
		fileOutputStream.close();
		return filePath;
	}

	private static BufferedImage rotateImg(BufferedImage image, int degree)
			throws IOException {

		int iw = image.getWidth();// 原始图象的宽度
		int ih = image.getHeight();// 原始图象的高度
		int w = 0;
		int h = 0;
		int x = 0;
		int y = 0;
		degree = degree % 360;
		if (degree < 0)
			degree = 360 + degree;// 将角度转换到0-360度之间
		double ang = Math.toRadians(degree);// 将角度转为弧度
		/**
		 * 确定旋转后的图象的高度和宽度
		 */
		if (degree == 180 || degree == 0 || degree == 360) {
			w = iw;
			h = ih;
		} else if (degree == 90 || degree == 270) {
			w = ih;
			h = iw;
		} else {
			int d = iw + ih;
			w = (int) (d * Math.abs(Math.cos(ang)));
			h = (int) (d * Math.abs(Math.sin(ang)));
		}

		x = (w / 2) - (iw / 2);// 确定原点坐标
		y = (h / 2) - (ih / 2);
		BufferedImage rotatedImage = new BufferedImage(w, h, image.getType());
		AffineTransform at = new AffineTransform();
		at.rotate(ang, w / 2, h / 2);// 旋转图象
		at.translate(x, y);
		AffineTransformOp op = new AffineTransformOp(at,
				AffineTransformOp.TYPE_BICUBIC);
		op.filter(image, rotatedImage);
		image = rotatedImage;
		return image;
	}
}
