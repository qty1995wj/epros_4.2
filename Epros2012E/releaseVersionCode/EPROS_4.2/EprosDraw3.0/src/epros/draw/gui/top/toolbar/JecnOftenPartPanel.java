package epros.draw.gui.top.toolbar;

import java.awt.GridBagConstraints;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnActionCommandConstants;
import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏中常用面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnOftenPartPanel extends JecnPanel {
	/** 保存 */
	protected JecnToolbarContentButton saveBtn = null;
	/** 另存为 */
	protected JecnToolbarContentButton saveAsBtn = null;
	/** 另存为图片 */
	protected JecnToolbarContentButton saveAsImageBtn = null;

	/** 撤销 */
	protected JecnToolbarContentButton undoBtn = null;
	/** 恢复 */
	protected JecnToolbarContentButton redoBtn = null;
	/** 当前画板：放大 */
	protected JecnToolbarContentButton zoomBigBtn = null;
	/** 当前画板：缩小 */
	protected JecnToolbarContentButton zoomSmallBtn = null;
	/** 当前画板：原始视图 */
	protected JecnToolbarContentButton originalBtn = null;
	/** 当前画板：放大缩小下拉选择控件 当前比例标签 */
	protected JLabel zoomLabel = null;
	/** 当前画板：放大缩小下拉选择控件 */
	protected JComboBox zoomComboBox = null;

	/** 工具栏对象 */
	private JecnToolBarPanel toolBarPanel = null;

	/** 放大缩小下拉框的选项值 */
	private String[] zoomComboBoxItems = new String[] { "25%", "50%", "75%", "100%", "125%", "150%", "175%", "200%",
			"225%", "250%", "275%", "300%", "325%", "350%", "375%", "400%" };

	/** 100%对应的序号 */
	private int zoomComboBoxDeaultIndex = 2;

	public JecnOftenPartPanel(JecnToolBarPanel toolBarPanel) {
		if (toolBarPanel == null) {
			JecnLogConstants.LOG_OFTEN_PART_PANEL.error("JecnOftenPartPanel类构造函数：toolBarPanel参数为NUll");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.toolBarPanel = toolBarPanel;
		initOftenPanel();
		initOneLayout();
	}

	/**
	 * 
	 * 
	 * 初始化经常使用控件
	 * 
	 */
	private void initOftenPanel() {
		// 设置边框
		this.setBorder(JecnUIUtil.getTootBarBorder());

		// 保存
		saveBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.oftenSave), ToolBarElemType.oftenSave);
		// 另存为
		saveAsBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.oftenSaveAS), ToolBarElemType.oftenSaveAS);
		// 导出为图片
		saveAsImageBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.oftenSaveImage), ToolBarElemType.oftenSaveImage);
		// 撤销
		undoBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.oftenUndo), ToolBarElemType.oftenUndo);
		// 恢复
		redoBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.oftenRedo), ToolBarElemType.oftenRedo);

		// 当前画板：放大
		zoomBigBtn = new JecnToolbarContentButton(null, ToolBarElemType.oftenZoomBig);

		// 当前画板：缩小
		zoomSmallBtn = new JecnToolbarContentButton(null, ToolBarElemType.oftenZoomSmall);
		// 当前画板：原始视图
		originalBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.oftenOriginal), ToolBarElemType.oftenOriginal);
		// 当前画板：放大缩小下拉选择控件
		zoomComboBox = new JComboBox(zoomComboBoxItems);

		// 当前显示比例
		zoomLabel = new JLabel();

		// 初始值
		setZoomComboBoxDefaultItem();

		// 默认可编辑
		zoomComboBox.setEditable(true);

		// 默认不可编辑
		undoBtn.setEnabled(false);
		redoBtn.setEnabled(false);
		saveBtn.setEnabled(false);
		saveAsBtn.setEnabled(false);
		saveAsImageBtn.setEnabled(false);
		zoomBigBtn.setEnabled(false);
		zoomSmallBtn.setEnabled(false);
		originalBtn.setEnabled(false);
		zoomComboBox.setEnabled(false);
		zoomLabel.setEnabled(false);

		// 提示信息
		// 保存 Ctrl+S
		toolBarPanel.setToolBarBtnToolTipText(saveBtn, "Ctlr+S", "");
		// 另存为 Ctrl+Shift+A
		toolBarPanel.setToolBarBtnToolTipText(saveAsBtn, "Ctrl+Shift+A", "");
		// 导出为图像 Ctrll+Shift+G
		toolBarPanel.setToolBarBtnToolTipText(saveAsImageBtn, "Ctrl+Shift+G", "");
		// 撤销 Ctrl+Z
		toolBarPanel.setToolBarBtnToolTipText(undoBtn, "Ctrl+Z", "");
		// 恢复 Ctrl+Y
		toolBarPanel.setToolBarBtnToolTipText(redoBtn, "Ctrl+Y", "");

		toolBarPanel.setToolBarBtnToolTipText(zoomBigBtn, "Ctlr+", "");
		toolBarPanel.setToolBarBtnToolTipText(zoomSmallBtn, "Ctlr-", "");

		// 事件
		zoomComboBox.addActionListener(JecnDrawMainPanel.getMainPanel());
		zoomComboBox.setActionCommand(JecnActionCommandConstants.ZOOM_COMBO_BOX);
	}

	/**
	 * 
	 * 第一层布局：工具栏标题区、分隔符、工具栏内容区
	 * 
	 */
	private void initOneLayout() {

		// 另保存
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0(), 0, 0);
		this.add(saveAsBtn.getJToolBar(), c);

		// 另存为图片
		c = new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		this.add(saveAsImageBtn.getJToolBar(), c);

		// 分隔符
		c = new GridBagConstraints(3, 0, 1, 2, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(new JSeparator(SwingConstants.VERTICAL), c);

		// 保存
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		this.add(saveBtn.getJToolBar(), c);

		// 撤销
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		this.add(undoBtn.getJToolBar(), c);

		// 恢复
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets0(), 0, 0);
		this.add(redoBtn.getJToolBar(), c);

		// 视图面板
		JecnPanel zoomPanel = initZoomComponents();
		c = new GridBagConstraints(4, 0, 1, 2, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, JecnUIUtil
				.getInsets0(), 0, 0);
		this.add(zoomPanel, c);
	}

	/**
	 * 
	 * 视图面板初始化
	 * 
	 * @return
	 */
	private JecnPanel initZoomComponents() {
		// 视图面板
		JecnPanel zoomPanel = new JecnPanel();

		// 视图上半操作面板
		JecnPanel topPanel = new JecnPanel();
		// 视图下半操作面板
		JecnPanel buttonPanel = new JecnPanel();

		zoomLabel.setText(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.oftenZoomLabel.toString()));

		// 上半面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.BOTH, JecnUIUtil.getInsets0(), 0, 0);
		zoomPanel.add(topPanel, c);
		// 下班面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		zoomPanel.add(buttonPanel, c);

		/** ***************上半面板**************** */
		// 当前显示比例
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, JecnUIUtil
				.getInsets3(), 0, 0);
		topPanel.add(zoomLabel, c);
		// 当前画板：放大缩小下拉选择控件
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0303(), 0, 0);
		topPanel.add(zoomComboBox, c);

		/** ***************上半面板**************** */

		/** ***************下半面板**************** */
		// 当前画板：原始视图
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		buttonPanel.add(originalBtn.getJToolBar(), c);

		// 当前画板：放大
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		buttonPanel.add(zoomBigBtn.getJToolBar(), c);

		// 当前画板：缩小
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		buttonPanel.add(zoomSmallBtn.getJToolBar(), c);
		/** ***************下半面板**************** */
		return zoomPanel;
	}

	/**
	 * 
	 * 设置当前画图面板的放大倍数
	 * 
	 */
	public void setZoomComboBoxVaule() {

		double spaceDouble = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale() * 100;
		// 获取视图内容
		JTextField textField = (JTextField) this.zoomComboBox.getEditor().getEditorComponent();
		// 放大倍数
		String text = ((int) spaceDouble) + "%";

		if (text.equals(textField.getText())) {// 给定的倍数不等于下拉框显示的倍数，重置倍数
			return;
		}
		// 设置转换后的值
		textField.setText(text);
	}

	/**
	 * 
	 * 当前选中流程图或流程地图时显示
	 * 
	 */
	public void currWorkflowNotNullEnabledTrue() {
		if (JecnSystemStaticData.isEprosDesigner()) {
			// 打开（导入）
			JecnToolbarContentButton openMapBtn = toolBarPanel.getFileToolBarItemPanel().getOpenMapBtn();
			if (openMapBtn != null && !openMapBtn.isEnabled()) {// 打开
				openMapBtn.setEnabled(true);
			}
		}
		if (!this.saveBtn.isEnabled()) {// 保存
			this.saveBtn.setEnabled(true);
		}

		if (!this.saveAsBtn.isEnabled()) {// 另存为
			this.saveAsBtn.setEnabled(true);
		}

		if (!this.saveAsImageBtn.isEnabled()) {// 保存为图片
			this.saveAsImageBtn.setEnabled(true);
		}

		if (!this.zoomBigBtn.isEnabled()) {// 放大按钮
			this.zoomBigBtn.setEnabled(true);
		}

		if (!this.zoomSmallBtn.isEnabled()) {// 缩小按钮
			this.zoomSmallBtn.setEnabled(true);
		}

		if (!this.zoomComboBox.isEnabled()) {// 放大缩小视图
			setZoomComboBoxVaule();
			this.zoomComboBox.setEnabled(true);
			// 当前画板：放大缩小下拉选择控件 当前比例标签
			this.zoomLabel.setEnabled(true);
		}

		if (!this.originalBtn.isEnabled()) {// 原始视图
			this.originalBtn.setEnabled(true);
		}

	}

	/**
	 * 
	 * 没有当前选中画图面板时不显示
	 * 
	 */
	public void currWorkflowNotNullEnabledFalse() {
		if (JecnSystemStaticData.isEprosDesigner()) {
			// 打开（导入）
			JecnToolbarContentButton openMapBtn = toolBarPanel.getFileToolBarItemPanel().getOpenMapBtn();
			if (openMapBtn != null && openMapBtn.isEnabled()) {// 打开
				openMapBtn.setEnabled(false);
			}
		}

		if (this.saveBtn.isEnabled()) {// 保存
			this.saveBtn.setEnabled(false);
		}

		if (this.saveAsBtn.isEnabled()) {// 另存为
			this.saveAsBtn.setEnabled(false);
		}

		if (this.saveAsImageBtn.isEnabled()) {// 保存为图片
			this.saveAsImageBtn.setEnabled(false);
		}

		if (this.zoomBigBtn.isEnabled()) {// 放大按钮
			this.zoomBigBtn.setEnabled(false);
		}

		if (this.zoomSmallBtn.isEnabled()) {// 缩小按钮
			this.zoomSmallBtn.setEnabled(false);
		}

		if (this.zoomComboBox.isEnabled()) {// 放大缩小视图
			setZoomComboBoxVaule();
			this.zoomComboBox.setEnabled(false);
			// 当前画板：放大缩小下拉选择控件 当前比例标签
			this.zoomLabel.setEnabled(false);
		}

		if (this.originalBtn.isEnabled()) {// 原始视图
			this.originalBtn.setEnabled(false);
		}
	}

	/**
	 * 
	 * 设置放大缩小下拉框系统默认倍数
	 * 
	 */
	public void setZoomComboBoxDefaultItem() {
		if (zoomComboBox == null) {
			return;
		}
		zoomComboBox.setSelectedIndex(zoomComboBoxDeaultIndex);
	}

	public JecnToolbarContentButton getUndoBtn() {
		return undoBtn;
	}

	public JecnToolbarContentButton getRedoBtn() {
		return redoBtn;
	}

	public JecnToolbarContentButton getSaveBtn() {
		return saveBtn;
	}

	public JecnToolbarContentButton getSaveAsBtn() {
		return saveAsBtn;
	}

	public JecnToolbarContentButton getSaveAsImageBtn() {
		return saveAsImageBtn;
	}

	public JecnToolbarContentButton getZoomBigBtn() {
		return zoomBigBtn;
	}

	public JecnToolbarContentButton getZoomSmallBtn() {
		return zoomSmallBtn;
	}
}
