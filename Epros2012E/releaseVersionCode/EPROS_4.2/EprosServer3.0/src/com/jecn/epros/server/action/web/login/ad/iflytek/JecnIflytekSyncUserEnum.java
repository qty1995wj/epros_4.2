package com.jecn.epros.server.action.web.login.ad.iflytek;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.common.HttpRequest;
import com.jecn.epros.server.common.JecnConfigTool;

public enum JecnIflytekSyncUserEnum {
	INSTANCE;

	/**
	 * 返回 人员登录账号和名称集合 key:真实姓名 value :登录名 根据真实姓名获取登录名
	 * 
	 * @param listNames
	 * @return
	 */
	public Map<String, String> getMapUser(List<String> listNames) {
		if (!JecnConfigTool.isIflytekLogin() || listNames == null || listNames.isEmpty()) {
			return null;
		}
		// 测试 返回map
		// Map<String, String> map = new HashMap<String, String>();
		// for (String string : listNames) {
		// map.put(string == null ? "" : string, "科大讯飞" + "(" + string + ")");
		// }
		return searchResultByTrueName(listNames.get(0));
	}

	private Map<String, String> searchResultByTrueName(String trueName) {
		String json = getJsonByName(trueName, "");
		String result = null;
		try {
			result = HttpRequest.send(JecnIflytekLoginItem.getValue("userInfoUrl"), "parm=" + json, "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("result=" + result);
		String returnObj = JSONObject.fromObject(result).getString("content");
		if (StringUtils.isBlank(returnObj) || "null".equals(returnObj)) {
			return new HashMap<String, String>();
		}
		JSONArray jsonArray = JSONArray.fromObject(returnObj);

		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			// accountName,emplName
			map.put(jsonObject.getString("accountName"), jsonObject.getString("emplName"));
		}
		return map;
	}

	private String getJsonByName(String trueName, String loginName) {
		IflytekUserInfo userInfo = new IflytekUserInfo();
		userInfo.setAccontNames(loginName);
		userInfo.setEmplName(trueName);
		return JSONObject.fromObject(userInfo).toString();
	}

	public static void main(String args[]) {
		IflytekUserInfo userInfo = new IflytekUserInfo();
		userInfo.setAccontNames("");
		userInfo.setEmplName("管安凡");
		String param1 = JSONObject.fromObject(userInfo).toString();
		String result = HttpRequest.sendPost(JecnIflytekLoginItem.getValue("userInfoUrl"), "parm=" + param1);
		System.out.println(result);
	}

	private Map<String, String> searchResultByLoginName(List<String> loginNames) {
		StringBuffer buffer = new StringBuffer();
		for (String string : loginNames) {
			if (buffer.length() == 0) {
				buffer.append(string);
			} else {
				buffer.append(",").append(string);
			}
		}
		String param = getJsonByName("", buffer.toString().toLowerCase());
		String result = HttpRequest.sendPost(JecnIflytekLoginItem.getValue("userInfoUrl"), "parm=" + param);
		String returnObj = JSONObject.fromObject(result).getString("content");
		if (StringUtils.isBlank(returnObj) || "null".equals(returnObj)) {
			return new HashMap<String, String>();
		}
		JSONArray jsonArray = JSONArray.fromObject(returnObj);

		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			// accountName,emplName
			map.put(jsonObject.getString("accountName"), jsonObject.getString("emplName"));
		}
		return map;
	}

	/**
	 * 返回 人员登录账号和名称集合 key:登录名 value :真实姓名 根据登录名获取真实姓名
	 * 
	 * @param listNames
	 * @return
	 */
	public Map<String, String> getMapUserName(List<String> listNames) {
		if (!JecnConfigTool.isIflytekLogin() || listNames == null) {
			return null;
		}
		// // 测试 返回map
		// Map<String, String> map = new HashMap<String, String>();
		// for (String string : listNames) {
		// map.put(string == null ? "" : string, "科大讯飞" + "(" + string + ")");
		// }
		return searchResultByLoginName(listNames);
	}
}
