package epros.designer.gui.tree;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ToolTipManager;
import javax.swing.tree.TreePath;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.ResourceHighEfficiencyTree;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 设计器左面树面板。包括树节点快速查询和树
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTreeMainPanel extends JPanel {

	/** 树搜索界面 */
	private JecnTreeSearchPanel treeSearchPanel = null;
	/** 树面板的容器：滚动面板 */
	private JScrollPane treeScrollPane = null;
	/** 树 */
	private ResourceHighEfficiencyTree tree = null;

	/** 设计器主界面 */
	private JecnDesignerMainPanel designerMainPanel = null;

	private JecnTreeNode pointNode = null;

	public JecnTreeMainPanel(JecnDesignerMainPanel designerMainPanel) {
		this.designerMainPanel = designerMainPanel;
		initComponent();
	}

	private void initComponent() {
		// 树搜索界面
		treeSearchPanel = new JecnTreeSearchPanel(this);
		// 树面板的容器：滚动面板
		treeScrollPane = new JScrollPane();
		if (JecnConstants.projectId != null) {
			// 树
			tree = new ResourceHighEfficiencyTree();
			tree.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			tree.setBorder(null);
			tree.addMouseMotionListener(new MouseMotionListener() {

				@Override
				public void mouseMoved(MouseEvent e) {
					int row = tree.getRowForLocation(e.getX(), e.getY());
					if (row == -1) {
						tree.setToolTipText(null);
						pointNode = null;
						return;
					}
					TreePath treePath = tree.getPathForRow(row);
					if (treePath != null) {
						JecnTreeNode node = (JecnTreeNode) treePath
								.getLastPathComponent();
						try {
							JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
							if (node == pointNode) {
								return;
							}
							switch (jecnTreeBean.getTreeNodeType()) {
							case riskPoint:// 风险点
								showTip(node, jecnTreeBean.getContent());
								break;
							case standardClauseRequire://条款要求
								showTip(node,jecnTreeBean.getContent());
								break;
							default:
								tree.setToolTipText(null);
								break;
							}
						} catch (Exception e1) {
							JecnOptionPane.showMessageDialog(null,
									JecnProperties
											.getValue("serverConnException"));
						}
					}

				}

				public void showTip(JecnTreeNode node, String tip) {
					pointNode = node;

					// 组织HTML格式字符串
					StringBuffer sbuf = new StringBuffer();
					sbuf
							.append("<html><div style= 'white-space:normal; display:block;min-width:200px; word-break:break-all'>");
					// 说明
					sbuf.append(DrawCommon.getsubstring(tip));
					sbuf.append("</div></html>");
					ToolTipManager.sharedInstance().setReshowDelay(100);
					// 指定取消工具提示的延迟值 设置为一小时 3600秒
					ToolTipManager.sharedInstance()
							.setDismissDelay(3600 * 1000);
					// 注册要在工具提示中显示的文本。光标处于该组件上时显示该文本。如果 参数 为
					// null，则关闭此组件的工具提示
					tree.setToolTipText(sbuf.toString());
				}

				@Override
				public void mouseDragged(MouseEvent e) {
					
				}
			});
		}

		// 透明
		this.setOpaque(false);
		// 无边框
		this.setBorder(JecnUIUtil.getTootBarBorder());
		this.setLayout(new BorderLayout());

		// 树面板的容器：滚动面板
		treeScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treeScrollPane.setBorder(null);
		treeScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		// Dimension treeSize = new Dimension(designerMainPanel
		// .getTreeGuideWidth(), 200);
		// tree.setPreferredSize(treeSize);

		// 树面板添加到滚动容器中
		treeScrollPane.setViewportView(tree);
        
		if(JecnConstants.isPubShow()){ //非D2版本
			this.add(treeSearchPanel, BorderLayout.NORTH);
		}
		this.add(treeScrollPane, BorderLayout.CENTER);
	}

	public ResourceHighEfficiencyTree getTree() {
		return tree;
	}

	public void setTree(ResourceHighEfficiencyTree tree) {
		this.tree = tree;
		// 树
		tree.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		tree.setBorder(null);
		treeScrollPane.setViewportView(tree);
	}

}
