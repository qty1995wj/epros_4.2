package epros.designer.gui.system.seting;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import epros.designer.gui.autoNum.FileAutoNumManageDialog;
import epros.designer.gui.autoNum.ProcessAutoNumManageDialog;
import epros.designer.gui.autoNum.RuleAutoNumManageDialog;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.system.JecnToolbarButton;
import epros.designer.util.JecnProperties;

/**
 * 
 * 自动编号配置
 * 
 * @author ZXH
 * 
 */
public class JecnAutoCodeSet implements ActionListener {
	private final String PROCESS_AUTO = "processAuto";
	private final String FILE_AUTO = "fileAuto";
	private final String RULE_AUTO = "ruleAuto";
	/** 选择按钮标识 */
	private final String SELECTEDING_CMD = "selectIng";
	/** 配置容器 */
	private JecnGroupButton groupRecoverBtn = null;
	/** 当前选中配置 */
	private JecnSelectedButton currSelectedBtn = null;
	/** 选择配置按钮 */
	private JecnSelectingButton selectingBtn = null;

	private JecnToolbarButton processButton = null;
	private JecnToolbarButton fileButton = null;
	private JecnToolbarButton ruleButton = null;

	private JecnPopupMenuButton popupMenu = null;

	public JecnAutoCodeSet() {
		// 初始化组件
		initComponent();
	}

	/**
	 * 
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponent() {
		// 当前选中项
		currSelectedBtn = new JecnSelectedButton(this);
		// 选择列表按钮
		selectingBtn = new JecnSelectingButton();
		// 容器
		groupRecoverBtn = new JecnGroupButton(currSelectedBtn, selectingBtn);

		fileButton = new JecnToolbarButton(JecnProperties.getValue("fileAutoNum"), null);
		ruleButton = new JecnToolbarButton(JecnProperties.getValue("ruleAutoNum"), null);
		processButton = new JecnToolbarButton(JecnProperties.getValue("flowAutoNum"), null);

		List<JecnToolbarButton> btnList = new ArrayList<JecnToolbarButton>();
		btnList.add(fileButton);
		btnList.add(ruleButton);
		btnList.add(processButton);

		popupMenu = new JecnPopupMenuButton(btnList);
		selectingBtn.setActionCommand(SELECTEDING_CMD);
		fileButton.setActionCommand(FILE_AUTO);
		ruleButton.setActionCommand(RULE_AUTO);
		processButton.setActionCommand(PROCESS_AUTO);

		Dimension thisSize = new Dimension(100, 20);
		// 大小
		currSelectedBtn.setPreferredSize(thisSize);
		currSelectedBtn.setMaximumSize(thisSize);
		currSelectedBtn.setMinimumSize(thisSize);
		currSelectedBtn.setSize(thisSize);

		// 选择按钮
		selectingBtn.addActionListener(this);

		fileButton.addActionListener(this);

		ruleButton.addActionListener(this);

		processButton.addActionListener(this);

		currSelectedBtn.setTextIcon(fileButton);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!JecnDesignerCommon.isAdminTip()) {
			return;
		}
		String cmdStr = e.getActionCommand();

		if (SELECTEDING_CMD.equals(cmdStr)) {// 选择按钮
			popupMenu.show(currSelectedBtn, 30, 25);
			popupMenu.setPopupSize(new Dimension(currSelectedBtn.getWidth() + 10, popupMenu.getHeight()));
			popupMenu.repaint();
			return;
		}

		if (currSelectedBtn.getWidth() != 100) {
			Dimension thisSize = new Dimension(100, 20);
			// 大小
			currSelectedBtn.setPreferredSize(thisSize);
			currSelectedBtn.setMaximumSize(thisSize);
			currSelectedBtn.setMinimumSize(thisSize);
			currSelectedBtn.setSize(thisSize);
		}

		if (popupMenu.isVisible()) {
			popupMenu.setVisible(false);
		}
		if (FILE_AUTO.equals(cmdStr)) {
			FileAutoNumManageDialog fileAutoNumManageDialog = new FileAutoNumManageDialog();
			fileAutoNumManageDialog.setVisible(true);
			currSelectedBtn.setTextIcon(fileButton);
		} else if (RULE_AUTO.equals(cmdStr)) {
			RuleAutoNumManageDialog ruleAutoNumManageDialog = new RuleAutoNumManageDialog();
			ruleAutoNumManageDialog.setVisible(true);
			currSelectedBtn.setTextIcon(ruleButton);
		} else if (PROCESS_AUTO.equals(cmdStr)) {
			ProcessAutoNumManageDialog processAutoNumManageDialog = new ProcessAutoNumManageDialog();
			processAutoNumManageDialog.setVisible(true);
			currSelectedBtn.setTextIcon(processButton);
		}
	}

	public JecnGroupButton getGroupRecoverBtn() {
		return groupRecoverBtn;
	}
}
