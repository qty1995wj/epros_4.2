package epros.designer.gui.system.config.ui.right;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.JecnAbstractJPanel;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnFileConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnFlowConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnPubSetConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnRuleConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnTotalMapConfigJDialog;
import epros.designer.util.JecnProperties;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 右面按钮面板，其包括加入、上移、下移、删除、默认等按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnRightButtonPanel extends JecnAbstractJPanel {
	/** 添加按钮 */
	private JecnAddJButton addJButton = null;
	/** 删除按钮 */
	private JecnDeleteJButton deleteJButton = null;
	/** 编辑按钮 */
	private JecnEditJButton editJButton = null;
	/** 上移按钮 */
	private JecnUpJButton upJButton = null;
	/** 下移按钮 */
	private JecnDownJButton downJButton = null;
	/** 默认按钮 */
	private JecnDefaultJButton defaultJButton = null;
	/** 必填按钮 */
	private JecnNotEmptyJButton notEmptyJButton = null;
	/** 非必填按钮 */
	private JecnEmptyJButton emptyJButton = null;
	/** 属性配置按钮 */
	private JecnPropertySetButton propertySetButton = null;

	public JecnPropertySetButton getPropertySetButton() {
		return propertySetButton;
	}

	public JecnRightButtonPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		initComponents();
		initLayout();
	}

	protected void initComponents() {
		// 无边框
		this.setBorder(null);

		// 添加按钮
		addJButton = new JecnAddJButton(dialog);
		addJButton.setText(JecnProperties.getValue("task_addBtn"));

		// 删除按钮
		deleteJButton = new JecnDeleteJButton(dialog);
		deleteJButton.setText(JecnProperties.getValue("task_deleteBtn"));

		// 编辑按钮
		editJButton = new JecnEditJButton(dialog);
		editJButton.setText(JecnProperties.getValue("task_editBtn"));

		// 上移按钮
		upJButton = new JecnUpJButton(dialog);
		upJButton.setText(JecnProperties.getValue("task_upBtn"));

		// 下移按钮
		downJButton = new JecnDownJButton(dialog);
		downJButton.setText(JecnProperties.getValue("task_downBtn"));

		// 默认按钮
		defaultJButton = new JecnDefaultJButton(dialog);
		defaultJButton.setText(JecnProperties.getValue("task_defaultBtn"));

		// 必填按钮
		notEmptyJButton = new JecnNotEmptyJButton(dialog);
		notEmptyJButton.setText(JecnProperties.getValue("task_notEmptyBtn"));
		// 非必填按钮
		emptyJButton = new JecnEmptyJButton(dialog);
		emptyJButton.setText(JecnProperties.getValue("task_emptyBtn"));
		// 属性配置按钮
		propertySetButton = new JecnPropertySetButton(dialog);
		propertySetButton.setText(JecnProperties.getValue("jecnPropertySet"));

		// 事件
		addJButton.addActionListener(addJButton);
		deleteJButton.addActionListener(deleteJButton);
		editJButton.addActionListener(editJButton);
		upJButton.addActionListener(upJButton);
		downJButton.addActionListener(downJButton);
		defaultJButton.addActionListener(defaultJButton);
		notEmptyJButton.addActionListener(notEmptyJButton);
		emptyJButton.addActionListener(emptyJButton);
		propertySetButton.addActionListener(propertySetButton);

		initBtnsEnabled(null);
	}

	private void initLayout() {
		// 布局
		Insets insets = new Insets(6, 7, 6, 7);
		// 添加按钮
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, insets, 0, 0);
		this.add(addJButton, c);
		// 删除按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(deleteJButton, c);
		// 编辑按钮
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(editJButton, c);
		// 上移按钮
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(upJButton, c);
		// 下移按钮
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(downJButton, c);
		// 默认按钮
		c = new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(defaultJButton, c);
		// 必填按钮
		c = new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(notEmptyJButton, c);
		// 非必填按钮
		c = new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(emptyJButton, c);
		// 属性配置按钮
		c = new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		this.add(propertySetButton, c);
		// 空闲区域
		c = new GridBagConstraints(0, 8, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(new JLabel(), c);
	}

	/**
	 * 
	 * 设置按钮是否和编辑状态     
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initBtnsEnabled(JecnConfigTypeDesgBean configTypeDesgBean) {
		if (configTypeDesgBean == null) {// 不合法
			setBtnsEnabled(false, false, false, false, false, false, false, false, false);
			return;
		}
		
		// 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
		// 添加按钮、删除按钮、上移按钮、下移按钮、默认按钮、编辑按钮、必填按钮、非必填按钮
		switch (configTypeDesgBean.getType()) {
		case 0:// 0：审批环节配置
			setBtnsEnabled(true, true, true, true, true, true, true, true, false);
			break;
		case 4:// 4：流程文件
			setBtnsEnabled(true, true, true, true, true, true, true, true, true);
			break;

		case 2:// 2：基本配置
			// 是否存在浏览端，如果有浏览端则显示必填/非必填按钮
			if (JecnConstants.isPubShow()) {
				//dialog instanceof JecnFileConfigJDialog ||  注释原有的 文件 基本配置 页面权限
				if ( dialog instanceof JecnPubSetConfigJDialog) {// 制度、文件、权限
					setBtnsEnabled(false, false, false, false, true, false, false, false, false);
				} else if (dialog instanceof JecnFlowConfigJDialog   || dialog instanceof JecnFileConfigJDialog ) {
					setBtnsEnabled(true, true, false, false, true, false, true, true, false);
				}else if (dialog instanceof JecnTotalMapConfigJDialog || dialog instanceof JecnRuleConfigJDialog) {
					setBtnsEnabled(true, true, false, false, true, true, true, true, false);
				} else {
					setBtnsEnabled(false, false, false, false, true, false, true, true, false);
				}
			} else {
				setBtnsEnabled(false, false, false, false, true, false, false, false, false);
			}

			break;
		case 5:// 5：邮箱配置
		case 6:// 6：权限配置
		case 8:// 8：消息邮件配置
		case 9:// 9：合理化建议配置
		case 10:// 10：流程属性
		case 11:// 11：发布配置
		case 14:// 下载次数配置
		case 15:// 文控信息配置
			setBtnsEnabled(false, false, false, false, true, false, false, false, false);
			break;
		case 1:// 1：任务界面显示项配置
			setBtnsEnabled(true, true, false, false, true, true, true, true, false);
			break;
		case 3:// 3：流程图建设规范
		case 7:// 7：任务操作
			setBtnsEnabled(true, true, false, false, true, false, false, false, false);
			break;
		case 16:// 16：流程KPI配置
			setBtnsEnabled(true, true, true, true, true, true, true, true, false);
			break;
		case 18:// 流程架构-其它配置
			setBtnsEnabled(false, false, false, false, true, false, false, false, false);
			break;
		default:
			setBtnsEnabled(false, false, false, false, false, false, false, false, false);
			break;
		}
	}

	private void setBtnsEnabled(boolean addBtnEnable, boolean deleteBtnEnable, boolean upBtnEnable,
			boolean downBtnEnable, boolean defaultBtnEnable, boolean editBtnEnable, boolean notEmptyEnable,
			boolean emptyEnable, boolean propertySetEnable) {
		// 添加按钮
		addJButton.setVisible(addBtnEnable);
		// 删除按钮
		deleteJButton.setVisible(deleteBtnEnable);
		// 编辑按钮
		editJButton.setVisible(editBtnEnable);
		// 上移按钮
		upJButton.setVisible(upBtnEnable);
		// 下移按钮
		downJButton.setVisible(downBtnEnable);
		// 默认按钮
		defaultJButton.setVisible(defaultBtnEnable);
		// 必填按钮
		notEmptyJButton.setVisible(notEmptyEnable);
		// 非必填按钮
		emptyJButton.setVisible(emptyEnable);
		// 属性配置按钮
		propertySetButton.setVisible(propertySetEnable);
	}
}
