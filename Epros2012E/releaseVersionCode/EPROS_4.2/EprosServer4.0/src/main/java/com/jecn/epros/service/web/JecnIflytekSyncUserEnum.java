package com.jecn.epros.service.web;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.util.ConfigUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

public enum JecnIflytekSyncUserEnum {
    INSTANCE;
    private Logger LOGGER = Logger.getLogger(JecnIflytekSyncUserEnum.class);
    // 根据真实姓名获取登录名

    /**
     * 返回 人员登录账号和名称集合 key:登录名 value :真实姓名 根据真实姓名获取登录名
     *
     * @param listNames
     * @return
     */
    public Map<String, String> getMapUserTrueName(List<String> listNames) {
        if (!ConfigUtils.isIflytekLogin() || listNames == null) {
            return null;
        }
        // 测试 返回map
//        Map<String, String> map = new HashMap<String, String>();
//        for (String string : listNames) {
//            //map.put(string == null ? "" : string, "科大訊飛測試" + (string));
//            map.put(string == null ? "" : string, "admin");
//        }
//        return map;
        return searchResultByTrueName(listNames.get(0));
    }

    private static ObjectMapper objectMapper = new ObjectMapper();

    private Map<String, String> searchResultByTrueName(String trueName) {
        String param = "{\"accontNames\":\"\",\"emplName\":\"" + trueName + "\"}";
        return getResultMap(param);
    }

    private Map<String, String> getResultMap(String json) {
        Map<String, String> map = new HashedMap();
        try {
            LOGGER.info(IfytekCommon.getValue("userInfoUrl"));
         //   String result = IfytekCommon.sendPost(IfytekCommon.getValue("userInfoUrl"), "parm=" + json, "utf-8");
            String result = "{\"result\":true,\"message\":\"\",\"content\":[{\"accountName\":\"admin\",\"emplName\":\"刘飞\"},{\"accountName\":\"feiliu6\",\"emplName\":\"刘飞\"},{\"accountName\":\"feiliu7\",\"emplName\":\"刘飞\"},{\"accountName\":\"feiliu5\",\"emplName\":\"刘飞\"}]}";
            JsonNode node = objectMapper.readTree(result);
            JsonNode content = node.get("content");
            if (content == null) {
                return map;
            }
            LOGGER.info("content = " + content.toString());
            if (content.isArray()) {
                for (JsonNode objNode : content) {
                    Map<String, String> mapNode = objectMapper.readValue(objNode.toString(), Map.class);
                    map.put(objNode.get("accountName").asText(), objNode.get("emplName").asText());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        LOGGER.info("map = " + map);
        return map;
    }


    /**
     * 返回 人员登录账号和名称集合 key:登录名 value :真实姓名 根据登录名获取真实姓名
     *
     * @param listNames
     * @return
     */
    public Map<String, String> getMapUserByLoginName(List<String> listNames) {
        if (!ConfigUtils.isIflytekLogin() || listNames == null) {
            return null;
        }
        return searchResultByLoginName(listNames);
    }

    private Map<String, String> searchResultByLoginName(List<String> loginNames) {
        StringBuffer buffer = new StringBuffer();
        for (String string : loginNames) {
            if (buffer.length() == 0) {
                buffer.append(string);
            } else {
                buffer.append(",").append(string);
            }
        }
        String param = "{\"accontNames\":\"" + buffer.toString().toLowerCase() + "\",\"emplName\":\"\"}";
        return getResultMap(param);
    }
}
