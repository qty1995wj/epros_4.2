package com.jecn.search.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.search.bean.JecnKeyWordSearchBean;
import com.jecn.search.bean.JecnSearchResultBean;
import com.jecn.search.dao.IJecnKeyWordSearchDao;
import com.jecn.search.service.IJecnKeyWordSearchService;

@Service
@Transactional(rollbackFor = Exception.class)
public class JecnKeyWordSearchServiceImpl implements IJecnKeyWordSearchService {
	@Resource
	private IJecnKeyWordSearchDao keyWordSearchDao;

	@Override
	public JecnSearchResultBean keyWordSearch(String searchName, int relatedType, int start, int pageSize)
			throws Exception {
		List<Object[]> lists = keyWordSearchDao.keyWordSearch(searchName, relatedType, start, pageSize);
		if (lists == null) {
			return null;
		}
		JecnSearchResultBean resultBean = new JecnSearchResultBean();
		List<JecnKeyWordSearchBean> resultLists = new ArrayList<JecnKeyWordSearchBean>();
		JecnKeyWordSearchBean searchBean = null;
		// Object[] : RELATEDID,REALTEDNAME,UPDATEDATE,RELATETYPE
		for (Object[] objects : lists) {
			if (objects[2] == null || objects[3] == null) {
				continue;
			}
			searchBean = new JecnKeyWordSearchBean();
			searchBean.setRelatedId(Long.valueOf(objects[0].toString()));
			searchBean.setRealtedName(objects[1].toString());
			searchBean.setUpdateDate(objects[2].toString());
			searchBean.setRelatedType(Integer.valueOf(objects[3].toString()));
			resultLists.add(searchBean);
		}
		resultBean.setSearchResultLists(resultLists);
		return resultBean;
	}

	@Override
	public JecnSearchResultBean keyWordSearchTotal(String searchName, int relatedType) throws Exception {
		List<Map<String, Object>> maps = keyWordSearchDao.keyWordSearchTotal(searchName, relatedType);
		int relateType = 0;
		JecnSearchResultBean resultBean = new JecnSearchResultBean();
		int totalCount = 0;
		for (Map<String, Object> map : maps) {
			relateType = Integer.valueOf(map.get("RELATETYPE").toString());
			totalCount = Integer.valueOf(map.get("RELATEDID").toString());
			// 1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险
			switch (relateType) {
			case 1:
				resultBean.setFlowMapCount(totalCount);
				break;
			case 2:
				resultBean.setFlowCount(totalCount);
				break;
			case 3:
				resultBean.setFileCount(totalCount);
				break;
			case 4:
				resultBean.setRuleCount(totalCount);
				break;
			case 5:
				resultBean.setStandardCount(totalCount);
				break;
			case 6:
				resultBean.setRiskCount(totalCount);
				break;
			}
		}
		return resultBean;
	}
}
