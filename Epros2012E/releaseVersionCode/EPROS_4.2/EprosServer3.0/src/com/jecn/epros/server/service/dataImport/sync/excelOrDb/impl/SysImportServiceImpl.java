package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.server.bean.dataimport.BasePosBean;
import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.bean.dataimport.JecnBaseRelPosBean;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.dataImport.excelorDB.IUserImportDao;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.dataImport.PathUpdater;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.ISysImportService;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncSqlConstant;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpOriDept;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;

/**
 * 人员同步业务处理接口
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Jun 3, 2013 时间：12:42:44 PM
 */
@Transactional(rollbackFor = Exception.class)
public class SysImportServiceImpl implements ISysImportService {
	/** DAO层专门处理UserImportBuss类的DB操作 */
	private IUserImportDao userImportDao = null;

	private final Log log = LogFactory.getLog(SysImportServiceImpl.class);

	@Override
	public void deleteAndInsertTmpOriJecnIOTable(BaseBean baseBean) throws Exception {
		// 3.清除原始数据临时表，并添加新数据
		userImportDao.deleteAndInsertTmpOriJecnIOTable(baseBean);
	}

	@Override
	public void deleteAndInsertTmpJecnIOTable(BaseBean baseBean) throws Exception {
		// 5.清除临时库、入库临时库
		userImportDao.deleteAndInsertTmpJecnIOTable(baseBean);
	}

	@Override
	public void updateProFlag(Long currProjectID, boolean isNotExistsDept) throws Exception {
		userImportDao.UpdateProFlag(currProjectID, isNotExistsDept);
	}

	public IUserImportDao getUserImportDao() {
		return userImportDao;
	}

	public void setUserImportDao(IUserImportDao userImportDao) {
		this.userImportDao = userImportDao;
	}

	@Override
	public List<Long> checkProjectId() throws Exception {
		return userImportDao.selectProjectId();
	}

	/**
	 * 获取主项目ID
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public Long getProtectedId() throws Exception {
		log.info("查询当前的项目ID：当前项目表(JECN_CURPROJECT)数据开始 ");
		String hql = "select curProjectId from JecnCurProject";
		List<Long> list = userImportDao.listHql(hql);
		if (list.size() > 0) {
			return list.get(0);
		}
		log.info("查询当前的项目ID：当前项目表(JECN_CURPROJECT)数据结束 ");
		return null;
	}

	@Override
	public void syncImortDataAndDB(BaseBean baseBean, Long curProjectID, boolean isSyncUserOnly) throws Exception {
		if (baseBean == null || baseBean.isNull()) {
			return;
		}

		// --------------------------------插入 start
		if (!isSyncUserOnly) {
			// 插入部门
			userImportDao.insertDept(curProjectID);
			// 插入岗位，且关联所属部门ID
			userImportDao.insertPos(curProjectID);
		}
		// 插入人员
		userImportDao.insertUser();
		// --------------------------------插入 end

		// -------------------------------- 删除 start
		// 更新【变更岗位】对应的岗位数据：此时 临时岗位表中数据对应的真实岗位表中数据都对应上新的部门ID
		// 执行到这就可以执行删除动作

		if (!isSyncUserOnly) {
			// 删除部门：先删除岗位相关再删除部门相关 start
			// 更新【变更岗位】对应的岗位数据,查询需要删除的部门为A，删除A下的岗位数据以及更新或删除岗位相关联的表中字段值
			userImportDao.updatePosUpAndDeletePos(curProjectID);
			// 删除真实部门表中部门，此部门是在临时部门表中不存在；
			userImportDao.deleteDept(curProjectID);
			// 删除部门：先删除岗位相关再删除部门相关 end

			// 删除真实岗位表中岗位，此岗位是在临时岗位表中不存在；
			userImportDao.deletePos(curProjectID);
		}

		// 删除真实人员表中人员，此人员是在临时人员表中不存；
		// userImportDao.deleteUser(curProjectID);
		log.info(" 删除真实人员表中人员，此人员是在临时人员表中不存 结束;");

		if (!isSyncUserOnly) {
			// 删除真实人员与岗位关系表中人员ID和岗位ID在临时表中没有对应的数据
			userImportDao.deleteUserPosRef(curProjectID);
		}

		log.info("删除真实人员与岗位关系表中人员ID和岗位ID在临时表中没有对应的数据 结束");
		// -------------------------------- 删除 end

		// -------------------------------- 更新 start

		// 获取更新信息

		// 临时表和正式表中需要修改的角色 数据格式为：posT/pos，主键集合
		Map<String, List<Long>> roleMap = userImportDao.getChangeRoles();

		if (!isSyncUserOnly) {
			// 更新父部门编号 (部门编号更新)
			userImportDao.updatePerOrgNum();
			// 部门
			userImportDao.updateNameAndPidByDept(curProjectID);

			log.info(" 更新部门  结束;");
			// 岗位
			userImportDao.updateNameAndDeptIdByPos(curProjectID);
		}

		log.info(" 更新岗位 结束;");
		// 人员
		userImportDao.updateAttributesByUser(curProjectID);
		log.info(" 更新人员 结束;");
		// -------------------------------- 更新 end

		// -------------------------------- 插入人员与岗位关系 start
		// 插入临时表中人员有岗位且真实人员与岗位关系表中没有对应的记录
		userImportDao.insertUserPosRef(curProjectID);

		// -------------------------------- 插入岗位基本信息表
		userImportDao.insertPositionFigInfo(curProjectID);

		// -------------------------------- 更新角色（只关联了一个岗位或岗位组的角色，且用户没有手动改过角色名称）
		userImportDao.updateFlowRoleFigureText(roleMap, curProjectID);

		log.info(" 插入临时表中人员有岗位且真实人员与岗位关系表中没有对应的记录 结束;");

		// ----------------基准岗位同步开始------------------------
		if (JecnContants.otherUserSyncType == 8) { // 8： 基准岗位同步
			// 匹配基准岗位主键ID
			log.info("除掉基准岗位中已经存在的数据，并且删除Epors库中多余的数据开始！");
			// 首先清除掉基准岗位中已经存在的数据，并且删除Epors库中多余的数据
			getFlowOrgImageList(baseBean.getJecnBasePosBeanList());
			log.info("除掉基准岗位中已经存在的数据，并且删除Epors库中多余的数据结束！");
			// 基准岗位插入
			log.info("插入基准岗位结束");
			// 插入基准岗位与岗位关系
			log.info("插入基准岗位与岗位关系开始");
			insertBaseRelPosInfo(baseBean.getBasePosBean());
			log.info("插入基准岗位与岗位关系结束");

			// 岗位对应基准岗位变更，岗位组相关岗位集合：删除不存在的记录(岗位组关联基准岗位)
			userImportDao.deletePosChangeBasePos();
			log.info(" 岗位组关联基准岗位 - 删除岗位对应基准岗位变更数据");
			// 更改基准岗位与岗位关系变更数据(添加)
			log.info("更改基准岗位与岗位关系变更数据开始！");
			userImportDao.addGroupRelPos();

			// userImportDao.delGroupRelPos(jecnBasePosBean.getBasePosNum());
			log.info("更改基准岗位与岗位关系变更数据结束！");
		} else if (JecnContants.otherUserSyncType == 9) {// 岗位组导入
			syncPosGroupData(curProjectID);
		}

		PathUpdater updater = new PathUpdater(userImportDao);
		// 更新部门表
		updater.updateOrgTLevelAndTPath();
		if (JecnContants.otherUserSyncType == 9) {
			// 更新岗位组
			updater.updateGroupLevelAndTPath();
		}
	}

	@Override
	public String syncDataValidate(Long projectId) throws Exception {
		StringBuilder builder = new StringBuilder();
		// 1、部门总数验证
		List<Object[]> syncDeptCounts = this.userImportDao.getSyncDeptNums(projectId);
		if (syncDeptCounts != null && syncDeptCounts.size() == 1) {
			Object[] obj = syncDeptCounts.get(0);
			int curDeptCount = Integer.valueOf(obj[0].toString());
			int syncDeptCount = Integer.valueOf(obj[1].toString());

			if (Math.abs(curDeptCount - syncDeptCount) >= 100) {
				builder.append("部门总数变动超过100，请确认数据准确性，然后手动同步！");
			}
		}
		// 2、 岗位总数验证
		List<Object[]> syncPosCounts = this.userImportDao.getSyncPosNumsCount(projectId);
		if (syncPosCounts != null && syncPosCounts.size() == 1) {
			Object[] obj = syncPosCounts.get(0);
			int curPosCount = Integer.valueOf(obj[0].toString());
			int syncPosCount = Integer.valueOf(obj[1].toString());

			if (Math.abs(curPosCount - syncPosCount) >= 100) {
				builder.append("岗位总数变动超过100，请确认数据准确性，然后手动同步！");
			}
		}

		// 3、 人员总数验证
		List<Object[]> syncUserCounts = this.userImportDao.getSyncUserNumsCount();
		if (syncUserCounts != null && syncUserCounts.size() == 1) {
			Object[] obj = syncUserCounts.get(0);
			int curUserCount = Integer.valueOf(obj[0].toString());
			int syncUserCount = Integer.valueOf(obj[1].toString());
			if (Math.abs(curUserCount - syncUserCount) >= 500) {
				builder.append("人员总数变动超过500，请确认数据准确性，然后手动同步！");
			}
		}
		return builder.toString();
	}

	/**
	 * 岗位组同步，岗位组增删改
	 * 
	 * @param curProjectID
	 * @throws Exception
	 */
	private void syncPosGroupData(Long curProjectID) throws Exception {
		// 插入岗位组
		userImportDao.insertPosGroup(curProjectID);
		userImportDao.insertPosGroupRelatedPos();
		deletePosGroup();
		updatePosGroup();
	}

	/**
	 * 更新岗位组
	 */
	private void updatePosGroup() {
		// 更新岗位组
		String sql = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = SyncSqlConstant.UPDATE_JECN_POSGROUP_ORACLE;
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = SyncSqlConstant.UPDATE_JECN_POSGROUP_SQLSERVER;
		}
		userImportDao.execteNative(sql);
		userImportDao.getSession().flush();
		// 更新顶点
		sql = " UPDATE JECN_POSITION_GROUP SET PER_ID = 0 WHERE PARENT_CODE = '0'";
		userImportDao.execteNative(sql);

		// 更新岗位组相关岗位
		sql = "UPDATE JECN_POSITION_GROUP_R" + "   SET FIGURE_ID =" + "       (SELECT FIGURE_ID"
				+ "          FROM JECN_FLOW_ORG_IMAGE OI" + "         WHERE OI.FIGURE_NUMBER_ID = POS_NUM),"
				+ "       GROUP_ID =" + "       (SELECT ID FROM JECN_POSITION_GROUP PG WHERE PG.CODE = POSGROUP_NUM)"
				+ " WHERE POS_NUM IS NOT NULL" + "   AND DATA_TYPE = 1";
		userImportDao.execteNative(sql);
	}

	private void deletePosGroup() {
		// 1、// 删除不存在的岗位组
		String sql = "DELETE FROM JECN_POSITION_GROUP" + " WHERE DATA_TYPE = 1" + "   AND NOT EXISTS"
				+ " (SELECT 1 FROM TMP_JECN_POS_GROUP JPG WHERE JPG.NUM = CODE)";
		userImportDao.execteNative(sql);

		// 2、岗位组相关角色，岗位组相关查阅权限（JECN_PROCESS_STATION_RELATED）1：岗位组 删除
		sql = SyncSqlConstant.DELETE_JECN_PROCESS_STATION_RELATED_DEL_POSGROUP;
		userImportDao.execteNative(sql);
		// 岗位组相关角色，岗位组相关查阅权限 （JECN_PROCESS_STATION_RELATED_T）1：岗位组 删除
		sql = SyncSqlConstant.DELETE_JECN_PROCESS_STATION_RELATED_T_DEL_POSGROUP;
		userImportDao.execteNative(sql);

		// 删除岗位组相关岗位
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = SyncSqlConstant.DELETE_JECN_POSITION_GROUP_R_BY_TMP_ORACLE;
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = SyncSqlConstant.DELETE_JECN_POSITION_GROUP_R_BY_TMP_SQLSERVER;
		}
		userImportDao.execteNative(sql);
	}

	/**
	 * 插入基准岗位与岗位关系
	 * 
	 * 
	 * basePosRelBeanList 基准岗位与岗位关系集合
	 * 
	 * @author cheshaowei
	 * 
	 * */
	private void insertBaseRelPosInfo(List<BasePosBean> basePosRelBeanList) throws Exception {

		// 说明：basePosRelBeanList 集合对象不会为空，所以无需做null判断
		// 先将原先数据清除
		userImportDao.deleteBaseRelPosInfo();

		JecnBaseRelPosBean jecnBaseRelPosBean = null;
		for (BasePosBean basePosRelBean : basePosRelBeanList) {
			// 基准岗位编号
			String basePosNum = basePosRelBean.getBasePosNum();
			if (StringUtils.isNotBlank(basePosRelBean.getPosNum())) {
				jecnBaseRelPosBean = new JecnBaseRelPosBean();
				// 主键ID
				jecnBaseRelPosBean.setId(UUID.randomUUID().toString());
				// 基准岗位编号
				jecnBaseRelPosBean.setBasePosId(basePosNum);
				// 岗位ID
				jecnBaseRelPosBean.setPosId(basePosRelBean.getPosNum());
				// 执行插入
				userImportDao.insertBaseRelPosInfo(jecnBaseRelPosBean);
			}
		}
	}

	/***
	 * 查找已经存在的基准岗位并且删除Epros库中待同步数据不存在的基准岗位
	 * 
	 * @param jecnBasePosBeanList
	 *            拆分后的基准岗位集合
	 * 
	 * @author cheshaowei
	 * 
	 * */
	private void getFlowOrgImageList(List<JecnBasePosBean> jecnBasePosBeanList) throws Exception {
		// Epros库中的基准岗位
		List<JecnBasePosBean> flowOrgImages = userImportDao.getJecnBasePosBeanList();

		// 添加和更新
		for (JecnBasePosBean jecnBasePosBean : jecnBasePosBeanList) {
			boolean isExists = false;
			for (JecnBasePosBean basePosBean : flowOrgImages) {
				if (basePosBean.getBasePosNum().equals(jecnBasePosBean.getBasePosNum())) {
					if (!basePosBean.getBasePosName().equals(jecnBasePosBean.getBasePosName())) {// 更新
						basePosBean.setBasePosName(jecnBasePosBean.getBasePosName());
						userImportDao.getSession().update(basePosBean);
					}
					isExists = true;
					break;
				}
			}
			if (!isExists) {// 不存在，添加
				userImportDao.insertBasePosInfo(jecnBasePosBean);
			}
		}

		String basePosNum = "";
		for (JecnBasePosBean basePosBean : flowOrgImages) {
			boolean isExists = false;
			basePosNum = basePosBean.getBasePosNum();
			for (JecnBasePosBean jecnBasePosBean : jecnBasePosBeanList) {
				if (basePosNum.equals(jecnBasePosBean.getBasePosNum())) {
					isExists = true;
					break;
				}
			}
			if (!isExists) { // 删除数据库中存在，视图中不存在的基准岗位相关数据
				// 如果待同步的基准岗位数据中没有此基准岗位，删掉Epros中多余的基准岗位
				userImportDao.delBasePosInfo(basePosNum);
				// 删除基准岗位与岗位的关系(此情况是直接删除一个基准岗位的时候)
				userImportDao.delBaseRelPosParmBaseNum(basePosNum);
				// 删除添加到岗位组与岗位关系表中的相关数据
				userImportDao.delJecnBasePosGroupR(basePosNum);
				// 删除此基准岗位所关联的岗位组下的岗位，不包括岗位组直接关联的基准岗位
				// 删掉基准岗位与岗位组的关系
				userImportDao.delBasePosGroup(basePosNum);
			}
		}
	}

	/**
	 * 发送邮件或消息给系统管理员
	 * 
	 * @param filePath
	 * @throws Exception
	 */
	@Override
	public void changeDataSendMailMessageToAdmin(MessageResult result, String filePath, boolean hasChange)
			throws Exception {

		// 管理员信息
		List<JecnUser> users = this.userImportDao.getSession().createSQLQuery(JecnCommonSql.getAdminEmailSql())
				.addEntity(JecnUser.class).list();
		if (!users.isEmpty()) {
			BaseEmailBuilder emailBuilder = null;
			if (result.isSuccess()) {
				if (StringUtils.isNotBlank(filePath) && new File(filePath).exists() && hasChange) {
					File file = new File(filePath);
					emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.USER_IMPORT_RESULT);
					emailBuilder.setData(users, file, "人员同步变更信息.xls");
				} else {
					emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.SIMPLE_EMAIL);
					emailBuilder.setData(users, JecnConfigTool.getEmailPlatName() + "人员同步成功，没有变更数据", "人员同步成功");
				}
			} else {
				emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.SIMPLE_EMAIL);
				String errorReason = result.getResultMessage();
				String platName = JecnConfigTool.getEmailPlatName();
				if (StringUtils.isNotBlank(filePath) && new File(filePath).exists()) {
					File file = new File(filePath);
					emailBuilder.setData(users, platName + "人员同步失败，附件为异常数据", "人员同步失败", file, "人员同步变更信息.xls");
				} else {
					emailBuilder.setData(users, platName + "人员同步失败，错误原因：<br/>" + errorReason, "人员同步失败");
				}
			}
			// TODO :EMAIL
			JecnUtil.saveEmail(emailBuilder.buildEmail());
		}

	}

	/**
	 * 发送消息给管理员
	 * 
	 * @param addDays
	 * @param objects
	 * @throws Exception
	 */
	private void sendMessage(List<JecnUser> users) throws Exception {
		if (users == null) {
			return;
		}
		for (JecnUser user : users) {
			if (user.getPeopleId() == null) {
				return;
			}
			createMessage(user.getPeopleId());
		}
	}

	/**
	 * 
	 * 发布给PRF查阅权限人员发送消息
	 * 
	 * @param peopleIds
	 *            人员主键ID集合
	 * @param prfName
	 *            PRF文件名称
	 */
	private void createMessage(Long peopleId) {
		Session s = this.userImportDao.getSession();
		// 已发布！
		if (peopleId != null) {
			JecnMessage jecnMessage = new JecnMessage();
			jecnMessage.setPeopleId(0L);
			jecnMessage.setMessageType(0);
			jecnMessage.setMessageContent(SyncConstant.IMPORT_MAIL_CONTENT1 + getContent2()
					+ JecnConfigTool.getEmailPlatName() + "<BR><BR>" + JecnCommon.getStringbyDateHMS(new Date()));
			// 服务器到期提醒！
			jecnMessage.setMessageTopic(SyncConstant.IMPORT_MAIL_SUBJET);
			jecnMessage.setNoRead(Long.valueOf(0));
			jecnMessage.setInceptPeopleId(peopleId);
			jecnMessage.setCreateTime(new Date());
			s.save(jecnMessage);
			s.flush();
		}
	}

	private String getContent2() {
		String result = "&nbsp;&nbsp;&nbsp;&nbsp;" + JecnConfigTool.getEmailPlatName()
				+ "人员同步存在信息变更，变更信息为附件，请查阅！<BR><BR>";
		return result;
	}

	@Override
	public List<JecnTmpOriDept> findAllDeptOri() throws Exception {
		return userImportDao.findAllDeptOri();
	}

	@Override
	public List<JecnTmpPosAndUserBean> findAllUserAndPosOri() throws Exception {
		return userImportDao.findAllUserAndPosOri();
	}

}
