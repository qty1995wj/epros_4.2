package com.jecn.epros.server.bean.rule.sync;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.rule.RuleT;

public class SyncRule implements Serializable {
	/**
	 * 传输的数据
	 */
	private RuleT self;
	private List<SyncRule> rules = new ArrayList<SyncRule>();
	private String id;
	private String pid;

	public boolean isDir() {
		return self.getIsDir() == 0;
	}

	public boolean isFile() {
		return !isDir();
	}

	// ----------------

	public RuleT getSelf() {
		return self;
	}

	public void setSelf(RuleT self) {
		this.self = self;
	}

	public List<SyncRule> getRules() {
		return rules;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public void setRules(List<SyncRule> rules) {
		this.rules = rules;
	}
}
