package com.jecn.epros.server.bean.process;

public class JecnFlowDriverEmail implements java.io.Serializable {
	private String id;
	private Long relateId;
	private Long peopleId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getRelateId() {
		return relateId;
	}
	public void setRelateId(Long relateId) {
		this.relateId = relateId;
	}
	public Long getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}
	
}
