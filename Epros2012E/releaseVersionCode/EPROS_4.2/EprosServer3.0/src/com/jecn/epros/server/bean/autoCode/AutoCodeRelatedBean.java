package com.jecn.epros.server.bean.autoCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 自动编号-文件类型关联表
 * 
 * @author ZXH
 * 
 */
public class AutoCodeRelatedBean implements Serializable {
	private String guid;
	/** 编号规则主键ID */
	private String autoCodeId;
	private Long relatedId;
	/** 0是流程架构、1是流程、2是制度目录、3是制度模板、4是制度文件（本地上传）、5是文件目录、6是文件*/
	private int relatedType;
	private Date createTime;
	private Long createPersonId;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public int getRelatedType() {
		return relatedType;
	}

	public void setRelatedType(int relatedType) {
		this.relatedType = relatedType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public String getAutoCodeId() {
		return autoCodeId;
	}

	public void setAutoCodeId(String autoCodeId) {
		this.autoCodeId = autoCodeId;
	}

}
