package com.jecn.epros.server.action.designer.autoCode.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.autoCode.IAutoCodeAction;
import com.jecn.epros.server.bean.autoCode.AutoCodeBean;
import com.jecn.epros.server.bean.autoCode.AutoCodeNodeData;
import com.jecn.epros.server.bean.autoCode.AutoCodeRelatedBean;
import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.bean.autoCode.ProcessCodeTree;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.service.autoCode.IAutoCodeService;
import com.jecn.epros.server.service.autoCode.IStandardizedAutoCodeService;

public class AutoCodeActionImpl implements IAutoCodeAction {
	private IAutoCodeService autoCodeService;

	private IStandardizedAutoCodeService standardizedAutoCode;

	@Override
	public Long addPorcessCode(ProcessCodeTree codeTree) throws Exception {
		return autoCodeService.addPorcessCode(codeTree);
	}

	@Override
	public void deleteProcessCode(List<Long> ids) throws Exception {
		autoCodeService.deleteProcessCode(ids);
	}

	@Override
	public void updateName(Long id, String newName, Long updatePersonId) throws Exception {
		autoCodeService.updateName(id, newName, updatePersonId);
	}

	@Override
	public List<JecnTreeBean> getChildProcessCodes(Long pId, int type) throws Exception {
		return autoCodeService.getChildProcessCodes(pId, type);
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long id, int type) throws Exception {
		return autoCodeService.getPnodes(id, type);
	}

	public IAutoCodeService getAutoCodeService() {
		return autoCodeService;
	}

	public void setAutoCodeService(IAutoCodeService autoCodeService) {
		this.autoCodeService = autoCodeService;
	}

	public IStandardizedAutoCodeService getStandardizedAutoCode() {
		return standardizedAutoCode;
	}

	public void setStandardizedAutoCode(IStandardizedAutoCodeService standardizedAutoCode) {
		this.standardizedAutoCode = standardizedAutoCode;
	}

	/**
	 * 自动编号保存
	 * 
	 * @param codeBean
	 * @param codeRelateds
	 * @param relatedType
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * @throws Exception
	 */
	@Override
	public AutoCodeBean saveAutoCode(AutoCodeBean codeBean, Long id, int relatedType) throws Exception {
		return autoCodeService.saveAutoCode(codeBean, id, relatedType);
	}

	@Override
	public AutoCodeBean getAutoCodeBean(Long relatedId, int relatedType) throws Exception {
		return autoCodeService.getAutoCodeBean(relatedId, relatedType);
	}

	@Override
	public JecnTreeBean getTreeBeanByName(String name) throws Exception {
		return autoCodeService.getTreeBeanByName(name);
	}

	@Override
	public AutoCodeRelatedBean getAutoCodeRelatedBean(Long relatedId, int relatedType) throws Exception {
		return autoCodeService.getAutoCodeRelatedBean(relatedId, relatedType);
	}

	@Override
	public AutoCodeResultData getStandardizedAutoCode(AutoCodeNodeData codeTempData) throws Exception {
		return standardizedAutoCode.getStandardizedAutoCode(codeTempData);
	}

	@Override
	public boolean reNumItemItemAction(AutoCodeNodeData nodeData, long userId) {
		 return standardizedAutoCode.reNumItemItemAction( nodeData,  userId);
	}
}
