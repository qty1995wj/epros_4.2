package com.jecn.epros.server.action.web.login.ad.addon;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnPath;

/**
 * 
 * 华帝单点登录所需配置信息，在系统启动时需要启动项
 * 
 * @author ZHOUXY
 * 
 */
public class JecnAddonAfterItem {
	private static Logger log = Logger.getLogger(JecnAddonAfterItem.class);
	/** IOR IOR内容 */
	public static String ior = null;
	/** domain DOMAIN */
	public static String domain = null;

	/** ***************** domino系统对接 ****************** */
	/** domino服务器IP地址 */
	public static String dominoIP = null;// http:// oadev01.vatti.com.cn
	/** domino用户名 */
	public static String dominoUser = null;
	/** domino密码 */
	public static String dominoPws = null;
	/** 数据库.nsf文件名称 */
	public static String dominoNsf = null;// http://oadev01.vatti.com.cn/oadata/PublicJecn.nsf
	/** 表单：fm_Main */
	public static String dominoFm = null;// fm_Main
	/** 数据库.nsf的视图名称 */
	public static String dominoVw = null;// vMain

	/** ***************** domino系统对接 ****************** */

	/**
	 * 东航单点登录 读取本地文件数据
	 * 
	 */
	public static void start() {
		// 读取配置文件
		InputStream fis = null;
		Properties properties = new Properties();

		String url = JecnPath.CLASS_PATH + "cfgFile/addon/addon.properties";
		log.info("配置文件路径:" + url);

		try {
			fis = new FileInputStream(url);
			properties.load(fis);

			// 读值
			ior = properties.getProperty("ior");
			domain = properties.getProperty("domain");

			/** ***************** domino系统对接 ****************** */
			/** domino服务器IP地址 */
			dominoIP = properties.getProperty("dominoIP");
			/** domino用户名 */
			dominoUser = properties.getProperty("dominoUser");
			/** domino密码 */
			dominoPws = properties.getProperty("dominoPws");
			/** 数据库.nsf文件名称 */
			dominoNsf = properties.getProperty("dominoNsf");
			/** 表单：fm_Main */
			dominoFm = properties.getProperty("dominoFm");
			/** 数据库.nsf的视图名称 */
			dominoVw = properties.getProperty("dominoVw");
			/** ***************** domino系统对接 ****************** */
		} catch (FileNotFoundException e) {
			log.error("获取文件路径失败:cfgFile/addon/addon.properties", e);
		} catch (IOException e) {
			log.error("读流异常:cfgFile/addon/addon.properties", e);
		} catch (Exception e) {
			log.error("其他异常:cfgFile/addon/addon.properties", e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					log.error(
							"关闭InputStream流异常:cfgFile/addon/addon.properties",
							e);
				}
			}
		}
	}
}
