package com.jecn.epros.server.common;

import java.util.Date;

import com.jecn.epros.server.bean.file.JecnFileBeanT;

public class JecnRelatedDirFileUtil {
	/**
	 * 支持文件目录 ，创建文件目录
	 * 
	 * @param projectId
	 * @param createPeopleId
	 * @param parentId
	 * @param fileName
	 * @param relatedId
	 * @param sortId
	 * @param relatedDirType
	 * @return
	 */
	public static JecnFileBeanT getFileBean(Long projectId, Long createPeopleId, Long parentId, String fileName,
			Long relatedId, Integer sortId, int relatedDirType) {
		// 向文件FileBean添加数据
		JecnFileBeanT fileBean = new JecnFileBeanT();
		// 文件名称
		fileBean.setFileName(fileName);
		// 创建人
		fileBean.setPeopleID(createPeopleId);
		// 更新人
		fileBean.setUpdatePersonId(createPeopleId);
		// 父ID
		fileBean.setPerFileId(parentId);

		if (relatedDirType == 0) {
			// 目录关联流程ID
			fileBean.setFlowId(relatedId);
		} else {
			fileBean.setRuleId(relatedId);
		}

		// 是否是目录 0：目录，1：文件
		fileBean.setIsDir(0);

		// 密级：0是秘密,1是公开
		fileBean.setIsPublic(0L);
		// 隐藏 0:隐藏 1：显示
		int hide = 1;

		fileBean.setHide(hide);
		// 创建时间
		fileBean.setCreateTime(new Date());
		// 项目ID
		fileBean.setProjectId(projectId);
		fileBean.setDelState(0);
		fileBean.setCreateTime(new Date());
		fileBean.setUpdateTime(fileBean.getCreateTime());
		fileBean.setSortId(sortId);
		return fileBean;
	}
}
