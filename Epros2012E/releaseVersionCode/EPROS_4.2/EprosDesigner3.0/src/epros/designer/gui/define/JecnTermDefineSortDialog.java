package epros.designer.gui.define;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class JecnTermDefineSortDialog extends JecnSortDialog {

	public JecnTermDefineSortDialog(JecnTreeNode selectNode, JecnTree jTree) {
		super(selectNode, jTree);
	}

	@Override
	public List<JecnTreeBean> getChild() {
		try {
			return ConnectionPool.getTermDefinitionAction().getChilds(this.getSelectNode().getJecnTreeBean().getId());
		} catch (Exception e) {
			log.error(JecnProperties.getValue("termDefineDownloadError"), e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public boolean saveData(List<JecnTreeDragBean> list) {
		try {
			ConnectionPool.getTermDefinitionAction().updateSortNodes(list,
					this.getSelectNode().getJecnTreeBean().getId(), JecnConstants.getUserId());
			return true;
		} catch (Exception e) {
			log.error("JecnTermDefineSortDialog saveData is error！", e);
		}
		return false;
	}

	protected int[] getHiddenCols() {
		return new int[] { 1, 3, 4 };
	}

}
