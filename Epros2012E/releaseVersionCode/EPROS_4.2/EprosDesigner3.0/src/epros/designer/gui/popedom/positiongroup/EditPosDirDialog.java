package epros.designer.gui.popedom.positiongroup;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;

/**
 * 岗位组目录重命名
 * 
 * @author fuzhh Apr 9, 2013
 * 
 */
public class EditPosDirDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(EditPosDirDialog.class);
	private JTree jTree = null;
	private boolean isOk = false;
	private JecnTreeBean jecnTreeBean = null;

	public boolean isOk() {
		return isOk;
	}

	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}

	public EditPosDirDialog(JecnTreeBean jecnTreeBean, JTree jTree) {
		this.jecnTreeBean = jecnTreeBean;
		this.jTree = jTree;

		this.setLocationRelativeTo(null);
		try {
			this.setName(jecnTreeBean.getName());
		} catch (Exception e) {
			log.error("EditPosDirDialog is error ", e);
		}
	}

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		/*
		 * 名称：
		 */
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		try {
			if (TreeNodeType.positionGroup.equals(jecnTreeBean.getTreeNodeType())) {
				if (!JecnValidateCommon.validateName(this.getName(), promptLab)) {
					return;
				}
			}
			ConnectionPool.getPosGroup().rePositionGroupName(this.getName(), jecnTreeBean.getId(),
					JecnConstants.loginBean.getJecnUser().getPeopleId());
			isOk = true;
		} catch (Exception e) {
			log.error("EditPosDirDialog saveData is error", e);
		}
		this.dispose();
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		JecnTreeNode selectNode = JecnTreeCommon.getTreeNode(jecnTreeBean, jTree);
		if (selectNode != null) {
			return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
		} else {
			try {
				return ConnectionPool.getPosGroup().validateRepeatNameEidt(this.getName(), jecnTreeBean.getId(),
						jecnTreeBean.getPid(), JecnConstants.loginBean.getJecnUser().getPeopleId());
			} catch (Exception e) {
				log.error("EditPosDirDialog validateNodeRepeat is error", e);
			}
		}
		return false;
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
