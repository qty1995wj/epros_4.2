/**
 * SetItemValue_AmountResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class SetItemValue_AmountResponse  implements java.io.Serializable {
    private java.lang.String setItemValue_AmountResult;

    public SetItemValue_AmountResponse() {
    }

    public SetItemValue_AmountResponse(
           java.lang.String setItemValue_AmountResult) {
           this.setItemValue_AmountResult = setItemValue_AmountResult;
    }


    /**
     * Gets the setItemValue_AmountResult value for this SetItemValue_AmountResponse.
     * 
     * @return setItemValue_AmountResult
     */
    public java.lang.String getSetItemValue_AmountResult() {
        return setItemValue_AmountResult;
    }


    /**
     * Sets the setItemValue_AmountResult value for this SetItemValue_AmountResponse.
     * 
     * @param setItemValue_AmountResult
     */
    public void setSetItemValue_AmountResult(java.lang.String setItemValue_AmountResult) {
        this.setItemValue_AmountResult = setItemValue_AmountResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetItemValue_AmountResponse)) return false;
        SetItemValue_AmountResponse other = (SetItemValue_AmountResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.setItemValue_AmountResult==null && other.getSetItemValue_AmountResult()==null) || 
             (this.setItemValue_AmountResult!=null &&
              this.setItemValue_AmountResult.equals(other.getSetItemValue_AmountResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSetItemValue_AmountResult() != null) {
            _hashCode += getSetItemValue_AmountResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetItemValue_AmountResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">SetItemValue_AmountResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("setItemValue_AmountResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "SetItemValue_AmountResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
