package com.jecn.epros.server.action.web.login.ad.wanhua;

import java.util.ResourceBundle;

/**
 * 中睿通信单点登录配置信息
 */
public class JecnWanHuaAfterItem {

    private static ResourceBundle wanHuaConfig;
    public static String TASK_URL;

	static {
		wanHuaConfig = ResourceBundle.getBundle("cfgFile/wanhua/wanhuaServerConfig");
	}

    public static String getValue(String key) {
        return wanHuaConfig.getString(key);
    }
}
