package epros.draw.gui.top.toolbar.right;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import epros.draw.gui.top.toolbar.JecnToolBarPanel;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 
 * 显示以藏的部分工具栏内容，当面板能全部显示工具栏内容时，JecnToolBarRightShowPanel按钮面板不显示
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolBarRightShowPanel extends JPanel implements ActionListener {
	/** 每次增量 */
	private final int currZ = 80;

	/** leftBtn/rightBtn容器 */
	private JPanel btnPanel = null;
	/** 向左移动按钮 */
	private JecnToolBarRightShowButton leftBtn = null;
	/** 向右移动按钮 */
	private JecnToolBarRightShowButton rightBtn = null;

	/** 工具栏对象 */
	private JecnToolBarPanel toolBarPanel = null;

	public JecnToolBarRightShowPanel(JecnToolBarPanel toolBarPanel) {
		this.toolBarPanel = toolBarPanel;
		initComponent();
	}

	private void initComponent() {
		// leftBtn/rightBtn容器
		btnPanel = new JPanel();
		// 向左移动按钮
		leftBtn = new JecnToolBarRightShowButton();
		// 向右移动按钮
		rightBtn = new JecnToolBarRightShowButton();

		this.setOpaque(false);
		this.setBorder(null);
		this.setLayout(new BorderLayout());

		btnPanel.setOpaque(false);
		btnPanel.setBorder(JecnUIUtil.getTootBarBorder());
		btnPanel.setLayout(new GridLayout(1, 2));

		// 图片
		leftBtn
				.setIcon(JecnFileUtil
						.getImageIconforPath("/epros/draw/images/toolbar/right/left.gif"));
		rightBtn
				.setIcon(JecnFileUtil
						.getImageIconforPath("/epros/draw/images/toolbar/right/right.gif"));

		// 点击事件
		leftBtn.addActionListener(this);
		rightBtn.addActionListener(this);

		this.add(btnPanel, BorderLayout.SOUTH);
		btnPanel.add(leftBtn.getJToolBar());
		btnPanel.add(rightBtn.getJToolBar());

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int value = toolBarPanel.getEachPartScrollPane()
				.getHorizontalScrollBar().getValue();
		if (e.getSource() == leftBtn) {
			toolBarPanel.getEachPartScrollPane().getHorizontalScrollBar()
					.setValue(value - currZ);
		} else if (e.getSource() == rightBtn) {
			toolBarPanel.getEachPartScrollPane().getHorizontalScrollBar()
					.setValue(value + currZ);
		}
	}
}
