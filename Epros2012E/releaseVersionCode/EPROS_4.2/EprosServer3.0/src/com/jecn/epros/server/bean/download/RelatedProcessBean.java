package com.jecn.epros.server.bean.download;

import java.io.Serializable;

public class RelatedProcessBean implements Serializable {
	/** 流程类型 1是上游流程,2是下游流程,3是过程流程,4是子流程 */
	private String linecolor;
	/** 流程编号 */
	private String flowNumber;
	/** 流程名称 */
	private String flowName;
	/** 流程ID */
	private Long flowId;
	/** 责任部门 */
	private String dutyOrg=null;

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getLinecolor() {
		return linecolor;
	}

	public void setLinecolor(String linecolor) {
		this.linecolor = linecolor;
	}

	public String getFlowNumber() {
		return flowNumber;
	}

	public void setFlowNumber(String flowNumber) {
		this.flowNumber = flowNumber;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getDutyOrg() {
		return dutyOrg;
	}

	public void setDutyOrg(String dutyOrg) {
		this.dutyOrg = dutyOrg;
	}

}
