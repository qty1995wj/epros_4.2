package epros.designer.gui.task.config;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.TaskTemplet;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 新建项目名称
 * @author 2012-05-23
 *
 */

public class AddTaskTempletDialog extends JecnEditNameDialog {

	private static Logger log = Logger.getLogger(AddTaskTempletDialog.class);
	
	private TaskTemplet taskTemplet;
	private int taskType;
	public AddTaskTempletDialog(int taskType){
		this.taskType=taskType;
		this.setLocationRelativeTo(null);
	}
	
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addTaskTemplate");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		TaskTemplet taskTemplet = new TaskTemplet();
		taskTemplet.setCreatePeople(JecnConstants.loginBean.getJecnUser().getPeopleId());
		taskTemplet.setName(super.getNameTextField().getText().trim());
		taskTemplet.seteName(super.geteNameTextField().getText().trim());
		taskTemplet.setType(this.taskType);
		
		try {
			this.taskTemplet=ConnectionPool.getTaskRecordAction().saveTaskTemplet(taskTemplet);
			this.dispose();
		} catch (Exception e) {
			log.error("AddTaskTempletDialog saveData is error",e);
		}
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
			return ConnectionPool.getTaskRecordAction().validateAddName(name,taskType);
	}

	public TaskTemplet getTaskTemplet() {
		return taskTemplet;
	}

	public void setTaskTemplet(TaskTemplet taskTemplet) {
		this.taskTemplet = taskTemplet;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	
	
}
