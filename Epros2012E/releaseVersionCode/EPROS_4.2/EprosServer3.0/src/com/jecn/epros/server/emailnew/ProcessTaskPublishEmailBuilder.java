package com.jecn.epros.server.emailnew;

import java.util.Date;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.email.buss.ToolEmail;
import com.jecn.epros.server.util.JecnUtil;

public class ProcessTaskPublishEmailBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

		Object[] data = getData();
		JecnTaskHistoryNew history = (JecnTaskHistoryNew) data[0];
		String prfName = data[1].toString();

		return createEmail(user, history, prfName, history.getType());
	}

	/**
	 * 流程文件发布 邮件处理
	 * 
	 * @param JecnUser
	 *           
	 * @param rifId
	 *            关联文件主键ID
	 * @param prfName
	 *            关键文件名称
	 * @param taskType
	 *            类型：0流程图；4：流程地图
	 */
	public EmailBasicInfo createEmail(JecnUser user,
			JecnTaskHistoryNew history, String prfName, int taskType) {

		StringBuffer buf = new StringBuffer();
		// 拟稿人
		if (!JecnCommon.isNullOrEmtryTrim(history.getDraftPerson())) {
			buf.append(JecnUtil.getValue("peopleMakeADraftC"));
			buf.append(history.getDraftPerson());
			buf.append(EmailContentBuilder.strBr);
		}
		// 文件名称
		if (!JecnCommon.isNullOrEmtryTrim(prfName)) {
			buf.append(JecnUtil.getValue("fileNameC") + prfName);
			buf.append(EmailContentBuilder.strBr);
		}
		// 变更说明
		if (!JecnCommon.isNullOrEmtryTrim(history.getModifyExplain())) {
			buf.append(JecnUtil.getValue("changeThatC")
					+ history.getModifyExplain());
			buf.append(EmailContentBuilder.strBr);
		}
		// 发布日期
		buf.append(JecnUtil.getValue("releaseDateC"));
		buf.append(JecnCommon.getStringbyDate(new Date(), "yyyy-MM-dd"));
		buf.append(EmailContentBuilder.strBr);

		// 生成邮件的正文
		EmailContentBuilder contentBuilder = new EmailContentBuilder();
		contentBuilder.setContent(buf.toString());
		contentBuilder.setResourceUrl(getHttpUrl(history.getRelateId(), user
				.getPeopleId(), taskType));

		// 邮件标题 已完成并发布, 请查阅!
		String subject = '"' + prfName + '"' + "," + ToolEmail.IS_FINISH_PUB;

		// 每个流程参与者都要接收邮件，点击邮件连接地址直接查看流程
		EmailBasicInfo emailBasicInfo = new EmailBasicInfo();
		emailBasicInfo.setSubject(subject);
		emailBasicInfo.setContent(contentBuilder.buildContent());
		emailBasicInfo.addRecipients(user);

		return emailBasicInfo;

	}

	private String getHttpUrl(long relatedId, long peopleId, int taskType) {

		// PRF文件类型
		String prfType = "";
		switch (taskType) {
		case 0:// 流程
			prfType = "process";
			break;
		// case 1:// 文件
		// break;
		// case 2:// 制度模板文件
		// break;
		// case 3:// 制度文件
		// break;
		case 4:// 流程地图
			prfType = "processMap";
			break;
		}

		//邮件打开流程图的链接
		String httpUrl = ToolEmail.getHttpUrl(relatedId, prfType, peopleId);

		return httpUrl;
	}

}
