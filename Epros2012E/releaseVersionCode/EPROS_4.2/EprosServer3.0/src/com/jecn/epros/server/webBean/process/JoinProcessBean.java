package com.jecn.epros.server.webBean.process;

import java.util.List;

/**
 * @author yxw 2012-9-26
 * @description：我参与的流程返回数据bean
 */
public class JoinProcessBean {
	
	/**岗位名称*/
	private String postionName;
	private List<RoleProcessBean> roleProcessList;
	public String getPostionName() {
		return postionName;
	}
	public void setPostionName(String postionName) {
		this.postionName = postionName;
	}
	public List<RoleProcessBean> getRoleProcessList() {
		return roleProcessList;
	}
	public void setRoleProcessList(List<RoleProcessBean> roleProcessList) {
		this.roleProcessList = roleProcessList;
	}
	
}
