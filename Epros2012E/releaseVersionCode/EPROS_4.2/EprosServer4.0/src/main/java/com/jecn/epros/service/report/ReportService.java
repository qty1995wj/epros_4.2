package com.jecn.epros.service.report;

import com.jecn.epros.domain.ChineseEnglisListWebBean;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.process.FlowActiveBaseBean;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.report.*;

import java.util.List;
import java.util.Map;

public interface ReportService {

    List<ProcessDetail> processDetailView(Map<String, Object> map);

    List<ProcessScan> processOptimizeView(Map<String, Object> map);

    /**
     * 以kpi为主键获得所有的kpi
     *
     * @param map
     * @return
     */
    List<KpiView> processKpiView(Map<String, Object> map);

    /**
     * 流程信息化统计
     *
     * @param map
     * @return
     */
    List<InformationStatistic> processInformation(Map<String, Object> map);

    /**
     * 流程角色统计
     *
     * @param map
     * @return
     */
    List<RoleStatistic> processRoleNum(Map<String, Object> map);

    /**
     * 流程活动统计
     *
     * @param map
     * @return
     */
    List<ActivityStatistic> processActivityNum(Map<String, Object> map);

    /**
     * 流程信息打开一个饼状图的区
     *
     * @param map
     * @return
     */
    List<FlowActiveBaseBean> processInformationDetail(Map<String, Object> map);

    Inventory getRoleInventory(Map<String, Object> map);

    Inventory getActivityInventory(Map<String, Object> map);

    Inventory getInformationInventory(Map<String, Object> map);

    Inventory primaryManage(PrimaryActivitySearchParam param, Paging paging);

    Map<String,Object> primaryManageStatistic();

    /**
     * 获取中英文 文件数量报表统计
     *
     * @param map
     * @return
     */
    public ChineseEnglisListWebBean getChineseEnglishList(Map<String, Object> map);


    public Inventory reportPermissionsReports(InventoryParam param);

}
