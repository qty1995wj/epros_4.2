package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;

public abstract class AbstractSyncDataFilter {
	/** 日志记录 */
	protected final Log log = LogFactory.getLog(AbstractSyncDataFilter.class);
	protected BaseBean syncData;

	public AbstractSyncDataFilter(BaseBean baseBean) {
		this.syncData = baseBean;
	}

	/**
	 * 过滤公司数据
	 * 
	 * @author weidp
	 * @date 2014-7-29 上午10:07:46
	 */
	public void filterData() {
		BaseBean.syncNotUpdateUsers.clear();
		BaseBean.syncNotUpdateUsers.add("admin");

		addedSpecialDataToSyncData();
		filterDeptDataBeforeCheck();
		filterUserPosDataBeforeCheck();
		removeUnusefulDataAfterCheck();
	}

	/**
	 * 添加需要保留的组织、人员、岗位数据
	 */
	protected abstract void addedSpecialDataToSyncData();

	/**
	 * 删除无用的数据
	 * 
	 * @author weidp
	 * @date 2014-7-29 上午09:48:59
	 * @return
	 */
	public abstract void removeUnusefulDataAfterCheck();

	/**
	 * 过滤部门数据
	 * 
	 * @author weidp
	 * @date 2014-7-26 下午01:43:15
	 */
	protected abstract void filterDeptDataBeforeCheck();

	/**
	 * 过滤人员数据
	 * 
	 * @author weidp
	 * @date 2014-7-26 下午01:43:28
	 */
	protected abstract void filterUserPosDataBeforeCheck();
}
