package epros.designer.util;

import org.apache.log4j.Logger;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

import com.jecn.epros.server.action.designer.autoCode.IAutoCodeAction;
import com.jecn.epros.server.action.designer.control.IJecnDocControlAction;
import com.jecn.epros.server.action.designer.define.ITermDefinitionAction;
import com.jecn.epros.server.action.designer.file.IFileAction;
import com.jecn.epros.server.action.designer.integration.IJecnActivityAction;
import com.jecn.epros.server.action.designer.integration.IJecnControlGuideAction;
import com.jecn.epros.server.action.designer.integration.IJecnRiskAction;
import com.jecn.epros.server.action.designer.popedom.IJecnRoleAction;
import com.jecn.epros.server.action.designer.popedom.IOrganizationAction;
import com.jecn.epros.server.action.designer.popedom.IPersonAction;
import com.jecn.epros.server.action.designer.popedom.IPositionGroupAction;
import com.jecn.epros.server.action.designer.process.IFlowStructureAcion;
import com.jecn.epros.server.action.designer.process.IFlowToolAction;
import com.jecn.epros.server.action.designer.process.IProcessModeAction;
import com.jecn.epros.server.action.designer.process.IProcessTemplateAction;
import com.jecn.epros.server.action.designer.project.ICurProjectAction;
import com.jecn.epros.server.action.designer.project.IProjectAction;
import com.jecn.epros.server.action.designer.rule.IRuleAction;
import com.jecn.epros.server.action.designer.rule.IRuleModeAction;
import com.jecn.epros.server.action.designer.standard.IStandardAction;
import com.jecn.epros.server.action.designer.system.IJecnConfigItemAciton;
import com.jecn.epros.server.action.designer.system.IJecnElementsLibraryAction;
import com.jecn.epros.server.action.designer.system.IJecnFixedEmailAction;
import com.jecn.epros.server.action.designer.system.IProcessRuleTypeAction;
import com.jecn.epros.server.action.designer.task.IJecnTaskRecordAction;
import com.jecn.epros.server.action.designer.task.approve.IJecnCreateTaskAction;
import com.jecn.epros.server.action.designer.tree.IJecnTreeAction;
import com.jecn.epros.server.action.designer.update.JecnUpdateFileAction;

/**
 * 
 * RMI连接类
 * 
 * @author yxw 2012-5-8
 * @description：RMI接口连接
 */
public class ConnectionPool {
	private static Logger log = Logger.getLogger(ConnectionPool.class);

	public static String getServerIp() {
		return serverIp;
	}

	public static void setServerIp(String serverIp) {
		ConnectionPool.serverIp = serverIp;
	}

	public static String getServerPort() {
		return serverPort;
	}

	public static void setServerPort(String serverPort) {
		ConnectionPool.serverPort = serverPort;
	}

	/** 服务器IP */
	private static String serverIp = "127.0.0.1";
	/** 服务端口号 */
	private static String serverPort = "2088";
	/** 角色接口 */
	private static IJecnRoleAction jecnRoleAction = null;
	/** 支持工具接口 */
	private static IFlowToolAction flowToolAction = null;
	/** 制度模板接口 */
	private static IRuleModeAction ruleModeAction = null;
	/** 组织接口 */
	private static IOrganizationAction organizationAction = null;

	/** 岗位组接口 */
	private static IPositionGroupAction positionGroupAction = null;

	private static IPersonAction personAction = null;
	/** 项目接口 */
	private static IProjectAction projectAction = null;

	/** 主项目接口 */
	private static ICurProjectAction curProjectAction = null;
	/** 文件 */
	private static IFileAction fileAction = null;

	private static IFlowStructureAcion processAction = null;
	private static IStandardAction standardAction = null;
	private static IRuleAction ruleAction = null;
	/** 流程模板 */
	private static IProcessModeAction processModeAction = null;
	/** 流程类别 */
	private static IProcessRuleTypeAction processRuleTypeAction = null;

	/** 模型 */
	private static IProcessTemplateAction processTemplateAction = null;
	/** 任务审批提交任务 */
	private static IJecnCreateTaskAction createTaskAction;

	/** 任务审批关联查询及验证接口 */
	private static IJecnTaskRecordAction taskRecordAction;

	/** 任务审批关联查询及验证接口 */
	private static IJecnDocControlAction docControlAction;

	/** 配置信息表（JECN_SET_TABLE）操作接口 */
	private static IJecnConfigItemAciton configAciton;

	/** 设计器更新验证及文件获取接口 */
	private static JecnUpdateFileAction updateFileAction;
	/*** 风险 */
	private static IJecnRiskAction jecnRiskAction;
	/** 活动类别 */
	private static IJecnActivityAction activityAction;
	/** 内控指引知识库操作接口 */
	private static IJecnControlGuideAction controlGuideAction;
	/** 元素库配置 */
	private static IJecnElementsLibraryAction elementsLibraryAction;
	/** 树 接口 */
	private static IJecnTreeAction treeAction;

	/** 发布 固定人员发送邮件消息 */
	private static IJecnFixedEmailAction fixedEmailAction = null;
	/** 术语定义 */
	private static ITermDefinitionAction termDefinitionAction = null;
	/** 字段编号 */
	private static IAutoCodeAction autoCodeAction = null;
//	private static ITaskRPCService taskRPCService = null;

	public static IJecnFixedEmailAction getFixedEmailAction() {
		if (fixedEmailAction == null) {
			try {
				fixedEmailAction = (IJecnFixedEmailAction) getObject(IJecnFixedEmailAction.class,
						JecnFinal.FIXEDEMAIL_SN);
			} catch (Exception e) {
				log.error("ConnectionPool  fixedEmailAction is error：", e);
			}
		}
		return fixedEmailAction;
	}

	public static IJecnControlGuideAction getControlGuideAction() {
		if (controlGuideAction == null) {
			try {
				controlGuideAction = (IJecnControlGuideAction) getObject(IJecnControlGuideAction.class,
						JecnFinal.CONTROLGUIDE_SN);
			} catch (Exception e) {
				log.error("ConnectionPool  controlGuideAction is error：", e);
			}
		}
		return controlGuideAction;
	}

	public static IJecnElementsLibraryAction getElementsLibraryAction() {
		if (elementsLibraryAction == null) {
			try {
				elementsLibraryAction = (IJecnElementsLibraryAction) getObject(IJecnElementsLibraryAction.class,
						JecnFinal.ELEMENTS_LIBRARY_ACTION);
			} catch (Exception e) {
				log.error("ConnectionPool  elementsLibraryAction is error：", e);
			}
		}
		return elementsLibraryAction;
	}

	public static IJecnRiskAction getJecnRiskAction() {
		if (jecnRiskAction == null) {
			try {
				jecnRiskAction = (IJecnRiskAction) getObject(IJecnRiskAction.class, JecnFinal.JECNRISKACTION_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return jecnRiskAction;
	}

	/**
	 * 
	 * 获取配置信息表对象
	 * 
	 * @return IJecnConfigAciton 配置信息表对象
	 */
	public static IJecnConfigItemAciton getConfigAciton() {
		if (configAciton == null) {
			try {
				configAciton = (IJecnConfigItemAciton) getObject(IJecnConfigItemAciton.class,
						JecnFinal.CONFIG_ACTION_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return configAciton;
	}

	public static IProcessTemplateAction getProcessTemplateAction() {
		if (processTemplateAction == null) {
			try {
				processTemplateAction = (IProcessTemplateAction) getObject(IProcessTemplateAction.class,
						JecnFinal.PROCESS_TEMPLATE_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return processTemplateAction;
	}

	public static IProcessRuleTypeAction getProcessRuleTypeAction() {
		if (processRuleTypeAction == null) {
			try {
				processRuleTypeAction = (IProcessRuleTypeAction) getObject(IProcessRuleTypeAction.class,
						JecnFinal.FLOW_TYPE_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return processRuleTypeAction;
	}

	public static IProcessModeAction getProcessModeAction() {
		if (processModeAction == null) {
			try {
				processModeAction = (IProcessModeAction) getObject(IProcessModeAction.class, JecnFinal.FLOW_MODEL_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return processModeAction;
	}

	public static IRuleModeAction getRuleModeAction() {
		if (ruleModeAction == null) {
			try {
				ruleModeAction = (IRuleModeAction) getObject(IRuleModeAction.class, JecnFinal.RULEMODE_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return ruleModeAction;
	}

	public static IJecnRoleAction getJecnRole() {
		if (jecnRoleAction == null) {
			try {
				jecnRoleAction = (IJecnRoleAction) getObject(IJecnRoleAction.class, JecnFinal.JECNROLE_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return jecnRoleAction;
	}

	public static IFlowToolAction getFlowTool() {
		if (flowToolAction == null) {
			try {
				flowToolAction = (IFlowToolAction) getObject(IFlowToolAction.class, JecnFinal.FLOWTOOL_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return flowToolAction;
	}

	public static IPersonAction getPersonAction() {
		if (personAction == null) {
			try {
				personAction = (IPersonAction) getObject(IPersonAction.class, JecnFinal.PERSON_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return personAction;
	}

	public static IOrganizationAction getOrganizationAction() {
		if (organizationAction == null) {
			try {
				organizationAction = (IOrganizationAction) getObject(IOrganizationAction.class,
						JecnFinal.ORGANIZATION_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return organizationAction;
	}

	public static IPositionGroupAction getPosGroup() {
		if (positionGroupAction == null) {
			try {
				positionGroupAction = (IPositionGroupAction) getObject(IPositionGroupAction.class,
						JecnFinal.POSGROUP_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return positionGroupAction;
	}

	public static IProjectAction getProject() {
		if (projectAction == null) {
			try {
				projectAction = (IProjectAction) getObject(IProjectAction.class, JecnFinal.PROJECT_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return projectAction;
	}

	public static ICurProjectAction getCurProject() {
		if (curProjectAction == null) {
			try {
				curProjectAction = (ICurProjectAction) getObject(ICurProjectAction.class, JecnFinal.CURPROJECT_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return curProjectAction;
	}

	public static IFileAction getFileAction() {
		if (fileAction == null) {
			try {
				fileAction = (IFileAction) getObject(IFileAction.class, JecnFinal.FILE_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return fileAction;
	}

	public static IFlowStructureAcion getProcessAction() {
		if (processAction == null) {
			try {
				processAction = (IFlowStructureAcion) getObject(IFlowStructureAcion.class, JecnFinal.PROCESS_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return processAction;
	}

	public static IStandardAction getStandardAction() {
		if (standardAction == null) {
			try {
				standardAction = (IStandardAction) getObject(IStandardAction.class, JecnFinal.STANDARD_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return standardAction;
	}

	public static IRuleAction getRuleAction() {
		if (ruleAction == null) {
			try {
				ruleAction = (IRuleAction) getObject(IRuleAction.class, JecnFinal.RULE_SN);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return ruleAction;
	}

	/**
	 * 获取 任务审批关联查询及验证接口
	 * 
	 * @return IJecnTaskRecord
	 */
	public static IJecnTaskRecordAction getTaskRecordAction() {
		if (taskRecordAction == null) {
			try {
				taskRecordAction = (IJecnTaskRecordAction) getObject(IJecnTaskRecordAction.class,
						JecnFinal.TASK_RECORD_ACTION);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return taskRecordAction;
	}

	/**
	 * 
	 * 任务审批 提交任务
	 * 
	 * @return IJecnDesignerTaskAction
	 */
	public static IJecnCreateTaskAction getDesignerTaskAction() {
		if (createTaskAction == null) {
			try {
				createTaskAction = (IJecnCreateTaskAction) getObject(IJecnCreateTaskAction.class,
						JecnFinal.CREATE_TASK_ACTION);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return createTaskAction;
	}

	/**
	 * 
	 * 任务审批 提交任务
	 * 
	 * @return IJecnDesignerTaskAction
	 */
	public static IJecnDocControlAction getDocControlAction() {
		if (docControlAction == null) {
			try {
				docControlAction = (IJecnDocControlAction) getObject(IJecnDocControlAction.class,
						JecnFinal.DOC_CONTROL_ACTION);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return docControlAction;
	}

	public static JecnUpdateFileAction getUpdateFileAction() {
		if (updateFileAction == null) {
			try {
				updateFileAction = (JecnUpdateFileAction) getObject(JecnUpdateFileAction.class, "updateFileAction");
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return updateFileAction;
	}

	/**
	 * 活动类别
	 * 
	 * @return
	 */
	public static IJecnActivityAction getActivityAction() {
		if (activityAction == null) {
			try {
				activityAction = (IJecnActivityAction) getObject(IJecnActivityAction.class,
						JecnFinal.ACTIVITY_TYPE_ACTION);
			} catch (Exception e) {

				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return activityAction;
	}

	public static IJecnTreeAction getTreeAction() {
		if (treeAction == null) {
			try {
				treeAction = (IJecnTreeAction) getObject(IJecnTreeAction.class, JecnFinal.TREE_ACTION);
			} catch (Exception e) {
				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return treeAction;
	}

	/**
	 * 字段编号
	 * 
	 * @return
	 */
	public static IAutoCodeAction getAutoCodeAction() {
		if (autoCodeAction == null) {
			try {
				autoCodeAction = (IAutoCodeAction) getObject(IAutoCodeAction.class, JecnFinal.AUTO_CODE_ACTION);
			} catch (Exception e) {
				log.error(JecnProperties.getValue("serverConnException"), e);
			}
		}
		return autoCodeAction;
	}

	public static ITermDefinitionAction getTermDefinitionAction() {
		if (termDefinitionAction == null) {
			try {
				termDefinitionAction = (ITermDefinitionAction) getObject(ITermDefinitionAction.class,
						JecnFinal.TREMDEFINE_ACTION);
			} catch (Exception e) {
				log.error("ConnectionPool  termDefinitionAction is error：", e);
			}
		}
		return termDefinitionAction;
	}

//	public static ITaskRPCService getTaskRPCService() {
//		if (taskRPCService == null) {
//			try {
//				taskRPCService = (ITaskRPCService) getObject(ITaskRPCService.class, JecnFinal.TASK_SERVICE);
//			} catch (Exception e) {
//				log.error("ConnectionPool  getTaskRPCService服务连接异常：", e);
//			}
//		}
//		return taskRPCService;
//	}

	/**
	 * @author yxw 2012-5-7
	 * @description:封装RMI的URL
	 * @param serverName
	 * @return
	 */
	private static String getRmiURL(String serverName) {
		return "rmi://" + serverIp + ":" + serverPort + "/" + serverName;
	}

	/**
	 * @author yxw 2012-5-7
	 * @description:获得服务器的接口实现类
	 * @param objectClass
	 * @param serverName
	 * @return
	 * @throws Exception
	 */
	private static Object getObject(Class objectClass, String serverName) throws Exception {
		try {
			RmiProxyFactoryBean factory = new RmiProxyFactoryBean();
			factory.setServiceInterface(objectClass);
			factory.setServiceUrl(getRmiURL(serverName));
			// 解决重启 rmi 的服务器后会出现拒绝连接或找不到服务对象的错误
			factory.setLookupStubOnStartup(false);
			factory.setRefreshStubOnConnectFailure(true);
			factory.afterPropertiesSet();
			return factory.getObject();
		} catch (Exception e) {
			log.error("ConnectionPool getObject is error", e);
			throw e;
		}
	}

	/**
	 * @author yxw 2012-8-17
	 * @description:把所有的接口连接置NULL
	 */
	public static void actionToNull() {
		/** 角色接口 */
		jecnRoleAction = null;
		/** 支持工具接口 */
		flowToolAction = null;
		/** 制度模板接口 */
		ruleModeAction = null;
		/** 组织接口 */
		organizationAction = null;

		/** 岗位组接口 */
		positionGroupAction = null;

		personAction = null;
		/** 项目接口 */
		projectAction = null;

		/** 主项目接口 */
		curProjectAction = null;
		/** 文件 */
		fileAction = null;

		processAction = null;
		standardAction = null;
		ruleAction = null;
		/** 流程模板 */
		processModeAction = null;
		/** 流程类别 */
		processRuleTypeAction = null;
		/** 模型 */
		processTemplateAction = null;
		// 任务审批关联查询及验证接口
		taskRecordAction = null;
		// 任务审批 提交任务
		createTaskAction = null;
		// 文控信息
		docControlAction = null;
		configAciton = null;
		// 版本验证
		updateFileAction = null;
		// 活动类别
		activityAction = null;
		// 风险
		jecnRiskAction = null;
		// 发布：设置固定人员发送邮件消息
		fixedEmailAction = null;
		treeAction = null;
//		taskRPCService = null;
	}
}
