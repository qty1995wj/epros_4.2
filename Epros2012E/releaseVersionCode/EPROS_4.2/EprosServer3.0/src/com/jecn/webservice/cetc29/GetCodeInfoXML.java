
package com.jecn.webservice.cetc29;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCodeInfoXML complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCodeInfoXML">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="modelcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codeField" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="queryXmlStr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCodeInfoXML", propOrder = {
    "modelcode",
    "codeField",
    "queryXmlStr"
})
public class GetCodeInfoXML {

    protected String modelcode;
    protected String codeField;
    protected String queryXmlStr;

    /**
     * Gets the value of the modelcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelcode() {
        return modelcode;
    }

    /**
     * Sets the value of the modelcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelcode(String value) {
        this.modelcode = value;
    }

    /**
     * Gets the value of the codeField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeField() {
        return codeField;
    }

    /**
     * Sets the value of the codeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeField(String value) {
        this.codeField = value;
    }

    /**
     * Gets the value of the queryXmlStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryXmlStr() {
        return queryXmlStr;
    }

    /**
     * Sets the value of the queryXmlStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryXmlStr(String value) {
        this.queryXmlStr = value;
    }

}
