package epros.designer.gui.popedom.positiongroup;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.designer.popedom.IPositionGroupAction;
import com.jecn.epros.server.bean.dataimport.BasePosAndPostionBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class PosGroupTreeMenu extends JPopupMenu {

	private static Logger log = Logger.getLogger(PosGroupTreeMenu.class);
	/** 新建目录 */
	private JMenuItem addDir;

	/** 新建岗位组 */
	private JMenuItem addPosGroup;

	/** 重命名 */
	private JMenuItem reName;

	/** 删除 */
	private JMenuItem delData;

	/** 刷新 */
	private JMenuItem refurbish;

	/** 节点排序 */
	private JMenuItem nodeSort;

	/** 节点移动 */
	private JMenuItem dragNode;

	/** 关联岗位 */
	private JMenuItem updatePosition;

	/** 关联HR基准岗位 */
	private JMenuItem baseMenuItem;

	private JecnTree jTree = null;

	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> listNode = null;

	/** 人员同步类型 */
	private Long syncType = 0L;

	/** 说明 **/
	private JMenuItem posGroupExplainMenu;

	/**
	 * 首页树的岗位组右键菜单初始化
	 * 
	 * @param jTree
	 * @param selectNodes
	 * @param selectMutil
	 */
	public PosGroupTreeMenu(JecnTree jTree, List<JecnTreeNode> selectNodes, int selectMutil) {
		// 获取人员同步类型
		syncType = Long.valueOf(ConnectionPool.getConfigAciton().selectValue(6, "otherUserSyncType"));
		initCompotents(jTree, selectNodes);
	}

	/**
	 * 初始化岗位组右键菜单（设计器首页树）
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-21 时间：下午04:22:30
	 * @param jTree2
	 * @param selectNodes
	 */
	private void initCompotents(JecnTree jTree, List<JecnTreeNode> selectNodes) {
		this.jTree = jTree;
		this.listNode = selectNodes;
		if (selectNodes.size() > 0) {
			this.selectNode = selectNodes.get(0);
		}
		doInitCompotents();
		if (selectNodes.size() == 1) {
			this.singleSelect();
		} else {
			this.mutilSelect();
		}
	}

	/**
	 * 初始化右键菜单内的容器
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-21 时间：下午04:29:00
	 */
	private void doInitCompotents() {
		// 新建项目
		addDir = new JMenuItem(JecnProperties.getValue("newDir"));
		// 新建岗位组
		addPosGroup = new JMenuItem(JecnProperties.getValue("addPositionGroup"));
		// 重命名
		reName = new JMenuItem(JecnProperties.getValue("rename"));
		// 删除
		delData = new JMenuItem(JecnProperties.getValue("delete"));
		// 刷新
		refurbish = new JMenuItem(JecnProperties.getValue("refresh"));
		// 节点排序
		nodeSort = new JMenuItem(JecnProperties.getValue("nodeSort"));
		// 节点移动
		dragNode = new JMenuItem(JecnProperties.getValue("nodeMove"));
		// 关联岗位
		updatePosition = new JMenuItem(JecnProperties.getValue("connPosition"));

		// 关联HR基准岗位
		baseMenuItem = new JMenuItem(JecnProperties.getValue("relBasePostion"));

		posGroupExplainMenu = new JMenuItem(JecnProperties.getValue("posGroupExplain"));

		baseMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String nodeId = selectNode.getJecnTreeBean().getId().toString();
				BasePosAndPostionBean basePosAndPostionBean = ConnectionPool.getPosGroup().getJecnBaseBean(null, null,
						nodeId);
				AddBasePosDialog addBasePosDialog = new AddBasePosDialog(basePosAndPostionBean);
				addBasePosDialog.setSelectNode(selectNode);
				addBasePosDialog.setVisible(true);
			}
		});

		posGroupExplainMenu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				addPosGroupExplain();
			}
		});
		// 新建目录监听
		addDir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDirPerformed();
			}

		});
		// 新建岗位组
		addPosGroup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addGroupPerformed();
			}

		});
		// 重命名
		reName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reNamePerformed();
			}

		});
		// 删除
		delData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deletePerformed();
			}

		});
		// 刷新
		refurbish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refurbishPerformed();
			}

		});
		// 节点排序
		nodeSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nodeSortPerformed();
			}

		});
		// 节点移动
		dragNode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dragNodePerformed();
			}

		});
		// 关联岗位
		updatePosition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updatePositionPerformed();
			}

		});
		// 是否有设计权限
		int authType = JecnTreeCommon.isAuthNode(selectNode);
		if (authType == 2) {
			this.add(addDir);
			this.add(addPosGroup);
			this.addSeparator();
			this.add(reName);
			this.add(delData);
			this.add(refurbish);
			this.add(nodeSort);
			this.add(dragNode);
			this.addSeparator();
			this.add(updatePosition);
			this.add(posGroupExplainMenu);
			if (syncType == 8) {
				this.add(baseMenuItem);
			}
		} else if (authType == 1) {
			// this.add(refurbish);
			this.add(addDir);
			this.add(addPosGroup);
			this.addSeparator();
			this.add(reName);
			this.add(refurbish);
			this.add(nodeSort);
			this.add(dragNode);
			this.addSeparator();
			this.add(updatePosition);
			this.add(posGroupExplainMenu);
			if (syncType == 8) {
				this.add(baseMenuItem);
			}
		} else {
			this.add(refurbish);
		}
	}

	/***************************************************************************
	 * 添加岗位组目录
	 */
	private void addDirPerformed() {
		if (selectNode == null) {
			return;
		}
		AddPosGroupDirDialog addPosGroupDirDialog = new AddPosGroupDirDialog(selectNode, jTree);
		addPosGroupDirDialog.setVisible(true);

	}

	/***************************************************************************
	 * 添加岗位组说明
	 */
	private void addPosGroupExplain() {
		if (selectNode == null) {
			return;
		}
		AddPosGroupExplainDialog addPosGroupExplainDialog = new AddPosGroupExplainDialog(selectNode);
		addPosGroupExplainDialog.setVisible(true);
	}

	/***************************************************************************
	 * 添加岗位组
	 */
	private void addGroupPerformed() {
		if (selectNode == null) {
			return;
		}
		AddPosGroupDialog addPosGroupDialog = new AddPosGroupDialog(selectNode, jTree);
		addPosGroupDialog.setVisible(true);
	}

	/**
	 * 重命名
	 */
	private void reNamePerformed() {
		if (selectNode == null) {
			return;
		}
		EditPosDirDialog editPosDirDialog = new EditPosDirDialog(selectNode.getJecnTreeBean(), jTree);
		editPosDirDialog.setVisible(true);
		if (editPosDirDialog.isOk()) {
			// 重命名
			JecnTreeCommon.reNameNode(jTree, selectNode, editPosDirDialog.getName());
			JecnTreeCommon.selectNode(jTree, selectNode);
		}
	}

	/**
	 * 删除
	 */
	private void deletePerformed() {
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		if (JecnConstants.isMysql) {
			listIds = JecnTreeCommon.getAllChildIds(listNode);
		} else {
			for (JecnTreeNode node : listNode) {
				listIds.add(node.getJecnTreeBean().getId());
			}
		}
		if (listIds.size() == 0) {
			return;
		}
		try {
			ConnectionPool.getPosGroup().deletePositionGroups(listIds, JecnConstants.projectId,
					JecnConstants.loginBean.getJecnUser().getPeopleId());
		} catch (Exception e) {
			log.error("PosGroupTreeMenu deletePerformed is error", e);
		}
		/** 删除节点 */
		JecnTreeCommon.removeNodes(jTree, listNode);
		/**
		 * 删除搜索表格中的数据
		 */
		// 要删除的数据名称
		// if (jecnManageDialog != null) {
		// Vector<Vector<Object>> vs = ((DefaultTableModel)
		// jecnManageDialog.getJecnTable().getModel())
		// .getDataVector();
		// for (int i = vs.size() - 1; i >= 0; i--) {
		// Vector<Object> vob = vs.get(i);
		// for (Long longId : listIds) {
		// if (vob.get(1).toString().equals(longId.toString())) {
		// ((DefaultTableModel)
		// jecnManageDialog.getJecnTable().getModel()).removeRow(i);
		// }
		// }
		// }
		// }

		JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("deleteSuccess"));
	}

	/**
	 * 刷新
	 */
	private void refurbishPerformed() {
		if (selectNode == null) {
			return;
		}
		JecnTreeBean b = selectNode.getJecnTreeBean();
		Long id = b.getId();
		try {
			List<JecnTreeBean> list = null;
			IPositionGroupAction positionGroupAction = ConnectionPool.getPosGroup();
			if (b.getTreeNodeType() == TreeNodeType.positionGroup) {
				list = positionGroupAction.getPosByPositionGroupId(id);
			} else {
				list = positionGroupAction.getChildPositionGroup(id, JecnConstants.projectId);

			}
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("PosGroupTreeMenu refurbishPerformed is error", ex);
		}
	}

	/**
	 * 节点排序
	 */
	private void nodeSortPerformed() {
		PosGroupSortDialog posGroupSortDialog = new PosGroupSortDialog(selectNode, jTree);
		posGroupSortDialog.setVisible(true);
	}

	/**
	 * 节点移动
	 */
	private void dragNodePerformed() {
		if (!JecnDesignerCommon.validateRepeatNodesName(listNode)) {
			return;
		}
		PosGrouMoveChooseDialog posGroupNodeMoveDialog = new PosGrouMoveChooseDialog(listNode, jTree);
		posGroupNodeMoveDialog.setVisible(true);
	}

	/***************************************************************************
	 * 关联岗位
	 */
	private void updatePositionPerformed() {
		if (selectNode == null) {
			return;
		}
		if (selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.positionGroup) {
			UpdatePosToGroupDialog updatePosToGroupDialog = new UpdatePosToGroupDialog(selectNode.getJecnTreeBean()
					.getId());
			updatePosToGroupDialog.setVisible(true);
		}
		refurbishPerformed();// 刷新节点
	}

	/**
	 * 初始化
	 */
	private void init() {
		addDir.setEnabled(false);
		addPosGroup.setEnabled(false);
		reName.setEnabled(false);
		delData.setEnabled(false);
		refurbish.setEnabled(false);
		nodeSort.setEnabled(false);
		dragNode.setEnabled(false);
		updatePosition.setEnabled(false);
		baseMenuItem.setEnabled(false);
		posGroupExplainMenu.setEnabled(false);
	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		JecnTreeCommon.autoExpandNode(jTree, selectNode);
		init();
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case positionGroupRoot:
			// 新建目录
			addDir.setEnabled(true);
			// 新建岗位组
			addPosGroup.setEnabled(true);
			// 排序
			nodeSort.setEnabled(true);
			// 刷新
			refurbish.setEnabled(true);
			break;
		// 目录节点
		case positionGroupDir:
			// 新建目录
			addDir.setEnabled(true);
			// 新建岗位组
			addPosGroup.setEnabled(true);
			// 重命名
			reName.setEnabled(true);
			// 删除
			delData.setEnabled(true);
			// 排序
			nodeSort.setEnabled(true);
			// 移动
			dragNode.setEnabled(true);
			// 刷新
			refurbish.setEnabled(true);
			break;
		// 岗位组节点
		case positionGroup:
			// 重命名
			reName.setEnabled(true);
			// 删除
			delData.setEnabled(true);
			// 刷新
			refurbish.setEnabled(true);
			// 移动
			dragNode.setEnabled(true);
			posGroupExplainMenu.setEnabled(true);
			// 岗位和基准岗位设置
			isVlidateGroupRefBasePos();
			break;
		default:
			break;
		}

	}

	/**
	 * 验证是否存在岗位组关联基准岗位
	 * 
	 * @Title: isVlidateGroupRefBasePos
	 * @author ZXH
	 * @date 2015-7-17 下午02:48:19
	 */
	private void isVlidateGroupRefBasePos() {
		if (selectNode == null) {
			return;
		}
		Long groupId = selectNode.getJecnTreeBean().getId();
		// 1:基准岗位匹配，2岗位匹配 ;0:未匹配
		int count = 0;
		try {
			count = ConnectionPool.getPosGroup().posGroupRelCount(groupId);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("postError"));
			e.printStackTrace();
		}
		if (count == 1) {// 基准岗位匹配
			// 关联基准岗位
			baseMenuItem.setEnabled(true);
			// 关联岗位
			updatePosition.setEnabled(false);
		} else if (count == 2) {// 岗位
			// 关联基准岗位
			baseMenuItem.setEnabled(false);
			// 关联岗位
			updatePosition.setEnabled(true);
		} else {// 未匹配
			// 关联基准岗位
			baseMenuItem.setEnabled(true);
			// 关联岗位
			updatePosition.setEnabled(true);
		}
	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		// 删除
		delData.setEnabled(true);
		// 移动
		dragNode.setEnabled(true);
	}

	public void setListNode(List<JecnTreeNode> listNode) {
		this.listNode = listNode;
		if (listNode.size() == 1) {
			this.selectNode = listNode.get(0);
			this.singleSelect();
		} else {
			this.mutilSelect();
		}
	}

	public List<JecnTreeNode> getListNode() {
		return listNode;
	}

}
