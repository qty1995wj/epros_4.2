package epros.draw.unredo.data;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.line.JecnBaseDividingLinePanel;

/**
 * 
 * 分割线属性备份类，只备份分割线重要的属性
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUnRedoDividingLineData {

	/** 备份分割线数据 */
	private JecnBaseDivedingLineData divedingLineDataClone = null;
	/** 分割线 */
	private JecnBaseDividingLinePanel dividingLine = null;

	public JecnUnRedoDividingLineData(JecnBaseDividingLinePanel dividingLine) {
		if (dividingLine == null) {
			JecnLogConstants.log_JecnUnRedoDividingLineData
					.error("JecnUnRedoDividingLineData(DividingLine dividingLine)异常：参数为空");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.divedingLineDataClone = dividingLine.getFlowElementData().clone();
		this.dividingLine = dividingLine;
	}

	/**
	 * 
	 * 获取分割线的备份数据对象
	 * 
	 * @return JecnBaseDivedingLineData
	 */
	public JecnBaseDivedingLineData getDivedingLineDataClone() {
		return divedingLineDataClone;
	}

	public JecnBaseDividingLinePanel getDividingLine() {
		return dividingLine;
	}

	/**
	 * 
	 * 获取分割线数据对象
	 * 
	 * @return JecnBaseDivedingLineData
	 */
	public JecnBaseDivedingLineData getDivedingLineData() {
		return dividingLine.getFlowElementData();
	}

}
