package epros.designer.gui.system.config.ui;

import java.awt.GridBagLayout;

import javax.swing.JPanel;

import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 面板超类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnAbstractJPanel extends JPanel {
	/** 配置界面 */
	protected JecnAbtractBaseConfigDialog dialog = null;

	public JecnAbstractJPanel(JecnAbtractBaseConfigDialog dialog) {
		if (dialog == null) {
			throw new IllegalArgumentException("JecnAbstractJPanel is error");
		}
		this.dialog = dialog;

		initComponents();
	}

	/**
	 * 
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 背景
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 开始布局
		this.setLayout(new GridBagLayout());
	}

	public JecnAbtractBaseConfigDialog getDialog() {
		return dialog;
	}
}
