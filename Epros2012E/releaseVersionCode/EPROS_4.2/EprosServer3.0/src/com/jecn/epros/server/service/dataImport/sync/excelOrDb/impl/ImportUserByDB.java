package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DBConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 
 * 基于访问数据技术同步数据
 * 
 */
public class ImportUserByDB extends AbstractImportUser {
	protected final Log log = LogFactory.getLog(this.getClass());
	/** 部门 */
	private List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
	/** 岗位 */
	private List<UserBean> userBeanList = new ArrayList<UserBean>();
	/** 执行读取标识(true:表示执行过；false：标识没有执行过) */
	protected boolean readedFlag = false;

	List<String> banDepts = new ArrayList(); // 需要过滤掉的部门

	public ImportUserByDB(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 
	 * 获取部门数据集合
	 * 
	 * @return
	 */
	public List<DeptBean> getDeptBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();

		return deptBeanList;
	}

	/**
	 * 
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	public List<UserBean> getUserBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();

		return userBeanList;
	}

	/**
	 * @throws BsException
	 * @throws Exception
	 * 
	 * 
	 * 
	 */
	private void readDB() throws Exception {
		// 执行过就不执行
		if (readedFlag) {
			return;
		}
		// Excel文件读取，部门、岗位、人员数据都全部取出来
		try {
			selectDB((DBConfigBean) configBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * 查询数据库返回查询结果
	 * 
	 * @param configBean
	 */
	private void selectDB(DBConfigBean configBean) throws Exception {
		// 部门查询返回记录集
		ResultSet resDept = null;
		// 人员查询返回记录集
		ResultSet resUser = null;
		Statement stmt = null;
		Connection conn = null;
		try {
			// 加载的驱动类
			Class.forName(configBean.getDriver());
			// 创建数据库连接
			conn = DriverManager.getConnection(configBean.getUrl(), configBean.getUsername(), configBean.getPassword());

			// 创建一个Statement
			stmt = conn.createStatement();
			// 执行查询，返回结果集
			// 部门
			resDept = stmt.executeQuery(configBean.getDeptSql());

			log.info("获取部门成功！");
			getDpetList(resDept);
			log.info("部门处理成功！");
			// 人员
			resUser = stmt.executeQuery(configBean.getUserSql());

			getUserList(resUser);

			// 更新数据状态
			String updateSqls = configBean.getUpdateSql();
			if (StringUtils.isNotBlank(updateSqls)) {
				String[] sqls = updateSqls.split("##");
				for (String sql : sqls) {
					stmt.executeUpdate(sql);
				}
			}
			readedFlag = true;
		} catch (ClassNotFoundException e) {
			log.error("ImportUserByDB类的selectDB方法加驱动时出错：" + e);
			throw e;
		} catch (SQLException e) {
			log.error("ImportUserByDB类的selectDB方法创建连接时出错：" + e);
			throw e;
		} finally {
			if (resUser != null) {// 关闭记录集
				try {
					resUser.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭人员记录集（ResultSet）时出错：" + e);
					throw e;
				}
			}
			if (resDept != null) {// 关闭记录集
				try {
					resDept.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭部门记录集（ResultSet）时出错：" + e);
					throw e;
				}
			}
			if (stmt != null) { // 关闭声明
				try {
					stmt.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭声明（Statement）时出错：" + e);
					throw e;
				}
			}
			if (conn != null) { // 关闭连接对象
				try {
					conn.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭连接对象（Connection）时出错：" + e);
					throw e;
				}
			}
		}
	}

	/**
	 * 
	 * 把数据取出结果集存储到bean对象中
	 * 
	 * @param resDept
	 * @throws SQLException
	 */
	protected void getDpetList(ResultSet resDept) throws SQLException {
		List<String> l = new ArrayList<String>();
		log.info("部门处理开始");
		while (resDept.next()) {
			DeptBean deptBean = new DeptBean();
			// 部门编号
			String deptNum = resDept.getString(1);
			// 部门名称
			String deptName = resDept.getString(2);
			// 上级部门编号
			String perDeptNum = resDept.getString(3);
			// 部门编号
			deptBean.setDeptNum(SyncTool.emtryToNull(deptNum));
			// 部门名称
			deptBean.setDeptName(SyncTool.emtryToNull(deptName));
			// 上级部门编号
			deptBean.setPerDeptNum(SyncTool.emtryToNull(perDeptNum));

			int column = resDept.getMetaData().getColumnCount();

			if (column > 3 && "1".equals(resDept.getString(4))) {
				l.add(deptNum);
			}

			this.deptBeanList.add(deptBean);
		}

		List<DeptBean> outList = new ArrayList<DeptBean>(this.deptBeanList);

		for (String num : l) {
			this.banDepts.addAll(banDepts(num));
		}
		if(banDepts.isEmpty()){
			return;
		}
		for (DeptBean b : this.deptBeanList) {
			if (this.banDepts.contains(b.getDeptNum())) {
				outList.remove(b);
			}
		}
		this.deptBeanList = outList;

	}

	/**
	 * 
	 * 把数据取出结果集存储到bean对象中
	 * 
	 * @param resUser
	 * @throws SQLException
	 */
	protected void getUserList(ResultSet resUser) throws SQLException {
		while (resUser.next()) {
			UserBean userBean = new UserBean();

			// 下面的顺序不要轻易改变
			// 登录名称
			String userNum = resUser.getString(1);
			// 真实姓名
			String trueName = resUser.getString(2);
			// 任职岗位编号
			String posNum = resUser.getString(3);
			// 任职岗位名称
			String posName = resUser.getString(4);
			// 岗位所属部门编号
			String deptNum = resUser.getString(5);
			// 邮箱地址
			String email = resUser.getString(6);
			// 内外网邮件标识(内网:0 外网:1)
			String emailType = resUser.getString(7);
			// 联系电话
			String phone = resUser.getString(8);

			// 登录名称
			userBean.setUserNum(SyncTool.emtryToNull(userNum));
			// 真实姓名
			userBean.setTrueName(SyncTool.emtryToNull(trueName));
			// 任职岗位编号
			userBean.setPosNum(SyncTool.emtryToNull(posNum));
			// 任职岗位名称
			userBean.setPosName(SyncTool.emtryToNull(posName));
			if (banDepts.contains(deptNum)) {
				continue;
			}
			// 岗位所属部门编号
			userBean.setDeptNum(SyncTool.emtryToNull(deptNum));
			// 邮箱地址
			userBean.setEmail(SyncTool.emtryToNull(email));
			// 邮箱、邮箱类型不等于空且是“0”，“1”
			if (SyncTool.isEmailTypeStr(emailType)) {
				// 内外网邮件标识(内网:0 外网:1)
				userBean.setEmailType(Integer.valueOf(emailType));
			}
			// 联系电话
			userBean.setPhone(SyncTool.emtryToNull(phone));

			int column = resUser.getMetaData().getColumnCount();
			if (column == 9) {
				// 域名
				String domainName = resUser.getString(9);
				userBean.setDomainName(domainName);
			}
			this.userBeanList.add(userBean);
		}

	}

	public List<String> banDepts(String Num) {
		List<String> l = new ArrayList<String>();
		for (DeptBean banDept : deptBeanList) {
			if (banDept.getDeptNum().equals(Num)) {
				l.add(banDept.getDeptNum());
			}
			if (banDept.getPerDeptNum().equals(Num)) {
				l.addAll(banDepts(banDept.getDeptNum()));
			}
		}
		return l;
	}

}
