package com.jecn.epros.server.service.dataImport.sync.xmlImport.dbSource;

import java.util.List;

import jxl.write.WriteException;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.DataBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.XmlStructureBean;

public interface IXmlDataImportService {

	void writeConfigFile(DataBean dataBean);

	void createXmlStructure(String userFilePath) throws Exception;

	List<XmlStructureBean> getXmlStructure() throws Exception;

	boolean importExcel();

	boolean saveOrg(String projectId);

	void excelRead(String projectId) throws Exception;

	void writeExcel(String fileName) throws WriteException;

	void synchronizationPeoples() throws Exception;

	List<JecnTreeBean> getChildOrgsAndPositon(Long id, boolean isLeaf)
			throws Exception;

}
