package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseCircle;

/**
 * 石勘院 结束元素
 * 
 *@author ZXH
 * @date 2016-8-27上午11:27:52
 */
public class EndFigureAR extends BaseCircle {

	public EndFigureAR(JecnFigureData flowElementData) {
		super(flowElementData);
	}
}
