package com.jecn.epros.server.bean.download;

import java.io.Serializable;

public class RuleFileBean implements Serializable {
	/** 制度标题ID*/
	private Long ruleTitleId;
	/** 文件名称*/
	private String fileName;
	/** 文件编号*/
	private String fileInput;
	
	public Long getRuleTitleId() {
		return ruleTitleId;
	}
	public void setRuleTitleId(Long ruleTitleId) {
		this.ruleTitleId = ruleTitleId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileInput() {
		return fileInput;
	}
	public void setFileInput(String fileInput) {
		this.fileInput = fileInput;
	}
}
