package epros.designer.gui.task.config;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import com.jecn.epros.server.bean.task.TaskConfigItem;

import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

/**
 * 
 * 任务审批环节面板 包含table面板和按钮面板 任务模板显示
 */
public class TempletConfigPanel extends JecnPanel implements ActionListener {

	/** 主导发布类型面板 */
	private JecnPanel otherPanel = null;
	/** 任务审批配置table项 **/
	private TempletConfigTableScrollPane taskTablePanel;
	/** 任务审批table和按钮面板panel **/
	private JecnPanel centerPanel;
	/** 主导发布类型:task_whoConntrolLead */
	private JLabel docPubTypeLabel = null;
	/** 文控主导审核发布 */
	private JRadioButton docControlLeadRadioButton = null;
	/** 拟稿人主导发布 */
	private JRadioButton docNotControlLeadRadioButton = null;
	/** 主导审批类型数据 **/
	private TaskConfigItem approveType = null;

	private List<TaskConfigItem> configs;

	public TempletConfigPanel(List<TaskConfigItem> configs) {
		this.configs = configs;
		initComponents();
		initLayout();
		initData(this.configs);
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {

		centerPanel = new JecnPanel();
		// 其他项面板
		otherPanel = new JecnPanel();

		taskTablePanel = new TempletConfigTableScrollPane(configs);

		// 主导发布类型:
		docPubTypeLabel = new JLabel(JecnProperties.getValue("task_whoConntrolLead"));
		// 文控主导审核发布
		docControlLeadRadioButton = new JRadioButton(JecnProperties.getValue("task_docControllead"));
		// 非文控主导审核发布
		docNotControlLeadRadioButton = new JRadioButton(JecnProperties.getValue("task_notDocControlLead"));

		// 其他项面板
		otherPanel.setOpaque(false);
		otherPanel.setBorder(null);

		// 添加按钮组
		ButtonGroup group = new ButtonGroup();
		group.add(docControlLeadRadioButton);
		group.add(docNotControlLeadRadioButton);
		docControlLeadRadioButton.setEnabled(false);
		docNotControlLeadRadioButton.setEnabled(false);

		// 事件
		docControlLeadRadioButton.addActionListener(this);
		docNotControlLeadRadioButton.addActionListener(this);
		// 命令按钮名称
		docControlLeadRadioButton.setActionCommand("docControlLeadRadioButton");
		docNotControlLeadRadioButton.setActionCommand("docNotControlLeadRadioButton");

		docControlLeadRadioButton.setOpaque(false);
		docNotControlLeadRadioButton.setOpaque(false);
		// 默认
		docNotControlLeadRadioButton.setSelected(true);
/*		
		if(JecnConfigTool.isHKWSOperType()){
			otherPanel.setVisible(false);
		}*/

	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		centerPanel.setLayout(new BorderLayout());
		centerPanel.add(taskTablePanel, BorderLayout.CENTER);
		centerPanel.add(otherPanel, BorderLayout.SOUTH);

		this.setLayout(new BorderLayout());
		this.add(centerPanel, BorderLayout.CENTER);

		Insets insets = new Insets(5, 5, 5, 5);

		// 流程责任人:标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(docPubTypeLabel, c);
		// 流程责任人:人员
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(docControlLeadRadioButton, c);
		// 流程责任人: 岗位
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(docNotControlLeadRadioButton, c);
		// 空闲区域
		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		otherPanel.add(new JLabel(), c);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configs
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 * @throws Exception
	 */
	public void initData(List<TaskConfigItem> configs) {
		this.configs = configs;

		// 主导审批类型
		approveType = taskTablePanel.getApproveType();
		if (approveType != null) {
			if (approveType.getValue().equals("0")) {
				docNotControlLeadRadioButton.setSelected(true);
			} else if (approveType.getValue().equals("1")) {
				docControlLeadRadioButton.setSelected(true);
			}
		}
		taskTablePanel.resetData(this.configs);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

	}

	public JecnPanel getOtherPanel() {
		return otherPanel;
	}

	public void setOtherPanel(JecnPanel otherPanel) {
		this.otherPanel = otherPanel;
	}

	public JecnPanel getCenterPanel() {
		return centerPanel;
	}

	public void setCenterPanel(JecnPanel centerPanel) {
		this.centerPanel = centerPanel;
	}

	public JLabel getDocPubTypeLabel() {
		return docPubTypeLabel;
	}

	public void setDocPubTypeLabel(JLabel docPubTypeLabel) {
		this.docPubTypeLabel = docPubTypeLabel;
	}

	public JRadioButton getDocControlLeadRadioButton() {
		return docControlLeadRadioButton;
	}

	public void setDocControlLeadRadioButton(JRadioButton docControlLeadRadioButton) {
		this.docControlLeadRadioButton = docControlLeadRadioButton;
	}

	public JRadioButton getDocNotControlLeadRadioButton() {
		return docNotControlLeadRadioButton;
	}

	public void setDocNotControlLeadRadioButton(JRadioButton docNotControlLeadRadioButton) {
		this.docNotControlLeadRadioButton = docNotControlLeadRadioButton;
	}

	public TaskConfigItem getApproveType() {
		return approveType;
	}

	public void setApproveType(TaskConfigItem approveType) {
		this.approveType = approveType;
	}
}
