package epros.designer.gui.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.popedom.LoginBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class JecnConstants {

	public static final int detailPopupMenuWidth = 250;
	/** 当前项目Id */
	public static Long projectId;
	/** 是否是Mysql */
	public static boolean isMysql = false;
	/** 用户登录信息 */
	public static LoginBean loginBean = null;

	/** 设计权限 能显示节点 */
	public static List<String> getDesignAuthFlows = null;

	public final static String ADMIN = "admin";
	/** true： 完整版 false：标准版 */
	/** 版本类型 0(p1)：有浏览端标准版（默认）；99(p2)：有浏览端完整版；1(d2)：无浏览端标准版；100(d3)：无浏览端完整版 */
	public static int versionType = -1;
	/** 要移动的节点类型 */
	public static TreeNodeType moveNodeType = null;

	/** 流程、架构树节点是否显示编号 0：默认都不选中，1：选中 */
	public static boolean isProcessShowNumber = false;
	/** true:显示项目管理 */
	public static boolean isShowProjectManagement;

	public static Map<Long, String> processRelatedTaskTemplet = new HashMap<Long, String>();
	public static Map<Long, String> processMapRelatedTaskTemplet = new HashMap<Long, String>();
	public static Map<Long, String> fileRelatedTaskTemplet = new HashMap<Long, String>();
	public static Map<Long, String> ruleRelatedTaskTemplet = new HashMap<Long, String>();

	/** 废止任务模板集合 */
	public static Map<Long, String> processRelatedAbolishTaskTemplet = new HashMap<Long, String>();
	public static Map<Long, String> processMapRelatedAbolishTaskTemplet = new HashMap<Long, String>();
	public static Map<Long, String> fileRelatedAbolishTaskTemplet = new HashMap<Long, String>();
	public static Map<Long, String> ruleRelatedAbolishTaskTemplet = new HashMap<Long, String>();

	/**
	 * @author yxw 2012-11-27
	 * @description:得到用户ID
	 * @return
	 */
	public static long getUserId() {
		return loginBean.getJecnUser().getPeopleId();
	}

	/**
	 * @author yxw 2013-7-22
	 * @description:0(p1)：有浏览端标准版（默认）；99(p2)：有浏览端完整版； 1(D2)：无浏览端标准版；
	 *                                                100(D3)：无浏览端完整版
	 * @return
	 */
	public static boolean isPubShow() {
		if (JecnConstants.versionType == 1 || JecnConstants.versionType == 100) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 是否是标准版版本
	 * 
	 * @return boolean true：是标准版本；false：不是标准版本
	 */
	public static boolean isEPS() {
		if (JecnConstants.versionType == 1 || JecnConstants.versionType == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 是否是完整版
	 * 
	 * @return boolean true：是完整版；false：不是完整版
	 */
	public static boolean isEpros() {
		if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {
			return true;
		}
		return false;
	}

}
