package epros.draw.gui.workflow.aid;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.JecnXYData;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.JecnEditPortPanel;
import epros.draw.gui.figure.unit.JecnFigureRefManhattanLine;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 快捷添加图形的主类
 * 
 * 此面板包括：四个小三角图标以及小三角图标所关联的快速添加图形面板
 * 
 * 通过点击图形四周的其中一个小三角图标时，显示的快速添加流程元素的面板
 * 
 * 点击快速添加流程元素的面板中某一个图标按钮直接在面板上添加一个图形
 * 
 * @author ZHOUXY
 * 
 */
public class JecnLinkFigureMainProcess implements MouseListener, ActionListener {
	/** 快捷按钮选中边框 */
	private Border shortcutBorder = BorderFactory.createLineBorder(JecnUIUtil.getAidShortCutBorderColor(), 3);
	/** 快捷创建的连接线类型 */
	private MapElemType shortcutLineMapElemType = MapElemType.ManhattanLine;

	/** 四个小三角管理对象 */
	private JecnTriangleButtonsManager linkButtonManager = null;
	/** 快捷面板管理对象 */
	private JecnShortcutButtonManager shortcutManager = null;

	/** 辅助面板 */
	private JecnAidCompCollection aidCompCollection = null;

	/** 左面小三角图形 */
	private JecnLinkTrianglesButton leftLinkFigure = null;
	/** 右面小三角图形 */
	private JecnLinkTrianglesButton rightLinkFigure = null;
	/** 上面小三角图形 */
	private JecnLinkTrianglesButton topLinkFigure = null;
	/** 下面小三角图形 */
	private JecnLinkTrianglesButton bottomLinkFigure = null;

	/** 快速添加流程元素的面板 */
	private JecnShortcutLinkFigurePanel linkPanel = null;

	/** 图形:被操作的图形 */
	private JecnBaseFigurePanel figure = null;
	/** 需要显示的小三角 */
	private List<JecnLinkTrianglesButton> linkTrianglesButtonList = null;

	/** 进入快捷添加图形按钮时，生成的临时图形 */
	private JecnBaseFigurePanel tmpShortcutFigure = null;
	/** 进入快捷添加图形按钮时，生成的临时连接线 */
	private JecnBaseManhattanLinePanel tmpManhattanLinePanel = null;

	public JecnLinkFigureMainProcess(JecnAidCompCollection aidCompCollection) {
		if (aidCompCollection == null) {
			JecnLogConstants.LOG_LINK_FIGURE_PANEL.error("JecnLinkFigureMainProcess类构造函数：参数为null.");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.aidCompCollection = aidCompCollection;

		// 初始化组件
		initComponents();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void initComponents() {

		// 四个小三角管理对象
		linkButtonManager = new JecnTriangleButtonsManager(this);
		// 快捷面板管理对象
		shortcutManager = new JecnShortcutButtonManager(this);

		// 快速添加流程元素的面板
		linkPanel = new JecnShortcutLinkFigurePanel(this);

		// 左面小三角图形
		leftLinkFigure = new JecnLinkTrianglesButton(this, EditPointType.left);
		// 右面小三角图形
		rightLinkFigure = new JecnLinkTrianglesButton(this, EditPointType.right);
		// 上面小三角图形
		topLinkFigure = new JecnLinkTrianglesButton(this, EditPointType.top);
		// 下面小三角图形
		bottomLinkFigure = new JecnLinkTrianglesButton(this, EditPointType.bottom);

		// 默认隐藏四个小三角和快捷面板
		hiddTrianglesBtnAndLinkPanel();
		linkPanel.setVisible(false);

		// 小三角图标添加到画图面板
		// 左面小三角
		aidCompCollection.getWorkflow().add(leftLinkFigure);
		// 右面小三角
		aidCompCollection.getWorkflow().add(rightLinkFigure);
		// 上面小三角
		aidCompCollection.getWorkflow().add(topLinkFigure);
		// 下面小三角
		aidCompCollection.getWorkflow().add(bottomLinkFigure);

		// 快速添加流程元素的面板
		aidCompCollection.getWorkflow().add(linkPanel);
	}

	/**
	 * 
	 * 对外接口
	 * 
	 * 显示关联图形四个小三角图形
	 * 
	 * @param figure
	 *            JecnBaseFigure 流程元素图形
	 */
	public void showSmallTriangles(JecnBaseFigurePanel figure) {
		if (figure == null) {
			JecnLogConstants.LOG_LINK_FIGURE_PANEL.error("JecnLinkFigureMainProcess类showSmallTriangles方法：参数为null.");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.figure = figure;

		// 获取需要显示的小三角形
		initShowTrianglesButton(figure);

		// if (this.figure != null && this.figure == figure) {//
		// 如果进入的不是同一个对象，隐藏快捷面板
		if (this.getLinkPanel().isVisible()) {
			this.getLinkPanel().setVisible(false);
		}
		// }

		// 显示四个小三角形
		linkButtonManager.registerComponent(figure, false);
	}

	/**
	 * 
	 * 对外接口
	 * 
	 * 隐藏关联图形四个小三角图形
	 * 
	 * @param desktopPane
	 *            JDesktopPane 当前操作面板
	 * 
	 */
	public void hideFigureLinkFigures() {
		// 取消注册
		linkButtonManager.unregisterComponent();
		// 重绘
		aidCompCollection.getWorkflow().repaint();
	}

	/**
	 * 
	 * 立即中断快捷功能
	 * 
	 */
	public void interruptComponent() {
		if (linkButtonManager.isTriangleShow()) {
			this.linkButtonManager.interruptComponent();
			// 隐藏
			linkButtonManager.setTriangleShowHidding();

			// 清除上一次添加的临时图形和线
			clearTmpFigureAndLine();
		}
	}

	/**
	 * 
	 * 隐藏四个小三角形和快捷面板
	 * 
	 */
	void hiddTrianglesBtnAndLinkPanel() {
		trianglesBtnsVisibleFalse();
		this.linkPanel.setVisible(false);
	}

	/**
	 * 
	 * 显示四个小三角形
	 * 
	 */
	void trianglesBtnsVisibleTrue() {
		// 需要显示的小三角形的集合类
		for (JecnLinkTrianglesButton btn : linkTrianglesButtonList) {
			if (!btn.isVisible()) {
				btn.setVisible(true);

				// 设置层级，此层级为当前画图面板最大层级
				JecnDrawMainPanel.getMainPanel().setFlowElemPanelLayer(btn);
			}
		}
	}

	/**
	 * 
	 * 设置小三角形的大小和位置点
	 * 
	 */
	void resetTrianglesPanel() {
		// figure图形离小三角底边最短距离
		int scan = 15;

		// 小三角的X、Y坐标
		int x = figure.getX() - scan - leftLinkFigure.getWidth();
		int y = figure.getY() + (figure.getHeight() - leftLinkFigure.getHeight()) / 2;

		// 左面
		leftLinkFigure.setLocation(x, y);
		// 右面
		x = figure.getX() + figure.getWidth() + scan;
		rightLinkFigure.setLocation(x, y);
		// 上面
		x = figure.getX() + (figure.getWidth() - topLinkFigure.getWidth()) / 2;
		y = figure.getY() - scan - topLinkFigure.getHeight();
		topLinkFigure.setLocation(x, y);
		// 下面
		y = figure.getY() + figure.getHeight() + scan;
		bottomLinkFigure.setLocation(x, y);
	}

	/**
	 * 
	 * 进入
	 * 
	 * @return e MouseEvent 鼠标事件
	 * 
	 */
	public void mouseEntered(MouseEvent e) {
		mouseEnteredProcess(e);
	}

	/**
	 * 
	 * 鼠标离开操作
	 * 
	 * @return e MouseEvent 鼠标事件
	 * 
	 */
	public void mouseExited(MouseEvent e) {
		mouseExitedProcess(e);
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {

	}

	public void actionPerformed(ActionEvent e) {
		actionPerformedProcess(e);
	}

	/**
	 * 
	 * 进入小三角图标事件
	 * 
	 * @param e
	 *            MouseEvent
	 */
	private void mouseEnteredProcess(MouseEvent e) {
		if (aidCompCollection.getWorkflow() == null) {
			return;
		}
		if (e.getSource() instanceof JecnLinkTrianglesButton) {// 小三角图标的事件类
			// 小三角图标
			JecnLinkTrianglesButton figureButton = (JecnLinkTrianglesButton) e.getSource();

			// 注册小三角形
			linkButtonManager.registerComponent(figure, true);
			// 注册快捷面板
			shortcutManager.registerComponent(figureButton);
		} else if (e.getSource() instanceof JecnShortcutLinkFigureButton) {// 快捷按钮
			// 源按钮
			JecnShortcutLinkFigureButton shortcutLinkFigureButton = (JecnShortcutLinkFigureButton) e.getSource();
			// 设置选中边框
			shortcutLinkFigureButton.setBorder(shortcutBorder);

			// 事件转移到面板上:不隐藏四个三角形和快捷面板
			mouseEventBtnToLinkPanel(e);

			// 添加临时图形和连接线
			addTmpFigureAndManhLineBymouseEnter(shortcutLinkFigureButton.getMapElemType());

			// 设置层级，此层级为当前画图面板最大层级：linkPanel一直处于最上层
			JecnDrawMainPanel.getMainPanel().setFlowElemPanelLayer(linkPanel);

		} else if (e.getSource() instanceof JecnShortcutLinkFigurePanel) {// 快捷面板
			// 注册小三角形
			linkButtonManager.registerComponent(figure, false);
			// 刷新时间事件:用于进入快捷面板时
			shortcutManager.refreshComponent();
		}
	}

	/**
	 * 
	 * 鼠标离开操作
	 * 
	 * @return e MouseEvent 鼠标事件
	 * 
	 */
	private void mouseExitedProcess(MouseEvent e) {
		if (aidCompCollection.getWorkflow() == null) {
			return;
		}
		if (e.getSource() instanceof JecnLinkTrianglesButton) {// 小三角图标的事件类
			// 取消注册小三角形
			hideFigureLinkFigures();
			// 取消注册快捷面板
			shortcutManager.unregisterComponent();
		} else if (e.getSource() instanceof JecnShortcutLinkFigureButton) {// 快捷按钮
			// 源按钮
			JecnShortcutLinkFigureButton shortcutLinkFigureButton = (JecnShortcutLinkFigureButton) e.getSource();
			// 取消边框
			shortcutLinkFigureButton.setBorder(null);

			// 清除上一次操作记录
			clearTmpFigureAndLine();

			mouseEventBtnToLinkPanel(e);

		} else if (e.getSource() instanceof JecnShortcutLinkFigurePanel) {// 快捷面板
			// 取消注册小三角形
			hideFigureLinkFigures();
			// 取消注册快捷面板
			shortcutManager.unregisterComponent();

		}

	}

	/**
	 * 
	 * 获取需要显示的小三角形
	 * 
	 * @param figure
	 *            JecnBaseFigurePanel 被操作图形
	 */
	private void initShowTrianglesButton(JecnBaseFigurePanel figure) {

		// 创建需要显示的小三角形的集合类
		linkTrianglesButtonList = new ArrayList<JecnLinkTrianglesButton>();

		// 需要隐藏的小三角形
		Set<JecnLinkTrianglesButton> hiddTrianglesButtonSet = new HashSet<JecnLinkTrianglesButton>();

		for (JecnFigureRefManhattanLine refData : figure.getListFigureRefManhattanLine()) {
			switch (refData.getEditPointType()) {// 连接线方向
			case left:// 左
				hiddTrianglesButtonSet.add(this.leftLinkFigure);
				// 不可见
				leftLinkFigure.setVisible(false);
				break;
			case right:// 右
				hiddTrianglesButtonSet.add(this.rightLinkFigure);
				// 不可见
				rightLinkFigure.setVisible(false);
				break;
			case top:// 上
				hiddTrianglesButtonSet.add(this.topLinkFigure);
				// 不可见
				topLinkFigure.setVisible(false);
				break;
			case bottom:// 下
				hiddTrianglesButtonSet.add(this.bottomLinkFigure);
				// 不可见
				bottomLinkFigure.setVisible(false);
				break;
			default:// 直接返回
				break;
			}
		}

		if (!hiddTrianglesButtonSet.contains(leftLinkFigure)) {// 不包含左面小三角形
			this.linkTrianglesButtonList.add(leftLinkFigure);
		}
		if (!hiddTrianglesButtonSet.contains(rightLinkFigure)) {// 不包含右面小三角形
			this.linkTrianglesButtonList.add(rightLinkFigure);
		}
		if (!hiddTrianglesButtonSet.contains(topLinkFigure)) {// 不包含上面小三角形
			this.linkTrianglesButtonList.add(topLinkFigure);
		}
		if (!hiddTrianglesButtonSet.contains(bottomLinkFigure)) {// 不包含下面小三角形
			this.linkTrianglesButtonList.add(bottomLinkFigure);
		}
	}

	/**
	 * 
	 * 隐藏四个小三角形
	 * 
	 */
	private void trianglesBtnsVisibleFalse() {
		leftLinkFigure.setVisible(false);
		rightLinkFigure.setVisible(false);
		topLinkFigure.setVisible(false);
		bottomLinkFigure.setVisible(false);
	}

	/**
	 * 
	 * 点击按钮的事件转移给面板本身
	 * 
	 * @param e
	 *            MouseEvent
	 */
	private void mouseEventBtnToLinkPanel(MouseEvent e) {
		// 源按钮
		JecnShortcutLinkFigureButton sourceBtn = (JecnShortcutLinkFigureButton) e.getSource();
		// 目标按钮
		MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(sourceBtn, e, linkPanel);
		// 触发给定事件
		linkPanel.dispatchEvent(desMouseEvent);
	}

	/**
	 * 
	 * 添加图形且设置位置点和大小；创建且添加连接线
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void actionPerformedProcess(ActionEvent e) {

		if (!(e.getSource() instanceof JecnShortcutLinkFigureButton)) {// 不是快捷添加图形按钮
			return;
		}

		// 立即停止时间线程
		this.shortcutManager.stopInteng();

		JecnShortcutLinkFigureButton figureButton = (JecnShortcutLinkFigureButton) e.getSource();

		if (figure == null || !getWorkflow().getPanelList().contains(figure)) {
			interruptComponent();
			return;
		}

		// 清除上一次添加的临时图形和线
		clearTmpFigureAndLine();

		// 创建图形
		JecnBaseFigurePanel newFigure = (JecnBaseFigurePanel) JecnCreateFlowElement.getFlowEmelentByType(figureButton
				.getMapElemType());
		// 创建连接线
		JecnBaseManhattanLinePanel manhattanLine = (JecnBaseManhattanLinePanel) JecnCreateFlowElement
				.getFlowEmelentByType(this.shortcutLineMapElemType);

		// 改变图形位置点和大小；创建且添加连接线
		addFigureAndManhLine(newFigure, manhattanLine);

		// 立即中断小三角型线程
		this.linkButtonManager.interruptComponent();
	}

	/**
	 * 
	 * 添加快捷图形和连接线，且设置图形位置点和大小
	 * 
	 * @param mapElemType
	 *            MapElemType 图形类型
	 */
	private void addTmpFigureAndManhLineBymouseEnter(MapElemType mapElemType) {
		if (mapElemType == null) {
			return;
		}

		// 清除上一次添加的临时图形和线
		clearTmpFigureAndLine();

		// 创建图形
		JecnBaseFigurePanel newFigure = (JecnBaseFigurePanel) JecnCreateFlowElement.createFlowElementObj(mapElemType);
		// 创建连接线
		JecnBaseManhattanLinePanel manhattanLine = (JecnBaseManhattanLinePanel) JecnCreateFlowElement
				.getFlowEmelentByType(this.shortcutLineMapElemType);

		// 改变图形位置点和大小；创建且添加连接线
		addTmpFigureAndManhLine(newFigure, manhattanLine);
	}

	/**
	 * 
	 * 快速添加图形和连接线:此方法时在实际添加过程中调用
	 * 
	 * @param newFigure
	 *            JecnBaseFigurePanel 快速添加的图形
	 * @param manhattanLine
	 *            JecnBaseManhattanLinePanel 快速添加的连接线
	 * 
	 */
	private void addFigureAndManhLine(JecnBaseFigurePanel newFigure, JecnBaseManhattanLinePanel manhattanLine) {

		// 获取图形的位置点，连接线的开始编辑点和结束编辑点
		TmpBean tmpBean = addTmpBean(newFigure);

		if (tmpBean == null || tmpBean.getLocation().x < 1 || tmpBean.getLocation().y < 1) {// 新添加图形不能超出上边界和左边界
			return;
		}

		// 添加图形到画图面板
		JecnAddFlowElementUnit.addFigure(tmpBean.getLocation(), newFigure);
		// 根据设置图形大小和位置点
		newFigure.setSizeAndLocation(tmpBean.getLocation());

		manhattanLine.reSetEditFigure(tmpBean.getFigurePort(), tmpBean.getNewFigurePort());
		// 添加连接线到画图面板
		JecnAddFlowElementUnit.addManhattinLine(manhattanLine);

		this.getWorkflow().revalidate();
		this.getWorkflow().repaint();

		// *******************撤销恢复*******************//
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		// 记录撤销恢复数据
		redoData.recodeFlowElement(newFigure);
		redoData.recodeFlowElement(manhattanLine);
		JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createAdd(redoData);
		getWorkflow().getUndoRedoManager().addEdit(undoProcess);
		// *******************撤销恢复*******************//

		getWorkflow().getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 
	 * 快速添加临时图形
	 * 
	 * @param newFigure
	 *            JecnBaseFigurePanel 快速添加的图形
	 * @param manhattanLine
	 *            JecnBaseManhattanLinePanel 快速添加的连接线
	 * 
	 */
	private void addTmpFigureAndManhLine(JecnBaseFigurePanel newFigure, JecnBaseManhattanLinePanel manhattanLine) {

		tmpShortcutFigure = newFigure;
		tmpManhattanLinePanel = manhattanLine;

		// 获取图形的位置点，连接线的开始编辑点和结束编辑点
		TmpBean tmpBean = addTmpBean(newFigure);

		if (tmpBean == null) {
			return;
		}

		// 添加图形到画图面板
		addTmpFigure(tmpBean.getLocation(), newFigure);

		// 添加连接线到画图面板
		addTmpManhattanLine(manhattanLine, tmpBean.getFigurePort(), tmpBean.getNewFigurePort());
	}

	/**
	 * 
	 * 添加图形
	 * 
	 * @param point
	 *            Point 图形位置点
	 * @param figure
	 *            JecnBaseFigurePanel 图形
	 * @return boolean false:不是图形或创建图形失败 true：创建图形成功
	 */
	private boolean addTmpFigure(Point point, JecnBaseFigurePanel figure) {
		if (figure == null) {// 图形对象是NULL 不添加
			return false;
		}
		// 设置属性值
		figure.setSizeAndLocation(point);

		// 添加图形画图面板
		this.getWorkflow().add(figure);

		return true;
	}

	/**
	 * 添加连接线到面板上
	 * 
	 * @param linePanel
	 *            连接线
	 * @param startEditPort
	 *            连接线开始端点(画线时鼠标最开始点击的端点)
	 * @param endEditPort
	 *            连接线结束端点（鼠标释放时的端点）
	 */
	private void addTmpManhattanLine(JecnBaseManhattanLinePanel linePanel, JecnEditPortPanel startEditPort,
			JecnEditPortPanel endEditPort) {
		// 设置连接线开始图形和结束图形
		linePanel.setStartFigure(startEditPort.getBaseFigurePanel());
		linePanel.setEndFigure(endEditPort.getBaseFigurePanel());
		// 设置连接线对应图形端口的位置
		linePanel.getFlowElementData().setStartEditPointType(startEditPort.getEditPointType());
		linePanel.getFlowElementData().setEndEditPointType(endEditPort.getEditPointType());

		// 执行连接线默认曼哈顿算法,并且添加到画图面板
		linePanel.paintLineByRouter();
	}

	/**
	 * 
	 * 获取快速添加图形的位置点以及连接线的开始编辑点和结束编辑点
	 * 
	 * @param newFigure
	 *            JecnBaseFigurePanel 快速添加的图形
	 * 
	 */
	private TmpBean addTmpBean(JecnBaseFigurePanel newFigure) {
		if (linkPanel.getActionTriangleButton() == null || figure == null || newFigure == null) {
			return null;
		}

		TmpBean tmpBean = new TmpBean();

		// 小三角形所在图形方向
		EditPointType editPointType = linkPanel.getActionTriangleButton().getEditPointType();

		// 图形的编辑中心点（连接线的开始点）
		JecnXYData xyData = null;

		// 快捷创建出的图形的位置点X坐标
		int x = 0;
		// 快捷创建出的图形的位置点Y坐标
		int y = 0;

		// 连接线开始编辑点
		JecnEditPortPanel figurePort = null;
		// 连接线结束编辑点
		JecnEditPortPanel newFigurePort = null;

		// 被操作的图形（活动等）
		JecnFigureDataCloneable figureDataCloneable = figure.getFlowElementData().getFigureDataCloneable()
				.getZoomFigureDataProcess(JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());

		// 快速关联出的图形
		JecnFigureDataCloneable newFigureDataCloneable = newFigure.getFlowElementData().getFigureDataCloneable()
				.getZoomFigureDataProcess(JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());

		// 获取当前比例下间距：保证放大添加的图形防缩到默认比例下是一样间距
		double space = getFinalSpace(newFigure) * JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();

		// 宽高
		double wDouble = newFigureDataCloneable.getWidth();
		double hDouble = newFigureDataCloneable.getHeight();

		switch (editPointType) {// 小三角形所在图形方向
		case left:// 左
			// 图形的编辑中心点（连接线的开始点）
			xyData = figureDataCloneable.getEditLeftPoint();
			// 快捷创建出的图形的位置点X坐标
			x = DrawCommon.convertDoubleToInt(xyData.getX() - space - wDouble);
			// 快捷创建出的图形的位置点Y坐标
			y = DrawCommon.convertDoubleToInt(xyData.getY() - hDouble / 2);

			// 连接线开始编辑点
			figurePort = figure.getEditLeftPortPanel();
			// 连接线结束编辑点
			newFigurePort = newFigure.getEditRightPortPanel();

			break;
		case right:// 右
			// 图形的编辑中心点（连接线的开始点）
			xyData = figureDataCloneable.getEditRightPoint();
			// 快捷创建出的图形的位置点X坐标
			x = DrawCommon.convertDoubleToInt(xyData.getX() + space);
			// 快捷创建出的图形的位置点Y坐标
			y = DrawCommon.convertDoubleToInt(xyData.getY() - hDouble / 2);

			// 连接线开始编辑点
			figurePort = figure.getEditRightPortPanel();
			// 连接线结束编辑点
			newFigurePort = newFigure.getEditLeftPortPanel();
			break;
		case top:// 上
			// 图形的编辑中心点（连接线的开始点）
			xyData = figureDataCloneable.getEditTopPoint();
			// 快捷创建出的图形的位置点X坐标
			x = DrawCommon.convertDoubleToInt(xyData.getX() - wDouble / 2);
			// 快捷创建出的图形的位置点Y坐标
			y = DrawCommon.convertDoubleToInt(xyData.getY() - space - hDouble);

			// 连接线开始编辑点
			figurePort = figure.getEditTopPortPanel();
			// 连接线结束编辑点
			newFigurePort = newFigure.getEditBottomPortPanel();
			break;
		case bottom:// 下
			// 图形的编辑中心点（连接线的开始点）
			xyData = figureDataCloneable.getEditBottomPoint();
			// 快捷创建出的图形的位置点X坐标
			x = DrawCommon.convertDoubleToInt(xyData.getX() - wDouble / 2);
			// 快捷创建出的图形的位置点Y坐标
			y = DrawCommon.convertDoubleToInt(xyData.getY() + space);

			// 连接线开始编辑点
			figurePort = figure.getEditBottomPortPanel();
			// 连接线结束编辑点
			newFigurePort = newFigure.getEditTopPortPanel();

			break;
		default:// 直接返回
			return null;
		}

		// 位置点
		tmpBean.setLocation(new Point(x, y));
		// 连接线开始编辑点
		tmpBean.setFigurePort(figurePort);
		// 连接线结束编辑点
		tmpBean.setNewFigurePort(newFigurePort);

		return tmpBean;
	}

	/**
	 * 
	 * 清除上一次添加的临时图形和线
	 * 
	 */
	private void clearTmpFigureAndLine() {
		if (tmpShortcutFigure != null) {// 移除上一次添加的图形
			// 元素阴影或活动信息化，AR活动编号
			tmpShortcutFigure.removeRelatedPanel();
			this.getWorkflow().remove(tmpShortcutFigure);
			tmpShortcutFigure = null;
		}

		if (tmpManhattanLinePanel != null) {// 移除上一次添加的连接线
			tmpManhattanLinePanel.disposeLines();
			tmpManhattanLinePanel = null;
		}
	}

	/**
	 * 
	 * 获取生成图形间距
	 * 
	 * @param newFigure
	 *            JecnBaseFigurePanel 被快速添加目标图形
	 * @return int
	 */
	private int getFinalSpace(JecnBaseFigurePanel newFigure) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || newFigure == null) {
			return 0;
		}

		if (workflow.getFlowMapData().isHFlag()) { // 是否是横向标识 true：横向 false：纵向
			return JecnWorkflowConstant.SHORTCUT_SPACE_V
					- newFigure.getFlowElementData().getFigureDataCloneable().getHeight();
		} else {
			return JecnWorkflowConstant.SHORTCUT_SPACE_H
					- newFigure.getFlowElementData().getFigureDataCloneable().getWidth();
		}
	}

	public JecnShortcutLinkFigurePanel getLinkPanel() {
		return linkPanel;
	}

	public JecnLinkTrianglesButton getLeftLinkFigure() {
		return leftLinkFigure;
	}

	public JecnLinkTrianglesButton getRightLinkFigure() {
		return rightLinkFigure;
	}

	public JecnLinkTrianglesButton getTopLinkFigure() {
		return topLinkFigure;
	}

	public JecnLinkTrianglesButton getBottomLinkFigure() {
		return bottomLinkFigure;
	}

	public JecnBaseFigurePanel getFigure() {
		return figure;
	}

	public void setFigure(JecnBaseFigurePanel figure) {
		this.figure = figure;
	}

	public JecnDrawDesktopPane getWorkflow() {
		return aidCompCollection.getWorkflow();
	}

	/**
	 * 
	 * 快速添加临时图形和线使用的记录类
	 * 
	 * @author Administrator
	 * 
	 */
	private class TmpBean {
		/** 位置点 */
		private Point location = null;
		/** 连接线开始编辑点 */
		private JecnEditPortPanel figurePort = null;
		// 连接线结束编辑点
		private JecnEditPortPanel newFigurePort = null;

		public Point getLocation() {
			return location;
		}

		public void setLocation(Point location) {
			this.location = location;
		}

		public JecnEditPortPanel getFigurePort() {
			return figurePort;
		}

		public void setFigurePort(JecnEditPortPanel figurePort) {
			this.figurePort = figurePort;
		}

		public JecnEditPortPanel getNewFigurePort() {
			return newFigurePort;
		}

		public void setNewFigurePort(JecnEditPortPanel newFigurePort) {
			this.newFigurePort = newFigurePort;
		}

	}
}
