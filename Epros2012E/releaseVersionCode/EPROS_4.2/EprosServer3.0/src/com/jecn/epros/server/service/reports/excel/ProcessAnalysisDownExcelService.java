package com.jecn.epros.server.service.reports.excel;

import java.util.List;

import jxl.write.Label;
import jxl.write.WritableSheet;

import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.reports.ProcessAnalysisBean;

/**
 * 
 * 流程分析统计
 * 
 * @author ZHOUXY
 * 
 */
public class ProcessAnalysisDownExcelService extends BaseDownExcelService {
	private List<ProcessAnalysisBean> processAnalysisList = null;

	public ProcessAnalysisDownExcelService() {
		this.mouldPath = getPrefixPath() + "mould/processAnalysisReport.xls";
		this.startDataRow = 2;
	}

	@Override
	protected boolean editExcle(WritableSheet dataSheet) {
		if (processAnalysisList == null) {
			return true;
		}

		try {
			// 第一行
			this.addFirstRowData(dataSheet);

			// 数据行号
			int i = 0;
			// 第三行开始
			for (ProcessAnalysisBean analysisBean : processAnalysisList) {
				// 时间类型 1日 2周 3月 4季 5年
				String timeTypeName = getTimeTypeName(analysisBean
						.getTimeType());
				// 一种角色数对应的数据集合
				List<ProcessWebBean> listProcessWebBean = analysisBean
						.getListProcessWebBean();
				// 个数
				String countTotal = String
						.valueOf(analysisBean.getCountTotal());

				if (timeTypeName == null) {
					continue;
				}

				if (listProcessWebBean == null
						|| listProcessWebBean.size() == 0) {
					int row = startDataRow + i;
					// 时间类型
					dataSheet.addCell(new Label(0, row, timeTypeName));
					// 个数
					dataSheet.addCell(new Label(2, row, "0"));
					i++;
					continue;
				}

				// 待合并开始行号
				int startFirstRow = startDataRow + i;
				
				for (ProcessWebBean bean : listProcessWebBean) {
					int row = startDataRow + i;
					// 时间类型
					dataSheet.addCell(new Label(0, row, timeTypeName));
					// 个数
					dataSheet.addCell(new Label(1, row, bean.getFlowName()));
					// 个数
					dataSheet.addCell(new Label(2, row, countTotal));
					i++;
				}
				// 合并单元格，参数格式（开始列，开始行，结束列，结束行）
				// 时间类型
				dataSheet.mergeCells(0, startFirstRow, 0, startDataRow + i - 1);
				// 个数
				dataSheet.mergeCells(2, startFirstRow, 2, startDataRow + i - 1);
			}
			return true;
		} catch (Exception e) {
			log.error("ProcessAnalysisDownExcelService类editExcle方法", e);
			return false;
		}
	}

	/**
	 * 
	 * 获取时间类型对应的名称
	 * 
	 * @param timeType
	 *            String
	 * @return 返回 日/周/月/季/年/null
	 */
	private String getTimeTypeName(String timeType) {
		// 日
		String day = JecnUtil.getValue("day");
		// 周
		String weeks = JecnUtil.getValue("weeks");
		// 月
		String month = JecnUtil.getValue("month");
		// 季
		String season = JecnUtil.getValue("season");
		// 年
		String years = JecnUtil.getValue("years");

		if ("1".equals(timeType)) {// 时间类型 1日 2周 3月 4季 5年
			return day;
		} else if ("2".equals(timeType)) {
			return weeks;
		} else if ("3".equals(timeType)) {
			return month;
		} else if ("4".equals(timeType)) {
			return season;
		} else if ("5".equals(timeType)) {
			return years;
		} else {
			return null;
		}
	}

	public List<ProcessAnalysisBean> getProcessAnalysisList() {
		return processAnalysisList;
	}

	public void setProcessAnalysisList(
			List<ProcessAnalysisBean> processAnalysisList) {
		this.processAnalysisList = processAnalysisList;
	}

}
