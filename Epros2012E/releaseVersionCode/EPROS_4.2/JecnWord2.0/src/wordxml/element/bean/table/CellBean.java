package wordxml.element.bean.table;

import wordxml.element.bean.common.BorderBean;
import wordxml.element.bean.common.ShdBean;

/**
 * 表格 单个格子属性
 */
public class CellBean {
	/** 单元格底纹 */
	private ShdBean shdBean;
	/** 单元格边框属性 **/
	private BorderBean borderTop;
	private BorderBean borderLeft;
	private BorderBean borderBottom;
	private BorderBean borderRight;

	public CellBean() {
	}

	public ShdBean getShdBean() {
		return shdBean;
	}       

	public void setShdBean(ShdBean shdBean) {
		this.shdBean = shdBean;
	}

	public BorderBean getBorderTop() {
		return borderTop;
	}

	public void setBorderTop(float width) {
		if (width <= 0) {
			this.borderTop = null;
		} else {
			this.borderTop = BorderBean.newInstance(width);
		}

	}

	public BorderBean getBorderLeft() {
		return borderLeft;
	}

	public void setBorderLeft(float width) {
		if (width <= 0) {
			this.borderLeft = null;
		} else {
			this.borderLeft = BorderBean.newInstance(width);
		}
	}

	public BorderBean getBorderBottom() {
		return borderBottom;
	}

	public void setBorderBottom(float width) {
		if (width <= 0) {
			this.borderBottom = null;
		} else {
			this.borderBottom = BorderBean.newInstance(width);
		}

	}

	public BorderBean getBorderRight() {
		return borderRight;
	}

	public void setBorderRight(float width) {
		if (width <= 0) {
			this.borderRight = null;
		} else {
			this.borderRight = BorderBean.newInstance(width);
		}
	}

	/**
	 * 设置所有边框 （单位 磅）
	 * 
	 * @param width
	 */
	public void setBorder(float width) {
		BorderBean border = BorderBean.newInstance(width);
		setBorderTop(border);
		setBorderBottom(border);
		setBorderLeft(border);
		setBorderRight(border);
	}

	public void setBorderTop(BorderBean borderTop) {
		this.borderTop = borderTop;
	}

	public void setBorderLeft(BorderBean borderLeft) {
		this.borderLeft = borderLeft;
	}

	public void setBorderBottom(BorderBean borderBottom) {
		this.borderBottom = borderBottom;
	}

	public void setBorderRight(BorderBean borderRight) {
		this.borderRight = borderRight;
	}

}
