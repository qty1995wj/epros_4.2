/*
 * Pentagon.java
 * 五边形
 * Created on 2008年11月4日, 下午2:26
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.total;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BasePentagon;

/**
 * 
 * 
 * 五边形
 * 
 * @author zhangxh
 * @date： 日期：Mar 23, 2012 时间：9:54:45 AM
 */
public class Pentagon extends BasePentagon {
	public Pentagon(JecnFigureData figureData) {
		super(figureData);
	}
}
