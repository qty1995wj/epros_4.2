/**
 * Tx.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class Tx  implements java.io.Serializable {
    private java.lang.String instanceId;

    private java.lang.String strStatusCode;

    private java.lang.String childTableName;

    private java.lang.String mapRelation;

    private java.lang.String sheetCode;

    public Tx() {
    }

    public Tx(
           java.lang.String instanceId,
           java.lang.String strStatusCode,
           java.lang.String childTableName,
           java.lang.String mapRelation,
           java.lang.String sheetCode) {
           this.instanceId = instanceId;
           this.strStatusCode = strStatusCode;
           this.childTableName = childTableName;
           this.mapRelation = mapRelation;
           this.sheetCode = sheetCode;
    }


    /**
     * Gets the instanceId value for this Tx.
     * 
     * @return instanceId
     */
    public java.lang.String getInstanceId() {
        return instanceId;
    }


    /**
     * Sets the instanceId value for this Tx.
     * 
     * @param instanceId
     */
    public void setInstanceId(java.lang.String instanceId) {
        this.instanceId = instanceId;
    }


    /**
     * Gets the strStatusCode value for this Tx.
     * 
     * @return strStatusCode
     */
    public java.lang.String getStrStatusCode() {
        return strStatusCode;
    }


    /**
     * Sets the strStatusCode value for this Tx.
     * 
     * @param strStatusCode
     */
    public void setStrStatusCode(java.lang.String strStatusCode) {
        this.strStatusCode = strStatusCode;
    }


    /**
     * Gets the childTableName value for this Tx.
     * 
     * @return childTableName
     */
    public java.lang.String getChildTableName() {
        return childTableName;
    }


    /**
     * Sets the childTableName value for this Tx.
     * 
     * @param childTableName
     */
    public void setChildTableName(java.lang.String childTableName) {
        this.childTableName = childTableName;
    }


    /**
     * Gets the mapRelation value for this Tx.
     * 
     * @return mapRelation
     */
    public java.lang.String getMapRelation() {
        return mapRelation;
    }


    /**
     * Sets the mapRelation value for this Tx.
     * 
     * @param mapRelation
     */
    public void setMapRelation(java.lang.String mapRelation) {
        this.mapRelation = mapRelation;
    }


    /**
     * Gets the sheetCode value for this Tx.
     * 
     * @return sheetCode
     */
    public java.lang.String getSheetCode() {
        return sheetCode;
    }


    /**
     * Sets the sheetCode value for this Tx.
     * 
     * @param sheetCode
     */
    public void setSheetCode(java.lang.String sheetCode) {
        this.sheetCode = sheetCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tx)) return false;
        Tx other = (Tx) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.instanceId==null && other.getInstanceId()==null) || 
             (this.instanceId!=null &&
              this.instanceId.equals(other.getInstanceId()))) &&
            ((this.strStatusCode==null && other.getStrStatusCode()==null) || 
             (this.strStatusCode!=null &&
              this.strStatusCode.equals(other.getStrStatusCode()))) &&
            ((this.childTableName==null && other.getChildTableName()==null) || 
             (this.childTableName!=null &&
              this.childTableName.equals(other.getChildTableName()))) &&
            ((this.mapRelation==null && other.getMapRelation()==null) || 
             (this.mapRelation!=null &&
              this.mapRelation.equals(other.getMapRelation()))) &&
            ((this.sheetCode==null && other.getSheetCode()==null) || 
             (this.sheetCode!=null &&
              this.sheetCode.equals(other.getSheetCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInstanceId() != null) {
            _hashCode += getInstanceId().hashCode();
        }
        if (getStrStatusCode() != null) {
            _hashCode += getStrStatusCode().hashCode();
        }
        if (getChildTableName() != null) {
            _hashCode += getChildTableName().hashCode();
        }
        if (getMapRelation() != null) {
            _hashCode += getMapRelation().hashCode();
        }
        if (getSheetCode() != null) {
            _hashCode += getSheetCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tx.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">Tx"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instanceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "instanceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("strStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "strStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("childTableName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "childTableName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mapRelation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "mapRelation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sheetCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "sheetCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
