package com.jecn.epros.server.bean.process;
import java.io.Serializable;

/**
 * 流程接口
 * @author user
 *
 */
public class ProcessInterface implements Serializable{
	
	/** 接口的图形id**/
	private Long id;
	/** 接口的图形name**/
	private String name;
	/** 关联流程的id**/
	private Long rid;
	/** 关联流程的名称**/
	private String rname;
	/** 接口的类型**/
	private int implType;
	/** 接口名称  **/
	private String implName;
	/** 备注  **/
	private String implNote;
	/** 流程要求**/
	private String processRequirements;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	public String getRname() {
		return rname;
	}
	public void setRname(String rname) {
		this.rname = rname;
	}
	public int getImplType() {
		return implType;
	}
	public void setImplType(int implType) {
		this.implType = implType;
	}
	public String getImplName() {
		return implName;
	}
	public void setImplName(String implName) {
		this.implName = implName;
	}
	public String getImplNote() {
		return implNote;
	}
	public void setImplNote(String implNote) {
		this.implNote = implNote;
	}
	public String getProcessRequirements() {
		return processRequirements;
	}
	public void setProcessRequirements(String processRequirements) {
		this.processRequirements = processRequirements;
	}
	

}
