package epros.designer.gui.popedom.role;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.table.JecnTableCommon;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;

public class RoleChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(RoleChooseDialog.class);

	public RoleChooseDialog(List<JecnTreeBean> list) {
		super(list, 6);
		this.setTitle(JecnProperties.getValue("roleChoose"));
	}

	/**
	 * 二级管理员不可以修改角色
	 */
	protected void addButton() {
		List<JecnTreeBean> list = getListJecnTreeBean();
		if (list == null || list.size() == 0) {
			super.addButton();
			return;
		}
		JecnTreeBean treeBean = list.get(0);
		if ("secondAdmin".equals(treeBean.getNumberId())) {
			buttonsPanel.add(cancelBtn);
		} else {
			super.addButton();
		}
	}

	public RoleChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		super(list, 6, jecnDialog);
		this.setTitle(JecnProperties.getValue("roleChoose"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getJecnRole().getRolesByName(JecnConstants.projectId, name,
					JecnDesignerCommon.isSecondAdmin() ? JecnConstants.getUserId() : null);
		} catch (Exception e) {
			log.error("RoleChooseDialog searchByName is error！", e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("roleName");
	}

	@Override
	public JecnTree getJecnTree() {

		return new HighEfficiencyRoleTree(null);
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("roleName"));// 角色名称
		return title;
	}

	@Override
	public boolean listJTabelResult(JecnTreeBean jecnTreeBean) {
		for (JecnTreeBean newTreeBean : getListJecnTreeBean()) {
			if (newTreeBean.getId().equals(jecnTreeBean.getId())) {
				JecnTableCommon.getTableSelect(jecnTreeBean.getId(), resultTable);
				return false;
			}

			if (isExistDesign(jecnTreeBean.getNumberId(), newTreeBean.getNumberId())) { // 是否存在设计权限
				if (JecnDesignerCommon.isShowSecondAdmin()) {// 启用二级管理员
					// 系统管理员和二级管理员和流程设计专家只能选择其中一个！
					JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("roleAuthDesignerOnlyOne"));
				} else {
					JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("adminDesignerOnlyOne"));
				}
				return false;
			}
		}
		if (isSelectMutil()) {
			getListJecnTreeBean().add(jecnTreeBean);
		} else {
			getListJecnTreeBean().clear();
			getListJecnTreeBean().add(jecnTreeBean);
		}

		return true;
	}

	@Override
	public void okBtnAction() {
		boolean userDefined = false;
		boolean designer = false;
		for (JecnTreeBean b : getListJecnTreeBean()) {
			if (DrawCommon.isNullOrEmtryTrim(b.getNumberId())) {
				if (!userDefined) {
					userDefined = true;
				}
			} else {
				if (b.getNumberId().equals("design")) {
					designer = true;
				}
			}

		}
		if (designer && !userDefined) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("designerMustUserDefined"));
			return;
		} else {
			super.okBtnAction();
		}
	}

	/**
	 * 是否存在登录设计器的权限
	 * 
	 * @param numId
	 * @param newNumId
	 * @return
	 */
	private boolean isExistDesign(String numId, String newNumId) {
		if (StringUtils.isEmpty(numId) || StringUtils.isEmpty(newNumId)) {
			return false;
		}
		List<String> list = new ArrayList<String>();
		list.add("admin");
		list.add("design");
		if (JecnDesignerCommon.isShowSecondAdmin()) {// 启用二级管理员
			list.add("secondAdmin");
		}
		if (list.contains(numId) && list.contains(newNumId)) {
			list = null;
			return true;
		}
		return false;
	}
}
