package com.jecn.webservice.goldenDragon;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.jecn.webservice.goldenDragon.bean.TaskApprovalRecords;

@WebService(endpointInterface = "IGoldenDragonWebService")
public interface IGoldenDragonWebService {

	@WebMethod(operationName = "releaseTask")
	public String releaseTask(String taskId, List<TaskApprovalRecords> approvalRecords);

	@WebMethod(operationName = "deleteTask")
	String deleteTask(String taskId);
}
