package com.jecn.epros.server.bean.popedom;


import java.util.Date;
import java.util.List;

/**
 * JecnRoleInfo entity.
 * 角色表
 * @author MyEclipse Persistence Tools
 */

public class JecnRoleInfo implements java.io.Serializable {
	private Long roleId;//主键ID
	private String roleName;//角色名称
	private String roleRemark;//备注
	private String filterId;//标示
	private List<JecnRoleContent> listJecnRoleContent;
	private Long projectId;//项目ID
	private Long perId;//父节点
	private Integer isDir;//目录（0角色，1是目录）
	private Integer sortId;//排序
	private Date createTime;//创建时间
	private Long createPersonId;//创建人
	private Date updateTime;//更新时间
	private Long updatePersonId;//更新人

    public Integer getIsDir() {
		return isDir;
	}

	public void setIsDir(Integer isDir) {
		this.isDir = isDir;
	}

	public Integer getSortId() {
		return sortId;
	}

	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getUpdatePersonId() {
		return updatePersonId;
	}

	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}

	/**
     * @return the roleId
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * @param roleName the roleName to set
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * @return the roleRemark
     */
    public String getRoleRemark() {
        return roleRemark;
    }

    /**
     * @param roleRemark the roleRemark to set
     */
    public void setRoleRemark(String roleRemark) {
        this.roleRemark = roleRemark;
    }

    /**
     * @return the filterId
     */
    public String getFilterId() {
        return filterId;
    }

    /**
     * @param filterId the filterId to set
     */
    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    /**
     * @return the listJecnRoleContent
     */
    public List<JecnRoleContent> getListJecnRoleContent() {
        return listJecnRoleContent;
    }

    /**
     * @param listJecnRoleContent the listJecnRoleContent to set
     */
    public void setListJecnRoleContent(List<JecnRoleContent> listJecnRoleContent) {
        this.listJecnRoleContent = listJecnRoleContent;
    }

    /**
     * @return the projectId
     */
    public Long getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

	public Long getPerId() {
		return perId;
	}

	public void setPerId(Long perId) {
		this.perId = perId;
	}

}