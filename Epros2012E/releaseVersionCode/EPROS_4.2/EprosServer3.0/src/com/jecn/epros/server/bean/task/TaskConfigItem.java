package com.jecn.epros.server.bean.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.jecn.epros.server.bean.popedom.JecnUser;

public class TaskConfigItem implements Serializable {

	private String id;
	/** 唯一标识 **/
	private String mark;
	/** 配置项名称 **/
	private String name;
	/** 默认名称 **/
	private String defaultName;
	/** 排序 **/
	private int sort;
	/** 默认排序 **/
	private int defaultSort;
	/** 是否必填0非必填1必填 **/
	private int isEmpty;
	/** 是否必填0非必填1必填 **/
	private int isDefaultEmpty;
	/** 任务模板主键id **/
	private String templetId;
	/** 默认值审批阶段表示0不显示 1显示 在审批状态0表示拟稿人 1表示文控 **/
	private String value;
	/** 默认值审批阶段表示0不显示 1显示 在审批状态0表示拟稿人 1表示文控 **/
	private String defaultValue;
	/**
	 * 英文
	 */
	private String enName;

	/** 默认审批人 **/
	private List<JecnUser> users = new ArrayList<JecnUser>();

	/**
	 * 是否必填
	 * 
	 * @return true为必填 false为非必填
	 */
	public boolean isNotEmpty() {
		if (isEmpty == 1) {
			return true;
		}
		return false;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefaultName() {
		return defaultName;
	}

	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getDefaultSort() {
		return defaultSort;
	}

	public void setDefaultSort(int defaultSort) {
		this.defaultSort = defaultSort;
	}

	public int getIsEmpty() {
		return isEmpty;
	}

	public void setIsEmpty(int isEmpty) {
		this.isEmpty = isEmpty;
	}

	public int getIsDefaultEmpty() {
		return isDefaultEmpty;
	}

	public void setIsDefaultEmpty(int isDefaultEmpty) {
		this.isDefaultEmpty = isDefaultEmpty;
	}

	public String getTempletId() {
		return templetId;
	}

	public void setTempletId(String templetId) {
		this.templetId = templetId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public List<JecnUser> getUsers() {
		return users;
	}

	public void setUsers(List<JecnUser> users) {
		this.users = users;
	}

	public String getName(Locale locale) {
		return locale == Locale.CHINESE ? name : enName;
	}

}
