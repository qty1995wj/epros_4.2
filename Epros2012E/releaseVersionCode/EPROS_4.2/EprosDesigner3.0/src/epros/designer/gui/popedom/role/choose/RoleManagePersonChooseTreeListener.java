package epros.designer.gui.popedom.role.choose;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.ProcessTreeListener;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class RoleManagePersonChooseTreeListener extends JecnTreeListener {
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(ProcessTreeListener.class);

	public RoleManagePersonChooseTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = null;
				if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.position) {
					list = ConnectionPool.getPersonAction().getPersonByPosition(node.getJecnTreeBean().getId());
				} else {
					list = ConnectionPool.getOrganizationAction().getRoleAuthChildPersons(
							node.getJecnTreeBean().getId(), JecnConstants.projectId,
							JecnDesignerCommon.getSecondAdminUserId());
				}

				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("RoleManagePersonChooseTreeListener treeExpanded is error！", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

}
