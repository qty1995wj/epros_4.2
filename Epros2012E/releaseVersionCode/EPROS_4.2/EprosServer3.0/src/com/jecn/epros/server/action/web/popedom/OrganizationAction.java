package com.jecn.epros.server.action.web.popedom;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.UserInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.popedom.IJecnRoleService;
import com.jecn.epros.server.service.popedom.IOrganizationService;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.popedom.buss.OrdListDownload;
import com.jecn.epros.server.service.task.search.IJecnTaskSearchService;
import com.jecn.epros.server.util.JecnUserSecurityKey;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.popedom.JobDescriptionBean;
import com.jecn.epros.server.webBean.popedom.OrgListBean;
import com.jecn.epros.server.webBean.popedom.OrgRelateFlowBean;
import com.jecn.epros.server.webBean.popedom.ProcessListBean;
import com.jecn.epros.server.webBean.popedom.RoleWebInfoBean;
import com.jecn.epros.server.webBean.popedom.UserWebInfoBean;
import com.jecn.epros.server.webBean.popedom.UserWebSearchBean;
import com.opensymphony.xwork2.ActionContext;

public class OrganizationAction extends BaseAction {
	private static final Logger log = Logger.getLogger(OrganizationAction.class);
	private IOrganizationService organizationService;
	private IPersonService personService;
	/** 开始行 */
	private int start;
	/** 每页显示行数 */
	private int limit;
	/** 登录名称 */
	private String loginName;
	/** 真实名称 */
	private String trueName;
	/** 登录密码 */
	private String password;
	private String node;
	/** 组织ID */
	private long orgId;
	/** 岗位ID */
	private long posId;
	/** 岗位名称 */
	private String posName;
	/** 名称搜索 */
	private String searchName = "";
	/** 部门名称 */
	private String orgName = "";
	/** 0不加载人员1加载人员 */
	private int posLoadtype;// 0
	/** 岗位说明书数据 */
	private JobDescriptionBean descriptionBean;
	/** 流程清单数据 */
	private List<OrgRelateFlowBean> orgRelateFlowList;
	/** 部门职责 */
	private String orgDuties;
	/** 打开文件文件流 */
	private byte[] content = null;
	/** 文件名称 */
	private String fileName;
	/** 角色ID */
	private long roleId = -1;
	/** 角色名称 */
	private String roleName;
	/** 角色类型 */
	private String roleType;
	/** 新密码 */
	private String newPassword;
	/** 原密码 */
	private String originalPassword;
	/** 保持登录 */
	private String keepLogin;
	/** 我的任务，任务计划 搜索、删除、显示记录 service接口 */
	private IJecnTaskSearchService searchService;
	/** 角色管理 */
	private IJecnRoleService roleService;
	/** 角色清单 */
	private OrgListBean orgList;
	/** 是否显示全部 */
	private boolean showAll = true;

	/** 访问岗位类型 默认true带返回主页，false 不带返回主页 */
	private String posType = "true";

	/* 用户信息用于页面显示 */
	private UserInfo userInfo;

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * @author yxw 2012-12-1
	 * @description:加载组织和岗位
	 * @return
	 */
	public String getChildsOrgAndPos() {
		List<JecnTreeBean> list;
		try {
			list = organizationService.getChildOrgsAndPositon(orgId, getProjectId(), posLoadtype == 0 ? false : true);
			if (list != null && list.size() > 0) {
				List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
				for (JecnTreeBean jecnTreeBean : list) {
					WebTreeBean webTreeBean = new WebTreeBean();
					webTreeBean
							.setId(jecnTreeBean.getId().toString() + "_" + jecnTreeBean.getTreeNodeType().toString());
					webTreeBean.setText(jecnTreeBean.getName());
					webTreeBean.setIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString() + ".gif");
					webTreeBean.setLeaf(!jecnTreeBean.isChildNode());
					webTreeBean.setType(jecnTreeBean.getTreeNodeType().toString());
					listWebTreeBean.add(webTreeBean);

				}
				JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("", e);
		}

		return null;
	}

	/**
	 * @author xiaobo
	 * @des 获取岗位组和岗位
	 * @return
	 * @throws Exception
	 * */

	public String getChildsGroupandOrg() throws Exception {

		List<JecnTreeBean> list;
		try {
			list = organizationService.getGroups(orgId, getProjectId());
			if (list != null && list.size() > 0) {
				List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
				for (JecnTreeBean jecnTreeBean : list) {
					WebTreeBean webTreeBean = new WebTreeBean();
					webTreeBean
							.setId(jecnTreeBean.getId().toString() + "_" + jecnTreeBean.getTreeNodeType().toString());
					webTreeBean.setText(jecnTreeBean.getName());
					webTreeBean.setIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString() + ".gif");
					webTreeBean.setLeaf(!jecnTreeBean.isChildNode());
					webTreeBean.setType(jecnTreeBean.getTreeNodeType().toString());
					listWebTreeBean.add(webTreeBean);
				}
				JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-12-26
	 * @description:组织列表查询
	 * @return
	 */
	public String searchOrgList() {
		try {
			int total = organizationService.getTotalSearchOrg(searchName, getProjectId());

			List<JecnTreeBean> list = organizationService.searchOrg(searchName, start, limit, getProjectId());
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("组织列表查询报错", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-12-26
	 * @description:岗位列表查询
	 * @return
	 */
	public String searchPosList() {
		try {
			int total = organizationService.getTotalSearchPos(searchName, getProjectId());

			List<JecnTreeBean> list = organizationService.searchPos(searchName, start, limit, getProjectId());
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("岗位列表查询报错", e);
		}
		return null;
	}

	/**
	 * 部门清单
	 * 
	 * @author fuzhh 2013-12-3
	 * @return
	 */
	public String findOrgList() {
		try {
			// 获取部门所在级别
			int depth = organizationService.findOrgDepth(orgId);
			// 获取数据
			orgList = organizationService.findOrgList(orgId, getProjectId(), depth, showAll);
			// 数据
			StringBuffer storesBuf = new StringBuffer();
			storesBuf.append("[");
			for (int i = 0; i < orgList.getLevelTotal(); i++) {
				storesBuf.append(JecnCommon.storesJson(i + "level", i + "orgName") + ",");
			}
			storesBuf.append(JecnCommon.storesJson("orgId", "orgId") + ",");
			storesBuf.append(JecnCommon.storesJson("orgDuties", "orgDuties") + ",");
			storesBuf.append(JecnCommon.storesJson("flowName", "flowName") + ",");
			storesBuf.append(JecnCommon.storesJson("flowId", "flowId") + ",");
			storesBuf.append(JecnCommon.storesJson("flowNum", "flowNum") + ",");
			storesBuf.append(JecnCommon.storesJson("flowKpi", "flowKpi") + ",");
			storesBuf.append(JecnCommon.storesJson("versionId", "versionId") + ",");
			storesBuf.append(JecnCommon.storesJson("typeByData", "typeByData") + ",");
			storesBuf.append(JecnCommon.storesJson("resPeopleName", "resPeopleName") + ",");
			storesBuf.append(JecnCommon.storesJson("guardianId", "guardianId") + ",");
			storesBuf.append(JecnCommon.storesJson("guardianName", "guardianName") + ",");
			storesBuf.append(JecnCommon.storesJson("draftPerson", "draftPerson") + ",");
			storesBuf.append(JecnCommon.storesJson("expiry", "expiry") + ",");
			storesBuf.append(JecnCommon.storesJson("optimization", "optimization") + ",");
			storesBuf.append(JecnCommon.storesJson("pubDate", "pubDate") + ",");
			storesBuf.append(JecnCommon.storesJson("keyActive", "keyActive") + "]");
			String stores = storesBuf.toString();

			// 表头
			StringBuffer columnsBuf = new StringBuffer();
			columnsBuf.append("[new Ext.grid.RowNumberer(),");
			for (int i = 0; i < orgList.getLevelTotal(); i++) {
				if (i >= depth) {
					columnsBuf.append(JecnCommon.columnsJson(i + JecnUtil.getValue("level"), i + "level", null) + ",");
				}
			}
			// 部门职责
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("departmentsResponsibility"), "orgDuties",
					"commonNameRenderer")
					+ ",");
			// 流程名称
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("processName"), "flowName",
					"orgProcessListNameRenderer")
					+ ",");
			// 流程编号
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("processNumber"), "flowNum", null) + ",");
			// 流程KPI
			if (JecnUtil.isVersionType()) {
				columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("processKPI"), "flowKpi", null) + ",");
			}
			// 活动
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("keyActivities"), "keyActive", null) + ",");
			// 流程责任人
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("processResponsiblePersons"), "resPeopleName",
					null)
					+ ",");
			boolean isGuardShow = false;
			if ("1".equals(JecnContants.getValue("guardianPeople"))) {
				isGuardShow = true;
			}
			if (isGuardShow) {
				// 流程监护人
				columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("processTheGuardian"), "guardianName", null)
						+ ",");
			}
			// 拟稿人
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("peopleMakeADraft"), "draftPerson", null) + ",");
			// 版本号
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("versionNumber"), "versionId", null) + ",");
			// 发布日期
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("releaseDate"), "pubDate", null) + ",");
			// 有效期
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("validity"), "expiry", null) + ",");
			// 是否及时优化
			columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("theTimelyOptimization"), "optimization", null)
					+ "]");
			String columns = columnsBuf.toString();

			StringBuffer sb = new StringBuffer("[");
			for (int i = 0; i < orgList.getOrgRelatsList().size(); i++) {
				StringBuffer orgStr = new StringBuffer();
				OrgRelateFlowBean orgRelateFlow = orgList.getOrgRelatsList().get(i);
				// 组织ID
				orgStr.append(JecnCommon.contentJson("orgId", orgRelateFlow.getOrgId()) + ",");
				// 组织名称
				String orgName = "<a href='organization.action?orgId=" + orgRelateFlow.getOrgId()
						+ "' target='_blank' style='cursor: pointer;' class='table'>" + orgRelateFlow.getOrgName()
						+ "</a>";
				orgStr.append(JecnCommon.contentJson(orgRelateFlow.getLevel() + "orgName", orgName) + ",");
				// 组织职责
				String orgDuties = orgRelateFlow.getOrgDuties();
				orgStr.append(JecnCommon.contentJson("orgDuties", orgDuties));
				// 流程ID集合
				Set<Long> flowIdSet = orgRelateFlow.getProcessMap().keySet();
				// 是否为第一条
				boolean isFlowOne = true;
				if (flowIdSet.size() > 0) {
					for (Long fId : flowIdSet) {
						ProcessListBean processListBean = orgRelateFlow.getProcessMap().get(fId);
						sb.append("{");
						if (isFlowOne) {
							sb.append(orgStr + ",");
							isFlowOne = false;
						}
						// 流程Id
						sb.append(JecnCommon.contentJson("flowId", processListBean.getFlowId()) + ",");
						// 流程名称
						sb.append(JecnCommon.contentJson("flowName", processListBean.getFlowName()) + ",");
						// 有效期
						String expiry = "";
						if (processListBean.getExpiry() != null) {
							expiry = processListBean.getExpiry().toString();
							if (processListBean.getExpiry() == 0L) {
								expiry = JecnUtil.getValue("permanent");
							}
						} else {
							expiry = JecnUtil.getValue("permanent");
						}
						sb.append(JecnCommon.contentJson("expiry", expiry) + ",");
						// 流程编号
						sb.append(JecnCommon.contentJson("flowNum", processListBean.getFlowNumber()) + ",");
						// 流程KPI
						StringBuffer flowKpi = new StringBuffer();
						flowKpi.append("<div style='width:100%' align='left'><table width='100%'>");
						// kpi ID集合
						Set<Long> kpiIdSet = processListBean.getFlowKPIMap().keySet();
						for (Long kpiId : kpiIdSet) {
							String kpi = processListBean.getFlowKPIMap().get(kpiId);
							flowKpi.append("<tr><td align='center'>" + kpi + "</td></tr>");
						}
						flowKpi.append("</table></div>");
						sb.append(JecnCommon.contentJson("flowKpi", flowKpi) + ",");
						// 关键活动
						StringBuffer keyActive = new StringBuffer();
						keyActive.append("<div style='width:100%' align='left'><table width='100%'>");
						// 活动ID 集合
						Set<Long> activeIdSet = processListBean.getProcessActiveMap().keySet();
						for (Long activeId : activeIdSet) {
							String activeName = processListBean.getProcessActiveMap().get(activeId);
							keyActive.append("<tr><td align='left'><a href='processActive.action?activeId=" + activeId
									+ "' style='cursor: pointer;' target='_blank' class='table'>" + activeName
									+ "</a></td></tr>");
						}
						keyActive.append("</table></div>");
						sb.append(JecnCommon.contentJson("keyActive", keyActive) + ",");
						// 流程责任人
						sb.append(JecnCommon.contentJson("resPeopleName", processListBean.getResPeopleName()) + ",");
						// 流程监护人
						sb.append(JecnCommon.contentJson("guardianName", processListBean.getGuardianName()) + ",");
						// 拟稿人
						sb.append(JecnCommon.contentJson("draftPerson", processListBean.getDraftPerson()) + ",");
						// 流程版本号
						sb.append(JecnCommon.contentJson("versionId", processListBean.getVersionId()) + ",");
						// 发布时间
						sb.append(JecnCommon.contentJson("pubDate", processListBean.getPubDate()) + ",");
						// 是否及时优化
						String optimizationVal = "";
						if (1 == processListBean.getOptimization()) {
							// 是
							optimizationVal = JecnUtil.getValue("is");
						} else if (2 == processListBean.getOptimization()) {
							// 否
							optimizationVal = JecnUtil.getValue("no");
						}
						sb.append(JecnCommon.contentJson("optimization", optimizationVal) + ",");
						sb.append(JecnCommon.contentJson("typeByData", processListBean.getFlowState()));
						sb.append("},");
					}
				} else {
					sb.append("{");
					sb.append(orgStr);
					sb.append("},");
				}

			}
			String str = "";
			if (sb.toString().endsWith(",")) {
				str = sb.substring(0, sb.length() - 1) + "]";
			} else {
				str = sb.append("]").toString();
			}
			String json = JecnCommon.composeJson(stores, columns, str, orgList.getNoPubFlowTotal(), orgList
					.getApproveFlowTotal(), orgList.getPubFlowTotal(), orgList.getOrgTotal());
			json = json.substring(0, json.length() - 1);
			json += ",\"flowTotal\":" + orgList.getFlowTotal() + "}";
			outJsonString(json);
		} catch (Exception e) {
			log.error("查询部门清单错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * 组织清单下载
	 * 
	 */
	public String downLoadOrgList() {
		try {
			// 获取部门所在级别
			int depth = organizationService.findOrgDepth(orgId);
			// 获取数据
			orgList = organizationService.findOrgList(orgId, getProjectId(), depth, true);
			if (orgList == null) {
				// 没有查询到岗位信息!
				outOrgHtml(this.getText("沒有查询到组织信息！"));
				return null;
			}
			// 生成bute[];
			content = OrdListDownload.createOrgListDesc(orgList, depth);
			// 组织清单
			fileName = JecnUtil.getValue("groupListing") + ".xls";
		} catch (Exception e) {
			log.error("下载组织清单异常！！！", e);
		}
		return SUCCESS;
	}

	/**
	 * 部门职责
	 * 
	 * @author fuzhh Dec 5, 2012
	 * @return
	 */
	public String findOrgDuties() {
		JecnFlowOrg jecnFlowOrg = organizationService.get(orgId);
		orgDuties = jecnFlowOrg.getOrgRespon();
		if (orgDuties != null) {
			orgDuties = orgDuties.replaceAll("\n", "<br>");
			orgDuties = orgDuties.replaceAll(" ", "&nbsp;");
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-11-30
	 * @description:部门树加载选择
	 * @return
	 */
	public String getChildOrg() {
		List<JecnTreeBean> list;
		try {
			list = organizationService.getChildOrgs(orgId, getProjectId());
			if (list != null && list.size() > 0) {
				List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
				for (JecnTreeBean jecnTreeBean : list) {
					WebTreeBean webTreeBean = new WebTreeBean();
					webTreeBean.setId(jecnTreeBean.getId().toString());
					webTreeBean.setText(jecnTreeBean.getName());
					webTreeBean.setIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString() + ".gif");
					webTreeBean.setLeaf(!jecnTreeBean.isChildNode());
					webTreeBean.setType(jecnTreeBean.getTreeNodeType().toString());
					listWebTreeBean.add(webTreeBean);

				}
				JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("组织加载出错", e);
		}
		return null;
	}

	/**
	 * 人员选择查询数据
	 * 
	 * @author fuzhh Nov 30, 2012
	 * @return
	 */
	public String peopleSearch() {
		try {
			List<JecnTreeBean> list = personService.searchUserByName(searchName, getProjectId());
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("查询人员数据错误！", e);
		}
		return null;
	}

	/**
	 * 组织选择查询数据
	 * 
	 * @author fuzhh Nov 30, 2012
	 * @return
	 */
	public String orgSearch() {
		try {
			List<JecnTreeBean> list = organizationService.searchByName(searchName, getProjectId());
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("查询组织数据错误！", e);
		}
		return null;
	}

	/**
	 * 岗位选择查询数据
	 * 
	 * @author fuzhh Nov 30, 2012
	 * @return
	 */
	public String posSearch() {
		try {
			List<JecnTreeBean> list = organizationService.searchPositionByName(searchName, getProjectId());
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("查询岗位数据错误！", e);
		}
		return null;
	}

	/***
	 * @author xiaobo 2014-05-27
	 * @desc:岗位组搜索
	 * @return
	 * **/
	public String groupSearch() throws Exception {
		try {
			// 调用模糊查询方法查询相关数据
			List<JecnTreeBean> list = organizationService.searchGroupByName(searchName, getProjectId());
			if (list != null && list.size() > 0) {
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-12-1
	 * @description:树加载 根据岗位ID查询人员
	 * @return
	 */
	public String getChildPerson() {
		List<JecnTreeBean> list;
		try {
			list = personService.getChildPoss(orgId);
			if (list != null && list.size() > 0) {
				List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
				for (JecnTreeBean jecnTreeBean : list) {
					WebTreeBean webTreeBean = new WebTreeBean();
					webTreeBean.setId(jecnTreeBean.getId().toString());
					webTreeBean.setText(jecnTreeBean.getName());
					webTreeBean.setIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString() + ".gif");
					webTreeBean.setLeaf(!jecnTreeBean.isChildNode());
					webTreeBean.setType(jecnTreeBean.getTreeNodeType().toString());
					listWebTreeBean.add(webTreeBean);

				}
				JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("根据岗位查人员出错", e);
		}
		return null;
	}

	/**
	 * 获取岗位说明书
	 * 
	 * @author fuzhh Dec 4, 2012
	 * @return
	 */
	public String orgSpecification() {
		try {
			descriptionBean = organizationService.getJobDescription(posId);
			if (descriptionBean != null) {
				String positionsNature = descriptionBean.getPositionsNature();
				if (positionsNature != null) {
					positionsNature = positionsNature.replaceAll("\n", "<br>");
					descriptionBean.setPositionsNature(positionsNature);
				}
				String schoolExperience = descriptionBean.getSchoolExperience();
				if (schoolExperience != null) {
					schoolExperience = schoolExperience.replaceAll("\n", "<br>");
					descriptionBean.setSchoolExperience(schoolExperience);
				}
				String knowledge = descriptionBean.getKnowledge();
				if (knowledge != null) {
					knowledge = knowledge.replaceAll("\n", "<br>");
					descriptionBean.setKnowledge(knowledge.replaceAll(" ", "&nbsp;"));
				}
				String positionSkill = descriptionBean.getPositionSkill();
				if (positionSkill != null) {
					positionSkill = positionSkill.replaceAll("\n", "<br>");
					descriptionBean.setPositionSkill(positionSkill.replaceAll(" ", "&nbsp;"));
				}
				String positionImportance = descriptionBean.getPositionImportance();
				if (positionImportance != null) {
					positionImportance = positionImportance.replaceAll("\n", "<br>");
					descriptionBean.setPositionImportance(positionImportance.replaceAll(" ", "&nbsp;"));
				}
				String positionDiathesisModel = descriptionBean.getPositionDiathesisModel();
				if (positionDiathesisModel != null) {
					positionDiathesisModel = positionDiathesisModel.replaceAll("\n", "<br>");
					descriptionBean.setPositionDiathesisModel(positionDiathesisModel.replaceAll(" ", "&nbsp;"));
				}
			}
		} catch (Exception e) {
			log.error("查询岗位说明书异常！！！", e);
		}
		return SUCCESS;
	}

	/**
	 * 修改密码
	 * 
	 * @author fuzhh Jan 23, 2013
	 * @return
	 */
	public String updatePassword() {
		try {

			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
			JecnUser user = personService.get(webLoginBean.getJecnUser().getPeopleId());
			if (StringUtils.isBlank(newPassword) || StringUtils.isBlank(originalPassword)) {
				log.error("密码为空 newPassword=" + newPassword + "或originalPassword=" + originalPassword);
				// 原密码错误
				outJsonString("{success:true,msg:2}");
				return SUCCESS;
			}
			if (user.getPassword().equals(JecnUserSecurityKey.getsecret(originalPassword.trim()))) {
				personService.userPasswordUpdate(webLoginBean.getJecnUser().getPeopleId(), JecnUserSecurityKey
						.getsecret(newPassword.trim()));
				// 修改成功
				outJsonString("{success:true,msg:1}");
			} else {
				// 原密码错误
				outJsonString("{success:true,msg:2}");
			}
		} catch (Exception e) {
			log.error("修改密码异常！", e);
			// 修改密码异常！
			outJsonString("{success:true,msg:3}");
		}
		return SUCCESS;
	}

	/**
	 * 下载岗位说明书
	 * 
	 */
	public String downLoadPosition() {
		try {
			// 岗位说明书数据
			ByteFile byteFile = organizationService.getJobDescriptionByteFile(posId);
			if (byteFile.getBytes() == null) {
				// 没有查询到岗位信息!
				outOrgHtml(this.getText("noInformationQueryToThePost"));
				return null;
			}
			// 生成bute[];
			content = byteFile.getBytes();
			fileName = byteFile.getFileName();
		} catch (Exception e) {
			log.error("查询岗位说明书异常！！！", e);
		}
		if (descriptionBean == null) {
			throw new NullPointerException("下载岗位说明书异常！");
		}
		return SUCCESS;
	}

	/**
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() throws Exception {
		// 获得文件
		if (content == null) {
			// 下载文件信息异常
			outResultHtml(this.getText("fileInformation"), "");
			throw new IllegalArgumentException("getInputStream方法中参数为空!");
		}
		return new ByteArrayInputStream(content);
	}

	// 对于配置中的 ${fileName}, 获得下载保存时的文件名
	public String getFileName() {
		return JecnCommon.getDownFileNameByEncoding(getResponse(), fileName, "");
	}

	/**
	 * @author yxw 2013-1-28
	 * @description:组织页面跳转
	 * @return
	 */
	public String organization() {
		// 验证组织是否被删除
		JecnFlowOrg org = organizationService.get(orgId);
		if (org != null) {
			if (org.getDelState() == 1) {

				super.ResultHtml(JecnUtil.getValue("thisOrgIsOnRecycle"), "");
				return null;
			}
		} else {

			super.ResultHtml(JecnUtil.getValue("thisOrgHasDelete"), "");
			return null;
		}
		return "organization";
	}

	/**
	 * @author yxw 2013-3-28
	 * @description:空方法，用于树节点双击权限验证
	 * @return
	 */
	public String organizationLook() {
		return null;
	}

	public String stationLook() {
		return null;
	}

	/**
	 * @author yxw 2013-3-7
	 * @description:人员管理列表
	 * @return
	 */
	public String personManageList() {

		UserWebSearchBean userWebSearchBean = new UserWebSearchBean();
		userWebSearchBean.setLoginName(loginName);
		userWebSearchBean.setTrueName(trueName);
		userWebSearchBean.setRoleType(roleType);
		userWebSearchBean.setOrgId(orgId);
		userWebSearchBean.setOrgName(orgName);
		userWebSearchBean.setPosId(posId);
		userWebSearchBean.setPosName(posName);
		try {
			int total = personService.getTotalPerson(userWebSearchBean);
			if (total > 0) {
				List<UserWebInfoBean> list = personService.getListPerson(userWebSearchBean, start, limit);
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonPage(jsonArray.toString(), total);

			} else {
				outJsonPage("[]", total);
			}
		} catch (Exception e) {

		}
		return null;
	}

	/**
	 * @author yxw 2013-3-7
	 * @description:角色管理
	 * @return
	 */
	public String roleManageList() {
		try {
			roleType = "";
			int total = roleService.getTotalRole(roleName, roleType);
			if (total > 0) {
				List<RoleWebInfoBean> list = roleService.getRoleList(roleName, roleType, start, limit);
				JSONArray jsonArray = JSONArray.fromObject(list);
				outJsonPage(jsonArray.toString(), total);
			} else {
				outJsonPage("[]", total);
			}
		} catch (Exception e) {
			log.error("角色管理查询失败", e);
		}
		return null;
	}

	/**
	 * @author hyl
	 * @description:通过peopleId获得用户信息
	 * @return
	 */
	public String getUserInfoById() {
		Long peopleId = Long.valueOf(ServletActionContext.getRequest().getParameter("peopleId"));
		try {

			userInfo = personService.getUserInfoById(peopleId);

		} catch (Exception e) {

			e.printStackTrace();
			log.error("通过peopleId获得人物信息异常！getUserInfoById", e);
		}

		return SUCCESS;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.orgId = Long.valueOf(node.split("_")[0]);
		this.node = node;
	}

	public IOrganizationService getOrganizationService() {
		return organizationService;
	}

	public void setOrganizationService(IOrganizationService organizationService) {
		this.organizationService = organizationService;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public IPersonService getPersonService() {
		return personService;
	}

	public void setPersonService(IPersonService personService) {
		this.personService = personService;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public int getPosLoadtype() {
		return posLoadtype;
	}

	public void setPosLoadtype(int posLoadtype) {
		this.posLoadtype = posLoadtype;
	}

	public JobDescriptionBean getDescriptionBean() {
		return descriptionBean;
	}

	public void setDescriptionBean(JobDescriptionBean descriptionBean) {
		this.descriptionBean = descriptionBean;
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public long getPosId() {
		return posId;
	}

	public void setPosId(long posId) {
		this.posId = posId;
	}

	public String getOrgDuties() {
		return orgDuties;
	}

	public void setOrgDuties(String orgDuties) {
		this.orgDuties = orgDuties;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public List<OrgRelateFlowBean> getOrgRelateFlowList() {
		return orgRelateFlowList;
	}

	public void setOrgRelateFlowList(List<OrgRelateFlowBean> orgRelateFlowList) {
		this.orgRelateFlowList = orgRelateFlowList;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getOriginalPassword() {
		return originalPassword;
	}

	public void setOriginalPassword(String originalPassword) {
		this.originalPassword = originalPassword;
	}

	public IJecnTaskSearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(IJecnTaskSearchService searchService) {
		this.searchService = searchService;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public IJecnRoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(IJecnRoleService roleService) {
		this.roleService = roleService;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getKeepLogin() {
		return keepLogin;
	}

	public void setKeepLogin(String keepLogin) {
		this.keepLogin = keepLogin;
	}

	public String getPosType() {
		return posType;
	}

	public void setPosType(String posType) {
		this.posType = posType;
	}

	public OrgListBean getOrgList() {
		return orgList;
	}

	public void setOrgList(OrgListBean orgList) {
		this.orgList = orgList;
	}

	public boolean isShowAll() {
		return showAll;
	}

	public void setShowAll(boolean showAll) {
		this.showAll = showAll;
	}
}
