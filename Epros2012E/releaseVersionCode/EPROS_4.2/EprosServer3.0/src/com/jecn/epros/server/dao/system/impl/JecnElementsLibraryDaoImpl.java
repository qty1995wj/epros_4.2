package com.jecn.epros.server.dao.system.impl;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnElementsLibrary;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.system.IJecnElementsLibraryDao;

/**
 * 
 * @author ZXH
 * @date 2015-12-2
 */
public class JecnElementsLibraryDaoImpl extends AbsBaseDao<JecnElementsLibrary, Long> implements
		IJecnElementsLibraryDao {

	@Override
	public void updateElementsLibary(List<String> list, int type) throws Exception {
		if (list == null) {
			return;
		}
		// 还原默认
		String sql = "UPDATE JECN_ELEMENTS_LIBRARY SET VALUE = 0 WHERE TYPE = ?";
		this.execteNative(sql, type);

		if (list.size() == 0) {
			return;
		}
		// 更新顺序
		List<JecnElementsLibrary> allList = this.getAllElementsLibraryByType(type);
		for (int i = 1; i < list.size() + 1; i++) {
			for (JecnElementsLibrary jecnElementsLibrary : allList) {
				if (jecnElementsLibrary.getMark().equals(list.get(i - 1))) {
					jecnElementsLibrary.setSort(i);
					jecnElementsLibrary.setValue(1);
					this.update(jecnElementsLibrary);
					break;
				}
			}
		}
		this.getSession().flush();
	}

	/**
	 * 初始化显示的元素
	 * 
	 * @param type
	 *            0：流程架构；1：流程;2：组织
	 * @param value
	 *            0:不显示，1：显示
	 * @return
	 * @throws Exception
	 */
	public List<JecnElementsLibrary> getShowElementsLibraryByType(int type) throws Exception {
		String sql = "SELECT * FROM JECN_ELEMENTS_LIBRARY WHERE TYPE = ? AND VALUE = 1 ORDER BY SORT";
		return this.listNativeAddEntity(sql, type);
	}

	@Override
	public List<JecnElementsLibrary> getAllElementsLibraryByType(int type) throws Exception {
		String sql = "SELECT * FROM JECN_ELEMENTS_LIBRARY WHERE TYPE = ?";
		return this.listNativeAddEntity(sql, type);
	}

	@Override
	public List<JecnElementsLibrary> getAddElementsLibraryByType(int type) throws Exception {
		String sql = "SELECT * FROM JECN_ELEMENTS_LIBRARY WHERE TYPE = ? AND VALUE = 0";
		return this.listNativeAddEntity(sql, type);
	}
}
