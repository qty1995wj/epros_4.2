package epros.draw.gui.top.toolbar.io;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.data.figure.JecnModeFigureData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.IconFigure;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.unit.JecnFlowTemplate;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitlePanel;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnXmlUtil;
import epros.draw.visio.JecnFileNameFilter;

/**
 * 流程图导入
 * 
 * @author Administrator
 * 
 */
public class JecnFlowImport {
	/** 文件名称 */
	private String fileName;
	/** 文件路径 */
	private String filePath;

	private static String[] fileEX = new String[] { "epr", "epros" };

	private static Logger log = Logger.getLogger(JecnFlowImport.class);

	public JecnFlowImport() {
	}

	public void initialize() {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFileDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}

		// 文件选择器
		JFileChooser fileChooser = new JecnFileChooser(pathUrl);
		// 移除系统给定的文件过滤器
		fileChooser.setAcceptAllFileFilterUsed(false);
		// 标题
		fileChooser.setDialogTitle(JecnResourceUtil.getJecnResourceUtil().getValue("pleaseChooseTheFile"));
		// 过滤文件后缀
		fileChooser.setFileFilter(new JecnFileNameFilter(fileEX));

		// 设置按钮为打开
		fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);

		// 显示文件选择器
		int i = fileChooser.showOpenDialog(JecnDrawMainPanel.getMainPanel());

		// 选择确认（yes、ok）后返回该值。
		if (i == JFileChooser.APPROVE_OPTION) {
			// 获取输出文件的路径
			filePath = fileChooser.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSaveFileDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			// 判断文件后缀名是否为epros结尾
			if (isEndEquals(filePath)) {
				File flowFile = fileChooser.getSelectedFile();
				// 获取文件名称
				fileName = flowFile.getName();
				fileName = fileName.substring(0, fileName.lastIndexOf("."));
				if (EprosType.eprosDraw == JecnSystemStaticData.getEprosType()) {
					// 获取输出文件的路径
					String xmlPath = fileChooser.getSelectedFile().getPath();
					// 判断文件后缀名是否为epros结尾
					if (!isEndEquals(xmlPath)) {
						xmlPath = xmlPath + ".epr";
					}
					boolean isOpen = isOpen(xmlPath);
					if (isOpen) {
						return;
					}
				}
				// 弹出确认框，覆盖或取消
				boolean flag = importConfirm();
				if (flag) {
					recoveryFlowFile(flowFile);
				}
			}
		}
	}

	/**
	 * 覆盖流程图/组织图/流程地图
	 * 
	 * @author weidp
	 * @date 2014-11-3 下午03:33:09
	 * @param flowFile
	 */
	public void recoveryFlowFile(File flowFile) {
		WorkTask task = new WorkTask();
		task.setFlowFile(flowFile);
		String error = JecnLoading.start(task);
		if (!DrawCommon.isNullOrEmtryTrim(error)) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil.getJecnResourceUtil()
					.getValue("openTheAbnormal"));
		}
	}

	/**
	 * 导入确认
	 * 
	 * @author weidp
	 * @date 2014-10-29 下午02:10:50
	 * @return
	 */
	private boolean importConfirm() {
		JecnDrawDesktopPane drawDesk = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (drawDesk == null) {
			// 判断是否为设计器
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
				// 必须要指定一个画图面板导入
				return false;
			} else {
				return true;
			}
		}
		// 提示语句
		String msg = "";
		switch (drawDesk.getFlowMapData().getMapType()) {
		case totalMap:
			msg = JecnResourceUtil.getJecnResourceUtil().getValue("totalMapTipMsg");
			break;
		case partMap:
			msg = JecnResourceUtil.getJecnResourceUtil().getValue("partMapTipMsg");
			break;
		case orgMap:
			msg = JecnResourceUtil.getJecnResourceUtil().getValue("orgMapTipMsg");
			break;
		case totalMapRelation:
			msg = JecnResourceUtil.getJecnResourceUtil().getValue("orgMapTipMsg");
			break;
		}
		// 弹出提示框
		int result = JecnOptionPane.showOptionDialog(null, msg, JecnResourceUtil.getJecnResourceUtil().getValue("tip"),
				JecnOptionPane.OK_CANCEL_OPTION, JecnOptionPane.INFORMATION_MESSAGE, null, new Object[] {
						JecnResourceUtil.getJecnResourceUtil().getValue("cover"),
						JecnResourceUtil.getJecnResourceUtil().getValue("cancel") }, 0);
		return result == JecnOptionPane.OK_OPTION ? true : false;
	}

	/**
	 * 判断文件是否以 数组中的数据 结尾
	 * 
	 * @author fuzhh 2014-1-7
	 * @param filePath
	 * @return
	 */
	public static boolean isEndEquals(String filePath) {
		for (String endEqu : fileEX) {
			if (filePath.endsWith(endEqu)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断界面是否打开
	 * 
	 * @author fuzhh Aug 24, 2012
	 * @param flowId
	 *            流程ID
	 * @param mapType
	 *            面板类型
	 * @return true 表示打开 ， false 表示未打开
	 */
	public static boolean isOpen(String path) {
		int count = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
		for (int i = 0; i < count; i++) {
			Component component = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabComponentAt(i);
			if (component instanceof JecnTabbedTitlePanel) {
				JecnTabbedTitlePanel tabbedTitlePanel = (JecnTabbedTitlePanel) component;
				JecnDrawDesktopPane drawDesktopPane = tabbedTitlePanel.getCurrDesktopPane();
				if (path.equals(drawDesktopPane.getFlowFilePath())) {
					JecnDrawMainPanel.getMainPanel().getTabbedPane().setSelectedComponent(
							drawDesktopPane.getScrollPanle());
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @author yxw 2012-8-10
	 * @description：面板保存进度控制线程
	 */
	class WorkTask extends SwingWorker<String, Void> {
		private File flowFile;

		@Override
		protected String doInBackground() throws Exception {
			imporotFlow(flowFile);
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}

		public File getFlowFile() {
			return flowFile;
		}

		public void setFlowFile(File flowFile) {
			this.flowFile = flowFile;
		}
	}

	/**
	 * 
	 * 打开epros文件
	 * 
	 * @param path
	 */
	public void inportEprosFile(String path) {
		if (DrawCommon.isNullOrEmtryTrim(path)) {
			return;
		}
		boolean isOpen = isOpen(path);
		if (isOpen) {
			return;
		}
		File file = new File(path);
		if (file.exists() && file.isFile()) {
			// 获取文件路径
			filePath = file.getPath();
			if (filePath != null && isEndEquals(filePath)) {// 文件路径后缀".epr"
				// 文件名称
				fileName = file.getName();
				fileName = fileName.substring(0, fileName.lastIndexOf("."));
				recoveryFlowFile(file);
			}
		}
	}

	/**
	 * 
	 * 读文件
	 * 
	 * @param flowFile
	 */
	private void imporotFlow(File flowFile) {
		try {
			Document doc = JecnXmlUtil.getDocumentByFile(flowFile);
			if (doc == null) {
				JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
						.getJecnResourceUtil().getValue("notFindTheDocument"), JecnResourceUtil.getJecnResourceUtil()
						.getValue("tip"), JOptionPane.ERROR_MESSAGE);
				return;
			}
			// 给面板赋值
			gainWorkflowDate(doc);
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
				if (getWorkflow() != null) {
					getWorkflow().getFlowMapData().setSave(true);
				}
			}
		} catch (Exception e) {
			log.error("打开流程异常！", e);
			JecnLoading.setError("1");
		}

	}

	/**
	 * 读取XML workflowInfo节点的数据
	 */
	private void gainWorkflowDate(Document doc) {
		// 获取读取XML workflowInfo节点的数据
		NodeList workflowInfo = doc.getElementsByTagName("workflowInfo");
		if (workflowInfo.getLength() > 0) {
			// 新旧版 标识
			String workType = null;
			for (int i = 0; i < workflowInfo.getLength(); i++) {
				NamedNodeMap attributes = workflowInfo.item(i).getAttributes();
				// 新版和旧版标识 0为新版
				Node workTypeNode = attributes.getNamedItem("WorkType");
				if (workTypeNode != null) {
					workType = workTypeNode.getNodeValue();
					// System.out.println("WorkType===" + WorkType);
				}
				boolean isHFlag = true;
				// 面板的横向和纵向标识 0 为横向 1为纵向
				Node isHFlagNode = attributes.getNamedItem("isHFlag");
				if (isHFlagNode != null) {
					String isHStr = isHFlagNode.getNodeValue();
					if ("0".equals(isHStr)) {
						isHFlag = true;
					} else {
						isHFlag = false;
					}
				}
				// 导如XML的类型 0是流程地图;1是流程图;2是组织图
				Node flowClassifyNode = attributes.getNamedItem("flowClassify");
				if (flowClassifyNode != null) {
					String flowClassify = flowClassifyNode.getNodeValue();
					if ("1".equals(flowClassify)) {
						// 判断是否为设计器
						if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
							if (getWorkflow().getFlowMapData().getMapType() != MapType.partMap) {
								JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
										.getJecnResourceUtil().getValue("fileFormatDoesNotMatch"));

								return;
							} else {
								this.getWorkflow().getFlowMapData().setHFlag(isHFlag);
							}
						} else {
							// 创建流程图
							createPartMap(fileName, isHFlag);
						}
					} else if ("0".equals(flowClassify)) {
						// 判断是否为设计器
						if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
							if (getWorkflow().getFlowMapData().getMapType() != MapType.totalMap
									&& getWorkflow().getFlowMapData().getMapType() != MapType.totalMapRelation) {
								JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
										.getJecnResourceUtil().getValue("fileFormatDoesNotMatch"));

								return;
							}
						} else {
							// 创建流程地图
							createTotalMap(fileName);
						}
					} else if ("2".equals(flowClassify)) {
						// 判断是否为设计器；
						if (getWorkflow() == null || getWorkflow().getFlowMapData().getMapType() != MapType.orgMap) {
							JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
									.getJecnResourceUtil().getValue("fileFormatDoesNotMatch"));
							return;
						}
					}
				}
				if (getWorkflow() == null) {
					JecnLogConstants.Log_JecnFlowImport.info("JecnFlowImport类中的导入方法创建面板失败!");
					return;
				}
				if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
					JecnIOUtil.removeAllWorkflow();
				}
				getWorkflow().setFlowFilePath(filePath);

				// 获取面板数据
				JecnFlowMapData flowMap = getWorkflow().getFlowMapData();

				// 面板的宽和高
				Node workflowWidthNode = attributes.getNamedItem("WorkflowWidth");
				Node workflowHeightNode = attributes.getNamedItem("WorkflowHeight");
				if (workflowWidthNode != null && workflowHeightNode != null) {
					int WorkflowWidth = Integer.parseInt(workflowWidthNode.getNodeValue());
					int WorkflowHeight = Integer.parseInt(workflowHeightNode.getNodeValue());
					// 设置面板的大小
					refrashPanelSize(getWorkflow(), WorkflowWidth, WorkflowHeight);

					// 给面板宽赋值
					flowMap.setWorkflowWidth(WorkflowWidth);
					// 给面板高赋值
					flowMap.setWorkflowWidth(WorkflowHeight);

				}

				// 目的
				Node flowPurpose = attributes.getNamedItem("Purpose");
				if (flowPurpose != null) {
					if (!DrawCommon.isNullOrEmtryTrim(flowPurpose.getNodeValue())) {
						flowMap.getFlowOperationBean().setFlowPurpose(flowPurpose.getNodeValue());
					}
				}
				// 适用范围
				Node applicability = attributes.getNamedItem("Applicability");
				if (applicability != null) {
					if (!DrawCommon.isNullOrEmtryTrim(applicability.getNodeValue())) {
						flowMap.getFlowOperationBean().setApplicability(applicability.getNodeValue());
					}
				}
				// 术语定义
				Node noutGlossary = attributes.getNamedItem("NoutGlossary");
				if (noutGlossary != null) {
					if (!DrawCommon.isNullOrEmtryTrim(noutGlossary.getNodeValue())) {
						flowMap.getFlowOperationBean().setNoutGlossary(noutGlossary.getNodeValue());
					}
				}
				// 流程输入
				Node flowInput = attributes.getNamedItem("FlowInput");
				if (flowInput != null) {
					if (!DrawCommon.isNullOrEmtryTrim(flowInput.getNodeValue())) {
						flowMap.getFlowOperationBean().setFlowInput(flowInput.getNodeValue());
					}
				}
				// 流程输出
				Node flowOutput = attributes.getNamedItem("FlowOutput");
				if (flowOutput != null) {
					if (!DrawCommon.isNullOrEmtryTrim(flowOutput.getNodeValue())) {
						flowMap.getFlowOperationBean().setFlowOutput(flowOutput.getNodeValue());
					}
				}
				// 起始活动
				Node flowStartActive = attributes.getNamedItem("FlowStartActive");
				if (flowStartActive != null) {
					if (!DrawCommon.isNullOrEmtryTrim(flowStartActive.getNodeValue())) {
						flowMap.getFlowOperationBean().setFlowStartActive(flowStartActive.getNodeValue());
					}
				}
				// 终止活动
				Node flowEndActive = attributes.getNamedItem("FlowEndActive");
				if (flowEndActive != null) {
					if (!DrawCommon.isNullOrEmtryTrim(flowEndActive.getNodeValue())) {
						flowMap.getFlowOperationBean().setFlowEndActive(flowEndActive.getNodeValue());
					}
				}
				// 不适用范围
				Node noApplicability = attributes.getNamedItem("NoApplicability");
				if (noApplicability != null) {
					if (!DrawCommon.isNullOrEmtryTrim(noApplicability.getNodeValue())) {
						flowMap.getFlowOperationBean().setNoApplicability(noApplicability.getNodeValue());
					}
				}
				// 概况信息
				Node flowSummarize = attributes.getNamedItem("FlowSummarize");
				if (flowSummarize != null) {
					if (!DrawCommon.isNullOrEmtryTrim(flowSummarize.getNodeValue())) {
						flowMap.getFlowOperationBean().setFlowSummarize(flowSummarize.getNodeValue());
					}
				}
				// 补充说明
				Node flowSupplement = attributes.getNamedItem("FlowSupplement");
				if (flowSupplement != null) {
					if (!DrawCommon.isNullOrEmtryTrim(flowSupplement.getNodeValue())) {
						flowMap.getFlowOperationBean().setFlowSupplement(flowSupplement.getNodeValue());
					}
				}
				// 流程客户
				Node flowCustom = attributes.getNamedItem("FlowCustom");
				if (flowCustom != null) {
					if (!DrawCommon.isNullOrEmtryTrim(flowCustom.getNodeValue())) {
						flowMap.getFlowOperationBean().setFlowCustom(flowCustom.getNodeValue());
					}
				}
				// 流程记录
				Node flowFile = attributes.getNamedItem("FlowFile");
				if (flowFile != null) {
					flowMap.getFlowOperationBean().setFlowFileStr(flowFile.getNodeValue());
				}
				// 操作规范
				Node practices = attributes.getNamedItem("Practices");
				if (practices != null) {
					flowMap.getFlowOperationBean().setPracticesStr(practices.getNodeValue());
				}
				// 相关流程
				Node relatedFlow = attributes.getNamedItem("RelatedFlow");
				if (relatedFlow != null) {
					flowMap.getFlowOperationBean().setRelatedFlowStr(relatedFlow.getNodeValue());
				}
				// 相关制度
				Node relatedRule = attributes.getNamedItem("RelatedRule");
				if (relatedRule != null) {
					flowMap.getFlowOperationBean().setRelatedRuleStr(relatedRule.getNodeValue());
				}
				// 流程评测及关键指标
				Node keyEvaluationIndi = attributes.getNamedItem("KeyEvaluationIndi");
				if (keyEvaluationIndi != null) {
					flowMap.getFlowOperationBean().setKeyEvaluationIndiStr(keyEvaluationIndi.getNodeValue());
				}

				// 流程评测及关键指标
				Node driveType = attributes.getNamedItem("driveType");
				if (driveType != null) {
					flowMap.getFlowOperationBean().setDriveType(driveType.getNodeValue());
				}

				// 流程评测及关键指标
				Node driveRules = attributes.getNamedItem("driveRules");
				if (driveRules != null) {
					flowMap.getFlowOperationBean().setDriveRules(driveRules.getNodeValue());
				}
				// 如果有值代表为新版 添加5个自定义元素
				if (workType != null && !"".equals(workType)) {
					// 自定义1
					Node customOne = attributes.getNamedItem("customOne");
					if (customOne != null) {
						flowMap.getFlowOperationBean().setCustomOne(customOne.getNodeValue());
					}
					// 自定义2
					Node customTwo = attributes.getNamedItem("customTwo");
					if (customTwo != null) {
						flowMap.getFlowOperationBean().setCustomTwo(customTwo.getNodeValue());
					}
					// 自定义3
					Node customThree = attributes.getNamedItem("customThree");
					if (customThree != null) {
						flowMap.getFlowOperationBean().setCustomThree(customThree.getNodeValue());
					}
					// 自定义4
					Node customFour = attributes.getNamedItem("customFour");
					if (customFour != null) {
						flowMap.getFlowOperationBean().setCustomFour(customFour.getNodeValue());
					}
					// 自定义5
					Node customFive = attributes.getNamedItem("customFive");
					if (customFive != null) {
						flowMap.getFlowOperationBean().setCustomFive(customFive.getNodeValue());
					}

					// 相关文件(D2版本特有的)
					// 1或100返回false
					if (!JecnDesignerProcess.getDesignerProcess().isPubShow()) {
						Node flowRelatedFile = attributes.getNamedItem("flowRelatedFile");
						if (flowRelatedFile != null) {
							flowMap.getFlowOperationBean().setFlowRelatedFile(flowRelatedFile.getNodeValue());
						}
					}
				}
			}
			// 如果有值代表为新版 调用新版的导入
			if (workType != null && !"".equals(workType)) {
				// 横分割线
				gainParallelLineData(doc);
				// 竖分割线
				gainVerticalLineData(doc);
				// 给图形赋值
				readXML(doc);
			} else { // 旧版导入
				JecnOldFlowImport oldReadXML = new JecnOldFlowImport();
				oldReadXML.oldReadXML(doc);
			}
		}
		if (getWorkflow().getTipPanel() != null) {
			JecnPaintFigureUnit.addOpenTipPanel();
		}
	}

	/**
	 * 导入图形到面板
	 * 
	 * @param doc
	 * @param workType
	 */
	public void readXML(Document doc) {
		NodeList figures = doc.getElementsByTagName("figures");
		// 获取所有图形集合
		List<JecnBaseFlowElementPanel> baseFlowElementList = gainFiguresDate(figures);
		// 获取所有连接线
		// 连接线
		NodeList commonLines = doc.getElementsByTagName("CommonLines");

		Map<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>> manLineList = new HashMap<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>>();
		gainConnectingLine(commonLines, baseFlowElementList, manLineList);

		// 连接线（带箭头）
		NodeList manLines = doc.getElementsByTagName("manhattanLines");
		gainConnectingLine(manLines, baseFlowElementList, manLineList);

		if (baseFlowElementList != null) {
			// 图形层级排序
			JecnFlowTemplate.sortAllFlowElementCloneList(baseFlowElementList);
			for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
				if (baseFlowElement instanceof JecnBaseFigurePanel) {
					JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) baseFlowElement;
					// 面板添加流程元素
					JecnAddFlowElementUnit.addFigure(figurePanel);
					// 面板最大层级数加一
					getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
				}
			}
		}

		Iterator<JecnBaseManhattanLinePanel> iterator = manLineList.keySet().iterator();
		while (iterator.hasNext()) {
			JecnBaseManhattanLinePanel jecnBaseManhattanLinePanel = (JecnBaseManhattanLinePanel) iterator.next();
			// 获取小线段数据
			List<JecnManhattanRouterResultData> datas = manLineList.get(jecnBaseManhattanLinePanel);
			// 排序
			// datas = jecnBaseManhattanLinePanel.reOrderLinePoint(datas);

			for (JecnManhattanRouterResultData jecnManhattanRouterResultData : datas) {
				jecnBaseManhattanLinePanel.addLineSegment(jecnManhattanRouterResultData.getStartPoint(),
						jecnManhattanRouterResultData.getEndPint());
			}

			// 设置线层级
			jecnBaseManhattanLinePanel.reSetFlowElementZorder();
			// 面板添加流程元素
			JecnAddFlowElementUnit.addImportManLine(jecnBaseManhattanLinePanel);

			jecnBaseManhattanLinePanel.insertTextToLine();
			// 面板最大层级数加一
			getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
		}
		// 斜线，横线，竖线
		NodeList dividing = doc.getElementsByTagName("dividLines");
		List<JecnBaseFlowElementPanel> diviLineList = new ArrayList<JecnBaseFlowElementPanel>();
		getBaseDivedingLineData(dividing, diviLineList);

		for (JecnBaseFlowElementPanel jecnBaseDividingLinePanel : diviLineList) {
			JecnAddFlowElementUnit.importAddDiviLine((JecnBaseDividingLinePanel) jecnBaseDividingLinePanel);
			// 设置线层级
			jecnBaseDividingLinePanel.reSetFlowElementZorder();
			// 面板最大层级数加一
			getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
		}
	}

	/**
	 * 设置连接线的开始图形和结束图形
	 * 
	 * @param panleList
	 *            List<JecnBaseFlowElementPanel>XML读取的所有图形元素
	 * @param manLine
	 *            XML读取的当前曼哈顿线段
	 */
	public void setManLineRefFigure(List<JecnBaseFlowElementPanel> panleList, JecnBaseManhattanLinePanel manLine) {
		if (panleList == null || manLine == null) {
			return;
		}
		JecnManhattanLineData lineData = manLine.getFlowElementData();
		if (lineData.getStartFigureUUID() == null || lineData.getEndFigureUUID() == null) {
			return;
		}
		// System.out.println("开始图形唯一标示 ：" + lineData.getStartFigureUUID());

		// System.out.println("结束图形唯一标示 ：" + lineData.getEndFigureUUID());
		for (JecnBaseFlowElementPanel flowElementPanel : panleList) {
			if (!(flowElementPanel instanceof JecnBaseFigurePanel)) {
				continue;
			}
			JecnFigureData figureData = (JecnFigureData) flowElementPanel.getFlowElementData();
			// System.out.println(figureData.getFlowElementUUID());
			if (lineData.getStartFigureUUID().equals(figureData.getFlowElementUUID())) {
				manLine.setStartFigure((JecnBaseFigurePanel) flowElementPanel);
			} else if (lineData.getEndFigureUUID().equals(figureData.getFlowElementUUID())) {
				manLine.setEndFigure((JecnBaseFigurePanel) flowElementPanel);
			}
		}
	}

	/**
	 * 刷新画图面板
	 * 
	 * @param workflow
	 * @param width
	 * @param height
	 */
	public static void refrashPanelSize(JecnDrawDesktopPane workflow, int width, int height) {
		workflow.setSize(width, height);
		workflow.setPreferredSize(new Dimension(width, height));
		workflow.revalidate();
	}

	/**
	 * 读取XML figures节点的数据
	 */
	public List<JecnBaseFlowElementPanel> gainFiguresDate(NodeList figures) {
		// 存储图形的List
		List<JecnBaseFlowElementPanel> baseFlowElementList = new ArrayList<JecnBaseFlowElementPanel>();
		if (figures.getLength() > 0) {
			String figureType = "";
			for (int i = 0; i < figures.getLength(); i++) {
				NamedNodeMap attributes = figures.item(i).getAttributes();
				// 图形的标识
				Node figureTypeNode = attributes.getNamedItem("FigureType");
				if (figureTypeNode != null) {
					figureType = figureTypeNode.getNodeValue();
				}

				// 判断图形的标识是否为空
				if (DrawCommon.isNullOrEmtryTrim(figureType)) {
					continue;
				}
				// 创建图形数据对象
				JecnFigureData figureData = JecnCreateFlowElement.createFigureData(MapElemType.valueOf(figureType));

				Node eleShapeNode = attributes.getNamedItem("EleShape");
				if (eleShapeNode != null) {
					if (!DrawCommon.isNullOrEmtryTrim(eleShapeNode.getNodeValue())) {
						figureData.setEleShape(MapElemType.valueOf(eleShapeNode.getNodeValue()));
					}
				}

				// 图形上添加的字符
				Node figureTextNode = attributes.getNamedItem("FigureText");
				if (figureTextNode != null) {
					String figureText = figureTextNode.getNodeValue();
					figureData.setFigureText(figureText);
				}

				// 图形的位置点
				Point point = new Point();
				// 图形的X
				double poLongx = 0;
				// 图形的X轴
				Node poLongxNode = attributes.getNamedItem("PoLongx");
				if (poLongxNode != null) {
					if (!DrawCommon.isNullOrEmtryTrim(poLongxNode.getNodeValue())) {
						poLongx = Double.valueOf(poLongxNode.getNodeValue()) < 1 ? 1 : Double.valueOf(poLongxNode
								.getNodeValue());
					}
				}
				// 图形的Y
				double poLongy = 0;
				// 图形的Y轴
				Node poLongyNode = attributes.getNamedItem("PoLongy");
				if (poLongyNode != null) {
					if (!DrawCommon.isNullOrEmtryTrim(poLongyNode.getNodeValue())) {
						poLongy = Double.valueOf(poLongyNode.getNodeValue()) < 1 ? 1 : Double.valueOf(poLongyNode
								.getNodeValue());
					}
				}
				point.setLocation(poLongx, poLongy);

				// 图形的宽度
				Node widthNode = attributes.getNamedItem("Width");
				if (widthNode != null) {
					String width = widthNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(width)) {
						figureData.setFigureSizeX(Integer.valueOf(width));
					}

				}

				Node heightNode = attributes.getNamedItem("Height");
				// 图型的高度
				if (heightNode != null) {
					String height = heightNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(height)) {
						figureData.setFigureSizeY(Integer.valueOf(height));
					}
				}

				// 图形的层级
				Node orderIndexNode = attributes.getNamedItem("OrderIndex");
				if (orderIndexNode != null) {
					String orderIndex = orderIndexNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(orderIndex)) {
						figureData.setZOrderIndex(Integer.valueOf(orderIndex));
					}
				}

				// 图形填充色效果标识
				Node backColorFlagNode = attributes.getNamedItem("BackColorFlag");
				if (backColorFlagNode != null) {
					String BackColorFlag = backColorFlagNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(BackColorFlag)) {
						if ("0".equals(BackColorFlag)) {
							figureData.setFillType(FillType.none);
						} else if ("1".equals(BackColorFlag)) {
							figureData.setFillType(FillType.one);
						} else {
							figureData.setFillType(FillType.two);
						}
						// 图形的填充颜色
						Node bgColorNode = attributes.getNamedItem("BGColor");
						if (bgColorNode != null) {
							String BGColor = bgColorNode.getNodeValue();
							if (!DrawCommon.isNullOrEmtryTrim(BGColor)) {
								String[] backColors = BGColor.split(",");
								if (backColors.length == 3) {// 单色效果
									Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer
											.parseInt(backColors[1]), Integer.parseInt(backColors[2]));
									figureData.setFillColor(newBackColor);

								} else if (backColors.length == 7) {// 双色效果+渐变方向
									Color oneBackColor = new Color(Integer.parseInt(backColors[0]), Integer
											.parseInt(backColors[1]), Integer.parseInt(backColors[2]));
									Color twoBackColor = new Color(Integer.parseInt(backColors[3]), Integer
											.parseInt(backColors[4]), Integer.parseInt(backColors[5]));
									figureData.setFillColor(oneBackColor);
									figureData.setChangeColor(twoBackColor);
									String colorChangType = backColors[6];
									if ("1".equals(colorChangType)) {
										// 向上倾斜
										figureData.setFillColorChangType(FillColorChangType.topTilt);
									} else if ("2".equals(colorChangType)) {
										// 向下倾斜
										figureData.setFillColorChangType(FillColorChangType.bottom);
									} else if ("3".equals(colorChangType)) {
										// 垂直
										figureData.setFillColorChangType(FillColorChangType.vertical);
									} else {
										// 水平
										figureData.setFillColorChangType(FillColorChangType.horizontal);
									}

								}
							}
						}
					}
				}

				// 字体设置
				fontValue(figureData, attributes);

				// 设置连接ID
				Node linkflowIdNode = attributes.getNamedItem("LinkflowId");
				if (linkflowIdNode != null) {
					String LinkflowId = linkflowIdNode.getNodeValue();
				}
				// 图形ID唯一标识
				Node figureImageIdNode = attributes.getNamedItem("FigureImageId");
				if (figureImageIdNode != null) {
					String figureImageId = figureImageIdNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(figureImageId)) {
						figureData.setFlowElementUUID(figureImageId);
					}
				}
				// 字体位置
				Node fontPositionNode = attributes.getNamedItem("FontPosition");
				if (fontPositionNode != null) {
					String fontPosition = fontPositionNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(fontPosition)) {
						figureData.setFontPosition(Integer.valueOf(fontPosition));
					}
				}
				// 是否存在阴影
				Node havaShadowNode = attributes.getNamedItem("HavaShadow");
				if (havaShadowNode != null) {
					String favaShadow = havaShadowNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(favaShadow)) {
						if ("1".equals(favaShadow)) {
							figureData.setShadowsFlag(true);
						} else {
							figureData.setShadowsFlag(false);
						}
					}
				}

				// 是否存在阴影
				Node shadowColorNode = attributes.getNamedItem("ShadowColor");
				if (shadowColorNode != null) {
					String shadowColor = shadowColorNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(shadowColor)) {
						String[] shadowColors = shadowColor.split(",");
						Color newBackColor = new Color(Integer.parseInt(shadowColors[0]), Integer
								.parseInt(shadowColors[1]), Integer.parseInt(shadowColors[2]));
						figureData.setShadowColor(newBackColor);
					}
				}

				// 旋转角度值
				Node circumgyrateNode = attributes.getNamedItem("Circumgyrate");
				if (circumgyrateNode != null) {
					String circumgyrate = circumgyrateNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(circumgyrate)) {
						figureData.setCurrentAngle(Double.valueOf(circumgyrate));
					}
				}
				// 边框颜色
				Node bodyColorNode = attributes.getNamedItem("BodyColor");
				if (bodyColorNode != null) {
					String bodyColor = bodyColorNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
						String[] backColors = bodyColor.split(",");
						Color newBackColor = new Color(Integer.parseInt(backColors[0]),
								Integer.parseInt(backColors[1]), Integer.parseInt(backColors[2]));
						figureData.setBodyColor(newBackColor);
					}
				}
				// 边框是否加粗
				Node frameBodyNode = attributes.getNamedItem("FrameBody");
				if (frameBodyNode != null) {
					String frameBody = frameBodyNode.getNodeValue();
					if (frameBody != null && !"".equals(frameBody)) {
						figureData.setBodyWidth(Integer.valueOf(frameBody));
					}
				}
				// 字体显示为横排还是竖排
				Node isErectNode = attributes.getNamedItem("IsErect");
				if (isErectNode != null) {
					String isErect = isErectNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(isErect)) {
						if ("1".equals(isErect)) {
							figureData.setFontUpRight(true);
						} else {
							figureData.setFontUpRight(false);
						}
					}
				}
				// 是否显示3D
				Node figure3DNode = attributes.getNamedItem("Figure3D");
				if (figure3DNode != null) {
					String figure3D = figure3DNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(figure3D)) {
						if ("1".equals(figure3D)) {
							figureData.setFlag3D(true);
						} else {
							figureData.setFlag3D(false);
						}
					}
				}
				// 边框的深浅
				Node lineThicknessNode = attributes.getNamedItem("LineThickness");
				if (lineThicknessNode != null) {
					String lineThickness = lineThicknessNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(lineThickness)) {
						figureData.setBodyWidth(Double.valueOf(lineThickness).intValue());
					}
				}
				// 判断是否为活动
				if (DrawCommon.isActive(MapElemType.valueOf(figureType))) {
					JecnActiveData activeData = (JecnActiveData) figureData;
					// 判断是否为关键活动
					Node activityIdNode = attributes.getNamedItem("ActivityId");
					// 活动编号
					if (activityIdNode != null) {
						String activityId = activityIdNode.getNodeValue();
						activeData.setActivityNum(activityId);
					}
					// 活动说明
					Node activityShowNode = attributes.getNamedItem("ActivityShow");
					if (activityShowNode != null) {
						String activityShow = activityShowNode.getNodeValue();
						activeData.setAvtivityShow(activityShow);
					}
					// 关键活动
					Node avtivityShowAndControlNode = attributes.getNamedItem("AvtivityShowAndControl");
					if (avtivityShowAndControlNode != null) {
						String avtivityShowAndControl = avtivityShowAndControlNode.getNodeValue();
						activeData.setAvtivityShowAndControl(avtivityShowAndControl);
					}

					// 活动输入
					Node activityInputNode = attributes.getNamedItem("AvtivityInput");
					if (activityInputNode != null) {
						String activityInput = activityInputNode.getNodeValue();
						// D1版本中 活动输入
						activeData.setActiveInput(activityInput);
						// 设计器：输入中 说明
						activeData.setActivityInput(activityInput);
					}

					// 活动输入
					Node activityoutputNode = attributes.getNamedItem("Avtivityoutput");
					if (activityoutputNode != null) {
						// D1版本中 活动输出
						String avtivityoutput = activityoutputNode.getNodeValue();
						activeData.setActiveOutPut(avtivityoutput);
						// 设计器：输入中 说明
						activeData.setActivityOutput(avtivityoutput);
					}
					// 活动办理时限-目标值
					Node targetValue = attributes.getNamedItem("TargetValue");
					if (targetValue != null && !DrawCommon.isNullOrEmtryTrim(targetValue.getNodeValue())) {
						activeData.setTargetValue(new BigDecimal(targetValue.getNodeValue()));
					}
					// 状态值
					Node statusValue = attributes.getNamedItem("StatusValue");
					if (targetValue != null && !DrawCommon.isNullOrEmtryTrim(statusValue.getNodeValue())) {
						activeData.setStatusValue(new BigDecimal(statusValue.getNodeValue()));
					}
					// 说明
					Node explain = attributes.getNamedItem("Explain");
					if (targetValue != null) {
						activeData.setExplain(explain.getNodeValue());
					}
				} else if ("CommentText".equals(figureType)) {// 如果为注释框
					JecnCommentLineData commentData = (JecnCommentLineData) figureData;
					// 获取连接线数据对象
					JecnBaseDivedingLineData lineData = new JecnBaseDivedingLineData(MapElemType.CommentLine);

					double startX = 0;
					double startY = 0;
					double endX = 0;
					double endY = 0;
					// 注释框小线段开始点
					Node getStartPosX = attributes.getNamedItem("getStartPosX");

					if (getStartPosX != null) {
						if (!DrawCommon.isNullOrEmtryTrim(getStartPosX.getNodeValue())) {
							startX = Double.valueOf(getStartPosX.getNodeValue());
						}
					}
					// 注释框小线段开始点
					Node getStartPosY = attributes.getNamedItem("getStartPosY");

					if (getStartPosY != null) {
						if (!DrawCommon.isNullOrEmtryTrim(getStartPosY.getNodeValue())) {
							startY = Double.valueOf(getStartPosY.getNodeValue());
						}
					}
					// 注释框小线段开始点
					Node getEndPosX = attributes.getNamedItem("getEndPosX");

					if (getEndPosX != null) {
						if (!DrawCommon.isNullOrEmtryTrim(getEndPosX.getNodeValue())) {
							endX = Double.valueOf(getEndPosX.getNodeValue());
						}
					}
					// 注释框小线段开始点
					Node getEndPosY = attributes.getNamedItem("getEndPosY");

					if (getEndPosY != null) {
						if (!DrawCommon.isNullOrEmtryTrim(getEndPosY.getNodeValue())) {
							endY = Double.valueOf(getEndPosY.getNodeValue());
						}
					}

					Point startPoint = new Point((int) startX, (int) startY);
					Point endPoint = new Point((int) endX, (int) endY);
					// 设置线段大小 记录线段原始状态下数值
					lineData.getDiviLineCloneable().initOriginalArributs(startPoint, endPoint,
							figureData.getBodyWidth(), getWorkflow().getWorkflowScale());

					// // 注释框小线段开始点
					// Node commentLineDragged = attributes
					// .getNamedItem("commentLineDragged");
					// if (commentLineDragged != null) {
					// if (!Tool.isNullOrEmtryTrim(commentLineDragged
					// .getNodeValue())) {
					// String isDragged = commentLineDragged
					// .getNodeValue();
					// if ("true".equals(isDragged)) {
					// commentData.setDragged(true);
					// } else {
					// commentData.setDragged(false);
					// }
					// }
					// }
					commentData.setLineData(lineData);

				} else if (DrawCommon.isRole(MapElemType.valueOf(figureType))) {// 角色

					JecnRoleData roleData = (JecnRoleData) figureData;

					// 角色名称
					Node roleNameNode = attributes.getNamedItem("roleName");
					if (roleNameNode != null) {
						roleData.setFigureText(roleNameNode.getNodeValue());
					}
					// 岗位名称
					Node posNameNode = attributes.getNamedItem("posName");
					if (posNameNode != null) {
						roleData.setPosName(posNameNode.getNodeValue());
					}
					// 角色职责
					Node roleResNode = attributes.getNamedItem("roleRes");
					if (roleResNode != null) {
						roleData.setRoleRes(roleResNode.getNodeValue());
					}
				} else if (MapElemType.ModelFigure == MapElemType.valueOf(figureType)) {// 泳池
					JecnModeFigureData modeFigureData = (JecnModeFigureData) figureData;
					// 泳池x点位置
					Node dividingXNode = attributes.getNamedItem("dividingX");
					if (dividingXNode != null) {
						String dividingStr = dividingXNode.getNodeValue();
						if (!DrawCommon.isNullOrEmtry(dividingStr)) {
							modeFigureData.setDividingX(Integer.valueOf(dividingStr));
						}
					}
				}

				// 判断是否为园图形
				// if (attributes.getNamedItem("ActivityShow") != null) {
				// String ActivityShow = attributes.getNamedItem(
				// "ActivityShow").getNodeValue();
				// }

				// 判断是否为角色,是否设置主责岗位
				Node linkMainTypeNode = attributes.getNamedItem("LinkMainType");
				if (linkMainTypeNode != null) {
					String LinkMainType = linkMainTypeNode.getNodeValue();
				}

				// 创建图形对象
				JecnBaseFigurePanel baseFigureElement = (JecnBaseFigurePanel) JecnCreateFlowElement.getFlowFigure(
						MapElemType.valueOf(figureType), figureData);
				// 图标插入框 图片ID
				Node imgIdNode = attributes.getNamedItem("imgId");
				if (imgIdNode != null) {
					String imgIdStr = imgIdNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(imgIdStr)) {
						baseFigureElement.getFlowElementData().getDesignerFigureData()
								.setLinkId(Long.valueOf(imgIdStr));
					}
				}
				if (MapElemType.CommentText == figureData.getMapElemType()) {// 判断是否为注释框
					CommentText commentText = (CommentText) baseFigureElement;
					// 默认算法 取数据层
					commentText.isDefault();
				}
				// 设置图形对象100%状态下位置、大小、编辑点等
				JecnFigureDataCloneable currFigureDataCloneable = baseFigureElement.getFlowElementData()
						.getFigureDataCloneable();
				currFigureDataCloneable.reSetAttributes(point, new Dimension(baseFigureElement.getFlowElementData()
						.getFigureSizeX(), baseFigureElement.getFlowElementData().getFigureSizeY()), 1.0);

				// 设置图形属性
				baseFigureElement.setBounds(currFigureDataCloneable.getX(), currFigureDataCloneable.getY(),
						currFigureDataCloneable.getWidth(), currFigureDataCloneable.getHeight());
				if (baseFigureElement instanceof IconFigure) { // 图标插入框
					// 判断是否为设计器
					if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {
						IconFigure iconFigure = (IconFigure) baseFigureElement;
						// 获取关联ID
						Long linkId = iconFigure.getFlowElementData().getDesignerFigureData().getLinkId();
						// 判断ID 是否为空
						if (linkId != null) {
							JecnDesignerProcess.getDesignerProcess().openFile(linkId, iconFigure);
						}
					}
				}
				// 流程地图描述
				Node mapDescNode = attributes.getNamedItem("mapDesc");
				if (mapDescNode != null) {
					String mapDescStr = mapDescNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(mapDescStr)) {
						baseFigureElement.getFlowElementData().setAvtivityShow(mapDescStr);
					}
				}
				// 把图形放入List中
				baseFlowElementList.add(baseFigureElement);
			}
		}
		return baseFlowElementList;
	}

	/**
	 * 设置字体属性
	 * 
	 * @param flowData
	 *            流程元素数据对象
	 * @param attributes
	 *            xml 流程元素节点
	 */
	public void fontValue(JecnAbstractFlowElementData flowData, NamedNodeMap attributes) {
		// 字体颜色
		Node fontColorNode = attributes.getNamedItem("FontColor");
		if (fontColorNode != null) {
			String fontColor = fontColorNode.getNodeValue();
			if (!DrawCommon.isNullOrEmtryTrim(fontColor)) {
				String[] backColors = fontColor.split(",");
				Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
						Integer.parseInt(backColors[2]));
				flowData.setFontColor(newBackColor);
			}
		}
		// 字体是否加粗0为正常，1为加粗，2为斜体
		Node fontBodyNode = attributes.getNamedItem("FontBody");
		if (fontBodyNode != null) {
			String fontBody = fontBodyNode.getNodeValue();
			if (!DrawCommon.isNullOrEmtryTrim(fontBody)) {
				flowData.setFontStyle(Integer.valueOf(fontBody));
			}
		}
		// 字体类型:默认为（宋体）
		Node fontTypeNode = attributes.getNamedItem("FontType");
		if (fontTypeNode != null) {
			String fontType = fontTypeNode.getNodeValue();
			if (!DrawCommon.isNullOrEmtryTrim(fontType)) {
				flowData.setFontName(fontType);
			}
		}
		// 字体的大小
		Node fontSizeNode = attributes.getNamedItem("FontSize");
		if (fontSizeNode != null) {
			String fontSize = fontSizeNode.getNodeValue();
			if (!DrawCommon.isNullOrEmtryTrim(fontSize)) {
				flowData.setFontSize(Integer.valueOf(fontSize));
			}
		}
		// 边框样式
		Node bodyLineNode = attributes.getNamedItem("BodyLine");
		if (bodyLineNode != null) {
			String bodyLine = bodyLineNode.getNodeValue();
			if (!DrawCommon.isNullOrEmtryTrim(bodyLine)) {
				flowData.setBodyStyle(Integer.valueOf(bodyLine));
			}
		}
	}

	public void isCommentText(JecnFigureData activeData) {

	}

	/**
	 * NodeList Lines 读取XML 线的数据
	 * 
	 * @param attributes
	 *            读取XML 数据
	 */
	public void gainConnectingLine(NodeList manLines, List<JecnBaseFlowElementPanel> baseFlowElementList,
			Map<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>> manLineList) {
		// 存储线段的List
		for (int i = 0; i < manLines.getLength(); i++) {
			NamedNodeMap attributes = manLines.item(i).getAttributes();
			// 线的标识
			String lineFlag = "";
			Node figureTypeNode = attributes.getNamedItem("FigureType");
			if (figureTypeNode != null) {
				lineFlag = figureTypeNode.getNodeValue();
			}
			// 获取连接线的数据对象
			JecnManhattanLineData manhattanLineData = new JecnManhattanLineData(MapElemType.valueOf(lineFlag));

			// 线的宽度
			Node lineSizeNode = attributes.getNamedItem("LineSize");
			if (lineSizeNode != null) {
				String lineSize = lineSizeNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(lineSize)) {
					manhattanLineData.setBodyWidth(Integer.valueOf(lineSize));
				}
			} else {
				manhattanLineData.setBodyWidth(1);
			}

			// 开始图形的ID
			Node startFigureNode = attributes.getNamedItem("StartFigure");
			if (startFigureNode != null) {
				String startFigureId = startFigureNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startFigureId)) {
					manhattanLineData.setStartFigureUUID(startFigureId);
				}
			}
			// 结束图形的ID
			Node endFigureNode = attributes.getNamedItem("EndFigure");
			if (endFigureNode != null) {
				String endFigureId = endFigureNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endFigureId)) {
					manhattanLineData.setEndFigureUUID(endFigureId);
				}
			}
			// 获取开始点的边界点
			Node poLongxNode = attributes.getNamedItem("PoLongx");
			if (poLongxNode != null) {
				String poLongx = poLongxNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(poLongx)) {
					EditPointType editPointType = DrawCommon.getLineType(poLongx);
					manhattanLineData.setStartEditPointType(editPointType);
				}
			}
			// 获取结束点的边界点
			Node poLongyNode = attributes.getNamedItem("PoLongy");
			if (poLongyNode != null) {
				String poLongy = poLongyNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(poLongy)) {
					EditPointType editPointType = DrawCommon.getLineType(poLongy);
					manhattanLineData.setEndEditPointType(editPointType);
				}
			}
			// 边框颜色
			Node lineColorNode = attributes.getNamedItem("LineColor");
			if (lineColorNode != null) {
				String bodyColor = lineColorNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
					String[] backColors = bodyColor.split(",");
					Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
							Integer.parseInt(backColors[2]));
					manhattanLineData.setBodyColor(newBackColor);
				}
			}
			// 给线段赋值
			Node lineTextNode = attributes.getNamedItem("LineText");
			if (lineTextNode != null) {
				String lineText = lineTextNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(lineText)) {
					manhattanLineData.setFigureText(lineText);
				}
			}

			// 获取层级
			Node orderIndexNode = attributes.getNamedItem("OrderIndex");
			if (orderIndexNode != null) {
				String orderIndex = orderIndexNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(orderIndex)) {
					manhattanLineData.setZOrderIndex(Integer.valueOf(orderIndex));
				}
			}

			// 是否显示双箭头
			Node twoArrowLineNode = attributes.getNamedItem("twoArrowLine");
			if (twoArrowLineNode != null) {
				String twoArrow = twoArrowLineNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(twoArrow)) {
					manhattanLineData.setTwoArrow(Boolean.valueOf(twoArrow));
				}
			}

			// 连接线字段位置
			Node textPointNode = attributes.getNamedItem("textPoint");
			if (textPointNode != null) {
				String textPoint = textPointNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(textPoint)) {
					String[] str = textPoint.split(",");
					manhattanLineData.setTextPoint(new Point(Integer.valueOf(str[0]), Integer.valueOf(str[1])));
				}
			}

			// //适应visio导出添加UUID
			manhattanLineData.setFlowElementUUID(DrawCommon.getUUID());

			// 字体设置
			fontValue(manhattanLineData, attributes);

			// 根据标识位获取连接线对象
			JecnBaseManhattanLinePanel baseManhattanLine = JecnPaintFigureUnit.getJecnBaseManhattanLinePanel(lineFlag,
					manhattanLineData);

			// 设置连接线的开始图形和结束图形
			setManLineRefFigure(baseFlowElementList, baseManhattanLine);

			// 获取小线段
			List<JecnManhattanRouterResultData> resultDataList = getManhattanRouterResultData((Element) manLines
					.item(i), baseManhattanLine);
			manLineList.put(baseManhattanLine, resultDataList);
		}
	}

	/**
	 * 获取线段下的小线段的集合
	 * 
	 * @param line
	 * @return
	 */
	public List<JecnManhattanRouterResultData> getManhattanRouterResultData(Element line,
			JecnBaseManhattanLinePanel linePanel) {
		NodeList lineSegments = line.getChildNodes();
		List<JecnManhattanRouterResultData> resultDatas = new ArrayList<JecnManhattanRouterResultData>();
		for (int k = 0; k < lineSegments.getLength(); k++) {
			NamedNodeMap lineSegmentAttributes = lineSegments.item(k).getAttributes();
			if (lineSegmentAttributes == null) {
				continue;
			}
			// 小线段开始点的X
			double startX = 0;
			Node startPtXNode = lineSegmentAttributes.getNamedItem("StartPtX");
			if (startPtXNode != null) {
				String startXstr = startPtXNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startXstr)) {
					startX = Double.valueOf(startXstr) < 1 ? 1 : Double.valueOf(startXstr);
				}
			}
			// 小线段结束点的Y
			double startY = 0;
			Node startPtYNode = lineSegmentAttributes.getNamedItem("StartPtY");
			if (startPtYNode != null) {
				String startYstr = startPtYNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startYstr)) {
					startY = Double.valueOf(startYstr) < 1 ? 1 : Double.valueOf(startYstr);
				}
			}

			// 小线段结束点的X
			double endX = 0;
			Node endPtXNode = lineSegmentAttributes.getNamedItem("EndPtX");
			if (endPtXNode != null) {
				String endXstr = endPtXNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endXstr)) {
					endX = Double.valueOf(endXstr) < 1 ? 1 : Double.valueOf(endXstr);
				}
			}
			// 小线段结束点的Y
			double endY = 0;
			Node endPtYNode = lineSegmentAttributes.getNamedItem("EndPtY");
			if (endPtYNode != null) {
				String endYstr = endPtYNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endYstr)) {
					endY = Double.valueOf(endYstr) < 1 ? 1 : Double.valueOf(endYstr);
				}
			}
			Point startPoint = new Point();
			startPoint.setLocation(startX, startY);

			Point endPoint = new Point();
			endPoint.setLocation(endX, endY);

			// 添加小线段到线段集合
			JecnManhattanRouterResultData data = new JecnManhattanRouterResultData();
			data.setStartPoint(startPoint);
			data.setEndPint(endPoint);
			resultDatas.add(data);
		}
		return resultDatas;
	}

	/**
	 * 获取横分割线
	 */
	private void gainParallelLineData(Document doc) {
		// 横分割线
		NodeList parallelLines = doc.getElementsByTagName("ParallelLines");
		for (int i = 0; i < parallelLines.getLength(); i++) {
			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.ParallelLines, LineDirectionEnum.horizontal);
			NamedNodeMap attributes = parallelLines.item(i).getAttributes();
			double poLongy = 0;
			Node poLongyNode = attributes.getNamedItem("PoLongy");
			if (poLongyNode != null) {
				String poLongyStr = poLongyNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(poLongyStr)) {
					poLongy = Double.valueOf(poLongyStr) < 1 ? 1 : Double.valueOf(poLongyStr);
				}
			}

			// 获取分割线层级
			int orderIndex = 0;
			Node orderIndexNode = attributes.getNamedItem("OrderIndex");
			if (orderIndexNode != null) {
				String orderIndexStr = orderIndexNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(orderIndexStr)) {
					orderIndex = Integer.valueOf(orderIndexStr);
				}
			}
			vhLineData.setZOrderIndex(orderIndex);
			// 线条类型
			int lineSize = 0;
			Node lineSizeNode = attributes.getNamedItem("LineSize");
			if (lineSizeNode != null) {
				String lineSizeStr = lineSizeNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(lineSizeStr)) {
					lineSize = Integer.valueOf(lineSizeStr);
				}
			}
			vhLineData.setBodyWidth(lineSize);

			// 分割线颜色
			Node bodyColorNode = attributes.getNamedItem("BodyColor");
			if (bodyColorNode != null) {
				String bodyColor = bodyColorNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
					String[] backColors = bodyColor.split(",");
					Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
							Integer.parseInt(backColors[2]));
					vhLineData.setBodyColor(newBackColor);
				}
			}

			// 边框样式
			Node bodyLineNode = attributes.getNamedItem("BodyLine");
			if (bodyLineNode != null) {
				String bodyLine = bodyLineNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(bodyLine)) {
					vhLineData.setBodyStyle(Integer.valueOf(bodyLine));
				}
			}
			ParallelLines parallelLine = new ParallelLines(vhLineData);
			JecnAddFlowElementUnit.addVHLine(parallelLine, new Point(0, (int) poLongy));
			parallelLine.reSetFlowElementZorder();
			// 面板最大层级数加一
			getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
		}

	}

	/**
	 * 获取竖分割线
	 */
	private void gainVerticalLineData(Document doc) {
		// 竖分割线
		NodeList verticalLines = doc.getElementsByTagName("VerticalLines");
		for (int i = 0; i < verticalLines.getLength(); i++) {
			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.VerticalLine, LineDirectionEnum.vertical);
			NamedNodeMap attributes = verticalLines.item(i).getAttributes();
			double poLongx = 0;
			Node poLongxNode = attributes.getNamedItem("PoLongx");
			if (poLongxNode != null) {
				String poLongxStr = poLongxNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(poLongxStr)) {
					poLongx = Double.valueOf(poLongxStr) < 1 ? 1 : Double.valueOf(poLongxStr);
				}
			}
			// 获取分割线层级
			int orderIndex = 0;
			Node orderIndexNode = attributes.getNamedItem("OrderIndex");
			if (orderIndexNode != null) {
				String orderIndexStr = orderIndexNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(orderIndexStr)) {
					orderIndex = Integer.valueOf(orderIndexStr);
				}
			}
			vhLineData.setZOrderIndex(orderIndex);

			// 分割线颜色
			Node bodyColorNode = attributes.getNamedItem("BodyColor");
			if (bodyColorNode != null) {
				String bodyColor = bodyColorNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
					String[] backColors = bodyColor.split(",");
					Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
							Integer.parseInt(backColors[2]));
					vhLineData.setBodyColor(newBackColor);
				}
			}

			// 线条类型
			int lineSize = 0;
			Node lineSizeNode = attributes.getNamedItem("LineSize");
			if (lineSizeNode != null) {
				String lineSizeStr = lineSizeNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(lineSizeStr)) {
					lineSize = Integer.valueOf(lineSizeStr);
				}
			}
			vhLineData.setBodyWidth(lineSize);

			// 边框样式
			Node bodyLineNode = attributes.getNamedItem("BodyLine");
			if (bodyLineNode != null) {
				String bodyLine = bodyLineNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(bodyLine)) {
					vhLineData.setBodyStyle(Integer.valueOf(bodyLine));
				}
			}

			VerticalLine verticalLine = new VerticalLine(vhLineData);

			JecnAddFlowElementUnit.addVHLine(verticalLine, new Point((int) poLongx, 0));
			verticalLine.reSetFlowElementZorder();
			// 面板最大层级数加一
			getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
		}

	}

	/**
	 * 获取待添加的分割线
	 * 
	 * @param baseDivedingLine
	 *            分割线节点
	 * @return List<JecnBaseDividingLinePanel>添加的分割线集合
	 */
	public void getBaseDivedingLineData(NodeList baseDivedingLine, List<JecnBaseFlowElementPanel> baseFlowElementList) {
		// 存储线段的List
		for (int i = 0; i < baseDivedingLine.getLength(); i++) {
			JecnBaseDivedingLineData baseDivedingLineData = null;
			NamedNodeMap attributes = baseDivedingLine.item(i).getAttributes();
			if (attributes == null) {
				continue;
			}
			// 线条的标识
			String figureType = null;
			Node figureTypeNode = attributes.getNamedItem("FigureType");
			if (figureTypeNode != null) {
				figureType = figureTypeNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(figureType)) {
					baseDivedingLineData = new JecnBaseDivedingLineData(MapElemType.valueOf(figureType));
				}
			}
			if (baseDivedingLineData == null) {
				return;
			}
			// 开始点X
			int startX = 0;
			Node startXNode = attributes.getNamedItem("StartX");
			if (startXNode != null) {
				String startXStr = startXNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startXStr)) {
					startX = Integer.valueOf(startXStr) < 1 ? 1 : Integer.valueOf(startXStr);
				}
			}
			int startY = 0;
			Node startYNode = attributes.getNamedItem("StartY");
			if (startYNode != null) {
				String startYStr = startYNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startYStr)) {
					startY = Integer.valueOf(startYStr) < 1 ? 1 : Integer.valueOf(startYStr);
				}
			}

			// 结束点
			int endX = 0;
			Node endXNode = attributes.getNamedItem("EndX");
			if (endXNode != null) {
				String endXStr = endXNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endXStr)) {
					endX = Integer.valueOf(endXStr) < 1 ? 1 : Integer.valueOf(endXStr);
				}
			}
			int endY = 0;
			Node endYNode = attributes.getNamedItem("EndY");
			if (endYNode != null) {
				String endYStr = endYNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endYStr)) {
					endY = Integer.valueOf(endYStr) < 1 ? 1 : Integer.valueOf(endYStr);
				}
			}
			// 线条颜色
			Node lineColorNode = attributes.getNamedItem("LineColor");
			if (lineColorNode != null) {
				String lineColor = lineColorNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(lineColor)) {
					String[] backColors = lineColor.split(",");
					Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
							Integer.parseInt(backColors[2]));
					baseDivedingLineData.setBodyColor(newBackColor);
				}
			}

			// 线条大小
			int lineSize = 0;
			Node lineSizeNode = attributes.getNamedItem("LineSize");
			if (lineSizeNode != null) {
				String lineSizeStr = lineSizeNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(lineSizeStr)) {
					lineSize = Integer.valueOf(lineSizeStr);
				}
			}
			baseDivedingLineData.setBodyWidth(lineSize);

			// 边框样式
			Node bodyLineNode = attributes.getNamedItem("BodyLine");
			if (bodyLineNode != null) {
				String bodyLine = bodyLineNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(bodyLine)) {
					baseDivedingLineData.setBodyStyle(Integer.valueOf(bodyLine));
				}
			}

			// 线的层级
			int orderIndex = 0;
			Node orderIndexNode = attributes.getNamedItem("OrderIndex");
			if (orderIndexNode != null) {
				String orderIndexStr = orderIndexNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(orderIndexStr)) {
					orderIndex = Integer.valueOf(orderIndexStr);
					baseDivedingLineData.setZOrderIndex(orderIndex);
				}
			}
			if (MapElemType.HDividingLine == baseDivedingLineData.getMapElemType()) {// 如果是横线，纵坐标为线宽
				// 图形的宽度
				baseDivedingLineData.setFigureSizeX(Math.abs(startX - endX));
				// 图形的高度
				baseDivedingLineData.setFigureSizeY(lineSize);
			} else if (MapElemType.VDividingLine == baseDivedingLineData.getMapElemType()) {// 如果是纵线，横坐标为线宽
				// 图形的宽度
				baseDivedingLineData.setFigureSizeX(lineSize);
				// 图形的高度
				baseDivedingLineData.setFigureSizeY(Math.abs(startY - endY));
			}

			JecnBaseDividingLinePanel dividingLinePanel = getJecnBaseDiviLinePanel(baseDivedingLineData);

			Point startPoint = new Point(startX, startY);
			Point endPoint = new Point(endX, endY);

			// 记录100%状态下位置点 (斜线、横线、竖线) 模型配置文件记录的数据为1.0状态下的数据
			dividingLinePanel.getFlowElementData().getDiviLineCloneable().reSetAttributes(startPoint, endPoint,
					dividingLinePanel.getFlowElementData().getBodyWidth(), 1.0);
			if (dividingLinePanel instanceof HDividingLine || dividingLinePanel instanceof VDividingLine) {// 横线或竖线设置位置点
				// 设置图形位置点
				dividingLinePanel.setBounds(startPoint, endPoint);
			}
			baseFlowElementList.add(dividingLinePanel);
		}
	}

	/**
	 * 根据标识位获取连接线对象
	 * 
	 * @param lineFlag
	 * @param manhattanLineData
	 * @return
	 */
	private JecnBaseDividingLinePanel getJecnBaseDiviLinePanel(JecnBaseDivedingLineData diviLineData) {
		switch (diviLineData.getMapElemType()) {
		case DividingLine:// 分隔符（分割线）
			return new DividingLine(diviLineData);
		case HDividingLine:// 分隔符（分割线）
			return new HDividingLine(diviLineData);
		case VDividingLine:// 分隔符（分割线）
			return new VDividingLine(diviLineData);
		}
		return null;
	}

	/**
	 * 获取面板
	 * 
	 * @return 当前面板
	 */
	private JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}

	/**
	 * 创建流程地图
	 * 
	 * @param totalMapName
	 *            流程地图名称
	 */
	private void createTotalMap(String totalMapName) {
		JecnFlowMapData totalMapData = new JecnFlowMapData(MapType.totalMap);
		totalMapData.setName(totalMapName);
		JecnDrawMainPanel.getMainPanel().addWolkflow(totalMapData);
	}

	/**
	 * 创建流程图
	 * 
	 * @param mapName
	 *            流程图名称
	 * @param isHFlag
	 *            横向纵向标识
	 */
	private void createPartMap(String mapName, boolean isHFlag) {
		JecnFlowMapData partMapData = new JecnFlowMapData(MapType.partMap);
		// 名称
		partMapData.setName(mapName);
		// 横竖标识
		partMapData.setHFlag(isHFlag);

		JecnDrawMainPanel.getMainPanel().addWolkflow(partMapData);
	}
}
