package com.jecn.epros.server.webBean.standard;

import java.util.List;

import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.rule.RuleInfoBean;
/**
 * @author yxw 2013-2-28
 * @description：标准相关流程
 */
public class StandardDetailBean {
	
	private  StandardWebBean standardWebBean;
	/** 岗位查阅权限 */
	private List<JecnTreeBean> listPos;
	/** 岗位组查阅权限 */
	private List<JecnTreeBean> listPosGroup;
	/** 部门查阅权限 */
	private List<JecnTreeBean> listOrg;
	/** 密级 */
	private long secretLevel ;
	/**文件bean*/
	private FileWebBean fileBean;
	
	/**相关流程*/
	private List<ProcessWebBean> listProcessBean;
	/**相关制度*/
	private List<RuleInfoBean>  listRule;
	public FileWebBean getFileBean() {
		return fileBean;
	}

	public List<JecnTreeBean> getListPosGroup() {
		return listPosGroup;
	}

	public void setListPosGroup(List<JecnTreeBean> listPosGroup) {
		this.listPosGroup = listPosGroup;
	}

	public void setFileBean(FileWebBean fileBean) {
		this.fileBean = fileBean;
	}

	public List<ProcessWebBean> getListProcessBean() {
		return listProcessBean;
	}

	public void setListProcessBean(List<ProcessWebBean> listProcessBean) {
		this.listProcessBean = listProcessBean;
	}

	public List<JecnTreeBean> getListPos() {
		return listPos;
	}

	public void setListPos(List<JecnTreeBean> listPos) {
		this.listPos = listPos;
	}

	public List<JecnTreeBean> getListOrg() {
		return listOrg;
	}

	public void setListOrg(List<JecnTreeBean> listOrg) {
		this.listOrg = listOrg;
	}

	public long getSecretLevel() {
		return secretLevel;
	}

	public void setSecretLevel(long secretLevel) {
		this.secretLevel = secretLevel;
	}

	public List<RuleInfoBean> getListRule() {
		return listRule;
	}

	public void setListRule(List<RuleInfoBean> listRule) {
		this.listRule = listRule;
	}

	public StandardWebBean getStandardWebBean() {
		return standardWebBean;
	}

	public void setStandardWebBean(StandardWebBean standardWebBean) {
		this.standardWebBean = standardWebBean;
	}
}
