package com.jecn.epros.server.service.process.history;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;

/**
 * 流程架构公用表
 * 
 * @author zhr
 * 
 */
public class JecnCommonProcessAndMapPublicHistory {

	/**
	 * 发布流程地图(架构)元素附件表 JECN_FIGURE_FILE_H 流程架构公用
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFigureFileHistorySql(Long processId, Long historyId) {
		/* 流程架构公用表 */
		return "insert into JECN_FIGURE_FILE_H" + "(ID, FIGURE_ID, FILE_ID, FIGURE_TYPE,HISTORY_ID,GUID)"
				+ "select FI.ID, FI.FIGURE_ID, JF.VERSION_ID, FI.FIGURE_TYPE," + historyId + " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + " from JECN_FIGURE_FILE_T  FI   "
				+ "inner join jecn_file_t JF on  JF.FILE_ID = FI.FILE_ID"
				+ " where FI.FIGURE_ID IN (SELECT T.FIGURE_ID FROM jecn_flow_structure_image_t T WHERE T.FLOW_ID="
				+ processId + ")";
	}

	/**
	 * 添加线的线段的数据 流程线段从表 JECN_LINE_SEGMENT_H 流程架构公用
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnLineSegmentHistorySql(Long processId, Long historyId) {
		return "INSERT INTO JECN_LINE_SEGMENT_H(ID,FIGURE_ID,START_X,START_Y,END_X,END_Y,HISTORY_ID,GUID) "
				+ "       SELECT JLST.ID,JLST.FIGURE_ID,JLST.START_X,JLST.START_Y,JLST.END_X,JLST.END_Y," + historyId
				+ " HISTORY_ID," + JecnCommonSql.getSqlGUID()
				+ "       FROM JECN_LINE_SEGMENT_T JLST,JECN_FLOW_STRUCTURE_IMAGE_T JFSIT"
				+ "       WHERE JLST.FIGURE_ID=JFSIT.FIGURE_ID AND JFSIT.FLOW_ID=" + processId;
	}

	/**
	 * 添加图形数据 流程元素表 JECN_FLOW_STRUCTURE_IMAGE_H 流程架构公用
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowStructureImage(Long processId, Long historyId) {
		return "insert into JECN_FLOW_STRUCTURE_IMAGE_H "
				+ "(FIGURE_ID, FLOW_ID, FIGURE_TYPE, FIGURE_TEXT, "
				+ "START_FIGURE, END_FIGURE, X_POINT, Y_POINT, WIDTH, "
				+ "HEIGHT, FONT_SIZE, BGCOLOR, FONTCOLOR, LINK_FLOW_ID, "
				+ "LINECOLOR, FONTPOSITION, HAVESHADOW, CIRCUMGYRATE,"
				+ " BODYLINE, BODYCOLOR, ISERECT, FIGURE_IMAGE_ID, "
				+ "Z_ORDER_INDEX, ACTIVITY_SHOW, ACTIVITY_ID, FONT_BODY,"
				+ " FRAME_BODY, FONT_TYPE, ROLE_RES, IS_3D_EFFECT, "
				+ "FILL_EFFECTS, LINE_THICKNESS, START_TIME, STOP_TIME,"
				+ " TIME_TYPE, SHADOW_COLOR,INNER_CONTROL_RISK,STANDARD_CONDITIONS,"
				+ "ACTIVE_TYPE_ID,ISONLINE,DIVIDING_X,ACTIVITY_INPUT,ACTIVITY_OUTPUT,"
				+ "TARGET_VALUE,STATUS_VALUE,EXPLAIN,IMPL_TYPE,IMPL_NAME,IMPL_NOTE,PROCESS_REQUIREMENTS,CUSTOM_ONE,ELE_SHAPE,HISTORY_ID,GUID)"
				+ " SELECT IT.FIGURE_ID, IT.FLOW_ID, IT.FIGURE_TYPE, IT.FIGURE_TEXT, IT.START_FIGURE,IT.END_FIGURE, IT.X_POINT,"
				+ "IT.Y_POINT, IT.WIDTH, IT.HEIGHT, IT.FONT_SIZE, IT.BGCOLOR, IT.FONTCOLOR,"
				+ "CASE WHEN JF.VERSION_ID IS NULL THEN IT.LINK_FLOW_ID ELSE JF.VERSION_ID END AS LINK_FLOW_ID"
				+ " , IT.LINECOLOR, IT.FONTPOSITION, IT.HAVESHADOW,"
				+ "         IT.CIRCUMGYRATE, IT.BODYLINE, IT.BODYCOLOR, IT.ISERECT,"
				+ "         IT.FIGURE_IMAGE_ID, IT.Z_ORDER_INDEX, IT.ACTIVITY_SHOW,"
				+ "         IT.ACTIVITY_ID, IT.FONT_BODY, IT.FRAME_BODY, IT.FONT_TYPE,"
				+ "         IT.ROLE_RES, IT.IS_3D_EFFECT, IT.FILL_EFFECTS, IT.LINE_THICKNESS,"
				+ "         IT.START_TIME, IT.STOP_TIME, IT.TIME_TYPE, IT.SHADOW_COLOR ,"
				+ "         IT.INNER_CONTROL_RISK,IT.STANDARD_CONDITIONS ,IT.ACTIVE_TYPE_ID,IT.ISONLINE,IT.DIVIDING_X,IT.ACTIVITY_INPUT,IT.ACTIVITY_OUTPUT,IT.TARGET_VALUE,"
				+ "         IT.STATUS_VALUE,IT.EXPLAIN,IT.IMPL_TYPE,IT.IMPL_NAME,IT.IMPL_NOTE,IT.PROCESS_REQUIREMENTS,CUSTOM_ONE,ELE_SHAPE,"
				+ historyId
				+ " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID()
				+ "   FROM JECN_FLOW_STRUCTURE_IMAGE_T IT LEFT JOIN JECN_FILE_T JF ON IT.LINK_FLOW_ID = JF.FILE_ID AND JF.DEL_STATE = 0"
				+ "AND IT.FIGURE_TYPE = " + JecnCommonSql.getIconString() + " WHERE IT.FLOW_ID=" + processId;
	}

	/**
	 * JECN_AUTHORITY_HISTORY 权限表 岗位组查阅权限
	 * 
	 * @return ACCESS_TYPE : 2 岗位组
	 */
	private static String getJecnAuthorityGroupHistory(Long processId, Long historyId) {
		return "insert into JECN_AUTHORITY_HISTORY" + " (ID,RELATE_ID, TYPE,ACCESS_ID,ACCESS_TYPE,HISTORY_ID)"
				+ " select " + JecnCommonSql.getSqlGUID() + ", RELATE_ID,TYPE,POSTGROUP_ID, 2  ACCESS_TYPE,"
				+ historyId + " HISTORY_ID " + " from JECN_GROUP_PERMISSIONS_T where type = 0 and RELATE_ID="
				+ processId;
	}

	/**
	 * JECN_AUTHORITY_HISTORY 权限表 部门查阅权限
	 * 
	 * @return ACCESS_TYPE : 0 部门
	 */
	private static String getJecnAuthorityOrgHistory(Long processId, Long historyId) {
		return "insert into JECN_AUTHORITY_HISTORY" + " (ID,RELATE_ID, TYPE,ACCESS_ID,ACCESS_TYPE,HISTORY_ID)"
				+ " select " + JecnCommonSql.getSqlGUID() + ",  RELATE_ID, TYPE,ORG_ID, 0  ACCESS_TYPE ," + historyId
				+ " HISTORY_ID" + "  from JECN_ORG_ACCESS_PERM_T where type=0 and RELATE_ID=" + processId;

	}

	/**
	 * JECN_AUTHORITY_HISTORY 权限表 岗位查阅权限
	 * 
	 * @return ACCESS_TYPE : 1 岗位
	 */
	private static String getJecnAuthorityAccessHistory(Long processId, Long historyId) {
		return "insert into JECN_AUTHORITY_HISTORY" + " (ID,RELATE_ID, TYPE,ACCESS_ID,ACCESS_TYPE,HISTORY_ID)"
				+ "  select " + JecnCommonSql.getSqlGUID() + ",RELATE_ID, TYPE,FIGURE_ID, 1  ACCESS_TYPE, " + historyId
				+ " HISTORY_ID" + "   from JECN_ACCESS_PERMISSIONS_T where type=0 and RELATE_ID=" + processId;
	}

	/**
	 * 记录流程对象文控 JECN_FLOW_STRUCTURE_H 流程(图/地图)主表 流程制度公用表
	 * 
	 * @param relateId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowStructureHistorySql(Long processId, Long historyId) {
		return "insert into JECN_FLOW_STRUCTURE_H "
				+ "(FLOW_ID, PROJECTID, FLOW_NAME, ISFLOW, PRE_FLOW_ID, FLOW_LEVEL, "
				+ "STR_WIDTH, STR_HEIGHT, FLOW_ID_INPUT, IS_PUBLIC, SORT_ID, "
				+ "DEL_STATE, PEOPLE_ID, CREATE_DATE, UPDATE_PEOPLE_ID, UPDATE_DATE, "
				+ "FILE_PATH, HISTORY_ID, ERROR_STATE, DATA_TYPE, OCCUPIER, T_PATH, "
				+ "T_LEVEL,PUB_TIME,VIEW_SORT,NODE_TYPE,FILE_CONTENT_ID,CONFIDENTIALITY_LEVEL,GUID,BUSS_TYPE, COMMISSIONER_ID, KEYWORD)"
				+ "SELECT FLOW_ID, PROJECTID, FLOW_NAME, ISFLOW, PRE_FLOW_ID, FLOW_LEVEL, "
				+ "STR_WIDTH, STR_HEIGHT, FLOW_ID_INPUT, IS_PUBLIC, SORT_ID, DEL_STATE,"
				+ " PEOPLE_ID, CREATE_DATE, UPDATE_PEOPLE_ID, UPDATE_DATE, FILE_PATH, "+historyId+" HISTORY_ID,"
				+ " ERROR_STATE, DATA_TYPE, OCCUPIER, T_PATH, T_LEVEL,PUB_TIME,VIEW_SORT,NODE_TYPE,"
				+ "FILE_CONTENT_ID,CONFIDENTIALITY_LEVEL," + JecnCommonSql.getSqlGUID()
				+ ",BUSS_TYPE, COMMISSIONER_ID, KEYWORD FROM JECN_FLOW_STRUCTURE_T WHERE FLOW_ID=" + processId;
	}

	/**
	 * 流程图与制度关联表 FLOW_RELATED_CRITERION_H 流程架构公用表 添加相关制度
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getFlowRelatedCriterionHistorySql(Long processId, Long historyId) {
		return "insert into FLOW_RELATED_CRITERION_H (FLOW_CRITERON_ID, FLOW_ID, CRITERION_CLASS_ID,HISTORY_ID,GUID)"
				+ "SELECT FLOW_CRITERON_ID, FLOW_ID, CRITERION_CLASS_ID, " + historyId + " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID() + "  FROM FLOW_RELATED_CRITERION_T WHERE FLOW_ID=" + processId;
	}

	/**
	 * 责任人部门 JECN_FLOW_RELATED_ORG_H 流程架构公用表
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getJecnFlowRelatedOrgHistorySql(Long processId, Long historyId) {
		return "insert into JECN_FLOW_RELATED_ORG_H (FLOW_ORG_ID, FLOW_ID, ORG_ID,HISTORY_ID,GUID) "
				+ "SELECT FLOW_ORG_ID, FLOW_ID, ORG_ID," + historyId + "   HISTORY_ID," + JecnCommonSql.getSqlGUID()
				+ "  FROM JECN_FLOW_RELATED_ORG_T WHERE FLOW_ID=" + processId;
	}

	/**
	 * 流程相关标准化文件 FLOW_STANDARDIZED_FILE_H 流程架构公用表
	 * 
	 * @param processId
	 * @param historyId
	 * @return
	 */
	private static String getFlowStandardizedFile(Long processId, Long historyId) {
		return " INSERT INTO FLOW_STANDARDIZED_FILE_H (ID,FLOW_ID,FILE_ID,HISTORY_ID,GUID) "
				+ "SELECT SF.ID,SF.FLOW_ID,JF.VERSION_ID,"
				+ historyId
				+ " HISTORY_ID,"
				+ JecnCommonSql.getSqlGUID()
				+ " FROM FLOW_STANDARDIZED_FILE_T SF inner join JECN_FILE_T JF ON JF.FILE_ID = SF.FILE_ID WHERE SF.FLOW_ID = "
				+ processId;
	}

	/**
	 * 流程（架构）公用表
	 * 
	 * @param ruleId
	 * @param historyId
	 * @param baseDao
	 */
	public static void recordProcessMapPublicHistory(Long processId, Long historyId, IBaseDao baseDao) {
		baseDao.execteNative(getJecnFigureFileHistorySql(processId, historyId));// 发布流程地图(架构)元素附件表
		baseDao.execteNative(getJecnLineSegmentHistorySql(processId, historyId));// 添加线的线段的数据
																					// 流程线段从表
		baseDao.execteNative(getJecnFlowStructureImage(processId, historyId));// 添加图形数据
																				// 流程元素表
		baseDao.execteNative(getJecnFlowStructureHistorySql(processId, historyId));// 记录流程对象文控
																					// 流程(图/地图)主表
		baseDao.execteNative(getFlowRelatedCriterionHistorySql(processId, historyId));// 流程图与制度关联表
		baseDao.execteNative(getJecnFlowRelatedOrgHistorySql(processId, historyId));// 责任人部门
		baseDao.execteNative(getFlowStandardizedFile(processId, historyId));// 流程相关标准化文件
		// ---权限
		baseDao.execteNative(getJecnAuthorityGroupHistory(processId, historyId));// 岗位组
		baseDao.execteNative(getJecnAuthorityOrgHistory(processId, historyId));// 部门
		baseDao.execteNative(getJecnAuthorityAccessHistory(processId, historyId));// 岗位

	}

}
