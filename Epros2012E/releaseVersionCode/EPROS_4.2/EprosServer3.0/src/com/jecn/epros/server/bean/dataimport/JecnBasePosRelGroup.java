package com.jecn.epros.server.bean.dataimport;

public class JecnBasePosRelGroup {
    /**
     * 主键ID
     * */
	private String id;
	/**
	 * 岗位组ID
	 * */
	private String groupId;
	/**
	 * 基准岗位ID
	 * */
	private String baseId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getBaseId() {
		return baseId;
	}
	public void setBaseId(String baseId) {
		this.baseId = baseId;
	}
	
}
