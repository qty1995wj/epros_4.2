package com.jecn.epros.domain.org;

import java.util.List;

public class PosBaseInfoBean {
    // 岗位基础信息
    private PosBaseCommonBean baseInfo;

    private List<PosRelatedBean> superPositions;// 上级岗位
    private List<PosRelatedBean> subPositions;// 上级岗位
    private List<PosRelatedBean> innerRules;// 部门内相关制度
    private List<PosRelatedBean> outerRules;// 部门内相关制度
    private List<PosRelatedRole> roles;// 角色职责

    public PosBaseCommonBean getBaseInfo() {
        return baseInfo;
    }

    public void setBaseInfo(PosBaseCommonBean baseInfo) {
        this.baseInfo = baseInfo;
    }

    public List<PosRelatedBean> getSuperPositions() {
        return superPositions;
    }

    public void setSuperPositions(List<PosRelatedBean> superPositions) {
        this.superPositions = superPositions;
    }

    public List<PosRelatedBean> getSubPositions() {
        return subPositions;
    }

    public void setSubPositions(List<PosRelatedBean> subPositions) {
        this.subPositions = subPositions;
    }

    public List<PosRelatedBean> getInnerRules() {
        return innerRules;
    }

    public void setInnerRules(List<PosRelatedBean> innerRules) {
        this.innerRules = innerRules;
    }

    public List<PosRelatedBean> getOuterRules() {
        return outerRules;
    }

    public void setOuterRules(List<PosRelatedBean> outerRules) {
        this.outerRules = outerRules;
    }

    public List<PosRelatedRole> getRoles() {
        return roles;
    }

    public void setRoles(List<PosRelatedRole> roles) {
        this.roles = roles;
    }
}
