package com.jecn.epros.server.webBean.rule;

import java.io.Serializable;
import java.util.List;

public class RuleSystemListBean implements Serializable{
	// 总的级别数
	private int levelTotal;
	// 制度清单数据
	private List<RuleInfoBean> listRuleInfoBean;
	/** 制度每个级别的个数 0、1、2.....（统计） */
	private List<Integer> listInt;
	/** 制度个数 */
	private int ruleTotal;
	/** 待建制度个数 */
	private int noPubRuleTotal;
	/** 审批制度个数 */
	private int approveRuleTotal;
	/** 发布制度个数 */
	private int pubRuleTotal;

	public int getLevelTotal() {
		return levelTotal;
	}

	public void setLevelTotal(int levelTotal) {
		this.levelTotal = levelTotal;
	}

	public List<RuleInfoBean> getListRuleInfoBean() {
		return listRuleInfoBean;
	}

	public void setListRuleInfoBean(List<RuleInfoBean> listRuleInfoBean) {
		this.listRuleInfoBean = listRuleInfoBean;
	}

	public List<Integer> getListInt() {
		return listInt;
	}

	public void setListInt(List<Integer> listInt) {
		this.listInt = listInt;
	}

	public int getRuleTotal() {
		return ruleTotal;
	}

	public void setRuleTotal(int ruleTotal) {
		this.ruleTotal = ruleTotal;
	}

	public int getNoPubRuleTotal() {
		return noPubRuleTotal;
	}

	public void setNoPubRuleTotal(int noPubRuleTotal) {
		this.noPubRuleTotal = noPubRuleTotal;
	}

	public int getApproveRuleTotal() {
		return approveRuleTotal;
	}

	public void setApproveRuleTotal(int approveRuleTotal) {
		this.approveRuleTotal = approveRuleTotal;
	}

	public int getPubRuleTotal() {
		return pubRuleTotal;
	}

	public void setPubRuleTotal(int pubRuleTotal) {
		this.pubRuleTotal = pubRuleTotal;
	}
}
