package com.jecn.epros.server.service.dataImport.match.buss;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;
import com.jecn.epros.server.service.dataImport.match.constant.MatchErrorInfo;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;
import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;

/**
 * 
 * 部门行内检验、行间校验
 * 
 * 部门编号不为空; 部门名称不为空; 上级部门编号不为空; 部门的父部门有且只有一个为0的
 * 
 * 部门出现循环关联父部门
 * 
 * @author Administrator
 * 
 */
public class CheckDeptData {

	/**
	 * 
	 * 部门行内检验
	 * 
	 * 部门编号不为空; 部门名称不为空; 上级部门编号不为空; 部门的父部门有且只有一个为0
	 * 
	 * @return boolean 检验失败返回false，校验成功返回true
	 */
	public boolean checkDeptRow(BaseBean baseBean) {
		if (baseBean == null || baseBean.isNull()) {
			return false;
		}
		boolean ret = true;

		// 获取部门数据
		List<DeptMatchBean> deptBeanList = baseBean.getDeptBeanList();
		if (deptBeanList == null || deptBeanList.size() == 0) {
			return ret;
		}
		// 判断父部门编号为0的数据
		int count = 0;

		for (int i = 0; i < deptBeanList.size(); i++) {

			// 获取当前部门记录
			DeptMatchBean deptBean = deptBeanList.get(i);

			// 部门编号不为空
			if (JecnCommon.isNullOrEmtryTrim(deptBean.getDeptNum())) {
				deptBean.addError(MatchErrorInfo.DEPT_NUM_NULL);
				ret = false;
			} else {
				// 部门编号不能超过122个字符
				if (MatchTool.checkNameMaxLength(deptBean.getDeptNum())) {
					deptBean.addError(MatchErrorInfo.DEPT_NUM_LENGTH_ERROR);
					ret = false;
				}
				deptBean.setDeptNum(deptBean.getDeptNum().trim());
			}

			// 部门名称不为空
			if (JecnCommon.isNullOrEmtryTrim(deptBean.getDeptName())) {
				deptBean.addError(MatchErrorInfo.DEPT_NAME_NULL);
				ret = false;
			} else {
				// 部门名称由中文、英文、数字及“_”、“-”组成
				// if
				// (MatchTool.checkNameFileSpecialChar(deptBean.getDeptName()))
				// {
				// deptBean.addError(MatchErrorInfo.DEPT_NAME_SPECIAL_CHAR);
				// ret = false;
				// } else
				if (MatchTool.checkNameMaxLength(deptBean.getDeptName())) {
					// 部门名称不能超过122个字符
					deptBean.addError(MatchErrorInfo.DEPT_NAME_LENGTH_ERROR);
					ret = false;
				}
				deptBean.setDeptName(deptBean.getDeptName().trim());
			}

			// 上级部门编号不为空
			if (JecnCommon.isNullOrEmtryTrim(deptBean.getPerDeptNum())) {
				deptBean.addError(MatchErrorInfo.P_DEPT_NUM_NULL);
				ret = false;
			} else {
				// 上级部门编号不能超过122个字符
				if (MatchTool.checkNameMaxLength(deptBean.getPerDeptNum())) {
					deptBean.addError(MatchErrorInfo.P_DEPT_NUM_LENGTH_ERROR);
					ret = false;
				}
				deptBean.setPerDeptNum(deptBean.getPerDeptNum().trim());
			}
			// // 标识位不为空 0：添加，1：更新，2：删除
			// if
			// (JecnCommon.isNullOrEmtryTrim(String.valueOf(deptBean.getDeptFlag())))
			// {
			// //deptBean.addError(ErrorInfo.P_DEPT_NUM_NULL);
			// ret = false;
			// } else {
			// deptBean.setDeptFlag(deptBean.getDeptFlag());
			// }

			// 项目ID不为空
			if (JecnCommon.isNullOrEmtryTrim(String.valueOf(deptBean
					.getProjectId()))) {
				deptBean.addError(MatchErrorInfo.P_DEPT_NUM_NULL);
				ret = false;
			} else {
				deptBean.setProjectId(deptBean.getProjectId());
			}

			/**父部门为0的节点可以有多个  */
			if (MatchConstant.PID.equals(deptBean.getPerDeptNum())) {
				count++;
				// if (count >= 2) {
				// deptBean.addError(ErrorInfo.P_DEPT_NUM_0S);
				// ret = false;
				// }
			}

			if (i == deptBeanList.size() - 1) {
				// 没有父部门为0的数据
				if (count == 0) {
					deptBean.addError(MatchErrorInfo.P_DEPT_NUM_0);
					ret = false;
				}
			}
		}

		// 行内检验失败，返回
		if (!ret) {
			return ret;
		}

		// ---------------行间校验
		// 部门编号唯一
		// ret = checkDeptRows(deptBeanList);
		if (!ret) {
			return ret;
		}

		// 按照部门的树形顺序深度排序,出现部门循环关联（A->B->C-A）返回false
		ret = sortDept(deptBeanList);
		// ---------------行间校验

		return ret;
	}

	/**
	 * 
	 * 判断部门编号是否唯一，唯一返回true，不唯一返回false
	 * 
	 * @param deptBeanList
	 *            List<DeptBean>部门集合
	 * @return boolean 唯一返回true，不唯一返回false
	 */
	private boolean checkDeptRows(List<DeptMatchBean> deptBeanList) {
		boolean ret = true;
		for (DeptMatchBean deptBean : deptBeanList) {
			for (DeptMatchBean deptBean2 : deptBeanList) {
				// 不是同一个对象，部门编号相同
				if (!deptBean.equals(deptBean2)
						&& deptBean.getDeptNum().equals(deptBean2.getDeptNum())) {
					deptBean.addError(MatchErrorInfo.DEPT_NUM_NOT_ONLY);
					ret = false;
				}
			}
		}

		return ret;
	}

	/**
	 * 
	 * 按照部门的树形顺序深度排序,排序后数据时按照树形结构排列
	 * 
	 * 
	 * @param deptBeanList
	 *            部门集合
	 * @return boolean 排序过程中出现死循环返回false，正常返回true
	 * 
	 */
	private boolean sortDept(List<DeptMatchBean> deptBeanList) {
		// 排序后的部门集合
		List<DeptMatchBean> deptBeanListNew = new ArrayList<DeptMatchBean>();
		// PID是0的排序部门集合
		List<DeptMatchBean> deptBeanPidOneList = new ArrayList<DeptMatchBean>();
		boolean retboo = false;

		// 获取根节点
		for (DeptMatchBean deptBean : deptBeanList) {
			if (MatchConstant.PID.equals(deptBean.getPerDeptNum())) { // 将PID为0的数据添加到deptBeanListNew里
				deptBeanListNew.add(deptBean);
				deptBeanPidOneList.add(deptBean);
				// break;
			}
		}

		if (deptBeanPidOneList.size() >= 1) {
			// 以根节点开始排序
			for (int i = 0; i < deptBeanPidOneList.size(); i++) { // new
				boolean ret = sort(deptBeanPidOneList.get(i), deptBeanList,
						deptBeanListNew);
				if (!ret) {
					return ret;
				}
			}

		} else {
			return retboo;
		}

		// 没有找到一个部门编号等于给定部门对应的父部门编号
		if (deptBeanList.size() != deptBeanListNew.size()) {
			for (DeptMatchBean deptBean : deptBeanList) {
				if (!deptBeanListNew.contains(deptBean)) {
					deptBean.addError(MatchErrorInfo.P_DEPT_NUM_OTHER);
				}
			}
			return retboo;
		}

		return true;
	}

	/**
	 * 
	 * 递归排序部门
	 * 
	 * @param PdeptBean
	 *            父部门
	 * @param deptBeanList
	 *            部门集合
	 * @param deptBeanListNew
	 *            排序后部门集合
	 * @return boolean 排序过程中出现死循环返回false，正常返回true
	 * 
	 */
	private boolean sort(DeptMatchBean PdeptBean,
			List<DeptMatchBean> deptBeanList,
			List<DeptMatchBean> deptBeanListNew) {

		for (DeptMatchBean deptBean : deptBeanList) {
			// 不是同一个对象,且父节点的部门编号==某部门的父部门编号
			if (!PdeptBean.equals(deptBean)
					&& PdeptBean.getDeptNum().equals(deptBean.getPerDeptNum())) {
				// 判断是否存在循环关联父部门（A->B->C->A）
				if (deptBeanListNew.contains(deptBean)) {
					deptBean.addError(MatchErrorInfo.P_DEPT_NUM_DEAD_LOCK);
					PdeptBean.addError(MatchErrorInfo.P_DEPT_NUM_DEAD_LOCK);
					return false;
				}
				deptBeanListNew.add(deptBean);
				// 递归查找
				sort(deptBean, deptBeanList, deptBeanListNew);
			}
		}
		return true;
	}

}
