package com.jecn.epros.domain.task;

import java.io.Serializable;
import java.sql.Blob;

/**
 * 文件-任务表
 *
 * @author xiaohu
 */
public class FileTaskBeanNew implements Serializable {
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 任务ID
     */
    private Long taskId;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件流内容
     */
    private Blob fileStream;
    /**
     * 文件编号
     */
    private String docId;
    /**
     * 0：公开，1秘密
     */
    private Long isPublic;
    /**
     * 查阅权限岗位ID集合
     */
    private String viewFileTaskOrgIds;
    /**
     * 文件bytes（不存在数据库中）
     */
    private byte[] bytes;//

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Blob getFileStream() {
        return fileStream;
    }

    public void setFileStream(Blob fileStream) {
        this.fileStream = fileStream;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public Long getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Long isPublic) {
        this.isPublic = isPublic;
    }

    public String getViewFileTaskOrgIds() {
        return viewFileTaskOrgIds;
    }

    public void setViewFileTaskOrgIds(String viewFileTaskOrgIds) {
        this.viewFileTaskOrgIds = viewFileTaskOrgIds;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
