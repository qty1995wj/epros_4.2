package com.jecn.svg.dataModel.epros;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.util.JecnUtil;

public class TimeLineDataTransfer {

	private static final Set<String> activityGraphs = new HashSet<String>(JecnUtil.getActiveLists());

	private Map<Long, TimeLineNode> allNodes = new HashMap<Long, TimeLineNode>();
	private List<TimeLinePath> allPaths = new ArrayList<TimeLinePath>();
	private Map<Long, TimeLineBaseData> baseDataMap;
	/** 删除的并行元素 */
	private List<TimeLineBaseData> deleteLineData = new ArrayList<TimeLineBaseData>();
	private Set<Long> endFigureList = new HashSet<Long>();

	public void translate(Map<Long, TimeLineBaseData> sourceMap) {
		baseDataMap = sourceMap;
		for (TimeLineBaseData graphData : baseDataMap.values()) {
			if ("ManhattanLine".equals(graphData.getType())) {
				TimeLinePath path = new TimeLinePath();
				path.setStartNodeId(graphData.getStartDataId());
				path.setEndNodeId(graphData.getEndDataId());
				allPaths.add(path);

				// 连接线结束点集合
				endFigureList.add(graphData.getEndDataId());
			}
		}
		TimeLineNode node = null;

		for (TimeLinePath path : allPaths) {
			if (!allNodes.containsKey(path.getStartNodeId())) {
				node = getTimeLineNodeById(baseDataMap.get(path.getStartNodeId()));
				allNodes.put(node.getId(), node);
			}
			if (!allNodes.containsKey(path.getEndNodeId())) {
				node = getTimeLineNodeById(baseDataMap.get(path.getEndNodeId()));
				allNodes.put(node.getId(), node);
			}
		}

		// 删除并行元素
		deleteLinePathByParallelActive(deleteLineData);
	}

	private void deleteLinePathByParallelActive(List<TimeLineBaseData> deleteActiveList) {
		if (endFigureList.size() == 0) {
			return;
		}
		for (TimeLineBaseData activeData : deleteActiveList) {
			List<TimeLinePath> deleteLines = new ArrayList<TimeLinePath>();
			for (TimeLinePath lineData : allPaths) {
				if (lineData.getStartNodeId() == activeData.getId() || lineData.getEndNodeId() == activeData.getId()) {
					deleteLines.add(lineData);
					allNodes.remove(activeData.getId());
				}
			}
			allPaths.removeAll(deleteLines);
		}
	}

	public Map<Long, TimeLineNode> getAllNodes() {
		return allNodes;
	}

	public List<TimeLinePath> getAllPaths() {
		return allPaths;
	}

	public long getStartNodeId() {
		if (allNodes.size() == 0) {
			return 0;
			// throw new RuntimeException("时间轴节点数据为空!allNodes.size()==0");
		}
		TimeLineNode startNode = null;
		for (TimeLineNode node : allNodes.values()) {
			if (startNode == null) {
				startNode = node;
				continue;
			}
			if (startNode.getX() > node.getX()) {
				startNode = node;
			}
		}
		return startNode.getId();
	}

	private TimeLineNode getTimeLineNodeById(TimeLineBaseData timeLineBaseData) {
		TimeLineNode node = new TimeLineNode();
		node.setId(timeLineBaseData.getId());
		node.setX(timeLineBaseData.getX());
		node.setY(timeLineBaseData.getY());
		node.setWidth(timeLineBaseData.getWidth());
		node.setHeight(timeLineBaseData.getHeight());
		if (isActivityGraph(timeLineBaseData.getType())) {
			long nodeId = findMaxValueGrahpInParentDottedRectBySon(node.getId());
			if (nodeId != -1) {
				TimeLineBaseData maxValueData = baseDataMap.get(nodeId);
				node.setNumber(maxValueData.getNumber());
				node.setValue(maxValueData.getTargetValue());
				node.setStatusValue(maxValueData.getStatusValue());
			} else {
				node.setNumber(timeLineBaseData.getNumber());
				node.setValue(timeLineBaseData.getTargetValue());
				node.setStatusValue(timeLineBaseData.getStatusValue());
			}
		} else if ("DottedRect".equals(timeLineBaseData.getType())) {
			long nodeId = findMaxValueGraphInDottedRectByParent(node.getId());
			if (nodeId != -1) {
				TimeLineBaseData maxValueData = baseDataMap.get(nodeId);
				node.setNumber(maxValueData.getNumber());
				node.setValue(maxValueData.getTargetValue());
				node.setStatusValue(maxValueData.getStatusValue());
			}
		}
		return node;
	}

	/**
	 * 获取协作框内的最大值
	 * 
	 * @param node
	 * @return
	 */
	private long findMaxValueGraphInDottedRectByParent(long parentId) {
		long graphId = -1;
		double maxValue = 0;
		// 并行的活动
		List<TimeLineBaseData> parallelActiveList = new ArrayList<TimeLineBaseData>();
		for (TimeLineBaseData graphData : baseDataMap.values()) {
			if (!isActivityGraph(graphData.getType())) {
				continue;
			}
			if (isInTargetRange(baseDataMap.get(parentId), graphData)) {
				parallelActiveList.add(graphData);
				if (graphData.getStatusValue() != null) {
					double value = graphData.getStatusValue();
					if (maxValue < value) {
						graphId = graphData.getId();
						maxValue = value;
					}
				}
			}
		}
		// 记录待删除的元素
		deleteCostActive(parallelActiveList);
		return graphId;
	}

	private void deleteCostActive(List<TimeLineBaseData> parallelActiveList) {
		for (TimeLineBaseData jecnFigureData : parallelActiveList) {
			if (endFigureList.contains(jecnFigureData.getId())) {
				parallelActiveList.remove(jecnFigureData);
				break;
			}
		}
		deleteLineData.addAll(parallelActiveList);
	}

	/**
	 * 获取协作框内的最大值
	 * 
	 * @param node
	 * @return
	 */
	private long findMaxValueGrahpInParentDottedRectBySon(long sonId) {
		long graphId = -1;
		for (TimeLineBaseData graphData : baseDataMap.values()) {
			if (!"DottedRect".equals(graphData.getType())) {
				continue;
			}
			if (isInTargetRange(graphData, baseDataMap.get(sonId))) {
				graphId = graphData.getId();
				break;
			}
		}
		if (graphId != -1) {
			graphId = findMaxValueGraphInDottedRectByParent(graphId);
		}
		return graphId;
	}

	private boolean isInTargetRange(TimeLineBaseData parent, TimeLineBaseData son) {
		if (son.getX() < parent.getX()) {
			return false;
		}
		if (son.getX() + son.getWidth() > parent.getX() + parent.getWidth()) {
			return false;
		}
		if (son.getY() < parent.getY()) {
			return false;
		}
		if (son.getY() + son.getHeight() > parent.getY() + parent.getHeight()) {
			return false;
		}
		return true;
	}

	private boolean isActivityGraph(String figureType) {
		return activityGraphs.contains(figureType);
	}
}
