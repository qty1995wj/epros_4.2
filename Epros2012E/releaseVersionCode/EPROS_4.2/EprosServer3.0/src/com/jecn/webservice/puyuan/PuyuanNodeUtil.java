package com.jecn.webservice.puyuan;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.jecn.webservice.puyuan.bean.FlowInfo;
import com.jecn.webservice.puyuan.bean.FlowLink;
import com.jecn.webservice.puyuan.bean.FlowNode;

/**
 * 
 * 普元xml生成
 * 
 * @author chehuanbo
 * @date 2014-01-20
 * 
 */
@SuppressWarnings("all")
public class PuyuanNodeUtil {

	private static final Logger log = Logger.getLogger(PuyuanNodeUtil.class);

	/**
	 * 普遍xml生成
	 * 
	 * @param flowInfo
	 *            流程信息（流程基本信息、流程元素）
	 * 
	 * **/
	public static String createXml(FlowInfo flowInfo) {

		try {
			if (flowInfo == null) {
				log.error("flowInfo对象为NULL PuyuanNodeUtil createXml()");
				return null;
			}
			log.info("创建XMl开始！");
			Document doc = null;
			doc = DocumentHelper.createDocument();
			Element workFlowProcessEle = doc.addElement("workflowProcess");
			// 创建根元素
			workFlowProcessEle.addAttribute("productVersion", "6.1");
			workFlowProcessEle.addAttribute("schemaVersion", "6.0");
			// 创建流程基本信息
			createFlowBaseInfoXml(flowInfo, workFlowProcessEle);
			// 创建流程连接线
			createLinkXml(flowInfo, workFlowProcessEle);
			// 创建流程元素
			createFlowNodeXml(flowInfo, workFlowProcessEle);
			return doc.asXML();
		} catch (Exception e) {
			return null;
		}

	}
 

	
	
	
	
	/**
	 * 创建流程基本信息XML
	 * 
	 * @author chehuanbo
	 * @date 2014-01-20
	 * @param flowInfo
	 *            流程信息（流程基本信息、流程元素）
	 * 
	 * **/
	private static void createFlowBaseInfoXml(FlowInfo flowInfo,
			Element workFlowProcessEle) {
		try {
			// ====流程基本信息创建开始======
			Element processHeaderEle = workFlowProcessEle
					.addElement("processHeader");

			Element processBasicInfoEle = processHeaderEle
					.addElement("processBasicInfo");

			// 流程ID
			Element processIdEle =processBasicInfoEle.addElement("processId");
			// 流程ID值(由于普元的BPS软件，流程ID必须以节点id必须以字母、"_"、"$"开头，为了满足此需求所以添加固定字符串"J2P")
			processIdEle.addText("J2P"+flowInfo.getFlowId());
			// 流程名称
			Element processNameEle = processBasicInfoEle.addElement("processName");
			processNameEle.addText(flowInfo.getFlowName());
			// 流程优先级
			Element priorityEle = processBasicInfoEle.addElement("priority");
			priorityEle.addText("60");
			// 流程创建人
			Element authorEle = processBasicInfoEle.addElement("author");
			authorEle.addText(flowInfo.getFlowCreateName());
			// 流程所属部门
			Element departmentEle = processBasicInfoEle.addElement("department");
			departmentEle.addText(flowInfo.getFlowDepart());
			// 流程描述
			Element descriptionEle = processBasicInfoEle.addElement("description");
			descriptionEle.addText("");
			// 相关数据
			processHeaderEle.addElement("dataFields");
			// 出发事件
			processHeaderEle.addElement("triggerEvents");
			// 处理时限
			Element timeLimitEle = processHeaderEle.addElement("timeLimit");
			Element isTimeLimitSetEle = timeLimitEle.addElement("isTimeLimitSet");
			isTimeLimitSetEle.addText("false");
			Element calendarSetEle = timeLimitEle.addElement("calendarSet");
			Element initTypeEle = calendarSetEle.addElement("initType");
			initTypeEle.addText("appoint");
			Element calendarInfoEle = calendarSetEle.addElement("calendarInfo");
			Element resourceTypeEle = calendarInfoEle.addElement("resourceType");
			resourceTypeEle.addText("business-calendar");
			Element resourceIDEle = calendarInfoEle.addElement("resourceID");
			resourceIDEle.addText("default");
			Element resourceNameEle = calendarInfoEle.addElement("resourceName");
			resourceNameEle.addText("默认日历");
			Element parametersEle = calendarInfoEle.addElement("parameters");
			// 可以启动流程的人员列表。如果没有内容，也需要保留all这个默认值。
			Element procStarterListsEle = processHeaderEle
					.addElement("procStarterLists");
			Element processStarterTypeEle = procStarterListsEle
					.addElement("processStarterType");
			processStarterTypeEle.addText("all");
			// 流程参数。如果没有内容，也需要保留空默认值。
			Element parameters2Ele = processHeaderEle.addElement("parameters");
			// 是否分割事务。一般填false
			Element splitTransactionEle = processHeaderEle
					.addElement("splitTransaction");
			splitTransactionEle.addText("false");
			// 是否是长流程。有人工干预的流程都叫长流程，全部是自动流程的叫短流程。必须要有。
			Element slongProcessEle = processHeaderEle.addElement("longProcess");
			slongProcessEle.addText("true");
			// 相关业务实体。如果没有内容，也需要保留空默认值
			Element bizEntityInfoEle = processHeaderEle.addElement("bizEntityInfo");
			// 流程说使用的日历。如果没有内容，也需要保留下面的默认值。
			Element calendarInfo2Ele = processHeaderEle.addElement("calendarInfo");
			Element resourceType2Ele = calendarInfo2Ele.addElement("resourceType");
			resourceType2Ele.addText("business-calendar");
			Element resourceID2Ele = calendarInfo2Ele.addElement("resourceID");
			resourceID2Ele.addText("default");
			Element resourceName2Ele = calendarInfo2Ele.addElement("resourceName");
			resourceName2Ele.addText("默认日历");
			Element parameters3Ele = calendarInfo2Ele.addElement("parameters");
			// ====流程基本信息创建结束==============
		} catch (Exception e) {
			log.error("创建流程基本信息XML出现异常！PuyuanNodeUtil createFlowBaseInfoXml()", e);
		}
	}

	/**
	 * 创建流程连接线XML
	 * 
	 * @author chehuanbo
	 * @date 2014-01-20
	 * @param flowInfo
	 *            流程信息（流程基本信息、流程元素） workFlowProcessEle 流程元素根节点
	 * */
	private static void createLinkXml(FlowInfo flowInfo,
			Element workFlowProcessEle) {
		try {
			// 创建流程连接线开始
			Element transitionsEle = workFlowProcessEle.addElement("transitions");
			for (FlowLink flowLink : flowInfo.getFlowLink()) {
				// transition用以描述连线的信息
				Element transitionEle = transitionsEle.addElement("transition");
				// 该连线的起始活动id，必须
				Element fromEle = transitionEle.addElement("from");
				fromEle.addText(flowLink.getFromNodeId());
				// 该连线的目标活动id，必须。
				Element toEle = transitionEle.addElement("to");
				toEle.addText(flowLink.getToNodeId());
				// 是否是默认连线，必须
				Element isDefaultEle = transitionEle.addElement("isDefault");
				isDefaultEle.addText("true");
				// 该连线的优先级，60表示中，从30到80依次表示：次低、低、次中、中、次高、高
				Element priorityEle = transitionEle.addElement("priority");
				priorityEle.addText("60");
				// 该连线显示的名称。如果没有，也需要保留空默认值。
				Element displayNameEle = transitionEle.addElement("displayName");
				displayNameEle.addText(flowLink.getLinkName());
				// 该连线的类型，分为：简单表达式，连线规则，复杂表达式。一般都是简单表达式
				Element typeEle = transitionEle.addElement("type");
				typeEle.addText("simpleCondition");
				// 表示连线是否存在拐点。请保留
				Element bendPointsEle = transitionEle.addElement("bendPoints");
			}
		} catch (Exception e) {
			log.error("创建流程连接线XML出现异常， PuyuanNodeUtil createLinkXml() ", e);
		}
	}

	/**
	 * 创建流程元素XML
	 * 
	 * @author chehuanbo
	 * @date 2014-01-20
	 * @param flowInfo
	 *            流程信息（流程基本信息、流程元素） workFlowProcessEle 流程元素根节点
	 * **/
	private static void createFlowNodeXml(FlowInfo flowInfo,
			Element workFlowProcessEle) {
		try {
			// 创建流程活动开始
			Element activitiesEle = workFlowProcessEle.addElement("activities");
			for (FlowNode flowNode : flowInfo.getFlowNode()) {
				Element activityEle = activitiesEle.addElement("activity");
				// 活动ID
				Element activityIdEle = activityEle.addElement("activityId");
				activityIdEle.addText(flowNode.getNodeId());
				// 活动名称
				Element activityNameEle = activityEle.addElement("activityName");
				activityNameEle.addText(flowNode.getNodeName());
				// 活动描述
				Element descriptionEle = activityEle.addElement("description");
				descriptionEle.addText(flowNode.getActiveDesc());
				// 活动分支模式
				Element splitTypeEle = activityEle.addElement("splitType");
				splitTypeEle.addText("XOR");
				// 活动聚合模式
				Element joinTypeEle = activityEle.addElement("joinType");
				joinTypeEle.addText("XOR");
				// 活动优先级
				Element priorityEle = activityEle.addElement("priority");
				priorityEle.addText("60");
				// 活动类型
				Element activityTypeEle = activityEle.addElement("activityType");
				activityTypeEle.addText(flowNode.getNodeType());
				// 是否是分割事务
				Element splitTransactionEle = activityEle
						.addElement("splitTransaction");
				splitTransactionEle.addText("false");

				// 启动策略
				if ("finish".equals(flowNode.getNodeType())||"manual".equals(flowNode.getNodeType())) {
					Element activateRuleEle = activityEle
							.addElement("activateRule");
					Element activateRuleTypeEle = activateRuleEle
							.addElement("activateRuleType");
					activateRuleTypeEle.addText("directRunning");
				}

				if (!"finish".equals(flowNode.getNodeType())) {
					// 该活动的扩展信息，必须。formFields表示表单字段，如果没有，保留空默认值.
					Element implementationEle = activityEle.addElement("implementation");
					if("start".equals(flowNode.getNodeType())){
						Element startActivityEle = implementationEle.addElement("startActivity");
						Element formFieldsEle = startActivityEle.addElement("formFields");	
					}else if("manual".equals(flowNode.getNodeType())){
						Element manualActivityEle = implementationEle.addElement("manualActivity");
						//是否允许代理，是的话填true，否填false.
						Element allowAgentEle = manualActivityEle.addElement("allowAgent");
						allowAgentEle.addText("true");
						// 表示人工活动的url链接。是否链接到url，是的话填true，否填false。如果是true，有默认URL，自定义url等
						Element customURLEle = manualActivityEle.addElement("customURL");
						
						Element isSpecifyURLEle = customURLEle.addElement("isSpecifyURL");
						isSpecifyURLEle.addText("false");
					    
					    Element urlTypeEle = customURLEle.addElement("urlType");
					    urlTypeEle.addText("presentation-logic");
					    
					    Element resetUrlEle = manualActivityEle.addElement("resetUrl");
					    Element isSpecify2URLEle = resetUrlEle.addElement("isSpecifyURL");
					    isSpecify2URLEle.addText("false");
					    
					    Element urlType2Ele = resetUrlEle.addElement("urlType");
					    urlType2Ele.addText("presentation-logic");
					    
					   // 在此处配置活动参与者，必须。
					    Element participantsEle = manualActivityEle.addElement("participants");
					    Element participantTypeEle = participantsEle.addElement("participantType");
					    participantTypeEle.addText("process-starter");
					    Element specialActivityIDEle = participantsEle.addElement("specialActivityID");
					    specialActivityIDEle.addText("");
					    
					    Element specialPathEle = participantsEle.addElement("specialPath");
					    specialPathEle.addText("");
					    
					    Element formFieldsEle = manualActivityEle.addElement("formFields");
					     // 在此处配置活动完成时限，如果没有，保留下面的默认值，但是必须。
					    Element timeLimitEle = manualActivityEle.addElement("timeLimit");
					    
					    Element isTimeLimitSetEle = timeLimitEle.addElement("isTimeLimitSet");
					    isTimeLimitSetEle.addText("false");
					    
					    Element calendarSetEle = timeLimitEle.addElement("calendarSet");

					    Element initTypeEle = calendarSetEle.addElement("initType");
					    initTypeEle.addText("appoint");
					    Element calendarInfoEle = calendarSetEle.addElement("calendarInfo");
					    	
					    Element resourceTypeEle = calendarInfoEle.addElement("resourceType");
					    resourceTypeEle.addText("business-calendar");
					    Element resourceIDEle = calendarInfoEle.addElement("resourceID");
					    resourceIDEle.addText("default");
					    Element resourceNameEle = calendarInfoEle.addElement("resourceName");
					    resourceNameEle.addText("默认日历");
					    Element parametersEle = calendarInfoEle.addElement("parameters");
					    // 在此处配置该活动是否有多个工作项，如果没有，保留下面的默认值，但是必须。
					    Element multiWorkItemEle = manualActivityEle.addElement("multiWorkItem");
					    Element isMulWIValidEle = multiWorkItemEle.addElement("isMulWIValid");			
					    isMulWIValidEle.addText("false");
					    Element workitemNumStrategyEle = multiWorkItemEle.addElement("workitemNumStrategy");
					    workitemNumStrategyEle.addText("participant-number");
					    Element finishRuleEle = multiWorkItemEle.addElement("finishRule");
					    finishRuleEle.addText("all");
					    Element isAutoCancelEle = multiWorkItemEle.addElement("isAutoCancel");
					    isAutoCancelEle.addText("false");
					    Element sequentialExecuteEle = multiWorkItemEle.addElement("sequentialExecute");
					    sequentialExecuteEle.addText("false");
					    // 在此处配置该活动的启动事件，如果没有，保留下面的默认值，但是必须。
					    Element triggerEventsEle = manualActivityEle.addElement("triggerEvents");
					    // 在此处配置该活动的异常回滚，如果没有，保留下面的默认值，但是必须。
					    Element rollBackEle = manualActivityEle.addElement("rollBack");
					    
					    Element freeFlowRuleEle = manualActivityEle.addElement("freeFlowRule");
					    
					   // 在此处配置该活动是否允许自由流，如果没有，保留下面的默认值，但是必须。
					    Element isFreeActivityEle = freeFlowRuleEle.addElement("isFreeActivity");
					    isFreeActivityEle.addText("false");
					    Element freeRangeStrategyEle = freeFlowRuleEle.addElement("freeRangeStrategy");
					    freeRangeStrategyEle.addText("freeWithinActivityList");
					    Element isOnlyLimitedManualActivityEle = freeFlowRuleEle.addElement("isOnlyLimitedManualActivity");
					    isOnlyLimitedManualActivityEle.addText("false");
					    
					    Element resetParticipantEle = manualActivityEle.addElement("resetParticipant");
					    resetParticipantEle.addText("originalParticipant");
					    
					    Element notificationEle = manualActivityEle.addElement("notification");
					    Element isSendNotificationEle =  notificationEle.addElement("isSendNotification");
					    isSendNotificationEle.addText("false");
					    
					    Element actionURLEle =  notificationEle.addElement("actionURL");
					    
					    Element isSpecifyURL2Ele = actionURLEle.addElement("isSpecifyURL");
					    isSpecifyURL2Ele.addText("false");
					    Element urlType3Ele = actionURLEle.addElement("urlType");
					    urlType3Ele.addText("presentation-logic");
					    Element participants2Ele =  notificationEle.addElement("participants");
					    
					    Element participantType2Ele =  participants2Ele.addElement("participantType");
					    participantType2Ele.addText("process-starter");
					    Element specialActivityID2Ele =  participants2Ele.addElement("specialActivityID");
//					    if(StringUtils.isNotBlank(flowNode.getActiveRole())&&StringUtils.isNotBlank(flowNode.getActiveRoleId())){
//					    	specialActivityID2Ele.addText(flowNode.getActiveRole());
//					    }else{
					    	specialActivityID2Ele.addText("");
//					    }
					    Element specialPath2Ele =  participants2Ele.addElement("specialPath");
					    specialPath2Ele.addText("");
					    
					    Element isExpandParticipantEle =  notificationEle.addElement("isExpandParticipant");
					    isExpandParticipantEle.addText("false");
					    
					    Element timeLimit3Ele =  notificationEle.addElement("timeLimit");
					    Element isTimeLimitSet3Ele =  timeLimit3Ele.addElement("isTimeLimitSet");
					    isTimeLimitSet3Ele.addText("false");
					    
					    Element calendarSet3Ele =  timeLimit3Ele.addElement("calendarSet");
					    Element  initType3Ele = calendarSet3Ele.addElement("initType");
					    initType3Ele.addText("appoint");
					    
					    Element  calendarInfo2Ele = calendarSet3Ele.addElement("calendarInfo");
					    Element  resourceType2Ele = calendarInfo2Ele.addElement("resourceType");
					    resourceType2Ele.addText("business-calendar");
					    
					    Element  resourceID2Ele = calendarInfo2Ele.addElement("resourceID");
					    resourceID2Ele.addText("default");
					    
					    Element  resourceName2Ele = calendarInfo2Ele.addElement("resourceName");
					    resourceName2Ele.addText("默认日历");
					    
					    Element  parameters2Ele = calendarInfo2Ele.addElement("parameters");
					    Element notificationImplTypeEle =  notificationEle.addElement("notificationImplType");
					    notificationImplTypeEle.addText("workItem");
					}
				}
				// 是否是启动活动
				Element isStartActivityEle = activityEle
						.addElement("isStartActivity");
				if ("start".equals(flowNode.getNodeType())) {
					isStartActivityEle.addText("true");
				} else {
					isStartActivityEle.addText("false");
				}
				// 活动样式
				Element nodeGraphInfoEle = activityEle.addElement("nodeGraphInfo");
				// 元素背景颜色
				Element colorEle = nodeGraphInfoEle.addElement("color");
				colorEle.addText("16777215");
				// 元素高度
				Element heightEle = nodeGraphInfoEle.addElement("height");
				heightEle.addText(flowNode.getNodeHeight());
				// 元素宽度
				Element widthEle = nodeGraphInfoEle.addElement("width");
				widthEle.addText(flowNode.getNodeWidth());
				// x坐标
				Element xEle = nodeGraphInfoEle.addElement("x");
				xEle.addText(flowNode.getxNodePoint());
				// y坐标
				Element yEle = nodeGraphInfoEle.addElement("y");
				yEle.addText(flowNode.getyNodePoint());

				Element lookAndFeelEle = nodeGraphInfoEle.addElement("lookAndFeel");
				lookAndFeelEle.addText("classic");

				Element fontNameEle = nodeGraphInfoEle.addElement("fontName");
				fontNameEle.addText("System");

				Element fontSizeEle = nodeGraphInfoEle.addElement("fontSize");
				fontSizeEle.addText("12");

				Element fontWidthEle = nodeGraphInfoEle.addElement("fontWidth");
				fontWidthEle.addText("550");

				Element foreColorEle = nodeGraphInfoEle.addElement("foreColor");
				foreColorEle.addText("0");

				Element isItalic = nodeGraphInfoEle.addElement("isItalic");
				isItalic.addText("0");

				Element isUnderlineEle = nodeGraphInfoEle.addElement("isUnderline");
				isUnderlineEle.addText("0");

				Element textHeightEle = nodeGraphInfoEle.addElement("textHeight");
				textHeightEle.addText("60");
			}
			workFlowProcessEle.addElement("notes");
			workFlowProcessEle.addElement("resource");
		} catch (Exception e) {
			log.error("创建流程元素XML出现异常！",e);
		}
	}
}
