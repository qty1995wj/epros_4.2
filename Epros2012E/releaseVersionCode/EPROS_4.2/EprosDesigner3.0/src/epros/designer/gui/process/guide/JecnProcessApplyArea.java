package epros.designer.gui.process.guide;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.process.guide.explain.GuideJScrollPane;
import epros.designer.gui.system.DepartMutilSelectCommon;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

public class JecnProcessApplyArea extends JecnFileDescriptionComponent {
	private static Logger log = Logger.getLogger(JecnProcessApplyArea.class);
	protected JecnTextArea jecnArea = new JecnTextArea();
	private String text = "";
	DepartMutilSelectCommon departMutilSelectCommon = null;
	List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	private Long flowId;

	public JecnProcessApplyArea(int index, String name, boolean isRequest, JecnPanel contentPanel, String text,
			JecnDialog jecnDialog, Long flowId, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowId = flowId;
		if (JecnConfigTool.isShowOrgScope()) {
			centerPanel.setPreferredSize(new Dimension(600, 200));
			initData();
			centerPanel.setLayout(new GridBagLayout());
			departMutilSelectCommon = new DepartMutilSelectCommon(0, centerPanel, insets, list, jecnDialog, false,
					JecnProperties.getValue("deptC"), false);
		}
		jecnArea.setRows(4);
		jecnArea.setBorder(null);
		GuideJScrollPane scrollPanel = new GuideJScrollPane();
		GridBagConstraints c = new GridBagConstraints(0, 1, 4, 1, 1.0, 0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		centerPanel.add(scrollPanel, c);
		scrollPanel.setViewportView(jecnArea);
		scrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.text = text == null ? "" : text;
		jecnArea.setText(this.text);
	}

	private void initData() {
		try {
			list = ConnectionPool.getProcessAction().getProcessApplyOrgs(flowId);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("saveException"));
			log.error("JecnProcessApplyArea initData is error!");
		}
	}

	@Override
	public boolean isUpdate() {
		if (departMutilSelectCommon != null) {
			if (departMutilSelectCommon.isUpdate()) {
				return true;
			}
		}
		if (!text.equals(jecnArea.getText())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

	public String getResultStr() {
		return jecnArea.getText().trim();
	}

	public String getOrgIds() {
		if (departMutilSelectCommon == null) {
			return null;
		}
		return departMutilSelectCommon.getResultIds();
	}

}
