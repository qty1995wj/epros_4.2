package com.jecn.epros.server.action.web.task;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.temp.SimpleSubmitMessage;
import com.jecn.epros.server.bean.task.temp.SimpleTaskBean;
import com.jecn.epros.server.bean.task.temp.TaskSearchBean;
import com.jecn.epros.server.bean.task.temp.TaskTypeNameBean;
import com.jecn.epros.server.bean.task.temp.TempAuditPeopleBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.connector.JecnSendTaskTOMainSystemFactory;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.service.task.search.IJecnTaskSearchService;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.epros.server.webBean.task.TaskSearchTempBean;
import com.opensymphony.xwork2.ActionContext;

/**
 * 我的任务，显示、搜索、删除
 * 
 * @author ZHAGNXU
 * @date： 日期：Nov 26, 2012 时间：1:53:19 PM
 */
public class TaskAction extends BaseAction {
	/** 开始行数 */
	private int start;
	/** 每页显示条数 */
	private int limit;
	/** 任务名称 */
	private String taskName;
	/** 创建人 */
	private long createUserId;
	/** 任务阶段 */
	private int taskStage = -1;
	/** 任务类型 */
	private int taskType = -1;
	/** 任务主键ID */
	private long taskId;
	/** 我的任务点击操作 0审批;1重新提交;2整理意见;3任务管理，编辑任务;4:撤回 */
	private int taskOperation;
	/** 开始时间 */
	private String startTime;
	/** 结束时间 */
	private String endTime;
	/** 我的任务，任务计划 搜索、删除、显示记录 service接口 */
	private IJecnTaskSearchService searchService;
	/** 提交审批 任务处理service接口 */
	private IJecnBaseTaskService flowTaskService;
	/** 提交审批 任务处理service接口 */
	private IJecnBaseTaskService fileTaskService;
	/** 提交审批 任务处理service接口 */
	private IJecnBaseTaskService ruleTaskService;
	/** 任务log */
	private Logger log = Logger.getLogger(TaskAction.class);
	/** 任务相关信息 （临时表） */
	private SimpleTaskBean simpleTaskBean;
	/** 任务类型名称 */
	private List<TaskTypeNameBean> listTaskTypeNameBean;
	/** TaskSearchBean 个任务类型下审核人名称 */
	private TaskSearchBean taskSearchBean;
	/** SimpleSubmitMessage 审批人提交任务信息 */
	private SimpleSubmitMessage submitMessage;
	/** 页面提交新增审批人 */
	private List<TempAuditPeopleBean> listAuditPeople;
	private IPersonService personService;

	/** 流程图：process；流程地图：processMap */
	private String type = null;
	/** 流程图/流程地图主键ID */
	private String flowId = null;
	/** 是否为邮件登录审批任务 1：邮件登录审批任务，0 否 */
	private int emailType;
	/** 审批任务进入方式：0：Epros系统，1：中电投 */
	private int eprosButtTask = 0;
	/** 任务搁置 */
	private boolean delayTask;
	/** 人员替换来源人 */
	private String fromPeopleId;
	/** 人员替换目标人 */
	private String toPeopleId;

	/** 文件名称 */
	private String fileName;
	/** 下载产生的文件流 */
	private byte[] bytes;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() throws Exception {
		// 获得文件
		if (bytes == null) {
			// 下载文件信息异常
			outResultHtml(this.getText("fileInformation"), "");
		}
		return new ByteArrayInputStream(bytes);
	}

	public String getFromPeopleId() {
		return fromPeopleId;
	}

	public void setFromPeopleId(String fromPeopleId) {
		this.fromPeopleId = fromPeopleId;
	}

	public String getToPeopleId() {
		return toPeopleId;
	}

	public void setToPeopleId(String toPeopleId) {
		this.toPeopleId = toPeopleId;
	}

	public boolean isDelayTask() {
		return delayTask;
	}

	public void setDelayTask(boolean delayTask) {
		this.delayTask = delayTask;
	}

	/**
	 * 获取查询初始化数据
	 * 
	 * @return
	 */
	public String initSearch() {
		// 获取我的任务查询条件
		taskSearchBean = JecnTaskCommon.getTaskSearchBean();
		// 任务类型名称
		listTaskTypeNameBean = JecnTaskCommon.getTaskTypeNameBeanList();
		return SUCCESS;
	}

	/**
	 * 点击"我的主页"时执行的方法
	 * 
	 * @author yxw 2013-5-10
	 * @description:查询任务总数
	 * @return
	 */
	public String getMyTaskTotal() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		try {
			int total = 0;
			if (hasPost(webLoginBean)) {
				// 在没有查询条件的情况下的任务总数
				total = searchService.getMyTaskCountBySearch(null, webLoginBean.getJecnUser().getPeopleId());
			}
			getRequest().setAttribute("taskTotal", total);

		} catch (Exception e) {
			log.error("", e);
		}
		return SUCCESS;
	}

	/**
	 * 检查登录人是否有岗位
	 * 
	 * @param webLoginBean
	 * @return true有 false没有
	 */
	private boolean hasPost(WebLoginBean webLoginBean) {
		boolean hasPost = true;
		if (webLoginBean.getListPosIds() == null || webLoginBean.getListPosIds().isEmpty()) {
			hasPost = false;
		}
		return hasPost;
	}

	/**
	 * 点击"我的体系任务"和"搜索"时执行的方法
	 * 
	 * @author yxw 2012-11-21
	 * @description:查询我的任务
	 * @return
	 */
	public String getMyTask() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		Long curPeopleId = webLoginBean.getJecnUser().getPeopleId();
		if (curPeopleId == null) {// 登录人不存在
			return null;
		}
		int total = 0;

		// 根据查询条件获取登录人相关任务
		List<MyTaskBean> listTask = new ArrayList<MyTaskBean>();
		try {
			ActionContext.getContext().getSession().put("taskTotal", 0);
			// 获取任务查询条件
			TaskSearchTempBean searchTempBean = getTaskSearchTempBean();
			// 我的任务获取登录人相关任务总数
			total = searchService.getMyTaskCountBySearch(searchTempBean, curPeopleId);
			if (total > 0) {
				// 根据查询条件获取登录人相关任务
				listTask = searchService.getMyTaskBySearch(searchTempBean, curPeopleId, start, limit);
			}
			JSONArray jsonArray = JSONArray.fromObject(listTask);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("getMyTask查询我的 任务失败！方法searchService.getMyTaskBySearch", e);
		}
		if (listTask == null) {
			throw new NullPointerException("获取getMyTask失败！");
		}

		return null;
	}

	/**
	 * 点击"任务管理"执行的方法
	 * 
	 * @author yxw 2012-11-22
	 * @description:任务管理查询任务列表
	 * @return
	 */
	public String taskManageSearch() {

		// 登录人员信息
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		Long curPeopleId = webLoginBean.getJecnUser().getPeopleId();
		if (curPeopleId == null) {// 登录人不存在
			return null;
		}
		// 根据查询条件获取登录人相关任务
		List<MyTaskBean> listTask = null;
		// 获取任务查询条件
		TaskSearchTempBean searchTempBean = getTaskSearchTempBean();
		int total = 0;
		try {
			if (!webLoginBean.isAdmin() && webLoginBean.isSecondAdmin()) {
				// 我的任务,以及我可以管理的任务
				total = searchService.getSecondAdminTaskManagementCountBySearch(searchTempBean, curPeopleId);
			} else {
				// 我的任务 -获取登录人相关任务总数
				total = searchService.getTaskManagementCountBySearch(searchTempBean, curPeopleId, webLoginBean
						.isAdmin());
			}

			if (total > 0) {

				if (!webLoginBean.isAdmin() && webLoginBean.isSecondAdmin()) {

					// 根据查询条件获取登录人相关任务
					listTask = searchService.getSecondAdminTaskManagementBySearch(searchTempBean, start, limit,
							curPeopleId);

				} else {
					// 根据查询条件获取登录人相关任务
					listTask = searchService.getTaskManagementBySearch(searchTempBean, start, limit, curPeopleId,
							webLoginBean.isAdmin());
				}

				JSONArray jsonArray = JSONArray.fromObject(listTask);
				outJsonPage(jsonArray.toString(), total);
			} else {
				outJsonPage("[]", 0);
			}

		} catch (Exception e) {
			log.error("taskManagerment任务管理获取任务集合失败！方法searchService.getMyTaskBySearch", e);
		}
		return null;
	}

	/**
	 * 任务管理下载
	 * 
	 * @return
	 */
	public String taskManageSearchDownload() {

		// 登录人员信息
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		Long curPeopleId = webLoginBean.getJecnUser().getPeopleId();
		if (curPeopleId == null) {// 登录人不存在
			return null;
		}
		// 根据查询条件获取登录人相关任务
		List<MyTaskBean> listTask = null;
		// 获取任务查询条件
		TaskSearchTempBean searchTempBean = getTaskSearchTempBean();
		try {
			if (!webLoginBean.isAdmin() && webLoginBean.isSecondAdmin()) {

				// 根据查询条件获取登录人相关任务
				listTask = searchService.getSecondAdminTaskManagementBySearch(searchTempBean, -1, -1, curPeopleId);

			} else {
				// 根据查询条件获取登录人相关任务
				listTask = searchService.getTaskManagementBySearch(searchTempBean, -1, -1, curPeopleId, webLoginBean
						.isAdmin());
			}

			bytes = searchService.downloadTaskManagementBySearch(listTask);
			fileName = "task.xls";
		} catch (Exception e) {
			log.error("taskManagerment任务管理获取任务集合失败！方法searchService.getMyTaskBySearch", e);
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-11-22
	 * @description:任务取消
	 * @return
	 */
	public String myTaskDelete() {
		try {
			searchService.falseDeleteTask(taskId);
		} catch (Exception e) {
			log.error("taskDelete我的任务假删除异常！方法searchService.falseDeleteTask", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-11-22
	 * @description:任务管理删除
	 * @return
	 */
	public String taskManagementDelete() {
		try {
			searchService.falseDeleteTask(taskId);
		} catch (Exception e) {
			log.error("taskDelete我的任务假删除异常！方法searchService.falseDeleteTask", e);
		}
		return null;
	}

	/**
	 * 
	 * 我的任务查询
	 * 
	 * @return TaskSearchTempBean
	 */
	protected TaskSearchTempBean getTaskSearchTempBean() {
		if (createUserId == -1 && JecnCommon.isNullOrEmtryTrim(taskName) && JecnCommon.isNullOrEmtryTrim(startTime)
				&& JecnCommon.isNullOrEmtryTrim(endTime) && taskStage == -1 && taskType == -1) {
			return null;
		}
		TaskSearchTempBean searchTempBean = null;
		if (createUserId == 0 && taskStage == -1 && taskType == -1 && JecnCommon.isNullOrEmtryTrim(taskName)
				&& JecnCommon.isNullOrEmtryTrim(startTime) && JecnCommon.isNullOrEmtryTrim(endTime) && !delayTask) {
			return searchTempBean;
		} else {
			searchTempBean = new TaskSearchTempBean();
		}
		// 创建人人员主键ID
		if (createUserId != 0 && createUserId != -1) {
			searchTempBean.setCreatePeopleId(createUserId);
		}
		// 任务名称
		searchTempBean.setTaskName(taskName);
		searchTempBean.setStartTime(startTime);
		searchTempBean.setEndTime(endTime);
		if (taskStage != -1) {
			searchTempBean.setTaskStage(taskStage);
		}
		if (taskType != -1) {
			searchTempBean.setTaskType(taskType);
		}
		searchTempBean.setDelayTask(delayTask);
		return searchTempBean;
	}

	/**
	 * @author yxw 2012-11-22
	 * @description:任务查阅
	 * @return
	 */
	public String taskLookUp() {
		if (taskId == -1) {
			throw new TaskIllegalArgumentException("我的任务点击审批获取任务主键ID异常！", -1);
		}
		// 获取任务类型对应的service接口
		IJecnBaseTaskService abstractTaskService = getTaskService();
		if (abstractTaskService == null) {
			throw new TaskIllegalArgumentException("我的任务点击审批获取任务主键ID异常！", -1);
		}
		try {
			simpleTaskBean = abstractTaskService.getSimpleTaskBeanById(taskId, -1);
		} catch (Exception e) {
			log.error("我的任务查阅人物信息异常！taskLookUp", e);
		}
		return SUCCESS;
	}

	/**
	 * @author yxw 2012-11-22
	 * @description:任务审批（审批、重新提交、整理意见、编辑任务）不包括撤回 0审批;1重新提交;2整理意见;3任务管理，编辑任务;
	 * @return
	 */
	public String taskApprove() {
		if (taskId == -1) {
			throw new TaskIllegalArgumentException("我的任务点击审批获取任务主键ID异常！", -1);
		}
		// 获取任务类型对应的service接口
		IJecnBaseTaskService abstractTaskService = getTaskService();
		if (abstractTaskService == null) {
			throw new TaskIllegalArgumentException("我的任务点击审批获取任务主键ID异常！", -1);
		}
		// 从session中获得登陆人信息（网页登陆）
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession()
				.get(JecnContants.SESSION_KEY);
		Long curPeopleId = webLoginBean.getJecnUser().getPeopleId();
		try {
			// 验证当前阶段的审批人是否已审批(防止再次审批，因为点击审批审批完成之后父页面不会刷新)
			if (taskOperation != 3 && abstractTaskService.hasApproveTask(taskId, curPeopleId, taskStage)) {// 不是点击任务编辑情况验证审批人是否已审批
				// 任务已审批！
				ResultHtml(JecnTaskCommon.TASK_APPROVE_OR_PEOPLE_LIVE, null);
				sendInfoByTaskError(webLoginBean);
				return null;
			}

			// 获取相关流程信息
			simpleTaskBean = abstractTaskService.getSimpleTaskBeanById(taskId, taskOperation);
			simpleTaskBean.setTaskId(Long.toString(taskId));

		} catch (Exception e) {
			log.error("我的任务点击任务审批异常！方法taskApprove", e);
		}

		if (simpleTaskBean == null || simpleTaskBean.getJecnTaskBeanNew() == null) {
			throw new TaskIllegalArgumentException("点击审批获取任务相关信息异常 simpleTaskBean！", -1);
		}

		if (taskOperation == 1) {// 打回之后重新提交
			return "re_back";
		} else if (taskOperation == 2) {// 整理意见
			return "coolate_opinion";
		} else if (taskOperation == 3) {// 编辑任务
			// 检查任务是否搁置
			try {
				simpleTaskBean = abstractTaskService.checkTaskIsDelay(simpleTaskBean);
			} catch (Exception e) {
				log.error("我的任务点击任务审批异常！方法taskApprove", e);
			}
			return "task_edit";
		} else if (isControl(simpleTaskBean.getJecnTaskBeanNew())) {
			return "is_Control";
		}
		return SUCCESS;
	}

	/**
	 * 任务审批异常，取消待办任务
	 * 
	 * @param webLoginBean
	 * @throws Exception
	 */
	private void sendInfoByTaskError(WebLoginBean webLoginBean) throws Exception {
		if (!JecnConfigTool.isCect10Login()) {
			return;
		}
		JecnTaskBeanNew taskBeanNew = searchService.getJecnTaskBeanNew(taskId);
		JecnUser createUser = personService.get(taskBeanNew.getCreatePersonId());
		JecnSendTaskTOMainSystemFactory.INSTANCE.sendTaskCancel(taskBeanNew.getCreatePersonId(), taskBeanNew,
				webLoginBean.getJecnUser(), createUser);
	}

	/**
	 * 邮件系统自动登录
	 * 
	 * @return
	 */
	public String loginTaskApprove() {
		if (taskId == 0) {
			log.error("邮件请求参数有空值：taskId = " + taskId);
			return ERROR;
		}

		try {
			JecnTaskBeanNew taskBeanNew = searchService.getJecnTaskBeanNew(taskId);
			if (taskBeanNew == null) {
				log.error("searchService.getJecnTaskBeanNew(taskId)返回null");
				return ERROR;
			}

			/**
			 * 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回
			 * 5：提交意见（提交审批意见(评审-------->拟稿人)提交到拟稿人整理意见 6：二次评审 拟稿人------>评审
			 * 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交
			 */
			if (taskBeanNew.getTaskElseState() == 4) {// 打回
				// 操作为重新提交
				taskOperation = 1;
			} else if (taskBeanNew.getState() == 10
					&& (taskBeanNew.getTaskElseState() == 5 || taskBeanNew.getTaskElseState() == 9)) {// 评审人提交完成
				taskOperation = 2;
			}
			// 当前操作人对应的任务状态
			taskStage = taskBeanNew.getState();
			// 当前操作人对应的任务类型
			taskType = taskBeanNew.getTaskType();
			// 邮件登录审批任务
			emailType = 1;

		} catch (Exception e) {
			log.error("执行loginTaskApprove方法异常：", e);
			return ERROR;
		}
		return taskApprove();
	}

	/**
	 * 任务审批完成发布后，邮件系统自动登录
	 * 
	 * @return
	 */
	public String loginPublicApprove() {
		if (JecnCommon.isNullOrEmtryTrim(type) || JecnCommon.isNullOrEmtryTrim(flowId)) {
			log.error("http请求参数为空。type=" + type + ";flowId=" + flowId);
			return ERROR;
		}
		if (type.equals("process")) {
			return "process";
		} else if (type.equals("processMap")) {
			return "processMap";
		} else {
			return ERROR;
		}
	}

	/**
	 * 是否为文控主导审批
	 * 
	 * @param taskBeanNew
	 * @return true 文控审核人主导审批
	 */
	private boolean isControl(JecnTaskBeanNew taskBeanNew) {
		if (taskBeanNew == null) {
			return false;
		} else if (taskBeanNew.getState() == 1 && taskBeanNew.getIsControlauditLead() == 1
				&& taskBeanNew.getTaskElseState() != 2) {// 文控审核阶段，文控主导审批，并且不是交办状态
			// 2：交办
			return true;
		}
		return false;
	}

	/**
	 * 
	 *任务操作（打回,返回,通过,转批，交办,二次评审取消等操作）
	 * 
	 * @return
	 */
	public String taskOperation() {
		if (taskId == -1) {
			throw new TaskIllegalArgumentException("我的任务点击审批获取任务主键ID异常！", -1);
		}
		// 获取任务类型对应的service接口
		IJecnBaseTaskService abstractTaskService = getTaskService();
		if (abstractTaskService == null) {
			throw new TaskIllegalArgumentException("我的任务点击审批获取任务主键ID异常！", -1);
		}
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		// 当前登录人主键ID
		Long curPeopleId = webLoginBean.getJecnUser().getPeopleId();

		// 登录人主键ID
		if (curPeopleId == null || JecnCommon.isNullOrEmtryTrim(submitMessage.getOpinion())) {
			throw new TaskIllegalArgumentException("任务提交审批时异常 方法taskOperation！" + "任务主键ID=" + submitMessage.getTaskId()
					+ " taskOperation=" + submitMessage.getTaskOperation() + " option=" + submitMessage.getOpinion()
					+ " 任务审批操作人=" + submitMessage.getCurPeopleId(), -1);
		}
		submitMessage.setCurPeopleId(curPeopleId.toString());
		// 设置预估结束时间

		if (simpleTaskBean.getJecnTaskBeanNew() != null
				&& simpleTaskBean.getJecnTaskBeanNew().getEndTimeStr().length() > 0) {
			submitMessage.setEndTime(JecnCommon.getDateByString(simpleTaskBean.getJecnTaskBeanNew().getEndTimeStr()
					.trim()));

		}

		if (simpleTaskBean != null) {
			// 存在审批变动时，获取form 提交的文本域 内容（权限对于的 部门名称，岗位名称）
			taskFixedMessage(submitMessage, simpleTaskBean.getOrgNames(), simpleTaskBean.getPosNames(), simpleTaskBean
					.getGroupNames());

			// 重新提交获取重新提交的人员 操作状态为7
			if (simpleTaskBean.getListAuditPeopleBean() != null) {
				submitMessage.setListAuditPeople(simpleTaskBean.getListAuditPeopleBean());
				if ("7".equals(submitMessage.getTaskOperation().trim())) {
					// 任务说明
					// 任务说明
					submitMessage.setTaskDesc(simpleTaskBean.getJecnTaskBeanNew().getTaskDesc());
				}
			}
		}
		try {
			// 验证当前阶段的审批人是否已审批
			if (!"11".equals(submitMessage.getTaskOperation())
					&& !"12".equals(submitMessage.getTaskOperation())
					&& abstractTaskService.hasApproveTask(Long.valueOf(submitMessage.getTaskId().trim()), curPeopleId,
							taskStage)) {// 不是点击任务编辑情况验证审批人是否已审批
				// 任务已审批
				ResultHtml(JecnTaskCommon.APPROVE_IS_SUBMIT, null);
				return null;
			}
			// 审批通过
			abstractTaskService.taskOperation(submitMessage);
			String strSuccess = JecnTaskCommon.APPROVE_SUCESS;
			if ("11".equals(submitMessage.getTaskOperation()) || "12".equals(submitMessage.getTaskOperation())) {// 编辑
				strSuccess = JecnTaskCommon.EDIT_TASK_SUCESS;
			}
			// "审批成功！"
			if (eprosButtTask == 1) {
				ResultHtml(strSuccess, null);
			} else {
				ResultHtml(strSuccess, getHrefString(Integer.valueOf(submitMessage.getTaskOperation())));
			}

		} catch (Exception e) {
			log.error("任务审批异常！方法taskOperation", e);
			if (e instanceof TaskIllegalArgumentException) {
				int type = ((TaskIllegalArgumentException) e).getExpType();
				// 人员离职或岗位变更
				try {
					String error = "";
					if (type == 1) {// 返回操作，人员相同
						error = JecnTaskCommon.CUR_PEOPLE_IS_SAME_TO_PEOPLE;
					} else if (type == 2)// 当前为整理意见阶段，会审阶段人员为空
					{
						error = JecnTaskCommon.REVIEW_PEOPLE_NOT_NULL;
					} else if (type == 7) {
						error = JecnTaskCommon.TASK_APPROVE_REPEAT;
					} else if (type == -1) {
						error = JecnTaskCommon.TASK_PARAM_ERROR;
					} else {
						// 人员已离职或没有岗位，请联系管理员！
						error = JecnTaskCommon.STAFF_TURNOVER_OR_NO_JOB;
					}
					ResultHtml(error, null);
				} catch (Exception e1) {
					log.error("", e);
				}
			}
			return "error";
		}
		return null;
	}

	/**
	 * 审批变动from提交是提交的文本域 ，name为simpleTaskBean
	 * 
	 * @param submitMessage
	 * @param orgNames
	 *            form 提交的部门名称
	 * @param posNames
	 *            form 提交的岗位名称
	 */
	private void taskFixedMessage(SimpleSubmitMessage submitMessage, String orgNames, String posNames, String groupNames) {
		if (taskStage == -1) {
			return;
		}
		if (taskStage == 1 || taskStage == 2) {
			submitMessage.setOrgNames(orgNames);
			submitMessage.setPosNames(posNames);
			submitMessage.setGroupNames(groupNames);
		}
	}

	/**
	 * 获取跳转后的URL
	 * 
	 * @param operation
	 * @return
	 * @throws Exception
	 */
	private String getHrefString(int operation) throws Exception {
		String basePath = (String) this.getSession().getAttribute("basePath");
		if (operation == 11 || operation == 12) {
			return basePath + "index.jsp?index=taskManage";
		} else {
			// 我的任务获取登录人相关任务总数
			int total = searchService.getMyTaskCountBySearch(null, getPeopleId());
			ActionContext.getContext().getSession().put("taskTotal", total);
			return basePath + "index.jsp?index=myhome&taskTotal=" + total;
		}
	}

	/**
	 * 
	 * 拼装前台界面提示页面
	 * 
	 * @param info
	 * @param href
	 */
	public void ResultHtml(String info, String href) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			getResponse().setContentType("text/html;charset=UTF-8");
			out = getResponse().getWriter();
			if (isNullObj(out)) {
				return;
			}
			out.println("<script language='javascript'>");
			String basePath = (String) this.getSession().getAttribute("basePath");
			// 父页面的URL
			if (!JecnCommon.isNullOrEmtryTrim(href)) {
				out.println("alert('" + info + "');");
				out.println("if (opener!=null&&opener.location != null) {");
				out.println("opener.location.href = '" + href + "';");
				out.println("window.opener=null;window.open('','_self');window.close();");
				out.println("}else{");
				out.println("window.location.href=" + "'" + basePath + "index.jsp?index=myhome';");
				out.println("}");
			} else {
				out.println("if(window.opener == null){");
				out.println(" setTimeout(\"alert('" + info + "');window.location.href=" + "'" + basePath
						+ "index.jsp?index=myhome';\",500)");
				out.println("}else{");
				// 关闭子页面
				out.println(" window.opener.location.href=" + "'" + basePath + "index.jsp?index=myhome';");
				out.println(" setTimeout(\"alert('" + info
						+ "');window.opener=null;window.open('','_self');window.close();\",500)");
				out.println("}");
			}
			out.println("</script>");

			out.flush();
		} catch (IOException e) {
			log.error("", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception ex) {
					log.error("TaskAciton类ResultHtml关闭流错误", ex);
				}

			}
		}
	}

	/**
	 * 任务撤回
	 * 
	 * @return
	 */
	public String taskReCall() {
		submitMessage = new SimpleSubmitMessage();
		// 登录人员信息
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		long curPeopleId = webLoginBean.getJecnUser().getPeopleId();
		// 任务主键ID
		submitMessage.setTaskId(String.valueOf(taskId));
		// 操作人
		submitMessage.setCurPeopleId(String.valueOf(curPeopleId));
		// 审批信息 无
		submitMessage.setOpinion(JecnTaskCommon.TASK_NOTHIN);
		// 操作
		submitMessage.setTaskOperation(String.valueOf(13));
		// 获取任务类型对应的service接口
		IJecnBaseTaskService abstractTaskService = getTaskService();
		if (abstractTaskService == null) {
			throw new TaskIllegalArgumentException("我的任务点击审批获取任务主键ID异常！", -1);
		}
		// 审批通过
		try {
			abstractTaskService.taskOperation(submitMessage);
		} catch (Exception e) {
			log.error("", e);
			return null;
		}
		return null;
	}

	/**
	 * 获取任务类型
	 * 
	 * @return IJecnBaseTaskService
	 */
	private IJecnBaseTaskService getTaskService() {
		if (taskType == -1) {
			return null;
		}
		switch (taskType) {
		case 0:
		case 4:
			return flowTaskService;
		case 2:
		case 3:
			return ruleTaskService;
		case 1:
			return fileTaskService;
		default:
			return null;
		}
	}

	/**
	 * 替换审批人员 如果成功输入success
	 * 
	 * @author huoyl
	 * 
	 */
	public String replaceApprovePeople() {
		if (!(JecnCommon.isNullOrEmtryTrim(fromPeopleId) && JecnCommon.isNullOrEmtryTrim(toPeopleId))) {
			try {

				searchService.replaceApprovePeople(fromPeopleId, toPeopleId);
				ResultHtmlNotClose("success");

			} catch (Exception e) {
				log.error("替换审批人员异常 来源人：" + fromPeopleId + "目标人：" + toPeopleId, e);
			}
		}
		return null;

	}

	/**
	 * 
	 * 拼装前台界面提示页面
	 * 
	 * @param info
	 * @param href
	 */
	public void ResultHtmlNotClose(String info) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			out = getResponse().getWriter();
			out.print(info);
			out.flush();
		} catch (IOException e) {
			log.error("", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception ex) {
					log.error("TaskActio类ResultHtmlNotClose方法关流异常", ex);
				}

			}
		}
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(long createUserId) {
		this.createUserId = createUserId;
	}

	public int getTaskStage() {
		return taskStage;
	}

	public void setTaskStage(int taskStage) {
		this.taskStage = taskStage;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public IJecnTaskSearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(IJecnTaskSearchService searchService) {
		this.searchService = searchService;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public SimpleTaskBean getSimpleTaskBean() {
		return simpleTaskBean;
	}

	public void setSimpleTaskBean(SimpleTaskBean simpleTaskBean) {
		this.simpleTaskBean = simpleTaskBean;
	}

	public IJecnBaseTaskService getFlowTaskService() {
		return flowTaskService;
	}

	public void setFlowTaskService(IJecnBaseTaskService flowTaskService) {
		this.flowTaskService = flowTaskService;
	}

	public IJecnBaseTaskService getFileTaskService() {
		return fileTaskService;
	}

	public void setFileTaskService(IJecnBaseTaskService fileTaskService) {
		this.fileTaskService = fileTaskService;
	}

	public IJecnBaseTaskService getRuleTaskService() {
		return ruleTaskService;
	}

	public void setRuleTaskService(IJecnBaseTaskService ruleTaskService) {
		this.ruleTaskService = ruleTaskService;
	}

	public List<TaskTypeNameBean> getListTaskTypeNameBean() {
		return listTaskTypeNameBean;
	}

	public void setListTaskTypeNameBean(List<TaskTypeNameBean> listTaskTypeNameBean) {
		this.listTaskTypeNameBean = listTaskTypeNameBean;
	}

	public TaskSearchBean getTaskSearchBean() {
		return taskSearchBean;
	}

	public void setTaskSearchBean(TaskSearchBean taskSearchBean) {
		this.taskSearchBean = taskSearchBean;
	}

	public int getTaskOperation() {
		return taskOperation;
	}

	public void setTaskOperation(int taskOperation) {
		this.taskOperation = taskOperation;
	}

	public SimpleSubmitMessage getSubmitMessage() {
		return submitMessage;
	}

	public void setSubmitMessage(SimpleSubmitMessage submitMessage) {
		this.submitMessage = submitMessage;
	}

	public List<TempAuditPeopleBean> getListAuditPeople() {
		return listAuditPeople;
	}

	public void setListAuditPeople(List<TempAuditPeopleBean> listAuditPeople) {
		this.listAuditPeople = listAuditPeople;
	}

	public IPersonService getPersonService() {
		return personService;
	}

	public void setPersonService(IPersonService personService) {
		this.personService = personService;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public int getEmailType() {
		return emailType;
	}

	public void setEmailType(int emailType) {
		this.emailType = emailType;
	}

	public int getEprosButtTask() {
		return eprosButtTask;
	}

	public void setEprosButtTask(int eprosButtTask) {
		this.eprosButtTask = eprosButtTask;
	}

}
