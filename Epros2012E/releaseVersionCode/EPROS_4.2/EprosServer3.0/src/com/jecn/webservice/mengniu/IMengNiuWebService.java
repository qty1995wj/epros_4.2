package com.jecn.webservice.mengniu;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * 蒙牛- 接口
 * 
 * @author xiaohu
 * 
 */
@WebService(endpointInterface = "IMengNiuWebService")
public interface IMengNiuWebService {

	@WebMethod(operationName = "findRuleListsByUserCode")
	public String findRuleListsByUserCode(String userCode, int rows);
}
