package epros.designer.gui.autoNum;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 自动编号-单位代码tree结构
 * 
 * @author Administrator
 * 
 */
public class MengNiuModuleCodeAutoNumJecnTree extends AutoNumJecnTree {

	public MengNiuModuleCodeAutoNumJecnTree(int treeType) {
		super(treeType);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			// 根节点"流程模板管理"
			JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.processCodeRoot, JecnProperties.getValue("modelCode"));
			List<JecnTreeBean> list = ConnectionPool.getAutoCodeAction().getChildProcessCodes(0L, 4);
			JecnTreeCommon.addNLevelNodes(list, rootNode);
			return new JecnTreeModel(rootNode);
		} catch (Exception e) {
			log.error("MengNiuModuleCodeAutoNumJecnTree getTreeModel is error", e);
			return null;
		}
	}
}
