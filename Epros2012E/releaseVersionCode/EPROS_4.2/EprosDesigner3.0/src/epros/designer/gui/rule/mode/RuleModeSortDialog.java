package epros.designer.gui.rule.mode;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

import epros.designer.gui.common.JecnSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class RuleModeSortDialog extends JecnSortDialog {
	private static Logger log = Logger.getLogger(RuleModeSortDialog.class);
	
	public RuleModeSortDialog(JecnTreeNode pNode, JecnTree jTree) {
		super(pNode, jTree);
	}
	@Override
	public List<JecnTreeBean> getChild(){
			try {
				return	ConnectionPool.getRuleModeAction().getChildRuleMode(this.getSelectNode().getJecnTreeBean().getId());
			} catch (Exception e) {
				log.error("RuleModeSortDialog getChild is error",e);
				return new ArrayList<JecnTreeBean>();
			}
		
	}

	@Override
	public boolean saveData(List<JecnTreeDragBean> list)  {
		try {
			ConnectionPool.getRuleModeAction().updateSortRuleMode(list, this.getSelectNode().getJecnTreeBean().getId());
			return true;
		} catch (Exception e) {
			log.error("RuleModeSortDialog saveData is error",e);
			return false;
		}
	
	}

}
