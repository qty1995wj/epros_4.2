package epros.draw.gui.top.shortcut;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import epros.draw.constant.JecnShortcutsConstants;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.JecnTextArea;
import epros.draw.util.DrawCommon;

/**
 * 
 * 快捷键设置类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnShortCutKeyProcess {
	/** 快捷键设置对象 */
	private static JecnShortCutKeyProcess shortCutKeyProcess = new JecnShortCutKeyProcess();

	private JecnShortCutKeyProcess() {

	}

	/**
	 * 
	 * 获取快捷键设置对象
	 * 
	 * @return JecnShortCutKeyProcess 快捷键设置对象
	 */
	public static JecnShortCutKeyProcess getShortCutKeyProcess() {
		return shortCutKeyProcess;
	}

	/**
	 * 
	 * 画图工具快捷键
	 * 
	 * @param toolbarBtn
	 */
	public void initMainPanelKeyAction() {

		// 打开流程图/流程地图/组织图 Ctrl+O
		createKeyAction(JecnShortcutsConstants.KEY_OPEN, KeyStroke
				.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
		// 保存 Ctrl+S
		createKeyAction(JecnShortcutsConstants.KEY_SAVE, KeyStroke
				.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));

		// 另存为 Ctrl+Shift+A
		createKeyAction(JecnShortcutsConstants.KEY_SAVE_AS, KeyStroke
				.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK
						| InputEvent.SHIFT_DOWN_MASK));
		// 导出为图像 Ctrll+Shift+G
		createKeyAction(JecnShortcutsConstants.KEY_SAVE_IMAGE, KeyStroke
				.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_DOWN_MASK
						| InputEvent.SHIFT_DOWN_MASK));
		//放大视图 (Ctrl=)
		createKeyAction(JecnShortcutsConstants.KEY_BIG_IMAGE, KeyStroke
				.getKeyStroke(KeyEvent.VK_EQUALS, InputEvent.CTRL_DOWN_MASK));
		//缩小视图//放大视图 (Ctrl-)
		createKeyAction(JecnShortcutsConstants.KEY_SMALL_IMAGE, KeyStroke
				.getKeyStroke(KeyEvent.VK_MINUS, InputEvent.CTRL_DOWN_MASK));

		// 撤销 Ctrl+Z
		createKeyAction(JecnShortcutsConstants.KEY_UNDO, KeyStroke
				.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));
		// 恢复 Ctrl+Y
		createKeyAction(JecnShortcutsConstants.KEY_REDO, KeyStroke
				.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_DOWN_MASK));

		// 剪切 Ctrl+X
		createKeyAction(JecnShortcutsConstants.KEY_CUT, KeyStroke.getKeyStroke(
				KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK));
		// 复制 Ctrl+C
		createKeyAction(JecnShortcutsConstants.KEY_COPY, KeyStroke
				.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
		// 粘贴 Ctrl+V
		createKeyAction(JecnShortcutsConstants.KEY_PASTE, KeyStroke
				.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
		// 删除 Delete
		createKeyAction(JecnShortcutsConstants.KEY_DELETE,
				KeyStroke.getKeyStroke(KeyEvent.VK_DELETE,
						KeyEvent.KEY_LOCATION_UNKNOWN));
		// 截图 Ctrll+G
		createKeyAction(JecnShortcutsConstants.KEY_ICON_SHOT, KeyStroke
				.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_DOWN_MASK));

		// 全选 Ctrl+A
		createKeyAction(JecnShortcutsConstants.KEY_ALL_SELECT, KeyStroke
				.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK));
	}

	/**
	 * 
	 * 画图面板上编辑框的快捷键，目的不让流程元素对象对应的功能执行
	 * 
	 * @param textArea
	 *            JecnTextArea 编辑框
	 */
	public void initJecnTextAreaKeyAction(JecnTextArea textArea) {
		// 剪切 Ctrl+X
		createJecnTextAreaKeyAction(textArea, JecnShortcutsConstants.KEY_CUT,
				KeyStroke.getKeyStroke(KeyEvent.VK_X,
						InputEvent.CTRL_DOWN_MASK, true));
		// 复制 Ctrl+C
		createJecnTextAreaKeyAction(textArea, JecnShortcutsConstants.KEY_COPY,
				KeyStroke.getKeyStroke(KeyEvent.VK_C,
						InputEvent.CTRL_DOWN_MASK, true));
		// 粘贴 Ctrl+V
		createJecnTextAreaKeyAction(textArea, JecnShortcutsConstants.KEY_PASTE,
				KeyStroke.getKeyStroke(KeyEvent.VK_V,
						InputEvent.CTRL_DOWN_MASK, true));

		// 粘贴 Ctrl+A
		createJecnTextAreaKeyAction(textArea,
				JecnShortcutsConstants.KEY_ALL_SELECT, KeyStroke.getKeyStroke(
						KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK, true));
	}

	/**
	 * 
	 * 添加快捷键，注册到当前主面板
	 * 
	 * 事件标识,唯一且不能为空
	 * 
	 * @param keyStroke
	 *            KeyStroke 快捷键对象
	 */
	private void createJecnTextAreaKeyAction(JecnTextArea textArea,
			String actionKey, KeyStroke keyStroke) {
		if (textArea == null || keyStroke == null
				|| DrawCommon.isNullOrEmtryTrim(actionKey)) {
			return;
		}
		// 整个窗体范围内有效
		InputMap inputMap = textArea.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(keyStroke, actionKey);
		// 注册监听
		ActionMap actionMap = textArea.getActionMap();
		JecnAbstractAction action = new JecnAbstractAction(actionKey);
		actionMap.put(actionKey, action);
	}

	/**
	 * 
	 * 添加快捷键，注册到当前主面板
	 * 
	 * @param actionKey
	 *            事件标识,唯一且不能为空
	 * @param keyStroke
	 *            KeyStroke 快捷键对象
	 */
	private void createKeyAction(String actionKey, KeyStroke keyStroke) {
		if (keyStroke == null || DrawCommon.isNullOrEmtryTrim(actionKey)) {
			return;
		}
		// 整个窗体范围内有效
		InputMap inputMap = JecnDrawMainPanel.getMainPanel().getInputMap(
				JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(keyStroke, actionKey);

		// 注册监听
		ActionMap actionMap = JecnDrawMainPanel.getMainPanel().getActionMap();
		JecnAbstractAction action = new JecnAbstractAction(actionKey);
		actionMap.put(actionKey, action);
	}
}
