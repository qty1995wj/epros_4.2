package com.jecn.epros.server.bean.integration;

import java.util.Date;

/***
 * 控制目标
 * 2013-10-29
 *
 */
public class JecnControlTarget implements java.io.Serializable{
	private static final long serialVersionUID = -6427887335927971595L;
	/***主键ID*/
	private Long id;
	/**所属风险ID**/
	private Long riskId;
	/***描述*/
	private String description;
	/***创建人*/
	private Long createPersonId;
	/***创建日期*/
	private Date createTime;
	/***更新人*/
	private Long updatePersonId;
	/***更新日期*/
	private Date updateTime;
	/**备注*/
	private String note;
	/**序号*/
	private Integer sort;
	/**风险编号*/
	private String riskNum;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRiskId() {
		return riskId;
	}
	public void setRiskId(Long riskId) {
		this.riskId = riskId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getCreatePersonId() {
		return createPersonId;
	}
	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getUpdatePersonId() {
		return updatePersonId;
	}
	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getRiskNum() {
		return riskNum;
	}
	public void setRiskNum(String riskNum) {
		this.riskNum = riskNum;
	}

}
