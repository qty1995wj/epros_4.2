package epros.designer.gui.define;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class JecnTermDefineTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(JecnTermDefineTree.class);
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new JecnTermDefineTreeListenr(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getTermDefinitionAction().getChilds(0L);
		} catch (Exception e) {
			log.error(JecnProperties.getValue("termDefineDownloadError"), e);
		}
		return JecnTermDefineCommon.getTermDefineTreeModel(list);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		JecnTermDefineCommon.treeMousePressed(evt, this);
	}

}
