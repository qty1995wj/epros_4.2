package com.jecn.epros.server.dao.popedom;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.bean.popedom.JecnPositionGroup;
import com.jecn.epros.server.common.IBaseDao;

public interface IPositoinGroupDao extends IBaseDao<JecnPositionGroup, Long> {
	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除岗位组
	 * @param setIds
	 * @throws Exception
	 */
	public void deletePositionGroup(Set<Long> setIds) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除岗位组
	 * @param projectId
	 * @throws Exception
	 */
	public void deletePositionGroup(Long projectId) throws Exception;

	/**
	 * 按照岗位组Id获得岗位信息
	 * @author weidp
	 * @date： 日期：2014-4-23 时间：上午10:14:47
	 * @param posGroupId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getPositionsByPosGroupId(long posGroupId)
			throws Exception;
	
	/**
	 * 查询基准岗位
	 * @author cheshaowei
	 * @date 2015-03-24
	 * @param projectId 项目ID
	 *        baseName 基准岗位名称
	 * */
	public List<JecnBasePosBean> getJecnBasePosBean(Long projectId,
			String baseName);
	
	/**
	 * 查询岗位组关联的基准岗位
	 * 
	 * @author cheshaowei
	 * 
	 * */
	public List<JecnBasePosBean> getJecnbasePosRelBean(Long projectId,String posGroupId);
	
	
	/**
	 * 根据ID查询基准岗位对应岗位
	 * 
	 * @author cheshaowei
	 * @param baseId 基准岗位ID
	 * @throws Exception 
	 * */
	public List<Object[]> getBaseRelPos(String baseId,String pageNum,String pageSize,String operationType) throws Exception;

	/**
	 * 岗位组关联基准岗位，去除已关联的并且要删除的基准岗位相关数据
	 * 
	 * @param @param deleteBasePosList
	 * @param @param groupId
	 * @param @throws Exception
	 * @date 2015-7-29 下午03:53:33
	 * @throws
	 */
	void deleteBasePosRel(List<String> deleteBasePosList, Long groupId)
			throws Exception;

	/**
	 * 添加岗位组和岗位关联关系（岗位组关联基准岗位）
	 * 
	 * @param addBaseIds
	 *            新增的基准岗位，对应岗位组关联基准岗位表
	 * @param updateOrInsetBaseIds
	 *            岗位组和岗位关联表，新增的或存在的基准岗位IDs
	 * @param groupId
	 * @return void
	 * @date 2015-7-29 下午03:56:21
	 * @throws
	 */
	void addBasePosRelGroup(List<String> addBaseIds,List<String> updateOrInsetBaseIds, Long groupId) throws Exception;
	
}
