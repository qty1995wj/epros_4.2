package epros.designer.gui.system.config.ui.property.type;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigPasswordField;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextField;
import epros.designer.gui.system.config.ui.comp.JecnDocumentListener;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/*******************************************************************************
 * 
 * 邮箱配置
 * 
 * @author ZHOUXY
 * 
 */
public class JecnMailBoxSetPanel extends JecnPanel implements ActionListener {
	/** 邮箱主面板 */
	private JecnMailPanel mailPanel = null;

	/** 内网面板inPanel */
	private JecnPanel inPanel = null;
	/** 外网面板outPanel */
	private JecnPanel outPanel = null;

	// *******************内网*******************//
	/** 内网服务器 */
	private JLabel inServerLab = null;
	/** 内网服务器Field */
	private JecnConfigTextField inServerField = null;
	/** 内网邮箱地址Lab */
	private JLabel inMailAddrLab = null;
	/** * 内网邮箱地址Field */
	private JecnConfigTextField inMailAddrField = null;
	/** 内网邮箱地址别名Lab */
	private JLabel inMailAddrAliasLab = null;
	/** * 内网邮箱地址别名Field */
	private JecnConfigTextField inMailAddrAliasField = null;
	/** 内网密码Lab */
	private JLabel inPasswordLab = null;
	/** * 内网密码Field */
	private JecnConfigPasswordField inPasswordField = null;
	/** 内网邮箱登陆名Lab */
	private JLabel inLoginNameLab = null;
	/*** -内网邮箱登陆名Field ***/
	private JecnConfigTextField inLoginNameField = null;
	/** 内网端口号Lab */
	private JLabel inPortLab = null;
	/***** 内网端口号Field ***/
	private JecnConfigTextField inPortField = null;
	/**** 内网是否启用ssl ****/
	private JecnConfigCheckBox inIsUseSSLBox;
	/*** 内网是否启用匿名发送 *****/
	private JecnConfigCheckBox inIsUseAnonymity;
	// *******************内网*******************//

	// *******************外网*******************//
	/** 外网服务器 */
	private JLabel outServerLab = null;
	/** 外网服务器Field */
	private JecnConfigTextField outServerField = null;
	/** 外网邮箱地址Lab */
	private JLabel outMailAddrLab = null;
	/** * 外网邮箱地址Field */
	private JecnConfigTextField outMailAddrField = null;
	/** 外网邮箱地址别名Lab */
	private JLabel outMailAddrAliasLab = null;
	/** * 外网邮箱地址别名Field */
	private JecnConfigTextField outMailAddrAliasField = null;
	/** 外网密码Lab */
	private JLabel outPasswordLab = null;
	/** * 外网密码Field */
	private JecnConfigPasswordField outPasswordField = null;
	/** 外网邮箱登陆名Lab */
	private JLabel outLoginNameLab = null;
	/*** 外网邮箱登陆名Field **/
	private JecnConfigTextField outLoginNameField = null;
	/** 外网端口号Lab **/
	private JLabel outPortLab = null;
	/** 外网端口号Field **/
	private JecnConfigTextField outPortField = null;
	/** 外网是否启用ssl */
	private JecnConfigCheckBox outIsUseSSLBox;
	/*** -外网是否启用匿名发送 */
	private JecnConfigCheckBox outIsUseAnonymity;
	// *******************外网*******************//

	/** 内网服务器提示信息 */
	private JecnUserInfoTextArea inServerInfoTextArea = null;
	/** 内网邮箱地址提示信息 */
	private JecnUserInfoTextArea inMailAddrInfoTextArea = null;
	private JecnUserInfoTextArea inMailAddrInfoTextAliasArea = null;
	/** 内网密码提示信息 */
	private JecnUserInfoTextArea inPwdInfoTextArea = null;
	/** 内网邮箱登陆名提示信息 */
	private JecnUserInfoTextArea inLoginNameTextArea = null;
	/** 内网端口号提示信息 */
	private JecnUserInfoTextArea inPortTextArea = null;
	/** 外网服务器提示信息 */
	private JecnUserInfoTextArea outServerInfoTextArea = null;
	/** 外网邮箱地址提示信息 */
	private JecnUserInfoTextArea outMailAddrInfoTextArea = null;
	private JecnUserInfoTextArea outMailAddrInfoTextAliasArea = null;
	/** 外网密码提示信息 */
	private JecnUserInfoTextArea outPwdInfoTextArea = null;
	/** 外网邮箱登陆名提示信息 */
	private JecnUserInfoTextArea outLoginNameTextArea = null;
	/** 外网端口号 提示信息 */
	private JecnUserInfoTextArea outPortTextArea = null;

	/** 选中节点 */
	private JecnConfigTypeDesgBean configTypeDesgBean = null;

	private JecnConfigCheckBox outIsUseTLSBox;

	private JecnConfigCheckBox inIsUseTLSBox;

	public JecnMailBoxSetPanel(JecnMailPanel mailPanel) {
		this.mailPanel = mailPanel;

		initComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {

		// 内网面板inPanel
		inPanel = new JecnPanel();
		// 外网面板outPanel
		outPanel = new JecnPanel();

		// *******************内网*******************//
		// 内网服务器
		inServerLab = new JLabel(JecnProperties.getValue("loginServer")); // /
		// 内网服务器Field
		inServerField = new JecnConfigTextField();
		// 内网邮箱地址Lab
		inMailAddrLab = new JLabel(JecnProperties.getValue("emailAddrC"));
		// * 内网邮箱地址Field
		inMailAddrField = new JecnConfigTextField();
		inMailAddrAliasLab = new JLabel(JecnProperties.getValue("emailAlias"));
		inMailAddrAliasField = new JecnConfigTextField();
		// 内网密码Lab
		inPasswordLab = new JLabel(JecnProperties.getValue("loginPwd"));//
		// * 内网密码Field
		inPasswordField = new JecnConfigPasswordField();
		// 内网邮箱登陆名
		inLoginNameLab = new JLabel(JecnProperties.getValue("emailLoginNameC"));
		// 内网邮箱登陆名
		inLoginNameField = new JecnConfigTextField();
		// 内网端口号
		inPortLab = new JLabel(JecnProperties.getValue("emailPortC"));
		// 内网端口号
		inPortField = new JecnConfigTextField();
		// 内网是否启用ssl*
		inIsUseSSLBox = new JecnConfigCheckBox(JecnProperties.getValue("useSSL"));
		// 内网是否启用TLS*
		inIsUseTLSBox = new JecnConfigCheckBox(JecnProperties.getValue("enableTLS"));
		// 内网是否启用匿名发送**
		inIsUseAnonymity = new JecnConfigCheckBox(JecnProperties.getValue("useNullName"));
		// *******************内网*******************//

		// *******************外网*******************//
		// 外网服务器
		outServerLab = new JLabel(JecnProperties.getValue("loginServer"));
		// 外网服务器Field
		outServerField = new JecnConfigTextField();
		// 外网邮箱地址Lab
		outMailAddrLab = new JLabel(JecnProperties.getValue("emailAddrC"));
		// * 外网邮箱地址Field
		outMailAddrField = new JecnConfigTextField();
		outMailAddrAliasLab = new JLabel(JecnProperties.getValue("emailAlias"));
		outMailAddrAliasField = new JecnConfigTextField();
		// 外网密码Lab
		outPasswordLab = new JLabel(JecnProperties.getValue("loginPwd"));
		// * 外网密码Field
		outPasswordField = new JecnConfigPasswordField();
		// 外网邮箱登陆名
		outLoginNameLab = new JLabel(JecnProperties.getValue("emailLoginNameC"));
		// 外网邮箱登陆名
		outLoginNameField = new JecnConfigTextField();
		// 外网端口号
		outPortLab = new JLabel(JecnProperties.getValue("emailPortC"));
		// 外网端口号
		outPortField = new JecnConfigTextField();
		// 外网是否启用ssl
		outIsUseSSLBox = new JecnConfigCheckBox(JecnProperties.getValue("useSSL"));
		// 外网是否启用tls
		outIsUseTLSBox = new JecnConfigCheckBox(JecnProperties.getValue("enableTLS"));
		// -内网是否启用匿名发送
		outIsUseAnonymity = new JecnConfigCheckBox(JecnProperties.getValue("useNullName"));
		// *******************外网*******************//

		// 内网服务器提示信息
		inServerInfoTextArea = new JecnUserInfoTextArea();
		// 内网邮箱地址提示信息
		inMailAddrInfoTextArea = new JecnUserInfoTextArea();
		inMailAddrInfoTextAliasArea = new JecnUserInfoTextArea();
		// 内网密码提示信息
		inPwdInfoTextArea = new JecnUserInfoTextArea();
		// 内网邮箱登陆名提示信息
		inLoginNameTextArea = new JecnUserInfoTextArea();
		// 内网端口号提示信息
		inPortTextArea = new JecnUserInfoTextArea();
		// 外网服务器提示信息
		outServerInfoTextArea = new JecnUserInfoTextArea();
		// 外网邮箱地址提示信息
		outMailAddrInfoTextArea = new JecnUserInfoTextArea();
		outMailAddrInfoTextAliasArea = new JecnUserInfoTextArea();
		// 外网密码提示信息
		outPwdInfoTextArea = new JecnUserInfoTextArea();
		// 外网邮箱登陆名提示信息
		outLoginNameTextArea = new JecnUserInfoTextArea();
		// 外网端口号提示信息
		outPortTextArea = new JecnUserInfoTextArea();
		inPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		outPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 边框
		this.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("mailBoxSetingLab")));
		inPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("intranet")));
		outPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("external")));

		// 内网服务器提示信息
		inServerInfoTextArea.setVisible(false);
		// 内网邮箱地址提示信息
		inMailAddrInfoTextArea.setVisible(false);
		inMailAddrInfoTextAliasArea.setVisible(false);
		// 内网密码提示信息
		inPwdInfoTextArea.setVisible(false);
		// 内网邮箱登陆名提示信息
		inLoginNameTextArea.setVisible(false);
		// 内网端口号提示信息
		inPortTextArea.setVisible(false);
		// 外网服务器提示信息
		outServerInfoTextArea.setVisible(false);
		// 外网邮箱地址提示信息
		outMailAddrInfoTextArea.setVisible(false);
		outMailAddrInfoTextAliasArea.setVisible(false);
		// 外网密码提示信息
		outPwdInfoTextArea.setVisible(false);
		// 外网邮箱登陆名提示信息
		outLoginNameTextArea.setVisible(false);
		// 外网端口号提示信息
		outPortTextArea.setVisible(false);

		// *******************内网*******************//
		inServerField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, inServerField, inServerInfoTextArea));
		inMailAddrField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, inMailAddrField, inMailAddrInfoTextArea));
		inMailAddrAliasField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, inMailAddrAliasField, inMailAddrInfoTextAliasArea));
		inPasswordField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, inPasswordField, inPwdInfoTextArea));
		inLoginNameField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, inLoginNameField, inLoginNameTextArea));
		inPortField.getDocument().addDocumentListener(new JecnDocumentListener(mailPanel, inPortField, inPortTextArea));
		// 默认不选中
		inIsUseSSLBox.setSelected(false);
		inIsUseAnonymity.setSelected(false);

		inIsUseSSLBox.setOpaque(false);
		inIsUseAnonymity.setOpaque(false);

		inIsUseSSLBox.addActionListener(this);
		inIsUseAnonymity.addActionListener(this);

		inIsUseTLSBox.addActionListener(this);
		outIsUseTLSBox.addActionListener(this);

		inIsUseTLSBox.setSelected(false);
		outIsUseTLSBox.setSelected(false);

		inIsUseTLSBox.setOpaque(false);
		outIsUseTLSBox.setOpaque(false);

		// *******************内网*******************//
		// *******************外网*******************//
		outServerField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, outServerField, outServerInfoTextArea));
		outMailAddrField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, outMailAddrField, outMailAddrInfoTextArea));
		outMailAddrAliasField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, outMailAddrAliasField, outMailAddrInfoTextAliasArea));
		outPasswordField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, outPasswordField, outPwdInfoTextArea));
		outLoginNameField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, outLoginNameField, outLoginNameTextArea));
		outPortField.getDocument().addDocumentListener(
				new JecnDocumentListener(mailPanel, outPortField, outPortTextArea));
		// 默认不选中
		outIsUseSSLBox.setSelected(false);
		outIsUseAnonymity.setSelected(false);

		outIsUseSSLBox.setOpaque(false);
		outIsUseAnonymity.setOpaque(false);

		outIsUseSSLBox.addActionListener(this);
		outIsUseAnonymity.addActionListener(this);
		outIsUseAnonymity.setVisible(false);
		// *******************外网*******************//
	}

	/**
	 * 
	 * 布局
	 * 
	 */
	private void initLayout() {
		Insets insets = new Insets(5, 3, 5, 3);

		// 内网面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		this.add(inPanel, c);
		// 外网面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		this.add(outPanel, c);

		// *************内网*************//
		int r = 0;
		// 服务器
		c = new GridBagConstraints(0, r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		inPanel.add(inServerLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inServerField, c);
		// 内网服务器提示信息
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inServerInfoTextArea, c);

		// 邮箱地址
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		inPanel.add(inMailAddrLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inMailAddrField, c);
		// 内网邮箱地址提示信息
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inMailAddrInfoTextArea, c);

		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		inPanel.add(inMailAddrAliasLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inMailAddrAliasField, c);
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inMailAddrInfoTextAliasArea, c);

		// 内网邮箱登陆名
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		inPanel.add(inLoginNameLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inLoginNameField, c);
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inLoginNameTextArea, c);

		// 密码
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		inPanel.add(inPasswordLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inPasswordField, c);
		// 内网密码提示信息
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inPwdInfoTextArea, c);

		// 内网端口号
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		inPanel.add(inPortLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inPortField, c);
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		inPanel.add(inPortTextArea, c);
		// 内网是否启用ssl
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		inPanel.add(inIsUseSSLBox, c);
		// 内网是否启用匿名发送
		c = new GridBagConstraints(1, r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		inPanel.add(inIsUseAnonymity, c);
		// 内网是否启用tls
		c = new GridBagConstraints(2, r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		inPanel.add(inIsUseTLSBox, c);
		// 空白
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0,
				0);
		inPanel.add(new JLabel(), c);
		// *************内网*************//

		// *************外网*************//
		r = 0;
		// 外网服务器
		c = new GridBagConstraints(0, r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		outPanel.add(outServerLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outServerField, c);
		// 外网服务器提示信息
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outServerInfoTextArea, c);

		// 外网邮箱地址
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		outPanel.add(outMailAddrLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outMailAddrField, c);
		// 外网邮箱地址提示信息
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outMailAddrInfoTextArea, c);

		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		outPanel.add(outMailAddrAliasLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outMailAddrAliasField, c);
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outMailAddrInfoTextAliasArea, c);

		// 外网邮箱登陆名
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		outPanel.add(outLoginNameLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outLoginNameField, c);
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outLoginNameTextArea, c);

		// 外网密码
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		outPanel.add(outPasswordLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outPasswordField, c);
		// 外网密码提示信息
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outPwdInfoTextArea, c);

		// 外网端口号
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		outPanel.add(outPortLab, c);
		c = new GridBagConstraints(1, r, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outPortField, c);
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		outPanel.add(outPortTextArea, c);
		// 外网是否启用ssl
		c = new GridBagConstraints(0, ++r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		outPanel.add(outIsUseSSLBox, c);
		// 外网是否启用匿名发送
		c = new GridBagConstraints(1, r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		outPanel.add(outIsUseAnonymity, c);
		// 内网是否启用tls
		c = new GridBagConstraints(2, r, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		outPanel.add(outIsUseTLSBox, c);
		// 空白
		c = new GridBagConstraints(1, ++r, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0,
				0);
		outPanel.add(new JLabel(), c);
		// *************外网*************//
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {

		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {// 非表数据
			// 唯一标识
			String mark = itemBean.getMark();
			// 值
			String itemValue = itemBean.getValue();

			if (ConfigItemPartMapMark.mailBoxInnerServerAddr.toString().equals(mark)) {// 邮箱内网服务器
				inServerField.setItemBean(itemBean);
				inServerField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxInnerMailAddr.toString().equals(mark)) {// 邮箱内网邮箱地址
				inMailAddrField.setItemBean(itemBean);
				inMailAddrField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxInnerAlias.toString().equals(mark)) {
				inMailAddrAliasField.setItemBean(itemBean);
				inMailAddrAliasField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxInnerPwd.toString().equals(mark)) {// 邮箱内网密码
				inPasswordField.setItemBean(itemBean);
				inPasswordField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxInnerLoginName.toString().equals(mark)) {// 邮箱内网登录名
				inLoginNameField.setItemBean(itemBean);
				inLoginNameField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxInnerPort.toString().equals(mark)) {// 邮箱内网端口号
				inPortField.setItemBean(itemBean);
				inPortField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxInnerIsUseSSL.toString().equals(mark)) {
				// 内网是否启用ssl
				inIsUseSSLBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(itemValue)) {
					inIsUseSSLBox.setSelected(true);
				} else {
					inIsUseSSLBox.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.mailBoxInnerIsUseAnonymity.toString().equals(mark)) {
				// 内网是否启用匿名发送
				inIsUseAnonymity.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(itemValue)) {
					inIsUseAnonymity.setSelected(true);
					inLoginNameField.setEditable(false);
					inPasswordField.setEditable(false);
				} else {
					inIsUseAnonymity.setSelected(false);
					inLoginNameField.setEditable(true);
					inPasswordField.setEditable(true);
				}
			} else if (ConfigItemPartMapMark.mailBoxOuterServerAdd.toString().equals(mark)) {// 邮箱外网服务器
				outServerField.setItemBean(itemBean);
				outServerField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxOuterMailAddr.toString().equals(mark)) { // 邮箱外网邮箱地址
				outMailAddrField.setItemBean(itemBean);
				outMailAddrField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxOuterAlias.toString().equals(mark)) {
				outMailAddrAliasField.setItemBean(itemBean);
				outMailAddrAliasField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxOuterPwd.toString().equals(mark)) { // 邮箱外网密码
				outPasswordField.setItemBean(itemBean);
				outPasswordField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxOuterLoginName.toString().equals(mark)) {// 邮箱外网登录名
				outLoginNameField.setItemBean(itemBean);
				outLoginNameField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxOuterPort.toString().equals(mark)) {// 邮箱外网端口号
				outPortField.setItemBean(itemBean);
				outPortField.setText(itemValue);
			} else if (ConfigItemPartMapMark.mailBoxOuterIsUseSSL.toString().equals(mark)) {
				// 外网是否启用ssl
				outIsUseSSLBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(itemValue)) {
					outIsUseSSLBox.setSelected(true);
				} else {
					outIsUseSSLBox.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.mailBoxOuterIsUseAnonymity.toString().equals(mark)) {
				// 外网是否启用匿名发送
				outIsUseAnonymity.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(itemValue)) {
					outIsUseAnonymity.setSelected(true);
					outLoginNameField.setEditable(false);
					outPasswordField.setEditable(false);
				} else {
					outIsUseAnonymity.setSelected(false);
					outLoginNameField.setEditable(true);
					outPasswordField.setEditable(true);
				}
			} else if (ConfigItemPartMapMark.mailBoxOuterIsUseTLS.toString().equals(mark)) {
				// 外网是否启用tls
				outIsUseTLSBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(itemValue)) {
					outIsUseTLSBox.setSelected(true);
				} else {
					outIsUseTLSBox.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.mailBoxInnerIsUseTLS.toString().equals(mark)) {
				// 内网是否启用tls
				inIsUseTLSBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(itemValue)) {
					inIsUseTLSBox.setSelected(true);
				} else {
					inIsUseTLSBox.setSelected(false);
				}
			}
		}
	}

	/**
	 * 
	 * 校验
	 * 
	 * @return boolean true：成功；false：失败
	 */
	public boolean check() {
		// 存在错误标识：true，无错：false

		boolean exsitsError = false;

		// 内网服务器提示信息:不能超过122个字符或61个汉字
		String info = JecnUserCheckUtil.checkNameLength(inServerField.getText());
		exsitsError = mailPanel.setErrorInfo(inServerInfoTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		// 内网邮箱地址提示信息:不能超过122个字符或61个汉字
		info = JecnUserCheckUtil.checkNameLength(inMailAddrField.getText());
		exsitsError = mailPanel.setErrorInfo(inMailAddrInfoTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		// 内网邮箱格式验证提示信息 c
		String inMailStr = inMailAddrField.getText();
		if (inMailStr != null && !"".equals(inMailStr.trim())) {
			info = mailPanel.cheackMailIp(inMailStr);
			exsitsError = mailPanel.setErrorInfo(inMailAddrInfoTextArea, info);
			if (exsitsError) {
				return exsitsError;
			}
		}
		info = JecnUserCheckUtil.checkNameLength(inMailAddrAliasField.getText());
		exsitsError = mailPanel.setErrorInfo(inMailAddrInfoTextAliasArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		// 内网密码提示信息:不能超过122个字符或61个汉字
		info = JecnUserCheckUtil.checkNameLength(new String(inPasswordField.getPassword()));
		exsitsError = mailPanel.setErrorInfo(inPwdInfoTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}

		// 外网服务器提示信息:不能超过122个字符或61个汉字
		info = JecnUserCheckUtil.checkNameLength(outServerField.getText());
		exsitsError = mailPanel.setErrorInfo(outServerInfoTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}

		// 外网邮箱地址提示信息:不能超过122个字符或61个汉字
		info = JecnUserCheckUtil.checkNameLength(outMailAddrField.getText());
		exsitsError = mailPanel.setErrorInfo(outMailAddrInfoTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		// 外网邮箱格式提示信息c
		String outMailStr = outMailAddrField.getText();
		if (outMailStr != null && !"".equals(outMailStr.trim())) {
			info = mailPanel.cheackMailIp(outMailStr);
			exsitsError = mailPanel.setErrorInfo(outMailAddrInfoTextArea, info);
			if (exsitsError) {
				return exsitsError;
			}
		}
		info = JecnUserCheckUtil.checkNameLength(outMailAddrAliasField.getText());
		exsitsError = mailPanel.setErrorInfo(outMailAddrInfoTextAliasArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		// 外网密码提示信息:不能超过122个字符或61个汉字
		info = JecnUserCheckUtil.checkNameLength(new String(outPasswordField.getPassword()));
		exsitsError = mailPanel.setErrorInfo(outPwdInfoTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		// 内网登录名称提示信息:不能超过122个字符或61个汉字
		info = JecnUserCheckUtil.checkNameLength(inLoginNameField.getText());
		exsitsError = mailPanel.setErrorInfo(inLoginNameTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		// 内网端口号验证
		info = getNumCheckInfo(inPortField.getText());
		exsitsError = mailPanel.setErrorInfo(inPortTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		// 外网登录名称提示信息:不能超过122个字符或61个汉字
		info = JecnUserCheckUtil.checkNameLength(outLoginNameField.getText());
		exsitsError = mailPanel.setErrorInfo(outLoginNameTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		// 外网端口号验证
		info = getNumCheckInfo(outPortField.getText());
		exsitsError = mailPanel.setErrorInfo(outPortTextArea, info);
		if (exsitsError) {
			return exsitsError;
		}
		return exsitsError;
	}

	private String getNumCheckInfo(String strValidity) {
		// 端口号验证
		String reg = "^[0-9]*$";
		String tipStr = "";
		if (!strValidity.matches(reg)) {
			tipStr = JecnProperties.getValue("emailPortIsNum");
		} else {
			tipStr = "";
		}
		return tipStr;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == inIsUseSSLBox) { // 启用内网SSL ：1：选中；0不选中
				if (inIsUseSSLBox.isSelected()) {// 选中
					inIsUseSSLBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					// 启用匿名发送
					inIsUseAnonymity.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					inIsUseAnonymity.setSelected(false);
					inLoginNameField.setEditable(true);
					inPasswordField.setEditable(true);
				} else {
					inIsUseSSLBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			} else if (e.getSource() == inIsUseAnonymity) {// 内网启用匿名发送：1：选中；0不选中
				if (inIsUseAnonymity.getItemBean() == null) {
					return;
				}
				if (inIsUseAnonymity.isSelected()) {// 选中
					inIsUseAnonymity.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					inLoginNameField.setEditable(false);
					inPasswordField.setEditable(false);
					// 启用SSL
					inIsUseSSLBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					inIsUseSSLBox.setSelected(false);
				} else {
					inIsUseAnonymity.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					inLoginNameField.setEditable(true);
					inPasswordField.setEditable(true);
				}
			} else if (e.getSource() == outIsUseSSLBox) {// 内网启用SSL： 1：选中；0不选中
				if (outIsUseSSLBox.getItemBean() == null) {
					return;
				}
				if (outIsUseSSLBox.isSelected()) {// 选中
					outIsUseSSLBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					// 启用匿名发送
					outIsUseAnonymity.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					outIsUseAnonymity.setSelected(false);
					outLoginNameField.setEditable(true);
					outPasswordField.setEditable(true);
				} else {
					outIsUseSSLBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			} else if (e.getSource() == outIsUseAnonymity) {// 外网启用匿名发送：
				// 1：选中；0不选中
				if (outIsUseAnonymity.getItemBean() == null) {
					return;
				}
				if (outIsUseAnonymity.isSelected()) {// 选中
					outIsUseAnonymity.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					outLoginNameField.setEditable(false);
					outPasswordField.setEditable(false);
					// 启用SSL
					outIsUseSSLBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					outIsUseSSLBox.setSelected(false);
				} else {
					outIsUseAnonymity.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					outLoginNameField.setEditable(true);
					outPasswordField.setEditable(true);
				}
			} else if (e.getSource() == inIsUseTLSBox) {// 内网启用tls： 1：选中；0不选中
				if (inIsUseTLSBox.getItemBean() == null) {
					return;
				}
				if (inIsUseTLSBox.isSelected()) {// 选中
					inIsUseTLSBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					outIsUseSSLBox.setSelected(false);
				} else {
					inIsUseTLSBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			} else if (e.getSource() == outIsUseTLSBox) {// 外网启用tls： 1：选中；0不选中
				if (outIsUseTLSBox.getItemBean() == null) {
					return;
				}
				if (outIsUseTLSBox.isSelected()) {// 选中
					outIsUseTLSBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					outIsUseTLSBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}
		}
	}

}
