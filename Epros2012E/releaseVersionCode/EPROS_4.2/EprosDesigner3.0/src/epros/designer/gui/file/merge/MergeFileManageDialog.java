package epros.designer.gui.file.merge;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.table.CheckBoxRenderer;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 
 * 合并文件管理
 * 
 * @author 2012-05-28
 * 
 */
public class MergeFileManageDialog extends JecnDialog implements ActionListener {
	private static Logger log = Logger.getLogger(MergeFileManageDialog.class);

	/** 主面板 */
	private JPanel mainPanel = null;
	/** 树滚动面板+快速查询面板的容器· */
	private JecnSplitPane splitPane = null;
	/** 树面板 */
	private JScrollPane treeScrollPanel = null;
	/** 右侧面板 */
	private JPanel centerPanel = null;
	/** 搜索面板 */
	private JPanel searchPanel = null;
	/** 结果面板 */
	private JPanel resultPanel = null;
	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = null;
	/** Dialog的关闭按钮在此面板上 */
	private JPanel colsePanel = null;
	/** 搜索名称 */
	private JLabel searchLabel = null;
	/** 搜索内容 */
	private JTextField searchText = null;
	/** 关闭按钮 */
	private JButton closeButton = null;
	/** 按钮面板，上面放四个按钮 */
	private JPanel buttonsPanel = null;
	/** 打开 */
	private JButton openBtn = null;
	/** 合并 */
	private JButton mergeBtn = null;
	/** 删除 */
	private JButton deleteBtn = null;
	private JButton addBtn = new JButton("增加文件");
	/** 搜索的结果 */
	private List<JecnFileBeanT> searchList = new ArrayList<JecnFileBeanT>();
	/** 搜索的jecnTreeBean结果 */
	private List<JecnTreeBean> searchJecnTreeBeanList;
	private JCheckBox selectBox = null;
	private FileManageTable fileTable = null;
	private JecnTree jTree = null;
	private JLabel tipLable = new JLabel();

	public MergeFileManageDialog() {
		this.setTitle("合并管理");
		initCompotents();
		initLayout();
		buttonInit();
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:初始化组件
	 */
	public void initCompotents() {
		tipLable.setForeground(Color.red);
		// 主面板
		mainPanel = new JPanel();
		// 树面板
		treeScrollPanel = new JScrollPane();
		// 右侧面板
		centerPanel = new JPanel();
		// 树滚动面板+快速查询面板的容器
		splitPane = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, mainPanel, treeScrollPanel, centerPanel);
		// 搜索面板
		searchPanel = new JPanel();
		// 结果面板
		resultPanel = new JPanel();
		// 结果滚动面板
		resultScrollPane = new JScrollPane();
		// 关闭按钮面板
		colsePanel = new JPanel();

		// 按钮面板
		buttonsPanel = new JPanel();
		// 搜索名称
		searchLabel = new JLabel(JecnProperties.getValue("fileNameC"));
		// 搜索内容
		searchText = new JTextField(80);

		// 打开
		openBtn = new JButton(JecnProperties.getValue("openBtn"));
		mergeBtn = new JButton("合并");
		// 删除
		deleteBtn = new JButton(JecnProperties.getValue("deleteBtn"));
		deleteBtn.setVisible(false);

		// 关闭按钮
		closeButton = new JButton(JecnProperties.getValue("closeBtn"));

		selectBox = new JCheckBox("全选/反选");

		// 把tree加入滚动面板
		jTree = initJecnTree();
		// 快速查询table
		fileTable = new FileManageTable();
		// 树滚动面板
		treeScrollPanel.setViewportView(jTree);
		fileTable.setOpaque(false);
		resultScrollPane.setViewportView(fileTable);

		// 大小
		this.setSize(810, 510);
		// 模态
		this.setModal(true);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);

		// 主面板布局-开始
		mainPanel.setLayout(new BorderLayout());
		// 快速查询面板
		centerPanel.setLayout(new BorderLayout(0, 2));
		// 结果面板布局-开始
		resultPanel.setLayout(new BorderLayout());
		// 搜索面板布局,流布局 靠左
		searchPanel.setLayout(new BorderLayout());
		// 关闭面板布局
		colsePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// 快速查询面板中按钮面板
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		// 树滚动面板大小
		Dimension size = new Dimension(200, 430);
		treeScrollPanel.setPreferredSize(size);
		treeScrollPanel.setMinimumSize(size);

		// 设置各面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treeScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		searchPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		colsePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 按钮命令标识
		// 打开
		openBtn.setActionCommand("openBtn");
		// 更新
		mergeBtn.setActionCommand("updateBtn");
		// 删除
		deleteBtn.setActionCommand("deleteBtn");
		// 关闭按钮
		closeButton.setActionCommand("closeBtn");
		// 双击搜索结果Table
		fileTable.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent evt) {
				fileTablemouseReleased(evt);
			}
		});

		selectBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				selectAction(e);
			}
		});

		// 边框
		// 树滚动面板
		treeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("StandardDocument")));
		treeScrollPanel.setVisible(false);
		// 快速查询面板
		centerPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("quickSearch")));
		// 左:右=3:7
		splitPane.setDividerLocation(0.3);
		// 当窗体变大时，右边面板获取空间,左面板不变
		splitPane.setResizeWeight(0.0);
		splitPane.showBothPanel();
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	@SuppressWarnings("serial")
	class CheckButtonEditor extends DefaultCellEditor implements ItemListener {
		private JCheckBox button;

		public CheckButtonEditor(JCheckBox checkBox) {
			super(checkBox);
		}

		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			if (value == null)
				return null;
			button = (JCheckBox) value;
			button.addItemListener(this);
			return (Component) value;
		}

		public Object getCellEditorValue() {
			button.removeItemListener(this);
			return button;
		}

		public void itemStateChanged(ItemEvent e) {
			super.fireEditingStopped();
		}
	}

	protected void selectAction(ItemEvent e) {
		boolean selected = selectBox.isSelected();
		int rowcount = fileTable.getRowCount();
		if (rowcount > 0) {
			for (int i = 0; i < rowcount; i++) {
				JCheckBox box = (JCheckBox) fileTable.getModel().getValueAt(i, 7);
				box.setSelected(selected);
			}
			fileTable.repaint();
		}
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:dialog布局方法
	 */
	public void initLayout() {
		// **************第一层布局**************//
		// 内容区
		mainPanel.add(splitPane, BorderLayout.CENTER);
		// 关闭按钮
		mainPanel.add(colsePanel, BorderLayout.SOUTH);
		// **************第一层布局**************//

		// **************快速查询布局**************//
		// 搜索名称
		searchPanel.add(searchLabel, BorderLayout.WEST);
		// 搜索内容
		searchPanel.add(searchText, BorderLayout.CENTER);

		buttonsPanel.add(tipLable);
		buttonsPanel.add(selectBox);
		buttonsPanel.add(openBtn);
		buttonsPanel.add(mergeBtn);
		buttonsPanel.add(deleteBtn);

		resultPanel.add(resultScrollPane, BorderLayout.CENTER);
		resultPanel.add(buttonsPanel, BorderLayout.SOUTH);

		centerPanel.add(searchPanel, BorderLayout.NORTH);
		centerPanel.add(resultPanel, BorderLayout.CENTER);
		// **************快速查询布局**************//

		// 结果面板布局-结束
		colsePanel.add(closeButton);

		// 把主面板增加到dialog
		this.getContentPane().add(mainPanel);
	}

	/**
	 * 
	 * 按钮点击事件
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if ("openBtn".equals(btn.getActionCommand())) {// 打开
				openBtnAction();
			} else if ("deleteBtn".equals(btn.getActionCommand())) {
				delAction();
			} else if ("closeBtn".equals(btn.getActionCommand())) { // 关闭
				closeDialog();
			}
		}
	}

	/**
	 * 双击搜索结果Table,实现搜索定位
	 * 
	 * @param evt
	 */
	private void fileTablemouseReleased(MouseEvent evt) {
		if (evt.getClickCount() == 2) {
			int index = fileTable.getSelectedRow();
			if (searchJecnTreeBeanList == null || searchJecnTreeBeanList.size() == 0 || index < 0
					|| index >= searchJecnTreeBeanList.size()) {
				return;
			}
			((JecnHighEfficiencyTree) jTree).setAllowExpand(false);
			JecnTreeBean jecnTreeBean = searchJecnTreeBeanList.get(fileTable.getSelectedRow());
			if (TreeNodeType.position.equals(jecnTreeBean.getTreeNodeType())) {
				try {
					JecnTreeCommon.searchNodePos(jecnTreeBean, ((JecnHighEfficiencyTree) jTree));
				} catch (Exception e) {
					log.error("FileManageDialog fileTablemouseReleased is error", e);
				}
			} else {
				try {
					JecnTreeCommon.searchNode(jecnTreeBean, ((JecnHighEfficiencyTree) jTree));
				} catch (Exception e) {
					log.error("FileManageDialog fileTablemouseReleased is error", e);
				}
			}
			((JecnHighEfficiencyTree) jTree).setAllowExpand(true);
		}
	}

	// 为按钮增加事件
	private void buttonInit() {
		// 打开
		openBtn.addActionListener(this);
		mergeBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mergeAction();
			}
		});
		// 删除
		deleteBtn.addActionListener(this);
		// 关闭
		closeButton.addActionListener(this);

		searchText.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				searchTextAction();
			}

			public void insertUpdate(DocumentEvent e) {
				searchTextAction();
			}

			public void changedUpdate(DocumentEvent e) {
			}
		});
	}

	protected void mergeAction() {
		int rowCount = fileTable.getRowCount();
		if (rowCount == 0) {
			pleaseSelect();
			return;
		}
		List<String[]> s = new ArrayList<String[]>();
		for (int i = 0; i < rowCount; i++) {
			JCheckBox box = (JCheckBox) fileTable.getModel().getValueAt(i, 7);
			if (box.isSelected()) {
				s.add(new String[] { JecnUtil.nullToEmpty(fileTable.getModel().getValueAt(i, 0)),
						JecnUtil.nullToEmpty(fileTable.getModel().getValueAt(i, 1)),
						JecnUtil.nullToEmpty(fileTable.getModel().getValueAt(i, 2)) });
			}
		}
		if (s.size() == 0) {
			pleaseSelect();
			return;
		}
//		MergeFileDialog d = new MergeFileDialog(s, this);
//		d.setVisible(true);
	}

	private void clearTip() {
		this.tipLable.setText("");
	}

	private void pleaseSelect() {
		JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
	}

	public void delAction() {
		// 提示选择一行进行删除
		int[] selectRows = fileTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		}
		// 删除table选中的数据 和树节点上的数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) fileTable.getModel()).removeRow(selectRows[i]);
		}
	}

	/**
	 * 搜索
	 * 
	 * @param evt
	 */
	private void searchTextAction() {
		clearTip();
		String name = searchText.getText().trim();
		if (!"".equals(name)) {
			searchList = searchByName(name);
			// 清空表格数据
			for (int index = fileTable.getModel().getRowCount() - 1; index >= 0; index--) {
				((DefaultTableModel) fileTable.getModel()).removeRow(index);
			}
			for (Vector<Object> v : updateTableContent(searchList)) {
				((DefaultTableModel) fileTable.getModel()).addRow(v);
			}
		} else {
			searchList.clear();
			// 清空
			for (int index = fileTable.getModel().getRowCount() - 1; index >= 0; index--) {
				((DefaultTableModel) fileTable.getModel()).removeRow(index);
			}
		}
	}

	private void closeDialog() {
		this.dispose();
	}

	/**
	 * 打开文件
	 */
	public void openBtnAction() {
		// 判断是否选中Table中的一行，没选中，提示选中一行
		int[] selectRows = this.fileTable.getSelectedRows();
		if (selectRows.length == 1) {
			JecnDesignerCommon.openFile(Long.valueOf(fileTable.getValueAt(selectRows[0], 0).toString()));
		} else {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		}
	}

	public JecnTree initJecnTree() {
		// return new HighEfficiencyFileTree(this); // null;//
		return null;
	}

	public List<JecnFileBeanT> searchByName(String name) {
		selectBox.setSelected(false);
		try {
			List<JecnFileBeanT> list = ConnectionPool.getFileAction()
					.getMergeFilesByName(name, JecnConstants.projectId);
			if (list.size() > 20) {
				list = list.subList(0, 21);
				tipLable.setText("搜索结果超出20个请缩小范围");
			}
			searchJecnTreeBeanList = new ArrayList<JecnTreeBean>();
			for (JecnFileBeanT fileBean : list) {
				JecnTreeBean treeBean = new JecnTreeBean();
				treeBean.setId(fileBean.getFileID());
				treeBean.setName(fileBean.getFileName());
				treeBean.setTreeNodeType(TreeNodeType.file);
				searchJecnTreeBeanList.add(treeBean);
			}
			return list;
		} catch (Exception e) {
			log.error("FileManageDialog FileManageDialog is error", e);
			return new ArrayList<JecnFileBeanT>();
		}
	}

	public Vector<Vector<Object>> updateTableContent(List<JecnFileBeanT> list) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		SimpleDateFormat newDate = new SimpleDateFormat("yyyy-MM-dd");
		Set<String> fileLastNames = getCommonFilesEnd();
		for (JecnFileBeanT jecnFileBean : list) {
			Vector<Object> data = new Vector<Object>();
			data.add(jecnFileBean.getFileID());
			// 文件编号
			data.add(jecnFileBean.getDocId());
			data.add(jecnFileBean.getFileName());
			data.add(jecnFileBean.getOrgName());
			// 创建时间
			data.add(newDate.format(jecnFileBean.getCreateTime()));
			// 修改时间
			data.add(newDate.format(jecnFileBean.getUpdateTime()));
			// 文件格式后缀
			String lastFileNameStr = null;
			if (fileLastNames.contains(JecnUtil.getFileExtension(jecnFileBean.getFileName()).toString())) {
				lastFileNameStr = JecnUtil.getFileExtension(jecnFileBean.getFileName()).toString();
			} else {
				lastFileNameStr = "elsefile";
			}
			// 文件格式
			data.add(new ImageIcon("images/fileImage/" + lastFileNameStr + ".gif"));
			data.add(new JCheckBox());
			vector.add(data);
		}
		return vector;
	}

	public Set<String> getCommonFilesEnd() {
		Set<String> fileLastNames = new HashSet<String>();
		fileLastNames.add("bmp");
		fileLastNames.add("doc");
		fileLastNames.add("docx");
		fileLastNames.add("gif");
		fileLastNames.add("jpg");
		fileLastNames.add("pdf");
		fileLastNames.add("png");
		fileLastNames.add("ppt");
		fileLastNames.add("pptx");
		fileLastNames.add("txt");
		fileLastNames.add("vsd");
		fileLastNames.add("xls");
		fileLastNames.add("xlsx");
		fileLastNames.add("epros");
		return fileLastNames;
	}

	@SuppressWarnings("serial")
	class FileManageTable extends JTable {
		public FileManageTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			TableColumnModel columnModel = this.getColumnModel();
			if (cols != null && cols.length > 0) {
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI(SwingConstants.CENTER));
			this.getTableHeader().setReorderingAllowed(false);
			JCheckBox box = new JCheckBox();
			columnModel.getColumn(7).setCellEditor(new CheckButtonEditor(box));
			columnModel.getColumn(7).setCellRenderer(new CheckBoxRenderer());

			columnModel.getColumn(7).setMaxWidth(40);
			columnModel.getColumn(6).setMaxWidth(40);
			columnModel.getColumn(5).setMaxWidth(80);
			columnModel.getColumn(4).setMaxWidth(80);
			columnModel.getColumn(3).setMaxWidth(120);
			columnModel.getColumn(1).setMaxWidth(120);
		}

		public FileManageTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("fileNum"));
			title.add(JecnProperties.getValue("name"));
			title.add(JecnProperties.getValue("responsibleDepart"));
			title.add(JecnProperties.getValue("createTime"));
			title.add(JecnProperties.getValue("updateTime"));
			title.add(JecnProperties.getValue("format"));
			title.add("合并");
			return new FileManageTableMode(updateTableContent(searchList), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {

			return new int[] { 0 };
		}

		class FileManageTableMode extends DefaultTableModel {
			public FileManageTableMode(Vector<Vector<Object>> data, Vector<String> title) {
				super(data, title);
			}

			@Override
			public Class getColumnClass(int columnIndex) {
				if (columnIndex == 6) {
					return ImageIcon.class;
				}
				return super.getColumnClass(columnIndex);
			}

			public boolean isCellEditable(int rowindex, int colindex) {
				if (colindex == 7) {
					return true;
				}
				return false;
			}

		}

	}
}
