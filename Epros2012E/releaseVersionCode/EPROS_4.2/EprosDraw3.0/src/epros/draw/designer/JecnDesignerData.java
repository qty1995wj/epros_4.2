package epros.draw.designer;

import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseLineData;

public class JecnDesignerData {
	/** 图形对象集合 （设计器专用） */
	private List<JecnFigureData> oldFigureList = null;
	/** 线对象集合 （设计器专用） */
	private List<JecnBaseLineData> oldLineList = null;
	
	/**流程模板类型*/
	private ModeType  modeType=ModeType.none;

	/**
	 * 图形对象集合 （设计器专用）
	 * 
	 * @return
	 */
	public List<JecnFigureData> getOldFigureList() {
		if (oldFigureList == null) {
			oldFigureList = new ArrayList<JecnFigureData>();
		}
		return oldFigureList;
	}

	/**
	 * 线对象集合（设计器专用）
	 * 
	 * @return
	 */
	public List<JecnBaseLineData> getOldLineList() {
		if (oldLineList == null) {
			oldLineList = new ArrayList<JecnBaseLineData>();
		}
		return oldLineList;
	}
	
	public ModeType getModeType() {
		return modeType;
	}

	public void setModeType(ModeType modeType) {
		this.modeType = modeType;
	}

}
