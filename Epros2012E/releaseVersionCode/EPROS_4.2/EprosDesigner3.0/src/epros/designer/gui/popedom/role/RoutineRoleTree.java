package epros.designer.gui.popedom.role;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class RoutineRoleTree extends JecnRoutineTree {
	private Logger log = Logger.getLogger(RoutineRoleTree.class);

	/** 所有节点 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/** 默认节点 */
	private List<JecnTreeBean> listDefault = new ArrayList<JecnTreeBean>();

	/** 角色对话框 */
	private RoleManageDialog jecnManageDialog = null;

	public RoutineRoleTree(RoleManageDialog jecnManageDialog) {
		this.jecnManageDialog = jecnManageDialog;
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			listDefault = JecnRoleCommon.getDefaultRoleTreeBeans();
			list = ConnectionPool.getJecnRole().getAllRoles(JecnConstants.projectId);
		} catch (Exception e) {
			log.error("RoutineRoleTree getTreeModel is error", e);
		}
		return JecnRoleCommon.getTreeModel(listDefault, list);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		JecnRoleCommon.treeMousePressed(evt, this, jecnManageDialog);
	}

}
