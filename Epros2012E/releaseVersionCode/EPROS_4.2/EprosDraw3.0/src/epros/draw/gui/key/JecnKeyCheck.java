package epros.draw.gui.key;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.JecnMainJFrame;
import epros.draw.util.DrawCommon;

/**
 * 
 * 密钥验证
 * 
 * @author ZHOUXY
 * 
 */
public class JecnKeyCheck {
	private final Log log = LogFactory.getLog(JecnMainJFrame.class);

	private JecnKeyProces keyProces = null;

	public JecnKeyCheck() {
		init();
	}

	private void init() {
		keyProces = new JecnKeyProces();
	}

	/**
	 * 
	 * 对外接口
	 * 
	 * 验证密钥
	 * 
	 */
	public void process() {
		checkKey();
	}

	/**
	 * 
	 * 密钥验证，返回验证信息
	 * 
	 * @return String 验证信息或NULL
	 */
	String checkKeyString() {
		return keyProces.checkKey();
	}

	/**
	 * 
	 * 执行验证
	 * 
	 */
	private void checkKey() {
		// 密钥验证
		try {
			if (!checkKeyProcess()) {
				log.info("JecnKeyCheck类checkKey方法系统退出。");
				System.exit(0);
			}
		} catch (Exception ex) {// 此try必须要，防止密钥验证失败还能进入系统
			// 正常情况不走此分支
			log.error("JecnKeyCheck类密钥验证异常。", ex);
			System.exit(0);
		}
	}

	/**
	 * 
	 * 验证密钥是否通过：true通过；false不通过
	 * 
	 * @return boolean true通过；false不通过
	 */
	private boolean checkKeyProcess() {
		// 密钥验证：NULL成功
		String info = keyProces.checkKey();

		if (DrawCommon.isNullOrEmtryTrim(info)) {
			// 提示用户信息
			String tipInfo = keyProces.checkKeyTipUser();
			if (!DrawCommon.isNullOrEmtryTrim(tipInfo)) {
				// JecnMainJFrame.getMainJFrame().getInfoLabel().setText(tipInfo);
				JecnOptionPane.showMessageDialog(JecnDrawMainPanel
						.getMainPanel(), tipInfo, null,
						JOptionPane.INFORMATION_MESSAGE);
			}
			return true;
		} else {
			JecnKeyDialog keyDialog = new JecnKeyDialog();
			keyDialog.setInfoLabelText(info);
			keyDialog.setVisible(true);

			if (keyDialog.isOk()) {// 标识重新上传密钥后验证成功
				return true;
			}
			return false;
		}
	}
}
