package epros.draw.gui.designer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JPanel;
import javax.swing.SwingWorker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.figure.shape.IconFigure;
import epros.draw.gui.operationConfig.DrawOperationDescriptionConfigBean;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.JecnFlowExport;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnDefaultFlowElementData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 
 * 操作画图工具入口，实现设计器功能类
 * 
 * 比如点击画图工具保存按钮实现设计器保存功能，即把画图面板中数据保存到数据库
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDesignerProcess {
	private final Log log = LogFactory.getLog(JecnDesignerProcess.class);
	/** 保存流程图 */
	private JecnIDesigner designer = null;

	/** JecnDesignerProcess对象 */
	private final static JecnDesignerProcess designerProcess = new JecnDesignerProcess();

	private JecnDesignerProcess() {

	}

	public void setDesigner(JecnIDesigner designer) {
		this.designer = designer;
	}

	public JecnIDesigner getDesigner() {
		return designer;
	}

	/**
	 * 
	 * JecnDesignerProcess对象
	 * 
	 * @return JecnDesignerProcess
	 */
	public static JecnDesignerProcess getDesignerProcess() {
		return designerProcess;
	}

	/**
	 * 
	 * 画图面板右键菜单
	 * 
	 */
	public void beforeWorkflowPopupShow() {
		if (designer != null) {
			designer.beforeWorkflowPopupShow();
		}
	}

	/**
	 * 
	 * 
	 * 元素右键菜单
	 * 
	 */
	public void beforeFlowElemPopupShow(JecnBaseFlowElementPanel flowElementPanel) {
		if (designer != null) {
			designer.beforeFlowElemPopupShow(flowElementPanel);
		}
	}

	/**
	 * 多选
	 */
	public void beforeFlowElemMutilsPopupShow() {
		if (designer != null) {
			designer.beforeFlowElemMutilsPopupShow();
		}
	}

	/**
	 * 
	 * 显示活动明细
	 * 
	 */
	public void showActiveDetailsDialog(JecnBaseActiveFigure activeFigure) {
		if (designer != null) {
			designer.activeDetailsDialog(activeFigure);
		}
	}

	/**
	 * 
	 * 获取当前流程元素属性值
	 * 
	 * @return List<JecnDefaultFlowElementData> 当前流程元素属性值
	 */
	public List<JecnDefaultFlowElementData> readCerrFlowElementDataList() {
		if (designer != null) {
			return designer.readCerrFlowElementDataList();
		}
		return null;
	}

	/**
	 * 
	 * 获取系统默认流程元素属性值
	 * 
	 * @return List<JecnDefaultFlowElementData> 系统默认流程元素属性值
	 */
	public List<JecnDefaultFlowElementData> readSystemDefaultFlowElementDataList() {
		if (designer != null) {
			return designer.readSystemDefaultFlowElementDataList();
		}
		return null;
	}

	/**
	 * 
	 * 修改当前流程元素属性值
	 * 
	 * @param dataList
	 *            List<JecnDefaultFlowElementData> 当前流程元素属性值
	 * @return boolean 更新成功：true 更新失败或无更新：false
	 */
	public boolean writeCurrFlowElementDataList(List<JecnDefaultFlowElementData> dataList) {
		if (designer != null) {
			return designer.writeCurrFlowElementDataList(dataList);
		}
		return false;
	}

	/**
	 * 
	 * 执行保存画图面板
	 * 
	 */
	public boolean processSaveMap() {
		String s = null;
		if (designer != null) {
			WorkFlowSaveTask task = new WorkFlowSaveTask();
			String error = JecnLoading.start(task);
			if (error != null) {
				return false;
			}
			try {
				s = task.get();
				if ("1".equals(s)) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
							.getJecnResourceUtil().getValue("saveAbnormalFlowChart"));
				} else if ("2".equals(s)) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
							.getJecnResourceUtil().getValue("saveTheFlowChartOfRefreshAnomaly"));
				} else if ("3".equals(s)) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), "文件已废止！");
				}
			} catch (InterruptedException e) {
				log.error("", e);
			} catch (ExecutionException e) {
				log.error("", e);
			}
			return s == null ? true : false;
		}
		log.error(s);
		return false;
	}

	/**
	 * @author yxw 2012-8-10
	 * @description：面板保存进度控制线程
	 */
	class WorkFlowSaveTask extends SwingWorker<String, Void> {
		@Override
		protected String doInBackground() throws Exception {
			String s = null;
			if (designer != null) {
				// s为null时保存正确 s不为null时保存失败
				s = designer.saveMap();
			}
			return s;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	/**
	 * 
	 * 执行新建流程图
	 * 
	 */
	public void processNewPartMap() {
		if (designer != null) {
			designer.newPartMap();
		}
	}

	/**
	 * 
	 * 执行新建流程地图
	 * 
	 */
	public void processNewTotalMap() {
		if (designer != null) {
			designer.newTotalMap();
		}
	}

	public void processAddGenFlowElem() {
		if (designer != null) {
			designer.addGenFlowElem();
		}
	}

	/**
	 * @description:保存模型
	 */
	public void saveTemplate() {
		if (designer != null) {
			designer.saveTemplate();
		}
	}

	/**
	 * @description:删除模型
	 */
	public void deleteTemplate() {
		if (designer != null) {
			designer.deleteTemplate();
		}
	}

	/**
	 * @description:修改模型
	 */
	public void updateTemplate() {
		if (designer != null) {
			designer.updateTemplate();
		}
	}

	/**
	 * @description:获取模型数据
	 */
	public void getJecnProcessTemplateBeanList() {
		if (designer != null) {
			designer.getJecnProcessTemplateBeanList();
		}
	}

	/**
	 * @author yxw 2012-8-15
	 * @description:活动明细
	 */
	public void activeDetail() {
		if (designer != null) {
			designer.activeDetail();
		}
	}

	/**
	 * @author yxw 2012-8-16
	 * @description:切换面板
	 * @param id
	 *            面板ID
	 * @param mapType
	 *            面板类型
	 * @param modeType
	 *            模板类型
	 */
	public void swicthWorkFlow(long id, MapType mapType, ModeType modeType) {
		if (designer != null) {
			designer.swicthWorkFlow(id, mapType, modeType);
		}
	}

	/**
	 * 获取操作说明配置
	 * 
	 * @author fuzhh Oct 10, 2012
	 * @param id
	 * @param mapType
	 */
	public List<DrawOperationDescriptionConfigBean> getOperConfigList() {
		if (designer != null) {
			return designer.operConfigList();
		}
		return null;
	}

	/**
	 * 
	 * 是否能操作流程设计向导
	 * 
	 * @param ispartMap
	 *            boolean true：流程图 ；false：不是流程图
	 */
	public void isEditFlowDesignGuide(MapType mapType) {
		if (designer != null) {
			designer.isEditFlowDesignGuide(mapType);
		}
	}

	/**
	 * @author yxw 2012-10-22
	 * @description:定时器重新启动
	 */
	public void timerRestart() {
		if (designer != null) {
			designer.timerRestart();
		}
	}

	/**
	 * @author yxw 2012-10-22
	 * @description:退出
	 */
	public void loginOut() {
		if (designer != null) {
			designer.loginOut();
		}
	}

	/**
	 * 
	 * 添加连接到角色移动上的图形
	 * 
	 * @param figure
	 *            JecnBaseRoleFigure
	 */
	public void addLinkToroleMobile(JecnBaseRoleFigure figure) {
		if (designer != null) {
			designer.addLinkToroleMobile(figure);
		}
	}

	/**
	 * 
	 * 保存面板
	 * 
	 * @return boolean true：成功；false：失败
	 * 
	 */
	public boolean workFlowClose(JecnDrawDesktopPane workflow) {
		if (designer != null) {
			return designer.workFlowClose(workflow);
		}
		return false;
	}

	/**
	 * 
	 * 获取版本类型
	 * 
	 * @return boolean true： 完整版 false：标准版
	 */
	public boolean isVersionType() {
		if (designer != null) {
			return designer.isVersionType();
		}
		return false;
	}

	/**
	 * @author yxw 2014年5月9日
	 * @description:0(p1)：有浏览端标准版（默认）；99(p2)：有浏览端完整版；1(D2)：无浏览端标准版；100(D3)：无浏览端完整版 
	 *                                                                             1
	 *                                                                             或100返回false
	 * @return
	 */
	public boolean isPubShow() {
		if (designer != null) {
			return designer.isPubShow();
		}
		return false;
	}

	/**
	 * 图标插入框 图片读取
	 * 
	 * @author fuzhh 2013-7-25
	 * @param id
	 *            图片ID
	 * @param iconFigure
	 *            图标插入框
	 */
	public void openFile(long id, IconFigure iconFigure) {
		if (designer != null) {
			designer.openFile(id, iconFigure);
		}
	}

	/**
	 * 
	 * 点击画图面板标题面板，显示/隐藏资源管理面板
	 * 
	 * @param e
	 */
	public void addTabbedPaneMouseLister(MouseEvent e) {
		if (designer != null && e != null) {
			designer.doubleCheckTabbedPanel(e);
		}
	}

	/***
	 * 系统配置---基本信息下的公司名称及公司logo
	 * 
	 * @return
	 */
	public JPanel getConfigFileConPanyPanel() {
		if (designer != null) {
			return designer.getConfigFileConPanyPanel();
		}
		return null;
	}

	/**
	 * 
	 * 检查权限，是否让操作
	 * 
	 * @return boolean true：有权限操作；false：没有权限操作
	 */
	public boolean checkAuth() {
		if (designer != null) {
			return designer.checkAuth();
		}
		return true;
	}

	public List<MapElemType> getShowElementsType(int type) {
		if (designer == null) {
			return null;
		}
		return designer.getShowElementsType(type);
	}

	public void updateElementsType(List<MapElemType> mapElemTypes, int type) {
		if (designer == null) {
			return;
		}
		designer.updateElementsType(mapElemTypes, type);
	}

	public List<MapElemType> getAllElementsType(int type) {
		if (designer == null) {
			return null;
		}
		return designer.getAllElementsType(type);
	}

	public boolean isAdmin() {
		if (designer == null) {
			return true;
		}
		return designer.isAdmin();
	}

	public boolean isDraw() {
		return designer == null ? true : false;
	}

	/**
	 * 流程编号是否和流程名称一起显示
	 * 
	 * @return
	 */
	public boolean isNumShow() {
		if (designer == null) {
			return false;
		}
		return designer.isNumShow();
	}

	public String getFlowIdInput(Long flowId) {
		if (designer == null) {
			return "";
		}
		return designer.getFlowIdInput(flowId);
	}

	public void activeAutoNums() {
		designer.activeAutoNums();
	}

	public boolean isSameNumberInDottedRect() {
		if (designer == null) {
			return false;
		}
		return designer.isSameNumberInDottedRect();
	}

	/**
	 * 流程架构图连接线
	 * 
	 * @return
	 */
	public boolean isShowLineHasNoFigure() {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getMapType() != MapType.totalMap
				&& JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getMapType() != MapType.totalMapRelation) {
			return false;
		}
		if (designer == null) {// 默认画图工具
			return true;
		}
		return designer.isShowLineHasNoFigure();
	}

	/**
	 * true：启用新版活动和角色（勘探院配置）
	 * 
	 * @return
	 */
	public boolean isUserActiveByItem() {
		if (designer == null) {
			return true;
		}
		return designer.isUserActiveByItem();
	}

	public boolean isSecondAdmin() {
		if (designer == null) {
			return false;
		}
		return designer.isSecondAdmin();
	}

	/**
	 * 是否有编辑流程地图的权限
	 * 
	 * @return
	 */
	public boolean isEditFlowMap() {
		if (designer == null) {
			return false;
		}
		return designer.isEditFlowMap();
	}

	public void addElemLink(JecnBaseFigurePanel elementPanel, MapType mapType) {
		if (designer == null) {
			return;
		}
		designer.addElemLink(elementPanel, mapType);
	}

	/**
	 * 科大讯飞- 隐藏修改元素属性操作
	 * 
	 * @return
	 */
	public boolean isHiddenEditToolBarAttrs() {
		if (designer == null) {
			return false;
		}
		return designer.isHiddenEditToolBarAttrs();
	}

	/**
	 * 隐藏 活动编号类型 弹出框
	 * 
	 * @return
	 */
	public boolean isHiddenActiveType() {
		if (designer == null) {
			return false;
		}
		return designer.isActivityNumTypeShow();
	}

	/**
	 * 隐藏客户属性
	 * 
	 * @return
	 */
	public boolean isShowProcessCustomer() {
		if (designer == null) {
			return false;
		}
		return designer.isShowProcessCustomer();
	}

	/**
	 * 隐藏新建元素
	 * 
	 * @return
	 */
	public boolean isShowBeginningEndElement() {
		if (designer == null) {
			return false;
		}
		return designer.isShowBeginningEndElement();
	}

	/**
	 * 获取是否 启用新版本输入输出
	 * 
	 * @return
	 */
	public boolean useNewInout() {
		if (designer == null) {
			return false;
		}
		return designer.useNewInout();
	}

	public void editButAction(ActionEvent e) {
		if (designer == null) {
			return;
		}
		designer.editButAction(e);
	}

	public boolean isShowWorkflowEdit() {
		if (designer == null) {
			return false;
		}
		return designer.isShowWorkflowEdit();
	}

	public Component initEleFigurePanel() {
		if (designer == null) {
			return null;
		}
		return designer.initEleFigurePanel();
	}

	public void showElementAttributePanel() {
		if (designer == null) {
			return;
		}
		designer.showElementAttributePanel();
	}

	public int getModelUpperLimit(int type) {
		if (designer == null) {
			return 0;
		}
		return designer.getModelUpperLimit(type);
	}

	public void updateElementPanel(JecnBaseFigurePanel curFigurePanel) {
		if (designer == null) {
			return;
		}
		designer.updateElementPanel(curFigurePanel);
	}

	public void hiddelElementPanel() {
		if (designer == null) {
			return;
		}
		designer.hiddelElementPanel();
	}

	// 是否显示流程图 背景网格线
	public boolean isShowGrid() {
		if (designer == null) {
			return true;
		}
		return designer.isShowGrid();
	}

	public void setGridState(boolean showGrid) {
		if (designer == null) {
			return;
		}
		designer.setGridState(showGrid);
	}
}
