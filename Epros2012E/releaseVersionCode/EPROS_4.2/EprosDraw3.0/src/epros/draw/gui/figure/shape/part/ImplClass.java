/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 
 * @author Administrator
 */
public class ImplClass extends JecnBaseFigurePanel {
	private String interfaceType = "";

	public ImplClass(JecnFigureData figureData) {
		super(figureData);
	}

	/**
	 * @return the interfaceType
	 */
	public String getInterfaceType() {
		return interfaceType;
	}

	/**
	 * @param interfaceType
	 *            the interfaceType to set
	 */
	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}
}
