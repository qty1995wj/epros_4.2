package epros.draw.system;

import java.util.List;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.system.data.JecnDefaultFlowElementData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;

/**
 * 
 * 获取配置文件流程元素属性值
 * 
 * @author ZHOUXY
 * 
 */
class JecnSystemFlowElementProcess {
	/** 当前设置流程元素属性 */
	private static List<JecnDefaultFlowElementData> cerrFlowElementDataList = null;
	/** 系统指定默认流程元素属性集合 */
	private static List<JecnDefaultFlowElementData> systemDefaultFlowElementDataList = null;

	/**
	 * 
	 * 获取当前设置流程元素属性集合
	 * 
	 * @return List<JecnDefaultFlowElementData> 当前设置流程元素属性集合
	 */
	static List<JecnDefaultFlowElementData> readCerrFlowElementDataList() {
		if (cerrFlowElementDataList == null) {// 获取当前设置流程元素属性
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {// 设计器
				cerrFlowElementDataList = JecnDesignerProcess.getDesignerProcess().readCerrFlowElementDataList();
			} else {
				cerrFlowElementDataList = JecnFlowElementForXML.readCurrentModelByXML();
			}
		}
		return cerrFlowElementDataList;
	}

	/**
	 * 
	 * 根据给定数据写入到本地XML中
	 * 
	 * @return boolean 更新成功：true 更新失败或无更新：false
	 */
	static boolean writeCurrFlowElementDataList() {
		if (cerrFlowElementDataList == null) {// 获取当前设置流程元素属性
			return false;
		}

		boolean ret = false;
		if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {// 设计器
			ret = JecnDesignerProcess.getDesignerProcess().writeCurrFlowElementDataList(cerrFlowElementDataList);
		} else {

			ret = JecnFlowElementForXML.writeCurrentModelByXML(readCerrFlowElementDataList());
		}
		return ret;
	}

	/**
	 * 
	 * 获取系统指定默认流程元素属性集合
	 * 
	 * @return List<JecnDefaultFlowElementData> 当前系统指定默认流程元素属性集合
	 */
	static List<JecnDefaultFlowElementData> readSystemDefaultFlowElementDataList() {
		if (systemDefaultFlowElementDataList == null) {// 获取系统指定默认流程元素属性
			if (EprosType.eprosDesigner == JecnSystemStaticData.getEprosType()) {// 设计器
				systemDefaultFlowElementDataList = JecnDesignerProcess.getDesignerProcess()
						.readSystemDefaultFlowElementDataList();
			} else {
				systemDefaultFlowElementDataList = JecnFlowElementForXML.readSystemInitModelByXML();
			}
		}
		return systemDefaultFlowElementDataList;
	}

	/**
	 * 
	 * 根据给定流程元素类型获取对应的流程元素系统默认属性值
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return JecnDefaultFlowElementData 流程元素系统默认属性值 或NULL
	 */
	static JecnDefaultFlowElementData getSystemDefaultFlowElementData(MapElemType mapElemType) {
		if (!DrawCommon.isFlowElementType(mapElemType)) {// 判断给定参数：空、指针或添加符号三种返回false
			return null;
		}
		// 获取当前设置流程元素属性集合
		List<JecnDefaultFlowElementData> systemDefaultFlowElementDataList = readSystemDefaultFlowElementDataList();

		// 查找给定流程元素类型的流程元素属性值
		for (JecnDefaultFlowElementData data : systemDefaultFlowElementDataList) {
			if (mapElemType == data.getMapElemType()) {
				return data;
			}
		}
		return null;
	}

	/**
	 * 
	 * 根据给定流程元素类型获取对应的流程元素属性值
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return JecnDefaultFlowElementData 流程元素属性值 或NULL
	 */
	static JecnDefaultFlowElementData getCurrFlowElementData(MapElemType mapElemType) {
		if (!DrawCommon.isFlowElementType(mapElemType)) {// 判断给定参数：空、指针或添加符号三种返回false
			return null;
		}
		// 获取当前设置流程元素属性集合
		List<JecnDefaultFlowElementData> cerrFlowElementDataList = readCerrFlowElementDataList();

		// 查找给定流程元素类型的流程元素属性值
		for (JecnDefaultFlowElementData data : cerrFlowElementDataList) {
			if (mapElemType == data.getMapElemType()) {
				return data;
			}
		}
		return null;
	}

	/**
	 * 
	 * 通过本地流程元素属性值，初始化流程元素对象数据
	 * 
	 * @param flowElementData
	 *            JecnAbstractFlowElementData 流程元素数据
	 * 
	 */
	static void initCurrFlowElementData(JecnAbstractFlowElementData flowElementData) {
		if (flowElementData == null) {
			JecnLogConstants.LOG_SYSTEM_DATA
					.error("JecnSystemData类initCurrFlowElementData方法：参数为null或空值。flowElementData=" + flowElementData);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);

		}
		// 流程元素默认数据
		JecnDefaultFlowElementData cerrFlowElementData = getCurrFlowElementData(flowElementData.getMapElemType());
		// 通过流程元素默认数据来初始化流程元素对象数据
		cerrFlowElementData.defaultDataToFlowData(flowElementData, cerrFlowElementData);
	}

	/**
	 * 
	 * 通过系统默认属性值，初始化流程元素对象数据
	 * 
	 * @param flowElementData
	 *            JecnAbstractFlowElementData 流程元素数据
	 * 
	 */
	static void initCurrFlowElementDataBySystemData(JecnAbstractFlowElementData flowElementData) {
		if (flowElementData == null) {
			JecnLogConstants.LOG_SYSTEM_DATA
					.error("JecnSystemData类initCurrFlowElementDataBySystemData方法：参数为null或空值。flowElementData="
							+ flowElementData);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);

		}
		// 流程元素系统默认数据
		JecnDefaultFlowElementData systemDefaultFlowElementData = getSystemDefaultFlowElementData(flowElementData
				.getMapElemType());

		// 通过流程元素默认数据来初始化流程元素对象数据
		systemDefaultFlowElementData.defaultDataToFlowData(flowElementData, systemDefaultFlowElementData);
	}

}
