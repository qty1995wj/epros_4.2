package com.jecn.webservice.mengniu.arch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.xml.namespace.QName;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jecn.epros.server.util.JecnUtil;

public class ArchiveService {
	private Logger log = Logger.getLogger(ArchiveService.class);
	protected static String ip = null;
	protected static String appid = null;
	protected static String apptoken = null;
	protected static String comcode = null;
	protected static String ftpName = null;
	protected static String ftpPassword = null;

	static {
		Properties props = new Properties();
		InputStream in = null;
		try {
			in = ArchiveService.class.getResourceAsStream("basic.properties");
			props.load(in);
			ip = props.getProperty("ip");
			appid = props.getProperty("appid");
			apptoken = props.getProperty("apptoken");
			comcode = props.getProperty("comcode");
			ftpName = props.getProperty("ftpName");
			ftpPassword = props.getProperty("ftpPassword");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != in)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		ArchiveService service = new ArchiveService();
		service.test();
	}

	/**
	 * 
	 * @param file
	 * @param fileAbsolutePath
	 * @param fileName
	 *            文件名称需要时一个带后缀的文件
	 * @return
	 * @throws Exception
	 */
	// public boolean send(ArchiveFile file, String fileAbsolutePath, String
	// fileName) throws Exception {
	// String saveFilePath = createSaveFilePath(fileName);
	// // 获得后缀
	// String endFix = getEndFix(fileAbsolutePath).toLowerCase();
	// if (StringUtils.isNotBlank(endFix) &&
	// !saveFilePath.toLowerCase().endsWith(endFix)) {
	// saveFilePath += endFix;
	// }
	// boolean uploadFile = FtpTest.uploadFile(ip, 21, ftpName, ftpPassword,
	// saveFilePath, new FileInputStream(
	// new File(fileAbsolutePath)));
	// if (!uploadFile) {
	// log.error("ftp上传失败:文件名称:" + fileName);
	// return false;
	// }
	// log.info("ftp上传成功:文件名称:" + fileName);
	// file.setAttachment(getAttachmentJsonV3(trimEndFix(fileName),
	// saveFilePath, fileAbsolutePath));
	// ArchiveService service = new ArchiveService();
	// boolean success = service.archive(file);
	// log.info("最终结果：" + success);
	// return success;
	// }

	public boolean send(ArchiveFile file, List<ArchiveF> as) throws Exception {
		boolean uploadFile = uploadToFTP(as);
		if (!uploadFile) {
			return false;
		}
		String attachment = filesToJson(as);
		log.info(attachment);
		file.setAttachment(attachment);
		ArchiveService service = new ArchiveService();
		boolean success = service.archive(file);
		log.info("最终结果：" + success);
		return success;
	}

	private String filesToJson(List<ArchiveF> as) throws Exception {
		if (JecnUtil.isEmpty(as)) {
			return null;
		}
		JSONArray ja = new JSONArray();
		for (ArchiveF f : as) {
			String fileName = f.getFileName();
			String ftpPath = f.getFtpPath();
			String localPath = f.getLocalPath();
			ja.add(getAttachmentJson(trimEndFix(fileName), ftpPath, localPath));
		}
		return JSONArray.fromObject(ja).toString();
	}

	public JSONObject getAttachmentJson(String filename, String filepath, String fileRealPath) throws Exception {
		JSONObject attachment = new JSONObject();
		attachment.put("name", filename);
		attachment.put("ext", filepath.substring(filepath.lastIndexOf(".") + 1));
		if ("WWW".equals(appid)) {
			attachment.put("file", getFileContentByBase64(fileRealPath));
		} else {
			attachment.put("file", filepath);
		}
		attachment.put("filetype", "正文");
		// md5文件内容校验
		attachment.put("filemd5", "");
		return attachment;
	}

	private boolean uploadToFTP(List<ArchiveF> as) throws FileNotFoundException {
		for (ArchiveF a : as) {
			boolean uploadFile = FtpTest.uploadFile(ip, 21, ftpName, ftpPassword, a.getFtpPath(), new FileInputStream(
					new File(a.getLocalPath())));
			if (!uploadFile) {
				log.error("ftp上传失败:文件名称:" + a.getFileName());
				return false;
			}
			log.info("ftp上传成功:文件名称:" + a.getFileName());
		}
		return true;
	}

	/**
	 * 带点的后缀名
	 * 
	 * @param fileAbsolutePath
	 * @return
	 */
	public static String getEndFix(String fileAbsolutePath) {
		int lastIndexOf = fileAbsolutePath.lastIndexOf(".");
		if (lastIndexOf <= 0) {
			return "";
		}
		return fileAbsolutePath.substring(lastIndexOf);
	}

	// @Test
	// public void testendfix() {
	// System.out.println(trimEndFix("哈哈.txt"));
	// }

	private String trimEndFix(String fileName) {
		int lastIndexOf = fileName.lastIndexOf(".");
		if (lastIndexOf <= 0) {
			return fileName;
		}
		return fileName.substring(0, lastIndexOf);
	}

	private void test() throws FileNotFoundException, Exception {
		String filePath = "e:\\1.txt";
		String fileName = "哈哈.txt";

		String saveFilePath = createSaveFilePath(getNewFileName(fileName));
		boolean uploadFile = FtpTest.uploadFile(ip, 21, ftpName, ftpPassword, saveFilePath, new FileInputStream(
				new File(filePath)));
		if (!uploadFile) {
			System.out.println("ftp上传失败");
			return;
		}
		System.out.println("ftp上传成功");
		ArchiveService service = new ArchiveService();
		ArchiveFile file = service.getArchiveFile(saveFilePath, filePath, fileName);
		boolean success = service.archive(file);
		System.out.println("最终结果：" + success);
	}

	private ArchiveFile getArchiveFile(String saveFilePath, String filePath, String fileName) throws Exception {
		ArchiveFile af = new ArchiveFile();
		af.setAppid(appid);
		af.setApptoken(apptoken);
		af.setCompanycode("main");
		af.setRid("OA12378");
		af.setPid("0");
		af.setTitle("关于xxx设备资料");
		af.setDocnumber("J001-02-0023");
		af.setAuthor("张XX");
		af.setDocdate("2017-02-28");
		af.setYear("2017");
		af.setSecurity("公开");
		af.setSecuritytime("5");
		af.setPagenum("24");
		af.setDepartment("项目建设部");
		af.setDepartmentid("10015");
		af.setDepartmentsn("建设");
		af.setSection("隶属处室");
		af.setSectionid("12344123");
		af.setCreater("zhangxx");
		af.setCreatername("张XX");
		af.setCreatetime("2017-02-28 13:34:56");
		af.setSubject("设备资料");
		af.setMemo("");
		af.setRelatearchives("");
		af.setKnownscope("");
		af.setRelatepersons("");
		af.setRelatedepartments("");
		af.setAttachment(getAttachmentJsonV3(fileName, saveFilePath, filePath));
		return af;
	}

	private boolean archive(ArchiveFile af) throws Exception {
		String resultMsg = receiveArchF3(af);
		log.info(resultMsg);
		JsonObject jsonObject = new JsonParser().parse(resultMsg).getAsJsonObject();
		JsonObject response = jsonObject.getAsJsonObject("Response");
		JsonElement code = response.get("Code");
		int c = code.getAsInt();
		log.info("返回的code:" + code);
		if (c == 0) {
			return true;
		}
		return false;
	}

	public static String createSaveFilePath(String fileName) {
		SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
		String date = formate.format(new Date());
		return "/epros/" + date + "/" + getNewFileName(fileName);
	}

	private static String getNewFileName(String oldName) {
		String uuid = UUID.randomUUID().toString();
		int index = oldName.lastIndexOf(".");
		if (index < 0) {
			return uuid;
		}
		String endfix = oldName.substring(index, oldName.length());
		return uuid + endfix;
	}

	// 归档接口3,项目归档
	public String receiveArchF3(ArchiveFile af) throws Exception {
		RPCServiceClient serviceClient = new RPCServiceClient();
		Options options = serviceClient.getOptions();
		String webUrl = "http://" + ip + "/Interfaces/Collect/archivesV3";
		// 指定调用WebService的URL
		EndpointReference targetEPR = new EndpointReference(webUrl);
		options.setTo(targetEPR);
		// 指定sayHelloToPerson方法的参数值
		Object[] opAddEntryArgs = new Object[] { appid, apptoken, af.getCompanycode(), af.getRid(), af.getPid(),
				af.getArchcode(), af.getDoccode(), af.getTitle(), af.getDocnumber(), af.getAuthor(), af.getDocdate(),
				af.getYear(), af.getRetentionperiod(), af.getSecurity(), af.getSecuritytime(), af.getPagenum(),
				af.getDepartment(), af.getDepartmentid(), af.getDepartmentsn(), af.getSection(), af.getSectionid(),
				af.getCreater(), af.getCreatername(), af.getCreatetime(), af.getSubject(), af.getMemo(),
				af.getRelatearchives(), af.getKnownscope(), af.getRelatepersons(), af.getRelatedepartments(),
				af.getAttachment(), af.getExt1(), af.getExt2(), af.getExt3(), af.getExt4(), af.getExt5(), af.getExt6(),
				af.getExt7(), af.getExt8(), af.getExt9(), af.getExt10(), af.getExt11(), af.getExt12(), af.getExt13(),
				af.getExt14(), af.getExt15(), af.getExt16(), af.getExt17(), af.getExt18(), af.getExt19(), af.getExt20() };
		// 指定sayHelloToPerson方法返回值的数据类型的Class对象
		Class[] classes = new Class[] { String.class };
		// 指定要调用的sayHelloToPerson方法及WSDL文件的命名空间
		QName opAddEntry = new QName(webUrl, "receiveDocArchive");
		log.info("调用的webservice地址：" + webUrl);
		String result = (String) serviceClient.invokeBlocking(opAddEntry, opAddEntryArgs, classes)[0];
		return result;
	}

	// /**
	// * 附件信息转JSON数据格式
	// *
	// * @param filename
	// * @param filepath
	// * @return
	// * @throws Exception
	// */
	// public String getAttachmentJson(String filename, String filepath) throws
	// Exception {
	// JSONObject attachment = new JSONObject();
	// attachment.put("name", filename);
	// attachment.put("ext", filepath.substring(filepath.lastIndexOf(".") + 1));
	// if ("WWW".equals(appid)) {
	// attachment.put("file", getFileContentByBase64(fileRealPath));
	// } else {
	// attachment.put("file", filepath);
	// }
	// // md5文件内容校验
	// attachment.put("filemd5", getMd5ByFile(new File(fileRealPath)));
	// JSONArray ja = new JSONArray();
	// ja.add(attachment);
	// String json = JSONArray.fromObject(ja).toString();
	// return json;
	// }

	/**
	 * 附件信息转JSON数据格式V3版
	 * 
	 * @param filename
	 * @param filepath
	 *            保存到归档系统中的路径
	 * @param fileRealPath
	 *            本地文件中的路径
	 * @return
	 * @throws Exception
	 */
	public String getAttachmentJsonV3(String filename, String filepath, String fileRealPath) throws Exception {
		JSONObject attachment = new JSONObject();
		attachment.put("name", filename);
		attachment.put("ext", filepath.substring(filepath.lastIndexOf(".") + 1));
		if ("WWW".equals(appid)) {
			attachment.put("file", getFileContentByBase64(fileRealPath));
		} else {
			attachment.put("file", filepath);
		}
		attachment.put("filetype", "正文");
		// md5文件内容校验
		attachment.put("filemd5", "");
		// attachment.put("filemd5", getMd5ByFile(new File(fileRealPath)));
		JSONArray ja = new JSONArray();
		ja.add(attachment);
		String json = JSONArray.fromObject(ja).toString();
		return json;
	}

	/**
	 * 将文件以base64编码格式读取
	 * 
	 * @param fileName
	 * @return
	 */
	public String getFileContentByBase64(String fileName) {
		StringBuffer sb = new StringBuffer();
		try {
			File file = new File(fileName);
			FileInputStream in = new FileInputStream(file);

			byte[] buf = new byte[3 * 1024 * 1024];
			int size;
			// System.out.println("我擦擦擦2");
			while ((size = in.read(buf)) != -1) {
				sb.append(Base64.encodeBase64String(Arrays.copyOf(buf, size)));
				// sb.append(new BASE64Encoder().encode(Arrays.copyOf(buf,
				// size)));
			}
			// System.out.println("我擦擦擦");
			in.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return sb.toString();
	}

	public static JSONObject getSingleArchiveData(String title, JSONArray attachmentList, String classfy,
			String classfyname, String retentionperiod, String year, String security, String docdate, String pagenum,
			String author, String department, String departmentid, String docnumber, String memo) {
		JSONObject postData = new JSONObject();
		JSONObject sourceData = new JSONObject();
		JSONObject archiveData = new JSONObject();
		sourceData.put("AppId", appid);
		sourceData.put("AppToken", apptoken);
		postData.put("Source", sourceData);
		// 原文附件
		// JSONArray attachmentList = new JSONArray();
		// attachmentList.add(getAttachment("附件名称1","/2016/1.pdf"));
		// attachmentList.add(getAttachment(filename,filepath));
		archiveData.put("Request", getArchData(attachmentList, title, classfy, classfyname, retentionperiod, year,
				security, docdate, pagenum, author, department, departmentid, docnumber, memo));
		postData.put("Data", archiveData);
		return postData;
	}

	/**
	 * 对文件进行md5加密
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static String getMd5ByFile(File file) throws FileNotFoundException {
		String value = null;
		FileInputStream in = new FileInputStream(file);
		try {
			MappedByteBuffer byteBuffer = in.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(byteBuffer);
			BigInteger bi = new BigInteger(1, md5.digest());
			value = bi.toString(16);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}

	public static JSONObject getAttachment(String filename, String filepath) {
		String filecontent = getFileContent(filepath);
		JSONObject attachment = new JSONObject();
		attachment.put("name", filename);
		attachment.put("ext", filepath.substring(filepath.lastIndexOf(".") + 1));
		attachment.put("file", filecontent);
		// attachment.put("filemd5",filecontent);
		return attachment;
	}

	public static String getFileContent(String fileName) {
		StringBuffer sb = new StringBuffer();

		try {

			FileInputStream in = new FileInputStream(fileName);
			byte[] buf = new byte[3 * 1024 * 1024];
			int size;
			System.out.println("我擦擦擦2");
			while ((size = in.read(buf)) != -1) {
				sb.append(Base64.encodeBase64String(Arrays.copyOf(buf, size)));

			}
			System.out.println("我擦擦擦");
			in.close();

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return sb.toString();
	}

	public static JSONObject getArchData(JSONArray attachmentList, String title, String classfy, String classfyname,
			String retentionperiod, String year, String security, String docdate, String pagenum, String author,
			String department, String departmentid, String docnumber, String memo) {
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		JSONObject data = new JSONObject();
		Date date = new Date();
		data.put("title", title + dateFormater.format(date) + "");// 题名
		// data.put("title", "test" + dateFormater.format(date) + "");//题名
		data.put("attachment", attachmentList);//
		data.put("classfy", classfy);//
		data.put("classfyname", classfyname);// 1
		data.put("retentionperiod", retentionperiod);//
		data.put("year", year);//
		data.put("security", security);//
		data.put("docdate", docdate);//
		data.put("pagenum", pagenum);//
		data.put("author", author);//
		data.put("department", department);//
		data.put("departmentid", departmentid);//
		data.put("docnumber", docnumber);//
		data.put("memo", memo);//
		return data;
	}

	private static String buildRequestData(JSONObject parameter) {
		StringBuffer soapRequestData = new StringBuffer();
		soapRequestData.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		soapRequestData.append("<Root>");
		soapRequestData.append(createXmlNode(parameter));
		soapRequestData.append("</Root>");
		System.out.println(soapRequestData);
		return soapRequestData.toString();
	}

	public static String createXmlNode(JSONObject parameter) {
		StringBuffer soapRequestData = new StringBuffer();
		Set<String> nameSet = parameter.keySet();
		for (String name : nameSet) {

			if (parameter.get(name) instanceof JSONArray) {
				JSONArray jsonArr = (JSONArray) parameter.get(name);
				for (int i = 0; i < jsonArr.size(); i++) {
					soapRequestData.append("<" + name + ">");
					soapRequestData.append(createXmlNode(jsonArr.getJSONObject(i)));
					soapRequestData.append("</" + name + ">");
				}
			} else {
				soapRequestData.append("<" + name + ">");
				if (parameter.get(name) instanceof JSONObject) {
					soapRequestData.append(createXmlNode((JSONObject) parameter.get(name)));
				} else {
					soapRequestData.append(parameter.get(name));
				}
				soapRequestData.append("</" + name + ">");
			}
		}
		return soapRequestData.toString();
	}

}
