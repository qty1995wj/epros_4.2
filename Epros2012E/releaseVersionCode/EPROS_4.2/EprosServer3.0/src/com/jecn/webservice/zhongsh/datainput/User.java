
package com.jecn.webservice.zhongsh.datainput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>User complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="User">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RowID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UserID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Position" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EHRCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserLevelNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UserLevelName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserLevelID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RoleLevel" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RoleID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RoleCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="EHRRoleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SAPRoleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RoleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="JobName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobEHRCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "User", propOrder = {
    "rowID",
    "userID",
    "username",
    "email",
    "displayName",
    "position",
    "idCard",
    "ehrCode",
    "userLevelNO",
    "userLevelName",
    "userLevelID",
    "roleLevel",
    "roleID",
    "roleCode",
    "ehrRoleCode",
    "sapRoleCode",
    "roleName",
    "jobID",
    "jobName",
    "jobEHRCode"
})
public class User {

    @XmlElement(name = "RowID")
    protected int rowID;
    @XmlElement(name = "UserID")
    protected int userID;
    @XmlElement(name = "Username")
    protected String username;
    @XmlElement(name = "Email")
    protected String email;
    @XmlElement(name = "DisplayName")
    protected String displayName;
    @XmlElement(name = "Position")
    protected String position;
    @XmlElement(name = "IDCard")
    protected String idCard;
    @XmlElement(name = "EHRCode")
    protected String ehrCode;
    @XmlElement(name = "UserLevelNO")
    protected int userLevelNO;
    @XmlElement(name = "UserLevelName")
    protected String userLevelName;
    @XmlElement(name = "UserLevelID", required = true, type = Integer.class, nillable = true)
    protected Integer userLevelID;
    @XmlElement(name = "RoleLevel")
    protected int roleLevel;
    @XmlElement(name = "RoleID")
    protected int roleID;
    @XmlElement(name = "RoleCode")
    protected int roleCode;
    @XmlElement(name = "EHRRoleCode")
    protected String ehrRoleCode;
    @XmlElement(name = "SAPRoleCode")
    protected String sapRoleCode;
    @XmlElement(name = "RoleName")
    protected String roleName;
    @XmlElement(name = "JobID")
    protected int jobID;
    @XmlElement(name = "JobName")
    protected String jobName;
    @XmlElement(name = "JobEHRCode")
    protected String jobEHRCode;

    /**
     * 获取rowID属性的值。
     * 
     */
    public int getRowID() {
        return rowID;
    }

    /**
     * 设置rowID属性的值。
     * 
     */
    public void setRowID(int value) {
        this.rowID = value;
    }

    /**
     * 获取userID属性的值。
     * 
     */
    public int getUserID() {
        return userID;
    }

    /**
     * 设置userID属性的值。
     * 
     */
    public void setUserID(int value) {
        this.userID = value;
    }

    /**
     * 获取username属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置username属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * 获取email属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置email属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * 获取displayName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * 设置displayName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayName(String value) {
        this.displayName = value;
    }

    /**
     * 获取position属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosition() {
        return position;
    }

    /**
     * 设置position属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosition(String value) {
        this.position = value;
    }

    /**
     * 获取idCard属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDCard() {
        return idCard;
    }

    /**
     * 设置idCard属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDCard(String value) {
        this.idCard = value;
    }

    /**
     * 获取ehrCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEHRCode() {
        return ehrCode;
    }

    /**
     * 设置ehrCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEHRCode(String value) {
        this.ehrCode = value;
    }

    /**
     * 获取userLevelNO属性的值。
     * 
     */
    public int getUserLevelNO() {
        return userLevelNO;
    }

    /**
     * 设置userLevelNO属性的值。
     * 
     */
    public void setUserLevelNO(int value) {
        this.userLevelNO = value;
    }

    /**
     * 获取userLevelName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserLevelName() {
        return userLevelName;
    }

    /**
     * 设置userLevelName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserLevelName(String value) {
        this.userLevelName = value;
    }

    /**
     * 获取userLevelID属性的值。
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUserLevelID() {
        return userLevelID;
    }

    /**
     * 设置userLevelID属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUserLevelID(Integer value) {
        this.userLevelID = value;
    }

    /**
     * 获取roleLevel属性的值。
     * 
     */
    public int getRoleLevel() {
        return roleLevel;
    }

    /**
     * 设置roleLevel属性的值。
     * 
     */
    public void setRoleLevel(int value) {
        this.roleLevel = value;
    }

    /**
     * 获取roleID属性的值。
     * 
     */
    public int getRoleID() {
        return roleID;
    }

    /**
     * 设置roleID属性的值。
     * 
     */
    public void setRoleID(int value) {
        this.roleID = value;
    }

    /**
     * 获取roleCode属性的值。
     * 
     */
    public int getRoleCode() {
        return roleCode;
    }

    /**
     * 设置roleCode属性的值。
     * 
     */
    public void setRoleCode(int value) {
        this.roleCode = value;
    }

    /**
     * 获取ehrRoleCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEHRRoleCode() {
        return ehrRoleCode;
    }

    /**
     * 设置ehrRoleCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEHRRoleCode(String value) {
        this.ehrRoleCode = value;
    }

    /**
     * 获取sapRoleCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSAPRoleCode() {
        return sapRoleCode;
    }

    /**
     * 设置sapRoleCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSAPRoleCode(String value) {
        this.sapRoleCode = value;
    }

    /**
     * 获取roleName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * 设置roleName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoleName(String value) {
        this.roleName = value;
    }

    /**
     * 获取jobID属性的值。
     * 
     */
    public int getJobID() {
        return jobID;
    }

    /**
     * 设置jobID属性的值。
     * 
     */
    public void setJobID(int value) {
        this.jobID = value;
    }

    /**
     * 获取jobName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * 设置jobName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobName(String value) {
        this.jobName = value;
    }

    /**
     * 获取jobEHRCode属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobEHRCode() {
        return jobEHRCode;
    }

    /**
     * 设置jobEHRCode属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobEHRCode(String value) {
        this.jobEHRCode = value;
    }

}
