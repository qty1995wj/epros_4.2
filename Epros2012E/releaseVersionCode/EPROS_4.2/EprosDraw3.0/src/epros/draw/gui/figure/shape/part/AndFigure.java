package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseCircle;

/**
 * 
 * And图形
 * 
 * @author ZHANGXH
 *
 */
public class AndFigure extends BaseCircle {

	public AndFigure(JecnFigureData figureData) {
		super(figureData);
	}
}
