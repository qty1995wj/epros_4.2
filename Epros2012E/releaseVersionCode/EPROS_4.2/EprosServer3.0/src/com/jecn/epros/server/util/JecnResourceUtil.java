package com.jecn.epros.server.util;

/**
 * 通过数据库类型获得国际化内容
 * 
 */
public class JecnResourceUtil {

	/**
	 * 
	 * 获取数据统计时间\频率的内容 0：月 1：季度
	 * 
	 * @param kpiDataMethod
	 *            int
	 * @return String
	 */
	public static String getKpiHorizontal(int kpiHorizontal) {
		switch (kpiHorizontal) {
		case 0:// 0：月
			return JecnUtil.getValue("kpiTargetUnitMonth");
		case 1:// 1：季度
			return JecnUtil.getValue("kpiTargetUnitQuarter");
		default:
			return "";
		}
	}

	/**
	 * 
	 * @param kpiHorizontal
	 * @return
	 */
	public static String getKpiType(int kpiHorizontal) {
		switch (kpiHorizontal) {
		case 0:// 0：结果性指标
			return JecnUtil.getValue("KpiTypea");
		case 1:// 1：过程性指标
			return JecnUtil.getValue("kpiTypeB");
		default:
			return "";
		}
	}

	/**
	 * 
	 * 获取数据获取方式的内容 0:人工 1：系统
	 * 
	 * @param kpiDataMethod
	 *            int
	 * @return String
	 */
	public static String getKpiDataMethod(int kpiDataMethod) {
		switch (kpiDataMethod) {
		case 0:// 0:人工
			return JecnUtil.getValue("artificial");
		case 1:// 1：系统
			return JecnUtil.getValue("messageSystem");
		default:
			return "";
		}
	}

	/**
	 * 
	 * 获取:KPI值单位名称(纵向单位)的内容
	 * 
	 * @param kpiVertical
	 *            int
	 * @return String
	 */
	public static String getKpiVertical(int kpiVertical) {
		switch (kpiVertical) {
		case 0:// 0:百分比
			return JecnUtil.getValue("kpiTargetUnitPercent");
		case 1:// 1:季度
			return JecnUtil.getValue("kpiTargetUnitQuarter");
		case 2:// 2:月
			return JecnUtil.getValue("kpiTargetUnitMonth");
		case 3:// 3:周
			return JecnUtil.getValue("kpiTargetUnitWeeks");
		case 4:// 4:工作日
			return JecnUtil.getValue("kpiTargetUnitWorkdDay");
		case 5:// 5:天
			return JecnUtil.getValue("kpiTargetUnitDay");
		case 6:// 6:小时
			return JecnUtil.getValue("kpiTargetUnitHour");
		case 7:// 7:分钟
			return JecnUtil.getValue("kpiTargetUnitMinutes");
		case 8:// 8:个
			return JecnUtil.getValue("kpiTargetUnitNum");
		case 9:// 9:人
			return JecnUtil.getValue("kpiTargetUnitPerson");
		case 10:// 10:ppm
			return JecnUtil.getValue("kpiTargetUnitPPM");
		case 11:// 11:元
			return JecnUtil.getValue("kpiTargetUnitYuan");
		case 12:// 12：次
			return JecnUtil.getValue("kpiTargetUnitFrequency");
		default:
			return "";
		}
	}

	/**
	 * 
	 * 获取KPI目标值比较符号的内容 0：= 1：> 2：< 3:>= 4:<=
	 * 
	 * @param kpiTargetOperator
	 *            int
	 * @return String
	 */
	public static String getKpiTargetOperator(int kpiTargetOperator) {
		switch (kpiTargetOperator) {
		case 0:
			return "=";
		case 1:
			return ">";
		case 2:
			return "<";
		case 3:
			return ">=";
		case 4:
			return "<=";
		default:
			return "";
		}
	}

	/**
	 * 
	 * 获取相关度的内容 0：强相关 1：弱相关 2：不相关
	 * 
	 * @param kpiRelevance
	 *            int
	 * @return String
	 */
	public static String getKpiRelevance(int kpiRelevance) {
		switch (kpiRelevance) {
		case 0:// 强相关
			return JecnUtil.getValue("strongCorrelation");
		case 1:// 弱相关
			return JecnUtil.getValue("weakCorrelation");
		case 2:// 不相关
			return JecnUtil.getValue("noCorrelation");
		default:
			return "";
		}
	}

	/**
	 * 
	 * 获取指标来源的内容 0:二级指标 1:个人PBC指标 2:其他重要工作
	 * 
	 * @param kpiTargetType
	 *            int
	 * @return String
	 */
	public static String getKpiTargetType(int kpiTargetType) {
		switch (kpiTargetType) {
		case 0:// 二级指标
			return JecnUtil.getValue("twoLevelTarget");
		case 1:// 个人PBC指标
			return JecnUtil.getValue("personPBCTarget");
		case 2:// 其他重要工作
			return JecnUtil.getValue("otherImpWork");
		default:
			return "";
		}
	}

}
