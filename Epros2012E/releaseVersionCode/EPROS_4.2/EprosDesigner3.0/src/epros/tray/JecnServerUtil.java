package epros.tray;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import jecntool.cmd.JecnCmd;
import jecntool.cmd.ResultBean;
import jecntool.util.Tool;

import org.apache.log4j.Logger;

import epros.designer.util.ConnectionPool;

/**
 * 服务工具类
 * 
 * @author admin
 * 
 */
public class JecnServerUtil {
	private static Logger log = Logger.getLogger(JecnServerUtil.class);

	public static boolean startTomcat() {
		return cmdPro(JecnTrayConstants.CMD_NET_START_EPROSSERVER3);
	}

	public static boolean stopTomcat() {
		return cmdPro(JecnTrayConstants.CMD_NET_STOP_EPROSSERVER3);
	}

	/**
	 * 
	 * 启动mysql服务
	 * 
	 * @return boolean true：启动成功 false：其他原因没有启动或已启动再启动不了
	 */
	public static boolean startMysql() {
		return cmdPro(JecnTrayConstants.CMD_NET_START_MYSQL5);
	}

	/**
	 * 
	 * 停止mysql服务
	 * 
	 * @return boolean
	 */
	public static boolean stopMysql() {
		return cmdPro(JecnTrayConstants.CMD_NET_STOP_MYSQL5);
	}

	private static boolean cmdPro(String cmd) {
		ResultBean retBean = JecnCmd.cmdExec(cmd);
		if (retBean.getResultState() == 0) {// 执行成功过
			return true;
		}
		return false;
	}

	/**
	 * 检测mysql数据库是否连接正常
	 * 
	 * return boolean 连接成功返回true连接失败返回false
	 */
	static boolean mysql5IsRun() {
		log.info("====检查MYSQL====");
		return TrayUtil.checkDBConn();
	}

	/**
	 * 检测 tomcat运行是否正常
	 * 
	 * @return boolean 正常返回true失败返回false
	 */
	public static boolean tomcatIsRun() {
		log.info("====检查TOMCAT======");
		boolean isRun = false;
		try {
			List list = ConnectionPool.getProject().getListProject();
			if (list != null) {
				isRun = true;
			}
		} catch (Exception e) {
			log.info("TOMCAT连接异常：");
		}
		return isRun;
	}

	/**
	 * 获取database.properties 配置文件路径
	 */
	static String getRmiConfigPath() {
		return getClassPath() + JecnTrayConstants.DB_CONFIG_FILE;
	}

	/**
	 * 获取JecnConfig.properties配置文件路径
	 */
	public static String getJecnConfigPath() {
		return getClassPath() + JecnTrayConstants.JECN_CONFIG;
	}
	
	/**
	 * 获取tomcat配置文件路径
	 * 
	 * @return String server.xml文件全路径
	 */
	static String getTomcatConfigPath() {
		return getClassPath() + JecnTrayConstants.TOMCAT_CONFIG_FILE;
	}

	/**
	 * 获取mysql配置文件路径
	 * 
	 * @return String my.ini文件全路径
	 */
	static String getMysqlConfigPath() {
		return getClassPath() + JecnTrayConstants.MYSQL_CONFIG_FILE;
	}

	/**
	 * 获取项目ClassPath路径
	 * 
	 * web项目：classpath路径，例如：D:/soft/worksoft/apache-tomcat-6.0.36-windows-x86/webapps/EprosServer3.0/WEB-INF/classes
	 * 
	 * application项目：项目根目录，比如：D:/soft/worksoft/EPROS-M
	 * 
	 * @return String ClassPath路径
	 */
	public static String getClassPath() {
		// 路径
		String path = null;
		URL url = JecnPort.class.getResource("");
		try {
			path = url.toURI().getPath();
		} catch (URISyntaxException e) {
			// 这儿一般是不进入此分支的
			log.error("转换成URI报错：path=" + path, e);
			path = url.getPath().replaceAll("%20", " ");
		}
		if (Tool.isNotBlank(path)) {// 获取项目classpath路径
			path = path.substring(0, path.length() - "/epros/tray/".length());
		}
		// 打印文件路径
		log.info("获取路径完成，classpath=" + path);
		return path;
	}

	// public static boolean checkResultStat(int state) {
	// String message = null;
	// switch (state) {
	// case 0: // 执行成功
	// return true;
	// case 1:
	// // 执行命令不正确
	// message = JecnProperties.getValue("");
	// break;
	// case 1056:// 该服务已在运行
	// message = JecnProperties.getValue("");
	// break;
	// case 1058:// 服务无法启动，可能因为被禁用，也可能因为没有关联的可用设备。稍后重试或重启计算机。
	// message = JecnProperties.getValue("");
	// break;
	// case 1060:// 指定的服务未安装
	// message = JecnProperties.getValue("");
	// break;
	// case 1061:// 该服务此时无法接收控制消息
	// message = JecnProperties.getValue("");
	// break;
	// case 1072:// 指定的服务已标记为删除,请重启计算机
	// message = JecnProperties.getValue("");
	// break;
	// default:// 启动失败，稍后再启动
	// message = JecnProperties.getValue("");
	// break;
	// }
	// JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(),
	// message);
	//
	// return false;
	// }
}
