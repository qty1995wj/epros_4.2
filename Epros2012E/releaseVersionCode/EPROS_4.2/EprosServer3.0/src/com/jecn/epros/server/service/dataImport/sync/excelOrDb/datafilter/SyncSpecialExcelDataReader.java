package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class SyncSpecialExcelDataReader {

	/** 日志记录 */
	protected final Log log = LogFactory.getLog(SyncSpecialExcelDataReader.class);

	/** 人员及岗位 */
	private List<UserBean> userPosBeanList = new ArrayList<UserBean>();
	/** 部门 */
	private List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
	private String path;

	public SyncSpecialExcelDataReader(String path) {
		this.path = path;
	}

	public List<UserBean> getUserPosData() {
		return userPosBeanList;
	}

	public List<DeptBean> getDeptData() {
		return deptBeanList;
	}

	/**
	 * 读取Hisense设计器固定数据
	 * 
	 * @author weidp
	 * @date 2014-8-1 下午02:09:32
	 */
	public void readExcelData() {
		// 读取本地excel数据
		FileInputStream inputStream = null;
		// 创建excel工作表
		Workbook rwb = null;
		try {
			if (StringUtils.isBlank(path)) {
				return;
			}
			// 读取本地excel数据
			inputStream = new FileInputStream(path);
			// 创建excel工作表
			rwb = Workbook.getWorkbook(inputStream);
			// 部门Sheet
			Sheet deptSheet = rwb.getSheet(0);
			// 人员及岗位Sheet
			Sheet userPosSheet = rwb.getSheet(1);
			// 读取数据
			readDeptData(deptSheet);
			readUserPosSheet(userPosSheet);

		} catch (Exception e) {
			log.error("读取人员同步特殊数据异常", e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error("关闭读取人员同步特殊数据异常", e);
				}
			}
			if (rwb != null) {
				rwb.close();
			}
		}
	}

	/**
	 * 读取人员及岗位数据
	 * 
	 * @author weidp
	 * @date 2014-8-1 下午02:10:29
	 * @param userPosSheet
	 */
	private void readUserPosSheet(Sheet userPosSheet) {
		// 第四行开始读数据
		for (int i = SyncConstant.WIRTER_ROW_NUM; i < userPosSheet.getRows(); i++) {

			UserBean userBean = new UserBean();

			// 登录名称
			String userNum = getCellText(userPosSheet.getCell(0, i));
			// 真实姓名
			String trueName = getCellText(userPosSheet.getCell(1, i));
			// 任职岗位编号
			String posNum = getCellText(userPosSheet.getCell(2, i));
			// 任职岗位名称
			String posName = getCellText(userPosSheet.getCell(3, i));
			// 岗位所属部门编号
			String deptNum = getCellText(userPosSheet.getCell(4, i));
			// 邮箱地址
			String email = getCellText(userPosSheet.getCell(5, i));
			// 内外网邮件标识(内网:0 外网:1)
			String emailType = getCellText(userPosSheet.getCell(6, i));
			// 联系电话
			String phone = getCellText(userPosSheet.getCell(7, i));

			// 登录名称
			userBean.setUserNum(userNum);
			// 真实姓名
			userBean.setTrueName(trueName);
			// 任职岗位编号
			userBean.setPosNum(posNum);
			// 任职岗位名称
			userBean.setPosName(posName);
			// 岗位所属部门编号
			userBean.setDeptNum(deptNum);
			// 邮箱地址
			userBean.setEmail(email);
			// 邮箱、邮箱类型不等于空且是“0”，“1”
			if (SyncTool.isEmailTypeStr(emailType)) {
				// 内外网邮件标识(内网:0 外网:1)
				userBean.setEmailType(Integer.valueOf(emailType));
			}
			// 联系电话
			userBean.setPhone(phone);

			// 属性值都为空情况
			if (userBean.isAttributesNull()) {
				continue;
			}
			userPosBeanList.add(userBean);
		}
	}

	/**
	 * 读取部门数据
	 * 
	 * @author weidp
	 * @date 2014-8-1 下午02:19:26
	 * @param deptSheet
	 */
	private void readDeptData(Sheet deptSheet) {
		DeptBean deptBean = null;
		// 第四行开始读数据
		for (int i = SyncConstant.WIRTER_ROW_NUM; i < deptSheet.getRows(); i++) {
			deptBean = new DeptBean();
			// 部门编号
			String deptNum = getCellText(deptSheet.getCell(0, i));
			// 部门名称
			String deptName = getCellText(deptSheet.getCell(1, i));
			// 上级部门编号
			String perDeptNum = getCellText(deptSheet.getCell(2, i));

			deptBean.setDeptNum(deptNum);
			deptBean.setDeptName(deptName);
			deptBean.setPerDeptNum(perDeptNum);

			deptBeanList.add(deptBean);
		}
	}

	/**
	 * 获取单元格内容
	 * 
	 * @param cell
	 * @return
	 */
	private String getCellText(Cell cell) {
		if (SyncTool.isNullObj(cell)) {
			return null;
		} else {
			String cellContent = cell.getContents();
			return SyncTool.emtryToNull(cellContent);
		}
	}
}
