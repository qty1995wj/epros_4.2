package com.jecn.epros.server.connector.task;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.webservice.cetc10.Cetc10TaskBean;

/**
 * 任务代办基类
 * 
 * @author xiaohu
 * 
 */
public abstract class JecnBaseTaskSend {
	protected static final Logger log = Logger.getLogger(JecnBaseTaskSend.class);

	public abstract void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao,
			Set<Long> handlerPeopleIds, Long curPeopleId) throws Exception;

	public abstract void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew, JecnUser jecnUser,
			JecnUser createUser);

	public abstract void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception;

	/**
	 * 任务审批,发生代办信息到门户系统
	 * 
	 * @param myTaskBean
	 * @param isProcessed
	 *            true:当我代办信息无效
	 * @return
	 * @throws Exception
	 */
	protected Cetc10TaskBean getCetc10ToDOTasks(MyTaskBean myTaskBean, IPersonDao personDao) {
		JecnUser jecnUser = personDao.get(myTaskBean.getTaskApprovePeopleId());
		Cetc10TaskBean taskBean = new Cetc10TaskBean();
		// 任务ID + 人员ID + 任务阶段
		taskBean.setTaskid(myTaskBean.getTaskId() + "_" + myTaskBean.getTaskApprovePeopleId() + "_"
				+ myTaskBean.getTaskStage());
		// 当前审批人名称
		taskBean.setName(jecnUser.getTrueName());
		taskBean.setTitle(myTaskBean.getTaskName());
		taskBean.setTime(myTaskBean.getUpdateTime());
		taskBean.setUrl(getMailUrl(myTaskBean.getTaskId(), myTaskBean.getTaskApprovePeopleId(), false, false));
		taskBean.setIsProcessed(myTaskBean.getIsProcessed());
		taskBean.setLoginName(jecnUser.getEmail());
		taskBean.setSummery(myTaskBean.getTaskDesc());
		log.info(taskBean);
		return taskBean;
	}

	protected MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew, IPersonDao personDao) {
		JecnUser createUser = personDao.get(beanNew.getCreatePersonId());
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(beanNew.getTaskName() + "(" + createUser.getTrueName() + ")");
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		return myTaskBean;
	}

	protected String getMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("getCetc10ToDOTasks获取系统配置URL为空!");
		}
		return basicEprosURL + "loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
				+ loginId + "&isApp=" + isApp + "&isView=" + isView;
	}

	protected String getMibileMailUrl(long taskId, long loginId, boolean isApp, boolean isView) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("getCetc10ToDOTasks获取系统配置URL为空!");
		}
		return basicEprosURL + "loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
				+ loginId + "&isApp=" + isApp + "&isView=" + isView;
	}

	protected String getTaskName(JecnTaskBeanNew beanNew) {
		String name = beanNew.getTaskName();
		if (beanNew.getTaskType() == 1 || beanNew.getTaskType() == 3) {// 为文件的时候，截取字符串
			return name.indexOf(".") != -1 ? name.substring(0, name.lastIndexOf(".")) : name;
		}
		return name;
	}
	
	private String SYS_NAME = "EPROS";

	/**
	 * 当前代办中唯一ID
	 * 
	 * @param taskBeanNew
	 *            任务对象
	 * @param handlerId
	 *            审批人员ID
	 * @param taskStatus
	 *            1 创建待办 0 待办转已办
	 * @return
	 */
	protected String getTaskOrderId(JecnTaskBeanNew taskBeanNew, Long handlerId, int taskStatus) {
		Long taskOrderId = null;
		if (taskStatus == 1) {// 创建代办
			taskOrderId = taskBeanNew.getHandlerPeopleMap().get(handlerId);
		} else {// 已办
			taskOrderId = taskBeanNew.getCanclePeopleMap().get(handlerId);
		}
		return SYS_NAME + "_" + taskOrderId;
	}


}
