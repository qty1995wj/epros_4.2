package com.jecn.epros.mapper;

import com.jecn.epros.domain.common.relatedfiles.RelatedFileBean;
import com.jecn.epros.domain.inventory.RiskInventoryRelatedControlPointBean;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.process.relatedFile.RelatedFile;
import com.jecn.epros.domain.process.relatedFile.RelatedFlow;
import com.jecn.epros.domain.process.relatedFile.RelatedRule;
import com.jecn.epros.domain.risk.Risk;
import com.jecn.epros.domain.risk.RiskRelatedGuide;
import com.jecn.epros.domain.risk.RiskRelatedTargetDescription;
import com.jecn.epros.domain.risk.SearchRiskResultBean;
import com.jecn.epros.sqlprovider.RiskSqlProvider;
import com.jecn.epros.sqlprovider.StandardSqlProvider;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface RiskMapper {

    @Select( "select distinct rik.ID," +
            "       rik.PARENT_ID," +
            "       rik.RISK_CODE," +
            "       rik.IS_DIR," +
            "       rik.NAME," +
            "       rik.GRADE," +
            "       rik.SORT," +
            "       rik.STANDARDCONTROL," +
            "       rik.CREATE_PERSON," +
            "       rik.CREATE_TIME," +
            "       rik.UPDATE_PERSON," +
            "       rik.UPDATE_TIME," +
            "       rik.NOTE," +
            "       rik.PROJECT_ID," +
            "       rik.T_PATH," +
            "       rik.T_LEVEL," +
            "       rik.VIEW_SORT," +
            "       DIC.VALUE RISK_TYPE" +
            "  from JECN_RISK RIK" +
            "  LEFT JOIN JECN_DICTIONARY dic" +
            "    ON RIK.RISK_TYPE = DIC.CODE" +
            "   and DIC.NAME = 'JECN_RISK_TYPE'" +
            " where rik.id = #{riskId}")
    @ResultMap("mapper.Risk.risk")
    public Risk getRisk(Long riskId);

    /**
     * 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RiskSqlProvider.class, method = "findChildRisks")
    @ResultMap("mapper.Risk.riskTreeBeans")
    public List<TreeNode> findChildRisks(Map<String, Object> map);

    /**
     * 相关流程
     *
     * @param riskId
     * @return
     */
    @SelectProvider(type = RiskSqlProvider.class, method = "findRelatedFlows")
    @ResultMap("mapper.Process.relatedFlow")
    public List<RelatedFlow> findRelatedFlows(Long riskId);

    /**
     * 风险相关控制点
     *
     * @param riskId
     * @return
     */
    @SelectProvider(type = RiskSqlProvider.class, method = "findRiskTargetDescriptions")
    @ResultMap("mapper.Risk.riskRelatedTargetDescription")
    public List<RiskRelatedTargetDescription> findRiskTargetDescriptions(Long riskId);

    /**
     * 风险相关内控指引
     *
     * @param riskId
     * @return
     */
    @SelectProvider(type = RiskSqlProvider.class, method = "findRiskGuide")
    @ResultMap("mapper.Risk.riskRelatedGuide")
    public List<RiskRelatedGuide> findRiskGuide(Long riskId);

    /**
     * 相关制度
     *
     * @param riskId
     * @return
     */
    @SelectProvider(type = RiskSqlProvider.class, method = "findRelatedRules")
    @ResultMap("mapper.Process.relatedRule")
    public List<RelatedRule> findRelatedRules(Long riskId);

    /**
     * 制度搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RiskSqlProvider.class, method = "searchRisk")
    @ResultMap("mapper.Risk.searchRiskResultBean")
    public List<SearchRiskResultBean> searchRisk(Map<String, Object> map);

    /**
     * 任务管理根据查询条件获得任务的数量
     */
    @SelectProvider(type = RiskSqlProvider.class, method = "searchRiskTotal")
    @ResultType(Integer.class)
    public int searchRiskTotal(Map<String, Object> map);

    @SelectProvider(type = RiskSqlProvider.class, method = "fetchInventory")
    @ResultMap("mapper.Risk.fetchInventory")
    public List<RiskInventoryRelatedControlPointBean> fetchInventory(Map<String, Object> paramMap);

    @SelectProvider(type = RiskSqlProvider.class, method = "findRelatedFiles")
    @ResultMap("mapper.Process.relatedFile")
    List<RelatedFile> findRelatedFiles(long id);

    @SelectProvider(type = RiskSqlProvider.class, method = "fetchInventoryParent")
    @ResultMap("mapper.Risk.fetchInventory")
    public List<RiskInventoryRelatedControlPointBean> fetchInventoryParent(Map<String, Object> paramMap);
}
