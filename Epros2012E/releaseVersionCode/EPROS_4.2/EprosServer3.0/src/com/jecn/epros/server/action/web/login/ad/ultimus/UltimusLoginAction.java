package com.jecn.epros.server.action.web.login.ad.ultimus;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * a
 * 
 *@author admin
 * @date 2016-6-14下午01:40:35
 */
public class UltimusLoginAction extends JecnAbstractADLoginAction {
	// "http://124.193.219.102:8088/EprosServer3.0/Login.action?UserInfo=0001"

	public UltimusLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		// http://127.0.0.1:8080/login.action?UserNumber=admin
		// request 获取连接传入 参数 ;URL 的加号 被转义为空格, 以下方式不会
		try {
			String loginName = loginAction.getRequest().getParameter(JecnUltimusAfterItem.LOGIN_NAME);
			if (StringUtils.isBlank(loginName)) {// 单点登录名称为空，执行普通登录路径
				return loginAction.loginGen();
			}

			// 获取跳转类型
			String forward = loginAction.getRequest().getParameter("epros_forward");
			if ("processView".equals(forward)) {// 直接打开流程图
				return loginToProcess(loginName);
			}
			return loginByLoginName(loginName);
		} catch (Exception e) {
			log.error("思维单点登录异常！" + loginAction.getRequest().getQueryString(), e);
			return null;
		}
	}

	private String loginToProcess(String loginName) {
		String ret = loginByLoginName(loginName);
		String redirectURL = null;
		// log.info("登录返回值ret = " + ret);
		if (LoginAction.MAIN.equals(ret) || LoginAction.SUCCESS.equals(ret)) {// 登录成功
			String flowId = loginAction.getRequest().getParameter("flowId");
			String type = loginAction.getRequest().getParameter("type");
			// 访问的流程EPROS地址
			redirectURL = JecnUltimusAfterItem.EPROS_URL + "process.action?type=" + type + "&flowId=" + flowId;
			// log.info("请求成功后登录流程地址：redirectURL = " + redirectURL);
			loginAction.setCustomRedirectURL(redirectURL);
			return LoginAction.REDIRECT;
		}
		return ret;
	}
}
