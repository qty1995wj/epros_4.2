package com.jecn.epros.server.download.wordxml.jinfeng;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Fonts;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ParagraphSpaceBean;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.bean.paragraph.GroupBean.Hanchor;
import wordxml.element.bean.paragraph.GroupBean.Vanchor;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.download.doc.RoleInfo;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 金风操作说明模版
 * 
 * @author hyl
 * 
 */
public class JFProcessModel extends EmptyProcessModel {
	/*** 段落行距 单倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距最小值12磅 *****/
	private final ParagraphLineRule leastVline12 = new ParagraphLineRule(12F, LineRuleType.AT_LEAST);

	public JFProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(false);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(17.38F);
		getDocProperty().setNewRowHeight(0.7F);
		getDocProperty().setFlowChartScaleValue(7.5F);
		setDocStyle(" ", textTitleStyle());

		processDownloadBean.getRoleInfo().init();
		processDownloadBean.getActiveInfo().init();
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 2.7F, 2.54F, 2.7F, 2F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(1F, 2F, 2.4F, 2F, 1F, 1.75F);
		titleSect.setSectBean(sectBean);
		addTitleSectContent();
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent() {

		Sect titleSect = docProperty.getTitleSect();
		SectBean sectBean = getSectBean();
		titleSect.setSectBean(sectBean);

		Paragraph emptyParagraph = createEmptyParagraph();
		// bfw
		createBFW(titleSect);
		titleSect.addParagraph(emptyParagraph);
		// 公司名称
		createTitleComponyName(titleSect);
		titleSect.addParagraph(emptyParagraph);
		standardVersion(titleSect);
		// 一条线
		createTopLine(titleSect);
		// 流程名称
		createProcessName(titleSect);
		// 密级 有效期等
		createSec(titleSect);
		// 发布行
		createPubLine(titleSect);
		// 一条线
		createBottomLine(titleSect);
		// 公司信息
		createComponyPub(titleSect);
	}

	private void createComponyPub(Sect titleSect) {
		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 14, GroupH.exact, 2).setHorizontal(3.79, Hanchor.page, 0.22)
				.setVertical(27, Vanchor.page, 0.32).lock(true);

		ParagraphBean pBean = new ParagraphBean(Align.center);
		Paragraph paragraph = new Paragraph(false);
		paragraph.setParagraphBean(pBean);
		FontBean fontBean = new FontBean("黑体", Constants.xiaoer, false);
		paragraph.appendTextRun("新疆金风科技股份有限公司", fontBean);
		fontBean = new FontBean("黑体", Constants.sihao, false);
		paragraph.appendTextRun("    发 布", fontBean);
		group.addParagraph(paragraph);
	}

	private void createSec(Sect titleSect) {
		ParagraphBean pubBean = new ParagraphBean(Align.left, Fonts.SONG, Constants.xiaosan, false);
		pubBean.setSpace(0F, 0F, new ParagraphLineRule(1, LineRuleType.AUTO));
		pubBean.setInd(0F, 0F, 0F, 8F);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 18, GroupH.exact, 8).setHorizontal(1F, Hanchor.page, 0)
				.setVertical(13.7, Vanchor.page, 0.32).lock(true);
		pubGroup.addParagraph(new Paragraph("版  本："
				+ nullToEmpty(processDownloadBean.getLatestHistoryNew() == null ? "" : processDownloadBean
						.getLatestHistoryNew().getVersionId()), pubBean));
		pubGroup.addParagraph(new Paragraph("", pubBean));
		pubGroup.addParagraph(new Paragraph("编  制： ", pubBean));
		pubGroup.addParagraph(new Paragraph("", pubBean));
		pubGroup.addParagraph(new Paragraph("审  核： ", pubBean));
		pubGroup.addParagraph(new Paragraph("", pubBean));
		pubGroup.addParagraph(new Paragraph("标准化：", pubBean));
		pubGroup.addParagraph(new Paragraph("", pubBean));
		pubGroup.addParagraph(new Paragraph("批  准： ", pubBean));

	}

	private Paragraph createEmptyParagraph() {
		Paragraph paragraph = new Paragraph("", new ParagraphBean(Align.center, "黑体", Constants.sihao, false));
		return paragraph;
	}

	private void createPubLine(Sect titleSect) {

		String pubDateStr = nullToEmpty(processDownloadBean.getReleaseDate());
		if (pubDateStr.length() > 10) {
			pubDateStr = pubDateStr.substring(0, 10);
		}
		// 2016 - 05 - 01实施 黑体 14
		String nextMonthFirstDay = "";
		if (StringUtils.isNotBlank(pubDateStr)) {
			try {
				// 下一月第一天
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date pubDate = format.parse(pubDateStr);
				Calendar cal = Calendar.getInstance();
				cal.setTime(pubDate);
				cal.add(Calendar.MONTH, 1);
				cal.set(Calendar.DAY_OF_MONTH, 1);
				nextMonthFirstDay = format.format(cal.getTime());

			} catch (ParseException e) {
				log.error(e);
			}
		}

		// 2016 - 04 - 11发布 黑体 四号
		ParagraphBean pubBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(3F, Hanchor.page, 0)
				.setVertical(26.32, Vanchor.page, 0.32).lock(true);
		pubGroup.addParagraph(new Paragraph(pubDateStr + "发布", pubBean));

		// 2016 - 04 - 11实施 黑体 四号
		ParagraphBean nextBean = new ParagraphBean(Align.right, "黑体", Constants.sihao, false);
		Group nextGroup = titleSect.createGroup();
		nextGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(11.1, Hanchor.page, 0)
				.setVertical(26.32, Vanchor.page, 0.32).lock(true);
		nextGroup.addParagraph(new Paragraph(nextMonthFirstDay + "实施", nextBean));
	}

	private void createProcessName(Sect titleSect) {
		String flowName = nullToEmpty(processDownloadBean.getFlowName());
		// 黑体 26
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		ParagraphLineRule vLine1 = new ParagraphLineRule(34F, LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph paragraph = new Paragraph(flowName, pBean);

		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 17, GroupH.exact, 2.7).setHorizontal(2.0, Hanchor.page, 0)
				.setVertical(11.27, Vanchor.page, 0).lock(true);
		group.addParagraph(paragraph);
	}

	private void createTopLine(Sect titleSect) {
		createLine(titleSect, 0.95F, 25F, 450F, 25F);
	}

	private void createBottomLine(Sect titleSect) {
		createLine(titleSect, 0.95F, 522F, 450F, 522F);
	}

	private void createLine(Sect titleSect, float startX, float startY, float endX, float endY) {
		LineGraph line = new LineGraph(LineStyle.single, 1.2F);
		line.setFrom(startX, startY);
		line.setTo(endX, endY);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		positionBean.setMarginRight(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);
	}

	private void standardVersion(Sect titleSect) {
		ParagraphBean pBean = new ParagraphBean(Align.right, Fonts.ARIAL, Constants.sihao, false);
		ParagraphLineRule vLine1 = new ParagraphLineRule(14, LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph vision = new Paragraph(nullToEmpty(processDownloadBean.getFlowInputNum()), pBean);

		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 17, GroupH.exact, 1.46).setHorizontal(1.59F, Hanchor.page, 0)
				.setVertical(6.67F, Vanchor.page, 0).lock(true);
		group.addParagraph(vision);
	}

	private void createTitleComponyName(Sect titleSect) {

		/*** 最小值 分散对齐 *****/
		ParagraphLineRule vLine = new ParagraphLineRule(0F, LineRuleType.AT_LEAST);
		ParagraphBean pBean = new ParagraphBean(Align.distribute, "黑体", Constants.xiaoyi, false);
		pBean.setSpace(0f, 0f, vLine);
		titleSect.createParagraph("新疆金风科技股份有限公司企业标准", pBean);

	}

	private void createBFW(Sect titleSect) {
		ParagraphBean pBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sishiba, true);
		Paragraph zcfcParagraph = new Paragraph("Q/GW", pBean);
		titleSect.addParagraph(zcfcParagraph);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {

		// 流程关键评测指标数据 0 KPI的名称 1 类型 2 目标值 3 kpi定义 4 kpi统计方法 5 数据统计时间频率 6
		// KIP值单位名称 7 数据获取方式 8 相关度 9 指标来源 10 数据提供者名称 11 支撑的一级指标内容 12
		// 12 目的 13 测量点 14 统计周期 15 说明 16 IT系统Id集合
		List<Object[]> flowKpiList = processDownloadBean.getFlowKpiList();
		String kpiName = null;
		String kpiDef = null;
		String kpiCycle = null;
		String kpiSource = null;
		if (flowKpiList.size() > 0) {
			Object[] objects = flowKpiList.get(0);
			kpiName = nullToEmpty(objects[0]);
			kpiDef = nullToEmpty(objects[3]);
			kpiCycle = nullToEmpty(objects[14]);
			kpiSource = nullToEmpty(JecnUtil
					.getKpiFrom(objects[9] == null ? 0 : Integer.valueOf(objects[9].toString())));
		}

		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		createCommfhdr(firstSect);
		ParagraphBean paragraphBean = new ParagraphBean(Align.center, "黑体", Constants.sanhao, false);
		firstSect.createParagraph("前    言", paragraphBean);
		firstSect.createMutilEmptyParagraph(2);
		ParagraphBean pBean = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		// 首行缩进两个字符
		pBean.setParagraphSpaceBean(new ParagraphSpaceBean(0F, 0F, new ParagraphLineRule(1, LineRuleType.AUTO)));
		pBean.setInd(0F, 0F, 0F, 1F);
		firstSect.createParagraph("本标准由新疆金风科技股份有限公司提出并归口。", pBean);
		firstSect.createParagraph("本标准起草单位：风机业务单元××中心×××部。", pBean);
		firstSect.createParagraph("本标准主要起草人：×××  ×××。", pBean);
		firstSect.createParagraph("本标准代替标准的历次版本发布情况:首次发布。------发布的是哪般就写哪版。", pBean);
		firstSect.createParagraph("如为修订版本，按以下内容：------如果是修订版本，这句删除。", pBean);
		firstSect.createParagraph("本标准替代并废止××流程，修订内容如下：-----编号《名称》版本号", pBean);
		firstSect.createParagraph("—	增加了×××××××（见××条款）；", pBean);
		firstSect.createParagraph("—	删除了×××××××（见原版本××条款）。", pBean);
		firstSect.createParagraph("—	……", pBean);
		firstSect.createParagraph("本标准代替标准的历次版本的发布情况为：", pBean);
		firstSect.createParagraph("—	编号《名称》，版本号：× ，编制人：××× ", pBean);
		firstSect.breakPage();
		firstSect.addParagraph(new Paragraph("新疆金风科技股份有限公司", new ParagraphBean(Align.center, "黑体", Constants.sanhao,
				false)));

		firstSect.createParagraph(processDownloadBean.getFlowName() + "说明：", new ParagraphBean(Align.left, Fonts.SONG,
				Constants.xiaosan, true));
		firstSect.createParagraph("流程特征", new ParagraphBean(Align.center, Fonts.SONG, Constants.xiaosi, true));

		pBean = new ParagraphBean(Align.left, "宋体", Constants.xiaosi, false);
		Table table = firstSect.createTable(tblStyle(), new float[] { 5.19F, 11F });
		table.createTableRow(new String[] { "流程控制目的 ", nullToEmpty(processDownloadBean.getFlowPurpose()) }, pBean);
		table.createTableRow(new String[] { "流程适用范围", "业务范围：" + nullToEmpty(processDownloadBean.getApplicability()) },
				pBean);
		table.createTableRow(new String[] { "", "适用范围：" + nullToEmpty(concat(processDownloadBean.getApplyOrgs())) },
				pBean);
		table.createTableRow(new String[] { "流程主导中心部门/角色 ", nullToEmpty(processDownloadBean.getOrgName()) }, pBean);
		table.createTableRow(new String[] { "流程涉及部门 ", nullToEmpty(getPosDepart()) }, pBean);
		table.createTableRow(new String[] {
				"流程触发事件 ",
				nullToEmpty(processDownloadBean.getFlowDriver() == null ? "" : processDownloadBean.getFlowDriver()
						.getDriveRules()) }, pBean);
		table.createTableRow(new String[] { "流程绩效指标", "指标名称：" + nullToEmpty(kpiName) }, pBean);
		table.createTableRow(new String[] { "", "指标定义：" + nullToEmpty(kpiDef) }, pBean);
		table.createTableRow(new String[] { "", "统计周期：" + nullToEmpty(kpiCycle) }, pBean);
		table.createTableRow(new String[] { " ", "数据来源：" + nullToEmpty(kpiSource) }, pBean);
		table.createTableRow(new String[] { "相关流程", nullToEmpty(getRelatedFlows()) }, pBean);
		table.createTableRow(new String[] { "相关制度", nullToEmpty(getRelatedRules()) }, pBean);
		table.setValign(Valign.center);
		// 居中，合并单元格，加粗
		table.setColStyle(0, new ParagraphBean(Align.center, Fonts.SONG, Constants.xiaosi, true), Valign.center);
		table.getRow(1).getCell(0).setRowspan(2);
		table.getRow(6).getCell(0).setRowspan(4);

		createRoleDuty(firstSect);
		createOther(firstSect);

	}

	private void createOther(Sect firstSect) {
		Paragraph p = firstSect.createParagraph("其他补充说明：", new ParagraphBean(Align.left, Fonts.SONG, Constants.wuhao,
				true));
		p.appendTextRun(nullToEmpty(processDownloadBean.getFlowSupplement()), new FontBean(Fonts.SONG, Constants.wuhao,
				false));
		firstSect.createParagraph("术语定义：" + nullToEmpty(processDownloadBean.getNoutGlossary()), new ParagraphBean(
				Align.left, Fonts.SONG, Constants.wuhao, false));
	}

	private void createRoleDuty(Sect firstSect) {
		firstSect
				.createParagraph("流程相关部门角色职责明晰表 ", new ParagraphBean(Align.center, Fonts.SONG, Constants.xiaosi, true));
		Table table = firstSect.createTable(new float[] { 3.78F, 4F, 8.41F });
		table.setTableBean(tblStyle());
		ParagraphBean pBean = tblContentStyle();
		table.createTableRow(new String[] { "组织", "角色名称", "职  责" }, pBean);
		RoleInfo roleInfo = processDownloadBean.getRoleInfo();
		List<String[]> infos = roleInfo.getJinfengRoles();
		table.createTableRow(infos, pBean);
		table.setValign(Valign.center);
		table.setColStyle(0, new ParagraphBean(Align.center, Fonts.SONG, Constants.xiaosi, true), Valign.center);
		table.setRowStyle(0, new ParagraphBean(Align.center, Fonts.SONG, Constants.xiaosi, true));
	}

	/**
	 * 选定角色关联岗位的部门
	 * 
	 * @return
	 */
	private String getPosDepart() {
		return processDownloadBean.getRoleInfo().getJinfengAllPosOrg();
	}

	private String getRelatedRules() {
		List<Object[]> ruleNameList = processDownloadBean.getRuleNameList();
		Set<String> names = new HashSet<String>();
		if (ruleNameList != null) {
			for (Object[] related : ruleNameList) {
				names.add(related[0].toString());
			}
		}
		return concat(names);
	}

	private String getRelatedFlows() {
		List<RelatedProcessBean> relateds = processDownloadBean.getRelatedProcessList();
		Set<String> names = new HashSet<String>();
		if (relateds != null) {
			for (RelatedProcessBean related : relateds) {
				names.add(related.getFlowName());
			}
		}
		return concat(names);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setSize(29.7F, 21F);
		flowChartSect.setSectBean(sectBean);
		createCharfhdr(flowChartSect);

	}

	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		ParagraphBean pBean = new ParagraphBean(Align.center);
		for (Image img : images) {
			processFileItem.getBelongTo().createParagraph(img, pBean);
		}
		Sect sect = processFileItem.getBelongTo();
		sect.breakPage();
		createCharExplan(sect);
		sect.createMutilEmptyParagraph(1);
		createRiks(sect);
		sect.createMutilEmptyParagraph(1);
		createProcessRecord(sect);

	}

	private void createProcessRecord(Sect sect) {
		sect.createParagraph("相关表单： ", new ParagraphBean(Align.left, Fonts.SONG, Constants.xiaosan, true));
		// 文件编号
		String fileNumber = JecnUtil.getValue("fileNumber");
		// 文件名称
		String fileName = JecnUtil.getValue("fileName");
		Table table = sect.createTable(tblStyle(), new float[] { 12F, 13F });
		table.createTableRow(new String[] { fileNumber, fileName }, new ParagraphBean(Align.center, Fonts.SONG,
				Constants.wuhao, true));
		ParagraphBean pBean = new ParagraphBean(Align.center, Fonts.SONG, Constants.wuhao, false);
		table.createTableRow(JecnWordUtil.obj2String(processDownloadBean.getProcessRecordList()), pBean);
		table.setValign(Valign.center);
	}

	private void createRiks(Sect sect) {
		Table table = sect.createTable(tblStyle(), new float[] { 25F });
		ParagraphBean pBean = new ParagraphBean(Align.left, Fonts.SONG, Constants.wuhao, true);
		table.createTableRow(new String[] { "重要EHS风险清单" }, pBean);
		table.setValign(Valign.center);
		pBean = new ParagraphBean(Align.center, Fonts.SONG, Constants.wuhao, true);
		table = sect.createTable(tblStyle(), new float[] { 7F, 9F, 9F });
		table.createTableRow(new String[] { "编号", "关键点描述", "控制措施" }, pBean);
		pBean = new ParagraphBean(Align.center, Fonts.SONG, Constants.wuhao, false);
		List<JecnRisk> risks = processDownloadBean.getRilsList();
		if (risks != null && !risks.isEmpty()) {
			for (JecnRisk risk : risks) {
				String[] data = new String[3];
				data[0] = nullToEmpty(risk.getRiskCode());
				data[1] = nullToEmpty(risk.getRiskName());
				data[2] = nullToEmpty(risk.getStandardControl());
				table.createTableRow(data, pBean);
			}
		}
		table.setValign(Valign.center);
	}

	private void createCharExplan(Sect sect) {
		sect.createParagraph(processDownloadBean.getFlowName() + "图释：", new ParagraphBean(Align.left, Fonts.SONG,
				Constants.xiaosan, true));
		Table table = sect.createTable(tblStyle(), new float[] { 1.25F, 2.7F, 6.01F, 1.25F, 2.5F, 2.5F, 2.5F, 2.5F,
				3.48F });
		table.createTableRow(new String[] { "步骤编号 ", "流程图步骤 ", "标准流程步骤描述 ", "完成时限（h） ", "风险 ", "组织 ", "输入 ", "输出 ",
				"涉及的系统 " }, tblTitleStyle());
		List<String[]> rowData = getActivityList();
		table.createTableRow(rowData, new ParagraphBean(Align.left, Fonts.SONG, Constants.xiaowu, false));
		table.setValign(Valign.center);
		table.setRowStyle(0, new ParagraphBean(Align.center, Fonts.SONG, Constants.xiaowu, true));
	}

	private List<String[]> getActivityList() {
		// 活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出 6办理时限目标值 7办理时限原始值 8角色id
		// allActivityShowList
		List<String[]> list = new ArrayList<String[]>();
		for (Object[] obj : processDownloadBean.getAllActivityShowList()) {
			String[] arr = new String[9];
			list.add(arr);
			Long artivityId = Long.valueOf(obj[8].toString());
			arr[0] = nullToEmpty(obj[0]);
			arr[1] = nullToEmpty(obj[2]);
			arr[2] = nullToEmpty(obj[3]);
			arr[3] = nullToEmpty(nullToEmpty(obj[7]) + "/" + nullToEmpty(obj[6]));
			arr[4] = nullToEmpty(getRisk(artivityId));
			arr[5] = nullToEmpty(getOrg(artivityId));
			arr[6] = nullToEmpty(obj[4]);
			arr[7] = nullToEmpty(obj[5]);
			arr[8] = nullToEmpty(getIt(artivityId));
		}
		return list;
	}

	private String getIt(Long artivityId) {
		return processDownloadBean.getActiveInfo().getItName(artivityId);
	}

	private String getOrg(Long activeId) {
		return processDownloadBean.getRoleInfo().getJinfengOrgName(activeId);
	}

	private String getRisk(Long artivityId) {
		return processDownloadBean.getActiveInfo().getRiskName(artivityId);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, float left, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaowu, false);
		Header header = sect.createHeader(HeaderOrFooterType.odd);

		TableBean tableBean = new TableBean();
		Table table = header.createTable(tableBean, new float[] { left, right });
		table.createTableRow(new String[] { "", nullToEmpty(processDownloadBean.getFlowInputNum()) }, pBean);

		ParagraphBean paragraphBean = new ParagraphBean(Align.right, Fonts.ARIAL, Constants.xiaowu, false);
		table.getRow(0).getCell(1).getParagraph(0).setParagraphBean(paragraphBean);
		table.setCellValignAndHeight(Valign.bottom, 0.6F);

		table.getRow(0).getCell(0).createParagraph(getLogoImage(4.79F, 1.01F));

		header.addParagraph(new Paragraph("", new ParagraphBean(PStyle.AF1)));
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 8F, 8F);
	}

	/***
	 * 创建流程图页眉页脚
	 * 
	 * @param sect
	 */
	private void createCharfhdr(Sect sect) {
		createCommhdr(sect, 8F, 16F);
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		textContentStyle.setInd(0F, 0F, 0F, 1F);
		textContentStyle.setSpace(0f, 0f, new ParagraphLineRule(12F, LineRuleType.AT_LEAST));
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("黑体", Constants.xiaosi, true);
		textTitleStyle.setInd(0F, 0F, 0.76F, 0F);
		textTitleStyle.setSpace(1F, 1F, leastVline12);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(2.5F);
		tblStyle.setBorderInsideH(0.5F);
		tblStyle.setBorderInsideV(0.5F);
		// 表格左缩进0.94 厘米
		tblStyle.setTableLeftInd(0F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

}
