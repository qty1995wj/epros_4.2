package com.jecn.dataimport.util;

import java.io.Serializable;

/**
 * 
 * 聚光科技,流程
 * 
 */
public class EipFlowBean implements Serializable {
	private Long projectId;//项目ID
	private Long flowId;//流程ID
	private String flowName;//流程名
	private String flowIdInput;//流程编号
	private Long taskId;//任务ID
	private String taskName;//任务名

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowIdInput() {
		return flowIdInput;
	}

	public void setFlowIdInput(String flowIdInput) {
		this.flowIdInput = flowIdInput;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
}
