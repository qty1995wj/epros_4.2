package com.jecn.epros.server.dao.popedom.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Hibernate;

import com.jecn.epros.server.bean.popedom.JecnRoleContent;
import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.popedom.UserRole;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnEnumUtil.AuthorName;
import com.jecn.epros.server.dao.popedom.IJecnRoleDao;

/**
 * @author zhangchen Apr 28, 2012
 * @description：角色管理Dao
 */
public class JecnRoleDaoImpl extends AbsBaseDao<JecnRoleInfo, Long> implements
		IJecnRoleDao {

	@Override
	public List<JecnRoleInfo> getDefaultRoleByPeopleId(Long peopleId)
			throws Exception {
		String hql = "select a from JecnRoleInfo as a,UserRole as b where a.roleId = b.roleId and b.peopleId=? and  a.filterId in"
				+ JecnCommonSql.getLoginDefaultAuthString();
		return this.listHql(hql, peopleId);
	}

	@Override
	public List<JecnRoleContent> getJecnRoleContentsByPeopleId(Long peopleId)
			throws Exception {
		String hql = "select c from JecnRoleInfo as a,UserRole as b ,JecnRoleContent as c where c.roleId =a.roleId and a.roleId = b.roleId and b.peopleId=?";
		return this.listHql(hql, peopleId);
	}

	@Override
	public void deleteRoles(Set<Long> setIds) throws Exception {
		// 删除角色人员信息表
		String hql = "delete from UserRole where roleId in";
		List<String> listSqls = JecnCommonSql.getSetSqls(hql, setIds);
		for (String delSql : listSqls) {
			execteHql(delSql);
		}
		// 删除角色权限信息表
		hql = "delete from JecnRoleContent where roleId in";
		listSqls = JecnCommonSql.getSetSqls(hql, setIds);
		for (String delSql : listSqls) {
			execteHql(delSql);
		}
		// 角色信息对象
		hql = "delete from JecnRoleInfo where roleId in";
		listSqls = JecnCommonSql.getSetSqls(hql, setIds);
		for (String delSql : listSqls) {
			execteHql(delSql);
		}
	}

	@Override
	public void deleteRoles(Long projectId) throws Exception {
		// 删除角色人员信息表
		String hql = "delete from UserRole where roleId in (select roleId from JecnRoleInfo where projectId=?)";
		execteHql(hql, projectId);
		// 删除角色权限信息表
		hql = "delete from JecnRoleContent where roleId in (select roleId from JecnRoleInfo where projectId=?)";
		execteHql(hql, projectId);
		// 角色信息对象
		hql = "delete from JecnRoleInfo where projectId =?";
		execteHql(hql, projectId);

	}

	@Override
	public Map<Long,AuthorName> getAdminAndDesignId() throws Exception {
		String hql = "select a.roleId,a.filterId from JecnRoleInfo as a where a.filterId in"
				+ JecnCommonSql.getDesignAuthString();
		List<Object[]> listObjectIds = this.listHql(hql);
		Map<Long,AuthorName> resultMap = new HashMap<Long, AuthorName>();
		for (Object[] obj : listObjectIds) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			String auth = obj[1].toString();
			if (JecnCommonSql.isAdmin(auth)) {
				resultMap.put(Long.valueOf(obj[0].toString()), AuthorName.admin);
			} else if (JecnCommonSql.isDesign(auth)) {
				resultMap.put(Long.valueOf(obj[0].toString()), AuthorName.design);
			} else if (JecnCommonSql.isSecondAdmin(auth)) {
				resultMap.put(Long.valueOf(obj[0].toString()), AuthorName.secondAdmin);
			}
		}
		return resultMap;
	}

	@Override
	public boolean isExistDefaultRole(String filterId, Long peopleId)
			throws Exception {
		String hql = "select count(a.roleId) from JecnRoleInfo as a,UserRole as b where b.peopleId=? and b.roleId = a.roleId and a.filterId=?";
		int count = this.countAllByParamsHql(hql, peopleId, filterId);
		if (count > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * 获取二级管理员对应角色ID
	 * 
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public Long getSecondAdminRoleId(Long peopleId) throws Exception{
		String sql = "SELECT RI.Role_Id" +
			"  FROM JECN_ROLE_INFO RI" + 
			" INNER JOIN JECN_USER_ROLE UR" + 
			"    ON RI.ROLE_ID = UR.ROLE_ID" + 
			"   AND UR.PEOPLE_ID = ?" + 
			" WHERE RI.FILTER_ID = '" + JecnCommonSql.getStrSecondAdmin()+"'";
		List<Long> list = this.listObjectNativeSql(sql, "Role_Id", Hibernate.LONG, peopleId);
		if(list.isEmpty()){
			return -1L;
		}
		return list.get(0);
	}
	
	@Override
	public List<Object[]> getSecondAdminObjs(Long preId) throws Exception {
		String sql = "select distinct t.role_id,t.role_name,t.FILTER_ID,CHILD_R.PER_ID,t.sort_id from jecn_role_info t " +
				"  LEFT JOIN jecn_role_info CHILD_R ON CHILD_R.PER_ID = T.ROLE_ID" +
				" where  T.PER_ID = ?  AND t.FILTER_ID NOT IN " +JecnCommonSql.getDefaultAuthString()+" order by t.sort_id asc";
		return this.listNativeSql(sql,preId);
	}
	
	@Override
	public void deleteUserRoleByRoleId(Long roleId) throws Exception {
		String sql = "DELETE FROM JECN_USER_ROLE WHERE ROLE_ID = ?";
		this.execteNative(sql, roleId);
	}

	@Override
	public void addUserRole(UserRole userRole) throws Exception {
		this.getSession().save(userRole);
	}
	
	/**
	 * 获取角色对应人员
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> selectUserRoleByRoleId(Long roleId) throws Exception {
		String trueName = "JU.TRUE_NAME";
		if(JecnConfigTool.isIflytekLogin()){
			trueName =  "JU.LOGIN_NAME";
		}
		String sql = "SELECT JU.PEOPLE_ID,"+trueName+" FROM JECN_USER_ROLE UR INNER JOIN JECN_USER JU " +
				"	ON JU.PEOPLE_ID = UR.PEOPLE_ID WHERE ROLE_ID = ?";
		return this.listNativeSql(sql, roleId);

	}
	
	
	/**
	 * 管理员总数
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getDesignerUserTotal(String userMark) throws Exception{
		String sql = "SELECT COUNT(T.ROLE_ID)" +
		"  FROM JECN_ROLE_INFO T, JECN_USER_ROLE JUR, JECN_USER JU" + 
		" WHERE T.FILTER_ID IN " + userMark + 
		"   AND T.ROLE_ID = JUR.ROLE_ID" + 
		"   AND JUR.PEOPLE_ID = JU.PEOPLE_ID" + 
		"   AND JU.ISLOCK = 0";
		return this.countAllByParamsNativeSql(sql);
	}
	
	@Override
	public List<Long> getDesignerAuths(String mark) throws Exception {
		String sql = "SELECT JUR.USER_POLE_ID" +
					"  FROM JECN_ROLE_INFO T, JECN_USER_ROLE JUR, JECN_USER JU" + 
					" WHERE T.FILTER_ID IN " + mark + 
					"   AND T.ROLE_ID = JUR.ROLE_ID" + 
					"   AND JUR.PEOPLE_ID = JU.PEOPLE_ID" + 
					"   AND JU.ISLOCK = 0 ORDER BY JUR.USER_POLE_ID DESC";
		return this.listObjectNativeSql(sql, "USER_POLE_ID", Hibernate.LONG);
	}

	
	/**
	 * 根据角色人员关联表主键ID删除角色和人员关联关系
	 * 
	 * @param list
	 * @throws Exception
	 */
	@Override
	public void deleteUserRoleByIds(List<Long> list) throws Exception {
		String sql = " DELETE FROM JECN_USER_ROLE WHERE  USER_POLE_ID IN" + JecnCommonSql.getIds(list);
		this.execteNative(sql);
	}
	
	@Override
	public void insertUserRoleLog(List<Long> lists) throws Exception {
		String sql = "   INSERT INTO JECN_USER_ROLE_LOG (USER_POLE_ID,ROLE_ID,PEOPLE_ID,CREATE_TIME)  " +
						"SELECT USER_POLE_ID,ROLE_ID,PEOPLE_ID," + JecnCommonSql.getDateTimeByDB() 
 						+ " FROM JECN_USER_ROLE WHERE USER_POLE_ID IN" + JecnCommonSql.getIds(lists);
		this.execteNative(sql);
	}
	
	/**
	 * 二级管理员下人员权限结果集
	 * 
	 * @param projectId
	 * @param ids
	 * @param name
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getSecondAdminRolesByName(Long projectId,Long roleId,String name) throws Exception{
		String sql = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = "WITH ROLE_PRIM AS" +
			" (SELECT T.*" + 
			"    FROM JECN_ROLE_INFO T" + 
			"  CONNECT BY PRIOR T.ROLE_ID = T.PER_ID" + 
			"   START WITH T.ROLE_ID = ?)" + 
			" SELECT T.ROLE_ID, T.ROLE_NAME, T.PER_ID, T.IS_DIR, T.FILTER_ID" + 
			"    FROM ROLE_PRIM T" + 
			"   WHERE T.PROJECT_ID = ?" + 
			"     AND T.IS_DIR = 0" + 
			"     AND T.ROLE_NAME LIKE ? AND T.ROLE_ID<>"+roleId + 
			"   ORDER BY T.PER_ID, T.SORT_ID, T.ROLE_ID";
		}else {
			sql = "WITH ROLE_PRIM AS" +
				" (SELECT *" + 
				"    FROM jecn_role_info RT" + 
				"   WHERE RT.ROLE_ID = ? " +
				"  UNION ALL" + 
				"  SELECT RTT.*" + 
				"    FROM ROLE_PRIM" + 
				"   INNER JOIN jecn_role_info RTT" + 
				"      ON ROLE_PRIM.ROLE_ID = RTT.per_id)" + 
				"SELECT T.ROLE_ID, T.ROLE_NAME, T.PER_ID, T.IS_DIR, T.FILTER_ID" + 
				"  FROM ROLE_PRIM T" + 
				" WHERE T.PROJECT_ID = ?" + 
				"   AND T.IS_DIR = 0" + 
				"   AND T.ROLE_NAME LIKE ? AND T.ROLE_ID<>"+roleId  + 
				" ORDER BY T.PER_ID, T.SORT_ID, T.ROLE_ID";
		}
		return this.listNativeSql(sql, roleId, projectId, "%" + name + "%");
	}
	
	/**
	 * 用户角色权限
	 * 
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getRoleAuthDesignerCount(Long peopleId) throws Exception{
		String sql = "SELECT COUNT(RI.ROLE_ID)" +
			"  FROM JECN_ROLE_INFO RI" + 
			" INNER JOIN JECN_USER_ROLE UR" + 
			"    ON RI.ROLE_ID = UR.ROLE_ID" + 
			"   AND UR.PEOPLE_ID = ?" +
			"  WHERE RI.FILTER_ID IN" + JecnCommonSql.getDesignAuthString();
		return this.countAllByParamsNativeSql(sql, peopleId);
	}
} 
