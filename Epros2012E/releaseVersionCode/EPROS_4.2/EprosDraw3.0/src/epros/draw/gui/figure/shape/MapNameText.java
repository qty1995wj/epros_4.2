package epros.draw.gui.figure.shape;

import java.awt.Graphics2D;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseRect;
import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 
 * 流程图/流程地图/组织图名称文本框
 * 
 * @author ZHANGXH
 * 
 */
public class MapNameText extends BaseRect {

	private int fontPx;
	private static final int minWidth = 400;

	public MapNameText(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	public void resizeByFigureText() {
		fontPx = this.getFlowElementData().getFontSize();
		Pattern pattern = Pattern.compile("[\u4E00-\u9FA5]");
		Matcher matcher = null;
		String figureText = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getName();
		// 预留一些宽度作为左右边界
		int curWidth = fontPx * 4;
		for (int i = 0; i < figureText.length(); i++) {
			matcher = pattern.matcher(String.valueOf(figureText.charAt(i)));
			if (matcher.matches()) {
				curWidth += fontPx;
			} else {
				curWidth += fontPx / 2;
			}
		}
		this.flowElementData.setFigureSizeX(curWidth >= minWidth ? curWidth : minWidth);
		this.flowElementData.setFigureSizeY((int) (fontPx * 1.5));

		// 纵向流程图
		if (!JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().isHFlag()) {
			// 纵向流程图 名称输入框 高度和宽度互换位置
			int width = this.flowElementData.getFigureSizeX();
			// 字体的宽度和高度在计算上有所不同，暂时多增加0.2的系数来避免文本显示不全
			width *= 1.2;
			this.flowElementData.setFigureSizeX(this.flowElementData.getFigureSizeY());
			this.flowElementData.setFigureSizeY(width);
		}
		this.reSetFigureSize();
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.setPaint(flowElementData.getBodyColor());
	}
}
