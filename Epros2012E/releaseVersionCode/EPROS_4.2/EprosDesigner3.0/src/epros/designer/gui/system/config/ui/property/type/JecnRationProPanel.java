package epros.designer.gui.system.config.ui.property.type;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 合理化建议配置
 * 
 * @author ZHANGXIAOHU
 * 
 */
public class JecnRationProPanel extends JecnAbstractPropertyBasePanel implements ActionListener {
	/** 处理合理化建议标签 */
	private JLabel rationLable;
	/** 处理人 */
	private JLabel processingPerson;

	/** 系统管理员复选 */
	private JecnConfigCheckBox sysBox;
	/** 流程责任人复选 */
	private JecnConfigCheckBox flowRefBox;

	/** *******单选 提交合理化建议********* */
	/** 公开单选按钮 */
	private JecnConfigRadioButton pubRadio;
	/** 秘密单选按钮 */
	private JecnConfigRadioButton secretRadio;

	/** *******处理人********* */
	/** 责任人 */
	private JecnConfigRadioButton personLiable;
	/** 专员 */
	private JecnConfigRadioButton commissioner;

	public JecnRationProPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 处理合理化建议：
		rationLable = new JLabel(JecnProperties.getValue("rationProposals"));
		// 处理人:
		processingPerson = new JLabel(JecnProperties.getValue("processingPerson"));

		// 系统管理员
		sysBox = new JecnConfigCheckBox(JecnProperties.getValue("systemMangePerson"));

		// 流程/制度责任人
		if (JecnConstants.versionType == 99) {
			flowRefBox = new JecnConfigCheckBox(JecnProperties.getValue("flowRuleResPeople"));
		} else {
			// 流程责任人
			flowRefBox = new JecnConfigCheckBox(JecnProperties.getValue("flowResponsePer"));
		}
		// 复选框设置

		// 默认不选中
		sysBox.setSelected(false);
		flowRefBox.setSelected(false);

		sysBox.setOpaque(false);
		flowRefBox.setOpaque(false);

		sysBox.addActionListener(this);
		flowRefBox.addActionListener(this);

		/** *******单选 提交合理化建议********* */
		// 公开单选按钮 (0：秘密 1：公开) 公开
		pubRadio = new JecnConfigRadioButton(JecnProperties.getValue("gongKai"));
		// 秘密单选按钮 (0：秘密 1：公开) 秘密
		secretRadio = new JecnConfigRadioButton(JecnProperties.getValue("secret"));
		/** *******处理人********* */
		// 责任人
		personLiable = new JecnConfigRadioButton(JecnProperties.getValue("personLiable"));
		// 专员
		commissioner = new JecnConfigRadioButton(JecnProperties.getValue("commissioner"));

		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(pubRadio);
		buttonGroup.add(secretRadio);

		ButtonGroup buttonGroupTo = new ButtonGroup();
		buttonGroupTo.add(personLiable);
		buttonGroupTo.add(commissioner);

		// 默认公开
		pubRadio.setSelected(true);

		pubRadio.setActionCommand("pubRadio");
		secretRadio.setActionCommand("secretRadio");
		personLiable.setActionCommand("personLiable");
		commissioner.setActionCommand("commissioner");

		pubRadio.setOpaque(false);
		secretRadio.setOpaque(false);
		personLiable.setOpaque(false);
		commissioner.setOpaque(false);

		// 添加监听
		pubRadio.addActionListener(this);
		secretRadio.addActionListener(this);
		personLiable.addActionListener(this);
		commissioner.addActionListener(this);

		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setLayout(new GridBagLayout());
	}

	private void initLayout() {
		Insets insets = new Insets(5, 5, 5, 5);
		GridBagConstraints c = null;
		// TODO 处理合理化建议标签label
		// c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		// this.add(rationLable, c);
		// 系统管理员
		// c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		// this.add(sysBox, c);
		// 流程责任人
		// c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		// this.add(flowRefBox, c);

		JecnPanel submitPanel = new JecnPanel();
		submitPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("submitProposals")));//提交合理化建议
		submitPanel.setLayout(new GridBagLayout());
		submitPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		pubRadio.setBackground(Color.red);
		// 公开
		submitPanel.add(pubRadio, c);
		// 秘密
		submitPanel.add(secretRadio, c);

		JecnPanel processPanel = new JecnPanel();
		processPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("processingPerson")));//处理人
		processPanel.setLayout(new GridBagLayout());
		processPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 责任人
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		processPanel.add(personLiable, c);
		// 专员
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		processPanel.add(commissioner, c);

		int rows = 0;
		// 提交合理化建议
		c = new GridBagConstraints(0, rows, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		this.add(submitPanel, c);
		rows++;
		// 处理人
		c = new GridBagConstraints(0, rows, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		this.add(processPanel, c);
		rows++;

		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
				insets, 0, 0);

		this.add(new JLabel(), c);
	}

	@Override
	public boolean check() {
		return true;
	}

	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 唯一标识
		String mark = null;
		String value = null;
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {// 非表数据
			// 唯一标识
			mark = itemBean.getMark();
			// 值
			value = itemBean.getValue();
			if (ConfigItemPartMapMark.proPubOrSecRation.toString().equals(mark)) {// 提交合理化建议
				initPubAndSecData(itemBean);
			} else if (ConfigItemPartMapMark.proSystemRation.toString().equals(mark)) {
				sysBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {// 1：启用
					sysBox.setSelected(true);
				} else {
					sysBox.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.proFlowRefRation.toString().equals(mark)) {// 流程地图(0：秘密1：公开)
				flowRefBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {// 1：启用
					flowRefBox.setSelected(true);
				} else {
					flowRefBox.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.commissioner.toString().equals(mark)) {// 合理化建议处理人（专员）
				commissioner.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {// 1：启用
					commissioner.setSelected(true);
				} else {
					commissioner.setSelected(false);
				}
			} else if (ConfigItemPartMapMark.personLiable.toString().equals(mark)) {// 合理化建议处理人（责任人）
				personLiable.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {// 1：启用
					personLiable.setSelected(true);
				} else {
					personLiable.setSelected(false);
				}
			}

		}
	}

	/**
	 * 
	 * 提交合理化建议 初始化
	 * 
	 * @param itemBean
	 *            JecnConfigItemBean
	 */
	private void initPubAndSecData(JecnConfigItemBean itemBean) {
		if (itemBean == null) {
			return;
		}
		pubRadio.setItemBean(itemBean);
		secretRadio.setItemBean(itemBean);

		// 值
		String value = itemBean.getValue();

		if (JecnConfigContents.ITEM_PUB_OPEN.equals(value)) {// 全局权限：公开
			// (0：秘密;1：公开)
			pubRadio.setSelected(true);
		} else {
			secretRadio.setSelected(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigRadioButton) {
			JecnConfigRadioButton btn = (JecnConfigRadioButton) e.getSource();
			if (btn.isSelected()) {// 是否选中
				// 数据对象
				JecnConfigItemBean itemBean = btn.getItemBean();
				if (itemBean == null) {
					return;
				}
				// (0：秘密 1：公开)
				if ("pubRadio".equals(btn.getActionCommand())) {// 公开
					btn.getItemBean().setValue(JecnConfigContents.ITEM_PUB_OPEN);
				} else if ("secretRadio".equals(btn.getActionCommand())) {// 秘密
					btn.getItemBean().setValue(JecnConfigContents.ITEM_PUB_SECRET);
				} else if ("commissioner".equals(btn.getActionCommand())) {
					commissioner.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					personLiable.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				} else if ("personLiable".equals(btn.getActionCommand())) {
					personLiable.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					commissioner.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}
		}
		if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == sysBox) { // 1：选中；0不选中
				if (sysBox.isSelected()) {// 选中
					sysBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					sysBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			} else if (e.getSource() == flowRefBox) {// 1：选中；0不选中
				if (flowRefBox.isSelected()) {// 选中
					flowRefBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					flowRefBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}
		}
	}
}
