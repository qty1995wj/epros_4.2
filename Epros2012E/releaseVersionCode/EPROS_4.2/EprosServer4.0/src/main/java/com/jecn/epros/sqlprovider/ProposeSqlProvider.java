package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.propose.*;
import com.jecn.epros.domain.security.AuthorityNodesData;
import com.jecn.epros.domain.security.NodeIdsData;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.util.ConfigUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ProposeSqlProvider {

    private ProposeSearch search;

    public String getProposeNum(Map<String, Object> paramMap) {
        // 思路 管理员可以看到所有的合理化建议，普通人员可以看到自己提交的，需要自己处理的以及公开的，以前的权限的不用了
        // 登陆人是否是系统管理员
        Boolean isAdmin = (Boolean) paramMap.get("isAdmin");
        // 登陆人ID
        Long loginPeopleId = (Long) paramMap.get("loginPeopleId");
        String sql = "with proposes as (" + getMaxVersionPropose(paramMap) + ")";
        // 合理化建议数据集合
        sql += "select count(pub.id) from (";
        sql += getICanSeePropose(isAdmin, loginPeopleId);
        sql += ") pub";
        return sql;
    }

    public String fetchProposes(Map<String, Object> paramMap) {
        // 登陆人是否是系统管理员
        Boolean isAdmin = (Boolean) paramMap.get("isAdmin");
        // 登陆人ID
        Long loginPeopleId = (Long) paramMap.get("loginPeopleId");
        // 合理化建议数据集合
        String sql = "with proposes as (" + getMaxVersionPropose(paramMap) + ")";
        ;
        sql += " select pub.id,pub.RELATION_ID,pub.PROPOSE_CONTENT,pub.REPLY_CONTENT,pub.state,pub.REPLY_PERSON,pub.REPLY_TIME,pub.CREATE_PERSON,pub.CREATE_TIME";
        sql += ",pub.UPDATE_PERSON,pub.UPDATE_TIME,pub.RELATION_TYPE,juu.ISLOCK,ju.ISLOCK as create_is_lock,ju.true_name as create_people_name,juu.true_name as replay_people_name,";
        sql += "jrpf.id as FILE_ID,jrpf.FILE_NAME as FILE_NAME";
        sql += " from (";
        sql += getICanSeePropose(isAdmin, loginPeopleId);
        sql += " ) pub";
        sql += " left join JECN_RTNL_PROPOSE_FILE jrpf on jrpf.propose_id=pub.id  ";
        sql += " left join jecn_user ju on pub.CREATE_PERSON=ju.PEOPLE_ID and ju.ISLOCK=0";
        sql += " left join jecn_user juu on pub.REPLY_PERSON = juu.PEOPLE_ID and juu.ISLOCK=0";
        sql += " order by pub.update_time desc";
        return sql;
    }

    public String fetchProposeRelatedFlows(Map<String, Object> paramMap) {

        Set<Long> setFlowIds = (Set<Long>) paramMap.get("setFlowIds");
        String flowSql = "select jfs.flow_id as id,jfs.flow_name as name,0 as type,jfs.flow_id_input as number_id,jfo.org_id as org_id,jfo.org_name as org_name,jfbi.type_res_people as res_type"
                + "   ,case when jfbi.type_res_people=1 then jfoi.figure_id else ju.people_id end as res_id"
                + "   ,case when jfbi.type_res_people=1 then jfoi.figure_text else ju.true_name end as res_name"
                + "    from jecn_flow_structure jfs,jecn_flow_basic_info jfbi"
                + "    left join jecn_flow_related_org jfro on jfro.flow_id = jfbi.flow_id"
                + "    left join jecn_flow_org jfo on jfo.org_id = jfro.org_id "
                + "    left join jecn_user ju on jfbi.type_res_people=0 and ju.people_id = jfbi.res_people_id and ju.islock=0"
                + "    left join jecn_flow_org_image jfoi on jfbi.type_res_people=1 and jfoi.figure_id = jfbi.res_people_id"
                + "    left join jecn_flow_org org on org.org_id = jfoi.org_id "
                + "    where jfbi.flow_id=jfs.flow_id  and jfs.flow_id in " + SqlCommon.getIdsSet(setFlowIds);

        return flowSql;
    }

    public String fetchProposeRelatedRules(Map<String, Object> paramMap) {
        Set<Long> setRuleIds = (Set<Long>) paramMap.get("setRuleIds");
        String ruleSql = "select jfbi.id as id,jfbi.rule_name as name,1 as type,jfbi.rule_number as number_id,jfo.org_id as org_id,jfo.org_name as org_name,jfbi.type_res_people as res_type"
                + "   ,case when jfbi.type_res_people=1 then jfoi.figure_id else ju.people_id end as res_id"
                + "   ,case when jfbi.type_res_people=1 then jfoi.figure_text else ju.true_name end as res_name"
                + "    from jecn_rule jfbi" + "    left join jecn_flow_org jfo on jfo.org_id = jfbi.org_id "
                + "    left join jecn_user ju on jfbi.type_res_people=0 and ju.people_id = jfbi.res_people_id and ju.islock=0"
                + "    left join jecn_flow_org_image jfoi on jfbi.type_res_people=1 and jfoi.figure_id = jfbi.res_people_id"
                + "    left join jecn_flow_org org on org.org_id = jfoi.org_id " + "    where  jfbi.id in "
                + SqlCommon.getIdsSet(setRuleIds);

        return ruleSql;

    }

    /**
     * 我可以查看的合理化建议 管理员全部可以看，非管理员可以查看我自己提交的，我是处理人的，公开的
     *
     * @param isAdmin
     * @param peopleId
     * @return
     */
    private String getICanSeePropose(boolean isAdmin, Long peopleId) {
        String sql = "";
        if (isAdmin) {
            sql = "select jrp.* from proposes jrp";
        } else {
            sql = "select jrp.* from proposes jrp where jrp.SHOW_TYPE=1"
                    + "	UNION"
                    + "	select jrp.* from proposes jrp where jrp.CREATE_PERSON=" + peopleId
                    + "	UNION"
                    + "	select jrp.* from proposes jrp ,PROPOSE_TOPIC pt where pt.RELATED_ID=jrp.RELATION_ID and pt.TYPE=jrp.RELATION_TYPE and pt.HANDLE_PEOPLE=" + peopleId;
        }
        return sql;
    }

    public String updatePropose(Propose propose) {

        String sql = "update JECN_RTNL_PROPOSE set STATE = #{state}";
        // 回复操作
        if (propose.getReplyContent() != null && !"".equals(propose.getReplyContent()) && propose.getReplyPersonId() != null) {// 有回复内容说明是回复操作
            sql = sql + " , REPLY_CONTENT = #{replyContent},REPLY_PERSON= #{replyPersonId}"
                    + " ,REPLY_TIME = #{replyTime} ";
            sql = sql + " , UPDATE_PERSON = #{replyPersonId}"
                    + " , UPDATE_TIME = #{updateTime},propose_content=#{proposeContent},show_type=#{showType} where ID = #{id}";
        } else {
            sql = sql + " , UPDATE_PERSON = #{updatePersonId}"
                    + " , UPDATE_TIME = #{updateTime},propose_content=#{proposeContent},show_type=#{showType}  where ID = #{id}";
        }
        return sql;
    }

    public String savePropose(Propose propose) {

        String sql = "insert into jecn_rtnl_propose(id,"
                + "                         relation_id,"
                + "                         relation_type,"
                + "                         propose_content,"
                + "                         reply_content,"
                + "                         state,"
                + "                         create_person,"
                + "                         create_time,"
                + "                         update_person,"
                + "                         update_time,"
                + "                         show_type,"
                + "                         history_id"
                + "                         )" + "                         values("
                + "                         #{id},"
                + "                         #{relationId},"
                + "                         #{type},"
                + "                         #{proposeContent},"
                + "                         #{replyContent},"
                + "                         #{state},"
                + "                         #{createPersonId},"
                + "                         #{createTime},"
                + "                         #{updatePersonId},"
                + "                         #{updateTime},"
                + "                         #{showType},"
                + "                         #{historyId,jdbcType=DECIMAL}"
                + "                         )";

        return sql;

    }

    public String saveProposeFile(ProposeFile proposeFile) {
        String sql = "insert into JECN_RTNL_PROPOSE_FILE(id," + "                         propose_id,"
                + "                         file_name," + "                         file_stream,"
                + "                         create_person," + "                         create_time,"
                + "                         update_person," + "                         update_time "
                + "                         )" + "                         values(" + "                         #{id},"
                + "                         #{proposeId}," + "                         #{fileName},"
                + "                         #{fileStream,jdbcType=BLOB},"
                + "                         #{createPersonId}," + "                         #{createTime},"
                + "                         #{updatePersonId}," + "                         #{updateTime}"
                + "                         )";
        return sql;

    }

    public String saveProposeTopic(ProposeTopic topic) {
        String sql = "insert into PROPOSE_TOPIC(RELATED_ID,"
                + "                         TYPE,"
                + "                         NAME,"
                + "                         CREATE_TIME,"
                + "                         UPDATE_TIME,"
                + "                         HANDLE_PEOPLE"
                + "                         )"
                + "                         values("
                + "                         #{relatedId},"
                + "                         #{type},"
                + "                         #{name},"
                + "                         #{createTime},"
                + "                         #{updateTime},"
                + "                         #{handlePeople,jdbcType=VARCHAR}"
                + "                         )";
        return sql;

    }

    public String updateProposeFile(ProposeFile file) {
        String sql = "UPDATE JECN_RTNL_PROPOSE_FILE SET FILE_NAME =#{fileName}, file_stream =#{fileStream,jdbcType=BLOB}, update_person =#{ updatePersonId }, update_time =#{ updateTime } where id=#{id}";
        return sql;
    }

    public String getRelated(Propose propose) {

        String sql = "";
        /** 0流程，1制度，2流程架构，3文件，4风险，5标准 **/
        String tableName = "";
        String name = "";
        String id = "";
        int type = propose.getType();
        if (type == 0) {
            id = "FLOW_ID";
            name = "FLOW_NAME";
            tableName = "JECN_FLOW_STRUCTURE";
        } else if (type == 1) {
            id = "ID";
            name = "RULE_NAME";
            tableName = "JECN_RULE";
        } else if (type == 2) {
            id = "FLOW_ID";
            name = "FLOW_NAME";
            tableName = "JECN_FLOW_STRUCTURE";
        } else if (type == 3) {
            id = "FILE_ID";
            name = "FILE_NAME";
            tableName = "JECN_FILE";
        } else if (type == 4) {
            id = "ID";
            name = "NAME";
            tableName = "JECN_RISK";
        } else if (type == 5) {
            id = "CRITERION_CLASS_ID";
            name = "CRITERION_CLASS_NAME";
            tableName = "JECN_CRITERION_CLASSES";
        }

        if (type < 4) {// 有文控信息的
            sql = " SELECT DISTINCT JFS." + name + " AS RELATED_NAME,	jthn.id as HISTORY_ID"
                    + " FROM " + tableName + " jfs"
                    + " LEFT JOIN JECN_TASK_HISTORY_NEW jthn ON jfs.HISTORY_ID = jthn.ID" + " WHERE" + "	JFS." + id
                    + " =#{relationId}";
        } else {
            sql = " SELECT DISTINCT   JFS." + name + " AS RELATED_NAME" + " FROM " + tableName + " JFS" + " WHERE"
                    + "	JFS." + id + " =#{relationId}";
        }

        return sql;

    }

    public String fetchMyTopic(Map<String, Object> paramMap) {
        Integer type = Integer.valueOf(paramMap.get("type").toString());
        String sql = "			SELECT DISTINCT" + "				pt.*" + "			FROM"
                + "				PROPOSE_TOPIC pt"
                + "			INNER JOIN JECN_RTNL_PROPOSE jrp ON pt.RELATED_ID = jrp.RELATION_ID"
                + "			AND pt.TYPE = jrp.RELATION_TYPE";
        String condition = "";
        if (type == 0) {// 我处理的
            condition = "			AND pt.HANDLE_PEOPLE =#{ peopleId }" + "			AND jrp.STATE IN (1,2,3)";
        } else if (type == 1) {// 我未处理的
            condition = "			AND pt.HANDLE_PEOPLE =#{ peopleId }" + "			AND jrp.STATE = 0";
        } else if (type == 2) {// 我提交的已处理的
            condition = "			AND jrp.CREATE_PERSON =#{ peopleId }" + "			AND jrp.STATE IN (1,2,3)";
        } else if (type == 3) {// 我提交的未处理的
            condition = "			AND jrp.CREATE_PERSON =#{ peopleId }" + "			AND jrp.STATE = 0";
        }
        sql += condition;
        return sql;

    }

    public String searchProposeTopicByType(Integer type) {

        String sql = "";
        if (type == 0) {// 未设置处理人的
            sql = "select * from propose_topic where handle_people is null";
        } else {// 设置处理人的
            sql = "select * from propose_topic where handle_people is not null";
        }
        return sql;
    }

    public String searchProposeTopicNumByType(Integer type) {

        String sql = "";
        if (type == 0) {// 未设置处理人的
            sql = "select count(1) from propose_topic where handle_people is null";
        } else {// 设置处理人的
            sql = "select count(1) from propose_topic where handle_people is not null";
        }
        return sql;
    }

    public String searchProposeTopic(Map<String, Object> paramMap) {
        AuthorityNodesData nodesData = AuthenticatedUserUtil.getNodesData();
        String peopleId = paramMap.get("peopleId").toString();
        Boolean isAdmin = (Boolean) paramMap.get("isAdmin");
        String sql = "select T.RELATED_ID,T.TYPE,T.NAME,T.CREATE_TIME,T.UPDATE_TIME,T.HANDLE_PEOPLE,ju.true_name as HANDLE_PEOPLE_NAME from " +
                " ( select distinct pt.* " + getProposeTopicQuerySql(paramMap) + ")T" +
                " left  join jecn_user ju on T.handle_people=ju.people_id" +
                " where exists (select *" +
                "          from jecn_flow_structure jf" +
                "         where jf.flow_id = t.related_id" +
                "           and jf.del_state = 0 " + inserProposeInSql(nodesData.getProcessNode(), isAdmin) + ")" +
                "    or exists (select *" +
                "          from jecn_rule r" +
                "         where r.id = t.related_id" +
                "           and r.del_state = 0 " + inserProposeInSql(nodesData.getRuleNode(), isAdmin) + ")" +
                "    or exists (select *" +
                "          from jecn_file f" +
                "         where f.file_id = t.related_id" +
                "           and f.del_state = 0 " + inserProposeInSql(nodesData.getFileNode(), isAdmin) + ")";
        return sql;
    }

    public String searchProposeTopicNum(Map<String, Object> paramMap) {
        AuthorityNodesData nodesData = AuthenticatedUserUtil.getNodesData();
        String peopleId = paramMap.get("peopleId").toString();
        Boolean isAdmin = (Boolean) paramMap.get("isAdmin");
        String sql = "select count(1) from " +
                " ( select distinct pt.* " + getProposeTopicQuerySql(paramMap) + ")T " +
                " left  join jecn_user ju on T.handle_people=ju.people_id" +
                " where exists (select *" +
                "          from jecn_flow_structure jf" +
                "         where jf.flow_id = t.related_id" +
                "           and jf.del_state = 0 " + inserProposeInSql(nodesData.getProcessNode(), isAdmin) + ")" +
                "    or exists (select *" +
                "          from jecn_rule r" +
                "         where r.id = t.related_id" +
                "           and r.del_state = 0 " + inserProposeInSql(nodesData.getRuleNode(), isAdmin) + ")" +
                "    or exists (select *" +
                "          from jecn_file f" +
                "         where f.file_id = t.related_id" +
                "           and f.del_state = 0 " + inserProposeInSql(nodesData.getFileNode(), isAdmin) + ")";
        return sql;
    }

    private String inserProposeInSql(NodeIdsData idsData, Boolean isAdmin) {
        if (isAdmin) {
            return "";
        }
        return ((idsData == null || idsData.getOperationIds().isEmpty()) ? " and 1=2" : " and t.related_id in " + SqlCommon.getIdsSet(idsData.getOperationIds()));
    }

    private String getProposeTopicQuerySql(Map<String, Object> paramMap) {
        TopicSearch search = (TopicSearch) paramMap.get("topicSearch");
        Boolean isAdmin = (Boolean) paramMap.get("isAdmin");
        Long peopleId = (Long) paramMap.get("peopleId");
        String sql = "from " + getICanSeeProposeTopic(isAdmin, peopleId) + " pt where 1=1 ";
        if (search.getType() != null && search.getType() != -1) {
            sql += " and pt.type=#{topicSearch.type}";
        }
        if (search.getHandlePeople() != null) {
            sql += " and pt.handle_people=#{topicSearch.handlePeople}";
        }
        if (StringUtils.isNotBlank(search.getName())) {
            sql += " and pt.name like '%" + search.getName() + "%'";
        }
        if (search.getCreatePeople() != null) {
            sql += " AND EXISTS (SELECT" + "		*" + "	FROM" + "		JECN_RTNL_PROPOSE jrp" + "	WHERE"
                    + "		pt.related_id = jrp.RELATION_ID"
                    + "	AND pt.type = jrp.RELATION_type and create_person=#{topicSearch.createPeople})";
        }
        return sql;
    }

    private String getICanSeeProposeTopic(Boolean isAdmin, Long peopleId) {
        String sql = " (select * from propose_topic  ) ";
        if (!isAdmin) {
            sql = " (select * from propose_topic where handle_people=" + peopleId + ")";
        }
        return sql;
    }

    /**
     * 合理化统计图表
     *
     * @param map
     * @return
     */
    public String findProposeTotalChart(Map<String, Object> map) {
        ProposeTotalSearchBean proposeTotalSearchBean = (ProposeTotalSearchBean) map.get("proposeTotalSearchBean");
        // 选择资源ID
        Long fileId = proposeTotalSearchBean.getFileId();
        // 0组织，1岗位，2人员
        int orgType = proposeTotalSearchBean.getOrgType();
        // 选择比较的组织、岗位、人员
        List<Long> orgIds = proposeTotalSearchBean.getOrgSetIds();

        Long projectId = (Long) map.get("projectId");
        // 日志文件类型
        int logType = proposeTotalSearchBean.convertToLogType();
        // 合理化建议类型 0流程，1制度，2流程架构，3文件，4风险，5标准
        int proposeType = proposeTotalSearchBean.convertToProposeType();

        switch (orgType) {
            case 0:
                return "SELECT TOTALS.ID,TOTALS.NAME,TOTALS.DATE_STR,COUNT(TOTALS.PROPOSE_ID) AS TOTAL_COUNT  FROM ("
                        + "    SELECT PUB.ORG_ID AS ID," + "           PUB.ORG_NAME AS NAME,"
                        + "           TO_CHAR(JPR.CREATE_TIME, 'YYYYMM') AS DATE_STR," + "           JPR.ID AS PROPOSE_ID"
                        + "      FROM JECN_RTNL_PROPOSE          JPR," + "           JECN_USER                  JU,"
                        + "           JECN_USER_POSITION_RELATED U_POS," + "           JECN_FLOW_ORG_IMAGE        POS,"
                        + "           JECN_FLOW_ORG              ORG"
                        + "     INNER JOIN JECN_FLOW_ORG PUB ON PUB.ORG_ID IN " + SqlCommon.getIds(orgIds)
                        + "                                 AND PUB.T_PATH LIKE " + SqlCommon.getJoinFunc("ORG")
                        + "     WHERE ORG.ORG_ID = POS.ORG_ID" + "       AND POS.FIGURE_ID = U_POS.FIGURE_ID"
                        + "       AND U_POS.PEOPLE_ID = JU.PEOPLE_ID" + "       AND JU.ISLOCK = 0"
                        + "       AND JU.PEOPLE_ID = JPR.CREATE_PERSON" + "       AND "
                        + SqlCommon.timeBetweenTheTwoSql(proposeTotalSearchBean.getStartTime(),
                        proposeTotalSearchBean.getEndTime(), "JPR.CREATE_TIME")
                        + "       AND JPR.RELATION_TYPE=" + proposeType + " AND JPR.RELATION_ID in ("
                        + getFileIdByFileType(fileId, projectId, logType) + ")" + "       ) TOTALS"
                        + "   GROUP BY TOTALS.ID,TOTALS.NAME,TOTALS.DATE_STR" + "   ORDER BY TOTALS.ID,TOTALS.DATE_STR";
            case 1:
                return "SELECT TOTALS.ID,TOTALS.NAME,TOTALS.DATE_STR,COUNT(TOTALS.PROPOSE_ID) AS TOTAL_COUNT   FROM ("
                        + "    SELECT POS.FIGURE_ID AS ID," + "           POS.FIGURE_TEXT AS NAME,"
                        + "           TO_CHAR(JPR.CREATE_TIME, 'YYYYMM') AS DATE_STR," + "           JPR.ID AS PROPOSE_ID"
                        + "      FROM JECN_RTNL_PROPOSE          JPR," + "           JECN_USER                  JU,"
                        + "           JECN_USER_POSITION_RELATED U_POS," + "           JECN_FLOW_ORG_IMAGE        POS,"
                        + "           JECN_FLOW_ORG              ORG" + "     WHERE ORG.ORG_ID = POS.ORG_ID"
                        + "       AND POS.FIGURE_ID = U_POS.FIGURE_ID" + "       AND U_POS.PEOPLE_ID = JU.PEOPLE_ID"
                        + "       AND JU.ISLOCK = 0" + "       AND JU.PEOPLE_ID = JPR.CREATE_PERSON"
                        + "       AND POS.FIGURE_ID in " + SqlCommon.getIds(orgIds) + "       AND "
                        + SqlCommon.timeBetweenTheTwoSql(proposeTotalSearchBean.getStartTime(),
                        proposeTotalSearchBean.getEndTime(), "JPR.CREATE_TIME")
                        + "       AND JPR.RELATION_TYPE=" + proposeType + " AND JPR.RELATION_ID in ("
                        + getFileIdByFileType(fileId, projectId, logType) + ")" + "       ) TOTALS"
                        + "   GROUP BY TOTALS.ID,TOTALS.NAME,TOTALS.DATE_STR" + "   ORDER BY TOTALS.ID,TOTALS.DATE_STR";
            case 2:
                return "SELECT TOTALS.ID,TOTALS.NAME,TOTALS.DATE_STR,COUNT(TOTALS.PROPOSE_ID) AS TOTAL_COUNT  FROM ("
                        + "    SELECT JU.PEOPLE_ID AS ID," + "           JU.TRUE_NAME AS NAME,"
                        + "           TO_CHAR(JPR.CREATE_TIME, 'YYYYMM') AS DATE_STR," + "           JPR.ID AS PROPOSE_ID"
                        + "      FROM JECN_RTNL_PROPOSE          JPR," + "           JECN_USER                  JU"
                        + "     WHERE JU.PEOPLE_ID = JPR.CREATE_PERSON" + "       AND JU.ISLOCK = 0"
                        + "       AND JU.PEOPLE_ID in " + SqlCommon.getIds(orgIds) + "       AND "
                        + SqlCommon.timeBetweenTheTwoSql(proposeTotalSearchBean.getStartTime(),
                        proposeTotalSearchBean.getEndTime(), "JPR.CREATE_TIME")
                        + "       AND JPR.RELATION_TYPE=" + proposeType + " AND JPR.RELATION_ID in ("
                        + getFileIdByFileType(fileId, projectId, logType) + ")" + "       ) TOTALS"
                        + "   GROUP BY TOTALS.ID,TOTALS.NAME,TOTALS.DATE_STR" + "   ORDER BY TOTALS.ID,TOTALS.DATE_STR";
            default:
                return "";

        }

    }

    private String getFileIdByFileType(Long fileId, Long projectId, int fileType) {
        return " select file_id from (" + SqlCommon.getFileIdByFileType(fileId, fileType, projectId) + ")";
    }

    /***
     * 合理化建议
     *
     * @return
     */
    private String getProposeTotalCommon(ProposeTotalSearchBean proposeTotalSearchBean, Long projectId) {

        String sql = " from jecn_user ju ,JECN_RTNL_PROPOSE JPR";
        // 没有选择关联文件类型
        sql += " inner join ("
                + SqlCommon.getFileIdByFileType(proposeTotalSearchBean.getFileId(),
                proposeTotalSearchBean.convertToLogType(), projectId)
                + ") file_r ON file_r.file_id = jpr.relation_id";
        sql += " where ju.people_id = JPR.CREATE_PERSON";
        sql += " and JPR.RELATION_TYPE=" + proposeTotalSearchBean.convertToProposeType();
        // 用户类型 0是人员，1是岗位，2是部门
        if (proposeTotalSearchBean.getOrgSetIds().size() > 0) {
            switch (proposeTotalSearchBean.getOrgType()) {
                case 2:
                    sql += " and ju.people_id in " + SqlCommon.getIds(proposeTotalSearchBean.getOrgSetIds());
                    break;
                case 1:
                    sql += " and ju.people_id in (" + SqlCommon.getUserByPosId(proposeTotalSearchBean.getOrgSetIds()) + ")";
                    break;
                case 0:
                    sql += " and ju.people_id in ("
                            + SqlCommon.getUserByOrgId(proposeTotalSearchBean.getOrgSetIds(), projectId) + ")";
            }
        }
        // 开始时间 结束时间
        if (proposeTotalSearchBean.getStartTime() != null && proposeTotalSearchBean.getEndTime() != null) {
            sql += " and " + SqlCommon.timeBetweenTheTwoSql(proposeTotalSearchBean.getStartTime(),
                    proposeTotalSearchBean.getEndTime(), "JPR.CREATE_TIME");
        }
        return sql;
    }

    /**
     * 合理化建议统计 内容
     *
     * @param map
     * @return
     */
    public String findProposeTotalData(Map<String, Object> map) {
        ProposeTotalSearchBean proposeTotalSearchBean = (ProposeTotalSearchBean) map.get("proposeTotalSearchBean");
        Long projectId = (Long) map.get("projectId");
        String sql = "select DISTINCT JPR.ID,JPR.PROPOSE_CONTENT,JU.PEOPLE_ID,JU.TRUE_NAME,JPR.CREATE_TIME,JPR.RELATION_ID,file_r.FILE_NAME as RELATED_NAME,file_r.FILE_TYPE AS RELATED_TYPE";
        return sql + getProposeTotalCommon(proposeTotalSearchBean, projectId);
    }

    /**
     * 合理化建议统计 总数
     *
     * @param map
     * @return
     */
    public String findProposeTotal(Map<String, Object> map) {
        ProposeTotalSearchBean proposeTotalSearchBean = (ProposeTotalSearchBean) map.get("proposeTotalSearchBean");
        Long projectId = (Long) map.get("projectId");
        String sql = "select count(DISTINCT JPR.ID)";
        return sql + getProposeTotalCommon(proposeTotalSearchBean, projectId);
    }

    private String getMaxVersionPropose(Map<String, Object> paramMap) {
        String sql = "		SELECT" +
                "			jrp.*" +
                "		FROM" +
                "			JECN_RTNL_PROPOSE jrp" +
                "		WHERE" +
                "		 jrp.RELATION_ID=#{proposeParam.relatedId}" +
                "		 and " +
                "		 jrp.RELATION_TYPE=#{proposeParam.type}";
        if (paramMap.get("historyId") != null) {
            // sql += " and jrp.history_id=#{historyId}";
        }
        if (paramMap.get("proposeState") != null && Integer.valueOf(paramMap.get("proposeState").toString()) != -1) {// 非全部
            sql += " and jrp.state=" + Integer.valueOf(paramMap.get("proposeState").toString());
        }
        ProposeSearch search = (ProposeSearch) paramMap.get("proposeParam");
        if (search.isMyPropose()) {
            sql += " and jrp.create_person=#{loginPeopleId}";
        }
        return sql;
    }

    public String getMaxHistoryId(Map<String, Object> paramMap) {
        ProposeSearch proposeParam = (ProposeSearch) paramMap.get("proposeParam");
        // 0流程，1制度，2流程架构，3文件，4风险，5标准
        Integer type = proposeParam.getType();
        String sql = "";
        switch (type) {
            case 0:
            case 2:
                sql =
                        "			SELECT" +
                                "				f.history_id" +
                                "			FROM" +
                                "				JECN_FLOW_STRUCTURE f" +
                                "			WHERE" +
                                "			 f.flow_id =#{proposeParam.relatedId}";
                break;
            case 1:
                sql =
                        "			SELECT" +
                                "				f.history_id" +
                                "			FROM" +
                                "				jecn_rule f" +
                                "			WHERE" +
                                "			 f.id =#{proposeParam.relatedId}";
            case 3:
                sql = "			SELECT" +
                        "				f.history_id" +
                        "			FROM" +
                        "				jecn_file f" +
                        "			WHERE" +
                        "			 f.file_id =#{proposeParam.relatedId}";
                break;
            default:
                throw new IllegalArgumentException("调用getMaxHistoryId接口异常，没有" + type + "类型的文控信息");
        }
        return sql;
    }

    public String getTopicHandler(Map<String, Object> params) {
        String sql = "	SELECT" +
                "		ju.PEOPLE_ID," +
                "		ju.TRUE_NAME," +
                "		ju.LOGIN_NAME" +
                "	FROM" +
                "		PROPOSE_TOPIC pt" +
                "	INNER JOIN JECN_USER ju ON pt.HANDLE_PEOPLE = ju.PEOPLE_ID" +
                "	where pt.RELATED_ID=#{relatedId} and pt.TYPE=#{relatedType}";
        return sql;
    }

    /**
     * 查询专员
     *
     * @param propose
     * @return
     */
    public String getHandlePeopleSql(Propose propose) {
        String sql = "";
        /** 0流程，1制度，2流程架构，3文件，4风险，5标准 **/
        String tableName = "";
        String name = "";
        String id = "";
        int type = propose.getType();
        int state = propose.getQueryProcessingState();
        if (type == 0) {
            sql = findFowCommissionerOrRespeople(state) + propose.getRelationId();
        } else if (type == 2) {
            sql = findFowMapCommissionerOrRespeople(state) + propose.getRelationId();
        } else if (type == 1) {
            sql = findRuleCommissionerOrRespeople(state) + propose.getRelationId();
        } else if (type == 3) {
            sql = findFileCommissionerOrRespeople(state) + propose.getRelationId();
        }
        return sql;
    }

    /**
     * 获取 流程 责任人或 专员
     *
     * @param state 0专员  1 责任人
     * @return
     */
    private String findFowCommissionerOrRespeople(int state) {
        String sql = "";
        switch (state) {
            case 0:
                sql = "select COMMISSIONER_ID from JECN_FLOW_STRUCTURE where FLOW_ID =";
                break;
            case 1:
                sql = "select " +
                        "    case when  inf.type_res_people = 0 then" +
                        "     inf.RES_PEOPLE_ID" +
                        "     else" +
                        "     ju.people_id" +
                        "      end  RES_PEOPLE_ID" +
                        "  from JECN_FLOW_BASIC_INFO inf" +
                        "  left join jecn_flow_org_image im" +
                        "    on im.figure_id = inf.RES_PEOPLE_ID" +
                        "   left join jecn_user_position_related pos" +
                        "    on im.figure_id = pos.figure_id" +
                        "  left join jecn_user ju" +
                        "    on ju.people_id = pos.people_id" +
                        " where inf.FLOW_ID =";
                break;
            default:
                break;
        }
        return sql;
    }

    /**
     * 获取 流程架构 责任人或 专员
     *
     * @param state 0专员  1 责任人
     * @return
     */
    private String findFowMapCommissionerOrRespeople(int state) {
        String sql = "";
        switch (state) {
            case 0:
                sql = "select COMMISSIONER_ID from JECN_FLOW_STRUCTURE where FLOW_ID =";
                break;
            case 1:
                sql = "select " +
                        "    case when  inf.type_res_people = 0 then" +
                        "     inf.RES_PEOPLE_ID" +
                        "     else" +
                        "     ju.people_id" +
                        "      end  RES_PEOPLE_ID" +
                        "  from JECN_MAIN_FLOW inf" +
                        "  left join jecn_flow_org_image im" +
                        "    on im.figure_id = inf.RES_PEOPLE_ID" +
                        "   left join jecn_user_position_related pos" +
                        "    on im.figure_id = pos.figure_id" +
                        "  left join jecn_user ju" +
                        "    on ju.people_id = pos.people_id" +
                        " where inf.FLOW_ID =";
                break;
            default:
                break;
        }
        return sql;
    }

    /**
     * 获取 制度 责任人或 专员
     *
     * @param state 0专员  1 责任人
     * @return
     */
    private String findRuleCommissionerOrRespeople(int state) {
        String sql = "";
        switch (state) {
            case 0:
                sql = "  select COMMISSIONER_ID from JECN_RULE where ID =";
                break;
            case 1:
                sql = "  select case" +
                        "         when inf.type_res_people = 0 then" +
                        "          inf.RES_PEOPLE_ID" +
                        "         else" +
                        "          ju.people_id" +
                        "       end RES_PEOPLE_ID" +
                        "  from JECN_RULE inf" +
                        "  left join jecn_flow_org_image im" +
                        "    on im.figure_id = inf.RES_PEOPLE_ID" +
                        "  left join jecn_user_position_related pos" +
                        "    on im.figure_id = pos.figure_id" +
                        "  left join jecn_user ju" +
                        "    on ju.people_id = pos.people_id" +
                        " where inf.Id = ";
                break;
            default:
                break;
        }
        return sql;
    }

    /**
     * 获取 文件 责任人或 专员
     *
     * @param state 0专员  1 责任人
     * @return
     */
    private String findFileCommissionerOrRespeople(int state) {
        String sql = "";
        switch (state) {
            case 0:
                sql = "SELECT COMMISSIONER_ID FROM JECN_FILE WHERE FILE_ID =";
                break;
            case 1:
                sql = "SELECT CASE" +
                        "         WHEN INF.TYPE_RES_PEOPLE = 0 THEN" +
                        "          INF.RES_PEOPLE_ID" +
                        "         ELSE" +
                        "          JU.PEOPLE_ID" +
                        "       END RES_PEOPLE_ID" +
                        "  FROM JECN_FILE INF" +
                        "  LEFT JOIN JECN_FLOW_ORG_IMAGE IM" +
                        "    ON IM.FIGURE_ID = INF.RES_PEOPLE_ID" +
                        "  LEFT JOIN JECN_USER_POSITION_RELATED POS" +
                        "    ON IM.FIGURE_ID = POS.FIGURE_ID" +
                        "  LEFT JOIN JECN_USER JU" +
                        "    ON JU.PEOPLE_ID = POS.PEOPLE_ID" +
                        " WHERE INF.FILE_ID = ";
                break;
            default:
                break;
        }
        return sql;
    }


}
