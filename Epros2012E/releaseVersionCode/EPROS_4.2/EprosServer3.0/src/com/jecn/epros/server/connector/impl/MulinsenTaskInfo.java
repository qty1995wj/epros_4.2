package com.jecn.epros.server.connector.impl;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.action.web.login.ad.mulinsen.MulinsenOAParamInfo;
import com.jecn.epros.server.action.web.login.ad.mulinsen.MunlinsenOASendEnum;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;

/**
 * 木林森-OA待办消息
 * 
 * @author ZXH
 * @date 2018-1-29
 */
public class MulinsenTaskInfo extends JecnBaseTaskSend {

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		if (handlerPeopleIds == null) {
			return;
		}
		MulinsenOAParamInfo paramInfo = new MulinsenOAParamInfo();
		paramInfo.setAction("send_msg");
		paramInfo.setContent(getTaskName(beanNew.getTaskName()) + "审批");

		for (Long peopleId : handlerPeopleIds) {
			JecnUser jecnUser = personDao.get(peopleId);
			log.info(" methord sendTaskInfoToMainSystem param eleId ： " + jecnUser.getDomainName());
			paramInfo.setRedirect_url(getMailUrl(beanNew.getId(), peopleId, false, false));
			paramInfo.setHr_userid(jecnUser.getDomainName());

			// 发送待办消息请求
			String result = MunlinsenOASendEnum.INSTANCE.sendMessage(paramInfo);
			log.info("sendMes ： " + new String(result.getBytes("UTF-8")));
		}
	}

	private String getTaskName(String name) {
		return name.indexOf(".") != -1 ? name.substring(0, name.lastIndexOf(".")) : name;
	}

	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew, JecnUser jecnUser, JecnUser createUser) {

	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		// TODO Auto-generated method stub

	}
}
