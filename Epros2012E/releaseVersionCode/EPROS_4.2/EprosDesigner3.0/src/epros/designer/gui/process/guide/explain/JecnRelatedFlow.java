package epros.designer.gui.process.guide.explain;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;

import org.apache.log4j.Logger;


import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;

/**
 * 相关流程
 * 
 * @author user
 * 
 */
public class JecnRelatedFlow extends JecnFileDescriptionTable {

	private Logger log = Logger.getLogger(JecnRelatedFlow.class);
	protected Long flowId = null;

	public JecnRelatedFlow(int index, String name, boolean isRequest, JecnPanel contentPanel, Long flowId, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowId = flowId;
		this.initTable();
		setColumnToFixedCenter(1);
		setColumnToFixedCenter(2);
	}

	@Override
	protected void dbClickMethod() {
		// TODO Auto-generated method stub

	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("type"));
		title.add(JecnProperties.getValue("actBaseNum"));
		title.add(JecnProperties.getValue("processName"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {

		List<Object[]> relatedFlowList = new ArrayList<Object[]>();
		try {
			relatedFlowList = ConnectionPool.getProcessAction().getFlowRelateByFlowIdDesign(flowId);
		} catch (Exception e) {
			log.error("JecnRelatedFlow getContent is error", e);
		}

		Vector<Vector<String>> relatedFlowContent = new Vector<Vector<String>>();
		for (Object[] object : relatedFlowList) {
			Vector<String> relatedFlowContentStr = new Vector<String>();
			// ID
			relatedFlowContentStr.add(String.valueOf(object[0]));
			// 类型
			String typeName = null;
			if ("1".equals(object[2].toString())) {
				typeName = JecnProperties.getValue("upstreamProcess");
			} else if ("2".equals(object[2].toString())) {
				typeName = JecnProperties.getValue("downstreamProcess");
			} else if ("3".equals(object[2].toString())) {
				typeName = JecnProperties.getValue("interfaceFlow");
			} else if ("4".equals(object[2].toString())) {
				typeName = JecnProperties.getValue("childProcess");
			}
			relatedFlowContentStr.add(typeName);
			relatedFlowContentStr.add(JecnUtil.nullToEmpty(object[3]));
			// 名称
			relatedFlowContentStr.add(String.valueOf(object[1]));

			relatedFlowContent.add(relatedFlowContentStr);
		}
		return relatedFlowContent;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
