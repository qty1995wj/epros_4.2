package com.jecn.epros.server.download.wordxml.hengshen;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.Valign;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;

/**
 * 恒神操作模版下载
 * 
 * @author admin
 * 
 */
public class HSProcessModel extends ProcessFileModel {
	/*** 段落行距单倍倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);

	/*** 段落行距固定值16磅值 *****/
	private ParagraphLineRule vLine2 = new ParagraphLineRule(16F, LineRuleType.EXACT);

	public HSProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		getDocProperty().setFlowChartScaleValue(5.5F);
		setDocStyle("、", textTitleStyle());
		getDocProperty().setNewTblWidth(17.49F);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		// sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent() {
		Table tab = null;
		/***** 默认表格样式 **********/
		TableBean tabBean = new TableBean();
		tabBean.setBorder(1.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		/**** 表格默认段落样式 宋体小四 居左 ************/
		ParagraphBean pBean = new ParagraphBean("宋体", Constants.xiaosi, false);
		pBean.setSpace(0f, 0f, vLine1);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "" });
		rowData.add(new String[] { "一级流程", "", "二级流程", "" });
		rowData.add(new String[] { "三级流程", "", "流程Owner", "" });
		rowData.add(new String[] { "文件编号", "", "版本号", "" });
		rowData.add(new String[] { "制订日期", "", "生效日期", "" });
		rowData.add(new String[] { "审批", "评审", "审核", "制订" });
		rowData.add(new String[] { "", "", "", "" });
		rowData.add(new String[] { "文件修订记录" });

		getDocProperty().getTitleSect().createParagraph("");
		// 17.5
		tab = JecnWordUtil.createTab(getDocProperty().getTitleSect(), tabBean, new float[] { 4.375F, 4.375F, 4.375F,
				4.375F }, rowData, pBean);
		/** 设置跨列 ****/
		tab.getRow(0).getCell(0).setColspan(4);
		tab.getRow(7).getCell(0).setColspan(4);
		/***** 设置行高 *****/
		for (int i = 0; i < tab.getRowCount(); i++) {
			if (i == 0) {
				tab.getRow(i).setHeight(13.9F);
			} else if (i == 6) {
				tab.getRow(i).setHeight(1.37F);
			} else {
				tab.getRow(i).setHeight(0.7F);
			}
		}
		// 宋体 一号 加粗 居中 单倍行距
		ParagraphBean componyPBean = new ParagraphBean(Align.center, "宋体", Constants.yihao, true);
		componyPBean.setSpace(0f, 0f, vLine1);
		TableCell cell = tab.getRow(0).getCell(0);
		for (int i = 0; i < 8; i++) {
			cell.createParagraph("");
		}
		cell.createParagraph(processDownloadBean.getCompanyName(), componyPBean);
		for (int i = 0; i < 6; i++) {
			cell.createParagraph("");
		}
		// 宋体小二 加粗 居中 单倍行距
		ParagraphBean flowNamePBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoer, true);
		flowNamePBean.setSpace(0f, 0f, vLine1);
		cell.createParagraph(processDownloadBean.getFlowName(), flowNamePBean);

		// 第二个单元格
		rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "序号", "版本变更", "修订页次", "所修订内容的摘要", "修订日期", "修订者" });
		for (int i = 0; i < 5; i++) {
			rowData.add(new String[] { "", "", "", "", "", "" });
		}

		tab = JecnWordUtil.createTab(getDocProperty().getTitleSect(), tabBean, new float[] { 2.5F, 2.5F, 2.5F, 5F,
				2.5F, 2.5F }, rowData, pBean);

		/***** 设置行高 *****/
		for (int i = 0; i < tab.getRowCount(); i++) {
			tab.getRow(i).setHeight(0.7F);
		}
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);

	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		String flowName = processDownloadBean.getFlowName();
		String flowIsPublic = getPubOrSec();
		// 文件编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		Table table = null;
		// Arial 小五号 居中
		ParagraphBean pBean = new ParagraphBean("宋体", Constants.xiaosi, true);
		pBean.setAlign(Align.center);
		Header hdr = sect.createHeader(type);
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(1.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "", flowName, flowIsPublic });
		rowData.add(new String[] { "", "", flowInputNum });
		table = JecnWordUtil.createTab(hdr, tabBean, new float[] { 5.72F, 7F, 4.78F }, rowData, pBean);
		table.getRow(0).setHeight(0.78F);
		table.getRow(1).setHeight(0.78F);
		table.setRowStyle(0, Valign.center);
		table.setRowStyle(1, Valign.center);
		table.getRow(0).getCell(0).setRowspan(2);
		table.getRow(0).getCell(1).setRowspan(2);
		// 公司 logo 图标
		Image logo = new Image(getDocProperty().getLogoImage());
		logo.setSize(6.06F, 1.59F);
		Paragraph p = table.getRow(0).getCell(0).getParagraph(0);
		p.appendGraphRun(logo);
		hdr.createParagraph("", new ParagraphBean(PStyle.AF1));
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// Arial 小五号 居中
		ParagraphBean pBean = new ParagraphBean("Arial", Constants.xiaowu, false);
		pBean.setAlign(Align.center);
		Footer ftr = sect.createFooter(type);
		Paragraph p = ftr.createParagraph("", pBean);
		p.appendCurPageRun();
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(1.2F, 1.76F, 1.2F, 1.76F, 0.5F, 0.5F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		// 添加封皮节点数据
		addTitleSectContent();
		// 首页显示装订线
		titleSect.createHeader(HeaderOrFooterType.first).createParagraph("", new ParagraphBean(PStyle.AF1));
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.center, "宋体", Constants.xiaosi, false);
		tblContentStyle.setSpace(0f, 0f, vLine2);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0, 0, 0, 0);
		// 表格左缩进0.01厘米
		tblStyle.setTableLeftInd(0.01F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.xiaosi, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		/*** 初始化标题内容段落属性 ****/
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, "宋体", Constants.xiaosi, false);
		textContentStyle.setSpace(0f, 0f, vLine1);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, "宋体", Constants.shisanhao, true);
		textTitleStyle.setSpace(0F, 0F, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
