package epros.draw.data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.TypeInfo;
import org.w3c.dom.UserDataHandler;

/**
 * 
 * Element的实现类 作用为包装Element实现类，实现去除特殊字符的功能
 * 
 */
public class JecnElement implements Node, Element {

	private Element element;

	public JecnElement(Element element) {
		this.element = element;
	}

	public Element getElement() {
		return element;
	}

	public void setElement(Element element) {
		this.element = element;
	}

	/**
	 * 去除ASCII中的控制字符9 10 13 除外
	 * 
	 * @param str
	 *            需要处理的字符串
	 * @return
	 */
	private String handleSpecialChar(String str) {

		// 如果传过来的字符串为空或者为空格直接返回
		if (str == null || str.trim().equals("")) {

			return str;

		}

		StringBuffer buf = new StringBuffer();
		char[] charArray = str.toCharArray();
		for (char c : charArray) {

			int i = (int) c;
			buf.append("&" + i + ";");
		}
		String rep = buf.toString();

		buf = new StringBuffer();
		// 将特殊字符去除
		String reg = "&0;|&1;|&2;|&3;|&4;|&5;|&6;|&7;|&8;|&14;|&15;|&16;|&17;|&18;|&19;|&20;|&21;|&22;|&23;|&24;|&25;|&26;|&27;|&28;|&29;|&30;|&31;";
		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(rep);
		// 发现特殊字符替换为空字符
		if (m.find()) {
			rep = m.replaceAll("");
		}

		// 将数字转换为字符
		reg = "&(\\d+);";
		p = Pattern.compile(reg);
		Matcher mm = p.matcher(rep);

		while (mm.find()) {
			buf.append((char) Integer.valueOf(mm.group(1)).intValue());
		}

		return buf.toString();

	}

	@Override
	public void setAttribute(String name, String value) {
		element.setAttribute(name, handleSpecialChar(value));
	}

	@Override
	public String getAttribute(String name) {
		return element.getAttribute(handleSpecialChar(name));
	}

	@Override
	public String getAttributeNS(String namespaceURI, String localName)
			throws DOMException {
		return element.getAttributeNS(namespaceURI, localName);
	}

	@Override
	public Attr getAttributeNode(String name) {
		return element.getAttributeNode(name);
	}

	@Override
	public Attr getAttributeNodeNS(String namespaceURI, String localName)
			throws DOMException {
		return element.getAttributeNodeNS(namespaceURI, localName);
	}

	@Override
	public NodeList getElementsByTagName(String name) {
		// TODO Auto-generated method stub
		return element.getElementsByTagName(name);
	}

	@Override
	public NodeList getElementsByTagNameNS(String namespaceURI, String localName)
			throws DOMException {
		// TODO Auto-generated method stub
		return element.getElementsByTagNameNS(namespaceURI, localName);
	}

	@Override
	public TypeInfo getSchemaTypeInfo() {
		// TODO Auto-generated method stub
		return element.getSchemaTypeInfo();
	}

	@Override
	public String getTagName() {
		// TODO Auto-generated method stub
		return element.getTagName();
	}

	@Override
	public boolean hasAttribute(String name) {
		// TODO Auto-generated method stub
		return element.hasAttribute(name);
	}

	@Override
	public boolean hasAttributeNS(String namespaceURI, String localName)
			throws DOMException {
		// TODO Auto-generated method stub
		return element.hasAttributeNS(namespaceURI, localName);
	}

	@Override
	public void removeAttribute(String name) throws DOMException {
		// TODO Auto-generated method stub
		element.removeAttribute(name);
	}

	@Override
	public void removeAttributeNS(String namespaceURI, String localName)
			throws DOMException {
		// TODO Auto-generated method stub
		element.removeAttributeNS(namespaceURI, localName);
	}

	@Override
	public Attr removeAttributeNode(Attr oldAttr) throws DOMException {
		// TODO Auto-generated method stub
		return element.removeAttributeNode(oldAttr);
	}

	@Override
	public void setAttributeNS(String namespaceURI, String qualifiedName,
			String value) throws DOMException {
		// TODO Auto-generated method stub
		element.setAttributeNS(namespaceURI, qualifiedName, value);
	}

	@Override
	public Attr setAttributeNode(Attr newAttr) throws DOMException {
		// TODO Auto-generated method stub
		return element.setAttributeNode(newAttr);
	}

	@Override
	public Attr setAttributeNodeNS(Attr newAttr) throws DOMException {
		// TODO Auto-generated method stub
		return element.setAttributeNodeNS(newAttr);
	}

	@Override
	public void setIdAttribute(String name, boolean isId) throws DOMException {
		// TODO Auto-generated method stub
		element.setIdAttribute(name, isId);
	}

	@Override
	public void setIdAttributeNS(String namespaceURI, String localName,
			boolean isId) throws DOMException {
		// TODO Auto-generated method stub
		element.setIdAttributeNS(namespaceURI, localName, isId);

	}

	@Override
	public void setIdAttributeNode(Attr idAttr, boolean isId)
			throws DOMException {
		// TODO Auto-generated method stub
		element.setIdAttributeNode(idAttr, isId);
	}

	@Override
	public Node appendChild(Node newChild) throws DOMException {
		if (newChild instanceof JecnElement) {
			JecnElement el = (JecnElement) newChild;
			return element.appendChild(el.element);
		}
		return element.appendChild(newChild);
	}

	@Override
	public Node cloneNode(boolean deep) {
		// TODO Auto-generated method stub
		return element.cloneNode(deep);
	}

	@Override
	public short compareDocumentPosition(Node other) throws DOMException {
		// TODO Auto-generated method stub
		return element.compareDocumentPosition(other);
	}

	@Override
	public NamedNodeMap getAttributes() {
		// TODO Auto-generated method stub
		return element.getAttributes();
	}

	@Override
	public String getBaseURI() {
		// TODO Auto-generated method stub
		return element.getBaseURI();
	}

	@Override
	public NodeList getChildNodes() {
		// TODO Auto-generated method stub
		return element.getChildNodes();
	}

	@Override
	public Object getFeature(String feature, String version) {
		// TODO Auto-generated method stub
		return element.getFeature(feature, version);
	}

	@Override
	public Node getFirstChild() {
		// TODO Auto-generated method stub
		return element.getFirstChild();
	}

	@Override
	public Node getLastChild() {
		// TODO Auto-generated method stub
		return element.getLastChild();
	}

	@Override
	public String getLocalName() {
		// TODO Auto-generated method stub
		return element.getLocalName();
	}

	@Override
	public String getNamespaceURI() {
		// TODO Auto-generated method stub
		return element.getNamespaceURI();
	}

	@Override
	public Node getNextSibling() {
		// TODO Auto-generated method stub
		return element.getNextSibling();
	}

	@Override
	public String getNodeName() {
		// TODO Auto-generated method stub
		return element.getNodeName();
	}

	@Override
	public short getNodeType() {
		// TODO Auto-generated method stub
		return element.getNodeType();
	}

	@Override
	public String getNodeValue() throws DOMException {
		// TODO Auto-generated method stub
		return element.getNodeValue();
	}

	@Override
	public Document getOwnerDocument() {
		// TODO Auto-generated method stub
		return element.getOwnerDocument();
	}

	@Override
	public Node getParentNode() {
		// TODO Auto-generated method stub
		return element.getParentNode();
	}

	@Override
	public String getPrefix() {
		// TODO Auto-generated method stub
		return element.getPrefix();
	}

	@Override
	public Node getPreviousSibling() {
		// TODO Auto-generated method stub
		return element.getPreviousSibling();
	}

	@Override
	public String getTextContent() throws DOMException {
		// TODO Auto-generated method stub
		return element.getTextContent();
	}

	@Override
	public Object getUserData(String key) {
		// TODO Auto-generated method stub
		return element.getUserData(key);
	}

	@Override
	public boolean hasAttributes() {
		// TODO Auto-generated method stub
		return element.hasAttributes();
	}

	@Override
	public boolean hasChildNodes() {
		// TODO Auto-generated method stub
		return element.hasChildNodes();
	}

	@Override
	public Node insertBefore(Node newChild, Node refChild) throws DOMException {
		// TODO Auto-generated method stub
		return element.insertBefore(newChild, refChild);
	}

	@Override
	public boolean isDefaultNamespace(String namespaceURI) {
		// TODO Auto-generated method stub
		return element.isDefaultNamespace(namespaceURI);
	}

	@Override
	public boolean isEqualNode(Node arg) {
		// TODO Auto-generated method stub
		return element.isEqualNode(arg);
	}

	@Override
	public boolean isSameNode(Node other) {
		// TODO Auto-generated method stub
		return element.isSameNode(other);
	}

	@Override
	public boolean isSupported(String feature, String version) {
		// TODO Auto-generated method stub
		return element.isSupported(feature, version);
	}

	@Override
	public String lookupNamespaceURI(String prefix) {
		// TODO Auto-generated method stub
		return element.lookupNamespaceURI(prefix);
	}

	@Override
	public String lookupPrefix(String namespaceURI) {
		// TODO Auto-generated method stub
		return element.lookupPrefix(namespaceURI);
	}

	@Override
	public void normalize() {
		// TODO Auto-generated method stub
		element.normalize();
	}

	@Override
	public Node removeChild(Node oldChild) throws DOMException {
		// TODO Auto-generated method stub
		return element.removeChild(oldChild);
	}

	@Override
	public Node replaceChild(Node newChild, Node oldChild) throws DOMException {
		// TODO Auto-generated method stub
		return element.replaceChild(newChild, oldChild);
	}

	@Override
	public void setNodeValue(String nodeValue) throws DOMException {
		// TODO Auto-generated method stub
		element.setNodeValue(nodeValue);
	}

	@Override
	public void setPrefix(String prefix) throws DOMException {
		// TODO Auto-generated method stub
		element.setPrefix(prefix);
	}

	@Override
	public void setTextContent(String textContent) throws DOMException {
		// TODO Auto-generated method stub
		element.setTextContent(textContent);
	}

	@Override
	public Object setUserData(String key, Object data, UserDataHandler handler) {
		// TODO Auto-generated method stub
		return element.setUserData(key, data, handler);
	}

}
