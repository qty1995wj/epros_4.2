package com.jecn.epros.server.webBean.dataImport.match;

/*******************************************************************************
 * 实际岗位表(与流程匹配的岗位临时表)
 * 
 * @author Administrator
 * 
 */
public class ActualPosBean {
	/** 主键ID */
	private Long id;

	/** 基准岗位编码 */
	private String basePosNum;

	/** 基准岗位名称 */
	private String basePosName;

	/** 实际部门编码 */
	private String deptNum;

	/** 实际岗位编码 */
	private String actPosNum;

	/** 实际岗位名称 */
	private String actPosName;

	/** 未匹配岗位类型 1:HR实际岗位；5：流程岗位；6基准岗位*/
	private int radioType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBasePosNum() {
		return basePosNum;
	}

	public void setBasePosNum(String basePosNum) {
		this.basePosNum = basePosNum;
	}

	public String getBasePosName() {
		return basePosName;
	}

	public void setBasePosName(String basePosName) {
		this.basePosName = basePosName;
	}

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public String getActPosNum() {
		return actPosNum;
	}

	public void setActPosNum(String actPosNum) {
		this.actPosNum = actPosNum;
	}

	public String getActPosName() {
		return actPosName;
	}

	public void setActPosName(String actPosName) {
		this.actPosName = actPosName;
	}

	public int getRadioType() {
		return radioType;
	}

	public void setRadioType(int radioType) {
		this.radioType = radioType;
	}

}
