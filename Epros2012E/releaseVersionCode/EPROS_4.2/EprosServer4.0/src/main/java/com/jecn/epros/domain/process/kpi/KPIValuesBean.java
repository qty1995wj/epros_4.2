package com.jecn.epros.domain.process.kpi;

/**
 * KPI值
 *
 * @author ZXH
 */
public class KPIValuesBean {
    private Long kpiId;

    /**
     * KPI横向单位
     */
    private String horValue;
    /**
     * KPI值
     */
    private String kpiValue;

    public String getHorValue() {
        return horValue;
    }

    public void setHorValue(String horValue) {
        this.horValue = horValue;
    }

    public String getKpiValue() {
        return kpiValue;
    }

    public void setKpiValue(String kpiValue) {
        this.kpiValue = kpiValue;
    }

    public Long getKpiId() {
        return kpiId;
    }

    public void setKpiId(Long kpiId) {
        this.kpiId = kpiId;
    }
}
