package epros.designer.gui.rule;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.rule.AddRuleFileData;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.system.AccessAuthorityCommon;
import epros.designer.gui.system.CategoryCommon;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.gui.system.PersonliableSelectCommon;
import epros.designer.gui.system.ResDepartSelectCommon;
import epros.designer.gui.system.field.JecnTipTextField;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 上传制度文件
 * 
 * @author Administrator
 * 
 *         修改人：chehuanbo 时间：2014-10-22 描述：添加制度责任人，支持选择岗位或者人员
 * @since V3.06
 */
public class UploadRuleFileDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(UploadRuleFileDialog.class);
	/** 主面板 */
	protected JecnPanel mainPanel = new JecnPanel();
	// ==========================================end C
	/** 文件面板 */
	protected JecnPanel filePanel = new JecnPanel(getWidthMax() - 10, 160);

	protected JecnPanel proPanel = new JecnPanel(getWidthMax() - 10, 400);

	/** 滚动面板，加入fileTable */
	protected JScrollPane fileTableScrollPanel = new JScrollPane();
	protected JTable fileTable = new UploadStandardFileTable();
	protected JecnPanel fileButtonPanel = new JecnPanel(getWidthMax() - 20, 30);
	/** 确定取消 */
	protected JecnPanel butPanel = new JecnPanel(getWidthMax() - 10, 60);

	/** 选择按钮 */
	protected JButton uploadBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 打开按钮 */
	protected JButton openBut = new JButton(JecnProperties.getValue("openBtn"));

	/** 删除 */
	protected JButton deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));

	// 确定
	protected JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	// 取消
	protected JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	// 验证提示
	protected JLabel fileVerfyLab = new JLabel();

	// 验证提示
	protected JLabel verfyLab = new JLabel();

	protected JecnTreeNode pNode = null;
	protected JecnTree jTree = null;

	// 要上传的制度文件
	protected List<RuleT> list = new ArrayList<RuleT>();

	// 要上传的制度文件的文件名称
	protected List<String> names = new ArrayList<String>();

	/** 类别 */
	CategoryCommon categoryCommon = null;
	/** 流程责任人 */
	PersonliableSelectCommon personliableSelect = null;
	/** 流程责任部门 */
	ResDepartSelectCommon resDepartSelectCommon = null;
	/** 查阅权限 */
	AccessAuthorityCommon accessAuthorityCommon = null;

	/** 专员 */
	PeopleSelectCommon commissionerPeopleSelectCommon = null;
	/** 关键字 */
	JecnTipTextField keyWordField = null;
	/** 监护人 */
	PeopleSelectCommon guardianPeopleSelectCommon = null;

	/** 是不是东软 */
	private boolean isDRCompany = JecnConfigTool.isDRYLOperType();

	public UploadRuleFileDialog(JecnTreeNode pNode, JecnTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		initCompotents();
		initLayout();
		buttonActionInit();
	}

	protected void initCompotents() {
		this.setTitle(JecnProperties.getValue("ralationFile"));
		this.setSize(getWidthMax(), 650);
		this.setResizable(false);
		this.setModal(true);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);

		Dimension dimension = new Dimension(getWidthMax() - 20, 100);
		fileTableScrollPanel.setPreferredSize(dimension);
		fileTableScrollPanel.setMinimumSize(dimension);
		fileTableScrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		/** ==============end================================ */
		filePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("addFile")));// 添加文件
		// 验证提示滚动设置tipLabScroPane
		fileVerfyLab.setForeground(Color.red);
		verfyLab.setForeground(Color.red);
	}

	protected void initLayout() {
		mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		Insets insets = new Insets(5, 5, 5, 5);

		int mainRows = 0;
		// 密级，权限，类别
		GridBagConstraints c = new GridBagConstraints(0, mainRows, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(proPanel, c);
		mainRows++;
		proPanel.setLayout(new GridBagLayout());
		int rows = 0;
		rows = initRuleLinkCompotents(rows, proPanel, insets);
		if (JecnConfigTool.isShowRuleType()) {
			// 类别
			categoryCommon = new CategoryCommon(rows, proPanel, insets, null, this,JecnConfigTool.isRequest(ConfigItemPartMapMark.ruleType.toString()),
					getName(ConfigItemPartMapMark.ruleType));
			rows++;
		}
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleResPeople)) {
			// 责任人
			personliableSelect = new PersonliableSelectCommon(rows, proPanel, insets, null, -1, "", this, 1,
					JecnConfigTool.isRequest(ConfigItemPartMapMark.ruleResPeople.toString()),
					getName(ConfigItemPartMapMark.ruleResPeople));
		}
		rows++;
		// 制度监护人
		if (JecnConfigTool.isShowRuleGuardianPeople()) {
			guardianPeopleSelectCommon = new PeopleSelectCommon(rows, proPanel, insets, null, "", this,
					getName(ConfigItemPartMapMark.ruleGuardianPeople), JecnConfigTool.isRequest("ruleGuardianPeople"));
			rows++;
		}
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.ruleResDept)) {
			resDepartSelectCommon = new ResDepartSelectCommon(rows, proPanel, insets, null, "", this, JecnConfigTool
					.isRequest(ConfigItemPartMapMark.ruleResDept.toString()),
					getName(ConfigItemPartMapMark.ruleResDept));
		}
		rows++;
		// 专员
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowRuleCommissioner)) {
			this.commissionerPeopleSelectCommon = new PeopleSelectCommon(rows, proPanel, insets, null, "", this,
					getName(ConfigItemPartMapMark.isShowRuleCommissioner), JecnConfigTool
							.isRequest("isShowRuleCommissioner"));
			rows++;
		}
		// 关键字
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowRuleKeyWord)) {
			this.keyWordField = new JecnTipTextField(rows, proPanel, insets,
					getName(ConfigItemPartMapMark.isShowRuleKeyWord), "",
					getName(ConfigItemPartMapMark.isShowRuleKeyWord), JecnConfigTool.isRequest("isShowRuleKeyWord"));
			rows++;
		}
		// 查阅权限
		accessAuthorityCommon = new AccessAuthorityCommon(rows, proPanel, insets, pNode.getJecnTreeBean().getId(), 3,
				this, true, true, getName(ConfigItemPartMapMark.ruleScurityLevel));
		rows = accessAuthorityCommon.getRows();

		customPanel(mainRows, mainPanel, insets);
		mainRows++;
		// =======================end C
		// 文件表格
		addRuleFileTable(c, insets, mainRows);
		mainRows++;

		// 添加按钮面板
		addButtonPanel();
		c = new GridBagConstraints(0, mainRows, 1, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(butPanel, c);
		mainRows++;
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		butPanel.add(verfyLab);
		butPanel.add(okBut);
		butPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);
	}

	protected void addRuleFileTable(GridBagConstraints c, Insets insets, int rows) {
		c = new GridBagConstraints(0, rows, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(filePanel, c);
		filePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		filePanel.add(fileTableScrollPanel);
		fileTableScrollPanel.setViewportView(fileTable);
		filePanel.add(fileButtonPanel);
	}

	protected int initRuleLinkCompotents(int rows, JecnPanel proPanel2, Insets insets) {
		return rows;
	}

	private String getName(ConfigItemPartMapMark mark) {
		String colon = JecnResourceUtil.getLocale() == Locale.ENGLISH ? ":" : "：";
		return JecnConfigTool.getConfigItemName(mark) + colon;
	}

	protected void addButtonPanel() {
		fileButtonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		fileButtonPanel.add(uploadBut);
		fileButtonPanel.add(openBut);
		fileButtonPanel.add(deleteBut);
		fileButtonPanel.add(fileVerfyLab);
	}

	protected void customPanel(int rows, JecnPanel mainPanel, Insets insets) {
	}

	private void buttonActionInit() {
		uploadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectFile();
			}
		});

		openBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				openFile();
			}
		});

		deleteBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int[] selectRows = fileTable.getSelectedRows();
				if (selectRows.length == 0) {
					fileVerfyLab.setText(JecnProperties.getValue("chooseOneRow"));
					return;
				}
				for (int i = selectRows.length - 1; i >= 0; i--) {
					((DefaultTableModel) fileTable.getModel()).removeRow(selectRows[i]);
					childDeleteData();
				}
				fileVerfyLab.setText("");
			}
		});

		/**
		 * 确定按钮
		 * 
		 * 修改人：chehuanbo 时间：2014-10-21 描述：添加制度责任人，代码修改行：824
		 * 
		 * @since V3.06
		 * */
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addFileSubmit();
			}
		});

		// 取消事件，关闭窗口
		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	protected void childDeleteData() {

	}

	/**
	 * @author hyl 选择要上传的制度文件
	 */
	protected void selectFile() {
		FileChooseDialog fileChooseDialog = new FileChooseDialog(new ArrayList<JecnTreeBean>());
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			// 已经选择过的文件
			Vector<Vector<String>> vs = ((DefaultTableModel) fileTable.getModel()).getDataVector();
			for (JecnTreeBean jecnTreeBean : fileChooseDialog.getListJecnTreeBean()) {
				for (Vector<String> vo : vs) {
					if (jecnTreeBean.getId().toString().equals(vo.get(0))) {
						continue;
					}
				}
				Vector<String> v = new Vector<String>();
				v.add(jecnTreeBean.getId().toString());
				v.add(jecnTreeBean.getName());
				((DefaultTableModel) fileTable.getModel()).addRow(v);
			}
		}
	}

	/**
	 * 打开文件
	 */
	protected void openFile() {
		// 判断是否选中Table中的一行，没选中，提示选中一行
		int[] selectRows = fileTable.getSelectedRows();
		if (selectRows.length == 1) {
			fileVerfyLab.setText("");
			Long id = Long.valueOf((String) fileTable.getValueAt(selectRows[0], 0));
			JecnDesignerCommon.openFile(id);
		} else {
			this.fileVerfyLab.setText(JecnProperties.getValue("chooseOneRow"));
		}

	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				addFileSubmit();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	/**
	 * 是否更新
	 * 
	 * @return
	 */
	private boolean isUpdate() {
		if (categoryCommon != null && categoryCommon.isUpdate()) {
			return true;
		}
		if (personliableSelect != null && personliableSelect.getSingleSelectCommon().isUpdate()) {
			return true;
		}
		if (resDepartSelectCommon != null && resDepartSelectCommon.isUpdate()) {
			return true;
		}
		if (commissionerPeopleSelectCommon != null && this.commissionerPeopleSelectCommon.isUpdate()) {
			return true;
		}
		if (this.keyWordField != null && keyWordField.isUpdate()) {
			return true;
		}
		if (accessAuthorityCommon != null && accessAuthorityCommon.isUpdate()) {
			return true;
		}
		if (((DefaultTableModel) fileTable.getModel()).getDataVector().size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 点击关联文件的确定按钮
	 * 
	 * @author hyl
	 */
	protected void addFileSubmit() {
		/*
		 * // 责任人 if (personliableSelect != null) { if
		 * (!personliableSelect.getSingleSelectCommon
		 * ().validateRequired(verfyLab)) { return; } } // 监护人 if
		 * (guardianPeopleSelectCommon != null) { if
		 * (!guardianPeopleSelectCommon.validateRequired(verfyLab)) { return; }
		 * } // 责任部门 if (resDepartSelectCommon != null) { if
		 * (!resDepartSelectCommon.validateRequired(verfyLab)) { return; } }
		 */
		// 关键字长度验证
		if (this.keyWordField != null && DrawCommon.checkNameMaxLength(this.keyWordField.getResultStr())) {
			verfyLab.setText(JecnProperties.getValue("keyWord") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		fileVerfyLab.setText("");
		// 数据
		if (checkUploadFile()) {
			return;
		}
		list.clear();
		names.clear();
		// 初始化和校验制度文件列表
		if (!initAndCheckRuleList()) {
			return;
		}
		try {
			if (validateNamefullPath(names)) {
				return;
			}
			// 验证文件是否重复
			String[] ns = ConnectionPool.getRuleAction().validateAddNames(names, pNode.getJecnTreeBean().getId());
			if (ns != null && ns.length > 0) {
				StringBuffer tip = new StringBuffer();
				for (String n : ns) {
					if (tip.length() == 0) {
						tip.append(n).append(",");
					} else {
						// tip.append("<br>");
						tip.append(n).append(",");
					}

				}
				String tipText = tip.toString().substring(0, tip.toString().length() - 1) + " "
						+ JecnProperties.getValue("haved");
				verfyLab.setText(tipText);
				// verfyLab.setText("<html>" + tipText + "</html>");
				return;
			}

			// 创建制度
			ConnectionPool.getRuleAction().addRuleFile(getRueFileData());
			// 关联制度文件结束后，重新查询制度文件集合，更新制度树节点
			List<JecnTreeBean> listNode = ConnectionPool.getRuleAction().getChildRules(pNode.getJecnTreeBean().getId(),
					JecnConstants.projectId);
			// 刷新制度树节点
			JecnTreeCommon.refreshNode(pNode, listNode, jTree);
			JecnTreeCommon.autoExpandNode(jTree, pNode);

			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
					.getValue("fileUplodSuccess"));
			this.setVisible(false);
		} catch (Exception e1) {
			log.error("UploadRuleFileDialog addFileSubmit is error", e1);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
		}

	}

	/**
	 * 提示 文件库中是否有重名文件
	 * 
	 * @param names
	 * @return
	 */
	protected boolean validateNamefullPath(List<String> names) {
		try {
			String[] ns = ConnectionPool.getRuleAction()
					.validateNamefullPath(names, pNode.getJecnTreeBean().getId(), 2);
			if (ns != null && ns.length > 0) {
				StringBuffer tip = new StringBuffer();
				for (String n : ns) {
					if (tip.length() == 0) {
						tip.append(n).append(",");
					} else {
						tip.append("\n");
						tip.append(n).append(",");
					}
				}
				String tipText = tip.toString().substring(0, tip.toString().length() - 1) + " \n" + JecnProperties.getValue("itExistsDirectories");
				// 是否删除提示框
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), tipText, null,
						JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					return true;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	protected boolean checkUploadFile() {
		Vector<Vector<String>> data = ((DefaultTableModel) fileTable.getModel()).getDataVector();
		if (data.size() == 0) {
			this.fileVerfyLab.setText(JecnProperties.getValue("selectUploadFile"));
			return true;
		}
		return false;
	}

	protected AddRuleFileData getRueFileData() {
		AddRuleFileData ruleFileData = new AddRuleFileData();
		ruleFileData.setList(list);
		AccessId accId = new AccessId();
		// 部门
		accId.setOrgAccessId(accessAuthorityCommon.getOrgAccessIds());
		accId.setPosAccessId(accessAuthorityCommon.getPosAccessIds());
		accId.setPosGroupAccessId(accessAuthorityCommon.getPosGroupAcessIds());
		ruleFileData.setAccIds(accId);
		ruleFileData.setUpdatePersonId(JecnConstants.getUserId());
		return ruleFileData;
	}

	protected boolean initAndCheckRuleList() {
		DefaultTableModel mode = (DefaultTableModel) fileTable.getModel();
		Vector<Vector<String>> data = mode.getDataVector();
		int i = 0;
		for (Vector<String> v : data) {
			RuleT ruleT = new RuleT();
			ruleT.setPerId(pNode.getJecnTreeBean().getId());
			ruleT.setRuleName(v.get(1));
			ruleT.setIsPublic(accessAuthorityCommon.getPublicStatic() == 0 ? 0L : 1L);
			if (categoryCommon != null) {
				ruleT.setTypeId(this.categoryCommon.getTypeResultId());
			}
			ruleT.setIsDir(2);// (0是目录，1是制度,2是制度文件)
			ruleT.setFileId(Long.valueOf(v.get(0)));
			ruleT.setCreatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
			ruleT.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
			ruleT.setSortId(JecnTreeCommon.getMaxSort(pNode) + i);
			ruleT.setProjectId(JecnConstants.projectId);
			if (resDepartSelectCommon != null) {
				ruleT.setOrgId(resDepartSelectCommon.getIdResult());
			}
			if (personliableSelect != null) {
				// 制度责任人id
				ruleT.setAttchMentId(personliableSelect.getSingleSelectCommon().getIdResult());
				// 制度责任人类型
				ruleT.setTypeResPeople(personliableSelect.getPeopleResType());
			}
			ruleT.setExpiry(Long.valueOf(JecnConfigTool.ruleValidityAllocation().toString()));
			// 专员
			if (commissionerPeopleSelectCommon != null) {
				ruleT.setCommissionerId(commissionerPeopleSelectCommon.getIdResult());
			}
			// 关键字
			if (this.keyWordField != null) {
				ruleT.setKeyword(keyWordField.getResultStr());
			}
			// 监护人
			if (guardianPeopleSelectCommon != null) {
				ruleT.setGuardianId(guardianPeopleSelectCommon.getIdResult());
			}
			// 保密级别
			ruleT.setConfidentialityLevel(accessAuthorityCommon.getConfidentialityLevelStatic());
			ruleT.setIsFileLocal(0);
			list.add(ruleT);
			names.add(ruleT.getRuleName());
			i++;
		}

		return true;
	}

	class UploadStandardFileTable extends JTable {

		public UploadStandardFileTable() {

			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			TableColumn column = columnModel.getColumn(0);
			column.setMinWidth(100);
			column.setMaxWidth(150);
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}
			// this.addKeyListener(new CTableKeyAdapter(this));
			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// // 自定义表头UI
			// this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI(SwingConstants.CENTER));
		}

		public DefaultTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("id"));
			// title.add(JecnProperties.getValue("fileNum"));
			title.add(JecnProperties.getValue("fileName"));
			return new DefaultTableModel(null, title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {
			return new int[] { 0 };
		}

	}

}
