package com.jecn.epros.server.bean.propose;

import java.util.Date;

public class JecnFlowRtnlPropose {
	
	/**合理化建议Bean*/
	private JecnRtnlPropose jecnRtnlPropose;
	/**流程编号*/
	private String flowNum;
	/**流程名称*/
	private String flowName;
	/**责任部门名称*/
	private String orgName;
	/**责任部门ID*/
	private Long orgId;
	/**流程责任人名称*/
	private String resFlowName;
	/**流程责任人ID*/
	private Long resFlowId;
	/**流程责任人类型：0：人员，1：岗位*/
	private int resPeopleType;
	/**合理化建议搜索类型：0：流程；1：制度*/
	private int rtnlType;
	/**制度类型：1：制度模板文件；2：制度文件；0：流程*/
	private int ruleIsDir =0;
	/**制度文件ID*/
	private Long ruleFileId;
	/**提交人ID*/
	private Long submitPeopleId;
	/**登陆人ID*/
	private Long loginUserId;
	/**登陆人是否是管理员*/
	private boolean isAdmin;
	/**我的建议是否被选中*/
	private boolean isSelectedSubmit;
	/**搜索起始时间*/
	private Date dateStartTime = null;
	/**搜索结束时间*/
	private Date dateEndTime = null;
	/**项目ID*/
	private Long projectId;
	
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public Long getSubmitPeopleId() {
		return submitPeopleId;
	}
	public void setSubmitPeopleId(Long submitPeopleId) {
		this.submitPeopleId = submitPeopleId;
	}
	public Long getLoginUserId() {
		return loginUserId;
	}
	public void setLoginUserId(Long loginUserId) {
		this.loginUserId = loginUserId;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public boolean isSelectedSubmit() {
		return isSelectedSubmit;
	}
	public void setSelectedSubmit(boolean isSelectedSubmit) {
		this.isSelectedSubmit = isSelectedSubmit;
	}
	public Date getDateStartTime() {
		return dateStartTime;
	}
	public void setDateStartTime(Date dateStartTime) {
		this.dateStartTime = dateStartTime;
	}
	public Date getDateEndTime() {
		return dateEndTime;
	}
	public void setDateEndTime(Date dateEndTime) {
		this.dateEndTime = dateEndTime;
	}
	
	
	public Long getRuleFileId() {
		return ruleFileId;
	}
	public void setRuleFileId(Long ruleFileId) {
		this.ruleFileId = ruleFileId;
	}
	public int getRuleIsDir() {
		return ruleIsDir;
	}
	public void setRuleIsDir(int ruleIsDir) {
		this.ruleIsDir = ruleIsDir;
	}
	public int getRtnlType() {
		return rtnlType;
	}
	public void setRtnlType(int rtnlType) {
		this.rtnlType = rtnlType;
	}
	public int getResPeopleType() {
		return resPeopleType;
	}
	public void setResPeopleType(int resPeopleType) {
		this.resPeopleType = resPeopleType;
	}
	/**
	 * 导航合理化建议-----显示的所有建议所在的流程图的流程责任人ID是否是当前登录的用户.0:否，1：是流程责任,
	 * 2：当前登录系统管理员配置选中系统管理员,
	 * 3:只是当前登录用户是系统管理员，针对合理化建议只有删除功能
	 * 
	 */
	private int isNowLoginPeople = 0;
	/**导航===界面编辑删除按钮是否提交人(是就显示) 0：否，1：是提交人*/
	private int isSubmitNavigationPeople  = 0;
	
	public int getIsSubmitNavigationPeople() {
		return isSubmitNavigationPeople;
	}
	public void setIsSubmitNavigationPeople(int isSubmitNavigationPeople) {
		this.isSubmitNavigationPeople = isSubmitNavigationPeople;
	}
	public JecnRtnlPropose getJecnRtnlPropose() {
		return jecnRtnlPropose;
	}
	public void setJecnRtnlPropose(JecnRtnlPropose jecnRtnlPropose) {
		this.jecnRtnlPropose = jecnRtnlPropose;
	}
	public String getFlowNum() {
		return flowNum;
	}
	public void setFlowNum(String flowNum) {
		this.flowNum = flowNum;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getResFlowName() {
		return resFlowName;
	}
	public void setResFlowName(String resFlowName) {
		this.resFlowName = resFlowName;
	}
	public Long getResFlowId() {
		return resFlowId;
	}
	public void setResFlowId(Long resFlowId) {
		this.resFlowId = resFlowId;
	}
	public int getIsNowLoginPeople() {
		return isNowLoginPeople;
	}
	public void setIsNowLoginPeople(int isNowLoginPeople) {
		this.isNowLoginPeople = isNowLoginPeople;
	}
	
}
