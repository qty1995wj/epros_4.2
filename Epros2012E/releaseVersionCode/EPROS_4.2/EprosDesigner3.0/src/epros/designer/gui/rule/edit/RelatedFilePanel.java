package epros.designer.gui.rule.edit;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.integration.risk.RiskChooseDialog;
import epros.designer.gui.rule.RuleChooseDialog;
import epros.designer.gui.rule.edit.BaseCommonJTable.RelatedFileTableModel;
import epros.designer.gui.standard.StandardChooseDialog;
import epros.designer.table.JecnTableCellRenderer;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;

/**
 * 相关文件
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-11-20 时间：上午10:18:04
 */
public abstract class RelatedFilePanel extends JecnPanel {
	protected Logger log = Logger.getLogger(RelatedFilePanel.class);
	/** 主面板 */
	protected JecnPanel mainPanel;
	/** table */
	protected RelatedFileTable relatedFileTable;

	/** table 滚动面板 */
	protected JScrollPane scrollPane;

	/** 底部按钮面板 */
	protected JecnPanel buttonPanel;
	/** 关联 */
	protected JButton refButton;
	/** 删除 */
	protected JButton deleteButton;
	/** 初始化生成tree结果集 */
	protected List<JecnTreeBean> treeBeanList = new ArrayList<JecnTreeBean>();

	private RelatedNodeType nodeType;

	private Set<Long> setOldIds = new HashSet<Long>();
	//相关制度
	private Long relatedId = 0L; 


	/**
	 * 流程架构、流程、文件相关标准等
	 * 
	 * @param activeData
	 */
	public RelatedFilePanel(List<JecnTreeBean> treeBeanList, RelatedNodeType nodeType) {
		this.treeBeanList = treeBeanList;
		for (JecnTreeBean jecnTreeBean : treeBeanList) {
			setOldIds.add(jecnTreeBean.getId());
		}
		this.nodeType = nodeType;
		initComs();
		initLayOut();
	}
	
	/**
	 * 流程架构、流程、文件相关标准等
	 * 
	 * @param activeData
	 */
	public RelatedFilePanel(List<JecnTreeBean> treeBeanList, RelatedNodeType nodeType,Long ruleId) {
		this.treeBeanList = treeBeanList;
		for (JecnTreeBean jecnTreeBean : treeBeanList) {
			setOldIds.add(jecnTreeBean.getId());
		}
		this.nodeType = nodeType;
		this.relatedId = ruleId;
		initComs();
		initLayOut();
	}


	public Set<Long> getResultIds() {
		Set<Long> set = new HashSet<Long>();
		for (JecnTreeBean jecnTreeBean : treeBeanList) {
			set.add(jecnTreeBean.getId());
		}
		return set;
	}

	/**
	 * 初始化组件
	 * 
	 */
	protected void initComs() {
		mainPanel = new JecnPanel(600, 400);
		relatedFileTable = new RelatedFileTable();
		relatedFileTable.setRowHeight(20);
		relatedFileTable.initTable();
		buttonPanel = new JecnPanel();
		refButton = new JButton(JecnProperties.getValue("relevance"));
		deleteButton = new JButton(JecnProperties.getValue("deleteBtn"));

		// 设置背景色
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 关联
		refButton.addActionListener(new ButtonListener());
		// 删除
		deleteButton.addActionListener(new DeleteButtListener());
	}

	/**
	 * 布局
	 * 
	 */
	protected void initLayOut() {
		Insets insets = new Insets(3, 3, 3, 3);
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		// 表格面板
		mainPanel.add(scrollPane, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		// 按钮面板
		mainPanel.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(refButton);
		buttonPanel.add(deleteButton);

		// 添加主面板
		Insets insets1 = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets1,
				0, 0);
		this.add(mainPanel, c);
	}

	/**
	 * 关联风险按钮
	 * 
	 * @author Administrator
	 * @date： 日期：2013-11-20 时间：下午01:52:55
	 */
	class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JButton) {
				buttonActionPerformed(nodeType);
			}
		}
	}

	/**
	 * 
	 * 选中一行，删除
	 * 
	 * @author ZHAGNXIAOHU
	 * @date： 日期：2013-11-22 时间：下午02:08:45
	 */
	class DeleteButtListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			deleteActionPerformed();
		}
	}

	/**
	 * 删除选中行
	 * 
	 */
	protected void deleteActionPerformed() {
		int[] selectRows = relatedFileTable.getSelectedRows();
		if (selectRows.length == 0) {
			// 请选择一行!
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("chooseOneRow"));
			return;
		}
		List<JecnTreeBean> deleteList = new ArrayList<JecnTreeBean>();
		for (int i = selectRows.length - 1; i >= 0; i--) {
			relatedFileTable.getEditMode().removeRow(selectRows[i]);
			deleteList.add(treeBeanList.get(selectRows[i]));
		}
		// 删除集合中存在
		treeBeanList.removeAll(deleteList);
	}

	/**
	 * 关联按钮
	 * 
	 */
	protected void buttonActionPerformed(RelatedNodeType nodeType) {
		switch (nodeType) {
		case rule:
			RuleChooseDialog ruleChooseDialog = new RuleChooseDialog(treeBeanList, 1L);
			ruleChooseDialog.setVisible(true);
			if (ruleChooseDialog.isOperation()) {// 操作
				initTableData();
			}
			break;
		case risk:
			RiskChooseDialog riskChooseDialog = new RiskChooseDialog(treeBeanList);
			riskChooseDialog.setVisible(true);
			if (riskChooseDialog.isOperation()) {// 操作
				initTableData();
			}
			break;
		case standard:
			StandardChooseDialog standardChooseDialog = new StandardChooseDialog(treeBeanList, 0);
			standardChooseDialog.setVisible(true);
			if (standardChooseDialog.isOperation()) {// 操作
				initTableData();
			}
			break;
		case standardizedFile:
			FileChooseDialog chooseDialog = new FileChooseDialog(treeBeanList);
			chooseDialog.setVisible(true);
			if (chooseDialog.isOperation()) {
				initTableData();
			}
			break;
		default:
			break;
		}

	}

	protected void initTableData() {
		if (treeBeanList.size() > 0) {
			// 清除表格数据
			((RelatedFileTableModel) relatedFileTable.getModel()).remoeAll();
			for (JecnTreeBean treeBean : treeBeanList) {
				if(relatedId.longValue() == treeBean.getId().longValue()){
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("cannotRelateThis"));
					treeBeanList.remove(treeBean);
					return ;
				}
				((RelatedFileTableModel) relatedFileTable.getModel()).addRow(initTableRowData(treeBean));
			}
		} else {
			// 清除表格数据
			((RelatedFileTableModel) relatedFileTable.getModel()).remoeAll();
		}
	}

	protected abstract Vector<Object> initTableRowData(JecnTreeBean treeBean);

	protected class RelatedFileTable extends BaseCommonJTable {
		public RelatedFileTable() {
			super();
		}

		/**
		 * 鼠标进入表格监听
		 * 
		 * @param e
		 */
		protected void mouseMovedProcess(MouseEvent e) {
			int row = this.rowAtPoint(e.getPoint());
			int column = this.columnAtPoint(e.getPoint());
			if (myRow == row && column == myColumn) {// 始终在指定的单元格移动，不执行重绘
				return;
			}
			myRow = this.rowAtPoint(e.getPoint());
			myColumn = this.columnAtPoint(e.getPoint());

			this.revalidate();
			this.repaint();
		}

		protected void initTable() {
			scrollPane = new JScrollPane();
			Dimension dimension = new Dimension(550, 360);
			scrollPane.setPreferredSize(dimension);
			scrollPane.setMaximumSize(dimension);
			scrollPane.setMinimumSize(dimension);
			scrollPane.setViewportView(this);
			scrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

			// 隐藏table
			hiddenColumns();
		}

		@Override
		public Vector<Vector<Object>> updateTableContent(List<Object> list) {
			return getContent();
		}

		@Override
		public Vector<Object> getTableTitleVector() {
			return getTitle();
		}

		/**
		 * 
		 * 单元格是否可编辑
		 * 
		 * @return
		 */
		protected void isClickEdit() {
			isClickTableEdit();
		}

	}

	public JecnTableCellRenderer newEditJLabel() {
		return new JecnTableCellRenderer(JecnProperties.getValue("linkDetail"));
	}

	/**
	 * 
	 * 单元格是否可编辑
	 * 
	 * @return
	 */
	protected abstract void isClickTableEdit();

	/**
	 * 隐藏table列
	 * 
	 */
	protected void hiddenColumns() {
		// 渲染表格
		relatedFileTable.getColumnModel().getColumn(2).setCellRenderer(
				new JecnTableCellRenderer(JecnProperties.getValue("linkDetail")));
		TableColumnModel columnModel = relatedFileTable.getColumnModel();
		columnModel.getColumn(2).setMaxWidth(100);
		columnModel.getColumn(2).setMinWidth(100);

		TableColumn tableColumn = columnModel.getColumn(0);
		// 默认隐藏
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);

		relatedFileTable.setRowHeight(20);
	}

	/**
	 * 获取表格数据
	 * 
	 * @return
	 */
	protected Vector<Vector<Object>> getContent() {
		// 获取制度关联标准集合，封装treeBeanList
		Vector<Vector<Object>> obVector = new Vector<Vector<Object>>();
		Vector<Object> vs = null;
		for (JecnTreeBean treeBean : treeBeanList) {
			vs = initTableRowData(treeBean);
			obVector.add(vs);
		}
		return obVector;
	}

	/**
	 * 获取表头
	 * 
	 * @return
	 */
	protected Vector<Object> getTitle() {
		Vector<Object> title = new Vector<Object>();
		title.add("id");
		// 名称
		title.add(JecnProperties.getValue("name"));
		// 操作
		title.add(JecnProperties.getValue("operational"));
		return title;
	}

	public boolean isUpdate() {
		if (setOldIds.size() != treeBeanList.size()) {
			return true;
		}
		for (Long id : setOldIds) {
			boolean isExist = false;
			for (JecnTreeBean jecnTreeBean : treeBeanList) {
				if (id.equals(jecnTreeBean.getId())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		return false;
	}

	protected enum RelatedNodeType {
		rule, risk, standard, standardizedFile
	}

	public List<JecnTreeBean> getTreeBeanList() {
		return treeBeanList;
	}

	public void setTreeBeanList(List<JecnTreeBean> treeBeanList) {
		this.treeBeanList = treeBeanList;
	}

	public JecnPanel getButtonPanel() {
		return buttonPanel;
	}

	public void setButtonPanel(JecnPanel buttonPanel) {
		this.buttonPanel = buttonPanel;
	}

	public JecnPanel getMainPanel() {
		return mainPanel;
	}

	public void setMainPanel(JecnPanel mainPanel) {
		this.mainPanel = mainPanel;
	}

}
