package epros.draw.gui.operationConfig;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 添加概况信息配置
 * 
 * @author 2012-07-03
 * 
 */
public class AddConfigurationDilaog extends JecnDialog {
	private static final Log log = LogFactory.getLog(AddConfigurationDilaog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(400, 450);
	/** 包括，结果面板、标题按钮面板 */
	private JecnPanel ruleModePanel = new JecnPanel(375, 370);
	/** 标题列表面板 */
	private JScrollPane titlePanel = new JScrollPane();
	/** 底部按钮面板 */
	private JecnPanel bomButtonPanel = new JecnPanel(385, 30);

	/** 加入标题按钮 */
	private JButton addTitleButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("add"));

	/** 取消按钮 */
	private JButton cancelButton = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("cancel"));

	public JecnTable titleTable = null;

	public boolean isOperation = false;
	/** 设置大小 */
	private Dimension dimension = null;
	/** 添加的ID */
	private List<DrawOperationDescriptionConfigBean> addOpera = new ArrayList<DrawOperationDescriptionConfigBean>();

	List<DrawOperationDescriptionConfigBean> operConfigList = null;

	public AddConfigurationDilaog(List<DrawOperationDescriptionConfigBean> operConfigList) {
		this.operConfigList = operConfigList;
		initCompotents();
		initLayout();
		initButtonAction();
	}

	private void initCompotents() {
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("configurationProcessOperatingInstructions"));
		this.setSize(400, 450);
		this.setResizable(true);
		this.setModal(true);
		this.setLocationRelativeTo(null);

		// 标题列表面板
		dimension = new Dimension(350, 350);
		titlePanel.setPreferredSize(dimension);
		titlePanel.setMaximumSize(dimension);
		titlePanel.setMinimumSize(dimension);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		ruleModePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		bomButtonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		titlePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		titlePanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	public void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 2, 2);
		// infoPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(ruleModePanel, c);
		ruleModePanel.setLayout(new GridBagLayout());
		ruleModePanel.setBorder(BorderFactory.createTitledBorder(""));

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		ruleModePanel.add(titlePanel, c);
		this.titleTable = new titleTable();
		titlePanel.setViewportView(titleTable);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(bomButtonPanel, c);
		bomButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		bomButtonPanel.add(addTitleButton);
		bomButtonPanel.add(cancelButton);

		this.getContentPane().add(mainPanel);
	}

	public void initButtonAction() {
		// 加入
		addTitleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addTitleButtonAction();
			}
		});
		// 取消
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
	}

	/**
	 * @author yxw 2012-5-15
	 * @description:加入标题
	 */
	public boolean addTitleButtonAction() {
		int[] rows = titleTable.getSelectedRows();
		for (int i = 0; i < rows.length; i++) {
			int flagId = Integer.valueOf(titleTable.getModel().getValueAt(rows[i], 2).toString());
			DrawOperationDescriptionConfigBean configurBean = operConfigList.get(flagId);
			if (configurBean.getIsShow() == 0) {
				configurBean.setIsShow(1);
				isOperation = true;
				addOpera.add(configurBean);
			}
		}
		this.dispose();
		return isOperation;
	}

	public void cancelButtonAction() {
		this.dispose();
	}

	public Vector<String> getTableTitle() {
		Vector<String> vector = new Vector<String>();
		vector.add(JecnResourceUtil.getJecnResourceUtil().getValue("serialNumber"));
		vector.add(JecnResourceUtil.getJecnResourceUtil().getValue("name"));
		vector.add("flagId");
		return vector;
	}

	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		// 获取操作说明数据
		try {
			int count = 1;
			for (int i = 0; i < operConfigList.size(); i++) {
				DrawOperationDescriptionConfigBean operCofigBean = operConfigList.get(i);
				Vector<String> row = new Vector<String>();
				// 是否显示 0:不显示，1：显示
				if (operCofigBean.getIsShow() == 0) {
					row.add(String.valueOf(count));
					count++;
					row.add(operCofigBean.getName());
					row.add(String.valueOf(i));
					vs.add(row);
				}
			}

		} catch (Exception e) {
			log.error("", e);
		}

		return vs;
	}

	class titleTable extends JecnTable {

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent());
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] { 2 };
		}

	}

	public List<DrawOperationDescriptionConfigBean> getAddOpera() {
		return addOpera;
	}

}
