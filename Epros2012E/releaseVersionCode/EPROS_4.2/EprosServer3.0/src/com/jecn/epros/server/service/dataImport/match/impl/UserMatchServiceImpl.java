package com.jecn.epros.server.service.dataImport.match.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.JecnUserInfo;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.dataImport.match.IMatchUserDataCheckDao;
import com.jecn.epros.server.dao.dataImport.match.IMatchUserImportDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.service.dataImport.PathUpdater;
import com.jecn.epros.server.service.dataImport.match.IUserMatchService;
import com.jecn.epros.server.service.dataImport.match.buss.AbstractFileSaveDownMatchService;
import com.jecn.epros.server.service.dataImport.match.buss.CheckDeptData;
import com.jecn.epros.server.service.dataImport.match.buss.CheckPositionData;
import com.jecn.epros.server.service.dataImport.match.buss.CheckUserData;
import com.jecn.epros.server.service.dataImport.match.constant.MatchErrorInfo;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool.DataErrorType;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.util.JecnKey;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.dataImport.match.ActualPosBean;
import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.PosEprosRelBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;

/**
 * 
 * 人员数据导入处理类
 * 
 * @author ZHAGNXIAOHU
 */
@Transactional(rollbackFor = Exception.class)
public class UserMatchServiceImpl extends AbstractFileSaveDownMatchService
		implements IUserMatchService {
	private final Log log = LogFactory.getLog(UserMatchServiceImpl.class);

	/** DAO层专门处理UserImportBuss类的DB操作 */
	private IMatchUserImportDao matchUserImportDao = null;
	/** 部门行内、行间校验 */
	private CheckDeptData deptCheckData = null;
	/** 岗位行内、行间校验 */
	private CheckPositionData posCheckData = null;
	/** 人员行内、行间校验 */
	private CheckUserData userCheckData = null;
	/** 人员表信息集合 */
	private List<UserMatchBean> userList = null;
	/** 人员岗位匹配关系表 */
	private List<PosEprosRelBean> relList = null;
	/** 临时岗位表信息 */
	private List<PosMatchBean> posList = null;
	/** 获取当前项目ID */
	private Long curProjectId = null;
	/** 人员同步数据验证 */
	private IMatchUserDataCheckDao matchUserCheckDao;
	/** 系统配置 */
	private IJecnConfigItemDao configDao = null;

	/**
	 * 人员同步行内校验
	 * 
	 */
	@Override
	public DataErrorType checkData(BaseBean baseBean) {
		// 2.验证数据：行内验证
		// ----------------------验证数据 start
		// 部门
		if (!deptCheckData.checkDeptRow(baseBean)) {
			return DataErrorType.rowsType;
		}
		log.info("部门行内校验成功！");
		// 3.人员(到这校验应该达到完全正确才行)
		if (!userCheckData.checkUserPosRow(baseBean)) {
			return DataErrorType.rowsType;
		}
		if (userCheckData.getUserNumberList().size() > JecnKey
				.getServerPeopleCounts()) {// 导入人数大于服务端最大限制
			log.error("超出最大用户数！");
			return DataErrorType.serverCount;
		}
		log.info("人员行内校验成功！");
		// 4.岗位行内校验
		if (!posCheckData.checkPosRow(baseBean)) {
			return DataErrorType.rowsType;
		}
		log.info("岗位行内校验成功！");
		return DataErrorType.none;
	}

	/**
	 * 人员同步行间校验
	 * 
	 * @return DataErrorType
	 */
	@Override
	public DataErrorType checkDataColumns() {
		if (!checkUserPosColu()) {// 人员原始结构行间校验
			return DataErrorType.columnsType;
		}
		log.info("验证数据：人员，人员岗位行间校验结束");
		if (!checkDeptColu()) {// 部门原始结构行间校验
			return DataErrorType.columnsType;
		}
		log.info("验证数据：部门行间校验结束");
		return DataErrorType.none;
	}

	/**
	 * 清除临时库、入库临时库
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	@Override
	public void deleteAndInsertTmpJecnIOTable(BaseBean baseBean)
			throws Exception {
		matchUserImportDao.deleteAndInsertTmpJecnIOTable(baseBean);
	}

	/**
	 * 如果 实际人员表、实际部门表、实际岗位表中有数据，则用临时表中的数据与实际表中的数据进行比较
	 * 
	 * @param curProjectId
	 * @throws Exception
	 */
	@Override
	public void updateProFlag(Long curProjectId) throws Exception {
		matchUserImportDao.updateProFlag(curProjectId);
	}

	/**
	 * 获取人员同步原始数据
	 * 
	 * @return
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean exportExcelOriData() {
		// 人员记录
		String sql = "select t.* from jecn_match_user_bean t order by t.login_name";
		List<UserMatchBean> userBeanList = matchUserImportDao.getSession()
				.createSQLQuery(sql).addEntity(UserMatchBean.class).list();
		// 岗位集合
		sql = "select t.* from jecn_match_pos_bean t";
		List<PosMatchBean> posList = matchUserImportDao.getSession()
				.createSQLQuery(sql).addEntity(PosMatchBean.class).list();

		// 部门集合
		sql = "select t.* from jecn_match_dept_bean t";
		List<DeptMatchBean> deptBeanList = matchUserImportDao.getSession()
				.createSQLQuery(sql).addEntity(DeptMatchBean.class).list();

		BaseBean baseBean = new BaseBean(deptBeanList, posList, userBeanList);
		// 人员错误数据导出模板文件
		this.setOutputExcelModelPath(MatchTool.getOutputErrorExcelModelPath());
		// 人员错误数据导出数据文件
		this.setOutputExcelDataPath(MatchTool.getOutPutExcelErrorDataPath());

		// 从DB中读数据写入到excel再保存到本地
		return editExcel(baseBean.getDeptBeanList(), baseBean
				.getUserPosBeanList(), baseBean.getPosBeanList());
	}

	/**
	 * 获得主项目
	 * 
	 * @return Long
	 * @throws DaoException
	 */
	@Override
	public Long getCurJecnProject() throws Exception {
		String hql = "select curProjectId from JecnCurProject";
		List<Long> list = this.matchUserImportDao.listHql(hql);
		if (list.size() > 0) {
			return list.get(0);
		}
		log.info("查询上次导入项目ID：通用部门临时表(TMP_JECN_IO_DEPT) null 数据结束 ");
		return null;
	}

	/**
	 * 操作人员表与临时表比较获取人员岗位变岗的集合
	 * 
	 * Object[] 0:岗位编号，1：登录名称
	 * 
	 * @return List<Object[]>
	 */
	@Override
	public List<Object[]> findJecnUserObject() {
		// 操作人员表与临时表比较获取人员岗位变岗的集合 (岗位变更数据集)
		return matchUserImportDao.findJecnUserObject();
	}

	/**
	 * 删除实际表(部门，岗位，人员)中的原有的数据
	 * 
	 * @throws Exception
	 */
	@Override
	public void deleteActualMatchTableData() throws Exception {
		// /*** 删除实际表(部门，岗位，人员)中的原有的数据
		matchUserImportDao.deleteActualMatchTableData();
	}

	/**
	 * 
	 * 将临时表(部门，岗位，人员)中的数据添加到实际表(部门，岗位，人员)
	 * 
	 * @throws Exception
	 */
	@Override
	public void insertActualMatchTableData() throws Exception {
		// /**** 将临时表(部门，岗位，人员)中的数据添加到实际表(部门，岗位，人员)
		matchUserImportDao.insertActualMatchTableData();
	}

	/**
	 * 删除人员岗位关系表 （删除 流程岗位ID在岗位匹配关系表中不存在的 记录）
	 * 
	 * @throws Exception
	 */
	@Override
	public void deletePosRelUser() throws Exception {
		/**
		 * 如果删除的岗位是实际岗位则 删除流程岗位与实际岗位关联表中的数据 如果删除的岗位是基准岗位则 不处理流程岗位与实际岗位关联表中的数据
		 */
		// 根据 流程岗位与实际岗位关联表数据在实际岗位表中不存在的岗位编码 查询流程岗位与实际岗位关联表数据
		// 获取流程中岗位ID（根据操作表要删除的岗位编号，和匹配关系表找出需要删除的流程岗位ID）
		// 是否删除流程中岗位与人员的关联关系
		matchUserImportDao.deletePosRelUser();

		log.info("删除人员岗位关系表开始 ：" + System.currentTimeMillis());
		matchUserImportDao.deleteEpsActPos();
		log.info("与真实库同步，删除人员岗位关系表结束 : " + System.currentTimeMillis());

	}

	/**
	 * 
	 * 与真实库同步同步数据
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	@Override
	public void syncImortDataAndDB(List<Object[]> posChangeList)
			throws Exception {
		/**
		 * 导入的人员数据 在人员岗位匹配关系表中查询是否存在匹配关系， 若存在，则更新流程人员表中岗位匹配，
		 * 若不存在且流程表中没有该人员信息，则添加到流程中， 若不存在且流程中有该人员信息，则锁定流程中对应的人员信息不作修改
		 */
		// 获取岗位实际表数据
		List<ActualPosBean> listActualPosBean = matchUserImportDao
				.selectActPos();
		// 获取添加的人员信息 标识位： 0：添加 1：更新
		relationInfo(listActualPosBean, posChangeList, 0);
		log.info("添加 人员相关岗位结束");

		// 获取 更新的人员信息 标识位： 0：添加 1：更新
		relationInfo(listActualPosBean, posChangeList, 1);
		log.info("更新人员相关岗位结束");

		log.info("获取未匹配的人员岗位数据（已存在匹配关系，但是Epros中人员岗位关系被删除情况） start");
		// 获取未匹配的人员岗位数据（已存在匹配关系，但是Epros中人员岗位关系被删除情况）
		List<Object[]> notMatchPosList = findNotMatchPosList();
		// 去除重复数据 (一人多岗数据可能和存在匹配关系的时间重复)
		reduceList(notMatchPosList, posChangeList);
		// 更新匹配关系 通过未匹配的人员岗位数据 同步JecnUserInfo
		addNoMatchUserPos(notMatchPosList, listActualPosBean);
		log.info("更新未匹配的人员岗位数据（已存在匹配关系，但是Epros中人员岗位关系被删除情况） end");
		// 更改 流程人员表中的锁定状态 ISLOCK 0是解锁，1是锁定 人员实际表中没有的流程人员数据，将锁定状态改为1 ：锁定
		matchUserImportDao.udateJecnUserIsLock();
		log.info("更改 流程人员表中的锁定状态 ISLOCK 0是解锁，1是锁定");
		
		updateOrgTLevelAndTPath();
	}
	
	private void updateOrgTLevelAndTPath() {
		PathUpdater updater=new PathUpdater(matchUserImportDao);
		//升级T_LEVEL T_PATH VIEW_SORT
		updater.updateOrgTLevelAndTPath();
	}

	/**
	 * 去除重复数据
	 * 
	 * @param notMatchPosList
	 *            岗位变更集合
	 * @param posChangeList
	 *            人员岗位关联删除后取出未匹配关系集合
	 */
	private void reduceList(List<Object[]> notMatchPosList,
			List<Object[]> posChangeList) throws Exception {
		if (notMatchPosList == null || posChangeList == null) {
			return;
		}
		List<Object[]> deleteList = new ArrayList<Object[]>();
		for (Object[] objects : notMatchPosList) {
			if (objects[0] == null || objects[1] == null) {
				continue;
			}
			String notMatchPosNum = objects[0].toString();
			String notMatchLoginName = objects[1].toString();
			for (Object[] objects1 : posChangeList) {
				if (objects1[0] == null || objects1[1] == null) {
					continue;
				}
				String changePosNum = objects1[0].toString();
				String changeLoginName = objects1[1].toString();
				if (notMatchPosNum.equals(changePosNum)
						&& notMatchLoginName.equals(changeLoginName)) {
					// 记录重复记录
					deleteList.add(objects);
				}
			}
		}
		if (deleteList.size() > 0) {
			notMatchPosList.removeAll(deleteList);
		}
	}

	/**
	 * 添加数据库 获取未匹配的人员岗位数据（已存在匹配关系，但是Epros中人员岗位关系被删除情况）
	 * 
	 * @param notMatchPosList
	 * @param listActualPosBean
	 * @throws Exception
	 */
	private void addNoMatchUserPos(List<Object[]> notMatchPosList,
			List<ActualPosBean> listActualPosBean) throws Exception {
		// 收集人员登录名称唯一记录
		Set<String> setNameList = new HashSet<String>();
		for (Object[] objects : notMatchPosList) {
			if (objects[1] == null) {
				continue;
			}
			setNameList.add(objects[1].toString());
		}

		// 根据人员登录名称获取人员登录名和人员主键ID
		String sql = "SELECT U.LOGIN_NAME,U.PEOPLE_ID FROM JECN_USER U WHERE U.LOGIN_NAME IN ";
		List<String> list = JecnCommonSql.getStringSqlsBySet(sql, setNameList);
		if (list == null) {
			return;
		}
		List<Object[]> lists = new ArrayList<Object[]>();
		for (String string : list) {
			List<Object[]> listOb = matchUserImportDao.listNativeSql(string);
			lists.addAll(listOb);
		}

		// map集合 获取人员Id和登录名称的Map集合
		Map<String, Long> map = new HashMap<String, Long>();
		for (Object[] objects2 : lists) {
			// key :登录名称；value：主键ID
			map.put(objects2[0].toString(), Long
					.valueOf(objects2[1].toString()));
		}

		// 人员岗位变岗的集合
		if (notMatchPosList.size() > 0) {
			List<JecnUserInfo> listJecnUserInfo = new ArrayList<JecnUserInfo>();
			String posNum = null;
			String loginName = null;
			for (Object[] object : notMatchPosList) {
				if (object[0] != null) {
					posNum = object[0].toString();
				}
				if (object[1] != null) {
					loginName = object[1].toString();
				}
				// 如果不存在，跳出当前循环
				if (loginName == null || posNum == null) {
					continue;
				}
				// 获取人员主键ID
				Long peopleId = map.get(loginName);
				getJecnUserInfo(posNum, listJecnUserInfo, peopleId,
						listActualPosBean);
			}
			// 去除数据库中已存在的关联
			List<JecnUserInfo> addJecnUserInfoList = deleteExitsPosUser(listJecnUserInfo);
			if (addJecnUserInfoList.size() == 0) {
				// 不存在岗位关联关系
				return;
			}
			matchUserImportDao.insertJecnUserPos(addJecnUserInfoList);
		}
	}

	/**
	 * 获取未匹配的人员岗位数据 (实际解决问题是 Epros删除人员或者删除岗位时，人员岗位关系表未删除)
	 * 
	 * @return List<Object[]>
	 */
	private List<Object[]> findNotMatchPosList() throws Exception {
		// 获取未匹配的人员岗位数据：通过Epros人员岗位与数据和岗位匹配关系数据对比，去除交集，获取岗位匹配人员和岗位关系在Epros中不存在的记录"

		// TMP_USER Epros中存在人员岗位匹配关系的数据
		// TMP_MATCH_USER 存在匹配关系的人员岗位数据

		// 返回值 ：TMP_MATCH_USER　在　TMP_USER　中不存在的集合
		String sql = "with TMP_USER AS ("
				+ "  SELECT JU.LOGIN_NAME, UP.FIGURE_ID"
				+ "  FROM JECN_USER JU"
				+ "  INNER JOIN JECN_USER_POSITION_RELATED UP ON JU.PEOPLE_ID = UP.PEOPLE_ID"
				+ " ),TMP_MATCH_USER AS ("
				+ " SELECT MU.LOGIN_NAME,MU.POSITION_NUM ,TMP_MATCH_POS.EPS_POSITION_ID"
				+ "  FROM JECN_MATCH_USER_BEAN MU"
				+ "  INNER JOIN (SELECT PEL.EPS_POSITION_ID, MAP.ACT_POS_NUM"
				+ "               FROM JECN_MATCH_POS_EPROS_REL PEL"
				+ "              INNER JOIN JECN_MATCH_ACTUAL_POS MAP ON PEL.REL_TYPE = 0"
				+ "                                                  AND PEL.POSITION_NUM ="
				+ "                                                      MAP.ACT_POS_NUM"
				+ "             UNION"
				+ "             SELECT PEL.EPS_POSITION_ID, MAP.ACT_POS_NUM"
				+ "               FROM JECN_MATCH_POS_EPROS_REL PEL"
				+ "              INNER JOIN JECN_MATCH_ACTUAL_POS MAP ON PEL.REL_TYPE = 1"
				+ "                                                  AND PEL.POSITION_NUM ="
				+ "                                                      MAP.BASE_POS_NUM) TMP_MATCH_POS ON MU.POSITION_NUM ="
				+ "                                                                                         TMP_MATCH_POS.ACT_POS_NUM"
				+ ")" + "select MU.POSITION_NUM, MU.LOGIN_NAME"
				+ "  FROM TMP_MATCH_USER MU" + " WHERE NOT EXISTS (SELECT 1"
				+ "          FROM TMP_USER TMU"
				+ "         WHERE TMU.LOGIN_NAME = MU.LOGIN_NAME"
				+ "           AND TMU.FIGURE_ID = MU.EPS_POSITION_ID)";
		log.error("未匹配的人员岗位数据 :" + sql);
		return matchUserImportDao.listNativeSql(sql);
	}

	/**
	 * 根据登录名称获取流程真是人员信息
	 * 
	 * @param loginName
	 * @param listJecnUser
	 * @return
	 */
	public JecnUser getJecnUser(String loginName, List<JecnUser> listJecnUser) {
		if (listJecnUser == null || loginName == null) {
			return null;
		}
		for (JecnUser jecnUser : listJecnUser) {
			if (loginName.equals(jecnUser.getLoginName())) {
				return jecnUser;
			}
		}
		return null;
	}

	/**
	 * 获取实际岗位表中的基准岗位编码
	 * 
	 * @param posNum
	 * @param listActualPosBean
	 * @return
	 */
	public String getPosBaseNum(String posNum,
			List<ActualPosBean> listActualPosBean) {
		if (listActualPosBean == null || posNum == null) {
			return null;
		}
		for (ActualPosBean actualPosBean : listActualPosBean) {
			if (actualPosBean.getActPosNum().equals(posNum)) {
				// 岗位对应的基准岗位编码
				return actualPosBean.getBasePosNum();
			}
		}
		return null;
	}

	/**
	 * 组装流程信息中人员信息
	 * 
	 * @param userBean
	 * @param isLock
	 * @return
	 */
	public JecnUser getJecnUser(UserMatchBean userBean) {
		JecnUser jecnUser = new JecnUser();
		jecnUser.setIsLock(0L);
		jecnUser.setLoginName(userBean.getLoginName());
		jecnUser.setEmail(userBean.getEmail());
		int emailtyp = userBean.getEmailType();
		String emailType = null;
		if (emailtyp == 0) {
			emailType = "inner";
		} else {
			emailType = "outer";
		}
		jecnUser.setPassword("AC");
		jecnUser.setEmailType(emailType);
		jecnUser.setPhoneStr(userBean.getPhone());
		jecnUser.setTrueName(userBean.getTrueName());
		return jecnUser;
	}

	/***************************************************************************
	 * 流程中岗位与人员管理表匹配关系
	 * 
	 * @param listActualPosBean
	 *            实际岗位集合
	 * @param isFlag
	 *            0：添加 1：更新
	 * @param posChangeList
	 *            操作人员表与临时表比较获取人员岗位变岗的集合
	 * @throws Exception
	 */
	public void relationInfo(List<ActualPosBean> listActualPosBean,
			List<Object[]> posChangeList, int isFlag) throws Exception {

		// 获取待添加的记录或待更新记录
		userList = matchUserImportDao.selectUserBeanList(isFlag);
		if (userList == null || userList.size() == 0) {
			return;
		}

		// 流程中岗位与人员管理表
		List<JecnUserInfo> listJecnUserInfo = new ArrayList<JecnUserInfo>();
		// 流程岗位与实际岗位关联表记录
		relList = matchUserImportDao.selectPosUserRel();
		// 实际岗位临时表数据 listActualPosBean
		// posList = iMatchUserImportDao.selectPosByRelId();
		Map<String, Long> map = new HashMap<String, Long>();
		// 流程中实际人员信息集合
		List<JecnUser> listJecnUser = null;
		// 添加人员
		if (isFlag == 0) {
			listJecnUser = new ArrayList<JecnUser>();
			long cuurTime = System.currentTimeMillis();
			log.info("添加人员信息开始 ：" + cuurTime);
			// 记录添加的登录名称集合
			List<String> listLoginNames = new ArrayList<String>();

			// 添加人员
			for (UserMatchBean userBean : userList) {
				if (!listLoginNames.contains(userBean.getLoginName())) {
					listLoginNames.add(userBean.getLoginName());
					JecnUser jecnUser = getJecnUser(userBean);
					if (jecnUser != null) {
						// listJecnUser.add(jecnUser);
						matchUserImportDao.getSession().save(jecnUser);
						map.put(jecnUser.getLoginName(), jecnUser
										.getPeopleId());
					}
				}

			}
			// if (listJecnUser != null && listJecnUser.size() > 0) {
			// // 添加listJecnUser 到数据库
			// matchUserImportDao.insertJecnUserInfo(listJecnUser, 0);
			// }
			/**
			 * //法二： 添加listJecnUser 到数据库 //
			 * iMatchUserImportDao.insertJecnUserInfos(); //添加人员临时表信息到流程人员表 //
			 * listJecnUser = iMatchUserImportDao.getJecnUserInfoList();
			 * //查询流程人员表
			 */
			for (UserMatchBean userBean : userList) { // 临时人员表标识为添加的信息

				// 获取人员主键ID
				Long peopleId = map.get(userBean.getLoginName());
				// 设置人员、岗位匹配关系
				getJecnUserInfo(userBean.getPosNum(), listJecnUserInfo,
						peopleId, listActualPosBean);
			}
			log.info("添加人员信息结束：" + (System.currentTimeMillis() - cuurTime));
		} else if (isFlag == 1) {// 更新
			// 临时表EHR数据 (Epros系统需要更新的人员信息集合)
			listJecnUser = matchUserImportDao.findJecnUser();
			for (JecnUser jecnUser : listJecnUser) {
				// 循环更新数据
				for (UserMatchBean userBean : userList) {
					if (jecnUser.getLoginName().equals(userBean.getLoginName())) {
						jecnUser.setEmail(userBean.getEmail());
						int emailtyp = userBean.getEmailType();
						String emailType = null;
						if (emailtyp == 0) {
							emailType = "inner";
						} else {
							emailType = "outer";
						}
						jecnUser.setEmailType(emailType);
						jecnUser.setPhoneStr(userBean.getPhone());
						jecnUser.setTrueName(userBean.getTrueName());
						// updateListUser.add(jecnUser);
						matchUserImportDao.getSession().update(jecnUser);
						map
								.put(jecnUser.getLoginName(), jecnUser
										.getPeopleId());
					}
				}
			}
			// 人员岗位变岗的集合
			if (posChangeList != null && posChangeList.size() > 0) {
				for (Object[] object : posChangeList) {
					String posNum = null;
					String loginName = null;
					if (object[0] != null) {
						posNum = object[0].toString();
					}
					if (object[1] != null) {
						loginName = object[1].toString();
					}
					// 如果不存在，跳出当前循环
					if (loginName == null || posNum == null) {
						continue;
					}
					// 获取人员主键ID
					Long peopleId = map.get(loginName);
					// 设置人员、岗位匹配关系
					getJecnUserInfo(posNum, listJecnUserInfo, peopleId,
							listActualPosBean);
				}

			}
			if (listJecnUserInfo.size() == 0) {
				return;
			}
		}
		if (relList == null || relList.size() == 0) {
			// 不存在关联关系
			return;
		}
		// 去除数据库中已存在的关联
		List<JecnUserInfo> addJecnUserInfoList = deleteExitsPosUser(listJecnUserInfo);
		if (addJecnUserInfoList == null || addJecnUserInfoList.size() == 0) {
			// 不存在岗位关联关系
			return;
		}
		matchUserImportDao.insertJecnUserPos(addJecnUserInfoList);
	}

	/**
	 * 重复记录不添加
	 * 
	 * @param listJecnUserInfo
	 * @param userInfo
	 */
	public void deleteSameUser(List<JecnUserInfo> listJecnUserInfo,
			JecnUserInfo userInfo) {
		if (listJecnUserInfo.size() == 0) {
			listJecnUserInfo.add(userInfo);
		}
		for (JecnUserInfo jecnUserInfo : listJecnUserInfo) {
			if (userInfo.getPeopleId().equals(jecnUserInfo.getPeopleId())
					&& userInfo.getFigureId()
							.equals(jecnUserInfo.getFigureId())) {
				return;
			}
		}
		listJecnUserInfo.add(userInfo);
	}

	/**
	 * 
	 * @param posNum
	 *            岗位编号
	 * @param listJecnUserInfo
	 *            获取待更新的流程岗位和人员管理数据集合
	 * @param JecnUser
	 *            jecnUser 人员信息集合
	 * @param listActualPosBean
	 *            操作表岗位信息集合
	 */
	public void getJecnUserInfo(String posNum,
			List<JecnUserInfo> listJecnUserInfo, Long peopleId,
			List<ActualPosBean> listActualPosBean) {
		if (relList == null) {
			return;
		}
		for (PosEprosRelBean posEprosRelBean : relList) {// 岗位临时表与流程岗位关联关系表集合
			if (peopleId == null) {
				break;
			}
			// 人员临时表中岗位编码与岗位匹配关联表比较获取匹配记录listActualPosBean
			if (posNum.equals(posEprosRelBean.getPosNum())
					&& posEprosRelBean.getRelType() == 0) {// 0:岗位匹配，1：基准岗位匹配
				// 设置人员、岗位匹配关系
				JecnUserInfo jecnUserInfo = new JecnUserInfo();
				jecnUserInfo.setFigureId(posEprosRelBean.getEpsPosId());
				jecnUserInfo.setPeopleId(peopleId);
				// listJecnUserInfo.add(jecnUserInfo);
				// 重复记录不添加
				deleteSameUser(listJecnUserInfo, jecnUserInfo);
			} else {
				// 获取实际岗位对应的基准岗位编号 应只取一次（暂不修改）
				String posBaseNum = getPosBaseNum(posNum, listActualPosBean);
				if (posBaseNum == null) {
					continue;
				}
				if (posBaseNum.equals(posEprosRelBean.getPosNum())
						&& posEprosRelBean.getRelType() == 1) {// 基准岗位匹配
					JecnUserInfo jecnUserInfo = new JecnUserInfo();
					jecnUserInfo.setFigureId(posEprosRelBean.getEpsPosId());
					jecnUserInfo.setPeopleId(peopleId);
					// listJecnUserInfo.add(jecnUserInfo);
					// 重复记录不添加
					deleteSameUser(listJecnUserInfo, jecnUserInfo);
				}
			}
		}
	}

	/**
	 * 
	 * 获取上次导入的项目ID，与当前项目ID做比较
	 * 
	 * 查询部门临时表（TMP_JECN_IO_DEPT）的项目ID：
	 * 
	 * 如果查询出来结果没有值，直接导入；
	 * 
	 * 如果查询结果有数据有且只有一条数据，判断项目ID是否等于当前主项目ID：
	 * 
	 * 如果是执行导入，如果不是不导入提示不能导入
	 * 
	 * 如果查询结果有数据且有多条数据，返回提示不正确
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean checkProjectId(Long pId) throws Exception {
		// 获取当前项目ID
		if (pId == null) {
			return false;
		}
		curProjectId = pId;
		// 查询部门临时表（TMP_JECN_IO_DEPT）
		List<Long> deptBeanList = matchUserImportDao.selectProjectId();

		if (deptBeanList == null || deptBeanList.size() == 0) {
			return true;
		} else if (deptBeanList.size() >= 2) {
			return false;
		}

		// 获取上次导入的项目ID
		Long projectId = deptBeanList.get(0);

		// 当前项目ID==上次导入项目ID
		if (curProjectId != null && curProjectId.equals(projectId)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 添加新的流程岗位与实际岗位匹配关系
	 * 
	 * @param posEprosRelList
	 * @return
	 * @throws Exception
	 */
	public boolean addNewPosEpsData(List<PosEprosRelBean> posEprosRelList,
			String[] strNums, int type, Long flowPosId) throws Exception {
		// 根据匹配关系获取需要更新的岗位的相应人员
		if (posEprosRelList == null) {
			return false;
		}
		// 实际岗位匹配 岗位编号/基准岗位集合
		List<String> posNumList = new ArrayList<String>();
		if (strNums != null) {
			for (String posNum : strNums) {
				posNumList.add(posNum.trim());
			}
		}
		// 根据岗位编号/基准岗位编号 获取该岗位/基准岗位对应的 所有人员
		List<JecnUserInfo> jecnUserInfoList = this.getFlowPeopleId(posNumList,
				type, flowPosId);

		// 去除已经存在的岗位人员匹配关系数据

		List<JecnUserInfo> addJecnUserInfoList = deleteExitsPosUser(jecnUserInfoList);
		// 添加 实际岗位与流程岗位关联表、 流程人员与岗位关联表
		return matchUserImportDao.addNewPosEpsData(posEprosRelList,
				addJecnUserInfoList);
	}

	/**
	 * 删除在 jecnUserInfoList 集合中的数据 在数据库中存在的记录
	 * 
	 * @param jecnUserInfoList
	 * @param figureId
	 * @return
	 * @throws Exception
	 */
	public List<JecnUserInfo> deleteExitsPosUser(
			List<JecnUserInfo> jecnUserInfoList) throws Exception {
		if (jecnUserInfoList == null) {
			return null;
		}
		// 获取人员岗位关联表中已存在的数据
		List<Object[]> exitsObjects = matchUserImportDao.isExitsUserPos();
		List<JecnUserInfo> addJecnUserInfoList = new ArrayList<JecnUserInfo>();
		for (JecnUserInfo jecnUserInfo : jecnUserInfoList) {
			if (!isExitsId(exitsObjects, jecnUserInfo.getPeopleId(),
					jecnUserInfo.getFigureId())) {
				addJecnUserInfoList.add(jecnUserInfo);
			}
		}
		return addJecnUserInfoList;
	}

	/**
	 * 判断人员ID 在以获取的ID集合中是否存在
	 * 
	 * @param exitsObjects
	 *            0:peopleId,1:fugireId
	 * @param peopleId
	 * @return
	 */
	public boolean isExitsId(List<Object[]> exitsObjects, Long peopleId,
			Long figureId) {
		for (Object[] object : exitsObjects) {
			if (object[0] == null || object[1] == null) {
				return false;
			}
			if (object[0].toString().equals(peopleId.toString())
					&& object[1].toString().equals(figureId.toString())) {
				return true;
			}
		}
		return false;
	}

	public List<Long> getPeopleIds(List<String> posNumList, int type,
			Long flowPosId) throws Exception {
		List<Object> listIds = null;
		if (posNumList.size() > 0) {
			// 根据岗位编号/基准岗位编号 获取该岗位/基准岗位对应的所有人员
			listIds = matchUserImportDao
					.getLoginNameByBaseNum(posNumList, type);
		}
		List<Long> peopleIds = null;
		if (listIds != null && listIds.size() > 0) {
			peopleIds = new ArrayList<Long>();
			for (Object id : listIds) {
				peopleIds.add(Long.valueOf(id.toString()));
			}
		}
		return peopleIds;
	}

	/**
	 * 添加 删除 通用 得到流程岗位与人员关联表数据集合 根据岗位编号/基准岗位编号 获取该岗位/基准岗位对应的所有人员
	 * 
	 * @param posNumList
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public List<JecnUserInfo> getFlowPeopleId(List<String> posNumList,
			int type, Long flowPosId) throws Exception {
		// 流程人员与岗位关联表
		List<JecnUserInfo> addJecnUserInfoList = new ArrayList<JecnUserInfo>();
		// 存在匹配关系的人员主键ID集合
		List<Long> peopleIds = getPeopleIds(posNumList, type, flowPosId);
		if (peopleIds == null) {
			return addJecnUserInfoList;
		}
		// 如果管理表中不存在 peopleIds人员记录责直接添加
		for (Long object : peopleIds) { // 人员ID
			if (object == null) {
				continue;
			}
			JecnUserInfo jecnUserInfo = new JecnUserInfo();
			jecnUserInfo.setPeopleId(object);
			jecnUserInfo.setFigureId(flowPosId);
			addJecnUserInfoList.add(jecnUserInfo);
		}
		return addJecnUserInfoList;
	}

	/***************************************************************************
	 * 删除 流程岗位与实际岗位关联表数据 JECN_POSITION_EPROS_REL 删除 流程人员与岗位管理表
	 * JECN_USER_POSITION_RELATED
	 * 
	 * @param posEprosRelBean
	 * @return
	 * @throws Exception
	 */
	@Override
	public void deletePosEpsData(Long flowPosId) throws Exception {
		matchUserImportDao.deletePosEpsData(flowPosId);
	}

	/**
	 * 获得匹配数据总数
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getPosEpsCount() throws Exception {
		return matchUserImportDao.getPosEpsCount();
	}

	/**
	 * 获取当前页岗位匹配集合
	 * 
	 * @return
	 * @throws BsException
	 * @throws Exception
	 */
	@Override
	public List<PosEprosRelBean> getStatcionData(int pageSize, int startRow)
			throws Exception {

		// 流程岗位ID集合
		List<Long> posIdList = matchUserImportDao.getJecnTempPositionBean(
				pageSize, startRow);
		if (posIdList.size() == 0) {
			return null;
		}
		// 根据岗位ID集合获取关联匹配信息
		List<PosEprosRelBean> epsPosList = matchUserImportDao
				.getPosEpsListInf(posIdList);
		List<String> posNumList = new ArrayList<String>();
		List<String> basePosNumList = new ArrayList<String>();

		for (PosEprosRelBean posEprosRelBean : epsPosList) {
			String posNum = posEprosRelBean.getPosNum();
			if (posEprosRelBean.getRelType() == 0) {// 岗位匹配
				if (!posNumList.contains(posNum)) {
					posNumList.add(posNum);
				}
			} else if (posEprosRelBean.getRelType() == 1) {// 基准岗位匹配
				if (!basePosNumList.contains(posNum)) {
					basePosNumList.add(posNum);
				}
			}
			if (!posIdList.contains(posEprosRelBean.getEpsPosId())) {
				posIdList.add(posEprosRelBean.getEpsPosId());
			}
		}
		if (posIdList.size() > 0) {
			// 根据岗位ID获取岗位名称
			List<Object[]> posObjectList = matchUserImportDao
					.getPosNameList(posIdList);
			List<PosEprosRelBean> resultList = getResultList(posNumList,
					basePosNumList, epsPosList, posIdList);
			if (resultList != null) {
				for (PosEprosRelBean relBean : resultList) {
					relBean.setFlowPosName(getStringById(relBean.getEpsPosId(),
							posObjectList));
				}
			}
			return resultList;
		}
		return null;
	}

	/**
	 * 根据ID获取名称
	 * 
	 * @param posId
	 * @param posList
	 *            0:id,1:名称
	 * @return
	 */
	public String getStringById(Long posId, List<Object[]> posList) {
		if (posList == null) {
			return null;
		}
		for (Object[] object : posList) {
			if (object[0] != null && object[1] != null) {
				if (object[0].toString().trim().equals(posId.toString())) {
					return object[1].toString().trim();
				}
			}
		}
		return null;
	}

	/**
	 * 根据匹配岗位的编码、ID查找对应的岗位名称
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> selectActEpsNames() throws Exception {
		if (curProjectId == null) {
			throw new NullPointerException();
		}
		return matchUserImportDao.selectActEpsNames(curProjectId);
	}

	/**
	 * 
	 * 
	 * 删除给定路径的文件 15010165914
	 * 
	 * @param filePath
	 */
	@Override
	public void deleteErrorLocalExcel(String filePath) {
		if (filePath == null) {
			return;
		}
		File file = new File(filePath);
		// 文件是否存在存在删除
		if (file.exists()) {
			file.delete();
		}
	}

	/**
	 * 
	 * 添加岗位匹配关系
	 * 
	 * @param stationId
	 * @param hrPosNums
	 * 
	 * @param type
	 * @throws Exception
	 */
	@Override
	public boolean addPosEpsRel(String stationId, String hrPosNums, int type)
			throws Exception {
		if (stationId == null) {
			return false;
		}
		String[] strNums = hrPosNums.split(",");
		if (strNums == null) {
			return false;
		}
		// 添加的 流程岗位与实际岗位关联表数据集合
		List<PosEprosRelBean> posEpsList = new ArrayList<PosEprosRelBean>();
		for (String posNum : strNums) {
			PosEprosRelBean posEprosRelBean = new PosEprosRelBean();
			posEprosRelBean.setEpsPosId(Long.valueOf(stationId.trim()));
			posEprosRelBean.setRelType(type);
			posEprosRelBean.setPosNum(posNum);
			posEpsList.add(posEprosRelBean);
		}
		if (posEpsList.size() > 0) {
			return addNewPosEpsData(posEpsList, strNums, type, Long
					.valueOf(stationId));
		}
		return false;
	}

	/***************************************************************************
	 * 更新岗位匹配关系
	 * 
	 * @param stationId
	 * @param hrPosNums
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean updatePosEpsRel(String stationId, String hrPosNums, int type)
			throws Exception {
		if (stationId == null) {
			return false;
		}
		String[] strNums = hrPosNums.split(",");
		if (strNums == null) {
			return false;
		}

		// 历史记录岗位编码集合 根据流程 岗位ID 从数据库获取 实际岗位与流程岗位关联表的数据
		List<String> dbRelNumList = matchUserImportDao.findPosEprosRelList(Long
				.valueOf(stationId));
		// 更新的 流程岗位与实际岗位关联表数据集合
		List<PosEprosRelBean> posEpsList = new ArrayList<PosEprosRelBean>();
		for (String posNum : strNums) {
			// for (PosEprosRelBean posEprosRelBean : dbRelList) {
			// 从编辑页面获取的数据， 不在从数据库获取数据的集合中,将这些不在的数据添加到新的对象,入库数据库
			PosEprosRelBean posEprosRelNew = new PosEprosRelBean();
			// if (!posNum.equals(posEprosRelBean.getPosNum())) {
			// posEprosRelNew.setPosNum(posNum);
			// posEprosRelNew.setRelType(type);
			// posEpsList.add(posEprosRelNew);
			// break;
			// }
			//				
			// }
			if (!dbRelNumList.contains(posNum)) {
				posEprosRelNew.setPosNum(posNum);
				posEprosRelNew.setRelType(type);
				posEprosRelNew.setEpsPosId(Long.valueOf(stationId));
				posEpsList.add(posEprosRelNew);
			}
		}
		// 最新岗位编码集合
		List<String> strNumList = new ArrayList<String>();
		for (int i = 0; i < strNums.length; i++) {
			strNumList.add(strNums[i]);
		}
		/**
		 * 数据库查询 根据从编辑页面获取的岗位编码集合 查询数据库中stationId为条件的关联表中是否在该集合范围内 若不存在
		 * 则删除数据库中对应的数据
		 */
		List<PosEprosRelBean> deleteNumList = matchUserImportDao
				.findRelByJspNum(Long.valueOf(stationId), strNumList);
		if (deleteNumList.size() > 0) {
			deleteRelNum(deleteNumList);// 删除数据
		}
		if (posEpsList.size() > 0) {
			return addNewPosEpsData(posEpsList, strNums, type, Long
					.valueOf(stationId));
		}
		return true;
	}

	/***************************************************************************
	 * 执行删除 编辑页面中没有的数据库中的数据 删除 流程岗位与人员关联表数据
	 * 
	 * @param deleteNumList
	 * @throws Exception
	 */
	public void deleteRelNum(List<PosEprosRelBean> deleteNumList)
			throws Exception {
		// 实际岗位匹配 岗位编号集合
		List<String> posNumList = new ArrayList<String>();
		for (PosEprosRelBean posEprosRelBean : deleteNumList) {
			posNumList.add(posEprosRelBean.getPosNum());
		}
		List<Long> peopleIds = getPeopleIds(posNumList, deleteNumList.get(0)
				.getRelType(), deleteNumList.get(0).getEpsPosId());
		matchUserImportDao.deleteRelNum(deleteNumList, peopleIds, deleteNumList
				.get(0).getEpsPosId());
	}

	/**
	 * 根据流程岗位名称获取匹配结果
	 * 
	 * @param psoName
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getSearchPosCount(String psoName) throws Exception {
		return matchUserImportDao.getSearchPosCount(psoName);
	}

	/**
	 * @param start
	 *            每页开始行
	 * @param limit
	 *            每页最大行数
	 * @param posName
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<PosEprosRelBean> getStatcionDataBySearch(int start, int limit,
			String posName) throws Exception {
		// 根据搜索添加组织查询语句
		List<Object> listArgs = new ArrayList<Object>();
		StringBuffer strBuff = new StringBuffer();

		String sql1 = "select distinct fl_ps.figureId from PosEprosRelBean a,"
				+ "JecnFlowOrgImage fl_ps where fl_ps.figureId=a.epsPosId and "
				+ " fl_ps.figureType='PositionFigure'";
		strBuff.append(sql1);
		if (!JecnCommon.isNullOrEmtryTrim(posName)) {//
			strBuff.append(" and fl_ps.figureText like ?");
			listArgs.add("%" + posName + "%");
		}
		strBuff.append("order by fl_ps.figureId");
		int size = listArgs.size();
		Object[] args = new Object[size];
		for (int i = 0; i < size; i++) {
			args[i] = listArgs.get(i);
		}
		// 根据分页获取需要查询的岗位记录集合
		List<Long> posIdList = matchUserImportDao.listHql(strBuff.toString(),
				start, limit, listArgs.toArray());
		// 0:岗位匹配表主键ID，1：匹配类型；2：匹配的岗位或基准岗位编号，3:流程岗位ID,4：流程岗位名称
		List<Object[]> epsPosList = null;
		if (posIdList != null && posIdList.size() > 0) {
			epsPosList = matchUserImportDao.getPosRelList(posIdList);
		}
		return getPosEprosRelBeanList(epsPosList);
	}

	/**
	 * 获取结果组装页面需求的结果集
	 * 
	 * @param epsPosList
	 *            0:岗位匹配表主键ID，1：匹配类型；2：匹配的岗位或基准岗位编号，3:流程岗位ID,4：流程岗位名称
	 * @return
	 * @throws Exception
	 */
	public List<PosEprosRelBean> getPosEprosRelBeanList(
			List<Object[]> epsPosList) {
		// 获取结果组装页面需求的结果集
		List<PosEprosRelBean> posEprList = null;
		// 记录流程岗位ID集合
		List<Long> posIdList = new ArrayList<Long>();
		if (epsPosList != null && epsPosList.size() > 0) {
			// 岗位编号集合
			List<String> posNumList = new ArrayList<String>();
			// 基准岗位编号集合
			List<String> basePosNumList = new ArrayList<String>();
			posEprList = new ArrayList<PosEprosRelBean>();
			// 分别获取岗位匹配和基准岗位匹配的数据集
			for (Object[] object : epsPosList) {
				PosEprosRelBean posEprosRelBean = new PosEprosRelBean();
				// 0:岗位匹配表主键ID，1：匹配类型；2：匹配的岗位或基准岗位编号，3:流程岗位ID,4：流程岗位名称
				if (object[0] == null || object[1] == null || object[2] == null
						|| object[3] == null || object[4] == null) {
					continue;
				}
				// 主键ID
				posEprosRelBean
						.setId(Long.valueOf(object[0].toString().trim()));
				// 关联类型
				posEprosRelBean.setRelType(Integer.valueOf(
						object[1].toString().trim()).intValue());
				String posNum = object[2].toString().trim();
				if (posEprosRelBean.getRelType() == 0) {// 岗位匹配
					if (!posNumList.contains(posNum)) {
						posNumList.add(posNum);
					}
				} else if (posEprosRelBean.getRelType() == 1) {// 基准岗位匹配
					if (!basePosNumList.contains(posNum)) {
						basePosNumList.add(posNum);
					}
				}
				posEprosRelBean.setPosNum(posNum);
				posEprosRelBean.setFlowPosName(object[4].toString());
				posEprosRelBean.setEpsPosId(Long.valueOf(object[3].toString()
						.trim()));
				// 记录岗位ID集合
				if (!posIdList.contains(posEprosRelBean.getEpsPosId())) {
					posIdList.add(posEprosRelBean.getEpsPosId());
				}
				posEprList.add(posEprosRelBean);
			}
			if (posEprList != null && posEprList.size() > 0) {
				return getResultList(posNumList, basePosNumList, posEprList,
						posIdList);
			}
		}
		return null;
	}

	/**
	 * 返回页面结果集 流程岗位对应HR为一对多
	 * 
	 * @param posNumList
	 *            HR岗位编号集合
	 * @param basePosNumList
	 *            基准岗位编号集合
	 * @param posEprList
	 *            关联表集合
	 * @param posIdList
	 *            流程岗位ID集合
	 * @return
	 */
	public List<PosEprosRelBean> getResultList(List<String> posNumList,
			List<String> basePosNumList, List<PosEprosRelBean> posEprList,
			List<Long> posIdList) {
		// 分别获取基准岗位和岗位的集合
		List<Object[]> posObjectList = null;
		List<Object[]> basePosObjectList = null;
		try {
			if (posNumList != null && posNumList.size() > 0) {
				posObjectList = matchUserImportDao.getPosNamesByNum(posNumList);
			}
			if (basePosNumList != null && basePosNumList.size() > 0) {
				basePosObjectList = matchUserImportDao
						.getPosNamesByBaseNum(basePosNumList);
			}
		} catch (Exception e) {
			log.error("根据编号获取名称失败！", e);
		}
		// 返回页面结果集 流程岗位对应HR为一对多
		List<PosEprosRelBean> resultList = new ArrayList<PosEprosRelBean>();
		if (posIdList.size() > 0 && posEprList != null) {
			for (Long posId : posIdList) {
				PosEprosRelBean eprosRelBean = new PosEprosRelBean();
				StringBuffer strPosName = new StringBuffer();
				StringBuffer strPosNum = new StringBuffer();
				for (PosEprosRelBean posEprosRelBean : posEprList) {
					if (posEprosRelBean.getEpsPosId().equals(posId)) {
						eprosRelBean.setEpsPosId(posId);
						eprosRelBean.setRelType(posEprosRelBean.getRelType());
						if (eprosRelBean.getRelType() == 0) {// 岗位匹配
							getPosEprosRelBeanData(posObjectList, strPosName,
									strPosNum, posEprosRelBean.getPosNum(), 0);
						} else {// 基准岗位匹配
							getPosEprosRelBeanData(basePosObjectList,
									strPosName, strPosNum, posEprosRelBean
											.getPosNum(), 1);
						}
						if (eprosRelBean.getFlowPosName() == null) {
							eprosRelBean.setFlowPosName(posEprosRelBean
									.getFlowPosName());
						}
					}
				}
				eprosRelBean.setPosNum(strPosNum.toString());
				if (eprosRelBean.getRelType() == 0) {
					eprosRelBean.setHrPosName(strPosName.toString());
				} else {
					eprosRelBean.setBasePosName(strPosName.toString());
				}
				resultList.add(eprosRelBean);
			}
		}
		return resultList;
	}

	/**
	 * 根据岗位匹配记录组织页面获取信息
	 * 
	 * @param posIdList
	 * @param posObjectList
	 * @param basePosObjectList
	 * @param posEprosRelBean
	 * @return
	 */
	public void getPosEprosRelBeanData(List<Object[]> posObjectList,
			StringBuffer strPosName, StringBuffer strPosNum, String posNum,
			int type) {
		String str_br = "<br>";
		if (posObjectList != null) {
			// 0:编号，1：名称
			String hrPosName = null;
			String hrPosNum = null;
			for (Object[] objects : posObjectList) {
				if (objects[0] != null
						&& objects[0].toString().trim().equals(posNum)) {
					if (hrPosName == null) {
						hrPosName = objects[1].toString();
						if (objects[0] != null) {
							hrPosNum = objects[0].toString();
						}
					} else {
						hrPosName = hrPosName + str_br + objects[1].toString();
						if (objects[0] != null) {
							hrPosNum = hrPosNum + "," + objects[0].toString();
						}
					}

				}
			}
			if (strPosNum.length() > 0 && !"".equals(hrPosName)) {// 如果存在
				if (type == 1) {
					strPosName.append(str_br + hrPosName);
				} else {
					strPosName.append(str_br + hrPosName);
				}
				strPosNum.append("," + hrPosNum);

			} else {
				if (type == 1) {
					strPosName.append(hrPosName);
				} else {
					strPosName.append(hrPosName);
				}
				strPosNum.append(hrPosNum);
			}
		}
	}

	/***************************************************************************
	 * 人员错误数据导出
	 * 
	 * @return
	 */
	/**
	 * 
	 * 把BaseBean对象中数据生成excel文件存储到本地
	 * 
	 * @param baseBean
	 * @return
	 */
	@Override
	public boolean saveErrorExcelToLocal(BaseBean baseBean) {
		if (baseBean == null || baseBean.isNull()) {
			return false;
		}
		// 人员错误数据导出模板文件
		this.setOutputExcelModelPath(MatchTool.getOutputErrorExcelModelPath());
		// 人员错误数据导出数据文件
		this.setOutputExcelDataPath(MatchTool.getOutPutExcelErrorDataPath());

		// 从DB中读数据写入到excel再保存到本地
		return editExcel(baseBean.getDeptBeanList(), baseBean
				.getUserPosBeanList(), baseBean.getPosBeanList());
	}

	/**
	 * 人员原始数据行间校验
	 * 
	 * @return boolean false 存在异常数据
	 */

	public boolean checkUserPosColu() {
		// 获取人员+岗位原始数据
		boolean ret = true;
		// 行间校验
		// 1 .获取重复数据
		List<UserMatchBean> listUser = matchUserCheckDao.userSameDataCheck();
		Session s = matchUserCheckDao.getSession();
		if (listUser != null) {
			for (UserMatchBean userBean : listUser) {
				// 有两条登录名称、任职岗位编号都相同数据
				userBean.addError(MatchErrorInfo.TWO_SANME_USER_DATA);
				s.update(userBean);
				ret = false;
			}
		}
		// 2.两个对象，人员编号相同 存在岗位编号不能为空
		List<UserMatchBean> listUserPosNull = matchUserCheckDao
				.loginNameSameAndPosNullCheck();
		if (listUserPosNull != null) {
			for (UserMatchBean userBean : listUserPosNull) {
				// 一人多岗情况，人员对应的岗位编号不能为空
				userBean.addError(MatchErrorInfo.USER_ONE_POS_MUTIL_NULL);
				s.update(userBean);
				ret = false;
			}
		}

		// 3. 一人多岗位:真实姓名、邮箱地址、邮箱内外网表示、电话都应该一样

		// 3.1 一人多岗，真实姓名不同
		List<UserMatchBean> listUserTreeNameNoSame = matchUserCheckDao
				.trueNameNoSameCheck();
		if (listUserTreeNameNoSame != null) {
			for (UserMatchBean userBean : listUserTreeNameNoSame) {
				// 一人多岗情况，人员对应的真实姓名必须相同
				userBean.addError(MatchErrorInfo.USER_TRUE_NAME_NOT_SAME);
				s.update(userBean);
				ret = false;
			}
		}

		// 3.2 一人多岗，邮箱不同
		List<UserMatchBean> listUserEmailNoSame = matchUserCheckDao
				.emailNoSameCheck();
		if (listUserEmailNoSame != null) {
			for (UserMatchBean userBean : listUserEmailNoSame) {
				// 一人多岗情况，人员对应的邮箱、邮箱类型必须相同
				userBean.addError(MatchErrorInfo.USER_EMAIL_NOT_SAME);
				s.update(userBean);
				ret = false;
			}
		}
		// 3.3 一人多岗情况，人员对应的电话必须相同
		List<UserMatchBean> listUserPhoneNoSame = matchUserCheckDao
				.phoneNoSameCheck();
		if (listUserPhoneNoSame != null) {
			for (UserMatchBean userBean : listUserPhoneNoSame) {
				// 一人多岗情况，人员对应的电话必须相同
				userBean.addError(MatchErrorInfo.USER_PHONE_NOT_SAME);
				s.update(userBean);
				ret = false;
			}
		}

		// 4.一人多岗位 岗位编号不能相同
		List<UserMatchBean> listLoginNameAndSamePos = matchUserCheckDao
				.sameLoginNameAndSamePos();
		if (listLoginNameAndSamePos != null) {
			for (UserMatchBean userBean : listLoginNameAndSamePos) {
				if (!JecnCommon.isNullOrEmtryTrim(userBean.getPosNum())) {
					// 出现多条登录名称和岗位编号相同的数据
					userBean.addError(MatchErrorInfo.USER_DATA_NOT_ONLY);
					s.update(userBean);
					ret = false;
				}
			}
		}
		// 5. 岗位所属部门编号在部门集合中不存在
		List<PosMatchBean> posNumRefDeptNumNoExists = matchUserCheckDao
				.posNumRefDeptNumNoExists();
		if (listLoginNameAndSamePos != null) {
			for (PosMatchBean userBean : posNumRefDeptNumNoExists) {
				// 岗位所属部门编号在部门集合中不存在
				userBean
						.addError(MatchErrorInfo.POS_DEPT_NUM_NOT_EXISTS_DEPT_LIST);
				s.update(userBean);
				ret = false;
			}
		}

		// 是否启用该验证
		String flag = configDao.selectValue(5, "allowSamePosName");
		if (!"1".equals(flag)) {
			// 6.同一部门下岗位名称不能重复
			List<PosMatchBean> listPostBean = matchUserCheckDao
					.deptHashSamePosName();
			if (listPostBean != null) {
				for (PosMatchBean userBean : listPostBean) {
					// 同一部门下岗位名称不能重复
					userBean.addError(MatchErrorInfo.POS_NAME_NOT_ONLY);
					s.update(userBean);
					ret = false;
				}
			}
		}
		// 7.岗位编号不唯一
		List<PosMatchBean> posNumNotOnly = matchUserCheckDao
				.deptHashSamePosName();
		if (posNumNotOnly != null) {
			for (PosMatchBean posBean : posNumNotOnly) {
				// 岗位编号不唯一
				posBean.addError(MatchErrorInfo.POS_NUM_NOT_ONLY);
				s.update(posBean);
				ret = false;
			}
		}
		// 8.岗位编号相同，岗位名称必须相同
		List<PosMatchBean> posNameNotSameList = matchUserCheckDao
				.posNameNotSameList();
		if (posNameNotSameList != null) {
			for (PosMatchBean posBean : posNameNotSameList) {
				// 岗位编号不唯一
				posBean.addError(SyncErrorInfo.POS_NAME_NOT_SAME);
				s.update(posBean);
				ret = false;
			}
		}
		// 9.// 岗位编号相同，岗位所属部门必须相同
		List<PosMatchBean> posNumSameAndDeptSameList = matchUserCheckDao
				.posNumSameAndDeptSameList();
		if (posNameNotSameList != null) {
			for (PosMatchBean posBean : posNumSameAndDeptSameList) {
				// 岗位编号相同，岗位所属部门必须相同
				posBean.addError(SyncErrorInfo.POS_NUM_SAME_DEPT_SAME);
				s.update(posBean);
				ret = false;
			}
		}
		return ret;
	}

	/**
	 * 部门外数据行间校验
	 * 
	 * @return
	 */
	public boolean checkDeptColu() {
		boolean ret = true;
		// 部门行间校验，部门编号不唯一
		List<DeptMatchBean> listSameDeptNum = matchUserCheckDao
				.hasSameDeptNum();
		if (listSameDeptNum != null) {
			for (DeptMatchBean deptBean : listSameDeptNum) {
				// 部门编号不唯一
				deptBean.addError(MatchErrorInfo.DEPT_NUM_NOT_ONLY);
				ret = false;
			}
		}// 同一部门下部门名称不能重复
		List<DeptMatchBean> listSameDeptName = matchUserCheckDao
				.hasSameDeptName();
		if (listSameDeptName != null) {
			for (DeptMatchBean deptBean : listSameDeptName) {
				// 同一部门下部门名称不能重复
				deptBean.addError(MatchErrorInfo.DEPT_NAME_NOT_ONLY_BY_PDEPT);
				ret = false;
			}
		}
		return ret;
	}

	public IMatchUserDataCheckDao getMatchUserCheckDao() {
		return matchUserCheckDao;
	}

	public void setMatchUserCheckDao(IMatchUserDataCheckDao matchUserCheckDao) {
		this.matchUserCheckDao = matchUserCheckDao;
	}

	/**
	 * 获取流程岗位ID在匹配关系中的个数
	 * 
	 * @param flowPosId
	 *            流程岗位ID
	 * @return int 流程岗位个数
	 */
	@Override
	public int onlyMatchPost(Long flowPosId) {
		String sql = "select count(*) from JECN_MATCH_POS_EPROS_REL where EPS_POSITION_ID=?";
		return matchUserImportDao.countAllByParamsNativeSql(sql, flowPosId);
	}

	public List<PosMatchBean> getPosList() {
		return posList;
	}

	public void setPosList(List<PosMatchBean> posList) {
		this.posList = posList;
	}

	public List<PosEprosRelBean> getRelList() {
		return relList;
	}

	public void setRelList(List<PosEprosRelBean> relList) {
		this.relList = relList;
	}

	public List<UserMatchBean> getUserList() {
		return userList;
	}

	public void setUserList(List<UserMatchBean> userList) {
		this.userList = userList;
	}

	public CheckDeptData getDeptCheckData() {
		return deptCheckData;
	}

	public void setDeptCheckData(CheckDeptData deptCheckData) {
		this.deptCheckData = deptCheckData;
	}

	public CheckPositionData getPosCheckData() {
		return posCheckData;
	}

	public void setPosCheckData(CheckPositionData posCheckData) {
		this.posCheckData = posCheckData;
	}

	public CheckUserData getUserCheckData() {
		return userCheckData;
	}

	public void setUserCheckData(CheckUserData userCheckData) {
		this.userCheckData = userCheckData;
	}

	public Long getCurProjectId() {
		return curProjectId;
	}

	public void setCurProjectId(Long curProjectId) {
		this.curProjectId = curProjectId;
	}

	public IMatchUserImportDao getMatchUserImportDao() {
		return matchUserImportDao;
	}

	public void setMatchUserImportDao(IMatchUserImportDao matchUserImportDao) {
		this.matchUserImportDao = matchUserImportDao;
	}

	public void setConfigDao(IJecnConfigItemDao configDao) {
		this.configDao = configDao;
	}

}
