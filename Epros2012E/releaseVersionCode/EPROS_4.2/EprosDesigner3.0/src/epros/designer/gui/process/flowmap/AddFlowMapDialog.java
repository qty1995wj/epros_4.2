package epros.designer.gui.process.flowmap;

import javax.swing.Icon;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.process.ProcessMapFigureData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.JecnProcessCommon;
import epros.designer.service.process.JecnProcess;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.tree.JecnTreeRenderer;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUserCheckUtil;

/*******************************************************************************
 * 
 * 新建流程地图
 * 
 * @author 2012-06-06
 * 
 */
public class AddFlowMapDialog extends FlowMapShowDialog {
	private static Logger log = Logger.getLogger(AddFlowMapDialog.class);

	public AddFlowMapDialog(JecnTreeNode pNode, JecnTree jTree) {
		super(pNode, jTree, true);
		this.setTitle(JecnProperties.getValue("newFlowMap"));
		this.initLayout();
	}

	public void okButPerformed() {
		// 流程地图名称
		String flowName = flowMapField.getText().toString().trim();
		// 判断流程名称填写是否正确
		if (DrawCommon.isNullOrEmtryTrim(flowName)) {
			// 名称不能为空
			verfyLab.setText(JecnProperties.getValue("processMapName") + JecnProperties.getValue("isNotEmpty"));
			return;
		} else if (!JecnUserCheckUtil.checkNameFileSpecialChar(flowName)) {
			verfyLab.setText(JecnProperties.getValue("processMapName") + JecnProperties.getValue("notInputThis"));
			return;
		} else if (JecnUserCheckUtil.getTextLength(flowName) > 122) {
			// 不能超过122个字符或61个汉字
			verfyLab.setText(JecnProperties.getValue("processMapName") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 流程编号
		if (this.mapNumField != null && DrawCommon.checkNameMaxLength(this.mapNumField.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("flowNum") + JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 目的
		if (DrawCommon.checkNoteLength(mapPurposeField.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("purpose") + JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 范围
		if (DrawCommon.checkNoteLength(mapRangeField.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("flowScope") + JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 名称定义
		if (DrawCommon.checkNoteLength(mapNameDefinedField.getText().toString())) {
			verfyLab.setText(mapNameDefinedLab.getText().substring(0, mapNameDefinedLab.getText().length() - 1)
					+ JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 输入
		if (DrawCommon.checkNoteLength(mapInputField.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("actIn") + JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 输出
		if (DrawCommon.checkNoteLength(mapOutputField.getText().toString())) {
			verfyLab.setText(JecnProperties.getValue("actOut") + JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 步骤说明
		if (DrawCommon.checkNoteLength(mapStepDescField.getText().toString())) {
			verfyLab.setText(mapStepDescLab.getText().substring(0, mapStepDescLab.getText().length() - 1)
					+ JecnProperties.getValue("lengthNotOut"));
			return;
		}
		try {
			if(validateNamefullPath(flowName,0)){
				return;
			}
			if (ConnectionPool.getProcessAction().validateAddName(flowName,
					this.getSelectNode().getJecnTreeBean().getId(), 0, JecnConstants.projectId)) {
				verfyLab.setText(JecnProperties.getValue("nameHaved"));
				return;
			}
		} catch (Exception e1) {
			log.error("AddFlowMapDialog okButPerformed is error", e1);
		}

		// 流程地图基本信息表
		JecnMainFlowT flowMapBean = new JecnMainFlowT();
		flowMapBean.setFlowAim(mapPurposeField.getText().trim());
		flowMapBean.setFlowArea(mapRangeField.getText().trim());
		flowMapBean.setFlowInput(mapInputField.getText().trim());
		flowMapBean.setFlowOutput(mapOutputField.getText().trim());
		flowMapBean.setFlowShowStep(mapStepDescField.getText().trim());
		flowMapBean.setFlowNounDefine(mapNameDefinedField.getText().trim());
		flowMapBean.setFileId(this.getFileEnclosureId());
		try {
			Long isPublic = 0L;
			// 获取父节点的密级，部门权限，岗位权限
			LookPopedomBean lookPopedomBean = ConnectionPool.getOrganizationAction().getAccessPermissions(
					this.getSelectNode().getJecnTreeBean().getId(), 0);
			//新建流程架构时候如果 全局权限设置公开则新建流架构也是公开
			if(JecnConfigTool.isShowItem(ConfigItemPartMapMark.pubTotalMap)){
				isPublic = 1L;
			}else{
				isPublic =Long.valueOf(lookPopedomBean.getIsPublic());
			}
			// 岗位权限
			String posIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosList());
			// 组织权限
			String orgIds = JecnDesignerCommon.getLook(lookPopedomBean.getOrgList());
			// 岗位组权限
			String posGroupIds = JecnDesignerCommon.getLook(lookPopedomBean.getPosGroupList());

			JecnFlowStructureT jecnFlowStructureT = JecnProcessCommon.createProcessMap(flowName, isPublic, this
					.getSelectNode(), mapNumField.getText(), flowMapBean, posIds, orgIds, this.getjTree(), posGroupIds);
			if (this.getModeId() != null) {
				initWorkFlowByModeId(this.getModeId(), jecnFlowStructureT);
			} else {
				// 添加到画图面板
				JecnFlowMapData totalMapData = JecnDesignerCommon.createFlowMapData(MapType.totalMap,
						jecnFlowStructureT.getFlowId());
				totalMapData.setName(flowName);
				totalMapData.setFlowId(jecnFlowStructureT.getFlowId());
				JecnDrawMainPanel.getMainPanel().addWolkflow(totalMapData);
				JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
				drawDesktopPane.getTabbedTitlePanel().setTabIcon(
						(Icon) JecnTreeRenderer.class.getField(TreeNodeType.processMap.toString() + "NoPub").get(null));
			}
			this.dispose();
		} catch (Exception e) {
			log.error("AddFlowMapDialog okButPerformed is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}
		this.setVisible(false);
	}

	
	/**
	 * 提示 文件库中是否有重名文件
	 * @param names
	 * @return
	 */
	private boolean validateNamefullPath(String flowName,int type){
		try {
			String ns = ConnectionPool.getProcessAction().validateNamefullPath(flowName,this.getSelectNode().getJecnTreeBean().getId(), type, JecnConstants.projectId);
			if(!"".equals(ns)){
				String tipText = ns+" "+JecnProperties.getValue("itExistsDirectories");
				// 是否删除提示框
				int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData
						.getFrame(), tipText, null,
						JecnOptionPane.YES_NO_OPTION,
						JecnOptionPane.INFORMATION_MESSAGE);
				if (option == JecnOptionPane.NO_OPTION) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private void initWorkFlowByModeId(Long flowMapModeId, JecnFlowStructureT jecnFlowStructureT) throws Exception {
		ProcessOpenMapData processMapData = ConnectionPool.getProcessModeAction().getProcessMapModeData(flowMapModeId);
		processMapData.setJecnFlowStructureT(jecnFlowStructureT);
		for (ProcessMapFigureData mapFigureData : processMapData.getMapFigureDataList()) {
			JecnFlowStructureImageT jecnFlowStructureImageT = mapFigureData.getJecnFlowStructureImageT();
			if (jecnFlowStructureImageT.getLinkFlowId() != null) {
				jecnFlowStructureImageT.setLinkFlowId(null);
			}

			jecnFlowStructureImageT.setFlowId(jecnFlowStructureT.getFlowId());
		}
		JecnProcess.openJecnProcessTotalMap(processMapData, true);

		// 重新设置tabpanel图标
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		drawDesktopPane.getTabbedTitlePanel().setTabIcon(
				(Icon) JecnTreeRenderer.class.getField(TreeNodeType.processMap.toString() + "NoPub").get(null));
	}

	public boolean isUpdate() {
		// 流程地图名称
		if (flowMapField.getText() != null && !"".equals(flowMapField.getText())) {
			return true;
		}
		// 编号
		if (mapNumField.getText() != null && !"".equals(mapNumField.getText())) {
			return true;
		}
		// 目的
		if (mapPurposeField.getText() != null && !"".equals(mapPurposeField.getText())) {
			return true;
		}
		// 范围
		if (mapRangeField.getText() != null && !"".equals(mapRangeField.getText())) {
			return true;
		}
		// 术语定义
		if (mapNameDefinedField.getText() != null && !"".equals(mapNameDefinedField.getText())) {
			return true;
		}
		// 输入
		if (mapInputField.getText() != null && !"".equals(mapInputField.getText())) {
			return true;
		}
		// 输出
		if (mapOutputField.getText() != null && !"".equals(mapOutputField.getText())) {
			return true;
		}
		// 步骤说明
		if (mapStepDescField.getText() != null && !"".equals(mapStepDescField.getText())) {
			return true;
		}
		// 附件
		if (fileEnclosureSelectCommon != null && fileEnclosureSelectCommon.getResultFileId() != null) {
			return true;
		}
		return false;
	}
}
