package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import java.util.List;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.PosBean;

/**
 * 
 * 岗位行内检验、行间校验
 * 
 * @author Administrator
 * 
 */
public class CheckPosRow {
	/**
	 * 
	 * 岗位行内检验
	 * 
	 */
	public boolean checkPosRow(BaseBean baseBean) {
		if (baseBean == null || baseBean.isNull()) {
			return false;
		}
		boolean ret = true;

		// 获取岗位数据
		List<PosBean> posBeanList = baseBean.getPosBeanList();
		if (posBeanList == null || posBeanList.size() == 0 || baseBean.getDeptBeanList().size() == 0) {
			return ret;
		}
		for (int i = 0; i < posBeanList.size(); i++) {
			// 获取当前岗位记录
			PosBean posBean = posBeanList.get(i);

			// 岗位编号不为空
			if (SyncTool.isNullOrEmtryTrim(posBean.getPosNum())) {
				posBean.addError(SyncErrorInfo.POS_NUM_NULL);
				ret = false;
			} else {
				// 去空
				posBean.setPosNum(posBean.getPosNum().trim());
			}

			// 岗位名称
			if (SyncTool.isNullOrEmtryTrim(posBean.getPosName())) {
				posBean.addError(SyncErrorInfo.POS_NAME_NULL);
				ret = false;
			} else {
				// 去空
				posBean.setPosName(posBean.getPosName().trim());
			}

			// 岗位所属部门编号去前缀后缀空后不为空
			if (SyncTool.isNullOrEmtryTrim(posBean.getDeptNum())) {

				posBean.addError(SyncErrorInfo.POS_DEPT_NUM_NULL);
				ret = false;

			} else {

				// 岗位所属部门编号在部门集合中不存在
				boolean existsPosDept = baseBean.existsPosByDept(posBean);
				if (!existsPosDept) {
					posBean.addError(SyncErrorInfo.POS_DEPT_NUM_NOT_EXISTS_DEPT_LIST);
					ret = false;
				} else {
					// 去空
					posBean.setDeptNum(posBean.getDeptNum().trim());
				}

			}

		}

		// 行内检验失败，返回
		if (!ret) {
			return ret;
		}
		// ---------------行间校验
		// 岗位编号唯一
		// ret = checkPosRows(posBeanList);
		if (!ret) {
			return ret;
		}
		// ---------------行间校验

		return ret;
	}
}
