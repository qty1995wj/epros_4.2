package com.jecn.epros.domain.risk;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.util.ConfigUtils;

public class RiskRelatedTargetDescription {
    /**
     * 控制目标
     */
    private String description;
    /**
     * 控制编号
     */
    private String controlCode;
    /**
     * 控制活动简描述
     */
    private String activeShow;
    /**
     * 活动ID
     */
    private Long figureId;
    /**
     * 活动名称
     */
    private String figureText;
    /**
     * 流程ID
     */
    private Long flowId;
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 部门ID
     */
    private Long orgId;
    /**
     * 责任部门
     */
    private String orgName;
    /**
     * 控制方法 ：人工、自动
     */
    private int method;
    /**
     * 控制类型 ： 预防型、发现型
     */
    private String type;
    /**
     * 是否为关键控制
     */
    private String keyPoint;
    /**
     * 控制频率
     */
    private String frequency;

    public String getKeyPointStr(){
        return ConfigUtils.getRiskKeyPoint(Integer.valueOf(keyPoint));
    }

    public String getFrequencyStr(){
        return ConfigUtils.getRiskFrequency(Integer.valueOf(frequency));
    }

    public String getMethodStr(){
        return ConfigUtils.getRiskMethod(method);
    }

    public String getTypeStr(){
        return ConfigUtils.getRiskType(Integer.valueOf(type));
    }

    /**
     * 相关流程
     *
     * @return
     */
    public LinkResource getProcessLink() {
        return new LinkResource(flowId, flowName, ResourceType.PROCESS);
    }

    /**
     * 责任部门
     *
     * @return
     */
    public LinkResource getOrgLink() {
        return new LinkResource(orgId, orgName, ResourceType.ORGANIZATION);
    }

    /**
     * 相关流程对应活动(控制点)
     *
     * @return
     */
    public LinkResource getActiveLink() {
        return new LinkResource(figureId, figureText, ResourceType.ACTIVITY);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getControlCode() {
        return controlCode;
    }

    public void setControlCode(String controlCode) {
        this.controlCode = controlCode;
    }

    public String getActiveShow() {
        return activeShow;
    }

    public void setActiveShow(String activeShow) {
        this.activeShow = activeShow;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    public void setFigureText(String figureText) {
        this.figureText = figureText;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKeyPoint() {
        return keyPoint;
    }

    public void setKeyPoint(String keyPoint) {
        this.keyPoint = keyPoint;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }
}
