package epros.designer.util;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 任务 版本号 工具类
 * 
 * @author Administrator
 * 
 */
public class JecnVersionUtil {

	/**
	 * 大版本，小版本 复选框
	 */
	private static JecnConfigCheckBox largeVersionCheckBox = null;

	private static JecnConfigCheckBox smallVersionCheckBox = null;

	// 自动版本号
	public static void initVersion(JTextField versonNumField, JecnTreeBean treeBean, int type) {
		if (JecnConfigTool.autoVersionNumber()) {
			versonNumField.setEditable(false);

			// 自动版本号 初始化版本号
			JecnConfigItemBean initVersionNumber = JecnConfigTool.getItemBean(ConfigItemPartMapMark.initVersionNumber);

			if (smallVersionCheckBox != null && smallVersionCheckBox.isSelected()) {
				String value = initVersionNumber.getValue();
				if (StringUtils.isNotBlank(value)) {
					Double valueOf = Double.valueOf(value);
					initVersionNumber.setValue(valueOf.toString());
				}
			}

			// 自动版本号 版本号前缀
			JecnConfigItemBean versionNumberId = JecnConfigTool.getItemBean(ConfigItemPartMapMark.versionNumberId);

			String autoNum = versionNumberId.getValue() + initVersionNumber.getValue();

			// 获取上一次发布的 版本号 判断格式是否相同
			List<String> versListData = ConnectionPool.getDocControlAction().findVersionListByFlowIdNoPage(
					treeBean.getId(), type);

			String verNum = "";
			String versionId = autoNum;
			if (versListData != null && !versListData.isEmpty()) {
				verNum = versListData.get(0);
				// 只有版本标识一致的时候才会 迭代版本号
				String num = StringUtils.substringAfter(verNum.toUpperCase(), versionNumberId.getValue());
				if (StringUtils.isNotBlank(num)) {
					versionId = getVersionNum(verNum, versionNumberId, initVersionNumber, versListData).toString();
				}
			}

			versonNumField.setText(versionId);
		}
	}

	/**
	 * 获得计算后的 版本号 不验证 版本号 是否重复(如果重复 说是客户问题)
	 * 
	 * @param verNum
	 * @param versionNumberId
	 * @param initVersionNumber
	 * @return
	 */
	private static StringBuilder getVersionNum(String verNum, JecnConfigItemBean versionNumberId,
			JecnConfigItemBean initVersionNumber, List<String> versListData) {

		// 自动生成的版本号
		StringBuilder versionNum = new StringBuilder();
		if (StringUtils.isNotBlank(verNum)) {
			// 获取 历史版本号中的 数字部分 进行 递增
			String num = StringUtils.substringAfter(verNum.toUpperCase(), versionNumberId.getValue());
			if (JecnUserCheckUtil.checkNumberFormat(num)) {
				if (StringUtils.isNotBlank(num)) {
					BigDecimal add = null;
					BigDecimal a = new BigDecimal(num);
					if (smallVersionCheckBox != null && smallVersionCheckBox.isSelected()) {
						// 如果是小版本 变更 值更改 小数后面的 递增 。
						String[] split = num.split("\\.");
						if (split.length >= 2) {
							String string = split[1];
							BigDecimal doubleA = new BigDecimal(string).add(new BigDecimal("1"));
							num = split[0] + "." + doubleA.toString();
							add = new BigDecimal(num);
						} else {
							// 如果初始化是整数 默认从 *.1 开始 初始化版本
							a = new BigDecimal(num);
							BigDecimal b = new BigDecimal("0.1");
							add = a.add(b);
						}
					} else {
						// 大版本 每次 递增1
						BigDecimal b = new BigDecimal("1");
						add = new BigDecimal(a.intValue()).add(b);
					}
					initVersionNumber.setValue(add.toString());
				}
			}
		}

		versionNum.append(versionNumberId.getValue());
		versionNum.append(initVersionNumber.getValue());

		/*
		 * if (versListData.contains(versionNum.toString())) { // 如果 版本号中有重复 则
		 * // 递归查找， 查找到不重复 为止。 return getVersionNum(versionNum.toString(),
		 * versionNumberId, initVersionNumber, versListData); }
		 */

		return versionNum;

	}

	public static JecnConfigCheckBox getLargeVersionCheckBox() {
		return largeVersionCheckBox;
	}

	public static void setLargeVersionCheckBox(JecnConfigCheckBox largeVersionCheckBox) {
		JecnVersionUtil.largeVersionCheckBox = largeVersionCheckBox;
	}

	public static JecnConfigCheckBox getSmallVersionCheckBox() {
		return smallVersionCheckBox;
	}

	public static void setSmallVersionCheckBox(JecnConfigCheckBox smallVersionCheckBox) {
		JecnVersionUtil.smallVersionCheckBox = smallVersionCheckBox;
	}

	public static JPanel getVersionTextPanel(JTextField versonNumField) {
		JecnPanel versonPanel = new JecnPanel();
		largeVersionCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("largeVersion"));
		smallVersionCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("smallVersion"));
		Insets insets1 = new Insets(0, 0, 0, 0);
		versonPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, insets1, 0, 0);
		versonPanel.add(versonNumField, c);
		if (JecnConfigTool.autoVersionNumber() && JecnConfigTool.isShowItem(ConfigItemPartMapMark.enableSmallVersion)) {
			// 是否显示大小版本复选框
			ButtonGroup group = new ButtonGroup();
			group.add(largeVersionCheckBox);
			group.add(smallVersionCheckBox);
			c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets1, 0, 0);
			versonPanel.add(largeVersionCheckBox, c);
			c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets1, 0, 0);
			versonPanel.add(smallVersionCheckBox, c);
		}
		return versonPanel;
	}

}
