package com.jecn.epros.server.webBean.standard;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * zhangxiaohu
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-13 时间：下午05:00:45
 */
public class StandardInfoListBean {
	/** 级别 */
	private int level;

	private Long standardId;

	private Long preStandId;

	private String standName;
	/** 标准类型 标准类型(0是目录，1文件标准，2流程标准 3流程地图标准4标准条款5条款要求) */
	private int standType;
	/** 条款对应要求 */
	private String standContent;
	/** 标准清单相关文件信息 **/
	private List<StandardRelatePRFBean> listStandardRefBean = new ArrayList<StandardRelatePRFBean>();
	/** 记录当前标准内添加的活动ID */
	private Set<Long> activeSet = new HashSet<Long>();
	/** 记录当前标准添加的流程ID */
	private Set<Long> flowSet = new HashSet<Long>();
	/** 记录当前标准添加的制度ID */
	private Set<Long> ruleSet = new HashSet<Long>();

	public Set<Long> getActiveSet() {
		return activeSet;
	}

	public Set<Long> getFlowSet() {
		return flowSet;
	}

	public Set<Long> getRuleSet() {
		return ruleSet;
	}

	public List<StandardRelatePRFBean> getListStandardRefBean() {
		return listStandardRefBean;
	}

	public void setListStandardRefBean(
			List<StandardRelatePRFBean> listStandardRefBean) {
		this.listStandardRefBean = listStandardRefBean;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Long getStandardId() {
		return standardId;
	}

	public void setStandardId(Long standardId) {
		this.standardId = standardId;
	}

	public Long getPreStandId() {
		return preStandId;
	}

	public void setPreStandId(Long preStandId) {
		this.preStandId = preStandId;
	}

	public String getStandName() {
		return standName;
	}

	public void setStandName(String standName) {
		this.standName = standName;
	}

	public int getStandType() {
		return standType;
	}

	public void setStandType(int standType) {
		this.standType = standType;
	}

	public String getStandContent() {
		return standContent;
	}

	public void setStandContent(String standContent) {
		this.standContent = standContent;
	}

	/**
	 * 条款要求 连接
	 * 
	 * @return
	 */
	public String getHrefStandContent() {
		return "<a href='standard.action?standardType=" + standType
				+ "&standardId=" + this.standardId
				+ "' target='_blank' style='cursor: pointer;' class='table'>"
				+ this.standContent + "</a>";
	}

	/**
	 * 条款要求 连接
	 * 
	 * @return
	 */
	public String getHrefStandName() {
		return "<a href='standard.action?standardType=" + standType
				+ "&standardId=" + this.standardId
				+ "' target='_blank' style='cursor: pointer;' class='table'>"
				+ this.standName + "</a>";
	}
}
