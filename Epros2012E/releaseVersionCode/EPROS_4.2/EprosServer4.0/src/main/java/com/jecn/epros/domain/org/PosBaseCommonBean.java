package com.jecn.epros.domain.org;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;

public class PosBaseCommonBean {
    private Long figureId;// 岗位ID
    private String figureName;// 岗位名称
    private String positionImportance;// 技能
    private String positionSkill;// 知识
    private String positionDiathesisModel;// 素质模型
    private String schoolExperience;// 核心价值
    private String knowledge;// 学历
    private String positionsNature;// 岗位类别
    private String fileName;// 岗位说明书文件名（不存入数据库）
    private Long fileId;// 岗位说明书


    public LinkResource getFileLink() {
        return new LinkResource(fileId, fileName, ResourceType.FILE, TreeNodeUtils.NodeType.position);
    }

    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    public String getFigureName() {
        return figureName;
    }

    public void setFigureName(String figureName) {
        this.figureName = figureName;
    }

    public String getPositionImportance() {
        return positionImportance;
    }

    public void setPositionImportance(String positionImportance) {
        this.positionImportance = positionImportance;
    }

    public String getPositionSkill() {
        return positionSkill;
    }

    public void setPositionSkill(String positionSkill) {
        this.positionSkill = positionSkill;
    }

    public String getPositionDiathesisModel() {
        return positionDiathesisModel;
    }

    public void setPositionDiathesisModel(String positionDiathesisModel) {
        this.positionDiathesisModel = positionDiathesisModel;
    }

    public String getSchoolExperience() {
        return schoolExperience;
    }

    public void setSchoolExperience(String schoolExperience) {
        this.schoolExperience = schoolExperience;
    }

    public String getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(String knowledge) {
        this.knowledge = knowledge;
    }

    public String getPositionsNature() {
        return positionsNature;
    }

    public void setPositionsNature(String positionsNature) {
        this.positionsNature = positionsNature;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }
}
