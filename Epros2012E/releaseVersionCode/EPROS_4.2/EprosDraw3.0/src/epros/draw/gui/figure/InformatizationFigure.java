package epros.draw.gui.figure;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;

import epros.draw.util.DrawCommon;

/**
 * 信息化显示
 * 
 * @author admin
 * 
 */
public class InformatizationFigure extends JPanel {
	private String text;

	private Font font = new Font("宋体", 1, 12);

	public InformatizationFigure() {
	}

	public void initSize(String text) {
		if (StringUtils.isEmpty(text)) {
			return;
		}
		this.text = text;
		int strWidth = getFontStringWidth(text);
		this.setSize(strWidth + 5, 16);
	}

	private int getFontStringWidth(String text) {
		FontMetrics fm = this.getFontMetrics(this.getFont());
		return fm.stringWidth(text);
	}

	public void paintComponent(Graphics g) {
		if (DrawCommon.isNullOrEmtryTrim(text)) {
			return;
		}
		Graphics2D g2d = (Graphics2D) g;
		// g2d.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);

		g2d.setPaint(Color.BLACK);
		g2d.fillRect(0, 0, this.getWidth() - 1, this.getHeight());
		g2d.setColor(Color.WHITE);
		g2d.setFont(font);
		g2d.drawString(text, 2, 14);
	}

	public Font getFont() {
		return font;
	}
}
