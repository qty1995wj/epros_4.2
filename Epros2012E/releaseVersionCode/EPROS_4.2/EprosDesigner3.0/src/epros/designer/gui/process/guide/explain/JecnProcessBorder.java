package epros.designer.gui.process.guide.explain;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;

import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.operationConfig.JecnTextArea;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.DrawCommon;

public class JecnProcessBorder extends JecnFileDescriptionComponent {

	/** 起始活动Lab */
	protected JLabel startActiveLab = new JLabel();
	/** 起始活动Field */
	protected JecnTextArea startActiveField = new JecnTextArea();
	/** 终止活动Lab */
	protected JLabel endActiveLab = new JLabel();
	/** 终止活动Field */
	protected JecnTextArea endActiveField = new JecnTextArea();

	/** 数据 */
	protected JecnFlowBasicInfoT flowBasicInfoT;

	public JecnProcessBorder(int index, String name, JecnPanel contentPanel, boolean isRequest,
			JecnFlowBasicInfoT flowBasicInfoT, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowBasicInfoT = flowBasicInfoT;
		initCom();
		initLayout();
		initData();
	}

	private void initData() {
		// 流程边界--起始活动
		if (!DrawCommon.isNullOrEmtryTrim(flowBasicInfoT.getFlowStartpoint())) {
			startActiveField.setText(flowBasicInfoT.getFlowStartpoint());
		}
		// 流程边界--终止活动
		if (!DrawCommon.isNullOrEmtryTrim(flowBasicInfoT.getFlowEndpoint())) {
			endActiveField.setText(flowBasicInfoT.getFlowEndpoint());
		}
	}

	private void initLayout() {
		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBorder(BorderFactory.createTitledBorder(""));
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, insets, 0, 0);
		// 起始活动Lab
		startActiveLab.setText(JecnProperties.getValue("startActiveC"));

		centerPanel.add(startActiveLab, c);
		// 起始活动Field
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(startActiveField, c);

		// 终止活动Lab
		endActiveLab.setText(JecnProperties.getValue("endActiveC"));
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		centerPanel.add(endActiveLab, c);

		// 终止活动Filed
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(endActiveField, c);
	}

	private void initCom() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isUpdate() {
		if (DrawCommon.checkStringSame(getFlowStart(), flowBasicInfoT.getFlowStartpoint())
				&& DrawCommon.checkStringSame(getFlowEnd(), flowBasicInfoT.getFlowEndpoint())) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		// 流程边界：起始活动
		if (JecnValidateCommon.validateFieldNotPass(startActiveField.getText(), verfyLab, JecnProperties
				.getValue("startActiveC"))) {
			return true;
		}
		// 流程边界：终止活动
		if (JecnValidateCommon.validateFieldNotPass(endActiveField.getText(), verfyLab, JecnProperties
				.getValue("endActiveC"))) {
			return true;
		}
		return false;
	}
	
	public String getFlowStart(){
		return startActiveField.getText().trim();
	}
	public String getFlowEnd(){
		return endActiveField.getText().trim();
	}

}
