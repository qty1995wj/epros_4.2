package epros.designer.gui.system.config.ui.comp;

import javax.swing.JRadioButton;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

/**
 * 
 * 单选框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigRadioButton extends JRadioButton {
	/** 数据对象 */
	private JecnConfigItemBean itemBean = null;

	public JecnConfigRadioButton(String text) {
		super(text);
	}

	public JecnConfigItemBean getItemBean() {
		return itemBean;
	}

	public void setItemBean(JecnConfigItemBean itemBean) {
		this.itemBean = itemBean;
	}

}
