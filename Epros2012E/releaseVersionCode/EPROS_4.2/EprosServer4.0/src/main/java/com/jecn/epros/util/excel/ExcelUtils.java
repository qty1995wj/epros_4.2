package com.jecn.epros.util.excel;

import com.jecn.epros.domain.InventoryCell;
import com.jecn.epros.domain.InventoryCellStyle;
import com.jecn.epros.domain.InventoryTable;
import com.jecn.epros.domain.InventoryTable.Status;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.file.FileDirCommonBean;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.*;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelUtils {

    private static final Log logger = LogFactory.getLog(ExcelUtils.class);

    public static final int UNIT_WIDTH = 22;


    /**
     * 所有的清单的转换为byte数组工具类
     */
    public static byte[] inventoryToByteArray(Inventory inventory) {
        return inventoryToByteArray(inventory, JecnProperties.getValue("listD"));//清单
    }

    /**
     * 所有的清单的转换为byte数组工具类
     */
    public static byte[] inventoryToByteArray(Inventory inventory, String sheetName) {
        ByteArrayOutputStream out = null;
        byte[] bytes;
        try {
            out = new ByteArrayOutputStream();
            HSSFWorkbook workBook = createInvenrotyWorkBook(inventory, sheetName);
            workBook.write(out);
        } catch (Exception e) {
            logger.error("生成清单异常", e);
        } finally {
            bytes = out.toByteArray();
            IOUtils.closeQuietly(out);
        }
        return bytes;
    }

    private static HSSFWorkbook createInvenrotyWorkBook(Inventory inventory, String sheetName) throws Exception {
        // 声明一个工作薄
        HSSFWorkbook workbook = new HSSFWorkbook();

        try {

            // 生成一个表格
            HSSFSheet sheet = workbook.createSheet(sheetName);
            int rowNum = 0;
            // 生成总计行
            if (inventory.getTotalNum() != null) {
                initInventoryNumberRow(rowNum, inventory, sheet, workbook);
                rowNum++;
            }
            Map<String, List<InventoryCellStyle>> styleMap = inventory.getStyleMap();
            List<InventoryCellStyle> titleCellStyles = styleMap.get(InventoryStyleManager.TITLE);
            List<InventoryCellStyle> contentCellStyles = styleMap.get(InventoryStyleManager.CONTENT);

            // 生成标题行
            initInventoryTitleRow(rowNum, inventory.getData(), sheet, workbook, titleCellStyles);
            rowNum++;
            // 生成内容行
            initInventoryContentRow(rowNum, inventory.getData(), sheet, workbook, contentCellStyles);

        } catch (Exception e) {
            throw e;
        } finally {
            workbook.close();
        }
        return workbook;
    }

    private static void initInventoryContentRow(int rowNum, InventoryTable data, HSSFSheet sheet, HSSFWorkbook workbook,
                                                List<InventoryCellStyle> contentCellStyles) throws Exception {
        Map<String, HSSFCellStyle> cellStyleMap = genericCellStyleMap(workbook);
        List<List<InventoryCell>> contents = data.getContents();
        HSSFCellStyle indexCellStyle = SheetCellStyle.getInventoryIndexCellStyle(workbook);
        for (int i = 1; i < contents.size(); i++) {
            HSSFRow hssfRow = sheet.createRow(rowNum);
            List<InventoryCell> cells = contents.get(i);
            // 创建索引列
            HSSFCell cell = hssfRow.createCell(0);
            cell.setCellValue(new HSSFRichTextString(i + ""));
            cell.setCellStyle(indexCellStyle);
            int contentStyleSize = contentCellStyles.size();
            if (cells.size() > contentStyleSize) {
                logger.error("Excel导出数据cells的size大于contentStyle的size cells size:" + cells.size() + " contentStyle size:" + contentCellStyles.size());
            }
            // 生成每一行的数据列
            for (int dataCol = 0, colIndex = 1; dataCol < cells.size(); dataCol++, colIndex++) {
                InventoryCell dataCell = cells.get(dataCol);
                cell = hssfRow.createCell(colIndex);
                cell.setCellValue(new HSSFRichTextString(getCellText(dataCell)));
                InventoryTable.Align align = InventoryTable.Align.left;
                if (contentStyleSize > dataCol) {
                    align = contentCellStyles.get(dataCol).getAlign();
                }
                cell.setCellStyle(cellStyleMap.get(concatCondition(dataCell.getStatus(), align)));
            }
            rowNum++;
        }
    }

    private static void initInventoryTitleRow(int rowNum, InventoryTable inventoryTable, HSSFSheet sheet,
                                              HSSFWorkbook workbook, List<InventoryCellStyle> titleCellStyles) throws Exception {

        List<InventoryCell> titileContents = inventoryTable.getTitileContents();
        if (titileContents == null || titileContents.size() == 0) {
            throw new Exception("写出清单excel，但是表头为空");
        }
        // index列的宽度
        sheet.setColumnWidth(0, 5 * 256);
        HSSFRow hssfRow = sheet.createRow(rowNum);
        // 创建索引列
        HSSFCell cell = hssfRow.createCell(0);
        cell.setCellValue(new HSSFRichTextString(JecnProperties.getValue("serialNumber")));//序号
        cell.setCellStyle(SheetCellStyle.getInventoryTitleCellStyle(workbook));
        for (int i = 0; i < titileContents.size(); i++) {
            // 标题列的宽度
            int width = titleCellStyles.get(i).getType() * UNIT_WIDTH * 256;
            sheet.setColumnWidth(i + 1, width);
            InventoryCell dataCell = titileContents.get(i);
            cell = hssfRow.createCell(i + 1);
            cell.setCellValue(new HSSFRichTextString(getCellText(dataCell)));
            cell.setCellStyle(createTitleCellStyle(workbook));
        }
    }

    private static void initInventoryNumberRow(int rowNum, Inventory inventory, HSSFSheet sheet,
                                               HSSFWorkbook workbook) {
        // 产生表格标题行
        HSSFRow row = sheet.createRow(rowNum);
        int col = 0;
        if (inventory.getOrgNum() != null) {
            col++;//部门总计
            createAndAddNumCell(JecnProperties.getValue("departmentTotal") + "(" + inventory.getOrgNum() + ")", workbook, row, col, SheetCellStyle.DEFAULT_COLOR);
        }
        if (inventory.getTotalNum() != null) {
            col++;//总计
            createAndAddNumCell(JecnProperties.getValue("total") + "(" + inventory.getTotalNum() + ")", workbook, row, col, SheetCellStyle.DEFAULT_COLOR);
        }
        if (inventory.getProcessNum() != null) {
            col++;//流程总计
            createAndAddNumCell(JecnProperties.getValue("processTotal") + "(" + inventory.getProcessNum() + ")", workbook, row, col, SheetCellStyle.DEFAULT_COLOR);
        }
        if (inventory.getMapNum() != null) {
            col++;//流程架构总计
            createAndAddNumCell(JecnProperties.getValue("processMapTotal") + "(" + inventory.getMapNum() + ")", workbook, row, col, SheetCellStyle.DEFAULT_COLOR);
        }
        if (inventory.getNotPubNum() != null) {
            col++;//待建
            createAndAddNumCell(JecnProperties.getValue("toBeBuilt") + "(" + inventory.getNotPubNum() + ")", workbook, row, col, SheetCellStyle.NOT_PUB_COLOR);
        }
        if (inventory.getApproveNum() != null) {
            col++;
            createAndAddNumCell(JecnProperties.getValue("toBeExaminedAndApproved") + "(" + inventory.getApproveNum() + ")", workbook, row, col, SheetCellStyle.APPROVE_COLOR);
        }
        if (inventory.getPubNum() != null) {
            col++;
            createAndAddNumCell(JecnProperties.getValue("release") + "(" + inventory.getPubNum() + ")", workbook, row, col, SheetCellStyle.PUB_COLOR);
        }

    }

    private static void createAndAddNumCell(String text, HSSFWorkbook workbook, HSSFRow row, int col, short color) {
        HSSFCellStyle numStyle = SheetCellStyle.getInventoryNumCellStyle(workbook, color);
        createAndAddCell(text, row, col, numStyle);
    }

    private static HSSFCellStyle createTitleCellStyle(HSSFWorkbook workbook) {
        return SheetCellStyle.getInventoryTitleCellStyle(workbook);
    }


    private static void createAndAddCell(String text, HSSFRow row, int col, HSSFCellStyle cellStyle) {
        HSSFCell cell = row.createCell(col);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(new HSSFRichTextString(text));
    }

    private static String getCellText(InventoryCell dataCell) {

        StringBuffer sb = new StringBuffer();
        List<LinkResource> links = dataCell.getLinks();
        if (links != null && links.size() > 0) {
            for (int i = 0; i < links.size(); i++) {
                sb.append(links.get(i).getName() == null ? "" : links.get(i).getName());
                if (i != links.size() - 1) {
                    sb.append("\n");
                }
            }
        } else {
            sb.append(dataCell.getText() == null ? "" : dataCell.getText());
        }

        return sb.toString();
    }

    public static List<InventoryCell> getMengNiuInventoryTitle(int start, int end, List<String> titles,boolean isShowNum) {
        List<InventoryCell> titleCells = new ArrayList<>();
        InventoryCell cell = null;
        for (String t : titles) {
            cell = new InventoryCell();
            cell.setText(t);
            titleCells.add(cell);
        }
        titleCells.addAll(initTitleInventoryCell(start, end, isShowNum));
        return titleCells;
    }

    /**
     * 获得清单的标题，包含几级名称编号以及其它具体的列的名称
     *
     * @param start
     * @param end
     * @param titles
     * @return
     */
    public static List<InventoryCell> getProessMapInventoryTitle(int start, int end, List<String> titles,boolean isShowNum) {
        List<InventoryCell> titleCells = initTitleInventoryCell(start, end, isShowNum);
        InventoryCell cell = null;
        for (String t : titles) {
            cell = new InventoryCell();
            cell.setText(t);
            titleCells.add(cell);
        }
        return titleCells;
    }

    /**
     * 获得清单的标题，包含几级名称编号以及其它具体的列的名称
     *
     * @param start
     * @param end
     * @param titles
     * @return
     */
    public static List<InventoryCell> getInventoryTitle(int start, int end, List<String> titles) {
        List<InventoryCell> titleCells = initTitleInventoryCell(start, end, true);
        InventoryCell cell = null;
        for (String t : titles) {
            cell = new InventoryCell();
            cell.setText(t);
            titleCells.add(cell);
        }
        return titleCells;
    }


    /**
     * 获得清单的标题，包含几级名称编号以及其它具体的列的名称
     *
     * @param start
     * @param end
     * @param titles
     * @return
     */
    public static List<InventoryCell> getProcessInventoryTitle(int start, int end, List<String> titles,boolean isShowNum) {
        List<InventoryCell> titleCells = initTitleInventoryCell(start, end, isShowNum);
        InventoryCell cell = null;
        for (String t : titles) {
            cell = new InventoryCell();
            cell.setText(t);
            titleCells.add(cell);
        }
        return titleCells;
    }

    /**
     * 获得清单的标题，包含几级名称以及其它具体的列的名称
     *
     * @param start
     * @param end
     * @param titles
     * @return
     */
    public static List<InventoryCell> getInventoryTitleNoNum(int start, int end, List<String> titles) {
        List<InventoryCell> titleCells = initTitleInventoryCell(start, end, false);
        InventoryCell cell = null;
        for (String t : titles) {
            cell = new InventoryCell();
            cell.setText(t);
            titleCells.add(cell);
        }
        return titleCells;
    }


    /**
     * 获得清单的标题，不包含几级名称
     *
     * @param start
     * @param end
     * @param titles
     * @return
     */
    public static List<InventoryCell> getInventoryTitleNoNums(int start, int end, List<String> titles) {
        List<InventoryCell> titleCells = initTitleInventoryCell(start, end, false);
        InventoryCell cell = null;
        for (String t : titles) {
            cell = new InventoryCell();
            cell.setText(t);
            titleCells.add(cell);
        }
        return titleCells;
    }


    /**
     * 获得清单的标题，包含几级名称以及其它具体的列的名称
     *
     * @param start
     * @param end
     * @param titles
     * @return
     */
    public static List<InventoryCell> getMengNiuInventoryTitleNoNum(int start, int end, List<String> titles) {
        List<InventoryCell> titleCells = new ArrayList<>();
        InventoryCell cell = null;
        for (String t : titles) {
            cell = new InventoryCell();
            cell.setText(t);
            titleCells.add(cell);
        }
        titleCells.addAll(initTitleInventoryCell(start, end, false));
        return titleCells;
    }

    /**
     * 初始化 n-m 级的名称和编号列
     *
     * @param start
     * @param end
     * @return
     */
    private static List<InventoryCell> initTitleInventoryCell(int start, int end, boolean hasNumber) {
        List<InventoryCell> title = new ArrayList<InventoryCell>();
        if (start > end) {
            return title;
        }
        InventoryCell cell = null;
        for (int i = start; i <= end; i++) {
            cell = new InventoryCell();
            cell.setText(i + JecnProperties.getValue("levelName"));//级名称
            title.add(cell);
            if (hasNumber) {
                cell = new InventoryCell();
                cell.setText(i + JecnProperties.getValue("levelNum"));//级编号
                title.add(cell);
            }
        }
        return title;
    }

    /**
     * 获得填充的级别名称和编号的集合
     *
     * @param dirMap
     * @param startLevel
     * @param endLevel
     * @param path       需要写出的一行数据的path,例如流程，制度，文件的一条记录的path
     * @return
     */
    public static List<InventoryCell> getLevels(Map<Long, FileDirCommonBean> dirMap, int startLevel, int endLevel, String path, boolean hasNumber) {
        return fillLevel(dirMap, startLevel, endLevel, path, hasNumber);
    }


    /**
     * 获得填充的级别名称的集合
     *
     * @param dirMap
     * @param startLevel
     * @param endLevel
     * @param path       需要写出的一行数据的path,例如流程，制度，文件的一条记录的path
     * @return
     */
    public static List<InventoryCell> getLevelsNoNum(Map<Long, FileDirCommonBean> dirMap, int startLevel, int endLevel, String path) {
        return fillLevel(dirMap, startLevel, endLevel, path, false);
    }


    private static List<InventoryCell> fillLevel(Map<Long, FileDirCommonBean> dirMap, int startLevel, int endLevel, String path, boolean hasNumber) {
        List<InventoryCell> content = new ArrayList<InventoryCell>();
        InventoryCell cell = null;
        for (int i = startLevel; i <= endLevel; i++) {
            boolean isExist = false;
            FileDirCommonBean fileDirCommonBean = null;
            for (String idStr : Common.getPaths(path)) {
                fileDirCommonBean = Common.getFileDirCommonBean(dirMap, idStr);
                if (fileDirCommonBean != null && i == fileDirCommonBean.getCurlevel()) {
                    isExist = true;
                    break;
                }
            }

            if (isExist) {
                cell = new InventoryCell();
                cell.setText(fileDirCommonBean.getName());
                content.add(cell);
                if (hasNumber) {
                    cell = new InventoryCell();
                    cell.setText(StringUtils.isBlank(fileDirCommonBean.getNumId()) ? "" : fileDirCommonBean.getNumId());
                    content.add(cell);
                }
            } else {
                cell = new InventoryCell();
                cell.setText("");
                content.add(cell);
                if (hasNumber) {
                    cell = new InventoryCell();
                    cell.setText("");
                    content.add(cell);
                }
            }
        }
        return content;
    }

    private static HSSFCellStyle getCellStyleByStatusAndAlign(Map<String, HSSFCellStyle> styleMap, Status status, InventoryTable.Align align) {
        String condition = concatCondition(status, align);
        return styleMap.get(condition);
    }

    private static String concatCondition(InventoryTable.Status status, InventoryTable.Align align) {
        if (status == null && status == null) {
            return null;
        }
        if (align == null) {
            return status.toString();
        }
        if (status == null) {
            return align.toString();
        }
        return status.toString() + "-" + align.toString();
    }

    private static Map<String, HSSFCellStyle> genericCellStyleMap(HSSFWorkbook workbook) {
        Map<String, HSSFCellStyle> cellStyleMap = new HashMap<>();
        // 既没有发布状态又没有对齐状态
        cellStyleMap.put(concatCondition(null, null), SheetCellStyle.getInventoryCellStyle(workbook, null, InventoryTable.Align.center));
        for (Status status : Status.values()) {
            for (InventoryTable.Align align : InventoryTable.Align.values()) {
                cellStyleMap.put(concatCondition(status, align), SheetCellStyle.getInventoryCellStyle(workbook, status, align));
            }
        }
        for (Status status : Status.values()) {
            cellStyleMap.put(concatCondition(status, null), SheetCellStyle.getInventoryCellStyle(workbook, status, null));
        }
        for (InventoryTable.Align align : InventoryTable.Align.values()) {
            cellStyleMap.put(concatCondition(null, align), SheetCellStyle.getInventoryCellStyle(workbook, null, align));
        }
        return cellStyleMap;
    }

}
