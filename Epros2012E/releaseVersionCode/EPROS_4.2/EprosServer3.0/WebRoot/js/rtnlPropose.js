/** 制度或者流程页面的合理化建议tab页加载时**/
function onloadPropose(flowId, isPop, flowName) {

	$("#allStateProposeId").children().remove();
	$("#allStateProposeId").load('allStateProposeJsp.action', {
		"flowId" : flowId,
		"pagerMethod" : "first",
		"processName" : flowName,
		"_totalRows" : $("#allProId").text()
	}, function() {
		
			setAllStateProposeDivWH('flowAllProposeJspId', isPop);
			onloadPage(document.getElementById("totalPages").value);
			
            
		});
}
function setAllStateProposeDivWH(divId, isPop) {
	if (document.getElementById(divId)) {
		document.getElementById("isPop").value = isPop;
		if (isPop == true || isPop == "true") {
			document.getElementById(divId).style.height = checkWidthHeight(170);
		} else {
			document.getElementById(divId).style.height = checkWidthHeight(240);
		}
	}
}
function rationalizationOnloadPropose(flowId) {
	//复选框是否选中,选中 liMyProposeCheck 值设置为1
	//		var liMyProposeCheck = 0;
	//		if(document.getElementById("myProposeCheckboxId").checked){
	//			liMyProposeCheck = 1;
	//		}
	//		alert(liMyProposeCheck);
	$("#allStateProposeId").children().remove();
	$("#allStateProposeId")
			.load(
					'allStateProposeJsp.action?flowId=' + flowId + '&pagerMethod=first',
					{
					//		"isCheckedMyPropose":liMyProposeCheck
					}, function() {
						//  if( (startti!="" && endti=="") || (startti=="" && endti!="")){
					//	  alert("请选择时间段，时间段的前后两个时间不能为空！");
					//	  return;
					//  }
					if (document.getElementById('flowAllProposeJspId')) {
						document.getElementById('flowAllProposeJspId').style.height = checkWidthHeight(218);
					}
				});
}
/***
 * 流程图---全部状态下的合理化建议数据===分页
 */
function onclickChangePage(currentPage, difPage, flowId, nowState, isResi,
		totalPages, isJump, isAdmin, event, isFlowRtnl) {
	var isPop = document.getElementById("isPop").value;
	var liMyProposeCheck = 0;
	if (isResi == 0 && isAdmin != 1) {
		if (document.getElementById("myProposeCheckboxId").checked) {
			liMyProposeCheck = 1;
		}
	}

	if (isJump != 0) {
		var theEvent = window.event || event;
		var code = theEvent.keyCode || theEvent.which;
		if (code == 13) {
			var jumpPage = document.getElementById("jumpToCurrentPageId").value;
			if (jumpPage == 0) {
				jumpPage = 1;
			}
			currentPage = jumpPage;
			difPage = '';
		} else {
			//			currentPage = isJump;
			return;
		}
	}
	if (isJump == 0 && difPage == '') {
		currentPage = document.getElementById("jumpToCurrentPageId").value;
		difPage = '';
	}

	//更换Tab显示的不同状态下的建议总数
	$
			.ajax( {
				type : 'post',
				url : 'getMyRtnlPropose.action',
				dataType : "json",
				data : {
					flowId : flowId,
					isCheckedMyPropose : liMyProposeCheck,
					isFlowRtnl : isFlowRtnl
				},
				success : function(msg) {
					document.getElementById("allProId").innerHTML = msg[0];
					document.getElementById("readProId").innerHTML = msg[1];
					document.getElementById("unReadProId").innerHTML = msg[2];
					document.getElementById("acceptProId").innerHTML = msg[3];
					document.getElementById("refuseProId").innerHTML = msg[4];

					initTabNum(msg, nowState);
					var _totalRows = getRowNum(msg,nowState);
					$
							.ajax( {
								type : 'post',
								url : 'allStateProposeJsp.action',
								data : {
									currentPage : currentPage,
									pagerMethod : difPage,
									flowId : flowId,
									nowState : nowState,
									isCheckedMyPropose : liMyProposeCheck,
									isFlowRtnl : isFlowRtnl,
									_totalRows : _totalRows
								},
								success : function(msg) {
									$("#allStateProposeId").html(msg);
									setAllStateProposeDivWH(
											'flowAllProposeJspId', isPop);
									if (difPage == 'first'
											|| (currentPage == 1 || currentPage == 2)
											&& (difPage == 'previous')) {
										document.getElementById("previousPage").src = basePath
												+ "images/propose/before1.gif";
										document.getElementById("previousPage").onclick = "";
										document.getElementById("homePage").src = basePath
												+ "images/propose/before2.gif";
										document.getElementById("homePage").onclick = "";
									} else {
										document.getElementById("previousPage").src = basePath
												+ "images/propose/before01.gif";
										document.getElementById("homePage").src = basePath
												+ "images/propose/before02.gif";
									}

									if (difPage == 'last'
											|| (totalPages == currentPage || parseInt(currentPage) + 1 == totalPages)
											&& (difPage == 'next')) {
										document.getElementById("nextPage").src = basePath
												+ "images/propose/after03.gif";
										document.getElementById("nextPage").onclick = "";
										document.getElementById("endPage").src = basePath
												+ "images/propose/after04.gif";
										document.getElementById("endPage").onclick = "";
									}
									document.getElementById("isPop").value = isPop;
									onloadPage(totalPages);
								}
							});
				}
			});

}

function onloadPage(totalPages) {
	if (totalPages <= 1) {
		document.getElementById("nextPage").src = basePath
				+ "images/propose/after03.gif";
		document.getElementById("nextPage").onclick = "";
		document.getElementById("endPage").src = basePath
				+ "images/propose/after04.gif";
		document.getElementById("endPage").onclick = "";

		document.getElementById("previousPage").src = basePath
				+ "images/propose/before1.gif";
		document.getElementById("previousPage").onclick = "";
		document.getElementById("homePage").src = basePath
				+ "images/propose/before2.gif";
		document.getElementById("homePage").onclick = "";
	}
	
	
}

/**
 * 初始化tab页面的数量
 * @param {Object} msg
 */
function initTabNum(msg) {
	document.getElementById("allProId").innerHTML = msg[0];
	document.getElementById("readProId").innerHTML = msg[1];
	document.getElementById("unReadProId").innerHTML = msg[2];
	document.getElementById("acceptProId").innerHTML = msg[3];
	document.getElementById("refuseProId").innerHTML = msg[4];
}

/**
 * 获得某个状态下的合理化建议的总数
 * @param {Object} msg
 * @param {Object} nowState 当前状态
 * @return {TypeName} 
 */
function getRowNum(msg, nowState) {

	var numIndex;
	if (nowState == -1) {//全部
		numIndex = 0;
	} else if (nowState == 0) {//未读
		numIndex = 2;
	} else if (nowState == 1) {//已读
		numIndex = 1;
	} else if (nowState == 2) {//采纳
		numIndex = 3;
	} else if (nowState == 3) {//拒绝
		numIndex = 4;
	}
	
	return msg[numIndex];

}

/***
 * 导航---全部状态下的合理化建议数据===分页
 */
function onclickNavigationChangePage(currentPage, difPage, flowId, nowState,
		isResi, totalPages, isJump, event) {
	var processName = document.getElementById("processName").value;
	var processNum = document.getElementById("processNumber").value;
	var orgId = document.getElementById("orgDutyId_rtnlPropose_id").value;
	var resPeopleId = document.getElementById("flowResPeople_rtnlPropose_id").value;
	var submitPeopleId = document.getElementById("submitPeople_rtnlPropose_id").value;
	var startTime = document.getElementById("startTime").value;
	var endTime = document.getElementById("endTime").value;
	var handleProposeId = document.getElementById("handleProposeId");
	var isSelectedHandle = 0;
	var submitProposeId = document.getElementById("submitProposeId");
	var isSelectedSubmit = 0;
	// 合理化建议类型
	var rtnlTypeId = document.getElementById("rtnlType_id").value;

	if (handleProposeId.checked) {
		isSelectedHandle = 1;
	}
	if (submitProposeId.checked) {
		isSelectedSubmit = 1;
	}
	if (isJump != 0) {
		var theEvent = window.event || event;
		var code = theEvent.keyCode || theEvent.which;
		if (code == 13) {
			var jumpPage = document.getElementById("jumpToCurrentPageId").value;
			if (jumpPage == 0) {
				jumpPage = 1;
			}
			currentPage = jumpPage;
			difPage = '';
		} else {
			//			currentPage = isJump;
			return;
		}
	}
	if (isJump == 0 && difPage == '') {
		currentPage = document.getElementById("jumpToCurrentPageId").value;
		difPage = '';
	}
	//更换Tab显示的不同状态下的建议总数
	$
			.ajax( {
				type : 'post',
				url : 'getNavigationRtnlPropose.action',
				dataType : "json",
				data : {
					rtnlTypeId : rtnlTypeId,
					processName : processName,
					processNum : processNum,
					orgId : orgId,
					resPeopleId : resPeopleId,
					submitPeopleId : submitPeopleId,
					startTime : startTime,
					endTime : endTime,
					isSelectedHandle : isSelectedHandle,
					isSelectedSubmit : isSelectedSubmit
				},
				success : function(msg) {

					initTabNum(msg, nowState);
					var _totalRows = getRowNum(msg,nowState);

					$
							.ajax( {
								type : 'post',
								url : 'searchProposeShow.action',
								data : {
									rtnlTypeId : rtnlTypeId,
									currentPage : currentPage,
									pagerMethod : difPage,
									nowState : nowState,
									processName : processName,
									processNum : processNum,
									orgId : orgId,
									resPeopleId : resPeopleId,
									submitPeopleId : submitPeopleId,
									startTime : startTime,
									endTime : endTime,
									isSelectedHandle : isSelectedHandle,
									isSelectedSubmit : isSelectedSubmit,
									_totalRows : _totalRows
								},
								success : function(msg) {
									$("#rtnlProposeShow_Grid").html(msg);
									setRtnlPropseWH('rtnlProposeShowContent');
									if (difPage == 'first'
											|| (currentPage == 1 || currentPage == 2)
											&& (difPage == 'previous')) {
										document.getElementById("previousPage").src = basePath
												+ "images/propose/before1.gif";
										document.getElementById("previousPage").onclick = "";
										document.getElementById("homePage").src = basePath
												+ "images/propose/before2.gif";
										document.getElementById("homePage").onclick = "";
									} else {
										document.getElementById("previousPage").src = basePath
												+ "images/propose/before01.gif";
										document.getElementById("homePage").src = basePath
												+ "images/propose/before02.gif";
									}

									if (difPage == 'last'
											|| (totalPages == currentPage || parseInt(currentPage) + 1 == totalPages)
											&& (difPage == 'next')) {
										document.getElementById("nextPage").src = basePath
												+ "images/propose/after03.gif";
										document.getElementById("nextPage").onclick = "";
										document.getElementById("endPage").src = basePath
												+ "images/propose/after04.gif";
										document.getElementById("endPage").onclick = "";
									}
									onloadPage(totalPages);
								}
							});

				}
			});

}
/**
 * 流程图/制度 合理化建议Tab切换
 * @param {Object} type
 */
/**
 * 
 * @param {Object} type
 * @param {Object} flowId
 * @param {Object} currentPage
 * @param {Object} isRes 是否是流程责任人标识 0：否，1：是 
 * @param {Object} isAdmin 
 * @param {Object} isShowAllReadImg
 * @param {Object} isFlowRtnl
 * @param {Object} flowRuleName
 */
function secProposeBoard(type, flowId, currentPage, isRes, isAdmin, isFlowRtnl,
		flowRuleName) {
	var isPop = document.getElementById("isPop").value;
	var liMyProposeCheck = 0;
	if (document.getElementById("myProposeCheckboxId").checked) {
		liMyProposeCheck = 1;
	}

	//当前tab页id
	var curProposeId = 'liAllStatePropose';
	//当前tab页状态
	var nowState = '-1';
	if (type == 0) {
		curProposeId = 'liAllStatePropose';
		nowState = '-1';
	} else if (type == 1) {
		curProposeId = 'liReadedPropose';
		nowState = '1';
	} else if (type == 2) {
		curProposeId = 'liUnReadPropose';
		nowState = '0';
	} else if (type == 3) {
		curProposeId = 'liAcceptPropose';
		nowState = '2';
	} else if (type == 4) {//拒绝
		curProposeId = 'liRefusePropose';
		nowState = '3';
	}

	//隐藏字段， 判断当前页面显示的是哪个状态下的建议数据 -1：全部，3:拒绝   2：采纳，1：已读，0：未读
	document.getElementById("hideStateValue").value = nowState;

	//所有的  已读  未读  已采纳的
	var proposeArr = [ 'liAllStatePropose', 'liReadedPropose',
			'liUnReadPropose', 'liAcceptPropose', 'liRefusePropose' ];

	//更换tab页的样式
	for ( var i = 0; i < proposeArr.length; i++) {
		if (proposeArr[i] == curProposeId) {
			document.getElementById(proposeArr[i]).className = "tabone1";
		} else {
			document.getElementById(proposeArr[i]).className = "tabone2";
		}
	}

	//更换Tab显示的不同状态下的建议总数
	$.ajax( {
		type : 'post',
		url : 'getMyRtnlPropose.action',
		dataType : "json",
		data : {
			flowId : flowId,
			isCheckedMyPropose : liMyProposeCheck,
			isFlowRtnl : isFlowRtnl
		},
		success : function(msg) {

			initTabNum(msg);
			var _totalRows = getRowNum(msg, nowState);

			$.ajax( {
				type : 'post',
				url : 'allStateProposeJsp.action',
				data : {
					currentPage : currentPage,
					flowId : flowId,
					pagerMethod : 'first',
					nowState : nowState,
					isCheckedMyPropose : liMyProposeCheck,
					isFlowRtnl : isFlowRtnl,
					processName : flowRuleName,
					_totalRows : _totalRows
				},
				success : function(msg) {
					$("#allStateProposeId").html(msg);
					setAllStateProposeDivWH('flowAllProposeJspId', isPop);
					onloadPage(_totalRows);
					if(_totalRows<=12){
						proposeCanNotJump();
					}
				}
			});
		}
	});
	if ($("#unReadProId").text() > 0) {
		allReadCanClick();
	}
}

/**
 * 全部已读可以使用
 */
function allReadCanClick() {

	//如果未读的数目大于0则将全部已读可以使用
	if ($("#unReadProId") != undefined) {

		$("#srcUnRead").attr("src", basePath + "images/propose/allRead.gif");
		$("#srcUnRead").click(function() {
			updateToRead();
		});

	}
}

/**
 * 导航===== 合理化建议Tab切换
 * @param {Object} type
 */
function tabNavigationProposeBoard(type, currentPage) {

	//搜索条件
	var processName = document.getElementById("processName").value;
	var processNum = document.getElementById("processNumber").value;
	var orgId = document.getElementById("orgDutyId_rtnlPropose_id").value;
	var resPeopleId = document.getElementById("flowResPeople_rtnlPropose_id").value;
	var submitPeopleId = document.getElementById("submitPeople_rtnlPropose_id").value;
	var startTime = document.getElementById("startTime").value;
	var endTime = document.getElementById("endTime").value;
	var handleProposeId = document.getElementById("handleProposeId");
	var isSelectedHandle = 0;
	var submitProposeId = document.getElementById("submitProposeId");
	var isSelectedSubmit = 0;
	// 合理化建议类型
	var rtnlTypeid = document.getElementById("rtnlType_id").value;

	if (startTime > endTime) {// 结束时间大于开始时间
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip, i_startTimeIsLessEndTime);
		return;
	}

	if (handleProposeId.checked) {
		isSelectedHandle = 1;
	}
	if (submitProposeId.checked) {
		isSelectedSubmit = 1;
	}

	var curProposeId = 'liAllStatePropose';
	var nowState = '-1';
	if (type == 0) {
		curProposeId = 'liAllStatePropose';
		nowState = '-1';
	} else if (type == 1) {
		curProposeId = 'liReadedPropose';
		nowState = '1';
	} else if (type == 2) {
		curProposeId = 'liUnReadPropose';
		nowState = '0';
	} else if (type == 3) {
		curProposeId = 'liAcceptPropose';
		nowState = '2';
	} else if (type == 4) {
		curProposeId = 'liRefusePropose';
		nowState = '3';
	}

	document.getElementById("hideStateValue").value = nowState;

	//所有的  已读  未读  已采纳的
	var proposeArr = [ 'liAllStatePropose', 'liReadedPropose',
			'liUnReadPropose', 'liAcceptPropose', 'liRefusePropose' ];

	//更换tab页的样式
	for ( var i = 0; i < proposeArr.length; i++) {
		if (proposeArr[i] == curProposeId) {

			document.getElementById(proposeArr[i]).className = "tabone1";

		} else {

			document.getElementById(proposeArr[i]).className = "tabone2";
		}

	}

	//更换Tab显示的不同状态下的建议总数
	$.ajax( {
		type : 'post',
		url : 'getNavigationRtnlPropose.action',
		dataType : "json",
		data : {
			processName : processName,
			processNum : processNum,
			orgId : orgId,
			resPeopleId : resPeopleId,
			submitPeopleId : submitPeopleId,
			startTime : startTime,
			endTime : endTime,
			isSelectedHandle : isSelectedHandle,
			isSelectedSubmit : isSelectedSubmit,
			rtnlTypeId : rtnlTypeid
		},
		success : function(msg) {

			initTabNum(msg);
			var _totalRows = getRowNum(msg, nowState);
			//内容请求
		$.ajax( {
			type : 'post',
			url : 'searchProposeShow.action',
			data : {
				currentPage : currentPage,
				pagerMethod : 'first',
				nowState : nowState,
				processName : processName,
				processNum : processNum,
				orgId : orgId,
				resPeopleId : resPeopleId,
				submitPeopleId : submitPeopleId,
				startTime : startTime,
				endTime : endTime,
				isSelectedHandle : isSelectedHandle,
				isSelectedSubmit : isSelectedSubmit,
				rtnlTypeId : rtnlTypeid,
				_totalRows : _totalRows
			},
			success : function(msg) {
				$("#rtnlProposeShow_Grid").html(msg);
				setRtnlPropseWH('rtnlProposeShowContent');
				onloadPage(_totalRows);
				if(_totalRows<=12){
					proposeCanNotJump();
				}
			}
		});

	}
	});

}
//合理化建议不可跳转分页栏设置
function proposeCanNotJump(){
	
	$("#homePage").removeAttr("onclick");
	$("#homePage").attr("style","");
	$("#previousPage").removeAttr("onclick");
	$("#previousPage").attr("style","");
	$("#nextPage").removeAttr("onclick");
	$("#nextPage").attr("style","");
	$("#nextPage").attr("src",basePath
				+ "images/propose/after03.gif");
	
	$("#endPage").removeAttr("onclick");
	$("#endPage").attr("style","");
	$("#endPage").attr("src",basePath
				+ "images/propose/after04.gif");
	
	
	
}

/**
 *  全部已读，将所有的未读的合理化建议全部改为已读
 */
function updateToRead() {

	var flowId = document.getElementById("flowId").value;
	var isFlowRtnl = document.getElementById("isFlowRtnl").value;
	$.ajax( {
		type : 'post',
		url : 'updateProStateToRead.action',
		data : {
			//			strProposeId : listProposeIds
			//流程、制度合理化建议id
			flowId : flowId,
			isFlowRtnl : isFlowRtnl
		},
		success : function(msg) {
			if (msg == '0') {
				Ext.Msg.alert(i_tip, i_rtnlPropseEditing);
			} else if (msg == '1') {
				//				for ( var i = 0; i < listProposeIds.split(',').length; i++) {
		//					document.getElementById("readeImgId"
		//							+ listProposeIds.split(',')[i]).src = basePath
		//							+ "images/propose/readGray.gif";
		//					document.getElementById("readeImgId"
		//							+ listProposeIds.split(',')[i]).onclick = "";
		//					document.getElementById("proStateId"
		//							+ listProposeIds.split(',')[i]).innerHTML = "已读";
		//				}
		//通过模拟点击进行刷新
		document.getElementById("liAllStatePropose").click();
		//将全部已读改为置灰
		document.getElementById("srcUnRead").src = basePath
				+ "images/propose/allReadGray.gif";
		document.getElementById("srcUnRead").onclick = "";
	} else if (msg == '2') {
		Ext.Msg.alert(i_tip, i_nowJspNoUnReadPropse);
	}
}
	});
}
/***
 * 已读   更新单条数据为已读状态
 */
function updateSingleUnToRead(id, intflag) {
	$
			.ajax( {
				type : 'post',
				url : 'updatesingleStateToRead.action',
				data : {
					rtnlProposeId : id,
					intflag : intflag
				},
				success : function(msg) {
					if (msg == '0') {
						Ext.Msg.alert(i_tip, i_rtnlPropseEditing);
					} else if (msg == '1') {
						document.getElementById("readeImgId" + id).src = basePath
								+ "images/propose/readGray.gif";
						document.getElementById("readeImgId" + id).onclick = "";
						document.getElementById("proStateId" + id).innerHTML = i_readed;
					} else if (msg == '2') {
						document.getElementById("readeImgId" + id).src = basePath
								+ "images/propose/readGray.gif";
						document.getElementById("readeImgId" + id).onclick = "";
						document.getElementById("proStateId" + id).innerHTML = i_readed;
					} else if (msg == '3') {
						Ext.Msg.alert(i_tip, i_noExitAndRefresh);
						o = document.getElementById("tablePropose" + id);
						o.parentNode.removeChild(o);
					} else if (msg == '6') {
						Ext.Msg.alert(i_tip, i_findManager);
					}

				}
			});

}
/***
 * 拒绝采纳
 */
function updateToCancelAccept(id, intflag) {
	$
			.ajax( {
				type : 'post',
				url : 'updateSingleState.action',
				data : {
					rtnlProposeId : id,
					intflag : intflag
				},
				success : function(msg) {
					if (msg == '0') {//失败
					Ext.Msg.alert(i_tip, i_rtnlPropseEditing);
				} else if (msg == '1') {//成功

					document.getElementById("cancelAcceptImgId" + id).src = basePath
							+ "images/propose/cancelAcceptGray.gif";
					document.getElementById("cancelAcceptImgId" + id).onclick = "";
					document.getElementById("proStateId" + id).innerHTML = i_refuse;
					document.getElementById("isReply" + id).value = intflag;
					//采纳不置灰
					document.getElementById("acceptImgId" + id).src = basePath
							+ "images/propose/accept.gif";
					document.getElementById("acceptImgId" + id).onclick = function() {
						updateToAccept(id, 2);
					}
					$("#editImgId" + id).attr("src", "images/propose/edit.gif");
				} else if (msg == '2') {
					document.getElementById("cancelAcceptImgId" + id).src = basePath
							+ "images/propose/cancelAcceptGray.gif";
					document.getElementById("cancelAcceptImgId" + id).onclick = "";
					document.getElementById("proStateId" + id).innerHTML = i_readed;
					document.getElementById("acceptImgId" + id).src = basePath
							+ "images/propose/accept.gif";
					document.getElementById("acceptImgId" + id + "").onclick = function() {
						updateToAccept(id, 2);
					};
					document.getElementById("isReply" + id).value = intflag;
					document.getElementById("editImgId" + id).src = basePath
							+ "images/propose/edit.gif";
				} else if (msg == '3') {
					Ext.Msg.alert(i_tip, i_noExitAndRefresh);
					o = document.getElementById("tablePropose" + id);
					o.parentNode.removeChild(o);
				} else if (msg == '6') {
					Ext.Msg.alert(i_tip, i_findManager);
				} else if (msg == '7') {
					Ext.Msg.alert(i_tip, i_handleFailProposeHadRefuse);
				}

			}
			});
}
/***
 * 采纳状态
 */
function updateToAccept(id, intflag) {
	//返回的状态码
	//1:修改成功 ;2:当前点击取消采纳/已读 时别人已取消采纳/已读 （不提示）;
	//3:该建议被删除(操作失败，该建议已不存在)；4：当前点击采纳时已被别人采纳（操作失败，该建议已被采纳）；
	//5：回复操作更新失败（操作失败）;6:逻辑错误或异常（后台逻辑错误，请联系管理员！）
	$
			.ajax( {
				type : 'post',
				url : 'updatesingleStateToRead.action',
				data : {
					rtnlProposeId : id,
					intflag : intflag
				},
				success : function(msg) {
					if (msg == '0') {
						Ext.Msg.alert(i_tip, i_rtnlPropseEditing);
					} else if (msg == '1') {
						document.getElementById("acceptImgId" + id).src = basePath
								+ "images/propose/acceptGray.gif";
						document.getElementById("acceptImgId" + id).onclick = "";
						document.getElementById("proStateId" + id).innerHTML = i_accept;
						document.getElementById("readeImgId" + id).src = basePath
								+ "images/propose/readGray.gif";
						document.getElementById("readeImgId" + id).onclick = "";
						document.getElementById("isReply" + id).value = intflag;

						//拒绝不置灰
						document.getElementById("cancelAcceptImgId" + id).src = basePath
								+ "images/propose/cancelAccept.gif";

						document.getElementById("cancelAcceptImgId" + id).onclick = function() {

							updateToCancelAccept(id, 3);

						}

						$("#editImgId" + id).attr("src",
								"images/propose/editGray.gif");
					} else if (msg == '4') {
						Ext.Msg.alert(i_tip, i_proposeHadAccept);
					} else if (msg == '3') {
						Ext.Msg.alert(i_tip, i_noExitAndRefresh);
						o = document.getElementById("tablePropose" + id);
						o.parentNode.removeChild(o);
					} else if (msg == '6') {
						Ext.Msg.alert(i_tip, i_findManager);
					}
				}
			});
}
/***
 * 回复建议
 * 
 */
function replyContentFunction(id) {
	if (document.getElementById("spanContentId" + id).innerHTML != '') {
		var textContents = document.getElementById("spanContentId" + id).innerHTML;
		document.getElementById("textContentId" + id).value = replaceTextarea(textContents);
	}
	var isShow = document.getElementById("isShowId" + id).value;
	var divShow = document.getElementById("replyContentDivId" + id);
	// 给回复状态标识隐藏字段赋值
	if (isShow == 0) {
		divShow.style.display = 'block';
		//给显示点击回复按钮显示与隐藏的隐藏字段赋值
		document.getElementById("isShowId" + id).value = 1;
	} else if (isShow == 1) {
		divShow.style.display = 'none';
		//给显示点击回复按钮显示与隐藏的隐藏字段赋值
		document.getElementById("isShowId" + id).value = 0;
	}
}
function replaceTextarea(str) {
	var reg = new RegExp("<br>", "g");
	str = str.replace(reg, "\r\n");

	var regn = new RegExp("<br>", "g");
	str = str.replace(regn, "\n");
	// 去掉左侧空格（span标签导致的问题）
	str = ltrim(str);
	return str;
}

function replaceTextareaSub(str) {
	var reg = new RegExp("\r\n", "g");
	str = str.replace(reg, "<BR>");

	var regn = new RegExp("\n", "g");
	str = str.replace(regn, "<BR>");

	return str;
}

/***
 * 回复内容提交
 */
function submitContent(id, intflag) {
	var divShow = document.getElementById("replyContentDivId" + id);
	var strContent = document.getElementById("textContentId" + id).value
	var loginName = document.getElementById("loginName").value
	var isout = strContent.replace(/[^\x00-\xFF]/g, '**').length;
	//去掉右空格
	strContent = rtrim(strContent);
	if (strContent == null || strContent == '') {
		Ext.Msg.alert(i_tip, i_replyContentNoEmpty);
		return false;
	}
	if (isout > 1200) {
		Ext.Msg.alert(i_tip, i_propseLength);
		return false;
	}
	//页面显示的建议状态，如果是采纳状态，回复内容事不改变状态
	var oldIntflag = document.getElementById("isReply" + id).value;
	if (oldIntflag == 2) {
		intflag = oldIntflag;
	}
	$
			.ajax( {
				type : 'post',
				url : 'updatesingleStateToRead.action',
				data : {
					rtnlProposeId : id,
					intflag : intflag,
					strContent : strContent
				},
				success : function(msg) {

					if (msg == '0') {
						Ext.Msg.alert(i_tip, i_rtnlPropseEditing);
					} else if (msg == '1') {

						//获取服务器的时间
						var df = formatDate('y-m-d', $.ajax( {
							async : false
						}).getResponseHeader("Date"));
						document.getElementById("spanContentId" + id).innerHTML = replaceTextareaSub(strContent);
						document.getElementById("replyFlagShow" + id).innerHTML = i_replyC;
						document.getElementById("replyFlagShow" + id).style.display = "block";
						document.getElementById("replyNameShow" + id).innerHTML = loginName;
						document.getElementById("replyTimeShow" + id).innerHTML = df;//"回复于:"
						document.getElementById("deleteReplyHrefId" + id).innerHTML = i_Delete;
						if (intflag == 1) {
							document.getElementById("proStateId" + id).innerHTML = i_readed;
						}
						document.getElementById("readeImgId" + id).src = basePath
								+ "images/propose/readGray.gif";
						document.getElementById("readeImgId" + id).onclick = "";
						var isShow = document.getElementById("isShowId" + id).value;
						// 给回复状态标识隐藏字段赋值
						if (isShow == 0) {
							divShow.style.display = 'block';
							//给显示点击回复按钮显示与隐藏的隐藏字段赋值
							document.getElementById("isShowId" + id).value = 1;
						} else if (isShow == 1) {
							divShow.style.display = 'none';
							//给显示点击回复按钮显示与隐藏的隐藏字段赋值
							document.getElementById("isShowId" + id).value = 0;
						}
					} else if (msg == '3') {
						Ext.Msg.alert(i_tip, i_noExitAndRefresh);
						o = document.getElementById("tablePropose" + id);
						o.parentNode.removeChild(o);
					} else if (msg == '5') {
						Ext.Msg.alert(i_tip, i_poposeError);
					} else if (msg == '6') {
						Ext.Msg.alert(i_tip, i_findManager);
					}

				}
			});
}
//格式化时间
function formatDate(formatStr, fdate) {
	var fTime, fStr = 'ymdhis';
	if (!formatStr)
		formatStr = "y-m-d h:i:s";
	if (fdate)
		fTime = new Date(fdate);
	else
		fTime = new Date();
	var formatArr = [ fTime.getFullYear().toString(),
			(fTime.getMonth() + 1).toString(), fTime.getDate().toString(),
			fTime.getHours().toString(), fTime.getMinutes().toString(),
			fTime.getSeconds().toString() ]
	for ( var i = 0; i < formatArr.length; i++) {
		formatStr = formatStr.replace(fStr.charAt(i), formatArr[i]);
	}
	return formatStr;
}
/***
 * 取消回复
 * @param {Object} flowId
 */
function cancelReply(id) {
	var isShow = document.getElementById("isShowId" + id).value;
	var divShow = document.getElementById("replyContentDivId" + id);
	//	// 给回复状态标识隐藏字段赋值
	//	document.getElementById("isReply"+id).value=intflag;
	if (isShow == 0) {
		divShow.style.display = 'block';
		//给显示点击回复按钮显示与隐藏的隐藏字段赋值
		document.getElementById("isShowId" + id).value = 1;
	} else if (isShow == 1) {
		divShow.style.display = 'none';
		//给显示点击回复按钮显示与隐藏的隐藏字段赋值
		document.getElementById("isShowId" + id).value = 0;
	}
}
/***
 * 获取我的建议/我处理的建议
 */
function selectMyPropose(flowId, isFlowRtnl) {
	//我的建议/我处理的建议是否选中
	var liMyProposeCheck = 0;
	if (document.getElementById("myProposeCheckboxId").checked) {
		liMyProposeCheck = 1;
	}
	var hideStateValue = document.getElementById("hideStateValue").value;
	$.ajax( {
		type : 'post',
		url : 'getMyRtnlPropose.action',
		dataType : "json",
		data : {
			flowId : flowId,
			isCheckedMyPropose : liMyProposeCheck,
			isFlowRtnl : isFlowRtnl
		},
		success : function(msg) {
			document.getElementById("allProId").innerHTML = msg[0];
			document.getElementById("unReadProId").innerHTML = msg[1];
			document.getElementById("readProId").innerHTML = msg[2];
			document.getElementById("acceptProId").innerHTML = msg[3];
			document.getElementById("refuseProId").innerHTML = msg[4];
			
			//刷新当前页面建议数据-1全部	0 未读 	1 已读	2 采纳 	 3拒绝
		if(hideStateValue == -1){//全部
		    onclickChangePage('1', 'first', flowId, -1, 0, msg[0], 0, 0, null,
					isFlowRtnl);
		} else if (hideStateValue == 3) {//拒绝
			onclickChangePage('1', 'first', flowId, 3, 0, msg[4], 0, 0, null,
					isFlowRtnl);
		} else if (hideStateValue == 2) {//采纳
			onclickChangePage('1', 'first', flowId, 2, 0, msg[3], 0, 0, null,
					isFlowRtnl);
		} else if (hideStateValue == 1) {//已读
			onclickChangePage('1', 'first', flowId, 1, 0, msg[2], 0, 0, null,
					isFlowRtnl);
		} else if (hideStateValue == 0) {//未读
			onclickChangePage('1', 'first', flowId, 0, 0, msg[1], 0, 0, null,
					isFlowRtnl);
		}

	}
	});

}
/***
 * 删除未读状态下的建议
 */
function deleteMyPropose(id) {
	if (!confirm('确认删除吗？')) {
		return false;
	}
	$.ajax( {
		type : 'post',
		url : 'deleteMyFlowPropose.action',
		data : {
			rtnlProposeId : id
		},
		success : function(msg) {
			if (msg == '0') {
				Ext.Msg.alert(i_tip, i_noDeletePropose);
			} else if (msg == '1') {
				//通过模拟点击进行刷新
		document.getElementById("liAllStatePropose").click();

	}
}
	});
}
//去左空格
function ltrim(s) {
	return s.replace(/(^\s*)/, "");
}
//去右空格;   
function rtrim(s) {
	return s.replace(/(\s*$)/, "");
}
/**
 * 点击提交建议显示DIV
 * @param {Object} isEdit 0：新建合理化建议；1：编辑合理化建议
 * @param {Object} methodType:0:针对流程制度详细提交建议；1：导航栏提交建议(导航栏编辑合理化建议时：传的值为0，添加时传值为1)
 * @param {Object} rtnlType: 针对单个流程、制度提交建议类型：0：流程；1：制度
 * @param {Object} rtnlTarNames  针对单个流程、制度提交建议：制度、流程名称
 * @param {Object} fileName  编辑合理化建议显示文件名称
 * @param {Object} rtnlId 合理化建议主键文件ID
 * @param {Object} isEditMethodType编辑合理化建议时：0：针对单个流程制度建议，1：导航栏建议
 */
function showDiv(isEdit, methodType, flowId, isRes, isAdmin, rtnlType,
		rtnlTarNames, fileName, rtnlId, isEditMethodType, fileId) {
	//下拉框数据
	var data;
	if (isHiddenSystem == 0) {// 0：显示制度
		data = [ {
			'name' : 'flowchart',
			'text' : i_process
		}, {
			'name' : 'rulechart',
			'text' : i_rule
		} ];
	} else {
		data = [ {
			'name' : 'flowchart',
			'text' : i_process
		} ];
	}
	
	//P1不显示制度移除制度信息
	if(!versionType){
		data.pop();
	}
	
	var store = new Ext.data.JsonStore( {
		data : data,
		fields : [ 'name', 'text' ]
	});
	//下拉框显示：选择的制度/流程
	var comboc =new Ext.form.ComboBox( {
		id : 'comBoxId',
		width : 70,
		store : store,
		mode : 'local',
		editable : false,
		valueField : 'name',
		displayField : 'text',
		triggerAction : 'all',
		listeners : {
			afterRender : function(comboc) {
				if(methodType==0){//针对单个制度、流程提交合理化建议
					//设计下拉框不可编辑
					Ext.getCmp("comBoxId").setDisabled(true);
					if(rtnlType == 0){
						var firstValue = store.reader.jsonData[0].name;
	        　　				comboc.setValue(firstValue);//同时下拉框会将与name为流程值对应的 text显示
					}else if(rtnlType == 1){
						var firstValue = store.reader.jsonData[1].name;
	        　　				comboc.setValue(firstValue);//同时下拉框会将与name为制度值对应的 text显示
					}
				}else if(methodType==1){//导航栏提交合理化建议
					//默认选中第一个
					var firstValue = store.reader.jsonData[0].name;
	        　　			comboc.setValue(firstValue);//同时下拉框会将与name为firstValue值对应的 text显示
				}
			}
		}
	});
	//显示 选择框：制度、流程名称数据源
	var storeComBox = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
        	url: "selectRuleFlowName.action"
        }),
		reader: new Ext.data.ArrayReader({}, [ {name: 'name'}, {name: 'text'}])
    	});
	//显示 选择框：制度、流程名称
	var inputComboBox = new Ext.form.ComboBox( {
		id : 'searchComNameId',
		width : 250,
		store : storeComBox,
		hideTrigger:true,
		mode : 'remote',
		editable : true,
		valueField : 'name',
		displayField : 'text',
		triggerAction : 'all',
		forceSelection: true,
	    allowBlank: false,
	    name:"ruleFlowName",
		hiddenName:"ruleFlowName",
		listeners : {
				'keyup' : function(o) {
					storeComBox.load(
						{
							params : {//获取输入ComboBox的值传到后台action
								ruleFlowName : ""+inputComboBox.getRawValue()+"",
								searchType : comboc.getValue()
							}
						}
					);
				},
				'beforeselect' : function(icmb,record ,index ){
			       tValue = record.data.name; //name为displayField中属性
			       rtnlTargetChoose.setValue(tValue)//设置显示值
			     }
		}
	
	});
	//选择框：制度、流程隐藏ID
	var rtnlTargetChoose = new Ext.form.TextField( {
		id : 'rtnlTargetChooseId',
		text:'',
		hidden: true
	});
	//打开流程/制度选择框
	var rtnlTargetBut = new Ext.Button( {
		id : 'showRtnlTargetbtn',
		width : 60,
		height: 20,
		enableToggle : true,
		pressed : true,//按钮按下状态enableToggle : true时有效
		text : i_selectBut,
		handler : function() {
			var inputId = inputComboBox.getId();
			var hiddenId = rtnlTargetChoose.getId();
			//判断标识0：流程，1：制度comboc.SelectedIndex
			var comValue = comboc.getValue();
			var type;
			if(comValue == 'flowchart'){
				type = 0;
			}else if(comValue == 'rulechart'){
				type = 1;
			}
			selectProcessOrRuleWindow(type,inputId, hiddenId);
		}
	});
	if(methodType == 0){
		//制度、名称输入框设置为只读
		inputComboBox.setDisabled(true);
		//设置显示值
		inputComboBox.setValue(rtnlTarNames);
		//隐藏制度、流程Id设置显示值
		rtnlTargetChoose.setValue(flowId);
		//选择制度、流程按钮设置隐藏
		rtnlTargetBut.hide();
	}
	//合理化建议内容编写框
	var rtnlContentArea = new Ext.form.TextArea({
		id : 'rtnlContentAreaId',
		height : 330,
		width : 466
	});
	//窗体显示内容
	var formPal = new Ext.form.FormPanel( {
		id :'formSubmitId',
		labelAlign : "right",
		labelWidth : 80,
		fileUpload: true,
		defaults : {
			width : 200
		},
		tbar : [ i_typeC, comboc, i_nameC, 
			inputComboBox,
			rtnlTargetChoose,
				rtnlTargetBut ],
		items : [{
			 layout: 'column',
             baseCls: 'x-panel-mc',
             anchor:'100%',
			items:[{
	            xtype:'textfield',
	            fieldLabel: i_plChooseFile,
	            id:'upfileProposeId',
	            allowBlank:true,
	            inputType:'file',
	            name:'upload'
            },{
            	xtype: 'label',
                id: 'rtnlFileLabId',
                text: fileName,
                hidden:true
	            },
	            {
            	xtype: 'button',
                id: 'rtnlFileDel',
                text: i_Delete
                ,
                hidden:true,
              	listeners : { click : function() {
	            	Ext.getCmp("rtnlFileLabId").setVisible(false);
	            	Ext.getCmp("rtnlFileDel").setVisible(false);
	            	Ext.getCmp("rtnlFileLabId").text='';
        			 }
    		     }
	            }]
				}]

	});
	
	
	//合理化建议内容编写框
	if(isEdit == 1){
		//合理化建议内容
		var proposeContent = document.getElementById("getProposeContent" + rtnlId).innerHTML;
		rtnlContentArea.setValue(replaceTextarea(proposeContent));
		Ext.getCmp("rtnlFileLabId").setVisible(true);
		var rtnFiledLabText = Ext.getCmp("rtnlFileLabId").text;
		if( rtnFiledLabText!= null && rtnFiledLabText != ''){
			Ext.getCmp("rtnlFileDel").setVisible(true);
		}
	}
	//弹出窗体
	var rtnlWindow = new Ext.Window(
			{
				title : i_submitRtnl,// 提交建议
				height : 450,
				width : 480,
				minHeight : 450,
				minWidth : 480,
				closeAction : "close",
				resizable : false,// 四边和四角改变window的大小
				modal : true,
				items : 
					[    formPal
						,rtnlContentArea
			        ],
				buttons : [
						{
							text : i_ok,// 确定
							handler : function() {
								var relationId = rtnlTargetChoose.getValue();
								if (relationId == null || relationId == "") {
									if (flowId == -1) {
										Ext.Msg.alert(i_tip, i_selectName);
										return;
									}
									relationId = flowId;
								}
 								//获取上传文件
								var upFileProposeValue = Ext.getCmp("upfileProposeId").getValue();
								//获取合理化建议内容
								var rtnlContent = rtnlContentArea.getValue();
								//编辑合理化建议：获取附件名称
								var rtnlFiledLabName = Ext.getCmp("rtnlFileLabId").text;
								//去左右空格;
								rtnlContent=rtrim(ltrim(rtnlContent));
								var isOut = rtnlContent.replace(
										/[^\x00-\xFF]/g, '**').length;
								if (rtnlContent == null || rtnlContent == "") {
									if((rtnlFiledLabName == null || rtnlFiledLabName =='') && (upFileProposeValue == null || upFileProposeValue =='')){
										Ext.Msg.alert(i_tip, i_propseContentAndFileNoSameEmpty);
										return false;
									}
								} else if (isOut > 1200) {
									Ext.Msg.alert(i_tip, i_propseLength);
									return false;
								}
								rtnlContent = replaceTextareaSub(rtnlContent);
								//判断标识0：流程，1：制度comboc.SelectedIndex
								var comValue = comboc.getValue();
								var type;
								if(comValue == 'flowchart'){
									type = 0;
								}else if(comValue == 'rulechart'){
									type = 1;
								}
								//判断是否删除原有文件
								var isFlag;
								if(Ext.getCmp("rtnlFileLabId").isVisible()){
									isFlag = '1';
								}else{
									isFlag ='0';
								}
								formPal.getForm().submit({
													url : "uploadRtnlProposeFile.action",// 这里是接收数据的程序
													params : {//参数
														flowId : relationId,
														isFlowRtnl :type,
														fileProposeContent :rtnlContent,
														addOrUpdateName : isEdit ,
														editrtnlProposeId : rtnlId,//合理化建议主键ID
														isFlag:isFlag,
														editProposeFileId:fileId
													},
													success:function(form,action) {
														if(methodType == 0 && isEditMethodType==0){//针对单个流程、制度提交合理化建议
															secProposeBoard(0,relationId,'1', isRes,isAdmin,type,rtnlTarNames);
															rtnlWindow.close();//关闭窗体
														}else if(methodType == 0 && isEditMethodType==1){
															tabNavigationProposeBoard(0, '1');//切换合理化建议 
															rtnlWindow.close();//关闭窗体
														}else if(methodType == 1){
															tabNavigationProposeBoard(0, '1');//切换合理化建议 
															rtnlWindow.close();//关闭窗体
														}
														//通过点击全部tab页进行刷新
													    $("#liAllStatePropose").trigger("click");
													    //全部已读可使用
													    allReadCanClick();
													}
													,
													failure:function(form,action){
														Ext.Msg.alert(i_tip,i_uploadCannotBigThan+"1M！");
													}
												});
							}
						}, {
							text : i_cancel,// 取消
							handler : function() {
								rtnlWindow.close();//关闭窗体
						}
						} ]
			});
	rtnlWindow.show();
}
/***
 * 点击显示编辑状态的DIV显示与否
 * @param {Object} id 合理化建议主键ID
 * @param {Object} fileId:文件ID
 * @param {Object} fileName: 文件名称
 * @param {Object} rtnlType  针对单个流程、制度提交建议类型：0：流程；1：制度
 * @param {Object} isEditMethodType编辑合理化建议时：0：针对单个流程制度建议，1：导航栏建议
 */
function showEditDiv(id, fileId, fileName,isRes,isAdmin,rtnlType,flowRuleName,flowRuleId,isEditMethodType) {
		$.ajax( {
					type : 'post',
					url : 'editDivIsShow.action',
					data : {
						rtnlProposeId : id
					},
					success : function(msg) {
						if(msg == '0'){
							Ext.Msg.alert(i_tip,i_noDeletePropose);
						} else if(msg == '1'){
							var proposeContent = document.getElementById("getProposeContent" + id).innerHTML;
							showDiv(1,0,flowRuleId,isRes,isAdmin,rtnlType,flowRuleName,fileName,id,isEditMethodType,fileId);
						}
					}
		});
}
function hidEditDiv() {
	document.getElementById("eidtbg").style.display = "none";
	document.getElementById("eidtshow").style.display = "none";
	document.getElementById("updateUploadFileSpan").style.display = "none";
	var file = document.getElementById("editUpfileProposeId");
	if (file.outerHTML) {
		file.outerHTML = file.outerHTML;
	}
}
/**
 * 隐藏DIV
 * 
 */
function hideDiv() {
	document.getElementById("bg").style.display = "none";
	document.getElementById("show").style.display = "none";
	document.getElementById("addUploadFileSpan").style.display = "none";
	var file = document.getElementById("upfileProposeId");
	if (file.outerHTML) {
		file.outerHTML = file.outerHTML;
	}
}
/**
 * 导航栏--提交建议界面：下拉框选择事件
 */
function selectOptionClick(){
	document.getElementById("processName_id").value='';
	document.getElementById("flowId").value='-1';
}
/**
 * 添加合理化建议
 * @param {Object} type 
 * @param {Object} flowId
 * @param {Object} isRes
 * @param {Object} isAdmin
 * @return {TypeName} 
 */
function okUploadFile(type,flowId, isRes, isAdmin) {
	if(flowId == null || flowId ==""){
		var rtnlIsFlowRtnl =document.getElementById("flowId").value;
		if(rtnlIsFlowRtnl == -1){
			Ext.Msg.alert(i_tip, i_selectName);
			return;
		}
		flowId = rtnlIsFlowRtnl;
	}
	var proposeTextarea = document.getElementById("fileProposeContentId").value;
	var upFileProposeValue = document.getElementById("upfileProposeId").value;
	var isFlowRtnl = document.getElementById("isFlowRtnl").value;
	var isOut = proposeTextarea.replace(/[^\x00-\xFF]/g, '**').length;//(proposeTextarea != null) ? proposeTextarea.length : 0;
	if ((proposeTextarea == null || proposeTextarea == "")
			&& (upFileProposeValue == null || upFileProposeValue == "")) {
		Ext.Msg.alert(i_tip, i_propseContentAndFileNoSameEmpty);
		return false;
	} else if (isOut > 1200) {
		Ext.Msg.alert(i_tip, i_propseLength);
		return false;
	}
	Ext.Ajax.request( {
		form : 'proposeFormId',
		dataType : "json",
		success : function(msg) {
			document.getElementById("bg").style.display = "none";
			document.getElementById("show").style.display = "none";
			if(type == 0){//流程/制度单独提交合理化建议
				secProposeBoard(0, flowId, '1', isRes, isAdmin,0,isFlowRtnl);
			}else if(type == 1){//导航栏提交合理化建议
				tabNavigationProposeBoard(0,'1');
			}
		}
	});
	document.getElementById("addUploadFileSpan").style.display = "none";
	document.getElementById("fileProposeContentId").value = '';
	var file = document.getElementById("upfileProposeId");
	if (file.outerHTML) {
		file.outerHTML = file.outerHTML;
	}
}
/**
 * 更新合理化建议
 * @param {Object} flowId
 * @param {Object} isRes
 * @param {Object} isAdmin
 * @return {TypeName} 
 */
function editUploadFile(flowId, isRes, isAdmin) {
	var proposeTextarea = document.getElementById("editProposeContentId").value;
	var upFileProposeValue = document.getElementById("editUpfileProposeId").value;
	var isFlowRtnl = document.getElementById("isFlowRtnl").value;
	if (document.getElementById("updateUploadFileNameId").style.display == "none") {
		var isOut = (proposeTextarea != null) ? proposeTextarea.length : 0;
		if ((proposeTextarea == null || proposeTextarea == "")
				&& (upFileProposeValue == null || upFileProposeValue == "")) {
			Ext.Msg.alert(i_tip, i_propseContentAndFileNoSameEmpty);
			return false;
		} else if (isOut > 1200) {
			Ext.Msg.alert(i_tip, i_propseLength);
			return false;
		}
	}
	Ext.Ajax.request( {
		form : 'editproposeFormId',
		dataType : "json",
		success : function(msg) {
			// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		if (msg.responseText == '0') {
			Ext.Msg.alert(i_tip, i_rtnlPropseEditing);
		} else if (msg.responseText == '1') {
			document.getElementById("eidtbg").style.display = "none";
			document.getElementById("eidtshow").style.display = "none";
			secProposeBoard(0, flowId, '1', isRes, isAdmin,0,isFlowRtnl,'');
		}
	}
	});
	document.getElementById("updateUploadFileSpan").style.display = "none";
}
/***
 * 导航---编辑合理化建议
 * @param {Object} flowId
 * @param {Object} isRes
 */
function editNavigationUploadFile() {
	var proposeTextarea = document.getElementById("editProposeContentId").value;
	var upFileProposeValue = document.getElementById("editUpfileProposeId").value;
	if (document.getElementById("updateUploadFileNameId").style.display == "none") {
		var isOut = proposeTextarea.replace(/[^\x00-\xFF]/g, '**').length;//(proposeTextarea != null) ? proposeTextarea.length : 0;
		if ((proposeTextarea == null || proposeTextarea == "")
				&& (upFileProposeValue == null || upFileProposeValue == "")) {
			Ext.Msg.alert(i_tip, i_propseContentAndFileNoSameEmpty);
			return false;
		} else if (isOut > 1200) {
			Ext.Msg.alert(i_tip, i_propseLength);
			return false;
		}
	}
	Ext.Ajax.request( {
		form : 'editproposeFormId',
		dataType : "json",
		success : function(msg) {
			// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		if (msg.responseText == '0') {
			Ext.Msg.alert(i_tip, i_rtnlPropseEditing);
		} else if (msg.responseText == '1') {
			document.getElementById("eidtbg").style.display = "none";
			document.getElementById("eidtshow").style.display = "none";
			tabNavigationProposeBoard(0, '1');
		}
	}
	})
}
/**删除合理化建议下的回复内容、回复人、回复时间*/
function deleteProposeContent(id) {
	$
			.ajax( {
				type : 'post',
				url : 'deleteProposeReplyContent.action',
				data : {
					rtnlProposeId : id
				},
				success : function(msg) {
					if (msg == '0') {
						Ext.Msg.alert(i_tip, i_rtnlPropseEditing);
					} else if (msg == '1') {
						document.getElementById("spanContentId" + id).innerHTML = '';
						document.getElementById("textContentId" + id).value = '';
						document.getElementById("replyNameShow" + id).innerHTML = '';
						document.getElementById("replyTimeShow" + id).innerHTML = '';
						document.getElementById("deleteReplyHrefId" + id).innerHTML = '';
						document.getElementById("replyFlagShow" + id).style.display = "none";
					}
				}
			});
}
function proposeSearchReset() {
	// 流程名称
	document.getElementById("processName").value = "";
	// 流程编号
	document.getElementById("processNumber").value = "";
	// 责任部门名称
	document.getElementById("orgDutyName_rtnlPropose_name").value = "";
	// 流程责任人名称
	document.getElementById("flowResPeople_rtnlPropose_name").value = "";
	// 提交人名称
	document.getElementById("submitPeople_rtnlPropose_name").value = "";
	// 开始时间
	document.getElementById("startTime").value = "";
	// 结束时间
	document.getElementById("endTime").value = "";
	// 责任部门ID
	document.getElementById("orgDutyId_rtnlPropose_id").value = "-1";
	// 流程责任人ID 
	document.getElementById("flowResPeople_rtnlPropose_id").value = "-1";
	// 提交人ID
	document.getElementById("submitPeople_rtnlPropose_id").value = "-1";
	//我处理的建议
	document.getElementById("handleProposeId").checked = false;
	//我的建议
	document.getElementById("submitProposeId").checked = false;
	//合理化建议类型
	document.getElementById("rtnlType_id").value = "-1";
}
/**提交建议点击添加附件按钮显示上传文件*/
function addFilePropose() {
	document.getElementById("addUploadFileSpan").style.display = "block";
}
/***
 * 提交建议点击删除 隐藏文件File控件
 */
function hideAddFileInput() {
	document.getElementById("addUploadFileSpan").style.display = "none";
	var file = document.getElementById("upfileProposeId");
	if (file.outerHTML) {
		file.outerHTML = file.outerHTML;
	}
}
/***
 * 编辑建议===删除原有建议
 */
function deleteProposeFileU() {
	document.getElementById("updateUploadFileNameId").style.display = "none";
	document.getElementById("isDeleteFileId").value = 1;
}
/****
 * 编辑建议===点击更新附件
 */
function updateFilePropose() {
	document.getElementById("updateUploadFileSpan").style.display = "block";
}
/***
 * 编辑建议===(删除)隐藏从本地上传的附件
 */
function hideUpdateFileInput() {
	document.getElementById("updateUploadFileSpan").style.display = "none";
	document.getElementById("isDeleteFileId").value = 1;
	var file = document.getElementById("editUpfileProposeId");
	if (file.outerHTML) {
		file.outerHTML = file.outerHTML;
	}
}


/**
 * 合理化建议-详细信息搜索
 */
function proposeDetailSearch(){
	
	//人员id
	var personIds = $("#personId_access_id_detail").val();
	var orgIds = $("#orgId_access_id_detail").val();
	var startTime=$("#startTime3").val();
	var endTime=$("#endTime3").val();
	
	//校验参数
	//校验参数
	if(!checkProposeDetailParm(personIds,orgIds,startTime,endTime)){
		
		return;
	}
	
	var proposeDetailGridStore=Ext.getCmp("proposeDetailGridPanel_id").store;
	
	proposeDetailGridStore.setBaseParam("personIds", personIds);
	proposeDetailGridStore.setBaseParam("orgIds", orgIds);
	proposeDetailGridStore.setBaseParam("startTime", startTime);
	proposeDetailGridStore.setBaseParam("endTime", endTime);

	proposeDetailGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		},callback:function(r,options,success){ 
		}
		
	});
}



/**
 * 合理化建议-创建人搜索
 */
function proposeCreateSearch(){
	
	//人员id
	var personIds = $("#personId_access_id_create").val();
	var startTime=$("#startTime1").val();
	var endTime=$("#endTime1").val();
	
	if(!checkProposeParm(personIds,startTime,endTime)){
		
		return;
	}
	
	var proposeCreateGridStore=Ext.getCmp("proposeCreateGridPanel_id").store;
	
	proposeCreateGridStore.setBaseParam("personIds", personIds);
	proposeCreateGridStore.setBaseParam("startTime", startTime);
	proposeCreateGridStore.setBaseParam("endTime", endTime);
	proposeCreateGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		},callback:function(r,options,success){

			if(success)
			{
				var pngUrl=proposeCreateGridStore.reader.jsonData.imageUrl;
				//从store中获得图片的url
	           $("#proposeImage").attr("src",basePath+pngUrl+"?temp="+Date.parse(new Date()));
	           //显示图片
				$("#proposeImage").css("display","");
			}else if(!success){
				 $("#proposeImage").attr("src","");
				
			}
		}
		
	});
}
/**
 * 合理化建议-部门搜索
 */
function proposeOrgSearch(){
	
	//人员id
	var orgIds = $("#personId_access_id_org").val();
	var startTime=$("#startTime2").val();
	var endTime=$("#endTime2").val();
	
	if(!checkProposeOrgParm(orgIds,startTime,endTime)){
		
		return;
	}
	
	var proposeOrgGridStore=Ext.getCmp("proposeOrgGridPanel_id").store;
	
	proposeOrgGridStore.setBaseParam("orgIds", orgIds);
	proposeOrgGridStore.setBaseParam("startTime", startTime);
	proposeOrgGridStore.setBaseParam("endTime", endTime);
	proposeOrgGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		},callback:function(r,options,success){

			if(success)
			{
				var pngUrl=proposeOrgGridStore.reader.jsonData.imageUrl;
				//从store中获得图片的url
	           $("#proposeOrgImage").attr("src",basePath+pngUrl+"?temp="+Date.parse(new Date()));
	           //显示图片
				$("#proposeOrgImage").css("display","");
			}else if(!success){
				 $("#proposeOrgImage").attr("src","");
				
			}
		}
		
	});
}

/**
 * 合理化建议判断参数是否合法
 * @param {Object} personIds
 * @param {Object} startTime
 * @param {Object} endTime
 * @return {TypeName} 
 */
function checkProposeParm(personIds,startTime,endTime){
	
	if(startTime==null||endTime==null||startTime.length==0||endTime.length==0){
		
		Ext.Msg.alert(i_tip,i_startTimeAndEndTimeMustNotNull);
		return false;
	}
	if (startTime > endTime) {// 结束时间大于开始时间
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip,i_startTimeIsLessEndTime);
		return false;
	}
	if(personIds==null||personIds.length==0||personIds==-1){
		
		Ext.Msg.alert(i_tip,i_pleaseSelectQueryPeople);
		return false;
	}
	
	
	return true;
}
/**
 * 合理化建议部门选择人判断参数是否合法
 * @param {Object} orgIds
 * @param {Object} startTime
 * @param {Object} endTime
 * @return {TypeName} 
 */
function checkProposeOrgParm(orgIds,startTime,endTime){
	
	if(startTime==null||endTime==null||startTime.length==0||endTime.length==0){
		
		Ext.Msg.alert(i_tip,i_startTimeAndEndTimeMustNotNull);
		return false;
	}
	if (startTime > endTime) {// 结束时间大于开始时间
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip,i_startTimeIsLessEndTime);
		return false;
	}
	if(orgIds==null||orgIds.length==0||orgIds==-1){
		
		Ext.Msg.alert(i_tip,i_pleaseSelectQueryOrg);
		return false;
	}
	
	
	return true;
}


/**
 * 合理化建议详细信息查询页面判断参数是否合法
 * @param {Object} personIds
 * @param {Object} startTime
 * @param {Object} endTime
 * @return {TypeName} 
 */
function checkProposeDetailParm(personIds,orgIds,startTime,endTime){
	
	if(startTime==null||endTime==null||startTime.length==0||endTime.length==0){
		
		Ext.Msg.alert(i_tip,i_startTimeAndEndTimeMustNotNull);
		return false;
	}
	if (startTime > endTime) {// 结束时间大于开始时间
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip,i_startTimeIsLessEndTime);
		return false;
	}
	if((personIds==null||personIds.length==0||personIds==-1)&&(orgIds==null||orgIds.length==0||orgIds==-1)){
		
		Ext.Msg.alert(i_tip,i_queryParmsIsNotNull);
		return false;
	}
	
	
	return true;
}

/**
 * 合理化建议-详情重置
 */
function proposeDetailReset(){

	//清除隐藏域中的值
	clearInputValue("proposeDetailForm");
	//清除页面显示的内容
	$("#select_person_id_detail").text("");
	$("#select_organization_id_detail").text("");
	$("#startTime3").attr("value",'');
	$("#endTime3").attr("value",'');
	
}

//合理化建议-提交人重置
function proposeCreateReset(){

	//清除隐藏域中的值
	clearInputValue("proposeCreateForm");
	//清除页面显示的内容
	$("#select_person_id_create").text("");
	$("#startTime1").attr("value",'');
	$("#endTime1").attr("value",'');
	
}
//合理化建议-部门重置
function proposeOrgReset(){

	//清除隐藏域中的值
	clearInputValue("proposeOrgForm");
	//清除页面显示的内容
	$("#select_org_id").text("");
	$("#startTime2").attr("value",'');
	$("#endTime2").attr("value",'');
	
}

/**
 * 合理化建议统计
 */
function rtnlProposeCountInit(){
	

		var rtnlProposeCountTabPanel =new Ext.TabPanel(
			{  
				border : false,
				activeTab : 0,
				id : 'rtnlProposeCount_id',
				height : Ext.getCmp("main-panel").getInnerHeight(),
				renderTo : 'rtnlProposeCount',
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				items : [
						{
							title : i_submitPeopleQuery,// 提交人查询
							id : "proposeCreateSearch_id",
							//layout: 'fit',
							autoLoad : {
								url : basePath
										+ "jsp/propose/rtnlProposeCountCreate.jsp",
								scripts : true,
								callback : function(el, success, response) {
							
							        //初始化grid
								    initProposeCreatePanel();
				
								}
							}
						},
						{
							title : i_orgQuery,// 部门查询
							id : "proposeOrgSearch_id",
							//layout: 'fit',
							autoLoad : {
								url : basePath
										+ "jsp/propose/rtnlProposeCountOrg.jsp",
								scripts : true,
								callback : function(el, success, response) {
								
							     initProposeOrgPanel();
				
								}
								
							}
							
						},
						{
							title : i_detailInfoQuery,// 部门查询
							id : "proposeDetailSearch_id",
							//layout: 'fit',
							autoLoad : {
								url : basePath
										+ "jsp/propose/rtnlProposeCountDetail.jsp",
							scripts : true,
							callback : function(el, success, response) {
							
			                 initProposeDetailPanel();
							}
								
							}
							
						}
					],listeners : {
								'tabchange' : function(p, width, height) {
					
										if(p.getActiveTab() == null){
											return;
										}
										//TODO
								  //如果tab页面加载完成的情况下
                                  if(Ext.getCmp("proposeOrgGridPanel_id")!=null&&Ext.getCmp("proposeCreateGridPanel_id")!=null&&Ext.getCmp("proposeDetailGridPanel_id")!=null){
                                	  
                                	   proposeTabResize();
                                	  
                                  }
								
								}
							}
	
		});
}

/**
 * 合理化建议的详细信息统计panel
 */
function initProposeDetailPanel(){
	
		//流程访问数统计
	var proposeDetailGridStore = new Ext.data.Store({
				
				proxy : new Ext.data.HttpProxy({
							url : "queryProposeDetail.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root"
						}, [ {
								name : "createName",
								mapping : "createName"
							}, {
								name : "orgName",
								mapping : "orgName"
							}, {
								name : "relateName",
								mapping : "relateName"
							}, {
								name : "rtnlContent",
								mapping : "rtnlContent"
							}, {
								name : "rtnlContent",
								mapping : "rtnlContent"
							}, {
								name : "createTime",
								mapping : "createTime"
							}
							])

			});

	var proposeDetailGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), 
			{
				header : i_submitPeople,
				dataIndex : "createName",
				align : "center",
				width:20,
				menuDisabled : true
			}, {
				header : i_orgName,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true,
				width:40,
				sortable : true
			}, {
				header : i_name,
				dataIndex : "relateName",
				align : "center",
				width:40,
				menuDisabled : true
			}, {
				header : i_proposeContent,
				dataIndex : "rtnlContent",
				align : "center",
				menuDisabled : true
			}, {
				header : i_time,
				dataIndex : "createTime",
				align : "center",
				width:30,
				menuDisabled : true
			}
	]);
	var proposeDetailGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : proposeDetailGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	var proposeGridPanel = new Ext.grid.GridPanel({
				id : "proposeDetailGridPanel_id",
				renderTo : "proposeDetail_id",
				height :  Ext.getCmp("main-panel").getInnerHeight()-220,
				layout:"fit",
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : proposeDetailGridStore,
				colModel : proposeDetailGridColumnModel,
				bbar : proposeDetailGridBbar
			})
	 
}



/**
 * 合理化建议的创建人采纳率统计panel
 */
function initProposeCreatePanel(){
	
	var clientWidth=document.body.clientWidth;
	var clientHeight=document.body.clientHeight;
	$("#proposeCreateShowDiv").width(clientWidth-24);
	$("#proposeCreateShowDiv").height(clientHeight-245);
	var gridPanelHeight=(clientHeight - 290)>400?clientHeight - 290:400;
		//流程访问数统计
	 var proposeCreateGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "queryProposeCreate.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							imageUrl:"imageUrl"
						}, [{
								name : "id",
								mapping : "id"
							}, {
								name : "sheetName",
								mapping : "sheetName"
							}, {
								name : "total",
								mapping : "total"
							}, {
								name : "acceptPercentStr",
								mapping : "acceptPercentStr"
							}, {
								name : "strDate",
								mapping : "strDate"
							}])

			});

	var proposeGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : "",
				dataIndex : "id",
				align : "center",
				menuDisabled : true,
				hidden:true,
				visable:false
			}, {
				header : i_submitPeople,
				dataIndex : "sheetName",
				align : "center",
				width:60,
				menuDisabled : true
			}, {
				header : i_total,
				dataIndex : "total",
				align : "center",
				menuDisabled : true,
				width:50,
				sortable : true
			}, {
				header : i_acceptPercent,
				dataIndex : "acceptPercentStr",
				align : "center",
				width:50,
				menuDisabled : true
			}, {
				header : i_time,
				dataIndex : "strDate",
				align : "center",
				menuDisabled : true
			}
	]);
	var proposeGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : proposeCreateGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	var proposeGridPanel = new Ext.grid.GridPanel({
				id : "proposeCreateGridPanel_id",
				renderTo : 'proposeCreate_id',
				height :  gridPanelHeight,
				width:400,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : proposeCreateGridStore,
				colModel : proposeGridColumnModel,
				bbar : proposeGridBbar
			})
	 
}
/**
 * 合理化建议的部门的采纳率统计panel
 */
function initProposeOrgPanel(){
	
	var clientWidth=document.body.clientWidth;
	var clientHeight=document.body.clientHeight;
	$("#proposeOrgShowDiv").width(clientWidth-24);
	$("#proposeOrgShowDiv").height(clientHeight-245);
	var gridPanelHeight=(clientHeight - 290)>400?clientHeight - 290:400;
		//流程访问数统计
	 var proposeOrgGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "queryProposeOrg.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							imageUrl:"imageUrl"
						}, [{
								name : "id",
								mapping : "id"
							}, {
								name : "sheetName",
								mapping : "sheetName"
							}, {
								name : "total",
								mapping : "total"
							}, {
								name : "acceptPercentStr",
								mapping : "acceptPercentStr"
							}, {
								name : "strDate",
								mapping : "strDate"
							}])

			});

	var proposeGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : "",
				dataIndex : "id",
				align : "center",
				menuDisabled : true,
				hidden:true,
				visable:false
			}, {
				header : i_org,
				dataIndex : "sheetName",
				align : "center",
				width:60,
				menuDisabled : true
			}, {
				header : i_total,
				dataIndex : "total",
				align : "center",
				menuDisabled : true,
				width:50,
				sortable : true
			}, {
				header : i_acceptPercent,
				dataIndex : "acceptPercentStr",
				align : "center",
				width:50,
				menuDisabled : true
			}, {
				header : i_time,
				dataIndex : "strDate",
				align : "center",
				menuDisabled : true
			}
	]);
	var proposeGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : proposeOrgGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	var proposeGridPanel = new Ext.grid.GridPanel({
				id : "proposeOrgGridPanel_id",
				renderTo : 'proposeOrg_id',
				height :  gridPanelHeight,
				width:400,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : proposeOrgGridStore,
				colModel : proposeGridColumnModel,
				bbar : proposeGridBbar
			})
	 
}

//下载创建人的合理化建议统计报表
function proposeCreateDownloadExcel(){
	
	//人员id
	var personIds = $("#personId_access_id_create").val();
	var startTime=$("#startTime1").val();
	var endTime=$("#endTime1").val();
	
	if(!checkProposeParm(personIds,startTime,endTime)){
		
		return;
	}
	
    window.open("downloadProposeCreateCount?personIds="+personIds+"&startTime="+startTime+"&endTime="+endTime);
	
}
//下载创建人的合理化建议统计报表
function proposeOrgDownloadExcel(){
	
	//组织id
	var orgIds = $("#personId_access_id_org").val();
	var startTime=$("#startTime2").val();
	var endTime=$("#endTime2").val();
	
	if(!checkProposeOrgParm(orgIds,startTime,endTime)){
		
		return;
	}
	
    window.open("downloadProposeOrgCount?orgIds="+orgIds+"&startTime="+startTime+"&endTime="+endTime);
	
	
}

//合理化建议-详细信息下载
function proposeDetailDownloadExcel(){
	
	//人员id
	var personIds = $("#personId_access_id_detail").val();
	var orgIds = $("#orgId_access_id_detail").val();
	var startTime=$("#startTime3").val();
	var endTime=$("#endTime3").val();
	
	//校验参数
	if(!checkProposeDetailParm(personIds,orgIds,startTime,endTime)){
		
		return;
	}
	
	 window.open("downloadProposeDetailCount?personIds="+personIds+"&orgIds="+orgIds+"&startTime="+startTime+"&endTime="+endTime);
	
	
}

/**
 * 合理化建议创建人查询，部门查询，详细信息查询tab页设置大小
 * @return {TypeName} 
 */
function proposeTabResize(){
	
	     var rtnlProposeCountTabPanel=Ext.getCmp("rtnlProposeCount_id");
     	 if (rtnlProposeCountTabPanel==null||rtnlProposeCountTabPanel.getActiveTab() == null) {
			 return;
		   }
		   
		   //获得浏览器宽度
		   var clientWidth=document.body.clientWidth;
		   var clientHeight=document.body.clientHeight;
		   rtnlProposeCountTabPanel.setSize(clientWidth, clientHeight - 100);
			//设置宽度
		   Ext.getCmp("proposeTabPanel_id").setSize(clientWidth, clientHeight - 80);
		   var gridPanelHeight=(clientHeight - 290)>400?clientHeight - 290:400;
		if (rtnlProposeCountTabPanel.getActiveTab().getId() == 'proposeCreateSearch_id') {
			
			Ext.getCmp("proposeCreateSearch_id").setSize(clientWidth, clientHeight - 110);
			Ext.getCmp("proposeCreateGridPanel_id").setSize(400, gridPanelHeight);
			
			
			$("#proposeCreateShowDiv").width(clientWidth);
			$("#proposeCreateShowDiv").height(clientHeight-245);
			
		} else if(rtnlProposeCountTabPanel.getActiveTab().getId() == 'proposeOrgSearch_id'){
		   
			Ext.getCmp("proposeOrgSearch_id").setSize(clientWidth, clientHeight - 110);
			Ext.getCmp("proposeOrgGridPanel_id").setSize(400, gridPanelHeight);
			
			$("#proposeOrgShowDiv").width(clientWidth);
			$("#proposeOrgShowDiv").height(clientHeight-245);
		
		}else if(rtnlProposeCountTabPanel.getActiveTab().getId() == 'proposeDetailSearch_id'){
			
			Ext.getCmp("proposeDetailSearch_id").setSize(clientWidth, clientHeight - 110);
			Ext.getCmp("proposeDetailGridPanel_id").setSize(clientWidth-25, clientHeight - 310);
		}
	
	
	
}

