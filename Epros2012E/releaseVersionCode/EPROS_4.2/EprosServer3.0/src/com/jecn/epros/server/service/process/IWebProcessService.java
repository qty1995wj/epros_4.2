package com.jecn.epros.server.service.process;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.download.ProcessMapDownloadBean;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.KeyValueBean;
import com.jecn.epros.server.webBean.PublicFileBean;
import com.jecn.epros.server.webBean.PublicSearchBean;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.process.ActiveRoleBean;
import com.jecn.epros.server.webBean.process.JecnRoleProcessBean;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;
import com.jecn.epros.server.webBean.process.ProcessAttributeWebBean;
import com.jecn.epros.server.webBean.process.ProcessBaseInfoBean;
import com.jecn.epros.server.webBean.process.ProcessMapBaseInfoBean;
import com.jecn.epros.server.webBean.process.ProcessMapWebBean;
import com.jecn.epros.server.webBean.process.ProcessSearchBean;
import com.jecn.epros.server.webBean.process.ProcessShowFile;
import com.jecn.epros.server.webBean.process.ProcessSystemListBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.process.RelatedProcessRuleStandardBean;

public interface IWebProcessService extends IBaseService<JecnFlowStructureT, Long> {

	/**
	 * 
	 * 
	 * @param processId
	 * @param isPub
	 *            true正式表，false临时表
	 * @return
	 * @throws Exception
	 */
	public ProcessBaseInfoBean getProcessBaseInfoBean(Long processId, boolean isPub) throws Exception;

	/**
	 * @author yxw 2013-4-1
	 * @description:
	 * @param processId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowStructure getJecnFlowStructure(Long processId) throws Exception;

	/**
	 * 流程-操作模板
	 * 
	 * @param processId
	 * @param isPub
	 *            true正式表，false临时表
	 * @return
	 * @throws Exception
	 */
	public List<ProcessActiveWebBean> getProcessActiveBeanList(Long processId, boolean isPub) throws Exception;

	/**
	 * 获得流程属性
	 * 
	 * @param processId
	 * @param isPub
	 *            true正式表，false临时表
	 * @return
	 * @throws Exception
	 */
	public ProcessAttributeWebBean getProcessAttributeWebBean(Long processId, boolean isPub) throws Exception;

	/**
	 * 
	 * @author yxw 2012-11-28
	 * @description:浏览端树加载 获取子节点
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<WebTreeBean> getChildProcessForWeb(Long pid, Long projectId, boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:公共文件查询总数
	 * @param publicSearchBean搜索条件bean
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public int getTotalPublicProcess(PublicSearchBean publicSearchBean, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:公共文件查询
	 * @param publicSearchBean搜索条件bean
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<PublicFileBean> getPublicProcess(PublicSearchBean publicSearchBean, int start, int limit, Long projectId)
			throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的流程 总数
	 * @param attenTionSearchBean
	 *            查询条件
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public int getTotalAttenTionProcess(AttenTionSearchBean attenTionSearchBean, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-5
	 * @description:我关注的流程
	 * @param attenTionSearchBean
	 *            查询条件
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> getAttenTionProcess(AttenTionSearchBean attenTionSearchBean, int start, int limit,
			Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-25
	 * @description:查询我参与的流程
	 * @param userId
	 *            用户id
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnRoleProcessBean> getMyJoinProcess(Long userId, String processName, String processNum, Long posId,
			Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-26
	 * @description:流程搜索查询总数
	 * @param processSearchBean
	 *            搜索条件
	 * @return
	 * @throws Exception
	 */
	public int getTotalSearchProcess(ProcessSearchBean processSearchBean, long peopleId, long projectId, boolean isAdmin)
			throws Exception;

	/**
	 * @author yxw 2012-12-25
	 * @description: 流程搜索
	 * @param processSearchBean
	 *            搜索条件bean
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> searchProcess(ProcessSearchBean processSearchBean, int start, int limit, long peopleId,
			long projectId, boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2012-12-26
	 * @description:流程地图搜索查询总数
	 * @param processSearchBean
	 *            搜索条件
	 * @return
	 * @throws Exception
	 */
	public int getTotalSearchProcessMap(ProcessSearchBean processSearchBean, long peopleId, long projectId,
			boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2012-12-25
	 * @description: 流程地图搜索
	 * @param processSearchBean
	 *            搜索条件bean
	 * @return
	 * @throws Exception
	 */
	public List<ProcessMapWebBean> searchProcessMap(ProcessSearchBean processSearchBean, int start, int limit,
			long peopleId, long projectId, boolean isAdmin) throws Exception;

	/**
	 * @author yxw 2013-1-17
	 * @description:查询流程操作说明
	 * @param processId
	 *            流程ID
	 * @param isPub
	 *            true正式表，false临时表
	 * @return
	 * @throws Exception
	 */
	public ProcessShowFile getProcessShowFile(Long processId, boolean isPub) throws Exception;

	/**
	 * @author yxw 2013-1-17
	 * @description:流程地图操作说明数据
	 * @param processId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */

	public ProcessMapDownloadBean getProcessMapDownloadBean(Long processId) throws Exception;

	/**
	 * @author yxw 2013-1-17
	 * @description:流程文件控信息
	 * @param processId流程ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnTaskHistoryNew> getTaskHistoryNew(Long processId) throws Exception;

	/**
	 * @author yxw 2013-1-17
	 * @description:
	 * @param processId
	 * @return
	 * @throws Exception
	 */
	public RelatedProcessRuleStandardBean getRelatedProcessRuleStandard(Long processId, boolean isPub) throws Exception;

	/**
	 * 查询流程处在的级别
	 * 
	 * @author fuzhh 2013-12-5
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public int findFlowDepth(Long flowId) throws Exception;

	/**
	 * @author yxw 2013-1-18
	 * @description:流程清单
	 * @param processMapId
	 *            流程地图ID
	 * @param isAll
	 *            是否显示全部
	 * @return
	 * @throws Exception
	 */
	public ProcessSystemListBean getProcessSystemList(Long processMapId, Long projectId, int depth, boolean isAll)
			throws Exception;

	/**
	 * @author yxw 2013-1-18
	 * @description:流程地图概况信息
	 * @param processMapId流程地图ID
	 * @param isPub
	 *            true正式表，false临时表
	 * @return
	 * @throws Exception
	 */
	public ProcessMapBaseInfoBean getProcessMapBaseInfo(Long processMapId, boolean isPub) throws Exception;

	/**
	 * @author yxw 2013-1-21
	 * @description:部门主导流程获取总行数
	 * @param processSearchBean
	 *            部门记导流程查询条件 条件只包括 流程编号 流程名称 责任部门 项目ID
	 * @return
	 * @throws Exception
	 */
	public int getTotalSearchDeptProcess(ProcessSearchBean processSearchBean) throws Exception;

	/**
	 * @author yxw 2013-1-21
	 * @description:部门主导流程集合
	 * @param processSearchBean
	 *            部门记导流程查询条件
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> searchDeptProcess(ProcessSearchBean processSearchBean, int start, int limit)
			throws Exception;

	/**
	 * @author yxw 2013-1-21
	 * @description:我的工作活动
	 * @param activeName
	 *            活动名称
	 * @param processName
	 *            流程名称
	 * @param processNum
	 *            流程编号
	 * @param startDate
	 *            开始时间
	 * @param endDate
	 *            结束时间
	 * @return
	 * @throws Exception
	 */
	public List<ProcessActiveWebBean> searchMyActive(String activeName, String processName, String processNum,
			String startDate, String endDate, Long peopleId, Long projectId) throws Exception;

	/**
	 * 操作说明下载 DOC
	 * 
	 * @author fuzhh Jan 21, 2013
	 * @param refId
	 * @param nodeType
	 * @param historyId 
	 * @throws Exception
	 */
	public JecnCreateDoc printFlowDoc(Long refId, TreeNodeType nodeType, boolean isPub, Long userId, Long historyId) throws Exception;
	
	/**
	 * 操作说明下载 PDF
	 * 
	 * @author zhr 
	 * @param refId
	 * @param nodeType
	 * @param historyId 
	 * @throws Exception
	 */
	public JecnCreateDoc printFlowPDF(Long refId, TreeNodeType nodeType, boolean isPub, Long userId, Long historyId) throws Exception;

	/**
	 * 文控信息版本记录
	 * 
	 * @param Id
	 *            ID
	 * @return List<JecnTaskHistoryNew>
	 * @throws Exception
	 */
	public JecnTaskHistoryNew getJecnTaskHistoryNew(Long id) throws Exception;

	/**
	 * 通过流程名称搜索流程
	 * 
	 * @author fuzhh Feb 26, 2013
	 * @param processName
	 *            流程名称
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchByName(String processName, Long projectId) throws Exception;

	/**
	 * @author yxw 2013-2-28
	 * @description:我的工作活动及指导书总数
	 * @param processId
	 *            流程ID
	 * @param activeId
	 *            活动ID
	 * @param fileName
	 *            文件名称
	 * @param userId
	 *            用户ID
	 * @return
	 * @throws Exception
	 */
	public int getTotalMyWorkModel(Long processId, Long activeId, String fileName, Long userId, Long projectId)
			throws Exception;

	/**
	 * @author yxw 2013-2-28
	 * @description:我的工作活动及指导书列表
	 * @param processId
	 *            流程ID
	 * @param activeId
	 *            活动ID
	 * @param fileName
	 *            文件名称
	 * @param userId
	 *            用户ID
	 * @param start
	 *            开始行数
	 * @param limit
	 *            每页显示条数
	 * @return
	 * @throws Exception
	 */
	public List<FileWebBean> searchMyWorkModel(Long processId, Long activeId, String fileName, Long userId,
			Long projectId, int start, int limit) throws Exception;

	/**
	 * @author yxw 2013-3-1
	 * @description:我参与的流程列表
	 * @param userId
	 *            用户ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<KeyValueBean> getMyJoinProcessList(Long userId, Long projectId) throws Exception;

	/**
	 * @author yxw 2013-3-1
	 * @description:根据用户与流程ID查询活动
	 * @param userId
	 *            用户ID
	 * @param processId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	public List<KeyValueBean> getActiveListByProcessId(Long userId, Long processId) throws Exception;

	/**
	 * 获得有效期邮件提醒内容
	 * @param type 
	 */
	public void sendOrgEmail(int type) throws Exception;

	/**
	 * 查询活动角色关联数据
	 * 
	 * @author fuzhh Apr 25, 2013
	 * @param flowId
	 *            流程ID
	 * @param isPub
	 *            是否发布 true为已发布 false为未发表
	 * @return
	 * @throws Exception
	 */
	public ActiveRoleBean getProcessActiveWeb(long flowId, boolean isPub) throws Exception;

	/**
	 * 获取流程检错信息
	 * 
	 * @author fuzhh 2013-9-9
	 * @param processCheckType
	 *            检错类型
	 * @return
	 * @throws Exception
	 */
	public List<ProcessBaseInfoBean> getProcessCheckList(long processCheckType, int start, int limit, Long projectId)
			throws Exception;

	/**
	 * 获取流程检错信息大小
	 * 
	 * @author fuzhh 2013-9-9
	 * @param processCheckType
	 *            检错类型
	 * @return
	 * @throws Exception
	 */
	public int getProcessCheckCount(long processCheckType, Long projectId) throws Exception;

	/**
	 * @author 2014-04-22
	 * @description:数据我关注的流程,我参与的流程
	 * @param start
	 *            开始行数
	 * @param limit
	 *            显示条数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<ProcessWebBean> getButtProcess(int start, int limit, Long projectId, Long userId) throws Exception;

	/**
	 * @author 2014-04-22
	 * @description:数据我关注的流程,我参与的流程 查询条件
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public int getTotalButtProcess(Long projectId, Long userId) throws Exception;

	/**
	 * 根据关联的元素获得支持工具（正式表）
	 * 
	 * @param relatedId
	 *            关联的id
	 * @param type
	 *            类型
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowSustainTool> getSustainTools(long relatedId, int type) throws Exception;

	Map<EXCESS, List<FileOpenBean>> getFlowFilesDownLoad(Long flowId, boolean isPub, Long peopleId) throws Exception;
}
