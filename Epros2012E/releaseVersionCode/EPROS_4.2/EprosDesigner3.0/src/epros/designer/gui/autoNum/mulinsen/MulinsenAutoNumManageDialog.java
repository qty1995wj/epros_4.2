package epros.designer.gui.autoNum.mulinsen;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.autoNum.AutoNumJecnTree;
import epros.designer.gui.autoNum.AutoNumManageDialog;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 流程、制度、文件自动编号管理页面
 * 
 * @author ZXH
 * 
 */
public class MulinsenAutoNumManageDialog extends JecnDialog {
	protected static Logger log = Logger.getLogger(AutoNumManageDialog.class);
	/** 主面板 */
	protected JPanel mainPanel = null;

	protected JecnPanel buttonPanel = null;

	/** L3 auto_productionLine */
	protected AutoTreeScroolPanel codeL3ItemScroolPanel = null;
	/** 公司代码 auto_company */
	protected AreaScrollPanel companyScrollPanel = null;
	/** L2 */
	protected AreaScrollPanel codeL2ScrollPanel = null;
	/** 文件类别 process_level */
	protected AreaScrollPanel fileTypeScrollPanel = null;

	protected JButton okButton = null;
	protected JButton calcelButton = null;

	/** 公司代码 */
	protected JecnConfigItemBean companyItem;
	/** L2 */
	protected JecnConfigItemBean codeL2ItemItem;
	protected JecnConfigItemBean fileTypeItem;

	/** 树面板 */
	protected JScrollPane treePanel = null;

	public MulinsenAutoNumManageDialog() {
		initData();
		initCompotents();
		initLayout();
		initCompotentsValue();
	}

	protected void initData() {
		// 公司代码
		companyItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_companyCode);
		// L2
		codeL2ItemItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_productionLine);
		fileTypeItem = JecnConfigTool.getItemBean(ConfigItemPartMapMark.auto_fileType);
		if (companyItem == null || codeL2ItemItem == null || fileTypeItem == null) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("accessServerError"));
			log.error(" MulinsenAutoNumManageDialog initData!" + fileTypeItem);
			throw new IllegalArgumentException("MulinsenAutoNumManageDialog initData!");
		}
	}

	protected void initCompotentsValue() {
		companyScrollPanel.getTextArea().setText(companyItem.getValue());
		fileTypeScrollPanel.getTextArea().setText(fileTypeItem.getValue());
		codeL2ScrollPanel.getTextArea().setText(codeL2ItemItem.getValue());
	}

	protected void initCompotents() {
		mainPanel = new JPanel();
		// 设置Dialog大小
		this.setSize(755, 430);
		this.setResizable(false);
		this.setTitle(JecnProperties.getValue("autoCode"));//自动编号管理
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		buttonPanel = new JecnPanel();
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		companyScrollPanel = new AreaScrollPanel();

		codeL3ItemScroolPanel = new AutoTreeScroolPanel("L3", 5);
		fileTypeScrollPanel = new AreaScrollPanel();
		codeL2ScrollPanel = new AreaScrollPanel();

		okButton = new JButton(JecnProperties.getValue("okBtn"));
		calcelButton = new JButton(JecnProperties.getValue("cancelBtn"));

		companyScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), companyItem
				.getName(JecnResourceUtil.getLocale())));

		fileTypeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				fileTypeItem.getName(JecnResourceUtil.getLocale())));
		codeL2ScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), codeL2ItemItem
				.getName(JecnResourceUtil.getLocale())));
		this.getContentPane().add(mainPanel);

		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		calcelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MulinsenAutoNumManageDialog.this.dispose();
			}
		});
	}

	protected void okButtonAction() {
		companyItem.setValue(companyScrollPanel.getTextArea().getText());

		fileTypeItem.setValue(fileTypeScrollPanel.getTextArea().getText());

		codeL2ItemItem.setValue(codeL2ScrollPanel.getTextArea().getText());

		List<JecnConfigItemBean> itemBeans = new ArrayList<JecnConfigItemBean>();
		getOkButtonItems(itemBeans);
		JecnConfigTool.updateItemBeans(itemBeans);
		this.dispose();
	}

	protected void getOkButtonItems(List<JecnConfigItemBean> itemBeans) {
		itemBeans.add(companyItem);
		itemBeans.add(codeL2ItemItem);
		itemBeans.add(fileTypeItem);
	}

	private void initLayout() {

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 0, 5);

		// other
		addOtherPanel(c, insets);

		c = new GridBagConstraints(0, 1, 6, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		mainPanel.add(buttonPanel, c);

		// 添加按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okButton);
		buttonPanel.add(calcelButton);
	}

	protected void addOtherPanel(GridBagConstraints c, Insets insets) {
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(companyScrollPanel, c);
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(codeL2ScrollPanel, c);
		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(codeL3ItemScroolPanel, c);
		c = new GridBagConstraints(4, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(fileTypeScrollPanel, c);
	}

	class AreaScrollPanel extends JScrollPane {
		private JTextArea textArea = null;

		public AreaScrollPanel() {
			textArea = new JTextArea();
			// 名称输入框
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
			textArea.setBorder(BorderFactory.createLineBorder(Color.gray));
			textArea.setBorder(null);

			Dimension dimension = new Dimension(150, 370);
			this.setMinimumSize(dimension);
			this.setViewportView(textArea);
			// 名称输入区域容器
			this.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		}

		public JTextArea getTextArea() {
			return textArea;
		}
	}

	class AutoTreeScroolPanel extends JScrollPane {
		/** 自动编号tree面板 */
		protected AutoNumJecnTree jecnTree = null;

		public AutoTreeScroolPanel(String title, int treeType) {
			jecnTree = getTree(treeType);
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 边框
			// 树滚动面板
			this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title));
			this.setViewportView(jecnTree);
		}

		public AutoNumJecnTree getJecnTree() {
			return jecnTree;
		}
	}

	/**
	 * 
	 * @param treeType
	 *            5 木林森树形结构 L3
	 * @return
	 */
	private AutoNumJecnTree getTree(int treeType) {
		return new MulinsenProcessMapCodeAutoTree(treeType);
	}

}
