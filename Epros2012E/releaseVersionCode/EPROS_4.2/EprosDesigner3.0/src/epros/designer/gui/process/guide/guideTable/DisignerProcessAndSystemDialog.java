package epros.designer.gui.process.guide.guideTable;

import java.util.Vector;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.guide.ProcessAndSystemDialog;
import epros.draw.gui.operationConfig.OrderTable;
import epros.draw.gui.operationConfig.RealtedRuleTable;

/**
 * 相关流程及制度,标准
 * 
 * @author 2012-07-04
 * 
 */
public class DisignerProcessAndSystemDialog extends ProcessAndSystemDialog {
	/** 制度表格 */
	private RealtedRuleTable realtedRuleTable;
	/** 标准表格 */
	private OrderTable orderTable;

	public DisignerProcessAndSystemDialog(Long flowId,
			RealtedRuleTable realtedRuleTable, OrderTable orderTable) {
		super(flowId);
		this.realtedRuleTable = realtedRuleTable;
		this.orderTable = orderTable;
	}

	/***************************************************************************
	 * 确定
	 */
	public void okButPerformed() {
		super.okButPerformed();
		// 相关制度
		realtedRuleTable.removeAll();
		Vector<Vector<String>> ruleContent = new Vector<Vector<String>>();
		for (JecnTreeBean treeBean : listRule) {
			Vector<String> ruleContentStr = new Vector<String>();
			// 制度ID
			ruleContentStr.add(String.valueOf(treeBean.getId()));
			// 制度编号
			ruleContentStr.add(treeBean.getNumberId());
			// 制度名称
			ruleContentStr.add(treeBean.getName());
			// 制度类型
			ruleContentStr.add(JecnDesignerCommon.ruleTypeVal(treeBean
					.getTreeNodeType()));
			ruleContent.add(ruleContentStr);
		}
		realtedRuleTable.setContent(ruleContent);
		realtedRuleTable.setTableModel();

		// 相关标准
		orderTable.removeAll();
		Vector<Vector<String>> orderContent = new Vector<Vector<String>>();
		for (JecnTreeBean treeBean : listStander) {
			Vector<String> orderContentStr = new Vector<String>();
			// 标准ID
			orderContentStr.add(String.valueOf(treeBean.getId()));
			// 标准名称
			orderContentStr.add(treeBean.getName());
			// 标准类型
			orderContentStr.add(JecnDesignerCommon.standardTypeVal(treeBean
					.getTreeNodeType()));
			orderContent.add(orderContentStr);
		}
		orderTable.setContent(orderContent);
		orderTable.setTableModel();
	}
}
