package epros.designer.gui.process.guide;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessInfoBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.system.AccessAuthorityCommon;
import epros.designer.gui.system.BusinessTypeCommon;
import epros.designer.gui.system.CategoryCommon;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.gui.system.ExpiryCommon;
import epros.designer.gui.system.FileEnclosureSelectCommon;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.gui.system.PersonliableSelectCommon;
import epros.designer.gui.system.ResDepartSelectCommon;
import epros.designer.gui.system.SupportToolsCommon;
import epros.designer.gui.system.combo.JecnSecurityLevelCommon;
import epros.designer.gui.system.field.JecnTipTextField;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程属性
 * 
 * @author 2012-07-04
 * 
 */
public class ProcessPropertyDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(ProcessPropertyDialog.class);

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();

	/** 流程名称Lab */
	private JLabel flowNameLab = new JLabel(JecnProperties.getValue("flowNameC"));
	/** 流程名称Field */
	private JecnTextField flowNameField = new JecnTextField();

	/** 流程编号Lab */
	private JLabel flowNumLab = new JLabel(JecnProperties.getValue("flowNumC"));
	/** 流程编号Field */
	private JecnTextField flowNumField = new JecnTextField();
	/** 验证提示 */
	private JLabel verfyLab = new JLabel();

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 设置大小 */
	Dimension dimension = null;

	// 流程ID===============================================自动获取
	private Long flowId = null;// 6359L;

	/** 基本信息 */
	private ProcessAttributeBean processAttributeBean = null;
	/** 流程类别 */
	private CategoryCommon categoryCommon = null;

	/** 业务类型 */
	private BusinessTypeCommon businessTypeCommon = null;
	/** 流程责任人 */
	private PersonliableSelectCommon personliableSelect = null;
	/** 流程监护人 */
	private PeopleSelectCommon guardianPeopleSelectCommon = null;
	/** 流程拟制人 */
	private PeopleSelectCommon fictionPeopleSelectCommon = null;
	/** 责任部门 */
	private ResDepartSelectCommon resDepartSelectCommon = null;

	/** 查阅权限 */
	private AccessAuthorityCommon accessAuthorityCommon = null;

	/** 支持工具 */
	private SupportToolsCommon supportToolsCommon;

	/** 附件 */
	private FileEnclosureSelectCommon fileEnclosureSelectCommon;

	private JecnTreeNode selectNode = null;
	/** 流程类别显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessCategory = 0;
	/** 流程拟制人显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessIntendedPerson = 0;
	/** 流程监护人显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessGuardian = 0;
	/** 流程附件显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessEnclosure = 0;
	/** 流程支持工具显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessSupportTools = 0;
	/** 流程业务类型显示状态 0是不显示，1是显示，2是显示和必填 */
	private int isShowProcessBussType = 0;

	/** 有效期 */
	private ExpiryCommon expiryCommon = null;

	/** 流程专员 */
	private PeopleSelectCommon commissionerPeopleSelectCommon = null;
	/** 关键字 */
	private JecnTipTextField keyWordField = null;

	public ProcessPropertyDialog(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
		this.flowId = selectNode.getJecnTreeBean().getId();
		initConfig();
		this.setSize(getWidthMax(), getCurPanelHeight());
		this.setResizable(true);
		this.setTitle(JecnProperties.getValue("flowAttrBtn"));
		this.setModal(true);
		this.setLocationRelativeTo(null);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 获取流程类别信息
		try {
			processAttributeBean = ConnectionPool.getProcessAction().getFlowAttribute(flowId);
		} catch (Exception e1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("gettingFlowAttrError"));
			log.error("ProcessPropertyDialog is error", e1);
		}

		cancelBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}

		});
		okBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}

		});
		initLayout();
	}

	/**
	 * 初始化配置
	 */
	private void initConfig() {
		this.isShowProcessCategory = JecnConfigTool.processCategoryShowType();
		this.isShowProcessIntendedPerson = JecnConfigTool.processIntendedPersonShowType();
		this.isShowProcessGuardian = JecnConfigTool.processGuardianShowType();
		this.isShowProcessSupportTools = JecnConfigTool.processSupportToolsShowType();
		this.isShowProcessEnclosure = JecnConfigTool.processEnclosureShowType();
		this.isShowProcessBussType = JecnConfigTool.processBussTypeShowType();
	}

	/**
	 * 初始化高度
	 */
	public int getCurPanelHeight() {
		int height = 460;

		// 流程类别
		if (this.isShowProcessCategory != 0) {
			height += 40;
		}
		// 流程业务类型
		if (this.isShowProcessBussType != 0) {
			height += 40;
		}
		// 拟制人
		if (this.isShowProcessIntendedPerson != 0) {
			height += 40;
		}
		// 监护人
		if (this.isShowProcessGuardian != 0) {
			height += 40;
		}
		// 支持工具
		if (this.isShowProcessSupportTools != 0) {
			height += 80;
		}
		// 附件
		if (this.isShowProcessEnclosure != 0) {
			height += 40;
		}
		return height;
	}

	private void initLayout() {
		verfyLab.setForeground(Color.red);
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());
		int rows = 0;
		// 流程名称
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(this.flowNameLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(this.flowNameField, c);
		this.flowNameField.setText(selectNode.getJecnTreeBean().getName());
		flowNameField.setEditable(false);
		rows++;
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(this.flowNumLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(this.flowNumField, c);
		this.flowNumField.setText(selectNode.getJecnTreeBean().getNumberId());

		if (JecnConfigTool.isRequest("189")) {// 流程编号是否必填
			c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}

		rows++;
		if (JecnConfigTool.isAutoNumber() && !JecnConfigTool.isMengNiuOperaType()) {
			flowNumField.setEditable(false);
		}
		if (this.isShowProcessCategory != 0) {
			// 流程类别
			categoryCommon = new CategoryCommon(rows, infoPanel, insets, processAttributeBean.getProcessTypeId(), this,
					JecnConfigTool.isRequest("4"));
			rows++;
		}
		// 业务类型
		if (this.isShowProcessBussType != 0) {
			businessTypeCommon = new BusinessTypeCommon(rows, infoPanel, insets, processAttributeBean.getBussType(),
					this, isShowProcessBussType == 2);
			rows++;
		}
		// 流程责任人
		personliableSelect = new PersonliableSelectCommon(rows, infoPanel, insets,
				processAttributeBean.getDutyUserId(), processAttributeBean.getDutyUserType(), processAttributeBean
						.getDutyUserName(), this, 0, JecnConfigTool.isRequest("3"));
		rows++;
		if (this.isShowProcessGuardian != 0) {
			// 流程监护人
			guardianPeopleSelectCommon = new PeopleSelectCommon(rows, infoPanel, insets, processAttributeBean
					.getGuardianId(), processAttributeBean.getGuardianName(), this, JecnProperties
					.getValue("flowGuardianPeopleC"), isShowProcessGuardian == 2);
			rows++;
		}
		if (this.isShowProcessIntendedPerson != 0) {
			// 流程拟制人
			fictionPeopleSelectCommon = new PeopleSelectCommon(rows, infoPanel, insets, processAttributeBean
					.getFictionPeopleId(), processAttributeBean.getFictionPeopleName(), this, JecnProperties
					.getValue("flowFictionPeopleC"), isShowProcessIntendedPerson == 2);
			rows++;
		}
		// 责任部门
		resDepartSelectCommon = new ResDepartSelectCommon(rows, infoPanel, insets, processAttributeBean.getDutyOrgId(),
				processAttributeBean.getDutyOrgName(), this, JecnConfigTool.isRequest("5"));
		rows++;

		// 专员
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessCommissioner)) {
			this.commissionerPeopleSelectCommon = new PeopleSelectCommon(rows, infoPanel, insets, processAttributeBean
					.getCommissionerId(), processAttributeBean.getCommissionerName(), this, JecnProperties
					.getValue("commissioner"), JecnConfigTool.isRequest("isShowProcessCommissioner"));

		}
		
		rows++;
		// 关键字
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessKeyWord)) {
			this.keyWordField = new JecnTipTextField(rows, infoPanel, insets, JecnProperties.getValue("keyWordC"),
					processAttributeBean.getKeyWord() == null ? "" : processAttributeBean.getKeyWord(), JecnProperties
							.getValue("keyWord"));

		}
		if (JecnConfigTool.isRequest("isShowProcessKeyWord")) {// 关键字是否必填
			c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}
		rows++;

		// 有效期
		this.expiryCommon = new ExpiryCommon(processAttributeBean.getExpiry() == null ? "0" : processAttributeBean
				.getExpiry().toString(), rows, infoPanel, insets, 0);
		rows++;

		// 查阅权限
		accessAuthorityCommon = new AccessAuthorityCommon(rows, infoPanel, insets, flowId, 0, this, false, true);
		rows = accessAuthorityCommon.getRows();
		if (this.isShowProcessSupportTools != 0) {
			// 根据流程ID获得支持工具ID集合
			List<JecnTreeBean> listFlowTool = new ArrayList<JecnTreeBean>();
			try {
				// 获得支持工具树对象
				listFlowTool = ConnectionPool.getFlowTool().getSustainToolsByFlowId(flowId);
			} catch (Exception e) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("gettingToolsError"));
				log.error("ProcessPropertyDialog  is error", e);
			}
			// 支持工具
			supportToolsCommon = new SupportToolsCommon(rows, infoPanel, insets, this, listFlowTool,
					isShowProcessSupportTools == 2);
			rows++;
		}
		if (this.isShowProcessEnclosure != 0) {
			// 附件
			Long fileId = null;
			try {
				JecnFlowBasicInfoT flowBasicInfoT = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(flowId);
				if (flowBasicInfoT != null) {
					fileId = flowBasicInfoT.getFileId();
				}
			} catch (Exception e) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("gettingFlowObjError"));
				log.error("ProcessPropertyDialog  initLayout is error", e);
			}
			fileEnclosureSelectCommon = new FileEnclosureSelectCommon(rows, infoPanel, insets, fileId, this,
					isShowProcessEnclosure == 2);
		}
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		if (JecnDesignerCommon.isAuthSave()) {// 有保存权限
			buttonPanel.add(verfyLab);
			buttonPanel.add(okBut);
		}
		buttonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	/**
	 * 是否更新
	 * 
	 * @return
	 */
	private boolean isUpdate() {
		if (!JecnDesignerCommon.isEditByTaskNode(this.selectNode.getJecnTreeBean(), JecnDesignerMainPanel
				.getDesignerMainPanel().getTree(), JecnConstants.loginBean.isDesignProcessAdmin())) {
			return false;
		}
		// 流程编号
		String oldFlowNumberId = "";
		if (selectNode.getJecnTreeBean().getNumberId() != null) {
			oldFlowNumberId = selectNode.getJecnTreeBean().getNumberId();
		}
		if (!this.flowNumField.getText().equals(oldFlowNumberId)) {
			return true;
		}
		// 流程类别
		if (categoryCommon != null) {
			if (categoryCommon.isUpdate()) {
				return true;
			}
		}
		// 流程类别
		if (businessTypeCommon != null) {
			if (businessTypeCommon.isUpdate()) {
				return true;
			}
		}
		// 责任人
		if (personliableSelect != null) {
			if (personliableSelect.getSingleSelectCommon().isUpdate()) {
				return true;
			}
		}
		// 流程监护人
		if (guardianPeopleSelectCommon != null) {
			if (guardianPeopleSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 流程拟制人
		if (fictionPeopleSelectCommon != null) {
			if (fictionPeopleSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 责任部门
		if (resDepartSelectCommon != null) {
			if (resDepartSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 有效期
		if (this.expiryCommon != null) {
			if (expiryCommon.isUpdate()) {
				return true;
			}
		}
		// 查阅权限
		if (accessAuthorityCommon != null) {
			if (accessAuthorityCommon.isUpdate()) {
				return true;
			}
		}
		// 支持工具
		if (supportToolsCommon != null) {
			if (supportToolsCommon.isUpdate()) {
				return true;
			}
		}
		// 附件
		if (fileEnclosureSelectCommon != null) {
			if (fileEnclosureSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 流程专员
		if (commissionerPeopleSelectCommon != null && this.commissionerPeopleSelectCommon.isUpdate()) {
			return true;
		}
		// 关键字
		if (keyWordField != null && this.keyWordField.isUpdate()) {
			return true;
		}

		return false;
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (JecnDesignerCommon.isAuthSave()) {// 有保存权限
			if (!isUpdate()) {// 没有更新
				this.setVisible(false);
			} else {
				int optionTig = this.dialogCloseBeforeMsgTipAction();
				if (optionTig == 2) {
					okButPerformed();
				} else if (optionTig == 1) {
					this.setVisible(false);
				}
			}
		} else {
			this.setVisible(false);
		}
	}

	/**
	 * 监护人名称
	 * 
	 * @return
	 */
	public String getGuardianName() {
		if (this.guardianPeopleSelectCommon != null) {
			return guardianPeopleSelectCommon.getNameResult();
		}
		return "";
	}

	/**
	 * 拟制人名称
	 * 
	 * @return
	 */
	public String getFictionPeopleName() {
		if (this.fictionPeopleSelectCommon != null) {
			return fictionPeopleSelectCommon.getNameResult();
		}
		return "";
	}

	/**
	 * 责任人
	 * 
	 * @return
	 */
	public String getResPeopleName() {
		return this.personliableSelect.getSingleSelectCommon().getNameResult();
	}

	/**
	 * 责任部门
	 * 
	 * @return
	 */
	public String getResOrgName() {
		return this.resDepartSelectCommon.getNameResult();
	}

	/***************************************************************************
	 * 确定
	 */
	private void okButPerformed() {
		if (isUpdate()) {
			// 关键字
			if (keyWordField != null) {
				if (this.keyWordField.isValidateNotPass(verfyLab)) {
					return;
				}
			}

			// 流程编号验证
			if (DrawCommon.checkNameMaxLength(flowNumField.getText().trim())) {
				verfyLab.setText(JecnProperties.getValue("actBaseNum") + JecnProperties.getValue("lengthNotBaiOut"));
				return;
			}
			ProcessInfoBean processInfoBean = new ProcessInfoBean();
			processInfoBean.setUpdatePeopleId(JecnUtil.getUserId());
			// 有效期
			if (expiryCommon != null) {
				if (expiryCommon.isValidateNotPass(verfyLab)) {
					return;
				}
				processInfoBean.setValidityValue(Integer.valueOf(expiryCommon.getValueResult()));
			}
			processInfoBean.setFlowId(flowId);
			processInfoBean.setFlowNumber(flowNumField.getText().trim());
			// 流程类别
			Long typeId = null;
			if (this.categoryCommon != null) {
				typeId = categoryCommon.getTypeResultId();
			}
			processInfoBean.setTypeId(typeId);
			// 关键字
			String bussType = "";
			if (businessTypeCommon != null) {
				bussType = businessTypeCommon.getKey();
			}
			processInfoBean.setBussType(bussType);
			// 流程责任人
			int resPersonType = personliableSelect.getPeopleResType();
			Long resPersonId = personliableSelect.getSingleSelectCommon().getIdResult();
			processInfoBean.setResPersonId(resPersonId);
			processInfoBean.setResPersonType(resPersonType);
			// 监护人
			Long guardianId = null;
			if (this.guardianPeopleSelectCommon != null) {
				guardianId = guardianPeopleSelectCommon.getIdResult();
			}
			processInfoBean.setGuardianId(guardianId);
			// 拟制人
			Long fictionPeopleId = null;
			if (this.fictionPeopleSelectCommon != null) {
				fictionPeopleId = fictionPeopleSelectCommon.getIdResult();
			}
			processInfoBean.setFictionPeopleId(fictionPeopleId);
			// 责任部门
			Long resOrgId = this.resDepartSelectCommon.getIdResult();
			processInfoBean.setResOrgId(resOrgId);
			// 查阅权限
			int isPublic = this.accessAuthorityCommon.getPublicStatic();
			processInfoBean.setIsPublic(isPublic);
			AccessId accId = new AccessId();
			// 部门权限
			accId.setOrgAccessId(accessAuthorityCommon.getOrgAccessIds());
			// 岗位权限
			accId.setPosAccessId(accessAuthorityCommon.getPosAccessIds());
			// 岗位组权限
			accId.setPosGroupAccessId(accessAuthorityCommon.getPosGroupAcessIds());
			processInfoBean.setAccId(accId);
			/*
			 * String orgAccessAuthorityids = accessAuthorityCommon.getOrgIds();
			 * processInfoBean.setOrgAccessAuthorityids(orgAccessAuthorityids);
			 * String posAccessAuthorityids = accessAuthorityCommon.getPosIds();
			 * processInfoBean.setPosAccessAuthorityids(posAccessAuthorityids);
			 * String posGroupAccessAuthorityids =
			 * accessAuthorityCommon.getPosGroupIds();
			 * processInfoBean.setPosGroupAccessAuthorityids
			 * (posGroupAccessAuthorityids);
			 */

			// 支持工具
			String supportToolIds = "";
			if (supportToolsCommon != null) {
				supportToolIds = supportToolsCommon.getResultIds();
			}
			processInfoBean.setSupportToolIds(supportToolIds);
			// 附件
			Long fileId = null;
			if (this.fileEnclosureSelectCommon != null) {
				fileId = fileEnclosureSelectCommon.getResultFileId();
			}
			processInfoBean.setFileId(fileId);
			processInfoBean.setConfidentialityLevel(this.accessAuthorityCommon.getConfidentialityLevelStatic());

			// 专员
			if (this.commissionerPeopleSelectCommon != null) {
				processInfoBean.setCommissionerId(commissionerPeopleSelectCommon.getIdResult());
			}
			if (keyWordField != null) {
				// 关键字
				processInfoBean.setKeyWord(this.keyWordField.getResultStr());
			}

			try {
				ConnectionPool.getProcessAction().updateFlowAttribute(processInfoBean);
				this.selectNode.getJecnTreeBean().setNumberId(processInfoBean.getFlowNumber());
				selectNode.getJecnTreeBean().setUpdate(true);
				JecnTreeCommon.reload(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), selectNode);
			} catch (Exception e) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("updateFlowAttrError"));
				log.error("ProcessPropertyDialog is error", e);
			}
		}
		this.setVisible(false);
	}
}
