package com.jecn.epros.server.action.web.login.ad.kuka;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.action.web.login.ad.dongh.JecnDesKeyTools;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.dao.popedom.IPersonDao;

public class KukaLoginAction extends JecnAbstractADLoginAction {
	
	public KukaLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 通过单点登录
	 * 
	 * @return String input;main;success;null
	 * 
	 */
	public String login() {
		try {
			return httpLogin();
		} catch (Exception e) {
			e.printStackTrace();
			return LoginAction.INPUT;
		}
	}

	/**
	 * HTTP 地址解析
	 * 
	 * @return String
	 * @date 2015-8-19 下午03:34:39
	 * @throws
	 */
	private String httpLogin() throws Exception {
		String token = loginAction.getRequest().getParameter("token");
		log.info("顾家  + token = " + token);
		if (StringUtils.isBlank(token)) {
			return LoginAction.INPUT;
		}
		// 获取密钥解析对象
		JecnDesKeyTools tools = new JecnDesKeyTools();
		// 获取解密后用户
		String encodeNum = null;
		try {
			// 获取解密后用户
			encodeNum = tools.decode(token);
			log.error("解析后：" + encodeNum);
		} catch (Exception e) {
			log.error("员工编号解密异常！");
			throw e;
		}

//		String str = encodeNum.substring(14);
//		String name = str.substring(0, str.length() - 46);
		String userId = encodeNum;
		log.info("顾家解析后名称：" + userId);
		
		JecnUser ju = loginAction.getPersonService().getJecnUserByPhone(userId);
		return loginByLoginName(ju.getLoginName());
		
	}

	/**
	 * 返回结果页
	 */
	public String returnPage() {
		return null;
	}

}
