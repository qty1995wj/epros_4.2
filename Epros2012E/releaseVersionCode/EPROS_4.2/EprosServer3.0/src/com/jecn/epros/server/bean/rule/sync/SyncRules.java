package com.jecn.epros.server.bean.rule.sync;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 设计思路，将同步的制度 组装成为树结构的嵌套类型
 * 
 * @author hyl
 * 
 */
public class SyncRules implements Serializable {
	/**
	 * 所有的子节点，包含文件，文件夹
	 */
	private List<SyncRule> rules = new ArrayList<SyncRule>();
	/**
	 * 操作人员
	 */
	private Long peopleId;
	/**
	 * 同步到的目标节点
	 */
	private Long toId;

	public List<SyncRule> getRules() {
		return rules;
	}

	public Long getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}

	public Long getToId() {
		return toId;
	}

	public void setToId(Long toId) {
		this.toId = toId;
	}

}
