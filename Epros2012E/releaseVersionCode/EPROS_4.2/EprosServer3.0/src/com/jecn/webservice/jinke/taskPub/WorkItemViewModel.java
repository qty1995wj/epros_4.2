/**
 * WorkItemViewModel.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class WorkItemViewModel  extends com.jecn.webservice.jinke.taskPub.ViewModelBase  implements java.io.Serializable {
    private java.lang.String participant;

    private java.lang.String participantName;

    private java.lang.String originator;

    private java.lang.String originatorName;

    private java.lang.String originatorOUName;

    private java.lang.String instanceName;

    private java.lang.String activityCode;

    private java.lang.String displayName;

    private java.lang.String receiveTime;

    private java.lang.String planFinishTime;

    private java.lang.String finishTime;

    private java.lang.String instanceId;

    private java.lang.String workflowCode;

    private java.lang.String workflowName;

    private java.lang.String itemCount;

    private boolean displayWorkflowCode;

    private java.lang.String priority;

    private int state;

    private boolean urged;

    private boolean assisted;

    private boolean assistantFinished;

    private boolean consulted;

    private boolean consultantFinished;

    private java.lang.String circulateCreator;

    private java.lang.String circulateCreatorName;

    private com.jecn.webservice.jinke.taskPub.TimeSpan stayTime;

    private java.lang.String itemSummary;

    public WorkItemViewModel() {
    }

    public WorkItemViewModel(
           java.lang.String objectID,
           java.lang.String participant,
           java.lang.String participantName,
           java.lang.String originator,
           java.lang.String originatorName,
           java.lang.String originatorOUName,
           java.lang.String instanceName,
           java.lang.String activityCode,
           java.lang.String displayName,
           java.lang.String receiveTime,
           java.lang.String planFinishTime,
           java.lang.String finishTime,
           java.lang.String instanceId,
           java.lang.String workflowCode,
           java.lang.String workflowName,
           java.lang.String itemCount,
           boolean displayWorkflowCode,
           java.lang.String priority,
           int state,
           boolean urged,
           boolean assisted,
           boolean assistantFinished,
           boolean consulted,
           boolean consultantFinished,
           java.lang.String circulateCreator,
           java.lang.String circulateCreatorName,
           com.jecn.webservice.jinke.taskPub.TimeSpan stayTime,
           java.lang.String itemSummary) {
        super(
            objectID);
        this.participant = participant;
        this.participantName = participantName;
        this.originator = originator;
        this.originatorName = originatorName;
        this.originatorOUName = originatorOUName;
        this.instanceName = instanceName;
        this.activityCode = activityCode;
        this.displayName = displayName;
        this.receiveTime = receiveTime;
        this.planFinishTime = planFinishTime;
        this.finishTime = finishTime;
        this.instanceId = instanceId;
        this.workflowCode = workflowCode;
        this.workflowName = workflowName;
        this.itemCount = itemCount;
        this.displayWorkflowCode = displayWorkflowCode;
        this.priority = priority;
        this.state = state;
        this.urged = urged;
        this.assisted = assisted;
        this.assistantFinished = assistantFinished;
        this.consulted = consulted;
        this.consultantFinished = consultantFinished;
        this.circulateCreator = circulateCreator;
        this.circulateCreatorName = circulateCreatorName;
        this.stayTime = stayTime;
        this.itemSummary = itemSummary;
    }


    /**
     * Gets the participant value for this WorkItemViewModel.
     * 
     * @return participant
     */
    public java.lang.String getParticipant() {
        return participant;
    }


    /**
     * Sets the participant value for this WorkItemViewModel.
     * 
     * @param participant
     */
    public void setParticipant(java.lang.String participant) {
        this.participant = participant;
    }


    /**
     * Gets the participantName value for this WorkItemViewModel.
     * 
     * @return participantName
     */
    public java.lang.String getParticipantName() {
        return participantName;
    }


    /**
     * Sets the participantName value for this WorkItemViewModel.
     * 
     * @param participantName
     */
    public void setParticipantName(java.lang.String participantName) {
        this.participantName = participantName;
    }


    /**
     * Gets the originator value for this WorkItemViewModel.
     * 
     * @return originator
     */
    public java.lang.String getOriginator() {
        return originator;
    }


    /**
     * Sets the originator value for this WorkItemViewModel.
     * 
     * @param originator
     */
    public void setOriginator(java.lang.String originator) {
        this.originator = originator;
    }


    /**
     * Gets the originatorName value for this WorkItemViewModel.
     * 
     * @return originatorName
     */
    public java.lang.String getOriginatorName() {
        return originatorName;
    }


    /**
     * Sets the originatorName value for this WorkItemViewModel.
     * 
     * @param originatorName
     */
    public void setOriginatorName(java.lang.String originatorName) {
        this.originatorName = originatorName;
    }


    /**
     * Gets the originatorOUName value for this WorkItemViewModel.
     * 
     * @return originatorOUName
     */
    public java.lang.String getOriginatorOUName() {
        return originatorOUName;
    }


    /**
     * Sets the originatorOUName value for this WorkItemViewModel.
     * 
     * @param originatorOUName
     */
    public void setOriginatorOUName(java.lang.String originatorOUName) {
        this.originatorOUName = originatorOUName;
    }


    /**
     * Gets the instanceName value for this WorkItemViewModel.
     * 
     * @return instanceName
     */
    public java.lang.String getInstanceName() {
        return instanceName;
    }


    /**
     * Sets the instanceName value for this WorkItemViewModel.
     * 
     * @param instanceName
     */
    public void setInstanceName(java.lang.String instanceName) {
        this.instanceName = instanceName;
    }


    /**
     * Gets the activityCode value for this WorkItemViewModel.
     * 
     * @return activityCode
     */
    public java.lang.String getActivityCode() {
        return activityCode;
    }


    /**
     * Sets the activityCode value for this WorkItemViewModel.
     * 
     * @param activityCode
     */
    public void setActivityCode(java.lang.String activityCode) {
        this.activityCode = activityCode;
    }


    /**
     * Gets the displayName value for this WorkItemViewModel.
     * 
     * @return displayName
     */
    public java.lang.String getDisplayName() {
        return displayName;
    }


    /**
     * Sets the displayName value for this WorkItemViewModel.
     * 
     * @param displayName
     */
    public void setDisplayName(java.lang.String displayName) {
        this.displayName = displayName;
    }


    /**
     * Gets the receiveTime value for this WorkItemViewModel.
     * 
     * @return receiveTime
     */
    public java.lang.String getReceiveTime() {
        return receiveTime;
    }


    /**
     * Sets the receiveTime value for this WorkItemViewModel.
     * 
     * @param receiveTime
     */
    public void setReceiveTime(java.lang.String receiveTime) {
        this.receiveTime = receiveTime;
    }


    /**
     * Gets the planFinishTime value for this WorkItemViewModel.
     * 
     * @return planFinishTime
     */
    public java.lang.String getPlanFinishTime() {
        return planFinishTime;
    }


    /**
     * Sets the planFinishTime value for this WorkItemViewModel.
     * 
     * @param planFinishTime
     */
    public void setPlanFinishTime(java.lang.String planFinishTime) {
        this.planFinishTime = planFinishTime;
    }


    /**
     * Gets the finishTime value for this WorkItemViewModel.
     * 
     * @return finishTime
     */
    public java.lang.String getFinishTime() {
        return finishTime;
    }


    /**
     * Sets the finishTime value for this WorkItemViewModel.
     * 
     * @param finishTime
     */
    public void setFinishTime(java.lang.String finishTime) {
        this.finishTime = finishTime;
    }


    /**
     * Gets the instanceId value for this WorkItemViewModel.
     * 
     * @return instanceId
     */
    public java.lang.String getInstanceId() {
        return instanceId;
    }


    /**
     * Sets the instanceId value for this WorkItemViewModel.
     * 
     * @param instanceId
     */
    public void setInstanceId(java.lang.String instanceId) {
        this.instanceId = instanceId;
    }


    /**
     * Gets the workflowCode value for this WorkItemViewModel.
     * 
     * @return workflowCode
     */
    public java.lang.String getWorkflowCode() {
        return workflowCode;
    }


    /**
     * Sets the workflowCode value for this WorkItemViewModel.
     * 
     * @param workflowCode
     */
    public void setWorkflowCode(java.lang.String workflowCode) {
        this.workflowCode = workflowCode;
    }


    /**
     * Gets the workflowName value for this WorkItemViewModel.
     * 
     * @return workflowName
     */
    public java.lang.String getWorkflowName() {
        return workflowName;
    }


    /**
     * Sets the workflowName value for this WorkItemViewModel.
     * 
     * @param workflowName
     */
    public void setWorkflowName(java.lang.String workflowName) {
        this.workflowName = workflowName;
    }


    /**
     * Gets the itemCount value for this WorkItemViewModel.
     * 
     * @return itemCount
     */
    public java.lang.String getItemCount() {
        return itemCount;
    }


    /**
     * Sets the itemCount value for this WorkItemViewModel.
     * 
     * @param itemCount
     */
    public void setItemCount(java.lang.String itemCount) {
        this.itemCount = itemCount;
    }


    /**
     * Gets the displayWorkflowCode value for this WorkItemViewModel.
     * 
     * @return displayWorkflowCode
     */
    public boolean isDisplayWorkflowCode() {
        return displayWorkflowCode;
    }


    /**
     * Sets the displayWorkflowCode value for this WorkItemViewModel.
     * 
     * @param displayWorkflowCode
     */
    public void setDisplayWorkflowCode(boolean displayWorkflowCode) {
        this.displayWorkflowCode = displayWorkflowCode;
    }


    /**
     * Gets the priority value for this WorkItemViewModel.
     * 
     * @return priority
     */
    public java.lang.String getPriority() {
        return priority;
    }


    /**
     * Sets the priority value for this WorkItemViewModel.
     * 
     * @param priority
     */
    public void setPriority(java.lang.String priority) {
        this.priority = priority;
    }


    /**
     * Gets the state value for this WorkItemViewModel.
     * 
     * @return state
     */
    public int getState() {
        return state;
    }


    /**
     * Sets the state value for this WorkItemViewModel.
     * 
     * @param state
     */
    public void setState(int state) {
        this.state = state;
    }


    /**
     * Gets the urged value for this WorkItemViewModel.
     * 
     * @return urged
     */
    public boolean isUrged() {
        return urged;
    }


    /**
     * Sets the urged value for this WorkItemViewModel.
     * 
     * @param urged
     */
    public void setUrged(boolean urged) {
        this.urged = urged;
    }


    /**
     * Gets the assisted value for this WorkItemViewModel.
     * 
     * @return assisted
     */
    public boolean isAssisted() {
        return assisted;
    }


    /**
     * Sets the assisted value for this WorkItemViewModel.
     * 
     * @param assisted
     */
    public void setAssisted(boolean assisted) {
        this.assisted = assisted;
    }


    /**
     * Gets the assistantFinished value for this WorkItemViewModel.
     * 
     * @return assistantFinished
     */
    public boolean isAssistantFinished() {
        return assistantFinished;
    }


    /**
     * Sets the assistantFinished value for this WorkItemViewModel.
     * 
     * @param assistantFinished
     */
    public void setAssistantFinished(boolean assistantFinished) {
        this.assistantFinished = assistantFinished;
    }


    /**
     * Gets the consulted value for this WorkItemViewModel.
     * 
     * @return consulted
     */
    public boolean isConsulted() {
        return consulted;
    }


    /**
     * Sets the consulted value for this WorkItemViewModel.
     * 
     * @param consulted
     */
    public void setConsulted(boolean consulted) {
        this.consulted = consulted;
    }


    /**
     * Gets the consultantFinished value for this WorkItemViewModel.
     * 
     * @return consultantFinished
     */
    public boolean isConsultantFinished() {
        return consultantFinished;
    }


    /**
     * Sets the consultantFinished value for this WorkItemViewModel.
     * 
     * @param consultantFinished
     */
    public void setConsultantFinished(boolean consultantFinished) {
        this.consultantFinished = consultantFinished;
    }


    /**
     * Gets the circulateCreator value for this WorkItemViewModel.
     * 
     * @return circulateCreator
     */
    public java.lang.String getCirculateCreator() {
        return circulateCreator;
    }


    /**
     * Sets the circulateCreator value for this WorkItemViewModel.
     * 
     * @param circulateCreator
     */
    public void setCirculateCreator(java.lang.String circulateCreator) {
        this.circulateCreator = circulateCreator;
    }


    /**
     * Gets the circulateCreatorName value for this WorkItemViewModel.
     * 
     * @return circulateCreatorName
     */
    public java.lang.String getCirculateCreatorName() {
        return circulateCreatorName;
    }


    /**
     * Sets the circulateCreatorName value for this WorkItemViewModel.
     * 
     * @param circulateCreatorName
     */
    public void setCirculateCreatorName(java.lang.String circulateCreatorName) {
        this.circulateCreatorName = circulateCreatorName;
    }


    /**
     * Gets the stayTime value for this WorkItemViewModel.
     * 
     * @return stayTime
     */
    public com.jecn.webservice.jinke.taskPub.TimeSpan getStayTime() {
        return stayTime;
    }


    /**
     * Sets the stayTime value for this WorkItemViewModel.
     * 
     * @param stayTime
     */
    public void setStayTime(com.jecn.webservice.jinke.taskPub.TimeSpan stayTime) {
        this.stayTime = stayTime;
    }


    /**
     * Gets the itemSummary value for this WorkItemViewModel.
     * 
     * @return itemSummary
     */
    public java.lang.String getItemSummary() {
        return itemSummary;
    }


    /**
     * Sets the itemSummary value for this WorkItemViewModel.
     * 
     * @param itemSummary
     */
    public void setItemSummary(java.lang.String itemSummary) {
        this.itemSummary = itemSummary;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WorkItemViewModel)) return false;
        WorkItemViewModel other = (WorkItemViewModel) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.participant==null && other.getParticipant()==null) || 
             (this.participant!=null &&
              this.participant.equals(other.getParticipant()))) &&
            ((this.participantName==null && other.getParticipantName()==null) || 
             (this.participantName!=null &&
              this.participantName.equals(other.getParticipantName()))) &&
            ((this.originator==null && other.getOriginator()==null) || 
             (this.originator!=null &&
              this.originator.equals(other.getOriginator()))) &&
            ((this.originatorName==null && other.getOriginatorName()==null) || 
             (this.originatorName!=null &&
              this.originatorName.equals(other.getOriginatorName()))) &&
            ((this.originatorOUName==null && other.getOriginatorOUName()==null) || 
             (this.originatorOUName!=null &&
              this.originatorOUName.equals(other.getOriginatorOUName()))) &&
            ((this.instanceName==null && other.getInstanceName()==null) || 
             (this.instanceName!=null &&
              this.instanceName.equals(other.getInstanceName()))) &&
            ((this.activityCode==null && other.getActivityCode()==null) || 
             (this.activityCode!=null &&
              this.activityCode.equals(other.getActivityCode()))) &&
            ((this.displayName==null && other.getDisplayName()==null) || 
             (this.displayName!=null &&
              this.displayName.equals(other.getDisplayName()))) &&
            ((this.receiveTime==null && other.getReceiveTime()==null) || 
             (this.receiveTime!=null &&
              this.receiveTime.equals(other.getReceiveTime()))) &&
            ((this.planFinishTime==null && other.getPlanFinishTime()==null) || 
             (this.planFinishTime!=null &&
              this.planFinishTime.equals(other.getPlanFinishTime()))) &&
            ((this.finishTime==null && other.getFinishTime()==null) || 
             (this.finishTime!=null &&
              this.finishTime.equals(other.getFinishTime()))) &&
            ((this.instanceId==null && other.getInstanceId()==null) || 
             (this.instanceId!=null &&
              this.instanceId.equals(other.getInstanceId()))) &&
            ((this.workflowCode==null && other.getWorkflowCode()==null) || 
             (this.workflowCode!=null &&
              this.workflowCode.equals(other.getWorkflowCode()))) &&
            ((this.workflowName==null && other.getWorkflowName()==null) || 
             (this.workflowName!=null &&
              this.workflowName.equals(other.getWorkflowName()))) &&
            ((this.itemCount==null && other.getItemCount()==null) || 
             (this.itemCount!=null &&
              this.itemCount.equals(other.getItemCount()))) &&
            this.displayWorkflowCode == other.isDisplayWorkflowCode() &&
            ((this.priority==null && other.getPriority()==null) || 
             (this.priority!=null &&
              this.priority.equals(other.getPriority()))) &&
            this.state == other.getState() &&
            this.urged == other.isUrged() &&
            this.assisted == other.isAssisted() &&
            this.assistantFinished == other.isAssistantFinished() &&
            this.consulted == other.isConsulted() &&
            this.consultantFinished == other.isConsultantFinished() &&
            ((this.circulateCreator==null && other.getCirculateCreator()==null) || 
             (this.circulateCreator!=null &&
              this.circulateCreator.equals(other.getCirculateCreator()))) &&
            ((this.circulateCreatorName==null && other.getCirculateCreatorName()==null) || 
             (this.circulateCreatorName!=null &&
              this.circulateCreatorName.equals(other.getCirculateCreatorName()))) &&
            ((this.stayTime==null && other.getStayTime()==null) || 
             (this.stayTime!=null &&
              this.stayTime.equals(other.getStayTime()))) &&
            ((this.itemSummary==null && other.getItemSummary()==null) || 
             (this.itemSummary!=null &&
              this.itemSummary.equals(other.getItemSummary())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getParticipant() != null) {
            _hashCode += getParticipant().hashCode();
        }
        if (getParticipantName() != null) {
            _hashCode += getParticipantName().hashCode();
        }
        if (getOriginator() != null) {
            _hashCode += getOriginator().hashCode();
        }
        if (getOriginatorName() != null) {
            _hashCode += getOriginatorName().hashCode();
        }
        if (getOriginatorOUName() != null) {
            _hashCode += getOriginatorOUName().hashCode();
        }
        if (getInstanceName() != null) {
            _hashCode += getInstanceName().hashCode();
        }
        if (getActivityCode() != null) {
            _hashCode += getActivityCode().hashCode();
        }
        if (getDisplayName() != null) {
            _hashCode += getDisplayName().hashCode();
        }
        if (getReceiveTime() != null) {
            _hashCode += getReceiveTime().hashCode();
        }
        if (getPlanFinishTime() != null) {
            _hashCode += getPlanFinishTime().hashCode();
        }
        if (getFinishTime() != null) {
            _hashCode += getFinishTime().hashCode();
        }
        if (getInstanceId() != null) {
            _hashCode += getInstanceId().hashCode();
        }
        if (getWorkflowCode() != null) {
            _hashCode += getWorkflowCode().hashCode();
        }
        if (getWorkflowName() != null) {
            _hashCode += getWorkflowName().hashCode();
        }
        if (getItemCount() != null) {
            _hashCode += getItemCount().hashCode();
        }
        _hashCode += (isDisplayWorkflowCode() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getPriority() != null) {
            _hashCode += getPriority().hashCode();
        }
        _hashCode += getState();
        _hashCode += (isUrged() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isAssisted() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isAssistantFinished() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isConsulted() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isConsultantFinished() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCirculateCreator() != null) {
            _hashCode += getCirculateCreator().hashCode();
        }
        if (getCirculateCreatorName() != null) {
            _hashCode += getCirculateCreatorName().hashCode();
        }
        if (getStayTime() != null) {
            _hashCode += getStayTime().hashCode();
        }
        if (getItemSummary() != null) {
            _hashCode += getItemSummary().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WorkItemViewModel.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "WorkItemViewModel"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("participant");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Participant"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("participantName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ParticipantName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Originator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originatorName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "OriginatorName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originatorOUName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "OriginatorOUName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instanceName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "InstanceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activityCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ActivityCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DisplayName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ReceiveTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("planFinishTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "PlanFinishTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("finishTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "FinishTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instanceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "InstanceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflowCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "WorkflowCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflowName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "WorkflowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ItemCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayWorkflowCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DisplayWorkflowCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priority");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Priority"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("urged");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Urged"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assisted");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Assisted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assistantFinished");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "AssistantFinished"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consulted");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Consulted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consultantFinished");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ConsultantFinished"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("circulateCreator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CirculateCreator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("circulateCreatorName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CirculateCreatorName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stayTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "StayTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "TimeSpan"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ItemSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
