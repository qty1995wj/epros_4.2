package epros.designer.gui.rule;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnMoveChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class RuleMoveChooseDialog extends JecnMoveChooseDialog {
	private Logger log = Logger.getLogger(RuleMoveChooseDialog.class);

	public RuleMoveChooseDialog(List<JecnTreeNode> listMoveNodes, JecnTree moveTree) {
		super(listMoveNodes, moveTree);
	}

	@Override
	public boolean savaData(List<Long> ids, Long pid) {
		try {
			ConnectionPool.getRuleAction().moveNodes(ids, pid, JecnConstants.getUserId(), this.getMoveNodeType());
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("moveSuccess"));
			return true;
		} catch (Exception e) {
			log.error("RuleMoveChooseDialog savaData is error", e);
			// 增加提示
			return false;

		}

	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyRuleMoveTree(this.getListIds());
		// if (JecnConstants.isMysql) {
		// return new RoutineRuleMoveTree(this.getListIds());
		// } else {
		// return new HighEfficiencyRuleMoveTree(this.getListIds());
		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));// 名称
		return title;
	}

	@Override
	public boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid) {
		try {
			List<JecnTreeBean> list = ConnectionPool.getRuleAction().getChildRules(pid, JecnConstants.projectId, 0, 1);
			for (JecnTreeNode node : listMoveNodes) {
				for (JecnTreeBean tr : list) {
					if (node.getJecnTreeBean().getTreeNodeType() == tr.getTreeNodeType()
							&& node.getJecnTreeBean().getName().equals(tr.getName())) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeHaved") + "\""
								+ tr.getName() + "\"");
						return true;
					}
				}
			}
		} catch (Exception e) {
			log.error("节点移动验证重名出错", e);
			return false;
		}
		return false;
	}
}
