package com.jecn.epros.domain;

import com.jecn.epros.domain.InventoryTable.Status;
import com.jecn.epros.domain.process.processFile.BaseProcessFileBean.CellType;
import com.jecn.epros.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 单元格对象
 */
public class InventoryCell {
    /**
     * 单元格内容
     */
    private String text;
    /**
     * 单元格连接
     */
    private List<LinkResource> links;
    /**
     * 单元格类型
     */
    private CellType type;
    /**
     * 状态
     **/
    private Status status;
    private InventoryCellStyle style;

    public InventoryCell() {

    }

    public InventoryCell(LinkResource link) {
        this.links = new ArrayList<>();
        this.links.add(link);
    }

    public InventoryCell(String text) {
        this.text = Utils.nullToEmpty(text);
    }

    public String getType() {
        if (text != null && links != null) {
            type = CellType.both;
        } else if (text != null) {
            type = CellType.text;
        } else if (links != null) {
            type = CellType.links;
        } else {
            type = null;
        }
        return type != null ? type.toString() : null;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setType(CellType type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public List<LinkResource> getLinks() {
        return links;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setLinks(List<LinkResource> links) {
        this.links = links;
    }

    public InventoryCellStyle getStyle() {
        return style;
    }

    public void setStyle(InventoryCellStyle style) {
        this.style = style;
    }
}
