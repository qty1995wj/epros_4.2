package epros.designer.gui.rule.edit;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 相关文件表格table
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-20 时间：上午10:23:04
 */
public class BaseCommonJTable extends JTable {
	/** 选中table 当前行和列 */
	protected int myRow = -10;

	protected int myColumn = -10;

	public BaseCommonJTable() {
		this.setModel(getTableModel());
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 列不可拖动
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		// 多选
		this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		this.setRowHeight(20);
		// 选中行事件 true 选中
		// this.setRowSelectionAllowed(false);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {// 双击
					// 编辑处理
					isClickEdit();
				}
			}

			/**
			 * 鼠标移动到表格监听
			 */
			public void mouseExited(MouseEvent e) {
				mouseMovedProcess(e);
			}
		});
		/**
		 * 添加鼠标监听
		 * 
		 */
		this.addMouseMotionListener(new MouseAdapter() {
			public void mouseMoved(MouseEvent e) {
				mouseMovedProcess(e);
			}
		});
	}

	protected void mouseMovedProcess(MouseEvent e) {

	}

	/**
	 * 初始化table
	 * 
	 * @return
	 */
	public RelatedFileTableModel getTableModel() {
		return new RelatedFileTableModel(updateTableContent(new ArrayList<Object>()), getTableTitleVector());
	}

	public RelatedFileTableModel getEditMode() {
		return (RelatedFileTableModel) this.getModel();
	}

	protected Vector<Object> getTableTitleVector() {
		return new Vector<Object>();
	}

	protected Vector<Vector<Object>> updateTableContent(List<Object> list) {
		if (list == null) {
			return new Vector<Vector<Object>>();
		}
		return new Vector<Vector<Object>>();
	}

	/**
	 * tableModel
	 * 
	 * @author ZHAGNXIAOHU
	 * @date： 日期：2013-11-20 时间：上午10:20:46
	 */
	class RelatedFileTableModel extends DefaultTableModel {
		/**
		 * @param data
		 * @param title
		 */
		public RelatedFileTableModel(Vector<Vector<Object>> data, Vector<Object> title) {
			super(data, title);
		}

		/**
		 * 单元格不可编辑
		 */
		@Override
		public boolean isCellEditable(int rowindex, int colindex) {
			return isTableCellEditable(rowindex, colindex);
		}

		public void remoeAll() {
			// 清空
			for (int index = this.getRowCount() - 1; index >= 0; index--) {
				this.removeRow(index);
			}
		}
	}

	/**
	 * 
	 * 单元格是否可编辑
	 * 
	 * @return
	 */
	protected void isClickEdit() {

	}

	protected boolean isTableCellEditable(int rowindex, int colindex) {
		return false;
	}

	public int getMyRow() {
		return myRow;
	}

	public int getMyColumn() {
		return myColumn;
	}
}
