package com.jecn.epros.server.service.process.buss;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.jecn.epros.bean.checkout.CheckoutRoleItem;
import com.jecn.epros.bean.checkout.CheckoutRoleResult;
import com.jecn.epros.server.util.JecnUtil;

public class RoleCheckout extends BaseCheckout {
	protected List<CheckoutRoleResult> checkouts;

	public RoleCheckout(List<CheckoutRoleResult> checkouts) {
		this.checkouts = checkouts;
	}

	protected HSSFWorkbook createWorkBook() {
		// 声明一个工作薄
		HSSFWorkbook workbook = new HSSFWorkbook();
		try {
			// 生成一个表格
			HSSFSheet sheet = workbook.createSheet("data");
			HSSFCellStyle titleStyle = SheetCellStyle.getInventoryTitleCellStyle(workbook);
			HSSFCellStyle contentStyle = SheetCellStyle.getInventoryContentCellStyle(workbook);
			titleStyle.setWrapText(true);
			contentStyle.setWrapText(true);
			createTitle(sheet, titleStyle);
			createContent(sheet, contentStyle, checkouts);
		} catch (Exception e) {
			log.error("", e);
		} finally {
			try {
				workbook.close();
			} catch (IOException e) {

			}
		}
		return workbook;
	}

	private void createContent(HSSFSheet sheet, HSSFCellStyle style, List<CheckoutRoleResult> checkouts) {
		int[] index = new int[1];
		index[0] = 1;
		String flowName;
		String posPrefix = "（岗位）";
		String groupPrefix = "（岗位组）";
		for (CheckoutRoleResult result : checkouts) {
			flowName = result.getName();
			List<CheckoutRoleItem> data = result.getData();
			for (CheckoutRoleItem d : data) {
				String prefix = d.getType() == 0 ? posPrefix : groupPrefix;
				String posName = "";
				String existName = "";
				if (d.isFpEqual() || d.isRelated()) {
					if (d.isFpEqual() && d.isRelated()) {
						existName = "√关联";
					} else if (d.isFpEqual()) {
						existName = "◎同名未关联";
					} else if (d.isRelated()) {
						existName = "△关联不同名";
					}

				} else {
					existName = "╳未关联";
				}
				if (StringUtils.isBlank(d.getPosName())) {
					posName = "";
				} else {
					posName = prefix + d.getPosName();
				}
				createRow(style, sheet, index[0]++, flowName, JecnUtil.nullToEmpty(d.getFigureText()), existName,
						posName);
			}
		}
	}

	private int createTitle(HSSFSheet sheet, HSSFCellStyle style) {
		String[] titles = new String[] { "流程名称", "角色", "是否关联", "角色库名称" };
		createRow(style, sheet, 0, titles);
		sheet.setColumnWidth(0, 2 * 22 * 256);
		sheet.setColumnWidth(1, 2 * 22 * 256);
		sheet.setColumnWidth(2, 22 * 256);
		sheet.setColumnWidth(3, 3 * 11 * 256);
		return 0;
	}

	private void createRow(HSSFCellStyle style, HSSFSheet sheet, int row, String... names) {
		HSSFRow hssfRow = sheet.createRow(row);
		int col = 0;
		for (String title : names) {
			HSSFCell cell = hssfRow.createCell(col);
			cell.setCellStyle(style);
			cell.setCellValue(new HSSFRichTextString(title));
			col++;
		}
	}
}
