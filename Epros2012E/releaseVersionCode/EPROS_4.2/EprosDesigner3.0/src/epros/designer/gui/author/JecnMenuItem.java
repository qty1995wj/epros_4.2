package epros.designer.gui.author;

import javax.swing.JMenuItem;

import epros.designer.util.JecnProperties;
import epros.draw.util.JecnFileUtil;

public class JecnMenuItem extends JMenuItem {

	private AuthorMenuEnum menuEnum;

	public JecnMenuItem(AuthorMenuEnum menuEnum, String text) {
		this.menuEnum = menuEnum;
	}

	private void initIconAndText() {
		// 图片
		if (menuEnum != null) {
			// 设置图片
			this.setIcon(JecnFileUtil.getToolBarImageIcon(menuEnum.toString()));
			// 设置内容
			this.setText(JecnProperties.getValue(menuEnum.toString()));
		}
	}
}
