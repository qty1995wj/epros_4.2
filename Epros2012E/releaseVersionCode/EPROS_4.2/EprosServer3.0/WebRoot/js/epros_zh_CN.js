/*国际化
 * 命名以i_开头 如:var i_name="xx"
 * 以中文冒号(：)结尾的变量命名以大写C结尾  如(姓名：)--> var nameC="xx"
 * */
/************************************************/
//名称
var i_name = "\u540d\u79f0";
//名称：
var i_nameC = "\u540d\u79f0\uff1a";
//编号
var i_number = "\u7f16\u53f7";
//岗位名称
var i_positionName = "\u5c97\u4f4d\u540d\u79f0";
//执行角色
var i_executiveRole = "\u6267\u884c\u89d2\u8272";
//流程编号
var i_processNumber = "\u6d41\u7a0b\u7f16\u53f7";
//流程名称
var i_processName = "\u6d41\u7a0b\u540d\u79f0";
//活动编号及名称
var i_activitiesNumbersAndName = "\u6d3b\u52a8\u7f16\u53f7\u53ca\u540d\u79f0";
//流程图
var i_procesChart = "\u6d41\u7a0b\u56fe";
//流程架构
var i_processMap = "\u6d41\u7a0b\u67b6\u6784";
//流程架构名称
var i_processMapName = "\u6d41\u7a0b\u67b6\u6784\u540d\u79f0";
//所属流程架构
var i_belongProcessMap = "\u6240\u5c5e\u6d41\u7a0b\u67b6\u6784";
//基本信息
var i_overviewInfo = "\u57FA\u672C\u4FE1\u606F";
//流程文件
var i_operateDes = "\u6d41\u7a0b\u6587\u4ef6";
//操作说明
var i_operateDec="\u64CD\u4F5C\u8BF4\u660E";
//操作模板
var i_operateMode = "\u64cd\u4f5c\u6a21\u677f";
//相关流程/标准/制度
var i_relaProStanRule = "\u76f8\u5173\u6d41\u7a0b/\u6807\u51c6/\u5236\u5ea6";
//属性
var i_property = "\u5c5e\u6027";
//文控信息
var i_historyInfo = "\u6587\u63a7\u4fe1\u606f";
//流程清单
var i_processList = "\u6d41\u7a0b\u6e05\u5355";

//流程架构说明
var i_fileDescription = "\u6D41\u7A0B\u67B6\u6784\u8BF4\u660E";
//流程体系
var i_processSys = "\u6d41\u7a0b\u4f53\u7cfb";
//标准
var i_standard = "\u6807\u51c6";
//标准体系
var i_standardSys = "\u6807\u51c6\u4f53\u7cfb";
//制度体系
var i_ruleSys = "\u5236\u5ea6\u4f53\u7cfb";
//组织与岗位
var i_orgPosition = "\u7ec4\u7ec7\u4e0e\u5c97\u4f4d";

/************************************************/
//任务名称
var i_taskName = "\u4efb\u52a1\u540d\u79f0";
//任务类型
var i_taskType = "\u4efb\u52a1\u7c7b\u578b";
//类型
var i_type = "\u7c7b\u578b";
//拟稿人
var i_creatName = "\u62DF\u7A3F\u4EBA";
//阶段
var i_stage = "\u9636\u6bb5";
//操作
var i_operation = "\u64cd\u4f5c";
//审批
var i_approve = "\u5ba1\u6279";
//取消
var i_cancel = "\u53d6\u6d88";
//查阅
var i_lookUp = "\u67e5\u9605";
//重新提交
var i_reSubmit = "\u91cd\u65b0\u63d0\u4ea4";
//整理意见
var i_opinion = "\u6574\u7406\u610f\u89c1";
//编辑
var i_Edit = "\u7f16\u8f91";
//标记已读
var is_read = "\u6807\u8BB0\u5DF2\u8BFB";
//删除
var i_Delete = "\u5220\u9664";
//清空
var i_empty = "\u6e05\u7a7a";

//条
var i_twig = "\u6761";
//共
var i_together = "\u5171";
//没有记录!
var i_noData = "\u6ca1\u6709\u8bb0\u5f55!";
//流程任务
var i_processTask = "\u6d41\u7a0b\u4efb\u52a1";
//流程架构任务
var i_processMapTask = "\u6d41\u7a0b\u67b6\u6784\u4efb\u52a1";
//文件任务
var i_fileTask = "\u6587\u4ef6\u4efb\u52a1";
//制度任务
var i_ruleTask = "\u5236\u5ea6\u4efb\u52a1";
//我的任务
var i_myTask = "\u6211\u7684\u4efb\u52a1";

//审核人
var i_approvePerson = "\u5ba1\u6838\u4eba";
//审批意见
var i_appIdea = "\u5ba1\u6279\u610f\u89c1";
//审批变动
var i_appChange = "\u5ba1\u6279\u53d8\u52a8";
//目标人
var i_targetPerson = "\u76ee\u6807\u4eba";
//操作日期
var i_operationDate = "\u64cd\u4f5c\u65e5\u671f";

//制度名称
var i_ruleName = "\u5236\u5ea6\u540d\u79f0";
//制度编号
var i_ruleNum = "\u5236\u5ea6\u7f16\u53f7";
//制度类别
var i_ruleType = "\u5236\u5ea6\u7c7b\u522b";
//责任部门
var i_dutyOrg = "\u8d23\u4efb\u90e8\u95e8";
//密级
var i_secretLevel = "\u5bc6\u7ea7";
//搜索
var i_search = "\u641c\u7d22";
//制度属性
var i_ruleProperty = "\u5236\u5ea6\u5c5e\u6027";
//制度说明
var i_ruleExplanation = "\u5236\u5ea6\u8bf4\u660e";
//制度文控信息
var i_ruleHistoryInfo = "\u5236\u5ea6\u6587\u63a7\u4fe1\u606f";
//制度体系内容
var i_ruleSysContent = "\u5236\u5ea6\u4f53\u7cfb\u5185\u5bb9";
//秘密
var i_secret = "\u79d8\u5bc6";
//公开
var i_public = "\u516c\u5f00";
//关闭
var i_close = "\u5173\u95ed";
//确定
var i_ok = "\u786e\u5b9a";
//部门选择
var i_orgSelect = "\u90e8\u95e8\u9009\u62e9";
//岗位选择
var i_posSelect = "\u5c97\u4f4d\u9009\u62e9";
//岗位组选择
var proGroup="\u5C97\u4F4D\u7EC4\u9009\u62E9";
//人员选择
var i_personSelect = "\u4eba\u5458\u9009\u62e9";
//标准名称
var i_standardName = "\u6807\u51c6\u540d\u79f0";
//所属目录
var i_BelongDir = "\u6240\u5c5e\u76ee\u5f55";
//文件体系
var i_fileSys = "\u6587\u4ef6\u4f53\u7cfb";
//文件名称
var i_fileName = "\u6587\u4ef6\u540d\u79f0";
//文件编号
var i_fileNum = "\u6587\u4ef6\u7f16\u53f7";
//责任人
var i_DutyPerson = "\u8d23\u4efb\u4eba";
//发布日期
var i_pubDate = "\u53d1\u5e03\u65e5\u671f";
//上级目录
var i_pdirName = "\u4e0a\u7ea7\u76ee\u5f55";
//组织图
var i_orgMap = "\u7ec4\u7ec7\u56fe";
//部门职责
var i_orgResponsibility = "\u90e8\u95e8\u804c\u8d23";
//相关流程
var i_relationProcess = "\u76f8\u5173\u6d41\u7a0b";
//岗位说明书
var i_positionDes = "\u5c97\u4f4d\u8bf4\u660e\u4e66";
//部门名称
var i_orgName = "\u90e8\u95e8\u540d\u79f0";
//上级部门
var i_superiorOrg = "\u4e0a\u7ea7\u90e8\u95e8";
//所属部门
var i_belongOrg = "\u6240\u5c5e\u90e8\u95e8";
//文件
var i_file = "\u6587\u4ef6";
//标准文件
var i_standardFile = "\u6807\u51c6\u6587\u4ef6";
//制度文件
var i_ruleFile = "\u5236\u5ea6\u6587\u4ef6";
//制度模板文件
var i_ruleModeFile = "\u5236\u5ea6\u6a21\u677f\u6587\u4ef6";
//必须存在拟稿人！
var i_DrafterMustExist = "\u5fc5\u987b\u5b58\u5728\u62df\u7a3f\u4eba\uff01";
//当前阶段为整理意见时，会审人员不能为空！
var i_reviewNotEmpty="\u5F53\u524D\u9636\u6BB5\u4E3A\u6574\u7406\u610F\u89C1\u65F6\uFF0C\u4F1A\u5BA1\u4EBA\u5458\u4E0D\u80FD\u4E3A\u7A7A\uFF01";
//审批人不能为空！
var i_AuditNotNull = "\u5ba1\u6279\u4eba\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//评审人后面必须存在审批人！
var i_AfterReviewerIsNotAudit = "\u8bc4\u5ba1\u4eba\u540e\u9762\u5fc5\u987b\u5b58\u5728\u5ba1\u6279\u4eba\uff01";
//必须存在审核人！
var i_mustExistAudit = "\u5fc5\u987b\u5b58\u5728\u5ba1\u6838\u4eba\uff01";
//当前审批阶段不能为空！
var i_nowStateNotNull = "\u5f53\u524d\u5ba1\u6279\u9636\u6bb5\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//相邻审批人不能相同或评审人不能为拟稿人！
var i_AdjacentApproversMustNotSame = "\u76F8\u90BB\u5BA1\u6279\u4EBA\u4E0D\u80FD\u76F8\u540C\u6216\u8BC4\u5BA1\u4EBA\u4E0D\u80FD\u4E3A\u62DF\u7A3F\u4EBA\uFF01";
// 两次密码输入不一致！
var i_TwoPasswordInputInconsistency = "\u4e24\u6b21\u5bc6\u7801\u8f93\u5165\u4e0d\u4e00\u81f4\uff01";
// 原密码不能为空！
var i_TheOriginalPasswordCantForEmpty = "\u539f\u5bc6\u7801\u4e0d\u80fd\u4e3a\u7a7a\uff01";
// 新密码不能为空！
var i_TheNewPasswordCantForEmpty = "\u65b0\u5bc6\u7801\u4e0d\u80fd\u4e3a\u7a7a\uff01";
// 确认密码不能为空！
var i_confirmPasswordCantForEmpty = "\u786e\u8ba4\u5bc6\u7801\u4e0d\u80fd\u4e3a\u7a7a\uff01";
// 访问服务器异常！
var i_AccessServerAnomaly = "\u8bbf\u95ee\u670d\u52a1\u5668\u5f02\u5e38\uff01";
// 原密码错误！
var i_OriginalPasswordMistake = "\u539f\u5bc6\u7801\u9519\u8bef\uff01";
// 密码修改成功！
var i_PasswordChangingSuccess = "\u5bc6\u7801\u4fee\u6539\u6210\u529f\uff01";
//请完善流程信息！
var i_PleaseFinishMessage = "\u8bf7\u5b8c\u5584\u6d41\u7a0b\u4fe1\u606f\uff01";
//变更说明不能为空！
var i_TaskDescIsNotNul = "\u53d8\u66f4\u8bf4\u660e\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//审批意见不能为空,请填写意见！
var i_TaskOpinionIsNotNul = "\u5ba1\u6279\u610f\u89c1\u4e0d\u80fd\u4e3a\u7a7a,\u8bf7\u586b\u5199\u610f\u89c1\uff01";
//输入长度越界！
var i_InputLengthBounds = "\u8f93\u5165\u957f\u5ea6\u8d8a\u754c\uff01";
//交办人不能为空！
var i_assignedPeopleIsNotNull = "\u4ea4\u529e\u4eba\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//转批人不能为空！
var i_SubPeopleIsNotNull = "\u8f6c\u6279\u4eba\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//我关注的制度
var i_myAttentionRule = "\u6211\u5173\u6CE8\u7684\u5236\u5EA6";
//我关注的标准
var i_myAttentionStandard = "\u6211\u5173\u6CE8\u7684\u6807\u51C6";
//我关注的文件
var i_myAttentionFile = "\u6211\u5173\u6CE8\u7684\u6587\u4EF6";
//我的岗位说明书
var i_myPositionDetail = "\u6211\u7684\u5C97\u4F4D\u8BF4\u660E\u4E66";
//制度列表
var i_ruleList = "\u5236\u5ea6\u5217\u8868";
//制度清单
var i_ruleSysList = "\u5236\u5ea6\u6e05\u5355";
//文件列表
var i_fileList = "\u6587\u4ef6\u5217\u8868";
//详情
var i_detail = "\u8be6\u60c5";
//我的工作模版及指导书
var i_myWorkModeDirectBook = "\u6211\u7684\u5de5\u4f5c\u6a21\u7248\u53ca\u6307\u5bfc\u4e66";
//流程选择
var i_processOrMapSelect = "\u6d41\u7a0b\u9009\u62e9";
//我关注的其他文件
var i_myAttentionOtherFile = "\u6211\u5173\u6ce8\u7684\u5176\u4ed6\u6587\u4ef6";
// 未更新
var i_failedToUpdate = "\u672a\u66f4\u65b0";
// 已更新
var i_hasBeenUpdated = "\u5df2\u66f4\u65b0";
// 状态
var i_state = "\u72b6\u6001";
//EXCEL人员同步
var i_excelPersonImport = "EXCEL\u4eba\u5458\u540c\u6b65";
//自动对接
var i_dbPersonImport = "\u81EA\u52A8\u5BF9\u63A5";
// 我的报表
var i_myReport = "\u6211\u7684\u62a5\u8868";
// 审批
var i_examinationAndApprovalOf = "\u5ba1\u6279";
// 发布
var i_released = "\u53d1\u5e03";
// 待建
var i_toBeBuilt = "\u5f85\u5efa";
//系统管理
var i_systemManage = "\u7cfb\u7edf\u7ba1\u7406";
//登录名称
var i_loginName = "\u767b\u5f55\u540d\u79f0";
//真实名称
var i_trueName = "\u771f\u5b9e\u540d\u79f0";
//任职岗位
var i_officeholdingPosition = "\u4efb\u804c\u5c97\u4f4d";
//系统角色
var i_sysRole = "\u7cfb\u7edf\u89d2\u8272";
// 角色数
var i_roleNumber = "\u89d2\u8272\u6570";
// 应用人数
var i_usingTheNumber = "\u5e94\u7528\u4eba\u6570";
// 上游流程
var i_upstreamProcess = "\u4e0a\u6e38\u6d41\u7a0b";
// 个数
var i_count = "\u4e2a\u6570";
// 下游流程
var i_downstreamProcess = "\u4e0b\u6e38\u6d41\u7a0b";
// 访问次数
var i_visits = "\u8bbf\u95ee\u6b21\u6570";
// 访问人
var i_interviewer = "\u8bbf\u95ee\u4eba";
// 访问时间
var i_accessTime = "\u8bbf\u95ee\u65f6\u95f4";
// 下载次数
var i_downloadTimes = "\u4e0b\u8f7d\u6b21\u6570";
// 下载人
var i_downloadPeople = "\u4e0b\u8f7d\u4eba";
// 下载时间
var i_downloadTime = "\u4e0b\u8f7d\u65f6\u95f4";
//角色名称
var i_roleName = "\u89d2\u8272\u540d\u79f0";
//备注
var i_remark = "\u5907\u6ce8";
//标准列表
var i_standardList = "\u6807\u51c6\u5217\u8868";
// 时间段不能为空！
var i_timeCantBeEmpty = "\u65f6\u95f4\u6bb5\u4e0d\u80fd\u4e3a\u7a7a\uff01";
// 时间段请输入数字！
var i_pleaseEnterTheNumbers = "\u65f6\u95f4\u6bb5\u8bf7\u8f93\u5165\u6570\u5b57\uff01";
// 请选择要查看的KPI！
var i_pleaseSelectToViewKpis = "\u8bf7\u9009\u62e9\u8981\u67e5\u770b\u7684KPI\uff01";
//您确定退出?
var i_sureExit = "\u60a8\u786e\u5b9a\u9000\u51fa?";
// 密码长度超出范围！
var i_passwordLengthIsBeyondTheScope = "\u5bc6\u7801\u957f\u5ea6\u8d85\u51fa\u8303\u56f4\uff01";
//正在加载...
var i_isLoading = "\u6b63\u5728\u52a0\u8f7d...";
//二次评审，评审人不能为空！
var i_reviewNotNull = "\u4e8c\u6b21\u8bc4\u5ba1\uff0c\u8bc4\u5ba1\u4eba\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//是否删除！
var i_HAS_DELETE_TASK = "\u662f\u5426\u5220\u9664\uff01";
//是否标记已读！
var i_HAS_TAG_TASK = "\u662F\u5426\u6807\u8BB0\u5DF2\u8BFB";
//结束时间不能为空！
var i_endTimeIsNotNull = "\u7ed3\u675f\u65f6\u95f4\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//开始时间不能为空！
var i_startTimeIsNotNull = "\u5f00\u59cb\u65f6\u95f4\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//结束时间应小于当前时间！
var i_endTimeIsMustBigNowTime = "\u7ed3\u675f\u65f6\u95f4\u5e94\u5c0f\u4e8e\u5f53\u524d\u65f6\u95f4\uff01";
//开始时间应小于结束时间！
var i_startTimeIsLessEndTime = "\u5f00\u59cb\u65f6\u95f4\u5e94\u5c0f\u4e8e\u7ed3\u675f\u65f6\u95f4\uff01";
//提示
var i_tip = "\u63d0\u793a";
//您没有访问权限!
var i_noPopedom = "\u60a8\u6ca1\u6709\u8bbf\u95ee\u6743\u9650!";
//首页
var i_homePage = "\u9996\u9875";
//我的主页
var i_myhome = "\u6211\u7684\u4E3B\u9875";
//任务管理
var i_taskManagement = "\u4efb\u52a1\u7ba1\u7406";
//报表
var i_theReport = "\u62a5\u8868";
//项目选择
var i_projectSelect = "\u9879\u76ee\u9009\u62e9";
//消息
var i_message = "\u6d88\u606f";
//未读消息
var i_unReadMesg = "\u672a\u8bfb\u6d88\u606f";
//发送人/时间
var i_sendPeopleDate = "\u53d1\u9001\u4eba/\u65f6\u95f4";
//主题
var i_mainTile = "\u4e3b\u9898";
//操作
var i_opera = "\u64cd\u4f5c";
//没有要删除的已读消息！
var i_noDelReadMesg = "\u6ca1\u6709\u8981\u5220\u9664\u7684\u5df2\u8bfb\u6d88\u606f\uff01";
//确认
var i_confirm = "\u786e\u8ba4";
//没有要删除的未读消息！
var i_noDelUnReadMesg = "\u6ca1\u6709\u8981\u5220\u9664\u7684\u672a\u8bfb\u6d88\u606f\uff01";
//没有要标记的未读消息！
var i_noTagUnReadMesg = "\u6CA1\u6709\u8981\u6807\u8BB0\u7684\u672A\u8BFB\u6D88\u606F\uFF01";
//原密码
var i_originalPassword = "\u539f\u5bc6\u7801";
//新密码
var i_theNewPassword = "\u65b0\u5bc6\u7801";
//确定密码
var i_confirmPassword = "\u786e\u5b9a\u5bc6\u7801";
//密码长度不能超过19个字符!
var i_passwordLength = "\u5bc6\u7801\u957f\u5ea6\u4e0d\u80fd\u8d85\u8fc720\u4e2a\u5b57\u7b26!";
//修改密码
var i_updatePassword = "\u4fee\u6539\u5bc6\u7801";
//项目列表
var i_projectList = "\u9879\u76ee\u5217\u8868";
//交办人不能为当前审批人！
var i_assignedIsNotCur = "\u4ea4\u529e\u4eba\u4e0d\u80fd\u4e3a\u5f53\u524d\u5ba1\u6279\u4eba\uff01";
//转批人不能为当前审批人！
var i_subPeopleIsNotCur = "\u8f6c\u6279\u4eba\u4e0d\u80fd\u4e3a\u5f53\u524d\u5ba1\u6279\u4eba\uff01";
//打回任务则重新审批，是否需要打回！
var i_isNeedToFightBack = "\u6253\u56de\u4efb\u52a1\u5219\u91cd\u65b0\u5ba1\u6279\uff0c\u662f\u5426\u9700\u8981\u6253\u56de\uff01";
//搜索时间不能为空！
var i_KPISearchNotNull = "\u641c\u7d22\u65f6\u95f4\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//原密码输入错误!
var i_oldpwdWrong = "\u539f\u5bc6\u7801\u8f93\u5165\u9519\u8bef!";
//是否需要返回！
var i_reCallToTask = "\u662f\u5426\u9700\u8981\u8fd4\u56de\uff01";
//撤回
var i_reCall = "\u64a4\u56de";
//是否撤回！
var i_whetherReCall = "\u662f\u5426\u64a4\u56de\uff01";
//撤回成功！
var i_reCallSuccess = "\u64a4\u56de\u6210\u529f\uff01";
//风险体系
var i_riskSys = "\u98CE\u9669\u4F53\u7CFB";
//风险编号
var i_riskNumber = "\u98CE\u9669\u7F16\u53F7";
//风险描述
var i_riskName = "\u98CE\u9669\u63CF\u8FF0";
//风险等级
var i_riskGrade = "\u98CE\u9669\u7B49\u7EA7";
//风险列表
var i_riskList = "\u98CE\u9669\u5217\u8868";
//我参与的流程
var i_myJoinProcess = "\u6211\u53c2\u4e0e\u7684\u6d41\u7a0b";
//我关注的流程
var i_myAttentionProcess = "\u6211\u5173\u6CE8\u7684\u6D41\u7A0B";
//部门主导流程
var i_orgLeadProcess = "\u90E8\u95E8\u4E3B\u5BFC\u6D41\u7A0B";
//我的流程架构
var i_myProcessMap = "\u6211\u7684\u6d41\u7a0b\u67b6\u6784";
//风险详情
var i_riskDetail = "\u98CE\u9669\u8BE6\u60C5";

// 组织清单
var i_orgList = "\u7ec4\u7ec7\u6e05\u5355";
//风险清单
var i_riskInventory = "\u98ce\u9669\u6e05\u5355";
//标准详情
var i_stanDetail = "\u6807\u51c6\u8be6\u60c5";
// 相关制度
var i_relateRule = "\u76f8\u5173\u5236\u5ea6";
//流程
var i_process = "\u6d41\u7a0b";
//活动
var i_active = "\u6d3b\u52a8";
//相关制度
var i_relatedRule = "\u76f8\u5173\u5236\u5ea6";
//相关标准
var i_relatedStan = "\u76f8\u5173\u6807\u51c6";
//相关风险
var i_relatedRisk = "\u76f8\u5173\u98ce\u9669";
//相关文件
var i_relateFile = "\u76f8\u5173\u6587\u4ef6";
// 获取风险控制清单数据异常！
var i_readRiskListException = "\u83b7\u53d6\u98ce\u9669\u63a7\u5236\u6e05\u5355\u6570\u636e\u5f02\u5e38\uff01";
// 获取流程清单数据异常！
var i_readFlowListException = "\u83b7\u53d6\u6d41\u7a0b\u6e05\u5355\u6570\u636e\u5f02\u5e38\uff01";
// 获取组织清单数据异常！
var i_readOrgListException = "\u83b7\u53d6\u7ec4\u7ec7\u6e05\u5355\u6570\u636e\u5f02\u5e38\uff01";
//条款
var i_clause="\u6761\u6b3e";
//条款要求
var i_clauseRequire="\u6761\u6b3e\u8981\u6c42";
//展开
var i_unfold = "\u5c55\u5f00";
//收起
var i_packUp = "\u6536\u8d77";
//文件属性
var i_fileDetail="\u6587\u4EF6\u5C5E\u6027";
//建议处于编辑中，请稍后再操作!
var i_rtnlPropseEditing ="\u5efa\u8bae\u5904\u4e8e\u7f16\u8f91\u4e2d\uff0c\u8bf7\u7a0d\u540e\u518d\u64cd\u4f5c\uff01";
//当前页面没有未读建议！
var i_nowJspNoUnReadPropse ="\u5f53\u524d\u9875\u9762\u6ca1\u6709\u672a\u8bfb\u5efa\u8bae\uff01";
//已读
var i_readed ="\u5df2\u8bfb";
//采纳
var i_accept="\u91c7\u7eb3";
//回复：
var i_replyC="\u56de\u590d\uff1a";
//回复内容不能为空！
var i_replyContentNoEmpty ="\u56de\u590d\u5185\u5bb9\u4e0d\u80fd\u4e3a\u7a7a\uff01";
//附件：
var i_fileC ="\u9644\u4ef6";
//建议内容和附件不能同时为空，请重新填写！
var i_propseContentAndFileNoSameEmpty="\u5efa\u8bae\u5185\u5bb9\u548c\u9644\u4ef6\u4e0d\u80fd\u540c\u65f6\u4e3a\u7a7a\uff0c\u8bf7\u91cd\u65b0\u586b\u5199\uff01";
//建议内容不能超过1200个字符或者600个汉字，请重新填写！
var i_propseLength="\u5efa\u8bae\u5185\u5bb9\u4e0d\u80fd\u8d85\u8fc71200\u4e2a\u5b57\u7b26\u6216\u8005600\u4e2a\u6c49\u5b57\uff0c\u8bf7\u91cd\u65b0\u586b\u5199\uff01";
//输入长度不能超过1200个字符或者600个汉字！
var i_inputLengthRange ="\u8f93\u5165\u957f\u5ea6\u4e0d\u80fd\u8d85\u8fc71200\u4e2a\u5b57\u7b26\u6216\u8005600\u4e2a\u6c49\u5b57\uff01";
//提交失败！
var i_submitFalse="\u63d0\u4ea4\u5931\u8d25\uff01";
//提交成功！
var i_submitSucces="\u63d0\u4ea4\u6210\u529f\uff01";
//人员同步人数超出上限！
var i_Beyondthelimit="\u4eba\u5458\u540c\u6b65\u4eba\u6570\u8d85\u51fa\u4e0a\u9650\uff01";
//同步开始时间格式不正确！
var i_importDateFormatFalse="\u540c\u6b65\u5f00\u59cb\u65f6\u95f4\u683c\u5f0f\u4e0d\u6b63\u786e\uff01";
//同步时间间隔最少为1天，且只能是正整数！
var i_importLimitOneDay="\u540C\u6B65\u65F6\u95F4\u95F4\u9694\u6700\u5C11\u4E3A1\u5929\uFF0C\u4E14\u53EA\u80FD\u662F\u6B63\u6574\u6570\uFF01";
//岗位匹配
var i_macthPost="\u5c97\u4f4d\u5339\u914d";
//未匹配流程岗位
var i_notMatchFlowPost="\u672a\u5339\u914d\u6d41\u7a0b\u5c97\u4f4d";
//未匹配岗位
var i_notMatchPost = "\u672a\u5339\u914d\u5c97\u4f4d";
//未匹配基准岗位
var i_notMatchBasePost="\u672a\u5339\u914d\u57fa\u51c6\u5c97\u4f4d";
// 合理化建议
var i_rationalization = "\u5408\u7406\u5316\u5efa\u8bae";
//该建议不是最新数据，请刷新后再操作！
var i_noDeletePropose ="\u8be5\u5efa\u8bae\u4e0d\u662f\u6700\u65b0\u6570\u636e\uff0c\u8bf7\u5237\u65b0\u540e\u518d\u64cd\u4f5c\uff01"
//操作失败，该建议已不存在
var i_noExitAndRefresh ="\u64cd\u4f5c\u5931\u8d25\uff0c\u8be5\u5efa\u8bae\u5df2\u4e0d\u5b58\u5728"
//操作失败，该建议已被采纳
var i_proposeHadAccept ="\u64cd\u4f5c\u5931\u8d25\uff0c\u8be5\u5efa\u8bae\u5df2\u88ab\u91c7\u7eb3"
//操作失败
var i_poposeError="\u64cd\u4f5c\u5931\u8d25"
//后台逻辑错误，请联系管理员
var i_findManager ="\u540e\u53f0\u903b\u8f91\u9519\u8bef\uff0c\u8bf7\u8054\u7cfb\u7ba1\u7406\u5458"
// 获取人员信息异常！
var i_peopleMessageError = "\u83b7\u53d6\u4eba\u5458\u4fe1\u606f\u5f02\u5e38\uff01";
// 请选择需要导入的Excel文件！
var i_needImpordExcelFile = "\u8bf7\u9009\u62e9\u9700\u8981\u5bfc\u5165\u7684Excel\u6587\u4ef6\uff01";
// 请选择正确的EXCEL文件(后缀为xls)！
var i_selectRightExcelFile = "\u8bf7\u9009\u62e9\u6b63\u786e\u7684EXCEL\u6587\u4ef6(\u540e\u7f00\u4e3axls)\uff01";
// 流程岗位
var i_flowPostion = "\u6d41\u7a0b\u5c97\u4f4d";
// 匹配岗位
var i_matchPostion = "\u5339\u914d\u5c97\u4f4d";
// 实际(基准)岗位编码
var i_treePosCode = "\u5b9e\u9645(\u57fa\u51c6)\u5c97\u4f4d\u7f16\u7801";
// 基准岗位
var i_basePostion = "\u57fa\u51c6\u5c97\u4f4d";
// 匹配类型
var i_matchType = "\u5339\u914d\u7c7b\u578b";
// 岗位编号
var i_postionCode = "\u5c97\u4f4d\u7f16\u53f7";
// 基准岗位名称
var i_basePosName = "\u57fa\u51c6\u5c97\u4f4d\u540d\u79f0";
// 基准岗位编号
var i_basePosCode = "\u57fa\u51c6\u5c97\u4f4d\u7f16\u53f7";
// 选择
var i_selectBut = "\u9009\u62e9";
// 实际岗位
var i_truePostion = "\u5b9e\u9645\u5c97\u4f4d";
// 实际岗位：
var i_truePostionC = "\u5b9e\u9645\u5c97\u4f4d\uff1a";
// 基准岗位：
var i_basePostionC = "\u57fa\u51c6\u5c97\u4f4d\uff1a";
// 添加岗位匹配
var i_addMatchPostion = "\u6dfb\u52a0\u5c97\u4f4d\u5339\u914d";
// 保存
var i_save = "\u4fdd\u5b58";
// 请选择流程岗位！
var i_selectFlowPostion = "\u8bf7\u9009\u62e9\u6d41\u7a0b\u5c97\u4f4d\uff01";
// 请选择匹配岗位！
var i_selectMatchPostion = "\u8bf7\u9009\u62e9\u5339\u914d\u5c97\u4f4d\uff01";
// 该匹配关系已存在！
var i_matchHasExists = "\u8be5\u5339\u914d\u5173\u7cfb\u5df2\u5b58\u5728\uff01";
// 错误提示
var i_errorTip = "\u9519\u8bef\u63d0\u793a";
// 请选择您要修改的数据！
var i_pleaseFixData = "\u8bf7\u9009\u62e9\u60a8\u8981\u4fee\u6539\u7684\u6570\u636e\uff01";
//编辑岗位匹配
var i_editMatchPostion="\u7f16\u8f91\u5c97\u4f4d\u5339\u914d";
//修改用户
var i_fixedUser="\u4fee\u6539\u7528\u6237";
//是否清空所有数据！
var i_isClearAllData="\u662f\u5426\u6e05\u7a7a\u6240\u6709\u6570\u636e\uff01";
//标准清单
var i_standardDirList="\u6807\u51c6\u6e05\u5355";
//保存配置数据异常！
var i_saveMatchDataError="\u4fdd\u5b58\u914d\u7f6e\u6570\u636e\u5f02\u5e38\uff01";
//保存配置数据成功！
var i_saveMatchDataSucess="\u4fdd\u5b58\u914d\u7f6e\u6570\u636e\u6210\u529f\uff01";
//字母、数字或特殊字符
var i_passTip="\u5b57\u6bcd\u3001\u6570\u5b57\u6216\u7279\u6b8a\u5b57\u7b26";
//开始时间
var i_startTime='\u5F00\u59CB\u65F6\u95F4';
//结束时间
var i_endTime='\u9884\u4F30\u7ED3\u675F\u65F6\u95F4';
//文件清单
var i_fileDetailList='\u6587\u4EF6\u6E05\u5355';
//发布时间
var i_publicTime ='\u53d1\u5e03\u65f6\u95f4';
//岗位组选择
var i_groupSelect='\u5C97\u4F4D\u7EC4';
//变更说明
var i_taskDesc='\u53D8\u66F4\u8BF4\u660E';
//达到最大下载次数提示
var downLoadCountMax='\u60A8\u5728\u5F53\u65E5\u5DF2\u7ECF\u8FBE\u5230\u6700\u5927\u4E0B\u8F7D\u6B21\u6570\uFF01';
//任务审批人替换
var i_taskManagementPeopleReplace='\u4EFB\u52A1\u5BA1\u6279\u4EBA\u66FF\u6362'
//审批人员不能为空
var i_approvePeopleIsNotNull='\u5BA1\u6279\u4EBA\u5458\u4E0D\u80FD\u4E3A\u7A7A\uFF01'
//人员替换成功！
var i_approvePeopleReplaceSuccess='\u4EBA\u5458\u66FF\u6362\u6210\u529F\uFF01'
//人员替换失败！
var i_approvePeopleReplaceFail='\u4EBA\u5458\u66FF\u6362\u5931\u8D25\uFF01'
//原审批人与新审批人不能相同！
var i_approvePeopleIsNotSame='\u539F\u5BA1\u6279\u4EBA\u4E0E\u65B0\u5BA1\u6279\u4EBA\u4E0D\u80FD\u76F8\u540C\uFF01'
//制度文件选择
var i_ruleFileSelect='\u5236\u5EA6\u6587\u4EF6\u9009\u62E9'
//流程架构选择
var selectProcessMapTitle='\u6d41\u7a0b\u67b6\u6784\u9009\u62E9'
//必须选择责任部门或流程架构！ 
var i_mustSelectOrgOrProcessMap='\u5FC5\u987B\u9009\u62E9\u8D23\u4EFB\u90E8\u95E8\u6216\u6d41\u7a0b\u67b6\u6784\uFF01'
//开始时间与结束时间都不能为空！
var i_startTimeAndEndTimeMustNotNull='\u5F00\u59CB\u65F6\u95F4\u4E0E\u7ED3\u675F\u65F6\u95F4\u90FD\u4E0D\u80FD\u4E3A\u7A7A\uFF01'
//时间段不能超过
var i_intervalTimeCannotBigThan='\u65F6\u95F4\u6BB5\u4E0D\u80FD\u8D85\u8FC7'
//年！
var i_year='\u5E74\uFF01'
//数量不能大于
var i_cannotBigThan='\u6570\u91CF\u4E0D\u80FD\u5927\u4E8E'
//日期	
var i_kpiHorVlaue='\u65e5\u671f'
//KPI值	
var i_kpiValue='KPI\u503c'
//修改KPI值
var i_updateKpiValue='\u4fee\u6539KPI\u503c'	
//编辑成功	
var i_editKpiSucess='\u7f16\u8f91\u6210\u529f'	
//编辑失败	
var i_editKpifail='\u7f16\u8f91\u5931\u8d25'	
//提交	
var i_submit='\u63d0\u4ea4'
//KPI详细信息	
var i_kpiDatil='KPI\u8be6\u7ec6\u4fe1\u606f'
//请先选择KPI
var i_qingxuanzekpi='\u8bf7\u9009\u62e9KPI'
//上传成功
var i_upsuccess='\u4e0a\u4f20\u6210\u529f'
//上传失败
var i_upfail='\u4e0a\u4f20\u5931\u8d25'
//KPI名称
var i_kpiName='KPI\u540d\u79f0'
//KPI类型
var i_kpiType='KPI\u7c7b\u578b'
//KPI单位名称	
var i_kpiVertical='KPI\u5355\u4f4d\u540d\u79f0'
//统计时间频率	
var i_kpiHorizontal='\u7edf\u8ba1\u65f6\u95f4\u9891\u7387'
//KPI目标值	
var i_kpiTarget='KPI\u76ee\u6807\u503c'
//KPI定义
var i_kpiDefinition='KPI\u5b9a\u4e49'
//流程计算公式
var i_kpiStatisticalMethods='\u6d41\u7a0b\u8ba1\u7b97\u516c\u5f0f'
//数据提供者
var i_kpiDataPeopleId='\u6570\u636e\u63d0\u4f9b\u8005'
//指标来源
var i_kpiTargetType='\u6307\u6807\u6765\u6e90'
//相关度
var i_kpiRelevance='\u76f8\u5173\u5ea6'
//支持的一级指标
var i_firstTargetId='\u652f\u6301\u7684\u4e00\u7ea7\u6307\u6807'
//必须选择责任部门！
var i_mustSelectOrg='\u5FC5\u987B\u9009\u62E9\u8D23\u4EFB\u90E8\u95E8\uFF01'
//制度选择
var i_ruleSelect ='\u5236\u5ea6\u9009\u62e9'
//请选择名称！
var i_selectName='\u8bf7\u9009\u62e9\u540d\u79f0\uff01'
//提交建议
var i_submitRtnl ='\u63d0\u4ea4\u5efa\u8bae'
//请选择文件
var i_plChooseFile ='\u8bf7\u9009\u62e9\u6587\u4ef6'
//类型：
var i_typeC = "\u7c7b\u578b\uff1a"
//制度
var i_rule ='\u5236\u5ea6'
//系统错误请联系管理员!
var i_systemErrorReportToAdministrator='\u7CFB\u7EDF\u9519\u8BEF\u8BF7\u8054\u7CFB\u7BA1\u7406\u5458\uFF01'
//数据格式错误导出失败！
var i_dataErrorInportFail='\u6570\u636E\u683C\u5F0F\u9519\u8BEF\uFF0C\u5BFC\u5165\u5931\u8D25\uFF01'
//导入成功！
var i_inportSuccess='\u5BFC\u5165\u6210\u529F\uFF01'
//导入失败！
var i_inportFail='\u5BFC\u5165\u5931\u8D25\uFF01'
//数据不能为空！
var i_dataIsCantNull='\u6570\u636E\u4E0D\u80FD\u4E3A\u7A7A\uFF01'
//数据不能小于
var i_CantLowerThan='\u6570\u636E\u4E0D\u80FD\u5C0F\u4E8E'
//数据不能大于
var i_CantBiggerThan='\u6570\u636E\u4E0D\u80FD\u5927\u4E8E'
//截止日期不能为空！
var i_expiryDateCantNull='\u622A\u6B62\u65E5\u671F\u4E0D\u80FD\u4E3A\u7A7A\uFF01'
//最多输入两位小数！
var i_theMostCanEnterTwoDecimalPlaces='\u6700\u591A\u53EA\u80FD\u8F93\u5165\u4E24\u4F4D\u5C0F\u6570\uFF01'
//附件不能超过
var i_uploadCannotBigThan='\u9644\u4EF6\u4E0D\u80FD\u8D85\u8FC7'
//提交人
var i_submitPeople='\u63D0\u4EA4\u4EBA'
//提交总数
var i_submitTotal='\u63D0\u4EA4\u603B\u6570'
//采纳率
var i_acceptPercent='\u91C7\u7EB3\u7387'
//时间
var i_time='\u65F6\u95F4'
//请选择要查询的人员！
var i_pleaseSelectQueryPeople='\u8BF7\u9009\u62E9\u8981\u67E5\u8BE2\u7684\u4EBA\u5458\uFF01'
//请选择要查询的部门！
var i_pleaseSelectQueryOrg='\u8BF7\u9009\u62E9\u8981\u67E5\u8BE2\u7684\u90E8\u95E8\uFF01'
//查询条件不能为空！
var i_queryParmsIsNotNull='\u67E5\u8BE2\u6761\u4EF6\u4E0D\u80FD\u4E3A\u7A7A\uFF01'
//提交人查询
var i_submitPeopleQuery='\u63D0\u4EA4\u4EBA\u67E5\u8BE2'
//部门查询
var i_orgQuery='\u90E8\u95E8\u67E5\u8BE2'
//详细信息查询
var i_detailInfoQuery='\u8BE6\u7EC6\u4FE1\u606F\u67E5\u8BE2'
//建议内容
var i_proposeContent='\u5EFA\u8BAE\u5185\u5BB9'
//总数
var i_total='\u603B\u6570'
//部门
var i_org='\u90E8\u95E8'
//截止时间不能为空！
var i_expiryTimeIsNotNull='\u622A\u6B62\u65F6\u95F4\u4E0D\u80FD\u4E3A\u7A7A\uFF01'
//拒绝
var i_refuse='\u62D2\u7EDD'
//操作失败，该建议已被拒绝
var i_handleFailProposeHadRefuse='\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BE5\u5EFA\u8BAE\u5DF2\u88AB\u62D2\u7EDD'
//通过
var i_pass='\u901a\u8fc7'
//打回
var i_repulse='\u6253\u56de'
	