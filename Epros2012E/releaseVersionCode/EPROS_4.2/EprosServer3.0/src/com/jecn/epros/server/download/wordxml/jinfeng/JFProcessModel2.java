package com.jecn.epros.server.download.wordxml.jinfeng;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Fonts;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ParagraphSpaceBean;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.bean.paragraph.GroupBean.Hanchor;
import wordxml.element.bean.paragraph.GroupBean.Vanchor;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.doc.RoleInfo;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;

/**
 * 金风操作说明模版
 * 
 * @author hyl
 * 
 */
public class JFProcessModel2 extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);

	public JFProcessModel2(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(15.5F);
		getDocProperty().setNewRowHeight(0.7F);
		getDocProperty().setFlowChartScaleValue(6.5F);
		setDocStyle("、", textTitleStyle());

		processDownloadBean.getRoleInfo().init();
		processDownloadBean.getActiveInfo().init();
	}

	@Override
	protected void a02(ProcessFileItem processFileItem, String... content) {
		word.createApplyScope(processFileItem);
	}

	@Override
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		createTitle(processFileItem);

		List<String[]> data = new ArrayList<String[]>();
		data.add(new String[] { "组织/部门", "角色名称", "职  责" });
		RoleInfo roleInfo = processDownloadBean.getRoleInfo();
		List<String[]> infos = roleInfo.getJinfengRoles();
		data.addAll(infos);

		Table tbl = createTableItem(processFileItem, new float[] { 3.78F, 4F, 8.41F }, data);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow());

		// table.setColStyle(0, tblContentStyle(), Valign.center);
		// table.setRowStyle(0, tblTitleStyle(),
		// docProperty.isTblTitleShadow());
	}

	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {

	}

	protected void a25(ProcessFileItem processFileItem, List<JecnRisk> riskList) {

	}

	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		word.paragraphStyleKpi(processFileItem, oldRowData);
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 2.7F, 2.54F, 2.7F, 2F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 2.7F, 2.54F, 2.7F, 2F, 1.75F);
		sectBean.setSize(21, 29.7F);
		sectBean.setPage(1F, 2F, 2.4F, 2F, 1F, 1.75F);
		titleSect.setSectBean(sectBean);
		addTitleSectContent();
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent() {

		Sect titleSect = docProperty.getTitleSect();
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 2.7F, 2.54F, 2.7F, 2F, 1.75F);
		sectBean.setSize(21, 29.7F);
		titleSect.setSectBean(sectBean);

		Paragraph emptyParagraph = createEmptyParagraph();
		// bfw
		createBFW(titleSect);
		titleSect.addParagraph(emptyParagraph);
		// 公司名称
		createTitleComponyName(titleSect);
		titleSect.addParagraph(emptyParagraph);
		standardVersion(titleSect);
		// 一条线
		createTopLine(titleSect);
		// 流程名称
		createProcessName(titleSect);
		// 密级 有效期等
		createSec(titleSect);
		// 发布行
		createPubLine(titleSect);
		// 一条线
		createBottomLine(titleSect);
		// 公司信息
		createComponyPub(titleSect);
	}

	private void createComponyPub(Sect titleSect) {
		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 14, GroupH.exact, 2).setHorizontal(3.79, Hanchor.page, 0.22)
				.setVertical(27, Vanchor.page, 0.32).lock(true);

		ParagraphBean pBean = new ParagraphBean(Align.center);
		Paragraph paragraph = new Paragraph(false);
		paragraph.setParagraphBean(pBean);
		FontBean fontBean = new FontBean("黑体", Constants.xiaoer, false);
		paragraph.appendTextRun("新疆金风科技股份有限公司", fontBean);
		fontBean = new FontBean("黑体", Constants.sihao, false);
		paragraph.appendTextRun("    发 布", fontBean);
		group.addParagraph(paragraph);
	}

	private void createSec(Sect titleSect) {
		ParagraphBean pubBean = new ParagraphBean(Align.left, Fonts.SONG, Constants.xiaosan, false);
		pubBean.setSpace(0F, 0F, new ParagraphLineRule(1, LineRuleType.AUTO));
		pubBean.setInd(0F, 0F, 0F, 8F);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 18, GroupH.exact, 8).setHorizontal(1F, Hanchor.page, 0)
				.setVertical(13.7, Vanchor.page, 0.32).lock(true);
		pubGroup.addParagraph(new Paragraph("版  本："
				+ nullToEmpty(processDownloadBean.getLatestHistoryNew() == null ? "" : processDownloadBean
						.getLatestHistoryNew().getVersionId()), pubBean));
		pubGroup.addParagraph(new Paragraph("", pubBean));
		pubGroup.addParagraph(new Paragraph("编  制： ", pubBean));
		pubGroup.addParagraph(new Paragraph("", pubBean));
		pubGroup.addParagraph(new Paragraph("审  核： ", pubBean));
		pubGroup.addParagraph(new Paragraph("", pubBean));
		pubGroup.addParagraph(new Paragraph("标准化：", pubBean));
		pubGroup.addParagraph(new Paragraph("", pubBean));
		pubGroup.addParagraph(new Paragraph("批  准： ", pubBean));

	}

	private Paragraph createEmptyParagraph() {
		Paragraph paragraph = new Paragraph("", new ParagraphBean(Align.center, "黑体", Constants.sihao, false));
		return paragraph;
	}

	private void createPubLine(Sect titleSect) {

		String pubDateStr = nullToEmpty(processDownloadBean.getReleaseDate());
		if (pubDateStr.length() > 10) {
			pubDateStr = pubDateStr.substring(0, 10);
		}
		// 2016 - 05 - 01实施 黑体 14
		String nextMonthFirstDay = "";
		if (StringUtils.isNotBlank(pubDateStr)) {
			try {
				// 下一月第一天
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date pubDate = format.parse(pubDateStr);
				Calendar cal = Calendar.getInstance();
				cal.setTime(pubDate);
				cal.add(Calendar.MONTH, 1);
				cal.set(Calendar.DAY_OF_MONTH, 1);
				nextMonthFirstDay = format.format(cal.getTime());

			} catch (ParseException e) {
				log.error(e);
			}
		}

		// 2016 - 04 - 11发布 黑体 四号
		ParagraphBean pubBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(3F, Hanchor.page, 0)
				.setVertical(26.32, Vanchor.page, 0.32).lock(true);
		pubGroup.addParagraph(new Paragraph(pubDateStr + "发布", pubBean));

		// 2016 - 04 - 11实施 黑体 四号
		ParagraphBean nextBean = new ParagraphBean(Align.right, "黑体", Constants.sihao, false);
		Group nextGroup = titleSect.createGroup();
		nextGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(11.1, Hanchor.page, 0)
				.setVertical(26.32, Vanchor.page, 0.32).lock(true);
		nextGroup.addParagraph(new Paragraph(nextMonthFirstDay + "实施", nextBean));
	}

	private void createProcessName(Sect titleSect) {
		String flowName = nullToEmpty(processDownloadBean.getFlowName());
		// 黑体 26
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		ParagraphLineRule vLine1 = new ParagraphLineRule(34F, LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph paragraph = new Paragraph(flowName, pBean);

		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 17, GroupH.exact, 2.7).setHorizontal(2.0, Hanchor.page, 0)
				.setVertical(11.27, Vanchor.page, 0).lock(true);
		group.addParagraph(paragraph);
	}

	private void createTopLine(Sect titleSect) {
		createLine(titleSect, 0.95F, 25F, 450F, 25F);
	}

	private void createBottomLine(Sect titleSect) {
		createLine(titleSect, 0.95F, 522F, 450F, 522F);
	}

	private void createLine(Sect titleSect, float startX, float startY, float endX, float endY) {
		LineGraph line = new LineGraph(LineStyle.single, 1.2F);
		line.setFrom(startX, startY);
		line.setTo(endX, endY);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		positionBean.setMarginRight(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);
	}

	private void standardVersion(Sect titleSect) {
		ParagraphBean pBean = new ParagraphBean(Align.right, Fonts.ARIAL, Constants.sihao, false);
		ParagraphLineRule vLine1 = new ParagraphLineRule(14, LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph vision = new Paragraph(nullToEmpty(processDownloadBean.getFlowInputNum()), pBean);

		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 17, GroupH.exact, 1.46).setHorizontal(1.59F, Hanchor.page, 0)
				.setVertical(6.67F, Vanchor.page, 0).lock(true);
		group.addParagraph(vision);
	}

	private void createTitleComponyName(Sect titleSect) {

		/*** 最小值 分散对齐 *****/
		ParagraphLineRule vLine = new ParagraphLineRule(0F, LineRuleType.AT_LEAST);
		ParagraphBean pBean = new ParagraphBean(Align.distribute, "黑体", Constants.xiaoyi, false);
		pBean.setSpace(0f, 0f, vLine);
		titleSect.createParagraph("新疆金风科技股份有限公司企业标准", pBean);

	}

	private void createBFW(Sect titleSect) {
		ParagraphBean pBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sishiba, true);
		Paragraph zcfcParagraph = new Paragraph("Q/GW", pBean);
		titleSect.addParagraph(zcfcParagraph);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommfhdr(firstSect);
		ParagraphBean paragraphBean = new ParagraphBean(Align.center, "黑体", Constants.sanhao, false);
		firstSect.createParagraph("前    言", paragraphBean);
		firstSect.createMutilEmptyParagraph(2);
		ParagraphBean pBean = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		// 首行缩进两个字符
		pBean.setParagraphSpaceBean(new ParagraphSpaceBean(0F, 0F, new ParagraphLineRule(1, LineRuleType.AUTO)));
		pBean.setInd(0F, 0F, 0F, 1F);
		firstSect.createParagraph("本标准由新疆金风科技股份有限公司提出并归口。", pBean);
		firstSect.createParagraph("本标准起草单位：风机业务单元××中心×××部。", pBean);
		firstSect.createParagraph("本标准主要起草人：×××  ×××。", pBean);
		firstSect.createParagraph("本标准代替标准的历次版本发布情况:首次发布。------发布的是哪版就写哪版。", pBean);
		firstSect.createParagraph("如为修订版本，按以下内容：------如果是修订版本，这句删除。", pBean);
		firstSect.createParagraph("本标准替代并废止××流程，修订内容如下：-----编号《名称》版本号", pBean);
		firstSect.createParagraph("—	增加了×××××××（见××条款）；", pBean);
		firstSect.createParagraph("—	删除了×××××××（见原版本××条款）。", pBean);
		firstSect.createParagraph("—	……", pBean);
		firstSect.createParagraph("本标准代替标准的历次版本的发布情况为：", pBean);
		firstSect.createParagraph("—	编号《名称》，版本号：× ，编制人：××× ", pBean);
		firstSect.breakPage();
		firstSect.createParagraph(processDownloadBean.getFlowName(), new ParagraphBean(Align.center, Fonts.HEI,
				Constants.sanhao, false));
		firstSect.createParagraph("");
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		createCommfhdr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getCharSectBean();
		flowChartSect.setSectBean(sectBean);
		createCharfhdr(flowChartSect);

	}

	private SectBean getCharSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.76F, 1.76F);
		sectBean.setSize(29.7F, 21F);
		return sectBean;
	}

	private List<String[]> getActivityList2() {
		// 活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出 6办理时限目标值 7办理时限原始值 8角色id
		// allActivityShowList
		List<String[]> list = new ArrayList<String[]>();
		for (Object[] obj : processDownloadBean.getAllActivityShowList()) {
			String[] arr = new String[9];
			list.add(arr);
			Long artivityId = Long.valueOf(obj[8].toString());
			int i = 0;
			arr[i++] = nullToEmpty(obj[0]);
			// 角色
			arr[i++] = nullToEmpty(obj[1]);
			arr[i++] = nullToEmpty(obj[2]);
			arr[i++] = nullToEmpty(obj[3]);
			arr[i++] = nullToEmpty(nullToEmpty(obj[7]) + "/" + nullToEmpty(obj[6]));
			arr[i++] = nullToEmpty(getRiskNum(artivityId));
			arr[i++] = nullToEmpty(obj[4]);
			arr[i++] = nullToEmpty(obj[5]);
			arr[i++] = nullToEmpty(getIt(artivityId));
		}
		return list;
	}

	private String getIt(Long artivityId) {
		return processDownloadBean.getActiveInfo().getItName(artivityId);
	}

	private String getOrg(Long activeId) {
		return processDownloadBean.getRoleInfo().getJinfengOrgName(activeId);
	}

	private String getRiskNum(Long artivityId) {
		return processDownloadBean.getActiveInfo().getRiskNum(artivityId);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, float left, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaowu, false);
		Header header = sect.createHeader(HeaderOrFooterType.odd);

		TableBean tableBean = new TableBean();
		Table table = header.createTable(tableBean, new float[] { left, right });
		table.createTableRow(new String[] { "", nullToEmpty(processDownloadBean.getFlowInputNum()) }, pBean);

		ParagraphBean paragraphBean = new ParagraphBean(Align.right, Fonts.ARIAL, Constants.xiaowu, false);
		table.getRow(0).getCell(1).getParagraph(0).setParagraphBean(paragraphBean);
		table.setCellValignAndHeight(Valign.bottom, 0.6F);

		table.getRow(0).getCell(0).createParagraph(getLogoImage(4.79F, 1.01F));

		header.addParagraph(new Paragraph("", new ParagraphBean(PStyle.AF1)));
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 8F, 8F);
		createCommftr(sect);
	}

	/***
	 * 创建流程图页眉页脚
	 * 
	 * @param sect
	 */
	private void createCharfhdr(Sect sect) {
		createCommhdr(sect, 8F, 16F);
	}

	/**
	 * 流程图横向
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0.1F, 0.1F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
        processFileItem.getBelongTo().breakPage();
		// 添加活动说明
		addActivity(processFileItem);
		processFileItem.getBelongTo().createMutilEmptyParagraph(1);
		// 添加相关风险
		addRisk(processFileItem);
	}

	private void addRisk(ProcessFileItem processFileItem) {
		processFileItem.setItemTitle("相关风险 ");
		createTitle(processFileItem);
		Sect sect = processFileItem.getBelongTo();
		Table table = sect.createTable(tblStyle(), new float[] { 2.69F, 8.75F, 12.5F, 2.61F });
		table.createTableRow(new String[] { "编号", "关键点描述", "控制措施", "是否EHS风险" }, tblTitleStyle());
		List<JecnRisk> risks = processDownloadBean.getRilsList();
		if (risks != null && !risks.isEmpty()) {
			for (JecnRisk risk : risks) {
				String[] data = new String[4];
				data[0] = nullToEmpty(risk.getRiskCode());
				data[1] = nullToEmpty(risk.getRiskName());
				data[2] = nullToEmpty(risk.getStandardControl());
				data[3] = nullToEmpty(getRiskTypeStr(risk.getRiskType()));
				table.createTableRow(data, tblContentStyle());
			}
		}
		table.setRowStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow(), true);
	}

	private String getRiskTypeStr(int riskType) {
		JecnDictionary dic = JecnConfigTool.getDictinary(DictionaryEnum.JECN_RISK_TYPE, riskType);
		return dic == null ? "" : nullToEmpty(dic.getValue());
	}

	private void addActivity(ProcessFileItem processFileItem) {
		processFileItem.setItemTitle("活动说明 ");
		createTitle(processFileItem);
		Sect sect = processFileItem.getBelongTo();
		Table table = sect.createTable(tblStyle(), new float[] { 1.3F, 1.96F, 2.79F, 6.37F, 2.02F, 2.24F, 3.85F, 3.85F,
				2.23F });
		table.createTableRow(new String[] { "步骤编号 ", "角色", "流程图步骤 ", "标准流程步骤描述 ", "完成时限（现状/目标）（单位：H） ", "风险编号", "输入 ",
				"输出 ", "涉及的系统 " }, new ParagraphBean(Align.center, Fonts.SONG, Constants.xiaowu, true));
		List<String[]> rowData = getActivityList2();
		table.createTableRow(rowData, new ParagraphBean(Align.left, Fonts.SONG, Constants.xiaowu, false));
		table.setValign(Valign.center);
		table.setRowStyle(0, new ParagraphBean(Align.center, Fonts.SONG, Constants.xiaowu, true), docProperty
				.isTblTitleShadow(), true);
	}

	private void addTitleSectContent(Sect titleSect) {
		// 宋体 四号
		ParagraphBean stshBean = new ParagraphBean(Align.center, "宋体", Constants.sihao, false);
		// 宋体 五号
		ParagraphBean stwhBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		for (int i = 0; i < 3; i++) {
			titleSect.createParagraph("");
		}
		// Times New Roman 48 号
		ParagraphBean gcBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sishiba, true);
		titleSect.createParagraph("GC", gcBean);

		// 公司名称文件
		// 黑体 小一
		ParagraphBean htxyBean = new ParagraphBean(Align.distribute, "黑体", Constants.xiaoyi, false);
		titleSect.createParagraph(processDownloadBean.getCompanyName() + "文件", htxyBean);
		// 流程编号
		// Times New Roman 4 号
		ParagraphBean bhBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sihao, false);
		titleSect.createParagraph(processDownloadBean.getFlowInputNum(), bhBean);
		titleSect.createParagraph("");
		LineGraph lineGraph = new LineGraph(LineStyle.single, 0.1F);
		lineGraph.setFrom(-12.3F, 69.75F);
		lineGraph.setTo(479.9F, 69.75F);
		titleSect.createParagraph(lineGraph);

		for (int i = 0; i < 7; i++) {
			titleSect.createParagraph("");
		}
		// 流程名称
		// 黑体 一号
		ParagraphBean htyhBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		titleSect.createParagraph(processDownloadBean.getFlowName(), htyhBean);
		for (int i = 0; i < 3; i++) {
			titleSect.createParagraph("", htyhBean);
		}
		int num = 0;
		if (processDownloadBean.getDocumentList() != null) {
			num += processDownloadBean.getDocumentList().size();
		}
		String version = processDownloadBean.getFlowVersion() == "" ? "  " : processDownloadBean.getFlowVersion();
		// 版本号
		String flowVersion = "第" + version + "版" + "  修订" + num + "次";
		titleSect.createParagraph(flowVersion, stshBean);
		titleSect.createParagraph("分发号：01", stwhBean);

		for (int i = 0; i < 13; i++) {
			titleSect.createParagraph("", htyhBean);
		}

		// 发布时间
		String releaseDateStr = processDownloadBean.getReleaseDate();
		String effectiveStr = "";
		if (StringUtils.isNotEmpty(releaseDateStr)) {
			Date release = JecnUtil.StrToDate(releaseDateStr);
			// 实施时间 发布时间+15
			Date effective = JecnUtil.addDay(release, 15);
			releaseDateStr = JecnUtil.DateToStr(release, "yyyy-MM-dd") + "发布";
			effectiveStr = JecnUtil.DateToStr(effective, "yyyy-MM-dd") + "实施";
		} else {
			releaseDateStr = "未发布";
			effectiveStr = "未实施";
		}
		List<String[]> rowData = new ArrayList<String[]>();
		String[] str = new String[] { releaseDateStr, effectiveStr };
		rowData.add(str);
		// 黑体 四号
		ParagraphBean tabLPBean = new ParagraphBean("黑体", Constants.sihao, false);

		ParagraphBean tabRPBean = new ParagraphBean(Align.right, "黑体", Constants.sihao, false);
		TableBean tblBean = new TableBean();

		Table tab = JecnWordUtil.createTab(titleSect, tblBean, new float[] { 8.5F, 8.5F }, rowData, tabLPBean);
		tab.getRow(0).getCell(1).getParagraph(0).setParagraphBean(tabRPBean);

		lineGraph = new LineGraph(LineStyle.single, 0.1F);
		lineGraph.setFrom(-12.3F, 69.75F);
		lineGraph.setTo(480.9F, 69.75F);
		titleSect.createParagraph(lineGraph);
		// 发布
		// 黑体 四号 居中
		ParagraphBean htshBean = new ParagraphBean(Align.center, "黑体", Constants.sihao, false);
		String comPubTitle = "  发  布";
		if (StringUtils.isNotEmpty(releaseDateStr)) {
			comPubTitle = "";
		}
		titleSect.createParagraph(processDownloadBean.getCompanyName() + comPubTitle, htshBean);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.right, "黑体", Constants.wuhao, false);
		// pBean.setpStyle(PStyle.AF1);
		pBean.setSpace(0f, 0f, vLine1);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		header.createParagraph(processDownloadBean.getFlowInputNum(), pBean);
	}

	/**
	 * 创建通用页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.center, "Arial", Constants.xiaowu, false);
		pBean.setSpace(0f, 0f, vLine1);
		Footer footer = sect.createFooter(HeaderOrFooterType.odd);
		Paragraph p = footer.createParagraph("", pBean);
		p.appendCurPageRun();
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(2F);
		tblStyle.setBorderInsideH(1F);
		tblStyle.setBorderInsideV(1F);
		// 表格左缩进0.01 厘米
		tblStyle.setTableLeftInd(0.01F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0F, 0, 0F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitleStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitleStyle.setSpace(0f, 0f, vLine1);
		return tblTitleStyle;
	}

	// 宋体 五号 首行2字符
	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体", Constants.xiaosi, false);
		textContentStyle.setSpace(0f, 0f, vLine1);
		textContentStyle.setInd(0.1F, 0, 0F, 0.7F);
		return textContentStyle;
	}

	// 黑体 五号 单倍行距
	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Fonts.SONG, Constants.xiaosi, true);
		textTitleStyle.setSpace(1F, 1F, vLine1);
		// 悬挂缩进默认0.74 会导致序号 超过10了增加两倍故设置0.9
		textTitleStyle.setInd(0F, 0, 0.9F, 0);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
