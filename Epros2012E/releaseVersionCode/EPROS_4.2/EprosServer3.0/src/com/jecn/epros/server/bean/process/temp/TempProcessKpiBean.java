package com.jecn.epros.server.bean.process.temp;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowKpiName;

/**
 * 流程KPI数据临时处理类
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Jan 16, 2013 时间：3:43:25 PM
 */
public class TempProcessKpiBean {
	/** 流程ID */
	private Long flowId;
	/** 流程名称 */
	private String flowName;
	/** KPI名称 */
	private List<String> listKpiNames;
	/** KPI横线单位名称 */
	private List<String> listKpiValues;
	/** 记录流程对应的所有KPI名称 */
	private List<JecnFlowKpiName> listFlowKpiName;
	/** KPI名称 */
	private List<Long> kpiNameIds;

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public List<String> getListKpiNames() {
		return listKpiNames;
	}

	public void setListKpiNames(List<String> listKpiNames) {
		this.listKpiNames = listKpiNames;
	}

	public List<String> getListKpiValues() {
		return listKpiValues;
	}

	public void setListKpiValues(List<String> listKpiValues) {
		this.listKpiValues = listKpiValues;
	}

	public List<JecnFlowKpiName> getListFlowKpiName() {
		return listFlowKpiName;
	}

	public void setListFlowKpiName(List<JecnFlowKpiName> listFlowKpiName) {
		this.listFlowKpiName = listFlowKpiName;
	}

	public List<Long> getKpiNameIds() {
		return kpiNameIds;
	}

	public void setKpiNameIds(List<Long> kpiNameIds) {
		this.kpiNameIds = kpiNameIds;
	}
}
