package epros.draw.gui.line.resize;

import java.awt.Color;
import java.awt.Cursor;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.constant.JecnWorkflowConstant.ReSizeLineType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.swing.JecnBaseResizePanel;

/**
 * 
 * 线三个选中点，其具有改变线大小、位置点的功能
 * 
 * @author ZHOUXY
 * 
 */
public class JecnLineResizePanel extends JecnBaseResizePanel {

	/** 线选中点类型 start 开始点,center线段中心点,end 结束点 */
	private ReSizeLineType reSizeLineType = null;

	/** 选中点相关对象 */
	private LineSegment lineSegment = null;

	private JecnLineResizePanel(ReSizeLineType reSizeLineType,
			LineSegment lineSegment) {
		if (reSizeLineType == null) {

			JecnLogConstants.LOG_JECN_BASE_RESIZE_PANEL
					.error("JecnFigureResizePanel类构造函数：参数为null。reSizeLineType="
							+ reSizeLineType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		// 显示点位置 start，center，end
		this.reSizeLineType = reSizeLineType;
		this.lineSegment = lineSegment;
		// 默认背景色为绿色
		this.setBackground(Color.GREEN);
		this.setSize(6, 6);
	}

	/**
	 * 鼠标移动到线段显示点，鼠标状态设置
	 * 
	 * @param lineDirectionEnum
	 */
	public void setMouseCursor(LineDirectionEnum lineDirectionEnum) {
		switch (lineDirectionEnum) {
		case horizontal:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
			break;
		case vertical:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 创建开始选中点
	 * 
	 * @return
	 */
	public static JecnLineResizePanel createStartLineResizePanel(
			LineSegment lineSegment) {
		return new JecnLineResizePanel(ReSizeLineType.start, lineSegment);
	}

	/**
	 * 
	 * 创建中间选中点
	 * 
	 * @return
	 */
	public static JecnLineResizePanel createCenterLineResizePanel(
			LineSegment lineSegment) {
		return new JecnLineResizePanel(ReSizeLineType.center, lineSegment);
	}

	/**
	 * 
	 * 创建结束选中点
	 * 
	 * @return
	 */
	public static JecnLineResizePanel createEndLineResizePanel(
			LineSegment lineSegment) {
		return new JecnLineResizePanel(ReSizeLineType.end, lineSegment);
	}

	public ReSizeLineType getReSizeLineType() {
		return reSizeLineType;
	}

	public LineSegment getLineSegment() {
		return lineSegment;
	}
}
