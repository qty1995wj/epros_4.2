package epros.designer.gui.system.config.ui.bottom;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import epros.designer.gui.JecnD2;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.JecnAbstractJPanel;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnFlowConfigJDialog;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;

/**
 * 
 * 确认取消按钮面板
 * 
 * @author Administrator
 * 
 */
public class JecnOKCancelJButtonPanel extends JecnAbstractJPanel {
	/** 确认按钮 */
	private JecnOKJButton okJButton = null;
	/** 取消按钮 */
	private JecnCancelJButton cancelJButton = null;
	/** 空白标签 */
	private JLabel label = null;
	/** 信息提示标签 */
	private JecnUserInfoTextArea infoLabel = null;

	public JecnOKCancelJButtonPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
		initLayout();
	}

	protected void initComponents() {

		this.setPreferredSize(new Dimension(100, 60));

		// 确认按钮
		okJButton = new JecnOKJButton(dialog);
		// 取消按钮
		cancelJButton = new JecnCancelJButton(dialog);
		// 空白标签
		label = new JLabel();
		// 信息提示标签
		infoLabel = new JecnUserInfoTextArea();
		// D2 无浏览端标准版
		if (JecnConstants.versionType == 1 && JecnD2.isTmpKey && dialog != null
				&& dialog instanceof JecnFlowConfigJDialog) {
			infoLabel.setText(JecnProperties.getValue("notSupportSystemSet"));
		}

		okJButton.setText(JecnProperties.getValue("task_okBtn"));
		cancelJButton.setText(JecnProperties.getValue("task_cancelBtn"));

		label.setOpaque(false);
		label.setBorder(null);
		label.setLayout(new BorderLayout());

		infoLabel.setVisible(true);
	}

	private void initLayout() {
		// 空白标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.BOTH, new Insets(15, 5, 0, 5), 0, 0);
		this.add(label, c);

		// 确认按钮
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				new Insets(26, 7, 13, 10), 0, 0);
		this.add(okJButton, c);

		// 取消按钮
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				new Insets(26, 0, 13, 7), 0, 0);
		this.add(cancelJButton, c);

		label.add(infoLabel);
	}

	/**
	 * 
	 * 设置按钮是否和编辑状态
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initBtnsEnabled(JecnConfigTypeDesgBean configTypeDesgBean) {
		if (configTypeDesgBean == null) {// 不合法
			okJButton.setEnabled(false);
			return;
		}
		// 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
		// 7：任务操作配置8：消息邮件配置9:合理化建议10:流程属性;
		// 11:发布流程地图，流程，制度 相关文件是否同步发布配置12：流程地图。流程图。制度；相关文件是否可查阅配置
		switch (configTypeDesgBean.getType()) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 14:
		case 15:
		case 16:
		case 18:
			okJButton.setEnabled(true);
			break;
		default:
			okJButton.setEnabled(false);
			break;
		}
	}

	public JecnOKJButton getOkJButton() {
		return okJButton;
	}

	public JecnCancelJButton getCancelJButton() {
		return cancelJButton;
	}

	public JecnUserInfoTextArea getInfoLabel() {
		return infoLabel;
	}

	/**
	 * 
	 * 设置提示信息以及确认按钮
	 * 
	 * @param text
	 *            String 提示信息
	 */
	public void updateBtnAndInfoText(String text) {
		if (DrawCommon.isNullOrEmtryTrim(text)) {
			infoLabel.setText("");
			if (!okJButton.isEnabled()) {
				okJButton.setEnabled(true);
			}

		} else {
			infoLabel.setText(text);
			if (okJButton.isEnabled()) {
				okJButton.setEnabled(false);
			}
		}
	}
}
