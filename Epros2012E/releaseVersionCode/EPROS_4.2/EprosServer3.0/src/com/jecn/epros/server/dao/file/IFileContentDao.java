package com.jecn.epros.server.dao.file;

import com.jecn.epros.server.bean.file.JecnFileContent;
import com.jecn.epros.server.common.IBaseDao;

public interface IFileContentDao extends IBaseDao<JecnFileContent, Long> {

}
