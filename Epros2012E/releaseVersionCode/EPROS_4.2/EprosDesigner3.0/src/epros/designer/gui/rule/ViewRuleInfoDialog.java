package epros.designer.gui.rule;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/***
 * 查看制度模板信息
 * 
 * @author hyl
 * 
 */
public class ViewRuleInfoDialog extends EditRuleInfoDialog {

	public ViewRuleInfoDialog(Long comId,TreeNodeType treeNodeType) {
		super(comId,treeNodeType,true);
	}
}
