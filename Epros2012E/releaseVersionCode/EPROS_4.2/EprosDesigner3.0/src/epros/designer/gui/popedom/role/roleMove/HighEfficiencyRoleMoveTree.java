package epros.designer.gui.popedom.role.roleMove;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.popedom.role.JecnRoleCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class HighEfficiencyRoleMoveTree extends JecnHighEfficiencyTree {
	private Logger log = Logger.getLogger(HighEfficiencyRoleMoveTree.class);
	/** 当前项目根节点下的角色目录 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	public HighEfficiencyRoleMoveTree(List<Long> listIds) {
		super(listIds);
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RoleMoveTreeListener(this.getListIds(), jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getJecnRole().getChildRoleDirs(0L, JecnConstants.projectId,
					JecnDesignerCommon.getSecondAdminUserId());
		} catch (Exception e) {
			log.error("HighEfficiencyRoleMoveTree getTreeModel is error！", e);
		}
		return JecnRoleCommon.getTreeMoveModel(list, this.getListIds());
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
