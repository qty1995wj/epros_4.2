package com.jecn.webservice.zhongsh.flowpush.bean;

import java.util.ArrayList;
import java.util.List;

public class WBDecision {
	private String guid;
	private String stateGUID;
	private List<WBDecisionAction> wBDecisionActionCollection=new ArrayList<WBDecisionAction>();

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getStateGUID() {
		return stateGUID;
	}

	public void setStateGUID(String stateGUID) {
		this.stateGUID = stateGUID;
	}

	public List<WBDecisionAction> getwBDecisionActionCollection() {
		return wBDecisionActionCollection;
	}

	public void setwBDecisionActionCollection(List<WBDecisionAction> wBDecisionActionCollection) {
		this.wBDecisionActionCollection = wBDecisionActionCollection;
	}

}
