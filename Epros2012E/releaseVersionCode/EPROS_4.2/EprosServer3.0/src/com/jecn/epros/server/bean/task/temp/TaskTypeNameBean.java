package com.jecn.epros.server.bean.task.temp;

/**
 * 任务类型中文名称记录
 * 
 * @author Administrator
 * @date： 日期：Nov 30, 2012 时间：2:45:17 PM
 */
public class TaskTypeNameBean {
	/** 任务类型 */
	private int taskType;
	/** 任务类型名称 */
	private String typeName;

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
