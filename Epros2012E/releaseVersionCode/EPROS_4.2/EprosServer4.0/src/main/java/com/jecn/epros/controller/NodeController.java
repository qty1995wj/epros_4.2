package com.jecn.epros.controller;

import com.jecn.epros.configuration.rpc.RPCServiceConfiguration;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.selectFile.SearchSelectBean;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.security.SecurityValidatorData;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.route.RouteService;
import com.jecn.epros.service.selectFile.SelectFileService;
import com.jecn.epros.util.HttpUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "/nodes", description = "树节点", position = 10)
public class NodeController {

    @Autowired
    private SelectFileService selectFileService;

    @Autowired
    private RouteService routeService;

    @ApiOperation(value = "树节点-搜索")
    @RequestMapping(value = "/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNode>> SearchSelectFile(@ModelAttribute SearchSelectBean searchSelectBean) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("type", searchSelectBean.getType());
        map.put("name", searchSelectBean.getName());
        return ResponseEntity.ok(selectFileService.searchName(map));
    }

    @ApiOperation(value = "树节点-展开")
    @RequestMapping(value = "/nodes/{id}/children", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNode>> findSelectFileTreeChild(
            @ApiParam(value = "父节点Id", required = true) @PathVariable Long id,
            @ApiParam(value = "数据类型['process'：流程及流程架构 'rule'：制度 'standard'：标准 'risk'：风险 'file'：文件 'organization'：组织和岗位 'people':人员]", required = true)
            @RequestParam String type,
            @ApiParam(value = "限制返回的数据类型（其实就是点击的那个节点的类型），当 type='people' 时可用", allowableValues = "organization,position")
            @RequestParam(required = false, defaultValue = "organization") String accept,
            @ApiParam(value = "顶级虚拟架构类型")
            @RequestParam(required = false) TreeNodeUtils.NodeType inventedType,
            @ApiParam(value = "顶级虚拟架构id")
            @RequestParam(required = false) Long inventedId) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("pId", id);
        map.put("type", type);
        map.put("inventedType", inventedType);
        map.put("inventedId", inventedId);
        if ("people".equals(type)) {
            map.put("accept", accept);
        }
        List<TreeNode> result = null;
        if (inventedType != null && inventedId != null) {// 虚拟树展开
            result = selectFileService.inventedTreeNodes(map);
        } else {
            result = selectFileService.treeNodes(map);
        }
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "树节点-父级路径")
    @RequestMapping(value = "/nodes/{id}/parents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNode>> findNodeRoute(
            @ApiParam(value = "文件Id", required = true) @PathVariable Long id,
            @ApiParam(value = "数据类型[1：流程及流程架构 2：制度 3：标准 4：风险 5：文件]", required = true, allowableValues = "1,2,3,4,5")
            @RequestParam Integer type) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        map.put("type", type);
        return ResponseEntity.ok(routeService.findNodeRoute(map));
    }

    //    @PreAuthorize("resource(#id,#type,#from)")
    @ApiOperation(value = "树节点")
    @RequestMapping(value = "/nodes/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TreeNode> findSelectFileTree(
            @ApiParam(value = "当前节点Id", required = true) @PathVariable Long id,
            @ApiParam(value = "数据类型['process'：流程及流程架构 'rule'：制度 'standard'：标准 'risk'：风险 'file'：文件 'organization'：组织 'position':岗位 'people':人员]", required = true)
            @RequestParam String type,
            @ApiParam(value = "0 未发布  1 发布", required = false)
            @RequestParam(defaultValue = "1", required = false) int pubState,
            @ApiParam(value = "所属资源['process'：流程及流程架构 'rule'：制度]", required = false)
            @RequestParam String from) {
        TreeNode treeNode = getTreeNode(id, type, pubState, from);
        return ResponseEntity.ok(treeNode);
    }

    private TreeNode getTreeNode(Long id, String type, int pubState, String from) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        map.put("type", type);
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        if (pubState == 1) {
            map.put("pub", "");
        } else {
            map.put("pub", "_t");
        }
        TreeNode treeNode = selectFileService.treeNode(map);
        if (treeNode == null) {
            // 未发布情况，不处理权限
            treeNode = new TreeNode();
            treeNode.setStatus("402");
        } else {
            if (pubState == 1) {
                // 权限认证
                boolean status = SecurityValidatorData.resource(Long.valueOf(treeNode.getId()), treeNode.getType(), from);
                if (!status) {
                    // 无权限状态码403
                    treeNode.setStatus("403");
                }
            }
        }
        return treeNode;
    }


    @ApiOperation(value = "树节点定位")
    @RequestMapping(value = "/nodes/{id}/orientate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TreeNode> findSelectFileTreeOrientate(
            @ApiParam(value = "父节点Id", required = true) @PathVariable Long id,
            @ApiParam(value = "树类型['process'：流程及流程架构 'rule'：制度 'standard'：标准 'risk'：风险 'file'：文件 'organization'：组织和岗位 ]", required = true)
            @RequestParam String type,
            @ApiParam(value = "组织或岗位定位时，须指明具体定位的节点类型", allowableValues = "organization,position", defaultValue = "organization")
            @RequestParam(required = false, defaultValue = "organization") String orientateType) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("id", id);
        map.put("type", type);
        if ("organization".equals(type)) {
            map.put("orientateType", orientateType);
        }
        return ResponseEntity.ok(selectFileService.treeNodesOrientate(map));
    }

    @ApiOperation(value = "树节点")
    @RequestMapping(value = "/nodes/{id}/getNodeViewUrl", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, String>> getNodeViewUrl(
            @ApiParam(value = "当前节点Id", required = true) @PathVariable Long id,
            @ApiParam(value = "数据类型['process'：流程及流程架构 'rule'：制度 'standard'：标准 'risk'：风险 'file'：文件 'organization'：组织 'position':岗位 'people':人员]", required = true)
            @RequestParam String type,
            @ApiParam(value = "0 未发布  1 发布", required = false)
            @RequestParam(defaultValue = "1", required = false) int pubState) {

        TreeNode treeNode = getTreeNode(id, type, pubState, "");
        if ("402".equals(treeNode.getStatus()) || "403".equals(treeNode.getStatus())) {
            return ResponseEntity.ok(null);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("pubState", pubState);
        map.put("treeNode", treeNode);
        map.put("id", treeNode.getId());
        Map<String, String> resultMap = new HashedMap();
        resultMap.put("isDownloadAuth", String.valueOf(treeNode.isDownloadAuth()));
        resultMap.put("resource", selectFileService.getFileLink(map).getUrl());
        resultMap.put("rpcUrl", RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL);
        return ResponseEntity.ok(resultMap);
    }
}