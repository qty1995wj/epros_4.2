package com.jecn.epros.server.bean.process;

/**
 * JecnFlowBasicInfo entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class JecnFlowBasicInfo implements java.io.Serializable {

	private Long flowId;// 主键ID（流程ID）
	private String flowName;// 流程名称
	private String flowPurpose;// 流程目的
	private String flowStartpoint;// 起始活动
	private String flowEndpoint;// 终止活动
	private String input;// 流程输入
	private String ouput;// 流程输出
	private String applicability;// 适用范围
	private String noutGlossary;// 术语定义
	private Long typeId;// 类别Id
	private String typeName;// 类别名称
	private String flowCustom;// 流程客户
	private Long fileId;// 附件
	private Long isShowLineX;// 是否显示横分割线（1是显示）
	private Long isShowLineY;// 是否显示纵分割线（1是显示）
	private Long isXorY;// 0横向,1纵向
	private Long driveType;// 0事件驱动,1是时间驱动
	private String driveRules;// 驱动规则
	private Long testRunNumber;// 试运行编号0试运行1,正式版,2升级版
	private Integer typeResPeople;// 0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人)
	private Long resPeopleId;// 流程责任人Id
	private String resPeopleName;// 流程责任人
	private String flowSummarize; // 概况信息
	private String noApplicability; // 不适用范围
	private String flowSupplement; // 补充说明
	private Long expiry;// 有效期
	private Long guardianId;// 监护人ID
	private Long fictionPeopleId;//流程拟制人 ID
	private String flowRelatedFile;//相关文件
	private String flowCustomOne;//自定义要素1
	private String flowCustomTwo;//自定义要素2
	private String flowCustomThree;//自定义要素3
	private String flowCustomFour;//自定义要素4
	private String flowCustomFive;//自定义要素5

	public String getFlowRelatedFile() {
		return flowRelatedFile;
	}

	public void setFlowRelatedFile(String flowRelatedFile) {
		this.flowRelatedFile = flowRelatedFile;
	}

	public String getFlowCustomOne() {
		return flowCustomOne;
	}

	public void setFlowCustomOne(String flowCustomOne) {
		this.flowCustomOne = flowCustomOne;
	}

	public String getFlowCustomTwo() {
		return flowCustomTwo;
	}

	public void setFlowCustomTwo(String flowCustomTwo) {
		this.flowCustomTwo = flowCustomTwo;
	}

	public String getFlowCustomThree() {
		return flowCustomThree;
	}

	public void setFlowCustomThree(String flowCustomThree) {
		this.flowCustomThree = flowCustomThree;
	}

	public String getFlowCustomFour() {
		return flowCustomFour;
	}

	public void setFlowCustomFour(String flowCustomFour) {
		this.flowCustomFour = flowCustomFour;
	}

	public String getFlowCustomFive() {
		return flowCustomFive;
	}

	public void setFlowCustomFive(String flowCustomFive) {
		this.flowCustomFive = flowCustomFive;
	}

	public Long getFictionPeopleId() {
		return fictionPeopleId;
	}

	public void setFictionPeopleId(Long fictionPeopleId) {
		this.fictionPeopleId = fictionPeopleId;
	}

	public Long getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(Long guardianId) {
		this.guardianId = guardianId;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowPurpose() {
		return flowPurpose;
	}

	public void setFlowPurpose(String flowPurpose) {
		this.flowPurpose = flowPurpose;
	}

	public String getFlowStartpoint() {
		return flowStartpoint;
	}

	public void setFlowStartpoint(String flowStartpoint) {
		this.flowStartpoint = flowStartpoint;
	}

	public String getFlowEndpoint() {
		return flowEndpoint;
	}

	public void setFlowEndpoint(String flowEndpoint) {
		this.flowEndpoint = flowEndpoint;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOuput() {
		return ouput;
	}

	public void setOuput(String ouput) {
		this.ouput = ouput;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String getNoutGlossary() {
		return noutGlossary;
	}

	public void setNoutGlossary(String noutGlossary) {
		this.noutGlossary = noutGlossary;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getFlowCustom() {
		return flowCustom;
	}

	public void setFlowCustom(String flowCustom) {
		this.flowCustom = flowCustom;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Long getIsShowLineX() {
		return isShowLineX;
	}

	public void setIsShowLineX(Long isShowLineX) {
		this.isShowLineX = isShowLineX;
	}

	public Long getIsShowLineY() {
		return isShowLineY;
	}

	public void setIsShowLineY(Long isShowLineY) {
		this.isShowLineY = isShowLineY;
	}

	public Long getIsXorY() {
		return isXorY;
	}

	public void setIsXorY(Long isXorY) {
		this.isXorY = isXorY;
	}

	public Long getDriveType() {
		return driveType;
	}

	public void setDriveType(Long driveType) {
		this.driveType = driveType;
	}

	public String getDriveRules() {
		return driveRules;
	}

	public void setDriveRules(String driveRules) {
		this.driveRules = driveRules;
	}

	public Long getTestRunNumber() {
		return testRunNumber;
	}

	public void setTestRunNumber(Long testRunNumber) {
		this.testRunNumber = testRunNumber;
	}

	public Integer getTypeResPeople() {
		return typeResPeople;
	}

	public void setTypeResPeople(Integer typeResPeople) {
		this.typeResPeople = typeResPeople;
	}

	public Long getResPeopleId() {
		return resPeopleId;
	}

	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}

	public String getResPeopleName() {
		return resPeopleName;
	}

	public void setResPeopleName(String resPeopleName) {
		this.resPeopleName = resPeopleName;
	}

	public String getFlowSummarize() {
		return flowSummarize;
	}

	public void setFlowSummarize(String flowSummarize) {
		this.flowSummarize = flowSummarize;
	}

	public String getNoApplicability() {
		return noApplicability;
	}

	public void setNoApplicability(String noApplicability) {
		this.noApplicability = noApplicability;
	}

	public String getFlowSupplement() {
		return flowSupplement;
	}

	public void setFlowSupplement(String flowSupplement) {
		this.flowSupplement = flowSupplement;
	}

	public Long getExpiry() {
		return expiry;
	}

	public void setExpiry(Long expiry) {
		this.expiry = expiry;
	}
}