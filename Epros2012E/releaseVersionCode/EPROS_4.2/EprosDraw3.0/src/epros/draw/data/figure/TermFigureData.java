package epros.draw.data.figure;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;

public class TermFigureData extends JecnFigureData {

	/** 术语定义 */
	private String termDefine;

	public TermFigureData(MapElemType mapElemType) {
		super(mapElemType);
	}

	/**
	 * 
	 * 实例化对象
	 * 
	 */
	public TermFigureData newInstance() {
		return new TermFigureData(mapElemType);
	}

	@Override
	/**
	 * 
	 * 克隆对象
	 * 
	 */
	public TermFigureData clone() {
		return (TermFigureData) this.currClone();
	}

	/**
	 * 
	 * 
	 * 复制流程元素属性
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		TermFigureData src = (TermFigureData) srcFlowElementData;
		TermFigureData dest = (TermFigureData) destFlowElementData;
		
		dest.setTermDefine(src.getTermDefine());
	}

	public String getTermDefine() {
		return termDefine;
	}

	public void setTermDefine(String termDefine) {
		this.termDefine = termDefine;
	}

}
