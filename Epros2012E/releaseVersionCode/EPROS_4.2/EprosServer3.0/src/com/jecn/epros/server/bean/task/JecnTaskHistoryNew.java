package com.jecn.epros.server.bean.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 发布流程、制度版本信息记录
 * 
 * @author ZHANGXIAOHU
 * 
 */
public class JecnTaskHistoryNew implements java.io.Serializable {
	/** 主键ID */
	private Long id;
	/** 拟稿人 */
	private String draftPerson;
	/** 关联Id */
	private Long relateId;
	/** 关联文件名称  不存数据库*/
	private String relateName;
	/** 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图 */
	private Integer type;
	/** 版本 */
	private String versionId;
	/** 变更说明 */
	private String modifyExplain;
	/** 发布日期 */
	private Date publishDate;
	/** 开始时间 */
	private String startTime;
	/** 结束时间 */
	private String endTime;
	/** 返工次数 */
	private Long approveCount;
	/** 编号 */
	private String numberId;
	/** type 是文件或者制度文件时 相关文本版本主键ID */
	private Long fileContentId;
	/** 生成的版本信息文件 存储生成的时间文件路径， 系统指定目录 + （时间和文件类型 ＋ id.doc） */
	private String filePath;
	/** 文件版本：0试运行；1升级；2正式 */
	private int testRunNumber;
	/** 记录任务审批阶段名称和各阶段审批人名称 */
	private List<JecnTaskHistoryFollow> listJecnTaskHistoryFollow = new ArrayList<JecnTaskHistoryFollow>();
    /** 流程有效期（流程或者流程地图 ）由于数据库是必填项，所以给个初始值   0代表永久*/
	private Long expiry=0L;
	/** 任务id*/
	private Long taskId;
	/**下一次审视优化的时间*/
	private Date newScanDate;
	/**实施日期*/
	private Date implementDate;
	
	public Date getImplementDate() {
		return implementDate;
	}

	public void setImplementDate(Date implementDate) {
		this.implementDate = implementDate;
	}

	public Date getNewScanDate() {
		return newScanDate;
	}

	public void setNewScanDate(Date newScanDate) {
		this.newScanDate = newScanDate;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Long getExpiry() {
		return expiry;
	}

	public void setExpiry(Long expiry) {
		if (null == expiry) {//保证有效期永远有值
			this.expiry = 0L;
		} else {
			this.expiry = expiry;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRelateId() {
		return relateId;
	}

	public void setRelateId(Long relateId) {
		this.relateId = relateId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getModifyExplain() {
		return modifyExplain;
	}

	public void setModifyExplain(String modifyExplain) {
		this.modifyExplain = modifyExplain;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Long getApproveCount() {
		return approveCount;
	}

	public void setApproveCount(Long approveCount) {
		this.approveCount = approveCount;
	}

	public String getNumberId() {
		return numberId;
	}

	public void setNumberId(String numberId) {
		this.numberId = numberId;
	}

	public Long getFileContentId() {
		return fileContentId;
	}

	public void setFileContentId(Long fileContentId) {
		this.fileContentId = fileContentId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getTestRunNumber() {
		return testRunNumber;
	}

	public void setTestRunNumber(int testRunNumber) {
		this.testRunNumber = testRunNumber;
	}

	public List<JecnTaskHistoryFollow> getListJecnTaskHistoryFollow() {
		return listJecnTaskHistoryFollow;
	}

	public void setListJecnTaskHistoryFollow(
			List<JecnTaskHistoryFollow> listJecnTaskHistoryFollow) {
		this.listJecnTaskHistoryFollow = listJecnTaskHistoryFollow;
	}

	public String getDraftPerson() {
		return draftPerson;
	}

	public void setDraftPerson(String draftPerson) {
		this.draftPerson = draftPerson;
	}

	public String getRelateName() {
		return relateName;
	}

	public void setRelateName(String relateName) {
		this.relateName = relateName;
	}

}
