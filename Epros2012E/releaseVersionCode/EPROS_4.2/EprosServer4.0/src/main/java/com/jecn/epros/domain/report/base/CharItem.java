package com.jecn.epros.domain.report.base;

import java.util.ArrayList;
import java.util.List;

/**
 * 图表的一项数据
 * 比如name可以作为x轴的坐标value可以作为纵坐标的数据
 */
public class CharItem {

    private String id;
    private String name;
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
