package com.jecn.epros.server.bean.popedom;
/**
 * 部门查阅权限
 * @author Administrator
 *
 */
public class JecnOrgAccessPermissionsT extends BaseAccess implements java.io.Serializable{
	/**
	* 主键Id
	 */
	private Long id;
	/**
	 * 部门Id
	 */
	private Long orgId;
	/**
	 * 部门名称
	 */
	private String orgName;
	/**
	 * 关联Id
	 */
	private Long relateId;
	/**
	 * 0是流程、1是文件、2是标准、3制度
	 */
	private Integer type;
	/**
	 * 关联名称
	 */
	private String relateName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Long getRelateId() {
		return relateId;
	}
	public void setRelateId(Long relateId) {
		this.relateId = relateId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getRelateName() {
		return relateName;
	}
	public void setRelateName(String relateName) {
		this.relateName = relateName;
	}
}
