package com.jecn.epros.server.bean.process;

import java.util.ArrayList;
import java.util.List;


/**
 * 流程接口描述信息
 * @author user
 *
 */
public class ProcessInterfacesDescriptionBean {
	
	/** 父节点id**/
	private Long pid;
	private String pname;
	private String pnumber;
	private List<ProcessInterface> uppers=new ArrayList<ProcessInterface>();
	private List<ProcessInterface> lowers=new ArrayList<ProcessInterface>();
	private List<ProcessInterface> procedurals=new ArrayList<ProcessInterface>();
	private List<ProcessInterface> sons=new ArrayList<ProcessInterface>();
	
	public Long getPid() {
		return pid;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getPnumber() {
		return pnumber;
	}
	public void setPnumber(String pnumber) {
		this.pnumber = pnumber;
	}
	public List<ProcessInterface> getUppers() {
		return uppers;
	}
	public void setUppers(List<ProcessInterface> uppers) {
		this.uppers = uppers;
	}
	public List<ProcessInterface> getLowers() {
		return lowers;
	}
	public void setLowers(List<ProcessInterface> lowers) {
		this.lowers = lowers;
	}
	public List<ProcessInterface> getProcedurals() {
		return procedurals;
	}
	public void setProcedurals(List<ProcessInterface> procedurals) {
		this.procedurals = procedurals;
	}
	public List<ProcessInterface> getSons() {
		return sons;
	}
	public void setSons(List<ProcessInterface> sons) {
		this.sons = sons;
	}

	
	
	
}
