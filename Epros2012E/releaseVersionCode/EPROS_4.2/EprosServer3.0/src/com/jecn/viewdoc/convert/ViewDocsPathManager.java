package com.jecn.viewdoc.convert;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnPath;

public class ViewDocsPathManager {

	protected static final String ROOT_DIR = JecnPath.APP_PATH;

	protected static final String ROOT = "/viewdocs";

	public static final String VIEWER_JS = "pdfjs/web/viewer.jsp";

	protected static final String FILE_DIR = ROOT_DIR + ROOT + "/file/";

	protected static final String FILE_T_DIR = ROOT_DIR + ROOT + "/file_t/";

	protected static final String RULE_DIR = ROOT_DIR + ROOT + "/rule/";

	protected static final String PROCESS_FILE = ROOT_DIR + ROOT + "/processFile/";
	
	protected static final String PROCESS = ROOT_DIR + ROOT + "/process/";
	
	protected static final String TEMP_DIR = ROOT_DIR + ROOT + "/temp/";

	public static final String OPENOFFICE = JecnContants.jecn_path + JecnFinal.FILE_DIR + "OpenOffice 4/program";

	public static final String RELAVTIVE_PATH = "../../";
}
