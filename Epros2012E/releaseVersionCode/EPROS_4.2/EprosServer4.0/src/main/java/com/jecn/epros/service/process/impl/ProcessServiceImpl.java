package com.jecn.epros.service.process.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.bean.checkout.CheckoutItem;
import com.jecn.epros.bean.checkout.CheckoutResult;
import com.jecn.epros.bean.checkout.CheckoutRoleItem;
import com.jecn.epros.bean.checkout.CheckoutRoleResult;
import com.jecn.epros.domain.*;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.common.relatedfiles.RelatedFileBean;
import com.jecn.epros.domain.file.FileBaseBean;
import com.jecn.epros.domain.file.FileBeanT;
import com.jecn.epros.domain.file.FileDirCommonBean;
import com.jecn.epros.domain.inventory.FlowInventoryBean;
import com.jecn.epros.domain.org.Department;
import com.jecn.epros.domain.process.*;
import com.jecn.epros.domain.process.kpi.*;
import com.jecn.epros.domain.process.processFile.BaseProcessFileBean;
import com.jecn.epros.domain.process.processFile.OperationFile;
import com.jecn.epros.domain.process.processFile.ProcessMapFileExplain;
import com.jecn.epros.domain.report.InventoryBaseInfo;
import com.jecn.epros.domain.system.Dictionary;
import com.jecn.epros.mapper.*;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.*;
import com.jecn.epros.service.TreeNodeUtils.TreeType;
import com.jecn.epros.service.common.CommonService;
import com.jecn.epros.service.process.ProcessFileEnum;
import com.jecn.epros.service.process.ProcessMapEnum;
import com.jecn.epros.service.process.ProcessService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant.TYPEAUTH;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.ConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.Utils;
import com.jecn.epros.util.excel.ExcelUtils;
import com.jecn.epros.util.excel.InventoryStyleManager;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 流程体系 service
 *
 * @author ZXH
 */
@Service
@Transactional
public class ProcessServiceImpl implements ProcessService {
    @Autowired
    private ProcessMapper processMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrgMapper orgMapper;
    @Autowired
    private FileMapper fileMapper;
    @Autowired
    private ConfigItemMapper configItemMapper;
    @Autowired
    private ActiveMapper activeMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private InventoryMapper inventoryMapper;
    @Autowired
    private StandardMapper standardMapper;
    @Autowired
    private DictionaryMapper dictionaryMapper;
    @Autowired
    private IProcessRPCService processRPCService;

    private boolean isHistory;

    @Override
    public List<TreeNode> findChildFlows(Map<String, Object> map) {
        map.put("typeAuth", TYPEAUTH.FLOW);
        List<TreeNode> treeBeans = processMapper.findChildFlowsAdmin(map);
        treeBeans = ServiceExpandTreeData.findExpandNodesByDataList(treeBeans, TreeType.process);
        TreeNodeUtils.setNodeType(treeBeans, TreeType.process);
        return treeBeans;
    }


    /**
     * 流程概况信息
     *
     * @param map
     * @return
     */
    @Override
    public ProcessBaseAttribute findBaseInfoBean(Map<String, Object> map) {
        Long historyId = (Long) map.get("historyId");
        // 1、流程主表信息
        FlowStructure structureInfo = processMapper.findFlowStructureByMap(map);
        ProcessBaseAttribute baseInfoBean = new ProcessBaseAttribute();
        String isPub = (String) map.get("isPub");
        // 1、流程名称
        if (structureInfo.getNodeType() == 1) {
            // 获取流程（文件）
            LinkResource linkProcess = new LinkResource(structureInfo.getFlowId(), structureInfo.getFlowName(), ResourceType.PROCESS_FILE);
            //流程名称
            ProcessAttribute processAttr = new ProcessAttribute(JecnProperties.getValue("processNameC"), null, linkProcess);

            processAttr.getDownLoadHrefURL(StringUtils.isBlank(isPub) ? true : false, structureInfo.getFileContentId());
            baseInfoBean.getList().add(processAttr);
        } else {
            baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("processNameC"), structureInfo.getFlowName(), null));
        }
        // 2、流程编号
        baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("processNumC"), structureInfo.getFlowIdInput(), null));
        // 流程basicInfo基本信息
        // T.FLOW_CUSTOM,T.FLOW_STARTPOINT,T.FLOW_ENDPOINT,T.INPUT,T.OUPUT,
        // T.TYPE_ID,T.EXPIRY,T.FICTION_PEOPLE_ID,T.WARD_PEOPLE_ID,
        // T.TYPE_RES_PEOPLE, T.RES_PEOPLE_ID,T.FILE_ID
        Map<String, Object> basicInfoMap = processMapper.findFlowBasicInfo(map);
        // 3、流程类别
        if (ConfigUtils.valueIsOneThenTrue("4")) {
            String typeName = "";
            if (basicInfoMap.get("TYPE_ID") != null) {
                typeName = processMapper.findFlowType(valueOfLong(basicInfoMap.get("TYPE_ID")));
            }
            //流程类别
            baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("processTypeC"), typeName, null));
        }

        // 3、流程业务类型
        if (ConfigUtils.valueIsOneThenTrue("processShowBussType")) {
            String typeName = "";
            if (structureInfo.getBussType() != null && !"".equals(structureInfo.getBussType().trim()) && !"0".equals(structureInfo.getBussType().trim())) {
                if (structureInfo.getBussType() != null) {
                    Map<String, Object> doMap = new HashedMap();
                    doMap.put("TYPE", "BUSS_TYPE");
                    doMap.put("CODE", structureInfo.getBussType());
                    Dictionary dictionary = dictionaryMapper.getDictionaryItemByType(doMap);
                    if (dictionary != null) {
                        typeName = dictionary.getValue();
                    }
                }
            }
            //流程业务类型
            baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("BUSS_TYPEC"), typeName, null));
        }

        // 4、责任人ID
        if (basicInfoMap.get("RES_PEOPLE_ID") != null) {
            baseInfoBean.getList().add(getResPeopleName(basicInfoMap));
        }
        // 专员
        if ("1".equals(ConfigUtils.getValue("isShowProcessCommissioner"))) { // 关键字
            if (structureInfo.getCommissionerId() != null && structureInfo.getCommissionerId() != 0) {
                baseInfoBean.getList().add(getCommissionerName(structureInfo.getCommissionerId()));
            }
        }
        // 关键字
        if ("1".equals(ConfigUtils.getValue("isShowProcessKeyWord"))) { // 关键字
            if (structureInfo.getKeyWord() != null && structureInfo.getKeyWord().length() > 0) {
                baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("keyWordC"), structureInfo.getKeyWord(), null));
            }
        }
        // 5、 流程监护人
        if (ConfigUtils.valueIsOneThenTrue(ConfigItemPartMapMark.guardianPeople.toString())) { // 流程监护人
            // 流程监护人ID 和 名称
            if (basicInfoMap.get("WARD_PEOPLE_ID") != null) {
                Long peopleId = valueOfLong(basicInfoMap.get("WARD_PEOPLE_ID"));
                String trueName = userMapper.findUserNameById(peopleId);
                trueName = IfytekCommon.getIflytekUserName(trueName, 0);
                baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("processGuardianC"), null,
                        new LinkResource(peopleId.toString(), trueName, ResourceType.PERSON)));
            }
        }
        // 6、流程拟制人
        if (ConfigUtils.valueIsOneThenTrue(ConfigItemPartMapMark.fictionPeople.toString())) { // 流程拟制人
            // 流程拟制人ID 和名称
            if (basicInfoMap.get("FICTION_PEOPLE_ID") != null) {
                Long peopleId = valueOfLong(basicInfoMap.get("FICTION_PEOPLE_ID"));
                String trueName = userMapper.findUserNameById(peopleId);
                trueName = IfytekCommon.getIflytekUserName(trueName, 0);
                baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("processDraftingC"), null,
                        new LinkResource(peopleId.toString(), trueName, ResourceType.PERSON)));
            }
        }
        // 7、责任部门
        Map<String, Object> resOrgMap = processMapper.findFlowResOrg(map);
        if (resOrgMap != null) {
            baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("responsibilityDepartmentC"), null,
                    new LinkResource(valueOfString(resOrgMap.get("ORG_ID")), valueOfString(resOrgMap.get("ORG_NAME")), ResourceType.ORGANIZATION)));
        } else {
            baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("responsibilityDepartmentC"), null, null));
        }
        // 8、有效期
        baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("termOfValiditC"), !"0".equals(valueOfString(basicInfoMap.get("EXPIRY"))) ? valueOfString(basicInfoMap.get("EXPIRY")) : JecnProperties.getValue("permanent"), null));
        // 保密级别
        if (ConfigUtils.valueIsOneThenTrue(ConfigItemPartMapMark.securityLevel.toString())) {
            baseInfoBean.getList().add(new ProcessAttribute(ConfigUtils.getSecurityLevelTitleC(), structureInfo.getConfidentialityLevelStr(), null));
        }
        // 9、密级
        baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("scopeOfAuthorityC"), ConfigUtils.getSecretByType(structureInfo.getIsPublic()), null));

        // 支持工具
        if (ConfigUtils.valueIsOneThenTrue(ConfigItemPartMapMark.supportTools.toString())) { // 支持工具
            baseInfoBean.getList().add(addFlowSustainTools(map));
        }

        // 10、附件
        if (ConfigUtils.valueIsOneThenTrue(ConfigItemPartMapMark.enclosure.toString())) { // 附件
            if (basicInfoMap.get("FILE_ID") != null && !"".equals(basicInfoMap.get("FILE_ID"))) {
                String fileName = "";
                FileBeanT file = fileMapper.getFileContent(Long.valueOf(basicInfoMap.get("FILE_ID").toString()));
                if (file != null) {
                    fileName = file.getFileName();
                    baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("enclosure"), null,
                            new LinkResource(valueOfString(basicInfoMap.get("FILE_ID")), fileName, ResourceType.FILE)));
                } else {
                    baseInfoBean.getList().add(new ProcessAttribute(JecnProperties.getValue("enclosure"), null, null));
                }
            }
        }
        String isApp = map.get("isApp").toString();
        if ("false".equals(isApp)) {
            return baseInfoBean;
        }
        ProcessLink processLink = CommonServiceUtil.getProcessNameLink(structureInfo, isApp, historyId);
        baseInfoBean.setDocLink(processLink.getDocLink());
        baseInfoBean.setImageLink(processLink.getImageLink());
        return baseInfoBean;
    }

    private ProcessAttribute addFlowSustainTools(Map<String, Object> map) {
        List<String> sustainTools = processMapper.getFlowSustainTool(map);
        StringBuffer buffer = new StringBuffer();
        if (sustainTools != null) {
            for (String name : sustainTools) {
                if (buffer.length() > 0) {
                    buffer.append("/").append(name);
                } else {
                    buffer.append(name);
                }
            }
        }
        return new ProcessAttribute(JecnProperties.getValue("supportTools"), buffer.toString(), null);
    }

    /**
     * 获取流程相关KPI名称
     *
     * @param map
     * @return
     */
    @Override
    public List<FlowKPIBean> findKpiNames(Map<String, Object> map) {
        return processMapper.findKpiNames(map);
    }

    @Override
    public KpiSearchResultData findKpiValuesByDate(Map<String, Object> map) {
        if (map.get("kpiAndId") == null || map.get("startTime") == null || map.get("endTime") == null) {
            return null;
        }
        Date startDate = Common.getDateByString(map.get("startTime").toString());
        String tempTime = Common.getStringbyDate(startDate, "yyyy-MM");
        // 查询的开始时间，默认为每月01号开始
        map.put("startTime", tempTime + "-01");
        // 产生所选时间段之内的日期的集合
        map.put("isPub", "");
        Map<String, Object> kpiMap = processMapper.getFlowKpiMap(map);
        List<KPIValuesBean> values = processMapper.findKpiValuesByDate(map);
        // 数据提供者 KPI_DATA_PEOPEL_ID
        Object dataPeople = kpiMap.get("KPI_DATA_PEOPEL_ID");
        boolean operation = AuthenticatedUserUtil.isAdmin();
        if (!operation && dataPeople != null) {
            operation = dataPeople.toString().equals(AuthenticatedUserUtil.getPeopleId().toString());
        }
        KpiSearchResultData searchResultData = new KpiSearchResultData();
        searchResultData.setOperation(operation);
        searchResultData.setKpiValues(getResultKpiValues(map.get("kpiAndId").toString(), kpiMap.get("KPI_HORIZONTAL").toString(), map.get("startTime").toString(), map.get("endTime").toString(), values));
        return searchResultData;
    }

    private List<KPIValuesBean> getResultKpiValues(String kpiAndId, String horizontalType, String startTime, String endTime, List<KPIValuesBean> values) {
        Map<String, KPIValuesBean> dbValues = kpiListToMap(values);
        List<KPIValuesBean> resultValues = new ArrayList<>();
        Calendar calendarbeg = getCalendar(startTime, horizontalType);
        Calendar calendarEnd = getCalendar(endTime, horizontalType);
        int num = 1;
        if ("1".equals(horizontalType)) {// 季度*******
            num = 3;// KPi按照季度查询*yyyy03、yyyy06、yyyy09、yyyy12
        }
        KPIValuesBean kpiValuesBean = null;
        while (true) {
            if (calendarbeg.compareTo(calendarEnd) > 0) {
                break;
            }
            String tempTime = Common.getStringbyDate(calendarbeg.getTime(), "yyyy-MM");
            if (dbValues.get(tempTime) == null) {
                kpiValuesBean = new KPIValuesBean();
                kpiValuesBean.setHorValue(tempTime);
                resultValues.add(kpiValuesBean);
            } else {
                resultValues.add(dbValues.get(tempTime));
            }
            calendarbeg.add(Calendar.MONTH, num);
        }
        return resultValues;
    }

    private Map<String, KPIValuesBean> kpiListToMap(List<KPIValuesBean> values) {
        Map<String, KPIValuesBean> resultMap = new HashMap<>();
        for (KPIValuesBean kpiVal : values) {
            kpiVal.setHorValue(kpiVal.getHorValue().substring(0, 7));
            resultMap.put(kpiVal.getHorValue(), kpiVal);
        }
        return resultMap;
    }

    /**
     * 根据输入的日期返回一个日历对象
     *
     * @param dateStr String yyyy-MM-dd
     * @return
     */
    private Calendar getCalendar(String dateStr, String kpiType) {
        String[] dateArray = dateStr.split("-");
        int year = Integer.valueOf(dateArray[0]).intValue();
        int month = Integer.valueOf(dateArray[1]).intValue();
        int day = 1;
        Calendar c = Calendar.getInstance();
        // 0为月 1为季度
        if ("1".equals(kpiType)) {
            month = getMonth(month);
        }
        c.set(year, month - 1, day);
        return c;
    }

    private int getMonth(int number) {
        int month = 3;
        if (1 <= number && number < 4) {
            month = 3;
        } else if (4 <= number && number < 7) {
            month = 6;
        } else if (7 <= number && number < 10) {
            month = 9;
        } else if (10 <= number) {
            month = 12;
        }
        return month;
    }

    /**
     * 流程KPI详情
     *
     * @param map
     */
    @Override
    public List<KPIInfo> findKpiInfos(Map<String, Object> map) {
        if (map.get("kpiAndId") == null) {
            return null;
        }
        map.put("typeBigModel", 1);
        map.put("typeSmallModel", 16);
        List<ConfigItemBean> itemBeans = configItemMapper.findConfigItems(map);
        if (itemBeans == null) {
            return null;
        }
        FlowKpiName flowKpi = processMapper.findKpiNameById(map);
        List<KPIInfo> kpiInfos = new ArrayList<>();
        KPIInfo kpiInfo = null;
        for (ConfigItemBean configItem : itemBeans) {
            if (configItem.getMark().equals("allowSupplierKPIData")) {
                continue;
            }
            kpiInfo = new KPIInfo();
            kpiInfo.setTitle(configItem.getName(AuthenticatedUserUtil.isLanguageType()));
            getKpiInfo(kpiInfo, configItem, flowKpi);
            kpiInfos.add(kpiInfo);
        }
        return kpiInfos;
    }

    /**
     * 保存流程KPI值
     *
     * @param map
     * @param kpiValuesBeans
     */
    @Override
    public void saveKpiValues(Map<String, Object> map, List<KPIValuesBean> kpiValuesBeans) {
        Long kpiAndId = (Long) map.get("kpiAndId");
        Long peopleId = (Long) map.get("peopleId");
        Date date = new Date();
        for (KPIValuesBean kpiValuesBean : kpiValuesBeans) {
            insertKpiValue(kpiAndId, peopleId, kpiValuesBean);
        }
    }

    private void insertKpiValue(Long kpiAndId, Long peopleId, KPIValuesBean kpiValue) {
        Map<String, Object> insertMap = new HashMap<>();
        Date date = new Date();
        insertMap.put("kpiAndId", kpiAndId);
        insertMap.put("kpiValue", kpiValue.getKpiValue());
        insertMap.put("kpiHorVlaue", Common.getDateByString(kpiValue.getHorValue(), "yyyy-MM"));
        insertMap.put("createTime", date);
        insertMap.put("updateTime", date);
        insertMap.put("createPeopleId", peopleId);
        insertMap.put("updatePeopleId", peopleId);
        processMapper.insertKpi(insertMap);
    }


    public void updateKpiValue(Map<String, Object> map, KPIValuesBean kpiValuesBean) {
        Long kpiAndId = (Long) map.get("kpiAndId");
        Long peopleId = (Long) map.get("peopleId");
        if (kpiValuesBean.getKpiId() == null) {
            insertKpiValue(kpiAndId, peopleId, kpiValuesBean);
        } else {
            processMapper.updateKpi(kpiValuesBean);
        }
    }

    @Override
    public List<SearchResultBean> searchProcess(Map<String, Object> map, SearchBean searchBean) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "FLOW_ID");
        return null;
    }

    private void getKpiInfo(KPIInfo kpiInfo, ConfigItemBean configItem, FlowKpiName flowKpi) {
        if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiName.toString())) {
            kpiInfo.setValue(flowKpi.getKpiName());
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiType.toString())) {// KPI类型
            // 0为结果性指标、1为过程性指标
            kpiInfo.setValue(ConfigUtils.getKpiType(flowKpi.getKpiType()));
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiUtilName.toString())) {// KPI单位名称
            // KPI值单位名称
            kpiInfo.setValue(ConfigUtils.getKpiVertical(Integer.valueOf(flowKpi.getKpiVertical())));
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiTargetValue.toString())) {// KPI目标值
            if (flowKpi.getKpiTarget() != null) {
                kpiInfo.setValue(flowKpi.getKpiTarget() + ConfigUtils.getKpiTargetOperator(flowKpi.getKpiTargetOperator()));
            }
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiTimeAndFrequency.toString())) {// 数据统计时间频率
            kpiInfo.setValue(ConfigUtils.getKpiHorizontal(Integer.valueOf(flowKpi.getKpiHorizontal())));
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDefined.toString())) {// KPI定义
            // KPI定义
            kpiInfo.setValue(flowKpi.getKpiDefinition());
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDesignFormulas.toString())) {// 流程KPI计算公式
            // 流程计算公式
            kpiInfo.setValue(flowKpi.getKpiStatisticalMethods());
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDataProvider.toString())) {// 数据提供者
            // 数据提供者
            if (flowKpi.getKpiDataPeopleId() != null) {
                long peopleId = flowKpi.getKpiDataPeopleId();
                String trueName = userMapper.findUserNameById(peopleId);
                kpiInfo.setValue(trueName);
            }
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDataAccess.toString())) {// 数据获取方式
            kpiInfo.setValue(ConfigUtils.getKpiDataMethod(flowKpi.getKpiDataMethod()));
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiIdSystem.toString())) {// IT系统
            // 支持工具
            if (flowKpi.getKpiAndId() != null) {
                Map<String, Object> params = new HashMap<>();
                params.put("kpiId", flowKpi.getKpiAndId());
                params.put("pub", "");
                List<FlowSustainTool> tools = processMapper.fetchFlowSustainToolByKpiId(params);
                kpiInfo.setValue(tools.stream().map(FlowSustainTool::getFlowSustainToolShow).collect(Collectors.joining(",")));
            }
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiSourceIndex.toString())) {// 指标来源
            kpiInfo.setValue(ConfigUtils.getKpiTargetType(flowKpi.getKpiTargetType()));
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiRrelevancy.toString())) {// 相关度
            // 相关度
            kpiInfo.setValue(ConfigUtils.getKpiRelevance(flowKpi.getKpiRelevance()));
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiSupportLevelIndicator.toString())) {// 支撑的一级指标
            // 支持的一级指标
            String firstTargetId = flowKpi.getFirstTargetId();
            if (StringUtils.isNotBlank(firstTargetId)) {
                FirstTarget firstTarget = processMapper.getFirstTarget(firstTargetId);
                kpiInfo.setValue(firstTarget.getTargetContent());
            }
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiPurpose.toString())) {// 设置目的
            kpiInfo.setValue(flowKpi.getKpiPurpose());
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiPoint.toString())) {// 测量点
            kpiInfo.setValue(flowKpi.getKpiPoint());
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiPeriod.toString())) {// 统计周期
            kpiInfo.setValue(flowKpi.getKpiPeriod());
        } else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiExplain.toString())) {// 说明
            kpiInfo.setValue(flowKpi.getKpiExplain());
        }
    }

    private ProcessAttribute getResPeopleName(Map<String, Object> basicInfoMap) {
        if (basicInfoMap.get("RES_PEOPLE_ID") != null) {
            LinkResource linkResource = null;
            String id = valueOfString(basicInfoMap.get("RES_PEOPLE_ID"));
            if (StringUtils.isNotBlank(id)) {
                if (valueOfInteger(basicInfoMap.get("TYPE_RES_PEOPLE")) == 0) {
                    String trueName = userMapper.findUserNameById(Long.valueOf(id));
                    trueName = IfytekCommon.getIflytekUserName(trueName, 0);
                    linkResource = new LinkResource(id, trueName, ResourceType.PERSON);
                } else {// 责任人为岗位
                    String postName = orgMapper.findPostNameById(Long.valueOf(id));
                    linkResource = new LinkResource(id, postName, ResourceType.POSITION);
                }
            }
            return new ProcessAttribute(JecnProperties.getValue("personLiableC"), null, linkResource);
        }
        return null;
    }

    private ProcessAttribute getCommissionerName(Long id) {
        if (id != null) {
            LinkResource linkResource = null;
            String trueName = userMapper.findUserNameById(Long.valueOf(id));
            trueName = IfytekCommon.getIflytekUserName(trueName, 0);
            linkResource = new LinkResource(id, trueName, ResourceType.PERSON);
            return new ProcessAttribute(JecnProperties.getValue("commissionerC"), null, linkResource);
        }
        return null;
    }

    private String replaceString(String str) {
        return str.replaceAll("\n", "<br>").replaceAll(" ", "&nbsp;");
    }

    private Long valueOfLong(Object obj) {
        return obj == null ? -1 : Long.parseLong(obj.toString());
    }

    private String valueOfString(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    private int valueOfInteger(Object obj) {
        return obj == null ? -1 : Integer.parseInt(obj.toString());
    }

    /**
     * 流程文件（流程操作说明）
     *
     * @param map
     * @return ShowProcessFile
     */
    public OperationFile getShowProcessFile(Map<String, Object> map) {
        OperationFile showProcessFile = new OperationFile();
        // 活动信息集合
        List<Map<String, Object>> activityMaps = null;
        // 角色信息集合
        List<Map<String, Object>> roleMaps = null;
        // 角色活动对应关系集合
        List<Map<String, Object>> roleRelatedActiveMaps = null;
        // 活动输入集合
        List<Map<String, Object>> activityInputMaps = null;
        // 活动输出集合
        List<Map<String, Object>> activityOutputMaps = null;
        // 操作规范集合
        List<Map<String, Object>> activityOperateMaps = null;
        map.put("typeBigModel", 1);
        map.put("typeSmallModel", 4);

        // 1、 流程基本信息
        FlowBasicInfo baseInfo = processMapper.getBasicInfo(map);
        if (baseInfo == null) {
            return null;
        }
        // 操作说明配置
        List<ConfigItemBean> configItemBeanList = configItemMapper.findConfigItems(map);
        if (configItemBeanList.isEmpty()) {
            return showProcessFile;
        }

        // 活动信息集合
        activityMaps = processMapper.findActivityListByFlowId(map);
        // 角色信息集合
        roleMaps = processMapper.findRoleListByFlowId(map);
        // 角色活动对应关系集合
        roleRelatedActiveMaps = processMapper.findRoleRelatedActiveByFlowId(map);
        // 活动输入集合
        activityInputMaps = processMapper.findActivityInputByFlowId(map);
        // 活动输出集合
        activityOutputMaps = processMapper.findActivityOutputByFlowId(map);
        // 操作规范集合
        activityOperateMaps = processMapper.findActivityOperateByFlowId(map);

        List<BaseProcessFileBean> processFileBeans = new ArrayList<>();

        Map<String, Object> paramMaps = new HashMap<>();
        paramMaps.put("processMapper", processMapper);
        paramMaps.put("baseInfo", baseInfo);
        paramMaps.put("controlMap", map);
        paramMaps.put("activityMaps", activityMaps);
        paramMaps.put("roleMaps", roleMaps);
        paramMaps.put("roleRelatedActiveMaps", roleRelatedActiveMaps);
        paramMaps.put("activityInputMaps", activityInputMaps);
        paramMaps.put("activityOutputMaps", activityOutputMaps);
        paramMaps.put("activityOperateMaps", activityOperateMaps);
        paramMaps.put("userMapper", userMapper);
        paramMaps.put("orgMapper", orgMapper);
        paramMaps.put("configItemMapper", configItemMapper);
        paramMaps.put("standardMapper", standardMapper);

        int isPub = map.get("isPub").equals("_T") ? 0 : 1;
        showProcessFile.setPublish(isPub);
        int languageType = AuthenticatedUserUtil.isLanguageType();
        for (ConfigItemBean configItemBean : configItemBeanList) {
            BaseProcessFileBean processFileBean = ProcessFileEnum.INSTANCE.createProcessFile(configItemBean.getMark(),
                    configItemBean.getName(languageType), paramMaps, isPub);
            if (processFileBean == null) {
                continue;
            }
            // 默认显示序号
            processFileBean.setShowIndex(true);
            processFileBeans.add(processFileBean);
        }

        showProcessFile.setId(baseInfo.getFlowId());
        showProcessFile.setName(baseInfo.getFlowName());
        showProcessFile.setType("process");
        showProcessFile.setFileBeans(processFileBeans);
        return showProcessFile;
    }

    /**
     * 流程架构 文件说明
     *
     * @param map
     * @return @
     */
    @Override
    public ProcessMapFileExplain getProcessMapFileExplain(Map<String, Object> map) {
        map.put("typeBigModel", 0);
        map.put("typeSmallModel", 4);
        // 操作说明配置
        List<ConfigItemBean> configItemBeanList = configItemMapper.findConfigItems(map);
        if (configItemBeanList.isEmpty()) {
            return new ProcessMapFileExplain();
        }
        FlowStructure flowStructure = processMapper.findFlowStructureByMap(map);

        Map<String, Object> processMap = processMapper.findProcessMapFile(map);
        if (processMap == null) {
            return new ProcessMapFileExplain();
        }
        ProcessMapFileExplain ProcessMapFileExplain = ProcessMapEnum.INSTANCE.createMapProcessFile(configItemBeanList,
                processMap);

        LinkResource mapLink = new LinkResource(flowStructure.getFlowId(), flowStructure.getFlowName(),
                ResourceType.PROCESS_MAP);
        ProcessMapFileExplain.setMapLink(mapLink);
        return ProcessMapFileExplain;
    }

    @Override
    public Inventory getInventory(Map<String, Object> paramMap) {
        String downloadType = paramMap.get("downloadType") == null ? "0" : paramMap.get("downloadType").toString();
        Boolean isPaging = (Boolean) paramMap.get("isPaging");
        List<FlowInventoryBean> flowsAndMaps = new ArrayList<>();
        if (isPaging) {
            // 查询分页的流程
            Paging paging = (Paging) paramMap.get("paging");
            // 通过view_sort分页查询流程
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "view_sort");
            List<String> paths = inventoryMapper.fetchPaths(paramMap);
            if (paths != null && paths.size() > 0) {
                // 将查询到的流程和他的所有父节点都集中处理为(1,2,3)类型
                String ids = Utils.convertPathCollectToSqlIn(paths);
                paramMap.put("ids", ids);
                flowsAndMaps = processMapper.fetchInventory(paramMap);
            }
        } else {
            if (!"2".equals(downloadType)) {
                //　查询选中的节点的父节点
                List<FlowInventoryBean> parents = processMapper.fetchInventoryParent(paramMap);
                flowsAndMaps.addAll(parents);
            }
            // 获得清单数据,经过view_sort排序的数据
            flowsAndMaps.addAll(processMapper.fetchInventory(paramMap));
        }
        //根据部门ID  查询部门的父类部门
        for (FlowInventoryBean flowBean : flowsAndMaps) {
            if (flowBean.getOrgId() != null) {
                flowBean.setDutyOrgList(orgMapper.findOrgPaterStrByOrgId(Long.valueOf(flowBean.getOrgId())));
            }
        }
        // 查询流程的最高的级别
        InventoryBaseInfo baseInfo = inventoryMapper.getInventoryBaseInfo(paramMap);
        // 初始化清单
        Inventory inventory = new Inventory(baseInfo);
        if ("2".equals(downloadType)) {// 下载流程架构清单设置开始节点
            FlowStructureT flow = processMapper.getFlowStructure(Long.valueOf(paramMap.get("flowId").toString()));
            inventory.setStartLevel(flow.gettLevel());
        }
        Map<Long, FileDirCommonBean> maps = new HashMap<>();
        List<FlowInventoryBean> processes = new ArrayList<>();
        for (FlowInventoryBean bean : flowsAndMaps) {
            if (bean.getIsFlow() == 0) {
                if ("2".equals(downloadType)) {
                    processes.add(bean);
                } else {
                    maps.put(bean.getFlowId(), new FileDirCommonBean(bean.getFlowId(), bean.getFlowName(), bean.getFlowIdInput(), bean.getLevel()));
                }
            } else { //流程架构的情况下
                processes.add(bean);
            }
        }
        //科大讯飞根据登录名获取真实姓名
        IfytekCommon.ifytekCommonGetProcessUserName(processes);
        // 获取清单数据实体
        InventoryTable inventoryTable = getInventoryTable(isPaging, maps, processes, inventory.getStartLevel(), inventory.getMaxLevel(), paramMap);
        inventory.setData(inventoryTable);
        if ("2".equals(downloadType)) {
            inventory.setStyleMap(getProcessMapInventoryStyles(inventoryTable.getTitileContents().size()));
        } else {
            inventory.setStyleMap(InventoryStyleManager.getProcessInventoryStyles(inventoryTable.getTitileContents()));
        }


        return inventory;
    }


    public static Map<String, List<InventoryCellStyle>> getProcessMapInventoryStyles(int num) {
        // size>0
        // 0 级 1级 N级 流程名称 .....
        List<InventoryCellStyle> titleStyles = InventoryStyleManager.createCellStyles(1, num);
        List<InventoryCellStyle> contentStyles = new ArrayList<InventoryCellStyle>(titleStyles);
        return InventoryStyleManager.buildResultMap(titleStyles, contentStyles);
    }


    private InventoryTable getInventoryTable(boolean isPaging, Map<Long, FileDirCommonBean> maps,
                                             List<FlowInventoryBean> processes, int minLv, int maxLv, Map<String, Object> paramMap) {
        InventoryTable data = new InventoryTable();
        String downloadType = paramMap.get("downloadType") == null ? "0" : paramMap.get("downloadType").toString();
        //是否隐藏清单编号列
        boolean isShowNum = ConfigUtils.isShowProcessListNum();
        // 添加标题行的数据
        if ("2".equals(downloadType)) {
            data.addContentRow(ExcelUtils.getProessMapInventoryTitle(minLv, maxLv, InventoryStyleManager.getProcessMapTitles(), isShowNum));
        } else {
            if (ConfigUtils.inventoryShowDirEnd()) {
                data.addContentRow(ExcelUtils.getMengNiuInventoryTitle(minLv, maxLv, InventoryStyleManager.getProcessMengNiuTitles(), isShowNum));
            } else {
                data.addContentRow(ExcelUtils.getProcessInventoryTitle(minLv, maxLv, InventoryStyleManager.getProcessTitles(), isShowNum));
            }
        }
        switch (downloadType) {
            case "0":
            case "1":
                List<Resource> relateds = inventoryMapper.findFlowRelateds(paramMap);
                Map<Long, List<Resource>> relatedMap = relateds.stream().collect(Collectors.groupingBy(r -> Long.valueOf(r.getRid())));
                for (FlowInventoryBean process : processes) {
                    List<InventoryCell> contentRow = new ArrayList<InventoryCell>();
                    if (ConfigUtils.inventoryShowDirEnd()) {
                        // 添加流程的相关数据
                        contentRow.addAll(getContentCells(process, relatedMap));
                        // 添加自动生成的级别数据getContentCells
                        contentRow.addAll(ExcelUtils.getLevels(maps, minLv, maxLv, process.getPath(), isShowNum));
                    } else {
                        // 添加自动生成的级别数据
                        contentRow.addAll(ExcelUtils.getLevels(maps, minLv, maxLv, process.getPath(), isShowNum));
                        // 添加流程的相关数据
                        contentRow.addAll(getContentCells(process, relatedMap));
                    }
                    data.addContentRow(contentRow);
                }
                break;
            case "2":
                for (FlowInventoryBean process : processes) {
                    List<InventoryCell> contentRow = new ArrayList<InventoryCell>();
                    // 添加自动生成的级别数据
                    // contentRow.addAll(ExcelUtils.getLevelsMap(maps, minLv, maxLv, process.getPath()));
                    // 添加流程的相关数据
                    contentRow.addAll(getContentCellsProcessMap(process, minLv, maxLv, isShowNum));
                    data.addContentRow(contentRow);
                }
                break;
            default:
                break;
        }

        return data;
    }

    private List<InventoryCell> getContentCells(FlowInventoryBean process, Map<Long, List<Resource>> relatedMap) {
        List<InventoryCell> contentCells = new ArrayList<InventoryCell>();
        InventoryCell cell = new InventoryCell();
        cell.setText(process.getFlowName());
        if (process.getPubState() == 1) {// 发布的有链接，未发布的没有
            LinkResource link = new LinkResource(process.getFlowId(), process.getFlowName(), ResourceType.PROCESS);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell.setLinks(links);
        }
        cell.setStatus(Utils.getNodeStatusByType(process.getTypeByData()));
        contentCells.add(cell);
        cell = new InventoryCell();
        cell.setText(process.getFlowIdInput());
        contentCells.add(cell);

        if (ConfigUtils.isShowItem(ConfigItemPartMapMark.securityLevel)) { //蒙牛登录类型情况下不显示级别
            cell = new InventoryCell();
            cell.setText(ConfigUtils.getConfidentialityLevel(process.getConfidentialityLevel()));
            contentCells.add(cell);
        }

        cell = new InventoryCell();
        if (!ConfigUtils.inventoryShowDirEnd()) {//蒙牛登录类型情况发布类型不同
            String pubState = JecnProperties.getValue("published");//已发布
            if (process.getPubState() == 0) {
                pubState = JecnProperties.getValue("unpublished");//未发布
            }
            cell.setText(pubState);
            contentCells.add(cell);
        } else {
            int countType = commonService.findMengNiuPubType("JECN_FLOW_STRUCTURE", process.getFlowId(), "FLOW_ID");
            String pubState = JecnProperties.getValue("newRelease");//新建发布
            if (process.getPubState() == 0) {
                pubState = JecnProperties.getValue("unpublished");//未发布
            } else if (countType > 1) {
                pubState = JecnProperties.getValue("newRevision");//新建换版
            }
            cell.setText(pubState);
            contentCells.add(cell);
        }

        // 相关文件
        cell = new InventoryCell();
        if (process.getPubState() != 0) {
            cell.setLinks(relatedMap.getOrDefault(process.getFlowId(), new ArrayList<Resource>()).stream().map(Resource::getLink).collect(Collectors.toList()));
        }
        contentCells.add(cell);

        // 责任部门
        String orgName = Common.getOrgFullPathStr(process.getOrgName(), process.getDutyOrgList());
        cell = new InventoryCell();
        List<LinkResource> links = new ArrayList<LinkResource>();
        links.add(new LinkResource(process.getOrgId(), orgName, ResourceType.ORGANIZATION));
        cell.setLinks(links);
        contentCells.add(cell);
        if (!ConfigUtils.inventoryShowDirEnd()) {//蒙牛登录类型情况下不显示
            // 责任人
            cell = new InventoryCell();
            cell.setText(process.getResPeople());
            contentCells.add(cell);
            // 监护人
            cell = new InventoryCell();
            cell.setText(process.getGuardianPeople());
            contentCells.add(cell);
            // 拟制人
            cell = new InventoryCell();
            cell.setText(process.getDraftPeople());
            contentCells.add(cell);
        }
        // 版本号
        cell = new InventoryCell();
        cell.setText(process.getVersionId());
        contentCells.add(cell);
        // 发布日期
        cell = new InventoryCell();
        if (process.getPubTime() == null) {
            cell.setText("");
        } else {
            cell.setText(Common.getStringbyDateHM(process.getPubTime()));
        }
        contentCells.add(cell);
        // 有效期
        cell = new InventoryCell();
        String periodOfValidity = "";
        if (process.getPubState() != 0) {
            if (process.getExpiry() != null && process.getExpiry() == 0) {
                periodOfValidity = JecnProperties.getValue("permanent");
            } else if (process.getExpiry() != null && process.getExpiry() > 0) {
                periodOfValidity = process.getExpiry() + "";
            }
        }
        cell.setText(periodOfValidity);
        contentCells.add(cell);
        // 是否及时优化(流程清单是没有是的，因为不知道将来是否已经优化)
        if (!ConfigUtils.inventoryShowDirEnd()) { //蒙牛登录类型情况下不显示
            cell = new InventoryCell();
            String optimization = "";
            if (process.getPubState() == 1 && !process.isSurvey()) {
                optimization = JecnProperties.getValue("no");
            }
            cell.setText(optimization);
            contentCells.add(cell);
            // 下次审视需完成时间
            cell = new InventoryCell();
            if (process.getPubState() != 0) {
                if ((process.getExpiry() != null && process.getExpiry() == 0) || process.getNextScanDate() == null) {
                    cell.setText("");
                } else {
                    cell.setText(Common.getStringbyDate(process.getNextScanDate()));
                }
            }
            contentCells.add(cell);
        }
        return contentCells;
    }

    private List<InventoryCell> getContentCellsProcessMap(FlowInventoryBean process, int startLevel, int endLevel, boolean isShowNum) {
        List<InventoryCell> content = new ArrayList<InventoryCell>();
        InventoryCell cell = null;
        int curLevel = process.getLevel();
        for (int i = startLevel; i <= endLevel; i++) {
            if (curLevel == i) {
                cell = new InventoryCell();
                cell.setText(process.getFlowName());
                cell.setStatus(Utils.getNodeStatusByType(process.getTypeByData()));
                content.add(cell);
                if (isShowNum) {
                    cell = new InventoryCell();
                    cell.setText(process.getFlowIdInput() == null ? "" : process.getFlowIdInput());
                    content.add(cell);
                }
            } else {
                cell = new InventoryCell();
                cell.setText("");
                content.add(cell);
                if (isShowNum) {
                    cell = new InventoryCell();
                    cell.setText("");
                    content.add(cell);
                }
            }
        }
        // 责任人
        cell = new InventoryCell();
        cell.setText(process.getResPeople());
        content.add(cell);

        // 责任部门
        String orgName = Common.getOrgFullPathStr(process.getOrgName(), process.getDutyOrgList());
        cell = new InventoryCell();
        cell.setText(orgName);
        content.add(cell);

        cell = new InventoryCell();
        String pubState = JecnProperties.getValue("published");
        if (process.getPubState() == 0) {
            pubState = JecnProperties.getValue("unpublished");
        }
        cell.setText(pubState);
        content.add(cell);
        return content;
    }


    @Override
    public List<FlowSearchResultBean> searchFlow(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "pub_time desc");
        map.put("typeAuth", TYPEAUTH.FLOW);
        List<FlowSearchResultBean> resultBeans = processMapper.searchFlow(map);
        IfytekCommon.iflytekCommonSearchFlow(resultBeans);
        return resultBeans;
    }

    @Override
    public int searchFlowTotal(Map<String, Object> map) {
        return processMapper.flowSearchTotal(map);
    }

    @Override
    public List<FlowMapSearchResultBean> searchFlowMap(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "t.pub_time");
        map.put("typeAuth", TYPEAUTH.FLOW);
        return processMapper.flowMapSearch(map);
    }

    @Override
    public int searchFlowMapTotal(Map<String, Object> map) {
        return processMapper.flowMapSearchTotal(map);
    }

    @Override
    public List<RelatedFileBean> findFlowMapRelatedFile(Map<String, Object> map) {
        List<Resource> resources = processMapper.findFlowMapRelatedFile(map);
        List<RelatedFileBean> resultList = new ArrayList<>();
        LinkResource link = null;
        for (Resource resource : resources) {
            link = new LinkResource(resource.getId(), resource.getName(), resource.getType(), resource.getIsPublic(), TreeNodeUtils.NodeType.processMap);
            resultList.add(new RelatedFileBean(link));
        }
        return resultList;
    }

    @Override
    public FlowActiveDetainBean findFlowActiveDetainBean(Map<String, Object> map) {

        boolean isHistory = map.get("historyId") == null || "0".equals(map.get("historyId").toString()) ? false : true;
        FlowActiveDetainBean flowActiveDetainBean = new FlowActiveDetainBean();

        // 活动基本信息
        FlowActiveBaseBean flowActiveBaseBean = this.activeMapper.findFlowActiveBaseBean(map);
        if (ConfigUtils.useNewInout()) {
            flowActiveBaseBean.setActivityInput("");
            flowActiveBaseBean.setActivityOutput("");
        }
        flowActiveDetainBean.setFlowActiveBaseBean(flowActiveBaseBean);
        // 是否显示爱普生活动担当
        flowActiveBaseBean.setShowCustomOne(ConfigUtils.isShowEpson());
        //是否隐藏 活动类别
        flowActiveBaseBean.setShowActivityType(ConfigUtils.isShowActivityType());
        // 信息化
        List<FlowActiveOnLineBean> listFlowActiveOnLineBeans = activeMapper.findFlowActiveOnLineBeans(map);
        flowActiveDetainBean.setListFlowActiveOnLineBeans(listFlowActiveOnLineBeans);
        // 指标
        List<FlowActiveRelatedRefIndicators> listFlowActiveRelatedRefIndicators = activeMapper
                .findFlowActiveRelatedRefIndicators(map);
        flowActiveDetainBean.setListFlowActiveRelatedRefIndicators(listFlowActiveRelatedRefIndicators);
        // 控制点
        List<FlowActiveRelatedRiskControlPoint> listFlowActiveRelatedRiskControlPoint = activeMapper
                .findFlowActiveRelatedRiskControlPoints(map);
        flowActiveDetainBean.setListFlowActiveRelatedRiskControlPoint(listFlowActiveRelatedRiskControlPoint);
        // 标准
        List<FlowActiveRelatedStandardBean> listFlowActiveRelatedStandardBean = activeMapper
                .findFlowActiveRelatedStandardBeans(map);
        flowActiveDetainBean.setListFlowActiveRelatedStandardBean(listFlowActiveRelatedStandardBean);
        // 操作规范文件
        List<FileBaseBean> listFileBaseBeanOperationFiles = activeMapper.findActiveOperationFiles(map);
        for (FileBaseBean fileBean : listFileBaseBeanOperationFiles) {
            fileBean.setHistory(isHistory);
        }
        flowActiveDetainBean.setListFileBaseBeanOperationFiles(listFileBaseBeanOperationFiles);
        if (!ConfigUtils.useNewInout()) {
            // 输入规范文件
            List<FileBaseBean> listFileBaseBeanInFiles = activeMapper.findActiveInFiles(map);
            for (FileBaseBean fileBean : listFileBaseBeanInFiles) {
                fileBean.setHistory(isHistory);
            }
            flowActiveDetainBean.setListFileBaseBeanInFiles(listFileBaseBeanInFiles);
        } else {
            // 输入规范文件
            List<FileBaseBean> listFileBaseBeanInFiles = activeMapper.findNewActiveInFiles(map);
            for (FileBaseBean fileBean : listFileBaseBeanInFiles) {
                fileBean.setHistory(isHistory);
            }
            flowActiveDetainBean.setListFileBaseBeanInFiles(listFileBaseBeanInFiles);
        }


        // 输出规范文件
        List<FlowActiveOutBean> listFlowActiveOutBean = new ArrayList<>();
        List<FlowActiveOutTemplet> listFlowActiveOutTemplet = activeMapper.findActiveOutFiles(map);
        //拼装 新版输出 模板样例
        if (!ConfigUtils.useNewInout()) {
            getOldFlowActiveOutTemplet(listFlowActiveOutTemplet, listFlowActiveOutBean);
        } else {
            getNewFlowActiveOutTemplet(listFlowActiveOutTemplet, listFlowActiveOutBean);
        }
        flowActiveDetainBean.setListFlowActiveOutBeans(listFlowActiveOutBean);
        // 爱普生 隐藏 标准和风险
        flowActiveDetainBean.setShowRiskAndStandard(!ConfigUtils.isShowEpson());
        return flowActiveDetainBean;
    }


    private void getOldFlowActiveOutTemplet(List<FlowActiveOutTemplet> listFlowActiveOutTemplet, List<FlowActiveOutBean> listFlowActiveOutBean) {
        String mode_file_id = null;
        List<FileBaseBean> templetList = null;
        FlowActiveOutBean flowActiveOutBean = null;
        FileBaseBean modeFile = null;
        for (int i = 0; i < listFlowActiveOutTemplet.size(); i++) {
            FlowActiveOutTemplet flowActiveOutTemplet = listFlowActiveOutTemplet.get(i);
            if (mode_file_id == null || !mode_file_id.equals(flowActiveOutTemplet.getModeFileId())) {
                mode_file_id = flowActiveOutTemplet.getModeFileId();
                flowActiveOutBean = new FlowActiveOutBean();
                templetList = new ArrayList<FileBaseBean>();
                modeFile = new FileBaseBean();
                modeFile.setFileId(flowActiveOutTemplet.getFileId());
                modeFile.setFileName(flowActiveOutTemplet.getFileName());
                modeFile.setHistory(isHistory);
                modeFile.setExplain(flowActiveOutTemplet.getExplain());
                modeFile.setName(flowActiveOutTemplet.getName());
                flowActiveOutBean.setFileBaseBean(modeFile);
                flowActiveOutBean.setListTemplet(templetList);
                listFlowActiveOutBean.add(flowActiveOutBean);
            }
            FileBaseBean templetFile = new FileBaseBean();
            templetFile.setFileId(flowActiveOutTemplet.getFileTempletId());
            templetFile.setFileName(flowActiveOutTemplet.getFileTempletName());
            templetFile.setHistory(isHistory);
            templetList.add(templetFile);
        }
    }


    private void getNewFlowActiveOutTemplet(List<FlowActiveOutTemplet> listFlowActiveOutTemplet, List<FlowActiveOutBean> listFlowActiveOutBean) {
        String mode_file_id = null;
        List<LinkResource> templetList = null;
        List<LinkResource> mouldtList = null;
        FlowActiveOutBean flowActiveOutBean = null;
        FileBaseBean modeFile = null;
        for (int i = 0; i < listFlowActiveOutTemplet.size(); i++) {
            FlowActiveOutTemplet flowActiveOutTemplet = listFlowActiveOutTemplet.get(i);
            if (mode_file_id == null || !mode_file_id.equals(flowActiveOutTemplet.getModeFileId())) {
                mode_file_id = flowActiveOutTemplet.getModeFileId();
                flowActiveOutBean = new FlowActiveOutBean();
                templetList = new ArrayList<LinkResource>();
                mouldtList = new ArrayList<LinkResource>();
                modeFile = new FileBaseBean();
                modeFile.setHistory(isHistory);
                modeFile.setExplain(flowActiveOutTemplet.getExplain());
                modeFile.setName(flowActiveOutTemplet.getName());
                flowActiveOutBean.setFileBaseBean(modeFile);
                flowActiveOutBean.setTempletLinks(templetList);
                flowActiveOutBean.setMouldLinks(mouldtList);
                listFlowActiveOutBean.add(flowActiveOutBean);
            }
            if (flowActiveOutTemplet.getFileTempletId() != null) {
                LinkResource file = new LinkResource(flowActiveOutTemplet.getFileTempletId().toString(), flowActiveOutTemplet.getFileTempletName(), ResourceType.FILE, TreeNodeUtils.NodeType.process);
                //样例
                if (flowActiveOutTemplet.getType() == 1) {
                    templetList.add(file);
                } else {
                    //模板
                    mouldtList.add(file);
                }

            }

        }
    }

    /**
     * 流程操作模板
     *
     * @param map
     * @return @e
     */
    @Override
    public List<FlowOperationModel> getProcessActiveBeanList(Map<String, Object> map) {
        List<FlowOperationModel> listResult = processMapper.findOperationModel(map);
        return listResult;
    }

    @Override
    public List<Resource> fetchProcessesSupportResources(Map<String, Object> paramMap) {
        Long historyId = (Long) paramMap.get("historyId");
        boolean idPub = (boolean) paramMap.get("isPub");
        List<Resource> resourceList = null;
        if (historyId == 0 || historyId == null) {
            resourceList = processMapper.fetchProcessesSupportResources(paramMap);
            this.isHistory = false;
        } else {
            resourceList = processMapper.fetchProcessesSupportHistoryResources(paramMap);
            idPub = false;
            this.isHistory = true;
        }
/*
        for (Resource resource : resourceList) {
            resource.setUrl(HttpUtil.getDownLoadFileHttp(valueOfLong(resource.getId()), idPub, ""));
        }*/
        return resourceList;
    }


    @Override
    public ProcessArchitectureCard getProcessArchitectureCard(Map<String, Object> paramMap) {
        // 架构卡基本信息
        Map<String, Object> cardMap = processMapper.getProcessArchitectureBase(paramMap);
        if (cardMap == null) {
            return null;
        }

        // 流程子级节点
        List<Map<String, Object>> subFlows = processMapper.getSubFlows(paramMap);
        ProcessArchitectureCard processArchitectureCard = new ProcessArchitectureCard();
        Map<String, Object> doMap = new HashedMap(); //获取架构卡 自定义名称
        doMap.put("typeBigModel", 0);
        doMap.put("typeSmallModel", 2);
        // 操作说明配置
        List<ConfigItemBean> configItemBeanList = configItemMapper.findConfigItems(doMap);
        doMap.clear();
        for (ConfigItemBean configData : configItemBeanList) {
            doMap.put(configData.getMark(), configData.getName(AuthenticatedUserUtil.isLanguageType()));
        }
        processArchitectureCard.setCardName(doMap);

        // 架构卡详细信息
        ProcessDetailInfo detailInfo = new ProcessDetailInfo();
        //架构卡流程描述
        detailInfo.setDescription(ConfigUtils.valueIsOneThenTrue("isShowProcessMapDescription") ? getValueOfMap("FLOW_DESCRIPTION", cardMap) : "hide");
        //架构卡流程目的
        detailInfo.setTarget(ConfigUtils.valueIsOneThenTrue("isShowProcessMapPurpose") ? getValueOfMap("FLOW_AIM", cardMap) : "hide");
        //架构卡流程输入
        detailInfo.setInput(ConfigUtils.valueIsOneThenTrue("isShowProcessMapFlowInputInfo") ? getValueOfMap("FLOW_INPUT", cardMap) : "hide");
        //架构卡流程输出
        detailInfo.setOutput(ConfigUtils.valueIsOneThenTrue("isShowProcessMapFlowOutputInfo") ? getValueOfMap("FLOW_OUTPUT", cardMap) : "hide");
        //流程起点
        detailInfo.setStartingPoint(ConfigUtils.valueIsOneThenTrue("isShowProcessMapFlowStart") ? getValueOfMap("FLOW_START", cardMap) : "hide");
        //流程终点
        detailInfo.setEndingPoint(ConfigUtils.valueIsOneThenTrue("isShowProcessMapFlowEnd") ? getValueOfMap("FLOW_END", cardMap) : "hide");
        //架构卡流程KPI
        detailInfo.setKpi(ConfigUtils.valueIsOneThenTrue("isShowProcessMapFlowKPI") ? getValueOfMap("FLOW_KPI", cardMap) : "hide");
        //架构卡详细信息
        processArchitectureCard.setDetailInfo(detailInfo);
        // 架构卡所属流程
        String flowId = cardMap.get("FLOW_ID").toString();
        //架构卡名称
        ProcessBean process = new ProcessBean(flowId, getValueOfMap("FLOW_NAME", cardMap));
        //架构卡编号 hide 为隐藏
        process.setNumber(ConfigUtils.valueIsOneThenTrue("isShowProcessMapNum") ? getValueOfMap("FLOW_ID_INPUT", cardMap) : "hide");
        //架构卡专员
        process.setCommissionerName(ConfigUtils.valueIsOneThenTrue("isShowProcessMapCommissioner") ? IfytekCommon.getIflytekUserName(getValueOfMap("COMMISSIONER", cardMap), 0) : "hide");//专员
        //架构卡关键字
        process.setKeyword(ConfigUtils.valueIsOneThenTrue("isShowProcessMapKeyWord") ? getValueOfMap("KEYWORD", cardMap) : "hide");//关键字
        //架构卡供应商
        process.setSupplier(ConfigUtils.valueIsOneThenTrue("isShowProcessMapSupplier") ? getValueOfMap("SUPPLIER", cardMap) : "hide");
        //架构卡客户
        process.setCustomer(ConfigUtils.valueIsOneThenTrue("isShowProcessMapCustomer") ? getValueOfMap("CUSTOMER", cardMap) : "hide");
        //架构卡风险与机会
        process.setRiskAndOpportunity(ConfigUtils.valueIsOneThenTrue("isShowProcessMapRiskAndOpportunity") ? getValueOfMap("RISK_OPPORTUNITY", cardMap) : "hide");
        //架构卡责任人
        process.setPersonInCharge(IfytekCommon.getIflytekUserName(getValueOfMap("RES_PEOPLE", cardMap), 0));
        //架构卡层级
        process.setLevel(cardMap.get("T_LEVEL") == null ? -1 : Integer.parseInt(cardMap.get("T_LEVEL").toString()));
        //架构卡版本号
        process.setVersion(getValueOfMap("VERSION_ID", cardMap));
        //架构卡发布日期
        String pubDate = getValueOfMap("PUB_TIME", cardMap);
        process.setPublishDate((Date) cardMap.get("PUB_TIME"));
        // 责任部门
        Department department = new Department();
        department.setId(cardMap.get("ORG_ID") == null ? null : Long.valueOf(cardMap.get("ORG_ID").toString()));
        department.setName(getValueOfMap("ORG_NAME", cardMap));
        process.setDepartment(department);
        processArchitectureCard.setArchitecture(process);

        // 上一层流程
        if (cardMap.get("P_FLOW_ID") != null) {
            ProcessBean parent = new ProcessBean(cardMap.get("P_FLOW_ID").toString(),
                    getValueOfMap("P_FLOW_NAME", cardMap));
            parent.setType("PROCESS");
            parent.setHistoryId(0);
            processArchitectureCard.setParent(parent);
            processArchitectureCard.setParentIsShow(ConfigUtils.valueIsOneThenTrue("isShowProcessMapLastFlow"));
        } else {
            ProcessBean parent = new ProcessBean("", "");
            parent.setType("PROCESS");
            parent.setHistoryId(0);
            processArchitectureCard.setParent(parent);
            processArchitectureCard.setParentIsShow(ConfigUtils.valueIsOneThenTrue("isShowProcessMapLastFlow"));
        }
        // 下一层流程
        List<ProcessBean> subProcesses = new ArrayList<ProcessBean>();
        for (Map<String, Object> map : subFlows) {
            if (map.get("FLOW_ID") != null) {
                ProcessBean subProcess = new ProcessBean(map.get("FLOW_ID").toString(), getValueOfMap("FLOW_NAME", map));
                subProcess.setType("PROCESS");
                subProcess.setHistoryId(0);
                subProcesses.add(subProcess);
            }
        }
        processArchitectureCard.setSubProcessesIsShow(ConfigUtils.valueIsOneThenTrue("isShowProcessMapNextFlow"));
        processArchitectureCard.setSubProcesses(subProcesses);

        String isApp = "true";
        if ("false".equals(isApp)) {
            return processArchitectureCard;
        }
        paramMap.put("flowId", paramMap.get("id"));
        paramMap.put("isPub", Utils.getPubByParams(paramMap));

        FlowStructure structure = processMapper.findFlowStructureByMap(paramMap);
        ProcessLink processLink = CommonServiceUtil.getProcessNameLink(structure, isApp, null);
        processArchitectureCard.setLinkImage(processLink.getImageLink());
        return processArchitectureCard;
    }


    private String getValueOfMap(String key, Map<String, Object> map) {
        return map.get(key) == null ? "" : map.get(key).toString();
    }

    @Override
    public FlowKpiName getKpiNameByMap(Map<String, Object> map) {
        return processMapper.findKpiNameById(map);
    }

    @Override
    public Inventory showCheckout(Long flowId) {
        Map<String, Object> map = new HashMap<>();
        map.put("flowId", flowId);
        List<CheckoutResult> checkout = processRPCService.getCheckout(map);
        Inventory inventory = new Inventory();
        List<List<InventoryCell>> contents = inventory.getData().getContents();
        EasyCell easy = EasyCell.newInstance().add(new InventoryCellStyle(2), JecnProperties.getValue("processName"))
                .add(new InventoryCellStyle(2), JecnProperties.getValue("inoutName"))//输入输出名称
                .add(new InventoryCellStyle(1), JecnProperties.getValue("type"))
                .add(new InventoryCellStyle(4), JecnProperties.getValue("hasSource"))
                .add(new InventoryCellStyle(4), JecnProperties.getValue("curFlowInUse"))
                .add(new InventoryCellStyle(4), JecnProperties.getValue("sonFlowInUse"))
                .add(new InventoryCellStyle(4), JecnProperties.getValue("curFlowGenerate"));
        List<InventoryCell> titleCells = easy.buildWithTitle();
        contents.add(titleCells);
        inventory.setStyleMap(easy.getStyleMap());

        for (CheckoutResult c : checkout) {
            String name = c.getName();
            contents.addAll(getCells(name + "(" + JecnProperties.getValue("curFlow") + ")", 0, c.getIns(), JecnProperties.getValue("mainFlowIn")));
            contents.addAll(getCellsRight(name + "(" + JecnProperties.getValue("curFlow") + ")", 1, c.getOuts()));
            if (!Utils.isEmpty(c.getSons())) {
                for (CheckoutResult s : c.getSons()) {
                    name = s.getName() + "(" + JecnProperties.getValue("subProcess") + ")";
                    contents.addAll(getCells(name, 0, s.getIns(), JecnProperties.getValue("sonFlowIn")));
                    contents.addAll(getCellsSonRight(name, 1, s.getOuts()));
                }
            }
        }

        return inventory;
    }

    private List<List<InventoryCell>> getCells(String name, int inoutType, List<CheckoutItem> inouts, String columnName) {
        List<List<InventoryCell>> result = new ArrayList<>();
        if (!Utils.isEmpty(inouts)) {
            for (CheckoutItem item : inouts) {
                List<InventoryCell> cells = EasyCell.newInstance().add(name)
                        .add(item.getName())
                        .add(columnName)
                        .add(Utils.concat(item.getCellA(), "<br>"))
                        .add(Utils.concat(item.getCellB(), "<br>"))
                        .add("")
                        .add("")
                        .build();
                result.add(cells);
            }
        }
        return result;
    }

    private List<List<InventoryCell>> getCellsSonRight(String name, int inoutType, List<CheckoutItem> inouts) {
        List<List<InventoryCell>> result = new ArrayList<>();
        if (!Utils.isEmpty(inouts)) {
            for (CheckoutItem item : inouts) {
                List<InventoryCell> cells = EasyCell.newInstance().add(name)
                        .add(item.getName())
                        .add(JecnProperties.getValue("sonFlowOut"))
                        .add("")
                        .add(Utils.concat(item.getCellA(), "<br>"))
                        .add("")
                        .add("")
                        .build();
                result.add(cells);
            }
        }
        return result;
    }

    private List<List<InventoryCell>> getCellsRight(String name, int inoutType, List<CheckoutItem> inouts) {
        List<List<InventoryCell>> result = new ArrayList<>();
        if (!Utils.isEmpty(inouts)) {
            for (CheckoutItem item : inouts) {
                List<InventoryCell> cells = EasyCell.newInstance().add(name)
                        .add(item.getName())
                        .add(JecnProperties.getValue("mainFlowOut"))
                        .add("")
                        .add("")
                        .add(Utils.concat(item.getCellA(), "<br>"))
                        .add(Utils.concat(item.getCellB(), "<br>"))
                        .build();
                result.add(cells);
            }
        }
        return result;
    }

    private String getInoutNameByType(int inoutType) {
        if (inoutType == 0) {
            return JecnProperties.getValue("input");
        } else {
            return JecnProperties.getValue("out");
        }
    }

    @Override
    public Inventory showCheckoutRole(Long flowId) {
        Map<String, Object> map = new HashMap<>();
        map.put("flowId", flowId);
        List<CheckoutRoleResult> checkout = processRPCService.getCheckoutRole(map);
        Inventory inventory = new Inventory();
        List<List<InventoryCell>> contents = inventory.getData().getContents();
        EasyCell easy = EasyCell.newInstance().add(new InventoryCellStyle(2), JecnProperties.getValue("processName"))
                .add(new InventoryCellStyle(2), JecnProperties.getValue("role"))
                .add(new InventoryCellStyle(1), JecnProperties.getValue("isRelated"))
                .add(new InventoryCellStyle(2), JecnProperties.getValue("belongToRoleStoreOrPos"));
        List<InventoryCell> titleCells = easy.buildWithTitle();
        contents.add(titleCells);
        inventory.setStyleMap(easy.getStyleMap());
        String posPrefix = "（" + JecnProperties.getValue("post") + "）";
        String groupPrefix = "（" + JecnProperties.getValue("positionGroup") + "）";
        String posPrefixNone = posPrefix + JecnProperties.getValue("nothing");
        String groupPrefixNone = groupPrefix + JecnProperties.getValue("nothing");
        for (CheckoutRoleResult c : checkout) {
            String name = c.getName();
            List<CheckoutRoleItem> data = c.getData();
            if (!Utils.isEmpty(data)) {
                for (CheckoutRoleItem item : data) {
                    String prefix = item.getType() == 0 ? posPrefix : groupPrefix;
                    String posName = prefix + item.getPosName();
                    if (StringUtils.isBlank(item.getPosName())) {
                        posName = "";
                    }
                    List<InventoryCell> cells = EasyCell.newInstance()
                            .add(name)
                            .add(item.getFigureText())
                            .add(getRelateType(item))
                            .add(posName)
                            .build();
                    contents.add(cells);
                }
            }
        }
        return inventory;
    }

    @Override
    public List<TreeNode> findInventedFlows(Map<String, Object> map) {
        if (!ConfigUtils.isEnableScopeOrg()) {
            return new ArrayList<>();
        }
        List<TreeNode> result = processMapper.findInventedFlows(map);
        result.stream().forEach(node -> {
            node.setType(TreeNodeUtils.NodeType.organization);
            node.setInventedType(TreeNodeUtils.NodeType.organization);
            node.setInventedId(String.valueOf(node.getId()));
        });
        return result;
    }

    private String getRelateType(CheckoutRoleItem d) {
        String posPrefix = "（" + JecnProperties.getValue("post") + "）";
        String groupPrefix = "（" + JecnProperties.getValue("positionGroup") + "）";
        String prefix = d.getType() == 0 ? posPrefix : groupPrefix;
        String existName = "";
        if (d.isFpEqual() || d.isRelated()) {
            if (d.isFpEqual() && d.isRelated()) {
                existName = "√" + JecnProperties.getValue("related");
            } else if (d.isFpEqual()) {
                existName = "◎" + JecnProperties.getValue("sameNameNotRealted");
            } else if (d.isRelated()) {
                existName = "△" + JecnProperties.getValue("relatedNotSame");
            }
        } else {
            existName = "╳" + JecnProperties.getValue("notRelated");
        }
        return existName;
    }

}
