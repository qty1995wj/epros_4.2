package com.jecn.epros.server.dao.integration;

import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;
import com.jecn.epros.server.common.IBaseDao;

/**
 * 活动类别DAO接口
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-10-31 时间：上午10:22:57
 */
public interface IJecnActivityTypeDao extends
		IBaseDao<JecnActiveTypeBean, Long> {

}
