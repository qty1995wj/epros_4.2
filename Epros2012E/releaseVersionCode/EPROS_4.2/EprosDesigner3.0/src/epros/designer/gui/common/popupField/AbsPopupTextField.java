package epros.designer.gui.common.popupField;

import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.event.DocumentEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSearchPopup;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.swing.textfield.search.JecnSearchDocumentListener;

/**
 * 文本搜索框抽象类
 * 
 * @author hyl
 * 
 */
public abstract class AbsPopupTextField extends JSearchTextField {

	private static final long serialVersionUID = 1L;
	/** popup */
	private JecnSearchPopup jecnSearchPopup = new JecnSearchPopup(this);

	public AbsPopupTextField() {
		// 隐藏第1列和第3列
		jecnSearchPopup.hiddenColumn(0, 2);
		this.addDocumentListener(new JecnSearchDocumentListener(this) {
			@Override
			public void removeUpdateJecn(DocumentEvent e) {
				fieldSearch();
			}

			@Override
			public void insertUpdateJecn(DocumentEvent e) {
				fieldSearch();
			}

			@Override
			public void keyEnterReleased(KeyEvent e) {
				jecnSearchPopup.searchTableMousePressed();
			}
		});
		jecnSearchPopup.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				if (jecnSearchPopup.getSelectTreeBean() != null) {
					jecnSearchPopup.setSelectTreeBean(null);
				}
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {

			}
		});
	}

	private void fieldSearch() {
		String name = this.getText().trim();
		if (!"".equals(name)) {
			if (jecnSearchPopup.isCanSearch()) {
				jecnSearchPopup.setTableData(searchName(name, JecnConstants.projectId));
			}
			jecnSearchPopup.setCanSearch(true);
		}
	}

	protected abstract List<JecnTreeBean> searchName(String name, Long projectId);

}
