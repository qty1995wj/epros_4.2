package com.jecn.webservice.zhongsh.flowpush.service.impl;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.jecn.webservice.zhongsh.flowpush.bean.ParallelState;
import com.jecn.webservice.zhongsh.flowpush.bean.WBAction;
import com.jecn.webservice.zhongsh.flowpush.bean.WBDecision;
import com.jecn.webservice.zhongsh.flowpush.bean.WBDecisionAction;
import com.jecn.webservice.zhongsh.flowpush.bean.WBRole;
import com.jecn.webservice.zhongsh.flowpush.bean.WBRoleStateAssignment;
import com.jecn.webservice.zhongsh.flowpush.bean.WBState;
import com.jecn.webservice.zhongsh.flowpush.bean.WBWorkflow;

/**
 * 石化i3系统流程文件推送，创建xmlservice
 * 
 *@author ZXH
 * @date 2016-4-21下午03:48:56
 */
public enum ICreateXmlServiceImpl {
	INSTANCE;
	private Log log = LogFactory.getLog(ICreateXmlServiceImpl.class);

	/**
	 * 把流程图对象转换为XMl
	 * 
	 * @param flowInfo
	 * @return
	 */
	public String createXml(WBWorkflow flowInfo) {
		if (flowInfo == null) {
			log.error("flowInfo  is null");
			return null;
		}
		Document doc = null;
		try {
			doc = DocumentHelper.createDocument();
			// 1、设置workflow
			Element workflow = doc.addElement("WBWorkflow", flowInfo.getXmlns());
			workflow.addNamespace("WBD", flowInfo.getXmlnsWBD());
			addWorkflowAttrs(workflow, flowInfo);

			// 2、 设置角色
			Element roleColl = workflow.addElement("WBD:WBRoleCollection");
			addRoleEle(roleColl, flowInfo.getwBRoleCollection());

			// 3、设置state
			Element stateColl = workflow.addElement("WBD:WBStateCollection");
			addStateEle(stateColl, flowInfo.getwBStateCollection());

			// 4、设置action
			Element actionColl = workflow.addElement("WBD:WBActionCollection");
			addActionEle(actionColl, flowInfo.getwBActionCollection());

			// 5、并行分支和并行会聚集合
			Element paralleStateColl = workflow.addElement("WBD:ParallelStateCollection");
			addParallelStateEle(paralleStateColl, flowInfo.getParallelStateCollection());

			// 6、元素对应连线对应关系集合(从State上出去的线大于1个会出现在这个集合中)
			Element decisionColl = workflow.addElement("WBD:WBDecisionCollection");
			addDecisionEle(decisionColl, flowInfo.getwBDecisionCollection());

			// 7、角色对应state对应关系
			Element roleStateAssignmentCool = workflow.addElement("WBD:WBRoleStateAssignmentCollection");
			addRoleStateAssignmentEle(roleStateAssignmentCool, flowInfo.getwBRoleStateAssignmentCollection());

			// 8、 action中可执行的角色
			Element roleActionAssignmentColl = workflow.addElement("WBD:WBRoleActionAssignmentCollection");
			addWBRoleStateAssignmentEle(roleActionAssignmentColl, flowInfo.getwBRoleStateAssignmentCollection(),
					flowInfo.getwBActionCollection());

			// 9、action执行条件WBD:WBActionExecutionConditionCollection
			Element actionExecutionColl = workflow.addElement("WBD:WBActionExecutionConditionCollection");
			addWBActionExecutionConditionEle(actionExecutionColl, flowInfo.getwBActionCollection());

			String asXML = workflow.asXML();
			asXML = asXML.replaceAll("WBWorkflow", "WBD:WBWorkflow");
//			System.out.println(asXML);
			return asXML;
		} catch (Exception e) {
			log.error("构建dom文档异常", e);
			return null;
		} finally {
			doc = null;
		}
	}

	private void addWorkflowAttrs(Element workflow, WBWorkflow flowInfo) {
		workflow.addAttribute("xmlns", flowInfo.getXmlns());
		workflow.addAttribute("xmlns:x", flowInfo.getXmlnsx());
		workflow.addAttribute("xmlns:WBD", flowInfo.getXmlnsWBD());
		workflow.addAttribute("WorkBoxVersion", flowInfo.getWorkBoxVersion());
		workflow.addAttribute("WorkflowType", flowInfo.getWorkflowType());
		workflow.addAttribute("ContentType", flowInfo.getContentType());
		workflow.addAttribute("WorkflowName", flowInfo.getWorkflowName());
		workflow.addAttribute("WorkflowDescription", flowInfo.getWorkflowDescription());
		workflow.addAttribute("StartManually", flowInfo.getStartManually());
		workflow.addAttribute("RequireManagePermission", flowInfo.getRequireManagePermission());
		workflow.addAttribute("StartOnChange", flowInfo.getStartOnChange());
		workflow.addAttribute("StartOnCreate", flowInfo.getStartOnCreate());
		workflow.addAttribute("CurrentColorSchemaID", flowInfo.getCurrentColorSchemaID());
		workflow.addAttribute("PlatformType", flowInfo.getContentType());
		workflow.addAttribute("ShowActionIndicators", flowInfo.getShowActionIndicators());
		workflow.addAttribute("IsApplication", flowInfo.getIsApplication());
	}

	/**
	 * 添加角色基础信息
	 * 
	 * @param roleColl
	 * @param roleColls
	 */
	private void addRoleEle(Element roleColl, List<WBRole> roleColls) {
		Element roleEle = null;
		for (WBRole wbRole : roleColls) {
			roleEle = roleColl.addElement("WBD:WBRole");
			addRoleAttrs(roleEle, wbRole);
		}
	}

	private void addRoleAttrs(Element roleEle, WBRole wbRole) {
		roleEle.addAttribute("GUID", wbRole.getGuid());
		roleEle.addAttribute("RoleName", wbRole.getRoleName());
		roleEle.addAttribute("RoleIconType", wbRole.getRoleIconType());
		roleEle.addAttribute("AllowedUsers", wbRole.getAllowedUsers());
		roleEle.addAttribute("DeniedUsers", wbRole.getDeniedUsers());
	}

	/**
	 * 添加state基础信息
	 * 
	 * @param roleColl
	 * @param roleColls
	 */
	private void addStateEle(Element stateColl, List<WBState> stateColls) {
		Element stateEle = null;
		for (WBState wBState : stateColls) {
			stateEle = stateColl.addElement("WBD:WBState");
			addStateAttrs(stateEle, wBState);
		}
	}

	private void addStateAttrs(Element stateEle, WBState wBState) {
		stateEle.addAttribute("GUID", wBState.getGuid());
		stateEle.addAttribute("StateName", wBState.getStateName());
		stateEle.addAttribute("Description", wBState.getDescription());
		// stateEle.addAttribute("GroupGUID", wBState.getGuid());
		stateEle.addAttribute("Type", wBState.getType());
		stateEle.addAttribute("ColorSchema", wBState.getColorSchema());
		stateEle.addAttribute("PositionX", wBState.getPositionX());
		stateEle.addAttribute("PositionY", wBState.getPositionY());
		stateEle.addAttribute("StateWidth", wBState.getStateWidth());
		stateEle.addAttribute("StateHeight", wBState.getStateHeight());
	}

	/**
	 * 添加连接线元素基础信息
	 * 
	 * @param roleColl
	 * @param roleColls
	 */
	private void addActionEle(Element actionColl, List<WBAction> actionColls) {
		Element actionEle = null;
		for (WBAction wBAction : actionColls) {
			actionEle = actionColl.addElement("WBD:WBAction");
			addActionAttrs(actionEle, wBAction);
		}
	}

	private void addActionAttrs(Element stateEle, WBAction wBAction) {
		stateEle.addAttribute("GUID", wBAction.getGuid());
		stateEle.addAttribute("ActionName", wBAction.getActionName());
		stateEle.addAttribute("Description", wBAction.getDescription());
		stateEle.addAttribute("FirstStateGUID", wBAction.getFirstStateGUID());
		stateEle.addAttribute("SecondStateGUID", wBAction.getSecondStateGUID());
		stateEle.addAttribute("FirstStateActionHandlerID", wBAction.getFirstStateActionHandlerID());
		stateEle.addAttribute("SecondStateActionHandlerID", wBAction.getSecondStateActionHandlerID());
		stateEle.addAttribute("FirstActivityGUID", wBAction.getFirstActivityGUID());
		stateEle.addAttribute("Icon", wBAction.getIcon());
		stateEle.addAttribute("ToolTip", wBAction.getToolTip());
		stateEle.addAttribute("AllowBulkLaunch", wBAction.getAllowBulkLaunch());

		stateEle.addElement("WBD:WBActionBreakPointCollection");
	}

	/**
	 * 并行，会聚 集合处理
	 * 
	 * @param paralleStateColl
	 * @param parallelStateCollections
	 */
	private void addParallelStateEle(Element paralleStateColl, List<ParallelState> parallelStateCollections) {
		Element parallelStateEle = null;
		for (ParallelState parallelState : parallelStateCollections) {
			parallelStateEle = paralleStateColl.addElement("WBD:ParallelState");
			addActionAttrs(parallelStateEle, parallelState);
		}

	}

	private void addActionAttrs(Element paraStateEle, ParallelState parallelState) {
		paraStateEle.addAttribute("GUID", parallelState.getGuid());
		paraStateEle.addAttribute("SplitStateGUID", parallelState.getSplitStateGUID());
		paraStateEle.addAttribute("JoinStateGUID", parallelState.getJoinStateGUID());
		paraStateEle.addAttribute("ParallelType", parallelState.getParallelType());
		paraStateEle.addAttribute("Condition", parallelState.getCondition());
	}

	/**
	 * state 出线大于1的 state和action的对应关系
	 * 
	 * @param decisionColl
	 * @param decisions
	 */
	private void addDecisionEle(Element decisionColl, List<WBDecision> decisions) {
		Element decisionEle = null;
		for (WBDecision wbDecision : decisions) {
			decisionEle = decisionColl.addElement("WBD:WBDecision");
			decisionEle.addAttribute("GUID", wbDecision.getGuid());
			decisionEle.addAttribute("StateGUID", wbDecision.getStateGUID());
			if (!wbDecision.getwBDecisionActionCollection().isEmpty()) {
				// 创建WBD:WBDecisionActionCollection
				Element element = decisionEle.addElement("WBD:WBDecisionActionCollection");
				addDecisionActionEle(element, wbDecision.getwBDecisionActionCollection());
			}
		}
	}

	private void addDecisionActionEle(Element descisionEle, List<WBDecisionAction> decisionActions) {
		Element decisionEle = null;
		for (WBDecisionAction decisionAction : decisionActions) {
			decisionEle = descisionEle.addElement("WBD:WBDecisionAction");
			decisionEle.addAttribute("GUID", decisionAction.getGuid());
			decisionEle.addAttribute("ActionGUID", decisionAction.getActionGUID());
		}
	}

	/**
	 * 角色和state对应关系集合
	 * 
	 * @param roleStateAssignmentColl
	 * @param roleStateAssignmentCollections
	 */
	private void addRoleStateAssignmentEle(Element roleStateAssignmentColl,
			List<WBRoleStateAssignment> roleStateAssignmentCollections) {
		Element roleStateAssignmentEle = null;
		for (WBRoleStateAssignment roleStateAssignment : roleStateAssignmentCollections) {
			roleStateAssignmentEle = roleStateAssignmentColl.addElement("WBD:WBRoleStateAssignment");
			addRoleStateAssignmentAttrs(roleStateAssignmentEle, roleStateAssignment);
		}
	}

	private void addRoleStateAssignmentAttrs(Element roleStateAssignmentEle, WBRoleStateAssignment roleStateAssignment) {
		roleStateAssignmentEle.addAttribute("RoleGUID", roleStateAssignment.getRoleGUID());
		roleStateAssignmentEle.addAttribute("StateGUID", roleStateAssignment.getStateGUID());
		roleStateAssignmentEle.addAttribute("EditableColumns", roleStateAssignment.getEditableColumns());
		roleStateAssignmentEle.addAttribute("DisplayableColumns", roleStateAssignment.getDisplayableColumns());
		roleStateAssignmentEle.addAttribute("NotEditableColumns", roleStateAssignment.getNotEditableColumns());
		roleStateAssignmentEle.addAttribute("NotDisplayableColumns", roleStateAssignment.getDisplayableColumns());
	}

	/**
	 * 
	 * @param roleStateAssignmentCool
	 *            角色对应state集合
	 * @param roleStateAssignmentCollections
	 * @param decisions
	 */
	private void addWBRoleStateAssignmentEle(Element roleStateAssignmentCool,
			List<WBRoleStateAssignment> roleStateAssignmentCollections, List<WBAction> actions) {
		Element roleActionAssignment = null;
		for (WBRoleStateAssignment roleStateAssignment : roleStateAssignmentCollections) {
			String firstStateGUID = getFirstStateGUID(actions, roleStateAssignment.getStateGUID());
			if (StringUtils.isEmpty(firstStateGUID)) {
				continue;
			}
			roleActionAssignment = roleStateAssignmentCool.addElement("WBD:WBRoleActionAssignment");
			roleActionAssignment.addAttribute("RoleGUID", roleStateAssignment.getRoleGUID());
			roleActionAssignment.addAttribute("ActionGUID", firstStateGUID);
		}
	}

	private void addWBActionExecutionConditionEle(Element actionExecutionColl, List<WBAction> actions) {
		Element actionExecutionCondition = null;
		for (WBAction action : actions) {
			actionExecutionCondition = actionExecutionColl.addElement("WBD:WBActionExecutionCondition");
			actionExecutionCondition.addAttribute("ConditionGUID", UUID.randomUUID().toString());
			actionExecutionCondition.addAttribute("ActionGUID", action.getGuid());
			actionExecutionCondition.addAttribute("Condition", "");
			actionExecutionCondition.addAttribute("ConditionType", "");
		}
	}

	/**
	 * 根据角色对应的state，找到firststateGUID的action，获取action的guid
	 * 
	 * @param actions
	 * @param stateGuid
	 * @return String action的guid
	 */
	private String getFirstStateGUID(List<WBAction> actions, String stateGuid) {
		for (WBAction action : actions) {
			if (stateGuid.equals(action.getFirstStateGUID())) {// 根据stateID获取开始线段的GUID
				return action.getGuid();
			}
		}
		return "";
	}
}
