package epros.designer.gui.system.combo;

import java.awt.Insets;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;

import epros.designer.gui.common.ConfigCommon;
import epros.designer.gui.common.ConfigCommon.ConfigCommonEnum;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnResourceUtil;

public class JecnSecurityLevelCommon extends KeyValComboBoxCommon {
	private static Logger log = Logger.getLogger(JecnSecurityLevelCommon.class);

	public JecnSecurityLevelCommon(int row, JecnPanel infoPanel, Insets insets, String key, JecnDialog jecnDialog,
			boolean isRequest) {
		super(row, infoPanel, insets, key, jecnDialog, getLevelTitle(), isRequest);
	}

	/**
	 * 保密级别 title初始化
	 * 
	 * @return
	 */
	private static String getLevelTitle() {
		List<JecnDictionary> list;
		try {
			list = ConnectionPool.getOrganizationAction().getDictionarys(DictionaryEnum.JECN_SECURITY_LEVEL);
			return JecnUtil.getName(list) + colon;
		} catch (Exception e) {
			log.error("", e);
		}
		return "";
	}

	@Override
	public Vector getListCheckBoxPo() {
		Vector model = new Vector();
		try {
			List<JecnDictionary> list = ConnectionPool.getOrganizationAction().getDictionarys(
					DictionaryEnum.JECN_SECURITY_LEVEL);
			int languageType = JecnResourceUtil.getLocale() == Locale.CHINESE ? 0 : 1;
			for (JecnDictionary dictionary : list) {
				model.add(new CheckBoxPo(dictionary.getCode().toString(), dictionary.getValue(languageType)));
			}
		} catch (Exception e1) {
			log.error("JecnSecurityLevelCommon getListCheckBoxPo is error", e1);
		}
		return model;
	}

	protected void initCombo(String key) {
		if (StringUtils.isBlank(key) || "0".equals(key)) {
			String localKey = ConfigCommon.INSTANTCE.getConfigProperties(ConfigCommonEnum.securityLevel);
			super.initCombo(StringUtils.isBlank(localKey) ? key : localKey);
		} else {
			super.initCombo(key);
		}
	}
}
