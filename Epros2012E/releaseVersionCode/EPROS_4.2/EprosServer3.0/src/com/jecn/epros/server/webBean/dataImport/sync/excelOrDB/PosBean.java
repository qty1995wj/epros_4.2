package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;

public class PosBean extends AbstractBaseBean {
	/** 岗位编号 POS_NUM */
	private String posNum = null;
	/** 岗位名称 POS_NAME */
	private String posName = null;
	/** 岗位所属部门编号 DEPT_NUM */
	private String deptNum = null;
	/** 存放人员岗位组合数据,即人员+关联岗位的数据 */
	private UserBean userPosBean = null;

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public UserBean getUserPosBean() {
		return userPosBean;
	}

	public void setUserPosBean(UserBean userPosBean) {
		this.userPosBean = userPosBean;
	}

	/**
	 * 
	 * 拼装字符串 把源字符串拼装到目标字符串上
	 * 
	 * @param srcInfo
	 *            目标字符串
	 * @param info
	 *            源字符串
	 * @return String 拼装后的字符串
	 */
	@Override
	public void addError(String info) {
		super.addError(info);
		if (userPosBean != null) {
			userPosBean.addError(info);
		}
	}

	@Override
	public String toInfo() {
		StringBuilder strBuf = new StringBuilder();
		// 岗位编号
		strBuf.append("岗位编号:");
		strBuf.append(this.getPosNum());
		strBuf.append(SyncConstant.EMTRY_SPACE);

		// 岗位名称
		strBuf.append("岗位名称:");
		strBuf.append(this.getPosName());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 岗位所属部门编号
		strBuf.append("岗位所属部门编号:");
		strBuf.append(this.getDeptNum());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 错误信息
		strBuf.append("错误信息:");
		strBuf.append(this.getError());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		strBuf.append("\r\n");
		return strBuf.toString();
	}

	/**
	 * 
	 * 判断所有数据是否都为空
	 * 
	 * @return
	 */
	public boolean isAttributesNull() {
		if (SyncTool.isNullOrEmtryTrim(this.getPosNum())
				&& SyncTool.isNullOrEmtryTrim(this.getPosName())
				&& SyncTool.isNullOrEmtryTrim(this.getDeptNum())) {
			return true;
		} else {
			return false;
		}
	}
}
