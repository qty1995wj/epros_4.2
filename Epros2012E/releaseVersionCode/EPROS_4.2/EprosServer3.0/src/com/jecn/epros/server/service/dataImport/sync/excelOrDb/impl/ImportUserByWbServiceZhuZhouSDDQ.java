package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;
import com.jecn.webservice.crrs.xc.dataimport.Iam_empbaseinfoLine;
import com.jecn.webservice.crrs.xc.dataimport.Organization_viewLine;
import com.jecn.webservice.crrs.xc.dataimport.sdxcWebServiceItem;
import com.jecn.webservice.crs.dataimport.ORG_VIEWLine;
import com.jecn.webservice.crs.dataimport.USER_POS_VIEWLine;
import com.jecn.webservice.crs.dataimport.xxtdWebServiceItem;

/**
 * 株洲时代电器 webservice 人员同步
 * 
 *@author ZXH
 * @date 2016-7-1上午10:02:21
 */
public class ImportUserByWbServiceZhuZhouSDDQ extends AbstractImportUser {
	private static final Logger log = Logger.getLogger(ImportUserByWbServiceZhuZhouSDDQ.class);

	public ImportUserByWbServiceZhuZhouSDDQ(AbstractConfigBean configBean) {
		super(configBean);
	}

	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		return resultDepts();
	}

	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		return resultUsers();
	}

	private List<DeptBean> resultDepts() {
		if (JecnContants.otherLoginType == 23) {
			return getCRRS_SDDQDepts();
		} else if (JecnContants.otherLoginType == 39) {
			return getCrrsSDXCDepts();
		}
		return null;
	}

	private List<DeptBean> getCRRS_SDDQDepts() {
		List<DeptBean> deptBeans = new ArrayList<DeptBean>();
		List<ORG_VIEWLine> orgviewLines = null;
		try {
			orgviewLines = xxtdWebServiceItem.syncOrgViews();
		} catch (Exception e) {
			log.error("信息通达 resultDepts：webservice获取接口数据异常！", e);
			return null;
		}
		DeptBean deptBean = null;
		for (ORG_VIEWLine orgviewLine : orgviewLines) {
			deptBean = new DeptBean();
			if (orgviewLine.getOrg_num() == null || orgviewLine.getPer_org_num() == null) {
				continue;
			}
			deptBean.setDeptNum(orgviewLine.getOrg_num().toString());
			deptBean.setDeptName(orgviewLine.getOrg_name().toString());
			deptBean.setPerDeptNum(orgviewLine.getPer_org_num().toString());
			deptBeans.add(deptBean);
		}
		return deptBeans;
	}

	public static void main(String[] args) {
		List<Organization_viewLine> orgviewLines = new ArrayList<Organization_viewLine>();
		Organization_viewLine line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal(1));
		line.setC_name("集团");
		line.setC_parentunitid(new BigDecimal(-1));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("13762560000237"));
		line.setC_name("时代新材");
		line.setC_parentunitid(new BigDecimal(1));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("11"));
		line.setC_name("时代新材1");
		line.setC_parentunitid(new BigDecimal("13762560000237"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("111"));
		line.setC_name("时代新材11");
		line.setC_parentunitid(new BigDecimal("11"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("112"));
		line.setC_name("时代新材12");
		line.setC_parentunitid(new BigDecimal("11"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("113"));
		line.setC_name("时代新材13");
		line.setC_parentunitid(new BigDecimal("11"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("12"));
		line.setC_name("时代新材2");
		line.setC_parentunitid(new BigDecimal("13762560000237"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("121"));
		line.setC_name("时代新材21");
		line.setC_parentunitid(new BigDecimal("12"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("122"));
		line.setC_name("时代新材22");
		line.setC_parentunitid(new BigDecimal("12"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("123"));
		line.setC_name("时代新材23");
		line.setC_parentunitid(new BigDecimal("12"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("13"));
		line.setC_name("时代新材3");
		line.setC_parentunitid(new BigDecimal("13762560000237"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("131"));
		line.setC_name("时代新材31");
		line.setC_parentunitid(new BigDecimal("13"));
		orgviewLines.add(line);

		line = new Organization_viewLine();
		line.setC_oid_orgunit(new BigDecimal("132"));
		line.setC_name("时代新材32");
		line.setC_parentunitid(new BigDecimal("13"));
		orgviewLines.add(line);

		List<Organization_viewLine> resultDepts = new ArrayList<Organization_viewLine>();
		// 获取有效部门
		// getSDXC_Depts(orgviewLines, resultDepts, "1");
		System.out.println(resultDepts.size());

	}

	private List<DeptBean> getCrrsSDXCDepts() {
		List<DeptBean> deptBeans = new ArrayList<DeptBean>();
		log.info("getCrrsSDXCDepts 获取导入数据开始");
		List<Organization_viewLine> orgviewLines = null;
		try {
			orgviewLines = sdxcWebServiceItem.syncSDXCOrgViews();
		} catch (Exception e) {
			log.error("信息通达 resultDepts：webservice获取接口数据异常！", e);
			return null;
		}
		DeptBean deptBean = null;
		List<Organization_viewLine> resultDepts = new ArrayList<Organization_viewLine>();
		// 获取有效部门
		getSDXC_Depts(orgviewLines, resultDepts, "1");
		log.info("获取有效部门结束");
		// 13762560000237(顶级节点编号)
		for (Organization_viewLine orgviewLine : resultDepts) {
			deptBean = new DeptBean();
			if (orgviewLine.getC_oid_orgunit() == null || orgviewLine.getC_parentunitid() == null) {
				continue;
			}
			deptBean.setDeptNum(orgviewLine.getC_oid_orgunit().toString());
			deptBean.setDeptName(orgviewLine.getC_name().toString());
			if ("-1".equals(orgviewLine.getC_parentunitid().toString())) {
				deptBean.setPerDeptNum("0");
			} else {
				deptBean.setPerDeptNum(orgviewLine.getC_parentunitid().toString());
			}
			deptBeans.add(deptBean);
		}
		return deptBeans;
	}

	private List<String> orgIds = new ArrayList<String>();

	private String SDXC_LEVEL0_DELT = "13762560000237";

	private void getSDXC_Depts(List<Organization_viewLine> viewLines, List<Organization_viewLine> resultDepts,
			String parentId) {
		for (Organization_viewLine organizationViewLine : viewLines) {
			if (SDXC_LEVEL0_DELT.equals(organizationViewLine.getC_oid_orgunit().toString())
					&& parentId.equals(organizationViewLine.getC_parentunitid().toString())) {// 获取顶级节点
				resultDepts.add(organizationViewLine);
				organizationViewLine.setC_parentunitid(new BigDecimal(0));
				// log.info("获取时代新材-公司节点成功");
				getSDXC_ChildDepts(viewLines, resultDepts, SDXC_LEVEL0_DELT);
				orgIds.add(SDXC_LEVEL0_DELT);
				return;
			}
		}
	}

	private void getSDXC_ChildDepts(List<Organization_viewLine> viewLines, List<Organization_viewLine> resultDepts,
			String parentId) {
		for (Organization_viewLine organizationViewLine : viewLines) {
			if (organizationViewLine.getC_parentunitid() != null
					&& organizationViewLine.getC_parentunitid().toString().equals(parentId)) {// 获取顶级节点
				log.info("获取子部门 = " + organizationViewLine.getC_name());
				resultDepts.add(organizationViewLine);
				orgIds.add(organizationViewLine.getC_oid_orgunit().toString());
				getSDXC_ChildDepts(viewLines, resultDepts, organizationViewLine.getC_oid_orgunit().toString());
			}
		}
	}

	private List<UserBean> resultUsers() {
		if (JecnContants.otherLoginType == 23) {
			return getCrrs_SDDQUsers();
		} else if (JecnContants.otherLoginType == 39) {
			return CrrsSDXCUsers();
		}
		return null;
	}

	/**
	 * 株洲时代电气-人员数据
	 * 
	 * @return
	 */
	private List<UserBean> getCrrs_SDDQUsers() {
		List<UserBean> userBeans = new ArrayList<UserBean>();
		List<USER_POS_VIEWLine> userposviewLines = null;
		try {
			userposviewLines = xxtdWebServiceItem.syncUserPosViews();
		} catch (Exception e) {
			log.error("信息通达 resultUsers ：webservice获取接口数据异常！", e);
			return null;
		}

		UserBean userBean = null;
		for (USER_POS_VIEWLine userposviewLine : userposviewLines) {
			if (userposviewLine == null) {
				continue;
			}
			userBean = new UserBean();
			userBean.setUserNum(userposviewLine.getLogin_name().toString());
			userBean.setTrueName(userposviewLine.getTrue_name().toString());
			userBean.setEmail(valueOfString(userposviewLine.getEmail()));
			userBean.setEmailType(valueOfInteger(userposviewLine.getEmail_type()));
			userBean.setPosNum(valueOfString(userposviewLine.getPos_num()));
			userBean.setPosName(valueOfString(userposviewLine.getPos_name()));
			userBean.setDeptNum(valueOfString(userposviewLine.getOrg_num()));
			userBean.setPhone(valueOfString(userposviewLine.getPhone_str()));

			userBeans.add(userBean);
		}
		return userBeans;
	}

	/**
	 * 株洲时代新材-人员数据
	 * 
	 * @return
	 */
	private List<UserBean> CrrsSDXCUsers() {
		List<UserBean> userBeans = new ArrayList<UserBean>();
		List<Iam_empbaseinfoLine> userposviewLines = null;
		try {
			userposviewLines = sdxcWebServiceItem.syncSDXCUserPosViews();
		} catch (Exception e) {
			log.error("时代新材 resultUsers ：webservice获取接口数据异常！", e);
			return null;
		}

		Map<String, UserBean> existsUsers = new HashMap<String, UserBean>();
		UserBean userBean = null;
		for (Iam_empbaseinfoLine userposviewLine : userposviewLines) {
			if (userposviewLine == null) {
				continue;
			}
			String orgNum = valueOfString(userposviewLine.getC_unitid());
			String posNum = valueOfString(userposviewLine.getC_jobcode());
			// 过滤-非时代新材 人员
			if (StringUtils.isNotBlank(orgNum) && !orgIds.contains(orgNum)) {
				continue;
			}
			userBean = new UserBean();
			userBean.setUserNum(userposviewLine.getC_code().toString());
			userBean.setTrueName(userposviewLine.getC_name().toString());
			userBean.setEmail(valueOfString(userposviewLine.getC_companymail()));
			userBean.setEmailType(valueOfInteger("0"));

			if (StringUtils.isNotBlank(orgNum) && StringUtils.isNotBlank(posNum)) {
				userBean.setPosNum(orgNum + "_" + posNum);
				userBean.setPosName(valueOfString(userposviewLine.getC_jobname()));
				userBean.setDeptNum(orgNum);
			}
			userBean.setPhone(valueOfString(userposviewLine.getC_mobile()));
			if (existsUsers.containsKey(userBean.getUserNum())
					&& isSameUser(existsUsers.get(userBean.getUserNum()), userBean)) {// 去除重複人員
				continue;
			}
			existsUsers.put(userBean.getUserNum(), userBean);
			userBeans.add(userBean);
		}
		return userBeans;
	}

	/**
	 * 是否为同一个人员
	 * 
	 * @param existsUser
	 * @param userBean
	 * @return
	 */
	private boolean isSameUser(UserBean existsUser, UserBean userBean) {
		if (existsUser.getPosNum().equals(userBean.getPosNum())
				&& existsUser.getDeptNum().equals(userBean.getDeptNum())) {
			return true;
		}
		return false;
	}

	private String valueOfString(Object object) {
		return object == null ? "" : object.toString();
	}

	private Integer valueOfInteger(Object object) {
		return object == null ? null : Integer.valueOf(object.toString());
	}
}
