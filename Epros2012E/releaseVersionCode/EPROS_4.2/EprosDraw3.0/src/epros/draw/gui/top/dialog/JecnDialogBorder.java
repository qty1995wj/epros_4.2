package epros.draw.gui.top.dialog;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.border.Border;

import epros.draw.util.JecnUIUtil;

/**
 * 
 * 对话框边框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDialogBorder implements Border {

	public Insets getBorderInsets(Component c) {
		return new Insets(1, 2, 2, 2);
	}

	public boolean isBorderOpaque() {
		return false;
	}

	public void paintBorder(Component c, Graphics g, int x, int y, int width,
			int height) {
		// 渲染边框
		g.setColor(JecnUIUtil.getDialogBorderColor());
		g.fillRect(0, 0, width, height);
		// 渲染边框以内颜色
		g.setColor(JecnUIUtil.getDefaultBackgroundColor());
		g.fillRect(1, 1, width - 2, height - 2);
	}

}