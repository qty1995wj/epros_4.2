package com.jecn.epros.domain.org;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.inventory.PosInventoryRoleBean;

import java.util.List;

public class SearchPosGroupStatisticsResultBean {

    //岗位组Id
    private Long figureId;
    //岗位组名称
    private String figureName;
    //岗位组说明
    private String explain;
    //岗位
    private List<PosRelatedBean> posBeans;

    @JsonIgnore
    private List<PosInventoryRoleBean> roles;


    public LinkResource getLink() {
        return new LinkResource(figureId, figureName, ResourceType.POSITION);
    }

    public List<PosInventoryRoleBean> getRoles() {
        return roles;
    }

    public void setRoles(List<PosInventoryRoleBean> roles) {
        this.roles = roles;
    }

    @JsonIgnore
    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    @JsonIgnore
    public String getFigureName() {
        return figureName;
    }

    public void setFigureName(String figureName) {
        this.figureName = figureName;
    }

    public List<PosRelatedBean> getPosBeans() {
        return posBeans;
    }

    public void setPosBeans(List<PosRelatedBean> posBeans) {
        this.posBeans = posBeans;
    }
    @JsonIgnore
    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }
}
