package com.jecn.epros.server.dao.task;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.IBaseDao;

/**
 * 搁置任务审批提醒Dao
 * 
 * @author huoyl
 * 
 */
public interface IJecnTaskApproveDelayDao extends IBaseDao<JecnUser, Long> {

	/**
	 * 获取系统管理员
	 * @return List<Object[]> 系统管理员的列表
	 */
	List<JecnUser> getAdminUserList();
	
	/**
	 * 获取搁置任务的数量
	 * 
	 * @return int 搁置任务的数量
	 */
	public int getTaskDelayCount();


}
