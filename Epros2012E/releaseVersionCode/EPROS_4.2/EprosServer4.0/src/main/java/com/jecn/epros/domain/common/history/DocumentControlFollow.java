package com.jecn.epros.domain.common.history;

import com.jecn.epros.sqlprovider.server3.Common;

/**
 * 文控信息表从表 记录审核人名称和审核阶段名称
 *
 * @author ZHAGNXIAOHU
 */
public class DocumentControlFollow {
    /**
     * 审核阶段名称
     */
    private String appellation;
    /**
     * 审核人名称
     */
    private String name;

    public String getAppellation() {
        return appellation;
    }

    public void setAppellation(String appellation) {
        this.appellation = appellation;
    }

    public String getName() {
        return Common.replaceSlash(name);
    }

    public void setName(String name) {
        this.name = name;
    }
}
