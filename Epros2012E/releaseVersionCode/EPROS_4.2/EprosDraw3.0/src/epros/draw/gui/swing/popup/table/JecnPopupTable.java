package epros.draw.gui.swing.popup.table;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ToolTipManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 弹出表类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnPopupTable extends JTable {
	/** 鼠标移动：鼠标点对应行 */
	private int mouseRow = -1;
	/** 鼠标移动：鼠标点对应列 */
	private int mouseColumn = -1;

	/** 树容器 */
	private JecnPopupTableScrollPane tableScrollPane = null;

	/** 数据对象 */
	private List<Object> popupRowDataList = new ArrayList<Object>();

	public JecnPopupTable() {
		initCompoments();
		initShortCutEvent();
		initEvent();
	}

	private void initCompoments() {
		// 树容器
		tableScrollPane = new JecnPopupTableScrollPane(this);

		// 表模型
		this.setModel(new JecnPopupDefaultTableModel());
		// 表头不可拖动
		this.getTableHeader().setReorderingAllowed(false);
		// 不可调整列大小
		this.getTableHeader().setResizingAllowed(false);
		// 一次选择一个列表索引
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// 网格颜色
		this.setGridColor(JecnUIUtil.getDialogBorderColor());

		// 行高
		this.setRowHeight(25);
		this.setRowMargin(1);
		this.setBorder(null);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 选中前景颜色
		this.setSelectionForeground(Color.BLACK);

		// 表头渲染器
		this.getTableHeader().setDefaultRenderer(
				new JecnPopupTableCellHeaderRenderer());
		this.getTableHeader().setFont(new Font(Font.DIALOG, Font.PLAIN, 12));

		// 滚动条不显示
		tableScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		tableScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 背景颜色
		tableScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		tableScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		// 边框
		tableScrollPane.setBorder(null);
		tableScrollPane.setViewportBorder(null);
		tableScrollPane.setViewportView(this);

		// 表头
		this.getModel().setColumnIdentifiers(getTableHeaderNames());

		// 添加列渲染器
		for (int i = 0; i < this.getColumnCount(); i++) {
			this.getColumnModel().getColumn(i).setCellRenderer(
					new JecnPopupTableCellRenderer(this));
		}
	}

	/**
	 * 
	 * 初始化鼠标事件
	 * 
	 */
	private void initEvent() {
		// 鼠标事件
		MouseAdapter mouseAdapter = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				mouseEventProcess(e);
			}

			public void mousePressed(MouseEvent e) {
				mouseEventProcess(e);
			}

			public void mouseReleased(MouseEvent e) {
				mouseEventProcess(e);
			}

			public void mouseEntered(MouseEvent e) {
				// 指定取消工具提示的延迟值 设置为一小时 3600秒
				ToolTipManager.sharedInstance().setDismissDelay(3600 * 1000);
				
				mouseEventProcess(e);
			}

			public void mouseExited(MouseEvent e) {
				// 返回初始延迟值
				int intD = ToolTipManager.sharedInstance().getInitialDelay();
				// 取消工具提示的延迟值
				ToolTipManager.sharedInstance().setDismissDelay(intD);
				
				mouseEventProcess(e);
			}

			public void mouseWheelMoved(MouseWheelEvent e) {
				mouseEventProcess(e);
			}

			public void mouseDragged(MouseEvent e) {
				mouseEventProcess(e);
			}

			public void mouseMoved(MouseEvent e) {
				// 提示信息
				mouseMovedToolTipText(e);

				mouseEventProcess(e);
			}
		};
		// 表鼠标事件
		this.addMouseListener(mouseAdapter);
		this.addMouseMotionListener(mouseAdapter);

	}



	/**
	 * 
	 * 添加快捷键
	 * 
	 */
	private void initShortCutEvent() {
		// **************快捷键**************//
		AbstractAction enter = (new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				actionPerformedEnter(e);
			}
		});

		InputMap inputMap = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		// 方向键-Enter
		KeyStroke enterKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);

		this.getActionMap().put(inputMap.get(enterKeyStroke), enter);
		// **************快捷键**************//
	}

	@Override
	public DefaultTableModel getModel() {
		return (DefaultTableModel) super.getModel();
	}

	@Override
	public void setModel(TableModel paramTableModel) {
		if (paramTableModel == null
				|| !(paramTableModel instanceof DefaultTableModel)) {
			throw new IllegalArgumentException(
					"参数为空或不是DefaultTableModel对象。paramTableModel="
							+ paramTableModel);
		}
		super.setModel(paramTableModel);
	}

	/**
	 * 
	 * 获取数据对象集合
	 * 
	 * @return List<Object> 数据对象集合
	 */
	public List<Object> getPopupRowDataList() {
		return popupRowDataList;
	}

	/**
	 * 
	 * 添加给定的行数据
	 * 
	 * @param popupRowData
	 *            Object
	 */
	public void addPopupRowDataList(Object popupRowData) {
		if (popupRowData == null) {
			return;
		}
		if (popupRowData instanceof List) {
			addPopupRowDataList((List) popupRowData);
		} else {
			this.popupRowDataList.add(popupRowData);
		}
	}

	public JScrollPane getTableScrollPane() {
		return tableScrollPane;
	}

	/**
	 * 
	 * 加载表数据
	 * 
	 */
	protected void refreshTableData() {
		if (this.getModel() instanceof DefaultTableModel) {
			// 表模型
			DefaultTableModel tableModel = this.getModel();

			for (int i = tableModel.getRowCount() - 1; i >= 0; i--) {
				tableModel.removeRow(i);
			}
			for (int i = 0; i < popupRowDataList.size(); i++) {
				// 添加数据
				Object popupRowData = popupRowDataList.get(i);
				Vector<Object> rowData = getRowVector(popupRowData);
				String[] names = getTableHeaderNames();
				if (rowData == null || names == null
						|| rowData.size() != names.length) {
					continue;
				}
				tableModel.addRow(rowData);
			}
		}
	}

	/**
	 * 
	 * 设置鼠标对应行列
	 * 
	 * @param row
	 * @param column
	 */
	protected void setMouseRowColumn(int row, int column) {
		mouseRow = row;
		mouseColumn = column;
	}

	/**
	 * 
	 * 键盘Enter事件
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void actionPerformedEnter(ActionEvent e) {
		// 选中单元格
		int rowIndex = this.getSelectedRow();
		int columnIndex = this.getSelectedColumn();
		cellEventProcess(rowIndex, columnIndex, e);
	}

	/**
	 * 
	 * 鼠标单击事件
	 * 
	 * @param e
	 *            MouseEvent
	 */
	private void mouseEventProcess(MouseEvent e) {
		int rowIndex = this.rowAtPoint(e.getPoint());
		int columnIndex = this.columnAtPoint(e.getPoint());
		cellEventProcess(rowIndex, columnIndex, e);
	}

	/**
	 * 
	 * 添加给定的行数据集合
	 * 
	 * @param popupRowDataList
	 *            List<Object>
	 */
	private void addPopupRowDataList(List<Object> popupRowDataList) {
		if (popupRowDataList == null || popupRowDataList.size() == 0) {
			return;
		}
		this.popupRowDataList.addAll(popupRowDataList);
	}
	
	/**
	 * 
	 * 鼠标移动添加信息提示
	 * 
	 * @param e MouseEvent
	 */
	private void mouseMovedToolTipText(MouseEvent e) {
		// 鼠标移动对应点
		int rowIndex = JecnPopupTable.this.rowAtPoint(e.getPoint());
		int columnIndex = JecnPopupTable.this.columnAtPoint(e.getPoint());
		if (rowIndex >= 0 && rowIndex < JecnPopupTable.this.getRowCount()
				&& columnIndex >= 0
				&& columnIndex < JecnPopupTable.this.getColumnCount()) {

			if (this.mouseRow == rowIndex && this.mouseColumn == columnIndex) {
				return;
			}

			// 鼠标移动
			Object value = JecnPopupTable.this
					.getValueAt(rowIndex, columnIndex);
			if (value != null && value instanceof ImageIcon) {
				return;
			}
			// 悬浮显示单元格内容
			String text = null;
			if (value != null && !DrawCommon.isNullOrEmtryTrim(value.toString())) {
				text = value.toString();
			}
			this.setToolTipText(text);

			this.mouseRow = rowIndex;
			this.mouseColumn = columnIndex;
		}
	}

	/** 伪表头(用第一行充当表头) */
	protected abstract String[] getTableHeaderNames();

	/**
	 * 
	 * 组件行数据数组
	 * 
	 * @param popupRowData
	 *            Object 行数据对象
	 * @return Vector 行数据集合
	 */
	protected abstract Vector<Object> getRowVector(Object popupRowData);

	/**
	 * 
	 * 按键Enter或点击单元格动作事件处理方法
	 * 
	 * @param row
	 *            int 鼠标对应的行索引
	 * @param column
	 *            int 鼠标对应的列索引
	 * @param e
	 *            AWTEvent 鼠标事件或键盘事件 (MouseEvent/ActionEvent)
	 */
	protected abstract void cellEventProcess(int row, int column, AWTEvent e);
}