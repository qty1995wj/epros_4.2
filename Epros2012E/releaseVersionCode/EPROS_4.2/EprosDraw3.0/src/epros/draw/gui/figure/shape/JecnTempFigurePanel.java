package epros.draw.gui.figure.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import epros.draw.gui.figure.unit.JecnTempFigureBean;

/**
 * 拖动图形虚拟图形组件
 * 
 * @author ZHANGXH
 * @date： 日期：Jun 30, 2012 时间：11:17:49 AM
 */
public class JecnTempFigurePanel extends JPanel {
	/** 记录虚拟图形数据集合 */
	private List<JecnTempFigureBean> tempFigureList = null;

	/** 记录拖动前虚拟图形位置点 */
	private Point oldPoint = null;

	public JecnTempFigurePanel() {
		// 透明
		this.setOpaque(false);
	}

	/**
	 * 根据给定虚拟图形数据集合画图形边框
	 * 
	 * @param g2d
	 */
	private void paintByList(Graphics2D g2d) {
		if (tempFigureList == null) {
			return;
		}
		int userWidth = 0;
		int userHeight = 0;
		double currentAngle = 0;
		int localX = 0;
		int localY = 0;
		for (JecnTempFigureBean tempFigureBean : tempFigureList) {
			if (tempFigureBean.getMapElemType() == null) {
				continue;
			}
			userWidth = tempFigureBean.getUserWidth() - 1;
			userHeight = tempFigureBean.getUserHeight() - 1;

			if (tempFigureList.size() == 1) {
				localX = 0;
				localY = 0;
			} else {
				localX = tempFigureBean.getLocalX() - this.getX();
				localY = tempFigureBean.getLocalY() - this.getY();
			}

			currentAngle = tempFigureBean.getCurrentAngle();
			switch (tempFigureBean.getMapElemType()) {
			case RoundRectWithLine:
				JecnPaintFigure.paintRoundRect(g2d, localX, localY, userWidth, userHeight);
				break;
			case DataImage:
			case ERPImage:
				JecnPaintFigure.paintDataImage(g2d, localX, localY, userWidth, userHeight);
				break;
			case Rhombus:
			case MapRhombus:
			case RiskPointFigure:// 风险点
			case ExpertRhombusAR:// 专家决策
				JecnPaintFigure.paintRhombus(g2d, localX, localY, userWidth, userHeight);
				break;
			case ITRhombus:
				JecnPaintFigure.paintITRhombus(g2d, localX, localY, userWidth, userHeight);
				break;

			case FromFigure:// 圆形
			case ToFigure:
			case AndFigure:
			case ORFigure:
			case XORFigure:
				JecnPaintFigure.paintCircle(g2d, localX, localY, userWidth, userHeight);
				break;
			case DottedRect:
				JecnPaintFigure.paintDottedRect(g2d, localX, localY, userWidth, userHeight);
				break;

			case KCPFigure:// 关键活动
			case KSFFigure:
			case PAFigure:
			case KCPFigureComp:
				JecnPaintFigure.paintKeyActive(g2d, localX, localY, userWidth, userHeight);
				break;

			case EventFigure:// 事件
				JecnPaintFigure.paintEvent(g2d, localX, localY, userWidth, userHeight);
				break;

			case FlowFigureStart:// 流程开始
			case FlowFigureStop:// 流程结束
			case MapFigureEnd: // 流程地图 END
			case SystemDept:// 流程地图信息部
				JecnPaintFigure.paintFlowFigure(g2d, localX, localY, userWidth, userHeight);
				break;

			case EndFigure:// 三角形
			case Triangle:// 三角形
			case FlowLevelFigureOne:
			case FlowLevelFigureTwo:
			case FlowLevelFigureThree:
			case FlowLevelFigureFour:
			case KeyPointFigure:// 关键控制点
				JecnPaintFigure.paintTriangle(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case Rect:// 组织
			case PositionFigure:// 岗位
			case FunctionField:// 功能域
			case CustomFigure:// 客户
			case RoleFigure:// 角色
			case VirtualRoleFigure:// 虚拟角色

			case FreeText:// 自由文本框
			case IconFigure:// 图标插入框
			case DateFreeText:// 时间输入框
			case MapNameText:// 名称输入框
			case ModelFigure:// 泳池
			case TermFigureAR:
				JecnPaintFigure.paintRect(g2d, localX, localY, userWidth, userHeight);
				break;
			case PartitionFigure:// 阶段分隔符
				JecnPaintFigure.paintPartitionFigure(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;

			case Oval:// 流程地图：圆形
			case OvalSubFlowFigure:// 流程图：子流程 8 ht);
			case ActivityOvalSubFlowFigure:// 子流程-活动
				JecnPaintFigure.paintOval(g2d, localX, localY, userWidth, userHeight);
				break;
			case ReverseArrowhead:// 风险箭头 返入
			case ReverseMapArrowhead:
				JecnPaintFigure.paintArrowHead(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case RightArrowhead:
			case RightMapArrowhead:
				JecnPaintFigure.paintRightArrowHead(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case RightHexagon:
			case InterfaceRightHexagon://接口六边形
				JecnPaintFigure.paintRightHexagon(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case HDividingLine:
			case VDividingLine:
			case ParallelLines:
			case VerticalLine:
				JecnPaintFigure.paintVHLine(g2d, localX, localY, userWidth, userHeight);
				break;
			case ImplFigure:// 接口
			case ActivityImplFigure:// 接口-活动
				JecnPaintFigure.paintImplFigure(g2d, localX, localY, userWidth, userHeight);
				break;
			case CommentText:// 注释框
				JecnPaintFigure.paintCommentText(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;

			case Pentagon:// 五边形
			case SystemFigure: // 信息部
			case StageSeparatorPentagon:// 流程图阶段分隔符
				JecnPaintFigure.paintPentagon(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case OneArrowLine:
				JecnPaintFigure.paintOneArrowLine(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case TwoArrowLine:
				JecnPaintFigure.paintTwoArrowLine(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case IsoscelesTrapezoidFigure:
				JecnPaintFigure.paintTrapezoidFigure(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case InputHandFigure:
				JecnPaintFigure.paintInputHandFigure(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case FileImage:
				JecnPaintFigure.paintFileImage(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case ActiveFigureAR:// 勘探院添加 元素开始
				JecnPaintFigure.paintRoundRectAR(g2d, localX, localY, userWidth, userHeight);
				break;
			case StartFigureAR:
				JecnPaintFigure.paintCircle(g2d, localX, localY, userWidth, userHeight);
				break;
			case EndFigureAR:
				JecnPaintFigure.paintCircle(g2d, localX, localY, userWidth, userHeight);
				break;
			case KCPFigureAR:
				JecnPaintFigure.paintTriangleAR(g2d, localX, localY, userWidth, userHeight, currentAngle);
				break;
			case XORFigureAR:
				JecnPaintFigure.paintCircle(g2d, localX, localY, userWidth, userHeight);
				break;
			case ORFigureAR:
				JecnPaintFigure.paintCircle(g2d, localX, localY, userWidth, userHeight);
				break;
			case AndFigureAR:
				JecnPaintFigure.paintCircle(g2d, localX, localY, userWidth, userHeight);
				break;
			case RoleFigureAR:
				JecnPaintFigure.paintRect(g2d, localX, localY, userWidth, userHeight);
				break;
			case TrialReport:
				JecnPaintFigure.paintTrialReport(g2d, localX, localY, userWidth, userHeight, currentAngle);
			default:
				break;
			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g; // 定义画笔

		g2d.setStroke(new BasicStroke(1));
		// 根据给定虚拟图形数据集合画图形边框
		paintByList(g2d);
		// 画虚拟图形外边框
		if (tempFigureList != null && tempFigureList.size() > 1) {
			g2d.setColor(Color.RED);
			g2d.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
		}
	}

	/**
	 * 设置虚拟图形位置及大小
	 * 
	 * @param location
	 * @param userWidth
	 * @param userHeight
	 */
	public void setBounds(Point location, int userWidth, int userHeight) {
		if (userHeight < 1) {
			userHeight = 1;
		}
		if (userWidth < 1) {
			userWidth = 1;
		}
		this.setBounds(location.x, location.y, userWidth, userHeight);
	}

	/**
	 * 获取拖动图形数据集合
	 * 
	 * @return
	 */
	public List<JecnTempFigureBean> getTempFigureList() {
		if (tempFigureList == null) {
			tempFigureList = new ArrayList<JecnTempFigureBean>();
		}
		return tempFigureList;
	}

	public Point getOldPoint() {
		return oldPoint;
	}

	public void setOldPoint(Point oldPoint) {
		this.oldPoint = oldPoint;
	}
}
