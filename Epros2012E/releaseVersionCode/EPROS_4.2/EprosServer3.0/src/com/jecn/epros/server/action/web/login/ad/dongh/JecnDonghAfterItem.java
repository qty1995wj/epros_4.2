package com.jecn.epros.server.action.web.login.ad.dongh;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnPath;

/**
 * 
 * 东航单点登录，在系统启动时需要启动项
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDonghAfterItem {
	private static Logger log = Logger.getLogger(JecnDonghAfterItem.class);
	// AD域IP
	public static String ADIP = "";
	// AD域邮箱后缀名
	public static String domain = "";
	// 东航ADIP 1
	public static String ADIP1 = "";
	// 东航ADIP 2
	public static String ADIP2 = "";
	// 东航ADIP 3
	public static String ADIP3 = "";

	/**
	 * 东航单点登录 读取本地文件数据
	 * 
	 */
	public static void start() {

		log.info("开始读取系统配置表中相关数据......");
		/** 获取单点登录配置文件信息 */
		Properties props = new Properties();

		String url = JecnPath.CLASS_PATH
				+ "cfgFile/dongh/serverPort.properties";
		log.info("配置文件路径:" + url);

		FileInputStream in = null;
		try {

			in = new FileInputStream(url);
			props.load(in);

			// AD域IP
			String ADIP = props.getProperty("com.jecn.loginAction.ADIP");
			if (!JecnCommon.isNullOrEmtryTrim(ADIP)) {
				JecnDonghAfterItem.ADIP = ADIP;
			}
			// AD域邮箱后缀名
			String domain = props.getProperty("com.jecn.loginAction.domain");
			if (!JecnCommon.isNullOrEmtryTrim(domain)) {
				JecnDonghAfterItem.domain = domain;
			}
			// 东航ADIP 1
			String ADIP1 = props.getProperty("com.jecn.loginAction.ADIP1");
			if (!JecnCommon.isNullOrEmtryTrim(ADIP1)) {
				JecnDonghAfterItem.ADIP1 = ADIP1;
			}
			// 东航ADIP 2
			String ADIP2 = props.getProperty("com.jecn.loginAction.ADIP2");
			if (!JecnCommon.isNullOrEmtryTrim(ADIP2)) {
				JecnDonghAfterItem.ADIP2 = ADIP2;
			}
			// 东航ADIP 3
			String ADIP3 = props.getProperty("com.jecn.loginAction.ADIP3");
			if (!JecnCommon.isNullOrEmtryTrim(ADIP3)) {
				JecnDonghAfterItem.ADIP3 = ADIP3;
			}
		} catch (Exception e) {
			log.error("读取单点登录配置文件信息IO流错误！", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("关闭流错误！", e1);
				}
			}
		}
	}
}
