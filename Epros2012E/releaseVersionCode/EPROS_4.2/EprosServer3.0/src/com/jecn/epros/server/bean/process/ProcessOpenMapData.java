package com.jecn.epros.server.bean.process;

import java.io.Serializable;
import java.util.List;

/**
 * 打开流程地图数据集合
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-11-6 时间：下午02:57:22
 */
public class ProcessOpenMapData implements Serializable {
	/** 流程主表 */
	private JecnFlowStructureT jecnFlowStructureT;
	/** 元素数据集合 */
	private List<ProcessMapFigureData> mapFigureDataList;

	/** 文件操作状态 1：可编辑，2：任务中，3 ：只读模式 ； 4：只读模式，可编辑 ；5：占用 */
	private int isAuthSave = 1;

	/** 面板占用人 */
	private String occupierName;

	public String getOccupierName() {
		return occupierName;
	}

	public void setOccupierName(String occupierName) {
		this.occupierName = occupierName;
	}

	public int getIsAuthSave() {
		return isAuthSave;
	}

	public void setIsAuthSave(int isAuthSave) {
		this.isAuthSave = isAuthSave;
	}

	public JecnFlowStructureT getJecnFlowStructureT() {
		return jecnFlowStructureT;
	}

	public void setJecnFlowStructureT(JecnFlowStructureT jecnFlowStructureT) {
		this.jecnFlowStructureT = jecnFlowStructureT;
	}

	public List<ProcessMapFigureData> getMapFigureDataList() {
		return mapFigureDataList;
	}

	public void setMapFigureDataList(List<ProcessMapFigureData> mapFigureDataList) {
		this.mapFigureDataList = mapFigureDataList;
	}

}
