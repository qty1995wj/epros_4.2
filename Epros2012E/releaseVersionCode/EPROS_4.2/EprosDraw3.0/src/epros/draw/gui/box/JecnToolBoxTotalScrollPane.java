package epros.draw.gui.box;

import java.util.List;

import epros.draw.constant.JecnToolBoxConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.system.JecnSystemData;

/**
 * 
 * 流程地图的流程元素库
 * 
 * @author ZHOUXY
 * 
 */
class JecnToolBoxTotalScrollPane extends JecnToolBoxAbstractScrollPane {

	JecnToolBoxTotalScrollPane(JecnToolBoxMainPanel toolBoxMainPanel) {
		super(toolBoxMainPanel);
	}

	@Override
	protected void initToolBoxfavrData() {
		// 常用线集合
		addOftenBtnToLineList(JecnToolBoxConstant.totalMapFavrLineFlowType);
		// 常用图形集合
		addOftenBtnToFigureList(JecnToolBoxConstant.totalMapFavrFigureFlowType);
	}

	/**
	 * 获取数据库中显示的元素集合
	 */
	protected void initShowToolBoxData() {
		List<MapElemType> list = JecnDesignerProcess.getDesignerProcess().getShowElementsType(1);
		if (list == null) {
			return;
		}
		addAllBtnToShowList(list);
	}

	@Override
	protected void initToolBoxGenData() {
		// 获取本地配置文件中不常用图形配置
		List<MapElemType> totalMapToolBoxDataList = JecnSystemData.readTotalListByPropertyFile();

		// 不常用流程元素集合
		addPropertiesGenBtnToList(totalMapToolBoxDataList);
	}

	@Override
	MapElemType[] getGenFigureTypeArray() {
		return JecnToolBoxConstant.totalMapGenNoShowFigureType;
	}

	@Override
	MapElemType[] getGenLineTypeArray() {
		return JecnToolBoxConstant.totalMapGenNoShowLineType;
	}
}
