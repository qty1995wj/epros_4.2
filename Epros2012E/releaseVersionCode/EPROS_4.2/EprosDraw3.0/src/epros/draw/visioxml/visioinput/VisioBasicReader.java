package epros.draw.visioxml.visioinput;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Element;

import visioxml.bean.CharProperty;
import visioxml.bean.ConnectProperty;
import visioxml.bean.FillProperty;
import visioxml.bean.LineProperty;
import visioxml.bean.ParaProperty;
import visioxml.bean.TextBlockProperty;
import visioxml.bean.XForm1DProperty;
import visioxml.bean.XFormProperty;
import visioxml.constant.Constants.Align;
import visioxml.constant.Constants.CellType;
import visioxml.constant.Constants.FillPattern;
import visioxml.constant.Constants.FontStyle;
import visioxml.constant.Constants.FontVertical;
import visioxml.constant.Constants.LinePattern;
import visioxml.constant.Constants.LineRoute;
import visioxml.constant.Constants.Valign;
import visioxml.util.Unit;

/**
 * 读取visio 原始数据
 * 
 * @author admin
 * 
 */
class VisioBasicReader {
	private static final Log log = LogFactory.getLog(VisioBasicReader.class);
	private static VisioBasicReader instance = new VisioBasicReader();
	/** visio 数据对象 **/
	private VisioBasicData data = new VisioBasicData();
	/** visio图形master集合 key=masterID,value=masterNameU **/
	private Map<String, String> master = new HashMap<String, String>();
	/** 保存字体集合<key=id,value=name> **/
	private Map<String, String> faceName = new HashMap<String, String>();
	/** 保存颜色集合<key=id,value=#000000> **/
	private Map<String, String> colorName = new HashMap<String, String>();
	/*** visio图形 key=MasterNameU和value=MasterShape **/
	private Map<String, Element> masterShapeMap = new HashMap<String, Element>();
	/** visio导入关系配置 key=nameU,value=figueType */
	private Map<String, String> configMap;
	/** 连接线 ****/
	private static final List<String> EPROS_COMMONLINE = Arrays.asList("ManhattanLine", "CommonLine");;
	/** 横线 竖线 分割线 ****/
	private static final List<String> EPROS_HDIVIDINGLINE = Arrays.asList("HDividingLine", "VDividingLine",
			"DividingLine");
	/** 横分割线、 竖分割线 ****/
	private static final List<String> EPROS_DASH_LINE = Arrays.asList("ParallelLines", "VerticalLine");
	/*** 活动图形 ****/
	private static final List<String> EPROS_ACTIVITY = Arrays.asList("RoundRectWithLine", "Rhombus", "ITRhombus",
			"DataImage");
	// visio中的泳道需要处理的对象
	/*** Visio2010中的泳道对应的epros标识 ***/
	private static final String EPROS_VISIO_SWIMLANE = "EPROS_VISIO_SWIMLANE";
	/*** Swimlane(角色框) **/
	private static final String VISIO_SWIMLANE = "Swimlane";
	/*** CFF Container（名称文本框） **/
	private static final String VISIO_CFFC = "CFF Container";
	/*** Phase List（竖向分割线） **/
	private static final String VISIO_PHASE_LIST = "Phase List";

	/*** Visio0007中的泳道对应的epros标识 *******/
	private static final String EPROS_VISIO_SWIMLANE_2007 = "EPROS_VISIO_SWIMLANE_2007";
	/*** Functional band(角色框) **/
	private static final String VISIO_FUNCTIONAL_BAND = "Functional band";
	/*** Horizontal holder（名称文本框） **/
	private static final String VISIO_HORIZONTAL_HOLDER = "Horizontal holder";

	private VisioBasicReader() {
	}

	public static VisioBasicReader getInstance() {
		return instance;
	}

	public VisioBasicData readXML(final Document doc, final Map<String, String> configMap) {
		this.configMap = configMap;
		init(doc);
		readPage(doc);
		readShape(doc);
		readConn(doc);
		return data;
	}

	/**
	 * 初始化基本属性，包括（字体，master集合，字体颜色）
	 * 
	 * @param doc
	 */
	@SuppressWarnings("unchecked")
	private void init(final Document doc) {
		List<Element> masters = doc.getRootElement().element("Masters").elements();
		String masterNameU = "";
		for (Element ele : masters) {
			masterNameU = ele.attributeValue("NameU").replaceAll("\\.\\d+", "").trim();
			master.put(ele.attributeValue("ID"), masterNameU);
			masterShapeMap.put(masterNameU, ele.element("Shapes").element("Shape"));
		}
		// 初始化字体
		List<Element> faceNames = doc.getRootElement().element("FaceNames").elements();
		for (Element face : faceNames) {
			faceName.put(face.attributeValue("ID"), face.attributeValue("Name"));
		}
		// 初始化颜色
		if (doc.getRootElement().element("Colors") != null) {
			List<Element> colorEntity = doc.getRootElement().element("Colors").elements();
			for (Element color : colorEntity) {
				colorName.put(color.attributeValue("IX"), color.attributeValue("RGB"));
			}
		}
	}

	/**
	 * 解析page 面板数据
	 * 
	 * @param doc
	 */
	private void readPage(final Document doc) {
		Element page = doc.getRootElement().element("Pages").element("Page");
		Element pageProps = page.element("PageSheet").element("PageProps");
		double panelHeight = Double.valueOf(pageProps.elementText("PageHeight"));
		double panelWidth = Double.valueOf(pageProps.elementText("PageWidth"));
		data.setPanelHeight(panelHeight);
		data.setPanelWidth(panelWidth);
	}

	/**
	 * 读取连接线和图形之间的连接关系
	 * 
	 * @param doc
	 */
	@SuppressWarnings("unchecked")
	private void readConn(final Document doc) {
		Element page = doc.getRootElement().element("Pages").element("Page");
		if (page.element("Connects") == null) {
			return;
		}
		List<Element> conns = page.element("Connects").elements();
		if (conns == null) {
			return;
		}
		Map<String, ConnectProperty> begConn = new HashMap<String, ConnectProperty>();
		Map<String, ConnectProperty> endConn = new HashMap<String, ConnectProperty>();
		ConnectProperty connPro = null;
		for (Element ele : conns) {
			int formSheet = Integer.valueOf(ele.attributeValue("FromSheet"));
			int toSheet = Integer.valueOf(ele.attributeValue("ToSheet"));
			CellType cellType = CellType.getCellType(ele.attributeValue("FromCell"));
			LineRoute lineRoute = LineRoute.getVisioRoute(ele.attributeValue("ToCell"));
			connPro = new ConnectProperty(formSheet, cellType, toSheet, lineRoute);
			if (CellType.BEGINX == cellType) {
				begConn.put(formSheet + "", connPro);
			} else if (CellType.ENDX == cellType) {
				endConn.put(formSheet + "", connPro);
			}
		}
		data.setBegConn(begConn);
		data.setEndConn(endConn);
	}

	/**
	 * 解析图形
	 * 
	 * @param doc
	 */
	@SuppressWarnings("unchecked")
	private void readShape(final Document doc) {
		Element page = doc.getRootElement().element("Pages").element("Page");
		List<Element> shapes = page.element("Shapes").elements();
		if (shapes == null || shapes.size() == 0) {
			return;
		}
		/** 图形集合 **/
		List<VisioShape> shapeList = new ArrayList<VisioShape>();
		/** 动态连接线集合 */
		List<VisioShape> manLineList = new ArrayList<VisioShape>();
		/** 横竖分割线集合 */
		List<VisioShape> vHLineList = new ArrayList<VisioShape>();
		/** 横线、竖线、斜线 */
		List<VisioShape> divedingLineList = new ArrayList<VisioShape>();
		// visio 原始图形
		VisioShape visioShape = null;
		// 泳道存在的标志默认不存在（存在偏移0.9）
		boolean swimlane_flag = false;
		// 泳道存在转换为角色，pinX=0.7；（偏移0.7)
		// 垂直分割线 偏移0.9；（泳道右边的图形全部偏移0.9）
		double moveX = 0.9;
		// 图形层级
		int zIndex = 0;
		for (Element ele : shapes) {
			String id = ele.attributeValue("ID");
			if (ele.attributeValue("Master") == null) {
				log.error("图形" + ele.attributeValue("ID") + "不存在masterz主控图形,过滤掉");
				continue;
			}
			String nameU = getNameU(ele.attributeValue("Master"));
			String figueType = getFigureType(nameU);
			if (figueType == null) {// 配置文件没有对应的图形
				log.equals("配置文件中没有对应的图形,nameU=" + nameU);
				continue;
			}
			visioShape = creatVisioShape(id, nameU, zIndex, ele);
			// 横竖 分割线
			if (EPROS_DASH_LINE.contains(figueType)) {
				vHLineList.add(visioShape);
			} else if (EPROS_COMMONLINE.contains(figueType)) {// 动态连接线
				manLineList.add(visioShape);
			} else if (EPROS_HDIVIDINGLINE.contains(figueType)) {// 横线 竖线 斜线
				divedingLineList.add(visioShape);
			} else if (EPROS_VISIO_SWIMLANE.equals(figueType)) {// visio2010中的泳池
				swimlane_flag = true;
				doVisioSwimlane(shapeList, vHLineList, visioShape, moveX, nameU);
			} else if (EPROS_VISIO_SWIMLANE_2007.equals(figueType)) { // visio中2007泳道
				swimlane_flag = true;
				doVisioSwimlane_2007(shapeList, vHLineList, visioShape, moveX, nameU);
			} else {
				shapeList.add(visioShape);// 图形
			}
			zIndex++;
		}
		// 存在泳池，图形和连接线统一偏移
		if (swimlane_flag) {
			doVisioSwimlane(shapeList, manLineList, moveX);
		}
		data.setShapeList(shapeList);
		data.setManLine(manLineList);
		data.setDivedingLine(divedingLineList);
		data.setvHLine(vHLineList);
	}

	/**
	 * 创建creatVisioShape对象
	 * 
	 * @param id
	 * @param nameU
	 * @param zIndex
	 * @param ele
	 * @return
	 */
	private VisioShape creatVisioShape(String id, String nameU, int zIndex, Element ele) {
		VisioShape visioShape = new VisioShape();
		visioShape.setzIndex(zIndex);
		visioShape.setId(id);
		visioShape.setNameU(nameU);
		visioShape.setXfrom(readXForm(ele));
		visioShape.setXfrom1D(readXForm1D(ele));
		/** 此处图形不存在Line属性取主控图形Line属性 **/
		LineProperty line = readLine(ele);
		if (line == null) {
			line = readLine(masterShapeMap.get(nameU));
		}
		visioShape.setLine(line);
		/** 此处图形不存在Fill属性取主控图形Fill属性 **/
		FillProperty fill = readFill(ele);
		if (fill == null) {
			fill = readFill(masterShapeMap.get(nameU));
		}
		visioShape.setFill(fill);
		visioShape.setVisioChar(readChart(ele));
		visioShape.setTextBlock(readTextBlock(ele));
		visioShape.setPara(readPara(ele));
		visioShape.setActNum(readActNum(ele));
		/** 此处图形不存在Text属性取主控图形Text属性 **/
		String text = readText(ele);
		if (text == null || "".equals(text)) {
			text = masterShapeMap.get(nameU).elementTextTrim("Text");
		}
		visioShape.setText(text);
		return visioShape;
	}

	/**
	 * visio 存在泳道，图形和连接线统一偏移
	 * 
	 * @param shapeList
	 * @param manLineList
	 * @param moveX
	 */
	private void doVisioSwimlane(final List<VisioShape> shapeList, final List<VisioShape> manLineList, double moveX) {
		for (VisioShape visio : shapeList) {
			double pinX = visio.getXfrom().getPinX() + moveX;
			visio.getXfrom().setPinX(pinX);
		}
		for (VisioShape visio : manLineList) {
			double beginX = visio.getXfrom1D().getBeginX() + moveX;
			double endX = visio.getXfrom1D().getEndX() + moveX;
			visio.getXfrom1D().setBeginX(beginX);
			visio.getXfrom1D().setEndX(endX);
		}
		data.setPanelWidth(data.getPanelWidth() + moveX);
	}

	/**
	 * visio_2007泳池特殊处理
	 * 
	 * @param shapeList
	 * @param vHLineList
	 * @param visioShape
	 * @param moveX
	 * @param nameU
	 */
	private void doVisioSwimlane_2007(List<VisioShape> shapeList, List<VisioShape> vHLineList, VisioShape visioShape,
			double moveX, String nameU) {
		// visio2007没有对应的竖向分割线 故需要定义一个，根据角色的X+偏移计算
		boolean IS_EXIT_VH = false;
		if (VISIO_FUNCTIONAL_BAND.equals(nameU)) {// 角色
			visioShape.setVisioChar(null);// 去掉visio07中读取的字体属性
			visioShape.setNameU("JECN_METRIC");
			// 为了统一偏移0.9；然而角色只需要0.7 故设为-0.2
			visioShape.getXfrom().setPinX(-0.2);
			visioShape.getXfrom().setPinY(visioShape.getXfrom().getPinY() - 0.6);// 在原来基础上下移0.6
			visioShape.getXfrom().setWidth(1.0416666666666667);
			visioShape.getXfrom().setHeight(0.625);
			shapeList.add(visioShape);

			// 记录角色的原始X 用来处理竖向分割线
			double pinX_juese = visioShape.getXfrom().getPinX() + visioShape.getXfrom().getWidth();

			// // 水平分割线在原来基础上下移0.6
			double pinY = visioShape.getXfrom().getPinY() - 0.6;// 下移0.6
			visioShape = new VisioShape();
			visioShape.setNameU("EPROS_LINE_PARALLELLINES");
			visioShape.setXfrom1D(new XForm1DProperty(0, pinY, data.getPanelWidth(), pinY));
			vHLineList.add(visioShape);

			if (!IS_EXIT_VH) {
				visioShape = new VisioShape();
				// 添加竖向分割线
				visioShape.setNameU("EPROS_LINE_VERTICALLINE");
				double pinX = pinX_juese + moveX;
				visioShape.setXfrom1D(new XForm1DProperty(pinX, 0, pinX, 0));
				vHLineList.add(visioShape);
				IS_EXIT_VH = true;
			}
		} else if (VISIO_HORIZONTAL_HOLDER.equals(nameU)) {// 名称文本框
			visioShape.setNameU("JECN_MAPNAME_TEXT");
			visioShape.getXfrom().setPinX(data.getPanelWidth() / 2);
			visioShape.getXfrom().setPinY(data.getPanelHeight() - 0.3);// 下移0.3
			visioShape.getXfrom().setWidth(3.125);// epros文本框宽度
			visioShape.getXfrom().setHeight(0.3125);// epros 文本框高度
			shapeList.add(visioShape);
		}
	}

	/**
	 * visio泳池特殊处理
	 * 
	 * @param shapeList
	 * @param vHLineList
	 * @param visioShape
	 * @param moveX
	 * @param nameU
	 */
	private void doVisioSwimlane(List<VisioShape> shapeList, List<VisioShape> vHLineList, VisioShape visioShape,
			double moveX, String nameU) {
		if (VISIO_SWIMLANE.equals(nameU)) {// 角色
			visioShape.setNameU("JECN_METRIC");
			// 为了统一偏移0.9；然而角色只需要0.7 故设为-0.2
			visioShape.getXfrom().setPinX(-0.2);
			visioShape.getXfrom().setWidth(1.0416666666666667);
			visioShape.getXfrom().setHeight(0.625);
			shapeList.add(visioShape);
			// // 水平分割线
			double pinY = visioShape.getXfrom().getPinY() - 0.6;// 下移0.6
			visioShape = new VisioShape();
			visioShape.setNameU("EPROS_LINE_PARALLELLINES");
			visioShape.setXfrom1D(new XForm1DProperty(0, pinY, data.getPanelWidth(), pinY));
			vHLineList.add(visioShape);
		} else if (VISIO_CFFC.equals(nameU)) {// 名称文本框
			visioShape.setNameU("JECN_MAPNAME_TEXT");
			visioShape.getXfrom().setPinY(data.getPanelHeight() - 0.3);// 下移0.3
			visioShape.getXfrom().setWidth(3.125);// epros文本框宽度
			visioShape.getXfrom().setHeight(0.3125);// epros 文本框高度
			shapeList.add(visioShape);
		} else if (VISIO_PHASE_LIST.equals(nameU)) {// 竖向分割线
			visioShape.setNameU("EPROS_LINE_VERTICALLINE");
			XFormProperty xform = visioShape.getXfrom();
			double pinX = xform.getPinX() + moveX;
			visioShape.setXfrom1D(new XForm1DProperty(pinX, 0, pinX, 0));
			vHLineList.add(visioShape);
		}
	}

	/**
	 * 读取图形的文字
	 * 
	 * @param shape
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String readText(final Element shape) {
		String figureText = "";
		if ("Shape".equals(shape.attributeValue("Type")) || shape.elementTextTrim("Text") != null) {
			figureText = shape.elementTextTrim("Text");
			return figureText;
		}
		// 组合图形
		else if ("Group".equals(shape.attributeValue("Type"))) {
			List<Element> shapeList = shape.element("Shapes").elements("Shape");
			for (Element ele : shapeList) {
				if (ele.elementTextTrim("Text") != null && !"".equals(ele.elementTextTrim("Text"))) {
					String text = ele.elementTextTrim("Text").trim();
					// 组合图形不是活动，直接当作文本内容返回
					if (!EPROS_ACTIVITY.contains(getFigureType(getNameU(shape.attributeValue("Master"))))) {
						figureText = text;
						break;
					} else {
						if (!VisioInputTools.isactivityNum(text)) {
							figureText = text;
							break;
						}
					}
				}
			}
		}
		return figureText;
	}

	/**
	 * 读取活动编号
	 * 
	 * @param ele
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String readActNum(final Element ele) {
		String activityNum = "";
		if ("Group".equals(ele.attributeValue("Type"))) {
			List<Element> shapeList = ele.element("Shapes").elements("Shape");
			for (Element shape : shapeList) {
				if (shape.elementTextTrim("Text") != null && !"".equals(shape.elementTextTrim("Text"))) {
					String text = shape.elementTextTrim("Text").trim();
					if (VisioInputTools.isactivityNum(text)) {
						activityNum = text;
						break;
					}
				}
			}
		}
		return activityNum;
	}

	/**
	 * visio 水平对齐方式
	 * 
	 * @param shape
	 * @return
	 */
	private ParaProperty readPara(final Element shape) {
		if (shape.element("Para") == null) {
			return null;
		}
		String visioAlign = shape.element("Para").elementTextTrim("HorzAlign");
		Align align = Align.getAlign(visioAlign);
		ParaProperty para = new ParaProperty();
		para.setHorzAlign(align);
		return para;
	}

	/**
	 * visio垂直对齐方式
	 * 
	 * @param shape
	 * @return
	 */
	private TextBlockProperty readTextBlock(final Element shape) {
		if (shape.element("TextBlock") == null) {
			return null;
		}
		String visioValign = shape.element("TextBlock").elementTextTrim("VerticalAlign");
		Valign valign = Valign.getValign(visioValign);
		TextBlockProperty textBlock = new TextBlockProperty();
		textBlock.setVerticalAlign(valign);
		return textBlock;
	}

	/**
	 * 字体
	 * 
	 * @param shape
	 * @return
	 */
	private CharProperty readChart(final Element shape) {
		Element chart = shape.element("Char");
		if (chart == null) {
			return null;
		}
		// 字体颜色
		String visioColor = chart.elementTextTrim("Color");
		if ((!visioColor.contains("#")) && (colorName.get(visioColor) != null)) {
			visioColor = colorName.get(visioColor);
		}
		// 文字垂直或水平 0:文字方向是水平的（默认） 1:文字方向是垂直的 */
		String visioUseVertical = chart.elementTextTrim("UseVertical");
		// 字体大小，活字典
		String visioSize = chart.elementTextTrim("Size"); // PT 活字典
		// 字体样式 默认常规
		FontStyle visioStyle = FontStyle.getFontStyle(chart.elementTextTrim("Style"));
		// 字体
		int vsisioFont = Integer.valueOf(chart.elementText("Font"));
		// 字体大小磅默认是八磅
		int size = PointsToPt(visioSize) + 3;
		// 字体颜色默认黑色
		Color color = Unit.RGBToColor(visioColor);
		// 0:文字方向是水平的（默认） 1:文字方向是垂直的
		FontVertical useVertical = FontVertical.getFontVertical(visioUseVertical);
		CharProperty chartProperty = new CharProperty();
		chartProperty.setFont(vsisioFont);
		chartProperty.setAsianFont(vsisioFont);
		chartProperty.setSize(size);
		chartProperty.setColor(color);
		chartProperty.setUseVertical(useVertical);
		chartProperty.setStyle(visioStyle);
		return chartProperty;
	}

	private FillProperty getLevalFill(String nameU, Element shape) {
		List<Element> listShape = shape.element("Shapes").elements("Shape");
		// 颜色1
		String fillColorStr = "";
		// 颜色2
		String fillBkColorStr = "";
		// 零级
		if ("EPROS_TRIANGLE_LEVEL_ONE".equals(nameU)) {
			fillColorStr = listShape.get(0).element("Fill").elementTextTrim("FillForegnd");
			fillBkColorStr = listShape.get(1).element("Fill").elementTextTrim("FillForegnd");
		}
		// 一级
		else if ("EPROS_TRIANGLE_LEVEL_TWO".equals(nameU)) {
			fillColorStr = listShape.get(3).element("Fill").elementTextTrim("FillForegnd");
			fillBkColorStr = listShape.get(1).element("Fill").elementTextTrim("FillForegnd");
		}
		// 二级
		else if ("EPROS_TRIANGLE_LEVEL_THREE".equals(nameU)) {
			fillColorStr = listShape.get(2).element("Fill").elementTextTrim("FillForegnd");
			fillBkColorStr = listShape.get(1).element("Fill").elementTextTrim("FillForegnd");
		}
		// 三级
		else if ("EPROS_TRIANGLE_LEVEL_FOUR".equals(nameU)) {
			fillColorStr = listShape.get(1).element("Fill").elementTextTrim("FillForegnd");
			fillBkColorStr = listShape.get(0).element("Fill").elementTextTrim("FillForegnd");
		}
		Color fillColor = Unit.RGBToColor(fillColorStr);
		Color fillBkColor = Unit.RGBToColor(fillBkColorStr);
		FillProperty fillProperty = new FillProperty();
		fillProperty.setFillForegnd(fillColor);
		fillProperty.setFillBkgnd(fillBkColor);
		fillProperty.setFillPattern(FillPattern._30);
		fillProperty.setShdwPattern(FillPattern._00);
		fillProperty.setShdwForegnd(null);
		return fillProperty;
	}

	/**
	 * 填充
	 * 
	 * @param shape
	 * @return
	 */
	private FillProperty readFill(final Element shape) {
		String nameU = "";
		if (shape.attributeValue("Master") != null) {
			nameU = getNameU(shape.attributeValue("Master"));
			if ("EPROS_TRIANGLE_LEVEL_ONE".equals(nameU) || "EPROS_TRIANGLE_LEVEL_TWO".equals(nameU)
					|| "EPROS_TRIANGLE_LEVEL_THREE".equals(nameU) || "EPROS_TRIANGLE_LEVEL_FOUR".equals(nameU)) {
				return getLevalFill(nameU, shape);
			}
		}
		Element fill = shape.element("Fill");
		if (fill == null) {
			return null;
		}
		// 背景模式
		FillPattern fillPattern = FillPattern.getFillPattern(fill.elementTextTrim("FillPattern"));
		// 阴影填充方式
		FillPattern shdwPattern = FillPattern.getFillPattern(fill.elementTextTrim("ShdwPattern"));
		// 颜色1
		String fillColorStr = fill.elementTextTrim("FillForegnd");
		// 颜色2
		String fillBkColorStr = fill.elementTextTrim("FillBkgnd");
		// 阴影填充颜色
		String sdwColorStr = fill.elementTextTrim("ShdwForegnd");
		if ((!fillColorStr.contains("#")) && (colorName.get(fillColorStr) != null)) {
			fillColorStr = colorName.get(fillColorStr);
		}
		if ((!fillBkColorStr.contains("#")) && (colorName.get(fillBkColorStr) != null)) {
			fillBkColorStr = colorName.get(fillBkColorStr);
		}
		if ((!sdwColorStr.contains("#")) && (colorName.get(sdwColorStr) != null)) {
			sdwColorStr = colorName.get(sdwColorStr);
		}
		// page下的
		if (shape.attributeValue("Master") != null) {
			nameU = getNameU(shape.attributeValue("Master"));
		}
		// master 主控图形
		if (shape.attributeValue("NameU") != null) {
			nameU = shape.attributeValue("NameU");
		}
		// 泳池 填充黑色 过滤掉黑色 #000000
		if (VISIO_SWIMLANE.equals(nameU)) {
			if ("#000000".equals(fillColorStr)) {
				return null;
			}
		}
		// 白色#FFFFFF
		if ("#FFFFFF".equals(fillColorStr)) {
			return null;
		}
		if ("#FFFFFF".equals(fillBkColorStr)) {
			return null;
		}
		// if ("#FFFFFF".equals(sdwColorStr)) {
		// return null;
		// }
		FillProperty fillProperty = new FillProperty();
		Color fillColor = Unit.RGBToColor(fillColorStr);
		Color fillBkColor = Unit.RGBToColor(fillBkColorStr);
		Color shadowColor = Unit.RGBToColor(sdwColorStr);
		fillProperty.setFillForegnd(fillColor);
		fillProperty.setFillBkgnd(fillBkColor);
		fillProperty.setFillPattern(fillPattern);
		fillProperty.setShdwPattern(shdwPattern);
		fillProperty.setShdwForegnd(shadowColor);
		return fillProperty;
	}

	/**
	 * 线 边框属性
	 * 
	 * @param shape
	 * @return
	 */
	private LineProperty readLine(final Element shape) {
		// 分割线 横线 竖线 取大小
		List<String> dividingLine = Arrays.asList("EPROS_LINE_DIVIDINGLINE", "EPROS_LINE_HDIVIDINGLINE",
				"EPROS_LINE_VDIVIDINGLINE");
		Element line = shape.element("Line");
		if (line == null) {
			return null;
		}
		// 线条粗细 默认写死 1磅
		String visioLineWeight = "0.01388888888888889";
		String nameU = "";
		if (shape.attributeValue("Master") != null) {
			nameU = getNameU(shape.attributeValue("Master"));
		}
		// master 主控图形
		if ("".equals(nameU)) {
			if (shape.attributeValue("NameU") != null) {
				nameU = shape.attributeValue("NameU");
			}
		}
		if (dividingLine.contains(nameU)) {
			visioLineWeight = line.elementTextTrim("LineWeight");
		}
		// 颜色
		String visioLineColor = line.elementTextTrim("LineColor");
		if ((!visioLineColor.contains("#")) && (colorName.get(visioLineColor) != null)) {
			visioLineColor = colorName.get(visioLineColor);
		}
		// 类型
		String visioLinePattern = line.elementTextTrim("LinePattern");
		int lineWeight = PointsToPt(visioLineWeight);
		Color lineColor = Unit.RGBToColor(visioLineColor);
		LinePattern linePattern = LinePattern.getLinePattern(visioLinePattern);

		LineProperty linePropery = new LineProperty();
		linePropery.setLineColor(lineColor);
		linePropery.setLineWeight(lineWeight);
		linePropery.setLinePattern(linePattern);
		return linePropery;
	}

	/**
	 * Xfrom 1D
	 * 
	 * @param shape
	 * @return
	 */
	private XForm1DProperty readXForm1D(final Element shape) {
		Element xFrom1D = shape.element("XForm1D");
		if (xFrom1D == null) {
			return null;
		}
		// 开始
		double begX = Double.valueOf(xFrom1D.elementText("BeginX"));
		// 结束
		double begY = Double.valueOf(xFrom1D.elementText("BeginY"));
		// 图形的X
		double endX = Double.valueOf(xFrom1D.elementText("EndX"));
		// 图形的Y
		double endY = Double.valueOf(xFrom1D.elementText("EndY"));
		return new XForm1DProperty(begX, begY, endX, endY);
	}

	/**
	 * Xfrom
	 * 
	 * @param shape
	 * @return
	 */
	private XFormProperty readXForm(final Element shape) {
		Element xFrom = shape.element("XForm");
		if (xFrom == null) {
			return null;
		}
		double pinX = Double.valueOf(xFrom.elementText("PinX"));
		double pinY = Double.valueOf(xFrom.elementText("PinY"));
		double width = Double.valueOf(xFrom.elementText("Width"));
		double height = Double.valueOf(xFrom.elementText("Height"));
		double visisAngle = Double.valueOf(xFrom.elementText("Angle"));
		int angle = VisioInputTools.DoubleToInt(Unit.toDeg(visisAngle));
		return new XFormProperty(pinX, pinY, width, height, angle);
	}

	/**
	 * 活字典 转为磅值
	 * 
	 * @param point
	 * @return
	 */
	private int PointsToPt(String point) {
		return VisioInputTools.DoubleToInt(Unit.PointsToPt(Double.valueOf(point)));
	}

	/**
	 * 获取nameU
	 * 
	 * @param masterId
	 * @return
	 */
	private String getNameU(String masterId) {
		String nameU = master.get(masterId);
		return nameU.replaceAll("\\.\\d+", "").trim();
	}

	/**
	 * 根据NameU获取FigureType
	 * 
	 * @param nameU
	 * @return
	 */
	private String getFigureType(String nameU) {
		return configMap.get(nameU);
	}

	public Map<String, String> getFaceName() {
		return faceName;
	}
}
