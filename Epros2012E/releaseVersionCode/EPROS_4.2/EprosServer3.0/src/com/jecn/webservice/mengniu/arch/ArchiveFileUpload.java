package com.jecn.webservice.mengniu.arch;

public class ArchiveFileUpload {

	private ArchiveFile fileDesc;
	private String filePath;
	private String fileName;

	public ArchiveFileUpload(ArchiveFile fileDesc, String filePath, String fileName) {
		this.fileDesc = fileDesc;
		this.filePath = filePath;
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public ArchiveFile getFileDesc() {
		return fileDesc;
	}

	public void setFileDesc(ArchiveFile fileDesc) {
		this.fileDesc = fileDesc;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
