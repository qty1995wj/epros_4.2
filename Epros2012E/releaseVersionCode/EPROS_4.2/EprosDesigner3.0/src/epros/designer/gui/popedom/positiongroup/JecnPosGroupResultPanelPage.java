package epros.designer.gui.popedom.positiongroup;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnTablePageBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.popedom.position.HighEfficiencyOrgPositionTree;
import epros.designer.gui.system.table.JecnTablePaging;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public class JecnPosGroupResultPanelPage extends JecnPanel {
	private static Logger log = Logger.getLogger(JecnPosGroupResultPanelPage.class);
	private Long groupId;

	private JecnDialog jecnDialog;
	Insets insets = new Insets(3, 3, 3, 3);
	public JecnTablePaging resultTable = null;

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();
	/** 清空 */
	private JButton cancelBtn = new JButton(JecnProperties.getValue("emptyBtn"));
	/** 删除选中 */
	private JButton onlyDelSelectBtn = new JButton(JecnProperties.getValue("delSelect"));

	public JecnPosGroupResultPanelPage(int row, JecnPanel infoPanel, Insets insets, JecnDialog jecnDialog, Long groupId) {
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("result")));
		this.groupId = groupId;
		this.jecnDialog = jecnDialog;
		GridBagConstraints c = new GridBagConstraints(0, row, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(this, c);
		initCompotents();
		resultTable.refreshTable("home");
	}

	public void initCompotents() {
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setLayout(new GridBagLayout());
		resultTable = new ResultPanelPage(0, this, insets, jecnDialog);
		GridBagConstraints c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		this.add(buttonPanel, c);
		initButton();
	}

	public void initButton() {
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel.add(cancelBtn);
		buttonPanel.add(onlyDelSelectBtn);
		cancelBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cancelBtnPerformed();
			}

		});
		onlyDelSelectBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				onlyDelSelectBtnPerformed();
			}

		});
	}

	protected void onlyDelSelectBtnPerformed() {
		Set<Long> selectIds = this.resultTable.getSelectIds();
		if (selectIds.size() > 0) {
			try {
				// 执行数据库表的添加保存
				ConnectionPool.getPosGroup().delResultSelectPoss(selectIds, groupId);
			} catch (Exception e) {
				log.error("JecnPosGroupResultPanelPage method onlyDelSelectBtnPerformed  error", e);
			}
			resultTable.refreshTable("home");
		}
	}

	protected void cancelBtnPerformed() {
		try {
			// 执行数据库表的添加保存
			ConnectionPool.getPosGroup().cancelResultPoss(groupId);
			resultTable.refreshTable("home");
		} catch (Exception e) {
			log.error("JecnPosGroupResultPanelPage method cancelResultPoss  error", e);
		}
	}

	class ResultPanelPage extends JecnTablePaging {

		public ResultPanelPage(int row, JecnPanel infoPanel, Insets insets, JecnDialog jecnDialog) {
			super(row, infoPanel, insets, jecnDialog);
		}

		@Override
		public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
			Vector<Vector<String>> content = new Vector<Vector<String>>();
			if (list != null) {
				for (JecnTreeBean jecnTreeBean : list) {
					Vector<String> data = new Vector<String>();
					data.add(jecnTreeBean.getId().toString());
					data.add(jecnTreeBean.getName());
					data.add(jecnTreeBean.getPname());
					content.add(data);
				}
			}
			return content;
		}

		@Override
		public JecnTablePageBean getJecnTablePageBean(String operationType, int pageSize) {
			try {
				// 执行数据库表的添加保存
				return ConnectionPool.getPosGroup().showPos(
						DrawCommon.isNullOrEmtryTrim(resultTable.nowPage.getText()) ? 0 : Integer
								.parseInt(resultTable.nowPage.getText()), operationType, pageSize, groupId);
			} catch (Exception e) {
				log.error("ResultPanelPage method getJecnTablePageBean  error", e);
			}
			return null;
		}

		@Override
		public Vector<String> getTableTitle() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("positionName"));// 岗位名称
			title.add(JecnProperties.getValue("deptName"));// 部门名称
			return title;
		}

		@Override
		public void isClickTable() {
			Set<Long> selectIds = this.getSelectIds();
			if (selectIds == null) {
				return;
			}
			Iterator<Long> ids = selectIds.iterator();
			while (ids.hasNext()) {
				Long id = ids.next();
				try {
					JecnTreeBean treeBean = ConnectionPool.getOrganizationAction().getJecnTreeBeanByposId(id);
					if (treeBean == null) {
						return;
					}
					JecnTree tree = ((UpdatePosToGroupDialog) jecnDialog).getjTree();
					HighEfficiencyOrgPositionTree jTree = ((HighEfficiencyOrgPositionTree) tree);
					jTree.setAllowExpand(false);
					JecnTreeCommon.searchNodePos(treeBean, jTree);
					jTree.setAllowExpand(true);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("PositionChooseDialog isClickTable is error！", e);
				}
			}
		}

	}
}
