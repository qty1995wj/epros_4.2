package com.jecn.webservice.jbshihua.entity;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.dataImport.sync.JecnOrgView;

public interface DeptViewDao extends IBaseDao<String, String> {

	int deleteDept(String code) throws Exception;

	int insertDept(JecnOrgView dept) throws Exception;

	int updateDept(JecnOrgView dept) throws Exception;

	JecnOrgView getDept(String code) throws Exception;

	int updateDeptByUUID(JecnOrgView dept) throws Exception;

	int insertDeptByUUID(JecnOrgView dept) throws Exception;

}
