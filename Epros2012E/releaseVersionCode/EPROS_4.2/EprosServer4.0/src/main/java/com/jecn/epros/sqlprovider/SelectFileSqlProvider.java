package com.jecn.epros.sqlprovider;

import com.jecn.epros.security.JecnContants;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.sqlprovider.server3.SqlConstant;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.Utils;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;
import java.util.Map;

public class SelectFileSqlProvider extends BaseSqlProvider {


    /**
     * 流程架构 树展开
     *
     * @param map
     * @return
     */
    public String findFlowMapTree(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        Long pId = Long.valueOf(map.get("pId").toString());
        return "SELECT DISTINCT JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME,PUB.FLOW_NAME AS P_FILE_NAME"
                + "   ,JFS.ISFLOW AS FILE_TYPE,CASE WHEN T.FLOW_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JFS.PRE_FLOW_ID,JFS.SORT_ID"
                + "   FROM JECN_FLOW_STRUCTURE JFS"
                + "   LEFT JOIN JECN_FLOW_STRUCTURE PUB ON JFS.PRE_FLOW_ID = PUB.FLOW_ID"
                + "   LEFT JOIN JECN_FLOW_STRUCTURE T ON T.DEL_STATE=0 AND T.ISFLOW=0 AND  T.PRE_FLOW_ID = JFS.FLOW_ID"
                + "   WHERE JFS.DEL_STATE=0 AND JFS.ISFLOW = 0 AND JFS.PRE_FLOW_ID=" + pId + " AND JFS.PROJECTID="
                + projectId + "   ORDER BY JFS.PRE_FLOW_ID,JFS.SORT_ID,JFS.FLOW_ID";
    }


    public String getFileIdByRuleIdSql(Map<String, Object> map) {
        String pub = map.get("pub").toString();
        String sql = "select rule_id  from G020_RULE_FILE_CONTENT where ID = #{id}";
        return sql;
    }

    /**
     * 流程架构 搜索
     *
     * @param map
     * @return
     */
    public String searchFlowMap(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "SELECT JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME,PUB.FLOW_NAME AS P_FILE_NAME,0 as file_type"
                + "          FROM JECN_FLOW_STRUCTURE JFS"
                + "          LEFT JOIN JECN_FLOW_STRUCTURE PUB ON JFS.PRE_FLOW_ID = PUB.FLOW_ID"
                + "          WHERE JFS.DEL_STATE=0 AND JFS.ISFLOW = 0  AND JFS.PROJECTID=" + projectId;
        if (Utils.strIsNotBlank(map.get("name"))) {
            String name = map.get("name").toString();
            sql += "          AND  JFS.FLOW_NAME like '%" + name + "%'";
        }
        String packSql = "select * from (" + sql + ") temp ";
        return packSql;
    }

    /**
     * 流程 树展开
     *
     * @param map
     * @return
     */
    public String findFlowTree(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String condition = getPidCondition(map);

        return "SELECT DISTINCT JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME,JFS.PRE_FLOW_ID AS P_FILE_ID,PUB.FLOW_NAME AS P_FILE_NAME"
                + "   ,JFS.ISFLOW AS FILE_TYPE,CASE WHEN T.FLOW_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JFS.SORT_ID,JFS.NODE_TYPE"
                + "   FROM JECN_FLOW_STRUCTURE JFS"
                + "   LEFT JOIN JECN_FLOW_STRUCTURE PUB ON JFS.PRE_FLOW_ID = PUB.FLOW_ID"
                + "   LEFT JOIN JECN_FLOW_STRUCTURE T ON T.DEL_STATE=0 AND T.PRE_FLOW_ID = JFS.FLOW_ID"
                + "   WHERE JFS.DEL_STATE=0 AND JFS.PRE_FLOW_ID" + condition + " AND JFS.PROJECTID=" + projectId
                + "   ORDER BY JFS.PRE_FLOW_ID,JFS.SORT_ID,JFS.FLOW_ID";
    }

    private String getPidCondition(Map<String, Object> map) {
        String condition = "";
        if (map.containsKey("pId")) {
            Long pId = Long.valueOf(map.get("pId").toString());
            condition = " = " + pId;
        } else if (map.containsKey("pIds")) {
            String pIds = map.get("pIds").toString();
            condition = " in " + pIds;
        }
        return condition;
    }

    /**
     * 流程
     *
     * @param map
     * @return
     */
    public String findFlowTreeNode(Map<String, Object> map) {
        String pub = getPubStr(map);
        return "SELECT DISTINCT JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME,PUB.FLOW_NAME AS P_FILE_NAME"
                + "   ,JFS.ISFLOW AS FILE_TYPE,CASE WHEN T.FLOW_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JFS.PRE_FLOW_ID,JFS.SORT_ID"
                + " ,JFS.NODE_TYPE NODE_TYPE"
                + "   FROM JECN_FLOW_STRUCTURE" + pub + " JFS"
                + "   LEFT JOIN JECN_FLOW_STRUCTURE" + pub + " PUB ON JFS.PRE_FLOW_ID = PUB.FLOW_ID"
                + "   LEFT JOIN JECN_FLOW_STRUCTURE" + pub + " T ON T.DEL_STATE <> 1 AND T.PRE_FLOW_ID = JFS.FLOW_ID"
                + "   WHERE JFS.DEL_STATE <> 1  AND JFS.FLOW_ID=#{id}";
    }

    private String getPubStr(Map<String, Object> map) {
        return map.containsKey("pub") ? map.get("pub").toString() : "";
    }

    /**
     * 流程搜索
     *
     * @param map
     * @return
     */
    public String searchFlow(Map<String, Object> map) {

        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "SELECT JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME,PUB.FLOW_NAME AS P_FILE_NAME,1 as file_type"
                + "          FROM JECN_FLOW_STRUCTURE JFS"
                + "          LEFT JOIN JECN_FLOW_STRUCTURE PUB ON JFS.PRE_FLOW_ID = PUB.FLOW_ID"
                + "          WHERE JFS.DEL_STATE=0 AND JFS.ISFLOW = 1  AND JFS.PROJECTID=" + projectId;
        if (Utils.strIsNotBlank(map.get("name"))) {
            String name = map.get("name").toString();
            sql += "          AND  JFS.FLOW_NAME like '%" + name + "%'";
        }
        String packSql = "select * from (" + sql + ") temp";
        return packSql;
    }

    /**
     * 制度 树展开
     *
     * @param map
     * @return
     */
    public String findRuleTree(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String condition = getPidCondition(map);
        return "SELECT DISTINCT JR.ID AS FILE_ID,JR.RULE_NAME AS FILE_NAME,JR.PER_ID AS P_FILE_ID,PUB.RULE_NAME AS P_FILE_NAME"
                + "    ,JR.IS_DIR AS FILE_TYPE,CASE WHEN T.ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.PER_ID,JR.SORT_ID"
                + "    FROM JECN_RULE JR" + "    LEFT JOIN JECN_RULE PUB ON PUB.ID=JR.PER_ID"
                + "    LEFT JOIN JECN_RULE T ON T.PER_ID=JR.ID" + "    WHERE JR.DEL_STATE=0 AND JR.PER_ID" + condition + " AND JR.PROJECT_ID="
                + projectId + "    ORDER BY JR.PER_ID,JR.SORT_ID,JR.ID";
    }

    /**
     * 制度
     *
     * @param map
     * @return
     */
    public String findRuleTreeNode(Map<String, Object> map) {
        String pub = getPubStr(map);
        return "SELECT DISTINCT JR.ID AS FILE_ID,JR.RULE_NAME AS FILE_NAME,PUB.RULE_NAME AS P_FILE_NAME"
                + "    ,JR.IS_DIR AS FILE_TYPE,CASE WHEN T.ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.PER_ID,JR.SORT_ID, JR.IS_FILE_LOCAL"
                + "    FROM JECN_RULE" + pub + " JR" + "    LEFT JOIN JECN_RULE" + pub + " PUB ON PUB.ID=JR.PER_ID"
                + "    LEFT JOIN JECN_RULE" + pub + " T ON T.PER_ID=JR.ID" + "    WHERE JR.DEL_STATE != 1 AND JR.ID=#{id}";
    }

    /**
     * 制度搜索
     *
     * @param map
     * @return
     */
    public String searchRule(Map<String, Object> map) {

        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "SELECT JR.ID AS FILE_ID,JR.RULE_NAME AS FILE_NAME,PUB.RULE_NAME AS P_FILE_NAME, JR.IS_DIR AS FILE_TYPE" +
                "             FROM JECN_RULE JR" +
                "             LEFT JOIN JECN_RULE PUB ON PUB.ID=JR.PER_ID" +
                "             WHERE JR.IS_DIR<>0 and JR.DEL_STATE=0 and JR.PROJECT_ID=" + projectId;
        if (Utils.strIsNotBlank(map.get("name"))) {
            String name = map.get("name").toString();
            sql += "          AND  JR.RULE_NAME like '%" + name + "%'";
        }
        String packSql = "select * from (" + sql + ") temp";
        return packSql;
    }

    /**
     * 标准 树展开
     *
     * @param map
     * @return
     */
    public String findStandardTree(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String conditon = getPidCondition(map);
        return "SELECT DISTINCT JR.CRITERION_CLASS_ID AS FILE_ID,JR.CRITERION_CLASS_NAME AS FILE_NAME,JR.PRE_CRITERION_CLASS_ID AS P_FILE_ID,PUB.CRITERION_CLASS_NAME AS P_FILE_NAME"
                + "    ,JR.STAN_TYPE AS FILE_TYPE"
                + "    ,CASE WHEN SUB.CRITERION_CLASS_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.PRE_CRITERION_CLASS_ID,JR.SORT_ID"
                + "    FROM JECN_CRITERION_CLASSES JR"
                + "    LEFT JOIN JECN_CRITERION_CLASSES PUB ON PUB.CRITERION_CLASS_ID=JR.PRE_CRITERION_CLASS_ID"
                + "    LEFT JOIN JECN_CRITERION_CLASSES SUB ON SUB.PRE_CRITERION_CLASS_ID=JR.CRITERION_CLASS_ID"
                + "    WHERE JR.PRE_CRITERION_CLASS_ID" + conditon + " AND JR.PROJECT_ID=" + projectId
                + "    ORDER BY JR.PRE_CRITERION_CLASS_ID,JR.SORT_ID,JR.CRITERION_CLASS_ID";
    }

    /**
     * 标准
     *
     * @param map
     * @return
     */
    public String findStandardTreeNode(Map<String, Object> map) {
        return "SELECT DISTINCT CASE" +
                "                  WHEN JR.STAN_TYPE = 2 OR JR.STAN_TYPE = 3 THEN" +
                "                   FS.FLOW_ID" +
                "                  ELSE" +
                "                   JR.CRITERION_CLASS_ID" +
                "                END AS FILE_ID," +
                "                JR.CRITERION_CLASS_NAME AS FILE_NAME," +
                "                PUB.CRITERION_CLASS_NAME AS P_FILE_NAME," +
                "                JR.STAN_TYPE AS FILE_TYPE," +
                "                CASE" +
                "                  WHEN SUB.CRITERION_CLASS_ID IS NULL THEN" +
                "                   0" +
                "                  ELSE" +
                "                   1" +
                "                END AS FILE_COUNT," +
                "                JR.PRE_CRITERION_CLASS_ID," +
                "                JR.SORT_ID" +
                "  FROM JECN_CRITERION_CLASSES JR" +
                "  LEFT JOIN JECN_CRITERION_CLASSES PUB" +
                "    ON PUB.CRITERION_CLASS_ID = JR.PRE_CRITERION_CLASS_ID" +
                "  LEFT JOIN JECN_CRITERION_CLASSES SUB" +
                "    ON SUB.PRE_CRITERION_CLASS_ID = JR.CRITERION_CLASS_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE FS" +
                "    ON FS.FLOW_ID = JR.RELATED_ID" +
                "   AND JR.STAN_TYPE IN (2, 3)" +
                "    WHERE JR.CRITERION_CLASS_ID=#{id}";
    }

    /**
     * 标准搜索
     *
     * @param map
     * @return
     */
    public String searchStandard(Map<String, Object> map) {

        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "SELECT  JR.CRITERION_CLASS_ID AS FILE_ID,JR.CRITERION_CLASS_NAME AS FILE_NAME,PUB.CRITERION_CLASS_NAME AS P_FILE_NAME,JR.STAN_TYPE AS FILE_TYPE" +
                "             FROM JECN_CRITERION_CLASSES JR" +
                "             LEFT JOIN JECN_CRITERION_CLASSES PUB ON PUB.CRITERION_CLASS_ID=JR.PRE_CRITERION_CLASS_ID" +
                "             WHERE  JR.STAN_TYPE<>0 AND JR.PROJECT_ID=  " + projectId;
        if (Utils.strIsNotBlank(map.get("name"))) {
            String name = map.get("name").toString();
            sql += "          AND  JR.CRITERION_CLASS_NAME like '%" + name + "%'";
        }
        String packSql = "select * from (" + sql + ") temp";
        return packSql;
    }

    /**
     * 风险 树展开
     *
     * @param map
     * @return
     */
    public String findRiskTree(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String condition = getPidCondition(map);
        return "SELECT DISTINCT JR.ID AS FILE_ID,JR.NAME AS FILE_NAME,JR.PARENT_ID AS P_FILE_ID,PUB.NAME AS P_FILE_NAME"
                + "    ,JR.IS_DIR AS FILE_TYPE"
                + "    ,CASE WHEN SUB.ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.PARENT_ID,JR.SORT"
                + "    FROM JECN_RISK JR" + "    LEFT JOIN JECN_RISK PUB ON PUB.ID=JR.PARENT_ID"
                + "    LEFT JOIN JECN_RISK SUB ON SUB.PARENT_ID=JR.ID" + "    WHERE JR.PARENT_ID" + condition
                + " AND JR.PROJECT_ID=" + projectId + "    ORDER BY JR.PARENT_ID,JR.SORT,JR.ID";
    }

    /**
     * 风险
     *
     * @param map
     * @return
     */
    public String findRiskTreeNode(Map<String, Object> map) {
        return "SELECT DISTINCT JR.ID AS FILE_ID,JR.NAME AS FILE_NAME,PUB.NAME AS P_FILE_NAME"
                + "    ,JR.IS_DIR AS FILE_TYPE"
                + "    ,CASE WHEN SUB.ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.PARENT_ID,JR.SORT"
                + "    FROM JECN_RISK JR" + "    LEFT JOIN JECN_RISK PUB ON PUB.ID=JR.PARENT_ID"
                + "    LEFT JOIN JECN_RISK SUB ON SUB.ID=JR.ID" + "   WHERE JR.ID=#{id}";
    }

    /**
     * 风险搜索
     *
     * @param map
     * @return
     */
    public String searchRisk(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "SELECT JR.ID AS FILE_ID,JR.NAME AS FILE_NAME,PUB.NAME AS P_FILE_NAME,JR.IS_DIR AS FILE_TYPE" +
                "             FROM JECN_RISK JR" +
                "             LEFT JOIN JECN_RISK PUB ON PUB.ID=JR.PARENT_ID" +
                "             WHERE JR.IS_DIR<>0 AND JR.PROJECT_ID=  " + projectId;
        if (Utils.strIsNotBlank(map.get("name"))) {
            String name = map.get("name").toString();
            sql += "          AND  JR.NAME like '%" + name + "%'";
        }
        String packSql = "select * from (" + sql + ") temp";
        return packSql;
    }

    /**
     * 文件 树展开
     *
     * @param map
     * @return
     */
    public String findFileTree(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String conditon = getPidCondition(map);
        return "SELECT DISTINCT JR.FILE_ID AS FILE_ID,JR.FILE_NAME AS FILE_NAME,JR.PER_FILE_ID AS P_FILE_ID,PUB.FILE_NAME AS P_FILE_NAME"
                + "    ,JR.IS_DIR AS FILE_TYPE"
                + "    ,CASE WHEN SUB.FILE_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.PER_FILE_ID,JR.SORT_ID"
                + "    FROM JECN_FILE JR" + "    LEFT JOIN JECN_FILE PUB ON PUB.FILE_ID=JR.PER_FILE_ID"
                + "    LEFT JOIN JECN_FILE SUB ON SUB.PER_FILE_ID=JR.FILE_ID AND SUB.DEL_STATE=0"
                + "    WHERE JR.PER_FILE_ID" + conditon + " AND JR.PROJECT_ID=" + projectId + " AND JR.DEL_STATE=0 AND JR.HIDE=1 "
                + "    ORDER BY JR.PER_FILE_ID,JR.SORT_ID,JR.FILE_ID";
    }

    /**
     * 文件 树展开
     *
     * @param map
     * @return
     */
    public String findFileTreeNode(Map<String, Object> map) {
        String pub = getPubStr(map);
        return "SELECT DISTINCT JR.FILE_ID AS FILE_ID,JR.FILE_NAME AS FILE_NAME,PUB.FILE_NAME AS P_FILE_NAME"
                + "    ,JR.IS_DIR AS FILE_TYPE"
                + "    ,CASE WHEN SUB.FILE_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.PER_FILE_ID,JR.SORT_ID"
                + "    FROM JECN_FILE" + pub + " JR" + "    LEFT JOIN JECN_FILE" + pub + " PUB ON PUB.FILE_ID=JR.PER_FILE_ID"
                + "    LEFT JOIN JECN_FILE" + pub + " SUB ON SUB.PER_FILE_ID=JR.FILE_ID AND SUB.DEL_STATE <>1"
                + "    WHERE JR.FILE_ID=#{id} AND JR.DEL_STATE<>1";
    }

    /**
     * 文件搜索
     *
     * @param map
     * @return
     */
    public String searchFile(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "SELECT JR.FILE_ID AS FILE_ID,JR.FILE_NAME AS FILE_NAME,PUB.FILE_NAME AS P_FILE_NAME,JR.IS_DIR AS FILE_TYPE" +
                "             FROM JECN_FILE JR" +
                "             LEFT JOIN JECN_FILE PUB ON PUB.FILE_ID=JR.PER_FILE_ID" +
                "             WHERE JR.IS_DIR<>0   AND JR.PROJECT_ID=" + projectId + " AND JR.DEL_STATE=0";
        if (Utils.strIsNotBlank(map.get("name"))) {
            String name = map.get("name").toString();
            sql += "          AND  JR.FILE_NAME like '%" + name + "%'";
        }
        String packSql = "select * from (" + sql + ") temp";
        return packSql;
    }

    /**
     * 组织 树展开
     *
     * @param map
     * @return
     */
    public String findOrgTree(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        Long pId = Long.valueOf(map.get("pId").toString());
        return "SELECT DISTINCT JR.ORG_ID AS FILE_ID,JR.ORG_NAME AS FILE_NAME,PUB.ORG_NAME AS P_FILE_NAME"
                + "    ,0 AS FILE_TYPE"
                + "    ,CASE WHEN SUB.ORG_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.PER_ORG_ID,JR.SORT_ID"
                + "    FROM JECN_FLOW_ORG JR" + "    LEFT JOIN JECN_FLOW_ORG PUB ON PUB.ORG_ID=JR.PER_ORG_ID"
                + "    LEFT JOIN JECN_FLOW_ORG SUB ON SUB.PER_ORG_ID=JR.ORG_ID AND SUB.DEL_STATE=0"
                + "    WHERE JR.PER_ORG_ID=" + pId + " AND JR.PROJECTID=" + projectId + " AND JR.DEL_STATE=0"
                + "    ORDER BY JR.PER_ORG_ID,JR.SORT_ID,JR.ORG_ID";
    }

    /**
     * 组织
     *
     * @param map
     * @return
     */
    public String findOrgTreeNode(Map<String, Object> map) {
        return "SELECT DISTINCT JR.ORG_ID AS FILE_ID,JR.ORG_NAME AS FILE_NAME,PUB.ORG_NAME AS P_FILE_NAME"
                + "    ,0 AS FILE_TYPE"
                + "    ,CASE WHEN SUB.ORG_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.PER_ORG_ID,JR.SORT_ID"
                + "    FROM JECN_FLOW_ORG JR" + "    LEFT JOIN JECN_FLOW_ORG PUB ON PUB.ORG_ID=JR.PER_ORG_ID"
                + "    LEFT JOIN JECN_FLOW_ORG SUB ON SUB.PER_ORG_ID=JR.ORG_ID AND SUB.DEL_STATE=0"
                + "    WHERE JR.ORG_ID=#{id} AND JR.DEL_STATE=0";
    }

    /**
     * 部门搜索
     *
     * @param map
     * @return
     */
    public String searchOrg(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "SELECT JR.ORG_ID AS FILE_ID,JR.ORG_NAME AS FILE_NAME,PUB.ORG_NAME AS P_FILE_NAME,0 AS FILE_TYPE" +
                "     FROM JECN_FLOW_ORG JR" +
                "             LEFT JOIN JECN_FLOW_ORG PUB ON PUB.ORG_ID=JR.PER_ORG_ID" +
                "             WHERE   JR.PROJECTID= " + projectId + "  AND JR.DEL_STATE=0";
        if (Utils.strIsNotBlank(map.get("name"))) {
            String name = map.get("name").toString();
            sql += "          AND  JR.ORG_NAME like '%" + name + "%'";
        }
        String packSql = "select * from (" + sql + ") temp";
        return packSql;
    }

    /**
     * 岗位 树展开
     *
     * @param map
     * @return
     */
    public String findPosTree(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String conditon = getPidCondition(map);
        return "SELECT POSITION_TREE.* FROM"
                + " (SELECT DISTINCT JR.ORG_ID AS FILE_ID,JR.ORG_NAME AS FILE_NAME,JR.PER_ORG_ID AS P_FILE_ID,PUB.ORG_NAME AS P_FILE_NAME"
                + "    ,0 AS FILE_TYPE"
                + "    ,CASE WHEN SUB.ORG_ID IS NULL AND POS.FIGURE_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.SORT_ID"
                + "    FROM JECN_FLOW_ORG JR" + "    LEFT JOIN JECN_FLOW_ORG PUB ON PUB.ORG_ID=JR.PER_ORG_ID"
                + "    LEFT JOIN JECN_FLOW_ORG SUB ON SUB.PER_ORG_ID=JR.ORG_ID AND SUB.DEL_STATE=0"
                + "    LEFT JOIN JECN_FLOW_ORG_IMAGE POS ON POS.ORG_ID = JR.ORG_ID AND POS.FIGURE_TYPE='PositionFigure'"
                + "    WHERE JR.PER_ORG_ID" + conditon + " AND JR.PROJECTID=" + projectId + " AND JR.DEL_STATE=0" + " UNION"
                + "  SELECT JFOI.FIGURE_ID AS FILE_ID,JFOI.FIGURE_TEXT AS FILE_NAME,ORG.ORG_ID AS P_FILE_ID,ORG.ORG_NAME AS P_FILE_NAME"
                + "     ,1 AS FILE_TYPE,0 AS FILE_COUNT,JFOI.SORT_ID" + "     FROM JECN_FLOW_ORG_IMAGE JFOI"
                + "     LEFT JOIN JECN_FLOW_ORG ORG ON JFOI.ORG_ID = ORG.ORG_ID"
                + "     WHERE JFOI.FIGURE_TYPE='PositionFigure' AND JFOI.ORG_ID " + conditon + ") POSITION_TREE"
                + "     ORDER BY POSITION_TREE.FILE_TYPE,POSITION_TREE.SORT_ID";
    }

    /**
     * 岗位
     *
     * @param map
     * @return
     */
    public String findPosTreeNode(Map<String, Object> map) {
        return "  SELECT DISTINCT JFOI.FIGURE_ID AS FILE_ID,JFOI.FIGURE_TEXT AS FILE_NAME,ORG.ORG_NAME AS P_FILE_NAME"
                + "     ,1 AS FILE_TYPE,0 AS FILE_COUNT,JFOI.SORT_ID" + "     FROM JECN_FLOW_ORG_IMAGE JFOI"
                + "     LEFT JOIN JECN_FLOW_ORG ORG ON JFOI.ORG_ID = ORG.ORG_ID"
                + "     WHERE JFOI.FIGURE_TYPE='PositionFigure' AND JFOI.FIGURE_ID = #{id}";
    }

    /**
     * 岗位搜索
     *
     * @param map
     * @return
     */
    public String searchPos(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String sql = "SELECT JFOI.FIGURE_ID AS FILE_ID,JFOI.FIGURE_TEXT AS FILE_NAME,ORG.ORG_NAME AS P_FILE_NAME,1 AS FILE_TYPE" +
                "           FROM JECN_FLOW_ORG_IMAGE JFOI" +
                "      INNER JOIN JECN_FLOW_ORG ORG ON JFOI.ORG_ID = ORG.ORG_ID" +
                "           WHERE JFOI.FIGURE_TYPE='PositionFigure' AND ORG.PROJECTID=" + projectId + " AND ORG.DEL_STATE=0";
        if (Utils.strIsNotBlank(map.get("name"))) {
            String name = map.get("name").toString();
            sql += "          AND  JFOI.FIGURE_TEXT like '%" + name + "%'";
        }
        String packSql = "select * from (" + sql + ") temp";
        return packSql;
    }


    /**
     * 人员选择 点击组织 树展开
     *
     * @param map
     * @return
     */
    public String findPersonOrgTree(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        Long pId = Long.valueOf(map.get("pId").toString());
        return "SELECT POSITION_TREE.* FROM"
                + " (SELECT DISTINCT JR.ORG_ID AS FILE_ID,JR.ORG_NAME AS FILE_NAME,PUB.ORG_NAME AS P_FILE_NAME"
                + "    ,0 AS FILE_TYPE"
                + "    ,CASE WHEN SUB.ORG_ID IS NULL AND POS.FIGURE_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JR.SORT_ID"
                + "    FROM JECN_FLOW_ORG JR" + "    LEFT JOIN JECN_FLOW_ORG PUB ON PUB.ORG_ID=JR.PER_ORG_ID"
                + "    LEFT JOIN JECN_FLOW_ORG SUB ON SUB.PER_ORG_ID=JR.ORG_ID AND SUB.DEL_STATE=0"
                + "    LEFT JOIN JECN_FLOW_ORG_IMAGE POS ON POS.ORG_ID = JR.ORG_ID AND POS.FIGURE_TYPE='PositionFigure'"
                + "    WHERE JR.PER_ORG_ID=" + pId + " AND JR.PROJECTID=" + projectId + " AND JR.DEL_STATE=0" + " UNION"
                + "  SELECT DISTINCT JFOI.FIGURE_ID AS FILE_ID,JFOI.FIGURE_TEXT AS FILE_NAME,ORG.ORG_NAME AS P_FILE_NAME"
                + "     ,1 AS FILE_TYPE,CASE WHEN JU.PEOPLE_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JFOI.SORT_ID"
                + "     FROM JECN_FLOW_ORG_IMAGE JFOI" + "     LEFT JOIN JECN_FLOW_ORG ORG ON JFOI.ORG_ID = ORG.ORG_ID"
                + "     LEFT JOIN JECN_USER_POSITION_RELATED JUPR ON JFOI.FIGURE_ID = JUPR.FIGURE_ID"
                + "     LEFT JOIN JECN_USER JU ON JUPR.PEOPLE_ID = JU.PEOPLE_ID AND JU.ISLOCK=0"
                + "     WHERE JFOI.FIGURE_TYPE='PositionFigure' AND JFOI.ORG_ID = " + pId + ") POSITION_TREE"
                + "     ORDER BY POSITION_TREE.FILE_TYPE,POSITION_TREE.SORT_ID";
    }

    /**
     * 人员选择
     *
     * @param map
     * @return
     */
    public String findPersonOrgTreeNode(Map<String, Object> map) {
        return "  SELECT DISTINCT JFOI.FIGURE_ID AS FILE_ID,JFOI.FIGURE_TEXT AS FILE_NAME,ORG.ORG_NAME AS P_FILE_NAME"
                + "     ,1 AS FILE_TYPE,CASE WHEN JU.PEOPLE_ID IS NULL THEN 0 ELSE 1 END AS FILE_COUNT,JFOI.SORT_ID"
                + "     FROM JECN_FLOW_ORG_IMAGE JFOI" + "     LEFT JOIN JECN_FLOW_ORG ORG ON JFOI.ORG_ID = ORG.ORG_ID"
                + "     INNER JOIN JECN_USER_POSITION_RELATED JUPR ON JFOI.FIGURE_ID = JUPR.FIGURE_ID"
                + "     INNER JOIN JECN_USER JU ON JUPR.PEOPLE_ID = JU.PEOPLE_ID AND JU.ISLOCK=0"
                + "     WHERE JFOI.FIGURE_TYPE='PositionFigure' AND JU.PEOPLE_ID = #{id} ";
    }

    /**
     * 人员选择 点击岗位树展开
     *
     * @param map
     * @return
     */
    public String findPersonPosTree(Map<String, Object> map) {
        Long pId = Long.valueOf(map.get("pId").toString());
        String accept = map.get("accept").toString();
        String subSql = "";
        if ("organization".equals(accept)) {
            subSql = "			SELECT " +
                    "				JR.ORG_ID AS FILE_ID," +
                    "				JR.ORG_NAME AS FILE_NAME," +
                    "				PUB.ORG_NAME AS P_FILE_NAME," +
                    "				0 AS FILE_TYPE," +
                    "				CASE" +
                    "			WHEN SUB.ORG_ID IS NULL" +
                    "			AND POS.FIGURE_ID IS NULL THEN" +
                    "				0" +
                    "			ELSE" +
                    "				1" +
                    "			END AS FILE_COUNT," +
                    "			JR.SORT_ID" +
                    "			FROM" +
                    "				JECN_FLOW_ORG JR" +
                    "			LEFT JOIN JECN_FLOW_ORG PUB ON PUB.ORG_ID = JR.PER_ORG_ID" +
                    "			LEFT JOIN JECN_FLOW_ORG SUB ON SUB.PER_ORG_ID = JR.ORG_ID" +
                    "			AND SUB.DEL_STATE = 0" +
                    "			LEFT JOIN JECN_FLOW_ORG_IMAGE POS ON POS.ORG_ID = JR.ORG_ID" +
                    "			AND POS.FIGURE_TYPE = 'PositionFigure'" +
                    "			WHERE" +
                    "				JR.PER_ORG_ID = #{pId}" +
                    "			AND JR.PROJECTID = #{projectId}" +
                    "			AND JR.DEL_STATE = 0" +
                    "			UNION" +
                    "				SELECT DISTINCT" +
                    "					JFOI.FIGURE_ID AS FILE_ID," +
                    "					JFOI.FIGURE_TEXT AS FILE_NAME," +
                    "					ORG.ORG_NAME AS P_FILE_NAME," +
                    "					1 AS FILE_TYPE," +
                    "					CASE" +
                    "				WHEN JU.PEOPLE_ID IS NULL THEN" +
                    "					0" +
                    "				ELSE" +
                    "					1" +
                    "				END AS FILE_COUNT," +
                    "				JFOI.SORT_ID" +
                    "				FROM" +
                    "					JECN_FLOW_ORG_IMAGE JFOI" +
                    "				LEFT JOIN JECN_FLOW_ORG ORG ON JFOI.ORG_ID = ORG.ORG_ID" +
                    "				LEFT JOIN JECN_USER_POSITION_RELATED JUPR ON JFOI.FIGURE_ID = JUPR.FIGURE_ID" +
                    "				LEFT JOIN JECN_USER JU ON JUPR.PEOPLE_ID = JU.PEOPLE_ID" +
                    "				AND JU.ISLOCK = 0" +
                    "				WHERE" +
                    "					JFOI.FIGURE_TYPE = 'PositionFigure'" +
                    "				AND JFOI.ORG_ID = #{pId}";
        } else if ("position".equals(accept)) {
            subSql = "SELECT" +
                    "						JU.PEOPLE_ID AS FILE_ID," +
                    "						JU.TRUE_NAME AS FILE_NAME," +
                    "						JFOI.FIGURE_TEXT AS P_FILE_NAME," +
                    "						2 AS FILE_TYPE," +
                    "						0 AS FILE_COUNT," +
                    "						JU.PEOPLE_ID AS SORT_ID" +
                    "					FROM" +
                    "						JECN_USER_POSITION_RELATED JUPR" +
                    "					LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JUPR.FIGURE_ID = JFOI.FIGURE_ID," +
                    "					JECN_USER JU" +
                    "				WHERE" +
                    "					JUPR.PEOPLE_ID = JU.PEOPLE_ID" +
                    "				AND JU.ISLOCK = 0" +
                    "				AND JUPR.FIGURE_ID = #{pId}";
        }
        String sql = "	SELECT" +
                "		POSITION_TREE.*" +
                "	FROM" +
                "		(" + subSql + " ) POSITION_TREE" +
                "	ORDER BY" +
                "		POSITION_TREE.FILE_TYPE,POSITION_TREE.SORT_ID ASC";
        return sql;
    }

    /**
     * 人员搜索
     *
     * @param map
     * @return
     */
    public String searchPeople(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT JU.PEOPLE_ID AS FILE_ID,");
        if (CommonSqlProvider.isOracle()) {
            sql.append(" JU.TRUE_NAME  || '('||JU.LOGIN_NAME||')'   AS FILE_NAME,");
        } else {
            sql.append(" JU.TRUE_NAME + '('+JU.LOGIN_NAME+')'   AS FILE_NAME,");
        }
        sql.append(" JFOI.FIGURE_TEXT AS P_FILE_NAME,2 AS FILE_TYPE,JU.LOGIN_NAME " +
                "              FROM JECN_USER JU" +
                "              LEFT JOIN JECN_USER_POSITION_RELATED JUPR ON JU.PEOPLE_ID = JUPR.PEOPLE_ID" +
                "      LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JUPR.FIGURE_ID=JFOI.FIGURE_ID" +
                "      WHERE JU.ISLOCK=0 ");
        if (Utils.strIsNotBlank(map.get("name"))) {
            if (ConfigUtils.isIflytekLogin()) {
                List<String> names = (List) map.get("name");
                sql.append("          AND  JU.LOGIN_NAME in " + SqlCommon.getStrs(names));
            } else {
                String name = map.get("name").toString();
                sql.append("          AND  JU.TRUE_NAME like '%" + name + "%'");
            }
        }
        String packSql = "select * from (" + sql.toString() + ") temp";
        return packSql;
    }

    public String getNodePath(Map<String, Object> map) {
        String type = map.get("type").toString();
        String id = map.get("id").toString();
        String sql = "select t_path from ";
        switch (type) {
            // 流程及流程架构
            case "process":
                sql += "JECN_FLOW_STRUCTURE WHERE FLOW_ID=" + id;
                break;
            // 制度
            case "rule":
                sql += "JECN_RULE WHERE ID=" + id;
                break;
            // 标准
            case "standard":
                sql += "JECN_CRITERION_CLASSES WHERE CRITERION_CLASS_ID=" + id;
                break;
            // 风险
            case "risk":
                sql += "JECN_RISK WHERE id=" + id;
                break;
            // 文件
            case "file":
                sql += "JECN_FILE WHERE FILE_ID=" + id;
                break;
            // 部门
            case "organization":
                sql += "JECN_FLOW_ORG WHERE ORG_ID=" + id;
                break;
            default:
                throw new IllegalArgumentException("该类型没有t_level,type:" + type);
        }
        return sql;

    }

    public String processFileAuthCountSql(Map<String, Object> map) {
        return "SELECT COUNT(*)" +
                "  FROM JECN_FLOW_FILE_POS_PERM JAP" +
                " INNER JOIN JECN_FLOW_ORG_IMAGE OI" +
                "    ON JAP.FIGURE_ID = OI.FIGURE_ID" +
                " INNER JOIN JECN_USER_POSITION_RELATED JUPR" +
                "    ON JUPR.FIGURE_ID = OI.FIGURE_ID" +
                "   AND JUPR.PEOPLE_ID = #{peopleId}" +
                " WHERE JAP.RELATE_ID = #{id}";
    }

    /**
     * 人员选择
     *
     * @param map
     * @return
     */
    public String findPersonNode(Map<String, Object> map) {
        return "  SELECT DISTINCT JU.PEOPLE_ID AS FILE_ID,JU.TRUE_NAME AS FILE_NAME"
                + "     ,2 AS FILE_TYPE,0 AS FILE_COUNT FROM JECN_USER JU "
                + "     WHERE JU.PEOPLE_ID = #{id} ";
    }


}
