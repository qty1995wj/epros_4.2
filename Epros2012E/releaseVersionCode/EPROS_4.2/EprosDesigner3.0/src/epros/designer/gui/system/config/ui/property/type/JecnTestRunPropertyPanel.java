package epros.designer.gui.system.config.ui.property.type;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;

import epros.designer.gui.system.config.ui.comp.JecnConfigSpinner;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 试运行显示panel
 * 
 * @author Administrator
 * 
 */
public class JecnTestRunPropertyPanel extends JecnPanel implements
		ChangeListener {
	/** 对话框 */
	private JecnAbtractBaseConfigDialog dialog = null;

	/** 试运行周期标签 */
	private JLabel testRunCycleLabel = null;
	/** 发送试运行报告周期标签 */
	private JLabel testRunReleaseCycleLabel = null;
	/** 试运行周期 */
	private JecnConfigSpinner testRunCycleSpinner = null;
	/** 发送试运行报告周期 */
	private JecnConfigSpinner sendTestRunRecordSpinner = null;

	/** 试运行数据对象 */
	private JecnConfigItemBean itemBean = null;

	public JecnTestRunPropertyPanel(JecnAbtractBaseConfigDialog dialog) {
		this.dialog = dialog;

		initComponents();
		initLayout();
	}

	private void initComponents() {

		// 试运行周期标签
		testRunCycleLabel = new JLabel(JecnProperties
				.getValue("task_testRunCycle"));
		// 发送试运行报告周期标签
		testRunReleaseCycleLabel = new JLabel(JecnProperties
				.getValue("task_testRunRealseCycle"));

		// 试运行周期
		testRunCycleSpinner = new JecnConfigSpinner(new SpinnerNumberModel(1,
				1, 1000000000, 1));
		// 发送试运行报告周期
		sendTestRunRecordSpinner = new JecnConfigSpinner(
				new SpinnerNumberModel(1, 1, 1000000000, 1));

		// 大小
		this.setPreferredSize(new Dimension(100, 75));
		// 设置边框
		this.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("task_TestRun")));
		// 背景
		this.setBackground(this.getBackground());
		// 默认不显示
		this.setVisible(false);

		// 提示信息
		testRunCycleSpinner.setToolTipText(JecnProperties
				.getValue("task_testRunCycleMax"));
		sendTestRunRecordSpinner.setToolTipText(JecnProperties
				.getValue("task_testRunCycleMax"));

		// 事件
		// 试运行周期,发送试运行报告周期都要大于0，且试运行周期必须大于发送试运行报告周期
		// 试运行周期
		testRunCycleSpinner.addChangeListener(this);
		// 发送试运行报告周期
		sendTestRunRecordSpinner.addChangeListener(this);
	}

	private void initLayout() {
		// 布局
		Insets insets = new Insets(0, 20, 0, 0);

		// 试运行周期标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(testRunCycleLabel, c);

		// 试运行周期
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(testRunCycleSpinner, c);

		// 发送试运行报告周期标签
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(testRunReleaseCycleLabel, c);

		// 发送试运行报告周期
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(sendTestRunRecordSpinner, c);
	}

	/**
	 * 
	 * 显示试运行
	 * 
	 * @param fileBean
	 */
	public void initData(JecnConfigItemBean itemBean) {
		this.itemBean = itemBean;

		if (itemBean == null
				|| JecnConfigContents.ITEM_IS_NOT_SHOW.equals(itemBean
						.getValue())) {// 试运行不显示
			// 默认不显示
			this.setVisible(false);
		} else if (JecnConfigContents.ITEM_IS_SHOW.equals(itemBean.getValue())) {// 试运行显示
			// 默认不显示
			this.setVisible(true);
			// 试运行周期
			this.testRunCycleSpinner.initConfigSpinnerData();
			// 发送试运行报告周期
			this.sendTestRunRecordSpinner.initConfigSpinnerData();
		}
	}

	/**
	 * 
	 * JSpinner的事件处理方法
	 * 
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() instanceof JecnConfigSpinner) {
			if (e.getSource() == testRunCycleSpinner) {// 试运行周期
				// 试运行周期,发送试运行报告周期,提示信息KEY
				spinnerValue(testRunCycleSpinner, sendTestRunRecordSpinner,
						"task_testRunCycleMax", true);
			} else if (e.getSource() == sendTestRunRecordSpinner) {// 发送试运行报告周期
				// 试运行周期,发送试运行报告周期,提示信息KEY
				spinnerValue(testRunCycleSpinner, sendTestRunRecordSpinner,
						"task_testRunCycleMax", false);
			}
		}
	}

	/**
	 * 
	 * 校验成功时复制到数据层
	 * 
	 * @param maxSpinner
	 *            JecnConfigSpinner 上限
	 * @param minSpinner
	 *            JecnConfigSpinner 下限
	 * @param infoKey
	 *            String 提示信息KEY
	 * @param isMax
	 *            boolean true：上限；false：下限
	 * 
	 */
	private void spinnerValue(JecnConfigSpinner maxSpinner,
			JecnConfigSpinner minSpinner, String infoKey, boolean isMax) {

		if (checkSpinner(maxSpinner, minSpinner, infoKey)) {// 校验成功：true；失败：false
			if (isMax) {
				maxSpinner.getItemBean().setValue(
						maxSpinner.getValue().toString());
			} else {
				minSpinner.getItemBean().setValue(
						minSpinner.getValue().toString());
			}
		}
	}

	/**
	 * 
	 * 校验：上下限制大于等于1且上限值大于等于下限值时失败
	 * 
	 * @param maxSpinner
	 *            JecnConfigSpinner 上限
	 * @param minSpinner
	 *            JecnConfigSpinner 下限
	 * @param infoKey
	 *            提示信息
	 * @return boolean 校验成功：true；失败：false
	 */
	private boolean checkSpinner(JecnConfigSpinner maxSpinner,
			JecnConfigSpinner minSpinner, String infoKey) {
		// 上限
		int maxvalue = Integer.parseInt(maxSpinner.getValue().toString());
		// 下限
		int minValue = Integer.parseInt(minSpinner.getValue().toString());

		if (maxvalue <= 0 || minValue <= 0 || maxvalue < minValue) {// 上下限小于等于0或上限值小于下限值
			if (getOkJButton().isEnabled()) {
				getOkJButton().setEnabled(false);
			}
			getInfoLable().setText(JecnProperties.getValue(infoKey));
			return false;
		}

		if (!getOkJButton().isEnabled()) {// 验证成功
			getOkJButton().setEnabled(true);
		}
		getInfoLable().setText("");
		return true;
	}

	/**
	 * 
	 * 校验数据
	 * 
	 * @return boolean 校验成功：true ；校验失败：false
	 * 
	 */
	public boolean check() {
		if (!this.isVisible()) {// 隐藏

			if (testRunCycleSpinner.getItemBean() == null
					|| sendTestRunRecordSpinner.getItemBean() == null) {
				return true;
			}

			// 上限
			int maxvalue = Integer.parseInt(testRunCycleSpinner.getItemBean()
					.getValue().toString());
			// 下限
			int minValue = Integer.parseInt(sendTestRunRecordSpinner
					.getItemBean().getValue().toString());
			if (maxvalue <= 0 || minValue <= 0 || maxvalue < minValue) {// 上下限小于等于0或上限值小于下限值
				return false;
			} else {
				return true;
			}
		}
		// 试运行周期,发送试运行报告周期,提示信息KEY
		// boolean 校验成功：true；失败：false
		return checkSpinner(testRunCycleSpinner, sendTestRunRecordSpinner,
				"task_testRunCycleMax");
	}

	public JecnConfigSpinner getTestRunCycleSpinner() {
		return testRunCycleSpinner;
	}

	public JecnConfigSpinner getSendTestRunRecordSpinner() {
		return sendTestRunRecordSpinner;
	}

	private JecnUserInfoTextArea getInfoLable() {
		return dialog.getOkCancelJButtonPanel().getInfoLabel();
	}

	private JButton getOkJButton() {
		return dialog.getOkCancelJButtonPanel().getOkJButton();
	}

}
