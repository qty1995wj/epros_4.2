
package com.jecn.webservice.zhongsh.i3.core;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eresult.core.workflow.i3 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EResult_QNAME = new QName("i3.Workflow.Core.EResult", "EResult");
    private final static QName _EResultDataValue_QNAME = new QName("i3.Workflow.Core.EResult", "DataValue");
    private final static QName _EResultValue_QNAME = new QName("i3.Workflow.Core.EResult", "value");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eresult.core.workflow.i3
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EResult }
     * 
     */
    public EResult createEResult() {
        return new EResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "i3.Workflow.Core.EResult", name = "EResult")
    public JAXBElement<EResult> createEResult(EResult value) {
        return new JAXBElement<EResult>(_EResult_QNAME, EResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "i3.Workflow.Core.EResult", name = "DataValue", scope = EResult.class)
    public JAXBElement<String> createEResultDataValue(String value) {
        return new JAXBElement<String>(_EResultDataValue_QNAME, String.class, EResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "i3.Workflow.Core.EResult", name = "value", scope = EResult.class)
    public JAXBElement<String> createEResultValue(String value) {
        return new JAXBElement<String>(_EResultValue_QNAME, String.class, EResult.class, value);
    }

}
