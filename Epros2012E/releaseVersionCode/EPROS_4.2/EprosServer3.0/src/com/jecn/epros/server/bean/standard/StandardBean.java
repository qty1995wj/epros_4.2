package com.jecn.epros.server.bean.standard;

import java.util.Date;
import java.util.List;

/**
 * @author yxw 2012-6-30
 * @description：标准bean
 */
@SuppressWarnings("serial")
public class StandardBean implements java.io.Serializable {

	// Fields

	private Long criterionClassId;// 主键
	private Long preCriterionClassId;// 父类主键
	private String criterionClassName;// 名称
	private Long projectId;// 工程Id
	private Long relationId;// 关联ID（stanType为1时关联文件 为2时关联标准）
	private Long sortId;// 排序
	private Date createDate;// 创建时间
	private Date updateDate;// 更新时间
	private Long createPeopleId;// 创建人ID
	private Long updatePeopleId;// 更新人ID
	private Long isPublic;// 密级(0是秘密，1公开)
	private Integer stanType;// 标准类型(0是目录，1文件标准，2流程标准 3流程地图标准4标准条款5条款要求)
	private String stanContent;// 条款内容
	private int isproFile;// 是否形成程序文件
	private String note;// 备注
	private String tPath;// tpath 节点层级关系
	private int tLevel;// level 节点级别，默认0开始
	private String viewSort;
	/** 保密级别 **/
	private Integer confidentialityLevel;

	// 条款相关附件
	List<StandardFileContent> fileContents;

	public Integer getConfidentialityLevel() {
		return confidentialityLevel;
	}

	public void setConfidentialityLevel(Integer confidentialityLevel) {
		this.confidentialityLevel = confidentialityLevel;
	}

	public String getViewSort() {
		return viewSort;
	}

	public void setViewSort(String viewSort) {
		this.viewSort = viewSort;
	}

	public String gettPath() {
		return tPath;
	}

	public void settPath(String tPath) {
		this.tPath = tPath;
	}

	public int gettLevel() {
		return tLevel;
	}

	public void settLevel(int tLevel) {
		this.tLevel = tLevel;
	}

	public Long getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Long isPublic) {
		this.isPublic = isPublic;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

	/** default constructor */
	public StandardBean() {
	}

	public Long getCriterionClassId() {
		return this.criterionClassId;
	}

	public void setCriterionClassId(Long criterionClassId) {
		this.criterionClassId = criterionClassId;
	}

	public Long getPreCriterionClassId() {
		return this.preCriterionClassId;
	}

	public void setPreCriterionClassId(Long preCriterionClassId) {
		this.preCriterionClassId = preCriterionClassId;
	}

	public String getCriterionClassName() {
		return this.criterionClassName;
	}

	public void setCriterionClassName(String criterionClassName) {
		this.criterionClassName = criterionClassName;
	}

	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getSortId() {
		return sortId;
	}

	public void setSortId(Long sortId) {
		this.sortId = sortId;
	}

	public Integer getStanType() {
		return stanType;
	}

	public void setStanType(Integer stanType) {
		this.stanType = stanType;
	}

	public String getStanContent() {
		return stanContent;
	}

	public void setStanContent(String stanContent) {
		this.stanContent = stanContent;
	}

	public int getIsproFile() {
		return isproFile;
	}

	public void setIsproFile(int isproFile) {
		this.isproFile = isproFile;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<StandardFileContent> getFileContents() {
		return fileContents;
	}

	public void setFileContents(List<StandardFileContent> fileContents) {
		this.fileContents = fileContents;
	}
}