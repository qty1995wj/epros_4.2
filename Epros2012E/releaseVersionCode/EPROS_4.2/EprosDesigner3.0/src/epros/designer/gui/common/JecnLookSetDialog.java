package epros.designer.gui.common;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.log.JecnPersonalPermRecord;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.system.JecnMessage;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.choose.DeptCompetecnTable;
import epros.designer.gui.choose.PosCompetenceTable;
import epros.designer.gui.choose.PosGroupCompetenceTable;
import epros.designer.gui.popedom.organization.OrgChooseDialog;
import epros.designer.gui.popedom.position.PositionChooseDialog;
import epros.designer.gui.popedom.positiongroup.choose.PosGropChooseDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnLoginProperties;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * @author yxw 2012-12-7
 * @description：设置查阅权限界面
 */
public class JecnLookSetDialog extends JecnDialog {
	Logger log = Logger.getLogger(JecnLookSetDialog.class);
	/** 主面板 */
	private JPanel mainPanel = new JecnPanel();
	/** 包括密级、岗位权限、组织权限 */
	private JecnPanel proPanel = new JecnPanel(); // 530,160

	/** 权限面板 */
	private JecnPanel permPanel = new JecnPanel();
	/** 密级面板 */
	private JecnPanel secretPanel = new JecnPanel();
	/** 密级 权限设置是否子节点面板 */
	private JPanel secretPopemPanel = new JecnPanel();
	/** 密级 设置当前节点及子节点 */
	private JRadioButton secretComChildNodeRadio = new JRadioButton(JecnProperties.getValue("nowNodeAndChildNode"));
	/** 密级只设置当前节点 */
	private JRadioButton secretOnlySelectNodeRadio = new JRadioButton(JecnProperties.getValue("nowNode"));
	/** 组织查阅权限 */
	private JecnPanel orgPanel = new JecnPanel();
	/** 组织 权限设置是否子节点面板 */
	private JPanel orgChoosePopemPanel = new JecnPanel();
	/** 组织 设置当前节点及子节点 */
	private JRadioButton orgComChildNodeRadio = new JRadioButton(JecnProperties.getValue("nowNodeAndChildNode"));
	/** 组织 只设置当前节点 */
	private JRadioButton orgOnlySelectNodeRadio = new JRadioButton(JecnProperties.getValue("nowNode"));

	/** 岗位查阅权限 */
	private JecnPanel posPanel = new JecnPanel();
	/** 岗位 权限设置是否子节点面板 */
	private JPanel posChoosePopemPanel = new JecnPanel();
	/** 岗位 设置当前节点及子节点 */
	private JRadioButton posComChildNodeRadio = new JRadioButton(JecnProperties.getValue("nowNodeAndChildNode"));;

	/** 岗位 只设置当前节点 */
	private JRadioButton posOnlySelectNodeRadio = new JRadioButton(JecnProperties.getValue("nowNode"));

	/** 岗位组查阅权限 */
	private JecnPanel posGroupPanel = new JecnPanel();
	/** 岗位组 权限设置是否子节点面板 */
	private JPanel posGroupChoosePopemPanel = new JecnPanel();
	/** 岗位组 设置当前节点及子节点 */
	private JRadioButton posGroupComChildNodeRadio = new JRadioButton(JecnProperties.getValue("nowNodeAndChildNode"));;

	/** 岗位组 只设置当前节点 */
	private JRadioButton posGroupOnlySelectNodeRadio = new JRadioButton(JecnProperties.getValue("nowNode"));

	/** 密级：Lab */
	private JLabel isPublicLab = new JLabel("");
	/** 密级ComboBox */
	private JComboBox isPublicCombox = new JComboBox(new String[] { JecnProperties.getValue("secret"),
			JecnProperties.getValue("gongKai") });
	/** 0是秘密,1是公开 */
	private Long isPublic = 1L;

	/** 部门权限Lab */
	private JLabel depCompetenceLab = new JLabel("");
	/** 部门权限滚动面板 */
	private JScrollPane depCompetenceScrollPane = new JScrollPane();
	private DeptCompetecnTable depCompetenceTable = null;
	/** 岗位权限Lab */
	private JLabel posCompetenceLab = new JLabel("");
	/** 岗位权限滚动面板 */
	private JScrollPane posCompetenceScrollPane = new JScrollPane();
	private PosCompetenceTable posCompetenceTable = null;

	/** 岗位组权限Lab */
	private JLabel posGroupCompetenceLab = new JLabel("");
	/** 岗位组权限滚动面板 */
	private JScrollPane posGroupCompetenceScrollPane = new JScrollPane();
	private PosGroupCompetenceTable posGroupCompetenceTable = null;

	/** 部门权限选择按钮 */
	private JButton depBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 岗位权限 选择按钮 */
	private JButton posBut = new JButton(JecnProperties.getValue("selectBtn"));
	/** 岗位组权限 选择按钮 */
	private JButton posGroupBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 主面板底部按钮面板，包括确定、取消按钮 */
	private JecnPanel botButtonPanel = new JecnPanel();

	private List<JecnTreeBean> orglist = null;
	private List<JecnTreeBean> poslist = null;
	private List<JecnTreeBean> posGrouplist = null;

	// private List<JecnTreeBean> orglistOverride = new
	// ArrayList<JecnTreeBean>();
	// private List<JecnTreeBean> poslistOverride = new
	// ArrayList<JecnTreeBean>();
	// private List<JecnTreeBean> posGrouplistOverride = new
	// ArrayList<JecnTreeBean>();

	// private List<JecnTreeBean> orglistAdd = new ArrayList<JecnTreeBean>();
	// private List<JecnTreeBean> poslistAdd = new ArrayList<JecnTreeBean>();
	// private List<JecnTreeBean> posGrouplistAdd = new
	// ArrayList<JecnTreeBean>();
	//
	// private List<JecnTreeBean> orglistDel = new ArrayList<JecnTreeBean>();
	// private List<JecnTreeBean> poslistDel = new ArrayList<JecnTreeBean>();
	// private List<JecnTreeBean> posGrouplistDel = new
	// ArrayList<JecnTreeBean>();

	// 确定
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	// 取消
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	private JecnTreeNode node;
	private int type;

	private JRadioButton permOverride = new JRadioButton(JecnProperties.getValue("cover"));
	private JRadioButton permAdd = new JRadioButton(JecnProperties.getValue("batchAddition"));
	private JRadioButton permDel = new JRadioButton(JecnProperties.getValue("batchDel"));

	/** 是否具有下载权限 **/
	private boolean isDownLoad = false;

	private JecnPersonalPermRecord record;

	public JecnLookSetDialog(LookPopedomBean lookPopedomBean, JecnTreeNode node, int type, boolean isDownLoad) {
		orglist = lookPopedomBean.getOrgList();
		poslist = lookPopedomBean.getPosList();
		posGrouplist = lookPopedomBean.getPosGroupList();
		this.isDownLoad = getIsDownLoadType(type, isDownLoad);
		try {
			record = ConnectionPool.getPersonAction().getPersionalPermRecord(node.getJecnTreeBean().getId(), type,
					JecnConstants.loginBean.getJecnUser().getPeopleId());
		} catch (Exception e) {
			log.error("", e);
		}
		// overrideToShow(false);

		this.isPublic = lookPopedomBean.getIsPublic() == null ? 0 : lookPopedomBean.getIsPublic();
		this.node = node;
		this.type = type;
		this.setSize(540, 540);
		this.setResizable(true);
		this.setModal(true);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);
		initCompotents();
		initLayout();
		if (this.record != null) {
			Integer handle = this.record.getHandle();
			Integer secret = this.record.getSecret();
			Integer org = this.record.getOrg();
			Integer pos = this.record.getPos();
			Integer posGroup = this.record.getPosGroup();
			if (handle != null) {
				if (handle == 1) {
					permAdd.setSelected(true);
				} else if (handle == 2) {
					permDel.setSelected(true);
				}
			}
			if (secret != null) {
				if (secret == 0) {
					secretOnlySelectNodeRadio.setSelected(true);
				}
			}
			if (org != null) {
				if (org == 0) {
					orgOnlySelectNodeRadio.setSelected(true);
				}
			}
			if (pos != null) {
				if (pos == 0) {
					posOnlySelectNodeRadio.setSelected(true);
				}
			}
			if (posGroup != null) {
				if (posGroup == 0) {
					posGroupOnlySelectNodeRadio.setSelected(true);
				}
			}
		}
	}

	private void initCompotents() {
		this.setTitle(JecnProperties.getValue("lookPermissions"));
		Dimension dimension = new Dimension(380, 145);
		depCompetenceScrollPane.setPreferredSize(dimension);
		depCompetenceScrollPane.setMinimumSize(dimension);
		posCompetenceScrollPane.setPreferredSize(dimension);
		posCompetenceScrollPane.setMinimumSize(dimension);
		posGroupCompetenceScrollPane.setPreferredSize(dimension);
		posGroupCompetenceScrollPane.setMinimumSize(dimension);
		depCompetenceTable = new DeptCompetecnTable(orglist, isDownLoad);
		if (isPublic == 1) {
			isPublicCombox.setSelectedIndex(1);
		}
		depCompetenceScrollPane.setViewportView(depCompetenceTable);
		depCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		posCompetenceTable = new PosCompetenceTable(poslist, isDownLoad);
		posCompetenceScrollPane.setViewportView(posCompetenceTable);
		posCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		posGroupCompetenceTable = new PosGroupCompetenceTable(posGrouplist, isDownLoad);
		posGroupCompetenceScrollPane.setViewportView(posGroupCompetenceTable);
		posGroupCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		/*
		 * 岗位查阅权限
		 */
		posPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("position")
				+ JecnProperties.getValue("lookPermissions")));
		/*
		 * 部门查阅权限
		 */
		orgPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("dept")
				+ JecnProperties.getValue("lookPermissions")));
		/*
		 * 岗位组查阅权限
		 */
		posGroupPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("positionGroup")
				+ JecnProperties.getValue("lookPermissions")));

		posPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("position")
				+ " " + JecnProperties.getValue("lookPermissions")));
		orgPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("dept")
				+ " " + JecnProperties.getValue("lookPermissions")));
		posGroupPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("positionGroup")
				+ " " + JecnProperties.getValue("lookPermissions")));
		secretPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("security")));

		ButtonGroup permGroup = new ButtonGroup();

		permAdd.setOpaque(false);
		permDel.setOpaque(false);
		permOverride.setOpaque(false);

		permGroup.add(permAdd);
		permGroup.add(permDel);
		permGroup.add(permOverride);
		permOverride.setSelected(true);

		depBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				OrgChooseDialog orgChooseDialog = new OrgChooseDialog(orglist, true);
				orgChooseDialog.setVisible(true);
				if (orgChooseDialog.isOperation()) {
					initPermOrgShow();
				}
			}
		});
		posBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PositionChooseDialog positionChooseDialog = new PositionChooseDialog(poslist, true);
				positionChooseDialog.setVisible(true);
				if (positionChooseDialog.isOperation()) {
					initPermPosShow();
				}
			}
		});
		posGroupBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PosGropChooseDialog posGroupChooseDialog = new PosGropChooseDialog(posGrouplist, true);
				posGroupChooseDialog.setVisible(true);
				if (posGroupChooseDialog.isOperation()) {
					initPermPosGroupShow();
				}
			}
		});

		isPublicCombox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					int i = isPublicCombox.getSelectedIndex();
					if (i == 0) {
						isPublic = 0L;// 秘密
					} else if (i == 1) {
						isPublic = 1L;// 公开
					}
					hiddenOrShowCompotent();
				}
			}
		});
		// 根据保密级别 判断是否置灰 权限组件
		hiddenOrShowCompotent();
		// 取消事件，关闭窗口
		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnLookSetDialog.this.dispose();
			}
		});
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButAction();
			}
		});

		permDel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				initData();
				initPermShow();

			}
		});

		permOverride.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				initData();
				initPermShow();

			}
		});

		permAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				initData();
				initPermShow();
			}

		});
	}

	private void initData() {
		// 默认为覆盖
		// if (permOverride.isSelected()) {
		// orglist = orglistOverride;
		// poslist = poslistOverride;
		// posGrouplist = posGrouplistOverride;
		// } else if (permAdd.isSelected()) {// 添加
		// orglist = orglistAdd;
		// poslist = poslistAdd;
		// posGrouplist = posGrouplistAdd;
		//
		// } else if (permDel.isSelected()) {// 删除
		// orglist = orglistDel;
		// poslist = poslistDel;
		// posGrouplist = posGrouplistDel;
		// }
	}

	/**
	 * 覆盖备份的list与显示的list的相互赋值
	 * 
	 * @param toShow
	 *            true显示的list添加备份的数据 false备份的数据添加显示的数据
	 */
	// private void overrideToShow(boolean toShow) {
	// if (toShow) {
	// orglist.addAll(orglistOverride);
	// poslist.addAll(poslistOverride);
	// posGrouplist.addAll(posGrouplistOverride);
	// } else {
	// orglistOverride.addAll(orglist);
	// poslistOverride.addAll(poslist);
	// posGrouplistOverride.addAll(posGrouplist);
	// }
	// }

	private void initPermPosGroupShow() {
		if (posGrouplist != null && posGrouplist.size() > 0) {
			posGroupCompetenceTable.remoeAll();
			for (int i = 0; i < posGrouplist.size(); i++) {
				JecnTreeBean jecnTreeBean = posGrouplist.get(i);
				Vector vec = new Vector();
				vec.add(0, jecnTreeBean.getId());
				vec.add(1, jecnTreeBean.getName());
				vec.add(2, new JCheckBox(JecnProperties.getValue("downLoadAccess"), jecnTreeBean.isDownLoad()));
				((DefaultTableModel) posGroupCompetenceTable.getModel()).addRow(vec);
			}
		} else {
			posGroupCompetenceTable.remoeAll();
		}
	}

	private void initPermPosShow() {
		if (poslist != null && poslist.size() > 0) {
			posCompetenceTable.remoeAll();
			for (int i = 0; i < poslist.size(); i++) {
				JecnTreeBean jecnTreeBean = poslist.get(i);
				Vector vec = new Vector();
				vec.add(jecnTreeBean.getId());
				vec.add(jecnTreeBean.getName());
				vec.add(2, new JCheckBox(JecnProperties.getValue("downLoadAccess"), jecnTreeBean.isDownLoad()));
				((DefaultTableModel) posCompetenceTable.getModel()).addRow(vec);
			}
		} else {
			posCompetenceTable.remoeAll();
		}
	}

	private void initPermOrgShow() {
		// 判断是否选择了部门
		if (orglist != null && orglist.size() > 0) {
			depCompetenceTable.remoeAll();
			for (int i = 0; i < orglist.size(); i++) {
				JecnTreeBean jecnTreeBean = orglist.get(i);
				Vector vec = new Vector();
				vec.add(orglist.get(i).getId());
				vec.add(orglist.get(i).getName());
				vec.add(2, new JCheckBox(JecnProperties.getValue("downLoadAccess"), jecnTreeBean.isDownLoad()));
				((DefaultTableModel) depCompetenceTable.getModel()).addRow(vec);
			}
		} else {
			depCompetenceTable.remoeAll();
		}
	}

	private void initPermShow() {
		initPermOrgShow();
		initPermPosShow();
		initPermPosGroupShow();
	}

	private void initLayout() {
		Insets insets = new Insets(2, 1, 2, 1);
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.EAST,
				GridBagConstraints.BOTH, insets, 0, 0);
		//
		mainPanel.add(proPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);

		mainPanel.add(botButtonPanel, c);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		proPanel.add(permPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		proPanel.add(secretPanel, c);
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0,
				0);
		proPanel.add(orgPanel, c);
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0,
				0);
		proPanel.add(posPanel, c);
		c = new GridBagConstraints(0, 4, 1, 1, 1.0, 1.0, GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0,
				0);
		proPanel.add(posGroupPanel, c);

		// 默认权限radio
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
				2, 6, 2, 1), 0, 0);
		permPanel.add(permOverride, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
				2, 6, 2, 1), 0, 0);
		permPanel.add(permAdd, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
				2, 6, 2, 1), 0, 0);
		permPanel.add(permDel, c);

		// 密级lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(
				2, 6, 2, 1), 0, 0);

		secretPanel.add(isPublicLab, c);
		// 密级combox
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		secretPanel.add(isPublicCombox, c);

		c = new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		secretPanel.add(secretPopemPanel, c);

		// 部门权限lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0,
				0);
		orgPanel.add(depCompetenceLab, c);
		// 部门权限area
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		orgPanel.add(depCompetenceScrollPane, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		orgPanel.add(depBut, c);
		c = new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		orgPanel.add(orgChoosePopemPanel, c);

		// 岗位权限lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0,
				0);
		posPanel.add(posCompetenceLab, c);
		// 岗位权限area
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		posPanel.add(posCompetenceScrollPane, c);
		// 岗位权限 选择button
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		posPanel.add(posBut, c);
		c = new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		posPanel.add(posChoosePopemPanel, c);

		// 岗位组权限lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0,
				0);
		posGroupPanel.add(posGroupCompetenceLab, c);
		// 岗位组权限area
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		posGroupPanel.add(posGroupCompetenceScrollPane, c);
		// 岗位组权限 选择button
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		posGroupPanel.add(posGroupBut, c);
		c = new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		posGroupPanel.add(posGroupChoosePopemPanel, c);

		// 如果是叶子节点就不显示权限类型选择
		if (node.getJecnTreeBean().isChildNode()) {
			secretComChildNodeRadio.setOpaque(false);
			secretComChildNodeRadio.setSelected(true);
			secretOnlySelectNodeRadio.setOpaque(false);
			orgComChildNodeRadio.setOpaque(false);
			orgComChildNodeRadio.setSelected(true);
			orgOnlySelectNodeRadio.setOpaque(false);
			posComChildNodeRadio.setOpaque(false);
			posComChildNodeRadio.setSelected(true);
			posOnlySelectNodeRadio.setOpaque(false);

			posGroupComChildNodeRadio.setOpaque(false);
			posGroupComChildNodeRadio.setSelected(true);
			posGroupOnlySelectNodeRadio.setOpaque(false);

			ButtonGroup bg1 = new ButtonGroup();
			bg1.add(orgComChildNodeRadio);
			bg1.add(orgOnlySelectNodeRadio);
			ButtonGroup bg2 = new ButtonGroup();
			bg2.add(posComChildNodeRadio);
			bg2.add(posOnlySelectNodeRadio);
			ButtonGroup bg3 = new ButtonGroup();
			bg3.add(secretComChildNodeRadio);
			bg3.add(secretOnlySelectNodeRadio);
			ButtonGroup bg4 = new ButtonGroup();
			bg4.add(posGroupComChildNodeRadio);
			bg4.add(posGroupOnlySelectNodeRadio);

			secretPopemPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			secretPopemPanel.add(secretComChildNodeRadio);
			secretPopemPanel.add(secretOnlySelectNodeRadio);

			orgChoosePopemPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			orgChoosePopemPanel.add(orgComChildNodeRadio);
			orgChoosePopemPanel.add(orgOnlySelectNodeRadio);

			posChoosePopemPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			posChoosePopemPanel.add(posComChildNodeRadio);
			posChoosePopemPanel.add(posOnlySelectNodeRadio);

			posGroupChoosePopemPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			posGroupChoosePopemPanel.add(posGroupComChildNodeRadio);
			posGroupChoosePopemPanel.add(posGroupOnlySelectNodeRadio);
		}

		botButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		botButtonPanel.add(okBut);
		botButtonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);

		JecnUtil.setMenuDisableThenInTaskExceptAuthorAdmin(node, okBut);
	}

	private void okButAction() {

		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isContinue"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}

		AccessId accId = new AccessId();// 权限集合
		// 密级 设置当前节点及子节点
		int securityType = secretComChildNodeRadio.isSelected() ? 1 : 0;
		int orgType = orgComChildNodeRadio.isSelected() ? 1 : 0;
		int posType = posComChildNodeRadio.isSelected() ? 1 : 0;
		int posGroupType = posGroupComChildNodeRadio.isSelected() ? 1 : 0;

		if (orglist != null && orglist.size() > 0) {
			List<AccessId> orgIds = new ArrayList<AccessId>();
			for (JecnTreeBean b : orglist) {
				AccessId orgId = new AccessId();// 权限集合
				orgId.setAccessId(b.getId());
				if (isDownLoad) {
					orgId.setDownLoad(depCompetenceTable.getIsDownLoadByTableId(b.getId()));
				}
				orgIds.add(orgId);
			}
			accId.setOrgAccessId(orgIds);
		}

		if (poslist != null && poslist.size() > 0) {
			List<AccessId> posIds = new ArrayList<AccessId>();
			for (JecnTreeBean b : poslist) {
				AccessId posId = new AccessId();// 权限集合
				posId.setAccessId(b.getId());
				if (isDownLoad) {
					posId.setDownLoad(posCompetenceTable.getIsDownLoadByTableId(b.getId()));
				}
				posIds.add(posId);
			}
			accId.setPosAccessId(posIds);
		}
		if (posGrouplist != null && posGrouplist.size() > 0) {
			List<AccessId> posGroupIds = new ArrayList<AccessId>();
			for (JecnTreeBean b : posGrouplist) {
				AccessId posGroupId = new AccessId();// 权限集合
				posGroupId.setAccessId(b.getId());
				if (isDownLoad) {
					posGroupId.setDownLoad(posGroupCompetenceTable.getIsDownLoadByTableId(b.getId()));
				}
				posGroupIds.add(posGroupId);
			}
			accId.setPosGroupAccessId(posGroupIds);
		}

		// 默认为覆盖
		int saveMode = 0;
		if (permAdd.isSelected()) {// 添加
			saveMode = 1;
		} else if (permDel.isSelected()) {// 删除
			saveMode = 2;
		}
		try {
			ConnectionPool.getOrganizationAction().saveOrUpdateAccessPermissions(node.getJecnTreeBean().getId(), type,
					node.getJecnTreeBean().getTreeNodeType(), isPublic, securityType, orgType, posType, 0,
					JecnConstants.projectId, posGroupType, saveMode, accId,
					JecnConstants.loginBean.getJecnUser().getPeopleId());
			this.dispose();
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("updateAccess"));
			log.error(JecnProperties.getValue("updateAccess"), e);
		}
	}

	/**
	 * 显示 置灰 权限查阅框体
	 */
	public void hiddenOrShowCompotent() {
		if (JecnConfigTool.pubDisableAuthSelect(isPublicCombox.getSelectedIndex())) {
			/** 部门权限选择按钮 */
			this.depBut.setEnabled(false);
			/** 岗位权限 选择按钮 */
			this.posBut.setEnabled(false);
			/** 岗位组权限 选择按钮 */
			this.posGroupBut.setEnabled(false);
			this.permOverride.setEnabled(false);
			this.permAdd.setEnabled(false);
			this.permDel.setEnabled(false);

		} else {
			/** 部门权限选择按钮 */
			this.depBut.setEnabled(true);
			/** 岗位权限 选择按钮 */
			this.posBut.setEnabled(true);
			/** 岗位组权限 选择按钮 */
			this.posGroupBut.setEnabled(true);
			this.permOverride.setEnabled(true);
			this.permAdd.setEnabled(true);
			this.permDel.setEnabled(true);
		}
	}

	private boolean getIsDownLoadType(int type, boolean isDownLoad) {
		if (type == 0) { // 流程
			return JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessDownLoad);
		} else if (type == 1) {
			return JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowFileDownLoad);
		} else if (type == 3) {
			return JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowRuleDownLoad);
		}
		return isDownLoad;
	}
}
