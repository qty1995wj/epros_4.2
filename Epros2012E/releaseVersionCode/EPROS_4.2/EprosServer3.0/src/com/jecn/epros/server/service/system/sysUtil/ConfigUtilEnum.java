package com.jecn.epros.server.service.system.sysUtil;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.common.JecnCommon;

public enum ConfigUtilEnum {
	INSTANCE;
	// 获取类名
	private String className = getStrName();
	private final Log log = LogFactory.getLog(ConfigUtilEnum.class);

	public String getKeyExpiryTime() {
		try {
			String value = invokeMethod(className, getStrKey()).toString();
			String dateStr = value.substring(0, 8);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			try {
				// 时间验证 获取秘密到期时间
				Date date = sdf.parse(dateStr);
				return JecnCommon.getStringbyDate(date);
			} catch (ParseException e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return null;
	}

	public boolean isTest() {
		try {
			String isTest = invokeMethod(className, getStrIsTest()).toString();
			return "true".equals(isTest);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return false;
	}

	public int getDesignerCount() {
		int adminCount = 0;
		try {
			adminCount = Integer.valueOf(invokeMethod(className, getDesignerUserInt()).toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return adminCount;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return adminCount;
		}
		return adminCount;
	}

	public int getAdminCount() {
		int designerCount = 0;
		try {
			designerCount = Integer.valueOf(invokeMethod(className, getSecondCountStrKey()).toString());
		} catch (NumberFormatException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return designerCount;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return designerCount;
		}
		return designerCount;
	}

	/**
	 * 类全名称获取 com.jecn.epros.server.util
	 * 
	 * @return
	 */
	private String getStrName() {
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("com.j");
		strBuff.append("ecn.e");
		strBuff.append("pro");
		strBuff.append("s.se");
		strBuff.append("rver.u");
		strBuff.append("til.");
		strBuff.append("Jec");
		strBuff.append("nKey");
		return strBuff.toString();
	}

	private Object invokeMethod(String name, String strMethod) throws Exception {
		// 获取class
		Class<?> cl = Class.forName(name);
		// 创建对象
		Object obj = cl.newInstance();
		// 获取方法
		Method method = cl.getMethod(strMethod);
		return method.invoke(obj);//
	}

	private String getStrIsTest() {
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("is");
		strBuff.append("Te");
		strBuff.append("st");
		return strBuff.toString();
	}

	/**
	 * 
	 * 类方法名称获取
	 * 
	 * @return
	 */
	private String getStrKey() {
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("ge");
		strBuff.append("tVa");
		strBuff.append("lue");
		return strBuff.toString();
	}

	private String getSecondCountStrKey() {
		// getSecondAdminUserInt
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("ge");
		strBuff.append("tSeco");
		strBuff.append("ndAdm");
		strBuff.append("inUse");
		strBuff.append("rInt");
		return strBuff.toString();
	}

	private String getDesignerUserInt() {
		// getSecondAdminUserInt
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("ge");
		strBuff.append("tDesi");
		strBuff.append("gnerU");
		strBuff.append("serI");
		strBuff.append("nt");
		return strBuff.toString();
	}

}
