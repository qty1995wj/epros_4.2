package com.jecn.epros.mapper;

import com.jecn.epros.domain.ChineseEnglisListBean;
import com.jecn.epros.domain.common.BaseBean;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.process.relatedFile.*;
import com.jecn.epros.domain.rule.*;
import com.jecn.epros.sqlprovider.RuleSqlProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface RuleMapper {

    /**
     * 系统管理员、浏览管理员、制度节点全局公开 树展开
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findChildRulesAdminSql")
    @ResultMap("mapper.Rule.ruleTreeBeans")
    public List<TreeNode> findChildFlowsAdmin(Map<String, Object> map);

    /**
     * 获得制度相关架构
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findChildFlowMaps")
    @ResultMap("mapper.Process.relatedFlowMap")
    public List<RelatedFlowMap> findRelatedFlowMap(Map<String, Object> map);

    /**
     * 获得制度相关流程
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findChildFlows")
    @ResultMap("mapper.Process.relatedFlow")
    public List<RelatedFlow> findRelatedFlow(Map<String, Object> map);

    /**
     * 获得制度相关标准
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedStandard")
    @ResultMap("mapper.Process.relatedStandard")
    public List<RelatedStandard> findRelatedStandard(Map<String, Object> map);

    /**
     * 获得制度相关标准文控
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedStandardHistory")
    @ResultMap("mapper.Process.relatedStandard")
    public List<RelatedStandard> findRelatedStandardHistory(Map<String, Object> map);


    /**
     * 获得制度相关风险
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedRisk")
    @ResultMap("mapper.Process.relatedRisk")
    public List<RelatedRisk> findRelatedRisk(Map<String, Object> map);

    /**
     * 相关制度
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedRuleSql")
    @ResultMap("mapper.Process.relatedRule")
    public List<RelatedRule> findRelatedRule(Map<String, Object> map);

    /**
     * 相关制度
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedRuleHSql")
    @ResultMap("mapper.Process.relatedRule")
    public List<RelatedRule> findRelatedRuleH(Map<String, Object> map);


    /**
     * 获得制度相关风险文控
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedRiskHistory")
    @ResultMap("mapper.Process.relatedRisk")
    public List<RelatedRisk> findRelatedRiskHistory(Map<String, Object> map);

    /**
     * 制度搜索
     *
     * @param paramMap
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "searchRule")
    @ResultMap("mapper.Rule.searchRuleResultBean")
    public List<SearchRuleResultBean> searchRule(Map<String, Object> map);

    /**
     * 制度搜索 总数
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "searchRuleTotal")
    @ResultType(Integer.class)
    public int searchRuleTotal(Map<String, Object> paramMap);

    @Select("select count(1)"
            + "    from jecn_rule t,Jecn_Flow_Org_Image jfoi,jecn_flow_org jfo,JECN_USER_POSITION_RELATED jupr"
            + "    where jupr.people_id=#{loginUserId} and t.id=#{relatedId} and jupr.figure_id=jfoi.figure_id and jfoi.org_id=jfo.org_id and jfo.del_state=0 and"
            + "    t.type_res_people=1 and jfoi.figure_id = t.res_people_id")
    public int peoplePositionIsRuleResPeople(Map<String, Object> paramMap);

    @Select("select * from jecn_rule_t where id=#{ruleId}")
    @ResultMap("mapper.Rule.ruleTWithTable")
    public RuleT getRuleT(Long ruleId);

    @Select("select rt.ID," +
            "       rt. PROJECT_ID," +
            "       rt. PER_ID," +
            "       rt. RULE_NUMBER," +
            "       rt. RULE_NAME," +
            "       rt. IS_PUBLIC," +
            "       rt. TYPE_ID," +
            "       rt. IS_DIR," +
            "       rt. ORG_ID," +
            "       rt. SORT_ID," +
            "       case when jf.version_id is not null " +
            "then  jf.version_id else rt.file_id end FILE_ID," +
            "       rt. TEMPLATE_ID," +
            "       rt. TEST_RUN_NUMBER," +
            "       rt. CREATE_DATE," +
            "       rt. CREATE_PEOPLE_ID," +
            "       rt. UPDATE_DATE," +
            "       rt. UPDATE_PEOPLE_ID," +
            "       rt. HISTORY_ID," +
            "       rt. RES_PEOPLE_ID," +
            "       rt. TYPE_RES_PEOPLE," +
            "       rt. IS_FILE_LOCAL," +
            "       rt. T_PATH," +
            "       rt. T_LEVEL," +
            "       rt. PUB_TIME," +
            "       rt. CONFIDENTIALITY_LEVEL," +
            "       rt. VIEW_SORT," +
            "       rt. EXPIRY," +
            "       rt. WARD_PEOPLE_ID," +
            "       rt. DEL_STATE," +
            "       rt. COMMISSIONER_ID," +
            "       rt. RULE_URL," +
            "       rt. KEYWORD, " +
            "       rt.EFFECTIVE_DATE," +
            "       rt.FICTION_PEOPLE_ID" +
            "  from jecn_rule_t rt" +
            "       left join jecn_file_t jf on jf.file_id = rt.file_id  AND rt.is_file_local = 0 where id=#{ruleId}")
    @ResultMap("mapper.Rule.ruleWithTable")
    public Rule getRuleByTRuleId(Long ruleId);

    @Select("select rt.ID," +
            "       rt. PROJECT_ID," +
            "       rt. PER_ID," +
            "       rt. RULE_NUMBER," +
            "       rt. RULE_NAME," +
            "       rt. IS_PUBLIC," +
            "       rt. TYPE_ID," +
            "       rt. IS_DIR," +
            "       rt. ORG_ID," +
            "       rt. SORT_ID," +
            "       case when jf.version_id is not null " +
            "then  jf.version_id else rt.file_id end FILE_ID," +
            "       rt. TEMPLATE_ID," +
            "       rt. TEST_RUN_NUMBER," +
            "       rt. CREATE_DATE," +
            "       rt. CREATE_PEOPLE_ID," +
            "       rt. UPDATE_DATE," +
            "       rt. UPDATE_PEOPLE_ID," +
            "       rt. HISTORY_ID," +
            "       rt. RES_PEOPLE_ID," +
            "       rt. TYPE_RES_PEOPLE," +
            "       rt. IS_FILE_LOCAL," +
            "       rt. T_PATH," +
            "       rt. T_LEVEL," +
            "       rt. PUB_TIME," +
            "       rt. CONFIDENTIALITY_LEVEL," +
            "       rt. VIEW_SORT," +
            "       rt. EXPIRY," +
            "       rt. WARD_PEOPLE_ID," +
            "       rt. DEL_STATE, " +
            "       rt. RULE_URL," +
            "       rt. COMMISSIONER_ID," +
            "       rt. KEYWORD," +
            "       rt.EFFECTIVE_DATE, " +
            "       rt.FICTION_PEOPLE_ID" +
            "  from jecn_rule rt" +
            "       left join jecn_file jf on jf.file_id = rt.file_id AND RT.is_file_local = 0 where id=#{ruleId}")
    @ResultMap("mapper.Rule.ruleWithTable")
    public Rule getRule(Long ruleId);

    @Select("select * from JECN_RULE_HISTORY where HISTORY_ID=#{historyId} ")
    @ResultMap("mapper.Rule.ruleWithTable")
    public Rule getRuleHistory(Long historyId);

    @Select("select count(id) from JECN_RULE_HISTORY where HISTORY_ID=#{historyId} AND ID = #{id}")
    public int getRuleHistoryCount(Map<String, Object> map);

    @Select("select * from jecn_rule_t where id in ${_parameter}")
    @ResultMap("mapper.Rule.ruleTWithTable")
    public List<RuleT> fetchRuleTByIds(String ids);

    @Update("update JECN_RULE_T set HISTORY_ID=#{historyId},UPDATE_DATE=#{updateDate},IS_PUBLIC=#{isPublic} where ID=#{id}")
    public void updateRuleT(RuleT ruleT);

    @Delete("delete from jecn_access_permissions where type=3 and RELATE_ID in ${ids}")
    public void deleteAccessPermissions(Map<String, Object> paramMap);

    @Delete("delete from JECN_ORG_ACCESS_PERMISSIONS where type=3 and RELATE_ID  in ${ids}")
    public void deleteOrgAccessPermissions(Map<String, Object> paramMap);

    @Delete("delete from JECN_GROUP_PERMISSIONS where type=3 and RELATE_ID  in ${ids}")
    public void deleteGroupPermissions(Map<String, Object> paramMap);

    @Delete("DELETE FROM Jecn_Rule_Risk WHERE RULE_ID  in ${ids}")
    public void deleteRuleRisk(Map<String, Object> paramMap);

    @Delete("DELETE FROM JECN_RULE_STANDARD WHERE RULE_ID  in ${ids}")
    public void deleteRuleStandard(Map<String, Object> paramMap);

    @Delete("delete from JECN_RULE where id  in ${ids}")
    public void deleteRule(Map<String, Object> paramMap);

    @Insert("insert into jecn_access_permissions(ID, FIGURE_ID, RELATE_ID, TYPE) select ID, FIGURE_ID, RELATE_ID, TYPE from jecn_access_permissions_t  where type=3 and RELATE_ID in ${ids}")
    public void insertAccessPermissions(Map<String, Object> paramMap);

    @Insert("insert into JECN_ORG_ACCESS_PERMISSIONS(ID, ORG_ID, RELATE_ID, TYPE) select ID, ORG_ID, RELATE_ID, TYPE from JECN_ORG_ACCESS_PERM_T where type=3 and RELATE_ID in ${ids}")
    public void insertOrgAccessPermissions(Map<String, Object> paramMap);

    @Insert("insert into JECN_GROUP_PERMISSIONS(ID, POSTGROUP_ID, RELATE_ID, TYPE) select ID, POSTGROUP_ID, RELATE_ID, TYPE from JECN_GROUP_PERMISSIONS_T where type=3 and RELATE_ID in ${ids}")
    public void insertGroupPermissions(Map<String, Object> paramMap);

    @Insert("INSERT INTO JECN_RULE_RISK(ID,RULE_ID,RISK_ID) SELECT ID,RULE_ID,RISK_ID FROM JECN_RULE_RISK_T WHERE RULE_ID in ${ids}")
    public void insertRuleRisk(Map<String, Object> paramMap);

    @Insert("insert into JECN_RULE (ID, PROJECT_ID, PER_ID, RULE_NUMBER, RULE_NAME, IS_PUBLIC, TYPE_ID, IS_DIR, ORG_ID, SORT_ID, FILE_ID, TEMPLATE_ID, TEST_RUN_NUMBER, CREATE_DATE, CREATE_PEOPLE_ID, UPDATE_DATE, UPDATE_PEOPLE_ID, HISTORY_ID,RES_PEOPLE_ID,TYPE_RES_PEOPLE,IS_FILE_LOCAL,T_PATH,T_LEVEL) "
            + "  select ID, PROJECT_ID, PER_ID, RULE_NUMBER, RULE_NAME, IS_PUBLIC, TYPE_ID, IS_DIR, ORG_ID, SORT_ID, FILE_ID, TEMPLATE_ID, TEST_RUN_NUMBER, CREATE_DATE, CREATE_PEOPLE_ID, UPDATE_DATE, UPDATE_PEOPLE_ID, HISTORY_ID,RES_PEOPLE_ID,TYPE_RES_PEOPLE,IS_FILE_LOCAL,T_PATH,T_LEVEL from JECN_RULE_T  where id in ${ids}")
    public void insertRule(Map<String, Object> paramMap);

    @Insert("INSERT INTO JECN_RULE_STANDARD (ID,RULE_ID,STANDARD_ID,RELA_TYPE,CLAUSE_ID) SELECT ID,RULE_ID,STANDARD_ID,RELA_TYPE,CLAUSE_ID FROM JECN_RULE_STANDARD_T WHERE RULE_ID in ${ids}")
    public void insertRuleStandard(Map<String, Object> paramMap);

    @Select("SELECT RT.ID, FC.FILE_NAME, FC.UPDATE_PEOPLE_ID, FC.FILE_STREAM"
            + "  FROM G020_RULE_FILE_CONTENT FC" + " INNER JOIN JECN_RULE_T RT" + "    ON RT.FILE_ID = FC.ID"
            + "   AND RT.ID = FC.RULE_ID" + "   AND RT.IS_FILE_LOCAL = 1" + " WHERE RT.ID IN ${ids}")
    public List<Map<String, Object>> fetchRuleTRelatedContentByIds(Map<String, Object> paramMap);

    @Select("select * from jecn_rule where id in ${ids}")
    @ResultMap("mapper.Rule.ruleWithTable")
    public List<Rule> fetchRuleByIds(Map<String, Object> paramMap);

    @Update("update JECN_RULE set HISTORY_ID=#{historyId},UPDATE_DATE=#{updateDate} where ID=#{id}")
    public void updateRule(Rule rule);

    @Delete("delete from RULE_FILE where RULE_TITLE_ID in (select rt.id from rule_title rt where rule_id in ${ids})")
    public void deleteRuleFile(Map<String, Object> paramMap);

    @Delete("delete from rule_content where RULE_TITLE_ID in (select rt.id from rule_title rt where rule_id in ${ids})")
    public void deleteRuleContent(Map<String, Object> paramMap);

    @Delete("delete from rule_title where rule_id in ${ids}")
    public void deleteRuleTitle(Map<String, Object> paramMap);

    @Insert("insert into rule_title(ID, RULE_ID, TITLE_NAME, ORDER_NUMBER, TYPE) "
            + " select ID,RULE_ID,TITLE_NAME,ORDER_NUMBER,TYPE from rule_title_t  where rule_id in ${ids}")
    public void insertRuleTitle(Map<String, Object> paramMap);

    @Insert("insert into rule_content(ID,RULE_TITLE_ID,CONTENT) "
            + "       select rft.ID,rft.RULE_TITLE_ID,rft.CONTENT from rule_content_t rft,RULE_TITLE_T rtt where rft.rule_title_id=rtt.id"
            + "       and rtt.rule_id in ${ids}")
    public void insertRuleContent(Map<String, Object> paramMap);

    @Insert("insert into RULE_FILE(ID, RULE_TITLE_ID, RULE_FILE_ID)"
            + "       select rft.ID,rft.RULE_TITLE_ID,rft.RULE_FILE_ID from RULE_FILE_T rft,RULE_TITLE_T rtt where rft.rule_title_id=rtt.id"
            + "       and rtt.rule_id in ${ids}")
    public void insertRuleFile(Map<String, Object> paramMap);

    @SelectKey(before = true, keyProperty = "id", resultType = Long.class, statement = {
            "select RULE_CONTENT_SQUENCE.nextval from dual"})
    @InsertProvider(type = RuleSqlProvider.class, method = "saveRuleFileContent")
    public void saveRuleFileContentOracle(G020RuleFileContent curContent);

    @SelectKey(before = false, keyProperty = "id", resultType = Long.class, statement = {"select @@identity"})
    @InsertProvider(type = RuleSqlProvider.class, method = "saveRuleFileContent")
    public void saveRuleFileContentSqlServer(G020RuleFileContent curContent);

    @SelectProvider(type = RuleSqlProvider.class, method = "fetchInventory")
    @ResultMap("mapper.Rule.fetchInventory")
    public List<RuleInfoBean> fetchInventory(Map<String, Object> paramMap);

    /**
     * 根据制度获取制度模板
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleTitleByRuleIdSql")
    public List<Map<String, Object>> findRuleTitleByRuleId(Map<String, Object> map);

    /**
     * 根据文控id获取制度模板
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleTitleHistoryByRuleIdSql")
    public List<Map<String, Object>> findRuleTitleHistoryByRuleId(Map<String, Object> map);

    /**
     * 制度操作说明- 制度文件
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleFileByRuleIdSql")
    public List<Map<String, Object>> findRuleFileByRuleId(Map<String, Object> map);

    /**
     * 制度操作说明- 制度文件文控信息
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleFileHistoryByRuleIdSql")
    public List<Map<String, Object>> findRuleFileHistoryByRuleId(Map<String, Object> map);

    /**
     * 获取制度模板，title对应的内容
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleContentByRuleIdSql")
    public List<Map<String, Object>> findRuleContentByRuleId(Map<String, Object> map);

    /**
     * 获取制度模板文控，title对应的内容
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleContentHistoryByRuleIdSql")
    public List<Map<String, Object>> findRuleContentHistoryByRuleId(Map<String, Object> map);

    /**
     * 获取制度相关流程、相关架构
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleRelatedFlowByIdSql")
    public List<Map<String, Object>> findRuleRelatedFlowById(Map<String, Object> map);

    /**
     * 制度操作说明相关标准
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleRelatedStandardByIdSql")
    public List<Map<String, Object>> findRuleRelatedStandardById(Map<String, Object> map);

    /**
     * 制度操作说明相关风险
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedRiskByIdSql")
    public List<Map<String, Object>> findRelatedRiskById(Map<String, Object> map);

    /**
     * 制度操作说明相关制度
     *
     * @param map
     * @return
     */
    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedRuleByIdSql")
    public List<Map<String, Object>> findRelatedRuleById(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "fetchInventoryParent")
    @ResultMap("mapper.Rule.fetchInventory")
    List<RuleInfoBean> fetchInventoryParent(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedStandardizedFilesSql")
    public List<StandardizedFile> findRelatedStandardizedFiles(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "findRelatedStandardizedFilesHistorySql")
    public List<StandardizedFile> findRelatedStandardizedFilesHistory(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleBaseInfoSql")
    public RuleBaseInfo findRuleBaseInfo(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleBaseHistoryInfoSql")
    public RuleBaseInfo findRuleBaseHistoryInfo(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "findRuleApplyOrgSql")
    public List<BaseBean> findRuleApplyOrg(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "findTaskRuleFileByRuleIdSql")
    List<BaseBean> findTaskRuleFiles(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "getRuleList")
    List<ChineseEnglisListBean> getRuleList(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "getRuleListCount")
    @ResultType(Integer.class)
    int getRuleListCount(Map<String, Object> map);

    @SelectProvider(type = RuleSqlProvider.class, method = "getChineseEnglishListCount")
    List<Map<String, Object>> getChineseEnglishListCount(Map<String, Object> map);

}
