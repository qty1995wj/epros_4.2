package com.jecn.viewdoc.convert.view;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

/**
 * 流程预览，pageoffice方式
 * 
 * @author hyl
 * 
 */
public class ProcessPageofficeView extends ProcessAbsView {

	public static String[] DEFAULT_ALLOW = new String[] { "doc" };

	public ProcessPageofficeView(IWebProcessService webProcessService, Long relatedId, TreeNodeType nodeType,
			boolean isPub, Long userId, Long historyId, HttpServletRequest request, HttpServletResponse response) {
		super(webProcessService, relatedId, nodeType, isPub, userId, historyId, request, response);
	}

	@Override
	public String getPath() {
		try {
			// 获取存储临时文件的目录
			String filePath = ViewUtils.getNewTempPath(getFileName()).replace("/", "\\\\");
			FileUtils.writeByteArrayToFile(new File(filePath), createDoc.getBytes());
			String extension = FilenameUtils.getExtension(getFileName());
			request.setAttribute("poCtrl", getData(extension, filePath));
			request.setAttribute("viewPage", ViewUtils.getPageofficePage(extension));
		} catch (Exception e) {
			log.error("", e);
			throw new IllegalArgumentException();
		}
		return ViewUtils.getPageofficePage("doc");
	}

	private Object getData(String extension, String filePath) {
		Object result = null;
		if (extension.startsWith("doc")) {
			result = getDOC(request, extension, filePath);
		}
		return result;
	}

	private Object getDOC(HttpServletRequest request, String extension, String filePath) {
		return ViewUtils.getPageOfficeBaseBean(request, extension, filePath);
	}
}
