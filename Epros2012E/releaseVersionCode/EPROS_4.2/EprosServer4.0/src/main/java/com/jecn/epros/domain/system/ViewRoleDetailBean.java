package com.jecn.epros.domain.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.util.JecnProperties;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;


/**
 * @author Angus
 * @description：浏览端角色列表bean
 */
public class ViewRoleDetailBean {
    /**
     * 主键ID
     */
    private String id;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色类型
     */
    private String roleType;
    /**
     * 角色类型名称 根据类型取值 0 用户 1 管理员  2 自定义用户
     */
    private String roleTypeName;
    /**
     * 建立人
     */
    private Long createPeople;
    /**
     * 变更人
     */
    private Long updatePeople;
    /**
     * 更改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date updateTime;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date createTime;
    /**
     * 角色关联人员 类型 0
     */
    private Long[] peopleId;

    /**
     * 角色关联部门 类型 1
     */
    private Long[] orgId;
    /**
     * 角色关联岗位 类型 2
     */
    private Long[] posId;

    private int[] nodes;

    public Long[] getOrgId() {
        return orgId;
    }

    public void setOrgId(Long[] orgId) {
        this.orgId = orgId;
    }

    public Long[] getPosId() {
        return posId;
    }

    public void setPosId(Long[] posId) {
        this.posId = posId;
    }

    public int[] getNodes() {
        return nodes;
    }

    public void setNodes(int[] nodes) {
        this.nodes = nodes;
    }

    public Long[] getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Long[] peopleId) {
        this.peopleId = peopleId;
    }


    public String getRoleTypeName() {
        switch (roleType) {
            case "0":
                roleTypeName = JecnProperties.getValue("user");
                break;
            case "1":
                roleTypeName = JecnProperties.getValue("manager");
                break;
            case "2":
                roleTypeName = JecnProperties.getValue("customRoles");
                break;
            default:
                break;
        }
        return roleTypeName;
    }

    public void setRoleTypeName(String roleTypeName) {
        this.roleTypeName = roleTypeName;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setId(String Id) {
        this.id = Id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public Long getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Long createPeople) {
        this.createPeople = createPeople;
    }

    public String getId() {
        return id;
    }

    public Long getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Long updatePeople) {
        this.updatePeople = updatePeople;
    }
}
