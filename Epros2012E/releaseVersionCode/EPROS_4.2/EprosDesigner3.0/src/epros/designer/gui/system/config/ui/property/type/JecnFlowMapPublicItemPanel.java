package epros.designer.gui.system.config.ui.property.type;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 流程地图发布相关配置
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2014-2-26 时间：下午01:52:52
 */
public class JecnFlowMapPublicItemPanel extends JecnAbstractPropertyBasePanel implements ActionListener {
	/** 相关制度，复选框 */
	private JecnConfigCheckBox checkRelateRuleBox;
	/** 相关文件，复选框 */
	private JecnConfigCheckBox checkRelateFileBox;
	/** 相关文件，复选框 */
	private JecnConfigCheckBox checkAbolishBox;

	private JecnPanel checkPanel = null;

	private JecnPanel abolishPanel = null;

	public JecnFlowMapPublicItemPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		initComponents();
		initLayout();
	}

	private void initComponents() {
		// 制度：
		checkRelateRuleBox = new JecnConfigCheckBox(JecnProperties.getValue("rule"));
		// 文件：
		checkRelateFileBox = new JecnConfigCheckBox(JecnProperties.getValue("file"));
		checkAbolishBox = new JecnConfigCheckBox(JecnProperties.getValue("AbolishFile"));

		checkPanel = new JecnPanel();
		abolishPanel = new JecnPanel();
		checkPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 默认不选中
		checkRelateRuleBox.setSelected(false);
		checkRelateFileBox.setSelected(false);
		checkAbolishBox.setSelected(false);

		checkRelateRuleBox.setOpaque(false);
		checkRelateFileBox.setOpaque(false);
		checkAbolishBox.setOpaque(false);
		
		checkRelateRuleBox.addActionListener(this);
		checkRelateFileBox.addActionListener(this);
		checkAbolishBox.addActionListener(this);
		
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		this.setLayout(new GridBagLayout());
		/** 是否是标准版版本 true：是标准版本；false：不是标准版本 */
		if (JecnConstants.isEPS()) {
			checkRelateRuleBox.setVisible(false);
		}
	}

	/**
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		checkPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

		checkPanel.add(checkRelateRuleBox);
		checkPanel.add(checkRelateFileBox);
		checkPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("publiFlowMapSet")));

		abolishPanel = new JecnPanel();
		abolishPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		abolishPanel.add(checkAbolishBox); //单选框
		abolishPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("abolishIf_file")));
		
	
		int rows = 0;
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0);
		this.add(checkPanel, c);
		rows++;

		c = new GridBagConstraints(0, rows, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0);
		this.add(abolishPanel, c);
	}

	@Override
	public boolean check() {
		return false;
	}

	/**
	 * 初始化获取数据
	 * 
	 */
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;
		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}
		// 唯一标识
		String mark = null;
		String value = null;
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {// 非表数据
			// 唯一标识
			mark = itemBean.getMark();
			// 值
			value = itemBean.getValue();
			if (ConfigItemPartMapMark.pubFlowMapRelateRule.toString().equals(mark)) {// 发布流程，相关制度文件
				// 流程拟制人
				checkRelateRuleBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {
					checkRelateRuleBox.setSelected(true);
				} else {
					checkRelateRuleBox.setSelected(false);
				}

			} else if (ConfigItemPartMapMark.pubFlowMapRelateFile.toString().equals(mark)) {// 发布流程相关文件
				checkRelateFileBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {
					checkRelateFileBox.setSelected(true);
				} else {
					checkRelateFileBox.setSelected(false);
				}
			}else if (ConfigItemPartMapMark.processMapAbolish.toString().equals(mark)) {
				checkAbolishBox.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(value)) {
					checkAbolishBox.setSelected(true);
				} else {
					checkAbolishBox.setSelected(false);
				}
			}
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == checkRelateRuleBox) { // 1：选中；0不选中
				if (checkRelateRuleBox.isSelected()) {// 选中
					checkRelateRuleBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					checkRelateRuleBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			} else if (e.getSource() == checkRelateFileBox) {// 1：选中；0不选中
				if (checkRelateFileBox.getItemBean() == null) {
					return;
				}
				if (checkRelateFileBox.isSelected()) {// 选中
					checkRelateFileBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					checkRelateFileBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}else if(e.getSource() == checkAbolishBox){
				if (checkAbolishBox.isSelected()) {// 选中
					checkAbolishBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					checkAbolishBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}
		}
	}
}
