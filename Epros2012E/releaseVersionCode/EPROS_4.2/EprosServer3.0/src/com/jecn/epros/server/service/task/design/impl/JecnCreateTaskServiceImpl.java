package com.jecn.epros.server.service.task.design.impl;

import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Set;

import oracle.sql.BLOB;

import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.lob.SerializableBlob;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.action.web.task.TaskIllegalArgumentException;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnFileTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskApprovePeopleConn;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskStage;
import com.jecn.epros.server.bean.task.JecnTaskTestRunFile;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;
import com.jecn.epros.server.bean.task.TaskApprovalView;
import com.jecn.epros.server.bean.task.temp.JecnTaskFileTemporary;
import com.jecn.epros.server.bean.task.temp.SimpleSubmitMessage;
import com.jecn.epros.server.bean.task.temp.SimpleTaskBean;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.task.design.IJecnCreateTaskDao;
import com.jecn.epros.server.download.word.ProcessDownDataUtil;
import com.jecn.epros.server.email.buss.TaskEmailParams;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;
import com.jecn.epros.server.service.task.app.impl.JecnCommonTaskService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.webservice.common.TaskApiUrlHandler;

/**
 * 
 * 创建任务任务处理
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Oct 25, 2012 时间：2:21:55 PM
 */
@Transactional(rollbackFor = Exception.class)
public class JecnCreateTaskServiceImpl extends JecnCommonTaskService implements IJecnBaseTaskService {
	/** 创建任务，任务处理DAO */
	private IJecnCreateTaskDao createTaskDao;
	private IFlowStructureDao structureDao;
	private IProcessBasicInfoDao basicInfoDao;

	/**
	 * 创建文件任务
	 * 
	 * @param tempCreateTaskBean
	 *            任务详细信息
	 */
	public void createFileTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {
		if (tempCreateTaskBean == null) {
			log.error("创建文件任务，tempCreateTaskBean验证失败！");
			return;
		}
		try {
			// 记录更新或上传的文件
			List<JecnFileBeanT> listFileBeanT = tempCreateTaskBean.getListFileBeanT();
			if (listFileBeanT == null || listFileBeanT.size() == 0) {
				return;
			}

			JecnTaskBeanNew jecnTaskBeanNew = tempCreateTaskBean.getJecnTaskBeanNew();
			// 源操作时间
			jecnTaskBeanNew.setUpdateTime(new Date());
			jecnTaskBeanNew.setEdit(0);
			Session s = createTaskDao.getSession();

			for (JecnFileBeanT jecnFileBeanT : listFileBeanT) {// 更新文件的基本信息(更新文件任务)
				jecnFileBeanT.setUpdateTime(new Date());
				// 保存到本地
				// jecnFileBeanT.setSaveType(0);
				if (jecnFileBeanT.getFileID() == null) {// 0是目录,1是文件 上传文件
					// 设置更新时间
					// jecnFileBeanT.setCreateTime(new Date());
					// s.save(jecnFileBeanT);
				} else {
					s.update(jecnFileBeanT);
				}
				jecnTaskBeanNew.setResPeopleId(jecnFileBeanT.getResPeopleId());
			}
			// 保存任务信息
			createTaskDao.save(jecnTaskBeanNew);
			// 根据获取的人员主键ID集合更新日志信息及目标操作人信息
			updateToPeopleAndForRecord(tempCreateTaskBean.getListToPeople(), jecnTaskBeanNew,
					JecnTaskCommon.TASK_NOTHIN, JecnTaskCommon.TASK_NOTHIN, jecnTaskBeanNew.getCreatePersonId());
			// 创建任务
			createTask(tempCreateTaskBean);
		} catch (Exception ex) {
			log.error("", ex);
			throw ex;
		}
	}

	/**
	 * 设置任务当前阶段
	 * 
	 * @param jecnTaskBeanNew
	 */
	private void settingTaskState(JecnTaskBeanNew jecnTaskBeanNew) {
		List<JecnTaskStage> taskStageList = this.abstractTaskDao.getJecnTaskStageList(jecnTaskBeanNew.getId());
		JecnTaskStage taskStage = JecnTaskCommon.getNextTaskStage(taskStageList, jecnTaskBeanNew.getState());
		jecnTaskBeanNew.setState(taskStage.getStageMark());

		jecnTaskBeanNew.setStateMark(JecnTaskCommon.getCurTaskStage(taskStageList, jecnTaskBeanNew.getState())
				.getStageName());
	}

	/**
	 * 获取人员主键ID集合 发送邮件
	 * 
	 * @param peopleList
	 * @throws Exception
	 */
	public void sendTaskEmail(Set<Long> peopleList, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		if (peopleList == null || peopleList.size() == 0) {
			return;
		}

		// 创建任务是否需要发送邮件
		boolean isTrue = JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.emailAddApprover.toString(), JecnTaskCommon
				.selectMessageAndEmailItemBean(configItemDao));
		if (isTrue) {
			// 获取任务创建人名称
			JecnUser createJecnUser = personDao.get(jecnTaskBeanNew.getCreatePersonId());
			jecnTaskBeanNew.setCreatePersonTemporaryName(createJecnUser.getTrueName());
			// Object[] 0:内外网；1：邮件地址；2：人员主键ID
			List<JecnUser> userObject = personDao.getJecnUserList(peopleList);
			if (!userObject.isEmpty()) {
				TaskEmailParams emailParams = null;
				if (jecnTaskBeanNew.getTaskType() == 0) {
					emailParams = ProcessDownDataUtil.getTaskEmailParams(false, jecnTaskBeanNew.getRid(), structureDao,
							basicInfoDao);
				}
				// 发送邮件 0为给审批人发送邮件 1为给拟稿人发送邮件
				BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_APPROVE);
				emailBuilder.setData(userObject, jecnTaskBeanNew, 0, emailParams);
				List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
				// TODO :EMAIL
				JecnUtil.saveEmail(buildEmail);
			}
		}

		// 创建任务是否需要发送消息
		boolean isMesTrue = JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.mesAddApprover.toString(), JecnTaskCommon
				.selectMessageAndEmailItemBean(configItemDao));
		if (isMesTrue) {// 发送消息
			JecnTaskCommon.createTaskMessage(peopleList, jecnTaskBeanNew.getTaskType(), jecnTaskBeanNew.getTaskName(),
					this.abstractTaskDao.getSession(), 3);
		}

	}

	/**
	 * 创建流程任务
	 * 
	 * @param tempCreateTaskBean
	 *            提交审批任务信息
	 * @throws Exception
	 */
	public void createFlowTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {
		if (tempCreateTaskBean == null || tempCreateTaskBean.getJecnTaskBeanNew() == null) {
			throw new IllegalArgumentException(JecnTaskCommon.STAFF_CHANGE);
		}
		// 任务主表信息
		JecnTaskBeanNew jecnTaskBeanNew = tempCreateTaskBean.getJecnTaskBeanNew();
		// 各阶段 审批人 目标人集合
		Set<Long> setPeopleIds = tempCreateTaskBean.getListToPeople();

		Long flowId = jecnTaskBeanNew.getRid();
		if (flowId == null || setPeopleIds == null || setPeopleIds.size() == 0) {
			throw new NullPointerException(
					"创建任务createFlowTask 异常！flowId == null || setPeopleIds == null || setPeopleIds.size() == 0");
		}
		// 源操作时间
		jecnTaskBeanNew.setUpdateTime(new Date());
		jecnTaskBeanNew.setEdit(0);
		Session s = createTaskDao.getSession();
		// 保存任务信息
		createTaskDao.save(jecnTaskBeanNew);

		// 更新流程属性表和流程主表
		if (tempCreateTaskBean.getFlowBasicInfoT() != null) {
			s.update(tempCreateTaskBean.getFlowBasicInfoT());
			jecnTaskBeanNew.setResPeopleId(tempCreateTaskBean.getFlowBasicInfoT().getResPeopleId());
		} else if (tempCreateTaskBean.getMainFlowT() != null) {
			s.update(tempCreateTaskBean.getMainFlowT());
			jecnTaskBeanNew.setResPeopleId(tempCreateTaskBean.getMainFlowT().getResPeopleId());
		}
		if (tempCreateTaskBean.getFlowStructureT() != null) {
			s.update(tempCreateTaskBean.getFlowStructureT());
		}

		// 根据获取的人员主键ID集合更新日志信息及目标操作人信息
		updateToPeopleAndForRecord(setPeopleIds, jecnTaskBeanNew, "", JecnTaskCommon.TASK_NOTHIN, jecnTaskBeanNew
				.getCreatePersonId());

		// 创建任务
		createTask(tempCreateTaskBean);

	}

	/**
	 * 创建制度任务
	 * 
	 * @param tempCreateTaskBean
	 *            任务详细信息
	 */
	public void createRuleTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {
		if (tempCreateTaskBean == null) {
			log.error("根据制度任务信息获取制度主键ID为空！");
			return;
		}
		// 任务主表信息
		JecnTaskBeanNew jecnTaskBeanNew = tempCreateTaskBean.getJecnTaskBeanNew();
		// 目标人集合
		Set<Long> setPeopleIds = tempCreateTaskBean.getListToPeople();
		try {
			// 源操作时间
			jecnTaskBeanNew.setUpdateTime(new Date());
			jecnTaskBeanNew.setEdit(0);
			Session s = createTaskDao.getSession();
			if (tempCreateTaskBean.getRuleT() != null) {
				// 更新制度信息
				s.update(tempCreateTaskBean.getRuleT());
				jecnTaskBeanNew.setResPeopleId(tempCreateTaskBean.getRuleT().getAttchMentId());
			}
			// 保存任务信息
			createTaskDao.save(jecnTaskBeanNew);

			// 根据获取的人员主键ID集合更新日志信息及目标操作人信息
			updateToPeopleAndForRecord(setPeopleIds, jecnTaskBeanNew, "", JecnTaskCommon.TASK_NOTHIN, jecnTaskBeanNew
					.getCreatePersonId());
			// 创建任务
			createTask(tempCreateTaskBean);

		} catch (Exception ex) {
			log.error("创建制度任务失败！" + ex);
			throw ex;
		}
	}

	/**
	 * 创建文件任务
	 * 
	 * @param jecnTaskTempBean
	 * @throws DaoException
	 */
	public void createUplodeFileTask(Long taskId, List<JecnFileTaskBeanNew> jecnFileTaskBeanNewList) throws Exception {
		if (jecnFileTaskBeanNewList == null) {
			return;
		}
		Session s = createTaskDao.getSession();
		try {
			/** 更新上传的文件 */
			for (JecnFileTaskBeanNew jecnFileTaskBeanNew : jecnFileTaskBeanNewList) {
				if (jecnFileTaskBeanNew == null || jecnFileTaskBeanNew.getBytes() == null) {
					continue;
				}
				jecnFileTaskBeanNew.setTaskId(taskId);
				if (JecnContants.dbType.equals(JecnCommonSql.DBType.SQLSERVER)) {
					jecnFileTaskBeanNew.setFileStream(Hibernate.createBlob(jecnFileTaskBeanNew.getBytes()));
					s.save(jecnFileTaskBeanNew);
				} else if (JecnContants.dbType.equals(JecnCommonSql.DBType.ORACLE)) {
					byte[] buffer = new byte[1];
					buffer[0] = 1;
					jecnFileTaskBeanNew.setFileStream(Hibernate.createBlob(buffer));
					s.save(jecnFileTaskBeanNew);
					s.flush();
					s.refresh(jecnFileTaskBeanNew, LockMode.UPGRADE);
					SerializableBlob sb = (SerializableBlob) jecnFileTaskBeanNew.getFileStream();
					java.sql.Blob wrapBlob = sb.getWrappedBlob();
					BLOB blob = (BLOB) wrapBlob;
					OutputStream out = blob.getBinaryOutputStream();
					out.write(jecnFileTaskBeanNew.getBytes());
					out.close();
				}
			}
		} catch (Exception ex) {
			log.error("createFileTask方法出现异常,数据库保存失败!" + ex);
			throw ex;
		}
	}

	/**
	 * 创建任务
	 * 
	 * @param tempCreateTaskBean
	 *            任务详细信息
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public void createTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {
		// 任务主键ID
		Long taskId = tempCreateTaskBean.getJecnTaskBeanNew().getId();
		Session s = createTaskDao.getSession();

		if (tempCreateTaskBean.getJecnTaskApplicationNew() != null) {// 任务显示信息
			tempCreateTaskBean.getJecnTaskApplicationNew().setTaskId(taskId);
			s.save(tempCreateTaskBean.getJecnTaskApplicationNew());
		}
		if (tempCreateTaskBean.getJecnTaskStageList() != null) { // 任务各阶段信息（审批顺序）
			for (JecnTaskStage jecnTaskStage : tempCreateTaskBean.getJecnTaskStageList()) {
				jecnTaskStage.setTaskId(taskId);
				s.save(jecnTaskStage);
			}
		}
		if (tempCreateTaskBean.getJecnTaskApprovePeopleConnList() != null) {// 任务各个环节的审批人
			for (JecnTaskApprovePeopleConn jecnTaskApprovePeopleConn : tempCreateTaskBean
					.getJecnTaskApprovePeopleConnList()) {
				for (JecnTaskStage jecnTaskStage : tempCreateTaskBean.getJecnTaskStageList()) {
					if (jecnTaskApprovePeopleConn.getStageId().intValue() == jecnTaskStage.getStageMark()) {
						jecnTaskApprovePeopleConn.setStageId(jecnTaskStage.getId());
						break;
					}
				}
				s.save(jecnTaskApprovePeopleConn);
			}
		}
		// 查阅权限
		if (tempCreateTaskBean.getAccessBean() != null && tempCreateTaskBean.getJecnTaskApplicationNew().getIsAccess() != 0) {// 显示查阅权限
			updateAccessPermissions(tempCreateTaskBean.getAccessBean());
		}
		// 任务上传试用报告
		JecnTaskTestRunFile jecnTaskTestRunFile = tempCreateTaskBean.getJecnTaskTestRunFile();
		if (jecnTaskTestRunFile != null && jecnTaskTestRunFile.getBytes() != null) {
			jecnTaskTestRunFile.setTaskId(taskId);
			if (JecnContants.dbType == DBType.SQLSERVER || JecnContants.dbType == DBType.MYSQL) {
				jecnTaskTestRunFile.setFileStream(Hibernate.createBlob(jecnTaskTestRunFile.getBytes()));
				s.save(jecnTaskTestRunFile);
				s.flush();
			} else if (JecnContants.dbType == JecnCommonSql.DBType.ORACLE) {
				byte[] buffer = new byte[1];
				buffer[0] = 1;
				jecnTaskTestRunFile.setFileStream(Hibernate.createBlob(buffer));
				s.save(jecnTaskTestRunFile);
				s.flush();
				s.refresh(jecnTaskTestRunFile, LockMode.UPGRADE);
				SerializableBlob sb = (SerializableBlob) jecnTaskTestRunFile.getFileStream();
				java.sql.Blob wrapBlob = sb.getWrappedBlob();
				BLOB blob = (BLOB) wrapBlob;
				OutputStream out = blob.getBinaryOutputStream();
				out.write(jecnTaskTestRunFile.getBytes());
				out.close();
			}
		}

		// 通过webservice 发布待办任务
		if (JecnConfigTool.isRelatedTaskByWebService()) {
			saveTaskApprovalView(tempCreateTaskBean.getJecnTaskBeanNew());
		}
		// 设置任务当前阶段
		settingTaskState(tempCreateTaskBean.getJecnTaskBeanNew());
		createTaskDao.update(tempCreateTaskBean.getJecnTaskBeanNew());
		// 发送代办信息
		sendTaskInfo(tempCreateTaskBean.getJecnTaskBeanNew(), personDao, tempCreateTaskBean.getListToPeople(),
				tempCreateTaskBean.getJecnTaskBeanNew().getCreatePersonId());

		sendTaskEmail(tempCreateTaskBean.getListToPeople(), tempCreateTaskBean.getJecnTaskBeanNew());
	}

	private void saveTaskApprovalView(JecnTaskBeanNew beanNew) {
		JecnUser user = this.personDao.get(beanNew.getCreatePersonId());
		TaskApprovalView approvalView = new TaskApprovalView();
		approvalView.setTaskId(beanNew.getId());
		approvalView.setTaskName(beanNew.getTaskName());
		approvalView.setRelatedId(beanNew.getRid());
		approvalView.setReleaseState(0);
		approvalView.setCreateDate(new Date());
		approvalView.setTaskType(beanNew.getTaskType());
		approvalView.setApplyName(user.getLoginName());
		approvalView.setRelatedDataUrl(getRelatedDataUrl(beanNew.getTaskType(), beanNew.getRid()));
		this.abstractTaskDao.getSession().save(approvalView);
	}

	/**
	 * 
	 * @param taskType
	 *            0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
	 * @return
	 */
	private String getRelatedDataUrl(int taskType, Long rid) {
		String baseUrl = JecnConfigTool.getBaseEprosUrl();
		switch (taskType) {
		case 0:
		case 4:
			return baseUrl + TaskApiUrlHandler.getProcessDetail(rid.toString(), "false");
		case 1:

			return baseUrl + TaskApiUrlHandler.getFile(rid.toString(), "false");
		case 2:
		case 3:
			return baseUrl + TaskApiUrlHandler.getRule(rid.toString(), "false");
		default:
			break;
		}
		return "";
	}

	public IJecnCreateTaskDao getCreateTaskDao() {
		return createTaskDao;
	}

	public void setCreateTaskDao(IJecnCreateTaskDao createTaskDao) {
		this.createTaskDao = createTaskDao;
	}

	/**
	 * 创建:流程任务、文件、制度任务
	 * 
	 * @param tempCreateTaskBean
	 *            创建任务信息
	 * @throws BsException
	 */
	@Override
	public void createPRFTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {
		if (tempCreateTaskBean == null || tempCreateTaskBean.getJecnTaskBeanNew() == null) {
			return;
		}
		// 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
		int taskType = tempCreateTaskBean.getJecnTaskBeanNew().getTaskType();
		boolean isInTask = isInTask(tempCreateTaskBean.getJecnTaskBeanNew().getRid(), taskType);
		if (isInTask) {
			throw new TaskIllegalArgumentException("文件已在审批中", 10);
		}
		switch (taskType) {
		case 0:// 流程图任务
		case 4:// 流程地图任务
			createFlowTask(tempCreateTaskBean);
			break;
		case 1:
			createFileTask(tempCreateTaskBean);
			break;
		case 2:
		case 3:
			createRuleTask(tempCreateTaskBean);
			break;
		default:
			break;
		}
	}

	private boolean isInTask(Long rid, int taskType) {
		String sql = "select count(t.id) from JECN_TASK_BEAN_NEW t where t.R_ID=? and t.TASK_TYPE=? and t.state<>5  and t.is_lock=1";
		int count = taskRecordDao.countAllByParamsNativeSql(sql, rid, taskType);
		if (count > 0) {
			return true;
		}
		return false;
	}

	@Override
	public JecnTaskFileTemporary downLoadTaskRunFile(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SimpleTaskBean getSimpleTaskBeanById(Long taskId, int taskOperation) throws Exception {
		return null;
	}

	@Override
	public boolean isApprovePeopleChecked(Long peopleId, Long taskId, int taskElseState, int approveCounts)
			throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void taskOperation(SimpleSubmitMessage submitMessage) throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * 当前任务，当前操作人是否已审批任务
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param curPeopleId
	 *            当前操作人主键ID
	 * @return true 当前操作人已审批，或无审批权限
	 * @throws Exception
	 */
	@Override
	public boolean hasApproveTask(Long taskId, Long curPeopleId, int taskOperation) throws Exception {
		return super.hasApproveTask(taskId, curPeopleId, taskOperation);
	}

	@Override
	public SimpleTaskBean checkTaskIsDelay(SimpleTaskBean simpleTaskBean) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public IFlowStructureDao getStructureDao() {
		return structureDao;
	}

	public void setStructureDao(IFlowStructureDao structureDao) {
		this.structureDao = structureDao;
	}

	public IProcessBasicInfoDao getBasicInfoDao() {
		return basicInfoDao;
	}

	public void setBasicInfoDao(IProcessBasicInfoDao basicInfoDao) {
		this.basicInfoDao = basicInfoDao;
	}
}
