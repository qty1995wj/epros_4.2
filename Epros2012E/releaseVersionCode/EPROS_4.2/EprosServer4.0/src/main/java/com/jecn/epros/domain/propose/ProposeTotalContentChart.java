package com.jecn.epros.domain.propose;

import java.util.List;

public class ProposeTotalContentChart {
    private String name;
    private List<Integer> listContents;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getListContents() {
        return listContents;
    }

    public void setListContents(List<Integer> listContents) {
        this.listContents = listContents;
    }
}
