package epros.designer.gui.system.config.ui.dialog;

import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.util.JecnProperties;

/**
 * 
 * 流程地图配置对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTotalMapConfigJDialog extends JecnAbtractBaseConfigDialog {

	public JecnTotalMapConfigJDialog(JecnConfigDesgBean configBean) {
		super(configBean);
		this.setTitle(JecnProperties.getValue("setTitleTotalName"));
	}
}
