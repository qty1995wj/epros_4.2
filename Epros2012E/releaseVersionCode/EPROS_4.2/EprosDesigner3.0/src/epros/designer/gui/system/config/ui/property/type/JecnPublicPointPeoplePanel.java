package epros.designer.gui.system.config.ui.property.type;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnFixedEmail;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.popedom.person.PersonChooseDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;
/***
 * 发布指向固定人员Panel 
 * 2014-04-09
 *
 */
public class JecnPublicPointPeoplePanel extends JecnPanel{
	
	private static Logger log = Logger.getLogger(JecnPublicPointPeoplePanel.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(450, 300);

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel(430, 225);

	/** 滚动面板 */
	private JScrollPane fixedEmailScrollPane = new JScrollPane();

	private FixedEmailTable fixedEmailTable = null;

	/** 操作按钮面板 */
	private JecnPanel operaButPanel = new JecnPanel(420, 30);

	/** 添加按钮 */
	private JButton addBut = new JButton(JecnProperties.getValue("addBtn"));

	/** 删除 */
	private JButton deleteBut = new JButton(JecnProperties
			.getValue("deleteBtn"));

	/** 设置大小 */
	Dimension dimension = null;
	
	/**类别JLabel*/
	private JLabel typeLab = new JLabel(JecnProperties.getValue("typeDifC"));
	/** 流程 单选按钮 */
	private JRadioButton flowRadBut = new JRadioButton(JecnProperties.getValue("process"));

	/** 制度 单选按钮 */
	private JRadioButton ruleRadBut = new JRadioButton(JecnProperties.getValue("rule"));
	/** 文件 单选按钮 */
	private JRadioButton fileRadBut = new JRadioButton(JecnProperties.getValue("file"));
	
	private Long isType = 0L;

	protected List<JecnFixedEmail> listFixedEmailBean = new ArrayList<JecnFixedEmail>();
	private List<JecnTreeBean> listFixedEmail = new ArrayList<JecnTreeBean>();
	/**发布：固定人员发送消息邮件Table集合：key：人员ID, value：类型(0流程、1制度、2文件)*/
	Map<String,String> mapTable = new HashMap<String,String>();
	/** 人员ids */
	private String personIds = "";
	
	public Map<String, String> getMapTable() {
		return mapTable;
	}
	public void setMapTable(Map<String, String> mapTable) {
		this.mapTable = mapTable;
	}
	public List<JecnFixedEmail> getListFixedEmailBean() {
		return listFixedEmailBean;
	}
	public void setListFixedEmailBean(List<JecnFixedEmail> listFixedEmailBean) {
		this.listFixedEmailBean = listFixedEmailBean;
	}
	public JecnPublicPointPeoplePanel(){
		this.setSize(450, 300);
		
		/** 类别 按钮实现 单选效果 */
		ButtonGroup bgp = new ButtonGroup();
		bgp.add(flowRadBut);
		bgp.add(ruleRadBut);
		bgp.add(fileRadBut);
		flowRadBut.setSelected(true);
		
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		fixedEmailScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		operaButPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		flowRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		ruleRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		fileRadBut.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		
		// 流程滚动面板大小
		dimension = new Dimension(410, 180);
		fixedEmailScrollPane.setPreferredSize(dimension);
		fixedEmailScrollPane.setMaximumSize(dimension);
		fixedEmailScrollPane.setMinimumSize(dimension);
		fixedEmailScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		addBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				addButPerformed();
			}

		});

		deleteBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}

		});
		flowRadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isType = 0L;
			}
		});
		ruleRadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isType = 1L;
			}
		});
		fileRadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isType = 2L;
			}
		});
		/** 是否是标准版版本 true：是标准版本；false：不是标准版本*/
		if(JecnConstants.isEPS()){
			ruleRadBut.setVisible(false);
		}
		initLayout();
	}
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0, 0);
		this.add(mainPanel, c);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 类别
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(fixedEmailScrollPane, c);
		// fixedEmailScrollPane
		fixedEmailTable = new FixedEmailTable();
		fixedEmailScrollPane.setViewportView(fixedEmailTable);

		// 操作按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(operaButPanel, c);
		operaButPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		operaButPanel.add(typeLab);
		operaButPanel.add(flowRadBut);
		operaButPanel.add(ruleRadBut);
		operaButPanel.add(fileRadBut);
		operaButPanel.add(addBut);
		operaButPanel.add(deleteBut);
	}

	/**
	 * 添加
	 */
	private void addButPerformed() {
		PersonChooseDialog personChooseDialog = new PersonChooseDialog(listFixedEmail);
		personChooseDialog.setSelectMutil(true);
		personChooseDialog.setVisible(true);
		if (personChooseDialog.isOperation()) {
			if (listFixedEmail != null && listFixedEmail.size() > 0) {
				// 判断Table集合中相同类型的人员是否重复
				String idTypeCompare = null;
				for (int i = listFixedEmail.size() - 1; i >= 0; i--) {
					if (mapTable.get(listFixedEmail.get(i).getId() + "," + isType) != null
							&& !"".equals(mapTable.get(listFixedEmail.get(i).getId() + "," + isType))) {
						idTypeCompare = mapTable.get(listFixedEmail.get(i).getId() + "," + isType);
						if (String.valueOf(listFixedEmail.get(i).getId()).equals(idTypeCompare)) {
							listFixedEmail.remove(i);
						}
					}
					// 如果不存在要添加的数据 return
					if (listFixedEmail.size() == 0) {
						return;
					}
				}
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : listFixedEmail) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				personIds = sbIds.substring(0, sbIds.length() - 1);
				for (int i = 0; i < listFixedEmail.size(); i++) {
					JecnTreeBean jecnTreeBean = listFixedEmail.get(i);
					Vector vec = new Vector();
					// 主键ID
					vec.add("");
					// 人员ID
					vec.add(jecnTreeBean.getId());
					vec.add(jecnTreeBean.getName());
					if (isType == 0L) {
						vec.add(JecnProperties.getValue("process"));
					} else if (isType == 1L) {
						vec.add(JecnProperties.getValue("rule"));
					} else if (isType == 2L) {
						vec.add(JecnProperties.getValue("file"));
					}
					((DefaultTableModel) fixedEmailTable.getModel()).addRow(vec);
					String idType = String.valueOf(jecnTreeBean.getId()) + "," + String.valueOf(isType);
					mapTable.put(idType, String.valueOf(jecnTreeBean.getId()));
				}
				List<JecnFixedEmail> fixedEmailBeanList = new ArrayList<JecnFixedEmail>();
				for (JecnTreeBean jecnTreeBean : listFixedEmail) {
					JecnFixedEmail fixedEmail = new JecnFixedEmail();
					fixedEmail.setId(UUID.randomUUID().toString());
					fixedEmail.setPersonId(jecnTreeBean.getId());
					fixedEmail.setType(Integer.parseInt(isType.toString()));
					fixedEmailBeanList.add(fixedEmail);
				}
				try {
					// 添加 固定接受消息邮件人员数据
					ConnectionPool.getFixedEmailAction().AddFixedEmail(fixedEmailBeanList);
				} catch (Exception e) {
					log.error("JecnPublicPointPeoplePanel addButPerformed is error", e);
				}
				listFixedEmail.clear();
			}
		}
	}

	/**
	 * 删除
	 */
	private void deleteButPerformed() {
		int[] selectRows = fixedEmailTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pleaseChooseDeleteType"));
			return;
		}
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		//获取表格中的数据
		Vector<Vector<String>> vectors = ((DefaultTableModel) fixedEmailTable
				.getModel()).getDataVector();
		List<String> fixedEmailIds = new ArrayList<String>();
		//记录删除的Map数据的Key
		String[] mapKey = new String[selectRows.length];
		for (int i = 0; i < selectRows.length; i++) {
			Vector<String> vector = vectors.get(selectRows[i]);
			fixedEmailIds.add(String.valueOf(vector.get(0)));
			// 获取该条数据的类型，默认为流程 0
			int peopleType=0;
			if(JecnProperties.getValue("rule").equals(vector.get(3))){
				peopleType=1;
			}else if(JecnProperties.getValue("file").equals(vector.get(3))){
				peopleType=2;
			}
			mapKey[i]=String.valueOf(vector.get(1))+","+peopleType;
		}
		// 删除数据库中的流程类别数据
		try {
			ConnectionPool.getFixedEmailAction().deleteFixedEmail(fixedEmailIds);
		} catch (Exception e) {
			log.error("JecnPublicPointPeoplePanel deleteButPerformed is error",e);
		}
		// 删除表格中的数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) fixedEmailTable.getModel())
					.removeRow(selectRows[i]);
			mapTable.remove(mapKey[i]);
		}
	}

	class FixedEmailTable extends JTable {

		FixedEmailTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		}

		public Vector<Vector<String>> getContent() {
			Vector<Vector<String>> vs = new Vector<Vector<String>>();
			// 获取数据
			try {
				listFixedEmailBean =ConnectionPool.getFixedEmailAction().getFixedEmailList();
				if(listFixedEmailBean != null && listFixedEmailBean.size() >0){
					for (JecnFixedEmail fixedEmail : listFixedEmailBean) {
						Vector<String> row = new Vector<String>();
						row.add(String.valueOf(fixedEmail.getId()));
						row.add(String.valueOf(fixedEmail.getPersonId()));
						row.add(fixedEmail.getPersonName());
						if(fixedEmail.getType() ==  0L){
							row.add(JecnProperties.getValue("process"));
						}else if(fixedEmail.getType() ==  1L){
							row.add(JecnProperties.getValue("rule"));
						}else if(fixedEmail.getType() ==  2L){
							row.add(JecnProperties.getValue("file"));
						}
						vs.add(row);
						String idType = String.valueOf(fixedEmail.getPersonId())+","+ String.valueOf(fixedEmail.getType());
						mapTable.put(idType, String.valueOf(fixedEmail.getPersonId()));
					}
				}
				
			} catch (Exception e1) {
				log.error("JecnPublicPointPeoplePanel FixedEmailTable is error",e1);
			}
			return vs;
		}

		public FixedEmailTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add("peopleId");
			title.add(JecnProperties.getValue("personName"));
			title.add(JecnProperties.getValue("typeName"));
			return new FixedEmailTableMode(getContent(), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {

			return new int[] { 0,1 };
		}

		class FixedEmailTableMode extends DefaultTableModel {
			public FixedEmailTableMode(Vector<Vector<String>> data,
					Vector<String> title) {
				super(data, title);
			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return false;
			}
		}
	}
}
