package epros.designer.gui.system.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnRecycleDialog;
import epros.designer.gui.project.RecycleProjectManageDiaolog;
import epros.designer.gui.recycle.FileRecyclePanel;
import epros.designer.gui.recycle.OrgRecyclePanel;
import epros.designer.gui.recycle.ProcessRecyclePanel;
import epros.designer.gui.recycle.RuleRecyclePanel;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;

/**
 * 
 * 回收站按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnRecoverMenu extends JPopupMenu implements ActionListener {

	/** 项目回收 */
	private JMenuItem projectRecoverButton = null;
	/** 流程回收 */
	private JMenuItem processRecoverButton = null;
	/** 组织回收 */
	private JMenuItem orgRecoverButton = null;
	/** 文件回收 */
	private JMenuItem fileRecoverButton = null;
	/** 制度回收 */
	private JMenuItem ruleRecoverButton = null;

	public JecnRecoverMenu() {

		// 流程回收
		processRecoverButton = new JMenuItem();
		processRecoverButton.setText(JecnProperties.getValue("processRecover"));
		// 添加事件监听
		processRecoverButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (JecnConstants.projectId == null) {
					return;
				}
				JecnRecycleDialog recycleDialog = new JecnRecycleDialog(new ProcessRecyclePanel());
				recycleDialog.setTitle(JecnProperties.getValue("flowRecoverStation"));
				recycleDialog.setDefaultCloseOperation(JecnRecycleDialog.DISPOSE_ON_CLOSE);
				recycleDialog.setVisible(true);
			}
		});
		processRecoverButton.setIcon(new ImageIcon("images/setingImages/processRecover.gif"));

		// 项目回收
		projectRecoverButton = new JMenuItem();
		projectRecoverButton.setText(JecnProperties.getValue("projectRecover"));
		// 添加事件监听
		projectRecoverButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!JecnDesignerCommon.isAdminTip()) {
					return;
				}
				RecycleProjectManageDiaolog projectDialog = new RecycleProjectManageDiaolog();
				projectDialog.setDefaultCloseOperation(RecycleProjectManageDiaolog.DISPOSE_ON_CLOSE);
				projectDialog.setVisible(true);
			}
		});
		projectRecoverButton.setIcon(new ImageIcon("images/setingImages/projectRecover.gif"));

		// 组织回收
		orgRecoverButton = new JMenuItem();
		orgRecoverButton.setText(JecnProperties.getValue("orgRecover"));
		// 添加事件监听
		orgRecoverButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (JecnConstants.projectId == null || !JecnDesignerCommon.isAdminTip()) {
					return;
				}
				JecnRecycleDialog recycleDialog = new JecnRecycleDialog(new OrgRecyclePanel());
				recycleDialog.setTitle(JecnProperties.getValue("orgRecoverStation"));
				recycleDialog.setDefaultCloseOperation(JecnRecycleDialog.DISPOSE_ON_CLOSE);
				recycleDialog.setVisible(true);
			}
		});
		orgRecoverButton.setIcon(new ImageIcon("images/setingImages/orgRecover.gif"));

		// 文件回收
		fileRecoverButton = new JMenuItem();
		fileRecoverButton.setText(JecnProperties.getValue("fileRecover"));
		// 添加事件监听
		fileRecoverButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (JecnConstants.projectId == null) {
					return;
				}
				JecnRecycleDialog recycleDialog = new JecnRecycleDialog(new FileRecyclePanel());
				recycleDialog.setTitle(JecnProperties.getValue("fileRecoverStation"));
				recycleDialog.setDefaultCloseOperation(JecnRecycleDialog.DISPOSE_ON_CLOSE);
				recycleDialog.setVisible(true);
			}
		});
		fileRecoverButton.setIcon(new ImageIcon("images/setingImages/fileRecover.gif"));

		// 制度回收
		ruleRecoverButton = new JMenuItem();
		ruleRecoverButton.setText(JecnProperties.getValue("ruleRecover"));
		// 添加事件监听
		ruleRecoverButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (JecnConstants.projectId == null) {
					return;
				}
				JecnRecycleDialog recycleDialog = new JecnRecycleDialog(new RuleRecyclePanel());
				recycleDialog.setTitle(JecnProperties.getValue("ruleRecoverStation"));
				recycleDialog.setDefaultCloseOperation(JecnRecycleDialog.DISPOSE_ON_CLOSE);
				recycleDialog.setVisible(true);
			}
		});
		ruleRecoverButton.setIcon(new ImageIcon("images/setingImages/ruleRecover.gif"));
		
		if (JecnConstants.isShowProjectManagement) {
			this.add(projectRecoverButton);
		}
		this.add(processRecoverButton);
		this.add(orgRecoverButton);
		this.add(fileRecoverButton);
		if (!JecnConfigTool.isHiddenRule()) {
			this.add(ruleRecoverButton);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
