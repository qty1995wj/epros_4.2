package epros.designer.gui.process.guide.explain;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnElementsLibrary;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.table.JecnActiveTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 关键活动
 * 
 * @author user
 * 
 */
public class JecnKeyActive extends JecnFileDescriptionComponent {

	/** 问题区域 **/
	private JecnActiveTable questionAreaTable;
	/** 关键成功因素 **/
	private JecnActiveTable keySuccessTable;
	/** 关键控制点 **/
	private JecnActiveTable keyContrPointTable;
	/** 关键控制点(合规) */
	private JecnActiveTable keyContrPointAllowTable;

	private ProcessOperationDialog processOperationDialog;

	public JecnKeyActive(int index, String name, boolean isRequest, JecnPanel contentPanel,
			ProcessOperationDialog processOperationDialog, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.processOperationDialog = processOperationDialog;
		questionAreaTable = new QuestionAreaTable(processOperationDialog);
		keySuccessTable = new KeySuccessTable(processOperationDialog);
		keyContrPointTable = new KeyContrPointTable(processOperationDialog);
		keyContrPointAllowTable = new KeyContrPointAllowTable(processOperationDialog);

		boolean kcpAllowShow = false;
		// 查询kcp（合规是否启用）
		JecnElementsLibrary e = ConnectionPool.getConfigAciton().getElement("KCPFigureComp");
		kcpAllowShow = e.getValue() == 0 ? false : true;

		GuideJScrollPane questionPanel = new GuideJScrollPane();
		questionPanel.setViewportView(questionAreaTable);
		questionPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		GuideJScrollPane keySuccessPanel = new GuideJScrollPane();
		keySuccessPanel.setViewportView(keySuccessTable);
		keySuccessPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		GuideJScrollPane keyControlPanel = new GuideJScrollPane();
		keyControlPanel.setViewportView(keyContrPointTable);
		keyControlPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		GuideJScrollPane keyControlAllowPanel = new GuideJScrollPane();
		keyControlAllowPanel.setViewportView(keyContrPointAllowTable);
		keyControlAllowPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBorder(BorderFactory.createTitledBorder(""));
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(new JLabel(JecnProperties.getValue("problemAreas")), c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(questionPanel, c);

		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(new JLabel(JecnProperties.getValue("keySuccessFactorsStar")), c);

		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(keySuccessPanel, c);

		c = new GridBagConstraints(0, 4, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(new JLabel(JecnProperties.getValue("keyControlPointStar")), c);

		c = new GridBagConstraints(0, 5, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(keyControlPanel, c);

		if (kcpAllowShow) {
			c = new GridBagConstraints(0, 6, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			centerPanel.add(new JLabel(JecnProperties.getValue("keyControlPointAllowStar")), c);

			c = new GridBagConstraints(0, 7, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
					0, 0);
			centerPanel.add(keyControlAllowPanel, c);
		}
	}

	public void refreshTable() {
		this.keySuccessTable.refluseTable();
		this.keyContrPointTable.refluseTable();
		this.questionAreaTable.refluseTable();
		this.keyContrPointAllowTable.refluseTable();
	}

	private Vector<Vector<String>> getContent(List<JecnActivityShowBean> data) {
		Vector<Vector<String>> rowData = new Vector<Vector<String>>();
		for (JecnActivityShowBean activityShowBean : data) {
			Vector<String> row = new Vector<String>();
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getActivityNum());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFigureText());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShowAndControl());
			rowData.add(row);
		}
		return rowData;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

	class QuestionAreaTable extends JecnActiveTable {
		public QuestionAreaTable(ProcessOperationDialog processOperationDialog) {
			super(0, processOperationDialog);
		}

		@Override
		public JecnTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("activitiesNumbers"));
			title.add(JecnProperties.getValue("activityTitle"));
			title.add(JecnProperties.getValue("activityIndicatingThat"));
			title.add(JecnProperties.getValue("noteStar"));

			// 获取操作说明数据
			Vector<Vector<String>> rowData = getContent(processOperationDialog.getFlowOperationBean()
					.getListPAActivityShow() == null ? new ArrayList<JecnActivityShowBean>() : processOperationDialog
					.getFlowOperationBean().getListPAActivityShow());
			return new JecnTableModel(title, rowData);
		}

	}

	class KeySuccessTable extends JecnActiveTable {
		public KeySuccessTable(ProcessOperationDialog processOperationDialog) {
			super(1, processOperationDialog);
		}

		@Override
		public JecnTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("activitiesNumbers"));
			title.add(JecnProperties.getValue("activityTitle"));
			title.add(JecnProperties.getValue("activityIndicatingThat"));
			title.add(JecnProperties.getValue("keySuccessFactors"));
			// 获取操作说明数据
			Vector<Vector<String>> rowData = getContent(processOperationDialog.getFlowOperationBean()
					.getListKSFActivityShow() == null ? new ArrayList<JecnActivityShowBean>() : processOperationDialog
					.getFlowOperationBean().getListKSFActivityShow());
			return new JecnTableModel(title, rowData);
		}

	}

	class KeyContrPointTable extends JecnActiveTable {
		public KeyContrPointTable(ProcessOperationDialog processOperationDialog) {
			super(2, processOperationDialog);
		}

		@Override
		public JecnTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("activitiesNumbers"));
			title.add(JecnProperties.getValue("activityTitle"));
			title.add(JecnProperties.getValue("activityIndicatingThat"));
			title.add(JecnProperties.getValue("keyControlPoint"));

			// 获取操作说明数据
			Vector<Vector<String>> rowData = getContent(processOperationDialog.getFlowOperationBean()
					.getListKCPActivityShow() == null ? new ArrayList<JecnActivityShowBean>() : processOperationDialog
					.getFlowOperationBean().getListKCPActivityShow());
			return new JecnTableModel(title, rowData);
		}

	}

	class KeyContrPointAllowTable extends JecnActiveTable {
		public KeyContrPointAllowTable(ProcessOperationDialog processOperationDialog) {
			super(3, processOperationDialog);
		}

		@Override
		public JecnTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("activitiesNumbers"));
			title.add(JecnProperties.getValue("activityTitle"));
			title.add(JecnProperties.getValue("activityIndicatingThat"));
			title.add(JecnProperties.getValue("keyControlPointAllowStar"));

			// 获取操作说明数据
			Vector<Vector<String>> rowData = getContent(processOperationDialog.getFlowOperationBean()
					.getListKCPActivityAllowShow() == null ? new ArrayList<JecnActivityShowBean>()
					: processOperationDialog.getFlowOperationBean().getListKCPActivityAllowShow());
			return new JecnTableModel(title, rowData);
		}

	}

}
