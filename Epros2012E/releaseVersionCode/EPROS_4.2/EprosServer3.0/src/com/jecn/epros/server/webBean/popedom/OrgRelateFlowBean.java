package com.jecn.epros.server.webBean.popedom;

import java.util.HashMap;
import java.util.Map;

public class OrgRelateFlowBean {
	/** 部门Id */
	private long orgId;
	/** 部门名称 */
	private String orgName;
	/** 部门职责 */
	private String orgDuties;
	/** 相关流程 */
	private Map<Long, ProcessListBean> processMap = new HashMap<Long, ProcessListBean>();;
	/** 大小 */
	private int sizeTotal;
	/** 级别 */
	private int level = -1;
	/** 父节点ID */
	private long perOreId;

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgDuties() {
		return orgDuties;
	}

	public void setOrgDuties(String orgDuties) {
		this.orgDuties = orgDuties;
	}

	public Map<Long, ProcessListBean> getProcessMap() {
		return processMap;
	}

	public void setProcessMap(Map<Long, ProcessListBean> processMap) {
		this.processMap = processMap;
	}

	public int getSizeTotal() {
		return sizeTotal;
	}

	public void setSizeTotal(int sizeTotal) {
		this.sizeTotal = sizeTotal;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public long getPerOreId() {
		return perOreId;
	}

	public void setPerOreId(long perOreId) {
		this.perOreId = perOreId;
	}
}
