package epros.draw.gui.workflow.tabbedPane;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 全景面板,显示画图面板全景的类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnAllMapShowPanel extends JPanel implements MouseMotionListener {

	/** 全景面板宽高 */
	private int panelLength = 200;

	/** 点位画图面板框 */
	private JPanel pointPanel = new JPanel();
	/** 画图面板在全景图中的大小 */
	private int[] workflowArray = { 0, 0 };

	public JecnAllMapShowPanel() {
		initComponent();
	}

	private void initComponent() {
		// 边框
		this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED,
				Color.WHITE, Color.gray));
		// 大小
		this.setSize(panelLength, panelLength);
		// 布局管理器
		this.setLayout(null);

		// 点位画图面板框
		pointPanel.setBorder(BorderFactory.createLineBorder(Color.red));
		// 透明
		pointPanel.setOpaque(false);
		// 事件
		this.addMouseMotionListener(this);

		this.add(pointPanel);
	}

	@Override
	public void paintComponent(Graphics g) {
		// 画图面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (workflow == null) {
			return;
		}

		Graphics2D g2d = (Graphics2D) g;

		// 防止失真
		RenderingHints rhO = g2d.getRenderingHints();
		g2d.setRenderingHints(JecnUIUtil.getRenderingHints());

		// 映射对象
		AffineTransform at = g2d.getTransform();

		// 获取宽缩小比例
		double width = ((double) workflowArray[0]) / workflow.getWidth();
		width = DrawCommon.convertDouble(width);
		// 获得高缩小比例
		double height = ((double) workflowArray[1]) / workflow.getHeight();
		height = DrawCommon.convertDouble(height);

		// 设置缩放因子
		g2d.transform(AffineTransform.getScaleInstance(width, height));

		// 画图面板子组件集合
		ArrayList<JComponent> compList = new ArrayList<JComponent>();

		// 递归循环获取子组件，关闭使用缓冲区绘制标识
		paintWorkflowToThis(workflow, compList);

		// 执行映射操作，绘制画图面板以及其子组件
		workflow.paint(g2d);

		// 恢复是否使用缓存区绘制
		resetDoubleBuffered(compList);

		g2d.setRenderingHints(rhO);

		g2d.setTransform(at);
	}

	/**
	 * 
	 * 设置画图面板位置点框的位置点和大小
	 * 
	 * 
	 */
	public void setPointPanelBounds() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (workflow == null) {
			return;
		}
		// 获取画图面板在全景面板中大小
		workflowArray = getAffineWorkflowSize(workflow.getWidth(), workflow
				.getHeight(), panelLength, panelLength);

		setPointPanelLocation(workflow);
		setPointPanelSize(workflow);
	}

	/**
	 * 
	 * 设置定位画图面板框的位置点
	 * 
	 */
	private void setPointPanelLocation(JecnDrawDesktopPane workflow) {

		// 点击画图面板滚动条位置点
		double workX = workflow.getScrollPanle().getHorizontalScrollBar()
				.getValue();
		double workY = workflow.getScrollPanle().getVerticalScrollBar()
				.getValue();

		// 画图面板滚动条最大值
		double workMaxX = workflow.getScrollPanle().getHorizontalScrollBar()
				.getMaximum();
		double workMaxY = workflow.getScrollPanle().getVerticalScrollBar()
				.getMaximum();

		// 定位画图面板框的位置点
		int pointPanelX = DrawCommon.convertDoubleToInt((workX * workflowArray[0])
				/ workMaxX);
		int pointPanelY = DrawCommon.convertDoubleToInt((workY * workflowArray[1])
				/ workMaxY);

		this.pointPanel.setLocation(pointPanelX, pointPanelY);
	}

	/**
	 * 
	 * 设置定位画图面板框的大小
	 * 
	 */
	private void setPointPanelSize(JecnDrawDesktopPane workflow) {

		// 画图面板大小
		int workWidth = workflow.getWidth();
		int workHeight = workflow.getHeight();

		// 滚动窗体宽
		int viewWidth = workflow.getScrollPanle().getViewport().getWidth();
		// 滚动窗体高
		int viewHeight = workflow.getScrollPanle().getViewport().getHeight();

		// 画图面板在全景面板对应框的大小
		int pointWidth = Math.round((viewWidth * workflowArray[0]) / workWidth);
		int pointHeight = Math.round((viewHeight * workflowArray[1])
				/ workHeight);

		// 设置大小
		pointPanel.setSize(pointWidth, pointHeight);
	}

	/**
	 * 
	 * 拖动
	 * 
	 */
	public void mouseDragged(MouseEvent e) {
		mouseDraggedProcess(e);
	}

	public void mouseMoved(MouseEvent e) {
	}

	/**
	 * 
	 * 拖动处理
	 * 
	 * @param e
	 *            MouseEvent 鼠标事件
	 */
	private void mouseDraggedProcess(MouseEvent e) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (workflow == null) {
			return;
		}

		// 画图面板在全景图中的大小
		int affWidth = workflowArray[0];
		int affHeight = workflowArray[1];

		// 点位画图面板框的位置点
		int pX = e.getX() - pointPanel.getWidth() / 2;
		int pY = e.getY() - pointPanel.getHeight() / 2;

		// 点位画图面板框：位置点
		pointPanel.setLocation(pX, pY);

		// 相对于画图面板在全景图中的大小，点位画图面板框的位置点比例因子
		double x = ((double) pX) / affWidth;
		double y = ((double) pY) / affHeight;

		// 点位画图面板框的位置点对应的画图面板滚动条位置点
		double workX = x
				* workflow.getScrollPanle().getHorizontalScrollBar()
						.getMaximum();
		double workY = y
				* workflow.getScrollPanle().getVerticalScrollBar().getMaximum();

		// 设置滚动条位置点
		workflow.getScrollPanle().getHorizontalScrollBar().setValue(
				DrawCommon.convertDoubleToInt(workX));
		workflow.getScrollPanle().getVerticalScrollBar().setValue(
				DrawCommon.convertDoubleToInt(workY));
	}

	/**
	 * 
	 * 获取画图面板的缩小大小
	 * 
	 * @param workWidth
	 *            int 画图面板宽
	 * @param workHeight
	 *            int 画图面板高
	 * @param affWidth
	 *            int 全景面板宽
	 * @param affHeight
	 *            int 全景面板高
	 */
	private int[] getAffineWorkflowSize(int workWidth, int workHeight,
			int affWidth, int affHeight) {
		int[] workflowArray = { 0, 0 };

		// 换算后宽
		int width = Math.round((workWidth * affHeight) / workHeight);
		// 换算后高
		int height = Math.round((affWidth * workHeight) / workWidth);

		if ((workWidth < affWidth) && (workHeight < affHeight)) {
			workflowArray[0] = workWidth;
			workflowArray[1] = workHeight;
			return workflowArray;
		}
		if (height <= affHeight) {
			workflowArray[0] = affWidth;
			workflowArray[1] = height;
		} else {
			workflowArray[0] = width;
			workflowArray[1] = affHeight;
		}
		return workflowArray;
	}

	/**
	 * 
	 * 递归循环获取子组件，关闭使用缓冲区绘制标识
	 * 
	 * @param component
	 *            JComponent 画图面板
	 * @param compList
	 *            ArrayList<JComponent>
	 */
	private void paintWorkflowToThis(JComponent component,
			ArrayList<JComponent> compList) {
		if (component.isDoubleBuffered()) {
			compList.add(component);
			component.setDoubleBuffered(false);
		}
		for (int i = 0; i < component.getComponentCount(); i++) {
			Component c = component.getComponent(i);
			if (c instanceof JComponent) {
				paintWorkflowToThis((JComponent) c, compList);
			}
		}
	}

	/**
	 * 
	 * 恢复是否使用缓存区绘制
	 * 
	 * @param compList
	 *            ArrayList<JComponent>
	 */
	private void resetDoubleBuffered(ArrayList<JComponent> compList) {
		for (JComponent component : compList) {
			component.setDoubleBuffered(true);
		}
	}
}
