package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BasePentagon;

/**
 * 阶段分割符五边形
 * 
 * @author Administrator
 * 
 */
public class StageSeparatorPentagon extends BasePentagon {
	public StageSeparatorPentagon(JecnFigureData figureData) {
		super(figureData);
	}
}
