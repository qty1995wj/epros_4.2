
package com.jecn.webservice.zhongsh.datainput;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>anonymous complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="includeDeputy" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "includeDeputy"
})
@XmlRootElement(name = "GetAllUser2")
public class GetAllUser2 {

    protected boolean includeDeputy;

    /**
     * 获取includeDeputy属性的值。
     * 
     */
    public boolean isIncludeDeputy() {
        return includeDeputy;
    }

    /**
     * 设置includeDeputy属性的值。
     * 
     */
    public void setIncludeDeputy(boolean value) {
        this.includeDeputy = value;
    }

}
