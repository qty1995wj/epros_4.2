package wordxml.element.bean.paragraph;

/**
 * 水印属性
 * 
 * @author hyl
 */
public class AntiFakeBean {

	/**
	 * 字体大小自动
	 */
	private static final int FONTSIZE_AUTO = 1;
	/**
	 * 字体颜色银色
	 */
	private static final String COLOR_SILVER = "silver";
	/**
	 * 字体
	 */
	private static final String FONT_DEFULT = "Simsun";

	private String font = FONT_DEFULT;
	private int fontSize = FONTSIZE_AUTO;
	private String fontColor = COLOR_SILVER;
	/**
	 * 不透明性 true 不透明 false半透明
	 */
	private boolean fontColorOpacity = false;
	/**
	 * true 斜体 false水平
	 */
	private boolean italic = true;
	/**
	 * 旋转角度单位为度
	 */
	private float rotation = 315F;
	/**
	 * 透明性
	 */
	private float opacity = 0.5F;

	public static final AntiFakeBean DEFAULT = new AntiFakeBean();

	public AntiFakeBean(String font, int fontSize, String fontColor, boolean fontColorTranslucent, boolean italic) {
		this.font = font;
		this.fontSize = fontSize;
		this.fontColorOpacity = fontColorTranslucent;
		this.italic = italic;
	}

	public AntiFakeBean(String font, int fontSize) {
		this.font = font;
	}

	public AntiFakeBean() {
	}

	// 图形的宽高
	// width:390.35pt;height:195.15pt;
	// width:468pt;height:117pt
	public float getWidth() {
		if (fontSize == AntiFakeBean.FONTSIZE_AUTO) {
			return 390;
		}
		return 2 * fontSize;
	}

	public float getHeight() {
		if (fontSize == AntiFakeBean.FONTSIZE_AUTO) {
			return 195;
		}
		return fontSize;
	}

	public float getOpacity() {
		if (fontColorOpacity) {
			opacity = 1;
		}
		return opacity;
	}

	public float getRotation() {
		if (!italic) {
			rotation = 0;
		}
		return rotation;
	}

	// --------------

	public String getFont() {
		return font;
	}

	public void setFontColorOpacity(boolean fontColorOpacity) {
		this.fontColorOpacity = fontColorOpacity;
	}

	public void setFont(String font) {
		this.font = font;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public boolean isFontColorTranslucent() {
		return fontColorOpacity;
	}

	public void setFontColorTranslucent(boolean fontColorTranslucent) {
		this.fontColorOpacity = fontColorTranslucent;
	}

	public boolean isItalic() {
		return italic;
	}

	public void setItalic(boolean italic) {
		this.italic = italic;
	}

}
