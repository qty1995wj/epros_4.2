package com.jecn.epros.aspect;

import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.util.JsonUtils;
import com.jecn.epros.util.UserAgentUtils;
import eu.bitwalker.useragentutils.UserAgent;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class AccessLogAspect {

    private static final Logger logger = LoggerFactory.getLogger(AccessLogAspect.class);
    @Autowired
    private AccessDownloadRecord accessDownloadRecord;

    ThreadLocal<AccessLog> accessLog = new ThreadLocal<AccessLog>();

    @Pointcut("execution(public * com.jecn.epros.controller.*.*(..))")
    public void log() {
    }



    @Before(value = "log()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 获取HttpServletRequest
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        // 获取要记录的日志内容
        accessLog.set(getAccessLog(request, joinPoint));

        accessDownloadRecord.recordAccessDownload(request,joinPoint);
    }

    @AfterReturning(returning = "ret", pointcut = "log()")
    public void doAfterReturning(ResponseEntity<?> ret) throws Throwable {
        accessLog.get().setStatusCode(ret.getStatusCode().toString());
        logAndFlush();
    }

    @AfterThrowing(throwing = "e", pointcut = "log()")
    public void doAfterThrowing(Throwable e) throws Throwable {
        accessLog.get().setStatusCode("500");
        logAndFlush();
    }

    private void logAndFlush() {
        logger.info("{}", JsonUtils.getJson(accessLog.get()));
        accessLog.remove();
    }

    private AccessLog getAccessLog(HttpServletRequest request, JoinPoint joinPoint) {
        AccessLog log = new AccessLog();
        log.setVisitor(AuthenticatedUserUtil.getUserName());
        log.setUri(request.getRequestURI());
        log.setQueryString(request.getQueryString());
        log.setRemoteAddr(request.getRemoteAddr());
        log.setRemoteHost(request.getRemoteHost());
        log.setMethod(request.getMethod());
        log.setClassMethod(joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
//        log.setArgs(JsonUtils.getJson(joinPoint.getArgs()));

        // 用户使用的客户端信息
        UserAgent userAgent = UserAgentUtils.getUserAgentByRequest(request);
        log.setOperatingSystem(userAgent.getOperatingSystem().toString());
        log.setBrowser(userAgent.getBrowser().toString());
        return log;
    }
}
