package com.jecn.epros.server.action.web.propose;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.propose.JecnRtnlPropose;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeFile;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.propose.IRtnlProposeFileService;
import com.jecn.epros.server.service.propose.IRtnlProposeService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

public class RtnlProposeFileAction extends BaseAction {
	private static final Logger log = Logger.getLogger(RtnlProposeFileAction.class);

	private IRtnlProposeService rtnlProposeService;
	private IRtnlProposeFileService rtnlProposeFileService;

	/** 流程ID */
	private Long flowId;
	/** 上传的附件文件 */
	private File upload;

	/*** 上传的文件名称 */
	private String uploadFileName;
	/*** 上传的文件类型 */
	private String uploadContentType;

	/*** 编辑上传的文件 */
	private File editupload;
	/*** 上传的文件名称 */
	private String edituploadFileName;
	/*** 上传的文件类型 */
	private String edituploadContentType;

	/** 新建建议内容 */
	private String fileProposeContent;
	/** 编辑建议内容 */
	private String editfileProposeContent;
	/** 提交单条合理化建议，添加更新标识：addOrUpdateName ： 0：添加，1：更新 */
	private int addOrUpdateName = 0;
	// /**编辑单条合理化建议，添加更新标识：addOrUpdateName ： 0：添加，1：更新*/
	// private int eidtaddOrUpdateName = 0;
	/** 编辑单个合理化建议主键ID */
	private String editrtnlProposeId = null;

	/** 判断合理化建议是更新还是添加 0:添加，1：更新 */
	private int inflag = 0;

	/*** 合理化建议附件主键ID */
	private String proposeFileId;
	/** 下载文件名称 */
	private String downFileName;
	/** 下载文件信息 */
	private JecnRtnlProposeFile downFile;
	/** 是否删除合理化建议附件： 0： 否，1： 是 */
	private int isDeleteFileId = 0;
	/** 合理化建议附件编辑界面主键ID */
	private String editProposeFileId;
	/** 流程制度标识：0：流程；1：制度 */
	private int isFlowRtnl = 0;
	/** 判断是否存在原来的文件:0:不存在，1：存在 */
	private String isFlag = null;

	/***
	 * 上传合理化建议附件
	 */
	public String uploadRtnlProposeFile() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// HttpServletRequest request = getRequest();
		// web端用户登录bean
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		// 当前登录人主键ID
		long peopleId = webLoginBean.getJecnUser().getPeopleId();

		JecnRtnlPropose jecnRtnlPropose = null;
		JecnRtnlProposeFile jecnRtnlProposeFile = null;
		// 获取文件流
		byte[] buffer = getFileContant();

		if (addOrUpdateName == 0) {
			try {
				inflag = 0;
				// 合理化建议Bean
				jecnRtnlPropose = new JecnRtnlPropose();
				// 建议表设置主键
				jecnRtnlPropose.setId(JecnCommon.getUUID());
				jecnRtnlPropose.setRelationId(flowId);
				jecnRtnlPropose.setState(0);
				// jecnRtnlPropose.setIsEdit(0);
				jecnRtnlPropose.setProposeContent(fileProposeContent);
				jecnRtnlPropose.setCreatePersonId(peopleId);
				jecnRtnlPropose.setCreateTime(sdf.parse(sdf.format(new Date())));
				jecnRtnlPropose.setUpdatePersonId(peopleId);
				jecnRtnlPropose.setUpdateTime(sdf.parse(sdf.format(new Date())));
				// 合理化建议类型：0：流程；1：制度
				jecnRtnlPropose.setRtnlType(isFlowRtnl);
				// 合理化建议附件Bean
				jecnRtnlProposeFile = new JecnRtnlProposeFile();
				jecnRtnlProposeFile.setId(JecnCommon.getUUID());
				jecnRtnlProposeFile.setProposeId(jecnRtnlPropose.getId());

				jecnRtnlProposeFile.setCreatePersonId(peopleId);
				jecnRtnlProposeFile.setCreateTime(sdf.parse(sdf.format(new Date())));
				jecnRtnlProposeFile.setUpdatePersonId(peopleId);
				jecnRtnlProposeFile.setUpdateTime(sdf.parse(sdf.format(new Date())));
				if (uploadFileName != null) {
					jecnRtnlProposeFile.setFileName(uploadFileName);
					jecnRtnlProposeFile.setFileContants(buffer);
				}
			} catch (ParseException e) {
				log.error(e);
			}
		} else if (addOrUpdateName == 1) {
			try {
				inflag = 1;
				// 合理化建议
				jecnRtnlPropose = this.rtnlProposeService.getJecnRtnlPropose(editrtnlProposeId);
				jecnRtnlPropose.setState(0);
				// jecnRtnlPropose.setIsEdit(0);
				jecnRtnlPropose.setProposeContent(fileProposeContent);
				jecnRtnlPropose.setUpdatePersonId(peopleId);
				jecnRtnlPropose.setUpdateTime(new Date());
				// 合理化建议附件

				if (buffer != null) {
					jecnRtnlProposeFile = this.rtnlProposeService.getJecnRtnlProposeFile(editrtnlProposeId);
					if (jecnRtnlProposeFile != null) {
						jecnRtnlProposeFile.setFileName(uploadFileName);
						jecnRtnlProposeFile.setFileContants(buffer);
						jecnRtnlProposeFile.setUpdatePersonId(peopleId);
						jecnRtnlProposeFile.setUpdateTime(new Date());
					} else {
						if (buffer != null) {
							// 合理化建议附件Bean
							jecnRtnlProposeFile = new JecnRtnlProposeFile();
							// jecnRtnlProposeFile.setId(JecnCommon.getUUID());
							jecnRtnlProposeFile.setProposeId(jecnRtnlPropose.getId());

							jecnRtnlProposeFile.setCreatePersonId(peopleId);
							jecnRtnlProposeFile.setCreateTime(new Date());
							jecnRtnlProposeFile.setUpdatePersonId(peopleId);
							jecnRtnlProposeFile.setUpdateTime(new Date());
							if (uploadFileName != null) {
								jecnRtnlProposeFile.setFileName(uploadFileName);
								jecnRtnlProposeFile.setFileContants(buffer);
							}
						}
					}
				}

				// else {
				// if (isDeleteFileId == 1) {
				// rtnlProposeService
				// .deleteProposeFileById(editProposeFileId);
				// }
				// }
				if (isFlag != null && isFlag.equals("0") && buffer == null) {
					isDeleteFileId = 1;
				}
				jecnRtnlPropose.setIsDeleteFileId(isDeleteFileId);
				jecnRtnlPropose.setEditProposeFileId(editProposeFileId);

			} catch (Exception e) {
				log.error("获取单条合理化建议附件出错", e);
			}
		}
		try {
			if (jecnRtnlPropose != null) {
				// 添加合理化建议
				String isSuccess = rtnlProposeService.addRtnlPropose(jecnRtnlPropose, jecnRtnlProposeFile, inflag);
				outJsonString("{success:true,msg:1}");
			}
		} catch (Exception e) {
			log.error("添加合理化建议、合理化建议附件出错", e);
		}
		return null;
	}

	/**
	 * 获取上传文件输入流
	 * 
	 * @return byte[]
	 */
	private byte[] getFileContant() {
		if (upload == null) {
			log.info("没有要上传的文件");
			return null;
		}
		return JecnUtil.getByte(upload);
	}

	/**
	 * 获取上传文件输入流
	 * 
	 * @return byte[]
	 */
	private byte[] getEditFileContant() {
		if (editupload == null) {
			log.info("没有要上传的文件");
			return null;
		}
		return JecnUtil.getByte(editupload);
	}

	/***
	 * 合理化建议文件下载
	 * 
	 * @return
	 */
	public String downloadProposeFile() {
		try {
			downFile = rtnlProposeService.getPropoFileById(proposeFileId);
			downFileName = downFile.getFileName();
		} catch (Exception e) {
			log.error(e);
		}

		return SUCCESS;
	}

	/**
	 * 
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() {
		// 获得文件
		if (downFile == null && downFile.getFileContants() == null) {
			throw new IllegalArgumentException("getInputStream方法中参数为空!");
		}
		return new ByteArrayInputStream(downFile.getFileContants());
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public IRtnlProposeService getRtnlProposeService() {
		return rtnlProposeService;
	}

	public void setRtnlProposeService(IRtnlProposeService rtnlProposeService) {
		this.rtnlProposeService = rtnlProposeService;
	}

	public IRtnlProposeFileService getRtnlProposeFileService() {
		return rtnlProposeFileService;
	}

	public void setRtnlProposeFileService(IRtnlProposeFileService rtnlProposeFileService) {
		this.rtnlProposeFileService = rtnlProposeFileService;
	}

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public String getUploadContentType() {
		return uploadContentType;
	}

	public void setUploadContentType(String uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public File getEditupload() {
		return editupload;
	}

	public void setEditupload(File editupload) {
		this.editupload = editupload;
	}

	public String getEdituploadFileName() {
		return edituploadFileName;
	}

	public void setEdituploadFileName(String edituploadFileName) {
		this.edituploadFileName = edituploadFileName;
	}

	public String getEdituploadContentType() {
		return edituploadContentType;
	}

	public void setEdituploadContentType(String edituploadContentType) {
		this.edituploadContentType = edituploadContentType;
	}

	public String getFileProposeContent() {
		return fileProposeContent;
	}

	public void setFileProposeContent(String fileProposeContent) {
		this.fileProposeContent = fileProposeContent;
	}

	public String getEditfileProposeContent() {
		return editfileProposeContent;
	}

	public void setEditfileProposeContent(String editfileProposeContent) {
		this.editfileProposeContent = editfileProposeContent;
	}

	public int getAddOrUpdateName() {
		return addOrUpdateName;
	}

	public void setAddOrUpdateName(int addOrUpdateName) {
		this.addOrUpdateName = addOrUpdateName;
	}

	public String getEditrtnlProposeId() {
		return editrtnlProposeId;
	}

	public void setEditrtnlProposeId(String editrtnlProposeId) {
		this.editrtnlProposeId = editrtnlProposeId;
	}

	public String getProposeFileId() {
		return proposeFileId;
	}

	public void setProposeFileId(String proposeFileId) {
		this.proposeFileId = proposeFileId;
	}

	public String getDownFileName() {
		return JecnCommon.getDownFileNameByEncoding(getResponse(), downFileName, "");
	}

	public void setDownFileName(String downFileName) {
		this.downFileName = downFileName;
	}

	public int getIsDeleteFileId() {
		return isDeleteFileId;
	}

	public void setIsDeleteFileId(int isDeleteFileId) {
		this.isDeleteFileId = isDeleteFileId;
	}

	public String getEditProposeFileId() {
		return editProposeFileId;
	}

	public void setEditProposeFileId(String editProposeFileId) {
		this.editProposeFileId = editProposeFileId;
	}

	public int getIsFlowRtnl() {
		return isFlowRtnl;
	}

	public void setIsFlowRtnl(int isFlowRtnl) {
		this.isFlowRtnl = isFlowRtnl;
	}

	public String getIsFlag() {
		return isFlag;
	}

	public void setIsFlag(String isFlag) {
		this.isFlag = isFlag;
	}

}
