package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DBConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnPosGroup;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnPosGroupRelatedPos;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 
 * 岗位组同步
 * 
 * 
 */
public class ImportPosGroupByDB extends AbstractImportUser {

	protected final Log log = LogFactory.getLog(this.getClass());
	/** 部门 */
	private List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
	/** 人员 */
	private List<UserBean> userBeanList = new ArrayList<UserBean>();
	/** 岗位组 */
	private List<JecnPosGroup> posGroups = new ArrayList<JecnPosGroup>();
	/** 岗位组关联岗位 */
	private List<JecnPosGroupRelatedPos> groupRelatedPos = new ArrayList<JecnPosGroupRelatedPos>();
	/** 执行读取标识(true:表示执行过；false：标识没有执行过) */
	protected boolean readedFlag = false;

	public ImportPosGroupByDB(AbstractConfigBean configBean) {
		super(configBean);
	}

	/**
	 * 
	 * 获取部门数据集合
	 * 
	 * @return
	 */
	public List<DeptBean> getDeptBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();
		return deptBeanList;
	}

	/**
	 * 
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	public List<UserBean> getUserBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();

		return userBeanList;
	}

	/**
	 * 岗位组集合
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnPosGroup> getPosGroups() throws Exception {
		// 执行读取数据库数据
		readDB();
		return posGroups;
	}

	/**
	 * 岗位组相关岗位
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnPosGroupRelatedPos> getGroupRelatedPos() throws Exception {
		// 执行读取数据库数据
		readDB();
		return groupRelatedPos;
	}

	/**
	 * 读取数据库
	 * 
	 * @throws Exception
	 */
	private void readDB() throws Exception {
		// 执行过就不执行
		if (readedFlag) {
			return;
		}

		// Excel文件读取，部门、岗位、人员数据都全部取出来
		try {
			selectDB((DBConfigBean) configBean);
		} catch (Exception e) {
			log.error("", e);
			throw new IOException(SyncErrorInfo.SELECT_DB_FAIL);
		}
	}

	/**
	 * 
	 * 查询数据库返回查询结果
	 * 
	 * @param configBean
	 */
	private void selectDB(DBConfigBean configBean) throws Exception {
		// 部门查询返回记录集
		ResultSet resDept = null;
		// 人员查询返回记录集
		ResultSet resUser = null;
		// 岗位组相关岗位查询结果集
		ResultSet resPosGroupRefPos;
		// 岗位组相关岗位查询结果集
		ResultSet resPosGroup = null;
		Statement stmt = null;
		Connection conn = null;

		CallableStatement callStmt = null;
		try {
			// 加载的驱动类
			Class.forName(configBean.getDriver());
			// 创建数据库连接
			conn = DriverManager.getConnection(configBean.getUrl(), configBean.getUsername(), configBean.getPassword());

			// 创建一个Statement
			stmt = conn.createStatement();

			// sql校验
			if (StringUtils.isNotBlank(configBean.getValidateSql())) {
				ResultSet validateResult = stmt.executeQuery(configBean.getValidateSql());
				if (!syncValidate(validateResult)) {
					log.error("岗位组同步-获取数据库sync状态为 false，请查看数据源错误原因！");
					throw new IllegalArgumentException("岗位组同步-获取数据库sync状态为 false，请查看数据源错误原因！");
				}
			}

			// 执行查询，返回结果集
			// 部门
			resDept = stmt.executeQuery(configBean.getDeptSql());

			getDpetList(resDept);

			// 东航必须执行存储过程，否则没有权限访问视图
			if (StringUtils.isNotEmpty(configBean.getProcedureSql())) {
				callStmt = conn.prepareCall(configBean.getProcedureSql());
				Date curDate = JecnCommon.getDateByString(JecnCommon.getStringbyDate(new Date()));
				callStmt.setDate(1, new java.sql.Date(curDate.getTime()));
				callStmt.executeUpdate();
				log.info("存储过程执行成功！");
			}

			// 人员
			resUser = stmt.executeQuery(configBean.getUserSql());
			getUserList(resUser);

			if (StringUtils.isNotEmpty(configBean.getBasePosSql())) {
				// 获取基准岗位及与岗位关系
				resPosGroupRefPos = stmt.executeQuery(configBean.getBasePosSql());
				// 岗位组相关岗位
				getBasePosList(resPosGroupRefPos);
			}
			if (StringUtils.isNotEmpty(configBean.getPosGroupSql())) {
				resPosGroup = stmt.executeQuery(configBean.getPosGroupSql());
				getPosGroupList(resPosGroup);
			}
			readedFlag = true;
		} catch (ClassNotFoundException e) {
			log.error("ImportUserByDB类的selectDB方法加驱动时出错：" + e);
			throw e;
		} catch (SQLException e) {
			log.error("ImportUserByDB类的selectDB方法创建连接时出错：" + e);
			throw e;
		} finally {
			if (resUser != null) {// 关闭记录集
				try {
					resUser.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭人员记录集（ResultSet）时出错：" + e);
					throw e;
				}
			}
			if (resDept != null) {// 关闭记录集
				try {
					resDept.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭部门记录集（ResultSet）时出错：" + e);
					throw e;
				}
			}
			if (stmt != null) { // 关闭声明
				try {
					stmt.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭声明（Statement）时出错：" + e);
					throw e;
				}
			}
			if (conn != null) { // 关闭连接对象
				try {
					conn.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭连接对象（Connection）时出错：" + e);
					throw e;
				}
			}
			if (callStmt != null) {
				callStmt.close();
			}
		}
	}

	private boolean syncValidate(ResultSet resultSet) throws SQLException {
		if (JecnConfigTool.isLiBangLoginType()) {// 立邦
			while (resultSet.next()) {
				return "5".equals(resultSet.getString(1)) ? true : false;
			}
		}
		return true;
	}


	/**
	 * 岗位组解析
	 * 
	 * @param resPosGroup
	 */
	private void getPosGroupList(ResultSet resPosGroup) {
		if (JecnContants.otherLoginType == 28) {// 立邦
			getLiBangProGroupAttrs(resPosGroup);
		} else {
			getProGroupAttrs(resPosGroup);
		}
	}

	private void getProGroupAttrs(ResultSet resPosGroup) {
		try {
			JecnPosGroup posGroup = null;
			while (resPosGroup.next()) {
				posGroup = new JecnPosGroup();
				posGroup.setNum(resPosGroup.getString(1));
				posGroup.setName(resPosGroup.getString(2));
				posGroup.setParentNum(resPosGroup.getString(3));
				posGroup.setIsDir(Integer.valueOf(resPosGroup.getString(4)));
				posGroups.add(posGroup);
			}
		} catch (Exception e) {
			log.info("数据集转换发生错误! line98 ImportBasePos getBasePosList");
		}
	}

	private void getLiBangProGroupAttrs(ResultSet resPosGroup) {
		try {
			// BASEPOSITIONID BASEPOSITIONNAME, BU, UNITNAME_1, UNITNAME_2,
			// UNITNAME_3, UNITNAME_4, UNITNAME_5, UNITNAME_6
			List<String[]> lists = new ArrayList<String[]>();

			String[] strings = null;
			while (resPosGroup.next()) {
				strings = new String[9];
				strings[0] = resPosGroup.getString(1);
				strings[1] = resPosGroup.getString(2);
				strings[2] = resPosGroup.getString(3);
				strings[3] = resPosGroup.getString(4);
				strings[4] = resPosGroup.getString(5);
				strings[5] = resPosGroup.getString(6);
				strings[6] = resPosGroup.getString(7);
				strings[7] = resPosGroup.getString(8);
				strings[8] = resPosGroup.getString(9);
				lists.add(strings);
			}
			// 数据解析
			new HandlePosGroup().getPosGroupByLists(lists);
			// 解析字符串数组
		} catch (Exception e) {
			log.info("数据集转换发生错误! line98 ImportBasePos getBasePosList");
		}
	}

	class HandlePosGroup {
		private List<String> buList = new ArrayList<String>();
		private List<String> name1Lists = new ArrayList<String>();
		private List<String> name2Lists = new ArrayList<String>();
		private List<String> name3Lists = new ArrayList<String>();
		private List<String> name4Lists = new ArrayList<String>();
		private List<String> name5Lists = new ArrayList<String>();
		private List<String> name6Lists = new ArrayList<String>();

		/** PATH 分割标识 */
		private final String SPLIT_STR = "-";

		/**
		 * 解析数组，拼装岗位组数据 BASEPOSITIONID BASEPOSITIONNAME, BU, UNITNAME_1,
		 * UNITNAME_2, UNITNAME_3, UNITNAME_4, UNITNAME_5, UNITNAME_6
		 * 
		 * @param lists
		 * @return
		 * @throws Exception
		 */
		@SuppressWarnings("unchecked")
		public void getPosGroupByLists(List<String[]> lists) throws Exception {
			if (lists == null) {
				return;
			}
			JecnPosGroup posGroup = null;
			// 目录路径
			String pathName = "";
			for (String[] obj : lists) {
				if (obj[0] == null) {
					continue;
				}
				String posGroupId = obj[0];
				String posGroupName = obj[1];
				String bu = obj[2];
				String name1 = obj[3];
				String name2 = obj[4];
				String name3 = obj[5];
				String name4 = obj[6];
				String name5 = obj[7];
				String name6 = obj[8];

				pathName = bu;
				if (!buList.contains(bu)) {
					// 创建BU目录
					posGroup = new JecnPosGroup();
					posGroup.setNum(bu);
					posGroup.setName(bu);
					posGroup.setParentNum("0");
					posGroup.setIsDir(1);
					posGroups.add(posGroup);
					buList.add(bu);
				}

				pathName = posGroupHandle(name1Lists, name1, pathName, posGroupId, posGroupName);
				if (pathName == null) {
					continue;
				}
				pathName = posGroupHandle(name2Lists, name2, pathName, posGroupId, posGroupName);
				if (pathName == null) {
					continue;
				}
				pathName = posGroupHandle(name3Lists, name3, pathName, posGroupId, posGroupName);
				if (pathName == null) {
					continue;
				}
				pathName = posGroupHandle(name4Lists, name4, pathName, posGroupId, posGroupName);
				if (pathName == null) {
					continue;
				}
				pathName = posGroupHandle(name5Lists, name5, pathName, posGroupId, posGroupName);
				if (pathName == null) {
					continue;
				}
				pathName = posGroupHandle(name6Lists, name6, pathName, posGroupId, posGroupName);
				if (pathName == null) {
					continue;
				}
				if (pathName != null) {// 前六级都是目录
					// 创建岗位组
					posGroups.add(createPosGroup(posGroupId, posGroupName, pathName));
				}
			}
		}

		private String getParentNum(String pathName, String curName) {
			return pathName.substring(0, pathName.length() - curName.length() - 1);
		}

		private String posGroupHandle(List<String> nameLists, String name, String pathName, String posGroupId,
				String posGroupName) {
			if (name == null) {
				// 创建岗位组
				posGroups.add(createPosGroup(posGroupId, posGroupName, pathName));
				return null;
			}
			pathName += SPLIT_STR + name;
			if (!nameLists.contains(pathName)) {
				nameLists.add(pathName);
				// 创建目录
				posGroups.add(createPosGroupDir(pathName, name));
			}

			return pathName;
		}

		private JecnPosGroup createPosGroup(String posGroupId, String posGroupName, String pathName) {
			JecnPosGroup posGroup = new JecnPosGroup();
			posGroup.setNum(posGroupId);
			posGroup.setName(posGroupName);
			posGroup.setParentNum(pathName);
			posGroup.setIsDir(0);
			return posGroup;
		}

		private JecnPosGroup createPosGroupDir(String pathName, String name) {
			JecnPosGroup posGroup = new JecnPosGroup();
			posGroup.setNum(pathName);
			posGroup.setName(name);
			posGroup.setParentNum(getParentNum(pathName, name));
			posGroup.setIsDir(1);
			return posGroup;
		}
	}

	private List<String> existsOrgNums = new ArrayList<String>();

	/**
	 * 
	 * 把数据取出结果集存储到bean对象中
	 * 
	 * @param resDept
	 * @throws SQLException
	 */
	protected void getDpetList(ResultSet resDept) throws SQLException {
		while (resDept.next()) {
			DeptBean deptBean = new DeptBean();
			// 部门编号
			String deptNum = resDept.getString(1);
			// 部门名称
			String deptName = resDept.getString(2);
			// 上级部门编号
			String perDeptNum = resDept.getString(3);
			// 部门编号
			deptBean.setDeptNum(SyncTool.emtryToNull(deptNum));
			// 部门名称
			deptBean.setDeptName(SyncTool.emtryToNull(deptName));
			// 上级部门编号
			deptBean.setPerDeptNum(SyncTool.emtryToNull(perDeptNum));

			existsOrgNums.add(deptNum);
			this.deptBeanList.add(deptBean);
		}
	}

	/**
	 * 
	 * 把数据取出结果集存储到bean对象中
	 * 
	 * @param resUser
	 * @throws SQLException
	 */
	protected void getUserList(ResultSet resUser) throws SQLException {
		boolean isLibang = JecnConfigTool.isLiBangLoginType();
		while (resUser.next()) {
			UserBean userBean = new UserBean();

			// 下面的顺序不要轻易改变
			// 登录名称
			String userNum = resUser.getString(1);
			// 真实姓名
			String trueName = resUser.getString(2);
			// 任职岗位编号
			String posNum = resUser.getString(3);
			// 任职岗位名称
			String posName = resUser.getString(4);
			// 岗位所属部门编号
			String deptNum = resUser.getString(5);
			// 邮箱地址
			String email = resUser.getString(6);
			// 内外网邮件标识(内网:0 外网:1)
			String emailType = resUser.getString(7);
			// 联系电话
			String phone = resUser.getString(8);

			// 登录名称
			userBean.setUserNum(SyncTool.emtryToNull(userNum));
			// 真实姓名
			userBean.setTrueName(SyncTool.emtryToNull(trueName));
			// 任职岗位编号
			userBean.setPosNum(SyncTool.emtryToNull(posNum));
			// 任职岗位名称
			userBean.setPosName(SyncTool.emtryToNull(posName));
			// 岗位所属部门编号
			userBean.setDeptNum(SyncTool.emtryToNull(deptNum));
			// 邮箱地址
			userBean.setEmail(email);
			// 邮箱、邮箱类型不等于空且是“0”，“1”
			if (SyncTool.isEmailTypeStr(emailType)) {
				// 内外网邮件标识(内网:0 外网:1)
				userBean.setEmailType(Integer.valueOf(emailType));
			}
			// 联系电话
			userBean.setPhone(SyncTool.emtryToNull(phone));

			if (isLibang && !existsOrgNums.contains(deptNum)) {// 部门编号在部门集合中不存在,立邦的情况下过滤掉
				userBean.setPosNum("");
				userBean.setPosName("");
				userBean.setDeptNum("");
			}
			this.userBeanList.add(userBean);
		}

	}

	/**
	 * 
	 * 将数据集合添加到BasePosBean中
	 * 
	 * @author cheshaowei
	 * @param ResultSet
	 *            jdbc查询结果集
	 * 
	 * 
	 * **/
	private void getBasePosList(ResultSet resBasePos) throws SQLException {
		try {
			JecnPosGroupRelatedPos posGroupRelatedPos = null;
			while (resBasePos.next()) {
				// 基准岗位编号
				String posGroupNum = resBasePos.getString(1);
				// 岗位编号
				String posNum = resBasePos.getString(4);
				// 基准岗位和岗位对应Bean
				posGroupRelatedPos = new JecnPosGroupRelatedPos();
				posGroupRelatedPos.setPosNum(posNum);
				posGroupRelatedPos.setPosGroupNum(posGroupNum);
				this.groupRelatedPos.add(posGroupRelatedPos);
			}
		} catch (Exception e) {
			log.info("数据集转换发生错误! line98 ImportBasePos getBasePosList");
		}
	}

}
