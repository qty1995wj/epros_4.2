package epros.designer.gui.integration.internalControl;

import java.util.Date;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 编辑条款
 * 
 * @author ZHF
 * 
 */
public class EditClauseDialog extends JecnClauseDialog {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(EditClauseDialog.class);

	/** 树节点 */
	private JecnTreeNode pNode = null;
	/** 树 */
	private JTree jTree = null;

	public EditClauseDialog(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		try {
			this.setName(pNode.getJecnTreeBean().getName());
			this.getCenterText().setText((pNode.getJecnTreeBean().getContent()));
		} catch (Exception e) {
			log.error("EditClauseDialog EditClauseDialog is error！", e);
		}
	}

	/** 窗口名称 */
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("editClause");
	}

	/** 修改数据 */
	@Override
	public void saveData() {
		String name = this.getNameTextField().getText();
		String description = this.getCenterText().getText();
		Long updatePerson = JecnConstants.loginBean.getJecnUser().getPeopleId();
		try {
			ConnectionPool.getControlGuideAction().updateJecnControlGuide(pNode.getJecnTreeBean().getId(), name,
					description, updatePerson, new Date());
			JecnTreeCommon.reNameNode(jTree, pNode, this.getName());
			pNode.getJecnTreeBean().setContent(description);
			this.dispose();
		} catch (Exception e) {
			log.error("EditClauseDialog saveData is error！", e);
		}
	}

	/** 判断是否重名 */
	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return JecnTreeCommon.validateRepeatNameEidt(name, pNode);
	}

	@Override
	protected void cancelButtonAction() {
		this.dispose();
	}

}
