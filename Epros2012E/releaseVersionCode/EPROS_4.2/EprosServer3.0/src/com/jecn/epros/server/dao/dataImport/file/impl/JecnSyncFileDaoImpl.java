package com.jecn.epros.server.dao.dataImport.file.impl;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.dataImport.file.IJecnSyncFileDao;

public class JecnSyncFileDaoImpl extends AbsBaseDao<String, String> implements IJecnSyncFileDao {

	@Override
	public void updateFileNameByOrg() throws Exception {
		String sql = updateFileNameByOrgSql("_T");
		this.execteNative(sql);
		sql = updateFileNameByOrgSql("");
		this.execteNative(sql);
		this.getSession().flush();
	}

	private String updateFileNameByOrgSql(String isPub) throws Exception {
		return "UPDATE JECN_FILE"
				+ isPub
				+ " T"
				+ "   SET T.FILE_NAME ="
				+ "       (SELECT O.ORG_NAME"
				+ "          FROM JECN_FLOW_ORG O"
				+ "         WHERE O.ORG_NUMBER = T.ORG_NUM)"
				+ " WHERE T.DEL_STATE = 0"
				+ "   AND T.ORG_NUM IS NOT NULL  AND EXISTS (SELECT 1 FROM JECN_FLOW_ORG O WHERE O.ORG_NUMBER = T.ORG_NUM)";
	}
}
