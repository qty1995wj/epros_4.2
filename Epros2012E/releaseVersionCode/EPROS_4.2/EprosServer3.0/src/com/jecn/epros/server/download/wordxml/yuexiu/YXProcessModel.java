package com.jecn.epros.server.download.wordxml.yuexiu;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.LineType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.BorderBean;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.common.ShdBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.GroupBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.GroupBean.GroupAlign;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.bean.paragraph.GroupBean.Hanchor;
import wordxml.element.bean.paragraph.GroupBean.Vanchor;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ActiveItSystem;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 越秀操作说明下载
 * 
 * @author admin
 * 
 */
public class YXProcessModel extends ProcessFileModel {
	/*** 段落行距1.5倍行距 ****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);

	private ParagraphLineRule vLine = new ParagraphLineRule(1F, LineRuleType.AUTO);
	private ParagraphLineRule ct = new ParagraphLineRule(1.73F, LineRuleType.AUTO);

	public YXProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		getDocProperty().setFlowChartScaleValue(6.5F);
		setDocStyle("、", textTitleStyle());
		getDocProperty().setNewTblWidth(16F);
		getDocProperty().setTblTitleShadow(true);
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, vLine);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getTFSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(2.54F, 3.17F, 2.54F, 3.17F, 0.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.76F, 1.76F, 1.76F, 1.76F, 1.27F, 1.27F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);

	}

	/**
	 * 添加封面节点封皮内容
	 * 
	 * @param sect
	 */
	private void addTitleSectContent(Sect sect) {
		sect.createMutilEmptyParagraph(6);
		createLine(sect, 70F, 35F, 430F, 35F);
		// sect.createMutilEmptyParagraph(1);
		GroupBean groupBean = new GroupBean();
		groupBean.setHorizontal(GroupAlign.center, Hanchor.page, 10).setHorizontal(2.25F, Hanchor.margin, 10)
				.setVertical(8F, Vanchor.page, 0)
				// .setVertical(GroupVlign.center, Vanchor.page, 1)
				.setSize(GroupW.exact, 16.43, GroupH.exact, 3.36);

		Group group = sect.createGroup(groupBean);
		ParagraphBean p = new ParagraphBean(Align.center, "微软雅黑", Constants.erhao, true);
		p.setSpace(0F, 0F, new ParagraphLineRule(2, LineRuleType.AUTO));
		group.createParagraph("越秀地产流程文件", p);
		group.createParagraph(processDownloadBean.getFlowName(), p);

		createLine(sect, 70F, 133F, 430F, 133F);

		sect.createMutilEmptyParagraph(4);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "流程编号", nullToEmpty(processDownloadBean.getFlowInputNum()) });
		rowData.add(new String[] { "文档版本", nullToEmpty(processDownloadBean.getVersion()) });
		rowData.add(new String[] { "生效日", processDownloadBean.getPubDate("yyyy年MM月dd日 ") });

		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(new BorderBean(LineType.SINGLE, 0.5f, Color.GRAY));
		tblStyle.setCellMargin(0, 0.19F, 0, 0.19F);
		tblStyle.setTableAlign(Align.right);
		Table table = new Table(tblStyle, new float[] { 2.44F, 4.81F });
		ParagraphBean pb = new ParagraphBean(Align.left, "微软雅黑", Constants.xiaosi, false);
		pb.getFontBean().setFontColor(Color.GRAY);
		table.createTableRow(rowData, pb);
		table.setValign(Valign.center);
		sect.addTable(table);
		sect.addParagraph(new Paragraph(true));
	}

	/**
	 * 关键活动
	 * 
	 * @param _count
	 * @param name
	 * @param keyActivityShowList
	 */
	protected void a07(ProcessFileItem processFileItem, List<KeyActivityBean> activeList) {
		if (activeList == null || activeList.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 活动编号
		String number = "关键控制点编号";
		// 活动名称
		String title = "关键控制点名称";
		// 重要说明
		String keyExplain = "控制措施";

		List<String[]> dataList = new ArrayList<String[]>();
		// 标题行
		dataList.add(new String[] { number, title, keyExplain });
		if (activeList != null) {
			for (KeyActivityBean keyActive : activeList) {
				if (!"3".equals(keyActive.getActiveKey())) {
					continue;
				}
				String[] rowData = new String[3];
				rowData[0] = keyActive.getActivityNumber();
				rowData[1] = keyActive.getActivityName();
				rowData[2] = keyActive.getActivityShowControl();
				dataList.add(rowData);
			}
		}
		float[] width = new float[] { 3.25F, 3.5F, 8.75F };
		Table tbl = createTableItem(processFileItem, width, dataList);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow());
	}

	private void createLine(Sect titleSect, float startX, float startY, float endX, float endY) {
		LineGraph line = new LineGraph(LineStyle.single, 1.5F);
		line.setFrom(startX, startY);
		line.setTo(endX, endY);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		positionBean.setMarginRight(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(Sect sect, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(sect, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		Table table = null;
		Paragraph p = null;
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.wuhao, false);
		List<String[]> rowData = new ArrayList<String[]>();
		Header hdr = sect.createHeader(type);
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(1F);
		rowData.add(new String[] { "", "公司制度流程文件", "编号：" + nullToEmpty(processDownloadBean.getFlowInputNum()) });
		rowData.add(new String[] { "", processDownloadBean.getFlowName(), "" });
		table = JecnWordUtil.createTab(hdr, tabBean, new float[] { 5F, 6F, 4.21F }, rowData, pBean);

		for (int i = 0; i < table.getRowCount(); i++) {
			table.getRow(i).setHeight(1.2F);
		}

		setParagraphBean(table.getRow(1).getCell(1).getParagraphs(), new ParagraphBean(Align.center, "黑体",
				Constants.sihao, false));
		setParagraphBean(table.getRow(0).getCell(2).getParagraphs(), new ParagraphBean(Align.left, "黑体",
				Constants.wuhao, false));

		table.getRow(0).getCell(0).setRowspan(2);
		table.getRow(0).getCell(2).setRowspan(2);

		/***** 公司logo图标 ****/
		Image logo = (Image) super.getLogoImage(4F, 1.03F);
		PositionBean positionBean = new PositionBean(PositionType.absolute, 15.05F, 0, 13.7F, 0);
		logo.setPositionBean(positionBean);
		table.getRow(0).getCell(0).getParagraph(0).appendGraphRun(logo);

		TableCell cell = table.getRow(0).getCell(2);
		cell.createParagraph(new String[] { "版本：" + nullToEmpty(processDownloadBean.getFlowVersion()), "" },
				new ParagraphBean(Align.left, "黑体", Constants.wuhao, false));
		p = cell.getParagraph(cell.getParagraphs().size() - 1);
		p.appendTextRun("页码：第");
		p.appendCurPageRun();
		p.appendTextRun("页，共 ");
		p.appendTotalPagesRun(0);
		p.appendTextRun("页");

		table.setValign(Valign.center);

	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.center, "Arial", Constants.xiaowu, false);
		Footer ftr = sect.createFooter(type);
		Paragraph paragraph = ftr.createParagraph("", pBean);
		paragraph.appendCurPageRun();
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		firstSect.setSectBean(getSectBean());
		createCommhdrftr(firstSect);
	}

	/**
	 * 创建人所在的部门
	 * 
	 * @return
	 */
	private String createOrg() {
		Long id = processDownloadBean.getFlowStructure() == null ? processDownloadBean.getFlowStructureT()
				.getPeopleId() : processDownloadBean.getFlowStructure().getPeopleId();
		String sql = "select distinct jupr.figure_id, jfoi.figure_text, jfo.org_name"
				+ "  from jecn_user_position_related jupr," + "       jecn_flow_org_image        jfoi,"
				+ "       jecn_flow_org              jfo" + " where jupr.people_id = ?"
				+ "   and jupr.figure_id = jfoi.figure_id" + "   and jfoi.org_id = jfo.org_id"
				+ "   and jfo.del_state = 0";

		List<Object[]> objs = processDownloadBean.getBaseDao().listNativeSql(sql, id);
		if (JecnUtil.isEmpty(objs)) {
			return "";
		}
		List<String> names = new ArrayList<String>();
		for (Object[] obj : objs) {
			String name = nullToEmpty(obj[2]);
			if (StringUtils.isBlank(name)) {
				continue;
			}
			names.add(name);
		}
		return JecnUtil.concat(names, "/");
	}

	private String getLastApprovePeople() {
		JecnTaskHistoryNew historyNew = processDownloadBean.getLatestHistoryNew();
		if (historyNew == null) {
			return "";
		}
		List<JecnTaskHistoryFollow> fllows = historyNew.getListJecnTaskHistoryFollow();
		if (JecnUtil.isEmpty(fllows)) {
			return "";
		}
		return nullToEmpty(fllows.get(fllows.size() - 1).getName());
	}

	private String getFirstPubTime(String f) {
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		if (JecnUtil.isEmpty(historys)) {
			return "";
		}
		return JecnUtil.DateToStr(historys.get(historys.size() - 1).getPublishDate(), f);
	}

	protected void a10(ProcessFileItem processFileItem, String[] ts, List<String[]> rd) {
		processFileItem.setBelongTo(docProperty.getFlowChartSect());
		docProperty.getFlowChartSect().createMutilEmptyParagraph(1);
		// 标题
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();

		// ---------标题
		List<String> titles = new ArrayList<String>();
		List<List<String>> datas = new ArrayList<List<String>>();
		// 活动编号
		titles.add("节点编号");
		// 活动名称
		titles.add("节点名称");
		titles.add("责任部门");
		titles.add("责任岗位");
		// 活动说明
		titles.add("业务指引");
		titles.add("处理时限(H)");
		// 输入
		titles.add("输入文档");
		// 输出
		titles.add("输出文档");
		// 信息化
		titles.add("实现方式");
		// 操作规范、相关标准
		titles.add("相关附件（工具/模板） ");

		// --------内容
		Map<Long, String> idToKey = new HashMap<Long, String>();
		if (processFileItem.getDataBean().getConfig().isActivityContentHasKeyContent()) {
			List<KeyActivityBean> keyActivityShowList = processFileItem.getDataBean().getKeyActivityShowList();
			for (KeyActivityBean k : keyActivityShowList) {
				idToKey.put(k.getId(), k.getActivityShowControl());
			}
		}
		List<String[]> dataRows = JecnWordUtil.obj2String(processFileItem.getDataBean().getAllActivityShowList());
		Map<String, List<String>> activeIdToITMap = getActiveIdToITMap();
		for (String[] data : dataRows) {
			String content = JecnUtil.nullToEmpty(data[3]);
			if (processFileItem.getDataBean().getConfig().isActivityContentHasKeyContent()) {
				String keyContent = JecnUtil.nullToEmpty(idToKey.get(Long.valueOf(data[8].toString())));
				if (StringUtils.isNotBlank(content) && StringUtils.isNotBlank(content)) {
					content = content + "\n" + keyContent;
				} else {
					content = content + keyContent;
				}
			}
			// 普通数据源 活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出 6办理时限目标值 7办理时限原始值
			// 8活动id
			// * 9角色id
			List<String> row = new ArrayList<String>();
			row.add(data[0]);
			row.add(data[2]);
			// 部门-岗位=执行角色
			String[] arr = getOrgPos(data[1]);
			row.add(arr[0]);
			row.add(arr[1]);
			row.add(content);
			// 现状目标
			String targetValue = StringUtils.isBlank(JecnUtil.objToStr(data[6])) ? "" : JecnUtil.objToStr(data[6]);
			String statusValue = StringUtils.isBlank(JecnUtil.objToStr(data[7])) ? "" : JecnUtil.objToStr(data[7]);
			String timeLineValue = ProcessFileModel.concatTimeLineValue(targetValue, statusValue);
			row.add(targetValue);
			// 输入
			row.add(data[4]);
			// 输出
			row.add(data[5]);
			// 信息化
			List<String> its = activeIdToITMap.get(data[8]);
			if (its != null && its.size() > 0) {
				row.add(JecnUtil.concat(its, "/"));
			} else {
				row.add("");
			}
			row.add(getRelatedFile(Long.valueOf(data[8].toString())));
			datas.add(row);
		}

		// 添加标题
		rowData.add(new String[] { "节点编号", "节点名称", "处理人员" });
		int columnSize = titles.size();
		rowData.add(titles.toArray(new String[columnSize]));
		for (List<String> data : datas) {
			rowData.add(data.toArray(new String[columnSize]));
		}
		Table tbl = new Table(tblStyle(), new float[] { 1.23F, 2.23F, 1.73F, 1.49F, 6.42F, 1.25F, 2F, 2F, 2F, 4F });
		tbl.createTableRow(rowData, tblContentStyle());
		tbl.getRow(0).getCell(0).setRowspan(2);
		tbl.getRow(0).getCell(1).setRowspan(2);
		tbl.getRow(0).getCell(2).setColspan(2);
		tbl.getRow(0).getCell(2).setVmergeBeg(true);
		// tbl.getRow(0).getCell(3).setVmerge(true);
		// 设置第一行居中
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
		tbl.setRowStyle(1, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
		docProperty.getFlowChartSect().addTable(tbl);
		tbl.setValign(Valign.center);
	}

	/**
	 * 通过活动ID获得相关的操作规范和标准
	 * 
	 * @param id
	 * @return
	 */
	private String getRelatedFile(Long id) {
		String sql = "select distinct jf.file_name as name" + "  from JECN_FLOW_STRUCTURE_T jfs"
				+ " inner join JECN_FLOW_STRUCTURE_IMAGE_T jfsi" + "    on jfs.flow_id = jfsi.FLOW_ID"
				+ "   and jfs.del_state = 0" + " inner join JECN_ACTIVITY_FILE_T jaf"
				+ "    on jaf.FIGURE_ID = jfsi.FIGURE_ID" + "   and jaf.FILE_TYPE = 1" + " inner join jecn_file_T jf"
				+ "    on jaf.FILE_S_ID = jf.file_id" + "   and jf.del_state = 0" + " inner join file_content fi"
				+ "    on fi.id = jf.version_id" + " where jfsi.figure_id=?" + "UNION"
				+ " select distinct JCC.CRITERION_CLASS_NAME as name" + "  from JECN_FLOW_STRUCTURE_T flows"
				+ " inner join JECN_FLOW_STRUCTURE_IMAGE_T jfsi" + "    on flows.flow_id = jfsi.FLOW_ID"
				+ " inner join JECN_ACTIVE_STANDARD_T JAS" + "    on jfsi.FIGURE_ID = JAS.ACTIVE_ID"
				+ " inner join JECN_CRITERION_CLASSES jcc" + "    on jcc.CRITERION_CLASS_ID = jas.STANDARD_ID"
				+ " where jfsi.figure_id=?";
		List<String> names = processDownloadBean.getBaseDao()
				.listObjectNativeSql(sql, "name", Hibernate.STRING, id, id);
		return JecnUtil.concat(names, "\n");
	}

	private String[] getOrgPos(String a) {
		String[] result = new String[] { "", "" };
		if (StringUtils.isBlank(a)) {
			return result;
		}
		String[] ss = a.split("-");
		if (ss.length == 1) {
			result[0] = ss[0];
		} else if (ss.length == 2) {
			result[0] = ss[0];
			result[1] = ss[1];
		}
		return result;
	}

	private Map<String, List<String>> getActiveIdToITMap() {
		List<ActiveItSystem> activeItSystems = processDownloadBean.getActiveItSystems();
		Map<String, List<String>> m = new HashMap<String, List<String>>();
		for (ActiveItSystem s : activeItSystems) {
			List<String> ls = JecnUtil.getElseAddList(m, s.getActiveId().toString());
			ls.add(s.getSystemName());
		}
		return m;
	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		word.paragraphStyleKpi(processFileItem, oldRowData);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(2.5F, 2F, 1.1F, 2F, 0.5F, 0.5F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommhdrftr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getTFSectBean());
		addTitleSectContent(titleSect);
		addSecondContent(titleSect);
		createTFhdrftr(titleSect);
	}

	private void addSecondContent(Sect firstSect) {
		ParagraphBean paragraphBean = new ParagraphBean(Align.center, "微软雅黑", Constants.shihao, false);
		paragraphBean.getFontBean().setI(true);
		Table table = new Table(tblStyle(), new float[] { 2.69F, 5F, 2.69F, 5F });
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "流程编号", nullToEmpty(processDownloadBean.getFlowInputNum()), "流程名称",
				processDownloadBean.getFlowName() });
		rowData.add(new String[] { "版本号", nullToEmpty(processDownloadBean.getVersion()), "版本日期 ",
				processDownloadBean.getPubDate("yyyy-MM-dd") });
		rowData.add(new String[] { "流程发布日期 ", getFirstPubTime("yyyy-MM-dd"), "流程实施日期 ",
				processDownloadBean.getImplementDate("yyyy-MM-dd") });
		rowData.add(new String[] { "流程起草人", nullToEmpty(processDownloadBean.getCreateName()), "流程批准人 ",
				getLastApprovePeople() });
		table.createTableRow(rowData, paragraphBean);
		firstSect.addTable(table);
		for (int i = 0; i < table.getRowCount(); i++) {
			TableRow row = table.getRow(i);
			row.getCell(0).getCellBean().setShdBean(ShdBean.getInstance());
			row.getCell(2).getCellBean().setShdBean(ShdBean.getInstance());
		}
		ParagraphBean pp = new ParagraphBean(Align.left, "微软雅黑", Constants.shihao, false);
		table.setCellStyle(1, pp);
		table.setCellStyle(3, pp);

		table = new Table(tblStyle(), new float[] { 2.69F, 12.69F });
		rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "流程管理责任部门 ", createOrg() });
		rowData.add(new String[] { "关键控制点说明", getKCP() });
		table.createTableRow(rowData, paragraphBean);
		firstSect.addTable(table);
		table.setCellStyle(1, pp);
		for (int i = 0; i < table.getRowCount(); i++) {
			TableRow row = table.getRow(i);
			row.getCell(0).getCellBean().setShdBean(ShdBean.getInstance());
		}
	}

	private String getKCP() {
		List<KeyActivityBean> keyActivityShowList = processDownloadBean.getKeyActivityShowList();
		List<String> names = new ArrayList<String>();
		for (KeyActivityBean b : keyActivityShowList) {
			String control = b.getActivityShowControl();
			if (StringUtils.isBlank(control)) {
				continue;
			}
			names.add(control);
		}
		return JecnUtil.concat(names, "\n");
	}

	private void createTFhdrftr(Sect sect) {
		createTFftr(sect);
		createCommhdr(sect, HeaderOrFooterType.odd);
	}

	private void createTFftr(Sect sect) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.left, "宋体 (中文正文)", Constants.xiaowu, false);
		Footer ftr = sect.createFooter(HeaderOrFooterType.odd);
		Paragraph paragraph = ftr.createParagraph(
				"越秀地产流程信息部                                                                         ", pBean);
		paragraph.appendCurPageRun();
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体", Constants.shihao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0, 0.19F, 0, 0.19F);
		// // 表格左缩进0.56厘米
		// tblStyle.setTableLeftInd(0.56F);
		tblStyle.setTableAlign(Align.center);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.shihao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体 (中文正文)", Constants.xiaosi, false);
		textContentStyle.setSpace(0F, 0F, new ParagraphLineRule(20F, LineRuleType.EXACT));
		textContentStyle.setInd(0.52F, 0F, 0F, 0F);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, "宋体 (中文标题)", Constants.xiaosi, true);
		textTitleStyle.setSpace(1f, 1f, ct);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

}
