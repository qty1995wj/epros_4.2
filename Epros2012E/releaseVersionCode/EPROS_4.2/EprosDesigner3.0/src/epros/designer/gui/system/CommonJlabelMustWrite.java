package epros.designer.gui.system;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;

import epros.designer.util.JecnProperties;

public class CommonJlabelMustWrite extends JLabel {
	public CommonJlabelMustWrite() {
		this.setText(JecnProperties.getValue("required"));
		this.setForeground(Color.red);
		this.setFont(new Font("宋体", Font.PLAIN, 12));
	}
}
