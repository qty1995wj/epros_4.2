package com.jecn.epros.bean.external;

import io.swagger.annotations.ApiParam;

import java.io.Serializable;
import java.util.Date;

public class TaskPeople implements Serializable{
    @ApiParam("登录名称，也就是人员的唯一标识")
    private String loginName;
    @ApiParam("真实姓名")
    private String trueName;
    @ApiParam("审批日期，取最新的审批日期")
    private Date approveTime;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public Date getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(Date approveTime) {
        this.approveTime = approveTime;
    }
}
