package epros.draw.gui.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicTableHeaderUI;
import javax.swing.table.TableCellRenderer;

import sun.swing.table.DefaultTableCellHeaderRenderer;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 自定义JTable表头UI
 * 
 * @author ZHOUXY
 * 
 */
public class JecnBasicTableHeaderUI extends BasicTableHeaderUI {
	/** 渲染器 */
	private TableCellRenderer originalHeaderRenderer;
	/** 内容位置 */
	private int textPostion = SwingConstants.CENTER;

	public JecnBasicTableHeaderUI() {
	}

	/**
	 * 
	 * 制定内容位置
	 * 
	 * @param alignment
	 *            One of the following constants defined in
	 *            <code>SwingConstants</code>: <code>LEFT</code>,
	 *            <code>CENTER</code> (the default for image-only labels),
	 *            <code>RIGHT</code>, <code>LEADING</code> (the default for
	 *            text-only labels) or <code>TRAILING</code>.
	 * 
	 */
	public JecnBasicTableHeaderUI(int textPostion) {
		this.textPostion = textPostion;
	}

	public void installUI(JComponent c) {
		super.installUI(c);

		originalHeaderRenderer = header.getDefaultRenderer();
		if (originalHeaderRenderer instanceof UIResource) {
			header.setDefaultRenderer(new JecnDefaultTableCellHeaderRenderer());
		}
	}

	public void uninstallUI(JComponent c) {
		if (header.getDefaultRenderer() instanceof JecnDefaultTableCellHeaderRenderer) {
			header.setDefaultRenderer(originalHeaderRenderer);
		}
		super.uninstallUI(c);
	}

	@Override
	protected void rolloverColumnUpdated(int oldColumn, int newColumn) {
		header.repaint(header.getHeaderRect(oldColumn));
		header.repaint(header.getHeaderRect(newColumn));
	}

	/**
	 * 
	 * 渲染器
	 * 
	 * @author ZHOUXY
	 * 
	 */
	private class JecnDefaultTableCellHeaderRenderer extends
			DefaultTableCellHeaderRenderer {

		boolean isSelected, hasFocus, hasRollover;

		/** 选中上半部颜色 */
		private final Color topEnterColor1 = new Color(227, 247, 254);
		private final Color topEnterColor2 = new Color(225, 245, 252);
		/** 选中下半部颜色 */
		private final Color buttomEnterColor1 = new Color(189, 236, 254);
		private final Color buttomEnterColor2 = new Color(187, 234, 252);

		/** 未选中上半部颜色 */
		private final Color topNoEnterColor1 = new Color(237, 237, 237);
		private final Color topNoEnterColor2 = new Color(235, 235, 235);
		/** 未选中下半部颜色 */
		private final Color buttomNoEnterColor1 = new Color(209, 209, 209);
		private final Color buttomNoEnterColor2 = new Color(207, 207, 207);

		/** 上半部颜色 */
		private Color topColor1 = topNoEnterColor1;
		private Color topColor2 = topNoEnterColor2;
		/** 下半部颜色 */
		private Color buttomColor1 = buttomNoEnterColor1;
		private Color buttomColor2 = buttomNoEnterColor2;

		JecnDefaultTableCellHeaderRenderer() {
			// 左对齐
			this.setHorizontalAlignment(textPostion);
			this.setOpaque(false);
			this.setBorder(null);
		}

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {

			this.isSelected = isSelected;
			this.hasFocus = hasFocus;
			this.hasRollover = (column == getRolloverColumn());

			if (isSelected || hasFocus || hasRollover) {
				// 上半部颜色
				topColor1 = topEnterColor1;
				topColor2 = topEnterColor2;
				// 下半部颜色
				buttomColor1 = buttomEnterColor1;
				buttomColor2 = buttomEnterColor2;
			} else {
				// 上半部颜色
				topColor1 = topNoEnterColor1;
				topColor2 = topNoEnterColor2;
				// 下半部颜色
				buttomColor1 = buttomNoEnterColor1;
				buttomColor2 = buttomNoEnterColor2;
			}

			super.getTableCellRendererComponent(table, value, isSelected,
					hasFocus, row, column);
			return this;
		}

		/**
		 * 
		 * 绘制组件
		 * 
		 */
		public void paint(Graphics g) {

			Graphics2D g2d = (Graphics2D) g;

			// 渐变
			g2d.setPaint(new GradientPaint(0, 0, topColor1, 0,
					this.getHeight() / 4, topColor2));
			g2d.fillRect(0, 0, this.getWidth(), this.getHeight() / 2);

			g2d.setPaint(new GradientPaint(0, 0, buttomColor1, 0, this
					.getHeight() / 4 * 3, buttomColor2));
			g2d.fillRect(0, this.getHeight() / 2, this.getWidth(), this
					.getHeight());

			// 边框颜色
			g2d.setColor(JecnUIUtil.getDialogBorderColor());

			// 底边框线
			g2d.drawLine(0, this.getHeight() - 1, this.getWidth(), this
					.getHeight() - 1);
			// 右边框线
			g2d.drawLine(this.getWidth() - 1, 0, this.getWidth() - 1, this
					.getHeight());

			super.paint(g);
		}

		public void setBorder(Border paramBorder) {
		}
	}
}
