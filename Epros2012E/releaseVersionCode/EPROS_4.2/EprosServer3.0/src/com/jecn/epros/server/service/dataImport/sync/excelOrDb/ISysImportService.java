package com.jecn.epros.server.service.dataImport.sync.excelOrDb;

import java.util.List;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpOriDept;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;

public interface ISysImportService {
	public void deleteAndInsertTmpOriJecnIOTable(BaseBean baseBean) throws Exception;

	public void updateProFlag(Long currProjectID, boolean isNotExistsDept) throws Exception;

	public void deleteAndInsertTmpJecnIOTable(BaseBean baseBean) throws Exception;

	public List<Long> checkProjectId() throws Exception;

	public Long getProtectedId() throws Exception;

	public void syncImortDataAndDB(BaseBean baseBean, Long currProjectID, boolean isSyncUserOnly) throws Exception;

	/**
	 * 原始数据部门集合
	 * 
	 * @return
	 * @throws Exception
	 */
	List<JecnTmpOriDept> findAllDeptOri() throws Exception;

	/**
	 * 获取人员数据集合
	 * 
	 * @return
	 * @throws Exception
	 */
	List<JecnTmpPosAndUserBean> findAllUserAndPosOri() throws Exception;

	/**
	 * 发送邮件或消息给系统管理员
	 * @param result 
	 * 
	 * @param filePath
	 * @param hasChange 
	 * @throws Exception
	 */
	void changeDataSendMailMessageToAdmin(MessageResult result, String filePath, boolean hasChange) throws Exception;

	String syncDataValidate(Long projectId) throws Exception;

}
