package com.jecn.epros.server.bean.propose;

import java.util.ArrayList;
import java.util.List;

/**
 * 合理化建议统计bean
 * @author hyl
 *
 */
public class JecnRtnlProposeCount {
	
	/** 合理化信息统计详情集合bean**/
	private List<JecnRtnlProposeCountDetail> countDetailList=new ArrayList<JecnRtnlProposeCountDetail>();
	/*** 时间**/
	private String dateStr;
	/** 数据类型 1为查询条件为创建人的统计信息  2为查询条件为部门的 3为时间部门综合查询**/
	private int type;
	public List<JecnRtnlProposeCountDetail> getCountDetailList() {
		return countDetailList;
	}
	public void setCountDetailList(List<JecnRtnlProposeCountDetail> countDetailList) {
		this.countDetailList = countDetailList;
	}
	public String getDateStr() {
		return dateStr;
	}
	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	
}
