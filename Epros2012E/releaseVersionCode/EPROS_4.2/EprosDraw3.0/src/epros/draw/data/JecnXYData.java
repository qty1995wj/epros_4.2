package epros.draw.data;

import java.awt.Point;

/**
 * 
 * 坐标值记录类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnXYData implements Cloneable {
	/** 画图时X坐标 */
	private double x = 0.0D;
	/** 画图时Y坐标 */
	private double y = 0.0D;

	public JecnXYData() {
		this.x = 0.0D;
		this.y = 0.0D;
	}

	public JecnXYData(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public JecnXYData(JecnXYData paraXY) {
		if (paraXY != null) {
			this.x = paraXY.getX();
			this.y = paraXY.getY();
		}
	}

	/**
	 * 
	 * 克隆放大对象
	 * 
	 * @param zooMmultiple
	 * @return
	 */
	public JecnXYData cloneZoomXYCloneable(double zooMmultiple) {
		JecnXYData xyCloneable = this.clone();
		xyCloneable.zoomXYCloneable(zooMmultiple);
		return xyCloneable;
	}

	/**
	 * 
	 * 放大缩小给定的对象属性值
	 * 
	 * @param xyCloneable
	 * @param zooMmultiple
	 */
	public void zoomXYCloneable(double zooMmultiple) {
		this.x = this.x * zooMmultiple;
		this.y = this.y * zooMmultiple;
	}

	/**
	 * 克隆对象
	 */
	public JecnXYData clone() {
		return new JecnXYData(this);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setXY(JecnXYData paramXY) {
		if (paramXY != null)
			setXY(paramXY.getX(), paramXY.getY());
	}

	/**
	 * 
	 * @param x
	 * @param y
	 */
	public void setXY(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Point getPoint() {
		Point p = new Point();
		p.setLocation(this.x, this.y);
		return p;
	}

}
