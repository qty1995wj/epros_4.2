package com.jecn.epros.server.service.popedom.buss;

import java.awt.Color;
import java.awt.Point;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.download.word.DownloadUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.popedom.JobDescriptionBean;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;
import com.jecn.epros.server.webBean.process.ProcessRoleWebBean;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.table.RtfBorder;
import com.lowagie.text.rtf.table.RtfBorderGroup;
import com.lowagie.text.rtf.table.RtfCell;

/**
 * 创建岗位说明书
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Jan 22, 2013 时间：3:06:41 PM
 */
public class PositionDescription {
	private static final Logger log = Logger.getLogger(PositionDescription.class);

	/** 1、基本岗位信息 */
	public static final String POS_BASIC_INFO = "1、基本岗位信息";
	/** 2、与本岗位相关制度 */
	public static final String POS_REF_RULE = "2、与本岗位相关制度";
	/** 3、任职条件 */
	public static final String QUALIFICATION = "3、任职条件";
	/** 2、EPS任职条件 */
	public static final String EPS_QUALIFICATION = "2、任职条件";
	/** 4、角色职责 */
	public static final String ROLE_DESC = "4、角色职责";
	/** 3、EPS角色职责 */
	public static final String EPS_ROLE_DESC = "3、角色职责";
	/** 岗位名称： */
	public static final String POS_NAME = "岗位名称：";
	/** 上级岗位： */
	public static final String UP_POSITION = "上级岗位：";
	/** 下级岗位： */
	public static final String LOWER_POSITION = "下级岗位：";
	/** 岗位类别 */
	public static final String POS_TYPE = "岗位类别：";
	/** 核心类别： */
	public static final String THE_CORE_CATEGORY = "核心价值：";
	/** 岗位说明书： */
	public static final String POSITION_FILE_NAME = "附件：";
	/** 部门内相关制度： */
	public static final String IN_DEPT_REF_RULE = "部门内相关制度：";
	/** 部门外相关制度： */
	public static final String OUT_DEPT_REF_RULE = "部门外相关制度：";
	/** 学历 */
	public static final String EDUCATION = "学历：";
	/** 知识： */
	public static final String KNOWLEDGE = "知识：";
	/** 素质模型： */
	public static final String THE_QUALLITY_MODEL = "素质模型：";
	/** 技能： */
	public static final String SKILL = "技能：";
	/** 执行角色 */
	public static final String EXCUTE_ROLE = "执行角色";
	/** 岗位职责内容 */
	public static final String POS_DESC = "岗位职责内容";
	/** 流程 */
	public static final String FLOW = "流程";
	/** 活动 */
	public static final String ACTIIVE = "活动";
	/** 活动KPI */
	public static final String ACTIVE_KPI = "活动KPI";
	/** 目标值 */
	public static final String GOAL_VALUE = "目标值";
	/** 无 */
	public static final String NONE = "无";

	/**
	 * 创建岗位说明书
	 * 
	 * @param descriptionBean
	 *            JobDescriptionBean 岗位说明书数据
	 * @throws Exception
	 * 
	 */
	public static String createPositionDesc(JobDescriptionBean descriptionBean) throws Exception {
		if (descriptionBean == null) {
			throw new NullPointerException();
		}

		String filePath = null;

		Document document = null;
		FileOutputStream fileOutputStream = null;
		try {
			filePath = JecnFinal.saveTempFilePath();

			document = new Document(PageSize.A4, 50, 50, 50, 50);
			fileOutputStream = new FileOutputStream(JecnFinal.getAllTempPath(filePath));
			RtfWriter2.getInstance(document, fileOutputStream);
			document.open();

			// 岗位名称
			document.add(DownloadUtil.ST_X2_BOLD_MIDDLE(descriptionBean.getPosName()));

			// 1、基本岗位信息
			Paragraph title1 = DownloadUtil.getParagraphTitle(POS_BASIC_INFO);
			document.add(title1);
			// 岗位名称：
			addContent(document, POS_NAME, descriptionBean.getPosName());
			// 上级岗位：
			addContent(document, UP_POSITION, descriptionBean.getUpPositionList());
			// 下级岗位：
			addContent(document, LOWER_POSITION, descriptionBean.getLowerPositionList());
			// 岗位类别：
			addContent(document, POS_TYPE, descriptionBean.getPositionsNature());
			// 核心价值：
			addContent(document, THE_CORE_CATEGORY, descriptionBean.getSchoolExperience());
			// 附件：
			addContent(document, POSITION_FILE_NAME, descriptionBean.getFileName());

			if (JecnUtil.isVersionType()) {
				// 2、与本岗位相关制度
				Paragraph title2 = DownloadUtil.getParagraphTitle(POS_REF_RULE);
				document.add(title2);

				// 部门内相关制度：
				addContent(document, IN_DEPT_REF_RULE, descriptionBean.getInListRule());
				// 部门外相关制度：
				addContent(document, OUT_DEPT_REF_RULE, descriptionBean.getOutListRule());
			}
			// 3、任职条件
			Paragraph title3 = null;
			if (JecnUtil.isVersionType()) {
				title3 = DownloadUtil.getParagraphTitle(QUALIFICATION);
			} else {
				title3 = DownloadUtil.getParagraphTitle(EPS_QUALIFICATION);
			}
			document.add(title3);
			// 学历：
			addContent(document, EDUCATION, descriptionBean.getKnowledge());
			// 知识：
			addContent(document, KNOWLEDGE, descriptionBean.getPositionSkill());
			// 技能：
			addContent(document, SKILL, descriptionBean.getPositionImportance());
			// 素质模型：
			addContent(document, THE_QUALLITY_MODEL, descriptionBean.getPositionDiathesisModel());

			// 4、任职条件
			Paragraph title4 = null;
			if (JecnUtil.isVersionType()) {
				title4 = DownloadUtil.getParagraphTitle(ROLE_DESC);
			} else {
				title4 = DownloadUtil.getParagraphTitle(EPS_ROLE_DESC);
			}

			document.add(title4);
			Table table4 = new Table(4);
			table4.setAlignment(Table.ALIGN_LEFT);
			table4.setWidth(100f);
			table4.setWidths(new int[] { 20, 30, 20, 20});
			RtfCell cellDotted6 = new RtfCell("Dotted border");
			cellDotted6.setBorders(new RtfBorderGroup(Rectangle.BOX, RtfBorder.BORDER_DOTTED, 1, new Color(0, 0, 0)));
			// "执行角色"
			table4.addCell(DownloadUtil.ST_5_BOLD_MIDDLE(EXCUTE_ROLE), new Point(0, 0));
			// "岗位职责内容"
			table4.addCell(DownloadUtil.ST_5_BOLD_MIDDLE(POS_DESC), new Point(0, 1));
			
			// "活动"
			table4.addCell(DownloadUtil.ST_5_BOLD_MIDDLE(ACTIIVE), new Point(0, 2));
			
			// "流程"
			table4.addCell(DownloadUtil.ST_5_BOLD_MIDDLE(FLOW), new Point(0, 3));
			// // "活动KPI"
			// table4.addCell(DownloadUtil.ST_5_BOLD_MIDDLE(ACTIVE_KPI),
			// new Point(0, 4));
			// // 目标值
			// table4.addCell(DownloadUtil.ST_5_BOLD_MIDDLE(GOAL_VALUE),
			// new Point(0, 5));
			int title6i = 1;
			List<ProcessRoleWebBean> list = descriptionBean.getJecnRoleProcessBean().getRoleList();
			for (ProcessRoleWebBean flowRalatedBean : list) {
				int roleRowSpan = 1;
				List<ProcessActiveWebBean> activeList = flowRalatedBean.getActiveList();
				if (activeList != null && activeList.size() > 0) {
					roleRowSpan = activeList.size();
//					for (ProcessActiveWebBean activityShowBean : activeList) {
//						if (activityShowBean.getIndicatorsList() != null
//								&& activityShowBean.getIndicatorsList().size() > 0) {
//							roleRowSpan += activityShowBean.getIndicatorsList().size() - 1;
//						}
//					}
				}
				Cell roleName = new Cell(DownloadUtil.changeNetToWordStyle(flowRalatedBean.getRoleName()));
				roleName.setRowspan(roleRowSpan);
				table4.addCell(roleName, new Point(title6i, 0));
				Cell dutyCell = new Cell(DownloadUtil.changeNetToWordStyle(flowRalatedBean.getRoleRes()));
				dutyCell.setRowspan(roleRowSpan);
				table4.addCell(dutyCell, new Point(title6i, 1));
				
				
				int activeRow = title6i;
				// 活动
				for (ProcessActiveWebBean activityShowBean : activeList) {
//					int actRowSpan = 1;
//					if (activityShowBean.getIndicatorsList() != null && activityShowBean.getIndicatorsList().size() > 0) {
//						actRowSpan = activityShowBean.getIndicatorsList().size();
//					}
					Cell actCell = new Cell(DownloadUtil.changeNetToWordStyle(activityShowBean.getActiveName()));
//					actCell.setRowspan(actRowSpan);
					table4.addCell(actCell, new Point(title6i, 2));
//					// 岗位KPI
//					for (JecnRefIndicators jecnRefIndicators : activityShowBean.getIndicatorsList()) {
//						table4.addCell(jecnRefIndicators.getIndicatorName(), new Point(title6i, 4));
//						table4.addCell(jecnRefIndicators.getIndicatorValue(), new Point(title6i, 5));
//						title6i++;
//					}
//					if (activityShowBean.getIndicatorsList() == null
//							|| activityShowBean.getIndicatorsList().size() == 0) {
						title6i++;
//					}
				}
				if (activeList == null || activeList.size() == 0) {
					title6i++;
				}
				Cell posFlowCell = new Cell(DownloadUtil.changeNetToWordStyle(flowRalatedBean.getFlowName()));
				posFlowCell.setRowspan(roleRowSpan);
				table4.addCell(posFlowCell, new Point(activeRow, 3));
			}
			document.add(table4);

		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (Exception ex) {
					log.error("关闭document", ex);
				}
			}

			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (Exception ex) {
					log.error("关闭fileOutputStream", ex);
				}
			}
		}
		return filePath;
	}

	/**
	 * 添加岗位说明书某属性
	 * 
	 * @param document
	 * @param lableName
	 *            标签
	 * @param content
	 *            正文
	 * @throws DocumentException
	 */
	public static void addContent(Document document, String lableName, String content) throws DocumentException {
		if (lableName == null) {
			throw new NullPointerException();
		}
		if (!JecnCommon.isNullOrEmtryTrim(content)) {
			document.add(DownloadUtil.getContent(lableName + content));
		} else {
			document.add(DownloadUtil.getContent(lableName + NONE));
		}
	}

	/**
	 * 添加岗位说明书某属性
	 * 
	 * @param document
	 * @param lableName
	 *            标签
	 * @param positionList
	 *            List<JecnTreeBean>相关岗位集合
	 * @throws DocumentException
	 */
	public static void addContent(Document document, String lableName, List<JecnTreeBean> positionList)
			throws DocumentException {
		if (lableName == null) {
			throw new NullPointerException();
		}
		if (positionList == null) {
			document.add(DownloadUtil.getContent(lableName + NONE));
			return;
		}
		String posName = "";
		for (JecnTreeBean jecnTreeBean : positionList) {
			posName = posName + "[" + jecnTreeBean.getName() + "]";
		}
		if (!JecnCommon.isNullOrEmtryTrim(posName)) {
			document.add(DownloadUtil.getContent(lableName + posName));
		} else {
			document.add(DownloadUtil.getContent(lableName + NONE));
		}
	}
}
