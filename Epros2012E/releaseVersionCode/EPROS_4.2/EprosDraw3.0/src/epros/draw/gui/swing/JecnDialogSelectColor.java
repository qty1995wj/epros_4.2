package epros.draw.gui.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.line.shape.ManhattanLine;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemOftenColorData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏颜色下拉框弹出菜单选择颜色
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDialogSelectColor extends JPopupMenu implements ActionListener {
	/** 主panel */
	private JecnPanel mainPanel = null;
	/** 无填充色 */
	private JButton noFillColorButton = null;
	/** 标准颜色panel */
	private JPanel standardPanel = null;
	/** 常用颜色panel */
	private JPanel commonlyPanel = null;
	/** 其他颜色button */
	private JButton otherButton = null;
	/** 选择类型 */
	private ToolBarElemType toolBarElemType = null;
	/** 常用颜色 */
	private JecnSystemOftenColorData systemOftenColorData = null;

	public JecnDialogSelectColor(ToolBarElemType toolBarElemType) {
		this.toolBarElemType = toolBarElemType;
		systemOftenColorData = JecnSystemData.readOftenColorData();
		initComponents();
	}

	private void initComponents() {
		// 设置背景色为默认色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 初始化 主Panel
		mainPanel = new JecnPanel();

		// 无填充色 button
		JecnToolBar noColorToolBar = getNoFillColorButton();

		// 标准色Panel
		getStandardColorPanel();

		// 常用颜色Panel
		getCommonlyColorPanel();

		// 其他属性button
		JecnToolBar OtherColorToolBar = getOtherColorButton();

		// 无填充色按钮
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, new Insets(2, 0, 0, 0), 0, 0);
		mainPanel.add(noColorToolBar, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(standardPanel, c);
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(commonlyPanel, c);
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 2, 0), 0, 0);
		mainPanel.add(OtherColorToolBar, c);

		this.add(mainPanel);
	}

	/**
	 * 初始化 无填充色button
	 */
	private JecnToolBar getNoFillColorButton() {
		// 初始化 无填充色 组件
		noFillColorButton = new JButton();
		// 添加button的点击事件
		noFillColorButton.addActionListener(this);
		// 设置button的动作事件名称
		noFillColorButton.setActionCommand(FillType.none.toString());
		// 大小
		noFillColorButton.setPreferredSize(new Dimension(20, 20));

		switch (toolBarElemType) {
		case editFillSelectColor: // 填充选择颜色
			noFillColorButton.setText(JecnResourceUtil.getJecnResourceUtil().getValue("selectColorNoFill"));
			break;
		case editShadowSelectColor: // 阴影选择颜色
			noFillColorButton.setText(JecnResourceUtil.getJecnResourceUtil().getValue("selectColorNoShadow"));
			break;
		case editLineSelectColor: // 线条选择颜色
			noFillColorButton.setText(JecnResourceUtil.getJecnResourceUtil().getValue("selectColorDefault"));
			break;
		case editfontSelectColor: // 字体选择颜色
			noFillColorButton.setText(JecnResourceUtil.getJecnResourceUtil().getValue("selectColorDefault"));
			break;
		}
		// 边框为空
		noFillColorButton.setBorder(null);
		// 透明
		noFillColorButton.setOpaque(false);
		// 不显示选中框
		noFillColorButton.setFocusable(false);

		JecnToolBar toolbar = new JecnToolBar();
		toolbar.add(noFillColorButton);
		return toolbar;
	}

	/**
	 * 初始化其他颜色button
	 */
	private JecnToolBar getOtherColorButton() {
		// 初始化 无填充色 组件
		otherButton = new JButton();
		// 添加button的点击事件
		otherButton.addActionListener(this);
		// 设置button的动作事件名称
		otherButton.setActionCommand("commonly");
		otherButton.setText(JecnResourceUtil.getJecnResourceUtil().getValue("selectColorOther"));
		// 不显示选中框
		otherButton.setFocusable(false);
		// 透明
		otherButton.setOpaque(false);
		// 不显示选中框
		otherButton.setFocusable(false);

		JecnToolBar toolbar = new JecnToolBar();
		toolbar.add(otherButton);
		return toolbar;
	}

	/**
	 * 常用颜色panel
	 */
	private void getCommonlyColorPanel() {
		// 初始化 常用颜色panel
		commonlyPanel = new JPanel();
		// 添加标题
		commonlyPanel.setBorder(BorderFactory.createTitledBorder(JecnResourceUtil.getJecnResourceUtil().getValue(
				"selectColorOften")));
		// 设置布局方式
		commonlyPanel.setLayout(new GridLayout(1, 8, 8, 8));
		// 设置背景色
		commonlyPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 添加常用颜色Panel
		getCommonlyColor();
	}

	/**
	 * 常用颜色
	 */
	private void getCommonlyColor() {
		switch (toolBarElemType) {
		case editFillSelectColor: // 填充选择颜色
			if (systemOftenColorData.getFillColorList() != null && systemOftenColorData.getFillColorList().size() == 8) {
				for (Color color : systemOftenColorData.getFillColorList()) {
					getButton(commonlyPanel, color);
				}
			} else {
				getDefaultCommonlyButton();
			}
			break;
		case editShadowSelectColor: // 阴影选择颜色
			if (systemOftenColorData.getShadowColorList() != null
					&& systemOftenColorData.getShadowColorList().size() == 8) {
				for (Color color : systemOftenColorData.getShadowColorList()) {
					getButton(commonlyPanel, color);
				}
			} else {
				getDefaultCommonlyButton();
			}
			break;
		case editLineSelectColor: // 线条选择颜色
			if (systemOftenColorData.getLineColorList() != null && systemOftenColorData.getLineColorList().size() == 8) {
				for (Color color : systemOftenColorData.getLineColorList()) {
					getButton(commonlyPanel, color);
				}
			} else {
				getDefaultCommonlyButton();
			}
			break;
		case editfontSelectColor: // 字体选择颜色
			if (systemOftenColorData.getFontColorList() != null && systemOftenColorData.getFontColorList().size() == 8) {
				for (Color color : systemOftenColorData.getFontColorList()) {
					getButton(commonlyPanel, color);
				}
			} else {
				getDefaultCommonlyButton();
			}

			break;
		}
	}

	private void getDefaultCommonlyButton() {
		for (int i = 0; i < 8; i++) {
			// 设置button的颜色
			Color color = null;
			switch (i) {
			case 0:
				color = new Color(255, 255, 255);
				break;
			case 1:
				color = new Color(240, 248, 255);
				break;
			case 2:
				color = new Color(230, 230, 250);
				break;
			case 3:
				color = new Color(220, 220, 220);
				break;
			case 4:
				color = new Color(250, 235, 215);
				break;
			case 5:
				color = new Color(240, 230, 140);
				break;
			case 6:
				color = new Color(255, 182, 193);
				break;
			case 7:
				color = new Color(254, 169, 254);
				break;
			}
			getButton(commonlyPanel, color);
			switch (toolBarElemType) {
			case editFillSelectColor: // 填充选择颜色
				systemOftenColorData.getFillColorList().add(color);
				break;
			case editShadowSelectColor: // 阴影选择颜色
				systemOftenColorData.getShadowColorList().add(color);
				break;
			case editLineSelectColor: // 线条选择颜色
				systemOftenColorData.getLineColorList().add(color);
				break;
			case editfontSelectColor: // 字体选择颜色
				systemOftenColorData.getFontColorList().add(color);
				break;
			}
		}
	}

	/**
	 * 标准色panel
	 */
	private void getStandardColorPanel() {
		// 标准颜色panel 初始化
		standardPanel = new JPanel();
		// 添加标题
		standardPanel.setBorder(BorderFactory.createTitledBorder(JecnResourceUtil.getJecnResourceUtil().getValue(
				"selectColorFac")));
		// 设置布局方式
		standardPanel.setLayout(new GridLayout(5, 8, 8, 8));
		// 设置背景色
		standardPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 添加panel
		getColorButton();
	}

	/**
	 * 获取颜色按钮
	 * 
	 * @param colorPanel
	 *            需要进行添加的Panel
	 * @param color
	 *            按钮的颜色
	 * @return
	 */
	private void getButton(JPanel colorPanel, Color color) {
		JButton colorbutton = new JButton();
		// 添加button的点击事件
		colorbutton.addActionListener(this);
		// 设置button的大小
		colorbutton.setPreferredSize(new Dimension(12, 12));
		// 不显示选中框
		colorbutton.setFocusable(false);
		// 颜色panel
		JPanel colorButonPanel = new JPanel();
		// 设置布局样式
		colorButonPanel.setLayout(new BorderLayout());
		// 设置panel背景色
		colorButonPanel.setBackground(color);
		// 向panel添加button
		colorbutton.add(colorButonPanel);
		// 不绘制button内容区域
		colorbutton.setContentAreaFilled(false);
		// 不显示选中框
		colorbutton.setFocusable(false);
		// 边框为空
		colorbutton.setBorder(null);
		// 透明
		colorbutton.setOpaque(false);

		// 设置此按钮的动作事件名称
		colorbutton.setActionCommand(DrawCommon.coverColorToString(colorButonPanel.getBackground()));
		colorbutton.setBorder(BorderFactory.createLineBorder(Color.gray));

		// JecnToolBar toolbar = new JecnToolBar();
		// toolbar.add(colorbutton);

		colorPanel.add(colorbutton);
	}

	/**
	 * 添加标准颜色
	 */
	private void getColorButton() {
		for (int i = 0; i < 40; i++) {
			// 设置button的颜色
			Color color = null;
			switch (i) {
			case 0:
				color = new Color(255, 255, 255);
				break;
			case 1:
				color = new Color(240, 248, 255);
				break;
			case 2:
				color = new Color(230, 230, 250);
				break;
			case 3:
				color = new Color(220, 220, 220);
				break;
			case 4:
				color = new Color(250, 235, 215);
				break;
			case 5:
				color = new Color(240, 230, 140);
				break;
			case 6:
				color = new Color(255, 182, 193);
				break;
			case 7:
				color = new Color(254, 169, 254);
				break;
			case 8:
				color = new Color(205, 205, 77);
				break;
			case 9:
				color = new Color(255, 215, 0);
				break;
			case 10:
				color = new Color(218, 165, 32);
				break;
			case 11:
				color = new Color(0, 255, 255);
				break;
			case 12:
				color = new Color(0, 206, 209);
				break;
			case 13:
				color = new Color(0, 191, 255);
				break;
			case 14:
				color = new Color(94, 170, 198);
				break;
			case 15:
				color = new Color(100, 149, 237);
				break;
			case 16:
				color = new Color(173, 255, 47);
				break;
			case 17:
				color = new Color(127, 255, 0);
				break;
			case 18:
				color = new Color(102, 205, 170);
				break;
			case 19:
				color = new Color(95, 158, 160);
				break;
			case 20:
				color = new Color(143, 188, 143);
				break;
			case 21:
				color = new Color(42, 190, 42);
				break;
			case 22:
				color = new Color(0, 100, 0);
				break;
			case 23:
				color = new Color(47, 79, 79);
				break;
			case 24:
				color = new Color(233, 150, 122);
				break;
			case 25:
				color = new Color(222, 184, 135);
				break;
			case 26:
				color = new Color(240, 128, 128);
				break;
			case 27:
				color = new Color(210, 105, 30);
				break;
			case 28:
				color = new Color(255, 140, 0);
				break;
			case 29:
				color = new Color(128, 64, 0);
				break;
			case 30:
				color = new Color(139, 0, 139);
				break;
			case 31:
				color = new Color(138, 43, 226);
				break;
			case 32:
				color = new Color(255, 127, 80);
				break;
			case 33:
				color = new Color(216, 30, 30);
				break;
			case 34:
				color = new Color(255, 20, 147);
				break;
			case 35:
				color = new Color(255, 0, 255);
				break;
			case 36:
				color = new Color(119, 136, 153);
				break;
			case 37:
				color = new Color(0, 0, 255);
				break;
			case 38:
				color = new Color(0, 0, 139);
				break;
			case 39:
				color = new Color(0, 0, 0);
				break;
			}
			getButton(standardPanel, color);
		}
	}

	/**
	 * 修改各颜色选择时常用颜色的List
	 */
	private void alertColorList(Color color) {
		// 清除 panel中的数据
		commonlyPanel.removeAll();
		// 修改List集合
		JecnElementAttributesUtil.alertColorList(systemOftenColorData, toolBarElemType, color);
		// 重新给panel 赋值
		getCommonlyColor();
		// 更新panel
		commonlyPanel.updateUI();
	}

	/**
	 * button 处理事件
	 */
	public void actionPerformed(ActionEvent e) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		if (e.getSource() instanceof JButton) {
			// 获取button
			JButton colorButton = (JButton) e.getSource();
			// 获取button的事件名称
			String colorCommand = colorButton.getActionCommand();
			// 获取选择图形的List
			List<JecnBaseFlowElementPanel> baseFlowElementList = JecnDrawMainPanel.getMainPanel().getWorkflow()
					.getCurrentSelectElement();
			// 操作前数据
			JecnUndoRedoData undoData = new JecnUndoRedoData();
			// 操作后数据
			JecnUndoRedoData redoData = new JecnUndoRedoData();
			// 判断点击的button
			if (FillType.none.toString().equals(colorCommand)) {
				switch (toolBarElemType) {
				case editFillSelectColor: // 填充选择颜色
					for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
						// 判断是否为图形
						if (baseFlowElement instanceof JecnBaseFigurePanel) {
							// 记录操作前数据
							undoData.recodeFlowElement(baseFlowElement);
							// 设置图形填充类型
							baseFlowElement.getFlowElementData().setFillType(FillType.none);
							baseFlowElement.getFlowElementData().setFillColor(null);
							baseFlowElement.getFlowElementData().setShadowColor(null);
							baseFlowElement.getFlowElementData().setShadowsFlag(false);
							baseFlowElement.getFlowElementData().setFlag3D(false);

							// 删除阴影元素
							((JecnBaseFigurePanel) baseFlowElement).removeRelatedPanel();
							// 记录操作后数据
							redoData.recodeFlowElement(baseFlowElement);
						}
					}
					break;
				case editShadowSelectColor: // 阴影选择颜色
					for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
						// 判断是否为图形
						if (baseFlowElement instanceof JecnBaseFigurePanel) {
							// 记录操作前数据
							undoData.recodeFlowElement(baseFlowElement);
							// 设置阴影填充类型
							baseFlowElement.getFlowElementData().setShadowsFlag(false);
							baseFlowElement.getFlowElementData().setShadowColor(null);
							JecnBaseFigurePanel baseFigurePanel = (JecnBaseFigurePanel) baseFlowElement;
							// 删除阴影元素
							baseFigurePanel.removeRelatedPanel();
							// 记录操作后数据
							redoData.recodeFlowElement(baseFlowElement);
						}
					}
					break;
				case editLineSelectColor: // 线条选择颜色
					for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
						// 记录操作前数据
						undoData.recodeFlowElement(baseFlowElement);
						// 判断是否为线条
						baseFlowElement.getFlowElementData().setBodyColor(Color.BLACK);
						if (baseFlowElement instanceof ManhattanLine) {
							ManhattanLine line = (ManhattanLine) baseFlowElement;
							line.setArrowColor();
						}
						// 记录操作后数据
						redoData.recodeFlowElement(baseFlowElement);
					}
					break;
				case editfontSelectColor: // 字体选择颜色
					for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
						// 记录操作前数据
						undoData.recodeFlowElement(baseFlowElement);
						// 设置字体的颜色
						baseFlowElement.getFlowElementData().setFontColor(Color.BLACK);
						// 记录操作后数据
						redoData.recodeFlowElement(baseFlowElement);
					}
					break;
				}
				if (baseFlowElementList.size() >= 1) {
					// 记录这次操作的数据集合
					JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
					JecnDrawMainPanel.getMainPanel().getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
				}
			} else if ("commonly".equals(colorCommand)) {
				this.setVisible(false);
				// 选择颜色时 的默认颜色
				Color selectColor = null;
				switch (toolBarElemType) {
				case editFillSelectColor: // 填充选择颜色
					if (systemOftenColorData.getFillColorList() == null
							&& systemOftenColorData.getFillColorList().size() != 8) {
						return;
					} else {
						selectColor = systemOftenColorData.getFillColorList().get(0);
					}
					break;
				case editShadowSelectColor: // 阴影选择颜色
					if (systemOftenColorData.getShadowColorList() == null
							&& systemOftenColorData.getShadowColorList().size() != 8) {
						return;
					} else {
						selectColor = systemOftenColorData.getShadowColorList().get(0);
					}
					break;
				case editLineSelectColor: // 线条选择颜色
					if (systemOftenColorData.getLineColorList() == null
							&& systemOftenColorData.getLineColorList().size() != 8) {
						return;
					} else {
						selectColor = systemOftenColorData.getLineColorList().get(0);
					}
					break;
				case editfontSelectColor: // 字体选择颜色
					if (systemOftenColorData.getFontColorList() == null
							&& systemOftenColorData.getFontColorList().size() != 8) {
						return;
					} else {
						selectColor = systemOftenColorData.getFontColorList().get(0);
					}
					break;
				}
				Color color = null;
				if (selectColor != null) {
					// 打开颜色选择器选择颜色
					color = JecnColorChooserProcess.showColorChooserDialog(JecnResourceUtil.getJecnResourceUtil()
							.getValue("selectColorTitle"), selectColor);
				} else {
					// 打开颜色选择器选择颜色
					color = JecnColorChooserProcess.showColorChooserDialog(JecnResourceUtil.getJecnResourceUtil()
							.getValue("selectColorTitle"), Color.BLACK);
				}

				if (color != null && color.getRGB() != selectColor.getRGB()) {
					switch (toolBarElemType) {
					case editFillSelectColor: // 填充选择颜色
						JecnElementAttributesUtil.updateElementFillColor(baseFlowElementList, color);
						// 修改按钮左下角panel颜色
						JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getStyleFillColorBtn()
								.getColorButonPanel().setBackground(color);
						break;
					case editShadowSelectColor: // 阴影选择颜色
						JecnElementAttributesUtil.updateElementShadowColor(baseFlowElementList, color);
						// 修改按钮左下角panel颜色
						JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getStyleShadowsBtn()
								.getColorButonPanel().setBackground(color);
						break;
					case editLineSelectColor: // 线条选择颜色
						JecnElementAttributesUtil.updateElementLineColor(baseFlowElementList, color);
						// 修改按钮左下角panel颜色
						JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getStyleLineColorBtn()
								.getColorButonPanel().setBackground(color);
						break;
					case editfontSelectColor: // 字体选择颜色
						JecnElementAttributesUtil.updateElementFontColor(baseFlowElementList, color);
						// 修改按钮左下角panel颜色
						JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getFontColorBtn()
								.getColorButonPanel().setBackground(color);
						break;
					}
					// 更新常用颜色的List
					alertColorList(color);
					// 改变状态为更新
					systemOftenColorData.setUpdateFlag(true);
					// 把常用颜色写入到配置文件中
					JecnSystemData.exportColorToPropertyFile();
				}
			} else {
				// 获取颜色
				Color color = DrawCommon.getColorByString(colorCommand);
				if (color != null) {
					switch (toolBarElemType) {
					case editFillSelectColor: // 填充选择颜色
						JecnElementAttributesUtil.updateElementFillColor(baseFlowElementList, color);
						// 修改按钮左下角panel颜色
						JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getStyleFillColorBtn()
								.getColorButonPanel().setBackground(color);
						break;
					case editShadowSelectColor: // 阴影选择颜色
						JecnElementAttributesUtil.updateElementShadowColor(baseFlowElementList, color);
						// 修改按钮左下角panel颜色
						JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getStyleShadowsBtn()
								.getColorButonPanel().setBackground(color);
						break;
					case editLineSelectColor: // 线条选择颜色
						JecnElementAttributesUtil.updateElementLineColor(baseFlowElementList, color);
						// 修改按钮左下角panel颜色
						JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getStyleLineColorBtn()
								.getColorButonPanel().setBackground(color);
						break;
					case editfontSelectColor: // 字体选择颜色
						JecnElementAttributesUtil.updateElementFontColor(baseFlowElementList, color);
						// 修改按钮左下角panel颜色
						JecnDrawMainPanel.getMainPanel().getToolbar().getEditToolBarItemPanel().getFontColorBtn()
								.getColorButonPanel().setBackground(color);
						break;
					}
				}
				// 更新常用颜色的List
				alertColorList(color);
				// 改变状态为更新
				systemOftenColorData.setUpdateFlag(true);
				// 把常用颜色写入到配置文件中
				JecnSystemData.exportColorToPropertyFile();
			}
			// 刷新面板
			JecnDrawMainPanel.getMainPanel().getWorkflow().repaint();
		}
	}
}
