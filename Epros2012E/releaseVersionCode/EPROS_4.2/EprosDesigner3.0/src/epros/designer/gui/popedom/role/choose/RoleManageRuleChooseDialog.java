package epros.designer.gui.popedom.role.choose;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class RoleManageRuleChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(RoleManageRuleChooseDialog.class);

	public RoleManageRuleChooseDialog(List<JecnTreeBean> list) {
		super(list, 27);
		this.setTitle(JecnProperties.getValue("ruleChoose"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getRuleAction().searchByName(name, JecnConstants.projectId,
					JecnDesignerCommon.getSecondAdminUserId());
		} catch (Exception e) {
			log.error("RoleManageRuleChooseDialog searchByName is error", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("ruleNameC");// 制度名称：
	}

	@Override
	public JecnTree getJecnTree() {
		return new RoleManageHighEfficiencyRuleTree();
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("ruleName"));// 制度名称：
		return title;
	}

}
