package epros.designer.gui.common.treeCheckbox;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.tree.TreeCellRenderer;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnTreeNode;

public class CheckBoxTreeCellRenderer extends JPanel implements
		TreeCellRenderer {
	private static Logger log = Logger
			.getLogger(CheckBoxTreeCellRenderer.class);

	private JecnCheckBoxTree checkBoxTree;
	// 复选框
	protected JCheckBox checkbox;
	protected CheckBoxTreeLabel label;

	public static ImageIcon project;// 项目
	public static ImageIcon processRoot;// 流程根节点
	public static ImageIcon processMap = new ImageIcon("images/treeNodeImages/"
			+ TreeNodeType.processMap + ".gif");// 流程地图
	public static ImageIcon processMapNoPub = new ImageIcon(
			"images/treeNodeImages/" + TreeNodeType.processMap + "NoPub.gif");// 流程地图未发布
	public static ImageIcon process = new ImageIcon("images/treeNodeImages/"
			+ TreeNodeType.process + ".gif");// 流程
	public static ImageIcon processNoPub = new ImageIcon(
			"images/treeNodeImages/" + TreeNodeType.process + "NoPub.gif");// 流程未发布
	public static ImageIcon processFileNoPub = new ImageIcon(
			"images/treeNodeImages/" + TreeNodeType.processFile + "NoPub.gif");// 流程未发布
	public static ImageIcon organizationRoot;// 组织根节点
	public static ImageIcon organization;// 组织
	public static ImageIcon position;// 岗位
	public static ImageIcon person;// 人员
	public static ImageIcon standardRoot;// 标准根节点
	public static ImageIcon standardDir;// 标准目录
	public static ImageIcon standard;// 标准
	public static ImageIcon standardProcess;// 标准流程
	public static ImageIcon standardProcessMap;// 标准流程地图
	public static ImageIcon standardProcessNoPub;// 标准流程
	public static ImageIcon standardProcessMapNoPub;// 标准流程地图
	public static ImageIcon standardClause;// 标准条款
	public static ImageIcon standardClauseRequire;// 标准条款
	public static ImageIcon ruleRoot;// 制度根节点
	public static ImageIcon ruleDir;// 制度目录
	public static ImageIcon ruleFile;// 制度文件
	public static ImageIcon ruleFileNoPub;// 制度文件未发布
	public static ImageIcon ruleModeFile;// 制度模板文件
	public static ImageIcon ruleModeFileNoPub;// 制度模板文件未发布
	public static ImageIcon fileRoot;// 文件根节点
	public static ImageIcon fileDir;// 文件目录
	public static ImageIcon file;// 文件
	public static ImageIcon fileNoPub;// 文件
	public static ImageIcon positionGroupRoot;// 岗位组根节点
	public static ImageIcon positionGroupDir;// 岗位组目录
	public static ImageIcon positionGroup;// 岗位组
	public static ImageIcon roleRoot; // 角色根节点
	public static ImageIcon roleDir;// 角色目录
	public static ImageIcon role;// 角色
	public static ImageIcon roleDefaultDir;// 默认角色目录
	public static ImageIcon roleDefault;// 默认角色节点
	public static ImageIcon toolRoot;// 支持工具根节点
	public static ImageIcon tool;// 支持工具
	public static ImageIcon ruleModeRoot;// 制度模板根节点
	public static ImageIcon ruleModeDir;// 制度模板目录
	public static ImageIcon ruleMode; // 制度模板
	public static ImageIcon processModeRoot; // 流程模板根节点
	public static ImageIcon processMapMode; // 流程地图模板
	public static ImageIcon processMode; // 流程图模板
	public static ImageIcon processModeFileRoot;// 流程模板文件根节点
	public static ImageIcon processModeFileDir; // 流程模板文件目录
	public static ImageIcon processModeFile; // 流程模板文件
	public static ImageIcon modeRoot;// 流程模板管理根节点
	public static ImageIcon riskRoot;// 风险根目录
	public static ImageIcon riskDir;// 风险目录
	public static ImageIcon riskPoint;// 风险点
	public static ImageIcon innerControlRoot;// 内控根目录
	public static ImageIcon innerControlDir;// 内控目录
	public static ImageIcon innerControlClause;// 内控条款
	/***节点类型 0是流程地图；1是流程；2是组织 */
	private int nodeType = -1;
	/**节点ID*/
	private Long id;
	public int getNodeType() {
		return nodeType;
	}
	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public CheckBoxTreeCellRenderer(JecnCheckBoxTree checkBoxTree) {
		setLayout(null);
		
		label = new CheckBoxTreeLabel();
		checkbox = new JCheckBox();
		checkbox.setBackground(UIManager.getColor("Tree.textBackground"));
		label.setForeground(UIManager.getColor("Tree.textForeground"));
		
		this.checkBoxTree = checkBoxTree;
		nodeType = checkBoxTree.getNodeType();
		id = checkBoxTree.getTreeId();
	}

	/**
	 * 返回的是一个<code>JPanel</code>对象，该对象中包含一个<code>JCheckBox</code>对象 和一个
	 * <code>JLabel</code>对象。并且根据每个结点是否被选中来决定<code>JCheckBox</code> 是否被选中。
	 */
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		String stringValue = tree.convertValueToText(value, selected, expanded,
				leaf, row, hasFocus);
		setEnabled(tree.isEnabled());
		//节点添加显示名称
		add(label);
		
		checkbox.setSelected(((JecnTreeNode) value).isSelected());
		label.setFont(tree.getFont());
		label.setText(stringValue);
		label.setSelected(selected);
		label.setFocus(hasFocus);
		if (leaf)
			label.setIcon(UIManager.getIcon("Tree.leafIcon"));
		else if (expanded)
			label.setIcon(UIManager.getIcon("Tree.openIcon"));
		else
			label.setIcon(UIManager.getIcon("Tree.closedIcon"));

		JecnTreeNode node = (JecnTreeNode) value;
//		if(node.getJecnTreeBean().getIsDelete() == 0){
//			checkbox.setVisible(false);
//		}
		
		if(node.getJecnTreeBean().getIsDelete() == 0){
			add(checkbox);
//			checkbox.setVisible(false);
		}
		try {
			JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
			
			
			// 树节点类型的字符串值
			TreeNodeType treeNodeType = jecnTreeBean.getTreeNodeType();
			String treeNodeTypeString = jecnTreeBean.getTreeNodeType()
					.toString();
			if ((treeNodeType == TreeNodeType.process
					|| treeNodeType == TreeNodeType.processMap
					|| treeNodeType == TreeNodeType.ruleFile
					|| treeNodeType == TreeNodeType.ruleModeFile
					|| treeNodeType == TreeNodeType.file
					|| treeNodeType == TreeNodeType.standardProcess || treeNodeType == TreeNodeType.standardProcessMap)
					&& !jecnTreeBean.isPub()) {
				if (null == CheckBoxTreeCellRenderer.class.getField(
						treeNodeTypeString + "NoPub").get(null)) {
					CheckBoxTreeCellRenderer.class.getField(
							treeNodeTypeString + "NoPub").set(
							treeNodeTypeString,
							new ImageIcon("images/treeNodeImages/"
									+ treeNodeTypeString + "NoPub.gif"));
				}

			} else {
				if (null == CheckBoxTreeCellRenderer.class.getField(treeNodeTypeString)
						.get(null)) {
					CheckBoxTreeCellRenderer.class.getField(treeNodeTypeString).set(
							treeNodeTypeString,
							new ImageIcon("images/treeNodeImages/"
									+ jecnTreeBean.getTreeNodeType().toString()
									+ ".gif"));
				}

			}
			//复选框置灰
			if(jecnTreeBean.getIsDelete() == 0){
				checkbox.setEnabled(false);
			}else{
				checkbox.setEnabled(true);
			}
			if ((treeNodeType == TreeNodeType.process
					|| treeNodeType == TreeNodeType.processMap
					|| treeNodeType == TreeNodeType.ruleFile
					|| treeNodeType == TreeNodeType.ruleModeFile
					|| treeNodeType == TreeNodeType.file
					|| treeNodeType == TreeNodeType.standardProcess || treeNodeType == TreeNodeType.standardProcessMap)
					&& !jecnTreeBean.isPub()) {//&&JecnConstants.isPubShow()
				// 设置图标
				label.setIcon((Icon) CheckBoxTreeCellRenderer.class.getField(
						treeNodeTypeString + "NoPub").get(null));
			} else {
				// 设置图标
				label.setIcon((Icon) CheckBoxTreeCellRenderer.class.getField(
						treeNodeTypeString).get(null));

			}

		} catch (Exception e) {
			log.error("CheckBoxTreeCellRenderer getTreeCellRendererComponent is error", e);
		}

		return this;
	}

	@Override
	public Dimension getPreferredSize() {
		Dimension dCheck = new Dimension(18, 15);
		Dimension dLabel = label.getPreferredSize();
		return new Dimension(dCheck.width + dLabel.width,
				dCheck.height < dLabel.height ? dLabel.height : dCheck.height);
	}

	@Override
	public void doLayout() {
		Dimension dCheck = new Dimension(18, 15);
		Dimension dLabel = label.getPreferredSize();
		int yCheck = 0;
		int yLabel = 0;
		if (dCheck.height < dLabel.height)
			yCheck = (dLabel.height - dCheck.height) / 2;
		else
			yLabel = (dCheck.height - dLabel.height) / 2;
		checkbox.setLocation(0, yCheck);
		checkbox.setBounds(0, yCheck, dCheck.width, dCheck.height);
		label.setLocation(dCheck.width, yLabel);
		label.setBounds(dCheck.width, yLabel, dLabel.width, dLabel.height);
	}

	@Override
	public void setBackground(Color color) {
		if (color instanceof ColorUIResource)
			color = null;
		super.setBackground(color);
	}

	public JecnCheckBoxTree getCheckBoxTree() {
		return checkBoxTree;
	}

	public void setCheckBoxTree(JecnCheckBoxTree checkBoxTree) {
		this.checkBoxTree = checkBoxTree;
	}
}
