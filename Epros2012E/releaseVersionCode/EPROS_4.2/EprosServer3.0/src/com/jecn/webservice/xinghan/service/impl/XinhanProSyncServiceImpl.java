package com.jecn.webservice.xinghan.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.jecn.webservice.common.SpecialTreatment;
import com.jecn.webservice.util.AddSoapHeader;
import com.jecn.webservice.util.PropertiesUtil;
import com.jecn.webservice.xinghan.XinghanNodeUtil;
import com.jecn.webservice.xinghan.bean.FlowInfo;
import com.jecn.webservice.xinghan.bean.FlowLink;
import com.jecn.webservice.xinghan.bean.FlowNode;
import com.jecn.webservice.xinghan.bean.Point;
import com.jecn.webservice.xinghan.dao.IXinghanProcessSynchronous;
import com.jecn.webservice.xinghan.service.IXinghanProSyncService;

@Component("XinhanProSyncServiceImpl")
public class XinhanProSyncServiceImpl extends SpecialTreatment implements
		IXinghanProSyncService {

	private IXinghanProcessSynchronous xingHanProcessSynchronous;

	/***************************************************************************
	 * 所有的流程元素集合
	 * 
	 */
	private List<Object[]> flowImageAllList = new ArrayList<Object[]>();

	/**
	 * 流程信息
	 */
	private List<FlowInfo> flowInfos = new ArrayList<FlowInfo>();
	/**
	 * 流程元素
	 */
	private List<FlowNode> flowNodes = new ArrayList<FlowNode>();

	/**
	 * 连接线
	 */
	private List<FlowLink> flowLinks = new ArrayList<FlowLink>();

	/**
	 * 浦沅同步的所有流程属性对象
	 * */
	private FlowInfo flowInfo = null;

	Logger log = Logger.getLogger(XinhanProSyncServiceImpl.class);

	/**
	 * 流程推送总方法
	 * 
	 * @param flowId
	 *            流程编号
	 * @return 0:失败 1:成功
	 * @author chehuanbo
	 */
	public int SysncFlowToBps(Long flowId) {
		flowInfo = new FlowInfo();
		flowLinks.clear();
		flowNodes.clear();
		boolean isFlag = false;
		if (flowId == null) {
			log.error("待推送的flowId为空，同步失败！");
			return 1;
		}
		// ======================当前流程的所有流程元素=======
		log.info("流程推送开始！");
		flowImageAllList = xingHanProcessSynchronous.findAllByFlowImageId(flowId);

		super.setFlowImageAllList(flowImageAllList);

		findDottedRectAll();// 调用处理特殊情况方法
		findFlowMapOrLFowInfo(flowId); // 获取当前流程信息以及流程所属的流程架构信息
		pdFlowFigureType(); // 判断流程元素类型,转换成BPS所识别的类型

		flowInfo.setFlowLink(flowLinks);
		flowInfo.setFlowNode(flowNodes);

		log.info("生成XML开始！");
		// 执行同步
		String xml = XinghanNodeUtil.createXml(flowInfo);
		log.info("创建webService客户端开始！");
		try {
			// 动态创建客户端
			Client client = null;
			// 创建客户端工厂
			JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory
					.newInstance();
			client = dcf.createClient(PropertiesUtil.getProperties()
					.getProperty("wsdlAddress"));
			log.info("创建webService客户端结束！");

			AddSoapHeader addSoapHeader = new AddSoapHeader(PropertiesUtil
					.getProperties());
			log.info("创建webService安全头结束！");
			client.getOutInterceptors().add(addSoapHeader);
			
			if (client == null) {
				return 0;
			}
			Object[] objects = client.invoke("CreateFlowcharts", xml);
			String resultXml = "";
			for (int i = 0; i < objects.length; i++) {
				resultXml += objects[i];
			}
			isFlag = XinghanNodeUtil.parseXml(resultXml);
			if (isFlag) {
				return 1;
			}
			log.info(resultXml);

		} catch (Exception e) {
			log.error("流程同步失败，BpsProSyncServiceImpl SysncFlowToBps()", e);
			return 0;
		}
		log.info("流程推送结束，结果：" + isFlag);
		return 0;

	}

	/**
	 * 查询当前流程所属的流程架构和当前流程信息
	 * 
	 * @author chehuanbo
	 */
	private void findFlowMapOrLFowInfo(Long flowId) {
		try {
			// ===========获取当前流程信息以及流程所属的流程架构====
			List<Object[]> objects = xingHanProcessSynchronous
					.findAllByFlowMapId(flowId);
			if (objects == null) {
				return;
			}
			int i = 0;
			for (Object[] obj : objects) {
				if (i > 0 && i < 2) {
					if (obj[1] != null) {
						flowInfo.setFlowId(Long.parseLong(obj[1].toString()));
					}
					if (obj[3] != null) {
						flowInfo.setFlowName(obj[3].toString());
					}
					if (obj[4] != null) {
						flowInfo.setFlowNo(obj[4].toString());
					}
				} else if (i == 0) {
					if (obj[1] != null) {
						flowInfo.setFlowCatId(Long.parseLong(obj[1].toString()));
					}
					if (obj[3] != null) {
						flowInfo.setFlowCatName(obj[3].toString());
					}
					if (obj[4] != null) {
						flowInfo.setFlowCatNo(obj[4].toString());
					}
				}
				i++;
			}
		} catch (NumberFormatException e) {
			log.error("生成流程架构信息和流程信息出现异常,BpsProSyncServiceImpl findFlowMapOrLFowInfo()",
							e);
		}
	}

	/****************Xinghan所识别的类型
	 * 
	 * @param objMap
	 *            当前流程下的所有的流程元素
	 * 
	 **************************************************************************/
	public void pdFlowFigureType() {
		try {
			for (Object[] objects : flowImageAllList) {
				String figure = objects[2].toString();
				if(figure == null){
					continue;
				}
				if ("RoundRectWithLine".equals(figure)) { // 活动
					flowNodeFz(objects, 1);
				} else if ("CommonLine".equals(figure)) { // 连接线
					flowLink(objects, 0);
				} else if ("ManhattanLine".equals(figure)) {// 连接线带箭头
					flowLink(objects, 0);
				} else if ("RightArrowhead".equals(figure)) { // 返出
					flowLink(objects, 1);
				} else if ("Rhombus".equals(figure)) {// 决策
					flowNodeFz(objects, 1);
				} else if ("ImplFigure".equals(figure)) { // 接口
					flowNodeFz(objects, 1);
				} else if ("OvalSubFlowFigure".equals(figure)) { // 子流程
					flowNodeFz(objects, 1);
				} else if ("DataImage".equals(figure)) { // 信息系统
					flowNodeFz(objects, 1);
				} else if ("ToFigure".equals(figure)) { // To跳转
					flowLink(objects, 0);
				} else if ("EndFigure".equals(figure)) { // end结束
					flowNodeFz(objects, 8);
				} else if ("XORFigure".equals(figure)) { // XOR
					flowNodeFz(objects, 4);
				} else if ("ORFigure".equals(figure)) { // OR
					flowNodeFz(objects, 4);
				} else if ("FlowFigureStart".equals(figure)) { // 流程开始
					flowNodeFz(objects, 0);
				} else if ("FlowFigureStop".equals(figure)) { // 流程结束
					flowNodeFz(objects, 8);
				} else if ("AndFigure".equals(figure)) { // AND
					flowNodeFz(objects, 5);
				} else if ("ITRhombus".equals(figure)) { // IT决策
					flowNodeFz(objects, 1);
				} else if ("EventFigure".equals(figure)) { // 事件
					flowNodeFz(objects, 1);
				}
			}
		} catch (Exception e) {
			log.error("流程元素类型,转换成BPS所识别的类型出现异常! XinhanProSyncServiceImpl pdFlowFigureType()",
							e);
		}
	}

	/**
	 * 生成流程元素
	 * 
	 * @param objects
	 *            流程元素 nodeType 节点类型
	 */
	public void flowNodeFz(Object[] objects, int nodeType) {

		try {
			FlowNode flowNode = new FlowNode();

			if (objects[1] != null) {
				flowNode.setNodeId(Long.parseLong(objects[1].toString()));// 节点ID
			}

			if (objects[3] != null) {
				flowNode.setNodeName(objects[3].toString()); // 节点名称
			}
			if (objects[6] != null) { // x坐标
				flowNode.setxNodePoint(Integer.parseInt(objects[6].toString()));
			}

			if (objects[7] != null) { // y坐标
				flowNode.setyNodePoint(Integer.parseInt(objects[7].toString()));
			}

			flowNode.setNodeType(String.valueOf(nodeType));
			flowNodes.add(flowNode);
		} catch (NumberFormatException e) {
			log.error("生成流程元素出现异常！XinhanProSyncServiceImpl flowNodeFz()", e);
		}

	}

	/**
	 * 生成流程元素连接线
	 * 
	 * @param objects
	 *            单个流程元素（连接线） linkType 连接线类型
	 */
	public void flowLink(Object[] objects, int linkType) {

		try {

			// 首先查找当前连接是否连接在协作框上

			// ============生成连接线开始=========================
			// 流程线属性bean
			FlowLink flowLink = new FlowLink();
			if (objects[1] != null) {
				// 连接线编号
				flowLink.setLinkId(Long.parseLong(objects[1].toString()));
			}

			if (objects[3] != null) {
				// 连接线名称
				flowLink.setLinkName(objects[3].toString()); // 连接线名称
			}

			// 连接线类型
			flowLink.setLinkType(linkType); // 连接线类型
			if (linkType == 0) { // 不是返工符
				// 连接线折点集合
				List<Point> points = new ArrayList<Point>();
				// 起始元素
				if (objects[4] == null && objects[5] == null) {
					return;
				}
				flowLink.setFromNodeId(Long.parseLong(objects[4].toString()));
				// 结束元素
				flowLink.setToNodeId(Long.parseLong(objects[5].toString()));
				// =================生成连接线折点开始=============================
				// 获取连接线的折点
				List<Object[]> objLink = xingHanProcessSynchronous
						.findSegmentId(Long.parseLong(objects[1].toString()));
				if (objLink != null && objLink.size() != 0) {
					for (Object[] objectLink : objLink) {
						Point point = new Point();
						if (objectLink[1] != null && objectLink[2] != null) {
							point.setxLinkPoint(Integer.parseInt(objectLink[1]
									.toString()));
							point.setyLinkPoint(Integer.parseInt(objectLink[2]
									.toString()));
						}
						points.add(point);
					}
				}
				flowLink.setPoint(points);
			} else if (linkType == 1) { // 返工符
				// 首先根据返出符号查找有没有对应的返入符号
				for (Object[] objRev : flowImageAllList) {
					// 找到对应的返入符
					if ((objRev[3] != null && objects[3] != null)
							&& (objRev[3].toString().equals(objects[3]
									.toString()))
							&& (objRev[2].toString())
									.equals("ReverseArrowhead")) {
						// 找到返出符最近的元素
						Object[] RightArrowhead = rArrowhead(objects, Double
								.parseDouble(objects[11].toString()),
								flowImageAllList);
						Object[] ReverseArrowhead = rArrowhead(objRev, Double
								.parseDouble(objRev[11].toString()),
								flowImageAllList);
						// 找到返入最近的元素
						if (RightArrowhead != null && ReverseArrowhead != null) {
							if (RightArrowhead[1] != null
									&& ReverseArrowhead[1] != null) {
								// 起始元素
								flowLink
										.setFromNodeId(Long
												.parseLong(RightArrowhead[1]
														.toString()));
								// 结束元素
								flowLink.setToNodeId(Long
										.parseLong(ReverseArrowhead[1]
												.toString()));
							}
						}
						break;
					}
				}
			}
			// =======================end
			// 结束======================================
			flowLinks.add(flowLink);
		} catch (NumberFormatException e) {
			log.error("生成流程元素连接线出现异常，BpsProSyncServiceImpl flowLink()", e);
		}
	}

	public IXinghanProcessSynchronous getBpsProcessSynchronous() {
		return xingHanProcessSynchronous;
	}

	@Resource
	public void setBpsProcessSynchronous(
			IXinghanProcessSynchronous bpsProcessSynchronous) {
		this.xingHanProcessSynchronous = bpsProcessSynchronous;
	}

	public List<FlowInfo> getFlowInfos() {
		return flowInfos;
	}

	public void setFlowInfos(List<FlowInfo> flowInfos) {
		this.flowInfos = flowInfos;
	}

	public List<FlowNode> getFlowNodes() {
		return flowNodes;
	}

	public void setFlowNodes(List<FlowNode> flowNodes) {
		this.flowNodes = flowNodes;
	}

	public List<FlowLink> getFlowLinks() {
		return flowLinks;
	}

	public void setFlowLinks(List<FlowLink> flowLinks) {
		this.flowLinks = flowLinks;
	}

}
