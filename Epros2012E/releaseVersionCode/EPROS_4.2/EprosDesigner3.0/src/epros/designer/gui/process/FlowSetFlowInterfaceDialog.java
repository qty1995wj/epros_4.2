package epros.designer.gui.process;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.process.mode.choose.FlowModeChooseDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 设置流程接口
 * 
 * @author Administrator 2012-11-14
 */
public class FlowSetFlowInterfaceDialog extends JecnDialog {
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();

	/** * 类型Lab */
	private JLabel typeLab = new JLabel(JecnProperties.getValue("typeC"));

	/** 类型ComboBox */
	private JComboBox typeCombox = new JComboBox(new String[] { JecnProperties.getValue("topFlow"),
			JecnProperties.getValue("bottomFlow"), JecnProperties.getValue("interfaceFlow"),
			JecnProperties.getValue("sonFlow") });

	/** * 关联流程Lab */
	private JLabel relatedFlowLab = new JLabel(JecnProperties.getValue("relatedFlowC"));

	/** * 关联流程Filed */
	private JTextField relatedFlowField = new JTextField();

	/** * 关联流程按钮 */
	private JButton relatedFlowBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 确定按钮 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** * 清空 */
	// private JButton clearBut = new
	// JButton(JecnProperties.getValue("emptyBtn"));
	/** 设置连接关联的流程ID */
	private Long relationId = null;
	private JecnBaseFigurePanel elementPanel;
	private MapType mapType;
	/** 流程编号 **/
	private String numberId = "";

	public FlowSetFlowInterfaceDialog(JecnBaseFigurePanel elementPanel, MapType mapType) {
		this.elementPanel = elementPanel;
		// 设置连接关联的流程ID
		relationId = elementPanel.getFlowElementData().getDesignerFigureData().getLinkId();
		// 已存在设置连接
		relatedFlowField.setText(elementPanel.getFlowElementData().getFigureText());
		String lineType = elementPanel.getFlowElementData().getDesignerFigureData().getLineType();
		if (!DrawCommon.isNullOrEmtry(lineType)) {
			typeCombox.setSelectedIndex(Integer.valueOf(lineType) - 1);
		} else {
			typeCombox.setSelectedIndex(0);
		}
		this.mapType = mapType;
		this.setSize(500, 160);
		this.setTitle(JecnProperties.getValue("setFlowInterface"));
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setModal(true);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		relatedFlowField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		relatedFlowField.setEditable(false);
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		// 确定

		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		relatedFlowBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				relatedFlowButPerformed();
			}
		});
		initLayout();
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());

		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		// 按钮
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);

		// 类型

		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(typeLab, c);
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(typeCombox, c);

		// 关联流程
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(relatedFlowLab, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(relatedFlowField, c);
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(relatedFlowBut, c);
		c = new GridBagConstraints(0, 2, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(new JLabel(), c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(okBut, c);
		// buttonPanel.add(clearBut, c);
		buttonPanel.add(cancelBut, c);

		this.getContentPane().add(mainPanel);
	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelButPerformed() {
		// 关闭窗体
		this.dispose();
	}

	/***************************************************************************
	 * 确定
	 * 
	 * @param args
	 */
	private void okButPerformed() {
		if (!isUpdate()) {
			this.dispose();
			return;
		}
		int index = typeCombox.getSelectedIndex();
		// 元素增加链接ID
		if (relationId != null) {
			elementPanel.getFlowElementData().getDesignerFigureData().setLinkId(relationId);
		}
		if (!DrawCommon.isNullOrEmtryTrim(relatedFlowField.getText())) {
			elementPanel.getFlowElementData().setFigureText(
					JecnTreeCommon.getShowName(relatedFlowField.getText(), numberId));
			elementPanel.updateUI();
		}
		switch (index) {
		case 0:
			elementPanel.getFlowElementData().getDesignerFigureData().setLineType("1");
			break;
		case 1:
			elementPanel.getFlowElementData().getDesignerFigureData().setLineType("2");
			break;
		case 2:
			elementPanel.getFlowElementData().getDesignerFigureData().setLineType("3");
			break;
		case 3:
			elementPanel.getFlowElementData().getDesignerFigureData().setLineType("4");
			break;
		default:
			break;
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		if (relationId != null) {
			JecnDesignerCommon.addElemLink(elementPanel, mapType);
		}
		this.dispose();
	}

	private boolean isUpdate() {
		int index = typeCombox.getSelectedIndex() + 1;
		if (!String.valueOf(index).equals(elementPanel.getFlowElementData().getDesignerFigureData().getLineType())) {
			return true;
		}
		if (relationId == null) {
			relationId = 0L;
		}
		Long oldLinkId = elementPanel.getFlowElementData().getDesignerFigureData().getLinkId();
		if (oldLinkId == null) {
			oldLinkId = 0L;
		}
		if (!relationId.equals(oldLinkId)) {
			return true;
		}
		return false;
	}

	/***************************************************************************
	 * 关联流程
	 * 
	 * @param args
	 */
	private void relatedFlowButPerformed() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		JecnSelectChooseDialog d = null;
		if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getDesignerData().getModeType() != ModeType.none) {
			d = new FlowModeChooseDialog(list, 11);
		} else {
			if (relationId != null) {
				// 设置连接默认显示已存在的值
				JecnTreeBean bean = new JecnTreeBean();
				bean.setId(relationId);
				bean.setName(elementPanel.getFlowElementData().getFigureText());
				list.add(bean);
			}
			d = new ProcessChooseDialog(list);
			// 设置只能选择流程13
			d.setChooseType(13);

		}
		d.removeEmptyBtn();
		d.setSelectMutil(false);
		d.setVisible(true);
		if (d.isOperation()) {
			if (list.size() == 1) {
				JecnTreeBean treeBean = list.get(0);
				relationId = treeBean.getId();
				numberId = treeBean.getNumberId();
				relatedFlowField.setText(treeBean.getName());
			}
		}
	}
}
