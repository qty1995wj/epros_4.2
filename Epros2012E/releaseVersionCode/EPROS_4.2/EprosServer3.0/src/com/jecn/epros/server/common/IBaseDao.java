package com.jecn.epros.server.common;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.Type;

/**
 * 
 * @author yxw 2012-4-24
 * @description：BaseDao 接口类，每建一个具体的Dao接口都要继承此接口
 * @param <M>
 * @param <PK>
 */
public interface IBaseDao<M extends java.io.Serializable, PK extends java.io.Serializable> {
	/**
	 * @author yxw 2012-4-24
	 * @description: 获得org.hibernate.SessionFactory
	 * @return
	 */
	public SessionFactory getSessionFactory();

	/**
	 * @author yxw 2012-4-24
	 * @description:获得org.hibernate.Session
	 * @return
	 */
	public Session getSession();

	/**
	 * @author yxw 2012-4-24
	 * @description:保存bean对象到数据库
	 * @param model
	 *            :bean对象
	 * @return
	 */
	public PK save(M model);

	/**
	 * @author yxw 2012-4-24
	 * @description: 保存或修改一条数据记录，数所库无此数据则保存，有则修改
	 * @param model
	 */
	public void saveOrUpdate(M model);

	/**
	 * @author yxw 2012-4-24
	 * @description:修改一条数据记录
	 * @param model
	 *            :bean对象
	 */
	public void update(M model);

	/**
	 * @author yxw 2012-4-24
	 * @description:托管一条数据记录
	 * @param model
	 */
	public void merge(M model);

	/**
	 * @author yxw 2012-4-24
	 * @description:根据主键ID删除一条记录
	 * @param id
	 *            :主键ID
	 */
	public void delete(PK id);

	/**
	 * @author yxw 2012-4-24
	 * @description:在数据库删除bean对象
	 * @param model
	 *            :bean对象
	 */
	public void deleteObject(M model);

	/**
	 * @author yxw 2012-4-24
	 * @description:根据主键ID查询，返回对应实例
	 * @param id
	 * @return
	 */
	public M get(PK id);

	/**
	 * @author yxw 2012-4-24
	 * @description:根据主键ID判断数据库是否有此记录
	 * @param id
	 *            ：主键ID
	 * @return true or false
	 */
	boolean exists(PK id);

	/**
	 * @author yxw 2012-4-24
	 * @description:查询一个表数据总数
	 * @return 总记录数
	 */
	public int countAll();

	/**
	 * @author yxw 2012-4-24
	 * @description:查询一个表所有数据集
	 * @return list数据集
	 */
	public List<M> listAll();

	/**
	 * @author yxw 2012-4-24
	 * @description: 查询满足一定条件的数据总数
	 * @param hql
	 * @param params
	 *            可扩展的条件值
	 * @return
	 */
	public int countAllByParamsHql(String hql, Object... params);

	public int countAllByParamsNativeSql(String sql, Object... params);

	/**
	 * @author yxw 2012-4-24
	 * @description:
	 * @param <T>
	 *            必须保证返回是一个对象（如：数据总数，一个对象，一个对象的某一字段），不能是一个数据集合
	 * @param hql
	 * @param params
	 *            :可扩展的条件值
	 * @return
	 */
	public <T> T getObjectHql(String hql, Object... params);

	public <T> T getObjectNativeSql(String sql, Object... params);

	/**
	 * @author yxw 2012-4-24
	 * @description:查询满足一定条件的数据集
	 * @param <T>
	 *            结果集中的对象类型
	 * @param hql
	 * @param params
	 *            :可扩展的条件值
	 * @return
	 */
	public <T> List<T> listHql(String hql, Object... params);

	public <T> List<T> listNativeSql(String sql, Object... params);

	/**
	 * @author yxw 2012-4-24
	 * @description:分页查询满足一定条件的数据集
	 * @param <T>:结果集中的对象类型
	 * @param hql
	 * @param start
	 *            ：开始行数
	 * @param pageSize
	 *            ：每页显示条数
	 * @param params
	 *            ：可扩展的条件值
	 * @return
	 */
	public <T> List<T> listHql(String hql, int start, int pageSize, Object... params);

	public <T> List<T> listNativeSql(String sql, int start, int pageSize, Object... params);

	/**
	 * @author yxw 2012-4-24
	 * @description: 得到一个org.hibernate.Query对象
	 * @param hql
	 * @return
	 */

	public int execteHql(String hql, final Object... paramlist);

	public int execteNative(String hql, final Object... paramlist);

	public Query getQuery(String hql);

	/**
	 * @author yxw 2012-4-24
	 * @description:得到一个org.hibernate.SQLQuery对象
	 * @param sql
	 * @return
	 */
	public SQLQuery getSQLQuery(String sql);

	/**
	 * @author yxw 2012-11-14
	 * @description:本地SQL查询，可直接转化为BEAN
	 * @param sql
	 * @return
	 */
	public List<M> listNativeAddEntity(String sql, final Object... paramlist);

	public List<?> listNativeAddEntity(String sql, Class<?> z, Object... paramlist);

	public <T> List<T> listSQLServerWith(String withSql, String selectSql, String orderByName, int start, int pageSize,
			Object... params);

	public <T> List<T> listSQLServerSelect(String selectSql, String orderByName, int start, int pageSize,
			Object... params);

	public Map<String, Object> getNativeResultToMap(final String sql, final Object... paramlist);

	public List<Map<String, Object>> getNativeResultToListMap(final String sql, final Object... paramlist);

	public <T> List<T> getNativeResultToBeanListMap(final String sql, Class target, final Object... params);

	public <T> List<T> listObjectNativeSql(String sql, String param, Type type, Object... params);
}
