package com.jecn.epros.server.webBean.reports;

import java.util.List;

import com.jecn.epros.server.webBean.process.ProcessRoleWebBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 流程活动数统计
 * 
 * @author fuzhh Feb 28, 2013
 * 
 */
public class ProcessApplicationDetailsBean {

	/**
	 *流程对象
	 */
	private ProcessWebBean processWebBean;

	/**
	 * 角色对象集合
	 */
	private List<ProcessRoleWebBean> listProcessRoleWebBean;

	public ProcessWebBean getProcessWebBean() {
		return processWebBean;
	}

	public void setProcessWebBean(ProcessWebBean processWebBean) {
		this.processWebBean = processWebBean;
	}

	public List<ProcessRoleWebBean> getListProcessRoleWebBean() {
		return listProcessRoleWebBean;
	}

	public void setListProcessRoleWebBean(
			List<ProcessRoleWebBean> listProcessRoleWebBean) {
		this.listProcessRoleWebBean = listProcessRoleWebBean;
	}

}
