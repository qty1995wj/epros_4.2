<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% response.setHeader("Pragma","No-cache"); response.setHeader("Cache-Control","no-cache");%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript">
    	window.onresize = function() {setShowRiskSystem('riskDivList');}
		var basePath = "${basePath}";
	</script>
  </head>
  
  <body>
    <div id="div" class="div" style="position: relative;overflow-y:scroll; overflow-x:hidden; height:93%;">
    
    	<!-- 风险详情Start -->
    	
    	<div id="riskDetailDiv" style="margin: 0px 15px 0px 0px; float: left; width: 100%; position: relative;">
    		<table style="width:100%; padding-right:30px;">
    			<tr>
    				<td class="td_dashed" width="150px;">
    					<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
    					<!-- 风险编号 -->
    					<s:text name="riskNumberC" />：
    				</td>
    				<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
						&nbsp;${riskDetailBean.riskCode }
					</td>
    			</tr>
    			<tr>
    				<td class="td_dashed" width="150px;">
    					<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
    					<!-- 风险描述 -->
    					<s:text name="riskName" />：
    				</td>
    				<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
						&nbsp;${riskDetailBean.name }
					</td>
    			</tr>
    			<tr>
    				<td class="td_dashed" width="150px;">
    					<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
    					<!-- 风险等级 -->
    					<s:text name="riskGrade" />：
    				</td>
    				<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
						&nbsp;${riskDetailBean.gradeStr }
					</td>
    			</tr>
    			<tr>
    				<td class="td_dashed" width="150px;">
    					<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
    					<!-- 风险标准化控制 -->
    					<s:text name="riskStandardControl" />：
    				</td>
    				<td style="word-wrap: break-word; word-break: break-all; padding-right:30px;" class="td_dashed">
						&nbsp;${riskDetailBean.standardControl }
					</td>
    			</tr>
    			<tr>
    				
    				<table style="width:100%; margin-left:8px; margin-right:10px; padding-right:20px;">
    					<tr>
    						<td width="100%;">
    							<img src="${basePath}images/common/triangle.gif"
										style="vertical-align: middle;">
		    					<!-- 控制目标描述 -->
		    					<s:text name="DetailGuide" />：
    						</td>
    					</tr>
    					<tr>
    						<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
								${riskDetailBean.controlGuide }
							</td>
    					</tr>
    				</table>
    				
    				<%--<td class="td_dashed" width="150px;">
    					<img src="${basePath}images/common/triangle.gif"
								style="vertical-align: middle;">
    					<!-- 控制目标描述 -->
    					<s:text name="DetailGuide" />：
    				</td>
    				<td style="word-wrap: break-word; word-break: break-all;" class="td_dashed">
						&nbsp;${riskDetailBean.controlGuide }
					</td>
    			--%></tr>
    		</table>
    	</div>
    	
    	<!-- 风险详情End -->
    	
    	<!-- 相关流程Start -->
    	
    	<div style="position: relative;padding-top:100px;">
    		<table width="98%">
				<tr>
					<td style="font: bold 15px ; padding-left: 12px;">
						相关流程
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="1" align="center" bgcolor="#D0D0D0" style="table-layout: fixed;word-WRAP: break-word">
				<tr class="FixedTitleRow" align="center">
					<td width="2%" class="gradient"></td>
					<td height="25" width="15%" class="gradient">控制目标</td>
					<td height="25" class="gradient">控制编号</td>
					<td height="25" width="15%" class="gradient">控制活动简描述</td>
					<td height="25" class="gradient">活动名称</td>
					<td height="25" class="gradient">流程名称</td>
					<td height="25" class="gradient">责任部门</td>
					<td height="25" class="gradient">控制方法</td>
					<td height="25" class="gradient">控制类型</td>
					<td height="25" class="gradient">是否是关键控制</td>
					<td height="25" class="gradient">控制频率</td>
					<td width="2%" class="gradient"></td>
				</tr>
				<s:set id="count" value="1" />
				<s:iterator value="listRiskRelatedProcessBean" var="list">
    				<tr align="center" bgcolor="#ffffff" onmouseover="style.backgroundColor='#EFEFEF';" onmouseout="style.backgroundColor='#ffffff';" height="20">
    					<td width="2%" class="gradient"><s:property value="#count" />
							<s:set id="count" value="#count+1" /></td>
	    				<td><s:property value="targetDescription"/></td>
	    				<td><s:property value="controlCode"/></td>
	    				<td style="text-align:left;overflow: hidden; text-overflow:ellipsis;word-break:keep-all;white-space:nowrap;"><s:property value="activeShow"/></td>
	    				<td><a href="${basePath}processActive.action?activeId=<s:property value='activityId'/>" style="cursor: pointer;" target='_blank' class="table"><s:property value="figureText"/></a></td>
	    				<td><a href="${basePath}process.action?type=process&flowId=<s:property value='flowId'/>" target='_blank' style="cursor: pointer;" class="table"><s:property value="flowName"/></a></td>
	    				<td><a href="organization.action?orgId=<s:property value='orgId'/>" target='_blank' class="table"><s:property value="orgName"/></a></td>
	    				<td><s:property value="strMethod"/></td>
	    				<td><s:property value="strType"/></td>
	    				<td><s:property value="keyPoin"/></td>
	    				<td><s:property value="strFrequency"/></td>
	    				<td width="2%" class="gradient"></td>
    				</tr>
    			</s:iterator>
			</table>
    	</div>
    	
    	<!-- 相关流程End -->
    	
    	<!-- 相关制度Start -->
    	
    	<div style="position: relative;overflow:auto; padding-bottom:15px;">
    		<table width="98%">
				<tr>
					<td style="font: bold 15px ; padding-left: 12px;">
						相关制度
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="1" align="center" bgcolor="#D0D0D0" style="table-layout: fixed;word-WRAP: break-word;">
				<tr class="FixedTitleRow" align="center">
					<td width="2%" class="gradient"></td>
					<td height="25" class="gradient">制度名称</td>
					<td height="25" class="gradient">制度编号</td>
					<td height="25" class="gradient">责任部门</td>
					<s:if test="#request.ruleTypeIsShow">
					<td height="25" class="gradient">制度类别</td>
					</s:if>
					<td width="2%" class="gradient"></td>
				</tr>
				<s:set id="count" value="1" />
				<s:iterator value="riskRelatedRuleBean">
    				<tr align="center" bgcolor="#ffffff" onmouseover="style.backgroundColor='#EFEFEF';" onmouseout="style.backgroundColor='#ffffff';" height="20">
    					<td width="2%" class="gradient"><s:property value="#count" />
							<s:set id="count" value="#count+1" /></td>
	    				<td><a target='_blank' href="rule.action?ruleId=<s:property value="ruleId"/>" class="table"><s:property value="ruleName"/></a></td>
	    				<td><s:property value="ruleNumber"/></td>
	    				<td><a href="organization.action?orgId=<s:property value='orgId'/>" target='_blank' class="table"><s:property value="orgName"/></a></td>
	    				<s:if test="#request.ruleTypeIsShow">
	    				<td><s:property value="ruleType"/></td>
	    				</s:if>
	    				<td width="2%" class="gradient"></td>
    				</tr>
    			</s:iterator>
			</table>
    	</div>
    	
    	<!-- 相关制度End -->
    	
    </div>
  </body>
</html>
