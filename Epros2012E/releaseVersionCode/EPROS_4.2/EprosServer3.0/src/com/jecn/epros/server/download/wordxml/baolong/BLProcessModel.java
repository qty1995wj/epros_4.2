package com.jecn.epros.server.download.wordxml.baolong;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellBean;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;

/**
 * 保隆操作说明下载
 * 
 * @author admin
 * 
 */
public class BLProcessModel extends ProcessFileModel {
	/*** 段落行距 1.5倍 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1.5F,
			LineRuleType.AUTO);

	/**** 段落行距最小值16磅值 ********/
	private ParagraphLineRule vLine_atLeast16 = new ParagraphLineRule(16F,
			LineRuleType.AT_LEAST);

	/*** 段落行距单倍行距自动 *****/
	private ParagraphLineRule vLine2 = new ParagraphLineRule(1F,
			LineRuleType.AUTO);

	/***** 段落空格换行属性bean *******/
	private ParagraphBean newLineBean;

	public BLProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		getDocProperty().setNewTblWidth(17.47F);
		getDocProperty().setFlowChartScaleValue(9F);
		initDocumentStyle();
		super.setDocStyle("、", textTitleStyle());
	}

	protected void initDocumentStyle() {
		/***** 段落空格换行属性bean 宋体 小四 最小值16磅值 *******/
		newLineBean = new ParagraphBean(Align.left, new FontBean("宋体",
				Constants.xiaosi, false));
		newLineBean.setSpace(0f, 0f, vLine_atLeast16);
	}

	/**
	 * 创建文本项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	@Override
	protected void createTextItem(ProcessFileItem processFileItem,
			String... content) {
		super.createTextItem(processFileItem, content);
		processFileItem.getBelongTo().createParagraph("", newLineBean);
	}

	/*****
	 * 创建表格项
	 * 
	 * @param contentSect
	 * @param fs
	 * @param list
	 * @return
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem,
			float[] width, List<String[]> dataRow) {
		Table tab = super.createTableItem(processFileItem, width, dataRow);
		processFileItem.getBelongTo().createParagraph("", newLineBean);
		processFileItem.getBelongTo().createParagraph("", newLineBean);
		return tab;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(2.7F, 2.7F, 2.7F, 2.7F, 1.4F, 1.4F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
		createCommhdr(sect, HeaderOrFooterType.odd);

	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {
		Table table = null;
		Paragraph p = null;
		// 宋体 五号
		ParagraphBean pBean = new ParagraphBean("宋体", Constants.wuhao, false);
		// 宋体 小四
		ParagraphBean pBean_xiaoshi = new ParagraphBean("宋体", Constants.xiaosi,
				false);
		List<String[]> rowData = new ArrayList<String[]>();
		Header hdr = sect.createHeader(type);
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		String flowInputNum = processDownloadBean.getFlowInputNum();
		String flowVersion = processDownloadBean.getFlowVersion();
		String releaseDate = processDownloadBean.getReleaseDate();
		String ectiveDate = processDownloadBean.getEffectiveDate();
		// String companyName = processDownloadBean.getCompanyName();
		String companyName = "保 隆 管 理 体 系 文 件";
		String flowName = processDownloadBean.getFlowName();
		rowData.add(new String[] { "", "文件编号", flowInputNum });
		rowData.add(new String[] { "", "文件版本", flowVersion });
		rowData.add(new String[] { "", "发布日期", releaseDate });
		rowData.add(new String[] { "", "生效日期", ectiveDate });
		rowData.add(new String[] { "", "页    码", "第" });
		table = JecnWordUtil.createTab(hdr, tabBean, new float[] { 10.69F,
				2.62F, 4.37F }, rowData, pBean);
		table.getRow(0).getCell(0).setRowspan(5);
		for (int i = 0; i < table.getRowCount(); i++) {
			table.getRow(i).setHeight(0.5F);
		}
		/*** 宋体 小二 加粗 单倍行距 ******/
		ParagraphBean tempPBean = new ParagraphBean(Align.center, "宋体",
				Constants.xiaoer, true);
		tempPBean.setSpace(0f, 0f, vLine2);
		/*** 宋体 二号加粗 单倍行距 ******/
		ParagraphBean temp1PBean = new ParagraphBean(Align.center, "宋体",
				Constants.erhao, true);
		temp1PBean.setSpace(0f, 0f, vLine2);
		/***** 公司名称 ****/
		table.getRow(0).getCell(0).getParagraph(0).appendTextRun(companyName);
		table.getRow(0).getCell(0).getParagraph(0).setParagraphBean(temp1PBean);
		/***** 流程名称 ****/
		p = table.getRow(0).getCell(0).createParagraph(flowName, tempPBean);
		p = table.getRow(4).getCell(2).getParagraph(0);
		p.appendCurPageRun();
		p.appendTextRun("页 共  ");
		p.appendTotalPagesRun();
		p.appendTextRun(" 页");
		hdr.createParagraph("");

		// 右边第二个 三个 单元格样式
		CellBean rightCellBean = new CellBean();
		rightCellBean.setBorderBottom(1);
		CellBean rightCellTitleBean = new CellBean();
		rightCellTitleBean.setBorderTop(2);
		rightCellTitleBean.setBorderBottom(1);

		// 左边第一个单元格样式
		CellBean leftCellBean = new CellBean();
		leftCellBean.setBorderBottom(1);
		leftCellBean.setBorderRight(1);
		CellBean leftCellTitleBean = new CellBean();
		leftCellTitleBean.setBorderTop(2);
		leftCellTitleBean.setBorderBottom(1);
		leftCellTitleBean.setBorderRight(1);
		for (int i = 0; i < table.getRowCount(); i++) {
			if (i == 0) {
				table.getRow(i).getCell(0).setCellBean(leftCellTitleBean);
				table.getRow(i).getCell(1).setCellBean(rightCellTitleBean);
				table.getRow(i).getCell(2).setCellBean(rightCellTitleBean);
			} else {
				table.getRow(i).getCell(0).setCellBean(leftCellBean);
				table.getRow(i).getCell(1).setCellBean(rightCellBean);
				table.getRow(i).getCell(2).setCellBean(rightCellBean);
			}
			// 宋体 小四
			table.getRow(i).getCell(1).getParagraph(0).setParagraphBean(
					pBean_xiaoshi);
			table.getRow(i).getCell(2).getParagraph(0).setParagraphBean(
					pBean_xiaoshi);
		}
		//hdr.createParagraph("");
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体",
				Constants.xiaosi, false);
		Footer ftr = sect.createFooter(type);
		ftr.createParagraph("公司机密,未经批准不得扩散", pBean);
	}

	@Override
	protected void initFirstSect(Sect sect) {
		sect.setSectBean(getSectBean());
		createCommhdrftr(sect);
		for (int i = 0; i < 3; i++) {
			sect.createParagraph("", newLineBean);
		}
	}

	@Override
	protected void initFlowChartSect(Sect sect) {
	}

	@Override
	protected void initSecondSect(Sect sect) {
	}

	@Override
	protected void initTitleSect(Sect sect) {
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体",
				Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0, 0, 0, 0);
		// 表格左缩进0.01厘米
		tblStyle.setTableLeftInd(0.01F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体",
				Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	// 宋体, 五号，加粗，行间距1.5倍
	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体",
				Constants.wuhao, false);
		textContentStyle.setSpace(0f, 0f, vLine1);
		return textContentStyle;
	}

	// 宋体, 五号，加粗，行间距1.5倍
	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("宋体", Constants.wuhao,
				true);
		textTitleStyle.setSpace(0f, 0f, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

}
