package epros.draw.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;

public class DrawCommon {

	/**
	 * 
	 * 获取UUID
	 * 
	 * @return String UUID
	 */
	public static String getUUID() {
		return UUID.randomUUID().toString();
	}

	/**
	 * 
	 * 判断字符串是否相等
	 * 
	 * @param str1
	 *            String 字符串1
	 * @param str2
	 *            String 字符串2
	 * @return boolean true：相等 false：不相等
	 */
	public static boolean checkStringSame(String str1, String str2) {

		if (DrawCommon.isNullOrEmtryTrim(str1) && DrawCommon.isNullOrEmtryTrim(str2)) {// 都为空
			return true;
		} else if (!DrawCommon.isNullOrEmtryTrim(str1) && str1.equals(str2)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkLongSame(Long long1, Long long2) {
		if (long1 == null && long2 == null) {// 都为空
			return true;
		} else if (long1 != null && long1.equals(long2)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkBigDecimalSame(BigDecimal decimal1, BigDecimal decimal2) {
		if (decimal1 == null && decimal2 == null) {// 都为空
			return true;
		} else if (decimal1 != null && decimal2 != null && decimal2.compareTo(decimal1) == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkDateSame(Date date1, Date date2) {
		if (date1 == null && date2 == null) {
			return true;
		}
		if (date1 != null && date2 != null && getStringbyDate(date1).equals(getStringbyDate(date2))) {
			return true;
		}
		return false;
	}

	/**
	 * 日期转换成字符串时间格式 到时分秒
	 * 
	 * @param date
	 *            Date日期
	 * @return String 日期格式
	 */
	public static String getStringbyDate(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	/**
	 * 
	 * 校验给定的颜色是否相等
	 * 
	 * @param oldColor
	 *            Color 颜色1
	 * @param newColor
	 *            Color 颜色2
	 * @return boolean true:颜色1和颜色2相等；false:颜色1和颜色2不一样
	 */
	public static boolean checkColorSame(Color oldColor, Color newColor) {
		return ((oldColor == null && newColor == null) || (oldColor != null && oldColor.equals(newColor))) ? true
				: false;
	}

	/**
	 * 
	 * 给定颜色RGB字符串返回Color对象
	 * 
	 * @param colorStr
	 *            String [A,B,C]格式
	 * @return Color 参数为空返回画图面板面板颜色，否则返回和参数对应的颜色
	 */
	public static Color covertStringToColor(String colorStr) {
		if (DrawCommon.isNullOrEmtryTrim(colorStr)) {
			return null;
		}
		// RBC
		String[] eleArrs = colorStr.split(",");
		if (eleArrs == null || eleArrs.length != 3) {
			return null;
		}
		return new Color(coverStringToInt(eleArrs[0]), coverStringToInt(eleArrs[1]), coverStringToInt(eleArrs[2]));
	}

	/**
	 * 
	 * 
	 * 字符串数字转换成整形
	 * 
	 * @param eleStr
	 *            String 字符串数字
	 * @return int
	 */
	public static int coverStringToInt(String eleStr) {
		if (DrawCommon.isNullOrEmtryTrim(eleStr)) {
			return 0;
		}
		return Integer.valueOf(eleStr);
	}

	/**
	 * 
	 * 颜色对象转换成RGB字符串
	 * 
	 * @param color
	 *            Color 颜色对象
	 * @return String [A,B,C]格式 或“”字符串
	 */
	public static String coverColorToString(Color color) {
		if (color == null) {
			return "";
		}
		return String.valueOf(color.getRed() + "," + color.getGreen() + "," + color.getBlue());
	}

	/**
	 * 
	 * 整形0、1转换成boolean型true、false
	 * 
	 * 0:false 1:true
	 * 
	 * @param param
	 *            int (0,1)
	 * @return boolean 0:false 1:true
	 */
	public static boolean converIntToBoolean(int param) {
		return (param == 0) ? false : true;
	}

	/**
	 * 
	 * boolean型true、false 转换成整形0、1
	 * 
	 * 0:false 1:true
	 * 
	 * @param param
	 *            boolean
	 * @return int 0:false 1:true
	 */
	public static int converBooleanToInt(boolean param) {
		return param ? 1 : 0;
	}

	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 *            String
	 * @return boolean str为空返回ture，不为空返回false
	 */
	public static boolean isNullOrEmtryTrim(String str) {
		return (str == null || str.trim().isEmpty()) ? true : false;
	}

	/**
	 * 
	 * 给定参数判断是否为null或空,如果需要去空再判断使用方法isNullOrEmtryTrim
	 * 
	 * @param str
	 *            String
	 * @return boolean str为空返回ture，不为空返回false
	 */
	public static boolean isNullOrEmtry(String str) {
		return (str == null || str.isEmpty()) ? true : false;
	}

	/**
	 * 
	 * 判断给定参数：空、指针或添加符号三种返回false
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return boolean mapElemType是能添加的图形返回ture，为空或是指针或添加符号返回false
	 */
	public static boolean isFlowElementType(MapElemType mapElemType) {
		if (mapElemType == null || "guide".equals(mapElemType.toString()) || "addFigure".equals(mapElemType.toString())) {
			// 指针 添加符号
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 名称不能大于61个汉字或122字母
	 * 
	 * @param name
	 * @return 超过返回true，没有超过返回false
	 */
	public static boolean checkNameMaxLength(String name) {
		return (JecnUserCheckUtil.getTextLength(name) > 122) ? true : false;
	}

	/**
	 * 
	 * 密码不能大于6个字符
	 * 
	 * @param name
	 * @return 超过返回true，没有超过返回false
	 */
	public static boolean checkPasswordMaxLength(String name) {
		return (JecnUserCheckUtil.getTextLength(name) > 19) ? true : false;
	}

	/***************************************************************************
	 * 验证输入内容不能大于600个汉字或1200字符
	 * 
	 * @param name
	 * @return 超过返回true，没有超过返回false
	 */
	public static boolean checkNoteLength(String name) {
		return (JecnUserCheckUtil.getTextLength(name) > 1200) ? true : false;
	}

	/***************************************************************************
	 * 验证输入内容不能大于5000个汉字或10000字符
	 * 
	 * @param name
	 * @return 超过返回true，没有超过返回false
	 */
	public static boolean checkNoteMaxLength(String name) {
		return (JecnUserCheckUtil.getTextLength(name) > 10000) ? true : false;
	}

	
	/**
	 * 
	 * 获取给定值的四舍五入（精确到小数点后3位）的值
	 * 
	 * @param value
	 *            double
	 * @return double
	 */
	public static double convertDouble(double value) {
		long valLong = Math.round(value * 1000); // 四舍五入
		return valLong / 1000.0;
	}

	/**
	 * 
	 * 四舍五入获取double值的int值
	 * 
	 * @param doubleObj
	 * @return int
	 */
	public static int convertDoubleToInt(double doubleObj) {
		return (int) Math.floor(doubleObj + 0.5);
	}

	/**
	 * 传入String型的Color 返回Color
	 * 
	 * @param tone
	 *            String的 color
	 * @return
	 */
	public static Color getColorByString(String tone) {
		Color color = null;
		String colorStr[] = tone.split(",");
		if (colorStr.length > 0) {
			color = new Color(Integer.valueOf(colorStr[0]), Integer.valueOf(colorStr[1]), Integer.valueOf(colorStr[2]));
		}
		return color;
	}

	/**
	 * 获取线的连接点类型
	 * 
	 * @param elemType
	 *            线的枚举类型 1左 2上 3右 4下
	 * @return
	 */
	public static int getLineType(EditPointType pointType) {
		int lineType = 4;
		if (EditPointType.left == pointType) {
			lineType = 1;
		} else if (EditPointType.top == pointType) {
			lineType = 2;
		} else if (EditPointType.right == pointType) {
			lineType = 3;
		} else {
			lineType = 4;
		}
		return lineType;
	}

	/**
	 * 获取线的连接点类型
	 * 
	 * @param elemType
	 *            线的枚举类型 1左 2上 3右 4下
	 * @return
	 */
	public static EditPointType getLineType(String pointType) {
		EditPointType lineType = null;
		if ("1".equals(pointType)) {
			lineType = EditPointType.left;
		} else if ("2".equals(pointType)) {
			lineType = EditPointType.top;
		} else if ("3".equals(pointType)) {
			lineType = EditPointType.right;
		} else {
			lineType = EditPointType.bottom;
		}
		return lineType;
	}

	/**
	 * 获取线的连接点类型
	 * 
	 * @param elemType
	 *            线的枚举类型 1左 2上 3右 4下
	 * @return
	 */
	public static EditPointType getLineType(int pointType) {
		EditPointType lineType = null;
		if (1 == pointType) {
			lineType = EditPointType.left;
		} else if (2 == pointType) {
			lineType = EditPointType.top;
		} else if (3 == pointType) {
			lineType = EditPointType.right;
		} else {
			lineType = EditPointType.bottom;
		}
		return lineType;
	}

	/**
	 * 反射获取类中指定方法
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static Object invokeMethod(String name, String strMethod) throws Exception {
		// 获取class
		Class<?> cl = Class.forName(name);
		// 创建对象
		Object obj = cl.newInstance();
		// 获取方法
		Method method = cl.getMethod(strMethod);
		return method.invoke(obj);//
	}

	/**
	 * 是否可设置连接
	 * 
	 * @return true:存在设置连接
	 */
	public static boolean isInfoFigure(MapElemType mapElemType) {
		if (mapElemType == null) {
			return false;
		}
		switch (mapElemType) {
		case Oval:// 流程地图：圆形 9
		case Triangle: // 流程地图 三角形 10
		case FunctionField: // 流程地图 矩形功能域 11
		case Pentagon: // 五边形 12
		case RightHexagon: // 箭头六边形 13
		case ModelFigure: // 泳池 14
		case InputHandFigure: // 手动输入 19
		case SystemFigure: // 制度 20
		case IsoscelesTrapezoidFigure: // 等腰梯形
		case FileImage:// 文档
		case MapRhombus:// 菱形
			return true;
		}
		return false;
	}

	/**
	 * 字符串的回车符转化成HTML中的换行符<BR>
	 * 
	 * @param string
	 *            活动说明或关键活动说明
	 * @return 替换后的字符串
	 */
	public static StringBuffer getsubstring(String string) {
		StringBuffer stringBuffer = new StringBuffer();
		if (DrawCommon.isNullOrEmtryTrim(string)) {
			return stringBuffer;
		}
		String[] str = string.split("\n");
		for (int i = 0; i < str.length; i++) {
			int s = 0;
			int stringLength = 50;
			if (str[i].length() == 0) {
				s = str[i].length() / stringLength;
			} else {
				s = str[i].length() / stringLength + 1;
			}
			for (int j = 0; j < s; j++) {
				if (str[i].length() < stringLength) {
					string = str[i] + "\n";
				} else {
					string = str[i].substring(0, stringLength) + "\n";
					str[i] = str[i].substring(stringLength);
				}
				string = string.replaceAll("\n", "<br>");
				stringBuffer.append(string);
			}
		}
		return stringBuffer;
	}

	/**
	 * 字符串的回车符转化成HTML中的换行符<BR>
	 * 可添加空格符
	 * 
	 * @param string
	 *            活动说明或关键活动说明
	 * @return 替换后的字符串
	 */
	public static StringBuffer getTermSubstring(String string, String character) {
		StringBuffer stringBuffer = new StringBuffer();
		if (StringUtils.isBlank(string)) {
			return stringBuffer;
		}
		String[] str = string.split("\n");
		for (int i = 0; i < str.length; i++) {
			int s = 0;
			int stringLength = 50;
			if (str[i].length() == 0) {
				s = str[i].length() / stringLength;
			} else {
				s = str[i].length() / stringLength + 1;
			}
			for (int j = 0; j < s; j++) {
				if (str[i].length() < stringLength) {
					string = str[i] + "\n";
				} else {
					string = str[i].substring(0, stringLength) + "\n";
					str[i] = str[i].substring(stringLength);
				}
				string = string.replaceAll("\n", "<br>" + character);
				stringBuffer.append(string);
			}
		}
		return stringBuffer;
	}

	/**
	 * 元素不设置填充色
	 * 
	 * @param @param baseFlowElement
	 * @return boolean true:不设置填充色
	 * @author ZXH
	 * @date 2015-10-8 下午04:01:55
	 */
	public static boolean isNoSetFillColor(MapElemType mapElemType) {
		switch (mapElemType) {
		case IconFigure:
		case PartitionFigure:
			return true;
		default:
			return false;
		}
	}

	/**
	 * son 是否在 parent 的范围内
	 * 
	 * @param parent
	 * @param son
	 * @return
	 */
	public static boolean isInside(JecnBaseFlowElementPanel son, JecnBaseFlowElementPanel parent) {
		if (son.getX() < parent.getX()) {
			return false;
		}
		if (son.getX() + son.getWidth() > parent.getX() + parent.getWidth()) {
			return false;
		}
		if (son.getY() < parent.getY()) {
			return false;
		}
		if (son.getY() + son.getHeight() > parent.getY() + parent.getHeight()) {
			return false;
		}
		return true;
	}

	/**
	 * 是否为活动
	 * 
	 * @param mapElemType
	 * @return
	 */
	public static boolean isActive(MapElemType mapElemType) {
		if (isStandardActive(mapElemType) || isImplActive(mapElemType)) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为活动
	 * 
	 * @param mapElemType
	 * @return
	 */
	public static boolean isStandardActive(MapElemType mapElemType) {
		switch (mapElemType) {
		case RoundRectWithLine:
		case Rhombus:
		case DataImage:
		case ITRhombus:
		case ActiveFigureAR:
		case ExpertRhombusAR:
			return true;
		}
		return false;
	}

	/**
	 * 接口活动
	 * 
	 * @param mapElemType
	 * @return
	 */
	public static boolean isImplActive(MapElemType mapElemType) {
		switch (mapElemType) {
		case ActivityImplFigure:
		case ActivityOvalSubFlowFigure:
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 判断是否是角色
	 * 
	 * @param mapElemType
	 * @return boolean true：角色 false：不是角色
	 */
	public static boolean isRole(MapElemType mapElemType) {
		if (mapElemType == null) {
			return false;
		}
		switch (mapElemType) {
		case RoleFigure: // 角色 22
		case RoleFigureAR:
		case CustomFigure: // 客户 23
		case VirtualRoleFigure: // 虚拟角色 42
			return true;
		default:
			return false;
		}
	}

	/**
	 * 获取虚线画笔
	 * 
	 * @return
	 */
	public static BasicStroke getDottedLineStorke() {
		float[] dottedLineSpace = { 4.0f * 1 };
		return new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f, dottedLineSpace, 0.0f);
	}

	/**
	 * 获取创建流程,对应活动的类型(勘探院情况,活动为定制元素)
	 * 
	 * @return
	 */
	public static MapElemType getCreateFlowActiveType() {
		// 石勘院新增元素：新建流程应用活动是否改用新增活动（：启用1：不启用）',0)
		return JecnDesignerProcess.getDesignerProcess().isUserActiveByItem() ? MapElemType.ActiveFigureAR
				: MapElemType.RoundRectWithLine;
	}

	/**
	 * 创建流程,根据配置判断是角色类型(勘探院情况,角色为定制元素)
	 * 
	 * @return
	 */
	public static MapElemType getCreateFlowRoleType() {
		// 石勘院新增元素：新建流程应用活动是否改用新增活动（：启用1：不启用）',0)
		return JecnDesignerProcess.getDesignerProcess().isUserActiveByItem() ? MapElemType.RoleFigureAR
				: MapElemType.RoleFigure;
	}

	/**
	 * 是否存在3D和阴影效果
	 * 
	 * @return true 不存在3D和阴影
	 */
	public static boolean isNOShadowAnd3D(MapElemType mapElemType) {
		switch (mapElemType) {
		case DottedRect:
		case PAFigure:
		case KCPFigure:
		case KCPFigureComp:
		case KSFFigure:// 无3D和阴影
		case CommentText:
		case PartitionFigure:// 阶段分隔符
		case ModelFigure:// 泳池
		case FlowLevelFigureOne:
		case FlowLevelFigureTwo:
		case FlowLevelFigureThree:
		case FlowLevelFigureFour:
		case MapLineFigure:
			return true;
		}
		return false;
	}

	/**
	 * 设计器接口判断
	 * 
	 * @param mapElemType
	 * @return
	 */
	public static boolean isImplFigure(MapElemType mapElemType) {
		switch (mapElemType) {
		case ImplFigure:
		case OvalSubFlowFigure:
		case ActivityImplFigure:
		case ActivityOvalSubFlowFigure:
		case InterfaceRightHexagon:
			return true;
		}
		return false;
	}

	/**
	 * 标准的接口元素
	 * 
	 * @param elemType
	 * @return
	 */
	public static boolean isStandardImpl(MapElemType elemType) {
		return MapElemType.ImplFigure == elemType || MapElemType.OvalSubFlowFigure == elemType
				|| MapElemType.InterfaceRightHexagon == elemType;
	}

	/**
	 * 术语
	 * 
	 * @param elemType
	 * @return
	 */
	public static boolean isTermFigure(MapElemType elemType) {
		return MapElemType.TermFigureAR == elemType;
	}

	/**
	 * 活动是否存在于协作框的范围内
	 * 
	 * @param activeData
	 * @param dottedRectData
	 * @return
	 */
	public static boolean isActiveInDottedRectRange(JecnFigureData activeData, JecnFigureData dottedRectData) {
		// dottedRect Range data
		int minXOfDottedRect = dottedRectData.getFigureDataCloneable().getX();
		int maxXOfDottedRect = minXOfDottedRect + dottedRectData.getFigureSizeX();
		int minYOfDottedRect = dottedRectData.getFigureDataCloneable().getY();
		int maxYOfDottedRect = minYOfDottedRect + dottedRectData.getFigureSizeY();

		// active range data
		int minXOfActive = activeData.getFigureDataCloneable().getX();
		int maxXOfActive = minXOfActive + activeData.getFigureSizeX();
		int minYOfActive = activeData.getFigureDataCloneable().getY();
		int maxYOfActive = minYOfActive + activeData.getFigureSizeY();

		// check active range is in dottedRect range
		if (minXOfActive > minXOfDottedRect && maxXOfActive < maxXOfDottedRect && minYOfActive > minYOfDottedRect
				&& maxYOfActive < maxYOfDottedRect) {
			return true;
		}
		return false;
	}
}
