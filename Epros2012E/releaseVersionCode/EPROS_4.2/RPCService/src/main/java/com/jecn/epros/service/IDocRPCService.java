package com.jecn.epros.service;

import com.jecn.epros.bean.ByteFile;

import java.util.Map;

public interface IDocRPCService {

    public ByteFile downloadDoc(Map<String, Object> param);

    public ByteFile downloadExcel(Map<String, Object> param);

    public ByteFile downLoadDocContainEnclosure(Map<String, Object> param) throws Exception;
}
