package epros.designer.gui.system.config.ui.property.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.util.JecnProperties;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 表组件的滚动面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnTableScrollPane extends JScrollPane {
	protected final Log log = LogFactory.getLog(JecnTableScrollPane.class);
	/** 配置界面 */
	protected JecnAbtractBaseConfigDialog dialog = null;
	/** 表头 */
	protected String[] cloumnNames = null;
	/** table对象 */
	protected JTable table = null;
	/** table对应模型对象 */
	protected JecnDefaultTableModel tableModel = null;

	/** 数据对象 */
	protected List<JecnConfigItemBean> itemList = null;

	public JecnTableScrollPane(String headerName, JecnAbtractBaseConfigDialog dialog) {
		if (dialog == null) {
			log.error("JecnTableScrollPane is error");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.dialog = dialog;

		// 初始化组件
		initComponents(headerName);
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents(String headerName) {
		// 滚动面板
		this.setOpaque(false);
		this.setBorder(JecnUIUtil.getTootBarBorder());
		// 背景颜色
		this.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 初始化表
		initTable(headerName);

		this.setViewportView(table);
	}

	/**
	 * 
	 * 初始化表
	 * 
	 * @param headerName
	 *            String
	 */
	protected void initTable(String headerName) {
		// 表头
		String[] header = { (headerName == null) ? "" : headerName,JecnProperties.getValue("enName"), "id" };
		cloumnNames = header;
		// 创建table模型
		tableModel = new JecnDefaultTableModel(cloumnNames);
		// 创建表
		table = new JTable(tableModel);

		// 设置用户是否可以拖动列头，以重新排序各列,设置为false
		table.getTableHeader().setReorderingAllowed(false);
		// 设置表头背景颜色
		table.getTableHeader().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置没有ID列的宽度
		int index = getBeanIndex();
		table.getColumnModel().getColumn(index).setPreferredWidth(0);
		table.getColumnModel().getColumn(index).setMinWidth(0);
		table.getColumnModel().getColumn(index).setMaxWidth(0);

		// 自定义表头UI
		table.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	/**
	 * 
	 * 加载表数据
	 * 
	 * @param itemList
	 *            List<JecnConfigItemBean>
	 */
	public void initData(List<JecnConfigItemBean> itemList) {
		setDataByBean(itemList);
	}

	/**
	 * 
	 * 获取选中行中最后一列对象
	 * 
	 * @return
	 */
	public List<JecnConfigItemBean> getSelectedItemBeanList() {

		List<JecnConfigItemBean> iTemBeanList = new ArrayList<JecnConfigItemBean>();

		// 获取选中序号集合
		int[] selectedRows = getSelectedRows();

		for (int i = 0; i < selectedRows.length; i++) {
			// 选中行序号
			int selectedIndex = selectedRows[i];
			// 获取最后一列的选中数据
			Object obj = this.table.getValueAt(selectedIndex, getBeanIndex());
			if (obj instanceof JecnConfigItemBean) {
				iTemBeanList.add((JecnConfigItemBean) obj);
			}
		}
		return iTemBeanList;
	}

	/**
	 * 
	 * 重新设置选中行
	 * 
	 * @param selectedItemBeanList
	 *            List<JecnConfigItemBean> 选中集合
	 */
	public void reSetSelectedItems(List<JecnConfigItemBean> selectedItemBeanList) {
		if (selectedItemBeanList == null || selectedItemBeanList.size() == 0) {
			return;
		}

		for (int i = 0; i < table.getRowCount(); i++) {
			Object obj = this.table.getValueAt(i, getBeanIndex());
			if (obj instanceof JecnConfigItemBean) {
				for (JecnConfigItemBean selItemBean : selectedItemBeanList) {
					if (selItemBean == obj) {
						// 选中行
						table.addRowSelectionInterval(i, i);
					}
				}
			}
		}
	}

	/**
	 * 根据给定的序号添加选中行,如果选中行超过界限选中边界行
	 * 
	 */
	public void setSelectedRowToTable(int[] selectedIndexs) {

		if (selectedIndexs == null || selectedIndexs.length == 0) {
			return;
		}
		// 表行数
		int rowCount = table.getRowCount();
		if (rowCount <= 0) {
			return;
		}
		int maxRowIndex = rowCount - 1;
		for (int i = 0; i < selectedIndexs.length; i++) {
			int selectedIndex = selectedIndexs[i];
			if (selectedIndex >= 0 && selectedIndex <= maxRowIndex) {
				// 选中行
				table.addRowSelectionInterval(selectedIndex, selectedIndex);
			} else if (selectedIndex < 0) {
				// 小于下限
				table.addRowSelectionInterval(0, 0);
			} else {
				// 大于上限
				table.addRowSelectionInterval(maxRowIndex, maxRowIndex);
			}
		}
	}

	/**
	 * 
	 * 获取选中行
	 * 
	 * @return int[]
	 */
	public int[] getSelectedRows() {
		return table.getSelectedRows();
	}

	/**
	 * 
	 * 获取存放数据对象列序号
	 * 
	 * @return int
	 */
	public int getBeanIndex() {
		return cloumnNames.length - 1;
	}

	/**
	 * 
	 * 此方法适用于对应数据库表value字段存储是否显示0、1值
	 * 
	 * 获取显示项
	 * 
	 * @param pItemList
	 *            List<JecnConfigItemBean> 项集合
	 * @param type
	 *            int 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
	 * 
	 * @return List<JecnConfigItemBean>
	 */
	public List<JecnConfigItemBean> getShowItem(List<JecnConfigItemBean> pItemList, int type) {
		// 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
		List<JecnConfigItemBean> itemBeanList = null;
		switch (type) {
		case 0:// 0：审批环节配置
		case 4:// 4：流程文件
			itemBeanList = getShowItem(pItemList, true);
			break;
		case 1:// 1：任务界面显示项配置
		case 2:// 2：基本配置
		case 3:// 3：流程图建设规范
		case 5:// 5：邮箱配置
		case 6:// 6：权限配置
		case 7:// 7：任务操作
		case 16:// 16：流程KPI配置
			itemBeanList = getShowItem(pItemList, true);
			break;
		default:
			itemBeanList = new ArrayList<JecnConfigItemBean>();
			break;
		}
		return itemBeanList;
	}

	/**
	 * 
	 * 此方法使用于对应数据库表value字段存储是否显示0、1值
	 * 
	 * 获取隐藏项 隐藏项是否排序根据参数sort确定
	 * 
	 * @param pItemList
	 *            List<JecnConfigItemBean> 项集合
	 * @param type
	 *            int 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
	 * @return List<JecnConfigItemBean>
	 */
	public List<JecnConfigItemBean> getHiddItem(List<JecnConfigItemBean> pItemList, int type) {

		List<JecnConfigItemBean> itemBeanList = null;
		switch (type) {
		case 0:// 0：审批环节配置
		case 4:// 4：流程文件
			itemBeanList = getHiddItem(pItemList, true);
			break;
		case 1:// 1：任务界面显示项配置
		case 2:// 2：基本配置
		case 3:// 3：流程图建设规范
		case 5:// 5：邮箱配置
		case 6:// 6：权限配置
		case 7:// 7:任务操作
		case 16:
			itemBeanList = getHiddItem(pItemList, false);
			break;
		default:
			itemBeanList = new ArrayList<JecnConfigItemBean>();
			break;
		}
		return itemBeanList;
	}

	/**
	 * 
	 * 上移选中行
	 * 
	 */
	public void upShowUItem() {
		upDownShowUItem(true);
	}

	/**
	 * 
	 * 下移选中行
	 * 
	 */
	public void downShowUItem() {
		upDownShowUItem(false);
	}

	/**
	 * 
	 * 编辑表第一列选中数据
	 * 
	 * @param text1 中文名称
	 *            String 不能为NULL或空
	 *            @param text1 英文名称
	 */
	public void editName(String text1,String text2) {
		if (DrawCommon.isNullOrEmtryTrim(text1)) {
			return;
		}
		String text = text1.trim();
		String textEn = text2.trim(); //英文名称
		int[] selecIndex = getSelectedRows();
		if (selecIndex == null || selecIndex.length != 1) {
			return;
		}
		int index = selecIndex[0];

		// 选中名称
		Object selecName = table.getValueAt(index, 0);
		Object selecEnName = table.getValueAt(index, 1);

		if (text.equals(String.valueOf(selecName)) && textEn.equals(String.valueOf(selecEnName))) {// 给定值==原来值
			return;
		}
		// 选中行最后一列对象
		Object obj = table.getValueAt(index, getBeanIndex());
		if (obj instanceof JecnConfigItemBean) {
			// 获取最后一列的选中数据
			JecnConfigItemBean itemBean = (JecnConfigItemBean) obj;
			// 修改名称********************************
			itemBean.setName(text);
			itemBean.setEnName(textEn);
			// 获取第一列的选中数据
			table.setValueAt(text, index, 0);
			// 获取第一列的选中数据
			table.setValueAt(textEn, index, 1);
			table.repaint();
		}
	}

	/**
	 * 
	 * 给定名称是否有重复情况
	 * 
	 * @param name
	 *            String 给定名称
	 * @return boolean true:参数为空或选中行不为1或有相同行；false：没有重复名称
	 */
	public boolean existsNameSame(String name) {
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			return true;
		}

		// 获取选中行
		int[] selecIndex = getSelectedRows();
		if (selecIndex == null || selecIndex.length != 1) {
			return true;
		}
		int index = selecIndex[0];

		for (int i = 0; i < table.getRowCount(); i++) {
			if (index == i) {// 除去选中行不判断
				continue;
			}
			// 选中行第一列对象
			String rowName = String.valueOf(table.getValueAt(i, 0));

			if (name.trim().equals(rowName)) {// 名称出现相同
				return true;
			}

		}

		// 判断隐藏项是否有相同
		JecnConfigTypeDesgBean configTypeDesgBean = dialog.getPropertyPanel().getConfigTypeDesgBean();
		if (configTypeDesgBean == null) {
			return true;
		}
		List<JecnConfigItemBean> hiddItemBeanList = getHiddItem(configTypeDesgBean.getTableItemList(), false);
		for (JecnConfigItemBean itemBean : hiddItemBeanList) {
			if (name.trim().equals(itemBean.getName())) {// 名称出现相同
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * 给表赋值
	 * 
	 * @param itemList
	 *            List<JecnConfigItemBean>
	 */
	private void setDataByBean(List<JecnConfigItemBean> itemList) {
		this.itemList = itemList;

		// 清除表行
		DefaultTableModel df = (DefaultTableModel) table.getModel();
		// 反向删除
		for (int i = table.getRowCount() - 1; i >= 0; i--) {
			df.removeRow(i);
		}

		// 添加表行
		Object[][] tableData = getTableData(itemList);

		if (tableData == null) {
			return;
		}
		tableModel.setData(tableData);

		table.repaint();
	}

	/**
	 * 
	 * 获取表数据
	 * 
	 * @param itemList
	 *            List<JecnConfigItemBean>
	 * 
	 * @return Object[][]
	 */
	protected Object[][] getTableData(List<JecnConfigItemBean> itemList) {
		if (itemList == null || itemList.size() == 0) {
			return null;
		}
		// 创建数组
		Object[][] data = new Object[itemList.size()][cloumnNames.length];

		for (int i = 0; i < itemList.size(); i++) {
			JecnConfigItemBean itemBean = itemList.get(i);
			data[i][0] = itemBean.getName();
			data[i][1] = itemBean.getEnName();
			data[i][this.getBeanIndex()] = itemBean;
		}
		return data;
	}

	/**
	 * 
	 * 获取必填值
	 * 
	 * @param isEmpty
	 *            Integer
	 * @param notEmptyText
	 *            String
	 * @return String
	 */
	public String getNotEmpty(Integer isEmpty, String notEmptyText) {
		if (isEmpty == null) {
			return "";
		}
		return (isEmpty.intValue() == JecnConfigContents.ITEM_NOT_EMPTY) ? notEmptyText : "";
	}

	/**
	 * 
	 * 此方法使用于对应数据库表value字段存储是否显示0、1值
	 * 
	 * 获取显示项 显示项是否排序根据参数sort确定
	 * 
	 * @param pItemList
	 *            List<JecnConfigItemBean> 项集合
	 * @param sort
	 *            boolean true:需要排序；false：不需要排序
	 * 
	 * @return List<JecnConfigItemBean>
	 */
	private List<JecnConfigItemBean> getShowItem(List<JecnConfigItemBean> pItemList, boolean sort) {
		// 排序后的显示项集合
		List<JecnConfigItemBean> itemBeanList = new ArrayList<JecnConfigItemBean>();

		for (int i = 0; i < pItemList.size(); i++) {
			JecnConfigItemBean itemBean = pItemList.get(i);
			if (JecnConfigContents.ITEM_IS_SHOW.equals(itemBean.getValue())) {// 显示
				itemBeanList.add(itemBean);
			}
		}
		// 按照序号排序（对应数据库表value2字段是存放序号的数据）
		if (sort) {
			sortConfigItemBeanList(itemBeanList);
		}
		return itemBeanList;
	}

	/**
	 * 
	 * 此方法使用于对应数据库表value字段存储是否显示0、1值
	 * 
	 * 获取隐藏项 隐藏项是否排序根据参数sort确定
	 * 
	 * @param pItemList
	 *            List<JecnConfigItemBean> 项集合
	 * @param sort
	 *            boolean true:需要排序；false：不需要排序
	 * @return List<JecnConfigItemBean>
	 */
	private List<JecnConfigItemBean> getHiddItem(List<JecnConfigItemBean> pItemList, boolean sort) {
		// 排序后的不显示项集合
		List<JecnConfigItemBean> itemBeanList = new ArrayList<JecnConfigItemBean>();

		for (int i = 0; i < pItemList.size(); i++) {
			JecnConfigItemBean itemBean = pItemList.get(i);
			if (JecnConfigContents.ITEM_IS_NOT_SHOW.equals(itemBean.getValue())) {// 不显示项
				itemBeanList.add(itemBean);
			}
		}

		// 按照序号排序
		if (sort) {// （对应数据库表value2字段是存放序号的数据）
			sortConfigItemBeanList(itemBeanList);
		}
		return itemBeanList;
	}

	/**
	 * 
	 * 此方法适用于对应数据库表中value2字段是存放序号的数据
	 * 
	 * 给定参数按照序号排序
	 * 
	 * @param itemBeanList
	 *            List<JecnConfigItemBean>
	 */
	private void sortConfigItemBeanList(List<JecnConfigItemBean> itemBeanList) {

		if (itemBeanList == null || itemBeanList.size() == 0) {
			return;
		}

		Collections.sort(itemBeanList, new Comparator() {

			public int compare(Object r1, Object r2) {
				if (r1 == null || r2 == null) {
					throw new IllegalArgumentException("JecnTableScrollPane sortConfigItemBeanList is error");
				}

				// 获取序号
				Integer sort1 = ((JecnConfigItemBean) r1).getSort();
				Integer sort2 = ((JecnConfigItemBean) r2).getSort();

				if (sort1 == null || sort2 == null) {
					return 0;
				}
				int sortId1 = sort1.intValue();
				int sortId2 = sort2.intValue();

				if (sortId1 < sortId2) {
					return -1;
				} else if (sortId1 > sortId2) {
					return 1;
				} else {
					return 0;
				}
			}
		});
	}

	/**
	 * 
	 * 上下移动选中行
	 * 
	 * @param upFlag
	 *            boolean true：上移；false：下移
	 */
	private void upDownShowUItem(boolean upFlag) {
		// 获取选中显示项
		List<JecnConfigItemBean> selectedItemBeanList = this.getSelectedItemBeanList();
		// 选中行
		int[] selectedIndexs = null;
		if (upFlag) {// 上移
			selectedIndexs = upShowUItem(itemList, selectedItemBeanList);
		} else {// 下移
			selectedIndexs = downShowUItem(itemList, selectedItemBeanList);
		}

		// 重新加载表数据
		setDataByBean(itemList);

		// 设置选中行
		setSelectedRowToTable(selectedIndexs);
	}

	/**
	 * 
	 * 上移显示项
	 * 
	 * @param showItemBeanList
	 *            List<JecnConfigItemBean>
	 * @param selectedItemBeanList
	 *            List<JecnConfigItemBean>
	 * 
	 */
	private int[] upShowUItem(List<JecnConfigItemBean> showItemBeanList, List<JecnConfigItemBean> selectedItemBeanList) {
		if (showItemBeanList == null || showItemBeanList.size() == 0 || selectedItemBeanList == null
				|| selectedItemBeanList.size() == 0) {
			return null;
		}
		// 存储下一次选中行序号
		int[] selecteIndexs = new int[selectedItemBeanList.size()];

		// ******************************************start
		// 上移
		// 被选中第一行到表第一行就不移动了
		int index = showItemBeanList.indexOf(selectedItemBeanList.get(0));
		if (index <= 0) {
			for (int k = 0; k < selectedItemBeanList.size(); k++) {
				selecteIndexs[k] = showItemBeanList.indexOf(selectedItemBeanList.get(k));
			}
			return selecteIndexs;
		}

		// ******************************************end

		for (int k = 0; k < selectedItemBeanList.size(); k++) {

			// 获取选中行对象
			JecnConfigItemBean selectedItemBean = selectedItemBeanList.get(k);

			// 当前选中行
			int indexCurr = showItemBeanList.indexOf(selectedItemBean);

			// 上一行数据对象序号
			int upIndex = indexCurr - 1;

			// 上一行的bean对象
			JecnConfigItemBean upRowTtemBean = showItemBeanList.get(upIndex);
			// 判断上一行是否是选中，是选中那说明逻辑错误，返回null
			if (selectedItemBeanList.contains(upRowTtemBean)) {
				// 是选中行
				return null;
			}
			// 上一行或下一行的序号
			int sortId = upRowTtemBean.getSort();
			// 选中序号
			int selectedSortId = selectedItemBean.getSort();

			upRowTtemBean.setSort(selectedSortId);
			selectedItemBean.setSort(sortId);

			// 重新排序
			// 按照序号排序
			showItemBeanList.remove(selectedItemBean);
			showItemBeanList.add(upIndex, selectedItemBean);

			selecteIndexs[k] = upIndex;
		}
		return selecteIndexs;
	}

	/**
	 * 
	 * 下移显示项
	 * 
	 * @param showItemBeanList
	 *            List<JecnConfigItemBean>
	 * @param selectedItemBeanList
	 *            List<JecnConfigItemBean>
	 * 
	 */
	private int[] downShowUItem(List<JecnConfigItemBean> showItemBeanList, List<JecnConfigItemBean> selectedItemBeanList) {
		if (showItemBeanList == null || showItemBeanList.size() == 0 || selectedItemBeanList == null
				|| selectedItemBeanList.size() == 0) {
			return null;
		}
		// 存储下一次选中行序号
		int[] selecteIndexs = new int[selectedItemBeanList.size()];

		// ******************************************start
		// 被选中第最后一行到表最后一行就不移动了
		int index = showItemBeanList.indexOf(selectedItemBeanList.get(selectedItemBeanList.size() - 1));
		if (index >= showItemBeanList.size() - 1) {
			for (int k = 0; k < selectedItemBeanList.size(); k++) {
				selecteIndexs[k] = showItemBeanList.indexOf(selectedItemBeanList.get(k));
			}
			return selecteIndexs;
		}
		// ******************************************end

		for (int k = selectedItemBeanList.size() - 1; k >= 0; k--) {

			// 获取选中行对象
			JecnConfigItemBean selectedItemBean = selectedItemBeanList.get(k);

			// 当前选中行
			int indexCurr = showItemBeanList.indexOf(selectedItemBean);

			// 上一行数据对象
			int upIndex = indexCurr + 1;

			// 上一行的bean对象
			JecnConfigItemBean upRowTtemBean = showItemBeanList.get(upIndex);
			// 判断上一行是否是选中，是选中那说明逻辑错误，返回null
			if (selectedItemBeanList.contains(upRowTtemBean)) {
				// 是选中行
				return null;
			}

			// 上一行或下一行的序号
			int sortId = upRowTtemBean.getSort();
			// 选中序号
			int selectedSortId = selectedItemBean.getSort();

			upRowTtemBean.setSort(selectedSortId);
			selectedItemBean.setSort(sortId);

			// 重新排序
			// 按照序号排序
			showItemBeanList.remove(selectedItemBean);
			showItemBeanList.add(upIndex, selectedItemBean);

			selecteIndexs[k] = upIndex;
		}
		return selecteIndexs;
	}

}
