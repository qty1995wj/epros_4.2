package com.jecn.epros.server.bean.process.driver;

import java.io.Serializable;

/**
 * 驱动规则时间基础类
 * 
 * @author hyl
 * 
 */
public class JecnFlowDriverTimeBase implements Serializable {
	private String id;
	private Long flowId;
	private Integer month;
	private Integer week;
	private Integer day;
	private Integer hour;

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

}
