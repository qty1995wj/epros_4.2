package com.jecn.webservice.common;

public class TaskApiUrlHandler {

	/**
	 * 查看流程图、键盘图
	 * 
	 * @param id
	 * @return
	 */
	public static String getProcessUrl(String id, String isPub) {
		return "index.html?mapID=" + id + "&isPub=" + isPub;
	}

	/**
	 * 查看集成关系图
	 * 
	 * @param id
	 * @return
	 */
	public static String getIntegrationDiagram(String id, String isPub) {
		return "index.html?mapID=" + id + "&isPub=" + isPub + "&mapType=processMapRelated";
	}

	/**
	 * 查看流程说明文件
	 * 
	 * @param id
	 * @return
	 */
	public static String getProcessDoc(String id, String isPub) {
		return "processFile.action?flowId=" + id + "&isPub=" + isPub;
	}

	/**
	 * 查看制度
	 * 
	 * @param id
	 * @return
	 */
	public static String getRule(String id, String isPub) {
		return "loginMail.action?accessType=mailOpenDominoRuleModelFile&mailRuleId=" + id + "&isPub=" + isPub;
	}

	/**
	 * 查看活动
	 * 
	 * @param id
	 * @return
	 */
	public static String getAcvitity(String id, String isPub) {
		return "processActive.action?activeId=" + id;
	}

	/**
	 * 查看标准
	 * 
	 * @param id
	 * @return
	 */
	public static String getStandard(String id, String isPub) {
		return "standard.action?standardId=" + id;
	}

	/**
	 * 查看风险
	 * 
	 * @param id
	 * @return
	 */
	public static String getRisk(String id, String isPub) {
		return "getRiskDetailById.action?Id=" + id;
	}

	/**
	 * 查看文件（模板、操作规范）
	 * 
	 * @param id
	 * @return
	 */
	public static String getFile(String id, String isPub) {
		return "loginMail.action?accessType=mailOpenDominoFile&mailFileId=" + id + "&isPub=" + isPub;
	}

	/**
	 * 查看流程或架构详情 走单点认证
	 * 
	 * @param id
	 * @return
	 */
	public static String getProcessDetail(String id, String isPub) {
		return "loginMail.action?accessType=mailOpenDominoMap&mailFlowId=" + id + "&isPub=" + isPub;
	}

	public static String getDownLoadFile(String id, String isPub) {
		return "downloadFile.action?fileId=" + id + "&isPub=" + isPub;
	}

	/**
	 * 查看流程或架构详情 不走单点认证 根据用户ID验证，单点登录需先获取用户信息
	 * 
	 * @param id
	 * @return
	 */
	public static String getProcess_Map_Chart(String id, String isPub, String peopleId) {
		return "loginMail.action?accessType=mailPubApprove&mailFlowId=" + id + "&isPub=" + isPub + "&mailPeopleId="
				+ peopleId;
	}

	/**
	 * 查看制度 不走单点认证 根据用户ID验证，单点登录需先获取用户信息
	 * 
	 * @param id
	 * @return
	 */
	public static String getRule_Info(String id, String isPub, String peopleId) {
		return "loginMail.action?accessType=mailOpenRule&mailRuleId=" + id + "&isPub=" + isPub + "&mailPeopleId="
				+ peopleId;
	}

	/**
	 * 查看文件（模板、操作规范） 不走单点认证 根据用户ID验证，单点登录需先获取用户信息
	 * 
	 * @param id
	 * @return
	 */
	public static String getFile_Info(String id, String isPub, String peopleId) {
		return "loginMail.action?accessType=mailOpenFile&mailFileId=" + id + "&isPub=" + isPub + "&mailPeopleId="
				+ peopleId;
	}

}
