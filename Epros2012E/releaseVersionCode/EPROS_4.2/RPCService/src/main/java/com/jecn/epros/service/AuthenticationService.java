package com.jecn.epros.service;

import com.jecn.epros.bean.authetication.AuthenticationRequest;
import com.jecn.epros.bean.authetication.TokenAnalysis;

public interface AuthenticationService {

    void destroy(String token);

    String authentication(AuthenticationRequest request);

    TokenAnalysis analyze(String token);
}
