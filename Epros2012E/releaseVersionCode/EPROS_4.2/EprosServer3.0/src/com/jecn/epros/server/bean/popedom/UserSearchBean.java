package com.jecn.epros.server.bean.popedom;

public class UserSearchBean implements java.io.Serializable {
	/**主键ID*/
	private Long peopleId;
	/**登录名*/
	private String loginName;
	/**邮箱*/
	private String email;
	/**真实姓名*/
	private String trueName;
	/**是否登录 true是登录，false没有登录*/
	private boolean isLogin = false;
	public Long getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public boolean isLogin() {
		return isLogin;
	}
	public void setLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}
}
