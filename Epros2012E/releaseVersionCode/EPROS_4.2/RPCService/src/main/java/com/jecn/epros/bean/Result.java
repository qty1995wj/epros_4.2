package com.jecn.epros.bean;

public interface Result {

	public boolean isSuccess();

	public String getResultMessage();

}
