package epros.designer.gui.pagingImage;

import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.JecnPageSetLine;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnRoleMobile;
import epros.draw.util.DrawCommon;

public class PageUtil {
	/**
	 *角色移动宽度 (第一个竖线的位置点X)
	 * 
	 * @return
	 */
	public static int getReduceRoleWorkWidth() {
		int wx = 0;
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (workflow == null) {
			return wx;
		}
		// 获取第一条横分割线
		JecnBaseVHLinePanel wLinePanel = JecnRoleMobile.getFristVLine(workflow);
		if (wLinePanel != null) {// 有竖分割线
			// 宽
			wx = DrawCommon.convertDoubleToInt(wLinePanel.getX()
					+ wLinePanel.getWidth());
		} else {// 没有竖分割线
			// 宽
			wx = DrawCommon.convertDoubleToInt(150 * workflow.getWorkflowScale());
		}
		return wx;
	}

	/**
	 * 分页显示，每页宽度
	 * 
	 * @return
	 */
	public static int getPageWidth() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (workflow == null) {
			return 0;
		}
		// 面板宽度
		// 减去 角色移动宽度后的面板宽度
		int roleWidth = getReduceRoleWorkWidth();

		int pageSize = workflow.getFlowMapData().getPageSetingBean()
				.getPageSets();
		if (pageSize <= 1) {
			return 0;
		}
		// 分页显示 每页宽度
		return (workflow.getFlowMapData().getPageSetingBean().getPageX() - roleWidth)
				/ (pageSize - 1);

	}

	/**
	 * 删除分页符
	 * 
	 * @param listPageSet
	 */
	public static void removePageSetLine() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (desktopPane == null) {
			return;
		}
		for (JecnPageSetLine jecnPageSetLine : desktopPane.getPageSetLines()) {
			desktopPane.remove(jecnPageSetLine);
		}
	}
}
