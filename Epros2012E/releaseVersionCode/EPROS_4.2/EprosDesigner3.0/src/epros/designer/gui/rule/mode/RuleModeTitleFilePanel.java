package epros.designer.gui.rule.mode;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.util.JecnUIUtil;

public class RuleModeTitleFilePanel extends JPanel {

//	private JButton button=null;
	private JScrollPane scrollPane=null;
	private JecnTable  table=null;
	public RuleModeTitleFilePanel(String title){
		
		
		this.setLayout(new FlowLayout(FlowLayout.RIGHT));
//		this.button=new JButton(JecnProperties.getValue("selectBtn"));
//		this.setBorder(BorderFactory.createTitledBorder(
//				BorderFactory.createEmptyBorder(1, 1, 1, 1), title,
//				TitledBorder.DEFAULT_JUSTIFICATION,
//				TitledBorder.DEFAULT_POSITION,
//				new java.awt.Font(JecnProperties.getValue("songTi"), 1, 12), Color.BLACK));
//		this.setBorder(BorderFactory.createLineBorder(Color.red));
		this.scrollPane=new JScrollPane();
		Dimension size =new Dimension(680, 120);
		this.scrollPane.setPreferredSize(size);
		this.table=new TitleFileTable();
		this.scrollPane.setViewportView(this.table);
		scrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0);
		this.setLayout(new GridBagLayout());
		this.add(this.scrollPane, c);
//		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
//				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
//				new Insets(0, 5, 0, 0), 0, 0);
//		this.add(this.button,c);
	}
	
	
	
	class TitleFileTable extends JecnTable{

		@Override
		public JecnTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("fileNum"));
			title.add(JecnProperties.getValue("fileName"));
			
			Vector<Vector<String>> vs=new	Vector<Vector<String>>();
			return new JecnTableModel(title, vs);
		}

		@Override
		public boolean isSelectMutil() {
			
			return false;
		}

		@Override
		public int[] gethiddenCols() {
			
			return new int[]{};
		}
		
	}
}
