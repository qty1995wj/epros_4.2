<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% response.setHeader("Pragma","No-cache"); response.setHeader("Cache-Control","no-cache");%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript">
              document.body.onresize=function(){
			    setShowProcessTemplate('processTemplateDiv');
			 }
        </script>
	</head>

	<body>
		<div id="processTemplateDivTitle"
			style="width: 100%; overflow-y: scroll;" align="center">
			<table width="98%" border="0" cellpadding="0" cellspacing="1"
				align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
				<tr class="FixedTitleRow" align="center">
					<td width="2%" class="gradient">
					</td>
					<td width="20%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 活动名称 -->
						<s:text name="activityTitle" />
					</td>
					<td width="16%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 活动编号 -->
						<s:text name="activitynumbers" />
					</td>
					<td width="15%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 输入 -->
						<s:text name="input" />
					</td>
					<td width="30%" height="32">
						<table width="100%" height="100%" cellspacing="0" frame="void">
							<tr class="FixedTitleRow" align="center">
								<td colspan="2" align="center" align="center" class="gradient"
									style="word-break: break-all;">
									<!-- 输出 -->
									<s:text name="output" />
								</td>
							</tr>
							<tr class="FixedTitleRow" align="center">
								<td width="50%" align="center" class="gradient"
									style="word-break: break-all;">
									<!-- 表单 -->
									<s:text name="form" />
								</td>
								<td width="50%" align="center" class="gradient"
									style="word-break: break-all;">
									<!-- 样例 -->
									<s:text name="sample" />
								</td>
							</tr>
						</table>
					</td>
					<td width="15%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 操作规范 -->
						<s:text name="operationSpecifications" />
					</td>
					<td width="2%" class="gradient">
					</td>
				</tr>
			</table>
		</div>
		<div id="processTemplateDiv" style="width: 100%; overflow-y: scroll;"
			align="center">
			<s:set id="count" value="1" />
			<table width="98%" border="0" cellpadding="0" cellspacing="1"
				align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
				<s:iterator value="processActiveTemplateList" status="st">
					<tr align="center" bgcolor="#ffffff" height="20">
						<td align="right" width="2%" class="gradient"
							style="word-break: break-all;">
							<s:property value="#count" />
							<s:set id="count" value="#count+1" />
						</td>
						<td class="t5" style="word-break: break-all;" width="20%">
							<a
								href="processActive.action?activeId=<s:property value='activeFigureId'/>&isPub=${isPub}"
								style="cursor: pointer;" target='_blank' class="table"> <s:property
									value="activeName" /> </a>
						</td>
						<td class="t5" style="word-break: break-all;" width="16%">
							<s:property value="activeId" />
						</td>
						<td class="t5" style="word-break: break-all;" width="15%">
						<s:if test="activityNoteShow==1">
						<s:property value="activityInput" /><br>
						</s:if>
							<s:iterator value="listInput">
								<a
									href="file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='fileId'/>&isPub=${isPub}"
									style="cursor: pointer;" target='_blank' class="table"> <s:property
										value="fileName" /> </a>
								<br>
							</s:iterator>
						</td>
						<td width="30%">
							<table frame="void" cellspacing="0" width="100%" height="100%"
								bgcolor="#BFBFBF" style="border-width: 0px 0px 0px 0px;">
								<s:if test="activityNoteShow==1">
								<s:property value="activityOutput" />
								</s:if>
								<s:if test="listMode.size > 0">
									<s:iterator value="listMode">
										<tr bgcolor="#ffffff" height="100%">
											<td class="t5" style="word-break: break-all;" width="50%">
												<a
													href="file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='modeFile.fileId'/>&isPub=${isPub}"
													style="cursor: pointer;" target='_blank' class="table">
													<s:property value="modeFile.fileName" /> </a>
											</td>
											<td class="t5" style="word-break: break-all;" width="50%">
												<s:iterator value="listTemplet">
													<a
														href="file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='fileId'/>&isPub=${isPub}"
														style="cursor: pointer;" target='_blank' class="table">
														<s:property value="fileName" /> </a>
													<br>
												</s:iterator>
												<br>
											</td>
										</tr>
									</s:iterator>
								</s:if>
								<tr bgcolor="#ffffff" height="100%">
									<td class="t5" style="word-break: break-all;" width="50%">
										&nbsp;
									</td>
									<td class="t5" style="word-break: break-all;" width="50%">
										&nbsp;
									</td>
								</tr>
							</table>
						</td>
						<td class="t5" style="word-break: break-all;" width="15%">
							<s:iterator value="listOperation">
								<a
									href="file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='fileId'/>&isPub=${isPub}"
									style="cursor: pointer;" target='_blank' class="table"> <s:property
										value="fileName" /> </a>
								<br>
							</s:iterator>
						</td>
						<td width="2%" class="t5" style="word-break: break-all;">
						</td>
					</tr>
				</s:iterator>
			</table>
		</div>
	</body>
</html>