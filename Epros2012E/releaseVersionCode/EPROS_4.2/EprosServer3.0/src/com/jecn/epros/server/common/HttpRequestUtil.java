package com.jecn.epros.server.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Logger;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.jecn.epros.server.action.web.login.ad.mengniu.MengNiuAfterItem;

/**
 * 
 * @author ZXH
 * 
 */
public class HttpRequestUtil {
	private static Logger log = Logger.getLogger(HttpRequestUtil.class);

	/**
	 * 获取HTTP请求返回的json
	 * 
	 * @param url
	 *            请求URL
	 * @param param
	 *            请求参数
	 * 
	 *@return String
	 */
	public static String getJsonHttpRequest(String url, String param) {
		return sendGetSSL(url, param);
	}

	/**
	 *
	 */
	public static String sendGetSSL(String url, String param) {
		String result = "";
		BufferedReader in = null;
		try {
			System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");

			HostnameVerifier hv = new HostnameVerifier() {
				public boolean verify(String urlHostName, SSLSession session) {
					return urlHostName.equals(session.getPeerHost());// 或return
					// true;
				}
			};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);

			String urlNameString = url + "?" + param;
			log.info("urlNameString = " + urlNameString);
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			HttpsURLConnection connection = (HttpsURLConnection) realUrl.openConnection();
			// 设置通用的请求属性
			SSLSocketFactory ssf = getSSFactory();
			// 设置域名校验
			connection.setHostnameVerifier(new TrustAnyHostnameVerifier());
			connection.setSSLSocketFactory(ssf);

			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			if (JecnConfigTool.isMengNiuLogin()) {
				String headerValue = getHeaderValue();
				// log.info("headerValue = " + headerValue);
				connection.setRequestProperty("Authorization", headerValue);
				System.out.println();
			}
			// 建立实际的连接
			connection.connect();

			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			for (String key : map.keySet()) {
				// System.out.println(key + "--->" + map.get(key));
			}
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			log.error("sendGet error " + url, e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 */
	public static String sendPost(String url, String param) {
		OutputStreamWriter out = null;
		BufferedReader in = null;
		String result = "";
		log.info("url = " + url);
		try {
			URL realUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
			// 打开和URL之间的连接

			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST"); // POST方法

			// 设置通用的请求属性

			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

			if (JecnConfigTool.isMengNiuLogin()) {
				String headerValue = getHeaderValue();
				log.info("headerValue = " + headerValue);
				conn.setRequestProperty("Authorization", headerValue);
			}
			conn.connect();

			// 获取URLConnection对象对应的输出流
			out = new OutputStreamWriter(conn.getOutputStream(), "utf-8");
			// 发送请求参数
			out.write(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			log.error("sendPost error " + url, e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 不进行主机名确认
	 */
	private static class TrustAnyHostnameVerifier implements HostnameVerifier {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

	private static BZX509TrustManager trustManager = null;

	private static BZX509TrustManager createBZX509TrustManager() {
		if (trustManager == null) {
			trustManager = new BZX509TrustManager();
		}
		return trustManager;
	}

	private static SSLSocketFactory getSSFactory() throws NoSuchAlgorithmException, NoSuchProviderException,
			KeyManagementException {
		TrustManager[] tm = { createBZX509TrustManager() };
		SSLContext sslContext = SSLContext.getInstance("SSL");
		sslContext.init(null, tm, new java.security.SecureRandom());
		SSLSocketFactory ssf = sslContext.getSocketFactory();
		return ssf;
	}

	/**
	 * 获取header 值
	 * 
	 * @return
	 */
	private static String getHeaderValue() {
		String headerValue = "Basic ";
		headerValue += MengNiuAfterItem.APP_CODE;
		String secret = MengNiuAfterItem.APP_KEY + JecnCommon.getStringbyDate(new Date());
		return headerValue + ":" + MD5(secret);
	}

	/**
	 * 返回MD5 32为字符串
	 * 
	 * @param sourceStr
	 * @return
	 */
	private static String MD5(String sourceStr) {
		String result = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(sourceStr.getBytes());
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			result = buf.toString();
			return result;
		} catch (NoSuchAlgorithmException e) {
			System.out.println(e);
		}
		return result;
	}

	static class BZX509TrustManager implements X509TrustManager {

		public BZX509TrustManager() {
		}

		@Override
		public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			// TODO Auto-generated method stub

		}

		@Override
		public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			// TODO Auto-generated method stub

		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			// TODO Auto-generated method stub
			return null;
		}

	}

	/**
	 * spring 调用 rest接口
	 * 
	 * @param url
	 * @param map
	 * @return
	 */
	public static String sendPostRest(String url, MultiValueMap<String, String> map) {
		RestTemplate template = new RestTemplate();
		return template.postForObject(url, map, String.class);
	}
	
	 /**
     * 向指定URL发送GET方法的请求
     * 
     * @param url
     *            发送请求的URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     */
    public static String sendGet(String url, String param) {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + param;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(),"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }
	

	public static void main(String[] args) {
		// MultiValueMap<String, String> map = new LinkedMultiValueMap<String,
		// String>();
		// map.add("syscode", "EPROS");
		// map.add("flowid", "123456");
		// map.add("requestname", "EPROS待办测试");
		// map.add("workflowname", "EPROS待办测试");
		// map.add("nodename", "文控审核");
		// map.add("pcurl", "http://10.12.8.48:8080/static-web/");
		// map.add("creator", "80021166");
		// map.add("createdatetime", "2018-06-13 00:00:00");
		// map.add("receiver", "80021166");
		// map.add("receivedatetime", "2018-06-13 00:00:00");
		// HttpRequestUtil.sendPostRest("http://10.12.8.41/rest/ofs/ReceiveTodoRequestByMap",
		// map);

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("syscode", "EPROS");
		map.add("flowid", "123456");
		map.add("userid", "80021166");
		HttpRequestUtil.sendPostRest("http://10.12.8.41/rest/ofs/deleteUserRequestInfoByMap", map);
	}

}
