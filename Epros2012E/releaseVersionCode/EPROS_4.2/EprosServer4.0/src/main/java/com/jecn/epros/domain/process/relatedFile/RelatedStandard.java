package com.jecn.epros.domain.process.relatedFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.sqlprovider.server3.Common;

/**
 * 相关标准
 *
 * @author ZXH
 */
public class RelatedStandard {
    private Long id;
    private String name;
    private String typeName;
    private TreeNodeUtils.NodeType from;

    public LinkResource getLink() {
        return new LinkResource(id, name, ResourceType.STANDARD, from);
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        // 0是目录，1文件标准，2流程标准 3流程地图标准4标准条款5条款要求
        this.typeName = Common.standardTypeVal(Integer.valueOf(typeName));
    }

    public TreeNodeUtils.NodeType getFrom() {
        return from;
    }

    public void setFrom(TreeNodeUtils.NodeType from) {
        this.from = from;
    }
}
