package epros.designer.gui.process.activitiesProperty;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;

import epros.designer.util.JecnProperties;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 活动明细：时间
 * 
 * @author 2012-07-27
 * 
 */
public class ActiveTimePanel extends JecnPanel {
	Insets insets = new Insets(1, 1, 1, 1);

	/** 类型选择Panel */
	private JecnPanel typeChangePanel = new JecnPanel(450, 95);

	private JLabel typeChangeLab = new JLabel(JecnProperties.getValue("timeType"));

	/** 类型选择ComboBox */
	public JComboBox typeChangeCombox = new JComboBox(new String[] { "", JecnProperties.getValue("actDay"),
			JecnProperties.getValue("actWeek"), JecnProperties.getValue("actMonth"), JecnProperties.getValue("actSea"),
			JecnProperties.getValue("actYear") });

	/**
	 * 开始时间*****************************************************************
	 * Panel
	 */
	private JecnPanel startTimePanel = new JecnPanel();

	/** 日 类型 */
	private JLabel startDayLab = new JLabel(JecnProperties.getValue("actDayTypeC"));
	/** 开始小时单位 */
	private JLabel startHourUnitLab = new JLabel(JecnProperties.getValue("actHour"));
	/** 开始分钟单位 */
	private JLabel startMinuteUnitLab = new JLabel(JecnProperties.getValue("actMinute"));
	/** 开始秒单位 */
	private JLabel startSecondUnitLab = new JLabel(JecnProperties.getValue("actSecond"));
	public JComboBox startHourCombox = new JComboBox(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" });
	public JComboBox startMinuteCombox = new JComboBox(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
			"28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45",
			"46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" });
	public JComboBox startSecondCombox = new JComboBox(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
			"28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45",
			"46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" });

	/** 周 类型 */
	private JLabel startWeekLab = new JLabel(JecnProperties.getValue("actWeekTypeC"));
	public JComboBox startWeekCombox = new JComboBox(new String[] { JecnProperties.getValue("mondayW"),
			JecnProperties.getValue("tuesdayW"), JecnProperties.getValue("wednesdayW"),
			JecnProperties.getValue("thursdayW"), JecnProperties.getValue("fridayW"),
			JecnProperties.getValue("faturdayW"), JecnProperties.getValue("sundayW") });

	/** 月 类型 */
	private JLabel startMonthLab = new JLabel(JecnProperties.getValue("actMonthTypeC"));
	/** 开始日 单位 */
	private JLabel startMonthDayUnitLab = new JLabel(JecnProperties.getValue("actDay"));
	public JComboBox startMonthCombox = new JComboBox(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
			"11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
			"29", "30", "31" });
	/** 季 类型 */
	private JLabel startSeasonLab = new JLabel(JecnProperties.getValue("actSeaTypeC"));
	/** 开始季单位 */
	private JLabel startSeaDayUnitLab = new JLabel(JecnProperties.getValue("actDay"));
	public JComboBox startSeasonCombox = new JComboBox(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
			"28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45",
			"46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63",
			"64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81",
			"82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92" });
	/** 年 类型 */
	private JLabel startYearLab = new JLabel(JecnProperties.getValue("actYearTypeC"));
	/** 年--月 单位 */
	private JLabel startYearMonthUnitLab = new JLabel(JecnProperties.getValue("actMonth"));
	/** 年--日 单位 */
	private JLabel startYearDayUnitLab = new JLabel(JecnProperties.getValue("actDay"));
	public JComboBox startYearCombox = new JComboBox(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
			"11", "12" });
	public JComboBox startYearDayCombox = new JComboBox(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
			"28", "29", "30", "31" });
	/**
	 * 结束时间*******************************************************************
	 * Panel
	 */
	private JecnPanel endTimePanel = new JecnPanel();
	/** 日 类型 */
	private JLabel endDayLab = new JLabel(JecnProperties.getValue("actDayTypeC"));
	/** 结束小时单位 */
	private JLabel endHourUnitLab = new JLabel(JecnProperties.getValue("actHour"));
	/** 结束分钟单位 */
	private JLabel endMinuteUnitLab = new JLabel(JecnProperties.getValue("actMinute"));
	/** 结束秒单位 */
	private JLabel endSecondUnitLab = new JLabel(JecnProperties.getValue("actSecond"));
	public JComboBox endHourCombox = new JComboBox(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" });
	public JComboBox endMinuteCombox = new JComboBox(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
			"28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45",
			"46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" });
	public JComboBox endSecondCombox = new JComboBox(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
			"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
			"28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45",
			"46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" });

	/** 周 类型 */
	private JLabel endWeekLab = new JLabel(JecnProperties.getValue("actWeekTypeC"));
	public JComboBox endWeekCombox = new JComboBox(new String[] { JecnProperties.getValue("mondayW"),
			JecnProperties.getValue("tuesdayW"), JecnProperties.getValue("wednesdayW"),
			JecnProperties.getValue("thursdayW"), JecnProperties.getValue("fridayW"),
			JecnProperties.getValue("faturdayW"), JecnProperties.getValue("sundayW") });

	/** 月 类型 */
	private JLabel endMonthLab = new JLabel(JecnProperties.getValue("actMonthTypeC"));
	/** 结束日 单位 */
	private JLabel endMonthDayUnitLab = new JLabel(JecnProperties.getValue("actDay"));
	public JComboBox endMonthCombox = new JComboBox(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
			"11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
			"29", "30", "31" });
	/** 季 类型 */
	private JLabel endSeasonLab = new JLabel(JecnProperties.getValue("actSeaTypeC"));
	/** 结束季单位 */
	private JLabel endSeaDayUnitLab = new JLabel(JecnProperties.getValue("actDay"));
	public JComboBox endSeanCombox = new JComboBox(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
			"11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
			"29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46",
			"47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64",
			"65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82",
			"83", "84", "85", "86", "87", "88", "89", "90", "91", "92" });

	/** 年 类型 */
	private JLabel endYearLab = new JLabel(JecnProperties.getValue("actYearTypeC"));
	/** 结束年--月 单位 */
	private JLabel endYearMonthUnitLab = new JLabel(JecnProperties.getValue("actMonth"));
	/** 结束年--日 单位 */
	private JLabel endYearDayUnitLab = new JLabel(JecnProperties.getValue("actDay"));

	public JComboBox endYearCombox = new JComboBox(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
			"11", "12" });
	public JComboBox endYearDayCombox = new JComboBox(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
			"11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
			"29", "30", "31" });
	/** 活动的数据 */
	private JecnActiveData jecnActiveData;

	/** 活动开始时间 */
	private String avtivityStartTime = "";
	/** 活动结束时间 */
	private String avtivityStopTime = "";
	/** 活动时间类型 1是日，2是周，3是月，4是季，5是年 */
	private Integer avtivityTimeType = 0;

	// /** 流程驱动规则 */
	// private JecnFlowDriverT jecnFlowDriverT = new JecnFlowDriverT();

	public ActiveTimePanel(JecnActiveData jecnActiveData) {
		this.jecnActiveData = jecnActiveData;
		this.setSize(480, 350);

		// 设置面板背景颜色
		typeChangePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		startTimePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		endTimePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// // 类型选择
		// dimension = new Dimension(100, 20);
		// typeChangeCombox.setPreferredSize(dimension);
		// typeChangeCombox.setMaximumSize(dimension);
		// typeChangeCombox.setMinimumSize(dimension);
		initLaout();
		typeChangeCombox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				typeChangePerformed(e);
			}
		});
		initData();
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData) {
		this.jecnActiveData = jecnActiveData;
		initData();
	}

	private void initData() {
		if (jecnActiveData != null) {
			String quantity = null;
			// 类型选择选中项设置
			int quantityNum = jecnActiveData.getAvtivityTimeType();
			if (1 == quantityNum) {
				quantity = JecnProperties.getValue("actDay");
			} else if (2 == quantityNum) {
				quantity = JecnProperties.getValue("actWeek");
			} else if (3 == quantityNum) {
				quantity = JecnProperties.getValue("actMonth");
			} else if (4 == quantityNum) {
				quantity = JecnProperties.getValue("actSea");
			} else if (5 == quantityNum) {
				quantity = JecnProperties.getValue("actYear");
			}

			if (quantity != null && !"".equals(quantity)) {
				if (typeChangeCombox.getItemAt(1).equals(quantity)) {// 日
					typeChangeCombox.setSelectedIndex(1);
				} else if (typeChangeCombox.getItemAt(2).equals(quantity)) {// 周
					typeChangeCombox.setSelectedIndex(2);
				} else if (typeChangeCombox.getItemAt(3).equals(quantity)) {// 月
					typeChangeCombox.setSelectedIndex(3);
				} else if (typeChangeCombox.getItemAt(4).equals(quantity)) {// 季度
					typeChangeCombox.setSelectedIndex(4);
				} else if (typeChangeCombox.getItemAt(5).equals(quantity)) {// 年
					typeChangeCombox.setSelectedIndex(5);
				}
			}
		}
	}

	private void initLaout() {
		GridBagConstraints c = null;
		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		this.add(typeChangePanel, c);
		typeChangePanel.setLayout(new GridBagLayout());
		typeChangePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("typeChoose")));
		// typeChangeLab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		typeChangePanel.add(typeChangeLab, c);
		// typeChangeCombox
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		typeChangePanel.add(typeChangeCombox, c);
		// 开始时间startTimePanel
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(startTimePanel, c);
		startTimePanel.setLayout(new GridBagLayout());
		startTimePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("stTime")));
		// 结束时间endTimePanel
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.add(endTimePanel, c);
		endTimePanel.setLayout(new GridBagLayout());
		endTimePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("eTime")));

	}

	/**
	 * 时间 类型选择
	 */
	private void typeChangePerformed(ItemEvent e) {

		if (e.getStateChange() == e.SELECTED) {
			// 数据库类型
			String getItemSel = typeChangeCombox.getSelectedItem().toString();
			// 日
			startDayLab.setVisible(false);
			startHourCombox.setVisible(false);
			startMinuteCombox.setVisible(false);
			startSecondCombox.setVisible(false);

			endDayLab.setVisible(false);
			endHourCombox.setVisible(false);
			endMinuteCombox.setVisible(false);
			endSecondCombox.setVisible(false);

			// 周
			startWeekLab.setVisible(false);
			startWeekCombox.setVisible(false);
			endWeekLab.setVisible(false);
			endWeekCombox.setVisible(false);
			// 月
			startMonthLab.setVisible(false);
			startMonthCombox.setVisible(false);
			endMonthLab.setVisible(false);
			endMonthCombox.setVisible(false);
			// 季
			startSeasonLab.setVisible(false);
			startSeasonCombox.setVisible(false);
			endSeasonLab.setVisible(false);
			endSeanCombox.setVisible(false);
			// 年
			startYearLab.setVisible(false);
			startYearCombox.setVisible(false);
			startYearDayCombox.setVisible(false);
			endYearLab.setVisible(false);
			endYearCombox.setVisible(false);
			endYearDayCombox.setVisible(false);

			// 单位名称
			// 日
			startHourUnitLab.setVisible(false);
			startMinuteUnitLab.setVisible(false);
			startSecondUnitLab.setVisible(false);

			endHourUnitLab.setVisible(false);
			endMinuteUnitLab.setVisible(false);
			endSecondUnitLab.setVisible(false);
			// 月
			startMonthDayUnitLab.setVisible(false);
			endMonthDayUnitLab.setVisible(false);
			// 季
			startSeaDayUnitLab.setVisible(false);
			endSeaDayUnitLab.setVisible(false);
			// 年
			startYearMonthUnitLab.setVisible(false);
			startYearDayUnitLab.setVisible(false);
			endYearMonthUnitLab.setVisible(false);
			endYearDayUnitLab.setVisible(false);

			String strStart = null;
			String strEnd = null;
			String getQua = null;

			String[] strStarts = null;
			String[] strEnds = null;
			if (jecnActiveData != null) {
				strStart = jecnActiveData.getAvtivityStartTime();
				strEnd = jecnActiveData.getAvtivityStopTime();
				// 类型选择选中项设置
				int quantityNum = jecnActiveData.getAvtivityTimeType();

				if (1 == quantityNum) {
					getQua = JecnProperties.getValue("actDay");
				} else if (2 == quantityNum) {
					getQua = JecnProperties.getValue("actWeek");
				} else if (3 == quantityNum) {
					getQua = JecnProperties.getValue("actMonth");
				} else if (4 == quantityNum) {
					getQua = JecnProperties.getValue("actSea");
				} else if (5 == quantityNum) {
					getQua = JecnProperties.getValue("actYear");
				}
				// 逗号隔开
				if (strStart != null) {
					strStarts = strStart.split(",");
				}
				if (strEnd != null) {
					strEnds = strEnd.split(",");
				}

			}

			if (getItemSel.equals(JecnProperties.getValue("actDay"))) {
				// 日类型
				startDayLab.setVisible(true);
				startHourCombox.setVisible(true);
				startMinuteCombox.setVisible(true);
				startSecondCombox.setVisible(true);

				endDayLab.setVisible(true);
				endHourCombox.setVisible(true);
				endMinuteCombox.setVisible(true);
				endSecondCombox.setVisible(true);

				startHourUnitLab.setVisible(true);
				startMinuteUnitLab.setVisible(true);
				startSecondUnitLab.setVisible(true);

				endHourUnitLab.setVisible(true);
				endMinuteUnitLab.setVisible(true);
				endSecondUnitLab.setVisible(true);

				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(1).equals(getQua)) {
						int intStartHour = 0;
						int intEndHour = 0;
						int intStartMinute = 0;
						int intEndMinute = 0;
						int intStartSecond = 0;
						int intEndSecond = 0;
						if (strStart != null) {
							// 时
							intStartHour = Integer.parseInt(strStarts[0].toString());
							// 分
							intStartMinute = Integer.parseInt(strStarts[1].toString());
							// 秒
							intStartSecond = Integer.parseInt(strStarts[2].toString());

						}
						if (strEnds != null) {
							intEndHour = Integer.parseInt(strEnds[0].toString());
							intEndMinute = Integer.parseInt(strEnds[1].toString());
							intEndSecond = Integer.parseInt(strEnds[2].toString());
						}

						startHourCombox.setSelectedIndex(intStartHour);
						startMinuteCombox.setSelectedIndex(intStartMinute);
						startSecondCombox.setSelectedIndex(intStartSecond);

						endHourCombox.setSelectedIndex(intEndHour);
						endMinuteCombox.setSelectedIndex(intEndMinute);
						endSecondCombox.setSelectedIndex(intEndSecond);

					}
				}

				addRowToPanel(startTimePanel, startDayLab, 0);
				addRowToPanel(startTimePanel, startHourCombox, 1);
				// 时 单位
				addRowToPanel(startTimePanel, startHourUnitLab, 2);
				addRowToPanel(startTimePanel, startMinuteCombox, 3);
				// 分 单位endHourUnitLab
				addRowToPanel(startTimePanel, startMinuteUnitLab, 4);
				addRowToPanel(startTimePanel, startSecondCombox, 5);
				// 秒 单位endHourUnitLab
				addRowToPanel(startTimePanel, startSecondUnitLab, 6);

				addRowToPanel(endTimePanel, endDayLab, 0);
				addRowToPanel(endTimePanel, endHourCombox, 1);
				// 时 单位endHourUnitLab
				addRowToPanel(endTimePanel, endHourUnitLab, 2);
				addRowToPanel(endTimePanel, endMinuteCombox, 3);
				// 分 单位endHourUnitLab
				addRowToPanel(endTimePanel, endMinuteUnitLab, 4);
				addRowToPanel(endTimePanel, endSecondCombox, 5);
				// 秒 单位endHourUnitLab
				addRowToPanel(endTimePanel, endSecondUnitLab, 6);

			} else if (getItemSel.equals(JecnProperties.getValue("actWeek"))) {

				// 周类型 startWeekLab
				startWeekLab.setVisible(true);
				startWeekCombox.setVisible(true);
				endWeekLab.setVisible(true);
				endWeekCombox.setVisible(true);

				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(2).equals(getQua)) {

						int intStart = Integer.parseInt(strStarts[0]);
						int intEnd = Integer.parseInt(strEnds[0]);
						// 开始周
						startWeekCombox.setSelectedIndex(intStart);
						// 结束周
						endWeekCombox.setSelectedIndex(intEnd);
					}
				}

				addRowToPanel(startTimePanel, startWeekLab, 0);
				addRowToPanel(startTimePanel, startWeekCombox, 1);
				// 结束
				addRowToPanel(endTimePanel, endWeekLab, 0);
				addRowToPanel(endTimePanel, endWeekCombox, 1);
			} else if (getItemSel.equals(JecnProperties.getValue("actMonth"))) {
				// 月类型 startMonthLab
				startMonthLab.setVisible(true);
				startMonthCombox.setVisible(true);
				endMonthLab.setVisible(true);
				endMonthCombox.setVisible(true);

				startMonthDayUnitLab.setVisible(true);
				endMonthDayUnitLab.setVisible(true);

				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(3).equals(getQua)) {// 月

						int intStart = Integer.parseInt(strStarts[0]);
						int intEnd = Integer.parseInt(strEnds[0]);

						startMonthCombox.setSelectedIndex(intStart);
						endMonthCombox.setSelectedIndex(intEnd);
					}
				}

				addRowToPanel(startTimePanel, startMonthLab, 0);
				addRowToPanel(startTimePanel, startMonthCombox, 1);
				addRowToPanel(startTimePanel, startMonthDayUnitLab, 2);
				addRowToPanel(endTimePanel, endMonthLab, 0);
				addRowToPanel(endTimePanel, endMonthCombox, 1);
				addRowToPanel(endTimePanel, endMonthDayUnitLab, 2);
			} else if (getItemSel.equals(JecnProperties.getValue("actSea"))) {
				// 季类型
				startSeasonLab.setVisible(true);
				startSeasonCombox.setVisible(true);
				endSeasonLab.setVisible(true);
				endSeanCombox.setVisible(true);

				startSeaDayUnitLab.setVisible(true);
				endSeaDayUnitLab.setVisible(true);

				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(4).equals(getQua)) {
						int intStart = Integer.parseInt(strStarts[0]);
						int intEnd = Integer.parseInt(strEnds[0]);
						startSeasonCombox.setSelectedIndex(intStart);
						endSeanCombox.setSelectedIndex(intEnd);
					}
				}

				addRowToPanel(startTimePanel, startSeasonLab, 0);
				addRowToPanel(startTimePanel, startSeasonCombox, 1);
				addRowToPanel(startTimePanel, startSeaDayUnitLab, 2);
				addRowToPanel(endTimePanel, endSeasonLab, 0);
				addRowToPanel(endTimePanel, endSeanCombox, 1);
				addRowToPanel(endTimePanel, endSeaDayUnitLab, 2);
			} else if (getItemSel.equals(JecnProperties.getValue("actYear"))) {
				// 年类型
				startYearLab.setVisible(true);
				startYearCombox.setVisible(true);
				startYearDayCombox.setVisible(true);
				endYearLab.setVisible(true);
				endYearCombox.setVisible(true);
				endYearDayCombox.setVisible(true);

				startYearMonthUnitLab.setVisible(true);
				startYearDayUnitLab.setVisible(true);
				endYearMonthUnitLab.setVisible(true);
				endYearDayUnitLab.setVisible(true);

				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(5).equals(getQua)) {
						int intStartMonth = 0;
						int intEndMonth = 0;
						int intStartDay = 0;
						int intEndDay = 0;
						// 开始月
						intStartMonth = Integer.parseInt(strStarts[0]);
						// 结束月
						intEndMonth = Integer.parseInt(strEnds[0]);
						// 开始天
						intStartDay = Integer.parseInt(strStarts[1]);
						// 结束天
						intEndDay = Integer.parseInt(strEnds[1]);
						startYearCombox.setSelectedIndex(intStartMonth);
						startYearDayCombox.setSelectedIndex(intStartDay);

						endYearCombox.setSelectedIndex(intEndMonth);
						endYearDayCombox.setSelectedIndex(intEndDay);
					}
				}
				addRowToPanel(startTimePanel, startYearLab, 0);
				addRowToPanel(startTimePanel, startYearCombox, 1);
				addRowToPanel(startTimePanel, startYearMonthUnitLab, 2);
				addRowToPanel(startTimePanel, startYearDayCombox, 3);
				addRowToPanel(startTimePanel, startYearDayUnitLab, 4);
				addRowToPanel(endTimePanel, endYearLab, 0);
				addRowToPanel(endTimePanel, endYearCombox, 1);
				addRowToPanel(endTimePanel, endYearMonthUnitLab, 2);
				addRowToPanel(endTimePanel, endYearDayCombox, 3);
				addRowToPanel(endTimePanel, endYearDayUnitLab, 4);
			}
			startTimePanel.validate();
			startTimePanel.repaint();
			endTimePanel.validate();
			endTimePanel.repaint();
		}
	}

	private void addRowToPanel(JecnPanel panel, JComponent leftLabel, int colsp) {
		// GridBagConstraints c = new GridBagConstraints(colsp, 0, 1, 1, 1.0,
		// 0.0,
		// GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
		// 0, 0);
		// panel.add(leftLabel, c);
		if (colsp == 0 || colsp == 2 || colsp == 4 || colsp == 6) {
			GridBagConstraints c = new GridBagConstraints(colsp, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
					GridBagConstraints.NONE, insets, 0, 0);
			panel.add(leftLabel, c);
		} else {
			GridBagConstraints c = new GridBagConstraints(colsp, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
					GridBagConstraints.HORIZONTAL, insets, 0, 0);
			panel.add(leftLabel, c);
		}
	}

	public void okButPerformed() {
		getResultInfo();
		jecnActiveData.setAvtivityStartTime(avtivityStartTime);
		jecnActiveData.setAvtivityStopTime(avtivityStopTime);
		jecnActiveData.setAvtivityTimeType(avtivityTimeType);
	}

	public boolean isUpdate() {
		getResultInfo();
		if (!DrawCommon.checkStringSame(avtivityStartTime, jecnActiveData.getAvtivityStartTime())) {
			return true;
		}
		if (!DrawCommon.checkStringSame(avtivityStopTime, jecnActiveData.getAvtivityStopTime())) {
			return true;
		}
		if (jecnActiveData.getAvtivityTimeType() == null) {
			jecnActiveData.setAvtivityTimeType(0);
		}
		if (avtivityTimeType != jecnActiveData.getAvtivityTimeType().intValue()) {
			return true;
		}
		return false;
	}

	private void getResultInfo() {
		// 活动的时间
		// *********************更新流程驱动规则临时表数据**********************
		String strQuantity = this.typeChangeCombox.getSelectedItem().toString();
		String strStartSelected = null;
		String strEndSelected = null;
		// 年--天 ///日类型--时
		String startYeaDaySel = null;
		// 年---天 //日类型---分
		String endYeaDaySel = null;

		String startSecSel = null;
		String endSecSel = null;

		// 根据索引获取类型
		if (this.typeChangeCombox.getItemAt(1).equals(strQuantity)) {
			// 开始
			strStartSelected = this.startHourCombox.getSelectedIndex() + ",";// 时
			startYeaDaySel = this.startMinuteCombox.getSelectedIndex() + ",";// 分
			startSecSel = String.valueOf(this.startSecondCombox.getSelectedIndex());// 秒
			// 结束
			strEndSelected = this.endHourCombox.getSelectedIndex() + ","; // 时
			endYeaDaySel = this.endMinuteCombox.getSelectedIndex() + ",";// 分
			endSecSel = String.valueOf(this.endSecondCombox.getSelectedIndex());// 秒
			// 开始时间
			avtivityStartTime = strStartSelected + startYeaDaySel + startSecSel;
			// 结束时间
			avtivityStopTime = strEndSelected + endYeaDaySel + endSecSel;
			// 时间类型
			avtivityTimeType = 1;

		} else if (this.typeChangeCombox.getItemAt(2).equals(strQuantity)) {
			// 开始时间
			avtivityStartTime = String.valueOf(this.startWeekCombox.getSelectedIndex());
			// 结束时间
			avtivityStopTime = String.valueOf(this.endWeekCombox.getSelectedIndex());
			// 时间类型
			avtivityTimeType = 2;
		} else if (this.typeChangeCombox.getItemAt(3).equals(strQuantity)) {
			// 开始时间
			avtivityStartTime = String.valueOf(this.startMonthCombox.getSelectedIndex());
			// 结束时间
			avtivityStopTime = String.valueOf(this.endMonthCombox.getSelectedIndex());
			// 时间类型
			avtivityTimeType = 3;
		} else if (this.typeChangeCombox.getItemAt(4).equals(strQuantity)) {
			// 开始时间
			avtivityStartTime = String.valueOf(this.startSeasonCombox.getSelectedIndex());
			// 结束时间
			avtivityStopTime = String.valueOf(this.endSeanCombox.getSelectedIndex());
			// 时间类型
			avtivityTimeType = 4;
		} else if (this.typeChangeCombox.getItemAt(5).equals(strQuantity)) {
			// 开始
			strStartSelected = this.startYearCombox.getSelectedIndex() + ",";// 月
			startYeaDaySel = String.valueOf(this.startYearDayCombox.getSelectedIndex());// 天
			// 结束
			strEndSelected = this.endYearCombox.getSelectedIndex() + ","; // 月
			endYeaDaySel = String.valueOf(this.endYearDayCombox.getSelectedIndex());// 天
			// 开始时间
			avtivityStartTime = strStartSelected + startYeaDaySel;
			// 结束时间
			avtivityStopTime = strEndSelected + endYeaDaySel;
			// 时间类型
			avtivityTimeType = 5;
		}
	}
}
