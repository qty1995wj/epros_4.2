package epros.designer.gui.integration.internalControl;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

/**
 * 内控指引管理树监听，合拢或张开
 * 
 */
public class ControlGuideTreeListener extends JecnTreeListener {

	/** 点击节点刷新生产子节点 */
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger
			.getLogger(ControlGuideTreeListener.class);

	/**
	 * 内控指引管理树监听
	 * 
	 * @param jTree
	 *            点击节点刷新生产子节点
	 */
	public ControlGuideTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}

	/**
	 * 合拢
	 */
	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

	/**
	 * 展开
	 */
	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = ConnectionPool
						.getControlGuideAction().getChildJecnControlGuides(
								node.getJecnTreeBean().getId(),
								JecnConstants.projectId);
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("ControlGuideTreeListener treeExpanded is error", e);
			}
			JecnTreeCommon.selectNode(jTree, node);
		}
	}
}
