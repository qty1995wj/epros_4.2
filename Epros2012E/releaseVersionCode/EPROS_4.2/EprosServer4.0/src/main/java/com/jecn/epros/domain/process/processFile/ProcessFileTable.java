package com.jecn.epros.domain.process.processFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件表格
 *
 * @author ZXH
 */
public class ProcessFileTable extends BaseProcessFileBean {
    /**
     * 1: 横表， 第一行是标题；2：纵表，第一列是标题
     */
    private List<List<FileTableCell>> contents = new ArrayList<>();

    public List<List<FileTableCell>> getContents() {
        return contents;
    }


    @JsonIgnore
    public FileTableCell getTableCell() {
        return new FileTableCell();
    }

    /**
     * 单元格对象
     *
     * @author ZXH
     */
    public class FileTableCell {
        /**
         * 单元格内容
         */
        private String text;

        private String style;

        private List<String> texts;
        /**
         * 单元格连接
         */
        private List<LinkResource> links;
        /**
         * 单元格类型
         */
        private CellType type;

        public String getText() {
            return text;
        }

        public List<LinkResource> getLinks() {
            return links;
        }

        public String getStyle() {
            return style;
        }

        public void setStyle(String style) {
            this.style = style;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getText(String text) {
            return text;
        }

        public void setLinks(List<LinkResource> links) {
            this.links = links;
        }

        public List<String> getTexts() {
            return texts;
        }

        public void setTexts(List<String> texts) {
            this.texts = texts;
        }

        public String getType() {
            if (text != null && links != null) {
                type = CellType.both;
            } else if (text != null) {
                type = CellType.text;
            } else if (links != null) {
                type = CellType.links;
            } else if (texts != null) {
                type = CellType.texts;
            } else {
                type = null;
            }
            return type != null ? type.toString() : null;
        }
    }
}
