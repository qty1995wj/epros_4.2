<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<link />
</head>
<body>
<div id="message_id" style="width: 100%;height:100%">
<div id='messageLeft_id' style="width:175px;height:100%;margin-left:2px;padding:0px;background:#F2F2F2;;">
		<ul id="messageUl_id">
			<li id="${basePath}jsp/message/unRead.jsp;unMessageSysGrid"><s:text name="unReadMesg" /></li><!-- 未读消息 -->
			<li id="${basePath}jsp/message/read.jsp;messageSysGrid"><s:text name="hasReadMesg" /></li><!-- 已读消息 -->
		</ul>
</div>
</div>
</body>
</html>