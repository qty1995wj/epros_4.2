package com.jecn.epros.server.webBean.file;

public class FileUseBean {

	/** 文件ID */
	private Long fileId;
	/** 文件名称 */
	private String fileName;
	/** 类型名称 */
	private String typeName;
	/** 密级*/
	private long isPublic;
	/** ID*/
	private long id;
	/** 标准类型(0是目录，1文件标准，2流程标准 3流程地图标准)*/
	private int stanType;
	
	public long getIsPublic() {
		return isPublic;
	}
	public void setIsPublic(long isPublic) {
		this.isPublic = isPublic;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getStanType() {
		return stanType;
	}
	public void setStanType(int stanType) {
		this.stanType = stanType;
	}
}
