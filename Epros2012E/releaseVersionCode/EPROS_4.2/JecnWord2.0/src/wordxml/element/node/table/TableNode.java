package wordxml.element.node.table;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import wordxml.constant.Constants.Align;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableRow;
import wordxml.element.node.common.CommonUtil;
import wordxml.exception.WordException;
import wordxml.util.Unit;

/**
 * 操作表格节点类
 */
public class TableNode {
	private static final Log log = LogFactory.getLog(TableNode.class);

	/**
	 * 创建一个表格 支持单元格横向，纵向合并 N*N 第一行当作表头处理 可以设置表头加粗，第一列加粗
	 * 
	 * @param tab
	 * @param parentEle
	 */
	public static void createTable(Table tab, Element parentEle) {
		if (parentEle == null || tab == null) {
			log.info("TableNode class createTable Argument is not valid");
			return;
		}
		// 创建表格
		Element tbl = parentEle.addElement("w:tbl");
		// 设置表格属性
		setTblPr(tbl, tab);
		// 表格行
		for (TableRow tblRow : tab.getRows()) {
			// 创建行数据
			TableRowNode.createTblRow(tbl, tblRow, true);
		}
	}

	/**
	 * 设置表格属性
	 * 
	 * @param tblEle
	 *            表格标签 w:tbl
	 * @throws WordException
	 */
	private static void setTblPr(Element tblEle, Table tbl) {
		if (tblEle == null || tbl == null) {
			return;
		}
		// 设置表格列宽
		Element tblGrid = tblEle.addElement("w:tblGrid");
		float[] colWidth = tbl.getColWidths();
		float tableWidth = 0;
		for (int i = 0; i < colWidth.length; i++) {
			tblGrid.addElement("w:gridCol").addAttribute("w:w", "" + Unit.convertCmToTwip(colWidth[i]));
			tableWidth += colWidth[i];
		}
		// 表格属性
		Element tblPr = tblEle.addElement("w:tblPr");
		// 宽度和计算方式
		tblPr.addElement("w:tblW").addAttribute("w:w", Unit.convertCmToTwip(tableWidth)).addAttribute("w:type", "dxa");

		TableBean tableBean = tbl.getTableBean();
		if (tableBean == null) {
			return;
		}
		// 存在左缩进时，居中和右对齐无效
		if (tableBean.getTableAlign() != null
				&& (tableBean.getTableAlign().equals(Align.center) || tableBean.getTableAlign().equals(Align.right))) {
			// 对齐方式
			tblPr.addElement("w:jc").addAttribute("w:val", tableBean.getTableAlign().toString());
		}

		// 表格缩进
		if (tableBean.getTableLeftInd() != null) {
			tblPr.addElement("w:tblInd").addAttribute("w:w", Unit.convertCmToTwip(tableBean.getTableLeftInd()))
					.addAttribute("w:type", "dxa");
		}

		// 表格边框
		if (bordersNotNull(tableBean)) {
			Element tblBorders = tblPr.addElement("w:tblBorders");
			if (tableBean.getBorderTop() != null) {
				Element borderEle = tblBorders.addElement("w:top");
				CommonUtil.buildBorder(tableBean.getBorderTop(), borderEle);
			}
			if (tableBean.getBorderLeft() != null) {
				Element borderEle = tblBorders.addElement("w:left");
				CommonUtil.buildBorder(tableBean.getBorderLeft(), borderEle);
			}
			if (tableBean.getBorderBottom() != null) {
				Element borderEle = tblBorders.addElement("w:bottom");
				CommonUtil.buildBorder(tableBean.getBorderBottom(), borderEle);
			}
			if (tableBean.getBorderRight() != null) {
				Element borderEle = tblBorders.addElement("w:right");
				CommonUtil.buildBorder(tableBean.getBorderRight(), borderEle);
			}
			if (tableBean.getBorderInsideH() != null) {
				Element borderEle = tblBorders.addElement("w:insideH");
				CommonUtil.buildBorder(tableBean.getBorderInsideH(), borderEle);
			}
			if (tableBean.getBorderInsideV() != null) {
				Element borderEle = tblBorders.addElement("w:insideV");
				CommonUtil.buildBorder(tableBean.getBorderInsideV(), borderEle);
			}
		}

		// 单元格间距
		CellMarginBean cellMargin = tableBean.getCellMarginBean();
		if (cellMargin != null) {
			Element tcMar = tblPr.addElement("w:tblCellMar");
			Element top = tcMar.addElement("w:top");
			// 构建margin 属性值
			buildMargin(Unit.convertCmToTwip(cellMargin.getTop()), top);
			Element left = tcMar.addElement("w:left");
			buildMargin(Unit.convertCmToTwip(cellMargin.getLeft()), left);
			Element bottom = tcMar.addElement("w:bottom");
			buildMargin(Unit.convertCmToTwip(cellMargin.getBottom()), bottom);
			Element right = tcMar.addElement("w:right");
			buildMargin(Unit.convertCmToTwip(cellMargin.getRight()), right);
		}
		// <w:tblLayout w:type="Fixed"/> 不开启自动调整尺寸以适应内容大小
		tblPr.addElement("w:tblLayout").addAttribute("w:type", "Fixed");
	}

	/**
	 * 表格边框都为空
	 * 
	 * @author weidp
	 * @date 2014-10-22 上午10:51:53
	 * @param tableBean
	 * @return
	 */
	private static boolean bordersNotNull(TableBean tableBean) {
		return tableBean.getBorderTop() != null || tableBean.getBorderLeft() != null
				|| tableBean.getBorderBottom() != null || tableBean.getBorderRight() != null
				|| tableBean.getBorderInsideH() != null || tableBean.getBorderInsideV() != null;
	}

	/**
	 * 构建Magin
	 * 
	 * @author weidp
	 * @date 2014-10-22 上午10:54:28
	 * @param val
	 * @param position
	 */
	private static void buildMargin(String val, Element position) {
		position.addAttribute("w:w", val);
		position.addAttribute("w:type", "dxa");
	}
}
