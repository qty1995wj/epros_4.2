package epros.designer.gui.standard;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 添加标准条款
 * 
 * @author user
 * 
 */
public class AddStanClauseDialog extends StanClauseDialog {
	private static Logger log = Logger.getLogger(AddStanClauseDialog.class);

	public AddStanClauseDialog(JecnTreeNode node, JTree jTree) {
		super(node, jTree);
		this.setTitle(JecnProperties.getValue("newStanClause"));
	}

	/**
	 * @author yxw 2013-11-4
	 * @description:确定执行事件
	 */
	protected boolean okButtonAction() {
		if (!check()) {
			return false;
		}
		String clauseName = clauseField.getText().trim();
		String clauseContent = clauseContentArea.getText().trim();
		StandardBean standardBean = new StandardBean();
		standardBean.setPreCriterionClassId(node.getJecnTreeBean().getId());
		standardBean.setCriterionClassName(clauseField.getText().trim());
		standardBean.setProjectId(JecnConstants.projectId);
		standardBean.setCreatePeopleId(JecnConstants.getUserId());
		standardBean.setUpdatePeopleId(JecnConstants.getUserId());
		standardBean.setIsPublic(1L);
		standardBean.setStanType(4);
		standardBean.setStanContent(clauseContent);
		standardBean.setIsproFile(programFileCheck.isSelected() ? 1 : 0);
		standardBean.setNote(remarkArea.getText().trim());
		standardBean.setSortId(Long.valueOf(node.getChildCount() + 1));
		standardBean.setFileContents(uploadFileCommon.getFileContents());
		Long id = null;
		try {
			id = ConnectionPool.getStandardAction().addStandard(standardBean);
			// 向树节点添加标准节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(clauseName);
			jecnTreeBean.setPid(node.getJecnTreeBean().getId());
			jecnTreeBean.setPname(node.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.standardClause);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, node);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("newStandardError"));
			log.error(JecnProperties.getValue("newStandardError"), e);
		}
		return true;
	}

	/**
	 * 内容是否变化
	 * 
	 * @author hyl
	 * @return 是返回true 否则false
	 */
	public boolean isUpdate() {
		// 由于添加标准条款的时候一切为空，所以只要有内容就是变化了。
		if (!"".equals(clauseField.getText().trim())) {
			return true;
		}
		if (!"".equals(clauseContentArea.getText().trim())) {
			return true;
		}
		if (!"".equals(remarkArea.getText().trim())) {
			return true;
		}
		if (programFileCheck.isSelected()) {
			return true;
		}
		return false;
	}

	@Override
	protected boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameAdd(node, name, TreeNodeType.standardClause);
	}

}
