package com.jecn.epros.server.service.process.buss;

import java.io.ByteArrayOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public abstract class BaseCheckout implements ICheckout {

	protected Logger log = Logger.getLogger(this.getClass());

	@Override
	public byte[] getByte() {
		return inventoryToByteArray(createWorkBook());
	}

	protected abstract HSSFWorkbook createWorkBook();

	/**
	 * 所有的清单的转换为byte数组工具类
	 */
	public byte[] inventoryToByteArray(HSSFWorkbook workBook) {
		ByteArrayOutputStream out = null;
		byte[] bytes = new byte[0];
		try {
			out = new ByteArrayOutputStream();
			workBook.write(out);
		} catch (Exception e) {
			log.error("生成清单异常", e);
		} finally {
			if (out != null) {
				bytes = out.toByteArray();
				IOUtils.closeQuietly(out);
			}
		}
		return bytes;
	}

}
