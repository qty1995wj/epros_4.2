package epros.draw.gui.top.toolbar.dialog;

import java.awt.Dimension;

import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.toolbar.element.JecnFlowElementMainPanel;

/**
 * 
 * 流程元素维护类
 * 
 * 主要对流程元素的属性值设置，设置成功后所有新添加的路程元素属性都按照当前设置属性来赋值
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementMainDialog extends JecnDialog {

	/** 流程元素维护内容区域 */
	private JecnFlowElementMainPanel flowElementMainPanel = null;

	public JecnFlowElementMainDialog() {
		intComponents();
	}

	private void intComponents() {
		// 流程元素维护内容区域
		flowElementMainPanel = new JecnFlowElementMainPanel(this);

		// 大小
		int dialogWidth = Integer.valueOf(flowElementMainPanel.getResourceManager().getValue("dialogWidth"));
		int dialogHeight = Integer.valueOf(flowElementMainPanel.getResourceManager().getValue("dialogHeight"));
		Dimension size = new Dimension(dialogWidth, dialogHeight);
		this.setSize(size);
		this.setMinimumSize(size);
		this.setPreferredSize(size);
		this.setModal(true);
//		this.setResizable(false);
		this.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);

		// 标题
		this.setTitle(flowElementMainPanel.getResourceManager().getValue(
				"flowElementTitle"));
		this.getContentPane().add(flowElementMainPanel);
	}
}
