package epros.designer.gui.pagingImage;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnRoleMobile;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnImageUtil;

public class PagingImageUtil {
	private static Logger log = Logger.getLogger(PagingImageUtil.class);

	/**
	 * 获取面板第一条横竖分割线 位置
	 * 
	 * @author fuzhh 2014-2-20
	 * @param workflow
	 * @return
	 */
	public static int getVHPagingLine(JecnDrawDesktopPane workflow) {
		int vhxy = 0;
		if (workflow.getFlowMapData().isHFlag()) { // 横向
			vhxy = workflow.getWidth();
			// 获取第一条横分割线
			JecnBaseVHLinePanel wLinePanel = JecnRoleMobile.getFristVLine(workflow);
			if (wLinePanel != null) {// 有竖分割线
				// 宽
				vhxy = DrawCommon.convertDoubleToInt(wLinePanel.getX() + wLinePanel.getWidth());
			} else {// 没有竖分割线
				// 宽
				vhxy = DrawCommon.convertDoubleToInt(150 * workflow.getWorkflowScale());
			}
		} else {
			vhxy = workflow.getHeight();
			// 获取第一条竖分割线
			JecnBaseVHLinePanel HLinePanel = JecnRoleMobile.getFristHLine(workflow);
			// 分割线高度
			vhxy = HLinePanel.getY();
			if (HLinePanel != null) {// 有横分割线
				// 高
				vhxy = DrawCommon.convertDoubleToInt(HLinePanel.getY() + HLinePanel.getHeight());
			} else {// 没有横分割线
				// 高
				vhxy = DrawCommon.convertDoubleToInt(150 * workflow.getWorkflowScale());
			}
		}
		return vhxy;
	}

	/**
	 * 根据传人数据返回 字节流
	 * 
	 * @author fuzhh 2014-2-21
	 * @param panelList
	 * @return
	 */
	public static List<byte[]> getPagingImageByte(List<JPanel> panelList) {
		List<byte[]> list = new ArrayList<byte[]>();
		for (JComponent component : panelList) {
			int width = component.getWidth() != 0 ? component.getWidth() : component.getPreferredSize().width;
			int height = component.getHeight() != 0 ? component.getHeight() : component.getPreferredSize().height;
			// 获取 图片缓存
			BufferedImage buffImage = JecnImageUtil.saveWorkflowDrawImage(component, width, height, 1.0, 1.0);
			// 转换为字节流
			byte[] data = getByte(buffImage);

			list.add(data);
		}
		return list;
	}

	/**
	 * 转换BuueredImage为字节流
	 * 
	 * @author fuzhh 2014-2-21
	 * @param buffImage
	 * @return
	 */
	public static byte[] getByte(BufferedImage buffImage) {
		// 转换为byte
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			ImageIO.write(buffImage, "jpg", out);
		} catch (IOException e) {
			log.error("PagingImageUtil getByte is error", e);
		} finally {
			try {
				out.close();
			} catch (IOException e1) {
				if (out != null) {
					try {
						out.close();
					} catch (IOException e) {
						log.error("", e);
					}
				}
				log.error("PagingImageUtil getByte is error", e1);
			}
		}
		byte[] data = out.toByteArray();
		return data;
	}
}
