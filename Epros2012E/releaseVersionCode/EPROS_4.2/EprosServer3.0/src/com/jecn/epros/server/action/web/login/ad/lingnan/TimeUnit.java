package com.jecn.epros.server.action.web.login.ad.lingnan;

public enum TimeUnit {
	SECONDS, MINUTES, HOURS;
}