package com.jecn.epros.server.dao.propose.impl;

import java.util.List;

import com.jecn.epros.server.bean.propose.JecnRtnlPropose;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.propose.IRtnlProposeDao;

public class RtnlProposeDaoImpl extends AbsBaseDao<JecnRtnlPropose, String>
		implements IRtnlProposeDao {

	@Override
	public int getProposeDetailNum(String peopleIds, String orgIds,
			String startTime, String endTime) {

		String startTimeDB = dateToAccurateSqlDate(startTime,0);
		String endTimeDB = dateToAccurateSqlDate(endTime,1);

		String sql = "select count(1)" + "  from jecn_rtnl_propose jrp"
				+ " inner join jecn_user ju"
				+ "    on jrp.create_person = ju.people_id"
				+ "   and ju.islock = 0"
				+ "  left join JECN_USER_POSITION_RELATED jupr"
				+ "    on ju.people_id = jupr.people_id"
				+ "  left join JECN_FLOW_ORG_IMAGE jfoi"
				+ "    on jfoi.figure_id = jupr.figure_id"
				+ "  left join jecn_flow_org jfo"
				+ "    on jfoi.org_id = jfo.org_id"
				+ "   where jrp.create_time between " + startTimeDB + " and "
				+ endTimeDB ;

		if (peopleIds != null && !"".equals(peopleIds.trim())
				&& !"-1".equals(peopleIds.trim())) {
			sql += " and ju.people_id in (" + peopleIds + ")";
		}
		if (orgIds != null && !"".equals(orgIds.trim())
				&& !"-1".equals(orgIds.trim())) {
			sql += " and jfo.org_id in (" + orgIds + ")";
		}
		return this.countAllByParamsNativeSql(sql);
	}

	@Override
	public List<Object[]> getProposeDetailList(String peopleIds, String orgIds,
			String startTime, String endTime,int start,int limit) throws Exception {

		String startTimeDB = dateToAccurateSqlDate(startTime,0);
		String endTimeDB = dateToAccurateSqlDate(endTime,1);

		String sql = "select * from (select ju.people_id," + "                ju.true_name,"
				+ "                jfo.org_id,"
				+ "                jfo.org_name,"
				+ "   case when jrp.relation_type = 1" + " then jr.id"
				+ "    when jrp.relation_type = 0 "
				+ " then jfs.flow_id end as related_id,"
				+ "   case when jrp.relation_type = 1" + " then jr.rule_name"
				+ "    when jrp.relation_type = 0 "
				+ " then jfs.flow_name end as related_name,"
				+ "                jrp.id,"
				+ "                jrp.propose_content,"
				+ "                jrp.create_time"
				+ "  from jecn_rtnl_propose jrp" + " inner join jecn_user ju"
				+ "    on jrp.create_person = ju.people_id"
				+ "   and ju.islock = 0"
				+ "  left join JECN_USER_POSITION_RELATED jupr"
				+ "    on ju.people_id = jupr.people_id"
				+ "  left join JECN_FLOW_ORG_IMAGE jfoi"
				+ "    on jfoi.figure_id = jupr.figure_id"
				+ "  left join jecn_flow_org jfo"
				+ "    on jfoi.org_id = jfo.org_id"
				+ "  left join jecn_flow_structure jfs"
				+ "    on jrp.relation_id = jfs.flow_id"
				+ "   and jrp.relation_type = 0" + "  left join jecn_rule jr"
				+ "    on jrp.relation_id = jr.id"
				+ "   and jrp.relation_type = 1"
				+ "   where jrp.create_time between " + startTimeDB + " and "
				+ endTimeDB;

		if (peopleIds != null && !"".equals(peopleIds.trim())
				&& !"-1".equals(peopleIds.trim())) {
			sql += " and ju.people_id in (" + peopleIds + ")";
		}
		if (orgIds != null && !"".equals(orgIds.trim())
				&& !"-1".equals(orgIds.trim())) {
			sql += " and jfo.org_id in (" + orgIds + ")";
		}

		sql += "   )r where r.related_id is not null and r.related_name is not null order by r.org_id,r.people_id asc ";

		return this.listNativeSql(sql,start,limit);

	}

	@Override
	public List<Object[]> getProposeOrgCountList(String orgIds,
			String startTime, String endTime, int start, int limit) throws Exception{
		String startTimeDB = dateToAccurateSqlDate(startTime,0);
		String endTimeDB = dateToAccurateSqlDate(endTime,1);
	

		String sql = " select jfo.org_name," + " ("
				+ "  select count(1) from jecn_rtnl_propose jrp"
				+ "  inner join jecn_user ju"
				+ "    on jrp.create_person = ju.people_id"
				+ "  inner join JECN_USER_POSITION_RELATED jupr"
				+ "    on ju.people_id = jupr.people_id"
				+ "  inner join JECN_FLOW_ORG_IMAGE jfoi"
				+ "    on jfoi.figure_id = jupr.figure_id"
				+ " where jrp.create_time between "
				+ startTimeDB
				+ " and "
				+ endTimeDB
				+ " and  jfoi.org_id = jfo.org_id"
				+ " ) as total_num,"
				+ " ("
				+ " select count(1) from jecn_rtnl_propose jrp"
				+ "  inner join jecn_user ju"
				+ "    on jrp.create_person = ju.people_id"
				+ "  inner join JECN_USER_POSITION_RELATED jupr"
				+ "    on ju.people_id = jupr.people_id"
				+ "  inner join JECN_FLOW_ORG_IMAGE jfoi"
				+ "    on jfoi.figure_id = jupr.figure_id"
				+ " where jrp.create_time between "
				+ startTimeDB
				+ " and "
				+ endTimeDB
				+ " and  jfoi.org_id = jfo.org_id"
				+ "       and jrp.state=2"
				+ " ) as accept_num "
				+ " from jecn_flow_org jfo where jfo.org_id in ("
				+ orgIds
				+ ")";

		return this.listNativeSql(sql, start, limit);
	}
	
	@Override
	public List<Object[]> getProposeCreateCountList(String personIds,
			String startTime, String endTime, int start, int limit)
			throws Exception {
		
		String startTimeDB = dateToAccurateSqlDate(startTime,0);
		String endTimeDB = dateToAccurateSqlDate(endTime,1);
		
        String  sql="SELECT" + 
			        "       JU.TRUE_NAME," + 
			        "       SUM_TMP.CREATE_COUNT," + 
			        "       ACCEPT_TMP.ACCEPT_COUNT" + 
			        "  FROM JECN_USER JU" + 
			        "  LEFT JOIN (SELECT COUNT(RP.ID) CREATE_COUNT, RP.CREATE_PERSON" + 
			        "          FROM JECN_RTNL_PROPOSE RP" + 
			        "         WHERE RP.CREATE_TIME >= " + startTimeDB+
			        "           AND RP.CREATE_TIME <= " + endTimeDB+
			        "         GROUP BY RP.CREATE_PERSON) SUM_TMP" + 
			        "          ON SUM_TMP.CREATE_PERSON = JU.PEOPLE_ID" + 
			        "  LEFT JOIN (SELECT COUNT(RP.ID) ACCEPT_COUNT, RP.CREATE_PERSON" + 
			        "               FROM JECN_RTNL_PROPOSE RP" + 
			        "              WHERE RP.CREATE_TIME >= " + startTimeDB +
			        "                AND RP.CREATE_TIME <= " + endTimeDB+
			        "                AND RP.STATE = 2" + 
			        "              GROUP BY RP.CREATE_PERSON) ACCEPT_TMP" + 
			        "    ON SUM_TMP.CREATE_PERSON = ACCEPT_TMP.CREATE_PERSON" + 
			        "   WHERE JU.PEOPLE_ID IN ("+personIds+")";
        
        return this.listNativeSql(sql, start, limit);
	}
	
	
	/**
	 * 将yyyy-mm-dd 的日期格式化为SQL日期格式为yyyy-mm-dd hh24:mi:ss
	 * @param date 日期 格式为yyyy-mm-dd
	 * @param type 0为开始日期 1为结束日期
	 * @return
	 */
	private String dateToAccurateSqlDate(String date,int type){
		
		String dateDB="";
		if(0==type){
			date=date+" 00:00:00";
		}else if(1==type){
			date=date+" 23:59:59";
		}
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
				dateDB = "to_date('" + date + "','yyyy-mm-dd hh24:mi:ss')";
			
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			dateDB = "convert(varchar(21),'" + date + "',23)";
		}
		
		return dateDB;
		
	}
	
	
}
