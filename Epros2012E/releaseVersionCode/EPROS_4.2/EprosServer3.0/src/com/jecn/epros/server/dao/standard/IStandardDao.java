package com.jecn.epros.server.dao.standard;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.common.IBaseDao;

public interface IStandardDao extends IBaseDao<StandardBean, Long> {
	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除标准
	 * @param setIds
	 * @throws Exception
	 */
	public void deleteStandards(Set<Long> setIds) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除标准
	 * @param setIds
	 * @throws Exception
	 */
	public void deleteStandards(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-12
	 * @description:当文件修改时，同时修改对应的制度名称
	 * @param newName
	 * @param type
	 *            0文件标准，1流程
	 * @param relationId
	 */
	public void reNameStandardByRelationId(String newName, int type, Long relationId);

	List<Object[]> getRoleAuthChildStandards(Long pid, Long projectId, Long peopleId) throws Exception;
}
