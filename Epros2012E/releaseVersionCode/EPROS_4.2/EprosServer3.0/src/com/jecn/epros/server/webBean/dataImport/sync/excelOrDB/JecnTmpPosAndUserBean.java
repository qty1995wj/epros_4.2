package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;

/**
 * 原始人员加岗位数据 （excel或试图获取 供数据校验）
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-5-29 时间：下午07:09:33
 */
public class JecnTmpPosAndUserBean extends AbstractBaseBean {
	/** 人员编号 USER_NUM */
	private String userNum = null;
	/** 真实姓名 TRUE_NAME */
	private String trueName = null;
	/** 任职岗位编号 */
	private String posNum = null;
	/** 任职岗位名称 */
	private String posName = null;
	/** 岗位所属部门编号 */
	private String deptNum = null;
	/** 邮箱 EMAIL */
	private String email = null;
	/** 联系电话 PHONE */
	private String phone = null;
	/** 内外网邮件标识 EMAIL_TYPE */
	private Integer emailType = null;
	/** 存储顺序 */
	private int sort;
	/** 域名称 */
	private String domainName;

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getUserNum() {
		return userNum;
	}

	public void setUserNum(String userNum) {
		this.userNum = userNum;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getEmailType() {
		return emailType;
	}

	public void setEmailType(Integer emailType) {
		this.emailType = emailType;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * 
	 * 邮箱类型是否是0、1
	 * 
	 * @return
	 */
	public boolean checkEmailType() {
		return (emailType != null && (emailType == SyncConstant.EMAIL_TYPE_INNER_INT || emailType == SyncConstant.EMAIL_TYPE_OUTER_INT)) ? true
				: false;
	}

	@Override
	public String toInfo() {
		return null;
	}
}
