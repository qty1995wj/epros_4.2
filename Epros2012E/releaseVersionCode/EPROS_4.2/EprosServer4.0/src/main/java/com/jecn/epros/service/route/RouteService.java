package com.jecn.epros.service.route;

import com.jecn.epros.domain.process.TreeNode;

import java.util.List;
import java.util.Map;

public interface RouteService {
    /**
     * 获得节点路径（所有树节点）
     *
     * @param map
     * @return
     * @
     */
    public List<TreeNode> findNodeRoute(Map<String, Object> map);
}
