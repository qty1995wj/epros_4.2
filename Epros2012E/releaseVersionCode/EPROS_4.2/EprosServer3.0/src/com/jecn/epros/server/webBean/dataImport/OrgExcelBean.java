package com.jecn.epros.server.webBean.dataImport;

public class OrgExcelBean {
	private String orgNumberId;// 组织编号
	private String orgName;// 组织名称
	private String preOrgNumberId;// 组织父编号

	public String getOrgNumberId() {
		return orgNumberId;
	}

	public void setOrgNumberId(String orgNumberId) {
		this.orgNumberId = orgNumberId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPreOrgNumberId() {
		return preOrgNumberId;
	}

	public void setPreOrgNumberId(String preOrgNumberId) {
		this.preOrgNumberId = preOrgNumberId;
	}
}
