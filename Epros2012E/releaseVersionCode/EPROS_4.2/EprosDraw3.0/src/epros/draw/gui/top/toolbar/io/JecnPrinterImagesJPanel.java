package epros.draw.gui.top.toolbar.io;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * 
 * 
 * 打印预览
 * 
 * @author Administrator
 *
 */
public class JecnPrinterImagesJPanel extends JPanel implements Printable {

    private JecnPrinterPreviewJPanel printerPreview;

    public JecnPrinterImagesJPanel(JecnPrinterPreviewJPanel printerPreview) {
        this.printerPreview = printerPreview;
    }

    
    public int print(Graphics g, PageFormat pf, int pageIndex)
            throws PrinterException {
        JComponent jcomponent = (JComponent) printerPreview;
        BufferedImage image = null;
        if (pf.getOrientation() == PageFormat.REVERSE_LANDSCAPE) {
            image = new BufferedImage((int) pf.getPaper().getImageableHeight(), (int) pf.getPaper().getImageableWidth(),
                    BufferedImage.TYPE_INT_RGB);
        } else if (pf.getOrientation() == PageFormat.PORTRAIT) {
            image = new BufferedImage((int)pf.getPaper().getImageableWidth() , (int) pf.getPaper().getImageableHeight(),
                    BufferedImage.TYPE_INT_RGB);
        }
        Graphics graphics = image.getGraphics();
        Graphics2D g2d = (Graphics2D) graphics;
        ArrayList<JComponent> dbcomponents = new ArrayList<JComponent>();
        updateDoubleBuffered(jcomponent, dbcomponents);
        jcomponent.paint(g2d);
        resetDoubleBuffered(dbcomponents);
        g2d.dispose();

        Graphics2D g2d2 = (Graphics2D) g;
        g2d2.drawImage(image, (int) pf.getPaper().getImageableX(), (int) pf.getPaper().getImageableY(), this);
        g2d2.dispose();
        return PAGE_EXISTS;
    }

    private void updateDoubleBuffered(JComponent component, ArrayList<JComponent> dbcomponents) {
        if (component.isDoubleBuffered()) {
            dbcomponents.add(component);
            component.setDoubleBuffered(false);
        }
        for (int i = 0; i < component.getComponentCount(); i++) {
            Component c = component.getComponent(i);
            if (c instanceof JComponent) {
                updateDoubleBuffered((JComponent) c, dbcomponents);
            }
        }
    }

    private void resetDoubleBuffered(ArrayList<JComponent> dbcomponents) {
        for (JComponent component : dbcomponents) {
            component.setDoubleBuffered(true);
        }
    }
}
