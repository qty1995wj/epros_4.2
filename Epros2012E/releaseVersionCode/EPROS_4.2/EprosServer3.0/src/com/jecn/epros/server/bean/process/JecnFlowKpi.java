package com.jecn.epros.server.bean.process;

import java.util.Date;
/**
 * 
 * kpi值bean
 * 
 *
 */
public class JecnFlowKpi implements java.io.Serializable {
	private Long kpiId;// 主键ID
	private String kpiValue;// 值
	private Date kpiHorVlaue;// 日期
	private Long kpiAndId;// kpi的ID
	private long flowId;// 流程ID
	private Date createTime;// 创建时间
	private Date updateTime;// 更新时间
	private long createPeopleId;// 创建人
	private long updatePeopleId;// 更新人
	private String kpiHorVlaueStr;



	public String getKpiHorVlaueStr() {
		return kpiHorVlaueStr;
	}

	public void setKpiHorVlaueStr(String kpiHorVlaueStr) {
		this.kpiHorVlaueStr = kpiHorVlaueStr;
	}

	public Long getKpiId() {
		return kpiId;
	}

	public void setKpiId(Long kpiId) {
		this.kpiId = kpiId;
	}

	public String getKpiValue() {
		return kpiValue;
	}

	public void setKpiValue(String kpiValue) {
		this.kpiValue = kpiValue;
	}

	public Date getKpiHorVlaue() {
		return kpiHorVlaue;
	}

	public void setKpiHorVlaue(Date kpiHorVlaue) {
		this.kpiHorVlaue = kpiHorVlaue;
	}

	public Long getKpiAndId() {
		return kpiAndId;
	}

	public void setKpiAndId(Long kpiAndId) {
		this.kpiAndId = kpiAndId;
	}

	public long getFlowId() {
		return flowId;
	}

	public void setFlowId(long flowId) {
		this.flowId = flowId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public long getCreatePeopleId() {
		return createPeopleId;
	}

	public void setCreatePeopleId(long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}

	public long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

}
