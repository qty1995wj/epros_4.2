package wordxml.exception;

public class WordException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public WordException() {
		super();
	}

	public WordException(String msg) {
		super(msg);
	}

	public WordException(String msg, Throwable t) {
		super(msg, t);
	}
}
