package com.jecn.epros.domain.process.relatedFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;

/**
 * Created by user on 2017/5/2.
 */
public class RelatedFile {

    private Long id;
    private String name;
    private String number;

    public LinkResource getLink() {
        return new LinkResource(id, name, LinkResource.ResourceType.FILE);
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
