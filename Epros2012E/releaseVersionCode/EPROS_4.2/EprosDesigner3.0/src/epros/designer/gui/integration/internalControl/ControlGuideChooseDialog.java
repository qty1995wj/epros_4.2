package epros.designer.gui.integration.internalControl;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;

/**
 * 内控指引知识库选择框界面
 * 
 * @author Administrator
 * 
 */
public class ControlGuideChooseDialog extends JecnSelectChooseDialog {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger
			.getLogger(ControlGuideChooseDialog.class);

	/**
	 * 内控指引知识库选择框界面构造方法
	 * 
	 * @param list
	 *            节点集合
	 * @param jecnDialog
	 *            自定义对话框
	 */
	public ControlGuideChooseDialog(List<JecnTreeBean> list,
			JecnDialog jecnDialog) {
		super(list, 22, jecnDialog);
		this.setTitle(JecnProperties.getValue("controlDB"));
	}

	/**
	 * 内控指引知识库选择框界面构造方法
	 * 
	 * @param list
	 *            节点集合
	 */
	public ControlGuideChooseDialog(List<JecnTreeBean> list) {
		super(list, 22);
		this.setTitle(JecnProperties.getValue("controlDB"));
	}

	/**
	 * 内控指引知识库选择框界面构造方法
	 * 
	 * @param list
	 *            节点集合
	 * @param isVisibleLab
	 *            (待定)
	 */
	public ControlGuideChooseDialog(List<JecnTreeBean> list, int isVisibleLab) {
		super(list, 22);
		if (isVisibleLab == 0) {
			this.getEmptyBtn().setVisible(false);
		}
		this.setTitle(JecnProperties.getValue("controlDB"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	/**
	 * 设置树信息
	 */
	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyJecnControlGuideTree(1, this);
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("clauseName");
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		return title;
	}

	/**
	 * 根据名称查询树节点信息
	 * 
	 * @param name
	 *            查询名称
	 */
	@Override
	public List<JecnTreeBean> searchByName(String name) {
		List<JecnTreeBean> listTreeBean = null;
		// 名称不能大于61个汉字或122个字母，超过返回true，没有超过返回false
		if (DrawCommon.checkNameMaxLength(name)) {
			listTreeBean = new ArrayList<JecnTreeBean>();
		} else {
			listTreeBean = new ArrayList<JecnTreeBean>();
			try {
				if (this.getChooseType() == 22) {
					listTreeBean = ConnectionPool.getControlGuideAction()
							.getControlGuideByName(name);
				}
			} catch (Exception e) {
				log.error("ControlGuideChooseDialog searchByName is error！", e);
			}
		}
		return listTreeBean;
	}

}
