package com.jecn.epros.server.service.system;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.system.JecnElementsLibrary;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;

/**
 * 
 * 配置信息表（JECN_SET_TABLE）操作类
 * 
 * @author ZHOUXY
 * 
 */
public interface IJecnConfigItemService {
	/**
	 * 
	 * 查询流程图设置值
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 流程图设置值
	 */
	public List<JecnConfigItemBean> select(int typeBigModel);

	/**
	 * 
	 * 查询流程文件排序后显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程文件排序后显示数据
	 */
	public List<JecnConfigItemBean> selectShowFlowFile();

	/**
	 * 
	 * 查询流程建设规范显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程建设规范显示数据
	 */
	public List<JecnConfigItemBean> selectShowPartMapStand();

	/**
	 * 
	 * 查询任务审批环节数据（带文控类型）
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 任务审批环节数据（带文控类型）
	 */
	public List<JecnConfigItemBean> selectShowTaskApp(int typeBigModel);

	/**
	 * 
	 * 查询任务审批环节数据（除去文控类型）
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 任务审批环节显示数据
	 */
	public List<JecnConfigItemBean> selectTaskAppTable(int typeBigModel);

	/**
	 * 
	 * 查询所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return List<JecnConfigItemBean> 所有任务显示项以及试运行对应的试运行周期，发送试运行报告周期
	 */
	public List<JecnConfigItemBean> selectTaskShowAllItems(int typeBigModel);

	/**
	 * 
	 * 查询所有表示公开秘密数据集合
	 * 
	 * @return List<JecnConfigItemBean> 所有表示公开秘密数据集合
	 */
	public List<JecnConfigItemBean> selectAllPub();

	/**
	 * 获得合理化建议
	 * 
	 * @return
	 */
	public List<JecnConfigItemBean> getRationalizationProposal();

	/**
	 * 
	 * 修改JECN_CONFIG_ITEM表
	 * 
	 * @param configItemList
	 *            List<JecnConfigItemBean>
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * 
	 * @return boolean true:修改成功；false：修改失败
	 */
	public boolean update(List<JecnConfigItemBean> configItemList, int typeBigModel);

	/**
	 * 
	 * 通过大类下唯一标示获取value字段值
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * @param String
	 *            mark 大类下唯一标识
	 * @return String value字段值或NULL
	 */
	public String selectValue(int typeBigModel, String mark);

	/**
	 * 
	 * 通过大类下唯一标示获取数据
	 * 
	 * @param int typeBigModel 大类 0：流程地图；1：流程图；2：制度；3：文件；4：邮箱配置；5：权限配置 6:其他设置
	 * @param String
	 *            mark 大类下唯一标识
	 * @return String 一行数据字段值或NULL
	 */
	public JecnConfigItemBean selectConfigItemBean(int typeBigModel, String mark);

	/**
	 * 查询流程基本信息
	 * 
	 * @return List<JecnConfigItemBean>查询流程基本信息
	 */
	public List<JecnConfigItemBean> selectBaseInfoFlowFile();

	/**
	 * 获取邮箱配置项
	 * 
	 * @return List<JecnConfigItemBean>邮箱配置项集合
	 * @throws Exception
	 */
	public List<JecnConfigItemBean> selectEmailConfigItem();

	/**
	 * 获取系统配置项信息
	 * 
	 * @param typeBigModel
	 *            int 0：流程地图 ,1：流程图 ,2：制度, 3：文件, 4：邮箱配置, 5：权限配置
	 * @param typeSmallModel
	 *            int 0：审批环节配置, 1：任务界面显示项配置, 2：基本配置, 3：流程图建设规范 ,4：流程文件,
	 *            5：邮箱配置,6：权限配置
	 * @return List<JecnConfigItemBean> 系统配置项信息
	 * @throws Exception
	 */
	public List<JecnConfigItemBean> selectConfigItemBeanByType(int typeBigModel, int typeSmallModel);

	/**
	 * 
	 * 查询流程地图文件排序后显示数据
	 * 
	 * @return List<JecnConfigItemBean> 流程文件排序后显示数据
	 */
	public List<JecnConfigItemBean> selectFlowMapShowFlowFile();

	/**
	 * 获取小类对应的配置信息
	 * 
	 * @param typeSmallModel
	 *            0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
	 *            11:发布配置
	 * @return List<JecnConfigItemBean>
	 */
	List<JecnConfigItemBean> selectSmallTypeItem(int typeSmallModel);

	public List<JecnConfigItemBean> selectShowSortConfigItemBeanByType(int typeBigModel, int typeSmallModel);

	public List<JecnConfigItemBean> selectKpiConfigItemBeanByTop(int top);

	List<JecnConfigItemBean> selectByTypeBigModel(int typeBigModel);

	List<JecnConfigItemBean> selectAllItem() throws Exception;

	void update(List<JecnConfigItemBean> updateItemList);

	List<JecnConfigItemBean> selectTableConfigItemBeanByType(int typeBigModel, int typeSmallModel);
	
	public Map<DictionaryEnum, List<JecnDictionary>>  initDictionaryMap();

	public List<Object[]> selectMaintainNode();

	public JecnElementsLibrary getElement(String mark);
}
