package com.jecn.epros.server.dao.process.impl;

import java.util.Date;
import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.process.IProcessKPIValueDao;

public class ProcessKPIValueDaoImpl extends AbsBaseDao<JecnFlowKpi, Long>
		implements IProcessKPIValueDao {
	@Override
	public List<JecnFlowKpi> getJecnFlowKpiListBy(Long kpiId) throws Exception {
		String hql = "from JecnFlowKpi where kpiAndId=? order by kpiHorVlaue";
		return this.listHql(hql, kpiId);
	}

	@Override
	public List<JecnFlowKpi> getJecnFlowKpiListByFlowId(Long flowId)
			throws Exception {
		String hql = "select a from JecnFlowKpi as a,JecnFlowKpiNameT as b where a.kpiAndId=b.kpiAndId and b.flowId=?";
		return this.listHql(hql, flowId);
	}

	/**
	 * 流程PKI数据处理DAO
	 * 
	 * @author ZHANGXIAOHU
	 * @date： 日期：Jan 16, 2013 时间：5:59:18 PM
	 */
	@Override
	public List<JecnFlowKpiName> getListJecnFlowKpiName(Long flowId)
			throws Exception {
		String hql = "from JecnFlowKpiName where flowId=? order by kpiName";
		return this.listHql(hql, flowId);
	}

	/**
	 * 获取单个KPI
	 */
	public JecnFlowKpiName getJecnFlowKpiName(Long kpiAndId) throws Exception {
		String hql = "from JecnFlowKpiName where kpiAndId=?";
		return this.getObjectHql(hql, kpiAndId);
	}

	/**
	 * 时间段查询KPI的值
	 * 
	 * @param kpiId
	 *            KPI从表之间ID
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @return List<JecnFlowKpi>
	 * @throws Exception
	 */
	@Override
	public List<JecnFlowKpi> findJecnFlowKpiList(Long kpiId, Date startTime,
			Date endTime) throws Exception {
		String hql = "from JecnFlowKpi where kpiAndId = ? and kpiHorVlaue>=? and kpiHorVlaue<=? order by kpiHorVlaue asc";
		return this.listHql(hql, kpiId, startTime, endTime);
	}

	public JecnFlowKpi getFlowKPIValue(long kpiAndId, String kpiVlaue)
			throws Exception {
		String hql = "from JecnFlowKpi where kpiAndId = ? and kpiHorVlaue=?";
		return this.getObjectHql(hql, kpiAndId, kpiVlaue);
	}

}
