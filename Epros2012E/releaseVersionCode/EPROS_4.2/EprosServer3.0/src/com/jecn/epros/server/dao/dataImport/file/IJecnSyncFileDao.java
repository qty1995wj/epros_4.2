package com.jecn.epros.server.dao.dataImport.file;

import com.jecn.epros.server.common.IBaseDao;

public interface IJecnSyncFileDao extends IBaseDao<String, String> {

	void updateFileNameByOrg() throws Exception;

}
