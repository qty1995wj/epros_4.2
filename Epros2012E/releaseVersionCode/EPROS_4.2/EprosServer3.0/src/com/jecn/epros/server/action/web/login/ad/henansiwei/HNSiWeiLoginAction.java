package com.jecn.epros.server.action.web.login.ad.henansiwei;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 河南思维
 * 
 *@author ZXH
 * @date 2016-6-14下午01:40:35
 */
public class HNSiWeiLoginAction extends JecnAbstractADLoginAction {
	/** http中员工编号参数 */
	private static final String NUM_PARA = "UserNumber";

	public HNSiWeiLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		// http://127.0.0.1:8080/login.action?UserNumber=admin
		try {
			String queryString = loginAction.getRequest().getQueryString();
			if (StringUtils.isEmpty(queryString)) {
				return loginAction.loginGen();
			}
			String[] str = queryString.split("&");
			queryString = str[0].substring(0);

			String em_Num = queryString.substring(NUM_PARA.length() + 1, queryString.length());
			if (StringUtils.isBlank(em_Num)) {// 单点登录名称为空，执行普通登录路径
				return loginAction.loginGen();
			}
			log.info("URL获取： loginName = " + em_Num);

			// 解密
			SiWeiDesKey desKey = new SiWeiDesKey();
			// 解密用户名
			String loginName = desKey.decode(em_Num);

			log.info(" 解密后：loginName = " + loginName);
			if (StringUtils.isBlank(loginName)) {// 单点登录名称为空，执行普通登录路径
				return loginAction.loginGen();
			}
			return loginByLoginName(loginName);
		} catch (Exception e) {
			log.error("思维单点登录异常！" + loginAction.getRequest().getQueryString(), e);
			return null;
		}
	}
}
