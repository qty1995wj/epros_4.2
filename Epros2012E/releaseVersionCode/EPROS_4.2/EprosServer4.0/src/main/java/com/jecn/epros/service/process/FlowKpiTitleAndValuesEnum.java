package com.jecn.epros.service.process;

import com.jecn.epros.domain.process.kpi.FlowKpiName;
import com.jecn.epros.domain.process.processFile.TmpKpiShowValues;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.sqlprovider.server3.ConfigContents.ConfigItemPartMapMark;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public enum FlowKpiTitleAndValuesEnum {
    INSTANCE;

    public TmpKpiShowValues getConfigKpiNameAndValues(List<FlowKpiName> kpiNameList,
                                                      List<ConfigItemBean> configItemBeans) {
        TmpKpiShowValues kpiShowValues = new TmpKpiShowValues();
        // 标题名称
        List<String> kpiTitles = new ArrayList<String>();
        // 行数据
        List<List<String>> rowValues = new ArrayList<List<String>>();
        if (configItemBeans == null) {
            return null;
        }
        int count = 0;
        for (ConfigItemBean ConfigItemBean : configItemBeans) {
            kpiTitles.add(ConfigItemBean.getName(AuthenticatedUserUtil.isLanguageType()));
            if (count == 4) {
                break;
            }
            count++;
        }
        for (FlowKpiName flowKpiName : kpiNameList) {
            rowValues.add(getRowValues(flowKpiName, configItemBeans));
        }
        kpiShowValues.setKpiRowValues(rowValues);
        kpiShowValues.setKpiTitles(kpiTitles);
        return kpiShowValues;
    }

    public TmpKpiShowValues getConfigKpiNameTAndValues(List<FlowKpiName> kpiNameList,
                                                       List<ConfigItemBean> configItemBeans) throws IllegalAccessException, InvocationTargetException {
        TmpKpiShowValues kpiShowValues = new TmpKpiShowValues();
        // 标题名称
        List<String> kpiTitles = new ArrayList<String>();
        // 行数据
        List<List<String>> rowValues = new ArrayList<List<String>>();
        if (configItemBeans == null) {
            return null;
        }
        int count = 0;
        for (ConfigItemBean ConfigItemBean : configItemBeans) {
            kpiTitles.add(ConfigItemBean.getName());
            if (count == 4) {
                break;
            }
            count++;
        }
        for (FlowKpiName flowKpiName : kpiNameList) {
            rowValues.add(getRowValues(flowKpiName, configItemBeans));
        }
        kpiShowValues.setKpiRowValues(rowValues);
        kpiShowValues.setKpiTitles(kpiTitles);
        kpiShowValues.setKpiNameList(kpiNameList);
        return kpiShowValues;
    }

    /**
     * 根据配置获取KPI前几列数据
     *
     * @param flowKpiName
     * @param configItemBeans
     * @return
     */
    public List<String> getConfigKpiRowValues(FlowKpiName flowKpiName, List<ConfigItemBean> configItemBeans) {
        List<String> rowValues = new ArrayList<String>();
        int count = 0;
        for (ConfigItemBean ConfigItemBean : configItemBeans) {
            rowValues.add(getKpiCells(ConfigItemBean.getMark(), flowKpiName));
            if (count == 4) {
                return rowValues;
            }
            count++;
        }
        return rowValues;
    }

    /**
     * KPI行数据
     *
     * @param flowKpiName
     * @param configItemBeans
     * @return
     */
    private List<String> getRowValues(FlowKpiName flowKpiName, List<ConfigItemBean> configItemBeans) {
        List<String> rowValues = new ArrayList<String>();
        int count = 0;
        for (ConfigItemBean ConfigItemBean : configItemBeans) {
            rowValues.add(getKpiCells(ConfigItemBean.getMark(), flowKpiName));
            if (count == 4) {
                return rowValues;
            }
            count++;
        }
        return rowValues;
    }

    private String getKpiCells(String mark, FlowKpiName flowKpiName) {
        if (ConfigItemPartMapMark.kpiName.toString().equals(mark)) {
            return flowKpiName.getKpiName();
        } else if (ConfigItemPartMapMark.kpiType.toString().equals(mark)) {
            return flowKpiName.getKpuStringType();
        } else if (ConfigItemPartMapMark.kpiUtilName.toString().equals(mark)) {
            return flowKpiName.getKpiStringVertical();// kpi单位名称
        } else if (ConfigItemPartMapMark.kpiTargetValue.toString().equals(mark)) {
            return flowKpiName.getKpiTarget();// kpi目标值
        } else if (ConfigItemPartMapMark.kpiTimeAndFrequency.toString().equals(mark)) {
            return flowKpiName.getKpiStringHorizontal();// KPI纵坐标 数据统计时间/频率 0：月
            // 1：季度
        } else if (ConfigItemPartMapMark.kpiDefined.toString().equals(mark)) {
            return flowKpiName.getKpiDefinition();// KPI定义
        } else if (ConfigItemPartMapMark.kpiDesignFormulas.toString().equals(mark)) {
            return flowKpiName.getKpiStatisticalMethods();// 流程计算公式
        } else if (ConfigItemPartMapMark.kpiDataProvider.toString().equals(mark)) {
            return flowKpiName.getKpiDataPeopleName();// 数据提供者
        } else if (ConfigItemPartMapMark.kpiDataAccess.toString().equals(mark)) {
            return flowKpiName.getKpiDataMethodString();
        } else if (ConfigItemPartMapMark.kpiIdSystem.toString().equals(mark)) {
            return flowKpiName.getKpiITSystemNames();// IT系统
        } else if (ConfigItemPartMapMark.kpiSourceIndex.toString().equals(mark)) {
            return flowKpiName.getKpiStringTargetType();// 指标来源
        } else if (ConfigItemPartMapMark.kpiRrelevancy.toString().equals(mark)) {
            return flowKpiName.getStringkpiRelevance();// 相关度
        } else if (ConfigItemPartMapMark.kpiSupportLevelIndicator.toString().equals(mark)) {
            return flowKpiName.getFirstTargetContent();// 支持的一级指标
        } else if (ConfigItemPartMapMark.kpiPurpose.toString().equals(mark)) {
            return flowKpiName.getKpiPurpose();// 设置目的
        } else if (ConfigItemPartMapMark.kpiPoint.toString().equals(mark)) {
            return flowKpiName.getKpiPoint();// 测量点
        } else if (ConfigItemPartMapMark.kpiPeriod.toString().equals(mark)) {
            return flowKpiName.getKpiPeriod();// 统计周期
        } else if (ConfigItemPartMapMark.kpiExplain.toString().equals(mark)) {
            return flowKpiName.getKpiExplain();// 说明
        }
        return "";
    }
}
