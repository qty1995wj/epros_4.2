package epros.designer.gui.recycle;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class RuleRecycleTreeListener extends RecycleTreeListener {

	public RuleRecycleTreeListener(JecnHighEfficiencyTree jTree) {
		super(jTree);
	}


	@Override
	protected List<JecnTreeBean> getTreeExpandedData(JecnTreeNode node)
			throws Exception {
		return ConnectionPool.getRuleAction().getRecycleJecnTreeBeanList(
				JecnConstants.projectId, node.getJecnTreeBean().getId());
	}

}
