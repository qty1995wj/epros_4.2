package com.jecn.epros.server.webBean.popedom;

import java.io.Serializable;

public class PosBean implements Serializable{
	/**岗位ID*/
	private Long posId;
	/**岗位名称*/
	private String posName;
	/**部门ID*/
	private Long orgId;
	/**部门名称*/
	private String orgName;
	public Long getPosId() {
		return posId;
	}
	public void setPosId(Long posId) {
		this.posId = posId;
	}
	public String getPosName() {
		return posName;
	}
	public void setPosName(String posName) {
		this.posName = posName;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
}
