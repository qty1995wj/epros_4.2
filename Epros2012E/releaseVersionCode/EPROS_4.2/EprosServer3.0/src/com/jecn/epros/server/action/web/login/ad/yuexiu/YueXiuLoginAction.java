package com.jecn.epros.server.action.web.login.ad.yuexiu;

import org.jasig.cas.client.util.AssertionHolder;
import org.jasig.cas.client.validation.Assertion;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

public class YueXiuLoginAction extends JecnAbstractADLoginAction {

	public YueXiuLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		Assertion assertion = AssertionHolder.getAssertion();
		String loginName = "";
		if (assertion != null) {
			loginName = assertion.getPrincipal().getName();
		}
		log.info("loginName = " + loginName);
		return loginByLoginName(loginName);
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return 0 登录验证成功，1,验证失败，2接口异常
	 */
	public static int designerLogin(String username, String password) {
		final String server = YueXiuAfterItem.getValue("server");
		final String service = YueXiuAfterItem.getValue("service");
		// 对接系统登录地址
		final String proxyValidate = YueXiuAfterItem.getValue("proxyValidate");

		try {
			boolean isResult = YueXiuCSLonin.ticketValidate(proxyValidate, YueXiuCSLonin.getTicket(server, username,
					password, service), service);
			if (isResult) {
				return 0;
			} else {
				return 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("designerLogin SSO is error!", e);
			return 2;
		}
	}

}
