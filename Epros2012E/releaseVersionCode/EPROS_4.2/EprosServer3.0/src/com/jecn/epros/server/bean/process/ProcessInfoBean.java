package com.jecn.epros.server.bean.process;

import java.io.Serializable;

import com.jecn.epros.server.bean.popedom.AccessId;

public class ProcessInfoBean implements Serializable {
	/** 流程Id */
	private Long flowId;
	/** 更新人ID */
	private Long updatePeopleId;
	/** 流程编号 */
	private String flowNumber;
	/** 有效期 */
	private int validityValue;
	/** 流程类别 */
	private Long typeId;
	/** 责任人类型 0是人员，1是岗位 */
	private int resPersonType;
	/** 责任人ID */
	private Long resPersonId;
	/** 监护人 */
	private Long guardianId;
	/** 拟制人 */
	private Long fictionPeopleId;
	/** 责任部门 */
	private Long resOrgId;
	/** 密级 0是秘密 1是公开 */
	private int isPublic;
	/** 部门查阅权限IDS */
	private String orgAccessAuthorityids;
	/** 岗位查阅权限IDS */
	private String posAccessAuthorityids;
	/** 岗位组查阅权限IDS */
	private String posGroupAccessAuthorityids;
	/** 支持工具 */
	private String supportToolIds;
	/** 附件 */
	private Long fileId;
	/** 保密级别 **/
	private Integer confidentialityLevel;
	/** 流程专员 */
	private Long commissionerId;
	/** 关键字 */
	private String keyWord;
	/** 查阅权限集合 */
	private AccessId accId;
	/**业务类型**/
	private String bussType;

	
	
	public String getBussType() {
		return bussType;
	}

	public void setBussType(String bussType) {
		this.bussType = bussType;
	}

	public Integer getConfidentialityLevel() {
		return confidentialityLevel;
	}

	public void setConfidentialityLevel(Integer confidentialityLevel) {
		this.confidentialityLevel = confidentialityLevel;
	}

	
	public AccessId getAccId() {
		return accId;
	}

	public void setAccId(AccessId accId) {
		this.accId = accId;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}

	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}

	public String getFlowNumber() {
		return flowNumber;
	}

	public void setFlowNumber(String flowNumber) {
		this.flowNumber = flowNumber;
	}

	public int getValidityValue() {
		return validityValue;
	}

	public void setValidityValue(int validityValue) {
		this.validityValue = validityValue;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public int getResPersonType() {
		return resPersonType;
	}

	public void setResPersonType(int resPersonType) {
		this.resPersonType = resPersonType;
	}

	public Long getResPersonId() {
		return resPersonId;
	}

	public void setResPersonId(Long resPersonId) {
		this.resPersonId = resPersonId;
	}

	public Long getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(Long guardianId) {
		this.guardianId = guardianId;
	}

	public Long getFictionPeopleId() {
		return fictionPeopleId;
	}

	public void setFictionPeopleId(Long fictionPeopleId) {
		this.fictionPeopleId = fictionPeopleId;
	}

	public Long getResOrgId() {
		return resOrgId;
	}

	public void setResOrgId(Long resOrgId) {
		this.resOrgId = resOrgId;
	}

	public int getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(int isPublic) {
		this.isPublic = isPublic;
	}

	public String getOrgAccessAuthorityids() {
		return orgAccessAuthorityids;
	}

	public void setOrgAccessAuthorityids(String orgAccessAuthorityids) {
		this.orgAccessAuthorityids = orgAccessAuthorityids;
	}

	public String getPosAccessAuthorityids() {
		return posAccessAuthorityids;
	}

	public void setPosAccessAuthorityids(String posAccessAuthorityids) {
		this.posAccessAuthorityids = posAccessAuthorityids;
	}

	public String getPosGroupAccessAuthorityids() {
		return posGroupAccessAuthorityids;
	}

	public void setPosGroupAccessAuthorityids(String posGroupAccessAuthorityids) {
		this.posGroupAccessAuthorityids = posGroupAccessAuthorityids;
	}

	public String getSupportToolIds() {
		return supportToolIds;
	}

	public void setSupportToolIds(String supportToolIds) {
		this.supportToolIds = supportToolIds;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public Long getCommissionerId() {
		return commissionerId;
	}

	public void setCommissionerId(Long commissionerId) {
		this.commissionerId = commissionerId;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

}
