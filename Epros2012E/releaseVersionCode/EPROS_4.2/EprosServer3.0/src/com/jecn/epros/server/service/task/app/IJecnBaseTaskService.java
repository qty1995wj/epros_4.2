package com.jecn.epros.server.service.task.app;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.external.TaskParam;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;
import com.jecn.epros.server.bean.task.temp.JecnTaskFileTemporary;
import com.jecn.epros.server.bean.task.temp.SimpleSubmitMessage;
import com.jecn.epros.server.bean.task.temp.SimpleTaskBean;

/**
 * 任务审批业务处理接口
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Nov 27, 2012 时间：11:28:31 AM
 */
public interface IJecnBaseTaskService {
	/**
	 * 创建:流程任务、文件、制度任务
	 * 
	 * @param tempCreateTaskBean
	 *            创建任务信息
	 * @throws BsException
	 */
	public void createPRFTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception;

	/**
	 * ****************************任务审批*****************************************
	 * *********
	 */

	/**
	 * 审批通过
	 * 
	 * @param submitMessage
	 *            审核人提交信息
	 * 
	 */
	public void taskOperation(SimpleSubmitMessage submitMessage) throws Exception;

	/**
	 * 验证当前登录人是否为任务审批人
	 * 
	 * @param peopleId
	 *            登录人人员主键ID
	 * @param taskId
	 *            任务主键ID
	 * @return True 是当前任务审批人；false：不是当前任务审批人
	 * @throws Exception
	 */
	public boolean isApprovePeopleChecked(Long peopleId, Long taskId, int taskElseState, int approveCounts)
			throws Exception;

	/**
	 * 打开试用行报告
	 * 
	 * @param id
	 *            任务主键ID
	 * @return JecnTaskFileTemporary 试运行文件
	 * @throws Exception
	 */
	public JecnTaskFileTemporary downLoadTaskRunFile(Long id) throws Exception;

	/**
	 * 获取任务信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @return SimpleTaskBean 页面获取的任务相关信息
	 * @throws Exception
	 */
	public SimpleTaskBean getSimpleTaskBeanById(Long taskId, int taskOperation) throws Exception;

	/**
	 * 验证当前任务，当前操作人是否已审批
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param curPeopleId
	 *            操作人
	 * @param taskOperation
	 *            我的任务操作人的操作状态 0审批;1重新提交;2整理意见;3任务管理，编辑任务
	 * @return
	 * @throws Exception
	 */
	public boolean hasApproveTask(Long taskId, Long curPeopleId, int taskOperation) throws Exception;

	/**
	 * 检测任务是否搁置
	 * 
	 * @param simpleTaskBean
	 *            任务信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public SimpleTaskBean checkTaskIsDelay(SimpleTaskBean simpleTaskBean) throws Exception;

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public JecnTaskBeanNew getTaskById(Long taskId) throws Exception;

	public void releaseTaskByWebService(Long taskId, List<JecnTaskHistoryFollow> taskHistorys) throws Exception;

	public MessageResult handleTaskExternal(TaskParam param);

}
