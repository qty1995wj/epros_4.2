package epros.draw.gui.top.optionpane;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.UIManager;

import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 
 * 确认对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnOptionPane extends JOptionPane {

	public JecnOptionPane(Object message, int messageType, int optionType, Icon icon, Object[] options,
			Object initialValue) {
		super(message, messageType, optionType, icon, options, initialValue);
	}

	/**
	 * Brings up an information-message dialog titled "Message".
	 * 
	 * @param parentComponent
	 *            determines the <code>Frame</code> in which the dialog is
	 *            displayed; if <code>null</code>, or if the
	 *            <code>parentComponent</code> has no <code>Frame</code>, a
	 *            default <code>Frame</code> is used
	 * @param message
	 *            the <code>Object</code> to display
	 * @exception HeadlessException
	 *                if <code>GraphicsEnvironment.isHeadless</code> returns
	 *                <code>true</code>
	 * @see java.awt.GraphicsEnvironment#isHeadless
	 */
	public static void showMessageDialog(Component parentComponent, Object message) throws HeadlessException {
		showMessageDialog(parentComponent, message, null, INFORMATION_MESSAGE);
	}

	public static void showMessageDialog(Component parentComponent, Object message, String title, int messageType)
			throws HeadlessException {
		showMessageDialog(parentComponent, message, title, messageType, null);
	}

	public static void showMessageDialog(Component parentComponent, Object message, String title, int messageType,
			Icon icon) throws HeadlessException {
		showOptionDialog(parentComponent, message, title, DEFAULT_OPTION, messageType, icon, null, null);
	}

	/**
	 * Brings up a dialog where the number of choices is determined by the
	 * <code>optionType</code> parameter.
	 * 
	 * @param parentComponent
	 *            determines the <code>Frame</code> in which the dialog is
	 *            displayed; if <code>null</code>, or if the
	 *            <code>parentComponent</code> has no <code>Frame</code>, a
	 *            default <code>Frame</code> is used
	 * @param message
	 *            the <code>Object</code> to display
	 * @param title
	 *            the title string for the dialog
	 * @param optionType
	 *            an int designating the options available on the dialog:
	 *            <code>YES_NO_OPTION</code>, <code>YES_NO_CANCEL_OPTION</code>,
	 *            or <code>OK_CANCEL_OPTION</code>
	 * @return an int indicating the option selected by the user
	 * @exception HeadlessException
	 *                if <code>GraphicsEnvironment.isHeadless</code> returns
	 *                <code>true</code>
	 * @see java.awt.GraphicsEnvironment#isHeadless
	 */
	public static int showConfirmDialog(Component parentComponent, Object message, String title, int optionType)
			throws HeadlessException {

		return showConfirmDialog(parentComponent, message, title, optionType, QUESTION_MESSAGE);
	}

	public static int showConfirmDialog(Component parentComponent, Object message, String title, int optionType,
			int messageType) throws HeadlessException {
		return showConfirmDialog(parentComponent, message, title, optionType, messageType, null);
	}

	public static int showConfirmDialog(Component parentComponent, Object message, String title, int optionType,
			int messageType, Icon icon) throws HeadlessException {
		return showOptionDialog(parentComponent, message, title, optionType, messageType, icon, null, null);
	}

	public static int showOptionDialog(Component parentComponent, Object message, String title, int optionType,
			int messageType, Icon icon, Object[] options, Object initialValue) throws HeadlessException {
		JecnOptionPane pane = new JecnOptionPane(message, messageType, optionType, icon, options, initialValue);

		pane.setInitialValue(initialValue);
		pane.setComponentOrientation(((parentComponent == null) ? getJecnRootFrame() : parentComponent)
				.getComponentOrientation());

		int style = styleFromMessageType(messageType);
		JecnDialog dialog = pane.createDialog(parentComponent, title, style);

		pane.selectInitialValue();
		dialog.setVisible(true);
		dialog.dispose();

		Object selectedValue = pane.getValue();

		if (selectedValue == null)
			return CLOSED_OPTION;
		if (options == null) {
			if (selectedValue instanceof Integer)
				return ((Integer) selectedValue).intValue();
			return CLOSED_OPTION;
		}
		for (int counter = 0, maxCounter = options.length; counter < maxCounter; counter++) {
			if (options[counter].equals(selectedValue))
				return counter;
		}
		return CLOSED_OPTION;
	}

	private static int styleFromMessageType(int messageType) {
		switch (messageType) {
		case ERROR_MESSAGE:
			return JRootPane.ERROR_DIALOG;
		case QUESTION_MESSAGE:
			return JRootPane.QUESTION_DIALOG;
		case WARNING_MESSAGE:
			return JRootPane.WARNING_DIALOG;
		case INFORMATION_MESSAGE:
			return JRootPane.INFORMATION_DIALOG;
		case PLAIN_MESSAGE:
		default:
			return JRootPane.PLAIN_DIALOG;
		}
	}

	private JecnDialog createDialog(Component parentComponent, String title, int style) throws HeadlessException {
		// 新标题
		String newTitle = null;
		if (DrawCommon.isNullOrEmtryTrim(title)) {
			newTitle = JecnResourceUtil.getJecnResourceUtil().getValue("dialogDefaultTitle");
		} else {
			newTitle = title;
		}

		final JecnDialog dialog;

		Window window = getWindowForComp(parentComponent);
		if (window instanceof Frame) {
			dialog = new JecnDialog((Frame) window, newTitle, true);
		} else {
			dialog = new JecnDialog((Dialog) window, newTitle, true);
		}
		// 隐藏对话框的右上角按钮
		dialog.getToolTitlePanel().hiddBtns();

		initDialog(dialog, style, parentComponent);
		return dialog;
	}

	private Window getWindowForComp(Component parentComponent) {
		if (parentComponent == null)
			return getJecnRootFrame();
		if (parentComponent instanceof Frame || parentComponent instanceof Dialog)
			return (Window) parentComponent;
		return getWindowForComp(parentComponent.getParent());
	}

	private void initDialog(final JecnDialog dialog, int style, Component parentComponent) {
		dialog.setComponentOrientation(this.getComponentOrientation());
		Container contentPane = dialog.getContentPane();

		// 修改颜色选择器背景颜色
		JecnUIUtil.setColorToSubComp(this);

		contentPane.add(this, BorderLayout.CENTER);
		dialog.setResizable(false);
		if (JecnDialog.isDefaultLookAndFeelDecorated()) {
			boolean supportsWindowDecorations = UIManager.getLookAndFeel().getSupportsWindowDecorations();
			if (supportsWindowDecorations) {
				dialog.setUndecorated(true);
				getRootPane().setWindowDecorationStyle(style);
			}
		}
		dialog.pack();
		dialog.setLocationRelativeTo(parentComponent);
		WindowAdapter adapter = new WindowAdapter() {
			private boolean gotFocus = false;

			public void windowClosing(WindowEvent we) {
				setValue(null);
			}

			public void windowGainedFocus(WindowEvent we) {
				// Once window gets focus, set initial focus
				if (!gotFocus) {
					selectInitialValue();
					gotFocus = true;
				}
			}
		};
		dialog.addWindowListener(adapter);
		dialog.addWindowFocusListener(adapter);
		dialog.addComponentListener(new ComponentAdapter() {
			public void componentShown(ComponentEvent ce) {
				// reset value to ensure closing works properly
				setValue(JOptionPane.UNINITIALIZED_VALUE);
			}
		});
		addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent event) {
				// Let the defaultCloseOperation handle the closing
				// if the user closed the window without selecting a button
				// (newValue = null in that case). Otherwise, close the dialog.
				if (dialog.isVisible() && event.getSource() == JecnOptionPane.this
						&& (event.getPropertyName().equals(VALUE_PROPERTY)) && event.getNewValue() != null
						&& event.getNewValue() != JOptionPane.UNINITIALIZED_VALUE) {
					dialog.setVisible(false);
				}
			}
		});
	}

	/**
	 * Returns the specified component's toplevel <code>Frame</code> or
	 * <code>Dialog</code>.
	 * 
	 * @param parentComponent
	 *            the <code>Component</code> to check for a <code>Frame</code>
	 *            or <code>Dialog</code>
	 * @return the <code>Frame</code> or <code>Dialog</code> that contains the
	 *         component, or the default frame if the component is
	 *         <code>null</code>, or does not have a valid <code>Frame</code> or
	 *         <code>Dialog</code> parent
	 * @exception HeadlessException
	 *                if <code>GraphicsEnvironment.isHeadless</code> returns
	 *                <code>true</code>
	 * @see java.awt.GraphicsEnvironment#isHeadless
	 */
	public static Window getWindowForComponent(Component parentComponent) throws HeadlessException {
		if (parentComponent == null)
			return getJecnRootFrame();
		if (parentComponent instanceof Frame || parentComponent instanceof Dialog)
			return (Window) parentComponent;
		return JecnOptionPane.getWindowForComponent(parentComponent.getParent());
	}

	/**
	 * 
	 * 获取跟窗体
	 * 
	 * @return
	 */
	public static Frame getJecnRootFrame() {
		JFrame tmpFrame = JecnSystemStaticData.getFrame();
		return (tmpFrame == null) ? getRootFrame() : tmpFrame;
	}
}
