package com.jecn.epros.server.service.task.app.impl;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.external.TaskParam;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.task.JecnTaskApplicationNew;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;
import com.jecn.epros.server.bean.task.temp.JecnTaskFileTemporary;
import com.jecn.epros.server.bean.task.temp.SimpleSubmitMessage;
import com.jecn.epros.server.bean.task.temp.SimpleTaskBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;

/**
 * 流程任务业务处理类
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 9, 2012 时间：4:00:32 PM
 */
@Transactional(rollbackFor = Exception.class)
public class JecnFlowTaskServiceImpl extends JecnCommonTaskService implements IJecnBaseTaskService {

	/**
	 * 创建:流程任务、文件、制度任务
	 * 
	 * @param tempCreateTaskBean
	 *            创建任务信息
	 * @throws BsException
	 */
	@Override
	public void createPRFTask(JecnTempCreateTaskBean tempCreateTaskBean) throws Exception {
		if (tempCreateTaskBean == null || tempCreateTaskBean.getJecnTaskBeanNew() == null) {
			return;
		}
		// 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
		int taskType = tempCreateTaskBean.getJecnTaskBeanNew().getTaskType();
		switch (taskType) {
		case 0:// 流程图任务
		case 4:// 流程地图任务
			createFlowTask(tempCreateTaskBean);
			break;
		case 1:
			createFileTask(tempCreateTaskBean);
			break;
		case 2:
		case 3:
			createRuleTask(tempCreateTaskBean);
			break;
		default:
			break;
		}
	}

	/**
	 * 获取流程信息
	 * 
	 * @param flowId
	 * @param simpleTaskBean
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public void getFlowInfomation(SimpleTaskBean simpleTaskBean) throws Exception {
		JecnTaskBeanNew taskBeanNew = simpleTaskBean.getJecnTaskBeanNew();
		if (taskBeanNew == null) {//
			log.error("JecnFlowTaskServiceImpl类getFlowInfomation方法中参数为空!");
			// 访问出错
			throw new IllegalArgumentException(
					"getFlowInfomation(SimpleTaskBean simpleTaskBean) +  taskBeanNew == null");
		}
		Long flowId = taskBeanNew.getRid();
		// 流程属性信息
		JecnFlowBasicInfoT jecnFlowBasicInfoT = null;
		// 流程地图属性信息
		JecnMainFlowT mainFlowT = null;
		// 获取流程信息
		JecnFlowStructureT jecnFlowStructureT = structureDao.get(flowId);
		if (taskBeanNew.getTaskType() == 0) {// 流程任务
			// 流程属性信息
			jecnFlowBasicInfoT = basicInfoDao.get(flowId);
		} else if (taskBeanNew.getTaskType() == 4) {
			// 流程地图属性信息
			mainFlowT = (JecnMainFlowT) structureDao.getSession().get(JecnMainFlowT.class, flowId);
		}

		// PRF类别
		setProceeRuleTypeBean(simpleTaskBean);
		// 流程不存在
		if (jecnFlowStructureT == null) {
			log.error("JecnFlowTaskServiceImpl类获取流程信息参数不能为空! jecnFlowStructureT == null");
			throw new IllegalArgumentException("JecnFlowTaskServiceImpl类获取流程信息参数不能为空! jecnFlowStructureT == null");
		}
		// 流程名称
		simpleTaskBean.setPrfName(jecnFlowStructureT.getFlowName());
		// 流程编号
		simpleTaskBean.setPrfNumber(jecnFlowStructureT.getFlowIdInput());
		// 获取流程地图信息
		if (jecnFlowStructureT.getPerFlowId() != null) {
			JecnFlowStructureT perJecnFlowStructureT = structureDao.get(jecnFlowStructureT.getPerFlowId());
			if (perJecnFlowStructureT != null) {
				simpleTaskBean.setPrfPreName(perJecnFlowStructureT.getFlowName());// 业务域名称
			}
		}
		// 是否存在错误信息
		if (jecnFlowStructureT.getIsFlow() == 1 && jecnFlowStructureT.getErrorState() == 0) {// 流程验证错误信息
			// 0:错误
			// 0:错误
			simpleTaskBean.setErrorState(jecnFlowStructureT.getErrorState());
		} else {// 1:正确
			simpleTaskBean.setErrorState(1);
		}
		// 0是密级，1是公开
		if (jecnFlowStructureT.getIsPublic() != null) {
			simpleTaskBean.setStrPublic(jecnFlowStructureT.getIsPublic().toString());
		}
		// 术语定义 页面显示
		String notGlossary = null;
		if (jecnFlowBasicInfoT != null) {
			notGlossary = jecnFlowBasicInfoT.getNoutGlossary();
			// 获取流程类别
			if (simpleTaskBean.getJecnTaskApplicationNew().getFileType() != 0 && jecnFlowBasicInfoT.getTypeId() != null) {// 显示文件类别
				simpleTaskBean.setPrfType(jecnFlowBasicInfoT.getTypeId().toString());
				// 获取文件类别名称
				simpleTaskBean.setPrfTypeName(getTypeName(jecnFlowBasicInfoT.getTypeId(), simpleTaskBean
						.getListTypeBean()));
			}
		} else if (mainFlowT != null) {
			notGlossary = mainFlowT.getFlowNounDefine();
		}
		if (!JecnCommon.isNullOrEmtryTrim(notGlossary)) {
			// 术语定义 页面显示
			simpleTaskBean.setDefinitionStr(JecnTaskCommon.replaceAll(notGlossary));
		}
	}

	/**
	 * 流程审批变动
	 * 
	 * @param submitMessage
	 *            提交任务信息
	 * @param taskBeanNew
	 *            任务主表
	 * @throws Exception
	 */
	public String updateAssembledFlowTaskFixed(SimpleSubmitMessage submitMessage, JecnTaskBeanNew taskBeanNew)
			throws Exception {
		Long rid = taskBeanNew.getRid();
		// 任务的显示项
		JecnTaskApplicationNew jecnTaskApplicationNew = abstractTaskDao.getJecnTaskApplicationNew(taskBeanNew.getId());
		// 获取流程信息
		JecnFlowStructureT jecnFlowStructureT = structureDao.get(rid);
		JecnFlowBasicInfoT jecnFlowBasicInfoT = basicInfoDao.get(rid);
		// 文件类型
		Long typeId = null;
		if (jecnTaskApplicationNew.getFileType() != 0 && taskBeanNew.getTaskType() == 0) {
			// basicInfoDao.getSession().evict(jecnFlowBasicInfoT);
			typeId = jecnFlowBasicInfoT.getTypeId();
		}
		// 密级 0公开：1 ； 秘密
		Long isPublic = jecnFlowStructureT.getIsPublic();
		String taskFixedDesc = JecnTaskCommon.TASK_NOTHIN;
		// 文控审核和部门审核
		if (taskBeanNew.getUpState() == 1 || taskBeanNew.getUpState() == 2) {
			// 获取审批变动
			taskFixedDesc = assembledPrfTaskFixed(submitMessage, taskBeanNew, jecnTaskApplicationNew, typeId, isPublic);
		}
		if (taskFixedDesc.equals(JecnTaskCommon.TASK_NOTHIN)) {// 无变动
			return taskFixedDesc;
		}
		if (jecnTaskApplicationNew.getFileType() == 1 && taskBeanNew.getTaskType() == 0) {
			if (isFixed(typeId, submitMessage.getPrfType())) {// 存在变更
				// 更新类别
				Long fixTypeId = null;
				if (!JecnCommon.isNullOrEmtryTrim(submitMessage.getPrfType())) {
					fixTypeId = Long.valueOf(submitMessage.getPrfType().trim());
				}
				jecnFlowBasicInfoT.setTypeId(fixTypeId);
				abstractTaskDao.getSession().update(jecnFlowBasicInfoT);
				abstractTaskDao.getSession().flush();
			}
		}
		if (!JecnCommon.isNullOrEmtryTrim(submitMessage.getStrPublic())) {// 更新密级
			Long isPublicStr = Long.valueOf(submitMessage.getStrPublic().trim());
			jecnFlowStructureT.setIsPublic(isPublicStr);
			abstractTaskDao.getSession().update(jecnFlowStructureT);
			abstractTaskDao.getSession().flush();
		}
		return taskFixedDesc;
	}

	/**
	 * true类别存在变更
	 * 
	 * @param typeId
	 * @param typeStr
	 * @return
	 */
	private boolean isFixed(Long typeId, String typeStr) {
		if (JecnCommon.isNullOrEmtryTrim(typeStr) && typeId != null) {
			return true;
		} else if (!JecnCommon.isNullOrEmtryTrim(typeStr) && typeId == null) {
			return true;
		} else if (!JecnCommon.isNullOrEmtryTrim(typeStr) && typeId != null) {
			if (!typeStr.trim().equals(typeId.toString())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 打开试用行报告
	 * 
	 * @param id
	 *            任务主键ID
	 * @return JecnTaskFileTemporary 试运行文件
	 * @throws Exception
	 */
	@Override
	public JecnTaskFileTemporary downLoadTaskRunFile(Long id) throws Exception {
		return super.downLoadTaskRunFile(id);
	}

	/**
	 * 获取任务信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param taskOperation
	 *            我的任务点击操作 0审批;1重新提交;2整理意见;3任务管理，编辑任务
	 * @return SimpleTaskBean 页面获取的任务相关信息
	 * @throws Exception
	 */
	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public SimpleTaskBean getSimpleTaskBeanById(Long taskId, int taskOperation) throws Exception {
		return super.getSimpleTaskBeanById(taskId, taskOperation);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public boolean isApprovePeopleChecked(Long peopleId, Long taskId, int taskElseState, int approveCounts)
			throws Exception {
		return super.isApprovePeopleChecked(peopleId, taskId, taskElseState, approveCounts);
	}

	/**
	 * 审批通过
	 * 
	 * @param submitMessage
	 *            审核人提交信息
	 * 
	 */

	@Override
	public void taskOperation(SimpleSubmitMessage submitMessage) throws Exception {
		super.taskOperation(submitMessage);
	}

	/**
	 * 当前任务，当前操作人是否已审批任务
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param curPeopleId
	 *            当前操作人主键ID
	 * @return true 当前操作人已审批，或无审批权限
	 * @throws Exception
	 */
	@Override
	public boolean hasApproveTask(Long taskId, Long curPeopleId, int taskOperation) throws Exception {
		return super.hasApproveTask(taskId, curPeopleId, taskOperation);
	}

	/**
	 * 检测任务是否搁置
	 * 
	 * @param simpleTaskBean
	 *            任务信息
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public SimpleTaskBean checkTaskIsDelay(SimpleTaskBean simpleTaskBean) throws Exception {

		return super.checkTaskIsDelay(simpleTaskBean);
	}

	@Override
	public void releaseTaskByWebService(Long taskId, List<JecnTaskHistoryFollow> taskHistorys) throws Exception {
		super.releaseTaskByWebService(taskId, taskHistorys);
	}
	
	@Override
	public MessageResult handleTaskExternal(TaskParam param) {
		return super.handleTaskExternal(param);
	}

}
