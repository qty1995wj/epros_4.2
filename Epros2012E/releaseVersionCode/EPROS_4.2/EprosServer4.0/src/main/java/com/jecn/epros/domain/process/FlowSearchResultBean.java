package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.util.ConfigUtils;

import java.util.Date;

public class FlowSearchResultBean {
    private Long flowId;
    private String flowName;
    private String flowNum;
    private Long orgId;
    private String orgName;
    /**
     * 流程责任人
     **/
    private String resPeople;
    private int isPublic;
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    private Date pubTime;

    public LinkResource getLink() {
        return new LinkResource(flowId, flowName, ResourceType.PROCESS,TreeNodeUtils.NodeType.search);
    }

    public LinkResource getOrgLink() {
        return new LinkResource(orgId, orgName, ResourceType.ORGANIZATION);
    }

    public String getPublicStr(){
        return ConfigUtils.getSecretByType(isPublic);
    }

    @JsonIgnore
    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    @JsonIgnore
    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getFlowNum() {
        return flowNum;
    }

    public void setFlowNum(String flowNum) {
        this.flowNum = flowNum;
    }

    @JsonIgnore
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    @JsonIgnore
    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getResPeople() {
        return resPeople;
    }

    public void setResPeople(String resPeople) {
        this.resPeople = resPeople;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }
}
