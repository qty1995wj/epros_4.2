package com.jecn.epros.server.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.util.JecnCommonSqlConstant;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public class JecnCommonSql {

	public static int pageSize = 20;

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：获得默认角色（sql 浏览端）
	 * @return
	 */
	public static String getDefaultAuthString() {
		return "('admin','design','isDesignFileAdmin','isDesignProcessAdmin','isDesignRuleAdmin','viewAdmin')";
	}

	/**
	 * 登录验证获取默认角色权限节点
	 * 
	 * @return
	 */
	public static String getLoginDefaultAuthString() {
		if (JecnCommon.isShowSecondAdmin()) {
			return "('admin','secondAdmin','design','isDesignFileAdmin','isDesignProcessAdmin','isDesignRuleAdmin','viewAdmin')";
		} else {
			return getDefaultAuthString();
		}
	}

	public static String getSecondAdminDefaultAuthString() {
		return "('design','isDesignFileAdmin','isDesignProcessAdmin','isDesignRuleAdmin')";
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：获得设计的默认角色（sql 浏览端）
	 * @return
	 */
	public static String getDesignDefaultAuthString() {
		return "('admin','design')";
	}

	public static String getDesignAuthString() {
		return "('admin','design','secondAdmin')";
	}

	public static String getAdminAuthString() {
		return "('admin','secondAdmin')";
	}

	/**
	 * @author zhangchen Nov 2, 2012
	 * @description：是否是流程管理员
	 * @param roleType
	 * @return
	 */
	public static boolean isAdmin(String roleType) {
		if ("admin".equals(roleType)) {
			return true;
		}
		return false;
	}

	/**
	 * 查询人员在项目下的岗位集合sql
	 * 
	 * @param peopleId
	 * @param projectId
	 * @return
	 */
	public static String findPosSql(Long peopleId, Long projectId) {
		String sql = "select jupr.figure_id from jecn_user_position_related jupr,jecn_flow_org_image pos,jecn_flow_org org"
				+ "       where jupr.figure_id= pos.figure_id and pos.org_id=org.org_id and org.del_state=0 and org.projectid="
				+ projectId + "       and jupr.people_id=" + peopleId;
		return sql;
	}

	/**
	 * 查询人员在项目下的岗位集合sql
	 * 
	 * @param peopleId
	 * @param projectId
	 * @return
	 */
	public static String findOrgSql(Long peopleId, Long projectId) {
		String sql = "SELECT DISTINCT ORG.ORG_ID" + "  FROM JECN_FLOW_ORG ORG"
				+ " WHERE ORG.DEL_STATE = 0 AND ORG.PROJECTID = " + projectId + "START WITH ORG.ORG_ID IN"
				+ "            (SELECT POS.ORG_ID" + "               FROM JECN_USER_POSITION_RELATED JUPR,"
				+ "                    JECN_FLOW_ORG_IMAGE        POS" + "              WHERE JUPR.PEOPLE_ID = "
				+ peopleId + "                AND JUPR.FIGURE_ID = POS.FIGURE_ID)"
				+ "CONNECT BY PRIOR ORG.PER_ORG_ID = ORG.ORG_ID";
		return sql;
	}

	/**
	 * 管理员权限 字符串
	 * 
	 * @return
	 */
	public static String getStrAdmin() {
		return "admin";
	}

	/**
	 * 浏览管理员权限 字符串
	 * 
	 * @return
	 */
	public static String getStrViewAdmin() {
		return "viewAdmin";
	}

	/**
	 * 下载管理员权限 字符串
	 * 
	 * @return
	 */
	public static String getStrDownLoadAdmin() {
		return "downLoadAdmin";
	}

	public static String getStrSecondAdmin() {
		return "secondAdmin";
	}

	public static String getStrDesignAdmin() {
		return "design";
	}

	/**
	 * @author zhangchen Nov 2, 2012
	 * @description：是否是流程管理员
	 * @param roleType
	 * @return
	 */
	public static boolean isDesign(String roleType) {
		if ("design".equals(roleType)) {
			return true;
		}
		return false;
	}

	public static boolean isSecondAdmin(String roleType) {
		if ("secondAdmin".equals(roleType)) {
			return true;
		}
		return false;
	}

	/**
	 * 活动(sql)
	 * 
	 * @return
	 */
	public static String getActiveString() {
		return "('Rhombus','ITRhombus','DataImage','RoundRectWithLine','ActiveFigureAR','ExpertRhombusAR')";
	}

	/**
	 * 操作说明-活动
	 * 
	 * @return
	 */
	public static String getOperationActiveString() {
		return "('Rhombus','ITRhombus','DataImage','RoundRectWithLine','ActiveFigureAR','ExpertRhombusAR','ActivityImplFigure','ActivityOvalSubFlowFigure')";
	}

	/**
	 * 角色(sql)
	 * 
	 * @return
	 */
	public static String getRoleString() {
		return "('RoleFigure','CustomFigure','VirtualRoleFigure','RoleFigureAR')";
	}

	/**
	 * 角色(sql)
	 * 
	 * @return
	 */
	public static String getNoCoustomRoleString() {
		return "('RoleFigure','VirtualRoleFigure','RoleFigureAR')";
	}

	/**
	 * 角色(sql)
	 * 
	 * @return
	 */
	public static String getOnlyRoleString() {
		return "('RoleFigure','RoleFigureAR')";
	}

	/**
	 * 连接线（sql）
	 * 
	 * @return
	 */
	public static String getLineString() {// sql查询用到的线
		return "('ManhattanLine','CommonLine')";
	}

	/**
	 * 接口（sql）
	 * 
	 * @return
	 */
	public static String getImplString() {// sql查询用到的线
		return "('OvalSubFlowFigure','ImplFigure','ActivityImplFigure','ActivityOvalSubFlowFigure','InterfaceRightHexagon')";
	}

	/**
	 * 设置连接元素集合
	 * 
	 * @return
	 */
	public static String getLinkProcessString() {
		return "('Oval','Triangle', 'FunctionField', 'Pentagon','RightHexagon', 'ModelFigure',"
				+ " 'InputHandFigure','OvalSubFlowFigure','ImplFigure','ActivityImplFigure','ActivityOvalSubFlowFigure'"
				+ ",'IsoscelesTrapezoidFigure','MapRhombus')";
	}

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：制度（sql）
	 * @return
	 */
	public static String getSystemString() {
		return "'SystemFigure'";
	}

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：制度（sql）
	 * @return
	 */
	public static String getIconString() {
		return "'IconFigure'";
	}

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：岗位
	 * @return
	 */
	public static String getPosString() {
		return "'PositionFigure'";
	}

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：组织
	 * @return
	 */
	public static String getOrgString() {
		return "'Rect'";
	}

	public static String getTermFigureAR() {
		return "'TermFigureAR'";
	}

	/**
	 * SQLSERVER：sqlserver 数据库，ORACLE：oracle数据库
	 * 
	 * @author wanglikai
	 * 
	 */
	public enum DBType {
		SQLSERVER, ORACLE, MYSQL, NONE
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：通过list封装in
	 * @param list
	 * @return
	 */
	public static String getIds(List<? extends Number> list) {

		StringBuffer buf = new StringBuffer();
		buf.append(" (");
		for (int i = 0; i < list.size(); i++) {
			if (i == 0) {
				buf.append(list.get(i));
			} else {
				buf.append("," + list.get(i));
			}
		}
		buf.append(")");
		return buf.toString();
	}

	/**
	 * 通过list封装in
	 * 
	 * @param list
	 * @return
	 */
	public static String getStrs(List<String> list) {
		StringBuffer buf = new StringBuffer();
		buf.append(" (");
		for (int i = 0; i < list.size(); i++) {
			if (i == 0) {
				buf.append("'" + list.get(i) + "'");
			} else {
				buf.append("," + "'" + list.get(i) + "'");
			}
		}
		buf.append(")");
		return buf.toString();
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：通过list封装sql集合
	 * @param sql
	 * @param list
	 * @return
	 */
	public static List<String> getListSqls(String sql, Collection<Long> list) {
		// 计数
		List<String> listResult = new ArrayList<String>();
		List<Long> listTmp = new ArrayList<Long>();

		/**
		 * 整数倍的查询
		 */
		int countRow = 0;
		for (Long id : list) {
			countRow++;
			listTmp.add(id);
			if (countRow % 100 == 0) {
				// 每100条执行一次查询操作
				listResult.add(sql + getIds(listTmp));
				listTmp.clear();
			}
		}

		/**
		 * 余数的查询
		 */
		if (listTmp.size() > 0) {
			listResult.add(sql + getIds(listTmp));
		}
		return listResult;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：通过list封装sql集合
	 * @param sql
	 * @param list
	 * @return
	 */
	public static List<String> getListSqls(String sql, List<Long> list) {
		// 计数
		List<String> listResult = new ArrayList<String>();
		List<Long> listTmp = new ArrayList<Long>();

		/**
		 * 整数倍的查询
		 */
		int countRow = 0;
		for (Long id : list) {
			countRow++;
			listTmp.add(id);
			if (countRow % 100 == 0) {
				// 每100条执行一次查询操作
				listResult.add(sql + getIds(listTmp));
				listTmp.clear();
			}
		}

		/**
		 * 余数的查询
		 */
		if (listTmp.size() > 0) {
			listResult.add(sql + getIds(listTmp));
		}
		return listResult;
	}

	/**
	 * 
	 * @author zhangchen Jul 2, 2012
	 * @description：通过set封装sql集合（最后面添加一个括号）
	 * @param sql
	 * @param set
	 * @return
	 */
	public static List<String> getListSqlAddBracket(String sql, List<Long> list) {
		// 计数

		List<String> listResult = new ArrayList<String>();
		List<Long> listTmp = new ArrayList<Long>();

		/**
		 * 整数倍的查询
		 */
		int countRow = 0;
		for (Long id : list) {
			countRow++;
			listTmp.add(id);
			if (countRow % 100 == 0) {
				// 每100条执行一次查询操作
				listResult.add(sql + getIds(listTmp) + ")");
				listTmp.clear();
			}
		}

		/**
		 * 余数的查询
		 */
		if (listTmp.size() > 0) {
			listResult.add(sql + getIds(listTmp) + ")");
		}
		return listResult;
	}

	/**
	 * 
	 * @author zhangchen Jul 2, 2012
	 * @description：通过set封装sql集合（最后面添加一个括号）
	 * @param sql
	 * @param set
	 * @return
	 */
	public static List<String> getSetSqlAddBracket(String sql, Set<Long> set) {
		// 计数

		List<String> listResult = new ArrayList<String>();
		List<Long> listTmp = new ArrayList<Long>();

		/**
		 * 整数倍的查询
		 */
		int countRow = 0;
		Iterator<Long> it = set.iterator();
		while (it.hasNext()) {
			Long id = it.next();
			countRow++;
			listTmp.add(id);
			if (countRow % 100 == 0) {
				// 每100条执行一次查询操作
				listResult.add(sql + getIds(listTmp) + ")");
				listTmp.clear();
			}
		}

		/**
		 * 余数的查询
		 */
		if (listTmp.size() > 0) {
			listResult.add(sql + getIds(listTmp) + ")");
		}
		return listResult;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：通过set封装sql集合
	 * @param sql
	 * @param set
	 * @return
	 */
	public static List<String> getSetSqls(String sql, Set<Long> set) {
		// 计数

		List<String> listResult = new ArrayList<String>();
		List<Long> listTmp = new ArrayList<Long>();

		/**
		 * 整数倍的查询
		 */
		int countRow = 0;
		Iterator<Long> it = set.iterator();
		while (it.hasNext()) {
			Long id = it.next();
			countRow++;
			listTmp.add(id);
			if (countRow % 100 == 0) {
				// 每100条执行一次查询操作
				listResult.add(sql + getIds(listTmp));
				listTmp.clear();
			}
		}

		/**
		 * 余数的查询
		 */
		if (listTmp.size() > 0) {
			listResult.add(sql + getIds(listTmp));
		}
		return listResult;
	}

	/**
	 * 通过Set封装in
	 * 
	 * @param set
	 * @return
	 */
	public static String getIdsSet(Set<Long> set) {
		StringBuffer buf = new StringBuffer();
		buf.append(" (");
		Iterator<Long> it = set.iterator();
		int count = 0;
		while (it.hasNext()) {
			Long flowId = it.next();
			if (count == 0) {
				buf.append(flowId);
			} else {
				buf.append("," + flowId);
			}
			count++;
		}
		buf.append(")");
		return buf.toString();
	}

	/**
	 * @author ZHANGXIAOHU
	 * @description：通过set封装sql集合
	 * @param sql
	 * @param set
	 * @return
	 */
	public static List<String> getStringSqlsBySet(String sql, Set<String> set) {
		// 计数

		List<String> listResult = new ArrayList<String>();
		List<String> listTmp = new ArrayList<String>();

		/**
		 * 整数倍的查询
		 */
		int countRow = 0;
		Iterator<String> it = set.iterator();
		while (it.hasNext()) {
			String id = it.next();
			countRow++;
			listTmp.add(id);
			if (countRow % 100 == 0) {
				// 每100条执行一次查询操作
				listResult.add(sql + getStrs(listTmp));
				listTmp.clear();
			}
		}

		/**
		 * 余数的查询
		 */
		if (listTmp.size() > 0) {
			listResult.add(sql + getStrs(listTmp));
		}
		return listResult;
	}

	/**
	 * 
	 * 将Clob转成String ,静态方法
	 * 
	 * @author fuzhh 2013-12-16
	 * @param clob
	 *            字段
	 * @return 内容字串，如果出现错误，返回 null
	 */
	public static String clobToString(Clob clob) {
		if (clob == null)
			return null;
		StringBuffer sb = new StringBuffer();
		Reader clobStream = null;
		try {
			clobStream = clob.getCharacterStream();
			char[] b = new char[60000];// 每次获取60K
			int i = 0;
			while ((i = clobStream.read(b)) != -1) {
				sb.append(b, 0, i);
			}
		} catch (Exception ex) {
			sb = null;
		} finally {
			try {
				if (clobStream != null) {
					clobStream.close();
				}
			} catch (Exception e) {
			}
		}
		if (sb == null)
			return null;
		else
			return sb.toString();
	}

	/**
	 * 获取系统管理员邮件信息
	 * 
	 * @return
	 */
	public static String getAdminEmailSql() {
		// 获取系统管理员sql
		return "SELECT JU.*" + "  FROM JECN_USER JU" + " INNER JOIN JECN_USER_ROLE JUR ON JU.PEOPLE_ID = JUR.PEOPLE_ID"
				+ " INNER JOIN JECN_ROLE_INFO JRI ON JUR.ROLE_ID = JRI.ROLE_ID" + " WHERE JRI.FILTER_ID = 'admin'";
	}

	public static String getUpperString(String str) {
		return "upper(" + str + ")";
	}

	public static String getUpperLikeString(String str) {
		return "upper('%" + str + "%')";
	}

	/**
	 * sqlserver 时间截取
	 * 
	 * @param colomn
	 * @return
	 */
	public static String getSqlServerCONVERT(String colomn) {
		if (!JecnCommon.isSQLServer()) {
			return colomn;
		}
		return "CONVERT(VARCHAR(100), " + colomn + ", 23) AS " + colomn;
	}

	/**
	 * sql 默认为空函数
	 * 
	 * @return
	 */
	public static String getSqlValueDefault() {
		if (JecnCommon.isSQLServer()) {
			return " isnull";
		} else if (JecnCommon.isOracle()) {
			return " NVL";
		}
		return " value";
	}

	/**
	 * 树形结构表，获取节点的子节点集合（删除）
	 * 
	 * @param tableName
	 *            表名称
	 * @param id
	 *            表主键ID
	 * @param pId
	 *            表父节点PID
	 * @param listIds
	 *            节点ID
	 * @return
	 */
	public static String getChildObjectsSqlMove(String tableName, String id, String pId, List<Long> listIds) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT C." + id + ",C." + pId + ",1 as perNumber,C.SORT_ID").append(" FROM ").append(tableName)
				.append(" C ");
		buffer.append(" INNER JOIN ").append(tableName).append(" P ON C.T_PATH LIKE ").append(getJoinFunc("P"));
		buffer.append(" WHERE P.").append(id).append(" IN ").append(getIds(listIds));
		buffer.append(" ORDER BY C.T_PATH");
		return buffer.toString();
	}

	/**
	 * 树形结构表，获取节点的子节点集合（删除）
	 * 
	 * @param tableName
	 *            表名称
	 * @param id
	 *            表主键ID
	 * @param pId
	 *            表父节点PID
	 * @param listIds
	 *            节点ID
	 * @return
	 */
	public static String getChildObjectsSqlMoveRisk(String tableName, String id, String pId, List<Long> listIds) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT C." + id + ",C." + pId + ",1 as perNumber,C.SORT").append(" FROM ").append(tableName)
				.append(" C ");
		buffer.append(" INNER JOIN ").append(tableName).append(" P ON C.T_PATH LIKE ").append(getJoinFunc("P"));
		buffer.append(" WHERE P.").append(id).append(" IN ").append(getIds(listIds));
		buffer.append(" ORDER BY C.T_PATH");
		return buffer.toString();
	}

	/**
	 * 树形结构表，获取节点的子节点集合（删除）
	 * 
	 * @param tableName
	 *            表名称
	 * @param id
	 *            表主键ID
	 * @param pId
	 *            表父节点PID
	 * @param listIds
	 *            节点ID
	 * @return
	 */
	public static String getOrgChildObjectsSqlMove(String tableName, String id, String pId, List<Long> listIds) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT C." + id + ",C." + pId + ",C.PREORG_NUMBER as perNumber,C.SORT_ID").append(" FROM ")
				.append(tableName).append(" C ");
		buffer.append(" INNER JOIN ").append(tableName).append(" P ON C.T_PATH LIKE ").append(getJoinFunc("P"));
		buffer.append(" WHERE P.").append(id).append(" IN").append(getIds(listIds));
		buffer.append(" ORDER BY C.T_PATH");
		return buffer.toString();
	}

	/**
	 * 通过类型获得所有子节点包含self
	 * 
	 * @param tableName
	 * @param id
	 * @param pId
	 * @param listIds
	 * @param moveNodeType
	 * @return
	 */
	public static String getChildObjectsSqlmoveByType(List<Long> listIds, TreeNodeType moveNodeType) {
		switch (moveNodeType) {
		case process:
		case processFile:
		case processMap:
		case processMapMode:
		case processMode:
		case processCodeRoot:
		case processRoot:
			return JecnCommonSql.getChildObjectsSqlMove("JECN_FLOW_STRUCTURE_T", "FLOW_ID", "PRE_FLOW_ID", listIds);
		case standardDir:
		case standard:
		case standardProcess:
		case standardProcessMap:
		case standardClause:
		case standardRoot:
			return JecnCommonSql.getChildObjectsSqlMove("JECN_CRITERION_CLASSES", "CRITERION_CLASS_ID",
					"PRE_CRITERION_CLASS_ID", listIds);
		case standardClauseRequire:
			return "SELECT T.CRITERION_CLASS_ID,T.PRE_CRITERION_CLASS_ID, 1 as perNumber,T.SORT_ID FROM JECN_CRITERION_CLASSES T WHERE T.CRITERION_CLASS_ID  in "
					+ JecnCommonSql.getIds(listIds);
		case organization:
			return JecnCommonSql.getOrgChildObjectsSqlMove("JECN_FLOW_ORG", "ORG_ID", "PER_ORG_ID", listIds);
		case position:
			return "SELECT T.FIGURE_ID,T.ORG_ID,T.ORG_NUMBER_ID,1 AS SORT_ID FROM JECN_FLOW_ORG_IMAGE T WHERE T.FIGURE_ID in "
					+ JecnCommonSql.getIds(listIds);
		case ruleDir:
			return JecnCommonSql.getChildObjectsSqlMove("JECN_RULE_T", "ID", "PER_ID", listIds);
		case ruleFile:
		case ruleModeFile:
			return "SELECT T.ID,T.Per_Id, 1 as perNumber,T.SORT_ID FROM JECN_RULE_T T WHERE T.ID  in"
					+ JecnCommonSql.getIds(listIds);
		case riskDir:
			return JecnCommonSql.getChildObjectsSqlMoveRisk("JECN_RISK", "ID", "PARENT_ID", listIds);
		case riskPoint:
			return "SELECT T.ID,T.PARENT_ID, 1 as perNumber,T.SORT FROM JECN_RISK T WHERE T.ID  in "
					+ JecnCommonSql.getIds(listIds);
		case fileDir:
			return JecnCommonSql.getChildObjectsSqlMove("JECN_FILE_T", "FILE_ID", "PER_FILE_ID", listIds);
		case file:
			return "SELECT T.FILE_ID,T.Per_File_Id, 1 as perNumber,T.SORT_ID FROM JECN_FILE_T T WHERE T.FILE_ID  in "
					+ JecnCommonSql.getIds(listIds);
		case positionGroupDir:
			return JecnCommonSql.getChildObjectsSqlMove("JECN_POSITION_GROUP", "ID", "PER_ID", listIds);
		case positionGroup:
			return "SELECT id,per_id,1 as perNumber,SORT_ID FROM JECN_POSITION_GROUP WHERE ID in"
					+ JecnCommonSql.getIds(listIds);
		case termDefineDir:
			return JecnCommonSql.getChildObjectsSqlMove("TERM_DEFINITION_LIBRARY", "ID", "PARENT_ID", listIds);
		case termDefine:
			return "SELECT id,PARENT_ID,1 as perNumber,SORT_ID FROM TERM_DEFINITION_LIBRARY WHERE ID in"
					+ JecnCommonSql.getIds(listIds);
		default:
			return "";
		}
	}

	/**
	 * 通过类型获得所有子节点包含self
	 * 
	 * @param tableName
	 * @param id
	 * @param pId
	 * @param listIds
	 * @param moveNodeType
	 * @return
	 */
	public static String getAllObjectsByType(TreeNodeType moveNodeType) {
		switch (moveNodeType) {
		case process:
		case processFile:
		case processMap:
		case processMapMode:
		case processMode:
		case processCodeRoot:
		case processRoot:
			return JecnCommonSql.getObjectsSqlMove("JECN_FLOW_STRUCTURE_T", "FLOW_ID", "PRE_FLOW_ID");
		case standardDir:
		case standard:
		case standardProcess:
		case standardProcessMap:
		case standardClause:
		case standardRoot:
		case standardClauseRequire:
			return JecnCommonSql.getObjectsSqlMove("JECN_CRITERION_CLASSES", "CRITERION_CLASS_ID",
					"PRE_CRITERION_CLASS_ID");
		case organization:
		case position:
		case organizationRoot:
			return JecnCommonSql.getObjectsSqlMove("JECN_FLOW_ORG", "ORG_ID", "PER_ORG_ID");
		case ruleDir:
		case ruleFile:
		case ruleModeFile:
		case ruleRoot:
			return JecnCommonSql.getObjectsSqlMove("JECN_RULE_T", "ID", "PER_ID");
		case riskDir:
		case riskPoint:
		case riskRoot:
			return JecnCommonSql.getObjectsSqlMove("JECN_RISK", "ID", "PARENT_ID");
		case fileDir:
		case file:
		case fileRoot:
			return JecnCommonSql.getObjectsSqlMove("JECN_FILE_T", "FILE_ID", "PER_FILE_ID");
		case positionGroupDir:
		case positionGroup:
		case positionGroupRoot:
			return JecnCommonSql.getObjectsSqlMove("JECN_POSITION_GROUP", "ID", "PER_ID");
		case termDefineDir:
		case termDefine:
		case termDefineRoot:
			return JecnCommonSql.getObjectsSqlMove("TERM_DEFINITION_LIBRARY", "ID", "PARENT_ID");
		case toolRoot:
		case tool:
			return JecnCommonSql.getObjectsSqlMove("JECN_FLOW_SUSTAIN_TOOL", "flow_sustain_tool_id",
					"PRE_FLOW_SUSTAIN_TOOL_ID");
		case innerControlRoot:
		case innerControlClause:
		case innerControlDir:
			return JecnCommonSql.getInnerSqlMove("JECN_CONTROL_GUIDE", "id",
					"parent_id");
		default:
			return "";
		}

	}

	private static String getInnerSqlMove(String tableName, String id, String pId) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT C." + id + ",C." + pId + ",1 as perNumber,C.SORT").append(" FROM ").append(tableName)
				.append(" C ");
		buffer.append(" ORDER BY C.T_PATH ASC");
		return buffer.toString();
	}

	private static String getObjectsSqlMove(String tableName, String id, String pId) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT C." + id + ",C." + pId + ",1 as perNumber,C.SORT_ID").append(" FROM ").append(tableName)
				.append(" C ");
		buffer.append(" ORDER BY C.T_PATH ASC");
		return buffer.toString();
	}

	public static String updateMoveCommon(String tableName, String pIdStr, Long pId, String idStr, Long id,
			String updateTimeStr, String updatePeopleIdStr, Long updatePeopleId, String t_path, int level,
			String perNumberStr, String perNumber, TreeNodeType moveNodeType, int isHide, String viewSort) {

		String sql = "UPDATE " + tableName + " SET " + pIdStr + "=" + pId;
		if (!"JECN_FLOW_ORG_IMAGE".equalsIgnoreCase(tableName)) {
			sql += ",VIEW_SORT=" + viewSort;
		}

		if (moveNodeType == TreeNodeType.organization || moveNodeType == TreeNodeType.position) {
			sql += "," + perNumberStr + "='" + perNumber + "'";
		}
		if (moveNodeType == TreeNodeType.file || moveNodeType == TreeNodeType.fileDir) {
			sql += ",HIDE=" + isHide;
		}
		if (moveNodeType != TreeNodeType.position) {
			sql += ",t_path='" + t_path + "',t_level=" + level;
		}
		sql += " where " + idStr + "=" + id;
		return sql;
	}

	public static String updateViewSortCommon(String tableName, String idStr, Long id, TreeNodeType moveNodeType,
			String viewSort) {
		String sql = "UPDATE " + tableName + " SET VIEW_SORT=" + viewSort + " where " + idStr + "=" + id;
		return sql;
	}

	private static String getSqlDate() {
		if (JecnContants.dbType == DBType.ORACLE) {
			return "=sysdate,";
		} else if (JecnContants.dbType == DBType.SQLSERVER) {
			return "=getdate(),";
		}
		return "";
	}

	/**
	 * 通过类型获得移动节点更新SQL
	 * 
	 * @param tableName
	 * @param id
	 * @param pId
	 * @param listIds
	 * @param moveNodeType
	 * @param isHide
	 *            : 1显示
	 * @param viewSort
	 * @return
	 */
	public static List<String> getUpdateMoveNodeSql(Long pId, String perNumber, Long id, String t_path, int level,
			Date updateTime, Long updatePeopleId, TreeNodeType moveNodeType, int isHide, String viewSort) {
		List<String> listStr = new ArrayList<String>();
		switch (moveNodeType) {
		case process:
		case processFile:
		case processMap:
		case processMapMode:
		case processMode:
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_FLOW_STRUCTURE_T", "PRE_FLOW_ID", pId, "FLOW_ID", id,
					null, "UPDATE_PEOPLE_ID", updatePeopleId, t_path, level, "", "", moveNodeType, isHide, viewSort));
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_FLOW_STRUCTURE", "PRE_FLOW_ID", pId, "FLOW_ID", id, null,
					"UPDATE_PEOPLE_ID", updatePeopleId, t_path, level, "", "", moveNodeType, isHide, viewSort));
			break;
		case standardDir:
		case standard:
		case standardProcess:
		case standardProcessMap:
		case standardClause:
		case standardClauseRequire:
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_CRITERION_CLASSES", "PRE_CRITERION_CLASS_ID", pId,
					"CRITERION_CLASS_ID", id, null, "UPDATE_PEOPLE_ID", updatePeopleId, t_path, level, "", "",
					moveNodeType, isHide, viewSort));
			break;
		case organization:
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_FLOW_ORG", "PER_ORG_ID", pId, "ORG_ID", id, "", "",
					updatePeopleId, t_path, level, "PREORG_NUMBER", perNumber, moveNodeType, isHide, viewSort));
			break;
		case position:
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_FLOW_ORG_IMAGE", "ORG_ID", pId, "FIGURE_ID", id, "", "",
					updatePeopleId, t_path, level, "ORG_NUMBER_ID", perNumber, moveNodeType, isHide, viewSort));
			break;
		case ruleDir:
		case ruleFile:
		case ruleModeFile:
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_RULE_T", "PER_ID", pId, "id", id, null,
					"UPDATE_PEOPLE_ID", updatePeopleId, t_path, level, "", "", moveNodeType, isHide, viewSort));
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_RULE", "PER_ID", pId, "id", id, null, "UPDATE_PEOPLE_ID",
					updatePeopleId, t_path, level, "", "", moveNodeType, isHide, viewSort));
			break;
		case riskDir:
		case riskPoint:
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_RISK", "PARENT_ID", pId, "id", id, null, "UPDATE_PERSON",
					updatePeopleId, t_path, level, "", "", moveNodeType, isHide, viewSort));
			break;
		case fileDir:
		case file:
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_FILE_T", "PER_FILE_ID", pId, "FILE_ID", id, null,
					"UPDATE_PERSON_ID", updatePeopleId, t_path, level, "", "", moveNodeType, isHide, viewSort));
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_FILE", "PER_FILE_ID", pId, "FILE_ID", id, null,
					"UPDATE_PERSON_ID", updatePeopleId, t_path, level, "", "", moveNodeType, isHide, viewSort));
			break;
		case positionGroupDir:
		case positionGroup:
			listStr.add(JecnCommonSql.updateMoveCommon("JECN_POSITION_GROUP", "PER_ID", pId, "id", id, null,
					"UPDATE_PERSON_ID", updatePeopleId, t_path, level, "", "", moveNodeType, isHide, viewSort));
			break;
		case termDefineDir:
		case termDefine:
			listStr.add(JecnCommonSql.updateMoveCommon("TERM_DEFINITION_LIBRARY", "PARENT_ID", pId, "id", id, null,
					"UPDATE_PERSON_ID", updatePeopleId, t_path, level, "", "", moveNodeType, isHide, viewSort));
			break;
		default:
			break;
		}
		return listStr;
	}

	public static List<String> getUpdateViewSortSql(Long id, TreeNodeType moveNodeType, String viewSort) {
		List<String> listStr = new ArrayList<String>();
		switch (moveNodeType) {
		case process:
		case processFile:
		case processMap:
		case processMapMode:
		case processMode:
		case processRoot:
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_FLOW_STRUCTURE_T", "FLOW_ID", id, moveNodeType,
					viewSort));
			listStr.add(JecnCommonSql
					.updateViewSortCommon("JECN_FLOW_STRUCTURE", "FLOW_ID", id, moveNodeType, viewSort));
			break;
		case standardDir:
		case standard:
		case standardProcess:
		case standardProcessMap:
		case standardClause:
		case standardClauseRequire:
		case standardRoot:
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_CRITERION_CLASSES", "CRITERION_CLASS_ID", id,
					moveNodeType, viewSort));
			break;
		case organization:
		case organizationRoot:
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_FLOW_ORG", "ORG_ID", id, moveNodeType, viewSort));
			break;
		case position:
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_FLOW_ORG_IMAGE", "FIGURE_ID", id, moveNodeType,
					viewSort));
			break;
		case ruleDir:
		case ruleFile:
		case ruleModeFile:
		case ruleRoot:
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_RULE_T", "id", id, moveNodeType, viewSort));
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_RULE", "PER_ID", id, moveNodeType, viewSort));
			break;
		case riskDir:
		case riskPoint:
		case riskRoot:
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_RISK", "id", id, moveNodeType, viewSort));
			break;
		case fileDir:
		case file:
		case fileRoot:
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_FILE_T", "FILE_ID", id, moveNodeType, viewSort));
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_FILE", "FILE_ID", id, moveNodeType, viewSort));
			break;
		case positionGroupDir:
		case positionGroup:
		case positionGroupRoot:
			listStr.add(JecnCommonSql.updateViewSortCommon("JECN_POSITION_GROUP", "id", id, moveNodeType, viewSort));
			break;
		case termDefineDir:
		case termDefine:
		case termDefineRoot:
			listStr
					.add(JecnCommonSql
							.updateViewSortCommon("TERM_DEFINITION_LIBRARY", "id", id, moveNodeType, viewSort));
			break;
		default:
			break;
		}
		return listStr;
	}

	/**
	 * 拼装like查询 字符串
	 * 
	 * @param alias
	 *            表别名
	 * @return String
	 */
	public static String getJoinFunc(String alias) {
		if (JecnContants.dbType.equals(DBType.ORACLE) || JecnContants.dbType.equals(DBType.MYSQL)) {
			return "CONCAT(" + alias + ".T_PATH,'%')";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			return alias + ".T_PATH + '%' ";
		}
		return "";
	}

	/**
	 * 获取指定节点的父节点IDs
	 * 
	 * @param tableName
	 *            表名称
	 * @param id
	 *            表主键ID
	 * @param pId
	 *            表父节点PID
	 * @param listIds
	 *            节点ID
	 * @return
	 */
	private static String getParentIds(String tableName, String id, String pId, Set<Long> listIds) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT P.").append(id).append(" FROM ").append(tableName).append(" P ");
		buffer.append(" INNER JOIN ").append(tableName).append(" C ON C.T_PATH LIKE ").append(getJoinFunc("P"));
		buffer.append("WHERE C.").append(id).append(" IN").append(getIdsSet(listIds));
		return buffer.toString();
	}

	/**
	 * 获取组织节点对应上级单位ID集合
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getParentOrgIds(Set<Long> listIds) {
		return JecnCommonSql.getParentIds(JecnCommonSqlConstant.ORG_TABLE_NAME, JecnCommonSqlConstant.ORG_ID,
				JecnCommonSqlConstant.ORG_PID, listIds);
	}

	/**
	 * 树形结构表，获取节点的子节点集合（删除）
	 * 
	 * @param tableName
	 *            表名称
	 * @param id
	 *            表主键ID
	 * @param pId
	 *            表父节点PID
	 * @param listIds
	 *            节点ID
	 * @return
	 */
	public static String getChildObjectsSql(String tableName, String id, String pId, String typeNode,
			List<Long> listIds, TreeNodeType treeNodeType) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT C." + id + ",C." + pId);
		if (treeNodeType != null && treeNodeType != TreeNodeType.organization) {
			buffer.append(",C." + typeNode);
		}
		buffer.append(" FROM ").append(tableName).append(" C ");
		buffer.append(" INNER JOIN ").append(tableName).append(" P ON C.T_PATH LIKE ").append(getJoinFunc("P"));
		buffer.append(" WHERE P.").append(id).append(" IN ").append(getIds(listIds));
		return buffer.toString();
	}

	/**
	 * 通过类型获得所有子节点包含self
	 * 
	 * @param tableName
	 * @param id
	 * @param pId
	 * @param listIds
	 * @param moveNodeType
	 * @return
	 */
	public static String getChildObjectsSqlByType(List<Long> listIds, TreeNodeType treeNodeType) {
		switch (treeNodeType) {
		case process:
		case processFile:
		case processMap:
		case processMapMode:
		case processMode:
			return JecnCommonSql.getChildObjectsSql("JECN_FLOW_STRUCTURE_T", "FLOW_ID", "PRE_FLOW_ID", "ISFLOW",
					listIds, treeNodeType);
		case standardDir:
		case standard:
		case standardProcess:
		case standardProcessMap:
		case standardClause:
			return JecnCommonSql.getChildObjectsSql("JECN_CRITERION_CLASSES", "CRITERION_CLASS_ID",
					"PRE_CRITERION_CLASS_ID", "STAN_TYPE", listIds, treeNodeType);
		case standardClauseRequire:
			return "SELECT T.CRITERION_CLASS_ID,T.PRE_CRITERION_CLASS_ID,T.STAN_TYPE FROM JECN_CRITERION_CLASSES T WHERE T.CRITERION_CLASS_ID  in "
					+ JecnCommonSql.getIds(listIds);
		case organization:
			return JecnCommonSql.getChildObjectsSql("JECN_FLOW_ORG", "ORG_ID", "PER_ORG_ID", "", listIds, treeNodeType);
		case position:
			return "SELECT T.FIGURE_ID,T.ORG_ID FROM JECN_FLOW_ORG_IMAGE T WHERE T.FIGURE_ID in "
					+ JecnCommonSql.getIds(listIds);
		case ruleDir:
			return JecnCommonSql.getChildObjectsSql("JECN_RULE_T", "ID", "PER_ID", "IS_DIR", listIds, treeNodeType);
		case ruleFile:
		case ruleModeFile:
			return "SELECT T.ID,T.Per_Id,T.IS_DIR FROM JECN_RULE_T T WHERE T.ID  in" + JecnCommonSql.getIds(listIds);
		case riskDir:
			return JecnCommonSql.getChildObjectsSql("JECN_RISK", "ID", "PARENT_ID", "IS_DIR", listIds, treeNodeType);
		case riskPoint:
			return "SELECT T.ID,T.PARENT_ID,T.IS_DIR FROM JECN_RISK T WHERE T.ID  in " + JecnCommonSql.getIds(listIds);
		case fileDir:
			return JecnCommonSql.getChildObjectsSql("JECN_FILE_T", "FILE_ID", "PER_FILE_ID", "IS_DIR", listIds,
					treeNodeType);
		case file:
			return "SELECT T.FILE_ID,T.Per_File_Id,T.IS_DIR FROM JECN_FILE_T T WHERE T.FILE_ID  in "
					+ JecnCommonSql.getIds(listIds);
		case positionGroupDir:
			return JecnCommonSql.getChildObjectsSql("JECN_POSITION_GROUP", "ID", "PER_ID", "IS_DIR", listIds,
					treeNodeType);
		case positionGroup:
			return "SELECT id,per_id,IS_DIR FROM JECN_POSITION_GROUP WHERE ID in" + JecnCommonSql.getIds(listIds);
		case termDefineDir:
			return JecnCommonSql.getChildObjectsSql("TERM_DEFINITION_LIBRARY", "ID", "PARENT_ID", "IS_DIR", listIds,
					treeNodeType);
		default:
			return "";
		}
	}

	/**
	 * 树形结构表，获取节点的子节点集合（删除）
	 * 
	 * @param tableName
	 *            表名称
	 * @param id
	 *            表主键ID
	 * @param pId
	 *            表父节点PID
	 * @param listIds
	 *            节点ID
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleId(String tableName, String id, List<Long> listIds) {
		return getChildObjectsSqlGetSingleIdCommon(tableName, id) + getIds(listIds);
	}

	private static String getChildObjectsSqlGetSingleIdCommon(String tableName, String id) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT  C." + id);
		buffer.append(" FROM ").append(tableName).append(" C ");
		buffer.append(" INNER JOIN ").append(tableName).append(" P ON C.T_PATH LIKE ").append(getJoinFunc("P"));
		buffer.append(" WHERE P.").append(id).append(" IN ");
		return buffer.toString();
	}

	private static String getChildObjectsSqlGetSingleIdDelState(String tableName, String id, List<Long> listIds,
			String typeNode) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT  C." + id + ",C." + typeNode);
		buffer.append(" FROM ").append(tableName).append(" C ");
		buffer.append(" INNER JOIN ").append(tableName).append(" P ON C.T_PATH LIKE ").append(getJoinFunc("P"));
		buffer.append(" WHERE C.DEL_STATE<>2 AND P.").append(id).append(" IN ").append(getIds(listIds));
		return buffer.toString();
	}

	/**
	 * 制度、流程、文件真删的时候，过滤已废止的文件
	 * 
	 * @param tableName
	 * @param id
	 * @param pId
	 * @param listIds
	 * @param moveNodeType
	 * @return
	 */
	public static String getChildObjectsSqlByTypeDelState(List<Long> listIds, TreeNodeType treeNodeType) {
		switch (treeNodeType) {
		case process:
		case processFile:
		case processMap:
			return JecnCommonSql.getChildObjectsSqlGetSingleIdDelState("JECN_FLOW_STRUCTURE_T", "FLOW_ID", listIds,
					"ISFLOW");
		case ruleDir:
		case ruleFile:
		case ruleModeFile:
			return JecnCommonSql.getChildObjectsSqlGetSingleIdDelState("JECN_RULE_T", "ID", listIds, "IS_DIR");
		case fileDir:
			return JecnCommonSql.getChildObjectsSqlGetSingleIdDelState("JECN_FILE_T", "FILE_ID", listIds, "IS_DIR");
		default:
			return "";
		}
	}

	/**
	 * 树形结构表，获取节点的子节点集合（删除）
	 * 
	 * @param tableName
	 *            表名称
	 * @param id
	 *            表主键ID
	 * @param pId
	 *            表父节点PID
	 * @param setIds
	 *            节点ID
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleId(String tableName, String id, Set<Long> setIds) {
		return getChildObjectsSqlGetSingleIdCommon(tableName, id) + getIdsSet(setIds);
	}

	/**
	 * 获得流程子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdFlow(List<Long> listIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_FLOW_STRUCTURE_T", "FLOW_ID", listIds);
	}

	/**
	 * 获得流程子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdFlow(Set<Long> setIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_FLOW_STRUCTURE_T", "FLOW_ID", setIds);
	}

	/**
	 * 获得标准子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdStandard(List<Long> listIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_CRITERION_CLASSES", "CRITERION_CLASS_ID", listIds);
	}

	/**
	 * 获得制度子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdRule(List<Long> listIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_RULE_T", "ID", listIds);
	}

	/**
	 * 获得制度子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdRule(Set<Long> setIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_RULE_T", "ID", setIds);
	}

	/**
	 * 获得文件子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdFile(List<Long> listIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_FILE_T", "FILE_ID", listIds);
	}

	/**
	 * 获得文件子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdFile(Set<Long> setIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_FILE_T", "FILE_ID", setIds);
	}

	/**
	 * 获得风险子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdRisk(List<Long> listIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_RISK", "ID", listIds);
	}

	/**
	 * 获得岗位组子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdPostionGroup(List<Long> listIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_POSITION_GROUP", "ID", listIds);
	}

	/**
	 * 通过类型获得所有子节点包含self
	 * 
	 * @param tableName
	 * @param id
	 * @param pId
	 * @param listIds
	 * @param moveNodeType
	 * @return
	 */
	public static String getChildObjectsSqlByTypeDel(List<Long> listIds, TreeNodeType treeNodeType) {
		switch (treeNodeType) {
		case process:
		case processMap:
		case processMapMode:
		case processMode:
		case processFile:
			return JecnCommonSql.getChildObjectsSqlGetSingleIdFlow(listIds);
		case standardDir:
		case standard:
		case standardProcess:
		case standardProcessMap:
		case standardClause:
			return JecnCommonSql.getChildObjectsSqlGetSingleIdStandard(listIds);
		case standardClauseRequire:
			return "SELECT T.CRITERION_CLASS_ID FROM JECN_CRITERION_CLASSES T WHERE T.CRITERION_CLASS_ID  in "
					+ JecnCommonSql.getIds(listIds);
		case organization:
			return JecnCommonSql.getChildObjectsSqlGetSingleId("JECN_FLOW_ORG", "ORG_ID", listIds);
		case position:
			return "SELECT T.FIGURE_ID FROM JECN_FLOW_ORG_IMAGE T WHERE T.FIGURE_ID in "
					+ JecnCommonSql.getIds(listIds);
		case ruleDir:
			return JecnCommonSql.getChildObjectsSqlGetSingleIdRule(listIds);
		case ruleFile:
		case ruleModeFile:
			return "SELECT T.ID  FROM JECN_RULE_T T WHERE T.ID  in" + JecnCommonSql.getIds(listIds);
		case riskDir:
			return JecnCommonSql.getChildObjectsSqlGetSingleIdRisk(listIds);
		case riskPoint:
			return "SELECT T.ID  FROM JECN_RISK T WHERE T.ID  in " + JecnCommonSql.getIds(listIds);
		case fileDir:
			return JecnCommonSql.getChildObjectsSqlGetSingleIdFile(listIds);
		case file:
			return "SELECT T.FILE_ID  FROM JECN_FILE_T T WHERE T.FILE_ID  in " + JecnCommonSql.getIds(listIds);
		case positionGroupDir:
			return JecnCommonSql.getChildObjectsSqlGetSingleIdPostionGroup(listIds);
		case positionGroup:
			return "SELECT id  FROM JECN_POSITION_GROUP WHERE ID in" + JecnCommonSql.getIds(listIds);
		default:
			return "";
		}
	}

	/**
	 * 通过类型获得所有子节点包含self
	 * 
	 * @param tableName
	 * @param id
	 * @param pId
	 * @param listIds
	 * @param moveNodeType
	 * @return
	 */
	public static String getChildObjectsSqlByTypeDelAddBracket(List<Long> listIds, TreeNodeType treeNodeType) {
		return " ( " + JecnCommonSql.getChildObjectsSqlByTypeDel(listIds, treeNodeType) + " ) ";
	}

	public static String getConcatChar() {
		String concat = "";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			concat = "||";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			concat = "+";
		}
		return concat;
	}

	public static String getDateTimeByDB() {
		String dateTime = "";
		if (JecnContants.dbType == DBType.ORACLE) {
			dateTime = "sysdate";
		} else if (JecnContants.dbType == DBType.SQLSERVER) {
			dateTime = "getdate()";
		}
		return dateTime;
	}

	public static String getNullByDB() {
		if (JecnContants.dbType == DBType.ORACLE) {
			return " IS NOT NULL";
		} else if (JecnContants.dbType == DBType.SQLSERVER) {
			return " <> ''";
		}
		return " IS NOT NULL";
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static String getFuncSqlDate(Date date) {
		if (date == null) {
			return null;
		}
		if (JecnContants.dbType == DBType.ORACLE) {
			return "to_date('" + getStringbyDateHMS(date) + "','yyyy-mm-dd hh24:mi:ss')";
		} else if (JecnContants.dbType == DBType.SQLSERVER) {
			return "CONVERT(varchar(120), '" + getStringbyDateHMS(date) + "', 120)";
		}
		return "";
	}

	public static String getStringbyDateHMS(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}

	public static String getSqlGUID() {
		if (JecnContants.dbType == DBType.ORACLE) {
			return "sys_guid()";
		} else if (JecnContants.dbType == DBType.SQLSERVER) {
			return "newid()";
		}
		return "";
	}

	/**
	 * 
	 * @param setFlowIds
	 * @param relatedType
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板、4是制度文件（本地上传）、5是文件目录、6是文件
	 * @return
	 */
	public static String getAutoCodeRelatedTableSql(Set<Long> setFlowIds, int relatedType) {
		return "DELETE FROM AUTO_CODE_RELATED_TABLE WHERE RELATED_ID IN" + JecnCommonSql.getIdsSet(setFlowIds)
				+ " AND RELATED_TYPE = " + relatedType;
	}

	public static String getAutoCodeTableSql(Set<Long> setFlowIds) {
		if (JecnContants.dbType == DBType.ORACLE) {
			return "DELETE FROM Auto_Code_Table T WHERE EXISTS (SELECT 1 FROM AUTO_CODE_RELATED_TABLE WHERE AUTO_CODE_ID = T.GUID AND RELATED_ID IN "
					+ JecnCommonSql.getIdsSet(setFlowIds) + ")";
		}
		return "DELETE T WHERE EXISTS (SELECT 1 FROM AUTO_CODE_RELATED_TABLE WHERE AUTO_CODE_ID = T.GUID AND RELATED_ID IN "
				+ JecnCommonSql.getIdsSet(setFlowIds) + ") FROM Auto_Code_Table T ";
	}

	/**
	 * 获得部门子节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getChildObjectsSqlGetSingleIdOrg(List<Long> listIds) {
		return JecnCommonSql.getChildObjectsSqlGetSingleId("jecn_flow_org", "org_id", listIds);
	}

	/**
	 * 获得部门fu节点集合（包含自己）SQL
	 * 
	 * @param listIds
	 * @return
	 */
	public static String getParentObjectsSqlOrg(Set<Long> setIds) {
		return "SELECT DISTINCT PARENT.ORG_ID, PARENT.ORG_NAME,PARENT.PER_ORG_ID FROM JECN_FLOW_ORG T"
				+ "  LEFT JOIN JECN_FLOW_ORG PARENT ON " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PARENT")
				+ " WHERE T.ORG_ID in" + JecnCommonSql.getIdsSet(setIds);
	}

	/**
	 * 自动编号-特殊公司处理
	 * 
	 * @param nodeId
	 * @return
	 */
	public static Long specialAutoCodeNodeHandle(Long nodeId, int nodeType) {
		if (isSpecialLoginType() && nodeType != 1) {
			nodeId = -1L;
		}
		return nodeId;
	}

	public static boolean isSpecialLoginType() {
		return JecnContants.otherLoginType == 23 || JecnContants.otherLoginType == 39;
	}

	public static String getPosByOrgAndPos(String orgName, String posName, boolean isSelect) {
		// a部门-b部门 搜索a部门的时候需要将b部门下的岗位也搜出来
		String sql = "select distinct jfoi.figure_id, jfoi.figure_text, jfo.org_id, jfo.org_name "
				+ "  from jecn_flow_org_image jfoi" + " inner join jecn_flow_org jfo"
				+ "    on jfoi.org_id = jfo.org_id ";
		if (StringUtils.isNotBlank(orgName)) {
			if (isSelect) {
				sql += " inner join (select c.*" + "               from jecn_flow_org c"
						+ "              inner join jecn_flow_org p" + "                 on c.t_path like p.t_path "
						+ JecnCommonSql.getConcatChar() + " '%' and c.del_state=0"
						+ "              where  p.org_name like '%" + orgName + "%') org"
						+ "    on jfoi.org_id = org.org_id";
			} else {
				sql += " inner join (select c.*" + "             from jecn_flow_org c"
						+ "           where    c.del_state=0 and c.org_name like '%" + orgName + "%') org"
						+ "    on jfoi.org_id = org.org_id";
			}
		}
		sql += " where jfoi.figure_type = 'PositionFigure' and jfo.del_state=0 ";
		if (StringUtils.isNotBlank(posName)) {
			sql += " and  jfoi.figure_text like '%" + posName + "%'";
		}
		return sql;
	}

	public static String getPosByOrgAndPos(String orgName, String posName) {
		// a部门-b部门 搜索a部门的时候需要将b部门下的岗位也搜出来
		String sql = "select distinct jfoi.figure_id, jfoi.figure_text, jfo.org_id, jfo.org_name "
				+ "  from jecn_flow_org_image jfoi" + " inner join jecn_flow_org jfo"
				+ "    on jfoi.org_id = jfo.org_id ";
		if (StringUtils.isNotBlank(orgName)) {
			sql += " inner join (select c.*" + "               from jecn_flow_org c"
					+ "              inner join jecn_flow_org p" + "                 on " + getSqlSubStr("p", "c")
					+ " and c.del_state=0" + "              where  p.org_name like '%" + orgName + "%') org"
					+ "    on jfoi.org_id = org.org_id";

		}
		sql += " where jfoi.figure_type = 'PositionFigure' and jfo.del_state=0 ";
		if (StringUtils.isNotBlank(posName)) {
			sql += " and  jfoi.figure_text like '%" + posName + "%'";
		}
		return sql;
	}

	private static String getSqlSubStr(String p, String c) {
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			return "" + p + ".T_PATH = SUBSTR(c.T_PATH, 0, LENGTH(" + p + ".T_PATH))";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			return "" + p + ".T_PATH = SUBSTRING(c.T_PATH, 0, LEN(" + p + ".T_PATH)+1)";
		}
		return "";
	}

	public static String getPosByOrgAndPos(List<Long> orgIds, String posName, boolean isSelect) {
		// a部门-b部门 搜索a部门的时候需要将b部门下的岗位也搜出来
		String sql = "select distinct jfoi.figure_id, jfoi.figure_text, jfo.org_id, jfo.org_name "
				+ "  from jecn_flow_org_image jfoi" + " inner join jecn_flow_org jfo"
				+ "    on jfoi.org_id = jfo.org_id ";
		if (orgIds != null && !orgIds.isEmpty()) {
			if (isSelect) {
				sql += " inner join (select c.*" + "               from jecn_flow_org c"
						+ "              inner join jecn_flow_org p" + "                 on c.t_path like p.t_path "
						+ JecnCommonSql.getConcatChar() + " '%' and c.del_state=0"
						+ "              where  p.org_id in " + JecnCommonSql.getIds(orgIds) + ") org"
						+ "    on jfoi.org_id = org.org_id";
			} else {
				sql += " inner join (select c.*" + "               from jecn_flow_org c"
						+ "              where   c.del_state=0 and c.org_id in " + JecnCommonSql.getIds(orgIds)
						+ ") org" + "    on jfoi.org_id = org.org_id";
			}
		}
		sql += " where jfoi.figure_type = 'PositionFigure' and jfo.del_state=0 ";
		if (StringUtils.isNotBlank(posName)) {
			sql += " and  jfoi.figure_text like '%" + posName + "%'";
		}
		return sql;
	}

	// 将字Clob转成String类型
	public static String ClobToString(Clob sc) throws SQLException, IOException {
		String reString = "";
		Reader is = sc.getCharacterStream();// 得到流
		BufferedReader br = new BufferedReader(is);
		String s = br.readLine();
		StringBuffer sb = new StringBuffer();
		while (s != null) {// 执行循环将字符串全部取出付值给StringBuffer由StringBuffer转成STRING
			sb.append(s);
			s = br.readLine();
		}
		reString = sb.toString();
		return reString;
	}
}
