package com.jecn.epros.server.connector.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONObject;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.jecn.epros.server.action.web.login.ad.fuyao.FuYaoiAfterItem;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.HttpRequest;
import com.jecn.epros.server.common.HttpRequestUtil;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.task.MyTaskBean;

public class FuYaoTaskInfo extends JecnBaseTaskSend {

	private IPersonDao personDao;

	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew, JecnUser jecnUser, JecnUser createUser) {
		IPersonDao personDao = (IPersonDao) ApplicationContextUtil.getContext().getBean("PersonDaoImpl");
		this.personDao = personDao;
		List<Long> cancelPeoples = new ArrayList<Long>();
		cancelPeoples.add(curPeopleId);
		try {
			sendInfoCancels(jecnTaskBeanNew, personDao, null, cancelPeoples, curPeopleId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew taskBeanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
		Set<Long> setCancelPeopleIds = new HashSet<Long>();
		setCancelPeopleIds.addAll(cancelPeoples);
		for (Long id : cancelPeoples) {
			changeStatusToHandled(myTaskBean, taskBeanNew, id);
		}
		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		// 审批任务，发送下一环节任务待办
		createTasks(myTaskBean, taskBeanNew, handlerPeopleIds);
	}

	@Override
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			log.info("创建代办开始~");
			// 创建代办
			createTasks(myTaskBean, beanNew, handlerPeopleIds);
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			changeStatusToHandled(myTaskBean, beanNew, curPeopleId);
		} else {// 一条from的数据,一条to 的数据
			// 代办转已办,是否为多审判断
			// 设置上一环节任务待办任务状态为false，表示未已审批；
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 上一个阶段，处理任务，代办转已办
			changeStatusToHandled(myTaskBean, beanNew, curPeopleId);

			if ((beanNew.getTaskElseState() == 2 || beanNew.getTaskElseState() == 3)
					|| beanNew.getState() != beanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				// 审批任务，发送下一环节任务待办
				createTasks(myTaskBean, beanNew, handlerPeopleIds);
			}
		}
	}

	/**
	 * 创建代办
	 * 
	 * @param myTaskBean
	 * @param taskBeanNew
	 * @throws Exception
	 */
	private void createTasks(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, Set<Long> handlerPeopleIds)
			throws Exception {

		// 获取token
		String tokenResult = HttpRequest.sendGet(FuYaoiAfterItem.getValue("TASK_VALIDATE_URL"), "");

		String returnObj = JSONObject.fromObject(tokenResult).getString("id");
		// 发送代办消息
		if ("-1".equals(returnObj)) {
			log.error("获取代办消息失败! tokenResult = " + tokenResult);
		}

		JecnUser createUser = personDao.get(taskBeanNew.getCreatePersonId());

		for (Long handlerPeopleId : handlerPeopleIds) {
			String result = HttpRequestUtil.sendPostRest(FuYaoiAfterItem.getValue("TASK_URL") + "/" + returnObj,
					getCreateTaskBean(myTaskBean, "0", createUser, handlerPeopleId, taskBeanNew));
			log.info(" result = " + result);
		}
	}

	/**
	 * 代办转已办
	 */
	private void changeStatusToHandled(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, Long handlerId)
			throws Exception {
		JecnUser createUser = personDao.get(taskBeanNew.getCreatePersonId());
		String result = HttpRequestUtil.sendPostRest(FuYaoiAfterItem.getValue("UPDATE_TASK_URL"), getCreateTaskBean(
				myTaskBean, "1", createUser, handlerId, taskBeanNew));
		log.info(" result = " + result);
	}

	public static void main(String[] args) {
		String result = "{'id': 'ca93b7f9-7b20-4bc0-9676-c361d81af625'} ";
		String returnObj = JSONObject.fromObject(result).getString("id");
		System.out.println(returnObj);
	}

	private MultiValueMap<String, String> getCreateTaskBean(MyTaskBean myTaskBean, String taskType,
			JecnUser createUser, Long handlerPeopleId, JecnTaskBeanNew taskBeanNew) {
		// 代办处理人
		JecnUser handlerUser = personDao.get(handlerPeopleId);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("registerCode", "EPROS");
		map.add("taskId", getSN(taskBeanNew.getId(), taskBeanNew.getRid()));
		map.add("title", "EPROS");
		map.add("senderName", myTaskBean.getTaskName());
		map.add("classify", "EPROS");
		map.add("contentType", "");
		map.add("state", taskType);
		map.add("thirdSenderId", createUser.getLoginName());
		map.add("thirdReceiverId", handlerUser.getLoginName());
		map.add("creationDate", JecnCommon.getStringbyDateHMS(taskBeanNew.getCreateTime()));
		map.add("content", "");
		map.add("h5url", getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, false));
		map.add("url", getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, false));
		map.add("noneBindingSender", createUser.getLoginName());
		map.add("noneBindingReceiver", handlerUser.getLoginName());
		return map;
	}

	private String getSN(Long taskId, Long rId) {
		return taskId + "_" + rId;
	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(getTaskName(beanNew) + "审批");
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setStageName(beanNew.getStateMark());
		return myTaskBean;
	}
}
