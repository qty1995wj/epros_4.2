package com.jecn.epros.server.action.designer.integration.impl;

import java.util.List;

import com.jecn.epros.server.action.designer.integration.IJecnActivityAction;
import com.jecn.epros.server.bean.integration.JecnActiveTypeBean;
import com.jecn.epros.server.service.integration.IJecnActivityService;

/**
 * Action接口实现
 * 
 * @author Administrator
 * @date： 日期：2013-10-31 时间：上午10:02:23
 */
public class JecnActivityActionImpl implements IJecnActivityAction {
	/** 活动类别service接口 */
	private IJecnActivityService activityService;

	/**
	 * 添加或更新活动类别
	 * 
	 * @param activeTypeBean
	 * @throws Exception
	 */
	@Override
	public Long addActivityType(JecnActiveTypeBean activeTypeBean)
			throws Exception {
		return activityService.addActivityType(activeTypeBean);
	}
	/**
	 * 验证是否存在相同名称
	 * 
	 * @param name
	 *            输入的类别名称
	 * @return true:存在相同名称
	 * @throws Exception
	 */
	@Override
	public boolean isSameActivityType(String name) throws Exception {
		// 获取指定名称的的个数
		int count = activityService.findCountByName(name);
		if (count > 0) {// 存在相同名称
			return true;
		}
		return false;
	}

	/**
	 * 获取活动类别集合
	 * 
	 * @return List<JecnActiveTypeBean>
	 * @throws Exception
	 */
	@Override
	public List<JecnActiveTypeBean> findJecnActiveTypeBeanList()
			throws Exception {
		return activityService.findJecnActiveTypeBeanList();
	}

	/**
	 * 
	 * 删除活动类别
	 * 
	 * @param typeIds
	 * @throws Exception
	 */
	@Override
	public void delteFlowArributeType(List<Long> typeIds) throws Exception {
		activityService.delteFlowArributeType(typeIds);
	}

	public IJecnActivityService getActivityService() {
		return activityService;
	}

	public void setActivityService(IJecnActivityService activityService) {
		this.activityService = activityService;
	}

}
