package epros.designer.gui.standard;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 新建条款要求
 * 
 * @author admin
 * 
 */
public class AddStanClauseRequireDialog extends StanClauseRequireDialog {
	private static Logger log = Logger.getLogger(AddStanClauseRequireDialog.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AddStanClauseRequireDialog(JecnTreeNode node, JTree jTree) {
		super(node, jTree);
		this.setTitle(JecnProperties.getValue("newStanClauseRequire"));
	}

	public void okButtonAction() {
		String clauseContent = clauseRequireArea.getText().trim();
		if (!DrawCommon.isNullOrEmtry(clauseContent)) {
			if (JecnUserCheckUtil.getTextLength(clauseContent) > 1200) {
				tipLabel.setText(JecnUserCheckInfoData.getNoteLengthInfo());
				return;
			}
		} else {
			tipLabel.setText(JecnUserCheckInfoData.getNameNotNull());
			return;
		}
		String name = "";
		if (clauseContent.length() > 8) {
			name = clauseContent.substring(0, 8) + "...";
		} else {
			name = clauseContent;
		}

		StandardBean standardBean = new StandardBean();
		standardBean.setPreCriterionClassId(node.getJecnTreeBean().getId());
		standardBean.setCriterionClassName(name);
		standardBean.setProjectId(JecnConstants.projectId);
		standardBean.setSortId(Long.valueOf(JecnTreeCommon.getMaxSort(node)));
		standardBean.setCreatePeopleId(JecnConstants.getUserId());
		standardBean.setUpdatePeopleId(JecnConstants.getUserId());
		standardBean.setIsPublic(1L);
		standardBean.setStanType(5);
		standardBean.setStanContent(clauseContent);
		
		Long id = null;
		try {
			id = ConnectionPool.getStandardAction().addStandard(standardBean);
			// 向树节点添加标准节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(name);
			jecnTreeBean.setPid(node.getJecnTreeBean().getId());
			jecnTreeBean.setPname(node.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.standardClauseRequire);
			jecnTreeBean.setContent(clauseContent);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, node);
			this.setVisible(false);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("newTermsAndConditionsRequired"));
			log.error(JecnProperties.getValue("newTermsAndConditionsRequired"), e);
		}
	}

	public boolean isUpdate() {
		String clauseContent = clauseRequireArea.getText().trim();
		if (!DrawCommon.isNullOrEmtry(clauseContent)) {
			return true;
		}
		return false;
	}

}
