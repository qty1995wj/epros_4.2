package epros.designer.gui.popedom.position;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
/**
 * @author yxw 2012-6-14
 * @description：
 */
public class HighEfficiencyOrgPositionTreeByStartId extends HighEfficiencyOrgPositionTree {
	private static Logger log = Logger.getLogger(HighEfficiencyOrgPositionTreeByStartId.class);

	public HighEfficiencyOrgPositionTreeByStartId(Long startId){
		super(startId);
	}
	
	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.organizationRoot,JecnProperties.getValue("orgPosition"));
		try {
			JecnTreeBean bean=ConnectionPool.getOrganizationAction().getCurrentOrg(startId,JecnConstants.projectId);
			JecnTreeCommon.addChildNode(rootNode, bean);
		} catch (Exception e) {
			log.error("HighEfficiencyOrgPositionTreeByStartId getTreeModel is error！",e);
		}
		return new JecnTreeModel(rootNode);
	}
}
