package epros.designer.gui.rule.table;

import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.rule.JecnFlowCommon;

import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

public class RuleFlowTable extends JTable {
	private List<JecnFlowCommon> listJecnFlowCommon = null;
	// 标题ID
	Long titleId = null;

	public List<JecnFlowCommon> getListJecnFlowCommon() {
		return listJecnFlowCommon;
	}

	public void setListJecnFlowCommon(List<JecnFlowCommon> listJecnFlowCommon) {
		this.listJecnFlowCommon = listJecnFlowCommon;
	}

	public RuleFlowTable(List<JecnFlowCommon> listJecnFlowCommon, Long titleId) {
		this.listJecnFlowCommon = listJecnFlowCommon;
		this.titleId = titleId;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	public RuleFlowTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		title.add(JecnProperties.getValue("flowNum"));
		title.add(JecnProperties.getValue("flowName")); // updateTableContent(ruleModelList)
		return new RuleFlowTableMode(updateTableContent(listJecnFlowCommon),
				title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class RuleFlowTableMode extends DefaultTableModel {
		public RuleFlowTableMode(Vector<Vector<Object>> data,
				Vector<String> title) {
			super(data, title);

		}

		@Override
		public Class getColumnClass(int columnIndex) {
			return getValueAt(0, columnIndex).getClass();
		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	public Vector<Vector<Object>> updateTableContent(List<JecnFlowCommon> list) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				JecnFlowCommon jecnFlowCommon = list.get(i);

				Vector<Object> data = new Vector<Object>();
				if (jecnFlowCommon != null) {
					data.add(jecnFlowCommon.getFlowId());
					// 流程编号
					data.add(jecnFlowCommon.getFlowNumber());
					// 流程名称
					data.add(jecnFlowCommon.getFlowName());
					vector.add(data);
				}
			}
		}
		return vector;
	}
}
