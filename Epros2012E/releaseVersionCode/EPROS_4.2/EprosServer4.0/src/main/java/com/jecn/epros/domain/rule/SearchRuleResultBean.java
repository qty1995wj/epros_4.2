package com.jecn.epros.domain.rule;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.util.ConfigUtils;

import java.util.Date;

public class SearchRuleResultBean {
    private long ruleId;
    /**
     * 制度名称
     */
    private String ruleName;
    /**
     * 制度编号
     */
    private String ruleNum;
    /**
     * 密级
     */
    private int isPublic;
    /**
     * 责任部门
     */
    private String dutyOrg;
    /**
     * 发布日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    public Date pubTime;

    public LinkResource getLink() {
        return new LinkResource(ruleId, ruleName, ResourceType.RULE, TreeNodeUtils.NodeType.search);
    }

    public String getPublicStr(){
        return ConfigUtils.getSecretByType(isPublic);
    }

    @JsonIgnore
    public long getRuleId() {
        return ruleId;
    }

    public void setRuleId(long ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleNum() {
        return ruleNum;
    }

    public void setRuleNum(String ruleNum) {
        this.ruleNum = ruleNum;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public String getDutyOrg() {
        return dutyOrg;
    }

    public void setDutyOrg(String dutyOrg) {
        this.dutyOrg = dutyOrg;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }

}
