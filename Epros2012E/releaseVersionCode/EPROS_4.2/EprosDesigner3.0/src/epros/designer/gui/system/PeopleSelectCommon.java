package epros.designer.gui.system;

import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.UserSearchBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class PeopleSelectCommon extends SingleSelectCommon {
	private static Logger log = Logger.getLogger(PeopleSelectCommon.class);

	public PeopleSelectCommon(int rows, JecnPanel infoPanel, Insets insets, Long guardianId, String guardianNames,
			JecnDialog jecnDialog, String labName, boolean isRequest) {
		super(rows, infoPanel, insets, new JecnTreeBean(guardianId, guardianNames, TreeNodeType.person), jecnDialog,
				labName, isRequest);
		editableIflytek();

	}

	public PeopleSelectCommon(int rows, JecnPanel infoPanel, Insets insets, JecnTreeBean jecnTreeBean,
			JecnDialog jecnDialog, String labName, boolean isRequest) {
		super(rows, infoPanel, insets, jecnTreeBean, jecnDialog, labName, isRequest);
		editableIflytek();

	}

	// 科大讯飞 禁用 文本搜索的输入
	private void editableIflytek() {
		if (JecnConfigTool.isIflytekLogin()) {
			jecnField.setEditable(false);
		}
	}

	// 科大讯飞 根据登录名 获取真实姓名
	protected void getIflytekPeopleName(JecnTreeBean jecnTreeBean) {
		if (JecnConfigTool.isIflytekLogin()) {
			JecnUser jecnUserByPeopleId = ConnectionPool.getPersonAction().getJecnUserByPeopleId(jecnTreeBean.getId());
			if(jecnUserByPeopleId != null){
				String name = ConnectionPool.getPersonAction().getListUserSearchBean(jecnUserByPeopleId.getLoginName());
				if (StringUtils.isNotBlank(name)) {
					jecnTreeBean.setName(name);
				}
			}
		}
	}

	@Override
	protected List<JecnTreeBean> searchName(String name, Long projectId) {
		try {
			// 科大讯飞人员搜索 禁用模糊搜索
			if (JecnConfigTool.isIflytekLogin()) {
				return new ArrayList<JecnTreeBean>();
			}
			return ConnectionPool.getPersonAction().searchUserByName(name, projectId);

		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this.getJecnDialog(), JecnProperties.getValue("serverConnException"));
			log.error(JecnProperties.getValue("searchPeopleException"), e);
		}
		return new ArrayList<JecnTreeBean>();
	}

	@Override
	protected void selectButPerformed(List<JecnTreeBean> list, JSearchTextField jecnField, Long id) {
		JecnDesignerCommon.setResponsiblePerson(list, jecnField, getJecnDialog(), id, super.selectChangeListeners);
	}
}
