package com.jecn.epros.server.bean.copyNode;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT;
import com.jecn.epros.server.bean.integration.JecnControlPointT;
import com.jecn.epros.server.bean.process.FlowOrgT;
import com.jecn.epros.server.bean.process.JecnActivityFileT;
import com.jecn.epros.server.bean.process.JecnFigureFileTBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStationT;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnLineSegmentT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.process.JecnModeFileT;
import com.jecn.epros.server.bean.process.JecnRefIndicatorsT;
import com.jecn.epros.server.bean.process.JecnTempletT;
import com.jecn.epros.server.bean.process.ProcessFigureData;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT;
import com.jecn.epros.server.bean.process.inout.JecnFigureInoutT;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.dao.process.IFlowOrgDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.util.JecnUtil;

public class JecnCopyHandle {
	
	private static Logger log = Logger.getLogger(JecnCopyHandle.class);
	private IFlowStructureDao flowStructureDao;
	private IFlowOrgDao flowOrgDao;
	private IProcessBasicInfoDao processBasicDao;

	/** 记录复制的元素ID与新生产的元素ID的对应关系集合 */
	private Map<Long, Long> lineFigureMap = new HashMap<Long, Long>();
	/** 记录复制的流程ID与新生成的流程ID对应关系集合 */
	private Map<Long, Long> linkFlowMap = new HashMap<Long, Long>();
	private List<JecnFlowStructureImageT> copyAllImageTs = new ArrayList<JecnFlowStructureImageT>();

	private JecnCopyHandle() {

	}

	public static JecnCopyHandle newInstance() {
		return new JecnCopyHandle();
	}

	public void recursiveSave(IFlowStructureDao flowStructureDao, List<ProcessOpenData> copyList,
			Map<Long, List<ProcessOpenData>> nodeMaps, Long copyToId, IFlowOrgDao flowOrgDao,
			IProcessBasicInfoDao processBasicDao, Long peopleId) throws Exception {
		this.flowStructureDao = flowStructureDao;
		this.flowOrgDao = flowOrgDao;
		this.processBasicDao = processBasicDao;
		lineFigureMap.clear();
		copyAllImageTs.clear();

		for (ProcessOpenData openData : copyList) {
			// 保存复制的当前节点
			Long newId = saveCopyFlow(openData, copyToId, openData.getJecnFlowStructureT().getIsFlow().intValue(),
					peopleId);
			// 保存复制节点
			try {
				saveCopyFlows(nodeMaps.get(openData.getJecnFlowStructureT().getFlowId()), newId, nodeMaps, peopleId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// 元素和线段的关联关系，link元素和流程的对应关系
		for (JecnFlowStructureImageT imageT : copyAllImageTs) {
			if ("ManhattanLine".equals(imageT.getFigureType()) || "CommonLine".equals(imageT.getFigureType())) {
				Long startFigureId = lineFigureMap.get(imageT.getStartFigure());
				Long endFigureId = lineFigureMap.get(imageT.getEndFigure());
				if (startFigureId == null || endFigureId == null) {
					log.error("复制连接线异常，line figure id = " + imageT.getFigureId());
				}
				imageT.setStartFigure(startFigureId);
				imageT.setEndFigure(endFigureId);
				flowStructureDao.getSession().update(imageT);
				flowStructureDao.getSession().flush();

				// 保存小线段
				saveLineSegment(imageT.getListLineSegmet(), imageT.getFigureId());
			} else if (imageT.getLinkFlowId() != null) {
				Long newLinkId = linkFlowMap.get(imageT.getLinkFlowId());
				if (newLinkId == null) {
					continue;
				}
				imageT.setLinkFlowId(newLinkId);
				flowStructureDao.getSession().update(imageT);
				flowStructureDao.getSession().flush();
			}
		}
	}

	private void saveLineSegment(List<JecnLineSegmentT> listLineSegmet, Long lineId) throws Exception {
		if (listLineSegmet == null || lineId == null) {
			return;
		}
		JecnLineSegmentT newLine = null;
		for (JecnLineSegmentT jecnLineSegmentT : listLineSegmet) {
			newLine = new JecnLineSegmentT();
			PropertyUtils.copyProperties(newLine, jecnLineSegmentT);
			newLine.setFigureId(lineId);
			newLine.setId(null);
			flowStructureDao.getSession().save(newLine);
			flowStructureDao.getSession().flush();
		}
	}

	private void saveCopyFlows(List<ProcessOpenData> openDatas, Long pId, Map<Long, List<ProcessOpenData>> nodeMaps,
			Long peopleId) throws Exception {
		if (openDatas == null) {
			return;
		}
		for (ProcessOpenData processOpenData : openDatas) {
			Long newFlowId = saveCopyFlow(processOpenData, pId, processOpenData.getJecnFlowStructureT().getIsFlow()
					.intValue(), peopleId);
			// 获取子节点
			Long flowId = processOpenData.getJecnFlowStructureT().getFlowId();
			List<ProcessOpenData> childs = nodeMaps.get(flowId);
			saveCopyFlows(childs, newFlowId, nodeMaps, peopleId);
		}
	}

	/**
	 * 保存top流程
	 * 
	 * @param topData
	 * @param peopleId
	 * @param flowId
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	private Long saveCopyFlow(ProcessOpenData topData, Long pId, int isFlow, Long peopleId) throws Exception {
		JecnFlowStructureT flowStructureT = new JecnFlowStructureT();
		PropertyUtils.copyProperties(flowStructureT, topData.getJecnFlowStructureT());
		Date now = new Date();
		flowStructureT.setUpdatePeopleId(peopleId);
		flowStructureT.setFlowId(null);
		flowStructureT.settPath(null);
		flowStructureT.setFlowName(flowStructureT.getFlowName());
		flowStructureT.setViewSort(null);
		flowStructureT.setCreateDate(now);
		flowStructureT.setCreateDate(flowStructureT.getCreateDate());
		flowStructureT.setPerFlowId(pId);
		flowStructureDao.save(flowStructureT);
		flowStructureDao.getSession().flush();

		FlowOrgT flowOrgTbean = processBasicDao.getFlowOrgTByFlowId(topData.getJecnFlowStructureT().getFlowId());// 保存责任部门
		if (flowOrgTbean != null) {
			FlowOrgT flowOrgT = new FlowOrgT();
			flowOrgT.setFlowId(flowStructureT.getFlowId());
			flowOrgT.setOrgId(flowOrgTbean.getOrgId());
			this.flowOrgDao.save(flowOrgT);
			this.flowOrgDao.getSession().flush();
		}
		// 更新Tpath
		updateTpathAndTLevel(flowStructureDao, flowStructureT);

		Long flowId = flowStructureT.getFlowId();
		linkFlowMap.put(topData.getJecnFlowStructureT().getFlowId(), flowId);

		if (isFlow == 1 && topData.getJecnFlowBasicInfoT() != null) {
			JecnFlowBasicInfoT jecnFlowBasicInfoT = new JecnFlowBasicInfoT();
			PropertyUtils.copyProperties(jecnFlowBasicInfoT, topData.getJecnFlowBasicInfoT());
			jecnFlowBasicInfoT.setFlowId(flowId);
			flowStructureDao.getSession().save(jecnFlowBasicInfoT);
			flowStructureDao.getSession().flush();
		} else if (isFlow == 0 && topData.getMainFlowT() != null) {
			JecnMainFlowT mainFlowT = new JecnMainFlowT();
			PropertyUtils.copyProperties(mainFlowT, topData.getMainFlowT());
			mainFlowT.setFlowId(flowId);
			flowStructureDao.getSession().save(mainFlowT);
			flowStructureDao.getSession().flush();
		}

		List<ProcessFigureData> processFigureDataList = topData.getProcessFigureDataList();
		if (processFigureDataList == null) {
			return flowId;
		}
		for (ProcessFigureData processFigureData : processFigureDataList) {
			saveCopyFlowAttrs(processFigureData, flowId, isFlow);
		}
		JecnUtil.saveJecnJournal(flowStructureT.getFlowId(), flowStructureT.getFlowName(), 1, 19, peopleId,
				flowStructureDao);
		return flowId;
	}

	/**
	 * 流程元素保存
	 * 
	 * @param processFigureData
	 * @param flowId
	 * @param isFlow
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccess
	 */
	private void saveCopyFlowAttrs(ProcessFigureData processFigureData, Long flowId, int isFlow) throws Exception {
		/** 元素 */
		JecnFlowStructureImageT jecnFlowStructureImageT = new JecnFlowStructureImageT();
		PropertyUtils.copyProperties(jecnFlowStructureImageT, processFigureData.getJecnFlowStructureImageT());
		jecnFlowStructureImageT.setFlowId(flowId);
		jecnFlowStructureImageT.setFigureId(null);
		flowStructureDao.getSession().save(jecnFlowStructureImageT);

		// 所有元素集合
		copyAllImageTs.add(jecnFlowStructureImageT);
		// 元素ID
		Long figureId = jecnFlowStructureImageT.getFigureId();
		// 复制的ID和新ID的对应关系
		lineFigureMap.put(processFigureData.getJecnFlowStructureImageT().getFigureId(), figureId);

		if (isFlow == 1) {// 流程架构
			// 角色与岗位关联
			saveRoleRelatedPostions(processFigureData.getListRolePosition(), figureId);
			// 输出
			saveModeFiles(processFigureData.getListModeFileT(), figureId);
			// 新版输出
			saveNewEditionModeFiles(processFigureData.getFigureOutList(), figureId, flowId);
			// 新版输入
			saveNewEditionInFiles(processFigureData.getFigureInList(), figureId, flowId);
			// 输入和操作规范
			saveActivityFiles(processFigureData.getListJecnActivityFileT(), figureId);
			// 指标
			saveRefIndicators(processFigureData.getListRefIndicatorsT(), figureId);
			// 控制点
			saveControlPoint(processFigureData.getControlPoint(), figureId);
			// 控制点集合
			saveControlPoints(processFigureData.getListControlPoint(), figureId);
			// 活动标准关联表
			saveActiveStandards(processFigureData.getListJecnActiveStandardBeanT(), figureId);
			// 线上信息
			saveActiveOnLines(processFigureData.getListJecnActiveOnLineTBean(), figureId, flowId);
		}

		// 相关附件集合
		saveFigureFiles(processFigureData.getListJecnFigureFileTBean(), figureId);
	}

	/**
	 * 活动信息化
	 * 
	 * @param listJecnActiveOnLineTBean
	 * @param figureId
	 * @throws Exception
	 */
	private void saveActiveOnLines(List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean, Long figureId, Long flowId)
			throws Exception {
		if (listJecnActiveOnLineTBean == null) {
			return;
		}
		for (JecnActiveOnLineTBean jecnActiveOnLineTBean : listJecnActiveOnLineTBean) {
			JecnActiveOnLineTBean saveActiveOnLine = new JecnActiveOnLineTBean();
			PropertyUtils.copyProperties(saveActiveOnLine, jecnActiveOnLineTBean);

			saveActiveOnLine.setActiveId(figureId);
			saveActiveOnLine.setId(JecnCommon.getUUID());
			saveActiveOnLine.setProcessId(flowId);
			flowStructureDao.getSession().save(saveActiveOnLine);
		}
	}

	/**
	 * 活动相关标准
	 * 
	 * @param listJecnActiveStandardBeanT
	 * @param figureId
	 * @throws Exception
	 */
	private void saveActiveStandards(List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT, Long figureId)
			throws Exception {
		if (listJecnActiveStandardBeanT == null) {
			return;
		}
		for (JecnActiveStandardBeanT jecnActiveStandardBeanT : listJecnActiveStandardBeanT) {
			JecnActiveStandardBeanT saveActiveStandardBeanT = new JecnActiveStandardBeanT();
			PropertyUtils.copyProperties(saveActiveStandardBeanT, jecnActiveStandardBeanT);
			saveActiveStandardBeanT.setActiveId(figureId);
			saveActiveStandardBeanT.setId(JecnCommon.getUUID());
			flowStructureDao.getSession().save(saveActiveStandardBeanT);
		}
	}

	/**
	 * 控制点集合
	 * 
	 * @param listControlPoint
	 * @param figureId
	 */
	private void saveControlPoints(List<JecnControlPointT> listControlPoint, Long figureId) throws Exception {
		if (listControlPoint == null) {
			return;
		}
		for (JecnControlPointT controlPoint : listControlPoint) {
			saveControlPoint(controlPoint, figureId);
		}
	}

	/**
	 * 控制点
	 * 
	 * @param controlPoint
	 * @param figureId
	 */
	private void saveControlPoint(JecnControlPointT controlPoint, Long figureId) throws Exception {
		if (controlPoint == null) {
			return;
		}
		JecnControlPointT saveControlPointT = new JecnControlPointT();
		PropertyUtils.copyProperties(saveControlPointT, controlPoint);

		saveControlPointT.setActiveId(figureId);
		saveControlPointT.setId(JecnCommon.getUUID());
		saveControlPointT.setCreateTime(new Date());
		saveControlPointT.setUpdateTime(saveControlPointT.getCreateTime());
		flowStructureDao.getSession().save(saveControlPointT);
	}

	/**
	 * 指标
	 * 
	 * @param listRefIndicatorsT
	 * @param figureId
	 */
	private void saveRefIndicators(List<JecnRefIndicatorsT> listRefIndicatorsT, Long figureId) throws Exception {
		if (listRefIndicatorsT == null) {
			return;
		}
		for (JecnRefIndicatorsT jecnRefIndicatorsT : listRefIndicatorsT) {
			JecnRefIndicatorsT saveRefIndicators = new JecnRefIndicatorsT();
			PropertyUtils.copyProperties(saveRefIndicators, jecnRefIndicatorsT);
			saveRefIndicators.setActivityId(figureId);
			saveRefIndicators.setId(null);
			flowStructureDao.getSession().save(saveRefIndicators);
		}
	}

	/**
	 * 输入和操作规范
	 * 
	 * @param listJecnActivityFileT
	 * @param figureId
	 */
	private void saveActivityFiles(List<JecnActivityFileT> listJecnActivityFileT, Long figureId) throws Exception {
		if (listJecnActivityFileT == null) {
			return;
		}
		for (JecnActivityFileT jecnActivityFileT : listJecnActivityFileT) {
			JecnActivityFileT saveActivityFile = new JecnActivityFileT();
			PropertyUtils.copyProperties(saveActivityFile, jecnActivityFileT);
			saveActivityFile.setFigureId(figureId);
			saveActivityFile.setFileId(null);
			flowStructureDao.getSession().save(saveActivityFile);
		}
	}

	/**
	 * 保存输出
	 * 
	 * @param listModeFileT
	 * @param figureId
	 */
	private void saveModeFiles(List<JecnModeFileT> listModeFileT, Long figureId) throws Exception {
		if (listModeFileT == null) {
			return;
		}
		for (JecnModeFileT jecnModeFileT : listModeFileT) {
			JecnModeFileT saveModeFile = new JecnModeFileT();
			PropertyUtils.copyProperties(saveModeFile, jecnModeFileT);
			saveModeFile.setFigureId(figureId);
			saveModeFile.setModeFileId(null);
			flowStructureDao.getSession().save(saveModeFile);
			// 输出样例保存
			saveTemplets(jecnModeFileT.getListJecnTempletT(), saveModeFile.getModeFileId());
		}
	}

	/**
	 * 保存新版 输入
	 * 
	 * @param listModeFileT
	 * @param figureId
	 */
	private void saveNewEditionInFiles(List<JecnFigureInoutT> listInFiles, Long figureId, Long flowId) throws Exception {
		if (listInFiles == null) {
			return;
		}
		for (JecnFigureInoutT jecnModeFileT : listInFiles) {
			JecnFigureInoutT saveModeFile = new JecnFigureInoutT();
			PropertyUtils.copyProperties(saveModeFile, jecnModeFileT);
			saveModeFile.setFigureId(figureId);
			saveModeFile.setId(JecnCommon.getUUID());
			saveModeFile.setFlowId(flowId);
			flowStructureDao.getSession().save(saveModeFile);
			// 新版输入 模板 保存
			saveOutSample(saveModeFile);
		}
	}

	/**
	 * 保存新版 输出
	 * 
	 * @param listModeFileT
	 * @param figureId
	 */
	private void saveNewEditionModeFiles(List<JecnFigureInoutT> listModeFileT, Long figureId, Long flowId)
			throws Exception {
		if (listModeFileT == null) {
			return;
		}
		for (JecnFigureInoutT jecnModeFileT : listModeFileT) {
			JecnFigureInoutT saveModeFile = new JecnFigureInoutT();
			PropertyUtils.copyProperties(saveModeFile, jecnModeFileT);
			saveModeFile.setFigureId(figureId);
			saveModeFile.setId(JecnCommon.getUUID());
			saveModeFile.setFlowId(flowId);
			flowStructureDao.getSession().save(saveModeFile);
			// 新版输出 模板 样例保存
			saveOutSample(saveModeFile);
		}
	}

	// 新版输出样例保存
	private void saveOutSample(JecnFigureInoutT figureInoutT) throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		if (figureInoutT.getListSampleT() != null && figureInoutT.getListSampleT().size() > 0) {
			for (JecnFigureInoutSampleT inoutSampleT : figureInoutT.getListSampleT()) {
				saveInOutSample(figureInoutT, inoutSampleT);
			}
		}
	}

	// 新版输出样例保存
	private void saveInOutSample(JecnFigureInoutT figureInoutT, JecnFigureInoutSampleT inoutSampleT)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		JecnFigureInoutSampleT sam = new JecnFigureInoutSampleT();
		PropertyUtils.copyProperties(sam, inoutSampleT);
		sam.setInoutId(figureInoutT.getId());
		sam.setFlowId(figureInoutT.getFlowId());
		this.flowStructureDao.getSession().save(sam);
	}

	/**
	 * 输出样例保存
	 * 
	 * @param listJecnTempletT
	 * @param modeFileId
	 */
	private void saveTemplets(List<JecnTempletT> listJecnTempletT, Long modeFileId) throws Exception {
		if (listJecnTempletT == null) {
			return;
		}
		for (JecnTempletT jecnTempletT : listJecnTempletT) {
			JecnTempletT saveTemplet = new JecnTempletT();
			PropertyUtils.copyProperties(saveTemplet, jecnTempletT);
			saveTemplet.setModeFileId(modeFileId);
			saveTemplet.setId(null);
			flowStructureDao.getSession().save(saveTemplet);
		}
	}

	/**
	 * 角色与岗位关联
	 * 
	 * @param listRolePosition
	 * @param figureId
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	private void saveRoleRelatedPostions(List<JecnFlowStationT> listRolePosition, Long figureId) throws Exception {
		if (listRolePosition == null) {
			return;
		}
		for (JecnFlowStationT jecnFlowStationT : listRolePosition) {
			JecnFlowStationT addFlowStation = new JecnFlowStationT();
			PropertyUtils.copyProperties(addFlowStation, jecnFlowStationT);
			addFlowStation.setFigureFlowId(figureId);
			addFlowStation.setFlowStationId(null);
			flowStructureDao.getSession().save(addFlowStation);
		}
	}

	/**
	 * 相关附件集合
	 * 
	 * @param listJecnFigureFileTBean
	 * @param figureId
	 */
	private void saveFigureFiles(List<JecnFigureFileTBean> listJecnFigureFileTBean, Long figureId) throws Exception {
		if (listJecnFigureFileTBean == null) {
			return;
		}
		for (JecnFigureFileTBean jecnFigureFileTBean : listJecnFigureFileTBean) {
			JecnFigureFileTBean addJecnFigureFileTBean = new JecnFigureFileTBean();
			PropertyUtils.copyProperties(addJecnFigureFileTBean, jecnFigureFileTBean);

			addJecnFigureFileTBean.setFigureId(figureId);
			addJecnFigureFileTBean.setId(JecnCommon.getUUID());
			flowStructureDao.getSession().save(addJecnFigureFileTBean);
		}
	}

	/**
	 * 更新tpath 和tlevel
	 * 
	 * @param jecnFlowStructureT
	 * @throws Exception
	 */
	public void updateTpathAndTLevel(IFlowStructureDao flowStructureDao, JecnFlowStructureT jecnFlowStructureT)
			throws Exception {
		// 获取上级节点
		JecnFlowStructureT preFlow = flowStructureDao.get(jecnFlowStructureT.getPerFlowId());
		if (preFlow == null) {
			jecnFlowStructureT.settLevel(0);
			jecnFlowStructureT.settPath(jecnFlowStructureT.getFlowId() + "-");
			jecnFlowStructureT.setViewSort(JecnUtil.concatViewSort("", jecnFlowStructureT.getSortId().intValue()));
		} else {
			jecnFlowStructureT.settLevel(preFlow.gettLevel() + 1);
			jecnFlowStructureT.settPath(preFlow.gettPath() + jecnFlowStructureT.getFlowId() + "-");
			jecnFlowStructureT.setViewSort(JecnUtil.concatViewSort(preFlow.getViewSort(), jecnFlowStructureT
					.getSortId().intValue()));
		}
		// 更新
		flowStructureDao.update(jecnFlowStructureT);
		flowStructureDao.getSession().flush();
	}
}
