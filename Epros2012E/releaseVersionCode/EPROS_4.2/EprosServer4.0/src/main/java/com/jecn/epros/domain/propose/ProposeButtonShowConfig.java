package com.jecn.epros.domain.propose;

/**
 * 合理化建议页面显示配置项
 *
 * @author huoyl
 */
public class ProposeButtonShowConfig {
    /**
     * 采纳按钮是否显示
     */
    private boolean acceptIsShow = false;
    /**
     * 采纳按钮是否置灰
     */
    private boolean acceptIsGray = false;

    /**
     * 取消采纳按钮是否显示
     */
    private boolean cancelAcceptIsShow = false;
    /**
     * 取消采纳按钮是否置灰
     */
    private boolean cancelAcceptIsGray = false;

    /**
     * 回复按钮是否显示
     */
    private boolean replayIsShow = false;
    /**
     * 回复按钮是否置灰
     */
    private boolean replayIsGray = false;

    /**
     * 已读按钮是否显示
     */
    private boolean readIsShow = false;
    /**
     * 已读按钮是否置灰
     */
    private boolean readIsGray = false;

    /**
     * 编辑按钮是否显示
     */
    private boolean editIsShow = false;
    /**
     * 编辑按钮是否置灰
     */
    private boolean editIsGray = false;

    /**
     * 删除按钮是否置灰
     */
    private boolean delIsShow = false;
    /**
     * 删除按钮是否置灰
     */
    private boolean delIsGray = false;
    /**
     * 是否有权限可以删除回复的信息
     */
    private boolean curPeopelIsCanDelAnswer = false;

    public boolean isCurPeopelIsCanDelAnswer() {
        return curPeopelIsCanDelAnswer;
    }

    public void setCurPeopelIsCanDelAnswer(boolean curPeopelIsCanDelAnswer) {
        this.curPeopelIsCanDelAnswer = curPeopelIsCanDelAnswer;
    }

    public boolean isAcceptIsShow() {
        return acceptIsShow;
    }

    public void setAcceptIsShow(boolean acceptIsShow) {
        this.acceptIsShow = acceptIsShow;
    }

    public boolean isAcceptIsGray() {
        return acceptIsGray;
    }

    public void setAcceptIsGray(boolean acceptIsGray) {
        this.acceptIsGray = acceptIsGray;
    }

    public boolean isCancelAcceptIsShow() {
        return cancelAcceptIsShow;
    }

    public void setCancelAcceptIsShow(boolean cancelAcceptIsShow) {
        this.cancelAcceptIsShow = cancelAcceptIsShow;
    }

    public boolean isCancelAcceptIsGray() {
        return cancelAcceptIsGray;
    }

    public void setCancelAcceptIsGray(boolean cancelAcceptIsGray) {
        this.cancelAcceptIsGray = cancelAcceptIsGray;
    }

    public boolean isReplayIsShow() {
        return replayIsShow;
    }

    public void setReplayIsShow(boolean replayIsShow) {
        this.replayIsShow = replayIsShow;
    }

    public boolean isReplayIsGray() {
        return replayIsGray;
    }

    public void setReplayIsGray(boolean replayIsGray) {
        this.replayIsGray = replayIsGray;
    }

    public boolean isReadIsShow() {
        return readIsShow;
    }

    public void setReadIsShow(boolean readIsShow) {
        this.readIsShow = readIsShow;
    }

    public boolean isReadIsGray() {
        return readIsGray;
    }

    public void setReadIsGray(boolean readIsGray) {
        this.readIsGray = readIsGray;
    }

    public boolean isEditIsShow() {
        return editIsShow;
    }

    public void setEditIsShow(boolean editIsShow) {
        this.editIsShow = editIsShow;
    }

    public boolean isEditIsGray() {
        return editIsGray;
    }

    public void setEditIsGray(boolean editIsGray) {
        this.editIsGray = editIsGray;
    }

    public boolean isDelIsShow() {
        return delIsShow;
    }

    public void setDelIsShow(boolean delIsShow) {
        this.delIsShow = delIsShow;
    }

    public boolean isDelIsGray() {
        return delIsGray;
    }

    public void setDelIsGray(boolean delIsGray) {
        this.delIsGray = delIsGray;
    }


}
