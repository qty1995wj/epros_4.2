package com.jecn.svg.dataModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SvgPanel {

	private List<SvgBaseGraph> baseGraphs;

	private double width;
	private double height;

	private double roleMoveEndX;
	private String flowName;
	/** 图片下载显示的名称 **/
	private String showName;
	private String preFlowId;
	private String flowId;
	private boolean isVerticalFlow;
	private String preFlowType;
	private String flowImagePath;
	private PanelType panelType;
	private String flowIdInput;

	/** 服务地址（svg显示中元素跳转IP地址） */
	private String httpURL;
	/** SVG中URL中项目名（新服务） */
	private String projectName;
	/** 0: 未发布，1：发布 */
	private int isPub;

	// EPROS config表中的配置映射
	private Map<String, String> eproConfigItem = new HashMap<String, String>();

	public enum PanelType {
		FLOW, FLOW_MAP, DEPT, INTEGRATION_CHART
	}

	public String getFlowName() {
		return flowName;
	}

	public SvgPanel setFlowName(String flowName) {
		this.flowName = flowName;
		return this;
	}

	public String getPreFlowId() {
		return preFlowId;
	}

	public SvgPanel setPreFlowId(String preFlowId) {
		this.preFlowId = preFlowId;
		return this;
	}

	public boolean getIsVerticalFlow() {
		return isVerticalFlow;
	}

	public SvgPanel setIsVerticalFlow(boolean isVerticalFlow) {
		this.isVerticalFlow = isVerticalFlow;
		return this;
	}

	public String getFlowImagePath() {
		return flowImagePath;
	}

	public SvgPanel setFlowImagePath(String flowImagePath) {
		this.flowImagePath = flowImagePath;
		return this;
	}

	public double getWidth() {
		return width;
	}

	public SvgPanel setWidth(double width) {
		this.width = width;
		return this;
	}

	public double getHeight() {
		return height;
	}

	public SvgPanel setHeight(double height) {
		this.height = height;
		return this;
	}

	public List<SvgBaseGraph> getBaseGraphs() {
		return baseGraphs;
	}

	public SvgPanel setBaseGraphs(List<SvgBaseGraph> baseGraphs) {
		this.baseGraphs = baseGraphs;
		return this;
	}

	public double getRoleMoveEndX() {
		return roleMoveEndX;
	}

	public SvgPanel setRoleMoveEndX(double roleMoveEndX) {
		this.roleMoveEndX = roleMoveEndX;
		return this;
	}

	public String getPreFlowType() {
		return preFlowType;
	}

	public SvgPanel setPreFlowType(String preFlowType) {
		this.preFlowType = preFlowType;
		return this;
	}

	public PanelType getPanelType() {
		return panelType;
	}

	public void setPanelType(PanelType panelType) {
		this.panelType = panelType;
	}

	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public String getFlowIdInput() {
		return flowIdInput;
	}

	public void setFlowIdInput(String flowIdInput) {
		this.flowIdInput = flowIdInput;
	}

	public void setVerticalFlow(boolean isVerticalFlow) {
		this.isVerticalFlow = isVerticalFlow;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public void addGraph(SvgBaseGraph graph) {
		if (graph == null) {
			return;
		}
		baseGraphs.add(graph);
	}

	public String getHttpURL() {
		return httpURL;
	}

	public void setHttpURL(String httpURL) {
		this.httpURL = httpURL;
	}

	public int getIsPub() {
		return isPub;
	}

	public void setIsPub(int isPub) {
		this.isPub = isPub;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Map<String, String> getEproConfigItem() {
		return eproConfigItem;
	}

	public void setEproConfigItem(Map<String, String> eproConfigItem) {
		this.eproConfigItem = eproConfigItem;
	}

}
