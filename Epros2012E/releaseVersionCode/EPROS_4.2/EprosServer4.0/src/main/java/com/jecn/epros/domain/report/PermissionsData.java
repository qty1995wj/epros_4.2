package com.jecn.epros.domain.report;

/**
 * Created by Administrator on 2018/9/30.
 */
public class PermissionsData {

    private Long id;
    private String name;
    private String accessType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }
}
