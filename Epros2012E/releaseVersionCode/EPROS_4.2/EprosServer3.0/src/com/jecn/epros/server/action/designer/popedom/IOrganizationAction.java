package com.jecn.epros.server.action.designer.popedom;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.DownloadPopedomBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnOpenOrgBean;
import com.jecn.epros.server.bean.popedom.JecnPositionFigInfo;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.popedom.PositonInfo;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface IOrganizationAction {

	/**
	 * @author yxw 2012-5-22
	 * @description: 树加载 根据组织ID加载子节点
	 * @param pId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildOrgs(Long pId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:树加载 所有的组织,只加载组织
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllOrgs(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:树加载 所有的组织和岗位
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllOrgAndPosition(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:树加载 根据组织ID加载子节点，包括组织与岗位
	 * @param id
	 * @param projectId
	 * @param isLeaf
	 *            是否让岗位为叶子节点 :要查询人员 false只查到岗位
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildOrgsAndPositon(Long id, Long projectId, boolean isLeaf) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:组织树右键增加组织
	 * @param name
	 * @param pId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public Long addOrg(String name, Long pId, String orgNumber, Long projectId, int sortId, String orgNum)
			throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:组织树增加岗位
	 * @param name
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	public Long addPosition(JecnFlowOrgImage jecnFlowOrgImage) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:根据组织ID获取组织详情
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnFlowOrg getOrgInfo(Long id) throws Exception;

	/**
	 * @author yxw 2012-6-8
	 * @description:更新组织职责
	 * @param orgId
	 * @param duty
	 * @throws Exception
	 */
	public void updateOrgDuty(Long orgId, String duty, String orgNum) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:根据岗位ID获取岗位详情
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public PositonInfo getPositionInfo(Long id) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:组织重命名
	 * @param id
	 * @param newName
	 * @throws Exception
	 */
	public void reNameOrg(Long id, String newName) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:岗位重命名
	 * @param id
	 * @param newName
	 * @throws Exception
	 */
	public void reNamePosition(Long id, String newName) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点排序
	 * @param list
	 * @param pId
	 * @param projectId
	 * @throws Exception
	 */
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:组织节点移动
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodesOrg(List<Long> listIds, Long pId, String orgNumber, Long updatePersonId,
			TreeNodeType moveNodeType) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:岗位节点移动
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodesPos(List<Long> listIds, Long pId, String posNumber, Long updatePersonId,
			TreeNodeType moveNodeType) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:删除组织
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteOrgs(List<Long> listIds, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:删除岗位
	 * @param listIds
	 * @throws Exception
	 */
	public void deletePosition(List<Long> listIds) throws Exception;

	/**
	 * @author yxw 2012-6-20
	 * @description:根据名称查询岗位
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchPositionByName(String name, Long projectId) throws Exception;
	
	/**
	 * @author yxw 2012-6-20
	 * @description:根据名称(岗位，部门，人员)查询岗位
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchPositionByName(Map<String,Object> nameMap, Long projectId) throws Exception;


	/**
	 * @author yxw 2012-6-15
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的组织权限
	 * @param relateId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getOrgAccessPermissions(Long relateId, int type) throws Exception;

	/**
	 * @author yxw 2012-6-15
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的岗位权限
	 * @param relateId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPositionAccessPermissions(Long relateId, int type) throws Exception;

	/**
	 * @author yxw 2012-6-8
	 * @description:岗位属性更新
	 * @param positionInfoBean
	 * @param upPositionIds
	 * @param lowerPositionIds
	 * @param innerRuleIds
	 * @param outRuleIds
	 * @throws Exception
	 */
	public void updatePosition(JecnPositionFigInfo positionInfoBean, String upPositionIds, String lowerPositionIds,
			String innerRuleIds, String outRuleIds, Long posId, String posName, String posNum) throws Exception;

	/**
	 * 根据组织ID查出父节点及兄弟节点，用于树定位
	 * 
	 * @author yxw 2012-8-2
	 * @description:
	 * @param orgId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodesOrg(Long orgId, Long projectId) throws Exception;

	/**
	 * 
	 * @author yxw 2012-8-2
	 * @description:根据岗位ID查出父节点及兄弟节点，用于树定位
	 * @param orgId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodesPos(Long posId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-8-14
	 * @description: 判断组织图能否打开
	 * @param userId
	 *            登录用户ID
	 * @param id
	 *            组织ID
	 * @return true 可以打开 false 不可打开
	 * @throws Exception
	 */
	public boolean updateOrgCanOpen(Long userId, Long id) throws Exception;

	/**
	 * @author yxw 2012-8-14
	 * @description:组织图解除编辑
	 * @param id
	 * @throws Exception
	 */
	public void moveEdit(Long id) throws Exception;

	/**
	 * @author yxw 2012-8-8
	 * @description:打开组织面板
	 * @param orgId
	 * @return
	 * @throws Exception
	 */
	public JecnOpenOrgBean openOrgMap(long orgId) throws Exception;

	/**
	 * @author yxw 2012-8-8
	 * @description:组织图保存
	 * @param jecnFlowOrg
	 * @param saveList
	 * @param updateList
	 * @param deleteListLine
	 * @param deleteList
	 * @throws Exception
	 */
	public void saveOrgMap(JecnFlowOrg jecnFlowOrg, List<JecnFlowOrgImage> saveList, List<JecnFlowOrgImage> updateList,
			List<Long> deleteListLine, List<Long> deleteList) throws Exception;

	/**
	 * 
	 * @author zhangchen Aug 6, 2012
	 * @description： 回收站-获得项目下删除的组织
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getDelsOrg(Long projectId) throws Exception;

	/**
	 * 
	 * @author zhangchen Aug 6, 2012
	 * @description：回收站-恢复删除
	 * @param id
	 * @throws Exception
	 */
	public void updateRecoverDelOrg(List<Long> ids) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：回收站--删除组织
	 * @param setIds
	 * @throws Exception
	 */
	public Set<Long> deleteIdsOrg(List<Long> ListIds, Long updatePeopleId) throws Exception;

	/**
	 * @author zhangchen Aug 7, 2012
	 * @description：获得父类节点的信息（包括子节点）（查看删除的路径）
	 * @param projectId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getParentsContainSelf(Long projectId, Long id) throws Exception;

	/**
	 * @description:根据组织父IDPID获取组织详情
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowOrg> getOrgInfos(Long pId) throws Exception;

	/**
	 * @author yxw 2012-10-24
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的查阅权限和（密级）是否公开
	 * @param relateId
	 * @param type0是流程1是文件2是标准3制度
	 * @return
	 * @throws Exception
	 */
	public LookPopedomBean getAccessPermissions(Long relateId, int type) throws Exception;

	/**
	 * @author yxw 2012-11-8
	 * @description:根据资源ID(流程、文件、标准、制度)查询此相关的查阅权限（不包括密级）
	 * @param relateId
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public LookPopedomBean getOnlyAccessPermissions(Long relateId, int type) throws Exception;

	/**
	 * 
	 * @author yxw 2012-10-25
	 * @description: 查阅权限 是否公开修改
	 * @param relateId
	 * @param type
	 *            0是流程，1是文件，2是标准，3是制度
	 * @param isPublic
	 *            密级
	 * @param orgIds
	 *            组织ids
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param posIds
	 *            岗位ids
	 * @param posType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0：管理员
	 * @param projectId
	 * @param saveMode
	 *            保存模式 0默认是完全覆盖 1添加 2删除
	 * @param userId 
	 * @throws Exception
	 */
	public void saveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType,  int orgType,  int posType, int userType, Long projectId,
			 int posGroupType, int saveMode,AccessId accId, Long userId) throws Exception;
	/**
	 * 
	 * @author yxw 2012-10-25
	 * @description: 查阅权限 是否公开修改
	 * @param relateId
	 * @param type
	 *            0是流程，1是文件，2是标准，3是制度
	 * @param isPublic
	 *            密级
	 * @param orgIds
	 *            组织ids
	 * @param orgType
	 *            0:本节点1：包括子节点
	 * @param posIds
	 *            岗位ids
	 * @param posType
	 *            0:本节点1：包括子节点
	 * @param userType
	 *            0：管理员
	 * @param projectId
	 * @param saveMode
	 *            保存模式 0默认是完全覆盖 1添加 2删除
	 * @throws Exception
	 */
	public void saveOrUpdateAccessPermissions(Long relateId, int type, TreeNodeType nodeType, long isPublic,
			int securityType, String orgIds, int orgType, String posIds, int posType, int userType, Long projectId,
			String posGroupIds, int posGroupType, int saveMode) throws Exception;

	/**
	 * @author yxw 2013-1-15
	 * @description:根据ids查询岗位的详细信息
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPositionsByIds(String ids) throws Exception;

	/***
	 * 验证岗位编号全表唯一
	 * 
	 * @param posNum
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public boolean validateAddUpdatePosNum(String posNum, Long id) throws Exception;

	/***
	 * 验证组织编号全表唯一
	 * 
	 * @param posNum
	 * @param id
	 * @return
	 * @throws Exception
	 */

	public boolean validateAddUpdateOrgNum(String orgNum, Long id) throws Exception;

	// ------------------------------------新添加---------------------------
	/***
	 * 组织回收站：获取组织回收树节点数据
	 * 
	 * @param projectId
	 *            项目ID
	 * @param pid
	 *            父节点ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pid) throws Exception;

	/**
	 * 组织回收站：恢复选中的节点以及它的子节点
	 * 
	 * @param recycleNodesIdList
	 *            选中的节点的id的集合
	 * @param recycleNodesIdList
	 *            项目id
	 */
	public void recoverCurAndChild(List<Long> recycleNodesIdList, Long projectId) throws Exception;

	/**
	 * 组织回收站：恢复选中的节点（不包括它的子节点）
	 * 
	 * @param recycleNodesIdList
	 *            id的集合
	 */
	public void recoverCur(List<Long> recycleNodesIdList) throws Exception;

	/**
	 * 组织回收站：真删选中的组织
	 * 
	 * @param orgIds
	 *            需要删除的组织的id(选中的组织id)
	 * @param projectId
	 *            项目id
	 * @param userId
	 *            人员id
	 */
	public void trueDelete(List<Long> ListIds, Long updatePeopleId) throws Exception;

	/**
	 * 组织回收站：搜索，获得删除的并且有权限的组织的集合（暂时只有管理员可以操作）
	 * 
	 * @param name
	 *            搜索的项目名称
	 * @param projectId
	 *            项目id
	 * @return
	 */
	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId) throws Exception;

	/**
	 * 组织回收站:定位
	 * 
	 * @param id
	 *            需要定位的节点的id
	 * @param projectId
	 *            项目id
	 * @return
	 */
	public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception;

	/**
	 * 获得岗位的JECNTREEBEAN
	 * 
	 * @param posId
	 * @return
	 * @throws Exception
	 */
	public JecnTreeBean getJecnTreeBeanByposId(Long posId) throws Exception;

	public List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception;

	public JecnTreeBean getCurrentOrg(Long startId, Long projectId) throws Exception;

	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId, Long startId) throws Exception;

	public JecnTreeBean getCurrentOrgContainDel(Long startId, Long projectId) throws Exception;

	public List<JecnTreeBean> searchPositionByName(String name, Long projectId, Long startId) throws Exception;

	public DownloadPopedomBean getFlowFileDownloadPermissions(Long id) throws Exception;

	public void saveOrUpdateFlowFileDownloadPermissions(Long id, String posaIds) throws Exception;

	@Deprecated
	public boolean peopleHasFlowFileDownloadPerm(Set<Long> flowSet, Long userId) throws Exception;

	List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception;

	List<JecnTreeBean> getRoleAuthChildPersons(Long pId, Long projectId, Long peopleId) throws Exception;

	List<JecnTreeBean> searchByName(String name, Long projectId) throws Exception;
	
	public List<JecnDictionary> getDictionarys(DictionaryEnum name)throws Exception;

	public AccessId getPepoleOrgAndPosAndPosPosGroup(long userId,Long projectId);

}
