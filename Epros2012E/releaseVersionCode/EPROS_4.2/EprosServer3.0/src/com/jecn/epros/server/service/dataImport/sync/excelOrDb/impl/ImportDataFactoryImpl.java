package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.List;

import com.jecn.epros.server.bean.dataimport.BasePosBean;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.ImportDataFactory;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.convert.ReadUserConfigXml;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class ImportDataFactoryImpl implements ImportDataFactory {
	/** 导入方式（excel、xml、hessian），默认是excel导入 */
	private ImportType importType = ImportType.excel;

	/**
	 * 
	 * 获取待导入数据
	 * 
	 * @param importType
	 *            导入方式（excel、xml、hessian），默认是excel导入
	 * @return BaseBean 待导入的部门、岗位以及人员
	 * @throws Exception
	 */
	public BaseBean getImportData(String importType) throws Exception {
		// 判断导入方式是否是给定的方式
		checkImpotType(importType);

		return readUserData();
	}

	/**
	 * 
	 * 获取配置文件之值
	 * 
	 * @param importType
	 *            String
	 * @return
	 */
	public AbstractConfigBean getInitTime(String importType) throws Exception {
		// 判断导入方式是否是给定的方式
		checkImpotType(importType);

		return ReadUserConfigXml.getAbstractConfigBean(this.importType);
	}

	/**
	 * 
	 * 判断导入方式是否是给定的方式
	 * 
	 * @param importType
	 */
	private void checkImpotType(String importType) {
		if (SyncConstant.IMPORT_TYPE_EXCEL.equals(importType)) {// excel
			this.importType = ImportType.excel;
		} else if (SyncConstant.IMPORT_TYPE_XML.equals(importType)) {// xml
			this.importType = ImportType.xml;
		} else if (SyncConstant.IMPORT_TYPE_HESSIAN.equals(importType)) {// hessian
			this.importType = ImportType.hessian;
		} else if (SyncConstant.IMPORT_TYPE_DB.equals(importType)) {// db
			this.importType = ImportType.db;
		} else if (SyncConstant.IMPORT_TYPE_WEBSERVICE.equals(importType)) {// webService
			this.importType = ImportType.webService;
		} else if (SyncConstant.IMPORT_TYPE_SAP_HR.equals(importType)) {// 通过Sap连接获取HR数据
			this.importType = ImportType.sapHr;
		} else if (SyncConstant.IMPORT_TYPE_AD.equals(importType)) { // AD域读取数据
			this.importType = ImportType.adDomain;
		} else if (SyncConstant.IMPORT_TYPE_BASE_DB.equals(importType)) { // DB数据导入附带基准岗位
			this.importType = ImportType.baseDB;
		} else {
			throw new IllegalArgumentException(SyncErrorInfo.IMPORT_TYPE_ERROR);
		}
	}

	/**
	 * 
	 * 读取数据，Hessian
	 * 
	 * @return
	 * @throws BsException
	 */
	private BaseBean readUserData() throws Exception {
		BaseBean baseBean = null;
		if (ImportType.hessian.equals(importType)) {// Hessian读取数据方式
			baseBean = proHessian();
		} else if (ImportType.excel.equals(importType)) {// excel读取方式
			baseBean = proExcel();
		} else if (ImportType.db.equals(importType) || ImportType.baseDB.equals(importType)) {// 数据库读取方式
			baseBean = proDB();
		} else if (ImportType.webService.equals(importType)) {// webService读取方式
			baseBean = proWebService();
		} else if (ImportType.sapHr.equals(importType)) {// 通过sapHr读取方式
			baseBean = proSapHr();
		} else if (ImportType.adDomain.equals(importType)) {// 通过AD域读取数据
			baseBean = proADDomain();
		} else if (ImportType.xml.equals(importType)) {// 通过AD域读取数据
			baseBean = proXML();
		}
		return baseBean;
	}

	private BaseBean proADDomain() throws Exception {
		if (!ImportType.adDomain.equals(importType)) {
			return null;
		}
		// 获取配置文件内容
		AbstractConfigBean configBean = ReadUserConfigXml.getAbstractConfigBean(importType);

		if (SyncTool.isNullObj(configBean)) {
			return null;
		}

		AbstractImportUser db = new ImportUserByAD();
		// 获取部门、岗位、人员数据 存储到BaseBean对象中
		return createBaseBean(db);
	}

	/**
	 * 
	 * 基于DB方式获取部门、岗位、人员数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public BaseBean proDB() throws Exception {

		if (!ImportType.db.equals(importType) && !ImportType.baseDB.equals(importType)) {
			return null;
		}
		// 获取配置文件内容
		AbstractConfigBean configBean = ReadUserConfigXml.getAbstractConfigBean(importType);

		if (SyncTool.isNullObj(configBean)) {
			return null;
		}

		AbstractImportUser db = null;
		// otherUserSyncType 1:Excel
		// 2:DB;3:岗位匹配,4:JSON、webservice）8：DB附带基准岗位同步,9:岗位组导入
		// DB方式读取用户数据对象
		if (ImportType.baseDB.equals(importType)) { // 农夫山泉附带基准岗位同步
			db = new ImportBasePos(configBean);
		} else if (JecnContants.otherUserSyncType == 9) {// 岗位组
			if (JecnContants.otherLoginType == 11) {// 29所
				db = new ImportUserByCetc29DB(configBean); // 29所
			} else if (JecnContants.otherLoginType == 40) {// 科大讯飞
				db = new ImportIflytekUserByJson(configBean);
			} else {
				db = new ImportPosGroupByDB(configBean);
			}
		} else {
			db = new ImportUserByDB(configBean);
		}

		// 获取部门、岗位、人员数据 存储到BaseBean对象中
		return createBaseBean(db);
	}

	/**
	 * 
	 * 基于excel方式获取部门、岗位、人员数据
	 * 
	 * @return
	 * @throws Exception
	 */
	private BaseBean proExcel() throws Exception {

		// excel方式读取用户数据对象
		ImportUserByExcel excel = new ImportUserByExcel();

		return createBaseBean(excel);
	}

	/**
	 * 
	 * 基于hessian方式获取部门、岗位、人员数据
	 * 
	 * @return
	 * @throws Exception
	 */
	private BaseBean proHessian() throws Exception {

		// 获取配置文件内容
		AbstractConfigBean configBean = ReadUserConfigXml.getAbstractConfigBean(importType);

		if (SyncTool.isNullObj(configBean)) {
			return null;
		}

		// hessian方式读取用户数据对象
		ImportUserByHessian hess = new ImportUserByHessian(configBean);

		// 获取部门、岗位、人员数据 存储到BaseBean对象中
		return createBaseBean(hess);
	}

	/**
	 * 
	 * 基于webService方式获取部门、岗位、人员数据
	 * 
	 * @return
	 * @throws Exception
	 */
	private BaseBean proWebService() throws Exception {
		// 获取配置文件内容
		AbstractConfigBean configBean = ReadUserConfigXml.getAbstractConfigBean(importType);

		if (SyncTool.isNullObj(configBean)) {
			return null;
		}

		// hessian方式读取用户数据对象
		AbstractImportUser webService = getResultByWebService(configBean);

		return createBaseBean(webService);
	}

	/**
	 * webservice方式人员同步-获取数据接口 数据库类型 4
	 * 
	 * @param configBean
	 * @return
	 */
	private AbstractImportUser getResultByWebService(AbstractConfigBean configBean) {
		AbstractImportUser webService = null;
		if (JecnContants.otherLoginType == 23 || JecnContants.otherLoginType == 39) {// 中车
			webService = new ImportUserByWbServiceZhuZhouSDDQ(configBean);
		} else if (JecnContants.otherLoginType == 20) {// 中石化-石勘院
			webService = new ImportUserByWbServiceZhongsh(configBean);
		} else if (JecnContants.otherLoginType == 42) {
			webService = new ImportCMSUserByJson(configBean);
		} else if (JecnContants.otherLoginType == 47) { // 九新药业
			webService = new ImportUserByWbServiceNine999(configBean);
		} else if (JecnContants.otherLoginType == 48) { // 贝豪婴童
			webService = new ImportUserByBaho(configBean);
		} else if (JecnContants.otherLoginType == 49) {
//			webService = new ImportPowerLongUserByJson(configBean);
		} else if (JecnContants.otherLoginType == 37) {// 蒙牛
			webService = new ImportMengNiuUserByJson(configBean);
		} else if (JecnContants.otherLoginType == 54) {// 岭南股份
			webService = new ImportLingNamUserByJson(configBean);
		} else {// 默认南京石化
			webService = new ImportUserByJson(configBean);
		}
		return webService;
	}

	/**
	 * 
	 * 基于webService方式获取部门、岗位、人员数据
	 * 
	 * @return
	 * @throws Exception
	 */
	private BaseBean proSapHr() throws Exception {
		// 获取配置文件内容
		AbstractConfigBean configBean = ReadUserConfigXml.getAbstractConfigBean(importType);

		if (SyncTool.isNullObj(configBean)) {
			return null;
		}

		// hessian方式读取用户数据对象
		ImportUserBySapHr sapHr = new ImportUserBySapHr(configBean);

		return createBaseBean(sapHr);
	}

	/**
	 * 
	 * 创建basebean对象
	 * 
	 * @param importUser
	 * @return
	 * @throws BsException
	 */
	private BaseBean createBaseBean(AbstractImportUser importUser) throws Exception {

		// if (importUser != null && importUser instanceof ImportUserAdByDB) {
		// List<UserDomainNameBean>
		// domainNameBeans=((ImportUserAdByDB)importUser).getDomainUser();
		// }
		// 部门
		List<DeptBean> deptList = importUser.getDeptBeanList();
		// 人员
		List<UserBean> userList = importUser.getUserBeanList();

		if (importType.equals(ImportType.baseDB)) {// 基准岗位同步
			List<BasePosBean> posBeanList = ((ImportBasePos) importUser).getBasePosBeanList();
			return new BaseBean(deptList, userList, posBeanList, null);
		} else if (JecnContants.otherUserSyncType == 9) {// 岗位组同步
			return new BaseBean(deptList, userList, importUser.getPosGroups(), importUser.getGroupRelatedPos());
		}
		// 获取部门、岗位、人员数据 存储到BaseBean对象中
		return new BaseBean(deptList, userList);

	}

	/**
	 * 
	 * 基于xml方式获取部门、岗位、人员数据
	 * 
	 * @return
	 * @throws Exception
	 */
	private BaseBean proXML() throws Exception {

		// 获取配置文件内容
		AbstractConfigBean configBean = ReadUserConfigXml.getAbstractConfigBean(importType);

		if (SyncTool.isNullObj(configBean)) {
			return null;
		}

		// XML方式读取用户数据对象
		ImportUserByXML xmlUser = new ImportUserByXML(configBean);

		// 获取部门、岗位、人员数据 存储到BaseBean对象中
		return createBaseBean(xmlUser);
	}

	/**
	 * 
	 * 外部获取数据方式
	 * 
	 */
	public enum ImportType {
		hessian, // 读取hessian接口 访问方式
		xml, // 读取xml中数据访问方式
		excel, // 读取excel访问方式
		db, // 读取数据库访问方式
		webService, // webservice访问方式(南京石化)
		sapHr, // 通过sap对接获取HR数据(华帝)
		adDomain, // 通过AD域获取数据
		none, baseDB
		// 读取数据库附带基准岗位
	}

}
