package com.jecn.epros.domain.report;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程信息化统计
 *
 * @author hyl
 */
public class InformationStatistic extends ReportBaseBean {

    private List<Part> parts = new ArrayList<Part>();

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }


}
