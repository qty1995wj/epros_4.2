package com.jecn.epros.server.action.web.login.ad.mengniu;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.HttpRequestUtil;

public class MengNiuLoginAction extends JecnAbstractADLoginAction {

	public final static String TICKET = "Ticket";

	public MengNiuLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	static {
		MengNiuAfterItem.start();
	}

	public String login() {
		String userCode = this.loginAction.getRequest().getParameter("userCode");
		if (StringUtils.isNotBlank(userCode)) {
			return loginByLoginName(userCode);
		}

		String ticket = this.loginAction.getRequest().getParameter(TICKET);
		if (StringUtils.isBlank(ticket)) {
			return LoginAction.INPUT;
		}

		// 获取URL请求的地址
		String response = HttpRequestUtil.getJsonHttpRequest(MengNiuAfterItem.SSO_ADDRESS, "Ticket=" + ticket);
		log.info("resultJson = " + response);

		if (StringUtils.isBlank(response)) {
			return LoginAction.INPUT;
		}
		// 获取返回状态
		String status = JSONObject.fromObject(response).getString("Status");
		if ("true".equals(status)) {
			String loginName = JSONObject.fromObject(response).getJSONObject("Data").getString("UserID");
			return loginByLoginName(loginName);
		} else {
			log.error(JSONObject.fromObject(response).getString("ErrorResult"));
			return LoginAction.INPUT;
		}
	}

}
