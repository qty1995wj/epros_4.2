/**
 * GetWorkfowNodeByUserResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetWorkfowNodeByUserResponse  implements java.io.Serializable {
    private java.lang.String getWorkfowNodeByUserResult;

    public GetWorkfowNodeByUserResponse() {
    }

    public GetWorkfowNodeByUserResponse(
           java.lang.String getWorkfowNodeByUserResult) {
           this.getWorkfowNodeByUserResult = getWorkfowNodeByUserResult;
    }


    /**
     * Gets the getWorkfowNodeByUserResult value for this GetWorkfowNodeByUserResponse.
     * 
     * @return getWorkfowNodeByUserResult
     */
    public java.lang.String getGetWorkfowNodeByUserResult() {
        return getWorkfowNodeByUserResult;
    }


    /**
     * Sets the getWorkfowNodeByUserResult value for this GetWorkfowNodeByUserResponse.
     * 
     * @param getWorkfowNodeByUserResult
     */
    public void setGetWorkfowNodeByUserResult(java.lang.String getWorkfowNodeByUserResult) {
        this.getWorkfowNodeByUserResult = getWorkfowNodeByUserResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetWorkfowNodeByUserResponse)) return false;
        GetWorkfowNodeByUserResponse other = (GetWorkfowNodeByUserResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getWorkfowNodeByUserResult==null && other.getGetWorkfowNodeByUserResult()==null) || 
             (this.getWorkfowNodeByUserResult!=null &&
              this.getWorkfowNodeByUserResult.equals(other.getGetWorkfowNodeByUserResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetWorkfowNodeByUserResult() != null) {
            _hashCode += getGetWorkfowNodeByUserResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetWorkfowNodeByUserResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetWorkfowNodeByUserResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getWorkfowNodeByUserResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GetWorkfowNodeByUserResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
