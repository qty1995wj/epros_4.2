package com.jecn.epros.domain;

/**
 * Created by thinkpad on 2017/1/3.
 */
public class TaskApproveBaseForm {

    private String operation;
    private int state;
    private String opinion;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

}
