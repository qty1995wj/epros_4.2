package epros.designer.gui.system.config.ui.property;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;

import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;

public class RadioData {
	private JLabel label = null;
	private JecnConfigRadioButton radioButton1 = null;
	private JecnConfigRadioButton radioButton2 = null;

	public RadioData(String lableText, String radio1Text, String radio2Text) {
		iniComponents();
		label.setText(lableText);
		radioButton1.setText(radio1Text);
		radioButton2.setText(radio2Text);
	}

	private void iniComponents() {
		label = new JLabel(JecnProperties.getValue("ruleResTypeC"));
		radioButton1 = new JecnConfigRadioButton(JecnProperties.getValue("person"));
		radioButton2 = new JecnConfigRadioButton(JecnProperties.getValue("position"));

		// 按钮实现 单选效果
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(radioButton1);
		bgpp.add(radioButton2);

		radioButton1.setOpaque(false);
		radioButton1.setSelected(true);
		radioButton2.setOpaque(false);
	}

	public void initLayout(JecnPanel otherPanel, Insets insets, int rowIndex) {
		GridBagConstraints c = new GridBagConstraints(0, rowIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		otherPanel.add(label, c);
		c = new GridBagConstraints(1, rowIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		otherPanel.add(radioButton1, c);
		c = new GridBagConstraints(2, rowIndex, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		otherPanel.add(radioButton2, c);
	}

	public JLabel getLabel() {
		return label;
	}

	public JecnConfigRadioButton getRadioButton1() {
		return radioButton1;
	}

	public JecnConfigRadioButton getRadioButton2() {
		return radioButton2;
	}

}
