package epros.draw.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.JecnUserInfo;

/**
 * 
 * XML操作工具类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnXmlUtil {
	private static final Log log = LogFactory.getLog(JecnXmlUtil.class);

	/**
	 * 
	 * XML特殊字符( &、"、'、<、>)转换方法
	 * 
	 * @param text
	 *            待存储到xml文件的字符串
	 * @return String 当参数为null或空字符串时不做转换返回参数字符串，其他情况返回转换后字符串
	 */
	public static String xmlEntities(String text) {
		if (text == null || "".equals(text)) {
			return text;
		}
		return text.replaceAll("&", "&amp;").replaceAll("\"", "&quot;")
				.replaceAll("'", "&prime;").replaceAll("<", "&lt;").replaceAll(
						">", "&gt;");
	}

	/**
	 * 获取Document对象
	 * 
	 * @return Document Document对象
	 */
	public static Document getDocument() {
		Document doc = null;
		// 从 XML 文档获取 DOM 文档实例
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
		} catch (ParserConfigurationException e) {
			log.error("JecnXmlUtil类getdocumnet方法异常：创建DocumentBuilder对象异常 ", e);
			return doc;
		}

		return doc;
	}

	/**
	 * 
	 * 通过文件全路径获取配置文件内容
	 * 
	 * @param allPath
	 *            String 文件全路径
	 * @return Document 文件內容解析成Document对象
	 */
	public static Document getDocumentByAllPath(String allPath) {
		// 文件全路径
		if (DrawCommon.isNullOrEmtryTrim(allPath)) {
			log.error("JecnXmlUtil类getDocumentByAllPath方法异常：文件路径为空。文件路径="
					+ allPath);
			return null;
		}

		File tempFile = new File(allPath);

		return getDocumentByFile(tempFile);
	}

	/**
	 * 
	 * 通过文件全路径获取配置文件内容
	 * 
	 * @param tempFile
	 *            File 带读取文件
	 * @return Document 文件內容解析成Document对象
	 */
	public static Document getDocumentByFile(File tempFile) {
		Document doc = null;
		if (!tempFile.exists() || !tempFile.isFile()) {// 文件不存在或者不是文件
			log.error("JecnXmlUtil类getDocumentByFile方法异常：找不到指定的文件。文件路径="
					+ tempFile.getName());
			return null;
		}

		// 从 XML 文档获取 DOM 文档实例
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			log.error(
					"JecnXmlUtil类getDocumentByFile方法异常：创建DocumentBuilder对象异常 ",
					e);
			return null;
		}

		try {
			// 解析文件
			doc = docBuilder.parse(tempFile);
		} catch (SAXException e) {
			log.error("JecnXmlUtil类getDocumentByFile方法异常：文件XML格式解析异常 文件路径="
					+ tempFile.getName(), e);
			return null;
		} catch (IOException e) {
			log.error("JecnXmlUtil类getDocumentByFile方法异常：文件读取异常 文件路径="
					+ tempFile.getName(), e);
			return null;
		}
		return doc;
	}

	/**
	 * 
	 * 获取配置文件内容
	 * 
	 * @param fileName
	 *            String 文件路径
	 * @return Document 文件內容解析成Document对象
	 */
	public static Document getDocumentByXmlFile(String fileName) {
		// 配置文件路径
		String path = JecnFileUtil.getSyncPath(fileName);

		return getDocumentByAllPath(path);
	}

	/**
	 * 
	 * 
	 * 通过提供的全路径把document对象写入到XML文件中
	 * 
	 * @param allPath
	 *            String 文件全路径
	 * @param doc
	 *            Document 待保存的Document对象
	 * @return boolean true：保存成功 false：没有成功保存
	 */
	public static boolean writeXmlByDocumentAndAllPath(String allPath,
			Document doc) {
		// 配置文件路径
		if (DrawCommon.isNullOrEmtryTrim(allPath)) {
			log
					.error("JecnXmlUtil类writeXmlByDocumentAndAllPath方法异常：文件路径为空。文件路径="
							+ allPath);
			return false;
		}

		File file = new File(allPath);
		if (file.isDirectory()) {// 不是文件
			log
					.error("JecnXmlUtil类writeXmlByDocumentAndAllPath方法异常:此路径不是文件  。文件路径="
							+ allPath);
			return false;
		}

		OutputStream fileoutputStream = null;

		try {
			// 输出流
			fileoutputStream = new FileOutputStream(file);

			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(fileoutputStream);
			// 写入XML文件
			transformer.transform(source, result);

		} catch (FileNotFoundException e) {
			log.error(
					"JecnXmlUtil类writeXmlByDocumentAndAllPath方法异常：文件找不到 文件路径="
							+ allPath, e);
			JecnOptionPane
					.showMessageDialog(JecnDrawMainPanel.getMainPanel(),
							JecnUserInfo.getValue("saveFilePathError"));
			return false;
		} catch (TransformerConfigurationException e) {
			log.error(
					"JecnXmlUtil类writeXmlByDocumentAndAllPath方法异常：创建Transformer对象异常 文件路径="
							+ allPath, e);
			return false;
		} catch (TransformerException e) {
			log
					.error(
							"JecnXmlUtil类writeXmlByDocumentAndAllPath方法异常：DOMSource对象转换成StreamResult对象是异常 文件路径="
									+ allPath, e);
			return false;
		}catch (Exception e) {
			log.error("JecnXmlUtil类writeXmlByDocumentAndAllPath方法异常：",e);
			JecnOptionPane
					.showMessageDialog(JecnDrawMainPanel.getMainPanel(),
							JecnUserInfo.getValue("saveFileFail"));
			return false;
		} finally {
			if (fileoutputStream != null) {
				try {
					fileoutputStream.close();
				} catch (IOException e) {
					log.error(
							"JecnXmlUtil类writeXmlByDocumentAndAllPath方法异常：OutputStream流关闭异常。 文件路径="
									+ allPath, e);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * 
	 * 
	 * 把document对象写入到XML文件中
	 * 
	 * @param fileName
	 *            String 文件名称
	 * @param doc
	 *            Document 待保存的Document对象
	 * @return boolean true：保存成功 false：没有成功保存
	 */
	public static boolean writeXmlByDocument(String fileName, Document doc) {
		// 配置文件路径
		String path = JecnFileUtil.getSyncPath(fileName);

		return writeXmlByDocumentAndAllPath(path, doc);
	}

	//
	// /**
	// *
	// * 解析给定XML格式字符串
	// *
	// * @param xmlContent
	// * XML格式字符串
	// * @return Document XML格式字符串解析后的Document对象
	// */
	// public static Document readXml(String xmlContent) {
	// // DocumentBuilderFactory工厂类
	// DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
	// .newInstance();
	//
	// // XML格式字符串的解析对象
	// DocumentBuilder docBuilder = null;
	// try {
	// docBuilder = docBuilderFactory.newDocumentBuilder();
	// // 解析XML格式字符串为Document对象
	// Document doc = docBuilder.parse(new InputSource(new StringReader(
	// xmlContent)));
	// return doc;
	//
	// } catch (ParserConfigurationException e) {
	// // 系统异常
	// log.error("", e);
	// } catch (SAXException e) {
	// // XML格式字符串格式不正确
	// log.error("", e);
	// } catch (IOException e) {
	// // 读写异常
	// log.error("", e);
	// }
	// return null;
	// }
	//
	// /**
	// * 获取给定的节点字符串
	// *
	// * @param node
	// * XML节点
	// * @return String XML格式字符串或""
	// */
	// public static String getXmlString(Node node) {
	//
	// Transformer tf;
	// try {
	// tf = TransformerFactory.newInstance().newTransformer();
	//
	// // XSLT处理器输出XML声明
	// tf.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	// // 指定编码字符集为UTF-8
	// tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	//
	// // 节点转换成XML格式的字符串
	// StreamResult dest = new StreamResult(new StringWriter());
	// tf.transform(new DOMSource(node), dest);
	//
	// return dest.getWriter().toString();
	// } catch (TransformerConfigurationException e) {
	// log.error("", e);
	// } catch (TransformerFactoryConfigurationError e) {
	// log.error("", e);
	// } catch (TransformerException e) {
	// log.error("", e);
	// }
	//
	// return "";
	// }
}
