/**
 * GetUserFinishedWorkItemCountResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetUserFinishedWorkItemCountResponse  implements java.io.Serializable {
    private int getUserFinishedWorkItemCountResult;

    public GetUserFinishedWorkItemCountResponse() {
    }

    public GetUserFinishedWorkItemCountResponse(
           int getUserFinishedWorkItemCountResult) {
           this.getUserFinishedWorkItemCountResult = getUserFinishedWorkItemCountResult;
    }


    /**
     * Gets the getUserFinishedWorkItemCountResult value for this GetUserFinishedWorkItemCountResponse.
     * 
     * @return getUserFinishedWorkItemCountResult
     */
    public int getGetUserFinishedWorkItemCountResult() {
        return getUserFinishedWorkItemCountResult;
    }


    /**
     * Sets the getUserFinishedWorkItemCountResult value for this GetUserFinishedWorkItemCountResponse.
     * 
     * @param getUserFinishedWorkItemCountResult
     */
    public void setGetUserFinishedWorkItemCountResult(int getUserFinishedWorkItemCountResult) {
        this.getUserFinishedWorkItemCountResult = getUserFinishedWorkItemCountResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetUserFinishedWorkItemCountResponse)) return false;
        GetUserFinishedWorkItemCountResponse other = (GetUserFinishedWorkItemCountResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.getUserFinishedWorkItemCountResult == other.getGetUserFinishedWorkItemCountResult();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getGetUserFinishedWorkItemCountResult();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetUserFinishedWorkItemCountResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetUserFinishedWorkItemCountResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getUserFinishedWorkItemCountResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GetUserFinishedWorkItemCountResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
