package epros.designer.gui.popedom.role.choose;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * @author yxw 2012-9-4
 * @description：部门选择
 */
public class RoleManageOrgChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(RoleManageOrgChooseDialog.class);

	public RoleManageOrgChooseDialog(List<JecnTreeBean> list) {
		super(list, 29);
		this.setTitle(JecnProperties.getValue("deptChoose"));
	}

	public RoleManageOrgChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog, Long startId) {
		super(list, 0, jecnDialog, startId);
		this.setTitle(JecnProperties.getValue("deptChoose"));
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("deptNameC");
	}

	@Override
	public JecnTree getJecnTree() {
		return new RoleManageHighEfficiencyOrgPersonSearchTree();
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("deptName"));// 部门名称
		return title;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getOrganizationAction().searchRoleAuthByName(name, JecnConstants.projectId,
					JecnDesignerCommon.getSecondAdminUserId());
		} catch (Exception e) {
			log.error("RoleManageOrgChooseDialog searchByName is error！", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {

		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				if (jecnTreeBean.getId() != null || jecnTreeBean.getName() != null) {
					Vector<String> data = new Vector<String>();
					data.add(jecnTreeBean.getId().toString());
					data.add(jecnTreeBean.getName());
					content.add(data);
				}
			}
		}
		return content;

	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}
}
