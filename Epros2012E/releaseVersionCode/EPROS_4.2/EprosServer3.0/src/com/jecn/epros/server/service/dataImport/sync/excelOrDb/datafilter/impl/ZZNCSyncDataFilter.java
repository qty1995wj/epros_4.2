package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.AbstractSyncDataFilter;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;

public class ZZNCSyncDataFilter extends AbstractSyncDataFilter {

	public ZZNCSyncDataFilter(BaseBean baseBean) {
		super(baseBean);
	}

	/**
	 * 
	 * 过滤部门数据
	 * 
	 * @param deptBeanList
	 *            List<DeptBean> 待导入的部门数据
	 * 
	 */
	@Override
	public void filterDeptDataBeforeCheck() {
		final String oldRootDeptNum = "-1";
		final String newRootDeptNum = "0";

		// 客户希望【只显示株洲时代电器集团股份有限公司】的组织结构，因此将此节点改为EPROS中的根节点（其余节点由于研究院的数据已经在EPROS中被删除，因此不显示，这些数据可以在回收站中查看）
		final String eprosRootDeptNum = "13762560000016";
		for (DeptBean deptBean : syncData.getDeptBeanList()) {
			//
			if (oldRootDeptNum.equals(deptBean.getPerDeptNum())) {
				deptBean.setPerDeptNum(newRootDeptNum);
			}

			if (eprosRootDeptNum.equals(deptBean.getDeptNum())) {
				deptBean.setPerDeptNum(newRootDeptNum);
			}
		}

	}

	/**
	 * 
	 * 过滤岗位人员数据
	 * 
	 * @param userPosBeanList
	 *            List<UserBean> 待导入的岗位人员数据
	 * 
	 */
	@Override
	public void filterUserPosDataBeforeCheck() {
	}

	/**
	 * 
	 * 
	 * 所有过滤校验规则后执行
	 * 
	 * @param baseBean
	 *            BaseBean待导入数据
	 * 
	 */
	@Override
	public void removeUnusefulDataAfterCheck() {
		// 入库之前删除掉 【岗位所属部门编号在部门集合中不存在】的数据
		for (int i = syncData.getUserBeanList().size() - 1; i >= 0; i--) {
			if (SyncErrorInfo.POS_DEPT_NUM_NOT_EXISTS_DEPT_LIST.equals(syncData.getUserBeanList().get(i)
					.getUserPosBean().getError())) {
				syncData.getUserBeanList().remove(i);
			}
		}
	}

	@Override
	protected void addedSpecialDataToSyncData() {

	}

}
