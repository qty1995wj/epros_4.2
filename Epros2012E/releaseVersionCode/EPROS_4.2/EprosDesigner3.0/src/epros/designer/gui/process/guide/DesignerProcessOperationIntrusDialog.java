package epros.designer.gui.process.guide;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT2;
import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.bean.process.JecnFlowRecordT;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.guide.guideTable.DesignerActiveDescTable;
import epros.designer.gui.process.guide.guideTable.DisignerProcessAndSystemDialog;
import epros.designer.gui.process.guide.guideTable.FlowControlTable;
import epros.designer.gui.process.guide.guideTable.FlowFileTable;
import epros.designer.gui.process.guide.guideTable.FlowRecordJDialog;
import epros.designer.gui.process.guide.guideTable.KeyControlPointTable;
import epros.designer.gui.process.guide.guideTable.KeySuccessFactorsTable;
import epros.designer.gui.process.guide.guideTable.ProcessEvaluationTable;
import epros.designer.gui.process.guide.guideTable.QuestionAreaTable;
import epros.designer.gui.process.guide.guideTable.RiskTable;
import epros.designer.gui.process.guide.guideTable.RoleResponseTable;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.service.process.JecnSelectProcessDownload;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.data.JecnFlowMapData;
import epros.draw.designer.JecnControlPointT;
import epros.draw.gui.operationConfig.ProcessOperationIntrusDialog;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.operationConfig.bean.JecnFlowOperationBean;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;

/*******************************************************************************
 * 流程操作说明
 * 
 * @author 2012-07-05
 * 
 */
public class DesignerProcessOperationIntrusDialog extends ProcessOperationIntrusDialog {
	private static Logger log = Logger.getLogger(DesignerProcessOperationIntrusDialog.class);
	/** 流程ID */
	private Long flowId;
	/** 驱动规则数据库查询数据 */
	private JecnFlowDriverT jecnFlowDriverT = null;
	/** 流程基本信息临时表 */
	private JecnFlowBasicInfoT flowBasicInfoT = null;
	/** 记录保存 */
	private List<JecnFlowRecordT> listJecnFlowRecordT = null;
	/** 处理后的记录保存数据 */
	private List<JecnFlowRecordT> jecnFlowRecordTList = new ArrayList<JecnFlowRecordT>();
	/** 相关制度 */
	private List<RuleT> listRuleT = null;
	/** 相关流程 */
	private List<Object[]> relatedFlowList = null;
	/** 相关标准 */
	private List<StandardBean> listStrand = null;
	/** 相关风险 */
	private List<JecnRisk> rilsList = null;
	/** 流程基本信息临时表 */
	private JecnFlowBasicInfoT2 flowBasicInfoT2 = null;
	/** 流程责任属性 ***/
	private ProcessAttributeBean processAttributeBean;
	/** 流程文控信息 **/
	private List<JecnTaskHistoryNew> listHistory = null;
	/** 获取审核人集合 **/
	private List<JecnTaskHistoryFollow> listHisFollow = null;

	private boolean isPurposeSaveNotNull = true;
	private boolean isApplicationScopeSaveNotNull = true;
	private boolean isDefinedTermSaveNotNull = true;
	private boolean isInSaveNotNull = true;
	private boolean isOutSaveNotNull = true;
	private boolean isClientSaveNotNull = true;
	private boolean isNotAplicationScopeSaveNotNull = true;
	private boolean isOutlineSaveNotNull = true;
	private boolean isSupplementSaveNotNull = true;
	private boolean isOrderSaveNotNull = true;
	private boolean isRelateFileNotNull = true;
	private boolean isCustomOneNotNull = true;
	private boolean isCustomTwoNotNull = true;
	private boolean isCustomThreeNotNull = true;
	private boolean isCustomFourNotNull = true;
	private boolean isCustomFiveNotNull = true;
	private boolean inOutNotNull = true;
	private boolean startEndActiveNull = true;
	private JecnTreeNode selectNode;

	public DesignerProcessOperationIntrusDialog(Long flowId, JecnTreeNode selectNode) {

		super(flowId);
		this.selectNode = selectNode;
		this.flowId = flowId;

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});

		try {
			jecnFlowDriverT = ConnectionPool.getProcessAction().getJecnFlowDriverT(flowId);
			flowBasicInfoT = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT(flowId);
			listJecnFlowRecordT = ConnectionPool.getProcessAction().getJecnFlowRecordTListByFlowId(flowId);
			listRuleT = ConnectionPool.getProcessAction().getRuleTByFlowId(flowId);
			relatedFlowList = ConnectionPool.getProcessAction().getFlowRelateByFlowIdDesign(flowId);
			listStrand = ConnectionPool.getProcessAction().getStandardBeanByFlowId(flowId);
			rilsList = ConnectionPool.getProcessAction().getRiskByRiskIds(getRiskList());
			listHistory = ConnectionPool.getDocControlAction().getJecnTaskHistoryNewList(flowId, 0);

			if (!JecnConstants.isPubShow()) {// D2版本
				// 获取流程基本信息表2(相关文件、自定义要素1-5)
				flowBasicInfoT2 = ConnectionPool.getProcessAction().getJecnFlowBasicInfoT2(flowId);
				if (flowBasicInfoT2 != null) {
					// 相关文件
					flowBasicInfoT.setFlowRelatedFile(flowBasicInfoT2.getFlowRelatedFile());
					// 自定义要素1
					flowBasicInfoT.setFlowCustomOne(flowBasicInfoT2.getFlowCustomOne());
					// 自定义要素2
					flowBasicInfoT.setFlowCustomTwo(flowBasicInfoT2.getFlowCustomTwo());
					// 自定义要素3
					flowBasicInfoT.setFlowCustomThree(flowBasicInfoT2.getFlowCustomThree());
					// 自定义要素4
					flowBasicInfoT.setFlowCustomFour(flowBasicInfoT2.getFlowCustomFour());
					// 自定义要素5
					flowBasicInfoT.setFlowCustomFive(flowBasicInfoT2.getFlowCustomFive());
				}
			}
			/** 流程责任属性 ***/
			processAttributeBean = ConnectionPool.getProcessAction().getFlowAttribute(flowId);
			// 初始化KPi动态值
			((ProcessEvaluationTable) processEvaluationTable).setTableModel(flowId);

		} catch (Exception e) {
			log.error("DesignerProcessOperationIntrusDialog is error", e);
		}

		// 流程监护人
		if ("0".equals(JecnConfigTool.getConfigItemValue(ConfigItemPartMapMark.guardianPeople))) {
			guardianPeopleLab.setVisible(false);
			guardianField.setVisible(false);
		}
		// 流程拟制人
		if ("0".equals(JecnConfigTool.getConfigItemValue(ConfigItemPartMapMark.fictionPeople))) {
			fictionPeopleLab.setVisible(false);
			fictionPeopleField.setVisible(false);
		}

		initializeData();

		// 相关标准隐藏name列
		hiddenStandardTableName();
	}

	/**
	 * 按钮面板
	 */
	protected void addButtonPanel() {
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(errorLab);
		buttonPanel.add(downLoad);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
	}

	/**
	 * 流程文件下载
	 */
	public void downLoadFile() {
		// 更新
		updateOperation();
		// 下载
		List<JecnTreeNode> selectNodes = new ArrayList<JecnTreeNode>();
		selectNodes.add(selectNode);
		JecnSelectProcessDownload.processDownload(false, selectNode, selectNodes);
	}

	// 时间驱动，时间配置
	private boolean isShowTimeDriverText() {
		String value = ConnectionPool.getConfigAciton().selectValue(6,
				ConfigItemPartMapMark.isShowDateFlowFile.toString());
		return JecnConfigContents.ITEM_IS_SHOW.equals(value) ? true : false;
	}

	public void initLayoutTable() {
		// 问题区域
		questionAreaTable = new QuestionAreaTable(flowOperationBean.getListPAActivityShow(), this);

		// 关键成功因素
		keySuccessTable = new KeySuccessFactorsTable(flowOperationBean.getListKSFActivityShow(), this);

		// 关键控制点
		keyContrPointTable = new KeyControlPointTable(flowOperationBean.getListKCPActivityShow(), this);

		// 活动明细
		drawActiveDescTable = new DesignerActiveDescTable(flowOperationBean.getListAllActivityShow(), this);

		// 角色职责
		roleResponseTable = new RoleResponseTable(flowOperationBean.getBaseRoleFigureList(), this);

		// 流程记录
		flowFileTable = new FlowFileTable(this, listAllActivityShow, true);

		// 操作规范
		practicesTable = new FlowFileTable(this, listAllActivityShow, false);

		// 流程关键评测指标
		processEvaluationTable = new ProcessEvaluationTable();

		// 相关风险
		riskTable = new RiskTable();
		// 流程文控信息
		flowControlTable = new FlowControlTable();
	}

	/**
	 * 初始化数据
	 * 
	 * @author fuzhh Oct 17, 2012
	 */
	public void initializeData() {
		try {
			// 相关文件
			if (relatedFileArea != null) {
				relatedFileArea.setText(flowBasicInfoT.getFlowRelatedFile());
			}
			// 自定义一
			if (customOneArea != null) {
				customOneArea.setText(flowBasicInfoT.getFlowCustomOne());
			}
			// 自定义二
			if (customTwoArea != null) {
				customTwoArea.setText(flowBasicInfoT.getFlowCustomTwo());
			}
			// 自定义三
			if (customThreeArea != null) {
				customThreeArea.setText(flowBasicInfoT.getFlowCustomThree());
			}
			// 自定义四
			if (customFourArea != null) {
				customFourArea.setText(flowBasicInfoT.getFlowCustomFour());
			}
			// 自定义五
			if (customFiveArea != null) {
				customFiveArea.setText(flowBasicInfoT.getFlowCustomFive());
			}
			// 目的
			if (purposeArea != null) {
				purposeArea.setText(flowBasicInfoT.getFlowPurpose());
			}
			// 适用范围
			if (applicationScopeArea != null) {
				applicationScopeArea.setText(flowBasicInfoT.getApplicability());
			}
			// 术语定义
			if (definedTermArea != null) {
				definedTermArea.setText(flowBasicInfoT.getNoutGlossary());
			}
			// 赋值
			if (flowBasicInfoT != null) {
				if (flowBasicInfoT.getDriveType() != null) {
					if (flowBasicInfoT.getDriveType() == 0) {
						driveTypeCombox.setSelectedIndex(0);
					} else if (flowBasicInfoT.getDriveType() == 1) {
						driveTypeCombox.setSelectedIndex(1);
					} else {
						driveTypeCombox.setSelectedIndex(2);
					}
				}
			}
			if (driveRuleArea != null) {
				driveRuleArea.setText(flowBasicInfoT.getDriveRules());
			}

			if (jecnFlowDriverT != null) {
				// 类型选择选中项设置
				String quantity = null;
				// 频率
				String strFrequency = null;
				quantity = jecnFlowDriverT.getQuantity();
				strFrequency = jecnFlowDriverT.getFrequency();
				if (quantity != null && !"".equals(quantity)) {
					if ("1".equals(quantity)) {// 日
						typeChangeCombox.setSelectedIndex(1);
					} else if ("2".equals(quantity)) {// 周
						typeChangeCombox.setSelectedIndex(2);
					} else if ("3".equals(quantity)) {// 月
						typeChangeCombox.setSelectedIndex(3);
					} else if ("4".equals(quantity)) {// 季度
						typeChangeCombox.setSelectedIndex(4);
					} else if ("5".equals(quantity)) {// 年
						typeChangeCombox.setSelectedIndex(5);
					}
				}
				// 开始时间
				String startTime = jecnFlowDriverT.getStartTime();
				// 结束时间
				String endTime = jecnFlowDriverT.getEndTime();
				if (startTime != null && endTime != null) {
					if (typeChangeCombox.getSelectedIndex() == 1) {
						String[] startTimeStr = startTime.split(",");
						startHourCombox.setSelectedIndex(Integer.valueOf(startTimeStr[0]));
						startMinuteCombox.setSelectedIndex(Integer.valueOf(startTimeStr[1]));
						startSecondCombox.setSelectedIndex(Integer.valueOf(startTimeStr[2]));

						String[] endTimeStr = endTime.split(",");
						endHourCombox.setSelectedIndex(Integer.valueOf(endTimeStr[0]));
						endMinuteCombox.setSelectedIndex(Integer.valueOf(endTimeStr[1]));
						endSecondCombox.setSelectedIndex(Integer.valueOf(endTimeStr[2]));

					} else if (typeChangeCombox.getSelectedIndex() == 2) {
						startWeekCombox.setSelectedIndex(Integer.valueOf(startTime));
						endWeekCombox.setSelectedIndex(Integer.valueOf(endTime));
					} else if (typeChangeCombox.getSelectedIndex() == 3) {
						startMonthCombox.setSelectedIndex(Integer.valueOf(startTime));
						endMonthCombox.setSelectedIndex(Integer.valueOf(endTime));
					} else if (typeChangeCombox.getSelectedIndex() == 4) {
						startSeasonCombox.setSelectedIndex(Integer.valueOf(startTime));
						endSeanCombox.setSelectedIndex(Integer.valueOf(endTime));
					} else if (typeChangeCombox.getSelectedIndex() == 5) {
						String[] startTimeStr = startTime.split(",");
						startYearCombox.setSelectedIndex(Integer.valueOf(startTimeStr[0]));
						startYearDayCombox.setSelectedIndex(Integer.valueOf(startTimeStr[1]));

						String[] endTimeStr = endTime.split(",");
						endYearCombox.setSelectedIndex(Integer.valueOf(endTimeStr[0]));
						endYearDayCombox.setSelectedIndex(Integer.valueOf(endTimeStr[1]));
					}
				}
				frequencyField.setText(strFrequency);
			}

			// 流程边界--起始活动
			if (startActiveField != null) {
				startActiveField.setText(flowBasicInfoT.getFlowStartpoint());
			}
			// 流程边界--终止活动
			if (endActiveField != null) {
				endActiveField.setText(flowBasicInfoT.getFlowEndpoint());
			}
			// 输入
			if (inField != null) {
				inField.setText(flowBasicInfoT.getInput());
			}
			// 输出
			if (outField != null) {
				outField.setText(flowBasicInfoT.getOuput());
			}
			// 客户
			if (clientField != null) {
				// 根据配置初始化客户
				initCustomerText();
			}
			// 不适用范围
			if (notAplicationScopeArea != null) {
				notAplicationScopeArea.setText(flowBasicInfoT.getNoApplicability());
			}
			// 概述
			if (outlineArea != null) {
				outlineArea.setText(flowBasicInfoT.getFlowSummarize());
			}
			// 补充说明
			if (supplementArea != null) {
				supplementArea.setText(flowBasicInfoT.getFlowSupplement());
			}
			// 相关流程
			if (relatedFlowTable != null) {
				Vector<Vector<String>> relatedFlowContent = new Vector<Vector<String>>();
				for (Object[] object : relatedFlowList) {
					Vector<String> relatedFlowContentStr = new Vector<String>();
					// ID
					relatedFlowContentStr.add(String.valueOf(object[0]));
					// 名称
					relatedFlowContentStr.add(String.valueOf(object[1]));
					// 类型
					String typeName = null;
					if ("1".equals(object[2].toString())) {
						typeName = JecnProperties.getValue("upstreamProcess");
					} else if ("2".equals(object[2].toString())) {
						typeName = JecnProperties.getValue("downstreamProcess");
					} else if ("3".equals(object[2].toString())) {
						typeName = JecnProperties.getValue("interfaceFlow");
					} else if ("4".equals(object[2].toString())) {
						typeName = JecnProperties.getValue("childProcess");
					}
					relatedFlowContentStr.add(typeName);

					relatedFlowContent.add(relatedFlowContentStr);
				}
				relatedFlowTable.setContent(relatedFlowContent);
				relatedFlowTable.setTableModel();
			}

			// 相关制度
			if (realtedRuleTable != null) {
				Vector<Vector<String>> ruleContent = new Vector<Vector<String>>();
				for (RuleT ruleT : listRuleT) {
					Vector<String> ruleContentStr = new Vector<String>();
					// 制度ID
					ruleContentStr.add(String.valueOf(ruleT.getId()));
					// 制度编号
					ruleContentStr.add(ruleT.getRuleNumber() == null ? "" : ruleT.getRuleNumber());
					// 制度名称
					ruleContentStr.add(ruleT.getRuleName());
					// 类型
					ruleContentStr.add(JecnDesignerCommon.ruleTypeVal(ruleT.getIsDir()));
					ruleContent.add(ruleContentStr);
				}
				realtedRuleTable.setContent(ruleContent);
				realtedRuleTable.setTableModel();
			}

			if (orderTable != null) {
				// 相关标准
				Vector<Vector<String>> orderContent = new Vector<Vector<String>>();
				for (StandardBean standardBean : listStrand) {
					Vector<String> orderContentStr = new Vector<String>();
					// 制度ID
					orderContentStr.add(String.valueOf(standardBean.getCriterionClassId()));
					// 标准名称
					orderContentStr.add(standardBean.getCriterionClassName());
					// 标准类型
					orderContentStr.add(JecnDesignerCommon.standardTypeVal(standardBean.getStanType()));
					orderContent.add(orderContentStr);
				}
				orderTable.setContent(orderContent);
				orderTable.setTableModel();
			}

			// 相关风险
			((RiskTable) riskTable).setContent(getRiskContent(rilsList));
			((RiskTable) riskTable).setTableModel();

			if (recordSaveTable != null) {
				// 记录保存
				recordSaveTable.setContent(getFlowRecordContent(listJecnFlowRecordT));
				recordSaveTable.setTableModel();
			}
			/** 流程责任属性开始 **/
			// 流程责任部门名称
			String departNames = processAttributeBean.getDutyOrgName();
			// 流程责任人名称
			String perpleNames = processAttributeBean.getDutyUserName();
			// 流程监护人名称
			String guardianNames = processAttributeBean.getGuardianName();
			// 流程拟制人名称
			String fictionPeopleNames = processAttributeBean.getFictionPeopleName();
			if (departNames != null && !"".equals(departNames)) {
				departField.setText(departNames);
			}
			if (perpleNames != null && !"".equals(perpleNames)) {
				attchMentField.setText(perpleNames);
			}
			if (guardianNames != null && !"".equals(guardianNames)) {
				guardianField.setText(guardianNames);
			}
			if (fictionPeopleNames != null && !"".equals(fictionPeopleNames)) {
				fictionPeopleField.setText(fictionPeopleNames);
			}
			/** 流程责任属性开始 **/
			/** 流程文控信息 **/
			((FlowControlTable) flowControlTable).setContent(getFlowControlTableContent(listHistory));
			((FlowControlTable) flowControlTable).setTableModel();
			/** 导入时操作说明重新赋值 */
			// 获取面板
			JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			JecnFlowOperationBean flowOperationData = drawDesktopPane.getFlowMapData().getFlowOperationBean();
			// 相关文件
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getFlowRelatedFile())) {
				relatedFileArea.setText(flowOperationData.getFlowRelatedFile());
			}
			// 自定义要素1
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getCustomOne())) {
				customOneArea.setText(flowOperationData.getCustomOne());
			}
			// 自定义要素2
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getCustomTwo())) {
				customTwoArea.setText(flowOperationData.getCustomTwo());
			}
			// 自定义要素3
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getCustomThree())) {
				customThreeArea.setText(flowOperationData.getCustomThree());
			}
			// 自定义要素4
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getCustomFour())) {
				customFourArea.setText(flowOperationData.getCustomFour());
			}
			// 自定义要素5
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getCustomFive())) {
				customFiveArea.setText(flowOperationData.getCustomFive());
			}
			// 目的
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getFlowPurpose())) {
				purposeArea.setText(flowOperationData.getFlowPurpose());
			}
			// 适用范围
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getApplicability())) {
				applicationScopeArea.setText(flowOperationData.getApplicability());
			}
			// 术语定义
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getNoutGlossary())) {
				definedTermArea.setText(flowOperationData.getNoutGlossary());
			}
			// 输入
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getFlowInput())) {
				inField.setText(flowOperationData.getFlowInput());
			}
			// 输出
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getFlowOutput())) {
				outField.setText(flowOperationData.getFlowOutput());
			}
			// 客户
			if (JecnConfigTool.processCustomShowType() == 0
					&& !DrawCommon.isNullOrEmtryTrim(flowOperationData.getFlowCustom())) {
				clientField.setText(flowOperationData.getFlowCustom());
			}
			// 不适用范围
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getNoApplicability())) {
				notAplicationScopeArea.setText(flowOperationData.getNoApplicability());
			}
			// 概述
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getFlowSummarize())) {
				outlineArea.setText(flowOperationData.getFlowSummarize());
			}
			// 补充说明
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getFlowSupplement())) {
				supplementArea.setText(flowOperationData.getFlowSupplement());
			}
			// 流程边界--起始活动
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getFlowStartActive())) {
				startActiveField.setText(flowOperationData.getFlowStartActive());
			}
			// 流程边界--终止活动
			if (!DrawCommon.isNullOrEmtryTrim(flowOperationData.getFlowEndActive())) {
				endActiveField.setText(flowOperationData.getFlowEndActive());
			}
			// 驱动规则
			if (StringUtils.isNotBlank(flowOperationData.getDriveType())) {
				driveTypeCombox.setSelectedIndex(Integer.valueOf(flowOperationData.getDriveType()));
				driveRuleArea.setText(flowOperationData.getDriveRules());
			}

			/** 导入时操作说明结束 */

		} catch (Exception e) {
			log.error("DesignerProcessOperationIntrusDialog initializeData is error", e);
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil.getJecnResourceUtil()
					.getValue("initializationDataAnomalies"));

			return;
		}
	}

	/**
	 * 拼装相关风险
	 * 
	 * @author fuzhh 2013-11-28
	 * @param rilsList
	 * @return
	 */
	public Vector<Vector<String>> getRiskContent(List<JecnRisk> rilsList) {
		Vector<Vector<String>> riskContent = new Vector<Vector<String>>();
		for (JecnRisk jecnRisk : rilsList) {
			Vector<String> riskContentStr = new Vector<String>();
			// ID
			riskContentStr.add(String.valueOf(jecnRisk.getId()));
			// 编号
			riskContentStr.add(jecnRisk.getRiskCode());
			// 名称
			riskContentStr.add(jecnRisk.getRiskName());

			riskContent.add(riskContentStr);
		}
		return riskContent;
	}

	/**
	 * 记录保存table数据
	 * 
	 * @author fuzhh Nov 5, 2012
	 * @param listJecnFlowRecordT
	 * @return
	 */
	public Vector<Vector<String>> getFlowRecordContent(List<JecnFlowRecordT> listJecnFlowRecordT) {
		Vector<Vector<String>> flowRecordContent = new Vector<Vector<String>>();
		for (JecnFlowRecordT flowRecordT : listJecnFlowRecordT) {
			Vector<String> flowRecordContentStr = new Vector<String>();
			// 编号ID
			String numberId = "";
			if (flowRecordT.getId() != null) {
				numberId = String.valueOf(flowRecordT.getId());
			}
			flowRecordContentStr.add(numberId);
			// 记录名称
			flowRecordContentStr.add(flowRecordT.getRecordName());
			// 保存责任人
			flowRecordContentStr.add(flowRecordT.getRecordSavePeople());
			// 保存场所
			flowRecordContentStr.add(flowRecordT.getSaveLaction());
			// 归档时间
			flowRecordContentStr.add(flowRecordT.getFileTime());
			// 保存期限
			flowRecordContentStr.add(flowRecordT.getSaveTime());
			// 到期处理方式
			flowRecordContentStr.add(flowRecordT.getRecordApproach());
			flowRecordContent.add(flowRecordContentStr);
		}
		return flowRecordContent;
	}

	/***
	 * 流程操作说明--流程文控信息
	 * 
	 * @param listFlowControl
	 * @return
	 */
	public Vector<Vector<String>> getFlowControlTableContent(List<JecnTaskHistoryNew> listFlowControl) {
		Vector<Vector<String>> flowControl = new Vector<Vector<String>>();
		for (JecnTaskHistoryNew taskHistoryNew : listFlowControl) {
			Vector<String> vec = new Vector<String>();
			// 流程ID
			vec.add(taskHistoryNew.getId().toString());
			// 版本号
			vec.add(taskHistoryNew.getVersionId());
			// 变更说明
			vec.add(taskHistoryNew.getModifyExplain());
			// 发布日期
			vec.add(taskHistoryNew.getPublishDate().toString());
			// 流程拟制人(拟稿人)
			vec.add(taskHistoryNew.getDraftPerson());
			// 审批人阶段名称
			try {
				String historyFollo = "";
				listHisFollow = ConnectionPool.getDocControlAction().getJecnTaskHistoryFollowList(
						taskHistoryNew.getId());
				for (JecnTaskHistoryFollow taskHistoryFollow : listHisFollow) {
					if (!DrawCommon.isNullOrEmtryTrim(taskHistoryFollow.getName())) {
						if ("".equals(historyFollo)) {
							historyFollo += taskHistoryFollow.getAppellation() + "：" + taskHistoryFollow.getName();
						} else {
							historyFollo += "\n" + taskHistoryFollow.getAppellation() + "："
									+ taskHistoryFollow.getName();
						}

					}
				}
				vec.add(historyFollo);
			} catch (Exception e) {
				log.error("DesignerProcessOperationIntrusDialog getFlowControlTableContent is error", e);
			}
			flowControl.add(vec);
		}
		return flowControl;
	}

	/**
	 * 确定
	 */
	public void okButPerformed() {
		updateOperation();
		this.setVisible(false);
	}

	private void updateOperation() {

		// 确认错误提示
		errorLab.setText("");

		// getFlowRole();
		/** *******************更新流程基本信息临时表数据********************* */
		if (isPurposeSave && isApplicationScopeSave && isInSave && isOutSave && isClientSave
				&& isNotAplicationScopeSave && isOutlineSave && isSupplementSave && isOrderSave && isCount
				&& isRealtedFileSave && isCustomOneSave && isCustomTwoSave && isCustomThreeSave && isCustomFourSave
				&& isCustomFiveSave && isInOutSave && isStartEndActive) {
			// 相关文件
			String relatedFileStr = relatedFileArea.getText();
			// 自定义一
			String customOneStr = customOneArea.getText();
			// 自定义二
			String customTwoStr = customTwoArea.getText();
			// 自定义三
			String customThreeStr = customThreeArea.getText();
			// 自定义四
			String customFourStr = customFourArea.getText();
			// 自定义五
			String customFiveStr = customFiveArea.getText();
			// 目的
			String purposeStr = purposeArea.getText();
			// 适用范围
			String applicationScopeStr = applicationScopeArea.getText();
			// 术语定义
			String definedTermStr = definedTermArea.getText();
			// 输入
			String inStr = inField.getText();
			// 输出
			String outStr = outField.getText();
			// 客户
			String clientStr = clientField.getText();
			// 不适用范围
			String notAplicationScopeStr = notAplicationScopeArea.getText();
			// 概述
			String outlineStr = outlineArea.getText();
			// 补充说明
			String supplementStr = supplementArea.getText();
			// 驱动规则
			String driveRuleStr = driveRuleArea.getText();
			// 起始活动
			String startActiveStr = startActiveField.getText();
			// 终止活动
			String endActiveStr = endActiveField.getText();

			if (isPurposeSaveNotNull && isApplicationScopeSaveNotNull && isDefinedTermSaveNotNull && isInSaveNotNull
					&& isOutSaveNotNull && isClientSaveNotNull && isNotAplicationScopeSaveNotNull
					&& isOutlineSaveNotNull && isSupplementSaveNotNull && isOrderSaveNotNull && isRelateFileNotNull
					&& isCustomOneNotNull && isCustomTwoNotNull && isCustomThreeNotNull && isCustomFourNotNull
					&& isCustomFiveNotNull && inOutNotNull && startEndActiveNull) {
				// 使用范围
				flowBasicInfoT.setApplicability(applicationScopeStr);
				// 术语定义
				flowBasicInfoT.setNoutGlossary(definedTermStr);
				// 目的
				flowBasicInfoT.setFlowPurpose(purposeStr);
				// 输入
				flowBasicInfoT.setInput(inStr);
				// 输出
				flowBasicInfoT.setOuput(outStr);
				// 客户
				flowBasicInfoT.setFlowCustom(clientStr);
				// 不适用范围
				flowBasicInfoT.setNoApplicability(notAplicationScopeStr);
				// 概述
				flowBasicInfoT.setFlowSummarize(outlineStr);
				// 补充说明
				flowBasicInfoT.setFlowSupplement(supplementStr);
				// 流程驱动类型
				flowBasicInfoT.setDriveRules(driveRuleStr);
				// 流程驱动规则
				flowBasicInfoT.setDriveType(Long.valueOf(driveTypeCombox.getSelectedIndex()));

				// 相关文件
				flowBasicInfoT.setFlowRelatedFile(relatedFileStr);
				// 自定义一
				flowBasicInfoT.setFlowCustomOne(customOneStr);
				// 自定义二
				flowBasicInfoT.setFlowCustomTwo(customTwoStr);
				// 自定义三
				flowBasicInfoT.setFlowCustomThree(customThreeStr);
				// 自定义四
				flowBasicInfoT.setFlowCustomFour(customFourStr);
				// 自定义五
				flowBasicInfoT.setFlowCustomFive(customFiveStr);
				// 起始活动
				flowBasicInfoT.setFlowStartpoint(startActiveStr);
				// 终止活动
				flowBasicInfoT.setFlowEndpoint(endActiveStr);
				// 驱动规则
				JecnFlowDriverT jecnFlowDriver = getJecnFlowDriver();
				// 获取面板
				JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
				// 获取面板数据
				JecnFlowMapData flowMap = drawDesktopPane.getFlowMapData();

				// 记录保存数据
				jecnFlowRecordTList.addAll(listJecnFlowRecordT);
				try {
					ConnectionPool.getProcessAction().updateFlowOperation(flowId, flowBasicInfoT, jecnFlowDriver,
							jecnFlowRecordTList, JecnConstants.loginBean.getJecnUser().getPeopleId());
					selectNode.getJecnTreeBean().setUpdate(true);
					JecnTreeCommon.reload(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), selectNode);
					drawDesktopPane.repaint();
					/** 保存操作说明时更新面板数据 */
					// 相关文件
					flowMap.getFlowOperationBean().setFlowRelatedFile(relatedFileStr);
					// 自定义一
					flowMap.getFlowOperationBean().setCustomOne(customOneStr);
					// 自定义二
					flowMap.getFlowOperationBean().setCustomTwo(customTwoStr);
					// 自定义三
					flowMap.getFlowOperationBean().setCustomThree(customThreeStr);
					// 自定义四
					flowMap.getFlowOperationBean().setCustomFour(customFourStr);
					// 自定义五
					flowMap.getFlowOperationBean().setCustomFive(customFiveStr);
					// 目的
					flowMap.getFlowOperationBean().setFlowPurpose(purposeStr);
					// 适用范围
					flowMap.getFlowOperationBean().setApplicability(applicationScopeStr);
					// 术语定义
					flowMap.getFlowOperationBean().setNoutGlossary(definedTermStr);
					// 流程输入
					flowMap.getFlowOperationBean().setFlowInput(inStr);
					// 流程输出
					flowMap.getFlowOperationBean().setFlowOutput(outStr);
					// 不适用范围
					flowMap.getFlowOperationBean().setNoApplicability(notAplicationScopeStr);
					// 概况信息
					flowMap.getFlowOperationBean().setFlowSummarize(outlineStr);
					// 补充说明
					flowMap.getFlowOperationBean().setFlowSupplement(supplementStr);
					// 流程客户
					flowMap.getFlowOperationBean().setFlowCustom(clientStr);
					// 起始活动
					flowMap.getFlowOperationBean().setFlowStartActive(startActiveStr);
					// 终止活动
					flowMap.getFlowOperationBean().setFlowEndActive(endActiveStr);
					// 驱动规则
					flowMap.getFlowOperationBean().setDriveType(flowBasicInfoT.getDriveType().toString());
					flowMap.getFlowOperationBean().setDriveRules(flowBasicInfoT.getDriveRules());

					/** 保存操作说明时更新面板数据结束 */
				} catch (Exception e) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
							.getJecnResourceUtil().getValue("saveException"));
					log.error("DesignerProcessOperationIntrusDialog updateOperation is error", e);
				}
			} else {
				String info = "";
				if (!isPurposeSaveNotNull) { // 目的
					info = purposeErrorLab.getText();
				} else if (!isApplicationScopeSaveNotNull) { // 适用范围
					info = applicationScopeErrorLab.getText();
				} else if (!isDefinedTermSaveNotNull) { // 术语定义
					info = definedTermErrorLab.getText();
				} else if (!isOrderSaveNotNull) { // 驱动规则
					info = orderErrorLab.getText();
				} else if (!isInSaveNotNull) { // 输入
					info = inErrorLab.getText();
				} else if (!isOutSaveNotNull) { // 输出
					info = outErrorLab.getText();
				} else if (!isClientSaveNotNull) { // 客户
					info = clientErrorLab.getText();
				} else if (!isNotAplicationScopeSaveNotNull) { // 不适用范围
					info = notAplicationScopeErrorLab.getText();
				} else if (!isOutlineSaveNotNull) { // 概述
					info = outlineErrorLab.getText();
				} else if (!isSupplementSaveNotNull) { // 补充说明
					info = supplementErrorLab.getText();
				} else if (!isRelateFileNotNull) {// 相关文件
					info = relatedFileErrorLab.getText();
				} else if (!isCustomOneNotNull) {// 自定义一
					info = customOneErrorLab.getText();
				} else if (!isCustomTwoNotNull) {// 自定义二
					info = customTwoErrorLab.getText();
				} else if (!isCustomThreeNotNull) {// 自定义三
					info = customThreeErrorLab.getText();
				} else if (!isCustomFourNotNull) {// 自定义四
					info = customFourErrorLab.getText();
				} else if (!isCustomFiveNotNull) {// 自定义五
					info = customFiveErrorLab.getText();
				} else if (!inOutNotNull) {// 输入和输出
					info = inOutErrorLab.getText();
				} else if (!startEndActiveNull) {// 流程边界
					info = flowActiveErrorLab.getText();
				}
				errorLab.setText(info);
			}
		}
	}

	/**
	 * 获取驱动规则数据
	 * 
	 * @author fuzhh Jan 7, 2013
	 * @return
	 */
	public JecnFlowDriverT getJecnFlowDriver() {
		/** *******************更新流程驱动规则临时表数据********************* */
		String strQuantity = String.valueOf(typeChangeCombox.getSelectedIndex());
		JecnFlowDriverT jecnFlowDriver = new JecnFlowDriverT();
		jecnFlowDriver.setQuantity(strQuantity);
		jecnFlowDriver.setFrequency(frequencyField.getText());
		jecnFlowDriver.setFlowId(flowId);

		String strStartSelected = null;
		String strEndSelected = null;
		// 年--天 ///日类型--时
		String startYeaDaySel = null;
		// 年---天 //日类型---分
		String endYeaDaySel = null;

		String startSecSel = null;
		String endSecSel = null;

		// 根据索引获取类型
		if ("1".equals(strQuantity)) {
			// 开始
			strStartSelected = startHourCombox.getSelectedIndex() + ",";// 时
			startYeaDaySel = startMinuteCombox.getSelectedIndex() + ",";// 分
			startSecSel = String.valueOf(startSecondCombox.getSelectedIndex());// 秒
			// 结束
			strEndSelected = endHourCombox.getSelectedIndex() + ","; // 时
			endYeaDaySel = endMinuteCombox.getSelectedIndex() + ",";// 分
			endSecSel = String.valueOf(endSecondCombox.getSelectedIndex());// 秒

			jecnFlowDriver.setStartTime(strStartSelected + startYeaDaySel + startSecSel);
			jecnFlowDriver.setEndTime(strEndSelected + endYeaDaySel + endSecSel);

		} else if ("2".equals(strQuantity)) {
			jecnFlowDriver.setStartTime(String.valueOf(startWeekCombox.getSelectedIndex()));
			jecnFlowDriver.setEndTime(String.valueOf(endWeekCombox.getSelectedIndex()));
		} else if ("3".equals(strQuantity)) {
			jecnFlowDriver.setStartTime(String.valueOf(startMonthCombox.getSelectedIndex()));
			jecnFlowDriver.setEndTime(String.valueOf(endMonthCombox.getSelectedIndex()));
		} else if ("4".equals(strQuantity)) {
			jecnFlowDriver.setStartTime(String.valueOf(startSeasonCombox.getSelectedIndex()));
			jecnFlowDriver.setEndTime(String.valueOf(endSeanCombox.getSelectedIndex()));
		} else if ("5".equals(strQuantity)) {
			// 开始
			strStartSelected = startYearCombox.getSelectedIndex() + ",";// 月
			startYeaDaySel = String.valueOf(startYearDayCombox.getSelectedIndex());// 天
			// 结束
			strEndSelected = endYearCombox.getSelectedIndex() + ","; // 月
			endYeaDaySel = String.valueOf(endYearDayCombox.getSelectedIndex());// 天
			jecnFlowDriver.setStartTime(strStartSelected + startYeaDaySel);
			jecnFlowDriver.setEndTime(strEndSelected + endYeaDaySel);
		}
		return jecnFlowDriver;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton but = (JButton) e.getSource();
			// 获取按钮名称
			String butName = but.getName();
			// 制度 标准 内控
			if ("rule".equals(butName) || "order".equals(butName)) {
				DisignerProcessAndSystemDialog processAndSystemDialog = new DisignerProcessAndSystemDialog(flowId,
						realtedRuleTable, orderTable);
				processAndSystemDialog.setVisible(true);
				realtedRuleTable.repaint();
				orderTable.repaint();
			} else if ("change".equals(butName)) { // 流程关键评测指标
				try {
					KPIShowDialog designerKPIShowDialog = new KPIShowDialog(flowId);
					designerKPIShowDialog.setVisible(true);
					processEvaluationTable.removeAll();
					((ProcessEvaluationTable) processEvaluationTable).setTableModel(flowId);
				} catch (Exception e1) {
					log.error("DesignerProcessOperationIntrusDialog actionPerformed is error", e1);
				}
			} else if ("flowProperty".equals(butName)) {// 流程责任属性
				ProcessPropertyDialog propertyDialog = new ProcessPropertyDialog(selectNode);
				propertyDialog.setVisible(true);
				this.guardianField.setText(propertyDialog.getGuardianName());// 流程监护人
				this.attchMentField.setText(propertyDialog.getResPeopleName());// 流程责任人
				this.fictionPeopleField.setText(propertyDialog.getFictionPeopleName());// 流程拟制人
				this.departField.setText(propertyDialog.getResOrgName());// 流程部门
			}
		}
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	/**
	 * 
	 * 是否有更新
	 * 
	 * @return boolean true：有更新；false：没有更新
	 */
	protected boolean isUpdate() {
		// 相关文件
		String relatedFile = relatedFileArea.getText();
		// 自定义一
		String customOneValue = customOneArea.getText();
		// 自定义二
		String customTwoValue = customTwoArea.getText();
		// 自定义三
		String customThreeValue = customThreeArea.getText();
		// 自定义四
		String customFourValue = customFourArea.getText();
		// 自定义五
		String customFiveValue = customFiveArea.getText();
		// 目的
		String purpose = purposeArea.getText();
		// 适用范围
		String applicationScope = applicationScopeArea.getText();
		// 术语定义
		String definedTerm = definedTermArea.getText();
		// 输入
		String input = inField.getText();
		// 输出
		String output = outField.getText();
		// 客户
		String client = clientField.getText();
		// 不适用范围
		String notAplication = notAplicationScopeArea.getText();
		// 概述
		String outline = outlineArea.getText();
		// 补充说明
		String supplement = supplementArea.getText();
		// 驱动规则
		String driveRuleStr = driveRuleArea.getText();
		// 驱动类型
		String driveType = String.valueOf(driveTypeCombox.getSelectedIndex());

		if (!isUpdate(purpose, applicationScope, definedTerm, input, output, client, notAplication, outline,
				supplement, driveRuleStr, driveType, relatedFile, customOneValue, customTwoValue, customThreeValue,
				customFourValue, customFiveValue)) {// 没有更新
			return false;
		}
		return true;
	}

	/**
	 * 判断是否有更新，有返回true，没有返回false
	 * 
	 * @author fuzhh Jan 5, 2013
	 * @param purpose
	 * @param applicationScope
	 * @param definedTerm
	 * @param input
	 * @param output
	 * @param client
	 * @param notAplication
	 * @param outline
	 * @param supplement
	 * @return boolean 有返回true，没有返回false
	 */
	protected boolean isUpdate(String purpose, String applicationScope, String definedTerm, String input,
			String output, String client, String notAplication, String outline, String supplement, String driveRuleStr,
			String driveType, String relatedFile, String customOneValue, String customTwoValue,
			String customThreeValue, String customFourValue, String customFiveValue) {
		// 驱动规则
		JecnFlowDriverT jecnFlowDriver = getJecnFlowDriver();
		if (jecnFlowDriverT == null) {
			jecnFlowDriverT = new JecnFlowDriverT();
		}
		if ((purpose != null && !DrawCommon.checkStringSame(purpose, flowBasicInfoT.getFlowPurpose()))
				|| (applicationScope != null && !DrawCommon.checkStringSame(applicationScope, flowBasicInfoT
						.getApplicability()))
				|| (relatedFile != null && !DrawCommon
						.checkStringSame(relatedFile, flowBasicInfoT.getFlowRelatedFile()))
				|| (customOneValue != null && !DrawCommon.checkStringSame(customOneValue, flowBasicInfoT
						.getFlowCustomOne()))
				|| (customTwoValue != null && !DrawCommon.checkStringSame(customTwoValue, flowBasicInfoT
						.getFlowCustomTwo()))
				|| (customThreeValue != null && !DrawCommon.checkStringSame(customThreeValue, flowBasicInfoT
						.getFlowCustomThree()))
				|| (customFourValue != null && !DrawCommon.checkStringSame(customFourValue, flowBasicInfoT
						.getFlowCustomFour()))
				|| (customFiveValue != null && !DrawCommon.checkStringSame(customFiveValue, flowBasicInfoT
						.getFlowCustomFive()))
				|| (definedTerm != null && !DrawCommon.checkStringSame(definedTerm, flowBasicInfoT.getNoutGlossary()))
				|| (input != null && !DrawCommon.checkStringSame(input, flowBasicInfoT.getInput()))
				|| (output != null && !DrawCommon.checkStringSame(output, flowBasicInfoT.getOuput()))
				|| (JecnConfigTool.processCustomShowType() == 0 && client != null && !DrawCommon.checkStringSame(
						client, flowBasicInfoT.getFlowCustom()))
				|| (notAplication != null && !DrawCommon.checkStringSame(notAplication, flowBasicInfoT
						.getNoApplicability()))
				|| (outline != null && !DrawCommon.checkStringSame(outline, flowBasicInfoT.getFlowSummarize()))
				|| (supplement != null && !DrawCommon.checkStringSame(supplement, flowBasicInfoT.getFlowSupplement()))
				|| (driveRuleStr != null && !DrawCommon.checkStringSame(driveRuleStr, flowBasicInfoT.getDriveRules()))
				|| (driveType != null && flowBasicInfoT.getDriveType() != null && !DrawCommon.checkStringSame(
						driveType, String.valueOf(flowBasicInfoT.getDriveType())))
				|| (jecnFlowDriver.getStartTime() != null && !DrawCommon.checkStringSame(jecnFlowDriver.getStartTime(),
						jecnFlowDriverT.getStartTime()))
				|| (jecnFlowDriver.getEndTime() != null && !DrawCommon.checkStringSame(jecnFlowDriver.getEndTime(),
						jecnFlowDriverT.getEndTime()))
				|| (jecnFlowDriver.getFrequency() != null && !DrawCommon.checkStringSame(jecnFlowDriver.getFrequency(),
						jecnFlowDriverT.getFrequency()))
				|| isFlowDriver(jecnFlowDriver.getQuantity(), jecnFlowDriverT.getQuantity())) {
			return true;
		}

		return false;
	}

	/**
	 * 判断操作说明 驱动规则 类型是否相等
	 * 
	 * @author fuzhh 2013-7-11
	 * @param quantityNow
	 *            本地 类型
	 * @param quantityDb
	 *            数据库存储类型
	 * @return
	 */
	private boolean isFlowDriver(String quantityNow, String quantityDb) {
		if ("0".equals(quantityNow)) {
			if (DrawCommon.isNullOrEmtryTrim(quantityDb) || "0".equals(quantityDb)) {
				return false;
			} else {
				return true;
			}
		} else {
			if (DrawCommon.checkStringSame(quantityNow, quantityDb)) {
				return false;
			} else {
				return true;
			}
		}
	}

	/**
	 * 获取流程 风险点ID
	 * 
	 * @author fuzhh 2013-11-28
	 * @return
	 */
	public Set<Long> getRiskList() {
		Set<Long> riskSet = new HashSet<Long>();
		for (JecnActivityShowBean activityShowBean : flowOperationBean.getListAllActivityShow()) {
			// 获取活动对应的控制点集合
			List<JecnControlPointT> jecnControlPointTList = activityShowBean.getActiveFigure().getFlowElementData()
					.getJecnControlPointTList();
			if (jecnControlPointTList == null) {// 为空执行下一个活动
				continue;
			}
			for (JecnControlPointT controlPointT : jecnControlPointTList) {
				if (controlPointT != null && controlPointT.getRiskId() != null) {
					riskSet.add(controlPointT.getRiskId());
				}
			}
		}
		return riskSet;
	}

	/**
	 * 记录保存添加
	 */
	public void addButPerformed() {
		JecnFlowRecordT flowRecordT = new JecnFlowRecordT();
		FlowRecordJDialog flowRecordJDialog = new FlowRecordJDialog(flowRecordT, true);
		flowRecordT.setFlowId(flowId);
		flowRecordJDialog.setVisible(true);
		if (flowRecordJDialog.isSave) {
			listJecnFlowRecordT.add(flowRecordT);
			recordSaveTable.setContent(getFlowRecordContent(listJecnFlowRecordT));
			recordSaveTable.setTableModel();
		}
	}

	/**
	 * 编辑
	 * 
	 * @author fuzhh Nov 5, 2012
	 */
	public void editButPerformed() {
		int row = recordSaveTable.getSelectedRow();
		if (row == -1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("pleaseChooseToEditTheRecord"));
			return;
		}
		JecnFlowRecordT flowRecordT = listJecnFlowRecordT.get(row);
		FlowRecordJDialog flowRecordJDialog = new FlowRecordJDialog(flowRecordT, false);
		flowRecordJDialog.setVisible(true);
		recordSaveTable.setContent(getFlowRecordContent(listJecnFlowRecordT));
		recordSaveTable.setTableModel();
	}

	/**
	 * 记录保存删除
	 */
	public void deleteButPerformed() {
		// 获取选中删除的行
		int[] selectRows = recordSaveTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnResourceUtil.getJecnResourceUtil().getValue(
					"pleaseChooseToDeleteRecords"));
			return;
		}
		// 删除记录保存表格中的数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) recordSaveTable.getModel()).removeRow(selectRows[i]);
			listJecnFlowRecordT.remove(selectRows[i]);
		}

	}

	private void initCustomerText() {
		if (JecnConfigTool.processCustomShowType() == 1) {
			clientField.setEditable(false);
			// 获取面板上客户
			clientField.setText(JecnDesignerCommon.getCustomValue());
		} else {
			clientField.setText(flowBasicInfoT.getFlowCustom());
		}
	}

	protected int addTimePanel(GridBagConstraints c, int flagNum) {
		// 判断是否移除驱动规则时间选项
		if (isShowTimeDriverText()) {
			return super.addTimePanel(c, flagNum);
		}
		flagNum = flagNum + 1;
		return flagNum;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	/**
	 * 相关标准列表，标准名称
	 */
	protected String getStandardTitle() {
		return JecnConfigTool.isBDFOperType() ? JecnProperties.getValue("fileName") : JecnProperties
				.getValue("standardName");
	}

	/**
	 * 隐藏相关标准标题列
	 */
	public void hiddenStandardTableName() {
		if (JecnConfigTool.isBDFOperType()) {
			TableColumnModel columnModel = orderTable.getColumnModel();
			TableColumn tableColumn = columnModel.getColumn(2);
			tableColumn.setMinWidth(0);
			tableColumn.setMaxWidth(0);
		}
	}
}
