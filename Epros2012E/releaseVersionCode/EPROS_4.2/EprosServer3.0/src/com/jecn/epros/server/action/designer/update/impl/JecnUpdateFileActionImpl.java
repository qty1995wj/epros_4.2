package com.jecn.epros.server.action.designer.update.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.designer.update.JecnUpdateFileAction;
import com.jecn.epros.server.bean.system.JecnVersionSys;
import com.jecn.epros.server.util.JecnPath;

/**
 * 获取版本信息 接口 实现类
 * @author fuzhh Apr 9, 2013
 *
 */
public class JecnUpdateFileActionImpl implements JecnUpdateFileAction {
	private static Logger log = Logger
			.getLogger(JecnUpdateFileActionImpl.class);

	/**
	 * 读取本地 版本配置文件信息
	 */
	public JecnVersionSys readProperties() throws Exception {
		JecnVersionSys versionSys = new JecnVersionSys();
		// 获取单点登录配置文件信息
		Properties props = new Properties();
		// 获取URL
		String url = JecnUpdateFileActionImpl.class.getClassLoader()
				.getResource("Sysinfo.properties").toString().substring(6);
		// 转换空格
		String empUrl = url.replaceAll("%20", " ");
		log.info("本地文件版本信息配置路径:" + empUrl);
		FileInputStream in = null;
		try {
			in = new FileInputStream(empUrl);
			props.load(in);
			// 版本号
			String version = props.getProperty("version");
			versionSys.setVersion(version);
			// 更新序号
			String updateNum = props.getProperty("updateNum");
			versionSys.setUpdateNum(updateNum);
			// 发布日期
			String pubtime = props.getProperty("pubtime");
			versionSys.setPubtime(pubtime);
		} catch (IOException e) {
			log.error("读取本地文件版本信息配置文件信息IO错误！", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("关闭流错误！", e1);
				}
			}
		}
		return versionSys;
	}

	/**
	 * 读取更新的exe文件
	 * 
	 * @author fuzhh Apr 7, 2013
	 * @return
	 * @throws Exception
	 */
	public byte[] readUpdateExe() throws Exception {
		String path = JecnPath.APP_PATH + "\\downLoad\\" + exeName;
		InputStream in = new FileInputStream(path);
		return getByte(in);
	}

	/**
	 * 读取更新的压缩包
	 * 
	 * @author fuzhh Apr 7, 2013
	 * @return
	 * @throws Exception
	 */
	public byte[] readUpdateZip() throws Exception {
		String path = JecnPath.APP_PATH + "\\downLoad\\" + zipName;
		InputStream in = new FileInputStream(path);
		return getByte(in);
	}

	/**
	 * 把一个文件转化为字节
	 * 
	 * @param file
	 * @return byte[]
	 * @throws Exception
	 */
	public byte[] getByte(InputStream in) throws Exception {
		byte[] bytes = null;
		if (in != null) {
			try{
			int length = in.available();
			if (length > Integer.MAX_VALUE) // 当文件的长度超过了int的最大值
			{
				return null;
			}
			bytes = new byte[length];
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length
					&& (numRead = in.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}
			// 如果得到的字节长度和file实际的长度不一致就可能出错了
			if (offset < bytes.length) {
				return null;
			}
			}finally{
				try{
				in.close();
				}catch(Exception e){
					log.error("", e);
				}
			}
		}
		return bytes;
	}

	// public static void main(String[] arge) {
	// try {
	// JecnUpdateFileActionImpl updateFileActionImpl = new
	// JecnUpdateFileActionImpl();
	// updateFileActionImpl.readUpdateZip();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
}
