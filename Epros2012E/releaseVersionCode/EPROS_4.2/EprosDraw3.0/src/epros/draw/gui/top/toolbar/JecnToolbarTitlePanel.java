package epros.draw.gui.top.toolbar;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JPanel;

import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.top.button.JecnToolbarTitleButton;
import epros.draw.system.JecnEprosVersion;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏标题面板：文件、编辑、格式、帮助等
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolbarTitlePanel extends JPanel implements ActionListener {
	/** 工具栏对象 */
	private JecnToolBarPanel toolBarPanel = null;
	/** 按钮互斥组 */
	private ButtonGroup btnGroup = null;

	/** 工具栏标题栏的背景图片 */
	private Icon toolbarTitleBackIcon = null;
	/** 工具栏标题栏的选中项图片 */
	private Icon toolbarTitleSelectedIcon = null;

	/** 选中的工具栏标题按钮 */
	private JecnToolbarTitleButton selectedTitleButton = null;

	/** 工具栏标题按钮以及对应面板集合 */
	private List<JecnToolBarTitleData> titleDataList = new ArrayList<JecnToolBarTitleData>();

	/** 按钮面板:工具栏标题按钮都添加到此面板中 */
	private JPanel btnPanel = null;
	/** 背景图片：520*30 选中图片：100*30 左右两边空闲：10 */
	private final int backWidth = 10;

	JecnToolbarTitlePanel(JecnToolBarPanel toolBarPanel) {
		if (toolBarPanel == null) {
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.toolBarPanel = toolBarPanel;
		initComponents();
		initBtnPanel();
	}

	private void initComponents() {
		// 按钮面板:工具栏标题按钮都添加到此面板中
		btnPanel = new JPanel();
		// 按钮互斥组
		btnGroup = new ButtonGroup();

		// 选中图片
		toolbarTitleSelectedIcon = JecnFileUtil
				.getToolBarTitleImageIcon(JecnEprosVersion.getEprosVersion()
						.getToolbarTitleBackSelectedIcon());
		// 获取图片
		toolbarTitleBackIcon = JecnFileUtil
				.getToolBarTitleImageIcon(JecnEprosVersion.getEprosVersion()
						.getToolbarTitleBackIcon());

		// 布局管理器
		this.setLayout(new GridBagLayout());
		// 透明
		this.setOpaque(false);
		// 无边框
		this.setBorder(null);
		// 大小：背景图片大小
		this.setPreferredSize(new Dimension(
				toolbarTitleBackIcon.getIconWidth(), toolbarTitleBackIcon
						.getIconHeight()));

		// // 按钮面板:工具栏标题按钮都添加到此面板中
		btnPanel.setOpaque(false);
		btnPanel.setBorder(null);
		btnPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		btnPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
	}

	private void initBtnPanel() {
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				new Insets(0, backWidth, 0, backWidth), 0, 0);
		this.add(btnPanel, c);
	}

	/**
	 * 
	 * 添加按钮到工具栏标题按钮集合中
	 * 
	 * @param titleData
	 *            JecnToolBarTitleData 按钮以及对应的面板
	 */
	void addButton(JecnToolBarTitleData titleData) {
		if (titleData == null) {
			return;
		}
		// 添加到集合
		titleDataList.add(titleData);
		// 添加到面板
		addButtonsToPanel(titleData);
	}

	/**
	 * 
	 * 插入按钮到指定位置的工具栏标题按钮集合中
	 * 
	 * @param titleData
	 *            JecnToolBarTitleData 按钮以及对应的面板
	 * @param index
	 *            index 插入的位置
	 */
	void insertButton(JecnToolBarTitleData titleData, int index) {
		if (titleData == null || index < 0 || index >= titleDataList.size()) {
			return;
		}
		// 添加到集合
		titleDataList.add(index, titleData);

		// 重新添加
		btnPanel.removeAll();
		for (JecnToolBarTitleData currTitleData : titleDataList) {
			btnGroup.remove(currTitleData.getTitleButton());
			addButtonsToPanel(currTitleData);
		}
	}

	/**
	 * 
	 * 添加指定的按钮到面板上
	 * 
	 * @param titleData
	 */
	private void addButtonsToPanel(JecnToolBarTitleData titleData) {
		if (titleData == null) {
			return;
		}
		// 标题按钮
		JecnToolbarTitleButton titleButton = titleData.getTitleButton();
		// 互斥组
		btnGroup.add(titleButton);
		// 添加到面板
		btnPanel.add(titleButton);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		// 绘制背景图片
		toolbarTitleBackIcon.paintIcon(this, g2d, 0, 0);

		if (selectedTitleButton == null) {
			return;
		}
		// 绘制选中图片
		toolbarTitleSelectedIcon.paintIcon(this, g2d, selectedTitleButton
				.getX(), selectedTitleButton.getY());

		// 绘制窗体底部线条
		JecnUIUtil.paintFrameBottomLine(this, g2d);
	}

	/**
	 * 
	 * 切换标题
	 * 
	 * @param paramActionEvent
	 *            动作事件
	 * @return Rectangle 需要渲染的区域
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnToolbarTitleButton) {
			JecnToolbarTitleButton currBtn = (JecnToolbarTitleButton) e
					.getSource();
			if (!currBtn.isSelected()) {
				return;
			}
			// 选中按钮
			selectedTitleButton = currBtn;

			for (JecnToolBarTitleData titleData : titleDataList) {
				if (currBtn == titleData.getTitleButton()) {
					// 添加按钮对应的面板
					toolBarPanel.getSubEachPartPanel().removeAll();
					toolBarPanel.getSubEachPartPanel()
							.add(titleData.getPnael());
					break;
				}
			}
		}
		this.validate();
		this.repaint();
		toolBarPanel.validate();
		toolBarPanel.repaint();
	}

	List<JecnToolBarTitleData> getTitleDataList() {
		return titleDataList;
	}

	JecnToolbarTitleButton getSelectedTitleButton() {
		return selectedTitleButton;
	}

	void setSelectedTitleButton(JecnToolbarTitleButton selectedTitleButton) {
		if (selectedTitleButton == null) {
			return;
		}
		this.selectedTitleButton = selectedTitleButton;
	}
}
