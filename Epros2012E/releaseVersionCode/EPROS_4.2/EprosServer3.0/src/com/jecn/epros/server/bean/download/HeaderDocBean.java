package com.jecn.epros.server.bean.download;

import java.io.Serializable;

/**
 * 页眉
 * 
 * @author fuzhh Nov 8, 2012
 * 
 */
public class HeaderDocBean implements Serializable {

	/** 名称 */
	private String name;
	/** 密集 */
	private String isPublic;
	/** 编号 */
	private String inputNum;
	/** 版本 */
	private String version;
	/** 发布日期 */
	private String releaseDate;
	/** 生效日期 */
	private String effectiveDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(String isPublic) {
		this.isPublic = isPublic;
	}

	public String getInputNum() {
		return inputNum;
	}

	public void setInputNum(String inputNum) {
		this.inputNum = inputNum;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
}
