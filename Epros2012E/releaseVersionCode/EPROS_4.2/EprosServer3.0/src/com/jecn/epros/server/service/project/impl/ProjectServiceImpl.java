package com.jecn.epros.server.service.project.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IJecnRoleDao;
import com.jecn.epros.server.dao.popedom.IOrganizationDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.popedom.IPositoinGroupDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.project.IProjectDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.standard.IStandardDao;
import com.jecn.epros.server.service.process.impl.FlowStructureServiceImpl;
import com.jecn.epros.server.service.project.IProjectService;

/*******************************************************************************
 * 项目管理处理
 * 
 * @author 2012-05-23
 * 
 */
@Transactional
public class ProjectServiceImpl extends AbsBaseService<JecnProject, Long>
		implements IProjectService {
	private Logger log = Logger.getLogger(ProjectServiceImpl.class);
	private IProjectDao projectDao;

	public IProjectDao getProjectDao() {
		return projectDao;
	}

	private IFileDao fileDao;
	private IJecnRoleDao jecnRoleDao;
	private IPositoinGroupDao positionGroupDao;
	private IOrganizationDao organizationDao;
	private IPersonDao personDao;
	private IFlowStructureDao flowStructureDao;
	private IRuleDao ruleDao;
	private IStandardDao standardDao;

	public void setProjectDao(IProjectDao projectDao) {
		this.projectDao = projectDao;
		this.baseDao = projectDao;
	}

	@Override
	public List<JecnProject> getListProject() throws Exception {
		String hql = "from JecnProject pt where pt.delSate = 0";
		List<JecnProject> projectObj = projectDao.listHql(hql);
		return projectObj;
	}

	@Override
	public void deleteProject(List<Long> listIds) throws Exception {
		//更改项目状态 1:已删除
		String sql = "update jecn_project set del_state = 1 where projectid in "
				+ JecnCommonSql.getIds(listIds);
		// 删除主项目
		String hql = "delete from JecnCurProject where curProjectId in "+ JecnCommonSql.getIds(listIds);
		projectDao.execteHql(hql);
		projectDao.execteNative(sql);
	}

	@Override
	public void openProject(Long proId) throws Exception {

	}

	@Override
	public void reProjectName(String newName, Long id) throws Exception {
		JecnProject jecnProject = projectDao.get(id);
		jecnProject.setProjectName(newName);
		projectDao.update(jecnProject);
	}

	@Override
	public void deleteIdsProject(Set<Long> setIds) throws Exception {
		Iterator<Long> it = setIds.iterator();
		while (it.hasNext()) {
			Long projectId = it.next();
			// 流程
			this.flowStructureDao.deleteFlowsByProjectId(projectId);
			// 组织
			this.organizationDao.deleteOrgs(projectId);
			// 标准
			this.standardDao.deleteStandards(projectId);
			// 制度
			this.ruleDao.deleteRules(projectId);
			// 文件
			this.fileDao.deleteFiles(projectId);
			// 岗位组
			this.positionGroupDao.deletePositionGroup(projectId);
			// 角色
			this.jecnRoleDao.deleteRoles(projectId);
			// 人员
			this.personDao.deletePeoples(projectId);
		}
//		// 删除主项目
//		String hql = "delete from JecnCurProject where curProjectId in";
//		List<String> list = JecnCommonSql.getSetSqls(hql, setIds);
//		for (String str : list) {
//			projectDao.execteHql(str);
//		}
		// 删除项目
		String hql = "delete from JecnProject where projectId in";
		List<String> list = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : list) {
			projectDao.execteHql(str);
		}
	}

	@Override
	public List<Object[]> getDelsProject() throws Exception {
		String hql = "select projectId,projectName from JecnProject pt where pt.delSate = 1";
		return projectDao.listHql(hql);
	}

	@Override
	public void updateRecoverDelProject(List<Long> ids) throws Exception {
		String sql = "update jecn_project set del_state = 0 where projectid in "+JecnCommonSql.getIds(ids);
		projectDao.execteNative(sql);
	}
	@Override
	public boolean validateAddName(String name) throws Exception {

		try {
			String hql = "from JecnProject where  projectName=? ";
			List<JecnFlowStructureT> list = projectDao.listHql(hql,name);
			if (list != null && list.size() > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	
	}

	@Override
	public boolean validateUpdateName(String name, long id) throws Exception {
		try {
			String hql = "from JecnProject where  projectId<> ? and projectName=? ";
			List<JecnFlowStructureT> list = flowStructureDao.listHql(hql,id, name);
			if (list != null && list.size() > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	
	}

	public IFileDao getFileDao() {
		return fileDao;
	}

	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
	}

	public IJecnRoleDao getJecnRoleDao() {
		return jecnRoleDao;
	}

	public void setJecnRoleDao(IJecnRoleDao jecnRoleDao) {
		this.jecnRoleDao = jecnRoleDao;
	}

	public IPositoinGroupDao getPositionGroupDao() {
		return positionGroupDao;
	}

	public void setPositionGroupDao(IPositoinGroupDao positionGroupDao) {
		this.positionGroupDao = positionGroupDao;
	}

	public IOrganizationDao getOrganizationDao() {
		return organizationDao;
	}

	public void setOrganizationDao(IOrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	public IPersonDao getPersonDao() {
		return personDao;
	}

	public void setPersonDao(IPersonDao personDao) {
		this.personDao = personDao;
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	public IRuleDao getRuleDao() {
		return ruleDao;
	}

	public void setRuleDao(IRuleDao ruleDao) {
		this.ruleDao = ruleDao;
	}

	public IStandardDao getStandardDao() {
		return standardDao;
	}

	public void setStandardDao(IStandardDao standardDao) {
		this.standardDao = standardDao;
	}


}
