package epros.designer.gui.process.guide;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.choose.DeptCompetecnTable;
import epros.designer.gui.choose.PosCompetenceTable;
import epros.designer.gui.choose.PosGroupCompetenceTable;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.popedom.organization.OrgChooseDialog;
import epros.designer.gui.popedom.position.PositionChooseDialog;
import epros.designer.gui.popedom.positiongroup.choose.PosGropChooseDialog;
import epros.designer.gui.process.flowtool.FlowToolChooseDialog;
import epros.designer.gui.system.ResourceHighEfficiencyTree;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.data.JecnFlowMapData;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程概况信息
 * 
 * @author 2012-07-04
 * 
 */
public class OverviewInformationDialog extends JecnDialog {
	private static Logger log = Logger
			.getLogger(OverviewInformationDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();

	/** 排列方式选择按钮面板 */
	private JecnPanel radioButPanel = new JecnPanel();

	/** 流程名称Lab */
	private JLabel flowNameLab = new JLabel(JecnProperties
			.getValue("flowNameC"));

	/** 流程名称Field */
	private JecnTextField flowField = new JecnTextField();

	/** 必填提示符 */
	private JLabel flowShowLab = new JLabel(JecnProperties.getValue("required"));

	/** 流程名称验证提示 */
	private VerifLabel flowNameVerif = new VerifLabel();

	/** 密级Lab */
	private JLabel isPublicLab = new JLabel(JecnProperties.getValue("isScretC"));

	/** 密级ComboBox */
	private JComboBox isPublicCombox = new JComboBox(new String[] {
			JecnProperties.getValue("secret"),
			JecnProperties.getValue("gongKai") });
	private Long isPublic = 1L;

	/** 密级空Label */
	private VerifLabel isPublicEmptyLab = new VerifLabel();

	/** 流程编号Lab */
	private JLabel flowNumLab = new JLabel(JecnProperties.getValue("flowNumC"));

	/** 流程编号Field */
	private JecnTextField flowNumField = new JecnTextField();

	/** 流程编号验证提示 */
	private VerifLabel flowNumVerif = new VerifLabel();

	/** 流程客户Lab */
	private JLabel flowClientLab = new JLabel(JecnProperties
			.getValue("flowClientC"));

	/** 流程客户Field */
	private JecnTextField flowClientField = new JecnTextField();

	/** 流程客户验证提示 */
	private VerifLabel flowClientVerif = new VerifLabel();

	/** 起始活动Lab */
	private JLabel startActivityLab = new JLabel(JecnProperties
			.getValue("startActiveC"));

	/** 起始活动Field */
	private JecnTextField startActivityField = new JecnTextField();

	/** 起始 活动验证提示 */
	private VerifLabel flowStartVerif = new VerifLabel();

	/** 终止活动Lab */
	private JLabel endActivityLab = new JLabel(JecnProperties
			.getValue("endActiveC"));

	/** 终止活动Field */
	private JecnTextField endActivityField = new JecnTextField();

	/** 终止 活动验证提示 */
	private VerifLabel flowEndVerif = new VerifLabel();

	/** 流程输入Lab */
	private JLabel flowInLab = new JLabel(JecnProperties.getValue("flowInC"));

	/** 流程输入Field */
	private JecnTextArea flowInField = new JecnTextArea();

	/** 输入JScrollPane */
	private JScrollPane flowInScrollPane = new JScrollPane(flowInField);

	/** 流程输入 验证提示 */
	private VerifLabel flowInVerif = new VerifLabel();

	/** 流程输出Lab */
	private JLabel flowOutLab = new JLabel(JecnProperties.getValue("flowOut"));

	/** 流程输出Field */
	private JecnTextArea flowOutField = new JecnTextArea();

	/** 流程输出JScrollPane */
	private JScrollPane flowOutScrollPane = new JScrollPane(flowOutField);

	/** 流程输出 验证提示 */
	private VerifLabel flowOutVerif = new VerifLabel();

	/** 支持工具Lab */
	private JLabel supporToolLab = new JLabel(JecnProperties
			.getValue("flowToolC"));

	/** 支持工具Area */
	private JecnTextArea supporToolArea = new JecnTextArea();
	private String sptIds = null;

	/** 支持工具选择按钮 */
	private JButton toolBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 附件Lab */
	private JLabel mapAttchMentLab = new JLabel(JecnProperties
			.getValue("mapFile"));

	/** 附件Area */
	private JecnTextField attchMentFiled = new JecnTextField();

	/** 附件的ID */
	private Long fileId = null;

	/** 附件选择按钮 */
	private JButton attchMentBut = new JButton(JecnProperties
			.getValue("selectBtn"));

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties
			.getValue("cancelBtn"));

	/** 验证提示Lab */
	private JLabel verfyLab = new JLabel();

	/** 设置大小 */
	Dimension dimension = null;

	/** 流程ID */
	private Long flowId = null;// 6359L;

	/** 支持工具 */
	private List<JecnTreeBean> listFlowTool = new ArrayList<JecnTreeBean>();

	/** 附件 */
	private List<JecnTreeBean> listFile = new ArrayList<JecnTreeBean>();
	JecnTreeBean fileTreeBean = new JecnTreeBean();

	/** 流程主表临时表 */
	private JecnFlowStructureT flowStructureT = new JecnFlowStructureT();
	/** 流程基本信息临时表 */
	private JecnFlowBasicInfoT flowBasicInfoT = new JecnFlowBasicInfoT();
	private List<Long> listToolIds = new ArrayList<Long>();

	/** 部门权限Lab */
	private JLabel depCompetenceLab = new JLabel(JecnProperties
			.getValue("departCompetenceC"));

	/** 部门权限Area */
	// private JecnTextArea depCompetenceArea = new JecnTextArea();
	private DeptCompetecnTable depCompetenceTable = null;

	/** 部门权限滚动面板 */
	private JScrollPane depCompetenceScrollPane = null; // new
	// JScrollPane(depCompetenceArea);

	/** 部门权限ids */
	private String orgIds = "";
	/** 岗位权限Lab */
	private JLabel posCompetenceLab = new JLabel(JecnProperties
			.getValue("posCompetenceC"));

	/** 岗位权限Area */
	// private JecnTextArea posCompetenceArea = new JecnTextArea();
	private PosCompetenceTable posCompetenceTable = null;

	/** 岗位权限滚动面板 */
	private JScrollPane posCompetenceScrollPane = null;// new
	// JScrollPane(posCompetenceArea);

	/** 岗位权限ids */
	private String posIds = "";
	
	/** 岗位组权限Lab */
	private JLabel posGroupCompetenceLab = new JLabel(JecnProperties
			.getValue("posCroupCompetenceC"));

	/** 岗位组权限Area */
	private PosGroupCompetenceTable posGroupCompetenceTable = null;

	/** 岗位组权限滚动面板 */
	private JScrollPane posGroupCompetenceScrollPane = null;// new

	/** 岗位组权限ids */
	private String posGroupIds = "";

	/** 部门权限选择按钮 */
	private JButton depBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 岗位权限 选择按钮 */
	private JButton posBut = new JButton(JecnProperties.getValue("selectBtn"));
	
	/** 岗位组权限 选择按钮 */
	private JButton posGroupBut = new JButton(JecnProperties.getValue("selectBtn"));

	private List<JecnTreeBean> orglist = new ArrayList<JecnTreeBean>();
	private List<JecnTreeBean> poslist = new ArrayList<JecnTreeBean>();
	private List<JecnTreeBean> posGroupList = new ArrayList<JecnTreeBean>();

	/** 判断面板中的可输入控件是否被修改 true：被修改，false 未修改 */
	private boolean isUpdate = false;
	/**获取面板*/
	private JecnDrawDesktopPane desktopPane = null;
	// 获取面板数据
	JecnFlowMapData flowMap = null;
	
	private JecnTreeNode selectNode=null;

	/** 获取流程概况信息 */

	public OverviewInformationDialog(Long flowId,JecnTreeNode workFlowNode) {
		this.flowId = flowId;
		this.setSize(550, 650);
		this.setTitle(JecnProperties.getValue("baseInfoBtn"));
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setModal(true);

		this.selectNode=workFlowNode;
		this.isPublicCombox.setSelectedIndex(1);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		radioButPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 流程输入Area添加滚动条
		flowInScrollPane.setBorder(null);
		flowInScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowInScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// flowInScrollPane.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		// 流程输出Area添加滚动条
		flowOutScrollPane.setBorder(null);
		flowOutScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowOutScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// flowOutScrollPane.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		dimension = new Dimension(450, 20);
		isPublicCombox.setPreferredSize(dimension);
		isPublicCombox.setMaximumSize(dimension);
		isPublicCombox.setMinimumSize(dimension);

		flowShowLab.setForeground(Color.red);
		supporToolArea.setEditable(false);
		attchMentFiled.setEditable(false);
		attchMentFiled.setBorder(supporToolArea.getBorder());
		supporToolArea.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		attchMentFiled.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		try {
			// 获取流程主表临时表数据
			flowStructureT = ConnectionPool.getProcessAction()
					.getJecnFlowStructureT(flowId);
			// 获取流程基本信息临时表数据
			flowBasicInfoT = ConnectionPool.getProcessAction()
					.getJecnFlowBasicInfoT(flowId);
			// 根据流程ID获得支持工具ID集合
			listToolIds = ConnectionPool.getFlowTool().getFlowToolIds(flowId);

			// 获得支持工具树对象
			listFlowTool = ConnectionPool.getFlowTool().getSustainToolsByIDs(
					listToolIds);
			sptIds = JecnDesignerCommon.getLook(listFlowTool, supporToolArea);

			// 附件
			fileTreeBean = ConnectionPool.getFileAction().getJecnTreeBeanByIds(
					flowBasicInfoT.getFileId(), JecnConstants.projectId);
			if (fileTreeBean != null) {
				listFile.add(fileTreeBean);
				fileId = JecnDesignerCommon.getFieldLook(listFile, attchMentFiled);
			}

			// 获取节点，部门权限，岗位权限

			LookPopedomBean lookPopedomBean = ConnectionPool
					.getOrganizationAction()
					.getOnlyAccessPermissions(flowId, 0);
			// 部门权限 depCompetenceArea orgIds
			// orgIds = JecnCommon.setDeptLook(, depCompetenceArea);
			orglist = lookPopedomBean.getOrgList();

			depCompetenceTable = new DeptCompetecnTable(orglist,false);
			depCompetenceScrollPane = new JScrollPane();
			depCompetenceScrollPane.setViewportView(depCompetenceTable);
			depCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
			if (orglist != null && orglist.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : orglist) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
					// listDept.add(o);
				}
				orgIds = sbIds.substring(0, sbIds.length() - 1);
			}
			// 岗位权限posIds posCompetenceArea
			poslist = lookPopedomBean.getPosList();
			posCompetenceTable = new PosCompetenceTable(poslist,false);
			posCompetenceScrollPane = new JScrollPane();
			posCompetenceScrollPane.setViewportView(posCompetenceTable);
			posCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
			if (poslist != null && poslist.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : poslist) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
					// listPos.add(o);
				}
				posIds = sbIds.substring(0, sbIds.length() - 1);
			}
			
			//岗位组权限posGroupCompetenceTable

			posGroupList = lookPopedomBean.getPosGroupList();
			posGroupCompetenceTable = new PosGroupCompetenceTable(posGroupList,false);
			posGroupCompetenceScrollPane = new JScrollPane();
			posGroupCompetenceScrollPane.setViewportView(posGroupCompetenceTable);
			posGroupCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
			if (posGroupList != null && posGroupList.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : posGroupList) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
					// listPos.add(o);
				}
				posGroupIds = sbIds.substring(0, sbIds.length() - 1);
			}
			
		} catch (Exception e1) {
			log.error("OverviewInformationDialog is error", e1);
		}
		// 给各个控件赋值
		// 流程名称
		flowField.setText(flowStructureT.getFlowName());
		flowField.setEditable(false);
		// 密级
		Long ispublic = flowStructureT.getIsPublic();
		if (ispublic == 0) {
			isPublicCombox.setSelectedIndex(0);
		} else if (ispublic == 1) {
			isPublicCombox.setSelectedIndex(1);
		}
		// 流程编号
		flowNumField.setText(flowStructureT.getFlowIdInput());
		// 流程客户
		flowClientField.setText(flowBasicInfoT.getFlowCustom());
		// 起始活动
		startActivityField.setText(flowBasicInfoT.getFlowStartpoint());
		// 终止活动
		endActivityField.setText(flowBasicInfoT.getFlowEndpoint());
		// 流程输入
		flowInField.setText(flowBasicInfoT.getInput());
		// 流程输出
		flowOutField.setText(flowBasicInfoT.getOuput());
		/**面板导入，获取面板上内容显示 ********开始*/
		desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if(desktopPane!=null){
			// 获取面板数据
			flowMap = desktopPane.getFlowMapData();
		}
		// 流程客户
		String cliendStr = flowMap.getFlowOperationBean().getFlowCustom();
		if(cliendStr!= null && !"".equals(cliendStr)){
			flowClientField.setText(cliendStr);
		}
		// 起始活动
		String startActiveStr = flowMap.getFlowOperationBean().getFlowStartActive();
		if(startActiveStr!= null && !"".equals(startActiveStr)){
			startActivityField.setText(startActiveStr);
		}
		// 终止活动
		String endActiveStr = flowMap.getFlowOperationBean().getFlowEndActive();
		if(endActiveStr!= null && !"".equals(endActiveStr)){
			endActivityField.setText(endActiveStr);
		}
		// 流程输入
		String flowInStr = flowMap.getFlowOperationBean().getFlowInput();
		if(flowInStr!= null && !"".equals(flowInStr)){
			flowInField.setText(flowInStr);
		}
		// 流程输出
		String flowOutStr = flowMap.getFlowOperationBean().getFlowOutput();
		if(flowOutStr!= null && !"".equals(flowOutStr)){
			flowOutField.setText(flowOutStr);
		}
		/**面板导入，获取面板上内容显示 ********结束*/
		//是否存在浏览端，没有浏览端隐藏查阅权限,密级,支持工具
		if(!JecnConstants.isPubShow()){
			//部门查阅权限
			depCompetenceLab.setVisible(false);
			depCompetenceScrollPane.setVisible(false);
			depBut.setVisible(false);
			//岗位查阅权限
			posCompetenceLab.setVisible(false);
			posCompetenceScrollPane.setVisible(false);
			posBut.setVisible(false);
			//岗位组查阅权限
			posGroupCompetenceLab.setVisible(false);
			posGroupCompetenceScrollPane.setVisible(false);
			posGroupBut.setVisible(false);	
			//密级
			isPublicLab.setVisible(false);
			isPublicCombox.setVisible(false);
			//支持工具
			supporToolLab.setVisible(false);
			supporToolArea.setVisible(false);
			toolBut.setVisible(false);
		}
		
		
		
		toolBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				toolButPerformed();
			}

		});
		attchMentBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				attchMentButPerformed();
			}

		});
		okBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}

		});
		cancelBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}

		});

		// 部门查阅权限选择
		depBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// orgIds = JecnCommon.setDeptLook(listDept,
				// depCompetenceArea,AddFlowDialog.this); //原来代码
				OrgChooseDialog orgChooseDialog = new OrgChooseDialog(orglist);
				orgChooseDialog.setVisible(true);
				if (orgChooseDialog.isOperation()) {

					// 判断是否选择了部门
					if (orglist != null && orglist.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : orglist) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						orgIds = sbIds.substring(0, sbIds.length() - 1);
						// ((DefaultTableModel)
						// depCompetenceTable.getModel()).setRowCount(0);
						depCompetenceTable.remoeAll();
						for (int i = 0; i < orglist.size(); i++) {
							JecnTreeBean jecnTreeBean = orglist.get(i);
							Vector vec = new Vector();
							vec.add(orglist.get(i).getId());
							vec.add(orglist.get(i).getName());
							((DefaultTableModel) depCompetenceTable.getModel())
									.addRow(vec);
						}
					} else {
						orgIds = "";
						depCompetenceTable.remoeAll();
					}
				}
			}
		});
		posBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// posIds = JecnCommon.setPositionLook(listPos,
				// posCompetenceArea,AddFlowDialog.this);
				PositionChooseDialog positionChooseDialog = new PositionChooseDialog(
						poslist);
				positionChooseDialog.setVisible(true);
				if (positionChooseDialog.isOperation()) {
					if (poslist != null && poslist.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : poslist) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						posIds = sbIds.substring(0, sbIds.length() - 1);
						// ((DefaultTableModel)
						// posCompetenceTable.getModel()).setRowCount(0);
						posCompetenceTable.remoeAll();
						for (int i = 0; i < poslist.size(); i++) {
							JecnTreeBean jecnTreeBean = poslist.get(i);
							Vector vec = new Vector();
							vec.add(poslist.get(i).getId());
							vec.add(poslist.get(i).getName());
							((DefaultTableModel) posCompetenceTable.getModel())
									.addRow(vec);
						}
					} else {
						posIds = "";
						posCompetenceTable.remoeAll();
					}
				}
			}
		});
		posGroupBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PosGropChooseDialog posGroupChooseDialog = new PosGropChooseDialog(
						posGroupList);
				posGroupChooseDialog.setVisible(true);
				if (posGroupChooseDialog.isOperation()) {
					if (posGroupList != null && posGroupList.size() > 0) {
						StringBuffer sbIds = new StringBuffer();
						StringBuffer sbNames = new StringBuffer();
						for (JecnTreeBean o : posGroupList) {
							sbIds.append(o.getId() + ",");
							sbNames.append(o.getName() + "/");
						}
						posGroupIds = sbIds.substring(0, sbIds.length() - 1);
						// ((DefaultTableModel)
						// posCompetenceTable.getModel()).setRowCount(0);
						posGroupCompetenceTable.remoeAll();
						for (int i = 0; i < posGroupList.size(); i++) {
							JecnTreeBean jecnTreeBean = posGroupList.get(i);
							Vector vec = new Vector();
							vec.add(posGroupList.get(i).getId());
							vec.add(posGroupList.get(i).getName());
							((DefaultTableModel) posGroupCompetenceTable.getModel())
									.addRow(vec);
						}
					} else {
						posGroupIds = "";
						posGroupCompetenceTable.remoeAll();
					}
				}
			}
		});
		verfyLab.setForeground(Color.red);
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
		// 布局
		initLayout();
	}

	/***************************************************************************
	 * 布局
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(3, 1, 3, 1);
		Insets insetsCo = new Insets(5, 1, 5, 1);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());
		// ============流程名称====================================
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(flowNameLab, c);
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		infoPanel.add(flowField, c);
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(flowShowLab, c);
		// /================流程名称验证提示信息============================
//		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
//				0, 0);
//		infoPanel.add(flowNameVerif, c);
		// ==============密级==========================================
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(isPublicLab, c);
		c = new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		infoPanel.add(isPublicCombox, c);
		// 空面板
//		c = new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
//		infoPanel.add(isPublicEmptyLab, c);
		// ==============流程编号==========================================
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(flowNumLab, c);
		c = new GridBagConstraints(1, 4, 2, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		infoPanel.add(flowNumField, c);
		// ==============流程编号验证提示信息==========================================
//		c = new GridBagConstraints(1, 5, 2, 1, 1.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
//				0, 0);
//		infoPanel.add(flowNumVerif, c);
		// ==============流程客户==========================================
		c = new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(flowClientLab, c);
		c = new GridBagConstraints(1, 6, 2, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		infoPanel.add(flowClientField, c);
		// ==============流程客户验证提示信息==========================================
//		c = new GridBagConstraints(1, 7, 2, 1, 1.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
//				0, 0);
//		infoPanel.add(flowClientVerif, c);
		// ==============起始活动==========================================
		c = new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(startActivityLab, c);
		c = new GridBagConstraints(1, 8, 2, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		infoPanel.add(startActivityField, c);
		// ==============起始活动验证提示信息==========================================
//		c = new GridBagConstraints(1, 9, 2, 1, 1.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
//				0, 0);
//		infoPanel.add(flowStartVerif, c);
		// ==============终止活动==========================================
		c = new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(endActivityLab, c);
		c = new GridBagConstraints(1, 10, 2, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		infoPanel.add(endActivityField, c);
		// ==============终止活动验证提示信息==========================================
//		c = new GridBagConstraints(1, 11, 2, 1, 1.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
//				0, 0);
//		infoPanel.add(flowEndVerif, c);

		// ==============流程输入==========================================
		c = new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(flowInLab, c);
		c = new GridBagConstraints(1, 12, 2, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(flowInScrollPane, c);
		// ==============流程输入验证提示信息==========================================
//		c = new GridBagConstraints(1, 13, 2, 1, 0.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
//		infoPanel.add(flowInVerif, c);
		// ==============流程输出==========================================
		c = new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(flowOutLab, c);
		c = new GridBagConstraints(1, 14, 2, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(flowOutScrollPane, c);
		// ==============流程输出验证提示信息==========================================
//		c = new GridBagConstraints(1, 15, 2, 1, 0.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
//		infoPanel.add(flowOutVerif, c);

		// ============= 部门权限=========================================
		c = new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(depCompetenceLab, c);
		c = new GridBagConstraints(1, 16, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(depCompetenceScrollPane, c);
		c = new GridBagConstraints(2, 16, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(depBut, c);
//		c = new GridBagConstraints(1, 17, 2, 1, 0.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
//		infoPanel.add(orglineButtomLab, c);

		// ============= 岗位权限=========================================
		c = new GridBagConstraints(0, 18, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(posCompetenceLab, c);
		c = new GridBagConstraints(1, 18, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(posCompetenceScrollPane, c);
		c = new GridBagConstraints(2, 18, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(posBut, c);
//		c = new GridBagConstraints(1, 19, 2, 1, 1.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
//				0, 0);
//		infoPanel.add(poslineButtomLab, c);

		//================岗位组权限=======================================
		c = new GridBagConstraints(0, 20, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(posGroupCompetenceLab, c);
		c = new GridBagConstraints(1, 20, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(posGroupCompetenceScrollPane, c);
		c = new GridBagConstraints(2, 20, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(posGroupBut, c);
//		c = new GridBagConstraints(1, 21, 2, 1, 1.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
//				0, 0);
//		infoPanel.add(poslineButtomLab, c);
		// ==============支持工具==========================================
		c = new GridBagConstraints(0, 22, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetsCo, 0,
				0);
		infoPanel.add(supporToolLab, c);
		c = new GridBagConstraints(1, 22, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsCo, 0, 0);
		infoPanel.add(supporToolArea, c);
		c = new GridBagConstraints(2, 22, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsCo, 0, 0);
		infoPanel.add(toolBut, c);
		// ==============附件==========================================
		c = new GridBagConstraints(0, 23, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetsCo, 0,
				0);
		infoPanel.add(mapAttchMentLab, c);
		c = new GridBagConstraints(1, 23, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsCo, 0, 0);
		infoPanel.add(attchMentFiled, c);
		c = new GridBagConstraints(2, 23, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsCo, 0, 0);
		infoPanel.add(attchMentBut, c);

		// 按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);

	}

	/**
	 * 支持工具选择
	 */
	private void toolButPerformed() {
		FlowToolChooseDialog flowToolChooseDialog = new FlowToolChooseDialog(
				listFlowTool);
		// flowToolChooseDialog.setSelectMutil(false);
		flowToolChooseDialog.setVisible(true);
		if (flowToolChooseDialog.isOperation()) {
			if (listFlowTool.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : listFlowTool) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				sptIds = sbIds.substring(0, sbIds.length() - 1);
				supporToolArea.setText(sbNames.substring(0,
						sbNames.length() - 1));
			} else {
				supporToolArea.setText("");
				sptIds = null;
			}
		}
	}

	/**
	 * 附件选择
	 */
	private void attchMentButPerformed() {
		FileChooseDialog fileChooseDialog = new FileChooseDialog(listFile,
				OverviewInformationDialog.this);
		// 只能选择一个文件
		fileChooseDialog.setSelectMutil(false);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			if (listFile.size() > 0) {
				JecnTreeBean o = listFile.get(0);
				attchMentFiled.setText(o.getName());
				fileId = o.getId();
			} else {
				listFile.clear();
				attchMentFiled.setText("");
				fileId = null;
			}
		}
	}

	/**
	 * 确定
	 */
	private void okButPerformed() {
		String flowName=flowField.getText().trim();
		// 流程编号验证
		if (DrawCommon.checkNameMaxLength(flowNumField.getText())) {
			verfyLab.setText(JecnProperties.getValue("actBaseNum")+JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 流程客户验证
		if (DrawCommon.checkNameMaxLength(flowClientField.getText())) {
			verfyLab.setText(JecnProperties.getValue("flowClient")+JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 起始活动验证
		if (DrawCommon.checkNameMaxLength(startActivityField.getText())) {
			verfyLab.setText(JecnProperties.getValue("startActiveC").substring(0,
					JecnProperties.getValue("startActiveC").length() - 1)+JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 终止活动验证
		if (DrawCommon.checkNameMaxLength(endActivityField.getText())) {
			verfyLab.setText(JecnProperties.getValue("endActiveC").substring(0,
					JecnProperties.getValue("endActiveC").length() - 1)+JecnProperties.getValue("lengthNotBaiOut"));
			return;
		}
		// 流程输入验证
		if (DrawCommon.checkNoteLength(flowInField.getText())) {
			verfyLab.setText(JecnProperties.getValue("actIn")+JecnProperties.getValue("lengthNotOut"));
			return;
		}
		// 流程输出验证
		if (DrawCommon.checkNoteLength(flowOutField.getText())) {
			verfyLab.setText(JecnProperties.getValue("actOut")+JecnProperties.getValue("lengthNotOut"));
			return;
		}

		if (!flowNumField.getText().equals(flowStructureT.getFlowIdInput())) {
			refreshFlowNode(flowNumField.getText());
		}
		flowStructureT.setFlowName(flowName);
		flowStructureT.setIsPublic(Long.valueOf(this.isPublicCombox
				.getSelectedIndex()));
		flowStructureT.setFlowIdInput(this.flowNumField.getText());

		flowBasicInfoT.setFlowName(flowName);
		flowBasicInfoT.setFlowCustom(this.flowClientField.getText());
		flowBasicInfoT.setFlowStartpoint(this.startActivityField.getText());
		flowBasicInfoT.setFlowEndpoint(this.endActivityField.getText());
		flowBasicInfoT.setInput(this.flowInField.getText());
		flowBasicInfoT.setOuput(this.flowOutField.getText());
		flowBasicInfoT.setFileId(fileId);
		listToolIds = new ArrayList<Long>();
		if (sptIds != null && !"".equals(sptIds)) {
			String sptid[] = sptIds.split(",");
			for (int i = 0; i < sptid.length; i++) {
				listToolIds.add(Long.valueOf(sptid[i]));
			}
		}
		
		try {
			//数据库保存数据
			ConnectionPool.getProcessAction().updateFlowBaseInformation(flowId,
					flowStructureT, flowBasicInfoT, orgIds, posIds, sptIds,
					JecnConstants.loginBean.getJecnUser().getPeopleId(),posGroupIds);
			if(desktopPane!=null){
				// 获取面板数据
				// 输入
				flowMap.getFlowOperationBean().setFlowInput(flowBasicInfoT.getInput());
				// 输出
				flowMap.getFlowOperationBean().setFlowOutput(flowBasicInfoT.getOuput());
				// 客户
				flowMap.getFlowOperationBean().setFlowCustom(flowBasicInfoT.getFlowCustom());
				//起始活动
				flowMap.getFlowOperationBean().setFlowStartActive(flowBasicInfoT.getFlowStartpoint());
				//终止活动
				flowMap.getFlowOperationBean().setFlowEndActive(flowBasicInfoT.getFlowEndpoint());
			}
			this.dispose();
			
			flashNode(flowName);
			
		} catch (Exception e) {
			log.error("okButPerformed is error", e);
		}

	}

	private void flashNode(String flowName) {
		ResourceHighEfficiencyTree tree = JecnDesignerMainPanel.getDesignerMainPanel().getTreePanel().getTree();
		JecnTreeCommon.reNameNode(tree, selectNode, flowName);
	}
	
	/**
	 * 更新缓存
	 * 
	 * @param flowNum
	 */
	private void refreshFlowNode(String flowNum) {
		String nodeUniqueIdentifier = JecnTreeCommon.getNodeUniqueIdentifier(flowId, TreeNodeType.process);
		JecnTreeNode curNode = JecnTreeCommon.getTreeNode(nodeUniqueIdentifier, JecnDesignerMainPanel
				.getDesignerMainPanel().getTree());
		curNode.getJecnTreeBean().setNumberId(flowNum);
	}

	/**
	 * 取消
	 */
	private void cancelButPerformed() {
		if (checkCancel()) {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if(optionTig==2){
				okButPerformed();
			}else if(optionTig==1){
				this.dispose();
			}
		}else{
			this.dispose();
		}
	}

	private boolean checkCancel() {
		isUpdate = false;
		// 流程名称
		if (!flowStructureT.getFlowName().equals(
				this.flowField.getText().toString())) {
			isUpdate = true;
		}
		// 流程编号
		if (flowStructureT.getFlowIdInput() != null) {
			if (!flowStructureT.getFlowIdInput().equals(
					this.flowNumField.getText().toString())) {
				isUpdate = true;
			}
		} else if (this.flowNumField.getText().toString() != null
				&& !"".equals(this.flowNumField.getText().toString())) {
			isUpdate = true;
		}
		// 客户
		if (flowBasicInfoT.getFlowCustom() != null) {
			if (!flowBasicInfoT.getFlowCustom().equals(
					this.flowClientField.getText().toString())) {
				isUpdate = true;
			}
		} else if (this.flowClientField.getText().toString() != null
				&& !"".equals(this.flowClientField.getText().toString())) {
			isUpdate = true;
		}

		// 起始活动
		if (flowBasicInfoT.getFlowStartpoint() != null) {
			if (!flowBasicInfoT.getFlowStartpoint().equals(
					this.startActivityField.getText().toString())) {
				isUpdate = true;
			}
		} else if (this.startActivityField.getText().toString() != null
				&& !"".equals(this.startActivityField.getText().toString())) {
			isUpdate = true;
		}
		// 终止活动
		if (flowBasicInfoT.getFlowEndpoint() != null) {
			if (!flowBasicInfoT.getFlowEndpoint().equals(
					this.endActivityField.getText().toString())) {
				isUpdate = true;
			}
		} else if (this.endActivityField.getText().toString() != null
				&& !"".equals(this.endActivityField.getText().toString())) {
			isUpdate = true;
		}
		// 输入
		if (flowBasicInfoT.getInput() != null) {
			if (!flowBasicInfoT.getInput().equals(
					this.flowInField.getText().toString())) {
				isUpdate = true;
			}
		} else if (this.flowInField.getText().toString() != null
				&& !"".equals(this.flowInField.getText().toString())) {
			isUpdate = true;
		}

		// 输出
		if (flowBasicInfoT.getOuput() != null) {
			if (!flowBasicInfoT.getOuput().equals(
					this.flowOutField.getText().toString())) {
				isUpdate = true;
			}
		} else if (this.flowOutField.getText().toString() != null
				&& !"".equals(this.flowOutField.getText().toString())) {
			isUpdate = true;
		}
		return isUpdate;
	}

	class VerifLabel extends JLabel {
		public VerifLabel() {
			initComponents();
		}

		private void initComponents() {
			// 项的内容大小
			dimension = new Dimension(450, 15);
			this.setPreferredSize(dimension);
			this.setMinimumSize(dimension);
			this.setMaximumSize(dimension);
			this.setForeground(Color.red);
		}
	}
}
