package com.jecn.epros.security;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 人员适用部门对应的流程ID
 */
public class OrgScope implements Serializable {

    /**
     * 适用部门对应的流程ID的上级流程ID(一直到顶级)
     */
    private Map<Long, Set<Long>> orgToPid = new HashMap<>();
    /**
     * 适用部门对应的流程ID
     */
    private Map<Long, Set<Long>> orgToId = new HashMap<>();

    public Map<Long, Set<Long>> getOrgToPid() {
        return orgToPid;
    }

    public void setOrgToPid(Map<Long, Set<Long>> orgToPid) {
        this.orgToPid = orgToPid;
    }

    public Map<Long, Set<Long>> getOrgToId() {
        return orgToId;
    }

    public void setOrgToId(Map<Long, Set<Long>> orgToId) {
        this.orgToId = orgToId;
    }
}
