package epros.designer.gui.process.guide.explain;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessInterface;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 流程接口描述
 * 
 * @author user
 * 
 */
public class JecnProcessInterfaces extends JecnFileDescriptionComponent {

	private final Logger log = Logger.getLogger(JecnProcessInterfaces.class);
	/** 对应的上层流程 **/
	private ParentTable parentTable;
	/** 上游流程 **/
	private InterfaceTable upperTable;
	/** 下游流程 **/
	private InterfaceTable lowerTable;
	/** 过程接口流程 **/
	private InterfaceTable proceduralTable;
	/** 子流程 **/
	private InterfaceTable sonTable;
	private List<ProcessInterface> interfaces;

	private ProcessOperationDialog processOperationDialog;

	public JecnProcessInterfaces(int index, String name, boolean isRequest, JecnPanel contentPanel,
			ProcessOperationDialog processOperationDialog, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.processOperationDialog = processOperationDialog;
		this.interfaces = queryInterfaces();
		parentTable = new ParentTable();
		upperTable = new UpperTable();
		lowerTable = new LowerTable();
		proceduralTable = new ProceduralTable();
		parentTable.setTableColumn(1, 180);

		sonTable = new SonTable();

		GuideJScrollPane parentPanel = new GuideJScrollPane();
		parentPanel.setViewportView(parentTable);
		parentPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		GuideJScrollPane upperPanel = new GuideJScrollPane();
		upperPanel.setViewportView(upperTable);
		upperPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		GuideJScrollPane lowerPanel = new GuideJScrollPane();
		lowerPanel.setViewportView(lowerTable);
		lowerPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		GuideJScrollPane proceduralPanel = new GuideJScrollPane();
		proceduralPanel.setViewportView(proceduralTable);
		proceduralPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		GuideJScrollPane sonPanel = new GuideJScrollPane();
		sonPanel.setViewportView(sonTable);
		sonPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBorder(BorderFactory.createTitledBorder(""));
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(new JLabel("*" + JecnProperties.getValue("correspondingUpProcess")), c);

		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(parentPanel, c);

		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(new JLabel("*" + JecnProperties.getValue("inProcessDescribe")), c);

		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(upperPanel, c);

		c = new GridBagConstraints(0, 4, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(new JLabel("*" + JecnProperties.getValue("outProcessDescribed")), c);

		c = new GridBagConstraints(0, 5, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(lowerPanel, c);

		c = new GridBagConstraints(0, 6, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(new JLabel("*" + JecnProperties.getValue("interfaceFlow")), c);

		c = new GridBagConstraints(0, 7, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(proceduralPanel, c);

		c = new GridBagConstraints(0, 8, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(new JLabel("*" + JecnProperties.getValue("sonFlow")), c);

		c = new GridBagConstraints(0, 9, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(sonPanel, c);

	}

	private List<ProcessInterface> queryInterfaces() {
		Long id = processOperationDialog.getCurWorkNode().getJecnTreeBean().getId();
		try {
			return ConnectionPool.getProcessAction().findFlowInterfaces(id, false);
		} catch (Exception e) {
			log.error("", e);
		}
		return new ArrayList<ProcessInterface>();
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

	/**
	 * 上层接口
	 * 
	 * @author user
	 * 
	 */
	class ParentTable extends JecnTable {
		@Override
		public JecnTableModel getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("flowFileName"));
			title.add(JecnProperties.getValue("fileCode"));
			// 获取操作说明数据
			Vector<Vector<String>> rowData = new Vector<Vector<String>>();
			Long pid = processOperationDialog.getCurWorkNode().getJecnTreeBean().getPid();
			if (pid != 0) {
				try {
					JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(pid);
					Vector<String> c = new Vector<String>();
					c.add(flowStructureT.getFlowName());
					c.add(flowStructureT.getFlowIdInput());
					rowData.add(c);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return new JecnTableModel(title, rowData);
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] {};
		}

		@Override
		public boolean isSelectMutil() {
			return false;
		}
	}

	abstract class InterfaceTable extends JecnTable {

		@Override
		public JecnTableModel getTableModel() {
			// 1 上层流程 2 下层流程 3 过程接口流程 4 子流程
			int type = getType();
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("flowInterface"));
			String interfaceName = "";
			if (type == 1) {
				interfaceName = JecnProperties.getValue("topFlow");
			} else if (type == 2) {
				interfaceName = JecnProperties.getValue("bottomFlow");
			} else if (type == 3) {
				interfaceName = JecnProperties.getValue("interfaceFlow");
			} else if (type == 4) {
				interfaceName = JecnProperties.getValue("sonFlow");
			}
			title.add(interfaceName);
			title.add(JecnProperties.getValue("processRequired"));
			title.add(JecnProperties.getValue("remark"));
			// 获取操作说明数据
			Vector<Vector<String>> rowData = getContent();
			return new JecnTableModel(title, rowData);
		}

		public Vector<Vector<String>> getContent() {
			// 1 上层流程 2 下层流程 3 过程接口流程 4 子流程
			int type = getType();
			Vector<Vector<String>> rowData = new Vector<Vector<String>>();
			Long pid = processOperationDialog.getCurWorkNode().getJecnTreeBean().getPid();
			if (pid == 0) {
				return rowData;
			}
			for (ProcessInterface data : interfaces) {
				if (data.getImplType() != type) {
					continue;
				}
				Vector<String> row = new Vector<String>();
				row.add(data.getImplName());
				row.add(data.getRname());
				row.add(data.getProcessRequirements());
				row.add(data.getImplNote());
				rowData.add(row);
			}
			return rowData;
		}

		abstract protected int getType();

		@Override
		public int[] gethiddenCols() {
			return new int[] {};
		}

		@Override
		public boolean isSelectMutil() {
			return false;
		}
	}

	/**
	 * 上游接口
	 * 
	 * @author user
	 * 
	 */
	class UpperTable extends InterfaceTable {
		@Override
		protected int getType() {
			return 1;
		}
	}

	/**
	 * 下游接口
	 * 
	 * @author user
	 * 
	 */
	class LowerTable extends InterfaceTable {
		@Override
		protected int getType() {
			return 2;
		}
	}

	/**
	 * 过程接口
	 * 
	 * @author user
	 * 
	 */
	class ProceduralTable extends InterfaceTable {
		@Override
		protected int getType() {
			return 3;
		}
	}

	/**
	 * 子接口
	 * 
	 * @author user
	 * 
	 */
	class SonTable extends InterfaceTable {
		@Override
		protected int getType() {
			return 4;
		}
	}

}
