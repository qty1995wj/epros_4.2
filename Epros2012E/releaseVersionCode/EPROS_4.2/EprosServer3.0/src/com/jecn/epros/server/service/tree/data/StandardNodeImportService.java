package com.jecn.epros.server.service.tree.data;

import java.io.File;

import jxl.Sheet;
import jxl.write.WritableSheet;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;

import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.data.Node;
import com.jecn.epros.server.common.IBaseDao;

public class StandardNodeImportService extends AbsNodeImportService<StandardBean> {

	public StandardNodeImportService(Long peopleId, Long pid, File file, Long projectId, IBaseDao baseDao, boolean CH) {
		super(peopleId, pid, file, projectId, baseDao, CH);
	}

	protected StandardBean getData(Sheet sheet, int line) {
		String name = getCellText(sheet, line, contentStartIndex);
		StandardBean data = getBaseData(name, 4);
		data.setStanContent(getCellText(sheet, line, contentStartIndex + 1));
		data.setNote(getCellText(sheet, line, contentStartIndex + 2));
		return data;
	}

	protected StandardBean getBaseData(String name, int type) {
		StandardBean bean = new StandardBean();
		bean.setCriterionClassName(name);
		bean.setStanType(type);
		bean.setCreateDate(now);
		bean.setUpdateDate(now);
		bean.setCreatePeopleId(peopleId);
		bean.setUpdatePeopleId(peopleId);
		bean.setProjectId(projectId);
		bean.setIsPublic(0L);
		return bean;
	}

	@Override
	protected void initRootNode(Node<StandardBean> pNode) {
		StandardBean bean = (StandardBean) baseDao.getObjectHql("from StandardBean where criterionClassId=?", pid);
		if (StringUtils.isBlank(bean.gettPath()) || StringUtils.isBlank(bean.getViewSort())) {
			throw new IllegalArgumentException("当前选中的节点的t_path或者view_sort为空。t_path:" + bean.gettPath() + " view_sort:"
					+ bean.getViewSort());
		}
		pNode.setDbId(bean.getCriterionClassId());
		pNode.setLevel(bean.gettLevel());
		pNode.setPath(bean.gettPath());
		pNode.setViewSort(bean.getViewSort());
		pNode.setData(bean);
	}

	@Override
	protected void save(Node<StandardBean> node) {
		Session s = baseDao.getSession();
		StandardBean data = node.getData();
		data.setPreCriterionClassId(node.getParent().getDbId());
		s.save(data);

		node.setDbId(data.getCriterionClassId());

		data.settLevel(node.getLevel());
		data.settPath(node.getPath());
		data.setViewSort(node.getViewSort());
		data.setSortId(node.getSort());
		s.update(data);
	}

	@Override
	protected int getReadStartLine() {
		return 1;
	}

	@Override
	protected long getRootChildsMaxSortId() {
		String sql = "select max(a.sort_id) from JECN_CRITERION_CLASSES a where a.pre_criterion_class_id = ?";
		int c = baseDao.countAllByParamsNativeSql(sql, pid);
		return c;
	}

	@Override
	protected void analysis(Sheet sheet, WritableSheet wsheet, int sheetIndex) {
		// 读取单元格确定列标题是否和模板一致
		// 条款号 条款 备注
		// 读取宽度
		contentStartIndex = getContentIndex(sheet, "条款号");
		contentEndIndex = Math.max(sheet.getColumns() - 1, 0);
		clearCol(wsheet, contentEndIndex + 1);
		if (contentStartIndex == -1) {
			writeMsg(wsheet, getColumnNotExistTip("条款"));
			errors[sheetIndex] = true;
			return;
		}
		if (columnNotExist(sheet, wsheet, sheetIndex, contentStartIndex + 1, "条款")) {
			return;
		}
		if (columnNotExist(sheet, wsheet, sheetIndex, contentStartIndex + 2, "备注")) {
			return;
		}
		contentEndIndex = contentStartIndex + 2;
		clearCol(wsheet, contentEndIndex + 1);
		dirStartIndex = 0;
		dirEndIndex = contentStartIndex - 1;
	}

	@Override
	protected boolean validateData(Sheet sheet, WritableSheet wsheet, boolean isDir, StandardBean bean, int line) {
		// 目录长度 61个汉字
		// 条款号 61个汉字
		// 条款 无限制
		// 备注 无限制
		boolean result = true;
		if (isDir && validateLenAndTipIfNotPass(wsheet, "目录", bean.getCriterionClassName(), 61, line)) {
			result = false;
		} else {
			if (validateLenAndTipIfNotPass(wsheet, "条款号", bean.getCriterionClassName(), 61, line)) {
				result = false;
			}
		}
		return result;
	}
}
