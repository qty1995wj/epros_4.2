package com.jecn.epros.server.dao.task.design.impl;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.task.design.IJecnTaskRecordDao;

/**
 * 任务审批信息验证处理类实现
 * 
 * @author Administrator
 * @date： 日期：Oct 22, 2012 时间：3:28:00 PM
 */
public class JecnTaskRecordDaoImpl extends AbsBaseDao<JecnTaskBeanNew, Long>
		implements IJecnTaskRecordDao {
	/** 日志 */
	private static Logger log = Logger.getLogger(JecnTaskRecordDaoImpl.class);

	/**
	 * @param set
	 *            人员表主键ID集合
	 * @param projectId
	 *            当前项目ID
	 * @return List<Object[]> 0:人员表真实姓名；1：人员表主键ID
	 */
	public List<Object[]> getJecnUserByUserIds(Set<Long> set, Long protectId) throws Exception {
		try {
			String sql = "select distinct u.true_name, u.people_id " + "from jecn_user u"
					+ " where u.islock = 0 and u.people_id in " + JecnCommonSql.getIdsSet(set);
			return this.listNativeSql(sql);
		} catch (Exception ex) {
			log.error("JecnTaskRecordDaoImpl类中getJecnUserByUserIds方法发生异常!", ex);
		}
		return null;
	}

	/**
	 * 判断userIds人员个数
	 * 
	 * @param userIds
	 *            人员岗位关联表主键ID集合（集合大于100 不能用此方法）
	 * @return
	 * @throws DaoException
	 */
	public int getPeopleIdCountByUserIds(Set<Long> userIds) throws Exception {
		String sql = "select count(*) from JecnUser u where u.peopleId in "
				+ "(select t.peopleId from JecnUserInfo t  "
				+ "where t.userId in " + JecnCommonSql.getIdsSet(userIds) + ")";
		return Integer.parseInt(getSession().createQuery(sql).uniqueResult().toString());
	}

	/**
	 * 验证版本号是否存在
	 * 
	 * @param id
	 *            流程或制度ID
	 * @param version
	 *            版本号
	 * @param type
	 *            类型 0 流程，1文件，2制度
	 * @return true 版本好存在
	 * @throws DaoException
	 */
	public List<Long> isExistVersionbyFlowId(Long id, String version, int type)
			throws Exception {
		try {
			String hql = "select id from JecnTaskHistoryNew where relateId=? and versionId=? and type=?";
			List<Long> list = this.listHql(hql, id, version, type);
			return list;
		} catch (RuntimeException ex) {
			log.error("JecnFlowHistoryDAO类中findByVersion方法出现异常", ex);
		}
		return null;
	}

	/**
	 * 是否处于任务中
	 * 
	 * @param rid
	 * @param type
	 *            0为流程任务，1为文件任务，2为制度任务
	 * @return int ==1PRF处于任务中
	 * @throws Exception
	 */
	@Override
	public int isBeInTask(Long rid, int type) throws Exception {
		String hql = "select count(id) from JecnTaskBeanNew where rid=? and taskType=? and state<>5 and state<>0";
		return this.countAllByParamsHql(hql, rid, type);
	}
}
