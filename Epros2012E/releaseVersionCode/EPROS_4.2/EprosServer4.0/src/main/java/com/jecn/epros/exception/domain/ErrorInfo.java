package com.jecn.epros.exception.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;

public class ErrorInfo {

    private String url;
    private String msg;
    private HttpStatus statusCode;
    private Throwable cause;

    @JsonIgnore
    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public Integer getErrorCode() {
        return statusCode == null ? null : statusCode.value();
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @JsonIgnore
    public Throwable getCause() {
        return cause;
    }

    public void setCause(Throwable cause) {
        this.cause = cause;
    }

}
