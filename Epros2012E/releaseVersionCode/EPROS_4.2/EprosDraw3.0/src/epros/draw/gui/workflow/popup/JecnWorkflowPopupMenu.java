package epros.draw.gui.workflow.popup;

import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.designer.JecnDesignerToolData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 右键点击流程元素弹出菜单处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnWorkflowPopupMenu {

	/** 右键点击流程元素弹出菜单 */
	private JPopupMenu workflowPopupMenu = null;

	/** 自动编号 */
	private JecnMenuItem antoNumMenuItem = null;

	/** 角色移动 */
	private JecnMenuItem roleMobileMenuItem = null;

	/** 粘贴 */
	private JecnMenuItem pasteMenuItem = null;
	/** 全选 */
	private JecnMenuItem checkAllMenuItem = null;

	/** 插入横分割线 */
	private JecnMenuItem insertPLineMenuItem = null;
	/** 插入竖分割线 */
	private JecnMenuItem insertVLineMenuItem = null;
	/** 画图面板类型 */
	private MapType mapType = null;

	/*** 导出VISIO *****/
	private JecnMenuItem outputVisio = null;
	/*** 导入VISIO *****/
	private JecnMenuItem inputVisio = null;

	/** 添加泳道 */
	private JMenuItem addModelLineItem = null;

	public JecnWorkflowPopupMenu(MapType mapType) {
		this.mapType = mapType;
		initComponent();
		initLayout();
		initDesMenuItems();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponent() {

		// 右键点击流程元素弹出菜单
		workflowPopupMenu = new JPopupMenu();
		// 自动编号
		antoNumMenuItem = new JecnMenuItem(this, ToolBarElemType.formatAntoNum, null, null);
		// 角色移动
		roleMobileMenuItem = new JecnMenuItem(this, ToolBarElemType.formatRoleMove, "formatRoleMove", "formatRoleMove");
		// 粘贴
		pasteMenuItem = new JecnMenuItem(this, ToolBarElemType.editPaste, null, null);
		// 全选
		checkAllMenuItem = new JecnMenuItem(this, ToolBarElemType.allSelected, null, null);

		// 插入横分割线
		insertPLineMenuItem = new JecnMenuItem(this, ToolBarElemType.formatPLine, "formatPLine", "insertPLine");
		// 插入竖分割线
		insertVLineMenuItem = new JecnMenuItem(this, ToolBarElemType.formatVLine, "formatVLine", "insertVLine");

		// 弹出菜单背景颜色
		workflowPopupMenu.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 粘贴 Ctrl+V
		pasteMenuItem.setText(pasteMenuItem.getText() + " Ctrl+V");
		// 全选 Ctrl+A
		checkAllMenuItem.setText(checkAllMenuItem.getText() + " Ctrl+A");
		// visio导出
		outputVisio = new JecnMenuItem(this, ToolBarElemType.outputVisio, "outputVisio", "outputVisio");
		// visio导入
		inputVisio = new JecnMenuItem(this, ToolBarElemType.inputVisio, "inputVisio", "inputVisio");

		addModelLineItem = new JecnMenuItem(this, ToolBarElemType.modelLine, "modelLine", "modelLine");
	}

	/**
	 * 
	 * 添加菜单项
	 * 
	 */
	private void initLayout() {
		if (mapType == null) { // 画图面板类型
			return;
		}
		switch (mapType) {
		case totalMap: // 流程地图
			workflowPopupMenu.add(pasteMenuItem);
			workflowPopupMenu.add(checkAllMenuItem);
			break;
		case partMap: // 流程图
			// 添加菜单项
			workflowPopupMenu.add(antoNumMenuItem);
			workflowPopupMenu.add(roleMobileMenuItem);
			// 插入线
			workflowPopupMenu.addSeparator();
			workflowPopupMenu.add(pasteMenuItem);
			workflowPopupMenu.add(checkAllMenuItem);

			workflowPopupMenu.addSeparator();
			workflowPopupMenu.add(addModelLineItem);
			workflowPopupMenu.add(insertPLineMenuItem);
			workflowPopupMenu.add(insertVLineMenuItem);
			break;
		case orgMap: // 组织图
			workflowPopupMenu.add(pasteMenuItem);
			workflowPopupMenu.add(checkAllMenuItem);
			break;
		default:
			break;
		}
	}

	// 添加visio导入导出
	public void addVisioOption() {
		workflowPopupMenu.addSeparator();
		workflowPopupMenu.add(inputVisio);
		workflowPopupMenu.add(outputVisio);
	}

	/**
	 * 
	 * 添加设计器的菜单项
	 * 
	 */
	private void initDesMenuItems() {
		if (mapType == null) { // 画图面板类型
			return;
		}
		switch (mapType) { // 添加菜单项
		case totalMap: // 流程地图
			addMenuItemList(JecnDesignerToolData.getTotalMapWorkflowMenuItemlist());
			break;
		case partMap: // 流程图
			addMenuItemList(JecnDesignerToolData.getPartMapWorkflowMenuItemlist());
			break;
		case orgMap: // 组织图
			addMenuItemList(JecnDesignerToolData.getOrgMapWorkflowMenuItemlist());
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 添加菜单项
	 * 
	 */
	private void addMenuItemList(List<JMenuItem> menuItemlist) {
		if (menuItemlist == null || menuItemlist.size() == 0) {
			return;
		}
		// 插入线
		workflowPopupMenu.addSeparator();
		for (JMenuItem menuItem : menuItemlist) {
			if (menuItem == null) {
				// 插入线
				workflowPopupMenu.addSeparator();
				continue;
			}
			workflowPopupMenu.add(menuItem);
		}
	}

	/**
	 * 
	 * 设置角色移动图标（选中，未选中）
	 * 
	 */
	public void setRoleMoveIcon() {
		JCheckBox roleMoveCheckBox = JecnDrawMainPanel.getMainPanel().getToolbar().getFormatToolBarItemPanel()
				.getRoleMoveCheckBox();
		// 判断角色移动是否选中
		if (roleMoveCheckBox.isSelected()) {
			// 角色移动
			roleMobileMenuItem.setIcon(JecnFileUtil.getToolBarImageIcon("formatRoleMoveSelected"));
		} else {
			// 角色移动
			roleMobileMenuItem.setIcon(JecnFileUtil.getToolBarImageIcon("formatRoleMove"));
		}
	}

	public JPopupMenu getWorkflowPopupMenu() {
		return workflowPopupMenu;
	}

	public JecnMenuItem getRoleMobileMenuItem() {
		return roleMobileMenuItem;
	}

}
