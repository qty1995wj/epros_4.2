package epros.designer.gui.error;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import epros.designer.gui.error.ErrorCommon.CheckErrorData;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.figure.unit.JecnFigureRefManhattanLine;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

public class TaskTool {

	/**
	 * 获取面板上所有的活动和角色
	 * 
	 * @param listActiveFigure
	 *            List<JecnBaseActiveFigure> 面板所有的活动集合
	 * @param listRoleFigure
	 *            List<JecnBaseRoleFigure> 面板所有的角色集合
	 * @param listAllCustomFigure
	 *            List<JecnBaseRoleFigure> 面板所有的客户集合
	 */
	public static void findActiveListAndRoleList(CheckErrorData errorCheckData) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		List<JecnBaseFlowElementPanel> panelList = JecnDrawMainPanel.getMainPanel().getWorkflow().getPanelList();
		for (JecnBaseFlowElementPanel figurePanel : panelList) {
			if (figurePanel.getFlowElementData().getMapElemType() == MapElemType.DottedRect) {// 泳池获取泳池包含的活动集合
				errorCheckData.getListAllDottedRect().add((JecnBaseFigurePanel) figurePanel);
			} else if (MapElemType.CustomFigure == figurePanel.getFlowElementData().getMapElemType()) {// 客户
				JecnBaseRoleFigure customFigure = (JecnBaseRoleFigure) figurePanel;
				// 角色和活动关联 默认false
				customFigure.getFlowElementData().setRefActive(false);
				errorCheckData.getListAllCustomFigure().add(customFigure);
			} else if (DrawCommon.isRole(figurePanel.getFlowElementData().getMapElemType())) {
				JecnBaseRoleFigure roleFigure = (JecnBaseRoleFigure) figurePanel;
				// 角色和活动关联 默认false
				roleFigure.getFlowElementData().setRefActive(false);
				errorCheckData.getListAllRoleFigure().add(roleFigure);
			} else if (DrawCommon.isActive(figurePanel.getFlowElementData().getMapElemType())) {
				JecnBaseActiveFigure activeFigure = (JecnBaseActiveFigure) figurePanel;
				activeFigure.getFlowElementData().setRefRole(false);
				errorCheckData.getListAllActiveFigure().add(activeFigure);
			}
		}

		if (errorCheckData.getIsExistLine() == 1) {// 启用活动必须存在连接线验证
			// 设置协作框包含活动集合
			getDottedRectContainsActives(errorCheckData);
		}
	}

	private static Map<JecnBaseFigurePanel, List<JecnBaseFigurePanel>> getDottedRectContainsActives(
			CheckErrorData errorCheckData) {
		Map<JecnBaseFigurePanel, List<JecnBaseFigurePanel>> dottedRectContainsActives = errorCheckData
				.getDottedRectContainsActives();
		for (JecnBaseFigurePanel dottedFigure : errorCheckData.getListAllDottedRect()) {
			for (JecnBaseActiveFigure activeFigure : errorCheckData.getListAllActiveFigure()) {
				if (DrawCommon.isActiveInDottedRectRange(activeFigure.getFlowElementData(), dottedFigure
						.getFlowElementData())) {// 活动在协作框范围内

					if (dottedRectContainsActives.get(dottedFigure) == null) {
						dottedRectContainsActives.put(dottedFigure, new ArrayList<JecnBaseFigurePanel>());
					}
					dottedRectContainsActives.get(dottedFigure).add(activeFigure);
				}
			}
		}
		return dottedRectContainsActives;
	}

	/**
	 * 活动是否在协作框中
	 * 
	 * @param dottedFigure
	 * @param activeFigure
	 * @return
	 */
	private static JecnBaseFigurePanel isActivesInDottedRectRange(List<JecnBaseFigurePanel> listAllDottedRect,
			JecnBaseFigurePanel activeFigure) {
		for (JecnBaseFigurePanel dottedFigure : listAllDottedRect) {
			if (DrawCommon.isActiveInDottedRectRange(activeFigure.getFlowElementData(), dottedFigure
					.getFlowElementData())) {
				return dottedFigure;
			}
		}
		return null;
	}

	/**
	 * 判断图形是否存在连接线
	 * 
	 * @param activeFigure
	 *            JecnBaseFigurePanel图形元素
	 * @return boolean True:存在连接线
	 */
	public static boolean isExistManLine(JecnBaseFigurePanel activeFigure, CheckErrorData checkErrorData) {
		if (activeFigure == null) {
			return true;
		}
		if (activeFigure.getListFigureRefManhattanLine().size() == 0) {// 不存在连接线
			JecnBaseFigurePanel dottedFigure = isActivesInDottedRectRange(checkErrorData.getListAllDottedRect(),
					activeFigure);
			if (dottedFigure == null) {
				return false;
			}
			if (dottedFigure.getListFigureRefManhattanLine().size() == 0
					&& !isInDottedActivesExistManLine(checkErrorData.getDottedRectContainsActives().get(dottedFigure))) {// 泳池也不存在连接线
				return false;
			} else {// 存在连接线
				return isExistInAndOutLine(dottedFigure.getListFigureRefManhattanLine());
			}
		}
		return isExistInAndOutLine(activeFigure.getListFigureRefManhattanLine());
	}

	private static boolean isInDottedActivesExistManLine(List<JecnBaseFigurePanel> activeFigures) {
		boolean isExistLine = false;
		for (JecnBaseFigurePanel activeFigure : activeFigures) {
			if (activeFigure.getListFigureRefManhattanLine().size() == 0) {
				continue;
			}
			isExistLine = isExistInAndOutLine(activeFigure.getListFigureRefManhattanLine());
		}
		return isExistLine;
	}

	private static boolean isExistInAndOutLine(List<JecnFigureRefManhattanLine> lines) {
		boolean isExistIn = false;
		boolean isExistOut = false;
		for (JecnFigureRefManhattanLine jecnFigureRefManhattanLine : lines) {
			if (!isExistOut && jecnFigureRefManhattanLine.isStart()) {
				isExistOut = true;
			} else if (!isExistIn && !jecnFigureRefManhattanLine.isStart()) {
				isExistIn = true;
			}
		}
		return isExistIn && isExistOut;
	}

	/**
	 * 以下图形元素可以不存在连接线
	 * 
	 * @param figure
	 *            JecnBaseFigurePanel图形元素
	 * @return true 是可以不存在连接线的图形
	 */
	public static boolean isNOLinesFigure(JecnBaseFigurePanel figure) {
		if(MapElemType.FreeText == figure.getFlowElementData().getEleShape()){
			return true;
		}
		MapElemType mapElemType = figure.getFlowElementData().getMapElemType();
		switch (mapElemType) {
		case IconFigure:
		case RoleFigure:
		case FreeText:
		case CommentText:
		case CustomFigure:
		case XORFigure:
		case ORFigure:
		case ToFigure:
		case FromFigure:
		case DataImage:
		case FileImage:
		case KSFFigure:
		case PAFigure:
		case KCPFigure:
		case KCPFigureComp:
		case DottedRect:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * 判断图形是否压线
	 * 
	 * @param figure
	 *            图形元素
	 * @return true 存在图形压线
	 */
	public static boolean checkFigureCoverLine(JecnBaseFigurePanel figure) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return false;
		}
		if (desktopPane.getFlowMapData().isHFlag()) {// 横向图
			for (JecnBaseVHLinePanel baseVHLinePanel : desktopPane.getVhLinePanelList()) {
				if (baseVHLinePanel instanceof ParallelLines) {
					ParallelLines parallelLines = (ParallelLines) baseVHLinePanel;
					if (figure.getY() <= parallelLines.getY()
							&& figure.getY() + figure.getHeight() > parallelLines.getY()) {
						return true;
					}
				} else if (baseVHLinePanel instanceof VerticalLine) {// 纵线
					VerticalLine verticalLine = (VerticalLine) baseVHLinePanel;
					if (figure.getX() <= verticalLine.getX() && figure.getX() + figure.getWidth() > verticalLine.getX()) {
						return true;
					}
				}
			}
		} else {// 纵向图
			for (JecnBaseVHLinePanel baseVHLinePanel : desktopPane.getVhLinePanelList()) {
				if (baseVHLinePanel instanceof VerticalLine) {
					VerticalLine verticalLine = (VerticalLine) baseVHLinePanel;
					if (figure.getX() <= verticalLine.getX() && figure.getX() + figure.getWidth() > verticalLine.getX()) {
						return true;

					}
				}
			}
		}
		return false;

	}

	/**
	 * 获取角色活动关联(设置角色、活动是否存在对应关系)
	 * 
	 * @param activeFigureDataList
	 *            活动集合
	 * @param roleFigureDataList
	 *            角色集合
	 * @param listVHLinePanel
	 *            分割线集合
	 * @return 角色与活动关联集合
	 */
	public static void setActiveIdForRoleFigure(List<JecnBaseActiveFigure> listActiveFigure,
			List<JecnBaseRoleFigure> listRoleFigure) {
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || listActiveFigure == null || listRoleFigure == null) {
			JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.error(JecnProperties.getValue("data_If_Null")
					+ "listActiveFigure" + listActiveFigure + "listRoleData" + listRoleFigure + "desktopPane"
					+ desktopPane);
			return;
		}

		// 记录活动ID， 每个活动只能对应一个角色，存在说明已经有对应关系，重复说明活动对应多个角色
		List<Long> activeIdList = new ArrayList<Long>();

		List<JecnBaseVHLinePanel> listVHLinePanel = desktopPane.getVhLinePanelList();
		// 循环角色
		for (JecnBaseRoleFigure roleFigure : listRoleFigure) {
			if (desktopPane.getFlowMapData().isHFlag()) { // 面板横向
				int oneVHLine = 0;
				int twoVHLine = desktopPane.getHeight();
				for (JecnBaseVHLinePanel vhLinePanel : listVHLinePanel) {
					// 获取分割线
					JecnVHLineData vHLineData = (JecnVHLineData) vhLinePanel.getFlowElementData();
					// 循环判断小于角色位置点的分割线，并且离角色最近的一条
					if (oneVHLine < vHLineData.getVhY()
							&& vHLineData.getVhY() < roleFigure.getFlowElementData().getFigureDataCloneable().getY()) {
						oneVHLine = (int) vHLineData.getVhY();

					}
					if (twoVHLine > vHLineData.getVhY()
							&& vHLineData.getVhY() > roleFigure.getFlowElementData().getFigureDataCloneable().getY()) {
						twoVHLine = (int) vHLineData.getVhY();
					}
				}
				if (listVHLinePanel.size() > 0) {
					// 数据不为默认值
					for (JecnBaseActiveFigure activeFigure : listActiveFigure) {
						if (activeFigure.getFlowElementData().getFigureDataCloneable().getY() < twoVHLine
								&& activeFigure.getFlowElementData().getFigureDataCloneable().getY() > oneVHLine) {

							if (!activeIdList.contains(activeFigure.getFlowElementData().getFlowElementId())) {
								activeIdList.add(activeFigure.getFlowElementData().getFlowElementId());
							} else if (activeFigure.getFlowElementData().getFlowElementId() != -1) {// 保存过得元素ID不是-1
								// 多个角色在同一行（相邻的两条分割线间）
								roleFigure.getFlowElementData().setMoreRefActive(true);
								continue;
							}
							// 活动和角色关联
							activeFigure.getFlowElementData().setRefRole(true);
							// 角色和活动关联
							roleFigure.getFlowElementData().setRefActive(true);
						}
					}
				}
			} else { // 面板纵向
				int oneVHLine = 0;
				int twoVHLine = desktopPane.getWidth();
				for (JecnBaseVHLinePanel vhLinePanel : listVHLinePanel) {
					// 获取分割线
					JecnVHLineData vHLineData = (JecnVHLineData) vhLinePanel.getFlowElementData();
					// 循环判断小于角色位置点的分割线，并且离角色最近的一条
					if (oneVHLine < vHLineData.getVhX()
							&& vHLineData.getVhX() < roleFigure.getFlowElementData().getFigureDataCloneable().getX()) {
						oneVHLine = (int) vHLineData.getVhX();

					}
					if (twoVHLine > vHLineData.getVhX()
							&& vHLineData.getVhX() > roleFigure.getFlowElementData().getFigureDataCloneable().getX()) {
						twoVHLine = (int) vHLineData.getVhX();
					}
				}
				if (listVHLinePanel.size() > 0) {
					// 数据不为默认值
					for (JecnBaseActiveFigure activeFigure : listActiveFigure) {
						if (activeFigure.getFlowElementData().getFigureDataCloneable().getX() < twoVHLine
								&& activeFigure.getFlowElementData().getFigureDataCloneable().getX() > oneVHLine) {
							if (!activeIdList.contains(activeFigure.getFlowElementData().getFlowElementId())) {
								activeIdList.add(activeFigure.getFlowElementData().getFlowElementId());
							} else if (activeFigure.getFlowElementData().getFlowElementId() != -1) {// 保存过得元素ID不是-1
								// 多个角色在同一行（相邻的两条分割线间）
								roleFigure.getFlowElementData().setMoreRefActive(true);
								continue;
							}
							// 活动和角色关联
							activeFigure.getFlowElementData().setRefRole(true);
							// 角色和活动关联
							roleFigure.getFlowElementData().setRefActive(true);
						}
					}
				}
			}
		}
	}
}
