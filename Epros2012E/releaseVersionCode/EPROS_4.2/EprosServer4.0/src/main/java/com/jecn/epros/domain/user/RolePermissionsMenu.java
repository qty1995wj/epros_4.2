package com.jecn.epros.domain.user;

/**
 * Created by lenovo on 2017/1/3.
 */
public class RolePermissionsMenu {

    private String guId;
    private String roleId;
    private String menuId;

    public String getGuId() {
        return guId;
    }

    public void setGuId(String guId) {
        this.guId = guId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }
}
