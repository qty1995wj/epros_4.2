package epros.designer.tree;

import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.treeCheckbox.JecnCheckBoxTree;
import epros.designer.util.JecnProperties;

public abstract class RecycleTree extends  JecnCheckBoxTree{
	/** 节点类型 0是流程地图；1是流程；2是组织 */
	private int nodeType = -1;
	private Long id = null;

	public RecycleTree(Long id, int nodeType) {
		this.nodeType = nodeType;
		this.id = id;
		JecnTreeNode node = null;
		if (nodeType == 0) {
			node = JecnTreeCommon.getTreeNode(TreeNodeType.processMap
					+ id.toString(), this);

		} else if (nodeType == 1) {
			node = JecnTreeCommon.getTreeNode(TreeNodeType.process
					+ id.toString(), this);
		} else if (nodeType == 2) {
			node = JecnTreeCommon.getTreeNode(TreeNodeType.organization
					+ id.toString(), this);
		}
		if (node != null) {
			JecnTreeCommon.selectNode(this, node);
		}
	}


	@Override
	public JecnTreeModel getTreeModel() {
		return getModel(getTreeData());
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {

	}
	@Override
	public Long getTreeId(){
		return id;
	}
	@Override
	public JecnTreeListener getTreeExpansionListener(JecnCheckBoxTree checkTree) {
		return new CheckBoxTreeListener(this);
	}
	//展开所有节点
	class CheckBoxTreeListener extends JecnTreeListener{
		private RecycleTree recyTree;
		public CheckBoxTreeListener(RecycleTree recyTree){
			this.recyTree = recyTree;
		}
		@Override
		public void treeCollapsed(TreeExpansionEvent event) {
			
		}

		@Override
		public void treeExpanded(TreeExpansionEvent event) {
			JecnTreeCommon.expandTree(recyTree);
		}
		
	}
	/**
	 * @author zhangchen Aug 8, 2012
	 * @description：获得显示树的Bean
	 * @return
	 */
	public abstract List<JecnTreeBean> getTreeData();

	/**
	 * @author zhangchen Aug 8, 2012
	 * @description：节点类型 0是流程；1是流程地图；2是组织
	 * @return
	 */
	public abstract int viewType();

	private JecnTreeModel getModel(List<JecnTreeBean> list) {
		// 根节点
		JecnTreeNode rootNode = null;
		nodeType = viewType();
		if (nodeType == 0 || nodeType == 1) {
			rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.processRoot,
					JecnProperties.getValue("process"));
		} else if (nodeType == 2) {
			rootNode = JecnTreeCommon.createTreeRoot(
					TreeNodeType.organizationRoot, JecnProperties
							.getValue("dept"));
		}
		for (JecnTreeBean jecnTreeBean : list) {
			if (jecnTreeBean.getPid() != null
					&& jecnTreeBean.getPid().intValue() == 0) {
				JecnTreeNode node = new JecnTreeNode(jecnTreeBean);
				this.getChildren(node, list);
				rootNode.add(node);
			}
		}
		return new JecnTreeModel(rootNode);
	}

	private void getChildren(JecnTreeNode pNode, List<JecnTreeBean> list) {
		for (JecnTreeBean jecnTreeBean : list) {
			if (jecnTreeBean.getPid() == null) {
				continue;
			}
			if ((pNode.getJecnTreeBean().getId().equals(jecnTreeBean.getPid()))) {
				JecnTreeNode node = new JecnTreeNode(jecnTreeBean);
				this.getChildren(node, list);
				pNode.add(node);
			}
		}
	}

	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
