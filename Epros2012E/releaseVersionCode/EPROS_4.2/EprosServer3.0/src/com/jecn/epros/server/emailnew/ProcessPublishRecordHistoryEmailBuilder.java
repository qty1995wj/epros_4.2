package com.jecn.epros.server.emailnew;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction.MailLoginEnum;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.email.buss.ToolEmail;
import com.jecn.epros.server.util.JecnUtil;

public class ProcessPublishRecordHistoryEmailBuilder extends DynamicContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {
		JecnTaskHistoryNew historyNew = (JecnTaskHistoryNew) getData()[0];
		// 生成邮件的正文
		EmailContentBuilder contentBuilder = new EmailContentBuilder();
		contentBuilder.setContent(getEmailContent(historyNew.getModifyExplain(), historyNew.getRelateName(), historyNew
				.getDraftPerson()));
		contentBuilder.setResourceUrl(getEprosResourceUrl(user.getPeopleId(), historyNew.getRelateId(), historyNew
				.getType()));

		// 生成邮件信息对象
		EmailBasicInfo emailBasicInfo = new EmailBasicInfo();
		emailBasicInfo.setSubject(getSubject(historyNew.getRelateName()));
		emailBasicInfo.setContent(contentBuilder.buildContent());
		emailBasicInfo.addRecipients(user);
		return emailBasicInfo;
	}

	/**
	 * 获取邮件内容
	 * 
	 * @param modifyExplain
	 * @param relatedName
	 * @param draftPerson
	 * @return
	 */
	private String getEmailContent(String modifyExplain, String relatedName, String draftPerson) {
		StringBuilder builder = new StringBuilder();

		// 文件名称
		builder.append(JecnUtil.getValue("fileNameC"));
		builder.append(relatedName);
		builder.append(EmailContentBuilder.strBr);

		// 更新时间
		String updateTime = JecnCommon.getStringbyDate(new Date());
		builder.append(JecnUtil.getValue("updateTimeC"));
		builder.append(updateTime);
		builder.append(EmailContentBuilder.strBr);

		// 变更说明
		String desc = "";
		if (StringUtils.isNotBlank(modifyExplain)) {
			desc = modifyExplain.replaceAll("\n", "<br>");
		}
		builder.append(JecnUtil.getValue("changeThatC"));
		builder.append(desc);
		builder.append(EmailContentBuilder.strBr);

		// 拟稿人
		builder.append(JecnUtil.getValue("peopleMakeADraftC"));
		builder.append(draftPerson);
		builder.append(EmailContentBuilder.strBr);

		return builder.toString();
	}

	/**
	 * 获取需要打开的链接信息
	 * 
	 * @param peopleId
	 * @param relatedId
	 * @param type
	 * @return
	 */
	private String getEprosResourceUrl(Long peopleId, Long relatedId, Integer type) {
		String link = "";
		switch (type) {
		case 0: // 流程
			link = "/loginMail.action?mailFlowType=process&mailFlowId=" + relatedId + "&mailPeopleId=" + peopleId
					+ "&accessType=" + MailLoginEnum.mailPubApprove.toString();
			break;
		case 1:// 文件
			link = "/loginMail.action?mailFileId=" + relatedId + "&mailPeopleId=" + peopleId + "&accessType="
					+ MailLoginEnum.mailOpenFile.toString() + "&isPub=true";
			break;
		case 2:// 制度模板
			link = "/loginMail.action?mailRuleType=ruleModeFile&mailRuleId=" + relatedId + "&mailPeopleId=" + peopleId
					+ "&accessType=" + MailLoginEnum.mailOpenRule.toString() + "&isPub=true";
			break;
		case 3:// 制度文件
			link = "/loginMail.action?mailRuleType=ruleFile&mailRuleId=" + relatedId + "&mailPeopleId=" + peopleId
					+ "&accessType=" + MailLoginEnum.mailOpenRule.toString() + "&isPub=true";
			break;
		case 4:// 流程架构
			link = "/loginMail.action?mailFlowType=processMap&mailFlowId=" + relatedId + "&mailPeopleId=" + peopleId
					+ "&accessType=" + MailLoginEnum.mailPubApprove.toString();
			break;
		default:
			link = "error.jsp";
		}
		return link;
	}

	private String getSubject(String relatedName) {
		return '"' + relatedName + '"' + "," + ToolEmail.IS_FINISH_PUB;
	}

}
