package epros.designer.gui.system.config.ui.right;

import java.awt.event.ActionEvent;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.task.Constants;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 
 * 删除按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDeleteJButton extends JecnAbstractBaseJButton {

	public JecnDeleteJButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// 获取当前选中的属性面板
		JecnAbstractPropertyBasePanel selectedPropContPanel = dialog.getSelectedPropContPanel();
		if (selectedPropContPanel == null) {
			return;
		}

		// //////////////开始删除操作// //////////////
		// 获取选中显示项
		List<JecnConfigItemBean> selectedItemBeanList = selectedPropContPanel.getTableScrollPane()
				.getSelectedItemBeanList();
		// 获取选中序号集合
		int[] selectedRows = selectedPropContPanel.getTableScrollPane().getSelectedRows();
		if (selectedItemBeanList == null || selectedItemBeanList.size() == 0) {
			if (dialog.getOkCancelJButtonPanel().getOkJButton().isEnabled()) {
				dialog.getOkCancelJButtonPanel().getInfoLabel().setText("");
			}
			return;
		}

		for (JecnConfigItemBean itemBean : selectedItemBeanList) {
			String tooltip = isNoDeleteItem(itemBean.getMark());
			if (StringUtils.isNotBlank(tooltip)) {
				JecnOptionPane.showMessageDialog(null, tooltip);
				return;
			}
			// 设置为不显示
			itemBean.setValue(Constants.ITEM_IS_NOT_SHOW);
		}

		// 重新加载
		dialog.getPropertyPanel().reShowProperty();

		// 根据给定的序号添加选中行,如果选中行超过界限选中边界行
		selectedPropContPanel.getTableScrollPane().setSelectedRowToTable(selectedRows);
	}

	/*
	 * 不允许删除的选项
	 * 
	 * @return
	 */
	private String isNoDeleteItem(String mark) {
		if (mark.equals("3") || mark.equals("5") || mark.equals("189")) {
			// 流程责任人、责任部门、流程编号不允许删除！
			return JecnProperties.getValue("isCanNotDeleteConfig");
		} else if (mark.equals("kpiName")) {
			// KPI名称不允许删除！
			return JecnProperties.getValue("canNotDeleteKPIName");
		} else if (mark.equals("ruleResPeople") || mark.equals("ruleResDept") || mark.equals("ruleCode")) {
			return JecnProperties.getValue("InstitutionCanNotBeDeleted");
		} else if (mark.equals("isShowProcessMapResPeople") || mark.equals("isShowProcessMapResponsibleDepartment")) {
			return JecnProperties.getValue("respCanNotBeDelete");
		} 
		return null;
	}
}
