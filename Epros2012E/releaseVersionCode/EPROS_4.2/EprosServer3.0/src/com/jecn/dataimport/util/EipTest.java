package com.jecn.dataimport.util;

import java.util.List;

import org.springframework.remoting.rmi.RmiProxyFactoryBean;

public class EipTest {
	public static void main(String[] args) throws Exception {
		RmiProxyFactoryBean factory = new RmiProxyFactoryBean();
		factory.setServiceInterface(EipRMI.class);
		factory.setServiceUrl("rmi://192.168.1.176:2088/EipRMI");
		factory.afterPropertiesSet();
		EipRMI service = (EipRMI) factory.getObject();
		List<EipDataBean> list = service.getEipDataBeanList("admin");
		List<EipDataBean> list1 = service.getEipAllDataBeanList();
		System.out.println(list1.size());
		for (int i = 0; i < list1.size(); i++) {
			System.out.println(i + ": " + list1.get(i).getFlowName());
		}
		System.out.println(service.getFlowCount("admin"));
		List<EipFlowBean> list2 = service.getEipAllFlowBeanList();
		for (EipFlowBean e : list2) {
			System.out.println(e.getFlowName() + "," + e.getTaskName());
		}
		List<EipMyFlowBean> list4 = service.getEipMyFlowBeanList("admin");
		if (list4 == null) {
			return;
		}
		System.out.println(list4.size());
		System.out.println(list4.get(0).getCreateTime());
		System.out.println(service.getEipMyFlowBeanList("admin").size());
	}
}
