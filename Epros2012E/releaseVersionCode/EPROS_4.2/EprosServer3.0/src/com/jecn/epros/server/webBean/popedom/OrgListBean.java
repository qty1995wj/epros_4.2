package com.jecn.epros.server.webBean.popedom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 组织清单bean
 * 
 * @author fuzhh 2013-12-3
 * 
 */
public class OrgListBean implements Serializable {
	/** 总的级别数 */
	private int levelTotal;
	/** 组织清单 */
	private List<OrgRelateFlowBean> orgRelatsList = new ArrayList<OrgRelateFlowBean>();
	/** 流程每个级别的个数 0、1、2.....（统计） */
	private List<Integer> listInt;
	/** 流程个数 */
	private int orgTotal;
	/** 待建流程个数 */
	private int noPubFlowTotal;
	/** 审批流程个数 */
	private int approveFlowTotal;
	/** 发布流程个数 */
	private int pubFlowTotal;
	/** 流程总数 */
	private int flowTotal;

	public int getLevelTotal() {
		return levelTotal;
	}

	public void setLevelTotal(int levelTotal) {
		this.levelTotal = levelTotal;
	}

	public List<OrgRelateFlowBean> getOrgRelatsList() {
		return orgRelatsList;
	}

	public void setOrgRelatsList(List<OrgRelateFlowBean> orgRelatsList) {
		this.orgRelatsList = orgRelatsList;
	}

	public List<Integer> getListInt() {
		return listInt;
	}

	public void setListInt(List<Integer> listInt) {
		this.listInt = listInt;
	}

	public int getOrgTotal() {
		return orgTotal;
	}

	public void setOrgTotal(int orgTotal) {
		this.orgTotal = orgTotal;
	}

	public int getNoPubFlowTotal() {
		return noPubFlowTotal;
	}

	public void setNoPubFlowTotal(int noPubFlowTotal) {
		this.noPubFlowTotal = noPubFlowTotal;
	}

	public int getApproveFlowTotal() {
		return approveFlowTotal;
	}

	public void setApproveFlowTotal(int approveFlowTotal) {
		this.approveFlowTotal = approveFlowTotal;
	}

	public int getPubFlowTotal() {
		return pubFlowTotal;
	}

	public void setPubFlowTotal(int pubFlowTotal) {
		this.pubFlowTotal = pubFlowTotal;
	}

	public int getFlowTotal() {
		flowTotal = noPubFlowTotal + approveFlowTotal + pubFlowTotal;
		return flowTotal;
	}

}
