package com.jecn.epros.server.dao.system;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnElementsLibrary;

public interface IJecnElementsLibraryDao {

	void updateElementsLibary(List<String> list, int type) throws Exception;

	/**
	 * 初始化显示的元素
	 * 
	 * @param type
	 *            0：流程架构；1：流程;2：组织
	 * @return
	 * @throws Exception
	 */
	public List<JecnElementsLibrary> getShowElementsLibraryByType(int type) throws Exception;

	List<JecnElementsLibrary> getAllElementsLibraryByType(int type) throws Exception;

	/**
	 * 待添加元素集合
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	List<JecnElementsLibrary> getAddElementsLibraryByType(int type) throws Exception;
}
