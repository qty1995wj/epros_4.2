/*
 * 
 * 连接线内容显示框
 * 
 */
package epros.draw.gui.line;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.util.List;

import javax.swing.JPanel;

import sun.swing.SwingUtilities2;
import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.line.JecnLineTextData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.line.resize.JecnLineTextResizePanel;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.line.shape.TempDividingLine;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 连接线文本显示框
 * 
 * @author zhangxh
 * @date： 日期：Mar 22, 2012 时间：10:40:13 AM
 */
public class JecnLineTextPanel extends JPanel {
	/** 连接线文本显示框默认宽度 */
	private final int INIT_WIDTH = 100;
	/** 连接线文本显示框默认高度 */
	private final int INIT_HEIGHT = 60;
	/** 字体距离边框的间距 2*2 */
	private final int SPACE = 4;
	/** 上次放大倍数 */
	private double oldWorkFlow = 1.0;
	/** 当前字体 */
	private Font currFont = null;
	/** 记录操作前字体大小 */
	private int oldFontSize = 0;
	/** 变化前字体样式 */
	private int oldFontStyle = 0;
	/** 变化前字体名称 */
	private String lodFontName = "";

	/** 连接线文本编辑框数据对象 */
	private JecnLineTextData lineTextData = null;

	/** 记录连接线编辑框层级 */
	private int textPanelOrder;

	/** 连接线 */
	private JecnBaseManhattanLinePanel manhattanLine = null;

	private JecnLineTextResizePanel textResizePanel;

	private TempDividingLine tempDividingLine;

	private Point textPoint = null;

	/** 文字框所属线段的开始点 */
	private Point lineStartP = null;
	/** 文字框所属线段的结束点 */
	private Point lineEndP = null;

	/** 1:开始线段，2：结束线段 */
	private int lineType;
	private int oldLineSize;

	public JecnLineTextPanel(JecnBaseManhattanLinePanel manhattanLine) {
		if (manhattanLine == null) {
			JecnLogConstants.LOG_BASE_FLOW_ELEMENT_PANEL.error("连接线编辑区域JecnLineTextPanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.manhattanLine = manhattanLine;
		textResizePanel = new JecnLineTextResizePanel(this);
		lineTextData = new JecnLineTextData();
		textPoint = manhattanLine.getFlowElementData().getTextPoint();
		oldLineSize = manhattanLine.getLineSegments().size();
		// 判断悬浮点属于那条线段
		setRelatedLine(getRelatedLine(manhattanLine.getLineSegments(), textPoint));
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// 获取原始绘画呈现处理对象
		Graphics2D graphics2D = (Graphics2D) g;
		RenderingHints originalRenderingHints = graphics2D.getRenderingHints();
		// 指定RenderingHints
		graphics2D.setRenderingHints(JecnUIUtil.getRenderingHints());

		// 填充区域
		graphics2D.setColor(JecnUIUtil.getWorkflowColor());
		graphics2D.fillRect(0, 0, this.getWidth(), this.getHeight());

		// 还原原始RenderingHints对象
		graphics2D.setRenderingHints(originalRenderingHints);
		graphics2D.setColor(getLineData().getFontColor());

		if (DrawCommon.isNullOrEmtryTrim(this.getLineData().getFigureText())) {
			return;
		}
		// 重新设置当前放大缩小倍数下字体。执行此处currFont变量不为空
		boolean ret = resetCurrFont();
		if (ret) {// 放大缩小、字体名称、样式、大小改变时
			// 设置显示框大小和位置点
			resetLinePanelBounds(currFont);
		}
		// 元素上添加文字
		setFontPostion(g);
	}

	/**
	 * 
	 * 元素上绘制文字(字体中间居中 )
	 * 
	 * @param g
	 */
	private void setFontPostion(Graphics g) {
		if (getLineData() == null || g == null || getLineData().getFigureText() == null) {
			return;
		}
		// 换行符分割字符串
		String[] splitStrings = getLineData().getFigureText().split("\n");
		// 获取当前字体样式下默认字体高度
		int textHeight = g.getFontMetrics(currFont).getHeight();
		for (int i = 0; i < splitStrings.length; i++) {
			int textLength = g.getFontMetrics(currFont).stringWidth(splitStrings[i]);
			// 添加文字
			SwingUtilities2.drawString(this, g, splitStrings[i], this.getWidth() / 2 - textLength / 2, this.getHeight()
					/ 2 + 3 * textHeight / 4 - ((splitStrings.length * textHeight) / 2) + textHeight * i);
		}
	}

	/**
	 * 
	 * 设置连接线显示框大小、位置点以及层级
	 * 
	 * 适用场合-显示框：显示，位置，大小、内容
	 * 
	 */
	void insertTextToLineTextPanel() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		// 重新设置当前放大缩小倍数下字体。执行此处currFont变量不为空
		resetCurrFont();
		// 设置显示框大小和位置点
		resetLinePanelBounds(currFont);

		// 存在获取编辑区域面板的层级
		int layer = this.manhattanLine.getFlowElementData().getZOrderIndex();
		// 设置连接线编辑框层级
		workflow.setLayer(this, layer + 2);
	}

	/**
	 * 
	 * 放大缩小改变位置点和大小
	 * 
	 */
	void zoomTextPanel() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		// 执行连接线编辑框放大缩小
		JecnLineTextData lineTextData = getLineTextData().getZoomLineTextData(workflow.getWorkflowScale());
		// 设置图形大小和位置点
		this.setBounds(lineTextData.getX(), lineTextData.getY(), lineTextData.getWidth(), lineTextData.getHeight());
	}

	/**
	 * 设置显示框大小和位置点
	 * 
	 * 适用场合-显示框：显示，位置，大小、内容、字体大小、字体样式、字体名称
	 * 
	 */
	private void resetLinePanelBounds(Font font) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}
		// 当前大小
		Dimension size = getSizeByContent(font);
		this.setSize(size);// 为了getLocationBySize方法计算使用
		// 位置点
		getLocationBySize(workflow);

		// 记录拖动连接线时连接线文本编辑框100%状态下数据层
		this.getLineTextData().reSetAttributes(this.getLocation(), size, workflow.getWorkflowScale());
	}

	/**
	 * 
	 * 根据编辑框内应显示的文字区域大小重置编辑框的位置和大小
	 * 
	 */
	private Dimension getSizeByContent(Font font) {
		// 连接线文字内容
		String text = getLineData().getFigureText();
		if (DrawCommon.isNullOrEmtryTrim(text)) {
			return new Dimension(INIT_WIDTH, this.INIT_HEIGHT);
		}

		// 换行符分割字符串
		String[] splitStrings = text.split("\n");
		// 获取当前字体样式下默认字体高度
		int textHeight = this.getFontMetrics(font).getHeight();
		int thisWidth = 0;
		for (int i = 0; i < splitStrings.length; i++) {
			int textLength = this.getFontMetrics(font).stringWidth(splitStrings[i]);
			if (textLength > thisWidth) {
				thisWidth = thisWidth + textLength;
			}
		}
		// 字体高度 (每一行的高度*字体行数)
		int thisHeight = textHeight * splitStrings.length;
		return new Dimension(thisWidth + SPACE, thisHeight);
	}

	/**
	 * 
	 * 设置连接线编辑区域位置、大小
	 * 
	 * @param workflow
	 */
	private void getLocationBySize(JecnDrawDesktopPane workflow) {
		if (workflow == null) {
			return;
		}

		// 小线段集合
		List<LineSegment> lineSegments = this.manhattanLine.getLineSegments();

		// 连接线小线段长度
		int size = lineSegments.size();
		if (size == 0) {
			return;
		}

		if (oldLineSize != 0 && oldLineSize == size && textPoint != null) {// 初始化数据
			reSetTextLocation(textPoint);
			return;
		}

		oldLineSize = size;

		LineSegment lineSegment = null;
		Point tempPoint = null;
		if (size % 2 == 0) {// 小线段条数为偶数时
			lineSegment = lineSegments.get(size / 2 - 1);
			tempPoint = lineSegment.getZoomEPoint();
			this.setLocation(tempPoint.x - this.getWidth() / 2, tempPoint.y - this.getHeight() / 2);

			textResizePanel.setLocation(tempPoint.x - 4, this.getY() + 4);
		} else if (size % 2 == 1) {
			lineSegment = lineSegments.get(size / 2);
			// 根据小线段开始点和结束点坐标、连接线文本大小获取连接线文本Location位置点
			this.setLocation(getTextPanelPoint(lineSegment, this.getWidth(), this.getHeight()));

			textResizePanel.setLocation(this.getX() + this.getWidth() / 2 - 4, this.getY() + 4);
		}
		setTextPoint(null);
	}

	public void reSetTextLocation(Point textPoint) {
		Point moveP = getMoveTextPoint(textPoint);
		if (moveP == null) {
			return;
		}
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		this.textPoint = moveP;
		int curScaleX = DrawCommon.convertDoubleToInt(moveP.x * scale);
		int curScaleY = DrawCommon.convertDoubleToInt(moveP.y * scale);
		this.setLocation(curScaleX - this.getWidth() / 2, curScaleY - this.getHeight() / 2);
		textResizePanel.setLocation(curScaleX - 4, curScaleY - 4);
		// 记录 原始状态下的 坐标
		manhattanLine.getFlowElementData().setTextPoint(this.textPoint);
	}

	private Point getMoveTextPoint(Point textPoint) {
		if (lineStartP == null) {
			// setTextPoint(null);
			// getLocationBySize(JecnDrawMainPanel.getMainPanel().getWorkflow());
			return textPoint;
		}

		int moveX = 0;
		int moveY = 0;

		Point moveP = null;
		// 先判断是不是整体移动
		moveP = isMoveAllLine();

		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		int moveLineType = 0;
		if (moveP != null) {
			moveX = moveP.x;
			moveY = moveP.y;
			moveLineType = 0;
		} else if (lineType == 1) {// 开始线段
			moveP = manhattanLine.getStartPoint();
			moveX = moveP.x - DrawCommon.convertDoubleToInt(lineStartP.x * scale);
			moveY = moveP.y - DrawCommon.convertDoubleToInt(lineStartP.y * scale);
			lineStartP = manhattanLine.getOriginalStartPoint();
			moveLineType = 1;
		} else if (lineType == 2) {
			moveP = manhattanLine.getEndPoint();
			moveX = moveP.x - DrawCommon.convertDoubleToInt(lineEndP.x * scale);
			moveY = moveP.y - DrawCommon.convertDoubleToInt(lineEndP.y * scale);
			lineEndP = manhattanLine.getOriginalEndPoint();
			moveLineType = 2;
		} else if (lineType == 0) {// 线段文字不显示在开始或结束
			Point moveSP = manhattanLine.getStartPoint();
			int moveSX = moveSP.x - DrawCommon.convertDoubleToInt(lineStartP.x * scale);
			int moveSY = moveSP.y - DrawCommon.convertDoubleToInt(lineStartP.y * scale);

			Point moveEP = manhattanLine.getEndPoint();
			int moveEX = moveEP.x - DrawCommon.convertDoubleToInt(lineEndP.x * scale);
			int moveEY = moveEP.y - DrawCommon.convertDoubleToInt(lineEndP.y * scale);

			// 判断当前文字中心点，是否在线段上，如不在，走标准显示文字处理方式
			if (!textCenterPointIsONLine()) {
				setTextPoint(null);
				getLocationBySize(JecnDrawMainPanel.getMainPanel().getWorkflow());
				return null;
			}
			if ((moveSX != 0 || moveSY != 0) && (moveEX != 0 || moveEY != 0)) {// 开始点和结束点都变
				moveX = moveEX;
				moveY = moveEY;

				lineStartP = manhattanLine.getOriginalStartPoint();
				lineEndP = manhattanLine.getOriginalEndPoint();
			}
			moveLineType = 0;
		}

		if (moveP == null && moveLineType != 0) {
			return textPoint;
		}

		moveX = LineDirectionEnum.horizontal == textLineDirection && moveLineType != 0 ? moveX / 2 : moveX;
		moveY = LineDirectionEnum.vertical == textLineDirection && moveLineType != 0 ? moveY / 2 : moveY;
		return new Point(textPoint.x + DrawCommon.convertDoubleToInt(moveX / scale), textPoint.y
				+ DrawCommon.convertDoubleToInt(moveY / scale));
	}

	private Point isMoveAllLine() {
		Point moveSP = manhattanLine.getStartPoint();
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		int moveSX = moveSP.x - DrawCommon.convertDoubleToInt(lineStartP.x * scale);
		int moveSY = moveSP.y - DrawCommon.convertDoubleToInt(lineStartP.y * scale);

		Point moveEP = manhattanLine.getEndPoint();
		int moveEX = moveEP.x - DrawCommon.convertDoubleToInt(lineEndP.x * scale);
		int moveEY = moveEP.y - DrawCommon.convertDoubleToInt(lineEndP.y * scale);

		if ((moveSX != 0 || moveSY != 0) && (moveEX != 0 || moveEY != 0)) {// 开始点和结束点都变
			lineStartP = manhattanLine.getOriginalStartPoint();
			lineEndP = manhattanLine.getOriginalEndPoint();
			return new Point(moveEX, moveEY);
		}
		return null;
	}

	private boolean textCenterPointIsONLine() {
		List<LineSegment> list = manhattanLine.getLineSegments();

		Point centerPoint = textResizePanel.getZoomCenterPoint();
		if (centerPoint == null) {
			centerPoint = textPoint;
		}

		for (LineSegment lineSegment : list) {
			if (lineSegment.getLineSegmentData().getLineDirectionEnum() == LineDirectionEnum.horizontal) {// 横线
				if (lineSegment.getZoomSPoint().x > lineSegment.getZoomEPoint().x) {
					if (lineSegment.getZoomSPoint().x >= centerPoint.x && lineSegment.getZoomEPoint().x < centerPoint.x
							&& Math.abs(centerPoint.y - lineSegment.getZoomEPoint().y) <= 5) {
						return true;
					}
				} else {
					if (lineSegment.getZoomSPoint().x <= centerPoint.x && lineSegment.getZoomEPoint().x > centerPoint.x
							&& Math.abs(centerPoint.y - lineSegment.getZoomEPoint().y) <= 5) {
						return true;
					}
				}
			} else {
				if (lineSegment.getZoomSPoint().y > lineSegment.getZoomEPoint().y) {
					if (lineSegment.getZoomSPoint().y >= centerPoint.y && lineSegment.getZoomEPoint().y < centerPoint.y
							&& Math.abs(centerPoint.x - lineSegment.getZoomEPoint().x) <= 5) {
						return true;
					}
				} else {
					if (lineSegment.getZoomSPoint().y <= centerPoint.y && lineSegment.getZoomEPoint().y > centerPoint.y
							&& Math.abs(centerPoint.x - lineSegment.getZoomEPoint().x) <= 5) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * 根据小线段开始点和结束点坐标、连接线文本大小获取连接线文本Location位置点
	 * 
	 * @param lineSegment
	 *            显示文本所要覆盖的小线段
	 * @param width
	 *            文本宽度
	 * @param height
	 *            文本高度
	 * @return Point文本区域位置点
	 */
	private Point getTextPanelPoint(LineSegment lineSegment, int width, int height) {
		if (lineSegment == null || lineSegment.getLineSegmentData() == null
				|| lineSegment.getLineSegmentData().getLineDirectionEnum() == null) {

		}
		// 线段中心点
		Point centerPoint = null;
		switch (lineSegment.getLineSegmentData().getLineDirectionEnum()) {
		case horizontal:
			// 纵线X轴中心点
			int lineX = 0;
			if (lineSegment.getZoomSPoint().x > lineSegment.getZoomEPoint().x) {
				lineX = lineSegment.getZoomEPoint().x;
			} else {
				lineX = lineSegment.getZoomSPoint().x;
			}
			// 纵线X轴中心点 返货Point值为编辑框的位置坐标，中心点要为编辑框的中心点
			centerPoint = new Point(lineX + Math.abs(lineSegment.getZoomSPoint().x - lineSegment.getZoomEPoint().x) / 2
					- width / 2, lineSegment.getZoomSPoint().y - height / 2);
			break;
		case vertical:
			// 纵线Y轴中心点
			int lineY = 0;
			if (lineSegment.getZoomSPoint().y > lineSegment.getZoomEPoint().y) {
				lineY = lineSegment.getZoomEPoint().y;
			} else {
				lineY = lineSegment.getZoomSPoint().y;
			}
			// 线段中心点 返货Point值为编辑框的位置坐标，中心点要为编辑框的中心点
			centerPoint = new Point(lineSegment.getZoomSPoint().x - width / 2, lineY
					+ Math.abs(lineSegment.getZoomSPoint().y - lineSegment.getZoomEPoint().y) / 2 - height / 2);
			break;
		}
		return centerPoint;
	}

	/**
	 * 如果有变化情况下，重新设置当前放大缩小倍数下字体
	 * 
	 * @return boolean true：有重新设置；false：保持原来值
	 */
	private boolean resetCurrFont() {
		double scale = getLineData().getWorkflowScale();

		// 字体大小
		int fontSize = getLineData().getFontSize();
		// 字体样式
		int fontStyle = getLineData().getFontStyle();
		// 字体名称
		String fontName = getLineData().getFontName();

		if (currFont == null || oldWorkFlow != scale || oldFontSize != fontSize || oldFontStyle != fontStyle
				|| (!DrawCommon.isNullOrEmtryTrim(fontName) && !fontName.equals(lodFontName))) {
			// 放大倍数
			oldWorkFlow = scale;
			// 字体大小
			oldFontSize = fontSize;
			// 字体样式 1：粗体。0普通
			oldFontStyle = fontStyle;
			// 字体名称
			lodFontName = fontName;

			currFont = new Font(lodFontName, oldFontStyle, DrawCommon.convertDoubleToInt(scale * oldFontSize));

			this.setFont(currFont);

			return true;
		}

		return false;
	}

	/**
	 * 记录连接线文本框数据克隆对象
	 * 
	 * @param manhattanLine
	 * @return
	 */
	public JecnLineTextPanel clone(JecnBaseManhattanLinePanel manhattanLine) {
		JecnLineTextPanel lineTextPanel = new JecnLineTextPanel(manhattanLine);
		lineTextPanel.lineTextData = this.lineTextData.clone();
		return lineTextPanel;
	}

	/**
	 * 
	 * 连接线数据对象
	 * 
	 * @return JecnManhattanLineData
	 */
	public JecnManhattanLineData getLineData() {
		return manhattanLine.getFlowElementData();
	}

	/**
	 * 
	 * 连接线内容框数据对象
	 * 
	 * @return
	 */
	public JecnLineTextData getLineTextData() {
		return lineTextData;
	}

	public int getTextPanelOrder() {
		return textPanelOrder;
	}

	public void setTextPanelOrder(int textPanelOrder) {
		this.textPanelOrder = textPanelOrder;
	}

	public JecnLineTextResizePanel getTextResizePanel() {
		return textResizePanel;
	}

	public TempDividingLine createTempLinePanel() {
		if (tempDividingLine == null) {
			tempDividingLine = new TempDividingLine();
		}
		// 拖动时虚拟分割线段
		return tempDividingLine;
	}

	public Point getTextPoint() {
		return textPoint;
	}

	public void setTextPoint(Point textPoint) {
		this.textPoint = textPoint;
		manhattanLine.getFlowElementData().setTextPoint(textPoint);
	}

	public void setRelatedLine(LineSegment relatedLine) {
		if (relatedLine == null) {
			lineStartP = null;
			lineStartP = null;
			lineType = 0;
		} else {
			this.lineType = getLineType(relatedLine);
		}
	}

	private LineDirectionEnum textLineDirection = null;

	public int getLineType(LineSegment relatedLine) {
		textLineDirection = relatedLine.getLineSegmentData().getLineDirectionEnum();
		if (relatedLine.getStartX() == manhattanLine.getStartPoint().x
				&& relatedLine.getStartY() == manhattanLine.getStartPoint().y) {// 开始线段
			lineStartP = relatedLine.getStartPoint();
			lineEndP = manhattanLine.getOriginalEndPoint();
			return 1;
		} else if (relatedLine.getEndX() == manhattanLine.getEndPoint().x
				&& relatedLine.getEndY() == manhattanLine.getEndPoint().y) {// 结束线段
			lineStartP = manhattanLine.getOriginalStartPoint();
			lineEndP = relatedLine.getEndPoint();
			return 2;
		} else {
			lineStartP = manhattanLine.getOriginalStartPoint();
			lineEndP = manhattanLine.getOriginalEndPoint();
			return 0;
		}
	}

	public JecnBaseManhattanLinePanel getManhattanLine() {
		return manhattanLine;
	}

	public Point getLineStartP() {
		return lineStartP;
	}

	public void setLineStartP(Point lineStartP) {
		this.lineStartP = lineStartP;
	}

	public Point getLineEndP() {
		return lineEndP;
	}

	public void setLineEndP(Point lineEndP) {
		this.lineEndP = lineEndP;
	}

	public LineSegment getRelatedLine(List<LineSegment> lineSegmentList, Point textPoint) {
		if (textPoint == null) {
			return null;
		}
		int textX = textPoint.x;
		int textY = textPoint.y;
		for (LineSegment lineSegment : lineSegmentList) {
			int startX = lineSegment.getZoomSPoint().x;
			int startY = lineSegment.getZoomSPoint().y;
			int endX = lineSegment.getZoomEPoint().x;
			int endY = lineSegment.getZoomEPoint().y;
			if (lineSegment.getLineSegmentData().getLineDirectionEnum() == LineDirectionEnum.horizontal) {// 横线
				if (startX > endX) {
					if (startX > textX && textX > endX && (Math.abs(textY - startY) <= 10)) {
						return lineSegment;
					}
				} else {
					if (endX > textX && textX > startX && (Math.abs(textY - startY) <= 10)) {
						return lineSegment;
					}
				}
			} else {// 纵线
				if (startY > endY) {
					if (startY > textY && textY > endY && (Math.abs(textX - startX) <= 10)) {
						return lineSegment;
					}
				} else {
					if (endY > textY && textY > startY && (Math.abs(textX - startX) <= 10)) {
						return lineSegment;
					}
				}
			}
		}
		return null;
	}

}
