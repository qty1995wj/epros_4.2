package com.jecn.epros.server.dao.dataImport.match.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;

import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.dataImport.match.IMatchPostTreeBeanDao;
import com.jecn.epros.server.service.dataImport.match.constant.MatchSqlConstant;

/**
 * 岗位匹配树结构数据获取
 * 
 * @author zhangxiaohu
 * 
 */
public class MatchPostTreeBeanDaoImpl extends AbsBaseDao<String, String>
		implements IMatchPostTreeBeanDao {
	private final Log log = LogFactory.getLog(MatchPostTreeBeanDaoImpl.class);

	/**
	 * 
	 * 查询根部门以及直接子部门
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> selectRootDeptAndChildDept(Long id) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("SELECT T.DEPT_NUM,T.DEPT_NAME,");
		sqlBuffer
				.append("(SELECT COUNT(*) FROM JECN_SANFU_ACTUAL_DEPT D WHERE D.P_DEPT_NUM=T.DEPT_NUM) AS COUNT_ORG,");
		sqlBuffer
				.append("(SELECT COUNT(*) FROM JECN_SANFU_ACTUAL_POS P WHERE P.DEPT_NUM=T.DEPT_NUM) AS COUNT_POS ");
		sqlBuffer.append("FROM JECN_SANFU_ACTUAL_DEPT T WHERE T.PROJECT_ID = "
				+ id);
		sqlBuffer.append(" AND T.P_DEPT_NUM = '0' ORDER BY T.DEPT_ID");
		// 执行查询
		return this.getSession().createSQLQuery(sqlBuffer.toString()).list();
	}

	/**
	 * 
	 * 根据给定项目ID和部门编号查找部门下的直接子部门
	 * 
	 * @param perOrgBean
	 *            JecnFlowOrg
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> selectChildDeptByPerDept(Long id, String deptNum)
			throws Exception {
		if (id == null || deptNum == null) {
			return null;
		}
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("SELECT T.DEPT_NUM,T.DEPT_NAME,");
		sqlBuffer
				.append("(SELECT COUNT(*) FROM JECN_SANFU_ACTUAL_DEPT D WHERE D.P_DEPT_NUM=T.DEPT_NUM) AS COUNT_ORG,");
		sqlBuffer
				.append("(SELECT COUNT(*) FROM JECN_SANFU_ACTUAL_POS P WHERE P.DEPT_NUM=T.DEPT_NUM) AS COUNT_POS ");
		sqlBuffer.append("FROM JECN_SANFU_ACTUAL_DEPT T WHERE T.PROJECT_ID = "
				+ id);
		sqlBuffer.append(" AND T.P_DEPT_NUM = " + "'" + deptNum + "'"
				+ " ORDER BY T.DEPT_ID");

		// 执行查询
		return this.getSession().createSQLQuery(sqlBuffer.toString()).list();
	}

	/**
	 * 
	 * 
	 * 获取给定部门下的岗位
	 * 
	 * @author
	 * @param perOrgBean
	 *            JecnFlowOrg
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> selectPosByDeptID(String deptNum) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append("SELECT T.ACT_POS_NUM,T.ACT_POS_NAME,");
		sqlBuffer
				.append("(SELECT COUNT(*) FROM JECN_SANFU_ACTUAL_PERSON JSU WHERE JSU.POSITION_NUM=T.ACT_POS_NUM) AS PSU_COUNT");
		sqlBuffer.append(" FROM JECN_SANFU_ACTUAL_POS T WHERE T.DEPT_NUM = "
				+ "'" + deptNum + "'" + " ORDER BY T.ID");
		// 执行查询
		return this.getSession().createSQLQuery(sqlBuffer.toString()).list();

	}

	/**
	 * 获取操作表中基准岗位记录
	 * 
	 * @param s
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getBasePosSfBeanList() throws Exception {
		log.info(" 根据操作表获取基准岗位 开始");
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_FLOW_BASE_POS);
		List<Object[]> objectList = query.list();
		log.info(" 根据操作表获取基准岗位数据结束 ");
		return objectList;
	}

	@Override
	public List<Object[]> getPosAndPersonByOrgId(String deptNum, Long projectId)
			throws Exception {
		String sql = "select jmap.ACT_POS_NUM,jmap.ACT_POS_NAME,jmap.DEPT_NUM,jmad.DEPT_NAME"
				+ " ,(select count(*) from JECN_MATCH_ACTUAL_PERSON where POSITION_NUM=jmap.ACT_POS_NUM) as count"
				+ "  from JECN_MATCH_ACTUAL_DEPT jmad,JECN_MATCH_ACTUAL_POS jmap where "
				+ " jmap.DEPT_NUM = jmad.DEPT_NUM and jmap.DEPT_NUM=? and jmad.PROJECT_ID=? order by jmad.DEPT_NAME";
		return this.listNativeSql(sql, deptNum, projectId);
	}

	/**
	 * 根据部门编号获取部门下岗位
	 * 
	 * @param deptNum
	 *            部门编号
	 * @param projectId
	 *            项目ID
	 * @return List<Object[]>
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getPosByOrgId(String deptNum, Long projectId)
			throws Exception {
		String sql = "select jmap.ACT_POS_NUM,jmap.ACT_POS_NAME,jmap.DEPT_NUM,jmad.DEPT_NAME "
				+ " from JECN_MATCH_ACTUAL_DEPT jmad,JECN_MATCH_ACTUAL_POS jmap where "
				+ " jmap.DEPT_NUM = jmad.DEPT_NUM and jmap.DEPT_NUM=? and jmad.PROJECT_ID=? order by jmad.DEPT_NAME";
		return this.listNativeSql(sql, deptNum, projectId);
	}
}
