package epros.designer.gui.system.config.ui.main;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.right.JecnRightButtonPanel;

/**
 * 
 * 内容面板(所有属性面板的容器)
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigMainPanel extends JPanel {
	/** 配置界面 */
	private JecnAbtractBaseConfigDialog dialog = null;

	/** 右面按钮面板、属性面板的容器 */
	private JecnConfigCenterPanel configCenterPanel = null;

	public JecnConfigMainPanel(JecnAbtractBaseConfigDialog dialog) {
		if (dialog == null) {
			throw new IllegalArgumentException("JecnConfigMainPanel is error");
		}
		this.dialog = dialog;

		initComponent();
	}

	private void initComponent() {
		// 右面按钮面板、属性面板的容器
		configCenterPanel = new JecnConfigCenterPanel(dialog);

		this.setOpaque(false);
		this.setBorder(null);
		this.setLayout(new BorderLayout());

		this.add(configCenterPanel, BorderLayout.CENTER);
	}

	/**
	 * 
	 * 获取属性面板
	 * 
	 * @return JecnPropertyPanel
	 */
	public JecnPropertyPanel getPropertyPanel() {
		return configCenterPanel.getPropertyPanel();
	}

	/**
	 * 
	 * 获取加入、删除等按钮面板
	 * 
	 * @return JecnRightButtonPanel
	 */
	public JecnRightButtonPanel getRightPanel() {
		return configCenterPanel.getRightPanel();
	}

	/**
	 * 
	 * 获取当前选中的属性面板，选中根节点或者没有选中时返回值NULL
	 * 
	 * @return JecnAbstractPropertyBasePanel 当前选中的属性面板或NULL
	 */
	public JecnAbstractPropertyBasePanel getSelectedPropContPanel() {
		return getPropertyPanel().getSelectedPropContPanel();
	}
}
