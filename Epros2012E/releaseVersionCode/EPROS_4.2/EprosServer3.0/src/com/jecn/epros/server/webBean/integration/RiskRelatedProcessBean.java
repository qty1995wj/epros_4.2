package com.jecn.epros.server.webBean.integration;

/**
 * 风险相关流程
 * @author ZHF
 *
 */
public class RiskRelatedProcessBean {

	/** 控制目标 */
	private String targetDescription;
	/** 控制编号 */
	private String controlCode;
	/** 控制活动简描述 */
	private String activeShow;
	/** 活动ID */
	private Long activityId;
	/** 活动名称 */
	private String figureText;
	/** 流程ID */
	private Long flowId;
	/** 流程名称 */
	private String flowName;
	/** 部门ID */
	private Long orgId;
	/** 责任部门 */
	private String orgName;
	/** 控制方法 ：人工、自动 */
	private String strMethod;
	/** 控制类型  ： 预防型、发现型*/
	private String strType;
	/** 是否为关键控制 */
	private String keyPoin;
	/** 控制频率 */
	private String strFrequency;
	
	public String getTargetDescription() {
		return targetDescription;
	}
	public void setTargetDescription(String targetDescription) {
		this.targetDescription = targetDescription;
	}
	public String getControlCode() {
		return controlCode;
	}
	public void setControlCode(String controlCode) {
		this.controlCode = controlCode;
	}
	public String getActiveShow() {
		return activeShow;
	}
	public void setActiveShow(String activeShow) {
		this.activeShow = activeShow;
	}
	public String getFigureText() {
		return figureText;
	}
	public void setFigureText(String figureText) {
		this.figureText = figureText;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getStrMethod() {
		return strMethod;
	}
	public void setStrMethod(String strMethod) {
		this.strMethod = strMethod;
	}
	public String getStrType() {
		return strType;
	}
	public void setStrType(String strType) {
		this.strType = strType;
	}
	public String getKeyPoin() {
		return keyPoin;
	}
	public void setKeyPoin(String keyPoin) {
		this.keyPoin = keyPoin;
	}
	public String getStrFrequency() {
		return strFrequency;
	}
	public void setStrFrequency(String strFrequency) {
		this.strFrequency = strFrequency;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public Long getActivityId() {
		return activityId;
	}
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}
	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	
}
