/**
 * POSITION_VIEWLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crs.dataimport;

public class POSITION_VIEWLine  implements java.io.Serializable {
    private java.math.BigDecimal c_oid_assignedjob;

    private java.lang.String c_code;

    private java.math.BigDecimal c_orgunitid;

    private java.lang.String c_ischarge;

    private java.lang.String c_name;

    private java.util.Date c_createdate;

    private java.lang.String c_duty;

    private java.lang.String c_content;

    private java.lang.String c_status;

    private java.lang.String c_labelcode;

    private java.lang.String c_worksite;

    private java.util.Date c_begindate;

    private java.util.Date c_enddate;

    private java.lang.String c_issecret;

    private java.math.BigDecimal c_parentajobid;

    private java.math.BigDecimal c_order;

    public POSITION_VIEWLine() {
    }

    public POSITION_VIEWLine(
           java.math.BigDecimal c_oid_assignedjob,
           java.lang.String c_code,
           java.math.BigDecimal c_orgunitid,
           java.lang.String c_ischarge,
           java.lang.String c_name,
           java.util.Date c_createdate,
           java.lang.String c_duty,
           java.lang.String c_content,
           java.lang.String c_status,
           java.lang.String c_labelcode,
           java.lang.String c_worksite,
           java.util.Date c_begindate,
           java.util.Date c_enddate,
           java.lang.String c_issecret,
           java.math.BigDecimal c_parentajobid,
           java.math.BigDecimal c_order) {
           this.c_oid_assignedjob = c_oid_assignedjob;
           this.c_code = c_code;
           this.c_orgunitid = c_orgunitid;
           this.c_ischarge = c_ischarge;
           this.c_name = c_name;
           this.c_createdate = c_createdate;
           this.c_duty = c_duty;
           this.c_content = c_content;
           this.c_status = c_status;
           this.c_labelcode = c_labelcode;
           this.c_worksite = c_worksite;
           this.c_begindate = c_begindate;
           this.c_enddate = c_enddate;
           this.c_issecret = c_issecret;
           this.c_parentajobid = c_parentajobid;
           this.c_order = c_order;
    }


    /**
     * Gets the c_oid_assignedjob value for this POSITION_VIEWLine.
     * 
     * @return c_oid_assignedjob
     */
    public java.math.BigDecimal getC_oid_assignedjob() {
        return c_oid_assignedjob;
    }


    /**
     * Sets the c_oid_assignedjob value for this POSITION_VIEWLine.
     * 
     * @param c_oid_assignedjob
     */
    public void setC_oid_assignedjob(java.math.BigDecimal c_oid_assignedjob) {
        this.c_oid_assignedjob = c_oid_assignedjob;
    }


    /**
     * Gets the c_code value for this POSITION_VIEWLine.
     * 
     * @return c_code
     */
    public java.lang.String getC_code() {
        return c_code;
    }


    /**
     * Sets the c_code value for this POSITION_VIEWLine.
     * 
     * @param c_code
     */
    public void setC_code(java.lang.String c_code) {
        this.c_code = c_code;
    }


    /**
     * Gets the c_orgunitid value for this POSITION_VIEWLine.
     * 
     * @return c_orgunitid
     */
    public java.math.BigDecimal getC_orgunitid() {
        return c_orgunitid;
    }


    /**
     * Sets the c_orgunitid value for this POSITION_VIEWLine.
     * 
     * @param c_orgunitid
     */
    public void setC_orgunitid(java.math.BigDecimal c_orgunitid) {
        this.c_orgunitid = c_orgunitid;
    }


    /**
     * Gets the c_ischarge value for this POSITION_VIEWLine.
     * 
     * @return c_ischarge
     */
    public java.lang.String getC_ischarge() {
        return c_ischarge;
    }


    /**
     * Sets the c_ischarge value for this POSITION_VIEWLine.
     * 
     * @param c_ischarge
     */
    public void setC_ischarge(java.lang.String c_ischarge) {
        this.c_ischarge = c_ischarge;
    }


    /**
     * Gets the c_name value for this POSITION_VIEWLine.
     * 
     * @return c_name
     */
    public java.lang.String getC_name() {
        return c_name;
    }


    /**
     * Sets the c_name value for this POSITION_VIEWLine.
     * 
     * @param c_name
     */
    public void setC_name(java.lang.String c_name) {
        this.c_name = c_name;
    }


    /**
     * Gets the c_createdate value for this POSITION_VIEWLine.
     * 
     * @return c_createdate
     */
    public java.util.Date getC_createdate() {
        return c_createdate;
    }


    /**
     * Sets the c_createdate value for this POSITION_VIEWLine.
     * 
     * @param c_createdate
     */
    public void setC_createdate(java.util.Date c_createdate) {
        this.c_createdate = c_createdate;
    }


    /**
     * Gets the c_duty value for this POSITION_VIEWLine.
     * 
     * @return c_duty
     */
    public java.lang.String getC_duty() {
        return c_duty;
    }


    /**
     * Sets the c_duty value for this POSITION_VIEWLine.
     * 
     * @param c_duty
     */
    public void setC_duty(java.lang.String c_duty) {
        this.c_duty = c_duty;
    }


    /**
     * Gets the c_content value for this POSITION_VIEWLine.
     * 
     * @return c_content
     */
    public java.lang.String getC_content() {
        return c_content;
    }


    /**
     * Sets the c_content value for this POSITION_VIEWLine.
     * 
     * @param c_content
     */
    public void setC_content(java.lang.String c_content) {
        this.c_content = c_content;
    }


    /**
     * Gets the c_status value for this POSITION_VIEWLine.
     * 
     * @return c_status
     */
    public java.lang.String getC_status() {
        return c_status;
    }


    /**
     * Sets the c_status value for this POSITION_VIEWLine.
     * 
     * @param c_status
     */
    public void setC_status(java.lang.String c_status) {
        this.c_status = c_status;
    }


    /**
     * Gets the c_labelcode value for this POSITION_VIEWLine.
     * 
     * @return c_labelcode
     */
    public java.lang.String getC_labelcode() {
        return c_labelcode;
    }


    /**
     * Sets the c_labelcode value for this POSITION_VIEWLine.
     * 
     * @param c_labelcode
     */
    public void setC_labelcode(java.lang.String c_labelcode) {
        this.c_labelcode = c_labelcode;
    }


    /**
     * Gets the c_worksite value for this POSITION_VIEWLine.
     * 
     * @return c_worksite
     */
    public java.lang.String getC_worksite() {
        return c_worksite;
    }


    /**
     * Sets the c_worksite value for this POSITION_VIEWLine.
     * 
     * @param c_worksite
     */
    public void setC_worksite(java.lang.String c_worksite) {
        this.c_worksite = c_worksite;
    }


    /**
     * Gets the c_begindate value for this POSITION_VIEWLine.
     * 
     * @return c_begindate
     */
    public java.util.Date getC_begindate() {
        return c_begindate;
    }


    /**
     * Sets the c_begindate value for this POSITION_VIEWLine.
     * 
     * @param c_begindate
     */
    public void setC_begindate(java.util.Date c_begindate) {
        this.c_begindate = c_begindate;
    }


    /**
     * Gets the c_enddate value for this POSITION_VIEWLine.
     * 
     * @return c_enddate
     */
    public java.util.Date getC_enddate() {
        return c_enddate;
    }


    /**
     * Sets the c_enddate value for this POSITION_VIEWLine.
     * 
     * @param c_enddate
     */
    public void setC_enddate(java.util.Date c_enddate) {
        this.c_enddate = c_enddate;
    }


    /**
     * Gets the c_issecret value for this POSITION_VIEWLine.
     * 
     * @return c_issecret
     */
    public java.lang.String getC_issecret() {
        return c_issecret;
    }


    /**
     * Sets the c_issecret value for this POSITION_VIEWLine.
     * 
     * @param c_issecret
     */
    public void setC_issecret(java.lang.String c_issecret) {
        this.c_issecret = c_issecret;
    }


    /**
     * Gets the c_parentajobid value for this POSITION_VIEWLine.
     * 
     * @return c_parentajobid
     */
    public java.math.BigDecimal getC_parentajobid() {
        return c_parentajobid;
    }


    /**
     * Sets the c_parentajobid value for this POSITION_VIEWLine.
     * 
     * @param c_parentajobid
     */
    public void setC_parentajobid(java.math.BigDecimal c_parentajobid) {
        this.c_parentajobid = c_parentajobid;
    }


    /**
     * Gets the c_order value for this POSITION_VIEWLine.
     * 
     * @return c_order
     */
    public java.math.BigDecimal getC_order() {
        return c_order;
    }


    /**
     * Sets the c_order value for this POSITION_VIEWLine.
     * 
     * @param c_order
     */
    public void setC_order(java.math.BigDecimal c_order) {
        this.c_order = c_order;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof POSITION_VIEWLine)) return false;
        POSITION_VIEWLine other = (POSITION_VIEWLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.c_oid_assignedjob==null && other.getC_oid_assignedjob()==null) || 
             (this.c_oid_assignedjob!=null &&
              this.c_oid_assignedjob.equals(other.getC_oid_assignedjob()))) &&
            ((this.c_code==null && other.getC_code()==null) || 
             (this.c_code!=null &&
              this.c_code.equals(other.getC_code()))) &&
            ((this.c_orgunitid==null && other.getC_orgunitid()==null) || 
             (this.c_orgunitid!=null &&
              this.c_orgunitid.equals(other.getC_orgunitid()))) &&
            ((this.c_ischarge==null && other.getC_ischarge()==null) || 
             (this.c_ischarge!=null &&
              this.c_ischarge.equals(other.getC_ischarge()))) &&
            ((this.c_name==null && other.getC_name()==null) || 
             (this.c_name!=null &&
              this.c_name.equals(other.getC_name()))) &&
            ((this.c_createdate==null && other.getC_createdate()==null) || 
             (this.c_createdate!=null &&
              this.c_createdate.equals(other.getC_createdate()))) &&
            ((this.c_duty==null && other.getC_duty()==null) || 
             (this.c_duty!=null &&
              this.c_duty.equals(other.getC_duty()))) &&
            ((this.c_content==null && other.getC_content()==null) || 
             (this.c_content!=null &&
              this.c_content.equals(other.getC_content()))) &&
            ((this.c_status==null && other.getC_status()==null) || 
             (this.c_status!=null &&
              this.c_status.equals(other.getC_status()))) &&
            ((this.c_labelcode==null && other.getC_labelcode()==null) || 
             (this.c_labelcode!=null &&
              this.c_labelcode.equals(other.getC_labelcode()))) &&
            ((this.c_worksite==null && other.getC_worksite()==null) || 
             (this.c_worksite!=null &&
              this.c_worksite.equals(other.getC_worksite()))) &&
            ((this.c_begindate==null && other.getC_begindate()==null) || 
             (this.c_begindate!=null &&
              this.c_begindate.equals(other.getC_begindate()))) &&
            ((this.c_enddate==null && other.getC_enddate()==null) || 
             (this.c_enddate!=null &&
              this.c_enddate.equals(other.getC_enddate()))) &&
            ((this.c_issecret==null && other.getC_issecret()==null) || 
             (this.c_issecret!=null &&
              this.c_issecret.equals(other.getC_issecret()))) &&
            ((this.c_parentajobid==null && other.getC_parentajobid()==null) || 
             (this.c_parentajobid!=null &&
              this.c_parentajobid.equals(other.getC_parentajobid()))) &&
            ((this.c_order==null && other.getC_order()==null) || 
             (this.c_order!=null &&
              this.c_order.equals(other.getC_order())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getC_oid_assignedjob() != null) {
            _hashCode += getC_oid_assignedjob().hashCode();
        }
        if (getC_code() != null) {
            _hashCode += getC_code().hashCode();
        }
        if (getC_orgunitid() != null) {
            _hashCode += getC_orgunitid().hashCode();
        }
        if (getC_ischarge() != null) {
            _hashCode += getC_ischarge().hashCode();
        }
        if (getC_name() != null) {
            _hashCode += getC_name().hashCode();
        }
        if (getC_createdate() != null) {
            _hashCode += getC_createdate().hashCode();
        }
        if (getC_duty() != null) {
            _hashCode += getC_duty().hashCode();
        }
        if (getC_content() != null) {
            _hashCode += getC_content().hashCode();
        }
        if (getC_status() != null) {
            _hashCode += getC_status().hashCode();
        }
        if (getC_labelcode() != null) {
            _hashCode += getC_labelcode().hashCode();
        }
        if (getC_worksite() != null) {
            _hashCode += getC_worksite().hashCode();
        }
        if (getC_begindate() != null) {
            _hashCode += getC_begindate().hashCode();
        }
        if (getC_enddate() != null) {
            _hashCode += getC_enddate().hashCode();
        }
        if (getC_issecret() != null) {
            _hashCode += getC_issecret().hashCode();
        }
        if (getC_parentajobid() != null) {
            _hashCode += getC_parentajobid().hashCode();
        }
        if (getC_order() != null) {
            _hashCode += getC_order().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(POSITION_VIEWLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://hr.publisher.mdm", "POSITION_VIEWLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_oid_assignedjob");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_oid_assignedjob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_orgunitid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_orgunitid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_ischarge");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_ischarge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_createdate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_createdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_duty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_duty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_content");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_content"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_labelcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_labelcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_worksite");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_worksite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_begindate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_begindate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_enddate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_enddate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_issecret");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_issecret"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_parentajobid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_parentajobid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_order");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hr.publisher.mdm", "c_order"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
