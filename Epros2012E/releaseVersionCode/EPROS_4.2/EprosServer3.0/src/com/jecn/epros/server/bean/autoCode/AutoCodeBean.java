package com.jecn.epros.server.bean.autoCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 自动编号- 编号规则
 * 
 * @author ZXH
 * 
 */
/**
 * @author admin
 *
 */
/**
 * @author admin
 * 
 */
public class AutoCodeBean implements Serializable {
	private String guid;
	/** 当前编码规则流水号计数器 */
	private int codeTotal = 0;
	private Date createTime;
	private Long createPersonId;
	private String companyCode;
	/** 过程代码（立邦树节点 -过程代码） */
	private String processCode;
	private String productionLine;
	private String fileType;
	private String smallClass;
	private String processLevel;
	/** 版本 */
	private String version;

	/** 返回的编号结果 */
	private String resultCode;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public int getCodeTotal() {
		return codeTotal;
	}

	public void setCodeTotal(int codeTotal) {
		this.codeTotal = codeTotal;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getProductionLine() {
		return productionLine;
	}

	public void setProductionLine(String productionLine) {
		this.productionLine = productionLine;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getSmallClass() {
		return smallClass;
	}

	public void setSmallClass(String smallClass) {
		this.smallClass = smallClass;
	}

	public String getProcessLevel() {
		return processLevel;
	}

	public void setProcessLevel(String processLevel) {
		this.processLevel = processLevel;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

}
