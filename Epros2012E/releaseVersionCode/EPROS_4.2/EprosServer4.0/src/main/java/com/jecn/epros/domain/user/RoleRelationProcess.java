package com.jecn.epros.domain.user;

import java.util.List;

/**
 * Created by ZXH on 2017/3/23.
 */
public class RoleRelationProcess {
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程ID
     */
    private Long flowId;
    /**
     * 父节点 ID
     */
    private Long pflowId;
    /**
     * 父节点名称
     */
    private String pflowName;
    /**
     * 父节点类型
     */
    private int ptype;

    /**
     * 相关制度
     */
    private List<Object[]> ruleList;
    /**
     * 相关标准
     */
    private List<Object[]> stanList;
    /**
     * 相关风险
     */
    private List<Object[]> riskList;

    private List<Object[]> activityList;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Long getPflowId() {
        return pflowId;
    }

    public void setPflowId(Long pflowId) {
        this.pflowId = pflowId;
    }

    public String getPflowName() {
        return pflowName;
    }

    public void setPflowName(String pflowName) {
        this.pflowName = pflowName;
    }

    public int getPtype() {
        return ptype;
    }

    public void setPtype(int ptype) {
        this.ptype = ptype;
    }

    public List<Object[]> getRuleList() {
        return ruleList;
    }

    public void setRuleList(List<Object[]> ruleList) {
        this.ruleList = ruleList;
    }

    public List<Object[]> getStanList() {
        return stanList;
    }

    public void setStanList(List<Object[]> stanList) {
        this.stanList = stanList;
    }

    public List<Object[]> getRiskList() {
        return riskList;
    }

    public void setRiskList(List<Object[]> riskList) {
        this.riskList = riskList;
    }

    public List<Object[]> getActivityList() {
        return activityList;
    }

    public void setActivityList(List<Object[]> activityList) {
        this.activityList = activityList;
    }
}
