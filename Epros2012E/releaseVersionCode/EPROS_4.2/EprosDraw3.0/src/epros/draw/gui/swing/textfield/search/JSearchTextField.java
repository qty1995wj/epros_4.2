package epros.draw.gui.swing.textfield.search;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTable;
import javax.swing.JTextField;

/**
 * 
 * 带有快速查询功能的输入框
 * 
 * @author ZHOUXY
 * 
 */
public class JSearchTextField extends JTextField {
	/** 带有快速查询功能和手动选择内容的输入框文档监听 */
	private JecnSearchDocumentListener searchDocumentListener = null;
	/** 快速查询弹出表 */
	private JTable searchTable = null;

	public JSearchTextField() {
		initComponents();
	}
	public JSearchTextField(int widthInt,int heigthInt){
		// 项的内容大小
		Dimension dimension = new Dimension(widthInt, heigthInt);
		this.setPreferredSize(dimension);
		this.setMinimumSize(dimension);
		this.setMaximumSize(dimension);
		initComponents();
	}
	private void initComponents() {
		this.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {// 使用方向键选择table行
				keyKPPressed(e);
			}

			public void keyReleased(KeyEvent e) {// 点击键盘Enter执行动作
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					keyEnterReleased(e);
				}
			}
		});
	}

	/**
	 * 
	 * 快速查询标识（true：快速查询；false：非快速查询）
	 * ture：执行JecnSearchDocumentListener中insertUpdateJecn方法
	 * false：不执行JecnSearchDocumentListener中insertUpdateJecn方法
	 * 
	 */
	private boolean searchFlag = true;

	/**
	 * 
	 * 输入框赋值且改变searchFlag状态
	 * 
	 * @param t
	 *            String 内容
	 */
	public void setTextJecn(String t) {
		if (searchFlag) {// 通过setText添加
			searchFlag = false;
		}
		setText(t);
	}

	/**
	 * 
	 * 添加JecnSearchDocumentListener监听
	 * 
	 * @param listener
	 *            JecnSearchDocumentListener
	 */
	public void addDocumentListener(JecnSearchDocumentListener listener) {
		if (listener == null) {
			return;
		}
		this.searchDocumentListener = listener;
		this.getDocument().addDocumentListener(listener);
	}

	/**
	 * 
	 * 设置快速查询出的表
	 * 
	 * @param searchTable
	 *            JTable
	 */
	public void setSearchTable(JTable searchTable) {
		this.searchTable = searchTable;
	}

	/**
	 * 
	 * 快速查询标识
	 * 
	 * @return
	 */
	boolean isSearchFlag() {
		return searchFlag;
	}

	/**
	 * 
	 * 设置快速查询标识为true
	 * 
	 */
	void searchFlagTrue() {
		searchFlag = true;
	}

	/**
	 * 
	 * 使用方向键选择table行
	 * 
	 * @param e
	 *            KeyEvent
	 */
	private void keyKPPressed(KeyEvent e) {
		if (searchTable == null||searchTable.getRowCount()<=0) {
			return;
		}
		int keyCodeInt = e.getKeyCode();
		// 当前选中行
		int index = searchTable.getSelectedRow();
		if (keyCodeInt == KeyEvent.VK_DOWN || keyCodeInt == KeyEvent.VK_KP_DOWN) {
			if (index < 0 || index >= searchTable.getRowCount() - 1) {
				index = 0;
			} else {
				index += 1;
			}
			searchTable.setRowSelectionInterval(index, index);
		} else if (keyCodeInt == KeyEvent.VK_UP
				|| keyCodeInt == KeyEvent.VK_KP_UP) {
			if (index <= 0) {
				index = searchTable.getRowCount() - 1;
			} else {
				index -= 1;
			}
			searchTable.setRowSelectionInterval(index, index);
		}
	}

	/**
	 * 
	 * 点击键盘Enter执行动作
	 * 
	 * @param e
	 *            KeyEvent
	 */
	private void keyEnterReleased(KeyEvent e) {
		if (searchTable == null || searchDocumentListener == null) {
			return;
		}
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			searchDocumentListener.keyEnterReleased(e);
		}
	}
}
