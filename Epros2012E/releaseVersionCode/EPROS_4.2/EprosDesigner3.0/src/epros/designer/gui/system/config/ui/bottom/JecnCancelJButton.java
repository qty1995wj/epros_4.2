package epros.designer.gui.system.config.ui.bottom;

import java.awt.event.ActionEvent;

import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;

/**
 * 
 * 关闭对话框按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnCancelJButton extends JecnAbstractBaseJButton {
	public JecnCancelJButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		
		this.setPreferredSize(null);
		this.setMaximumSize(null);
		this.setMinimumSize(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		dialog.setVisible(false);
	}
}
