package com.jecn.epros.server.dao.task;

import java.util.List;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.IBaseDao;

public interface IJecnTaskTestRunDao extends IBaseDao<JecnTaskBeanNew, Long> {
	/**
	 * 获取试运行任务（只有流程任务）相关信息及用户信息
	 * 
	 * @return List<Object[]>组装后的任务信息
	 */
	public List<Object[]> getTaskTestRun();

	/**
	 * 试运行到期提醒(提醒系统管理员)
	 */
	public List<Object[]> sendTestRunEndE();
}
