package epros.draw.data.line;

import java.awt.Point;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.JecnSystemData;

/**
 * 斜线数据处理类，斜线包括分割线、注释框的线段、阶段分隔符、虚线等等
 * 
 * @author ZHANGXH
 * 
 */
public class JecnBaseDivedingLineData extends JecnBaseLineData {
	/** 小线段数据对象 */
	protected JecnDiviLineDataCloneable diviLineCloneable = null;

	/**
	 * 
	 * 构造函数
	 * 
	 * 名称：DividingLine
	 * 
	 * @param mapElemType
	 *            MapElemType 分割线类型
	 */
	public JecnBaseDivedingLineData(MapElemType mapElemType) {

		super(mapElemType);

		this.diviLineCloneable = new JecnDiviLineDataCloneable();

		// 初始化图形属性
		JecnSystemData.initCurrFlowElementData(this);
	}

	@Override
	/**
	 * 
	 * 实例化JecnBaseDivedingLineData对象
	 * 
	 */
	public JecnBaseDivedingLineData newInstance() {
		return new JecnBaseDivedingLineData(mapElemType);
	}

	@Override
	/**
	 * 
	 * 克隆对象
	 * 
	 */
	public JecnBaseDivedingLineData clone() {
		return (JecnBaseDivedingLineData) this.currClone();
	}

	@Override
	/**
	 * 
	 * 
	 * 复制流程元素属性,参数不能为空
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(
			JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		if (!(srcFlowElementData instanceof JecnBaseDivedingLineData)
				|| !(destFlowElementData instanceof JecnBaseDivedingLineData)) {
			return;
		}

		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		JecnBaseDivedingLineData src = (JecnBaseDivedingLineData) srcFlowElementData;
		JecnBaseDivedingLineData dest = (JecnBaseDivedingLineData) destFlowElementData;

		// 小线段数据对象
		dest.setDiviLineCloneable(src.getDiviLineCloneable().clone());
	}


	/**
	 * 当横线或竖线时获取当前放大缩小倍数下的位置点
	 * 
	 * @return
	 */
	public Point getOriginalPoint() {
		switch (this.getDiviLineCloneable().getLineDirectionEnum()) {
		case horizontal:
			if (this.getDiviLineCloneable().getStartXY().getX() < this
					.getDiviLineCloneable().getEndXY().getX()) {
				return this.getDiviLineCloneable().getStartXY().getPoint();
			} else {
				return this.getDiviLineCloneable().getEndXY().getPoint();
			}
		case vertical:
			if (this.getDiviLineCloneable().getStartXY().getY() < this
					.getDiviLineCloneable().getEndXY().getY()) {
				return this.getDiviLineCloneable().getStartXY().getPoint();
			} else {
				return this.getDiviLineCloneable().getEndXY().getPoint();
			}
		}
		return null;
	}

	public JecnDiviLineDataCloneable getDiviLineCloneable() {
		return diviLineCloneable;
	}

	public JecnDiviLineDataCloneable getZoomDiviLineCloneable() {
		return getDiviLineCloneable().getZoomedLineSegmentCloneble(
				JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
	}

	public void setDiviLineCloneable(JecnDiviLineDataCloneable diviLineCloneable) {
		this.diviLineCloneable = diviLineCloneable;
	}

}
