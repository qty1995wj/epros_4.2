package com.jecn.epros.server.action.web.login.ad.datangyidong;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.JecnCommon;

/**
 * 
 * 大唐移动域登录，使用域账号和密码到域服务器验证登录
 * 
 * @author weidp
 *
 */
public class DaTangYiDongLoginAction extends JecnAbstractADLoginAction {

	public DaTangYiDongLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		try {
			// admin用户执行普通登录
			if ("admin".equals(loginAction.getLoginName())) {
				return loginAction.userLoginGen(true);
			}
			// 进行AD域验证
			int result = doLdapAuthority(DaTangYiDongAfterItem.DOMAIN1);
			// 不成功时继续验证子域
			if (result != 0) {
				result = doLdapAuthority(DaTangYiDongAfterItem.DOMAIN2);
			}
			if (result != 0) {
				result = doLdapAuthority(DaTangYiDongAfterItem.DOMAIN3);
			}
			log.info("权限验证结果:" + result);
			// 验证成功
			if (result == 0) {
				// 不进行密码验证 登录
				return loginAction.userLoginGen(false);
			} else if (result == 1) {
				// 用户名或密码不正确！
				this.loginAction.addFieldError("loginMessage", loginAction
						.getText("nameOrPasswordError"));
				return LoginAction.INPUT;
			} else if (result == 2) {
				// 无法验证用户，请联系系统管理员!
				this.loginAction.addFieldError("loginMessage", loginAction
						.getText("anJianZhiYuanADConnectFailed"));
				return LoginAction.INPUT;
			}
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
		}
		return LoginAction.INPUT;
	}

	/**
	 * 进行Ldap权限验证
	 * 
	 * @author weidp
	 * @date 2014-9-3 下午04:37:17
	 * @return 0 成功验证 1 验证失败（ 提示用户名或密码不正确） 2 连接Ldap服务器异常
	 */
	private int doLdapAuthority(String domain) {
		
		DirContext ctx = null;
		try {
			// 获取用户输入的Ldap账号
			String ldapAccount = loginAction.getLoginName();
			// 获取用户输入的密码
			String ldapPassword = loginAction.getDecodePwd(loginAction.getPassword());
			// 账号或密码为空
			if (JecnCommon.isNullOrEmtryTrim(ldapAccount)
					|| JecnCommon.isNullOrEmtryTrim(ldapPassword)) {
				return 1;
			}

			// 拼接域名
			if (!ldapAccount.startsWith(domain)) {
				ldapAccount = domain + ldapAccount;
			}
			
			// 获取Ldap验证环境
			Hashtable<String, String> ldapEnv = getLdapEnv(ldapAccount,
					ldapPassword);
			ctx = new InitialDirContext(ldapEnv);
			
			return 0;
		} catch (AuthenticationException ex) {
			log.error("Ldap权限验证失败：", ex);
			return 1;
		} catch (NamingException e) {
			log.error("连接Ldap服务器出现异常：", e);
			return 2;
		} catch (Exception e) {
			log.error("其他异常：", e);
			return 2;
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					log.error("释放资源出现异常：", e);
				}
			}
		}
	}

	/**
	 * 获取Ldap验证环境
	 * 
	 * @author weidp
	 * @date 2014-8-27 下午03:00:46
	 * @param account
	 *            用户在Epros登录页面输入的账号
	 * @param password
	 *            密码
	 * @return
	 */
	private Hashtable<String, String> getLdapEnv(String account, String password) {
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, DaTangYiDongAfterItem.LDAP_URL);
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, account);
		env.put(Context.SECURITY_CREDENTIALS, password);
		return env;
	}
}
