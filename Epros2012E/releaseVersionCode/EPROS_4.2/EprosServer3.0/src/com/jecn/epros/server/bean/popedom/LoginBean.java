package com.jecn.epros.server.bean.popedom;

import java.util.HashSet;
import java.util.Set;

/**
 * @author zhangchen Jul 30, 2012
 * @description：登录bean
 */
public class LoginBean implements java.io.Serializable {
	/** 人员信息 */
	private JecnUser jecnUser;
	/** 是否是管理员 */
	private boolean isAdmin = false;
	/** 是否是二级管理员 */
	private boolean isSecondAdmin = false;
	/** 是否有设计管理员-文件 */
	private boolean isDesignFileAdmin = false;
	/** 是否有设计管理员-流程 */
	private boolean isDesignProcessAdmin = false;
	/** 是否有设计管理员-制度 */
	private boolean isDesignRuleAdmin = false;
	/** 涉及角色信息 流程 */
	private Set<Long> setFlow;
	/** 涉及角色信息 文件 */
	private Set<Long> setFile;
	/** 涉及角色信息 标准 */
	private Set<Long> setStandiad;
	/** 涉及角色信息 制度 */
	private Set<Long> setRule;
	/** 设计角色信息 风险C */
	private Set<Long> setRisk;
	/** 设计角色信息 术语C */
	private Set<Long> setTerm;
	/** 自定义角色：组织权限范围 */
	private Set<Long> setOrg;
	/** 自定义角色：岗位组权限范围 */
	private Set<Long> setPosGroup;
	/** 0、是可以通过 1、用户不存在 2、密码不正确 3、用户是不是锁定,4另一台电脑已登录此用户,5用户没有设计权限 */
	private int loginState;
	/** 0：有浏览端标准版（默认）；99：有浏览端完整版；1：无浏览端标准版；100：无浏览端完整版 */
	private int versionType;
	/** 流程元素编辑点:1：左上进右下出（默认）；0：四个编辑点都能进出 */
	private int editPortType = 1;
	/**
	 * 登录类型（0：普通登录（默认）;
	 * 1:东航;2:中国科学院计算机网络信息中心;3:三幅百货单点登录；4：中石化&南京物探院;5:中数通信息有限公司;6:聚光;7:上海保隆 ）
	 * 20:中石化（石勘院）;22:BPM 安码
	 */
	public int otherLoginType = 0;

	public int getOtherLoginType() {
		return otherLoginType;
	}

	public void setOtherLoginType(int otherLoginType) {
		this.otherLoginType = otherLoginType;
	}

	public JecnUser getJecnUser() {
		return jecnUser;
	}

	public void setJecnUser(JecnUser jecnUser) {
		this.jecnUser = jecnUser;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public int getLoginState() {
		return loginState;
	}

	public void setLoginState(int loginState) {
		this.loginState = loginState;
	}

	public Set<Long> getSetFlow() {
		return setFlow;
	}

	public void setSetFlow(Set<Long> setFlow) {
		this.setFlow = setFlow;
	}

	public Set<Long> getSetFile() {
		if (setFile == null) {
			setFile = new HashSet<Long>();
		}
		return setFile;
	}

	public void setSetFile(Set<Long> setFile) {
		this.setFile = setFile;
	}

	public Set<Long> getSetStandiad() {
		return setStandiad;
	}

	public void setSetStandiad(Set<Long> setStandiad) {
		this.setStandiad = setStandiad;
	}

	public Set<Long> getSetRule() {
		return setRule;
	}

	public void setSetRule(Set<Long> setRule) {
		this.setRule = setRule;
	}

	// 风险get方法
	public Set<Long> getSetRisk() {
		return setRisk;
	}

	// 风险set方法
	public void setSetRisk(Set<Long> setRisk) {
		this.setRisk = setRisk;
	}

	
	
	public Set<Long> getSetTerm() {
		return setTerm;
	}

	public void setSetTerm(Set<Long> setTerm) {
		this.setTerm = setTerm;
	}

	public boolean isDesignFileAdmin() {
		return isDesignFileAdmin;
	}

	public void setDesignFileAdmin(boolean isDesignFileAdmin) {
		this.isDesignFileAdmin = isDesignFileAdmin;
	}

	public boolean isDesignProcessAdmin() {
		return isDesignProcessAdmin;
	}

	public void setDesignProcessAdmin(boolean isDesignProcessAdmin) {
		this.isDesignProcessAdmin = isDesignProcessAdmin;
	}

	public boolean isDesignRuleAdmin() {
		return isDesignRuleAdmin;
	}

	public void setDesignRuleAdmin(boolean isDesignRuleAdmin) {
		this.isDesignRuleAdmin = isDesignRuleAdmin;
	}

	public int getVersionType() {
		return versionType;
	}

	public void setVersionType(int versionType) {
		this.versionType = versionType;
	}

	public int getEditPortType() {
		return editPortType;
	}

	public void setEditPortType(int editPortType) {
		this.editPortType = editPortType;
	}

	public boolean isSecondAdmin() {
		return isSecondAdmin;
	}

	public void setSecondAdmin(boolean isSecondAdmin) {
		this.isSecondAdmin = isSecondAdmin;
	}

	public Set<Long> getSetOrg() {
		return setOrg;
	}

	public void setSetOrg(Set<Long> setOrg) {
		this.setOrg = setOrg;
	}

	public Set<Long> getSetPosGroup() {
		return setPosGroup;
	}

	public void setSetPosGroup(Set<Long> setPosGroup) {
		this.setPosGroup = setPosGroup;
	}

}
