package epros.draw.data.line;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.system.JecnSystemData;

/**
 * 
 * 所有线的父类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnBaseLineData extends JecnAbstractFlowElementData {

	public JecnBaseLineData(MapElemType mapElemType) {
		super(mapElemType);

		// 初始化线属性
		JecnSystemData.initCurrFlowElementData(this);
	}
}
