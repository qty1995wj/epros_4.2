package com.jecn.viewdoc.convert;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.viewdoc.MSOffice.OfficeTools;
import com.jecn.viewdoc.MSOffice.PDFWaterMarkUtils;
import com.jecn.viewdoc.convert.view.CommonView;
import com.jecn.viewdoc.convert.view.PageofficeView;
import com.opensymphony.xwork2.ActionContext;

public class ViewDocsManager {

	private static Logger log = Logger.getLogger(ViewDocsManager.class);

	public static final String FILE = "FILE";
	public static final String FILE_T = "FILE_T";
	public static final String PROCESS_FILE = "PROCESS_FILE";
	public static final String RULE = "RULE";
	public static final String PROCESS = "PROCESS";

	public static boolean isAllowedExtension(String fileName) {
		String extension = FilenameUtils.getExtension(fileName).toLowerCase();
		if (JecnContants.otherLoginType == 23) {
			if ("pdf".equals(extension)) {// 时代电气要求 除了PDF 全部下载
				return true;
			} else {
				return false;
			}
		} else if (JecnConfigTool.isLiBangLoginType()) {
			return new HashSet<String>(Arrays.asList(PageofficeView.DEFAULT_ALLOW)).contains(extension);
		} else {
			return new HashSet<String>(Arrays.asList(CommonView.DEFAULT_ALLOW)).contains(extension);
		}
	}

	public static boolean enabledOnlineViewDownload() {
		return JecnContants.SHOW_VALUE.equals(JecnContants.getValue(ConfigItemPartMapMark.enableOnlineViewDownload
				.toString())) ? true : false;
	}

	private static String getInputFilePath(String fileName, byte[] bytes) {
		String extension = FilenameUtils.getExtension(fileName);
		File temp = null;
		try {
			temp = new File(ViewDocsPathManager.TEMP_DIR + UUID.randomUUID().toString() + "." + extension);
			FileUtils.writeByteArrayToFile(temp, bytes);
		} catch (Exception e) {
			log.error("获取需要转换的预览文件时出现异常", e);
		}
		return temp.getPath();
	}

	/**
	 * @author Angus
	 * @param fileName
	 *            流程的名字
	 * @param bytes
	 *            字节流
	 * @return 该方法成功会在tmep目录保存流程文件的xml版的doc
	 */
	public static String getProcessFilePath(String fileName, byte[] bytes) {
		String extension = FilenameUtils.getExtension(fileName);
		File temp = null;
		try {
			temp = new File(ViewDocsPathManager.TEMP_DIR + fileName);
			FileUtils.writeByteArrayToFile(temp, bytes);
		} catch (Exception e) {
			log.error("获取需要转换的预览文件时出现异常", e);
		}
		return temp.getPath();
	}

	private static String getConvertType(String extension) {
		if (extension.equals("xls") || extension.equals("xlsx")) {
			return DocumentConverter.EXCEL;
		} else if (isImage(extension)) {
			return DocumentConverter.IMG;
		} else if (extension.equals("pdf")) {
			return DocumentConverter.NONE;
		}
		return DocumentConverter.PDF;
	}

	public static boolean isImage(String imagePath) {
		return imagePath.toLowerCase().endsWith("jpg") || imagePath.toLowerCase().endsWith("png")
				|| imagePath.toLowerCase().endsWith("gif") || imagePath.toLowerCase().endsWith("bmp");
	}

	private static String getTargetPath(String dataType, String convertType) {
		String dirPath = getDirPath(dataType);
		String extesion = convertType == DocumentConverter.EXCEL ? "html" : "pdf";
		return dirPath + UUID.randomUUID().toString() + "." + extesion;
	}

	private static String getTargetProcessPath(String dataType, String fileName) {
		String dirPath = getDirPath(dataType);
		return dirPath + fileName + ".pdf";
	}

	private static String getDirPath(String dataType) {
		String dirPath;
		if (dataType.equals(FILE)) {
			dirPath = ViewDocsPathManager.FILE_DIR;
		} else if (dataType.equals(FILE_T)) {
			dirPath = ViewDocsPathManager.FILE_T_DIR;
		} else if (dataType.equals(PROCESS_FILE)) {
			dirPath = ViewDocsPathManager.PROCESS_FILE;
		} else if (dataType.equals(PROCESS)) {
			dirPath = ViewDocsPathManager.PROCESS;
		} else {
			dirPath = ViewDocsPathManager.RULE_DIR;
		}
		return dirPath;
	}

	public static String buildViewDoc(byte[] bytes, String fileName, String dataType) throws Exception {
		String convertType = getConvertType(FilenameUtils.getExtension(fileName));
		String relativePath = "";
		String tempFilePath = "";
		if (convertType.equals(DocumentConverter.NONE)) {
			File temp = new File(getDirPath(dataType) + UUID.randomUUID().toString() + ".pdf");
			FileUtils.writeByteArrayToFile(temp, bytes);
			tempFilePath = temp.getAbsolutePath();
			relativePath = getRelativePath(temp.getAbsolutePath());
		} else if (convertType.equals(DocumentConverter.IMG)) {
			File temp = new File(getDirPath(dataType) + UUID.randomUUID().toString() + "."
					+ FilenameUtils.getExtension(fileName));
			FileUtils.writeByteArrayToFile(temp, bytes);
			tempFilePath = temp.getAbsolutePath();
			relativePath = getRelativePath(temp.getAbsolutePath());
		} else {
			tempFilePath = getInputFilePath(fileName, bytes);

			ConvertInfo convertInfo = new ConvertInfo();
			convertInfo.setInputPath(tempFilePath);
			convertInfo.setConvertType(convertType);
			convertInfo.setOutputPath(getTargetPath(dataType, convertInfo.getConvertType()));
			int n = OfficeTools.officeToPdf(convertInfo.getInputPath(), convertInfo.getOutputPath());
			// DocumentConverter.convert(convertInfo);

			relativePath = getRelativePath(convertInfo.getOutputPath());
			FileUtils.deleteQuietly(new File(tempFilePath));
		}
		if (relativePath.endsWith("html")) {
			return relativePath.replaceAll("\\\\", "/");
		}
		if (convertType.equals(DocumentConverter.IMG)) {
			return relativePath.replaceAll("\\\\", "/");
		}
		return ViewDocsPathManager.RELAVTIVE_PATH + relativePath.replaceAll("\\\\", "/");
	}

	public static String buildProcessViewDoc(byte[] bytes, String fileName, String dataType) throws Exception {
		String convertType = getConvertType(FilenameUtils.getExtension(fileName));
		String relativePath = "";
		String tempFilePath = "";

		// 在这里开始准备DOC转pdf 呵呵
		tempFilePath = getProcessFilePath(fileName, bytes);

		ConvertInfo convertInfo = new ConvertInfo();
		convertInfo.setInputPath(tempFilePath);
		convertInfo.setConvertType(convertType);
		convertInfo.setOutputPath(getTargetProcessPath(dataType, fileName));

		// 需要判断目录下是否已经有了，有了的话需要删除后新建
		File dir = new File(getDirPath(dataType));
		File[] fileLists = dir.listFiles();
		for (int i = 0; fileLists != null && i < fileLists.length; i++) {
			if (null != fileLists[i].getName() && fileLists[i].getName().contains(fileName)) {
				fileLists[i].delete();
			}
		}

		// if ("doc".equals(FilenameUtils.getExtension(fileName)) ||
		// "docx".equals(FilenameUtils.getExtension(fileName))) {
		int n = OfficeTools.officeToPdf(convertInfo.getInputPath(), convertInfo.getOutputPath());
		// } else {
		// DocumentConverter.convert(convertInfo);
		// }

		relativePath = getRelativePath(convertInfo.getOutputPath());

		FileUtils.deleteQuietly(new File(tempFilePath));

		return ViewDocsPathManager.RELAVTIVE_PATH + relativePath.replaceAll("\\\\", "/");
	}

	public static byte[] getBytesByProcessDocToPDF(byte[] bytes, String fileName, String dataType) throws Exception {
		String suffixStr = FilenameUtils.getExtension(fileName);
		String convertType = getConvertType(suffixStr);
		String tempFilePath = "";

		// 在这里开始准备DOC转pdf 呵呵
		tempFilePath = getProcessFilePath(fileName, bytes);

		ConvertInfo convertInfo = new ConvertInfo();
		convertInfo.setInputPath(tempFilePath);
		convertInfo.setConvertType(convertType);
		convertInfo.setOutputPath(getTargetProcessPath(dataType, fileName));

		// 需要判断目录下是否已经有了，有了的话需要删除后新建
		File dir = new File(getDirPath(dataType));
		File[] fileLists = dir.listFiles();
		for (int i = 0; fileLists != null && i < fileLists.length; i++) {
			if (null != fileLists[i].getName() && fileLists[i].getName().contains(fileName)) {
				fileLists[i].delete();
			}
		}

		// if ("doc".equals(suffixStr) || "docx".equals(suffixStr)) {
		int n = OfficeTools.officeToPdf(convertInfo.getInputPath(), convertInfo.getOutputPath());
		// } else {
		// DocumentConverter.convert(convertInfo);
		// }

		InputStream in = new FileInputStream(convertInfo.getOutputPath());
		return toByteArray(in);
	}

	public static byte[] getBytesByProcessDocToPDF(byte[] bytes, String fileName, String dataType, String mark)
			throws Exception {
		String suffixStr = FilenameUtils.getExtension(fileName);
		String convertType = getConvertType(suffixStr);
		String tempFilePath = "";

		// 在这里开始准备DOC转pdf 呵呵
		tempFilePath = getProcessFilePath(fileName, bytes);

		ConvertInfo convertInfo = new ConvertInfo();
		convertInfo.setInputPath(tempFilePath);
		convertInfo.setConvertType(convertType);
		convertInfo.setOutputPath(getTargetProcessPath(dataType, fileName));

		// 需要判断目录下是否已经有了，有了的话需要删除后新建
		File dir = new File(getDirPath(dataType));
		File[] fileLists = dir.listFiles();
		for (int i = 0; fileLists != null && i < fileLists.length; i++) {
			if (null != fileLists[i].getName() && fileLists[i].getName().contains(fileName)) {
				fileLists[i].delete();
			}
		}

		// if ("doc".equals(suffixStr) || "docx".equals(suffixStr)) {
		int n = OfficeTools.officeToPdf(convertInfo.getInputPath(), convertInfo.getOutputPath(), mark);
		// } else {
		// DocumentConverter.convert(convertInfo);
		// }

		InputStream in = new FileInputStream(convertInfo.getOutputPath());
		return toByteArray(in);
	}

	private static byte[] toByteArray(InputStream in) throws IOException {

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024 * 4];
		int n = 0;
		while ((n = in.read(buffer)) != -1) {
			out.write(buffer, 0, n);
		}
		return out.toByteArray();
	}

	private static String getRelativePath(String absolutePath) {
		return absolutePath.substring(ViewDocsPathManager.ROOT_DIR.length());
	}

	public static boolean enabledOnlineView() {
		return JecnContants.SHOW_VALUE.equals(JecnContants.getValue(ConfigItemPartMapMark.enableOnlineView.toString())) ? true
				: false;
	}
}
