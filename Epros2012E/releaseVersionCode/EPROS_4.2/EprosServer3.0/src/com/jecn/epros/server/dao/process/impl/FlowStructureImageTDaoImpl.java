package com.jecn.epros.server.dao.process.impl;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnMapRelatedStructureImageT;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.process.IFlowStructureImageTDao;

public class FlowStructureImageTDaoImpl extends AbsBaseDao<JecnFlowStructureImageT, Long> implements
		IFlowStructureImageTDao {
	@Override
	public List<JecnFlowStructureImageT> findAllByFlowId(Long flowId) throws Exception {
		String hql = "from JecnFlowStructureImageT where flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
	public void updateLinkedImageName(Long flowId, String showName) throws Exception {

		String sql = "UPDATE JECN_FLOW_STRUCTURE_IMAGE_T SET FIGURE_TEXT=? WHERE LINK_FLOW_ID=?";
		this.execteNative(sql, showName, flowId);

	}

	/**
	 * 获取流程架构-集成关系图元素集合
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<JecnMapRelatedStructureImageT> findMapRelatedStructureImageTById(Long flowId) throws Exception {
		String hql = "from JecnMapRelatedStructureImageT where flowId=?";
		return this.listHql(hql, flowId);
	}
}
