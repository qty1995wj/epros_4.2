package epros.designer.gui.system;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.LoginBean;

import epros.designer.gui.autoNum.MengNiuAutoNumManageDialog;
import epros.designer.gui.autoNum.mulinsen.MulinsenAutoNumManageDialog;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.define.JecnTermDefineDialog;
import epros.designer.gui.file.FileManageDialog;
import epros.designer.gui.integration.internalControl.ControlGuideManageDialog;
import epros.designer.gui.popedom.person.PasswordInputDialog;
import epros.designer.gui.popedom.person.PersonManageDialog;
import epros.designer.gui.popedom.role.RoleManageDialog;
import epros.designer.gui.process.flowtool.FlowToolManageDialog;
import epros.designer.gui.project.ProjectManageDialog;
import epros.designer.gui.rule.mode.RuleModeManageDialog;
import epros.designer.gui.system.menu.JecnRecoverMenu;
import epros.designer.gui.system.menu.JecnSetMenu;
import epros.designer.gui.system.menu.JecnSetTaskMenu;
import epros.designer.gui.system.seting.JecnAutoCodeSet;
import epros.designer.gui.workflow.WorkFlowBakTimer;
import epros.designer.gui.workflow.WorkFlowTimer;
import epros.designer.service.process.JecnTemplate;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnLoginUtil;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏设置功能面板
 * 
 * @author ZHOUXY
 * 
 */
public class ToolbarSetingButtonPanel extends JPanel {
	private static Logger log = Logger.getLogger(ToolbarSetingButtonPanel.class);
	/** 角色管理、人员管理、岗位组的容器 */
	private JecnPanel panelOne = null;
	/** 内控指引知识库、流程支持工具、文件管理、制度模板管理的容器 */
	private JecnPanel panelTwo = null;
	/** 设置 */
	private JecnPanel panelThree = null;
	/** 刷新的容器 */
	private JecnPanel panelFive = null;

	/** 角色管理 */
	private JecnToolbarButton roleButton = null;
	/** 人员管理 */
	private JecnToolbarButton personButton = null;
	/** 项目管理 */
	private JecnToolbarButton projectButton = null;

	/** 流程支持工具 */
	private JecnToolbarButton processToolButton = null;
	/** 文件管理 */
	private JecnToolbarButton fileButton = null;
	/***
	 * 文件合并
	 */
	private JecnToolbarButton fileMergeButton = null;
	/** 制度模板 */
	private JecnToolbarButton ruleModeButton = null;
	/** 刷新 */
	private JecnToolbarButton refreshButton = null;

	/** 内控指引知识库 */
	private JecnToolbarButton interControlButton = null;
	/** 术语定义 */
	private JecnToolbarButton termDefineButton = null;
	/** 自动编号 **/
	private JecnToolbarButton autoCodeButton = null;
	/**
	 * 任务设置
	 */
	private JecnToolbarButton taskBut = null;
	/**
	 * 回收站
	 */
	private JecnToolbarButton recoverBut = null;
	/**
	 * 系统设置
	 */
	private JecnToolbarButton systemBut = null;

	public ToolbarSetingButtonPanel() {
		// 初始化组件
		initComponent();
		// 布局
		if (JecnDesignerCommon.isAdmin() || JecnDesignerCommon.isSecondAdmin()) {
			initLayout();
		} else {
			initDesignerLayout();
		}
		initPanelOneChildComp();
		initPanelTwoChildComp();
		if (JecnDesignerCommon.isAdmin() || JecnDesignerCommon.isSecondAdmin()) {
			initPanelThreeChildComp();
		}
		// initPanelFourChildComp();
		initPanelFiveChildComp();
		// 添加事件
		initAction();
	}

	/**
	 * 
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponent() {
		// 角色管理、人员管理、岗位组的容器
		panelOne = new JecnPanel();
		// 内控指引知识库、流程支持工具、文件管理、制度模板管理的容器
		panelTwo = new JecnPanel();
		// 项目管理，回收站，配置容器
		panelThree = new JecnPanel();
		// 刷新的容器
		panelFive = new JecnPanel();
		// 任务设置
		taskBut = new JecnToolbarButton(JecnProperties.getValue("taskSetting"), "processTool");
		// 回收站
		recoverBut = new JecnToolbarButton(JecnProperties.getValue("recyclingBin"), "processRecover");
		// 系统设置
		systemBut = new JecnToolbarButton(JecnProperties.getValue("systemSetLab"), "eprosSet");
		// 角色管理
		roleButton = new JecnToolbarButton(JecnProperties.getValue("roleManage"), "roleManage");
		// 人员管理
		personButton = new JecnToolbarButton(JecnProperties.getValue("personManage"), "personManage");
		// 流程支持工具
		processToolButton = new JecnToolbarButton(JecnProperties.getValue("processTool"), "processTool");
		// 项目管理
		projectButton = new JecnToolbarButton(JecnProperties.getValue("projectMange"), "projectMange");
		// 制度模板
		ruleModeButton = new JecnToolbarButton(JecnProperties.getValue("ruleMode"), "ruleMode");
		// 文件管理
		fileButton = new JecnToolbarButton(JecnProperties.getValue("StandardDocument"), "fileManage");
		fileMergeButton = new JecnToolbarButton("文件合并", "fileMerge");
		if (!JecnConstants.isPubShow()) {
			fileButton.setHorizontalTextPosition(SwingConstants.CENTER);
			fileButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		}
		// 术语定义
		termDefineButton = new JecnToolbarButton(JecnProperties.getValue("termDefine"), "termDefine");
		// 刷新
		refreshButton = new JecnToolbarButton(JecnProperties.getValue("refresh"), "refresh");
		refreshButton.setHorizontalAlignment(SwingConstants.CENTER);
		refreshButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		refreshButton.setHorizontalTextPosition(SwingConstants.CENTER);

		projectButton.setHorizontalAlignment(SwingConstants.CENTER);
		projectButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		projectButton.setHorizontalTextPosition(SwingConstants.CENTER);

		// 边框
		panelOne.setBorder(JecnUIUtil.getTootBarBorder());
		panelTwo.setBorder(JecnUIUtil.getTootBarBorder());
		panelThree.setBorder(JecnUIUtil.getTootBarBorder());
		// panelFour.setBorder(JecnUIUtil.getTootBarBorder());
		panelFive.setBorder(JecnUIUtil.getTootBarBorder());

		// 设置人员管理按钮居中靠下
		// personButton.setHorizontalTextPosition(SwingConstants.CENTER);
		// personButton.setVerticalTextPosition(SwingConstants.BOTTOM);

		// 内控指引知识库
		interControlButton = new JecnToolbarButton(JecnProperties.getValue("controlDB"), "interControl");

		// 自动编号
		autoCodeButton = new JecnToolbarButton(JecnProperties.getValue("numberManagement"), null);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 布局管理器
		this.setLayout(new GridBagLayout());

		// 是否存在浏览端，不存在则隐藏：流程支持工具、项目管理、人员管理、角色管理、刷新
		if (!JecnConstants.isPubShow()) {
			// 流程支持工具
			processToolButton.setVisible(false);
			// 项目管理
			projectButton.setVisible(false);
			// 角色管理,人员管理容器
			panelOne.setVisible(false);
			// 刷新
			panelFive.setVisible(false);
		}
	}

	public JecnToolbarButton getProjectButton() {
		return projectButton;
	}

	public void setProjectButton(JecnToolbarButton projectButton) {
		this.projectButton = projectButton;
	}

	/**
	 * 
	 * 布局
	 * 
	 */
	private void initLayout() {
		// 内控指引知识库、流程支持工具、文件管理、制度模板管理的容器
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets3(), 0, 0);
		this.add(panelTwo, c);
		// 角色管理、人员管理、岗位组的容器
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(panelOne, c);

		// // 流程回收、项目回收、组织回收的容器
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets3(), 0, 0);
		this.add(panelThree, c);
		// 刷新的容器
		c = new GridBagConstraints(4, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets3(), 0, 0);
		this.add(panelFive, c);
	}

	private void initDesignerLayout() {
		// 内控指引知识库、流程支持工具、文件管理、制度模板管理的容器
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets3(), 0, 0);
		this.add(panelTwo, c);

		// 刷新的容器
		c = new GridBagConstraints(4, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets3(), 0, 0);
		this.add(panelFive, c);
	}

	/**
	 * 
	 * 布局：角色管理、人员管理、岗位组的容器
	 * 
	 */
	private void initPanelOneChildComp() {
		// 岗位组管理(已移除)
		GridBagConstraints c = null;
		// c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
		// GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
		// JecnUIUtil.getInsets0(), 0, 0);
		// panelOne.add(positionGroupButton.getJToolBar(), c);
		// 角色管理
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		panelOne.add(roleButton.getJToolBar(), c);

		// 人员管理
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		panelOne.add(personButton.getJToolBar(), c);

	}

	/**
	 * 
	 * 布局：内控指引知识库、流程支持工具、文件管理、制度模板管理
	 * 
	 */
	private void initPanelTwoChildComp() {
		panelTwo.setLayout(new GridBagLayout());
		if (JecnDesignerCommon.isAdmin() || JecnDesignerCommon.isSecondAdmin()) {
			initAdminPanelTwo();
		} else {
			initDesignerPanelTwo();
		}
	}

	/**
	 * 设计者布局
	 */
	private void initDesignerPanelTwo() {
		// 流程支持工具
		GridBagConstraints c = null;

		// 文件管理
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		panelTwo.add(fileButton.getJToolBar(), c);

		// 回收站
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		panelTwo.add(recoverBut.getJToolBar(), c);

	}

	private void initAdminPanelTwo() {
		// 流程支持工具
		GridBagConstraints c = null;
		if (JecnConstants.isPubShow()) {//
			c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0(), 0, 0);
			panelTwo.add(processToolButton.getJToolBar(), c);
		}
		// 文件管理
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		panelTwo.add(fileButton.getJToolBar(), c);

		// 制度模板管理
		if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {
			// 内控指引知识库
			c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0(), 0, 0);
			panelTwo.add(interControlButton.getJToolBar(), c);
			if (!JecnConfigTool.isHiddenRule()) {
				c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
						JecnUIUtil.getInsets0(), 0, 0);
				panelTwo.add(ruleModeButton.getJToolBar(), c);
			}
		}

		// 任务模板
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		// panelTwo.add(new JecnTaskSet().getGroupRecoverBtn().getJToolBar(),
		// c);
		panelTwo.add(taskBut.getJToolBar(), c);

		if (JecnConfigTool.isShowAutoNumber()) {// 是否允许自动编号
			// 自动编号
			c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					JecnUIUtil.getInsets0(), 0, 0);
			if (JecnConfigTool.isLiBangLoginType()) {
				panelTwo.add(new JecnAutoCodeSet().getGroupRecoverBtn().getJToolBar(), c);
			} else {
				panelTwo.add(autoCodeButton.getJToolBar(), c);
			}
		}

		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		// panelTwo.add(this.termDefineButton.getJToolBar(), c);
	}

	/**
	 * 
	 * 布局：项目管理、流程支持工具、文件管理、制度模板管理的容器
	 * 
	 */
	private void initPanelThreeChildComp() {
		// 回收站
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, JecnUIUtil.getInsets0(), 0, 0);
		panelThree.add(recoverBut.getJToolBar(), c);
		// 系统配置
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0(), 0, 0);
		// panelThree.add(new JecnSet().getGroupRecoverBtn().getJToolBar(), c);
		panelThree.add(systemBut.getJToolBar(), c);
		// 项目管理
		c = new GridBagConstraints(1, 0, 1, 2, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		panelThree.add(projectButton.getJToolBar(), c);
	}

	/**
	 * 
	 * 布局：刷新的容器
	 * 
	 */
	private void initPanelFiveChildComp() {
		// 刷新
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets0(), 0, 0);
		panelFive.add(refreshButton.getJToolBar(), c);
	}

	/**
	 * 
	 * 添加事件
	 * 
	 */
	private void initAction() {
		// 角色管理
		roleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (JecnConstants.projectId == null) {
					return;
				}
				if (!JecnDesignerCommon.isAdminAuthTip()) {
					return;
				}
				RoleManageDialog rd = new RoleManageDialog();
				rd.setVisible(true);
			}
		});
		// 人员管理
		personButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (JecnConstants.projectId == null) {
					return;
				}
				if (!JecnDesignerCommon.isAdminAuthTip()) {// 管理员和二级管理员可以操作
					return;
				}
				PersonManageDialog pmd = new PersonManageDialog();
				pmd.setVisible(true);
			}
		});

		//
		interControlButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!JecnDesignerCommon.isAdminTip()) {
					return;
				}
				ControlGuideManageDialog guideManageDialog = new ControlGuideManageDialog();
				guideManageDialog.setVisible(true);
			}
		});

		// 流程支持工具
		processToolButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!JecnDesignerCommon.isAdminTip()) {
					return;
				}
				FlowToolManageDialog ftmd = new FlowToolManageDialog();
				ftmd.setVisible(true);
			}
		});

		// 项目管理
		projectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ProjectManageDialog pmd = new ProjectManageDialog();
				pmd.setVisible(true);
			}
		});
		// 制度模板
		ruleModeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!JecnDesignerCommon.isAdminTip()) {
					return;
				}
				RuleModeManageDialog rd = new RuleModeManageDialog();
				rd.setVisible(true);
			}
		});
		// 文件管理
		fileButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (JecnConstants.projectId == null) {
					return;
				}
				FileManageDialog fd = new FileManageDialog();
				fd.setVisible(true);
			}
		});

		// 刷新
		refreshButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RefreshTask refreshTask = new RefreshTask();
				// refreshTask.execute();
				if (!DrawCommon.isNullOrEmtry(JecnLoading.start(refreshTask))) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("refreshError"));
				}
			}

		});
		// 术语定义
		termDefineButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JecnTermDefineDialog jecnTermDefineDialog = new JecnTermDefineDialog();
				jecnTermDefineDialog.setVisible(true);
			}
		});
		// 自动编号管理
		autoCodeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (JecnConfigTool.isMengNiuOperaType()) {
					MengNiuAutoNumManageDialog dialog = new MengNiuAutoNumManageDialog();
					dialog.setVisible(true);
				} else {
					MulinsenAutoNumManageDialog dialog = new MulinsenAutoNumManageDialog();
					dialog.setVisible(true);
				}
			}
		});
		// 任务设置
		taskBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!JecnDesignerCommon.isAdminTip()) {
					return;
				}
				// TODO Auto-generated method stub
				JecnSetTaskMenu menu = new JecnSetTaskMenu();
				menu.show((JecnToolbarButton) e.getSource(), 0, 25);
			}
		});
		// 回收站
		recoverBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JecnRecoverMenu menu = new JecnRecoverMenu();
				menu.show((JecnToolbarButton) e.getSource(), 0, 25);
			}
		});
		// 系统设置
		systemBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (!JecnDesignerCommon.isAdminTip()) {
					return;
				}
				JecnSetMenu menu = new JecnSetMenu();
				menu.show((JecnToolbarButton) e.getSource(), 0, 25);
			}
		});
	}

	/**
	 * @author yxw 2012-12-17
	 * @throws Exception
	 * @description:刷新系统
	 */
	private void refresh() throws Exception {

		// 1.RMI重置接链
		ConnectionPool.actionToNull();

		try {
			JecnUser jecnUser = JecnConstants.loginBean.getJecnUser();
			// 2.用户重登录
			LoginBean loginBean = ConnectionPool.getPersonAction().loginDesign(jecnUser.getLoginName(),
					jecnUser.getPassword(), JecnLoginUtil.getMacAddress(), 3);
			if (loginBean != null) {
				if (loginBean.getLoginState() != 0) {
					userLogin(jecnUser.getLoginName(), loginBean);
					return;
				}
				JecnConstants.loginBean = loginBean;
			}

		} catch (Exception e1) {
			log.error("ToolbarSetingBUttonPanel refresh is error", e1);
			throw e1;
		}
		// 定时器刷新
		WorkFlowTimer.refresh();
		// 自动备份定时器刷新 V3.06
		WorkFlowBakTimer.getInstance().refresh();
		// 3.模型刷新
		JecnTemplate.readTemplateDataToWorkflow();
		JecnDrawMainPanel.getMainPanel().getBoxPanel().setPartMapModelScrollPane(null);
		JecnDrawMainPanel.getMainPanel().getBoxPanel().setTotalMapModelScrollPane(null);
		JecnDrawMainPanel.getMainPanel().getBoxPanel().setOrgMapModelScrollPane(null);

		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow != null) {
			JecnDrawMainPanel.getMainPanel().getBoxPanel().showModePanel(workflow.getFlowMapData().getMapType());
		}

	}

	private void userLogin(String loginName, LoginBean loginBean) throws Exception {
		// 用户已删除
		if (loginBean.getLoginState() == 1) {
			JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnProperties.getValue("userDeleted"));
			System.exit(0);
			// 用户密码已改 ,重新输入密码
		} else if (loginBean.getLoginState() == 2) {
			PasswordInputDialog d = new PasswordInputDialog();
			d.setVisible(true);
			if (d.getPassword() == null) {
				return;
			}
			loginBean = ConnectionPool.getPersonAction().loginDesign(loginName, d.getPassword(),
					JecnLoginUtil.getMacAddress(), 0);
			if (loginBean.getLoginState() != 0) {
				userLogin(loginName, loginBean);
			}
		}
	}

	class RefreshTask extends SwingWorker<Void, Void> {

		@Override
		protected Void doInBackground() throws Exception {
			try {
				refresh();
			} catch (Exception e) {
				log.error("ToolbarSetingBUttonPanel RefreshTask is error", e);
				done();
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			}
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}
}
