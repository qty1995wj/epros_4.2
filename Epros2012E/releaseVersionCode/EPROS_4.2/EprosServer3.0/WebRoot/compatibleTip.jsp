<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>
<%
	Cookie cookies[] = request.getCookies();
	if (cookies != null) {
		for (Cookie c : cookies) {
			if (c.getName().equals("compatibleTipShow")) {
				request.setAttribute("compatibleTipShow", c.getValue());
			}
		}
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/float.css" />
		<script type="text/javascript">
</script>
	</head>
	<body>
		<!--[if IE]>
	 <!--[if lte IE 9]>
		<s:if test="%{#request.compatibleTipShow!='false'}">
			<div id="compatible_tip" class="main_tip">
				<div class="tip_contaner">
					<span class="main_tip_message">IE9以及IE9以下不支持资源检索功能</span>
					<span class="main_tip_close_image_span"
						onclick="compatibleTipClose()">
							<img class="main_tip_close_image" src="images/close_item.gif" />
					</span>
				</div>
			</div>
		</s:if>
		 <![endif]-->
	</body>

</html>
