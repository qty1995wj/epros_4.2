package epros.designer.gui.integration.internalControl;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

/**
 * 内控指引知识库节点移动
 * 
 * @author Administrator
 * 
 */
public class HighEfficiencyControlGuideMoveTree extends JecnHighEfficiencyTree {

	private static final long serialVersionUID = 1L;

	private Logger log = Logger
			.getLogger(HighEfficiencyControlGuideMoveTree.class);

	/** 当前项目根节点下的角色目录 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/**
	 * 内控指引知识库节点移动构造方法
	 * 
	 * @param listIds
	 *            节点集合
	 */
	public HighEfficiencyControlGuideMoveTree(List<Long> listIds) {
		super(listIds);
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree tree) {
		return new ControlGuideMoveTreeListener(this.getListIds(), tree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getControlGuideAction()
					.getChildJecnControlGuides(Long.valueOf(0),
							JecnConstants.projectId);
		} catch (Exception e) {
			log.error("HighEfficiencyControlGuideMoveTree getTreeModel is error", e);
		}
		return ControlGuideCommon.getControlGuideTreeModel(list);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {

	}

}
