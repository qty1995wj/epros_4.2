package com.jecn.epros.server.dao.control;

import java.util.List;

import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.IBaseDao;

public interface IJecnDocControlDao extends IBaseDao<JecnTaskHistoryNew, Long> {
	/**
	 * 验证输入的编号在当前版本记录下是否唯一
	 * 
	 * @param docId
	 *            主键ID
	 * @return String 返回文件存储在本地的路径
	 * @throws Exception
	 */
	public int getVersionIdCountById(String versionId) throws Exception;

	/**
	 * 
	 * 根据流程ID获取文控信息版本号集合
	 * 
	 * @param flowId
	 *            流程ID
	 * @return List<String>文控信息版本号集合
	 */
	public List<String> findVersionListByFlowId(Long flowId, int type) throws Exception;

	/**
	 * 文件文控类型和关联ID获取文控信息版本记录
	 * 
	 * @param refId
	 *            关联ID
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
	 * @return List<JecnTaskHistoryNew>
	 */
	public List<JecnTaskHistoryNew> getJecnTaskHistoryNewList(Long refId, int type) throws Exception;

	/**
	 * 文件关联ID获取文控信息版本记录
	 * 
	 * @param refId
	 *            关联ID
	 * @return List<JecnTaskHistoryNew>
	 */
	public List<JecnTaskHistoryNew> getTaskHistoryNewListById(Long refId) throws Exception;

	/**
	 * 获取任务审批阶段信息
	 * 
	 * @param flowId
	 *            流程id
	 * @param projectId
	 *            项目ID
	 * @return
	 */
	public List<Object[]> getTaskCheckStageInfo(long flowId, long projectId);

	/**
	 * @author yxw 2012-11-8
	 * @description:
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnTaskHistoryNew getJecnTaskHistoryNew(Long id) throws Exception;

	/**
	 * 
	 * 获取流程中存在的制度ID
	 * 
	 * @param flowId
	 *            Long 流程主键ID
	 * @return List<Object[]> Object[]在流程中存在的制度主键集合 0:制度主键ID，1制度类别,2编号
	 */
	public List<Object[]> getRulesInProcess(Long flowId) throws Exception;

	/**
	 * 
	 * 获取流程地图中存在的制度ID
	 * 
	 * @param flowId
	 *            Long 流程地图主键ID
	 * @return List<Long>在流程地图中存在的制度主键集合
	 */
	public List<Long> getRulesInProcessMap(Long flowId) throws Exception;

	/**
	 * 保存文控信息
	 * 
	 * @param historyNew
	 * @throws Exception
	 */
	public void saveJecnTaskHistoryNew(JecnTaskHistoryNew historyNew) throws Exception;

	/**
	 * 删除文控信息
	 * 
	 * @param historyNew
	 * @throws Exception
	 */
	public void deleteJecnTaskHistoryNew(List<Long> listId) throws Exception;

	/**
	 * 根据文控信息主键ID获取从表信息
	 * 
	 * @param historyId
	 *            文控主键ID
	 * @return 文控信息对应的各阶段审批人信息
	 * @throws Exception
	 */
	public List<JecnTaskHistoryFollow> getJecnTaskHistoryFollowList(Long historyId) throws Exception;

	public JecnTaskHistoryNew getLatestHistory(Long flowId) throws Exception;

	public JecnTaskHistoryNew getLatestHistory(Long id, int type) throws Exception;

	public List<String> findVersionListByFlowIdNoPage(Long id, int type);

}
