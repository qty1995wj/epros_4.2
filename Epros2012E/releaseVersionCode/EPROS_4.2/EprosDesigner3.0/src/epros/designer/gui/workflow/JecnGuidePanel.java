package epros.designer.gui.workflow;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.flowmap.FlowMapArchitectureCardDialog;
import epros.designer.gui.process.guide.KPIShowDialog;
import epros.designer.gui.process.guide.OverviewInformationDialog;
import epros.designer.gui.process.guide.ProcessAndSystemDialog;
import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.process.guide.ProcessPropertyDialog;
import epros.designer.gui.rule.RuleDoControlPanel;
import epros.designer.service.process.JecnProcess;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程设计向导
 * 
 * @author ZHOUXY
 * 
 */
public class JecnGuidePanel extends JPanel {
	private final static Logger log = Logger.getLogger(JecnGuidePanel.class);
	/** 概况信息 */
	private JButton baseInfoBtn = null;
	/** 流程图 */
	private JButton drawFlowMapBtn = null;
	/** 流程文件 */
	private JButton flowProcessDocBtn = null;
	/** 相关流程及制度 */
	private JButton linkFlowAndInstBtn = null;
	/** 流程属性 */
	private JButton flowAttrBtn = null;
	/** 流程文控信息 */
	private JButton flowControlBtn = null;
	/** 流程KPI */
	private JButton flowKPIBtn = null;

	/** 架构图 */
	private JButton keyboardMapBtn = null;
	/** 流程架构 架构卡 */
	private JButton architectureCardBtn = null;
	/** 流程架构 集成关系图 */
	private JButton integratedRelationGraphBtn = null;
	private JPanel mainPanel = null;
	private MapType mapType;

	public JecnGuidePanel(MapType mapType) {
		this.mapType = mapType;
		initComponent();
	}

	private void initComponent() {
		// 概况信息
		baseInfoBtn = new JButton(JecnProperties.getValue("baseInfoBtn"));
		// 流程图
		drawFlowMapBtn = new JButton(JecnProperties.getValue("drawFlowMapBtn"));
		// 流程文件
		flowProcessDocBtn = new JButton(JecnProperties.getValue("flowProcessDocBtn"));
		// 相关流程及制度
		linkFlowAndInstBtn = new JButton(JecnProperties.getValue("linkFlowAndInstBtn"));
		// 流程属性
		flowAttrBtn = new JButton(JecnProperties.getValue("flowAttrBtn"));
		// 流程文控信息
		flowControlBtn = new JButton(JecnProperties.getValue("flowControlBtn"));
		// 流程KPI
		flowKPIBtn = new JButton(JecnProperties.getValue("flowKPIBtn"));

		// 键盘图
		keyboardMapBtn = new JButton(JecnProperties.getValue("keyboardMap"));
		// 架构卡
		architectureCardBtn = new JButton(JecnProperties.getValue("cardStructure"));
		// 集成关系图
		integratedRelationGraphBtn = new JButton(JecnProperties.getValue("integratedRelationGraph"));
		// 概况信息
		baseInfoBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Long flowId = JecnDesignerCommon.getWorkFlowId();
				if (flowId == null || flowId == -1L) {
					return;
				}

				OverviewInformationDialog overviewInformationDialog = new OverviewInformationDialog(flowId,
						JecnTreeCommon.getWorkNode());
				overviewInformationDialog.setVisible(true);
			}
		});
		// 流程文件
		flowProcessDocBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Long flowId = JecnDesignerCommon.getWorkFlowId();
				if (flowId == null || flowId == -1L) {
					return;
				}
				ProcessOperationDialog designerProcessOperationIntrusDialog = new ProcessOperationDialog(flowId,
						JecnTreeCommon.getWorkNode());

				// DesignerProcessOperationIntrusDialog
				// designerProcessOperationIntrusDialog = new
				// DesignerProcessOperationIntrusDialog(
				// flowId, JecnTreeCommon.getWorkNode());

				designerProcessOperationIntrusDialog.setVisible(true);
			}
		});
		// 相关流程及制度
		linkFlowAndInstBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Long flowId = JecnDesignerCommon.getWorkFlowId();
				if (flowId == null || flowId == -1L) {
					return;
				}
				ProcessAndSystemDialog processAndSystemDialog = new ProcessAndSystemDialog(flowId);
				processAndSystemDialog.setVisible(true);
			}
		});
		// 流程文控信息
		flowControlBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Long flowId = JecnDesignerCommon.getWorkFlowId();
				if (flowId == null || flowId == -1L) {
					return;
				}
				RuleDoControlPanel ruleDoControlPanel = new RuleDoControlPanel(JecnTreeCommon.getWorkNode(), flowId, 0);
				ruleDoControlPanel.setVisible(true);
			}
		});

		// 流程属性
		flowAttrBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ProcessPropertyDialog processPropertyDialog = new ProcessPropertyDialog(JecnTreeCommon.getWorkNode());
				processPropertyDialog.setVisible(true);
			}
		});
		// 流程KPI
		flowKPIBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Long flowId = JecnDesignerCommon.getWorkFlowId();
				if (flowId == null || flowId == -1L) {
					return;
				}
				KPIShowDialog kPIShowDialog = new KPIShowDialog(flowId);
				kPIShowDialog.setVisible(true);
			}
		});

		keyboardMapBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				keyboardMapAction();
			}
		});
		architectureCardBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				architectureCardAction();
			}
		});
		integratedRelationGraphBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				integratedRelationGraphAction();
			}
		});
		initDefault();
	}

	private JPanel getPanel() {
		return new JecnGuideArrawPanel();
	}

	private void initComponentState() {
		mainPanel.setBorder(BorderFactory.createTitledBorder(JecnUIUtil.getTootBarBorder(), JecnProperties
				.getValue("showFlowElemGuied")));
		Dimension size = new Dimension(10, 20);
		baseInfoBtn.setPreferredSize(size);
		drawFlowMapBtn.setPreferredSize(size);
		flowProcessDocBtn.setPreferredSize(size);
		linkFlowAndInstBtn.setPreferredSize(size);
		flowAttrBtn.setPreferredSize(size);
		flowControlBtn.setPreferredSize(size);
		flowKPIBtn.setPreferredSize(size);

		/** 概况信息 */
		baseInfoBtn.setEnabled(false);
		/** 流程图 */
		drawFlowMapBtn.setEnabled(false);
		/** 流程文件 */
		flowProcessDocBtn.setEnabled(false);
		/** 相关流程及制度 */
		linkFlowAndInstBtn.setEnabled(false);
		/** 流程属性 */
		flowAttrBtn.setEnabled(false);
		/** 流程文控信息 */
		flowControlBtn.setEnabled(false);
		/** 流程KPI */
		flowKPIBtn.setEnabled(false);
	}

	private void allEnabledFalse() {
		// 概况信息
		baseInfoBtn.setEnabled(false);
		// 流程图
		drawFlowMapBtn.setEnabled(false);
		// 流程文件
		flowProcessDocBtn.setEnabled(false);
		// 相关流程及制度
		linkFlowAndInstBtn.setEnabled(false);
		// 流程属性
		flowAttrBtn.setEnabled(false);
		// 流程文控信息
		flowControlBtn.setEnabled(false);
		// 流程KPI
		flowKPIBtn.setEnabled(false);
	}

	/**
	 * 架构图
	 */
	private void keyboardMapAction() {
		Long flowId = JecnDesignerCommon.getWorkFlowId();
		if (flowId == null || flowId == -1L) {
			return;
		}
		JecnProcess.isOpen(flowId.intValue(), MapType.totalMap);
	}

	/***
	 * 流程架构 架构卡
	 */
	private void architectureCardAction() {
		Long flowId = JecnDesignerCommon.getWorkFlowId();
		if (flowId == null || flowId == -1L) {
			return;
		}
		// 流程地图描述
		FlowMapArchitectureCardDialog flowMapDesDialog = new FlowMapArchitectureCardDialog(flowId);
		flowMapDesDialog.setVisible(true);
	}

	/**
	 * 流程架构 集成关系图
	 */
	private void integratedRelationGraphAction() {
		Long flowId = JecnDesignerCommon.getWorkFlowId();
		if (flowId == null || flowId == -1L) {
			return;
		}
		try {
			JecnDesignerCommon.openMapRelatedProcess(flowId, TreeNodeType.processMapRelation);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("JecnGuidePanel integratedRelationGraphAction is error！ ", e);
		}
	}

	/**
	 * 初始流程
	 */
	private void initFlow() {
		initDefault();
		JecnFlowMapData flowMapData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();
		if (flowMapData != null && flowMapData.getDesignerData().getModeType() == ModeType.none) { // 不是流程架构和流程模板
			/** 概况信息 */
			baseInfoBtn.setEnabled(true);
			/** 流程文件 */
			flowProcessDocBtn.setEnabled(true);
			/** 相关流程及制度 */
			linkFlowAndInstBtn.setEnabled(true);
			/** 流程属性 */
			flowAttrBtn.setEnabled(true);
			/** 流程文控信息 */
			flowControlBtn.setEnabled(true);
			/** 流程KPI */
			flowKPIBtn.setEnabled(true);
		}
	}

	/**
	 * 其他
	 */
	private void initDefault() {
		getMainPanel();
		initComponentState();
		initLayOutFlow();
	}

	private void getMainPanel() {
		if (this.mainPanel != null) {
			this.remove(mainPanel);
			this.repaint();
			this.validate();
			this.updateUI();
			mainPanel = null;
		}
		mainPanel = new JPanel();
		this.setLayout(new BorderLayout(0, 0));
		this.add(mainPanel);
		// 透明
		mainPanel.setOpaque(false);
		// 无边框
		mainPanel.setBorder(null);
		mainPanel.setLayout(new GridBagLayout());
	}

	/**
	 * 架构图
	 */
	public void initFlowMap() {
		getMainPanel();
		this.initComponentFlowMap();
		this.initLayOutFlowMap();
		JecnFlowMapData flowMapData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();
		if (flowMapData != null && flowMapData.getDesignerData().getModeType() == ModeType.none) { // 不是流程架构和流程模板
			// 流程架构 架构卡
			this.architectureCardBtn.setEnabled(true);
			// 流程架构 集成关系图
			this.integratedRelationGraphBtn.setEnabled(true);
		}
	}

	/**
	 * 流程架构 集成关系图
	 */
	public void initFlowMapRelation() {
		getMainPanel();
		this.initComponentFlowMap();
		this.initLayOutFlowMap();
		JecnFlowMapData flowMapData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData();
		if (flowMapData != null && flowMapData.getDesignerData().getModeType() == ModeType.none) { // 不是流程架构和流程模板
			// 架构图
			this.keyboardMapBtn.setEnabled(false);
			// 流程架构 架构卡
			this.architectureCardBtn.setEnabled(true);
		}
	}

	/**
	 * 
	 * 是否能操作流程设计向导
	 * 
	 * @param mapType
	 * 
	 */
	public void isEditFlowDesignGuide(MapType mapType) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		// 模板情况 全部置灰
		if (!desktopPane.getFlowMapData().getDesignerData().getModeType().equals(ModeType.none)) {
			initDefault();
		}
		if (this.mapType == mapType) {
			if (mapType == MapType.totalMap) {
				// 键盘图
				keyboardMapBtn.setEnabled(false);
				// 架构卡
				architectureCardBtn.setEnabled(true);
				// 集成关系图
				integratedRelationGraphBtn.setEnabled(true);
			} else if (mapType == MapType.partMap) {
				/** 概况信息 */
				baseInfoBtn.setEnabled(true);
				// 流程图
				drawFlowMapBtn.setEnabled(false);
				/** 流程文件 */
				flowProcessDocBtn.setEnabled(true);
				/** 相关流程及制度 */
				linkFlowAndInstBtn.setEnabled(true);
				/** 流程属性 */
				flowAttrBtn.setEnabled(true);
				/** 流程文控信息 */
				flowControlBtn.setEnabled(true);
				/** 流程KPI */
				flowKPIBtn.setEnabled(true);
			} else if (mapType == MapType.totalMapRelation) {
				// 键盘图
				keyboardMapBtn.setEnabled(false);
				// 架构卡
				architectureCardBtn.setEnabled(true);
				// 集成关系图
				integratedRelationGraphBtn.setEnabled(false);
			}
			return;
		} else if ((mapType == MapType.totalMap && this.mapType == MapType.totalMapRelation)
				|| (mapType == MapType.totalMapRelation && this.mapType == MapType.totalMap)) {
			if (mapType == MapType.totalMap) {
				// 键盘图
				keyboardMapBtn.setEnabled(false);
				// 架构卡
				architectureCardBtn.setEnabled(true);
				// 集成关系图
				integratedRelationGraphBtn.setEnabled(true);
			} else if (mapType == MapType.totalMapRelation) {
				// 键盘图
				keyboardMapBtn.setEnabled(false);
				// 架构卡
				architectureCardBtn.setEnabled(true);
				// 集成关系图
				integratedRelationGraphBtn.setEnabled(false);
			}
			this.mapType = mapType;
		} else {
			this.mapType = mapType;
			switch (mapType) {
			case partMap:
				initFlow();
				break;
			case totalMap:
				initFlowMap();
				break;
			case totalMapRelation:
				initFlowMapRelation();
				break;
			default:
				initDefault();
				break;
			}
		}

	}

	/**
	 * 流程架构
	 */
	private void initComponentFlowMap() {
		mainPanel.setBorder(BorderFactory.createTitledBorder(JecnUIUtil.getTootBarBorder(), JecnProperties.getValue("showFlowMapElemGuied")));
		Dimension size = new Dimension(10, 20);
		keyboardMapBtn.setPreferredSize(size);
		architectureCardBtn.setPreferredSize(size);
		integratedRelationGraphBtn.setPreferredSize(size);
		// 键盘图
		keyboardMapBtn.setEnabled(false);
		// 架构卡
		architectureCardBtn.setEnabled(false);
		// 集成关系图
		integratedRelationGraphBtn.setEnabled(false);
	}

	/**
	 * 流程架构布局
	 */
	private void initLayOutFlowMap() {
		Insets insets = new Insets(0, 20, 0, 20);
		Insets topSets = new Insets(10, 20, 0, 20);
		Insets bottomsets = new Insets(0, 20, 10, 20);
		// 键盘图
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, topSets, 0, 0);
		mainPanel.add(this.keyboardMapBtn, c);
		// 箭头
		JPanel panel = getPanel();
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(panel, c);
		// 架构卡
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(this.architectureCardBtn, c);
		// 箭头
		panel = getPanel();
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(panel, c);
		// 集成关系图
		c = new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				bottomsets, 0, 0);
		mainPanel.add(integratedRelationGraphBtn, c);
	}

	private void initLayOutFlow() {
		Insets insets = new Insets(0, 20, 0, 20);
		Insets topSets = new Insets(10, 20, 0, 20);
		Insets bottomsets = new Insets(0, 20, 10, 20);

		// 流程图
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, topSets, 0, 0);

		mainPanel.add(drawFlowMapBtn, c);
		// 箭头
		JPanel panel = getPanel();
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(panel, c);
		// 流程文件
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(flowProcessDocBtn, c);

		// 箭头
		panel = getPanel();
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		mainPanel.add(panel, c);

		// 流程属性
		c = new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				bottomsets, 0, 0);
		mainPanel.add(flowAttrBtn, c);
	}
}
