package com.jecn.epros.server.dao.integration.impl;

import com.jecn.epros.server.bean.integration.JecnControlPoint;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.integration.IJecnControlPointDao;

/***
 * 风险控制点表操作类
 * 2013-10-30
 *
 */
public class JecnControlPointDaoImpl extends AbsBaseDao<JecnControlPoint, String> implements IJecnControlPointDao {

}
