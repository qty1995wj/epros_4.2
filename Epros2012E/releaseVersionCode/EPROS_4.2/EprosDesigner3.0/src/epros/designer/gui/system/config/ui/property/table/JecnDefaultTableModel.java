package epros.designer.gui.system.config.ui.property.table;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

/**
 * 
 * 表的模型类
 * 
 * @author ZHOUXY
 *
 */
public class JecnDefaultTableModel extends DefaultTableModel {
	/** 列名称集合 */
	private String[] jecnColumnNames = null;

	public JecnDefaultTableModel(String[] jecnColumnNames) {
		super();

		// 替换模型中的列标识符
		setColumnNames(jecnColumnNames);
	}

	/**
	 * 
	 * 是否编辑
	 * 
	 */
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	/**
	 * 
	 * 替换模型中的列标识符
	 * 
	 * @param jecnColumnNames
	 */
	public void setColumnNames(String[] jecnColumnNames) {
		if (jecnColumnNames == null || jecnColumnNames.length == 0) {
			columnIdentifiers = new Vector();
		}
		this.jecnColumnNames = jecnColumnNames;

		columnIdentifiers = convertToVector(jecnColumnNames);

		// // 通知所有侦听器，表的结构已更改
		fireTableStructureChanged();
	}

	/**
	 * 
	 * 设置表数据
	 * 
	 * @param data
	 */
	public void setData(Vector data) {
		if (data == null) {
			dataVector = new Vector(0);
		} else {
			dataVector = data;
		}
		fireTableDataChanged();

	}

	/**
	 * 
	 * 设置表数据
	 * 
	 * @param data
	 */
	public void setData(Object[][] data) {
		setData(convertToVector(data));
	}

	public String[] getJecnColumnNames() {
		return jecnColumnNames;
	}

	public void setJecnColumnNames(String[] jecnColumnNames) {
		this.jecnColumnNames = jecnColumnNames;
	}

}
