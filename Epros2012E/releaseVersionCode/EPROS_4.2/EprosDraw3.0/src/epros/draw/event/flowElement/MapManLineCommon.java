package epros.draw.event.flowElement;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.List;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.ReSizeLineType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.event.JecnTabbedEventProcess;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.JecnEditPortPanel;
import epros.draw.gui.figure.unit.SnapToGrid;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.resize.JecnManhattanLineResizePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;

public enum MapManLineCommon {
	INSTANCE;

	/**
	 * 流程架构图，拖动线段端点处理
	 * 
	 * @param lineResizePanel
	 * @param cloneLinePanel
	 */
	public void setMapManLinePort(JecnManhattanLineResizePanel lineResizePanel,
			JecnBaseManhattanLinePanel cloneLinePanel) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (lineResizePanel == null || !JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure()) {
			return;
		}
		// 流程架构图，添加连接线 ,设置拖动的端点
		if (lineResizePanel.getReSizeLineType() == ReSizeLineType.start) {// 拖动开始点
			if (isMapLineFigure(cloneLinePanel.getStartFigure())) {
				desktopPane.setTempEditPort(getEditPortPanel(cloneLinePanel.getStartEditPointType(), cloneLinePanel
						.getStartFigure()));
			}
		} else if (lineResizePanel.getReSizeLineType() == ReSizeLineType.end) {
			if (isMapLineFigure(cloneLinePanel.getEndFigure())) {
				desktopPane.setTempEditPort(getEditPortPanel(cloneLinePanel.getEndEditPointType(), cloneLinePanel
						.getEndFigure()));
			}
		}
	}

	/**
	 * 拖动流程架构图连接线端点
	 * 
	 * @param ePoint
	 */
	public void mouseResizeReleaseMapLine(Point ePoint) {
		if (!JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure()) {
			return;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane.getTempEditPort() != null) {
			JecnBaseFigurePanel baseFigurePanel = desktopPane.getTempEditPort().getBaseFigurePanel();
			if (isMapLineFigure(baseFigurePanel)) {
				baseFigurePanel.setSizeAndLocation(ePoint);
			}
		}
	}

	private boolean isMapLineFigure(JecnBaseFigurePanel baseFigurePanel) {
		return baseFigurePanel.getFlowElementData().getMapElemType() == MapElemType.MapLineFigure;
	}

	/**
	 * 流程架构 画流程图
	 * 
	 * 获取图连接线图形编辑点（目前拖动 不存在真实的开始图形或结束图形的连接线存在此情况）
	 * 
	 * @param editPointType
	 * @param baseFigurePanel
	 * @return
	 */
	public JecnEditPortPanel getEditPortPanel(EditPointType editPointType, JecnBaseFigurePanel baseFigurePanel) {
		JecnEditPortPanel editPortPanel = null;
		switch (editPointType) {
		case left:
			editPortPanel = baseFigurePanel.getEditLeftPortPanel();
			break;
		case right:
			editPortPanel = baseFigurePanel.getEditRightPortPanel();
			break;
		case top:
			editPortPanel = baseFigurePanel.getEditTopPortPanel();
			break;
		case bottom:
			editPortPanel = baseFigurePanel.getEditBottomPortPanel();
			break;
		}
		return editPortPanel;
	}

	/**
	 * 根据结束点和开始点，判断开始点画线方向
	 * 
	 * @param ePoint
	 */
	private void setStartPointTypeByEndPoint(Point ePoint) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		Point startPoint = desktopPane.getStartPoint();
		if (startPoint == null) {
			return;
		}

		int startX = startPoint.x;
		int startY = startPoint.y;

		int eX = Math.abs(ePoint.x);
		int eY = Math.abs(ePoint.y);

		EditPointType xPointType = null;
		EditPointType yPointType = null;

		if (eX >= startX) {// 鼠标X距离右侧近
			xPointType = EditPointType.right;
		} else {
			xPointType = EditPointType.left;
		}

		if (eY >= startY) {// 鼠标Y距离底部近
			yPointType = EditPointType.bottom;
		} else {
			yPointType = EditPointType.top;
		}

		if (desktopPane.getStartPointType() == null) {// 没次画线，需设置开始点方向
			// 距离哪个方向最近， 就是哪个方向
			if (Math.abs(eX - startX) >= Math.abs(eY - startY)) {
				desktopPane.setStartPointType(xPointType);
			} else {
				desktopPane.setStartPointType(yPointType);
			}
		} else {// 开始点方向定位后，如果原来为X轴方向，那么修改后也必须为X轴反方向，Y轴同理
			if (desktopPane.getStartPointType() == EditPointType.left
					|| desktopPane.getStartPointType() == EditPointType.right) {// X方向位置
				desktopPane.setStartPointType(xPointType);
			} else {
				desktopPane.setStartPointType(yPointType);
			}
		}
	}

	/********************* 【流程地图画线start】 *********************************/
	private void drawLineForFlowMap(MouseEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane.getTempLine() == null) {// 不存在连接线临时线段且可连接情况下
			// 创建连接线临时线段对象
			JecnEditPortEventProcess.creatTempLine(desktopPane);
			desktopPane.getTempLine().setArrowStartPoint(desktopPane.getStartPoint());
			desktopPane.getTempLine().setArrowEndPoint(e.getPoint());
		} else if (desktopPane.getTempLine() != null) {// 已存在临时线段
			Point tempEndPoint = SnapToGrid.checkIsGrid(e.getX(), e.getY());
			// 设置线段开始点 ， 默认为原始大小
			desktopPane.getTempLine().setArrowEndPoint(tempEndPoint);
			// 拖动连接线结束点面板横竖滚动条跟随移动
			JecnEditPortEventProcess.draggedScrollPanel(tempEndPoint);

			drawTmpLine();
		}
	}

	private void drawTmpLine() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 根据Point相对位置获取结束端点位置
		EditPointType endType = getEndPortType(desktopPane.getStartPointType());
		// 设置数据层结束点
		desktopPane.getTempLine().getFlowElementData().setEndEditPointType(endType);
		// 清空临时线段
		desktopPane.getTempLine().disposeLines();
		// 拖动时生成虚拟连接线线段
		desktopPane.getTempLine().paintDraggedLinePort(desktopPane.getStartPoint(), desktopPane.getStartPointType(),
				desktopPane.getTempLine().getArrowEndPoint(), endType);

		desktopPane.getWorkflowZoomProcess().zoomManhattanLine(desktopPane.getTempLine());
	}

	public EditPointType getEndPortType(EditPointType startType) {
		if (startType == null) {
			throw new IllegalArgumentException("流程架构图，添加连接线参数非法！");
		}
		EditPointType endPointType = null;
		// 1、开始点为水平方向，结束点也为水平方向
		if (startType == EditPointType.left) {
			endPointType = EditPointType.right;
		} else if (startType == EditPointType.right) {
			endPointType = EditPointType.left;
		}
		// 2、开始为垂直方向，结束点也为垂直方向
		else if (startType == EditPointType.top) {
			endPointType = EditPointType.bottom;
		} else if (startType == EditPointType.bottom) {
			endPointType = EditPointType.top;
		}
		return endPointType;

	}

	public boolean addManLine(MouseEvent e) {
		// 是在拖动状态为false
		JecnManLineResizeProcess.setDragged(false);

		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		JecnBaseManhattanLinePanel linePanel = JecnEditPortEventProcess.getJecnBaseManhattanLinePanel();
		if (linePanel == null || desktopPane.getTempLine() == null) {
			return false;
		}

		JecnBaseFigurePanel startFigure = craeteFigurePanel(desktopPane.getStartPoint());
		JecnBaseFigurePanel endFigure = craeteFigurePanel(e.getPoint());

		// 判断开始点和结束点距离 10像素以内不生成线段
		if (!lineGridValidate(startFigure.getLocation(), endFigure.getLocation())) {
			clearTempLine();
			return false;
		}
		// 设置连接线开始图形和结束图形
		linePanel.setStartFigure(startFigure);
		linePanel.setEndFigure(endFigure);
		// 设置连接线对应图形端口的位置
		linePanel.getFlowElementData().setStartEditPointType(desktopPane.getStartPointType());
		linePanel.getFlowElementData().setEndEditPointType(getEndPortType(desktopPane.getStartPointType()));
		// 添加连接线到画图面板
		JecnAddFlowElementUnit.addMapManhattinLine(linePanel);

		// 获取待添加的连接线开始图形和结束图形主键ID和UUID
		JecnAddFlowElementUnit.setLineDataByFigure(linePanel);
		// *******************撤销恢复*******************//
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		// 记录撤销恢复数据
		redoData.recodeFlowElement(linePanel);
		JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createAdd(redoData);
		desktopPane.getUndoRedoManager().addEdit(undoProcess);
		// 清空临时线段
		clearTempLine();
		return true;
	}

	private void clearTempLine() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 清空临时线段
		desktopPane.getTempLine().disposeLines();
		// 清空拖动连接线生成的临时编辑点
		desktopPane.setTempEditPort(null);
		desktopPane.setTempLine(null);

		desktopPane.setStartPointType(null);
		// 添加完成后，选中指针
		JecnTabbedEventProcess.cancleAddFigure();
	}

	private boolean lineGridValidate(Point startPoint, Point endPoint) {
		if (Math.abs(startPoint.x - endPoint.x) < 20 && Math.abs(startPoint.y - endPoint.y) < 20) {
			return false;
		}
		return true;
	}

	public JecnBaseFigurePanel craeteFigurePanel(Point point) {
		JecnFigureData figureData = JecnCreateFlowElement.createFigureData(MapElemType.MapLineFigure);
		figureData.setFlowElementUUID(DrawCommon.getUUID());
		JecnBaseFigurePanel figurePabel = (JecnBaseFigurePanel) JecnCreateFlowElement.getFlowFigure(
				MapElemType.MapLineFigure, figureData);
		// 设置图形大小和位置点
		figurePabel.setSizeAndLocation(point);

		JecnDrawMainPanel.getMainPanel().getWorkflow().addJecnPanel(figurePabel);
		return figurePabel;
	}

	public JecnFigureData createFigureData() {
		JecnFigureData figureData = new JecnFigureData(MapElemType.FreeText);
		figureData.setMapElemType(MapElemType.MapLineFigure);
		figureData.setFigureSizeX(0);
		figureData.setFigureSizeY(0);
		figureData.setFlowElementUUID(DrawCommon.getUUID());
		figureData.setFigureText(MapElemType.MapLineFigure.toString());
		return figureData;
	}

	/**
	 * 画图面板鼠标拖拽
	 * 
	 * @param e
	 */
	public void deskTopMouseDragged(MouseEvent e) {
		if (JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure()) {// 流程架构图，允许画连接线
			setStartPointTypeByEndPoint(e.getPoint());
			// 流程地图画线
			drawLineForFlowMap(e);
			return;
		}
	}

	/**
	 * 获取 最小距离的坐标值
	 * 
	 * @param ePoint
	 * @return
	 */
	public Point getDistancePoint(JecnBaseFigurePanel figurePanel) {
		if (!isArrowheadPanel(figurePanel.getFlowElementData().getMapElemType())) {
			return null;
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		List<JecnBaseFlowElementPanel> figurePanels = desktopPane.getPanelList();

		// 记录到 ePoint的最小坐标值
		int minDistanceX = 10, minDistanceY = 10;

		double angle = figurePanel.getFlowElementData().getCurrentAngle();
		// 垂直方向 返入返出
		boolean isVertical = isVertical(angle);

		Point ePoint = figurePanel.getLocation();

		// 返回结果坐标
		int resultX = ePoint.x, resultY = ePoint.y;
		for (JecnBaseFlowElementPanel baseFigurePanel : figurePanels) {
			if (!(baseFigurePanel instanceof JecnBaseFigurePanel)
					|| isArrowheadPanel(baseFigurePanel.getFlowElementData().getMapElemType())) {
				continue;
			}
			if (baseFigurePanel instanceof JecnBaseFigurePanel) {
				if (isVertical) {// 垂直方向，Y坐标变动X不变
					if (baseFigurePanel.getFlowElementData().getMapElemType() == MapElemType.RightArrowhead) {// 返出
						if (angle == 180.0) {// 线段朝下
							int distanceY = Math.abs(ePoint.y - baseFigurePanel.getY());
							if (distanceY <= minDistanceY) {
								minDistanceY = distanceY;
								resultY = baseFigurePanel.getY() - figurePanel.getHeight();
							}
						} else {// 线段朝上
							int distanceHY = Math.abs(ePoint.y - baseFigurePanel.getY() - baseFigurePanel.getHeight());
							if (distanceHY <= minDistanceY) {
								minDistanceY = distanceHY;
								resultY = baseFigurePanel.getY() + baseFigurePanel.getHeight();
							}
						}
					} else {// 返入

					}
				} else {// 水平方向
					if (angle == 270.0) {// 线段朝右
						int distanceX = Math.abs(ePoint.x - baseFigurePanel.getX());
						if (distanceX <= minDistanceX) {
							minDistanceX = distanceX;
							resultX = baseFigurePanel.getX() - figurePanel.getWidth();
						}
					} else {// 线段朝左
						int distanceWX = Math.abs(ePoint.x - baseFigurePanel.getX() - baseFigurePanel.getWidth());
						if (distanceWX <= minDistanceX) {
							minDistanceX = distanceWX;
							resultX = baseFigurePanel.getX();
						}
					}
				}

				int distanceX = Math.abs(ePoint.x - baseFigurePanel.getX());
				int distanceY = Math.abs(ePoint.y - baseFigurePanel.getY());

				int distanceWX = Math.abs(ePoint.x - baseFigurePanel.getX() - baseFigurePanel.getWidth());
				int distanceHY = Math.abs(ePoint.y - baseFigurePanel.getY() - baseFigurePanel.getHeight());

				if (distanceX < minDistanceX) {
					minDistanceX = distanceX;
					resultX = baseFigurePanel.getX();
				}
				if (distanceY < minDistanceY) {
					minDistanceY = distanceY;
					resultY = baseFigurePanel.getY();
				}
				if (distanceWX < minDistanceX) {
					minDistanceX = distanceWX;
					resultX = baseFigurePanel.getX() + baseFigurePanel.getWidth();
				}
				if (distanceHY < minDistanceY) {
					minDistanceY = distanceHY;
					resultY = baseFigurePanel.getY() + baseFigurePanel.getHeight();
				}
			}
		}
		return new Point(resultX, resultY);
	}

	/**
	 * 根据角度判断 方向
	 * 
	 * @param angle
	 * @return
	 */
	private boolean isVertical(double angle) {
		return angle == 180.0 || angle == 0.0 || angle == 360.0;
	}

	/********************* 【流程地图画线end】 *********************************/

	private boolean isArrowheadPanel(MapElemType mapElemType) {
		return mapElemType == MapElemType.RightArrowhead || mapElemType == MapElemType.ReverseArrowhead
				|| mapElemType == MapElemType.MapLineFigure;
	}

	public EditPointType getEditPortByFigures(Point mouseP) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		List<JecnBaseFlowElementPanel> panelList = desktopPane.getPanelList();
		EditPointType editPointType = null;
		for (JecnBaseFlowElementPanel elementPanel : panelList) {
			if (elementPanel instanceof JecnBaseFigurePanel) {
				if (!elementPanel.getFlowElementData().getEditPoint()) {// 是否显示图形编辑点
					continue;
				}

				// 判定当前鼠标相对元素的方向，上下左右；
				editPointType = getEditPointType(elementPanel, mouseP);
				if (editPointType != null) {
					return editPointType;
				}
			}
		}
		return editPointType;
	}

	private EditPointType getEditPointType(JecnBaseFlowElementPanel elementPanel, Point mouseP) {
		int INSTANCE = 5;
		int localX = elementPanel.getX();
		int localY = elementPanel.getY();

		int widthX = localX + elementPanel.getWidth();
		int heightY = localY + elementPanel.getHeight();

		int pX = mouseP.x;
		int pY = mouseP.y;

		EditPointType editPointType = null;
		if ((localX <= pX && pX <= widthX) && Math.abs(localY - pY) <= INSTANCE) {// 上
			editPointType = EditPointType.top;
		} else if ((localY <= pY && pY <= heightY) && Math.abs(localX - pX) <= INSTANCE) {// 左
			editPointType = EditPointType.left;
		} else if ((localX <= pX && pX <= widthX) && Math.abs(heightY - pY) <= INSTANCE) {// 下
			editPointType = EditPointType.bottom;
		} else if ((localY <= pY && pY <= heightY) && Math.abs(widthX - pX) <= INSTANCE) {// 右
			editPointType = EditPointType.right;
		}
		return editPointType;
	}

}
