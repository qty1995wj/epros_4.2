package epros.designer.gui.process.guide.guideTable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowKpi;

import epros.designer.util.ConnectionPool;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程KPI数据显示
 * 
 * @author 2012-07-09
 * 
 */
public class ProcessKPIValueTable extends JTable {
	private static Logger log = Logger.getLogger(ProcessKPIValueTable.class);
	private String verName = null;
	private String honName = null;
	private Long flowKpiId = null;
	private List<JecnFlowKpi> kpiValueList = new ArrayList<JecnFlowKpi>();

	public ProcessKPIValueTable(List<JecnFlowKpi> kpiValueList, String honName,
			String verName) {
		this.honName = honName;
		this.verName = verName;
		this.kpiValueList = kpiValueList;
		this.setModel(getTableListModel());
		setTitleModel();
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
	}

	public ProcessKPIValueTable(String verName, String honName, Long flowKpiId) {
		this.verName = verName;
		this.honName = honName;
		this.flowKpiId = flowKpiId;
		this.setModel(getTableModel());
		setTitleModel();
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
	}

	public void setTitleModel() {
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
	}

	public Vector<Vector<String>> getListContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		for (JecnFlowKpi flowKpiValue : kpiValueList) {
			vs.add(getRow(flowKpiValue));
		}
		return vs;
	}

	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		if (flowKpiId != null) {
			if (verName != null || !"".equals(verName) || honName != null
					|| !"".equals(honName)) {
				// 获取流程KPI
				try {
					kpiValueList = ConnectionPool.getProcessAction()
							.getJecnFlowKpiListByKPIId(flowKpiId);
				} catch (Exception e1) {
					log.error("ProcessKPIValueTable getContent is error", e1);
				}
				// 获取流程KPI值数据
				try {
					for (JecnFlowKpi flowKpiValue : kpiValueList) {
						vs.add(getRow(flowKpiValue));
					}
				} catch (Exception e) {
					log.error("ProcessKPIValueTable getContent is error", e);
				}
			}
		}
		return vs;
	}

	public Vector<String> getRow(JecnFlowKpi flowKpiValue) {
		Vector<String> row = new Vector<String>();
		if (flowKpiValue.getKpiId() != null) {
			// 主键ID
			row.add(flowKpiValue.getKpiId().toString());
		} else {
			row.add("");
		}
		// 关联ID
		if (flowKpiValue.getKpiAndId() != null) {
			row.add(flowKpiValue.getKpiAndId().toString());
		} else {
			row.add("");
		}
		// 纵向值 String
		row.add(flowKpiValue.getKpiValue());
		// 横向值 Date
		SimpleDateFormat strFormat = null;
		String strHorValue = null;
		Date spHorValue = flowKpiValue.getKpiHorVlaue();

		if (honName.equals("周")) {
			if (spHorValue != null) {
				strFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				strHorValue = strFormat.format(spHorValue);
				strHorValue = strHorValue.split("-")[0] + "年"
						+ strHorValue.split(":")[1] + "周";
			}
		} else {
			strFormat = new SimpleDateFormat("yyyy-MM-dd");
			if (spHorValue != null) {
				strHorValue = strFormat.format(spHorValue);
			}
			if (honName.equals("年")) {
				strHorValue = strHorValue.split("-")[0];
			} else if (honName.equals("周")) {
				strHorValue = strHorValue.split("-")[0]
						+ strHorValue.split("-")[1];
			} else if (honName.equals("月")) {
				strHorValue = strHorValue.split("-")[0] + "-"
						+ strHorValue.split("-")[1];
			} else if (honName.equals("季度")) {
				if (strHorValue.split("-")[1].equals("01")) {
					strHorValue = strHorValue.split("-")[0] + "一季度";
				} else if (strHorValue.split("-")[1].equals("04")) {
					strHorValue = strHorValue.split("-")[0] + "二季度";
				} else if (strHorValue.split("-")[1].equals("07")) {
					strHorValue = strHorValue.split("-")[0] + "三季度";
				} else if (strHorValue.split("-")[1].equals("10")) {
					strHorValue = strHorValue.split("-")[0] + "四季度";
				}
			}
			// else if (honName.equals("天")) {
			// strHorValue = strHorValue;
			// }
		}
		row.add(strHorValue);

		return row;
	}

	public ProcessKPIValueTableMode getTableListModel() {
		return new ProcessKPIValueTableMode(getListContent(), getTtitle());
	}

	public ProcessKPIValueTableMode getTableModel() {
		return new ProcessKPIValueTableMode(getContent(), getTtitle());
	}

	public Vector<String> getTtitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add("关联ID");
		title.add("KPI纵向值" + verName);
		title.add("KPI横向值" + honName);
		return title;
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0, 1 };
	}

	class ProcessKPIValueTableMode extends DefaultTableModel {
		public ProcessKPIValueTableMode(Vector<Vector<String>> data,
				Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}
}
