function processListGrid(json, isPop) {
	processListGridStore = new Ext.data.JsonStore( {
		autoLoad : false,
		data : json.dataList,
		fields : json.stores
	});

	processListGridColumnModel = new Ext.grid.ColumnModel( {
		defaults : {
			align : 'center',
			width : 120,
			sortable : true,
			menuDisabled : false,
			flex :50
		},
		columns : json.columns
	});
	if (isPop == true || isPop == "true") {
		processListHeight = document.documentElement.clientHeight - 85
	} else {
		processListHeight = document.documentElement.clientHeight - 150
	}
	processListGridPanel = new Ext.grid.GridPanel( {
		id : "processListGridPanel_id",
		renderTo : 'processListShow_id',
		height : processListHeight,
		autoScroll : true,//滚动条
		stripeRows : true,//显示行的分隔符
		columnLines : true,//显示列的分隔线
		enableColumnMove : false,
		viewConfig : {
		//		 让列的宽度和grid一样	
		//		forceFit : true
		},
		bodyStyle : 'border-left: 0px;',
		frame : false,
		store : processListGridStore,
		colModel : processListGridColumnModel
	})
}

function processMapRenderer(value, cellmeta, record) {
	return returnConnect(record.data['flowId'], 0,
			record.data['typeByData'], value);
}

function processRenderer(value, cellmeta, record) {
	return returnConnect(record.data['flowId'], 1,
			record.data['typeByData'], value);
}

function returnColor(isFlow, typeByData) {
	if (isFlow == 1) {
		if (typeByData == 0) {
			return "#FB1A1C;";
		} else if (typeByData == 1) {
			return "#e7a100;";
		} else if (typeByData == 2) {
			return "#00c317;";
		}
	}
}

function returnHref(flowId,isFlow, typeByData){
	if(isFlow==0){
		return "process.action?type=processMap&flowId="+flowId;
	}else{
		return "process.action?type=process&flowId="+flowId;
	}
}

function returnMap(flowId,isFlow, typeByData,value){
	if(typeByData==2){
		return "<div  ext:qtip='"
			+ value+ "'><a target='_blank' class='a-operation' href='"
			+ returnHref(flowId, isFlow,typeByData)
			+ "'>" + value + "</a></div>";
	}else{
		return "<div  ext:qtip='"
			+ value+ "'>" + value + "</div>";
	}
}

function returnConnect(flowId,isFlow, typeByData,value){
	if(typeByData==2){
		return "<div  ext:qtip='"
			+ value
			+ "'><a target='_blank' class='a-operation' href='"
			+ returnHref(flowId, isFlow,typeByData)
			+ "' style='color:"
			+ returnColor(isFlow, typeByData)
			+ "'>" + value + "</a></div>";
	}else{
		if(isFlow == 1){
			return "<div  ext:qtip='"
			+ value
			+ "'><span style='color:"
			+ returnColor(isFlow, typeByData)
			+ "'>" + value + "</span></div>";
		}else{
			return returnMap(flowId,isFlow,typeByData,value);
		}
	}
}

function processListJson(processId,isShowAll,isPop) {
	Ext.Ajax.request( {
		url : 'processSystemList.action',
		method : 'POST',
		timeout : 1000000,
		params : {
			flowId : processId,
			showAll : isShowAll
		},
		success : function(response, options) {
			// 获取response中的数据
		var json = Ext.util.JSON.decode(response.responseText);
		processListGrid(json,isPop);
		document.getElementById("noPubFlowTotal").innerHTML="("+json.noPubTotal+")";
		document.getElementById("approveFlowTotal").innerHTML="("+json.approveTotal+")";
		document.getElementById("pubFlowTotal").innerHTML="("+json.pubTotal+")";
		document.getElementById("allFlowTotal").innerHTML="("+json.allTotal+")";
		
		document.getElementById("processId").value=processId;
		document.getElementById("depth").value=depth;
		document.getElementById("isPop").value=isPop;
	},
	// 失败调用
		failure : function(response, options) {
		   Ext.Msg.alert(i_tip, i_readFlowListException);
		}
	});
}

// 显示全部
function showAllProcessList(){
	var processId = document.getElementById("processId").value;
	var isPop = document.getElementById("isPop").value;
	processListGridPanel.destroy();
	processListJson(processId,true,isPop);
}

// 下载
function downloadProcessList(){
	var processId = document.getElementById("processId").value;
	window.open( "downfindProcessSystemList.action?flowId="+processId);
}
