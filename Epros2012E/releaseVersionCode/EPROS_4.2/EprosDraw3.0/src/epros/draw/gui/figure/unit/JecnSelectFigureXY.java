package epros.draw.gui.figure.unit;

/**
 * 
 * 获取指定区域内坐标的最大值和最小值
 * 
 * @author Administrator
 * @date： 日期：May 10, 2012 时间：5:51:05 PM
 */
public class JecnSelectFigureXY {
	/** 制定区域 X坐标最小值 */
	private int minX;
	/** 制定区域 Y坐标最小值 */
	private int minY;
	/** 制定区域 X坐标最大值 */
	private int maxX;
	/** 制定区域 Y坐标最大值 */
	private int maxY;

	public int getMinX() {
		return minX;
	}

	public void setMinX(int minX) {
		this.minX = minX;
	}

	public int getMinY() {
		return minY;
	}

	public void setMinY(int minY) {
		this.minY = minY;
	}

	public int getMaxX() {
		return maxX;
	}

	public void setMaxX(int maxX) {
		this.maxX = maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public void setMaxY(int maxY) {
		this.maxY = maxY;
	}

}
