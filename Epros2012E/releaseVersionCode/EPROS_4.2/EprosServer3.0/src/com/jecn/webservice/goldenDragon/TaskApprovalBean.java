package com.jecn.webservice.goldenDragon;

public class TaskApprovalBean {
	private Long taskId;
	private String taskName;
	private Long realtedId;
	private int taskType;
	/** 申请人登录名称 */
	private String applyName;
	private String createDate;

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Long getRealtedId() {
		return realtedId;
	}

	public void setRealtedId(Long realtedId) {
		this.realtedId = realtedId;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public String getApplyName() {
		return applyName;
	}

	public void setApplyName(String applyName) {
		this.applyName = applyName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

}
