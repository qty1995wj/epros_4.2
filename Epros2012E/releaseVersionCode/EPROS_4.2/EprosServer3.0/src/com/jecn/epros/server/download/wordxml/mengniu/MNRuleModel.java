package com.jecn.epros.server.download.wordxml.mengniu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ParagraphRowIndBean;
import wordxml.element.bean.paragraph.ParagraphSpaceBean;
import wordxml.element.bean.paragraph.GroupBean.GroupAlign;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.bean.paragraph.GroupBean.Hanchor;
import wordxml.element.bean.paragraph.GroupBean.Vanchor;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.RuleDownloadBean;
import com.jecn.epros.server.bean.rule.JecnRuleBaiscInfoT;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.download.wordxml.RuleModel;

public class MNRuleModel extends RuleModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/****** 段落行距固定值20磅 *********/
	private ParagraphLineRule lineRule = new ParagraphLineRule(18, LineRuleType.EXACT);

	/**
	 * 默认模版
	 * 
	 * @param ruleDownloadBean
	 * @param path
	 * @param flowChartDirection
	 */
	public MNRuleModel(RuleDownloadBean ruleDownloadBean, String path) {
		super(ruleDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(15F);
		getDocProperty().setNewRowHeight(0.6F);
		getDocProperty().setFlowChartScaleValue(6F);
		setDocStyle(" ", textTitleStyle());
		setDefAscii("黑体");
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 15.6F));
		// 设置页边距
		sectBean.setPage(3.7F, 2.8F, 3.5F, 2.6F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	private SectBean getTitleSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(3.7F, 2.8F, 3.5F, 2.6F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		Footer footer = sect.createFooter(type);
		ParagraphBean pBean = new ParagraphBean(Align.center, "仿宋_GB2312", Constants.xiaosi, false);
		// footer.createParagraph("公司机密,未经批准不得扩散", pBean);
		footer.createParagraph("", pBean).appendCurPageRun();
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		// 创建前言
		firstSect.createParagraph("前  言", new ParagraphBean(Align.center, "黑体", Constants.xiaoer, false));
		firstSect.createMutilEmptyParagraph(2);

		ParagraphBean pBean = new ParagraphBean(Align.left, "仿宋_GB2312", Constants.sanhao, false);
		pBean.setParagraphSpaceBean(new ParagraphSpaceBean(0F, 0F, new ParagraphLineRule(26, LineRuleType.EXACT)));
		pBean.setInd(0F, 0F, 0, 1.5F);

		JecnRuleBaiscInfoT baiscInfo = ruleDownloadBean.getBaiscInfo();
		firstSect.createParagraph("目的", pBean);
		firstSect.createParagraph(baiscInfo.getPurpose(), pBean);
		firstSect.createParagraph("适用范围", pBean);
		firstSect.createParagraph(baiscInfo.getApplicability(), pBean);
		firstSect.createParagraph("文件换版说明", pBean);
		firstSect.createParagraph(baiscInfo.getChangeVersionExplain(), pBean);
		firstSect.createParagraph("文件编写部门、归口部门", pBean);
		firstSect.createParagraph(baiscInfo.getWriteDept(), pBean);
		firstSect.createParagraph("主要起草人", pBean);
		firstSect.createParagraph(baiscInfo.getDraftman(), pBean);
		firstSect.createParagraph("主要起草单位", pBean);
		firstSect.createParagraph(baiscInfo.getDraftingUnit(), pBean);

		firstSect.createParagraph("文件发布次数及历次更改信息", pBean);
		TableBean tBean = new TableBean();
		tBean.setBorder(0.5F);
		tBean.setTableAlign(Align.center);
		Table table = firstSect.createTable(tBean, new float[] { 1.44F, 6.01F, 3.89F, 4.07F });

		ParagraphBean titleP = new ParagraphBean(Align.center, "仿宋_GB2312", Constants.xiaosi, true);
		table.createTableRow(new ArrayList<String[]>() {
			{
				add(new String[] { "序号", "文件编号", "文件实施日期", "更改内容" });
			}
		}, titleP);

		List<String[]> data = new ArrayList<String[]>();
		if (ruleDownloadBean.getHistorys() != null && ruleDownloadBean.getHistorys().size() > 0) {
			for (int i = 0; i < ruleDownloadBean.getHistorys().size(); i++) {
				JecnTaskHistoryNew historyNew = ruleDownloadBean.getHistorys().get(i);
				data.add(new String[] { i + 1 + "", historyNew.getVersionId(),
						JecnCommon.getStringbyDate(historyNew.getImplementDate()), historyNew.getModifyExplain() });
			}
		}
		ParagraphBean contentP = new ParagraphBean(Align.center, "仿宋_GB2312", Constants.xiaowu, false);
		table.createTableRow(data, contentP);
		table.setCellValignAndHeight(Valign.center, 0.7F);

		firstSect.createParagraph("表1  文件历次更改信息", new ParagraphBean(Align.center, "黑体", Constants.xiaosi, false)
				.setParagraphSpaceBean(new ParagraphSpaceBean(0F, 0F, new ParagraphLineRule(26, LineRuleType.EXACT))));
		if (StringUtils.isNotBlank(baiscInfo.getTestRunFile())) {
			firstSect.createParagraph("试行版文件", pBean);
			firstSect.createParagraph(baiscInfo.getTestRunFile(), pBean);
		}
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getTitleSectBean());

		Image logo = new Image(getDocProperty().getLogoImage());
		logo.setSize(3.76F, 1.46F);
		Paragraph p = titleSect.createParagraph("");
		p.setParagraphBean(new ParagraphBean(Align.right));
		p.appendGraphRun(logo);
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		pBean.setSpace(0F, 0F, vLine1);
		ParagraphRowIndBean iBean = new ParagraphRowIndBean();
		iBean.setLeft(0F);
		// 0.11个字符 0.9187282
		iBean.setRight(0.101060102F);
		pBean.setParagraphRowIndBean(iBean);
		// 公司名称
		titleSect.createParagraph("内蒙古蒙牛乳业（集团）股份有限公司", pBean);

		crateFlowNumber(titleSect);

		// 创建一条线
		createLine(titleSect, 0, 35, 438, 35);

		createFlowName(titleSect);

		createPubLine(titleSect);

		createLine(titleSect, 0, 480, 438, 480);

		createComponyPubLine(titleSect);

		// 分页
		titleSect.addParagraph(new Paragraph(true));

		createApprovePage(titleSect);

		createCommhdrftr(titleSect);

	}

	private void crateFlowNumber(Sect titleSect) {

		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 7, GroupH.exact, 0.6).setHorizontal(12, Hanchor.page, 0)
				.setVertical(6.8, Vanchor.page, 0).lock(true);

		// 编号
		ParagraphBean pBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		pubGroup.createParagraph("编号：" + nullToEmpty(ruleDownloadBean.getRuleInputNum()), pBean);

	}

	private void createFlowName(Sect titleSect) {
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.auto, 0, GroupH.auto, 0).setHorizontal(GroupAlign.center, Hanchor.page,
				0).setVertical(12F, Vanchor.page, 0).lock(true);

		// 流程名称
		String flowName = ruleDownloadBean.getRuleName();
		// 仿宋_GB2312 一号 加粗 居中
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		pubGroup.addParagraph(new Paragraph(flowName, pBean));
	}

	private void createApprovePage(Sect titleSect) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.xiaoer, false);
		pBean.setInd(0, 0, 0, 0.78f);
		pBean.setSpace(1f, 1.5f, 0, vLine1);
		// 审 批 单
		titleSect.createParagraph("审 批 单", pBean);
		titleSect.addParagraph(getEmptyEight());

		List<JecnTaskHistoryNew> historys = ruleDownloadBean.getHistorys();
		TableBean tBean = new TableBean();
		tBean.setBorder(0.5f);
		tBean.setTableAlign(Align.center);

		Table approveTable = titleSect.createTable(new float[] { 5.94f, 4.56f });
		approveTable.setTableBean(tBean);
		titleSect.createMutilEmptyParagraph(2);
		Table lastApproveTable = titleSect.createTable(new float[] { 5.94f, 4.56f });
		lastApproveTable.setTableBean(tBean);
		ParagraphBean titleP = new ParagraphBean(Align.center, "仿宋_GB2312", Constants.sanhao, false);
		ParagraphBean contentP = new ParagraphBean(Align.center, "仿宋_GB2312", Constants.sihao, false);

		approveTable.createTableRow(new ArrayList<String[]>() {
			{
				add(new String[] { "审核单位", "审核人" });
			}
		}, titleP);
		lastApproveTable.createTableRow(new ArrayList<String[]>() {

			{
				add(new String[] { "批准单位", "批准人" });
			}

		}, titleP);
		List<String[]> approveList = new ArrayList<String[]>();
		List<String[]> lastApproveList = new ArrayList<String[]>();
		if (historys != null && historys.size() > 0) {
			List<JecnTaskHistoryFollow> listJecnTaskHistoryFollow = historys.get(0).getListJecnTaskHistoryFollow();
			if (listJecnTaskHistoryFollow != null && listJecnTaskHistoryFollow.size() > 0) {
				for (int i = 0; i < listJecnTaskHistoryFollow.size() - 1; i++) {
					approveList.add(new String[] { listJecnTaskHistoryFollow.get(i).getApproveOrgName(),
							nullToEmpty(listJecnTaskHistoryFollow.get(i).getName()).trim() });
				}
				lastApproveList.add(new String[] {
						listJecnTaskHistoryFollow.get(listJecnTaskHistoryFollow.size() - 1).getApproveOrgName(),
						nullToEmpty(listJecnTaskHistoryFollow.get(listJecnTaskHistoryFollow.size() - 1).getName())
								.trim() });
			}
		}
		approveTable.createTableRow(approveList, contentP);
		lastApproveTable.createTableRow(lastApproveList, contentP);
	}

	private void createComponyPubLine(Sect titleSect) {

		ParagraphBean pubBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.auto, 0, GroupH.auto, 0).setHorizontal(GroupAlign.center, Hanchor.page,
				0).setVertical(24, Vanchor.page, 0).lock(true);
		pubGroup.addParagraph(new Paragraph("内蒙古蒙牛乳业（集团）股份有限公司行政管理中心  发布", pubBean));

	}

	private void createPubLine(Sect titleSect) {

		// 发布时间
		String pubDateStr = nullToEmpty(ruleDownloadBean.getPubDate());
		if (pubDateStr.length() >= 10) {
			pubDateStr = pubDateStr.substring(0, 10).replaceFirst("-", "年").replaceFirst("-", "月") + "日";
		}
		// 2016 - 05 - 01实施 黑体 14
		String nextMonthFirstDay = "";
		if (StringUtils.isNotBlank(pubDateStr)) {

			try {
				// 发布日期后的三天
				SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
				Date pubDate = format.parse(pubDateStr);
				Calendar cal = Calendar.getInstance();
				cal.setTime(pubDate);
				cal.add(Calendar.DAY_OF_MONTH, 3);
				nextMonthFirstDay = format.format(cal.getTime());

			} catch (ParseException e) {
				log.error(e);
			}
		}
		// ParagraphBean pBean = new ParagraphBean(Align.center, "黑体",
		// Constants.sihao, false);
		// pBean.setSpace(0f, 0f, 0, new ParagraphLineRule(28,
		// LineRuleType.EXACT));
		// titleSect.createParagraph(pubDateStr + "发布" +
		// "	                             " + nextMonthFirstDay + "实施",
		// pBean);

		// 2016 - 04 - 11发布 黑体 四号
		ParagraphBean pubBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(2.8, Hanchor.page, 0)
				.setVertical(23, Vanchor.page, 0.32).lock(true);
		pubGroup.addParagraph(new Paragraph(pubDateStr + "发布", pubBean));

		// 2016 - 04 - 11实施 黑体 四号
		ParagraphBean nextBean = new ParagraphBean(Align.right, "黑体", Constants.sihao, false);
		Group nextGroup = titleSect.createGroup();
		nextGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(11.3, Hanchor.page, 0)
				.setVertical(23, Vanchor.page, 0.32).lock(true);
		nextGroup.addParagraph(new Paragraph(nextMonthFirstDay + "实施", nextBean));
	}

	private void createLine(Sect sect, float fx, float fy, float tx, float ty) {
		// 单线条
		LineGraph line = new LineGraph(LineStyle.single, 0.5F);
		line.setFrom(fx, fy);
		line.setTo(tx, ty);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(2);
		line.setPositionBean(positionBean);
		sect.createParagraph(line);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, new FontBean("仿宋_GB2312", Constants.wuhao, false));
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		/** 默认表格样式 ****/
		TableBean tblStyle = new TableBean();// 
		// 所有边框 1.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setTableAlign(Align.center);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, new FontBean("仿宋_GB2312", Constants.wuhao, true));
		tblTitileStyle.setAlign(Align.center);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, new FontBean("仿宋_GB2312", Constants.sanhao,
				false));
		// 2字符转厘米 0.4240284
		textContentStyle.setInd(0, 0, 0, 0.8480568f);
		textContentStyle.setSpace(0.2f, 0.2f, lineRule);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, new FontBean("黑体", Constants.sanhao, false));
		textTitleStyle.setInd(0, 0, 0.74F, 0); // 段落缩进 厘米
		textTitleStyle.setSpace(1F, 1F, vLine1); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	protected void beforeHandle() {

	}

	/**
	 * 空八号
	 * 
	 * @return
	 */
	private Paragraph getEmptyEight() {
		Paragraph paragraph = new Paragraph(" ");
		paragraph.setParagraphBean(new ParagraphBean("仿宋_GB2312", Constants.bahao, false));
		return paragraph;
	}

	private void setHeight(Table table, float height) {
		for (TableRow row : table.getRows()) {
			row.setHeight(height);
		}
	}

}
