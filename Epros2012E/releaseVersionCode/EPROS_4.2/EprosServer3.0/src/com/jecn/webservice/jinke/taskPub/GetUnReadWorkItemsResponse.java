/**
 * GetUnReadWorkItemsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetUnReadWorkItemsResponse  implements java.io.Serializable {
    private com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getUnReadWorkItemsResult;

    public GetUnReadWorkItemsResponse() {
    }

    public GetUnReadWorkItemsResponse(
           com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getUnReadWorkItemsResult) {
           this.getUnReadWorkItemsResult = getUnReadWorkItemsResult;
    }


    /**
     * Gets the getUnReadWorkItemsResult value for this GetUnReadWorkItemsResponse.
     * 
     * @return getUnReadWorkItemsResult
     */
    public com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getGetUnReadWorkItemsResult() {
        return getUnReadWorkItemsResult;
    }


    /**
     * Sets the getUnReadWorkItemsResult value for this GetUnReadWorkItemsResponse.
     * 
     * @param getUnReadWorkItemsResult
     */
    public void setGetUnReadWorkItemsResult(com.jecn.webservice.jinke.taskPub.CirculateItemViewModel[] getUnReadWorkItemsResult) {
        this.getUnReadWorkItemsResult = getUnReadWorkItemsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetUnReadWorkItemsResponse)) return false;
        GetUnReadWorkItemsResponse other = (GetUnReadWorkItemsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getUnReadWorkItemsResult==null && other.getGetUnReadWorkItemsResult()==null) || 
             (this.getUnReadWorkItemsResult!=null &&
              java.util.Arrays.equals(this.getUnReadWorkItemsResult, other.getGetUnReadWorkItemsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetUnReadWorkItemsResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetUnReadWorkItemsResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetUnReadWorkItemsResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetUnReadWorkItemsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetUnReadWorkItemsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getUnReadWorkItemsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GetUnReadWorkItemsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "CirculateItemViewModel"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "CirculateItemViewModel"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
