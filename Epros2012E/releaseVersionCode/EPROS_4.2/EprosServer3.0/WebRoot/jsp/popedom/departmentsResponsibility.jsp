<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div">
			<table width="100%" align="left">
				<tr>
					<td width="100px" style="vertical-align: top;">
						<img src="${basePath}images/common/triangle.gif"
							style="vertical-align: middle;">
						<!-- 部门职责 -->
						<s:text name="departmentsResponsibilityC" />
					</td>
					<td style="word-wrap: break-word; word-break: break-all;"
						align="left">
						<s:property value="orgDuties" escape="false" />
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>