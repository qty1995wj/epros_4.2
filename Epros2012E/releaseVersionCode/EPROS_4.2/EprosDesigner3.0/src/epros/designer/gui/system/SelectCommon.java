package epros.designer.gui.system;

import java.util.ArrayList;
import java.util.List;

import epros.designer.gui.system.listener.SelectChangeListener;

public abstract class SelectCommon {
	
	// 目前只有人员单选实现了该监听，其余未实现
	List<SelectChangeListener> selectChangeListeners=new ArrayList<SelectChangeListener>();
	
	public void addSelectChangeListener(SelectChangeListener listener){
		this.selectChangeListeners.add(listener);
	}
	
	public void clearSelectChangeListeners() {
		selectChangeListeners.clear();
	}

}
