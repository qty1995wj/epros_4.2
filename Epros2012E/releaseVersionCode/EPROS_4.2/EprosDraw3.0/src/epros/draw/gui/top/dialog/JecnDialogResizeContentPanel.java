package epros.draw.gui.top.dialog;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;

/**
 * 
 * 对话框内容面板:改变对话框大小的面板
 * 
 * 拖动对话框边框改变大小
 * 
 * 根据鼠标进入对话框的方向指定对应的鼠标类型，根据鼠标类型改变对话框大小或位置点
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDialogResizeContentPanel extends JPanel implements MouseMotionListener, MouseListener {
	/** 叫边长度 */
	private final int panelHorn = 6;
	/** 对话框 */
	private JecnDialog dialog = null;
	/** 按下鼠标坐标 */
	private Point pressPoint;

	public JecnDialogResizeContentPanel(JecnDialog dialog) {
		if (dialog == null) {
			JecnLogConstants.LOG_DIALOG_RESIZE_CONTENT_PANEL.error("JecnDialogResizeContentPanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.dialog = dialog;

		initComponent();
	}

	private void initComponent() {
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}

	public void mouseMoved(MouseEvent e) {
		if (dialog == null || dialog.getToolTitlePanel() == null) {
			return;
		}
		boolean isMax = dialog.getToolTitlePanel().isMax();
		if (!dialog.isResizable() || isMax) {// 对话框最大化或不支持改变对话框大小
			return;
		}

		// 根据鼠标进入区域设置对应的鼠标类型
		if (e.getSource() instanceof JecnDialogResizeContentPanel) {
			JecnDialogResizeContentPanel resizePanel = (JecnDialogResizeContentPanel) e.getSource();
			// 获取鼠标进入面板的鼠标类型：上、下、左、右、左上、左下、右上、右下
			Cursor resizeCursor = getResizeCursor(e.getX(), e.getY());
			if (resizePanel.getCursor() != resizeCursor) {
				// 设置鼠标类型
				resizePanel.setCursor(resizeCursor);
			}
		}
	}

	public void mouseDragged(MouseEvent e) {
		boolean isMax = dialog.getToolTitlePanel().isMax();
		if (!dialog.isResizable() || isMax) {// 对话框最大化或不支持改变对话框大小
			return;
		}

		if (e.getSource() instanceof JecnDialogResizeContentPanel) {
			JecnDialogResizeContentPanel resizePanel = (JecnDialogResizeContentPanel) e.getSource();
			// 改变对话框的大小和位置点
			resizeJecnFrame(e.getX(), e.getY(), resizePanel);
		}

	}

	public void mouseExited(MouseEvent e) {
		if (this.dialog.getCurrContent() == null) {
			return;
		}
		this.dialog.getCurrContent().setCursor(Cursor.getDefaultCursor());
		this.dialog.getDialogHeaderPanel().setCursor(Cursor.getDefaultCursor());
	}

	public void mousePressed(MouseEvent e) {
		pressPoint = e.getPoint();

	}

	public void mouseClicked(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {
	}

	private void resizeJecnFrame(int x, int y, JecnDialogResizeContentPanel resizePanel) {
		if (pressPoint == null) {
			return;
		}

		double minWidth = 100;
		int minHeight = 25;
		if (resizePanel.getCursor() == Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR)) {// 北面进入拖动
			// 偏移量
			int offsetY = y - pressPoint.y;
			int nH = dialog.getHeight() - offsetY;
			if (nH <= minHeight) {
				return;
			}
			// 改变大小
			dialog.setBounds(dialog.getX(), dialog.getY() + offsetY, dialog.getWidth(), nH);
		} else if (resizePanel.getCursor() == Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR)) {// 东面进入拖动
			int offsetX = x - pressPoint.x;
			int nW = dialog.getWidth() + offsetX;
			if (nW < minWidth) {
				return;
			}
			pressPoint.x += offsetX;
			// 改变大小
			dialog.setSize(nW, dialog.getHeight());
		} else if (resizePanel.getCursor() == Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR)) {// 南面进入拖动
			int offset = y - pressPoint.y;
			int nH = dialog.getHeight() + offset;
			if (nH <= minHeight) {
				return;
			}
			pressPoint.y += offset;
			dialog.setSize(dialog.getWidth(), nH);
		} else if (resizePanel.getCursor() == Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR)) {// 西面进入拖动
			int offset = x - pressPoint.x;
			int nW = dialog.getWidth() - offset;
			if (nW < minWidth) {
				return;
			}
			dialog.setBounds(dialog.getX() + offset, dialog.getY(), nW, dialog.getHeight());
		} else if (resizePanel.getCursor() == Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR)) {// 东北面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;
			int nW = dialog.getWidth() + xoffset;
			int nH = dialog.getHeight() - yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}
			pressPoint.x += xoffset;
			dialog.setBounds(dialog.getX(), dialog.getY() + yoffset, nW, nH);
		} else if (resizePanel.getCursor() == Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR)) {// 东南面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;

			int nW = dialog.getWidth() + xoffset;
			int nH = dialog.getHeight() + yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}

			pressPoint.x += xoffset;
			pressPoint.y += yoffset;
			dialog.setBounds(dialog.getX(), dialog.getY(), nW, nH);
		} else if (resizePanel.getCursor() == Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR)) {// 西南面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;

			int nW = dialog.getWidth() - xoffset;
			int nH = dialog.getHeight() + yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}

			pressPoint.y += yoffset;
			dialog.setBounds(dialog.getX() + xoffset, dialog.getY(), nW, nH);
		} else if (resizePanel.getCursor() == Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR)) {// 西北面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;
			int nW = dialog.getWidth() - xoffset;
			int nH = dialog.getHeight() - yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}
			dialog.setBounds(dialog.getX() + xoffset, dialog.getY() + yoffset, nW, nH);
		}
	}

	/**
	 * 
	 * 获取鼠标进入面板的鼠标类型：上、下、左、右、左上、左下、右上、右下
	 * 
	 * 
	 * @param x
	 *            鼠标点击X坐标
	 * @param y
	 *            鼠标点击Y坐标
	 * @return Cursor 返回鼠标类型
	 */
	private Cursor getResizeCursor(int x, int y) {
		// 左边进入
		boolean left = isInLeft(x);
		// 右边进入
		boolean right = isInRight(x);
		// 上边进入
		boolean top = isInTop(y);
		// 下边进入
		boolean bottom = isInBottom(y);
		if (left) {
			if (top) {// 左上角
				return Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR);
			} else if (bottom) {// 左下角
				return Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
			} else {// 左边
				return Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR);
			}
		} else if (right) {
			if (top) {// 右上
				return Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR);
			} else if (bottom) {// 右下
				return Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR);
			} else {// 右边
				return Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
			}
		} else if (top) {// 上边
			return Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);
		} else if (bottom) {// 下边
			return Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR);
		} else {
			return Cursor.getDefaultCursor();
		}

	}

	/**
	 * 
	 * 左边进入
	 * 
	 * @param x
	 *            int 鼠标点击X坐标
	 * @return boolean true：左边进入 false：不是左边进入
	 */
	private boolean isInLeft(int x) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (x > rect.getX() && x < rect.getX() + panelHorn) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 右边进入
	 * 
	 * @param x
	 *            int 鼠标点击X坐标
	 * @return boolean true：右边进入 false：不是右边进入
	 */
	private boolean isInRight(int x) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (x > rect.getX() + rect.getWidth() - panelHorn && x < rect.getX() + rect.getWidth()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 上边进入
	 * 
	 * @param y
	 *            int 鼠标点击Y坐标
	 * @return boolean true：上边进入 false：不是上边进入
	 */
	private boolean isInTop(int y) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (y > rect.getY() && y < rect.getY() + panelHorn) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 下边进入
	 * 
	 * @param y
	 *            int 鼠标点击Y坐标
	 * @return boolean true：下边进入 false：不是下边进入
	 */
	private boolean isInBottom(int y) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (y > rect.getY() + rect.getHeight() - panelHorn && y < rect.getY() + rect.getHeight()) {
			return true;
		}
		return false;
	}

}
