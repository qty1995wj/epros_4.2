package epros.designer.gui.process.guide.guideTable;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.gui.process.guide.DesignerProcessOperationIntrusDialog;
import epros.designer.gui.process.roleDetail.DesignerRoleDetailDialog;
import epros.designer.util.ConnectionPool;
import epros.draw.designer.JecnFlowStationT;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * @author fuzhh Oct 26, 2012
 * 
 */
public class RoleResponseTable extends JTable implements MouseListener {
	/** 初始化传入的数据 */
	private List<JecnBaseRoleFigure> baseRoleFigureList;
	// 操作说明
	private DesignerProcessOperationIntrusDialog processOperationIntrusDialog;

	/**
	 * 初始化JTable数据
	 * 
	 * @param listRoleBean
	 *            用来初始化的数据
	 */
	public RoleResponseTable(List<JecnBaseRoleFigure> baseRoleFigureList,
			DesignerProcessOperationIntrusDialog processOperationIntrusDialog) {
		this.baseRoleFigureList = baseRoleFigureList;
		this.processOperationIntrusDialog = processOperationIntrusDialog;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
		this.addMouseListener(this);
	}

	/**
	 * 初始化JTable
	 * 
	 * @author fuzhh Aug 28, 2012
	 * @return
	 */
	public RoleResponseTableMode getTableModel() {
		boolean showPosNameBox = false;
		try {
			String value = ConnectionPool.getConfigAciton().selectValue(1, "showPosNameBox");
			if ("1".equals(value)) {
				showPosNameBox = true;
			}
		} catch (Exception e) {
			throw new RuntimeException("RoleResponseTable getTableModel is error");
		}
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("roleName"));
		// 岗位名称
		if (showPosNameBox) {
			title.add(JecnResourceUtil.getJecnResourceUtil().getValue("positionName"));
		}
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("responsibilities"));

		// 获取操作说明数据
		Vector<Vector<Object>> rowData = new Vector<Vector<Object>>();
		for (JecnBaseRoleFigure baseRoleFigure : baseRoleFigureList) {
			Vector<Object> row = new Vector<Object>();
			// 岗位名称
			String roleName = "";
			if (baseRoleFigure.getFlowElementData().getFlowStationList() != null) {
				for (JecnFlowStationT flowStationT : baseRoleFigure.getFlowElementData().getFlowStationList()) {
					if ("0".equals(flowStationT.getType())) {
						roleName += flowStationT.getStationName() + "/";
					}
				}
				if ("".equals(roleName)) {
					for (JecnFlowStationT flowStationT : baseRoleFigure.getFlowElementData().getFlowStationList()) {
						if ("1".equals(flowStationT.getType())) {
							roleName += flowStationT.getStationName() + "/";
						}
					}
				}
				if (!"".equals(roleName)) {
					roleName = roleName.substring(0, roleName.length() - 1);
				}
			}
			row.add(baseRoleFigure.getFlowElementData().getFlowElementUUID());
			row.add(baseRoleFigure.getFlowElementData().getFigureText());
			if (showPosNameBox) {
				row.add(roleName);
			}
			row.add(baseRoleFigure.getFlowElementData().getRoleRes());
			rowData.add(row);
		}
		return new RoleResponseTableMode(rowData, title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class RoleResponseTableMode extends DefaultTableModel {
		public RoleResponseTableMode(Vector<Vector<Object>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// 判断为左键双击
		if (e.getClickCount() == 2 && e.getButton() == 1) {
			int selectRow = this.getSelectedRow();
			JecnBaseRoleFigure baseRoleFigure = baseRoleFigureList.get(selectRow);
			DesignerRoleDetailDialog roleDetailDialog = new DesignerRoleDetailDialog(baseRoleFigure,
					processOperationIntrusDialog);
			roleDetailDialog.setVisible(true);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}
}
