<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" class="div">
			<div id="inDetailDocument">
				<table align="left">
					<tr>
						<td>
							<img src="${basePath}images/common/triangle.gif">
							<!-- 流程地图名称： -->
							<s:text name="processMapNameC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;">
							<s:property value="processMapBaseInfo.flowMapName" />
						</td>
					</tr>
					<tr>
						<td>
							<img src="${basePath}images/common/triangle.gif">
							<!-- 流程地图编号： -->
							<s:text name="processMapNumbersC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;">
							<s:property value="processMapBaseInfo.flowMapIdInput" />
						</td>
					</tr>
					<tr>
						<td>
							<img src="${basePath}images/common/triangle.gif">
							<!-- 密级： -->
							<s:text name="intensiveC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;">
							<s:if test="processMapBaseInfo.isPublic == 0">
								${secret}
							</s:if>
							<s:elseif test="processMapBaseInfo.isPublic == 1">
								<s:property value="#application.public"/>
							</s:elseif>
						</td>
					</tr>
					<tr>
						<td>
							<img src="${basePath}images/common/triangle.gif">
							<!-- 部门权限： -->
							<s:text name="orgPermissionC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;">
							<s:iterator value="processMapBaseInfo.orgList">
							<a href="organization.action?orgId=<s:property value='id'/>"
							   target='_blank' style="cursor: pointer;" class="table">
						         [<s:property value="name" />]
						      </a>
						    </s:iterator>
						</td>
					</tr>
					<tr>
						<td>
							<img src="${basePath}images/common/triangle.gif">
							<!-- 岗位权限： -->
							<s:text name="posPermissionC" />
						</td>
						<td style="word-wrap: break-word; word-break: break-all;">
							<s:iterator value="processMapBaseInfo.posList">
							 <a href="orgSpecification.action?posId=<s:property value='id'/>" 
						         target='_blank' style="cursor: pointer;" class="table">
						      [<s:property value="name" />]
						     </a>
						    </s:iterator>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</body>
</html>