package com.jecn.epros.domain.system;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.ScopeAuthority;
import com.jecn.epros.domain.ScopeAuthority;

import java.util.List;

/**
 * @author yxw 2013-3-7
 * @description：角色列表bean
 */
public class RoleWebInfoBean {
    private Long roleId;//主键ID
    private String roleName;//角色名称
    private String roleRemark;//备注
    private String  roleType;//角色类型

    //流程权限范围
    @JsonIgnore
    private List<ScopeAuthority> flowRes;
    //文档权限范围
    @JsonIgnore
    private List<ScopeAuthority> fileRes;
    //标准权限范围
    @JsonIgnore
    private List<ScopeAuthority> standRes;
    //制度权限范围
    @JsonIgnore
    private List<ScopeAuthority> ruleRes;
    // 风险范围
    @JsonIgnore
    private List<ScopeAuthority> riskRes;
    // 术语范围
    @JsonIgnore
    private List<ScopeAuthority> termRes;
    //组织权限范围
    @JsonIgnore
    private List<ScopeAuthority> orgRes;
    //岗位组权限范围
    @JsonIgnore
    private List<ScopeAuthority> posGroupkRes;


    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleRemark() {
        return roleRemark;
    }

    public void setRoleRemark(String roleRemark) {
        this.roleRemark = roleRemark;
    }

    @JsonIgnore
    public List<ScopeAuthority> getRiskRes() {
        return riskRes;
    }

    public void setRiskRes(List<ScopeAuthority> riskRes) {
        this.riskRes = riskRes;
    }

    @JsonIgnore
    public List<ScopeAuthority> getFlowRes() {
        return flowRes;
    }

    public void setFlowRes(List<ScopeAuthority> flowRes) {
        this.flowRes = flowRes;
    }

    @JsonIgnore
    public List<ScopeAuthority> getFileRes() {
        return fileRes;
    }

    public void setFileRes(List<ScopeAuthority> fileRes) {
        this.fileRes = fileRes;
    }

    @JsonIgnore
    public List<ScopeAuthority> getStandRes() {
        return standRes;
    }

    public void setStandRes(List<ScopeAuthority> standRes) {
        this.standRes = standRes;
    }

    @JsonIgnore
    public List<ScopeAuthority> getRuleRes() {
        return ruleRes;
    }

    public void setRuleRes(List<ScopeAuthority> ruleRes) {
        this.ruleRes = ruleRes;
    }

    @JsonIgnore
    public List<ScopeAuthority> getOrgRes() {
        return orgRes;
    }

    public void setOrgRes(List<ScopeAuthority> orgRes) {
        this.orgRes = orgRes;
    }

    @JsonIgnore
    public List<ScopeAuthority> getPosGroupkRes() {
        return posGroupkRes;
    }

    public void setPosGroupkRes(List<ScopeAuthority> posGroupkRes) {
        this.posGroupkRes = posGroupkRes;
    }

    public List<ScopeAuthority> getTermRes() {
        return termRes;
    }

    public void setTermRes(List<ScopeAuthority> termRes) {
        this.termRes = termRes;
    }
}
