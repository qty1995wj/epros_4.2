package com.jecn.dataimport.util;


import java.util.List;

public class FileActivityOutBean implements java.io.Serializable {
	private String fileMId;
	private String fileTId;
	private String fileNameMode;
	private String fileNameTemplet;
	private List<FileBean> fileBeanList;

	public String getFileMId() {
		return fileMId;
	}

	public void setFileMId(String fileMId) {
		this.fileMId = fileMId;
	}

	public String getFileTId() {
		return fileTId;
	}

	public void setFileTId(String fileTId) {
		this.fileTId = fileTId;
	}

	public String getFileNameMode() {
		return fileNameMode;
	}

	public void setFileNameMode(String fileNameMode) {
		this.fileNameMode = fileNameMode;
	}

	public String getFileNameTemplet() {
		return fileNameTemplet;
	}

	public void setFileNameTemplet(String fileNameTemplet) {
		this.fileNameTemplet = fileNameTemplet;
	}

	public List<FileBean> getFileBeanList() {
		return fileBeanList;
	}

	public void setFileBeanList(List<FileBean> fileBeanList) {
		this.fileBeanList = fileBeanList;
	}
}
