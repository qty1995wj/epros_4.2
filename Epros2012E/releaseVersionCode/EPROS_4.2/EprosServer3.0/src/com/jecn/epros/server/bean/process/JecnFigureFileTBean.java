package com.jecn.epros.server.bean.process;

import java.io.Serializable;

/**
 * 流程地图元素添加附件临时表
 * 
 * 目前流程图的文档元素也使用此bean存储数据
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-4 时间：上午10:44:33
 */
public class JecnFigureFileTBean implements Serializable {
	/** 主键ID */
	private String id;
	/** 关联地图元素ID */
	private Long figureId;
	private String figureUUID;// 元素索引
	/** 关联附件ID */
	private Long fileId;
	/** 关联地图元素类型 */
	private String figureType;
	/** 文件名称 */
	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFigureType() {
		return figureType;
	}

	public void setFigureType(String figureType) {
		this.figureType = figureType;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}
}
