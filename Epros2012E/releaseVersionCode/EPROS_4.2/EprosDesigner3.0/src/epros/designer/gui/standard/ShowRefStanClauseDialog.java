package epros.designer.gui.standard;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.standard.StandardBean;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 制度相关标准，双击详情弹出
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-11-27 时间：下午02:11:58
 */
public class ShowRefStanClauseDialog extends StanClauseDialog {
	private static Logger log = Logger.getLogger(ShowRefStanClauseDialog.class);
	private Long standardId;
	private StandardBean standardBean = null;

	/** 节点是否存在 */
	private boolean isExists = true;

	public boolean isExists() {
		return isExists;
	}

	public ShowRefStanClauseDialog(Long standardId) {
		super();
		this.standardId = standardId;
		this.setTitle(JecnProperties.getValue("showClause"));
		initData();
	}

	private void initData() {
		try {
			standardBean = ConnectionPool.getStandardAction().getStandardBean(standardId);
			if (standardBean != null) {
				clauseField.setText(standardBean.getCriterionClassName());
				clauseContentArea.setText(standardBean.getStanContent());
				remarkArea.setText(standardBean.getNote());
				programFileCheck.setSelected(standardBean.getIsproFile() == 1);

				// 不可编辑
				clauseField.setEditable(false);
				clauseContentArea.setEditable(false);
				remarkArea.setEditable(false);
				programFileCheck.setEnabled(false);

				// 隐藏确定按钮
				okButton.setVisible(false);
				// 初始化表格
				uploadFileCommon.initTableData(standardBean.getFileContents());
			} else {
				isExists = false;
				// 条款已删除！
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("termsIsDelete"));
				return;
			}

		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("displayStandardError"));
			log.error(JecnProperties.getValue("displayStandardError"), e);
		}
	}

	@Override
	protected boolean okButtonAction() {
		return false;
	}

	@Override
	protected boolean validateNodeRepeat(String name) {
		return false;
	}

	@Override
	protected boolean isUpdate() {
		return false;
	}

}
