package com.jecn.epros.service.org.impl;

import com.github.pagehelper.ISelect;
import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.*;
import com.jecn.epros.domain.InventoryTable.Status;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.inventory.*;
import com.jecn.epros.domain.org.*;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.process.kpi.RefIndicators;
import com.jecn.epros.domain.report.InventoryBaseInfo;
import com.jecn.epros.mapper.InventoryMapper;
import com.jecn.epros.mapper.OrgMapper;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.org.OrgService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.excel.ExcelUtils;
import com.jecn.epros.util.excel.InventoryStyleManager;
import javafx.beans.binding.BooleanBinding;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrgServiceImpl implements OrgService {
    @Autowired
    private OrgMapper orgMapper;

    @Autowired
    private InventoryMapper inventoryMapper;

    @Override
    public List<TreeNode> findChildOrgs(Map<String, Object> map) {
        return TreeNodeUtils.setNodeType(orgMapper.findChildOrgs(map), TreeNodeUtils.TreeType.organization);
    }

    @Override
    public OrgBaseInfoBean findOrgBaseInfoBean(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        return orgMapper.findOrgBaseInfoBean(id);
    }

    @Override
    public PosBaseInfoBean findPosBaseInfoBean(Map<String, Object> map) {
        Long posId = Long.valueOf(map.get("id").toString());
        PosBaseInfoBean posBaseInfoBean = new PosBaseInfoBean();
        posBaseInfoBean.setBaseInfo(orgMapper.findPosBaseCommonBean(posId));
        // 上级岗位、下级岗位、部门内相关制度、部门外相关制度
        posBaseInfoBean.setInnerRules(orgMapper.findInRulePosRelatedBeans(posId));
        posBaseInfoBean.setOuterRules(orgMapper.findOutRulePosRelatedBeans(posId));
        posBaseInfoBean.setSuperPositions(orgMapper.findPubPosPosRelatedBeans(posId));
        posBaseInfoBean.setSubPositions(orgMapper.findSubPosPosRelatedBeans(posId));
        // 岗位相关角色集合
        List<PosResBean> listPosResBean = orgMapper.findPosRelateRole(posId);
        posBaseInfoBean.setRoles(getPosRelatedRoles(listPosResBean));
        return posBaseInfoBean;
    }

    @Override
    public List<PosRelatedRole> getPosRelatedRole(Long posId) {
        List<PosRelatedRole> roles = new ArrayList<PosRelatedRole>();
        // 岗位相关角色集合
        List<PosResBean> listPosResBean = orgMapper.findPosRelateRole(posId);
        ;// 角色职责
        roles.addAll(getPosRelatedRoles(listPosResBean));
        return roles;
    }


    /**
     * 获得岗位职责
     *
     * @param listPosResBean
     * @return
     */
    private List<PosRelatedRole> getPosRelatedRoles(List<PosResBean> listPosResBean) {
        List<PosRelatedRole> listPosRelatedRole = new ArrayList<PosRelatedRole>();
        PosRelatedRole posRelatedRole = null;
        List<PosRelatedActive> listPosRelatedActive = null;
        List<RefIndicators> listRefIndicators = null;
        PosRelatedActive posRelatedActive = null;
        Long activeId = null;
        Long roleId = null;
        for (int i = 0; i < listPosResBean.size(); i++) {
            PosResBean posResBean = listPosResBean.get(i);
            if (roleId == null || !roleId.equals(posResBean.getRoleId())) {
                roleId = posResBean.getRoleId();
                posRelatedRole = new PosRelatedRole();
                posRelatedRole.setRoleName(posResBean.getRoleName());
                posRelatedRole.setRoleRespon(posResBean.getRoleRespon());
                posRelatedRole.setFlowId(posResBean.getFlowId());
                posRelatedRole.setFlowName(posResBean.getFlowName());
                listPosRelatedRole.add(posRelatedRole);
                listPosRelatedActive = new ArrayList<PosRelatedActive>();
                posRelatedRole.setActivities(listPosRelatedActive);
            }
            if (activeId == null || !activeId.equals(posResBean.getActiveId())) {
                posRelatedActive = new PosRelatedActive();
                posRelatedActive.setActiveId(posResBean.getActiveId());
                posRelatedActive.setActiveName(posResBean.getActiveName());
                posRelatedActive.setActiceNum(posResBean.getActiveNum());
                listPosRelatedActive.add(posRelatedActive);
                listRefIndicators = new ArrayList<RefIndicators>();
                posRelatedActive.setListRefIndicators(listRefIndicators);
                activeId = posResBean.getActiveId();
            }

            RefIndicators refIndicators = new RefIndicators();
            refIndicators.setIndicatorName(posResBean.getIndicatorName());
            refIndicators.setIndicatorValue(posResBean.getIndicatorValue());
            listRefIndicators.add(refIndicators);
        }
        return listPosRelatedRole;
    }

    @Override
    public List<SearchOrgResultBean> searchOrg(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "jfo.org_id");
        return orgMapper.searchOrg(map);
    }

    @Override
    public int searchOrgTotal(Map<String, Object> map) {
        return orgMapper.searchOrgTotal(map);
    }

    @Override
    public List<SearchPosResultBean> searchPos(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "figure_id");
        return orgMapper.searchPos(map);
    }

    @Override
    public Inventory searchPosStatistics(Map<String, Object> map) {
        boolean isDownLoad = (map.get("isDownLoad") == null || "".equals(map.get("isDownLoad").toString())) ? true : false;
        List<String> ids = new ArrayList<>();
        if (isDownLoad) {
            Paging paging = (Paging) map.get("paging");
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "figure_id");
            //获取岗位数据 根据岗位Id 查询 相关文件
            List<SearchPosResultBean> post = orgMapper.searchPos(map);
            for (SearchPosResultBean pos : post) {
                if (pos.getFigureId() != null) {
                    ids.add(pos.getFigureId().toString());
                }
            }
            map.put("ids", ids);
        }
        //获取岗位统计数据
        List<SearchPosStatisticsResultBean> rBean = orgMapper.searchPosStatistics(map);
        //根据部门ID  查询部门的父类部门
        for (SearchPosStatisticsResultBean bean : rBean) {
            if (bean.getOrgId() != null) {
                bean.setDutyOrgList(orgMapper.findOrgPaterStrByOrgId(Long.valueOf(bean.getOrgId())));
            }
        }


        // 初始化清单
        Inventory inventory = new Inventory();
        inventory.setStartLevel(1);
        inventory.setTotalNum(searchPosTotal(map));
        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        data.addContentRow(ExcelUtils.getInventoryTitleNoNum(inventory.getStartLevel(), inventory.getMaxLevel(),
                InventoryStyleManager.getPosStatisticsTitles()));
        inventory.setStyleMap(InventoryStyleManager.getPosInventoryStyles(data.getTitileContents()));
        InventoryCell cell = null;

        for (SearchPosStatisticsResultBean pos : rBean) {
            List<InventoryCell> content = new ArrayList<InventoryCell>();
            data.getContents().add(content);
            //岗位
            cell = new InventoryCell();
            List<LinkResource> posList = new ArrayList<LinkResource>();
            posList.add(pos.getLink());
            cell.setLinks(posList);
            content.add(cell);
            //所属部门
            String orgName = Common.getOrgFullPathStr(pos.getOrgName(), pos.getDutyOrgList());
            cell = new InventoryCell();
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(new LinkResource(pos.getOrgId(), orgName, ResourceType.ORGANIZATION));
            cell.setLinks(links);
            content.add(cell);
            // 写出角色
            if (pos.getRoles() != null && pos.getRoles().size() > 0) {
                boolean posIsFirst = true;
                for (PosInventoryRoleBean role : pos.getRoles()) {
                    if (!posIsFirst) {// 角色换行显示
                        content = new ArrayList<InventoryCell>();
                        data.getContents().add(content);
                        fillEmptyCell(2, content, data);
                    }
                    for (FileInventoryCommonBean bean : role.getListFileInventoryCommonBean()) {
                        bean.setFileType(5);
                    }
                    // 填充角色以及后续内容
                    fillRole(role, content, data, 2);
                    posIsFirst = false;
                }
            } else {
                fillEmptyCell(5, content, data);
            }
        }

        return inventory;
    }

    @Override
    public Inventory searchPosGroupStatistics(Map<String, Object> map) {
        boolean isDownLoad = (map.get("isDownLoad") == null || "".equals(map.get("isDownLoad").toString())) ? true : false;
        List<String> ids = new ArrayList<>();
        if (isDownLoad) {
            Paging paging = (Paging) map.get("paging");
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "id");
            //获取岗位组数据 根据岗位Id 查询 相关文件
            List<SearchPosResultBean> postGroup = orgMapper.searchPosGroup(map);
            for (SearchPosResultBean pos : postGroup) {
                if (pos.getFigureId() != null) {
                    ids.add(pos.getFigureId().toString());
                }
            }
            map.put("ids", ids);
        }
        //获取岗位组统计数据
        List<SearchPosGroupStatisticsResultBean> rBean = orgMapper.searchPosGroupStatistics(map);
        // 初始化清单
        Inventory inventory = new Inventory();
        inventory.setStartLevel(1);
        inventory.setTotalNum(searchPosGroupTotal(map));
        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        data.addContentRow(ExcelUtils.getInventoryTitleNoNum(inventory.getStartLevel(), inventory.getMaxLevel(),
                InventoryStyleManager.getPosGroupStatisticsTitles()));
        inventory.setStyleMap(InventoryStyleManager.getPosInventoryStyles(data.getTitileContents()));
        InventoryCell cell = null;

        for (SearchPosGroupStatisticsResultBean posGroup : rBean) {
            List<InventoryCell> content = new ArrayList<InventoryCell>();
            data.getContents().add(content);
            //岗位组
            cell = new InventoryCell();
            cell.setText(posGroup.getFigureName());
            content.add(cell);
            //岗位组
            cell = new InventoryCell();
            cell.setText(posGroup.getExplain());
            content.add(cell);
            //岗位
            cell = new InventoryCell();
            List<LinkResource> posList = new ArrayList<LinkResource>();
            for (PosRelatedBean pos : posGroup.getPosBeans()) {
                posList.add(pos.getLink());
            }
            cell.setLinks(posList);
            content.add(cell);
            // 写出角色
            if (posGroup.getRoles() != null && posGroup.getRoles().size() > 0) {
                boolean posIsFirst = true;
                for (PosInventoryRoleBean role : posGroup.getRoles()) {
                    if (!posIsFirst) {// 角色换行显示
                        content = new ArrayList<InventoryCell>();
                        data.getContents().add(content);
                        fillEmptyCell(3, content, data);
                    }
                    for (FileInventoryCommonBean bean : role.getListFileInventoryCommonBean()) {
                        bean.setFileType(5);
                    }
                    // 填充角色以及后续内容
                    fillRole(role, content, data, 3);
                    posIsFirst = false;
                }
            } else {
                fillEmptyCell(5, content, data);
            }
        }
        return inventory;
    }

    @Override
    public int searchPosTotal(Map<String, Object> map) {
        return orgMapper.searchPosTotal(map);
    }

    @Override
    public int searchPosGroupTotal(Map<String, Object> map) {
        return orgMapper.searchPosGroupTotal(map);
    }

    /**
     * 部门没有相关流程的时候
     *
     * @param rowData
     * @param cell
     * @param data
     */
    private void addRelatedFlowEmpty(List<InventoryCell> rowData, InventoryCell cell, InventoryTable data) {
        // 流程名称
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 流程ID
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 相关关键活动
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 相关KPI
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);

        // 相关流程
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 相关制度
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 相关标准
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 相关风险
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);

        // 责任人
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 监护人
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 拟制人
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 版本号
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 发布日期
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 有效期
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 是否及时优化(流程清单是没有是的，因为不知道将来是否已经优化)
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
        // 下次审视需完成时间
        cell = new InventoryCell();
        cell.setText("");
        rowData.add(cell);
    }

    /**
     * 添加相关流程
     *
     * @param content
     * @param process
     * @param cell
     * @param inventory
     */
    private void addRelatedFlow(List<InventoryCell> content, OrgInventoryRelatedBean process, Inventory inventory,
                                InventoryCell cell, InventoryTable data, List<FileInventoryRelateBean> activitys,
                                List<FileInventoryRelateBean> KPIs, List<FileInventoryRelateBean> impFlows,
                                List<FileInventoryRelateBean> rules, List<FileInventoryRelateBean> standards,
                                List<FileInventoryRelateBean> risks) {
        cell = new InventoryCell();
        cell.setText(process.getFlowName());
        if (process.getPubState() == 1) {// 发布的有链接，未发布的没有
            LinkResource link = new LinkResource(process.getFlowId(), process.getFlowName(), ResourceType.PROCESS);
            List<LinkResource> links = new ArrayList<LinkResource>();
            links.add(link);
            cell.setLinks(links);
        }
        if (process.getTypeByData() == 0) {
            cell.setStatus(Status.notPub);
        } else if (process.getTypeByData() == 1) {
            cell.setStatus(Status.approve);
        } else if (process.getTypeByData() == 2) {
            cell.setStatus(Status.pub);
        }
        content.add(cell);
        cell = new InventoryCell();
        cell.setText(process.getFlowIdInput());
        content.add(cell);
        cell = new InventoryCell();
        cell.setText(process.getOrgType());
        content.add(cell);
        // 关键活动
        List<LinkResource> relatedLinks = Common.getFileInventoryCommonBeans(process.getFlowId(), activitys);
        cell = new InventoryCell();
        cell.setLinks(relatedLinks);
        content.add(cell);
        // KPI
        relatedLinks = Common.getFileInventoryCommonBeans(process.getFlowId(), KPIs);
        cell = new InventoryCell();
        cell.setLinks(relatedLinks);
        content.add(cell);

        List<LinkResource> relateds = new ArrayList<>();

        if (process.getTypeByData() != 0) {
            // 相关流程
            relateds.addAll(Common.getFileInventoryCommonBeans(process.getFlowId(), impFlows));
            // 相关制度
            relateds.addAll(Common.getFileInventoryCommonBeans(process.getFlowId(), rules));
            // 相关标准
            relateds.addAll(Common.getFileInventoryCommonBeans(process.getFlowId(), standards));
            // 相关风险
            relateds.addAll(Common.getFileInventoryCommonBeans(process.getFlowId(), risks));
        }

        cell = new InventoryCell();
        cell.setLinks(relateds);
        content.add(cell);

        // 责任人
        cell = new InventoryCell();
        cell.setText(IfytekCommon.getIflytekUserName(process.getResPeople(), 0));
        content.add(cell);
        // 监护人
        cell = new InventoryCell();
        cell.setText(IfytekCommon.getIflytekUserName(process.getGuardianPeople(), 0));
        content.add(cell);
        // 拟制人
        cell = new InventoryCell();
        cell.setText(IfytekCommon.getIflytekUserName(process.getDraftPeople(), 0));
        content.add(cell);
        // 版本号
        cell = new InventoryCell();
        cell.setText(process.getVersionId());
        content.add(cell);
        // 发布日期
        cell = new InventoryCell();
        if (process.getTypeByData() != 0 && process.getPubTime() != null) {
            cell.setText(Common.getStringbyDate(process.getPubTime()));
        } else {
            cell.setText("");
        }
        content.add(cell);
        // 有效期
        cell = new InventoryCell();
        String periodOfValidity = "";
        if (process.getTypeByData() != 0) {// 未发布的不能出现有效期和是否及时优化
            if (process.getExpiry() != null && process.getExpiry().intValue() == 0) {
                periodOfValidity = JecnProperties.getValue("permanent");//永久
            } else if (process.getExpiry() != null && process.getExpiry().intValue() > 0) {
                periodOfValidity = process.getExpiry() + JecnProperties.getValue("month");//月
            }
        }
        cell.setText(periodOfValidity);
        content.add(cell);
        // 是否及时优化(流程清单是没有是的，因为不知道将来是否已经优化)
        cell = new InventoryCell();
        String optimization = "";
        if (process.getTypeByData() != 0) {
            if (!process.isSurvey()) {
                optimization = JecnProperties.getValue("no");//否
            }
        }
        cell.setText(optimization);
        content.add(cell);
        // 下次审视需完成时间
        cell = new InventoryCell();
        if (process.getExpiry() != null && process.getExpiry().intValue() != 0 && process.getNextScanDate() != null) {
            cell.setText(Common.getStringbyDate(process.getNextScanDate()));
        } else {
            cell.setText("");
        }
        content.add(cell);
    }

    private void addRelatedFlowOnlyRow(OrgInventoryRelatedBean process, Inventory inventory, InventoryCell cell,
                                       InventoryTable data, List<FileInventoryRelateBean> activitys, List<FileInventoryRelateBean> KPIs,
                                       List<FileInventoryRelateBean> impFlows, List<FileInventoryRelateBean> rules,
                                       List<FileInventoryRelateBean> standards, List<FileInventoryRelateBean> risks) {

        List<InventoryCell> rowData = new ArrayList<InventoryCell>();
        for (int i = inventory.getStartLevel(); i <= inventory.getMaxLevel(); i++) {
            cell = new InventoryCell();
            cell.setText("");
            rowData.add(cell);
        }
        this.addRelatedFlow(rowData, process, inventory, cell, data, activitys, KPIs, impFlows, rules, standards,
                risks);
        data.getContents().add(rowData);
    }

    @Override
    public Inventory getInventory(Map<String, Object> paramMap) {

        Boolean isPaging = (Boolean) paramMap.get("isPaging");
        List<OrgInventoryBean> orgs = new ArrayList<>();
        if (isPaging) {
            // 部门清单的分页逻辑是将部门分页
            Paging paging = (Paging) paramMap.get("paging");
            // 通过view_sort分页查询
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "view_sort");
            List<String> paths = inventoryMapper.fetchPaths(paramMap);
            if (paths != null && paths.size() > 0) {
                Set<String> idSet = new HashSet<>();
                for (String path : paths) {
                    String[] idArr = path.split("-");
                    idSet.add(idArr[idArr.length - 1]);
                }
                String ids = "(" + idSet.stream().collect(Collectors.joining(",")) + ")";
                paramMap.put("ids", ids);
                orgs = orgMapper.fetchInventory(paramMap);
            }
        } else {
            // 获得清单数据,经过view_sort排序的数据，包含分页的数据以及他到level0的所有父节点
            orgs = orgMapper.fetchInventory(paramMap);
        }

        // 其中的级别为部门的最高级别,数量是部门的数量
        InventoryBaseInfo baseInfo = inventoryMapper.getInventoryBaseInfo(paramMap);
        InventoryBaseInfo flowBaseInfo = inventoryMapper.getInventoryOrgFlowsBaseInfo(paramMap);
        // 初始化清单
        Inventory inventory = new Inventory();
        OrgBaseInfoBean startOrg = orgMapper.findOrgBaseInfoBean(Long.valueOf(paramMap.get("orgId").toString()));
        inventory.setStartLevel(startOrg.getLevel());
        inventory.setMaxLevel(baseInfo.getMaxLevel());
        inventory.setOrgNum(baseInfo.getTotal());

        if (flowBaseInfo != null) {
            inventory.setTotalNum(flowBaseInfo.getTotal());
            inventory.setApproveNum(flowBaseInfo.getApprove());
            inventory.setPubNum(flowBaseInfo.getPub());
            inventory.setNotPubNum(flowBaseInfo.getNotPub());
        } else {
            inventory.setTotalNum(0);
            inventory.setApproveNum(0);
            inventory.setPubNum(0);
            inventory.setNotPubNum(0);
        }

        List<FileInventoryRelateBean> activitys = new ArrayList<>();
        // 责任部门下流程的KPI
        List<FileInventoryRelateBean> KPIs = new ArrayList<>();
        // 责任部门下流程的接口流程
        List<FileInventoryRelateBean> impFlows = new ArrayList<>();
        // 责任部门下流程的制度
        List<FileInventoryRelateBean> rules = new ArrayList<>();
        // 责任部门下流程的标准
        List<FileInventoryRelateBean> standards = new ArrayList<>();
        // 责任部门下流程的标准
        List<FileInventoryRelateBean> risks = new ArrayList<>();
        if (orgs.size() > 0) {
            // 责任部门下流程的关键活动
            activitys = inventoryMapper.findOrgRelatedActivity(paramMap);
            // 责任部门下流程的KPI
            KPIs = inventoryMapper.findOrgRelatedKPI(paramMap);
            // 责任部门下流程的接口流程
            impFlows = inventoryMapper.findOrgRelatedProcess(paramMap);
            // 责任部门下流程的制度
            rules = inventoryMapper.findOrgRelatedRule(paramMap);
            // 责任部门下流程的标准
            standards = inventoryMapper.findOrgRelatedStandard(paramMap);
            // 责任部门下流程的风险
            risks = inventoryMapper.findOrgRelatedRisk(paramMap);
        }

        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        data.addContentRow(ExcelUtils.getInventoryTitleNoNum(inventory.getStartLevel(), inventory.getMaxLevel(),
                InventoryStyleManager.getOrgTitles()));
        inventory.setStyleMap(InventoryStyleManager.getOrgInventoryStyles(data.getTitileContents()));
        List<InventoryCell> rowData = null;
        InventoryCell cell = null;
        if (ConfigUtils.isIflytekLogin()) {
            List<OrgInventoryRelatedBean> allOrgRelateds = new ArrayList<>();
            for (OrgInventoryBean org : orgs) {
                List<OrgInventoryRelatedBean> flows = org.getListOrgInventoryRelatedBean();
                allOrgRelateds.addAll(flows);
            }
            //科大讯飞根据 登录名重新查询人员姓名
            IfytekCommon.ifytekCommonGetOrgUserName(allOrgRelateds);
        }
        for (OrgInventoryBean org : orgs) {
            rowData = new ArrayList<InventoryCell>();
            // 第一行
            for (int i = inventory.getStartLevel(); i <= inventory.getMaxLevel(); i++) {
                if (i == org.gettLevel()) {
                    cell = new InventoryCell();
//                    cell.setText(org.getOrgName());
                    LinkResource link = new LinkResource(org.getOrgId(), org.getOrgName(), ResourceType.ORGANIZATION);
                    List<LinkResource> links = new ArrayList<LinkResource>();
                    links.add(link);
                    cell.setLinks(links);
                    rowData.add(cell);
                } else {
                    cell = new InventoryCell();
                    cell.setText("");
                    rowData.add(cell);
                }
            }

            List<OrgInventoryRelatedBean> flows = org.getListOrgInventoryRelatedBean();

            if (flows != null && flows.size() > 0) {
                OrgInventoryRelatedBean process = flows.get(0);
                this.addRelatedFlow(rowData, process, inventory, cell, data, activitys, KPIs, impFlows, rules,
                        standards, risks);
            } else {
                this.addRelatedFlowEmpty(rowData, cell, data);
            }

            // 后面的行数
            data.getContents().add(rowData);
            if (flows != null && flows.size() > 1) {
                for (int i = 1; i < flows.size(); i++) {
                    OrgInventoryRelatedBean process = flows.get(i);
                    this.addRelatedFlowOnlyRow(process, inventory, cell, data, activitys, KPIs, impFlows, rules, standards, risks);
                }
            }
        }
        return inventory;
    }




    private int getStartLevel(List<OrgInventoryBean> orgs, Long id) {
        for (OrgInventoryBean org : orgs) {
            if (id.equals(org.getOrgId())) {
                return org.gettLevel();
            }
        }
        return 0;
    }

    @Override
    public Inventory getPosInventory(Map<String, Object> paramMap) {
        Boolean isPaging = (Boolean) paramMap.get("isPaging");
        List<PosInventoryOrgBean> orgs = new ArrayList<>();
        if (isPaging) {
            // 岗位清单按照部门分页
            Paging paging = (Paging) paramMap.get("paging");
            // 通过view_sort分页查询
            PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "view_sort");
            List<String> paths = inventoryMapper.fetchPaths(paramMap);
            if (paths != null && paths.size() > 0) {
                Set<String> idSet = new HashSet<>();
                for (String path : paths) {
                    String[] idArr = path.split("-");
                    idSet.add(idArr[idArr.length - 1]);
                }
                String ids = "(" + idSet.stream().collect(Collectors.joining(",")) + ")";
                paramMap.put("ids", ids);
                orgs = inventoryMapper.findPosOrgs(paramMap);
            }
        } else {
            // 获得清单数据,经过view_sort排序的数据，包含分页的数据以及他到level0的所有父节点
            orgs = inventoryMapper.findPosOrgs(paramMap);
        }

        // 其中的级别为部门的最高级别,数量是部门的数量
        InventoryBaseInfo baseInfo = inventoryMapper.getInventoryBaseInfo(paramMap);
        // 初始化清单
        Inventory inventory = new Inventory();
        inventory.setTotalNum(baseInfo.getTotal());
        OrgBaseInfoBean startOrg = orgMapper.findOrgBaseInfoBean(Long.valueOf(paramMap.get("orgId").toString()));
        inventory.setStartLevel(startOrg.getLevel());
        inventory.setMaxLevel(baseInfo.getMaxLevel());

        List<PosInventoryRelatedRoleBean> findPosRoleAndActive = new ArrayList<>();
        if (orgs.size() > 0) {
            findPosRoleAndActive = inventoryMapper.findPosRoleAndActive(paramMap);
        }
        Map<Long, List<JecnUser>> user = getPosUser();
        //获取部门岗位下所有的人员
        for (PosInventoryOrgBean org : orgs) {
            for (PosInventoryPosBean pos : org.getListPosInventoryPosBean()) {
                if (user.containsKey(pos.getPosId())) {
                    pos.setPosUser(user.get(pos.getPosId()));
                }
            }
        }
        initInventoryContent(orgs, isPaging, inventory, findPosRoleAndActive);

        return inventory;
    }



    //获取所有岗位下人员
    protected Map<Long, List<JecnUser>> getPosUser() {
        Map<Long, List<JecnUser>> map = new HashedMap();
        List<JecnUser> pepole = inventoryMapper.findPosUser();
        for (JecnUser user : pepole) {
            if (map.containsKey(user.getFigureId())) {
                map.get(user.getFigureId()).add(user);
            } else {
                List<JecnUser> user1 = new ArrayList<>();
                user1.add(user);
                map.put(user.getFigureId(), user1);
            }
        }
        return map;
    }


    private int getPosStartLevel(List<PosInventoryOrgBean> orgs, Long id) {
        for (PosInventoryOrgBean org : orgs) {
            if (id.equals(org.getOrgId())) {
                return org.getLevel();
            }
        }
        return 0;
    }

    private Inventory initInventoryContent(List<PosInventoryOrgBean> orgs, boolean isPaging, Inventory inventory,
                                           List<PosInventoryRelatedRoleBean> listRelateFlows) {
        // 将岗位id作为key 关联的角色作为value
        Map<Long, List<PosInventoryRoleBean>> posIdRelatedRoleMap = new HashMap<Long, List<PosInventoryRoleBean>>();
        if (listRelateFlows != null && listRelateFlows.size() > 0) {
            for (PosInventoryRelatedRoleBean relatedRole : listRelateFlows) {
                posIdRelatedRoleMap.put(relatedRole.getPosId(), relatedRole.getListPosInventoryRoleBean());
            }
        }

        InventoryTable data = new InventoryTable();
        inventory.setData(data);
        data.addContentRow(ExcelUtils.getInventoryTitleNoNum(inventory.getStartLevel(), inventory.getMaxLevel(),
                InventoryStyleManager.getPosTitles()));
        inventory.setStyleMap(InventoryStyleManager.getPosInventoryStyles(data.getTitileContents()));
        InventoryCell cell = null;

        // 替换部门列的空格的数量
        int orgFillNum = inventory.getMaxLevel() - inventory.getStartLevel() + 1;
        // 替换部门和岗位的空格的数量
        int posFillNum = orgFillNum + 2;
        // 部门后没有岗位替换的空格的数量
        int noPosFillNum = 7;
        // 岗位没有角色替换的空格的数量
        int noRoleFillNum = 5;
        // 先写出部门然后写出该部门下的岗位然后写出该岗位下的角色
        for (PosInventoryOrgBean org : orgs) {
            List<InventoryCell> content = new ArrayList<InventoryCell>();
            data.getContents().add(content);

            for (int i = inventory.getStartLevel(); i <= inventory.getMaxLevel(); i++) {
                if (org.getLevel() == i) {
                    cell = new InventoryCell();
                    cell.setText(org.getOrgName());
                    content.add(cell);
                } else {
                    cell = new InventoryCell();
                    cell.setText("");
                    content.add(cell);
                }
            }
            // 单个部门下的岗位
            if (org.getListPosInventoryPosBean() != null && org.getListPosInventoryPosBean().size() > 0) {
                boolean orgIsFirst = true;
                for (PosInventoryPosBean pos : org.getListPosInventoryPosBean()) {
                    if (!orgIsFirst) {// 岗位换行显示
                        content = new ArrayList<InventoryCell>();
                        data.getContents().add(content);
                        fillEmptyCell(orgFillNum, content, data);
                    }
                    // 岗位名称
                    cell = new InventoryCell();
//                    cell.setText(pos.getPosName());
                    LinkResource link = new LinkResource(pos.getPosId(), pos.getPosName(), ResourceType.POSITION);
                    List<LinkResource> links = new ArrayList<LinkResource>();
                    links.add(link);
                    cell.setLinks(links);
                    content.add(cell);
                    if (pos.getPosUser() != null && !pos.getPosUser().isEmpty()) {
                        IfytekCommon.ifytekCommonGetPosUserName(pos.getPosUser());
                        cell = new InventoryCell();
                        List<LinkResource> uLinks = new ArrayList<LinkResource>();
                        for (JecnUser user : pos.getPosUser()) {
                            LinkResource uLink = new LinkResource(user.getId(), user.getName(), ResourceType.PERSON);
                            uLinks.add(uLink);
                        }
                        cell.setLinks(uLinks);
                        content.add(cell);
                    } else {
                        fillEmptyCell(1, content, data);
                    }
                    List<PosInventoryRoleBean> posRelatedRole = posIdRelatedRoleMap.get(pos.getPosId());
                    // 写出角色
                    if (posRelatedRole != null && posRelatedRole.size() > 0) {
                        boolean posIsFirst = true;
                        for (PosInventoryRoleBean role : posRelatedRole) {
                            if (!posIsFirst) {// 角色换行显示
                                content = new ArrayList<InventoryCell>();
                                data.getContents().add(content);
                                fillEmptyCell(posFillNum, content, data);
                            }
                            // 填充角色以及后续内容
                            fillRole(role, content, data, posFillNum);

                            posIsFirst = false;
                        }
                    } else {
                        fillEmptyCell(noRoleFillNum, content, data);
                    }
                    orgIsFirst = false;
                }
            } else {
                fillEmptyCell(noPosFillNum, content, data);
            }
        }

        return inventory;
    }

    private void fillRole(PosInventoryRoleBean role, List<InventoryCell> content, InventoryTable data, int posFillNum) {
        // 写出角色以及后续内容
        InventoryCell cell = new InventoryCell();
        cell.setText(role.getRoleName());
        content.add(cell);

        cell = new InventoryCell();
        cell.setText(StringUtils.isBlank(role.getRoleRes()) ? "" : role.getRoleRes());
        content.add(cell);

        // 关联的流程
        cell = new InventoryCell();
//        cell.setText(role.getFlowName());
        LinkResource link = new LinkResource(role.getFlowId(), role.getFlowName(), ResourceType.PROCESS);
        List<LinkResource> links = new ArrayList<LinkResource>();
        links.add(link);
        cell.setLinks(links);
        content.add(cell);
        // 相关活动
        cell = new InventoryCell();
        StringBuffer timeLine = new StringBuffer();
        if (role.getListFileInventoryCommonBean() != null && role.getListFileInventoryCommonBean().size() > 0) {
            List<LinkResource> relatedLinks = null;
            boolean flowIsFirst = true;
            for (FileInventoryCommonBean activitys : role.getListFileInventoryCommonBean()) {
                relatedLinks = new ArrayList<>();
                if (activitys.getFileId() != null) {
                    if (!flowIsFirst) {// 活动换行显示
                        content = new ArrayList<InventoryCell>();
                        data.getContents().add(content);
                        fillEmptyCell(posFillNum + 3, content, data);
                    }
                    cell = new InventoryCell();
                    relatedLinks.add(activitys.getLink());
                    cell.setLinks(relatedLinks);
                    content.add(cell);

                    // 办理时限
                    cell = new InventoryCell();
                    if (StringUtils.isNotBlank(activitys.getTargetValue()) || StringUtils.isNotBlank(activitys.getStatusValue())) {
                        cell.setText((activitys.getTargetValue() == null ? "" : activitys.getTargetValue()) + "/" + (activitys.getStatusValue() == null ? "" : activitys.getStatusValue()));
                    }
                    content.add(cell);
                    flowIsFirst = false;
                } else {
                    fillEmptyCell(2, content, data);
                }
            }
        } else {
            fillEmptyCell(2, content, data);
        }
    }

    private void fillEmptyCell(int orgFillNum, List<InventoryCell> content, InventoryTable data) {
        InventoryCell cell = null;
        for (int i = 0; i < orgFillNum; i++) {
            cell = new InventoryCell();
            cell.setText("");
            content.add(cell);
        }

    }

}
