package epros.designer.gui.popedom.person;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.UserAddReturnBean;
import com.jecn.epros.server.bean.popedom.UserEditBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/***
 * 更新用户
 * 
 * @author 2012-05-23
 * 
 */
public class EditPersonDialog extends PersonDialog {

	private static Logger log = Logger.getLogger(EditPersonDialog.class);
	private PersonManageDialog personManageDialog = null;
	private JecnUser user;
	private int selectRow;

	public EditPersonDialog(PersonManageDialog personManageDialog, int selectRow) {
		this.selectRow = selectRow;
		
		// 更新用户
		this.setTitle(JecnProperties.getValue("updateUser"));
		this.setLocationRelativeTo(null);
		this.personManageDialog = personManageDialog;

		Long id = (Long) personManageDialog.getPersonTable().getModel().getValueAt(
				personManageDialog.getPersonTable().getSelectedRow(), 0);

		initComponents();
		try {
			UserEditBean usereditBean = ConnectionPool.getPersonAction().getUserEidtBeanByPeopleId(id);
			user = usereditBean.getJecnUser();
			this.posList = usereditBean.getPosList();
			this.roleList = usereditBean.getRoleList();
			this.loginNameField.setText(user.getLoginName().trim());
			this.emailType = user.getEmailType();
			this.realNameField.setText(user.getTrueName().trim());
			this.departField.setText(usereditBean.getOrgNames());
			positionIds = JecnDesignerCommon.getLook(posList, posField);
			systemRoleIds = JecnDesignerCommon.getLook(roleList, systemRoleField);
			this.pwdField.setText(JecnUtil.getDesecret(user.getPassword()));
			this.emailField.setText(user.getEmail());
			this.phoneField.setText(user.getPhoneStr());
			managerTreeBean.setId(user.getManagerId());
			managerTreeBean.setName(user.getManagerName());
			managerTreeBean.setTreeNodeType(TreeNodeType.person);
			if (user.getEmailType() == null || user.getEmailType().equals("inner")) {
				this.inRadioBut.setSelected(true);
				this.inRadioBut.doClick();
			} else {
				this.outRadioBut.setSelected(true);
				this.outRadioBut.doClick();
			}
			if (user.getIsLock() == 0) {
				this.unlockRadioBut.setSelected(true);
				this.unlockRadioBut.doClick();
			} else {
				this.lockRadioBut.setSelected(true);
				this.lockRadioBut.doClick();
			}
		} catch (Exception e) {
			log.error("EditPersonDialog is error！", e);
		}

		initLayout();
		actionInit();

	}

	@Override
	public void saveData() {
		try {
			List<Long> adminIdList = ConnectionPool.getPersonAction().getAllAdminList();
			if (adminIdList != null && adminIdList.size() == 1) {
				if (adminIdList.get(0).equals(user.getPeopleId())) {
					boolean flag = false;
					for (JecnTreeBean treeBean : roleList) {
						if (treeBean.getNumberId() != null && treeBean.getNumberId().equals(JecnConstants.ADMIN)) {
							flag = true;
							break;
						}
					}
					if (!flag) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("atLeastAdmin"));
						return;
					}
				}
			}
		} catch (Exception e1) {
			log.error("EditPersonDialog saveData is error", e1);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return;
		}
		user.setLoginName(this.loginNameField.getText().trim());
		user.setPassword(new String(pwdField.getPassword()).trim());
		user.setTrueName(this.realNameField.getText().trim());
		user.setEmail(this.emailField.getText().trim());
		// user.setIsLock(isLock);
		// user.setMacAddress("");
		user.setEmailType(emailType);
		user.setPhoneStr(this.phoneField.getText());
		user.setManagerId(selectCommon.getIdResult());
		try {
			UserAddReturnBean userAddReturnBean = ConnectionPool.getPersonAction().updatePerson(user, positionIds,
					systemRoleIds);
			if (userAddReturnBean.getType() == 1) {
				// 角色人数已达上限!
				this.systemEmpLab.setText(JecnProperties.getValue("roleOver"));
				return;
			} else if (userAddReturnBean.getType() == 2) {
				// 二级管理员总数已达上限！
				this.systemEmpLab.setText(JecnProperties.getValue("secondAdminRoleOver"));
				return;
			} else if (userAddReturnBean.getType() == 3) {
				// 管理员总数已达上限！
				this.systemEmpLab.setText(JecnProperties.getValue("adminRoleOver"));
				return;
			}
			((DefaultTableModel) this.personManageDialog.getPersonTable().getModel()).setValueAt(this.loginNameField
					.getText().trim(), selectRow, 1);
			((DefaultTableModel) this.personManageDialog.getPersonTable().getModel()).setValueAt(this.realNameField
					.getText().trim(), selectRow, 2);
			((DefaultTableModel) this.personManageDialog.getPersonTable().getModel()).setValueAt(this.emailField
					.getText().trim(), selectRow, 3);
			// 如果更新的是自已，则更新登录信息
			if (user.getPeopleId() == JecnConstants.getUserId()) {
				user.setPassword(userAddReturnBean.getPassword());
				JecnConstants.loginBean.setJecnUser(user);
			}
			this.dispose();
		} catch (Exception e) {
			log.error("EditPersonDialog saveData is error！", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}
	}

	@Override
	public boolean validateLoginName(String loginName) {
		try {
			return ConnectionPool.getPersonAction().validateUpdateLoginName(loginName, user.getPeopleId());
		} catch (Exception e) {
			log.error("EditPersonDialog validateLoginName is error！", e);
		}
		return false;
	}
}
