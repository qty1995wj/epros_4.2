package com.jecn.epros.server.bean.autoCode;

import java.io.Serializable;

/**
 * 标准自动编号 - 编号数据对象
 * 
 * @author xiaohu
 * 
 */
public class AutoCodeNodeData implements Serializable {
	private Long nodeId;
	/** 节点类型 0:流程、1：文件、2：制度 */
	private int nodeType;
	/** 文件类型 默认 0 表单，1 ：操作规范 */
	private int fileType;
	/** 维护支撑文件-目录对应的节点编号 */
	private String standardizedFileCode;
	/** 节点编号**/
	private String NodeCode;
	
	

	public String getNodeCode() {
		return NodeCode;
	}

	public void setNodeCode(String nodeCode) {
		NodeCode = nodeCode;
	}

	public Long getNodeId() {
		return nodeId;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	public String getStandardizedFileCode() {
		return standardizedFileCode;
	}

	public void setStandardizedFileCode(String standardizedFileCode) {
		this.standardizedFileCode = standardizedFileCode;
	}
	

}
