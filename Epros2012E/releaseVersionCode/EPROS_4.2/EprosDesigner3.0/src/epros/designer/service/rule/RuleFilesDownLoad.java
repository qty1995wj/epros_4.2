package epros.designer.service.rule;

import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.system.data.JecnSystemStaticData;

public class RuleFilesDownLoad {
	public static boolean ruleFilesDown(Long pid, String pName) {
		String isQuery = null;
		RuleFilesDownLoadWorker rileFilesDown = new RuleFilesDownLoadWorker();
		rileFilesDown.setPid(pid);
		rileFilesDown.setpName(pName);
		String error = JecnLoading.start(rileFilesDown);

		if (error != null) {
			return false;
		}
		try {
			isQuery = rileFilesDown.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		if ("false".equals(isQuery)) {
			JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties.getValue("ruleFileDownError"));
			return false;
		}
		return true;
	}

}

class RuleFilesDownLoadWorker extends SwingWorker<String, Void> {
	private Long pid;
	private String pName;

	@Override
	protected String doInBackground() throws Exception {
		String isQuery = new RuleFilesDown().downloadSelectProcess(pid, pName);
		return isQuery;
	}

	@Override
	public void done() {
		JecnLoading.stop();
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

}
