package com.jecn.epros.server.action.web.login.ad.epson;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.action.web.login.ad.lianhedianzi.LdapAuthVerifier;

/**
 * 爱普生单点登录
 * 
 * @author xiaohu
 * 
 */
public class EpsonLoginAction extends JecnAbstractADLoginAction {
	static {
		JecnEpsonItem.start();
	}

	private LdapAuthVerifier ldapAuthVerifier;

	public EpsonLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		// 单点登录名称
		String adLoginName = this.loginAction.getLoginName();
		// 单点登录密码
		String adPassWord = this.loginAction.getPassword();

		log.info(" adLoginName = " + adLoginName + " adPassWord = " + adPassWord);
		if (StringUtils.isNotBlank(adLoginName) && StringUtils.isNotBlank(adPassWord)) {
			return loginAction.userLoginGen(true);
		}

		// Assertion assertion = (Assertion)
		// loginAction.getRequest().getSession().getAttribute(
		// AbstractCasFilter.CONST_CAS_ASSERTION);
		// Assertion assertion = AssertionHolder.getAssertion();
		// String loginName = "";
		// if (assertion != null) {
		// loginName = assertion.getPrincipal().getName();
		// log.info("sesson 获取用户 = " + loginName);
		// }
		if (StringUtils.isBlank(adLoginName)) {
			adLoginName = this.loginAction.getRequest().getRemoteUser();
		}
		if (StringUtils.isBlank(adLoginName)) {
			return LoginAction.INPUT;
		}

		return this.loginByLoginName(adLoginName);
	}

	/**
	 * 判断用户是在域内
	 * 
	 * @param loginName
	 * @return
	 * @throws Exception
	 */
	private Object doDomainAuth(String loginName) throws Exception {
		return searchInformation("" + JecnEpsonItem.BASE, JecnEpsonItem.FILTER, loginName);
	}

	private Object searchInformation(String base, String filter, String loginName) throws Exception {
		LdapContext ctx = getLdapContext();
		SearchControls sc = new SearchControls();
		sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
		// sc.setReturningAttributes(LianHeDianZiAfterItem.DATACOLUMNS.split(","));
		NamingEnumeration<SearchResult> ne = ctx.search(base, filter, sc);

		String domainName = JecnEpsonItem.DOMAIN_NAME == null ? "" : JecnEpsonItem.DOMAIN_NAME;
		while (ne.hasMore()) {
			SearchResult sr = ne.next();
			log.info("sr.getName() = " + sr.getName());
			// System.out.println("sr.getName() = " + sr.getName());
			Attributes attrs = sr.getAttributes();
			NamingEnumeration<? extends Attribute> enumeration = attrs.getAll();
			String fullName = "";
			String userNum = "";
			while (enumeration.hasMoreElements()) {
				Attribute attribute = (Attribute) enumeration.nextElement();
				if (attribute == null) {
					continue;
				}
				if ("sAMAccountName".equals(attribute.getID())) {
					fullName = attribute.get() == null ? "" : attribute.get().toString();
				}
				if ("physicalDeliveryOfficeName".equals(attribute.getID())) {
					userNum = attribute.get() == null ? "" : attribute.get().toString();
				}

				if (StringUtils.isNotBlank(userNum) && StringUtils.isNotBlank(fullName)) {
					log.info("fullName = " + fullName);
					log.info("userNum = " + userNum + "  loginName = " + loginName);
					if ((domainName + fullName + JecnEpsonItem.DOMAIN).equals(loginName)) {
						return userNum;
					}
				}
			}
		}
		ctx.close();
		return null;
	}

	/**
	 * 管理员账号认证
	 * 
	 * @return
	 * @throws NamingException
	 */
	private LdapContext getLdapContext() throws NamingException {
		return ldapAuthVerifier.verifyAndGetContext();
	}

	private LdapAuthVerifier getLdapAuthVerifier(String username, String password, String ldapServiceUrl) {
		return LdapAuthVerifier.newInstance(username, password, ldapServiceUrl);
	}

}
