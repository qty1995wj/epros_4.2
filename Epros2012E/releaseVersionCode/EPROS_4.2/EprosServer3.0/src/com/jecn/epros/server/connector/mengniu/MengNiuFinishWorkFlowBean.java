package com.jecn.epros.server.connector.mengniu;

public class MengNiuFinishWorkFlowBean {
	/** 第三方流程实例编号或单据流水号 */
	private String EngineInstanceID;
	private String InstanceResult;
	/** OA中流程唯一编码,集成双方约定好的编码 */
	private String ProcessCode;
	/** 流程结束信息备注 */
	private String Remark;

	public String getEngineInstanceID() {
		return EngineInstanceID;
	}

	public void setEngineInstanceID(String engineInstanceID) {
		EngineInstanceID = engineInstanceID;
	}

	public String getProcessCode() {
		return ProcessCode;
	}

	public void setProcessCode(String processCode) {
		ProcessCode = processCode;
	}

	public String getRemark() {
		return Remark;
	}

	public void setRemark(String remark) {
		Remark = remark;
	}

	public String getInstanceResult() {
		return InstanceResult;
	}

	public void setInstanceResult(String instanceResult) {
		InstanceResult = instanceResult;
	}

}
