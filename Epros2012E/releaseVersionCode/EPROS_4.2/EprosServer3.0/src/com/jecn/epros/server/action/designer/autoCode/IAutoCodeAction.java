package com.jecn.epros.server.action.designer.autoCode;

import java.util.List;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;
import com.jecn.epros.server.bean.autoCode.AutoCodeRelatedBean;
import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.bean.autoCode.AutoCodeNodeData;
import com.jecn.epros.server.bean.autoCode.ProcessCodeTree;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

public interface IAutoCodeAction {
	void deleteProcessCode(List<Long> ids) throws Exception;

	void updateName(Long id, String newName, Long updatePersonId) throws Exception;

	Long addPorcessCode(ProcessCodeTree codeTree) throws Exception;

	List<JecnTreeBean> getChildProcessCodes(Long pId, int treeType) throws Exception;

	/**
	 * 自动编号保存
	 * 
	 * @param codeBean
	 * @param codeRelateds
	 * @param relatedType
	 *            0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * @throws Exception
	 */
	AutoCodeBean saveAutoCode(AutoCodeBean codeBean, Long id, int relatedType) throws Exception;

	AutoCodeBean getAutoCodeBean(Long relatedId, int relatedType) throws Exception;

	JecnTreeBean getTreeBeanByName(String name) throws Exception;

	AutoCodeRelatedBean getAutoCodeRelatedBean(Long relatedId, int relatedType) throws Exception;

	List<JecnTreeBean> getPnodes(Long id, int type) throws Exception;

	AutoCodeResultData getStandardizedAutoCode(AutoCodeNodeData codeTempData) throws Exception;

	boolean reNumItemItemAction(AutoCodeNodeData nodeData, long userId);
}
