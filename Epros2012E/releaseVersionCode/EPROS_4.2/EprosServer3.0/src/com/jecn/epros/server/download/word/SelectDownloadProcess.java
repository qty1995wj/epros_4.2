package com.jecn.epros.server.download.word;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.JecnFileBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.process.IProcessKPIDao;
import com.jecn.epros.server.dao.process.IProcessRecordDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.webBean.WebLoginBean;

public class SelectDownloadProcess {
	private static final Logger log = Logger.getLogger(SelectDownloadProcess.class);

	private static String processFiles = "process files";
	// 获取临时文件目录
	private static String filePath = JecnFinal.getAllTempPath(JecnFinal.FILE_DIR + JecnFinal.TEMP_DIR);
	// 获取文件存储路径
	private static String docPath = filePath + processFiles;

	/**
	 * 流程操作说明批量下载
	 * 
	 * @author fuzhh 2013-9-2
	 * @param processIdList
	 */
	public static String downloadProcess(List<Long> processIdList, IJecnConfigItemDao configItemDao,
			IJecnDocControlDao docControlDao, IFlowStructureDao flowStructureDao, IProcessBasicInfoDao processBasicDao,
			IProcessKPIDao processKPIDao, IProcessRecordDao processRecordDao, boolean isPub, boolean isDownFile) {
		// 压缩文件路径
		String zipPath = docPath + ".zip";
		File zipFile = new File(zipPath);
		if (zipFile.exists()) {
			zipFile.delete();
		}
		// 删除文件下所有的文件和文件夹
		deleteDirectory(docPath);

		for (long flowId : processIdList) {
			// 获取流程操作说明信息
			ProcessDownloadBean processDownloadBean;
			try {
				processDownloadBean = DownloadUtil.getProcessDownloadBean(flowId, isPub, configItemDao, docControlDao,
						flowStructureDao, processBasicDao, processKPIDao, processRecordDao);
				// 判断文件路径是否存在，不存在创建
				String docFileDirectory = docPath + File.separator + processDownloadBean.getFlowName();
				File docDirectoryFile = new File(docFileDirectory);
				if (!docDirectoryFile.exists()) {
					docDirectoryFile.mkdirs();
				}
				// 创建流程文件路径
				String docFilePath = docFileDirectory + File.separator + processDownloadBean.getFlowName() + ".doc";
				// 生成流程操作说明
				DownloadUtil.createProcessFile(flowId, processDownloadBean, docControlDao, processBasicDao,
						configItemDao, docFilePath, isPub);
			} catch (Exception e) {
				log.error("批量下载流程操作说明异常！", e);
			}

		}
		// 压缩文件
		try {
			zipFile(filePath, zipPath);
		} catch (Exception e) {
			log.error("压缩文件异常！", e);
		}
		// byte[] fileByte = null;
		// try {
		// fileByte = getBytesByFile(zipPath);
		// // File zipFiles = new File(zipPath);
		// // if (zipFiles.exists()) {
		// // zipFiles.delete();
		// // }
		// // // 删除文件下所有的文件和文件夹
		// // deleteDirectory(docPath);
		// } catch (Exception e) {
		// log.error("压缩文件，读取为流时发生异常！", e);
		// }
		return zipPath;
	}

	public static byte[] getProcessFile(Long flowId, IJecnConfigItemDao configItemDao,
			IJecnDocControlDao docControlDao, IFlowStructureDao flowStructureDao, IProcessBasicInfoDao processBasicDao,
			IProcessKPIDao processKPIDao, IProcessRecordDao processRecordDao, boolean isPub, Long peopleId,
			Long projectId, boolean isAdmin) throws Exception {
		// 获取流程操作说明信息
		ProcessDownloadBean processDownloadBean = DownloadUtil.getProcessDownloadBean(flowId, isPub, configItemDao,
				docControlDao, flowStructureDao, processBasicDao, processKPIDao, processRecordDao);
		// 生成流程操作说明
		byte[] bytes = DownloadUtil.docDownType(flowId, processDownloadBean, docControlDao, processBasicDao,
				configItemDao, null, isPub);
		return bytes;
	}

	/**
	 * 操作说明附件集合
	 * 
	 * @param flowId
	 * @param flowStructureDao
	 * @param isPub
	 * @param peopleId
	 * @param projectId
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public static Map<EXCESS, List<FileOpenBean>> getProcessExcessFile(Long flowId, IFlowStructureDao flowStructureDao,
			IFileService fileService, boolean isPub, Long peopleId, Long projectId, boolean isAdmin) throws Exception {

		// 相关制度附件
		List<Long> ruleList = new ArrayList<Long>();
		// 相关制度 附件（本地上传）
		List<Object[]> ruleFileLocal = new ArrayList<Object[]>();
		// 流程标准化文件
		List<Long> standardizedList = new ArrayList<Long>();
		// 相关标准
		List<Long> standList = new ArrayList<Long>();

		// 活动中的附件 ==》 输入输出、操作规范
		List<Long> actList = flowStructureDao.getAssociatedFileIdByProcessId(flowId, projectId, peopleId, isPub,
				isAdmin);

		// 流程文件定义中 是否启用了 相关制度
		if (JecnConfigTool.isEnableRule()) {
			// 相关制度附件
			ruleList.addAll(flowStructureDao.getProcessRuleFile(flowId, projectId, peopleId, isPub, isAdmin));

			// 制度文件本地上传
			ruleFileLocal.addAll(flowStructureDao.getProcessLocalRuleFile(flowId, projectId, peopleId, isPub, isAdmin));

		}

		// 流程文件定义中是否启用了 相关标准
		if (JecnConfigTool.isEnableStandardized()) {
			standList.addAll(flowStructureDao.getProcessStandFile(flowId, projectId, peopleId, isPub, isAdmin));
		}

		// 流程文件定义中是否启用了相关标准化文件
		if (JecnConfigTool.isEnableStandardizedFile()) {
			standardizedList.addAll(flowStructureDao.getAcessStandardizedFileIds(flowId, projectId, peopleId, isPub,
					isAdmin));
		}

		return getOpenFileMaps(actList, ruleList, standList, standardizedList, ruleFileLocal, fileService);
	}

	/**
	 * 临时文件写入本地
	 * 
	 * @param downloadMap
	 * @param path
	 */
	public static void writeEnclosureToLocal(Map<EXCESS, List<FileOpenBean>> downloadMap, String path) {
		if (downloadMap == null || StringUtils.isBlank(path)) {
			return;
		}
		for (EXCESS key : downloadMap.keySet()) {
			String tempPath = path + File.separator + key.getValue();
			List<FileOpenBean> fileList = downloadMap.get(key);
			if (fileList == null || fileList.size() == 0) {
				continue;
			}
			// 判断目录是否存在
			getFilePath(tempPath);
			// 存储文件名称集合
			Map<String, Integer> fileMap = new HashMap<String, Integer>();
			// 获取文件信息
			for (FileOpenBean fileOpenBean : fileList) {
				// 获取文件路径
				String filePaths = tempPath + File.separator + fileOpenBean.getName();
				// 文件输出流
				FileOutputStream fileFos = null;
				try {
					// 判断是否重名
					File isExists = new File(filePaths);
					String fileName = isExists.getName();
					if (isExists.exists()) {
						if (fileMap.containsKey(fileName)) {
							int fileVal = fileMap.get(fileName);
							StringBuffer fileStr = new StringBuffer(fileName);
							fileStr.insert(fileName.lastIndexOf("."), "(" + (fileVal + 1) + ")");
							fileMap.put(fileName, fileVal + 1);
							filePaths = tempPath + File.separator + fileStr.toString();
						} else {
							fileMap.put(fileName, 1);
						}
					} else {
						fileMap.put(fileName, 1);
					}

					fileFos = new FileOutputStream(filePaths);
					fileFos.write(fileOpenBean.getFileByte());
				} catch (Exception e1) {
					log.error("processFiles流程文件带附件下载附件输出异常！", e1);
					// return;
				} finally {
					if (fileFos != null) {
						try {
							fileFos.close();
						} catch (Exception e2) {
							log.error("关闭文件附件输入流异常！", e2);
						}
					}
				}
			}
		}
	}

	/**
	 * 判断文件目录是存在不存在创建
	 * 
	 * @param path
	 */
	public static void getFilePath(String path) {
		// 判断文件目录是否存在不存在创建
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
	}

	/**
	 * 制度相关附件下载
	 * 
	 * @param ruleId
	 * @param ruleDao
	 * @param fileService
	 * @param isPub
	 * @param peopleId
	 * @param projectId
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public static Map<EXCESS, List<FileOpenBean>> ruleFilesDownLoad(Long ruleId, IRuleDao ruleDao, IFileDao fileDao,
			boolean isPub, WebLoginBean bean) throws Exception {
		Map<EXCESS, List<FileOpenBean>> map = new HashMap<EXCESS, List<FileOpenBean>>();
		// 权限类别
		int fileAccessType = "1".equals(JecnConfigTool
				.getItemValue(ConfigItemPartMapMark.isShowFileDownLoad.toString())) ? 1 : 0;
		String pubStr = isPub ? "" : "_T";
		// 获取制度相关附件
		List<Long> standardizedFiles = ruleDao.getRuleStandardizedFileIds(ruleId, pubStr, bean, fileAccessType);
		List<FileOpenBean> ruleFile = new ArrayList<FileOpenBean>();
		FileOpenBean openBean = null;
		for (Long id : standardizedFiles) {
			openBean = openFile(id, isPub, fileDao);
			if (openBean != null) {
				ruleFile.add(openBean);
			}
		}
		map.put(EXCESS.FILE_STANDIZED, ruleFile);
		// 标准附件
		List<Long> standardFiles = ruleDao.getStandFileByFlowId(ruleId, bean, pubStr, fileAccessType);
		List<FileOpenBean> ruleStandardFile = new ArrayList<FileOpenBean>();
		for (Long id : standardFiles) {
			openBean = openFile(id, isPub, fileDao);
			if (openBean != null) {
				ruleStandardFile.add(openBean);
			}
		}
		map.put(EXCESS.FILE_STAND, ruleStandardFile);

		// 相关制度文件（制度模板中的相关文件）
		List<Long> ruleFiles = ruleDao.getRuleModelFileIds(ruleId, pubStr, bean, fileAccessType);
		List<FileOpenBean> ruleFile1 = new ArrayList<FileOpenBean>();
		for (Long id : ruleFiles) {
			openBean = openFile(id, isPub, fileDao);
			if (openBean != null) {
				ruleFile1.add(openBean);
			}
		}
		map.put(EXCESS.FILE_RULE, ruleFile1);
		return map;
	}

	/**
	 * 流程相关附件下载（浏览端-根据权限获取相关附件）
	 * 
	 * @param flowId
	 * @param flowStructureDao
	 * @param fileService
	 * @param isPub
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	public static Map<EXCESS, List<FileOpenBean>> getFlowFileDownLoad(Long flowId, IFlowStructureDao flowStructureDao,
			IFileService fileService, boolean isPub, WebLoginBean bean) throws Exception {
		int fileAccessType = "1".equals(JecnConfigTool
				.getItemValue(ConfigItemPartMapMark.isShowFileDownLoad.toString())) ? 1 : 0;
		int ruleAccessType = "1".equals(JecnConfigTool
				.getItemValue(ConfigItemPartMapMark.isShowRuleDownLoad.toString())) ? 1 : 0;
		String pubStr = isPub ? "" : "_T";
		// 1、活动中的附件 ==》 输入输出、操作规范
		List<Long> actList = flowStructureDao.getActivityFileByFlowId(flowId, bean, pubStr, fileAccessType);
		// 相关制度附件
		List<Long> ruleList = flowStructureDao.getRuleFileIdsByFlowId(flowId, bean, pubStr, fileAccessType);
		// 相关标准附件
		List<Long> standList = flowStructureDao.getStandFileByFlowId(flowId, bean, pubStr, fileAccessType);
		// 相关标准化文件
		List<Long> standardizedIds = flowStructureDao.getStandardizedFileIds(flowId, bean, pubStr, fileAccessType);
		// 制度文件本地上传
		List<Object[]> ruleFileLocal = flowStructureDao.getLocalRuleFileByFlowId(flowId, bean, pubStr, ruleAccessType);

		return getOpenFileMaps(actList, ruleList, standList, standardizedIds, ruleFileLocal, fileService);
	}

	private static Map<EXCESS, List<FileOpenBean>> getOpenFileMaps(List<Long> actList, List<Long> ruleList,
			List<Long> standList, List<Long> standardizedIds, List<Object[]> ruleFileLocal, IFileService fileService)
			throws Exception {
		Map<EXCESS, List<FileOpenBean>> map = new HashMap<EXCESS, List<FileOpenBean>>();
		List<FileOpenBean> actFile = new ArrayList<FileOpenBean>();
		List<FileOpenBean> ruleFile = new ArrayList<FileOpenBean>();
		List<FileOpenBean> standFile = new ArrayList<FileOpenBean>();
		// 标准化文件
		List<FileOpenBean> standizedFile = new ArrayList<FileOpenBean>();
		FileOpenBean openBean = null;
		for (Long id : actList) {
			openBean = fileService.openFile(id, false);
			if (openBean != null) {
				actFile.add(openBean);
			}
		}
		for (Long id : ruleList) {
			openBean = fileService.openFile(id, false);
			if (openBean != null) {
				ruleFile.add(openBean);
			}
		}
		for (Long id : standList) {
			openBean = fileService.openFile(id, false);
			if (openBean != null) {
				standFile.add(openBean);
			}
		}
		for (Long id : standardizedIds) {
			openBean = fileService.openFile(id, false);
			if (openBean != null) {
				standizedFile.add(openBean);
			}
		}

		for (Object[] obj : ruleFileLocal) {
			if (obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}
			openBean = new FileOpenBean();
			openBean.setId(Long.valueOf(obj[0].toString()));
			openBean.setName(obj[1] + "");
			java.sql.Blob blob = (java.sql.Blob) obj[2];
			byte[] content = new byte[(int) blob.length()];
			InputStream in = null;
			try {
				in = blob.getBinaryStream();
				in.read(content);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (in != null) {
					in.close();
				}
			}
			openBean.setFileByte(content);
			ruleFile.add(openBean);
		}
		map.put(EXCESS.FILE_ACT, actFile);
		map.put(EXCESS.FILE_RULE, ruleFile);
		map.put(EXCESS.FILE_STAND, standFile);
		map.put(EXCESS.FILE_STANDIZED, standizedFile);
		return map;
	}

	/**
	 * 获取文件
	 * 
	 * @param id
	 * @param isPub
	 * @param fileDao
	 * @return
	 * @throws Exception
	 */
	public static FileOpenBean openFile(Long id, boolean isPub, IFileDao fileDao) throws Exception {
		// 文件对应版本
		Long versionId = null;
		// 文件名称
		if (isPub) {
			JecnFileBean jecnFileBean = fileDao.findJecnFileBeanById(id);
			// 如果为空或者被删除的话(假删)
			if (jecnFileBean == null || jecnFileBean.getDelState() == 1) {
				return null;
			}
			if (jecnFileBean.getVersionId() == null) {
				return null;
			}
			versionId = jecnFileBean.getVersionId();
		} else {
			JecnFileBeanT jecnFileBeanT = fileDao.get(id);
			if (jecnFileBeanT == null || jecnFileBeanT.getDelState() == 1) {
				return null;
			}
			// 根据文件ID查阅文件版本信息
			if (jecnFileBeanT.getVersionId() == null) {
				return null;
			}
			versionId = jecnFileBeanT.getVersionId();
		}
		return JecnDaoUtil.getFileOpenBean(fileDao, versionId);
	}

	/**
	 * 删除目录（文件夹）以及目录下的文件
	 * 
	 * @author fuzhh 2013-9-2
	 * @param sPath
	 *            被删除目录的文件路径
	 * @return 目录删除成功返回true，否则返回false
	 */
	public static boolean deleteDirectory(String sPath) {
		// 如果sPath不以文件分隔符结尾，自动添加文件分隔符
		if (!sPath.endsWith(File.separator)) {
			sPath = sPath + File.separator;
		}
		File dirFile = new File(sPath);
		// 如果dir对应的文件不存在，或者不是一个目录，则退出
		if (!dirFile.exists() || !dirFile.isDirectory()) {
			return false;
		}
		boolean flag = true;
		// 删除文件夹下的所有文件(包括子目录)
		File[] files = dirFile.listFiles();
		for (int i = 0; i < files.length; i++) {
			// 删除子文件
			if (files[i].isFile()) {
				flag = deleteFile(files[i].getAbsolutePath());
				if (!flag)
					break;
			} // 删除子目录
			else {
				flag = deleteDirectory(files[i].getAbsolutePath());
				if (!flag)
					break;
			}
		}
		if (!flag)
			return false;
		// 删除当前目录
		if (dirFile.delete()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 删除单个文件
	 * 
	 * @author fuzhh 2013-9-2
	 * @param sPath
	 *            被删除文件的文件名
	 * @return 单个文件删除成功返回true，否则返回false
	 */
	public static boolean deleteFile(String sPath) {
		boolean flag = false;
		File file = new File(sPath);
		// 路径为文件且不为空则进行删除
		if (file.isFile() && file.exists()) {
			file.delete();
			flag = true;
		}
		return flag;
	}

	/**
	 * 压缩文件(包括子目录)
	 * 
	 * @author fuzhh 2013-9-2
	 * @param baseDir
	 *            要压缩的文件夹(物理路径)
	 * @param zipName
	 *            保存的文件名称(物理文件路径)
	 * @throws Exception
	 */
	public static void zipFile(String baseDir, String zipName) {
		ZipOutputStream zos = null;
		InputStream is = null;
		try {
			List<File> fileList = getSubFiles(new File(baseDir));
			zos = new ZipOutputStream(new FileOutputStream(zipName));
			zos.setEncoding(System.getProperty("sun.jnu.encoding"));// 设置文件名编码方式
			ZipEntry ze = null;
			byte[] buf = new byte[1024];
			int readLen = 0;
			for (int i = 0; i < fileList.size(); i++) {
				File f = (File) fileList.get(i);
				ze = new ZipEntry(getAbsFileName(baseDir, f));
				ze.setSize(f.length());
				ze.setTime(f.lastModified());
				zos.putNextEntry(ze);
				is = new BufferedInputStream(new FileInputStream(f));
				while ((readLen = is.read(buf, 0, 1024)) != -1) {
					zos.write(buf, 0, readLen);
				}
				is.close();
			}
		} catch (Exception e) {
			log.error("生成压缩文件异常！", e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					log.error("", e);
				}
			}
			if (zos != null) {
				try {
					zos.closeEntry();
					zos.close();
				} catch (IOException e) {
					log.error("", e);
				}

			}
		}
	}

	/**
	 * 给定根目录，返回另一个文件名的相对路径，用于zip文件中的路径.
	 * 
	 * @param baseDir
	 *            java.lang.String 根目录
	 * @param realFileName
	 *            java.io.File 实际的文件名
	 * @return 相对文件名
	 */
	private static String getAbsFileName(String baseDir, File realFileName) {
		File real = realFileName;
		File base = new File(baseDir);
		String ret = real.getName();
		while (true) {
			real = real.getParentFile();
			if (real == null)
				break;
			if (real.equals(base))
				break;
			else
				ret = real.getName() + File.separator + ret;
		}
		return ret;
	}

	/**
	 * 取得指定目录下的所有文件列表，包括子目录.
	 * 
	 * @author fuzhh 2013-9-2
	 * @param baseDir
	 *            File 指定的目录
	 * @return 包含java.io.File的List
	 */
	private static List<File> getSubFiles(File baseDir) {
		List<File> ret = new ArrayList<File>();
		File[] tmp = baseDir.listFiles();
		if (tmp != null) {
			for (int i = 0; i < tmp.length; i++) {
				if (tmp[i].isFile()) {
					ret.add(tmp[i]);
				}
				if (tmp[i].isDirectory()) {
					ret.addAll(getSubFiles(tmp[i]));
				}
			}
		}
		return ret;
	}

}
