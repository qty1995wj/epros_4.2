package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;
import com.jecn.webservice.nine999.Nine999ServiceClient;

/**
 * 九新药业 webservice 人员同步数据封装
 * 
 * @author Angus
 * @date 2018年8月3日16:11:53
 */
public class ImportUserByWbServiceNine999 extends AbstractImportUser {
	private static final Logger log = Logger.getLogger(ImportUserByWbServiceNine999.class);


	Nine999ServiceClient webService = new Nine999ServiceClient();

	public ImportUserByWbServiceNine999(AbstractConfigBean configBean) {
		super(configBean);
	}

	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
//		List<DeptBean> deptBeans = new ArrayList<DeptBean>();
//		Map<String,String> deptMap = new HashMap<String,String>();
//		String jsonMap = webService.getDeptAll();
//		
//		JSONArray deptMapJson = JSONArray.parseArray(jsonMap);
//		for (Iterator iterator = deptMapJson.iterator(); iterator.hasNext();) {
//			JSONObject job = (JSONObject) iterator.next();
//			deptMap.put(job.get("id").toString(), job.get("name").toString());
//		}
//		
//		
//		String jsonStr = webService.getDeptString();
//		JSONArray depts = JSONArray.parseArray(jsonStr);
//
//		DeptBean deptBean = null;
//		for (Iterator iterator = depts.iterator(); iterator.hasNext();) {
//			JSONObject job = (JSONObject) iterator.next();
//			if("false".equals(job.get("isAvailable").toString())){
//				continue;
//			}
//			deptBean = new DeptBean();
//			deptBean.setDeptNum(job.get("id").toString());
//			deptBean.setDeptName(job.get("name").toString());
//			if(null == job.get("parent") || "".equals(job.get("parent").toString())){
//				continue;
////				deptBean.setPerDeptNum("");
//			}else{
//				deptBean.setPerDeptNum(job.get("parent").toString());
//			}
//			
//			deptBeans.add(deptBean);
//		}
//		DeptBean rootBean = new DeptBean();
//		rootBean.setDeptNum("142f9f17771779cb69eb83f4b91999c4");
//		rootBean.setDeptName("华润三九");
//		rootBean.setPerDeptNum("0");
//		deptBeans.add(rootBean);
		List<DeptBean> deptBeans = new ArrayList<DeptBean>();
		JSONArray depts = webService.getDeptJson();

		DeptBean deptBean = null;
		for (Iterator iterator = depts.iterator(); iterator.hasNext();) {
			JSONObject job = (JSONObject) iterator.next();
			if("false".equals(job.get("isAvailable").toString())){
				continue;
			}
			deptBean = new DeptBean();
			deptBean.setDeptNum(job.get("id").toString());
			deptBean.setDeptName(job.get("name").toString());
			if(null == job.get("parent") || "".equals(job.get("parent").toString())){
				deptBean.setPerDeptNum("0");
			}else{
				deptBean.setPerDeptNum(job.get("parent").toString());
			}
			
			deptBeans.add(deptBean);
		}

		return deptBeans;
	}

	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		//先获取岗位集合
		JSONArray postions = webService.getPostJson();
		Map<String,String> posMap = new HashMap<String,String>();
		Map<String,String> posDeptMap = new HashMap<String,String>();
		for(Iterator p = postions.iterator();p.hasNext();){
			JSONObject obj = (JSONObject) p.next();
			boolean available = Boolean.parseBoolean( obj.get("isAvailable").toString());
			if(null == obj.get("isAvailable") || !available){
				continue;
			}
			posMap.put(obj.get("id").toString(), obj.get("name").toString());
			if(null == obj.get("parent") || "".equals(obj.get("parent").toString())){
				log.error(obj.toString());
			}else{
				posDeptMap.put(obj.get("id").toString(), obj.get("parent").toString());
			}
			
		}
		
		
		List<UserBean> userBeans = new ArrayList<UserBean>();
		JSONArray users = webService.getPersonJson();

		log.error("获取到人员数量："+users.size());
		for (Iterator iterator = users.iterator(); iterator.hasNext();) {
			JSONObject obj = (JSONObject) iterator.next();
			if ("admin".equals(obj.get("name").toString())) {
				continue;
			}
			if("false".equals(obj.get("isAvailable").toString())){
				continue;
			}
			UserBean userBean = new UserBean();
			userBean.setUserNum(obj.get("loginName").toString() + "");
			userBean.setTrueName(obj.get("name").toString());
			
//			if(null == obj.get("parent") || "".equals(obj.get("parent").toString())){
//				userBean.setDeptNum("");
//			}else{
//				userBean.setDeptNum(obj.get("parent").toString());
//			}
			if(null == obj.get("emaill") || "".equals(obj.get("emaill").toString())){
				userBean.setEmail("");
			}else{
				userBean.setEmail(obj.get("emaill").toString());
			}
			
			userBean.setEmailType(0);

			//添加岗位ID
			if(null == obj.get("posts") || "".equals(obj.get("posts").toString())){
				userBean.setPosNum("");
				userBeans.add(userBean);
			}else{
				JSONArray positions = JSONArray.parseArray(obj.get("posts").toString());
				String firstPos = "";
				for(Iterator pos = positions.iterator();pos.hasNext();){
					String posId = (String) pos.next();
					if(firstPos.equals("")){
						firstPos = posId;
					}else if(firstPos.equals(posId)){
						continue;
					}
					UserBean userJobBean = new UserBean();
					userJobBean.setUserNum(userBean.getUserNum());
					userJobBean.setTrueName(userBean.getTrueName());
					userJobBean.setDeptNum(posDeptMap.get(posId));
					userJobBean.setEmail(userBean.getEmail());
					userJobBean.setEmailType(userBean.getEmailType());
					userJobBean.setPosNum(posId);
					userJobBean.setPosName(posMap.get(posId));
					userBeans.add(userJobBean);
				}
			}
			
		}
		
		return userBeans;
	}
}
