package com.jecn.epros.server.download.wordxml.siwei;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ParagraphRowIndBean;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 河南思维操作说明下载
 * 
 * @author hyl
 * 
 */
public class SWProcessModel extends ProcessFileModel {

	private ParagraphLineRule vLine1p5 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);
	private ParagraphLineRule vLine = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/*** 段落行距最小值22.5磅 *****/
	private ParagraphLineRule vLineFix22 = new ParagraphLineRule(22.5F, LineRuleType.AT_LEAST);
	/**** 段落行距最小值16磅值 **********/
	private ParagraphLineRule vLineFix16 = new ParagraphLineRule(16F, LineRuleType.AT_LEAST);
	/***** 段落空格换行属性bean *******/
	private ParagraphBean newLineBean;

	public SWProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		setDocStyle("、", textTitleStyle());
		docProperty.setTblTitleShadow(true);

		/***** 段落空格换行属性bean 宋体 小四 最小值16磅值 *******/
		newLineBean = new ParagraphBean(Align.left, new FontBean("宋体", Constants.xiaosi, false));
		newLineBean.setSpace(0f, 0f, vLineFix22);
		docProperty.setNewTblWidth(17.49F);
	}

	@Override
	protected void afterHandle() {
		// 获得所有的table
		List<Table> tables = docProperty.getSecondSect().getTables();
		alterTableStyle(tables);
		tables = docProperty.getFirstSect().getTables();
		alterTableStyle(tables);
	}

	private void alterTableStyle(List<Table> tables) {
		if (tables == null || tables.size() == 0) {
			return;
		}

		for (Table table : tables) {
			table.setValign(Valign.center);
			TableBean tableBean = table.getTableBean();
			if (tableBean != null) {
				tableBean.setCellMarginBean(new CellMarginBean(0f, 0.2f, 0, 0.2F));
			}
		}

	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.5F, 2F, 1.5F, 1.7F, 1F, 1.2F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/*****
	 * 创建表格项
	 * 
	 * @param contentSect
	 * @param fs
	 * @param list
	 * @return
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] fs, List<String[]> list) {
		processFileItem.getBelongTo().createParagraph("", newLineBean);
		Table tab = super.createTableItem(processFileItem, fs, list);
		processFileItem.getBelongTo().createParagraph("", newLineBean);
		return tab;
	}

	/**
	 * 创建文本项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	@Override
	protected void createTextItem(ProcessFileItem processFileItem, String... content) {
		super.createTextItem(processFileItem, content);
		processFileItem.getBelongTo().createParagraph("", newLineBean);
	}

	/**
	 * 添加封面节点数据
	 * 
	 * @param titleSect
	 */
	private void addTitleSectContent(Sect titleSect) {
		// 编制部门
		String orgName = processDownloadBean.getOrgName();
		// 文件编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		// 文件版本
		String flowVersion = processDownloadBean.getFlowVersion();
		// 流程名称
		String flowName = processDownloadBean.getFlowName();

		// 宋体 小一 加粗 1倍行距
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoyi, true);

		pBean.setSpace(0f, 0f, vLine);
		titleSect.createParagraph(flowName, pBean);

		titleSect.createMutilEmptyParagraph(2);

		// 宋体 四号 1.5倍行距
		pBean = new ParagraphBean(Align.left, "宋体", Constants.sihao, false);
		pBean.setSpace(0f, 0f, vLine1p5);

		ParagraphRowIndBean paragraphRowIndBean = new ParagraphRowIndBean();
		paragraphRowIndBean.setLeft(3.7f);
		paragraphRowIndBean.setRight(0f);
		paragraphRowIndBean.setFirstLine(0.25f);
		pBean.setParagraphRowIndBean(paragraphRowIndBean);

		titleSect.createParagraph("编制部门：" + orgName, pBean);
		titleSect.createParagraph("文件编号：" + flowInputNum, pBean);
		titleSect.createParagraph("文件版号：" + flowVersion, pBean);
		titleSect.createMutilEmptyParagraph(4);

		// 边框粗细
		TableBean tabBean = new TableBean();
		tabBean.setBorder(1f);
		tabBean.setBorderBottom(2f);
		tabBean.setBorderTop(2f);
		tabBean.setBorderLeft(2f);
		tabBean.setBorderRight(2f);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));

		// 宋体 小四 居左 最小值 16磅值
		pBean = new ParagraphBean("宋体", Constants.xiaosi, false);
		pBean.setSpace(0f, 0f, vLineFix16);
		List<String[]> rowData = new ArrayList<String[]>();
		String[] firstRowData = new String[4];
		firstRowData[0] = "审批栏";
		firstRowData[1] = "姓名";
		firstRowData[2] = "签名";
		firstRowData[3] = "日期 ";
		rowData.add(firstRowData);
		/** 评审人集合 0 评审人类型 1名称 2日期 */
		List<Object[]> peopleList = processDownloadBean.getPeopleList();
		String[] str = null;
		for (Object[] obj : peopleList) {
			str = new String[] { "" + obj[0], "" + obj[1], "", "" + obj[2] };
			rowData.add(str);
		}
		Table tab = JecnWordUtil.createTab(titleSect, tabBean, new float[] { 4.37F, 4.37F, 4.37F, 4.37F }, rowData,
				pBean);
		/***** 设置表格行高0.56 ******/
		ParagraphBean titleParagraphBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		ParagraphBean centerParagraphBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		for (int i = 0; i < tab.getRowCount(); i++) {
			tab.getRow(i).setHeight(0.56F);
			if (i == 0) {

				tab.getRow(i).getCell(0).getParagraph(0).setParagraphBean(titleParagraphBean);
				tab.getRow(i).getCell(1).getParagraph(0).setParagraphBean(titleParagraphBean);
				tab.getRow(i).getCell(2).getParagraph(0).setParagraphBean(titleParagraphBean);
				tab.getRow(i).getCell(3).getParagraph(0).setParagraphBean(titleParagraphBean);
			} else {
				tab.getRow(i).getCell(0).getParagraph(0).setParagraphBean(titleParagraphBean);
				tab.getRow(i).getCell(1).getParagraph(0).setParagraphBean(centerParagraphBean);
				tab.getRow(i).getCell(2).getParagraph(0).setParagraphBean(centerParagraphBean);
				tab.getRow(i).getCell(3).getParagraph(0).setParagraphBean(centerParagraphBean);
			}
			for (int j = 0; j < 4; j++) {
				tab.getRow(i).getCell(j).setvAlign(Valign.center);
			}
		}
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommhdr(sect, HeaderOrFooterType.odd);

	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, HeaderOrFooterType type) {

		List<String[]> rowData = new ArrayList<String[]>();
		Header hdr = sect.createHeader(type);

		// 流程编号
		String flowInputNum = processDownloadBean.getFlowInputNum();
		// 文件版本
		String flowVersion = processDownloadBean.getFlowVersion();
		// 发布时间
		String releaseDate = processDownloadBean.getReleaseDate();
		// 生效时间
		String ectiveDate = processDownloadBean.getEffectiveDate();
		// 公司名称
		String companyName = processDownloadBean.getCompanyName();
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		rowData.add(new String[] { "", "文件编号", flowInputNum });
		rowData.add(new String[] { "", "文件版本", flowVersion });
		rowData.add(new String[] { "", "发布日期", releaseDate });
		rowData.add(new String[] { "", "生效日期", ectiveDate });
		rowData.add(new String[] { "", "页    码", "第" });
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		tabBean.setBorder(1);
		Table table = JecnWordUtil.createTab(hdr, tabBean, new float[] { 10.69F, 2.25F, 4.44F }, rowData,
				new ParagraphBean("宋体", Constants.wuhao, false));
		// 合并单元格
		table.getRow(0).getCell(0).setRowspan(2);
		table.getRow(2).getCell(0).setRowspan(3);

		for (int i = 0; i < table.getRowCount(); i++) {
			table.getRow(i).setHeight(0.5F);
		}
		/*** 宋体 小二 加粗 1.5倍行距 ******/
		ParagraphBean tempPBean = new ParagraphBean(Align.center, "宋体", Constants.xiaoer, true);
		tempPBean.setSpace(0f, 0f, vLine1p5);
		/*** logo字体 ******/
		ParagraphBean logoBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		logoBean.setSpace(0f, 0f, vLine);
		/***** 公司logo图标 ****/
		Image logo = new Image(docProperty.getLogoImage());
		PositionBean positionBean = new PositionBean(PositionType.absolute, 1F, 0, 2F, 0);
		logo.setSize(1.01F, 0.53F);
		logo.setPositionBean(positionBean);
		table.getRow(0).getCell(0).getParagraph(0).appendGraphRun(logo);
		table.getRow(0).getCell(0).getParagraph(0).appendTextRun("	  " + companyName,
				new FontBean("黑体", Constants.sihao, true));

		Paragraph p = table.getRow(4).getCell(2).getParagraph(0);
		p.appendCurPageRun();
		p.appendTextRun("页 共 ");
		p.appendTotalPagesRun(0);
		p.appendTextRun("页");
		hdr.createParagraph("");

		table.getRow(2).getCell(0).createParagraph(flowName, tempPBean);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		firstSect.setSectBean(getSectBean());
		createCommhdr(firstSect, HeaderOrFooterType.first);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = new SectBean();
		sectBean.setSize(29.7F, 21);
		sectBean.setPage(2, 0.55F, 1.8F, 1.5F, 0.78F, 0.9F);
		sectBean.setHorizontal(true);
		flowChartSect.setSectBean(sectBean);
		this.createCommhdrftr(flowChartSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		// 添加封皮节点数据
		addTitleSectContent(titleSect);
		createCommhdr(titleSect, HeaderOrFooterType.first);
	}

	private void createTitlehdr(Sect titleSect) {

		Header hdr = titleSect.createHeader(HeaderOrFooterType.odd);

		/***** 公司logo图标 ****/
		Image logo = new Image(docProperty.getLogoImage());
		PositionBean positionBean = new PositionBean(PositionType.absolute, 1F, 0, 2F, 0);
		logo.setSize(1.01F, 0.53F);
		logo.setPositionBean(positionBean);
		Paragraph paragraph = new Paragraph(false);
		paragraph.appendGraphRun(logo);
		paragraph.appendTextRun("河南思维流程文件");
		paragraph.setParagraphBean(textContentStyle());
		hdr.addParagraph(paragraph);

	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0, 0, 0, 0);
		// 表格左缩进0.01厘米
		tblStyle.setTableLeftInd(0.01F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		textContentStyle.setSpace(0f, 0f, vLineFix22);
		textContentStyle.setInd(0.75f, 0, 1f, 0f);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("宋体", Constants.xiaosi, true);
		textTitleStyle.setSpace(0F, 0F, vLine1p5);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	/**
	 * 流程责任属性
	 * 
	 * @param name
	 * @param responFlow
	 * @param contentSect
	 */
	@Override
	protected void a23(ProcessFileItem processFileItem, ProcessAttributeBean responFlow) {
		createTitle(processFileItem);

		Sect firstSect = docProperty.getFirstSect();
		// 流程责任人
		String responsiblePerson = JecnUtil.getValue("processResponsiblePersons");

		firstSect.createParagraph(responsiblePerson + "：" + responFlow.getDutyUserName(), newLineBean)
				.setParagraphBean(textContentStyle());

		// 流程责任部门
		String responsibleDept = JecnUtil.getValue("processResponsibilityDepartment");
		firstSect.createParagraph(responsibleDept + "：" + responFlow.getDutyOrgName(), newLineBean).setParagraphBean(
				textContentStyle());

	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {
		createTitle(processFileItem);
		// 活动编号
		String activityNumber = JecnUtil.getValue("activitynumbers");
		// 执行角色s
		String executiveRole = JecnUtil.getValue("executiveRole");
		// 活动名称
		String eventTitle = JecnUtil.getValue("activityTitle");
		// 活动说明
		String activityDescription = "具体活动说明";
		String input = "输入";
		String output = "输出";
		float[] width = new float[] { 1.19F, 2.5F, 2.5F, 5.25F, 2F, 2.04F };
		List<Object[]> newRowData = new ArrayList<Object[]>();
		// 标题数组
		newRowData.add(new Object[] { activityNumber, executiveRole, eventTitle, activityDescription, input, output });
		// 添加数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出
		for (Object[] activeData : processDownloadBean.getAllActivityShowList()) {
			newRowData.add(new Object[] { activeData[0], activeData[1], activeData[2], activeData[3], activeData[4],
					activeData[5] });
		}
		Table tbl = createTableItem(processFileItem, width, JecnWordUtil.obj2String(newRowData));
		TableBean tableBean = new TableBean();
		tableBean.setBorder(tbl.getTableBean().getBorderBottom().getBorderWidth());
		tableBean.setTableAlign(Align.left);
		tableBean.setTableLeftInd(0F);
		tbl.setTableBean(tableBean);

		for (int i = 0; i < tbl.getRowCount(); i++) {
			if (i == 0) {
				ParagraphBean titleBean = tblTitleStyle();
				titleBean.setAlign(Align.center);
				tbl.setRowStyle(0, titleBean, true, docProperty.isTblTitleCrossPageBreaks());
				tbl.setRowStyle(i, Valign.center);
			} else {
				tbl.setRowStyle(i, Valign.center);
				tbl.setRowStyle(i, tblContentStyle());
			}
		}

	}

	/**
	 * 关键活动
	 * 
	 * @param _count
	 * @param name
	 * @param keyActivityShowList
	 */
	protected void a07(ProcessFileItem processFileItem, List<KeyActivityBean> activeList) {
		if (activeList == null || activeList.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 关键活动
		String active = JecnUtil.getValue("keyActivities");
		// 活动编号
		String number = JecnUtil.getValue("activitynumbers");
		// 活动名称
		String title = JecnUtil.getValue("activityTitle");
		// 活动说明
		String activeExplain = JecnUtil.getValue("activityIndicatingThat");
		// 重要说明
		String keyExplain = JecnUtil.getValue("keyShow");

		List<String[]> dataList = new ArrayList<String[]>();
		// 标题行
		dataList.add(new String[] { active, number, title, activeExplain, keyExplain });

		boolean kcpAllowShow = kcpAllowShow();
		if (activeList != null) {
			for (KeyActivityBean keyActive : activeList) {
				if (!kcpAllowShow && "4".equals(keyActive.getActiveKey())) {
					continue;
				}
				String[] rowData = new String[5];
				if ("1".equals(keyActive.getActiveKey())) {
					rowData[0] = JecnUtil.getValue("problemAreas");// 问题区域
				} else if ("2".equals(keyActive.getActiveKey())) {
					rowData[0] = JecnUtil.getValue("keySuccessFactors");// 关键成功因素
				} else if ("3".equals(keyActive.getActiveKey())) {
					rowData[0] = JecnUtil.getValue("keyControlPoint");// 关键控制点
				} else if ("4".equals(keyActive.getActiveKey())) {
					rowData[0] = JecnUtil.getValue("keyControlPoint") + "(合规)";// 关键成功因素(合规)
				}
				rowData[1] = keyActive.getActivityNumber();
				rowData[2] = keyActive.getActivityName();
				rowData[3] = keyActive.getActivityShow();
				rowData[4] = keyActive.getActivityShowControl();
				dataList.add(rowData);
			}
		}
		float[] width = new float[] { 3, 2, 2.25F, 5.25F, 5F };
		Table tbl = createTableItem(processFileItem, width, dataList);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 相关流程
	 * 
	 * @param _count
	 * @param name
	 * @param relatedProcessMap
	 */
	protected void a13(ProcessFileItem processFileItem, Map<String, List<RelatedProcessBean>> relatedProcessMap) {
		if (relatedProcessMap == null || relatedProcessMap.isEmpty() || relatedProcessMap.keySet().size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 类型
		String type = JecnUtil.getValue("type");
		// 流程编号
		String processNumber = JecnUtil.getValue("processNumber");
		// 流程名称
		String processName = JecnUtil.getValue("processName");
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { type, processNumber, processName });
		for (String key : relatedProcessMap.keySet()) {
			for (RelatedProcessBean relatedProcess : relatedProcessMap.get(key)) {
				// 添加数据
				rowData.add(new String[] { getRelatedProcessType(key), relatedProcess.getFlowNumber(),
						relatedProcess.getFlowName() });
			}
		}
		float[] width = new float[] { 3.5F, 3, 11 };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}
}
