package com.jecn.epros.server.service.standard.impl;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import oracle.sql.BLOB;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.lob.SerializableBlob;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.file.JecnFileBean;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.standard.StandardFileContent;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IOrgAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPosGroupnAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPositionAccessPermissionsDao;
import com.jecn.epros.server.dao.standard.IStandardDao;
import com.jecn.epros.server.dao.standard.IStandardFlowRelationDao;
import com.jecn.epros.server.service.standard.IStandardService;
import com.jecn.epros.server.service.tree.data.INodeImportService;
import com.jecn.epros.server.service.tree.data.RiskNodeImportService;
import com.jecn.epros.server.service.tree.data.StandardNodeImportService;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnCommonSqlConstant.TYPEAUTH;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.PublicFileBean;
import com.jecn.epros.server.webBean.PublicSearchBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;
import com.jecn.epros.server.webBean.rule.RuleInfoBean;
import com.jecn.epros.server.webBean.standard.StandardDetailBean;
import com.jecn.epros.server.webBean.standard.StandardSearchBean;
import com.jecn.epros.server.webBean.standard.StandardWebBean;
import com.sun.xml.internal.txw2.IllegalAnnotationException;

@Transactional(rollbackFor = Exception.class)
public class StandardServiceImpl extends AbsBaseService<StandardBean, Long> implements IStandardService {
	private Logger log = Logger.getLogger(StandardServiceImpl.class);
	private IStandardDao standardDao;
	private IStandardFlowRelationDao standardFlowRelationDao;
	private IOrgAccessPermissionsDao orgAccessPermissionsDao;
	private IPositionAccessPermissionsDao positionAccessPermissionsDao;
	private IFileDao fileDao;
	private IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao;

	public void setPosGroupAccessPermissionsDao(IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao) {
		this.posGroupAccessPermissionsDao = posGroupAccessPermissionsDao;
	}

	public void setOrgAccessPermissionsDao(IOrgAccessPermissionsDao orgAccessPermissionsDao) {
		this.orgAccessPermissionsDao = orgAccessPermissionsDao;
	}

	public void setPositionAccessPermissionsDao(IPositionAccessPermissionsDao positionAccessPermissionsDao) {
		this.positionAccessPermissionsDao = positionAccessPermissionsDao;
	}

	public IStandardDao getStandardDao() {
		return standardDao;
	}

	public void setStandardDao(IStandardDao standardDao) {
		this.standardDao = standardDao;
		this.baseDao = standardDao;
	}

	public IStandardFlowRelationDao getStandardFlowRelationDao() {
		return standardFlowRelationDao;
	}

	public void setStandardFlowRelationDao(IStandardFlowRelationDao standardFlowRelationDao) {
		this.standardFlowRelationDao = standardFlowRelationDao;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description:树节点 标准封装
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjectsTree(List<Object[]> list) throws Exception {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectStandard(obj);
			if (jecnTreeBean == null) {
				continue;
			}
			// 流程名称
			if (obj.length > 6) {
				if ((Integer.valueOf(obj[3].toString()) == 2 || Integer.valueOf(obj[3].toString()) == 3)
						&& obj[6] == null) {
					continue;
				}
			}
			// 标准条款
			if (obj.length > 7 && obj[7] != null) {
				jecnTreeBean.setContent(obj[7].toString());
			}
			// 流程或流程地图标准是否发布
			if (obj.length > 8) {
				jecnTreeBean.setPub(obj[8] == null ? false : true);
			}
			// 文件有没有假删
			if (obj.length > 9) {
				if (Integer.valueOf(obj[3].toString()) == 1 && obj[9] == null) {
					continue;
				}
			}
			// 标准节点父类名称
			jecnTreeBean.setPname("");
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	/**
	 * @author zhangchen Apr 28, 2012
	 * @description：标准子节点通用查询
	 * @param list
	 * @return
	 */
	private List<JecnTreeBean> findByListObjects(List<Object[]> list) throws Exception {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null) {
				continue;
			}

			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			// 标准节点ID
			jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
			// 标准名称
			if (obj[1] != null) {
				jecnTreeBean.setName(obj[1].toString());
			} else {
				jecnTreeBean.setName("");
			}
			// 标准节点父ID
			jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
			// 标准类型
			if (Integer.valueOf(obj[3].toString()) == 0) {
				// 标准目录
				jecnTreeBean.setTreeNodeType(TreeNodeType.standardDir);
			} else if (Integer.valueOf(obj[3].toString()) == 1) {
				// 文件标准
				jecnTreeBean.setTreeNodeType(TreeNodeType.standard);
			} else if (Integer.valueOf(obj[3].toString()) == 2) {
				// 流程标准
				jecnTreeBean.setTreeNodeType(TreeNodeType.standardProcess);
			} else if (Integer.valueOf(obj[3].toString()) == 3) {
				// 流程地图标准
				jecnTreeBean.setTreeNodeType(TreeNodeType.standardProcessMap);
			} else if (Integer.valueOf(obj[3].toString()) == 4) {
				// 标准条款
				jecnTreeBean.setTreeNodeType(TreeNodeType.standardClause);
			} else if (Integer.valueOf(obj[3].toString()) == 5) {
				// 条款要求
				jecnTreeBean.setTreeNodeType(TreeNodeType.standardClauseRequire);
			}
			if (obj[4] != null) {
				jecnTreeBean.setRelationId(Long.valueOf(obj[4].toString()));
			}
			// 标准是否有子节点
			if (Integer.parseInt(obj[5].toString()) > 0) {
				jecnTreeBean.setChildNode(true);
			} else {
				jecnTreeBean.setChildNode(false);
			}

			// 标准节点父类名称
			jecnTreeBean.setPname("");
			listResult.add(jecnTreeBean);
		}
		return listResult;
	}

	@Override
	// 根据父id查找子节点
	public List<JecnTreeBean> getChildStandards(Long pid, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.criterion_class_id,"
					+ "       t.criterion_class_name ,t.pre_criterion_class_id"
					+ "       ,t.stan_type,t.related_id,"
					+ "       case when jup.criterion_class_id is null then '0' else '1' end as count"
					+ " ,jfs.FLOW_NAME,'' AS STAN_CONTENT,jf.flow_id,jft.file_id,jft.file_name,t.sort_id"
					+ "         from jecn_criterion_classes t"
					+ "         left join JECN_FLOW_STRUCTURE_T jfs on JFS.FLOW_ID = t.RELATED_ID"
					+ "         and (t.STAN_TYPE = 2 OR t.STAN_TYPE = 3) and JFS.DEL_STATE = 0"
					+ "         left join jecn_file_t jft on jft.file_id=t.related_id and t.STAN_TYPE=1 and jft.del_state=0"
					+ "         left join JECN_FLOW_STRUCTURE jf on JFS.FLOW_ID = jf.flow_id"
					+ "         left join jecn_criterion_classes jup on jup.pre_criterion_class_id=t.criterion_class_id"
					+ "         where t.pre_criterion_class_id=? and t.project_id=?"
					+ "         order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";
			List<Object[]> list = standardDao.listNativeSql(sql, pid, projectId);
			return findByListObjectsTree(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChildStandards(Long pid, Long projectId, Long peopleId) throws Exception {
		List<Object[]> list = standardDao.getRoleAuthChildStandards(pid, projectId, peopleId);
		return findByListObjectsTree(list);
	}

	@Override
	// 根据父id查找子节点
	public List<JecnTreeBean> getChildStandardsWeb(Long pid, Long projectId, Long peopleId, boolean isAdmin)
			throws Exception {
		try {
			String sql = "";
			if (!isAdmin && !JecnContants.isCriterionAdmin) {
				sql = JecnCommonSqlTPath.INSTANCE.getTreeAppend(peopleId, projectId, TYPEAUTH.STANDARD);
			}
			List<Object[]> list = new ArrayList<Object[]>();
			sql += "select distinct t.criterion_class_id," + "       t.criterion_class_name,"
					+ "       t.pre_criterion_class_id," + "       t.stan_type," + "       t.related_id,"
					+ "       case when jc.criterion_class_id is null then 0 else 1 end as count,"
					+ "       t.sort_id,t.t_path" + "  from jecn_criterion_classes t"
					+ "  left join jecn_criterion_classes jc on jc.pre_criterion_class_id=t.criterion_class_id";
			if (!isAdmin && !JecnContants.isCriterionAdmin) {
				sql = sql + JecnCommonSqlTPath.INSTANCE.getSqlFuncSubStr();
			}
			sql += " where t.pre_criterion_class_id = ?" + "   and t.project_id = ?";
			sql += " order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";

			list = standardDao.listNativeSql(sql, pid, projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getAllStandard(Long projectId) throws Exception {
		try {
			String sql = "select distinct t.criterion_class_id,"
					+ "       t.criterion_class_name ,t.pre_criterion_class_id"
					+ "       ,t.stan_type,t.related_id,"
					+ "       case when jup.criterion_class_id is null then '0' else '1' end"
					+ "       as count"
					+ "         ,jfs.FLOW_NAME,'' AS STAN_CONTENT,jf.flow_id,jft.file_id,t.sort_id"
					+ "         from jecn_criterion_classes t"
					+ "         left join JECN_FLOW_STRUCTURE_T jfs on JFS.FLOW_ID = t.RELATED_ID"
					+ "         and (t.STAN_TYPE = 2 OR t.STAN_TYPE = 3) and JFS.DEL_STATE = 0"
					+ "         left join jecn_file_t jft on jft.file_id=t.related_id and t.STAN_TYPE=1 and jft.del_state=0"
					+ "         left join JECN_FLOW_STRUCTURE jf on JFS.FLOW_ID = jf.flow_id"
					+ "  		left join jecn_criterion_classes jup on jup.pre_criterion_class_id=t.criterion_class_id"
					+ "         where  t.project_id=?"
					+ "         order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";
			List<Object[]> list = standardDao.listNativeSql(sql, projectId, projectId);
			return findByListObjectsTree(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void reName(String newName, Long id, Long updatePersonId) throws Exception {
		try {
			StandardBean standardBean = standardDao.get(id);
			standardBean.setCriterionClassName(newName);
			standardBean.setUpdateDate(new Date());
			standardBean.setUpdatePeopleId(updatePersonId);
			standardDao.update(standardBean);
			// 添加日志
			JecnUtil.saveJecnJournal(standardBean.getCriterionClassId(), standardBean.getCriterionClassName(), 5, 2,
					updatePersonId, standardDao, 4);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception {
		try {

			String pViewSort = "";
			if (pId.intValue() != 0) {
				String hql = "from StandardBean where criterionClassId=?";
				List<StandardBean> data = standardDao.listHql(hql, pId);
				pViewSort = data.get(0).getViewSort();
			}

			String hql = "from StandardBean where preCriterionClassId=? and projectId=? order by sortId";
			List<StandardBean> listStandardBean = standardDao.listHql(hql, pId, projectId);
			List<StandardBean> listUpdate = new ArrayList<StandardBean>();
			for (JecnTreeDragBean jecnTreeDragBean : list) {
				for (StandardBean standardBean : listStandardBean) {
					if (jecnTreeDragBean.getId().equals(standardBean.getCriterionClassId())) {
						if (standardBean.getSortId() == null
								|| (jecnTreeDragBean.getSortId() != standardBean.getSortId().intValue())) {
							standardBean.setSortId(Long.valueOf(jecnTreeDragBean.getSortId()));
							standardBean.setViewSort(JecnUtil.concatViewSort(pViewSort, jecnTreeDragBean.getSortId()));
							listUpdate.add(standardBean);
							break;
						}
					}
				}
			}

			for (StandardBean standardBean : listUpdate) {
				standardDao.update(standardBean);
				// 添加日志
				JecnUtil.saveJecnJournal(standardBean.getCriterionClassId(), standardBean.getCriterionClassName(), 5,
						5, updatePersonId, standardDao);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		try {
			String t_path = "";
			int level = -1;
			if (pId.intValue() != 0) {
				StandardBean standardBean = this.get(pId);
				t_path = standardBean.gettPath();
				level = standardBean.gettLevel();
			}
			JecnCommon.updateNodesChildTreeBeans(listIds, pId, "", new Date(), updatePersonId, t_path, level,
					standardDao, moveNodeType, 1);
			for (Long id : listIds) {
				// 添加日志
				JecnUtil.saveJecnJournal(id, null, 5, 4, updatePersonId, standardDao);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void deleteStandard(List<Long> listIds, Long projectId, Long updatePersonId) throws Exception {
		try {
			Set<Long> setIds = new HashSet<Long>();
			String sql = JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.standardDir);
			List<Object[]> listAll = standardDao.listNativeSql(sql);
			for (Object[] obj : listAll) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
					continue;
				}
				setIds.add(Long.valueOf(obj[0].toString()));
			}
			if (setIds.size() > 0) {
				// 添加日志
				JecnUtil.saveJecnJournals(setIds, 5, 3, updatePersonId, standardDao);
				standardDao.deleteStandards(setIds);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void addStandardFile(List<StandardBean> list, String posIds, String orgIds, String posGroupIds,
			Long updatePersonId) throws Exception {
		try {
			Set<Long> setFileIds = new HashSet<Long>();
			for (StandardBean standardBean : list) {
				if (standardBean.getRelationId() == null) {
					continue;
				}
				setFileIds.add(standardBean.getRelationId());
				standardBean.setCreateDate(new Date());
				standardBean.setUpdateDate(new Date());
				standardDao.save(standardBean);
				// 添加日志
				JecnUtil.saveJecnJournal(standardBean.getCriterionClassId(), standardBean.getCriterionClassName(), 5,
						1, updatePersonId, standardDao);
				// 保存岗位查阅权限
				positionAccessPermissionsDao.savaOrUpdate(standardBean.getCriterionClassId(), 2, posIds, 0, 0,
						standardBean.getProjectId(), TreeNodeType.standard);

				// 保存部门查阅权限
				orgAccessPermissionsDao.savaOrUpdate(standardBean.getCriterionClassId(), 2, orgIds, 0, 0, standardBean
						.getProjectId(), TreeNodeType.standard);

				// 岗位组查阅权限posGroupIds
				posGroupAccessPermissionsDao.savaOrUpdate(standardBean.getCriterionClassId(), 2, posGroupIds, 0, 0,
						standardBean.getProjectId(), TreeNodeType.standard);
				// // 岗位查阅权限
				// sql = JecnSqlConstant.JECN_ACCESS_PERMISSIONS_T;
				// orgAccessPermissionsDao.execteNative(sql, 2, standardBean
				// .getCriterionClassId());
				//
				// // 部门查阅权限
				// sql = JecnSqlConstant.JECN_ORG_ACCESS_PERM_T;
				// orgAccessPermissionsDao.execteNative(sql, 2, standardBean
				// .getCriterionClassId());
			}
			if (setFileIds.size() > 0) {
				// 找到未发布的文件
				List<Long> listNoPubFiles = fileDao.getNoPubFiles(setFileIds);
				if (listNoPubFiles.size() > 0) {
					// 直接发布未发布文件
					fileDao.releaseFiles(listNoPubFiles);
				}
			}
			// 更新tpath 和tlevel
			for (StandardBean standardBean : list) {
				updateTpathAndTLevel(standardBean);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchByName(String name, Long projectId) throws Exception {
		try {
			String sql = "select t.criterion_class_id," + "t.criterion_class_name,t.pre_criterion_class_id"
					+ " ,t.stan_type,t.related_id,'1'"
					+ " from jecn_criterion_classes t where t.criterion_class_name like ? "
					+ " and t.STAN_TYPE in (0,1,4,5) and t.project_id=? "
					+ " order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";

			List<Object[]> list = standardDao.listNativeSql(sql, 0, 20, "%" + name + "%", projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception {
		String sql = "select t.criterion_class_id," + "t.criterion_class_name,t.pre_criterion_class_id"
				+ " ,t.stan_type,t.related_id,'1'" + " from jecn_criterion_classes t ";
		// 拼装角色权限sql
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthSqlCommon(2, peopleId, projectId);
		sql = sql + " where t.criterion_class_name like ? " + " and t.STAN_TYPE in (0,1,4,5) and t.project_id=? "
				+ " order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";
		try {
			List<Object[]> list = standardDao.listNativeSql(sql, 0, 20, "%" + name + "%", projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	// 根据父id查找目录节点
	public List<JecnTreeBean> getChildStandardDirs(Long pid, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.criterion_class_id,t.criterion_class_name,t.pre_criterion_class_id,t.stan_type,t.related_id"
					+ "      ,case when jcc.criterion_class_id is null then '0' else '1' end as count,t.sort_id"
					+ "      from jecn_criterion_classes t"
					+ "      left join jecn_criterion_classes jcc on jcc.pre_criterion_class_id=t.criterion_class_id and jcc.stan_type=0"
					+ "      where t.pre_criterion_class_id=? and t.stan_type = 0 and t.project_id=?"
					+ "      order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";
			List<Object[]> list = standardDao.listNativeSql(sql, pid, projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	@Override
	public List<JecnTreeBean> getAllStandardDir(Long pid, Long projectId) throws Exception {
		try {
			String sql = "select distinct t.criterion_class_id,t.criterion_class_name,t.pre_criterion_class_id,t.stan_type,t.related_id"
					+ "      ,case when jcc.criterion_class_id is null then '0' else '1' end as count,t.sort_id"
					+ "      from jecn_criterion_classes t"
					+ "      left join jecn_criterion_classes jcc on jcc.pre_criterion_class_id=t.criterion_class_id and jcc.stan_type=0"
					+ "      where t.pre_criterion_class_id=? and t.stan_type = 0 and t.project_id=?"
					+ "      order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";
			List<Object[]> list = standardDao.listNativeSql(sql, pid, projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long standardId, Long projectId) throws Exception {

		try {
			StandardBean standardBean = standardDao.get(standardId);
			Set<Long> set = JecnCommon.getPositionTreeIds(standardBean.gettPath(), standardId);
			String sql = " select distinct t.criterion_class_id,"
					+ "       t.criterion_class_name ,t.pre_criterion_class_id"
					+ "       ,t.stan_type,t.related_id,"
					+ "       case when jup.criterion_class_id is null then '0' else '1' end as count"
					+ " ,jfs.FLOW_NAME,'' AS STAN_CONTENT,jf.flow_id,jft.file_id,jft.file_name,t.sort_id"
					+ "         from jecn_criterion_classes t"
					+ "         left join JECN_FLOW_STRUCTURE_T jfs on JFS.FLOW_ID = t.RELATED_ID"
					+ "         and (t.STAN_TYPE = 2 OR t.STAN_TYPE = 3) and JFS.DEL_STATE = 0"
					+ "         left join jecn_file_t jft on jft.file_id=t.related_id and t.STAN_TYPE=1 and jft.del_state=0"
					+ "         left join JECN_FLOW_STRUCTURE jf on JFS.FLOW_ID = jf.flow_id"
					+ "         left join jecn_criterion_classes jup on jup.pre_criterion_class_id=t.criterion_class_id"
					+ "  where t.Project_Id=?" + "   AND t.PRE_CRITERION_CLASS_ID in " + JecnCommonSql.getIdsSet(set)
					+ "   ORDER BY t.PRE_CRITERION_CLASS_ID,t.SORT_ID,t.Criterion_Class_Id";
			List<Object[]> list = standardDao.listNativeSql(sql, projectId);
			return findByListObjectsTree(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateStandardFile(long id, long fileId, String fileName, Long updatePersonId) throws Exception {
		try {
			String hql = "update StandardBean set relationId=?,criterionClassName=?,updatePeopleId=?,updateDate=? where id=?";
			standardDao.execteHql(hql, fileId, fileName, updatePersonId, new Date(), id);
			// 添加日志
			JecnUtil.saveJecnJournal(id, fileName, 5, 2, updatePersonId, standardDao);
			// 文件是否发布
			Set<Long> setFileIds = new HashSet<Long>();
			setFileIds.add(fileId);
			// 获得未发布文件
			List<Long> listNotPubFiles = this.fileDao.getNoPubFiles(setFileIds);
			if (listNotPubFiles.size() > 0) {
				// 直接发布文件
				fileDao.releaseFiles(listNotPubFiles);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void addRelationFlow(List<StandardBean> list, Long updatePersonId) throws Exception {
		try {
			for (StandardBean standardBean : list) {
				standardBean.setCreateDate(new Date());
				standardBean.setUpdateDate(new Date());
				standardDao.save(standardBean);
				// 添加日志
				JecnUtil.saveJecnJournal(standardBean.getCriterionClassId(), standardBean.getCriterionClassName(), 5,
						1, updatePersonId, standardDao);
			}
			// 更新tpath 和tlevel
			for (StandardBean standardBean : list) {
				updateTpathAndTLevel(standardBean);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateStandardProcess(Long id, Long processId, String name, int type, Long updatePersonId)
			throws Exception {
		try {
			String hql = "update StandardBean set relationId=?,criterionClassName=?,stanType=?,updatePeopleId=?,updateDate=? where id=?";
			standardDao.execteHql(hql, processId, name, type, updatePersonId, new Date(), id);
			// 添加日志
			JecnUtil.saveJecnJournal(id, name, 5, 2, updatePersonId, standardDao);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	public IFileDao getFileDao() {
		return fileDao;
	}

	public void setFileDao(IFileDao fileDao) {
		this.fileDao = fileDao;
	}

	/**
	 * 获得标准搜索的sql
	 * 
	 * @param ruleSearchBean
	 * @return
	 */
	private String getStandardSearchSql(StandardSearchBean standardSearchBean, Long peopleId, Long projectId) {
		String sql = "select distinct ju.criterion_class_id,"
				+ "  ju.criterion_class_name,ju.pre_criterion_class_id"
				+ "  ,jc.criterion_class_name as pname"
				+ "  ,ju.stan_type,case when ju.stan_type=1 then jf.file_id when (ju.stan_type=2 or ju.stan_type=3) then jfs.flow_id end as standard_relate_id,ju.is_public"
				+ "  ,case when ju.stan_type=1 then jf.file_name"
				+ "  when (ju.stan_type=2 or ju.stan_type=3) then jfs.flow_name" + "  end as file_name,JU.T_PATH"
				+ " from jecn_criterion_classes ju "
				+ " left join jecn_access_permissions jap on jap.type=2 and jap.relate_id=ju.criterion_class_id"
				+ " left join jecn_org_access_permissions joap on joap.type=2 and joap.relate_id=ju.criterion_class_id"
				+ " left join jecn_criterion_classes jc on jc.criterion_class_id=ju.pre_criterion_class_id"
				+ " left join jecn_file jf on jf.file_id=ju.related_id "
				+ " left join jecn_flow_structure jfs on jfs.flow_id=ju.related_id"
				+ " where ju.stan_type<>0  and ju.project_id=" + projectId;
		// 标准类型
		if (standardSearchBean.getStandardType() != -1) {
			sql = sql + " and ju.stan_type=" + standardSearchBean.getStandardType();
		}
		// 标准名称
		if (StringUtils.isNotBlank(standardSearchBean.getStandardName())) {
			sql = sql + " and ju.criterion_class_name like '%" + standardSearchBean.getStandardName() + "%'";
		}
		// 所属目录
		if (standardSearchBean.getPerId() != -1) {
			sql = sql + " and ju.pre_criterion_class_id=" + standardSearchBean.getPerId();
		}
		// 密级
		if (standardSearchBean.getSecretLevel() != -1) {
			sql = sql + " and ju.is_public=" + standardSearchBean.getSecretLevel();
		}
		// 查阅岗位权限
		if (standardSearchBean.getPosLookId() != -1) {
			sql = sql + " and jap.figure_id=" + standardSearchBean.getPosLookId();
		}
		// 查阅部门权限
		if (standardSearchBean.getOrgLookId() != -1) {
			sql = sql + " and joap.org_id=" + standardSearchBean.getOrgLookId();
		}
		return sql;
	}

	/**
	 * 获得公共搜索的sql
	 * 
	 * @param ruleSearchBean
	 * @return
	 */
	private String getSql(PublicSearchBean publicSearchBean, Long projectId) {
		String sql = "select ju.criterion_class_id,case when ju.STAN_TYPE=1 then jf.file_name"
				+ " else null end as fileName,ju.STAN_TYPE" + ",case when ju.STAN_TYPE=1 then jf.file_id"
				+ "   else null end as standard_relate_id"
				+ ",case when ju.STAN_TYPE=1 then jf.doc_id  end as number_id" + ",ju.sort_id,ju.is_public "
				+ " from jecn_criterion_classes ju"
				+ " left join jecn_file jf on jf.file_id=ju.RELATED_ID  and jf.del_state=0"

				+ " where ju.stan_type=1  and ju.project_id=" + projectId;

		// getSql 新添加密级条件(不能是默认 is_public =1)
		if (-1 != publicSearchBean.getSecretLevel()) { // -1:全部,0秘密,1公开
			// 不拼接is_public条件
			sql = sql + "  and ju.is_public= " + publicSearchBean.getSecretLevel();
		}
		// 标准名称
		if (StringUtils.isNotBlank(publicSearchBean.getName())) {
			sql = sql + " and ju.criterion_class_name like '%" + publicSearchBean.getName() + "%'";
		}
		sql = " from (" + sql + ") standard where standard.standard_relate_id is not null";
		return sql;
	}

	@Override
	public int getTotalPublicStandard(PublicSearchBean publicSearchBean, Long projectId) throws Exception {
		try {
			String sql = "select count(standard.criterion_class_id)";
			sql = sql + getSql(publicSearchBean, projectId);
			return standardDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<PublicFileBean> getPublicStandard(PublicSearchBean publicSearchBean, int start, int limit,
			Long projectId) throws Exception {
		try {
			String sql = "select standard.*";
			sql = sql + getSql(publicSearchBean, projectId);
			sql += " order by standard.sort_id,standard.criterion_class_id";
			List<Object[]> listObjects = standardDao.listNativeSql(sql, start, limit);
			List<PublicFileBean> listResult = new ArrayList<PublicFileBean>();
			for (Object[] obj : listObjects) {
				if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
					continue;
				}
				PublicFileBean publicFileBean = new PublicFileBean();
				publicFileBean.setId(Long.valueOf(obj[0].toString()));
				publicFileBean.setType(3);
				publicFileBean.setName(obj[1].toString());
				// 关联类型
				publicFileBean.setRelationName(obj[1].toString());
				if ("1".equals(obj[2].toString())) {
					publicFileBean.setRelationType(1);
				} else if ("2".equals(obj[2].toString())) {
					publicFileBean.setRelationType(2);
				} else if ("3".equals(obj[2].toString())) {
					publicFileBean.setRelationType(3);
				}
				publicFileBean.setRelationId(Long.valueOf(obj[3].toString()));
				// 编号
				if (obj[4] != null) {
					publicFileBean.setNumber(obj[4].toString());
				}
				if (obj[6] != null) {
					publicFileBean.setSecretLevel(Integer.parseInt(obj[6].toString()));
				}
				listResult.add(publicFileBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int getTotalStandardList(StandardSearchBean standardSearchBean, Long peopleId, Long projectId,
			boolean isAdmin) throws Exception {
		try {
			String sql = getStandardSearchSql(standardSearchBean, peopleId, projectId);
			sql = "select count(t.criterion_class_id) from (" + sql + ") T";
			if (!isAdmin && !JecnContants.isCriterionAdmin) {
				sql = sql + "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.STANDARD)
						+ " on MY_AUTH_FILES.criterion_class_id=t.criterion_class_id";
			}
			return standardDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<StandardWebBean> getStandardList(StandardSearchBean standardSearchBean, int start, int limit,
			Long peopleId, Long projectId, boolean isAdmin) throws Exception {
		try {
			String sql = getStandardSearchSql(standardSearchBean, peopleId, projectId);
			sql = "select  t.* from (" + sql + ") T";
			if (!isAdmin && !JecnContants.isCriterionAdmin) {
				sql = sql + "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.STANDARD)
						+ " on MY_AUTH_FILES.criterion_class_id=t.criterion_class_id";
			}
			sql += " order by t.criterion_class_id";
			List<Object[]> listObjects = standardDao.listNativeSql(sql, start, limit);
			List<StandardWebBean> listResult = new ArrayList<StandardWebBean>();
			for (Object[] obj : listObjects) {
				StandardWebBean standardWebBean = JecnUtil.getStandardWebBeanByObject(obj);
				if (standardWebBean != null) {
					listResult.add(standardWebBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 获得关注标准搜索的sql
	 * 
	 * @param ruleSearchBean
	 * @return
	 */
	private String getSql(AttenTionSearchBean attenTionSearchBean, Long projectId) {
		String sql = "select ju.criterion_class_id,ju.criterion_class_name,ju.pre_criterion_class_id"
				+ "        ,jc.criterion_class_name as standradWebBean"
				+ "        ,ju.stan_type,case when ju.stan_type=1 then jf.file_id when (ju.stan_type=2 or ju.stan_type=3) then jfs.flow_id end as standard_relate_id,ju.is_public"
				+ "        ,case when ju.stan_type=1 then jf.file_name"
				+ "         when (ju.stan_type=2 or ju.stan_type=3) then jfs.flow_name"
				+ "        end as file_name,ju.sort_id" + " from jecn_criterion_classes ju "
				+ " left join jecn_access_permissions jap on jap.type=2 and jap.relate_id=ju.criterion_class_id"
				+ " left join jecn_org_access_permissions joap on joap.type=2 and joap.relate_id=ju.criterion_class_id"
				+ " left join jecn_criterion_classes jc on jc.criterion_class_id=ju.pre_criterion_class_id"
				+ " left join jecn_file jf on jf.file_id=ju.related_id  and jf.del_state=0"
				+ " left join jecn_flow_structure jfs on jfs.del_state=0 and jfs.flow_id=ju.related_id"
				+ " where (ju.stan_type=1 or ju.stan_type=2 or ju.stan_type=3) and ju.project_id=" + projectId;
		// 标准名称
		if (StringUtils.isNotBlank(attenTionSearchBean.getName())) {
			sql = sql + " and ju.criterion_class_name like '%" + attenTionSearchBean.getName() + "%'";
		}
		// 密级
		if (attenTionSearchBean.getSecretLevel() != -1) {
			sql = sql + " and ju.is_public=" + attenTionSearchBean.getSecretLevel();
		}
		// 查阅岗位权限
		if (attenTionSearchBean.getPosId() != -1) {
			sql = sql + " and (jap.figure_id=" + attenTionSearchBean.getPosId();
			sql = sql + " or joap.org_id in (select jfoi.org_id from jecn_flow_org_image jfoi where jfoi.figure_id="
					+ attenTionSearchBean.getPosId() + "))";
		} else {
			sql = sql + " and (jap.figure_id in ("
					+ JecnCommonSql.findPosSql(attenTionSearchBean.getUserId(), projectId) + ")";
			sql = sql + " or joap.org_id in (" + JecnCommonSql.findOrgSql(attenTionSearchBean.getUserId(), projectId)
					+ "))";
		}
		sql = " from (" + sql + ") standard where standard.standard_relate_id is not null";
		return sql;
	}

	@Override
	public int getTotalAttenTionStandard(AttenTionSearchBean attenTionSearchBean, Long projectId) throws Exception {
		try {
			String sql = "select count (standard.criterion_class_id)";
			sql = sql + getSql(attenTionSearchBean, projectId);
			return standardDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<StandardWebBean> getAttenTionStandard(AttenTionSearchBean attenTionSearchBean, int start, int limit,
			Long projectId) throws Exception {
		try {
			String sql = "select standard.*";
			sql = sql + getSql(attenTionSearchBean, projectId);
			sql += " order by standard.pre_criterion_class_id,standard.sort_id,standard.criterion_class_id";
			List<Object[]> listObjects = standardDao.listNativeSql(sql, start, limit);
			List<StandardWebBean> listResult = new ArrayList<StandardWebBean>();
			for (Object[] obj : listObjects) {
				StandardWebBean standardWebBean = JecnUtil.getStandardWebBeanByObject(obj);
				if (standardWebBean != null) {
					listResult.add(standardWebBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnTreeBean> searchStandarFileByName(String name, Long projectId) throws Exception {
		try {
			String sql = "select t.criterion_class_id,"
					+ "t.criterion_class_name,t.pre_criterion_class_id"
					+ "       ,t.stan_type,t.related_id,(select count(*) from jecn_criterion_classes where pre_criterion_class_id=t.criterion_class_id) as count"
					+ "        from jecn_criterion_classes t where t.criterion_class_name like ? and t.STAN_TYPE in (1,4,5) and t.project_id=? order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";
			List<Object[]> list = standardDao.listNativeSql(sql, 0, 20, "%" + name + "%", projectId);
			return findByListObjects(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public int getTotalStandardListByPid(Long pId, long peopleId, long projectId, boolean isAdmin) throws Exception {
		try {

			String sql = "select count(t.criterion_class_id) from (" + getStandardListSql(pId) + ") t ";
			if (!isAdmin && !JecnContants.isCriterionAdmin) {
				sql = sql + "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.STANDARD)
						+ " on MY_AUTH_FILES.criterion_class_id=t.criterion_class_id";
			}
			return standardDao.countAllByParamsNativeSql(sql);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<StandardWebBean> getStandardListByPid(Long pId, int start, int limit, long peopleId, long projectId,
			boolean isAdmin) throws Exception {
		try {
			String sql = "select t.* from (" + getStandardListSql(pId) + ") t ";
			if (!isAdmin && !JecnContants.isCriterionAdmin) {
				sql = sql + "  inner join "
						+ JecnCommonSqlTPath.INSTANCE.getFileSearch(peopleId, projectId, TYPEAUTH.STANDARD)
						+ " on MY_AUTH_FILES.criterion_class_id=t.criterion_class_id";
			}
			sql = sql + " order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";
			List<Object[]> listObjects = standardDao.listNativeSql(sql, start, limit);
			List<StandardWebBean> listResult = new ArrayList<StandardWebBean>();
			for (Object[] obj : listObjects) {
				StandardWebBean standardWebBean = JecnUtil.getStandardWebBeanByObject(obj);
				if (standardWebBean != null) {
					listResult.add(standardWebBean);
				}
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}

	}

	/**
	 * 点击数展开的子集
	 * 
	 * @param standardId
	 * @return
	 */
	private String getStandardListSql(Long standardId) {
		List<Long> listIds = new ArrayList<Long>();
		listIds.add(standardId);
		return "  select distinct ju.criterion_class_id,ju.criterion_class_name,"
				+ "             ju.pre_criterion_class_id,"
				+ "             jc.criterion_class_name as standradWebBean, ju.stan_type,"
				+ "             case when  ju.stan_type=1 then jf.file_id "
				+ "             when ju.stan_type in (2,3) then jfs.flow_id else -1 end as standard_relate_id"
				+ "  			,ju.is_public,"
				+ "             jf.file_name, ju.t_path,ju.sort_id"
				+ "          from jecn_criterion_classes ju inner join ("
				+ JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.standardDir)
				+ ") click_Files on click_Files.criterion_class_id=ju.criterion_class_id"
				+ "   		 left join jecn_criterion_classes jc on jc.criterion_class_id = ju.pre_criterion_class_id"
				+ "          left join jecn_file jf on ju.stan_type=1 and jf.file_id = ju.related_id"
				+ "          left join jecn_flow_structure jfs  on ju.stan_type in (2,3) and jfs.flow_id = ju.related_id"
				+ "         where ju.stan_type <> 0 ";
	}

	@Override
	public StandardDetailBean getStandardDetail(Long standardId, int standardType) throws Exception {
		StandardBean standardBean = this.get(standardId);
		if (standardBean == null) {
			return null;
		}

		// 标准详情
		StandardDetailBean standardDetail = new StandardDetailBean();
		if (standardType == 1 && standardBean.getRelationId() != null) {
			JecnFileBean jecnFileBean = this.fileDao.findJecnFileBeanById(standardBean.getRelationId());
			if (jecnFileBean == null || jecnFileBean.getDelState() == null
					|| jecnFileBean.getDelState().intValue() == 1) {
				return null;
			}
			FileWebBean fileWebBean = new FileWebBean();
			fileWebBean.setFileId(jecnFileBean.getFileID());
			fileWebBean.setFileName(jecnFileBean.getFileName());
			fileWebBean.setFileNum(jecnFileBean.getDocId());
			standardDetail.setFileBean(fileWebBean);
		}
		if (standardType == 4) {
			StandardWebBean standardWebBean = new StandardWebBean();
			String content = "";
			if (standardBean.getStanContent() != null) {
				content = standardBean.getStanContent().replaceAll("\n", "<br>").replaceAll(" ", "&nbsp;");
			}
			standardWebBean.setId(standardBean.getCriterionClassId());
			standardWebBean.setName(standardBean.getCriterionClassName());
			standardWebBean.setIsProfile(standardBean.getIsproFile());
			standardWebBean.setStanContent(content);
			standardDetail.setStandardWebBean(standardWebBean);
		}
		if (standardType == 5) {
			String content = "";
			if (standardBean.getStanContent() != null) {
				content = standardBean.getStanContent().replaceAll("\n", "<br>").replaceAll(" ", "&nbsp;");
			}
			StandardBean parentStandardBean = this.get(standardBean.getPreCriterionClassId());
			StandardWebBean standardWebBean = new StandardWebBean();
			standardWebBean.setId(standardBean.getCriterionClassId());
			standardWebBean.setPname(parentStandardBean.getCriterionClassName());
			standardWebBean.setStanContent(content);
			standardDetail.setStandardWebBean(standardWebBean);
		}
		standardDetail.setSecretLevel(standardBean.getIsPublic());

		// 标准相关流程

		String sql = "SELECT JFS.FLOW_ID," + "       JFS.FLOW_NAME," + "       JFS.FLOW_ID_INPUT,"
				+ "       LEADORG.ORG_ID," + "       LEADORG.ORG_NAME," + "       JFBI.TYPE_RES_PEOPLE,"
				+ "       JFBI.RES_PEOPLE_ID," + "       CASE" + "         WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN"
				+ "          JU.TRUE_NAME" + "         WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN"
				+ "          POS.FIGURE_TEXT" + "       END AS RES_NAME"
				+ "  FROM (SELECT JFS.FLOW_ID, JFS.FLOW_NAME, JFS.FLOW_ID_INPUT"
				+ "          FROM JECN_FLOW_STRUCTURE JFS" + "         INNER JOIN JECN_FLOW_STANDARD SF"
				+ "            ON JFS.FLOW_ID = SF.FLOW_ID" + "           AND SF.CRITERION_CLASS_ID = ?"
				+ "        UNION" + "        SELECT FS.FLOW_ID, FS.FLOW_NAME, FS.FLOW_ID_INPUT"
				+ "          FROM JECN_ACTIVE_STANDARD_T AST" + "         INNER JOIN JECN_FLOW_STRUCTURE_IMAGE FSI"
				+ "            ON AST.ACTIVE_ID = FSI.FIGURE_ID" + "         INNER JOIN JECN_FLOW_STRUCTURE FS"
				+ "            ON FSI.FLOW_ID = FS.FLOW_ID" + "         WHERE AST.STANDARD_ID = ?) JFS,"
				+ "       JECN_FLOW_BASIC_INFO JFBI" + "  LEFT JOIN (SELECT JFO.ORG_ID, JFO.ORG_NAME, JFRO.FLOW_ID"
				+ "               FROM JECN_FLOW_ORG JFO, JECN_FLOW_RELATED_ORG JFRO"
				+ "              WHERE JFO.DEL_STATE = 0" + "                AND JFO.ORG_ID = JFRO.ORG_ID) LEADORG"
				+ "    ON LEADORG.FLOW_ID = JFBI.FLOW_ID" + "  LEFT JOIN JECN_USER JU" + "    ON JU.ISLOCK = 0"
				+ "   AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID" + "  LEFT JOIN (SELECT JFOI.FIGURE_ID, JFOI.FIGURE_TEXT"
				+ "               FROM JECN_FLOW_ORG ORG, JECN_FLOW_ORG_IMAGE JFOI"
				+ "              WHERE JFOI.ORG_ID = ORG.ORG_ID" + "                AND ORG.DEL_STATE = 0) POS"
				+ "    ON POS.FIGURE_ID = JFBI.RES_PEOPLE_ID" + " WHERE JFS.FLOW_ID = JFBI.FLOW_ID";
		List<Object[]> listobj = this.standardDao.listNativeSql(sql, standardId, standardId);
		// 标准相关制度
		sql = "select r.id,r.rule_name,r.rule_number,r.is_dir,o.org_id,o.org_name,t.type_name from JECN_RULE_STANDARD_t rs left join JECN_RULE r on rs.rule_id=r.id"
				+ " left join JECN_FLOW_ORG o on r.org_id=o.org_id left join JECN_FLOW_TYPE t on r.type_id=t.type_id where rs.standard_id=?";
		List<Object[]> listRule = standardDao.listNativeSql(sql, standardId);
		List<ProcessWebBean> listProcessWebBean = new ArrayList<ProcessWebBean>();
		List<RuleInfoBean> listRuleWebBean = new ArrayList<RuleInfoBean>();
		for (Object[] obj : listobj) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			ProcessWebBean processWebBean = new ProcessWebBean();
			// 相关流程ID
			processWebBean.setFlowId(Long.valueOf(obj[0].toString()));
			if (obj[1] != null) {
				processWebBean.setFlowName(obj[1].toString());
			}
			if (obj[2] != null) {
				processWebBean.setFlowIdInput(obj[2].toString());
			}
			// 责任部门
			if (obj[3] != null && obj[4] != null) {
				processWebBean.setOrgId(Long.valueOf(obj[3].toString()));
				processWebBean.setOrgName(obj[4].toString());
			}
			// 责任人
			if (obj[5] != null && obj[6] != null && obj[7] != null) {
				processWebBean.setTypeResPeople(Integer.parseInt(obj[5].toString()));
				processWebBean.setResPeopleId(Long.valueOf(obj[6].toString()));
				processWebBean.setResPeopleName(obj[7].toString());
			}
			listProcessWebBean.add(processWebBean);
		}
		if (listRule != null) {
			for (Object[] obj : listRule) {
				if (obj == null || obj[0] == null) {
					continue;
				}
				RuleInfoBean ruleInfoBean = new RuleInfoBean();
				ruleInfoBean.setRuleId(Long.valueOf(obj[0].toString()));
				if (obj[1] != null) {
					ruleInfoBean.setRuleName(obj[1].toString());// 制度名称
				}
				if (obj[2] != null) {
					ruleInfoBean.setRuleNum(obj[2].toString());// 制度编号
				}
				if (obj[3] != null) {
					ruleInfoBean.setIsDir(Integer.valueOf(obj[3].toString()));// 制度类型
				}
				if (obj[4] != null) {
					ruleInfoBean.setDutyOrgId(Long.valueOf(obj[4].toString()));// 责任部门ID
				}
				if (obj[5] != null) {
					ruleInfoBean.setDutyOrg(obj[5].toString());// 责任部门名称
				}
				if (obj[6] != null) {
					ruleInfoBean.setRuleTypeName(obj[6].toString());// 制度类别
				}
				listRuleWebBean.add(ruleInfoBean);
			}
		}
		standardDetail.setListProcessBean(listProcessWebBean);
		standardDetail.setListRule(listRuleWebBean);
		return standardDetail;
	}

	public void updateStanClauseRequire(Long id, String name, String context, Long updatePersonId) throws Exception {
		String hql = "update StandardBean set criterionClassName=?,stanContent=?,updatePeopleId=?,updateDate=? where criterionClassId=?";

		standardDao.execteHql(hql, name, context, updatePersonId, new Date(), id);
		// 添加日志
		JecnUtil.saveJecnJournal(id, name, 5, 2, updatePersonId, standardDao);
	}

	/**
	 * 更新tpath 和tlevel
	 * 
	 * @throws Exception
	 */
	private void updateTpathAndTLevel(StandardBean standardBean) throws Exception {
		// 获取上级节点
		StandardBean preStandard = standardDao.get(standardBean.getPreCriterionClassId());
		if (preStandard == null) {
			standardBean.settLevel(0);
			standardBean.settPath(standardBean.getCriterionClassId() + "-");
			standardBean.setViewSort(JecnUtil.concatViewSort("", standardBean.getSortId().intValue()));
		} else {
			standardBean.settLevel(preStandard.gettLevel() + 1);
			standardBean.settPath(preStandard.gettPath() + standardBean.getCriterionClassId() + "-");
			standardBean.setViewSort(JecnUtil.concatViewSort(preStandard.getViewSort(), standardBean.getSortId()
					.intValue()));
		}
		// 更新
		standardDao.update(standardBean);
		standardDao.getSession().flush();
	}

	@Override
	public Long addStandard(StandardBean standardBean) throws Exception {
		standardBean.setCreateDate(new Date());
		standardBean.setUpdateDate(new Date());
		Long id = standardDao.save(standardBean);
		// 更新tpath 和tlevel
		updateTpathAndTLevel(standardBean);
		orgAccessPermissionsDao.addPermissionisParentNode(standardBean.getPreCriterionClassId(), standardBean
				.getCriterionClassId());
		// 添加日志
		JecnUtil.saveJecnJournal(standardBean.getCriterionClassId(), standardBean.getCriterionClassName(), 5, 1,
				standardBean.getCreatePeopleId(), standardDao);

		// 条款附件处理
		saveFileContent(standardBean.getFileContents(), standardBean.getCriterionClassId());
		return id;
	}

	@Override
	public void updateStandardBean(StandardBean standardBean) throws Exception {
		standardBean.setUpdateDate(new Date());
		standardDao.update(standardBean);
		// 添加日志
		JecnUtil.saveJecnJournal(standardBean.getCriterionClassId(), standardBean.getCriterionClassName(), 5, 2,
				standardBean.getUpdatePeopleId(), standardDao);
		saveFileContent(standardBean.getFileContents(), standardBean.getCriterionClassId());
	}

	/**
	 * 根据标准ID获取关联的流程节点ID 和 节点类型
	 * 
	 * @param standardId
	 * @return
	 * @throws Exception
	 */
	@Override
	public Object[] getRelFlowObject(Long standardId) throws Exception {
		String sql = "SELECT FS.FLOW_ID, FS.ISFLOW" + "  FROM JECN_CRITERION_CLASSES CC"
				+ " INNER JOIN JECN_FLOW_STRUCTURE FS" + "    ON CC.RELATED_ID = FS.FLOW_ID"
				+ " WHERE CC.CRITERION_CLASS_ID = ?" + "   AND CC.STAN_TYPE IN (2, 3)";
		List<Object[]> list = this.standardDao.listNativeSql(sql, standardId);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<JecnTreeBean> getStandardByFlowId(Long flowId) throws Exception {
		List<JecnTreeBean> treeBeans = new ArrayList<JecnTreeBean>();
		try {
			String sql = "select b.criterion_class_id,b.criterion_class_name,b.pre_criterion_class_id,b.stan_type,b.related_id,"
					+ " 0 as hasChild "
					+ " from JECN_FLOW_STANDARD_T a, JECN_CRITERION_CLASSES b"
					+ " LEFT JOIN JECN_FILE_T JF ON b.stan_type=1 AND b.related_id=JF.FILE_ID AND JF.DEL_STATE=0"
					+ " where a.criterion_class_id = b.criterion_class_id" + " and a.flow_id = ?";
			List<Object[]> list = this.standardDao.listNativeSql(sql, flowId);

			treeBeans = findByListObjectsTree(list);

		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
		return treeBeans;
	}

	private void saveFileContent(List<StandardFileContent> fileContents, Long relatedId) throws Exception {
		if (fileContents == null) {
			return;
		}

		if (fileContents.size() == 0) {
			String sql = "DELETE FROM STANDARD_FILE_CONTENT WHERE RELATED_ID = ?";
			standardDao.execteNative(sql, relatedId);
			return;
		}

		List<Long> deleteIds = new ArrayList<Long>();
		List<StandardFileContent> addFileContent = new ArrayList<StandardFileContent>();

		String sql = "SELECT ID FROM STANDARD_FILE_CONTENT WHERE RELATED_ID = " + relatedId;
		List<Long> fileContentIds = standardDao.listObjectNativeSql(sql, "ID", Hibernate.LONG);

		for (StandardFileContent fileContent : fileContents) {
			if (fileContent.getId() == null) {
				addFileContent.add(fileContent);
			}
		}

		for (Long contentId : fileContentIds) {
			boolean isDelete = true;
			for (StandardFileContent fileContent : fileContents) {
				if (contentId.equals(fileContent.getId())) {
					isDelete = false;
					continue;
				}
			}
			if (isDelete) {
				deleteIds.add(contentId);
			}
		}

		// 删除文件附件
		if (deleteIds.size() > 0) {
			sql = "DELETE FROM STANDARD_FILE_CONTENT WHERE RELATED_ID = ? AND ID IN " + JecnCommonSql.getIds(deleteIds);
			standardDao.execteNative(sql, relatedId);
		}
		// 添加文件附件
		for (StandardFileContent fileContent : addFileContent) {
			fileContent.setRelatedId(relatedId);
			saveFileContent(fileContent);
		}
	}

	private void saveFileContent(StandardFileContent fileContent) throws Exception {
		// 文件保存数据库
		fileContent.setCreateTime(new Date());
		// 存储数据库，标识为1
		fileContent.setIsVersionLocal(1);
		if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
			fileContent.setFileStream(Hibernate.createBlob(fileContent.getBytes()));
			this.standardDao.getSession().save(fileContent);
		} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
			fileContent.setFileStream(Hibernate.createBlob(new byte[1]));
			standardDao.getSession().save(fileContent);
			standardDao.getSession().flush();
			standardDao.getSession().refresh(fileContent, LockMode.UPGRADE);
			SerializableBlob sb = (SerializableBlob) fileContent.getFileStream();
			java.sql.Blob wrapBlob = sb.getWrappedBlob();
			BLOB blob = (BLOB) wrapBlob;
			try {
				OutputStream out = blob.getBinaryOutputStream();
				out.write(fileContent.getBytes());
				out.close();
			} catch (SQLException ex) {
				log.error("写blob大字段错误！", ex);
				throw ex;
			} catch (IOException ex) {
				log.error("写blob大字段错误！", ex);
				throw ex;
			}
		}
	}

	private void saveVersionFileToLocal(Long id, Long updatePersonId) throws Exception {
		// 1、获取版本文件
		StandardFileContent fileContent = (StandardFileContent) standardDao.getSession().get(StandardFileContent.class,
				id);
		byte[] bytes = JecnDaoUtil.blobToBytes(fileContent.getFileStream());
		if (bytes == null) {
			log.error("saveVersionFileToLocal 保存制度版本文件到本地异常！");
			throw new IllegalArgumentException("保存制度版本文件到本地异常！");
		}
		fileContent.setBytes(bytes);
		// 2、更新版本文件标识
		fileContent.setIsVersionLocal(0);
		fileContent.setCreateTime(new Date());
		fileContent.setCreatePeopleId(updatePersonId);
		fileContent.setFileStream(null);
		// 3、保存版本文件到本地
		String filePath = JecnFinal.saveFileToLocal(bytes, fileContent.getFileName(), JecnFinal.STANDARD_FILE_RELEASE);
		if (filePath == null) {
			throw new IllegalAnnotationException("JecnFinal.saveFileToLocal获取文件路径非法!");
		}
		fileContent.setFilePath(filePath);

		standardDao.getSession().update(fileContent);
		standardDao.getSession().flush();
	}

	@Override
	public List<StandardFileContent> getStandardFileContents(Long relatedId) throws Exception {
		String sql = "SELECT ID,FILE_NAME FROM STANDARD_FILE_CONTENT WHERE RELATED_ID = ? ";
		List<Object[]> list = this.standardDao.listNativeSql(sql, relatedId);
		List<StandardFileContent> fileContents = new ArrayList<StandardFileContent>();
		StandardFileContent fileContent = null;
		for (Object[] objects : list) {
			fileContent = new StandardFileContent();
			fileContent.setId(Long.valueOf(objects[0].toString()));
			fileContent.setFileName(objects[1].toString());
			// fileContent.setBytes(JecnDaoUtil.blobToBytes((Blob)objects[1]));
			fileContents.add(fileContent);
		}
		return fileContents;
	}

	/**
	 * 打开标准附件
	 */
	@Override
	public FileOpenBean openFileContent(Long id) throws Exception {
		try {
			FileOpenBean fileOpenBean = new FileOpenBean();
			String hql = "from StandardFileContent where id=?";
			StandardFileContent fileContent = standardDao.getObjectHql(hql, id);
			if (fileContent != null) {
				fileOpenBean.setId(fileContent.getId());
				fileOpenBean.setFileByte(JecnDaoUtil.blobToBytes(fileContent.getFileStream()));
				fileOpenBean.setName(fileContent.getFileName());
			}
			return fileOpenBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public ByteFileResult importData(long userId, byte[] b, Long id, String type, Long projectId, boolean CH) {
		try {
			File file = JecnFinal.writeFileToTempDir(b);
			if ("standard".equals(type.toLowerCase())) {
				return importStandard(userId, file, id, projectId, CH);
			} else if ("risk".equals(type.toLowerCase())) {
				return importRisk(userId, file, id, projectId, CH);
			}
		} catch (Exception e) {
			log.error("导入异常：id:" + id + " type:" + type, e);
		}
		return null;
	}

	private ByteFileResult importRisk(long userId, File file, Long id, Long projectId, boolean CH) {
		INodeImportService<JecnRisk> service = new RiskNodeImportService(userId, id, file, projectId, baseDao, CH);
		return service.save();
	}

	private ByteFileResult importStandard(long userId, File file, Long id, Long projectId, boolean CH) {
		INodeImportService<StandardBean> service = new StandardNodeImportService(userId, id, file, projectId, baseDao,
				CH);
		return service.save();
	}

}
