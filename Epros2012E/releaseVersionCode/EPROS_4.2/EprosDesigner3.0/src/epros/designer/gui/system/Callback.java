package epros.designer.gui.system;

public interface Callback<T> {
	public T call(Object obj);
}
