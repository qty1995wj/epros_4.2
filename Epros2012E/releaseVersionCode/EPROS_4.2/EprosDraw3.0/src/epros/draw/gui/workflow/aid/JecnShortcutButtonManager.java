package epros.draw.gui.workflow.aid;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import epros.draw.gui.workflow.JecnDrawMainPanel;

/**
 * 
 * 快捷按钮管理类
 * 
 * 主组件：被操作组件，通过其变化能改变辅助组件的显示位置等等状态。
 * 
 * 辅助组件：是通过主组件来操作控制的。
 * 
 * 
 * @author ZHOUXY
 * 
 */
public class JecnShortcutButtonManager {
	/** 进入计时 */
	private Timer enterTimer;
	/** 退出计时 */
	private Timer exitTimer;
	/** 快捷添加图形的主类 */
	private JecnLinkFigureMainProcess linkFigurePanel = null;
	/** 主组件:四个小三角形中其中一个 */
	private JecnLinkTrianglesButton linkTrianglesButton = null;

	public JecnShortcutButtonManager(JecnLinkFigureMainProcess linkFigurePanel) {
		this.linkFigurePanel = linkFigurePanel;

		// 进入计时
		enterTimer = new Timer(1, new EnterTimerActionListener());
		enterTimer.setRepeats(false);
		// 退出计时
		exitTimer = new Timer(1000, new ExitTimerActionListener());
		exitTimer.setRepeats(false);
	}

	/**
	 * 
	 * 注册JecnLinkTrianglesButton
	 * 
	 * @param component
	 *            JecnLinkTrianglesButton 组件
	 * 
	 */
	public void registerComponent(JecnLinkTrianglesButton component) {
		if (linkTrianglesButton != null && linkTrianglesButton == component) {// 是同一个对象
		} else {
			this.linkTrianglesButton = component;
		}

		// 退出时间停止
		exitTimer.stop();

		// 进入时间开始
		if (enterTimer.isRunning()) {
			enterTimer.restart();
		} else {
			enterTimer.start();
		}
	}

	/**
	 * 
	 * 刷新时间事件:用于进入快捷面板时
	 * 
	 * 
	 */
	public void refreshComponent() {
		if (linkTrianglesButton != null) {
			// 退出时间停止
			exitTimer.stop();

			// 进入时间开始
			if (enterTimer.isRunning()) {
				enterTimer.restart();
			} else {
				enterTimer.start();
			}
		}
	}

	/**
	 * 
	 * 取消JComponent
	 * 
	 */
	public void unregisterComponent() {
		// 进入时间停止
		enterTimer.stop();

		// 退出时间开始
		if (exitTimer.isRunning()) {
			exitTimer.restart();
		} else {
			exitTimer.start();
		}
	}

	/**
	 * 
	 * 立即停止所有线程
	 * 
	 */
	public void stopInteng() {
		// 停止时间线程
		this.enterTimer.stop();
		this.exitTimer.stop();

		// 快捷面板不显示
		linkFigurePanel.getLinkPanel().setVisible(false);

		// 主组件
		linkTrianglesButton = null;
	}

	/**
	 * 
	 * 进入组件触发的动作事件类
	 * 
	 * @author ZHOUXY
	 * 
	 */
	private class EnterTimerActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			enterTimerActionPerformed(e);
		}
	}

	/**
	 * 
	 * 退出组件触发的动作事件类
	 * 
	 * @author ZHOUXY
	 * 
	 */
	private class ExitTimerActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			exitTimerActionPerformed(e);
		}
	}

	/**
	 * 
	 * 进入组件触发的动作事件类
	 * 
	 * @param e
	 */
	private void enterTimerActionPerformed(ActionEvent e) {
		if (linkTrianglesButton != null && linkTrianglesButton.isShowing()) {// 主组件不为空且显示在屏幕上时
			if (!linkFigurePanel.getLinkPanel().isVisible()) {
				// 显示
				linkFigurePanel.getLinkPanel().setVisible(true);

				// 设置层级，此层级为当前画图面板最大层级
				JecnDrawMainPanel.getMainPanel().setFlowElemPanelLayer(
						linkFigurePanel.getLinkPanel());
			}

			// 停止退出动作事件
			exitTimer.stop();

			// 设置小三角形的大小和位置点
			linkFigurePanel.getLinkPanel().resetLinkPanelLocationSize(
					linkTrianglesButton);
		}
	}

	/**
	 * 
	 * 退出组件触发的动作事件类
	 * 
	 * @param e
	 */
	private void exitTimerActionPerformed(ActionEvent e) {
		// 快捷面板不显示
		linkFigurePanel.getLinkPanel().setVisible(false);

		// 主组件
		linkTrianglesButton = null;
	}
}
