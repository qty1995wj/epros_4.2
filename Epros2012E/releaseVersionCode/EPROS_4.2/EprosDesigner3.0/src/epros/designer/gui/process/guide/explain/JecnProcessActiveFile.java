package epros.designer.gui.process.guide.explain;

import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;

import com.jecn.epros.server.bean.download.ProcessDownloadBaseBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;

/**
 * 流程文件，操作规范
 * 
 * @author user
 * 
 */
public class JecnProcessActiveFile extends JecnFileDescriptionTable {

	private ProcessOperationDialog dialog = null;
	/** 0是流程记录（活动输入和输出的集合），1是活动操作说明 */
	private int type = -1;
	private JecnFlowBasicInfoT flowBasicInfoT;

	public JecnProcessActiveFile(int index, String name, boolean isRequest, JecnPanel contentPanel,
			ProcessOperationDialog dialog, int type, String tip, JecnFlowBasicInfoT flowBasicInfoT) {
		super(index, name, isRequest, contentPanel, tip);
		this.dialog = dialog;
		this.type = type;
		this.flowBasicInfoT = flowBasicInfoT;
		this.initTable();
		table.setTableColumn(1, 180);
	}

	@Override
	protected void dbClickMethod() {
	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("fileNumbers"));
		title.add(JecnProperties.getValue("fileName"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getFlowActiveOperationFiles() {
		// 操作规范
		Vector<Vector<String>> practicesContent = new Vector<Vector<String>>();
		try {
			// 为了数据顺序一致性只能再次查询数据库
			ProcessDownloadBaseBean downloadBean = ConnectionPool.getProcessAction().getProcessDownloadBean(
					flowBasicInfoT.getFlowId(), false, "a12");
			List<Object[]> list = downloadBean.getOperationTemplateList();
			for (Object[] objs : list) {
				Vector<String> vs = new Vector<String>();
				vs.add("0");
				vs.add(JecnUtil.nullToEmpty(objs[0]));
				vs.add(JecnUtil.nullToEmpty(objs[1]));
				practicesContent.add(vs);
			}
		} catch (Exception e) {
			throw new RuntimeException("", e);
		}

		// // 存储文件ID
		// Set<Long> fileIdList = new HashSet<Long>();
		// List<JecnActivityShowBean> listAllActivityShow =
		// dialog.getFlowOperationBean().getListAllActivityShow();
		// if (listAllActivityShow != null && listAllActivityShow.size() > 0) {
		//
		// for (JecnActivityShowBean activityShowBean : listAllActivityShow) {
		// // 输入 和 操作规范
		// for (JecnActivityFileT activityFileT :
		// activityShowBean.getActiveFigure().getFlowElementData()
		// .getListJecnActivityFileT()) {
		// // 判断为操作规范
		// if (activityFileT.getFileType() != null &&
		// activityFileT.getFileType().intValue() == 1) {
		// if (!fileIdList.contains(activityFileT.getFileSId())) {
		// Vector<String> practicesContentStr = new Vector<String>();
		// // 文件ID
		// practicesContentStr.add(String.valueOf(activityFileT.getFileSId()));
		// // 文件编号
		// practicesContentStr.add(activityFileT.getFileNumber());
		// // 文件名称
		// practicesContentStr.add(activityFileT.getFileName());
		// practicesContent.add(practicesContentStr);
		// fileIdList.add(activityFileT.getFileSId());
		// }
		// }
		// }
		// }
		// }
		return practicesContent;
	}

	private Vector<Vector<String>> getFlowActiveInputAndOutputModeFiles() {
		// 操作规范
		Vector<Vector<String>> flowFileContent = new Vector<Vector<String>>();
		try {
			// 为了数据顺序一致性只能再次查询数据库
			ProcessDownloadBaseBean downloadBean = ConnectionPool.getProcessAction().getProcessDownloadBean(
					flowBasicInfoT.getFlowId(), false, "a11");
			List<Object[]> list = downloadBean.getProcessRecordList();
			for (Object[] objs : list) {
				Vector<String> vs = new Vector<String>();
				vs.add("0");
				vs.add(JecnUtil.nullToEmpty(objs[0]));
				vs.add(JecnUtil.nullToEmpty(objs[1]));
				flowFileContent.add(vs);
			}
		} catch (Exception e) {
			throw new RuntimeException("", e);
		}
		// List<JecnActivityShowBean> listAllActivityShow =
		// dialog.getFlowOperationBean().getListAllActivityShow();
		// if (listAllActivityShow != null && listAllActivityShow.size() > 0) {
		// // 存储文件ID
		// Set<Long> fileIdList = new HashSet<Long>();
		// for (JecnActivityShowBean activityShowBean : listAllActivityShow) {
		// // 输出
		// for (JecnModeFileT modeFileT :
		// activityShowBean.getActiveFigure().getFlowElementData()
		// .getListModeFileT()) {
		// if (!fileIdList.contains(modeFileT.getFileMId())) {
		// Vector<String> flowFileContentStr = new Vector<String>();
		// // 文件ID
		// flowFileContentStr.add(String.valueOf(modeFileT.getFileMId()));
		// // 文件编号
		// flowFileContentStr.add(modeFileT.getFileNumber());
		// // 文件名称
		// flowFileContentStr.add(modeFileT.getModeName());
		// flowFileContent.add(flowFileContentStr);
		// fileIdList.add(modeFileT.getFileMId());
		// }
		// }
		// // 输入 和 操作规范
		// for (JecnActivityFileT activityFileT :
		// activityShowBean.getActiveFigure().getFlowElementData()
		// .getListJecnActivityFileT()) {
		// // 判断为输入
		// if (activityFileT.getFileType() != null &&
		// activityFileT.getFileType().intValue() == 0) {
		// if (!fileIdList.contains(activityFileT.getFileSId())) {
		// Vector<String> flowFileContentStr = new Vector<String>();
		// // 文件ID
		// flowFileContentStr.add(String.valueOf(activityFileT.getFileSId()));
		// // 文件编号
		// flowFileContentStr.add(activityFileT.getFileNumber());
		// // 文件名称
		// flowFileContentStr.add(activityFileT.getFileName());
		// flowFileContent.add(flowFileContentStr);
		// fileIdList.add(activityFileT.getFileSId());
		// }
		// }
		// }
		// }
		// }
		return flowFileContent;
	}

	/**
	 * 初始化数据显示
	 * 
	 * @author fuzhh Oct 31, 2012
	 * @return
	 */
	public Vector<Vector<String>> getContent() {
		// 流程记录
		switch (type) {
		case 0:
			return getFlowActiveInputAndOutputModeFiles();
		case 1:
			return getFlowActiveOperationFiles();
		default:
			return new Vector<Vector<String>>();
		}
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
