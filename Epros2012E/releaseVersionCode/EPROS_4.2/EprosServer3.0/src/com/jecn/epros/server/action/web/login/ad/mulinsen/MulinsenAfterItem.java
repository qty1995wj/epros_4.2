package com.jecn.epros.server.action.web.login.ad.mulinsen;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnPath;

public class MulinsenAfterItem {
	private static Logger log = Logger.getLogger(MulinsenAfterItem.class);

	/** 单点请求地址 */
	public static String SSO_ADDRESS = null;
	public static String SSOLoginUrl = null;
	public static String OA_KEY = null;
	public static String OA_URL = null;
	public static String APP_NAME = null;
	public static String APP_KEY = null;
	public static String APP_URL = null;

	public static void start() {
		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/mulinsen/mulinsenConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);
			SSO_ADDRESS = props.getProperty("SSO_ADDRESS");
			SSOLoginUrl = props.getProperty("SSOLoginUrl");
			OA_KEY = props.getProperty("OA_KEY");
			OA_URL = props.getProperty("OA_URL");
			APP_NAME = props.getProperty("APP_NAME");
			APP_KEY = props.getProperty("APP_KEY");
			APP_URL = props.getProperty("APP_URL");
		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);

			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);

			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		}
	}
}