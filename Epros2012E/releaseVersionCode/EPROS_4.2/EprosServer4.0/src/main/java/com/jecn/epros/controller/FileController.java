package com.jecn.epros.controller;

import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.file.FileBaseInfoBean;
import com.jecn.epros.domain.file.FileUseDetailBean;
import com.jecn.epros.domain.file.FileViewBean;
import com.jecn.epros.domain.file.SearchFileResultBean;
import com.jecn.epros.domain.process.SearchBean;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.FileService;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.AttachmentDownloadUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "/file", description = "文件体系", position = 6)
public class FileController {

    @Autowired
    private FileService fileService;

    @ApiOperation(value = "文件-搜索")
    @RequestMapping(value = "/files", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FileViewBean> search(@ModelAttribute SearchBean searchBean, @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("searchBean", searchBean);
        map.put("paging", paging);
        FileViewBean ruleViewBean = new FileViewBean();
        int total = fileService.searchFileTotal(map);
        List<SearchFileResultBean> listSearchRuleResultBean = fileService.searchFile(map);
        ruleViewBean.setTotal(total);
        ruleViewBean.setData(listSearchRuleResultBean);
        return ResponseEntity.ok(ruleViewBean);
    }

    @PreAuthorize(" hasAuthority('fileBaseInfo')")
    @ApiOperation(value = "文件-概况信息")
    @RequestMapping(value = "/files/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FileBaseInfoBean> findBaseInfo(
            @ApiParam(value = "文件 Id", required = true) @PathVariable Long id,
            @ApiParam(value = "是否已发布 (0:未发布 1:已发布)", allowableValues = "0,1", defaultValue = "1") @RequestParam(defaultValue = "1")
                    int isPublish, @RequestParam Long historyId) {
        Map<String, Object> map = getFileMap(isPublish, id);
        map.put("historyId", historyId);
        FileBaseInfoBean baseInfo = fileService.findBaseInfoBean(map);
        baseInfo.setHistoryId(historyId);
        return ResponseEntity.ok(baseInfo);
    }

    @ApiOperation(value = "文件-树节点展开")
    @RequestMapping(value = "/files/{id}/children", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNode>> fetchChildren(@ApiParam(value = "父节点Id", required = true) @PathVariable Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        // 父节点ID
        map.put("pid", id);
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        return ResponseEntity.ok(fileService.findChildFiles(map));

    }

    @PreAuthorize(" hasAuthority('fileUsage')")
    @ApiOperation(value = "文件-使用情况")
    @RequestMapping(value = "/files/{id}/usage", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FileUseDetailBean>> findFileUsage(@PathVariable Long id, @RequestParam int isPublish) {
        Map<String, Object> map = getFileMap(isPublish, id);
        return ResponseEntity.ok(fileService.findFileUseDetail(map));
    }

    private Map<String, Object> getFileMap(int isPub, Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        map.put("isPub", isPub == 1 ? "" : Common.PUB_T_STR);
        return map;
    }

    @ApiOperation(value = "文件-下载-DEMO")
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public ResponseEntity<?> downloadFile(@RequestParam Long id,
                                          HttpServletRequest request) {
        Attachment attachment = fileService.fetchAttachmentById(id);
        if (attachment == null) {
            return ResponseEntity.notFound().build();
        }
        return AttachmentDownloadUtils.newResponseEntity(request, attachment);
    }

    @ApiOperation(value = "文件-上传-DEMO")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<?> uploadFile(@RequestParam("uploadfile") MultipartFile uploadfile) {
        try {
            System.out.println(uploadfile.getOriginalFilename());
            System.out.println(uploadfile.getBytes().length);
            System.out.println(uploadfile.getContentType());
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

}
