package com.jecn.webservice.crs.task;

/**
 * 株洲南车，任务待办对象
 * 
 *@author ZXH
 * @date 2016-9-19上午11:51:04
 */
public class CsrTaskBean {
	/** */
	private String PSCODE = "EPROS";
	/** 任务编码 */
	private String PCODE;
	/** 任务标题 */
	private String PTITLE;
	/** 待办时间 */
	private String PDATE;
	/** 待办人登录名 */
	private String PPRINCIPAL;
	/** 任务URL */
	private String PURL;
	/** 待办发起人标示 */
	private String PORANIGER;
	/** 待办类别 */
	private String PTYPE;
	/** 任务主键 */
	private String PSCODEZH = "流程管理平台";

	public String getPSCODE() {
		return PSCODE;
	}

	public void setPSCODE(String pSCODE) {
		PSCODE = pSCODE;
	}

	public String getPCODE() {
		return PCODE;
	}

	public void setPCODE(String pCODE) {
		PCODE = pCODE;
	}

	public String getPTITLE() {
		return PTITLE;
	}

	public void setPTITLE(String pTITLE) {
		PTITLE = pTITLE;
	}

	public String getPDATE() {
		return PDATE;
	}

	public void setPDATE(String pDATE) {
		PDATE = pDATE;
	}

	public String getPPRINCIPAL() {
		return PPRINCIPAL;
	}

	public void setPPRINCIPAL(String pPRINCIPAL) {
		PPRINCIPAL = pPRINCIPAL;
	}

	public String getPURL() {
		return PURL;
	}

	public void setPURL(String pURL) {
		PURL = pURL;
	}

	public String getPORANIGER() {
		return PORANIGER;
	}

	public void setPORANIGER(String pORANIGER) {
		PORANIGER = pORANIGER;
	}

	public String getPTYPE() {
		return PTYPE;
	}

	public void setPTYPE(String pTYPE) {
		PTYPE = pTYPE;
	}

	public String getPSCODEZH() {
		return PSCODEZH;
	}

	public void setPSCODEZH(String pSCODEZH) {
		PSCODEZH = pSCODEZH;
	}

	public String toString() {
		return "PTITLE = " + PTITLE + "PURL = " + PURL;
	}
}
