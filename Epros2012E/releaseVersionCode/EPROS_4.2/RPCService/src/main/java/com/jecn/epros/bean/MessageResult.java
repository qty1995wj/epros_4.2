package com.jecn.epros.bean;

import java.io.Serializable;

/**
 * 信息返回
 * 
 * @author user
 * 
 */
public class MessageResult implements Serializable,Result{

	private boolean isSuccess=true;
	private String resultMessage = "";
	
	public MessageResult(){
		
	}
	public MessageResult(boolean isSuccess,String resultMessage){
		this.isSuccess=isSuccess;
		this.resultMessage=resultMessage;
	}
	
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
}
