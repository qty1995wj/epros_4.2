/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnImplData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.PaintImplFigure;

/**
 * 上游流程、下游流程、子流程和过程接口流程
 * 
 * @author zhangxh
 * @date： 日期：Mar 22, 2012 时间：10:51:41 AM
 */
public class ImplFigure extends JecnBaseFigurePanel {

	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int x5;
	private int x6;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private int y5;
	private int y6;

	private PaintImplFigure figure = null;

	public ImplFigure(JecnFigureData flowElementData) {
		super(flowElementData);
		figure = new PaintImplFigure(this);
	}

	@Override
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		x1 = userWidth / 5;
		y1 = (userHeight - userHeight / 5) / 2 + userHeight / 5;
		x2 = userWidth / 5 + 20 - 10;
		y2 = userHeight / 5;
		x3 = userWidth - 20 + 10;
		y3 = userHeight / 5;
		x4 = userWidth;
		y4 = (userHeight - userHeight / 5) / 2 + userHeight / 5;
		x5 = userWidth - 20 + 10;
		y5 = userHeight;
		x6 = userWidth / 5 + 20 - 10;
		y6 = userHeight;
		paintFigure(g);
	}

	public JecnImplData getFlowElementData() {
		JecnImplData implData = (JecnImplData) flowElementData;
		return implData;
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		figure.paintNoneFigure(g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, userWidth, userHeight);
	}

	/**
	 * 
	 * 阴影效果 （包含原图）
	 */
	public void paintShadowFigure(Graphics2D g2d) {
		if (flowElementData.getFillType().equals(FillType.none)) {
			// 不存在填充色
			paintNoneFigure(g2d);
			return;
		}
		// 阴影
		// paintShadows(g2d, 1);
		//
		// // 阴影矩形
		// paintShadows(g2d, 0);
		// addShadowPanel();

		// 顶层 (分六边形和矩形两部分)
		// 六边形
		paintImplTop(g2d, shadowCount, 1);

		// 顶层 矩形
		paintImplTop(g2d, shadowCount, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 原图
		// 六边形
		paintImplTop(g2d, 0, 1);
		// 矩形
		paintImplTop(g2d, 0, 0);
	}

	/**
	 * 既有阴影也有3D效果
	 * 
	 * @param g
	 */
	public void paintShadowAnd3DFigure(Graphics2D g2d) {
		if (flowElementData.getFillType().equals(FillType.none)) {
			// 不存在填充色
			paintNoneFigure(g2d);
			return;
		}
		// 设置六边形 3D效果
		// 阴影效果 六边形
		// paintShadows(g2d, 1);
		// // 矩形3D和阴影设置
		// paintShadows(g2d, 0);
		// addShadowPanel();

		paint3DFigure(g2d);
	}

	/**
	 * 
	 * 3D背景渐变色
	 * 
	 * @param g
	 */
	public void paint3DFigure(Graphics2D g2d) {
		if (flowElementData.getFillType().equals(FillType.none)) {
			// 不存在填充色
			paintNoneFigure(g2d);
			return;
		}
		// 设置六边形 3D效果
		// 六边形底层填充
		paintImpl3DLow(g2d, 0, 1);
		// 六边形边框线
		paintImpl3DLine(g2d, 0, 1);
		// 六边形顶层填充
		paintImpl3DPartTop(g2d, 1);

		// 矩形3D设置
		// 六边形底层填充
		paintImpl3DLow(g2d, 0, 0);
		// 六边形边框线
		paintImpl3DLine(g2d, 0, 0);
		// 六边形顶层填充
		paintImpl3DPartTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintImplTop(Graphics2D g2d, int shadowCount, int type) {
		// 顶层 原图，榜值均为零
		// 顶层颜色渐变
		paintBackColor(g2d);
		figure.paintImplTop(g2d, shadowCount, type, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, userWidth,
				userHeight);
	}

	/**
	 * 3D效果底层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时缩进值
	 * @param type
	 *            1:六边形
	 */
	private void paintImpl3DLow(Graphics2D g2d, int indent3Dvalue, int type) {
		paint3DFig(g2d);
		figure.paintImpl3DLow(g2d, indent3Dvalue, type, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, userWidth,
				userHeight);
	}

	/**
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 * @param type
	 *            1:六边形
	 */
	private void paintImpl3DLine(Graphics2D g2d, int indent3Dvalue, int type) {
		// 3D线条颜色
		paintLineColor(g2d);
		figure.paintImpl3DLine(g2d, indent3Dvalue, type, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, userWidth,
				userHeight);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	private void paintImpl3DPartTop(Graphics2D g2d, int type) {
		// 顶层 颜色填充,渐变色
		paintBackColor(g2d);
		figure.paintImpl3DPartTop(g2d, type, x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, userWidth, userHeight);
	}
}
