package epros.designer.gui.rule;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;
import com.jecn.epros.server.bean.rule.AddRuleFileData;
import com.jecn.epros.server.bean.rule.RuleStandardizedFileT;
import com.jecn.epros.server.bean.rule.RuleT;

import epros.designer.gui.autoNum.MengNiuAutoNumSelectDialog;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.JecnSystemData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 自动编号方式上传本地制度文件
 * 
 * @author ZXH
 * 
 */
public class MengNiuUploadRuleFileDialog extends G020UploadRuleFileDialog {

	private String viewCode = null;
	private int codeTotal = 0;

	private JecnTreeNode selectNode;

	private JButton uploadBut;
	protected JTextField viewText;
	protected JTextField ruleLocalText;

	protected JButton selectCodeBut;

	private byte[] fileContent;

	private List<RuleStandardizedFileT> standardizedFileTs;

	public MengNiuUploadRuleFileDialog(JecnTreeNode pNode, JecnTree jTree) {
		super(pNode, jTree);
		this.selectNode = pNode;
		initData();

		// 添加标准化文件
		filePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("addStandardizedFile")));// 添加文件
	}

	private void initData() {
		try {
			// 制度目录-编号规则获取
			AutoCodeBean codeBean = ConnectionPool.getAutoCodeAction().getAutoCodeBean(
					selectNode.getJecnTreeBean().getId(), 2);
			if (codeBean == null) {
				return;
			}
			String viewCode = MengNiuAutoNumSelectDialog.getResultAutoCode(codeBean, true);
			showViewCode(viewCode + "##" + (codeBean.getCodeTotal()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void initCompotents() {
		super.initCompotents();
		this.setTitle(JecnProperties.getValue("newRuleFile"));
		this.setSize(getWidthMax(), 750);
	}

	private void showViewCode(String viewSelectCode) {
		if (StringUtils.isNotBlank(viewSelectCode)) {
			String resultCode = "";
			if (!viewSelectCode.contains("##")) {
				resultCode = viewSelectCode;
			} else {
				String[] strs = viewSelectCode.split("##");
				viewCode = strs[0];
				codeTotal = Integer.valueOf(strs[1]);
				resultCode = getCurFileCode();
			}
			MengNiuUploadRuleFileDialog.this.viewText.setText(resultCode);
		}
	}

	private String getCurFileCode() {
		return viewCode.replace("#", codeTotal + "");
	}

	protected void customPanel(int rows, JecnPanel mainPanel, Insets insets) {

		initChildComponents();
		initChildListener();
		JecnPanel rulePanel = new JecnPanel(getWidthMax() - 10, 100);
		TitledBorder border = BorderFactory.createTitledBorder(JecnProperties.getValue("selectRuleNumUpRuleFile"));
		border.setTitleColor(Color.red);
		border.setTitleFont(new Font("宋体", Font.PLAIN, 14));
		rulePanel.setBorder(border);
		// 自动编号预览
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(rulePanel, c);
		initRulePanel(rulePanel, insets);
	}

	private void initRulePanel(JecnPanel filePanel, Insets insets) {
		filePanel.setLayout(new GridBagLayout());

		// 编号
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		filePanel.add(viewText, c);

		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		filePanel.add(selectCodeBut, c);

		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		filePanel.add(new CommonJlabelMustWrite(), c);

		// 上传制度文件
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		filePanel.add(ruleLocalText, c);

		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		filePanel.add(uploadBut, c);

		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		filePanel.add(new CommonJlabelMustWrite(), c);
	}

	private void initChildComponents() {
		viewText = new JTextField();
		ruleLocalText = new JTextField();

		selectCodeBut = new JButton(JecnProperties.getValue("selectNum"));
		// 上传文件
		uploadBut = new JButton(JecnProperties.getValue("ruleOperation"));

		viewText.setEditable(false);
		ruleLocalText.setEditable(false);

	}

	private void initChildListener() {

		uploadBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				uploadFile();
			}
		});

		selectCodeBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MengNiuAutoNumSelectDialog selectDialog = new MengNiuAutoNumSelectDialog(pNode,
						MengNiuUploadRuleFileDialog.this, true);
				selectDialog.setVisible(true);
				if (StringUtils.isNotBlank(selectDialog.getViewCode())) {
					showViewCode(selectDialog.getViewCode());
				}
			}
		});
	}

	/**
	 * 上传附件-检测
	 */
	protected boolean checkUploadFile() {
		return false;
	}

	protected AddRuleFileData getRueFileData() {
		AddRuleFileData ruleFileData = super.getRueFileData();
		ruleFileData.setStandardizedFileTs(standardizedFileTs);
		// 保存当前值
		ruleFileData.setTotalCode(codeTotal);
		return ruleFileData;
	}

	void uploadFile() {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSavaRuleUpdateDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}

		// 文件选择器
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);

		fileChoose.setEidtTextFiled(false);

		// 单选
		fileChoose.setMultiSelectionEnabled(false);
		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("fileUpload"));
		int i = fileChoose.showSaveDialog(this);
		if (i == JFileChooser.APPROVE_OPTION) {
			File file = fileChoose.getSelectedFile();
			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();

			// 附件内容
			fileContent = JecnUtil.changeFileToByte(file.getPath());
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSavaRuleUpdateDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();

			this.ruleLocalText.setText(JecnUtil.getFileName(file.getName()));
		}
	}

	/**
	 * 点击确定执行的方法
	 */
	@Override
	protected boolean initAndCheckRuleList() {
		verfyLab.setText("");

		if (StringUtils.isBlank(ruleLocalText.getText())) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("upRule_upFile"));
			return false;
		}

		if (StringUtils.isBlank(viewText.getText())) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("numberIsNull"));
			return false;
		}

		DefaultTableModel mode = (DefaultTableModel) fileTable.getModel();

		if (fileTable.isEditing()) {
			fileTable.getCellEditor().stopCellEditing();
		}
		int rows = fileTable.getRowCount();
		RuleT ruleT = getRuleT();
		list.add(ruleT);
		names.add(ruleT.getRuleName());

		standardizedFileTs = new ArrayList<RuleStandardizedFileT>();

		RuleStandardizedFileT standardizedFileT = null;
		// 相关标准化文件
		for (int i = 0; i < rows; i++) {
			standardizedFileT = new RuleStandardizedFileT();
			String s = JecnUserCheckUtil.checkNameLength((String) mode.getValueAt(i, 0));
			if (!DrawCommon.isNullOrEmtryTrim(s)) {
				verfyLab.setText(JecnProperties.getValue("fileNum") + s);
				return false;
			}

			// 编号
			if (mode.getValueAt(i, 0) != null && !"".equals(mode.getValueAt(i, 0).toString().trim())) {
				standardizedFileT.setFileCode(((String) mode.getValueAt(i, 0)));
			}
			standardizedFileT.setFileStream(JecnUtil.changeFileToByte((String) mode.getValueAt(i, 2)));

			standardizedFileT.setFileName((String) mode.getValueAt(i, 1));
			standardizedFileTs.add(standardizedFileT);
		}
		return true;

	}

	private RuleT getRuleT() {
		RuleT ruleT = new RuleT();
		ruleT.setPerId(pNode.getJecnTreeBean().getId());
		// 制度名称
		ruleT.setRuleName(ruleLocalText.getText());
		// 制度编号
		ruleT.setRuleNumber(viewText.getText());

		if (categoryCommon != null) {
			// 制度类别
			ruleT.setTypeId(this.categoryCommon.getTypeResultId());
		}
		if (resDepartSelectCommon != null) {
			// 责任部门
			ruleT.setOrgId(this.resDepartSelectCommon.getIdResult());
		}
		// 专员
		if (commissionerPeopleSelectCommon != null) {
			ruleT.setCommissionerId(commissionerPeopleSelectCommon.getIdResult());
		}
		// 关键字
		if (this.keyWordField != null) {
			ruleT.setKeyword(keyWordField.getResultStr());
		}
		if (personliableSelect != null) {
			// 制度责任人id
			ruleT.setAttchMentId(personliableSelect.getSingleSelectCommon().getIdResult());
			// 制度责任人类型
			ruleT.setTypeResPeople(personliableSelect.getPeopleResType());
		}
		// 监护人
		if (guardianPeopleSelectCommon != null) {
			ruleT.setGuardianId(guardianPeopleSelectCommon.getIdResult());
		}
		ruleT.setCreatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		ruleT.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		ruleT.setIsDir(2);// (0是目录，1是制度,2是制度文件)
		// 密级
		ruleT.setIsPublic(accessAuthorityCommon.getPublicStatic() == 0 ? 0L : 1L);
		ruleT.setSortId(JecnTreeCommon.getMaxSort(pNode) + 1);
		ruleT.setProjectId(JecnConstants.projectId);

		ruleT.setFileBytes(fileContent);
		ruleT.setIsFileLocal(1);
		// 保密级别
		ruleT.setConfidentialityLevel(accessAuthorityCommon.getConfidentialityLevelStatic());
		return ruleT;
	}

	/**
	 * 添加附件 (编号特殊处理)
	 */
	@SuppressWarnings("unchecked")
	protected void addLocalFiles(File[] files) {
		Vector<Vector<Object>> vs = ((DefaultTableModel) fileTable.getModel()).getDataVector();
		int index = 1;
		F: for (File f : files) {
			for (Vector t : vs) {// 判断是否已上传
				if (f.getPath().equals((String) t.get(2))) {
					continue F;
				}
			}
			Vector<Object> v = new Vector<Object>();
			// 添加编号
			v.add(getLocalFileNum(index));
			v.add(JecnUtil.getFileName(f.getPath()));
			v.add(f.getPath());
			((DefaultTableModel) fileTable.getModel()).addRow(v);
			index++;
		}
	}

	private String getLocalFileNum(int index) {
		if (StringUtils.isBlank(viewText.getText())) {
			return "";
		}
		return viewText.getText() + "(" + index + ")";
	}
}
