package com.jecn.epros.server.bean.download;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.download.doc.ActiveInfo;
import com.jecn.epros.server.bean.download.doc.RoleInfo;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.LookPopedomBean;
import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessInterfacesDescriptionBean;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutData;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 流程操作说明数据Bean
 * 
 * @author fuzhh Nov 7, 2012
 */
public class ProcessDownloadBean extends ProcessDownloadBaseBean implements Serializable {

	/** 流程配置文件 */
	private List<JecnConfigItemBean> jecnConfigItemBean = new ArrayList<JecnConfigItemBean>();

	/** 图片存储路径 */
	private String imgPath;

	/** 公司名称 */
	private String companyName;
	/** 流程名称 */
	private String flowName;
	/** 流程客户 */
	private String flowCustom;
	/** 流程密集 */
	private String flowIsPublic;
	/** 保密级别 */
	private String confidentialityLevelName;
	/** 流程编号 */
	private String flowInputNum;
	/** 版本 */
	private String flowVersion;
	/** 发布日期 */
	private String releaseDate;
	/** 更新日期 */
	private String updateDate;
	/** 生效日期 */
	private String effectiveDate;
	/** 流程创建人 */
	private String createName;
	/** 责任部门Id */
	private Long orgId;
	/** 责任部门名称 */
	private String orgName;

	/** 目的 */
	private String flowPurpose;
	/** 适用范围 */
	private String applicability;
	/** 术语定义 */
	private String noutGlossary;
	/** 流程输入 */
	private String flowInput;
	/** 流程输出 */
	private String flowOutput;
	/** 不适用范围 */
	private String noApplicability;
	/** 补充说明 */
	private String flowSupplement;
	/** 概况信息 */
	private String flowSummarize;
	/** 流程责任人类型 */
	private Long dutyUserType;
	/** 流程责任人ID */
	private Long dutyUserId;
	/** 流程监护人ID */
	private Long guardianId;
	/** 流程拟制人ID */
	private Long fictionPeopleId;
	/** 流程文件类型 0 为表格 1为段落 */
	private int flowFileType;

	/** ** v3.05新添加的要素*** */
	/** a26相关文件 */
	private String flowRelatedFile = null;
	/** a27自定义要素1 */
	private String flowCustomOne = null;
	/** a28自定义要素2 */
	private String flowCustomTwo = null;
	/** a29自定义要素3 */
	private String flowCustomThree = null;
	/** a30自定义要素4 */
	private String flowCustomFour = null;
	/** a31自定义要素5 */
	private String flowCustomFive = null;

	/** ** v3.05新添加的要素*** */

	/** 驱动规则 */
	private FlowDriverBean flowDriver = null;

	// 流程驱动规则时间配置
	private String flowDrivenRuleTimeConfig = null;

	/** 评审人集合 0 评审人阶段名称 1名称 2日期 3审批阶段标识 */
	private List<Object[]> peopleList = new ArrayList<Object[]>();
	// 提交任务审批时使用
	/**
	 * 评审人集合 select distinct ts.stage_mark," + " ts.stage_name," +
	 * "                u.true_name," + "                org.org_name," +
	 * "                tr.Update_Time," + "                ts.sort," +
	 * "                u.people_id," + "		           pos.figure_text"
	 */
	private List<Object[]> peopleListNew = new ArrayList<Object[]>();
	/** 长春轨道数据源活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出 （长春轨道加入 6指标） */
	/**
	 * 普通数据源 活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出 6办理时限目标值 7办理时限原始值 8活动id
	 * 9角色id
	 */
	private List<Object[]> allActivityShowList = new ArrayList<Object[]>();
	/** 关键活动 */
	private List<KeyActivityBean> keyActivityShowList = new ArrayList<KeyActivityBean>();
	/** 角色信息集合 0 角色名称 1 岗位名称 2 角色职责 */
	private List<Object[]> roleList = new ArrayList<Object[]>();
	// 流程关键评测指标数据 0 KPI的名称 1 类型 2 目标值 3 kpi定义 4 kpi统计方法 5 数据统计时间频率 6
	// KIP值单位名称 7 数据获取方式 8 相关度 9 指标来源 10 数据提供者名称 11 支撑的一级指标内容 12
	// 12 目的 13 测量点 14 统计周期 15 说明 16 IT系统Id集合
	private List<Object[]> flowKpiList = new ArrayList<Object[]>();
	/** KPI 配置 **/
	private List<JecnConfigItemBean> configKpiItemBean = new ArrayList<JecnConfigItemBean>();
	/** 相关流程 */
	private List<RelatedProcessBean> relatedProcessList = new ArrayList<RelatedProcessBean>();
	/** 记录保存数据 0 记录名称 1保存责任人 2保存场所 3归档时间 4保存期限 5到期处理方式 6编号 7移交责任 */
	private List<Object[]> flowRecordList = new ArrayList<Object[]>();
	/** 流程记录 0 文件编号 1 文件名称 */
	private List<Object[]> processRecordList = new ArrayList<Object[]>();
	/** 操作规范 0 文件编号 1 文件名称 2 对应的活动编号 */
	private List<Object[]> operationTemplateList = new ArrayList<Object[]>();
	/** 农夫山泉-活动说明显示操作规范 操作规范 0 文件编号 1 文件名称 2 对应的活动编号 */
	private List<Object[]> operationTemplateListNF = new ArrayList<Object[]>();
	/** 流程边界 0起始活动 1终止活动 */
	private Object[] startEndActive = new Object[] {};
	/** 流程责任属性 0流程监护人 1流程责任人 2流程责任部门 */
	private ProcessAttributeBean responFlow = new ProcessAttributeBean();
	/** 流程文控信息 0版本号 1变更说明 2发布日期 3流程拟制人 4审批人 */
	private List<Object[]> documentList = new ArrayList<Object[]>();
	/** 相关风险 */
	private List<JecnRisk> rilsList = new ArrayList<JecnRisk>();

	/** 相关制度名称 类型 0.id 1.name */
	private List<Object[]> ruleNameList = new ArrayList<Object[]>();
	/** 相关标准名称 类型 */
	private List<Object[]> standardList = new ArrayList<Object[]>();

	/** 有该流程查阅权限的部门 **/
	private List<JecnFlowOrg> permOrgs = new ArrayList<JecnFlowOrg>();

	/** 最新的文控详细信息 */
	private JecnTaskHistoryNew latestHistoryNew;

	/** 所有的文控记录排序按照新旧顺序 **/
	private List<JecnTaskHistoryNew> historys = new ArrayList<JecnTaskHistoryNew>();

	/** 术语 **/
	private List<TermDefinitionLibrary> definitions = new ArrayList<TermDefinitionLibrary>();
	/** 相关文件 **/
	private List<FileWebBean> relatedFiles = new ArrayList<FileWebBean>();
	/** 描述信息 **/
	private ProcessInterfacesDescriptionBean description;
	/** 上级目录 **/
	private List<Object[]> parents = new ArrayList<Object[]>();
	/** 流程相关标准文件 A.ID,B.FILE_ID,B.FILE_NAME,B.DOC_ID */
	private List<String[]> standardFileList = new ArrayList<String[]>();
	/** 适用部门 **/
	private List<String> applyOrgs;

	private RoleInfo roleInfo;
	private ActiveInfo activeInfo;
	// ----------

	private IBaseDao baseDao;
	private boolean pub;
	private JecnFlowStructure flowStructure;
	private JecnFlowStructureT flowStructureT;

	/** 活动信息化 */
	private List<ActiveItSystem> activeItSystems = null;
	private LookPopedomBean perms = new LookPopedomBean();
	/*
	 * 输入
	 */
	private List<JecnFlowInoutData> inData;
	/**
	 * 输出
	 */
	private List<JecnFlowInoutData> outData;

	/**
	 * 操作说明头信息
	 */
	private FlowFileHeadData flowFileHeadData;
	private DocConfig config;

	public String getImplementDate() {
		getImplementDate("yyyy-MM-dd");
		return "";
	}

	/**
	 * 保密级别
	 * 
	 * @return
	 */
	public Integer getSectLevel() {
		if (flowStructure != null) {
			return flowStructure.getConfidentialityLevel();
		} else if (flowStructureT != null) {
			return flowStructureT.getConfidentialityLevel();
		}
		return null;
	}

	public String getVersion() {
		if (latestHistoryNew == null) {
			return "";
		}
		return JecnUtil.nullToEmpty(latestHistoryNew.getVersionId());
	}

	public Integer getLevel() {
		if (flowStructure != null) {
			return flowStructure.gettLevel();
		} else if (flowStructureT != null) {
			return flowStructureT.gettLevel();
		}
		return null;
	}

	public String getPubDate(String pattern) {
		Date pubDate = null;
		if (latestHistoryNew != null && latestHistoryNew.getPublishDate() != null) {
			pubDate = latestHistoryNew.getPublishDate();
		} else {
			if (flowStructure != null) {
				pubDate = flowStructure.getPubTime();
			} else if (flowStructureT != null) {
				pubDate = flowStructureT.getPubTime();
			}
		}
		if (pubDate != null) {
			return JecnUtil.DateToStr(pubDate, pattern);
		}
		return "";
	}

	public String getImplementDate(String pattern) {
		if (latestHistoryNew != null && latestHistoryNew.getImplementDate() != null) {
			return JecnUtil.DateToStr(latestHistoryNew.getImplementDate(), pattern);
		}
		return "";
	}

	public String getExpiryStr() {
		if (StringUtils.isNotBlank(responFlow.getExpiry())) {
			if ("0".equals(responFlow.getExpiry())) {
				return "永久";
			} else {
				return responFlow.getExpiry();
			}
		}
		return "";
	}

	public ProcessAttributeBean getFlowAttr() {
		return responFlow;
	}

	public Long getFlowId() {
		if (flowStructure != null) {
			return flowStructure.getFlowId();
		}
		if (flowStructureT != null) {
			return flowStructureT.getFlowId();
		}
		return null;
	}

	// --------

	public RoleInfo getRoleInfo() {
		return roleInfo;
	}

	public ActiveInfo getActiveInfo() {
		return activeInfo;
	}

	public void setActiveInfo(ActiveInfo activeInfo) {
		this.activeInfo = activeInfo;
	}

	public void setRoleInfo(RoleInfo roleInfo) {
		this.roleInfo = roleInfo;
	}

	public List<String> getApplyOrgs() {
		return applyOrgs;
	}

	public void setApplyOrgs(List<String> applyOrgs) {
		this.applyOrgs = applyOrgs;
	}

	public List<String[]> getStandardFileList() {
		return standardFileList;
	}

	public void setStandardFileList(List<String[]> standardFileList) {
		this.standardFileList = standardFileList;
	}

	public List<JecnTaskHistoryNew> getHistorys() {
		return historys;
	}

	public void setHistorys(List<JecnTaskHistoryNew> historys) {
		this.historys = historys;
	}

	public void setOperationTemplateListNF(List<Object[]> operationTemplateListNF) {
		this.operationTemplateListNF = operationTemplateListNF;
	}

	public JecnTaskHistoryNew getLatestHistoryNew() {
		return latestHistoryNew;
	}

	public void setLatestHistoryNew(JecnTaskHistoryNew latestHistoryNew) {
		this.latestHistoryNew = latestHistoryNew;
	}

	public int getFlowFileType() {
		return flowFileType;
	}

	public void setFlowFileType(int flowFileType) {
		this.flowFileType = flowFileType;
	}

	public Long getDutyUserType() {
		return dutyUserType;
	}

	public void setDutyUserType(Long dutyUserType) {
		this.dutyUserType = dutyUserType;
	}

	public Long getDutyUserId() {
		return dutyUserId;
	}

	public void setDutyUserId(Long dutyUserId) {
		this.dutyUserId = dutyUserId;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public Long getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(Long guardianId) {
		this.guardianId = guardianId;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public Long getFictionPeopleId() {
		return fictionPeopleId;
	}

	public void setFictionPeopleId(Long fictionPeopleId) {
		this.fictionPeopleId = fictionPeopleId;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getFlowCustom() {
		return flowCustom;
	}

	public void setFlowCustom(String flowCustom) {
		this.flowCustom = flowCustom;
	}

	public String getFlowIsPublic() {
		return flowIsPublic;
	}

	public void setFlowIsPublic(String flowIsPublic) {
		this.flowIsPublic = flowIsPublic;
	}

	public String getFlowInputNum() {
		return flowInputNum;
	}

	public void setFlowInputNum(String flowInputNum) {
		this.flowInputNum = flowInputNum;
	}

	public String getFlowVersion() {
		return flowVersion;
	}

	public void setFlowVersion(String flowVersion) {
		this.flowVersion = flowVersion;
	}

	public String getFlowPurpose() {
		return flowPurpose;
	}

	public void setFlowPurpose(String flowPurpose) {
		this.flowPurpose = flowPurpose;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String getNoutGlossary() {
		return noutGlossary;
	}

	public void setNoutGlossary(String noutGlossary) {
		this.noutGlossary = noutGlossary;
	}

	public String getFlowInput() {
		return flowInput;
	}

	public void setFlowInput(String flowInput) {
		this.flowInput = flowInput;
	}

	public String getFlowOutput() {
		return flowOutput;
	}

	public void setFlowOutput(String flowOutput) {
		this.flowOutput = flowOutput;
	}

	public String getNoApplicability() {
		return noApplicability;
	}

	public void setNoApplicability(String noApplicability) {
		this.noApplicability = noApplicability;
	}

	public String getFlowSupplement() {
		return flowSupplement;
	}

	public void setFlowSupplement(String flowSupplement) {
		this.flowSupplement = flowSupplement;
	}

	public String getFlowSummarize() {
		return flowSummarize;
	}

	public void setFlowSummarize(String flowSummarize) {
		this.flowSummarize = flowSummarize;
	}

	public List<KeyActivityBean> getKeyActivityShowList() {
		return keyActivityShowList;
	}

	public FlowDriverBean getFlowDriver() {
		return flowDriver;
	}

	public String getFlowDrivenRuleTimeConfig() {
		return flowDrivenRuleTimeConfig;
	}

	public void setFlowDrivenRuleTimeConfig(String flowDrivenRuleTimeConfig) {
		this.flowDrivenRuleTimeConfig = flowDrivenRuleTimeConfig;
	}

	public List<RelatedProcessBean> getRelatedProcessList() {
		return relatedProcessList;
	}

	public List<Object[]> getRuleNameList() {
		return ruleNameList;
	}

	public List<Object[]> getStandardList() {
		return standardList;
	}

	public List<Object[]> getPeopleList() {
		return peopleList;
	}

	public List<Object[]> getRoleList() {
		return roleList;
	}

	public List<Object[]> getFlowKpiList() {
		return flowKpiList;
	}

	public List<Object[]> getFlowRecordList() {
		return flowRecordList;
	}

	public List<Object[]> getProcessRecordList() {
		return processRecordList;
	}

	public List<Object[]> getOperationTemplateList() {
		return operationTemplateList;
	}

	public List<Object[]> getAllActivityShowList() {
		return allActivityShowList;
	}

	public void setFlowDriver(FlowDriverBean flowDriver) {
		this.flowDriver = flowDriver;
	}

	public void setPeopleList(List<Object[]> peopleList) {
		this.peopleList = peopleList;
	}

	public void setAllActivityShowList(List<Object[]> allActivityShowList) {
		this.allActivityShowList = allActivityShowList;
	}

	public void setKeyActivityShowList(List<KeyActivityBean> keyActivityShowList) {
		this.keyActivityShowList = keyActivityShowList;
	}

	public void setRoleList(List<Object[]> roleList) {
		this.roleList = roleList;
	}

	public void setFlowKpiList(List<Object[]> flowKpiList) {
		this.flowKpiList = flowKpiList;
	}

	public void setRelatedProcessList(List<RelatedProcessBean> relatedProcessList) {
		this.relatedProcessList = relatedProcessList;
	}

	public void setFlowRecordList(List<Object[]> flowRecordList) {
		this.flowRecordList = flowRecordList;
	}

	public void setProcessRecordList(List<Object[]> processRecordList) {
		this.processRecordList = processRecordList;
	}

	public void setOperationTemplateList(List<Object[]> operationTemplateList) {
		this.operationTemplateList = operationTemplateList;
	}

	public void setRuleNameList(List<Object[]> ruleNameList) {
		this.ruleNameList = ruleNameList;
	}

	public void setStandardList(List<Object[]> standardList) {
		this.standardList = standardList;
	}

	public List<JecnConfigItemBean> getJecnConfigItemBean() {
		return jecnConfigItemBean;
	}

	public void setJecnConfigItemBean(List<JecnConfigItemBean> jecnConfigItemBean) {
		this.jecnConfigItemBean = jecnConfigItemBean;
	}

	public String getCompanyName() {
		if (this.getFlowFileHeadData() != null && StringUtils.isNotBlank(this.getFlowFileHeadData().getCompanyName())) {
			return this.getFlowFileHeadData().getCompanyName();
		}
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getEffectiveDate() {
		if (effectiveDate == null) {
			return "";
		}
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Object[] getStartEndActive() {
		return startEndActive;
	}

	public void setStartEndActive(Object[] startEndActive) {
		this.startEndActive = startEndActive;
	}

	public ProcessAttributeBean getResponFlow() {
		return responFlow;
	}

	public void setResponFlow(ProcessAttributeBean responFlow) {
		this.responFlow = responFlow;
	}

	public List<Object[]> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<Object[]> documentList) {
		this.documentList = documentList;
	}

	public List<JecnRisk> getRilsList() {
		return rilsList;
	}

	public void setRilsList(List<JecnRisk> rilsList) {
		this.rilsList = rilsList;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		if (orgName == null) {
			return "";
		}
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getFlowRelatedFile() {
		return flowRelatedFile;
	}

	public void setFlowRelatedFile(String flowRelatedFile) {
		this.flowRelatedFile = flowRelatedFile;
	}

	public String getFlowCustomOne() {
		return flowCustomOne;
	}

	public void setFlowCustomOne(String flowCustomOne) {
		this.flowCustomOne = flowCustomOne;
	}

	public String getFlowCustomTwo() {
		return flowCustomTwo;
	}

	public void setFlowCustomTwo(String flowCustomTwo) {
		this.flowCustomTwo = flowCustomTwo;
	}

	public String getFlowCustomThree() {
		return flowCustomThree;
	}

	public void setFlowCustomThree(String flowCustomThree) {
		this.flowCustomThree = flowCustomThree;
	}

	public String getFlowCustomFour() {
		return flowCustomFour;
	}

	public void setFlowCustomFour(String flowCustomFour) {
		this.flowCustomFour = flowCustomFour;
	}

	public String getFlowCustomFive() {
		return flowCustomFive;
	}

	public void setFlowCustomFive(String flowCustomFive) {
		this.flowCustomFive = flowCustomFive;
	}

	public List<Object[]> getPeopleListNew() {
		return peopleListNew;
	}

	public void setPeopleListNew(List<Object[]> peopleListNew) {
		this.peopleListNew = peopleListNew;
	}

	public List<JecnConfigItemBean> getConfigKpiItemBean() {
		return configKpiItemBean;
	}

	public void setConfigKpiItemBean(List<JecnConfigItemBean> configKpiItemBean) {
		this.configKpiItemBean = configKpiItemBean;
	}

	public List<Object[]> getOperationTemplateListNF() {
		return operationTemplateListNF;
	}

	public void addOperationTemplateListNF(Object[] objects) {
		if (objects == null || objects.length == 0) {
			return;
		}
		this.operationTemplateListNF.add(objects);
	}

	public List<JecnFlowOrg> getPermOrgs() {
		return permOrgs;
	}

	public void setPermOrgs(List<JecnFlowOrg> permOrgs) {
		this.permOrgs = permOrgs;
	}

	public List<TermDefinitionLibrary> getDefinitions() {
		return definitions;
	}

	public void setDefinitions(List<TermDefinitionLibrary> definitions) {
		this.definitions = definitions;
	}

	public List<FileWebBean> getRelatedFiles() {
		return relatedFiles;
	}

	public void setRelatedFiles(List<FileWebBean> relatedFiles) {
		this.relatedFiles = relatedFiles;
	}

	public ProcessInterfacesDescriptionBean getDescription() {
		return description;
	}

	public void setDescription(ProcessInterfacesDescriptionBean description) {
		this.description = description;
	}

	public List<Object[]> getParents() {
		return parents;
	}

	public void setParents(List<Object[]> parents) {
		this.parents = parents;
	}

	public IBaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(IBaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public boolean isPub() {
		return pub;
	}

	public void setPub(boolean pub) {
		this.pub = pub;
	}

	public JecnFlowStructure getFlowStructure() {
		return flowStructure;
	}

	public void setFlowStructure(JecnFlowStructure flowStructure) {
		this.flowStructure = flowStructure;
	}

	public JecnFlowStructureT getFlowStructureT() {
		return flowStructureT;
	}

	public void setFlowStructureT(JecnFlowStructureT flowStructureT) {
		this.flowStructureT = flowStructureT;
	}

	public String getConfidentialityLevelName() {
		return confidentialityLevelName;
	}

	public void setConfidentialityLevelName(String confidentialityLevelName) {
		this.confidentialityLevelName = confidentialityLevelName;
	}

	public List<ActiveItSystem> getActiveItSystems() {
		return activeItSystems;
	}

	public void setActiveItSystems(List<ActiveItSystem> activeItSystems) {
		this.activeItSystems = activeItSystems;
	}

	public List<JecnFlowInoutData> getInData() {
		return inData;
	}

	public void setInData(List<JecnFlowInoutData> inData) {
		this.inData = inData;
	}

	public List<JecnFlowInoutData> getOutData() {
		return outData;
	}

	public void setOutData(List<JecnFlowInoutData> outData) {
		this.outData = outData;
	}

	public LookPopedomBean getPerms() {
		return perms;
	}

	public void setPerms(LookPopedomBean perms) {
		this.perms = perms;
	}

	public FlowFileHeadData getFlowFileHeadData() {
		return flowFileHeadData;
	}

	public void setFlowFileHeadData(FlowFileHeadData flowFileHeadData) {
		this.flowFileHeadData = flowFileHeadData;
	}

	public DocConfig getConfig() {
		return config;
	}

	public void setConfig(DocConfig config) {
		this.config = config;
	}

}
