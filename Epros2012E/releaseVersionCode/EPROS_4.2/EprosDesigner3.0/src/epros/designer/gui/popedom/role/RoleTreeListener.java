package epros.designer.gui.popedom.role;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;

public class RoleTreeListener extends JecnTreeListener {
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(RoleTreeListener.class);

	public RoleTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}

	/**
	 * 展开
	 */
	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = JecnRoleCommon.getRoleTreeBeans(node);
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("RoleTreeListener treeExpanded is error ！", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
	}

	/**
	 * 合拢
	 */
	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

}
