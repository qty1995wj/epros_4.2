/*
 * ToFigure.java
 *to元素
 * Created on 2008年12月3日, 下午5:17
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseCircle;

/**
 * TO图标
 * 
 * @author zhangxh
 * @date： 日期：Mar 23, 2012 时间：3:03:48 PM
 */
public class ToFigure extends BaseCircle {

	public ToFigure(JecnFigureData figureData) {
		super(figureData);
	}
}
