package epros.draw.gui.box;

import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;

/**
 * 
 * 流程元素库数据层
 * 
 * @author ZHOUXY
 * 
 */
class JecnToolBoxBean {
	/** 常用线集合 */
	// private List<JecnToolBoxButton> favrLineList = new
	// ArrayList<JecnToolBoxButton>();
	// /** 常用图形集合 */
	// private List<JecnToolBoxButton> favrFigureList = new
	// ArrayList<JecnToolBoxButton>();
	// /** 不常用流程元素集合 */
	// private List<JecnToolBoxButton> genflowElemList = new
	// ArrayList<JecnToolBoxButton>();

	private List<JecnToolBoxButton> allShowElemList = new ArrayList<JecnToolBoxButton>();

	/**
	 * 
	 * 设置按钮显示样式: 1、图片、内容左右布局 2、图片、内容上下布局 3、图片、详细内容布局
	 * 
	 */
	protected void setButtonShowType() {
		for (JecnToolBoxButton btn : allShowElemList) {
			btn.setButtonShowType();
		}
		// for (JecnToolBoxButton btn : favrFigureList) {
		// btn.setButtonShowType();
		// }
		// for (JecnToolBoxButton btn : genflowElemList) {
		// btn.setButtonShowType();
		// }
	}

	/**
	 * 
	 * 获取流程元素面板按钮总个数
	 * 
	 * @return
	 */
	int getCount() {
		return (allShowElemList.size());
	}

	List<JecnToolBoxButton> getAllShowList() {
		return allShowElemList;
	}

	// List<JecnToolBoxButton> getFavrFigureList() {
	// return favrFigureList;
	// }
	//
	// List<JecnToolBoxButton> getGenflowElemList() {
	// return genflowElemList;
	// }

	/**
	 * 
	 * 添加按钮到常用线中
	 * 
	 * @param btn
	 *            JecnToolBoxButton 按钮
	 */
	boolean addBtnToShowList(JecnToolBoxButton btn) {
		if (btn == null) {
			return false;
		}
		allShowElemList.add(btn);
		return true;
	}

	/**
	 * 
	 * 添加按钮到常用线中
	 * 
	 * @param btn
	 *            JecnToolBoxButton 按钮
	 */
	// boolean addBtnToFavrLineList(JecnToolBoxButton btn) {
	// if (btn == null) {
	// return false;
	// }
	// favrLineList.add(btn);
	// return true;
	// }
	//
	// public List<JecnToolBoxButton> getFavrLineList() {
	// return favrLineList;
	// }

	/**
	 * 
	 * 添加按钮到常用图形中
	 * 
	 * @param btn
	 *            JecnToolBoxButton 按钮
	 */
	// boolean addBtnToFavrFigureList(JecnToolBoxButton btn) {
	// if (btn == null) {
	// return false;
	// }
	// favrLineList.add(btn);
	// return true;
	// }

	/**
	 * 
	 * 添加不常用流程元素
	 * 
	 * @param btn
	 *            JecnToolBoxButton
	 * @param btnList
	 *            List<JecnToolBoxButton> 存放按钮的集合
	 * @return boolean true:添加成功 false：添加失败或参数为NULL
	 */
	boolean addBtnToGenflowElemList(JecnToolBoxButton btn) {
		if (btn == null || isExistGenflowElemBoolean(btn.getBtnType())) {// 已经存在
			return false;
		}
		allShowElemList.add(btn);
		return true;
	}

	/**
	 * 
	 * 判断给定的流程元素类型是否已经存在
	 * 
	 * @param mapElemType
	 * @return boolean true:存在；false:不存在
	 */
	boolean isExistGenflowElemBoolean(MapElemType mapElemType) {
		if (isExistGenflowElem(mapElemType) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 判断给定的流程元素类型是否已经存在
	 * 
	 * @param mapElemType
	 * @return JecnToolBoxButton JecnToolBoxButton对象或NULL
	 */
	JecnToolBoxButton isExistGenflowElem(MapElemType mapElemType) {
		for (JecnToolBoxButton btn : allShowElemList) {
			if (btn.getBtnType() == mapElemType) {
				return btn;
			}
		}
		return null;
	}

	/**
	 * 
	 * 移除给定流程元素类型的对应的按钮
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return boolean true：移除成功；false：没有移除或集合中不存在对应个按钮
	 */
	boolean removeGenflowElemList(MapElemType mapElemType) {
		for (JecnToolBoxButton btn : allShowElemList) {
			if (btn.getBtnType() == mapElemType) {
				allShowElemList.remove(btn);
				return true;
			}
		}
		return false;
	}
}
