package epros.designer.gui.rule;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.JecnRuleOperationCommon;
import com.jecn.epros.server.bean.rule.RuleContentT;
import com.jecn.epros.server.bean.rule.RuleModeTitleBean;
import com.jecn.epros.server.bean.rule.RuleTitleT;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.process.guide.explain.JecnRelatedStandard;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnResourceUtil;

/***
 * 制度操作说明
 * 
 * @author 2012-06-07
 * 
 */
public class RuleOperatePanel extends JecnPanel {

	private static Logger log = Logger.getLogger(RuleOperatePanel.class);

	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = new JScrollPane();

	/** 获取操作说明数据集合RuleModeTitleBean */
	List<JecnRuleOperationCommon> jecnRuleOperationCommonList;

	/** 文本，文件表单，流程表单Panel集合 */
	protected Map<Long, Object> panelList = new HashMap<Long, Object>();

	// 制度ID
	private int widthL = 600;
	private int heighL = 600;

	private Long ruleId;

	private JecnRelatedStandard relatedStandard;

	private RuleRiskFormPanel relatedRisk;

	private RuleInstitutionFormPanel relatedRule;

	private void init() {
		// 把主面板放在带滚动条的ScrollPane上
		resultScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		resultScrollPane.setPreferredSize(new Dimension(630, 365));
		// 鼠标滚动大小
		resultScrollPane.getHorizontalScrollBar().setUnitIncrement(30);
		resultScrollPane.getVerticalScrollBar().setUnitIncrement(30);
		mainPanel.setBorder(BorderFactory.createTitledBorder(""));

		initLayout();
		mainPanel.setPreferredSize(new Dimension(widthL, heighL));
		mainPanel.setMaximumSize(new Dimension(widthL, heighL));
		mainPanel.setMinimumSize(new Dimension(widthL, heighL));
	}

	/**
	 * 添加制度---制度操作说明
	 * 
	 * @param selectNode
	 * @param inFlag
	 * @param comId
	 */
	public RuleOperatePanel(JecnTreeNode selectNode) {
		List<RuleModeTitleBean> listRuleMode = null;
		// 制度操作说明显示数据
		try {
			Long modeId = JecnTreeCommon.getRuleModeId(selectNode);
			if (modeId != null) {
				listRuleMode = ConnectionPool.getRuleModeAction().getRuleModeTitle(modeId);
				jecnRuleOperationCommonList = this.getJecnRuleOperationCommons(listRuleMode);
			}
		} catch (Exception e) {
			log.error("RuleOperatePanel is error", e);
		}
		init();
	}

	/**
	 * 编辑和查看
	 * 
	 * @param ruleId
	 */
	public RuleOperatePanel(Long ruleId) {
		this.ruleId = ruleId;
		// 获取操作说明数据集合
		try {
			if (ruleId != null) {
				if (ruleId != null) {
					jecnRuleOperationCommonList = ConnectionPool.getRuleAction().findRuleOperationShow(ruleId);
				}
			}
		} catch (Exception e) {
			log.error("RuleOperatePanel is error", e);
		}
		init();
	}

	/**
	 * 获得要保存的制度信息
	 * 
	 * @return
	 */
	public List<JecnRuleOperationCommon> getJecnRuleOperationCommons(List<RuleModeTitleBean> listRuleMode) {
		List<JecnRuleOperationCommon> ruleList = new ArrayList<JecnRuleOperationCommon>();
		if (listRuleMode != null && listRuleMode.size() > 0) {
			for (RuleModeTitleBean ruleModeTitleBean : listRuleMode) {
				JecnRuleOperationCommon ruleOperationCommon = new JecnRuleOperationCommon();
				// 将模板制度标题封装到制度标题表中
				RuleTitleT ruleTitleT = new RuleTitleT();
				ruleTitleT.setOrderNumber(ruleModeTitleBean.getSortId());
				ruleTitleT.setTitleName(ruleModeTitleBean.getTitleName());// 判断中英文
				ruleTitleT.setEnName(ruleModeTitleBean.getEnName());
				ruleTitleT.setType(ruleModeTitleBean.getType());
				ruleTitleT.setRequiredType(ruleModeTitleBean.getRequiredType());
				ruleOperationCommon.setRuleTitleT(ruleTitleT);
				ruleList.add(ruleOperationCommon);
			}
		}
		return ruleList;
	}

	/***
	 * 布局
	 */
	private void initLayout() {
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		String titleName = null;
		int titleType = 0;
		boolean requiredType = false;
		mainPanel.setLayout(new GridBagLayout());
		if (jecnRuleOperationCommonList != null && jecnRuleOperationCommonList.size() > 0) {
			for (int i = 0; i < jecnRuleOperationCommonList.size(); i++) {
				if (i > 5) {
					heighL = heighL + 150;
				}
				int row = i + 1;
				// 标题名称
				titleName = jecnRuleOperationCommonList.get(i).getRuleTitleT().getTitleName(
						JecnResourceUtil.getLocale() == Locale.CHINESE ? 0 : 1);
				// 标题类型
				titleType = jecnRuleOperationCommonList.get(i).getRuleTitleT().getType();
				requiredType = jecnRuleOperationCommonList.get(i).getRuleTitleT().getRequiredType() == 1 ? true : false;
				switch (titleType) {
				case 0:
					String ruleContentStr = "";
					if (jecnRuleOperationCommonList.get(i).getRuleContentT() != null
							&& jecnRuleOperationCommonList.get(i).getRuleContentT().getContentStr() != null) {
						ruleContentStr = jecnRuleOperationCommonList.get(i).getRuleContentT().getContentStr();
					}
					ContentJPanel contentJPanel = new ContentJPanel(row, titleName, mainPanel, ruleContentStr,
							requiredType);
					panelList.put(Long.valueOf(i), contentJPanel);
					break;
				case 1:
					RuleFilePanel ruleFilePanel = new RuleFilePanel(row, titleName, mainPanel,
							jecnRuleOperationCommonList.get(i).getListFileBean(), jecnRuleOperationCommonList.get(i)
									.getRuleTitleT().getId(), requiredType);
					panelList.put(Long.valueOf(i), ruleFilePanel);
					break;
				case 2:
					// 流程表单添加
					new RuleFlowFormPanel(row, titleName, mainPanel, jecnRuleOperationCommonList.get(i)
							.getListJecnFlowCommon(), requiredType, 1);
					break;
				case 3:
					// 流程地图表单添加
					new RuleFlowFormPanel(row, titleName, mainPanel, jecnRuleOperationCommonList.get(i)
							.getListJecnFlowMapCommon(), requiredType, 0);
					break;
				case 4:
					// 标准表单添加
					// new RuleStandarFormPanel(row, titleName, mainPanel,
					// jecnRuleOperationCommonList.get(i)
					// .getListJecnStandarCommon(), false);
					relatedStandard = new JecnRelatedStandard(row, titleName, requiredType, mainPanel, ruleId,
							TreeNodeType.ruleModeFile);
					break;
				case 5:
					// 风险表单添加
					relatedRisk = new RuleRiskFormPanel(row, titleName, mainPanel, ruleId, TreeNodeType.riskPoint,
							requiredType);
					break;
				case 6:
					// 制度表单添加
					relatedRule = new RuleInstitutionFormPanel(row, titleName, mainPanel, ruleId,
							TreeNodeType.riskPoint, requiredType);
					break;
				default:
					break;
				}
			}
		}
		Insets insets = new Insets(5, 5, 5, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		this.add(resultScrollPane, c);
		resultScrollPane.setViewportView(mainPanel);
	}

	public boolean isUpdate() {
		int count = 0;
		if (jecnRuleOperationCommonList != null && jecnRuleOperationCommonList.size() > 0) {
			for (JecnRuleOperationCommon jecnRuleOperationCommon : jecnRuleOperationCommonList) {
				int ruleType = jecnRuleOperationCommon.getRuleTitleT().getType();
				if (ruleType == 0) {
					ContentJPanel contentJPanel = (ContentJPanel) panelList.get(Long.valueOf(count));
					if (contentJPanel.isUpdate()) {
						return true;
					}
				} else if (ruleType == 1) {// 文件表单
					RuleFilePanel ruleFilePanel = (RuleFilePanel) panelList.get(Long.valueOf(count));
					if (ruleFilePanel.isUpdate()) {
						return true;
					}
				}
				count++;
			}

		}
		if (relatedStandard != null && relatedStandard.isUpdate()) {
			return true;
		}
		if (relatedRisk != null && relatedRisk.isUpdate()) {
			return true;
		}
		if (relatedRule != null && relatedRule.isUpdate()) {
			return true;
		}
		return false;
	}

	/**
	 * 获得要保存的制度信息
	 * 
	 * @return
	 */
	public List<JecnRuleOperationCommon> getResultData() {
		int count = 0;
		for (JecnRuleOperationCommon jecnRuleOperationCommon : jecnRuleOperationCommonList) {
			int ruleType = jecnRuleOperationCommon.getRuleTitleT().getType();
			if (ruleType == 0) {
				ContentJPanel contentJPanel = (ContentJPanel) panelList.get(Long.valueOf(count));
				RuleContentT ruleContentT = jecnRuleOperationCommon.getRuleContentT();
				if (ruleContentT == null) {
					ruleContentT = new RuleContentT();
					ruleContentT.setRuleTitleId(jecnRuleOperationCommon.getRuleTitleT().getId());
				}
				ruleContentT.setContentStr(contentJPanel.getResultStr());
				jecnRuleOperationCommon.setRuleContentT(ruleContentT);
			} else if (ruleType == 1) {// 文件表单
				RuleFilePanel ruleFilePanel = (RuleFilePanel) panelList.get(Long.valueOf(count));
				jecnRuleOperationCommon.setListFileBean(ruleFilePanel.getResultList());
			}
			count++;
		}
		return jecnRuleOperationCommonList;
	}

	public JecnPanel getMainPanel() {
		return mainPanel;
	}

	public Set<Long> getStandardIds() {
		return relatedStandard == null ? null : relatedStandard.getResultIds();
	}

	public Set<Long> getRuleIds() {
		return relatedRule == null ? null : relatedRule.getResultIds();
	}

	public Set<Long> getRiskIds() {
		return relatedRisk == null ? null : relatedRisk.getResultIds();
	}
}
