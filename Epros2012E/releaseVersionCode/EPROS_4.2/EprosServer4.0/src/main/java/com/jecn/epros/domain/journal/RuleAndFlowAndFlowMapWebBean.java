package com.jecn.epros.domain.journal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.service.TreeNodeUtils;

import java.util.Date;

public class RuleAndFlowAndFlowMapWebBean {

    /**
     * 类型  当前节点
     */
    private LinkResource.ResourceType type;
    /**
     * 节点类型名称
     */
    private String typeName;
    /**
     * 资源ID
     */
    private Long id;

    private String name;
    @JsonIgnore
    private String version;
    @JsonIgnore
    private String dutyDept;

    private String dutyPerson;

    private String url;

    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    private Date pubTime;
    @JsonIgnore
    private String isPublic;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(String isPublic) {
        this.isPublic = isPublic;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }

    public LinkResource.ResourceType getType() {
        return type;
    }

    public void setType(LinkResource.ResourceType type) {
        this.type = type;
    }

    public String getTypeName() {
        return type.getName();
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDutyPerson(String dutyPerson) {
        this.dutyPerson = dutyPerson;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDutyDept() {
        return dutyDept;
    }

    public void setDutyDept(String dutyDept) {
        this.dutyDept = dutyDept;
    }

    public String getDutyPerson() {
        return dutyPerson;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
