package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class FlowActiveOnLineBean {
    /**
     * 上线时间
     */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date onLineTime;
    /**
     * 系统名称
     */
    private String sysName;

    public Date getOnLineTime() {
        return onLineTime;
    }

    public void setOnLineTime(Date onLineTime) {
        this.onLineTime = onLineTime;
    }

    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }
}
