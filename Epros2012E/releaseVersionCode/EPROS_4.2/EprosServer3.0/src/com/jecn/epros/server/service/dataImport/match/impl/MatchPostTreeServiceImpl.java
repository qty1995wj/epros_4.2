package com.jecn.epros.server.service.dataImport.match.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.dao.dataImport.match.IMatchPostTreeBeanDao;
import com.jecn.epros.server.service.dataImport.match.IMatchPostTreeService;
import com.jecn.epros.server.service.dataImport.match.constant.MatchErrorInfo;
import com.jecn.epros.server.service.dataImport.match.constant.TmpMatchTreePostBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.WebTreeBean;

/**
 * 岗位匹配临时组织架构处理
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Feb 28, 2013 时间：2:08:08 PM
 */
public class MatchPostTreeServiceImpl implements IMatchPostTreeService {
	private IMatchPostTreeBeanDao matchPostTreeDao;

	private Logger log = Logger.getLogger(MatchPostTreeServiceImpl.class);

	/**
	 * 获取当前节点下子节点
	 * 
	 * @param strNumber
	 *            编号
	 * @param projectId
	 *            项目ID
	 * @param isLeaf
	 *            true 含子节点
	 * @return List<WebTreeBean>
	 * @throws Exception
	 */
	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<WebTreeBean> getChildMatchOrgsAndPositon(String strNumber,
			Long projectId) throws Exception {
		try {
			String sql = "select jmad.DEPT_NUM,jmad.DEPT_NAME,jmad.P_DEPT_NUM, "
					+ " (select count(*) from JECN_MATCH_ACTUAL_DEPT where P_DEPT_NUM=jmad.DEPT_NUM) as dept_count,"
					+ " (select count(*) from JECN_MATCH_ACTUAL_POS where DEPT_NUM=jmad.DEPT_NUM) as pos_count"
					+ "  from JECN_MATCH_ACTUAL_DEPT jmad where jmad.P_DEPT_NUM=? and jmad.PROJECT_ID=? order by jmad.DEPT_NAME";
			// 组织 数组0：部门编号；1：部门名称；2：部门父节点编号；3：当前节点下子部门个数；4：当前节点下岗位个数
			List<Object[]> listOrg = matchPostTreeDao.listNativeSql(sql,
					strNumber, projectId);
			// 岗位
			// if (isLeaf) {// 包含子节点
			// listPos = stationMatchMenuDao.getPosAndPersonByOrgId(strNumber,
			// projectId);
			// } else {
			// }
			// 根据部门编号获取部门下岗位
			List<Object[]> listPos = matchPostTreeDao.getPosByOrgId(strNumber,
					projectId);
			List<WebTreeBean> listResult = getChildDeptMenu(listOrg);
			for (Object[] obj : listPos) {
				if (obj == null || obj[0] == null || obj[1] == null
						|| obj[2] == null || obj[3] == null) {
					continue;
				}
				WebTreeBean webTreeBean = new WebTreeBean();
				// 部门ID
				webTreeBean.setId(obj[0].toString());
				// 部门名称
				webTreeBean.setText(obj[1].toString());
				// 设置图片路径
				webTreeBean.setIcon("images/treeNodeImages/position.gif");
				webTreeBean.setType(TreeNodeType.position.toString());
				// 没有子节点
				webTreeBean.setLeaf(true);
				listResult.add(webTreeBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 获取基准岗位
	 * 
	 * @param strNumber
	 *            编号
	 * @param projectId
	 *            项目ID
	 * @param isLeaf
	 *            true 含子节点
	 * @return List<WebTreeBean>
	 * @throws Exception
	 */
	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<WebTreeBean> getChildMatchBasePos() throws Exception {
		try {
			String sql = " SELECT BASE_POS_NUM, BASE_POS_NAME,0,0 "
					+ " FROM JECN_MATCH_ACTUAL_POS group by BASE_POS_NUM, BASE_POS_NAME order by BASE_POS_NAME desc";

			// 组织 数组0：部门编号；1：部门名称；2：部门父节点编号；3：当前节点下子部门个数；4：当前节点下岗位个数
			List<Object[]> listPos = matchPostTreeDao.listNativeSql(sql);

			List<WebTreeBean> listResult = new ArrayList<WebTreeBean>();
			for (Object[] obj : listPos) {
				if (obj == null || obj[0] == null || obj[1] == null
						|| obj[2] == null || obj[3] == null) {
					continue;
				}
				WebTreeBean webTreeBean = new WebTreeBean();
				// 部门ID
				webTreeBean.setId(obj[0].toString());
				// 部门名称
				webTreeBean.setText(obj[1].toString());
				// 设置图片路径
				webTreeBean.setIcon("images/treeNodeImages/position.gif");
				// 基准岗位标识
				webTreeBean.setType("basePosition");
				// 没有子节点
				webTreeBean.setLeaf(true);
				listResult.add(webTreeBean);
			}
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 数组0：部门编号；1：部门名称；2：部门父节点编号；3：当前节点下子部门个数；4：当前节点下岗位个数
	 * 
	 * @param currDeptList
	 * @return
	 */
	private List<WebTreeBean> getChildDeptMenu(List<Object[]> currDeptList) {
		if (currDeptList == null) {
			return null;
		}
		List<WebTreeBean> menusList = new ArrayList<WebTreeBean>();

		for (Object[] obj : currDeptList) {
			if (obj == null || obj[0] == null || obj[1] == null
					|| obj[2] == null || obj[3] == null) {
				continue;
			}
			WebTreeBean webTreeBean = new WebTreeBean();
			// 部门ID
			webTreeBean.setId(obj[0].toString());
			// 部门名称
			webTreeBean.setText(obj[1].toString());
			// 3：当前节点下子部门个数
			int childOrg = Integer.parseInt(obj[3].toString());
			// 4：当前节点下岗位个数
			int childPos = Integer.parseInt(obj[4].toString());
			webTreeBean.setType(TreeNodeType.organization.toString());
			webTreeBean.setIcon("images/treeNodeImages/organization.gif");
			if (childOrg + childPos == 0) {
				// menu.setCls("file");
				webTreeBean.setLeaf(true);
			} else {
				// menu.setCls("folder");
				webTreeBean.setLeaf(false);
			}
			menusList.add(webTreeBean);
		}
		return menusList;
	}

	/**
	 * 
	 * 根据岗位名称模糊查询
	 * 
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<TmpMatchTreePostBean> searchPositionByName(String name,
			Long projectId) throws Exception {
		try {
			String sql = "select jmap.ACT_POS_NUM,jmap.ACT_POS_NAME,jmap.DEPT_NUM,jmad.DEPT_NAME "
					+ " from JECN_MATCH_ACTUAL_DEPT jmad,JECN_MATCH_ACTUAL_POS jmap where "
					+ " jmap.DEPT_NUM = jmad.DEPT_NUM and jmap.ACT_POS_NAME like ? and jmad.PROJECT_ID=? order by jmad.DEPT_NAME";
			// 岗位
			List<Object[]> listPos = matchPostTreeDao.listNativeSql(sql, 0, 20,
					"%" + name + "%", projectId);
			List<TmpMatchTreePostBean> listResult = getSearchWebList(listPos);
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 数组0：岗位编号；1：岗位名称；2：部门节点编号；3:部门节点名称
	 * 
	 * @param listPos
	 *            根据岗位名称查询的结果集
	 * @return List<WebTreeBean>
	 */
	private List<TmpMatchTreePostBean> getSearchWebList(List<Object[]> listPos) {
		if (listPos == null) {
			return null;
		}
		List<TmpMatchTreePostBean> menusList = new ArrayList<TmpMatchTreePostBean>();

		for (Object[] obj : listPos) {
			if (obj == null || obj[0] == null || obj[1] == null
					|| obj[2] == null) {
				continue;
			}
			TmpMatchTreePostBean treePostBean = new TmpMatchTreePostBean();
			// 岗位编号
			treePostBean.setId(obj[0].toString());
			// 岗位名称
			treePostBean.setName(obj[1].toString());
			// 部门节点编号
			treePostBean.setPName(obj[2].toString());
			menusList.add(treePostBean);
		}
		return menusList;
	}

	/**
	 * 
	 * 根据基准岗位名称模糊查询
	 * 
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<TmpMatchTreePostBean> searchBaseNameByName(String baseName)
			throws Exception {
		try {
			String sql = "select jmap.base_pos_num, jmap.base_pos_name"
					+ "  from JECN_MATCH_ACTUAL_POS jmap"
					+ " where jmap.ACT_POS_NAME like ? "
					+ " group by jmap.base_pos_num, jmap.base_pos_name";

			// 岗位
			List<Object[]> listPos = matchPostTreeDao.listNativeSql(sql, 0, 20,
					"%" + baseName + "%");
			List<TmpMatchTreePostBean> listResult = getSearchBaseNameList(listPos);
			return listResult;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 数组0：基准岗位编号；1：基准岗位名称；
	 * 
	 * @param listPos
	 *            根据岗位名称查询的结果集
	 * @return List<WebTreeBean>
	 */
	private List<TmpMatchTreePostBean> getSearchBaseNameList(
			List<Object[]> listPos) {
		if (listPos == null) {
			return null;
		}
		List<TmpMatchTreePostBean> menusList = new ArrayList<TmpMatchTreePostBean>();

		for (Object[] obj : listPos) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			TmpMatchTreePostBean treePostBean = new TmpMatchTreePostBean();
			// 基准岗位ID
			treePostBean.setId(obj[0].toString());
			// 基准岗位名称
			treePostBean.setName(obj[1].toString());
			// 部门名称
			treePostBean.setPName(MatchErrorInfo.BASE_POS_TITLE);
			menusList.add(treePostBean);
		}
		return menusList;
	}

	public IMatchPostTreeBeanDao getMatchPostTreeDao() {
		return matchPostTreeDao;
	}

	public void setMatchPostTreeDao(IMatchPostTreeBeanDao matchPostTreeDao) {
		this.matchPostTreeDao = matchPostTreeDao;
	}

}
