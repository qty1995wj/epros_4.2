package com.jecn.epros.server.action.web.login.ad.wanhua;

import javax.servlet.http.Cookie;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 万华SSO
 * 
 * @author ZXH
 * 
 */
public class WanHuaLoginAction extends JecnAbstractADLoginAction {

	public WanHuaLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 通过单点登录
	 * 
	 * @return String input;main;success;null
	 * 
	 */
	public String login() {
		String ltpaToken = getTokenByCookie();
		if (StringUtils.isBlank(ltpaToken)) {
			return LoginAction.INPUT;
		}

		try {
			String loginName = JecnWanHuaKey.getUid(ltpaToken);
			return this.loginByLoginName(loginName.toLowerCase());
		} catch (Exception e) {
			log.error("", e);
			e.printStackTrace();
		}

		return LoginAction.INPUT;
	}

	private String getTokenByCookie() {
		Cookie[] allCookies = this.loginAction.getRequest().getCookies();
		for (Cookie cookie : allCookies) {
			if ("LtpaToken".equals(cookie.getName())) {
				return cookie.getValue();
			}
		}
		return null;
	}
}
