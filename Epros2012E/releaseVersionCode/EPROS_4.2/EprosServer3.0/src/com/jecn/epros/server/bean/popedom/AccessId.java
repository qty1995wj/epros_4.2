package com.jecn.epros.server.bean.popedom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AccessId implements Serializable {
	/**
	 * 部门权限ID
	 */
	private List<AccessId> orgAccessId;
	/**
	 * 岗位权限ID
	 */
	private List<AccessId> posAccessId;
	/**
	 * 岗位组 ID
	 */
	private List<AccessId> PosGroupAccessId;

	/**
	 * 权限ID
	 */
	protected Long accessId;
	/**
	 * 是否具有下载权限
	 */
	protected boolean isDownLoad;

	public AccessId() {

	}

	public Long getAccessId() {
		return accessId;
	}

	public void setAccessId(Long accessId) {
		this.accessId = accessId;
	}

	public boolean isDownLoad() {
		return isDownLoad;
	}

	public void setDownLoad(boolean isDownLoad) {
		this.isDownLoad = isDownLoad;
	}

	public List<AccessId> getOrgAccessId() {
		if (orgAccessId == null) {
			return new ArrayList<AccessId>();
		}
		return orgAccessId;
	}

	public void setOrgAccessId(List<AccessId> orgAccessId) {
		this.orgAccessId = orgAccessId;
	}

	public List<AccessId> getPosAccessId() {
		if (posAccessId == null) {
			return new ArrayList<AccessId>();
		}
		return posAccessId;
	}

	public void setPosAccessId(List<AccessId> posAccessId) {
		this.posAccessId = posAccessId;
	}

	public List<AccessId> getPosGroupAccessId() {
		if (PosGroupAccessId == null) {
			return new ArrayList<AccessId>();
		}
		return PosGroupAccessId;
	}

	public void setPosGroupAccessId(List<AccessId> posGroupAccessId) {
		PosGroupAccessId = posGroupAccessId;
	}

}
