package epros.designer.gui.system.config.ui.dialog;

import java.awt.Dimension;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.gui.system.config.ui.tree.JecnTreeModel;
import epros.designer.gui.system.config.ui.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

/**
 * 
 * 权限配置对话框
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPubSetConfigJDialog extends JecnAbtractBaseConfigDialog {

	public JecnPubSetConfigJDialog(JecnConfigDesgBean configBean) {
		super(configBean);
		this.setTitle(JecnProperties.getValue("setTitlePubName"));
		JecnTreeNode rootNode=(JecnTreeNode)this.treePanel.getJecnTree().getModel().getRoot();
		JecnTreeNode node = null;
		if(!JecnConstants.isEPS()){
			//flowRuleTypeName:流程制度类别配置
			node = new JecnTreeNode(JecnProperties
					.getValue("flowRuleTypeName"));
		}else{//flowRuleTypeName:流程类别配置
			node = new JecnTreeNode(JecnProperties
					.getValue("flowTypeName"));
		}
		if (JecnConstants.versionType!=1){//非EPROS-D2
			rootNode.add(node);
		}
		
		//是否存在浏览端，没有浏览端隐藏指定收件人配置
		if(JecnConstants.isPubShow()){
			node = new JecnTreeNode(JecnProperties.getValue("pointReceivePeople"));
			rootNode.add(node);	
		}
		
		((JecnTreeModel)this.treePanel.getJecnTree().getModel()).reload(rootNode);
		// 大小
		Dimension size = new Dimension(680, 620);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
	}
}
