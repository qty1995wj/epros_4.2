<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div>
			<form action="getRuleList.action" id="ruleForm">
				<div id="search" class="divBorder" align="center">
					<div id="ruleSearchText" style="overflow: hidden;" class="minWidth">
						<table width="100%">
							<tr>
								<td width="10%" height="25px" align="right">
									<!-- 制度名称： -->
									<s:text name="ruleNameC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 80%" id="ruleName_id"
										maxLength="32" />
								</td>


								<td width="10%" align="right">
									<!-- 制度编号： -->
									<s:text name="ruleNumberC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" type="text" style="width: 80%"
										id="ruleNum_id" maxLength="32" />
								</td>

								<td style="vertical-align: middle;" width="10%">
									<a onClick="toggle(ruleSearchText)" style="cursor: pointer;"
										class="packUp"> <img id="packUp"
											src="${basePath}images/iParticipateInProcess/on.gif"
											align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
												name="packUp" />
									</span> </a>
								</td>
							</tr>

							<tr>
								<td height="25px" align="right">
									<!-- 制度类别: -->
									<s:text name="ruleTypeC" />
								</td>
								<td align="left">
									<select style="width: 80%" id="ruleType_id">
										<option value="-1">
											<s:text name="all" />
										</option>
										<s:iterator value="ruleTypeList">
											<option value="<s:property value="typeId" />">
												<s:property value="typeName" />
											</option>
										</s:iterator>
									</select>
								</td>

								<td align="right">
									<!-- 密级: -->
									<s:text name="intensiveC" />
								</td>
								<td align="left">
									<select style="width: 80%" id="secretLevel_id">
										<option value="-1">
											<s:text name="all" />
										</option>
										<option value="1">
											<s:property value="#application.public"/>
										</option>
										<option value="0">
											${secret}
										</option>
									</select>
								</td>

								<td></td>
							</tr>


							<tr>
								<td height="25px;" align="right">
									<!-- 责任部门: -->
									<s:text name="responsibilityDepartmentC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgDutyName_rule_id"
										onkeyup="search(orgDutyName_rule_id,orgDutyId_rule_id,search_rule_orgDutyName,'org');"
										onblur="divNode(search_rule_orgDutyName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgDutyName_rule_id","orgDutyId_rule_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_rule_orgDutyName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td align="right">
									<!-- 查阅部门: -->
									<s:text name="referToTheDepartmentC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgLookName_rule_id"
										onkeyup="search(orgLookName_rule_id,orgLookId_rule_id,search_rule_orgLookName,'org');"
										onblur="divNode(search_rule_orgLookName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgLookName_rule_id","orgLookId_rule_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_rule_orgLookName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td></td>
							</tr>
							<tr>
								<td height="25px;" align="right">
									<!-- 查阅岗位: -->
									<s:text name="referToThePostC" />
								</td>
								<td align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="posLookName_rule_id"
										onkeyup="search(posLookName_rule_id,posLookId_rule_id,search_rule_posLookName,'pos');"
										onblur="divNode(search_rule_posLookName)" maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("position","posLookName_rule_id","posLookId_rule_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_rule_posLookName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>

								<td></td>
								<td></td>

								<td></td>
							</tr>
						</table>
					</div>

					<div id="searchBut" align="center">
						<img src="${basePath}images/iParticipateInProcess/search.gif"
							style="cursor: pointer;" onclick="ruleSearch()" />
						<img src="${basePath}images/iParticipateInProcess/reset.gif"
							style="cursor: pointer;" onclick="ruelSearchReset()" />
					</div>
				</div>
				<!-- 责任部门ID -->
				<input type="hidden" id="orgDutyId_rule_id" />
				<!-- 查阅部门ID -->
				<input type="hidden" id="orgLookId_rule_id" />
				<!-- 查阅岗位ID -->
				<input type="hidden" id="posLookId_rule_id" />
			</form>
			<div id="ruleSysGrid_id">

			</div>
		</div>
	</body>
</html>