package com.jecn.epros.domain.process;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.util.ConfigUtils;

public class FlowActiveRelatedRiskControlPoint {
    /**
     * 控制点编号
     */
    private String controlCode;
    /**
     * 控制目标
     */
    private String controlTarget;
    /**
     * 风险ID
     */
    private Long riskId;
    /**
     * 风险编号
     */
    private String riskNum;
    /**
     * 风险描述
     */
    private String riskName;
    /**
     * 是否是关键控制点：0：是，1：否
     */
    private int keyPoint;
    /**
     * 控制频率 0:随时 1:日 2: 周 3:月 4:季度 5:年度
     */
    private int frequency;
    /**
     * 控制方法 0:人工1:自动
     */
    private int method;
    /**
     * 控制类型 0:预防性1:发现性
     */
    private int type;

    public String getKeyPointStr() {
        return ConfigUtils.getRiskKeyPoint(keyPoint);
    }

    public String getFrequencyStr() {
        return ConfigUtils.getRiskFrequency(frequency);
    }

    public String getMethodStr() {
        return ConfigUtils.getRiskMethod(method);
    }

    public String getTypeStr() {
        return ConfigUtils.getRiskType(type);
    }

    public String getControlCode() {
        return controlCode;
    }

    public void setControlCode(String controlCode) {
        this.controlCode = controlCode;
    }

    public String getControlTarget() {
        return controlTarget;
    }

    public void setControlTarget(String controlTarget) {
        this.controlTarget = controlTarget;
    }

    public void setRiskId(Long riskId) {
        this.riskId = riskId;
    }

    public void setRiskNum(String riskNum) {
        this.riskNum = riskNum;
    }

    public LinkResource getLink() {
        return new LinkResource(riskId, riskNum, ResourceType.RISK, TreeNodeUtils.NodeType.process);
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public int getKeyPoint() {
        return keyPoint;
    }

    public void setKeyPoint(int keyPoint) {
        this.keyPoint = keyPoint;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
