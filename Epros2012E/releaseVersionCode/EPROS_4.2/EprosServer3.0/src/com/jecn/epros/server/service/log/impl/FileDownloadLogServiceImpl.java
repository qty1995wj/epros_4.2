package com.jecn.epros.server.service.log.impl;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.log.FileDownloadLog;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.dao.log.IFileDownloadLogDao;
import com.jecn.epros.server.service.log.IFileDownloadLogService;

@Transactional
public class FileDownloadLogServiceImpl extends
		AbsBaseService<FileDownloadLog, Long> implements
		IFileDownloadLogService {

	private IFileDownloadLogDao fileDownloadLogDao;

	public void setFileDownloadLogDao(IFileDownloadLogDao fileDownloadLogDao) {
		this.fileDownloadLogDao = fileDownloadLogDao;
		this.baseDao = fileDownloadLogDao;
	}

}
