package com.jecn.epros.mapper;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.MyStar;
import com.jecn.epros.domain.UserResourceStatistics;
import com.jecn.epros.domain.org.SearchPosResultBean;
import com.jecn.epros.domain.process.FlowSearchResultBean;
import com.jecn.epros.domain.process.map.DeptAerialData;
import com.jecn.epros.domain.report.base.CharItem;
import com.jecn.epros.domain.risk.SearchRiskResultBean;
import com.jecn.epros.domain.rule.SearchRuleResultBean;
import com.jecn.epros.domain.user.*;
import com.jecn.epros.sqlprovider.UserSqlProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserMapper {

    @Select("select people_id,login_name,true_name from jecn_user ju")
    @ResultMap("mapper.SystemManage.JecnUserResult")
    List<JecnUser> findAllUsers();


    @SelectProvider(type = UserSqlProvider.class,method = "findMyResponsibilityProcess")
    public List<MyResponsibilityProcess> findMyResponsibilityProcess(Map<String ,Object> map);

    @SelectProvider(type = UserSqlProvider.class,method = "findMyResponsibilityRule")
    public List<MyResponsibilityRule> findMyResponsibilityRule(Map<String ,Object> map);

    @Select("select ju.people_id,ju.login_name,ju.true_name,jfoi.figure_id, jfoi.figure_text, jfo.org_id,jfo.org_name from jecn_user ju "
            + "left join jecn_user_position_related jupr on jupr.people_id=ju.people_id"
            + "left join jecn_flow_org_image jfoi on jupr.figure_id=jfoi.figure_id"
            + "left join jecn_flow_org jfo on jfoi.org_id = jfo.org_id and and jfo.del_state=0")
    @ResultMap("mapper.SystemManage.UserManageResult")
    JecnUser selectUserWithOrganization(int studId);

    @SelectProvider(type = UserSqlProvider.class, method = "findUserNameByIdSql")
    public String findUserNameById(Long peopleId);

    @Select("select * from jecn_user ju where people_id=#{createPersonId}")
    @ResultMap("mapper.SystemManage.JecnUserResult")
    public JecnUser getUser(Long createPersonId);

    @SelectProvider(type = UserSqlProvider.class, method = "findUsers")
    @ResultMap("mapper.SystemManage.JecnUserResult")
    List<JecnUser> findUsers(Map<String, Object> m);


    @Select("select count(1) from jecn_user where people_id=#{peopleId} and islock=0")
    public int isPeopleExist(Long peopleId);

    @Select("select ju.* from jecn_user ju where people_id in ${_parameter}")
    @ResultMap("mapper.SystemManage.JecnUserResult")
    public List<JecnUser> fetchUserByIds(String ids);

    @Select("select ju.* from jecn_user ju" + " inner join JECN_USER_ROLE jur on ju.people_id=jur.people_id"
            + " inner join JECN_ROLE_INFO jri on jri.role_id=jur.role_id"
            + " where jri.filter_id='admin' and ju.islock=0")
    @ResultMap("mapper.SystemManage.JecnUserResult")
    List<JecnUser> getAdmins();

    @Select("select ju.people_id from JECN_FIXED_EMAIL jfe" + " inner join jecn_user ju on jfe.person_id=ju.people_id "
            + " where ju.islock=0 and jfe.type=#{type}")
    @ResultMap("mapper.SystemManage.JecnUserResult")
    List<JecnUser> getFixeds(int type);

    @Select("SELECT DISTINCT U.PEOPLE_ID" +
            "  FROM JECN_USER U" +
            " INNER JOIN JECN_USER_POSITION_RELATED U_P" +
            "    ON U.PEOPLE_ID = U_P.PEOPLE_ID" +
            "  LEFT JOIN JECN_ACCESS_PERMISSIONS_T J_P" +
            "    ON J_P.FIGURE_ID = U_P.FIGURE_ID" +
            "   AND J_P.RELATE_ID = #{rid}" +
            " WHERE U.ISLOCK = 0" +
            "   AND J_P.RELATE_ID IS NOT NULL")
    List<Long> fetchPosAccessPermissApprove(Long rid);

    @Select("select distinct u.people_id"
            + "          from jecn_user u, jecn_user_position_related u_p"
            + "         where u.islock = 0"
            + "           and u.people_id = u_p.people_id"
            + "           and u_p.figure_id in"
            + "               (select distinct j_p.figure_id"
            + "                  from Jecn_Flow_Org_Image j_p, Jecn_Org_Access_Perm_t j_o"
            + "                 where j_p.org_id = j_o.org_id  and j_o.relate_id=#{rid})")
    List<Long> fetchOrgAccessPermissApprove(Long rid);

    @Select("SELECT J_U_P_R.PEOPLE_ID FROM JECN_USER_POSITION_RELATED J_U_P_R"
            + " WHERE FIGURE_ID IN(SELECT J_F_I.FIGURE_ID  FROM JECN_FLOW_ORG_IMAGE J_F_I "
            + " WHERE  FIGURE_ID IN (SELECT FIGURE_ID FROM JECN_POSITION_GROUP_R J_P_R"
            + " WHERE J_P_R.GROUP_ID IN(SELECT J_P.POSTGROUP_ID FROM JECN_GROUP_PERMISSIONS J_P WHERE J_P.RELATE_ID=#{rid})))")
    List<Long> fetchGroupAccessPermissApprove(Long rid);

    @Select("SELECT DISTINCT U.PEOPLE_ID" +
            "  FROM JECN_USER U" +
            " INNER JOIN JECN_USER_POSITION_RELATED U_P" +
            "    ON U.PEOPLE_ID = U_P.PEOPLE_ID" +
            "  LEFT JOIN JECN_ACCESS_PERMISSIONS_T J_P" +
            "    ON U_P.FIGURE_ID = J_P.FIGURE_ID" +
            "   AND J_P.TYPE = #{type}" +
            "   AND J_P.RELATE_ID = #{rid}" +
            " WHERE U.ISLOCK = 0" +
            "   AND J_P.RELATE_ID IS NOT NULL")
    Set<Long> fetchPosAccessPermissApproveByType(Map<String, Object> paramMap);

    /**
     * 流程角色的参与人
     *
     * @param flowId
     * @return
     */
    @Select("select  u.*"
            + "       from jecn_user u,"
            + "       jecn_user_position_related u_p"
            + "       where u.people_id = u_p.people_id and u.islock=0  and"
            + "      ( u_p.figure_id in"
            + "       (select jfoi.figure_id  from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related_t psrt"
            + "       ,jecn_flow_structure_image_t jfsi"
            + "       where psrt.type='0' and psrt.figure_position_id = jfoi.figure_id and jfoi.org_id = jfo.org_id"
            + "       and jfo.del_state=0 and psrt.figure_flow_id =jfsi.figure_id and jfsi.flow_id=#{flowId} "
            + ")"
            + "       or"
            + "       u_p.figure_id in"
            + "        (select jfoi.figure_id  from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related_t psrt"
            + "       ,jecn_flow_structure_image_t jfsi,jecn_position_group_r jpgr"
            + "       where psrt.type='1' and psrt.figure_position_id = jpgr.group_id and jpgr.figure_id=jfoi.figure_id"
            + "        and jfoi.org_id = jfo.org_id"
            + "       and jfo.del_state=0 and psrt.figure_flow_id =jfsi.figure_id and jfsi.flow_id=#{flowId} "
            + "))")
    @ResultMap("mapper.SystemManage.JecnUserResult")
    List<JecnUser> fetchRoleRelatedUser(Long flowId);

    @SelectProvider(type = UserSqlProvider.class, method = "getMyResources")
    @ResultMap("mapper.User.UserResourceStatistics")
    UserResourceStatistics getMyResources(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "getMyProcesses")
    @ResultMap("mapper.Process.flowSearchResultBean")
    List<FlowSearchResultBean> getMyProcesses(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "getMyRules")
    @ResultMap("mapper.Rule.searchRuleResultBean")
    List<SearchRuleResultBean> getMyRules(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "getMyRisks")
    @ResultMap("mapper.Risk.searchRiskResultBean")
    List<SearchRiskResultBean> getMyRisks(Map<String, Object> paramMap);

    @Select("select id,user_id,related_id,type,name,create_date,sorted_date from jecn_my_star where user_id=#{userId}")
    @ResultMap("mapper.User.MyStar")
    List<MyStar> getRecommendStars(Map<String, Object> map);

    @SelectProvider(type = UserSqlProvider.class, method = "getMyStarsSql")
    @ResultMap("mapper.User.MyStar")
    List<MyStar> getMyStars(Map<String, Object> map);

    @Select("select * from jecn_my_star where user_id=#{userId} and related_id=#{relatedId} and type=#{type}")
    @ResultMap("mapper.User.MyStar")
    MyStar getStar(Map<String, Object> params);

    @Delete("delete from jecn_my_star where id=#{id}")
    void deleteStar(Map<String, Object> map);

    @Update("update jecn_my_star set sorted_date=#{sortedDate} where id=#{id}")
    void updateStar(Map<String, Object> map);

    @Insert("insert into jecn_my_star(id,user_id,related_id,type,name,create_date) values(#{id},#{userId},#{relatedId},#{type},#{name},#{createDate})")
    void addStar(Map<String, Object> map);

    @Insert("insert into ROLE_PERMISSIONS_MENU(guid,role_id,menu_id) values(#{guid},#{roleId},#{menuId})")
    void addWebAccessRoleMenu(Map<String, Object> map);

    @Insert("insert into role_permissions(id,name,type,CREATE_TIME,CREATE_PEOPLE_ID,UPDATE_TIME,UPDATE_PEOPLE_ID) " +
            "values(#{id},#{name},#{type},#{createTime},#{createPeopleId},#{updateTime},#{updatePeopleId})")
    void addWebAccessRole(Map<String, Object> map);


    @Update("update role_permissions set UPDATE_TIME=#{updateTime},UPDATE_PEOPLE_ID=#{UPDATE_PEOPLE_ID} where id=#{roleId}")
    void updateWebAccessRole(Map<String, Object> map);

    @Delete("delete from ROLE_PERMISSIONS_MENU where guid=#{guid}")
    void deleteWebAccessRoleMenu(Map<String, Object> map);

    @Select(
            "SELECT DISTINCT T.ID," +
                    "                T.NAME," +
                    "                T.CODE," +
                    "                T.PID," +
                    "                CASE" +
                    "                  WHEN EXIST_ACCESS_ID.MENU_ID IS NULL THEN" +
                    "                   0" +
                    "                  ELSE" +
                    "                   1" +
                    "                END AS EXIST_TYPE," +
                    "                T.SORTID" +
                    "  FROM PERMISSIONS_MENU T" +
                    "  LEFT JOIN (SELECT RPM.MENU_ID" +
                    "               FROM ROLE_PERMISSIONS RP, ROLE_PERMISSIONS_MENU RPM" +
                    "              WHERE RP.ID = #{roleId}" +
                    "                AND RP.ID = RPM.ROLE_ID) EXIST_ACCESS_ID ON T.ID =" +
                    "                                                            EXIST_ACCESS_ID.MENU_ID" +
                    " ORDER BY T.PID, T.SORTID")
    @ResultMap("mapper.User.TreeNodeAccess")
    List<TreeNodeAccess> getWebAccesssByRoleId(Map<String, Object> paramMap);


    @Select("select urp.role_id from USER_ROLE_PERMISSIONS urp where urp.people_id=#{peopleId}")
    List<String> getWebAccessRoleIdByPeopleId(Map<String, Object> paramMap);


    @Select("select urp.id from ROLE_PERMISSIONS urp where urp.type=0")
    List<String> getDefaultWebAccessRoleId();


    @Select("select urp.guid,urp.role_id,urp.menu_id from ROLE_PERMISSIONS_MENU urp where urp.role_id=#{roleId}")
    @ResultMap("mapper.User.RolePermissionsMenu")
    List<RolePermissionsMenu> getRolePermissionsMenusByRoleId(Map<String, Object> paramMap);

    /**
     * 获取登录角色对应的权限菜单
     *
     * @param doMap
     * @return
     */
    @SelectProvider(type = UserSqlProvider.class, method = "getPermissionsMenuByIdSql")
    @ResultMap("mapper.User.PermissionsMenu")
    List<PermissionsMenu> getPermissionsMenuById(Map<String,Object> doMap);

    @SelectProvider(type = UserSqlProvider.class, method = "getPermissionsMenuByPeopleIdSql")
    @ResultMap("mapper.User.PermissionsMenu")
    List<PermissionsMenu> getPermissionsMenuByPeopleId(Long peopleId);

    /**
     * 默认的角色权限菜单
     *
     * @return
     */
    @SelectProvider(type = UserSqlProvider.class, method = "getDefaultPermissionsMenuSql")
    @ResultMap("mapper.User.PermissionsMenu")
    List<PermissionsMenu> getDefaultPermissionsMenu();

    @SelectProvider(type = UserSqlProvider.class, method = "getMyStarsNumSql")
    int getMyStarsNum(Map<String, Object> params);

    @SelectProvider(type = UserSqlProvider.class, method = "getVisitTopProcess")
    @ResultMap("mapper.User.ProcessVisitItem")
    List<ProcessVisitItem> getVisitTopProcess(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "getVisitTopProcessIds")
    List<String> getVisitTopProcessIds(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "getProposeTopProcess")
    @ResultMap("mapper.Report.CharItem")
    List<CharItem> getProposeTopProcess(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "getProposeTopRule")
    @ResultMap("mapper.Report.CharItem")
    List<CharItem> getProposeTopRule(Map<String, Object> paramMap);


    @SelectProvider(type = UserSqlProvider.class, method = "getVisitTopProcessHistogram")
    @ResultMap("mapper.Report.CharItem")
    List<CharItem> getVisitTopProcessHistogram(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "getVisitTopRuleHistogram")
    @ResultMap("mapper.Report.CharItem")
    List<CharItem> getVisitTopRuleHistogram(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "listUserOrgAndPosInfos")
    @ResultMap("mapper.Org.searchPosResultBean")
    List<SearchPosResultBean> listUserOrgAndPosInfos(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "validateUserPwd")
    int validateUserPwd(Map<String, Object> paramMap);

    @Update("update jecn_user set password=#{pwd} where people_id=#{peopleId}")
    void updateUserPwd(Map<String, Object> paramMap);

    @SelectProvider(type = UserSqlProvider.class, method = "findDeptAerialMapsByPeopleSql")
    List<DeptAerialData> findDeptAerialMapsByPeople(Map<String, Object> paramMap);

    @Select("select U.PEOPLE_ID from JECN_USER U where U.LOGIN_NAME=#{loginName} and U.ISLOCK = 0")
    Long getUserIdByLoginName(Map<String, Object> paramMap);

    @Select("select U.PEOPLE_ID,u.login_name from JECN_USER U WHERE SUBSTR(u.email,0,INSTR(u.email,'@')-1) = #{emailName} and U.ISLOCK = 0")
    Long getUserIdByEmailName(Map<String, Object> paramMap);
}
