/**
 * SSOWebServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cetc10.sso;

public interface SSOWebServiceSoap extends java.rmi.Remote {
    public int userLogon(java.lang.String logonName, java.lang.String IPAddrStr) throws java.rmi.RemoteException;
    public int userLogoff(java.lang.String IPAddrStr) throws java.rmi.RemoteException;
    public com.jecn.webservice.cetc10.sso.UserInfo getUserInfo(java.lang.String IPAddrStr) throws java.rmi.RemoteException;
    public java.lang.String getUserInfoStr(java.lang.String IPAddrStr) throws java.rmi.RemoteException;
    public com.jecn.webservice.cetc10.sso.ApplicationInfo[] getAppInfo(java.lang.String IPAddrStr) throws java.rmi.RemoteException;
}
