package epros.designer.tree;

import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;

public abstract class JecnTreeListener  implements TreeExpansionListener {

	@Override
	public abstract void treeExpanded(TreeExpansionEvent event);

	@Override
	public abstract void treeCollapsed(TreeExpansionEvent event);

}
