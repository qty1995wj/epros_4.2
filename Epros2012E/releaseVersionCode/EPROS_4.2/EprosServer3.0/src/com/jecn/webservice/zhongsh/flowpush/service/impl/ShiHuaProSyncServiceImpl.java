package com.jecn.webservice.zhongsh.flowpush.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.webservice.zhongsh.flowpush.dao.IShiHuaDao;
import com.jecn.webservice.zhongsh.flowpush.service.IShiHuaProSyncService;
import com.jecn.webservice.zhongsh.flowpush.service.buss.ShiHuaXmlHandle;

@Service("shihuaSyncService")
@Transactional
public class ShiHuaProSyncServiceImpl implements IShiHuaProSyncService {

	@Autowired
	private IShiHuaDao shiHuaDao;

	@Override
	public int SysncFlowToBps(Long flowId) {
		ShiHuaXmlHandle handle = new ShiHuaXmlHandle();
		return handle.sysncFlowToBps(flowId, shiHuaDao);
	}
}
