package com.jecn.epros.mapper;

import com.jecn.epros.domain.process.relatedFile.RelatedActivity;
import com.jecn.epros.domain.process.relatedFile.RelatedFlow;
import com.jecn.epros.domain.report.*;
import com.jecn.epros.sqlprovider.ReportSqlProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ReportMapper {

    @SelectProvider(type = ReportSqlProvider.class, method = "processDetailView")
    @ResultMap("mapper.Report.ProcessDetail")
    List<ProcessDetail> processDetailView(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "processOptimizeView")
    @ResultMap("mapper.Report.ProcessScan")
    List<ProcessScan> processOptimizeView(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "processKpiView")
    @ResultMap("mapper.Report.kpiView")
    List<KpiView> processKpiView(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "processRoleNum")
    @ResultMap("mapper.Report.processRoleNum")
    List<RoleStatistic> processRoleNum(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "processActivityNum")
    @ResultMap("mapper.Report.processActivityNum")
    List<ActivityStatistic> processActivityNum(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "processRoleDetailNum")
    int processRoleDetailNum(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "processRoleDetail")
    @ResultMap("mapper.Process.relatedFlow")
    List<RelatedFlow> processRoleDetail(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "processActivityDetail")
    @ResultMap("mapper.Process.relatedFlow")
    List<RelatedFlow> processActivityDetail(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "processActivityDetailNum")
    int processActivityDetailNum(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "processInformation")
    @ResultMap("mapper.Report.processInformation")
    List<InformationStatistic> processInformation(Map<String, Object> map);

    /**
     * 信息化关联的活动的数量
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ReportSqlProvider.class, method = "processInformationDetailNum")
    int processInformationDetailNum(Map<String, Object> map);

    /**
     * 信息化关联的活动的详情
     *
     * @param map
     * @return
     */
    @SelectProvider(type = ReportSqlProvider.class, method = "processInformationDetail")
    @ResultMap("mapper.Process.relatedActivity")
    List<RelatedActivity> processInformationDetail(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "primaryManageNum")
    int primaryManageNum(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "fetchPrimaryManage")
    @ResultMap("mapper.Report.PrimaryStatistic")
    List<PrimaryStatistic> fetchPrimaryManage(Map<String, Object> map);

    @SelectProvider(type = ReportSqlProvider.class, method = "primaryManageStatistic")
    @ResultType(HashMap.class)
    Map<String, Object> primaryManageStatistic();

    @SelectProvider(type = ReportSqlProvider.class, method = "reportPermissionsReports")
    @ResultMap("mapper.Report.PermissionsReport")
    List<PermissionsReport> reportPermissionsReports(Map<String, Object> map);
}
