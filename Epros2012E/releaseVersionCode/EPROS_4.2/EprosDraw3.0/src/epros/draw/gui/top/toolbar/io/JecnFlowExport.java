package epros.draw.gui.top.toolbar.io;

import java.awt.Component;
import java.awt.Point;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnElement;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnModeFigureData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.data.line.JecnLineSegmentData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitlePanel;
import epros.draw.system.JecnSystemData;
import epros.draw.system.JecnUserInfo;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUserCheckUtil;
import epros.draw.util.JecnXmlUtil;

/**
 * 流程图导出
 * 
 * @author Administrator
 * 
 */
public class JecnFlowExport {
	private static final Log log = LogFactory.getLog(JecnFlowExport.class);
	private Document doc = null;
	/** 0是流程地图;1是流程图;2是组织图 */
	private String imageType;

	/**
	 * 画图面板保存弹出框处理
	 * 
	 * @param fileName
	 *            保存的文件名称
	 * @param type
	 *            判断点击的是保存还是另存为 true 为另存为 ， false为保存
	 * @return true:保存成功，false:保存失败
	 */
	public boolean initialize(JecnDrawDesktopPane desktopPane, String fileName, boolean type) {
		// 设置界面风格居中
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			// 获取默认路径
			String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveFileDirectory();
			// 获取截取后正确的路径
			if (pathUrl != null && !"".equals(pathUrl)) {
				pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
			}
			JecnFileChooser fileChooser = new JecnFileChooser(pathUrl);
			// 移除系统给定的文件过滤器
			fileChooser.setAcceptAllFileFilterUsed(false);
			// 设置标题
			fileChooser.setDialogTitle(JecnResourceUtil.getJecnResourceUtil().getValue("pleaseChooseThePath"));
			// 过滤文件
			fileChooser.setFileFilter(new ImportFlowFilter("epr"));
			// 文件名称获取当前面板名称
			fileChooser.setSelectedFile(new File(fileName));
			// 判断主界面是否存在
			if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
				return false;
			}
			int i = fileChooser.showSaveDialog(null);
			// 选择确认（yes、ok）后返回该值。
			if (i == JFileChooser.APPROVE_OPTION) {
				fileChooser.getTextFiled().getText();
				// 获取面板数据
				JecnFlowMapData flowMap = desktopPane.getFlowMapData();
				// 获取输出文件的路径
				String fileDirectory = fileChooser.getSelectedFile().getPath();

				String pathName = fileChooser.getSelectedFile().getName();
				String error = JecnUserCheckUtil.checkFileName(pathName);
				if (!"".equals(error)) {
					JecnOptionPane.showMessageDialog(null, error, JecnResourceUtil.getJecnResourceUtil()
							.getValue("tip"), JOptionPane.ERROR_MESSAGE);
					return false;
				}
				// 修改配置文件中的路径
				JecnSystemData.readCurrSystemFileIOData().setSaveFileDirectory(fileDirectory);
				JecnSystemData.writeCurrSystemFileIOData();
				String xmlPath = null;
				// 判断文件后缀名是否为epr结尾
				if (fileDirectory.endsWith(".epr")) {
					xmlPath = fileDirectory;
				} else {
					xmlPath = fileDirectory + ".epr";
				}
				// 判断是否为保存操作或另存为 true 另存为 false 保存
				if (type) {
					// 判断文件是否存在
					File f = new File(xmlPath);
					if (f.exists()) {
						int result = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
								.getJecnResourceUtil().getValue("theCurrentFileAlreadyExistsIsCovered"),
								JecnResourceUtil.getJecnResourceUtil().getValue("confirm"), JOptionPane.YES_NO_OPTION);
						if (result != JOptionPane.YES_NO_OPTION) {
							return false;
						}
					}
				}
				WorkTask task = new WorkTask();
				task.setFlowMap(flowMap);
				task.setType(type);
				task.setXmlFile(xmlPath);
				String errorStr = JecnLoading.start(task);

				if (!DrawCommon.isNullOrEmtryTrim(errorStr) && !"0".equals(errorStr)) {
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
							.getJecnResourceUtil().getValue("saveFailed"));
					return false;
				} else {
					boolean isSave = task.get();
					// 判断面板路径是否为空
					if (isSave && EprosType.eprosDraw == JecnSystemStaticData.getEprosType()) {
						desktopPane.setFlowFilePath(xmlPath);
						updateName(pathName);
					}
					return isSave;
				}
			}
			return true;
		} catch (Exception ex) {
			log.error("", ex);
			return false;
		}
	}

	/**
	 * 面板是否相等
	 * 
	 * @author fuzhh Jan 10, 2013
	 * @return
	 */
	public static void updateName(String flowName) {
		int count = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabCount();
		// 当前面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		for (int i = 0; i < count; i++) {
			Component component = JecnDrawMainPanel.getMainPanel().getTabbedPane().getTabComponentAt(i);
			if (component instanceof JecnTabbedTitlePanel) {
				JecnTabbedTitlePanel tabbedTitlePanel = (JecnTabbedTitlePanel) component;
				JecnDrawDesktopPane drawDesktopPane = tabbedTitlePanel.getCurrDesktopPane();
				if (drawDesktopPane.equals(workflow)) {
					tabbedTitlePanel.setTabText(flowName);
				}
			}
		}
	}

	/**
	 * @author yxw 2012-8-10
	 * @description：面板保存进度控制线程
	 */
	class WorkTask extends SwingWorker<Boolean, Void> {
		private JecnFlowMapData flowMap;
		private String xmlFile;
		private boolean type;

		@Override
		protected Boolean doInBackground() throws Exception {
			try {
				boolean isSave = true;
				int num = saveFlow(flowMap, xmlFile, type);
				if (num != 0) {
					isSave = false;
				}
				JecnLoading.setError(String.valueOf(num));
				return isSave;
			} catch (Exception e) {
				log.error("", e);
				return false;
			}

		}

		@Override
		public void done() {
			JecnLoading.stop();
		}

		public JecnFlowMapData getFlowMap() {
			return flowMap;
		}

		public void setFlowMap(JecnFlowMapData flowMap) {
			this.flowMap = flowMap;
		}

		public String getXmlFile() {
			return xmlFile;
		}

		public void setXmlFile(String xmlFile) {
			this.xmlFile = xmlFile;
		}

		public boolean isType() {
			return type;
		}

		public void setType(boolean type) {
			this.type = type;
		}

	}

	/**
	 * /** 导出为xml文件
	 * 
	 * @author fuzhh Jan 10, 2013
	 * 
	 * @param flowMap
	 *            需要导出的数据
	 * @param xmlFile
	 *            String xml路径
	 * @param type
	 *            判断点击的是保存还是另存为 true 为另存为 ， false为保存
	 * @return 0 正常 1 异常 2 取消覆盖
	 */
	public int saveFlow(JecnFlowMapData flowMap, String xmlFile, boolean type) {
		// 读取XML 获取Document 对象
		doc = JecnXmlUtil.getDocument();
		if (doc != null) {
			JecnElement workflowInfo = new JecnElement(doc.createElement("workflowInfo"));
			if (MapType.totalMap == flowMap.getMapType() || MapType.totalMapRelation == flowMap.getMapType()) {
				imageType = "0";
			} else if (MapType.partMap == flowMap.getMapType()) {
				imageType = "1";
			} else if (MapType.orgMap == flowMap.getMapType()) {
				imageType = "2";
			} else {
				imageType = "";
			}
			// 3.0表示新版 为空或不存在代表老版
			workflowInfo.setAttribute("WorkType", JecnWorkflowConstant.EPROSDRAW_RESION);
			// 面板的宽
			workflowInfo.setAttribute("WorkflowWidth", String.valueOf(flowMap.getWorkflowWidth()));
			// 面板的高
			workflowInfo.setAttribute("WorkflowHeight", String.valueOf(flowMap.getWorkflowHeight()));

			// 流程操作说明
			workflowInfo.setAttribute("Purpose", flowMap.getFlowOperationBean().getFlowPurpose());// 目的
			workflowInfo.setAttribute("Applicability", flowMap.getFlowOperationBean().getApplicability());// 适用范围
			workflowInfo.setAttribute("NoutGlossary", flowMap.getFlowOperationBean().getNoutGlossary());// 术语定义
			workflowInfo.setAttribute("FlowInput", flowMap.getFlowOperationBean().getFlowInput());// 输入
			workflowInfo.setAttribute("FlowOutput", flowMap.getFlowOperationBean().getFlowOutput());// 输出
			workflowInfo.setAttribute("FlowStartActive", flowMap.getFlowOperationBean().getFlowStartActive());// 起始活动
			workflowInfo.setAttribute("FlowEndActive", flowMap.getFlowOperationBean().getFlowEndActive());// 终止活动
			workflowInfo.setAttribute("NoApplicability", flowMap.getFlowOperationBean().getNoApplicability());// 不适用范围
			workflowInfo.setAttribute("FlowSummarize", flowMap.getFlowOperationBean().getFlowSummarize());// 概况信息
			workflowInfo.setAttribute("FlowSupplement", flowMap.getFlowOperationBean().getFlowSupplement());// 补充说明
			workflowInfo.setAttribute("FlowCustom", flowMap.getFlowOperationBean().getFlowCustom());// 客户

			workflowInfo.setAttribute("FlowFile", flowMap.getFlowOperationBean().getFlowFileStr());// 流程记录
			workflowInfo.setAttribute("Practices", flowMap.getFlowOperationBean().getPracticesStr());// 操作规范
			workflowInfo.setAttribute("RelatedFlow", flowMap.getFlowOperationBean().getRelatedFlowStr());// 相关流程
			workflowInfo.setAttribute("RelatedRule", flowMap.getFlowOperationBean().getRelatedRuleStr());// 相关制度
			workflowInfo.setAttribute("KeyEvaluationIndi", flowMap.getFlowOperationBean().getKeyEvaluationIndiStr());// 流程评测及关键指标

			// 驱动规则
			// 驱动规则 0事件驱动,1是时间驱动,2：时间/事件驱动
			workflowInfo.setAttribute("driveType", flowMap.getFlowOperationBean().getDriveType());
			workflowInfo.setAttribute("driveRules", flowMap.getFlowOperationBean().getDriveRules());
			// 添加5个自定义要素
			workflowInfo.setAttribute("customOne", flowMap.getFlowOperationBean().getCustomOne());
			workflowInfo.setAttribute("customTwo", flowMap.getFlowOperationBean().getCustomTwo());
			workflowInfo.setAttribute("customThree", flowMap.getFlowOperationBean().getCustomThree());
			workflowInfo.setAttribute("customFour", flowMap.getFlowOperationBean().getCustomFour());
			workflowInfo.setAttribute("customFive", flowMap.getFlowOperationBean().getCustomFive());
			// 相关文件(D2版本特有的)
			if (!JecnDesignerProcess.getDesignerProcess().isPubShow()) {
				workflowInfo.setAttribute("flowRelatedFile", flowMap.getFlowOperationBean().getFlowRelatedFile());
			}
			// true 为横向 false 为纵向
			boolean flag = flowMap.isHFlag();
			// 横向或纵向标识 0 为横向 1为纵向
			int hv = 0;
			if (!flag) {
				hv = 1;
			}
			workflowInfo.setAttribute("isHFlag", String.valueOf(hv));
			// 导出的类型
			workflowInfo.setAttribute("flowClassify", imageType);

			// 图形的数据
			for (JecnFigureData figure : flowMap.getFigureList()) {
				getExportFigure(doc, workflowInfo, figure, flag);
			}

			// 线的集合
			for (JecnBaseLineData baseLineData : flowMap.getLineList()) {
				getExportLine(doc, workflowInfo, baseLineData);
			}

			doc.appendChild(workflowInfo.getElement());
			try {
				JecnXmlUtil.writeXmlByDocumentAndAllPath(xmlFile, doc);
			} catch (Exception e) {
				log.error("JecnFlowExport类saveFlow方法关错误！", e);

				JecnOptionPane.showMessageDialog(null, JecnUserInfo.getCreateFileError(), null,
						JOptionPane.ERROR_MESSAGE);
				return 1;
			}
		}
		return 0;
	}

	/**
	 * 生成图形的数据
	 * 
	 * @param workflowInfo
	 * @param figure
	 * @param boolean flag true 为横向 false 为纵向 图形
	 */
	public void getExportFigure(Document doc, Element workflowInfo, JecnFigureData figure, boolean flag) {
		JecnElement figureElement = new JecnElement(doc.createElement("figures"));
		// 横向或纵向标识 0 为横向 1为纵向
		int hv = 0;
		if (!flag) {
			hv = 1;
		}
		// 横向或纵向标识
		figureElement.setAttribute("isXorY", String.valueOf(hv));
		// 图形上添加的字符
		figureElement.setAttribute("FigureText", figure.getFigureText());

		// 图形的标识
		figureElement.setAttribute("FigureType", figure.getMapElemType().toString());

		figureElement.setAttribute("EleShape", figure.getEleShape() == null ? null : figure.getEleShape().toString());
		// 图形的X轴
		double poLongx = figure.getFigureDataCloneable().getDoubleX();
		if (poLongx < 1) {
			poLongx = 1;
		}
		figureElement.setAttribute("PoLongx", String.valueOf(poLongx));

		// 图形的Y轴
		double poLongy = figure.getFigureDataCloneable().getDoubleY();
		if (poLongy < 1) {
			poLongy = 1;
		}
		figureElement.setAttribute("PoLongy", String.valueOf(poLongy));
		// 图形的宽度
		figureElement.setAttribute("Width", String.valueOf(figure.getFigureSizeX()));
		// 图型的高度
		figureElement.setAttribute("Height", String.valueOf(figure.getFigureSizeY()));
		// 字体的大小
		figureElement.setAttribute("FontSize", String.valueOf(figure.getFontSize()));
		// 图形的层级
		figureElement.setAttribute("OrderIndex", String.valueOf(figure.getZOrderIndex()));
		// 图形的填充颜色
		String backColor = "";
		// 图行颜色的类型none为未填充,one为单色效果,two为双色效果 默认未填充单色效果
		if (figure.getFillType() != null) {
			switch (figure.getFillType()) {
			case none:
				// 图形填充色效果标识
				figureElement.setAttribute("BackColorFlag", String.valueOf(0));
				break;
			case one:
				// 图形填充色效果标识
				figureElement.setAttribute("BackColorFlag", String.valueOf(1));
				// 单色填充
				if (figure.getFillColor() != null) {
					backColor = figure.getFillColor().getRed() + "," + figure.getFillColor().getGreen() + ","
							+ figure.getFillColor().getBlue();
				}
				break;
			case two:
				// 图形填充色效果标识
				figureElement.setAttribute("BackColorFlag", String.valueOf(2));
				int colorChangType = 0;
				switch (figure.getFillColorChangType()) {
				case topTilt:
					colorChangType = 1;
					break;
				case bottom:
					colorChangType = 2;
					break;
				case vertical:
					colorChangType = 3;
					break;
				case horizontal:
					colorChangType = 4;
					break;
				}

				// 双色填充
				if (figure.getFillColor() != null && figure.getChangeColor() != null) {
					backColor = +figure.getFillColor().getRed() + "," + figure.getFillColor().getGreen() + ","
							+ figure.getFillColor().getBlue() + "," + figure.getChangeColor().getRed() + ","
							+ figure.getChangeColor().getGreen() + "," + figure.getChangeColor().getBlue() + ","
							+ colorChangType;
				} else if (figure.getFillColor() != null) {
					// 单色填充
					if (figure.getFillColor() != null) {
						backColor = figure.getFillColor().getRed() + "," + figure.getFillColor().getGreen() + ","
								+ figure.getFillColor().getBlue() + "," + colorChangType;
					}
				} else {
					// 图形填充色效果标识
					figureElement.setAttribute("BackColorFlag", String.valueOf(0));
				}
				break;
			}
		}
		figureElement.setAttribute("BGColor", backColor);
		if (figure.getFontColor() != null) {
			// 字体颜色
			figureElement.setAttribute("FontColor", String.valueOf(figure.getFontColor().getRed() + ","
					+ figure.getFontColor().getGreen() + "," + figure.getFontColor().getBlue()));
		}

		// 字体是否加粗0为正常，1为加粗，2为斜体
		figureElement.setAttribute("FontBody", String.valueOf(figure.getFontStyle()));
		// 字体类型:默认为（宋体）
		figureElement.setAttribute("FontType", figure.getFontName());
		// 设置连接ID
		figureElement.setAttribute("LinkflowId", "");
		// 图形ID唯一标识
		figureElement.setAttribute("FigureImageId", figure.getFlowElementUUID());
		// 字体显示为横排还是竖排
		if (figure.getFontUpRight()) {
			figureElement.setAttribute("IsErect", String.valueOf(1));
		} else {
			figureElement.setAttribute("IsErect", String.valueOf(0));
		}
		// 字体位置
		figureElement.setAttribute("FontPosition", String.valueOf(figure.getFontPosition()));
		// 是否存在阴影
		if (figure.isShadowsFlag()) {
			figureElement.setAttribute("HavaShadow", String.valueOf(1));
		} else {
			figureElement.setAttribute("HavaShadow", String.valueOf(0));
		}
		// 是否存在阴影
		if (figure.getShadowColor() != null) {
			// 阴影颜色
			figureElement.setAttribute("ShadowColor", String.valueOf(figure.getShadowColor().getRed() + ","
					+ figure.getShadowColor().getGreen() + "," + figure.getShadowColor().getBlue()));

		} else {
			figureElement.setAttribute("ShadowColor", "");
		}
		// 旋转角度值
		figureElement.setAttribute("Circumgyrate", String.valueOf(figure.getCurrentAngle()));
		// 边框是否加粗
		figureElement.setAttribute("FrameBody", String.valueOf(figure.getBodyWidth()));
		// 边框样式 0实线 1 虚线 2点线 3双线
		figureElement.setAttribute("BodyLine", String.valueOf(figure.getBodyStyle()));
		if (figure.getBodyColor() != null) {
			// 边框颜色
			figureElement.setAttribute("BodyColor", String.valueOf(figure.getBodyColor().getRed() + ","
					+ figure.getBodyColor().getGreen() + "," + figure.getBodyColor().getBlue()));
		} else {
			figureElement.setAttribute("BodyColor", "");
		}

		// 边框的深浅
		figureElement.setAttribute("LineThickness", String.valueOf(figure.getBodyWidth()));

		figureElement.setAttribute("IsTurnOver", "");
		// 是否显示3D
		if (figure.isFlag3D()) {
			figureElement.setAttribute("Figure3D", String.valueOf(1));
		} else {
			figureElement.setAttribute("Figure3D", String.valueOf(0));
		}

		if (DrawCommon.isActive(figure.getMapElemType())) { // 活动
			JecnActiveData activeData = (JecnActiveData) figure;
			// 活动编号
			figureElement.setAttribute("ActivityId", activeData.getActivityNum());
			// 活动说明
			figureElement.setAttribute("ActivityShow", activeData.getAvtivityShow());
			// 关键活动
			figureElement.setAttribute("AvtivityShowAndControl", activeData.getAvtivityShowAndControl());
			// 活动输入
			figureElement.setAttribute("AvtivityInput",
					DrawCommon.isNullOrEmtryTrim(activeData.getActiveInput()) ? activeData.getActivityInput()
							: activeData.getActiveInput());
			// 活动输出
			figureElement.setAttribute("Avtivityoutput",
					DrawCommon.isNullOrEmtryTrim(activeData.getActiveOutPut()) ? activeData.getActivityOutput()
							: activeData.getActiveOutPut());
			// 活动办理时限-目标值
			figureElement.setAttribute("TargetValue", activeData.getTargetValue() != null ? activeData.getTargetValue()
					.toString() : null);
			// 状态值
			figureElement.setAttribute("StatusValue", activeData.getStatusValue() != null ? activeData.getStatusValue()
					.toString() : null);
			// 说明
			figureElement.setAttribute("Explain", activeData.getExplain());
		} else if (DrawCommon.isRole(figure.getMapElemType())) {// 角色
			JecnRoleData roleData = (JecnRoleData) figure;
			// 角色名称
			figureElement.setAttribute("roleName", roleData.getFigureText());
			// 岗位名称
			figureElement.setAttribute("posName", roleData.getPosName());
			// 角色职责
			figureElement.setAttribute("roleRes", roleData.getRoleRes());
		} else if (MapElemType.ModelFigure == figure.getMapElemType()) {// 泳池
			JecnModeFigureData modeFigureData = (JecnModeFigureData) figure;
			// 泳池x点位置
			figureElement.setAttribute("dividingX", String.valueOf(modeFigureData.getDividingX()));
		} else if (MapElemType.IconFigure == figure.getMapElemType()) { // 图标插入框
			// 图片ID
			Long linkId = figure.getDesignerFigureData().getLinkId();
			if (linkId != null) {
				figureElement.setAttribute("imgId", String.valueOf(linkId));
			}
		}

		// 判断是否为圆图形
		if (MapElemType.Oval == figure.getMapElemType()) {
			figureElement.setAttribute("ActivityShow", "");
		}
		// 判断是否为虚拟角色
		figureElement.setAttribute("LinkType", "");
		// 判断是否为角色、客户,是否设置主责岗位
		if (MapElemType.RoleFigure == figure.getMapElemType() || MapElemType.CustomFigure == figure.getMapElemType()
				|| MapElemType.RoleFigureAR == figure.getMapElemType()) {
			figureElement.setAttribute("LinkMainType", "");
		}
		// 判断是否为注释框
		if (MapElemType.CommentText == figure.getMapElemType()) {
			JecnCommentLineData commentData = (JecnCommentLineData) figure;
			if (commentData.getLineData() == null) {
				return;
			}
			// // 判断是否有注释框线条
			figureElement.setAttribute("getStartPosX", String.valueOf(commentData.getStartX()));
			figureElement.setAttribute("getStartPosY", String.valueOf(commentData.getStartY()));
			figureElement.setAttribute("getEndPosX", String.valueOf(commentData.getEndX()));
			figureElement.setAttribute("getEndPosY", String.valueOf(commentData.getEndY()));
			// /** 是否拖动 */
			// if (commentData.isDragged()) {
			// figureElement.setAttribute("commentLineDragged", "true");
			// } else {
			// figureElement.setAttribute("commentLineDragged", "false");
			// }
		}

		// 流程地图描述
		String mapDesc = "";
		if (DrawCommon.isInfoFigure(figure.getMapElemType())) {
			mapDesc = figure.getAvtivityShow();
		}
		figureElement.setAttribute("mapDesc", mapDesc);
		workflowInfo.appendChild(figureElement.getElement());
	}

	/**
	 * 生成线的数据
	 * 
	 * @param workflowInfo
	 * @param baseLineData
	 *            线数据
	 */
	public void getExportLine(Document doc, Element workflowInfo, JecnBaseLineData baseLineData) {
		// 横竖分割线
		if (baseLineData instanceof JecnVHLineData) {
			JecnVHLineData vHLineData = (JecnVHLineData) baseLineData;

			JecnElement figureElement = null;
			// 水平
			if (LineDirectionEnum.horizontal == vHLineData.getLineDirectionEnum()) {
				figureElement = new JecnElement(doc.createElement("ParallelLines"));
				// 老版中的标识
				figureElement.setAttribute("FigureType", "ParallelLines");
				// 老版用到的 结束点
				figureElement.setAttribute("PoLongx", String.valueOf(0));
				// 结束点Y的值
				double poLongy = vHLineData.getVhY();
				if (poLongy < 1) {
					poLongy = 1;
				}
				figureElement.setAttribute("PoLongy", String.valueOf(poLongy));
			} else if (LineDirectionEnum.vertical == vHLineData.getLineDirectionEnum()) {// 垂直
				figureElement = new JecnElement(doc.createElement("VerticalLines"));
				// 老版中的标识
				figureElement.setAttribute("FigureType", "VerticalLine");
				// 结束点X的值
				double poLongx = vHLineData.getVhX();
				if (poLongx < 1) {
					poLongx = 1;
				}
				figureElement.setAttribute("PoLongx", String.valueOf(poLongx));
				// 老版用到的 结束点
				figureElement.setAttribute("PoLongy", String.valueOf(0));
			}
			// 老版本中代表开始的X点
			figureElement.setAttribute("StartFigure", "0");
			// 分割线的层级
			figureElement.setAttribute("OrderIndex", String.valueOf(vHLineData.getZOrderIndex()));

			// 线条颜色
			if (vHLineData.getBodyColor() != null) {
				// 边框颜色
				figureElement.setAttribute("BodyColor", String.valueOf(vHLineData.getBodyColor().getRed() + ","
						+ vHLineData.getBodyColor().getGreen() + "," + vHLineData.getBodyColor().getBlue()));
			} else {
				figureElement.setAttribute("BodyColor", "");
			}

			// 边框样式
			figureElement.setAttribute("BodyLine", String.valueOf(vHLineData.getBodyStyle()));
			// 线条类型
			figureElement.setAttribute("LineSize", String.valueOf(vHLineData.getBodyWidth()));

			workflowInfo.appendChild(figureElement.getElement());
		} else if (baseLineData instanceof JecnManhattanLineData) { // 连接线
			JecnElement figureElement = null;
			JecnManhattanLineData manhattanLineData = (JecnManhattanLineData) baseLineData;
			if (baseLineData.getMapElemType() == MapElemType.CommonLine) {
				figureElement = new JecnElement(doc.createElement("CommonLines"));
				// 连接线的标识
				figureElement.setAttribute("FigureType", "CommonLine");
			} else {
				figureElement = new JecnElement(doc.createElement("manhattanLines"));
				// 连接线的标识
				figureElement.setAttribute("FigureType", "ManhattanLine");
			}
			if (figureElement == null) {
				return;
			}
			// 开始图形的ID
			figureElement.setAttribute("StartFigure", manhattanLineData.getStartFigureUUID());
			// 结束图形的ID
			figureElement.setAttribute("EndFigure", manhattanLineData.getEndFigureUUID());
			// 获取开始点的边界点
			String startLineType = String.valueOf(DrawCommon.getLineType(manhattanLineData.getStartEditPointType()));
			figureElement.setAttribute("PoLongx", startLineType);
			// 获取结束点的边界点
			String endLineType = String.valueOf(DrawCommon.getLineType(manhattanLineData.getEndEditPointType()));
			figureElement.setAttribute("PoLongy", endLineType);
			// 线的颜色
			figureElement.setAttribute("LineColor", String.valueOf(manhattanLineData.getBodyColor().getRed() + ","
					+ manhattanLineData.getBodyColor().getGreen() + "," + manhattanLineData.getBodyColor().getBlue()));
			// 线的层级
			figureElement.setAttribute("OrderIndex", String.valueOf(manhattanLineData.getZOrderIndex()));
			// 线条宽度
			figureElement.setAttribute("LineSize", String.valueOf(manhattanLineData.getBodyWidth()));
			if (manhattanLineData.getFigureText() == null) {
				// 线数据
				figureElement.setAttribute("LineText", String.valueOf(""));
			} else {
				// 线数据
				figureElement.setAttribute("LineText", String.valueOf(manhattanLineData.getFigureText()));
			}

			// 线段字体大小
			figureElement.setAttribute("FontSize", String.valueOf(manhattanLineData.getFontSize()));

			// 边框样式
			figureElement.setAttribute("BodyLine", String.valueOf(manhattanLineData.getBodyStyle()));

			// 字体颜色
			if (manhattanLineData.getFontColor() != null) {
				// 字体颜色
				figureElement.setAttribute("FontColor", String.valueOf(manhattanLineData.getFontColor().getRed() + ","
						+ manhattanLineData.getFontColor().getGreen() + ","
						+ manhattanLineData.getFontColor().getBlue()));
			}
			// 字体是否加粗0为正常，1为加粗，2为斜体
			figureElement.setAttribute("FontBody", String.valueOf(manhattanLineData.getFontStyle()));
			// 字体类型:默认为（宋体）
			figureElement.setAttribute("FontType", manhattanLineData.getFontName());

			// 连接线字段位置
			if (manhattanLineData.getTextPoint() != null) {
				Point textPoint = manhattanLineData.getTextPoint();
				figureElement.setAttribute("textPoint", textPoint.x + "," + textPoint.y);
			}

			// true:双箭头
			figureElement.setAttribute("twoArrowLine", String.valueOf(manhattanLineData.isTwoArrow()));
			// 添加小线段开始
			for (JecnLineSegmentData lineSegmentData : manhattanLineData.getLineSegmentDataList()) {
				JecnElement lineSegmentsElement = new JecnElement(doc.createElement("lineSegments"));
				// 线条开始点X
				lineSegmentsElement.setAttribute("StartPtX", String.valueOf(lineSegmentData.getLineSegmentData()
						.getStartXY().getX()));
				// 线条开始点Y
				lineSegmentsElement.setAttribute("StartPtY", String.valueOf(lineSegmentData.getLineSegmentData()
						.getStartXY().getY()));
				// 线条结束点X
				lineSegmentsElement.setAttribute("EndPtX", String.valueOf(lineSegmentData.getLineSegmentData()
						.getEndXY().getX()));
				// 线条结束点Y
				lineSegmentsElement.setAttribute("EndPtY", String.valueOf(lineSegmentData.getLineSegmentData()
						.getEndXY().getY()));
				// 添加小线段结束
				figureElement.appendChild(lineSegmentsElement.getElement());
			}
			// 添加整条连接线
			workflowInfo.appendChild(figureElement.getElement());
		} else if (baseLineData instanceof JecnBaseDivedingLineData) { // 斜线，横线，竖线
			JecnBaseDivedingLineData baseDivedingLineData = (JecnBaseDivedingLineData) baseLineData;
			JecnElement figureElement = new JecnElement(doc.createElement("dividLines"));
			// 线段的标识
			figureElement.setAttribute("FigureType", baseDivedingLineData.getMapElemType().toString());
			// 开始点的X
			int startX = baseDivedingLineData.getDiviLineCloneable().getStartXInt();
			if (startX < 1) {
				startX = 1;
			}
			figureElement.setAttribute("StartX", String.valueOf(startX));
			// 开始点的Y
			int startY = baseDivedingLineData.getDiviLineCloneable().getStartYInt();
			if (startY < 1) {
				startY = 1;
			}
			figureElement.setAttribute("StartY", String.valueOf(startY));
			// 结束点的X
			int endX = baseDivedingLineData.getDiviLineCloneable().getEndXInt();
			if (endX < 1) {
				endX = 1;
			}
			figureElement.setAttribute("EndX", String.valueOf(endX));
			// 结束点的Y
			int endY = baseDivedingLineData.getDiviLineCloneable().getEndYInt();
			if (endY < 1) {
				endY = 1;
			}
			figureElement.setAttribute("EndY", String.valueOf(endY));

			// 线条的颜色
			if (baseDivedingLineData.getBodyColor() != null) {
				figureElement.setAttribute("LineColor", String.valueOf(baseDivedingLineData.getBodyColor().getRed()
						+ "," + baseDivedingLineData.getBodyColor().getGreen() + ","
						+ baseDivedingLineData.getBodyColor().getBlue()));
			}
			// 线条类型
			figureElement.setAttribute("LineSize", String.valueOf(baseDivedingLineData.getBodyWidth()));

			// 边框样式
			figureElement.setAttribute("BodyLine", String.valueOf(baseDivedingLineData.getBodyStyle()));

			// 线的层级
			figureElement.setAttribute("OrderIndex", String.valueOf(baseDivedingLineData.getZOrderIndex()));

			workflowInfo.appendChild(figureElement.getElement());
		}
	}
}
