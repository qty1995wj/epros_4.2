package com.jecn.search.bean;

/**
 * 关键字搜索查询添加和返回结果数据对象
 * 
 * @author ZXH
 * 
 */
public class JecnKeyWordSearchBean {
	/** 搜索的文件ID */
	private Long relatedId;
	/** 所属对应文件的类型：1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险 */
	private int relatedType;
	/** 关联文件名称 */
	private String realtedName;

	private String updateDate;

	private String relatedTitle;

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public int getRelatedType() {
		return relatedType;
	}

	public void setRelatedType(int relatedType) {
		this.relatedType = relatedType;
	}

	public String getRealtedName() {
		return realtedName;
	}

	public void setRealtedName(String realtedName) {
		this.realtedName = realtedName;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getRelatedTitle() {
		// 1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险;
		switch (relatedType) {
		case 1:
			relatedTitle = "流程架构";
			break;
		case 2:
			relatedTitle = "流程";
			break;
		case 3:
			relatedTitle = "文件";
			break;
		case 4:
			relatedTitle = "制度";
			break;
		case 5:
			relatedTitle = "标准";
			break;
		case 6:
			relatedTitle = "风险";
			break;
		default:
			relatedTitle = "";
			break;
		}
		return relatedTitle;
	}

	public void setRelatedTitle(String relatedTitle) {
		this.relatedTitle = relatedTitle;
	}

}
