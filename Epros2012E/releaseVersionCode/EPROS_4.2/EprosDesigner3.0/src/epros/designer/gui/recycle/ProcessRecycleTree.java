package epros.designer.gui.recycle;

import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnRecycleMenu;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class ProcessRecycleTree extends RecycleTree {

	private static Logger log = Logger.getLogger(ProcessRecycleTree.class);

	@Override
	public JecnTreeListener getTreeExpansionListener(
			JecnHighEfficiencyTree jTree) {
		return new ProcessRecycleTreeListener(jTree);
	}


	@Override
	public JecnTreeModel getTreeModel() {
		List<JecnTreeBean> dataList = null;
		try {

			dataList = ConnectionPool.getProcessAction()
					.getRecycleJecnTreeBeanList(Long.valueOf(0),JecnConstants.projectId);
		} catch (Exception e) {
			log.error("ProcessRecycleTree getTreeModel is error", e);
		}
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.processRoot, JecnProperties.getValue("process"));
		JecnTreeCommon.addNLevelNodes(dataList, rootNode);
		return new JecnTreeModel(rootNode);
	}

	@Override
	protected JecnRecycleMenu getRecycleMenu() {
		return new ProcessRecycleMenu(this);
	}

}
