package com.jecn.epros.server.webBean;
/**
 * @author yxw 2012-12-5
 * @description：公共文件Bean
 */
public class PublicFileBean {
	/**主键*/
	private long id;
	/**名称*/
	private String name;
	/**编号*/
	private String number;
	/**类型 0是流程，1是流程地图，2是文件，3是标准文件，4是制度文件，5制度模板文件*/
	private int type;
	/**责任部门名称*/
	private String orgName;
	/**关联ID*/
	private long relationId;
	/**关联名称*/
	private String relationName;
	/**关联类型 1是文件、2是流程、3是流程地图*/
	private int relationType;
	/**密级*/
	private int secretLevel;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public long getRelationId() {
		return relationId;
	}
	public void setRelationId(long relationId) {
		this.relationId = relationId;
	}
	public String getRelationName() {
		return relationName;
	}
	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}
	public int getRelationType() {
		return relationType;
	}
	public void setRelationType(int relationType) {
		this.relationType = relationType;
	}
	public int getSecretLevel() {
		return secretLevel;
	}
	public void setSecretLevel(int secretLevel) {
		this.secretLevel = secretLevel;
	}

}
