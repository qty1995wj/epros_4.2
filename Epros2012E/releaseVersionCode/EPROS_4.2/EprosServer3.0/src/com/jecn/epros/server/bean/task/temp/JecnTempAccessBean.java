package com.jecn.epros.server.bean.task.temp;

import com.jecn.epros.server.bean.popedom.AccessId;

/**
 * 单一节点查阅权限
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Nov 20, 2012 时间：6:49:01 PM
 */
public class JecnTempAccessBean implements java.io.Serializable {
	/** 关联ID */
	private Long relateId;
	/** 0是流程，1是文件，2是标准，3是制度 */
	private int type;
	/** 查阅权限组织ID集合 ','分隔 */
	private String orgIds;
	/** 查阅权限岗位ID集合 ','分隔 */
	private String posIds;
	/** 是否保存 true:保存  true表示已发布*/
	boolean isSave = false;
	/**查阅权限岗位组ID集合 ','分隔*/
	private String posGroupIds;
	/**
	 * 权限集合
	 */
	private AccessId accId;

	
	
	public AccessId getAccId() {
		return accId;
	}

	public void setAccId(AccessId accId) {
		this.accId = accId;
	}

	public String getPosGroupIds() {
		return posGroupIds;
	}

	public void setPosGroupIds(String posGroupIds) {
		this.posGroupIds = posGroupIds;
	}

	public Long getRelateId() {
		return relateId;
	}

	public void setRelateId(Long relateId) {
		this.relateId = relateId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public String getPosIds() {
		return posIds;
	}

	public void setPosIds(String posIds) {
		this.posIds = posIds;
	}

	public boolean isSave() {
		return isSave;
	}

	public void setSave(boolean isSave) {
		this.isSave = isSave;
	}

}
