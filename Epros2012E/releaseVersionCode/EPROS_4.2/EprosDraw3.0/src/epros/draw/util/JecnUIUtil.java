package epros.draw.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.Border;

import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;

/**
 * 
 * UI使用常用工具类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUIUtil {
	/** 窗体标题名称颜色 */
	private static Color frameTtileNameColor = Color.BLACK;
	/** 窗体标题上半部背景颜色 */
	private static Color frameTitleTopColor = Color.WHITE;
	/** 窗体标题下半部颜色 */
	private static Color frameTitleColor = new Color(238, 238, 238);
	/** 窗体标题下半部局部渐变颜色 */
	private static Color frameTitleBottomColor = new Color(220, 220, 220);
	/** 窗体标题下部线颜色 */
	private static Color frameTitleBottomLineColor = new Color(72, 139, 200);
	/** 窗体边框颜色 */
	private static Color frameBorderColor = new Color(220, 220, 220);
	/** 对话框边框颜色 */
	private static Color dialogBorderColor = new Color(153, 166, 182);
	/** 对话框拖动背景颜色 */
	private static Color dialogDragColor = new Color(181, 210, 238);

	/** 流程元素库标题栏下半部分颜色 */
	private static Color boxTitleBottomColor = new Color(192, 203, 221);

	/** 默认背景颜色 */
	private static Color defaultBackgroundColor = new Color(245, 245, 245);

	/** 画图面板背景颜色 */
	private static Color workflowColor = Color.WHITE;
	/** 画图面板的网格线条颜色 */
	private static Color gridColor = new Color(235, 235, 235);
	/** 画图面板快速添加图形按钮的边框颜色 */
	private static Color aidShortCutBorderColor = new Color(255, 199, 6);
	/** 流程元素编辑框内容不正确时的背景颜色 */
	private static Color aidEditTextAreaBackColor = new Color(251, 226, 226);

	/** 阴影边框颜色 */
	private static Color shadowBodyColor = Color.WHITE;
	/** 画图面板标题栏的滑干颜色 */
	private static Color scaleColor = new Color(72, 139, 200);
	/** 编辑框的边框颜色 */
	private static Color editTextAreaBorderColor = Color.GRAY;
	/** 连接线开始点的颜色 */
	private static Color lineStartPointColor = Color.YELLOW;
	/** 连接线结束点的颜色 */
	private static Color lineEndPointColor = Color.RED;
	/** 文本不可编辑的颜色 */
	private static Color noEditColor = new Color(245, 244, 234);
	/** 相关文件 详情 */
	private static final Color topNoColor = new Color(248, 252, 255);
	/** 相关文件 详情 */
	private static final Color buttomNoColor = new Color(230, 240, 255);

	/** 工具栏边框 */
	private static Border tootBarBorder = BorderFactory.createLineBorder(new Color(205, 205, 205), 1);

	/** 整个画图面板大小 */
	private static Dimension eprosDrawSize = null;
	/** 工具栏面板大小 */
	private static Dimension tootBarSize = null;
	/** 工具栏标题按钮大小 */
	private static Dimension tootBarTitleBtnSize = null;
	/** 画图面板大小 */
	private static Dimension workflowSize = null;
	/** 流程元素内容编辑框默认大小 */
	private static Dimension editTextAreaSize = new Dimension(100, 60);

	/** 间距：(0,0,0,0) */
	private static Insets insets0 = new Insets(0, 0, 0, 0);
	/** 间距：(0,3,0,0) */
	private static Insets insets3 = new Insets(0, 3, 0, 0);
	/** 间距：(0,3,0,3) */
	private static Insets insets0303 = new Insets(0, 3, 0, 3);
	/** 间距：(0,5,0,5) */
	private static Insets insets0202 = new Insets(0, 2, 0, 2);
	/** 间距(0,0,5,5) */
	private static Insets insets0555 = new Insets(0, 5, 5, 5);

	/** 呈现和图像处理服务管理对象 */
	private static RenderingHints renderingHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
			RenderingHints.VALUE_ANTIALIAS_ON);
	/** 图形图元轮廓呈现属性对象：宽度为1其他默认 */
	private static BasicStroke basicStrokeOne = new BasicStroke(1);
	/** 画图面板的网格画笔宽度 */
	private static BasicStroke girdLineBasicStroke = new BasicStroke(0.5f);

	/** 流程元素库宽 */
	private static final int boxWidth = 150;
	/** 工具栏宽 */
	private static final int toolBarWidth = 1500;

	static {
		// 窗体宽
		int width = 800;
		// 窗体高
		int height = 600;
		// 按钮高
		int btnHeight = 30;
		// 工具栏高
		int toobarHeigth = 56;
		// 画图面板宽
		int workflowWidth = width - boxWidth;
		// 画图面板高
		int workflowHeigth = height - toobarHeigth;

		// 整个画图面板大小
		eprosDrawSize = new Dimension(width, height);
		// 工具栏面板大小
		tootBarSize = new Dimension(toolBarWidth, toobarHeigth);
		// 工具栏标题按钮大小
		tootBarTitleBtnSize = new Dimension(100, btnHeight);
		// 画图面板大小
		workflowSize = new Dimension(workflowWidth, workflowHeigth);
	}

	/**
	 * 
	 * 获取流程元素库宽
	 * 
	 * @return
	 */
	public static int getBoxBtnWidth() {
		return boxWidth;
	}

	/**
	 * 
	 * 根据渐变方向类型绘制渐变色
	 * 
	 * @param comp
	 *            JComponent 需要填充的组件
	 * @param g2d
	 *            Graphics2D 画笔
	 * @param fillColorChangType
	 *            FillColorChangType 渐变方向类型
	 * @param Color
	 *            color1 第一种颜色
	 * @param Color
	 *            color2 第二种颜色
	 */
	public static void fillCompByChangColor(JComponent comp, Graphics2D g2d, FillColorChangType fillColorChangType,
			Color color1, Color color2) {

		if (g2d == null || fillColorChangType == null || color1 == null || color2 == null) {
			return;
		}

		switch (fillColorChangType) {
		case topTilt:// 1：向上倾斜
			g2d.setPaint(new GradientPaint(0, 0, color1, comp.getWidth(), comp.getHeight(), color2));
			g2d.fillRect(0, 0, comp.getWidth(), comp.getHeight());
			break;
		case bottom:// 2：向下倾斜
			g2d.setPaint(new GradientPaint(0, 0, color2, comp.getWidth(), comp.getHeight(), color1));
			g2d.fillRect(0, 0, comp.getWidth(), comp.getHeight());
			break;
		case vertical:// 3：垂直
			g2d.setPaint(new GradientPaint(0, 0, color1, comp.getWidth(), 0, color2));
			g2d.fillRect(0, 0, comp.getWidth(), comp.getHeight());
			break;
		case horizontal:// 4：水平
			g2d.setPaint(new GradientPaint(0, 0, color1, 0, comp.getHeight(), color2));
			g2d.fillRect(0, 0, comp.getWidth(), comp.getHeight());
			break;
		default:
			break;
		}
	}

	public static BasicStroke getBasicStrokeOne() {
		return basicStrokeOne;
	}

	/**
	 * 
	 * 呈现和图像处理服务管理对象
	 * 
	 * @return
	 */
	public static RenderingHints getRenderingHints() {
		return renderingHints;

	}

	/**
	 * 
	 * 获取窗体标题名称颜色
	 * 
	 * @return Color 窗体标题名称颜色
	 */
	public static Color getFrameTtileNameColor() {
		return frameTtileNameColor;
	}

	/**
	 * 
	 * 获取窗体标题上半部背景颜色
	 * 
	 * @return Color 窗体标题上半部背景颜色
	 */
	public static Color getFrameTitleTopColor() {
		return frameTitleTopColor;
	}

	/**
	 * 
	 * 窗体标题颜色
	 * 
	 * @return
	 */
	public static Color getFrameTitleColor() {
		return frameTitleColor;
	}

	/**
	 * 
	 * 窗体标题下部线颜色
	 * 
	 * @return
	 */
	public static Color getFrameTitleBottomLineColor() {
		return frameTitleBottomLineColor;
	}

	/**
	 * 
	 * 窗体边框颜色
	 * 
	 * @return
	 */
	public static Color getFrameBorderColor() {
		return frameBorderColor;
	}

	/**
	 * 
	 * 对话框边框颜色
	 * 
	 * @return
	 */
	public static Color getDialogBorderColor() {
		return dialogBorderColor;
	}

	/**
	 * 
	 * 对话框拖动背景颜色
	 * 
	 * @return
	 */
	public static Color getDialogDragColor() {
		return dialogDragColor;
	}

	/**
	 * 
	 * 窗体标题下半部局部渐变颜色
	 * 
	 * @return Color
	 */
	public static Color getFrameTitleBottomColor() {
		return frameTitleBottomColor;
	}

	/**
	 * 
	 * 工具栏边框
	 * 
	 * @return Border
	 */
	public static Border getTootBarBorder() {
		return tootBarBorder;
	}

	/**
	 * 
	 * 整个画图面板大小
	 * 
	 * @return Dimension
	 */
	public static Dimension getEprosDrawSize() {
		return eprosDrawSize;
	}

	/**
	 * 
	 * 工具栏面板大小
	 * 
	 * @return Dimension
	 */
	public static Dimension getTootBarSize() {
		return tootBarSize;
	}

	/**
	 * 
	 * 工具栏标题按钮大小
	 * 
	 * @return Dimension
	 */
	public static Dimension getTootBarTitleBtnSize() {
		return tootBarTitleBtnSize;
	}

	/**
	 * 
	 * 画图面板大小
	 * 
	 * @return Dimension
	 */
	public static Dimension getWorkflowSize() {
		return workflowSize;
	}

	/**
	 * 
	 * 流程元素内容编辑框默认大小
	 * 
	 * @return Dimension
	 */
	public static Dimension getEditTextAreaSize() {
		return editTextAreaSize;
	}

	/**
	 * 
	 * 获取流程元素库标题栏下半部分颜色
	 * 
	 * @return Color 流程元素库标题栏下半部分颜色
	 */
	public static Color getBoxTitleBottomColor() {
		return boxTitleBottomColor;
	}

	/**
	 * 
	 * 获取默认背景颜色
	 * 
	 * @return Color 默认背景颜色
	 */
	public static Color getDefaultBackgroundColor() {
		return defaultBackgroundColor;
	}

	/**
	 * 
	 * 获取画图面板背景颜色
	 * 
	 * @return Color 画图面板背景颜色
	 */
	public static Color getWorkflowColor() {
		return workflowColor;
	}

	/**
	 * 
	 * 获取阴影边框颜色
	 * 
	 * @return
	 */
	public static Color getShadowBodyColor() {
		return shadowBodyColor;
	}

	public static Color getLineStartPointColor() {
		return lineStartPointColor;
	}

	public static Color getLineEndPointColor() {
		return lineEndPointColor;
	}

	public static Color getNoEditColor() {
		return noEditColor;
	}

	/**
	 * 
	 * 获取画图面板的网格线条颜色
	 * 
	 * @return Color
	 */
	public static Color getGridColor() {
		return gridColor;
	}

	/**
	 * 
	 * 获取画图面板快速添加图形按钮的边框颜色
	 * 
	 * @return Color 画图面板快速添加图形按钮的边框颜色
	 */
	public static Color getAidShortCutBorderColor() {
		return aidShortCutBorderColor;
	}

	/**
	 * 
	 * 获取流程元素编辑框内容不正确时的背景颜色
	 * 
	 * @return Color 流程元素编辑框内容不正确时的背景颜色
	 */
	public static Color getAidEditTextAreaBackColor() {
		return aidEditTextAreaBackColor;
	}

	/**
	 * 画图面板标题栏的滑干颜色
	 * 
	 * @return Color
	 */
	public static Color getScaleColor() {
		return scaleColor;
	}

	/**
	 * 
	 * 获取编辑框的边框颜色
	 * 
	 * @return
	 */
	public static Color getEditTextAreaBorderColor() {
		return editTextAreaBorderColor;
	}

	/**
	 * 
	 * 画图面板的网格画笔宽度
	 * 
	 * @return BasicStroke
	 */
	public static BasicStroke getGirdLineBasicStroke() {
		return girdLineBasicStroke;
	}

	/**
	 * 
	 * 获取间距(0,0,0,0)对象
	 * 
	 * @return Insets
	 */
	public static Insets getInsets0() {
		return insets0;
	}

	/**
	 * 
	 * 获取间距(0,3,0,0)对象
	 * 
	 * @return Insets
	 */
	public static Insets getInsets3() {
		return insets3;
	}

	/**
	 * 
	 * 获取间距(0,2,0,2)对象
	 * 
	 * @return Insets
	 */
	public static Insets getInsets0202() {
		return insets0202;
	}

	/**
	 * 
	 * 获取间距(0,3,0,3)对象
	 * 
	 * @return Insets
	 */
	public static Insets getInsets0303() {
		return insets0303;
	}

	public static Insets getInsets0555() {
		return insets0555;
	}

	/**
	 * 
	 * 改变给定组件的所有子组件的背景颜色
	 * 
	 * 
	 * @param com
	 *            Component 组件
	 */
	public static void setColorToSubComp(Component com) {
		com.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		if (com instanceof Container) {
			Container cont = (Container) com;
			for (Component sucom : cont.getComponents()) {
				setColorToSubComp(sucom);
			}
		}
	}

	/**
	 * 
	 * 绘制窗体底部线条
	 * 
	 * @param panel
	 *            JPanel
	 * @param g2d
	 *            Graphics2D
	 */
	public static void paintFrameBottomLine(JPanel panel, Graphics2D g2d) {
		if (g2d == null || panel == null) {
			return;
		}
		g2d.setColor(JecnUIUtil.getFrameTitleBottomLineColor());
		g2d.fillRect(0, panel.getHeight() - 2, panel.getWidth(), panel.getHeight());
	}

	public static int getToolBarWidth() {
		return toolBarWidth;
	}

	public static Color getTopNoColor() {
		return topNoColor;
	}

	public static Color getButtomNoColor() {
		return buttomNoColor;
	}
}
