package com.jecn.epros.service.keySearch.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.keySearch.ResourceSearchBean;
import com.jecn.epros.domain.keySearch.ResourceSearchResultBean;
import com.jecn.epros.mapper.KeySearchMapper;
import com.jecn.epros.service.keySearch.KeySearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class KeySearchServiceImpl implements KeySearchService {

    @Autowired
    private KeySearchMapper searchMapper;

    /**
     * 关键字搜索
     *
     * @param map
     * @return
     * @
     */
    @Override
    public ResourceSearchResultBean keyWordSearch(Map<String, Object> map) {
        ResourceSearchResultBean resultBean = getResourceSearchResult(map);
        if (resultBean.getResultNum() == 0) {
            return resultBean;
        }
        resultBean.setSearchResultLists(getSearchResultEntities(map));
        return resultBean;
    }

    private List<ResourceSearchBean> getSearchResultEntities(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "UPDATEDATE");
        return searchMapper.findKeySearchResult(map);
    }

    /**
     * 按类别分组
     *
     * @param map
     * @return
     * @
     */
    public ResourceSearchResultBean getResourceSearchResult(Map<String, Object> map) {
        List<Map<String, Object>> countMaps = searchMapper.findKeySearchCountResult(map);
        ResourceSearchResultBean searchResult = new ResourceSearchResultBean();
        if (countMaps == null) {
            return searchResult;
        }
        int totalCount = 0;
        int relatedType = 0;
        for (Map<String, Object> resultMap : countMaps) {
            totalCount = valueOfInt(resultMap.get("RELATEDID"));
            relatedType = valueOfInt(resultMap.get("RELATETYPE"));
            // 1：流程架构；2:流程；3：文件；4:制度；5：标准；6：风险;7:活动
            switch (relatedType) {
                case 1:
                    searchResult.setFlowMapCount(totalCount);
                    break;
                case 2:
                    searchResult.setFlowCount(totalCount);
                    break;
                case 3:
                    searchResult.setFileCount(totalCount);
                    break;
                case 4:
                    searchResult.setRuleCount(totalCount);
                    break;
                case 5:
                    searchResult.setStandardCount(totalCount);
                    break;
                case 6:
                    searchResult.setRiskCount(totalCount);
                    break;
                case 7:
                    searchResult.setFlowAndMapNum(totalCount);
                    break;
            }
        }
        return searchResult;
    }

    private int valueOfInt(Object obj) {
        return Integer.parseInt(obj.toString());
    }

}
