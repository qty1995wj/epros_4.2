package com.jecn.epros.server.bean.integration;

import java.io.Serializable;

import com.jecn.epros.server.util.JecnUtil;

/**
 * 活动与标准关系临时表
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-11 时间：下午03:41:40
 */
public class JecnActiveStandardBeanT implements Serializable {
	private String id;
	/** 活动ID */
	private Long activeId;
	/** 关联类型 关联类型 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求 */
	private int relaType;
	/** 所属条款ID */
	private Long clauseId;
	/** 标准ID */
	private Long standardId;
	/** 标准或条款名称 */
	private String relaName;

	private String strType;

	private String figureUUID;

	public String getRelaName() {
		return relaName;
	}

	public void setRelaName(String relaName) {
		this.relaName = relaName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getStandardId() {
		return standardId;
	}

	public void setStandardId(Long standardId) {
		this.standardId = standardId;
	}

	public int getRelaType() {
		return relaType;
	}

	public void setRelaType(int relaType) {
		this.relaType = relaType;
	}

	public Long getClauseId() {
		return clauseId;
	}

	public void setClauseId(Long clauseId) {
		this.clauseId = clauseId;
	}

	/**
	 * 根据标准类型返回对象的类型名称
	 * 
	 * @param relaType
	 * @return
	 */
	public String getStrType() {
		switch (this.relaType) {
		case 1:// 文件标准
			strType = JecnUtil.getValue("fileStandar");
			break;
		case 4:// 标准条款
			strType = JecnUtil.getValue("standarClause");
			break;
		case 5:// 条款要求
			strType = JecnUtil.getValue("clauseRequest");
			break;
		}
		return strType;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}
}
