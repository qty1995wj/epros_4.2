package com.jecn.epros.domain.process.inout;

import java.io.Serializable;
import java.util.List;

public class JecnFigureInoutBase implements Serializable {
    private String id;
    private Long flowId;
    private Long figureId;
    private Integer type;
    private String name;
    private String explain;
    private Long fileId;
    /**
     * 文件名称 不存数据库
     */
    private String fileName;

    /**
     * 样例
     */
    private List<JecnFigureInoutSampleBase> samples;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<JecnFigureInoutSampleBase> getSamples() {
        return samples;
    }

    public void setSamples(List<JecnFigureInoutSampleBase> samples) {
        this.samples = samples;
    }

}
