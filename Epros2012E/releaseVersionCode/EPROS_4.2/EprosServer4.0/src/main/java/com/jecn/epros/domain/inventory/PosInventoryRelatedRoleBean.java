package com.jecn.epros.domain.inventory;

import java.util.List;

public class PosInventoryRelatedRoleBean {
    private Long posId;
    private List<PosInventoryRoleBean> listPosInventoryRoleBean;

    public Long getPosId() {
        return posId;
    }

    public void setPosId(Long posId) {
        this.posId = posId;
    }

    public List<PosInventoryRoleBean> getListPosInventoryRoleBean() {
        return listPosInventoryRoleBean;
    }

    public void setListPosInventoryRoleBean(List<PosInventoryRoleBean> listPosInventoryRoleBean) {
        this.listPosInventoryRoleBean = listPosInventoryRoleBean;
    }
}
