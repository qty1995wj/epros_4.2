package com.jecn.epros.controller;

import com.jecn.epros.domain.*;
import com.jecn.epros.domain.home.JoinProcess;
import com.jecn.epros.domain.home.RoleRelatedFlowInfo;
import com.jecn.epros.domain.journal.*;
import com.jecn.epros.domain.org.SearchPosResultBean;
import com.jecn.epros.domain.process.FlowSearchResultBean;
import com.jecn.epros.domain.process.map.DeptAerialData;
import com.jecn.epros.domain.report.base.CharItem;
import com.jecn.epros.domain.report.base.LineChartOptions;
import com.jecn.epros.domain.risk.SearchRiskResultBean;
import com.jecn.epros.domain.rule.SearchRuleResultBean;
import com.jecn.epros.domain.system.Dictionary;
import com.jecn.epros.domain.task.MyTaskBean;
import com.jecn.epros.domain.task.TaskSearchTempBean;
import com.jecn.epros.domain.user.*;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.TreeNodeUtils.NodeType;
import com.jecn.epros.service.common.CommonService;
import com.jecn.epros.service.home.HomeService;
import com.jecn.epros.service.journal.JournalService;
import com.jecn.epros.service.process.ProcessService;
import com.jecn.epros.service.system.DictionaryService;
import com.jecn.epros.service.task.TaskService;
import com.jecn.epros.service.user.UserService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.DictionaryEnum;
import com.jecn.epros.util.Params;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(description = "用户信息", position = 7)
public class UserController {
    @Value("${jwt.header}")
    private String tokenHeader;
    @Autowired
    private UserService userService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private JournalService journalService;
    @Autowired
    private HomeService homeService;
    @Autowired
    private ProcessService processService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private DictionaryService dictionaryService;


    @ApiOperation(value = "用户-基本信息")
    @RequestMapping(value = "/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAuthenticatedUser() {
        return ResponseEntity.ok(AuthenticatedUserUtil.getPrincipal());
    }

    @ApiOperation(value = "用户-收藏")
    @RequestMapping(value = "/user/stars", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MyStars> getMyStars(@ApiParam(value = "搜索条件") @ModelAttribute SearchInfo searchInfo, @ApiParam(value = "分页信息") @ModelAttribute Paging paging) {
        MyStars starts = new MyStars();
        Map<String, Object> params = new Params("userId", AuthenticatedUserUtil.getPeopleId()).add(paging).add("search", searchInfo).toMap();
        int total = userService.getMyStarsNum(params);
        starts.setTotal(total);
        if (total > 0) {
            List<MyStar> myStars = userService.getMyStars(params);
            starts.setData(myStars);
        }
        return ResponseEntity.ok(starts);
    }

    @ApiOperation(value = "用户-排名靠前的收藏")
    @RequestMapping(value = "/user/stars/recommend", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MyStar>> getRecommendStars() {
        List<MyStar> myStars = userService
                .getRecommendStars(new Params("userId", AuthenticatedUserUtil.getPeopleId()).add(Paging.DEFAULT).toMap());
        return ResponseEntity.ok(myStars);
    }

    @ApiOperation(value = "用户-添加收藏")
    @RequestMapping(value = "/user/stars", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MyStar> addStar(@RequestBody MyStarAddForm myStar) {
        Params params = new Params("userId", AuthenticatedUserUtil.getPeopleId()).add("relatedId", myStar.getRelatedId())
                .add("type", MyStar.getStarType(myStar.getType())).add("name", myStar.getName());
        Map<String, Object> paramsMap = params.toMap();
        userService.addStar(paramsMap);
        MyStar star = userService.getStar(paramsMap);
        return ResponseEntity.ok(star);
    }

    @ApiOperation(value = "用户-取消收藏")
    @RequestMapping(value = "/user/stars/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> deleteStar(
            @ApiParam(value = "取消 Star", required = true) @PathVariable String id) {
        userService.deleteStar(new Params("id", id).toMap());
        return ResponseEntity.ok(null);
    }

    @ApiOperation(value = "用户-更新收藏")
    @RequestMapping(value = "/user/stars/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateStar(@ApiParam(value = "主键ID") @PathVariable String id) {
        userService.updateStar(new Params("id", id).toMap());
        return ResponseEntity.ok(null);
    }

    @ApiOperation(value = "用户-获取对应资源的Star数据")
    @RequestMapping(value = "/user/stars/{r_id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MyStar> getStar(@PathVariable("r_id") Long relatedId, @RequestParam NodeType type) {
        Map<String, Object> params = new Params("userId", AuthenticatedUserUtil.getPeopleId()).add("relatedId", relatedId).add("type", MyStar.getStarType(type)).toMap();
        MyStar myStar = userService.getStar(params);
        return ResponseEntity.ok(myStar);
    }

    @ApiOperation(value = "用户-任务")
    @RequestMapping(value = "/user/tasks", method = RequestMethod.GET)
    public ResponseEntity<List<MyTaskBean>> fetchMyTask() {
        Long loginPeopleId = AuthenticatedUserUtil.getPeopleId();
        List<MyTaskBean> tasks = taskService.fetchMyTasks(new TaskSearchTempBean(), loginPeopleId, null);
        return ResponseEntity.ok(tasks);
    }

    @ApiOperation(value = "用户-流程/制度/风险内控数量", notes = "获取与用户相关的流程、制度、风险/内控的数量")
    @RequestMapping(value = "/user/resources", method = RequestMethod.GET)
    public ResponseEntity<UserResourceStatistics> getMyResources() {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("username", AuthenticatedUserUtil.getUserName());
        paramMap.put("projectId", AuthenticatedUserUtil.getProjectId());
        UserResourceStatistics resourceStatistics = userService.getMyResources(paramMap);
        return ResponseEntity.ok(resourceStatistics);
    }

    @ApiOperation(value = "用户-流程", notes = "获取与用户相关的流程数据列表")
    @RequestMapping(value = "/user/processes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FlowSearchResultBean>> getMyProcesses(@ModelAttribute MySearchInfo searchInfo) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("username", AuthenticatedUserUtil.getUserName());
        paramMap.put("projectId", AuthenticatedUserUtil.getProjectId());
        paramMap.put("name", searchInfo.getName());
        paramMap.put("number", searchInfo.getNumber());
        List<FlowSearchResultBean> viewModel = userService.getMyProcesses(paramMap);
        return ResponseEntity.ok(viewModel);
    }

    @ApiOperation(value = "用户-制度", notes = "获取与用户相关的制度数据列表")
    @RequestMapping(value = "/user/rules", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SearchRuleResultBean>> getMyRules(@ModelAttribute MySearchInfo searchInfo) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("username", AuthenticatedUserUtil.getUserName());
        paramMap.put("projectId", AuthenticatedUserUtil.getProjectId());
        paramMap.put("name", searchInfo.getName());
        paramMap.put("number", searchInfo.getNumber());
        List<SearchRuleResultBean> processes = userService.getMyRules(paramMap);
        return ResponseEntity.ok(processes);
    }

    @ApiOperation(value = "用户-风险/内控", notes = "获取与用户相关的风险/内控数据列表")
    @RequestMapping(value = "/user/risks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SearchRiskResultBean>> getMyRisks(@ModelAttribute MySearchInfo searchInfo) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("username", AuthenticatedUserUtil.getUserName());
        paramMap.put("projectId", AuthenticatedUserUtil.getProjectId());
        paramMap.put("name", searchInfo.getName());
        paramMap.put("number", searchInfo.getNumber());
        List<SearchRiskResultBean> processes = userService.getMyRisks(paramMap);
        return ResponseEntity.ok(processes);
    }


    @ApiOperation(value = "用户-功能权限")
    @RequestMapping(value = "/user/authorities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNodeAccess>> getWebAccessByUserId() {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        List<TreeNodeAccess> list = userService.getWebAccessByPeopleId(paramMap);
        return ResponseEntity.ok(list);
    }

    @ApiOperation(value = "用户-更新功能权限/用户权限")
    @RequestMapping(value = "/users/{id}/authorities", method = RequestMethod.POST)
    public ResponseEntity<?> updateUserWebAccess(@PathVariable String id, @RequestBody List<String> authorities) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("peopleId", id);
        paramMap.put("menusList", authorities);
        userService.updateWebAccess(paramMap);
        return ResponseEntity.ok(null);
    }

    @ApiOperation(value = "用户-待查看的资源")
    @RequestMapping(value = "/user/notices", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FlowViewWebLogBean> notice(@ModelAttribute SearchWebLogBean searchBean, @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAccessAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("searchBean", searchBean);
        map.put("paging", paging);

        FlowViewWebLogBean viewBean = new FlowViewWebLogBean();
        int total = journalService.searchNoticeTotal(map);
        viewBean.setTotal(total);
        if (total > 0) {
            List<SearchResultWebLogBean> listSearchFlowResultBean = journalService.searchNotice(map);
            viewBean.setData(listSearchFlowResultBean);
        }
        return ResponseEntity.ok(viewBean);
    }

    @ApiOperation(value = "用户-权限菜单")
    @RequestMapping(value = "/user/view_permissions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PermissionsMenuInfo> getUserPermissionsMenu() {
        /**
         * 获取用户的浏览角色ID
         */
        List<String> roleId = AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getRowId();
        int languageType = AuthenticatedUserUtil.isLanguageType(); //获取 语言类型
        PermissionsMenuInfo menuInfo = userService.getPermissionsMenuById(roleId, languageType);
        return ResponseEntity.ok(menuInfo);
    }

    @ApiOperation(value = "我参与的流程")
    @RequestMapping(value = "/user/myProcesses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JoinProcess> fetchMyProcesses(String posId, Paging paging) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("projectId", AuthenticatedUserUtil.getProjectId());
        maps.put("posId", posId);
        maps.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        maps.put("paging", paging);
        JoinProcess bean = new JoinProcess();
        List<RoleRelatedFlowInfo> findMyJoinProcess = homeService.findMyJoinProcess(maps);
        bean.setTotal(homeService.findMyJoinProcessCount(maps));
        bean.setData(findMyJoinProcess);
        return ResponseEntity.ok(bean);
    }

    @ApiOperation(value = "我参与的流程-获取人员对应岗位")
    @RequestMapping(value = "/user/myFetchMyFlow", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<JoinProcess>> fetchMyFlow() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("projectId", AuthenticatedUserUtil.getProjectId());
        maps.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        List<JoinProcess> findMyJoinProcess = homeService.getPostByUserId(maps);
        return ResponseEntity.ok(findMyJoinProcess);
    }


    @ApiOperation(value = "用户-访问最多的5条流程")
    @RequestMapping(value = "/user/visit_top_process", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LineChartOptions> getVisitTopProcess() {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("topN", 5);
        LineChartOptions graph = userService.getVisitTopProcess(paramMap);
        return ResponseEntity.ok(graph);
    }

    @ApiOperation(value = "用户-合理化建议最多的10条流程,")
    @RequestMapping(value = "/user/propose_top_process", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CharItem>> getProposeTopProcess() {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("topN", 10);
        List<CharItem> datas = userService.getProposeTopProcess(paramMap);
        return ResponseEntity.ok(datas);
    }

    @ApiOperation(value = "用户-合理化建议最多的10条制度")
    @RequestMapping(value = "/user/propose_top_rule", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CharItem>> getProposeTopRule() {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("topN", 10);
        List<CharItem> datas = userService.getProposeTopRule(paramMap);
        return ResponseEntity.ok(datas);
    }

    @ApiOperation(value = "用户-访问最多的10条流程柱状图，包含当月以及之前总共12个月")
    @RequestMapping(value = "/user/visit_top_process_histogram", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getVisitTopProcessHistogram() {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("topN", 10);
        List<CharItem> datas = userService.getVisitTopProcessHistogram(paramMap);
        return ResponseEntity.ok(datas);
    }

    @ApiOperation(value = "用户-访问最多的10条制度柱状图，包含当月以及之前总共12个月")
    @RequestMapping(value = "/user/visit_top_rule_histogram", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getVisitTopRuleHistogram() {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("topN", 10);
        List<CharItem> datas = userService.getVisitTopRuleHistogram(paramMap);
        return ResponseEntity.ok(datas);
    }

    @ApiOperation(value = "用户基本信息")
    @RequestMapping(value = "/user/info/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUserInfo(@PathVariable("id") Long peopleId) {
        if (peopleId == null) {
            throw new IllegalArgumentException("用户基本信息中的人员id为空");
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("peopleId", peopleId);
        JecnUser user = userService.getUser(paramMap);
        user.setName(IfytekCommon.getIflytekUserName(user.getLoginName(), 0));
        List<SearchPosResultBean> orgAndPoss = userService.listUserOrgAndPosInfos(paramMap);
        if (user != null) {
            user.setPassword(null);
        }
        UserDetail detail = new UserDetail();
        detail.setUser(user);
        detail.setOrgAndPoss(orgAndPoss);
        return ResponseEntity.ok(detail);
    }

    @ApiOperation(value = "登陆用户信息")
    @RequestMapping(value = "/user/login_info", method = RequestMethod.GET)
    public ResponseEntity<?> getLoginUserInfo() {
        Long peopleId = AuthenticatedUserUtil.getPeopleId();
//        AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().setLanguage(1);
        if (peopleId == null) {
            throw new IllegalArgumentException("UserController getLoginUserInfo is error");
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("peopleId", peopleId);
        JecnUser user = userService.getUser(paramMap);
        if (user != null) {
            if (ConfigUtils.isIflytekLogin()) { //科大讯飞重新获取 真实姓名
                user.setName(IfytekCommon.getIflytekUserName(user.getLoginName(), 0));
            }
            user.setPassword(null);
        }
        return ResponseEntity.ok(user);
    }

    @ApiOperation(value = "修改密码")
    @RequestMapping(value = "/user/alter_password", method = RequestMethod.POST)
    public ResponseEntity<?> alterPassword(@RequestBody Password password) {
        Long peopleId = AuthenticatedUserUtil.getPeopleId();
        if (peopleId == null) {
            throw new IllegalArgumentException("UserController alterPassword is error");
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("peopleId", peopleId);
        paramMap.put("oldPwd", password.getOldPwd());
        paramMap.put("newPwd", password.getNewPwd());
        ViewResultWithMsg result = userService.alterUserPwd(paramMap);
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "我负责的流程")
    @RequestMapping(value = "/user/MyResponsibilityProcess", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MyResponsibilityProcess>> findMyResponsibilityProcess() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("peopleId", AuthenticatedUserUtil.getPeopleId());   //获取人员ID
        maps.put("paging", Paging.DEFAULT);
        List<MyResponsibilityProcess> myResponsibilityProcess = userService.findMyResponsibilityProcess(maps);
        for (MyResponsibilityProcess myProcess : myResponsibilityProcess) {
            maps.put("projectId", AuthenticatedUserUtil.getProjectId());
            maps.put("id", myProcess.getFlowId());
            maps.put("isPub", true);
            maps.put("historyId", 0L);
            myProcess.setRelatedFiles(processService.fetchProcessesSupportResources(maps));
        }
        return ResponseEntity.ok(myResponsibilityProcess);
    }

    @ApiOperation(value = "我负责的制度")
    @RequestMapping(value = "/user/MyResponsibilityRule", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MyResponsibilityRule>> findMyResponsibilityRule() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("peopleId", AuthenticatedUserUtil.getPeopleId());   //获取人员ID
        maps.put("paging", Paging.DEFAULT);
        List<MyResponsibilityRule> myResponsibilityRule = userService.findMyResponsibilityRule(maps);
        for (MyResponsibilityRule myRule : myResponsibilityRule) {
            maps.put("id", myRule.getId());   //获取人员ID
            maps.put("type", 2);
            maps.put("historyId", 0L);
            maps.put("isPub", "");
            myRule.setRelatedFiles(commonService.findRelatedFiles(maps));
        }
        return ResponseEntity.ok(myResponsibilityRule);
    }


    @ApiOperation(value = "部门鸟瞰图")
    @RequestMapping(value = "/user/deptBirdviewMapsByPeople", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DeptAerialData>> findDeptAerialMapsByPeople() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("peopleId", AuthenticatedUserUtil.getPeopleId());   //获取人员ID
        maps.put("paging", Paging.DEFAULT);
        return ResponseEntity.ok(userService.findDeptAerialMapsByPeople(maps));
    }

    @ApiOperation(value = "首页常用文件")
    @RequestMapping(value = "/user/normalFile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SPDictionaryPageBean> getNormalFile(@ModelAttribute Paging paging) {
        List<Dictionary> list = dictionaryService.getDictionaryByName(DictionaryEnum.NORMAL_FILE);
        int begin = (paging.getPageNum() - 1) * paging.getPageSize() * 2;
        int end = begin + paging.getPageSize() * 2;
        List<Map<Dictionary,Dictionary>> data = new ArrayList<>();
//        for (int i = begin; i < end && i < list.size()/2; i++) {
//            Map m = new HashMap();
//            m.put("a",list.get(i));
//            m.put("b",list.get(list.size()-i-1));
//            data.add(m);
//        }

        int i = begin;
        while(i<list.size() && i < end){
            Map m = new HashMap();
            if(null != list.get(i)){
                m.put("a",list.get(i));
            }
            if(i+1<list.size()){
                m.put("b",list.get(i+1));
            }else {
                data.add(m);
                break;
            }

            data.add(m);
            i = i+2;
        }


        SPDictionaryPageBean out = new SPDictionaryPageBean();
        out.setData(data);
        out.setTotal(list.size());
        return ResponseEntity.ok(out);

    }
}
