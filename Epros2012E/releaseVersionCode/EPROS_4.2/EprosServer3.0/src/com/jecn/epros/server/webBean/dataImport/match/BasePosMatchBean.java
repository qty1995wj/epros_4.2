package com.jecn.epros.server.webBean.dataImport.match;

/**
 * 基准岗位
 * 
 * @author Administrator
 * 
 */
public class BasePosMatchBean {
	private String basePosNum;
	private String basePosName;

	public String getBasePosNum() {
		return basePosNum;
	}

	public void setBasePosNum(String basePosNum) {
		this.basePosNum = basePosNum;
	}

	public String getBasePosName() {
		return basePosName;
	}

	public void setBasePosName(String basePosName) {
		this.basePosName = basePosName;
	}
}
