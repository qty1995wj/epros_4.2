package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.bottom.JecnOKJButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextField;
import epros.designer.gui.system.config.ui.comp.JecnDocumentListener;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 文件基本信息面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnBasicInfoFilePanel extends JecnAbstractPropertyBasePanel {

	/** 其他项面板 */
	private JecnPanel otherPanel = null;

	/** 文件自动发布版本号：标签 */
	private JLabel autoPubVersionLabel = null;
	/** 文件自动发布版本号：输入框 */
	private JecnConfigTextField autoPubVersionTextField = null;

	public JecnBasicInfoFilePanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		iniComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 其他项面板
		otherPanel = new JecnPanel();

		// 文件自动发布版本号：标签
		autoPubVersionLabel = new JLabel(JecnProperties
				.getValue("basicFileAutoPubVersion"));
		// 文件自动发布版本号：输入框
		autoPubVersionTextField = new JecnConfigTextField();

		// 其他项面板
		otherPanel.setOpaque(false);
		otherPanel.setBorder(null);

		// 事件
		autoPubVersionTextField.getDocument().addDocumentListener(
				new JecnDocumentListener(this, autoPubVersionTextField, dialog
						.getOkCancelJButtonPanel().getInfoLabel()));

	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		this.getLayout();
		this.add(otherPanel, BorderLayout.CENTER);
		Insets insets = new Insets(5, 5, 5, 5);
		// 文件自动发布版本号：标签
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		otherPanel.add(autoPubVersionLabel, c);
		// 文件自动发布版本号：输入框
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		otherPanel.add(autoPubVersionTextField, c);
		// 空闲区域
		c = new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, insets,
				0, 0);
		otherPanel.add(new JLabel(), c);
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null
				|| !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean
				.getOtherItemList()) {
			// 唯一标识
			String mark = itemBean.getMark();
			if (ConfigItemPartMapMark.basicFileAutoPubVersion.toString()
					.equals(mark)) {// 文件自动发布版本号
				autoPubVersionTextField.setItemBean(itemBean);
				autoPubVersionTextField
						.setText((itemBean.getValue() == null) ? "" : itemBean
								.getValue());
			}
		}
	}

	/**
	 * 
	 * 
	 * @param boolean
	 *            校验成功：true ；失败：false
	 * 
	 */
	@Override
	public boolean check() {
		// 名称不能超过122个字符或61个汉字
		String info = JecnUserCheckUtil.checkNameLength(autoPubVersionTextField
				.getText());
		// 存在错误标识：true，无错：false
		boolean exsitsError = setErrorInfo(dialog.getOkCancelJButtonPanel()
				.getInfoLabel(), info);

		JecnOKJButton btn = dialog.getOkCancelJButtonPanel().getOkJButton();
		if (exsitsError) {// 验证失败
			if (btn.isEnabled()) {
				btn.setEnabled(false);
			}
			return false;
		}

		if (!btn.isEnabled()) {// 验证成功
			btn.setEnabled(true);
		}
		return true;
	}
}
