package com.jecn.epros.service;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.checkout.CheckoutResult;
import com.jecn.epros.bean.checkout.CheckoutRoleResult;

import java.util.List;
import java.util.Map;

/**
 * Created by user on 2017/4/11.
 */
public interface IProcessRPCService {

    String excelImporteFlowKpiImage(Map<String, Object> map);

    ByteFile exportKpi(Map<String, Object> map);

    /**
     * 下载流程有效期维护信息
     *
     * @param orgIds
     * @param mapIds
     * @param projectId
     * @return
     */
    ByteFile downloadExpiryMaintenance(String orgIds, String mapIds, Long projectId, int type);

    /**
     * 下载流程有效期维护错误数据
     *
     * @return
     */
    ByteFileResult downloadExpiryMaintenanceErrorData(Long peopleId, int type);

    /**
     * 导入流程有效期维护信息
     *
     * @param file
     * @return
     */
    MessageResult importExpiryMaintenance(ByteFile file, Long peopleId, int type);

    public ByteFile downLoadCheckout(Map<String, Object> param);

    public ByteFile downLoadCheckoutRole(Map<String, Object> param);

    public List<CheckoutResult> getCheckout(Map<String, Object> param);

    public List<CheckoutRoleResult> getCheckoutRole(Map<String, Object> param);

}
