package com.jecn.epros.server.webBean.integration;

import java.io.Serializable;

/**
 * 风险列表显示字段
 * @author Administrator
 *
 */
public class RiskWebBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 样例主键ID */
	private int modeId;
	private long id;
	/** 风险点编号 */
	private String riskCode;
	/** 风险点描述 */
	private String name;
	/** 风险等级 */
	private int grade;
	/** 风险等级 */
	private String gradeStr;
	
	public int getModeId() {
		return modeId;
	}
	public void setModeId(int modeId) {
		this.modeId = modeId;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getRiskCode() {
		return riskCode;
	}
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getGradeStr() {
		return gradeStr;
	}
	public void setGradeStr(String gradeStr) {
		this.gradeStr = gradeStr;
	}

}
