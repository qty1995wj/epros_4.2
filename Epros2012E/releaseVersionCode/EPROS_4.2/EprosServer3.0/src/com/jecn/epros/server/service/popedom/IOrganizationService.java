package com.jecn.epros.server.service.popedom;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.server.bean.popedom.AccessId;
import com.jecn.epros.server.bean.popedom.DownloadPopedomBean;
import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnOpenOrgBean;
import com.jecn.epros.server.bean.popedom.JecnPositionFigInfo;
import com.jecn.epros.server.bean.popedom.PositonInfo;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.system.JecnMaintainNode;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.popedom.JobDescriptionBean;
import com.jecn.epros.server.webBean.popedom.OrgListBean;

public interface IOrganizationService extends IBaseService<JecnFlowOrg, Long> {
	/**
	 * @author yxw 2012-5-22
	 * @description: 树加载 根据组织ID加载子节点
	 * @param pId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildOrgs(Long pId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:树加载 所有的组织
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllOrgs(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:树加载 根据组织ID加载子节点，包括组织与岗位
	 * @param id
	 *            组织ID
	 * @param projectId
	 *            项目ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildOrgsAndPositon(Long id, Long projectId, boolean isLeaf) throws Exception;

	/***
	 * @author xiaobo
	 * @des：树加载，包括岗位组和岗位
	 * @param id
	 *            项目ID
	 * @return
	 * @throws Exception
	 * */
	public List<JecnTreeBean> getGroups(Long orgId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:组织树右键增加组织
	 * @param name
	 * @param pId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public Long addOrg(String name, Long pId, String orgNumber, Long projectId, int sortId, String orgNum)
			throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:组织树增加岗位
	 * @param name
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	public Long addPosition(JecnFlowOrgImage jecnFlowOrgImage) throws Exception;

	/**
	 * @author yxw 2012-6-8
	 * @description:岗位属性更新
	 * @param positionInfoBean
	 * @param upPositionIds
	 * @param lowerPositionIds
	 * @param innerRuleIds
	 * @param outRuleIds
	 * @throws Exception
	 */
	public void updatePosition(JecnPositionFigInfo positionInfoBean, String upPositionIds, String lowerPositionIds,
			String innerRuleIds, String outRuleIds, Long posId, String posName, String posNum) throws Exception;

	/**
	 * @author yxw 2012-6-8
	 * @description:更新组织职责
	 * @param orgId
	 * @param duty
	 * @throws Exception
	 */
	public void updateOrgDuty(Long orgId, String duty, String orgNum) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:根据岗位ID获取岗位详情
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public PositonInfo getPositionInfo(Long id) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:组织重命名
	 * @param id
	 * @param newName
	 * @throws Exception
	 */
	public void reNameOrg(Long id, String newName) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:岗位重命名
	 * @param id
	 * @param newName
	 * @throws Exception
	 */
	public void reNamePosition(Long id, String newName) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点排序
	 * @param list
	 * @param pId
	 * @param projectId
	 * @throws Exception
	 */
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:组织节点移动
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodesOrg(List<Long> listIds, Long pId, String orgNumber, Long updatePersonId,
			TreeNodeType moveNodeType) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:岗位节点移动
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodesPos(List<Long> listIds, Long pId, String posNumber, Long updatePersonId,
			TreeNodeType moveNodeType) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:删除组织
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteOrgs(List<Long> listIds, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:删除岗位
	 * @param listIds
	 * @throws Exception
	 */
	public void deletePosition(List<Long> listIds) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:根据名称搜索组织
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchByName(String name, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-20
	 * @description:根据名称搜索岗位
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchPositionByName(String name, Long projectId) throws Exception;
	
	/**
	 * @author yxw 2012-6-20
	 * @description:根据名称(岗位，部门，人员)搜索岗位
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchPositionByName(Map<String,Object> nameMap, Long projectId) throws Exception;

	/**
	 * @author xiaobo 2014-05-27
	 * @description:根据名称搜索岗位组
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchGroupByName(String name, Long projectId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：组织搜索
	 * @param orgId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodesOrg(Long orgId, Long projectId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：岗位搜索
	 * @param orgId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodesPos(Long posId, Long projectId) throws Exception;

	/**
	 * 
	 * @author zhangchen Aug 6, 2012
	 * @description： 回收站-获得项目下删除的组织
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getDelsOrg(Long projectId) throws Exception;

	/**
	 * 
	 * @author zhangchen Aug 6, 2012
	 * @description：回收站-恢复删除
	 * @param id
	 * @throws Exception
	 */
	public void updateRecoverDelOrg(List<Long> ids) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：回收站--删除组织
	 * @param setIds
	 * @throws Exception
	 */
	public Set<Long> deleteIdsOrg(List<Long> ListIds, Long updatePeopleId) throws Exception;

	/**
	 * @author zhangchen Aug 7, 2012
	 * @description：获得父类节点的信息（包括子节点）（查看删除的路径）
	 * @param projectId
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getParentsContainSelf(Long projectId, Long id) throws Exception;

	/**
	 * @author yxw 2012-8-14
	 * @description: 判断组织图能否打开
	 * @param userId
	 *            登录用户ID
	 * @param id
	 *            组织ID
	 * @return true 可以打开 false 不可打开
	 * @throws Exception
	 */
	public boolean updateOrgCanOpen(Long userId, Long id) throws Exception;

	/**
	 * @author yxw 2012-8-14
	 * @description:组织图解除编辑
	 * @param id
	 * @throws Exception
	 */
	public void moveEdit(Long id) throws Exception;

	/**
	 * @author yxw 2012-8-8
	 * @description:打开组织图
	 * @return
	 * @throws Exception
	 */
	public JecnOpenOrgBean openOrgMap(long orgId) throws Exception;

	/**
	 * @author yxw 2012-8-8
	 * @description:组织图保存
	 * @param jecnFlowOrg
	 * @param saveList
	 * @param updateList
	 * @param deleteListLine
	 * @param deleteList
	 * @throws Exception
	 */
	public void saveOrgMap(JecnFlowOrg jecnFlowOrg, List<JecnFlowOrgImage> saveList, List<JecnFlowOrgImage> updateList,
			List<Long> deleteListLine, List<Long> deleteList) throws Exception;

	/**
	 * @description:根据组织父IDPID获取组织详情
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowOrg> getOrgInfos(Long pId) throws Exception;

	/**
	 * @author yxw 2012-12-25
	 * @description:获取岗位说明书
	 * @param posId岗位ID
	 * @return
	 * @throws Exception
	 */
	public JobDescriptionBean getJobDescription(Long posId) throws Exception;

	/**
	 * @author yxw 2012-12-26
	 * @description:根据条件查询组织总数
	 * @param name
	 *            查询条件"名称"
	 * @return
	 * @throws Exception
	 */
	public int getTotalSearchOrg(String name, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-26
	 * @description:根据部门名称搜索匹配部门
	 * @param name
	 *            名称
	 * @param start
	 *            开始行数
	 * @param limit
	 *            每页显示数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchOrg(String name, int start, int limit, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-26
	 * @description:根据条件查询岗位总数
	 * @param name
	 *            查询条件"名称"
	 * @return
	 * @throws Exception
	 */
	public int getTotalSearchPos(String name, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-12-26
	 * @description:根据部门名称搜索匹配部门
	 * @param name
	 *            名称
	 * @param start
	 *            开始行数
	 * @param limit
	 *            每页显示数
	 * @param projectId
	 *            项目id
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchPos(String name, int start, int limit, Long projectId) throws Exception;

	/**
	 * @author yxw 2013-1-15
	 * @description:根据ids查询岗位的详细信息
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPositionsByIds(String ids) throws Exception;

	/**
	 * 查询组织处在的级别
	 * 
	 * @author fuzhh 2013-12-5
	 * @param orgId
	 * @return
	 * @throws Exception
	 */
	public int findOrgDepth(Long orgId) throws Exception;

	/**
	 * 角色清单
	 * 
	 * @author fuzhh 2013-12-3
	 * @return
	 * @throws Exception
	 */
	public OrgListBean findOrgList(Long orgId, Long projectId, int depth, boolean isAll) throws Exception;

	/***
	 * 验证岗位编号全表唯一
	 * 
	 * @param posNum
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public boolean validateAddUpdatePosNum(String posNum, Long id) throws Exception;

	/***
	 * 验证组织编号全表唯一
	 * 
	 * @param posNum
	 * @param id
	 * @return
	 * @throws Exception
	 */

	public boolean validateAddUpdateOrgNum(String orgNum, Long id) throws Exception;

	/**
	 * 获取选中的组织下的组织
	 * 
	 * @param projectId
	 *            项目id
	 * @param pid
	 *            组织id
	 * @return
	 */
	public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long projectId, Long pid) throws Exception;

	/**
	 * 组织回收站：恢复当前选中的节点的节点
	 * 
	 * @param recycleNodesIdList
	 */
	public void recoverCurOrgs(List<Long> recycleNodesIdList) throws Exception;

	/**
	 * 组织回收站：恢复当前选中的节点的节点以及子节点
	 * 
	 * @param recycleNodesIdList
	 *            要恢复的节点的集合
	 * @param projectId
	 *            项目id
	 */
	public void recoverCurAndChildOrgs(List<Long> recycleNodesIdList, Long projectId) throws Exception;

	/**
	 * 组织回收站：根据搜索的名称获得它的节点以及父节点
	 * 
	 * @param fileId
	 * @param projectId
	 * @return
	 */
	public List<JecnTreeBean> getRecyleSelectedNodes(Long id, Long projectId) throws Exception;

	/**
	 * 组织回收站：搜索
	 * 
	 * @param name
	 *            搜索的名称
	 * @param projectId
	 *            项目id
	 * @return
	 */
	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId);

	/**
	 * 获得岗位的JECNTREEBEAN
	 * 
	 * @param posId
	 * @return
	 * @throws Exception
	 */
	public JecnTreeBean getJecnTreeBeanByposId(Long posId) throws Exception;

	public JecnTreeBean getCurrentOrg(Long orgId, Long projectId) throws Exception;

	public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId, Long startId) throws Exception;

	public JecnTreeBean getCurrentOrgContainDel(Long orgId, Long projectId) throws Exception;

	public List<JecnTreeBean> searchPositionByName(String name, Long projectId, Long startId) throws Exception;

	public DownloadPopedomBean getFlowFileDownloadPermissions(Long id) throws Exception;

	public void saveOrUpdateFlowFileDownloadPermissions(Long flowId, String posIds) throws Exception;

	List<JecnTreeBean> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId) throws Exception;

	List<JecnTreeBean> getRoleAuthChildPersonOrgs(Long pId, Long projectId, Long peopleId) throws Exception;

	List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception;

	ByteFile getJobDescriptionByteFile(Long posId) throws Exception;

	public List<JecnDictionary> getDictionarys(DictionaryEnum name)throws Exception;
	
	public List<JecnDictionary> getDictionarys()throws Exception;

	public AccessId getPepoleOrgAndPosAndPosPosGroup(long userId,Long projectId);

	public List<JecnMaintainNode> getLangues();

}
