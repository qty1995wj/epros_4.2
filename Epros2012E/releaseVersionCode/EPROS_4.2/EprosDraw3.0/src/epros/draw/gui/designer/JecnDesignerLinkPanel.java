package epros.draw.gui.designer;

import javax.swing.JPanel;

import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 
 * 
 * 设计器专用：连接图形等的容器
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDesignerLinkPanel extends JPanel {
	/** 图形 */
	private JecnBaseFigurePanel figure = null;

	public JecnDesignerLinkPanel(JecnBaseFigurePanel figure) {
		this.figure = figure;
		initComponent();
	}

	private void initComponent() {
		this.setOpaque(false);
		this.setBorder(null);
	}
}
