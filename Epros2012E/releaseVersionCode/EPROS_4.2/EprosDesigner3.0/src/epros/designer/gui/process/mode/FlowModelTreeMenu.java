package epros.designer.gui.process.mode;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/*******************************************************************************
 * 流程模板树节点
 * 
 * @author 2012-06-26
 * 
 */

public class FlowModelTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(FlowModelTreeMenu.class);
	/** 新建流程地图模板 */
	private JMenuItem newFlowMapModelItem = new JMenuItem(JecnProperties.getValue("newFlowMapMode"), new ImageIcon(
			"images/menuImage/newFlowMapMode.gif"));
	// /** 编辑流程地图模板 */
	// private JMenuItem editFlowMapModelItem = new JMenuItem("编辑流程地图模板", new
	// ImageIcon(
	// "images/menuImage/newFlowMap.gif"));
	/** 新建流程图模板 */
	private JMenuItem newFlowModelItem = new JMenuItem(JecnProperties.getValue("newFlowMode"), new ImageIcon(
			"images/menuImage/newFlowMode.gif"));
	// /** 编辑流程图模板 */
	// private JMenuItem editFlowModelItem = new JMenuItem("编辑流程图模板",
	// new ImageIcon("images/menuImage/newFlow.gif"));

	/** 删除 */
	private JMenuItem deleteModelItem = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon(
			"images/menuImage/delete.gif"));

	/** 搜索 */
	// private JMenuItem searchItem = new JMenuItem(JecnProperties
	// .getValue("search"), new ImageIcon("images/menuImage/search.gif"));
	/** 节点排序 */
	private JMenuItem nodeSortItem = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon(
			"images/menuImage/nodeSort.gif"));
	/** 节点移动 */
	private JMenuItem nodeMoveItem = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon(
			"images/menuImage/nodeMove.gif"));
	/** 刷新 */
	private JMenuItem refreshItem = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon(
			"images/menuImage/refresh.gif"));
	/** 重命名 */
	private JMenuItem renameItem = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon(
			"images/menuImage/rename.gif"));

	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> listNode = null;

	private JecnTree jTree = null;

	public FlowModelTreeMenu(JecnTree jTree, List<JecnTreeNode> listNode, int selectMutil) {
		this.jTree = jTree;
		this.listNode = listNode;
		if (0 == selectMutil) {
			selectNode = listNode.get(0);
		}
		if (listNode.size() == 1) {
			// 单选设置menu
			this.singleSelect();
		} else {
			// 多选设置menu
			this.mutilSelect();
		}
		this.add(newFlowMapModelItem);
		this.add(newFlowModelItem);
		// this.add(searchItem);
		this.add(refreshItem);
		this.add(renameItem);
		this.add(nodeSortItem);
		this.add(nodeMoveItem);
		this.add(deleteModelItem);
	}

	/**
	 * 初始化
	 */
	private void init() {
		// 新建流程地图模板
		newFlowMapModelItem.setEnabled(false);
		// 新建流程模板
		newFlowModelItem.setEnabled(false);
		// 删除
		deleteModelItem.setEnabled(false);
		// 搜索
		// searchItem.setEnabled(true);
		// 重命名
		renameItem.setEnabled(false);
		// 节点排序
		nodeSortItem.setEnabled(false);
		// 移动
		nodeMoveItem.setEnabled(false);
		// 刷新
		refreshItem.setEnabled(false);

		newFlowMapModelItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newFlowMapModelItemPerformed();
			}
		});
		newFlowModelItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newFlowModelItemPerformed();
			}
		});
		deleteModelItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteModelItemPerformed();
			}
		});
		// searchItem.addActionListener(new ActionListener() {
		// public void actionPerformed(ActionEvent e) {
		// searchItemPerformed();
		// }
		// });
		renameItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				renameItemPerformed();
			}
		});
		nodeSortItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nodeSortItemPerformed();
			}
		});
		nodeMoveItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nodeMoveItemPerformed();
			}
		});
		refreshItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshItemPerformed();
			}
		});

	}

	private void newFlowMapModelItemPerformed() {
		AddFlowMapModelDialog addFlowMapModelDialog = new AddFlowMapModelDialog(selectNode, jTree);
		addFlowMapModelDialog.setVisible(true);
	}

	private void newFlowModelItemPerformed() {
		AddFlowModelDialog addFlowModelDialog = new AddFlowModelDialog(selectNode, jTree);
		addFlowModelDialog.setVisible(true);
	}

	private void deleteModelItemPerformed() {
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		if (JecnConstants.isMysql) {
			listIds = JecnTreeCommon.getAllChildIds(listNode);
		} else {
			for (JecnTreeNode node : listNode) {
				listIds.add(node.getJecnTreeBean().getId());
			}
		}
		if (listIds.size() == 0) {
			return;
		}
		try {
			for (JecnTreeNode node : listNode) {
				MapType mapType = MapType.partMap;
				if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMapMode) { // 流程地图
					mapType = MapType.totalMap;
				} else if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMode) { // 流程图
					mapType = MapType.partMap;
				}

				boolean isOpen = JecnDesignerCommon.isOpen(node.getJecnTreeBean().getId().intValue(), mapType);

				if (isOpen) {
					JecnOptionPane.showMessageDialog(null, "[" + node.getJecnTreeBean().getName() + "]"
							+ JecnProperties.getValue("alreadyOpenPleaseCloseInCarryingOutAfterDeleted"), null,
							JecnOptionPane.ERROR_MESSAGE);
					return;
				}
			}
			ConnectionPool.getProcessModeAction().deleteFlowModel(listIds);
		} catch (Exception e) {
			log.error("FlowModelTreeMenu deleteModelItemPerformed is error", e);
		}
		/** 删除节点 */
		JecnTreeCommon.removeNodes(jTree, listNode);

	}

	// private void searchItemPerformed(){
	//		
	// }
	private void renameItemPerformed() {
		EditFlowMapModelDialog editFlowMapModelDialog = new EditFlowMapModelDialog(selectNode, jTree);
		editFlowMapModelDialog.setVisible(true);
	}

	/**
	 * 节点排序
	 */
	private void nodeSortItemPerformed() {
		FlowModelNodeSortDialog flowModelNodeSortDialog = new FlowModelNodeSortDialog(selectNode, jTree);
		flowModelNodeSortDialog.setVisible(true);
	}

	private void nodeMoveItemPerformed() {
		// 判断是否存在相同名称的节点
		if (!JecnDesignerCommon.validateRepeatNodesName(listNode)) {
			return;
		}
		FlowModelNodeMoveDialog flowModelNodeMoveDialog = new FlowModelNodeMoveDialog(listNode, jTree);
		flowModelNodeMoveDialog.setVisible(true);
	}

	private void refreshItemPerformed() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			List<JecnTreeBean> list = ConnectionPool.getProcessModeAction().getFlowModels(id);
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("FlowModelTreeMenu refreshItemPerformed is error", ex);
		}
	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		// 删除
		deleteModelItem.setEnabled(true);
		// 移动
		nodeMoveItem.setEnabled(true);
	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		JecnTreeCommon.autoExpandNode(jTree, selectNode);
		init();
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case processModeRoot:
			// 新建流程地图模板
			newFlowMapModelItem.setEnabled(true);
			newFlowModelItem.setEnabled(true);
			// 节点排序
			nodeSortItem.setEnabled(true);
			// 刷新
			refreshItem.setEnabled(true);
			break;
		case processMapMode:// 流程地图模板
			// 新建流程地图模板
			newFlowMapModelItem.setEnabled(true);
			// 如果父节点是根节点， 流程地图下不能创建流程图
			Long pId = selectNode.getJecnTreeBean().getPid();
			// 新建流程模板
			newFlowModelItem.setEnabled(true);
			// 删除
			deleteModelItem.setEnabled(true);
			// 搜索
			// searchItem.setEnabled(true);
			// 重命名
			renameItem.setEnabled(true);
			// 节点排序
			nodeSortItem.setEnabled(true);
			// 移动
			nodeMoveItem.setEnabled(true);
			// 刷新
			refreshItem.setEnabled(true);
			break;
		case processMode: // 流程图模板
			// 新建流程地图模板
			newFlowMapModelItem.setEnabled(false);
			// 新建流程模板
			newFlowModelItem.setEnabled(true);
			// 删除
			deleteModelItem.setEnabled(true);
			// 搜索
			// searchItem.setEnabled(true);
			// 重命名
			renameItem.setEnabled(true);
			// 节点排序
			nodeSortItem.setEnabled(true);
			// 移动
			nodeMoveItem.setEnabled(true);
			// 刷新
			refreshItem.setEnabled(true);
			break;
		default:
			break;
		}
	}
}
