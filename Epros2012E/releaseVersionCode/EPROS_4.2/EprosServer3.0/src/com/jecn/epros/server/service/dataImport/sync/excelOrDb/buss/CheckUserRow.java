package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 人员同步，人员信息行内行间校验
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-5-29 时间：下午02:41:18
 */
@Transactional(rollbackFor = Exception.class)
public class CheckUserRow {
	/** 记录登录名称个数 :为验证服务店最大用户 手机数据 */
	private Set<String> userNumberList = new HashSet<String>();

	/**
	 * 
	 * 校验人员+岗位原始数据 行内校验
	 * 
	 * @param baseBean
	 * @return
	 */
	public boolean checkUserPosRow(BaseBean baseBean) {

		boolean ret = true;

		// 获取人员+岗位原始数据
		List<UserBean> userPosBeanList = baseBean.getUserPosBeanList();
		if (userPosBeanList == null || userPosBeanList.size() == 0) {
			return ret;
		}

		// 清空数组
		userNumberList.clear();
		for (UserBean userPosBean : userPosBeanList) {
			// 人员所有字段都为空
			if (userPosBean.isAttributesNull()) {
				userPosBean.addError(SyncErrorInfo.USER_ALL_NULL);
				ret = false;
			}

			// 一条数据校验 单个值校验********************
			if (!checkOneUserPosBean(userPosBean, baseBean)) {
				ret = false;
			}
			if (StringUtils.isNotBlank(userPosBean.getUserNum())) {
				userNumberList.add(userPosBean.getUserNum());
			}
		}

		return ret;
	}

	/**
	 * 
	 * 验证人员+岗位原始数据（行内校验）
	 * 
	 * @param userBean
	 * @param baseBean
	 * @return
	 */
	private boolean checkOneUserPosBean(UserBean userBean, BaseBean baseBean) {
		boolean ret = true;
		// 人员编号去前缀后缀空后为空
		if (SyncTool.isNullOrEmtryTrim(userBean.getUserNum())) {
			userBean.addError(SyncErrorInfo.USER_NUM_NULL);
			ret = false;
		} else {
			if (SyncConstant.USER_ADMIN.equals(userBean.getUserNum())) {
				// 人员编号不能为"admin"
				userBean.addError(SyncErrorInfo.USER_NUM_AMIN);
				ret = false;
			} else if (SyncTool.checkNameMaxLength(userBean.getUserNum())) {
				// 人员编号(登录名称)不能超过122个字符
				userBean.addError(SyncErrorInfo.LOGIN_NAME_LENGTH_ERROR);
				ret = false;
			}
			userBean.setUserNum(userBean.getUserNum().trim());
		}

		// 真实姓名去前缀后缀空后为空
		if (SyncTool.isNullOrEmtryTrim(userBean.getTrueName())) {
			userBean.addError(SyncErrorInfo.TRUE_NAME_NULL);
			ret = false;
		} else {
			// if (SyncTool.checkNameFileSpecialChar(userBean.getTrueName())) {
			// // 部真实姓名由中文、英文、数字及“_”、“-”组成
			// userBean.addError(SyncErrorInfo.TRUE_NUME_SPECIAL_CHAR);
			// ret = false;
			// } else
			if (SyncTool.checkNameMaxLength(userBean.getTrueName())) {
				// 真实姓名不能超过122个字符
				userBean.addError(SyncErrorInfo.TRUE_NAME_LENGTH_ERROR);
				ret = false;
			}
			userBean.setTrueName(userBean.getTrueName().trim());
		}

		// 邮箱：必须满足邮箱格式
		if (SyncTool.isNullOrEmtryTrim(userBean.getEmail())) {
			userBean.setEmail(null);
			userBean.setEmailType(null);
		} else {
			// 去空
			userBean.setEmail(userBean.getEmail().trim());
			// 邮箱格式不正确
			Pattern emailer = Pattern.compile(SyncConstant.EMAIL_FORMAT);

			// 邮箱地址不能超过122个字符
			if (SyncTool.checkNameMaxLength(userBean.getEmail()) || !emailer.matcher(userBean.getEmail()).matches()) {
				userBean.setEmail(null);
				userBean.setEmailType(null);
			} else {
				// 内外网邮件标识:填写邮箱地址，必须指定内网还是外网(内网：0,外网：1)
				if (!userBean.checkEmailType()) {
					userBean.addError(SyncErrorInfo.EMAIL_TYPE_FAIL);
					ret = false;
				}
			}
		}

		// 电话
		if (SyncTool.isNullOrEmtryTrim(userBean.getPhone())) {
			userBean.setPhone(null);
		} else {
			// 去空
			userBean.setPhone(userBean.getPhone().trim());
			// 联系电话不能超过122个字符
			if (SyncTool.checkNameMaxLength(userBean.getPhone())) {
				userBean.setPhone(null);
			}
		}
		if (baseBean.getDeptBeanList().size() == 0) {
			return ret;
		}

		// 岗位编号
		if (!SyncTool.isNullOrEmtryTrim(userBean.getPosNum())) {// 岗位编号不为空时
			// 任职岗位编号不能超过122个字符
			if (SyncTool.checkNameMaxLength(userBean.getPosNum())) {
				userBean.addError(SyncErrorInfo.POS_NUM_LENGTH_ERROR);
				ret = false;
			} else if (SyncTool.isNullOrEmtryTrim(userBean.getPosName())
					|| SyncTool.isNullOrEmtryTrim(userBean.getDeptNum())) {
				// 岗位名称或岗位所属部门编号不能为空
				userBean.addError(SyncErrorInfo.POS_NAME_NULL_POS_NUM_NOT_NULL);
				ret = false;
			}
			// else if
			// (SyncTool.checkNameFileSpecialChar(userBean.getPosName())) {
			// // 岗位名称由中文、英文、数字及“_”、“-”组成
			// userBean.addError(SyncErrorInfo.POS_NAME_SPECIAL_CHAR);
			// ret = false;
			// }
			else if (SyncTool.checkNameMaxLength255(userBean.getPosName())) {
				// 任职岗位名称不能超过1200个字符
				userBean.addError(SyncErrorInfo.POS_NAME_LENGTH_ERROR);
				ret = false;
			} else if (SyncTool.checkNameMaxLength(userBean.getDeptNum())) {
				// 岗位所属部门编号不能超过122个字符
				userBean.addError(SyncErrorInfo.POS_DEPT_NUM_LENGTH_ERROR);
				ret = false;
			} else if (!SyncTool.isNullOrEmtryTrim(userBean.getDeptNum())) {
				// 所属部门编号在部门集合中不存在
				// if (!baseBean.existsPosByDept(userBean.getDeptNum())) {
				// userBean
				// .addError(SyncErrorInfo.POS_DEPT_NUM_NOT_EXISTS_DEPT_LIST);
				// ret = false;
				// }
			}
			userBean.setPosNum(userBean.getPosNum().trim());

		} else {// 岗位编号为空时
			if (!SyncTool.isNullOrEmtryTrim(userBean.getPosName())
					|| !SyncTool.isNullOrEmtryTrim(userBean.getDeptNum())) {
				// 岗位名称或岗位所属部门编号不能为空
				userBean.addError(SyncErrorInfo.POS_ATTRIBUTES_NULL);
				ret = false;
			} else {
				userBean.setPosNum(null);
				userBean.setPosName(null);
				userBean.setDeptNum(null);
			}
		}

		return ret;
	}

	/**
	 * 
	 * 人员行内校验、行间校验
	 * 
	 */
	public boolean checkUserRow(BaseBean baseBean) {
		if (baseBean == null || baseBean.isNull()) {
			return false;
		}

		boolean ret = true;

		// 获取人员数据
		List<UserBean> userBeanList = baseBean.getUserBeanList();
		if (userBeanList == null || userBeanList.size() == 0) {
			return ret;
		}

		// 用户数据行内校验
		for (UserBean userBean : userBeanList) {
			if (!checkOneUserBean(userBean, baseBean)) {
				ret = false;
			}
		}

		if (!ret) {
			return ret;
		}

		// ---------------行间校验
		// 人员编号唯一
		ret = checkUserRows(userBeanList);
		// ---------------行间校验

		return ret;
	}

	private boolean checkOneUserBean(UserBean userBean, BaseBean baseBean) {
		boolean ret = true;
		// 人员编号去前缀后缀空后为空
		if (SyncTool.isNullOrEmtryTrim(userBean.getUserNum())) {
			userBean.addError(SyncErrorInfo.USER_NUM_NULL);
			ret = false;
		} else {
			userBean.setUserNum(userBean.getUserNum().trim());
			// 人员编号不能为"admin"
			if (SyncConstant.USER_ADMIN.equals(userBean.getUserNum())) {
				userBean.addError(SyncErrorInfo.USER_NUM_AMIN);
				ret = false;
			}
		}

		// 真实姓名去前缀后缀空后为空
		if (SyncTool.isNullOrEmtryTrim(userBean.getTrueName())) {
			userBean.addError(SyncErrorInfo.TRUE_NAME_NULL);
			ret = false;
		} else {
			userBean.setTrueName(userBean.getTrueName().trim());
		}

		// 任职岗位编号不为空时，任职岗位编号在岗位集合中必须存在
		if (SyncTool.isNullOrEmtryTrim(userBean.getPosNum())) {
			userBean.setPosNum(null);

		} else {

			// 任职岗位编号不为空时，任职岗位编号在岗位集合中必须存在
			boolean existsUserByPos = baseBean.existsUserByPos(userBean);
			if (!existsUserByPos) {
				userBean.addError(SyncErrorInfo.USER_POS_NUM_NOT_EXISTS_POS_LIST);
				ret = false;
			} else {
				userBean.setPosNum(userBean.getPosNum().trim());
			}
		}

		// 邮箱：必须满足邮箱格式
		if (SyncTool.isNullOrEmtryTrim(userBean.getEmail())) {
			userBean.setEmail(null);
			userBean.setEmailType(null);
		} else {
			// 去空
			userBean.setEmail(userBean.getEmail().trim());

			// 邮箱格式不正确
			Pattern emailer = Pattern.compile(SyncConstant.EMAIL_FORMAT);
			if (!emailer.matcher(userBean.getEmail()).matches()) {
				userBean.addError(SyncErrorInfo.EMAIL_FORMAT_FAIL);
				ret = false;
			} else {
				// 内外网邮件标识:填写邮箱地址，必须指定内网还是外网(内网：0,外网：1)

				if (!userBean.checkEmailType()) {
					userBean.addError(SyncErrorInfo.EMAIL_TYPE_FAIL);
					ret = false;
				}
			}
		}

		// 电话
		if (SyncTool.isNullOrEmtryTrim(userBean.getPhone())) {
			userBean.setPhone(null);
		} else {
			// 去空
			userBean.setPhone(userBean.getPhone().trim());
		}
		return ret;
	}

	/**
	 * 
	 * 判断人员编号是否唯一，唯一返回true，不唯一返回false
	 * 
	 * @param userBeanList
	 *            List<UserBean>人员集合
	 * @return boolean 唯一返回true，不唯一返回false
	 */
	private boolean checkUserRows(List<UserBean> userBeanList) {
		boolean ret = true;
		for (UserBean userBean : userBeanList) {
			for (UserBean userBean2 : userBeanList) {
				// 不是同一个对象，人员编号相同
				if (!userBean.equals(userBean2) && userBean.getUserNum().equals(userBean2.getUserNum())) {

					if (SyncTool.isNullOrEmtryTrim(userBean.getPosNum())
							|| SyncTool.isNullOrEmtryTrim(userBean2.getPosNum())) {
						// 同一个人员不同岗位情况，岗位编号不能为空
						userBean.addError(SyncErrorInfo.USER_NUM_MUTIL_POS_NUM_NOT_NULL);
						ret = false;
					} else if (userBean.getPosNum().equals(userBean2.getPosNum())) {
						// 出现多条人员编号和岗位编号相同的数据
						userBean.addError(SyncErrorInfo.USER_DATA_NOT_ONLY);
						ret = false;
					} else {
						// 判断真实姓名、邮箱、邮箱类型、电话是否相同
						if (!checkRowAndRowSame(userBean, userBean2)) {
							ret = false;

						}
					}
				}
			}
		}
		return ret;
	}

	/**
	 * 
	 * 前提条件：给定参数是属于同一人员不同记录情况下 判断真实姓名、邮箱、邮箱类型、电话是否相同
	 * 
	 * @param userBean
	 * @param userBean2
	 * @return
	 */
	private boolean checkRowAndRowSame(UserBean userBean, UserBean userBean2) {
		boolean ret = false;
		// 真实姓名
		if (!userBean.getTrueName().equals(userBean2.getTrueName())) {
			userBean.addError(SyncErrorInfo.USER_TRUE_NAME_NOT_SAME);
			return ret;
		}

		// 邮箱:userBean对象邮箱不为空情况下，邮箱不相等或邮箱类型不相等
		if (!SyncTool.isNullOrEmtryTrim(userBean.getEmail())
				&& (!userBean.getEmail().equals(userBean2.getEmail()) || userBean.getEmailType() != userBean2
						.getEmailType())) {
			userBean.addError(SyncErrorInfo.USER_EMAIL_NOT_SAME);
			return ret;
		} else if (!SyncTool.isNullOrEmtryTrim(userBean2.getEmail())
				&& (!userBean2.getEmail().equals(userBean.getEmail()) || userBean2.getEmailType() != userBean
						.getEmailType())) {// 邮箱:userBean2对象邮箱不为空情况下，邮箱不相等或邮箱类型不相等
			userBean.addError(SyncErrorInfo.USER_EMAIL_NOT_SAME);
			return ret;
		}

		// 电话不为空，电话不相等
		if (!SyncTool.isNullOrEmtryTrim(userBean.getPhone()) && !userBean.getPhone().equals(userBean2.getPhone())) {
			userBean.addError(SyncErrorInfo.USER_PHONE_NOT_SAME);
			return ret;
		} else if (!SyncTool.isNullOrEmtryTrim(userBean2.getPhone())
				&& !userBean2.getPhone().equals(userBean.getPhone())) {
			userBean.addError(SyncErrorInfo.USER_PHONE_NOT_SAME);
			return ret;
		}

		return true;
	}

	public Set<String> getUserNumberList() {
		return userNumberList;
	}

}
