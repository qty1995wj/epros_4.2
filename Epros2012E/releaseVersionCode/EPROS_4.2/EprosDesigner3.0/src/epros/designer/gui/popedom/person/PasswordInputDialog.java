package epros.designer.gui.popedom.person;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;

import org.apache.log4j.Logger;

import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 输入密码
 * 
 */
public class PasswordInputDialog extends JecnDialog {
	private final Logger log = Logger.getLogger(PasswordInputDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;
	/** 密码输入panel */
	private JPanel passwordPanel = null;
	/** 按钮panel */
	private JPanel buttonPanel = null;
	/** 提示信息 */
	private JTextArea textArea = null;
	/** 密码label */
	private JLabel passwordLabel = null;
	/** 密码Field */
	private JPasswordField passwordField = null;
	private JLabel tipLabel = null;
	/** 确定按钮 */
	private JButton okBut = null;
	/** 取消按钮 */
	private JButton cancelBut = null;

	private String password = null;

	public PasswordInputDialog() {
		initCompotents();
		initLayout();
		buttonActionInit();
	}

	/**
	 * @author yxw 2013-1-11
	 * @description:组件初始化
	 */
	private void initCompotents() {
		this.setSize(300, 142);
		this.setModal(true);
		this.setLocationRelativeTo(null);
		// 提示信息
		textArea = new JTextArea();
		mainPanel = new JPanel();
		passwordPanel = new JPanel();
		buttonPanel = new JPanel();
		passwordLabel = new JLabel(JecnProperties.getValue("passwordFS"));
		passwordField = new JPasswordField();
		tipLabel = new JLabel();
		tipLabel.setForeground(Color.RED);
		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));
		// 取消按钮
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		passwordPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		textArea.setText(JecnProperties.getValue("passwordModify"));
		textArea.setOpaque(false);
		textArea.setBorder(null);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
	}

	/**
	 * @author yxw 2013-1-11
	 * @description:面板布局
	 */
	private void initLayout() {
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 2, 2);
		mainPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						10, 10, 2, 10), 0, 0);
		mainPanel.add(passwordPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		mainPanel.add(buttonPanel, c);
		// 密码修改面板
		passwordPanel.setLayout(new GridBagLayout());
		insets = new Insets(2, 1, 5, 2);

		// 提示信息
		c = new GridBagConstraints(1, 0,1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		passwordPanel.add(textArea, c);
		// 密码
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		passwordPanel.add(passwordLabel, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		passwordPanel.add(passwordField, c);
		// 空闲区域
		c = new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.BOTH, insets,
				0, 0);
		passwordPanel.add(new JLabel(), c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(tipLabel);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);

	}

	private void buttonActionInit() {
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PasswordInputDialog.this.dispose();
			}

		});
	}

	private void okButPerformed() {
		String pwd = new String(passwordField.getPassword());
		// 新密码验证
		if (DrawCommon.isNullOrEmtryTrim(pwd)) {
			tipLabel.setText(JecnProperties.getValue("newPasswordNoEmpty"));
			return;
		} else if (DrawCommon.checkPasswordMaxLength(pwd)) {
			tipLabel.setText(JecnProperties.getValue("passwordIsEnough"));
			return;
		}
		password = pwd;
		this.setVisible(false);
	}

	public String getPassword() {
		return password;
	}
}
