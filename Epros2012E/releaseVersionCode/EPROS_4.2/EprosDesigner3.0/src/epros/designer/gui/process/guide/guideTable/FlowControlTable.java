package epros.designer.gui.process.guide.guideTable;

import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.gui.integration.MultiLineCellRenderer;
import epros.designer.util.ConnectionPool;
import epros.draw.gui.operationConfig.ProcessOperationIntrusDialog;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/***
 * 流程操作说明--流程文控信息
 * 
 * @Time 2014-11-13
 * 
 */
public class FlowControlTable extends JTable {
	private Vector<Vector<String>> content;

	public FlowControlTable() {
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
	}

	public void setTableModel() {
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		this.setDefaultRenderer(Object.class, new MultiLineCellRenderer(true, 0));
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
	}

	public FlowControlTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("versionId"));// 版本号
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("modifyExplain"));// 变更说明
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("publishDate"));// 发布日期
		String otherOperaTypeValule = ConnectionPool.getConfigAciton().selectValue(6, "otherOperaType");
		if ("1".equals(otherOperaTypeValule)) {// 上海宝隆
			title.add(JecnResourceUtil.getJecnResourceUtil().getValue("fictionPeople"));// 流程拟制人
		} else {
			title.add(JecnResourceUtil.getJecnResourceUtil().getValue("drafterPeople"));// 拟稿人
		}
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("historyFollow"));// 审批人
		return new FlowControlTableMode(getContent(), title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class FlowControlTableMode extends DefaultTableModel {
		public FlowControlTableMode(Vector<Vector<String>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	public Vector<Vector<String>> getContent() {
		return content;
	}

	public void setContent(Vector<Vector<String>> content) {
		this.content = content;
	}
}
