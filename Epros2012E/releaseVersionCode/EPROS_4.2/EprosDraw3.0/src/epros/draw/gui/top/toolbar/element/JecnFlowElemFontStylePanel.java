package epros.draw.gui.top.toolbar.element;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ItemEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;

/**
 * 
 * 流程元素字体、样式属性设置类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElemFontStylePanel extends JecnAbstarctFlowElementPanel {

	/** 字体名称 */
	private JLabel fontNameLabel = null;
	private FlowElementComboBox fontNameComboBox = null;
	/** 字体大小 */
	private JLabel fontSizeLabel = null;
	private FlowElementComboBox fontSizeComboBox = null;
	/** 字体位置 */
	private JLabel fontPotisionLabel = null;
	private FlowElementComboBox fontPotisionComboBox = null;
	/** 线条类型 */
	private JLabel bodyStyleLabel = null;
	private FlowElementComboBox bodyStyleComboBox = null;
	/** 字体颜色 */
	private JLabel fontColorLabel = null;
	private JecnElementButtonPanel fontColorPanel = null;
	/** 线条粗细 */
	private JLabel bodyWidthLabel = null;
	private FlowElementComboBox bodyWidthComboBox = null;
	/** 线条颜色 */
	private JLabel bodyColorLabel = null;
	private JecnElementButtonPanel bodyColorPanel = null;

	/** 元素形状 */
	private JLabel elementShapeLabel = null;
	private FlowElementComboBox elementShapeBox = null;

	public JecnFlowElemFontStylePanel(JecnFlowElementPanel flowElementPanel) {
		super(flowElementPanel);
		init();
	}

	/**
	 * 
	 * 创建组件以及属性
	 * 
	 */
	protected void initComponents() {
		// 字体大小
		fontSizeLabel = new JLabel(flowElementPanel.getResourceManager().getValue("fontSize"), SwingConstants.RIGHT);
		fontSizeComboBox = new FlowElementComboBox(JecnFlowElementUtil.getFontSizeVector(), flowElementPanel
				.getSizeFontStyleContent());
		// 字体位置
		fontPotisionLabel = new JLabel(flowElementPanel.getResourceManager().getValue("fontPosition"),
				SwingConstants.RIGHT);
		fontPotisionComboBox = new FlowElementComboBox(JecnFlowElementUtil.getFontPostionVector(), flowElementPanel
				.getSizeFontStyleContent());
		// 字体样式
		fontNameLabel = new JLabel(flowElementPanel.getResourceManager().getValue("fontName"), SwingConstants.RIGHT);
		fontNameComboBox = new FlowElementComboBox(JecnFlowElementUtil.getFontNameVector(), flowElementPanel
				.getSizeFontStyleContent());
		// 字体颜色
		fontColorLabel = new JLabel(flowElementPanel.getResourceManager().getValue("fontColor"), SwingConstants.RIGHT);
		fontColorPanel = new JecnElementButtonPanel(flowElementPanel, flowElementPanel.getSizeFontStyleContent(),
				flowElementPanel.getBTN_FONT_COLOR());
		// 线条类型
		bodyStyleLabel = new JLabel(flowElementPanel.getResourceManager().getValue("bodyStyle"), SwingConstants.RIGHT);
		bodyStyleComboBox = new FlowElementComboBox(JecnFlowElementUtil.getBodyStyleVector(), flowElementPanel
				.getSizeFontStyleContent());
		// 线条粗细
		bodyWidthLabel = new JLabel(flowElementPanel.getResourceManager().getValue("bodyWidth"), SwingConstants.RIGHT);
		bodyWidthComboBox = new FlowElementComboBox(JecnFlowElementUtil.getBodyWidthArray(), flowElementPanel
				.getSizeFontStyleContent());
		// 线条颜色
		bodyColorLabel = new JLabel(flowElementPanel.getResourceManager().getValue("bodyColor"), SwingConstants.RIGHT);
		bodyColorPanel = new JecnElementButtonPanel(flowElementPanel, flowElementPanel.getSizeFontStyleContent(),
				flowElementPanel.getBTN_BODY_COLOR());

		// 元素形状
		elementShapeLabel = new JLabel("元素形状 ", SwingConstants.RIGHT);
		elementShapeBox = new FlowElementComboBox(JecnFlowElementUtil.getElementshapearray(), flowElementPanel
				.getSizeFontStyleContent());

		// 边框标题
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), flowElementPanel
				.getResourceManager().getValue("fontStyleTitle")));

		// *********事件*******
		// 字体名称
		fontNameComboBox.addItemListener(flowElementPanel);
		// 字体大小
		fontSizeComboBox.addItemListener(flowElementPanel);
		// 字体位置
		fontPotisionComboBox.addItemListener(flowElementPanel);
		// 线条类型
		bodyStyleComboBox.addItemListener(flowElementPanel);
		// 线条粗细
		bodyWidthComboBox.addItemListener(flowElementPanel);
		// 元素形状
		elementShapeBox.addItemListener(flowElementPanel);
	}

	/**
	 * 
	 * 布局
	 * 
	 */
	protected void initLayout() {

		// ***************************第1行 start***************************//
		// 字体样式 字体大小
		addRowToPanel(fontNameLabel, fontNameComboBox, fontSizeLabel, fontSizeComboBox, 0);
		// ***************************第1行 end***************************//

		// ***************************第2行 start***************************//
		// 字体位置 线条类型
		addRowToPanel(fontPotisionLabel, fontPotisionComboBox, bodyStyleLabel, bodyStyleComboBox, 1);
		// ***************************第2行 end***************************//

		// ***************************第3行 start***************************//
		// 字体颜色 线条粗细
		addRowToPanel(fontColorLabel, fontColorPanel, bodyWidthLabel, bodyWidthComboBox, 2);

		// ***************************第3行 end***************************//
		
		// ***************************第4行 start***************************//
//		if (flowElementPanel.getSelectedBasePanel().getFlowElementData().getEleShape() != null) {
			// 字体颜色 线条粗细
			addRowToPanel(bodyColorLabel, bodyColorPanel, elementShapeLabel, elementShapeBox, 3);
//		} else {
//			// 线条颜色
//			GridBagConstraints c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
//					GridBagConstraints.NONE, flowElementPanel.getInsetsFirst(), 0, 0);
//			this.add(bodyColorLabel, c);
//			c = new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
//					flowElementPanel.getInsetsContent(), 0, 0);
//			this.add(bodyColorPanel, c);
//		}
		// ***************************第4行 end***************************//
	}

	/**
	 * 
	 * 在流程元素面板上添加一行子组件
	 * 
	 * @param leftLabel
	 *            JLabel 左面标签
	 * @param leftCom
	 *            JComponent 左面内容
	 * @param rightLabel
	 *            JLabel 右面标签
	 * @param rightCom
	 *            JComponent 右面内容
	 * @param row
	 *            子组件的行索引 从0开始的
	 */
	private void addRowToPanel(JLabel leftLabel, JComponent leftCom, JLabel rightLabel, JComponent rightCom, int row) {
		// 左面元素
		GridBagConstraints c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, flowElementPanel.getInsetsFirst(), 0, 0);
		this.add(leftLabel, c);
		c = new GridBagConstraints(1, row, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsContent(), 0, 0);
		this.add(leftCom, c);

		// 空闲区域
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				flowElementPanel.getInsetsLabel(), 0, 0);
		this.add(new JLabel(), c);

		// 右面元素
		c = new GridBagConstraints(3, row, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsLabel(), 0, 0);
		this.add(rightLabel, c);
		c = new GridBagConstraints(4, row, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsContent(), 0, 0);
		this.add(rightCom, c);
	}

	@Override
	public void initData(JecnAbstractFlowElementData flowElementData) {
		initFontStyleData(flowElementData);
	}

	private void initFontStyleData(JecnAbstractFlowElementData flowElementData) {
		if (flowElementData == null) {
			return;
		}

		// 字体样式
		fontNameComboBox.setSelectedItem(flowElementData.getFontName());

		// 字体大小
		fontSizeComboBox.setSelectedItem(flowElementData.getFontSize());

		// 字体位置 (0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下)
		fontPotisionComboBox.setSelectedIndex(flowElementData.getFontPosition());

		// 线条类型
		bodyStyleComboBox.setSelectedIndex(flowElementData.getBodyStyle());

		// 字体颜色
		fontColorPanel.setBackground(flowElementData.getFontColor());

		// 线条粗细
		bodyWidthComboBox.setSelectedItem(flowElementData.getBodyWidth());

		// 线条颜色
		bodyColorPanel.setBackground(flowElementData.getBodyColor());

		if(flowElementData.getEleShape() == null){
			elementShapeBox.setEnabled(false);
			elementShapeLabel.setEnabled(false);
		}else{
			// 元素形状
			elementShapeBox.setSelectedIndex(getMapElemTypeByIndex(flowElementData.getMapElemType()));
		}
	}

	public void itemStateChanged(ItemEvent e) {
		// 字体名称、字体大小、字体位置、线条类型、线条粗细
		if (e.getSource() != fontNameComboBox && e.getSource() != fontSizeComboBox
				&& e.getSource() != fontPotisionComboBox && e.getSource() != bodyStyleComboBox
				&& e.getSource() != bodyWidthComboBox && e.getSource() != elementShapeBox) {

			return;
		}
		itemStateChangedProcess(e);
	}

	private void itemStateChangedProcess(ItemEvent e) {
		if (e.getStateChange() == e.SELECTED) {// 选中
			// 字体名称、字体大小、字体位置、线条类型、线条粗细
			FlowElementComboBox comboBox = (FlowElementComboBox) e.getSource();
			// 选中内容
			if (e.getSource() == fontNameComboBox) {// 字体类型
				// 修改字体类型
				JecnElementAttributesUtil.updateFontType(flowElementPanel.getSelectedBasePanel(), String
						.valueOf(comboBox.getSelectedItem()));
			} else if (e.getSource() == fontSizeComboBox) {// 字体大小
				// 修改字体大小
				JecnElementAttributesUtil.updateFontSize(flowElementPanel.getSelectedBasePanel(), Integer.valueOf(
						comboBox.getSelectedItem().toString()).intValue());
			} else if (e.getSource() == fontPotisionComboBox) {// 字体位置
				int index = comboBox.getSelectedIndex();
				if (index < 0 || index >= comboBox.getItemCount()) {
					return;
				}
				// 修改字体位置
				JecnElementAttributesUtil.updateFontPosition(flowElementPanel.getSelectedBasePanel(), index);

			} else if (e.getSource() == bodyStyleComboBox) {// 线条类型
				int index = comboBox.getSelectedIndex();
				if (index < 0 || index >= comboBox.getItemCount()) {
					return;
				}
				// 修改线条类型
				JecnElementAttributesUtil.updateLineType(flowElementPanel.getSelectedBasePanel(), index);
			} else if (e.getSource() == bodyWidthComboBox) {// 线条粗细
				int index = comboBox.getSelectedIndex();
				if (index < 0 || index >= JecnFlowElementUtil.getBodyWidthArray().length) {
					return;
				}
				int valueInt = Integer.valueOf(JecnFlowElementUtil.getBodyWidthArray()[index].toString()).intValue();
				if ((flowElementPanel.getSelectedBasePanel() instanceof VDividingLine)) {// 竖线没有重写paintComponent方法
					JecnBaseDividingLinePanel dividingLinePanel = (JecnBaseDividingLinePanel) flowElementPanel
							.getSelectedBasePanel();
					// 设置竖分割线位置点
					dividingLinePanel.setSize(valueInt, dividingLinePanel.getHeight());
				} else if ((flowElementPanel.getSelectedBasePanel() instanceof HDividingLine)) {// 横线
					JecnBaseDividingLinePanel dividingLinePanel = (JecnBaseDividingLinePanel) flowElementPanel
							.getSelectedBasePanel();
					// 设置横分割线位置点
					dividingLinePanel.setSize(dividingLinePanel.getWidth(), valueInt);
				}
				// 修改线条粗细
				JecnElementAttributesUtil.updateLineWidth(flowElementPanel.getSelectedBasePanel(), valueInt);

			} else if (e.getSource() == elementShapeBox) {// 形状
				int index = comboBox.getSelectedIndex();
				// {"","三角形","四边形","五边形","六边形","椭圆","等腰梯形","手动输入"}
				if (index == 0) {
					return;
				}
				MapElemType elemType = getMapElemTypeByIndex(index);
				flowElementPanel.getSelectedBasePanel().getFlowElementData().setMapElemType(elemType);
			}
		}

	}

	private MapElemType getMapElemTypeByIndex(int index) {
		// {"","三角形","四边形","五边形","六边形","椭圆","等腰梯形","手动输入"}
		switch (index) {
		case 1:
			return MapElemType.Triangle;
		case 2:
			return MapElemType.FunctionField;
		case 3:
			return MapElemType.Pentagon;
		case 4:
			return MapElemType.RightHexagon;
		case 5:
			return MapElemType.Oval;
		case 6:
			return MapElemType.IsoscelesTrapezoidFigure;
		case 7:
			return MapElemType.InputHandFigure;
		default:
			break;
		}
		return null;
	}

	private int getMapElemTypeByIndex(MapElemType elemType) {
		// {"","三角形","四边形","五边形","六边形","椭圆","等腰梯形","手动输入"}
		int index = 0;
		switch (elemType) {
		case Triangle:
			return 1;
		case FunctionField:
			return 2;
		case Pentagon:
			return 3;
		case RightHexagon:
			return 4;
		case Oval:
			return 5;
		case IsoscelesTrapezoidFigure:
			return 6;
		case InputHandFigure:
			return 7;
		default:
			break;
		}
		return index;
	}

	/**
	 * 
	 * 流程元素下拉框
	 * 
	 * @author ZHOUXY
	 * 
	 */
	class FlowElementComboBox extends JComboBox {

		FlowElementComboBox(Vector ctvectoror, Dimension size) {
			super(ctvectoror);
			initComponents(size);
		}

		FlowElementComboBox(Object[] objArray, Dimension size) {
			super(objArray);
			initComponents(size);
		}

		FlowElementComboBox(Dimension size) {
			initComponents(size);
		}

		private void initComponents(Dimension size) {
			// 大小
			this.setPreferredSize(size);
			this.setMinimumSize(size);
			this.setMaximumSize(size);
		}
	}

	public JLabel getFontNameLabel() {
		return fontNameLabel;
	}

	public void setFontNameLabel(JLabel fontNameLabel) {
		this.fontNameLabel = fontNameLabel;
	}

	public FlowElementComboBox getFontNameComboBox() {
		return fontNameComboBox;
	}

	public JLabel getFontSizeLabel() {
		return fontSizeLabel;
	}

	public FlowElementComboBox getFontSizeComboBox() {
		return fontSizeComboBox;
	}

	public JLabel getFontPotisionLabel() {
		return fontPotisionLabel;
	}

	public FlowElementComboBox getFontPotisionComboBox() {
		return fontPotisionComboBox;
	}

	public JLabel getBodyStyleLabel() {
		return bodyStyleLabel;
	}

	public FlowElementComboBox getBodyStyleComboBox() {
		return bodyStyleComboBox;
	}

	public JLabel getFontColorLabel() {
		return fontColorLabel;
	}

	public JecnElementButtonPanel getFontColorPanel() {
		return fontColorPanel;
	}

	public JLabel getBodyWidthLabel() {
		return bodyWidthLabel;
	}

	public FlowElementComboBox getBodyWidthComboBox() {
		return bodyWidthComboBox;
	}

	public JLabel getBodyColorLabel() {
		return bodyColorLabel;
	}

	public JecnElementButtonPanel getBodyColorPanel() {
		return bodyColorPanel;
	}
}
