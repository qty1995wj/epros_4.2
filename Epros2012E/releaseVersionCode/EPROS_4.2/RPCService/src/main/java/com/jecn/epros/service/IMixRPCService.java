package com.jecn.epros.service;

import com.jecn.epros.bean.MessageResult;

import java.util.List;

/**
 * Created by user on 2017/5/13.
 * 无法归类，或者不想归类的服务都放在这里
 */
public interface IMixRPCService {

    /**
     * 校验邮件的用户名和密码
     *
     * @param isOuter true外网 false内网
     * @param name
     * @param pwd
     * @return
     */
    public MessageResult validateEmailPassword(boolean isOuter, String name, String pwd);

    public MessageResult handleProposeSendEmail(String proposeId, int flag);

    public MessageResult submitProposeSendEmail(String proposeId);

    public MessageResult syncDictinaryConfig();

    public List<String> fetchLogNames();

    byte[] getLogByte(String name);
}
