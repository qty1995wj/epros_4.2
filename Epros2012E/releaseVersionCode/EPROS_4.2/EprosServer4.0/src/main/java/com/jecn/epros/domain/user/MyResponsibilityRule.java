package com.jecn.epros.domain.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.Resource;
import com.jecn.epros.domain.common.relatedfiles.RelatedFileBean;
import com.jecn.epros.service.TreeNodeUtils;

import java.util.Date;
import java.util.List;

/**
 * 我负责的制度 2018-7-6
 */
public class MyResponsibilityRule {
    @JsonIgnore
    private String trueName;
    @JsonIgnore
    private String ruleName;
    @JsonIgnore
    private Long id;
    @JsonIgnore
    //  1是制度,2是制度文件
    private int nodeType;
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    private Date pubTime;
    //所属目录ID
    private Long dirId;
    //所属目录名称
    private String dirName;

    List<RelatedFileBean> relatedFiles;

    public LinkResource getLink() {
        return new LinkResource(id, ruleName,LinkResource.ResourceType.RULE, TreeNodeUtils.NodeType.main);
    }

    public Long getDirId() {
        return dirId;
    }

    public void setDirId(Long dirId) {
        this.dirId = dirId;
    }

    public String getDirName() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName = dirName;
    }

    public List<RelatedFileBean> getRelatedFiles() {
        return relatedFiles;
    }

    public void setRelatedFiles(List<RelatedFileBean> relatedFiles) {
        this.relatedFiles = relatedFiles;
    }

    public LinkResource getRuleDirLink() {
        return new LinkResource(dirId, dirName, LinkResource.ResourceType.RULE_DIR,TreeNodeUtils.NodeType.main);
    }


    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNodeType() {
        return nodeType;
    }

    public void setNodeType(int nodeType) {
        this.nodeType = nodeType;
    }

    public Date getPubTime() {
        return pubTime;
    }

    public void setPubTime(Date pubTime) {
        this.pubTime = pubTime;
    }
}
