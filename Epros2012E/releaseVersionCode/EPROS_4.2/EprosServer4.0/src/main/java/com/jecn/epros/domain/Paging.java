package com.jecn.epros.domain;

import io.swagger.annotations.ApiModelProperty;

public class Paging {

    public static final Paging DEFAULT = new Paging();

    static {
        DEFAULT.pageNum = 1;
        DEFAULT.pageSize = 20;
    }

    @ApiModelProperty(value = "当前页数", required = true)
    private int pageNum;

    @ApiModelProperty(value = "每页显示的条数", required = true, allowableValues = "10,15,20")
    private int pageSize;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
