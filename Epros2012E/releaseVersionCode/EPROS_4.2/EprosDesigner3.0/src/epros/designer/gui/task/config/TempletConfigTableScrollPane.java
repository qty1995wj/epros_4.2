package epros.designer.gui.task.config;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.ui.property.table.JecnDefaultTableModel;
import epros.designer.gui.task.Constants;
import epros.designer.util.JecnProperties;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 表组件的滚动面板
 * 
 * @author ZHOUXY
 * 
 */
public class TempletConfigTableScrollPane extends JScrollPane {
	protected final Log log = LogFactory.getLog(TempletConfigTableScrollPane.class);
	/** table对象 */
	protected JTable table = null;
	/** table对应模型对象 */
	protected JecnDefaultTableModel tableModel = null;
	private List<TaskConfigItem> configs = null;

	public TempletConfigTableScrollPane(TaskConfigDialog dialog) {
		if (dialog == null) {
			log.error("JecnTableScrollPane dialog is error");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.configs = dialog.getConfigs();

		// 初始化组件
		initComponents();
	}

	public TempletConfigTableScrollPane(List<TaskConfigItem> configs) {
		this.configs = configs;
		// 初始化组件
		initComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 滚动面板
		this.setOpaque(false);
		this.setBorder(JecnUIUtil.getTootBarBorder());
		// 背景颜色
		this.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 初始化表
		initTable();

		this.setViewportView(table);
	}

	/**
	 * 
	 * 初始化表
	 * 
	 * @param headerName
	 *            String
	 */
	protected void initTable() {
		// 表头
		String[] header = { "id", "config", 
				JecnProperties.getValue("name"), 
				JecnProperties.getValue("EnglishName"),
				JecnProperties.getValue("defaultNames"),
				JecnProperties.getValue("defaultApprover"), 
				JecnProperties.getValue("whetherRequired") };
		// 创建table模型
		tableModel = new JecnDefaultTableModel(header);
		// 创建表
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		// 设置用户是否可以拖动列头，以重新排序各列,设置为false
		table.getTableHeader().setReorderingAllowed(false);
		// 设置表头背景颜色
		table.getTableHeader().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			for (int col : cols) {
				TableColumn tableColumn = table.getColumnModel().getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(6).setCellRenderer(render);
		table.getColumnModel().getColumn(6).setMinWidth(56);
		table.getColumnModel().getColumn(6).setMaxWidth(56);

		// 自定义表头UI
		table.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	public int[] gethiddenCols() {

		return new int[] { 0, 1 };
	}

	/**
	 * 
	 * 加载表数据
	 * 
	 * @param showItemList
	 *            List<TaskConfigItem>
	 */
	private void flushTable(List<TaskConfigItem> showItemList) {
		// 清除表行
		DefaultTableModel df = (DefaultTableModel) table.getModel();
		// 反向删除
		for (int i = table.getRowCount() - 1; i >= 0; i--) {
			df.removeRow(i);
		}

		// 添加表行
		Object[][] tableData = getTableData(showItemList);

		if (tableData == null) {
			return;
		}
		tableModel.setData(tableData);

		table.repaint();
	}

	/**
	 * 
	 * 获取表数据
	 * 
	 * @param showItemList
	 *            List<TaskConfigItem>
	 * 
	 * @return Object[][]
	 */
	protected Object[][] getTableData(List<TaskConfigItem> configs) {

		if (configs == null || configs.size() == 0) {
			return null;
		}
		// 创建数组
		Object[][] data = new Object[configs.size()][7];

		for (int i = 0; i < configs.size(); i++) {
			TaskConfigItem item = configs.get(i);
			data[i][0] = item.getId();
			data[i][1] = item;
			data[i][2] = item.getName();
			data[i][3] = item.getEnName();
			data[i][4] = item.getDefaultName();
			data[i][5] = appendApprovePeopleName(item.getUsers());
			data[i][6] = item.getIsEmpty() == 0 ? JecnProperties.getValue("task_emptyBtn") : JecnProperties.getValue("task_notEmptyBtn");
		}
		return data;
	}

	private String appendApprovePeopleName(List<JecnUser> users) {
		if (users == null || users.size() == 0) {
			return "";
		}
		String approvePeopleName = "";
		for (JecnUser user : users) {
			approvePeopleName += getSplitChar() + user.getTrueName();
		}
		if (approvePeopleName.startsWith(getSplitChar())) {
			approvePeopleName = approvePeopleName.replaceFirst(getSplitChar(), "");
		}

		return approvePeopleName;
	}

	private String getSplitChar() {
		return ",";
	}





	@SuppressWarnings("unchecked")
	public List<TaskConfigItem> getShowConfigItems() {
		List<TaskConfigItem> showConfigs = new ArrayList<TaskConfigItem>();
		for (TaskConfigItem item : configs) {
			if (item.getMark().equals(ConfigItemPartMapMark.taskAppPubType.toString())) {
				continue;
			}
			if (item.getValue().equals(Constants.ITEM_IS_SHOW)) {
				showConfigs.add(item);
			}
		}
		return showConfigs;
	}

	public TaskConfigItem getApproveType() {
		// List<TaskConfigItem> configs = dialog.getConfigs();
		if (configs == null || configs.size() == 0) {
			return null;
		}
		for (TaskConfigItem itemBean : configs) {
			if (itemBean.getMark().equals(ConfigItemPartMapMark.taskAppPubType.toString())) {
				return itemBean;
			}
		}

		throw new NullPointerException();

	}

	public void resetData(List<TaskConfigItem> configs) {
		this.configs = configs;
		this.flushTable(getShowConfigItems());
	}

}
