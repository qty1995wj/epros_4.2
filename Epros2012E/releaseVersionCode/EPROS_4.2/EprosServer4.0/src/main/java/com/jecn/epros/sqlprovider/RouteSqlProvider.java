package com.jecn.epros.sqlprovider;

import com.jecn.epros.sqlprovider.server3.SqlCommon;

import java.util.Map;

public class RouteSqlProvider extends BaseSqlProvider {

    /**
     * 流程路径
     *
     * @param map
     * @return
     */
    public String findFlowRoute(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        return "SELECT DISTINCT JFS.FLOW_ID AS FILE_ID,JFS.FLOW_NAME AS FILE_NAME,'' AS P_FILE_NAME"
                + "            ,JFS.ISFLOW AS FILE_TYPE,CASE WHEN JFS.FLOW_ID =" + id
                + " THEN 0 ELSE 1 END AS FILE_COUNT,JFS.PRE_FLOW_ID,JFS.SORT_ID"
                + "            FROM JECN_FLOW_STRUCTURE JFS"
                + "            INNER JOIN JECN_FLOW_STRUCTURE PUB ON PUB.FLOW_ID =" + id + " AND PUB.T_PATH LIKE "
                + SqlCommon.getJoinFunc("JFS") + "            WHERE JFS.DEL_STATE=0"
                + "            ORDER BY JFS.PRE_FLOW_ID,JFS.SORT_ID,JFS.FLOW_ID";
    }

    /**
     * 制度路径
     *
     * @param map
     * @return
     */
    public String findRuleRoute(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        return "SELECT DISTINCT JR.ID AS FILE_ID,JR.RULE_NAME AS FILE_NAME,'' AS P_FILE_NAME"
                + "             ,JR.IS_DIR AS FILE_TYPE,CASE WHEN PUB.ID=" + id
                + " THEN 0 ELSE 1 END AS FILE_COUNT,JR.PER_ID,JR.SORT_ID" + "             FROM JECN_RULE JR"
                + "             INNER JOIN JECN_RULE PUB ON PUB.ID=" + id + " AND PUB.T_PATH LIKE "
                + SqlCommon.getJoinFunc("JR") + "  where JR.DEL_STATE=0 ORDER BY JR.PER_ID,JR.SORT_ID,JR.ID";
    }

    /**
     * 标准路径
     *
     * @param map
     * @return
     */
    public String findStandardRoute(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        return "SELECT DISTINCT JR.CRITERION_CLASS_ID AS FILE_ID,JR.CRITERION_CLASS_NAME AS FILE_NAME,'' AS P_FILE_NAME"
                + "     ,JR.STAN_TYPE AS FILE_TYPE" + "     ,CASE WHEN JR.CRITERION_CLASS_ID =" + id
                + " THEN 0 ELSE 1 END AS FILE_COUNT,JR.PRE_CRITERION_CLASS_ID,JR.SORT_ID"
                + "     FROM JECN_CRITERION_CLASSES JR"
                + "     INNER JOIN JECN_CRITERION_CLASSES PUB ON PUB.CRITERION_CLASS_ID=" + id + " AND PUB.T_PATH LIKE "
                + SqlCommon.getJoinFunc("JR")
                + "     ORDER BY JR.PRE_CRITERION_CLASS_ID,JR.SORT_ID,JR.CRITERION_CLASS_ID";
    }

    /**
     * 风险 路径
     *
     * @param map
     * @return
     */
    public String findRiskRoute(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        return "SELECT DISTINCT JR.ID AS FILE_ID,JR.NAME AS FILE_NAME,'' AS P_FILE_NAME"
                + "     ,JR.IS_DIR AS FILE_TYPE" + "     ,CASE WHEN JR.ID =" + id
                + " THEN 0 ELSE 1 END AS FILE_COUNT,JR.PARENT_ID,JR.SORT" + "     FROM JECN_RISK JR"
                + "             INNER JOIN JECN_RISK PUB ON PUB.ID=" + id + " AND PUB.T_PATH LIKE "
                + SqlCommon.getJoinFunc("JR") + "     ORDER BY JR.PARENT_ID,JR.SORT,JR.ID";
    }

    /**
     * 文件 路径
     *
     * @param map
     * @return
     */
    public String findFileRoute(Map<String, Object> map) {
        Long id = Long.valueOf(map.get("id").toString());
        return "SELECT DISTINCT JR.FILE_ID AS FILE_ID,JR.FILE_NAME AS FILE_NAME,'' AS P_FILE_NAME"
                + "     ,JR.IS_DIR AS FILE_TYPE" + "     ,CASE WHEN JR.FILE_ID =" + id
                + " THEN 0 ELSE 1 END AS FILE_COUNT,JR.PER_FILE_ID,JR.SORT_ID" + "     FROM JECN_FILE JR"
                + "            INNER JOIN JECN_FILE PUB ON PUB.FILE_ID=" + id + " AND PUB.T_PATH LIKE "
                + SqlCommon.getJoinFunc("JR") + "            WHERE  JR.DEL_STATE=0"
                + "            ORDER BY JR.PER_FILE_ID,JR.SORT_ID,JR.FILE_ID";
    }

}
