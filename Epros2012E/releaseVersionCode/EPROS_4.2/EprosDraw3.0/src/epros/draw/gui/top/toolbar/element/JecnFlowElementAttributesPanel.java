package epros.draw.gui.top.toolbar.element;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 
 * 
 * 右键点击流程元素
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementAttributesPanel extends JecnFlowElementPanel {

	/** 画板上被点击的流程元素对象 */
	private JecnBaseFlowElementPanel elementPanel = null;

	public JecnFlowElementAttributesPanel(Container comp, JecnBaseFlowElementPanel elementPanel, boolean downLayoutFlag) {

		super(comp, downLayoutFlag);

		if (elementPanel == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_ATTRIBUTES_PANEL
					.error("JecnFlowElementAttributesPanel类构造函数：参数为null。elementPanel=" + elementPanel);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.elementPanel = elementPanel;

		// 添加流程元素到流程元素界面，更新流程面板上属性值
		initFlowELementObj();
	}

	/**
	 * 
	 * 确认按钮动作处理
	 * 
	 */
	protected void okBtnProcess(ActionEvent e) {
		if (selectedBasePanel == null || elementPanel == null) {
			return;
		}

		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}

		// 获取选中的流程元素
		List<JecnBaseFlowElementPanel> currentSelectElement = workflow.getCurrentSelectElement();

		if (currentSelectElement.size() == 0) {
			return;
		}

		// 操作前数据
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();

		// 记录操作图形元素集合
		List<JecnBaseFigurePanel> listFigure = new ArrayList<JecnBaseFigurePanel>();

		// 记录操作前数据
		undoData.recodeFlowElement(currentSelectElement);
		for (JecnBaseFlowElementPanel elem : currentSelectElement) {

			// 获取当前修改的流程元素值更新画图面板上的流程元素
			elem.getFlowElementData().converFlowElementData(selectedBasePanel.getFlowElementData());

			if ((elem instanceof CommentText) && (selectedBasePanel instanceof CommentText)) {// 判断是否为注释框
				((CommentText) elem).setBooleanFlags((CommentText) selectedBasePanel);
			}

			if (elem instanceof JecnBaseFigurePanel) {// 图形
				// 重设大小
				JecnBaseFigurePanel figure = (JecnBaseFigurePanel) elem;
				figure.reSetFigureSize();
				// 记录操作图形元素集合
				listFigure.add(figure);

				// 阴影判断
				figure.setFigureShadow();
			} else if (elem instanceof JecnBaseManhattanLinePanel) {
				JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) elem;
				// 当前面板下执行画线（放大缩小、改变线属性算法） 小线段数据层不变
				linePanel.paintZoomLine();
			} else if (elem instanceof HDividingLine || elem instanceof VDividingLine) {
				JecnBaseDividingLinePanel dividingLinePanel = (JecnBaseDividingLinePanel) elem;
				// 设置横竖分割线位置点
				dividingLinePanel.setBounds(dividingLinePanel.getStartPoint(), dividingLinePanel.getEndPoint());
			}

		}

		if (currentSelectElement.size() == 1) {
			// 更新工具栏的状态
			JecnElementAttributesUtil.updateEditTool(currentSelectElement.get(0));
		}

		// 重画图形相关的连接线
		JecnDraggedFiguresPaintLine.paintLineByFigures(listFigure);
		workflow.getCurrDrawPanelSizeByPanelList();
		
		// 记录操作后数据
		redoData.recodeFlowElement(currentSelectElement);
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		workflow.getUndoRedoManager().addEdit(unredoProcess);

		workflow.repaint();
	}

	/**
	 * 
	 * 添加流程元素到流程元素界面，更新流程面板上属性值
	 * 
	 */
	private void initFlowELementObj() {
		// 加流程元素到流程元素界面，修改属性
		this.addFlowELementObj(elementPanel.clone());
	}
}
