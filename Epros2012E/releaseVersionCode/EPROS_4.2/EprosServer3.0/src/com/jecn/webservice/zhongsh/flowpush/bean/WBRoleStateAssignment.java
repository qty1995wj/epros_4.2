package com.jecn.webservice.zhongsh.flowpush.bean;

public class WBRoleStateAssignment {
	private String roleGUID;
	private String stateGUID;
	private String editableColumns = "";
	private String displayableColumns = "";
	private String notEditableColumns = "";
	private String notDisplayableColumns = "";

	public String getRoleGUID() {
		return roleGUID;
	}

	public void setRoleGUID(String roleGUID) {
		this.roleGUID = roleGUID;
	}

	public String getStateGUID() {
		return stateGUID;
	}

	public void setStateGUID(String stateGUID) {
		this.stateGUID = stateGUID;
	}

	public String getEditableColumns() {
		return editableColumns;
	}

	public void setEditableColumns(String editableColumns) {
		this.editableColumns = editableColumns;
	}

	public String getDisplayableColumns() {
		return displayableColumns;
	}

	public void setDisplayableColumns(String displayableColumns) {
		this.displayableColumns = displayableColumns;
	}

	public String getNotEditableColumns() {
		return notEditableColumns;
	}

	public void setNotEditableColumns(String notEditableColumns) {
		this.notEditableColumns = notEditableColumns;
	}

	public String getNotDisplayableColumns() {
		return notDisplayableColumns;
	}

	public void setNotDisplayableColumns(String notDisplayableColumns) {
		this.notDisplayableColumns = notDisplayableColumns;
	}
}
