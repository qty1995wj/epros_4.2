package epros.designer.gui.popedom.positiongroup;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class PosGroupTreeListener extends JecnTreeListener {
	
	private static Logger log = Logger.getLogger(PosGroupTreeListener.class);
	private JecnHighEfficiencyTree jTree = null;
	
	public PosGroupTreeListener(JecnHighEfficiencyTree jTree){
		this.jTree = jTree;
	}
	
	/**
	 * 合拢
	 */
	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}
	/**
	 * 展开
	 */
	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = null;
				if(node.getJecnTreeBean().getTreeNodeType()==TreeNodeType.positionGroup){
					list = ConnectionPool.getPosGroup().getPosByPositionGroupId(node.getJecnTreeBean().getId());
				}else{
					list = ConnectionPool.getPosGroup().getChildPositionGroup(node.getJecnTreeBean().getId(),JecnConstants.projectId);
				}
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("PosGroupTreeListener treeExpanded is error！", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);		
	}

}
