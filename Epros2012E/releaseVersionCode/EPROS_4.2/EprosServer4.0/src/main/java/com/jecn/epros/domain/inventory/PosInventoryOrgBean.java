package com.jecn.epros.domain.inventory;

import java.util.List;

public class PosInventoryOrgBean {
    private Long orgId;
    private String orgName;
    private Long pOrgId;
    private int level;
    private String path;
    private List<PosInventoryPosBean> listPosInventoryPosBean;

    public List<PosInventoryPosBean> getListPosInventoryPosBean() {
        return listPosInventoryPosBean;
    }

    public void setListPosInventoryPosBean(List<PosInventoryPosBean> listPosInventoryPosBean) {
        this.listPosInventoryPosBean = listPosInventoryPosBean;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getpOrgId() {
        return pOrgId;
    }

    public void setpOrgId(Long pOrgId) {
        this.pOrgId = pOrgId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
