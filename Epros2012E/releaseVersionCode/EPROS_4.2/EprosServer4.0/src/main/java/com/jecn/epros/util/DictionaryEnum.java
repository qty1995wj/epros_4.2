package com.jecn.epros.util;

/**
 * Created by user on 2017/5/24.
 */
public enum DictionaryEnum {
    FILE_TYPE,// 文件类型
    DCC,// 指定DCC
    INCONFORMITY_TYPE,// 不符合类别
    CULTURE_LEVEL,// 文化层级
    APPLICABLE_SYSTEM,// 适用体系
    APPLICABLE_COMPANY,// 适用公司
    SECRET_LEVEL,//密级
    APPLICABLE_PROVISION, //适用条款
    STANDARD_FRAMEWORK, // 标准化架构
    PEOPLE_GROUP,// 人员组
    JECN_SECURITY_LEVEL,// 保密级别
    JECN_RISK_TYPE,// 风险类别
    BUSS_TYPE,//业务类型
    NORMAL_FILE  //常用文件
}
