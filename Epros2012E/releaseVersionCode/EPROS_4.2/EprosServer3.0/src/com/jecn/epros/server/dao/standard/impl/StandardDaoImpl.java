package com.jecn.epros.server.dao.standard.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.dao.standard.IStandardDao;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnUtil;

public class StandardDaoImpl extends AbsBaseDao<StandardBean, Long> implements IStandardDao {

	@Override
	public void deleteStandards(Set<Long> setIds) throws Exception {
		// 标准和流程的关系 -设计器
		String hql = "delete from StandardFlowRelationTBean where criterionClassId in";
		List<String> list = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : list) {
			execteHql(str);
		}
		// 标准和流程的关系 -浏览端
		hql = "delete from StandardFlowRelationBean where criterionClassId in";
		list = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : list) {
			execteHql(str);
		}
		// 查阅权限
		JecnUtil.deleteViewAuth(setIds, 2, this);
		// 设计器权限
		JecnUtil.deleteDesignAuth(setIds, 2, this);

		// 标准对象
		hql = "delete from StandardBean where criterionClassId in";
		list = JecnCommonSql.getSetSqls(hql, setIds);
		for (String str : list) {
			execteHql(str);
		}
		//删除收藏
		String sql =  "delete from jecn_my_star where type=5 and related_id in";
		list = JecnCommonSql.getSetSqls(sql, setIds);
		for (String str : list) {
			execteNative(str);
		}
		// 删除合理化建议 0流程，1制度，2流程架构，3文件，4风险，5标准
		JecnDaoUtil.deleteProposeByRelatedIds(setIds, 5, this);
	}

	@Override
	public void deleteStandards(Long projectId) throws Exception {
		String hql = "select criterionClassId from StandardBean where projectId=?";
		List<Long> list = this.listHql(hql, projectId);
		Set<Long> setIds = new HashSet<Long>();
		for (Long id : list) {
			if (id != null) {
				setIds.add(id);
			}
		}
		if (setIds.size() > 0) {
			this.deleteStandards(setIds);
		}

	}

	@Override
	public void reNameStandardByRelationId(String newName, int type, Long relationId) {
		String hql;
		if (type == 0) {
			hql = "update StandardBean set criterionClassName=? where stanType=1 and relationId=? ";

		} else {
			hql = "update StandardBean set criterionClassName=? where (stanType=2 or stanType=3) and relationId=? ";
		}

		this.execteHql(hql, newName, relationId);
	}

	/**
	 * 角色管理，标准权限结果集
	 * 
	 * @param pid
	 * @param projectId
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getRoleAuthChildStandards(Long pid, Long projectId, Long peopleId) throws Exception {
		String sql = "select distinct t.criterion_class_id,"
				+ "       t.criterion_class_name ,t.pre_criterion_class_id"
				+ "       ,t.stan_type,t.related_id,"
				+ "       case when jup.criterion_class_id is null then '0' else '1' end as count"
				+ " ,jfs.FLOW_NAME,'' AS STAN_CONTENT,jf.flow_id,jft.file_id,jft.file_name,t.sort_id"
				+ "         from jecn_criterion_classes t"
				+ "         left join JECN_FLOW_STRUCTURE_T jfs on JFS.FLOW_ID = t.RELATED_ID"
				+ "         and (t.STAN_TYPE = 2 OR t.STAN_TYPE = 3) and JFS.DEL_STATE = 0"
				+ "         left join jecn_file_t jft on jft.file_id=t.related_id and t.STAN_TYPE=1 and jft.del_state=0"
				+ "         left join JECN_FLOW_STRUCTURE jf on JFS.FLOW_ID = jf.flow_id"
				+ "         left join jecn_criterion_classes jup on jup.pre_criterion_class_id=t.criterion_class_id";
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(2, peopleId, projectId);
		sql = sql + "         where t.pre_criterion_class_id=? and t.project_id=?"
				+ "         order by t.pre_criterion_class_id,t.sort_id,t.criterion_class_id";
		return this.listNativeSql(sql, pid, projectId);
	}
}
