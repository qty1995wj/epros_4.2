package com.jecn.epros.server.bean.system;

import java.io.Serializable;

public class PubSourceCopyRelated implements Serializable {

	public final static int PROCESS = 0;
	public final static int FILE = 1;
	public final static int RULE = 2;

	public final static int ORG = 0;
	public final static int POS_GROUP = 1;
	public final static int POS = 2;
	public final static int PEOPLE = 3;

	private String id;
	private Long rId;
	private String rName;
	private Integer rType;
	private Long cId;
	private String cName;
	private Integer cType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getrId() {
		return rId;
	}

	public void setrId(Long rId) {
		this.rId = rId;
	}

	public String getrName() {
		return rName;
	}

	public void setrName(String rName) {
		this.rName = rName;
	}

	public Integer getrType() {
		return rType;
	}

	public void setrType(Integer rType) {
		this.rType = rType;
	}

	public Long getcId() {
		return cId;
	}

	public void setcId(Long cId) {
		this.cId = cId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public Integer getcType() {
		return cType;
	}

	public void setcType(Integer cType) {
		this.cType = cType;
	}

}
