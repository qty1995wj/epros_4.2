/*
 * 流程体系
 */

function processSys() {
	var processRoot = new Ext.tree.AsyncTreeNode({
		expanded : true,
		id : "0",
		listeners : {
			'expand' : function() {
				if (processRoot.hasChildNodes()) {
					node = processRoot.childNodes[0];
					Ext.Ajax.request({
						url : basePath + "processLook.action",
						params : {
							flowId : node.id,
							type : node.attributes.type,
							dbClick : 'dbClick'
						},
						success : function(response, r) {
							if (response.responseText == 'noPopedom') {
								processTabPanel.add({
											title : i_search,// 搜索
											border : false,
											id : "processSearchTab_id",
											autoLoad : {
												url : basePath
														+ "getAllProcessType.action",
												callback : function() {
													processGrid();
													processMapGrid();
												}
											}

										});
								// 激活流程搜索tab
								processTabPanel
										.setActiveTab("processSearchTab_id");
								return;
							}
							//请求的非搜索页面时，加载流程地图Tab
							if(index==""){
								node.select();
								processMapContainerJsp!="processMapSvg.jsp"&&Ext.MessageBox.wait(i_isLoading, i_tip);
							}
							processTabPanel.insert(0, {
								title : i_processMap,// 流程地图
								id : "processMap_id",
								border : false,
								autoLoad : {
									url : basePath
											+ "jsp/processSys/"+processMapContainerJsp+"?mapID="
											+ node.id + "&mapName="
											+ node.attributes.text
											+ "&mapType="
											+ node.attributes.type+"&isPub=true",
									callback:function(){
										 Ext.TaskMgr.start(taskTimer);
									}
								}
							});
							if (isMapFileShow) {
								processTabPanel.insert(1, {
									title : "流程架构说明",// 文件说明
									border : false,
									id : "processMapOperateDes_id",
									autoLoad : {
										url : basePath
												+ "processMapFile.action?flowId="
												+ node.id,
										callback : function() {
											setProcessFileDivWH('processMapFileDiv');
										}
									}
								});
							}
							if (isHiddenSystem==0) {
								processTabPanel.insert(2, {
									title : i_relateRule,// 相关制度
									border : false,
									id : "processMapRelateRule_id",
									autoLoad : {
										url : basePath
												+ "jsp/processSys/processMap/ProcessMapRelateRule.jsp",
										callback : function() {
											processMapRelateRuleGrid(node.id,false);
										}
									}
								});
							}
							//if (isAdmin || isViewAdmin) {
								processTabPanel.insert(3, {
									title : i_processList,// 流程清单
									border : false,
									id : "processList_id",
									autoLoad : {
										scripts : true,
										url : basePath
												+ "jsp/processSys/processMap/processList.jsp",
										callback : function() {
											processListJson(node.id, false,false);
										}
									}
								});
								

							//}
						if (isAdmin || isViewAdmin || isDocControlPermission) {
							processTabPanel.insert(4, {
									title : i_historyInfo,// 文控信息
									border : false,
									id : "processMap_historyInfo_id",
									autoLoad : {
										url : basePath
												+ "processHistoryNew.action?flowId="
												+ node.id,
										callback : function() {
											setDocumentTemplate('processDocumentDiv');
										}
									}
								});
							}
							processTabPanel.add({
								title : i_search,// 搜索
								border : false,
								id : "processSearchTab_id",
								autoLoad : {
									url : basePath + "getAllProcessType.action",
									callback : function() {
										processGrid();
										processMapGrid();
									}
								}
							});
							// 激活Tab
							if(index=="processSearch"){	//华帝特殊需求-切换激活的Tab为搜索页面
								processTabPanel.setActiveTab("processSearchTab_id");
								index="";
							}else{
								processTabPanel.setActiveTab("processMap_id");
							}
						}
					});

				}
			}
		}
	})

	var processLoader = new Ext.tree.TreeLoader({
				dataUrl : 'getChildFlows.action'
			})

	var processTabPanel = new Ext.TabPanel({
		border : false,
//		 resizeTabs:true,//True表示为自动调整各个标签页的宽度
		 enableTabScroll:true,
		height : Ext.getCmp("main-panel").getInnerHeight(),
		listeners : {
			'bodyresize' : function(p, width, height) {
				if (p.getActiveTab() == null) {
					return;
				}
				if (p.getActiveTab().getId() == 'processSearchTab_id') {
					if (document.getElementById("processRadio") != null
							&& document.getElementById("processRadio").checked) {
						if (typeof(processGridPanel) != "undefined") {
							processGridPanel.setSize(width, height - 187);
						}
					} else {
						if (typeof(processMapGridPanel) != "undefined") {
							processMapGridPanel.setSize(width, height - 187);
						}
					}
				} else if (p.getActiveTab().getId() == 'processMapOperateDes_id') {
					setProcessFileDivWH('processMapFileDiv');
				} else if (p.getActiveTab().getId() == 'processMap_historyInfo_id') {
					setDocumentTemplate('processDocumentDiv');
				} else if (p.getActiveTab().getId() == 'processOperateDes_id') {
					setProcessFileDivWH('processFileDiv');
				} else if (p.getActiveTab().getId() == 'processoperateMode_id') {
					setProcessTemplate('processTemplateDiv');
				} else if (p.getActiveTab().getId() == 'relaProStanRule_id') {
					setDocumentTemplate('relatedProcessRuleStandardId');
				} else if (p.getActiveTab().getId() == 'process_historyInfo_id') {
					setDocumentTemplate('processDocumentDiv');
				} else if (p.getActiveTab().getId() == 'rtnlPropose_id') {
					setAllStateProposeDivWH('flowAllProposeJspId',false);
				} else if (p.getActiveTab().getId() == 'processList_id') {
					if (typeof(processListGridPanel) != "undefined") {
						processListGridPanel.setSize(width, height - 35);
					}
				}
			},
			'tabchange' : function(tabPanel, panel) {
				if (typeof(panel) == "undefined") {
					return;
				}
				if (panel.getId() == 'processSearchTab_id') {
					if (document.getElementById("processRadio") != null
							&& document.getElementById("processRadio").checked) {
						if (typeof(processGridPanel) != "undefined") {
							processGridPanel.setSize(tabPanel.getWidth(),
									tabPanel.getHeight() - 215);
						}
					} else {
						if (typeof(processMapGridPanel) != "undefined") {
							processMapGridPanel.setSize(tabPanel.getWidth(),
									tabPanel.getHeight() - 215);
						}
					}
				} else if (panel.getId() == 'processMapOperateDes_id') {
					setProcessFileDivWH('processMapFileDiv');
				} else if (panel.getId() == 'processMap_historyInfo_id') {
					setDocumentTemplate('processDocumentDiv');
				} else if (panel.getId() == 'processOperateDes_id') {
					setProcessFileDivWH('processFileDiv');
				} else if (panel.getId() == 'processoperateMode_id') {
					setProcessTemplate('processTemplateDiv');
				} else if (panel.getId() == 'relaProStanRule_id') {
					setDocumentTemplate('relatedProcessRuleStandardId');
				} else if (panel.getId() == 'process_historyInfo_id') {
					setDocumentTemplate('processDocumentDiv');
				} else if (panel.getId() == 'rtnlPropose_id') {
					setAllStateProposeDivWH('flowAllProposeJspId',false);
				} else if (panel.getId() == 'processList_id') {
					if (typeof(processListGridPanel) != "undefined") {
						processListGridPanel.setSize(tabPanel.getWidth() - 2,
								tabPanel.getHeight() - 60);
					}
				}

			}
		}
	})
	var processTree = new Ext.tree.TreePanel({
		root : processRoot,
		height : Ext.getCmp("main-panel").getInnerHeight() - 25,
		loader : processLoader,
		useArrows : true,
		border : false,
		autoScroll : true,
		rootVisible : false,// 隐藏根节点
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		listeners : {
			'dblclick' : function(node, e) {
				Ext.Ajax.request({
					url : basePath + "processLook.action",
					params : {
						flowId : node.id,
						type : node.attributes.type,
						dbClick : 'dbClick'
					},
					success : function(response, r) {
						if (response.responseText == 'noPopedom') {
							alert(i_noPopedom);// 您没有访问权限!
							return;
						}
						processMapContainerJsp!="processMapSvg.jsp"&&Ext.MessageBox.wait(i_isLoading, i_tip);
						if (node.attributes.type == "process") {// 流程图节点
							var panel = processTabPanel.getActiveTab();
							var length = processTabPanel.items.length;
							for (var i = length - 1; i >= 0; i--) {
								var p = processTabPanel.get(i);
								if (p.getId() != panel.getId()) {
									processTabPanel.remove(p, true);
								}
							}
							processTabPanel.remove(panel, true);
							processTabPanel.insert(0, {
								title : i_procesChart,// 流程图
								border : false,
								id : "processChar_id",
								autoLoad : {
									url : basePath
											+ "jsp/processSys/"+processMapContainerJsp+"?mapID="
											+ node.id + "&mapName="
											+ node.attributes.text
											+ "&mapType="
											+ node.attributes.type+"&isPub=true",
									callback : function() {
										Ext.TaskMgr.start(taskTimer);
									}
								}
							});
							processTabPanel.insert(1, {
								title : "流程属性",// 概况信息
								border : false,
								id : "processOvervicwInfo_id",
								autoLoad : basePath
										+ "processProfileInformation.action?flowId="
										+ node.id
							});
							processTabPanel.insert(2, {
										title : "流程说明",// 操作说明
										border : false,
										id : "processOperateDes_id",
										autoLoad : {
											url : basePath
													+ "processFile.action?flowId="
													+ node.id,
											callback : function() {
												setProcessFileDivWH('processFileDiv');
											}
										}
									});
							if(!isHiddenBDF){
								processTabPanel.insert(3, {
								title : i_operateMode,// 操作模板
								border : false,
								id : "processoperateMode_id",
								autoLoad : {
									url : basePath
											+ "processTemplate.action?flowId="
											+ node.id,
									callback : function() {
										setProcessTemplate('processTemplateDiv');
									}
								}
							});
							}
							
							if (versionType&&!isHiddenBDF) {// 非标准版或者巴德富 隐藏相关文件
								processTabPanel.insert(4, {
									title : i_relateFile,// 相关文件
									border : false,
									id : "relaProStanRule_id",
									autoLoad : {
										url : basePath
												+ "relatedProcessRuleStandard.action?flowId="
												+ node.id,
										callback : function() {
											setDocumentTemplate('relatedProcessRuleStandardId');
										}
									}
								});
							}
							//
							// processTabPanel
							// .insert(
							// 5,
							// {
							// title : i_property,// 属性
							// id : "processProperty_id",
							// autoLoad : basePath
							// + "processAttribute.action?flowId="
							// + node.id
							// });
							if (isAdmin || isViewAdmin || isDocControlPermission) {
								processTabPanel.insert(6, {
									title : i_historyInfo,// 文控信息
									border : false,
									id : "process_historyInfo_id",
									autoLoad : {
										url : basePath
												+ "processHistoryNew.action?flowId="
												+ node.id,
										callback : function() {
											setDocumentTemplate('mainProposeId');
										}
									}
								});
							}
							if (versionType) {
								processTabPanel.insert(7, {
											title : "KPI",
											border : false,
											id : "processKPI_id",
											autoLoad : {
												url : basePath
														+ "flowKpiNames.action?flowId="
														+ node.id,
												scripts : true,
												callback :function(){
									                   initFlowKpiWH();
													
												}
											},
											listeners : {
														'render' : function() {
															
														},
														'bodyresize':function(){
				                                         initFlowKpiWH();
												}
											}
										});
										
							}
							processTabPanel.insert(8, {
								title : i_rationalization,// 合理化建议
								border : false,
								id : "rtnlPropose_id",
								autoLoad : {
								    scripts : true,
									url : basePath
											+ "allrtnlPropose.action?flowId="
											+ node.id,
									callback : function() {
										onloadPropose(node.id,false,node.text);
									}
								}

							});
							
							processTabPanel.add({
								title : i_search,// 搜索
								border : false,
								id : "processSearchTab_id",
								autoLoad : {
									url : basePath + "getAllProcessType.action",
									callback : function() {
										processGrid();
										processMapGrid();
									}
								}

							});
							// 激活流程图tab
							processTabPanel.setActiveTab("processChar_id");

						} else if (node.attributes.type == "processMap") {
							var panel = processTabPanel.getActiveTab();
							var length = processTabPanel.items.length;
							for (var i = length - 1; i >= 0; i--) {
								var p = processTabPanel.get(i);
								if (p.getId() != panel.getId()) {
									processTabPanel.remove(p, true);
								}
							}
							processTabPanel.remove(panel, true);
							processTabPanel.insert(0, {
								title : i_processMap,// 流程地图
								border : false,
								id : "processMap_id",
								autoLoad : {
									url : basePath
											+ "jsp/processSys/"+processMapContainerJsp+"?mapID="
											+ node.id + "&mapName="
											+ node.attributes.text
											+ "&mapType="
											+ node.attributes.type+"&isPub=true",
									callback : function() {
										Ext.TaskMgr.start(taskTimer);
									}
								}
							});
							if (isMapFileShow) {
								processTabPanel.insert(1, {
									title : i_fileDescription,// 文件说明
									border : false,
									id : "processMapOperateDes_id",
									autoLoad : {
										url : basePath
												+ "processMapFile.action?flowId="
												+ node.id,
										callback : function() {
											setProcessFileDivWH('processMapFileDiv');
										}

									}
								});
							}
							if (isHiddenSystem==0) {
								processTabPanel.insert(2, {
									title : i_relateRule,// 相关制度
									border : false,
									id : "processMapRelateRule_id",
									autoLoad : {
										url : basePath
												+ "jsp/processSys/processMap/ProcessMapRelateRule.jsp",
										callback : function() {
											processMapRelateRuleGrid(node.id,false);
										}
									}
								});
							}
							//if (isAdmin || isViewAdmin) {
								processTabPanel.insert(3, {
									title : i_processList,// 流程清单
									border : false,
									id : "processList_id",
									autoLoad : {
										scripts : true,
										url : basePath
												+ "jsp/processSys/processMap/processList.jsp",
										callback : function() {
											processListJson(node.id, false,false);
										}
									}
								});
								
							//}
							if (isAdmin || isViewAdmin || isDocControlPermission) {
									processTabPanel.insert(4, {
									title : i_historyInfo,// 文控信息
									border : false,
									id : "processMap_historyInfo_id",
									autoLoad : {
										url : basePath
												+ "processHistoryNew.action?flowId="
												+ node.id,
										callback : function() {
											setDocumentTemplate('processDocumentDiv');
										}
									}
								});
							}
							processTabPanel.add({
								title : i_search,// 搜索
								border : false,
								id : "processSearchTab_id",
								autoLoad : {
									url : basePath + "getAllProcessType.action",
									callback : function() {
										processGrid();
										processMapGrid();
									}
								}

							});
							// 激活流程图tab
							processTabPanel.setActiveTab("processMap_id");
						
						}

					}

				});

			}

		}

	});
	var processSysPanel = new Ext.Panel({
		renderTo : "process_id",
		border : false,
		id : 'process_index_id',
		layout : 'border',
		height : Ext.getCmp("main-panel").getInnerHeight(),
		items : [{
			region : 'west',
			title : '<div id="left_title_id" style="text-align:center;font-weight:bold">'
					+ i_processSys + '</div>',
			split : true,
			collapseMode : 'mini',
			width : 180,
			items : [processTree]

		}, {
			region : 'center',
			id : 'prcessCenter-panel',
			items : [processTabPanel],
			listeners : {
				'bodyresize' : function(p, width, height) {
					processTree.setHeight(height - 5);
					processTabPanel.setSize(width, height);
				}
			}
		}]
	});
}

/**
 * 流程地图与流程图搜索切换
 */
function selectRadioClick() {
	if (document.getElementById("processRadio").checked) {
		document.getElementById("processMapSearch").style.display = "none";
		document.getElementById("processSearch").style.display = "block";
		document.getElementById("processMapGrid_id").style.display = "none";
		document.getElementById("processGrid_id").style.display = "block";
		processGridPanel.setSize(Ext.getCmp("prcessCenter-panel")
						.getInnerWidth()
						- 2, Ext.getCmp("prcessCenter-panel").getInnerHeight()
						- 215);
	} else if (document.getElementById("processMapRadio").checked) {
		document.getElementById("processSearch").style.display = "none";
		document.getElementById("processMapSearch").style.display = "block";
		document.getElementById("processGrid_id").style.display = "none";
		document.getElementById("processMapGrid_id").style.display = "block";

		processMapGridPanel.doLayout(true);
		processMapGridPanel.setSize(Ext.getCmp("prcessCenter-panel")
						.getInnerWidth()
						- 2, Ext.getCmp("prcessCenter-panel").getInnerHeight()
						- 215);

	}
}
//初始化流程kpi显示区的高度（使用范围：从流程树节点点击流程）
function initFlowKpiWH()
{
	  if(document.getElementById('flow_kpi')!=null){
		// 屏幕可用区域
		var clientHeight = document.documentElement.clientHeight;
		//kpi显示区上边栏的高度为226	
		if(clientHeight-226>440)
		{
			
			document.getElementById('flow_kpi').style.height=clientHeight-226+'px';
		}else{
			
			document.getElementById('flow_kpi').style.height='440px';
		}
	   }
	
}
