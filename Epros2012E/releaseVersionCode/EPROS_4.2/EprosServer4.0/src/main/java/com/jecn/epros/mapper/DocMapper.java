package com.jecn.epros.mapper;

import com.jecn.epros.domain.task.TaskHistoryNew;
import com.jecn.epros.sqlprovider.DocSqlProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

public interface DocMapper {

    @SelectKey(before = true, keyProperty = "id", resultType = Long.class, statement = {"select TASK_NUM_NEW_SQUENCE.nextval from dual"})
    @InsertProvider(type = DocSqlProvider.class, method = "saveJecnTaskHistory")
    void saveJecnTaskHistoryNewOracle(TaskHistoryNew historyNew);

    @SelectKey(before = false, keyProperty = "id", resultType = Long.class, statement = {"select @@identity"})
    @InsertProvider(type = DocSqlProvider.class, method = "saveJecnTaskHistory")
    void saveJecnTaskHistoryNewSqlServer(TaskHistoryNew historyNew);

    @SelectProvider(type = DocSqlProvider.class, method = "fetchProcessMapNoPubFiles")
    List<Map<String, Object>> fetchProcessMapNoPubFiles(Long flowId);

    @SelectProvider(type = DocSqlProvider.class, method = "fetchProcessNoPubFiles")
    List<Map<String, Object>> fetchProcessNoPubFiles(Long flowId);

    @SelectProvider(type = DocSqlProvider.class, method = "fetchRuleModeNoPubFiles")
    List<Map<String, Object>> fetchRuleModeNoPubFiles(Long fileId);

    @SelectProvider(type = DocSqlProvider.class, method = "fetchRuleFileNoPubFiles")
    List<Map<String, Object>> fetchRuleFileNoPubFiles(Long relateId);


}
