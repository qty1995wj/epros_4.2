package com.jecn.epros.server.bean.download.doc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.util.JecnUtil;

public class RoleInfo implements Info {
	private IBaseDao baseDao;
	// 角色明细数据 0是角色主键ID 1角色名称 2角色职责
	private List<Object[]> roles;
	// 流程下所有的角色和岗位关系表 0是角色主键ID 1是岗位主键ID 2是岗位名称 3部门id
	private List<Object[]> roleRelatePos;
	// 流程下所有的角色和岗位组关系表 0是角色主键ID 1是岗位组主键ID 2是岗位组名称
	private List<Object[]> roleRealtePosGroup;
	// 角色与岗位关联关系表 0角色id 1角色对应的同一行活动id
	private List<Object[]> roleRelateActive;
	private Map<Long, List<Object[]>> roleToPosMap = null;
	private Map<Long, List<Object[]>> roleToPosGroupMap = null;
	// 活动对应角色
	private Map<Long, Long> activeToRoleMap = null;
	// 角色对应组织(金风专用，其它公司绝对不适用)
	private Map<Long, String> jinfengRoleToOrgMap = null;
	private Map<Long, JecnFlowOrg> orgMap = null;

	public RoleInfo(IBaseDao baseDao, List<Object[]> roles, List<Object[]> roleRelatePos,
			List<Object[]> roleRealtePosGroup, List<Object[]> roleRelateActive) {
		this.baseDao = baseDao;
		this.roles = roles;
		this.roleRelatePos = roleRelatePos;
		this.roleRealtePosGroup = roleRealtePosGroup;
		this.roleRelateActive = roleRelateActive;
	}

	@Override
	public void init() {
		this.roleToPosGroupMap = new HashMap<Long, List<Object[]>>();
		this.roleToPosMap = new HashMap<Long, List<Object[]>>();
		this.activeToRoleMap = new HashMap<Long, Long>();
		this.jinfengRoleToOrgMap = new HashMap<Long, String>();
		this.orgMap = new HashMap<Long, JecnFlowOrg>();
		if (JecnUtil.isNotEmpty(roleRealtePosGroup)) {
			for (Object[] objs : roleRealtePosGroup) {
				if (objs[0] == null) {
					continue;
				}
				Long roleId = Long.valueOf(objs[0].toString());
				List<Object[]> objList = roleToPosGroupMap.get(roleId);
				if (objList == null) {
					objList = new ArrayList<Object[]>();
				}
				objList.add(objs);
				roleToPosGroupMap.put(roleId, objList);
			}
		}
		Set<Long> orgSet = new HashSet<Long>();
		if (JecnUtil.isNotEmpty(roleRelatePos)) {
			for (Object[] objs : roleRelatePos) {
				if (objs[0] == null) {
					continue;
				}
				Long roleId = Long.valueOf(objs[0].toString());
				List<Object[]> objList = roleToPosMap.get(roleId);
				if (objList == null) {
					objList = new ArrayList<Object[]>();
				}
				objList.add(objs);
				roleToPosMap.put(roleId, objList);
				if (objs[3] != null) {
					orgSet.add(Long.valueOf(objs[3].toString()));
				}
			}
			if (!orgSet.isEmpty()) {
				orgMap = getOrgMap(orgSet);
			}

			Set<Long> roles = new HashSet<Long>();
			for (Object[] objs : roleRealtePosGroup) {
				Long roleId = Long.valueOf(objs[0].toString());
				roles.add(roleId);
			}

			for (Object[] objs : roleRelatePos) {
				Long roleId = Long.valueOf(objs[0].toString());
				roles.add(roleId);
			}

			for (Long roleId : roles) {
				String jinfengOrg = getJinfengOrg(roleId);
				jinfengRoleToOrgMap.put(roleId, jinfengOrg);
			}
		}
		if (JecnUtil.isNotEmpty(roleRelateActive)) {
			for (Object[] objs : roleRelateActive) {
				if (objs[0] != null && objs[1] != null) {
					Long activeId = Long.valueOf(objs[1].toString());
					Long roleId = Long.valueOf(objs[0].toString());
					activeToRoleMap.put(activeId, roleId);
				}
			}
		}
	}

	/**
	 * 虚拟角色或岗位组只显示名称；若是岗位则显示：‘上级部门/上级部门/岗位名称’，多个岗位则并排显示 0 组织 1是角色主键ID 2角色名称
	 * 
	 * @return
	 */
	public List<String[]> getJinfengRoles() {
		List<String[]> result = new ArrayList<String[]>();
		String[] arr = null;
		for (Object[] obj : roles) {
			if (obj[0] == null) {
				continue;
			}
			arr = new String[3];
			result.add(arr);
			arr[0] = JecnUtil.nullToEmpty(jinfengRoleToOrgMap.get(Long.valueOf(obj[0].toString())));
			arr[1] = JecnUtil.nullToEmpty(obj[1]);
			arr[2] = JecnUtil.nullToEmpty(obj[2]);
		}
		return result;
	}

	private Map<Long, JecnFlowOrg> getOrgMap(Set<Long> orgSet) {
		Map<Long, JecnFlowOrg> m = new HashMap<Long, JecnFlowOrg>();
		String hql = "from JecnFlowOrg where orgId in " + JecnCommonSql.getIdsSet(orgSet);
		List<JecnFlowOrg> orgList = baseDao.listHql(hql);
		List<JecnFlowOrg> result = new ArrayList<JecnFlowOrg>();
		result.addAll(orgList);
		Set<Long> pOrgSet = new HashSet<Long>();
		if (orgList != null && orgList.size() > 0) {
			for (JecnFlowOrg org : orgList) {
				String path = org.gettPath();
				if (StringUtils.isNotBlank(path)) {
					String[] ids = path.split("-");
					if (ids != null && ids.length > 0) {
						for (String idStr : ids) {
							Long id = Long.valueOf(idStr);
							if (!orgSet.contains(id)) {
								pOrgSet.add(id);
							}
						}
					}
				}
			}
		}
		if (pOrgSet.size() > 0) {
			hql = "from JecnFlowOrg where  orgId in " + JecnCommonSql.getIdsSet(pOrgSet);
			List<JecnFlowOrg> pOrgs = baseDao.listHql(hql);
			result.addAll(pOrgs);
		}

		for (JecnFlowOrg org : result) {
			m.put(org.getOrgId(), org);
		}
		return m;
	}

	private String getJinfengOrg(Long roleId) {
		String result = "";
		if (isContainsPos(roleId)) {// 上级部门/上级部门/岗位名称\n上级部门/上级部门/岗位名称
			List<Object[]> posAndOrgList = roleToPosMap.get(roleId);
			List<String> resultList = new ArrayList<String>();
			for (Object[] orgs : posAndOrgList) {
				resultList.add(concatOrgs(Long.valueOf(orgs[3].toString())) + orgs[2].toString());
			}
			result += JecnUtil.concat(resultList, "\n");
		}
		if (StringUtils.isNotBlank(result)) {
			result += "\n";
		}
		if (isContainsPosGroup(roleId)) {// 岗位组/岗位组
			List<Object[]> list = roleToPosGroupMap.get(roleId);
			if (list != null && !list.isEmpty()) {
				List<Object> objs = new ArrayList<Object>();
				for (Object[] obj : list) {
					objs.add(obj[2]);
				}
				result += JecnUtil.concat(objs, "/");
			}
		}
		return result;
	}

	private String concatOrgs(Long orgId) {
		List<String> names = new ArrayList<String>();
		// 部门最多显示两级
		int index = 0;
		while (true) {
			index++;
			if (index > 2) {
				break;
			}
			JecnFlowOrg org = orgMap.get(orgId);
			if (org == null) {
				break;
			}
			orgId = org.getPreOrgId();
			names.add(org.getOrgName());
		}
		StringBuffer buf = new StringBuffer();
		for (int i = names.size() - 1; i >= 0; i--) {
			buf.append(names.get(i));
			buf.append("/");
		}
		return buf.toString();
	}

	private boolean isContainsPos(Long roleId) {
		return roleToPosMap.containsKey(roleId);
	}

	private boolean isContainsPosGroup(Long roleId) {
		return roleToPosGroupMap.containsKey(roleId);
	}

	public String getJinfengOrgName(Long activeId) {
		return JecnUtil.nullToEmpty(jinfengRoleToOrgMap.get(activeToRoleMap.get(activeId)));
	}

	public String getJinfengAllPosOrg() {
		if (JecnUtil.isEmpty(roleRelatePos)) {
			return "";
		}
		Set<String> names = new HashSet<String>();
		for (Object[] objs : roleRelatePos) {
			JecnFlowOrg org = orgMap.get(Long.valueOf(objs[3].toString()));
			if (org != null) {
				names.add(org.getOrgName());
			}
		}
		return JecnUtil.concat(names, "/");
	}
}
