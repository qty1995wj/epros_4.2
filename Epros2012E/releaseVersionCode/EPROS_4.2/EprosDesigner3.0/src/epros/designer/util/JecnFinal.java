package epros.designer.util;

/**
 * 
 * RMI服务名称
 * 
 * @author Administrator
 * 
 */
public class JecnFinal {
	/** rmi 请求服务名 strat */
	public static final String JECNROLE_SN = "jecnRoleAction";// 角色
	public static final String RULEMODE_SN = "ruleModeAction";// 制度模板
	public static final String FLOWTOOL_SN = "flowToolAction";// 支持工具
	public static final String PERSON_SN = "personAction";// 人员
	public static final String ORGANIZATION_SN = "organizationAction";// 组织
	public static final String POSGROUP_SN = "positionGroupAction";// 岗位组
	public static final String PROJECT_SN = "projectAction"; // 项目
	public static final String CURPROJECT_SN = "curProjectAction"; // 主项目
	public static final String FILE_SN = "fileAction"; // 主项目
	public static final String PROCESS_SN = "processAction"; // 流程
	public static final String STANDARD_SN = "standardAction"; // 标准
	public static final String RULE_SN = "ruleAction"; // 制度
	public static final String FLOW_MODEL_SN = "processModeAction"; // 流程模板
	public static final String FLOW_TYPE_SN = "processRuleTypeAction"; // 流程类别
	public static final String DICTBASE_SN = "dictBaseAction"; // 数据字典
	public static final String PROCESS_TEMPLATE_SN = "processTemplateAction"; // 模型
	public static final String CONFIG_ACTION_SN = "configAciton";// 配置信息表
	public static final String JECNRISKACTION_SN = "jecnRiskAction"; // 风险
	/** 内控指引知识库 */
	public static final String CONTROLGUIDE_SN = "controlGuideAction"; // 内控指引知识库
	public static final String FIXEDEMAIL_SN = "fixedEmailAction";// 发布：
	// 固定人员发送邮件消息
	/** 任务审批关联查询及验证接口 */
	public static final String TASK_RECORD_ACTION = "taskRecordAction";
	/** 任务审批 提交任务 */
	public static final String CREATE_TASK_ACTION = "createTaskAction";
	/** 文件版本记录（流程、制度、地图） */
	public static final String DOC_CONTROL_ACTION = "docControlAction";
	/** rmi 请求服务名 end */

	/** 活动类别 */
	public static final String ACTIVITY_TYPE_ACTION = "activityAction";

	/** 元素库配置接口 */
	public static final String ELEMENTS_LIBRARY_ACTION = "elementsLibraryAction";

	public static final String TREE_ACTION = "treeAction";

	/** 自动编号 */
	public static final String AUTO_CODE_ACTION = "autoCodeAction";

	public static final String TREMDEFINE_ACTION = "termDefinitionAction";
	public static final String TASK_SERVICE = "taskRPCService";
}
