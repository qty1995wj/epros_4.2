package epros.designer.gui.process.activitiesProperty;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import epros.designer.util.JecnProperties;
import epros.draw.designer.JecnControlPointT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;

/***
 * 添加或编辑控制点
 * 
 * @author CHEHUANBO
 * 
 */
public class AddOrEditPointDialog extends JecnDialog {

	// 主面板
	private JecnPanel mainPanel = null;
	// 存放按钮面板
	private JecnPanel butPanel = null;
	// 新建或编辑控制点面板
	private ActiveControlPointPanel activeControlPointPanel = null;
	// 错误提示Lable
	private JLabel errorLabel;
	// 确定
	private JButton okButon = null;
	// 取消
	private JButton cancelButon = null;
	// 设置面板标题
	private String title = null;
	// 控制点bean
	private JecnControlPointT jecnControlPointT = null;
	// 判断是点击的确定按钮还是取消按钮 0:确定 1:取消
	private int okOrCanel;
	// 判断是添加还是编辑 0:添加 1：编辑
	private int addOrEditPoint = 0;

	private int selectRow;

	private JecnControlPointT controlPoint;
	private ActiveControlPointTablPanel pointTablPanel;

	public AddOrEditPointDialog(JecnControlPointT controlPoint, int addOrEditPoint,
			ActiveControlPointTablPanel pointTablPanel, int selectRow) {
		this.controlPoint = controlPoint;
		this.addOrEditPoint = addOrEditPoint;
		this.pointTablPanel = pointTablPanel;
		this.selectRow = selectRow;
		initComponents();
		initLayout();
	}

	/***
	 * 初始化所有组件
	 * 
	 */
	private void initComponents() {
		// 主面板
		mainPanel = new JecnPanel();
		// 按钮面板
		butPanel = new JecnPanel();
		// 初始化控制点面板
		activeControlPointPanel = new ActiveControlPointPanel(controlPoint, addOrEditPoint);
		errorLabel = new JLabel();
		errorLabel.setForeground(Color.red);
		// 确定按钮
		okButon = new JButton(JecnProperties.getValue("okBtn"));
		// 取消按钮
		cancelButon = new JButton(JecnProperties.getValue("cancelBtn"));
		// 设置标题
		if (addOrEditPoint == 0) {// 添加
			super.setTitle(JecnProperties.getValue("addControlPoint"));
		} else {// 编辑
			super.setTitle(JecnProperties.getValue("editControlPoint"));
		}
		// 禁用此对话框的装饰
		this.setUndecorated(true);
		// 设置此对话框是否可由用户调整大小
		this.setResizable(true);
		// 大小
		this.setSize(600, 350);
		// 窗体模式化
		this.setModal(true);
		// 居中
		this.setLocationRelativeTo(null);

		// 确定事件
		okButon.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButon();
			}
		});

		// 取消事件
		cancelButon.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				canelButon();
			}
		});

		// 关闭窗体事件
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				canelButon();
				return false;
			}
		});

	}

	/**
	 * 初始化面板布局
	 * 
	 */
	private void initLayout() {

		/** 标签间距 */
		Insets insetsLabel = new Insets(7, 11, 3, 3);
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		// 新建控制点面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insetsLabel, 0, 0);
		mainPanel.add(activeControlPointPanel, c);
		// 按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsLabel, 0, 0);

		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT)); // 按钮居右
		butPanel.add(errorLabel);
		butPanel.add(okButon);
		butPanel.add(cancelButon);
		// 将按钮面板添加到主面板中
		mainPanel.add(butPanel, c);
		this.getContentPane().add(mainPanel);

	}

	/**
	 * 确定
	 * 
	 */
	private void okButon() {
		// 验证
		if (activeControlPointPanel.isCheckButt(errorLabel)) {
			this.setOkOrCanel(0); // 设置确定标识 0：表示确定
			this.setVisible(false);
			if (addOrEditPoint == 0) {
				pointTablPanel.addTableData(activeControlPointPanel.getControlPoint());
			} else if (addOrEditPoint == 1) {
				pointTablPanel.editRefushTable(selectRow, activeControlPointPanel.getControlPoint());
			}
		}
	}

	/**
	 * 取消
	 * 
	 */
	private void canelButon() {
		this.setOkOrCanel(1); // 设置取消标识 1：表示取消
		this.setVisible(false);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getOkOrCanel() {
		return okOrCanel;
	}

	public void setOkOrCanel(int okOrCanel) {
		this.okOrCanel = okOrCanel;
	}

	public JecnControlPointT getJecnControlPointT() {
		return jecnControlPointT;
	}

	public void setJecnControlPointT(JecnControlPointT jecnControlPointT) {
		this.jecnControlPointT = jecnControlPointT;
	}

	public int getAddOrEditPoint() {
		return addOrEditPoint;
	}

	public void setAddOrEditPoint(int addOrEditPoint) {
		this.addOrEditPoint = addOrEditPoint;
	}

}
