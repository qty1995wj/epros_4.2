package com.jecn.epros.server.bean.process;

/**
 * 获得样例表（临时）
 * 
 * @author Administrator
 * 
 */
public class JecnTemplet implements java.io.Serializable {
	/** 主键ID */
	private Long id;
	/** 输出的模板主键ID */
	private Long modeFileId;
	private String modeUUID;// 输出元素索引
	/** 文件Id */
	private Long fileId;
	private String UUID;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getModeFileId() {
		return modeFileId;
	}

	public void setModeFileId(Long modeFileId) {
		this.modeFileId = modeFileId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getModeUUID() {
		return modeUUID;
	}

	public void setModeUUID(String modeUUID) {
		this.modeUUID = modeUUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String UUID) {
		this.UUID = UUID;
	}

}
