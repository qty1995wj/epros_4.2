function personImport() {
	postMatch();
	// 异步请求获取 initParams
	Ext.Ajax
			.request( {
				url : basePath + 'getImportDataType.action',
				method : 'post',
				success : function(req) {
					// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
				var jsonObj = Ext.util.JSON.decode(req.responseText);
				// 到这就可以取你想要的东东了
				// 取消息id
				importValue = jsonObj.id;

				var url = "";
				if (importValue == '3') {// 岗位匹配
					url = "matchInitParams.action";
				} else {// 2:DB人员同步,4:webService,5:sapHr,6:xml（大唐）
					url = "initParams.action";
				}
				var personImportTabPanel = new Ext.TabPanel(
						{
							id : 'personImportTab_id',
							border : false,
							activeTab : 0,
							height : Ext.getCmp("main-panel").getInnerHeight(),
							renderTo : 'peronImportMain_id',
							items : [
									{
										title : i_excelPersonImport,// EXCEL人员同步
										id : "excelPersonImport_id",
										autoLoad : {
											url : basePath
													+ "jsp/systemManager/import/excelImport.jsp",
											callback : function() {
											}
										}
									}, {
										title : i_dbPersonImport,// DB人员同步
										id : "dbPersonImport_id",
										autoLoad : {
											// url : basePath +
											// "jsp/systemManager/import/dbImport.jsp",
											url : url,
											callback : function() {
												// 初始化页面加载
											changeRadio();
										}
										}
									}, {
										title : i_macthPost,// DB 岗位匹配
										id : "postMatch_id",
										items : [ postMatchTabPanel ]
									} ],
							listeners : {
								'bodyresize' : function(p, width, height) {
									if (p.getActiveTab() != null
											&& p.getActiveTab().getId() == 'postMatch_id') {
										Ext.getCmp('postMatchTab_id').setSize(
												width - 10, height - 5);
									}

								}
							}
						});
				if (importValue == '1') {// 1：显示excel 2: 显示db 3:db和岗位匹配
					// 4:webservice
					personImportTabPanel.remove(Ext.getCmp("dbPersonImport_id"));
					personImportTabPanel.remove(Ext.getCmp("postMatch_id"));
					// personImportTabPanel.getItem("dbPersonImport_id")
				} else if (importValue == '3') {// 岗位匹配
					personImportTabPanel.remove(Ext.getCmp("excelPersonImport_id"));
				} else {// DB、webService、sapHr
					personImportTabPanel.remove(Ext.getCmp("excelPersonImport_id"));
					personImportTabPanel.remove(Ext.getCmp("postMatch_id"));
				}
				// Ext.getCmp('sysManagerCenter_id').removeAll();
				Ext.getCmp("sysManagerCenter_id").add(personImportTabPanel);
			}
			});
	sysManage_index_id = 'personImportTab_id';
}
/**
 * 去空格
 */
String.prototype.Trim = function() {
	return this.replace(/(^\s*)|(\s*$)/g, "");
};

/**
 * DB数据同步，保存参数
 */
function checkAndSaveForm() {
	var startTime = document.getElementById("startTime").value.Trim();
	var intervalDay = document.getElementById("day").value.Trim();

	// 定义时间格式 hh:MM
	if (null == startTime.match(/^(0\d{1}|1\d{1}|2[0-3]):([0-5]\d{1})$/)
			|| startTime.length > 5) {
		// 同步开始时间格式不正确！
		Ext.Msg.alert(i_tip, i_importDateFormatFalse);
		return false;
	}
	// 验证间隔天数
	if (null == intervalDay.match(/^[1-9]\d*$/)) {
		// 同步时间间最少为1天，且只能正整数！
		Ext.Msg.alert(i_tip, i_importLimitOneDay);
		return false;
	}
	var url = "";
	if (importValue == '3') {// 岗位匹配
		url = basePath + "matchSaveParams.action";
	} else {// DB人员同步 2:DB; 4:webServer； 5：sapHr；6:xml（大唐）
		url = basePath + "saveParams.action";
	}
	var radio0 = document.getElementById("radio0");
	var radio1 = document.getElementById("radio1");
	var radioValue = 0;
	if (radio0.checked) {
		radioValue = radio0.value;
	} else {
		radioValue = radio1.value;
	}

	var data = "abstractConfigBean.iaAuto=" + radioValue
			+ "&abstractConfigBean.startTime=" + startTime
			+ "&abstractConfigBean.intervalDay=" + intervalDay;
	// 异步请求获取 initParams
	Ext.Ajax.request( {
		url : url,
		params : data,
		method : 'post',
		url : url,
		success : function(req) {
			// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		if (req.responseText == 'true') {
			//保存配置数据成功！
			Ext.Msg.alert(i_tip, i_saveMatchDataSucess);
			return;
		} else {
			//保存配置数据异常！
			Ext.Msg.alert(i_tip, i_saveMatchDataError);
			return ;
		}
}
	});
}
/**
 * 短信按钮切换
 * 
 */
function changeRadio() { // 传入一个对象
	var oRadioValue = document.getElementById("iaRadio").value;
	var radio0 = document.getElementById("radio0");
	var radio1 = document.getElementById("radio1");
	if (oRadioValue == 0) {
		radio0.checked = true;
		radio1.checked = false;
	} else if (oRadioValue == 1) {
		radio0.checked = false;
		radio1.checked = true;
	}
}

/**
 * 同步数据
 */
function doImport(loadingImage, submitLoading) {
	document.getElementById("dbImportTip").style.display = "";
	// 提交时按钮状态 taskCommon.js 方法 loadingSubmit(loadingImage, submitLoading)
	loadingSubmit(loadingImage, submitLoading);
	var url = "";
	if (importValue == '3') {// 岗位匹配
		url = basePath + "matchImportData.action";
	} else {// DB人员同步 2:DB; 4:webServer； 5：sapHr；6:xml（大唐）
		url = basePath + "importData.action";
	}
	var importType = document.getElementById("dbImportType").value;
	var data = "importType=" + importType;
	Ext.Ajax.request( {
		url : url,
		method : 'post',
		params : data,
		timeout : 6000000,
		success : function(req) {
			// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		if (req.responseText == 'true') {
			Ext.Msg.alert(i_tip, i_submitSucces);
		} else if (req.responseText == 'serverCountError') {
			// "人员同步人数超出上限！"
		Ext.Msg.alert(i_tip, i_Beyondthelimit);
	} else {
		Ext.Msg.alert(i_tip, i_submitFalse);
	}
	document.getElementById("dbImportTip").style.display = "none";
	hiddeningSubmit('loadingImage1', 'submitLoading1');
},
failure : function(req) {
	// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		// 提交失败！
		Ext.Msg.alert(i_tip, i_submitFalse);
		document.getElementById("dbImportTip").style.display = "none";
		hiddeningSubmit('loadingImage1', 'submitLoading1');
	}

	});
}

function hiddeningSubmit(loadingImage, submitLoading) {
	if (loadingImage == null || submitLoading == null) {
		return;
	}
	var loadImage = 'loadingImage';
	var submitLoad = 'submitLoading';
	for ( var i = 0; i < 20; i++) {// 12个操作状态 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回
		// 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
		// * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交；11：编辑
		var temploadingImage = loadImage + i;
		var tempsubmitLoading = submitLoad + i;
		if (loadExists(temploadingImage, tempsubmitLoading)) {// 存在节点
			document.getElementById(temploadingImage).style.display = "";
		}
	}
	document.getElementById(submitLoading).style.display = "none";
}

/**
 * Excel数据同步
 * 
 */
function excelSubmit() {
	var excelFilePath = document.getElementById("excelFilePath").value;
	if (excelFilePath == '') {
		// 请选择需要导入的Excel文件！
		Ext.Msg.alert(i_tip, i_needImpordExcelFile);
		return;
	}
	// var setUserFilePath = document.getElementById("setUserFilePath").value;

	// 检查文件格式(.xls)
	if (!/\.xls$/.test(excelFilePath)) {
		// 请选择正确的EXCEL文件(后缀为xls)！
		Ext.Msg.alert(i_tip, i_selectRightExcelFile);
		return;
	}
	if (excelFilePath != '') {
		document.getElementById("setUserFilePath").value = excelFilePath;
	}
	document.forms[0].submit();
}

function downErrorExcel() {
	var chForm = document.form1;
	chForm.action = "userErrorDataFileDownAction.do";
	chForm.submit();
}
var importValue = "";
// 异步请求调用的方法
/**
 * 人员错误信息导出
 * 
 */
function userErrorDataFile() {
	Ext.Ajax.request( {
		url : "userErrorDataFileDownAction.action",
		method : 'post',
		success : function(req) {
			// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		Ext.Msg.alert(i_tip, req.responseText);
	},
	failure : function(req) {
		// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		// 获取人员信息异常！
		Ext.Msg.alert(i_tip, i_peopleMessageError);
	}

	});
}

/**
 * 人员信息导出
 * 
 */
function downUserDataExcel() {
	Ext.Ajax.request( {
		url : "downUserDataExcel.action",
		method : 'post',
		success : function(req) {
			// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		Ext.Msg.alert(i_tip, req.responseText);
	},
	failure : function(req) {
		// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		Ext.Msg.alert(i_tip, i_peopleMessageError);
	}

	});
}

/**
 * 人员模板下载
 * 
 */
function userModelFileDown() {
	Ext.Ajax.request( {
		url : "userModelFileDownAction.action",
		method : 'post',
		success : function(req) {
			// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		Ext.Msg.alert(i_tip, req.responseText);
	},
	failure : function(req) {
		// 如果成功则使用Ext将JSON字符串转换为JavaScript对象
		Ext.Msg.alert(i_tip, i_peopleMessageError);
	}

	});
}
