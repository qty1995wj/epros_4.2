package epros.designer.gui.system;

import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.ProcessChooseDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class ResProcessSelectCommon extends SingleSelectCommon {
	private static Logger log = Logger.getLogger(ResProcessSelectCommon.class);
	public ResProcessSelectCommon(int rows, JecnPanel infoPanel, Insets insets, Long flowId,String flowName,
			JecnDialog jecnDialog, String labName, boolean isRequest) {
		super(rows, infoPanel, insets, new JecnTreeBean(flowId, flowName, TreeNodeType.process), jecnDialog, labName, isRequest);
	}

	@Override
	protected List<JecnTreeBean> searchName(String name, Long projectId) {
		try {
			return ConnectionPool.getProcessAction().searchFlowByName(name, JecnConstants.projectId, 2,
					JecnDesignerCommon.isSecondAdmin() ? JecnConstants.getUserId() : null);
		} catch (Exception e) {
			log.error("ResProcessSelectCommon searchName is error", e);
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	protected void selectButPerformed(List<JecnTreeBean> list, JSearchTextField jecnField, Long id) {
		ProcessChooseDialog processChooseDialog = new ProcessChooseDialog(list, 13, 2);
		processChooseDialog.setSelectMutil(false);
		processChooseDialog.setVisible(true);
		if (processChooseDialog.isOperation()) {
			if (list.size() > 0) {
				JecnTreeBean jecnTreeBean = list.get(0);
				jecnField.setText(jecnTreeBean.getName());
			} else {
				jecnField.setText("");
			}
		}
	}

}
