package com.jecn.epros.bean;

import java.util.List;

/**
 * 文控主导审批
 * @author user
 *
 */
public class TaskControlSubmitParam extends TaskChangeInfoApproveParam {

	/** 页面提交新增审批人 */
	private List<AuditPeopleBean> listAuditPeople;
	
	
	public List<AuditPeopleBean> getListAuditPeople() {
		return listAuditPeople;
	}
	public void setListAuditPeople(List<AuditPeopleBean> listAuditPeople) {
		this.listAuditPeople = listAuditPeople;
	}
	
	
}
