package com.jecn.epros.server.download.wordxml.yage;

import java.util.ArrayList;
import java.util.List;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.WordGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.FlowDriverBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.download.wordxml.EnumClass.FLOW_DERVER_RULE;
import com.jecn.epros.server.util.JecnResourceUtil;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 雅阁操作说明模版
 * 
 * @author hyl
 * 
 */
public class YGProcessModel extends ProcessFileModel {
	/*** 段落行距 1.5倍行距 *****/
	private ParagraphLineRule vLine2 = new ParagraphLineRule(1.5F, LineRuleType.AUTO);
	/*** 段落行距最小值12磅 *****/
	private ParagraphLineRule leastVline12 = new ParagraphLineRule(12F, LineRuleType.AT_LEAST);

	public YGProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(false);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(17.38F);
		getDocProperty().setNewRowHeight(0.6F);
		getDocProperty().setFlowChartScaleValue(5.8F);
		setDocStyle("、", textTitleStyle());
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(processFileItem, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, new ParagraphLineRule(20F, LineRuleType.EXACT));
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);
		float[] width = null;
		if (JecnContants.showPosNameBox) {
			width = new float[] { 3.5F, 3.5F, 7.5F };
		} else if (!JecnContants.showPosNameBox) {
			width = new float[] { 5.0F, 9.5F };
		}
		List<String[]> rowData2 = new ArrayList<String[]>();
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = JecnUtil.getValue("positionName");
		// 角色职责
		String roleResponsibility = JecnUtil.getValue("responsibilities");
		if (JecnContants.showPosNameBox) {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility, positionName });
		} else {
			// 标题行
			rowData2.add(0, new String[] { roleName, roleResponsibility });
		}
		for (String[] str : rowData) {
			if (JecnContants.showPosNameBox) {
				rowData2.add(new String[] { str[0], str[2], str[1] });
			} else {
				rowData2.add(new String[] { str[0], str[2] });
			}
		}
		Table tbl = createTableItem(processFileItem, width, rowData2);
		tbl.setRowStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow(), getDocProperty()
				.isTblTitleCrossPageBreaks());
	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {
		createTitle(processFileItem);
		// 活动编号
		String activityNumber = JecnUtil.getValue("activitynumbers");
		// 执行角色s
		String executiveRole = JecnUtil.getValue("executiveRole");
		// 活动名称
		String eventTitle = JecnUtil.getValue("activityTitle");
		// 活动说明
		String activityDescription = "具体活动说明";
		String input = "输入";
		String output = "输出";
		float[] width = new float[] { 1.19F, 2.5F, 2.5F, 5.25F, 2F, 2.04F };
		List<Object[]> newRowData = new ArrayList<Object[]>();
		// 标题数组
		newRowData.add(new Object[] { activityNumber, executiveRole, eventTitle, activityDescription, input, output });
		// 添加数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出
		for (Object[] activeData : processDownloadBean.getAllActivityShowList()) {
			newRowData.add(new Object[] { activeData[0], activeData[1], activeData[2], activeData[3], activeData[4],
					activeData[5] });
		}
		Table tbl = createTableItem(processFileItem, width, JecnWordUtil.obj2String(newRowData));
		TableBean tableBean = new TableBean();
		tableBean.setBorder(tbl.getTableBean().getBorderBottom().getBorderWidth());
		tableBean.setTableAlign(Align.left);
		tableBean.setTableLeftInd(0F);
		tbl.setTableBean(tableBean);

		ParagraphBean titleBean = tblTitleStyle();
		titleBean.getFontBean().setFontSize(Constants.xiaowu);
		titleBean.setAlign(Align.center);

		ParagraphBean contentBean = tblContentStyle();
		contentBean.getFontBean().setFontSize(Constants.xiaowu);

		// 编号居中
		ParagraphBean bhBean = tblContentStyle();
		bhBean.getFontBean().setFontSize(Constants.xiaowu);
		bhBean.setAlign(Align.center);
		for (int i = 0; i < tbl.getRowCount(); i++) {
			if (i == 0) {
				tbl.setRowStyle(0, titleBean, getDocProperty().isTblTitleShadow(), getDocProperty()
						.isTblTitleCrossPageBreaks());
				continue;
			}
			tbl.setRowStyle(i, contentBean);
		}
		tbl.setCellStyle(0, bhBean);

	}

	/**
	 * 流程关键测评指标
	 * 
	 * @param _count
	 * @param name
	 * @param flowKpiList
	 */
	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		// KPI配置bean
		List<JecnConfigItemBean> configItemBeanList = processDownloadBean.getConfigKpiItemBean();
		createTitle(processFileItem);
		// 是否创建表格风格段落
		boolean flagTab = oldRowData.size() > 1 ? true : false;
		ParagraphBean tPbean = getDocProperty().getTextTitleStyle();

		ParagraphBean pBean = new ParagraphBean(tPbean.getFontBean());
		pBean.getFontBean().setB(false);
		if (tPbean.getParagraphIndBean() != null) {
			pBean.setInd(tPbean.getParagraphIndBean().getLeft(), tPbean.getParagraphIndBean().getRight(), tPbean
					.getParagraphIndBean().getHanging(), 0);
		}

		// 表格内容
		int count = 0;
		for (String[] row : oldRowData) { // 对应一条配置数据
			List<String[]> tableData = new ArrayList<String[]>();// 一个表格数据
			String[] rowContent = null;// 表格一行数据
			// boolean isExitKpiName = false;
			if (flagTab) {
				String number = "" + numberMark + "." + ++count;
				processFileItem.getBelongTo().createParagraph(number, pBean);
			}
			for (JecnConfigItemBean config : configItemBeanList) {
				if ("0".equals(config.getValue())) {
					continue;
				}
				/*
				 * if (!isExitKpiName) { tableData.add(new String[] { "KPI名称",
				 * row[0] }); isExitKpiName = true; }
				 */
				rowContent = new String[2];
				rowContent[0] = config.getName();
				rowContent[1] = getKpiContent(row, config.getMark());
				tableData.add(rowContent);
			}
			Table tbl = createTableItem(processFileItem, new float[] { 4.25F, 11.15F }, tableData);
			tbl.setCellStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow());

		}

	}

	/**
	 * * 0 KPI的名称 1 类型 2 目标值 3 kpi定义 4 kpi统计方法 5 数据统计时间频率 6 KIP值单位名称 7 数据获取方式 8
	 * 相关度 9 指标来源 10 数据提供者名称 11 支撑的一级指标内容 12 目的 13 测量点 14 统计周期 15 说明
	 */
	private String getKpiContent(String[] row, String mark) {
		if ("kpiName".equals(mark)) {// KPI名称
			return row[0];
		} else if ("kpiType".equals(mark)) {// KPI类型
			return row[1];
		} else if ("kpiUtilName".equals(mark)) {// KPI单位名称
			return JecnResourceUtil.getKpiVertical(Integer.valueOf(row[6]));
		} else if ("kpiTargetValue".equals(mark)) {// KPI目标值
			return row[2];
		} else if ("kpiTimeAndFrequency".equals(mark)) {// 数据统计时间频率
			return JecnResourceUtil.getKpiHorizontal(Integer.valueOf(row[5]));
		} else if ("kpiDefined".equals(mark)) {// KPI定义
			return row[3];
		} else if ("kpiDesignFormulas".equals(mark)) {// 流程KPI计算方式
			return row[4];
		} else if ("kpiDataProvider".equals(mark)) {// 数据提供者
			return row[10];
		} else if ("kpiDataAccess".equals(mark)) {// 数据获取方式
			return JecnResourceUtil.getKpiDataMethod(Integer.valueOf(row[7]));
		} else if ("kpiIdSystem".equals(mark)) {// IT 系统
			return row[16];
		} else if ("kpiSourceIndex".equals(mark)) {// 指标来源
			return JecnResourceUtil.getKpiTargetType(Integer.valueOf(row[9]));
		} else if ("kpiRrelevancy".equals(mark)) {// 相关度
			return JecnResourceUtil.getKpiRelevance(Integer.valueOf(row[8]));
		} else if ("kpiSupportLevelIndicator".equals(mark)) {// 支持的一级指标
			return row[11];
		} else if ("kpiPurpose".equals(mark)) {// 设置目的
			return row[12];
		} else if ("kpiPoint".equals(mark)) {// 测量点
			return row[13];
		} else if ("kpiPeriod".equals(mark)) {// 统计周期
			return row[14];
		} else if ("kpiExplain".equals(mark)) {// 说明
			return row[15];
		}
		return "";
	}

	/****
	 * 输入和输出
	 * 
	 * @param titleName
	 * @param inputorout
	 */
	protected void a32(ProcessFileItem processFileItem, String[] input, String[] output) {
		createTitle(processFileItem);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "输入", processDownloadBean.getFlowInput() });
		rowData.add(new String[] { "输出", processDownloadBean.getFlowOutput() });
		float[] width = new float[] { 4.75F, 8.6F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setCellStyle(0, getDocProperty().getTblTitleStyle(), getDocProperty().isTblTitleShadow());
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2F, 2F, 2F, 2F, 0.83F, 1.17F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = getSectBean();
		titleSect.setSectBean(sectBean);
		addTitleSectContent(titleSect);
	}

	/**
	 * 添加首页内容
	 * 
	 * @param titleSect
	 */
	private void addTitleSectContent(Sect titleSect) {

		PositionBean positionBean = new PositionBean(PositionType.absolute, 40, 0F, 176F, 0F);
		Image logoImage = (Image) getLogoImage(4.63F, 3.76F);
		logoImage.setPositionBean(positionBean);
		titleSect.createParagraph(logoImage);

		titleSect.createMutilEmptyParagraph(16);

		/***** 默认表格样式 **********/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0F, 0F, 0F, 0F));
		ParagraphBean cellBean = new ParagraphBean(Align.center, "宋体", Constants.sihao, true);

		// 为了简单表格将由四个表格组合而成
		Table nameTable = titleSect.createTable(tabBean, new float[] { 2.69F, 14.69F });
		List<String[]> titleLine = new ArrayList<String[]>();
		titleLine.add(new String[] { "文件名称", processDownloadBean.getFlowName() });
		nameTable.createTableRow(titleLine, cellBean);
		nameTable.setCellValignAndHeight(Valign.center, 0.8F);

		Table versionTable = titleSect.createTable(tabBean, new float[] { 2.69F, 5.15F, 2.21F, 7.33F });
		List<String[]> versionLine = new ArrayList<String[]>();
		String pubTime = getPubTime();
		versionLine.add(new String[] { "版    本", processDownloadBean.getFlowVersion(), "生效日期", pubTime });
		versionTable.createTableRow(versionLine, cellBean);
		versionTable.setCellValignAndHeight(Valign.center, 0.8F);

		// 级别中应该是不显示t_level=0的以及自己本身
		String L1 = "";
		String L2 = "";
		String L3 = "";
		String L4 = "";
		String L5 = "";
		List<Object[]> parents = processDownloadBean.getParents();
		int parentLen = parents.size();
		if (parentLen > 2) {
			L1 = parents.get(1)[1].toString();
		}
		if (parentLen > 3) {
			L2 = parents.get(2)[1].toString();
		}
		if (parentLen > 4) {
			L3 = parents.get(3)[1].toString();
		}
		if (parentLen > 5) {
			L4 = parents.get(4)[1].toString();
		}
		if (parentLen > 6) {
			L5 = parents.get(5)[1].toString();
		}

		ProcessAttributeBean responFlow = processDownloadBean.getResponFlow();
		String dutyUserName = null;
		String guardianName = null;
		String fictionPeopleName = null;
		String dutyOrg = null;
		if (responFlow != null) {
			dutyUserName = responFlow.getDutyUserName();
			guardianName = responFlow.getGuardianName();
			fictionPeopleName = responFlow.getFictionPeopleName();
			dutyOrg = responFlow.getDutyOrgName();
		}
		Table codeTable = titleSect.createTable(tabBean, new float[] { 2.69F, 5.15F, 0.93F, 1.28F, 7.33F });
		List<String[]> codeData = new ArrayList<String[]>();
		codeData.add(new String[] { "流程编号", processDownloadBean.getFlowInputNum(), "流程层级", "L1", L1 });
		codeData.add(new String[] { "责 任 人", nullToEmpty(dutyUserName), "", "L2", L2 });
		codeData.add(new String[] { "监 护 人", nullToEmpty(guardianName), "", "L3", L3 });
		codeData.add(new String[] { "拟 制 人", nullToEmpty(fictionPeopleName), "", "L4", L4 });
		codeData.add(new String[] { "责任部门", nullToEmpty(dutyOrg), "", "L5", L5 });
		codeTable.createTableRow(codeData, cellBean);
		// 合并流程架构单元格
		codeTable.getRow(0).getCell(2).setVmergeBeg(true);
		codeTable.getRow(0).getCell(2).setRowspan(5);
		codeTable.getRow(4).getCell(2).setVmerge(true);
		codeTable.setCellValignAndHeight(Valign.center, 0.8F);

		Table scopeTable = titleSect.createTable(tabBean, new float[] { 2.69F, 14.69F });
		List<String[]> scopeLine = new ArrayList<String[]>();
		scopeLine.add(new String[] { "适用范围", processDownloadBean.getApplicability() });
		scopeTable.createTableRow(scopeLine, cellBean);
		scopeTable.setCellValignAndHeight(Valign.center, 2.0F);

		ParagraphBean titleBean = tblContentStyle();
		titleBean.setAlign(Align.left);
		titleBean.getFontBean().setB(true);
		titleBean.getFontBean().setFontSize(Constants.sihao);
		List<Paragraph> paragraphs = scopeTable.getRow(0).getCell(1).getParagraphs();
		for (Paragraph p : paragraphs) {
			p.setParagraphBean(titleBean);
		}

		titleSect.createMutilEmptyParagraph(12);

		titleSect.createParagraph("深圳市太格尔电源科技有限公司", new ParagraphBean(Align.center, "宋体", Constants.sanhao, true));
	}

	// codeData.add(new String[] { "拟 制 人",
	// processDownloadBean.getResponFlow().getFictionPeopleName(), "", "L2", L2
	// });
	// String approvePeople = getApprovePeople();
	// codeData.add(new String[] { "审 核 人", approvePeople, "", "L3", L3 });
	// String lastApprovePeople = getLastApprovePeople();
	// codeData.add(new String[] { "批 准 人", lastApprovePeople, "", "L4", L4 });

	private void createHistoryTable(Sect sect) {

		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0F, 0F, 0F, 0F));
		sect.createMutilEmptyParagraph(1);
		sect.createParagraph("文件拟制/修订记录", new ParagraphBean(Align.center, "宋体", Constants.sihao, false));
		sect.createMutilEmptyParagraph(2);
		Table historyTable = sect.createTable(tabBean, new float[] { 1.46F, 3.18F, 2.86F, 7.5F, 2.28F });
		historyTable.createTableRow(new String[] { "版本", "拟制/修订责任人", "拟制/修订日期", "修订内容及理由", "批准人" }, new ParagraphBean(
				Align.center, "宋体", Constants.wuhao, true));
		List<String[]> tableData = new ArrayList<String[]>();
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		if (historys.size() == 0) {
			tableData.add(new String[] { "", "", "", "", "" });
		} else {
			for (JecnTaskHistoryNew history : historys) {
				String approveName = "";
				for (int i = history.getListJecnTaskHistoryFollow().size() - 1; i >= 0; i--) {
					JecnTaskHistoryFollow approve = history.getListJecnTaskHistoryFollow().get(i);
					if (approve.getName() != null && !"".equals(approve.getName())) {
						approveName = approve.getName();
						break;
					}
				}
				tableData.add(new String[] { history.getVersionId(), history.getDraftPerson(),
						JecnUtil.DateToStr(history.getPublishDate(), "yyyy-MM-dd"), history.getModifyExplain(),
						approveName });
			}
		}
		historyTable.createTableRow(tableData, new ParagraphBean(Align.center, "宋体", Constants.wuhao, false));
		historyTable.setCellValignAndHeight(Valign.center, 0.6F);
	}

	private String getLastApprovePeople() {

		if (processDownloadBean.getHistorys().size() == 0) {
			return "";
		}
		JecnTaskHistoryNew jecnTaskHistoryNew = processDownloadBean.getHistorys().get(0);
		List<JecnTaskHistoryFollow> approvePeoples = jecnTaskHistoryNew.getListJecnTaskHistoryFollow();
		JecnTaskHistoryFollow jecnTaskHistoryFollow = approvePeoples.get(approvePeoples.size() - 1);
		return jecnTaskHistoryFollow.getName();
	}

	private String getApprovePeople() {
		if (processDownloadBean.getHistorys().size() == 0) {
			return "";
		}
		JecnTaskHistoryNew jecnTaskHistoryNew = processDownloadBean.getHistorys().get(0);
		StringBuffer buf = new StringBuffer();
		List<JecnTaskHistoryFollow> approvePeoples = jecnTaskHistoryNew.getListJecnTaskHistoryFollow();
		for (int i = 0; i < approvePeoples.size() - 1; i++) {
			if (approvePeoples.get(i).getName() == null || !"".equals(approvePeoples.get(i).getName())) {
				continue;
			}
			if (i == 0) {
				buf.append(approvePeoples.get(i).getName());
			} else {
				buf.append("/" + approvePeoples.get(i).getName());
			}
		}
		return buf.toString();
	}

	private String getPubTime() {
		List<JecnTaskHistoryNew> historys = processDownloadBean.getHistorys();
		for (JecnTaskHistoryNew history : historys) {
			if (history != null) {
				return JecnUtil.DateToStr(history.getPublishDate(), "yyyy-MM-dd");
			}
		}
		return "";
	}

	/**
	 * 流程图前面的数据
	 */
	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		firstSect.setSectBean(sectBean);
		createCommfhdr(firstSect);
		createHistoryTable(firstSect);
		firstSect.addParagraph(new Paragraph(true));

	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setSize(29.7F, 21F);
		flowChartSect.setSectBean(sectBean);
		createCharfhdr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		// 添加页眉
		createCommfhdr(secondSect);
	}

	/**
	 * 表格内容段落样式 宋体 五号 最小值12磅值
	 */
	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.both, "宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, leastVline12);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.94 厘米
		tblStyle.setTableLeftInd(0F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0.19F, 0, 0.19F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

	/**
	 * 表格标题样式 宋体 五号 最小值12磅值
	 */
	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitleStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		tblTitleStyle.setSpace(0f, 0f, leastVline12);
		return tblTitleStyle;
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, float left, float center, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaowu, false);
		Header header = sect.createHeader(HeaderOrFooterType.odd);

		TableBean tableBean = new TableBean();
		tableBean.setBorderBottom(1F);
		tableBean.setTableLeftInd(0.2F);
		Table table = header.createTable(tableBean, new float[] { left, center, right });
		table.createTableRow(new String[] { "", processDownloadBean.getFlowName() + "流程说明", "文档密级：" + processDownloadBean.getConfidentialityLevelName() },
				pBean);

		ParagraphBean paragraphBean = new ParagraphBean(Align.right, "宋体", Constants.xiaowu, false);
		table.getRow(0).getCell(2).getParagraph(0).setParagraphBean(paragraphBean);
		table.setCellValignAndHeight(Valign.bottom, 0.8F);

		WordGraph image = super.getImage(1.96F, 0.61F, "wordLogo1.png");
		table.getRow(0).getCell(0).addParagraph(new Paragraph(image));
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommfdr(Sect sect, float left, float center, float right) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.xiaowu, false);
		Footer header = sect.createFooter(HeaderOrFooterType.odd);

		TableBean tableBean = new TableBean();
		tableBean.setBorder(1F);
		tableBean.setTableLeftInd(0.2F);
		// 3F,11F,3F
		Table table = header.createTable(tableBean, new float[] { left, center, right });
		table.createTableRow(new String[] { nullToEmpty(processDownloadBean.getFlowInputNum()), "太格尔机密，未经许可不得扩散", "" },
				pBean);

		table.getRow(0).getCell(0).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.left, "宋体", Constants.xiaowu, false));
		table.getRow(0).getCell(2).getParagraph(0).setParagraphBean(
				new ParagraphBean(Align.right, "宋体", Constants.xiaowu, false));
		Paragraph pageParagraph = table.getRow(0).getCell(2).getParagraph(0);
		pageParagraph.appendTextRun("第");
		pageParagraph.appendCurPageRun();
		pageParagraph.appendTextRun("页, 共");
		pageParagraph.appendTotalPagesRun();
		pageParagraph.appendTextRun("页");
		table.setCellValignAndHeight(Valign.top, 0.6F);
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 3F, 11F, 3F);
		createCommfdr(sect, 6F, 6F, 5F);
	}

	/***
	 * 创建流程图页眉页脚
	 * 
	 * @param sect
	 */
	private void createCharfhdr(Sect sect) {
		createCommhdr(sect, 3F, 19.7F, 3F);
		createCommfdr(sect, 3F, 19.7F, 3F);
	}

	/**
	 * 标题内容属性
	 */
	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体", Constants.xiaosi, true);
		textContentStyle.setInd(-0.5F, 0, 0, 0.75F);
		textContentStyle.setSpace(0f, 0f, vLine2);
		return textContentStyle;
	}

	/**
	 * 标题样式 宋体 小四 加粗 1.5倍数行距
	 */
	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("宋体", Constants.xiaosi, true);
		textTitleStyle.setSpace(0f, 0f, vLine2);
		// 悬挂缩进默认0.74 会导致序号 超过10了增加两倍故设置0.9
		textTitleStyle.setInd(-0.5F, 0, 0.9F, 0);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

}
