package wordxml.element.bean.common;

import wordxml.constant.Constants.PositionType;

/**
 * 图片位置属性bean 一般情况是不使用
 * 
 * @author admin
 * 
 */
// position:absolute;margin-left:-9pt;margin-top:-2.95pt;width:97.4pt;height:28.15pt;z-index:3;mso-wrap-style:none
public class PositionBean {
	private PositionType positionType = PositionType.absolute;
	/**** 距离顶部的距离 单位 pt 磅值 *******/
	private float marginTop;
	/**** 距离左边的距离 单位 pt 磅值 *******/
	private float marginLeft;
	/**** 距离底部的距离 单位 pt 磅值 *******/
	private float marginBottom;
	/**** 距离右边的距离 单位 pt 磅值 *******/
	private float marginRight;
	/***** 垂直位置 ***********/
	private int zIndex = 1;

	public PositionBean() {
	}

	public PositionBean(PositionType positionType, float marginTop,
			float marginBottom, float marginLeft, float marginRight) {

		this.positionType = positionType;
		this.marginTop = marginTop;
		this.marginLeft = marginLeft;
		this.marginBottom = marginBottom;
		this.marginRight = marginRight;
	}

	public float getMarginTop() {
		return marginTop;
	}

	public void setMarginTop(float marginTop) {
		this.marginTop = marginTop;
	}

	public float getMarginLeft() {
		return marginLeft;
	}

	public void setMarginLeft(float marginLeft) {
		this.marginLeft = marginLeft;
	}

	public float getMarginBottom() {
		return marginBottom;
	}

	public void setMarginBottom(float marginBottom) {
		this.marginBottom = marginBottom;
	}

	public float getMarginRight() {
		return marginRight;
	}

	public void setMarginRight(float marginRight) {
		this.marginRight = marginRight;
	}

	public void setMarginRight(int marginRight) {
		this.marginRight = marginRight;
	}

	public PositionType getPositionType() {
		return positionType;
	}

	public void setPositionType(PositionType positionType) {
		this.positionType = positionType;
	}

	public int getzIndex() {
		return zIndex;
	}

	public void setzIndex(int zIndex) {
		this.zIndex = zIndex;
	}

}
