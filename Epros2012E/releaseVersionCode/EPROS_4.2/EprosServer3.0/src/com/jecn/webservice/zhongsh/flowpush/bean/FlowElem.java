package com.jecn.webservice.zhongsh.flowpush.bean;

public class FlowElem {
	private Long figureId;// 主键ID
	private String guid;// uuid
	private String figureType;// 元素类型
	private String figureText;// 元素名称
	private Long startFigure;// 连接线输出的图像元素的FigureId
	private Long endFigure;// 连接线输入的图像元素的FigureId
	/** 数据库读取坐标值 */
	private Long poLongx;// 图像元素开始点的X
	private Long poLongy;// 图像元素开始点的Y

	/** state元素获取前，EPROS转换的坐标显示 */
	private Long showPoLongx;
	private Long showPoLongy;

	private Long width = 60L;// 图像元素的宽
	private Long height = 60L;// 图像元素的高
	private String activityId;// 活动编号
	private String activityShow;// 活动说明
	private String relatedRoleText;// 关联角色名称
	private Long relatedRoleId;// 关联角色id
	private Double revolve;// 旋转

	private String startGuid;
	private String endGuid;
	private String type;

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getFigureType() {
		return figureType;
	}

	public void setFigureType(String figureType) {
		this.figureType = figureType;
	}

	public String getFigureText() {
		return figureText;
	}

	public void setFigureText(String figureText) {
		this.figureText = figureText;
	}

	public Long getStartFigure() {
		return startFigure;
	}

	public void setStartFigure(Long startFigure) {
		this.startFigure = startFigure;
	}

	public Long getEndFigure() {
		return endFigure;
	}

	public void setEndFigure(Long endFigure) {
		this.endFigure = endFigure;
	}

	public Long getPoLongx() {
		return poLongx;
	}

	public void setPoLongx(Long poLongx) {
		this.poLongx = poLongx;
	}

	public Long getPoLongy() {
		return poLongy;
	}

	public void setPoLongy(Long poLongy) {
		this.poLongy = poLongy;
	}

	public Long getWidth() {
		return width;
	}

	public void setWidth(Long width) {
		this.width = width;
	}

	public Long getHeight() {
		return height;
	}

	public void setHeight(Long height) {
		this.height = height;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getActivityShow() {
		return activityShow;
	}

	public void setActivityShow(String activityShow) {
		this.activityShow = activityShow;
	}

	public String getRelatedRoleText() {
		return relatedRoleText;
	}

	public void setRelatedRoleText(String relatedRoleText) {
		this.relatedRoleText = relatedRoleText;
	}

	public Long getRelatedRoleId() {
		return relatedRoleId;
	}

	public void setRelatedRoleId(Long relatedRoleId) {
		this.relatedRoleId = relatedRoleId;
	}

	public String getStartGuid() {
		return startGuid;
	}

	public void setStartGuid(String startGuid) {
		this.startGuid = startGuid;
	}

	public String getEndGuid() {
		return endGuid;
	}

	public void setEndGuid(String endGuid) {
		this.endGuid = endGuid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getRevolve() {
		return revolve;
	}

	public void setRevolve(Double revolve) {
		this.revolve = revolve;
	}

	public Long getShowPoLongx() {
		return showPoLongx;
	}

	public void setShowPoLongx(Long showPoLongx) {
		this.showPoLongx = showPoLongx;
	}

	public Long getShowPoLongy() {
		return showPoLongy;
	}

	public void setShowPoLongy(Long showPoLongy) {
		this.showPoLongy = showPoLongy;
	}
}
