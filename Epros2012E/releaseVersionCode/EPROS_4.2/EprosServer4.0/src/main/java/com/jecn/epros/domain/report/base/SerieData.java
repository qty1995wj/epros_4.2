package com.jecn.epros.domain.report.base;

import java.util.ArrayList;
import java.util.List;

/**
 * 对应折线Echart series数组内的一个数据
 */
public class SerieData {

    private String id;
    private String name;
    private List<String> data = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
