package com.jecn.epros.server.service.file.history;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;

public class JecnCommonFileHistory {

	/**
	 * 文件表文控 jecn_file_h
	 * 
	 * @param relateId
	 *            关联ID
	 * @param historyId
	 *            文控ID
	 * @return
	 */
	private static String getJecnFileHistorySql(Long relateId, Long historyId) {
		return "insert into jecn_file_h"
				+ "(FILE_ID, FILE_NAME, "
				+ "PEOPLE_ID, PER_FILE_ID,CREATE_TIME,"
				+ "UPDATE_PERSON_ID,UPDATE_TIME, IS_DIR, "
				+ "ORG_ID, DOC_ID, IS_PUBLIC, FLOW_ID, SORT_ID, "
				+ "SAVETYPE, PROJECT_ID,FILE_PATH,VERSION_ID,"
				+ "HISTORY_ID,DEL_STATE,T_PATH,T_LEVEL,PUB_TIME,HIDE,VIEW_SORT,CONFIDENTIALITY_LEVEL,GUID)"
				+ " select FILE_ID, FILE_NAME, PEOPLE_ID, PER_FILE_ID,CREATE_TIME, UPDATE_PERSON_ID,UPDATE_TIME, IS_DIR, "
				+ "ORG_ID, DOC_ID, IS_PUBLIC, FLOW_ID, SORT_ID, SAVETYPE, PROJECT_ID,FILE_PATH,VERSION_ID, "+historyId+",DEL"
				+ "_STATE,T_PATH,T_LEVEL,PUB_TIME,HIDE,VIEW_SORT,CONFIDENTIALITY_LEVEL, "
				+ JecnCommonSql.getSqlGUID()
				+ " from jecn_file_t where file_Id = " + relateId;
	}


	/**
	 *  相关标准  JECN_STANDARD_HISTORY
	 * @param relateId
	 * @param historyId
	 * @return RELATE_TYPE ： 0 流程 1制度 2 文件
	 */
	private static String getFileRelatedStandardHistorySql(Long relateId,Long historyId) {
		return "INSERT INTO JECN_STANDARD_HISTORY " + "(ID,RELATE_ID,RELATE_TYPE,STANDARD_ID,HISTORY_ID)"
				+ "SELECT "+JecnCommonSql.getSqlGUID()+",FILE_ID,2  RELATE_TYPE,STANDARD_ID, " + historyId + " HISTORY_ID" +
				 "  FROM FILE_RELATED_STANDARD_T WHERE FILE_ID=" + relateId;
	}
	

	/**
	 * 相关风险文控
	 * 
	 * @param relateId
	 * @param historyId
	 * @return
	 */
	private static String getFileRelatedRiskHistorySql(Long relateId, Long historyId) {
		return "INSERT INTO JECN_RELATE_RISK_HISTORY" + 
		"(ID,RELATE_ID, RELATE_TYPE, RISK_ID,HISTORY_ID)"
		+ "SELECT "+JecnCommonSql.getSqlGUID()+",FILE_ID,2 RELATE_TYPE,RISK_ID, " + historyId + " HISTORY_ID"
		+ "   FROM FILE_RELATED_RISK_T WHERE FILE_ID=" + relateId;
	}

	/**
	 * JECN_AUTHORITY_HISTORY 权限表 岗位组查阅权限
	 * 
	 * @return ACCESS_TYPE : 2 岗位组
	 */
	private static String getJecnAuthorityGroupHistory(Long relateId,Long historyId) {
		return "insert into JECN_AUTHORITY_HISTORY"
				+ " (ID,RELATE_ID, TYPE,ACCESS_ID,ACCESS_TYPE,HISTORY_ID)"
				+ " select "+ JecnCommonSql.getSqlGUID()+ ", RELATE_ID,TYPE,POSTGROUP_ID, 2  ACCESS_TYPE,"+ historyId+ " HISTORY_ID "
				+ " from JECN_GROUP_PERMISSIONS_T where type = 1 and RELATE_ID="+ relateId;
	}

	/**
	 * JECN_AUTHORITY_HISTORY 权限表 部门查阅权限
	 * 
	 * @return ACCESS_TYPE : 0 部门
	 */
	private static String getJecnAuthorityOrgHistory(Long relateId,Long historyId) {
		return "insert into JECN_AUTHORITY_HISTORY"
				+ " (ID,RELATE_ID, TYPE,ACCESS_ID,ACCESS_TYPE,HISTORY_ID)"
				+ " select " + JecnCommonSql.getSqlGUID()
				+ ",  RELATE_ID, TYPE,ORG_ID, 0  ACCESS_TYPE ," + historyId+ " HISTORY_ID"+ 
				"  from JECN_ORG_ACCESS_PERM_T where type=1 and RELATE_ID="+ relateId;

	}

	/**
	 * JECN_AUTHORITY_HISTORY 权限表 岗位查阅权限
	 * 
	 * @return ACCESS_TYPE : 1 岗位
	 */
	private static String getJecnAuthorityAccessHistory(Long relateId,Long historyId) {
		return "insert into JECN_AUTHORITY_HISTORY"
				+ " (ID,RELATE_ID, TYPE,ACCESS_ID,ACCESS_TYPE,HISTORY_ID)"
				+ "  select "+ JecnCommonSql.getSqlGUID()+ ",RELATE_ID, TYPE,FIGURE_ID, 1  ACCESS_TYPE, "+ historyId+ " HISTORY_ID"
				+ "   from JECN_ACCESS_PERMISSIONS_T where type=1 and RELATE_ID="+ relateId;
	}
	/**
	 * 记录文件文控
	 * 
	 * @param ruleId
	 * @param historyId
	 * @param baseDao
	 */
	public static void recordFileModeHistory(Long fileId, Long historyId,IBaseDao baseDao) {
		baseDao.execteNative(getJecnAuthorityGroupHistory(fileId, historyId));// 岗位组
		baseDao.execteNative(getJecnAuthorityOrgHistory(fileId, historyId));// 部门
		baseDao.execteNative(getJecnAuthorityAccessHistory(fileId, historyId));// 岗位
		baseDao.execteNative(getJecnFileHistorySql(fileId, historyId));// 文件主表
		baseDao.execteNative(getFileRelatedStandardHistorySql(fileId, historyId));// 文件相关标准
		baseDao.execteNative(getFileRelatedRiskHistorySql(fileId, historyId));// 文件风险
	}
	
	
	
}
