package epros.designer.gui.system.field;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.system.BasicComponent;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;

/**
 * 文本框的组件
 * 
 * @author user
 * 
 */
public class JecnTipTextField implements BasicComponent {
	/** 流程输入Lab */
	private JLabel textFieldLab = new JLabel();
	/** 流程输入Field */
	private JecnTextField textField = new JecnTextField();

	private final String text;

	/** 提示名称 */
	private String tipName;

	private boolean request = false;
	private boolean enabled = true;

	public JecnTipTextField(int rows, JPanel infoPanel, Insets insets, String labName, String text, String tipName) {
		this(rows, infoPanel, insets, labName, text, tipName, false);
	}

	public JecnTipTextField(int rows, JPanel infoPanel, Insets insets, String labName, String text, String tipName,
			boolean request) {
		this.tipName = tipName;
		this.text = text == null ? "" : text;
		textFieldLab.setText(labName);
		textField.setText(text);
		// ==============流程输入==========================================
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(textFieldLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(textField, c);
		this.request = request;
		if (request) {
			c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}
	}

	public boolean isUpdate() {
		if (!text.equals(textField.getText())) {
			return true;
		}
		return false;
	}

	public String getResultStr() {
		return textField.getText().trim();
	}

	public boolean isValidateNotPass(JLabel verfyLab) {
		if (this.request) {
			return JecnValidateCommon.validateFieldNotPass(textField.getText().trim(), verfyLab, this.tipName);
		}
		return JecnValidateCommon.validateFieldNotPass(textField.getText().trim(), verfyLab, this.tipName);
	}
	
	/**
	 * 验证必填项
	 * 
	 * @param verfyLab
	 * @return
	 */
	public boolean validateRequired(JLabel verfyLab) {
		if (this.request) {
			if (textField.getText().trim().length() <= 0 && "".equals(textField.getText().trim())) {
				verfyLab.setText(tipName + JecnProperties.getValue("isNotEmpty"));
				return false;
			}
		}
		return true;
	}


	@Override
	public void setEnabled(boolean enable) {
		this.enabled = enable;
		textField.setEnabled(enable);
	}

	@Override
	public boolean isRequest() {
		return this.request;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setText(String text) {
		textField.setText(text);
	}

}
