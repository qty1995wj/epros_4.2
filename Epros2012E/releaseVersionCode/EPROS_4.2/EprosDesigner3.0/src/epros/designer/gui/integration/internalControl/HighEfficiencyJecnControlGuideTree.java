package epros.designer.gui.integration.internalControl;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

/**
 * 内控指引管理树
 * 
 * @author Administrator
 * 
 */
public class HighEfficiencyJecnControlGuideTree extends JecnHighEfficiencyTree {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger
			.getLogger(HighEfficiencyJecnControlGuideTree.class);

	/** 内控指引知识库窗口Dialog */
	private ControlGuideManageDialog controlGuideDialog;
	/** 内控指引知识库选择框Dialog */
	private ControlGuideChooseDialog controlGuideChooseDialog;

	/** 所有内控指引目录 */
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
	/** 是否支持双击打开文件，1不支持，0支持 */
	private int isOpen = 0;
	/** 判断是0:管理框操作，还是1:选择框操作 */
	private int isControlGuideMange = 0;

	/** 树节点 */
	private List<JecnTreeNode> node;

	/**
	 * HighEfficiencyJecnControlGuideTree构造方法
	 * 
	 * @param isOpen
	 *            是否支持双击打开文件，1不支持，0支持
	 * @param controlGuideChooseDialog
	 *            内控指引知识库选择框Dialog
	 */
	public HighEfficiencyJecnControlGuideTree(int isOpen,
			ControlGuideChooseDialog controlGuideChooseDialog) {
		this.isOpen = isOpen;
		this.controlGuideChooseDialog = controlGuideChooseDialog;
		isControlGuideMange = 1;
	}

	/**
	 * HighEfficiencyJecnControlGuideTree构造方法
	 * 
	 * @param controlGuideChooseDialog
	 *            内控指引知识库选择框Dialog
	 */
	public HighEfficiencyJecnControlGuideTree(
			ControlGuideChooseDialog controlGuideChooseDialog) {
		this.controlGuideChooseDialog = controlGuideChooseDialog;
	}

	/**
	 * HighEfficiencyJecnControlGuideTree构造方法
	 * 
	 * @param isOpenn
	 *            是否支持双击打开文件，1不支持，0支持
	 */
	public HighEfficiencyJecnControlGuideTree(int isOpen) {
		this.isOpen = isOpen;
	}

	/**
	 * HighEfficiencyJecnControlGuideTree构造方法
	 * 
	 * @param controlGuideDialog
	 *            内控指引知识库Dialog
	 */
	public HighEfficiencyJecnControlGuideTree(
			ControlGuideManageDialog controlGuideDialog) {
		this.controlGuideDialog = controlGuideDialog;
		this.isControlGuideMange = 0;
	}

	/**
	 * 获得Tree展开的监听器
	 * 
	 * @param tree
	 *            点击节点刷新生产子节点
	 */
	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree tree) {
		return new ControlGuideTreeListener(tree);
	}

	/** 获得TreeModel所有信息 */
	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getControlGuideAction()
					.getChildJecnControlGuides(Long.valueOf(0),
							JecnConstants.projectId);
		} catch (Exception e) {
			log.error("HighEfficiencyControlGuideMoveTree getTreeModel is error", e);
		}
		return ControlGuideCommon.getControlGuideTreeModel(list);
	}

	/**
	 * 设置Tree是否多选
	 */
	@Override
	public boolean isSelectMutil() {
		return true;
	}

	/**
	 * 添加鼠标事件
	 */
	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		ControlGuideCommon.treeMousePressed(evt, this, controlGuideDialog);
		node = ControlGuideCommon.getNode();
	}

	// getter,setter
	/** 是否支持双击打开文件，1不支持，0支持 */
	public int getIsOpen() {
		return isOpen;
	}

	/** 是否支持双击打开文件，1不支持，0支持 */
	public void setIsOpen(int isOpen) {
		this.isOpen = isOpen;
	}

	public ControlGuideChooseDialog getControlGuideChooseDialog() {
		return controlGuideChooseDialog;
	}

	public void setControlGuideChooseDialog(
			ControlGuideChooseDialog controlGuideChooseDialog) {
		this.controlGuideChooseDialog = controlGuideChooseDialog;
	}

	public int getIsControlGuideMange() {
		return isControlGuideMange;
	}

	public void setIsControlGuideMange(int isControlGuideMange) {
		this.isControlGuideMange = isControlGuideMange;
	}

	public List<JecnTreeNode> getNode() {
		return node;
	}

	public void setNode(List<JecnTreeNode> node) {
		this.node = node;
	}

}
