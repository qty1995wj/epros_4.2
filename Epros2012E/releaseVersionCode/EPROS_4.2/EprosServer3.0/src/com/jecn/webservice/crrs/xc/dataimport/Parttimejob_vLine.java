/**
 * Parttimejob_vLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.crrs.xc.dataimport;

public class Parttimejob_vLine  implements java.io.Serializable {
    private java.lang.String c_code;

    private java.lang.String c_name;

    private java.lang.String parttimeunitcode;

    private java.lang.String parttimeunitname;

    private java.math.BigDecimal parttimejobid;

    private java.lang.String parttimejobcode;

    private java.lang.String parttimejobname;

    private java.lang.String c_positionlevelname;

    private java.lang.String c_positionlevel;

    private java.lang.String enabled_flag;

    private java.lang.String c_type;

    private java.util.Date c_lut;

    private java.lang.String last_update_by;

    private java.math.BigDecimal versionid;

    public Parttimejob_vLine() {
    }

    public Parttimejob_vLine(
           java.lang.String c_code,
           java.lang.String c_name,
           java.lang.String parttimeunitcode,
           java.lang.String parttimeunitname,
           java.math.BigDecimal parttimejobid,
           java.lang.String parttimejobcode,
           java.lang.String parttimejobname,
           java.lang.String c_positionlevelname,
           java.lang.String c_positionlevel,
           java.lang.String enabled_flag,
           java.lang.String c_type,
           java.util.Date c_lut,
           java.lang.String last_update_by,
           java.math.BigDecimal versionid) {
           this.c_code = c_code;
           this.c_name = c_name;
           this.parttimeunitcode = parttimeunitcode;
           this.parttimeunitname = parttimeunitname;
           this.parttimejobid = parttimejobid;
           this.parttimejobcode = parttimejobcode;
           this.parttimejobname = parttimejobname;
           this.c_positionlevelname = c_positionlevelname;
           this.c_positionlevel = c_positionlevel;
           this.enabled_flag = enabled_flag;
           this.c_type = c_type;
           this.c_lut = c_lut;
           this.last_update_by = last_update_by;
           this.versionid = versionid;
    }


    /**
     * Gets the c_code value for this Parttimejob_vLine.
     * 
     * @return c_code
     */
    public java.lang.String getC_code() {
        return c_code;
    }


    /**
     * Sets the c_code value for this Parttimejob_vLine.
     * 
     * @param c_code
     */
    public void setC_code(java.lang.String c_code) {
        this.c_code = c_code;
    }


    /**
     * Gets the c_name value for this Parttimejob_vLine.
     * 
     * @return c_name
     */
    public java.lang.String getC_name() {
        return c_name;
    }


    /**
     * Sets the c_name value for this Parttimejob_vLine.
     * 
     * @param c_name
     */
    public void setC_name(java.lang.String c_name) {
        this.c_name = c_name;
    }


    /**
     * Gets the parttimeunitcode value for this Parttimejob_vLine.
     * 
     * @return parttimeunitcode
     */
    public java.lang.String getParttimeunitcode() {
        return parttimeunitcode;
    }


    /**
     * Sets the parttimeunitcode value for this Parttimejob_vLine.
     * 
     * @param parttimeunitcode
     */
    public void setParttimeunitcode(java.lang.String parttimeunitcode) {
        this.parttimeunitcode = parttimeunitcode;
    }


    /**
     * Gets the parttimeunitname value for this Parttimejob_vLine.
     * 
     * @return parttimeunitname
     */
    public java.lang.String getParttimeunitname() {
        return parttimeunitname;
    }


    /**
     * Sets the parttimeunitname value for this Parttimejob_vLine.
     * 
     * @param parttimeunitname
     */
    public void setParttimeunitname(java.lang.String parttimeunitname) {
        this.parttimeunitname = parttimeunitname;
    }


    /**
     * Gets the parttimejobid value for this Parttimejob_vLine.
     * 
     * @return parttimejobid
     */
    public java.math.BigDecimal getParttimejobid() {
        return parttimejobid;
    }


    /**
     * Sets the parttimejobid value for this Parttimejob_vLine.
     * 
     * @param parttimejobid
     */
    public void setParttimejobid(java.math.BigDecimal parttimejobid) {
        this.parttimejobid = parttimejobid;
    }


    /**
     * Gets the parttimejobcode value for this Parttimejob_vLine.
     * 
     * @return parttimejobcode
     */
    public java.lang.String getParttimejobcode() {
        return parttimejobcode;
    }


    /**
     * Sets the parttimejobcode value for this Parttimejob_vLine.
     * 
     * @param parttimejobcode
     */
    public void setParttimejobcode(java.lang.String parttimejobcode) {
        this.parttimejobcode = parttimejobcode;
    }


    /**
     * Gets the parttimejobname value for this Parttimejob_vLine.
     * 
     * @return parttimejobname
     */
    public java.lang.String getParttimejobname() {
        return parttimejobname;
    }


    /**
     * Sets the parttimejobname value for this Parttimejob_vLine.
     * 
     * @param parttimejobname
     */
    public void setParttimejobname(java.lang.String parttimejobname) {
        this.parttimejobname = parttimejobname;
    }


    /**
     * Gets the c_positionlevelname value for this Parttimejob_vLine.
     * 
     * @return c_positionlevelname
     */
    public java.lang.String getC_positionlevelname() {
        return c_positionlevelname;
    }


    /**
     * Sets the c_positionlevelname value for this Parttimejob_vLine.
     * 
     * @param c_positionlevelname
     */
    public void setC_positionlevelname(java.lang.String c_positionlevelname) {
        this.c_positionlevelname = c_positionlevelname;
    }


    /**
     * Gets the c_positionlevel value for this Parttimejob_vLine.
     * 
     * @return c_positionlevel
     */
    public java.lang.String getC_positionlevel() {
        return c_positionlevel;
    }


    /**
     * Sets the c_positionlevel value for this Parttimejob_vLine.
     * 
     * @param c_positionlevel
     */
    public void setC_positionlevel(java.lang.String c_positionlevel) {
        this.c_positionlevel = c_positionlevel;
    }


    /**
     * Gets the enabled_flag value for this Parttimejob_vLine.
     * 
     * @return enabled_flag
     */
    public java.lang.String getEnabled_flag() {
        return enabled_flag;
    }


    /**
     * Sets the enabled_flag value for this Parttimejob_vLine.
     * 
     * @param enabled_flag
     */
    public void setEnabled_flag(java.lang.String enabled_flag) {
        this.enabled_flag = enabled_flag;
    }


    /**
     * Gets the c_type value for this Parttimejob_vLine.
     * 
     * @return c_type
     */
    public java.lang.String getC_type() {
        return c_type;
    }


    /**
     * Sets the c_type value for this Parttimejob_vLine.
     * 
     * @param c_type
     */
    public void setC_type(java.lang.String c_type) {
        this.c_type = c_type;
    }


    /**
     * Gets the c_lut value for this Parttimejob_vLine.
     * 
     * @return c_lut
     */
    public java.util.Date getC_lut() {
        return c_lut;
    }


    /**
     * Sets the c_lut value for this Parttimejob_vLine.
     * 
     * @param c_lut
     */
    public void setC_lut(java.util.Date c_lut) {
        this.c_lut = c_lut;
    }


    /**
     * Gets the last_update_by value for this Parttimejob_vLine.
     * 
     * @return last_update_by
     */
    public java.lang.String getLast_update_by() {
        return last_update_by;
    }


    /**
     * Sets the last_update_by value for this Parttimejob_vLine.
     * 
     * @param last_update_by
     */
    public void setLast_update_by(java.lang.String last_update_by) {
        this.last_update_by = last_update_by;
    }


    /**
     * Gets the versionid value for this Parttimejob_vLine.
     * 
     * @return versionid
     */
    public java.math.BigDecimal getVersionid() {
        return versionid;
    }


    /**
     * Sets the versionid value for this Parttimejob_vLine.
     * 
     * @param versionid
     */
    public void setVersionid(java.math.BigDecimal versionid) {
        this.versionid = versionid;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Parttimejob_vLine)) return false;
        Parttimejob_vLine other = (Parttimejob_vLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.c_code==null && other.getC_code()==null) || 
             (this.c_code!=null &&
              this.c_code.equals(other.getC_code()))) &&
            ((this.c_name==null && other.getC_name()==null) || 
             (this.c_name!=null &&
              this.c_name.equals(other.getC_name()))) &&
            ((this.parttimeunitcode==null && other.getParttimeunitcode()==null) || 
             (this.parttimeunitcode!=null &&
              this.parttimeunitcode.equals(other.getParttimeunitcode()))) &&
            ((this.parttimeunitname==null && other.getParttimeunitname()==null) || 
             (this.parttimeunitname!=null &&
              this.parttimeunitname.equals(other.getParttimeunitname()))) &&
            ((this.parttimejobid==null && other.getParttimejobid()==null) || 
             (this.parttimejobid!=null &&
              this.parttimejobid.equals(other.getParttimejobid()))) &&
            ((this.parttimejobcode==null && other.getParttimejobcode()==null) || 
             (this.parttimejobcode!=null &&
              this.parttimejobcode.equals(other.getParttimejobcode()))) &&
            ((this.parttimejobname==null && other.getParttimejobname()==null) || 
             (this.parttimejobname!=null &&
              this.parttimejobname.equals(other.getParttimejobname()))) &&
            ((this.c_positionlevelname==null && other.getC_positionlevelname()==null) || 
             (this.c_positionlevelname!=null &&
              this.c_positionlevelname.equals(other.getC_positionlevelname()))) &&
            ((this.c_positionlevel==null && other.getC_positionlevel()==null) || 
             (this.c_positionlevel!=null &&
              this.c_positionlevel.equals(other.getC_positionlevel()))) &&
            ((this.enabled_flag==null && other.getEnabled_flag()==null) || 
             (this.enabled_flag!=null &&
              this.enabled_flag.equals(other.getEnabled_flag()))) &&
            ((this.c_type==null && other.getC_type()==null) || 
             (this.c_type!=null &&
              this.c_type.equals(other.getC_type()))) &&
            ((this.c_lut==null && other.getC_lut()==null) || 
             (this.c_lut!=null &&
              this.c_lut.equals(other.getC_lut()))) &&
            ((this.last_update_by==null && other.getLast_update_by()==null) || 
             (this.last_update_by!=null &&
              this.last_update_by.equals(other.getLast_update_by()))) &&
            ((this.versionid==null && other.getVersionid()==null) || 
             (this.versionid!=null &&
              this.versionid.equals(other.getVersionid())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getC_code() != null) {
            _hashCode += getC_code().hashCode();
        }
        if (getC_name() != null) {
            _hashCode += getC_name().hashCode();
        }
        if (getParttimeunitcode() != null) {
            _hashCode += getParttimeunitcode().hashCode();
        }
        if (getParttimeunitname() != null) {
            _hashCode += getParttimeunitname().hashCode();
        }
        if (getParttimejobid() != null) {
            _hashCode += getParttimejobid().hashCode();
        }
        if (getParttimejobcode() != null) {
            _hashCode += getParttimejobcode().hashCode();
        }
        if (getParttimejobname() != null) {
            _hashCode += getParttimejobname().hashCode();
        }
        if (getC_positionlevelname() != null) {
            _hashCode += getC_positionlevelname().hashCode();
        }
        if (getC_positionlevel() != null) {
            _hashCode += getC_positionlevel().hashCode();
        }
        if (getEnabled_flag() != null) {
            _hashCode += getEnabled_flag().hashCode();
        }
        if (getC_type() != null) {
            _hashCode += getC_type().hashCode();
        }
        if (getC_lut() != null) {
            _hashCode += getC_lut().hashCode();
        }
        if (getLast_update_by() != null) {
            _hashCode += getLast_update_by().hashCode();
        }
        if (getVersionid() != null) {
            _hashCode += getVersionid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Parttimejob_vLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "parttimejob_vLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parttimeunitcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "parttimeunitcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parttimeunitname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "parttimeunitname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parttimejobid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "parttimejobid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parttimejobcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "parttimejobcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parttimejobname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "parttimejobname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_positionlevelname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_positionlevelname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_positionlevel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_positionlevel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enabled_flag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "enabled_flag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("c_lut");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "c_lut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("last_update_by");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "last_update_by"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versionid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://hrcom.publisher.mdm", "versionid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
