package com.jecn.epros.server.service.reports;

import java.util.List;

import com.jecn.epros.server.bean.process.temp.TempApproveTimeHeadersBean;
import com.jecn.epros.server.bean.process.temp.TempTaskApproveTime;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeCount;
import com.jecn.epros.server.webBean.reports.AgingProcessBean;
import com.jecn.epros.server.webBean.reports.ProcessAccessBean;
import com.jecn.epros.server.webBean.reports.ProcessActivitiesBean;
import com.jecn.epros.server.webBean.reports.ProcessAnalysisBean;
import com.jecn.epros.server.webBean.reports.ProcessApplicationDetailsBean;
import com.jecn.epros.server.webBean.reports.ProcessConstructionBean;
import com.jecn.epros.server.webBean.reports.ProcessDetailBean;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceBean;
import com.jecn.epros.server.webBean.reports.ProcessInputOutputBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIFollowBean;
import com.jecn.epros.server.webBean.reports.ProcessRoleBean;
import com.jecn.epros.server.webBean.reports.ProcessScanOptimizeBean;

/**
 * 
 * 报表统计下载Excel类
 * 
 * @author ZHOUXY
 * 
 */
public interface IReportDownExcelService {

	/**
	 * 
	 * 获取流程时效性统计下载
	 * 
	 * @param agingProcessBean
	 *            AgingProcessBean 待下载数据
	 * @return byte[]
	 */
	public byte[] getAgingProcessExcel(AgingProcessBean agingProcessBean);

	/**
	 * 
	 * 获取流程建设覆盖度下载
	 * 
	 * @param buildCoverageBean
	 *            ProcessConstructionBean 待下载数据
	 * @return byte[]
	 */
	public byte[] getProcessBuildCoverageExcel(
			ProcessConstructionBean buildCoverageBean);

	/**
	 * 流程角色数统计下载
	 * 
	 * @param processRoleList
	 *            List<ProcessRoleBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessRole(List<ProcessRoleBean> processRoleList);

	/**
	 * 流程活动数统计下载
	 * 
	 * @param processActivitiesList
	 *            List<ProcessActivitiesBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessActivities(
			List<ProcessActivitiesBean> processActivitiesList);

	/**
	 * 流程应用人数统计下载
	 * 
	 * @param appUserCountList
	 *            List<ProcessApplicationDetailsBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessApplication(
			List<ProcessApplicationDetailsBean> appUserCountList);

	/**
	 * 流程输入输出统计下载
	 * 
	 * @param processInputOutputList
	 *            List<ProcessInputOutputBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessInputOutput(
			List<ProcessInputOutputBean> processInputOutputList);

	/**
	 * 流程分析统计下载
	 * 
	 * @param processAnalysisList
	 *            List<ProcessAnalysisBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessAnalysis(
			List<ProcessAnalysisBean> processAnalysisList);

	/**
	 * 流程访问数统计下载
	 * 
	 * @param processAccessList
	 *            List<ProcessAccessBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessAccess(
			List<ProcessAccessBean> processAccessList);

	/**
	 * 流程访问人统计下载
	 * 
	 * @param processAccessList
	 *            List<ProcessAccessBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessInterviewer(
			List<ProcessAccessBean> processAccessList);

	/**
	 * 流程文档下载数统计下载
	 * 
	 * @param processDocumentDownloadList
	 *            List<ProcessDocumentDownloadBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessDocumentNumber(
			List<ProcessAccessBean> processDocumentDownloadList);

	/**
	 * 流程文档下载人统计下载
	 * 
	 * @param processDocumentDownloadList
	 *            List<ProcessDocumentDownloadBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessDocumentPeople(
			List<ProcessAccessBean> processDocumentDownloadList);
	
	/**
	 * 任务审批各阶段审批时间统计下载
	 * @param headersBean 
	 * 
	 * @param taskAppriveTimeList
	 *            List<TempTaskApproveTime> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadTaskApproveTime(
			List<TempTaskApproveTime> taskAppriveTimeList, TempApproveTimeHeadersBean headersBean);
    
	/**
	 * 流程清单报表待下载数据
	 * @param processDetailBean 
	 *          ProcessDetailBean 待下载数据
	 * @return
	 * @throws Exception 
	 */
	public byte[] downloadProcessDetail(ProcessDetailBean processDetailBean) throws Exception;
	
	/**
	 * 流程优化审视统计表待下载数据
	 * @param processScanOptimize 
	 *          ProcessScanOptimizeBean 待下载数据
	 * @return
	 * @throws Exception 
	 */
	public byte[] downloadProcessScanOptimize(ProcessScanOptimizeBean processScanOptimize)throws Exception;

	/**
	 * 流程KPI跟踪表下载数据
	 * @param processKPIFollowBean 
	 *          ProcessKPIFollowBean 待下载数据
	 * @return
	 * @throws Exception 
	 */
	public byte[] downloadProcessKPIFollow(ProcessKPIFollowBean processKPIFollowBean)throws Exception;

	/**
	 * 流程有效期维护
	 * @param expiryMaintenanceBean
	 * @return
	 */
	public byte[] downloadProcessExpiryMaintenanceBean(
			ProcessExpiryMaintenanceBean expiryMaintenanceBean) throws Exception;

	/**
	 * 导出流程审视优化错误数据
	 * @param peopleId 
	 * @param type 
	 * @return
	 */
	public byte[] downloadProcessExpiryMaintenanceErrorDate(Long peopleId, int type) throws Exception;
	
	/**
	 * 导出合理化建议的统计数据 <br>三个统计报表的统一入口 根据proposeCountBean中的type判断为那个报表
	 * @return
	 */
	public byte[] downloadProposeCount(JecnRtnlProposeCount proposeCountBean) throws Exception;
	
}
