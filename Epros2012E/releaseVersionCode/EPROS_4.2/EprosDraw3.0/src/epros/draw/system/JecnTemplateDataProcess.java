package epros.draw.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import epros.draw.gui.figure.JecnTemplateData;
import epros.draw.gui.swing.JecnToolBar;

/**
 * 
 * 模板初始数据对象
 * 
 * @author fuzhh
 * 
 */
class JecnTemplateDataProcess {
	/** 模板数据集合 */
	private static List<JecnTemplateData> templateDataList = null;

	/** 模板数据 */
	private static JecnTemplateData templateData = null;

	/** 面板对应的模板数据和toolBar */
	private static Map<JecnTemplateData, JecnToolBar> templateMap = null;

	/** 地图按钮个数 */
	private static int totalMapCount;

	/** 流程图按钮个数 */
	private static int partMapCount;

	/**
	 * 
	 * 初始化数据集合
	 * 
	 */
	static List<JecnTemplateData> getTemplateDataList() {
		if (templateDataList == null) {
			templateDataList = new ArrayList<JecnTemplateData>();
		}
		return templateDataList;
	}

	static JecnTemplateData getTemplateData() {
		return templateData;
	}

	static void setTemplateData(JecnTemplateData templateData) {
		JecnTemplateDataProcess.templateData = templateData;
	}

	static Map<JecnTemplateData, JecnToolBar> getTemplateMap() {
		if (templateMap == null) {
			templateMap = new HashMap<JecnTemplateData, JecnToolBar>();
		}
		return templateMap;
	}

	static int getTotalMapCount() {
		return totalMapCount;
	}

	static void setTotalMapCount(int totalMapCount) {
		JecnTemplateDataProcess.totalMapCount = totalMapCount;
	}

	static int getPartMapCount() {
		return partMapCount;
	}

	static void setPartMapCount(int partMapCount) {
		JecnTemplateDataProcess.partMapCount = partMapCount;
	}
}
