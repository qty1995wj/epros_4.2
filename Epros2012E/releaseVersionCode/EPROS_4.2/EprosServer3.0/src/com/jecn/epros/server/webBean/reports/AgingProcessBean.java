package com.jecn.epros.server.webBean.reports;

import java.util.List;

import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 流程时效性统计
 * 
 * @author fuzhh Feb 28, 2013
 * 
 */
public class AgingProcessBean {
	/** 时间段(123....12) */
	private long time;

	/** 未更新次数 */
	private int failedToUpdate;
	/** 已更新次数 */
	private int hasBeenUpdated;
	/** 流程具体数据bean */
	private List<ProcessWebBean> processWebList;

	public int getFailedToUpdate() {
		return failedToUpdate;
	}

	public void setFailedToUpdate(int failedToUpdate) {
		this.failedToUpdate = failedToUpdate;
	}

	public int getHasBeenUpdated() {
		return hasBeenUpdated;
	}

	public void setHasBeenUpdated(int hasBeenUpdated) {
		this.hasBeenUpdated = hasBeenUpdated;
	}

	public List<ProcessWebBean> getProcessWebList() {
		return processWebList;
	}

	public void setProcessWebList(List<ProcessWebBean> processWebList) {
		this.processWebList = processWebList;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
}
