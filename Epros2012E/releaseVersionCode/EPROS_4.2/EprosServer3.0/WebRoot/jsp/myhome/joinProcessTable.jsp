<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% response.setHeader("Pragma","No-cache"); response.setHeader("Cache-Control","no-cache");%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
	    <div>
	    <div id="showDivTitle" style="overflow-y: scroll;overflow-x:hidden;width: 100%;" align="center">
	        <table width="98%" border="0" cellpadding="0" cellspacing="1" 
				align="center" style="background-color: #BFBFBF">
				<tr class="FixedTitleRow" align="center" >
				    <td width="2" class="gradient">
				    </td>
					<td width="15%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 岗位名称 -->
						<s:text name="positionName" />
					</td>
					<td width="15%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 执行角色 -->
						<s:text name="executiveRole" />
					</td>
					<td width="20%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 流程名称 -->
						<s:text name="processName" />
					</td>
					<td width="18%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 流程编号 -->
						<s:text name="processNumber" />
					</td>
					<td width="15%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 活动名称 -->
						<s:text name="activityTitle" />
					</td>
					<td width="13%" height="32" class="gradient"
						style="word-break: break-all;">
						<!-- 活动编号 -->
						<s:text name="activitynumbers" />
					</td>
					<td width="2" class="gradient">
					</td>
				</tr>
			</table>
	    </div>
		<div id="joinprocess_div_id" style="height:100%;overflow-y: scroll;overflow-x:hidden;width: 100%;" align="center">
			<table width="98%" border="0" cellpadding="0" cellspacing="1"
				align="center" style="background-color: #BFBFBF" style="table-layout: fixed;">
				<s:set id="count" value="1" />
				<s:iterator value="roleProcessList">
					<tr align="center" style="background-color: #ffffff"
						onmouseover="style.backgroundColor='#EFEFEF';"
						onmouseout="style.backgroundColor='#ffffff';" 
						height="20">
						<td align="right" class="gradient" style="word-break: break-all;" width="2%">
								<s:property value="#count" />
								<s:set id="count" value="#count+1"/>
						</td>
						<td class="t5" style="word-break: break-all;" width="15%"
							rowspan="<s:if test='roleList.size>0'><s:property value='roleList.size'/></s:if><s:else>1</s:else>">
							<s:property value="posName" />
						</td>
						<s:if test="roleList.size>0">
							<s:iterator value="roleList" status="st">
								<s:if test="#st.index == 0">
									<td class="t5" style="word-break: break-all;" width="15%">
										<s:property value="roleName" />
									</td>
									<td class="t5" style="word-break: break-all;" width="20%">
									<a href="process.action?type=process&flowId=<s:property value='flowId'/>" target='_blank' style="cursor: pointer;" class="table">
										<s:property value="flowName" />
									</a>
									</td>
									<td class="t5" style="word-break: break-all;" width="18%">
										<s:property value="flowNumber" />
									</td>
									<td class="t5" style="word-break: break-all;" width="15%">
									    <s:if test="activeList.size > 0">
										    <s:iterator value="activeList">
												<a href="processActive.action?activeId=<s:property value='activeFigureId'/>"
																style="cursor: pointer;" target='_blank' class="table">
													<s:if test="activeName!=null && activeName!=''">
													   <s:property value="activeName" />
													</s:if>
													<s:else>
													   <s:text name="activities"/>
													</s:else>
												</a>
												<br>
											</s:iterator>
									    </s:if>
										<s:else>
										   &nbsp;
										</s:else>
									</td>
									<td class="t5" style="word-break: break-all;" width="13%">
										<s:if test="activeList.size > 0">
											<s:iterator value="activeList">
											    <s:if test="activeId!=null && activeId!=''">
													   <s:property value="activeId" />
												</s:if>
												<s:else>
												    &nbsp;
												</s:else>
											<br>
										    </s:iterator>
									    </s:if>
									    <s:else>
									      &nbsp;
									    </s:else>
								    </td>
								 </s:if>
							</s:iterator>
						</s:if>
						<s:else>
							<td class="t5" style="word-break: break-all;" width="15%">
							</td>
							<td class="t5" style="word-break: break-all;" width="20%">
							</td>
							<td class="t5" style="word-break: break-all;" width="20%">
							</td>
							<td class="t5" style="word-break: break-all;" width="15%">
							</td>
							<td class="t5" style="word-break: break-all;" width="15%">
							</td>
						</s:else>
						<td class="t5" style="word-break: break-all;" width="2%"></td>
					</tr>
					<s:iterator value="roleList" status="st">
						<s:if test="#st.index != 0">
							<tr align="center" style="background-color: #ffffff"
								onmouseover="style.backgroundColor='#EFEFEF';"
								onmouseout="style.backgroundColor='#ffffff';" height="20">
								<td align="right"  class="gradient" style="word-break: break-all;" width="2%">
									<s:property value="#count" />
									<s:set id="count" value="#count+1"/>
						        </td>
								<td class="t5" style="word-break: break-all;" width="15%">
									<s:property value="roleName" />
								</td>
								<td class="t5" style="word-break: break-all;" width="20%">
									<a href="process.action?type=process&flowId=<s:property value='flowId'/>" target='_blank' style="cursor: pointer;" class="table">
									   <s:property value="flowName" />
									</a>
								</td>
								<td class="t5" style="word-break: break-all;" width="18%">
									<s:property value="flowNumber" />
								</td>
								<td class="t5" style="word-break: break-all;" width="15%">
								    <s:if test="activeList.size > 0">
									    <s:iterator value="activeList">
											<a href="processActive.action?activeId=<s:property value='activeFigureId'/>"
															style="cursor: pointer;" target='_blank' class="table">
												<s:if test="activeName!=null && activeName!=''">
												   <s:property value="activeName" />
												</s:if>
												<s:else>
												    <s:text name="activities"/>
												</s:else>
											</a>
											<br>
										</s:iterator>
								    </s:if>
									<s:else>
									   &nbsp;
									</s:else>
								</td>
								<td class="t5" style="word-break: break-all;" width="13%">
								<s:if test="activeList.size > 0">
									<s:iterator value="activeList">
									    <s:if test="activeId!=null && activeId!=''">
											   <s:property value="activeId" />
											</s:if>
											<s:else>
											    &nbsp;
											</s:else>
										<br>
									</s:iterator>
								   </s:if>
								   <s:else>
								      &nbsp;
								   </s:else>
								</td>
						       <td class="t5" style="word-break: break-all;" width="2%"></td>
							</tr>
						</s:if>
					</s:iterator>
				</s:iterator>
			</table>
		</div>
		</div>
	</body>
</html>