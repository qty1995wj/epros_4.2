package epros.designer.gui.popedom.positiongroup;

import java.util.List;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnPositionGroup;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeNode;

/**
 * 岗位组重命名
 * @author 2012-05-18
 *
 */
public class UpdateNamePosGroupDialog extends JecnEditNameDialog {
	
	private static Logger log = Logger.getLogger(UpdateNamePosGroupDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;
	private JecnPositionGroup jecnPositionGroup = null;
	private List<JecnTreeBean> treeBeanList = null;
	
	

	@Override
	public String getDialogTitle() {
		return null;
	}

	@Override
	public String getNameLab() {
		return null;
	}

	@Override
	public void saveData() {
		
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return false;
	}

}
