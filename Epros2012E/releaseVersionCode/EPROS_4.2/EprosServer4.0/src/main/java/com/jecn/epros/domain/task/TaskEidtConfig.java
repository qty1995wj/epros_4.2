package com.jecn.epros.domain.task;

import com.jecn.epros.util.ConfigUtils;

/**
 * Created by user on 2017/2/27.
 * 任务界面是否可编辑配置项
 */
public class TaskEidtConfig {
    /**
     * 密级是否可编辑
     **/
    private boolean isSecret = false;
    /**
     * 流程类别是否可编辑
     **/
    private boolean isResourceType = false;
    /**
     * 部门查阅权限是否可编辑
     **/
    private boolean isOrgPerm = false;
    /**
     * 岗位查阅权限是否可编辑
     **/
    private boolean isPosPerm = false;
    /**
     * 岗位组查阅权限是否可编辑
     **/
    private boolean isPosGroupPerm = false;
    /**
     * 二次验证是否开启
     **/
    private boolean twoValidteEnable = false;

    /**
     * 实施日期是否显示 true显示 false不显示
     **/
    private boolean implementDateShow = false;

    /**
     * 实施日期是否显示 true显示 false不显示
     **/
    private boolean implementDateNeedDefault = false;

    public boolean isImplementDateNeedDefault() {
        return implementDateNeedDefault;
    }

    public void setImplementDateNeedDefault(boolean implementDateNeedDefault) {
        this.implementDateNeedDefault = implementDateNeedDefault;
    }

    public boolean isImplementDateShow() {
        return implementDateShow;
    }

    public void setImplementDateShow(boolean implementDateShow) {
        if (ConfigUtils.isHiddenHistoryDetailDate()) {
            this.implementDateShow = false;
        } else {
            this.implementDateShow = implementDateShow;
        }
    }

    public boolean isTwoValidteEnable() {
        return twoValidteEnable;
    }

    public void setTwoValidteEnable(boolean twoValidteEnable) {
        this.twoValidteEnable = twoValidteEnable;
    }

    public boolean isSecret() {
        return isSecret;
    }

    public void setSecret(boolean secret) {
        isSecret = secret;
    }

    public boolean isResourceType() {
        return isResourceType;
    }

    public void setResourceType(boolean resourceType) {
        isResourceType = resourceType;
    }

    public boolean isOrgPerm() {
        return isOrgPerm;
    }

    public void setOrgPerm(boolean orgPerm) {
        isOrgPerm = orgPerm;
    }

    public boolean isPosPerm() {
        return isPosPerm;
    }

    public void setPosPerm(boolean posPerm) {
        isPosPerm = posPerm;
    }

    public boolean isPosGroupPerm() {
        return isPosGroupPerm;
    }

    public void setPosGroupPerm(boolean posGroupPerm) {
        isPosGroupPerm = posGroupPerm;
    }
}
