package epros.designer.gui.process.flowmap;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.process.ProcessArchitectureDescriptionBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.gui.system.PersonliableSelectCommon;
import epros.designer.gui.system.ResDepartSelectCommon;
import epros.designer.gui.system.field.JecnTipTextField;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程架构描述
 * 
 * @author Administrator
 * 
 */
public class FlowMapArchitectureCardDialog extends JecnDialog {

	private static Logger log = Logger.getLogger(FlowMapArchitectureCardDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();// 510, 660

	/** center面板 */
	private JecnPanel centerPanel = new JecnPanel();// 510, 505

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();// 500, 30

	/** 流程架构名称Lab */
	private JLabel flowMapLab = new JLabel(JecnProperties.getValue("flowMapNameC"));
	/** 流程架构名称Field */
	private JTextField flowMapField = new JTextField();// 170, 20

	private String colon = JecnResourceUtil.getLocale() == Locale.ENGLISH ? ":" : "：";
	/** 流程架构供应商Lab */
	private JLabel flowMapSupplierLab = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapSupplier)
			+ colon);// 供应商
	/** 流程架构供应商 */
	private JecnTextArea flowMapSupplierArea = new JecnTextArea();// 428, 45
	/** 流程架构供应商滚动面板 */
	private JScrollPane flowMapSupplierScrollPane = new JScrollPane(flowMapSupplierArea);

	/** 流程架构客户Lab */
	private JLabel flowMapCustomerLab = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapCustomer)
			+ colon);// 客户
	/** 流程架构供应商 */
	private JecnTextArea flowMapCustomerArea = new JecnTextArea();// 428, 45
	/** 流程架构供应商滚动面板 */
	private JScrollPane flowMapCustomerScrollPane = new JScrollPane(flowMapCustomerArea);

	/** 流程架构风险与机会ab */
	private JLabel flowMapRiskAndOpportunityLab = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapRiskAndOpportunity)
			+ colon);// 风险与机会
	/** 流程架构风险与机会 */
	private JecnTextArea flowMapRiskAndOpportunityArea = new JecnTextArea();// 428,
	/** 流程架构风险与机会 滚动面板 */
	private JScrollPane flowMapRiskAndOpportunityScrollPane = new JScrollPane(flowMapRiskAndOpportunityArea);

	/** 流程架构 层级 */
	private JLabel flowLevel = new JLabel(JecnProperties.getValue("flowLevelC"));
	/** 流程架构 层级Field */
	private JTextField flowLevelField = new JTextField();// 170, 20

	/** 流程架构编号Lab */
	private JLabel mapNumLab = new JLabel(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapNum)
			+ colon);// 编号
	/** 流程架构编号Field */
	private JTextField mapNumField = new JTextField();// 170, 20

	/** 描述 */
	private JLabel flowDescriptionLab = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapDescription)
			+ colon);// 流程描述
	/** 描述 */
	private JecnTextArea flowDescriptionArea = new JecnTextArea();// 428, 45
	/** 描述滚动面板 */
	private JScrollPane flowDescriptionScrollPane = new JScrollPane(flowDescriptionArea);

	/** 目的Lab */
	private JLabel mapPurposeLab = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapPurpose)
			+ colon);
	/** 目的Field */
	private JecnTextArea mapPurposeArea = new JecnTextArea();// 428, 45
	/** 目的滚动面板 */
	private JScrollPane mapPurposeScrollPane = new JScrollPane(mapPurposeArea);

	/** 关键字 */
	private JecnTipTextField keyWordField = null;

	/** 流程责任人 */
	private PersonliableSelectCommon personliableSelect = null;

	/** 责任部门 */
	private ResDepartSelectCommon resDepartSelectCommon = null;

	/** 流程专员 */
	private PeopleSelectCommon commissionerPeopleSelectCommon = null;

	/** 流程架构 上一层流程名称 */
	private JLabel pubFlow = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapLastFlow)
			+ colon);
	/** 流程架构 上一层流程名称Field */
	private JTextField pubFlowField = new JTextField();// 170, 20

	/** 流程架构 包含的下一层流程 */
	private JLabel subFlow = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapNextFlow)
			+ colon);
	/** 流程架构 包含的下一层流程 */
	private JecnTextArea subFlowArea = new JecnTextArea();// 170, 20
	/** 输出滚动面板 */
	private JScrollPane subFlowScrollPane = new JScrollPane(subFlowArea);

	/** 输入Lab */
	private JLabel mapInputLab = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapFlowInputInfo)
			+ colon);
	/** 输入Field */
	private JecnTextArea mapInputArea = new JecnTextArea();// 428, 45
	/** 输入滚动面板 */
	private JScrollPane mapInputScrollPane = new JScrollPane(mapInputArea);

	/** 输出Lab */
	private JLabel mapOutputLab = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapFlowOutputInfo)
			+ colon);
	/** 输出Field */
	private JecnTextArea mapOutputArea = new JecnTextArea();// 428, 45
	/** 输出滚动面板 */
	private JScrollPane mapOutputScrollPane = new JScrollPane(mapOutputArea);

	/** 流程起始 Lab */
	private JLabel flowstart = new JLabel(JecnConfigTool
			.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapFlowStart)
			+ colon);
	/** 流程起始Field */
	private JecnTextArea flowstartArea = new JecnTextArea();// 428, 45
	/** 流程起始滚动面板 */
	private JScrollPane flowstartScrollPane = new JScrollPane(flowstartArea);

	/** 流程终止 Lab */
	private JLabel flowEnd = new JLabel(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapFlowEnd)
			+ colon);
	/** 流程终止Field */
	private JecnTextArea flowEndArea = new JecnTextArea();// 428, 45
	/** 流程终止滚动面板 */
	private JScrollPane flowEndScrollPane = new JScrollPane(flowEndArea);

	/** 流程KPI Lab */
	private JLabel flowKPI = new JLabel(JecnConfigTool.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapFlowKPI)
			+ colon);
	/** 流程KPIField */
	private JecnTextArea flowKPIArea = new JecnTextArea();// 428, 45
	/** 流程KPI滚动面板 */
	private JScrollPane flowKPIScrollPane = new JScrollPane(flowKPIArea);

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 流程KPI验证信息提示 */
	private VerifLabel verfyLab = new VerifLabel();

	/** 设置大小 */
	Dimension dimension = null;
	/** 面板宽 */
	private int dialogWidth = 650;

	private ProcessArchitectureDescriptionBean processCord;

	private Long flowId;
	private Insets insets = new Insets(3, 3, 3, 3);
	private int rows = 0;

	public FlowMapArchitectureCardDialog(Long flowId) {
		/*
		 * 架构卡
		 */
		this.setTitle(JecnProperties.getValue("cardStructure"));
		this.flowId = flowId;
		this.setSize(dialogWidth, getCurPanelHeight());
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setModal(true);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 流程描述
		flowDescriptionArea.setBorder(null);
		flowDescriptionScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		flowDescriptionScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowDescriptionScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		flowMapSupplierArea.setBorder(null);
		flowMapSupplierScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		flowMapSupplierScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowMapSupplierScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		flowMapCustomerArea.setBorder(null);
		flowMapCustomerScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		flowMapCustomerScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowMapCustomerScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		flowMapRiskAndOpportunityArea.setBorder(null);
		flowMapRiskAndOpportunityScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		flowMapRiskAndOpportunityScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowMapRiskAndOpportunityScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 包含下一层级流程
		this.subFlowArea.setBorder(null);
		subFlowScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		subFlowScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		subFlowScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 目的
		mapPurposeArea.setBorder(null);
		mapPurposeScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		mapPurposeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mapPurposeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 流程输入（信息）
		mapInputArea.setBorder(null);
		mapInputScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		mapInputScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mapInputScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 流程输出（信息）
		mapOutputArea.setBorder(null);
		mapOutputScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		mapOutputScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mapOutputScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 流程起始
		flowstartArea.setBorder(null);
		flowstartScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		flowstartScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowstartScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 流程终止
		flowEndArea.setBorder(null);
		flowEndScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		flowEndScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowEndScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 流程KPI
		flowKPIArea.setBorder(null);
		flowKPIScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		flowKPIScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowKPIScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 根据ID获得流程地图描述数据
		try {
			processCord = ConnectionPool.getProcessAction().getProcessArchitectureDescriptionBean(flowId);
			if (processCord == null) {
				throw new NullPointerException("processCord is null");
			}
		} catch (Exception e1) {
			log.error("FlowMapArchitectureCardDialog is error", e1);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});

		// 组件布局
		initLayout();
		// 初始化数据
		initData();
		// 事件初始化
		actionInit();

	}

	private void initLayout() {
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		centerPanel.setLayout(new GridBagLayout());
		// 流程名称
		name();
		// 流程层级
		level();
		// 流程专员
		commissioner();
		// 关键字
		keyword();
		// 查询配置项
		List<JecnConfigItemBean> configs = ConnectionPool.getConfigAciton().selectConfigItemBeanByType(0, 2);
		for (JecnConfigItemBean config : configs) {
			if (config.getIsTableData() == 0) {
				continue;
			}
			ConfigItemPartMapMark mark = ConfigItemPartMapMark.valueOf(config.getMark());
			if (ConfigItemPartMapMark.isShowProcessMapNum == mark) {
				// 流程编号
				num();
			} else if (ConfigItemPartMapMark.isShowProcessMapDescription == mark) {
				// 流程描述
				description();
			} else if (ConfigItemPartMapMark.isShowProcessMapPurpose == mark) {
				// 流程目的
				purpose();
			} else if (ConfigItemPartMapMark.isShowProcessMapResPeople == mark) {
				// 责任人
				duty();
			} else if (ConfigItemPartMapMark.isShowProcessMapResponsibleDepartment == mark) {
				// 责任部门
				dutyOrg();
			} else if (ConfigItemPartMapMark.isShowProcessMapLastFlow == mark) {
				// 上一层流程名称
				preFlow();
			} else if (ConfigItemPartMapMark.isShowProcessMapNextFlow == mark) {
				// 包含的下一层流程
				nextFlow();
			} else if (ConfigItemPartMapMark.isShowProcessMapFlowInputInfo == mark) {
				// 流程输入（信息）
				input();
			} else if (ConfigItemPartMapMark.isShowProcessMapFlowOutputInfo == mark) {
				// 流程输出（信息）
				output();
			} else if (ConfigItemPartMapMark.isShowProcessMapFlowStart == mark) {
				// 流程起始
				start();
			} else if (ConfigItemPartMapMark.isShowProcessMapFlowEnd == mark) {
				// 流程终止
				end();
			} else if (ConfigItemPartMapMark.isShowProcessMapFlowKPI == mark) {
				// 流程KPI
				kpi();
			} else if (ConfigItemPartMapMark.isShowProcessMapSupplier == mark) {
				// 流程供应商
				supplier();
			} else if (ConfigItemPartMapMark.isShowProcessMapCustomer == mark) {
				// 流程客户
				customer();
			} else if (ConfigItemPartMapMark.isShowProcessMapRiskAndOpportunity == mark) {
				// 流程风险与机会
				riskAndOpportunity();
			}
		}

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(centerPanel, c);
		// 按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		mainPanel.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		if (JecnDesignerCommon.isAuthSave()) {
			buttonPanel.add(okBut);
		}
		buttonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);
	}

	private void name() {
		GridBagConstraints c;
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		centerPanel.add(flowMapLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(flowMapField, c);
		flowMapField.setEditable(false);
		rows++;
	}

	private void level() {
		GridBagConstraints c;
		c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		centerPanel.add(flowLevel, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(flowLevelField, c);
		flowLevelField.setEditable(false);
		rows++;
	}

	private void num() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapNum)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapNumLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			centerPanel.add(mapNumField, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapNum.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void description() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapDescription)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(flowDescriptionLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(flowDescriptionScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapDescription.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void purpose() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapPurpose)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapPurposeLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(mapPurposeScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapPurpose.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void duty() {
		personliableSelect = new PersonliableSelectCommon(rows, centerPanel, insets, processCord.getDutyUserId(),
				processCord.getDutyUserType(), processCord.getDutyUserName(), this, 3, JecnConfigTool
						.isRequest(ConfigItemPartMapMark.isShowProcessMapResPeople.toString()));
		rows++;
	}

	private void commissioner() {
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapCommissioner)) {
			this.commissionerPeopleSelectCommon = new PeopleSelectCommon(rows, centerPanel, insets, processCord
					.getCommissionerId(), processCord.getCommissionerName(), this, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapCommissioner)
					+ colon, JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapCommissioner.toString()));
			rows++;
		}
	}

	private void keyword() {
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapKeyWord)) {
			this.keyWordField = new JecnTipTextField(rows, centerPanel, insets, JecnConfigTool
					.getConfigItemName(ConfigItemPartMapMark.isShowProcessMapKeyWord)
					+ colon, processCord.getKeyWord() == null ? "" : processCord.getKeyWord(), JecnProperties
					.getValue("keyWord"), JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapKeyWord
					.toString()));
			rows++;
		}
	}

	private void dutyOrg() {
		resDepartSelectCommon = new ResDepartSelectCommon(rows, centerPanel, insets, processCord.getDutyOrgId(),
				processCord.getDutyOrgName(), this, JecnConfigTool
						.isRequest(ConfigItemPartMapMark.isShowProcessMapResponsibleDepartment.toString()));
		rows++;
	}

	private void preFlow() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapLastFlow)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(this.pubFlow, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			centerPanel.add(this.pubFlowField, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapLastFlow.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			pubFlowField.setEditable(false);
			rows++;
		}
	}

	private void nextFlow() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapNextFlow)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(this.subFlow, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(this.subFlowScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapNextFlow.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			subFlowArea.setEditable(false);
			rows++;
		}
	}

	private void supplier() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapSupplier)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(flowMapSupplierLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(flowMapSupplierScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapSupplier.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void input() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowInputInfo)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapInputLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(mapInputScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapFlowInputInfo.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void customer() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapCustomer)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(flowMapCustomerLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(flowMapCustomerScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapCustomer.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void output() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowOutputInfo)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(mapOutputLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(mapOutputScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapFlowOutputInfo.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void start() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowStart)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(flowstart, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(flowstartScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapFlowStart.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void end() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowEnd)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(flowEnd, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(flowEndScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapFlowEnd.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void kpi() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowKPI)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(flowKPI, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(flowKPIScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapFlowKPI.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void riskAndOpportunity() {
		GridBagConstraints c;
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapRiskAndOpportunity)) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			centerPanel.add(flowMapRiskAndOpportunityLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
					insets, 0, 0);
			centerPanel.add(flowMapRiskAndOpportunityScrollPane, c);
			if (JecnConfigTool.isRequest(ConfigItemPartMapMark.isShowProcessMapRiskAndOpportunity.toString())) {
				c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
						new Insets(3, 1, 3, 1), 0, 0);
				centerPanel.add(new CommonJlabelMustWrite(), c);
			}
			rows++;
		}
	}

	private void actionInit() {
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
	}

	private void initData() {
		if (this.processCord != null) {
			flowMapField.setText(processCord.getFlowName());
			flowLevelField.setText(processCord.getLevel() + "");
			mapNumField.setText(processCord.getNumId());
			if (processCord.getJecnMainFlowT() != null) {
				// 流程描述
				if (!"".equals(processCord.getJecnMainFlowT().getFlowDescription())) {
					this.flowDescriptionArea.setText(processCord.getJecnMainFlowT().getFlowDescription());
				}
				// 流程供应商
				if (!"".equals(processCord.getJecnMainFlowT().getSupplier())) {
					this.flowMapSupplierArea.setText(processCord.getJecnMainFlowT().getSupplier());
				}
				// 流程客户
				if (!"".equals(processCord.getJecnMainFlowT().getCustomer())) {
					this.flowMapCustomerArea.setText(processCord.getJecnMainFlowT().getCustomer());
				}
				// 流程风险与机会
				if (!"".equals(processCord.getJecnMainFlowT().getRiskAndOpportunity())) {
					this.flowMapRiskAndOpportunityArea.setText(processCord.getJecnMainFlowT().getRiskAndOpportunity());
				}
				// 流程目的
				if (!"".equals(processCord.getJecnMainFlowT().getFlowAim())) {
					this.mapPurposeArea.setText(processCord.getJecnMainFlowT().getFlowAim());
				}
				// 流程輸入（信息）
				if (!"".equals(processCord.getJecnMainFlowT().getFlowInput())) {
					this.mapInputArea.setText(processCord.getJecnMainFlowT().getFlowInput());
				}
				// 流程輸出（信息）
				if (!"".equals(processCord.getJecnMainFlowT().getFlowOutput())) {
					this.mapOutputArea.setText(processCord.getJecnMainFlowT().getFlowOutput());
				}
				// 流程起始
				if (!"".equals(processCord.getJecnMainFlowT().getFlowStart())) {
					this.flowstartArea.setText(processCord.getJecnMainFlowT().getFlowStart());
				}
				// 流程終止
				if (!"".equals(processCord.getJecnMainFlowT().getFlowEnd())) {
					this.flowEndArea.setText(processCord.getJecnMainFlowT().getFlowEnd());
				}
				// 流程KPI
				if (!"".equals(processCord.getJecnMainFlowT().getFlowKpi())) {
					this.flowKPIArea.setText(processCord.getJecnMainFlowT().getFlowKpi());
				}
			}
			// 上級流程
			if (processCord.getPubBean() != null) {
				this.pubFlowField.setText(processCord.getPubBean().getName());
			}
			// 下一級流程
			if (processCord.getSubList().size() > 0) {
				String flowNames = "";
				for (JecnTreeBean treeBean : processCord.getSubList()) {
					flowNames = flowNames + treeBean.getName() + "/";
				}
				flowNames = flowNames.substring(0, flowNames.length() - 1);
				this.subFlowArea.setText(flowNames);
			}

		}
	}

	/**
	 * 检查是否更新
	 * 
	 * @return
	 */
	private boolean isUpdate() {
		if (processCord == null) {
			return false;
		}
		if (!JecnDesignerCommon.isEditByTaskNode(flowId, TreeNodeType.processMap, JecnDesignerMainPanel
				.getDesignerMainPanel().getTree(), JecnConstants.loginBean.isDesignProcessAdmin())) {
			return false;
		}
		// 编号
		String numId = "";
		if (processCord.getNumId() != null) {
			numId = processCord.getNumId();
		}
		if (!numId.equals(this.mapNumField.getText())) {
			return true;
		}
		JecnMainFlowT jecnMainFlow = processCord.getJecnMainFlowT();
		// 流程描述
		String flowDescription = "";
		if (jecnMainFlow != null && jecnMainFlow.getFlowDescription() != null) {
			flowDescription = jecnMainFlow.getFlowDescription();
		}
		if (!flowDescription.equals(this.flowDescriptionArea.getText().toString())) {
			return true;
		}
		// 目的
		String flowAim = "";
		if (jecnMainFlow != null && jecnMainFlow.getFlowAim() != null) {
			flowAim = jecnMainFlow.getFlowAim();
		}
		if (!flowAim.equals(this.mapPurposeArea.getText())) {
			return true;
		}
		// 供应商
		String supplier = "";
		if (jecnMainFlow != null && jecnMainFlow.getSupplier() != null) {
			supplier = jecnMainFlow.getSupplier();
		}
		if (!supplier.equals(this.flowMapSupplierArea.getText())) {
			return true;
		}
		// 客户
		String customer = "";
		if (jecnMainFlow != null && jecnMainFlow.getCustomer() != null) {
			customer = jecnMainFlow.getCustomer();
		}
		if (!customer.equals(this.flowMapCustomerArea.getText())) {
			return true;
		}
		// 风险与机会
		String riskAndOpportunity = "";
		if (jecnMainFlow != null && jecnMainFlow.getRiskAndOpportunity() != null) {
			riskAndOpportunity = jecnMainFlow.getRiskAndOpportunity();
		}
		if (!riskAndOpportunity.equals(this.flowMapRiskAndOpportunityArea.getText())) {
			return true;
		}
		// 输入
		String fowInput = "";
		if (jecnMainFlow != null && jecnMainFlow.getFlowInput() != null) {
			fowInput = jecnMainFlow.getFlowInput();
		}
		if (!fowInput.equals(this.mapInputArea.getText())) {
			return true;
		}
		// 输出
		String fowOutput = "";
		if (jecnMainFlow != null && jecnMainFlow.getFlowOutput() != null) {
			fowOutput = jecnMainFlow.getFlowOutput();
		}
		if (!fowOutput.equals(this.mapOutputArea.getText())) {
			return true;
		}
		// 流程起始
		String flowStartData = "";
		if (jecnMainFlow != null && jecnMainFlow.getFlowStart() != null) {
			flowStartData = jecnMainFlow.getFlowStart();
		}
		if (!flowStartData.equals(this.flowstartArea.getText())) {
			return true;
		}
		// 流程终止
		String flowEndData = "";
		if (jecnMainFlow != null && jecnMainFlow.getFlowEnd() != null) {
			flowEndData = jecnMainFlow.getFlowEnd();
		}
		if (!flowEndData.equals(this.flowEndArea.getText())) {
			return true;
		}
		// 流程KPI
		String flowKpiData = "";
		if (jecnMainFlow != null && jecnMainFlow.getFlowKpi() != null) {
			flowKpiData = jecnMainFlow.getFlowKpi();
		}
		if (!flowKpiData.equals(this.flowKPIArea.getText())) {
			return true;
		}
		// 责任人
		if (personliableSelect != null) {
			if (personliableSelect.getSingleSelectCommon().isUpdate()) {
				return true;
			}
		}
		// 责任部门
		if (resDepartSelectCommon != null) {
			if (resDepartSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 流程专员
		if (this.commissionerPeopleSelectCommon != null) {
			if (commissionerPeopleSelectCommon.isUpdate()) {
				return true;
			}
		}
		// 关键字
		if (this.keyWordField != null) {
			if (this.keyWordField.isUpdate()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!JecnDesignerCommon.isAuthSave()) {
			this.setVisible(false);
		} else {
			if (!isUpdate()) {// 没有更新
				this.setVisible(false);
			} else {
				int optionTig = this.dialogCloseBeforeMsgTipAction();
				if (optionTig == 2) {
					okButPerformed();
				} else if (optionTig == 1) {
					this.setVisible(false);
				}
			}
		}
	}

	/**
	 * 确定 保存流程地图描述
	 */
	private void okButPerformed() {
		if (isUpdate()) {
			if (DrawCommon.checkNameMaxLength(this.mapNumField.getText().trim())) {
				verfyLab.setText(JecnProperties.getValue("actBaseNum") + " "
						+ JecnProperties.getValue("lengthNotBaiOut"));
				return;
			}
			// 关键字 长度验证
			if (this.keyWordField != null && DrawCommon.checkNameMaxLength(this.keyWordField.getResultStr())) {
				verfyLab.setText(JecnProperties.getValue("keyWord") + JecnProperties.getValue("lengthNotBaiOut"));
				return;
			}

			// 流程描述
			if (DrawCommon.checkNoteLength(flowDescriptionArea.getText().trim())) {
				verfyLab.setText(JecnProperties.getValue("processRequireMents") + " "
						+ JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 目的
			if (DrawCommon.checkNoteLength(mapPurposeArea.getText().trim())) {
				verfyLab.setText(JecnProperties.getValue("purpose") + " " + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 输入
			if (DrawCommon.checkNoteLength(mapInputArea.getText().trim())) {
				verfyLab.setText(JecnProperties.getValue("flowInputInfo") + " "
						+ JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 输出
			if (DrawCommon.checkNoteLength(mapOutputArea.getText().trim())) {
				verfyLab
						.setText(JecnProperties.getValue("flowOutInfo") + " " + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 流程起始
			if (DrawCommon.checkNoteLength(flowstartArea.getText().trim())) {
				verfyLab.setText(JecnProperties.getValue("flowstart") + " " + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 流程终止
			if (DrawCommon.checkNoteLength(flowEndArea.getText().trim())) {
				verfyLab.setText(JecnProperties.getValue("flowStop") + " " + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 流程KPI
			if (DrawCommon.checkNoteLength(this.flowKPIArea.getText().trim())) {
				verfyLab.setText(JecnProperties.getValue("flowKPIBtn") + " " + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 流程供应商
			if (DrawCommon.checkNoteLength(flowMapSupplierArea.getText().trim())) {
				String title = JecnProperties.getValue("supplier").substring(0,
						JecnProperties.getValue("supplier").length() - 1);
				verfyLab.setText(title + " " + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 流程客户
			if (DrawCommon.checkNoteLength(flowMapCustomerArea.getText().trim())) {
				String title = JecnProperties.getValue("customer").substring(0,
						JecnProperties.getValue("customer").length() - 1);
				verfyLab.setText(title + " " + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			// 流程风险与机会
			if (DrawCommon.checkNoteLength(flowMapRiskAndOpportunityArea.getText().trim())) {
				String title = JecnProperties.getValue("RiskAndOpportunity").substring(0,
						JecnProperties.getValue("RiskAndOpportunity").length() - 1);
				verfyLab.setText(title + " " + JecnProperties.getValue("lengthNotOut"));
				return;
			}
			this.processCord.setNumId(mapNumField.getText().trim());
			if (processCord.getJecnMainFlowT() == null) {
				processCord.setJecnMainFlowT(new JecnMainFlowT());
			}
			processCord.getJecnMainFlowT().setFlowAim(mapPurposeArea.getText().trim());
			processCord.getJecnMainFlowT().setFlowDescription(flowDescriptionArea.getText().trim());
			processCord.getJecnMainFlowT().setFlowInput(mapInputArea.getText().trim());
			processCord.getJecnMainFlowT().setFlowOutput(mapOutputArea.getText().trim());
			processCord.getJecnMainFlowT().setFlowStart(flowstartArea.getText().trim());
			processCord.getJecnMainFlowT().setFlowEnd(flowEndArea.getText().trim());
			processCord.getJecnMainFlowT().setFlowKpi(flowKPIArea.getText().trim());
			processCord.getJecnMainFlowT().setSupplier(flowMapSupplierArea.getText().trim());
			processCord.getJecnMainFlowT().setCustomer(flowMapCustomerArea.getText().trim());
			processCord.getJecnMainFlowT().setRiskAndOpportunity(flowMapRiskAndOpportunityArea.getText().trim());
			processCord.setDutyUserId(personliableSelect.getSingleSelectCommon().getIdResult());
			processCord.setDutyOrgId(resDepartSelectCommon.getIdResult());
			processCord.setUpdatePeopleId(JecnUtil.getUserId());
			// 责任人类型
			processCord.setDutyUserType(personliableSelect.getPeopleResType());
			this.processCord.setKeyWord(this.keyWordField == null ? "" : this.keyWordField.getResultStr());
			// 流程专员
			if (this.commissionerPeopleSelectCommon != null) {
				processCord.setCommissionerId(commissionerPeopleSelectCommon.getIdResult());
			}
			// 流程地图描述 执行数据库保存
			try {
				ConnectionPool.getProcessAction().updateFlowMap(processCord);

			} catch (Exception e) {
				log.error("FlowMapArchitectureCardDialog okButt is error", e);
			}
			updateTreeNode();
		}
		this.setVisible(false);
	}

	/**
	 * 刷新树节点的更新状态
	 */
	private void updateTreeNode() {
		JecnTreeNode curWorkNode = JecnTreeCommon.getWorkNode();
		if (curWorkNode != null) {
			curWorkNode.getJecnTreeBean().setUpdate(true);
			JecnTreeCommon.reload(JecnDesignerMainPanel.getDesignerMainPanel().getTree(), curWorkNode);
		}
	}

	/**
	 * 初始化高度
	 */
	public int getCurPanelHeight() {
		int height = 210;

		// 流程目的
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapPurpose)) {
			height += 40;
		}
		// 编号
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapNum)) {
			height += 40;
		}
		// 描述
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapDescription)) {
			height += 40;
		}
		// 专员
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapCommissioner)) {
			height += 40;
		}
		// 上一层流程名称
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapLastFlow)) {
			height += 40;
		}
		// 包含的下一层流程
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapNextFlow)) {
			height += 40;
		}
		// 流程输入（信息）
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowInputInfo)) {
			height += 40;
		}
		// 流程输出（信息）
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowOutputInfo)) {
			height += 40;
		}

		// 流程起始
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowStart)) {
			height += 40;
		}
		// 流程终止
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowEnd)) {
			height += 40;
		}
		// 流程KPI
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapFlowKPI)) {
			height += 40;
		}
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapSupplier)) {
			height += 40;
		}
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapCustomer)) {
			height += 40;
		}
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isShowProcessMapRiskAndOpportunity)) {
			height += 40;
		}
		return height;
	}

	class VerifLabel extends JLabel {
		public VerifLabel() {
			initComponents();
		}

		private void initComponents() {
			// 项的内容大小
			dimension = new Dimension(420, 15);
			this.setPreferredSize(dimension);
			this.setMinimumSize(dimension);
			this.setMaximumSize(dimension);
			this.setForeground(Color.red);
		}
	}

}
