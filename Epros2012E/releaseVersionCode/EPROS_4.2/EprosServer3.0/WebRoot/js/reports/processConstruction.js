//流程建设覆盖度 3.06废弃
function processConstructionGrid() {
	processConstructionGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "processConstruction.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "flowId",
									mapping : "flowId"
								}, {
									name : "flowName",
									mapping : "flowName"
								}, {
									name : "flowIdInput",
									mapping : "flowIdInput"
								}, {
									name : "resPeopleName",
									mapping : "resPeopleName"
								}, {
									name : "orgName",
									mapping : "orgName"
								}, {
									name : "pubDate",
									mapping : "pubDate"
								},{
									name : "typeByData",
									mapping : "typeByData"
								}])

			});

	processConstructionGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processConstructionNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_pubDate,
				dataIndex : "pubDate",
				align : "center",
				sortable : true,
				menuDisabled : true
			},{
				header : i_state,
				dataIndex : "typeByData",
				align : "center",
				menuDisabled : true,
				renderer:secretTypeByData
			}

	]);
	processConstructionGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processConstructionGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	processConstructionGridPanel = new Ext.grid.GridPanel({
				id : "processConstructionGridPanel_id",
				renderTo : 'processConstructionGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-175,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processConstructionGridStore,
				colModel : processConstructionGridColumnModel,
				bbar : processConstructionGridBbar
			})
             processConstructionPieCharts();
    reports_index_id='reports_processConstruction_id'; 
}

// 状态为 0待建 1审批中 2已发布
function secretTypeByData(value){
	if(value==0){
		return i_toBeBuilt;
	}else if(value==1){
		return i_examinationAndApprovalOf;
	}else if(value==2){
		return i_released;
	}
}


// 加载流程建设覆盖度饼状图
function processConstructionPieCharts(){
	$("#sysGrid_id").children().remove();
	$("#sysGrid_id").load('processConstructionPieCharts.action',{
	"resOrgId":document.getElementById("orgDutyId_construction_id").value,
	"processId":document.getElementById("processId_construction_id").value,
	"constructionType":document.getElementById("state_id").value
	  },function(){
		
	});
}


/**
 * 流程建设覆盖度
 */
function processConstructionGridSearch(){
	 //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_construction_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_construction_id").value;
	//状态
	 var constructionType=document.getElementById("state_id").value;
	
	 if(resOrgId&&resOrgId!=""){
     	processConstructionGridStore.setBaseParam("resOrgId", resOrgId);
     }
     if(processId&&processId!=""){
     	processConstructionGridStore.setBaseParam("processId", processId);
     }
     processConstructionGridStore.setBaseParam("constructionType", constructionType);

	 processConstructionGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
	processConstructionPieCharts();
}

/**
 * 流程名称增加超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function processConstructionNameRenderer(value, cellmeta, record){
	if(record.data['typeByData']==2){
		return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['flowId']+"' >"+value+"</a>";
	}else{
		return value;
	}
}

/**
 * 流程建设覆盖度重置
 **/
function processConstructionSearchReset(){
        document.getElementById("orgDutyName_construction_id").value = "";
		document.getElementById("processName_construction_id").value = "";
		document.getElementById("orgDutyId_construction_id").value = "-1";
		document.getElementById("processId_construction_id").value = "-1";
		document.getElementById("state_id").options[0].selected=true;
}

// 流程建设覆盖度下载
function processConstructionDownloadExcel(){
     //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_construction_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_construction_id").value;
	 //状态
	 var constructionType=document.getElementById("state_id").value;
	 window.location.href="downloadProcessConstruction.action?resOrgId="+resOrgId+"&processId="+processId+"&constructionType="+constructionType;
}