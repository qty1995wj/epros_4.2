package com.jecn.epros.server.bean.file;

import java.io.Serializable;

/**
 * @author yxw 2012-12-4
 * @description：文件列表显示字段
 */
public class FileWebBean implements Serializable {
	/** 样例主键ID */
	private int modeId;
	/** 文件ID */
	private long fileId;
	/** 文件名称 */
	private String fileName;
	/** 文件编号 */
	private String fileNum;
	/** 责任部门名称 */
	private String orgName;
	/** 密级 */
	private int secretLevel;
	/** 制度文件上传方式 0关联文件 1本地上传*/
	private int isFileLocal;
	
	public int getIsFileLocal() {
		return isFileLocal;
	}

	public void setIsFileLocal(int isFileLocal) {
		this.isFileLocal = isFileLocal;
	}

	public int getModeId() {
		return modeId;
	}

	public void setModeId(int modeId) {
		this.modeId = modeId;
	}

	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileNum() {
		return fileNum;
	}

	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public int getSecretLevel() {
		return secretLevel;
	}

	public void setSecretLevel(int secretLevel) {
		this.secretLevel = secretLevel;
	}
}
