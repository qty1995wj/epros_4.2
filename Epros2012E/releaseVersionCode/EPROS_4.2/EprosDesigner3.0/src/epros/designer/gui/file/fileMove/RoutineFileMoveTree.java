package epros.designer.gui.file.fileMove;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.file.JecnFileCommon;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class RoutineFileMoveTree extends JecnRoutineTree {
	private Logger log = Logger.getLogger(RoutineFileMoveTree.class);
	/***/
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/** 待移动ID集合 */
	private List<Long> listIds = new ArrayList<Long>();
	
	public RoutineFileMoveTree(List<Long> listIds){
		super(listIds);
		this.listIds = listIds;
	}
	
	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getFileAction().getAllFileDirs(
					JecnConstants.projectId);
		} catch (Exception e) {
			log.error("RoutineFileMoveTree is error！", e);
		}
		return JecnFileCommon.getTreeMoveModel(list, listIds);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		
	}

}
