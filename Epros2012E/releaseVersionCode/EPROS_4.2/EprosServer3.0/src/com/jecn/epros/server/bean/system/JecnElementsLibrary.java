package com.jecn.epros.server.bean.system;

public class JecnElementsLibrary implements java.io.Serializable {
	private Long id;
	/** int值，0：流程；1：流程架构 2：组织 */
	private int type;
	/** 0：不显示；1：显示(元素库上显示) */
	private int value;
	private int sort;
	private int defaultSort;
	private int defaultValue;
	private String mark;
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(int defaultValue) {
		this.defaultValue = defaultValue;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getDefaultSort() {
		return defaultSort;
	}

	public void setDefaultSort(int defaultSort) {
		this.defaultSort = defaultSort;
	}
}
