package com.jecn.epros.server.bean.download;

/**
 * 流程文件某些自定义的配置
 * 
 * @author hyl
 * 
 */
public class DocConfig {
	/**
	 * 活动内容是否同时包含关键活动内容
	 */
	private boolean activityContentHasKeyContent = false;

	public boolean isActivityContentHasKeyContent() {
		return activityContentHasKeyContent;
	}

	public void setActivityContentHasKeyContent(boolean activityContentHasKeyContent) {
		this.activityContentHasKeyContent = activityContentHasKeyContent;
	}

}
