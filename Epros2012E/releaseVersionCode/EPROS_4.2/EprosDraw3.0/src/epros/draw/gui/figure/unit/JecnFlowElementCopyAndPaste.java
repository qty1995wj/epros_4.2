package epros.draw.gui.figure.unit;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.event.flowElement.MapManLineCommon;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.MapLineFigure;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.TempDividingLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.toolbar.io.JecnSaveFlowImageJDialog;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnRemoveFlowElementUnit;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;

/**
 * 流程元素复制、粘贴、剪切、删除处理类
 * 
 * @author ZHANGXH
 * @date： 日期：May 15, 2012 时间：11:08:46 AM
 */
public class JecnFlowElementCopyAndPaste {
	/** 剪贴板临时文件夹 */
	public final static String COPY_PRINT = "print/";
	/** 返回临时文件名称：文件路径 + 系统毫秒数＋ jpg */
	public final static String TEMP_COPY = "print/tempCopy";
	/** 文件扩展名 */
	public final static String COPY_FILE_TYPE = ".jpg";
	/** 连续粘贴流程元素相差距离 */
	private static int countPative = -1;
	/** 鼠标点击 图形 右键菜单记录鼠标点击的临时点 鼠标右键菜单专用 */
	private static Point editPastePoint = null;

	/**
	 * 
	 * 初始化countPative值
	 * 
	 */
	public static void initCountPative() {
		countPative = -1;
	}

	/**
	 * 
	 * 截图
	 * 
	 */
	public static void iconShot() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || workflow.getCurrentSelectElement() == null
				|| workflow.getCurrentSelectElement().size() == 0) {
			return;
		}
		// 剪切、复制选中区域图形处理，添加到剪贴板
		copyImageHandle(workflow.getCurrentSelectElement());
	}

	/**
	 * 
	 * 流程元素复制处理
	 */
	public static void flowElementCopy() {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取复制、剪贴、删除 选中面板集合及图形最大和最小XY坐标
		if (workflow.getCurrentSelectElement().size() == 0 || !init()) {
			return;
		}
		workflow.getCurCopyList().clear();

		// 画图面板选中流程元素
		List<JecnBaseFlowElementPanel> selectList = workflow.getCurrentSelectElement();

		if (selectList.size() == 1) {
			// 泳池特殊处理
			selectList = JecnWorkflowUtil.selectModelFigureContainsEles(selectList.get(0));
		}

		// 添加剪切复制的流程元素到集合中
		workflow.addCurCopyList(selectList);

		// 剪切、复制选中区域图形处理，添加到剪贴板
		copyCutLocation(workflow.getCurrentSelectElement());
	}

	/**
	 * 
	 * 获取复制、剪贴、删除 选中面板集合及图形最大和最小XY坐标
	 */
	private static boolean init() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取复制、剪贴、删除 选中面板集合及图形最大和最小XY坐标
		if (workflow == null || workflow.getCurrentSelectElement() == null) {
			return false;
		}
		// 获取选中区域的X和Y坐标的最小值
		// setLeftXAndLeftY();

		return true;
	}

	/**
	 * 面板图形元素剪切处理
	 * 
	 */
	public static void flowElementCut() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取复制、剪贴、删除 选中面板集合及图形最大和最小XY坐标
		if (workflow == null || workflow.getCurrentSelectElement().size() == 0) {
			return;
		}
		// 获取复制、剪贴、删除 选中面板集合及图形最大和最小XY坐标
		if (!init()) {
			return;
		}
		workflow.getCurCopyList().clear();

		// 用于撤销恢复:记录被操作对象集合
		List<JecnBaseFlowElementPanel> undoPanelList = new ArrayList<JecnBaseFlowElementPanel>();
		// 连接线(记录选中的连接线集合,)
		Set<JecnBaseManhattanLinePanel> manLineSet = new HashSet<JecnBaseManhattanLinePanel>();
		// 撤销恢复前数据处理
		cutCurSeleteListPanel(undoPanelList, manLineSet, true);

		if (undoPanelList.size() == 0) {// 无剪切
			return;
		}
		// 操作前数据
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 记录撤销恢复数据
		undoData.recodeFlowElement(undoPanelList);
		JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createDelete(undoData);
		workflow.getUndoRedoManager().addEdit(undoProcess);
		// *******************撤销恢复*******************//

		// 添加所有图形到copyList
		workflow.addCurCopyList(workflow.getCurrentSelectElement());
		// // 添加剪切图形关联线到copyList
		// for (JecnBaseManhattanLinePanel manLinePanel : manLineSet) {
		// if (workflow.getCurrentSelectElement().contains(manLinePanel)) {//
		// 删除图形的相关连接线是否在选中集合中，不存在
		// // 则不记录在copyList
		// workflow.addCurCopyList(manLinePanel);
		// }
		// }
		// 剪切、复制选中区域图形处理，添加到剪贴板
		copyCutLocation(workflow.getCurrentSelectElement());

		// 删除图形元素
		JecnRemoveFlowElementUnit.removeFlowElement(undoPanelList);
		// 清空选中图形元素集合
		workflow.getCurrentSelectElement().clear();
		// 刷新面板
		workflow.updateUI();
	}

	/**
	 * 剪切、复制选中区域图形处理，添加到剪贴板
	 */
	public static void copyImageHandle(List<JecnBaseFlowElementPanel> listPanel) {
		// 剪贴版临时文件夹是否存在，存在责清除文件夹内文件
		checkDir();
		// 返回临时文件名称：文件路径 + 系统毫秒数＋ jpg
		String pathCopy = getPathCopy();
		// 获取复制时的选中区域
		boolean isSave = copyImageToSystem(pathCopy, listPanel);
		if (!isSave) {
			return;
		}

		for (JecnBaseFlowElementPanel baseFlowElement : listPanel) {
			JecnWorkflowUtil.setCurrentSelect(baseFlowElement);
		}
		// 复制图片到剪贴板
		copyPanelIamge(pathCopy);
	}

	/**
	 * 撤销恢复前数据处理
	 * 
	 * @param undoPanelList
	 *            List<JecnBaseFlowElementPanel> 撤销恢复处理记录的流程元素集合
	 * @param manLineSet
	 *            Set<JecnBaseManhattanLinePanel>选中的是所有连接线和图形相关连接线的集合
	 */
	public static void cutCurSeleteListPanel(List<JecnBaseFlowElementPanel> undoPanelList,
			Set<JecnBaseManhattanLinePanel> manLineSet, boolean isCut) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取复制、剪贴、删除 选中面板集合及图形最大和最小XY坐标
		if (workflow == null || workflow.getCurrentSelectElement().size() == 0) {
			return;
		}

		for (int i = 0; i < workflow.getCurrentSelectElement().size(); i++) {
			JecnBaseFlowElementPanel flowElementPanel = workflow.getCurrentSelectElement().get(i);

			if (isCut && (flowElementPanel instanceof JecnBaseVHLinePanel)) {// 横竖分割线不执行复制、粘贴
				continue;
			}
			if (!undoPanelList.contains(flowElementPanel)) {// 不包含
				undoPanelList.add(flowElementPanel);
			}

			// 获取当前选中图形或线
			if (flowElementPanel instanceof JecnBaseFigurePanel) {
				JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) flowElementPanel;
				// 图形关联的开始连接线
				JecnDraggedFiguresPaintLine.getManhattanLineList(manLineSet, curFigure);
			} else if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {
				JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) flowElementPanel;
				// 记录线段当前状态下数据
				linePanel.curLineSegmentsClone.addAll(linePanel.getLineSegmentsClone(linePanel));
			}
		}

		// *******************撤销恢复*******************//
		for (JecnBaseManhattanLinePanel manhattanLinePanel : manLineSet) {// 图形相关联线
			if (!undoPanelList.contains(manhattanLinePanel)) {// 不包含
				undoPanelList.add(manhattanLinePanel);
			}
		}
	}

	/**
	 * 面板图形元素删除
	 */
	public static void flowElementDelete() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取复制、剪贴、删除 选中面板集合及图形最大和最小XY坐标
		if (workflow == null || workflow.getCurrentSelectElement().size() == 0) {
			return;
		}
		// 用于撤销恢复:记录被操作对象集合
		List<JecnBaseFlowElementPanel> undoPanelList = new ArrayList<JecnBaseFlowElementPanel>();
		// 连接线(记录选中的连接线集合,)
		Set<JecnBaseManhattanLinePanel> manLineSet = new HashSet<JecnBaseManhattanLinePanel>();

		// 撤销恢复前数据处理
		cutCurSeleteListPanel(undoPanelList, manLineSet, false);
		// // 连接线(记录选中的连接线集合,)
		// Set<JecnBaseManhattanLinePanel> manLineSet = new
		// HashSet<JecnBaseManhattanLinePanel>();
		// for (int i = 0; i < workflow.getCurrentSelectElement().size(); i++) {
		// // 获取当前选中图形或线
		// if (workflow.getCurrentSelectElement().get(i) instanceof
		// JecnBaseFigurePanel) {
		// JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) workflow
		// .getCurrentSelectElement().get(i);
		// // 判断是否是关键活动
		// // Common.checkActive(figure, figureList, curCopyList);
		//
		// // 图形关联的开始连接线
		// JecnDraggedFiguresPaintLine.getManhattanLineList(manLineSet,
		// curFigure);
		// } else if (workflow.getCurrentSelectElement().get(i) instanceof
		// JecnBaseManhattanLinePanel) {
		// JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel)
		// workflow
		// .getCurrentSelectElement().get(i);
		// // 记录线段当前状态下数据
		// linePanel.getLineSegmentsClone();
		// }
		// }
		//
		// // 添加所有图形到copyList
		// workflow.addCurCopyList(workflow.getCurrentSelectElement());
		// // 添加剪切图形关联线到copyList
		// workflow.addCurCopyList(manLineSet);

		// *******************撤销恢复*******************//
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		// 记录撤销恢复数据
		redoData.recodeFlowElement(undoPanelList);
		JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createDelete(redoData);
		workflow.getUndoRedoManager().addEdit(undoProcess);
		// *******************撤销恢复*******************//

		// 删除图形元素
		JecnRemoveFlowElementUnit.removeFlowElement(workflow.getCurrentSelectElement());
		// 清空选中图形元素集合
		workflow.getCurrentSelectElement().clear();
	}

	/**
	 * 流程元素粘贴处理
	 * 
	 */
	public static void flowElementPaste() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || workflow.getCurCopyList().size() == 0 || workflow.getCurrentSelectElement() == null) {
			return;
		}
		List<JecnBaseFlowElementPanel> curCopyList = workflow.getCurCopyList();

		// 记录粘贴需要添加的流程元素集合
		List<JecnBaseFlowElementPanel> addListPanel = new ArrayList<JecnBaseFlowElementPanel>();
		// 根据复制或剪切的集合获取需要粘贴的集合数据
		getPasteList(curCopyList, addListPanel);
		// 需要操作的对象集合都为空直接返回
		if (addListPanel.size() == 0) {
			return;
		}

		Point startPoint = workflow.getStartPoint();
		if (startPoint == null) {// 换画图面板粘贴不用点击面板再粘贴
			startPoint = getPaseSelectedPanelPoint(curCopyList);
		}

		int leftX = workflow.getAidCompCollection().getLeftMinX();
		int leftY = workflow.getAidCompCollection().getLeftMinY();
		// if (countPative == -1 && (leftX != startPoint.x)) {
		// // countPative = 0;
		// }
		if (countPative != -1 || (leftX == startPoint.x && leftY == startPoint.y)) {
			countPative = countPative + 50;
		}

		// 获取鼠标按下坐标
		int mouseX = startPoint.x;
		int mouseY = startPoint.y;

		// 泳池锁定，粘贴处理
		if (JecnSystemStaticData.isShowModel()) {
			JecnBaseFlowElementPanel elementPanel = null;
			if (workflow.getCurrentSelectElement().isEmpty()) {
				for (JecnBaseFlowElementPanel jecnBaseFlowElementPanel : curCopyList) {
					if (jecnBaseFlowElementPanel instanceof ModelFigure) {
						elementPanel = jecnBaseFlowElementPanel;
						continue;
					}
				}
			} else if (workflow.getCurrentSelectElement().size() == 1) {
				elementPanel = workflow.getCurrentSelectElement().get(0);
			}
			if (elementPanel != null) {
				countPative = 0;
				JecnSelectFigureXY selectFigureXY = workflow.getFlowMapData().getModelFigureXY();
				startPoint = new Point(selectFigureXY.getMinX(), selectFigureXY.getMaxY());
				if (workflow.getCurrentSelectElement().size() > 0) {
					int localX = elementPanel.getX();
					int localY = elementPanel.getY();
					mouseX = startPoint.x - localX;
					mouseY = startPoint.y - localY;
				} else if (workflow.getCurrentSelectElement().isEmpty()) {
					mouseX = selectFigureXY.getMinX() - 10;
					mouseY = selectFigureXY.getMinY() + selectFigureXY.getMaxY() - selectFigureXY.getMinY() - 10;
				}
			}

		}

		// addListPanel 集合降序排序
		getPanelListByOrder(addListPanel);

		List<JecnBaseManhattanLinePanel> addManLIneList = new ArrayList<JecnBaseManhattanLinePanel>();

		for (JecnBaseFlowElementPanel flowElementPanel : addListPanel) {
			if (flowElementPanel instanceof JecnBaseFigurePanel) {// 如果复制对象为连接线
				// 添加待粘贴的图形到面板
				reSetParamsAndAddFigture((JecnBaseFigurePanel) flowElementPanel, mouseX, mouseY);
			} else if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {// 如果复制对象为连接线
				// 获取连接线对象
				JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) flowElementPanel;
				// 重设ZOrderIndex
				linePanel.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
				// 面板层级计数器+1
				workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + 2);
				// 添加连接线文件显示框
				if (linePanel.getTextPanel() != null) {
					workflow.add(linePanel.getTextPanel());
				}
				// 记录待添加的连接线
				addManLIneList.add(linePanel);
			} else if (flowElementPanel instanceof JecnBaseDividingLinePanel) {// 如果复制对象为连接线
				JecnBaseDividingLinePanel dividingLine = (JecnBaseDividingLinePanel) flowElementPanel;
				// 粘贴分割线处理
				reSetDividingLine(dividingLine, mouseX, mouseY, "figureImageId");
				// 面板层级计数器+1
				workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + 2);
				// 添加图形
				JecnAddFlowElementUnit.addDividingLine(dividingLine);

				if (flowElementPanel instanceof DividingLine) {// 分割线
					// 目的：为了让面板重新设置大小
					dividingLine.setSize(dividingLine.getWidth(), dividingLine.getHeight());
				}
			}
		}
		for (JecnBaseManhattanLinePanel linePanel : addManLIneList) {
			// 添加连接线到面板
			JecnAddFlowElementUnit.addManhattinLine(linePanel);
		}
		workflow.updateUI();

		// *******************撤销恢复*******************//
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		// 记录撤销恢复数据
		redoData.recodeFlowElement(addListPanel);
		JecnUnRedoProcess undoProcess = JecnUnRedoProcessFactory.createAdd(redoData);
		workflow.getUndoRedoManager().addEdit(undoProcess);
		// *******************撤销恢复*******************//
		if (countPative == -1) {
			countPative = 0;
		}
		workflow.getCurrDrawPanelSizeByPanelList();
	}

	/**
	 * 根据复制或剪切的集合获取需要粘贴的集合数据
	 * 
	 * @param curCopyList
	 *            List<JecnBaseFlowElementPanel>复制或剪切的数据对象集合
	 * @param addListPanel
	 *            List<JecnBaseFlowElementPanel>粘贴的数据对象集合
	 */
	public static void getPasteList(List<JecnBaseFlowElementPanel> curCopyList,
			List<JecnBaseFlowElementPanel> addListPanel) {
		// <原图形，原图形克隆图形>：为了下面处理克隆后连接线关联的图形和原连接线关联图形做相应的对应
		Map<JecnBaseFigurePanel, JecnBaseFigurePanel> srcFigureAndFigureCloneMap = new HashMap<JecnBaseFigurePanel, JecnBaseFigurePanel>();
		// 连接线集合
		Set<JecnBaseManhattanLinePanel> manLineSet = new HashSet<JecnBaseManhattanLinePanel>();
		// 图形元素集合
		Set<JecnBaseFigurePanel> figureSet = new HashSet<JecnBaseFigurePanel>();

		for (JecnBaseFlowElementPanel flowElementPanel : curCopyList) {
			if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {// 如果复制对象为连接线
				JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) flowElementPanel;
				if (linePanel.getStartFigure() == null || linePanel.getEndFigure() == null) {
					continue;
				}
				// 添加连接线到连接线集合
				manLineSet.add(linePanel);
				// 添加连接线对应的开始图形和结束图形到图形元素集合
				figureSet.add(linePanel.getStartFigure());
				figureSet.add(linePanel.getEndFigure());
			} else if (flowElementPanel instanceof JecnBaseFigurePanel) {
				JecnBaseFigurePanel baseFigurePanel = (JecnBaseFigurePanel) flowElementPanel;
				// 添加图形元素到图形元素集合
				figureSet.add(baseFigurePanel);
			} else if (flowElementPanel instanceof JecnBaseDividingLinePanel) {// 分割线
				// 分割线
				JecnBaseDividingLinePanel dividingLine = (JecnBaseDividingLinePanel) flowElementPanel;
				// 记录元素对象的克隆对象
				addFlowElement(addListPanel, dividingLine.clone());
			}
		}

		for (JecnBaseFigurePanel baseFigurePanel : figureSet) {
			JecnBaseFigurePanel figurePanelClone = baseFigurePanel.clone();
			// 记录元素对象的克隆对象
			addFlowElement(addListPanel, figurePanelClone);
			// <原图形，原图形克隆图形>：为了下面处理克隆后连接线关联的图形和原连接线关联图形做相应的对应
			srcFigureAndFigureCloneMap.put(baseFigurePanel, figurePanelClone);
		}

		for (JecnBaseManhattanLinePanel manLine : manLineSet) {
			// 添加线段的克隆数据和线段相关图形的克隆对象到 记录粘贴需要添加的流程元素集合
			addCloneManLine(addListPanel, manLine, srcFigureAndFigureCloneMap);
		}
	}

	/**
	 * 流程架构-连接线可单独显示-粘贴特殊处理
	 * 
	 * @param flowElementPanel
	 * @param manLineSet
	 *            待粘贴的连接线集合
	 * @param figureSet
	 *            连接线对应的开始图形和结束图形的集合
	 */
	private static void copyManLineSpecialHandel(JecnBaseManhattanLinePanel linePanel,
			List<JecnBaseFlowElementPanel> addListPanel) {
		// 添加连接线对应的开始图形和结束图形到图形元素集合
		if (linePanel.getStartFigure() instanceof MapLineFigure) {
			linePanel.setStartFigure(linePanel.getStartFigure().clone());
		} else {
			linePanel.setStartFigure(MapManLineCommon.INSTANCE.craeteFigurePanel(linePanel.getStartPoint()));
		}
		if (linePanel.getEndFigure() instanceof MapLineFigure) {
			linePanel.setEndFigure(linePanel.getEndFigure().clone());
		} else {
			linePanel.setEndFigure(MapManLineCommon.INSTANCE.craeteFigurePanel(linePanel.getEndPoint()));
		}
		addListPanel.clear();
		addListPanel.add(linePanel);
		addListPanel.add(linePanel.getEndFigure());
		addListPanel.add(linePanel.getStartFigure());
	}

	/**
	 * 升序排列
	 * 
	 * @param addListPanel
	 */
	public static void getPanelListByOrder(List<JecnBaseFlowElementPanel> addListPanel) {
		// 重排列图形List的层级顺序
		for (int i = 0; i < addListPanel.size(); i++) {
			for (int j = addListPanel.size() - 1; j > i; j--) {
				// 获取图形的层级
				int figureLayer = addListPanel.get(j).getFlowElementData().getZOrderIndex();
				int nextFigureLayer = addListPanel.get(j - 1).getFlowElementData().getZOrderIndex();
				if (figureLayer < nextFigureLayer) {
					JecnBaseFlowElementPanel baseFlowElement = addListPanel.get(j - 1);
					addListPanel.set(j - 1, addListPanel.get(j));
					addListPanel.set(j, baseFlowElement);
				}
			}
		}
	}

	/**
	 * 
	 * 设置cloneFigure对象属性且添加到画板上，此方法只适用于粘贴使用
	 * 
	 * @param cloneFigure
	 *            克隆figure的对象
	 * @param mouseX
	 *            鼠标点击画板的X做坐标
	 * @param mouseY
	 *            鼠标点击画板的Y坐标
	 * @param figureImageId
	 *            图形唯一标识
	 */
	private static void reSetParamsAndAddFigture(JecnBaseFigurePanel cloneFigure, int mouseX, int mouseY) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || workflow.getCurrentSelectElement() == null) {
			return;
		}
		// 重设FigureImageId
		// cloneFigure.setFigureImageId(figureImageId);
		// 重设ZOrderIndex
		cloneFigure.getFlowElementData().setZOrderIndex(workflow.getMaxZOrderIndex());
		workflow.setLayer(cloneFigure, workflow.getMaxZOrderIndex());
		// 面板层级计数器+2
		workflow.setMaxZOrderIndex(workflow.getMaxZOrderIndex() + 2);

		JecnFigureDataCloneable figureDataCloneable = cloneFigure.getZoomJecnFigureDataCloneable();

		// 复制粘贴 选中图形区域X坐标最大值
		int leftMinX = workflow.getAidCompCollection().getCurrLeftMinX();
		// 复制粘贴 选中图形区域Y坐标最大值
		int leftMinY = workflow.getAidCompCollection().getCurrLeftMinY();

		int newcountPative = (countPative == -1) ? 0 : countPative;
		int fX = figureDataCloneable.getX() - leftMinX;
		int fY = figureDataCloneable.getY() - leftMinY;
		fX = (fX < 0) ? 0 : fX;
		fY = (fY < 0) ? 0 : fY;

		// 图形新的位置点X坐标 当前放大缩小倍数下
		int newX = mouseX + fX + newcountPative;
		// 图形新的位置点X坐标
		int newY = mouseY + fY + newcountPative;

		// 设置注释框分割线位置
		// 设置新的位置点
		Point localPoint = new Point();
		localPoint.setLocation(new Point(newX, newY));
		Dimension size = new Dimension();
		// 获取100%状态下的图形
		size.setSize(cloneFigure.getSize().getWidth() / workflow.getWorkflowScale(), cloneFigure.getSize().getHeight()
				/ workflow.getWorkflowScale());
		if (cloneFigure instanceof CommentText) {// 判断是否为注释框
			CommentText commentText = (CommentText) cloneFigure;
			// 执行位移算法
			commentText.isMove();
		}

		// 添加图形
		JecnAddFlowElementUnit.addFigure(localPoint, cloneFigure);
	}

	/**
	 * 粘贴分割线处理
	 * 
	 * @param dividingLine
	 *            DividingLine需要粘贴的分割线
	 * @param mouseX
	 *            int鼠标相对面板的横坐标
	 * @param mouseY
	 *            int鼠标相对面板的纵坐标
	 * @param figureImageId
	 *            String图形元素位移标识位
	 */
	private static void reSetDividingLine(JecnBaseDividingLinePanel dividingLine, int mouseX, int mouseY,
			String figureImageId) {

		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || workflow.getCurrentSelectElement() == null) {
			return;
		}
		// 线条开始点
		int xStart = dividingLine.getStartPoint().x;
		int yStart = dividingLine.getStartPoint().y;

		// 线条结束点
		int xEnd = (int) dividingLine.getEndPoint().getX();
		int yEnd = (int) dividingLine.getEndPoint().getY();

		// 复制粘贴 选中图形区域X坐标最大值
		int leftMinX = workflow.getAidCompCollection().getCurrLeftMinX();
		// 复制粘贴 选中图形区域Y坐标最大值
		int leftMinY = workflow.getAidCompCollection().getCurrLeftMinY();

		// 获取鼠标按下坐标+线条开始点-选中区域中图形或线的最小X坐标
		Point startPos = new Point(mouseX + xStart - leftMinX, mouseY + yStart - leftMinY);
		Point endPos = new Point(mouseX + xEnd - leftMinX, mouseY + yEnd - leftMinY);
		if (dividingLine instanceof DividingLine) {
			// 获取分割线虚拟图形对象
			TempDividingLine tempDividing = dividingLine.createTempLinePanel();
			tempDividing.setStartPoint(startPos);
			tempDividing.setEndPoint(endPos);
		}

		// 记录分割线100%状态下的克隆值
		dividingLine.originalCloneData(startPos, endPos);
	}

	/**
	 * 添加线段的克隆数据和线段相关图形的克隆对象到 记录粘贴需要添加的流程元素集合
	 * 
	 * @param addListPanel
	 *            记录粘贴需要添加的流程元素集合
	 * @param linePanel
	 *            连接线
	 */
	public static void addCloneManLine(List<JecnBaseFlowElementPanel> addListPanel,
			JecnBaseManhattanLinePanel linePanel,
			Map<JecnBaseFigurePanel, JecnBaseFigurePanel> srcFigureAndFigureCloneMap) {

		// 连接线开始图形
		JecnBaseFigurePanel startFigureClone = srcFigureAndFigureCloneMap.get(linePanel.getStartFigure());
		// 连接线结束图形
		JecnBaseFigurePanel endFigureClone = srcFigureAndFigureCloneMap.get(linePanel.getEndFigure());
		// 获取连接线克隆对象
		JecnBaseManhattanLinePanel lineClone = linePanel.clone();
		// // 获取开始图形和线对应的编辑点
		// JecnEditPortPanel startPort = startFigureClone.getEditPortPanel(
		// linePanel.getFlowElementData().getStartEditPointType(),
		// startFigureClone);
		// // 结束图形和线对应的编辑点
		// JecnEditPortPanel endPort = endFigureClone.getEditPortPanel(linePanel
		// .getFlowElementData().getEndEditPointType(), endFigureClone);
		//
		// // 设置克隆连接线对应图形的开始和结束编辑点
		// lineClone.setStartEditPort(startPort);
		// lineClone.setEndEditPort(endPort);

		List<JecnBaseFlowElementPanel> curCopyList = JecnDrawMainPanel.getMainPanel().getWorkflow().getCurCopyList();
		if (isOnlyCopyManLine(curCopyList)) {
			copyManLineSpecialHandel(lineClone, addListPanel);
		} else {
			lineClone.setStartFigure(startFigureClone);
			lineClone.setEndFigure(endFigureClone);
		}

		// 添加流程元素到元素集合
		addFlowElement(addListPanel, lineClone);
	}

	private static boolean isOnlyCopyManLine(List<JecnBaseFlowElementPanel> curCopyList) {
		return curCopyList.size() == 1 && JecnDesignerProcess.getDesignerProcess().isShowLineHasNoFigure();
	}

	/**
	 * 添加流程元素到元素集合
	 * 
	 * @param addListPanel
	 *            记录流程元素集合
	 * @param flowElementPanel
	 *            待添加的流程元素
	 */
	private static void addFlowElement(List<JecnBaseFlowElementPanel> addListPanel,
			JecnBaseFlowElementPanel flowElementPanel) {
		if (!addListPanel.contains(flowElementPanel)) {
			addListPanel.add(flowElementPanel);
		}
	}

	/**
	 * 
	 * 
	 * @param imagePath
	 */
	public static void copyPanelIamge(String imagePath) {
		Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
		ImageTransferable icon = new ImageTransferable(new ImageIcon(imagePath));
		cb.setContents(icon, null);
	}

	/**
	 * 剪贴版临时文件夹是否存在，存在责清除文件夹内文件
	 * 
	 */
	private static void checkDir() {
		// 剪贴板文件夹是否存在
		File fileDir = new File(COPY_PRINT);
		if (!fileDir.exists()) {
			fileDir.mkdir();
		}
		// 删除临时文件
		File[] delFile = fileDir.listFiles();
		for (int i = 0; i < delFile.length; i++) {
			delFile[i].delete();
		}
	}

	/**
	 * 
	 * 返回临时文件名称：文件路径 + 系统毫秒数＋ jpg
	 * 
	 * @return
	 */
	private static String getPathCopy() {
		return TEMP_COPY + DrawCommon.getUUID() + COPY_FILE_TYPE;
	}

	/**
	 * 计算位置点
	 * 
	 * @param pathCopy
	 *            路径
	 */
	public static boolean copyCutLocation(List<JecnBaseFlowElementPanel> listPanel) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || listPanel == null) {
			return false;
		}
		// 获取所有选中组件元素
		List<JecnBaseFlowElementPanel> allComponents = new ArrayList<JecnBaseFlowElementPanel>();
		// workFlow.image = null;
		// 获取所有选中组件元素
		getAllListBySelectList(allComponents);

		// 获取选中图形元素的最大XY坐标和最小XY坐标
		JecnSelectFigureXY figureXY = JecnWorkflowUtil.getMaxSize(workflow, allComponents);

		if (figureXY == null || figureXY.getMaxX() == 0 || figureXY.getMaxY() == 0) {
			return false;
		}
		// 清空选中的图形元素集合的所有显示点 并清空选中集合
		JecnWorkflowUtil.hideAllResizeHandle();

		int minX = figureXY.getMinX();
		int minY = figureXY.getMinY();
		minX = DrawCommon.convertDoubleToInt(minX / workflow.getWorkflowScale());
		minY = DrawCommon.convertDoubleToInt(minY / workflow.getWorkflowScale());

		// 设置粘贴时图形默认位置点
		workflow.setStartPoint(new Point(minX, minY));
		// 获取选中区域的X和Y坐标的最小值
		// 设置选中区域中心点
		workflow.getAidCompCollection().setLeftMinX(minX);
		workflow.getAidCompCollection().setLeftMinY(minY);
		return true;
	}

	/**
	 * 粘贴时计算位置点
	 * 
	 * @param listPanel
	 *            List<JecnBaseFlowElementPanel> 剪切复制图形
	 */
	public static Point getPaseSelectedPanelPoint(List<JecnBaseFlowElementPanel> listPanel) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || listPanel == null) {
			return new Point();
		}

		// 获取选中图形元素的最大XY坐标和最小XY坐标
		JecnSelectFigureXY figureXY = JecnWorkflowUtil.getMaxSize(workflow, listPanel);

		if (figureXY == null) {
			return new Point();
		}
		int minX = figureXY.getMinX();
		int minY = figureXY.getMinY();
		minX = DrawCommon.convertDoubleToInt(minX / workflow.getWorkflowScale());
		minY = DrawCommon.convertDoubleToInt(minY / workflow.getWorkflowScale());

		return new Point(minX, minY);
	}

	/**
	 * 获取复制时的选中区域
	 * 
	 * @param pathCopy
	 *            路径
	 */
	public static boolean copyImageToSystem(String pathCopy, List<JecnBaseFlowElementPanel> listPanel) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || listPanel == null) {
			return false;
		}
		// 获取所有选中组件元素
		List<JecnBaseFlowElementPanel> allComponents = new ArrayList<JecnBaseFlowElementPanel>();
		// workFlow.image = null;
		// 获取所有选中组件元素
		getAllListBySelectList(allComponents);

		int imageWidth = 0;// 图片的宽度
		int imageHeight = 0;// 图片的高度
		// 获取选中图形元素的最大XY坐标和最小XY坐标
		JecnSelectFigureXY figureXY = JecnWorkflowUtil.getMaxSize(workflow, allComponents);

		if (figureXY == null || figureXY.getMaxX() == 0 || figureXY.getMaxY() == 0) {
			return false;
		}
		// 清空选中的图形元素集合的所有显示点 并清空选中集合
		JecnWorkflowUtil.hideAllResizeHandle();

		// 设置粘贴时图形默认位置点
		workflow.setStartPoint(new Point(figureXY.getMinX(), figureXY.getMinY()));

		// 将生成的图片宽度和高度
		imageWidth = figureXY.getMaxX() - figureXY.getMinX();
		imageHeight = figureXY.getMaxY() - figureXY.getMinY();
		// 收集图片数据
		JecnSaveFlowImageJDialog dialog = new JecnSaveFlowImageJDialog();
		// 获取image缓冲区
		BufferedImage image = dialog.drawImage(workflow, figureXY.getMinX(), figureXY.getMinY(), imageWidth,
				imageHeight, figureXY.getMaxX(), figureXY.getMaxY());
		if (image != null) {
			// 图片缓冲区截取圈选的面积大小
			// BufferedImage tempImage = getImageByClip(image,
			// figureXY.getMinX(),
			// figureXY.getMinY(), imageWidth, imageHeight);
			// 通过流的形式JPEGImageEncoder导成图片
			JecnFileUtil.getCreateImage(pathCopy, image);
			// // 获取选中区域的X和Y坐标的最小值
			// setLeftXAndLeftY(figureXY);
			return true;
		}
		return false;
	}

	/**
	 * 获取复制时的选中区域
	 * 
	 * @param pathCopy
	 *            路径(绝对路径)
	 * @param imageWidth
	 *            int 图片宽
	 * @param imageHeight
	 *            int 图片高
	 * 
	 */
	public boolean copyImageToSystem(String pathCopy, int imageWidth, int imageHeight) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || workflow.getCurrentSelectElement() == null) {
			return false;
		}
		// // 获取初始化时网格是否显示
		// boolean showGridFlag = JecnSystemStaticData.isShowGrid();
		// if (showGridFlag) {
		// // 网格不显示
		// JecnSystemStaticData.setShowGrid(false);
		// }
		// // 获取初始的横分割线的状态
		// boolean showShowParallelLine = JecnSystemStaticData
		// .isShowParallelLines();
		// // 获取竖分割线的状态
		// boolean showVerticalLine = JecnSystemStaticData.isShowVerticalLine();
		//
		// JecnSystemStaticData.setShowParallelLines(false);
		// JecnSystemStaticData.setShowVerticalLine(false);
		//
		// // 修改横竖分割线状态
		// workflow.repaint();

		// 获取所有选中组件元素
		List<JecnBaseFlowElementPanel> allComponents = new ArrayList<JecnBaseFlowElementPanel>();
		// 获取所有选中组件元素
		List<JecnBaseFlowElementPanel> tempCurSelectList = new ArrayList<JecnBaseFlowElementPanel>();
		// 获取所有选中组件元素
		getListBySelectList(allComponents);

		tempCurSelectList.addAll(workflow.getCurrentSelectElement());

		// 获取选中图形元素的最大XY坐标和最小XY坐标
		JecnSelectFigureXY figureXY = JecnWorkflowUtil.getWorkflowMaxSize(workflow, allComponents);

		if (figureXY == null) {
			return false;
		}
		// 清空选中的图形元素集合的所有显示点 并清空选中集合
		JecnWorkflowUtil.hideAllResizeHandle();
		// double originalScale = 1 / workflow.getWorkflowScale();

		JecnSaveFlowImageJDialog saveFlowImageJDialog = new JecnSaveFlowImageJDialog();

		// 将生成的图片宽度和高度
		int maxX = figureXY.getMaxX();
		int maxY = figureXY.getMaxY();
		int minX = figureXY.getMinX();
		int minY = figureXY.getMinY();
		if (maxX == 0 || maxY == 0) {
			return false;
		}
		int iWidth = maxX - minX;
		int iHeight = maxY - minY;

		double multiples = 0;
		double doX = iWidth / Double.valueOf(imageWidth);
		double doY = iHeight / Double.valueOf(imageHeight);
		if (doX > doY) {
			multiples = doX;
		} else {
			multiples = doY;
		}
		double doubleXY = 1 / multiples;
		// 获取image缓冲区
		BufferedImage image = saveFlowImageJDialog.drawImage(workflow, minX, minY, iWidth, iHeight, maxX, maxY,
				doubleXY);

		// // 还原网格的初始化状态
		// JecnSystemStaticData.setShowGrid(showGridFlag);
		// // 还原横分割线为原始状态
		// JecnSystemStaticData.setShowParallelLines(showShowParallelLine);
		// // 还原竖分割线的状态
		// JecnSystemStaticData.setShowVerticalLine(showVerticalLine);
		// workflow.repaint();
		if (image != null) {
			// 通过流的形式JPEGImageEncoder导成图片
			JecnFileUtil.getCreateImage(pathCopy, image);
			for (JecnBaseFlowElementPanel baseFlowElement : tempCurSelectList) {
				JecnWorkflowUtil.setCurrentSelect(baseFlowElement);
			}
			return true;
		}
		return false;
	}

	/**
	 * 获取圈选图形元素集合（圈选流程元素时不一定选中连接线相关的图形元素，连接线需做特殊处理）
	 * 
	 * @param allComponents
	 *            List<JecnBaseFlowElementPanel>返回圈选的图形元素集合
	 */
	private static void getAllListBySelectList(List<JecnBaseFlowElementPanel> allComponents) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || workflow.getCurrentSelectElement() == null) {
			return;
		}
		// 获取所有选中组件元素
		for (JecnBaseFlowElementPanel flowElementPanel : workflow.getCurrentSelectElement()) {
			if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {
				// 获取连接线对象
				JecnBaseManhattanLinePanel manLine = (JecnBaseManhattanLinePanel) flowElementPanel;
				// 获取连接线的开始图形和结束图形
				JecnBaseFigurePanel startFigure = manLine.getStartFigure();
				JecnBaseFigurePanel endFigure = manLine.getEndFigure();
				// 添加图形到选中集合（选中连接线时不一定选中连接线对应的图形）
				if (!isOnlyCopyManLine(workflow.getCurrentSelectElement()) && !allComponents.contains(startFigure)) {
					allComponents.add(startFigure);
				}
				if (!isOnlyCopyManLine(workflow.getCurrentSelectElement()) && !allComponents.contains(endFigure)) {
					allComponents.add(endFigure);
				}
				// 添加连接线到选中集合中
				allComponents.add(manLine);
			} else {
				// 添加流程元素到选中集合
				allComponents.add(flowElementPanel);
			}
		}
	}

	/**
	 * 获取圈选图形元素集合（圈选流程元素时不一定选中连接线相关的图形元素，连接线需做特殊处理(不添加连接线)）
	 * 
	 * @param allComponents
	 *            List<JecnBaseFlowElementPanel>返回圈选的图形元素集合
	 */
	public static void getListBySelectList(List<JecnBaseFlowElementPanel> allComponents) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || workflow.getCurrentSelectElement() == null) {
			return;
		}
		// 获取所有选中组件元素
		for (JecnBaseFlowElementPanel flowElementPanel : workflow.getCurrentSelectElement()) {
			if (flowElementPanel instanceof JecnBaseManhattanLinePanel) {
				// 获取连接线对象
				JecnBaseManhattanLinePanel manLine = (JecnBaseManhattanLinePanel) flowElementPanel;
				// 获取连接线的开始图形和结束图形
				JecnBaseFigurePanel startFigure = manLine.getStartFigure();
				JecnBaseFigurePanel endFigure = manLine.getEndFigure();
				// 添加图形到选中集合（选中连接线时不一定选中连接线对应的图形）
				if (!workflow.getCurrentSelectElement().contains(startFigure)
						|| !workflow.getCurrentSelectElement().contains(endFigure)) {
					continue;
				}
				// 添加连接线到选中集合中
				allComponents.add(manLine);
			} else {
				// 添加流程元素到选中集合
				allComponents.add(flowElementPanel);
			}
		}
	}

	/**
	 * 
	 * 格式刷
	 */
	public static void formatPainterFigureAttrs() {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// 画图面板选中流程元素
		List<JecnBaseFlowElementPanel> selectList = workflow.getCurrentSelectElement();

		// 格式刷获取选中元素
		if (selectList.size() != 1) {
			return;
		}

		JecnAbstractFlowElementData formatPainterData = selectList.get(0).getFlowElementData().clone();
		workflow.setFormatPainterData(formatPainterData);
	}

	/**
	 * 单选格式刷
	 * 
	 * @param formatPainterData
	 * @param elementFigure
	 */
	public static void singleFormatPainterFigureAttrs(JecnBaseFlowElementPanel elementFigure) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow.getFormatPainterData() == null) {// 未启用格式刷
			return;
		}
		JecnAbstractFlowElementData formatPainterData = workflow.getFormatPainterData();
		if (elementFigure == null) {
			throw new IllegalArgumentException("singleFormatPainterFigureAttrs param  is error!");
		}
		// 操作前数据
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();

		// 记录操作前数据
		undoData.recodeFlowElement(elementFigure);
		// 复制格式刷数据
		JecnAbstractFlowElementData.setFormatPainterFigureAttrs(formatPainterData, elementFigure.getFlowElementData());
		// 记录操操作后
		redoData.recodeFlowElement(elementFigure);

		if (elementFigure instanceof JecnBaseFigurePanel) {
			JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) elementFigure;
			if (!formatPainterData.isShadowsFlag()) {
				figurePanel.removeRelatedPanel();
			} else if (figurePanel.getShadowPanel() == null) {
				figurePanel.addShadowPanel();
			}
		}
		workflow.repaint();
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		workflow.getUndoRedoManager().addEdit(unredoProcess);

		JecnWorkflowUtil.clearFormatPainter();
	}

	/**
	 * 多选（圈选，格式刷）
	 * 
	 * @param formatPainterFigure
	 * @param elementFigures
	 */
	public static void multipleFormatPainterFigureAttrs(List<JecnBaseFlowElementPanel> elementFigures) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow.getFormatPainterData() == null) {
			return;
		}

		JecnAbstractFlowElementData formatPainterData = workflow.getFormatPainterData();
		if (elementFigures == null || elementFigures.isEmpty()) {
			throw new IllegalArgumentException("multipleFormatPainterFigureAttrs param  is error!");
		}
		// 操作前数据
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();

		// 记录操作前数据
		undoData.recodeFlowElement(elementFigures);
		for (JecnBaseFlowElementPanel elementFigure : elementFigures) {
			// 复制格式刷数据
			JecnAbstractFlowElementData.setFormatPainterFigureAttrs(formatPainterData, elementFigure
					.getFlowElementData());
			if (elementFigure instanceof JecnBaseFigurePanel) {
				JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) elementFigure;
				if (!formatPainterData.isShadowsFlag()) {
					figurePanel.removeRelatedPanel();
				} else if (figurePanel.getShadowPanel() == null) {
					figurePanel.addShadowPanel();
				}
			}
		}
		workflow.repaint();
		// 记录操操作后
		redoData.recodeFlowElement(elementFigures);
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		workflow.getUndoRedoManager().addEdit(unredoProcess);
		JecnWorkflowUtil.clearFormatPainter();
	}

	/**
	 * 
	 * 鼠标点击图形右键菜单记录鼠标点击的临时点
	 * 
	 * @return Point
	 */
	public static Point getEditPastePoint() {
		return editPastePoint;
	}

	public static void setEditPastePoint(Point editPastePoint) {
		JecnFlowElementCopyAndPaste.editPastePoint = editPastePoint;
	}
}
