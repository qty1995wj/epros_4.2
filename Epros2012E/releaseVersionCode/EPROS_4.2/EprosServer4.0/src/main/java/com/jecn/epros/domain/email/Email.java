package com.jecn.epros.domain.email;

import java.util.Date;

/**
 * 邮件主表
 */

public class Email implements java.io.Serializable {


    private String id;
    /**
     * 邮件创建日期
     */
    private Date createDate;
    /**
     * 邮件发送日期
     */
    private Date sendDate;
    /**
     * 发送状态
     */
    private Integer sendState;
    /**
     * 内容表id
     */
    private String contentId;
    /**
     * 收件人id
     */
    private Long peopleId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Integer getSendState() {
        return sendState;
    }

    public void setSendState(Integer sendState) {
        this.sendState = sendState;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public Long getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(Long peopleId) {
        this.peopleId = peopleId;
    }


}