package com.jecn.epros.server.bean.task;

/**
 * 记录所有审核人
 * 
 * @author wanglikai
 * 
 */
public class JecnTaskApprovePeopleConn implements java.io.Serializable {
	/** 主键 */
	private Long id;
	/** 审核人 */
	private Long approvePid;
	/** 任务阶段 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段 */
	/** 操作人 临时 */
	private String approveTemporaryName;
	/** 任务阶段表Id */
	private Long stageId;

	private int state;

	public Long getId() {
		return id;
	}

	public Long getApprovePid() {
		return approvePid;
	}

	public void setApprovePid(Long approvePid) {
		this.approvePid = approvePid;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApproveTemporaryName() {
		return approveTemporaryName;
	}

	public void setApproveTemporaryName(String approveTemporaryName) {
		this.approveTemporaryName = approveTemporaryName;
	}

	public Long getStageId() {
		return stageId;
	}

	public void setStageId(Long stageId) {
		this.stageId = stageId;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

}
