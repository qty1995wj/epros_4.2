package com.jecn.epros.server.bean.task.temp;

import java.util.List;

/**
 * 个任务类型审核阶段名称
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 30, 2012 时间：3:04:13 PM
 */
public class TaskSearchBean {
	/** 流程任务各审核阶段名称 */
	private List<TempAuditPeopleBean> listFlowTaskState;
	/** 流程地图任务各审核阶段名称 */
	private List<TempAuditPeopleBean> listFlowMapTaskState;
	/** 文件任务各审核阶段名称 */
	private List<TempAuditPeopleBean> listFileTaskState;
	/** 制度任务各审核阶段名称 */
	private List<TempAuditPeopleBean> listRuleTaskState;

	public List<TempAuditPeopleBean> getListFlowTaskState() {
		return listFlowTaskState;
	}

	public void setListFlowTaskState(List<TempAuditPeopleBean> listFlowTaskState) {
		this.listFlowTaskState = listFlowTaskState;
	}

	public List<TempAuditPeopleBean> getListFlowMapTaskState() {
		return listFlowMapTaskState;
	}

	public void setListFlowMapTaskState(
			List<TempAuditPeopleBean> listFlowMapTaskState) {
		this.listFlowMapTaskState = listFlowMapTaskState;
	}

	public List<TempAuditPeopleBean> getListFileTaskState() {
		return listFileTaskState;
	}

	public void setListFileTaskState(List<TempAuditPeopleBean> listFileTaskState) {
		this.listFileTaskState = listFileTaskState;
	}

	public List<TempAuditPeopleBean> getListRuleTaskState() {
		return listRuleTaskState;
	}

	public void setListRuleTaskState(List<TempAuditPeopleBean> listRuleTaskState) {
		this.listRuleTaskState = listRuleTaskState;
	}
}
