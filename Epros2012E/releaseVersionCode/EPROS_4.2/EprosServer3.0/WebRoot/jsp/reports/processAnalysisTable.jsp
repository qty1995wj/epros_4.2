<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div style="height: 99%;" align="center">
			<table width="97%" border="0" cellpadding="0" cellspacing="1"
				align="center" bgcolor="#BFBFBF" style="margin: 5px;">
				<tr class="FixedTitleRow" align="center">
					<td width="2%" class="gradient">
					</td>
					<td class="gradient" width="40%" height="25" align="center">
						<!-- 时间类型 -->
						<s:text name="timeType" />
					</td>
					<td class="gradient" width="40%" height="25">
						<!-- 个数 -->
						<s:text name="number" />
					</td>
					<td class="gradient" width="16%" height="25">
						<!-- 操作 -->
						<s:text name="operation" />
					</td>
					<td width="2%" class="gradient">
					</td>
				</tr>
				<s:set id="count" value="1" />
				<s:iterator value="processAnalysisList">
					<s:if test="countTotal>0">
						<tr align="center" bgcolor="#ffffff"
							onmouseover="style.backgroundColor='#EFEFEF';"
							onmouseout="style.backgroundColor='#ffffff';" height="20">
							<td align="right" class="gradient">
								<s:property value="#count" />
								<s:set id="count" value="#count+1" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:if test="timeType==1">
									<s:text name="day" />
								</s:if>
								<s:elseif test="timeType==2">
									<s:text name="weeks" />
								</s:elseif>
								<s:elseif test="timeType==3">
									<s:text name="month" />
								</s:elseif>
								<s:elseif test="timeType==4">
									<s:text name="season" />
								</s:elseif>
								<s:elseif test="timeType==5">
									<s:text name="years" />
								</s:elseif>
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="countTotal" />
							</td>
							<td class="t5" style="word-break: break-all; color: #005EA7;">
								<!-- 详情 -->
								<a style="cursor: pointer;" target='_blank' class="table"
									onclick="findProcessAnalysisSub(<s:property value='timeType'/>)">
									<s:text name="details" /> </a>
							</td>
							<td></td>
						</tr>
					</s:if>
				</s:iterator>
			</table>
		</div>
	</body>
</html>