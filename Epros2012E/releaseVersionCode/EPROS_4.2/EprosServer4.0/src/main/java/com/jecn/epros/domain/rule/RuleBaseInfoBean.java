package com.jecn.epros.domain.rule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.HttpUtil;

import java.util.List;

public class RuleBaseInfoBean {
    private long ruleId;
    /**
     * 制度名称
     */
    private String ruleName;
    /**
     * 制度编号
     */
    private String ruleNum;
    /**
     * 密级
     */
    private int secretLevel;
    /**
     * 制度类别ID
     */
    private Long ruleTypeNameId;
    /**
     * 制度类别
     */
    private String ruleTypeName;
    /**
     * 制度责任人ID
     */
    private LinkResource responsibleLink;
    /**
     * 责任部门
     */
    private long dutyOrgId;
    /**
     * 责任部门
     */
    private String dutyOrg;

    /**
     * 0：知识库关联，1：本地上传
     */
    private int isFileLocal;
    /**
     * 1: 制度模板文件，2：制度文件
     */
    private int nodeType;

    private boolean isPub;

    private long fileId;

    private String expiry;// 有效期
    // 监护人ID
    private LinkResource guardianLink;
    private String businessScope;// 业务范围
    private String otherScope;// 其他范围

    private List<LinkResource> applyOrgs;

    public void setNodeType(int nodeType) {
        this.nodeType = nodeType;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public void setPub(boolean pub) {
        isPub = pub;
    }

    @JsonIgnore
    public int getIsFileLocal() {
        return isFileLocal;
    }

    public String RuleType() {
        return isFileLocal == 1 ? "ruleLocal" : "";
    }

    public void setIsFileLocal(int isFileLocal) {
        this.isFileLocal = isFileLocal;
    }

    public String getSectStr() {
        return ConfigUtils.getSecretByType(secretLevel);
    }

    public LinkResource getDept() {
        return new LinkResource(dutyOrgId, dutyOrg, ResourceType.ORGANIZATION);
    }

    public long getRuleId() {
        return ruleId;
    }

    public void setRuleId(long ruleId) {
        this.ruleId = ruleId;
    }

    public String getHrefURL() {
        if (nodeType != 2) {
            return null;
        }
        return HttpUtil.getDownLoadFileHttp(isFileLocal == 1 ? ruleId : fileId, isPub, RuleType());
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getRuleNum() {
        return ruleNum;
    }

    public void setRuleNum(String ruleNum) {
        this.ruleNum = ruleNum;
    }

    public int getSecretLevel() {
        return secretLevel;
    }

    public void setSecretLevel(int secretLevel) {
        this.secretLevel = secretLevel;
    }

    public Long getRuleTypeNameId() {
        return ruleTypeNameId;
    }

    public void setRuleTypeNameId(Long ruleTypeNameId) {
        this.ruleTypeNameId = ruleTypeNameId;
    }

    public String getRuleTypeName() {
        return ruleTypeName;
    }

    public void setRuleTypeName(String ruleTypeName) {
        this.ruleTypeName = ruleTypeName;
    }

    @JsonIgnore
    public long getDutyOrgId() {
        return dutyOrgId;
    }

    public void setDutyOrgId(long dutyOrgId) {
        this.dutyOrgId = dutyOrgId;
    }

    @JsonIgnore
    public String getDutyOrg() {
        return dutyOrg;
    }

    public void setDutyOrg(String dutyOrg) {
        this.dutyOrg = dutyOrg;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getBusinessScope() {
        return businessScope;
    }

    public void setBusinessScope(String businessScope) {
        this.businessScope = businessScope;
    }

    public String getOtherScope() {
        return otherScope;
    }

    public void setOtherScope(String otherScope) {
        this.otherScope = otherScope;
    }

    public List<LinkResource> getApplyOrgs() {
        return applyOrgs;
    }

    public void setApplyOrgs(List<LinkResource> applyOrgs) {
        this.applyOrgs = applyOrgs;
    }

    public LinkResource getGuardianLink() {
        return guardianLink;
    }

    public void setGuardianLink(LinkResource guardianLink) {
        this.guardianLink = guardianLink;
    }

    public LinkResource getResponsibleLink() {
        return responsibleLink;
    }

    public void setResponsibleLink(LinkResource responsibleLink) {
        this.responsibleLink = responsibleLink;
    }
}
