package epros.designer.gui.rule.mode;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleModeBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 新建制度模板目录
 * @author 
 * 2012-05-09
 *
 */
public class AddRuleModelDirNameDialog extends JecnEditNameDialog {
	private static final long serialVersionUID = -8261538098605650769L;
	private static Logger log = Logger.getLogger(AddRuleModelDirNameDialog.class);

	private JecnTreeNode pNode = null;
	private JTree jTree = null;
	
	public AddRuleModelDirNameDialog(JecnTreeNode pNode, JTree jTree){
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}
	
	

	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addDirI");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}


	@Override
	public void saveData() {
		RuleModeBean ruleModeBean = new RuleModeBean();
		//设置模板目录名称
		ruleModeBean.setModeName(getName());
		//设置模板名称类型   0：目录   1：模板
		ruleModeBean.setModeType(0);
		//设置创建人
		ruleModeBean.setCreatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		//设置更新人
		ruleModeBean.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		//设置父ID
		ruleModeBean.setParentId(pNode.getJecnTreeBean().getId());
		ruleModeBean.setSortId(JecnTreeCommon.getMaxSort(pNode));
		
		
		try {
			Long id = ConnectionPool.getRuleModeAction().addRuleModeDir(ruleModeBean);
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.ruleModeDir);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("AddRuleModelDirNameDialog saveData is error",e);
		}
		
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name,TreeNodeType.ruleModeDir);
	}
	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
