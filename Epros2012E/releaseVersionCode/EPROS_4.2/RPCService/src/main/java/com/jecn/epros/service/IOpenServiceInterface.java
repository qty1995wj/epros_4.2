package com.jecn.epros.service;

import com.jecn.epros.bean.ByteFileResult;

import java.util.Map;

/**
 * 对外接口
 * Created by ZXH on 2018/5/22.
 */
public interface IOpenServiceInterface {
    /**
     * 根据用户登录名获取可查阅的制度
     *
     * @param userCode 登录名
     * @param PNUM     待获取的制度数，-1：查询所有
     * @return
     */
    String findRuleListsByUserCode(String userCode, int PNUM);

    /**
     * 获取 系统 管理员 设计管理员 数量 以及秘钥使用信息
     *
     * @return    Map<String, String> : ADMIN_NUM : 管理员数量 总数
     *                                   DESIGN_NUM : 流程设计专家 总数
     *                                   DESIGN_NUM_USE:流程设计专家 使用数
     *                                   KEY_TYPE : 秘钥类型 永久 or 临时
     *                                   KEY_EXPIRY_TIME : 秘钥到期时间
     */
    Map<String, String> findSytemKeyAndUserNumber();

    /**
     *  获取设计器的安装包
     * @return
     */
    ByteFileResult downLoadEprosDesigner();
}
