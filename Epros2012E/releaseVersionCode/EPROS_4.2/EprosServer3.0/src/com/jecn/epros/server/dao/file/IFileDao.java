package com.jecn.epros.server.dao.file;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.file.JecnFileBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.IBaseDao;

public interface IFileDao extends IBaseDao<JecnFileBeanT, Long> {
	/**
	 * @author yxw 2012-7-3
	 * @description:删除文件
	 * @param setFileIds
	 *            文件IDS
	 * @param setDirIds
	 *            文件夹IDS
	 * @param setLocalFileIds
	 *            要删除的文件BEAN 用于删除本地的文件
	 * @throws Exception
	 */
	public void deleteFiles(Set<Long> setFileIds, Set<Long> setDirIds) throws Exception;

	/**
	 * @author yxw 2012-7-3
	 * @description:删除文件
	 * @param setFileIds
	 *            文件IDS
	 * @param setDirIds
	 *            文件夹IDS
	 * @param listDeleteBean
	 *            要删除的文件BEAN 用于删除本地的文件
	 * @throws Exception
	 */
	public void deleteFiles(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-7-3
	 * @description:获得文件对象
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	public JecnFileBean findJecnFileBeanById(Long fileId) throws Exception;

	/**
	 * @author yxw 2012-7-3
	 * @description:撤销多个文件
	 * @param setFileIds
	 * @throws Exception
	 */
	public void revokeFiles(List<Long> listFileIds) throws Exception;

	/**
	 * @author yxw 2012-7-3
	 * @description:发布多个文件
	 * @param listFileIds
	 * @throws Exception
	 */
	public void releaseFiles(List<Long> listFileIds) throws Exception;

	/**
	 * 获得未发布的文件
	 * 
	 * @param setFileIds
	 * @return
	 * @throws Exception
	 */
	public List<Long> getNoPubFiles(Set<Long> setFileIds) throws Exception;

	/**
	 * 根据文件id获得临时bean(未删除的)
	 * 
	 * @author huoyl
	 * @param fileId
	 *            文件id
	 * @return
	 * @throws Exception
	 */
	public JecnFileBeanT findJecnFileBeanTById(Long fileId) throws Exception;

	public List<JecnTaskHistoryNew> getTaskHistoryNewListById(long fileId) throws Exception;

	public List<Object[]> getRoleAuthChildFiles(Long pId, Long projectId, Long peopleId);
	
	List<Object[]> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception;
}
