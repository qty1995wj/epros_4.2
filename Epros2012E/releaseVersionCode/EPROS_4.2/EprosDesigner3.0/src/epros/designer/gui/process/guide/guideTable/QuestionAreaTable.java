package epros.designer.gui.process.guide.guideTable;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.gui.process.guide.DesignerProcessOperationIntrusDialog;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 问题区域表格
 * 
 * @author 2012-07-05
 * 
 */
public class QuestionAreaTable extends JTable implements MouseListener {
	/** 初始化传入的数据 */
	private List<JecnActivityShowBean> listPAActivityShow;
	/** 活动说明 */
	private DesignerProcessOperationIntrusDialog processOperationIntrusDialog;

	/**
	 * 初始化JTable
	 * 
	 * @param listPAActivityShow
	 *            用来初始化显示的数据
	 */
	public QuestionAreaTable(List<JecnActivityShowBean> listPAActivityShow,
			DesignerProcessOperationIntrusDialog processOperationIntrusDialog) {
		this.processOperationIntrusDialog = processOperationIntrusDialog;
		this.listPAActivityShow = listPAActivityShow;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
		this.addMouseListener(this);
	}

	/**
	 * jTable中显示的数据
	 * 
	 * @author fuzhh Aug 28, 2012
	 * @return
	 */
	public QuestionAreaTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activitiesNumbers"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activityTitle"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("activityIndicatingThat"));
		title.add(JecnResourceUtil.getJecnResourceUtil().getValue("note"));
		// 获取操作说明数据
		Vector<Vector<Object>> rowData = new Vector<Vector<Object>>();
		for (JecnActivityShowBean activityShowBean : listPAActivityShow) {
			Vector<Object> row = new Vector<Object>();
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFlowElementUUID());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getActivityNum());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getFigureText());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShow());
			row.add(activityShowBean.getActiveFigure().getFlowElementData().getAvtivityShowAndControl());
			rowData.add(row);
		}
		return new QuestionAreaTableMode(rowData, title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}

	class QuestionAreaTableMode extends DefaultTableModel {
		public QuestionAreaTableMode(Vector<Vector<Object>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// 判断为左键双击
		if (e.getClickCount() == 2 && e.getButton() == 1) {
			int selectRow = this.getSelectedRow();
			DesignerActivitiesDetailsDialog drawJecnActiveDetailsDialog = new DesignerActivitiesDetailsDialog(
					listPAActivityShow.get(selectRow), processOperationIntrusDialog);
			drawJecnActiveDetailsDialog.setVisible(true);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}
}
