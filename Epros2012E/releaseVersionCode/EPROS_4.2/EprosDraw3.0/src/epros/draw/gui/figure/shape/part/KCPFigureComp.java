package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseKeyActiveFigure;

/**
 * 
 * 关键合规点 （关键合规点Key compliance point）
 * 
 * @date： 日期：07-02, 2018 时间：10:40:29 AM
 * @author ZHAGNXH
 */
public class KCPFigureComp extends JecnBaseKeyActiveFigure {

	public KCPFigureComp(JecnFigureData keyActiveData) {
		super(keyActiveData);
	}

}
