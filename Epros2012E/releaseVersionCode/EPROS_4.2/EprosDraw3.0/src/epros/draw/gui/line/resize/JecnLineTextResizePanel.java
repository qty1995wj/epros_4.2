package epros.draw.gui.line.resize;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;

import epros.draw.gui.line.JecnLineTextPanel;
import epros.draw.gui.swing.JecnBaseResizePanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 
 * 线三个选中点，其具有改变线大小、位置点的功能
 * 
 * @author ZHOUXY
 * 
 */
public class JecnLineTextResizePanel extends JecnBaseResizePanel {
	private JecnLineTextPanel textPanel;

	private Point originalPoint;

	public JecnLineTextResizePanel(JecnLineTextPanel textPanel) {
		this.textPanel = textPanel;
		this.setBackground(Color.YELLOW);
		this.setSize(8, 8);
		setMouseCursor();
	}

	/**
	 * 鼠标移动到线段显示点，鼠标状态设置
	 * 
	 * @param lineDirectionEnum
	 */
	public void setMouseCursor() {
		this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
	}

	public JecnLineTextPanel getTextPanel() {
		return textPanel;
	}

	public Point getZoomCenterPoint() {
		if (originalPoint == null) {
			return null;
		}
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		return new Point(DrawCommon.convertDoubleToInt(originalPoint.getX() * scale) + this.getWidth() / 2, DrawCommon
				.convertDoubleToInt(originalPoint.getY() * scale)
				+ this.getHeight() / 2);
	}

	public void setLocation(int x, int y) {
		super.setLocation(x, y);
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		originalPoint = new Point(DrawCommon.convertDoubleToInt(x / scale), DrawCommon.convertDoubleToInt(y / scale));
	}
}
