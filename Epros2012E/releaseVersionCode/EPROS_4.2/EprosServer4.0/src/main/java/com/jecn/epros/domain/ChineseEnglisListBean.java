package com.jecn.epros.domain;

import com.jecn.epros.domain.process.Inventory;

/**
 * 中英文报表统计
 * Created by Administrator on 2018/9/5/005.
 */
public class ChineseEnglisListBean {
    //名称
    private String name;
    //主键
    private Long id;
    // 中文 统计条数
    private Integer zhCount;
    // 英文 统计条数
    private Integer enCount;
    //所属父级别
    private String pName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getZhCount() {
        return zhCount;
    }

    public void setZhCount(Integer zhCount) {
        this.zhCount = zhCount;
    }

    public Integer getEnCount() {
        return enCount;
    }

    public void setEnCount(Integer enCount) {
        this.enCount = enCount;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }
}
