function processActivitiesGrid() {
	var processActivitiesGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				baseParams:{
						processId : document.getElementById("processId_id").value,
						startNum : document.getElementById("startNum").value,
						endNum : document.getElementById("endNum").value
				},
				proxy : new Ext.data.HttpProxy({
							url : "processActivitiesDetails.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "flowId",
									mapping : "flowId"
								}, {
									name : "flowName",
									mapping : "flowName"
								}, {
									name : "flowIdInput",
									mapping : "flowIdInput"
								}, {
									name : "resPeopleName",
									mapping : "resPeopleName"
								}, {
									name : "orgName",
									mapping : "orgName"
								}])

			});

	var processActivitiesGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processActivitiesRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			}

	]);
	var processActivitiesGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : processActivitiesGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	var processActivitiesGridPanel = new Ext.grid.GridPanel({
				id : "processActivitiesGridPanel_id",
				renderTo : 'processActivitiesGrid_id',
				height :  document.documentElement.clientHeight-25,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : processActivitiesGridStore,
				colModel : processActivitiesGridColumnModel,
				bbar : processActivitiesGridBbar
			})
}

// 加载流程活动数统计
function processActivitiesPieCharts(){
	$("#sysGrid_id").children().remove();
	$("#sysGrid_id").load('processActivities.action',{
	"processId":document.getElementById("processId_id").value
	  },function(){
		setReportsDivWidthHeight('sysGrid_id');
	});
}

// 流程活动重置
function processActivitiesReset(){
		document.getElementById("processName_id").value = "";
		document.getElementById("processId_id").value = "-1";
}

/**
 * 流程名称增加超链接 用于Ext Grid
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 * @return {}
 */
function processActivitiesRenderer(value, cellmeta, record){
	 return "<div  ext:qtip='"+value+"'><a target='_blank' class='a-operation' href='process.action?type=process&flowId="+record.data['flowId']+"' >"+value+"</a>";
}

/**
 * 流程活动数详情链接
 **/
function findProcessActivitiesSub(statrNum,endNum){
   var processId = document.getElementById("processId_id").value;
   window.open(basePath+"processActivitiesDetailsLink.action?processId="+processId+"&statrNum="+statrNum+"&endNum="+endNum)
}

// 流程活动数统计下载
function processActivitiesDownloadExcel(){
	var processId = document.getElementById("processId_id").value;
	window.location.href="downloadProcessActivities.action?processId="+processId;
}