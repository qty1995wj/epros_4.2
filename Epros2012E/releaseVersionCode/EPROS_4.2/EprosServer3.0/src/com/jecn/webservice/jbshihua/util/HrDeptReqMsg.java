package com.jecn.webservice.jbshihua.util;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DATAINFOS")
public class HrDeptReqMsg
{
  private String uuid;
  private List<HrDeptMsg> depts;
  
  @XmlAttribute(name="uuid")
  public String getUuid()
  {
    return this.uuid;
  }
  
  public void setUuid(String uuid)
  {
    this.uuid = uuid;
  }
  
  @XmlElement(name="DATAINFO")
  public List<HrDeptMsg> getDepts()
  {
    return this.depts;
  }
  
  public void setDepts(List<HrDeptMsg> depts)
  {
    this.depts = depts;
  }
}
