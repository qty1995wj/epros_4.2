package com.jecn.epros.server.service.system.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.system.JecnMessageReaded;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.system.IReadMessageDao;
import com.jecn.epros.server.service.system.IReadMessageService;
import com.jecn.epros.server.util.JecnUtil;

@Transactional
public class ReadMessageServiceImpl implements IReadMessageService {
	private IReadMessageDao readMessageDao;
	

	public IReadMessageDao getReadMessageDao() {
		return readMessageDao;
	}


	public void setReadMessageDao(IReadMessageDao readMessageDao) {
		this.readMessageDao = readMessageDao;
	}


	@Override
	public List<JecnMessageReaded> getAllReadMessage(int startNum,int limit,Long inceptPeopleId) throws Exception {
		String sql = "select jm.message_id,jm.people_id,"
			+ "jm.message_content," 
			+ "jm.message_topic," 
			+ "jm.or_read," 
			+ " jm.incept_people_id,"
			+ " jm.create_time," 
			+ "ju.LOGIN_NAME" 
			+ " from jecn_message_readed jm, jecn_user ju " 
			+ "where jm.incept_people_id = ju.people_id and jm.incept_people_id='"+inceptPeopleId+"'";
		List<Object[]> listObjects = readMessageDao.listNativeSql(sql, startNum,
				limit);
		List<JecnMessageReaded> listResult = new ArrayList<JecnMessageReaded>();
		for (Object[] obj : listObjects) {
			JecnMessageReaded messageReaded = getMessageWebBeanByObject(obj);
			if (messageReaded != null) {
				listResult.add(messageReaded);
			}
		}
		return listResult;
	}
	public JecnMessageReaded getMessageWebBeanByObject(Object[] obj) {
		if (obj == null || obj[0] == null || obj[4] == null) {
			return null;
		}

		SimpleDateFormat newDate = new SimpleDateFormat("yyyy-MM-dd");
		JecnMessageReaded jecnMessageReaded = new JecnMessageReaded();
		// 主键ID
		jecnMessageReaded.setMessageId(Long.valueOf(obj[0].toString()));
		// 人员ID
		if (obj[1] != null) {
			jecnMessageReaded.setPeopleId(Long.valueOf(obj[1].toString()));
		} else {
			jecnMessageReaded.setPeopleId(null);
		}
		// 信息内容
		if (obj[2] != null) {
			jecnMessageReaded.setMessageContent(obj[2].toString());
		} else {
			jecnMessageReaded.setMessageContent("");
		}
		// 信息标题
		if (obj[3] != null) {
			jecnMessageReaded.setMessageTopic(obj[3].toString());
		} else {
			jecnMessageReaded.setMessageTopic("");
		}
		//是否已读
		if (obj[4] != null) {
			jecnMessageReaded.setNoRead(Long.valueOf(obj[4].toString()));
		} else {
			jecnMessageReaded.setNoRead(null);;
		}
		//收件人ID
		if (obj[5] != null) {
			jecnMessageReaded.setInceptPeopleId(Long.valueOf(obj[5].toString()));
		} else {
			jecnMessageReaded.setInceptPeopleId(null);;
		}
		//创建日期
		if (obj[6] != null) {
			try {
				jecnMessageReaded.setCreateTime(newDate.parse(obj[6].toString()));
			} catch (ParseException e) {
			}
		} else {
			jecnMessageReaded.setNoRead(null);;
		}
		// 人员名称和发送日期
		if (obj[7] != null || obj[6] != null) {
			jecnMessageReaded.setPeopleTime(JecnUtil.getValue("messageSystem")+"/"+obj[6].toString());
		} else {
			jecnMessageReaded.setPeopleTime(null);
		}
		return jecnMessageReaded;
	}

	@Override
	public int getTotalMessageList(Long inceptPeopleId) throws Exception {
		String sql = "select count (MESSAGE_ID) from JECN_MESSAGE_READED where INCEPT_PEOPLE_ID = "+inceptPeopleId;
		return readMessageDao.countAllByParamsNativeSql(sql);
	}


	@Override
	public JecnMessageReaded searchReadMesgById(Long messageId) throws Exception {
//		String sql = "select * from JECN_MESSAGE_READED where JECN_MESSAGE_READED = ?";
		return readMessageDao.get(messageId);
	}


	@Override
	public void deleteReadMesgs(List<Long> mesgIds) throws Exception {
		// 删除项目
		Set<Long> mesgIdSet =  new HashSet<Long>();
		for(Long mesgId : mesgIds){
			mesgIdSet.add(mesgId);
		}
		String hql = "delete from JecnMessageReaded where messageId in";
		List<String> list = JecnCommonSql.getSetSqls(hql, mesgIdSet);
		for (String str : list) {
			readMessageDao.execteHql(str);
		}
	}


	@Override
	public void addReadMessage(JecnMessageReaded messageReaded)
			throws Exception {
		readMessageDao.save(messageReaded);
	}

}
