package com.jecn.epros.domain.task;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.process.ProceeRuleTypeBean;
import com.jecn.epros.util.ConfigUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class SimpleTaskRelatedResource {

    /**
     * 节点ID
     */
    private Long id;
    /**
     * PRF节点名称
     */
    private String name;
    /**
     * 父节节点名称
     */
    private String preName;
    /**
     * 编号
     */
    private String number;
    /**
     * 术语定义
     */
    private String definitionStr;
    /**
     * PRF类别
     */
    private List<ProceeRuleTypeBean> listTypeBean;

    /**
     * PRF当前类别
     */
    private String typeId = "-1";

    private String typeName;

    /**
     * 密级
     */
    private String strPublic;
    /**
     * 查阅权限组织ID集合 ','分隔
     */
    private String orgIds;
    /**
     * 查阅权限岗位ID集合 ','分隔
     */
    private String posIds;
    /**
     * 查阅权限相关部门名称‘/’分离
     */
    private String orgNames;
    /**
     * 查阅权限相关岗位名称‘/’分离
     */
    private String posNames;

    /**
     * 显示的部门查阅权限名称
     */
    private String tableOrgNames;
    /**
     * 显示的岗位查阅权限名称
     */
    private String tablePosNames;
    /**
     * 查阅权限组岗位ID集合 ','分隔
     */
    private String groupIds;
    /**
     * 查阅权限组相关部门名称‘/’分离
     */
    private String groupNames;
    /**
     * 显示的岗位组查阅权限名称
     */
    private String tableGroupNames;
    /**
     * 目的
     */
    private String purpose;
    /**
     * 适用范围
     */
    private String applicability;
    /**
     * 保密级别
     */
    private String secretLevel;

    private LinkResource link;

    private LinkResource linkImage;


    public String getSecret() {
        return ConfigUtils.getSecretByType(strPublic);
    }

    public String getTypeStr() {
        if (listTypeBean == null || listTypeBean.size() == 0) {
            return typeName;
        }
        for (ProceeRuleTypeBean typeBean : listTypeBean) {
            if (Long.valueOf(typeId).equals(typeBean.getTypeId())) {
                return typeBean.getTypeName();
            }
        }
        return "";
    }

    public List<Secret> getSecrets() {
        List<Secret> secrets = new ArrayList<>();
        Secret sec = new Secret(0, ConfigUtils.getSecretByType("0"));
        Secret pub = new Secret(1, ConfigUtils.getSecretByType("1"));
        secrets.add(sec);
        secrets.add(pub);
        return secrets;
    }

    class Secret {

        private Integer type;
        private String name;

        public Secret(Integer type, String name) {
            this.type = type;
            this.name = name;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDefinitionStr() {
        return definitionStr;
    }

    public void setDefinitionStr(String definitionStr) {
        this.definitionStr = definitionStr;
    }

    public List<ProceeRuleTypeBean> getListTypeBean() {
        return listTypeBean;
    }

    public void setListTypeBean(List<ProceeRuleTypeBean> listTypeBean) {
        this.listTypeBean = listTypeBean;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }


    public String getStrPublic() {
        return strPublic;
    }

    public void setStrPublic(String strPublic) {
        this.strPublic = strPublic;
    }

    public String getOrgIds() {
        return orgIds;
    }

    public void setOrgIds(String orgIds) {
        this.orgIds = orgIds;
    }

    public String getPosIds() {
        return posIds;
    }

    public void setPosIds(String posIds) {
        this.posIds = posIds;
    }

    public String getOrgNames() {
        return orgNames;
    }

    public void setOrgNames(String orgNames) {
        this.orgNames = orgNames;
    }

    public String getPosNames() {
        return posNames;
    }

    public void setPosNames(String posNames) {
        this.posNames = posNames;
    }

    public String getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(String groupIds) {
        this.groupIds = groupIds;
    }

    public String getGroupNames() {
        return groupNames;
    }

    public void setGroupNames(String groupNames) {
        this.groupNames = groupNames;
    }

    public String getTableGroupNames() {
        return tableGroupNames;
    }

    public void setTableGroupNames(String tableGroupNames) {
        this.tableGroupNames = tableGroupNames;
    }

    public String getTableOrgNames() {
        tableOrgNames = rebackReplaceString(orgNames);
        return tableOrgNames;
    }

    public void setTableOrgNames(String tableOrgNames) {
        this.tableOrgNames = tableOrgNames;
    }

    public String getTablePosNames() {
        tablePosNames = rebackReplaceString(posNames);
        return tablePosNames;
    }

    public void setTablePosNames(String tablePosNames) {
        this.tablePosNames = tablePosNames;
    }

    private String rebackReplaceString(String str) {
        if (StringUtils.isBlank(str)) {
            return str;
        }
        return str.replaceAll("\n", "<br>");
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getApplicability() {
        return applicability;
    }

    public void setApplicability(String applicability) {
        this.applicability = applicability;
    }

    public String getSecretLevel() {
        return secretLevel;
    }

    public void setSecretLevel(String secretLevel) {
        this.secretLevel = secretLevel;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public LinkResource getLink() {
        return link;
    }

    public void setLink(LinkResource link) {
        this.link = link;
    }

    public LinkResource getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(LinkResource linkImage) {
        this.linkImage = linkImage;
    }
}
