package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

import java.io.Serializable;

public class JecnStationMatch implements Serializable {
	private Long id;
	private Long stationId;
	private String stationMatchNames;// 存的数据库临时Id
	private String stationName;
	private String stationMatchIds;// 显示的名字

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getStationMatchNames() {
		return stationMatchNames;
	}

	public void setStationMatchNames(String stationMatchNames) {
		this.stationMatchNames = stationMatchNames;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getStationMatchIds() {
		return stationMatchIds;
	}

	public void setStationMatchIds(String stationMatchIds) {
		this.stationMatchIds = stationMatchIds;
	}
}