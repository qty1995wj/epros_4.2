package com.jecn.epros.server.action.web.login.ad.yuexiu;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jecn.epros.server.common.HttpRequestUtil;

public class YueXiuCSLonin {
	public static String getTicket(final String server, final String username, final String password,
			final String service) throws Exception {
		notNull(server, "server must not be null");
		notNull(username, "username must not be null");
		notNull(password, "password must not be null");
		notNull(service, "service must not be null");

		return getServiceTicket(server, getTicketGrantingTicket(server, username, password), service);
	}

	/**
	 * 取得ST
	 * 
	 * @param server
	 * @param ticketGrantingTicket
	 * @param service
	 * @throws IOException
	 */
	private static String getServiceTicket(final String server, final String ticketGrantingTicket, final String service)
			throws IOException {
		if (ticketGrantingTicket == null)
			return null;
		return HttpRequestUtil.sendPost(server + "/" + ticketGrantingTicket, "service=" + service);
	}

	/**
	 * 取得TGT
	 * 
	 * @param server
	 * @param username
	 * @param password
	 * @throws IOException
	 */
	private static String getTicketGrantingTicket(final String server, final String username, final String password)
			throws IOException {

		String response = HttpRequestUtil.sendPost(server, "username=" + username + "&password=" + password);
		final Matcher matcher = Pattern.compile(".*action=\".*/(.*?)\".*").matcher(response);
		if (matcher.matches()) {
			return matcher.group(1);
		}
		return null;
	}

	public static boolean ticketValidate(String serverValidate, String serviceTicket, String service) throws Exception {
		notNull(serviceTicket, "paramter 'serviceTicket' is not null");
		notNull(service, "paramter 'service' is not null");

		String response = HttpRequestUtil.sendPost(serverValidate + "?" + "ticket=" + serviceTicket + "&service="
				+ URLEncoder.encode(service, "UTF-8"), "");
		System.out.println(response);
		if(response.contains("authenticationSuccess")){
			return true;
		}
		return false;
	}

	private static void notNull(final Object object, final String message) {
		if (object == null)
			throw new IllegalArgumentException(message);
	}

	public static void main(String[] args) {
		try {
			// https://ssodev.yuexiu.com/cas/v1/tickets
			final String server = "https://ssodev.yuexiuproperty.cn/cas/v1/tickets";
			final String username = "luoxiaofeng";
			final String password = "Yxdc@1234";
			final String service = "http://10.2.128.188:8080/login.action"; //
			// 对接系统登录地址
			final String proxyValidate = "https://ssodev.yuexiuproperty.cn/cas/proxyValidate";

			ticketValidate(proxyValidate, getTicket(server, username, password, service), service);

			// Map<String, String> map = new HashMap<String, String>();
			// map.put("username", "luoxiaofeng");
			// map.put("password", "Yxdc@1234");
			//
			// String result =
			// HttpRequestUtil.sendPost("https://ssodev.yuexiuproperty.cn/cas/v1/tickets",
			// "username=luoxiaofeng&password=Yxdc@1234");
			// System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
