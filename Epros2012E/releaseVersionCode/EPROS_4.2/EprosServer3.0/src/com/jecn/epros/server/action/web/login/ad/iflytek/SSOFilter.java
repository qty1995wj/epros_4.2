///*
// *  Copyright (c) 2018 IFlyTek . All rights reserved.<br>
// */
//
//package com.jecn.epros.server.action.web.login.ad.iflytek;
//
//import java.io.IOException;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.util.AntPathMatcher;
//import org.springframework.util.PathMatcher;
//
//import com.ifly.qxb.uap.client.constants.SSOConstants;
//import com.ifly.qxb.uap.client.entity.SSOUser;
//import com.ifly.qxb.uap.client.properties.PropertiesCacheUtil;
//
///**
// * <p>
// * <code>SSOFilter</code>
// * </p>
// * Description:sso登陆拦截
// * 
// * @author jwsong
// * @date 2018/3/29 16:21
// */
//public class SSOFilter implements Filter {
//
//	/**
//	 * sso地址
//	 */
//	private String ssoUrl;
//
//	/**
//	 * 集成客户端地址
//	 */
//	private String clientUrl;
//
//	/**
//	 * 证书路径
//	 */
//	private String keystorePath;
//
//	/**
//	 * 证书密码
//	 */
//	private String keystorePwd;
//
//	/**
//	 * 不拦截的地址
//	 */
//	private String[] excludeUrls;
//
//	/**
//	 * 检查间隔时间 单位：s
//	 */
//	private long refreshInterval;
//
//	private PathMatcher pathMatcher;
//
//	private static final Logger LOGGER = LoggerFactory.getLogger(SSOFilter.class);
//
//	@Override
//	public void init(FilterConfig filterConfig) throws ServletException {
//		pathMatcher = new AntPathMatcher();
//		String configPath = filterConfig.getInitParameter("configPath");
//		this.keystorePath = PropertiesCacheUtil.getValue("sso.keystorePath", configPath);
//		this.keystorePwd = PropertiesCacheUtil.getValue("sso.keystorePwd", configPath);
//		this.ssoUrl = PropertiesCacheUtil.getValue("sso.ssoUrl", configPath);
//		this.clientUrl = PropertiesCacheUtil.getValue("sso.clientUrl", configPath);
//		this.refreshInterval = Long.parseLong(PropertiesCacheUtil.getValue("sso.refreshInterval", configPath));
//		String excludeUrl = PropertiesCacheUtil.getValue("sso.excludeUrl", configPath);
//		if (StringUtils.isNotEmpty(excludeUrl)) {
//			excludeUrls = excludeUrl.split(",");
//		}
//
//		// https证书设置
//		System.clearProperty("javax.net.ssl.trustStore");
//		String trustStore = Thread.currentThread().getContextClassLoader().getResource("/").getPath() + keystorePath;
//		System.setProperty("javax.net.ssl.trustStore", trustStore);
//		System.setProperty("javax.net.ssl.trustStorePassword", keystorePwd);
//	}
//
//	@Override
//	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
//			throws IOException, ServletException {
//		final HttpServletRequest request = (HttpServletRequest) servletRequest;
//		final HttpServletResponse response = (HttpServletResponse) servletResponse;
//
//		// 不需要拦截
//		if (null != excludeUrls) {
//			for (String item : excludeUrls) {
//				if (pathMatcher.match(item, request.getServletPath())) {
//					filterChain.doFilter(servletRequest, servletResponse);
//					return;
//				}
//			}
//		}
//
//		// 获取session
//		SSOUser ssoUser = (SSOUser) request.getSession().getAttribute(SSOConstants.SSO_USER);
//		// 已登录
//		if (null != ssoUser && StringUtils.isNotEmpty(ssoUser.getAccountName())) {
//			// 检查缓存时间 判断是否需要重新检测登陆状态
//			long exitTimes = (System.currentTimeMillis() - ssoUser.getTimestamp()) / 1000;
//			if (exitTimes < refreshInterval) {
//				filterChain.doFilter(servletRequest, servletResponse);
//				return;
//			}
//		}
//		String ticket = "ticket";
//		if (StringUtils.isNotEmpty(ticket)) {
//			// 验证st
//			try {
//				SSOUser user = new SSOUser();
//				user.setTimestamp(System.currentTimeMillis());
//				user.setAccountName("admin");
//				// if (validateST(ticket, user, request)) {
//				request.getSession().setAttribute(SSOConstants.SSO_USER, user);
//				response.sendRedirect(request.getRequestURL().toString());
//				return;
//				// } else {
//				// // 验证不通过 返回到无权限页面
//				// response.sendError(HttpServletResponse.SC_FORBIDDEN);
//				// return;
//				// }
//			} catch (Exception e) {
//				LOGGER.error("验证ticket失败", e);
//				// 验证失败
//				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
//			}
//		} else {
//			// 跳转单点登录
//			if (isAjax(request)) {
//				response.setHeader("sessionstatus", "timeout");
//				filterChain.doFilter(servletRequest, servletResponse);
//				return;
//			} else {
//				response.sendRedirect(String.format("%s/login?service=%s", ssoUrl, getSSOServiceUrl(request)));
//			}
//
//		}
//	}
//
//	//
//	// /**
//	// * 验证ticket
//	// *
//	// * @param ticket
//	// * sso返回票据
//	// * @param ssoUser
//	// * sso用户信息
//	// * @return 是否成功
//	// * @author jwsong
//	// * @date 2018/3/31 10:13
//	// */
//	// private boolean validateST(String ticket, SSOUser ssoUser,
//	// HttpServletRequest request) throws Exception {
//	// String url = String.format("%s/p3/serviceValidate?service=%s&ticket=%s",
//	// ssoUrl, getSSOServiceUrl(request),
//	// ticket);
//	// LOGGER.info("ticket验证：" + url);
//	// HttpGet httpGet = new HttpGet(url);
//	// HttpClient client = HttpClientBuilder.create().build();
//	// HttpResponse response = client.execute(httpGet);
//	// String content = EntityUtils.toString(response.getEntity(), "utf-8");
//	// LOGGER.info("ticket验证结果：" + content);
//	// if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//	// DocumentBuilderFactory documentBuilderFactory =
//	// DocumentBuilderFactory.newInstance();
//	// Document document = documentBuilderFactory.newDocumentBuilder().parse(
//	// new ByteArrayInputStream(content.getBytes("utf-8")));
//	// ssoUser.setUserAccount(document.getElementsByTagName("cas:userAccount").item(0).getTextContent());
//	// ssoUser.setUserName(document.getElementsByTagName("cas:userName").item(0).getTextContent());
//	// ssoUser.setUserSource(Integer.valueOf(document.getElementsByTagName("cas:userSource").item(0)
//	// .getTextContent()));
//	// ssoUser.setUserId(document.getElementsByTagName("cas:userId").item(0).getTextContent());
//	// ssoUser.setTimestamp(System.currentTimeMillis());
//	// return true;
//	// }
//	// return false;
//	// }
//	//
//	// /**
//	// * 判断是否登陆
//	// *
//	// * @param request
//	// * @return
//	// * @author jwsong
//	// * @date 2018/4/13 16:54
//	// */
//	// private boolean isLogin(HttpServletRequest request) throws IOException {
//	// String ip = StringUtils.isEmpty(request.getHeader("x-forwarded-for")) ?
//	// request.getRemoteAddr() : request
//	// .getHeader("x-forwarded-for");
//	// String browser = request.getHeader("User-Agent");
//	// String userSignId = Base64Utils.encodeToString(String.format("%s%s", ip,
//	// browser).getBytes());
//	// SSOUser tempUser = (SSOUser)
//	// request.getSession().getAttribute(SSOConstants.SSO_USER);
//	// String url =
//	// String.format("%s/loginState/check?userSignId=%s&userAccount=%s", ssoUrl,
//	// userSignId, tempUser
//	// .getUserAccount());
//	// LOGGER.info("登陆状态验证：" + url);
//	// HttpGet httpGet = new HttpGet(url);
//	// HttpClient client = HttpClientBuilder.create().build();
//	// HttpResponse response = client.execute(httpGet);
//	// String content = EntityUtils.toString(response.getEntity(), "utf-8");
//	// LOGGER.info("登陆状态验证结果：" + content);
//	// if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//	// if (Boolean.valueOf(content)) {
//	// // 刷新人员session
//	// tempUser.setTimestamp(System.currentTimeMillis());
//	// return true;
//	// }
//	// }
//	// return false;
//	// }
//
//	/**
//	 * 获取sso跳转地址
//	 * 
//	 * @param request
//	 *            http请求
//	 * @return String
//	 * @author jwsong
//	 * @date 2018/4/9 15:45
//	 */
//	private String getSSOServiceUrl(HttpServletRequest request) {
//		return StringUtils.isEmpty(this.clientUrl) ? request.getRequestURL().toString() : clientUrl;
//	}
//
//	/**
//	 * 判断是否是ajax请求
//	 * 
//	 * @param request
//	 *            请求
//	 * @return
//	 * @author jwsong
//	 * @date 2018/4/11 15:40
//	 */
//	private boolean isAjax(HttpServletRequest request) {
//		return request.getHeader("X-Requested-With") != null
//				&& "XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"));
//	}
//
//	@Override
//	public void destroy() {
//
//	}
//}
