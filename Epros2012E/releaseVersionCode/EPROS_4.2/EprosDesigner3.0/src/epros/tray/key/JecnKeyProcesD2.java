package epros.tray.key;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.Key;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;

import com.sun.org.apache.xml.internal.security.utils.Base64;

import epros.designer.gui.JecnD2;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnOPUtil;
import epros.draw.util.JecnResourceUtil;

public class JecnKeyProcesD2 {
	private final String ALGORITHM = "AES";
	private final String HEX_MATCH = "0123456789ABCDEF";
	private final String KEY_TYPE = "D2";

	/** 密钥内容 */
	private String value = null;
	/** 解密需要的key */
	private String decodeKey = null;

	/** 解密文件路径 */
	private static String decodeKeyPath = null;
	/** 秘钥文件路径 */
	private static String valuePath = null;

	/**
	 * 
	 * 验证是否通过
	 * 
	 * @return String 通过返回NULL，没有通过返回提示信息
	 */
	String checkKey() {
		// 读取文件内容：文件不存在或为空之间返回
		String info = readFileValue();
		if (info != null) {
			return JecnResourceUtil.getJecnResourceUtil().getValue(info)
					+ JecnResourceUtil.getJecnResourceUtil().getValue("compContactWay");
		}

		return null;
	}

	/**
	 * 
	 * 解密文件路径
	 * 
	 * @return
	 */
	String getDecodeKeyPath() {
		if (DrawCommon.isNullOrEmtryTrim(decodeKeyPath)) {
			String path = getSyncPath();
			if (!DrawCommon.isNullOrEmtryTrim(path)) {
				decodeKeyPath = path + "images/word.gif";
				JecnKeyCheckD2.printLog("decodeKeyPath=" + decodeKeyPath);
			}
		}
		return decodeKeyPath;
	}

	/**
	 * 
	 * 
	 * 秘钥文件路径
	 * 
	 * @return String
	 */
	static String getValuePath() {
		if (DrawCommon.isNullOrEmtryTrim(valuePath)) {
			String path = getSyncPath();
			if (!DrawCommon.isNullOrEmtryTrim(path)) {
				valuePath = path + "keylist.properties";
				JecnKeyCheckD2.printLog("valuePath=" + valuePath);
			}
		}
		return valuePath;
	}

	/**
	 * 
	 * 读取文件内容
	 * 
	 * @return String 验证失败：返回错误信息key，成功:返回NULL
	 */
	private String readFileValue() {
		// 获取密钥文件内容
		getFileValue();

		if (DrawCommon.isNullOrEmtryTrim(value) || DrawCommon.isNullOrEmtryTrim(decodeKey)) {// 为空
			JecnKeyCheckD2.printLog("JecnKeyProcesD2 readFileValue is null.value=" + value + ";decodeKey=" + decodeKey);
			return "infoNull";
		}
		// 解密
		try {
			value = decrypt(value, getSecretKey(decodeKey));
		} catch (Exception e) {
			JecnKeyCheckD2.printLog("", e);
			return "infoFomcatError";
		}
		if (value == null) {
			JecnKeyCheckD2.printLog("JecnKeyProcesD2 readFileValue is nullvalue=" + value);
			return "infoFomcatError";
		}
		// 密钥版本验证
		if (!keyTypeCheck()) {
			JecnKeyCheckD2.printLog("JecnKeyProcesD2 readFileValue is error");
			return "infoFomcatError";
		}

		if (value.length() >= 17 && checkIsNumber(value.substring(0, 18))) {// 试用
			if (isDateValid()) {// True：验证通过
				JecnD2.isTmpKey = true;
				return null;
			} else {
				return "infoDataError";
			}
		} else {// 正式
			if (isMacValid()) {// True：验证通过
				JecnD2.isTmpKey = false;
				return null;
			} else {
				return "infoFomcatError";
			}
		}
	}

	/**
	 * MAC地址验证
	 * 
	 * @return
	 */
	private boolean isMacValid() {
		// 验证是否通过：true通过；false不通过
		boolean flag = false;
		if (DrawCommon.isNullOrEmtryTrim(value) && value.length() < 27) {
			return flag;
		}
		String strMac = value.substring(10, 27);

		// 获取所有MAC地址，结构：A,B,C
		String macStr = getMACAddress();
		if (DrawCommon.isNullOrEmtryTrim(macStr)) {
			return flag;
		}
		// 拆分MAC地址
		String[] allMACAddress = macStr.split(",");

		for (int i = 0; i < allMACAddress.length; i++) {
			// MAC地址是否和密钥中MAC相等
			if (strMac.equals(allMACAddress[i])) {
				flag = true;
				break;
			}
		}

		return flag;
	}

	/**
	 * 
	 * 获取本机的MAC地址
	 * 
	 * @return
	 */
	private String getMACAddress() {
		// MAC地址
		String address = null;

		// 操作系统名称
		String os = System.getProperty("os.name");
		if (DrawCommon.isNullOrEmtryTrim(os)) {
			return address;
		} else if (os.startsWith("Windows")) {// Windows系统
			address = JecnOPUtil.getWindowsMACAddress();
		} else {// linux系统
			address = JecnOPUtil.getLinuxMACAddress();
		}
		return address;
	}

	/**
	 * 日期验证
	 * 
	 * @return
	 */
	private boolean isDateValid() {
		String dateValue = getDateStr();
		// 使用天数
		// 时间格式化对象
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		// 时间验证 获取秘密开始时间
		Date date = null;
		try {
			date = sdf.parse(dateValue);
			if (new Date().after(date)) {// 是否到期
				// 到期
				return false;
			}
			return true;
		} catch (ParseException e) {
			JecnKeyCheckD2.printLog("", e);
		}
		return false;
	}

	/**
	 * 获取日期
	 * 
	 * @return
	 */
	private String getDateStr() {
		return value.substring(0, 8);
	}

	/**
	 * 
	 * 获取给定注册文件内容
	 * 
	 */
	private void getFileValue() {
		// 解密key获取
		String ret = getfileContent(getDecodeKeyPath(), "key.word");
		if (DrawCommon.isNullOrEmtryTrim(ret)) {// 文件不存在，内容为空
			return;
		}
		decodeKey = ret;

		// 秘钥获取
		ret = getfileContent(getValuePath(), "key.license");
		if (DrawCommon.isNullOrEmtryTrim(ret)) {// 文件不存在，内容为空
			return;
		}
		value = ret;
	}

	/**
	 * 
	 * 获取给定文件内容
	 * 
	 * @param path
	 *            String 文件所在路径
	 * @param key
	 *            String 文件内容key
	 * @return String 文件内容之
	 */
	private String getfileContent(String path, String key) {
		if (DrawCommon.isNullOrEmtryTrim(path) || DrawCommon.isNullOrEmtryTrim(key)) {
			return null;
		}
		Properties properties = readPropertiesForFileName(path);
		if (properties == null) {// 文件不存在
			return null;
		}
		// 文件内容
		String keyWOrd = properties.getProperty(key);
		if (!DrawCommon.isNullOrEmtryTrim(keyWOrd)) {// 空
			return keyWOrd;
		}
		return null;
	}

	/**
	 * 判断是否为数字
	 * 
	 * @param curStr
	 *            传入的参数
	 * @return
	 */
	private boolean checkIsNumber(String curStr) {
		if (curStr.matches("[0-9]*")) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 校验密钥，提示用户
	 * 
	 * @return String 返回提示信息或NULL
	 */
	String checkKeyTipUser() {
		String lastData = isLastData();
		if (!DrawCommon.isNullOrEmtryTrim(lastData)) {
			return JecnResourceUtil.getJecnResourceUtil().getValue("infoVailBefore") + lastData
					+ JecnResourceUtil.getJecnResourceUtil().getValue("infoVailAfter")
					+ JecnResourceUtil.getJecnResourceUtil().getValue("compContactWay");
		}
		return null;
	}

	private boolean isTest() {
		return checkIsNumber(value.substring(0, 12));
	}

	/**
	 * 
	 * 当密钥有效期只有一个月就开始提示密钥快过期，知道密钥过期位置
	 * 
	 * @param valueArray
	 *            String[]
	 * @return String 有效期时间
	 */
	private String isLastData() {
		if (!isTest()) {
			return null;
		}
		// 开始时间
		String dateStr = getDateStr();
		// 时间格式化对象
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

		try {
			// 时间验证 获取秘密到期时间
			Date date = format.parse(dateStr);
			Calendar maxVailedC = Calendar.getInstance();
			maxVailedC.setTime(date);

			// // 开始时间+使用天数
			// maxVailedC.add(Calendar.DATE, Integer.parseInt(days)); // 日期加

			Calendar upMaxVailedC = (Calendar) maxVailedC.clone();
			// 获取最大有效时间的上一月
			upMaxVailedC.add(Calendar.MONTH, -1);

			Calendar sysCurrC = Calendar.getInstance();

			// 最大有效期前一个月<M<最大有效期
			String lastData = null;
			if (sysCurrC.after(upMaxVailedC) && sysCurrC.before(maxVailedC)) {
				lastData = format.format(maxVailedC.getTime());
			}

			return lastData;

		} catch (ParseException e) {
			JecnKeyCheckD2.printLog("", e);
		} catch (Exception e) {
			JecnKeyCheckD2.printLog("", e);
		}
		return null;
	}

	/**
	 * <p>
	 * BASE64字符串解码为二进制数据
	 * </p>
	 * 
	 * @param base64
	 * @return
	 * @throws Exception
	 */
	private byte[] decode(String base64) throws Exception {
		return Base64.decode(base64.getBytes());
	}

	/**
	 * BASE64解密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private byte[] decryptBASE64(String key) throws Exception {
		return (new BASE64Decoder()).decodeBuffer(key);
	}

	/**
	 * <p>
	 * 二进制数据编码为BASE64字符串
	 * </p>
	 * 
	 * @param bytes
	 * @return
	 * @throws Exception
	 */
	private String encode(byte[] bytes) throws Exception {
		return new String(Base64.encode(bytes));
	}

	/**
	 * 密钥版本验证
	 * 
	 * @return
	 */
	private boolean keyTypeCheck() {
		// 密钥版本 P1，或P2 大版本为P2
		String keyType = value.substring(value.lastIndexOf(KEY_TYPE), value.length() - 13);
		if (keyType.equals(KEY_TYPE)) {
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * 解密
	 * </p>
	 * 
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private String decrypt(String data, String key) throws Exception {
		// 可逆
		byte[] byteData = hexStringToByte(data);
		byte[] decrData = decryptBASE64(new String(byteData));
		Key k = toKey(decode(key));
		byte[] raw = k.getEncoded();
		SecretKeySpec secretKeySpec = new SecretKeySpec(raw, ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
		return new String(cipher.doFinal(decrData));
	}

	/**
	 * * 将16进制字符串转换成字节数组 *
	 * 
	 * @param str
	 *            String
	 * @return byte[]
	 */
	private byte[] hexStringToByte(String str) {
		int len = (str.length() / 2);
		byte[] result = new byte[len];
		char[] charArray = str.toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (HEX_MATCH.indexOf(charArray[pos]) << 4 | HEX_MATCH.indexOf(charArray[pos + 1]));
		}
		return result;
	}

	/**
	 * <p>
	 * 生成密钥
	 * </p>
	 * 
	 * @param seed
	 *            密钥种子
	 * @return
	 * @throws Exception
	 */
	private String getSecretKey(String seed) throws Exception {
		KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
		SecureRandom secureRandom;
		if (seed != null && !"".equals(seed)) {
			secureRandom = new SecureRandom(seed.getBytes());
		} else {
			secureRandom = new SecureRandom();
		}
		keyGenerator.init(128, secureRandom);
		SecretKey secretKey = keyGenerator.generateKey();
		return encode(secretKey.getEncoded());
	}

	/**
	 * <p>
	 * 转换密钥
	 * </p>
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private Key toKey(byte[] key) throws Exception {
		SecretKey secretKey = new SecretKeySpec(key, ALGORITHM);
		return secretKey;
	}

	/**
	 * 
	 * 获取EprosServer3.0所在目录
	 * 
	 * @return
	 */
	private static String getSyncPath() {
		// 路径
		String path = null;

		// 获取路径
		URL url = JecnKeyProcesD2.class.getResource("");
		if (url == null) {
			JecnKeyCheckD2.printLog("JecnKeyProcesD2 getSyncPath is error");
			return path;
		}

		try {
			path = url.toURI().getPath();
		} catch (URISyntaxException e) {
			JecnKeyCheckD2.printLog("", e);
			// 这儿一般是不进入此分支的
			path = url.getPath().replaceAll("%20", " ");
		}
		if (path != null) {
			path = path.substring(0, path.lastIndexOf("epros/tray/key/")) + "server/jecn/EprosServer3.0/";
		}
		JecnKeyCheckD2.printLog("getSyncPath" + path);
		return path;
	}

	/**
	 * 
	 * 获取给定的property文件内容
	 * 
	 * 返回为null时读取文件失败
	 * 
	 * @param propertyFileName
	 *            property文件名称
	 * @return Properties Properties对象或NULL
	 */
	private Properties readPropertiesForFileName(String path) {
		// 属性对象
		Properties properties = new Properties();
		FileInputStream inputStream = null;

		try {
			JecnKeyCheckD2.printLog("path=" + path);
			// 读取文件
			inputStream = new FileInputStream(path);
			properties.load(inputStream);
		} catch (FileNotFoundException e) {
			JecnKeyCheckD2.printLog("", e);
			return null;
		} catch (IOException e) {
			JecnKeyCheckD2.printLog("", e);
			return null;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					JecnKeyCheckD2.printLog("", e);
					return null;
				}
			}
		}
		return properties;
	}

	/**
	 * 
	 * 把文件存储到本地(本地文件时在当前系统根目录下)
	 * 
	 * @param upLoadFile
	 *            待保存文件
	 * @param localPath
	 *            目的地路径
	 * @return String 成功:true，失败:false
	 */
	static boolean writeFileToLocal(File upLoadFile, String localPath) {
		JecnKeyCheckD2.printLog("upLoadFile=" + upLoadFile + ";localPath=" + localPath);
		if (upLoadFile == null || localPath == null) {
			return false;
		}

		// 本地的输出流
		FileOutputStream outStream = null;
		// 远程excel输入流
		FileInputStream inputStream = null;
		try {
			File fileLocal = new File(localPath);
			// 判断文件是否存在
			if (!fileLocal.exists()) {
				fileLocal.createNewFile();
			}

			// 本地的输出流
			outStream = new FileOutputStream(fileLocal);

			// 远程excel输入流
			inputStream = new FileInputStream(upLoadFile);

			// 读取给定的文件数据存入到指定目录
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}

			return true;

		} catch (IOException e) {
			JecnKeyCheckD2.printLog("", e);
		} finally {
			if (outStream != null) {
				try {
					outStream.close();
				} catch (IOException e) {
					JecnKeyCheckD2.printLog("", e);
				}
			}

			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					JecnKeyCheckD2.printLog("", e);
				}
			}
		}

		return false;
	}
}
