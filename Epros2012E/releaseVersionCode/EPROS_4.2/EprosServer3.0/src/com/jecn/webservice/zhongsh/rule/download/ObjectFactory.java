
package com.jecn.webservice.zhongsh.rule.download;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.jecn.webservice.zhongsh.rule.download package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.jecn.webservice.zhongsh.rule.download
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UploadSystemToPdfResponse }
     * 
     */
    public UploadSystemToPdfResponse createUploadSystemToPdfResponse() {
        return new UploadSystemToPdfResponse();
    }

    /**
     * Create an instance of {@link GetDocumentBySystemId }
     * 
     */
    public GetDocumentBySystemId createGetDocumentBySystemId() {
        return new GetDocumentBySystemId();
    }

    /**
     * Create an instance of {@link DownloadSystemByDocIdResponse }
     * 
     */
    public DownloadSystemByDocIdResponse createDownloadSystemByDocIdResponse() {
        return new DownloadSystemByDocIdResponse();
    }

    /**
     * Create an instance of {@link UploadSystem }
     * 
     */
    public UploadSystem createUploadSystem() {
        return new UploadSystem();
    }

    /**
     * Create an instance of {@link UploadSystemToPdfQuickResponse }
     * 
     */
    public UploadSystemToPdfQuickResponse createUploadSystemToPdfQuickResponse() {
        return new UploadSystemToPdfQuickResponse();
    }

    /**
     * Create an instance of {@link DownloadSystemDocumentBySystemId }
     * 
     */
    public DownloadSystemDocumentBySystemId createDownloadSystemDocumentBySystemId() {
        return new DownloadSystemDocumentBySystemId();
    }

    /**
     * Create an instance of {@link UploadSystemDocument }
     * 
     */
    public UploadSystemDocument createUploadSystemDocument() {
        return new UploadSystemDocument();
    }

    /**
     * Create an instance of {@link DeleteSystemDocumentResponse }
     * 
     */
    public DeleteSystemDocumentResponse createDeleteSystemDocumentResponse() {
        return new DeleteSystemDocumentResponse();
    }

    /**
     * Create an instance of {@link DownloadSystem }
     * 
     */
    public DownloadSystem createDownloadSystem() {
        return new DownloadSystem();
    }

    /**
     * Create an instance of {@link DownloadSystemDocumentBySystemIdResponse }
     * 
     */
    public DownloadSystemDocumentBySystemIdResponse createDownloadSystemDocumentBySystemIdResponse() {
        return new DownloadSystemDocumentBySystemIdResponse();
    }

    /**
     * Create an instance of {@link DeleteSystemDocumentBySystemIdResponse }
     * 
     */
    public DeleteSystemDocumentBySystemIdResponse createDeleteSystemDocumentBySystemIdResponse() {
        return new DeleteSystemDocumentBySystemIdResponse();
    }

    /**
     * Create an instance of {@link HelloWorld }
     * 
     */
    public HelloWorld createHelloWorld() {
        return new HelloWorld();
    }

    /**
     * Create an instance of {@link UploadSystemToPdf }
     * 
     */
    public UploadSystemToPdf createUploadSystemToPdf() {
        return new UploadSystemToPdf();
    }

    /**
     * Create an instance of {@link DownloadSystemResponse }
     * 
     */
    public DownloadSystemResponse createDownloadSystemResponse() {
        return new DownloadSystemResponse();
    }

    /**
     * Create an instance of {@link DeleteSystemDocument }
     * 
     */
    public DeleteSystemDocument createDeleteSystemDocument() {
        return new DeleteSystemDocument();
    }

    /**
     * Create an instance of {@link UploadSystemResponse }
     * 
     */
    public UploadSystemResponse createUploadSystemResponse() {
        return new UploadSystemResponse();
    }

    /**
     * Create an instance of {@link UploadSystemToPdfQuick }
     * 
     */
    public UploadSystemToPdfQuick createUploadSystemToPdfQuick() {
        return new UploadSystemToPdfQuick();
    }

    /**
     * Create an instance of {@link DownloadSystemByDocId }
     * 
     */
    public DownloadSystemByDocId createDownloadSystemByDocId() {
        return new DownloadSystemByDocId();
    }

    /**
     * Create an instance of {@link UploadSystemDocumentResponse }
     * 
     */
    public UploadSystemDocumentResponse createUploadSystemDocumentResponse() {
        return new UploadSystemDocumentResponse();
    }

    /**
     * Create an instance of {@link GetDocumentBySystemIdResponse }
     * 
     */
    public GetDocumentBySystemIdResponse createGetDocumentBySystemIdResponse() {
        return new GetDocumentBySystemIdResponse();
    }

    /**
     * Create an instance of {@link HelloWorldResponse }
     * 
     */
    public HelloWorldResponse createHelloWorldResponse() {
        return new HelloWorldResponse();
    }

    /**
     * Create an instance of {@link DeleteSystemDocumentBySystemId }
     * 
     */
    public DeleteSystemDocumentBySystemId createDeleteSystemDocumentBySystemId() {
        return new DeleteSystemDocumentBySystemId();
    }

}
