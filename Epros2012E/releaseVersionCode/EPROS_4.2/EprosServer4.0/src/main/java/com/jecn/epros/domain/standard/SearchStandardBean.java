package com.jecn.epros.domain.standard;

import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.ConfigUtils;

public class SearchStandardBean {
    /**
     * 名称
     **/
    private String name;
    /**
     * 类型
     **/
    private int standType;

    /**
     * 0是目录,1文件标准,2流程标准,3.流程地图标准,4标准条款,5条款要求
     **/
    public String getStandTypeStr() {
        return Common.standardTypeVal(standType);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStandType() {
        return standType;
    }

    public void setStandType(int standType) {
        this.standType = standType;
    }
}
