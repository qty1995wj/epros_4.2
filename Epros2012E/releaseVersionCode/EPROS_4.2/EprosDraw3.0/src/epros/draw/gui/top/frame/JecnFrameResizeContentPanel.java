package epros.draw.gui.top.frame;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.error.ErrorInfoConstant;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 窗体内容面板:改变窗体大小的面板
 * 
 * 拖动窗体边框改变大小
 * 
 * 根据鼠标进入窗体的方向指定对应的鼠标类型，根据鼠标类型改变窗体大小或位置点
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFrameResizeContentPanel extends JPanel implements
		MouseMotionListener, MouseListener {
	/** 日志对象 */
	private final Log log = LogFactory
			.getLog(JecnFrameResizeContentPanel.class);
	/** 叫边长度 */
	private final int panelHorn = 6;

	private JecnFrame frame = null;
	/** 按下鼠标坐标 */
	private Point pressPoint;

	public JecnFrameResizeContentPanel(JecnFrame frame) {
		if (frame == null) {
			log.error("JecnFrameResizeContentPanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.frame = frame;

		initComponent();
	}

	private void initComponent() {
		// 透明
		this.setOpaque(false);
		// 事件
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}

	public void mouseMoved(MouseEvent e) {
		if (frame == null || !frame.isResizable() || frame.isMaxFlag()
				|| frame.getExtendedState() == JFrame.MAXIMIZED_BOTH) {// 窗体最大化或不支持改变窗体大小
			return;
		}

		// 根据鼠标进入区域设置对应的鼠标类型
		if (e.getSource() instanceof JecnFrameResizeContentPanel) {
			JecnFrameResizeContentPanel resizePanel = (JecnFrameResizeContentPanel) e
					.getSource();
			// 获取鼠标进入面板的鼠标类型：上、下、左、右、左上、左下、右上、右下
			Cursor resizeCursor = getResizeCursor(e.getX(), e.getY());
			if (resizePanel.getCursor() != resizeCursor) {
				// 设置鼠标类型
				resizePanel.setCursor(resizeCursor);
			}
		}
	}

	public void mouseDragged(MouseEvent e) {
		if (frame.getExtendedState() == JFrame.MAXIMIZED_BOTH
				|| !frame.isResizable() || frame.isMaxFlag()) {// 窗体最大化或不支持改变窗体大小
			return;
		}

		if (e.getSource() instanceof JecnFrameResizeContentPanel) {
			JecnFrameResizeContentPanel resizePanel = (JecnFrameResizeContentPanel) e
					.getSource();
			// 改变窗体的大小和位置点
			resizeJecnFrame(e.getX(), e.getY(), resizePanel);
		}

	}

	public void mouseExited(MouseEvent e) {
		this.frame.getCurrContent().setCursor(Cursor.getDefaultCursor());
		this.frame.getFrameHeaderPanel().setCursor(Cursor.getDefaultCursor());
	}

	public void mousePressed(MouseEvent e) {
		pressPoint = e.getPoint();

	}

	public void mouseClicked(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {
	}

	private void resizeJecnFrame(int x, int y,
			JecnFrameResizeContentPanel resizePanel) {
		if (pressPoint == null) {
			return;
		}

		double minWidth = JecnUIUtil.getEprosDrawSize().width;
		int minHeight = JecnUIUtil.getEprosDrawSize().height;
		if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.N_RESIZE_CURSOR)) {// 北面进入拖动
			// 偏移量
			int offsetY = y - pressPoint.y;
			int nH = frame.getHeight() - offsetY;
			if (nH <= minHeight) {
				return;
			}
			// 改变大小
			frame.setBounds(frame.getX(), frame.getY() + offsetY, frame
					.getWidth(), nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.E_RESIZE_CURSOR)) {// 东面进入拖动
			int offsetX = x - pressPoint.x;
			int nW = frame.getWidth() + offsetX;
			if (nW < minWidth) {
				return;
			}
			pressPoint.x += offsetX;
			// 改变大小
			frame.setSize(nW, frame.getHeight());
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.S_RESIZE_CURSOR)) {// 南面进入拖动
			int offset = y - pressPoint.y;
			int nH = frame.getHeight() + offset;
			if (nH <= minHeight) {
				return;
			}
			pressPoint.y += offset;
			frame.setSize(frame.getWidth(), nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.W_RESIZE_CURSOR)) {// 西面进入拖动
			int offset = x - pressPoint.x;
			int nW = frame.getWidth() - offset;
			if (nW < minWidth) {
				return;
			}
			frame.setBounds(frame.getX() + offset, frame.getY(), nW, frame
					.getHeight());
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR)) {// 东北面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;
			int nW = frame.getWidth() + xoffset;
			int nH = frame.getHeight() - yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}
			pressPoint.x += xoffset;
			frame.setBounds(frame.getX(), frame.getY() + yoffset, nW, nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR)) {// 东南面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;

			int nW = frame.getWidth() + xoffset;
			int nH = frame.getHeight() + yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}

			pressPoint.x += xoffset;
			pressPoint.y += yoffset;
			frame.setBounds(frame.getX(), frame.getY(), nW, nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR)) {// 西南面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;

			int nW = frame.getWidth() - xoffset;
			int nH = frame.getHeight() + yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}

			pressPoint.y += yoffset;
			frame.setBounds(frame.getX() + xoffset, frame.getY(), nW, nH);
		} else if (resizePanel.getCursor() == Cursor
				.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR)) {// 西北面
			int xoffset = x - pressPoint.x;
			int yoffset = y - pressPoint.y;
			int nW = frame.getWidth() - xoffset;
			int nH = frame.getHeight() - yoffset;
			if (nH <= minHeight || nW < minWidth) {
				return;
			}
			frame.setBounds(frame.getX() + xoffset, frame.getY() + yoffset, nW,
					nH);
		}
	}

	/**
	 * 
	 * 获取鼠标进入面板的鼠标类型：上、下、左、右、左上、左下、右上、右下
	 * 
	 * 
	 * @param x
	 *            鼠标点击X坐标
	 * @param y
	 *            鼠标点击Y坐标
	 * @return Cursor 返回鼠标类型
	 */
	private Cursor getResizeCursor(int x, int y) {
		// 左边进入
		boolean left = isInLeft(x);
		// 右边进入
		boolean right = isInRight(x);
		// 上边进入
		boolean top = isInTop(y);
		// 下边进入
		boolean bottom = isInBottom(y);
		if (left) {
			if (top) {// 左上角
				return Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR);
			} else if (bottom) {// 左下角
				return Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
			} else {// 左边
				return Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR);
			}
		} else if (right) {
			if (top) {// 右上
				return Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR);
			} else if (bottom) {// 右下
				return Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR);
			} else {// 右边
				return Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
			}
		} else if (top) {// 上边
			return Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR);
		} else if (bottom) {// 下边
			return Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR);
		} else {
			return Cursor.getDefaultCursor();
		}

	}

	/**
	 * 
	 * 左边进入
	 * 
	 * @param x
	 *            int 鼠标点击X坐标
	 * @return boolean true：左边进入 false：不是左边进入
	 */
	private boolean isInLeft(int x) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (x > rect.getX() && x < rect.getX() + panelHorn) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 右边进入
	 * 
	 * @param x
	 *            int 鼠标点击X坐标
	 * @return boolean true：右边进入 false：不是右边进入
	 */
	private boolean isInRight(int x) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (x > rect.getX() + rect.getWidth() - panelHorn
				&& x < rect.getX() + rect.getWidth()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 上边进入
	 * 
	 * @param y
	 *            int 鼠标点击Y坐标
	 * @return boolean true：上边进入 false：不是上边进入
	 */
	private boolean isInTop(int y) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (y > rect.getY() && y < rect.getY() + panelHorn) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 下边进入
	 * 
	 * @param y
	 *            int 鼠标点击Y坐标
	 * @return boolean true：下边进入 false：不是下边进入
	 */
	private boolean isInBottom(int y) {
		// 边框区域
		Rectangle rect = this.getBounds();
		if (y > rect.getY() + rect.getHeight() - panelHorn
				&& y < rect.getY() + rect.getHeight()) {
			return true;
		}
		return false;
	}

}
