package epros.designer.gui.file.merge;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.file.FileManageDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 
 * 合并文件管理
 * 
 * @author 2012-05-28
 * 
 */
public class MergeFileDialog extends JecnDialog implements ActionListener {
	private static Logger log = Logger.getLogger(MergeFileDialog.class);

	/** 主面板 */
	private JPanel mainPanel = null;
	/** 树滚动面板+快速查询面板的容器· */
	private JecnSplitPane splitPane = null;
	/** 树面板 */
	private JScrollPane treeScrollPanel = null;
	/** 右侧面板 */
	private JPanel centerPanel = null;
	/** 结果面板 */
	private JPanel resultPanel = null;
	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = null;
	/** Dialog的关闭按钮在此面板上 */
	private JPanel colsePanel = null;
	/** 关闭按钮 */
	private JButton closeButton = null;
	/** 按钮面板，上面放四个按钮 */
	private JPanel buttonsPanel = null;
	/** 打开 */
	private JButton openBtn = null;
	private JButton addBtn = null;
	private JButton delBtn = null;
	/** 合并 */
	private JButton mergeBtn = null;
	private FileManageTable fileTable = null;
	private JecnTree jTree = null;
	private JLabel tipLable = new JLabel();
	private FileManageDialog dialog = null;
	private List<String[]> data;
	private ButtonGroup group = new ButtonGroup();
	private Set<Long> noPermIds = new HashSet<Long>();

	public MergeFileDialog(List<String[]> data, FileManageDialog dialog) {
		this.setTitle(JecnProperties.getValue("merge"));
		this.dialog = dialog;
		this.data = data;
		initCompotents();
		initLayout();
		buttonInit();
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:初始化组件
	 */
	public void initCompotents() {
		tipLable.setForeground(Color.red);
		// 主面板
		mainPanel = new JPanel();
		// 树面板
		treeScrollPanel = new JScrollPane();
		// 右侧面板
		centerPanel = new JPanel();
		// 树滚动面板+快速查询面板的容器
		splitPane = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, mainPanel, treeScrollPanel, centerPanel);
		// 结果面板
		resultPanel = new JPanel();
		// 结果滚动面板
		resultScrollPane = new JScrollPane();
		// 关闭按钮面板
		colsePanel = new JPanel();

		// 按钮面板
		buttonsPanel = new JPanel();

		// 打开
		openBtn = new JButton(JecnProperties.getValue("openBtn"));
		openBtn.setVisible(false);
		addBtn = new JButton(JecnProperties.getValue("addFile"));// "增加文件"
		delBtn = new JButton(JecnProperties.getValue("delFile"));// "删除文件"
		mergeBtn = new JButton(JecnProperties.getValue("confirmationMerger"));// "确认合并"
		// 关闭按钮
		closeButton = new JButton(JecnProperties.getValue("closeBtn"));

		// 把tree加入滚动面板
		jTree = initJecnTree();
		// 快速查询table
		fileTable = new FileManageTable();
		// 树滚动面板
		treeScrollPanel.setViewportView(jTree);
		fileTable.setOpaque(false);
		resultScrollPane.setViewportView(fileTable);

		// 大小
		this.setSize(750, 460);
		// 模态
		this.setModal(true);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);

		// 主面板布局-开始
		mainPanel.setLayout(new BorderLayout());
		// 快速查询面板
		centerPanel.setLayout(new BorderLayout(0, 2));
		// 结果面板布局-开始
		resultPanel.setLayout(new BorderLayout());
		// 关闭面板布局
		colsePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// 快速查询面板中按钮面板
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		// 树滚动面板大小
		Dimension size = new Dimension(200, 430);
		treeScrollPanel.setPreferredSize(size);
		treeScrollPanel.setMinimumSize(size);

		// 设置各面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treeScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		centerPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		colsePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 按钮命令标识
		// 打开
		openBtn.setActionCommand("openBtn");
		// 更新
		mergeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mergeAction();
			}
		});
		// 关闭按钮
		closeButton.setActionCommand("closeBtn");
		// 双击搜索结果Table
		fileTable.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent evt) {
				fileTablemouseReleased(evt);
			}
		});

		addBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addFile();
			}
		});

		delBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				delAction();
			}
		});

		// 边框
		// 树滚动面板
		treeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("StandardDocument")));
		treeScrollPanel.setVisible(false);
		// 快速查询面板
		centerPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), ""));
		// 左:右=3:7
		splitPane.setDividerLocation(0.3);
		// 当窗体变大时，右边面板获取空间,左面板不变
		splitPane.setResizeWeight(0.0);
		splitPane.showBothPanel();
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	protected void addFile() {
		List<JecnTreeBean> beans = new ArrayList<JecnTreeBean>();
		FileChooseDialog fileChooseDialog = new FileChooseDialog(beans);
		fileChooseDialog.setSelectMutil(true);
		fileChooseDialog.setVisible(true);
		if (fileChooseDialog.isOperation()) {
			if (beans.size() > 0) {
				List<Long> addIds = new ArrayList<Long>();
				for (JecnTreeBean bean : beans) {
					addIds.add(bean.getId());
				}
				List<JecnFileBeanT> files = ConnectionPool.getFileAction().getFilesFullPath(addIds);
				Set<Long> ids = getIdsFromTable(fileTable);
				List<String[]> datas = new ArrayList<String[]>();
				for (JecnFileBeanT bean : files) {
					if (ids.contains(bean.getFileID())) {
						continue;
					}
					datas.add(new String[] { bean.getFileID().toString(), JecnUtil.nullToEmpty(bean.getDocId()),
							bean.getFileName() });
				}
				for (Vector<Object> v : updateTableContent(datas)) {
					((DefaultTableModel) fileTable.getModel()).addRow(v);
				}
			}
		}
	}

	private Set<Long> getIdsFromTable(FileManageTable table) {
		Set<Long> result = new HashSet<Long>();
		int rowCount = table.getModel().getRowCount();
		if (rowCount == 0) {
			return result;
		}
		for (int i = 0; i < rowCount; i++) {
			Long id = Long.valueOf(table.getModel().getValueAt(i, 0).toString());
			result.add(id);
		}
		return result;
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:dialog布局方法
	 */
	public void initLayout() {
		// **************第一层布局**************//
		// 内容区
		mainPanel.add(splitPane, BorderLayout.CENTER);
		// 关闭按钮
		mainPanel.add(colsePanel, BorderLayout.SOUTH);
		// **************第一层布局**************//

		buttonsPanel.add(tipLable);
		buttonsPanel.add(openBtn);
		buttonsPanel.add(addBtn);
		buttonsPanel.add(delBtn);
		buttonsPanel.add(mergeBtn);

		resultPanel.add(resultScrollPane, BorderLayout.CENTER);
		resultPanel.add(buttonsPanel, BorderLayout.SOUTH);

		centerPanel.add(resultPanel, BorderLayout.CENTER);
		// **************快速查询布局**************//

		// 结果面板布局-结束
		colsePanel.add(closeButton);

		// 把主面板增加到dialog
		this.getContentPane().add(mainPanel);
	}

	/**
	 * 
	 * 按钮点击事件
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if ("openBtn".equals(btn.getActionCommand())) {// 打开
				openBtnAction();
			} else if ("deleteBtn".equals(btn.getActionCommand())) {
				delAction();
			} else if ("closeBtn".equals(btn.getActionCommand())) { // 关闭
				closeDialog();
			}
		}
	}

	/**
	 * 双击搜索结果Table,实现搜索定位
	 * 
	 * @param evt
	 */
	private void fileTablemouseReleased(MouseEvent evt) {

	}

	// 为按钮增加事件
	private void buttonInit() {
		// 打开
		openBtn.addActionListener(this);
		// 关闭
		closeButton.addActionListener(this);
	}

	protected void mergeAction() {
		noPermIds.clear();
		if (!validatePerm()) {
			return;
		}
		int confirmDialog = JOptionPane.showConfirmDialog(this, JecnProperties
				.getValue("operateCannotRecoverIsContinue"));// "操作完成之后数据无法恢复，是否确认操作？"
		if (confirmDialog != JOptionPane.OK_OPTION) {
			return;
		}

		List<Long> mergeIds = getMergeIds();
		Long mainId = getMainId();
		try {
			ConnectionPool.getFileAction().merge(mainId, mergeIds);
			dialog.searchTextAction();
			this.dispose();
			JOptionPane.showMessageDialog(this, JecnProperties.getValue("mergeSuccess"));
		} catch (Exception e) {
			log.error("", e);
			JOptionPane.showMessageDialog(this, JecnProperties.getValue("mergeFail"));
		}
	}

	private List<Long> getMergeIds() {
		TableModel model = fileTable.getModel();
		int count = model.getRowCount();
		List<Long> mergeIds = new ArrayList<Long>();
		for (int i = 0; i < count; i++) {
			Long id = Long.valueOf(model.getValueAt(i, 0).toString());
			JRadioButton button = (JRadioButton) fileTable.getModel().getValueAt(i, 3);
			if (!button.isSelected()) {
				mergeIds.add(id);
			}
		}
		return mergeIds;
	}

	private Long getMainId() {
		TableModel model = fileTable.getModel();
		int count = model.getRowCount();
		Long mainId = null;
		for (int i = 0; i < count; i++) {
			Long id = Long.valueOf(model.getValueAt(i, 0).toString());
			JRadioButton button = (JRadioButton) fileTable.getModel().getValueAt(i, 3);
			if (button.isSelected()) {
				mainId = id;
				break;
			}
		}
		return mainId;
	}

	private boolean validatePerm() {
		List<Long> ids = getMergeIds();
		ids.add(getMainId());
		if (ids.size() == 1 || JecnConstants.loginBean.isAdmin()) {
			return true;
		}
		Set<Long> setFile = JecnConstants.loginBean.getSetFile();
		if (JecnUtil.isEmpty(setFile)) {
			return false;
		}
		boolean result = true;
		List<JecnFileBeanT> files = ConnectionPool.getFileAction().getFilesFullPath(ids);
		for (JecnFileBeanT file : files) {
			boolean exist = false;
			if (StringUtils.isNotBlank(file.gettPath())) {
				String[] idArr = file.gettPath().split("-");
				for (String i : idArr) {
					if (setFile.contains(Long.valueOf(i))) {
						exist = true;
						break;
					}
				}
			} else {
				if (setFile.contains(file.getFileID())) {
					exist = true;
				}
			}
			if (!exist) {
				Long fileId = file.getFileID();
				result = false;
				noPermIds.add(fileId);
			}
		}

		if (!result) {
			// "红色部分为无操作权限的文件，请先找相关人员获取权限"
			tipLable.setText(JecnProperties.getValue("redPartHasNoPerm"));
			fileTable.repaint();
		}
		return result;
	}

	private void clearTip() {
		this.tipLable.setText("");
	}

	private void pleaseSelect() {
		JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
	}

	public void delAction() {
		// 提示选择一行进行删除
		int[] selectRows = fileTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		}
		for (int row : selectRows) {
			JRadioButton button = (JRadioButton) fileTable.getModel().getValueAt(row, 3);
			if (button.isSelected()) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("canNotDelMainFile"));// "不能删除主文件"
				return;
			}
		}

		// 删除table选中的数据 和树节点上的数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) fileTable.getModel()).removeRow(selectRows[i]);
		}
	}

	private void closeDialog() {
		this.dispose();
	}

	/**
	 * 打开文件
	 */
	public void openBtnAction() {
		// 判断是否选中Table中的一行，没选中，提示选中一行
		int[] selectRows = this.fileTable.getSelectedRows();
		if (selectRows.length == 1) {
			JecnDesignerCommon.openFile(Long.valueOf(fileTable.getValueAt(selectRows[0], 0).toString()));
		} else {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		}
	}

	public JecnTree initJecnTree() {
		// return new HighEfficiencyFileTree(this); // null;//
		return null;
	}

	public Vector<Vector<Object>> updateTableContent(List<String[]> datas) {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		int i = 0;
		for (String[] d : datas) {
			Vector<Object> data = new Vector<Object>();
			data.add(d[0]);
			// 文件编号
			data.add(JecnUtil.nullToEmpty(d[1]));
			data.add(JecnUtil.nullToEmpty(d[2]));
			JRadioButton radioButton = new JRadioButton("", i == 0);
			group.add(radioButton);
			data.add(radioButton);
			vector.add(data);
			i++;
		}
		return vector;
	}

	@SuppressWarnings("serial")
	class FileManageTable extends JTable {
		public FileManageTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			TableColumnModel columnModel = this.getColumnModel();
			if (cols != null && cols.length > 0) {
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI(SwingConstants.CENTER));
			this.getTableHeader().setReorderingAllowed(false);
			JCheckBox box = new JCheckBox();
			columnModel.getColumn(3).setCellEditor(new RadioButtonEditor(box));
			columnModel.getColumn(3).setCellRenderer(new RadioButtonRenderer());

			columnModel.getColumn(3).setMaxWidth(120);
			columnModel.getColumn(1).setMaxWidth(200);
			columnModel.getColumn(1).setMinWidth(200);
		}

		public FileManageTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("fileNum"));
			title.add(JecnProperties.getValue("name"));
			title.add(JecnProperties.getValue("selectMainFile"));// "选择主文件"
			return new FileManageTableMode(updateTableContent(data), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {

			return new int[] { 0 };
		}

		class FileManageTableMode extends DefaultTableModel {
			public FileManageTableMode(Vector<Vector<Object>> data, Vector<String> title) {
				super(data, title);
			}

			public boolean isCellEditable(int rowindex, int colindex) {
				if (colindex == 3) {
					return true;
				}
				return false;
			}

		}

		@Override
		public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
			Component component = super.prepareRenderer(renderer, row, column);
			Long id = Long.valueOf(this.getModel().getValueAt(row, 0).toString());
			if (noPermIds.contains(id)) {
				component.setForeground(Color.RED);
			} else {
				component.setForeground(Color.BLACK);
			}
			return component;
		}

	}

	class RadioButtonEditor extends DefaultCellEditor implements ItemListener {
		private JRadioButton button;

		public RadioButtonEditor(JCheckBox checkBox) {
			super(checkBox);
		}

		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			if (value == null) {
				return null;
			}
			button = (JRadioButton) value;
			button.addItemListener(this);
			return (Component) value;
		}

		public Object getCellEditorValue() {
			button.removeItemListener(this);
			return button;
		}

		public void itemStateChanged(ItemEvent e) {
			super.fireEditingStopped();
			MergeFileDialog.this.fileTable.repaint();
		}
	}

	class RadioButtonRenderer extends DefaultTableCellRenderer {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			if (value == null) {
				return null;
			}
			// if (column == 3) {
			super.setHorizontalAlignment(SwingConstants.CENTER);
			// }
			return (Component) value;
		}

	}

}
