package com.jecn.epros.server.download.wordxml.kedaxunfei;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Fonts;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.PositionType;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.bean.paragraph.GroupBean.Hanchor;
import wordxml.element.bean.paragraph.GroupBean.Vanchor;
import wordxml.element.bean.table.CellMarginBean;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 科大讯飞操作说明模版
 * 
 * @author hyl
 * 
 */
public class KDXFProcessModel extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);

	public KDXFProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 标题行带阴影
		getDocProperty().setTblTitleShadow(true);
		// 表格标题不跨行显示
		getDocProperty().setTblTitleCrossPageBreaks(false);
		// 设置表格统一宽度
		getDocProperty().setNewTblWidth(14.8F);
		getDocProperty().setNewRowHeight(0.7F);
		getDocProperty().setFlowChartScaleValue(5.5F);
		setDocStyle(".", textTitleStyle());
		super.setDefAscii(Fonts.ROMAN);
	}

	/**
	 * 设置表格垂直居中
	 */
	@Override
	protected Table createTableItem(Sect sect, float[] width, List<String[]> rowData) {
		Table tab = super.createTableItem(sect, width, rowData);
		for (TableRow row : tab.getRows()) {
			for (TableCell tc : row.getCells()) {
				tc.setvAlign(Valign.center);
			}
		}
		return tab;
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 3.17F, 2F, 3.17F, 1.5F, 1.07F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/**
	 * 添加首页标题
	 */
	@Override
	protected void initTitleSect(Sect titleSect) {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.7F, 3.17F, 2.54F, 3.17F, 1F, 0.89F);
		sectBean.setSize(21, 29.7F);
		titleSect.setSectBean(sectBean);
		addTitleSectContent(titleSect);
		createTitleCommhdr(titleSect);
	}

	private void createTitleCommhdr(Sect sect) {
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		Image image = new Image(getDocProperty().getLogoImage());
		image.setSize(2.91F, 1.02F);
		image.setPositionBean(new PositionBean(PositionType.absolute, 3F, 0, 330, 0));
		header.createParagraph(image);
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent(Sect titleSect) {
		// 公司名称
		createTitleComponyName(titleSect);
		// 一条线
		createTopLine(titleSect);
		// 文档密级
		createSectLevel(titleSect);
		// 流程名称
		createProcessName(titleSect);
		titleSect.createMutilEmptyParagraph(12);
		// 属性table
		createPropertyTable(titleSect);
		// 发布行
		createPubLine(titleSect);
		// 一条线
		createBottomLine(titleSect);
		// 公司信息
		createComponyPub(titleSect);
	}

	private void createHistorys(Sect titleSect) {
		titleSect.createMutilEmptyParagraph(2);
		titleSect.createParagraph("修订历史", new ParagraphBean(Align.center, "宋体 (中文正文)", Constants.sihao, true));
		/** 流程文控信息 0版本号 1变更说明 2发布日期 3流程拟制人 4审批人 */
		List<Object[]> documentList = processDownloadBean.getDocumentList();
		/***** 默认表格样式 **********/
		TableBean tabBean = new TableBean();
		tabBean.setTableAlign(Align.center);
		tabBean.setBorder(0.5F);
		tabBean.setCellMarginBean(new CellMarginBean(0, 0.19F, 0, 0.19F));
		/**** 表格默认段落样式 宋体小居中 ************/
		ParagraphBean tabPBean = new ParagraphBean(Align.center, "宋体", Constants.wuhao, false);
		tabPBean.setSpace(0f, 0f, vLine1);
		List<String[]> rowData = new ArrayList<String[]>();
		rowData.add(new String[] { "版本", "拟制/修订日期", "编写和修订简要说明", "编制/修订人" });
		// 添加文控发布记录
		if (documentList != null) {
			for (Object[] obj : documentList) {
				String version = String.valueOf(obj[0]);
				String content = String.valueOf(obj[1]);
				String revisePeople = String.valueOf(obj[3]);
				String reviseData = String.valueOf(obj[2]).replaceAll("\\w+\\d+\\:\\d+.+", "").replaceAll("\\-", "/");
				rowData.add(new String[] { version, reviseData, content, revisePeople });
			}
		}
		// 表格
		Table tab = createTableItem(titleSect, new float[] { 1.94F, 2.75F, 7.3F, 2.64F }, rowData);
		tab.setRowStyle(0, tblTitleStyle(), docProperty.isTblTitleShadow());
	}

	private void createPropertyTable(Sect titleSect) {
		List<String[]> rowData = new ArrayList<String[]>();
		ProcessAttributeBean flow = processDownloadBean.getResponFlow();
		rowData.add(new String[] { "流程类别", flow.getProcessTypeName(), "流程级别",
				"L" + nullToEmpty(processDownloadBean.getLevel()) });
		rowData.add(new String[] { "流程责任人", flow.getDutyUserName(), "版本号", processDownloadBean.getVersion() });
		rowData.add(new String[] { "流程拟制人", flow.getFictionPeopleName(), "文件编号",
				nullToEmpty(processDownloadBean.getFlowInputNum()) });
		rowData.add(new String[] { "责任部门", flow.getDutyOrgName(), "有效期",
				nullToEmpty(processDownloadBean.getExpiryStr()) });
		Table table = createTableItem(titleSect, new float[] { 2.25F, 5F, 2.25F, 5F }, rowData);
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体 (中文正文)", Constants.wuhao, true);
		table.setCellStyle(0, pBean, getDocProperty().isTblTitleShadow());
		table.setCellStyle(2, pBean, getDocProperty().isTblTitleShadow());
	}

	private void createSectLevel(Sect titleSect) {
		titleSect.createParagraph("文档密级：" + processDownloadBean.getConfidentialityLevelName(), new ParagraphBean(
				Align.left, "宋体 (中文正文)", Constants.wuhao, false));
	}

	private void createComponyPub(Sect titleSect) {
		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 14, GroupH.exact, 2).setHorizontal(3.79, Hanchor.page, 0.22)
				.setVertical(20.87, Vanchor.page, 0.32).lock(true);

		ParagraphBean pBean = new ParagraphBean(Align.center);
		Paragraph paragraph = new Paragraph(false);
		paragraph.setParagraphBean(pBean);
		FontBean fontBean = new FontBean("微软雅黑", Constants.xiaoer, true);
		paragraph.appendTextRun("科大讯飞股份有限公司", fontBean);
		fontBean = new FontBean("微软雅黑", Constants.sihao, false);
		paragraph.appendTextRun("  发布", fontBean);
		group.addParagraph(paragraph);
	}

	private void createPubLine(Sect titleSect) {

		ParagraphBean pubBean = new ParagraphBean(Align.left, "宋体 (中文正文)", Constants.wuhao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(3.2F, Hanchor.page, 0)
				.setVertical(19.95, Vanchor.page, 0.32).lock(true);
		pubGroup.addParagraph(new Paragraph(processDownloadBean.getPubDate("yyyy年MM月dd日") + "发布", pubBean));

		ParagraphBean nextBean = new ParagraphBean(Align.right, "宋体 (中文正文)", Constants.wuhao, false);
		Group nextGroup = titleSect.createGroup();
		nextGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(10.6, Hanchor.page, 0)
				.setVertical(19.95, Vanchor.page, 0.32).lock(true);
		nextGroup.addParagraph(new Paragraph(processDownloadBean.getImplementDate("yyyy年MM月dd日") + "实施", nextBean));
	}

	private void createProcessName(Sect titleSect) {
		String flowName = nullToEmpty(processDownloadBean.getFlowName());
		ParagraphBean pBean = new ParagraphBean(Align.center, "宋体", Constants.erhao, true);
		ParagraphLineRule vLine1 = new ParagraphLineRule(34F, LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph paragraph = new Paragraph(flowName, pBean);

		Group group = titleSect.createGroup();
		group.getGroupBean().setSize(GroupW.exact, 17, GroupH.exact, 2.4).setHorizontal(2.0, Hanchor.page, 0)
				.setVertical(10.42, Vanchor.page, 0).lock(true);
		group.addParagraph(paragraph);
	}

	private void createTopLine(Sect titleSect) {
		createLine(titleSect, 0.95F, 7F, 415F, 7F);
	}

	private void createBottomLine(Sect titleSect) {
		createLine(titleSect, 0F, 140F, 415F, 140F);
	}

	protected void a15(ProcessFileItem processFileItem, List<String[]> oldRowData) {
		createTitle(processFileItem);
		List<String[]> tableData = new ArrayList<String[]>();// 一个表格数据
		// 科大讯飞的目标值取的是设置目的
		tableData.add(new String[] { "名称", "类型", "定义", "统计方法", "目标值" });
		if (processDownloadBean.getFlowKpiList() != null && processDownloadBean.getFlowKpiList().size() > 0) {
			for (Object[] objs : processDownloadBean.getFlowKpiList()) {
				tableData.add(new String[] { nullToEmpty(objs[0]), nullToEmpty(objs[1]), nullToEmpty(objs[3]),
						nullToEmpty(objs[4]), nullToEmpty(objs[15]) });
			}
		}
		Table tbl = createTableItem(processFileItem, new float[] { 2.97F, 2.97F, 2.97F, 2.97F, 2.97F }, tableData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	private void createLine(Sect titleSect, float startX, float startY, float endX, float endY) {
		LineGraph line = new LineGraph(LineStyle.single, 0.5F);
		line.setFrom(startX, startY);
		line.setTo(endX, endY);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		positionBean.setMarginRight(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);
	}

	private void createTitleComponyName(Sect titleSect) {
		/*** 最小值 分散对齐 *****/
		ParagraphLineRule vLine = new ParagraphLineRule(0F, LineRuleType.AT_LEAST);
		ParagraphBean pBean = new ParagraphBean(Align.distribute, "微软雅黑", Constants.yihao, true);
		pBean.setSpace(0f, 0f, vLine);
		titleSect.createParagraph("科大讯飞股份有限公司", pBean);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommfhdr(firstSect);
		// 修订历史
		createHistorys(firstSect);
		firstSect.breakPage();
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		SectBean sectBean = getSectBean();
		secondSect.setSectBean(sectBean);
		createCommfhdr(secondSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getCharSectBean();
		flowChartSect.setSectBean(sectBean);
		createCharCommfhdr(flowChartSect);

	}

	private SectBean getCharSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(2.54F, 3.17F, 2F, 3.17F, 1.5F, 1.07F);
		sectBean.setSize(29.7F, 21F);
		return sectBean;
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect, float left, float right) {
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		Image image = new Image(getDocProperty().getLogoImage());
		image.setSize(2.08F, 0.73F);
		image.setPositionBean(new PositionBean(PositionType.relative, -10F, 0, 355, 0));
		Paragraph p = header.addParagraph(new Paragraph("", new ParagraphBean(PStyle.AF1)));
		p.appendGraphRun(image);
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommfhdr(Sect sect) {
		createCommhdr(sect, 8F, 8F);
		createCommftr(sect);
	}

	private void createCharCommfhdr(Sect sect) {
		createCharCommhdr(sect);
		createCommftr(sect);
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCharCommhdr(Sect sect) {
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		Image image = new Image(getDocProperty().getLogoImage());
		image.setSize(2.08F, 0.73F);
		image.setPositionBean(new PositionBean(PositionType.relative, -10F, 0, 605, 0));
		Paragraph p = header.addParagraph(new Paragraph("", new ParagraphBean(PStyle.AF1)));
		p.appendGraphRun(image);
	}

	/**
	 * 流程图横向
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0.1F, 0.1F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 创建通用页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect) {
		ParagraphBean pBean = new ParagraphBean(Align.center, "Calibri (西文正文)", Constants.xiaowu, false);
		pBean.setSpace(0f, 0f, vLine1);
		Footer footer = sect.createFooter(HeaderOrFooterType.odd);
		Paragraph p = footer.createParagraph("", pBean);
		p.appendCurPageRun();
		p.appendTextRun("/");
		p.appendTotalPagesRun();
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		// 初始化表格属性
		TableBean tblStyle = new TableBean();
		// 所有边框 0.5 磅
		tblStyle.setBorder(1F);
		tblStyle.setBorderInsideH(1F);
		tblStyle.setBorderInsideV(1F);
		// 表格左缩进0.01 厘米
		tblStyle.setTableLeftInd(0.01F);
		// 表格内边距 厘米
		CellMarginBean marginBean = new CellMarginBean(0, 0F, 0, 0F);
		tblStyle.setCellMarginBean(marginBean);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitleStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitleStyle.setSpace(0f, 0f, vLine1);
		return tblTitleStyle;
	}

	// 宋体 五号 首行2字符
	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean("宋体 (中文正文)", Constants.wuhao, false);
		textContentStyle.setSpace(0f, 0f, vLine1);
		textContentStyle.setInd(0.1F, 0, 0F, 0.7F);
		return textContentStyle;
	}

	// 微软雅黑 五号 单倍行距
	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Fonts.SONG, Constants.sihao, true);
		textTitleStyle.setSpace(1F, 1F, vLine1);
		// 悬挂缩进默认0.74 会导致序号 超过10了增加两倍故设置0.9
		textTitleStyle.setInd(0F, 0, 0.9F, 0);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}
}
