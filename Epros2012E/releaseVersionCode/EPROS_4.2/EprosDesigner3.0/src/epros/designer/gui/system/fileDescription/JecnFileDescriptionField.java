package epros.designer.gui.system.fileDescription;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;

import epros.designer.gui.common.JecnTextField;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnPanel;

/**
 * 操作说明只包含一个JField的通用组件
 * 
 * @author user
 * 
 */
public class JecnFileDescriptionField extends JecnFileDescriptionComponent {
	protected JecnTextField jecnField = new JecnTextField();
	private String text = "";

	/**
	 * 
	 * @param index
	 *            标题序号
	 * @param paragrapHeadingName
	 *            标题名称
	 * @param rows
	 *            行数
	 * @param insets
	 *            边距
	 * @param contentPanel
	 * @param isRequest
	 *            是否必填
	 * @param isFourLenght
	 *            是否是有4000长度限制
	 * @param text
	 *            内容
	 * @param tip
	 * @param tipName
	 *            提示名称
	 */
	public JecnFileDescriptionField(int index, String paragrapHeadingName, JecnPanel contentPanel, boolean isRequest,
			String text, String tip) {
		super(index, paragrapHeadingName, isRequest, contentPanel, tip);

		centerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, insets, 0, 0);
		this.centerPanel.add(jecnField, c);
		this.text = text == null ? "" : text;
		jecnField.setText(this.text);
	}

	/**
	 * 
	 * @param index
	 *            标题序号
	 * @param paragrapHeadingName
	 *            标题名称
	 * @param rows
	 *            行数
	 * @param insets
	 *            边距
	 * @param contentPanel
	 * @param isRequest
	 *            是否必填
	 * @param isEditAble
	 *            是否可编辑
	 * @param isFourLenght
	 *            是否是有4000长度限制
	 * @param text
	 *            内容
	 * @param tip
	 * @param tipName
	 *            提示名称
	 */
	public JecnFileDescriptionField(int index, String paragrapHeadingName, JecnPanel contentPanel, boolean isRequest,
			boolean isEditAble, String text, String tip) {
		this(index, paragrapHeadingName, contentPanel, isRequest, text, tip);
		jecnField.setEditable(isEditAble);
	}

	public boolean isUpdate() {
		if (!jecnField.getText().equals(text)) {
			return true;
		}
		return false;
	}

	public String getResultStr() {
		return jecnField.getText().trim();
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return JecnValidateCommon.validateFieldNotPass(jecnField.getText(), verfyLab, this.name);
	}
}
