package com.jecn.epros.server.action.designer.standard;

import java.util.List;

import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface IStandardAction {

	/**
	 * @author yxw 2012-6-7
	 * @description:增加标准目录
	 * @return
	 * @throws Exception
	 */
	public Long addStandard(StandardBean standardBean) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:根据父id查找子节点
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildStandards(Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-14
	 * @description::根据父id查找目录节点
	 * @param pid
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildStandardDirs(Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:获取所有节点 目录和标准文件
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllStandard(Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-14
	 * @description:获取所有目录节点
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllStandardDir(Long pid, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:标准重命名
	 * @param newName
	 * @param id
	 * @param updatePersonId
	 * @throws Exception
	 */
	public void reName(String newName, Long id, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点排序
	 * @param list
	 * @param pId
	 * @throws Exception
	 */
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
			throws Exception;

	/**
	 * @author yxw 2012-6-6
	 * @description:节点移动
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteStandard(List<Long> listIds, Long projectId, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:标准关联流程
	 * @param standardId
	 * @param flowIds
	 * @param updatePersonId
	 * @throws Exception
	 */
	public void addRelationFlow(List<StandardBean> list, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-8
	 * @description:上传标准文件
	 * @param list
	 * @param posIds
	 * @param orgIds
	 */
	public void addStandardFile(List<StandardBean> list, String posIds, String orgIds, String posGroupIds,
			Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-6-7
	 * @description:根据名称搜索
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchByName(String name, Long projectId, Long peopelId) throws Exception;

	/**
	 * @author zhangchen Jul 30, 2012
	 * @description：搜索标准节点
	 * @param standardId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodes(Long standardId, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-10-23
	 * @description:更新标准文件
	 * @param id
	 * @param fileId
	 * @param fileName
	 * @throws Exception
	 */
	public void updateStandardFile(Long id, Long fileId, String fileName, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2012-10-23
	 * @description:更新标准流程/流程地图
	 * @param id
	 * @param processId
	 * @param name
	 * @param TreeNodeType
	 *            2:标准流程 3标准流程地图
	 * @throws Exception
	 */
	public void updateStandardProcess(Long id, Long processId, String name, TreeNodeType type, Long updatePersonId)
			throws Exception;

	/**
	 * @description:根据名称搜索标准文件
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchStandarFileByName(String name, Long projectId) throws Exception;

	/**
	 * @author yxw 2013-11-15
	 * @description:更新标准条款要求
	 * @param id
	 * @param name
	 *            名称
	 * @param context
	 *            要求内容
	 * @throws Exception
	 */
	public void updateStanClauseRequire(Long id, String name, String context, Long updatePersonId) throws Exception;

	/**
	 * @author yxw 2013-11-19
	 * @description:根据ID获取标准详情
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public StandardBean getStandardBean(Long id) throws Exception;

	/**
	 * @author yxw 2013-11-19
	 * @description:
	 * @param standardBean
	 * @throws Exception
	 */
	public void updateStandardBean(StandardBean standardBean) throws Exception;

	List<JecnTreeBean> getRoleAuthChildStandards(Long pid, Long projectId, Long peopleId) throws Exception;

	public List<JecnTreeBean> getStandardByFlowId(Long flowId) throws Exception;

	FileOpenBean openFileContent(Long id) throws Exception;

	public ByteFileResult importData(long userId, byte[] b, Long id, String type, Long projectId, boolean CH);
}
