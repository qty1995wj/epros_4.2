package com.jecn.epros.server.service.reports.excel;

import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.block.BorderArrangement;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.CompositeTitle;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleEdge;

import com.jecn.epros.server.bean.propose.JecnRtnlProposeCount;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeCountDetail;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

/**
 * 
 * 合理化建议统计报表
 * 
 * @author hyl
 * 
 */
public class ProposeDownExcelService {

	private Log log = LogFactory.getLog(this.getClass());
	/** 数据对象 */
	private JecnRtnlProposeCount proposeCountBean;

	public void setProposeCountBean(JecnRtnlProposeCount proposeCountBean) {
		this.proposeCountBean = proposeCountBean;
	}

	public ProposeDownExcelService() {

	}

	/**
	 * 设置名称防止重复
	 * 
	 * @param beans
	 */
	private void setProcessScanSheetName(List<JecnRtnlProposeCountDetail> beans) {

		Map<String, Integer> sheetNames = new HashMap<String, Integer>();
		for (JecnRtnlProposeCountDetail bean : beans) {
			String sheetName = bean.getName();
			if (!sheetNames.keySet().contains(sheetName)) {
				sheetNames.put(bean.getName(), 0);
			} else {// 如果sheet的名称重复那么累加1
				sheetNames.put(sheetName, sheetNames.get(sheetName) + 1);
				sheetName = sheetName + "(" + sheetNames.get(sheetName) + ")";
			}
			bean.setSheetName(sheetName);
		}
	}

	public byte[] getExcelFile() throws Exception {

		if (proposeCountBean == null) {
			throw new Exception(
					"ProposeDownExcelService类proposeCountBean为空");
		}
		byte[] tempDate = null;
		WritableWorkbook wbookData = null;
		File file = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path
					+ JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);
			// 写出文件
			writeExcelFile(wbookData);
			// 后续处理--------------------------------------
			wbookData.write();
			// 工作簿不关闭不会写出数据
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;
			// 把一个文件转化为字节
			tempDate = JecnUtil.getByte(file);

		} catch (Exception e) {
			throw new Exception(
					"ProposeDownExcelService类getExcelFile方法异常", e);

		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();

				} catch (Exception e) {
					log.error(e);
					throw new Exception(
							"ProposeDownExcelService类关闭excel工作表异常", e);
				}
			}
			// 删除临时文件
			if (file != null && file.exists()) {
				file.delete();
			}
		}

		return tempDate;
	}
    
	private void writeExcelFile(WritableWorkbook wbookData) throws Exception {

		// 当前sheet页
		int curSheet = 0;
		WritableSheet wsheet = wbookData.createSheet(proposeCountBean
				.getDateStr(), curSheet);

		//由于提交人查询与部门查询报表只有细微的差别所以共用一个
		if (proposeCountBean.getType() == 1 || proposeCountBean.getType() == 2) {
			// 处理重复名称
			setProcessScanSheetName(proposeCountBean.getCountDetailList());
			createTitle(wsheet);
			createContent(wsheet);

		} else if (proposeCountBean.getType() == 3) {
			createDetailTitle(wsheet);
			createDetailContent(wsheet);
		}

	}

	private void createDetailContent(WritableSheet wsheet) throws Exception {

		// 行
		int row = 0;
		// 列
		int col = 0;
		// 序号
		int index = 1;
		// 定义样式
		WritableCellFormat contentStyle = getSheetContentStyle();
		// 写出所有数据
		for (JecnRtnlProposeCountDetail webBean : proposeCountBean
				.getCountDetailList()) {
			// 写入下一行
			row++;
			col = 0;
			wsheet.setRowView(row, 400);
			wsheet.addCell(new Label(col++, row, String.valueOf(index),
					contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getCreateName(),
					contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getOrgName(),
					contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getRelateName(),
					contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getRtnlContent(),
					contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getCreateTime(),
					contentStyle));

			index++;
		}

	}

	private void createDetailTitle(WritableSheet wsheet)
			throws RowsExceededException, WriteException {
		// 行
		int row = 0;
		// 列
		int col = 0;
		// 序号
		String indexStr = JecnUtil.getValue("index");
		// 提交人
		String submitPerson = JecnUtil.getValue("submitPerson");
		// 时间
		String activeTime = JecnUtil.getValue("activeTime");
		// 部门名称
		String orgName = JecnUtil.getValue("orgName");
		// 提交意见
		String submitComments = JecnUtil.getValue("submitComments");
		// 流程名称
		String processName = JecnUtil.getValue("processName");

		// 定义样式
		WritableCellFormat contentStyle = getSheetContentStyle();
		// 表头的样式 背景为黄色 字体为红色
		WritableCellFormat titleStyleY = getSheetTitleStyleY();
		// 表头的样式 背景为橘黄色 字体为黑色
		WritableCellFormat titleStyleO = getSheetTitleStyleO();

		// ------------------------写出表头------------------------
		// -------------调整列宽
		// 索引
		wsheet.setColumnView(col++, 5);
		wsheet.setColumnView(col++, 20);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 60);
		wsheet.setColumnView(col++, 20);

		// -------------写出表头的内容
		col = 0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 序号
		wsheet.addCell(new Label(col++, row, indexStr, titleStyleY));
		// 第一列名称
		wsheet.addCell(new Label(col++, row, submitPerson, titleStyleY));
		wsheet.addCell(new Label(col++, row, orgName, titleStyleY));
		wsheet.addCell(new Label(col++, row, processName, titleStyleY));
		wsheet.addCell(new Label(col++, row, submitComments, titleStyleY));
		wsheet.addCell(new Label(col++, row, activeTime, titleStyleY));

	}

	private void createTitle(WritableSheet wsheet)
			throws RowsExceededException, WriteException {
		// 行
		int row = 0;
		// 列
		int col = 0;
		// 序号
		String indexStr = JecnUtil.getValue("index");
		// 提交人
		String submitPerson = JecnUtil.getValue("submitPerson");
		// 提交总数
		String submitTotal = JecnUtil.getValue("submitTotal");
		// 采纳总数
		String acceptTotal = JecnUtil.getValue("acceptTotal");
		// 采纳率
		String acceptPercent = JecnUtil.getValue("acceptPercent");
		// 时间
		String activeTime = JecnUtil.getValue("activeTime");
		// 部门名称
		String orgName = JecnUtil.getValue("orgName");
		// 流程责任人
		String submitComments = JecnUtil.getValue("submitComments");

		// 定义样式
		WritableCellFormat contentStyle = getSheetContentStyle();
		// 表头的样式 背景为黄色 字体为红色
		WritableCellFormat titleStyleY = getSheetTitleStyleY();
		// 表头的样式 背景为橘黄色 字体为黑色
		WritableCellFormat titleStyleO = getSheetTitleStyleO();

		// ------------------------写出表头------------------------
		// -------------调整列宽
		// 索引
		wsheet.setColumnView(col++, 5);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 20);
		wsheet.setColumnView(col++, 10);
		wsheet.setColumnView(col++, 28);

		// -------------写出表头的内容
		col = 0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 表头
		wsheet.addCell(new Label(col++, row, indexStr, titleStyleY));

		// 第一列名称 创建人/部门名称
		if (proposeCountBean.getType() == 1) {
			wsheet.addCell(new Label(col++, row, submitPerson, titleStyleY));
		} else if (proposeCountBean.getType() == 2) {
			wsheet.addCell(new Label(col++, row, orgName, titleStyleY));
		}

		wsheet.addCell(new Label(col++, row, submitTotal, titleStyleY));
		wsheet.addCell(new Label(col++, row, acceptPercent, titleStyleY));
		wsheet.addCell(new Label(col++, row, activeTime, titleStyleY));

	}

	private void createContent(WritableSheet wsheet) throws Exception {

		// 行
		int row = 0;
		// 列
		int col = 0;
		// 序号
		int index = 1;
		// 定义样式
		WritableCellFormat contentStyle = getSheetContentStyle();
		String dateStr = proposeCountBean.getDateStr();
		// 写出所有数据
		for (JecnRtnlProposeCountDetail webBean : proposeCountBean
				.getCountDetailList()) {
			// 写入下一行
			row++;
			col = 0;
			wsheet.setRowView(row, 400);
			wsheet.addCell(new Label(col++, row, String.valueOf(index),
					contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getSheetName(),
					contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getTotal() + "",
					contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getAcceptPercentStr(),
					contentStyle));
			wsheet.addCell(new Label(col++, row, dateStr, contentStyle));

			index++;
		}
		index++;
		// 写出图片
		String relatePath = createImage();
		String truePath = JecnPath.APP_PATH + relatePath;
		File imageFile = new File(truePath);
		JecnUtil.addPictureToExcel(wsheet, imageFile, index, 0);
	}

	/**
	 * 设置表头的样式
	 */
	private WritableCellFormat getSheetTitleStyleY() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.RED);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.YELLOW);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);

		return tempCellFormat;
	}

	/**
	 * 设置表头的样式
	 */
	private WritableCellFormat getSheetTitleStyleO() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);

		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc,
				NumberFormats.TEXT);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);
		tempCellFormat.setBackground(Colour.LIGHT_ORANGE);
		return tempCellFormat;
	}

	/**
	 * 表头的内容样式
	 */
	private WritableCellFormat getSheetContentStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc,
				NumberFormats.TEXT);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		return tempCellFormat;
	}

	/**
	 * 获得合理化建议创建人或者部门下报表的图片
	 * @author hyl
	 * @return
	 * @throws Exception
	 */
	public String getPorposeCreateImage() throws Exception {

		if (proposeCountBean == null) {

			throw new Exception("合理化建议统计 proposeCountBean为空");
		}
		// 处理重复名称
		setProcessScanSheetName(proposeCountBean.getCountDetailList());
		String url = createImage();

		return url;
	}

	/**
	 * 创建图片
	 * @return
	 * @throws Exception
	 */
	private String createImage() throws Exception {

		int chartHeight = 400;
		int chartWidth = 300;
		// 将值与值之间的间隔设为20
		if (proposeCountBean.getCountDetailList().size() > 5) {
			chartWidth = proposeCountBean.getCountDetailList().size() * 60;
		}

		// 生成的图片右侧的图片定为100
		chartWidth = chartWidth + 100;
		JFreeChart jfchart = getProposeJFreeChart(proposeCountBean.getType());
		String url = jfreeChartToPng(jfchart, chartWidth, chartHeight);

		return url;

	}

	/**
	 * 生成jfreechart
	 * 
	 * @param type
	 *            1以人为查询条件 2以部门为查询条件 3详细信息
	 * @return
	 * @throws Exception
	 */
	private JFreeChart getProposeJFreeChart(int type) throws Exception {

		String title = "";
		// 以人员为查询条件
		if (type == 1) {
			title = proposeCountBean.getDateStr() + "提交人查询表";

		} else if (type == 2) {// 以部门为查询条件
			title = proposeCountBean.getDateStr() + "部门查询表";
		}

		// 标题
		JFreeChart localJFreeChart = ChartFactory.createBarChart("", "", "",
				createDataset1(proposeCountBean.getType()),
				PlotOrientation.VERTICAL, false, false, false);

		Font font = new Font("宋体", Font.BOLD, 18);
		// 图片标题
		localJFreeChart.setTitle(new TextTitle(title, font));

		CategoryPlot localCategoryPlot = (CategoryPlot) localJFreeChart
				.getPlot();

		CategoryDataset localCategoryDataset = createDataset2(proposeCountBean
				.getType());
		localCategoryPlot.setDataset(1, localCategoryDataset);
		localCategoryPlot.mapDatasetToRangeAxis(1, 1);

		// 水平
		CategoryAxis localCategoryAxis = localCategoryPlot.getDomainAxis();
		localCategoryAxis
				.setCategoryLabelPositions(CategoryLabelPositions.UP_45);

		// 竖直 我是左侧的单位
		NumberAxis leftNumberAxis = new NumberAxis(JecnUtil.getValue("unitOne"));
		// 如果总计小于10
		int maxCount = getMaxCount();
		if (maxCount < 10) {
			maxCount = 10;
		}
		// 设置左侧显示坐标的最大值
		leftNumberAxis.setUpperBound(maxCount + maxCount / 10);
		leftNumberAxis.setLabelFont(new Font("宋体", Font.BOLD, 12));

		// 设置左侧竖直刻度长度
		leftNumberAxis.setAutoTickUnitSelection(false);
		double leftUnit = maxCount / 10;// 刻度的长度
		NumberTickUnit leftNtu = new NumberTickUnit(leftUnit);
		leftNumberAxis.setTickUnit(leftNtu);

		localCategoryPlot.setRangeAxis(0, leftNumberAxis);

		// 竖直 我是%比 右侧单位
		NumberAxis localNumberAxis = new NumberAxis(JecnUtil
				.getValue("unitPercent"));
		// 设置百分百的最大值
		localNumberAxis.setUpperBound(110);

		localNumberAxis.setLabelFont(new Font("宋体", Font.BOLD, 12));

		localCategoryPlot.setRangeAxis(1, localNumberAxis);

		// 柱状图数字显示
		BarRenderer localBarRenderer = (BarRenderer) localCategoryPlot
				.getRenderer();
		// localBarRenderer.setBaseItemLabelFont(new Font("宋体", Font.BOLD, 12));
		// 柱状图标签距离柱状图的距离
		localBarRenderer.setItemLabelAnchorOffset(0.0D);
		// 标签是否显示
		localBarRenderer.setBaseItemLabelsVisible(true);
		localBarRenderer
				.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		localBarRenderer
				.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());

		// 柱状图的宽度
		localBarRenderer.setMaximumBarWidth(0.1);
		localBarRenderer.setMinimumBarLength(0.1);
		localBarRenderer.setItemMargin(0.1);
		// 折线图显示标签
		LineAndShapeRenderer localLineAndShapeRenderer = new LineAndShapeRenderer();
		localLineAndShapeRenderer.setBaseShapesVisible(true);
		localLineAndShapeRenderer.setBaseItemLabelsVisible(true);
		// 折线图的线是否显示
		localLineAndShapeRenderer.setSeriesLinesVisible(0, true);
		localLineAndShapeRenderer
				.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());

		localLineAndShapeRenderer.setItemLabelAnchorOffset(2.0D);

		// 柱状图的标签的单位
		DecimalFormat localDecimalFormat2 = new DecimalFormat("##.#'%'");
		localDecimalFormat2.setNegativePrefix("(");
		localDecimalFormat2.setNegativeSuffix(")");
		localLineAndShapeRenderer
				.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator(
						"{2}", localDecimalFormat2));
		localLineAndShapeRenderer.setBaseItemLabelsVisible(true);

		localCategoryPlot.setRenderer(1, localLineAndShapeRenderer);
		// 折线是在前段还是在后端
		localCategoryPlot
				.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

		localCategoryAxis.setTickLabelFont(new Font("宋体", Font.BOLD, 12));
		// 描述的显示
		// 折线的描述（就是描述各种类型的线或者图）
		LegendTitle legend = new LegendTitle(localCategoryPlot);

		legend.setPosition(RectangleEdge.LEFT);
		BlockContainer blockcontainer = new BlockContainer(
				new BorderArrangement());
		blockcontainer.add(legend, RectangleEdge.LEFT);// 这行代码可以控制位置
		CompositeTitle compositetitle = new CompositeTitle(blockcontainer);
		compositetitle.setPosition(RectangleEdge.RIGHT);// 放置图形代表区域位置的代码

		localJFreeChart.addSubtitle(compositetitle);

		return localJFreeChart;

	}

	/**
	 * 数值最大值
	 * 
	 * @author hyl
	 * @return
	 */
	private int getMaxCount() {

		int maxCount = 0;

		for (JecnRtnlProposeCountDetail detail : proposeCountBean
				.getCountDetailList()) {

			if (maxCount < detail.getTotal()) {
				maxCount = detail.getTotal();
			}
		}

		return maxCount;
	}

	/**
	 * 柱状图数值源
	 * 
	 * @return
	 * @throws Exception
	 */
	private CategoryDataset createDataset1(int type) throws Exception {

		DefaultCategoryDataset localDefaultCategoryDataset = new DefaultCategoryDataset();
		// 提交总数
		String submitTotal = JecnUtil.getValue("submitTotal");
		// 采纳数
		String acceptTotal = JecnUtil.getValue("acceptTotal");
		for (JecnRtnlProposeCountDetail proposeDetail : proposeCountBean
				.getCountDetailList()) {
			// 总数
			localDefaultCategoryDataset.addValue(proposeDetail.getTotal(),
					submitTotal, proposeDetail.getName());
			localDefaultCategoryDataset.addValue(
					proposeDetail.getAcceptTotal(), acceptTotal, proposeDetail
							.getSheetName());

		}

		return localDefaultCategoryDataset;
	}

	/*
	 * 折线数值源
	 */
	private CategoryDataset createDataset2(int type) {

		DefaultCategoryDataset localDefaultCategoryDataset = new DefaultCategoryDataset();

		String acceptPercent = JecnUtil.getValue("acceptPercent");
		for (JecnRtnlProposeCountDetail proposeDetail : proposeCountBean
				.getCountDetailList()) {
			localDefaultCategoryDataset.addValue(proposeDetail
					.getAcceptPercent(), acceptPercent, proposeDetail
					.getSheetName());

		}
		return localDefaultCategoryDataset;
	}

	/**
	 * jfreechart生成的图片,由于页面需要使用所以不能够立即删除所以需要放在PROCESS_KPI_VALUE目录下定时任务自动清理
	 * @author hyl
	 * @param localJFreeChart
	 * @param width
	 * @param height
	 * @return relativePath 生成的图片的地址,相对路径
	 */
	private String jfreeChartToPng(JFreeChart localJFreeChart, int width,
			int height) {
		String path = JecnPath.APP_PATH;

		// 获取存储临时文件的目录
		String tempFileDir = "images/tempImage/" + "PROCESS_KPI_VALUE/";
		// 创建临时文件目录
		File fileDir = new File(path+tempFileDir);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		// 使用登录人的id为图片的名称
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext()
				.getSession().get("webLoginBean");
		Long curPeopleId = webLoginBean.getJecnUser().getPeopleId();
		int type=proposeCountBean.getType();
		// 相对路径 (由于本功能有两个地方使用，为了避免功能一的图片被功能二使用，故而再加一个类型)
		String relativePath = tempFileDir + type+"_"+curPeopleId + ".png";
		// 绝对路径
		String fileName = path + relativePath;
		// 删除历史图片
		File tempFileImage = new File(fileName);
		if (tempFileImage.exists()) {
			tempFileImage.delete();
		}

		FileOutputStream fos = null;

		try {
			fos = new FileOutputStream(fileName);
			ChartUtilities.writeChartAsPNG(fos, localJFreeChart, width, height);
			fos.flush();
		} catch (Exception e) {
			log.error("ProposeDownExcelService生成图片异常", e);
		} finally {

			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("ProposeDownExcelService关闭图片流异常", e);
				}
			}
		}

		return relativePath;
	}

}
