package epros.draw.gui.figure.unit;

import java.awt.Point;

import epros.draw.constant.JecnWorkflowConstant.ReSizeLineType;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;

/**
 * 连接线端点拖动，吸附坐标处理
 * 
 *@author ZXH
 * @date 2016-11-26下午03:17:04
 */
public class DraggedManLineUtil {
	/**
	 * 连接线端点拖动-线段端点坐标对齐
	 * 
	 * @param linePanel
	 * @param lineType
	 * @throws Exception
	 */
	public static Point draggLineEndPoint(JecnBaseManhattanLinePanel linePanel, ReSizeLineType lineType, Point movePoint) {
		if (linePanel == null || lineType == null || movePoint == null) {
			return movePoint;
		}
		int moveX = movePoint.x;
		int moveY = movePoint.y;
		int otherPX = 0;
		int otherPY = 0;
		if (lineType == ReSizeLineType.start) {
			otherPX = linePanel.getEndPoint().x;
			otherPY = linePanel.getEndPoint().y;
		} else if (lineType == ReSizeLineType.end) {
			otherPX = linePanel.getStartPoint().x;
			otherPY = linePanel.getStartPoint().y;
		}
		if (Math.abs(otherPX - moveX) < 10) {// 获取中心点X坐标相等的图形元素
			// X坐标一致
			return new Point(otherPX, moveY);
		}
		if (Math.abs(otherPY - moveY) < 10) {// 获取中心点Y坐标相等的图形元素
			// Y坐标一致
			return new Point(moveX, otherPY);
		}
		return movePoint;
	}
}
