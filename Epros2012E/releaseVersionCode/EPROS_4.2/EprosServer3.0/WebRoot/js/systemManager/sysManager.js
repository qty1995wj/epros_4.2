
var sysManage_index_id='';
function sysManager() {
	var reportsPanel = new Ext.Panel({
		renderTo : "sysMain_id",
		id:"sysManagerPanel_index_id",
		border : false,
		layout : 'border',
		height : Ext.getCmp("main-panel").getInnerHeight(),
		items : [{
			region : 'west',
			border : 0,
			title : '<div id="left_title_id" style="text-align:center;font-weight:bold">系统管理</div>',
			contentEl : 'sysMainLeft_id',
			split : true,
			collapseMode : 'mini',
			width : 180

		}, {
			region : 'center',
			border : 0,
			id : 'sysManagerCenter_id',
			title : i_systemManage,
			listeners :{
				'bodyresize':function(p,width,height){
					if(sysManage_index_id!=""){
						if(sysManage_index_id=="sysManage_personManage_id"&&Ext.getCmp('personManageGridPanel_id')!=null){
							Ext.getCmp('personManageGridPanel_id').setSize(width,height-137);
						}else if(sysManage_index_id=="sysManage_roleManage_id"&&Ext.getCmp('roleManageGridPanel_id')!=null){
							Ext.getCmp('roleManageGridPanel_id').setSize(width,height-90);
						}else if(sysManage_index_id=="personImportTab_id"){
							Ext.getCmp('personImportTab_id').setSize(width,height);
						}
					}
				}
			}			

		}]
	});
	var sysMainSelectedLi;
	$("#sysMainUl_id").children().bind("click", function(e) {
		$(sysMainSelectedLi).css("background",
				"url(" + basePath + "images/common/libackground.jpg)");
		e.target.style.background = "url(" + basePath
				+ "images/common/libackgroundClicked.jpg)";
		sysMainSelectedLi = e.target;
		Ext.getCmp("sysManagerCenter_id").setTitle(sysMainSelectedLi.innerHTML);
		Ext.getCmp("sysManagerCenter_id").removeAll(true);
		Ext.getCmp("sysManagerCenter_id").load({
			url : sysMainSelectedLi.attributes.id.value.split(';')[0]+'?timsStamp='+getTimeStamp(),
			scripts : true,
			callback : function(el, success, response) {
				if (sysMainSelectedLi.attributes.id.value.split(';').length == 2) {
					eval(sysMainSelectedLi.attributes.id.value.split(';')[1])();
				}
			}
		});
	});
}

function personManageGrid() {

	personManageGridStore = new Ext.data.Store({
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize
			}
		},
		proxy : new Ext.data.HttpProxy({
			url : "personManageList.action"
		}),
		reader : new Ext.data.JsonReader({
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [{
			name : "userId",
			mapping : "userId"
		}, {
			name : "loginName",
			mapping : "loginName"
		}, {
			name : "trueName",
			mapping : "trueName"
		}, {
			name : "posName",
			mapping : "listPos"
		}, {
			name : "orgId",
			mapping : "orgId"
		}, {
			name : "orgName",
			mapping : "listOrg"
		}, {
			name : "roleName",
			mapping : "listRole"
		}])

	});

	personManageGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_loginName,// 登录名称
				dataIndex : "loginName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_trueName,// 真实名称
				dataIndex : "trueName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_belongOrg,// 所属部门
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer : personManageRenderer
			}, {
				header : i_officeholdingPosition,// 任职岗位
				dataIndex : "posName",
				align : "center",
				menuDisabled : true,
				renderer : personManageRenderer
			}, {
				header : i_sysRole,// 系统角色
				dataIndex : "roleName",
				align : "center",
				menuDisabled : true,
				renderer : personManageRenderer
			}

	]);
	personManageGridBbar = new Ext.PagingToolbar({
		pageSize : pageSize,
		store : personManageGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	personManageGridPanel = new Ext.grid.GridPanel({
		id : "personManageGridPanel_id",
		renderTo : 'personManageGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 163,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		// columnLines:true,//显示列的分隔线
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		store : personManageGridStore,
		colModel : personManageGridColumnModel,
		bbar : personManageGridBbar
	})
	sysManage_index_id='sysManage_personManage_id';
}
/**
 * 人员管理搜索
 */
function personManageGridSearch() {
	personManageGridStore.setBaseParam("loginName",
			$("#personManage_loginName_id").attr("value"));
	personManageGridStore.setBaseParam("trueName",
			$("#personManage_trueName_id").attr("value"));
	personManageGridStore.setBaseParam("orgName",
			$("#personManage_belongOrgName_id").attr("value"));
	personManageGridStore.setBaseParam("orgId", $("#personManage_orgId_id")
			.attr("value"));
	personManageGridStore.setBaseParam("posName", $("#personManage_PosName_id")
			.attr("value"));
	personManageGridStore.setBaseParam("posId", $("#personManage_posId_id")
			.attr("value").split('_')[0]);
	personManageGridStore.setBaseParam("roleType", $("#personManage_role_id")
			.val());
	personManageGridStore.load({
		params : {
			start : 0,
			limit : pageSize
		}
	});
}
/**
 * 人员管理搜索重置
 */
function personManageReset() {
	document.getElementById("personManage_loginName_id").value = "";
	document.getElementById("personManage_trueName_id").value = "";
	document.getElementById("personManage_belongOrgName_id").value = "";
	document.getElementById("personManage_orgId_id").value = -1;
	document.getElementById("personManage_PosName_id").value = "";
	document.getElementById("personManage_posId_id").value = -1;
	document.getElementById("personManage_role_id").options[0].selected = true;
}

function personManageRenderer(value, cellmeta, record) {
	rendererValue = "<div>";
	for (i = 0; i < value.length; i++) {
		rendererValue += value[i] += "<br/>";
	}
	return rendererValue + "</div>";

}

function roleManageGrid() {

	roleManageGridStore = new Ext.data.Store({
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize
			}
		},
		proxy : new Ext.data.HttpProxy({
			url : "roleManageList.action"
		}),
		reader : new Ext.data.JsonReader({
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [{
			name : "roleId",
			mapping : "roleId"
		}, {
			name : "roleName",
			mapping : "roleName"
		}, {
			name : "roleRemark",
			mapping : "roleRemark"
		}])

	});

	roleManageGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_roleName,// 角色名称
				dataIndex : "roleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_remark,// 备注
				dataIndex : "roleRemark",
				align : "center",
				menuDisabled : true
			}

	]);
	roleManageGridBbar = new Ext.PagingToolbar({
		pageSize : pageSize,
		store : roleManageGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	roleManageGridPanel = new Ext.grid.GridPanel({
		id : "roleManageGridPanel_id",
		renderTo : 'roleManageGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 117,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		// columnLines:true,//显示列的分隔线
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		store : roleManageGridStore,
		colModel : roleManageGridColumnModel,
		bbar : roleManageGridBbar
	})
sysManage_index_id='sysManage_roleManage_id';
}
/**
 * 人员管理搜索
 */
function roleManageGridSearch() {
	roleManageGridStore.setBaseParam("roleName", $("#roleName_id")
			.attr("value"));
	roleManageGridStore.load({
		params : {
			start : 0,
			limit : pageSize
		}
	});
}
/**
 * 人员管理搜索重置
 */
function roleManageReset() {
	document.getElementById("roleName_id").value = "";
}
