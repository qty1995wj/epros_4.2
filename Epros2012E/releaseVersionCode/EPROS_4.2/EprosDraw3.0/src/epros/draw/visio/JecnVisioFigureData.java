package epros.draw.visio;

/**
 * 存储图形部分数据
 * 
 * @author fuzhh 2013-7-12
 * 
 */
public class JecnVisioFigureData {
	/** 图形标识 */
	private String fugureType;
	/** 图形ID */
	private String figureId;
	/** 图形的顺序 ， 代表图形的层级 */
	private int index;
	/** 图形的master */
	private String master;
	/** 图形标识 */
	private String fugereFlag;
	/** 图形的masterShape */
	private String masterShape;

	public String getFugureType() {
		return fugureType;
	}

	public void setFugureType(String fugureType) {
		this.fugureType = fugureType;
	}

	public String getFigureId() {
		return figureId;
	}

	public void setFigureId(String figureId) {
		this.figureId = figureId;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getMaster() {
		return master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	public String getFugereFlag() {
		return fugereFlag;
	}

	public void setFugereFlag(String fugereFlag) {
		this.fugereFlag = fugereFlag;
	}

	public String getMasterShape() {
		return masterShape;
	}

	public void setMasterShape(String masterShape) {
		this.masterShape = masterShape;
	}
}
