package com.jecn.epros.server.download.wordxml.mengniu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.constant.Constants.Valign;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ParagraphRowIndBean;
import wordxml.element.bean.paragraph.GroupBean.GroupAlign;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.bean.paragraph.GroupBean.Hanchor;
import wordxml.element.bean.paragraph.GroupBean.Vanchor;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.JecnWordUtil;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.download.wordxml.approve.ApproveManager;
import com.jecn.epros.server.download.wordxml.approve.ApprovePeople;
import com.jecn.epros.server.download.wordxml.approve.ApproveState;
import com.jecn.epros.server.util.JecnUtil;

public class MNProcessModel extends ProcessFileModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/****** 段落行距固定值20磅 *********/
	private ParagraphLineRule lineRule = new ParagraphLineRule(18, LineRuleType.EXACT);

	/**
	 * 默认模版
	 * 
	 * @param processDownloadBean
	 * @param path
	 * @param flowChartDirection
	 */
	public MNProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(15F);
		getDocProperty().setNewRowHeight(0.82F);
		getDocProperty().setFlowChartScaleValue(6F);
		setDocStyle(" ", textTitleStyle());
		setDefAscii("黑体");
	}

	/**
	 * 重写流程图标题段落高度
	 */
	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		super.a09(processFileItem, images);
		ParagraphBean newBean = textTitleStyle();
		newBean.setSpace(0F, 0F, vLine1);
		processFileItem.getBelongTo().getParagraph(0).setParagraphBean(newBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 15.6F));
		// 设置页边距
		sectBean.setPage(3.7F, 2.8F, 3.5F, 2.6F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	private SectBean getTitleSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(null);
		// 设置页边距
		sectBean.setPage(3.7F, 2.8F, 3.5F, 2.6F, 1.5F, 1.75F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/***
	 * 创建通用页眉页脚
	 * 
	 * @param sect
	 */
	private void createCommhdrftr(Sect sect) {
		createCommftr(sect, HeaderOrFooterType.odd);
	}

	/**
	 * 创建通用的页眉
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect, HeaderOrFooterType type) {
		Footer footer = sect.createFooter(type);
		ParagraphBean pBean = new ParagraphBean(Align.center, "仿宋_GB2312", Constants.xiaosi, false);
		footer.createParagraph("公司机密,未经批准不得扩散", pBean);
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		SectBean sectBean = getSectBean();
		sectBean.setStartNum(1);
		firstSect.setSectBean(sectBean);
		createCommhdrftr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setPage(2.0F, 1.76F, 1.5F, 1.76F, 0.5F, 0.5F);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommhdrftr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdrftr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getTitleSectBean());

		Image logo = new Image(getDocProperty().getLogoImage());
		logo.setSize(3.76F, 1.46F);
		Paragraph p = titleSect.createParagraph("");
		p.setParagraphBean(new ParagraphBean(Align.right));
		p.appendGraphRun(logo);
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		pBean.setSpace(0F, 0F, vLine1);
		ParagraphRowIndBean iBean = new ParagraphRowIndBean();
		iBean.setLeft(0F);
		// 0.11个字符 0.9187282
		iBean.setRight(0.101060102F);
		pBean.setParagraphRowIndBean(iBean);
		// 公司名称
		titleSect.createParagraph("内蒙古蒙牛乳业（集团）股份有限公司", pBean);

		crateFlowNumber(titleSect);

		// 创建一条线
		createLine(titleSect, 0, 35, 438, 35);

		createFlowName(titleSect);

		createPubLine(titleSect);

		createLine(titleSect, 0, 480, 438, 480);

		createComponyPubLine(titleSect);

		// 分页
		titleSect.addParagraph(new Paragraph(true));

		createApprovePage(titleSect);

		createCommhdrftr(titleSect);

	}

	private void crateFlowNumber(Sect titleSect) {

		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 7, GroupH.exact, 0.6).setHorizontal(12, Hanchor.page, 0)
				.setVertical(6.8, Vanchor.page, 0).lock(true);

		// 编号
		ParagraphBean pBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		pubGroup.createParagraph("编号：" + nullToEmpty(processDownloadBean.getFlowInputNum()), pBean);

	}

	private void createFlowName(Sect titleSect) {
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.auto, 0, GroupH.auto, 0).setHorizontal(GroupAlign.center, Hanchor.page,
				0).setVertical(12F, Vanchor.page, 0).lock(true);

		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		// 仿宋_GB2312 一号 加粗 居中
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		pubGroup.addParagraph(new Paragraph(flowName, pBean));
	}

	private void createApprovePage(Sect titleSect) {

		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.xiaoer, false);
		pBean.setInd(0, 0, 0, 0.78f);
		pBean.setSpace(1f, 1.5f, 0, vLine1);
		// 审 批 单
		titleSect.createParagraph("审 批 单", pBean);
		titleSect.addParagraph(getEmptyEight());

		TableBean tBean = new TableBean();
		tBean.setBorder(0.5f);
		tBean.setTableAlign(Align.center);
		
		ParagraphBean titleP = new ParagraphBean(Align.center, "仿宋_GB2312", Constants.sanhao, true);
		ParagraphBean contentP = new ParagraphBean(Align.center, "仿宋_GB2312", Constants.sanhao, false);

		List<String[]> approveList = new ArrayList<String[]>();
		List<String[]> lastApproveList = new ArrayList<String[]>();

		Long flowId = processDownloadBean.getFlowId();
		List<TaskConfigItem> configs = JecnCommon.getVisibleTaskConfigs(JecnCommon.getTaskConfigItems(
				processDownloadBean.getBaseDao(), flowId, 0, 0));
		if (JecnUtil.isNotEmpty(configs) && processDownloadBean.getLatestHistoryNew() != null) {
			ApproveManager approveManager = new ApproveManager(processDownloadBean.getBaseDao(), processDownloadBean
					.getLatestHistoryNew());
			ApproveState a = null;
			ApproveState b = null;
			if (configs.size() <= 5) {
				a = approveManager.getState(11);
				b = approveManager.getState(12);
			} else {
				/**
				 * 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段
				 * 7：IT总监审批阶段 8:事业部经理 9：总经理 10:整理意见
				 */
				a = approveManager.mergeState(1, 4, 6);
				b = approveManager.mergeState(7, 8, 9);
			}

			addApprove(approveList, a);
			addApprove(lastApproveList, b);
		}
		Table approveTable = titleSect.createTable(new float[] { 5.94f, 4.56f });
		approveTable.setTableBean(tBean);
		approveTable.createTableRow(new String[] { "审核单位", "审核人" }, titleP);
		approveTable.createTableRow(approveList, contentP);
		
		titleSect.createMutilEmptyParagraph(2);
		
		Table lastApproveTable = titleSect.createTable(new float[] { 5.94f, 4.56f });
		lastApproveTable.setTableBean(tBean);
		lastApproveTable.createTableRow(new String[] { "批准单位", "批准人" }, titleP);
		lastApproveTable.createTableRow(lastApproveList, contentP);

	}

	private void addApprove(List<String[]> approveList, ApproveState a) {
		if (a != null && JecnUtil.isNotEmpty(a.getPeoples())) {
			List<ApprovePeople> peoples = a.getPeoples();
			for (ApprovePeople people : peoples) {
				approveList.add(new String[] { concat(people.getOrgs()), JecnUtil.nullToEmpty(people.getName()) });
			}
		}
	}

	private void createComponyPubLine(Sect titleSect) {

		ParagraphBean pubBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.auto, 0, GroupH.auto, 0).setHorizontal(GroupAlign.center, Hanchor.page,
				0).setVertical(24, Vanchor.page, 0).lock(true);
		String org = nullToEmpty(processDownloadBean.getOrgName());
		pubGroup.addParagraph(new Paragraph("内蒙古蒙牛乳业（集团）股份有限公司" + org + "  发布", pubBean));

	}

	private void createPubLine(Sect titleSect) {

		String pubDateStr = nullToEmpty(processDownloadBean.getReleaseDate());
		if (pubDateStr.length() > 10) {
			pubDateStr = pubDateStr.substring(0, 10).replaceFirst("-", "年").replaceFirst("-", "月") + "日";
		}
		// 2016 - 05 - 01实施 黑体 14
		String nextMonthFirstDay = "";
		if (StringUtils.isNotBlank(pubDateStr)) {

			try {
				// 发布日期后的三天
				SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
				Date pubDate = format.parse(pubDateStr);
				Calendar cal = Calendar.getInstance();
				cal.setTime(pubDate);
				cal.add(Calendar.DAY_OF_MONTH, 3);
				nextMonthFirstDay = format.format(cal.getTime());

			} catch (ParseException e) {
				log.error(e);
			}
		}
		// ParagraphBean pBean = new ParagraphBean(Align.center, "黑体",
		// Constants.sihao, false);
		// pBean.setSpace(0f, 0f, 0, new ParagraphLineRule(28,
		// LineRuleType.EXACT));
		// titleSect.createParagraph(pubDateStr + "发布" +
		// "	                             " + nextMonthFirstDay + "实施",
		// pBean);

		// 2016 - 04 - 11发布 黑体 四号
		ParagraphBean pubBean = new ParagraphBean(Align.left, "黑体", Constants.sihao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(2.8, Hanchor.page, 0)
				.setVertical(23, Vanchor.page, 0.32).lock(true);
		pubGroup.addParagraph(new Paragraph(pubDateStr + "发布", pubBean));

		// 2016 - 04 - 11实施 黑体 四号
		ParagraphBean nextBean = new ParagraphBean(Align.right, "黑体", Constants.sihao, false);
		Group nextGroup = titleSect.createGroup();
		nextGroup.getGroupBean().setSize(GroupW.exact, 7.05, GroupH.exact, 0.83).setHorizontal(11.3, Hanchor.page, 0)
				.setVertical(23, Vanchor.page, 0.32).lock(true);
		nextGroup.addParagraph(new Paragraph(nextMonthFirstDay + "实施", nextBean));
	}

	private void createLine(Sect sect, float fx, float fy, float tx, float ty) {
		// 单线条
		LineGraph line = new LineGraph(LineStyle.single, 0.5F);
		line.setFrom(fx, fy);
		line.setTo(tx, ty);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(2);
		line.setPositionBean(positionBean);
		sect.createParagraph(line);
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean(Align.left, new FontBean("仿宋_GB2312", Constants.wuhao, false));
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		/** 默认表格样式 ****/
		TableBean tblStyle = new TableBean();// 
		// 所有边框 1.5 磅
		tblStyle.setBorder(0.5F);
		// 表格左缩进0.01 厘米
		tblStyle.setTableLeftInd(0.01F);
		// tblStyle.setTableAlign(Align.center);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, new FontBean("仿宋_GB2312", Constants.wuhao, true));
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, new FontBean("仿宋_GB2312", Constants.sanhao,
				false));
		// 2字符转厘米 0.4240284
		textContentStyle.setInd(0, 0, 0, 0.8480568f);
		textContentStyle.setSpace(0.2f, 0.2f, lineRule);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean(Align.left, new FontBean("黑体", Constants.sanhao, true));
		textTitleStyle.setInd(0, 0, 0.74F, 0); // 段落缩进 厘米
		textTitleStyle.setSpace(1F, 1F, vLine1); // 设置段落行间距
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	protected void itemAfterHandle(JecnConfigItemBean configItem, ProcessFileItem processFileItem) {
		Sect sect = processFileItem.getBelongTo();
		sect.createParagraph("");

	}

	@Override
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {

		// 段落显示 1:段落；0:表格（默认）
		if (processDownloadBean.getFlowFileType() == 1) {
			a10InParagraph(processFileItem, rowData);
			return;
		}
		// 标题
		createTitle(processFileItem);
		// 添加标题
		rowData.add(0, titles);
		float[] width = new float[] { 1.5F, 2.29F, 2.31F, 5, 3, 3 };
		Table tbl = createTableItem(processFileItem, width, rowData);
		// 设置第一行居中
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());

	}

	@Override
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {

		// 标题
		createTitle(processFileItem);
		// 角色名称
		String roleName = JecnUtil.getValue("roleName");
		// 岗位名称
		String positionName = JecnUtil.getValue("positionName");
		// 角色职责
		String roleResponsibility = JecnUtil.getValue("responsibilities");
		if (JecnContants.showPosNameBox) {
			// 标题行
			rowData.add(0, new String[] { roleName, positionName, roleResponsibility });
		} else {
			List<String[]> newDataRows = new ArrayList<String[]>();
			String[] newData = null;
			for (String[] data : rowData) {
				newData = new String[] { data[0], data[2] };
				newDataRows.add(newData);
			}
			rowData.clear();
			rowData = newDataRows;
			// 标题行
			rowData.add(0, new String[] { roleName, roleResponsibility });
		}
		float[] width = new float[] { 6, 10F };
		if (JecnContants.showPosNameBox) {
			width = new float[] { 3, 3, 10F };
		}
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());

	}

	/**
	 * 流程文控信息
	 * 
	 * @param name
	 * @param documentList
	 * @param contentSect
	 */
	protected void a24(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 版本号
		String version = JecnUtil.getValue("versionNumber");
		// 变更说明
		String descOfChange = JecnUtil.getValue("changeThat");
		// 发布日期
		String releaseDate = JecnUtil.getValue("releaseDate");
		// 流程拟稿人
		String artificialPerson = "拟稿人";
		// 审批人
		String approver = JecnUtil.getValue("theApprover");
		rowData.add(0, new String[] { version, descOfChange, releaseDate, artificialPerson, approver });
		float[] width = new float[] { 2, 6F, 3F, 2.4F, 4.5F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	@Override
	protected void beforeHandle() {
		Sect firstSect = docProperty.getFirstSect();
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		Paragraph paragraph = firstSect.createParagraph(flowName);
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.xiaoer, false);
		pBean.setSpace(1F, 1F, lineRule);
		paragraph.setParagraphBean(pBean);
		firstSect.addParagraph(getEmptyEight());
	}

	/**
	 * 空八号
	 * 
	 * @return
	 */
	private Paragraph getEmptyEight() {
		Paragraph paragraph = new Paragraph(" ");
		paragraph.setParagraphBean(new ParagraphBean("仿宋_GB2312", Constants.bahao, false));
		return paragraph;
	}

	/**
	 * 自定义1
	 * 
	 * @param titleName
	 *            标题
	 * @param content
	 *            内容
	 */
	protected void a27(ProcessFileItem processFileItem, String... content) {
		createTitle(processFileItem);
		Sect sect = processFileItem.getBelongTo();
		Table titleTable = new Table(tblStyle(), new float[] { 4, 3, 3, 3, 3 });
		sect.addTable(titleTable);
		List<String[]> titles = new ArrayList<String[]>();
		titles.add(new String[] { "活动\n（一般是流程中活动组或关键活动）", "角色（一般表述为部门或核心岗位）" });
		titles.add(new String[] { "", "", "", "", "" });
		titleTable.createTableRow(titles, tblTitleStyle());
		titleTable.getRow(0).setValign(Valign.center);

		titleTable.getRow(0).getCell(1).setVmergeBeg(true);
		titleTable.getRow(0).getCell(1).setColspan(4);

		titleTable.getRow(0).getCell(0).setVmergeBeg(true);
		titleTable.getRow(1).getCell(0).setVmerge(true);
		titleTable.getRow(0).getCell(0).setRowspan(2);

		Table contentTable = new Table(tblStyle(), new float[] { 4, 3, 3, 3, 3 });
		sect.addTable(contentTable);
		List<String[]> rows = new ArrayList<String[]>();
		for (int i = 0; i < 7; i++) {
			rows.add(new String[] { "", "", "", "", "" });
		}
		contentTable.createTableRow(rows, tblContentStyle());

		Table detailTable = new Table(tblStyle(), new float[] { 16F });
		sect.addTable(detailTable);
		String detail = "说明：注意：每项活动有且只有⼀项R。" + "\nR 表示：负责启动该项活动，并确保该活动的顺利完成。" + "\nS 表示：为该项活动提供支持，同时需注明提供何种支持。"
				+ "\nAV表示：审批或否决该项活动的权力。" + "\nR*表示：部分负责启动某⼀活动，并确保该活动的顺利完成。" + "\nI 表示：必须被告知，但是没有直接影响力。";
		detailTable.createTableRow(new String[] { detail }, tblContentStyle());

		setHeight(titleTable, 0.77F);
		setHeight(contentTable, 0.77F);
	}

	private void setHeight(Table table, float height) {
		for (TableRow row : table.getRows()) {
			row.setHeight(height);
		}
	}

	@Override
	public void a11(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 文件编号 文件名称
		List<Object[]> processRecordList = processFileItem.getDataBean().getProcessRecordList();
		List<Object[]> operationTemplateList = processFileItem.getDataBean().getOperationTemplateList();
		List<Object[]> ruleNameList = processFileItem.getDataBean().getRuleNameList();

		List<Object[]> objList = new ArrayList<Object[]>();
		for (Object[] obj : processRecordList) {
			objList.add(new Object[] { obj[0], obj[1] });
		}
		for (Object[] obj : operationTemplateList) {
			objList.add(new Object[] { obj[0], obj[1] });
		}
		for (Object[] obj : ruleNameList) {
			objList.add(new Object[] { obj[2], obj[0] });
		}
		super.a11(processFileItem, JecnWordUtil.obj2String(objList));
	}

	@Override
	public void a12(ProcessFileItem processFileItem, List<String[]> rowData) {

	}

	@Override
	public void a14(ProcessFileItem processFileItem, List<String[]> rowData) {

	}

}
