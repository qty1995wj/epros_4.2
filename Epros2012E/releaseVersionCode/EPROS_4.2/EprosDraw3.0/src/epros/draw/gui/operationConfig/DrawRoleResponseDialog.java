package epros.draw.gui.operationConfig;

import javax.swing.JTable;

import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.workflow.dialog.JecnRoleDetailsDialog;

/*******************************************************************************
 * 角色职责
 * 
 * @author 2012-08-28
 * 
 */
public class DrawRoleResponseDialog extends JecnRoleDetailsDialog {
	/** 用来显示的数据 */
	private JecnBaseRoleFigure baseRoleFigure;

	/** 点击的table */
	private RoleResponseTable roleResponseTable;
	/** 点击table的行数 */
	private int selectRow;
	/** 活动说明table */
	private JTable drawActiveDescTable;

	public DrawRoleResponseDialog(JecnBaseRoleFigure baseRoleFigure,
			RoleResponseTable roleResponseTable, int selectRow,
			JTable drawActiveDescTable) {
		super(baseRoleFigure);
		this.baseRoleFigure = baseRoleFigure;
		this.drawActiveDescTable = drawActiveDescTable;
		this.roleResponseTable = roleResponseTable;
		this.selectRow = selectRow;
	}

	/**
	 * 确定事件
	 * 
	 * @author fuzhh Oct 11, 2012
	 */
	public void updateValuesToActionFigure() {
		super.updateValuesToActionFigure();
		roleResponseTable.setValueAt(roleNameTextArea.getText(), selectRow, 1);

		roleResponseTable.setValueAt(roleResTextArea.getText(), selectRow, 2);

		for (int index = drawActiveDescTable.getModel().getRowCount() - 1; index >= 0; index--) {
			if (drawActiveDescTable.getValueAt(index, 7) == null) {
				continue;
			}
			String rUIID = drawActiveDescTable.getValueAt(index, 7).toString();
			if (baseRoleFigure.getFlowElementData().getFlowElementUUID()
					.equals(rUIID)) {
				drawActiveDescTable.setValueAt(roleNameTextArea.getText(),
						index, 2);
			}
		}
		this.dispose();
	}
}
