package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.EnumClass.TreeNodeType;
import com.jecn.epros.domain.process.SearchBean;
import com.jecn.epros.domain.rule.G020RuleFileContent;
import com.jecn.epros.security.JecnContants;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant.TYPEAUTH;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.Contants;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.util.Utils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RuleSqlProvider extends BaseSqlProvider {

    public String saveRuleFileContent(G020RuleFileContent curContent) {

        String id = "";
        String value = "";

        if (isOracle()) {
            id = "id,";
            id = "#{id},";
        }

        String sql = "insert into G020_Rule_File_Content(" + id + "rule_id,file_stream," + "file_path,update_time,"
                + "update_people_id,file_name," + "is_version_local)" + "values(" + value
                + "#{id},#{fileStream,jdbcType=BLOB},#{filePath},#{updateTime},#{updatePeopleId},#{fileName},#{isVersionLocal}"
                + ")";

        return sql;
    }


    /**
     * 制度搜索
     *
     * @param map
     * @return
     */
    public String searchRule(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchBean searchBean = (SearchBean) map.get("searchBean");
        boolean isAdmin = (boolean) map.get("isAdmin");
        String sql = "select s_rule.* from" + searchRuleSQLCommon(projectId, searchBean);
        if (!isAdmin && !Contants.isSystemAdmin) {
            map.put("typeAuth", TYPEAUTH.RULE);
            sql = sql
                    + "  inner join "
                    + CommonSqlTPath.INSTANCE.getFileSearch(map) + " on MY_AUTH_FILES.id=s_rule.rule_id";
        }
        return sql;
    }

    /**
     * 制度搜索 总数
     *
     * @param map
     * @return
     */
    public String searchRuleTotal(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchBean searchBean = (SearchBean) map.get("searchBean");
        boolean isAdmin = (boolean) map.get("isAdmin");
        String sql = "select count(s_rule.rule_id) from" + searchRuleSQLCommon(projectId, searchBean);
        if (!isAdmin && !Contants.isSystemAdmin) {
            map.put("typeAuth", TYPEAUTH.RULE);
            sql = sql
                    + "  inner join "
                    + CommonSqlTPath.INSTANCE.getFileSearch(map) + " on MY_AUTH_FILES.id=s_rule.rule_id";
        }
        return sql;
    }

    /**
     * 制度搜索 管理员ROLE
     *
     * @return
     */
    private String searchRuleSQLCommon(Long projectId, SearchBean searchbean) {
        String sql = " (select distinct jr.id as rule_id, jr.rule_name"
                + "        ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.doc_id else jr.rule_number end as RULE_NUMBER"
                + "        ,jr.org_id,jfo.org_name,jr.is_public,jr.pub_time"
                + " from jecn_rule jr"
                + " left join jecn_flow_org jfo on jfo.del_state=0 and jfo.org_id = jr.org_id"
                + " left join jecn_file jf on jf.file_id = jr.file_id and jf.del_state=0"
                + " where jr.is_dir<>0 and jr.del_state=0 and jr.project_id=" + projectId;
        // 制度名称
        if (StringUtils.isNotBlank(searchbean.getName())) {
            sql = sql + " and (jr.rule_name like '%" + searchbean.getName() + "%'" + " or jr.RULE_NUMBER like '%" + searchbean.getName() + "%'" + " or jr.KEYWORD like '%" + searchbean.getName() + "%')";
        }
        // 责任部门
        if (searchbean.getOrgId() != null) {
            sql = sql + " and jr.org_id=" + searchbean.getOrgId();
        }
        // 密级
        if (searchbean.getSecret() != -1) {
            sql = sql + " and jr.is_public=" + searchbean.getSecret();
        }
        sql += ") s_rule";
        return sql;
    }


    /**
     * 获得制度相关流程架构
     *
     * @param map
     * @return
     */
    public String findChildFlowMaps(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        Long id = Long.valueOf(map.get("id").toString());
        String sql = "select jf.flow_id,jf.flow_name,jf.flow_id_input"
                + "  from jecn_flow_structure" + isPub + " jf,jecn_flow_basic_info jfbi"
                + "  left join JECN_FLOW_RELATED_ORG" + isPub + " jfro on jfro.flow_id = jfbi.flow_id"
                + " ,FLOW_RELATED_CRITERION" + isPub + " frc"
                + "  where jfbi.flow_id = jf.flow_id and frc.flow_id = jf.flow_id and jf.isflow=0   and jf.del_state = 0 and frc.criterion_class_id = "
                + id;
        return sql;
    }

    /**
     * 获得制度相关流程
     *
     * @param map
     * @return
     */
    public String findChildFlows(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        Long id = Long.valueOf(map.get("id").toString());
        String sql = "select jf.flow_id,jf.flow_name,jf.flow_id_input,jfo.ORG_NAME"
                + "   ,case when jfbi.type_res_people=0 then ju.true_name"
                + "    when jfbi.type_res_people=1 then jfoi.figure_text" + "    end as RES_PEOPLE"
                + "  from jecn_flow_structure" + isPub + " jf,jecn_flow_basic_info" + isPub + " jfbi"
                + "  left join JECN_FLOW_RELATED_ORG" + isPub + " jfro on jfro.flow_id = jfbi.flow_id"
                + "  left join jecn_flow_org jfo on jfo.org_id =  jfro.org_id and jfo.del_state=0"
                + "  left join jecn_user ju on jfbi.type_res_people=0 and  ju.islock=0 and ju.people_id = jfbi.res_people_id"
                + "  left join jecn_flow_org_image jfoi on jfbi.type_res_people=1 and jfoi.figure_id=jfbi.res_people_id"
                + " ,FLOW_RELATED_CRITERION" + isPub + " frc"
                + "  where jfbi.flow_id = jf.flow_id and frc.flow_id = jf.flow_id and jf.isflow=1 and frc.criterion_class_id = "
                + id + " and jf.del_state=0 ";
        return sql;
    }

    /**
     * 制度相关标准
     *
     * @param map
     * @return
     */
    public String findRelatedStandard(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        Long id = Long.valueOf(map.get("id").toString());
        String sql = "select jcc.CRITERION_CLASS_ID, jcc.CRITERION_CLASS_NAME, jcc.STAN_TYPE"
                + "   from JECN_RULE_STANDARD" + isPub + " jrs,JECN_CRITERION_CLASSES jcc"
                + "  where jcc.CRITERION_CLASS_ID = jrs.STANDARD_ID" + "    and jrs.RULE_ID =" + id;
        return sql;
    }


    /**
     * 制度相关标准文控
     *
     * @param map
     * @return
     */
    public String findRelatedStandardHistory(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        Long historyId = Long.valueOf(map.get("historyId").toString());
        String sql = "select jcc.CRITERION_CLASS_ID, jcc.CRITERION_CLASS_NAME, jcc.STAN_TYPE"
                + "   from JECN_STANDARD_HISTORY jrs,JECN_CRITERION_CLASSES jcc"
                + "  where jcc.CRITERION_CLASS_ID = jrs.STANDARD_ID" + " and RELATE_TYPE = 1  and jrs.HISTORY_ID =#{historyId} and jrs.RELATE_ID = #{id}";
        return sql;
    }

    /**
     * 制度相关风险
     *
     * @param map
     * @return
     */
    public String findRelatedRisk(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        Long id = Long.valueOf(map.get("id").toString());
        String sql = "select jr.id,jr.name,jr.risk_code from jecn_risk  jr,JECN_RULE_RISK" + isPub + "  jrr "
                + "  where jr.id = jrr.RISK_ID and jrr.RULE_ID =" + id;
        return sql;
    }

    /**
     * 相关制度
     *
     * @param map
     * @return
     */
    public String findRelatedRuleSql(Map<String, Object> map) {
        // Long flowId = (Long) map.get("flowId");
        String isPub = map.get("isPub").toString();
        Long id = Long.valueOf(map.get("id").toString());
        return "select r.id," +
                "       r.rule_name," +
                "       r.rule_number" +
                "  from Jecn_Rule_Related_Rule" + isPub + " rs, JECN_RULE" + isPub + " r" +
                "  where r.del_state = 0" +
                "   and rs.related_id = r.id" +
                "   and rs.rule_id = " + id;
    }

    /**
     * 相关制度
     *
     * @param map
     * @return
     */
    public String findRelatedRuleHSql(Map<String, Object> map) {
        // Long flowId = (Long) map.get("flowId");
        Long id = Long.valueOf(map.get("id").toString());
        Long historyId = Long.valueOf(map.get("historyId").toString());
        return "select r.id," +
                "       r.rule_name," +
                "       r.rule_number" +
                "  from Jecn_Rule_Related_Rule_H rs, JECN_RULE r" +
                "  where r.del_state = 0" +
                "   and rs.related_id = r.id" +
                "   and rs.HISTORY_ID = #{historyId}" +
                "   and rs.rule_id = " + id;
    }


    /**
     * 制度相关风险文控
     *
     * @param map
     * @return
     */
    public String findRelatedRiskHistory(Map<String, Object> map) {
        String isPub = map.get("isPub").toString();
        Long historyId = Long.valueOf(map.get("historyId").toString());
        String sql = "select jr.id,jr.name,jr.risk_code from jecn_risk  jr,JECN_RELATE_RISK_HISTORY  jrr" +
                "    where jr.id = jrr.RISK_ID  and jrr.relate_type = 1 and  jrr.HISTORY_ID = #{historyId} and jrr.RELATE_ID = #{id}";
        return sql;
    }

    /**
     * 制度树展开 WEB
     */
    String sqlCommonfindChildRules = " select distinct t.id, t.rule_name "
            + ",t.is_dir,case when ju.id is null then '0' else '1' end as count,t.sort_id,t.per_id"
            + " from jecn_rule t " + " left join jecn_rule ju on ju.per_id=t.id";

    /**
     * 制度树展开 WEB 管理员 流程管理员 制度模块默认公开
     */
    public String findChildRulesAdminSql(Map<String, Object> map) {
        return sqlCommonfindChildRules + sqlCommonfindChildRules(map);
    }

    /**
     * 制度树展开 WEB
     */
    private String sqlCommonfindChildRules(Map<String, Object> map) {
        if (map.get("pId") == null) {
            throw new IllegalArgumentException("展开树节点，获取上级ID异常！");
        }
        Long pId = Long.valueOf(map.get("pId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return " where t.per_id= " + pId + " and t.del_state=0 and t.project_id=" + projectId
                + " order by t.per_id,t.sort_id,t.id";
    }

    /**
     * 制度清单
     */
    public String fetchInventory(Map<String, Object> map) {
        String isVersion = map.get("downloadType") == null ? null : map.get("downloadType").toString();
        if ("1".equals(isVersion)) {
            return getRuleVersionInventorySql(map);
        } else {
            return getRuleInventorySql(map);
        }
    }

    private String getRuleInventorySql(Map<String, Object> map) {
        String sql = "select jr.id,jr.per_id,jr.is_dir"
                + "            ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.file_name else jr.rule_name end as ruleName"
                + "              ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.doc_id else jr.rule_number end as rule_Number"
                + "               ,jf.file_id,jfo.org_id,jfo.org_name," +
                "            case" +
                "         when t.type_res_people = 0 then" +
                "          ju.people_id" +
                "         else" +
                "          pos.figure_id" +
                "       end as res_id," +
                "       case" +
                "         when t.type_res_people = 0 then" +
                "          ju.true_name" +
                "         else" +
                "          pos.figure_text" +
                "       end as res_name," +
                "       t.type_res_people,  jr.is_public,t.update_date"
                + "                ,t.id as ispub,jtbn.state,"
                + "			 case when t.id is null and jtbn.state is not null THEN 1 " //审批中
                + "			 when t.id is not null then 2"//发布
                + "			 else 0 end as type_by_data, "//未发布 待建
                + "jt.version_id,jr.t_level,jr.t_path,t.pub_time,jr.expiry,JR.CONFIDENTIALITY_LEVEL, jr.ward_people_id,ward.true_name ward_people_name,jt.draft_person "
                + "              from jecn_rule_t jr " + "              inner join ("
                + InventorySqlProvider.getRules(map)
                + ") id on id.id=jr.id" + "                left join jecn_rule t on t.id=jr.id"
                + "                left join jecn_flow_org jfo on jfo.org_id = jr.org_id"
                + "                 left join jecn_file_t jf on jf.file_id = jr.file_id "
                + "                 left join jecn_task_history_new jt on jt.id = jr.history_id"
                + "                  left join jecn_task_bean_new jtbn on jtbn.task_type in (2,3)"
                + "                and jtbn.r_id = jr.id"
                + "                and jtbn.is_lock = 1 and jtbn.state<>5" +
                " left join jecn_user ju" +
                "    on t.res_people_id = ju.people_id" +
                "    and ju.islock = 0" +
                "    and t.type_res_people = 0" +
                "  left join jecn_flow_org_image pos" +
                "    on t.res_people_id = pos.figure_id" +
                "      and t.type_res_people = 1 " +
                "  left join jecn_user ward " +
                "  on jr.ward_people_id = ward.people_id " +
                "  and ward.islock = 0"
                + "               where jr.del_state=0 order by jr.view_sort asc";
        return sql;
    }

    private String getRuleVersionInventorySql(Map<String, Object> map) {
        String sql = "select jr.id,jr.per_id,jr.is_dir"
                + "            ,case when jr.is_dir=2 and jr.is_file_local=0 then jf.file_name else jr.rule_name end as ruleName"
                + "              ,t.rule_number"
                + "               ,t.file_id,jfo.org_id,jfo.org_name," +
                "            case" +
                "         when t.type_res_people = 0 then" +
                "          ju.people_id" +
                "         else" +
                "          pos.figure_id" +
                "       end as res_id," +
                "       case" +
                "         when t.type_res_people = 0 then" +
                "          ju.true_name" +
                "         else" +
                "          pos.figure_text" +
                "       end as res_name," +
                "       t.type_res_people,  t.is_public,t.update_date"
                + "                ,t.id as ispub,jtbn.state,"
                + "			 case when t.id is null and jtbn.state is not null THEN 1 " //审批中
                + "			 when t.id is not null then 2"//发布
                + "			 else 0 end as type_by_data, "//未发布 待建
                + "jt.version_id,jr.t_level,jr.t_path,t.pub_time,t.expiry,t.CONFIDENTIALITY_LEVEL,jr.ward_people_id,ward.true_name ward_people_name,jt.draft_person"
                + "              from jecn_rule_t jr " + "              inner join ("
                + InventorySqlProvider.getRules(map)
                + ") id on id.id=jr.id" + "                left join jecn_rule_history t on t.id=jr.id"
                + "                left join jecn_flow_org jfo on jfo.org_id = t.org_id"
                + "                 left join FILE_CONTENT jf on jf.ID = t.file_id "
                + "                 left join jecn_task_history_new jt on jt.id = t.history_id"
                + "                  left join jecn_task_bean_new jtbn on jtbn.task_type in (2,3)"
                + "                and jtbn.r_id = jr.id"
                + "                and jtbn.is_lock = 1 and jtbn.state<>5" +
                "                     left join jecn_user ju" +
                "    on t.res_people_id = ju.people_id" +
                "    and ju.islock = 0" +
                "    and t.type_res_people = 0" +
                "  left join jecn_flow_org_image pos" +
                "    on t.res_people_id = pos.figure_id" +
                "      and t.type_res_people = 1 "
                + "               where jr.del_state=0 order by jr.view_sort asc,JT.VERSION_ID";
        return sql;
    }

    /**
     * 获取制度模板
     *
     * @param map
     * @return
     */
    public String findRuleTitleByRuleIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        int type = (int) map.get("languageType");
        if (type != 0) {
            return "SELECT T.ID,T.EN_NAME TITLE_NAME,T.TYPE FROM RULE_TITLE" + isPub + " T WHERE T.RULE_ID = #{id} ORDER BY T.ORDER_NUMBER";
        }
        return "SELECT T.ID,T.TITLE_NAME,T.TYPE FROM RULE_TITLE" + isPub + " T WHERE T.RULE_ID = #{id} ORDER BY T.ORDER_NUMBER";
    }

    /**
     * 获取制度模板文控信息
     *
     * @param map
     * @return
     */
    public String findRuleTitleHistoryByRuleIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        int type = (int) map.get("languageType");
        if (type != 0) {
            return "SELECT T.ID,T.EN_NAME TITLE_NAME,T.TYPE FROM RULE_TITLE_HISTORY T WHERE T.HISTORY_ID = #{historyId} ORDER BY T.ORDER_NUMBER";
        }
        return "SELECT T.ID,T.TITLE_NAME,T.TYPE FROM RULE_TITLE_HISTORY T WHERE T.HISTORY_ID = #{historyId} ORDER BY T.ORDER_NUMBER";
    }

    /**
     * 制度操作说明- 制度文件
     *
     * @param map
     * @return
     */
    public String findRuleFileByRuleIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT RT.ID,JF.VERSION_ID FILE_ID, JF.FILE_NAME, JF.DOC_ID" +
                "  FROM RULE_FILE" + isPub + " RF" +
                " INNER JOIN RULE_TITLE" + isPub + " RT" +
                "    ON RT.ID = RF.RULE_TITLE_ID" +
                "  INNER JOIN JECN_FILE" + isPub + " JF" +
                "    ON JF.FILE_ID = RF.RULE_FILE_ID  AND JF.DEL_STATE = 0" +
                " WHERE RT.RULE_ID = #{id}";
        return sql;
    }

    /**
     * 制度操作说明- 制度文件文控信息
     *
     * @param map
     * @return
     */
    public String findRuleFileHistoryByRuleIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        long historyId = (long) map.get("historyId");
        String sql = "SELECT RT.ID, RF.RULE_FILE_ID FILE_ID, JF.FILE_NAME" +
                "  FROM RULE_FILE_HISTORY RF" +
                " INNER JOIN RULE_TITLE_HISTORY RT" +
                "    ON RT.ID = RF.RULE_TITLE_ID AND RF.HISTORY_ID = RT.HISTORY_ID" +
                "  LEFT JOIN FILE_CONTENT JF" +
                "    ON JF.ID = RF.RULE_FILE_ID" +
                " WHERE RT.HISTORY_ID = #{historyId}";
        return sql;
    }

    /**
     * 获取制度模板，title对应的内容
     *
     * @param map
     * @return
     */
    public String findRuleContentByRuleIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT RT.ID, RC.CONTENT FROM RULE_CONTENT" + isPub + " RC" +
                " INNER JOIN RULE_TITLE" + isPub + " RT ON RT.ID = RC.RULE_TITLE_ID" +
                " WHERE RT.RULE_ID = #{id}";
        return sql;
    }

    /**
     * 获取制度模板文控，title对应的内容
     *
     * @param map
     * @return
     */
    public String findRuleContentHistoryByRuleIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        long historyId = (long) map.get("historyId");
        String sql =
                "SELECT RT.ID, RC.CONTENT" +
                        "  FROM RULE_CONTENT_HISTORY RC" +
                        " INNER JOIN RULE_TITLE_HISTORY RT" +
                        "    ON RT.ID = RC.RULE_TITLE_ID" +
                        " WHERE RT.HISTORY_ID = #{historyId}";
        return sql;
    }

    /**
     * 获取制度相关流程、相关架构
     *
     * @param map
     * @return
     */
    public String findRuleRelatedFlowByIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT FS.FLOW_ID, FS.FLOW_NAME, FS.FLOW_ID_INPUT" +
                "  FROM JECN_FLOW_STRUCTURE" + isPub + " FS" +
                "  LEFT JOIN FLOW_RELATED_CRITERION" + isPub + " FRC" +
                "    ON FS.FLOW_ID = FRC.FLOW_ID" +
                " WHERE FRC.CRITERION_CLASS_ID = #{id} AND FS.ISFLOW = #{isFlow} AND FS.DEL_STATE = 0";
        return sql;
    }

    /**
     * 制度操作说明相关标准
     *
     * @param map
     * @return
     */
    public String findRuleRelatedStandardByIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT JCC.CRITERION_CLASS_ID," +
                "       JCC.CRITERION_CLASS_NAME," +
                "       JCC.STAN_TYPE" +
                "  FROM JECN_CRITERION_CLASSES JCC," +
                "       JECN_RULE_STANDARD" + isPub + "   JRS," +
                "       RULE_TITLE" + isPub + "          RT" +
                " WHERE JCC.CRITERION_CLASS_ID = JRS.STANDARD_ID" +
                "   AND JRS.RULE_ID = RT.RULE_ID" +
                "   AND RT.RULE_ID = #{id}" +
                "   AND RT.TYPE = 4" +
                " ORDER BY JRS.ID";
        return sql;
    }

    /**
     * 制度相关风险
     *
     * @param map
     * @return
     */
    public String findRelatedRiskByIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT JR.ID, JR.NAME, JR.RISK_CODE" +
                "  FROM JECN_RISK JR, JECN_RULE_RISK" + isPub + " JRR, RULE_TITLE" + isPub + " RT" +
                " WHERE JR.ID = JRR.RISK_ID" +
                "   AND JRR.RULE_ID = RT.RULE_ID" +
                "   AND RT.RULE_ID = #{id}" +
                "   AND RT.TYPE = 5" +
                " ORDER BY JRR.ID";
        return sql;
    }


    /**
     * 制度相关制度
     *
     * @param map
     * @return
     */
    public String findRelatedRuleByIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        long historyId = (long) map.get("historyId");
        String sql = "SELECT JR.ID, JR.RULE_NAME, JR.RULE_NUMBER" +
                " FROM JECN_RULE" + isPub + " JR, JECN_RULE_RELATED_RULE" + (historyId != 0L ? "_H " : isPub) + " JRR, RULE_TITLE" + (historyId != 0L ? "_HISTORY" : isPub) + "  RT" +
                " WHERE JR.ID = JRR.Related_Id" +
                "  AND JRR.RULE_ID = RT.RULE_ID" +
                " AND RT.RULE_ID = #{id}" +
                " AND RT.TYPE = 6";
        if (historyId != 0L) {
            sql += "  AND RT.HISTORY_ID = #{historyId}     AND JRR.HISTORY_ID =  #{historyId}  ";
        }
        sql += " ORDER BY JRR.RULE_ID";
        return sql;
    }


    public String fetchInventoryParent(Map<String, Object> map) {
        String pub = Utils.getPubByParams(map);
        String sql = "	select p.id,p.per_id,p.is_dir,p.rule_name as ruleName,p.rule_number,p.t_level " +
                "	from JECN_RULE" + pub + " c" +
                "	inner join JECN_RULE" + pub + " p on c.id=#{ruleId} and c.id<>p.id and c.t_path is not null and p.t_path is not null and c.t_path like p.t_path" + BaseSqlProvider.getConcatChar() + "'%'";
        return sql + " where c.del_state=0";
    }

    public String findRelatedStandardizedFilesSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT jf.version_id  id, JF.FILE_NAME name, JF.DOC_ID docId" +
                "  FROM RULE_STANDARDIZED_FILE" + isPub + " RSF" +
                "  LEFT JOIN JECN_FILE" + isPub + " JF" +
                "    ON JF.FILE_ID = RSF.FILE_ID  " +
                "   WHERE RSF.RELATED_ID = #{id}";
        return sql;
    }

    //标准化文件文控表信息
    public String findRelatedStandardizedFilesHistorySql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT JF.ID id, JF.FILE_NAME name" +
                "  FROM STANDARDIZED_FILE_HISTORY RSF" +
                "  LEFT JOIN FILE_CONTENT JF" +
                "    ON JF.ID = RSF.FILE_ID" +
                " WHERE RSF.HISTORY_ID = #{historyId} AND RSF.RELATED_ID = #{id}";
        return sql;
    }

    private static Long getHistoryId(Map<String, Object> map) {
        return Long.valueOf(map.get("historyId").toString());
    }

    public String findRuleBaseInfoSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long historyId = getHistoryId(map);
        String sql = "";
        if (historyId == 0 || historyId == null) {
            sql = "SELECT RULE_ID ruleId,BUSINESS_SCOPE businessScope,OTHER_SCOPE otherScope," +
                    "WRITE_DEPT writeDept,DRAFTMAN draftman,DRAFTING_UNIT draftingUnit,TEST_RUN_FILE testRunFile," +
                    "CHANGE_VERSION_EXPLAIN changeVersionExplain,PURPOSE purpose,APPLICABILITY applicability" +
                    " FROM JECN_RULE_BASIC_INFO" + isPub + " WHERE RULE_ID = #{id}";
        } else {
            sql = findRuleBaseHistoryInfoSql(map);
        }
        return sql;
    }

    public String findRuleBaseHistoryInfoSql(Map<String, Object> map) {
        String sql = "SELECT RULE_ID ruleId,BUSINESS_SCOPE businessScope,OTHER_SCOPE otherScope," +
                "WRITE_DEPT writeDept,DRAFTMAN draftman,DRAFTING_UNIT draftingUnit,TEST_RUN_FILE testRunFile," +
                "CHANGE_VERSION_EXPLAIN changeVersionExplain,PURPOSE purpose,APPLICABILITY applicability,HISTORY_ID historyId " +
                " FROM JECN_RULE_BASIC_INFO_HISTORY WHERE HISTORY_ID = #{historyId} AND RULE_ID = #{id}";
        return sql;
    }

    public String findRuleApplyOrgSql(Map<String, String> map) {
        String isPub = map.get("isPub");
        String sql = "SELECT JFO.ORG_ID id, JFO.ORG_NAME name" +
                "  FROM JECN_FLOW_ORG JFO, JECN_RULE_APPLY_ORG_T T" +
                " WHERE JFO.ORG_ID = T.ORG_ID" +
                "   AND T.RELATED_ID = #{id}" +
                "   AND JFO.DEL_STATE = 0" +
                " ORDER BY JFO.PER_ORG_ID, JFO.SORT_ID, JFO.ORG_ID";
        return sql;
    }

    public String findTaskRuleFileByRuleIdSql(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        String sql = "SELECT JF.VERSION_ID id, JF.FILE_NAME name" +
                "  FROM RULE_FILE" + isPub + " RF" +
                " INNER JOIN RULE_TITLE" + isPub + " RT" +
                "    ON RT.ID = RF.RULE_TITLE_ID" +
                "  INNER JOIN JECN_FILE" + isPub + " JF" +
                "    ON JF.FILE_ID = RF.RULE_FILE_ID" +
                " WHERE RT.RULE_ID = #{id}" +
                " UNION " +
                "SELECT JF.VERSION_ID id, JF.FILE_NAME name" +
                "  FROM RULE_STANDARDIZED_FILE" + isPub + " FSF" +
                " INNER JOIN JECN_FILE" + isPub + " JF" +
                "    ON JF.FILE_ID = FSF.FILE_ID" +
                "   AND JF.del_state = 0" +
                " WHERE FSF.RELATED_ID = #{id}";
        return sql;
    }


    public String getRuleList(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        Long id = map.get("id") == null ? 0L : Long.valueOf(map.get("id").toString());
        String sql = "select g.rule_name pName, t.rule_name name,t.id " +
                "  from jecn_rule" + isPub + " t" +
                " left join jecn_rule" + isPub + " g" +
                "    on g.id = t.per_id" +
                " where t.is_dir = 0    and t.del_state = 0 " +
                "  and t.per_id = " + id;
        return sql;
    }

    public String getChineseEnglishListCount(Map<String, Object> map) {
        String isPub = (String) map.get("isPub");
        StringBuilder str = new StringBuilder();
        List<Long> ids = (List) map.get("ids");
        if (ids != null && !ids.isEmpty()) {
            if (ids.size() <= 100) {
                str.append(" and  t.id in" + SqlCommon.getIds(ids));
            } else {
                str.append(" and  t.id in");
                str.append("(");
                for (int i = 0; i < ids.size(); i++) {
                    if (i > 0) {
                        str.append("union");
                    }
                    str.append("select " + ids.get(i) + " from " + (CommonSqlProvider.isOracle() ? " dual" : ""));
                }
                str.append(")");
            }
        }

        String sql = "select (case" +
                "         when upper(rt.rule_number) like '%EN%' then" +
                "          'EN'" +
                "         else" +
                "          'ZH'" +
                "       end) as TYPE," +
                "       count(rt.id) COUNT," +
                "       t.ID" +
                "  from jecn_rule" + isPub + " t" +
                " inner join jecn_rule" + isPub + " rt " +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("rt", "t") +
                "   and rt.is_dir = 2" +
                "    and rt.del_state = 0" +
                "   and rt.is_file_local is not null" +
                " where t.is_dir = 0 and t.del_state = 0 " + str +
                " group by ((case" +
                "            when upper(rt.rule_number) like '%EN%' then" +
                "             'EN'" +
                "            else" +
                "             'ZH'" +
                "          end))," +
                "          t.id";
        return sql;
    }


    public String getRuleListCount(Map<String, Object> map) {
        String sql = "select count(1) count from jecn_rule where is_dir = 0 and per_id = #{id} and del_state = 0";
        return sql;
    }
}
