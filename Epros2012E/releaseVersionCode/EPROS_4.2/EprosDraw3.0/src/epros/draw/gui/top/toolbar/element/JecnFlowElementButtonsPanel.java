package epros.draw.gui.top.toolbar.element;

import java.awt.GridBagConstraints;

import javax.swing.JButton;
import javax.swing.JLabel;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 
 * 元素属性按钮面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementButtonsPanel extends JecnPanel {
	/** 流程元素面板 */
	private JecnFlowElementPanel flowElementPanel = null;

	/** 恢复默认值 */
	private JButton resetBtn = null;
	/** 确定 */
	private JButton okBtn = null;
	/** 取消 */
	private JButton cancelBtn = null;

	public JecnFlowElementButtonsPanel(JecnFlowElementPanel flowElementPanel) {
		if (flowElementPanel == null) {
			JecnLogConstants.LOG_FIGURE_BUTTONPANEL
					.error("JecnFlowElementButtonsPanel类构造函数：参数为null。flowElementPanel="
							+ flowElementPanel);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.flowElementPanel = flowElementPanel;

		initComponents();
		initBtnPanelLayout();
	}

	private void initComponents() {
		// 恢复默认值
		resetBtn = new JButton(flowElementPanel.getResourceManager().getValue(
				"resetDefault"));
		// 确定
		okBtn = new JButton(flowElementPanel.getResourceManager().getValue(
				"okBtn"));
		// 取消
		cancelBtn = new JButton(flowElementPanel.getResourceManager().getValue(
				"cancelBtn"));

		// 恢复默认值
		resetBtn.addActionListener(flowElementPanel);
		resetBtn.setActionCommand(flowElementPanel.getBTN_RESETBTN());
		// 确定
		okBtn.addActionListener(flowElementPanel);
		okBtn.setActionCommand(flowElementPanel.getBTN_OKBTN());
		// 取消
		cancelBtn.addActionListener(flowElementPanel);
		cancelBtn.setActionCommand(flowElementPanel.getBTN_CANCELTBTN());

		// 大小
		this.setPreferredSize(flowElementPanel.getSizeBottomBtnPanel());
		this.setMinimumSize(flowElementPanel.getSizeBottomBtnPanel());
	}

	/**
	 * 
	 * 按钮区域
	 * 
	 */
	private void initBtnPanelLayout() {

		// 空闲区域
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(new JLabel(), c);
		// 恢复默认值
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0555(), 0, 0);
		this.add(resetBtn, c);

		// 确定
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0555(), 0, 0);
		this.add(okBtn, c);

		// 取消
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0555(), 0, 0);
		this.add(cancelBtn, c);

	}
}
