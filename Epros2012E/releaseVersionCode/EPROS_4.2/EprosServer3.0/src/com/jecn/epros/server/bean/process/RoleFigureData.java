package com.jecn.epros.server.bean.process;

import java.util.ArrayList;
import java.util.List;

public class RoleFigureData implements java.io.Serializable{
	/**等于null代表不更新*/
	private JecnFlowStructureImageT jecnFlowStructureImageT;
	/**角色与岗位关联 增加*/
	private List<JecnFlowStationT> listRolePositionInsert;
	/**角色与岗位关联 删除*/
	private List<String> listRolePositionDelete = new ArrayList<String>();
	public JecnFlowStructureImageT getJecnFlowStructureImageT() {
		return jecnFlowStructureImageT;
	}
	public void setJecnFlowStructureImageT(
			JecnFlowStructureImageT jecnFlowStructureImageT) {
		this.jecnFlowStructureImageT = jecnFlowStructureImageT;
	}
	public List<JecnFlowStationT> getListRolePositionInsert() {
		return listRolePositionInsert;
	}
	public void setListRolePositionInsert(
			List<JecnFlowStationT> listRolePositionInsert) {
		this.listRolePositionInsert = listRolePositionInsert;
	}
	public List<String> getListRolePositionDelete() {
		return listRolePositionDelete;
	}
	public void setListRolePositionDelete(List<String> listRolePositionDelete) {
		this.listRolePositionDelete = listRolePositionDelete;
	}
}
