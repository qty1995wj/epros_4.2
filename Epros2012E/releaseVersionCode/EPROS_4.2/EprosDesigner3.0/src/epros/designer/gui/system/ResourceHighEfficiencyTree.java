package epros.designer.gui.system;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JPopupMenu;
import javax.swing.SwingWorker;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnOpenOrgBean;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.define.EditTermDefineDialog;
import epros.designer.gui.define.JecnTermDefineTreeMenu;
import epros.designer.gui.integration.risk.RiskPropertyDialog;
import epros.designer.gui.integration.risk.RiskResourceTreeMenu;
import epros.designer.gui.popedom.organization.OrgResourceTreeMenu;
import epros.designer.gui.popedom.positiongroup.PosGroupTreeMenu;
import epros.designer.gui.process.FlowResourceTreeMenu;
import epros.designer.gui.process.flow.EditFlowFileDialog;
import epros.designer.gui.process.flow.FlowRoleActiveDialog;
import epros.designer.gui.project.ProjectResourceTreeMenu;
import epros.designer.gui.rule.EditRuleInfoDialog;
import epros.designer.gui.rule.RuleResourceTreeMenu;
import epros.designer.gui.standard.EditStanClauseDialog;
import epros.designer.gui.standard.EditStanClauseRequireDialog;
import epros.designer.gui.standard.StadandardResourceTreeMenu;
import epros.designer.service.process.JecnOrganization;
import epros.designer.service.process.JecnProcess;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.tree.JecnTreeRenderer;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;

/**
 * 
 * 资源管理树
 * 
 * @author Administrator
 * 
 */
public class ResourceHighEfficiencyTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(ResourceHighEfficiencyTree.class);

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new ResourceTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {

		try {
			JecnProject project = ConnectionPool.getProject().getProjectById(JecnConstants.projectId);
			// 根节点
			JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.project, project.getProjectName());
			JecnTreeNode flowRootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.processRoot, JecnProperties
					.getValue("process"));
			JecnTreeNode orgRootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.organizationRoot, JecnProperties
					.getValue("organization"));
			rootNode.add(flowRootNode);
			// 加载组织根节点下的第一层子节点
			if (JecnConfigTool.isHiddenSelectTreePanel()) {
				if (JecnDesignerCommon.isAdmin()) {
					rootNode.add(orgRootNode);
				}
			} else {
				rootNode.add(orgRootNode);
			}
			if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {
				JecnTreeNode stRootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.standardRoot, JecnProperties
						.getValue("ISO_Standard"));// .getValue("standard")
				JecnTreeNode ruleRootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.ruleRoot, JecnProperties
						.getValue("rule"));
				rootNode.add(stRootNode);
				if (!JecnConfigTool.isHiddenRule()) {
					rootNode.add(ruleRootNode);
				}
				JecnTreeNode riskRootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.riskRoot, JecnProperties
						.getValue("risk"));
				rootNode.add(riskRootNode);

				JecnTreeNode termDefineRootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.termDefineRoot,
						JecnProperties.getValue("termDefine"));
				rootNode.add(termDefineRootNode);
			}

			return new JecnTreeModel(rootNode);
		} catch (Exception e) {
			log.error("ResourceHighEfficiencyTree getTreeModel is error", e);
			return null;
		}
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {

		// 击左键
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {

			// 双击
			if (evt.getClickCount() == 2) {
				TreePath[] treePath = this.getSelectionPaths();
				if (treePath != null && treePath.length == 1) {
					JecnTreeNode node = (JecnTreeNode) treePath[0].getLastPathComponent();
					JecnTreeCommon.autoExpandNode(this, node);

					try {
						JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
						switch (jecnTreeBean.getTreeNodeType()) {
						case processMap:// 流程地图
						case process:// 流程图
						case organization:// 组织图
						case standardProcess:// 标准(流程标准)
						case standardProcessMap:// 标准(流程地图标准)
							OpenWorkTask openWorkTask = new OpenWorkTask(node);
							String error = JecnLoading.start(openWorkTask);
							if (!DrawCommon.isNullOrEmtryTrim(error)) {
								JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
										.getValue("openAnomalies"));
							}
							break;
						case processFile:
							if (!JecnDesignerCommon.processOpenValidate(node)) {
								return;
							}

							if (JecnTreeCommon.isDesigner(node) && !JecnTool.isBeInTask(node)) {// 如果是设计者，并且处于任务中退出
								return;
							}
							// 验证是否有操作权限
							if (!JecnTreeCommon.isAuthNodeAddTip(node, null)) {
								return;
							}

							EditFlowFileDialog editFlowFile = new EditFlowFileDialog(node);
							editFlowFile.setVisible(true);
							break;
						case standard:// 标准文件
							if (JecnTreeCommon.isAuthNode(node) == 0) {
								JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
										.getValue("noPopedomOp"));
								return;
							}
							JecnDesignerCommon.openFile(jecnTreeBean.getRelationId());
							break;
						case ruleFile:// 制度文件
							if (JecnTreeCommon.isDesigner(node) && !JecnTool.isBeInTask(node)) {// 如果是设计者，并且处于任务中退出
								return;
							}
							// 验证是否有操作权限
							if (!JecnTreeCommon.isAuthNodeAddTip(node, null)) {
								return;
							}
							RuleT ruleT = ConnectionPool.getRuleAction().getRuleT(jecnTreeBean.getId());
							if (ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 1) {
								JecnDesignerCommon.openRuleFile(ruleT.getFileId());
							} else if (ruleT.getIsFileLocal() != null && ruleT.getIsFileLocal() == 0) {
								JecnDesignerCommon.openFile(jecnTreeBean.getRelationId());
							} else {
								EditRuleInfoDialog editRuleInfoDialog = new EditRuleInfoDialog(node.getJecnTreeBean()
										.getId(), node, false, this);
								editRuleInfoDialog.setVisible(true);
							}
							break;
						case ruleModeFile:// 制度模板文件
							if (JecnTreeCommon.isDesigner(node) && !JecnTool.isBeInTask(node)) {// 如果是设计者，并且处于任务中退出
								return;
							}
							// 验证是否有操作权限
							if (!JecnTreeCommon.isAuthNodeAddTip(node, null)) {
								return;
							}
							EditRuleInfoDialog editRuleInfoDialog = new EditRuleInfoDialog(node.getJecnTreeBean()
									.getId(), node, false, this);
							editRuleInfoDialog.setVisible(true);
							break;
						case riskPoint:// 风险点C

							// 验证是否是管理员或是否具有相关操作权限C
							if (JecnConstants.loginBean.isAdmin() || JecnTreeCommon.isAuthNodeAddTip(node, null)) {
								RiskPropertyDialog riskPropertyDialog = new RiskPropertyDialog(node, this);
								riskPropertyDialog.setVisible(true);
							}
							break;
						case standardClause:
							// 验证是否是管理员或是否具有相关操作权限C
							if (JecnConstants.loginBean.isAdmin() || JecnTreeCommon.isAuthNodeAddTip(node, null)) {
								EditStanClauseDialog d = new EditStanClauseDialog(node, this);
								d.setVisible(true);
							}
							break;
						case standardClauseRequire:
							// 验证是否是管理员或是否具有相关操作权限C
							if (JecnConstants.loginBean.isAdmin() || JecnTreeCommon.isAuthNodeAddTip(node, null)) {
								EditStanClauseRequireDialog editStanClauseRequireDialog = new EditStanClauseRequireDialog(
										node, this);
								editStanClauseRequireDialog.setVisible(true);
							}
							break;
						case termDefine:
							// 验证是否是管理员或是否具有相关操作权限C
							if (JecnConstants.loginBean.isAdmin() || JecnTreeCommon.isAuthNodeAddTip(node, null)) {
								if (node != null) {
									EditTermDefineDialog editTermDefineDialog = new EditTermDefineDialog(node,
											(JecnTree) this);
									editTermDefineDialog.setVisible(true);
								}
							}
							break;
						default:
							break;
						}
						this.revalidate();
						this.repaint();
					} catch (Exception e) {
						log.error("ResourceHighEfficiencyTree jTreeMousePressed is error", e);
						JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
					}
				}
			}

		}
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = this.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick.getLastPathComponent();
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean().getTreeNodeType();
				// 保存去掉不同类型后的节点
				List<JecnTreeNode> selectNodes = new ArrayList<JecnTreeNode>();
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
						if (treeNodeType.equals(nodeSelect.getJecnTreeBean().getTreeNodeType())) {
							selectNodes.add(nodeSelect);
							listTreePaths.add(treePath);
						}
					}
				}

				if (selectNodes.size() > 0) {
					JPopupMenu menu = null;
					if (selectNodes.size() == 1) {
						// 选择了一个节点
						menu = getPopupMenu(selectNodes, 0);

						this.setSelectionPath(listTreePaths.get(0));
						if (menu == null) {
							return;
						}
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
						JecnTreeCommon.autoExpandNode(this, selectNodes.get(0));
						return;
					}
					// 多选择时去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon.repeatChildNodes(selectNodes, this);
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
								if (node.getJecnTreeBean().getId().equals(nodeSelect.getJecnTreeBean().getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}
						if (treePaths.length == 1) {
							menu = getPopupMenu(selectNodes, 0);
							JecnTreeCommon.autoExpandNode(this, selectNodes.get(0));
							this.setSelectionPath(treePaths[0]);
						} else {
							JecnDesignerCommon.sortJecnTreeBeanList(selectNodes);
							menu = getPopupMenu(selectNodes, 1);
							this.setSelectionPaths(treePaths);
						}
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
					}
				}

			}
		}

	}

	private JPopupMenu getPopupMenu(List<JecnTreeNode> selectNodes, int selectMutil) {
		JPopupMenu menu = null;
		switch (selectNodes.get(0).getJecnTreeBean().getTreeNodeType()) {
		case project:
			menu = new ProjectResourceTreeMenu(this, selectNodes.get(0));
			break;
		case processRoot:
		case processMap:
		case process:
		case processFile:
			if (selectNodes.size() == 1
					&& !"processRoot".equals(selectNodes.get(0).getJecnTreeBean().getTreeNodeType().toString())) {
				if (JecnDesignerCommon.isNotAbolishOrNotDelete(selectNodes.get(0).getJecnTreeBean().getId(), 0)) {
					if (selectNodes.get(0).getJecnTreeBean().getApproveType() != 2) {
						menu = new FlowResourceTreeMenu(this, selectNodes, selectMutil);
						break;
					}
				}
			} else {
				menu = new FlowResourceTreeMenu(this, selectNodes, selectMutil);
			}
			break;
		case organizationRoot:// 组织根节点
		case organization: // 组织
		case position: // 岗位
			if (!JecnDesignerCommon.isAdmin()) {
				break;
			}
			menu = new OrgResourceTreeMenu(this, selectNodes, selectMutil);
			break;
		case positionGroupRoot:// 岗位组根节点
		case positionGroupDir:// 岗位组目录
		case positionGroup:// 岗位组角色
			menu = new PosGroupTreeMenu(this, selectNodes, selectMutil);
			break;
		case standardRoot: // 标准根节点
		case standardDir: // 标准目录
		case standard: // 标准
		case standardProcess:
		case standardProcessMap:
		case standardClause:
		case standardClauseRequire:
			menu = new StadandardResourceTreeMenu(this, selectNodes, selectMutil);
			break;
		case ruleRoot: // 制度根节点
		case ruleDir: // 制度目录
		case ruleFile: // 制度文件
		case ruleModeFile:// 制度模板文件
			if (selectNodes.size() == 1
					&& !"ruleRoot".equals(selectNodes.get(0).getJecnTreeBean().getTreeNodeType().toString())) {
				if (JecnDesignerCommon.isNotAbolishOrNotDelete(selectNodes.get(0).getJecnTreeBean().getId(), 2)) {
					if (selectNodes.get(0).getJecnTreeBean().getApproveType() != 2) {
						menu = new RuleResourceTreeMenu(this, selectNodes, selectMutil);
					}
				}
			} else {
				menu = new RuleResourceTreeMenu(this, selectNodes, selectMutil);

			}
			break;
		case riskRoot: // 风险根节点
		case riskDir: // 风险目录
		case riskPoint: // 风险点
			menu = new RiskResourceTreeMenu(this, selectNodes, selectMutil);
			break;
		case termDefine: // 风险根节点
		case termDefineDir: // 风险目录
		case termDefineRoot: // 风险点
			if (selectNodes.size() == 1) {
				menu = new JecnTermDefineTreeMenu((JecnTree) this, selectNodes.get(0));
			} else {
				menu = new JecnTermDefineTreeMenu((JecnTree) this, selectNodes, selectNodes.get(0).getJecnTreeBean()
						.getTreeNodeType());
			}
			break;
		default:
			break;
		}
		return menu;
	}

	class OpenWorkTask extends SwingWorker<Void, Void> {
		private JecnTreeNode selectNode;

		public OpenWorkTask(JecnTreeNode selectNode) {
			this.selectNode = selectNode;
		}

		@Override
		protected Void doInBackground() throws Exception {
			try {
				switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
				case process:
					openProcess(selectNode);
					break;
				case processMap:
					openProcessMap(selectNode);
					break;
				case organization:
					openOrgMap(selectNode);
					break;
				case standardProcess:
					openStandardProcess(selectNode);
					break;
				case standardProcessMap:
					openStandardProcessMap(selectNode);
					break;
				default:
					break;
				}
			} catch (Exception e) {
				log.error("ResourceHighEfficiencyTree OpenWorkTask is error", e);
				done();
				JecnLoading.setError("1");
			}
			return null;
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	/**
	 * @author yxw 2012-12-17
	 * @description:打开流程图
	 * @param node
	 *            要打开的节点
	 * @throws Exception
	 */
	private void openProcess(JecnTreeNode node) throws Exception {
		try {
			JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
			// 验证面板是否被占用 (是否被打开，是否任务中，是否被编辑中)
			if (!JecnDesignerCommon.processOpenValidateIsOpen(node)) {
				return;
			}
			try {
				int status = JecnDesignerCommon.getSaveAuthStatus(jecnTreeBean.getTreeNodeType(), jecnTreeBean.getId());
				if (status == 0) {
					return;
				}
				ProcessOpenData processOpenData = ConnectionPool.getProcessAction().getProcessData(
						jecnTreeBean.getId(), JecnConstants.projectId);
				processOpenData.setIsAuthSave(status);
				if (status == 5) {
					processOpenData.setOccupierName(JecnDesignerCommon.getOccupierName(jecnTreeBean.getTreeNodeType(),
							jecnTreeBean.getId()));
				}
				JecnProcess.openJecnProcessMap(processOpenData, false);
			} catch (Exception e) {
				log.error("JecnDesignerCommon processOpenValidate is error", e);
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			}
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (desktopPane == null) {
				return;
			}
			if (!jecnTreeBean.isPub()) {
				desktopPane.getTabbedTitlePanel().setTabIcon(
						(Icon) JecnTreeRenderer.class.getField(jecnTreeBean.getTreeNodeType().toString() + "NoPub")
								.get(null));
			}
			// 如果面板中没有任何元素，弹出重置模板
			boolean flag = false;
			for (JecnBaseFlowElementPanel baseFlowElementPanel : desktopPane.getPanelList()) {
				if (baseFlowElementPanel instanceof JecnBaseFigurePanel) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				FlowRoleActiveDialog d = new FlowRoleActiveDialog();
				d.setVisible(true);
			}
		} catch (Exception e) {
			log.error("ResourceHighEfficiencyTree openProcess is error！", e);
			JecnLoading.setError("1");
		}
	}

	/**
	 * @author yxw 2012-12-17
	 * @description:打开流程地图
	 * @param node
	 *            要打开的节点
	 * @throws Exception
	 */
	private void openProcessMap(JecnTreeNode node) throws Exception {
		try {
			JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
			// 验证面板是否被占用
			if (!JecnDesignerCommon.processOpenValidateIsOpen(node)) {
				return;
			}

			try {
				int status = JecnDesignerCommon.getSaveAuthStatus(jecnTreeBean.getTreeNodeType(), jecnTreeBean.getId());
				if (status == 0) {
					return;
				}
				ProcessOpenMapData openMapData = ConnectionPool.getProcessAction().getProcessMapData(
						jecnTreeBean.getId(), JecnConstants.projectId);
				openMapData.setIsAuthSave(status);
				if (status == 5) {
					openMapData.setOccupierName(JecnDesignerCommon.getOccupierName(jecnTreeBean.getTreeNodeType(),
							jecnTreeBean.getId()));
				}
				JecnProcess.openJecnProcessTotalMap(openMapData, false);

			} catch (Exception e) {
				log.error("JecnDesignerCommon processOpenValidate is error", e);
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("serverConnException"));
			}

			if (!jecnTreeBean.isPub()) {// && JecnConstants.isPubShow()
				JecnDrawMainPanel.getMainPanel().getWorkflow().getTabbedTitlePanel().setTabIcon(
						(Icon) JecnTreeRenderer.class.getField(jecnTreeBean.getTreeNodeType().toString() + "NoPub")
								.get(null));
			}
		} catch (Exception e) {
			log.error("ResourceHighEfficiencyTree openProcessMap is error！", e);
			JecnLoading.setError("1");
		}
	}

	/**
	 * @author yxw 2012-12-17
	 * @description:打开组织图
	 * @param node
	 *            要打开的节点
	 * @throws Exception
	 */
	private void openOrgMap(JecnTreeNode node) throws Exception {
		try {
			if (!JecnDesignerCommon.isAdmin()) {// 只有系统管理员可操作组织节点
				JecnLoading.stop();
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("noPopedomOp"));
				return;
			}
			JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
			if (!JecnDesignerCommon.processOpenValidate(node)) {
				return;
			}
			JecnOpenOrgBean orgOpenData = ConnectionPool.getOrganizationAction().openOrgMap(jecnTreeBean.getId());
			JecnOrganization.openOrganizationMap(orgOpenData);
		} catch (Exception e) {
			log.error("ResourceHighEfficiencyTree openOrgMap is error！", e);
			JecnLoading.setError("1");
		}
	}

	/**
	 * @author yxw 2012-12-17
	 * @description:打开标准关联的流程图
	 * @param node
	 *            要打开的节点
	 * @throws Exception
	 */
	private void openStandardProcess(JecnTreeNode node) throws Exception {
		try {
			JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
			JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(
					jecnTreeBean.getRelationId());
			if (flowStructureT == null) {
				return;
			}
			JecnTreeNode reNode = JecnTreeCommon.searchNode(flowStructureT.getFlowId(), TreeNodeType.process, this);
			if (reNode == null) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("noPopedomOp"));
				return;
			}
			// 判断是否已打开
			if (!JecnDesignerCommon.processOpenValidate(reNode)) {
				return;
			}
			ProcessOpenData processOpenData = ConnectionPool.getProcessAction().getProcessData(
					jecnTreeBean.getRelationId(), JecnConstants.projectId);
			JecnProcess.openJecnProcessMap(processOpenData, false);
		} catch (Exception e) {
			log.error("ResourceHighEfficiencyTree openStandardProcess is error！", e);
			JecnLoading.setError("1");
		} finally {
			this.validate();
			this.repaint();

		}
	}

	/**
	 * @author yxw 2012-12-17
	 * @description:打开标准关联的流程地图
	 * @param node
	 *            要打开的节点
	 * @throws Exception
	 */
	private void openStandardProcessMap(JecnTreeNode node) throws Exception {
		try {
			JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
			JecnFlowStructureT flowStructure = ConnectionPool.getProcessAction().getJecnFlowStructureT(
					jecnTreeBean.getRelationId());
			if (flowStructure == null) {
				return;
			}
			JecnTreeCommon.searchNode(flowStructure.getFlowId(), TreeNodeType.processMap, this);
			String nodeUniqueIdentifier = JecnTreeCommon.getNodeUniqueIdentifier(jecnTreeBean.getRelationId(),
					TreeNodeType.processMap);
			JecnTreeNode processMapNode = JecnTreeCommon.getTreeNode(nodeUniqueIdentifier, this);
			if (processMapNode == null) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("noPopedomOp"));
				return;
			}
			// 判断是否已打开
			if (!JecnDesignerCommon.processOpenValidate(processMapNode)) {
				return;
			}
			ProcessOpenMapData processMapData = ConnectionPool.getProcessAction().getProcessMapData(
					jecnTreeBean.getRelationId(), JecnConstants.projectId);
			JecnProcess.openJecnProcessTotalMap(processMapData, false);
		} catch (Exception e) {
			log.error("ResourceHighEfficiencyTree openStandardProcessMap is error！", e);
			JecnLoading.setError("1");
		} finally {
			this.validate();
			this.repaint();
		}
	}
}
