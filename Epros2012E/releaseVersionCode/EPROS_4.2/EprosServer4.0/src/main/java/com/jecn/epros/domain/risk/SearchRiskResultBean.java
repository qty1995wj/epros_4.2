package com.jecn.epros.domain.risk;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.util.ConfigUtils;

public class SearchRiskResultBean {
    private Long id;
    private String name;
    private String riskCode;
    /**
     * 1: 高   2:中 3:低
     */
    private int grade;

    public String getGradeStr(){
        return ConfigUtils.getGradeStrByType(grade);
    }

    public LinkResource getLink() {
        return new LinkResource(id, riskCode, ResourceType.RISK, TreeNodeUtils.NodeType.search);
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public String getRiskCode() {
        return riskCode;
    }

    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
