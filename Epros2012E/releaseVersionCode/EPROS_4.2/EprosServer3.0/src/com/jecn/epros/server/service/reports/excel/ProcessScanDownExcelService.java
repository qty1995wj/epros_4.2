package com.jecn.epros.server.service.reports.excel;

import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.block.BorderArrangement;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.title.CompositeTitle;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleEdge;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.reports.ProcessScanOptimizeBean;
import com.jecn.epros.server.webBean.reports.ProcessScanWebBean;
import com.jecn.epros.server.webBean.reports.ProcessScanWebListBean;

/**
 * 
 * 流程审视优化统计表
 * 
 * @author huoyl
 * 
 */
public class ProcessScanDownExcelService {

	private Log log = LogFactory.getLog(this.getClass());
	/** 流程审视优化统计数据源bean */
	private ProcessScanOptimizeBean processScanOptimizeBean;
	// 0流程 2制度
	private int type = 0;

	public ProcessScanOptimizeBean getProcessScanOptimizeBean() {
		return processScanOptimizeBean;
	}

	public void setProcessScanOptimizeBean(ProcessScanOptimizeBean processScanOptimizeBean) {
		this.processScanOptimizeBean = processScanOptimizeBean;
		this.type = processScanOptimizeBean.getType();
	}

	public byte[] getExcelFile() throws Exception {

		byte[] tempDate = null;
		if (processScanOptimizeBean == null) {
			throw new Exception("ProcessScanDownExcelService类processScanOptimizeBean为空");
		}

		if (0 == processScanOptimizeBean.getAllScanWebListBeans().size()) {
			throw new Exception("ProcessScanDownExcelService类processScanOptimizeBean.getAllScanWebListBeans().size()为0");
		}
		// 设置流程审视优化统计表的sheetname 处理部门或者流程架构的相同名称这种情况
		setProcessScanSheetName(processScanOptimizeBean.getAllScanWebListBeans());

		WritableWorkbook wbookData = null;
		File file = null;
		List<File> imageFiles = new ArrayList<File>();
		// 流程监护人与流程监护人部门是否显示
		boolean guardianIsShow = processScanOptimizeBean.isGuardianIsShow();
		List<ProcessScanWebListBean> allScanWebListBeans = processScanOptimizeBean.getAllScanWebListBeans();
		try {
			int curSheet = 0;
			int imageHeight = 400;
			int imageWidth = allScanWebListBeans.size() * 40 + 400;
			int row = 0;
			int column = 0;
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path + JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);

			// 开始写入第一页
			WritableSheet wsheet = wbookData.createSheet(JecnUtil.getValue("statistics"), curSheet);
			WritableCellFormat firstSheetHeaderStyle = getFirstSheetHeaderStyle();
			// 第一行
			wsheet.setColumnView(0, 18);// 列宽
			wsheet.addCell(new Label(0, 0, type == 0 ? JecnUtil.getValue("processManagementMonthReport") : "制度管理月报",
					firstSheetHeaderStyle));
			// 获得年月
			String endTime = processScanOptimizeBean.getEndTime();
			wsheet.setColumnView(1, 13);// 列宽
			wsheet.addCell(new Label(1, row, endTime, firstSheetHeaderStyle));
			wsheet.setColumnView(2, 14);// 列宽
			wsheet.addCell(new Label(2, row, JecnUtil.getValue("period"), firstSheetHeaderStyle));
			wsheet.setColumnView(3, 45);// 列宽
			wsheet.addCell(new Label(3, row, "", firstSheetHeaderStyle));
			row++;

			// 第二行
			// 合并单元格
			wsheet.mergeCells(0, 1, 3, 1);
			wsheet.setRowView(row, 480); // 24*20
			wsheet.addCell(new Label(0, row, getTitle(), getFirstSheetSecondStyle()));
			row++;
			// 第三行
			WritableCellFormat firstSheetThirdStyle = getFirstSheetThirdStyle();
			// 调整行高
			wsheet.setRowView(row, 500);
			wsheet.addCell(new Label(0, row, JecnUtil.getValue("org"), firstSheetThirdStyle));
			wsheet.addCell(new Label(1, row, type == 0 ? JecnUtil.getValue("needScanProcessCount") : "需审视制度数量",
					firstSheetThirdStyle));
			wsheet.addCell(new Label(2, row, JecnUtil.getValue("onTimeScanProcessCount"), firstSheetThirdStyle));
			wsheet.addCell(new Label(3, row, JecnUtil.getValue("scanOptimizeFinishPersent"), firstSheetThirdStyle));
			row++;
			WritableCellFormat firstSheetContentStyle = getFirstSheetContentStyle();
			String scanPercent = "";
			// 所有部门或者流程架构需审视的流程的数量
			int totalAllScanCount = 0;
			// 所有的部门或者流程架构按时审视完成的数量
			int totalScanCount = 0;
			for (ProcessScanWebListBean sacnBean : allScanWebListBeans) {
				totalAllScanCount += sacnBean.getAllScanCount();
				totalScanCount += sacnBean.getScanCount();
				column = 0;
				wsheet.addCell(new Label(column++, row, sacnBean.getSheetName(), firstSheetContentStyle));
				wsheet.addCell(new Label(column++, row, String.valueOf(sacnBean.getAllScanCount()),
						firstSheetContentStyle));
				wsheet
						.addCell(new Label(column++, row, String.valueOf(sacnBean.getScanCount()),
								firstSheetContentStyle));
				// 覆盖率从零点几转换为百分比
				scanPercent = String.valueOf(sacnBean.getScanPercent()) + "%";
				wsheet.addCell(new Label(column++, row, String.valueOf(scanPercent), firstSheetContentStyle));
				row++;
			}
			column = 0;
			// 总数
			wsheet.addCell(new Label(column++, row, JecnUtil.getValue("totalStr"), firstSheetContentStyle));
			// 需要审视的数量总数
			wsheet.addCell(new Label(column++, row, String.valueOf(totalAllScanCount), firstSheetContentStyle));
			// 按时审视的总数
			wsheet.addCell(new Label(column++, row, String.valueOf(totalScanCount), firstSheetContentStyle));
			String scanAllPercent = "0%";
			// 百分比
			if (totalAllScanCount > 0) {
				scanAllPercent = String.valueOf(totalScanCount * 100 / totalAllScanCount) + "%";
			}
			wsheet.addCell(new Label(column++, row, scanAllPercent, firstSheetContentStyle));

			row++;
			row++;

			// 添加图片
			JFreeChart localJFreeChart = getScanPercentChart();
			String imagePath = jfreeChartToPng(localJFreeChart, imageWidth, imageHeight);
			File pictureFile = new File(imagePath);
			imageFiles.add(pictureFile);
			JecnUtil.addPictureToExcel(wsheet, pictureFile, row, 0);

			// 图片高度相当于 24行
			row = row + 24;

			// 2014年6-8月 需完成审视优化的流程分布
			// 合并单元格
			wsheet.mergeCells(0, row, 3, row);
			wsheet.setRowView(row, 480);
			wsheet.addCell(new Label(0, row, getSecondTitle(), getFirstSheetSecondStyle()));
			row++;
			// 表头
			wsheet.mergeCells(0, row, 2, row);
			wsheet.addCell(new Label(0, row, JecnUtil.getValue("org"), firstSheetThirdStyle));
			wsheet.addCell(new Label(3, row, type == 0 ? JecnUtil.getValue("needScanProcessCount") : "需审视制度数量",
					firstSheetThirdStyle));
			row++;
			// 内容
			for (ProcessScanWebListBean scanBean : allScanWebListBeans) {
				// 合并1到3
				wsheet.mergeCells(0, row, 2, row);
				wsheet.addCell(new Label(0, row, scanBean.getSheetName(), firstSheetContentStyle));
				wsheet.addCell(new Label(3, row, String.valueOf(scanBean.getNextTimeScanCount()),
						firstSheetContentStyle));
				row++;
			}
			row++;
			// 生成图片
			localJFreeChart = getNextScanCountChart();
			imagePath = jfreeChartToPng(localJFreeChart, imageWidth, imageHeight);
			pictureFile = new File(imagePath);
			imageFiles.add(pictureFile);
			JecnUtil.addPictureToExcel(wsheet, pictureFile, row, 0);
			curSheet++;

			// 写出第二与之后的页
			// 表头的内容

			// 需完成的月份（计划）
			String needFinishMonth = JecnUtil.getValue("needFinishMonth");
			// 需审视流程
			String needScanProcess = type == 0 ? JecnUtil.getValue("needScanProcess") : "需审视制度";
			// 责任部门
			String responsibilityDepartment = JecnUtil.getValue("responsibilityDepartment");
			// 流程责任人
			String processResponsiblePersons = "责任人";
			// 流程监护人
			String processTheGuardian = "监护人";
			// 拟稿人
			String peopleMake = JecnUtil.getValue("peopleMake");
			// 版本号
			String versionNumber = JecnUtil.getValue("versionNumber");
			// 需审视流程发布时间
			String needScanProcessPubTime = type == 0 ? JecnUtil.getValue("needScanProcessPubTime") : "需审视制度发布时间";
			// 流程监护人所属部门
			String processGuardianOrg = "监护人所属部门";
			// 当前发布时间
			String curPubTime = JecnUtil.getValue("curPubTime");
			// 是否及时优化
			String scanOnTime = JecnUtil.getValue("theTimelyOptimization");
			// 是
			String yes = JecnUtil.getValue("is");
			// 否
			String no = JecnUtil.getValue("no");

			// 第二页与之后的表头
			WritableCellFormat secondSheetFirstStyle = getSecondSheetFirstStyle();
			// 流程名称的样式
			WritableCellFormat flowNameStyle = getFlowNameStyle();
			// 当前发布时间的样式
			WritableCellFormat curPubDateStyle = getSecondSheetFirstStyle();
			curPubDateStyle.setBackground(Colour.ROSE);
			WritableCellFormat secondSheetContentStyle = getSecondSheetContentStyle();
			WritableCellFormat modifySheetContentStyle = getModifySheetContentStyle();

			for (ProcessScanWebListBean bean : allScanWebListBeans) {
				row = 0;
				column = 0;

				wsheet = wbookData.createSheet(bean.getSheetName(), curSheet);

				wsheet.setRowView(column, 480);
				// 写出表头
				wsheet.setColumnView(column, 14);
				wsheet.addCell(new Label(column++, row, needFinishMonth, secondSheetFirstStyle));

				wsheet.setColumnView(column, 42);
				wsheet.addCell(new Label(column++, row, needScanProcess, flowNameStyle));

				wsheet.setColumnView(column, 8);
				wsheet.addCell(new Label(column++, row, versionNumber, secondSheetFirstStyle));

				wsheet.setColumnView(column, 14);
				wsheet.addCell(new Label(column++, row, needScanProcessPubTime, secondSheetFirstStyle));

				wsheet.setColumnView(column, 14);
				wsheet.addCell(new Label(column++, row, peopleMake, secondSheetFirstStyle));

				wsheet.setColumnView(column, 14);
				wsheet.addCell(new Label(column++, row, responsibilityDepartment, secondSheetFirstStyle));

				wsheet.setColumnView(column, 21);
				wsheet.addCell(new Label(column++, row, processResponsiblePersons, secondSheetFirstStyle));
				if (guardianIsShow) {
					wsheet.setColumnView(column, 14);
					wsheet.addCell(new Label(column++, row, processGuardianOrg, secondSheetFirstStyle));

					wsheet.setColumnView(column, 14);
					wsheet.addCell(new Label(column++, row, processTheGuardian, secondSheetFirstStyle));
				}

				wsheet.setColumnView(column, 15);
				wsheet.addCell(new Label(column++, row, curPubTime, curPubDateStyle));
				// 是否及时优化
				wsheet.setColumnView(column, 15);
				wsheet.addCell(new Label(column++, row, scanOnTime, curPubDateStyle));

				wsheet.setColumnView(column, 60);
				wsheet.addCell(new Label(column++, row, "变更说明", curPubDateStyle));

				column = 0;
				row++;
				// 写出内容
				for (ProcessScanWebBean webBean : bean.getAllScanWebBeans()) {
					wsheet.setRowView(row, 360);
					// 写出内容
					wsheet.addCell(new Label(column++, row, webBean.getNeedFinishDate(), secondSheetContentStyle));
					wsheet.addCell(new Label(column++, row, webBean.getFlowName(), secondSheetContentStyle));
					wsheet.addCell(new Label(column++, row, webBean.getVersionId(), secondSheetContentStyle));
					wsheet.addCell(new Label(column++, row, webBean.getNeedScanPubDate(), secondSheetContentStyle));
					wsheet.addCell(new Label(column++, row, webBean.getDraftPerson(), secondSheetContentStyle));
					wsheet.addCell(new Label(column++, row, webBean.getTrueOrgName(), secondSheetContentStyle));
					wsheet.addCell(new Label(column++, row, webBean.getResPeopleName(), secondSheetContentStyle));
					if (guardianIsShow) {
						wsheet.addCell(new Label(column++, row, webBean.getGuardianOrgName(), secondSheetContentStyle));
						wsheet.addCell(new Label(column++, row, webBean.getGuardianName(), secondSheetContentStyle));

					}
					wsheet.addCell(new Label(column++, row, webBean.getCurScanPubDate(), secondSheetContentStyle));
					// 是否优化 1.为是 0.为否 .2 为空
					if (1 == webBean.getOptimization()) {
						// 是
						wsheet.addCell(new Label(column++, row, yes, secondSheetContentStyle));
					} else if (2 == webBean.getOptimization()) {
						// 空
						wsheet.addCell(new Label(column++, row, "", secondSheetContentStyle));
					} else {
						// 否
						wsheet.addCell(new Label(column++, row, no, secondSheetContentStyle));
					}
					wsheet.addCell(new Label(column++, row, webBean.getModifyExplain(), modifySheetContentStyle));

					row++;
					column = 0;

				}
				curSheet++;

			}

			// 后续处理--------------------------------------

			wbookData.write();
			// 工作簿不关闭不会写出数据
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;
			// 把一个文件转化为字节
			tempDate = JecnUtil.getByte(file);

		} catch (Exception e) {
			throw new Exception("ProcessScanDownExcelService类getExcelFile方法异常", e);
		} finally {
			if (wbookData != null) {
				try {
					wbookData.close();

				} catch (Exception e) {
					throw new Exception("ProcessScanDownExcelService类关闭excel工作表异常", e);
				}
			}
			// 删除临时文件临时图片
			if (file != null && file.exists()) {
				file.delete();
			}
			for (File tempFile : imageFiles) {
				if (tempFile.exists()) {
					tempFile.delete();
				}
			}
		}

		return tempDate;
	}

	/**
	 * 设置流程审视优化统计表的sheet名称（防止重复）
	 * 
	 * @param allScanWebListBeans
	 */
	private void setProcessScanSheetName(List<ProcessScanWebListBean> allScanWebListBeans) {

		Map<String, Integer> sheetNames = new HashMap<String, Integer>();
		for (ProcessScanWebListBean bean : allScanWebListBeans) {
			String sheetName = bean.getName();
			if (!sheetNames.keySet().contains(sheetName)) {
				sheetNames.put(bean.getName(), 0);
			} else {// 如果sheet的名称重复那么累加1
				sheetNames.put(sheetName, sheetNames.get(sheetName) + 1);
				sheetName = sheetName + "(" + sheetNames.get(sheetName) + ")";
			}
			bean.setSheetName(sheetName);
		}

	}

	// 第二个sheet表头样式
	private WritableCellFormat getSecondSheetFirstStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.YELLOW);
		// 单元格的线
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);
		return tempCellFormat;
	}

	// 第二个sheet内容样式
	private WritableCellFormat getSecondSheetContentStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.NO_BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		// 单元格的线
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		return tempCellFormat;
	}

	private WritableCellFormat getModifySheetContentStyle() throws WriteException {
		WritableCellFormat contentStyle = getSecondSheetContentStyle();
		contentStyle.setWrap(true);
		contentStyle.setAlignment(Alignment.LEFT);
		return contentStyle;
	}

	// 第二个sheet内容中的流程名称的样式
	private WritableCellFormat getFlowNameStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		// 单元格的线
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setBackground(Colour.LIGHT_ORANGE);
		return tempCellFormat;
	}

	private CategoryDataset getScanPercentDataset() {

		DefaultCategoryDataset localDefaultCategoryDataset = new DefaultCategoryDataset();
		String processScanFinishPercent = type == 0 ? JecnUtil.getValue("processScanFinishPercent") : "制度审视优化及时完成率";
		for (ProcessScanWebListBean sacnBean : processScanOptimizeBean.getAllScanWebListBeans()) {
			localDefaultCategoryDataset.addValue(sacnBean.getScanPercent(), processScanFinishPercent, sacnBean
					.getName());
		}
		return localDefaultCategoryDataset;
	}

	/**
	 * 生成流程审批优化及时完成率图片的JFreeChart
	 * 
	 * @return
	 */
	private JFreeChart getScanPercentChart() {
		// TODO Auto-generated method stub
		JFreeChart localJFreeChart = ChartFactory.createBarChart(getTitle(), "", JecnUtil.getValue("unitPercent"),
				getScanPercentDataset(), PlotOrientation.VERTICAL, false, false, false);
		CategoryPlot localCategoryPlot = (CategoryPlot) localJFreeChart.getPlot();
		NumberAxis localNumberAxis = (NumberAxis) localCategoryPlot.getRangeAxis();
		localNumberAxis.setLabelFont(new Font("宋体", Font.BOLD, 12));
		// 设置刻度长度自动变化
		localNumberAxis.setAutoTickUnitSelection(false);
		double unit = 10d;// 刻度的长度
		NumberTickUnit ntu = new NumberTickUnit(unit);
		localNumberAxis.setTickUnit(ntu);

		localNumberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		localNumberAxis.setUpperBound(110);
		CategoryItemRenderer localCategoryItemRenderer = localCategoryPlot.getRenderer();
		localCategoryItemRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		// 柱状图的标签显示项
		localCategoryItemRenderer.setSeriesItemLabelsVisible(0, Boolean.TRUE);

		// 柱状图的标签的单位
		localCategoryItemRenderer.setBaseItemLabelsVisible(true);
		// 柱状图的宽度
		((BarRenderer) localCategoryItemRenderer).setMaximumBarWidth(0.1);
		((BarRenderer) localCategoryItemRenderer).setMinimumBarLength(0.1);
		((BarRenderer) localCategoryItemRenderer).setItemMargin(0.05);

		CategoryAxis localCategoryAxis = localCategoryPlot.getDomainAxis();
		localCategoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		localCategoryAxis.setTickLabelFont(new Font("宋体", Font.BOLD, 12));

		// 描述的显示
		// 折线的描述（就是描述各种类型的线或者图）
		LegendTitle legend = new LegendTitle(localCategoryPlot);
		legend.setPosition(RectangleEdge.LEFT);
		BlockContainer blockcontainer = new BlockContainer(new BorderArrangement());
		blockcontainer.add(legend, RectangleEdge.LEFT);// 这行代码可以控制位置
		CompositeTitle compositetitle = new CompositeTitle(blockcontainer);
		compositetitle.setPosition(RectangleEdge.RIGHT);// 放置图形代表区域位置的代码
		localJFreeChart.addSubtitle(compositetitle);

		return localJFreeChart;

	}

	private DefaultCategoryDataset getNextScanCountData() {
		DefaultCategoryDataset localDefaultCategoryDataset = new DefaultCategoryDataset();
		String processCount = type == 0 ? JecnUtil.getValue("processCount") : "制度数量";
		int count = 0;
		int nextScanCount = 0;
		for (ProcessScanWebListBean scanBean : processScanOptimizeBean.getAllScanWebListBeans()) {
			nextScanCount = scanBean.getNextTimeScanCount();
			count = count + nextScanCount;
			localDefaultCategoryDataset.addValue(nextScanCount, processCount, scanBean.getSheetName());
		}
		localDefaultCategoryDataset.addValue(count, processCount, "TOTAL");
		return localDefaultCategoryDataset;

	}

	/**
	 * 下两个月需要完成的流程分布图片的JFreeChart
	 * 
	 * @return
	 * @throws ParseException
	 */
	private JFreeChart getNextScanCountChart() throws ParseException {

		int maxCount = 0;
		// 最大值
		int upBound = 0;
		// 刻度间隔
		int leftUnit = 0;

		for (ProcessScanWebListBean bean : processScanOptimizeBean.getAllScanWebListBeans()) {
			int scanCount = bean.getNextTimeScanCount();
			maxCount = maxCount + scanCount;
		}

		if (maxCount < 10) {
			maxCount = 10;
		}
		upBound = (int) (maxCount * 1.1);
		leftUnit = upBound / 11;
		JFreeChart localJFreeChart = ChartFactory.createBarChart(getSecondTitle(), "", "", getNextScanCountData(),
				PlotOrientation.VERTICAL, false, false, false);

		CategoryPlot localCategoryPlot = (CategoryPlot) localJFreeChart.getPlot();

		localCategoryPlot.mapDatasetToRangeAxis(1, 1);

		// 水平
		CategoryAxis localCategoryAxis = localCategoryPlot.getDomainAxis();
		localCategoryAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
		localCategoryAxis.setTickLabelFont(new Font("宋体", Font.BOLD, 12));
		// 竖直
		NumberAxis leftNumberAxis = new NumberAxis(JecnUtil.getValue("unitOne"));
		leftNumberAxis.setUpperBound(upBound);
		leftNumberAxis.setLabelFont(new Font("宋体", Font.BOLD, 12));

		// 设置竖直刻度长度
		leftNumberAxis.setAutoTickUnitSelection(false);

		NumberTickUnit leftNtu = new NumberTickUnit(leftUnit);
		leftNumberAxis.setTickUnit(leftNtu);

		localCategoryPlot.setRangeAxis(0, leftNumberAxis);

		// 柱状图数字显示
		BarRenderer localBarRenderer = (BarRenderer) localCategoryPlot.getRenderer();
		localBarRenderer.setBaseItemLabelFont(new Font("宋体", Font.BOLD, 12));
		// 柱状图标签距离柱状图的距离
		localBarRenderer.setItemLabelAnchorOffset(0.0D);
		// 标签是否显示
		localBarRenderer.setBaseItemLabelsVisible(true);
		localBarRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		localBarRenderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());

		// localBarRenderer.setItemMargin(1);
		// 柱状图的宽度
		localBarRenderer.setMaximumBarWidth(0.05);
		localBarRenderer.setMinimumBarLength(0.05);

		// 折线的描述（就是描述各种类型的线或者图）
		LegendTitle legend = new LegendTitle(localCategoryPlot);
		legend.setPosition(RectangleEdge.LEFT);
		BlockContainer blockcontainer = new BlockContainer(new BorderArrangement());
		blockcontainer.add(legend, RectangleEdge.LEFT);// 这行代码可以控制位置
		CompositeTitle compositetitle = new CompositeTitle(blockcontainer);
		compositetitle.setPosition(RectangleEdge.RIGHT);// 放置图形代表区域位置的代码

		localJFreeChart.addSubtitle(compositetitle);

		return localJFreeChart;

	}

	// 第一页第一行
	private WritableCellFormat getFirstSheetHeaderStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.YELLOW);
		return tempCellFormat;
	}

	// 第一页第二行
	private WritableCellFormat getFirstSheetSecondStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.WHITE);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.RED);
		return tempCellFormat;
	}

	// 第一页第三行
	private WritableCellFormat getFirstSheetThirdStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		// 单元格的线
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);
		return tempCellFormat;
	}

	// 第一页内容
	private WritableCellFormat getFirstSheetContentStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.NO_BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		// 单元格的线
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		return tempCellFormat;
	}

	private String getTitle() {

		String startTime = processScanOptimizeBean.getStartTime();
		String endTime = processScanOptimizeBean.getEndTime();
		startTime = startTime.substring(0, 7);
		endTime = endTime.substring(0, 7);
		startTime = startTime.replaceFirst("-", JecnUtil.getValue("years"));
		endTime = endTime.replaceFirst("-", JecnUtil.getValue("years"));

		StringBuffer buf = new StringBuffer();
		buf.append(startTime);
		buf.append(JecnUtil.getValue("month"));
		buf.append("-");
		buf.append(endTime);
		buf.append(JecnUtil.getValue("month"));
		buf.append(" ");
		buf.append(type == 0 ? JecnUtil.getValue("processScanFinishPercent") : "制度审视优化及时完成率");
		return buf.toString();
	}

	private String getSecondTitle() throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		// 开始时间
		String startTime = processScanOptimizeBean.getEndTime();
		String endTime = processScanOptimizeBean.getEndTime();
		Date startDate = format.parse(startTime);
		Date endDate = format.parse(endTime);

		Calendar calender = Calendar.getInstance();
		calender.setTime(startDate);
		calender.add(Calendar.MONTH, 1);
		startTime = format.format(calender.getTime()).substring(0, 8);
		startTime = startTime.replaceFirst("-", JecnUtil.getValue("years"));
		startTime = startTime.replaceFirst("-", JecnUtil.getValue("month"));

		calender.setTime(endDate);
		calender.add(Calendar.MONTH, 2);
		endTime = format.format(calender.getTime()).substring(0, 8);
		endTime = endTime.replaceFirst("-", JecnUtil.getValue("years"));
		endTime = endTime.replaceFirst("-", JecnUtil.getValue("month"));

		StringBuffer buf = new StringBuffer();
		buf.append(startTime);
		buf.append("-");
		buf.append(endTime);
		buf.append(" ");
		buf.append(type == 0 ? JecnUtil.getValue("needFinishScanProcessDistribute") : "需完成审视优化的制度分布");
		return buf.toString();
	}

	private String jfreeChartToPng(JFreeChart localJFreeChart, int width, int height) {

		String path = JecnPath.APP_PATH;
		// 获取存储临时文件的目录
		String tempFileDir = path + "images/tempImage/" + "PROCESS_SCAN/";
		// 创建临时文件目录
		File fileDir = new File(tempFileDir);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		String timefile = UUID.randomUUID().toString();
		String fileName = tempFileDir + timefile + ".png";
		FileOutputStream fos = null;

		try {
			fos = new FileOutputStream(fileName);
			ChartUtilities.writeChartAsPNG(fos, localJFreeChart, width, height);
			fos.flush();
		} catch (Exception e) {
			log.error("ProcessScanDownExcelService生成图片异常", e);
		} finally {

			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("ProcessScanDownExcelService关闭图片流异常", e);
				}
			}
		}

		return fileName;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
