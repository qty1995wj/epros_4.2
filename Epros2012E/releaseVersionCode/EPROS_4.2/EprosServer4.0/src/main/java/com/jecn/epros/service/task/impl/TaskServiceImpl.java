package com.jecn.epros.service.task.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.common.BaseBean;
import com.jecn.epros.domain.common.abolish.AbolishBean;
import com.jecn.epros.domain.common.relatedfiles.RelatedFileBean;
import com.jecn.epros.domain.email.EmailBasicInfo;
import com.jecn.epros.domain.email.EmailTip;
import com.jecn.epros.domain.file.FileBeanT;
import com.jecn.epros.domain.process.*;
import com.jecn.epros.domain.rule.Rule;
import com.jecn.epros.domain.rule.RuleBaseInfo;
import com.jecn.epros.domain.rule.RuleT;
import com.jecn.epros.domain.system.Message;
import com.jecn.epros.domain.task.*;
import com.jecn.epros.email.BaseEmailBuilder;
import com.jecn.epros.email.EmailBuilderFactory;
import com.jecn.epros.email.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.mapper.*;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.CommonServiceUtil;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.service.common.CommonService;
import com.jecn.epros.service.task.TaskService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.service.web.JecnIflytekSyncUserEnum;
import com.jecn.epros.sqlprovider.BaseSqlProvider;
import com.jecn.epros.sqlprovider.server3.*;
import com.jecn.epros.sqlprovider.server3.ConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.Utils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class TaskServiceImpl implements TaskService {

    private static final String STR_SPLIT = "\n";
    private static final Log log = LogFactory.getLog(TaskServiceImpl.class);

    @Autowired
    private TaskMapper taskMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RuleMapper ruleMapper;

    @Autowired
    private ProcessMapper processMapper;

    @Autowired
    private FileMapper fileMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private DocMapper docMapper;

    @Autowired
    private ConfigItemMapper configItemMapper;

    @Autowired
    private EmailMapper emailMapper;

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private OrgMapper orgMapper;
    @Autowired
    private CommonService commonService;


    @Override
    public int getTaskCountBySearch(TaskSearchTempBean taskSearchTempBean, Long curPeopleId, Boolean isAdmin,
                                    Boolean isSecondAdmin) {

        Long roleId = roleMapper.getSecondAdminRoleId(curPeopleId);

        Map<String, Object> parmMap = new HashMap<String, Object>();
        parmMap.put("taskSearchTempBean", taskSearchTempBean);
        parmMap.put("curPeopleId", curPeopleId);
        parmMap.put("isAdmin", isAdmin);
        parmMap.put("isSecondAdmin", isSecondAdmin);
        parmMap.put("roleId", roleId);

        int total = taskMapper.getTaskManageCountBySearch(parmMap);
        return total;
    }

    @Override
    public List<MyTaskBean> fetchTasks(TaskSearchTempBean taskSearchTempBean, Paging paging, Long curPeopleId,
                                       Boolean isAdmin, Boolean isSecondAdmin) {

        Long roleId = roleMapper.getSecondAdminRoleId(curPeopleId);

        Map<String, Object> parmMap = new HashMap<String, Object>();
        parmMap.put("taskSearchTempBean", taskSearchTempBean);
        parmMap.put("curPeopleId", curPeopleId);
        parmMap.put("isAdmin", isAdmin);
        parmMap.put("isSecondAdmin", isSecondAdmin);
        parmMap.put("roleId", roleId);

        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "UPDATE_TIME DESC");
        List<MyTaskBean> tasks = taskMapper.fetchTasks(parmMap);
        for (MyTaskBean task : tasks) {
            task.setTaskName(task.getApproveType() == 0 ? task.getTaskName() : task.getTaskName() + " - <font  color=\"#FF0000\">" + JecnProperties.getValue("abolitionTask") + "</font >");
        }
        for (MyTaskBean myTaskBean : tasks) {
            // 在任务管理页面后两个参数是无用的，因为只有管理员可以编辑查阅删除，普通人员只有查阅功能
            initTaskOperation(myTaskBean, isAdmin, true, false, false);
        }
        IfytekCommon.getIflytekTaskUserName(tasks);
        return tasks;

    }


    @Override
    public void replaceApprovePeople(Long fromPeopleId, Long toPeopleId) {

        Map<String, Object> parmMap = new HashMap<String, Object>();
        parmMap.put("fromPeopleId", fromPeopleId);
        parmMap.put("toPeopleId", toPeopleId);
        taskMapper.updateTaskPeopleConn(parmMap);
        taskMapper.updateTaskPeople(parmMap);
        taskMapper.updateTaskCreatePeople(parmMap);
        taskMapper.updateTaskFromPeople(parmMap);
    }

    @Override
    public int getMyTaskCount(TaskSearchTempBean taskSearchTempBean, Long peopleId) {

        boolean isAppMesTrue = taskIsCallBack();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("taskSearchTempBean", taskSearchTempBean);
        paramMap.put("curPeopleId", peopleId);
        paramMap.put("isAppMesTrue", isAppMesTrue);
        int total = taskMapper.getMyTaskCount(paramMap);
        return total;
    }

    /**
     * 任务是否可以撤回
     *
     * @return
     */
    private boolean taskIsCallBack() {
        String value = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.taskOperationReCall.toString());
        if (value != null && value.trim().equals("1")) {
            return true;
        }
        return false;
    }

    @Override
    public List<MyTaskBean> fetchMyTasks(TaskSearchTempBean taskSearchTempBean, Long loginPeopleId, Paging paging) {
        // 撤回
        boolean isAppMesTrue = taskIsCallBack();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("taskSearchTempBean", taskSearchTempBean);
        paramMap.put("curPeopleId", loginPeopleId);
        paramMap.put("isAppMesTrue", isAppMesTrue);
        List<TaskBean> tasks = taskMapper.fetchMyTasks(paramMap);
        List<Long> taskIds = taskMapper.fetchMyCurrentTasks(loginPeopleId);
        List<MyTaskBean> myTaskBeans = new ArrayList<MyTaskBean>();
        for (TaskBean task : tasks) {
            myTaskBeans.add(initMyTaskBean(loginPeopleId, isAppMesTrue, taskIds, task));
        }
        resortTask(myTaskBeans);
        return myTaskBeans;
    }

    /**
     * 任务重排序，排序之前的任务是根据更新日期已经排过序了。所以现在日期的基础上依据操作再来一次
     * 福耀 - 首页任务代办显示 - 优先按时顺序排列 - 先显示待审批的，后显示可撤回，最后显示拟稿人查阅取消的
     *
     * @param tasks
     */
    private void resortTask(List<MyTaskBean> tasks) {
        if (Utils.isEmpty(tasks)) {
            return;
        }
        List<MyTaskBean> A = new ArrayList<>();
        List<MyTaskBean> B = new ArrayList<>();
        List<MyTaskBean> C = new ArrayList<>();
        tasks.stream().forEach(t -> {
            List<LinkResource> links = t.getLinks();
            for (LinkResource l : links) {
                if (l.getType() == ResourceType.TASK_APPROVE || l.getType() == ResourceType.TASK_RESUBMIT || l.getType() == ResourceType.TASK_ORGANIZE) {
                    A.add(t);
                    break;
                } else if (l.getType() == ResourceType.TASK_RETRACT) {
                    B.add(t);
                    break;
                } else {
                    C.add(t);
                    break;
                }
            }
        });
        tasks.clear();
        tasks.addAll(A);
        tasks.addAll(B);
        tasks.addAll(C);
    }

    private MyTaskBean initMyTaskBean(Long loginPeopleId, Boolean isAppMesTrue, List<Long> taskIds, TaskBean task) {

        boolean loginPeopleIsCreatePeople = loginPeopleId.equals(task.getCreatePersonId());
        boolean loginPeopleIsApprove = taskIds == null ? false : taskIds.contains(task.getTaskId());

        final Integer taskElseState = task.getTaskElseState();

        // 0是打回重新提交状态,1是评审意见整理状态,2为系统管理员;3:拟稿人无审批状态;4:拟稿人为当前阶段审批人
        Integer taskElseType = -1;
        Integer taskState = task.getTaskState();
        Integer upState = task.getUpState();
        Integer reJect = null;
        // 3任务 拟稿人操作状态 0：拟稿人提交审批 4：打回
        // 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
        // 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交
        if (loginPeopleIsCreatePeople) {// 当前登陆人是拟稿人
            if ((taskState == 0 || taskState == 10)) {// 整理意见阶段或拟搞阶段
                if (taskElseState == 4) {// 打回 (被打回，拟稿人不应该有撤回操作)
                    // 0是驳回重新提交状态
                    taskElseType = 4;
                } else {
                    // 1是评审意见整理状态
                    taskElseType = 1;
                }
            } else {// 当前登录人为拟稿人，并且在目标人集合中不存在，不是当前阶段审批人
                // 拟稿人无审批状态
                if (loginPeopleIsApprove) {//
                    // 拟稿人为当前阶段审批人
                    taskElseType = 4;
                } else {
                    // 登录人为拟稿人，3:拟稿人无审批状态
                    taskElseType = 3;
                }
            }
        }

        // 配置项有撤回功能，非拟稿人操作，当前登陆人员是任务的来源人，当前阶段非会审阶段
        if (isAppMesTrue && !isDraftCur(taskElseType) && loginPeopleId.equals(task.getFromPeopleId()) && canCallBack(upState)
                && taskElseState != 5) {
            // 只有拟稿人才会给3这个标识
            if (taskElseType == 3) {
                // 存在撤回状态
                reJect = 1;
            } else {
                // 判断如果当前登录人员是当前任务的审批人(也就是登录人当前需要审批的任务集合listTaskIds包含taskId)那么他只有审批操作，不可能有撤回操作
                // true为可以撤回 false 为不可以撤回
                boolean canCancel = !loginPeopleIsApprove;
                // 可以撤回
                if (canCancel) { // 撤回标示 1为可以撤回
                    reJect = 1;
                }
            }
        }

        MyTaskBean myTaskBean = new MyTaskBean();
        myTaskBean.setTaskId(task.getTaskId());
        myTaskBean.setTaskName(task.getApproveType() == 0 ? task.getTaskName() : task.getTaskName() + " - <font  color=\"#FF0000\">" + JecnProperties.getValue("abolitionTask") + "</font >");
        myTaskBean.setCreateName(task.getCreateName());
        myTaskBean.setTaskType(task.getTaskType());
        myTaskBean.setResPeopleName(task.getResPeopleName());
        myTaskBean.setReJect(reJect);
        myTaskBean.setStageName(task.getStateMark());
        myTaskBean.setTaskStage(taskState);
        myTaskBean.setUpdateTime(task.getUpdateTime());
        initTaskOperation(myTaskBean, false, false, loginPeopleIsCreatePeople, loginPeopleIsApprove);

        return myTaskBean;
    }

    /**
     * 是否可以撤回
     *
     * @param upState
     * @return
     */
    private boolean canCallBack(Integer upState) {
        if (upState != null && hasMultiPeople(upState)) {
            return false;
        }
        return true;
    }

    /**
     * 是否为自定义阶段
     *
     * @param state
     * @return
     */
    private boolean hasMultiPeople(Integer state) {
        if ((state >= 11 && state <= 14) || state == 3) {
            return true;
        }
        return false;
    }

    /**
     * 拼接当前任务的操作
     *
     * @param myTaskBean
     * @return
     */
    private void initTaskOperation(MyTaskBean myTaskBean, boolean isAdmin, boolean isTaskManage,
                                   boolean loginPeopleIsCreatePeople, boolean loginPeopleIsApprovePeople) {

        Integer taskElseState = myTaskBean.getTaskElseState();
        Integer taskStage = myTaskBean.getTaskStage();
        Long taskId = myTaskBean.getTaskId();
        Integer reJect = myTaskBean.getReJect();

        // 编辑edit
        ResourceType edit = null;
        // 查阅都有
        ResourceType view = ResourceType.TASK_VIEW;
        // 撤回retract
        ResourceType retract = null;
        // 删除delete
        ResourceType delete = null;
        // 审批approve
        ResourceType approve = null;
        // 重新提交resubmit
        ResourceType resubmit = null;
        // 整理意见organize
        ResourceType organize = null;

        // 删除功能
        // 任务管理界面管理员||我的任务界面我提交的任务
        if ((isTaskManage && isAdmin) || (!isTaskManage && loginPeopleIsCreatePeople)) {
            delete = ResourceType.TASK_DELETE;
        }

        // 编辑功能
        // 任务管理界面管理员未完成
        if ((isTaskManage && isAdmin && myTaskBean.getTaskStage() != 5)) {
            edit = ResourceType.TASK_EDIT;
        }

        // 撤回
        if (!isTaskManage && reJect != null && reJect == 1 && stateCanCallBack(taskStage)) {
            retract = ResourceType.TASK_RETRACT;
        }

        // 重新提交功能 我的任务阶段 是我的任务 打回重新提交状态
        if (!isTaskManage && loginPeopleIsCreatePeople && taskStage == 0) {
            resubmit = ResourceType.TASK_RESUBMIT;
        }

        // 整理意见功能
        if (!isTaskManage && loginPeopleIsCreatePeople && taskStage == 10) {
            organize = ResourceType.TASK_ORGANIZE;
        }

        // 审批
        if (!isTaskManage && loginPeopleIsApprovePeople) {
            approve = ResourceType.TASK_APPROVE;
        }

        // 同一个人不可能同时是重新提交和整理意见阶段
        // 重新提交整理意见优先级大于审批
        // 拼接字段
        List<LinkResource> links = myTaskBean.getLinks();
        if (resubmit != null) {
            //重新提交
            links.add(getLinkResource(resubmit, taskId, JecnProperties.getValue("resubmit")));
        }

        if (organize != null) {
            //整理意见
            links.add(getLinkResource(organize, taskId, JecnProperties.getValue("collateOpinions")));
        }

        if (retract != null) {
            //
            links.add(getLinkResource(retract, taskId, JecnProperties.getValue("withdraw")));
        }

        if (resubmit == null && organize == null && approve != null) {
            //审批
            links.add(getLinkResource(approve, taskId, JecnProperties.getValue("examineAndApprove")));
        }

        if (edit != null) {
            //编辑
            links.add(getLinkResource(edit, taskId, JecnProperties.getValue("edit")));
        }

        if (view != null) {
            //查阅
            links.add(getLinkResource(view, taskId, JecnProperties.getValue("consult")));
        }

        if (delete != null) {
            links.add(getLinkResource(delete, taskId, JecnProperties.getValue("delete")));
        }
    }

    private boolean stateCanCallBack(Integer taskStage) {
        if (taskStage == 10 || (taskStage >= 11 && taskStage <= 14)) {
            return false;
        }
        return true;
    }

    private LinkResource getLinkResource(ResourceType type, Long taskId, String name) {

        LinkResource r = new LinkResource(String.valueOf(taskId), name, type);

        return r;
    }

    /**
     * 0是打回重新提交状态,1是评审意见整理状态,2为系统管理员;3:拟稿人无审批状态;4:拟稿人为当前阶段审批人
     *
     * @param taskElseType
     * @return
     */
    private boolean isDraftCur(int taskElseType) {
        if (taskElseType == 0 || taskElseType == 1 || taskElseType == 4) {
            return true;
        }
        return false;
    }

    private void getCustomName(SimpleTaskBean simpleTaskBean) {
        TaskBean taskBean = simpleTaskBean.getTaskBeanNew();
        int taskType = taskBean.getTaskType();
        int typeBigModel = 0; //流程架构
        int typeSmallModel = 1;
        Map<String, Object> configMap = new HashMap<String, Object>();
        switch (taskType) {
            case 0: //流程
                typeBigModel = 1;
                break;
            case 1: //文件
                typeBigModel = 3;
                break;
            case 2: //制度
            case 3:
                typeBigModel = 2;
                break;
            default:
                break;
        }
        configMap.put("typeBigModel", typeBigModel);
        configMap.put("typeSmallModel", typeSmallModel);
        List<ConfigItemBean> items = configItemMapper.findConfigItems(configMap);
        for (ConfigItemBean configItem : items) {
            String name = configItem.getName(AuthenticatedUserUtil.isLanguageType());
            if ("isShowCustomOneInput".equals(configItem.getMark())) {
                simpleTaskBean.setCustomInputItemOneName(name);
            } else if ("isShowCustomTwoInput".equals(configItem.getMark())) {
                simpleTaskBean.setCustomInputItemTwoName(name);
            } else if ("isShowCustomThreeInput".equals(configItem.getMark())) {
                simpleTaskBean.setCustomInputItemThreeName(name);
            }
        }
    }


    /**
     * 相关文件
     *
     * @param taskBean
     * @return
     */
    private List<LinkResource> getRelatedFiles(TaskBean taskBean, String isApp) {
        List<LinkResource> relatedFiles = null;
        Map<String, Object> map = new HashedMap();
        map.put("isPub", "_T");
        // 0为流程任务，1为文件任务，2为制度任务
        switch (taskBean.getTaskType()) {
            case 0:
                map.put("flowId", taskBean.getRid());
                List<BaseBean> processRelatedFiles = processMapper.findRelatedFiles(map);
                relatedFiles = getRelatedAppFiles(processRelatedFiles, ResourceType.FILE, isApp);
                break;
            case 4:// 获取流程架构信息
                map.put("flowId", taskBean.getRid());
                List<BaseBean> processMapRelatedFiles = processMapper.findMapRelatedFiles(map);
                relatedFiles = getRelatedAppFiles(processMapRelatedFiles, ResourceType.FILE, isApp);
                break;
            case 1:// 获取文件信息
                break;
            case 2:// 获取制度
            case 3:// 制度
                map.put("id", taskBean.getRid());
                relatedFiles = getRelatedAppFiles(ruleMapper.findTaskRuleFiles(map), ResourceType.FILE, isApp);
                break;
            default:
                break;
        }
        return relatedFiles;
    }


    /**
     * 支撑文件
     *
     * @param taskBean
     * @return
     */
    private List<LinkResource> getSupportingFiles(TaskBean taskBean, String isApp) {
        Map<String, Object> map = new HashedMap();
        map.put("flowId", taskBean.getRid());   //获取人员ID
        map.put("historyId", 0L);
        map.put("isPub", "_T");
        // 0为流程任务，1为文件任务，2为制度任务
        switch (taskBean.getTaskType()) {
            case 0:
                map.put("type", 1);
                break;
            case 4:// 获取流程架构信息
                map.put("type", 0);
                break;
            case 1:// 获取文件信息
                map.put("type", -1);
                break;
            case 2:// 获取制度
            case 3:// 制度
                map.put("id", taskBean.getRid());   //获取人员ID
                map.put("type", 2);
                break;
            default:
                break;
        }
        return getSupportingAppFiles(commonService.findRelatedFiles(map), isApp);
    }

    @Override
    public SimpleTaskBean getSimpleTaskBean(Long taskId, ResourceType taskOperation, String isApp) {
        // 任务相关信息
        SimpleTaskBean simpleTaskBean = new SimpleTaskBean();
        // 获取任务信息
        TaskBean taskBean = getTaskBeanNew(taskId);
        MessageResult msg = new MessageResult();
        // 任务相关信息
        if (taskBean.getIsLock() == 0) {
            msg.setSuccess(false);
            msg.setResultMessage("The task has been deleted");
            simpleTaskBean.setResult(msg);
            return simpleTaskBean;
        }
        if (ResourceType.TASK_EDIT == taskOperation && !AuthenticatedUserUtil.isAdmin()) {
            simpleTaskBean.setResult(new MessageResult(false, "Permission denied"));
            return simpleTaskBean;
        }
        // 设置任务信息
        simpleTaskBean.setTaskBeanNew(taskBean);

        // 获取任务显示项
        TaskApplicationNew taskApplicationNew = taskMapper.getTaskApplicationNew(taskId);
        simpleTaskBean.setTaskApplicationNew(taskApplicationNew);
        getCustomName(simpleTaskBean);//获取自定义输入项 配置名称
        // 获取相关资源（流程、文件、制度）的数据
        simpleTaskBean.setIsApp(isApp);
        simpleTaskBean.setRelatedResource(getSimpleTaskRelatedResource(simpleTaskBean));
        // 试运行报告
        if (taskBean.getFlowType() != null && taskBean.getFlowType() != 0) {
            // 获取试运行报告
            Map<String, Object> taskRunFileMap = taskMapper.getTaskRunFileName(taskId);
            if (taskRunFileMap != null) {
                // 试运行主键ID
                simpleTaskBean.setTaskRunFileId(taskRunFileMap.get("ID").toString());
                // 试运行文件名称
                simpleTaskBean.setTaskRunFileName(taskRunFileMap.get("FILENAME").toString());
            }
        }

        // 获取任务下的所有阶段信息
        List<TaskStage> taskStageList = taskMapper.fetchTaskStagesByTaskId(taskId);

        // 任务审批阶段选择人的文本框的内容
        List<TempAuditPeopleBean> listAuditPeopleBean = getListAuditPeopleBean(taskBean, taskStageList, simpleTaskBean,
                taskOperation, taskApplicationNew.getIsControlAuditLead());
        simpleTaskBean.setListAuditPeopleBean(listAuditPeopleBean);

        // 获取任务审批流转记录集合
        List<TaskForRecodeNew> listTaskForRecodeNew = findTaskForRecodeNewByTaskId(taskBean, taskStageList);
        simpleTaskBean.setListTaskForRecodeNew(listTaskForRecodeNew);

        // 任务界面按钮的显示
        simpleTaskBean.setTaskButtons(getTaskButtons(taskOperation, taskStageList, listAuditPeopleBean, taskBean));

        simpleTaskBean.setApproveRecords(getApproveRecords(simpleTaskBean));

        if (taskOperation == ResourceType.TASK_EDIT) {
            simpleTaskBean.setDelayReason(getDelayReason(simpleTaskBean));
        }

        simpleTaskBean.setTaskEditConfig(getTaskEditConfig(taskOperation, taskBean, taskStageList));
        //任务相关文件
        simpleTaskBean.setRelatedFiles(getRelatedFiles(taskBean, isApp));
        simpleTaskBean.setSupportingFiles(getSupportingFiles(taskBean, isApp));
        return simpleTaskBean;
    }

    private List<LinkResource> getRelatedAppFiles(List<BaseBean> baseBeans, ResourceType resourceType, String isApp) {
        List<LinkResource> relatedFiles = new ArrayList();
        if (baseBeans == null) {
            return relatedFiles;
        }
        LinkResource link = null;
        for (BaseBean bean : baseBeans) {
            link = new LinkResource(bean.getId(), bean.getName(), resourceType);
            link.setIsPublic(0);
            link.setAppRequest("true".equals(isApp));
            relatedFiles.add(link);
        }
        return relatedFiles;
    }

    private List<LinkResource> getSupportingAppFiles(List<RelatedFileBean> baseBeans, String isApp) {
        List<LinkResource> relatedFiles = new ArrayList();
        if (baseBeans == null) {
            return relatedFiles;
        }
        for (RelatedFileBean bean : baseBeans) {
            bean.getLink().setIsPublic(0);
            bean.getLink().setAppRequest("true".equals(isApp));
            if (bean.getLink() != null && bean.getLink().getType() == ResourceType.RISK) {
                bean.getLink().setName(bean.getName());
            }
            relatedFiles.add(bean.getLink());
        }
        return relatedFiles;
    }

    private TaskEidtConfig getTaskEditConfig(ResourceType taskOperation, TaskBean task, List<TaskStage> stages) {
        TaskEidtConfig editConfig = new TaskEidtConfig();
        if (taskOperation == ResourceType.TASK_APPROVE && (task.getTaskState() == 2 || task.getTaskState() == 1)) {
            editConfig.setOrgPerm(true);
            editConfig.setPosGroupPerm(true);
            editConfig.setPosPerm(true);
            editConfig.setResourceType(true);
            editConfig.setSecret(true);
        }
        editConfig.setTwoValidteEnable(ConfigUtils.isDryl());
        Integer taskState = task.getTaskState();
        // 查看任务的当前阶段是否为任务最后一个阶段
        if (task.getApproveType() == 2) {
            editConfig.setImplementDateShow(false);
        } else {
            for (int i = stages.size() - 1; i >= 0; i--) {
                if (1 == stages.get(i).getIsShow() && stages.get(i).getIsSelectedUser() == 1) {
                    editConfig.setImplementDateShow(taskState.equals(stages.get(i).getStageMark()));
                    break;
                }
            }
        }
        if (task.getApproveType() == 2) {
            editConfig.setImplementDateNeedDefault(false);
        } else {
            editConfig.setImplementDateNeedDefault(!ConfigUtils.isDryl());
        }
        return editConfig;
    }

    // case 0:// 拟稿人提交审批
    // case 1:// 通过
    // case 2:// 交办
    // case 3:// 转批
    // case 4:// 打回
    // case 5:// 提交意见（提交审批意见(评审-------->拟稿人)
    // case 6:// 二次评审 拟稿人------>评审
    // case 7:// 拟稿人重新提交审批
    // case 8:// 完成
    // case 9:// 打回整理意见(任务状态在评审人后的审核阶段------>拟稿人)）
    // case 10:// 交办人提交
    // case 11:// 编辑 (系统管理员编辑任务)
    // case 12:// 编辑 (系统管理员编辑任务)
    // case 13:// 撤回
    // case 14:// 返回
    // case 15:// 交给拟稿人整理（相当于打回整理---->拟稿人，只不过还在当前阶段）
    // case 16:// 拟稿人整理完交给当前阶段（拟稿人------->当前阶段审批人）

    private List<ApproveRecord> getApproveRecords(SimpleTaskBean simpleTaskBean) {
        List<ApproveRecord> approveRecords = new ArrayList<ApproveRecord>();
        List<TaskForRecodeNew> listTaskForRecodeNew = simpleTaskBean.getListTaskForRecodeNew();
        for (TaskForRecodeNew recordNew : listTaskForRecodeNew) {
            approveRecords.add(crateRecordByTaskForRecord(recordNew));
        }
        int taskState = simpleTaskBean.getTaskBeanNew().getTaskState();
        if (taskState != 5) {// 未完成
            List<TempAuditPeopleBean> listAudit = simpleTaskBean.getListAuditPeopleBean();
            boolean isStart = false;
            for (TempAuditPeopleBean audit : listAudit) {
                if (audit.getState() == taskState) {// 当前阶段
                    isStart = true;
                    approveRecords.add(createRecordByTempAuditPeople(audit, 1, simpleTaskBean.getTaskBeanNew()));
                } else if (isStart && audit.getIsSelectedUser() == 1) {// 当前阶段之后的阶段
                    approveRecords.add(createRecordByTempAuditPeople(audit, 2, simpleTaskBean.getTaskBeanNew()));
                }
            }
        }
        return approveRecords;
    }

    private ApproveRecord createRecordByTempAuditPeople(TempAuditPeopleBean audit, int isApprove, TaskBean taskBean) {
        ApproveRecord record = new ApproveRecord();
        record.setIsApprove(isApprove);
        record.setStateName(audit.getAuditLabel());
        if (audit.getState() == 10) {
            record.setHandleName(taskBean.getCreatePeopleTemporaryName());
        } else {
            record.setHandleName(audit.getAuditName());
        }

        return record;
    }

    private ApproveRecord crateRecordByTaskForRecord(TaskForRecodeNew recordNew) {
        ApproveRecord record;
        record = new ApproveRecord();
        record.setIsApprove(0);
        record.setHandleName(recordNew.getFromPeopleTemporaryName());
        record.setOption(recordNew.getOpinion());
        record.setStateName(recordNew.getUpStateName());
        record.setTaskElseStateName(recordNew.getTaskElseStateName());
        record.setTaskFixedDesc(recordNew.getTaskFixedDesc());
        record.setUpdateTime(recordNew.getUpdateTime());
        return record;
    }

    private List<TaskButton> getTaskButtons(ResourceType taskOperation, List<TaskStage> taskStageList,
                                            List<TempAuditPeopleBean> listAuditPeopleBean, TaskBean taskBean) {
        Map map = new HashedMap();
        map.put("typeBigModel", 5);
        map.put("typeSmallModel", 7);
        List<ConfigItemBean> config = configItemMapper.findConfigItems(map);
        List<TaskButton> taskButtons = new ArrayList<TaskButton>();
        TaskButton taskButton = null;
        if (ResourceType.TASK_APPROVE == taskOperation && taskBean.getTaskState() == 0) {
            taskOperation = ResourceType.TASK_RESUBMIT;
        }
        if (ResourceType.TASK_APPROVE == taskOperation) {
            if (isAssignNeedApprove(listAuditPeopleBean, taskBean)) {// 如果当前人员是交办人
                // 只有提交和取消按钮
                taskButton = new TaskButton(JecnProperties.getValue("submit"), 10);
                taskButtons.add(taskButton);
            } else if (taskBean.getTaskState() == 3) {// 会审
                taskButton = new TaskButton(JecnProperties.getValue("submissionOfOpinion"), 5);
                taskButtons.add(taskButton);
            } else {
                if (curLoginPeopleIsNewVersionBackCollate(taskBean.getTaskId())) {
                    taskButton = new TaskButton(JecnProperties.getValue("submit"), 16);
                    taskButtons.add(taskButton);
                } else {
                    // 是否存在返回
                    boolean reback = taskIsReBack(taskBean.getTaskState());
                    // 是否存在交办
                    boolean assign = taskIsAssign();
                    // 是否存在转批
                    boolean transfer = taskIsTransfer();
                    // 任务State集合
                    List<Integer> orderList = getOrderList(taskStageList);
                    // 是否存在打回
                    boolean back = taskIsBack();

                    if (ConfigUtils.newBackCollateEnable()) {
                        if (stateCanHaveNewVersionBackCollate(taskBean)) {
                            taskButton = new TaskButton(JecnProperties.getValue("collate"), 15);
                            taskButtons.add(taskButton);
                        }
                    } else if (ConfigUtils.oldBackCollateEnable()) {
                        // 打回整理验证 审批人必须存在评审人会审阶段后
                        boolean isCallBack = TaskCommon.isCallBack(orderList, taskBean.getTaskState(), listAuditPeopleBean);
                        if (isCallBack) {// 存在打回整理状态 说明评审阶段已审批完成
                            taskButton = new TaskButton(JecnProperties.getValue("collate"), 9);
                            taskButtons.add(taskButton);
                        }
                    }

                    if (reback) {
                        taskButton = new TaskButton(findTaskConfigName(config, ConfigItemPartMapMark.taskOperationReBack), 14); //fanhui
                        taskButtons.add(taskButton);
                    }
                    if (back) {
                        taskButton = new TaskButton(findTaskConfigName(config, ConfigItemPartMapMark.taskOperationRepulse), 4); //打回
                        taskButtons.add(taskButton);
                    }
                    if (transfer) {
                        taskButton = new TaskButton(findTaskConfigName(config, ConfigItemPartMapMark.taskOperationTransfer), 3);//转批
                        taskButtons.add(taskButton);
                    }
                    if (assign) {
                        taskButton = new TaskButton(findTaskConfigName(config, ConfigItemPartMapMark.taskOperationAssigned), 2);//交办
                        taskButtons.add(taskButton);
                    }
                    taskButton = new TaskButton(JecnProperties.getValue("adopt"), 1);
                    taskButtons.add(taskButton);
                }
            }
        } else if (ResourceType.TASK_RESUBMIT == taskOperation) {
            taskButton = new TaskButton(JecnProperties.getValue("resubmit"), 7);
            taskButtons.add(taskButton);
        } else if (ResourceType.TASK_ORGANIZE == taskOperation) {
            taskButton = new TaskButton(JecnProperties.getValue("submit"), 1);
            taskButtons.add(taskButton);
            if (taskBean.getRevirewCounts() < 2) {
                taskButton = new TaskButton(JecnProperties.getValue("twoReview"), 6);
                taskButtons.add(taskButton);
            }
        } else if (ResourceType.TASK_EDIT == taskOperation) {
            taskButton = new TaskButton(JecnProperties.getValue("submit"), 11);
            taskButtons.add(taskButton);
            taskButton = new TaskButton(findTaskConfigName(config, ConfigItemPartMapMark.taskOperationRepulse), 12);
            taskButtons.add(taskButton);
        } else {
            taskButtons = null;
        }
        return taskButtons;
    }

    public boolean taskIsBack() {
        String value = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.taskOperationRepulse.toString());
        if (value != null && value.trim().equals("1")) {
            return true;
        }
        return false;
    }

    private String findTaskConfigName(List<ConfigItemBean> config, ConfigItemPartMapMark type) {
        for (ConfigItemBean data : config) {
            if (type.toString().equals(data.getMark())) {
                return data.getName(AuthenticatedUserUtil.isLanguageType());
            }
        }
        return "error";
    }

    private boolean curLoginPeopleIsNewVersionBackCollate(Long taskId) {
        Long peopleId = AuthenticatedUserUtil.getPeopleId();
        TaskForRecodeNew conn = getTaskForRecordByApprovePeopleNew(taskId, peopleId);
        if (conn != null && conn.getTaskElseState() == 15) {
            return true;
        }
        return false;
    }

    /**
     * 有新版本整理意见的阶段
     *
     * @param taskBean
     * @return
     */
    private boolean stateCanHaveNewVersionBackCollate(TaskBean taskBean) {
        return taskBean.getTaskState() != 0 && taskBean.getTaskState() != 10 && taskBean.getTaskState() != 3;
    }

    public boolean taskIsReBack(Integer taskState) {
        if (taskState >= 11 && taskState <= 14) {
            return false;
        }
        String value = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.taskOperationReBack.toString());
        if (value != null && value.trim().equals("1")) {
            return true;
        }
        return false;
    }

    public boolean taskIsRepulse(Integer taskState) {
        if (taskState >= 11 && taskState <= 14) {
            return false;
        }
        String value = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.taskOperationReBack.toString());
        if (value != null && value.trim().equals("1")) {
            return true;
        }
        return false;
    }

    public boolean taskIsAssign() {
        String value = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.taskOperationAssigned.toString());
        if (value != null && value.trim().equals("1")) {
            return true;
        }
        return false;
    }

    public boolean taskIsTransfer() {
        String value = configItemMapper.findConfigItemByMark(ConfigItemPartMapMark.taskOperationTransfer.toString());
        if (value != null && value.trim().equals("1")) {
            return true;
        }
        return false;
    }

    /**
     * 当前是否是交办人审批（页面展示）
     * 遇到的问题：由于自定义审批阶段也可以转批和交办了，所以无法依据taskElseState来判断当前人员的按钮状况，
     * 所以只能判断当前的审批人是否为conn表中默认的审批人，但是转批也不在那个表中，所以还需要从日志表中查询日志判断操作。真是太麻烦了。
     *
     * @param listAuditPeopleBean
     * @param taskBean
     * @return
     */
    private boolean isAssignNeedApprove(List<TempAuditPeopleBean> listAuditPeopleBean, TaskBean taskBean) {
        /**
         * 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段
         * 8:事业部经理 9：总经理 10:整理意见 11-14 自定义
         */
        Integer taskState = taskBean.getTaskState();
        /**
         * 子操作 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
         * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)） 10：交办人提交 11:编辑提交
         * 12:编辑打回 13:撤回 14:反回
         */
        Integer taskElseState = taskBean.getTaskElseState();
        if (taskState >= 11 && taskState <= 14) {
            boolean curPeopleIsInApproveConn = curLoginPeopleIsInApproveConn(taskBean.getTaskState(), listAuditPeopleBean);
            if (!curPeopleIsInApproveConn) {// 转批人或者交办人
                Long peopleId = AuthenticatedUserUtil.getPeopleId();
                TaskForRecodeNew conn = getTaskForRecordByApprovePeopleNew(taskBean.getTaskId(), peopleId);
                if (conn != null && conn.getTaskElseState() == 2) {
                    return true;
                }
            }
        } else if (taskElseState == 2) {
            return true;
        }

        Map<String, Object> param = new HashMap<>();
        param.put("taskId", taskBean.getTaskId());
        param.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        TaskForRecodeNew record = taskMapper.getTaskForRecordNewByTaskIdAndPeopleId(param);
        if (record != null && record.getTaskElseState().equals(2)) {
            return true;
        }
        return false;
    }

    private TaskForRecodeNew getTaskForRecordByApprovePeopleNew(Long taskId, Long peopleId) {
        Map<String, Object> params = new HashMap<>();
        params.put("peopleId", peopleId);
        params.put("taskId", taskId);
        return taskMapper.getTaskForRecordNewByTaskIdAndPeopleId(params);
    }

    /**
     * 当前登录人是否存在于jecn_task_people_conn表中
     *
     * @param taskState
     * @param listAuditPeopleBean
     * @return
     */
    private boolean curLoginPeopleIsInApproveConn(Integer taskState, List<TempAuditPeopleBean> listAuditPeopleBean) {
        String audits = null;
        for (TempAuditPeopleBean audit : listAuditPeopleBean) {
            if (taskState.equals(audit.getState())) {
                audits = audit.getAuditId();
                break;
            }
        }

        if (StringUtils.isNotBlank(audits)) {
            String[] ids = audits.split(",");
            for (String id : ids) {
                if (AuthenticatedUserUtil.getPeopleId().equals(Long.valueOf(id))) {
                    return true;
                }
            }
        }
        return false;
    }

    private SimpleTaskRelatedResource getSimpleTaskRelatedResource(SimpleTaskBean simpleTaskBean) {
        SimpleTaskRelatedResource relatedResource = null;
        // 获取查阅权限
        long historyId = 0;
        // 0为流程任务，1为文件任务，2为制度任务
        if (simpleTaskBean.getTaskBeanNew().getTaskState() == 5) {
            historyId = findHistoryId(simpleTaskBean.getTaskBeanNew()) == null ? 0 : findHistoryId(simpleTaskBean.getTaskBeanNew()); //为零时表示没有文控信息
            simpleTaskBean.getTaskBeanNew().setHistoryId(historyId);
        }
        switch (simpleTaskBean.getTaskBeanNew().getTaskType()) {
            case 0:
            case 4:// 获取流程信息
                relatedResource = getFlowInfomation(simpleTaskBean, historyId);
                break;
            case 1:// 获取文件信息
                relatedResource = getFileInfomation(simpleTaskBean, historyId);
                break;
            case 2:// 制度
            case 3:// 获取制度
                relatedResource = getRuleInfomation(simpleTaskBean, historyId);
                break;
            default:
                break;
        }
        initAccessPermissions(simpleTaskBean, relatedResource);

        return relatedResource;
    }

    protected Long findHistoryId(TaskBean bean) {
        Map<String, Object> map = new HashedMap();
        map.put("relateId", bean.getRid());
        map.put("type", bean.getTaskType());
        map.put("taskId", bean.getTaskId());
        return processMapper.findHistoryId(map);
    }

    /**
     * 获取当前任务所有的State
     *
     * @param taskStageList
     * @return
     */
    protected List<Integer> getOrderList(List<TaskStage> taskStageList) {
        List<Integer> orderList = new ArrayList<Integer>();
        for (TaskStage taskStage : taskStageList) {
            orderList.add(taskStage.getStageMark());
        }
        return orderList;
    }

    /**
     * 获取任务审批流转记录集合
     *
     * @param taskStageList 任务阶段集合
     * @return 任务审批流转记录集合
     */
    private List<TaskForRecodeNew> findTaskForRecodeNewByTaskId(TaskBean task, List<TaskStage> taskStageList) {
        Long taskId = task.getTaskId();
        // 获取审批人ID集合
        StringBuffer tempToPeopleIds = new StringBuffer();
        // 获取审批人名称集合
        StringBuffer tempToPeopleIdNames = new StringBuffer();
        // 记录所有的审批人
        List<TaskForRecodeNew> tempAllList = new ArrayList<TaskForRecodeNew>();
        // 获取审批记录
        List<TaskForRecodeNew> listJecnTaskForRecodeNew = getTaskForRecodeNewList(taskId);
        // 来源人
        Long fromPeopleId = null;
        // 文控信息
        TaskForRecodeNew record = null;
        // 上个阶段
        Integer upState = null;
        // 更新时间
        Date updateTime = null;
        Integer taskElseState = null;

        for (int i = 0; i < listJecnTaskForRecodeNew.size(); i++) {
            TaskForRecodeNew jecnTaskForRecodeNew = listJecnTaskForRecodeNew.get(i);
            if (jecnTaskForRecodeNew.getState() == null) {
                continue;
            }
            if (jecnTaskForRecodeNew.getUpState() != 0) {// 不是拟稿状态
                // 当前审核阶段名称
                //获取语言类型
                int type = AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getLanguage();
                jecnTaskForRecodeNew.setUpStateName(TaskCommon.getCurTaskStage(taskStageList, jecnTaskForRecodeNew.getUpState()).getStageName(type));
            }
            if (fromPeopleId == null) {
                fromPeopleId = jecnTaskForRecodeNew.getFromPeopleId();
                upState = jecnTaskForRecodeNew.getUpState();
                updateTime = jecnTaskForRecodeNew.getUpdateTime();
                taskElseState = jecnTaskForRecodeNew.getTaskElseState();
                record = jecnTaskForRecodeNew;
            } else {
                if (fromPeopleId.equals(jecnTaskForRecodeNew.getFromPeopleId())
                        && upState.equals(jecnTaskForRecodeNew.getUpState())
                        && taskElseState.equals(jecnTaskForRecodeNew.getTaskElseState())
                        && updateTime.equals(jecnTaskForRecodeNew.getUpdateTime())) {
                    //UN TO DO
                } else {
                    // from人不同
                    addTempList(record, tempToPeopleIds, tempToPeopleIdNames, tempAllList);
                    // 初始化
                    fromPeopleId = jecnTaskForRecodeNew.getFromPeopleId();
                    upState = jecnTaskForRecodeNew.getUpState();
                    updateTime = jecnTaskForRecodeNew.getUpdateTime();
                    record = jecnTaskForRecodeNew;
                    // 获取审批人ID集合
                    tempToPeopleIds = new StringBuffer();
                    // 获取审批人名称集合
                    tempToPeopleIdNames = new StringBuffer();
                }
            }

            appendTempToPeopleIdAndName(tempToPeopleIds, tempToPeopleIdNames, jecnTaskForRecodeNew);

            if (i == listJecnTaskForRecodeNew.size() - 1) {
                addTempList(record, tempToPeopleIds, tempToPeopleIdNames, tempAllList);
            }
            // 记录上一个记录
            fromPeopleId = jecnTaskForRecodeNew.getFromPeopleId();
            upState = jecnTaskForRecodeNew.getUpState();
            updateTime = jecnTaskForRecodeNew.getUpdateTime();
            taskElseState = jecnTaskForRecodeNew.getTaskElseState();
        }
        if (task.getTaskState() == 5 && tempAllList.size() > 0) {
            tempAllList.get(tempAllList.size() - 1).setToPeopleTemporaryName("");
        }
        return tempAllList;
    }


    private void appendTempToPeopleIdAndName(StringBuffer tempToPeopleIds, StringBuffer tempToPeopleIdNames,
                                             TaskForRecodeNew jecnTaskForRecodeNew) {
        if (jecnTaskForRecodeNew.getToPeopleId() != null) {
            tempToPeopleIds.append(jecnTaskForRecodeNew.getToPeopleId().toString()).append(",");
            // 获取评审人name集合
            tempToPeopleIdNames.append(jecnTaskForRecodeNew.getToPeopleTemporaryName()).append("/");
        }
    }

    /**
     * 获取评审阶段审批人ID和名称
     *
     * @param taskForRecodeNew    当前记录日志
     * @param tempToPeopleIds     目标人ID集合
     * @param tempToPeopleIdNames 目标人真实姓名集合
     * @return true评审状态
     */
    private boolean getReviewPeoples(TaskForRecodeNew taskForRecodeNew, StringBuffer tempToPeopleIds,
                                     StringBuffer tempToPeopleIdNames) {
        // 获取评审人ID集合
        if (taskForRecodeNew.getToPeopleId() == null || taskForRecodeNew.getToPeopleTemporaryName() == null) {
            return false;
        }
        tempToPeopleIds.append(taskForRecodeNew.getToPeopleId().toString()).append(",");
        // 获取评审人name集合
        tempToPeopleIdNames.append(taskForRecodeNew.getToPeopleTemporaryName()).append("/");
        return true;
    }

    /**
     * 添加组装后的评审阶段信息到临时日志集合
     *
     * @param record              记录组装后的日志记录
     * @param tempToPeopleIds     评审阶段评审人ID集合
     * @param tempToPeopleIdNames 评审阶段评审名称集合
     * @param tempAllList         临时日志集合
     */
    private void addTempList(TaskForRecodeNew record, StringBuffer tempToPeopleIds,
                             StringBuffer tempToPeopleIdNames, List<TaskForRecodeNew> tempAllList) {
        // 评审人结束
        if (record != null) {
            if (tempToPeopleIds.length() > 1 && tempToPeopleIdNames.length() > 1) {
                String strPeopleIds = tempToPeopleIds.toString();
                strPeopleIds = strPeopleIds.substring(0, strPeopleIds.length() - 1);
                String strPeopleNames = tempToPeopleIdNames.toString();
                strPeopleNames = strPeopleNames.substring(0, strPeopleNames.length() - 1);
                record.setTempToPeopleIds(strPeopleIds);
                record.setToPeopleTemporaryName(strPeopleNames);
            }
            tempAllList.add(record);
        }
    }

    /**
     * 获得任务的审批记录
     *
     * @param taskId
     * @return
     */
    private List<TaskForRecodeNew> getTaskForRecodeNewList(Long taskId) {
        // 获取当前任务下所有日志记录
        List<TaskForRecodeNew> taskRecords = taskMapper.fetchTaskForRecords(taskId);
        // 记录所有日志下所有人员的人员ID集合
        Set<Long> setPeopleId = new HashSet<Long>();
        for (TaskForRecodeNew taskRecord : taskRecords) {
            if (taskRecord.getCreatePersonId() != null) {
                setPeopleId.add(taskRecord.getCreatePersonId());
            }
            if (taskRecord.getFromPeopleId() != null) {
                setPeopleId.add(taskRecord.getFromPeopleId());
            }
            if (taskRecord.getToPeopleId() != null) {
                setPeopleId.add(taskRecord.getToPeopleId());
            }
            if (!Common.isNullOrEmtryTrim(taskRecord.getOpinion())) {// 评审意见换行符替换
                taskRecord.setOpinion(TaskCommon.replaceAll(taskRecord.getOpinion()));
            }
        }
        if (setPeopleId.isEmpty()) {
            return taskRecords;
        }
        List<String> listLoginName = new ArrayList<>();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("ids", setPeopleId);
        List<JecnUser> users = userMapper.findUsers(paramMap);
        Map<Long, JecnUser> userMap = new HashMap<Long, JecnUser>();
        for (JecnUser user : users) {
            userMap.put(user.getId(), user);
        }
        for (TaskForRecodeNew taskForRecodeNew : taskRecords) {
            // 通过人员id获得人员的姓名
            if (taskForRecodeNew.getCreatePersonId() != null) {
                taskForRecodeNew.setCreatePersonTemporaryName(
                        TaskCommon.getTrueNameByPeopleId(userMap, taskForRecodeNew.getCreatePersonId()));
            }
            if (taskForRecodeNew.getFromPeopleId() != null) {
                taskForRecodeNew.setFromPeopleTemporaryName(
                        TaskCommon.getTrueNameByPeopleId(userMap, taskForRecodeNew.getFromPeopleId()));
            }
            if (taskForRecodeNew.getToPeopleId() != null) {
                taskForRecodeNew.setToPeopleTemporaryName(
                        TaskCommon.getTrueNameByPeopleId(userMap, taskForRecodeNew.getToPeopleId()));
            }
        }

        iflytekTaskRecord(taskRecords, listLoginName, userMap);
        return taskRecords;
    }

    private void iflytekTaskRecord(List<TaskForRecodeNew> taskRecords, List<String> listLoginName, Map<Long, JecnUser> userMap) {
        //科大讯飞 需要根据登录名重新查询人员名称
        if (ConfigUtils.isIflytekLogin()) {
            Map<String, String> loginMap = JecnIflytekSyncUserEnum.INSTANCE.getMapUserByLoginName(listLoginName);
            for (TaskForRecodeNew taskForRecodeNew : taskRecords) {
                if (taskForRecodeNew.getCreatePersonId() != null) {
                    JecnUser user = userMap.get(taskForRecodeNew.getCreatePersonId());
                    String trueName = loginMap.get(user.getLoginName());
                    taskForRecodeNew.setCreatePersonTemporaryName(StringUtils.isNotBlank(trueName) ? trueName : taskForRecodeNew.getCreatePersonTemporaryName());
                }
                if (taskForRecodeNew.getFromPeopleId() != null) {
                    JecnUser user = userMap.get(taskForRecodeNew.getFromPeopleId());
                    String trueName = loginMap.get(user.getLoginName());
                    taskForRecodeNew.setFromPeopleTemporaryName(StringUtils.isNotBlank(trueName) ? trueName : taskForRecodeNew.getFromPeopleTemporaryName());
                }
                if (taskForRecodeNew.getToPeopleId() != null) {
                    JecnUser user = userMap.get(taskForRecodeNew.getToPeopleId());
                    String trueName = loginMap.get(user.getLoginName());
                    taskForRecodeNew.setToPeopleTemporaryName(StringUtils.isNotBlank(trueName) ? trueName : taskForRecodeNew.getToPeopleTemporaryName());
                }
            }
        }
    }

    /**
     * 获取各阶段审批人名称
     */
    private List<TempAuditPeopleBean> getListAuditPeopleBean(TaskBean taskBean, List<TaskStage> fullStates,
                                                             SimpleTaskBean simpleTaskBean, ResourceType taskOperation, Integer controlType) {

        List<TaskStage> states = new ArrayList<TaskStage>();
        states.addAll(fullStates);
        // 获取页面中显示所需的Stage信息
        if (controlType != 0 && TaskCommon.isStateBeforeDocControl(taskBean.getTaskState(), states)) {
            // 如果是在文控选人之前，去掉处于文控后面的Stage
            if (ResourceType.TASK_EDIT == taskOperation || taskBean.getTaskState() != 1) {
                TaskStage docControlTaskStage = TaskCommon.getCurTaskStage(states, 1);
                for (int i = states.size() - 1; i >= 0; i--) {
                    if (states.get(i).getSort() > docControlTaskStage.getSort()) {
                        states.remove(i);
                    }
                }
            }
        }

        // 根据任务类型获取对应的系统审批阶段配置项信息
        List<TempAuditPeopleBean> listAuditPeopleBean = getListAuditPeopleBean(taskBean, states);
        return listAuditPeopleBean;
    }

    /**
     * 获取审批,各阶段审核人信息
     *
     * @param taskBeanNew 任务主表信息
     * @param stages      审批顺序
     * @return List<TempAuditPeopleBean> 审批个阶段审核人信息
     */
    private List<TempAuditPeopleBean> getListAuditPeopleBean(TaskBean taskBeanNew, List<TaskStage> stages) {
        // 根据任务类型获取对应的系统审批阶段配置项信息
        List<TempAuditPeopleBean> listAuditPeopleBean = TaskCommon.getTempAuditPeopleBeanList(taskBeanNew,
                stages);
        if (listAuditPeopleBean == null || listAuditPeopleBean.size() == 0) {
            return listAuditPeopleBean;
        }
        // 获取拟稿阶段
        TempAuditPeopleBean tempPeopleBean = listAuditPeopleBean.get(0);
        if (tempPeopleBean.getState() == 0) {// 拟稿状态 设置拟稿人名称
            tempPeopleBean.setAuditName(taskBeanNew.getCreatePeopleTemporaryName());
            tempPeopleBean.setAuditId(taskBeanNew.getCreatePersonId().toString());
        }
        // 获取任务// 各阶段审批人集合 List<Object[]> 0：任务阶段；1：人员真实姓名2：人员主键ID;3:是否删除1:删除
        // select T_P.stage_Id as stageId, u.true_name as trueName, u.people_id
        // as peopleId, u.ISLOCK as isLock
        List<Map<String, Object>> approveList = taskMapper.fetchApproceStateAndPeople(taskBeanNew.getTaskId());

        for (Map<String, Object> approveMap : approveList) {
            int stageId = 0;
            if (approveMap.get("STAGEID") != null) {
                stageId = Integer.valueOf(approveMap.get("STAGEID").toString());
            }

            String userId = "";
            if (approveMap.get("PEOPLEID") != null) {
                userId = approveMap.get("PEOPLEID").toString();
            }

            int isLock = 0;
            if (approveMap.get("ISLOCK") != null) {
                isLock = Integer.valueOf(approveMap.get("ISLOCK").toString());
            }
            for (TempAuditPeopleBean auditPeopleBean : listAuditPeopleBean) {
                if (auditPeopleBean.getState() == 0) {// 跳过拟稿人阶段
                    continue;
                }
                if (stageId == auditPeopleBean.getStageId()) {// 多个审批人
                    String auditName = "";
                    if (approveMap.get("TRUENAME") != null) {
                        auditName = approveMap.get("TRUENAME").toString();
                    }
                    if (isLock == 1) {// 人员删除
                        // auditName + (已离职)
                        auditName = auditName + JecnProperties.getValue("resignation");
                        userId = null;
                    }
                    if (!Common.isNullOrEmtryTrim(auditPeopleBean.getAuditName())) {
                        // 审核人ID
                        auditPeopleBean.setAuditId(auditPeopleBean.getAuditId() + "," + userId);
                        // 审核人名称
                        auditPeopleBean.setAuditName(auditPeopleBean.getAuditName() + "\n" + auditName);
                    } else {
                        auditPeopleBean.setAuditName(auditName);
                        auditPeopleBean.setAuditId(userId);
                    }
                }
            }
        }
        IfytekCommon.ifytekCommonTempAuditPeopleBean(listAuditPeopleBean);
        return listAuditPeopleBean;
    }

    /**
     * 换行标识符替换
     *
     * @param str “带\n 的字符串”
     * @return String
     */
    public static String rebackReplaceString(String str) {
        if (StringUtils.isBlank(str)) {
            return str;
        }
        return str.replaceAll("\n", "<br>");
    }

    /**
     * 获取制度
     *
     * @param simpleTaskBean
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public SimpleTaskRelatedResource getRuleInfomation(SimpleTaskBean simpleTaskBean, Long historyId) {
        TaskBean taskBeanNew = simpleTaskBean.getTaskBeanNew();
        if (taskBeanNew == null) {//
            log.error("RuleTaskServiceImpl类getFileInfomation方法中参数为空!");
            throw new IllegalArgumentException("参数不能为空!");
        }
        Long ruleId = taskBeanNew.getRid();
        Rule ruleT = ruleMapper.getRuleByTRuleId(ruleId);
        // 制度基本信息
        Map<String, Object> map = new HashMap<>();
        map.put("isPub", "_T");
        map.put("id", ruleId);
        map.put("historyId", historyId);
        RuleBaseInfo baseInfo = ruleMapper.findRuleBaseInfo(map);
        if (ruleT == null) {
            log.error("RuleTaskBS类getRuleInfomation方法中制度不存在!");
            throw new IllegalArgumentException("参数不能为空!");
        }

        SimpleTaskRelatedResource relatedResouce = new SimpleTaskRelatedResource();
        // 制度阶段名称
        relatedResouce.setName(ruleT.getRuleName());
        // 制度编号
        relatedResouce.setNumber(ruleT.getRuleNumber());
        // 制度密级
        if (ruleT.getIsPublic() != null) {
            relatedResouce.setStrPublic(ruleT.getIsPublic().toString());
        }
        // 获取制度父节点
        if (ruleT.getPerId() != null) {
            RuleT preRuleT = ruleMapper.getRuleT(ruleT.getPerId());
            if (preRuleT == null) {
                throw new IllegalArgumentException("制度任务getRuleInfomation获取制度父节点异常！");
            }
            relatedResouce.setPreName(preRuleT.getRuleName());
        }

        TaskApplicationNew applicationNew = simpleTaskBean.getTaskApplicationNew();
        // 保密级别
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleTaskSecurityLevel.toString())) {
            relatedResouce.setSecretLevel(ruleT.getConfidentialityLevelStr());
            applicationNew.setIsSecurityLevel(1);
        }
        if (baseInfo != null) {
            if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleTaskPurpose.toString())) {
                relatedResouce.setPurpose(Common.replaceWarp(baseInfo.getPurpose()));
                applicationNew.setIsPurpose(1);
            }
            if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleTaskApplicability.toString())) {
                relatedResouce.setApplicability(Common.replaceWarp(baseInfo.getApplicability()));
                applicationNew.setIsApplicability(1);
            }
        }
        // 类别
        if (ConfigUtils.valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.ruleTaskType.toString())) {
            relatedResouce.setTypeName(orgMapper.findTypeNameById(ruleT.getTypeId()));
            applicationNew.setFileType(1);
        }

        // 制度名称设置连接
        relatedResouce.setLink(CommonServiceUtil.getRuleNameLink(ruleT, simpleTaskBean.getIsApp()));

        return relatedResouce;
    }


    /**
     * 获取文件信息
     *
     * @param historyId
     * @param simpleTaskBean
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public SimpleTaskRelatedResource getFileInfomation(SimpleTaskBean simpleTaskBean, Long historyId) {
        TaskBean taskBeanNew = simpleTaskBean.getTaskBeanNew();
        if (taskBeanNew == null) {//
            log.error("FileTaskServiceImpl类getFileInfomation方法中参数为空!");
            throw new IllegalArgumentException("参数不能为空!");
        }
        Long fileId = taskBeanNew.getRid();
        FileBeanT fileBeanT = null;

        if (historyId == 0 || historyId == null) {
            // 文件信息
            fileBeanT = fileMapper.getFileBeanT(fileId);
        } else {
            //文控
            Map<String, Object> map = new HashedMap();
            map.put("fileId", fileId);
            map.put("historyId", historyId);
            fileBeanT = fileMapper.getFileBeanHistory(map);
        }
        if (fileBeanT == null) {
            log.error("FileTaskServiceImpl类getFlowInfomation方法中文件不存在!");
            return null;
        }

        SimpleTaskRelatedResource relatedResouce = new SimpleTaskRelatedResource();
        if (fileBeanT.getPerFileId() != null) {// 是否存在父节点。存在获取父节点名称
            FileBeanT preFileBeanT = fileMapper.getFileBeanT(fileBeanT.getPerFileId());
            // PRF父节点名称
            relatedResouce.setPreName(preFileBeanT.getFileName());
        }
        // 当前节点名称
        relatedResouce.setName(fileBeanT.getFileName());
        // 设置密级
        if (fileBeanT.getIsPublic() != null) {
            relatedResouce.setStrPublic(fileBeanT.getIsPublic().toString());
        }
        // 设置编号
        relatedResouce.setNumber(fileBeanT.getDocId());

        relatedResouce.setLink(CommonServiceUtil.getFileNameLink(fileBeanT, simpleTaskBean.getIsApp()));
        return relatedResouce;
    }


    public SimpleTaskRelatedResource getFlowInfomation(SimpleTaskBean simpleTaskBean, Long historyId) {
        TaskBean taskBeanNew = simpleTaskBean.getTaskBeanNew();
        if (taskBeanNew == null) {//
            log.error("FlowTaskServiceImpl类getFlowInfomation方法中参数为空!");
            // 访问出错
            throw new IllegalArgumentException(
                    "getFlowInfomation(SimpleTaskBean simpleTaskBean) +  taskBeanNew == null");
        }
        Long flowId = taskBeanNew.getRid();
        SimpleTaskRelatedResource relatedResouce = null;
        if (historyId == 0 || historyId == null) {
            relatedResouce = findProcess(simpleTaskBean, taskBeanNew, flowId, historyId); //文控Id为空查询
        } else {
            relatedResouce = findProcessHistory(simpleTaskBean, taskBeanNew, flowId, historyId); //文控不为空查询历史记录
        }

        return relatedResouce;
    }

    //-----
    private SimpleTaskRelatedResource findProcess(SimpleTaskBean simpleTaskBean, TaskBean taskBeanNew, Long flowId, Long historyId) {
        // 流程属性信息
        FlowBasicInfoT flowBasicInfoT = null;
        // 流程地图属性信息
        MainFlowT mainFlowT = null;
        FlowStructureT flowStructureT = null;
        // 获取流程信息
        flowStructureT = processMapper.getFlowStructureT(flowId);
        if (taskBeanNew.getTaskType() == 0) {// 流程任务
            // 流程属性信息
            flowBasicInfoT = processMapper.getProcessBasicInfoT(flowId);
        } else if (taskBeanNew.getTaskType() == 4) {
            // 流程地图属性信息
            mainFlowT = processMapper.getMainFlowT(flowId);
        }

        // 流程不存在
        if (flowStructureT == null) {
            log.error("FlowTaskServiceImpl类获取流程信息参数不能为空! FlowStructureT == null");
            throw new IllegalArgumentException("FlowTaskServiceImpl类获取流程信息参数不能为空! FlowStructureT == null");
        }

        return getFlowResource(flowBasicInfoT, simpleTaskBean, flowStructureT, mainFlowT, historyId);
    }

    private SimpleTaskRelatedResource getFlowResource(FlowBasicInfoT flowBasicInfoT, SimpleTaskBean simpleTaskBean,
                                                      FlowStructureT flowStructureT, MainFlowT mainFlowT, Long historyId) {
        SimpleTaskRelatedResource relatedResource = new SimpleTaskRelatedResource();
        // PRF类别
        relatedResource.setListTypeBean(getProceeRuleTypeBean(simpleTaskBean));
        // 流程名称
        relatedResource.setName(flowStructureT.getFlowName());
        // 流程编号
        relatedResource.setNumber(flowStructureT.getFlowIdInput());

        relatedResource.setStrPublic(flowStructureT.getIsPublic() == null ? null : flowStructureT.getIsPublic().toString());
        // 获取流程地图信息
        if (flowStructureT.getPerFlowId() != null) {
            FlowStructureT perFlowStructureT = processMapper.getFlowStructureT(flowStructureT.getPerFlowId());
            if (perFlowStructureT != null) {
                relatedResource.setPreName(perFlowStructureT.getFlowName());// 业务域名称
            }
        }
        String notGlossary = null;
        if (flowBasicInfoT != null) {
            notGlossary = flowBasicInfoT.getNoutGlossary();
            // 获取流程类别
            if (simpleTaskBean.getTaskApplicationNew().getFileType() != 0 && flowBasicInfoT.getTypeId() != null) {// 显示文件类别
                relatedResource.setTypeId(flowBasicInfoT.getTypeId().toString());
            }
            relatedResource.setPurpose(Common.replaceWarp(flowBasicInfoT.getFlowPurpose()));
            relatedResource.setApplicability(Common.replaceWarp(flowBasicInfoT.getApplicability()));
        } else if (mainFlowT != null) {
            notGlossary = mainFlowT.getFlowNounDefine();
        }
        if (!Common.isNullOrEmtryTrim(notGlossary)) {
            // 术语定义 页面显示
            relatedResource.setDefinitionStr(Common.replaceWarp(notGlossary));
        }
        // 任务中，文件或图标 link
        ProcessLink processLink = CommonServiceUtil.getProcessNameLink(flowStructureT, simpleTaskBean.getIsApp(), historyId);
        if (processLink == null) {
            return relatedResource;
        }
        relatedResource.setLink(processLink.getDocLink());
        if (flowStructureT.getNodeType() == 0) { //判断节点类型 为流程文件时候 不显示 流程图链接
            relatedResource.setLinkImage(processLink.getImageLink());
        }
        return relatedResource;
    }

    //----- 查询文控
    private SimpleTaskRelatedResource findProcessHistory(SimpleTaskBean simpleTaskBean, TaskBean taskBeanNew, Long flowId, Long historyId) {
        // 流程属性信息
        FlowBasicInfoT flowBasicInfoT = null;
        // 流程地图属性信息
        MainFlowT mainFlowT = null;
        Map<String, Object> map = new HashMap<>();
        map.put("flowId", flowId);
        map.put("historyId", historyId);
        // 获取流程信息
        FlowStructureT flowStructureT = processMapper.getFlowStructureH(map);
        if (taskBeanNew.getTaskType() == 0) {// 流程任务
            // 流程属性信息
            flowBasicInfoT = processMapper.getProcessBasicInfoH(map);
        } else if (taskBeanNew.getTaskType() == 4) {
            // 流程地图属性信息
            mainFlowT = processMapper.getMainFlowH(map);
        }
        // 流程不存在
        if (flowStructureT == null) {
            log.error("FlowTaskServiceImpl类获取流程信息参数不能为空! FlowStructureT == null");
            throw new IllegalArgumentException("FlowTaskServiceImpl类获取流程信息参数不能为空! FlowStructureT == null");
        }
        return getFlowResource(flowBasicInfoT, simpleTaskBean, flowStructureT, mainFlowT, historyId);
    }


    /**
     * 获取文件类别ID对应的类别名称
     *
     * @param typeId
     * @param listTypeBean
     * @return String 类别名称
     */
    protected String getTypeName(Long typeId, List<ProceeRuleTypeBean> listTypeBean) {
        for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
            if (typeId.toString().equals(proceeRuleTypeBean.getTypeId().toString())) {
                return proceeRuleTypeBean.getTypeName();
            }
        }
        return null;
    }

    /**
     * 设置类别
     *
     * @param simpleTaskBean
     */
    protected List<ProceeRuleTypeBean> getProceeRuleTypeBean(SimpleTaskBean simpleTaskBean) {
        if (simpleTaskBean == null) {
            return null;
        }
        TaskBean taskBeanNew = simpleTaskBean.getTaskBeanNew();
        if (taskBeanNew == null) {
            return null;
        }
        List<ProceeRuleTypeBean> listTypeBean = new ArrayList<ProceeRuleTypeBean>();
        // 当前审批阶段为文控阶段或部门阶段
        if (simpleTaskBean.getTaskApplicationNew().getFileType() != 0) {

            ProceeRuleTypeBean bean = new ProceeRuleTypeBean();
            bean.setTypeId(-1L);
            bean.setTypeName("");
            listTypeBean.add(bean);
            listTypeBean.addAll(getProceeRuleTypeBean());
        }
        // 添加类别
        return listTypeBean;
    }

    /**
     * 获取PRF；类别集合
     *
     * @return
     */
    private List<ProceeRuleTypeBean> getProceeRuleTypeBean() {
        return processMapper.fetchProcessRuleTyps();
    }

    /**
     * 设置PRF文件查阅权限
     *
     * @param simpleTaskBean
     * @param relatedResource
     */
    private void initAccessPermissions(SimpleTaskBean simpleTaskBean, SimpleTaskRelatedResource relatedResource) {
        int accessType = -1;
        switch (simpleTaskBean.getTaskBeanNew().getTaskType()) {
            case 0:
            case 4:
                accessType = 0;
                break;
            case 1:// 文件
                accessType = 1;
                break;
            case 2:// 制度
            case 3:
                accessType = 3;
                break;
            default:
                break;
        }
        StringBuffer orgBufIds = new StringBuffer();
        StringBuffer orgBufNames = new StringBuffer();
        Long relatedId = simpleTaskBean.getTaskBeanNew().getRid();
        // 部门权限IDs
        getOrgIds(orgBufIds, orgBufNames, relatedId, accessType);

        // 添加部门查阅权限
        relatedResource.setOrgIds(orgBufIds.toString());
        relatedResource.setOrgNames(orgBufNames.toString());

        // 岗位查阅权限ID集合
        StringBuffer posBufIds = new StringBuffer();
        StringBuffer posBufNames = new StringBuffer();
        // 岗位查阅权限
        getPosIds(posBufIds, posBufNames, relatedId, accessType);

        // 岗位部门查阅权限
        relatedResource.setPosIds(posBufIds.toString());
        relatedResource.setPosNames(posBufNames.toString());

        // 岗位组查阅权限ID集合
        StringBuffer groupBufIds = new StringBuffer();
        StringBuffer groupBufNames = new StringBuffer();
        // 岗位组查阅权限
        getGroupIds(groupBufIds, groupBufNames, relatedId, accessType);
        // 岗位组部门查阅权限
        relatedResource.setGroupIds(groupBufIds.toString());
        relatedResource.setGroupNames(groupBufNames.toString());
    }

    /**
     * 获取部门查阅权限
     *
     * @param orgBufIds   部门查阅权限IDS (','分隔)
     * @param orgBufNames 部门查阅权限NameS ('/'分隔)
     * @param relateId    关联ID
     * @param accessType  类型 0是流程，1是文件，2是标准，3是制度
     */
    private void getOrgIds(StringBuffer orgBufIds, StringBuffer orgBufNames, Long relateId, int accessType) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("relateId", relateId);
        paramMap.put("accessType", accessType);
        paramMap.put("isPub", false);
        // 部门查阅权限
        List<Map<String, Object>> orgList = permissionMapper.getOrgAccessPermissions(paramMap);
        // 部门权限IDs
        Map<String, Object> map;
        if (orgList != null && orgList.size() > 0) {
            for (int i = 0; i < orgList.size(); i++) {
                map = orgList.get(i);
                if (i == orgList.size() - 1) {
                    orgBufIds.append(map.get("ORG_ID").toString());
                    orgBufNames.append(map.get("ORG_NAME").toString());
                } else {
                    orgBufIds.append(map.get("ORG_ID").toString() + ",");
                    orgBufNames.append(map.get("ORG_NAME").toString() + STR_SPLIT);
                }
            }
        }
    }

    /**
     * 获取岗位查阅权限
     *
     * @param posBufIds   岗位查阅权限IDS (','分隔)
     * @param posBufNames 岗位查阅权限NameS ('/'分隔)
     * @param relateId    关联ID
     * @param accessType  类型 0是流程，1是文件，2是标准，3是制度
     */
    private void getPosIds(StringBuffer posBufIds, StringBuffer posBufNames, Long relateId, int accessType) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("relateId", relateId);
        paramMap.put("accessType", accessType);
        paramMap.put("isPub", false);
        // 部门查阅权限
        List<Map<String, Object>> posList = permissionMapper.getPositionAccessPermissions(paramMap);
        // 部门权限IDs
        Map<String, Object> map;
        if (posList != null && posList.size() > 0) {
            for (int i = 0; i < posList.size(); i++) {
                map = posList.get(i);
                if (i == posList.size() - 1) {
                    posBufIds.append(map.get("FIGURE_ID").toString());
                    posBufNames.append(map.get("FIGURE_TEXT").toString());
                } else {
                    posBufIds.append(map.get("FIGURE_ID").toString() + ",");
                    posBufNames.append(map.get("FIGURE_TEXT").toString() + STR_SPLIT);
                }
            }
        }
    }

    /**
     * @param groupBufIds   岗位查阅权限IDS (','分隔)
     * @param groupBufNames 岗位查阅权限NameS ('/'分隔)
     * @param relateId      关联ID
     * @param accessType    类型 0是流程，1是文件，2是标准，3是制度
     * @author xiaobo 获取岗位组查阅权限
     */
    private void getGroupIds(StringBuffer groupBufIds, StringBuffer groupBufNames, Long relateId, int accessType) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("relateId", relateId);
        paramMap.put("accessType", accessType);
        paramMap.put("isPub", false);
        // 部门查阅权限
        List<Map<String, Object>> groupList = permissionMapper.getPositionAccessPermissionsGroup(paramMap);
        // 部门权限IDs
        Map<String, Object> map;
        if (groupList != null && groupList.size() > 0) {
            for (int i = 0; i < groupList.size(); i++) {
                map = groupList.get(i);
                if (i == groupList.size() - 1) {
                    groupBufIds.append(map.get("ID").toString());
                    groupBufNames.append(map.get("NAME").toString());
                } else {
                    groupBufIds.append(map.get("ID").toString() + ",");
                    groupBufNames.append(map.get("NAME").toString() + STR_SPLIT);
                }
            }
        }
    }

    private String getDelayReason(SimpleTaskBean simpleTaskBean) {

        boolean isDelay = false;

        List<Map<String, Object>> records = taskMapper
                .fetchTaskRecordAndApproveName(simpleTaskBean.getTaskBeanNew().getTaskId().toString());

        String peopleLeave = JecnProperties.getValue("missingPeople");
        String assignedPeople = JecnProperties.getValue("operator");
        String subconcessionsPeople = JecnProperties.getValue("transferOfPeople");
        String peopleNoPos = JecnProperties.getValue("noPost");
        StringBuffer delayBuffer = new StringBuffer();
        for (Map<String, Object> recordMap : records) {
            // 阶段名
            Object stageName = recordMap.get("STAGE_NAME");
            Object approveName = recordMap.get("TRUE_NAME");
            Object figureId = recordMap.get("FIGURE_ID");
            Object islock = recordMap.get("ISLOCK");

            // 审核人姓名为空 搁置
            if (approveName == null || (islock != null && Integer.parseInt(islock.toString()) == 1)) {
                isDelay = true;
                // xxx:人员离职
                delayBuffer.append(stageName + "：" + peopleLeave + "<br/>");
            } else {
//                if (figureId == null) {
//                    if (approveName != null) {
//                        isDelay = true;
//                        String name = approveName.toString();
//                        delayBuffer.append(stageName + "：" + name + peopleNoPos + "<br/>");
//                    }
//                }
            }
        }

        // 当前任务阶段所做的操作
        int taskElseState = simpleTaskBean.getTaskBeanNew().getTaskElseState();
        boolean curPeopleIsNull = false;

        StringBuffer curStateBuf = new StringBuffer();
        // 当前任务为会审阶段或者当前阶段执行交办、转批操作时 查询当前任务操作人数据是否异常
        if (taskElseState == 2 || taskElseState == 3 || simpleTaskBean.getTaskBeanNew().getTaskState() == 3) {

            List<Map<String, Object>> curApproveList = taskMapper
                    .getTaskCurApprovePeople(simpleTaskBean.getTaskBeanNew().getTaskId());

            // 当前阶段无审批人数据
            if (curApproveList == null || curApproveList.size() == 0) {
                isDelay = true;
                curPeopleIsNull = true;
                // 交办人人员缺失！
                if (taskElseState == 2) {
                    curStateBuf.append(assignedPeople + peopleLeave + "<br/>");
                }
                // 转批人人员缺失！
                else if (taskElseState == 3) {
                    curStateBuf.append(subconcessionsPeople + peopleLeave + "<br/>");
                }
                // 当前阶段为评审人会审阶段
                else if (simpleTaskBean.getTaskBeanNew().getTaskState() == 3) {
                    if (delayBuffer.indexOf(simpleTaskBean.getTaskBeanNew().getStateMark() + "：" + peopleLeave) != -1) {
                        curPeopleIsNull = false;
                    } else {
                        curStateBuf.append(peopleLeave + "<br/>");
                    }
                }
            } else {
                // 当前阶段存在审批人 判断有没有岗位
//                for (Map<String, Object> recordMap : curApproveList) {
//                    Object approveName = recordMap.get("TRUE_NAME");
//                    Object userId = recordMap.get("USER_ID");
//                    // 姓名不为空，岗位为空
//                    if (approveName != null && userId == null) {
//                        // 如果user_id 为空说明没有岗位
//                        curPeopleIsNull = true;
//                        isDelay = true;
//                        if (taskElseState == 2) {
//                            // 交办人xxx没有岗位
//                            curStateBuf.append(assignedPeople + approveName + peopleNoPos);
//                        } else if (taskElseState == 3) {
//                            // 转批人xxx没有岗位
//                            curStateBuf.append(subconcessionsPeople + approveName + peopleNoPos);
//                        }
//                    }
//                }
            }

        }
        // 如果当前阶段审批人为空
        if (curPeopleIsNull && isDelay) {
            // 拼装错误提示
            delayBuffer.append(simpleTaskBean.getTaskBeanNew().getStateMark() + "：" + curStateBuf.toString());
        }
        return delayBuffer.toString();
    }

    private TaskBean getTaskBeanNew(Long taskId) {
        // 获取任务信息
        TaskBean taskBean = taskMapper.getTaskBean(taskId);
        if (taskBean == null) {
            throw new TaskIllegalArgumentException("任务不存在，请刷新页面重试");
        }
        // 获得需要审批的文件的名称
        /**
         * taskType=0时，表示流程ID，taskType=1时，表示文件ID，taskType=2时，表示制度模板ID,3:制度文件ID，
         * 4： 流程地图ID
         */
        long rid = taskBean.getRid();
        if (0 == taskBean.getTaskType() || 4 == taskBean.getTaskType()) {
            FlowStructureT flowStructureT = processMapper.getFlowStructureT(rid);
            taskBean.setRname(flowStructureT.getFlowName());
        } else if (1 == taskBean.getTaskType()) {
            FileBeanT FileBeanT = fileMapper.getFileBeanT(rid);
            taskBean.setRname(FileBeanT.getFileName());
        } else if (2 == taskBean.getTaskType() || 3 == taskBean.getTaskType()) {
            RuleT ruleT = ruleMapper.getRuleT(rid);
            taskBean.setRname(ruleT.getRuleName());
        }

        // 如果任务当前是完成阶段
        if (taskBean.getTaskState() == 5) {
            taskBean.setStateMark(JecnProperties.getValue("complete"));
        }

        // 创建人信息
        JecnUser user = userMapper.getUser(taskBean.getCreatePersonId());
        // 开始时间转型
        taskBean.setStartTimeStr(Common.getStringbyDate(taskBean.getStartTime()));
        // 结束时间转型
        taskBean.setEndTimeStr(Common.getStringbyDate(taskBean.getEndTime()));
        if (user != null) {
            //科大讯飞需要重新根据登录名查询真实姓名
            user.setName(IfytekCommon.getIflytekUserName(user.getLoginName(), 0));
            if (user.getIsLock() == 1) {
                // name + (已离职)
                taskBean.setCreatePeopleTemporaryName(user.getName() + JecnProperties.getValue("resignation"));
            } else {
                taskBean.setCreatePeopleTemporaryName(user.getName());
            }
            taskBean.setCreateName(user.getName());
            // 电话
            taskBean.setPhone(user.getPhoneStr());
        }

        return taskBean;

    }


    @Override
    public void taskOperation(SimpleSubmitMessage submitMessage) {

        // 任务主键ID
        Long taskId = Long.valueOf(submitMessage.getTaskId());

        // 获取任务主表信息
        TaskBean taskBean = getTaskBeanNew(taskId);

        if (taskBean.getTaskState() == 5) {
            return;
        }
        // 任务操作
        int taskElseState = Integer.valueOf(submitMessage.getTaskOperation()).intValue();
        if (taskElseState != 11 && taskElseState != 12) {
            if (taskBean.getTaskState() == 0 || taskBean.getTaskState() == 10) {// 拟稿人提交审批
                JecnUser jecnUser = userMapper.getUser(taskBean.getCreatePersonId());
                if (jecnUser == null || jecnUser.getIsLock() == 1) {// 拟稿人离职
                    throw new TaskIllegalArgumentException(TaskCommon.STAFF_CHANGE);
                }
            }
        }
        // 任务阶段信息
        List<TaskStage> taskStageList = taskMapper.fetchTaskStagesByTaskId(taskId);

        switch (taskElseState) {
            case 0:// 拟稿人提交审批
                break;
            case 1:// 通过
                if (taskBean.getIsControlauditLead() == 1 && taskBean.getTaskState() == 1) {
                    // 文控主导审核
                    controlPass(submitMessage, taskBean, taskStageList);
                } else {
                    // 拟稿人主导审核
                    approvePass(submitMessage, taskBean, taskStageList);
                }

                break;
            case 2:// 交办
                assignedTask(submitMessage, taskElseState, taskStageList);
                break;
            case 3:// 转批
                assignedTask(submitMessage, taskElseState, taskStageList);
                break;
            case 4:// 打回
                callBackTask(submitMessage, taskStageList);
                break;
            case 5:// 提交意见（提交审批意见(评审-------->拟稿人)
                submitTaskIdea(submitMessage, taskStageList);
                break;
            case 6:// 二次评审 拟稿人------>评审
                twoReviewTask(submitMessage, taskStageList);
                break;
            case 7:// 拟稿人重新提交审批
                reSubmitTask(submitMessage, taskStageList);
                break;
            case 8:// 完成
                break;
            case 9:// 打回整理意见(任务状态在评审人后的审核阶段------>拟稿人)）
                callBackArrangement(submitMessage, taskStageList);
                break;
            case 10:// 交办人提交
                assignedPeopleSubmitTask(submitMessage);
                break;
            case 11:// 编辑 (系统管理员编辑任务)
            case 12:// 编辑打回 (系统管理员编辑任务)
                editTask(submitMessage, taskBean);
                break;
            case 13:// 撤回
                reCallTask(Long.valueOf(submitMessage.getCurPeopleId()), taskBean, taskStageList);
                break;
            case 14:// 返回
                reBackTask(submitMessage, taskBean, taskStageList);
                break;
        }
    }

    /**
     * 返回操作 从当前阶段返回至上一阶段，当前阶段操作人可执行
     *
     * @param submitMessage 页面提交的信息
     * @param taskBean      任务
     * @param taskStageList 任务对应的阶段信息
     */
    private void reBackTask(SimpleSubmitMessage submitMessage, TaskBean taskBean, List<TaskStage> taskStageList) {
        if (submitMessage == null || taskBean == null) {
            throw new IllegalArgumentException("返回获取提交信息异常！");
        }
        if (StringUtils.isEmpty(submitMessage.getCurPeopleId())) {
            throw new IllegalArgumentException("参数非法，操作人不能为空！");
        }
        // 当前操作人
        long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        // 任务操作 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段
        // 7：IT总监审批阶段
        // 8:事业部经理 9：总经理 10:整理意见
        int state = taskBean.getTaskState();

        // 获取任务的前一阶段
        TaskStage taskStage = TaskCommon.getPreTaskStage(taskStageList, state);
        int upState = taskStage.getStageMark();

        if (state == 0 || state == 3 || state == 10) {// 0：拟稿人阶段3：评审阶段10:整理意见无返回操作
            return;
        }

        // 获取审批记录 当前任务审批最近一次提交记录
        TaskForRecodeNew forRecodeNew = getTaskForRecodeNew(taskBean.getTaskId());

        // 上一次操作状态 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人)
        // 6：二次评审
        // * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交
        // 11:编辑提交 12 编辑打回
        // * 13: 撤回 14: 反回
        int taskElseType = sepicalOperReBack(forRecodeNew.getTaskElseState(), upState);

        // 上次操作状态
        taskBean.setTaskState(upState);
        // 记录当前阶段名称
        taskBean.setStateMark(taskStage.getStageName());
        // 当前操作状态
        taskBean.setUpState(state);
        // 源人
        long fromPeopleId = forRecodeNew.getToPeopleId();
        taskBean.setFromPeopleId(fromPeopleId);
        // 操作 返回
        taskBean.setTaskElseState(14);
        // 操作 4拟稿人为重新提交按钮
        taskBean.setTaskElseState(taskElseType);
        // 目标人
        long toPeopleId = getApprovePeopleIdByState(taskStage.getId());

        if (toPeopleId == -1 && taskBean.getTaskState() == 10) {
            toPeopleId = taskBean.getCreatePersonId();
        }

        Set<Long> setPeopleId = new HashSet<Long>();
        setPeopleId.add(toPeopleId);

        // TODO 稳定之后删除
        // this.abstractTaskDao.update(taskBean);
        // this.abstractTaskDao.getSession().flush();
        //
        // // 目标人
        // long toPeopleId = getApprovePeopleIdByState(taskStage.getId());
        //
        // if (toPeopleId == -1 && taskBean.getTaskState() == 10) {
        // toPeopleId = taskBean.getCreatePersonId();
        // }
        //
        // Set<Long> setPeopleId = new HashSet<Long>();
        // setPeopleId.add(toPeopleId);
        //
        // // 删除当前目标人
        // deleteJecnTaskPeopleNew(curPeopleId, taskBean.getTaskId());
        // // 记录日志
        // // 获取目标人集合 并保存流程记录
        // recordTaskHandleLogs(setPeopleId, taskBean,
        // submitMessage.getOpinion(), TaskCommon.TASK_NOTHIN);
        // // 操作 4拟稿人为重新提交按钮
        // taskBean.setTaskElseState(taskElseType);
        // // 更新
        // this.abstractTaskDao.update(taskBean);
        // this.abstractTaskDao.getSession().flush();
        //
        // // 获取人员信息，发送邮件
        // sendTaskEmail(setPeopleId, taskBean);

        // 保存数据库以及发送邮件
        saveTaskHandleRecordAndSendEmail(taskBean, curPeopleId, "", TaskCommon.TASK_NOTHIN, setPeopleId);

    }

    /**
     * 根据任务的state获取对应的来源人
     *
     * @return -1 当前阶段不存在【需管理员指派阶段审批人】
     * @author weidp @date： 日期：2014-6-16 时间：上午09:29:43
     */
    private long getApprovePeopleIdByState(long stageId) {
        List<TaskApprovePeopleConn> taskPeopleConns = taskMapper.fetchTaskApprovePeopleConnByStageId(stageId);
        if (taskPeopleConns == null || taskPeopleConns.size() == 0) {
            return -1;
        }
        return taskPeopleConns.get(0).getApprovePid();
    }

    /**
     * 特殊操作处理
     *
     * @param taskElseType 最后一次提交的状态
     * @param upState      上一阶段状态
     * @return int页面对应操作
     */
    private int sepicalOperReBack(int taskElseType, int upState) {
        if (upState == 0) {// 拟稿人提交任务
            // 返回为打回操作
            taskElseType = 4;
        } else if (upState == 10) {// 整理意见
            // 设为整理意见操作
            taskElseType = 9;
        }

        if (taskElseType == 10) {
            // 页面显示为交办人的页面
            taskElseType = 2;
        }
        return taskElseType;
    }

    /**
     * 获取审批记录 当前任务审批最近一次提交记录
     *
     * @param taskId 任务主键ID
     * @return JecnTaskForRecodeNew
     */
    private TaskForRecodeNew getTaskForRecodeNew(Long taskId) {
        List<TaskForRecodeNew> list = taskMapper.fetchTaskForRecords(taskId);
        if (list.size() == 0 || list.get(0) == null) {
            return null;
        }
        return list.get(0);
    }

    /**
     * 撤回操作 （将任务从当前阶段撤回到上一阶段，只有上一阶段审批人可执行此操作）
     *
     * @param curPeopleId   当前操作人
     * @param taskBean      任务
     * @param taskStageList 任务对应的阶段信息
     */
    private void reCallTask(long curPeopleId, TaskBean taskBean, List<TaskStage> taskStageList) {
        if (curPeopleId == -1 || taskBean == null) {
            throw new NullPointerException("撤销获取提交信息异常！");
        }
        // 获取审批记录 当前任务审批最近一次提交记录
        TaskForRecodeNew forRecodeNew = getTaskForRecodeNew(taskBean.getTaskId());
        if (forRecodeNew == null) {
            return;
        }
        int taskElseType = TaskCommon.sepicalAssignReBack(forRecodeNew.getTaskElseState(), forRecodeNew.getUpState());

        if (curPeopleId != taskBean.getFromPeopleId() && curPeopleId != forRecodeNew.getFromPeopleId()) {// 当前操作人是否为上次提交源人
            throw new IllegalArgumentException(
                    "撤回操作参数非法！curPeopleId = " + curPeopleId + " taskBeanNew.getFromPeopleId()="
                            + taskBean.getFromPeopleId() + " forRecodeNew.getFromPeopleId()=" + curPeopleId);
        }
        // 操作人
        int state = taskBean.getTaskState();
        // 当前操作人为任务源人，可执行撤回操作
        taskBean.setTaskState(taskBean.getUpState());
        // 设置 当前阶段名称
        taskBean.setStateMark(TaskCommon.getCurTaskStage(taskStageList, taskBean.getTaskState()).getStageName());
        taskBean.setUpState(state);
        taskBean.setUpdateTime(new Date());
        // 13: 撤回 14: 反回
        taskBean.setTaskElseState(13);
        // 相对页面操作
        taskBean.setTaskElseState(taskElseType);
        // 更新源人
        taskBean.setFromPeopleId(forRecodeNew.getToPeopleId());
        // 谁操作，目标人就是谁
        Set<Long> setPeopleId = new HashSet<Long>();
        setPeopleId.add(curPeopleId);

        // TODO 稳定之后删除 更新任务
        // abstractTaskDao.getSession().update(taskBean);
        // abstractTaskDao.getSession().flush();
        //
        // // 删除已经审批的目标人
        // String hql = "delete from JecnTaskPeopleNew where taskId=?";
        // abstractTaskDao.execteHql(hql, taskBean.getId());
        // abstractTaskDao.getSession().flush();
        //
        // // 谁操作，目标人就是谁
        // Set<Long> setPeopleId = new HashSet<Long>();
        // setPeopleId.add(curPeopleId);
        // // 获取目标人集合 并保存流程记录
        // recordTaskHandleLogs(setPeopleId, taskBean, TaskCommon.TASK_NOTHIN,
        // TaskCommon.TASK_NOTHIN);
        //
        // // 相对页面操作
        // taskBean.setTaskElseState(taskElseType);
        // // 更新源人
        // taskBean.setFromPeopleId(forRecodeNew.getToPeopleId());
        // // 更新任务
        // abstractTaskDao.getSession().update(taskBean);
        // abstractTaskDao.getSession().flush();
        //
        // // 获取人员信息，发送邮件
        // sendTaskEmail(setPeopleId, taskBean);

        // 保存数据库以及发送邮件
        saveTaskHandleRecordAndSendEmail(taskBean, curPeopleId, "", TaskCommon.TASK_NOTHIN, setPeopleId);
    }

    /**
     * 交办人提交
     *
     * @param submitMessage 页面提交的信息
     */
    private void assignedPeopleSubmitTask(SimpleSubmitMessage submitMessage) {
        // 获取提交审批意见
        String opinion = submitMessage.getOpinion();
        // 任务主键ID
        Long taskId = Long.valueOf(submitMessage.getTaskId());
        // 当前操作人
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        /** 任务基本信息 */
        TaskBean taskBean = taskMapper.getTaskBean(taskId);
        // 目标原人 交办人提交，目标人为被交办人（执行交办操作的人）
        Long toPeopleId = taskBean.getFromPeopleId();
        // 源人是否存在是否存在
        checkUserExist(toPeopleId);

        // 更新目标人
        Set<Long> peopleList = new HashSet<Long>();
        peopleList.add(toPeopleId);

        // TODO 稳定之后删除
        // /** 更新任务基本信息 */
        // taskBean.setUpdateTime(new Date());
        // taskBean.setFromPeopleId(curPeopleId);
        // // 交办人提交
        // taskBean.setTaskElseState(10);
        // // 更新目标人
        // Set<Long> peopleList = new HashSet<Long>();
        // peopleList.add(toPeopleId);
        // // 删除当前操作目标人
        // deleteJecnTaskPeopleNew(curPeopleId, taskId);
        // // 更新目标人信息和操作人流转记录
        // recordTaskHandleLogs(peopleList, taskBean, opinion,
        // TaskCommon.TASK_NOTHIN);

        // 保存数据库
        saveTaskHandleRecord(taskBean, curPeopleId, opinion, TaskCommon.TASK_NOTHIN, peopleList);

    }

    /**
     * 打回整理意见
     *
     * @param submitMessage 页面提交的信息
     * @param taskStageList 任务对应的阶段信息
     */
    private void callBackArrangement(SimpleSubmitMessage submitMessage, List<TaskStage> taskStageList) {
        Long taskId = Long.valueOf(submitMessage.getTaskId());
        // 当前操作人
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        /** 任务基本信息 */
        TaskBean taskBean = taskMapper.getTaskBean(taskId);
        // 拟稿人是否存在
        try {
            checkUserExist(taskBean.getCreatePersonId());
        } catch (Exception e) {
            throw new TaskIllegalArgumentException("打回整理意见阶段，拟稿人不存在");
        }

        // 更新任务基本信息
        taskBean.setUpdateTime(new Date());
        taskBean.setFromPeopleId(curPeopleId);
        taskBean.setTaskElseState(9);
        taskBean.setUpState(taskBean.getTaskState());
        // 整理意见
        taskBean.setTaskState(10);
        // 设置当前阶段名称
        taskBean.setStateMark(TaskCommon.getCurTaskStage(taskStageList, taskBean.getTaskState()).getStageName());
        // 获取目标人集合
        Set<Long> setPeopleId = new HashSet<Long>();
        setPeopleId.add(taskBean.getCreatePersonId());

        // TODO 稳定之后删除
        // // 删除当前操作目标人
        // deleteJecnTaskPeopleNew(curPeopleId, taskId);
        // // 获取目标人集合
        // Set<Long> setPeopleId = new HashSet<Long>();
        // setPeopleId.add(taskBean.getCreatePersonId());
        // // 获取审批变动结果
        // String taskFixedDesc = getTaskFixedDesc(submitMessage, taskBean);
        // recordTaskHandleLogs(setPeopleId, taskBean,
        // submitMessage.getOpinion(), taskFixedDesc);
        // // 获取人员信息，发送邮件
        // if (taskBean.getCreatePersonId() != null) {
        // Set<Long> peopleList = new HashSet<Long>();
        // peopleList.add(taskBean.getCreatePersonId());
        // // 获取人员信息，发送邮件
        // sendTaskEmail(peopleList, taskBean);
        // }

        // 保存数据库以及发送邮件
        saveTaskHandleRecordAndSendEmail(taskBean, curPeopleId, "", TaskCommon.TASK_NOTHIN, setPeopleId);

    }

    /**
     * 管理员编辑任务
     *
     * @param submitMessage 页面提交的信息
     * @param taskBean      任务
     */
    private void editTask(SimpleSubmitMessage submitMessage, TaskBean taskBean) {
        if (submitMessage == null || taskBean == null || submitMessage.getListAuditPeople() == null) {
            throw new NullPointerException("编辑获取提交信息异常！");
        }
        // 获取任务主键ID
        Long taskId = taskBean.getTaskId();
        // 当前操作状态 11：提交；12：重新提交
        int curTaskElse = Integer.valueOf(submitMessage.getTaskOperation());
        // 当前操作人
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        // 更新时间
        taskBean.setUpdateTime(new Date());

        Long createPeopleId = null;
        if (submitMessage.getListAuditPeople().size() > 0) {// 获取创建人
            createPeopleId = Long.valueOf(submitMessage.getListAuditPeople().get(0).getAuditId().trim());
        }
        if (createPeopleId != null && !createPeopleId.equals(taskBean.getCreatePersonId())) {
            // 验证人员是否存在
            try {
                checkUserExist(createPeopleId);
            } catch (Exception e) {
                log.error("任务编辑替换拟稿人，拟稿人不存在", e);
                throw new TaskIllegalArgumentException("编辑任务，选择的拟稿人不存在，请重新选择");
            }

            // 更新拟稿人
            taskBean.setCreatePersonId(createPeopleId);
        }
        // 目标人集合
        Set<Long> setPeopleIds = new HashSet<Long>();
        if (curTaskElse == 11) {// 任务管理 ,编辑任务提交
            // 获取待更新目标人和审批人
            changeAuditPeople(taskId, submitMessage.getListAuditPeople(), taskBean, setPeopleIds);
            // 在转批或者交办时更新任务主表
            changeTaskFromPeopleAndTaskElseState(taskId, submitMessage.getListAuditPeople(), taskBean);
        } else if (curTaskElse == 12 || taskBean.getTaskElseState() == 4) {// 打回到拟稿人
            // 设置操作状态 缺少编辑状态
            if (createPeopleId != null) {
                int approveNoCounts = taskBean.getApproveNoCounts() + 1;
                /** 打回次数 */
                taskBean.setApproveNoCounts(approveNoCounts);
                /** 审批次数 */
                taskBean.setRevirewCounts(0);
                // 打回
                taskBean.setTaskElseState(4);
                taskBean.setUpState(taskBean.getTaskState());
                taskBean.setTaskState(0);
                setPeopleIds.add(createPeopleId);
            }
        }
        // 更新任务主表
        taskMapper.updateTaskBean(taskBean);

        // 编辑状态(不保存，更新日志用)
        taskBean.setFromPeopleId(curPeopleId);
        taskBean.setTaskElseState(11);
        // 删除目标人
        taskMapper.deletePeopleTaskPeopleNewById(taskId);

        // 获取下个阶段审批人
        if (setPeopleIds.size() > 0) {
            // 根据获取的人员主键ID集合更新日志信息及目标操作人信息
            recordTaskNextHandlePeopleAndRecordLog(setPeopleIds, taskBean, taskBean.getTaskDesc(),
                    TaskCommon.TASK_NOTHIN);
        } else {// 当前阶段无审批人,说明已删除
            log.info("当前阶段审批人不存在！");
            int sort = taskMapper.getCurTaskRecordCount(taskId) + 1;
            // 根据获取的人员主键ID更新日志信息及目标操作人信息
            // 记录当前操作人信息
            TaskForRecodeNew jecnTaskForRecodeNew = this.getJecnTaskForRecodeNew(taskBean, -1L, TaskCommon.TASK_NOTHIN,
                    sort, TaskCommon.TASK_NOTHIN);

            if (BaseSqlProvider.isOracle()) {
                taskMapper.saveTaskForRecodeNewOracle(jecnTaskForRecodeNew);
            } else {
                taskMapper.saveTaskForRecodeNewSqlServer(jecnTaskForRecodeNew);
            }

        }

    }

    /**
     * 获取待更新目标人和审批人
     *
     * @param taskId
     * @param listTempAuditPeopleBean
     */
    private void changeAuditPeople(Long taskId, List<TempAuditPeopleBean> listTempAuditPeopleBean, TaskBean taskBeanNew,
                                   Set<Long> setPeopleIds) {
        if (listTempAuditPeopleBean == null || listTempAuditPeopleBean.size() == 0 || taskId == null) {
            throw new NullPointerException("changeAuditPeople任务编辑获取待更新目标人和审批人异常！");
        }
        // 当前任务阶段
        int curState = taskBeanNew.getTaskState();
        try {
            // 判断当前是否是整理意见阶段是的话判断评审人会审是否为空
            if (curState == 10) {
                // 当前阶段为整理意见阶段
                // 所以当前阶段为整理意见这个阶段页面没有审批人的数据，后台处理的时候不会将数据加入setPeopleIds。需要自己将拟稿人作为审批人加入。
                if (curState == 10) {
                    setPeopleIds.add(taskBeanNew.getCreatePersonId());
                }
                // 是否有会审人
                boolean hasReviewPeople = false;
                for (TempAuditPeopleBean auditPeopleBean : listTempAuditPeopleBean) {
                    if (auditPeopleBean == null) {
                        continue;
                    }
                    if (auditPeopleBean.getState() == 3 && StringUtils.isNotBlank(auditPeopleBean.getAuditId())) {
                        hasReviewPeople = true;
                    }
                }
                if (!hasReviewPeople) {
                    throw new TaskIllegalArgumentException(TaskCommon.REVIEW_PEOPLE_NOT_NULL);
                }
            }
        } catch (Exception e) {
            throw e;
        }

        // 获取任务目标人集合
        List<Long> approveList = new ArrayList<Long>();
        List<Map<String, Object>> taskCurApprovePeople = taskMapper.getTaskCurApprovePeople(taskId);
        for (Map<String, Object> people : taskCurApprovePeople) {
            approveList.add(Long.valueOf(people.get("PEOPLE_ID").toString()));
        }

        long curStageId = -1;
        List<Integer> stateList = new ArrayList<Integer>();

        // 获取完整的state数据
        for (TempAuditPeopleBean auditPeopleBean : listTempAuditPeopleBean) {
            if (auditPeopleBean != null && StringUtils.isNotBlank(auditPeopleBean.getAuditId())) {
                if (auditPeopleBean.getState() == curState) {
                    curStageId = auditPeopleBean.getStageId();
                }
                // 会审
                if (auditPeopleBean.getState() == 3) {
                    stateList.add(10);
                }
                stateList.add(auditPeopleBean.getState());
            }
        }

        // 更新此次提交的state的isSelectedUser为已选择状态
        if (stateList.size() > 0) {
            StringBuffer strBuf = new StringBuffer();
            for (Integer state : stateList) {
                strBuf.append(state + ",");
            }
            String stateStr = strBuf.substring(0, strBuf.length() - 1);
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("taskId", taskId);
            paramMap.put("stateStr", stateStr);
            taskMapper.updateTaskStageSelectUserTrue(paramMap);
            taskMapper.updateTaskStageSelectUserFalse(paramMap);

        }

        // 获取当前阶段审批人
        List<TaskApprovePeopleConn> conns = taskMapper.fetchTaskApprovePeopleConnByStageId(curStageId);
        List<Long> curAppIdList = new ArrayList<Long>();
        for (TaskApprovePeopleConn conn : conns) {
            curAppIdList.add(conn.getApprovePid());
        }

        taskMapper.deleteJecnTaskPeopleConnByTaskId(taskId);

        // 修改后当前阶段审批人
        List<Long> editAuditPeople = new ArrayList<Long>();
        /** 当前审核阶段是否存在审核人 true:不存在；false 存在 */
        boolean isCurAudit = false;
        for (TempAuditPeopleBean auditPeopleBean : listTempAuditPeopleBean) {
            if (auditPeopleBean == null) {
                continue;
            }
            String auditId = auditPeopleBean.getAuditId();
            if (StringUtils.isNotBlank(auditId)) {// 当前审批阶段不存在审核人
                if (auditPeopleBean.getIsEmpty() == 1) {// 必填项不存在值
                    // 当前审批阶段审批人不能为空!
                    throw new TaskIllegalArgumentException(TaskCommon.CUR_APP_PEOPLE_NOT_NULL);
                } else if (curState == auditPeopleBean.getState()) {
                    // 当前阶段为非必填，编辑后删除当前审批阶段
                    isCurAudit = true;
                    continue;
                } else {
                    continue;
                }
            }
            String[] str = auditId.split(",");
            for (int i = 0; i < str.length; i++) {
                TaskApprovePeopleConn approvePeopleConn = new TaskApprovePeopleConn();
                approvePeopleConn.setApprovePid(Long.valueOf(str[i].trim()));
                approvePeopleConn.setStageId(auditPeopleBean.getStageId());

                if (BaseSqlProvider.isOracle()) {
                    taskMapper.saveTaskPeopleConnOracle(approvePeopleConn);
                } else {
                    taskMapper.saveTaskPeopleConnSqlServer(approvePeopleConn);
                }

                if (curState == auditPeopleBean.getState()) {
                    editAuditPeople.add(approvePeopleConn.getApprovePid());
                }
            }
        }

        if (isCurAudit) {// 当前阶段为非必填，编辑后删除当前审批阶段
            getNextAuditPeople(taskBeanNew, listTempAuditPeopleBean, setPeopleIds);
        } else if (editAuditPeople.size() >= 1 && taskBeanNew.getTaskState() == 3) {// 验证当前阶段审核人是否还能继续审批
            // 去掉已审批的记录 (编辑前审批人 集合 - 目标人集合 = 已审批的人集合) 3
            curAppIdList.removeAll(approveList);

            // 已经审批的人在编辑后当前的审批阶段存在的记录
            List<Long> sameList = new ArrayList<Long>();
            for (Long long1 : editAuditPeople) {
                for (Long long2 : curAppIdList) {
                    if (long1.equals(long2)) {
                        sameList.add(long1);
                    }
                }
            }
            // 去掉编辑后已审批的人 3 /5/admin
            editAuditPeople.removeAll(sameList);

            // 删除不存在的目标人集合
            taskMapper.deletePeopleTaskPeopleNewById(taskId);

            setPeopleIds.addAll(editAuditPeople);
            if (setPeopleIds.size() == 0) {// 评审人不存在，当前阶段未审批完成，跳转到下一个审批阶段
                // 获得下一级审批人
                this.getNextPeoples(taskBeanNew, setPeopleIds);
            }
        } else {
            setPeopleIds.addAll(editAuditPeople);
        }
    }

    /**
     * 编辑评审人，评审阶段无审批人，获取评审下一阶段-》整理意见阶段的
     *
     * @param taskBeanNew
     * @param setUserId
     */
    private void getNextPeoples(TaskBean taskBeanNew, Set<Long> setUserId) {
        // 整理意见阶段
        taskBeanNew.setTaskState(10);

        taskBeanNew.setUpState(3);
        // 获得任务审批操作人 整理意见阶段审批人默认为任务创建人
        setUserId.add(taskBeanNew.getCreatePersonId());
    }

    /**
     * 编辑后删除当前审批阶段,获取下一阶段审批人(可填可不填情况)
     */
    private void getNextAuditPeople(TaskBean taskBeanNew, List<TempAuditPeopleBean> listTempAuditPeopleBean,
                                    Set<Long> setPeopleIds) {
        if (taskBeanNew == null || listTempAuditPeopleBean == null) {
            return;
        }
        // 当前任务阶段
        int curState = taskBeanNew.getTaskState();
        // 是否存在当前审批阶段
        boolean curAuditState = false;
        // 获取下一阶段审批人
        int nextState = -1;
        for (TempAuditPeopleBean tempAuditPeopleBean : listTempAuditPeopleBean) {
            if (tempAuditPeopleBean == null) {
                continue;
            }
            if (curState == tempAuditPeopleBean.getState()) {//
                curAuditState = true;
            } else if (curAuditState) {// 如果
                int state = tempAuditPeopleBean.getState();
                // 更新审批状态
                taskBeanNew.setTaskState(state);
                String auditIds = tempAuditPeopleBean.getAuditId();
                if (StringUtils.isNotBlank(auditIds)) {// 如果存在审核人，则当前阶段为当前审批阶段
                    // 获取split‘，’ 后setPeopleIds集合
                    getSplitAudits(setPeopleIds, auditIds);
                    nextState = state;
                    break;
                } else {
                    nextState = -1;
                }
            }
        }
        if (nextState == -1) {// 找不到下一级审批人，任务审批完成
            // 更新审批状态
            taskBeanNew.setTaskState(5);
        }
    }

    /**
     * 获取split‘，’ 后setPeopleIds集合
     *
     * @param setPeopleIds 返回解析后人员ID集合
     * @param auditIds     ‘，’ 拼装的ID字符串
     */
    private void getSplitAudits(Set<Long> setPeopleIds, String auditIds) {
        if (setPeopleIds == null || StringUtils.isBlank(auditIds)) {
            return;
        }
        String[] strArray = auditIds.split(",");
        for (String string : strArray) {
            setPeopleIds.add(Long.valueOf(string.trim()));
        }
    }

    /**
     * 当taskElseState为转批或者交办时更新fromPeopleId和将taskElseState 改为通过
     *
     * @param taskId                  任务id
     * @param listTempAuditPeopleBean 页面提交过来的数据
     * @param taskBeanNew             任务信息
     * @author user huoyl
     */
    private void changeTaskFromPeopleAndTaskElseState(Long taskId, List<TempAuditPeopleBean> listTempAuditPeopleBean,
                                                      TaskBean taskBeanNew) {
        // 操作
        int taskElseState = taskBeanNew.getTaskElseState();
        // 当前阶段
        int curState = taskBeanNew.getTaskState();
        // 操作如果是转批或者交办
        if (taskElseState == 2 || taskElseState == 3) {
            List<TempAuditPeopleBean> listAudit = new ArrayList<TempAuditPeopleBean>();
            // 获得上个阶段的审批人
            for (TempAuditPeopleBean auditPeopleBean : listTempAuditPeopleBean) {
                if (auditPeopleBean == null) {
                    continue;
                }

                if (auditPeopleBean.getState() == curState) {
                    break;
                }
                listAudit.add(auditPeopleBean);

            }
            if (listAudit.size() > 0) {
                // 由于listAudit是当前阶段之前的阶段的集合，是按照审批顺序排好序的。所以取最后一条。
                TempAuditPeopleBean tempAuditPeopleBean = listAudit.get(listAudit.size() - 1);
                String auditId = tempAuditPeopleBean.getAuditId();
                if (StringUtils.isBlank(auditId)) {
                    log.error("管理员编辑任务，操作室转批或者交办的时候上个阶段的审批人为空");
                    return;
                }
                // 上个阶段是会审阶段，审批人员按照逗号分隔
                if (tempAuditPeopleBean.getState() == 3) {
                    auditId = auditId.split(",")[0];
                }

                // 获得上个阶段操作人id
                // 当评审人会审阶段时审批人为多个所以取其中一个就行（会审人员没有撤回操作）其他阶段审批人只有一个
                taskElseState = 1;
                // String sql = "update jecn_task_bean_new set
                // from_person_id=?,task_else_state=? where id=? ";
                // abstractTaskDao.execteNative(sql, auditId, taskElseState,
                // taskId);
                TaskBean taskBean = taskMapper.getTaskBean(taskId);
                taskBean.setFromPeopleId(Long.valueOf(auditId));
                taskBean.setTaskElseState(taskElseState);
                taskBean.setTaskId(taskId);
                taskMapper.updateTaskBean(taskBean);
            }
        }
    }

    /**
     * 拟稿人重新提交
     *
     * @param submitMessage 页面提交的信息
     * @param taskStageList 任务对应的阶段信息
     */
    protected void reSubmitTask(SimpleSubmitMessage submitMessage, List<TaskStage> taskStageList) {
        if (submitMessage.getListAuditPeople() == null) {
            throw new TaskIllegalArgumentException("拟稿人重新提交各阶段审批人为空！");
        }
        // 任务主键ID
        Long taskId = Long.valueOf(submitMessage.getTaskId());
        /** 任务基本信息 */
        TaskBean TaskBean = taskMapper.getTaskBean(taskId);
        // 任务重新提交
        reSubmitMethodCommon(submitMessage, TaskBean, taskStageList);
    }

    /**
     * PRF任务重新提交
     *
     * @param submitMessage 重新提交信息
     * @param taskBeanNew   任务主表信息
     */
    private void reSubmitMethodCommon(SimpleSubmitMessage submitMessage, TaskBean taskBeanNew,
                                      List<TaskStage> taskStageList) {
        if (submitMessage.getListAuditPeople() == null || submitMessage.getListAuditPeople().size() <= 1) {
            throw new IllegalArgumentException("页面提交的任务的审批阶段为空或者少于2");
        }
        Long taskId = taskBeanNew.getTaskId();
        /** 更新时间 */
        taskBeanNew.setUpdateTime(new Date());
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId().trim());
        taskBeanNew.setFromPeopleId(curPeopleId);
        taskBeanNew.setUpState(0);
        if (submitMessage.getEndTime() != null) {
            taskBeanNew.setEndTime(submitMessage.getEndTime());
        }
        // 拟稿下的第一个阶段审批人
        TempAuditPeopleBean auditPeople = getReSubmitState(submitMessage.getListAuditPeople());
        if (auditPeople == null) {
            throw new NullPointerException("重新提交获取当前审批阶段审批人ID异常！reSubmitMethodCommon");
        }

        // 获取下个审批阶段(拟稿人的下一个阶段)
        int nextState = auditPeople.getState();
        // 获取目标人集合
        Set<Long> peopleSet = new HashSet<Long>();

        // 解析字符串数组"," 逗号为分隔符
        String[] strIds = auditPeople.getAuditId().split(",");
        for (String string : strIds) {
            if (StringUtils.isBlank(string)) {
                throw new NullPointerException("重新提交获取当前审批阶段审批人ID异常！reSubmitMethodCommon");
            }
            peopleSet.add(Long.valueOf(string.trim()));
        }
        if (peopleSet.size() == 0) {
            throw new TaskIllegalArgumentException("拟稿人重新提交，审批人为空！");
        }
        // 设置任务状态
        taskBeanNew.setTaskState(nextState);
        // 设置当前任务阶段名称
        // 设置操作状态 重新提交
        taskBeanNew.setStateMark(TaskCommon.getCurTaskStage(taskStageList, taskBeanNew.getTaskState()).getStageName());
        taskBeanNew.setTaskElseState(7);
        // 更新变更说明
        taskBeanNew.setTaskDesc(submitMessage.getTaskDesc());

        // 删除上次存在的各阶段审批人
        // abstractTaskDao.deleteJecnTaskPeopleConnByTaskId(taskId);
        taskMapper.deleteJecnTaskPeopleConnByTaskId(taskId);
        // JecnTaskStage的stage_mark的集合
        Set<Integer> stateSet = new HashSet<Integer>();
        // 重新设置各阶段审批人
        for (TempAuditPeopleBean auditPeopleBean : submitMessage.getListAuditPeople()) {
            if (auditPeopleBean == null || StringUtils.isBlank(auditPeopleBean.getAuditId())) {// 验证人员是否存在
                // 拟稿人不读取，数组0为空
                continue;
            }
            stateSet.add(auditPeopleBean.getState());
            TaskApprovePeopleConn taskApprovePeopleConn = null;
            String[] strAuditIds = auditPeopleBean.getAuditId().split(",");
            for (int i = 0; i < strAuditIds.length; i++) {
                taskApprovePeopleConn = new TaskApprovePeopleConn();
                String str = strAuditIds[i];
                taskApprovePeopleConn.setApprovePid(Long.valueOf(str.trim()));
                taskApprovePeopleConn.setStageId(auditPeopleBean.getStageId());
                // abstractTaskDao.getSession().save(taskApprovePeopleConn);
                if (BaseSqlProvider.isOracle()) {
                    taskMapper.saveTaskPeopleConnOracle(taskApprovePeopleConn);
                } else {
                    taskMapper.saveTaskPeopleConnSqlServer(taskApprovePeopleConn);
                }
            }
        }

        StringBuffer stageMarkBuf = new StringBuffer();
        for (Integer stageMark : stateSet) {
            if (stageMark == 3) {
                stageMarkBuf.append(10 + ",");
            }
            stageMarkBuf.append(stageMark + ",");
        }

        String stageMarks = stageMarkBuf.substring(0, stageMarkBuf.length() - 1);

        // 更新JecnTaskStage的isSelectedUser 为1
        // String hql="update JecnTaskStage set isSelectedUser=1 where stageMark
        // in ("+stageMarks+") and taskId=? " ;
        // abstractTaskDao.execteHql(hql, taskId);
        // hql="update JecnTaskStage set isSelectedUser=0 where stageMark not in
        // ("+stageMarks+") and taskId=?" ;
        // abstractTaskDao.execteHql(hql, taskId);
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("taskId", taskId);
        paramMap.put("stateStr", "(" + stageMarks + ")");
        paramMap.put("peopleId", curPeopleId);
        taskMapper.updateTaskStageSelectUserTrue(paramMap);
        taskMapper.updateTaskStageSelectUserFalse(paramMap);

        // // 删除当前阶任务操作人
        // deleteJecnTaskPeopleNew(curPeopleId, taskId);
        // // 获取目标人集合 并保存流程记录
        // updateToPeopleAndForRecord(peopleList, taskBeanNew,
        // submitMessage.getOpinion(), TaskCommon.TASK_NOTHIN);
        taskMapper.deleteTaskPeopleNewByTaskAndPeopleId(paramMap);
        recordTaskNextHandlePeopleAndRecordLog(peopleSet, taskBeanNew, submitMessage.getOpinion(),
                TaskCommon.TASK_NOTHIN);

        // 获取人员信息，发送邮件
        if (peopleSet.size() > 0) {
            sendTaskEmail(peopleSet, taskBeanNew);
        }
    }

    /**
     * 重新提交获取第一阶段
     *
     * @param listAuditPeople
     * @return int 审批状态
     */
    private TempAuditPeopleBean getReSubmitState(List<TempAuditPeopleBean> listAuditPeople) {
        if (listAuditPeople == null) {
            throw new NullPointerException("重新提交获取第一阶段异常！getReSubmitState");
        }
        for (TempAuditPeopleBean tempAuditPeopleBean : listAuditPeople) {
            if (tempAuditPeopleBean == null || tempAuditPeopleBean.getState() == 0) {
                continue;
            }
            if (tempAuditPeopleBean.getIsEmpty() == 1 && StringUtils.isBlank(tempAuditPeopleBean.getAuditId())) {// 必填项，切不存在值
                // 异常！
                throw new NullPointerException("重新提交获取第一阶段中必填项不存在审批人！getReSubmitState");
            } else if (StringUtils.isNotBlank(tempAuditPeopleBean.getAuditId())) {
                return tempAuditPeopleBean;
            }
        }
        return null;
    }

    /**
     * 二次评审 任务返回至评审阶段
     *
     * @param submitMessage 页面提交的信息
     * @param taskStageList 任务对应的阶段信息
     */
    private void twoReviewTask(SimpleSubmitMessage submitMessage, List<TaskStage> taskStageList) {
        // 获取提交审批意见
        String opinion = submitMessage.getOpinion();
        // 二次评审人ID集合（字符串拼装）
        String newViewerIds = submitMessage.getNewViewerIds();
        if (StringUtils.isEmpty(newViewerIds)) {
            return;
        }
        // 任务主键ID
        Long taskId = Long.valueOf(submitMessage.getTaskId());
        // 当前操作人
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        Set<Long> peopleSet = new HashSet<Long>();
        // 任务信息
        TaskBean taskBean = taskMapper.getTaskBean(taskId);
        /** 审批次数 */
        int revirewCounts = taskBean.getRevirewCounts() + 1;
        taskBean.setRevirewCounts(revirewCounts);
        /** 更新任务基本信息 */
        taskBean.setUpdateTime(new Date());
        taskBean.setFromPeopleId(curPeopleId);
        taskBean.setTaskElseState(6);
        taskBean.setTaskState(3);
        // 整理意见
        taskBean.setUpState(10);

        // 获取评审阶段的阶段信息
        TaskStage taskStage = TaskCommon.getCurTaskStage(taskStageList, 3);
        // 删除任务的人员关系表中的评审人信息
        taskMapper.deleteJecnTaskPeopleConnByStageId(taskStage.getId());
        String[] strArr = newViewerIds.split(",");
        for (String str : strArr) {
            peopleSet.add(Long.valueOf(str.trim()));
            TaskApprovePeopleConn taskApprovePeopleConn = new TaskApprovePeopleConn();
            taskApprovePeopleConn.setApprovePid(Long.valueOf(str.trim()));
            taskApprovePeopleConn.setStageId(taskStage.getId());
            if (BaseSqlProvider.isOracle()) {
                taskMapper.saveTaskPeopleConnOracle(taskApprovePeopleConn);
            } else {
                taskMapper.saveTaskPeopleConnSqlServer(taskApprovePeopleConn);
            }
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("taskId", taskId);
        paramMap.put("peopleId", curPeopleId);
        paramMap.put("stateStr", "3");
        // 删除当前操作目标人
        taskMapper.deleteTaskPeopleNewByTaskAndPeopleId(paramMap);
        // 更新目标人信息和操作人流转记录
        recordTaskNextHandlePeopleAndRecordLog(peopleSet, taskBean, opinion, TaskCommon.TASK_NOTHIN);
        // 更新TaskStage 设置会审阶段的isSelectedUser设置为 1
        // String sql = "update jecn_task_stage set is_selected_user=1 where
        // stage_mark=3 and task_id=?";
        // abstractTaskDao.execteNative(sql, taskId);
        taskMapper.updateTaskStageSelectUserTrue(paramMap);
        // 设置阶段名称 用于邮件发送
        taskBean.setStateMark(TaskCommon.getCurTaskStage(taskStageList, taskBean.getTaskState()).getStageName());
        // 获取人员信息，发送邮件
        if (peopleSet.size() > 0) {
            // sendTaskEmail(peopleSet, taskBean);
        }

    }

    /**
     * 评审人提交意见 （存在其他评审人时继续评审，不存在时跳转至整理意见阶段）
     *
     * @param submitMessage 页面提交的信息
     * @param taskStageList 任务对应的阶段信息
     */
    private void submitTaskIdea(SimpleSubmitMessage submitMessage, List<TaskStage> taskStageList) {
        // 操作人ID
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        // 审批意见
        String opinion = submitMessage.getOpinion();
        // 任务主键ID
        Long taskId = Long.valueOf(submitMessage.getTaskId());
        // 审批变动
        String taskFixedDesc = TaskCommon.TASK_NOTHIN;
        /** 任务基本信息 */
        TaskBean taskBean = taskMapper.getTaskBean(taskId);
        // 获取目标人集合
        List<TaskPeopleNew> listPeopleNew = taskMapper.fetchArrpovePeople(taskId);
        if (listPeopleNew.size() == 1) {// 只剩下一个评审人，提交后应到下一审批环节
            /** 更新任务基本信息 */
            taskBean.setUpdateTime(new Date());
            taskBean.setFromPeopleId(curPeopleId);
            // 评审人提交意见操作状态
            taskBean.setTaskElseState(5);
            // 评审次数
            if (taskBean.getRevirewCounts() != 2) {
                // 评审次数
                taskBean.setRevirewCounts(1);
            }
            // 设置上次任务阶段
            taskBean.setUpState(taskBean.getTaskState());
            // 下一个阶段为拟稿人 整理意见
            taskBean.setTaskState(10);
            // 设置阶段名称 用于邮件发送
            taskBean.setStateMark(TaskCommon.getCurTaskStage(taskStageList, taskBean.getTaskState()).getStageName());
            // 获取邮件发送的插进入
            Set<Long> peopleSet = new HashSet<Long>();
            peopleSet.add(taskBean.getCreatePersonId());

            // TODO 稳定之后删除
            // // 删除当前操作目标人
            // deleteJecnTaskPeopleNew(curPeopleId, taskId);
            // // 获取目标人集合
            // Set<Long> setPeopleId = new HashSet<Long>();
            // setPeopleId.add(taskBean.getCreatePersonId());
            // recordTaskHandleLogs(setPeopleId, taskBean, opinion,
            // taskFixedDesc);
            // // 获取邮件发送的插进入
            // Set<Long> peopleSet = new HashSet<Long>();
            // peopleSet.add(taskBean.getCreatePersonId());
            // // 获取人员信息，发送邮件
            // sendTaskEmail(peopleSet, taskBean);

            saveTaskHandleRecordAndSendEmail(taskBean, curPeopleId, opinion, taskFixedDesc, peopleSet);

        } else {// 存在其他审批人
            /** 更新任务基本信息 */
            taskBean.setUpdateTime(new Date());
            // 设置上次任务阶段
            taskBean.setUpState(taskBean.getTaskState());
            // 评审提交审批
            taskBean.setTaskElseState(5);
            taskBean.setFromPeopleId(curPeopleId);

            // TODO 稳定之后删除
            // // 更新当前任务
            // abstractTaskDao.update(taskBean);
            // // 删除当前操作目标人
            // deleteJecnTaskPeopleNew(curPeopleId, taskId);
            // taskBean.setFromPeopleId(curPeopleId);
            // // 记录排序号 NOTHING:无变动
            // int sort = abstractTaskDao.getCurTaskRecordCount(taskId) + 1;
            // // 获取任务日志信息
            // JecnTaskForRecodeNew jecnTaskForRecodeNew =
            // this.getJecnTaskForRecodeNew(taskBean,
            // taskBean.getCreatePersonId(), opinion, sort,
            // TaskCommon.TASK_NOTHIN);
            // abstractTaskDao.getSession().save(jecnTaskForRecodeNew);

            // 更新任务
            taskMapper.updateTaskBean(taskBean);
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("taskId", taskId);
            paramMap.put("peopleId", curPeopleId);
            // 删除当前阶任务操作人
            taskMapper.deleteTaskPeopleNewByTaskAndPeopleId(paramMap);
            // 记录排序号 NOTHING:无变动
            int sort = taskMapper.getCurTaskRecordCount(taskId) + 1;
            // 获取任务日志信息
            TaskForRecodeNew jecnTaskForRecodeNew = this.getJecnTaskForRecodeNew(taskBean, taskBean.getCreatePersonId(),
                    opinion, sort, TaskCommon.TASK_NOTHIN);
            if (BaseSqlProvider.isOracle()) {
                taskMapper.saveTaskForRecodeNewOracle(jecnTaskForRecodeNew);
            } else {
                taskMapper.saveTaskForRecodeNewSqlServer(jecnTaskForRecodeNew);
            }
        }

    }

    /**
     * 打回操作
     *
     * @param submitMessage 页面提交的信息
     * @param taskStageList 任务对应的阶段信息
     */
    private void callBackTask(SimpleSubmitMessage submitMessage, List<TaskStage> taskStageList) {
        if (submitMessage == null) {
            log.error("JecnAbstractTaskBS类callBackTask方法中参数为空!");
            throw new IllegalArgumentException("参数不能为空!");
        }
        Long taskId = Long.valueOf(submitMessage.getTaskId());
        // 操作人ID
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        // 审批意见
        String opinion = submitMessage.getOpinion();
        // 审批变动
        String taskFixedDesc = TaskCommon.TASK_NOTHIN;
        /** 任务基本信息 */
        TaskBean taskBean = taskMapper.getTaskBean(taskId);
        int approveNoCounts = taskBean.getApproveNoCounts() + 1;
        /** 打回次数 */
        taskBean.setApproveNoCounts(approveNoCounts);
        /** 审批次数 */
        taskBean.setRevirewCounts(0);
        /** 更新任务基本信息 */
        taskBean.setUpdateTime(new Date());
        taskBean.setFromPeopleId(curPeopleId);
        // 打回
        taskBean.setTaskElseState(4);
        taskBean.setUpState(taskBean.getTaskState());
        taskBean.setTaskState(0);
        // 拟稿人是否存在
        try {
            checkUserExist(taskBean.getCreatePersonId());
        } catch (Exception e) {
            log.error(e);
            throw new TaskIllegalArgumentException("打回操作拟稿人不存在，请联系管理员");
        }

        // TODO 稳定之后删除
        // // 更新任务
        // taskMapper.updateTaskBean(taskBean);
        // // 设置阶段名称 用于邮件发送
        // taskBean.setStateMark(TaskCommon.getCurTaskStage(taskStageList,
        // taskBean.getTaskState()).getStageName());
        // // 删除已经审批的目标人
        // taskMapper.deletePeopleTaskPeopleNew(curPeopleId, taskId);
        //
        // /** 更新目标人 */
        // TaskPeopleNew jecnTaskPeopleNew = new TaskPeopleNew();
        // jecnTaskPeopleNew.setApprovePid(taskBean.getCreatePersonId());
        // jecnTaskPeopleNew.setTaskId(taskId);
        // taskMapper.saveTaskPeopleNew(jecnTaskPeopleNew);
        //
        // // 记录排序号
        // int sort = taskMapper.getCurTaskRecordCount(taskId) + 1;
        // // 获取审批变动结果
        // taskFixedDesc = "";
        // // 任务审批流转记录
        // TaskForRecodeNew jecnTaskForRecodeNew =
        // this.getJecnTaskForRecodeNew(taskBean, taskBean.getCreatePersonId(),
        // opinion, sort, taskFixedDesc);
        // // 保存任务流转记录
        // taskMapper.saveTaskForRecodeNew(jecnTaskForRecodeNew);
        // // 获取人员信息，发送邮件
        // if (taskBean.getCreatePersonId() != null) {
        // Set<Long> peopleList = new HashSet<Long>();
        // peopleList.add(taskBean.getCreatePersonId());
        // // 获取人员信息，发送邮件
        // sendTaskEmail(peopleList, taskBean);
        // }

        Set<Long> peopleSet = new HashSet<Long>();
        peopleSet.add(taskBean.getCreatePersonId());

        // 保存数据库以及发送邮件
        saveTaskHandleRecordAndSendEmail(taskBean, curPeopleId, opinion, taskFixedDesc, peopleSet);
    }

    /**
     * 转批或交办
     *
     * @param submitMessage 页面提交的信息
     * @param taskOperation 用户操作： 2交办 3转批
     * @param taskStageList 任务对应的阶段信息
     */
    private void assignedTask(SimpleSubmitMessage submitMessage, int taskOperation, List<TaskStage> taskStageList) {
        if (StringUtils.isEmpty(submitMessage.getCurPeopleId())
                || StringUtils.isEmpty(submitMessage.getUserAssignedId())) {
            throw new IllegalArgumentException("转批或交办处理异常！");
        }
        // 任务主键ID
        Long taskId = Long.valueOf(submitMessage.getTaskId());
        /** 任务基本信息 */
        TaskBean taskBean = taskMapper.getTaskBean(taskId);
        // 操作人ID
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        /** 通过userId集合找到peopleId */
        /** 更新任务基本信息 */
        taskBean.setUpdateTime(new Date());
        taskBean.setFromPeopleId(curPeopleId);
        taskBean.setTaskElseState(taskOperation);
        taskBean.setTaskState(taskBean.getTaskState());
        taskBean.setUpState(taskBean.getTaskState());
        // 获取任务阶段名称，用于邮件发送
        taskBean.setStateMark(TaskCommon.getCurTaskStage(taskStageList, taskBean.getTaskState()).getStageName());
        // 转批或交办处理
        // 转批人
        Long userAssignedId = Long.valueOf(submitMessage.getUserAssignedId());
        // 验证人员是否存在
        checkUserExist(userAssignedId);

        // 获取审批变动
        String taskFixedDesc = TaskCommon.TASK_NOTHIN;
        Set<Long> peopleList = new HashSet<Long>();
        peopleList.add(userAssignedId);
        // 审批意见
        String opinion = submitMessage.getOpinion();

        // TODO 稳定之后删除
        // // 记录排序号
        // int sort = taskMapper.getCurTaskRecordCount(taskId) + 1;
        // // 删除已经审批的目标人
        // taskMapper.deletePeopleTaskPeopleNew(Long.valueOf(submitMessage.getCurPeopleId()),
        // taskId);

        // // 根据获取的人员主键ID更新日志信息及目标操作人信息
        // updateTaskPeopleNew(userAssignedId, taskId, taskBean, opinion,
        // taskFixedDesc, sort);
        // // 获取人员信息，发送邮件

        // // 获取人员信息，发送邮件
        // sendTaskEmail(peopleList, taskBean);

        // 保存数据库以及发送邮件
        saveTaskHandleRecordAndSendEmail(taskBean, curPeopleId, opinion, taskFixedDesc, peopleList);
    }

    /**
     * 审批通过
     *
     * @param submitMessage 页面提交的内容
     * @param taskBean      任务
     * @param taskStageList 任务对应的阶段信息
     */
    private void approvePass(SimpleSubmitMessage submitMessage, TaskBean taskBean, List<TaskStage> taskStageList) {
        // 操作人ID
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        // 审批意见
        String opinion = submitMessage.getOpinion();
        // 审批变动
        String taskFixedDesc = TaskCommon.TASK_NOTHIN;
        // 获得下一级审批人
        Set<Long> peopleList = getNextUserIdsApprove(taskBean, taskStageList);
        if (peopleList.size() == 0) {// 审批完成 无下级审批人
            // 任务审批完成
            finishTask(submitMessage, taskBean);
        } else {// 审批通过
            /** 更新任务基本信息 */
            taskBean.setUpdateTime(new Date());
            taskBean.setFromPeopleId(curPeopleId);
            taskBean.setTaskElseState(1);
            // 获得下一阶段state
            TaskStage taskStage = TaskCommon.getNextTaskStage(taskStageList, taskBean.getTaskState());
            taskBean.setUpState(taskBean.getTaskState());
            taskBean.setTaskState(taskStage.getStageMark());
            taskBean.setStateMark(taskStage.getStageName());// 设置阶段名称，用于邮件内容
            taskFixedDesc = getTaskFixedDesc(submitMessage, taskBean);
            // 保存数据库以及发送邮件
            saveTaskHandleRecordAndSendEmail(taskBean, curPeopleId, opinion, taskFixedDesc, peopleList);
        }
    }

    /**
     * 保存数据库以及发送邮件
     *
     * @param taskBean       任务bean
     * @param curPeopleId    当前任务处理人id
     * @param opinion
     * @param taskFixedDesc
     * @param nextApproveSet 邮件下个阶段的处理人
     */
    private void saveTaskHandleRecordAndSendEmail(TaskBean taskBean, Long curPeopleId, String opinion,
                                                  String taskFixedDesc, Set<Long> nextApproveSet) {

        saveTaskHandleRecord(taskBean, curPeopleId, opinion, taskFixedDesc, nextApproveSet);

        // 获取人员信息，发送邮件
        if (nextApproveSet != null && nextApproveSet.size() > 0) {
            // 获取人员信息，发送邮件
            sendTaskEmail(nextApproveSet, taskBean);
        }
    }

    private void saveTaskHandleRecord(TaskBean taskBean, Long curPeopleId, String opinion, String taskFixedDesc,
                                      Set<Long> nextApproveSet) {
        // 更新任务
        taskMapper.updateTaskBean(taskBean);
        // 删除当前阶任务操作人
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("taskId", taskBean.getTaskId());
        paramMap.put("peopleId", curPeopleId);
        taskMapper.deleteTaskPeopleNewByTaskAndPeopleId(paramMap);
        // 将任务交给当前审批人以及记录操作日志
        recordTaskNextHandlePeopleAndRecordLog(nextApproveSet, taskBean, opinion, taskFixedDesc);

    }

    private void recordTaskNextHandlePeopleAndRecordLog(Set<Long> nextApproveSet, TaskBean taskBean, String opinion,
                                                        String taskFixedDesc) {
        if (nextApproveSet != null && nextApproveSet.size() > 0) {

            /** 记录排序号 */
            int sort = 0;
            Long taskId = taskBean.getTaskId();

            if (taskId == null) {
                sort = 1;
            } else {
                sort = taskMapper.getCurTaskRecordCount(taskId) + 1;
            }

            TaskPeopleNew jecnTaskPeopleNew = null;
            for (Long toPeopleId : nextApproveSet) {
                // 目标人是否存在
                checkUserExist(toPeopleId);
                jecnTaskPeopleNew = new TaskPeopleNew();
                jecnTaskPeopleNew.setApprovePid(toPeopleId);
                jecnTaskPeopleNew.setTaskId(taskId);
                // 保存目标人
                if (BaseSqlProvider.isOracle()) {
                    taskMapper.saveTaskPeopleNewOracle(jecnTaskPeopleNew);
                } else {
                    taskMapper.saveTaskPeopleNewSqlServer(jecnTaskPeopleNew);
                }

                // 根据获取的人员主键ID更新日志信息及目标操作人信息
                recordTaskHandleLog(toPeopleId, taskId, taskBean, opinion, taskFixedDesc, sort);
                // 日志顺序++
                sort++;

            }

        }

    }

    /**
     * 保存数据库以及发送邮件
     *
     * @param taskBean         任务bean
     * @param curPeopleId      当前任务处理人id
     * @param opinion
     * @param taskFixedDesc
     * @param recordApproveSet 邮件下个阶段的处理人
     */
    private void saveFinishTaskHandleRecord(TaskBean taskBean, Long curPeopleId, String opinion, String taskFixedDesc,
                                            Set<Long> recordApproveSet) {
        // 更新任务
        taskMapper.updateTaskBean(taskBean);
        // 删除当前阶任务操作人
        // 获取审批变动结果
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("peopleId", curPeopleId);
        paramMap.put("taskId", taskBean.getTaskId());
        taskMapper.deleteTaskPeopleNewByTaskAndPeopleId(paramMap);
        // 获取目标人集合 并保存流程记录
        recordTaskHandleLogs(recordApproveSet, taskBean, opinion, taskFixedDesc);
        // 获取人员信息，发送邮件
        if (recordApproveSet != null && recordApproveSet.size() > 0) {
            // 获取人员信息，发送邮件
            sendTaskEmail(recordApproveSet, taskBean);
        }
    }

    /**
     * 根据获取的人员主键ID集合更新日志信息及目标操作人信息
     *
     * @param setPeopleId     人员主键集合
     * @param jecnTaskBeanNew 任务主表记录
     * @param opinion         提交意见
     * @param taskFixedDesc   审批变动
     * @return Set<Long>目标操作人集合
     */
    protected void recordTaskHandleLogs(Set<Long> setPeopleId, TaskBean jecnTaskBeanNew, String opinion,
                                        String taskFixedDesc) {
        // 获取目标人集合
        if (setPeopleId == null || setPeopleId.size() == 0 || jecnTaskBeanNew == null) {
            return;
        }
        Iterator<Long> it = setPeopleId.iterator();
        /** 记录排序号 */
        int sort = 0;

        Long taskId = jecnTaskBeanNew.getTaskId();

        if (taskId == null) {
            sort = 1;
        } else {
            sort = taskMapper.getCurTaskRecordCount(taskId) + 1;
        }
        while (it.hasNext()) {
            Long peopleAssignedId = it.next();
            // 目标人是否存在
            checkUserExist(peopleAssignedId);
            // 根据获取的人员主键ID更新日志信息及目标操作人信息
            recordTaskHandleLog(peopleAssignedId, taskId, jecnTaskBeanNew, opinion, taskFixedDesc, sort);
            // 日志顺序++
            sort++;
        }

        return;
    }

    /**
     * 根据获取的人员主键ID更新日志信息及目标操作人信息
     *
     * @param toPeopleId      目标人主键ID
     * @param taskId          任务Id
     * @param jecnTaskBeanNew 任务主表记录
     * @param opinion         提交意见
     * @param taskFixedDesc   审批变动
     * @param sort            当前任务日志的数据记录
     * @return sort
     */
    private void recordTaskHandleLog(Long toPeopleId, Long taskId, TaskBean jecnTaskBeanNew, String opinion,
                                     String taskFixedDesc, int sort) {
        // 记录当前操作人信息
        TaskForRecodeNew jecnTaskForRecodeNew = getJecnTaskForRecodeNew(jecnTaskBeanNew, toPeopleId, opinion, sort,
                taskFixedDesc);

        // 保存记录表
        if (BaseSqlProvider.isOracle()) {
            taskMapper.saveTaskForRecodeNewOracle(jecnTaskForRecodeNew);
        } else {
            taskMapper.saveTaskForRecodeNewSqlServer(jecnTaskForRecodeNew);
        }
    }

    /**
     * 获得审批记录
     *
     * @param taskBean
     * @param toPeopleId
     * @param opinion
     * @param sort
     * @param taskFixedDesc
     * @return
     */
    private TaskForRecodeNew getJecnTaskForRecodeNew(TaskBean taskBean, Long toPeopleId, String opinion, int sort,
                                                     String taskFixedDesc) {
        /** 添加审批记录 */
        TaskForRecodeNew jecnTaskForRecodeNew = new TaskForRecodeNew();
        /** 驳回次数 */
        jecnTaskForRecodeNew.setApproveNoCounts(taskBean.getApproveNoCounts());
        /** 评审次数 */
        jecnTaskForRecodeNew.setRevirewCounts(taskBean.getRevirewCounts());
        /** 创建人 */
        jecnTaskForRecodeNew.setCreatePersonId(taskBean.getCreatePersonId());
        /** 创建时间 */
        jecnTaskForRecodeNew.setCreateTime(taskBean.getCreateTime());
        /** 开始时间 */
        jecnTaskForRecodeNew.setStartTime(taskBean.getStartTime());
        /** 结束时间 */
        jecnTaskForRecodeNew.setEndTime(taskBean.getEndTime());
        /** 源人(操作人) */
        jecnTaskForRecodeNew.setFromPeopleId(taskBean.getFromPeopleId());
        /** 更新时间 */
        jecnTaskForRecodeNew.setUpdateTime(taskBean.getUpdateTime());
        /** 目标人 */
        jecnTaskForRecodeNew.setToPeopleId(toPeopleId);
        /** 是否解锁 */
        jecnTaskForRecodeNew.setIsLock(taskBean.getIsLock());
        /** 说明 */
        jecnTaskForRecodeNew.setOpinion(opinion);

        jecnTaskForRecodeNew.setSortId(sort);
        /** 当前阶段 */
        jecnTaskForRecodeNew.setState(taskBean.getTaskState());
        /** 上个阶段 */
        jecnTaskForRecodeNew.setUpState(taskBean.getUpState());
        /** 当前操作状态 */
        jecnTaskForRecodeNew.setTaskElseState(taskBean.getTaskElseState());
        /** 变更说明 */
        jecnTaskForRecodeNew.setTaskFixedDesc(taskFixedDesc);
        /** 任务Id */
        jecnTaskForRecodeNew.setTaskId(taskBean.getTaskId());
        return jecnTaskForRecodeNew;
    }

    /**
     * @param peopleAssignedId
     */
    private void checkUserExist(Long peopleAssignedId) {
        if (peopleAssignedId == null) {
            log.error("JecnAbstractTaskBS类审批人为空方法中参数为空!");
            throw new IllegalArgumentException(TaskCommon.STAFF_CHANGE);
        }
        int count = userMapper.isPeopleExist(peopleAssignedId);
        if (count == 0) {
            throw new IllegalArgumentException(TaskCommon.STAFF_CHANGE);
        }
    }

    /**
     * 获得下一级审批人
     *
     * @return
     */
    private Set<Long> getNextUserIdsApprove(TaskBean taskBeanNew, List<TaskStage> taskStageList) {
        // 任务当前阶段
        int state = taskBeanNew.getTaskState();
        Set<Long> setUserId = new HashSet<Long>();

        // 任务审批带获取的下一个阶段
        long nextStageId = 0;
        TaskStage taskStage = TaskCommon.getNextTaskStage(taskStageList, state);
        if (taskStage == null) {
            return setUserId;
        }
        // 获取下个审批阶段
        nextStageId = taskStage.getId();
        List<TaskApprovePeopleConn> taskPeopleConn = taskMapper.fetchTaskApprovePeopleConnByStageId(nextStageId);
        for (TaskApprovePeopleConn people : taskPeopleConn) {
            setUserId.add(people.getApprovePid());
        }
        if (nextStageId != -1 && setUserId.size() == 0) {// 存在下一个审核阶段
            // 人员离职或岗位变更
            throw new TaskIllegalArgumentException("下阶段审人不存在，请联系管理员！");
        }
        return setUserId;
    }

    /**
     * 文控审核主导审批
     *
     * @param submitMessage
     */
    private void controlPass(SimpleSubmitMessage submitMessage, TaskBean taskBean, List<TaskStage> taskStageList) {
        if (submitMessage == null || taskBean == null) {
            throw new NullPointerException("文控审核人主导审批异常！submitMessage==null || TaskBean==null");
        }
        // 任务不存在或已删除
        if (taskBean == null || taskBean.getTaskId() == null) {
            // 任务审批异常
            throw new NullPointerException("任务不存在或者已审批");
        }
        // 文控审核人操作前的审批顺序
        int stateCount = 0;
        boolean startLookUp = false;
        // ListAuditPeople是页面提交过来的有序的审批列表，获取下个审批阶段(文控审核人的下一个阶段)
        int nextState = -1;
        if (submitMessage.getListAuditPeople() != null) {
            for (TempAuditPeopleBean auditPeopleBean : submitMessage.getListAuditPeople()) {
                if (auditPeopleBean == null || StringUtils.isBlank(auditPeopleBean.getAuditId())) {
                    continue;
                }
                if (auditPeopleBean.getState() == 1) {
                    startLookUp = true;
                    continue;
                }
                if (startLookUp) {
                    stateCount++;
                    nextState = auditPeopleBean.getState();
                    break;
                }
            }
        }

        // 不存在其他阶段审批 文控审核人直接点击审批
        if (stateCount == 0) {
            // 文控主导不存在其他审核人 任务审批完成
            finishTask(submitMessage, taskBean);
        } else {// 文控主导审批：文控审核人选择其他阶段审批人
            controlToOtherPeoples(submitMessage, nextState, taskBean, taskStageList);
        }
    }

    /**
     * 文控主导审批：文控审核人选择其他阶段审批人
     *
     * @param submitMessage
     * @param nextState
     * @param taskBean
     * @param taskStageList
     */
    private void controlToOtherPeoples(SimpleSubmitMessage submitMessage, int nextState, TaskBean taskBean,
                                       List<TaskStage> taskStageList) {
        // 获取目标人集合
        Set<Long> peopleSet = new HashSet<Long>();
        // 获取下一个阶段
        nextState = getControlNextStatePeople(nextState, submitMessage.getListAuditPeople(), taskStageList, peopleSet);
        // // 当前操作人
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());

        if (peopleSet != null && peopleSet.size() > 0) {
            // 更新时间
            taskBean.setUpdateTime(new Date());
            // 源操作人
            taskBean.setFromPeopleId(curPeopleId);
            // 操作状态 1：通过
            taskBean.setTaskElseState(1);
            // 记录上传提交状态 1:文控审核人
            taskBean.setUpState(1);
            // 更新当前状态
            taskBean.setTaskState(nextState);
            // 记录当前阶段名称
            taskBean.setStateMark(TaskCommon.getCurTaskStage(taskStageList, taskBean.getTaskState()).getStageName());

            // 设置文控主导选择的审批人
            saveJecnTaskApprovePeopleConn(submitMessage, taskBean.getTaskId(), taskStageList);

            // TODO 稳定之后删除
            // // 更新任务
            // abstractTaskDao.update(taskBean);
            // // 设置文控主导选择的审批人
            // saveJecnTaskApprovePeopleConn(submitMessage,
            // taskBean.getTaskId(), taskStageList);
            //
            // // 删除当前阶任务操作人
            // deleteJecnTaskPeopleNew(curPeopleId, taskBean.getTaskId());
            // // 获取审批变动结果
            // String taskFixedDesc = "";
            // // 更新目标人信息和操作人流转记录
            // recordTaskHandleLogs(peopleList, taskBean,
            // submitMessage.getOpinion(), taskFixedDesc);
            //
            // // 获取人员信息，发送邮件
            // if (peopleList.size() > 0) {
            // // 获取人员信息，发送邮件
            // sendTaskEmail(peopleList, taskBean);
            // }

            // 获取审批变动结果
            String taskFixedDesc = getTaskFixedDesc(submitMessage, taskBean);

            saveTaskHandleRecordAndSendEmail(taskBean, curPeopleId, submitMessage.getOpinion(), taskFixedDesc,
                    peopleSet);

        } else {
            // 文控主导未选择其他审核人（存在其他审核人阶段） 审批完成
            finishTask(submitMessage, taskBean);
        }
    }

    /**
     * 获取文控审核主导审批下一集审批人
     *
     * @return
     */
    private int getControlNextStatePeople(int nextState, List<TempAuditPeopleBean> listAuditPeople,
                                          List<TaskStage> taskStageList, Set<Long> auditPeopleIds) {
        if (taskStageList == null || taskStageList.size() == 0 || listAuditPeople == null || listAuditPeople.size() == 0
                || nextState == -1) {
            return -1;
        }
        for (TempAuditPeopleBean auditPeopleBean : listAuditPeople) {
            if (auditPeopleBean == null) {// 数组第一个为拟稿人，页面请求不返回
                continue;
            }
            if (auditPeopleBean.getAuditId() == null) {
                continue;
            }
            if (nextState == auditPeopleBean.getState()) {
                // 获取审批人人员ID集合
                auditPeopleIds.addAll(getPeopleIds(auditPeopleBean.getAuditId()));
            }
        }
        if (nextState != -1 && auditPeopleIds.size() == 0) {
            // 获取下一个阶段的Stage
            TaskStage taskStage = TaskCommon.getNextTaskStage(taskStageList, nextState);
            if (taskStage == null) {
                nextState = -1;
            } else {
                nextState = taskStage.getStageMark();
            }
            return getControlNextStatePeople(nextState, listAuditPeople, taskStageList, auditPeopleIds);
        }
        return nextState;
    }

    /**
     * 获取审批人人员ID集合
     *
     * @param strPeopleIds
     * @return
     */
    private Set<Long> getPeopleIds(String strPeopleIds) {
        if (strPeopleIds == null && "".equals(strPeopleIds)) {
            return null;
        }
        Set<Long> peopleds = new HashSet<Long>();
        String[] str = strPeopleIds.split(",");
        for (String id : str) {
            if (StringUtils.isNotBlank(id)) {
                peopleds.add(Long.valueOf(id.trim()));
            }
        }
        return peopleds;
    }

    /**
     * 设置文控主导选择的审批人
     *
     * @param taskId 任务主键ID
     */
    private void saveJecnTaskApprovePeopleConn(SimpleSubmitMessage submitMessage, Long taskId,
                                               List<TaskStage> taskStageList) {
        List<TempAuditPeopleBean> listPList = submitMessage.getListAuditPeople();
        // 删除各阶段关系人的数据
        taskMapper.deleteJecnTaskPeopleConnByTaskId(taskId);
        // 获取下一集操作人
        // 所有操作人集合
        if (listPList != null && listPList.size() > 0) {
            for (TempAuditPeopleBean auditPeopleBean : listPList) {
                if (auditPeopleBean == null || StringUtils.isEmpty(auditPeopleBean.getAuditId())) {
                    continue;
                }
                String strPeopleIds = auditPeopleBean.getAuditId();
                String[] str = strPeopleIds.split(",");
                // 获取当前阶段
                TaskStage taskStage = TaskCommon.getCurTaskStage(taskStageList, auditPeopleBean.getState());
                taskStage.setIsSelectedUser(1);// 设置已选择人
                for (String string : str) {
                    TaskApprovePeopleConn approvePeopleConn = new TaskApprovePeopleConn();
                    // 当前阶段操作人（state）
                    approvePeopleConn.setApprovePid(Long.valueOf(string.trim()));
                    // 阶段信息Id
                    approvePeopleConn.setStageId(taskStage.getId());
                    if (BaseSqlProvider.isOracle()) {
                        taskMapper.saveTaskPeopleConnOracle(approvePeopleConn);
                    } else {
                        taskMapper.saveTaskPeopleConnSqlServer(approvePeopleConn);
                    }
                }

                if (taskStage.getStageMark().intValue() == 3) {
                    // 获取整理意见Stage
                    TaskStage stage = TaskCommon.getCurTaskStage(taskStageList, 10);
                    stage.setIsSelectedUser(1);

                    TaskApprovePeopleConn jecnTaskApprovePeopleConn = new TaskApprovePeopleConn();
                    // 当前阶段操作人（state）
                    jecnTaskApprovePeopleConn.setApprovePid(Long.valueOf(listPList.get(0).getAuditId()));
                    // 阶段信息Id
                    jecnTaskApprovePeopleConn.setStageId(stage.getId());
                    taskMapper.saveTaskPeopleConnOracle(jecnTaskApprovePeopleConn);
                }
            }
        }
    }

    /**
     * 任务审批完成
     *
     * @param submitMessage 提交的基本信息
     * @param taskBean      任务详细信息
     */
    private void finishTask(SimpleSubmitMessage submitMessage, TaskBean taskBean) {
        // 操作人ID
        Long curPeopleId = Long.valueOf(submitMessage.getCurPeopleId());
        // 审批意见
        String opinion = submitMessage.getOpinion();
        // 记录当前操作
        taskBean.setUpState(taskBean.getTaskState());
        // 获取审批变动结果
        String taskFixedDesc = "";
        /** 更新任务基本信息 */
        taskBean.setUpdateTime(new Date());
        taskBean.setFromPeopleId(curPeopleId);
        taskBean.setTaskElseState(1);
        taskBean.setTaskState(5);

        Set<Long> recordPeopleSet = new HashSet<Long>();
        recordPeopleSet.add(curPeopleId);
        // 保存数据库
        saveFinishTaskHandleRecord(taskBean, curPeopleId, opinion, taskFixedDesc, recordPeopleSet);

        // TODO RMI远程调用

        // 远程调用RMI，不使用新版本的文控发布暂时注掉
        // 获取文控记录
        // TaskHistoryNew historyNew = getJecnTaskHistoryNew(taskBean);
        // if (historyNew == null) {
        // log.error("finishTask 任务发布，获取流程记录异常！historyNew =null");
        // throw new
        // IllegalArgumentException("finishTask任务发布，获取流程记录异常！historyNew =null");
        // }
        // 发布PRF文件记录文控信息
        // TaskCommon.saveTaskHistoryNew(historyNew, userMapper, docMapper,
        // processMapper, ruleMapper, fileMapper);
        // FIXME 消息邮件处理
        // messageEmailDesc(taskBean);
    }

    /**
     * 获取审批记录
     *
     * @param taskBean
     * @return
     */
    private TaskHistoryNew getJecnTaskHistoryNew(TaskBean taskBean) {
        if (taskBean == null) {
            log.error("getJecnTaskHistoryNew 审批任务完成，发布相关文件异常！taskBeanNew = " + taskBean);
            return null;
        }
        TaskHistoryNew historyNew = new TaskHistoryNew();
        // 获取版本号
        historyNew.setVersionId(taskBean.getHistoryVistion());
        JecnUser jecnUser = userMapper.getUser(taskBean.getCreatePersonId());
        if (jecnUser == null) {
            log.warn("获取审批记录，其中流程的拟稿人为空，任务id" + taskBean.getTaskId() + "任务名称" + taskBean.getTaskName());
        }
        // 拟稿人
        historyNew.setDraftPerson(jecnUser.getName());
        historyNew.setApproveCount(Long.valueOf(taskBean.getApproveNoCounts()));
        historyNew.setEndTime(Common.getStringbyDate(taskBean.getEndTime()));
        historyNew.setStartTime(Common.getStringbyDate(taskBean.getStartTime()));
        historyNew.setModifyExplain(taskBean.getTaskDesc());
        historyNew.setPublishDate(new Date());
        historyNew.setTestRunNumber(taskBean.getSendRunTimeValue());
        historyNew.setRelateId(taskBean.getRid());
        historyNew.setType(taskBean.getTaskType());
        historyNew.setTaskId(taskBean.getTaskId());

        // 获取文控各阶段审批人信息
        List<TaskHistoryFollow> listJecnTaskHistoryFollow = getJecnTaskHistoryFollowList(taskBean.getTaskId(),
                taskBean.getTaskType());
        historyNew.setListJecnTaskHistoryFollow(listJecnTaskHistoryFollow);
        return historyNew;
    }

    /**
     * 获取文控各阶段审批人信息
     *
     * @param taskId
     * @param taskType
     * @return
     */
    private List<TaskHistoryFollow> getJecnTaskHistoryFollowList(Long taskId, int taskType) {
        // 获取任务审批日志记录
        List<TaskForRecodeNew> listRecode = null;
        // 任务审批阶段集合
        List<TaskStage> taskStageList = null;
        try {
            // 获取任务审批日志记录
            listRecode = taskMapper.fetchTaskForRecords(taskId);

            // 任务审批顺序
            taskStageList = taskMapper.fetchTaskStagesByTaskId(taskId);
        } catch (Exception e) {
            log.error("获取任务日志记录异常");
        }
        if (taskStageList == null || listRecode == null) {
            log.error("getJecnTaskHistoryFollowList 获取文控各阶段审批人信息异常！taskOrderNew = " + taskStageList + "listRecode = "
                    + listRecode);
            return null;
        }
        List<TaskHistoryFollow> list = new ArrayList<TaskHistoryFollow>();
        if (taskStageList.size() == 0) {
            return list;
        }
        Set<Long> set = null;
        // 根据任务类型获取对应的系统审批阶段配置项信息
        List<ConfigItemBean> listItemBean = getTaskApproveConfigByTaskType(taskType);
        int sort = 0;

        // 获取最近的提交时间
        Date lastDate = null;
        for (int i = 1; i < taskStageList.size(); i++) {
            int state = taskStageList.get(i).getStageMark();
            set = new HashSet<Long>();
            for (TaskForRecodeNew forRecodeNew : listRecode) {
                if (forRecodeNew.getUpState() == state && forRecodeNew.getTaskElseState() != 11) {// 编辑人员不记录
                    // 获取源人Id集合
                    set.add(forRecodeNew.getFromPeopleId());
                    // 获取最近的提交时间
                    lastDate = forRecodeNew.getUpdateTime();
                }
            }
            if (set.size() > 0) {
                // 根据任务审批状态获取任务配置对于的mark唯一标识
                String mark = TaskCommon.getItemMark(state);
                log.error("TaskCommon.getItemMark(state)异常！");
                if (Common.isNullOrEmtryTrim(mark)) {
                    return null;
                }
                ConfigItemBean itemBean = TaskCommon.getJecnConfigItemBena(listItemBean, mark);
                if (itemBean == null) {
                    continue;
                }
                List<JecnUser> userList = userMapper.fetchUserByIds(SqlCommon.getIdsSet(set));

                TaskHistoryFollow taskHistoryFollow = new TaskHistoryFollow();
                // 任务审批阶段标识
                taskHistoryFollow.setStageMark(state);
                // 审核阶段名称：
                taskHistoryFollow.setAppellation(itemBean.getName() + "：");
                taskHistoryFollow.setSort(sort);
                // 记录当前阶段审批人名称
                StringBuffer buffUserName = new StringBuffer();

                for (int j = 0; j < userList.size(); j++) {
                    JecnUser jecnUser = userList.get(j);
                    if (j == userList.size() - 1) {
                        buffUserName.append(jecnUser.getName());
                    } else {
                        buffUserName.append(jecnUser.getName()).append("/");
                    }
                }
                // 更新时间(操作时间)
                taskHistoryFollow.setApprovalTime(lastDate);
                taskHistoryFollow.setName(buffUserName.toString());
                sort++;
                list.add(taskHistoryFollow);
            }
        }
        return list;
    }

    private List<ConfigItemBean> getTaskApproveConfigByTaskType(int taskType) {

        List<ConfigItemBean> configs = configItemMapper.fetchTaskApproveConfigByBigModel(taskType);
        return configs;
    }

    /**
     * FIXME 发送邮件 任务审批完成，邮件和消息处理
     *
     * @param TaskBean
     */
    private void messageEmailDesc(TaskBean TaskBean) {
        try {
            long taskId = TaskBean.getTaskId();
            // 任务审批完成给任务审批人发送邮件
            // boolean isTrue =
            // TaskCommon.isTrueMark(ConfigItemPartMapMark.emailPulishApprover.toString(),
            // TaskCommon.selectMessageAndEmailItemBean(configItemDao));
            // FIXME
            boolean isTrue = true;

            Set<Long> peopleList = new HashSet<Long>();
            Set<Long> peopleListInformation = new HashSet<Long>();
            if (isTrue) {
                peopleList.addAll(getPubRefApp(taskId));
            }
            // 任务审批完成给具有相关查阅权限的的人发送邮件
            // boolean isGroupTrue =
            // TaskCommon.isTrueMark(ConfigItemPartMapMark.emailPulishCompetence.toString(),
            // TaskCommon.selectMessageAndEmailItemBean(configItemDao));
            // FIXME
            boolean isGroupTrue = true;
            if (isGroupTrue) {
                peopleList.addAll(getGroupRefApp(taskId));
            }

            // 任务审批完成发布消息
            // boolean isMesTrue =
            // TaskCommon.isTrueMark(ConfigItemPartMapMark.mesPulishApprover.toString(),
            // TaskCommon.selectMessageAndEmailItemBean(configItemDao));
            // FIXME
            boolean isMesTrue = true;
            if (isMesTrue) {// 任务相关人员发现消息
                peopleListInformation.addAll(getPubRefApp(taskId));
            }

            /** *****拟稿人邮件消息处理********** */
            // 任务审批完成发布邮件
            // boolean isDrafEmailTrue =
            // TaskCommon.isTrueMark(ConfigItemPartMapMark.emailPulishDraftPeople.toString(),
            // TaskCommon.selectMessageAndEmailItemBean(configItemDao));
            boolean isDrafEmailTrue = true;
            if (isDrafEmailTrue) {// 拟稿人邮件处理

                peopleList.add(TaskBean.getCreatePersonId());
            }
            // 拟稿人任务审批完成消息处理
            // boolean isDrafMesTrue =
            // TaskCommon.isTrueMark(ConfigItemPartMapMark.mesPulishDraftPeople.toString(),
            // TaskCommon.selectMessageAndEmailItemBean(configItemDao));

            // FIXME
            boolean isDrafMesTrue = true;
            if (isDrafMesTrue) {
                peopleListInformation.add(TaskBean.getCreatePersonId());
            }

            // 是否给管理员发送邮件
            // boolean isToAdminEmail =
            // TaskCommon.isTrueMark(ConfigItemPartMapMark.mailPubToAdmin.toString(),
            // TaskCommon.selectMessageAndEmailItemBean(configItemDao));
            // FIXME
            boolean isToAdminEmail = true;
            if (isToAdminEmail) {
                List<JecnUser> adminEmailPeople = userMapper.getAdmins();
                for (JecnUser user : adminEmailPeople) {
                    peopleList.add(user.getId());
                }
            }

            // 是否给固定收件人发送邮件
            // boolean isToFixedEmail =
            // TaskCommon.isTrueMark(ConfigItemPartMapMark.mailPubToFixed.toString(),
            // TaskCommon.selectMessageAndEmailItemBean(configItemDao));
            boolean isToFixedEmail = true;
            if (isToFixedEmail) {
                // 任务类型
                int taskType = TaskBean.getTaskType();
                int type = -1;
                // 流程/流程地图
                if (taskType == 0 || taskType == 4) {
                    type = 0;
                }
                // 文件
                else if (taskType == 1) {
                    type = 2;
                }
                // 制度文件/制度模板
                else if (taskType == 2 || taskType == 3) {
                    type = 1;
                }

                List<JecnUser> fixedEmailPeople = userMapper.getFixeds(type);
                for (Object obj : fixedEmailPeople) {
                    peopleList.add(Long.valueOf(obj.toString()));
                }

            }

            if (peopleList != null && peopleList.size() > 0) {
                finishSendEmail(peopleList, TaskBean);
            }
            if (peopleListInformation != null && peopleListInformation.size() > 0) {
                // FIXME
                // createTaskMessage(peopleListInformation,
                // TaskBean.getTaskType(), TaskBean.getTaskName(), 2);
            }

        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    private void finishSendEmail(Set<Long> peopleSet, TaskBean jecnTaskBeanNew) {

        // Object[] 0:内外网；1：邮件地址；2：人员主键ID
        // List<JecnUser> userObject = personDao.getJecnUserList(peopleList);
        if (peopleSet.size() == 0) {
            return;
        }

        List<JecnUser> userObject = userMapper.fetchUserByIds(SqlCommon.getIdsSet(peopleSet));
        if (!userObject.isEmpty()) {
            // 获取任务创建人名称
            // JecnUser createJecnUser =
            // userMapper.getUser(jecnTaskBeanNew.getCreatePersonId());
            // BaseEmailBuilder emailBuilder =
            // EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_FINISH);
            // emailBuilder.setData(userObject, jecnTaskBeanNew);
            // List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
            // //TODO :EMAIL
            // JecnUtil.saveEmail(buildEmail);
        }

    }

    /**
     * 获取岗位查阅权限和部门和岗位组查阅权限对应的人员ID集合
     *
     * @return Set<Long> 查阅权限对应的人员ID集合
     */
    private Set<Long> getGroupRefApp(Long taskId) {

        TaskBean taskBean = taskMapper.getTaskBean(taskId);
        Long rid = taskBean.getRid();

        Set<Long> accessPeopleIds = new HashSet<Long>();
        // 获取岗位查阅权限对应的人员
        accessPeopleIds.addAll(userMapper.fetchPosAccessPermissApprove(rid));
        // 获取岗位组查阅权限对应的人员
        accessPeopleIds.addAll(userMapper.fetchOrgAccessPermissApprove(rid));
        // 获取岗位组查阅权限对应的人员
        accessPeopleIds.addAll(userMapper.fetchGroupAccessPermissApprove(rid));

        return accessPeopleIds;
    }

    // /**
    // *
    // * 发布给PRF查阅权限人员发送消息
    // *
    // * @param peopleIds
    // * 人员主键ID集合
    // * @param prfName
    // * PRF文件名称
    // * @param mesType
    // * 1:审批；2：发布；3：创建
    // */
    // public static void createTaskMessage(Set<Long> peopleIds, int taskType,
    // String prfName,int mesType) {
    // if (peopleIds == null || Common.isNullOrEmtryTrim(prfName) || s == null)
    // {
    // return;
    // }
    // for (Long peopleId : peopleIds) {
    // if (peopleId != null) {
    // JecnMessage jecnMessage = new JecnMessage();
    // jecnMessage.setPeopleId(0L);
    // // 消息类型：任务
    // jecnMessage.setMessageType(1);
    // jecnMessage.setMessageContent(prfName);
    // String strName = null;
    // if (mesType == 2) {// 发布任务
    // strName = getStrTitle(mesType) + prfName + getMesContants(mesType);
    // } else {// 创建任务或审批任务
    // strName = getStrTitle(mesType) + getMesContants(mesType) + prfName;
    // }
    // jecnMessage.setMessageTopic(strName);
    // jecnMessage.setNoRead(Long.valueOf(0));
    // jecnMessage.setInceptPeopleId(peopleId);
    // jecnMessage.setCreateTime(new Date());
    // s.save(jecnMessage);
    // s.flush();
    // }
    // }
    // }

    /**
     * 获取任务记录中相关人员
     *
     * @param taskId
     * @return
     */
    private Set<Long> getPubRefApp(long taskId) {
        // 获取任务记录
        List<TaskForRecodeNew> list = getTaskForRecodeNewList(taskId);
        Set<Long> set = new HashSet<Long>();
        for (TaskForRecodeNew jecnTaskForRecodeNew : list) {
            set.add(jecnTaskForRecodeNew.getFromPeopleId());
        }
        // 获取审批记录
        return set;
    }

    @Override
    public void markTaskToDelete(Long taskId) {
        Map<String, Object> map = new HashMap<>();
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        map.put("updateTime", new Date());
        map.put("taskId", taskId);
        // 标记任务被删除（假删除）
        taskMapper.markTaskToDelete(map);
    }

    /**
     * 该人员是否为当前任务的审批人 如果是返回true 如果不是返回false
     */
    @Override
    public boolean hasApproveTask(Long taskId, Long curPeopleId, Integer taskState) {

        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("taskId", taskId);
        paramMap.put("curPeopleId", curPeopleId);
        paramMap.put("taskState", taskState);
        int count = taskMapper.taskApproveCount(paramMap);
        if (count == 0) {
            return false;
        }
        return true;
    }

    @Override
    public boolean createPeopleHandleIsNotLeaveOffice(String taskId) {

        TaskBean taskBean = taskMapper.getTaskBean(Long.valueOf(taskId));
        if (taskBean.getTaskState() == 0 || taskBean.getTaskState() == 10) {
            JecnUser user = userMapper.getUser(taskBean.getCreatePersonId());
            if (user == null || user.getIsLock() == 1) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean flowNorms(Long processId) {
        FlowStructureT flowStructureT = processMapper.getFlowStructureT(processId);
        if (flowStructureT == null) {
            return false;
        }
        // 是否存在错误信息
        if (flowStructureT.getIsFlow() == 1 && flowStructureT.getErrorState() == 1) {// 流程验证错误信息
            return true;
        }
        return false;
    }

    @Override
    public TaskTestRunFile downloadTaskRunFile(Long id) {
        TaskTestRunFile runFile = taskMapper.getTaskRunFile(id);
        return runFile;
    }

    /**
     * 获取审批变动结果
     *
     * @param taskBean
     * @return String 审批变动结果
     */
    private String getTaskFixedDesc(SimpleSubmitMessage submitMessage, TaskBean taskBean) {

        // 获取审批变动
        if (taskBean.getUpState() == 1 || taskBean.getUpState() == 2) {// 如果是文控或者部门审核人提交
            switch (taskBean.getTaskType()) {
                case 0:// 流程
                case 4:// 流程地图
                    // 获取审批变动
                    return updateAssembledFlowTaskFixed(submitMessage, taskBean);
                case 1:// 文件
                    // 获取审批变动
                    return updateAssembledFileTaskFixed(submitMessage, taskBean);
                case 2:// 制度模板文件
                case 3:// 制度文件
                    // 获取审批变动
                    return updateAssembledRuleTaskFixed(submitMessage, taskBean);

            }
        }
        return TaskCommon.TASK_NOTHIN;
    }

    /**
     * 流程审批变动
     *
     * @param submitMessage 提交任务信息
     * @param taskBean      任务主表
     */
    public String updateAssembledFlowTaskFixed(SimpleSubmitMessage submitMessage, TaskBean taskBean) {
        Long rid = taskBean.getRid();
        // 任务的显示项
        // TaskApplicationNew jecnTaskApplicationNew = abstractTaskDao
        // .getTaskApplicationNew(taskBean.getId());
        TaskApplicationNew jecnTaskApplicationNew = taskMapper.getTaskApplicationNew(taskBean.getTaskId());
        // 获取流程信息
        // JecnFlowStructureT jecnFlowStructureT = structureDao.get(rid);
        // JecnFlowBasicInfoT jecnFlowBasicInfoT = basicInfoDao.get(rid);
        FlowStructureT jecnFlowStructureT = processMapper.getFlowStructureT(rid);
        FlowBasicInfoT jecnFlowBasicInfoT = processMapper.getProcessBasicInfoT(rid);
        // 文件类型
        Long typeId = null;
        if (jecnTaskApplicationNew.getFileType() != 0 && taskBean.getTaskType() == 0) {
            // basicInfoDao.getSession().evict(jecnFlowBasicInfoT);
            typeId = jecnFlowBasicInfoT.getTypeId();
        }
        // 密级 0公开：1 ； 秘密
        Long isPublic = jecnFlowStructureT.getIsPublic();
        String taskFixedDesc = TaskCommon.TASK_NOTHIN;
        // 文控审核和部门审核
        if (taskBean.getUpState() == 1 || taskBean.getUpState() == 2) {
            // 获取审批变动
            taskFixedDesc = assembledPrfTaskFixed(submitMessage, taskBean, jecnTaskApplicationNew, typeId, isPublic);
        }
        if (taskFixedDesc.equals(TaskCommon.TASK_NOTHIN)) {// 无变动
            return taskFixedDesc;
        }
        if (jecnTaskApplicationNew.getFileType() != 0 && taskBean.getTaskType() == 0) {
            if (isFixed(typeId, submitMessage.getType())) {// 存在变更
                // 更新类别
                Long fixTypeId = null;
                if (!Common.isNullOrEmtryTrim(submitMessage.getType())) {
                    fixTypeId = Long.valueOf(submitMessage.getType().trim());
                }
                jecnFlowBasicInfoT.setTypeId(fixTypeId);
                // abstractTaskDao.getSession().update(jecnFlowBasicInfoT);
                // abstractTaskDao.getSession().flush();
                Map<String, Object> paramMap = new HashMap<String, Object>();
                paramMap.put("obj", jecnFlowBasicInfoT);
                paramMap.put("pub", "_t");
                processMapper.updateFlowBasicInfo(paramMap);
            }
        }
        if (!Common.isNullOrEmtryTrim(submitMessage.getStrPublic())) {// 更新密级
            Long isPublicStr = Long.valueOf(submitMessage.getStrPublic().trim());
            jecnFlowStructureT.setIsPublic(isPublicStr);
            // abstractTaskDao.getSession().update(jecnFlowStructureT);
            processMapper.updateFlowStructureT(jecnFlowStructureT);
        }
        return taskFixedDesc;
    }

    /**
     * 文件审批变动
     *
     * @param submitMessage 提交任务信息
     * @param taskBeanNew   任务主表
     */

    public String updateAssembledFileTaskFixed(SimpleSubmitMessage submitMessage, TaskBean taskBeanNew) {
        Long rid = taskBeanNew.getRid();
        // JecnFileBeanT fileBeanT = fileDao.get(rid);
        FileBeanT fileBeanT = fileMapper.getFileBeanT(rid);
        // 任务的显示项
        // TaskApplicationNew jecnTaskApplicationNew =
        // abstractTaskDao.getTaskApplicationNew(taskBeanNew.getId());
        TaskApplicationNew jecnTaskApplicationNew = taskMapper.getTaskApplicationNew(taskBeanNew.getTaskId());
        // 密级 0公开：1 ； 秘密
        Long isPublic = fileBeanT.getIsPublic();
        String taskFixedDesc = TaskCommon.TASK_NOTHIN;
        // 文控审核和部门审核
        if (taskBeanNew.getUpState() == 1 || taskBeanNew.getUpState() == 2) {
            // 获取审批变动
            taskFixedDesc = assembledPrfTaskFixed(submitMessage, taskBeanNew, jecnTaskApplicationNew, null, isPublic);
        }
        if (taskFixedDesc.equals(TaskCommon.TASK_NOTHIN)) {// 无变动
            return taskFixedDesc;
        }
        if (!Common.isNullOrEmtryTrim(submitMessage.getStrPublic())) {
            fileBeanT.setIsPublic(Long.valueOf(submitMessage.getStrPublic().trim()));
            // abstractTaskDao.getSession().update(fileBeanT);
            // abstractTaskDao.getSession().flush();
            fileMapper.updateFileBeanT(fileBeanT);
        }
        return taskFixedDesc;
    }

    /**
     * 制度审批变动
     *
     * @param submitMessage 提交任务信息
     * @param taskBeanNew   任务主表
     */
    protected String updateAssembledRuleTaskFixed(SimpleSubmitMessage submitMessage, TaskBean taskBeanNew) {
        Long rid = taskBeanNew.getRid();
        // RuleT ruleT = ruleDao.get(rid);
        RuleT ruleT = ruleMapper.getRuleT(rid);
        // 任务的显示项
        // TaskApplicationNew jecnTaskApplicationNew =
        // abstractTaskDao.getTaskApplicationNew(taskBeanNew.getId());
        TaskApplicationNew jecnTaskApplicationNew = taskMapper.getTaskApplicationNew(taskBeanNew.getTaskId());

        // 密级 0秘密，1公开
        Long isPublic = ruleT.getIsPublic();
        Long typeId = ruleT.getTypeId();
        String taskFixedDesc = TaskCommon.TASK_NOTHIN;
        // 文控审核和部门审核
        if (taskBeanNew.getUpState() == 1 || taskBeanNew.getUpState() == 2) {
            // 获取审批变动
            taskFixedDesc = assembledPrfTaskFixed(submitMessage, taskBeanNew, jecnTaskApplicationNew, typeId, isPublic);
        }
        if (!Common.isNullOrEmtryTrim(submitMessage.getStrPublic())) {// 更新密级
            ruleT.setIsPublic(Long.valueOf(submitMessage.getStrPublic().trim()));
            // abstractTaskDao.getSession().update(ruleT);
            // abstractTaskDao.getSession().flush();
            ruleMapper.updateRuleT(ruleT);
        }
        return taskFixedDesc;
    }

    /**
     * 组装审批变动信息
     */
    protected String assembledPrfTaskFixed(SimpleSubmitMessage submitMessage, TaskBean taskBeanNew,
                                           TaskApplicationNew jecnTaskApplicationNew, Long typeId, Long isPublic) {
        // 密级
        String strPublic = submitMessage.getStrPublic();
        // 类别
        String strType = submitMessage.getType();
        // 组装审批变动信息
        StringBuffer strBuffer = new StringBuffer();
        if (jecnTaskApplicationNew.getIsPublic() != 0) {// 存在密级
            getFixIsPublic(strPublic, strBuffer, isPublic);
        }
        // 显示类别
        if (jecnTaskApplicationNew.getFileType() != 0) {
            getFileType(submitMessage, strBuffer, typeId, strType);
        }

        // 存在查阅权限
        if (jecnTaskApplicationNew.getIsAccess() != 0) {
            getAccessPermiss(submitMessage, taskBeanNew.getTaskType(), taskBeanNew.getRid(), strBuffer);
        }
        if (strBuffer.length() == 0) {
            return TaskCommon.TASK_NOTHIN;
        }
        return strBuffer.toString();
    }

    /**
     * true类别存在变更
     *
     * @param typeId
     * @param typeStr
     * @return
     */
    private boolean isFixed(Long typeId, String typeStr) {
        if (Common.isNullOrEmtryTrim(typeStr) && typeId != null) {
            return true;
        } else if (!Common.isNullOrEmtryTrim(typeStr) && typeId == null) {
            return true;
        } else if (!Common.isNullOrEmtryTrim(typeStr) && typeId != null) {
            if (!typeStr.trim().equals(typeId.toString())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取密级变动
     *
     * @param strPublic
     * @param strBuffer
     * @param isPublic
     */
    private void getFixIsPublic(String strPublic, StringBuffer strBuffer, Long isPublic) {
        if (!Common.isNullOrEmtryTrim(strPublic) && isPublic != null && !strPublic.equals(isPublic.toString())) {// 密级不相同
            if (isPublic == 0) {// 秘密
                // "密级由秘密更改为公开"
                strBuffer.append(TaskCommon.CHANGE_SECRET_TO_PUBLIC).append("\n");
            } else {// 公开
                // "密级由公开更改为秘密"
                strBuffer.append(TaskCommon.CHANGE_PUBLIC_TO_SECRET).append("\n");
            }
        }
    }

    /**
     * 获取类别变动
     *
     * @param submitMessage
     * @param strBuffer
     * @param typeId
     * @param strType
     */
    private void getFileType(SimpleSubmitMessage submitMessage, StringBuffer strBuffer, Long typeId, String strType) {
        if (Common.isNullOrEmtryTrim(submitMessage.getTypeName())) {// 类别不存在，默认名称为‘无’
            submitMessage.setTypeName(TaskCommon.TASK_NOTHIN);
        }
        if (typeId == null && (!Common.isNullOrEmtryTrim(strType) && !"-1".equals(strType))) {
            // 获取变动信息
            strBuffer.append(
                    // "类别由(" **************** ")更改为("
                    TaskCommon.TYPE_FROM + TaskCommon.TASK_NOTHIN + TaskCommon.TO_TYPE + submitMessage.getTypeName())
                    .append(")\n");
        } else if (strType != null && typeId != null) {
            // 获取Prf类别
            List<ProceeRuleTypeBean> listTypeBean = null;
            if (!strType.equals(typeId.toString())) {
                // 当前流程类别
                String curTypeName = TaskCommon.TASK_NOTHIN;
                listTypeBean = getProceeRuleTypeBean();
                if (listTypeBean == null || listTypeBean.size() == 0) {
                    return;
                }
                for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
                    if (typeId.equals(proceeRuleTypeBean.getTypeId())) {
                        curTypeName = proceeRuleTypeBean.getTypeName();
                        break;
                    }
                }
                // 获取变动信息
                strBuffer
                        .append(
                                // "类别由(" curTypeName
                                // ")更改为("submitMessage.getType()
                                TaskCommon.TYPE_FROM + curTypeName + TaskCommon.TO_TYPE + submitMessage.getTypeName())
                        .append(")\n");
            }
        }
    }

    /**
     * 获取查阅权限变动
     *
     * @param submitMessage 页面提交信息
     * @param taskType      任务类型
     * @param rifId         关联ID
     * @param strBuffer     返回变动信息
     */
    private void getAccessPermiss(SimpleSubmitMessage submitMessage, int taskType, Long rifId, StringBuffer strBuffer) {
        int accessType = -1;
        switch (taskType) {
            case 0:
            case 4:
                accessType = 0;
                break;
            case 1:// 文件
                accessType = 1;
                break;
            case 2:// 制度
            case 3:
                accessType = 3;
                break;
            default:
                break;
        }
        /** **************查阅权限变更************************ */
        // true 存在变动
        boolean isOrg = false;
        boolean isPos = false;
        boolean isGroup = false;
        // 部门查阅权限
        isOrg = orgOrPosAccessPermiss(submitMessage, accessType, rifId, strBuffer, 0);
        // 岗位查阅权限
        isPos = orgOrPosAccessPermiss(submitMessage, accessType, rifId, strBuffer, 1);
        // 岗位组查阅权限
        isGroup = orgOrPosAccessPermiss(submitMessage, accessType, rifId, strBuffer, 2);

        if (isOrg || isPos || isGroup) {// 是否存在变动
            TempAccessBean accessBean = new TempAccessBean();
            accessBean.setOrgIds(submitMessage.getOrgIds());
            accessBean.setPosIds(submitMessage.getPosIds());
            // 设置岗位组属性
            accessBean.setPosGroupIds(submitMessage.getGroupIds());
            accessBean.setRelateId(rifId);
            accessBean.setSave(false);
            accessBean.setType(accessType);
            // 更新查阅权限
            updateAccessPermissions(accessBean);
        }
    }

    /**
     * 部门查阅权限
     *
     * @param submitMessage 提交信息
     * @param accessType    查阅权限类别
     * @param relateId      关联ID
     */
    private boolean orgOrPosAccessPermiss(SimpleSubmitMessage submitMessage, int accessType, Long relateId,
                                          StringBuffer strBuffer, int type) {
        // 当前部门查阅权限
        List<Long> listOrgIds = new ArrayList<Long>();
        // StrorgIds 转List
        List<Long> listStrOrgIds = new ArrayList<Long>();
        String STR_SPLIT = "/";
        String tempIDs = "";
        if (type == 0) {// 组织
            tempIDs = submitMessage.getOrgIds();
        } else if (type == 2) {
            tempIDs = submitMessage.getGroupIds();

        } else {
            tempIDs = submitMessage.getPosIds();
        }

        if (!Common.isNullOrEmtryTrim(tempIDs)) {
            // 把拼装的权限ID以数组获取
            String[] strOrg = tempIDs.split(",");
            for (int i = 0; i < strOrg.length; i++) {
                String string = strOrg[i];
                listStrOrgIds.add(Long.valueOf(string.trim()));
            }
        }
        StringBuffer orgBufIds = new StringBuffer();
        StringBuffer orgBufNames = new StringBuffer();
        if (type == 0) {// 部门
            // 部门权限IDs
            getOrgIds(orgBufIds, orgBufNames, relateId, accessType);
        } else if (type == 2) {
            // 岗位组权限IDs
            getGroupIds(orgBufIds, orgBufNames, relateId, accessType);
        } else {
            // 岗位权限IDs
            getPosIds(orgBufIds, orgBufNames, relateId, accessType);
        }

        if (orgBufIds.length() > 0) {
            // 把拼装的权限ID以数组获取
            String[] strOrg = orgBufIds.toString().split(",");
            for (int i = 0; i < strOrg.length; i++) {
                String string = strOrg[i];
                listOrgIds.add(Long.valueOf(string.trim()));
            }
        }
        // 验证两个Long型数组是否相同false 不同
        boolean isSameList = isSameList(listStrOrgIds, listOrgIds);
        if (!isSameList) {// 存在不同的数据集
            // 当前部门权限
            String orgCur = orgBufNames.toString();
            if (Common.isNullOrEmtryTrim(orgCur)) {
                orgCur = TaskCommon.TASK_NOTHIN;
            }
            // 提交审批后查阅权限
            String orgStr = "";
            if (type == 0) {
                orgStr = submitMessage.getOrgNames();
            } else if (type == 2) {
                orgStr = submitMessage.getGroupNames();
            } else {
                orgStr = submitMessage.getPosNames();
            }
            if (Common.isNullOrEmtryTrim(orgStr)) {
                orgStr = TaskCommon.TASK_NOTHIN;
            } else {
                orgStr = orgStr.replaceAll("\n", "/");
            }
            if (type == 0) {
                // "部门权限由(" ********** ")更改为("
                strBuffer.append(TaskCommon.ROG_FROM + orgCur + TaskCommon.TO_TYPE + orgStr + ")");

            } else if (type == 2) {
                // //"岗位组权限由(" ********** ")更改为("
                strBuffer.append(TaskCommon.GRO_FROM + orgCur + TaskCommon.TO_TYPE + orgStr + ")");

            } else {
                // //"部门权限由(" ********** ")更改为("
                strBuffer.append(TaskCommon.POS_FROM + orgCur + TaskCommon.TO_TYPE + orgStr + ")");
            }
            return true;
        }
        return false;
    }

    /**
     * 验证两个Long型数组是否相同
     *
     * @param list1 List<Long>
     * @param list2 List<Long>
     * @return true 相同，false 不同
     */
    private boolean isSameList(List<Long> list1, List<Long> list2) {
        if (list1 == null || list2 == null) {
            throw new NullPointerException("数据处理异常！isSameList中数组不能为空");
        }
        if (list1.size() != list2.size()) {
            return false;
        } else {
            int count = 0;
            for (Long long1 : list1) {
                for (Long long2 : list2) {
                    if (long1.equals(long2)) {
                        count++;
                    }
                }
            }
            if (count != list1.size()) {// 存在不同的数据
                return false;
            }
        }
        return true;

    }

    /**
     * 更新查阅权限
     *
     * @param accessBean 查阅权限对象
     */
    protected void updateAccessPermissions(TempAccessBean accessBean) {
        // 部门查阅权限以逗号分隔‘,’
        String orgIds = accessBean.getOrgIds();
        // 岗位查阅权限以逗号分隔‘,’
        String posIds = accessBean.getPosIds();
        // 岗位组查阅权限以逗号分隔','
        String posGroupIds = accessBean.getPosGroupIds();
        // 0是流程，1是文件，2是标准，3是制度
        int type = accessBean.getType();
        // 关联ID
        Long relateId = accessBean.getRelateId();

        // 删除查阅权限

        // 1 岗位查阅权限
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("relateId", relateId);
        paramMap.put("type", type);
        paramMap.put("pub", "_t");
        permissionMapper.deletePosPermission(paramMap);

        // 2 部门查阅权限
        permissionMapper.deleteOrgPermission(paramMap);

        // 岗位组查阅权限
        permissionMapper.deletePosGroupPermission(paramMap);

        // 添加部门查阅权限
        if (!Common.isNullOrEmtryTrim(orgIds)) {
            String[] orgIdsArr = orgIds.split(",");
            for (String orgIdStr : orgIdsArr) {
                if (!Common.isNullOrEmtryTrim(orgIdStr)) {

                    Long orgId = Long.valueOf(orgIdStr.toString());
                    paramMap.put("figureId", orgId);

                    permissionMapper.insertOrgPerm(paramMap);
                }
            }
        }
        // 添加岗位查阅权限
        if (!Common.isNullOrEmtryTrim(posIds)) {
            String[] posIdsArr = posIds.split(",");
            for (String posIdStr : posIdsArr) {
                if (!Common.isNullOrEmtryTrim(posIdStr)) {
                    Long posId = Long.valueOf(posIdStr.toString());
                    paramMap.put("figureId", posId);
                    permissionMapper.insertPosPerm(paramMap);
                }
            }
        }
        // 添加岗位组查阅权限
        if (!Common.isNullOrEmtryTrim(posGroupIds)) {
            String[] posGroupIdsArr = posGroupIds.split(",");
            for (String posGroupIdStr : posGroupIdsArr) {
                if (!Common.isNullOrEmtryTrim(posGroupIdStr)) {
                    Long posGroupId = Long.valueOf(posGroupIdStr.toString());
                    paramMap.put("figureId", posGroupId);
                    permissionMapper.insertPosGroupPerm(paramMap);

                }
            }
        }

    }

    /**
     * 获取人员主键ID集合 发送邮件
     *
     * @param peopleSet
     * @param taskBean
     */
    private void sendTaskEmail(Set<Long> peopleSet, TaskBean taskBean) {
        if (peopleSet == null || peopleSet.size() == 0) {
            return;
        }
        // 任务审批是否需要发送邮件
        // boolean isTrue =
        // JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.emailApprover.toString(),
        // JecnTaskCommon
        // .selectMessageAndEmailItemBean(configItemDao));
        EmailTip emailTip = Util.getEmailTip(configItemMapper);
        if (isEnable(ConfigItemPartMapMark.emailApprover)) {// 发送邮件通知下级审批人
            // 获取任务创建人名称
            // Object[] 0:内外网；1：邮件地址；2：人员主键ID
            // List<JecnUser> userObject =
            // personDao.getJecnUserList(peopleList);
            List<JecnUser> users = getPeopleList(peopleSet);
            // 发送邮件 0为下个阶段审批人 1为拟稿人
            BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_APPROVE);

            emailBuilder.setData(users, emailTip, taskBean, 0);
            List<EmailBasicInfo> emails = emailBuilder.buildEmail();
            Util.saveEmails(emails, emailMapper);
        }
        // 任务审批是否发送消息
        // boolean isAppMesTrue =
        // JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.mesApprover.toString(),
        // JecnTaskCommon
        // .selectMessageAndEmailItemBean(configItemDao));

        if (isEnable(ConfigItemPartMapMark.mesApprover)) {// 任务审批通过发送消息同事下级审批人
            List<Message> messages = TaskCommon.createTaskMessages(peopleSet, taskBean.getTaskType(),
                    taskBean.getTaskName(), 1);
            saveMessages(messages);
        }

        // 是否给拟稿人发送邮件
        // boolean isDraf =
        // JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.emailDraftPeople.toString(),
        // JecnTaskCommon
        // .selectMessageAndEmailItemBean(configItemDao));
        if (isEnable(ConfigItemPartMapMark.emailDraftPeople)) {// 发送邮件给拟稿人
            peopleSet.clear();
            peopleSet.add(taskBean.getCreatePersonId());
            List<JecnUser> users = getPeopleList(peopleSet);
            BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_APPROVE);

            // 发送邮件 0为下个阶段审批人 1为拟稿人
            emailBuilder.setData(users, emailTip, taskBean, 1);
            List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
            Util.saveEmails(buildEmail, emailMapper);

        }

        // 是否给拟稿人发送消息
        // boolean isDrafMesTrue =
        // JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.mesDraftPeople.toString(),
        // JecnTaskCommon.selectMessageAndEmailItemBean(configItemDao));
        if (isEnable(ConfigItemPartMapMark.mesDraftPeople)) {// 任务审批通过发送消息同事下级审批人
            peopleSet.clear();
            peopleSet.add(taskBean.getCreatePersonId());
            List<Message> messages = TaskCommon.createTaskMessages(peopleSet, taskBean.getTaskType(),
                    taskBean.getTaskName(), 1);
            saveMessages(messages);
        }
    }

    private List<JecnUser> getPeopleList(Set<Long> peopleSet) {
        String ids = setToInStr(peopleSet);
        List<JecnUser> users = userMapper.fetchUserByIds(ids);
        return users;
    }

    private void saveMessages(List<Message> messages) {
        for (Message message : messages) {
            messageMapper.saveMessage(message);
        }

    }

    /**
     * 如果值为1表示启用其它值表示不启用
     *
     * @param mesapprover
     * @return
     */
    private boolean isEnable(ConfigItemPartMapMark mesapprover) {
        String value = configItemMapper.findConfigItemByMark(mesapprover.toString());
        if ("1".equals(value)) {
            return true;
        }
        return false;
    }

    /**
     * 拼接集合的id为sql语句的(1,2,3)格式
     *
     * @param peopleList
     * @return
     */
    private String setToInStr(Set<Long> peopleList) {
        StringBuffer buf = new StringBuffer();
        buf.append(" (");
        for (Long id : peopleList) {
            buf.append(id);
            buf.append(",");
        }
        buf.replace(buf.length() - 1, buf.length(), "");
        buf.append(") ");
        return buf.toString();
    }

    public List<AbolishBean> findAbolish(Map<String, Object> map) {
        Paging paging = (Paging) map.get("paging");
        PageHelper.startPage(paging.getPageNum(), paging.getPageSize(), "ABOLISH_TIME DESC");
        map.put("count", false);
        return taskMapper.findAbolish(map);
    }

    public int findCount(Map<String, Object> map) {
        map.put("count", true);
        return taskMapper.findCount(map);
    }

    public ResourceType getTaskBean(Long id) {
        TaskBean beanNew = taskMapper.getTaskBean(id);
        ResourceType type = null;
        if (beanNew.getTaskState() == 0) {// 拟稿阶段
            type = ResourceType.TASK_RESUBMIT;
        } else if (beanNew.getTaskState() == 10) {// 整理意见
            type = ResourceType.TASK_ORGANIZE;
        } else {
            type = ResourceType.TASK_APPROVE;
        }
        return type;
    }
}
