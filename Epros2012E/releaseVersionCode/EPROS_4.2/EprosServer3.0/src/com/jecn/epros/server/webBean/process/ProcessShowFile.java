package com.jecn.epros.server.webBean.process;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.download.FlowDriverBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.JecnFlowRecord;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessInterfacesDescriptionBean;
import com.jecn.epros.server.bean.process.temp.TmpKpiShowValues;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;

/**
 * 流程操作说明页面显示Bean
 * 
 * @author fuzhh Nov 7, 2012
 */
public class ProcessShowFile implements Serializable {

	/** 流程配置文件 */
	private List<JecnConfigItemBean> jecnConfigItemBean = new ArrayList<JecnConfigItemBean>();

	/** 流程名称 */
	private String flowName;
	/** 流程ID */
	private String flowId;

	/** 目的 */
	private String flowPurpose;
	/** 适用范围 */
	private String applicability;
	/** 术语定义 */
	private String noutGlossary;
	/** 流程输入 */
	private String flowInput;
	/** 流程输出 */
	private String flowOutput;
	/** 不适用范围 */
	private String noApplicability;
	/** 补充说明 */
	private String flowSupplement;
	/** 概况信息 */
	private String flowSummarize;
	/** 流程客户 */
	private String flowCustom;
	/** 流程文件类型 0 为表格 1为段落 */
	private int flowFileType;
	/** 流程创建人 */
	private String createName;

	/** 驱动规则 */
	private FlowDriverBean flowDriver = new FlowDriverBean();

	/** 活动明细数据 */
	private List<ProcessActiveWebBean> allActivityShowList = new ArrayList<ProcessActiveWebBean>();
	/** 关键活动PA */
	private List<ProcessActiveWebBean> paActivityShowList = new ArrayList<ProcessActiveWebBean>();
	/** 关键活动ksf */
	private List<ProcessActiveWebBean> ksfActivityShowList = new ArrayList<ProcessActiveWebBean>();
	/** 关键活动kcp */
	private List<ProcessActiveWebBean> kcpActivityShowList = new ArrayList<ProcessActiveWebBean>();
	/** 角色信息 */
	private List<ProcessRoleWebBean> roleList = new ArrayList<ProcessRoleWebBean>();
	/** 流程关键评测指标数据 */
//	private List<JecnFlowKpiName> flowKpiList = new ArrayList<JecnFlowKpiName>();
	private TmpKpiShowValues kpiShowValues = null;
	
	/** 相关流程 */
	private List<RelatedProcessBean> relatedProcessList = new ArrayList<RelatedProcessBean>();
	/** 记录保存数据 */
	private List<JecnFlowRecord> flowRecordList = new ArrayList<JecnFlowRecord>();
	/** 流程记录 */
	private List<FileWebBean> processRecordList = new ArrayList<FileWebBean>();
	/** 操作规范 */
	private List<FileWebBean> operationTemplateList = new ArrayList<FileWebBean>();

	/** 相关制度 */
	private List<Rule> ruleNameList = new ArrayList<Rule>();
	/** 相关标准 */
	private List<StandardBean> standardList = new ArrayList<StandardBean>();
	/** 相关风险 */
	private List<JecnRisk> riskList = new ArrayList<JecnRisk>();
	/** 流程边界 0起始活动 1终止活动 */
	private Object[] startEndActive = new Object[] {};
	/** 流程责任属性 0流程监护人 1流程责任人 2流程责任部门 */
	private ProcessAttributeBean responFlow = new ProcessAttributeBean();
	/** 流程文控信息 0版本号 1变更说明 2发布日期 3流程拟制人 4审批人 */
	private List<Object[]> documentList = new ArrayList<Object[]>();
	/***自定义要素1*/
	private String flowCustomOne;
	/***自定义要素2*/
	private String flowCustomTwo;
	/***自定义要素3*/
	private String flowCustomThree;
	/***自定义要素4*/
	private String flowCustomFour;
	/***自定义要素5*/
	private String flowCustomFive;
	/**术语**/
	private List<TermDefinitionLibrary> definitions=new ArrayList<TermDefinitionLibrary>();
	/**相关文件**/
	private List<FileWebBean> relatedFiles=new ArrayList<FileWebBean>();
	
	//活动输入输出说明配置
	private String  activityNoteShow;
	//流程驱动规则时间配置
	private String flowDrivenRuleTimeConfig;
	/** 流程接口描述信息**/
	private ProcessInterfacesDescriptionBean processInterfacesDescriptionBean;
	
	/**角色/职责属性配置：显示岗位名称 **/
	public boolean isShowPosNameBox() {
		return JecnContants.showPosNameBox;
	}
	/**活动配置属性：显示输入输出 **/
	public boolean isShowActInOutBox(){
		return JecnContants.showActInOutBox;
	}
	
	/***
	 * 是否显示记录保存的移交责任人和编号
	 */
	public boolean isShowRecordTransferAndNum(){
		return JecnConfigTool.isShowRecordTransferAndNum();
	}
	
	/**
	 * 相关文件
	 * @return
	 */
	public List<FileWebBean> getRelatedFiles() {
		if(allActivityShowList==null||allActivityShowList.size()==0){
			return relatedFiles;
		}
		return getDistinctRelatedFilesByActivitys(allActivityShowList);
	}
	
	private List<FileWebBean> getDistinctRelatedFilesByActivitys(List<ProcessActiveWebBean> allActivityShowList) {
		List<FileWebBean> files=new ArrayList<FileWebBean>();
		Set<Long> fileIds=new HashSet<Long>();
		for(ProcessActiveWebBean activeWebBean:allActivityShowList){
			for(FileWebBean file:activeWebBean.getListInput()){
				if(!fileIds.contains(file.getFileId())){
					files.add(file);
					fileIds.add(file.getFileId());
				}
			}
			for(FileWebBean file:activeWebBean.getListOperation()){
				if(!fileIds.contains(file.getFileId())){
					files.add(file);
					fileIds.add(file.getFileId());
				}
			}
			for(ActiveModeBean mode:activeWebBean.getListMode()){
				if(mode.getModeFile()==null){
					continue;
				}
				if(!fileIds.contains(mode.getModeFile().getFileId())){
					FileWebBean file=new FileWebBean();
					
					file.setFileId(mode.getModeFile().getFileId());
					file.setFileName(mode.getModeFile().getFileName());
					file.setFileNum(mode.getModeFile().getFileNum());
					
					files.add(file);
					fileIds.add(file.getFileId());
				}
			}
		}
		return files;
	}
	
	
	
	public String getActivityNoteShow() {
		return activityNoteShow;
	}

	public void setActivityNoteShow(String activityNoteShow) {
		this.activityNoteShow = activityNoteShow;
	}
	
	public String getFlowDrivenRuleTimeConfig() {
		return flowDrivenRuleTimeConfig;
	}

	public void setFlowDrivenRuleTimeConfig(String flowDrivenRuleTimeConfig) {
		this.flowDrivenRuleTimeConfig = flowDrivenRuleTimeConfig;
	}

	public String getFlowCustomOne() {
		return flowCustomOne;
	}

	public void setFlowCustomOne(String flowCustomOne) {
		this.flowCustomOne = flowCustomOne;
	}

	public String getFlowCustomTwo() {
		return flowCustomTwo;
	}

	public void setFlowCustomTwo(String flowCustomTwo) {
		this.flowCustomTwo = flowCustomTwo;
	}

	public String getFlowCustomThree() {
		return flowCustomThree;
	}

	public void setFlowCustomThree(String flowCustomThree) {
		this.flowCustomThree = flowCustomThree;
	}

	public String getFlowCustomFour() {
		return flowCustomFour;
	}

	public void setFlowCustomFour(String flowCustomFour) {
		this.flowCustomFour = flowCustomFour;
	}

	public String getFlowCustomFive() {
		return flowCustomFive;
	}

	public void setFlowCustomFive(String flowCustomFive) {
		this.flowCustomFive = flowCustomFive;
	}

	public int getFlowFileType() {
		return flowFileType;
	}

	public void setFlowFileType(int flowFileType) {
		this.flowFileType = flowFileType;
	}

	public List<JecnConfigItemBean> getJecnConfigItemBean() {
		return jecnConfigItemBean;
	}

	public void setJecnConfigItemBean(
			List<JecnConfigItemBean> jecnConfigItemBean) {
		this.jecnConfigItemBean = jecnConfigItemBean;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowPurpose() {
		return flowPurpose;
	}

	public void setFlowPurpose(String flowPurpose) {
		this.flowPurpose = flowPurpose;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String getNoutGlossary() {
		return noutGlossary;
	}

	public void setNoutGlossary(String noutGlossary) {
		this.noutGlossary = noutGlossary;
	}

	public String getFlowInput() {
		return flowInput;
	}

	public void setFlowInput(String flowInput) {
		this.flowInput = flowInput;
	}

	public String getFlowOutput() {
		return flowOutput;
	}

	public void setFlowOutput(String flowOutput) {
		this.flowOutput = flowOutput;
	}

	public String getNoApplicability() {
		return noApplicability;
	}

	public void setNoApplicability(String noApplicability) {
		this.noApplicability = noApplicability;
	}

	public String getFlowSupplement() {
		return flowSupplement;
	}

	public void setFlowSupplement(String flowSupplement) {
		this.flowSupplement = flowSupplement;
	}

	public String getFlowSummarize() {
		return flowSummarize;
	}

	public void setFlowSummarize(String flowSummarize) {
		this.flowSummarize = flowSummarize;
	}

	public FlowDriverBean getFlowDriver() {
		return flowDriver;
	}

	public void setFlowDriver(FlowDriverBean flowDriver) {
		this.flowDriver = flowDriver;
	}

	public List<ProcessActiveWebBean> getAllActivityShowList() {
		return allActivityShowList;
	}

	public void setAllActivityShowList(
			List<ProcessActiveWebBean> allActivityShowList) {
		this.allActivityShowList = allActivityShowList;
	}

	public List<ProcessRoleWebBean> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<ProcessRoleWebBean> roleList) {
		this.roleList = roleList;
	}

	public List<RelatedProcessBean> getRelatedProcessList() {
		return relatedProcessList;
	}

	public void setRelatedProcessList(
			List<RelatedProcessBean> relatedProcessList) {
		this.relatedProcessList = relatedProcessList;
	}

	public List<JecnFlowRecord> getFlowRecordList() {
		return flowRecordList;
	}

	public void setFlowRecordList(List<JecnFlowRecord> flowRecordList) {
		this.flowRecordList = flowRecordList;
	}

	public List<FileWebBean> getProcessRecordList() {
		return processRecordList;
	}

	public void setProcessRecordList(List<FileWebBean> processRecordList) {
		this.processRecordList = processRecordList;
	}

	public List<FileWebBean> getOperationTemplateList() {
		return operationTemplateList;
	}

	public void setOperationTemplateList(List<FileWebBean> operationTemplateList) {
		this.operationTemplateList = operationTemplateList;
	}

	public List<Rule> getRuleNameList() {
		return ruleNameList;
	}

	public void setRuleNameList(List<Rule> ruleNameList) {
		this.ruleNameList = ruleNameList;
	}

	public List<StandardBean> getStandardList() {
		return standardList;
	}

	public void setStandardList(List<StandardBean> standardList) {
		this.standardList = standardList;
	}

	public String getFlowCustom() {
		return flowCustom;
	}

	public void setFlowCustom(String flowCustom) {
		this.flowCustom = flowCustom;
	}

	public String getFlowId() {
		return flowId;
	}

	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	public List<JecnRisk> getRiskList() {
		return riskList;
	}

	public void setRiskList(List<JecnRisk> riskList) {
		this.riskList = riskList;
	}

	public List<ProcessActiveWebBean> getPaActivityShowList() {
		return paActivityShowList;
	}

	public void setPaActivityShowList(
			List<ProcessActiveWebBean> paActivityShowList) {
		this.paActivityShowList = paActivityShowList;
	}

	public List<ProcessActiveWebBean> getKsfActivityShowList() {
		return ksfActivityShowList;
	}

	public void setKsfActivityShowList(
			List<ProcessActiveWebBean> ksfActivityShowList) {
		this.ksfActivityShowList = ksfActivityShowList;
	}

	public List<ProcessActiveWebBean> getKcpActivityShowList() {
		return kcpActivityShowList;
	}

	public void setKcpActivityShowList(
			List<ProcessActiveWebBean> kcpActivityShowList) {
		this.kcpActivityShowList = kcpActivityShowList;
	}

	public Object[] getStartEndActive() {
		return startEndActive;
	}

	public void setStartEndActive(Object[] startEndActive) {
		this.startEndActive = startEndActive;
	}

	public ProcessAttributeBean getResponFlow() {
		return responFlow;
	}

	public void setResponFlow(ProcessAttributeBean responFlow) {
		this.responFlow = responFlow;
	}

	public List<Object[]> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<Object[]> documentList) {
		this.documentList = documentList;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public TmpKpiShowValues getKpiShowValues() {
		return kpiShowValues;
	}

	public void setKpiShowValues(TmpKpiShowValues kpiShowValues) {
		this.kpiShowValues = kpiShowValues;
	}
	public List<TermDefinitionLibrary> getDefinitions() {
		return definitions;
	}
	public void setDefinitions(List<TermDefinitionLibrary> definitions) {
		this.definitions = definitions;
	}
	public ProcessInterfacesDescriptionBean getProcessInterfacesDescriptionBean() {
		return processInterfacesDescriptionBean;
	}
	public void setProcessInterfacesDescriptionBean(ProcessInterfacesDescriptionBean processInterfacesDescriptionBean) {
		this.processInterfacesDescriptionBean = processInterfacesDescriptionBean;
	}
	
}
