package epros.draw.gui.workflow.tabbedPane;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JDesktopPane;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.JecnFlowMapData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseLineData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.event.JecnFormatActionProcess;
import epros.draw.event.JecnKeyHandler;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseKeyActiveFigure;
import epros.draw.gui.figure.shape.AuthTipPanel;
import epros.draw.gui.figure.shape.ErrorPanel;
import epros.draw.gui.figure.shape.JecnEditPortPanel;
import epros.draw.gui.figure.shape.JecnTempFigurePanel;
import epros.draw.gui.figure.shape.part.TermFigureAR;
import epros.draw.gui.figure.unit.JecnFlowElementCopyAndPaste;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.JecnLineTextPanel;
import epros.draw.gui.line.JecnPageSetLine;
import epros.draw.gui.line.JecnTempLine;
import epros.draw.gui.line.shape.CommentLine;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.JecnTempManLine;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.TempDividingLine;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.JecnAidCompCollection;
import epros.draw.gui.workflow.aid.JecnLinkFigureMainProcess;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.gui.workflow.grid.JecnShapeGridProcess;
import epros.draw.gui.workflow.zoom.JecnWorkflowZoomProcess;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.unredo.JecnUndoRedoManager;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 画图面板
 * 
 * 一个桌面面板对应一个流程图或流程地图
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDrawDesktopPane extends JDesktopPane {
	/** 画图面板对应的标题面板 */
	private JecnTabbedTitlePanel tabbedTitlePanel = null;

	/** 流程文件路径 */
	private String flowFilePath;
	// ********************* 画图面板需要保存对象 start *********************//
	/** 流程元素集合 */
	private List<JecnBaseFlowElementPanel> panelList = new ArrayList<JecnBaseFlowElementPanel>();

	private List<JecnBaseFlowElementPanel> modelPanelList = new ArrayList<JecnBaseFlowElementPanel>();
	/** 记录面板所有连接线集合 */
	private List<JecnBaseManhattanLinePanel> manLineList = new ArrayList<JecnBaseManhattanLinePanel>();
	/** 当前选中的所有控件 */
	private List<JecnBaseFlowElementPanel> currentSelectElement = new ArrayList<JecnBaseFlowElementPanel>();
	/** 记录面板所有横竖分割线 */
	private List<JecnBaseVHLinePanel> vhLinePanelList = new ArrayList<JecnBaseVHLinePanel>();
	/** 面板所有横线、竖线、斜线（老版分割线） */
	private List<JecnBaseDividingLinePanel> diviLineList = new ArrayList<JecnBaseDividingLinePanel>();
	/** 面板所有注释框的线段 */
	private List<CommentLine> commentLineList = new ArrayList<CommentLine>();
	/** 临时小线段集合，比如拖动过程中生成线段，拖动结束后清除该临时线段 */
	private List<JecnTempLine> tempLines = new ArrayList<JecnTempLine>();
	/** 流程面板分页符集合 */
	private List<JecnPageSetLine> pageSetLines = new ArrayList<JecnPageSetLine>();

	/** 当勾选连接线时不显示元素编辑点时，当前集合记录显示编辑点的元素 （点击连接线，100*100的 区域内 有交集的元素） */
	private List<JecnBaseFigurePanel> showEditPointFigures = new ArrayList<JecnBaseFigurePanel>();
	// ********************* 画图面板需要保存对象 end*********************//

	/** **** 记录剪贴板复制的流程元素集合:源流程元素（不是克隆后的流程元素） ***** */
	// 流程图
	private static List<JecnBaseFlowElementPanel> curCopyPartMapList = new ArrayList<JecnBaseFlowElementPanel>();
	// 流程地图
	private static List<JecnBaseFlowElementPanel> curCopyTotalMapList = new ArrayList<JecnBaseFlowElementPanel>();
	// 组织图
	private static List<JecnBaseFlowElementPanel> curCopyOrgMapList = new ArrayList<JecnBaseFlowElementPanel>();
	/** **** 记录剪贴板复制的流程元素集合:源流程元素（不是克隆后的流程元素）***** */

	/** 拖动分割线生成的虚拟线段 */
	private List<TempDividingLine> tempDividingList = new ArrayList<TempDividingLine>();
	/** 拖动图形，图形生成虚拟边框 */
	private List<Rectangle> listRectangle = new ArrayList<Rectangle>();

	/** ********************************************** */
	/** 记录面板错误信息记录集合(流程属性规范) */
	private List<ErrorPanel> errorPanelList = new ArrayList<ErrorPanel>();
	/** ******************************************** */

	/** 流程图术语元素集合 */
	private List<TermFigureAR> termFigureList = new ArrayList<TermFigureAR>();

	/** 当前流程图或流程地图对应的数据对象 */
	private JecnFlowMapData flowMapData = null;
	/** 撤销恢复管理对象 */
	private JecnUndoRedoManager undoRedoManager = null;

	/** 画图面板的容器 */
	private JecnDrawScrollPane scrollPanle = null;

	/** 当前放大缩小倍数 */
	private double workflowScale = 1.0;
	/** 当前面板最大图形层级数 */
	private int maxZOrderIndex = 0;

	/** 画图面板网格绘制类 */
	private JecnShapeGridProcess shapeProcess = null;
	/** 临时线段记录开始点 ； 当剪切复制粘贴使用时，此值存储是原始状态下的值；Ctrl+左键复制：拖动间距 */
	private Point startPoint = null;
	/** 临时线段记录开始点 ； 线段方向（目前只有流程架构由此功能） */
	private EditPointType startPointType = null;

	/** 画连接线 生成的虚拟线段 */
	public TempDividingLine activeLine = null;
	/** 首次划线（点击右侧流程元素添加连接线），生成的虚拟曼哈顿线 */
	private JecnTempManLine tempLine = null;
	/** 连接线端点拖动专用： 拖动曼哈顿线端点，生成的虚拟曼哈顿线 */
	private JecnTempManLine jecnTempManLine = null;
	/** 点击流程元素库生成图形虚拟边框 */
	private JecnTempFigurePanel jecnTempFigurePanel = null;

	/** 拖动线段端点记录临时端点 */
	private JecnEditPortPanel tempEditPort;

	/** 辅助集合对象 */
	private JecnAidCompCollection aidCompCollection = null;
	/** 放大缩小处理类 */
	private JecnWorkflowZoomProcess workflowZoomProcess = null;
	/** 图形层级处理类 */
	private JecnGraphicsLevel graphicsLevel = null;
	/** 角色移动处理 */
	private JecnRoleMobile roleMobile = null;

	/** 记录片段的ID */
	private String templateId = null;

	/** X坐标最大的流程元素 */
	private JecnBaseFlowElementPanel maxXPanel = null;
	/** Y坐标最大的流程元素 */
	private JecnBaseFlowElementPanel maxYPanel = null;

	/** 画图面板 上下左右键 键盘事件处理类 */
	private JecnKeyHandler keyHandler = null;
	/** 是否执行圈选选中 */
	private boolean isSelectArea;
	/** 格式刷数据对象 */
	private JecnAbstractFlowElementData formatPainterData;

	private AuthTipPanel tipPanel;

	public JecnDrawDesktopPane(JecnFlowMapData flowMapData, JecnTabbedTitlePanel tabbedTitlePanel) {
		if (flowMapData == null || tabbedTitlePanel == null) {
			JecnLogConstants.LOG_DRAW_DESKTOP_PANE.error("JecnDrawDesktopPane类构造函数：参数为null.flowMapData=" + flowMapData
					+ ";tabbedTitlePanel=" + tabbedTitlePanel);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.flowMapData = flowMapData;
		this.tabbedTitlePanel = tabbedTitlePanel;

		initComponents();
	}

	private void initComponents() {

		// 撤销恢复管理对象
		undoRedoManager = new JecnUndoRedoManager(this);
		// 画图面板的容器
		scrollPanle = new JecnDrawScrollPane(this);
		// 画图面板网格绘制类
		shapeProcess = new JecnShapeGridProcess();
		// 辅助集合对象
		aidCompCollection = new JecnAidCompCollection(this);
		// 放大缩小处理类
		workflowZoomProcess = new JecnWorkflowZoomProcess(this);
		// 图形层级处理类
		graphicsLevel = new JecnGraphicsLevel(this);

		// 背景颜色
		this.setBackground(JecnUIUtil.getWorkflowColor());
		// 画图面板
		this.setBorder(null);
		// 大小
		this.setPreferredSize(JecnUIUtil.getWorkflowSize());
		this.setMinimumSize(JecnUIUtil.getWorkflowSize());
		// 此设置必须存在，因为角色移动使用当前面板的宽/高
		this.setSize(JecnUIUtil.getWorkflowSize());
		flowMapData.setWorkflowWidth(JecnUIUtil.getWorkflowSize().width);
		flowMapData.setWorkflowHeight(JecnUIUtil.getWorkflowSize().height);

		// 鼠标事件
		this.addMouseListener(JecnDrawMainPanel.getMainPanel());
		this.addMouseMotionListener(JecnDrawMainPanel.getMainPanel());
		this.addComponentListener(new JecnWorkflowComponentAdapter());
		// 面板键盘事件监听
		keyHandler = new JecnKeyHandler(this);
		this.addKeyListener(keyHandler);
	}

	/**
	 * 主面板画图算法
	 * 
	 */
	protected void paintComponent(Graphics g) {
		// 重绘横竖分割线
		if (!JecnLoading.isDialogShow()) {// 此判断针对打开时并发异常添加的
			rePaintVHLinePanel();
		}

		// 显示/隐藏网格
		if (JecnSystemStaticData.isShowGrid()) {
			shapeProcess.drawWorkflowGride(g, this.getWorkflowScale(), this.getWidth(), this.getHeight());
		}

		if (tempDividingList != null) {// 画连接线时临时线
			for (TempDividingLine dividingLine : tempDividingList) {
				dividingLine.paintDiveLine(g);
			}
		}

		if (diviLineList.size() != 0) {// 分割线
			for (JecnBaseDividingLinePanel dividingLine : diviLineList) {
				if (dividingLine instanceof DividingLine) {
					DividingLine line = (DividingLine) dividingLine;
					line.paintDiviLine(g);
				}
			}
		}
		if (commentLineList.size() > 0) {
			for (CommentLine commentLine : commentLineList) {
				commentLine.paintComentLine(g);
			}
		}
		// 拖动图形，图形生成虚拟边框
		if (listRectangle.size() > 0) {
			Graphics2D g2d = (Graphics2D) g;
			for (Rectangle shape : listRectangle) {
				g2d.setPaint(Color.blue);
				g2d.draw(shape);
			}
		}

	}

	/**
	 * 清空拖动生成的所有虚拟图形
	 * 
	 */
	public void removeTempListAll() {
		// 图形元素虚拟图形集合
		listRectangle.clear();
		for (TempDividingLine tempLine : tempDividingList) {
			this.remove(tempLine);
		}
		// 分割线虚拟图形集合
		tempDividingList.clear();
	}

	/**
	 * 
	 * 判断内容是否合法
	 * 
	 * @return boolean 合法返回true；不合法返回false
	 */
	public boolean isEditFail() {
		return this.aidCompCollection.getEditTextArea().isEditFail();
	}

	/**
	 * 添加流程元素到panel集合
	 * 
	 * @param flowElementPanel
	 *            JecnBaseFlowElementPanel流程元素对象
	 */
	public void addJecnPanel(JecnBaseFlowElementPanel flowElementPanel) {
		if (flowElementPanel == null) {
			return;
		}
		if (panelList.contains(flowElementPanel)) {// 给定组件已经添加到画图面板中
			return;
		}
		// 添加元素到面板
		if (flowElementPanel.getFlowElementData().getMapElemType() != MapElemType.MapLineFigure) {
			this.add(flowElementPanel);
		}

		panelList.add(flowElementPanel);
		if (flowElementPanel instanceof JecnBaseFigurePanel) {
			this.getFlowMapData().getFigureList().add((JecnFigureData) flowElementPanel.getFlowElementData());
		} else {
			// 添加线数据到面板
			this.getFlowMapData().getLineList().add((JecnBaseLineData) flowElementPanel.getFlowElementData());
		}

		switch (flowElementPanel.getFlowElementData().getMapElemType()) {
		case ManhattanLine:// 带箭头连接线
		case CommonLine:
			JecnBaseManhattanLinePanel manLinePanel = (JecnBaseManhattanLinePanel) flowElementPanel;
			if (this.flowMapData.getMapType() == MapType.partMap || this.flowMapData.getMapType() == MapType.totalMap
					|| this.flowMapData.getMapType() == MapType.totalMapRelation) {
				// 设置元素开始元素和结束元素
				manLinePanel.getFlowElementData()
						.setStartFigureData(manLinePanel.getStartFigure().getFlowElementData());
				manLinePanel.getFlowElementData().setEndFigureData(manLinePanel.getEndFigure().getFlowElementData());
			}
			manLineList.add(manLinePanel);
			break;
		case DividingLine:// 随意拖动的分割线
		case HDividingLine:
		case VDividingLine:
			JecnBaseDividingLinePanel dividingLine = (JecnBaseDividingLinePanel) flowElementPanel;
			this.diviLineList.add(dividingLine);
			break;
		case ParallelLines:
		case VerticalLine:
			JecnBaseVHLinePanel baseVHLinePanel = (JecnBaseVHLinePanel) flowElementPanel;
			this.vhLinePanelList.add(baseVHLinePanel);
			break;
		case TermFigureAR:
			termFigureList.add((TermFigureAR) flowElementPanel);
			break;
		case ModelFigure:
			modelPanelList.add(flowElementPanel);
			break;
		default:
			break;
		}
	}

	/**
	 * 删除面板流程元素集合对应图形
	 * 
	 * @param flowElementPanel
	 *            JecnBaseFlowElementPanel流程元素
	 */
	public void removePanelList(JecnBaseFlowElementPanel flowElementPanel) {
		if (flowElementPanel == null) {
			return;
		}
		if (flowElementPanel instanceof JecnBaseFigurePanel) {
			this.getFlowMapData().getFigureList().remove((JecnFigureData) flowElementPanel.getFlowElementData());
			((JecnBaseFigurePanel) flowElementPanel).removeRelatedPanel();
		} else {
			// 添加线数据到面板
			this.getFlowMapData().getLineList().remove((JecnBaseLineData) flowElementPanel.getFlowElementData());
		}
		switch (flowElementPanel.getFlowElementData().getMapElemType()) {
		case ManhattanLine:// 带箭头连接线
		case CommonLine:
			manLineList.remove(flowElementPanel);
			break;
		case DividingLine:// 随意拖动的分割线
		case HDividingLine:
		case VDividingLine:
			this.diviLineList.remove(flowElementPanel);
			break;
		case ParallelLines:
		case VerticalLine:
			this.vhLinePanelList.remove(flowElementPanel);
			break;
		case TermFigureAR:
			termFigureList.remove(flowElementPanel);
			break;
		case ModelFigure:
			modelPanelList.remove(flowElementPanel);
			if (JecnSystemStaticData.isShowModel()) {
				JecnFormatActionProcess.megerModel();
			}
			break;
		default:
			break;
		}

		// 判断当前图形是否为面板X或Y轴坐标方向的最大图形
		if (flowElementPanel == this.getMaxXPanel()) {
			this.setMaxXPanel(null);
		} else if (flowElementPanel == this.getMaxYPanel()) {
			this.setMaxYPanel(null);
		}

		this.remove(flowElementPanel);
		panelList.remove(flowElementPanel);
	}

	/**
	 * 
	 * 获取选中的活动和关键活动
	 * 
	 * @param activeFigureList
	 *            List<JecnBaseActiveFigure> 存放活动的集合
	 * @param keyActiveFigureList
	 *            List<JecnBaseKeyActiveFigure> 存放关键活动的集合
	 */
	public void getSelectedActionAndKeyAction(List<JecnBaseActiveFigure> activeFigureList,
			List<JecnBaseKeyActiveFigure> keyActiveFigureList) {

		if (activeFigureList == null || keyActiveFigureList == null) {
			return;
		}

		for (JecnBaseFlowElementPanel elemPanelTmp : panelList) {
			if (elemPanelTmp instanceof JecnBaseActiveFigure) {// 活动
				activeFigureList.add((JecnBaseActiveFigure) elemPanelTmp);
			} else if (elemPanelTmp instanceof JecnBaseKeyActiveFigure) {// 关键活动
				keyActiveFigureList.add((JecnBaseKeyActiveFigure) elemPanelTmp);
			}
		}
	}

	/** *******************晓虎 start********************************* */

	public JecnFlowMapData getFlowMapData() {
		return flowMapData;
	}

	public String getFlowFilePath() {
		return flowFilePath;
	}

	public void setFlowFilePath(String flowFilePath) {
		this.flowFilePath = flowFilePath;
	}

	public void setFlowMapData(JecnFlowMapData flowMapData) {
		this.flowMapData = flowMapData;
	}

	public JecnDrawScrollPane getScrollPanle() {
		return scrollPanle;
	}

	public double getWorkflowScale() {
		return workflowScale;
	}

	public void setWorkflowScale(double workflowScale) {
		this.workflowScale = workflowScale;
	}

	public List<JecnBaseFlowElementPanel> getCurrentSelectElement() {
		return currentSelectElement;
	}

	public JecnGraphicsLevel getGraphicsLevel() {
		return graphicsLevel;
	}

	public Point getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
		JecnFlowElementCopyAndPaste.initCountPative();
	}

	public int getMaxZOrderIndex() {
		return maxZOrderIndex;
	}

	public void setMaxZOrderIndex(int maxZOrderIndex) {
		this.maxZOrderIndex = maxZOrderIndex;
	}

	public JecnShapeGridProcess getShapeProcess() {
		return shapeProcess;
	}

	public void setShapeProcess(JecnShapeGridProcess shapeProcess) {
		this.shapeProcess = shapeProcess;
	}

	public JecnAidCompCollection getAidCompCollection() {
		return aidCompCollection;
	}

	public void setAidCompCollection(JecnAidCompCollection aidCompCollection) {
		this.aidCompCollection = aidCompCollection;
	}

	/**
	 * 
	 * 获取放大缩小处理对象
	 * 
	 * @return JecnWorkflowZoomProcess 放大缩小处理对象
	 */
	public JecnWorkflowZoomProcess getWorkflowZoomProcess() {
		return workflowZoomProcess;
	}

	public JecnUndoRedoManager getUndoRedoManager() {
		return undoRedoManager;
	}

	public void setUndoRedoManager(JecnUndoRedoManager undoRedoManager) {
		this.undoRedoManager = undoRedoManager;
	}

	/**
	 * 
	 * 角色移动处理
	 * 
	 */
	public void processRoleMobile() {
		// 角色移动:主要针对切换画图面板使用，新建流程图时在新建流程图事件中重新添加角色移动
		if (JecnSystemStaticData.isShowRoleMove()) {
			// 添加角色移动
			this.createAddJecnRoleMobile();
		} else {
			// 移除角色移动
			this.removeJecnRoleMobile();
		}

		if (JecnSystemStaticData.isShowModel()) {
			JecnFormatActionProcess.megerModel();
		} else {
			this.getFlowMapData().setModelFigureXY(null);
		}
	}

	/**
	 * 
	 * 创建且添加角色移动对象
	 * 
	 */
	public void createAddJecnRoleMobile() {
		if (roleMobile == null && this.getFlowMapData().getMapType() == MapType.partMap) {
			// 创建JecnRoleMobile对象
			roleMobile = new JecnRoleMobile(this);

			// 重新向面板上添加角色移动框
			this.add(roleMobile.getRoleMove());
			// 设置角色移动的层级
			this.setLayer(roleMobile.getRoleMove(), JecnSystemStaticData.getRoleMaxLayer());
		}
	}

	/**
	 * 
	 * 如果是流程图且显示角色移动时: 删除之前角色移动添加新的角色移动
	 * 
	 */
	public void removeAddRoleMove() {
		// 判断是否需要角色移动
		if (MapType.partMap == this.getFlowMapData().getMapType() && JecnSystemStaticData.isShowRoleMove()) {
			// 移除角色移动
			this.removeJecnRoleMobile();
			// 创建并添加角色移动
			this.createAddJecnRoleMobile();
		}
	}

	/**
	 * 
	 * 移除角色移动
	 * 
	 */
	public void removeJecnRoleMobile() {
		// 如果角色移动不为空
		if (roleMobile != null && this.getFlowMapData().getMapType() == MapType.partMap) {
			this.remove(this.getRoleMobile().getRoleMove());
			// 清空角色移动
			this.setRoleMobile(null);
		}
	}

	/**
	 * 角色移动
	 * 
	 * @return
	 */
	public JecnRoleMobile getRoleMobile() {
		return roleMobile;
	}

	public void setRoleMobile(JecnRoleMobile roleMobile) {
		this.roleMobile = roleMobile;
	}

	public JecnEditPortPanel getTempEditPort() {
		return tempEditPort;
	}

	public void setTempEditPort(JecnEditPortPanel tempEditPort) {
		this.tempEditPort = tempEditPort;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	/**
	 * 
	 * 判断给定的流程元素在选中集合中是否存在
	 * 
	 * @param panel
	 *            JecnBaseFlowElementPanel 流程元素
	 * @return boolean true：包含；false：不包含
	 */
	public boolean isContainsSelectList(JecnBaseFlowElementPanel panel) {
		if (panel == null) {
			return false;
		}
		if (this.currentSelectElement.contains(panel)) {// 包含
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 添加选中流程元素到选中集合中
	 * 
	 * @param panel
	 *            JecnBaseFlowElementPanel 选中流程元素
	 */
	public void addSelectFlowElementList(JecnBaseFlowElementPanel panel) {
		if (isContainsSelectList(panel)) {
			return;
		}
		this.currentSelectElement.add(panel);
	}

	/**
	 * 
	 * 获取剪切复制对象集合
	 * 
	 * @return List<JecnBaseFlowElementPanel>
	 */
	public List<JecnBaseFlowElementPanel> getCurCopyList() {
		switch (flowMapData.getMapType()) {
		case partMap:// 流程图
			return curCopyPartMapList;
		case totalMap:// 流程地图
		case totalMapRelation:// 集成关系图
			return curCopyTotalMapList;
		case orgMap:// 组织图
			return curCopyOrgMapList;
		}

		// 这种情况不会出现
		JecnLogConstants.LOG_DRAW_DESKTOP_PANE.error("逻辑错误！flowMapData中MapType值必须是totalMap/partMap/orgMap其中之一");
		return new ArrayList<JecnBaseFlowElementPanel>();
	}

	/**
	 * 
	 * 添加剪切复制的流程元素到集合中
	 * 
	 * @param flowElementPanelList
	 *            List<JecnBaseFlowElementPanel> 剪切复制的流程元素集合
	 */
	public void addCurCopyList(List<JecnBaseFlowElementPanel> flowElementPanelList) {
		if (flowElementPanelList == null || flowElementPanelList.size() == 0 || flowMapData.getMapType() == null
				|| MapType.none == flowMapData.getMapType()) {
			return;
		}

		// 初始化剪切复制集合
		curCopyPartMapList.clear();
		curCopyTotalMapList.clear();
		curCopyOrgMapList.clear();

		// 获取集合
		List<JecnBaseFlowElementPanel> curCopyList = getCurCopyList();
		// 添加流程元素
		for (JecnBaseFlowElementPanel panel : flowElementPanelList) {
			if (panel == null || curCopyList.contains(panel)) {
				continue;
			}
			curCopyList.add(panel);
		}
	}

	public List<Rectangle> getListRectangle() {
		return listRectangle;
	}

	/**
	 * 
	 * 添加虚拟边框到集合中
	 * 
	 * @param r
	 *            Rectangle 虚拟边框
	 */
	public void addToListRectangle(Rectangle r) {
		if (r == null) {
			return;
		}
		if (!this.listRectangle.contains(r)) {// 不包含
			this.listRectangle.add(r);
		}
	}

	public JecnTempFigurePanel getJecnTempFigurePanel() {
		return jecnTempFigurePanel;
	}

	public void setJecnTempFigurePanel(JecnTempFigurePanel jecnTempFigurePanel) {
		this.jecnTempFigurePanel = jecnTempFigurePanel;
	}

	/**
	 * 
	 * 改变流程元素位置点或大小时，需要计算画图面板大小
	 * 
	 * 当满足条件需要计算时，重新计算画图面板大小，不需要计算时保持之前的设置
	 * 
	 * @param panel
	 *            JecnBaseFlowElementPanel 流程元素
	 */
	public void getCurrDrawPanelSize(JecnBaseFlowElementPanel panel) {
		if (panel == null) {// 参数为空
			return;
		}

		// 期望大小
		Dimension preSize = this.getPreferredSize();
		// 期望值宽
		int preWidth = preSize.width;
		// 期望值高
		int preHeight = preSize.height;

		// 流程元素右下角X坐标
		int x = 0;
		// 流程元素右下角Y坐标
		int y = 0;
		if (panel instanceof ParallelLines) {// 参数对象是横分割线

			x = preWidth - JecnSystemStaticData.getWorkflowResizeWidth();
			y = panel.getY() + panel.getHeight() + JecnSystemStaticData.getWorkflowResizeHeight();

		} else if (panel instanceof VerticalLine) {// 参数对象是竖分割线

			x = panel.getX() + panel.getWidth() + JecnSystemStaticData.getWorkflowResizeWidth();
			y = preHeight - JecnSystemStaticData.getWorkflowResizeHeight();

		} else if (panel instanceof DividingLine) { // 斜线
			DividingLine dividingLine = (DividingLine) panel;
			Point startPoint = dividingLine.getStartPoint();
			Point endPoint = dividingLine.getEndPoint();

			if (startPoint.x >= endPoint.x) {
				x = startPoint.x + JecnSystemStaticData.getWorkflowResizeWidth();
			} else {
				x = endPoint.x + JecnSystemStaticData.getWorkflowResizeWidth();
			}

			if (startPoint.y >= endPoint.y) {
				y = startPoint.y + JecnSystemStaticData.getWorkflowResizeHeight();
			} else {
				y = endPoint.y + JecnSystemStaticData.getWorkflowResizeHeight();
			}
		} else {
			x = panel.getX() + panel.getWidth() + JecnSystemStaticData.getWorkflowResizeWidth();
			y = panel.getY() + panel.getHeight() + JecnSystemStaticData.getWorkflowResizeHeight();
		}

		// 画图面板的大小
		int workWidth = preWidth;
		int workHeight = preHeight;

		// 是否需要通过所有流程元素来计算画图面板的大小
		boolean flag = false;

		if (workWidth < x) {// 流程元素X坐标大于画图面板宽时
			workWidth = x;
			// 记录最大X坐标对应的流程元素
			this.setMaxXPanel(panel);
		} else if (panel == this.getMaxXPanel() && workWidth > x) {
			// 参数对象是记录中的最大X坐标对象且其X坐标小于画图面板宽时,需要执行通过所有流程元素来计算画图面板的大小
			flag = true;
		}

		if (workHeight < y) {// 流程元素Y坐标大于画图面板高时
			workHeight = y;
			// 记录最大Y坐标对应的流程元素
			this.setMaxYPanel(panel);
		} else if (panel == this.getMaxYPanel() && workHeight > y) {
			// 参数对象是记录中的最大Y坐标对象且其Y坐标小于画图面板高时,需要执行通过所有流程元素来计算画图面板的大小
			flag = true;
		}

		if (flag) {
			// 通过流程元素记录集合计算画图面板的大小
			getCurrDrawPanelSizeByPanelList();
		} else {
			// 设置画图面板大小
			if (workWidth != preWidth || workHeight != preHeight) {
				this.setPreferredSize(new Dimension(workWidth, workHeight));
			}
		}

		this.revalidate();
		this.repaint();
	}

	/**
	 * 
	 * 使用场合：操作对象是记录的最大X/Y坐标的流程对象且其右下角位置点小于当前面板的大小
	 * 
	 * 通过流程元素记录集合计算画图面板的大小
	 * 
	 */
	public void getCurrDrawPanelSizeByPanelList() {
		// 临时X坐标最大的流程元素
		JecnBaseFlowElementPanel maxXPanel = null;
		// Y坐标最大的流程元素
		JecnBaseFlowElementPanel maxYPanel = null;

		// 最大的X坐标
		int workWidth = 0;
		// 最大的Y坐标
		int workHeight = 0;

		for (JecnBaseFlowElementPanel pnael : panelList) {
			int x = 0;
			int y = 0;
			if (pnael instanceof VerticalLine) {// 如果是横线
				// 流程元素的右下角的坐标
				x = pnael.getX() + pnael.getWidth() + JecnSystemStaticData.getWorkflowResizeWidth();
			} else if (pnael instanceof ParallelLines) {
				y = pnael.getY() + pnael.getHeight() + JecnSystemStaticData.getWorkflowResizeHeight();
			} else {
				// 流程元素的右下角的坐标
				x = pnael.getX() + pnael.getWidth() + JecnSystemStaticData.getWorkflowResizeWidth();
				y = pnael.getY() + pnael.getHeight() + JecnSystemStaticData.getWorkflowResizeHeight();
			}

			if (workWidth < x) {// 获取X最大值
				workWidth = x;
				maxXPanel = pnael;
			}
			if (workHeight < y) {
				{// 获取Y最大值
					workHeight = y;
					maxYPanel = pnael;
				}
			}
		}

		if (maxXPanel != null && maxXPanel != this.getMaxXPanel()) {// 最大流程元素不是同一个对象，替换原来的对象
			this.setMaxXPanel(maxXPanel);
		}
		if (maxYPanel != null && maxYPanel != this.getMaxYPanel()) {// 最大流程元素不是同一个对象，替换原来的对象
			this.setMaxYPanel(maxYPanel);
		}

		// 系统给定的画图面板的最小大小
		Dimension wSize = JecnUIUtil.getWorkflowSize();
		if (workWidth < wSize.width) {//
			// 当计算出来的画图面板宽小于系统给定的最小宽度时，使用系统最小宽度替换计算的值
			workWidth = wSize.width;
		}
		if (workHeight < wSize.height) {//
			// 当计算出来的画图面板高小于系统给定的最小高度时，使用系统最小高度替换计算的值
			workHeight = wSize.height;
		}
		// 期望大小
		Dimension preSize = this.getPreferredSize();

		// 设置画图面板大小
		if (workWidth != preSize.width || workHeight != preSize.height) {
			this.setPreferredSize(new Dimension(workWidth, workHeight));
		}
	}

	/**
	 * 
	 * 设置给定组件的层级，此层级为当前面板最大层级
	 * 
	 * @param comp
	 *            JComponent 组件
	 */
	public void setFlowElemPanelLayer(JComponent comp) {
		if (comp != null) {
			this.setLayer(comp, this.getMaxZOrderIndex());
		}
	}

	/**
	 * 
	 * 中断快速添加图形功能
	 * 
	 */
	public void interruptLinkFigure() {
		// **************清除快捷添加图形显示小三角形**************//
		// 快捷添加图形
		JecnLinkFigureMainProcess linkPro = this.getAidCompCollection().getLinkFigureMainProcess();
		// 立即中断处理
		linkPro.interruptComponent();
		// **************清除快捷添加图形显示小三角形**************//
	}

	/**
	 * 面板变化，重置分割线
	 * 
	 */
	private void rePaintVHLinePanel() {
		// **************设置画图面板数据层大小**************//
		// 画图面板宽
		int workflowWidth = this.getWidth();
		// 画图面板高
		int workflowHeight = this.getHeight();

		// 和数据层大小是否相同
		boolean isSizeSame = this.getFlowMapData().isSizeSame(workflowWidth, workflowHeight);

		if (!isSizeSame) {// 不相同
			// 设置数据层大小
			this.getFlowMapData().setSize(workflowWidth, workflowHeight);
		}
		// **************绘制横竖分割线**************//

		// **************绘制横竖分割线**************//
		// 绘制横竖分割线
		for (JecnBaseVHLinePanel baseVHLinePanel : vhLinePanelList) {
			// 显示/隐藏横竖分割线
			baseVHLinePanel.rePaintVHLinePanel();

			if (baseVHLinePanel.isVisible()) {// 显示情况下才绘制
				// 重新设置位置点和大小
				switch (baseVHLinePanel.getFlowElementData().getLineDirectionEnum()) {
				case horizontal:
					if (baseVHLinePanel.getWidth() != this.getFlowMapData().getWorkflowWidth()) {
						baseVHLinePanel.setVHLineBounds();
						baseVHLinePanel.repaint();
					}
					break;
				case vertical:
					if (baseVHLinePanel.getHeight() != this.getFlowMapData().getWorkflowHeight()) {
						baseVHLinePanel.setVHLineBounds();
						baseVHLinePanel.repaint();
					}
					break;
				}
			}
		}
		// **************绘制横竖分割线**************//
	}

	public JecnBaseFlowElementPanel getMaxXPanel() {
		return maxXPanel;
	}

	public void setMaxXPanel(JecnBaseFlowElementPanel maxXPanel) {
		this.maxXPanel = maxXPanel;
	}

	public JecnBaseFlowElementPanel getMaxYPanel() {
		return maxYPanel;
	}

	public void setMaxYPanel(JecnBaseFlowElementPanel maxYPanel) {
		this.maxYPanel = maxYPanel;
	}

	public List<JecnBaseManhattanLinePanel> getManLineList() {
		return manLineList;
	}

	public List<JecnBaseVHLinePanel> getVhLinePanelList() {
		return vhLinePanelList;
	}

	public List<JecnBaseDividingLinePanel> getDiviLineList() {
		return diviLineList;
	}

	public List<JecnBaseFlowElementPanel> getPanelList() {
		return panelList;
	}

	/**
	 * 添加注释框小线段
	 * 
	 * @param commentLine
	 */
	public void setCommentLineList(CommentLine commentLine) {
		if (commentLineList == null || commentLine == null) {
			return;
		}
		if (commentLineList.contains(commentLine)) {
			return;
		}
		commentLineList.add(commentLine);
	}

	/**
	 * 删除注释框小线段
	 * 
	 */
	public void removeCommentLine(CommentLine commentLine) {
		this.remove(commentLine);
		commentLineList.remove(commentLine);
	}

	public void setLayer(Component c, int layer) {
		super.setLayer(c, layer);
		if (c instanceof JecnLineTextPanel) {// 添加此值是为了连接线有编辑文字时，为了时选中点显示在编辑框上层
			JecnLineTextPanel textPanel = (JecnLineTextPanel) c;
			textPanel.setTextPanelOrder(layer);
		}
	}

	public TempDividingLine getActiveLine() {
		return activeLine;
	}

	public JecnTempManLine getTempLine() {
		return tempLine;
	}

	public void setTempLine(JecnTempManLine tempLine) {
		this.tempLine = tempLine;
	}

	public List<TempDividingLine> getTempDividingList() {
		return tempDividingList;
	}

	public List<CommentLine> getCommentLineList() {
		return commentLineList;
	}

	public JecnKeyHandler getKeyHandler() {
		return keyHandler;
	}

	public List<JecnTempLine> getTempLines() {
		return tempLines;
	}

	public List<JecnPageSetLine> getPageSetLines() {
		return pageSetLines;
	}

	public JecnTempManLine getJecnTempManLine() {
		return jecnTempManLine;
	}

	public void setJecnTempManLine(JecnTempManLine jecnTempManLine) {
		this.jecnTempManLine = jecnTempManLine;
	}

	public List<ErrorPanel> getErrorPanelList() {
		return errorPanelList;
	}

	public void setErrorPanelList(List<ErrorPanel> errorPanelList) {
		this.errorPanelList = errorPanelList;
	}

	public JecnTabbedTitlePanel getTabbedTitlePanel() {
		return tabbedTitlePanel;
	}

	public EditPointType getStartPointType() {
		return startPointType;
	}

	public void setStartPointType(EditPointType startPointType) {
		this.startPointType = startPointType;
	}

	public List<TermFigureAR> getTermFigureList() {
		return termFigureList;
	}

	public List<JecnBaseFlowElementPanel> getModelPanelList() {
		return modelPanelList;
	}

	public boolean isSelectArea() {
		return isSelectArea;
	}

	public void setSelectArea(boolean isSelectArea) {
		this.isSelectArea = isSelectArea;
	}

	public JecnAbstractFlowElementData getFormatPainterData() {
		return formatPainterData;
	}

	public void setFormatPainterData(JecnAbstractFlowElementData formatPainterData) {
		this.formatPainterData = formatPainterData;
	}

	public AuthTipPanel getTipPanel() {
		return tipPanel;
	}

	public void setTipPanel(AuthTipPanel tipPanel) {
		this.tipPanel = tipPanel;
	}

	public List<JecnBaseFigurePanel> getShowEditPointFigures() {
		return showEditPointFigures;
	}
}
