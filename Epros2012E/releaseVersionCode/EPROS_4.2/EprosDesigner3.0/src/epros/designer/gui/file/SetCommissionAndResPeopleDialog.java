package epros.designer.gui.file;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jecn.epros.server.bean.file.FileAttributeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.PeopleSelectCommon;
import epros.designer.gui.system.PersonliableSelectCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.util.JecnUIUtil;

public class SetCommissionAndResPeopleDialog extends JecnDialog {
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();
	private JecnPanel infoPanel = new JecnPanel();
	/** 按钮面板 */
	private JPanel buttonPanel = new JecnPanel();

	/** 流程责任人 */
	private PersonliableSelectCommon personliableSelect = null;

	/** 流程专员 */
	private PeopleSelectCommon commissionerPeopleSelectCommon = null;

	/** 确定按钮 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	/** 取消按钮 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 提示Lab */
	private JLabel verfyLab = new JLabel();

	private FileAttributeBean fileAttributeBean = null;

	private JecnTreeNode selectNode;

	public SetCommissionAndResPeopleDialog(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
		/*
		 * 设置专员和责任人
		 */
		this.setTitle(JecnProperties.getValue("settingCommissionerOrPersonLiable"));
		this.setResizable(true);
		this.setModal(true);
		this.setLocationRelativeTo(null);
		this.setSize(this.getWidthMin(), 150);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		try {
			fileAttributeBean = ConnectionPool.getFileAction().getFileAttributeBeanById(
					selectNode.getJecnTreeBean().getId());
		} catch (Exception e) {
			log.error("SetCommissionAndResPeopleDialog SetCommissionAndResPeopleDialog is error", e);
		}
		initLayout();
	}

	/***************************************************************************
	 * 布局
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(3, 3, 3, 3);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());
		// 责任人
		personliableSelect = new PersonliableSelectCommon(0, infoPanel, insets, fileAttributeBean.getResPeopleId(),
				fileAttributeBean.getResPeopleType(), fileAttributeBean.getRePeopleName(), this, 2, false);
		this.getContentPane().add(mainPanel);
		// 专员
		this.commissionerPeopleSelectCommon = new PeopleSelectCommon(1, infoPanel, insets, fileAttributeBean
				.getCommissionerId(), fileAttributeBean.getCommissionerName(), this, JecnProperties.getValue("commissioner"), false);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);

		cancelBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}

		});
		okBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}

		});
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});

	}

	private boolean isUpdate() {
		if (personliableSelect.getSingleSelectCommon().isUpdate()) {
			return true;
		}
		if (this.commissionerPeopleSelectCommon.isUpdate()) {
			return true;
		}
		return false;
	}

	private void okButPerformed() {
		if (isUpdate()) {
			fileAttributeBean.setResPeopleId(personliableSelect.getSingleSelectCommon().getIdResult());
			fileAttributeBean.setResPeopleType(personliableSelect.getPeopleResType());
			fileAttributeBean.setCommissionerId(commissionerPeopleSelectCommon.getIdResult());
			try {
				ConnectionPool.getFileAction().updateFileAttributeBeanById(fileAttributeBean,
						selectNode.getJecnTreeBean().getId(), JecnConstants.getUserId());
			} catch (Exception e) {
				log.error("SetCommissionAndResPeopleDialog okButPerformed is error", e);
			}
		}
		this.setVisible(false);
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButPerformed();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

}
