package epros.designer.gui.process.guide;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;

import epros.designer.gui.common.JecnConstants;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 添加流程KPI
 * 
 * @author 2012-07-09
 * 
 */
public class AddProcessKPIDialog extends EditProcessKPIDialog {
	private static Logger log = Logger.getLogger(AddProcessKPIDialog.class);
	protected boolean isOperation = false;
	// 流程ID
	private Long flowId = null;
	/** 面板可输入控件 是否修改标识 返回true表示修改，false表示未修改 */
	private boolean isUpdate = false;
	/**流程KPI属性Bean*/
	private JecnFlowKpiNameT flowKpiNameT;
	private KPIShowDialog showDialog;

	public AddProcessKPIDialog(Long flowId,
			KPIShowDialog showDialog) {
		this.flowId = flowId;
		this.showDialog = showDialog;
		this.setLocationRelativeTo(null);
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				if (check()) {// true：需要执行关闭 false：不要执行关闭
					return true;
				}
				return false;
			}
		});
		//数据提供者框监听
		this.dataProviderListener();
	}

	public AddProcessKPIDialog() {
		this.setLocationRelativeTo(null);
	}

	@Override
	public String getKPITile() {
		return JecnProperties.getValue("addFlowKPI");
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		if (showDialog != null) {
			List<JecnFlowKpiNameT> list = showDialog.getKpiShowValues().getKpiNameList();
			for (JecnFlowKpiNameT jecnFlowKpiNameT : list) {
				if (name.equals(jecnFlowKpiNameT.getKpiName().trim())) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void saveData() {
		//当前登录人ID
		Long loginPeopleId = JecnConstants.loginBean.getJecnUser().getPeopleId();
		// KPI名称
		String kpiName = this.kpiNameField.getText().trim();
		// KPI 类型
		String kpiType = (String)this.kpiTypeCombox.getSelectedItem();
		// KPI 值单位名称
		String kpiLongitudinal =  String.valueOf(this.longitudinalCombox
				.getSelectedIndex());
		// kpi数据统计时间\频率
		String kpiTransverse = String.valueOf(this.transverseCombox.getSelectedIndex());
		// kpi定义
		String kpiDefined = this.kpiDefinedArea.getText();
		// 流程kpi计算公式
		String kpiMethods = this.kpiMethodsArea.getText();
		// kpi目标值
		String kpiTarget = this.targetField.getText();
		//KPI目标值比较符号
		int targetOperator= this.targetCombox.getSelectedIndex();
		//数据获取方式
		int dataSourceMethod = this.dataSourceMethodCombox.getSelectedIndex();
		//IT系统
		String itSystemIds = this.itSystemIds;
		String itStytemNames=itSystemArea.getText();
//		Long 
		//数据提供者
		Long kpiDataPeopleId = this.dataProviderId;
		//相关度
		int kpiRelevance = this.kpiRelevanceCombox.getSelectedIndex();
		//指标来源
		int kpiTargetType = this.targetSourceCombox.getSelectedIndex();
		//支撑的一级指标ID
		String firstTargetId = this.kpiFirstTargetId.getText();
		//数据提供者名称
		String kpiDataPeopleName = this.dataProviderField.getText();
		//数据提供者隐藏名称
		String kpiDataPeopleHideName = this.dataProviderHideField.getText();
		//支撑的一级指标内容
		String firstTargetContent = this.kpiFirstTargetArea.getText();
		//选中人员ID后，输入不同的流程名称，最后还是保存与ID对应的名称
		if(kpiDataPeopleName!= null && !"".equals(kpiDataPeopleName) && !kpiDataPeopleHideName.equals(kpiDataPeopleName)){
			kpiDataPeopleName = kpiDataPeopleHideName;
		}
		String kpiPurpose=this.purposeArea.getText();//设置目的
		String kpiPoint=this.pointArea.getText();//测量点
		String kpiPeriod=this.periodArea.getText();//统计周期
		String kpiExplain=this.explainArea.getText();//说明

		// KPI数据Bean
		flowKpiNameT = new JecnFlowKpiNameT();
//		if (kpiType != null
//				&& kpiType.equals(JecnProperties.getValue("effectTarget"))) {
//			flowKpiNameT.setKpiType(0);
//		}
//		if (kpiType != null
//				&& kpiType.equals(JecnProperties.getValue("efficiencyTarget"))) {
//			flowKpiNameT.setKpiType(1);
//		}
		List<String> targetTypeList = this.showDialog.getTargetTypeList();
		if(kpiType != null){
			for(int i=0;i<targetTypeList.size();i++){
				if(targetTypeList.get(i).equals(kpiType)){
					flowKpiNameT.setKpiType(i);
					break;
				}
			}
		}
		flowKpiNameT.setFlowId(flowId);
		flowKpiNameT.setKpiName(kpiName);
		flowKpiNameT.setKpiHorizontal(kpiTransverse);
		flowKpiNameT.setKpiVertical(kpiLongitudinal);
		flowKpiNameT.setKpiDefinition(kpiDefined);
		flowKpiNameT.setKpiStatisticalMethods(kpiMethods);
		flowKpiNameT.setKpiTarget(kpiTarget);
		// 创建时间
		// SimpleDateFormat strFormat = new SimpleDateFormat("yyyy-MM-dd");
		flowKpiNameT.setCreatTime(new Date());
		//KPI目标值比较符号
		flowKpiNameT.setKpiTargetOperator(targetOperator);
		//s数据获取方式
		flowKpiNameT.setKpiDataMethod(dataSourceMethod);
		//数据提供者
		flowKpiNameT.setKpiDataPeopleId(kpiDataPeopleId);
		//相关度
		flowKpiNameT.setKpiRelevance(kpiRelevance);
		//指标来源
		flowKpiNameT.setKpiTargetType(kpiTargetType);
		//支撑的一级指标ID
		flowKpiNameT.setFirstTargetId(firstTargetId);
		//更新时间
		flowKpiNameT.setUpdateTime(new Date());
		//创建人
		flowKpiNameT.setCreatePeopleId(loginPeopleId);
		//更新人
		flowKpiNameT.setUpdatePeopleId(loginPeopleId);
		flowKpiNameT.setKpiDataPeopleName(kpiDataPeopleName);//数据提供者名称
		flowKpiNameT.setFirstTargetContent(firstTargetContent);//支撑的一级指标
		flowKpiNameT.setKpiITSystemIds(itSystemIds);//选择的支持工具ids集合（IT系统）
		
		flowKpiNameT.setKpiPurpose(kpiPurpose);
		flowKpiNameT.setKpiPoint(kpiPoint);
		flowKpiNameT.setKpiExplain(kpiExplain);
		flowKpiNameT.setKpiPeriod(kpiPeriod);
		
		flowKpiNameT.setKpiITSystemNames(itStytemNames);
		
		// 添加流程KPI
		try {
			Long kpiId = ConnectionPool.getProcessAction().addKPI(flowKpiNameT,loginPeopleId);
			flowKpiNameT.setKpiAndId(kpiId);
			isOperation = true;
			this.dispose();
		} catch (Exception e) {
			log.error("AddProcessKPIDialog saveData is error", e);
		}
	}

	@Override
	public void cancelButPerformed() {
		if (check()) {
			this.dispose();
		}
	}

	private boolean check() {
		// kpi名称
		if (this.kpiNameField.getText().toString() != null
				&& !"".equals(this.kpiNameField.getText().toString())) {
			isUpdate = true;
		}
		// kpi定义
		if (!isUpdate&&this.kpiDefinedArea.getText().toString() != null
				&& !"".equals(this.kpiDefinedArea.getText().toString())) {
			isUpdate = true;
		}
		// kpi统计方法
		if (!isUpdate&&this.kpiMethodsArea.getText().toString() != null
				&& !"".equals(this.kpiMethodsArea.getText().toString())) {
			isUpdate = true;
		}
		// kpi目标值
		if (!isUpdate&&this.targetField.getText().toString() != null
				&& !"".equals(this.targetField.getText().toString())) {
			isUpdate = true;
		}
		
		if (!isUpdate&&this.periodArea.getText().toString() != null
				&& !"".equals(this.periodArea.getText().toString())) {
			isUpdate = true;
		}
		
		if (!isUpdate&&this.pointArea.getText().toString() != null
				&& !"".equals(this.pointArea.getText().toString())) {
			isUpdate = true;
		}
		
		if (!isUpdate&&this.purposeArea.getText().toString() != null
				&& !"".equals(this.purposeArea.getText().toString())) {
			isUpdate = true;
		}
		
		if (!isUpdate&&this.explainArea.getText().toString() != null
				&& !"".equals(this.explainArea.getText().toString())) {
			isUpdate = true;
		}
		
		
		if(isUpdate){
			int option = JecnOptionPane.showConfirmDialog(this, JecnProperties
					.getValue("yncancel"), null, JecnOptionPane.YES_NO_OPTION,
					JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return false;
			}
			return true;
		}
		return true;
	}

	public JecnFlowKpiNameT getFlowKpiNameT() {
		return flowKpiNameT;
	}
	
}
