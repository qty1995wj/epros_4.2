package com.jecn.epros.server.service.integration;

import java.util.Date;
import java.util.List;

import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;

/**
 * 内控指引知识库Service接口
 * 
 */
public interface IJecnControlGuideService extends IBaseService<JecnControlGuide, Long> {

	/**
	 * 添加内控指引目录
	 * 
	 * @param jecnControlGuide
	 *            内控指引知识库Bean
	 * @return
	 * @throws Exception
	 */
	public long addJecnControlGuideDir(JecnControlGuide jecnControlGuide) throws Exception;

	/**
	 * 根据文件名称查询文件目录
	 * 
	 * @param name
	 *            所选文件名称
	 * @return
	 * @throws Exception
	 */
	public List<JecnControlGuide> getJecnControlGuideDirsByName(String name) throws Exception;

	/**
	 * 根据父ID查询内控指引目录子节点
	 * 
	 * @param pId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildJecnControlGuideDirs(Long pId) throws Exception;

	/**
	 * 移动节点
	 * 
	 * @param listIds
	 *            要移动的节点ID集合
	 * @param pId
	 *            当前节点父ID
	 * @param updatePersonId
	 *            更新人
	 * @throws Exception
	 */
	public void moveSortControlGuide(List<Long> listIds, Long pId, Long updatePersonId) throws Exception;

	/**
	 * 根据父ID查询内控指引子节点
	 * 
	 * @param pId
	 *            当前节点父ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildJecnControlGuides(Long pId, Long projectId) throws Exception;

	/**
	 * 内控指引目录重命名
	 * 
	 * @param id
	 *            当前目录ID
	 * @param name
	 *            新名称
	 * @param updatePersonId
	 *            更改人
	 * @throws Exception
	 */
	public void reControlGuideDirName(Long id, String name, Long updatePersonId) throws Exception;

	/**
	 * 根据所选ID集合删除
	 * 
	 * @param listIds
	 *            所选ID集合
	 * @throws Exception
	 */
	public void deleteControlGuides(List<Long> listIds, Long peopleId) throws Exception;

	/**
	 * 内控指引节点排序
	 * 
	 * @param list
	 *            节点集合
	 * @param pId
	 *            当前节点父ID
	 * @param updatePersonId
	 *            更改人
	 * @throws Exception
	 */
	public void updateSortControlGuide(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception;

	/**
	 * 根据名称查询内控指引知识库目录
	 * 
	 * @param name
	 *            内控指引目录名称
	 * @return JecnControlGuide集合
	 * @throws Exception
	 */
	public List<JecnTreeBean> getControlGuideByName(String name) throws Exception;

	/**
	 * 根据选中节点查询具体条款内容
	 * 
	 * @param id
	 *            当前选中节点
	 * @return
	 * @throws Exception
	 */
	public JecnControlGuide getJecnControlGuideById(Long id) throws Exception;

	/**
	 * 编辑知识库条款
	 * 
	 * @param jecnControlGuide
	 *            知识库Bean
	 * @return
	 * @throws Exception
	 */
	public boolean updateJecnControlGuide(Long id, String name, String description, Long updatePerson, Date updateTime)
			throws Exception;

}
