package com.jecn.epros.server.connector.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.jecn.epros.server.action.web.login.ad.libang.LiBangAfterItem;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.HttpRequest;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.connector.Libang.LibangDeleteReceivers;
import com.jecn.epros.server.connector.Libang.LibangFinishBean;
import com.jecn.epros.server.connector.Libang.LibangTaskReceivers;
import com.jecn.epros.server.connector.Libang.LibangTasksBean;
import com.jecn.epros.server.connector.Libang.TaskJsonBean;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.webBean.task.MyTaskBean;

public class LiBangTaskInfo extends JecnBaseTaskSend {

	private IPersonDao personDao;

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			log.info("创建代办开始~");
			// 创建代办
			createTasks(myTaskBean, beanNew, handlerPeopleIds);
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 上一阶段改为已办
			JecnUser handlerUser = personDao.get(curPeopleId);
			// handlerUser.getLoginName()
			changeStatusToHandled(myTaskBean, beanNew, handlerUser);
		} else {// 一条from的数据,一条to 的数据
			// 代办转已办,是否为多审判断
			// 设置上一环节任务待办任务状态为false，表示未已审批；
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			JecnUser handlerUser = personDao.get(curPeopleId);
			// 上一个阶段，处理任务，代办转已办
			changeStatusToHandled(myTaskBean, beanNew, handlerUser);

			if ((beanNew.getTaskElseState() == 2 || beanNew.getTaskElseState() == 3)
					|| beanNew.getState() != beanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				// 审批任务，发送下一环节任务待办
				createTasks(myTaskBean, beanNew, handlerPeopleIds);
			}
		}
	}

	/**
	 * 批量删除代办
	 * 
	 * 1、审批人撤回，删除上一阶段待办，创建当前审批人待办 2、删除任务，删除所有审批人待办
	 * 
	 */
	@Override
	public void sendInfoCancels(JecnTaskBeanNew taskBeanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {

		this.personDao = personDao;
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(taskBeanNew);
		Set<Long> setCancelPeopleIds = new HashSet<Long>();
		setCancelPeopleIds.addAll(cancelPeoples);
		// 上一个阶段，代办删除
		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			// 删除任务
			for (Long cancelPeopleId : cancelPeoples) {
				deletePCTASK(taskBeanNew, taskBeanNew.getId(), cancelPeopleId);
			}
		} else {
			// 删除人员
			deletePCRUserTASK(taskBeanNew, setCancelPeopleIds, taskBeanNew.getId());
		}

		// 创建微信待办
		getCreateAPPTask(taskBeanNew, setCancelPeopleIds, false);

		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		// 审批任务，发送下一环节任务待办
		createTasks(myTaskBean, taskBeanNew, handlerPeopleIds);
	}

	private void deletePCTASK(JecnTaskBeanNew taskBeanNew, Long id, Long handlerPeopleId) throws IOException {
		Map<String, String> deleteTaskMap = new LinkedHashMap<String, String>();
		deleteTaskMap.put("AppCode", LiBangAfterItem.getValue("TaskAppCode"));
		deleteTaskMap.put("RefID", getTaskOrderId(taskBeanNew, handlerPeopleId, 0));
		httpPost(LiBangAfterItem.getValue("TASK_DEL_URL"), deleteTaskMap);
	}

	private String httpPost(String url, Object object) {
		String result = null;
		try {
			result = HttpRequest.httpPost(url, object);
			log.info("result = " + result);
		} catch (IOException e) {
			e.printStackTrace();
			log.error(" ", e);

		}
		return url;
	}

	private void deletePCRUserTASK(JecnTaskBeanNew taskBeanNew, Set<Long> setCancelPeopleIds, Long taskId)
			throws Exception {
		List<JecnUser> users = personDao.getJecnUserList(setCancelPeopleIds);
		LibangDeleteReceivers deleteReceivers = new LibangDeleteReceivers();
		for (JecnUser jecnUser : users) {
			List<String> receivers = new ArrayList<String>();
			for (JecnUser user : users) {
				receivers.add(user.getLoginName());
			}
			deleteReceivers.setAppCode(LiBangAfterItem.getValue("TaskAppCode"));
			deleteReceivers.setReceivers(receivers);
			deleteReceivers.setRefID(getTaskOrderId(taskBeanNew, jecnUser.getPeopleId(), 0));

			httpPost(LiBangAfterItem.getValue("TASK_DEL_REVOKE_URL"), deleteReceivers);
		}
	}

	/**
	 * 代办转已办
	 */
	private void changeStatusToHandled(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, JecnUser handlerUser)
			throws Exception {
		LibangFinishBean finishBean = finishTask(taskBeanNew, handlerUser);
		log.info("finishBean = " + finishBean.toString());
		httpPost(LiBangAfterItem.getValue("TASK_FINISH_URL"), finishBean);

		log.info("微信待办转已办  ！");
		// 创建微信待办
		changeAPPTaskToFinish(taskBeanNew, handlerUser);
	}

	/**
	 * 待办审批完成（待办转已办）
	 * 
	 * @param taskBeanNew
	 * @param receiver
	 *            接收人
	 * @return
	 */
	private LibangFinishBean finishTask(JecnTaskBeanNew taskBeanNew, JecnUser handlerUser) {
		LibangFinishBean finishBean = new LibangFinishBean();
		finishBean.setAppCode(LiBangAfterItem.getValue("TaskAppCode"));
		// 域账号
		finishBean.setReceiver(getEmailName(handlerUser.getEmail()));
		// 全局唯一标识
		finishBean.setRefID(getTaskOrderId(taskBeanNew, handlerUser.getPeopleId(), 0));
		return finishBean;
	}

	/**
	 * 创建代办
	 * 
	 * @param myTaskBean
	 * @param taskBeanNew
	 * @throws Exception
	 */
	private void createTasks(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, Set<Long> handlerPeopleIds)
			throws Exception {

		try {
			JecnUser createUser = personDao.get(taskBeanNew.getCreatePersonId());
			TaskJsonBean jsonBean = null;

			for (Long handlerUserId : handlerPeopleIds) {
				jsonBean = new TaskJsonBean();
				JecnUser handlerUser = personDao.get(handlerUserId);
				LibangTasksBean pendingTask = getCreatePCTask(myTaskBean, taskBeanNew, handlerUser, createUser);
				// 因为返回json 格式要求 需要包装一层
				if (pendingTask != null) {
					jsonBean.setTask(pendingTask);
				}
				log.info("pendingTask = " + pendingTask.toString());
				httpPost(LiBangAfterItem.getValue("TASK_CREATE_URL"), jsonBean);
			}

			log.info("创建微信待办开始  ！");
			// 创建微信待办
			getCreateAPPTask(taskBeanNew, handlerPeopleIds, true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
	}

	private LibangTasksBean getCreatePCTask(MyTaskBean myTaskBean, JecnTaskBeanNew taskBeanNew, JecnUser handlerUser,
			JecnUser createUser) throws Exception {

		LibangTasksBean tasksBean = new LibangTasksBean();

		tasksBean.setAppCode(LiBangAfterItem.getValue("TaskAppCode"));
		tasksBean.setCategoryCode(LiBangAfterItem.getValue("CategoryCode"));

		// 创建人 createUser.getLoginName()
		tasksBean.setInitiator(getEmailName(createUser.getEmail()));
		tasksBean.setInitiatorName(createUser.getTrueName());
		long curTime = System.currentTimeMillis();
		tasksBean.setReceiveDate((int) curTime);

		// 待审批人计划
		tasksBean.setReceivers(getReceivers(handlerUser));
		// 全局唯一标识
		tasksBean.setRefID(getTaskOrderId(taskBeanNew, handlerUser.getPeopleId(), 1));
		// 发起人createUser.getLoginName()
		tasksBean.setSender(getEmailName(createUser.getEmail()));
		tasksBean.setSenderName(createUser.getTrueName());

		tasksBean.setTitle(taskBeanNew.getTaskName());
		tasksBean.setUrl(getPCUrl(myTaskBean.getTaskId(), handlerUser.getPeopleId()));

		return tasksBean;
	}

	/**
	 * 创建微信待办
	 * 
	 * @param myTaskBean
	 * @param taskBeanNew
	 * @param handlerPeopleIds
	 * @param createUser
	 * @throws Exception
	 */
	private void getCreateAPPTask(JecnTaskBeanNew taskBeanNew, Set<Long> handlerPeopleIds, boolean isAdd)
			throws Exception {
		String appId = LiBangAfterItem.getValue("APPID");

		String secretKey = LiBangAfterItem.getValue("SecretKey");

		String userName = LiBangAfterItem.getValue("UserName");
		JecnUser createUser = personDao.get(taskBeanNew.getCreatePersonId());

		List<JecnUser> users = personDao.getJecnUserList(handlerPeopleIds);
		for (JecnUser handlerUser : users) {
			String xmlString = getXmlByTaskBeans(taskBeanNew, handlerUser, isAdd, createUser);
			String result = sendReq(LiBangAfterItem.getValue("APP_TASK_URL"), getSoapXml(xmlString, appId, userName,
					secretKey));
			System.out.println(result);
			log.info("app task result= " + result);
		}

	}

	private void changeAPPTaskToFinish(JecnTaskBeanNew taskBeanNew, JecnUser handlerUser) throws Exception {
		String appId = LiBangAfterItem.getValue("APPID");

		String secretKey = LiBangAfterItem.getValue("SecretKey");

		String userName = LiBangAfterItem.getValue("UserName");
		JecnUser createUser = personDao.get(taskBeanNew.getCreatePersonId());

		String xmlString = getXmlByTaskBeans(taskBeanNew, handlerUser, false, createUser);
		String result = sendReq(LiBangAfterItem.getValue("APP_TASK_URL"), getSoapXml(xmlString, appId, userName,
				secretKey));
		System.out.println(result);
		log.info("app task result= " + result);

	}

	private String getSoapXml(String xmlString, String appId, String userName, String secretKey) {
		if (StringUtils.isEmpty(xmlString) || StringUtils.isEmpty(appId)) {
			return "";
		}
		StringBuffer str = new StringBuffer();
		str
				.append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'  xmlns:tem='http://tempuri.org/'> ");
		/** 头信息 */
		str.append("<soapenv:Header>");
		str.append("<tem:UserValidationSoapHeader> ");
		str.append("<tem:AppId>" + appId + "</tem:AppId>  ");
		str.append(" <tem:SecretKey>" + secretKey + "</tem:SecretKey> ");
		str.append("</tem:UserValidationSoapHeader> ");
		str.append("</soapenv:Header>");

		/** Body */
		str.append("<soapenv:Body> ");
		str.append("  <tem:DoProcess>  ");
		str.append("  <tem:APPID>" + appId + "</tem:APPID>");
		str.append("  <tem:xml><![CDATA[" + xmlString + "]]></tem:xml>");
		str.append("   <tem:formHTML></tem:formHTML>   ");
		str.append(" <tem:approveHTML></tem:approveHTML>");
		str.append("  </tem:DoProcess> </soapenv:Body> </soapenv:Envelope>");
		return str.toString();
	}

	private String getXmlByTaskBeans(JecnTaskBeanNew taskBeanNew, JecnUser approveUser, boolean isAdd,
			JecnUser createUser) {
		Document doc = null;
		try {
			doc = DocumentHelper.createDocument();
			// 1、设置root
			Element root = doc.addElement("DATA");
			Element ele = null;
			ele = root.addElement("PROCINSTNO");
			ele.setText(taskBeanNew.getId().toString());
			ele = root.addElement("TOPIC");
			ele.setText(taskBeanNew.getTaskDesc());
			ele = root.addElement("APPLYEMPNO");
			// createUser.getLoginName()
			ele.setText(approveUser.getLoginName());
			ele = root.addElement("APPLYEMPNAME");
			ele.setText(createUser.getTrueName());
			ele = root.addElement("STARTDATE");
			ele.setText(JecnCommon.getStringbyDate(taskBeanNew.getUpdateTime()));
			ele = root.addElement("STATUS");
			ele.setText(isAdd ? "审批中" : "审批通过");
			ele = root.addElement("FORMURL");
			ele.setText(getAppUrl(taskBeanNew.getId(), approveUser.getPeopleId()));

			Element JOBLIST = root.addElement("JOBLIST");

			Element item = JOBLIST.addElement("ITEM");
			ele = item.addElement("JOBID");
			ele.setText(taskBeanNew.getId().toString());

			ele = item.addElement("USERID");
			// approveUser.getLoginName()
			ele.setText(approveUser.getLoginName());

			ele = item.addElement("ARRIVALDATE");
			ele.setText(JecnCommon.getStringbyDateHMS(taskBeanNew.getUpdateTime()));

			ele = item.addElement("STATUS");
			ele.setText(isAdd ? "0" : "4");
			return root.asXML();
		} catch (Exception e) {
			log.error("构建dom文档异常", e);
			return null;
		} finally {
			doc = null;
		}
	}

	protected String getPCUrl(long taskId) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("获取系统配置URL为空!");
		}
		return basicEprosURL + "loginMail.action?accessType=openApproveDomino&mailTaskId=" + taskId;
	}

	protected String getPCUrl(long taskId, long loginId) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("获取系统配置URL为空!");
		}
		return basicEprosURL + "loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
				+ loginId + "&isApp=" + false + "&isView=" + false;
	}

	protected String getAppUrl(long taskId, long loginId) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		if (StringUtils.isBlank(basicEprosURL)) {
			throw new IllegalArgumentException("获取系统配置URL为空!");
		}
		return basicEprosURL + "loginMail.action?accessType=mailApprove&mailTaskId=" + taskId + "&mailPeopleId="
				+ loginId + "&isApp=" + true + "&isView=" + false;
	}

	private List<LibangTaskReceivers> getReceivers(JecnUser handlerUser) throws Exception {
		List<LibangTaskReceivers> taskReceivers = new ArrayList<LibangTaskReceivers>();
		LibangTaskReceivers bean = null;
		bean = new LibangTaskReceivers();
		// user.getLoginName()
		bean.setLoginName(getEmailName(handlerUser.getEmail()));
		bean.setDisplayName(handlerUser.getTrueName());
		taskReceivers.add(bean);
		return taskReceivers;
	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(getTaskName(beanNew));
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setStageName(beanNew.getStateMark());
		return myTaskBean;
	}

	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew, JecnUser jecnUser, JecnUser createUser) {
		List<Long> cancelPeoples = new ArrayList<Long>();
		cancelPeoples.add(curPeopleId);
		try {
			sendInfoCancels(jecnTaskBeanNew, personDao, null, cancelPeoples, curPeopleId);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
	}

	// post 发送xml 报文
	private String sendReq(String url1, String paraXml) { // type: dept post
		// person
		String urlStr = url1;
		System.out.println(paraXml);
		OutputStream out = null;
		String respData = "";
		URL url;
		try {
			url = new URL(urlStr);

			HttpURLConnection con;
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestMethod("POST");
			con.setUseCaches(false);
			con.setConnectTimeout(1200000);
			con.setReadTimeout(1200000);
			con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
			// con.setRequestProperty("WSS-Password Type", "PasswordText");

			// con.setRequestProperty("SOAPAction", soapAction);
			// con.setRequestProperty("Encoding", "UTF-8");
			out = con.getOutputStream();
			out.flush();
			con.getOutputStream().write(paraXml.getBytes("utf-8"));
			out.close();
			int code = con.getResponseCode();
			String tempString = null;
			StringBuffer sb1 = new StringBuffer();
			if (code == HttpURLConnection.HTTP_OK) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
				while ((tempString = reader.readLine()) != null) {
					sb1.append(tempString);
				}
				if (null != reader) {
					reader.close();
				}
			} else {
				BufferedReader reader = new BufferedReader(new InputStreamReader(con.getErrorStream(), "utf-8"));
				// 一次读入一行，直到读入null为文件结束
				while ((tempString = reader.readLine()) != null) {
					sb1.append(tempString);
				}
				if (null != reader) {
					reader.close();
				}
			}
			// 响应报文
			// String respData =
			// StringEscapeUtils.unescapeHtml4(sb1.toString());
			respData = sb1.toString();
			System.out.println(respData);
			// // 通过read方法读取一个文件 转换成Document对象
			// Document document = DocumentHelper.parseText(respData);
			// // 获取根节点元素对象
			// Element node = document.getRootElement();
			// // 遍历所有的元素节点
			// // listNodes(node);
			// DefaultXPath xpath = new
			// DefaultXPath("//ns1:getUpdatedElementsResponse");
			// xpath.setNamespaceURIs(Collections.singletonMap("ns1",
			// "http://webservice.notify.sys.kmss.landray.com/"));
			//
			// Element ns = (Element)
			// node.selectNodes("/soap:Envelope/soap:Body").get(0);
			// Element re = (Element) xpath.selectNodes(ns).get(0);
			// Element returnEle = re.element("return");
			// Element message = returnEle.element("message");
			// count = Integer.parseInt(returnEle.element("count").getText());
			// beginTime = returnEle.element("timeStamp").getText();
			// messageText = message.getText();
			// String a = node.element("message").getText();
			// return messageText;
		} catch (MalformedURLException e) {
			log.error("链接出错");
			e.printStackTrace();
		} catch (IOException e) {
			log.error("请求出错");
			e.printStackTrace();
		}
		return respData;
	}

	/**
	 * 根据邮箱获取 域账号
	 * 
	 * @param emaiil
	 * @return
	 */
	private String getEmailName(String email) {
		if (StringUtils.isEmpty(email)) {
			return "";
		}
		if (!email.matches("[\\w\\.\\-]+@([\\w\\-]+\\.)+[\\w\\-]+")) {
			System.out.println("邮箱格式错误");
			log.error("邮箱格式错误：" + email);
		}
		return email.split("@")[0];
	}

}
