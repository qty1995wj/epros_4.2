package com.jecn.webservice.xinghan.bean;

/**
 * 流程节点属性
 * 
 * @author admin
 * 
 */
public class FlowNode {
	/** 节点ID */
	private long nodeId;
	/** 节点名称 */
	private String nodeName;
	/** 节点类型（开始0、审批1、结束8、分流4、合流5） **/
	private String nodeType;
	/*** X坐标 **/
	private int xNodePoint;
	/*** Y坐标 *****/
	private int yNodePoint;

	public String getNodeId() {
		if (nodeId == 0) {
			return "";
		}
		return String.valueOf(nodeId);
	}

	public void setNodeId(long nodeId) {
		this.nodeId = nodeId;
	}

	public String getNodeName() {
		if(nodeName==null){
			return "";
		}
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public String getxNodePoint() {
		if (xNodePoint == 0) {
			return "";
		}
		return String.valueOf(xNodePoint);
	}

	public void setxNodePoint(int xNodePoint) {
		this.xNodePoint = xNodePoint;
	}

	public String getyNodePoint() {
		if (yNodePoint == 0) {
			return "";
		}
		return String.valueOf(yNodePoint);
	}

	public void setyNodePoint(int yNodePoint) {
		this.yNodePoint = yNodePoint;
	}

}
