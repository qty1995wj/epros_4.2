package com.jecn.epros.server.download.wordxml.approve;

import java.util.List;

public class ApproveState {
	private Integer state;
	private String stateName;
	private List<ApprovePeople> peoples;
	
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public List<ApprovePeople> getPeoples() {
		return peoples;
	}
	public void setPeoples(List<ApprovePeople> peoples) {
		this.peoples = peoples;
	}
}
