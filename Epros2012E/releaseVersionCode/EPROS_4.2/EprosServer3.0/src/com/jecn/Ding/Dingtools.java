package com.jecn.Ding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

public class Dingtools {

	private static final Logger log = Logger.getLogger(Dingtools.class);
	
	/**
	 * @author Angus
	 * 获得钉钉的token
	 * @return
	 */
	public static String getToken(){
		//https://oapi.dingtalk.com/gettoken  参数   appkey=key&appsecret=secret
		String url = DingAfterItem.getValue("URL_GET_TOKEN");
		String param = "appkey="+DingAfterItem.getValue("APP_KEY")+"&appsecret="+DingAfterItem.getValue("APP_SECRET");
		String response = sendGet(url,param);
		JSONObject json = JSONObject.parseObject(response);
		String errcode = json.getString("errcode");
		if("0".equals(errcode)){
			return json.getString("access_token");
		}
		log.error("getDingTokenError:"+json.toJSONString());
		return "";
	}
	
	/**
	 * @author Angus
	 * 获得钉钉的jsapi_ticket
	 * @return
	 */
	public static String getJSAPITicket(){
		//https://oapi.dingtalk.com/get_jsapi_ticket?access_token=ACCESS_TOKE
		String token = getToken();
		String url = DingAfterItem.getValue("URL_JSAPI_TICKET");
		String response = sendGet(url,"access_token=" + token);
		JSONObject json = JSONObject.parseObject(response);
		String errcode = json.getString("errcode");
		if("0".equals(errcode)){
			return json.getString("ticket");
		}
		log.error("getDingJSAPITicketError:"+json.toJSONString());
		return "";
	}
	
	/**
	 * @author Angus
	 * 获得钉钉的jsapi_ticket
	 * @return
	 */
	public static JSONObject getUseridByCode(String code){
		//https://oapi.dingtalk.com/user/getuserinfo?access_token=access_token&code=authCode
		String token = getToken();
		String url = DingAfterItem.getValue("URL_USERID_CODE");
		String param = "access_token=" + token + "&code=" + code;
		String response = sendGet(url,param);
		JSONObject json = JSONObject.parseObject(response);
		String errcode = json.getString("errcode");
		if("0".equals(errcode)){
			return json;
		}
		log.error("getDingUserByCodeError:"+json.toJSONString());
		return null;
	}
	
	/**
	 * * @param taskJson
	 *  userid	String	必须	manager70	待办事项对应的用户id
		create_time	Long	必须	1496678400000	待办时间。Unix时间戳，毫秒级
		title	String	必须	标题	待办事项的标题
		url	String	必须	https://oa.dingtalk.com	待办事项的跳转链接
		formItemList	List	必须		待办事项表单
			└title	String	必须	表单标题	表单标题
			└content	String	必须	表单内容	表单内容
	 * @return  
		record_id	待办事项唯一id，更新待办事项的时候需要用到
	 */
	public static JSONObject workAdd(String taskJson){
		//https://oapi.dingtalk.com/topapi/workrecord/add?access_token=ACCESS_TOKEN
		String token = getToken();
		String url = DingAfterItem.getValue("URL_WORKRECORD_ADD") + "?access_token=" + token;
		
		String response = sendPost(url,taskJson);
		JSONObject json = JSONObject.parseObject(response);
		String errcode = json.getString("errcode");
		if("0".equals(errcode)){
			return json;
		}
		log.error("addDingWorkError:"+json.toJSONString());
		return null;
	}
	
	/**
	 * * @param taskJson
	 *  userid	String	必须	manager70	待办事项对应的用户id
		offset	Number	必须	0	分页游标，从0开始，如返回结果中has_more为true，则表示还有数据，offset再传上一次的offset+limit
		limit	Number	必须	50	分页大小，最多50
		status	Number	必须	1	待办事项状态，0表示未完成，1表示完成
	 * @return  
		record_id	待办事项唯一id，更新待办事项的时候需要用到
	 */
	public static JSONObject workGet(String taskJson){
		//https://oapi.dingtalk.com/topapi/workrecord/getbyuserid?access_token=ACCESS_TOKEN
		String token = getToken();
		String url = DingAfterItem.getValue("URL_WORKRECORD_GET") + "?access_token=" + token;
		
		String response = sendPost(url,taskJson);
		JSONObject json = JSONObject.parseObject(response);
		String errcode = json.getString("errcode");
		if("0".equals(errcode)){
			return json;
		}
		log.error("getDingWorkError:"+json.toJSONString());
		return null;
	}
	
	/**
	 * * @param taskJson
	 *  userid	String	必须	manager70	待办事项对应的用户id
		record_id	String	必须	record123	待办事项唯一id
	 * @return  
		errcode	返回码
		errmsg	对返回码的文本描述内容
		result	true表示更新成功，false表示更新失败
	 */
	public static JSONObject workUpdate(String taskJson){
		//https://oapi.dingtalk.com/topapi/workrecord/update?access_token=ACCESS_TOKEN
		String token = getToken();
		String url = DingAfterItem.getValue("URL_WORKRECORD_UPDATE") + "?access_token=" + token;
		
		String response = sendPost(url,taskJson);
		JSONObject json = JSONObject.parseObject(response);
		String errcode = json.getString("errcode");
		if("0".equals(errcode)){
			return json;
		}
		log.error("updateDingWorkError:"+json.toJSONString());
		return null;
	}
	
	/**
	 * * @param taskJson
		agent_id	Number	必须	1234	企业自建应用是微应用agentId，第三方应用是通过获取授权企业的应用信息接口/service/get_agent获取到的agentId
		userid_list	String	可选(userid_list,dept_id_list, to_all_user必须有一个不能为空)	zhangsan,lisi	接收者的用户userid列表，最大列表长度：20
		dept_id_list	String	可选	123,456	接收者的部门id列表，最大列表长度：20,  接收者是部门id下(包括子部门下)的所有用户
		to_all_user	Boolean	可选	false	是否发送给企业全部用户(ISV不能设置true)
		msg	json对象	必须	{"msgtype":"text","text":{"content":"消息内容"}}	消息内容，具体见“消息类型与数据格式”。最长不超过2048个字节
	 * @return  
		record_id	待办事项唯一id，更新待办事项的时候需要用到
	 */
	public static JSONObject sendMessage(String msgJson){
		//https://oapi.dingtalk.com/topapi/workrecord/add?access_token=ACCESS_TOKEN
		String token = getToken();
		String url = DingAfterItem.getValue("URL_MESSAGE") + "?access_token=" + token;
		
		String response = sendPost(url,msgJson);
		JSONObject json = JSONObject.parseObject(response);
		String errcode = json.getString("errcode");
		if("0".equals(errcode)){
			return json;
		}
		log.error("sendDingMessageError:"+json.toJSONString());
		return null;
	}
	
	
	 /**
     * 向指定URL发送GET方法的请求
     * 
     * @param url
     *            发送请求的URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     */
    public static String sendGet(String url, String param) {
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + param;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
//            for (String key : map.keySet()) {
//                System.out.println(key + "--->" + map.get(key));
//            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(),"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！"+url+param);
            log.error(e.toString());
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }
	
	/**
	 * 向指定 URL 发送POST方法的请求
	 * 
	 * @param url
	 *            发送请求的 URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return 所代表远程资源的响应结果
	 */
	public static String sendPost(String url, String param) {
		OutputStreamWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) realUrl.openConnection();
			// 打开和URL之间的连接

			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST"); // POST方法
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			conn.connect();

			// 获取URLConnection对象对应的输出流
			out = new OutputStreamWriter(conn.getOutputStream(), "utf-8");
			// 发送请求参数
			out.write(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
            System.out.println("发送POST请求出现异常！"+url+param);
            log.error(e.toString());
            e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	
	public static String sign(String ticket, String nonceStr, long timeStamp, String url) throws Exception {
		String plain = "jsapi_ticket=" + ticket + "&noncestr=" + nonceStr + "&timestamp=" + String.valueOf(timeStamp)
				+ "&url=" + url;
		try {
			MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
			sha1.reset();
			sha1.update(plain.getBytes("UTF-8"));
			return bytesToHex(sha1.digest());
		} catch (NoSuchAlgorithmException e) {
			throw new Exception(e.getMessage());
		} catch (UnsupportedEncodingException e) {
			throw new Exception(e.getMessage());
		}
	}

	private static String bytesToHex(byte[] hash) {
		Formatter formatter = new Formatter();
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}
	
	public static void main(String[] args){
        
        String s = "{" +
                "     \"msgtype\": \"oa\"," +
                "     \"oa\": {" +
                "        \"message_url\": \"http://192.168.1.133:8080/EprosServer3.0/ddPC.html\"," +
                "        \"head\": {" +
                "            \"bgcolor\": \"FFBBBBBB\"," +
                "            \"text\": \"EPROS\"" +
                "        }," +
                "        \"body\": {" +
                "            \"title\": \"正文标题\"," +
                "            \"form\": [" +
                "                {" +
                "                    \"key\": \"姓名:\"," +
                "                    \"value\": \"张三\"" +
                "                }," +
                "                {" +
                "                    \"key\": \"年龄:\"," +
                "                    \"value\": \"20\"" +
                "                }," +
                "                {" +
                "                    \"key\": \"身高:\"," +
                "                    \"value\": \"1.8米\"" +
                "                }," +
                "                {" +
                "                    \"key\": \"体重:\"," +
                "                    \"value\": \"130斤\"" +
                "                }," +
                "                {" +
                "                    \"key\": \"学历:\"," +
                "                    \"value\": \"本科\"" +
                "                }," +
                "                {" +
                "                    \"key\": \"爱好:\"," +
                "                    \"value\": \"打球、听音乐\"" +
                "                }" +
                "            ]," +
                "            \"rich\": {" +
                "                \"num\": \"15.6\"," +
                "                \"unit\": \"元\"" +
                "            }," +
                "            \"content\": \"大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本大段文本\"," +
//                "            \"image\": \"@lADOADmaWMzazQKA\"," +
                "            \"file_count\": \"3\"," +
                "            \"author\": \"李四 \"" +
                "        }" +
                "    }" +
                "}";
        String tt = "{    \"msgtype\": \"markdown\",    \"markdown\": {\"text\":\"### 您好  \n*2[进入](http://192.168.1.133:8080/EprosServer3.0ddTask.html?t=7154&p=null&v=false)\",\"title\":\"任务  springBoot 已提交，请审批！\"}}";
        String msg = "{" +
                "    \"agent_id\": "+DingAfterItem.getValue("AGENT_ID")+"," +
                "    \"userid_list\": \"0359333719957370250\"," +
                "    \"msg\": "+tt+"" +
                "}";
        String t = "{\"agent_id\":218162580,\"userid_list\":\"0359333719957370250\",\"msg\":{\"msgtype\":\"oa\",\"oa\":{\"message_url\":\"http://192.168.1.133:8080/EprosServer3.0/dd.html\",\"pc_message_url\":\"http://192.168.1.133:8080/EprosServer3.0/ddPC.html\",\"head\":{\"bgcolor\":\"FFBBBBBB\",\"text\":\"EPROS\"},\"body\":{\"title\":\"EPROS标题\",\"content\":\"EPROS巴拉巴拉测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试\"}}}}";
		JSONObject a = sendMessage(msg);
		System.out.print(a);
	}
}
