<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript"
			src="${basePath}js/reports/processRole.js">
</script>
		<script type="text/javascript">
document.body.onresize = function() {
	setReportsDivWidthHeight('sysGrid_id');
}
</script>
	</head>

	<body>
		<div id="div" class="div" style="height: 99%;">
			<form action="" id="ruleForm">
				<div id="search" class="divBorder" align="center">
					<div id="searchText" style="overflow: hidden;" class="minWidth"
						align="center">
						<table width="100%" align="center">
							<tr align="center">
								<td height="25px">
									<!-- 流程名称： -->
									<s:text name="processNameC"></s:text>
									<input type="text" style="width: 25%; vertical-align: middle;"
										id="processName_id" maxLength="32" readonly="readonly"/>
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectProcessOrMaWindow("processName_id","processId_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
								</td>
							</tr>
						</table>
					</div>

					<div id="searchBut" align="center">
						<img src="${basePath}images/iParticipateInProcess/search.gif"
							style="cursor: pointer;" onclick="processRolePieCharts()" />
						<img src="${basePath}images/iParticipateInProcess/reset.gif"
							style="cursor: pointer;" onclick="processRoleReset()" />
					</div>
				</div>
				<!-- 流程ID -->
				<input type="hidden" id="processId_id" value="-1" />
			</form>
			<div id="sysGrid_id" class="divBorder" style="overflow-y: scroll;overflow-x:hidden;">

			</div>
			<div align="center">
				<a onclick="processRoleDownloadExcel()"
					style="cursor: pointer; color: #005EA7;" class="table"><s:text
						name="downLoadExcel" /> </a>
			</div>
		</div>
	</body>
</html>