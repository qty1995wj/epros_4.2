package com.jecn.svg.dataModel.graphs;

import java.util.List;

import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.svg.dataModel.SvgBaseGraph;
import com.jecn.svg.dataModel.epros.TimeLineNode;

public class SvgTimeLine extends SvgBaseGraph {

	private List<TimeLineNode> nodes;
	/** 目标值 */
	private Double sumTargetValue;
	/** 现状值 */
	private Double sumStatusValue;

	public Double getSumTargetValue() {
		return sumTargetValue;
	}

	public void setSumTargetValue(Double sumTargetValue) {
		this.sumTargetValue = sumTargetValue;
	}

	public Double getSumStatusValue() {
		return sumStatusValue;
	}

	public void setSumStatusValue(Double sumStatusValue) {
		this.sumStatusValue = sumStatusValue;
	}

	public List<TimeLineNode> getNodes() {
		return nodes;
	}

	public void setNodes(List<TimeLineNode> nodes) {
		this.nodes = nodes;
	}

	public String getTimeLineUtil() {
		return JecnConfigTool.getActiveTimeLineUtil();
	}

}
