package com.jecn.epros.server.bean.process;



/**
 * JecnFlowBasicInfo entity.
 * 
 * @author MyEclipse Persistence Tools
 */
public class JecnFlowBasicInfoT2 implements java.io.Serializable {
	private Long flowId;//主键ID（流程ID）
	private String flowRelatedFile;//相关文件
	private String flowCustomOne;//自定义要素1
	private String flowCustomTwo;//自定义要素2
	private String flowCustomThree;//自定义要素3
	private String flowCustomFour;//自定义要素4
	private String flowCustomFive;//自定义要素5

	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public String getFlowCustomOne() {
		if(!isNullOrEmtryTrim(flowCustomOne)){
			return flowCustomOne;
		}else{
			return "";
		}
	}
	public void setFlowCustomOne(String flowCustomOne) {
		this.flowCustomOne = flowCustomOne;
	}
	public String getFlowCustomTwo() {
		if(!isNullOrEmtryTrim(flowCustomTwo)){
			return flowCustomTwo;
		}else{
			return "";
		}
	}
	public void setFlowCustomTwo(String flowCustomTwo) {
		this.flowCustomTwo = flowCustomTwo;
	}
	public String getFlowCustomThree() {
		if(!isNullOrEmtryTrim(flowCustomThree)){
			return flowCustomThree;
		}else{
			return "";
		}
	}
	public void setFlowCustomThree(String flowCustomThree) {
		this.flowCustomThree = flowCustomThree;
	}
	public String getFlowCustomFour() {
		if(!isNullOrEmtryTrim(flowCustomFour)){
			return flowCustomFour;
		}else{
			return "";
		}
	}
	public void setFlowCustomFour(String flowCustomFour) {
		this.flowCustomFour = flowCustomFour;
	}
	public String getFlowCustomFive() {
		if(!isNullOrEmtryTrim(flowCustomFive)){
			return flowCustomFive;
		}else{
			return "";
		}
	}
	public void setFlowCustomFive(String flowCustomFive) {
		this.flowCustomFive = flowCustomFive;
	}
	public String getFlowRelatedFile() {
		if(!isNullOrEmtryTrim(flowRelatedFile)){
			return flowRelatedFile;
		}else{
			return "";
		}
	}
	
  	public void setFlowRelatedFile(String flowRelatedFile) {
		this.flowRelatedFile = flowRelatedFile;
	}
	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	  boolean isNullOrEmtryTrim(String str) {
		return (str == null || "".equals(str.trim())) ? true : false;
	}

}
