package com.jecn.epros.server.action.web.login.ad.net99;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 
 * 99Net：单点登录
 * 
 * http://172.20.29.55/login.action?Login_Name=admin
 * 
 * @author ZXH
 * 
 */
public class Net99LoginAction extends JecnAbstractADLoginAction {

	/** http中员工编号参数 */
	private static final String NUM_PARA = "Login_Name";

	public Net99LoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 通过单点登录
	 * 
	 * @return String input;main;success;null
	 * 
	 */
	public String login() {
		try {
			return httpLogin();
		} catch (Exception e) {
			e.printStackTrace();
			return LoginAction.INPUT;
		}
	}

	/**
	 * HTTP 地址解析
	 * 
	 * @return String
	 * @date 2015-8-19 下午03:34:39
	 * @throws
	 */
	private String httpLogin() throws Exception {
		// http://172.20.29.55/login.action?Login_Name=admin
		// request 获取连接传入 参数 ;URL 的加号 被转义为空格, 以下方式不会
		String queryString = loginAction.getRequest().getQueryString();
		log.info(" loginName = " + queryString);
		if (StringUtils.isEmpty(queryString)) {
			return loginAction.loginGen();
		}
		String[] str = queryString.split("&");
		queryString = str[0].substring(0);

		String loginName = queryString.substring(NUM_PARA.length() + 1, queryString.length());
		if (StringUtils.isBlank(loginName)) {// 单点登录名称为空，执行普通登录路径
			return loginAction.loginGen();
		}
		return loginByLoginName(loginName);
	}
}
