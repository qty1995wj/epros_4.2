package epros.designer.gui.process.flowtool.NodeMove;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.process.flowtool.JecnFlowToolCommon;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class RoutineFlowToolMoveTree extends JecnRoutineTree {
	
	private Logger log = Logger.getLogger(RoutineFlowToolMoveTree.class);
	/***/
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/** 待移动ID集合 */
	private List<Long> listIds = new ArrayList<Long>();

	public RoutineFlowToolMoveTree(List<Long> listIds){
		super(listIds);
		this.listIds = listIds;
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getFlowTool().getSustainTools();
		} catch (Exception e) {
			log.error("RoutineFlowToolMoveTree getTreeModel is error", e);
		}
		return JecnFlowToolCommon.getFlowToolMoveTreeModel(list, listIds);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
