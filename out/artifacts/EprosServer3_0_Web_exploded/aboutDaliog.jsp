<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><s:if test="#application.versionType">
		<s:text name="eprosSytem"/>
		</s:if>
		<s:else>
		<s:text name="epsSytem"/>
		</s:else></title>
<script type="text/javascript">
	function closeAbout(){
			window.close();
		}
</script>
</head>
<body style="text-align:center;" >
<s:if test="#application.versionType">
<div  style='margin:0px auto;width:440px; height:320px;background-image:url(images/about-epros.jpg)'>
</div>
</s:if>
<s:else>
<div style='margin:0px auto;width:440px; height:320px;background-image:url(images/about-eps.jpg)'>
</div>
</s:else>
</body>
</html>