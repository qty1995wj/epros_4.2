package com.jecn.epros.server.service.process.impl;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.bean.checkout.CheckoutResult;
import com.jecn.epros.bean.checkout.CheckoutRoleItem;
import com.jecn.epros.bean.checkout.CheckoutRoleResult;
import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.bean.copyNode.JecnCopyHandle;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.download.ProcessDownloadBaseBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.ProcessMapDownloadBean;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.file.JecnFileContent;
import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT;
import com.jecn.epros.server.bean.integration.JecnControlPointT;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.FlowCriterionT;
import com.jecn.epros.server.bean.process.FlowDriver;
import com.jecn.epros.server.bean.process.FlowFileContent;
import com.jecn.epros.server.bean.process.FlowFileHeadData;
import com.jecn.epros.server.bean.process.FlowOrgT;
import com.jecn.epros.server.bean.process.FlowStandardizedFileT;
import com.jecn.epros.server.bean.process.JecnActivityFileT;
import com.jecn.epros.server.bean.process.JecnFigureFileTBean;
import com.jecn.epros.server.bean.process.JecnFlowApplyOrgT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT2;
import com.jecn.epros.server.bean.process.JecnFlowDriverEmailT;
import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.bean.process.JecnFlowFigureNow;
import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.bean.process.JecnFlowRecordT;
import com.jecn.epros.server.bean.process.JecnFlowRelatedRiskT;
import com.jecn.epros.server.bean.process.JecnFlowStationT;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnLineSegmentT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.process.JecnMapLineSegmentT;
import com.jecn.epros.server.bean.process.JecnMapRelatedStructureImageT;
import com.jecn.epros.server.bean.process.JecnModeFileT;
import com.jecn.epros.server.bean.process.JecnRefIndicatorsT;
import com.jecn.epros.server.bean.process.JecnRoleActiveT;
import com.jecn.epros.server.bean.process.JecnSaveProcessData;
import com.jecn.epros.server.bean.process.JecnSustainToolConnTBean;
import com.jecn.epros.server.bean.process.JecnSustainToolRelatedT;
import com.jecn.epros.server.bean.process.JecnTempletT;
import com.jecn.epros.server.bean.process.JecnToFromActiveT;
import com.jecn.epros.server.bean.process.ProcessArchitectureDescriptionBean;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.process.ProcessData;
import com.jecn.epros.server.bean.process.ProcessFigureData;
import com.jecn.epros.server.bean.process.ProcessFileImageFigureData;
import com.jecn.epros.server.bean.process.ProcessFileNodeData;
import com.jecn.epros.server.bean.process.ProcessInfoBean;
import com.jecn.epros.server.bean.process.ProcessInterface;
import com.jecn.epros.server.bean.process.ProcessMapData;
import com.jecn.epros.server.bean.process.ProcessMapFigureData;
import com.jecn.epros.server.bean.process.ProcessMapRelatedFileData;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.bean.process.RoleFigureData;
import com.jecn.epros.server.bean.process.StandardFlowRelationTBean;
import com.jecn.epros.server.bean.process.driver.JecnFlowDriverTimeT;
import com.jecn.epros.server.bean.process.entry.JecnEntry;
import com.jecn.epros.server.bean.process.entry.JecnEntryFigureT;
import com.jecn.epros.server.bean.process.inout.JecnFigureInoutSampleT;
import com.jecn.epros.server.bean.process.inout.JecnFigureInoutT;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutBase;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutData;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutPosT;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutT;
import com.jecn.epros.server.bean.process.temp.TmpKpiShowValues;
import com.jecn.epros.server.bean.process.through.RoleCheck;
import com.jecn.epros.server.bean.process.through.ThroughCheck;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.system.FavoriteUpdate;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnUserTotal;
import com.jecn.epros.server.bean.system.NightEvent;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.system.PubSourceCopyRelated;
import com.jecn.epros.server.bean.system.PubSourceData;
import com.jecn.epros.server.bean.system.PubSourceSave;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.task.TaskSendEmail;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.common.JecnConfigContents.EXCESS;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnRelatedDirFileUtil;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.file.IFileDao;
import com.jecn.epros.server.dao.popedom.IFlowOrgImageDao;
import com.jecn.epros.server.dao.popedom.IOrgAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.popedom.IPosGroupnAccessPermissionsDao;
import com.jecn.epros.server.dao.popedom.IPositionAccessPermissionsDao;
import com.jecn.epros.server.dao.process.IFlowDriverT;
import com.jecn.epros.server.dao.process.IFlowOrgDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IFlowStructureImageTDao;
import com.jecn.epros.server.dao.process.IFlowToolDao;
import com.jecn.epros.server.dao.process.IJecnLineSegmentTDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfo2Dao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.process.IProcessKPIDao;
import com.jecn.epros.server.dao.process.IProcessKPIValueDao;
import com.jecn.epros.server.dao.process.IProcessRecordDao;
import com.jecn.epros.server.dao.rule.IRuleDao;
import com.jecn.epros.server.dao.standard.IStandardDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.dao.system.IProcessRuleTypeDao;
import com.jecn.epros.server.dao.tree.IJecnTreeDao;
import com.jecn.epros.server.download.word.DownloadUtil;
import com.jecn.epros.server.download.word.SelectDownloadProcess;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.emailnew.EmailResource;
import com.jecn.epros.server.service.autoCode.AutoCodeEnum;
import com.jecn.epros.server.service.autoCode.IStandardizedAutoCodeService;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.service.process.ActivitySaveData;
import com.jecn.epros.server.service.process.ActivityUpdateData;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.buss.FlowCheckout;
import com.jecn.epros.server.service.process.buss.FlowKpiTitleAndValues;
import com.jecn.epros.server.service.process.buss.ICheckout;
import com.jecn.epros.server.service.process.buss.RoleCheckout;
import com.jecn.epros.server.service.reports.excel.ProcessExpiryDownExcelService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.service.vatti.buss.DominoOperation;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.SqlUtil;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceBean;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceWebBean;
import com.jecn.webservice.common.IFLowSync;
import com.sun.xml.internal.txw2.IllegalAnnotationException;
import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.WritableWorkbook;
import oracle.sql.BLOB;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.lob.SerializableBlob;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

@Transactional(rollbackFor = Exception.class)
public class FlowStructureServiceImpl extends AbsBaseService<JecnFlowStructureT, Long> implements IFlowStructureService {
    private Logger log = Logger.getLogger(FlowStructureServiceImpl.class);
    private IFlowStructureDao flowStructureDao;
    private IFileService fileService;
    private IFileDao fileDao;
    private IProcessBasicInfoDao processBasicDao;
    private IProcessBasicInfo2Dao processBasic2Dao;
    private IProcessKPIDao processKPIDao;
    private IProcessKPIValueDao processKPIValueDao;
    private IProcessRecordDao processRecordDao;
    private IProcessRuleTypeDao processRuleTypeDao;
    private IPersonDao userDao;
    private IFlowOrgImageDao flowOrgImageDao;
    private IFlowOrgDao flowOrgDao;
    private IFlowDriverT processDriverTDao;
    private IFlowStructureImageTDao flowStructureImageTDao;
    private IJecnLineSegmentTDao lineSegmentTDao;
    private IPositionAccessPermissionsDao positionAccessPermissionsDao;
    private IOrgAccessPermissionsDao orgAccessPermissionsDao;
    private IJecnDocControlDao docControlDao;
    private IJecnConfigItemDao configItemDao;
    private IRuleDao ruleDao;
    private IStandardDao standardDao;
    private IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao;
    private IFlowToolDao flowToolDao;
    @Resource(name = "treeDaoImpl")
    private IJecnTreeDao treeDao;

    @Resource(name = "standardizedAutoCodeServiceImpl")
    private IStandardizedAutoCodeService autoCodeService;

    public IFlowToolDao getFlowToolDao() {
        return flowToolDao;
    }

    public void setFlowToolDao(IFlowToolDao flowToolDao) {
        this.flowToolDao = flowToolDao;
    }

    private IFLowSync flowSync;

    public IFLowSync getFlowSync() {
        return flowSync;
    }

    public void setFlowSync(IFLowSync flowSync) {
        this.flowSync = flowSync;
    }

    public void setProcessBasic2Dao(IProcessBasicInfo2Dao processBasic2Dao) {
        this.processBasic2Dao = processBasic2Dao;
    }

    public IPosGroupnAccessPermissionsDao getPosGroupAccessPermissionsDao() {
        return posGroupAccessPermissionsDao;
    }

    public void setPosGroupAccessPermissionsDao(IPosGroupnAccessPermissionsDao posGroupAccessPermissionsDao) {
        this.posGroupAccessPermissionsDao = posGroupAccessPermissionsDao;
    }

    public void setStandardDao(IStandardDao standardDao) {
        this.standardDao = standardDao;
    }

    public void setRuleDao(IRuleDao ruleDao) {
        this.ruleDao = ruleDao;
    }

    public void setDocControlDao(IJecnDocControlDao docControlDao) {
        this.docControlDao = docControlDao;
    }

    public void setPositionAccessPermissionsDao(IPositionAccessPermissionsDao positionAccessPermissionsDao) {
        this.positionAccessPermissionsDao = positionAccessPermissionsDao;
    }

    public void setOrgAccessPermissionsDao(IOrgAccessPermissionsDao orgAccessPermissionsDao) {
        this.orgAccessPermissionsDao = orgAccessPermissionsDao;
    }

    public void setLineSegmentTDao(IJecnLineSegmentTDao lineSegmentTDao) {
        this.lineSegmentTDao = lineSegmentTDao;
    }

    public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
        this.flowStructureDao = flowStructureDao;
        this.baseDao = flowStructureDao;
    }

    public void setFileService(IFileService fileService) {
        this.fileService = fileService;
    }

    public IProcessRuleTypeDao getProcessRuleTypeDao() {
        return processRuleTypeDao;
    }

    public void setProcessRuleTypeDao(IProcessRuleTypeDao processRuleTypeDao) {
        this.processRuleTypeDao = processRuleTypeDao;
    }

    public void setUserDao(IPersonDao userDao) {
        this.userDao = userDao;
    }

    public void setFlowOrgImageDao(IFlowOrgImageDao flowOrgImageDao) {
        this.flowOrgImageDao = flowOrgImageDao;
    }

    public void setFlowOrgDao(IFlowOrgDao flowOrgDao) {
        this.flowOrgDao = flowOrgDao;
    }

    public void setFileDao(IFileDao fileDao) {
        this.fileDao = fileDao;
    }

    public void setProcessBasicDao(IProcessBasicInfoDao processBasicDao) {
        this.processBasicDao = processBasicDao;
    }

    public void setProcessKPIDao(IProcessKPIDao processKPIDao) {
        this.processKPIDao = processKPIDao;
    }

    public void setProcessKPIValueDao(IProcessKPIValueDao processKPIValueDao) {
        this.processKPIValueDao = processKPIValueDao;
    }

    public void setProcessRecordDao(IProcessRecordDao processRecordDao) {
        this.processRecordDao = processRecordDao;
    }

    public void setProcessDriverTDao(IFlowDriverT processDriverTDao) {
        this.processDriverTDao = processDriverTDao;
    }

    /**
     * @param list
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：流程子节点通用查询
     */
    private List<JecnTreeBean> findByListObjects(List<Object[]> list, List<Object[]> listTaskState) {
        List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
        for (Object[] obj : list) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null
                    || obj[6] == null) {
                continue;
            }
            JecnTreeBean jecnTreeBean = new JecnTreeBean();
            if ("0".equals(obj[3].toString())) {
                // 流程地图
                jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
            } else {
                // 流程
                jecnTreeBean.setTreeNodeType(TreeNodeType.process);
            }

            // 流程节点ID
            jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
            // 流程节点名称
            jecnTreeBean.setName(obj[1].toString());
            // 流程节点父ID
            jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
            // 流程节点父类名称
            jecnTreeBean.setPname("");
            // 流程编号
            if (obj[4] != null) {
                jecnTreeBean.setNumberId(obj[4].toString());
            } else {
                jecnTreeBean.setNumberId("");
            }
            // 流程是否发布
            if ("0".equals(obj[5].toString())) {
                jecnTreeBean.setPub(false);
            } else {
                jecnTreeBean.setPub(true);
            }
            // 流程是否有子节点
            if (Integer.parseInt(obj[6].toString()) > 0) {

                jecnTreeBean.setChildNode(true);
            } else {
                jecnTreeBean.setChildNode(false);
            }
            if (obj.length > 8) {
                jecnTreeBean.setIsDelete(Integer.parseInt(obj[7].toString()));
            }
            JecnCommon.setTaskState(jecnTreeBean.getId(), listTaskState, jecnTreeBean);
            listResult.add(jecnTreeBean);
        }
        return listResult;
    }

    /**
     * @param list
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：流程子节点通用查询
     */
    private List<JecnTreeBean> findByListObjects(List<Object[]> list) {
        List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
        for (Object[] obj : list) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null
                    || obj[6] == null) {
                continue;
            }
            JecnTreeBean jecnTreeBean = new JecnTreeBean();
            if ("0".equals(obj[3].toString())) {
                // 流程地图
                jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
            } else {
                // 流程
                jecnTreeBean.setTreeNodeType(TreeNodeType.process);
            }

            // 流程节点ID
            jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
            // 流程节点名称
            jecnTreeBean.setName(obj[1].toString());
            // 流程节点父ID
            jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
            // 流程节点父类名称
            jecnTreeBean.setPname("");
            // 流程编号
            if (obj[4] != null) {
                jecnTreeBean.setNumberId(obj[4].toString());
            } else {
                jecnTreeBean.setNumberId("");
            }
            // 流程是否发布
            if ("0".equals(obj[5].toString())) {
                jecnTreeBean.setPub(false);
            } else {
                jecnTreeBean.setPub(true);
            }
            // 流程是否有子节点
            if (Integer.parseInt(obj[6].toString()) > 0) {

                jecnTreeBean.setChildNode(true);
            } else {
                jecnTreeBean.setChildNode(false);
            }
            if (obj.length > 8) {
                jecnTreeBean.setIsDelete(Integer.parseInt(obj[7].toString()));
            }
            listResult.add(jecnTreeBean);
        }
        return listResult;
    }

    @Override
    public List<JecnTreeBean> getPnodes(Long flowId, Long projectId) throws Exception {

        try {
            JecnFlowStructureT flowBean = flowStructureDao.get(flowId);
            String t_path = flowBean.gettPath();
            Set<Long> setIds = JecnCommon.getPositionTreeIds(t_path, flowId);
            String sql = "SELECT DISTINCT ST.FLOW_ID,ST.FLOW_NAME,ST.PRE_FLOW_ID,ST.ISFLOW,ST.FLOW_ID_INPUT,CASE WHEN S.FLOW_ID IS NULL THEN '0' ELSE '1' END AS S_FLOW_ID"
                    + " ,CASE WHEN SUB.FLOW_ID is null then 0 else 1 end AS COUNT,ST.T_PATH,ST.SORT_ID,ST.NODE_TYPE"
                    + " FROM JECN_FLOW_STRUCTURE_T ST"
                    + " LEFT JOIN JECN_FLOW_STRUCTURE S ON ST.FLOW_ID = S.FLOW_ID"
                    + " LEFT JOIN JECN_FLOW_STRUCTURE_T sub on sub.PRE_FLOW_ID = ST.FLOW_ID AND sub.DEL_STATE=0"
                    + "                          WHERE ST.Del_State=0 AND ST.DATA_TYPE=0 AND ST.PROJECTID=?"
                    + " AND ST.PRE_FLOW_ID in "
                    + JecnCommonSql.getIdsSet(setIds)
                    + " ORDER BY ST.PRE_FLOW_ID,ST.SORT_ID,ST.FLOW_ID";
            List<Object[]> list = flowStructureDao.listNativeSql(sql, projectId);
            sql = "SELECT DISTINCT ST.FLOW_ID,JECN_TASK.STATE,JECN_TASK.APPROVE_TYPE,JECN_TASK.EDIT"
                    + "            FROM JECN_FLOW_STRUCTURE_T ST"
                    + "            LEFT JOIN JECN_FLOW_STRUCTURE_T SUB ON SUB.PRE_FLOW_ID = ST.FLOW_ID AND SUB.DEL_STATE=0"
                    + "            INNER JOIN JECN_TASK_BEAN_NEW JECN_TASK ON JECN_TASK.TASK_TYPE IN (0,4) AND JECN_TASK.STATE <> 5 AND JECN_TASK.IS_LOCK <> 0"
                    + "            AND JECN_TASK.R_ID=ST.FLOW_ID"
                    + "            WHERE ST.DEL_STATE=0 AND ST.DATA_TYPE=0 AND ST.PROJECTID=?"
                    + "                                  AND ST.PRE_FLOW_ID IN " + JecnCommonSql.getIdsSet(setIds);
            List<Object[]> listTaskState = flowStructureDao.listNativeSql(sql, projectId);
            return findByListObjectsFlowAppend(list, listTaskState);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 加载所有的流程地图与流程图
    public List<JecnTreeBean> getAllFlows(Long projectId) throws Exception {
        try {
            String sql = "select t.flow_id,t.flow_name,t.pre_flow_id,t.isflow,t.flow_id_input,"
                    + "       case when js.flow_id is null then '0' else '1' end as is_pub,"
                    + "       (select count(*) from jecn_flow_structure_t where pre_flow_id = t.flow_id and del_state=0) as count"
                    + "       from jecn_flow_structure_t t"
                    + "       left join jecn_flow_structure js on t.flow_id = js.flow_id"
                    + "       where t.projectid=? AND t.DATA_TYPE=0 and t.del_state=0 order by t.pre_flow_id,t.sort_id,t.flow_id";
            List<Object[]> list = flowStructureDao.listNativeSql(sql, projectId);
            return findByListObjects(list);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 根据父节点ID获得流程或流程地图的子节点
    public List<JecnTreeBean> getChildFlows(Long flowMapId, Long projectId, boolean isContainsDelNode) throws Exception {
        try {
            List<Object[]> list = flowStructureDao.getChildFlowsT(flowMapId, projectId, isContainsDelNode);
            String sql = "SELECT DISTINCT T.FLOW_ID,"
                    + "             JECN_TASK.STATE,JECN_TASK.APPROVE_TYPE,JECN_TASK.EDIT"
                    + "  FROM JECN_FLOW_STRUCTURE_T T"
                    + "  INNER JOIN JECN_TASK_BEAN_NEW JECN_TASK ON JECN_TASK.TASK_TYPE IN (0,4) AND JECN_TASK.IS_LOCK=1 AND JECN_TASK.STATE <> 5  AND JECN_TASK.R_ID=T.FLOW_ID"
                    + " WHERE T.PRE_FLOW_ID = ?" + "   AND T.DATA_TYPE = 0" + "   AND T.PROJECTID = ?"
                    + "   AND T.DEL_STATE = 0";
            List<Object[]> listTaskState = flowStructureDao.listNativeSql(sql, flowMapId, projectId);
            return findByListObjectsFlowAppend(list, listTaskState);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * @param list
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：设计器流程数展开专用
     */
    private List<JecnTreeBean> findByListObjectsFlowAppend(List<Object[]> list, List<Object[]> listTaskState) {
        List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
        for (Object[] obj : list) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null
                    || obj[6] == null || obj[7] == null) {
                continue;
            }
            JecnTreeBean jecnTreeBean = new JecnTreeBean();
            if ("0".equals(obj[3].toString())) {
                // 流程地图
                jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
            } else {
                if ("1".equals(obj[9].toString())) {// 0:流程/架构图； 1：流程（文件）
                    jecnTreeBean.setTreeNodeType(TreeNodeType.processFile);
                } else {
                    // 流程
                    jecnTreeBean.setTreeNodeType(TreeNodeType.process);
                }
            }

            // 流程节点ID
            jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
            // 流程节点名称
            jecnTreeBean.setName(obj[1].toString());
            // 流程节点父ID
            jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
            // 流程节点父类名称
            jecnTreeBean.setPname("");
            // 流程编号
            if (obj[4] != null) {
                jecnTreeBean.setNumberId(obj[4].toString());
            } else {
                jecnTreeBean.setNumberId("");
            }
            // 流程是否发布
            if ("0".equals(obj[5].toString())) {
                jecnTreeBean.setPub(false);
            } else {
                jecnTreeBean.setPub(true);
            }
            // 流程是否有子节点
            if (Integer.parseInt(obj[6].toString()) > 0) {

                jecnTreeBean.setChildNode(true);
            } else {
                jecnTreeBean.setChildNode(false);
            }
            // t_Path
            if (!"".equals(obj[7].toString())) {
                jecnTreeBean.setT_Path(obj[7].toString());
            }
            if (obj.length >= 11) {
                if (obj[10] != null) {
                    if (Integer.valueOf(obj[10].toString()) == 1) {
                        jecnTreeBean.setUpdate(true);
                    } else {
                        jecnTreeBean.setUpdate(false);
                    }
                }
            }
            if (obj[9] != null) {
                jecnTreeBean.setNodeType(Integer.valueOf(obj[9].toString()));
            }
            JecnCommon.setTaskState(jecnTreeBean.getId(), listTaskState, jecnTreeBean);
            listResult.add(jecnTreeBean);
        }
        return listResult;
    }

    /**
     * @param list
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：设计器流程数展开专用
     */
    private List<JecnTreeBean> findByListObjectsFlowAppend(List<Object[]> list) {
        List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
        for (Object[] obj : list) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null
                    || obj[6] == null || obj[7] == null) {
                continue;
            }
            JecnTreeBean jecnTreeBean = new JecnTreeBean();
            if ("0".equals(obj[3].toString())) {
                // 流程地图
                jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
            } else {
                if ("1".equals(obj[9].toString())) {// 0:流程/架构图； 1：流程（文件）
                    jecnTreeBean.setTreeNodeType(TreeNodeType.processFile);
                } else {
                    // 流程
                    jecnTreeBean.setTreeNodeType(TreeNodeType.process);
                }
            }

            // 流程节点ID
            jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
            // 流程节点名称
            jecnTreeBean.setName(obj[1].toString());
            // 流程节点父ID
            jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
            // 流程节点父类名称
            jecnTreeBean.setPname("");
            // 流程编号
            if (obj[4] != null) {
                jecnTreeBean.setNumberId(obj[4].toString());
            } else {
                jecnTreeBean.setNumberId("");
            }
            // 流程是否发布
            if ("0".equals(obj[5].toString())) {
                jecnTreeBean.setPub(false);
            } else {
                jecnTreeBean.setPub(true);
            }
            // 流程是否有子节点
            if (Integer.parseInt(obj[6].toString()) > 0) {

                jecnTreeBean.setChildNode(true);
            } else {
                jecnTreeBean.setChildNode(false);
            }
            // t_Path
            if (!"".equals(obj[7].toString())) {
                jecnTreeBean.setT_Path(obj[7].toString());
            }
            if (!"".equals(obj[8].toString())) {
                jecnTreeBean.setSortId(Integer.valueOf(obj[8].toString()));
            }
            if (obj.length >= 11) {
                if (obj[10] != null) {
                    if (Integer.valueOf(obj[10].toString()) == 1) {
                        jecnTreeBean.setUpdate(true);
                    } else {
                        jecnTreeBean.setUpdate(false);
                    }
                }
            }
            listResult.add(jecnTreeBean);
        }
        return listResult;
    }

    @Override
    // 增加流程地图
    public JecnFlowStructureT addFlowMap(JecnFlowStructureT jecnFlowStructureT, JecnMainFlowT jecnMainFlowT,
                                         String posIds, String orgIds, String posGroupIds, Long updatePersionId) throws Exception {
        try {
            jecnFlowStructureT.setCreateDate(new Date());
            jecnFlowStructureT.setUpdateDate(new Date());
            jecnFlowStructureT.setDataType(0);
            Long flowId = flowStructureDao.save(jecnFlowStructureT);
            jecnMainFlowT.setFlowId(flowId);
            flowStructureDao.getSession().save(jecnMainFlowT);
            // 岗位查阅权限
            positionAccessPermissionsDao.savaOrUpdate(flowId, 0, posIds, true);
            // 部门查阅权限
            orgAccessPermissionsDao.savaOrUpdate(flowId, 0, orgIds, true);
            // 岗位查阅权限
            posGroupAccessPermissionsDao.savaOrUpdate(flowId, 0, posGroupIds, false);
            // 更新tpath 和tlevel
            JecnCopyHandle.newInstance().updateTpathAndTLevel(flowStructureDao, jecnFlowStructureT);
            // 继承上级的任务模板
            JecnTaskCommon.inheritTaskTemplate(flowStructureDao, jecnFlowStructureT.getFlowId(), jecnFlowStructureT
                    .getIsFlow() == 0 ? 4 : 0, jecnFlowStructureT.getPeopleId());
            // 添加日志
            JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 1,
                    updatePersionId, flowStructureDao);
            return jecnFlowStructureT;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 添加流程
    public JecnFlowStructureT addFlow(JecnSaveProcessData processData) throws Exception {
        try {
            JecnFlowStructureT jecnFlowStructureT = processData.getJecnFlowStructureT();
            JecnFlowBasicInfoT jecnFlowBasicInfoT = processData.getJecnFlowBasicInfoT();
            String posIds = processData.getPosIds();
            String orgIds = processData.getOrgIds();
            String sptIds = processData.getSptIds();
            Long updatePersionId = processData.getUpdatePersionId();
            String posGroupIds = processData.getPosGroupIds();
            // 查询父节点的责任人和专员
            if (jecnFlowStructureT.getPerFlowId() != null && jecnFlowStructureT.getPerFlowId() != 0) {
                Long pid = jecnFlowStructureT.getPerFlowId();
                JecnFlowStructureT pflow = flowStructureDao.get(pid);
                jecnFlowStructureT.setCommissionerId(pflow.getCommissionerId());
                if (pflow.getIsFlow() == 1) {
                    JecnFlowBasicInfoT flowBasicInfoT = processBasicDao.getFlowBasicInfoT(pid);
                    jecnFlowBasicInfoT.setResPeopleId(flowBasicInfoT.getResPeopleId());
                    jecnFlowBasicInfoT.setTypeResPeople(flowBasicInfoT.getTypeResPeople());
                } else {
                    JecnMainFlowT jecnMainFlow = (JecnMainFlowT) processBasicDao.getSession().get(JecnMainFlowT.class,
                            pid);
                    jecnFlowBasicInfoT.setResPeopleId(jecnMainFlow.getResPeopleId());
                    jecnFlowBasicInfoT.setTypeResPeople(jecnMainFlow.getTypeResPeople());
                }
            }
            jecnFlowStructureT.setCreateDate(new Date());
            jecnFlowStructureT.setUpdateDate(new Date());
            jecnFlowStructureT.setDataType(0);

            if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode)) {
                AutoCodeResultData resultData = AutoCodeEnum.INSTANCE.getStandardizedAutoCodeResultData(
                        jecnFlowStructureT.getPerFlowId(), 0, autoCodeService);
                jecnFlowStructureT.setFlowIdInput(resultData.getCode());
            }

            Long flowId = flowStructureDao.save(jecnFlowStructureT);
            // 流程主键ID
            jecnFlowBasicInfoT.setFlowId(flowId);
            // 新建流程图时，流程有效期获取配置表中数据
            flowExpiry(jecnFlowBasicInfoT);

            flowStructureDao.getSession().save(jecnFlowBasicInfoT);
            if (!JecnContants.isPubShow()) {// D2版本
                // 流程基本信息表2(相关文件、自定义要素1-5)
                JecnFlowBasicInfoT2 jecnFlowBasicInfoT2 = new JecnFlowBasicInfoT2();
                jecnFlowBasicInfoT2.setFlowId(flowId);
                flowStructureDao.getSession().save(jecnFlowBasicInfoT2);
            }
            // 岗位查阅权限
            positionAccessPermissionsDao.savaOrUpdate(flowId, 0, posIds, true);
            // 部门查阅权限
            orgAccessPermissionsDao.savaOrUpdate(flowId, 0, orgIds, true);
            // 岗位组查阅权限
            posGroupAccessPermissionsDao.savaOrUpdate(flowId, 0, posGroupIds, true);
            // 支持工具
            if (sptIds != null && !"".equals(sptIds)) {
                String[] sptIdsArr = sptIds.split(",");
                for (String supId : sptIdsArr) {
                    if (supId == null || "".equals(supId)) {
                        continue;
                    }
                    JecnSustainToolRelatedT jecnSustainToolRelatedT = new JecnSustainToolRelatedT();
                    jecnSustainToolRelatedT.setFlowId(flowId);
                    jecnSustainToolRelatedT.setFlowSustainToolId(Long.valueOf(supId));
                    flowStructureDao.getSession().save(jecnSustainToolRelatedT);
                }
            }
            // 更新tpath 和tlevel
            JecnCopyHandle.newInstance().updateTpathAndTLevel(flowStructureDao, jecnFlowStructureT);
            JecnTaskCommon.inheritTaskTemplate(flowStructureDao, jecnFlowStructureT.getFlowId(), 0, jecnFlowStructureT
                    .getPeopleId());
            // 添加日志
            JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 1,
                    updatePersionId, flowStructureDao);
            return jecnFlowStructureT;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * 新建流程图时，流程有效期值默认获取配置表数据
     *
     * @param jecnFlowBasicInfoT JecnFlowBasicInfoT 流程基本信息对象
     */
    private void flowExpiry(JecnFlowBasicInfoT jecnFlowBasicInfoT) {
        if ("1".equals(JecnContants.getValue("processValidForever"))) {// 流程默认有效期是否永久(0:是永久
            // 1：不是永久)
            jecnFlowBasicInfoT.setExpiry(Long.valueOf(JecnContants.getValue("processValidDefaultValue")));
        }
    }

    @Override
    // 节点排序
    public void updateSortFlows(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersonId)
            throws Exception {
        try {
            String viewSort = "";
            if (pId.intValue() != 0) {
                String hql = "from JecnFlowStructureT where flowId=?";
                List<JecnFlowStructureT> flows = flowStructureDao.listHql(hql, pId);
                viewSort = flows.get(0).getViewSort();
            }

            String hql = "from JecnFlowStructureT where perFlowId=? and dataType=0 and projectId = ?";
            List<JecnFlowStructureT> listJecnFlowStructureT = flowStructureDao.listHql(hql, pId, projectId);
            List<JecnFlowStructureT> listUpdateT = new ArrayList<JecnFlowStructureT>();

            for (JecnTreeDragBean jecnTreeDragBean : list) {
                for (JecnFlowStructureT jecnFlowStructureT : listJecnFlowStructureT) {
                    if (jecnTreeDragBean.getId().equals(jecnFlowStructureT.getFlowId())) {
                        if (jecnFlowStructureT.getSortId() == null
                                || (jecnTreeDragBean.getSortId() != jecnFlowStructureT.getSortId().intValue())) {
                            jecnFlowStructureT.setSortId(Long.valueOf(jecnTreeDragBean.getSortId()));
                            jecnFlowStructureT.setViewSort(JecnUtil.concatViewSort(viewSort, jecnTreeDragBean
                                    .getSortId()));
                            listUpdateT.add(jecnFlowStructureT);
                            break;
                        }
                    }
                }
            }

            Map<Long, Integer> relatedMap = new LinkedHashMap<Long, Integer>();

            for (JecnFlowStructureT jecnFlowStructureT : listUpdateT) {
                flowStructureDao.update(jecnFlowStructureT);
                // 更新正式表
                hql = "update JecnFlowStructure set sortId=? where flowId=?";
                flowStructureDao.execteHql(hql, jecnFlowStructureT.getSortId(), jecnFlowStructureT.getFlowId());
                // 添加日志
                JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 5,
                        updatePersonId, flowStructureDao);
                relatedMap.put(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getSortId().intValue());
            }
            if (JecnConfigTool.isArchitectureUploadFile()) {
                // 父节点对应的文件目录节点ID
                Long parentRelatedId = this.getRelatedFileId(pId);
                if (parentRelatedId != null) {
                    fileService.updateSortFiles(parentRelatedId, relatedMap, updatePersonId);
                }
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 节点移动
    public void moveFlows(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
            throws Exception {
        try {
            String t_path = "";
            int level = -1;
            JecnFlowStructureT bean = this.get(pId);
            if (pId.intValue() != 0) {
                t_path = bean.gettPath();
                level = bean.gettLevel();
            }
            JecnCommon.updateNodesChildTreeBeans(listIds, pId, "", new Date(), updatePersonId, t_path, level,
                    flowStructureDao, moveNodeType, 1);
            for (Long flowId : listIds) {
                // 添加日志
                JecnUtil.saveJecnJournal(flowId, null, 1, 4, updatePersonId, flowStructureDao);
            }

            if (JecnConfigTool.isArchitectureUploadFile()) {
                // 获取移动的节点对应关联的文件目录集合
                List<Long> moveRelatedIds = this.flowStructureDao.getRelatedFileIdsByFlowIds(listIds);
                if (moveRelatedIds.isEmpty()) {
                    return;
                }
                Long parentRelatedId = -1L;
                if (pId.intValue() == 0) {
                    parentRelatedId = 0L;
                } else {
                    // 获取节点移动，父节点对应的文件目录ID
                    parentRelatedId = createRelatedFileDirByPath(bean.getFlowId(), bean.getProjectId(), updatePersonId);
                }

                fileService.moveFiles(moveRelatedIds, parentRelatedId, updatePersonId, TreeNodeType.fileDir);
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * 流程假删
     */
    @Override
    public void deleteFlows(List<Long> listIds, Long updatePersonId, Long projectId) throws Exception {
        try {

            String sql = "update Jecn_Flow_Structure_T set del_State=1 where del_State <> 2 and flow_Id in"
                    + JecnCommonSql.getChildObjectsSqlByTypeDelAddBracket(listIds, TreeNodeType.processMap);
            flowStructureDao.execteNative(sql);
            sql = "update Jecn_Flow_Structure set del_State=1 where  del_State <> 2  and flow_Id in"
                    + JecnCommonSql.getChildObjectsSqlByTypeDelAddBracket(listIds, TreeNodeType.processMap);
            flowStructureDao.execteNative(sql);
            // 流程地图或流程假删除的时候任务也假删除
            sql = "update JECN_TASK_BEAN_NEW set is_Lock = 0 where APPROVE_TYPE <> 2 and (task_Type=0 or task_Type=4) and r_id in"
                    + JecnCommonSql.getChildObjectsSqlByTypeDelAddBracket(listIds, TreeNodeType.processMap);
            // 添加日志
            JecnUtil.saveJecnJournals(listIds, 1, 6, updatePersonId, flowStructureDao);
            // 华帝登录 删除Domino数据
            if (JecnUtil.isVattiLogin()) {
                Set<Long> setIds = new HashSet<Long>();
                setIds.addAll(listIds);
                DominoOperation.deleteDataFromDomino(setIds, "Flow");
            }

            if (JecnConfigTool.isArchitectureUploadFile()) {// 流程、流程架构允许维护文档管理（支撑文件）
                // 获取可维护的文件节点
                List<Long> fileIds = flowStructureDao.getRelatedFileIdsByFlowIds(listIds);
                if (fileIds == null || fileIds.isEmpty()) {
                    return;
                }
                fileService.deleteFileManage(fileIds, projectId, updatePersonId, TreeNodeType.fileDir);
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 流程地图与流程重命名
    public void updateName(Long id, String newName, Long updatePersonId, int userType) throws Exception {
        try {
            JecnFlowStructureT jecnFlowStructureT = this.get(id);
            // 获得老的流程名称
            String oldName = jecnFlowStructureT.getFlowName();
            String hql = "update JecnFlowStructureT set flowName=? where flowId=?";
            flowStructureDao.execteHql(hql, newName, id);

            if (JecnConfigTool.isAllowedPubReName()) {// 重命名，同步发布
                // 0是管理员
                hql = "update JecnFlowStructure set flowName=? where flowId=?";
                flowStructureDao.execteHql(hql, newName, id);
                hql = "update JecnFlowBasicInfo set flowName=? where flowId=?";
                flowStructureDao.execteHql(hql, newName, id);
            }

            standardDao.reNameStandardByRelationId(newName, 1, id);
            if (jecnFlowStructureT != null) {
                hql = "update JecnFlowStructureImageT set figureText=? where figureText=? and linkFlowId=? and figureType in "
                        + JecnCommonSql.getLinkProcessString();
                flowStructureDao.execteHql(hql, newName, oldName, id);

                if (JecnConfigTool.isAllowedPubReName()) {
                    hql = "update JecnFlowStructureImage set figureText=? where figureText=? and linkFlowId=? and figureType in "
                            + JecnCommonSql.getLinkProcessString();
                    flowStructureDao.execteHql(hql, newName, oldName, id);
                }
            }

            flowStructureImageTDao.updateLinkedImageName(id, JecnUtil.getShowName(newName, jecnFlowStructureT
                    .getFlowIdInput()));

            // 添加日志
            JecnUtil.saveJecnJournal(id, newName, 1, 2, updatePersonId, flowStructureDao, 4);

            if (JecnConfigTool.isArchitectureUploadFile()) {// 流程、流程架构允许维护文档管理（支撑文件）
                // 获取关联ID
                Long relatedId = this.getRelatedFileId(id);
                if (relatedId != null) {
                    fileService.reFileDirName(newName, relatedId, updatePersonId);
                }
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 根据名称查询流程地图和流程图
    public List<JecnTreeBean> searchFlowByName(String name, Long projectId, int type) throws Exception {
        List<Object[]> list = new ArrayList<Object[]>();
        String sql = "select distinct t.flow_id,t.flow_name,t.pre_flow_id,t.isflow,t.flow_id_input,"
                + "       case when js.flow_id is null then '0' else '1' end as is_pub,"
                + "       case when sub.flow_id is null then 0 else 1 end as count,t.t_path,t.sort_id,T.NODE_TYPE,"
                + "       case when t.pub_time<>t.update_date then 1 else 0 end as isUpdate"
                + "       from jecn_flow_structure_t t"
                + "       left join jecn_flow_structure js on t.flow_id = js.flow_id"
                + "       left join jecn_flow_structure_t sub on sub.pre_flow_id = t.flow_id and sub.del_state=0"
                + "       where t.projectid= " + projectId
                + " and t.del_state=0 and t.data_type=0 and t.flow_name like ?";
        try {
            if (type == 1) {
                sql += " and t.isflow=0";
            } else if (type == 2) {
                sql += " and t.isflow=1";
            }
            sql += " order by t.pre_flow_id,t.sort_id,t.flow_id";
            list = flowStructureDao.listNativeSql(sql, 0, 20, "%" + name + "%");
            return findByListObjectsFlowAppend(list);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, int type, Long peopelId)
            throws Exception {
        List<Object[]> list = new ArrayList<Object[]>();
        String sql = "select distinct t.flow_id,t.flow_name,t.pre_flow_id,t.isflow,t.flow_id_input,"
                + "       case when js.flow_id is null then '0' else '1' end as is_pub,"
                + "       case when sub.flow_id is null then 0 else 1 end as count,t.t_path,t.sort_id,T.NODE_TYPE"
                + "       from jecn_flow_structure_t t"
                + "       left join jecn_flow_structure js on t.flow_id = js.flow_id"
                + "       left join jecn_flow_structure_t sub on sub.pre_flow_id = t.flow_id and sub.del_state=0 ";
        // 拼装角色权限sql
        sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthSqlCommon(0, peopelId, projectId);
        sql = sql + "       where t.projectid= " + projectId
                + " and t.del_state=0 and t.data_type=0 and t.flow_name like ?";
        try {
            if (type == 1) {
                sql += " and t.isflow=0";
            } else if (type == 2) {
                sql += " and t.isflow=1";
            }
            sql += " order by t.pre_flow_id,t.sort_id,t.flow_id";
            list = flowStructureDao.listNativeSql(sql, 0, 20, "%" + name + "%");
            return findByListObjectsFlowAppend(list);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 流程地图描述
    public JecnMainFlowT getFlowMapInfo(Long id) throws Exception {
        try {
            JecnMainFlowT mf = (JecnMainFlowT) flowStructureDao.getSession().get(JecnMainFlowT.class, id);
            if (mf.getFileId() != null && mf.getFileId().intValue() != -1) {
                JecnFileBeanT fileBean = fileDao.get(mf.getFileId());
                // 未删除的文件 0是删除 1是未删除
                if (fileBean != null && fileBean.getDelState() != null && fileBean.getDelState().intValue() == 0) {
                    mf.setFileName(fileBean.getFileName());
                }
            }
            return mf;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 流程解除编辑
    public void moveEdit(Long id, Long updatePersonId) throws Exception {
        try {
            String hql = "update JecnFlowStructureT set occupier=0 where flowId=?";
            flowStructureDao.execteHql(hql, id);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 更新流程地图描述
    public void updateFlowMap(JecnMainFlowT jecnMainFlowT, Long updatePersonId, String flowNum) throws Exception {
        try {
            Long flowId = jecnMainFlowT.getFlowId();
            if (flowId == null) {
                return;
            }
            JecnFlowStructureT jecnFlowStructureT = flowStructureDao.get(jecnMainFlowT.getFlowId());
            jecnFlowStructureT.setFlowIdInput(flowNum);
            jecnFlowStructureT.setUpdateDate(new Date());
            jecnFlowStructureT.setUpdatePeopleId(updatePersonId);
            flowStructureDao.update(jecnFlowStructureT);
            // 添加日志
            JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 2,
                    updatePersonId, flowStructureDao);
            // 更新流程地图描述
            flowStructureDao.getSession().update(jecnMainFlowT);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 获得流程KPI
    public Long addKPI(JecnFlowKpiNameT jecnFlowKpiNameT, Long updatePeopleId) throws Exception {
        try {
            updateFlowModifyRecord(jecnFlowKpiNameT.getFlowId(), updatePeopleId, 11);
            Long kpiId = this.processKPIDao.save(jecnFlowKpiNameT);
            /** ** 添加KPI中IT系统与支持工具关联关系表 * */
            String itSystemIds = jecnFlowKpiNameT.getKpiITSystemIds();
            if (itSystemIds != null && !"".equals(itSystemIds)) {
                String[] strArr = itSystemIds.split(",");
                for (String str : strArr) {
                    if (str == null || "".equals(str)) {
                        continue;
                    }
                    JecnSustainToolConnTBean jecnSustainToolConnTBean = new JecnSustainToolConnTBean();
                    jecnSustainToolConnTBean.setId(JecnCommon.getUUID());// 主键ID
                    jecnSustainToolConnTBean.setRelatedId(kpiId);// 关联ID(kpi主键ID)
                    jecnSustainToolConnTBean.setRelatedType(0);// 关联类型
                    jecnSustainToolConnTBean.setSustainToolId(Long.valueOf(str));// 支持工具ID
                    jecnSustainToolConnTBean.setCreatePeopleId(updatePeopleId);
                    jecnSustainToolConnTBean.setCreateTime(new Date());
                    jecnSustainToolConnTBean.setUpdatePeopleId(updatePeopleId);
                    jecnSustainToolConnTBean.setUpdateTime(new Date());
                    processBasicDao.getSession().save(jecnSustainToolConnTBean);
                }
            }
            return kpiId;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 添加KPI的值
    public void addKPIVaule(JecnFlowKpi jecnFlowKpi, Long updatePeopleId, Long flowId) throws Exception {
        try {
            updateFlowModifyRecord(flowId, updatePeopleId, 11);
            this.processKPIValueDao.save(jecnFlowKpi);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * @param flowId
     * @param updatePeopleId
     * @author zhangchen Jul 13, 2012
     * @description：更新流程修改记录
     */
    private JecnFlowStructureT updateFlowModifyRecord(Long flowId, Long updatePeopleId) throws Exception {
        return updateFlowModifyRecord(flowId, updatePeopleId, 0);
    }

    private JecnFlowStructureT updateFlowModifyRecord(Long flowId, Long updatePeopleId, int secondOperatorType)
            throws Exception {
        try {
            JecnFlowStructureT jecnFlowStructureT = this.flowStructureDao.get(flowId);
            jecnFlowStructureT.setUpdateDate(new Date());
            jecnFlowStructureT.setUpdatePeopleId(updatePeopleId);
            flowStructureDao.update(jecnFlowStructureT);
            // 添加日志
            JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 2,
                    updatePeopleId, flowStructureDao, secondOperatorType);
            return jecnFlowStructureT;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public void addKPIVaules(Long kpiAndId, List<JecnFlowKpi> jecnFlowKpiList, Long updatePeopleId, Long flowId)
            throws Exception {
        try {

            updateFlowModifyRecord(flowId, updatePeopleId, 11);
            String hql = "delete from JecnFlowKpi where kpiAndId=? and kpiHorVlaue=?";
            for (JecnFlowKpi jecnFlowKpi : jecnFlowKpiList) {
                Date kpiHorValue = jecnFlowKpi.getKpiHorVlaue();
                if (JecnCommon.isNullOrEmtryTrim(jecnFlowKpi.getKpiValue())) {// 为空说明该条数据需要删除

                    this.processKPIValueDao.execteHql(hql, kpiAndId, kpiHorValue);
                } else {// 不为空说明需要保存

                    this.processKPIValueDao.execteHql(hql, kpiAndId, kpiHorValue);
                    processKPIValueDao.save(jecnFlowKpi);
                }

            }

        } catch (Exception e) {
            log.error("addKPIVaules 保存kpi异常", e);
            throw e;
        }
    }

    @Override
    /***************************************************************************
     * @author zhangchen Jul 13, 2012
     * @description：获得流程属性
     * @param flowId
     * @return list一共返回7个值，0是流程类别ID，1是流程类别名称，2是流程责任部门ID，3是流程责任部门名称，4是流程责任类型，5是责任人ID，6是流程责任人名称
     * @throws Exception
     */
    public ProcessAttributeBean getFlowAttribute(Long flowId) throws Exception {
        JecnFlowStructureT jecnFlowStructureT = this.get(flowId);
        ProcessAttributeBean processAttributeBean = new ProcessAttributeBean();
        Integer typeResPeople = null;
        Long resPeopleId = null;
        if (jecnFlowStructureT.getIsFlow() == 1) {
            JecnFlowBasicInfoT jecnFlowBasicInfoT = this.processBasicDao.get(flowId);
            // 流程类别
            if (jecnFlowBasicInfoT.getTypeId() != null) {
                ProceeRuleTypeBean proceeRuleTypeBean = this.processRuleTypeDao.get(jecnFlowBasicInfoT.getTypeId());
                if (proceeRuleTypeBean == null) {
                    // 流程类别ID
                    processAttributeBean.setProcessTypeId(null);
                    // 流程类别名称
                    processAttributeBean.setProcessTypeName("");
                } else {
                    // 流程类别ID
                    processAttributeBean.setProcessTypeId(jecnFlowBasicInfoT.getTypeId());
                    if (proceeRuleTypeBean.getTypeName() == null) {
                        // 流程类别名称
                        processAttributeBean.setProcessTypeName("");
                        // 流程类别名称
                    } else {
                        processAttributeBean.setProcessTypeName(proceeRuleTypeBean.getTypeName());
                    }
                }
            }
            // 有效期
            if (jecnFlowBasicInfoT.getExpiry() == null) {
                processAttributeBean.setExpiry(JecnUtil.getValue("processValidDefaultValue"));
            } else {
                processAttributeBean.setExpiry(jecnFlowBasicInfoT.getExpiry().toString());
            }

            // 责任人ID
            Object[] obj = processBasicDao.getFlowRelateOrgIds(flowId, false);
            if (obj != null && obj[0] != null && obj[1] != null) {
                // 责任部门Id
                processAttributeBean.setDutyOrgId(Long.valueOf(obj[0].toString()));
                // 责任部门
                processAttributeBean.setDutyOrgName(obj[1].toString());
            }

            // 流程责任人
            typeResPeople = jecnFlowBasicInfoT.getTypeResPeople();
            resPeopleId = jecnFlowBasicInfoT.getResPeopleId();

            // 流程监护人
            if (jecnFlowBasicInfoT.getGuardianId() != null) {
                // 监护人ID
                processAttributeBean.setGuardianId(jecnFlowBasicInfoT.getGuardianId());
                JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfoT.getGuardianId());
                if (jecnUser != null) {
                    processAttributeBean.setGuardianName(jecnUser.getTrueName());
                }
            }
            // 流程拟制人
            if (jecnFlowBasicInfoT.getFictionPeopleId() != null) {
                // 拟制人ID
                processAttributeBean.setFictionPeopleId(jecnFlowBasicInfoT.getFictionPeopleId());
                JecnUser jecnUser = this.userDao.get(jecnFlowBasicInfoT.getFictionPeopleId());
                if (jecnUser != null) {
                    processAttributeBean.setFictionPeopleName(jecnUser.getTrueName());
                }
            }
            // 关键字
            processAttributeBean.setKeyWord(jecnFlowStructureT.getKeyword());
            // 业务类型
            processAttributeBean.setBussType((jecnFlowStructureT.getBussType() == null || "".equals(jecnFlowStructureT.getBussType())) ? "0" : jecnFlowStructureT
                    .getBussType());
        } else {
            JecnMainFlowT map = (JecnMainFlowT) processBasicDao.getSession().get(JecnMainFlowT.class, flowId);
            typeResPeople = map.getTypeResPeople();
            resPeopleId = map.getResPeopleId();

        }
        // 流程专员
        if (jecnFlowStructureT.getCommissionerId() != null) {
            JecnUser jecnUser = this.userDao.get(jecnFlowStructureT.getCommissionerId());
            if (jecnUser != null) {
                processAttributeBean.setCommissionerId(jecnFlowStructureT.getCommissionerId());
                processAttributeBean.setCommissionerName(jecnUser.getTrueName());
            }
        }
        // 流程责任人
        if (typeResPeople != null && resPeopleId != null) {
            // 责任人类型
            processAttributeBean.setDutyUserType(typeResPeople.intValue());
            // 责任人ID
            processAttributeBean.setDutyUserId(resPeopleId);
            if (typeResPeople.intValue() == 0) {
                JecnUser jecnUser = this.userDao.get(resPeopleId);
                if (jecnUser != null) {
                    processAttributeBean.setDutyUserName(jecnUser.getTrueName());
                }
            } else if (typeResPeople.intValue() == 1) {
                JecnFlowOrgImage jecnFlowOrgImage = this.flowOrgImageDao.get(resPeopleId);
                if (jecnFlowOrgImage != null) {
                    processAttributeBean.setDutyUserName(jecnFlowOrgImage.getFigureText());
                }
            }
        }
        return processAttributeBean;
    }

    @Override
    // 获得KPI的值
    public List<JecnFlowKpi> getJecnFlowKpiListByKPIId(Long flowKpiId) throws Exception {
        try {
            return this.processKPIValueDao.getJecnFlowKpiListBy(flowKpiId);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 获得流程KPI
    public List<JecnFlowKpiNameT> getJecnFlowKpiNameTList(Long flowId) throws Exception {
        try {
            return this.processKPIDao.getJecnFlowKpiNameTList(flowId);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 获得流程记录保存
    public List<JecnFlowRecordT> getJecnFlowRecordTListByFlowId(Long flowId) throws Exception {
        try {
            return this.processRecordDao.getJecnFlowRecordTListByFlowId(flowId);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 获得流程记录保存
    public List<RuleT> getRuleTByFlowId(Long flowId) throws Exception {
        try {
            List<Object[]> list = this.processBasicDao.getFlowRelateRules(flowId, false);
            List<RuleT> listRuleT = new ArrayList<RuleT>();
            for (Object[] obj : list) {
                if (obj == null || obj[0] == null || obj[1] == null || obj[4] == null) {
                    continue;
                }
                RuleT ruleT = new RuleT();
                ruleT.setId(Long.valueOf(obj[0].toString()));
                ruleT.setRuleName(obj[1].toString());
                if (obj[2] != null) {
                    ruleT.setRuleNumber(obj[2].toString());
                } else {
                    ruleT.setRuleNumber("");
                }
                if (obj[3] != null) {
                    ruleT.setFileId(Long.valueOf(obj[3].toString()));
                }
                ruleT.setIsDir(Integer.parseInt(obj[4].toString()));
                listRuleT.add(ruleT);
            }
            return listRuleT;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public List<StandardBean> getStandardBeanByFlowId(Long flowId) throws Exception {
        try {

            String sql = "select b.criterion_class_id,b.criterion_class_name,b.related_id,b.stan_type"
                    + " ,CASE WHEN b.stan_type=1 AND JF.FILE_ID IS NULL THEN '0' ELSE '1' END AS IS_DELETE"
                    + " from JECN_FLOW_STANDARD_T a, JECN_CRITERION_CLASSES b"
                    + " LEFT JOIN JECN_FILE_T JF ON b.stan_type=1 AND b.related_id=JF.FILE_ID AND JF.DEL_STATE=0"
                    + " where a.criterion_class_id = b.criterion_class_id" + " and a.flow_id = ?";
            List<Object[]> list = this.processBasicDao.listNativeSql(sql, flowId);
            List<StandardBean> listStandardBean = new ArrayList<StandardBean>();
            for (Object[] obj : list) {
                if (obj == null || obj[0] == null || obj[1] == null || obj[3] == null || obj[4] == null
                        || "0".equals(obj[4].toString())) {
                    continue;
                }
                StandardBean standardBean = new StandardBean();
                standardBean.setCriterionClassId(Long.parseLong(obj[0].toString()));
                standardBean.setCriterionClassName(obj[1].toString());
                if (obj[2] != null) {
                    standardBean.setRelationId(Long.valueOf(obj[2].toString()));
                }
                standardBean.setStanType(Integer.parseInt(obj[3].toString()));
                listStandardBean.add(standardBean);
            }
            return listStandardBean;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public JecnFlowStructureT createProcessFile(ProcessFileNodeData processFileData) throws Exception {
        JecnFlowStructureT flowStructureT = processFileData.getFlowStructureT();
        // 0:流程/架构图； 1：流程（文件）
        flowStructureT.setNodeType(1);
        try {
            JecnSaveProcessData processData = new JecnSaveProcessData();
            processData.setJecnFlowStructureT(flowStructureT);
            processData.setJecnFlowBasicInfoT(processFileData.getBasicInfoT());
            processData.setPosIds(null);
            processData.setPosGroupIds(null);
            processData.setOrgIds(null);
            processData.setSptIds(null);
            processData.setUpdatePersionId(processFileData.getCreatePeopleId());
            // 1、创建流程节点
            flowStructureT = this.addFlow(processData);
            processFileData.setFlowStructureT(flowStructureT);
            saveOrUpdateProcessFile(processFileData);
        } catch (Exception e) {
            log.error("流程（文件）创建节点异常！", e);
        }
        return flowStructureT;
    }

    @Override
    public JecnFlowStructureT updateProcessFile(ProcessFileNodeData processFileData) throws Exception {
        try {
            JecnFlowStructureT result = saveOrUpdateProcessFile(processFileData);
            JecnFlowStructureT jecnFlowStructureT = processFileData.getFlowStructureT();
            // 添加日志
            JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 2,
                    processFileData.getInfoBean().getUpdatePeopleId(), flowStructureDao);
            return result;
        } catch (Exception e) {
            log.error("流程（文件）更新属性异常！", e);
        }
        return null;
    }

    private JecnFlowStructureT saveOrUpdateProcessFile(ProcessFileNodeData processFileData) throws Exception {
        JecnFlowStructureT flowStructureT = processFileData.getFlowStructureT();
        Long flowId = flowStructureT.getFlowId();
        // 2、保存流程（文件）
        FlowFileContent fileContent = processFileData.getFileContent();
        if (fileContent != null && fileContent.getBytes() != null) {
            fileContent.setCreatePeopleId(processFileData.getCreatePeopleId());
            fileContent.setRelatedId(flowId);
            saveProcessFile(fileContent);
            // 3、更新流程节点和流程文件内容关联关系
            flowStructureT.setFileContentId(fileContent.getId());
            flowStructureDao.update(flowStructureT);

            // 保存历史版本到本地
            saveVersionFileToLocal(flowStructureT.getFlowId(), fileContent.getId(), processFileData.getCreatePeopleId());
        }

        // 4、保存流程属性
        processFileData.getInfoBean().setFlowId(flowId);
        updateFlowAttribute(processFileData.getInfoBean(), false);
        // 5、标准化文件
        saveOrUpdateRelatedStandardizedFiles(flowId, JecnCommon.getIdsByTreeBeans(processFileData
                .getRelatedStandardizeds()));
        // 4、相关标准
        saveOrUpdateRelatedStandards(flowId, JecnCommon.getIdsByTreeBeans(processFileData.getRelatedStandards()));
        // 5、相关制度
        saveOrUpdateRealtedRules(flowId, JecnCommon.getIdsByTreeBeans(processFileData.getRelatedRules()));
        // 6、相关风险
        saveOrUpdateRelatedRisks(flowId, JecnCommon.getIdsByTreeBeans(processFileData.getRelatedRisks()));
        return flowStructureT;
    }

    private void saveVersionFileToLocal(Long flowId, Long contentId, Long updatePersonId) throws Exception {
        String hql = "from FlowFileContent where relatedId = ? and isVersionLocal = 1 and id <> " + contentId;
        List<FlowFileContent> fileContents = flowStructureDao.listHql(hql, flowId);

        if (fileContents.size() == 0) {
            return;
        }
        // 获取 正式表
        JecnFlowStructure flowStructure = flowStructureDao.findJecnFlowStructureById(flowId);

        for (FlowFileContent fileContent : fileContents) {
            if (flowStructure != null && flowStructure.getFileContentId() != null
                    && fileContent.getId().equals(flowStructure.getFileContentId())) {
                // 过滤掉发布版本
                continue;
            }
            byte[] bytes = JecnDaoUtil.blobToBytes(fileContent.getFileStream());
            if (bytes == null) {
                log.error("saveVersionFileToLocal 保存制度版本文件到本地异常！");
                throw new IllegalArgumentException("保存制度版本文件到本地异常！");
            }
            // 2、更新版本文件标识
            fileContent.setIsVersionLocal(0);
            fileContent.setCreateTime(new Date());
            fileContent.setCreatePeopleId(updatePersonId);
            fileContent.setFileStream(null);
            // 3、保存版本文件到本地
            String filePath = JecnFinal.saveFileToLocal(bytes, fileContent.getFileName(), JecnFinal.FLOW_FILE_RELEASE);
            if (filePath == null) {
                throw new IllegalAnnotationException("JecnFinal.saveFileToLocal获取文件路径非法!");
            }
            fileContent.setFilePath(filePath);

            flowStructureDao.getSession().update(fileContent);
            flowStructureDao.getSession().flush();
        }
    }

    @SuppressWarnings("deprecation")
    private void saveProcessFile(FlowFileContent fileContent) throws Exception {
        // 文件保存数据库
        fileContent.setCreateTime(new Date());
        // 存储数据库，标识为1
        fileContent.setIsVersionLocal(1);

        if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
            fileContent.setFileStream(Hibernate.createBlob(fileContent.getBytes()));
            this.flowStructureDao.getSession().save(fileContent);
        } else if (JecnContants.dbType.equals(DBType.ORACLE)) {
            fileContent.setFileStream(Hibernate.createBlob(new byte[1]));
            flowStructureDao.getSession().save(fileContent);
            flowStructureDao.getSession().flush();
            flowStructureDao.getSession().refresh(fileContent, LockMode.UPGRADE);
            SerializableBlob sb = (SerializableBlob) fileContent.getFileStream();
            java.sql.Blob wrapBlob = sb.getWrappedBlob();
            BLOB blob = (BLOB) wrapBlob;
            try {
                OutputStream out = blob.getBinaryOutputStream();
                out.write(fileContent.getBytes());
                out.close();
            } catch (SQLException ex) {
                log.error("写blob大字段错误！", ex);
                throw ex;
            } catch (IOException ex) {
                log.error("写blob大字段错误！", ex);
                throw ex;
            }
        }
    }

    /**
     * 更新流程属性
     */
    public void updateFlowAttribute(ProcessInfoBean processInfoBean) throws Exception {
        updateFlowAttribute(processInfoBean, true);
    }

    public void updateFlowAttribute(ProcessInfoBean processInfoBean, boolean hasLog) throws Exception {
        Long flowId = processInfoBean.getFlowId();
        JecnFlowStructureT jecnFlowStructureT = this.flowStructureDao.get(flowId);
        jecnFlowStructureT.setUpdateDate(new Date());
        jecnFlowStructureT.setUpdatePeopleId(processInfoBean.getUpdatePeopleId());
        // 流程密级
        jecnFlowStructureT.setIsPublic(Long.valueOf(processInfoBean.getIsPublic()));
        if (!JecnConfigTool.isShowItem(ConfigItemPartMapMark.isStandardizedAutoCode)) {// 不是自动编号，获取本地编号保存
            jecnFlowStructureT.setFlowIdInput(processInfoBean.getFlowNumber());
        }
        jecnFlowStructureT.setConfidentialityLevel(processInfoBean.getConfidentialityLevel());
        jecnFlowStructureT.setCommissionerId(processInfoBean.getCommissionerId());
        jecnFlowStructureT.setKeyword(processInfoBean.getKeyWord());
        jecnFlowStructureT.setBussType(processInfoBean.getBussType());
        flowStructureDao.update(jecnFlowStructureT);
        flowStructureDao.getSession().flush();

        JecnFlowBasicInfoT jecnFlowBasicInfoT = this.processBasicDao.get(flowId);

        jecnFlowBasicInfoT.setTypeId(processInfoBean.getTypeId());
        jecnFlowBasicInfoT.setResPeopleId(processInfoBean.getResPersonId());
        jecnFlowBasicInfoT.setTypeResPeople(processInfoBean.getResPersonType());
        jecnFlowBasicInfoT.setExpiry(Long.valueOf(processInfoBean.getValidityValue()));
        jecnFlowBasicInfoT.setGuardianId(processInfoBean.getGuardianId());
        jecnFlowBasicInfoT.setFictionPeopleId(processInfoBean.getFictionPeopleId());
        // 附件
        jecnFlowBasicInfoT.setFileId(processInfoBean.getFileId());
        processBasicDao.update(jecnFlowBasicInfoT);
        if (processInfoBean.getResOrgId() == null) {
            String hql = "delete from FlowOrgT where flowId=?";
            this.flowOrgDao.execteHql(hql, flowId);
        } else {
            FlowOrgT flowOrgT = processBasicDao.getFlowOrgTByFlowId(flowId);
            if (flowOrgT != null) {
                if (!processInfoBean.getResOrgId().equals(flowOrgT.getOrgId())) {
                    flowOrgT.setOrgId(processInfoBean.getResOrgId());
                    this.flowOrgDao.update(flowOrgT);
                }
            } else {
                flowOrgT = new FlowOrgT();
                flowOrgT.setFlowId(flowId);
                flowOrgT.setOrgId(processInfoBean.getResOrgId());
                this.flowOrgDao.save(flowOrgT);
            }
        }
        // 支持工具
        String hql = "from JecnSustainToolRelatedT where flowId=?";
        List<JecnSustainToolRelatedT> listJecnSustainToolRelatedT = processBasicDao.listHql(hql, flowId);
        String supportToolIds = processInfoBean.getSupportToolIds();
        if (supportToolIds != null && !"".equals(supportToolIds)) {
            String[] strArr = supportToolIds.split(",");
            for (String str : strArr) {
                if (str == null || "".equals(str)) {
                    continue;
                }
                boolean isExist = false;
                for (JecnSustainToolRelatedT jecnSustainToolRelatedT : listJecnSustainToolRelatedT) {
                    if (Long.valueOf(str).equals(jecnSustainToolRelatedT.getFlowSustainToolId())) {
                        listJecnSustainToolRelatedT.remove(jecnSustainToolRelatedT);
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    JecnSustainToolRelatedT jecnSustainToolRelatedT = new JecnSustainToolRelatedT();
                    jecnSustainToolRelatedT.setFlowId(flowId);
                    jecnSustainToolRelatedT.setFlowSustainToolId(Long.valueOf(str));
                    processBasicDao.getSession().save(jecnSustainToolRelatedT);
                }
            }
        }
        for (JecnSustainToolRelatedT jecnSustainToolRelatedT : listJecnSustainToolRelatedT) {
            processBasicDao.getSession().delete(jecnSustainToolRelatedT);
        }
        // 岗位查阅权限
        positionAccessPermissionsDao.savaOrUpdate(flowId, 0, processInfoBean.getAccId().getPosAccessId(), false);
        // 部门查阅权限
        orgAccessPermissionsDao.savaOrUpdate(flowId, 0, processInfoBean.getAccId().getOrgAccessId(), false);
        // 岗位组查阅权限
        posGroupAccessPermissionsDao.savaOrUpdate(flowId, 0, processInfoBean.getAccId().getPosGroupAccessId(), false);

        if (hasLog) {
            // 添加日志
            JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 2,
                    processInfoBean.getUpdatePeopleId(), flowStructureDao);
        }
    }

    @Override
    // 更新流程概括信息
    public void updateFlowBaseInformation(Long flowId, JecnFlowStructureT jecnFlowStructureT,
                                          JecnFlowBasicInfoT jecnFlowBasicInfoT, String orgIds, String posIds, String supportToolIds,
                                          Long updatePeopleId, String posGroupIds) throws Exception {
        try {
            JecnFlowStructureT flowStructureT = this.flowStructureDao.get(flowId);
            flowStructureT.setFlowName(jecnFlowStructureT.getFlowName());
            flowStructureT.setUpdateDate(new Date());
            flowStructureT.setUpdatePeopleId(updatePeopleId);
            // 流程编号
            flowStructureT.setFlowIdInput(jecnFlowStructureT.getFlowIdInput());
            // 流程密级
            flowStructureT.setIsPublic(jecnFlowStructureT.getIsPublic());
            flowStructureDao.update(flowStructureT);
            // 添加日志
            JecnUtil.saveJecnJournal(flowStructureT.getFlowId(), flowStructureT.getFlowName(), 1, 2, updatePeopleId,
                    flowStructureDao);
            JecnFlowBasicInfoT flowBasicInfoT = this.processBasicDao.get(flowId);
            // 流程输入
            flowBasicInfoT.setInput(jecnFlowBasicInfoT.getInput());
            // 流程输出
            flowBasicInfoT.setOuput(jecnFlowBasicInfoT.getOuput());
            // 流程客户
            flowBasicInfoT.setFlowCustom(jecnFlowBasicInfoT.getFlowCustom());
            // 活动起始
            flowBasicInfoT.setFlowStartpoint(jecnFlowBasicInfoT.getFlowStartpoint());
            // 活动终止
            flowBasicInfoT.setFlowEndpoint(jecnFlowBasicInfoT.getFlowEndpoint());
            // 附件
            flowBasicInfoT.setFileId(jecnFlowBasicInfoT.getFileId());
            flowBasicInfoT.setFlowName(jecnFlowBasicInfoT.getFlowName());
            processBasicDao.update(flowBasicInfoT);
            String hql = "from JecnSustainToolRelatedT where flowId=?";
            List<JecnSustainToolRelatedT> listJecnSustainToolRelatedT = processBasicDao.listHql(hql, flowId);
            if (supportToolIds != null && !"".equals(supportToolIds)) {
                String[] strArr = supportToolIds.split(",");
                for (String str : strArr) {
                    if (str == null || "".equals(str)) {
                        continue;
                    }
                    boolean isExist = false;
                    for (JecnSustainToolRelatedT jecnSustainToolRelatedT : listJecnSustainToolRelatedT) {
                        if (Long.valueOf(str).equals(jecnSustainToolRelatedT.getFlowSustainToolId())) {
                            listJecnSustainToolRelatedT.remove(jecnSustainToolRelatedT);
                            isExist = true;
                            break;
                        }
                    }
                    if (!isExist) {
                        JecnSustainToolRelatedT jecnSustainToolRelatedT = new JecnSustainToolRelatedT();
                        jecnSustainToolRelatedT.setFlowId(flowId);
                        jecnSustainToolRelatedT.setFlowSustainToolId(Long.valueOf(str));
                        processBasicDao.getSession().save(jecnSustainToolRelatedT);
                    }
                }
            }
            for (JecnSustainToolRelatedT jecnSustainToolRelatedT : listJecnSustainToolRelatedT) {
                processBasicDao.getSession().delete(jecnSustainToolRelatedT);
            }
            // 岗位查阅权限
            positionAccessPermissionsDao.savaOrUpdate(flowId, 0, posIds, false);
            // 部门查阅权限
            orgAccessPermissionsDao.savaOrUpdate(flowId, 0, orgIds, false);
            // 岗位组查阅权限
            posGroupAccessPermissionsDao.savaOrUpdate(flowId, 0, posGroupIds, false);

            // 　修改关联本流程架构的元素的名称
            flowStructureImageTDao.updateLinkedImageName(flowId, JecnUtil.getShowName(flowStructureT.getFlowName(),
                    flowStructureT.getFlowIdInput()));
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 更新操作说明
    public void updateFlowOperation(Long flowId, JecnFlowBasicInfoT jecnFlowBasicInfoT,
                                    JecnFlowDriverT jecnFlowDriverT, List<JecnFlowRecordT> listJecnFlowRecordT, Long updatePeopleId)
            throws Exception {
        try {
            updateFlowModifyRecord(flowId, updatePeopleId, 6);
            JecnFlowBasicInfoT jecnFlowBasicInfoTOld = this.processBasicDao.get(flowId);
            if (!JecnContants.isPubShow()) {// D2版本
                JecnFlowBasicInfoT2 jecnFlowBasicInfoT2Old = this.processBasic2Dao.get(flowId);
                if (jecnFlowBasicInfoT2Old != null) {
                    // 相关文件(D2版本专用)
                    jecnFlowBasicInfoT2Old.setFlowRelatedFile(jecnFlowBasicInfoT.getFlowRelatedFile());
                    // 自定义要素1
                    jecnFlowBasicInfoT2Old.setFlowCustomOne(jecnFlowBasicInfoT.getFlowCustomOne());
                    // 自定义要素2
                    jecnFlowBasicInfoT2Old.setFlowCustomTwo(jecnFlowBasicInfoT.getFlowCustomTwo());
                    // 自定义要素3
                    jecnFlowBasicInfoT2Old.setFlowCustomThree(jecnFlowBasicInfoT.getFlowCustomThree());
                    // 自定义要素4
                    jecnFlowBasicInfoT2Old.setFlowCustomFour(jecnFlowBasicInfoT.getFlowCustomFour());
                    // 自定义要素5
                    jecnFlowBasicInfoT2Old.setFlowCustomFive(jecnFlowBasicInfoT.getFlowCustomFive());
                    this.processBasic2Dao.update(jecnFlowBasicInfoT2Old);
                }
            } else {
                // 自定义要素1
                jecnFlowBasicInfoTOld.setFlowCustomOne(jecnFlowBasicInfoT.getFlowCustomOne());
                // 自定义要素2
                jecnFlowBasicInfoTOld.setFlowCustomTwo(jecnFlowBasicInfoT.getFlowCustomTwo());
                // 自定义要素3
                jecnFlowBasicInfoTOld.setFlowCustomThree(jecnFlowBasicInfoT.getFlowCustomThree());
                // 自定义要素4
                jecnFlowBasicInfoTOld.setFlowCustomFour(jecnFlowBasicInfoT.getFlowCustomFour());
                // 自定义要素5
                jecnFlowBasicInfoTOld.setFlowCustomFive(jecnFlowBasicInfoT.getFlowCustomFive());
            }
            // 目的
            jecnFlowBasicInfoTOld.setFlowPurpose(jecnFlowBasicInfoT.getFlowPurpose());
            // 适用范围
            jecnFlowBasicInfoTOld.setApplicability(jecnFlowBasicInfoT.getApplicability());
            // 术语定义
            jecnFlowBasicInfoTOld.setNoutGlossary(jecnFlowBasicInfoT.getNoutGlossary());
            // 驱动类型，驱动规则
            jecnFlowBasicInfoTOld.setDriveType(jecnFlowBasicInfoT.getDriveType());
            jecnFlowBasicInfoTOld.setDriveRules(jecnFlowBasicInfoT.getDriveRules());
            // 流程输入、流程输出
            jecnFlowBasicInfoTOld.setInput(jecnFlowBasicInfoT.getInput());
            jecnFlowBasicInfoTOld.setOuput(jecnFlowBasicInfoT.getOuput());
            // 流程边界--起始活动
            jecnFlowBasicInfoTOld.setFlowStartpoint(jecnFlowBasicInfoT.getFlowStartpoint());
            // 流程边界-终止活动
            jecnFlowBasicInfoTOld.setFlowEndpoint(jecnFlowBasicInfoT.getFlowEndpoint());
            // 流程客户
            jecnFlowBasicInfoTOld.setFlowCustom(jecnFlowBasicInfoT.getFlowCustom());
            // 不适用范围
            jecnFlowBasicInfoTOld.setNoApplicability(jecnFlowBasicInfoT.getNoApplicability());
            // 概述
            jecnFlowBasicInfoTOld.setFlowSummarize(jecnFlowBasicInfoT.getFlowSummarize());
            // 补充说明
            jecnFlowBasicInfoTOld.setFlowSupplement(jecnFlowBasicInfoT.getFlowSupplement());
            this.processBasicDao.update(jecnFlowBasicInfoTOld);

            // 驱动规则
            processBasicDao.getSession().saveOrUpdate(jecnFlowDriverT);
            // 流程记录
            List<JecnFlowRecordT> listJecnFlowRecordTBefore = processRecordDao.getJecnFlowRecordTListByFlowId(flowId);
            for (JecnFlowRecordT jecnFlowRecordT : listJecnFlowRecordT) {
                boolean isExist = false;
                if (listJecnFlowRecordTBefore != null) {
                    for (JecnFlowRecordT flowRecordT : listJecnFlowRecordTBefore) {
                        if (jecnFlowRecordT.getId() != null && jecnFlowRecordT.getId().equals(flowRecordT.getId())) {
                            isExist = true;
                            boolean isModify = false;
                            if (jecnFlowRecordT.getRecordName() != null
                                    && !jecnFlowRecordT.getRecordName().equals(flowRecordT.getRecordName())) {
                                isModify = true;
                                flowRecordT.setRecordName(jecnFlowRecordT.getRecordName());
                            }
                            if (jecnFlowRecordT.getRecordApproach() != null
                                    && !jecnFlowRecordT.getRecordApproach().equals(flowRecordT.getRecordApproach())) {
                                isModify = true;
                                flowRecordT.setRecordApproach(jecnFlowRecordT.getRecordApproach());
                            }

                            if (jecnFlowRecordT.getRecordSavePeople() != null
                                    && !jecnFlowRecordT.getRecordSavePeople().equals(flowRecordT.getRecordSavePeople())) {
                                isModify = true;
                                flowRecordT.setRecordSavePeople(jecnFlowRecordT.getRecordSavePeople());
                            }
                            if (jecnFlowRecordT.getSaveLaction() != null
                                    && !jecnFlowRecordT.getSaveLaction().equals(flowRecordT.getSaveLaction())) {
                                isModify = true;
                                flowRecordT.setSaveLaction(jecnFlowRecordT.getSaveLaction());
                            }
                            if (jecnFlowRecordT.getSaveTime() != null
                                    && !jecnFlowRecordT.getSaveTime().equals(flowRecordT.getSaveTime())) {
                                isModify = true;
                                flowRecordT.setSaveTime(jecnFlowRecordT.getSaveTime());
                            }
                            if (jecnFlowRecordT.getFileTime() != null
                                    && !jecnFlowRecordT.getFileTime().equals(flowRecordT.getFileTime())) {
                                isModify = true;
                                flowRecordT.setFileTime(jecnFlowRecordT.getFileTime());
                            }
                            if (!isModify) {
                                processRecordDao.update(flowRecordT);
                            }
                            listJecnFlowRecordTBefore.remove(flowRecordT);
                            break;
                        }
                    }
                }
                if (!isExist) {
                    processRecordDao.save(jecnFlowRecordT);
                }
            }
            // 删除多余的流程记录
            for (JecnFlowRecordT flowRecordT : listJecnFlowRecordTBefore) {
                processRecordDao.deleteObject(flowRecordT);
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public void updateFlowRelateRuleT(Long flowId, String ruleIds, String standardIds, Long updatePeopleId)
            throws Exception {
        this.updateFlowModifyRecord(flowId, updatePeopleId, 5);
        try {
            // 相关制度
            String hql = "from FlowCriterionT where flowId=?";
            List<FlowCriterionT> listFlowCriterionT = processBasicDao.listHql(hql, flowId);
            if (ruleIds != null && !"".equals(ruleIds)) {

                String[] strArr = ruleIds.split(",");
                for (String str : strArr) {
                    if (str == null || "".equals(str)) {
                        continue;
                    }
                    boolean isExist = false;
                    for (FlowCriterionT flowCriterionT : listFlowCriterionT) {
                        if (Long.valueOf(str).equals(flowCriterionT.getCriterionClassId())) {
                            listFlowCriterionT.remove(flowCriterionT);
                            isExist = true;
                            break;
                        }
                    }
                    if (!isExist) {
                        FlowCriterionT flowCriterionT = new FlowCriterionT();
                        flowCriterionT.setFlowId(flowId);
                        flowCriterionT.setCriterionClassId(Long.valueOf(str));
                        processBasicDao.getSession().save(flowCriterionT);
                    }
                }

            }
            for (FlowCriterionT flowCriterionT : listFlowCriterionT) {
                processBasicDao.getSession().delete(flowCriterionT);
            }
            // 相关标准
            hql = "from StandardFlowRelationTBean where flowId=?";
            List<StandardFlowRelationTBean> listStandardFlowRelationTBean = processBasicDao.listHql(hql, flowId);
            if (standardIds != null && !"".equals(standardIds)) {

                String[] strArr = standardIds.split(",");
                for (String str : strArr) {
                    if (str == null || "".equals(str)) {
                        continue;
                    }
                    boolean isExist = false;
                    for (StandardFlowRelationTBean standardFlowRelationTBean : listStandardFlowRelationTBean) {
                        if (str.equals(standardFlowRelationTBean.getCriterionClassId())) {
                            isExist = true;
                            listStandardFlowRelationTBean.remove(standardFlowRelationTBean);
                            break;
                        }
                    }
                    if (!isExist) {
                        StandardFlowRelationTBean standardFlowRelationTBean = new StandardFlowRelationTBean();
                        standardFlowRelationTBean.setFlowId(flowId);
                        standardFlowRelationTBean.setCriterionClassId(Long.valueOf(str));
                        processBasicDao.getSession().save(standardFlowRelationTBean);
                    }
                }
            }
            for (StandardFlowRelationTBean standardFlowRelationTBean : listStandardFlowRelationTBean) {
                processBasicDao.getSession().delete(standardFlowRelationTBean);
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 更新KPI
    public void updateKPI(JecnFlowKpiNameT jecnFlowKpiNameT, Long updatePeopleId) throws Exception {
        try {
            // 更新流程修改记录
            this.updateFlowModifyRecord(jecnFlowKpiNameT.getFlowId(), updatePeopleId, 11);
            // 获取关联Id，关联类型对应的数据库中的关联关系数据集合
            String hql = "from JecnSustainToolConnTBean where relatedId=? and relatedType = 0";
            List<JecnSustainToolConnTBean> listJecnSustainToolConnTBean = processBasicDao.listHql(hql, jecnFlowKpiNameT
                    .getKpiAndId());
            // 更新 “与支持工具的关联关系”表中的数据
            String supportToolIds = jecnFlowKpiNameT.getKpiITSystemIds();
            // 要删除的支持工具Ids集合
            List<Long> delToolIds = new ArrayList<Long>();
            if (supportToolIds != null && !"".equals(supportToolIds)) {
                String[] strArr = supportToolIds.split(",");
                for (String str : strArr) {
                    if (str == null || "".equals(str)) {
                        continue;
                    }
                    boolean isExist = false;
                    for (JecnSustainToolConnTBean sustainToolConnTBean : listJecnSustainToolConnTBean) {
                        if (Long.valueOf(str).equals(sustainToolConnTBean.getSustainToolId())) {
                            isExist = true;
                            delToolIds.add(sustainToolConnTBean.getSustainToolId());
                            break;
                        }
                    }
                    if (!isExist) {// 不存在，添加关联关系
                        JecnSustainToolConnTBean jecnSustainToolConnTBean = new JecnSustainToolConnTBean();
                        jecnSustainToolConnTBean.setId(JecnCommon.getUUID());// 主键ID
                        jecnSustainToolConnTBean.setRelatedId(jecnFlowKpiNameT.getKpiAndId());// 关联ID
                        jecnSustainToolConnTBean.setRelatedType(0);// 关联类型
                        jecnSustainToolConnTBean.setSustainToolId(Long.valueOf(str));// 支持工具ID
                        jecnSustainToolConnTBean.setCreatePeopleId(updatePeopleId);
                        jecnSustainToolConnTBean.setCreateTime(new Date());
                        jecnSustainToolConnTBean.setUpdatePeopleId(updatePeopleId);
                        jecnSustainToolConnTBean.setUpdateTime(new Date());
                        processBasicDao.getSession().save(jecnSustainToolConnTBean);
                    }
                }
            }
            for (JecnSustainToolConnTBean jecnSustainToolConnTBean : listJecnSustainToolConnTBean) {
                // for(Long delId : delToolIds){
                if (!delToolIds.contains(jecnSustainToolConnTBean.getSustainToolId())) {
                    processBasicDao.getSession().delete(jecnSustainToolConnTBean);
                }
                // }
            }
            this.processKPIDao.update(jecnFlowKpiNameT);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public void updateKPIVaule(JecnFlowKpi jecnFlowKpi, Long updatePeopleId, Long flowId) throws Exception {
        try {
            this.updateFlowModifyRecord(flowId, updatePeopleId, 11);
            // 删除该kpi的某个时间段的值
            String hql = "delete from JecnFlowKpi where kpiAndId=? and kpiHorVlaue=?";
            Date kpiHorValue = jecnFlowKpi.getKpiHorVlaue();
            if (JecnCommon.isNullOrEmtryTrim(jecnFlowKpi.getKpiValue())) {// 为空说明该条数据需要删除
                this.processKPIValueDao.execteHql(hql, jecnFlowKpi.getKpiAndId(), kpiHorValue);
            } else {// 不为空说明需要保存
                this.processKPIValueDao.execteHql(hql, jecnFlowKpi.getKpiAndId(), kpiHorValue);
                this.processKPIValueDao.save(jecnFlowKpi);
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public void deleteKPIValues(List<Long> listIds, Long updatePeopleId, Long flowId) throws Exception {
        try {
            this.updateFlowModifyRecord(flowId, updatePeopleId, 11);
            String hql = "delete from JecnFlowKpi where kpiId in" + JecnCommonSql.getIds(listIds);
            processKPIDao.execteHql(hql);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    public void deleteKPIValues(Long id, Long updatePeopleId, Long flowId) throws Exception {
        try {
            // this.updateFlowModifyRecord(flowId, updatePeopleId);
            String hql = "delete from JecnFlowKpi where kpiId =" + id;
            processKPIDao.execteHql(hql);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 删除kpi
    public void deleteKPIs(List<Long> listIds, Long updatePeopleId, Long flowId) throws Exception {
        try {
            this.updateFlowModifyRecord(flowId, updatePeopleId, 11);
            String hql = "delete from JecnFlowKpiNameT where kpiAndId in" + JecnCommonSql.getIds(listIds);
            processKPIDao.execteHql(hql);
            // 删除IT系统与支持工具关联关系表
            hql = "delete from JecnSustainToolConnTBean where relatedId in " + JecnCommonSql.getIds(listIds);
            processKPIDao.getSession().createQuery(hql).executeUpdate();
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * @param mapData         流程地图数据
     * @param flowImgeBytes
     * @param updatePersionId
     * @throws Exception
     * @author yxw 2012-7-31
     * @description:流程地图保存
     */
    @Override
    public void saveProcessMap(ProcessMapData mapData, byte[] flowImgeBytes, Long updatePeopleId) throws Exception {
        JecnFlowStructureT jecnFlowStructureT = mapData.getJecnFlowStructureT();
        List<ProcessMapFigureData> saveFigureDataList = mapData.getSaveFigureDataList();
        List<ProcessMapFigureData> updateFigureDataList = mapData.getUpdateFigureDataList();
        // 流程地图附件集合
        List<JecnFigureFileTBean> listJecnFigureFileTBean = new ArrayList<JecnFigureFileTBean>();
        try {
            if (mapData.isSave()) {
                jecnFlowStructureT.setUpdateDate(new Date());
            }
            // 获取所有元素
            Map<String, Long> figureUUIDMap = getFigureUUIDMaps(jecnFlowStructureT.getFlowId());
            jecnFlowStructureT.setUpdatePeopleId(updatePeopleId);
            // 图片路径
            String imagePath = jecnFlowStructureT.getFilePath();
            try {
                JecnFinal.deleteFile(imagePath);
            } catch (Exception e) {
                log.error("文件删除失败！path=" + imagePath, e);
            }
            // 保存流程图
            String filePath = JecnFinal.getImagePathFileAndSave(flowImgeBytes);
            jecnFlowStructureT.setFilePath(filePath);
            this.flowStructureDao.update(jecnFlowStructureT);
            // 添加日志
            JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 2,
                    updatePeopleId, flowStructureDao, 2);

            // 记录连接线
            List<JecnFlowStructureImageT> relateLinesList = new ArrayList<JecnFlowStructureImageT>();
            // 保持新添加的流程元素
            for (ProcessMapFigureData saveFigureData : saveFigureDataList) {
                JecnFlowStructureImageT jecnFlowStructureImageT = saveFigureData.getJecnFlowStructureImageT();
                if (JecnUtil.isRelateLine(jecnFlowStructureImageT.getFigureType())) {
                    relateLinesList.add(jecnFlowStructureImageT);
                } else {
                    this.flowStructureImageTDao.save(jecnFlowStructureImageT);
                    // session同步 大字段处理
                    this.flowStructureImageTDao.getSession().flush();
                    figureUUIDMap.put(jecnFlowStructureImageT.getUUID(), jecnFlowStructureImageT.getFigureId());
                    // 新增元素，保存元素对应的附件
                    setFigureIdFigureFileT(saveFigureData.getListJecnFigureFileTBean(), jecnFlowStructureImageT
                            .getFigureId(), jecnFlowStructureImageT.getUUID());

                    listJecnFigureFileTBean.addAll(saveFigureData.getListJecnFigureFileTBean());
                }

            }
            // 保存连接线
            for (JecnFlowStructureImageT jecnFlowStructureImageT : relateLinesList) {
                if (jecnFlowStructureImageT.getStartFigure() == null
                        || jecnFlowStructureImageT.getStartFigure().intValue() == -1) {
                    jecnFlowStructureImageT.setStartFigure(figureUUIDMap.get(jecnFlowStructureImageT
                            .getStartFigureUUID()));
                }
                if (jecnFlowStructureImageT.getEndFigure() == null
                        || jecnFlowStructureImageT.getEndFigure().intValue() == -1) {
                    jecnFlowStructureImageT.setEndFigure(figureUUIDMap.get(jecnFlowStructureImageT.getEndFigureUUID()));
                }
                flowStructureImageTDao.save(jecnFlowStructureImageT);
                if (jecnFlowStructureImageT.getListLineSegmet() != null) {
                    for (JecnLineSegmentT jecnLineSegmentT : jecnFlowStructureImageT.getListLineSegmet()) {
                        jecnLineSegmentT.setFigureId(jecnFlowStructureImageT.getFigureId());
                        jecnLineSegmentT.setFigureUUID(jecnFlowStructureImageT.getUUID());
                        lineSegmentTDao.save(jecnLineSegmentT);
                    }
                }
            }
            // 更新的流程元素
            // 需要保存的
            List<List<JecnLineSegmentT>> listSave = new ArrayList<List<JecnLineSegmentT>>();
            for (ProcessMapFigureData updateFigureData : updateFigureDataList) {
                setFileImageFigureID(updateFigureData.getListJecnFigureFileTBean(), figureUUIDMap, updateFigureData
                        .getJecnFlowStructureImageT());

                listJecnFigureFileTBean.addAll(updateFigureData.getListJecnFigureFileTBean());

                JecnFlowStructureImageT jecnFlowStructureImageT = updateFigureData.getJecnFlowStructureImageT();
                if (jecnFlowStructureImageT == null) {
                    continue;
                }
                jecnFlowStructureImageT.setFigureId(figureUUIDMap.get(jecnFlowStructureImageT.getUUID()));
                if (JecnUtil.isRelateLine(jecnFlowStructureImageT.getFigureType())) {
                    if (jecnFlowStructureImageT.getStartFigure() == null
                            || jecnFlowStructureImageT.getStartFigure().intValue() == -1) {
                        jecnFlowStructureImageT.setStartFigure(figureUUIDMap.get(jecnFlowStructureImageT
                                .getStartFigureUUID()));
                    }
                    if (jecnFlowStructureImageT.getEndFigure() == null
                            || jecnFlowStructureImageT.getEndFigure().intValue() == -1) {
                        jecnFlowStructureImageT.setEndFigure(figureUUIDMap.get(jecnFlowStructureImageT
                                .getEndFigureUUID()));
                    }
                    if (jecnFlowStructureImageT.getListLineSegmet() != null
                            && jecnFlowStructureImageT.getListLineSegmet().size() > 0) {
                        if (mapData.getDeleteListLine() == null) {
                            mapData.setDeleteListLine(new ArrayList<String>());
                        }
                        mapData.getDeleteListLine().add(jecnFlowStructureImageT.getUUID());
                        if (jecnFlowStructureImageT.getListLineSegmet() != null) {
                            for (JecnLineSegmentT jecnLineSegmentT : jecnFlowStructureImageT.getListLineSegmet()) {
                                jecnLineSegmentT.setFigureId(jecnFlowStructureImageT.getFigureId());
                                jecnLineSegmentT.setFigureUUID(jecnFlowStructureImageT.getUUID());
                            }
                        }
                        listSave.add(jecnFlowStructureImageT.getListLineSegmet());
                    }
                }
                this.flowStructureImageTDao.update(jecnFlowStructureImageT);
                // session同步 大字段处理
                this.flowStructureImageTDao.getSession().flush();
            }
            // 删除线
            List<Long> deleteListLine = getDeleteFigureIdsByUUID(figureUUIDMap, mapData.getDeleteListLine());
            if (deleteListLine != null && deleteListLine.size() > 0) {
                String hql = "delete from JecnLineSegmentT where figureId in";
                List<String> listHql = JecnCommonSql.getListSqls(hql, deleteListLine);
                for (String str : listHql) {
                    flowStructureDao.execteHql(str);
                }
            }

            // 删除图形
            List<Long> deleteList = getDeleteFigureIdsByUUID(figureUUIDMap, mapData.getDeleteList());
            if (deleteList != null && deleteList.size() > 0) {
                String hql = "delete from JecnFlowStructureImageT where figureId in";
                List<String> listHql = JecnCommonSql.getListSqls(hql, deleteList);
                for (String str : listHql) {
                    flowStructureDao.execteHql(str);
                }
                // 删除元素相关附件
                deleteFigureFile(deleteList);
            }

            // 保存线的线段
            for (List<JecnLineSegmentT> list : listSave) {
                for (JecnLineSegmentT jecnLineSegmentT : list) {
                    lineSegmentTDao.save(jecnLineSegmentT);
                }
            }
            // 保存流程地图附件信息
            editFlowFigureFileTBean(listJecnFigureFileTBean, jecnFlowStructureT.getFlowId());
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * 流程架构集成关系图
     *
     * @param mapData
     * @param flowImgeBytes
     * @param updatePeopleId
     * @throws Exception
     */
    @Override
    public void saveMapRelatedProcess(ProcessMapData mapData, byte[] flowImgeBytes, Long updatePeopleId)
            throws Exception {
        JecnFlowStructureT jecnFlowStructureT = mapData.getJecnFlowStructureT();
        List<ProcessMapFigureData> saveFigureDataList = mapData.getSaveFigureDataList();
        List<ProcessMapFigureData> updateFigureDataList = mapData.getUpdateFigureDataList();
        List<String> deleteListLine = mapData.getDeleteListLine();

        try {
            // 添加日志
            JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 2,
                    updatePeopleId, flowStructureDao, 3);
            // 获取所有元素
            Map<String, Long> figureUUIDMap = getRelatedFigureUUIDMaps(jecnFlowStructureT.getFlowId());
            // 记录连接线
            List<JecnMapRelatedStructureImageT> relateLinesList = new ArrayList<JecnMapRelatedStructureImageT>();
            // 保持新添加的流程元素
            JecnMapRelatedStructureImageT relatedStructureImageT = null;
            for (ProcessMapFigureData saveFigureData : saveFigureDataList) {
                relatedStructureImageT = new JecnMapRelatedStructureImageT();
                // 复制属性
                PropertyUtils.copyProperties(relatedStructureImageT, saveFigureData.getJecnFlowStructureImageT());
                if (JecnUtil.isRelateLine(relatedStructureImageT.getFigureType())) {
                    relateLinesList.add(relatedStructureImageT);
                } else {
                    this.flowStructureImageTDao.getSession().save(relatedStructureImageT);
                    // session同步 大字段处理
                    this.flowStructureImageTDao.getSession().flush();
                    figureUUIDMap.put(relatedStructureImageT.getUUID(), relatedStructureImageT.getFigureId());
                }
            }
            // 保存连接线
            for (JecnMapRelatedStructureImageT jecnFlowStructureImageT : relateLinesList) {
                if (jecnFlowStructureImageT.getStartFigure() == null
                        || jecnFlowStructureImageT.getStartFigure().intValue() == -1) {
                    jecnFlowStructureImageT.setStartFigure(figureUUIDMap.get(jecnFlowStructureImageT
                            .getStartFigureUUID()));
                }
                if (jecnFlowStructureImageT.getEndFigure() == null
                        || jecnFlowStructureImageT.getEndFigure().intValue() == -1) {
                    jecnFlowStructureImageT.setEndFigure(figureUUIDMap.get(jecnFlowStructureImageT.getEndFigureUUID()));
                }
                flowStructureImageTDao.getSession().save(jecnFlowStructureImageT);
                JecnMapLineSegmentT mapLineSegmentT = null;
                if (jecnFlowStructureImageT.getListLineSegmet() != null) {
                    for (JecnLineSegmentT jecnLineSegmentT : jecnFlowStructureImageT.getListLineSegmet()) {
                        jecnLineSegmentT.setFigureId(jecnFlowStructureImageT.getFigureId());
                        mapLineSegmentT = new JecnMapLineSegmentT();
                        // 复制属性
                        PropertyUtils.copyProperties(mapLineSegmentT, jecnLineSegmentT);
                        mapLineSegmentT.setFigureUUID(jecnFlowStructureImageT.getUUID());
                        lineSegmentTDao.getSession().save(mapLineSegmentT);
                    }
                }
            }
            // 更新的流程元素
            // 需要保存的
            List<List<JecnLineSegmentT>> listSave = new ArrayList<List<JecnLineSegmentT>>();
            for (ProcessMapFigureData updateFigureData : updateFigureDataList) {
                // listJecnFigureFileTBean.addAll(updateFigureData.getListJecnFigureFileTBean());
                JecnFlowStructureImageT jecnFlowStructureImageT = updateFigureData.getJecnFlowStructureImageT();
                if (jecnFlowStructureImageT == null) {
                    continue;
                }
                jecnFlowStructureImageT.setFigureId(figureUUIDMap.get(jecnFlowStructureImageT.getUUID()));
                if (JecnUtil.isRelateLine(jecnFlowStructureImageT.getFigureType())) {
                    if (jecnFlowStructureImageT.getStartFigure() == null
                            || jecnFlowStructureImageT.getStartFigure().intValue() == -1) {
                        jecnFlowStructureImageT.setStartFigure(figureUUIDMap.get(jecnFlowStructureImageT
                                .getStartFigureUUID()));
                    }
                    if (jecnFlowStructureImageT.getEndFigure() == null
                            || jecnFlowStructureImageT.getEndFigure().intValue() == -1) {
                        jecnFlowStructureImageT.setEndFigure(figureUUIDMap.get(jecnFlowStructureImageT
                                .getEndFigureUUID()));
                    }
                    if (jecnFlowStructureImageT.getListLineSegmet() != null
                            && jecnFlowStructureImageT.getListLineSegmet().size() > 0) {
                        if (deleteListLine == null) {
                            deleteListLine = new ArrayList<String>();
                        }
                        deleteListLine.add(jecnFlowStructureImageT.getUUID());
                        if (jecnFlowStructureImageT.getListLineSegmet() != null) {
                            for (JecnLineSegmentT jecnLineSegmentT : jecnFlowStructureImageT.getListLineSegmet()) {
                                jecnLineSegmentT.setFigureId(jecnFlowStructureImageT.getFigureId());
                                jecnLineSegmentT.setFigureUUID(jecnFlowStructureImageT.getUUID());
                            }
                        }
                        listSave.add(jecnFlowStructureImageT.getListLineSegmet());
                    }
                }
                relatedStructureImageT = new JecnMapRelatedStructureImageT();
                // 复制属性
                PropertyUtils.copyProperties(relatedStructureImageT, jecnFlowStructureImageT);
                relatedStructureImageT.setFigureId(figureUUIDMap.get(relatedStructureImageT.getUUID()));
                this.flowStructureImageTDao.getSession().update(relatedStructureImageT);
                // session同步 大字段处理
                this.flowStructureImageTDao.getSession().flush();
            }
            // 删除线
            List<Long> deleteLineIds = getDeleteFigureIdsByUUID(figureUUIDMap, deleteListLine);
            if (deleteLineIds != null && deleteLineIds.size() > 0) {
                String hql = "delete from JecnMapLineSegmentT where figureId in";
                List<String> listHql = JecnCommonSql.getListSqls(hql, deleteLineIds);
                for (String str : listHql) {
                    flowStructureDao.execteHql(str);
                }
            }

            // 删除图形
            List<Long> deleteFigureIds = getDeleteFigureIdsByUUID(figureUUIDMap, mapData.getDeleteList());
            if (deleteFigureIds != null && deleteFigureIds.size() > 0) {
                String hql = "delete from JecnMapRelatedStructureImageT where figureId in";
                List<String> listHql = JecnCommonSql.getListSqls(hql, deleteFigureIds);
                for (String str : listHql) {
                    flowStructureDao.execteHql(str);
                }
            }

            // 保存线的线段
            JecnMapLineSegmentT mapLineSegmentT = null;
            for (List<JecnLineSegmentT> list : listSave) {
                for (JecnLineSegmentT jecnLineSegmentT : list) {
                    mapLineSegmentT = new JecnMapLineSegmentT();
                    // 复制属性
                    PropertyUtils.copyProperties(mapLineSegmentT, jecnLineSegmentT);
                    lineSegmentTDao.getSession().save(mapLineSegmentT);
                }
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * 保存流程地图附件信息
     *
     * @param listJecnFigureFileTBean
     * @param flowId
     * @throws Exception
     */
    private void editFlowFigureFileTBean(List<JecnFigureFileTBean> listJecnFigureFileTBean, Long flowId)
            throws Exception {

        if (flowId == null || -1 == flowId) {
            return;
        }
        // 获取当前地图对应的所有附件
        List<JecnFigureFileTBean> listDataFileT = this.flowStructureDao.findJecnFigureFileTList(flowId);
        Session s = this.flowStructureDao.getSession();

        // 传过来的是要保存的数据如果为空则删除所有的附件
        if (listJecnFigureFileTBean == null || listJecnFigureFileTBean.size() == 0) {
            for (JecnFigureFileTBean dataFileT : listDataFileT) {// 循环删除数据库数据
                s.delete(dataFileT);
                s.flush();
            }
            return;
        }

        // 记录当前流程文件 存在的所有附件ID集合
        List<String> list = new ArrayList<String>();
        List<JecnFigureFileTBean> updateList = new ArrayList<JecnFigureFileTBean>();
        for (JecnFigureFileTBean jecnFigureFileTBean : listJecnFigureFileTBean) {
            if (JecnCommon.isNullOrEmtryTrim(jecnFigureFileTBean.getId())) {// 新增
                // 设置UUID主键
                jecnFigureFileTBean.setId(JecnCommon.getUUID());
                s.save(jecnFigureFileTBean);
                s.flush();
                list.add(jecnFigureFileTBean.getId());
            } else {
                // 更新
                updateList.add(jecnFigureFileTBean);

                list.add(jecnFigureFileTBean.getId());
            }
        }
        // 删除
        boolean isDelete = false;
        for (JecnFigureFileTBean dataFileT : listDataFileT) {// 循环数据库数据
            if (list.contains(dataFileT.getId())) {// 传入的集合包含当前数据
                isDelete = true;
            }
            if (!isDelete) {
                s.delete(dataFileT);
                s.flush();
            }
            isDelete = false;
        }

        for (JecnFigureFileTBean jecnFigureFileTBean : updateList) {// 更新
            s.saveOrUpdate(jecnFigureFileTBean);
            s.flush();
        }
    }

    private void setFigureIdFigureFileT(List<JecnFigureFileTBean> listJecnFigureFileTBean, Long figureId,
                                        String figureUUID) {
        if (listJecnFigureFileTBean == null || figureId == null) {
            return;
        }
        for (JecnFigureFileTBean jecnFigureFileTBean : listJecnFigureFileTBean) {
            jecnFigureFileTBean.setFigureId(figureId);
            jecnFigureFileTBean.setFigureUUID(figureUUID);
        }
    }

    /**
     * @param figureId
     * @param listJecnModeFileT
     * @return
     * @author zhangchen Oct 11, 2012
     * @description：通过活动Id获得保存前的输出
     */
    private List<JecnModeFileT> getJecnModeFileTByFigureId(Long figureId, List<JecnModeFileT> listJecnModeFileT) {
        List<JecnModeFileT> listResult = new ArrayList<JecnModeFileT>();
        for (JecnModeFileT jecnModeFileT : listJecnModeFileT) {
            if (figureId.equals(jecnModeFileT.getFigureId())) {
                listResult.add(jecnModeFileT);
            }
        }
        return listResult;
    }

    /**
     * @param figureId
     * @param listJecnTempletT
     * @return
     * @author zhangchen Oct 11, 2012
     * @description：通过活动的输出Id获得保存前的样例
     */
    private List<JecnTempletT> getJecnTempletTByFigureModeId(Long figureModeId, List<JecnTempletT> listJecnTempletT) {
        List<JecnTempletT> listResult = new ArrayList<JecnTempletT>();
        for (JecnTempletT jecnTempletT : listJecnTempletT) {
            if (figureModeId.equals(jecnTempletT.getModeFileId())) {
                listResult.add(jecnTempletT);
            }
        }
        return listResult;
    }

    /**
     * @param figureId
     * @param listJecnActivityFileT
     * @return
     * @author zhangchen Oct 11, 2012
     * @description：通过活动Id获得保存前的输入和样例
     */
    private List<JecnActivityFileT> getJecnActivityFileTByFigureId(Long figureId,
                                                                   List<JecnActivityFileT> listJecnActivityFileT) {
        List<JecnActivityFileT> listResult = new ArrayList<JecnActivityFileT>();
        for (JecnActivityFileT jecnActivityFileT : listJecnActivityFileT) {
            if (figureId.equals(jecnActivityFileT.getFigureId())) {
                listResult.add(jecnActivityFileT);
            }
        }
        return listResult;
    }

    /**
     * @param figureId
     * @param listJecnRefIndicatorsT
     * @return
     * @author zhangchen Oct 11, 2012
     * @description：通过活动Id获得保存前的指标
     */
    private List<JecnRefIndicatorsT> getJecnJecnRefIndicatorsTByFigureId(Long figureId,
                                                                         List<JecnRefIndicatorsT> listJecnRefIndicatorsT) {
        List<JecnRefIndicatorsT> listResult = new ArrayList<JecnRefIndicatorsT>();
        for (JecnRefIndicatorsT jecnRefIndicatorsT : listJecnRefIndicatorsT) {
            if (figureId.equals(jecnRefIndicatorsT.getActivityId())) {
                listResult.add(jecnRefIndicatorsT);
            }
        }
        return listResult;
    }

    /**
     * 保存流程
     */
    @Override
    public void saveProcess(ProcessData processData, byte[] flowImgeBytes, Long updatePeopleId) throws Exception {
        if (processData.getJecnFlowStructureT() == null || processData.getJecnFlowStructureT().getFlowId() == null) {
            // 流程不存在
            return;
        }
        Long flowId = processData.getJecnFlowStructureT().getFlowId();
        try {
            // 保存流程图
            if (processData.isSave()) {
                processData.getJecnFlowStructureT().setUpdateDate(new Date());
            }
            processData.getJecnFlowStructureT().setUpdatePeopleId(updatePeopleId);
            // 图片路径
            String imagePath = processData.getJecnFlowStructureT().getFilePath();
            // 分页图片文件夹路径
            String pagingPath = JecnCommon.getSavePorcessImagePath(imagePath);
            try {
                JecnFinal.deleteFile(imagePath);
                JecnFinal.doDelete(pagingPath);
            } catch (Exception e) {
                log.error("文件删除失败！path=" + imagePath, e);
            }
            // 保存流程图
            String filePath = JecnFinal.getImagePathFileAndSave(flowImgeBytes);
            processData.getJecnFlowStructureT().setFilePath(filePath);

            // 保存分页图片
            if (processData.getPagingImageList() != null && processData.getPagingImageList().size() > 0) {
                JecnFinal.savePagingImage(processData.getPagingImageList(), JecnCommon.getPorcessImagePath(filePath));
            }

            this.flowStructureDao.update(processData.getJecnFlowStructureT());
            flowStructureDao.getSession().update(processData.getJecnFlowBasicInfoT());
            // 添加日志
            JecnUtil.saveJecnJournal(processData.getJecnFlowStructureT().getFlowId(), processData
                    .getJecnFlowStructureT().getFlowName(), 1, 2, updatePeopleId, flowStructureDao, 1);

            // 获取所有元素
            Map<String, Long> figureUUIDMap = getFigureUUIDMaps(flowId);
            /** 更新的活动 */
            if (processData.getActivityUpdateList() != null && processData.getActivityUpdateList().size() > 0) {

                // 保存前,流程中活动所有的输出
                List<JecnModeFileT> listJecnModeFileT = flowStructureDao.findJecnModeFileTByFlowId(flowId);

                // 保存前,流程中活动所有的样例
                List<JecnTempletT> listJecnTempletT = flowStructureDao.findJecnTempletTByFlowId(flowId);

                // 保存前,流程中活动所有的操作规范和输入
                List<JecnActivityFileT> listJecnActivityFileT = flowStructureDao.findJecnActivityFileTByFlowId(flowId);

                // 保存前,流程中活动所有的指标
                List<JecnRefIndicatorsT> listJecnRefIndicatorsT = flowStructureDao
                        .findAllJecnRefIndicatorsTsByFlowIdT(flowId);

                // 保存前，流程中说有活动线上信息集合
                List<JecnActiveOnLineTBean> dbOnLineTBeanList = this.flowStructureDao.findActiveOnLineTBeans(flowId,
                        false);
                // 保存前活动关联标准集合
                List<JecnActiveStandardBeanT> dbStandardList = this.flowStructureDao.findActiveStandardBeanTs(flowId);
                // 活动关联控制点集合
                List<JecnControlPointT> dbConPointList = this.flowStructureDao.findControlPointTs(flowId);

                // 新版 输入输出 集合
                // 新版 活动输入输出
                List<JecnFigureInoutT> dbFigureInList = new ArrayList<JecnFigureInoutT>();
                List<JecnFigureInoutT> dbFigureOutList = new ArrayList<JecnFigureInoutT>();

                getFigureInOutList(flowId, dbFigureInList, dbFigureOutList);

                // 待更新的活动需要删除输出
                List<Long> listActiveOutIdsDelete = new ArrayList<Long>();

                // 待更新的活动需要删除输出的样例
                List<Long> listActiveOutTempletIdsDelete = new ArrayList<Long>();

                // 待更新的活动需要删除的输入和操作规范
                List<Long> listActiveInputAndOperationIdsDelete = new ArrayList<Long>();

                // 待更新的活动需要删除的指标
                List<Long> listActiveIndicationsIdsDelete = new ArrayList<Long>();

                // 待删除的活动相关线上信息
                Set<String> deleteOnLineList = new HashSet<String>();
                // 待删除的活动相关标准或条款信息集合
                Set<String> deleteStandardList = new HashSet<String>();
                // 待删除的活动的相关控制点
                Set<String> deleteConPointList = new HashSet<String>();

                for (ActivityUpdateData activityUpdateData : processData.getActivityUpdateList()) {
                    if (activityUpdateData.getJecnFlowStructureImageT() != null) {
                        Long figureId = figureUUIDMap.get(activityUpdateData.getJecnFlowStructureImageT().getUUID());
                        // 获取主键
                        activityUpdateData.getJecnFlowStructureImageT().setFigureId(figureId);
                        this.flowStructureImageTDao.update(activityUpdateData.getJecnFlowStructureImageT());
                        // session同步 大字段处理
                        flowStructureImageTDao.getSession().flush();
                    }
                    Long figureId = figureUUIDMap.get(activityUpdateData.getFigureUUID());

                    // 三期需求，活动新增属性更新
                    updateFlowActivePart(activityUpdateData, flowId, dbOnLineTBeanList, dbStandardList, dbConPointList,
                            deleteOnLineList, deleteStandardList, deleteConPointList, figureId);

                    // --活动输出 start--
                    // 保存前 此活动的输出
                    List<JecnModeFileT> listActiveOut = this.getJecnModeFileTByFigureId(figureId, listJecnModeFileT);

                    if (activityUpdateData.getListModeFileT() != null) {
                        for (JecnModeFileT jecnModeFileT : activityUpdateData.getListModeFileT()) {
                            if (jecnModeFileT.getModeFileId() != null) {
                                // 是否需要更新
                                boolean isNeedUpdate = true;
                                for (JecnModeFileT jecnModeFileTOld : listActiveOut) {
                                    if (jecnModeFileT.getUUID().equals(jecnModeFileTOld.getUUID())) {
                                        if (jecnModeFileT.getFigureUUID().equals(jecnModeFileTOld.getFigureUUID())
                                                && jecnModeFileT.getFileMId().equals(jecnModeFileTOld.getFileMId())) {
                                            isNeedUpdate = false;
                                        }
                                        listActiveOut.remove(jecnModeFileTOld);
                                        break;
                                    }
                                }
                                if (isNeedUpdate) {
                                    this.flowStructureDao.getSession().merge(jecnModeFileT);
                                }

                                if (jecnModeFileT.getListJecnTempletT() != null
                                        && jecnModeFileT.getListJecnTempletT().size() > 0) {
                                    // 保存前 此活动的输出的样例
                                    List<JecnTempletT> listActiveOutTemplet = this.getJecnTempletTByFigureModeId(
                                            jecnModeFileT.getModeFileId(), listJecnTempletT);
                                    for (JecnTempletT jecnTempletT : jecnModeFileT.getListJecnTempletT()) {
                                        if (jecnTempletT.getUUID() != null) {
                                            // 是否需要更新
                                            isNeedUpdate = true;
                                            for (JecnTempletT jecnTempletTOld : listActiveOutTemplet) {
                                                if (jecnTempletT.getUUID().equals(jecnTempletTOld.getUUID())) {
                                                    if (jecnTempletT.getModeUUID()
                                                            .equals(jecnTempletTOld.getModeUUID())
                                                            && jecnTempletT.getFileId().equals(
                                                            jecnTempletTOld.getFileId())) {
                                                        isNeedUpdate = false;
                                                    }
                                                    listActiveOutTemplet.remove(jecnTempletTOld);
                                                    break;
                                                }
                                            }
                                            if (isNeedUpdate) {
                                                this.flowStructureDao.getSession().merge(jecnTempletT);
                                            }
                                        } else {
                                            jecnTempletT.setModeFileId(jecnModeFileT.getModeFileId());
                                            this.flowStructureDao.getSession().save(jecnTempletT);
                                        }
                                    }
                                    // 需要删除的样例
                                    for (JecnTempletT jecnTempletT : listActiveOutTemplet) {
                                        listActiveOutTempletIdsDelete.add(jecnTempletT.getId());
                                    }
                                }
                            } else {
                                this.flowStructureDao.getSession().save(jecnModeFileT);
                                if (jecnModeFileT.getListJecnTempletT() != null
                                        && jecnModeFileT.getListJecnTempletT().size() > 0) {
                                    for (JecnTempletT jecnTempletT : jecnModeFileT.getListJecnTempletT()) {
                                        jecnTempletT.setModeFileId(jecnModeFileT.getModeFileId());
                                        jecnTempletT.setModeUUID(jecnModeFileT.getUUID());
                                        this.flowStructureDao.getSession().save(jecnTempletT);
                                    }
                                }
                            }

                        }
                    }

                    // 更新新版活动的输入和输出，包含增删改
                    updateFigureInout(activityUpdateData, dbFigureInList, dbFigureOutList, figureId, flowId);

                    // 需要删除的输出
                    for (JecnModeFileT jecnModeFileT : listActiveOut) {
                        listActiveOutIdsDelete.add(jecnModeFileT.getModeFileId());
                    }
                    // --活动输出 end--

                    // --活动的输入和操作规范 start--
                    // 保存前 此活动的输入和操作规范
                    List<JecnActivityFileT> listActiveInputAndOperation = this.getJecnActivityFileTByFigureId(figureId,
                            listJecnActivityFileT);

                    if (activityUpdateData.getListJecnActivityFileT() != null) {
                        for (JecnActivityFileT jecnActivityFileT : activityUpdateData.getListJecnActivityFileT()) {
                            if (jecnActivityFileT.getFileId() == null) {
                                this.flowStructureDao.getSession().save(jecnActivityFileT);
                            } else {
                                // 是否需要更新
                                boolean isNeedUpdate = true;
                                for (JecnActivityFileT jecnActivityFileTOld : listActiveInputAndOperation) {
                                    if (jecnActivityFileT.getUUID().equals(jecnActivityFileTOld.getUUID())) {
                                        if (jecnActivityFileT.getFigureUUID().equals(
                                                jecnActivityFileTOld.getFigureUUID())
                                                && jecnActivityFileT.getFileSId().equals(
                                                jecnActivityFileTOld.getFileSId())
                                                && jecnActivityFileT.getFileType().equals(
                                                jecnActivityFileTOld.getFileType())) {
                                            isNeedUpdate = false;
                                        }
                                        listActiveInputAndOperation.remove(jecnActivityFileTOld);
                                        break;
                                    }
                                }
                                if (isNeedUpdate) {
                                    this.flowStructureDao.getSession().merge(jecnActivityFileT);
                                }
                            }

                        }
                    }
                    // 需要删除的输入和操作规范
                    for (JecnActivityFileT jecnActivityFileT : listActiveInputAndOperation) {
                        listActiveInputAndOperationIdsDelete.add(jecnActivityFileT.getFileId());
                    }
                    // --活动的输入和操作规范 end--

                    // --活动的指标 start--
                    // 保存前 此活动的指标
                    List<JecnRefIndicatorsT> listActiveIndications = this.getJecnJecnRefIndicatorsTByFigureId(figureId,
                            listJecnRefIndicatorsT);

                    if (activityUpdateData.getListRefIndicatorsT() != null) {
                        for (JecnRefIndicatorsT jecnRefIndicatorsT : activityUpdateData.getListRefIndicatorsT()) {
                            if (jecnRefIndicatorsT.getId() != null) {
                                // 是否需要更新
                                boolean isNeedUpdate = true;
                                for (JecnRefIndicatorsT jecnRefIndicatorsTOld : listActiveIndications) {
                                    if (jecnRefIndicatorsT.getId().equals(jecnRefIndicatorsTOld.getId())) {
                                        if (jecnRefIndicatorsT.getFigureUUID().equals(
                                                jecnRefIndicatorsTOld.getFigureUUID())
                                                && jecnRefIndicatorsT.getIndicatorName().equals(
                                                jecnRefIndicatorsTOld.getIndicatorName())
                                                && jecnRefIndicatorsT.getIndicatorValue().equals(
                                                jecnRefIndicatorsTOld.getIndicatorValue())) {
                                            isNeedUpdate = false;
                                        }
                                        listActiveIndications.remove(jecnRefIndicatorsTOld);
                                        break;
                                    }
                                }
                                if (isNeedUpdate) {
                                    this.flowStructureDao.getSession().merge(jecnRefIndicatorsT);
                                }

                            } else {
                                this.flowStructureDao.getSession().save(jecnRefIndicatorsT);
                            }
                        }
                    }
                    // 需要删除的指标
                    for (JecnRefIndicatorsT jecnRefIndicatorsT : listActiveIndications) {
                        listActiveIndicationsIdsDelete.add(jecnRefIndicatorsT.getId());
                    }
                    // --活动的指标 end--
                }

                // 删除样例
                deleteInoutSampleT(dbFigureInList);
                deleteInoutSampleT(dbFigureOutList);

                deleteInoutT(dbFigureInList);
                deleteInoutT(dbFigureOutList);

                // 删除更新活动中多余的数据
                // 删除更新活动中更新输出文件中删除的样例
                if (listActiveOutTempletIdsDelete.size() > 0) {
                    // 样例
                    String hql = "delete from JecnTempletT where id in";
                    List<String> listSql = JecnCommonSql.getListSqls(hql, listActiveOutTempletIdsDelete);
                    for (String sql : listSql) {
                        this.flowStructureDao.execteHql(sql);
                    }
                }

                // 删除更新活动中删除的输出文件
                if (listActiveOutIdsDelete.size() > 0) {
                    // 样例
                    String hql = "delete from JecnTempletT where modeFileId in";
                    List<String> listSql = JecnCommonSql.getListSqls(hql, listActiveOutIdsDelete);
                    for (String sql : listSql) {
                        this.flowStructureDao.execteHql(sql);
                    }
                    // 输出
                    hql = "delete from JecnModeFileT where modeFileId in";
                    listSql = JecnCommonSql.getListSqls(hql, listActiveOutIdsDelete);
                    for (String sql : listSql) {
                        this.flowStructureDao.execteHql(sql);
                    }

                }

                // 删除更新活动中删除的操作规范和输入文件
                if (listActiveInputAndOperationIdsDelete.size() > 0) {
                    // 输入和操作规范
                    String hql = "delete from JecnActivityFileT where fileId in";
                    List<String> listSql = JecnCommonSql.getListSqls(hql, listActiveInputAndOperationIdsDelete);
                    for (String sql : listSql) {
                        this.flowStructureDao.execteHql(sql);
                    }
                }

                // 删除更新活动中删除的指标
                if (listActiveIndicationsIdsDelete.size() > 0) {
                    // 输入和操作规范
                    String hql = "delete from JecnRefIndicatorsT where id in";
                    List<String> listSql = JecnCommonSql.getListSqls(hql, listActiveIndicationsIdsDelete);
                    for (String sql : listSql) {
                        this.flowStructureDao.execteHql(sql);
                    }
                }

                // 删除活动上线集合
                if (deleteOnLineList.size() > 0) {
                    String sql = "DELETE FROM JECN_ACTIVE_ONLINE_T WHERE ID IN ";
                    List<String> listSql = JecnCommonSql.getStringSqlsBySet(sql, deleteOnLineList);
                    for (String str : listSql) {
                        this.flowStructureDao.execteNative(str);
                    }
                }
                if (deleteStandardList.size() > 0) {
                    String sql = "DELETE FROM JECN_ACTIVE_STANDARD_T WHERE ID IN ";
                    List<String> listSql = JecnCommonSql.getStringSqlsBySet(sql, deleteStandardList);
                    for (String str : listSql) {
                        this.flowStructureDao.execteNative(str);
                    }
                }
                if (deleteConPointList.size() > 0) {
                    String sql = "DELETE FROM JECN_CONTROL_POINT_T WHERE ID IN ";
                    List<String> listSql = JecnCommonSql.getStringSqlsBySet(sql, deleteConPointList);
                    for (String str : listSql) {
                        this.flowStructureDao.execteNative(str);
                    }
                }
            }
            /** 新增活动 */
            if (processData.getActivitySaveList() != null) {
                for (ActivitySaveData activitySaveData : processData.getActivitySaveList()) {
                    // 活动基本对象
                    this.flowStructureImageTDao.save(activitySaveData.getJecnFlowStructureImageT());
                    // session同步 大字段处理
                    flowStructureImageTDao.getSession().flush();

                    figureUUIDMap.put(activitySaveData.getJecnFlowStructureImageT().getUUID(), activitySaveData
                            .getJecnFlowStructureImageT().getFigureId());
                    // 活动的输出
                    if (activitySaveData.getListModeFileT() != null) {
                        for (JecnModeFileT jecnModeFileT : activitySaveData.getListModeFileT()) {
                            jecnModeFileT.setFigureId(activitySaveData.getJecnFlowStructureImageT().getFigureId());
                            this.flowStructureDao.getSession().save(jecnModeFileT);
                            if (jecnModeFileT.getListJecnTempletT() != null) {
                                for (JecnTempletT jecnTempletT : jecnModeFileT.getListJecnTempletT()) {
                                    jecnTempletT.setModeFileId(jecnModeFileT.getModeFileId());
                                    this.flowStructureDao.getSession().save(jecnTempletT);
                                }
                            }
                        }
                    }
                    // 活动的输入和操作规范
                    if (activitySaveData.getListJecnActivityFileT() != null) {
                        for (JecnActivityFileT jecnActivityFileT : activitySaveData.getListJecnActivityFileT()) {
                            jecnActivityFileT.setFigureId(activitySaveData.getJecnFlowStructureImageT().getFigureId());
                            this.flowStructureDao.getSession().save(jecnActivityFileT);
                        }
                    }

                    // 新版活动输入、输出
                    if (activitySaveData.getListFigureInTs() != null) {
                        for (JecnFigureInoutT figureInT : activitySaveData.getListFigureInTs()) {
                            figureInT.setFigureId(activitySaveData.getJecnFlowStructureImageT().getFigureId());
                            // figureInT.setId(JecnCommon.getUUID());
                            figureInT.setFlowId(flowId);
                            this.flowStructureDao.getSession().save(figureInT);
                            // 更新样例
                            saveOutSample(figureInT);
                        }
                    }
                    if (activitySaveData.getListFigureOutTs() != null) {
                        for (JecnFigureInoutT figureOutT : activitySaveData.getListFigureOutTs()) {
                            figureOutT.setFigureId(activitySaveData.getJecnFlowStructureImageT().getFigureId());
                            // figureOutT.setId(JecnCommon.getUUID());
                            figureOutT.setFlowId(flowId);
                            this.flowStructureDao.getSession().save(figureOutT);
                            // 更新样例
                            saveOutSample(figureOutT);
                        }
                    }
                    // 指标
                    if (activitySaveData.getListRefIndicatorsT() != null) {
                        for (JecnRefIndicatorsT jecnRefIndicatorsT : activitySaveData.getListRefIndicatorsT()) {
                            jecnRefIndicatorsT.setActivityId(activitySaveData.getJecnFlowStructureImageT()
                                    .getFigureId());
                            this.flowStructureDao.getSession().save(jecnRefIndicatorsT);
                        }
                    }

                    // 三期新增活动信息

                    // 线上信息
                    if (activitySaveData.getListJecnActiveOnLineTBean() != null) {
                        for (JecnActiveOnLineTBean onLineTBean : activitySaveData.getListJecnActiveOnLineTBean()) {
                            // 活动主键ID
                            onLineTBean.setActiveId(activitySaveData.getJecnFlowStructureImageT().getFigureId());
                            // onLineTBean.setId(JecnCommon.getUUID());
                            this.flowStructureDao.getSession().save(onLineTBean);
                        }
                    }
                    // 活动相关标准
                    if (activitySaveData.getListJecnActiveStandardBeanT() != null) {
                        for (JecnActiveStandardBeanT standardBeanT : activitySaveData.getListJecnActiveStandardBeanT()) {
                            standardBeanT.setActiveId(activitySaveData.getJecnFlowStructureImageT().getFigureId());
                            // standardBeanT.setId(JecnCommon.getUUID());
                            this.flowStructureDao.getSession().save(standardBeanT);
                        }
                    }

                    // 活动关联的控制点由一对一 修改为一个活动对应多个控制点 C
                    if (activitySaveData.getListControlPoint() != null) { // 控制点
                        for (JecnControlPointT controlPointT : activitySaveData.getListControlPoint()) {
                            // 获取控制点的关联活动ID
                            controlPointT.setActiveId(activitySaveData.getJecnFlowStructureImageT().getFigureId());
                            // controlPointT.setId(JecnCommon.getUUID());
                            this.flowStructureDao.getSession().save(controlPointT);
                        }
                    }
                }
            }
            /** 活动的删除 */
            List<Long> deleteActiveFigureIds = getDeleteFigureIdsByUUID(figureUUIDMap, processData
                    .getActiviteDeleteList());
            if (processData.getActiviteDeleteList() != null && processData.getActiviteDeleteList().size() > 0) {
                // 输出-样例
                String hql = "delete from JecnTempletT where modeFileId in (select modeFileId from JecnModeFileT where figureId in";
                List<String> listSql = JecnCommonSql.getListSqlAddBracket(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }
                // 输出
                hql = "delete from JecnModeFileT where figureId in";
                listSql = JecnCommonSql.getListSqls(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }
                // 输入和操作规范
                hql = "delete from JecnActivityFileT where figureId in";
                listSql = JecnCommonSql.getListSqls(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }
                // 指标
                hql = "delete from JecnRefIndicatorsT where activityId in";
                listSql = JecnCommonSql.getListSqls(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }
                // 删除线上信息
                hql = "delete from JecnActiveOnLineTBean where activeId in";
                listSql = JecnCommonSql.getListSqls(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }
                // 相关标准
                hql = "delete from JecnActiveStandardBeanT where activeId in";
                listSql = JecnCommonSql.getListSqls(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }
                // 相关控制点
                hql = "delete from JecnControlPointT where activeId in";
                listSql = JecnCommonSql.getListSqls(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }

                // 新版的 输入输出 删除活动对应样例、输入输出
                hql = "delete from JecnFigureInoutSampleT where inoutId in (select id from JecnFigureInoutT where figureId in";
                listSql = JecnCommonSql.getListSqlAddBracket(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }

                hql = "delete from JecnFigureInoutT where figureId in ";
                listSql = JecnCommonSql.getListSqlAddBracket(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }

                // 活动
                hql = "delete from JecnFlowStructureImageT where figureId in";
                listSql = JecnCommonSql.getListSqls(hql, deleteActiveFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }
            }

            /** 角色的新增 */
            if (processData.getRoleSaveList() != null) {
                for (RoleFigureData roleFigureData : processData.getRoleSaveList()) {
                    this.flowStructureImageTDao.save(roleFigureData.getJecnFlowStructureImageT());
                    figureUUIDMap.put(roleFigureData.getJecnFlowStructureImageT().getUUID(), roleFigureData
                            .getJecnFlowStructureImageT().getFigureId());
                    // 角色-角色与岗位的关系-添加
                    if (roleFigureData.getListRolePositionInsert() != null) {
                        for (JecnFlowStationT jecnFlowStationT : roleFigureData.getListRolePositionInsert()) {
                            jecnFlowStationT.setFigureFlowId(roleFigureData.getJecnFlowStructureImageT().getFigureId());
                            this.flowStructureDao.getSession().save(jecnFlowStationT);
                        }
                    }
                    // 角色-角色与岗位的关系-删除
                    if (roleFigureData.getListRolePositionDelete() != null
                            && roleFigureData.getListRolePositionDelete().size() > 0) {
                        String hql = "delete from JecnFlowStationT where flowStationId in";
                        Set<String> sets = new HashSet<String>();
                        sets.addAll(roleFigureData.getListRolePositionDelete());
                        List<String> listSql = JecnCommonSql.getStringSqlsBySet(hql, sets);
                        for (String sql : listSql) {
                            this.flowStructureDao.execteHql(sql);
                        }
                    }
                }
            }
            /** 角色的更新 */
            if (processData.getRoleUpdateList() != null) {
                for (RoleFigureData roleFigureData : processData.getRoleUpdateList()) {
                    if (roleFigureData.getJecnFlowStructureImageT() != null) {
                        // 获取主键
                        roleFigureData.getJecnFlowStructureImageT().setFigureId(
                                figureUUIDMap.get(roleFigureData.getJecnFlowStructureImageT().getUUID()));
                        this.flowStructureImageTDao.update(roleFigureData.getJecnFlowStructureImageT());
                    }
                    // 角色-角色与岗位的关系-添加
                    if (roleFigureData.getListRolePositionInsert() != null) {
                        for (JecnFlowStationT jecnFlowStationT : roleFigureData.getListRolePositionInsert()) {
                            this.flowStructureDao.getSession().save(jecnFlowStationT);
                        }
                    }
                    // 角色-角色与岗位的关系-删除
                    if (roleFigureData.getListRolePositionDelete() != null
                            && roleFigureData.getListRolePositionDelete().size() > 0) {
                        String hql = "delete from JecnFlowStationT where UUID in";
                        Set<String> sets = new HashSet<String>();
                        sets.addAll(roleFigureData.getListRolePositionDelete());
                        List<String> listSql = JecnCommonSql.getStringSqlsBySet(hql, sets);
                        for (String sql : listSql) {
                            this.flowStructureDao.execteHql(sql);
                        }
                    }
                }
            }
            /** 角色的删除 */
            List<Long> deleteRoleFigureIds = getDeleteFigureIdsByUUID(figureUUIDMap, processData.getRoleDeleteList());
            if (deleteRoleFigureIds != null && deleteRoleFigureIds.size() > 0) {
                String hql = "delete from JecnFlowStationT where figureFlowId in";
                List<String> listSql = JecnCommonSql.getListSqls(hql, deleteRoleFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }

                // 删除角色活动关联
                hql = "delete from JecnRoleActiveT where figureRoleId in ";
                listSql = JecnCommonSql.getListSqls(hql, deleteRoleFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }
                // 角色
                hql = "delete from JecnFlowStructureImageT where figureId in";
                listSql = JecnCommonSql.getListSqls(hql, deleteRoleFigureIds);
                for (String sql : listSql) {
                    this.flowStructureDao.execteHql(sql);
                }
            }

            // 文档元素添加
            List<JecnFigureFileTBean> listJecnFigureFileTBean = saveFileImageFigures(processData, figureUUIDMap);

            /** 图形-新增 */
            if (processData.getSaveList() != null) {
                // 需要保存
                List<JecnFlowStructureImageT> relateLinesList = new ArrayList<JecnFlowStructureImageT>();
                for (JecnFlowStructureImageT jecnFlowStructureImageT : processData.getSaveList()) {

                    if (JecnUtil.isRelateLine(jecnFlowStructureImageT.getFigureType())) {
                        relateLinesList.add(jecnFlowStructureImageT);
                    } else {
                        this.flowStructureImageTDao.save(jecnFlowStructureImageT);
                        figureUUIDMap.put(jecnFlowStructureImageT.getUUID(), jecnFlowStructureImageT.getFigureId());
                    }
                }
                // 保存连接线
                for (JecnFlowStructureImageT jecnFlowStructureImageT : relateLinesList) {
                    if (jecnFlowStructureImageT.getStartFigure() == null
                            || jecnFlowStructureImageT.getStartFigure().intValue() == -1) {
                        jecnFlowStructureImageT.setStartFigure(figureUUIDMap.get(jecnFlowStructureImageT
                                .getStartFigureUUID()));
                    }
                    if (jecnFlowStructureImageT.getEndFigure() == null
                            || jecnFlowStructureImageT.getEndFigure().intValue() == -1) {
                        jecnFlowStructureImageT.setEndFigure(figureUUIDMap.get(jecnFlowStructureImageT
                                .getEndFigureUUID()));
                    }
                    flowStructureImageTDao.save(jecnFlowStructureImageT);
                    if (jecnFlowStructureImageT.getListLineSegmet() != null) {
                        for (JecnLineSegmentT jecnLineSegmentT : jecnFlowStructureImageT.getListLineSegmet()) {
                            jecnLineSegmentT.setFigureId(jecnFlowStructureImageT.getFigureId());
                            jecnLineSegmentT.setFigureUUID(jecnFlowStructureImageT.getUUID());
                            lineSegmentTDao.save(jecnLineSegmentT);
                        }
                    }
                }
            }

            // 需要保存的
            List<List<JecnLineSegmentT>> listSave = new ArrayList<List<JecnLineSegmentT>>();
            /** 图形-更新 */
            if (processData.getUpdateList() != null) {
                for (JecnFlowStructureImageT jecnFlowStructureImageT : processData.getUpdateList()) {
                    // 获取主键
                    jecnFlowStructureImageT.setFigureId(figureUUIDMap.get(jecnFlowStructureImageT.getUUID()));
                    // 更新图形相关连接线
                    updateFigureRelateLine(jecnFlowStructureImageT, figureUUIDMap, processData, listSave);
                    this.flowStructureImageTDao.update(jecnFlowStructureImageT);
                }
            }

            /** 删除线 */
            List<Long> deleteLineIds = getDeleteFigureIdsByUUID(figureUUIDMap, processData.getDeleteListLine());
            if (deleteLineIds != null && deleteLineIds.size() > 0) {
                String hql = "delete from JecnLineSegmentT where figureId in";
                List<String> listHql = JecnCommonSql.getListSqls(hql, deleteLineIds);
                for (String str : listHql) {
                    flowStructureDao.execteHql(str);
                }
            }

            /** 删除图形 */
            List<Long> deleteFigureIds = getDeleteFigureIdsByUUID(figureUUIDMap, processData.getDeleteList());
            if (deleteFigureIds != null && deleteFigureIds.size() > 0) {
                String hql = "delete from JecnFlowStructureImageT where figureId in";
                List<String> listHql = JecnCommonSql.getListSqls(hql, deleteFigureIds);
                for (String str : listHql) {
                    flowStructureDao.execteHql(str);
                }

                // 删除元素相关附件
                deleteFigureFile(deleteFigureIds);
            }

            // 保存线的线段
            for (List<JecnLineSegmentT> list : listSave) {
                for (JecnLineSegmentT jecnLineSegmentT : list) {
                    lineSegmentTDao.save(jecnLineSegmentT);
                }
            }
            // 活动和角色关联关系
            saveRoleRefActive(processData, figureUUIDMap);
            // 更新文档框元素处理
            updateFileImageFigures(processData, figureUUIDMap, listSave, listJecnFigureFileTBean, figureUUIDMap);
            // 保存流程图附件信息
            editFlowFigureFileTBean(listJecnFigureFileTBean, processData.getJecnFlowStructureT().getFlowId());

            // 保存to from 和活动的关联关系
            saveToAndFromRelated(processData, flowId, figureUUIDMap);

            // resetEntryInoutRelated(processData, flowId);

        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * 关键词和活动输入输出的关系
     *
     * @param processData
     * @param flowId
     */
    private void resetEntryInoutRelated(ProcessData processData, Long flowId) {
        String sql = "delete from JECN_ENTRY_FIGURE_T where flow_id=?";
        this.flowStructureDao.execteNative(sql, flowId);

        String hql = "from JecnFigureInoutT where flowId=?";
        List<JecnFigureInoutT> inouts = this.flowStructureDao.listHql(hql, flowId);
        Session s = this.flowStructureDao.getSession();
        for (JecnFigureInoutT inout : inouts) {
            String name = inout.getName();
            if (StringUtils.isBlank(name)) {
                continue;
            }
            JecnEntry save = JecnContants.ENTRY_MANAGER.saveElseGet(name.trim());
            // 建立关联关系
            JecnEntryFigureT ef = new JecnEntryFigureT();
            ef.setId(UUID.randomUUID().toString());
            ef.setEntryId(save.getId());
            ef.setFlowId(flowId);
            ef.setFigureId(inout.getFigureId());
            ef.setInoutId(inout.getId());
            s.save(ef);
        }
    }

    private void saveRoleRefActive(ProcessData processData, Map<String, Long> map) throws Exception {
        /** 活动和角色关联关系 */
        List<JecnRoleActiveT> listJecnRoleActiveT = flowStructureDao.findAllJecnRoleActiveTByFlowIdT(processData
                .getJecnFlowStructureT().getFlowId());
        if (processData.getRoleActiveTList() != null && processData.getRoleActiveTList().size() > 0) {
            for (JecnRoleActiveT jecnRoleActiveT : processData.getRoleActiveTList()) {
                if (jecnRoleActiveT.getFigureRoleId().intValue() == -1) {
                    jecnRoleActiveT.setFigureRoleId(map.get(jecnRoleActiveT.getRoleImageId()));
                }
                if (jecnRoleActiveT.getFigureActiveId().intValue() == -1) {
                    jecnRoleActiveT.setFigureActiveId(map.get(jecnRoleActiveT.getActiveFigureId()));
                }
                boolean isExist = false;
                for (JecnRoleActiveT old : listJecnRoleActiveT) {
                    if (jecnRoleActiveT.getFigureActiveId().equals(old.getFigureActiveId())
                            && jecnRoleActiveT.getFigureRoleId().equals(old.getFigureRoleId())) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    this.flowStructureDao.getSession().save(jecnRoleActiveT);
                }
            }
            List<Long> listJecnRoleActiveTDelete = new ArrayList<Long>();
            for (JecnRoleActiveT old : listJecnRoleActiveT) {
                boolean isExist = false;
                for (JecnRoleActiveT jecnRoleActiveT : processData.getRoleActiveTList()) {
                    if (jecnRoleActiveT.getFigureActiveId().equals(old.getFigureActiveId())
                            && jecnRoleActiveT.getFigureRoleId().equals(old.getFigureRoleId())) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    listJecnRoleActiveTDelete.add(old.getAutoIncreaseId());
                }
            }
            if (listJecnRoleActiveTDelete.size() > 0) {
                String hql = "delete from JecnRoleActiveT where autoIncreaseId in";
                List<String> listHql = JecnCommonSql.getListSqls(hql, listJecnRoleActiveTDelete);
                for (String str : listHql) {
                    flowStructureDao.execteHql(str);
                }
            }
        } else {
            if (listJecnRoleActiveT.size() > 0) {
                String hql = "delete from JecnRoleActiveT where figureRoleId in (select figureId from JecnFlowStructureImageT where flowId= ?)";
                flowStructureDao.execteHql(hql, processData.getJecnFlowStructureT().getFlowId());
            }
        }
    }

    private Map<String, Long> getFigureUUIDMaps(Long flowId) {
        String sql = "SELECT UUID,FIGURE_ID FROM JECN_FLOW_STRUCTURE_IMAGE_T WHERE FLOW_ID = ?";
        List<Object[]> list = this.flowStructureDao.listNativeSql(sql, flowId);
        Map<String, Long> map = new HashMap<String, Long>();
        for (Object[] objects : list) {
            map.put(objects[0].toString(), Long.valueOf(objects[1].toString()));
        }
        return map;
    }

    /**
     * 集成关系图 - 元素UUID
     *
     * @param flowId
     * @return
     */
    private Map<String, Long> getRelatedFigureUUIDMaps(Long flowId) {
        String sql = "SELECT UUID,FIGURE_ID FROM JECN_MAP_STRUCTURE_IMAGE_T WHERE FLOW_ID = ?";
        List<Object[]> list = this.flowStructureDao.listNativeSql(sql, flowId);
        Map<String, Long> map = new HashMap<String, Long>();
        for (Object[] objects : list) {
            map.put(objects[0].toString(), Long.valueOf(objects[1].toString()));
        }
        return map;
    }

    private List<Long> getDeleteFigureIdsByUUID(Map<String, Long> figureUUIDMap, List<String> uuids) {
        List<Long> ids = new ArrayList<Long>();
        for (String uuid : uuids) {
            Long id = figureUUIDMap.get(uuid);
            if (id != null) {
                ids.add(id);
            }
        }
        return ids;
    }

    /**
     * 更新活动
     *
     * @param activityUpdateData
     * @param dbListFigureInTs
     * @param dbListFigureOutTs
     * @param figureId
     * @param flowId
     */
    private void updateFigureInout(ActivityUpdateData activityUpdateData, List<JecnFigureInoutT> dbListFigureInTs,
                                   List<JecnFigureInoutT> dbListFigureOutTs, Long figureId, Long flowId) {
        if (activityUpdateData.getListFigureInTs() != null) {
            updateFigureInoutBase(dbListFigureInTs, activityUpdateData.getListFigureInTs(), figureId, flowId);
        }
        if (activityUpdateData.getListFigureOutTs() != null) {
            updateFigureInoutBase(dbListFigureOutTs, activityUpdateData.getListFigureOutTs(), figureId, flowId);
        }
    }

    private void updateFigureInoutBase(List<JecnFigureInoutT> dbFigureInoutTs,
                                       List<JecnFigureInoutT> listFigureInoutTs, Long figureId, Long flowId) {
        for (JecnFigureInoutT figureInoutT : listFigureInoutTs) {
            if (StringUtils.isNotBlank(figureInoutT.getId())) {
                for (JecnFigureInoutT dbInoutT : dbFigureInoutTs) {
                    if (figureInoutT.getId().equals(dbInoutT.getId())) {
                        figureInoutT.setFigureId(figureId);
                        this.flowStructureDao.getSession().merge(figureInoutT);
                        // 更新样例
                        updateFigureInoutSample(flowId, figureInoutT, dbInoutT.getListSampleT());
                        dbFigureInoutTs.remove(dbInoutT);
                        break;
                    }
                }

            } else {
                figureInoutT.setFigureId(figureId);
                // figureInoutT.setId(JecnCommon.getUUID());
                figureInoutT.setFlowId(flowId);
                this.flowStructureDao.getSession().save(figureInoutT);
                saveOutSample(figureInoutT);
            }
        }
    }

    private void saveOutSample(JecnFigureInoutT figureInoutT) {
        if (figureInoutT.getListSampleT() != null && figureInoutT.getListSampleT().size() > 0) {
            for (JecnFigureInoutSampleT inoutSampleT : figureInoutT.getListSampleT()) {
                saveInOutSample(figureInoutT, inoutSampleT);
            }
        }
    }

    private void saveInOutSample(JecnFigureInoutT figureInoutT, JecnFigureInoutSampleT inoutSampleT) {
        inoutSampleT.setInoutId(figureInoutT.getId());
        inoutSampleT.setFlowId(figureInoutT.getFlowId());
        this.flowStructureDao.getSession().save(inoutSampleT);
    }

    private void updateFigureInoutSample(Long flowId, JecnFigureInoutT figureInoutT,
                                         List<JecnFigureInoutSampleT> dbFigureInout) {
        if (figureInoutT.getListSampleT() != null) {
            for (JecnFigureInoutSampleT inoutSampleT : figureInoutT.getListSampleT()) {
                inoutSampleT.setFlowId(flowId);
                if (StringUtils.isNotBlank(inoutSampleT.getId()) && dbFigureInout != null && !dbFigureInout.isEmpty()) {
                    for (JecnFigureInoutSampleT dbInoutSample : dbFigureInout) {
                        if (inoutSampleT.getId().equals(dbInoutSample.getId())) {
                            this.flowStructureDao.getSession().merge(dbInoutSample);
                            dbFigureInout.remove(dbInoutSample);
                            break;
                        }
                    }
                } else {
                    saveInOutSample(figureInoutT, inoutSampleT);
                }
            }
            // 需要删除的样例
            if (dbFigureInout == null || dbFigureInout.isEmpty()) {
                return;
            }
            for (JecnFigureInoutSampleT dbInoutSample : dbFigureInout) {
                dbInoutSample.setFlowId(flowId);
                this.flowStructureDao.getSession().delete(dbInoutSample);
            }
        }
    }

    private void deleteInoutSampleT(List<JecnFigureInoutT> dbFigureInoutList) {
        for (JecnFigureInoutT figureInoutT : dbFigureInoutList) {
            if (figureInoutT.getListSampleT() != null) {
                for (JecnFigureInoutSampleT inoutSampleT : figureInoutT.getListSampleT()) {
                    String sql = "DELETE JECN_FIGURE_IN_OUT_SAMPLE_T WHERE ID = ?";
                    flowStructureDao.execteNative(sql, inoutSampleT.getId());
                }
            }
            String sql = "DELETE JECN_FIGURE_IN_OUT_SAMPLE_T WHERE IN_OUT_ID = ?";
            flowStructureDao.execteNative(sql, figureInoutT.getId());
        }
    }

    private void deleteInoutT(List<JecnFigureInoutT> dbFigureInoutList) {
        for (JecnFigureInoutT figureInoutT : dbFigureInoutList) {
            String sql = "DELETE JECN_FIGURE_IN_OUT_T WHERE ID = ?";
            flowStructureDao.execteNative(sql, figureInoutT.getId());
        }
    }

    private void saveToAndFromRelated(ProcessData processData, Long flowId, Map<String, Long> map) throws Exception {

        /** 活动和角色关联关系 */
        List<JecnToFromActiveT> figures = flowStructureDao.findToFromRelatedActiveT(flowId);
        if (processData.getToFromActiveTList() != null && processData.getToFromActiveTList().size() > 0) {
            for (JecnToFromActiveT figure : processData.getToFromActiveTList()) {
                if (figure.getToFromFigureId().intValue() == -1) {
                    figure.setToFromFigureId(map.get(figure.getToFromFigureUUId()));
                }
                if (figure.getActiveFigureId().intValue() == -1) {
                    figure.setActiveFigureId(map.get(figure.getActiveFigureUUId()));
                }
                boolean isExist = false;
                for (JecnToFromActiveT old : figures) {
                    if (figure.getToFromFigureId().equals(old.getToFromFigureId())) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    this.flowStructureDao.getSession().save(figure);
                }
            }
            List<Long> listJecnToFromActiveTDelete = new ArrayList<Long>();
            for (JecnToFromActiveT old : figures) {
                boolean isExist = false;
                for (JecnToFromActiveT figure : processData.getToFromActiveTList()) {
                    if (figure.getToFromFigureId().equals(old.getToFromFigureId())) {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist) {
                    listJecnToFromActiveTDelete.add(old.getToFromFigureId());
                }
            }
            if (listJecnToFromActiveTDelete.size() > 0) {
                String hql = "delete from JecnToFromActiveT where toFromFigureId in";
                List<String> listHql = JecnCommonSql.getListSqls(hql, listJecnToFromActiveTDelete);
                for (String str : listHql) {
                    flowStructureDao.execteHql(str);
                }
            }
        } else {
            if (figures.size() > 0) {
                String hql = "delete from JecnToFromActiveT where flowId =?)";
                flowStructureDao.execteHql(hql, flowId);
            }
        }
    }

    private List<JecnFigureFileTBean> saveFileImageFigures(ProcessData processData, Map<String, Long> map)
            throws Exception {
        /** ****** 文档处理 ******* */
        List<JecnFigureFileTBean> listJecnFigureFileTBean = new ArrayList<JecnFigureFileTBean>();
        if (processData.getSaveFileImageDataList() != null) {// 新增文档
            for (ProcessFileImageFigureData fileImageFigureData : processData.getSaveFileImageDataList()) {
                this.flowStructureDao.getSession().save(fileImageFigureData.getJecnFlowStructureImageT());
                this.flowStructureDao.getSession().flush();
                map.put(fileImageFigureData.getJecnFlowStructureImageT().getUUID(), fileImageFigureData
                        .getJecnFlowStructureImageT().getFigureId());
                if (fileImageFigureData.getListJecnFigureFileTBean() != null) {
                    for (JecnFigureFileTBean jecnFigureFileTBean : fileImageFigureData.getListJecnFigureFileTBean()) {
                        jecnFigureFileTBean.setFigureId(fileImageFigureData.getJecnFlowStructureImageT().getFigureId());
                        jecnFigureFileTBean.setFigureUUID(fileImageFigureData.getJecnFlowStructureImageT().getUUID());
                    }
                    listJecnFigureFileTBean.addAll(fileImageFigureData.getListJecnFigureFileTBean());
                }
            }
        }
        return listJecnFigureFileTBean;
    }

    private void updateFileImageFigures(ProcessData processData, Map<String, Long> map,
                                        List<List<JecnLineSegmentT>> listSave, List<JecnFigureFileTBean> listJecnFigureFileTBean,
                                        Map<String, Long> figureUUIDMap) {
        if (processData.getUpdateFileImageDataList() != null) {
            for (ProcessFileImageFigureData fileImageFigureData : processData.getUpdateFileImageDataList()) {
                setFileImageFigureID(fileImageFigureData.getListJecnFigureFileTBean(), figureUUIDMap,
                        fileImageFigureData.getJecnFlowStructureImageT());
                listJecnFigureFileTBean.addAll(fileImageFigureData.getListJecnFigureFileTBean());

                if (fileImageFigureData.getJecnFlowStructureImageT() != null) {
                    // 获取主键
                    fileImageFigureData.getJecnFlowStructureImageT().setFigureId(
                            figureUUIDMap.get(fileImageFigureData.getJecnFlowStructureImageT().getUUID()));
                    this.flowStructureDao.getSession().update(fileImageFigureData.getJecnFlowStructureImageT());
                    this.flowStructureDao.getSession().flush();
                }
                // 更新图形相关连接线
                updateFigureRelateLine(fileImageFigureData.getJecnFlowStructureImageT(), map, processData, listSave);
            }
        }
    }

    private void setFileImageFigureID(List<JecnFigureFileTBean> listJecnFigureFileTBean,
                                      Map<String, Long> figureUUIDMap, JecnFlowStructureImageT imageT) {
        if (listJecnFigureFileTBean == null || listJecnFigureFileTBean.isEmpty()) {
            return;
        }
        Long figureId = null;
        if (imageT != null) {
            figureId = figureUUIDMap.get(imageT.getUUID());
        } else {
            figureId = figureUUIDMap.get(listJecnFigureFileTBean.get(0).getFigureUUID());
        }
        for (JecnFigureFileTBean jecnFigureFileTBean : listJecnFigureFileTBean) {
            jecnFigureFileTBean.setFigureId(figureId);
        }
    }

    /**
     * 删除图形元素相关附件（可设置连接的元素和文档元素）
     *
     * @param figureIds
     */
    private void deleteFigureFile(List<Long> figureIds) {
        String hql = "delete from JecnFigureFileTBean where figureId in ";
        List<String> listHql = JecnCommonSql.getListSqls(hql, figureIds);
        for (String str : listHql) {
            flowStructureDao.execteHql(str);
        }
    }

    /**
     * 更新图形相关连接线
     *
     * @param jecnFlowStructureImageT 当前元素
     * @param map                     记录图形的UUID和主键ID
     * @param processData             流程相关元素数据集合
     * @param listSave                需要保存的连接线
     */
    private void updateFigureRelateLine(JecnFlowStructureImageT jecnFlowStructureImageT, Map<String, Long> map,
                                        ProcessData processData, List<List<JecnLineSegmentT>> listSave) {
        if (jecnFlowStructureImageT == null) {
            return;
        }// 主键ID不存在或为-1时，表示此图形是新添加的需要使用UUID获取其对应的主键ID
        // 设计器元素data对象中的uuid是画图面板本身使用
        if (JecnUtil.isRelateLine(jecnFlowStructureImageT.getFigureType())) {
            if (jecnFlowStructureImageT.getStartFigure() == null
                    || jecnFlowStructureImageT.getStartFigure().intValue() == -1) {
                jecnFlowStructureImageT.setStartFigure(map.get(jecnFlowStructureImageT.getStartFigureUUID()));
            }
            if (jecnFlowStructureImageT.getEndFigure() == null
                    || jecnFlowStructureImageT.getEndFigure().intValue() == -1) {
                jecnFlowStructureImageT.setEndFigure(map.get(jecnFlowStructureImageT.getEndFigureUUID()));
            }
            if (jecnFlowStructureImageT.getListLineSegmet() != null
                    && jecnFlowStructureImageT.getListLineSegmet().size() > 0) {
                if (processData.getDeleteListLine() == null) {
                    processData.setDeleteListLine(new ArrayList<String>());
                }
                processData.getDeleteListLine().add(jecnFlowStructureImageT.getUUID());
                for (JecnLineSegmentT jecnLineSegmentT : jecnFlowStructureImageT.getListLineSegmet()) {
                    jecnLineSegmentT.setFigureId(jecnFlowStructureImageT.getFigureId());
                    jecnLineSegmentT.setFigureUUID(jecnFlowStructureImageT.getUUID());
                }
                listSave.add(jecnFlowStructureImageT.getListLineSegmet());
            }
        }
    }

    /**
     * 三期需求更新活动相关信息
     *
     * @param activityUpdateData 活动数据对象
     * @param flowId             流程主键ID
     * @param dbOnLineTBeanList  数据库中当前流程活动的线上信息集合
     * @param dbStandardList     数据库中当前流程活动的相关标准集合
     * @param dbConPointList     数据库中当前流程活动的相关控制点集合
     * @param deleteOnLineList   待删除线上信息集合ID
     * @param deleteStandardList
     * @param deleteConPointList
     */
    private void updateFlowActivePart(ActivityUpdateData activityUpdateData, Long flowId,
                                      List<JecnActiveOnLineTBean> dbOnLineTBeanList, List<JecnActiveStandardBeanT> dbStandardList,
                                      List<JecnControlPointT> dbConPointList, Set<String> deleteOnLineList, Set<String> deleteStandardList,
                                      Set<String> deleteConPointList, Long figureId) {
        if (activityUpdateData == null) {
            return;
        }
        // 线上信息集合
        List<JecnActiveOnLineTBean> onLineTBeanList = activityUpdateData.getListJecnActiveOnLineTBean();
        // 更新线上信息
        updateOnLineTBeanList(dbOnLineTBeanList, onLineTBeanList, deleteOnLineList, figureId);

        // 活动关联标准集合
        List<JecnActiveStandardBeanT> curStandardList = activityUpdateData.getListJecnActiveStandardBeanT();
        // 更新活动相关标准
        updateActiveStandardList(curStandardList, dbStandardList, deleteStandardList, figureId);

        // 保存时更新控制点
        // 说明：由于活动对应的控制点由一对一 改为一个活动对应多个控制点所以需要做如下修改
        // 控制点集合
        List<JecnControlPointT> listControlPoint = activityUpdateData.getListControlPoint();

        // 更新控制点
        updateControlPoint(dbConPointList, deleteConPointList, listControlPoint, figureId);

    }

    /**
     * 更新活动控制点
     *
     * @param dbConPointList     ：原先的控制点集合
     * @param deleteConPointList ：要删除的控制点集合
     * @param listControlPointT  ：新的控制点集合
     * @param figureId           ：活动编号
     * @author chehuanbo
     */
    private void updateControlPoint(List<JecnControlPointT> dbConPointList, Set<String> deleteConPointList,
                                    List<JecnControlPointT> listControlPointT, Long figureId) {

        // 没有控制点，将原先控制点删除
        if (listControlPointT == null || listControlPointT.size() == 0) {
            for (JecnControlPointT jecnControlPointT : dbConPointList) {
                if (figureId.equals(jecnControlPointT.getActiveId())) {
                    deleteConPointList.add(jecnControlPointT.getId()); // 添加到要删除的集合中
                }
            }
        } else {
            // 临时集合
            List<JecnControlPointT> temporaryList = new ArrayList<JecnControlPointT>();
            // 添加或者更新控制点
            for (JecnControlPointT jecnControlPointT : listControlPointT) {
                // 首先查到需要删除的控制点
                for (JecnControlPointT jecnControlPointTOld : dbConPointList) {
                    // 判断原先集合中的数据是否属于当前活动
                    if (figureId.equals(jecnControlPointTOld.getActiveId())) {
                        if (jecnControlPointT.getId() == null) {
                            continue;
                        } else if (!"".equals(jecnControlPointT.getId())
                                && jecnControlPointT.getId().equals(jecnControlPointTOld.getId())) {
                            temporaryList.add(jecnControlPointTOld);
                            continue;
                        }
                    }
                }
                jecnControlPointT.setActiveId(figureId);
                // 存在主键ID执行更新
                this.flowStructureDao.getSession().saveOrUpdate(jecnControlPointT);
                this.flowStructureDao.getSession().flush();
            }
            if (temporaryList != null) {
                // 将存在的控制点从临时集合中删除
                dbConPointList.removeAll(temporaryList);
                // 将多余的数据添加删除集合中
                for (JecnControlPointT jecnControlPointT3 : dbConPointList) {
                    if (figureId.equals(jecnControlPointT3.getActiveId())) {
                        deleteConPointList.add(jecnControlPointT3.getId());
                    }
                }
            }
        }
    }

    private boolean isUpdateRiskId(Long localId, Long serverId) {
        if (serverId == null && localId != null || serverId != null && localId == null) {
            return true;
        } else if (serverId == null && localId == null) {
            return false;
        } else if (serverId.equals(localId)) {
            return true;
        }
        return false;
    }

    /**
     * 活动关联标准集合
     *
     * @param curStandardList    当前活动关联标准集合
     * @param dbStandardList     数据库中当前流程所有活动的关联标准集合
     * @param deleteStandardList 删除活动关联标准
     * @param figureId
     */
    private void updateActiveStandardList(List<JecnActiveStandardBeanT> curStandardList,
                                          List<JecnActiveStandardBeanT> dbStandardList, Set<String> deleteStandardList, Long figureId) {

        // 获取当前活动对应数据库中关联标准的集合
        List<JecnActiveStandardBeanT> curDBStandardList = getActiveStandardBeanTListByDB(dbStandardList, figureId);
        // 数据库中当前活动对应的线上信息集合
        Map<String, JecnActiveStandardBeanT> map = new HashMap<String, JecnActiveStandardBeanT>();
        if (curStandardList == null || curStandardList.size() == 0) {// 不存在数据，清空数据库，记录待删除集合
            for (JecnActiveStandardBeanT jecnActiveStandardBeanT : curDBStandardList) {
                deleteStandardList.add(jecnActiveStandardBeanT.getId());
            }
            return;
        } else {
            for (JecnActiveStandardBeanT jecnActiveStandardBeanT : curStandardList) {
                jecnActiveStandardBeanT.setActiveId(figureId);
                this.flowStructureDao.getSession().saveOrUpdate(jecnActiveStandardBeanT);
                this.flowStructureDao.getSession().flush();
                map.put(jecnActiveStandardBeanT.getId(), jecnActiveStandardBeanT);
            }
        }

        for (JecnActiveStandardBeanT standardBeanT : curDBStandardList) {// 数据库中当前活动关联标准的对象在map中不存在，记录删除
            if (map.get(standardBeanT.getId()) == null) {
                deleteStandardList.add(standardBeanT.getId());
            }
        }

    }

    /**
     * 更新活动线上信息
     *
     * @param dbOnLineTBeanList 数据库当前流程对应的所有线上信息
     * @param onLineTBeanList   传入的当前活动线上信息
     * @param deleteOnLineList  获取数据库中待删除的线上信息集合
     * @param figureId          活动主键ID
     */
    private void updateOnLineTBeanList(List<JecnActiveOnLineTBean> dbOnLineTBeanList,
                                       List<JecnActiveOnLineTBean> onLineTBeanList, Set<String> deleteOnLineList, Long figureId) {

        // 数据库中当前活动对应的线上信息集合
        Map<String, JecnActiveOnLineTBean> onLineMap = new HashMap<String, JecnActiveOnLineTBean>();

        // 当前活动的线上集合
        List<JecnActiveOnLineTBean> curDBOnLineList = getAcitveOnLineListByDB(dbOnLineTBeanList, figureId);

        if (onLineTBeanList == null || onLineTBeanList.size() == 0) {// 线上集合不存在
            // 删除线上集合
            for (JecnActiveOnLineTBean jecnActiveOnLineTBean : curDBOnLineList) {
                deleteOnLineList.add(jecnActiveOnLineTBean.getId());
            }
            return;
        } else {
            for (JecnActiveOnLineTBean jecnActiveOnLineTBean : onLineTBeanList) {// 循环当前传入的线上信息集合
                jecnActiveOnLineTBean.setActiveId(figureId);
                this.flowStructureDao.getSession().saveOrUpdate(jecnActiveOnLineTBean);
                this.flowStructureDao.getSession().flush();
                onLineMap.put(jecnActiveOnLineTBean.getId(), jecnActiveOnLineTBean);
            }
        }

        for (JecnActiveOnLineTBean dbOnLineTBean : curDBOnLineList) {// 数据库中当前信息集合
            if (onLineMap.get(dbOnLineTBean.getId()) == null) {// 数据库中线上信息在当前集合中不存在
                deleteOnLineList.add(dbOnLineTBean.getId());
            }
        }
    }

    /**
     * 判断信息化更新
     *
     * @param curDBOnLineList
     * @param jecnActiveOnLineTBean
     * @return
     */
    private boolean isUpdateLine(List<JecnActiveOnLineTBean> curDBOnLineList,
                                 JecnActiveOnLineTBean jecnActiveOnLineTBean) {
        for (JecnActiveOnLineTBean old : curDBOnLineList) {
            if (old.getId().equals(jecnActiveOnLineTBean.getId())) {
                if (!old.getToolId().equals(jecnActiveOnLineTBean.getToolId())) {
                    return true;
                }
                String oldIformationDescription = old.getInformationDescription() == null ? "" : old
                        .getInformationDescription();
                String newIformationDescription = jecnActiveOnLineTBean.getInformationDescription() == null ? ""
                        : jecnActiveOnLineTBean.getInformationDescription();
                if (!oldIformationDescription.equals(newIformationDescription)) {
                    return true;
                }
                if (old.getOnLineTime() == null) {
                    if (jecnActiveOnLineTBean.getOnLineTime() != null) {
                        return true;
                    }
                } else {
                    if (jecnActiveOnLineTBean.getOnLineTime() == null) {
                        return true;
                    } else {
                        if (!old.getOnLineTime().equals(jecnActiveOnLineTBean.getOnLineTime())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * 当前活动对应的线上集合
     *
     * @param dbOnLineTBeanList
     * @param figureId
     * @return
     */
    private List<JecnActiveOnLineTBean> getAcitveOnLineListByDB(List<JecnActiveOnLineTBean> dbOnLineTBeanList,
                                                                Long figureId) {
        List<JecnActiveOnLineTBean> curDBList = new ArrayList<JecnActiveOnLineTBean>();
        if (dbOnLineTBeanList != null && dbOnLineTBeanList.size() > 0) {
            for (JecnActiveOnLineTBean activeOnLineTBean : dbOnLineTBeanList) {
                if (activeOnLineTBean.getActiveId().equals(figureId)) {
                    curDBList.add(activeOnLineTBean);
                }
            }
        }
        return curDBList;
    }

    /**
     * 当前活动对应的线上集合
     *
     * @param dbOnLineTBeanList
     * @param figureId
     * @return
     */
    private List<JecnActiveStandardBeanT> getActiveStandardBeanTListByDB(
            List<JecnActiveStandardBeanT> dbJecnActiveStandardBeanTList, Long figureId) {
        List<JecnActiveStandardBeanT> curDBList = new ArrayList<JecnActiveStandardBeanT>();
        if (dbJecnActiveStandardBeanTList != null && dbJecnActiveStandardBeanTList.size() > 0) {
            for (JecnActiveStandardBeanT standardBeanT : dbJecnActiveStandardBeanTList) {
                if (standardBeanT.getActiveId().equals(figureId)) {
                    curDBList.add(standardBeanT);
                }
            }
        }
        return curDBList;
    }

    @Override
    public JecnFlowBasicInfoT getJecnFlowBasicInfoT(Long flowId) throws Exception {
        try {
            return processBasicDao.get(flowId);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ProcessOpenMapData getProcessMapData(long id, long projectId) throws Exception {
        try {
            JecnFlowStructureT jecnFlowStructureT = this.flowStructureDao.get(id);
            if (jecnFlowStructureT == null) {
                return null;
            }
            return getProcessMapData(jecnFlowStructureT);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    private ProcessOpenMapData getProcessMapData(JecnFlowStructureT jecnFlowStructureT) throws Exception {
        ProcessOpenMapData openMapData = new ProcessOpenMapData();
        openMapData.setJecnFlowStructureT(jecnFlowStructureT);

        Long id = jecnFlowStructureT.getFlowId();
        List<ProcessMapFigureData> listMapFigureData = new ArrayList<ProcessMapFigureData>();
        // 图形
        List<JecnFlowStructureImageT> listJecnFlowStructureImageT = this.flowStructureImageTDao.findAllByFlowId(id);
        // 线段
        List<JecnLineSegmentT> listJecnLineSegmentT = this.lineSegmentTDao.findAllByFlowId(id);
        // 当前流程地图的所有附件集合
        List<JecnFigureFileTBean> listJecnFigureFileTBean = flowStructureDao.findJecnFigureFileTList(id);

        // 地图元素数据对象
        ProcessMapFigureData mapFigureData = null;
        // 元素对应的附件集合
        List<JecnFigureFileTBean> listResult = null;
        for (JecnFlowStructureImageT jecnFlowStructureImageT : listJecnFlowStructureImageT) {
            mapFigureData = new ProcessMapFigureData();
            mapFigureData.setJecnFlowStructureImageT(jecnFlowStructureImageT);
            if (JecnUtil.isRelateLine(jecnFlowStructureImageT.getFigureType())) {
                List<JecnLineSegmentT> listJecnLineSegmentTSurplus = new ArrayList<JecnLineSegmentT>();
                jecnFlowStructureImageT.setListLineSegmet(this.getlistJecnLineSegmentTSurplusByFigureId(
                        jecnFlowStructureImageT.getFigureId(), listJecnLineSegmentT, listJecnLineSegmentTSurplus));
                listJecnLineSegmentT = listJecnLineSegmentTSurplus;
            } else if (JecnCommon.isInfoFigure(jecnFlowStructureImageT.getFigureType())) {// 为地图可添加附件元素

                // 当前元素对应的附件集合
                listResult = getlistJecnFigureFileTBeanByFigureId(jecnFlowStructureImageT.getFigureId(),
                        listJecnFigureFileTBean);

                mapFigureData.setListJecnFigureFileTBean(listResult);
                // 去除当前集合
                if (listResult.size() > 0) {
                    listJecnFigureFileTBean.removeAll(listResult);
                }
            }
            listMapFigureData.add(mapFigureData);
        }
        openMapData.setMapFigureDataList(listMapFigureData);
        return openMapData;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void copyNodeDatasHandle(List<Long> copyIds, Long copyToId, int option, Long peopleId) throws Exception {
        if (copyIds == null || copyIds == null || copyIds.isEmpty()) {
            return;
        }
        // 1、 获取复制节点，及子节点集合
        String sql = "SELECT T.*   FROM JECN_FLOW_STRUCTURE_T T inner JOIN JECN_FLOW_STRUCTURE_T PT ON "
                + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("PT") + " WHERE T.DEL_STATE = 0 AND PT.FLOW_ID IN "
                + JecnCommonSql.getIds(copyIds);
        // option 1 不包含子节点 0包含子节点 默认包含
        if (option == 1) {
            sql = "SELECT T.*   FROM JECN_FLOW_STRUCTURE_T T   WHERE T.DEL_STATE = 0 AND T.FLOW_ID IN "
                    + JecnCommonSql.getIds(copyIds);
        }

        List<JecnFlowStructureT> flowStructureTs = this.flowStructureDao.getSession().createSQLQuery(sql).addEntity(
                JecnFlowStructureT.class).list();

        // 获取sortId
        JecnFlowStructureT copyToFlowT = flowStructureDao.get(copyToId);
        JecnTreeBean treeBean = new JecnTreeBean();
        treeBean.setId(copyToId);
        if (copyToFlowT == null) {
            treeBean.setTreeNodeType(TreeNodeType.processRoot);
        } else {
            treeBean.setTreeNodeType(copyToFlowT.getIsFlow() == 0 ? TreeNodeType.processMap : TreeNodeType.process);
        }
        int maxSort = treeDao.getTreeChildMaxSortId(treeBean);
        // 2、获取复制的节点集合 key:父节点ID, value : 子节点集合
        Map<Long, List<ProcessOpenData>> nodeMaps = new HashMap<Long, List<ProcessOpenData>>();
        ProcessOpenData openData = null;

        List<ProcessOpenData> copyList = new ArrayList<ProcessOpenData>();
        for (JecnFlowStructureT flowStructureT : flowStructureTs) {
            Long pId = flowStructureT.getPerFlowId();
            if (flowStructureT.getIsFlow() == 0) {// 架构
                ProcessOpenMapData mapData = getProcessMapData(flowStructureT);
                openData = new ProcessOpenData();
                JecnFlowStructureT newFlowStructureT = new JecnFlowStructureT();
                PropertyUtils.copyProperties(newFlowStructureT, mapData.getJecnFlowStructureT());
                openData.setJecnFlowStructureT(newFlowStructureT);
                openData.setProcessFigureDataList(copyMapEles(mapData.getMapFigureDataList()));
                openData.setMainFlowT(getFlowMapInfo(mapData.getJecnFlowStructureT().getFlowId()));
            } else if (flowStructureT.getIsFlow() == 1) {
                openData = getProcessData(flowStructureT);
            }
            if (!nodeMaps.containsKey(pId)) {
                nodeMaps.put(pId, new ArrayList<ProcessOpenData>());
            }
            // 添加子集
            nodeMaps.get(pId).add(openData);

            if (copyIds.contains(flowStructureT.getFlowId())) {
                maxSort++;
                openData.getJecnFlowStructureT().setSortId((long) maxSort);
                copyList.add(openData);
            }
        }
        // 另存为复制节点
        JecnCopyHandle.newInstance().recursiveSave(flowStructureDao, copyList, nodeMaps, copyToId, flowOrgDao,
                processBasicDao, peopleId);
    }

    /**
     * 流程架构数据集，转换成流程数据集
     *
     * @param mapFigureDataList
     * @return
     */
    private List<ProcessFigureData> copyMapEles(List<ProcessMapFigureData> mapFigureDataList) {
        List<ProcessFigureData> processFigureDataList = new ArrayList<ProcessFigureData>();
        ProcessFigureData figureData = null;
        for (ProcessMapFigureData mapFigureData : mapFigureDataList) {
            figureData = new ProcessFigureData();
            figureData.setJecnFlowStructureImageT(mapFigureData.getJecnFlowStructureImageT());
            figureData.setListJecnFigureFileTBean(mapFigureData.getListJecnFigureFileTBean());
            processFigureDataList.add(figureData);
        }
        return processFigureDataList;
    }

    /**
     * 打开流程架构-集成关系图
     *
     * @param id
     * @param projectId
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ProcessOpenMapData getProcessMapRelatedData(long id, long projectId) throws Exception {
        try {
            JecnFlowStructureT jecnFlowStructureT = this.flowStructureDao.get(id);
            if (jecnFlowStructureT == null) {
                return null;
            }
            return getProcessOpenMapData(jecnFlowStructureT);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    private ProcessOpenMapData getProcessOpenMapData(JecnFlowStructureT jecnFlowStructureT) throws Exception {
        Long id = jecnFlowStructureT.getFlowId();
        ProcessOpenMapData openMapData = new ProcessOpenMapData();
        openMapData.setJecnFlowStructureT(jecnFlowStructureT);

        List<ProcessMapFigureData> listMapFigureData = new ArrayList<ProcessMapFigureData>();
        // 图形
        List<JecnMapRelatedStructureImageT> listJecnFlowStructureImageT = this.flowStructureImageTDao
                .findMapRelatedStructureImageTById(id);
        // 线段
        List<JecnMapLineSegmentT> mapLines = this.lineSegmentTDao.findMapRelatedLineById(id);
        List<JecnLineSegmentT> listJecnLineSegmentT = new ArrayList<JecnLineSegmentT>();
        JecnLineSegmentT lineSegmentT = null;
        for (JecnMapLineSegmentT jecnMapLineSegmentT : mapLines) {
            lineSegmentT = new JecnLineSegmentT();
            // 复制属性
            PropertyUtils.copyProperties(lineSegmentT, jecnMapLineSegmentT);
            listJecnLineSegmentT.add(lineSegmentT);
        }

        // 当前流程地图的所有附件集合
        // List<JecnFigureFileTBean> listJecnFigureFileTBean =
        // flowStructureDao.findJecnFigureFileTList(id);

        // 地图元素数据对象
        ProcessMapFigureData mapFigureData = null;
        // 元素对应的附件集合
        // List<JecnFigureFileTBean> listResult = null;
        JecnFlowStructureImageT flowStructureImageT = null;
        for (JecnMapRelatedStructureImageT relatedStructureImageT : listJecnFlowStructureImageT) {
            mapFigureData = new ProcessMapFigureData();
            flowStructureImageT = new JecnFlowStructureImageT();
            // 复制属性
            PropertyUtils.copyProperties(flowStructureImageT, relatedStructureImageT);
            mapFigureData.setJecnFlowStructureImageT(flowStructureImageT);
            if (JecnUtil.isRelateLine(relatedStructureImageT.getFigureType())) {
                List<JecnLineSegmentT> listJecnLineSegmentTSurplus = new ArrayList<JecnLineSegmentT>();
                flowStructureImageT.setListLineSegmet(this.getlistJecnLineSegmentTSurplusByFigureId(
                        relatedStructureImageT.getFigureId(), listJecnLineSegmentT, listJecnLineSegmentTSurplus));
                listJecnLineSegmentT = listJecnLineSegmentTSurplus;
            } else if (JecnCommon.isInfoFigure(relatedStructureImageT.getFigureType())) {// 为地图可添加附件元素

                // 当前元素对应的附件集合
                // listResult =
                // getlistJecnFigureFileTBeanByFigureId(jecnFlowStructureImageT.getFigureId(),
                // listJecnFigureFileTBean);

                // mapFigureData.setListJecnFigureFileTBean(listResult);
                // 去除当前集合
                // if (listResult.size() > 0) {
                // listJecnFigureFileTBean.removeAll(listResult);
                // }
            }
            listMapFigureData.add(mapFigureData);
        }
        openMapData.setMapFigureDataList(listMapFigureData);
        return openMapData;
    }

    /**
     * 获取当前元素ID对应的附件集合
     *
     * @param figureId
     * @param listJecnFigureFileTBean
     * @return
     */
    private List<JecnFigureFileTBean> getlistJecnFigureFileTBeanByFigureId(Long figureId,
                                                                           List<JecnFigureFileTBean> listJecnFigureFileTBean) {
        List<JecnFigureFileTBean> listResult = new ArrayList<JecnFigureFileTBean>();
        for (JecnFigureFileTBean figureFileTBean : listJecnFigureFileTBean) {
            if (figureId.equals(figureFileTBean.getFigureId())) {
                listResult.add(figureFileTBean);
            }
        }
        return listResult;
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ProcessOpenData getProcessData(long id, long projectId) throws Exception {
        try {
            JecnFlowStructureT jecnFlowStructureT = this.flowStructureDao.get(id);
            if (jecnFlowStructureT == null) {
                return null;
            }

            return getProcessData(jecnFlowStructureT);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    private ProcessOpenData getProcessData(JecnFlowStructureT jecnFlowStructureT) throws Exception {
        Long id = jecnFlowStructureT.getFlowId();
        JecnFlowBasicInfoT jecnFlowBasicInfoT = processBasicDao.get(id);
        // D2版本自定义要素和相关文件
        if (!JecnContants.isPubShow()) {
            JecnFlowBasicInfoT2 jecnFlowBasicInfoT2 = processBasic2Dao.get(id);
            jecnFlowBasicInfoT.setFlowRelatedFile(jecnFlowBasicInfoT2.getFlowRelatedFile());
            jecnFlowBasicInfoT.setFlowCustomOne(jecnFlowBasicInfoT2.getFlowCustomOne());
            jecnFlowBasicInfoT.setFlowCustomTwo(jecnFlowBasicInfoT2.getFlowCustomTwo());
            jecnFlowBasicInfoT.setFlowCustomThree(jecnFlowBasicInfoT2.getFlowCustomThree());
            jecnFlowBasicInfoT.setFlowCustomFour(jecnFlowBasicInfoT2.getFlowCustomFour());
            jecnFlowBasicInfoT.setFlowCustomFive(jecnFlowBasicInfoT2.getFlowCustomFive());
        }
        ProcessOpenData processOpenData = new ProcessOpenData();
        // 流程基本对象
        processOpenData.setJecnFlowStructureT(jecnFlowStructureT);
        processOpenData.setJecnFlowBasicInfoT(jecnFlowBasicInfoT);
        List<ProcessFigureData> listProcessFigureData = new ArrayList<ProcessFigureData>();
        // 图形
        List<JecnFlowStructureImageT> listJecnFlowStructureImageT = this.flowStructureImageTDao.findAllByFlowId(id);

        // 线段
        List<JecnLineSegmentT> listJecnLineSegmentT = this.lineSegmentTDao.findAllByFlowId(id);

        // 流程下所有的活动的输入和操作规范（设计器）
        List<JecnActivityFileT> listJecnActivityFileT = new ArrayList<JecnActivityFileT>();
        String sql = "select t.file_id,t.figure_id,t.file_s_id,"
                + "        jf.file_name,t.file_type,jf.doc_id,JFO.ORG_NAME,T.UUID,T.FIGURE_UUID"
                + "       from jecn_activity_file_t t,jecn_flow_structure_image_t jsi,jecn_file_t jf"
                + "		  LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID = JF.ORG_ID"
                + "        where t.figure_id=jsi.figure_id and t.file_s_id=jf.file_id and jsi.flow_id=?";
        List<Object[]> listObjects = flowStructureDao.listNativeSql(sql, id);

        // 获取输入输出集合，new

        // 流程中术语 集合
        List<Object[]> termDefineList = flowStructureDao.getTermDefineByFlowId(id, "_T");

        for (Object[] obj : listObjects) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[4] == null) {
                continue;
            }
            JecnActivityFileT jecnActivityFileT = new JecnActivityFileT();
            jecnActivityFileT.setFileId(Long.valueOf(obj[0].toString()));
            jecnActivityFileT.setFigureId(Long.valueOf(obj[1].toString()));
            jecnActivityFileT.setFileSId(Long.valueOf(obj[2].toString()));
            jecnActivityFileT.setFileName(obj[3].toString());
            jecnActivityFileT.setFileType(Long.valueOf(obj[4].toString()));
            if (obj[5] != null) {
                jecnActivityFileT.setFileNumber(obj[5].toString());
            } else {
                jecnActivityFileT.setFileNumber("");
            }
            if (obj[7] != null) {
                jecnActivityFileT.setUUID(obj[7].toString());
            }
            if (obj[8] != null) {
                jecnActivityFileT.setFigureUUID(obj[8].toString());
            }
            listJecnActivityFileT.add(jecnActivityFileT);
        }
        // 流程下所有的活动的输出
        List<JecnModeFileT> listJecnModeFileT = new ArrayList<JecnModeFileT>();
        sql = "select t.mode_file_id,t.figure_id,t.file_m_id,jf.file_name,jf.doc_id,T.UUID,T.FIGURE_UUID"
                + "       from jecn_mode_file_t t,jecn_flow_structure_image_t jsi,jecn_file_t jf"
                + "       where t.figure_id=jsi.figure_id and t.file_m_id = jf.file_id and jsi.flow_id=?";

        listObjects = flowStructureDao.listNativeSql(sql, id);
        for (Object[] obj : listObjects) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
                continue;
            }
            JecnModeFileT jecnModeFileT = new JecnModeFileT();
            jecnModeFileT.setModeFileId(Long.valueOf(obj[0].toString()));
            jecnModeFileT.setFigureId(Long.valueOf(obj[1].toString()));
            jecnModeFileT.setFileMId(Long.valueOf(obj[2].toString()));
            jecnModeFileT.setModeName(obj[3].toString());
            if (obj[4] != null) {
                jecnModeFileT.setFileNumber(obj[4].toString());
            } else {
                jecnModeFileT.setFileNumber("");
            }
            if (obj[5] != null) {
                jecnModeFileT.setUUID(obj[5].toString());
            }
            if (obj[6] != null) {
                jecnModeFileT.setFigureUUID(obj[6].toString());
            }
            listJecnModeFileT.add(jecnModeFileT);
        }
        // 流程下所有的活动的样例
        List<JecnTempletT> listJecnTempletT = new ArrayList<JecnTempletT>();
        sql = "select t.id,t.mode_file_id,t.file_id,jf.file_name,jf.doc_id,T.UUID,T.MODE_UUID"
                + "        from jecn_templet_t t,jecn_mode_file_t jm,jecn_flow_structure_image_t jsi,jecn_file_t jf"
                + "        where t.mode_file_id=jm.mode_file_id and jm.figure_id=jsi.figure_id and t.file_id=jf.file_id and jsi.flow_id=?";
        listObjects = flowStructureDao.listNativeSql(sql, id);
        for (Object[] obj : listObjects) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
                continue;
            }
            JecnTempletT jecnTempletT = new JecnTempletT();
            jecnTempletT.setId(Long.valueOf(obj[0].toString()));
            jecnTempletT.setModeFileId(Long.valueOf(obj[1].toString()));
            jecnTempletT.setFileId(Long.valueOf(obj[2].toString()));
            jecnTempletT.setFileName(obj[3].toString());
            if (obj[4] != null) {
                jecnTempletT.setFileNumber(obj[4].toString());
            } else {
                jecnTempletT.setFileNumber("");
            }
            if (obj[5] != null) {
                jecnTempletT.setUUID(obj[5].toString());
            }
            if (obj[6] != null) {
                jecnTempletT.setModeUUID(obj[6].toString());
            }
            listJecnTempletT.add(jecnTempletT);
        }
        // 流程下所有的活动的指标
        List<JecnRefIndicatorsT> listJecnRefIndicatorsT = flowStructureDao.findAllJecnRefIndicatorsTsByFlowIdT(id);
        // 流程下所有的角色的相关岗位和岗位组
        List<JecnFlowStationT> listJecnFlowStationT = new ArrayList<JecnFlowStationT>();
        listObjects = flowStructureDao.findAllRolePosByFlowIdT(id);
        for (Object[] obj : listObjects) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[4] == null) {
                continue;
            }
            JecnFlowStationT jecnFlowStationT = new JecnFlowStationT();
            jecnFlowStationT.setFlowStationId(Long.valueOf(obj[0].toString()));
            jecnFlowStationT.setFigureFlowId(Long.valueOf(obj[1].toString()));
            jecnFlowStationT.setFigurePositionId(Long.valueOf(obj[2].toString()));
            jecnFlowStationT.setStationName(obj[3].toString());
            jecnFlowStationT.setType(obj[4].toString());
            if (obj[5] == null) {
                log.error("角色对应岗位信息异常！ 角色ID" + jecnFlowStationT.getFigureFlowId());
            } else {
                jecnFlowStationT.setFigureUUID(obj[5].toString());
                jecnFlowStationT.setUUID(obj[6].toString());
            }
            listJecnFlowStationT.add(jecnFlowStationT);
        }
        // 流程下所有活动对应的控制点
        List<JecnControlPointT> listControlPointT = flowStructureDao.findListControlPointTAll(id);
        // 流程下所有活动对应的线上信息
        List<JecnActiveOnLineTBean> listOnLineT = flowStructureDao.findActiveOnLineTBeans(id, false);
        List<JecnActiveStandardBeanT> listStandardT = this.flowStructureDao.findActiveStandardBeanTs(id);
        // 流程中文档相关文件集合
        List<JecnFigureFileTBean> listJecnFigureFileTBean = flowStructureDao.findJecnFigureFileTList(id);

        // 查询活动和to from的关联关系
        List<JecnToFromActiveT> toFromRelatedList = flowStructureDao.findToFromRelatedActiveT(id);
        Map<Long, Long> relatedMap = new HashMap<Long, Long>();
        for (JecnToFromActiveT activeT : toFromRelatedList) {
            relatedMap.put(activeT.getToFromFigureId(), activeT.getActiveFigureId());
        }

        // 新版 活动输入输出
        List<JecnFigureInoutT> figureInList = new ArrayList<JecnFigureInoutT>();
        List<JecnFigureInoutT> figureOutList = new ArrayList<JecnFigureInoutT>();

        getFigureInOutList(id, figureInList, figureOutList);

        // 流程下活动对应的所有活动相关标准信息
        for (JecnFlowStructureImageT structureImageT : listJecnFlowStructureImageT) {
            ProcessFigureData processFigureData = new ProcessFigureData();
            processFigureData.setJecnFlowStructureImageT(structureImageT);
            // 活动
            if (JecnUtil.isActive(structureImageT.getFigureType())) {
                // 输入和操作规范
                List<JecnActivityFileT> listJecnActivityFileTSurplus = new ArrayList<JecnActivityFileT>();
                processFigureData.setListJecnActivityFileT(this.getJecnActivityFileTByFigureId(structureImageT
                        .getFigureId(), listJecnActivityFileT, listJecnActivityFileTSurplus));
                listJecnActivityFileT = listJecnActivityFileTSurplus;
                // 输出
                // 减去已经找到，剩下活动的输出
                List<JecnModeFileT> listJecnModeFileTSurplus = new ArrayList<JecnModeFileT>();

                processFigureData.setListModeFileT(this.getJecnModeFileTsByFigureId(structureImageT.getFigureId(),
                        listJecnModeFileT, listJecnModeFileTSurplus, listJecnTempletT));
                listJecnModeFileT = listJecnModeFileTSurplus;
                // 指标
                List<JecnRefIndicatorsT> listJecnRefIndicatorsTSurplus = new ArrayList<JecnRefIndicatorsT>();
                processFigureData.setListRefIndicatorsT(this.getJecnRefIndicatorsTByFigureId(structureImageT
                        .getFigureId(), listJecnRefIndicatorsT, listJecnRefIndicatorsTSurplus));
                listJecnRefIndicatorsT = listJecnRefIndicatorsTSurplus;

                // 新版，改造后输入、输出
                processFigureData.setFigureInList(getJecnFigureInOutTByFigureId(structureImageT.getFigureId(),
                        figureInList));
                processFigureData.setFigureOutList(getJecnFigureInOutTByFigureId(structureImageT.getFigureId(),
                        figureOutList));

                // 控制点 C注
                // JecnControlPointT controlPointT =
                // getControlPointTByFigureId(
                // jecnFlowStructureImageT.getFigureId(),
                // listControlPointT);
                // if (controlPointT != null) {// 存在控制点
                // processFigureData.setControlPoint(controlPointT);
                // listControlPointT.remove(controlPointT);
                // }

                // 控制点集合
                processFigureData.setListControlPoint(getControlPointTByFigureIdList(structureImageT.getFigureId(),
                        listControlPointT));

                // 线上信息
                processFigureData.setListJecnActiveOnLineTBean(getJecnActiveOnLineTBeanByFigureId(structureImageT
                        .getFigureId(), listOnLineT));
                // 去掉已匹配的线上信息
                listOnLineT.removeAll(processFigureData.getListJecnActiveOnLineTBean());

                // 活动相关标准
                processFigureData.setListJecnActiveStandardBeanT(getJecnActiveStandardBeanTByFigureId(structureImageT
                        .getFigureId(), listStandardT));

            }// 角色
            else if (JecnUtil.isRole(structureImageT.getFigureType())) {
                // 岗位或者岗位组
                List<JecnFlowStationT> listJecnFlowStationTSurplus = new ArrayList<JecnFlowStationT>();
                processFigureData.setListRolePosition(this.getJecnFlowStationTByFigureId(structureImageT.getFigureId(),
                        listJecnFlowStationT, listJecnFlowStationTSurplus));
                listJecnFlowStationT = listJecnFlowStationTSurplus;
            } else if (JecnUtil.isRelateLine(structureImageT.getFigureType())) {// 连接线
                List<JecnLineSegmentT> listJecnLineSegmentTSurplus = new ArrayList<JecnLineSegmentT>();
                structureImageT.setListLineSegmet(this.getlistJecnLineSegmentTSurplusByFigureId(structureImageT
                        .getFigureId(), listJecnLineSegmentT, listJecnLineSegmentTSurplus));
                listJecnLineSegmentT = listJecnLineSegmentTSurplus;
            } else if (JecnUtil.isToOrFrom(structureImageT.getFigureType())) {
                processFigureData.setRelatedId(relatedMap.get(processFigureData.getJecnFlowStructureImageT()
                        .getFigureId()));
            } else if (JecnUtil.isTermFigure(structureImageT.getFigureType())) {
                processFigureData.setTermDefine(getTermDefine(termDefineList, structureImageT.getLinkFlowId()));
            }

            // 元素对应的附件集合
            List<JecnFigureFileTBean> listResult = null;
            if (structureImageT.getFigureType().equals("FileImage")) {
                // 当前元素对应的附件集合
                listResult = getlistJecnFigureFileTBeanByFigureId(structureImageT.getFigureId(),
                        listJecnFigureFileTBean);
                processFigureData.setListJecnFigureFileTBean(listResult);
            }
            listProcessFigureData.add(processFigureData);
        }
        processOpenData.setProcessFigureDataList(listProcessFigureData);
        return processOpenData;
    }

    private void getFigureInOutList(Long flowId, List<JecnFigureInoutT> figureInList,
                                    List<JecnFigureInoutT> figureOutList) {
        // 获取输入输出集合，new
        String sql = "SELECT DISTINCT FIO.ID," + "          FIO.FIGURE_ID," + "          null," + "          FIO.TYPE,"
                + "          FIO.NAME," + "          FIO.EXPLAIN," + "          null,FIO.SORT_ID "
                + "     FROM JECN_FIGURE_IN_OUT_T FIO" + "    INNER JOIN JECN_FLOW_STRUCTURE_IMAGE_T FSIT"
                + "       ON FSIT.FIGURE_ID = FIO.FIGURE_ID" + " WHERE FSIT.FLOW_ID = ? order by sort_id";
        List<Object[]> listObjects = flowStructureDao.listNativeSql(sql, flowId);
        if (listObjects == null || listObjects.isEmpty()) {
            return;
        }

        List<Long> figureIds = new ArrayList<Long>();
        JecnFigureInoutT figureInoutT = null;
        for (Object[] obj : listObjects) {
            figureInoutT = new JecnFigureInoutT();
            figureInoutT.setFlowId(flowId);
            figureInoutT.setId(valueOfString(obj[0]));
            figureInoutT.setFigureId(valueOfLong(obj[1]));
            figureInoutT.setType(valueOfInt(obj[3]));
            figureInoutT.setName(valueOfString(obj[4]));
            figureInoutT.setExplain(valueOfString(obj[5]));
            figureInoutT.setSortId(valueOfLong(obj[7]));
            figureIds.add(figureInoutT.getFigureId());
            if (figureInoutT.getType() == 1) {// 输出
                figureOutList.add(figureInoutT);
            } else {
                figureInList.add(figureInoutT);
            }
        }

        // 获取输出对应的样例集合 和 模板集合 (后期改造 新版活动输入输出 模板样例可以选择多条)
        getInOutSampleList(flowId, figureIds, figureOutList, figureInList);
    }

    @SuppressWarnings("unchecked")
    private void getInOutSampleList(Long flowId, List<Long> figureIds, List<JecnFigureInoutT> figureOutList,
                                    List<JecnFigureInoutT> figureInList) {
        if (figureIds == null || figureIds.isEmpty()) {
            return;
        }

        String sql = "SELECT DISTINCT IOST.ID, IOST.IN_OUT_ID, IOST.FILE_ID, JFT.FILE_NAME ,IOST.TYPE"
                + "  FROM JECN_FIGURE_IN_OUT_SAMPLE_T IOST" + " INNER JOIN JECN_FIGURE_IN_OUT_T IOT"
                + "    ON IOT.ID = IOST.IN_OUT_ID" + " INNER JOIN JECN_FILE_T JFT"
                + "    ON JFT.FILE_ID = IOST.FILE_ID "
                + " WHERE JFT.DEL_STATE=0 AND IOT.FLOW_ID=? AND IOST.FLOW_ID=? ORDER BY JFT.FILE_NAME ASC";
        List<Object[]> inOutSamples = flowStructureDao.listNativeSql(sql, flowId, flowId);
        JecnFigureInoutSampleT inoutSampleT = null;
        List<JecnFigureInoutSampleT> inoutSample = null;
        for (JecnFigureInoutT figureInoutT : figureOutList) {
            inoutSample = new ArrayList<JecnFigureInoutSampleT>();
            for (Object[] obj : inOutSamples) {
                inoutSampleT = new JecnFigureInoutSampleT();
                inoutSampleT.setId(valueOfString(obj[0]));
                inoutSampleT.setFlowId(flowId);
                inoutSampleT.setInoutId(valueOfString(obj[1]));
                inoutSampleT.setFileId(valueOfLong(obj[2]));
                inoutSampleT.setFileName(valueOfString(obj[3]));
                inoutSampleT.setType(valueOfInt(obj[4]));
                if (figureInoutT.getId().equals(inoutSampleT.getInoutId())) {
                    inoutSample.add(inoutSampleT);
                }
            }
            figureInoutT.setListSampleT(inoutSample);
        }

        for (JecnFigureInoutT figureInoutT : figureInList) {
            inoutSample = new ArrayList<JecnFigureInoutSampleT>();
            for (Object[] obj : inOutSamples) {
                inoutSampleT = new JecnFigureInoutSampleT();
                inoutSampleT.setId(valueOfString(obj[0]));
                inoutSampleT.setFlowId(flowId);
                inoutSampleT.setInoutId(valueOfString(obj[1]));
                inoutSampleT.setFileId(valueOfLong(obj[2]));
                inoutSampleT.setFileName(valueOfString(obj[3]));
                inoutSampleT.setType(valueOfInt(obj[4]));
                if (figureInoutT.getId().equals(inoutSampleT.getInoutId())) {
                    inoutSample.add(inoutSampleT);
                }
            }
            figureInoutT.setListSampleT(inoutSample);
        }
    }

    private String valueOfString(Object obj) {
        return obj == null ? null : obj.toString();
    }

    private String getTermDefine(List<Object[]> list, Long id) {
        if (list == null || id == null) {
            return null;
        }
        for (Object[] obj : list) {
            if (obj[0].toString().equals(id.toString())) {
                return obj[1] == null ? null : obj[1].toString();
            }
        }
        return null;
    }

    /**
     * @param figureId
     * @param listJecnLineSegmentT
     * @param listJecnLineSegmentTSurplus
     * @return
     * @author zhangchen Jul 26, 2012
     * @description：获得连接线的线段
     */
    private List<JecnLineSegmentT> getlistJecnLineSegmentTSurplusByFigureId(Long figureId,
                                                                            List<JecnLineSegmentT> listJecnLineSegmentT, List<JecnLineSegmentT> listJecnLineSegmentTSurplus) {
        List<JecnLineSegmentT> listResult = new ArrayList<JecnLineSegmentT>();
        for (JecnLineSegmentT jecnLineSegmentT : listJecnLineSegmentT) {
            if (figureId.equals(jecnLineSegmentT.getFigureId())) {
                listResult.add(jecnLineSegmentT);
            } else {
                listJecnLineSegmentTSurplus.add(jecnLineSegmentT);
            }
        }
        return listResult;
    }

    /**
     * @param figureId
     * @param listJecnFlowStationT
     * @param listJecnFlowStationTSurplus
     * @return
     * @author zhangchen Jul 26, 2012
     * @description：获得角色的岗位或岗位组
     */
    private List<JecnFlowStationT> getJecnFlowStationTByFigureId(Long figureId,
                                                                 List<JecnFlowStationT> listJecnFlowStationT, List<JecnFlowStationT> listJecnFlowStationTSurplus) {
        List<JecnFlowStationT> listResult = new ArrayList<JecnFlowStationT>();
        for (JecnFlowStationT jecnFlowStationT : listJecnFlowStationT) {
            if (figureId.equals(jecnFlowStationT.getFigureFlowId())) {
                listResult.add(jecnFlowStationT);
            } else {
                listJecnFlowStationTSurplus.add(jecnFlowStationT);
            }
        }
        return listResult;
    }

    /**
     * @param figureId
     * @param listJecnRefIndicatorsT
     * @param listJecnRefIndicatorsTSurplus
     * @return
     * @author zhangchen Jul 26, 2012
     * @description：获得活动的指标
     */
    private List<JecnRefIndicatorsT> getJecnRefIndicatorsTByFigureId(Long figureId,
                                                                     List<JecnRefIndicatorsT> listJecnRefIndicatorsT, List<JecnRefIndicatorsT> listJecnRefIndicatorsTSurplus) {
        List<JecnRefIndicatorsT> listResult = new ArrayList<JecnRefIndicatorsT>();
        for (JecnRefIndicatorsT jecnRefIndicatorsT : listJecnRefIndicatorsT) {
            if (figureId.equals(jecnRefIndicatorsT.getActivityId())) {
                listResult.add(jecnRefIndicatorsT);
            } else {
                listJecnRefIndicatorsTSurplus.add(jecnRefIndicatorsT);
            }
        }
        return listResult;
    }

    /**
     * 获取当前活动对应的所有控制点ID
     *
     * @param figureId
     * @param listControlPointT
     * @return List<JecnControlPointT> 活动的控制点集合
     */
    private JecnControlPointT getControlPointTByFigureId(Long figureId, List<JecnControlPointT> listControlPointT) {
        for (JecnControlPointT controlPointT : listControlPointT) {
            if (controlPointT.getActiveId().equals(figureId)) {
                return controlPointT;
            }
        }
        return null;
    }

    /**
     * 获取当前活动对应的所有的控制点
     *
     * @param figureId ： 元素ID listControlPointT： 当前流程下所有的控制点集合
     * @return List<JecnControlPointT> 当前活动对应的所有的控制点集合
     * @author CHEHUANBO
     */
    @SuppressWarnings("all")
    private List<JecnControlPointT> getControlPointTByFigureIdList(Long figureId,
                                                                   List<JecnControlPointT> listControlPointT) {
        // 存储当前元素下的控制点集合
        List<JecnControlPointT> listJecnControlPointT = new ArrayList<JecnControlPointT>();
        // 遍历流程下所有的控制点集合 获取当前元素下的控制点
        for (JecnControlPointT jecnControlPointT : listControlPointT) {
            if (figureId != null && figureId.equals(jecnControlPointT.getActiveId())) {
                listJecnControlPointT.add(jecnControlPointT);
            }
        }
        return listJecnControlPointT;
    }

    /**
     * 获取当前活动ID对应的所有线上信息
     *
     * @param figureId                  活动主键ID
     * @param listJecnActiveOnLineTBean 当前流程对应的所有线上信息
     * @return List<JecnActiveOnLineTBean> 活动对应的线上信息
     */
    private List<JecnActiveOnLineTBean> getJecnActiveOnLineTBeanByFigureId(Long figureId,
                                                                           List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean) {
        List<JecnActiveOnLineTBean> listResult = new ArrayList<JecnActiveOnLineTBean>();
        for (JecnActiveOnLineTBean activeOnLineTBean : listJecnActiveOnLineTBean) {
            if (figureId.equals(activeOnLineTBean.getActiveId())) {
                listResult.add(activeOnLineTBean);
            }
        }
        return listResult;
    }

    /**
     * 活动相关标准集合
     *
     * @param figureId                    活动主键ID
     * @param listJecnActiveStandardBeanT
     * @return
     */
    private List<JecnActiveStandardBeanT> getJecnActiveStandardBeanTByFigureId(Long figureId,
                                                                               List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT) {
        List<JecnActiveStandardBeanT> listResult = new ArrayList<JecnActiveStandardBeanT>();
        for (JecnActiveStandardBeanT activeOnLineTBean : listJecnActiveStandardBeanT) {
            if (figureId.equals(activeOnLineTBean.getActiveId())) {
                listResult.add(activeOnLineTBean);
            }
        }
        return listResult;
    }

    /**
     * @param figureId
     * @param listJecnActivityFileT
     * @param listJecnActivityFileTSurplus
     * @return
     * @author zhangchen Jul 26, 2012
     * @description：获得活动的输入和操作规范
     */
    private List<JecnActivityFileT> getJecnActivityFileTByFigureId(Long figureId,
                                                                   List<JecnActivityFileT> listJecnActivityFileT, List<JecnActivityFileT> listJecnActivityFileTSurplus) {
        List<JecnActivityFileT> listResult = new ArrayList<JecnActivityFileT>();
        for (JecnActivityFileT jecnActivityFileT : listJecnActivityFileT) {
            if (figureId.equals(jecnActivityFileT.getFigureId())) {
                listResult.add(jecnActivityFileT);
            } else {
                listJecnActivityFileTSurplus.add(jecnActivityFileT);
            }
        }
        return listResult;
    }

    private List<JecnFigureInoutT> getJecnFigureInOutTByFigureId(Long figureId,
                                                                 List<JecnFigureInoutT> listJecnActivityFileT) {
        List<JecnFigureInoutT> listResult = new ArrayList<JecnFigureInoutT>();
        for (JecnFigureInoutT inoutT : listJecnActivityFileT) {
            if (figureId.equals(inoutT.getFigureId())) {
                listResult.add(inoutT);
            }
        }
        return listResult;
    }

    /**
     * @param figureId
     * @param listJecnModeFileT
     * @param listJecnModeFileTSurplus 减去这个活动的输出，剩下的所有的
     * @return
     * @author zhangchen Jul 26, 2012
     * @description：获得活动的输出和样例
     */
    private List<JecnModeFileT> getJecnModeFileTsByFigureId(Long figureId, List<JecnModeFileT> listJecnModeFileT,
                                                            List<JecnModeFileT> listJecnModeFileTSurplus, List<JecnTempletT> listJecnTempletT) {
        List<JecnModeFileT> listResult = new ArrayList<JecnModeFileT>();
        for (JecnModeFileT jecnModeFileT : listJecnModeFileT) {
            if (figureId.equals(jecnModeFileT.getFigureId())) {
                List<JecnTempletT> listResultJecnTempletT = new ArrayList<JecnTempletT>();
                for (JecnTempletT jecnTempletT : listJecnTempletT) {
                    if (jecnModeFileT.getModeFileId().equals(jecnTempletT.getModeFileId())) {
                        listResultJecnTempletT.add(jecnTempletT);
                    }
                }
                jecnModeFileT.setListJecnTempletT(listResultJecnTempletT);
                listResult.add(jecnModeFileT);
            } else {
                listJecnModeFileTSurplus.add(jecnModeFileT);
            }
        }
        return listResult;
    }

    public JecnFlowDriverT getJecnFlowDriverT(Long flowId) throws Exception {
        try {
            JecnFlowDriverT driver = processDriverTDao.get(flowId);
            if (driver != null) {
                String hql = "from JecnFlowDriverTimeT where flowId=?";
                List<JecnFlowDriverTimeT> times = this.flowStructureDao.listHql(hql, flowId);
                driver.setTimes(times);
            }
            return driver;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public boolean validateAddName(String name, long pid, long isFlow, Long projectId) throws Exception {
        try {
            String hql = "from JecnFlowStructureT where dataType<>1 and perFlowId =? and isFlow=? and flowName=? and projectId=? and delState <> 2";
            List<JecnFlowStructureT> list = flowStructureDao.listHql(hql, pid, isFlow, name, projectId);
            if (list != null && list.size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    public boolean validateUpdateName(String name, long id, long pid, long isFlow, Long projectId) throws Exception {
        try {
            String hql = "from JecnFlowStructureT where dataType<>1 and perFlowId =? and isFlow=? and flowId<> ? and flowName=? and projectId=?  and delState <> 2";
            List<JecnFlowStructureT> list = flowStructureDao.listHql(hql, pid, isFlow, id, name, projectId);
            if (list != null && list.size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    public void setFlowStructureImageTDao(IFlowStructureImageTDao flowStructureImageTDao) {
        this.flowStructureImageTDao = flowStructureImageTDao;
    }

    /**
     * 流程真删
     */
    @Override
    public void deleteIdsFlow(List<Long> listIds, Long projectId, Long updatePersionId) throws Exception {// 所有删除的流程
        try {
            // 所有删除的流程
            Set<Long> setFlowIds = new HashSet<Long>();
            // 所有删除的流程地图
            Set<Long> setMainFlowIds = new HashSet<Long>();
            String sql = JecnCommonSql.getChildObjectsSqlByTypeDelState(listIds, TreeNodeType.processMap);
            List<Object[]> listAll = fileDao.listNativeSql(sql);
            for (Object[] obj : listAll) {
                if (obj == null || obj[0] == null || obj[1] == null) {
                    continue;
                }
                if ("1".equals(obj[1].toString())) {
                    setFlowIds.add(Long.valueOf(obj[0].toString()));
                } else {
                    setMainFlowIds.add(Long.valueOf(obj[0].toString()));
                }
            }
            // 删除流程地图
            if (setMainFlowIds.size() > 0) {
                // 添加日志
                JecnUtil.saveJecnJournals(setMainFlowIds, 1, 3, updatePersionId, flowStructureDao);
                flowStructureDao.deleteMainFlows(setMainFlowIds);
            }
            sql = "SELECT T.FILE_PATH FROM FLOW_FILE_CONTENT T "
                    + " WHERE  T.IS_VERSION_LOCAL = 0 AND T.RELATED_ID IN "
                    + JecnCommonSql.getChildObjectsSqlByTypeDelAddBracket(listIds, TreeNodeType.processFile);
            List<String> listFilePath = this.flowStructureDao.listNativeSql(sql);
            // 删除流程
            if (setFlowIds.size() > 0) {
                // 添加日志
                JecnUtil.saveJecnJournals(setFlowIds, 1, 3, updatePersionId, flowStructureDao);
                flowStructureDao.deleteFlows(setFlowIds);
            }
            // 删除本地版本文件
            for (String filePath : listFilePath) {
                JecnFinal.deleteFile(filePath);
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public List<Object[]> getDelsFlow(Long projectId, Set<Long> setIds, boolean isAdmin) throws Exception {
        try {
            if (isAdmin) {
                String hql = "select flowId,flowName,isFlow,perFlowId from JecnFlowStructureT where dataType=0 and delState = 1 and projectId =? order by perFlowId,sortId,flowId";
                return flowStructureDao.listHql(hql, projectId);
            } else {
                if (setIds == null || setIds.size() == 0) {
                    return null;
                }
                if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
                    String sql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JF WHERE JF.FLOW_ID in"
                            + JecnCommonSql.getIdsSet(setIds)
                            + " UNION ALL"
                            + " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.Flow_Id=JFS.Pre_Flow_Id)"
                            + " SELECT distinct flow_id,flow_name,isflow,pre_flow_id FROM MY_FLOW WHERE DATA_TYPE=0 and del_state=1 and projectid=? and flow_id not in"
                            + JecnCommonSql.getIdsSet(setIds);
                    return flowStructureDao.listNativeSql(sql, projectId);
                } else if (JecnContants.dbType.equals(DBType.ORACLE)) {
                    String sql = "select distinct t.flow_id,t.flow_name,t.isflow,t.pre_flow_id from Jecn_Flow_Structure_t t"
                            + " where t.data_type=0 and t.del_state=1 and t.projectid=? and t.flow_id not in"
                            + JecnCommonSql.getIdsSet(setIds)
                            + "       connect by prior t.flow_id=t.pre_flow_id"
                            + "       start with t.flow_id in" + JecnCommonSql.getIdsSet(setIds);
                    // 获得所有需要删除的流程或流程地图
                    return flowStructureDao.listNativeSql(sql, projectId);
                } else if (JecnContants.dbType.equals(DBType.MYSQL)) {
                    List<Object[]> listResult = new ArrayList<Object[]>();
                    String sql = "select t.flow_id,t.flow_name,t.isflow,t.pre_flow_id from Jecn_Flow_Structure_t t where t.data_type=0 and t.projectid=?";
                    List<Object[]> listAll = flowStructureDao.listNativeSql(sql, projectId);
                    Iterator<Long> it = setIds.iterator();
                    while (it.hasNext()) {
                        Long flowId = it.next();
                        this.findChilds(flowId, listAll, listResult);
                    }
                    return listResult;
                }
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
        return null;
    }

    private void findChilds(Long flowId, List<Object[]> listAll, List<Object[]> listResult) {
        for (Object[] obj : listAll) {
            if (obj == null || obj[0] == null || obj[2] == null || obj[3] == null) {
                continue;
            }
            if (obj[2].toString().equals(flowId.toString())) {
                listResult.add(obj);
                this.findChilds(Long.valueOf(obj[0].toString()), listAll, listResult);
            }
        }
    }

    @Override
    public List<JecnTreeBean> getParentsContainSelf(Long projectId, Long id) throws Exception {
        try {
            if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
                String sql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JF WHERE JF.FLOW_ID ="
                        + id
                        + " UNION ALL"
                        + " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.PRE_FLOW_ID=JFS.FLOW_ID)"
                        + " ,"
                        + " MY_JECN AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JFF WHERE JFF.FLOW_ID = "
                        + id
                        + " UNION ALL"
                        + " SELECT JFS.* FROM MY_JECN INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_JECN.FLOW_ID=JFS.PRE_FLOW_ID) "
                        + " SELECT ST.FLOW_ID,ST.FLOW_NAME,ST.PRE_FLOW_ID,ST.ISFLOW,ST.FLOW_ID_INPUT,CASE WHEN S.FLOW_ID IS NULL THEN '0' ELSE '1' END AS S_FLOW_ID"
                        + "  ,(SELECT COUNT(*) FROM JECN_FLOW_STRUCTURE_T WHERE PRE_FLOW_ID = ST.FLOW_ID) AS COUNT,ST.DEL_STATE "
                        + " FROM JECN_FLOW_STRUCTURE_T ST"
                        + " LEFT JOIN JECN_FLOW_STRUCTURE S ON ST.FLOW_ID = S.FLOW_ID"
                        + " WHERE ST.PROJECTID=? AND ST.DATA_TYPE=0 AND ST.FLOW_ID in (SELECT FLOW_ID FROM  MY_FLOW) OR ST.PRE_FLOW_ID in"
                        + " (SELECT FLOW_ID" + " FROM MY_FLOW WHERE MY_FLOW.DEL_STATE = 1) "
                        + " OR ST.FLOW_ID IN (SELECT FLOW_ID FROM MY_JECN)" + " OR ST.PRE_FLOW_ID in"
                        + " (SELECT FLOW_ID" + " FROM MY_JECN)" + " ORDER BY ST.PRE_FLOW_ID,ST.SORT_ID,ST.FLOW_ID";
                List<Object[]> list = flowStructureDao.listNativeSql(sql, projectId);
                return findByListObjects(list);
            } else if (JecnContants.dbType.equals(DBType.ORACLE)) {
                String sql = "SELECT ST.FLOW_ID,ST.FLOW_NAME,ST.PRE_FLOW_ID,ST.ISFLOW,ST.FLOW_ID_INPUT,CASE WHEN S.FLOW_ID IS NULL THEN '0' ELSE '1' END AS S_FLOW_ID"
                        + "  ,(SELECT COUNT(*) FROM JECN_FLOW_STRUCTURE_T WHERE PRE_FLOW_ID = ST.FLOW_ID) AS COUNT ,ST.DEL_STATE "
                        + " FROM JECN_FLOW_STRUCTURE_T ST"
                        + " LEFT JOIN JECN_FLOW_STRUCTURE S ON ST.FLOW_ID = S.FLOW_ID"
                        + " WHERE ST.PROJECTID=? AND ST.DATA_TYPE=0 AND ST.FLOW_ID in (SELECT T.FLOW_ID FROM JECN_FLOW_STRUCTURE_T T CONNECT BY PRIOR T.PRE_FLOW_ID = T.FLOW_ID  START WITH T.FLOW_ID ="
                        + id
                        + ") OR ST.FLOW_ID IN (select st.FLOW_ID from JECN_FLOW_STRUCTURE_T st "
                        + " connect by prior st.FLOW_ID = st.PRE_FLOW_ID START WITH st.FLOW_ID ="
                        + id
                        + ") ORDER BY ST.PRE_FLOW_ID,ST.SORT_ID,ST.FLOW_ID";
                List<Object[]> list = flowStructureDao.listNativeSql(sql, projectId);
                return findByListObjects(list);
            } else if (JecnContants.dbType.equals(DBType.MYSQL)) {
                String sql = "SELECT ST.FLOW_ID,ST.FLOW_NAME,ST.PRE_FLOW_ID,ST.ISFLOW,ST.FLOW_ID_INPUT,CASE WHEN S.FLOW_ID IS NULL THEN '0' ELSE '1' END AS S_FLOW_ID"
                        + "  ,(SELECT COUNT(*) FROM JECN_FLOW_STRUCTURE_T WHERE PRE_FLOW_ID = ST.FLOW_ID) AS COUNT,ST.DEL_STATE"
                        + " FROM JECN_FLOW_STRUCTURE_T ST"
                        + " LEFT JOIN JECN_FLOW_STRUCTURE S ON ST.FLOW_ID = S.FLOW_ID"
                        + " WHERE ST.PROJECTID=? AND ST.DATA_TYPE=0 ORDER BY ST.PRE_FLOW_ID,ST.SORT_ID,ST.FLOW_ID";
                List<Object[]> list = flowStructureDao.listNativeSql(sql, projectId);
                return findByListObjects(list);
            }
            return null;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 判断流程地图或流程图能否打开
    public boolean updateProcessCanOpen(Long userId, Long processId) throws Exception {
        try {
            String hql = "update JecnFlowStructureT set occupier=? where flowId=? and (occupier =0 or occupier=?)";
            int i = flowStructureDao.execteHql(hql, userId, processId, userId);
            if (i == 1) {
                return true;
            }
            return false;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public String getFlowOccupierUserNameById(Long flowId, Long peopleId) throws Exception {
        String sql = "SELECT JU.TRUE_NAME  FROM JECN_FLOW_STRUCTURE_T T INNER JOIN JECN_USER JU "
                + "ON T.OCCUPIER = JU.PEOPLE_ID WHERE " + "T.FLOW_ID = ? and JU.PEOPLE_ID <> ?";
        List<String> list = flowStructureDao.listNativeSql(sql, flowId, peopleId);
        if (list != null && list.size() > 0) {
            return list.get(0);
        }
        return "";
    }

    @Override
    // 获得流程中活动的操作规范文件的集合（设计器）
    public List<Object[]> getFlowOeprationFilesByFlowIdDesign(Long flowId) throws Exception {
        try {
            String sql = "select distinct jf.file_id,jf.doc_id,jf.file_name from jecn_file jf"
                    + "       where jf.is_dir=1 and jf.del_state=0 and jf.file_id in"
                    + "       (select jaft.file_s_id from jecn_activity_file_t jaft,jecn_flow_structure_image_t jfsit where jaft.file_type=1 and jaft.figure_id=jfsit.figure_id and jfsit.flow_id="
                    + flowId + ")";
            return flowStructureDao.listNativeSql(sql);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    // 获得流程中相关流程
    public List<Object[]> getFlowRelateByFlowIdDesign(Long flowId) throws Exception {
        try {
            String sql = "SELECT T.LINK_FLOW_ID," + "       CASE" + "         WHEN JFS.FLOW_ID IS NULL THEN"
                    + "          T.FIGURE_TEXT" + "         ELSE" + "          JFS.FLOW_NAME" + "       END FLOW_NAME,"
                    + "       T.IMPL_TYPE,JFS.FLOW_ID_INPUT " + "  FROM JECN_FLOW_STRUCTURE_IMAGE_T T"
                    + "   INNER JOIN JECN_FLOW_STRUCTURE_T JFS" + "    ON T.LINK_FLOW_ID = JFS.FLOW_ID"
                    + "   AND JFS.DEL_STATE = 0" + " WHERE T.FIGURE_TYPE IN " + JecnCommonSql.getImplString()
                    + "   AND T.IMPL_TYPE IN (1, 2, 3, 4)"
                    + "   AND T.FLOW_ID = ?  ORDER BY T.IMPL_TYPE,T.LINK_FLOW_ID";
            return flowStructureDao.listNativeSql(sql, flowId);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public JecnCreateDoc getFlowFile(Long flowId, TreeNodeType treeNodeType, boolean isPub) throws Exception {
        return DownloadUtil.getFlowFile(flowId, treeNodeType, isPub, configItemDao, docControlDao, flowStructureDao,
                processBasicDao, processKPIDao, processRecordDao);
    }

    @Override
    public void cancelRelease(Long id, TreeNodeType type, Long peopleId) throws Exception {
        if (JecnCommon.isProcess(type)) {
            flowStructureDao.revokeProcessRelease(id);
        } else if (type == TreeNodeType.processMap) {
            flowStructureDao.revokeProcessMapRelease(id);
        }
        JecnFlowStructureT flowStructureT = flowStructureDao.get(id);
        JecnUtil.saveJecnJournal(id, flowStructureT.getFlowName(), 1, 18, peopleId, configItemDao);
    }

    /**
     * @param id   流程ID
     * @param type 节点类型
     * @throws Exception
     * @author yxw 2012-11-13
     * @description:直接发布-不记录版本
     */
    @Override
    public void directRelease(Long id, TreeNodeType type, Long peopleId) throws Exception {
        // 获取正式版
        String oldPath = "";
        JecnFlowStructure structure = flowStructureDao.findJecnFlowStructureById(id);

        // 保留更新时间
        Date pubTime = new Date();

        // 更换发布时图片路径
        JecnFlowStructureT structureT = flowStructureDao.get(id);

        if (structure != null) {
            oldPath = structure.getFilePath();
            pubTime = structure.getPubTime();
        }
        // 还原发布时间:(不记录文控信息发布，不能更改发布时间)
        String sql = "update jecn_flow_structure_t set update_date=?,pub_time=? where flow_id=?";
        flowStructureDao.execteNative(sql, pubTime, pubTime, id);

        // 相关类型 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图
        int refType = 0;
        if (JecnCommon.isProcess(type)) {// 流程
            // 发布流程的数据
            flowStructureDao.releaseProcessData(id);
            refType = 0;
        } else if (type == TreeNodeType.processMap) {// 地图
            // 发布流程地图的数据
            flowStructureDao.releaseProcessMapData(id);
            refType = 4;
        }
        // 发布获取保存路径下的图片拷贝到发布图片路径下，并记录当前生成当前版本文件
        JecnTaskCommon.getPubImagePath(flowStructureDao, structureT, oldPath, docControlDao, configItemDao,
                processBasicDao, processKPIDao, processRecordDao, type, false);
        // 发布流程、流程地图相关文件，相关制度
        pubRelateFileAndRule(refType, id, type);

        JecnUtil.saveJecnJournal(id, structureT.getFlowName(), 1, 17, peopleId, configItemDao);
    }

    /**
     * 发布流程、流程地图相关文件，相关制度
     *
     * @param refType
     * @param id
     * @param type
     * @throws Exception
     */
    private void pubRelateFileAndRule(int refType, Long id, TreeNodeType type) throws Exception {
        if (JecnTaskCommon.validateRelateFile(refType)) {// 相关文件
            if (JecnCommon.isProcess(type)) {// 流程
                // 未发布文件
                List<Long> listFileIds = flowStructureDao.findProcessNoPubFiles(id);
                if (listFileIds.size() > 0) {
                    this.fileDao.releaseFiles(listFileIds);
                }
            } else if (type == TreeNodeType.processMap) {
                // 未发布文件
                List<Long> listFileIds = flowStructureDao.findProcessMapNoPubFiles(id);
                if (listFileIds.size() > 0) {
                    this.fileDao.releaseFiles(listFileIds);
                }
            }
        }
        if (!JecnTaskCommon.validateRelateRule(refType)) {// 相关制度
            return;
        }
        // 发布流程是否存在相关制度为发布--start
        List<Object[]> listRuleIdsNoRelease = null;
        if (type == TreeNodeType.process || type == TreeNodeType.processFile) {// 流程
            // 相关制度
            listRuleIdsNoRelease = flowStructureDao.getRelateRuleByFlowId(id);
        } else if (type == TreeNodeType.processMap) {// 地图
            // 相关制度
            listRuleIdsNoRelease = flowStructureDao.getRelateRuleByFlowMapId(id);
        }
        // 制度文件
        List<Long> listRuleFileIds = new ArrayList<Long>();
        // 制度模板文件
        List<Long> listRuleModeIds = new ArrayList<Long>();
        for (Object[] obj : listRuleIdsNoRelease) {
            if (obj == null || obj[0] == null || obj[1] == null) {
                continue;
            }
            if ("1".equals(obj[1].toString())) {
                listRuleModeIds.add(Long.valueOf(obj[0].toString()));
            } else {
                listRuleFileIds.add(Long.valueOf(obj[0].toString()));
            }
        }
        if (listRuleFileIds.size() > 0) {// 制度文件
            ruleDao.releaseRuleFileData(listRuleFileIds);
        }
        if (listRuleModeIds.size() > 0) {// 制度模板文件
            ruleDao.releaseRuleModeData(listRuleModeIds);
        }
    }

    public IJecnConfigItemDao getConfigItemDao() {
        return configItemDao;
    }

    public void setConfigItemDao(IJecnConfigItemDao configItemDao) {
        this.configItemDao = configItemDao;
    }

    @Override
    public List<JecnTreeBean> getChildProcessMap(Long flowMapId, Long projectId) throws Exception {
        try {
            String sql = "SELECT DISTINCT T.FLOW_ID, T.FLOW_NAME, T.PRE_FLOW_ID," + " T.ISFLOW, T.FLOW_ID_INPUT, CASE"
                    + "         WHEN JS.FLOW_ID IS NULL THEN" + "          '0'" + "         ELSE" + "          '1'"
                    + "       END AS IS_PUB, CASE WHEN SUB.FLOW_ID IS NOT NULL THEN 1 ELSE 0 END AS COUNT,T.SORT_ID"
                    + "  FROM JECN_FLOW_STRUCTURE_T T" + "  LEFT JOIN JECN_FLOW_STRUCTURE JS ON T.FLOW_ID = JS.FLOW_ID"
                    + "  LEFT JOIN JECN_FLOW_STRUCTURE_T SUB ON SUB.DEL_STATE=0 AND SUB.PRE_FLOW_ID=T.FLOW_ID"
                    + " WHERE T.PRE_FLOW_ID = ?" + "   AND T.ISFLOW = 0" + "   AND T.DATA_TYPE = 0"
                    + "   AND T.PROJECTID =?" + "   AND T.DEL_STATE = 0"
                    + " ORDER BY T.PRE_FLOW_ID, T.SORT_ID, T.FLOW_ID";
            List<Object[]> list = flowStructureDao.listNativeSql(sql, flowMapId, projectId);
            sql = "SELECT T.FLOW_ID,JECN_TASK.STATE,JECN_TASK.APPROVE_TYPE,JECN_TASK.EDIT"
                    + "  FROM JECN_FLOW_STRUCTURE_T T"
                    + "  INNER JOIN JECN_TASK_BEAN_NEW JECN_TASK ON JECN_TASK.R_ID=T.FLOW_ID AND JECN_TASK.IS_LOCK=1 AND JECN_TASK.STATE<>5 AND JECN_TASK.TASK_TYPE in (0,4)"
                    + " WHERE T.PRE_FLOW_ID = ?" + "   AND T.ISFLOW = 0" + "   AND T.DATA_TYPE = 0"
                    + "   AND T.PROJECTID =?" + "   AND T.DEL_STATE = 0";
            List<Object[]> listTaskState = flowStructureDao.listNativeSql(sql, flowMapId, projectId);
            return findByListObjects(list, listTaskState);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public JecnFlowStructureT getJecnFlowStructureT(Long flowId) throws Exception {
        String hql = "from JecnFlowStructureT where flowId=? and delState=0";
        return flowStructureDao.getObjectHql(hql, flowId);
    }

    /**
     * 获取流程图操作说明
     *
     * @return
     * @throws Exception
     * @author fuzhh Dec 22, 2012
     */
    public ProcessDownloadBean getProcessFile(Long flowId) throws Exception {
        return DownloadUtil.getProcessDownloadBean(flowId, true, configItemDao, docControlDao, flowStructureDao,
                processBasicDao, processKPIDao, processRecordDao);
    }

    /**
     * 获取流程地图操作说明
     *
     * @param isPub true正式表，false临时表
     * @return
     * @throws Exception
     * @author fuzhh Dec 22, 2012
     */
    public ProcessMapDownloadBean getProcessMapFile(Long flowId, boolean isPub) throws Exception {
        return DownloadUtil.getProcessMapDownloadBean(flowId, isPub, configItemDao, docControlDao, flowStructureDao);
    }

    /**
     * 批量下载操作说明
     *
     * @param processIdList
     * @return
     * @author fuzhh 2013-9-2
     */
    public String getProcessFiles(List<Long> processIdList, boolean isPub, boolean isDownFile) {
        return SelectDownloadProcess.downloadProcess(processIdList, configItemDao, docControlDao, flowStructureDao,
                processBasicDao, processKPIDao, processRecordDao, isPub, isDownFile);
    }

    /**
     * 根据流程ID查询对应风险
     *
     * @param flowId 流程ID
     * @return
     * @throws Exception
     * @author fuzhh 2013-11-27
     */
    public List<JecnRisk> getRiskByFlowId(Long flowId, boolean isPub) throws Exception {
        return flowStructureDao.getRiskByFlowId(flowId, isPub);
    }

    /**
     * 根据风险ID集合查询对应风险
     *
     * @param flowId 流程ID
     * @return
     * @throws Exception
     * @author fuzhh 2013-11-27
     */
    public List<JecnRisk> getRiskByRiskIds(Set<Long> riskIds) throws Exception {
        if (riskIds != null && riskIds.size() > 0) {
            String sql = "select * from JECN_RISK where id in " + JecnCommonSql.getIdsSet(riskIds);
            List<JecnRisk> jecnRiskList = this.flowStructureDao.getSession().createSQLQuery(sql).addEntity(
                    JecnRisk.class).list();
            return jecnRiskList;
        } else {
            return new ArrayList<JecnRisk>();
        }

    }

    @Override
    public List<JecnFlowStructureT> searchRecycleFlowByName(String flowName, Long projectId, Set<Long> setIds,
                                                            boolean isAdmin) throws Exception {
        if (isAdmin) {
            String hql = "from JecnFlowStructureT where flowName like " + "'%" + flowName + "%'"
                    + " and dataType=0 and delState = 1 and projectId =? order by perFlowId,sortId,flowId";
            return flowStructureDao.listHql(hql, projectId);
        } else {
            List<Object[]> objList = new ArrayList<Object[]>();
            if (setIds == null || setIds.size() == 0) {
                return null;
            }
            if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
                String sql = "WITH MY_FLOW AS(SELECT * FROM JECN_FLOW_STRUCTURE_T JF WHERE JF.FLOW_ID in"
                        + JecnCommonSql.getIdsSet(setIds)
                        + " UNION ALL"
                        + " SELECT JFS.* FROM MY_FLOW INNER JOIN JECN_FLOW_STRUCTURE_T JFS ON MY_FLOW.Flow_Id=JFS.Pre_Flow_Id)"
                        + " SELECT distinct flow_id,flow_name,isflow,pre_flow_id FROM MY_FLOW WHERE flow_name like "
                        + "'%" + flowName + "%'"
                        + " and DATA_TYPE=0 and del_state=1 and projectid=? and flow_id not in"
                        + JecnCommonSql.getIdsSet(setIds);
                objList = flowStructureDao.listNativeSql(sql, projectId);
            } else if (JecnContants.dbType.equals(DBType.ORACLE)) {
                String sql = "select distinct t.flow_id,t.flow_name,t.isflow,t.pre_flow_id from Jecn_Flow_Structure_t t"
                        + " where t.flow_name like "
                        + "'%"
                        + flowName
                        + "%'"
                        + " and t.data_type=0 and t.del_state=1 and t.projectid=? and t.flow_id not in"
                        + JecnCommonSql.getIdsSet(setIds)
                        + "       connect by prior t.flow_id=t.pre_flow_id"
                        + "       start with t.flow_id in" + JecnCommonSql.getIdsSet(setIds);
                // 获得所有需要删除的流程或流程地图
                objList = flowStructureDao.listNativeSql(sql, projectId);
            } else if (JecnContants.dbType.equals(DBType.MYSQL)) {
                String sql = "select t.flow_id,t.flow_name,t.isflow,t.pre_flow_id from Jecn_Flow_Structure_t t where t.data_type=0 and t.projectid=?"
                        + " and t.flow_name like " + "'%" + flowName + "%' ";
                List<Object[]> listAll = flowStructureDao.listNativeSql(sql, projectId);
                Iterator<Long> it = setIds.iterator();
                while (it.hasNext()) {
                    Long flowId = it.next();
                    this.findChilds(flowId, listAll, objList);
                }
            }
            return getFlowStructureTByObject(objList);
        }
    }

    private List<JecnFlowStructureT> getFlowStructureTByObject(List<Object[]> objList) {
        List<JecnFlowStructureT> listFlowStructureT = new ArrayList<JecnFlowStructureT>();
        if (objList != null && objList.size() > 0) {
            for (Object[] objects : objList) {
                if (objects == null || objects[0] == null || objects[1] == null || objects[2] == null
                        || objects[3] == null) {
                    continue;
                }
                JecnFlowStructureT flowStructureT = new JecnFlowStructureT();
                flowStructureT.setFlowId(Long.valueOf(objects[0].toString()));
                flowStructureT.setFlowName(objects[1].toString());
                flowStructureT.setIsFlow(Long.valueOf(objects[2].toString()));
                flowStructureT.setPerFlowId(Long.valueOf(objects[3].toString()));
                listFlowStructureT.add(flowStructureT);
            }
        }

        return listFlowStructureT;
    }

    /**
     * 获取流程相关制度
     *
     * @param flowId
     * @param isPub
     * @return
     * @throws Exception
     * @author fuzhh 2013-12-9
     */
    public List<Object[]> getFlowRelateRulesWeb(Long flowId, boolean isPub) throws Exception {
        return processBasicDao.getFlowRelateRulesWeb(flowId, isPub);
    }

    @Override
    public JecnFlowBasicInfoT2 getJecnFlowBasicInfoT2(Long flowId) throws Exception {
        try {
            return processBasic2Dao.get(flowId);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * @param flowId
     * @return
     * @throws Exception
     * @description：获得流程下的KPI及相关数据
     */
    @Override
    public List<JecnFlowKpiNameT> getFlowKpiNameTList(Long flowId) throws Exception {
        List<Object[]> list = this.processKPIDao.getFlowKpiNameTList(flowId, "_T");
        List<Object[]> flowTools = flowToolDao.getFlowTools(flowId, "_T");
        List<JecnFlowKpiNameT> kpiList = new ArrayList<JecnFlowKpiNameT>();
        for (Object[] obj : list) {
            if (obj == null || obj[0] == null) {
                return null;
            }
            JecnFlowKpiNameT flowKpi = new JecnFlowKpiNameT();
            flowKpi.setKpiAndId(Long.valueOf(obj[0].toString()));// 主键ID0
            flowKpi.setFlowId(Long.valueOf(obj[1].toString()));// 流程Id1
            flowKpi.setKpiName(obj[2].toString());// kpi名称2
            if (obj[3] != null && !"".equals(obj[3])) {
                flowKpi.setKpiType(Integer.parseInt(obj[3].toString()));// kpi类别3
            }
            if (obj[4] != null && !"".equals(obj[4])) {
                flowKpi.setKpiDefinition(obj[4].toString());// kpi定义4
            }
            if (obj[5] != null && !"".equals(obj[5])) {
                flowKpi.setKpiStatisticalMethods(obj[5].toString());// kpi计算公式5
            }
            if (obj[6] != null && !"".equals(obj[6])) {
                flowKpi.setKpiTarget(obj[6].toString());// 目标值6
            }
            flowKpi.setKpiHorizontal(obj[7].toString());// 数据统计时间\频率7
            flowKpi.setKpiVertical(obj[8].toString());// kpi值单位名称8
            flowKpi.setCreatTime(JecnCommon.getDateByString(obj[9].toString()));// 创建时间9
            flowKpi.setKpiTargetOperator(Integer.parseInt(obj[10].toString()));// kpi目标值比较符号10
            flowKpi.setKpiDataMethod(Integer.parseInt(obj[11].toString()));// kpi数据获取方式11
            if (obj[12] != null && !"".equals(obj[12])) {
                flowKpi.setKpiDataPeopleId(Long.valueOf(obj[12].toString()));// 数据提供者ID
                // 13
            }
            flowKpi.setKpiRelevance(Integer.parseInt(obj[13].toString()));// 相关度14
            flowKpi.setKpiTargetType(Integer.parseInt(obj[14].toString()));// 指标来源15
            if (obj[15] != null && !"".equals(obj[15])) {
                flowKpi.setFirstTargetId(obj[15].toString());// 支撑的一级指标ID16
            }
            flowKpi.setUpdateTime(JecnCommon.getDateByString(obj[16].toString()));// 更新时间17
            flowKpi.setCreatePeopleId(Long.valueOf(obj[17].toString()));// 创建人ID
            flowKpi.setUpdatePeopleId(Long.valueOf(obj[18].toString()));// 更新人Id
            if (obj[19] != null && !"".equals(obj[19])) {
                flowKpi.setKpiDataPeopleName(obj[19].toString());// 数据提供者名称
            }
            if (obj[20] != null && !"".equals(obj[20])) {
                flowKpi.setFirstTargetContent(obj[20].toString());// 支撑的一级指标名称21
            }
            if (obj[21] != null && !"".equals(obj[21])) {
                flowKpi.setKpiPurpose(obj[21].toString());
            }
            if (obj[22] != null && !"".equals(obj[22])) {
                flowKpi.setKpiPoint(obj[22].toString());
            }
            if (obj[23] != null && !"".equals(obj[23])) {
                flowKpi.setKpiExplain(obj[23].toString());
            }
            if (obj[24] != null && !"".equals(obj[24])) {
                flowKpi.setKpiPeriod(obj[24].toString());
            }
            // IT系统名称
            flowKpi.setKpiITSystemNames(getToolNamesByID(flowTools, flowKpi.getKpiAndId()));
            kpiList.add(flowKpi);
        }
        return kpiList;
    }

    private String getToolNamesByID(List<Object[]> flowTools, Long kpiId) {
        if (flowTools == null || kpiId == null) {
            return "";
        }
        StringBuffer buffer = new StringBuffer();
        for (Object[] objects : flowTools) {
            if (objects[0] == null || objects[1] == null) {
                continue;
            }
            if (kpiId.toString().equals(objects[0].toString())) {
                if (buffer.length() == 0) {
                    buffer.append(objects[1]);
                } else {
                    buffer.append("/").append(objects[1]);
                }
            }
        }
        return buffer.toString();
    }

    private boolean isEqualId(String[] toolIdArr, Object objId) {
        for (String string : toolIdArr) {
            if (string.equals(objId.toString())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<JecnTreeBean> searchPubFlowByName(String name, Long projectId, int type) throws Exception {
        List<Object[]> list = new ArrayList<Object[]>();
        String sql = "";
        try {
            switch (type) {

                case 1:// 流程架构
                    sql = "select t.flow_id,t.flow_name,t.pre_flow_id,t.isflow,t.flow_id_input"
                            + "       from jecn_flow_structure t"
                            + "       where t.projectid="
                            + projectId
                            + " and t.isflow=0 and t.del_state=0 and t.data_type=0 and t.flow_name like ? order by t.pre_flow_id,t.sort_id,t.flow_id";
                    list = flowStructureDao.listNativeSql(sql, 0, 20, "%" + name + "%");
                    break;
                case 2:// 流程
                    sql = "select t.flow_id,t.flow_name,t.pre_flow_id,t.isflow,t.flow_id_input"
                            + "       from jecn_flow_structure t"
                            + "       where t.projectid="
                            + projectId
                            + " and t.isflow=1 and t.del_state=0 and t.data_type=0 and t.flow_name like ? order by t.pre_flow_id,t.sort_id,t.flow_id";
                    list = flowStructureDao.listNativeSql(sql, 0, 20, "%" + name + "%");
                    break;
                default:// 搜索流程与流程架构
                    sql = "select t.flow_id,t.flow_name,t.pre_flow_id,t.isflow,t.flow_id_input"
                            + "       from jecn_flow_structure t"
                            + "       where t.projectid="
                            + projectId
                            + " and t.del_state=0 and t.data_type=0 and t.flow_name like ? order by t.pre_flow_id,t.sort_id,t.flow_id";
                    list = flowStructureDao.listNativeSql(sql, 0, 20, "%" + name + "%");
                    break;
            }
            return findByPubListObjects(list);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * @param list
     * @return
     * @author huoyl
     * @description：流程子节点通用查询(已发布的子节点查询)
     */
    private List<JecnTreeBean> findByPubListObjects(List<Object[]> list) {
        List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
        for (Object[] obj : list) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
                continue;
            }
            JecnTreeBean jecnTreeBean = new JecnTreeBean();
            if ("0".equals(obj[3].toString())) {
                // 流程地图
                jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
            } else {
                // 流程
                jecnTreeBean.setTreeNodeType(TreeNodeType.process);
            }

            // 流程节点ID
            jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
            // 流程节点名称
            jecnTreeBean.setName(obj[1].toString());
            // 流程节点父ID
            jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
            // 流程节点父类名称
            jecnTreeBean.setPname("");
            // 流程编号
            if (obj[4] != null) {
                jecnTreeBean.setNumberId(obj[4].toString());
            } else {
                jecnTreeBean.setNumberId("");
            }
            listResult.add(jecnTreeBean);
        }
        return listResult;
    }

    /**
     * 获取单个流程操作说明
     */
    @Override
    public byte[] getProcessFile(Long flowId, boolean isPub, Long peopleId, Long projectId, boolean isAdmin)
            throws Exception {
        byte[] processFile = SelectDownloadProcess
                .getProcessFile(flowId, configItemDao, docControlDao, flowStructureDao, processBasicDao, processKPIDao,
                        processRecordDao, isPub, peopleId, projectId, isAdmin);
        JecnUserTotal userTotal = new JecnUserTotal();
        userTotal.setAccessDate(new Date());
        userTotal.setFlowId(flowId);
        userTotal.setPeopleId(peopleId);
        userTotal.setType(1);
        userTotal.setAccessFrom(1);
        flowStructureDao.getSession().save(userTotal);
        return processFile;
    }

    /**
     * 获取单个流程附件
     */
    @Override
    public Map<EXCESS, List<FileOpenBean>> getProcessExcessFile(Long flowId, boolean isPub, Long peopleId,
                                                                Long projectId, boolean isAdmin) throws Exception {
        Map<EXCESS, List<FileOpenBean>> processExcessFile = SelectDownloadProcess.getProcessExcessFile(flowId,
                flowStructureDao, fileService, isPub, peopleId, projectId, isAdmin);
        JecnUserTotal userTotal = new JecnUserTotal();
        userTotal.setAccessDate(new Date());
        userTotal.setFlowId(flowId);
        userTotal.setPeopleId(peopleId);
        userTotal.setType(1);
        userTotal.setAccessFrom(1);
        flowStructureDao.getSession().save(userTotal);
        return processExcessFile;
    }

    /**
     * 推送流程图到第三方系统
     *
     * @param flowId 流程编号 sySncType 公司类型 path 选择的路径
     * @return 0:失败 1：成功 2：不存在流程开始元素（普元专用）
     * @author chehuabo
     */
    @Override
    public int sysNcFlowProcess(Long flowId, int sySncType, String path) throws Exception {
        // 执行流程同步
        return flowSync.sysncMethd(flowId, sySncType, path);
    }

    /**
     * 流程回收站 流程树显示
     */
    @Override
    public List<JecnTreeBean> getRecycleJecnTreeBeanList(Long id, Long projectId) throws Exception {
        String sql = "";
        try {
            if (JecnContants.dbType.equals(DBType.MYSQL)) {
                sql = "select distinct t.flow_id,t.flow_name,t.pre_flow_id,t.isflow, t.flow_id_input,"
                        + " CASE WHEN jf.flow_id IS NULL THEN '0' ELSE '1' END AS is_pub,"
                        + " CASE WHEN jft.flow_id is not null THEN '1' ELSE '0'END AS child_count,"
                        + " t.del_state,t.sort_id" + " from jecn_flow_structure_t t"
                        + " LEFT JOIN jecn_flow_structure jf ON t.flow_id = jf.flow_id"
                        + " LEFT JOIN jecn_flow_structure_t jft ON t.flow_id = jft.pre_flow_id"
                        + " where   t.del_state <> 2 and t.projectid =?"
                        + " order by t.pre_flow_id,t.sort_id,t.flow_id";
                List<Object[]> list = fileDao.listNativeSql(sql, projectId);
                return findByListObjectsRecy(list);
            } else {
                sql = "select distinct t.flow_id,t.flow_name,t.pre_flow_id,t.isflow, t.flow_id_input,"
                        + " CASE WHEN jf.flow_id IS NULL THEN '0' ELSE '1' END AS is_pub,"
                        + " CASE WHEN jft.flow_id is not null THEN '1' ELSE '0'END AS child_count,"
                        + " t.del_state,t.sort_id" + " from jecn_flow_structure_t t"
                        + " LEFT JOIN jecn_flow_structure jf ON t.flow_id = jf.flow_id"
                        + " LEFT JOIN jecn_flow_structure_t jft ON t.flow_id = jft.pre_flow_id"
                        + " where t.del_state <> 2 and t.pre_flow_id = ? and t.projectid =?"
                        + " order by t.pre_flow_id,t.sort_id,t.flow_id";
                List<Object[]> list = fileDao.listNativeSql(sql, id, projectId);
                return findByListObjectsRecy(list);
            }

        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * 流程回收站 恢复当前节点及其子节点
     */
    @Override
    public void updateRecoverCurAndChild(List<Long> listIds, Long projectId) throws Exception {

        String sql = "";
        try {
            Set<Long> setFileTIds = new HashSet<Long>();
            if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
                sql = " WITH my_flow AS (" + "  SELECT * FROM jecn_flow_structure_t JF" + "  WHERE JF.flow_id in "
                        + JecnCommonSql.getIds(listIds) + "  UNION ALL" + "  SELECT JFF.*" + "  FROM my_flow"
                        + "  INNER JOIN jecn_flow_structure_t JFF ON my_flow.flow_id = JFF.pre_flow_id)"
                        + "  SELECT distinct flow_id FROM my_flow";
                List<Object> list = flowStructureDao.listNativeSql(sql);
                for (Object obj : list) {
                    if (obj == null) {
                        continue;
                    }
                    setFileTIds.add(Long.valueOf(obj.toString()));
                }
            } else if (JecnContants.dbType.equals(DBType.ORACLE)) {
                sql = "  select distinct JF.flow_id" + "  from jecn_flow_structure_t JF"
                        + "  CONNECT BY PRIOR JF.flow_id = JF.Pre_flow_id" + "  START WITH JF.flow_id in "
                        + JecnCommonSql.getIds(listIds);
                List<Object> list = flowStructureDao.listNativeSql(sql);
                for (Object obj : list) {
                    if (obj == null) {
                        continue;
                    }
                    setFileTIds.add(Long.valueOf(obj.toString()));
                }
            } else if (JecnContants.dbType.equals(DBType.MYSQL)) {
                sql = "select tt.flow_id,tt.pre_flow_id from  jecn_flow_structure_t  tt where tt.projectid=?";
                List<Object[]> listAll = fileDao.listNativeSql(sql, projectId);
                setFileTIds = JecnCommon.getAllChilds(listIds, listAll);
            }
            if (setFileTIds.size() > 0) {
                sql = "update jecn_flow_structure_t set del_state=0 where del_state<>2 and   flow_id in";
                List<String> listSql = JecnCommonSql.getSetSqls(sql, setFileTIds);
                for (String sqlStr : listSql) {
                    flowStructureDao.execteNative(sqlStr);
                }
                sql = "update jecn_flow_structure set del_State=0 where del_state<>2 and   flow_id in";
                listSql = JecnCommonSql.getSetSqls(sql, setFileTIds);
                for (String sqlStr : listSql) {
                    flowStructureDao.execteNative(sqlStr);
                }

                // 判断流程是否开启维护支撑文件
                if (JecnConfigTool.isArchitectureUploadFile()) {
                    List<Long> relatedIds = this.flowStructureDao.getRelatedFileIdsByFlowIds(listIds);
                    if (relatedIds.isEmpty()) {
                        return;
                    }
                    fileService.updateRecycleFileOrDirIds(relatedIds, projectId);
                }
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }

    }

    /**
     * 流程回收站 恢复当前节点
     */
    @Override
    public void updateRecoverCur(List<Long> ids, Long projectId) throws Exception {
        try {
            if (ids != null && ids.size() > 0) {
                String sql = "update jecn_flow_structure_t set del_state=0 where flow_id in"
                        + JecnCommonSql.getIds(ids);
                flowStructureDao.execteNative(sql);
                sql = "update jecn_flow_structure set del_state=0 where flow_id in" + JecnCommonSql.getIds(ids);
                flowStructureDao.execteNative(sql);

                // 判断流程是否开启维护支撑文件
                if (JecnConfigTool.isArchitectureUploadFile()) {
                    List<Long> relatedIds = this.flowStructureDao.getRelatedFileIdsByFlowIds(ids);
                    fileService.updateRecycleFileOrDirIds(relatedIds, projectId);
                }
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }

    }

    /**
     * 流程回收站 搜索
     */
    @Override
    public List<JecnTreeBean> getDelAuthorityByName(String name, Long projectId, Set<Long> fileIds, boolean isAdmin)
            throws Exception {
        String sql = " select jft.flow_id, jft.flow_name, jft.isflow,jft.pre_flow_id from jecn_flow_structure_t jft";
        if (!isAdmin && !JecnContants.dbType.equals(DBType.MYSQL)) {
            if (fileIds.size() > 0) {
                sql = sql + " INNER JOIN (" + JecnCommonSql.getChildObjectsSqlGetSingleIdFlow(fileIds)
                        + ") DESIGN_AUTH ON DESIGN_AUTH.FLOW_ID=jft.FLOW_ID";
            } else {
                return new ArrayList<JecnTreeBean>();
            }
        }
        sql = sql + " where jft.flow_name like '%" + name + "%' and jft.del_state=1 and jft.projectid=?";
        if (!isAdmin && !JecnContants.dbType.equals(DBType.MYSQL)) {
            if (fileIds.size() > 0) {
                sql = sql + " and jft.FLOW_ID not in " + JecnCommonSql.getIdsSet(fileIds);
            }
        }
        List<Object[]> objs = flowStructureDao.listNativeSql(sql, 0, 40, projectId);
        return getFlowListByObjs(objs);
    }

    /**
     * 流程回收站 搜索 BEAN 封装
     *
     * @param objs 0 id 1 name 2 isflow 3 pid
     * @return
     */
    private List<JecnTreeBean> getFlowListByObjs(List<Object[]> objs) {
        List<JecnTreeBean> flieList = new ArrayList<JecnTreeBean>();
        if (objs.size() == 0) {
            return flieList;
        }
        for (Object[] obj : objs) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null) {
                continue;
            }
            JecnTreeBean fileT = new JecnTreeBean();
            fileT.setId(Long.valueOf(obj[0].toString()));
            fileT.setName(obj[1].toString());
            if (Integer.parseInt(obj[2].toString()) == 1) {// 流程

                fileT.setTreeNodeType(TreeNodeType.process);
            } else {
                fileT.setTreeNodeType(TreeNodeType.processMap);
            }

            fileT.setPid(Long.valueOf(obj[3].toString()));
            flieList.add(fileT);
        }
        return flieList;
    }

    /**
     * 流程回收站 定位方法
     */
    @Override
    public List<JecnTreeBean> getPnodesRecy(Long id, Long projectId) throws Exception {
        JecnFlowStructureT jecnFlowStructureT = flowStructureDao.get(id);
        Set<Long> setIds = JecnCommon.getPositionTreeIds(jecnFlowStructureT.gettPath(), jecnFlowStructureT.getFlowId());
        String sql = "SELECT DISTINCT ST.FLOW_ID,ST.FLOW_NAME,ST.PRE_FLOW_ID,ST.ISFLOW,ST.FLOW_ID_INPUT,CASE WHEN S.FLOW_ID IS NULL THEN '0' ELSE '1' END AS S_FLOW_ID"
                + " ,CASE WHEN JFST.FLOW_ID IS NULL THEN '0' ELSE '1' END AS child_count,ST.DEL_STATE,ST.SORT_ID"
                + " FROM JECN_FLOW_STRUCTURE_T ST"
                + " LEFT JOIN JECN_FLOW_STRUCTURE S ON ST.FLOW_ID = S.FLOW_ID"
                + " LEFT JOIN JECN_FLOW_STRUCTURE_T JFST ON ST.FLOW_ID = JFST.PRE_FLOW_ID"
                + " WHERE ST.DATA_TYPE=0 AND ST.PROJECTID=?"
                + " AND ST.PRE_FLOW_ID in "
                + JecnCommonSql.getIdsSet(setIds) + " ORDER BY ST.PRE_FLOW_ID,ST.SORT_ID,ST.FLOW_ID";
        List<Object[]> list = flowStructureDao.listNativeSql(sql, projectId);
        return findByListObjectsRecy(list);
    }

    /**
     * @param list
     * @return
     * @author zhangchen Apr 28, 2012
     * @description：流程子节点通用查询
     */
    private List<JecnTreeBean> findByListObjectsRecy(List<Object[]> list) {
        List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
        for (Object[] obj : list) {
            if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[5] == null
                    || obj[6] == null || obj[7] == null) {
                continue;
            }
            JecnTreeBean jecnTreeBean = new JecnTreeBean();
            if ("0".equals(obj[3].toString())) {
                // 流程地图
                jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
            } else {
                // 流程
                jecnTreeBean.setTreeNodeType(TreeNodeType.process);
            }

            // 流程节点ID
            jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
            // 流程节点名称
            jecnTreeBean.setName(obj[1].toString());
            // 流程节点父ID
            jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));
            // 流程节点父类名称
            jecnTreeBean.setPname("");
            // 流程编号
            if (obj[4] != null) {
                jecnTreeBean.setNumberId(obj[4].toString());
            } else {
                jecnTreeBean.setNumberId("");
            }
            // 流程是否发布
            if ("0".equals(obj[5].toString())) {
                jecnTreeBean.setPub(false);
            } else {
                jecnTreeBean.setPub(true);
            }
            // 流程是否有子节点
            if (Integer.parseInt(obj[6].toString()) > 0) {

                jecnTreeBean.setChildNode(true);
            } else {
                jecnTreeBean.setChildNode(false);
            }
            // 是否删除
            jecnTreeBean.setIsDelete(Integer.parseInt(obj[7].toString()));
            listResult.add(jecnTreeBean);
        }
        return listResult;
    }

    @Override
    public int[] importValidityData(File upload, Long curPeopleId, int type) throws Exception {
        // 读取本地excel数据
        FileInputStream inputStream = null;
        // 创建excel工作表
        Workbook rwb = null;

        int[] result = new int[2];
        try {

            inputStream = new FileInputStream(upload);
            // 创建excel工作表
            rwb = Workbook.getWorkbook(inputStream);

            // 从excel中读取数据，然后校验数据合法性
            ProcessExpiryMaintenanceBean maintenanceBean = checkMaintenanceData(rwb, type);

            // 导入的数据
            List<ProcessExpiryMaintenanceWebBean> importList = new ArrayList<ProcessExpiryMaintenanceWebBean>();
            // 错误的数据
            List<ProcessExpiryMaintenanceWebBean> errorList = new ArrayList<ProcessExpiryMaintenanceWebBean>();
            for (ProcessExpiryMaintenanceWebBean webBean : maintenanceBean.getExpiryMaintenanceWebs()) {

                if (webBean.isError()) {
                    errorList.add(webBean);
                } else {
                    importList.add(webBean);

                }

            }

            // 保存正确数据 循环更新每一行
            for (ProcessExpiryMaintenanceWebBean webBean : importList) {

                processBasicDao.updateExpiry(Long.valueOf(webBean.getFlowId()), Long.valueOf(webBean.getDocId()),
                        Integer.parseInt(webBean.getExpiry()), webBean.getNextScandDate(), type);

            }

            // 写出错误数据
            if (errorList.size() > 0) {
                ProcessExpiryMaintenanceBean expiryMaintenanceBean = new ProcessExpiryMaintenanceBean();
                expiryMaintenanceBean.setError(true);
                expiryMaintenanceBean.setExpiryMaintenanceWebs(errorList);
                expiryMaintenanceBean.setType(type);
                recordExpiryErrorDate(expiryMaintenanceBean, curPeopleId, type);
            } else {
                // 导入成功，删除流程维护信息的错误数据的文件
                deleteProcessExpiryErrorFile(curPeopleId, type);
            }

            // 给返回值赋值 0为导入的数据个数 1为导出的错误数据
            result[0] = importList.size();
            result[1] = errorList.size();

        } catch (Exception e) {

            throw e;

        } finally {

            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error("关闭流异常", e);
                }
            }
            if (rwb != null) {
                rwb.close();
            }
        }

        return result;
    }

    /**
     * 删除流程维护信息的错误数据的文件
     *
     * @param type
     */
    private void deleteProcessExpiryErrorFile(Long curPeopleId, int type) {
        String filePath = JecnContants.jecn_path + JecnFinal.FILE_DIR + JecnFinal.REPORT_ERROR_DIR + curPeopleId;
        if (type == 0) {
            filePath += "ProcessExpiryMaintenanceError.xls";
        } else {
            filePath += "RuleExpiryMaintenanceError.xls";
        }
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }

    /**
     * 从excel中读取数据，然后校验数据合法性
     *
     * @param type
     * @return List<ProcessExpiryMaintenanceWebBean>
     * @author huoyl
     */
    private ProcessExpiryMaintenanceBean checkMaintenanceData(Workbook rwb, int type) {

        List<ProcessExpiryMaintenanceWebBean> allList = new ArrayList<ProcessExpiryMaintenanceWebBean>();

        // 流程监护人是否显示
        boolean guardianPeopleIsShow = false;
        // 流程拟制人是否显示
        boolean artificialPersonIsShow = false;

        Sheet[] sheets = rwb.getSheets();
        Sheet contentSheet = sheets[0];
        // 判断流程拟制人和流程监护人是否存在于导入的数据中
        int cellNum = contentSheet.getColumns();
        for (int i = 1; i < cellNum; i++) {
            String columnName = getCellText(contentSheet.getCell(i, 0));
            if (type == 0) {
                if (JecnUtil.getValue("processTheGuardian").equals(columnName)) {
                    guardianPeopleIsShow = true;
                } else if (JecnUtil.getValue("processArtificialPerson").equals(columnName)) {
                    artificialPersonIsShow = true;
                }
            } else {
                if ("制度监护人".equals(columnName)) {
                    guardianPeopleIsShow = true;
                } else if ("制度拟制人".equals(columnName)) {
                    artificialPersonIsShow = true;
                }
            }
        }

        // 文控id 流程id 流程名称 流程编号 责任部门 流程责任人 流程监护人 拟稿人 发布日期 有效日期 下次审视需要完成时间
        // 第二行开始读数据
        for (int k = 1; k < contentSheet.getRows(); k++) {
            boolean isError = false;
            ProcessExpiryMaintenanceWebBean webBean = new ProcessExpiryMaintenanceWebBean();
            int i = 1;
            String docIdStr = getCellText(sheets[0].getCell(i++, k));
            // 文控id非空并且不为数字
            if (StringUtils.isEmpty(docIdStr) || !docIdStr.matches("^\\d+$")) {
                isError = true;
                webBean.setErrorInfo("文控id格式错误");
            }
            webBean.setDocId(docIdStr);

            String flowIdStr = getCellText(sheets[0].getCell(i++, k));
            // id非空并且不为数字
            if (!isError) {
                if (StringUtils.isEmpty(flowIdStr) || !docIdStr.matches("^\\d+$")) {
                    isError = true;
                    webBean.setErrorInfo("流程id格式错误");
                }
            }
            webBean.setFlowId(flowIdStr);

            // 流程名称
            String name = getCellText(sheets[0].getCell(i++, k));
            webBean.setFlowName(name);

            // 流程编号
            String flowIdInput = getCellText(sheets[0].getCell(i++, k));
            webBean.setFlowIdInput(flowIdInput);

            // 责任部门
            String dutyOrgName = getCellText(sheets[0].getCell(i++, k));
            webBean.setDutyOrgName(dutyOrgName);

            // 流程责任人
            String dutyPeopleName = getCellText(sheets[0].getCell(i++, k));
            webBean.setDutyPeopleName(dutyPeopleName);

            if (guardianPeopleIsShow) {
                // 流程监护人
                String guardianName = getCellText(sheets[0].getCell(i++, k));
                webBean.setGuardianName(guardianName);
            }

            if (artificialPersonIsShow) {
                // 拟制人
                String peopleMakeName = getCellText(sheets[0].getCell(i++, k));
                webBean.setPeopleMakeName(peopleMakeName);
            }

            SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
            dateFormate.setLenient(false);
            // 发布日期
            String pubDateStr = getCellText(sheets[0].getCell(i++, k));
            Date pubDate = null;

            /**
             * 判断日期格式和范围
             */
            String dateReg = "^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$";

            if (!isError) {

                // 发布日期为空
                if (StringUtils.isEmpty(pubDateStr)) {
                    isError = true;
                    webBean.setErrorInfo("发布日期不能为空");
                } else {
                    try {
                        if (!pubDateStr.matches(dateReg)) {
                            isError = true;
                            webBean.setErrorInfo("下次审视日期格式不正确");
                        } else {
                            pubDate = dateFormate.parse(pubDateStr);
                        }
                    } catch (Exception e) {
                        isError = true;
                        webBean.setErrorInfo("发布日期格式不正确");
                    }
                }

            }
            webBean.setPubDate(pubDateStr);
            // 版本号
            String versionId = getCellText(sheets[0].getCell(i++, k));
            webBean.setVersionId(versionId);
            // 有效期
            String expiry = getCellText(sheets[0].getCell(i++, k));
            if (!isError) {

                // 有效期
                if (StringUtils.isEmpty(expiry)) {
                    isError = true;
                    webBean.setErrorInfo("发布日期不能为空");
                } else {
                    if (!expiry.matches("^\\d+$")) {
                        isError = true;
                        webBean.setErrorInfo("有效期格式不正确 ，有效期必须是大于等于零的整数");
                    } else if (expiry.matches("^\\d+$") && (expiry.length() > 4 || Integer.parseInt(expiry) > 9000)) {
                        isError = true;
                        webBean.setErrorInfo("有效期不能超过9000个月");
                    }
                }

            }
            webBean.setExpiry(expiry);

            // 下次审视需要完成时间
            String nextScandDateStr = getCellText(sheets[0].getCell(i++, k));
            Date nextScandDate = null;
            if (!isError) {

                // 发布日期为空
                if (StringUtils.isEmpty(nextScandDateStr)) {
                    isError = true;
                    webBean.setErrorInfo("下次审视日期不能为空");
                } else {
                    try {
                        if (!nextScandDateStr.matches(dateReg)) {
                            isError = true;
                            webBean.setErrorInfo("下次审视日期格式不正确");
                        } else {
                            nextScandDate = dateFormate.parse(nextScandDateStr);
                            if (nextScandDate.getTime() < pubDate.getTime()) {
                                isError = true;
                                webBean.setErrorInfo("下次审视日期不能小于发布日期");

                            }
                        }

                    } catch (Exception e) {
                        isError = true;
                        webBean.setErrorInfo("下次审视需完成时间格式不正确");
                    }
                }
            }

            webBean.setNextScandDate(nextScandDateStr);
            webBean.setError(isError);
            allList.add(webBean);

        }

        ProcessExpiryMaintenanceBean maintenanceBean = new ProcessExpiryMaintenanceBean();
        maintenanceBean.setGuardianPeopleIsShow(guardianPeopleIsShow);
        maintenanceBean.setArtificialPersonIsShow(artificialPersonIsShow);
        maintenanceBean.setExpiryMaintenanceWebs(allList);

        return maintenanceBean;
    }

    /**
     * 写出错误数据
     *
     * @param type
     * @param ProcessExpiryMaintenanceBean expiryMaintenanceBean 错误数据的bean
     * @author huoyl
     */
    private void recordExpiryErrorDate(ProcessExpiryMaintenanceBean expiryMaintenanceBean, Long curPeopleId, int type) {
        WritableWorkbook wbookData = null;
        try {

            ProcessExpiryDownExcelService downService = new ProcessExpiryDownExcelService(expiryMaintenanceBean);
            String filePath = JecnContants.jecn_path + JecnFinal.FILE_DIR + JecnFinal.REPORT_ERROR_DIR + curPeopleId;
            if (type == 0) {
                filePath += "ProcessExpiryMaintenanceError.xls";
            } else {
                filePath += "RuleExpiryMaintenanceError.xls";
            }

            wbookData = Workbook.createWorkbook(new File(filePath));
            // 写出文件
            downService.writeExcelFile(wbookData);

            // 后续处理--------------------------------------
            wbookData.write();
            // 工作簿不关闭不会写出数据
            // workbook在调用close方法时，才向输出流中写入数据
            wbookData.close();
            wbookData = null;

        } catch (Exception e) {
            log.error("有效期维护信息导入写出错误数据异常recordExpiryErrorDate", e);
        } finally {

            if (wbookData != null) {
                try {
                    wbookData.close();
                } catch (Exception e) {
                    log.error("流程有效期信息维护导出错误数据关闭workbook出错", e);

                }
            }

        }

    }

    /**
     * 获取单元格内容
     *
     * @param cell
     * @return
     */
    private String getCellText(Cell cell) {
        if (isNullObj(cell)) {
            return null;
        } else {
            String cellContent = cell.getContents();
            if (cellContent != null) {
                return cellContent.trim();
            }
            return null;
        }
    }

    /**
     * 给定参数是否为null
     *
     * @param obj Object
     * @return
     */
    public boolean isNullObj(Object obj) {
        return (obj == null) ? true : false;
    }

    @Override
    public JecnFlowStructure findJecnFlowStructureById(Long flowId) throws Exception {

        return flowStructureDao.findJecnFlowStructureById(flowId);
    }

    @Override
    public TmpKpiShowValues getKpiShowValues(Long flowId) throws Exception {
        List<JecnFlowKpiNameT> listJecnFlowKpiNameT = getFlowKpiNameTList(flowId);
        return FlowKpiTitleAndValues.INSTANCE.getConfigKpiNameTAndValues(listJecnFlowKpiNameT, configItemDao
                .selectTableConfigItemBeanByType(1, 16));
    }

    /**
     * 根据配置获取KPI前几列数据
     *
     * @param flowKpiName
     * @param configItemBeans
     * @return
     */
    @Override
    public List<String> getConfigKpiRowValues(JecnFlowKpiName flowKpiName, List<JecnConfigItemBean> configItemBeans) {
        return FlowKpiTitleAndValues.INSTANCE.getConfigKpiRowValues(flowKpiName, configItemBeans);
    }

    @Override
    public List<String> getDesignAuthFlows(Long peopleId, Long projectId) throws Exception {
        String sql = "select flow.t_path from jecn_flow_structure_t flow,jecn_user_role jur,jecn_role_content jrc"
                + "   where jur.people_id=? and jur.role_id=jrc.role_id and jrc.type=0 and jrc.relate_id=flow.flow_id"
                + "   and flow.projectid=?";
        return this.flowStructureDao.listNativeSql(sql, peopleId, projectId);
    }

    /**
     * 安码BPM接口，插入流程版本发布记录
     *
     * @param flowId
     * @param version
     * @return 0:插入成功，-1:插入失败：1：已存在当前版本，不允许重复推送
     * @throws Exception
     */
    @Override
    public int processPushUltimus(Long flowId, String flowName, String version) throws Exception {
        if (flowId == null || StringUtils.isBlank(flowName) || StringUtils.isBlank(version)) {
            return -1;
        }
        if (validateEPROSBPM(flowId, version)) {
            return 1;
        }
        insertEPROSBPM(flowId, flowName, version);
        return 0;
    }

    /**
     * 插入当前发布版本
     *
     * @param flowId
     * @param version 发布的版本号
     * @throws Exception
     */
    private void insertEPROSBPM(Long flowId, String flowName, String version) throws Exception {
        flowStructureDao.insertEPROSBPM(flowId, flowName, version);
    }

    /**
     * 验证当前发布版本是否存在
     *
     * @param flowId
     * @param version 发布的版本号
     * @return true:已存在当前发布版本
     * @throws Exception
     */
    private boolean validateEPROSBPM(Long flowId, String version) throws Exception {
        int resultCount = flowStructureDao.validateEPROSBPM(flowId, version);
        return resultCount > 0 ? true : false;
    }

    @Override
    public void batchReleaseNotRecord(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) throws Exception {
        for (Long id : ids) {
            directRelease(id, treeNodeType, peopleId);
            // 华帝登录 写入数据至Domino平台
            if (JecnUtil.isVattiLogin()) {
                DominoOperation.importDataToDomino(id, "Flow");
            }
        }
    }

    @Override
    public void batchCancelRelease(List<Long> ids, TreeNodeType treeNodeType, Long peopleId) throws Exception {

        for (Long id : ids) {
            cancelRelease(id, treeNodeType, peopleId);
            // 华帝 取消发布 删除Domino平台数据
            if (JecnUtil.isVattiLogin()) {
                DominoOperation.deleteDataFromDomino(id, "Flow");
            }
        }
    }

    /**
     * 更新适用部门
     *
     * @param orgIds
     * @param flowId
     */
    private void updateApplyOrgs(String orgIds, Long flowId) {

        String hql = "from JecnFlowApplyOrgT where relateId=?";
        List<JecnFlowApplyOrgT> listOld = processBasicDao.listHql(hql, flowId);
        if (orgIds != null && !"".equals(orgIds)) {
            String[] orgArr = orgIds.split(",");
            for (String orgId : orgArr) {
                if (orgId != null && !"".equals(orgId)) {
                    boolean isExist = false;
                    for (JecnFlowApplyOrgT jecnFlowApplyOrgT : listOld) {
                        if (jecnFlowApplyOrgT.getOrgId().equals(Long.valueOf(orgId))) {
                            isExist = true;
                            listOld.remove(jecnFlowApplyOrgT);
                            break;
                        }
                    }
                    if (!isExist) {
                        JecnFlowApplyOrgT jecnFlowApplyOrgT = new JecnFlowApplyOrgT();
                        jecnFlowApplyOrgT.setId(JecnCommon.getUUID());
                        jecnFlowApplyOrgT.setOrgId(Long.valueOf(orgId));
                        jecnFlowApplyOrgT.setRelateId(flowId);
                        processBasicDao.getSession().save(jecnFlowApplyOrgT);
                    }

                }
            }
        }
        for (JecnFlowApplyOrgT jecnFlowApplyOrgT : listOld) {
            processBasicDao.getSession().delete(jecnFlowApplyOrgT);
        }
    }

    /**
     * 更新通知人
     *
     * @param peopleId
     * @param flowId
     */
    private void updateDriverEmailPeople(Set<Long> peopleIds, Long flowId) {
        String sql = "delete from JECN_FLOW_DRIVER_EMAIL_T where  RELATED_ID=?";
        processBasicDao.execteNative(sql, flowId);
        for (Long peopleId : peopleIds) {
            JecnFlowDriverEmailT jecnFlowDriverEmailT = new JecnFlowDriverEmailT();
            jecnFlowDriverEmailT.setId(JecnCommon.getUUID());
            jecnFlowDriverEmailT.setPeopleId(peopleId);
            jecnFlowDriverEmailT.setRelateId(flowId);
            processBasicDao.getSession().save(jecnFlowDriverEmailT);
        }
    }

    @Override
    public void updateFlowOperation(Long flowId, JecnFlowBasicInfoT jecnFlowBasicInfoT, Map<String, Object> resultMap,
                                    Long updatePeopleId) throws Exception {
        try {
            updateFlowModifyRecord(flowId, updatePeopleId, 6);
            JecnFlowBasicInfoT jecnFlowBasicInfoTOld = this.processBasicDao.get(flowId);
            // 自定义要素1
            jecnFlowBasicInfoTOld.setFlowCustomOne(jecnFlowBasicInfoT.getFlowCustomOne());
            // 自定义要素2
            jecnFlowBasicInfoTOld.setFlowCustomTwo(jecnFlowBasicInfoT.getFlowCustomTwo());
            // 自定义要素3
            jecnFlowBasicInfoTOld.setFlowCustomThree(jecnFlowBasicInfoT.getFlowCustomThree());
            // 自定义要素4
            jecnFlowBasicInfoTOld.setFlowCustomFour(jecnFlowBasicInfoT.getFlowCustomFour());
            // 自定义要素5
            jecnFlowBasicInfoTOld.setFlowCustomFive(jecnFlowBasicInfoT.getFlowCustomFive());
            // 目的
            jecnFlowBasicInfoTOld.setFlowPurpose(jecnFlowBasicInfoT.getFlowPurpose());
            // 适用范围
            jecnFlowBasicInfoTOld.setApplicability(jecnFlowBasicInfoT.getApplicability());
            // 术语定义
            jecnFlowBasicInfoTOld.setNoutGlossary(jecnFlowBasicInfoT.getNoutGlossary());
            // 驱动类型，驱动规则
            jecnFlowBasicInfoTOld.setDriveType(jecnFlowBasicInfoT.getDriveType());
            jecnFlowBasicInfoTOld.setDriveRules(jecnFlowBasicInfoT.getDriveRules());
            // 流程输入、流程输出
            jecnFlowBasicInfoTOld.setInput(jecnFlowBasicInfoT.getInput());
            jecnFlowBasicInfoTOld.setOuput(jecnFlowBasicInfoT.getOuput());
            // 流程边界--起始活动
            jecnFlowBasicInfoTOld.setFlowStartpoint(jecnFlowBasicInfoT.getFlowStartpoint());
            // 流程边界-终止活动
            jecnFlowBasicInfoTOld.setFlowEndpoint(jecnFlowBasicInfoT.getFlowEndpoint());
            // 流程客户
            jecnFlowBasicInfoTOld.setFlowCustom(jecnFlowBasicInfoT.getFlowCustom());
            // 不适用范围
            jecnFlowBasicInfoTOld.setNoApplicability(jecnFlowBasicInfoT.getNoApplicability());
            // 概述
            jecnFlowBasicInfoTOld.setFlowSummarize(jecnFlowBasicInfoT.getFlowSummarize());
            // 补充说明
            jecnFlowBasicInfoTOld.setFlowSupplement(jecnFlowBasicInfoT.getFlowSupplement());
            this.processBasicDao.update(jecnFlowBasicInfoTOld);
            if (resultMap.get("a2") != null) {
                // 适用范围
                String applyOrgIds = (String) resultMap.get("a2");
                this.updateApplyOrgs(applyOrgIds, flowId);
            }

            if (resultMap.get("a4") != null) {
                // 驱动规则
                JecnFlowDriverT jecnFlowDriverT = (JecnFlowDriverT) resultMap.get("a4");
                processBasicDao.getSession().saveOrUpdate(jecnFlowDriverT);
                this.updateDriverEmailPeople(jecnFlowDriverT.getDriverEmailPeopleIds(), flowId);
                // 删除时间
                String hql = "delete from JecnFlowDriverTimeT where flowId=?";
                processBasicDao.execteHql(hql, flowId);
                if (JecnUtil.isNotEmpty(jecnFlowDriverT.getTimes())) {
                    for (JecnFlowDriverTimeT time : jecnFlowDriverT.getTimes()) {
                        processBasicDao.getSession().save(time);
                    }
                }
            }

            if (resultMap.get("a20") != null) {

                List<JecnFlowRecordT> listJecnFlowRecordT = (List<JecnFlowRecordT>) resultMap.get("a20");
                // 流程记录
                List<JecnFlowRecordT> listJecnFlowRecordTBefore = processRecordDao
                        .getJecnFlowRecordTListByFlowId(flowId);
                for (JecnFlowRecordT jecnFlowRecordT : listJecnFlowRecordT) {
                    boolean isExist = false;
                    if (listJecnFlowRecordTBefore != null) {
                        for (JecnFlowRecordT flowRecordT : listJecnFlowRecordTBefore) {
                            if (jecnFlowRecordT.getId() != null && jecnFlowRecordT.getId().equals(flowRecordT.getId())) {
                                isExist = true;
                                boolean isModify = false;
                                if (jecnFlowRecordT.getRecordName() != null
                                        && !jecnFlowRecordT.getRecordName().equals(flowRecordT.getRecordName())) {
                                    isModify = true;
                                    flowRecordT.setRecordName(jecnFlowRecordT.getRecordName());
                                }
                                if (jecnFlowRecordT.getDocId() != null
                                        && !jecnFlowRecordT.getDocId().equals(flowRecordT.getDocId())) {
                                    isModify = true;
                                    flowRecordT.setDocId(jecnFlowRecordT.getDocId());
                                }
                                if (jecnFlowRecordT.getRecordTransferPeople() != null
                                        && !jecnFlowRecordT.getRecordTransferPeople().equals(
                                        flowRecordT.getRecordTransferPeople())) {
                                    isModify = true;
                                    flowRecordT.setRecordTransferPeople(jecnFlowRecordT.getRecordTransferPeople());
                                }
                                if (jecnFlowRecordT.getRecordApproach() != null
                                        && !jecnFlowRecordT.getRecordApproach().equals(flowRecordT.getRecordApproach())) {
                                    isModify = true;
                                    flowRecordT.setRecordApproach(jecnFlowRecordT.getRecordApproach());
                                }

                                if (jecnFlowRecordT.getRecordSavePeople() != null
                                        && !jecnFlowRecordT.getRecordSavePeople().equals(
                                        flowRecordT.getRecordSavePeople())) {
                                    isModify = true;
                                    flowRecordT.setRecordSavePeople(jecnFlowRecordT.getRecordSavePeople());
                                }
                                if (jecnFlowRecordT.getSaveLaction() != null
                                        && !jecnFlowRecordT.getSaveLaction().equals(flowRecordT.getSaveLaction())) {
                                    isModify = true;
                                    flowRecordT.setSaveLaction(jecnFlowRecordT.getSaveLaction());
                                }
                                if (jecnFlowRecordT.getSaveTime() != null
                                        && !jecnFlowRecordT.getSaveTime().equals(flowRecordT.getSaveTime())) {
                                    isModify = true;
                                    flowRecordT.setSaveTime(jecnFlowRecordT.getSaveTime());
                                }
                                if (jecnFlowRecordT.getFileTime() != null
                                        && !jecnFlowRecordT.getFileTime().equals(flowRecordT.getFileTime())) {
                                    isModify = true;
                                    flowRecordT.setFileTime(jecnFlowRecordT.getFileTime());
                                }
                                if (!isModify) {
                                    processRecordDao.update(flowRecordT);
                                }
                                listJecnFlowRecordTBefore.remove(flowRecordT);
                                break;
                            }
                        }
                    }
                    if (!isExist) {
                        processRecordDao.save(jecnFlowRecordT);
                    }
                }
                // 删除多余的流程记录
                for (JecnFlowRecordT flowRecordT : listJecnFlowRecordTBefore) {
                    processRecordDao.deleteObject(flowRecordT);
                }
            }

            // 相关制度
            if (resultMap.get("a14") != null) {
                List<Long> ids = (List<Long>) resultMap.get("a14");
                saveOrUpdateRealtedRules(flowId, ids);
            }

            // 相关标准
            if (resultMap.get("a21") != null) {
                List<Long> ids = (List<Long>) resultMap.get("a21");
                // 相关标准
                saveOrUpdateRelatedStandards(flowId, ids);
            }

            // 流程相关标准文件
            if (resultMap.get("a37") != null) {
                List<Long> ids = (List<Long>) resultMap.get("a37");
                saveOrUpdateRelatedStandardizedFiles(flowId, ids);
            }

            if (resultMap.get("a39") != null) {
                List<Map<String, Object>> inouts = (List<Map<String, Object>>) resultMap.get("a39");
                saveOrUpdateNewInout(0, inouts, flowId);
            }

            if (resultMap.get("a40") != null) {
                List<Map<String, Object>> inouts = (List<Map<String, Object>>) resultMap.get("a40");
                saveOrUpdateNewInout(1, inouts, flowId);
            }

        } catch (Exception e) {
            log.error("", e);
            throw e;
        }

    }

    private void saveOrUpdateNewInout(int type, List<Map<String, Object>> inouts, Long flowId)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Set<String> oldInout = new HashSet<String>();
        // 查询数据库的数据
        String a = "from JecnFlowInoutT where flowId=? and type=?";
        List<JecnFlowInoutT> oldInouts = flowStructureDao.listHql(a, flowId, type);
        Map<String, JecnFlowInoutT> map = new HashMap<String, JecnFlowInoutT>();
        if (oldInouts != null && oldInouts.size() > 0) {
            for (JecnFlowInoutT flow : oldInouts) {
                oldInout.add(flow.getId());
                map.put(flow.getId(), flow);
            }
        }
        Session s = flowStructureDao.getSession();
        Set<String> newInout = new HashSet<String>();
        for (Map<String, Object> m : inouts) {
            JecnFlowInoutBase base = (JecnFlowInoutBase) m.get("inout");
            base.setFlowId(flowId);
            Set<Long> poses = (Set<Long>) m.get("pos");
            Set<Long> posGroups = (Set<Long>) m.get("posGroup");
            if (StringUtils.isNotBlank(base.getId())) {// 更新
                JecnFlowInoutT inout = map.get(base.getId());
                PropertyUtils.copyProperties(inout, base);
                newInout.add(inout.getId());
                s.update(inout);
                // 更新保存岗位岗位组关联表
                saveOrUpdateInoutPos(flowId, 0, poses, inout.getId());
                saveOrUpdateInoutPos(flowId, 1, posGroups, inout.getId());
            } else {
                JecnFlowInoutT inout = new JecnFlowInoutT();
                PropertyUtils.copyProperties(inout, base);
                s.save(inout);
                saveInoutPos(flowId, 0, poses, inout.getId());
                saveInoutPos(flowId, 1, posGroups, inout.getId());
            }
        }

        // 删除
        oldInout.removeAll(newInout);
        if (oldInout.size() > 0) {
            String hql = "delete from JecnFlowInoutT where id=?";
            for (String id : oldInout) {
                flowStructureDao.execteHql(hql, id);
            }
        }
    }

    private void saveInoutPos(Long flowId, int rType, Set<Long> rIds, String inoutId) {
        if (rIds == null || rIds.size() == 0) {
            return;
        }
        for (Long rId : rIds) {
            JecnFlowInoutPosT p = new JecnFlowInoutPosT();
            p.setInoutId(inoutId);
            p.setrId(rId);
            p.setrType(rType);
            p.setFlowId(flowId);
            processRecordDao.getSession().save(p);
        }
    }

    private void saveOrUpdateInoutPos(Long flowId, int rType, Set<Long> newrIds, String inoutId) {
        if (newrIds == null) {
            newrIds = new HashSet<Long>();
        }
        String hql = "from JecnFlowInoutPosT where inoutId=? and rType=?";
        List<JecnFlowInoutPosT> inouts = flowStructureDao.listHql(hql, inoutId, rType);
        Set<Long> oldrIds = new HashSet<Long>();
        for (JecnFlowInoutPosT r : inouts) {
            oldrIds.add(r.getrId());
        }
        // 新增
        Set<Long> temp1 = new HashSet<Long>(newrIds);
        Set<Long> temp2 = new HashSet<Long>(oldrIds);
        temp1.removeAll(temp2);
        for (Long rId : temp1) {
            JecnFlowInoutPosT f = new JecnFlowInoutPosT();
            f.setInoutId(inoutId);
            f.setrId(rId);
            f.setrType(rType);
            f.setFlowId(flowId);
            flowStructureDao.getSession().save(f);
            flowStructureDao.getSession().flush();
        }
        // 删除
        temp1 = new HashSet<Long>(newrIds);
        temp2 = new HashSet<Long>(oldrIds);
        temp2.removeAll(temp1);
        String delHql = "delete from JecnFlowInoutPosT where rId=? and rType=?";
        for (Long rId : temp2) {
            flowStructureDao.execteHql(delHql, rId, rType);
        }
    }

    private void saveOrUpdateRealtedRules(Long flowId, List<Long> ids) {
        String hql = "from FlowCriterionT where flowId=?";
        List<FlowCriterionT> listFlowCriterionT = processBasicDao.listHql(hql, flowId);

        for (Long id : ids) {
            boolean isExist = false;
            for (FlowCriterionT flowCriterionT : listFlowCriterionT) {
                if (id.equals(flowCriterionT.getCriterionClassId())) {
                    listFlowCriterionT.remove(flowCriterionT);
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                FlowCriterionT flowCriterionT = new FlowCriterionT();
                flowCriterionT.setFlowId(flowId);
                flowCriterionT.setCriterionClassId(id);
                processBasicDao.getSession().save(flowCriterionT);
            }
        }
        for (FlowCriterionT flowCriterionT : listFlowCriterionT) {
            processBasicDao.getSession().delete(flowCriterionT);
        }
    }

    private void saveOrUpdateRelatedStandards(Long flowId, List<Long> ids) {
        String hql = "from StandardFlowRelationTBean where flowId=?";
        List<StandardFlowRelationTBean> listStandardFlowRelationTBean = processBasicDao.listHql(hql, flowId);
        for (Long id : ids) {
            boolean isExist = false;
            for (StandardFlowRelationTBean standardFlowRelationTBean : listStandardFlowRelationTBean) {
                if (id.equals(standardFlowRelationTBean.getCriterionClassId())) {
                    isExist = true;
                    listStandardFlowRelationTBean.remove(standardFlowRelationTBean);
                    break;
                }
            }
            if (!isExist) {
                StandardFlowRelationTBean standardFlowRelationTBean = new StandardFlowRelationTBean();
                standardFlowRelationTBean.setFlowId(flowId);
                standardFlowRelationTBean.setCriterionClassId(id);
                processBasicDao.getSession().save(standardFlowRelationTBean);
            }
        }
        for (StandardFlowRelationTBean standardFlowRelationTBean : listStandardFlowRelationTBean) {
            processBasicDao.getSession().delete(standardFlowRelationTBean);
        }
    }

    /**
     * 流程相关风险
     *
     * @param flowId
     * @param ids
     */
    private void saveOrUpdateRelatedRisks(Long flowId, List<Long> ids) {
        String hql = "from JecnFlowRelatedRiskT where flowId=?";
        List<JecnFlowRelatedRiskT> listRisk = flowStructureDao.listHql(hql, flowId);
        for (Long id : ids) {
            boolean isExist = false;
            for (JecnFlowRelatedRiskT relatedRisk : listRisk) {
                if (id.equals(relatedRisk.getRiskId())) {
                    isExist = true;
                    listRisk.remove(relatedRisk);
                    break;
                }
            }
            if (!isExist) {
                JecnFlowRelatedRiskT relatedRisk = new JecnFlowRelatedRiskT();
                relatedRisk.setFlowId(flowId);
                relatedRisk.setRiskId(id);
                relatedRisk.setId(JecnCommon.getUUID());
                flowStructureDao.getSession().save(relatedRisk);
            }
        }
        for (JecnFlowRelatedRiskT relatedRisk : listRisk) {
            flowStructureDao.getSession().delete(relatedRisk);
        }
    }

    /**
     * 相关标准化文件
     *
     * @param flowId
     * @param ids
     */
    private void saveOrUpdateRelatedStandardizedFiles(Long flowId, List<Long> ids) {
        String hql = "from FlowStandardizedFileT where flowId=?";
        List<FlowStandardizedFileT> listStandardizedFile = flowStructureDao.listHql(hql, flowId);
        for (Long id : ids) {
            boolean isExist = false;
            for (FlowStandardizedFileT standardizedFile : listStandardizedFile) {
                if (id.equals(standardizedFile.getFileId())) {
                    isExist = true;
                    listStandardizedFile.remove(standardizedFile);
                    break;
                }
            }
            if (!isExist) {
                FlowStandardizedFileT standardizedFile = new FlowStandardizedFileT();
                standardizedFile.setFlowId(flowId);
                standardizedFile.setFileId(id);
                standardizedFile.setId(JecnCommon.getUUID());
                flowStructureDao.getSession().save(standardizedFile);
            }
        }
        for (FlowStandardizedFileT standardizedFile : listStandardizedFile) {
            flowStructureDao.getSession().delete(standardizedFile);
        }
    }

    /**
     * 流程架构卡
     */
    @Override
    public ProcessArchitectureDescriptionBean getProcessArchitectureDescriptionBean(Long flowId) throws Exception {
        ProcessArchitectureDescriptionBean processCord = new ProcessArchitectureDescriptionBean();
        JecnFlowStructureT jecnFlowStructureT = flowStructureDao.get(flowId);
        processCord.setFlowId(flowId);
        processCord.setFlowName(jecnFlowStructureT.getFlowName());
        processCord.setNumId(jecnFlowStructureT.getFlowIdInput());
        processCord.setLevel(jecnFlowStructureT.gettLevel());
        processCord.setJecnMainFlowT(this.getFlowMapInfo(flowId));
        processCord.setKeyWord(jecnFlowStructureT.getKeyword());
        // 上一级流程
        if (jecnFlowStructureT.getPerFlowId().intValue() != 0) {
            JecnFlowStructureT pubFlow = flowStructureDao.get(jecnFlowStructureT.getPerFlowId());
            JecnTreeBean jecnTreeBean = new JecnTreeBean();
            jecnTreeBean.setId(pubFlow.getFlowId());
            jecnTreeBean.setName(pubFlow.getFlowName());
            if (pubFlow.getIsFlow().intValue() == 0) {
                jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
            } else {
                jecnTreeBean.setTreeNodeType(TreeNodeType.process);
            }
            processCord.setPubBean(jecnTreeBean);
        }
        // 下一級流程
        String sql = "select t.flow_id,t.flow_name,t.isflow from jecn_flow_structure_t t where t.DEL_STATE = 0 and  t.pre_flow_id="
                + flowId + " order by t.sort_id";
        List<Object[]> listObjs = flowStructureDao.listNativeSql(sql);
        List<JecnTreeBean> sublists = new ArrayList<JecnTreeBean>();
        for (Object[] obj : listObjs) {
            if (obj[0] == null || obj[1] == null || obj[2] == null) {
                continue;
            }
            JecnTreeBean jecnTreeBean = new JecnTreeBean();
            jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
            jecnTreeBean.setName(obj[1].toString());
            if ("0".equals(obj[2].toString())) {
                jecnTreeBean.setTreeNodeType(TreeNodeType.processMap);
            } else {
                jecnTreeBean.setTreeNodeType(TreeNodeType.process);
            }
            sublists.add(jecnTreeBean);
        }
        processCord.setSubList(sublists);
        // 责任部门
        sql = "SELECT JFO.ORG_ID,JFO.ORG_NAME FROM JECN_FLOW_STRUCTURE_T JFS, JECN_FLOW_RELATED_ORG_T T,JECN_FLOW_ORG JFO"
                + "  WHERE JFS.FLOW_ID = T.FLOW_ID AND T.ORG_ID = JFO.ORG_ID AND JFO.DEL_STATE=0 AND JFS.FLOW_ID="
                + flowId;
        listObjs = flowStructureDao.listNativeSql(sql);
        if (listObjs.size() == 1) {
            Object[] obj = listObjs.get(0);
            if (obj[0] != null && obj[1] != null) {
                processCord.setDutyOrgId(Long.valueOf(obj[0].toString()));
                processCord.setDutyOrgName(obj[1].toString());
            }
        }

        // 责任人
        if (processCord.getJecnMainFlowT() != null && processCord.getJecnMainFlowT().getResPeopleId() != null
                && processCord.getJecnMainFlowT().getTypeResPeople() != null) {
            int resType = processCord.getJecnMainFlowT().getTypeResPeople();
            if (resType == 0) {
                sql = "select ju.people_id,ju.true_name from jecn_user ju where ju.people_id" + "="
                        + processCord.getJecnMainFlowT().getResPeopleId() + " and ju.islock=0";
            } else {
                sql = "SELECT JFOI.FIGURE_ID, JFOI.FIGURE_TEXT, JFO.ORG_NAME"
                        + "  FROM JECN_FLOW_ORG JFO, JECN_FLOW_ORG_IMAGE JFOI" + " WHERE JFOI.ORG_ID = JFO.ORG_ID"
                        + "   AND JFO.DEL_STATE = 0" + "   AND JFOI.FIGURE_ID = "
                        + processCord.getJecnMainFlowT().getResPeopleId();
            }
            processCord.setDutyUserType(resType);
            listObjs = flowStructureDao.listNativeSql(sql);
            if (listObjs.size() == 1) {
                Object[] obj = listObjs.get(0);
                if (obj[0] != null && obj[1] != null) {
                    processCord.setDutyUserId(Long.valueOf(obj[0].toString()));
                    processCord.setDutyUserName(obj[1].toString());
                }
            }
        }

        // 专员
        if (jecnFlowStructureT.getCommissionerId() != null) {
            sql = "select ju.people_id,ju.true_name from jecn_user ju where ju.islock=0 and ju.people_id="
                    + jecnFlowStructureT.getCommissionerId();
            listObjs = flowStructureDao.listNativeSql(sql);
            if (listObjs.size() == 1) {
                Object[] obj = listObjs.get(0);
                if (obj[0] != null && obj[1] != null) {
                    processCord.setCommissionerId(Long.valueOf(obj[0].toString()));
                    processCord.setCommissionerName(obj[1].toString());
                }
            }

        }
        return processCord;
    }

    /**
     * 更新架构卡
     */
    @Override
    public void updateFlowMap(ProcessArchitectureDescriptionBean processCord) throws Exception {
        Long flowId = processCord.getFlowId();
        JecnFlowStructureT jecnFlowStructureT = flowStructureDao.get(flowId);
        jecnFlowStructureT.setFlowIdInput(processCord.getNumId());
        jecnFlowStructureT.setUpdateDate(new Date());
        jecnFlowStructureT.setKeyword(processCord.getKeyWord());
        // 专员
        jecnFlowStructureT.setCommissionerId(processCord.getCommissionerId());
        jecnFlowStructureT.setUpdateDate(new Date());
        jecnFlowStructureT.setUpdatePeopleId(processCord.getUpdatePeopleId());
        flowStructureDao.update(jecnFlowStructureT);
        JecnMainFlowT mf = (JecnMainFlowT) flowStructureDao.getSession().get(JecnMainFlowT.class, flowId);
        if (mf == null) {
            processCord.getJecnMainFlowT().setTypeResPeople(0);
            if (processCord.getDutyUserId() != null) {
                processCord.getJecnMainFlowT().setTypeResPeople(processCord.getDutyUserType());
                processCord.getJecnMainFlowT().setResPeopleId(processCord.getDutyUserId());
            } else {
                processCord.getJecnMainFlowT().setResPeopleId(null);
            }
            flowStructureDao.getSession().save(processCord.getJecnMainFlowT());
        } else {
            mf.setFlowAim(processCord.getJecnMainFlowT().getFlowAim());
            mf.setFlowDescription(processCord.getJecnMainFlowT().getFlowDescription());
            mf.setFlowInput(processCord.getJecnMainFlowT().getFlowInput());
            mf.setFlowOutput(processCord.getJecnMainFlowT().getFlowOutput());
            mf.setFlowStart(processCord.getJecnMainFlowT().getFlowStart());
            mf.setFlowEnd(processCord.getJecnMainFlowT().getFlowEnd());
            mf.setFlowKpi(processCord.getJecnMainFlowT().getFlowKpi());
            mf.setSupplier(processCord.getJecnMainFlowT().getSupplier());
            mf.setCustomer(processCord.getJecnMainFlowT().getCustomer());
            mf.setRiskAndOpportunity(processCord.getJecnMainFlowT().getRiskAndOpportunity());
            mf.setTypeResPeople(0);
            if (processCord.getDutyUserId() != null) {
                mf.setTypeResPeople(processCord.getDutyUserType());
                mf.setResPeopleId(processCord.getDutyUserId());
            } else {
                mf.setResPeopleId(null);
            }
            flowStructureDao.getSession().update(mf);
        }

        // 責任部門
        String sql = "SELECT JFO.ORG_ID,JFO.ORG_NAME FROM JECN_FLOW_STRUCTURE_T JFS, JECN_FLOW_RELATED_ORG_T T,JECN_FLOW_ORG JFO"
                + "  WHERE JFS.FLOW_ID = T.FLOW_ID AND T.ORG_ID = JFO.ORG_ID AND JFO.DEL_STATE=0 AND JFS.FLOW_ID="
                + flowId;
        List<Object[]> listObjs = flowStructureDao.listNativeSql(sql);
        if (listObjs.size() == 1) {
            if (processCord.getDutyOrgId() == null) {
                sql = "delete from JECN_FLOW_RELATED_ORG_T where flow_id=" + flowId;
                flowStructureDao.execteNative(sql);
                return;
            }
            Object[] obj = listObjs.get(0);
            if (!processCord.getDutyOrgId().equals(Long.valueOf(obj[0].toString()))) {
                sql = "delete from JECN_FLOW_RELATED_ORG_T where flow_id=" + flowId;
                flowStructureDao.execteNative(sql);
                FlowOrgT flowOrgT = new FlowOrgT();
                flowOrgT.setFlowId(flowId);
                flowOrgT.setOrgId(processCord.getDutyOrgId());
                flowStructureDao.getSession().save(flowOrgT);
                return;
            }
        } else {
            if (processCord.getDutyOrgId() == null) {
                return;
            }
            FlowOrgT flowOrgT = new FlowOrgT();
            flowOrgT.setFlowId(flowId);
            flowOrgT.setOrgId(processCord.getDutyOrgId());
            flowStructureDao.getSession().save(flowOrgT);
        }

        JecnUtil.saveJecnJournal(jecnFlowStructureT.getFlowId(), jecnFlowStructureT.getFlowName(), 1, 2,
                jecnFlowStructureT.getUpdatePeopleId(), configItemDao);
    }

    @Override
    public List<ProcessInterface> findFlowInterfaces(Long id, boolean isPub) throws Exception {
        return flowStructureDao.findFlowInterfaces(id, isPub);
    }

    @Override
    public String excelImporteFlowKpiImage(Long flowId, Long kpiAndId, File upload, String kpiHorType,
                                           String startTime, String endTime, Long loginUserId) {
        // 读取本地excel数据
        FileInputStream inputStream = null;
        // 创建excel工作表
        Workbook rwb = null;
        List<JecnFlowKpi> jecnFlowKpiList = new ArrayList<JecnFlowKpi>();

        try {

            inputStream = new FileInputStream(upload);
            // 创建excel工作表
            rwb = Workbook.getWorkbook(inputStream);
            Sheet[] sheets = rwb.getSheets();
            Sheet contentSheet = sheets[0];
            // 第二行开始读数据
            for (int k = 1; k < contentSheet.getRows(); k++) {
                // KPI日期
                Date kpiHorVlaue = null;
                Cell c = sheets[0].getCell(0, k);
                // 判断如果日期的内容开始为空那么直接跳出循环
                if (JecnCommon.isNullOrEmtryTrim(getCellText(c))) {
                    break;
                }

                // 判断日期类型是否合法
                if (c.getType() == CellType.DATE) {// 手动填写模板文件时为 date
                    // 类型，其他情况有可能不是date类型
                    try {
                        DateCell dc = (DateCell) c;
                        kpiHorVlaue = dc.getDate();
                    } catch (Exception e) {
                        JecnUtil.getValue("inputDateFormateError");// 日期格式错误
                    }
                } else {
                    String kpiHorVlaueStr = getCellText(contentSheet.getCell(0, k));
                    // kpi统计类型 0：月 1：季度
                    if ("0".equals(kpiHorType)) {// 月份
                        // 日期判断如果不是日期类型返回数据错误提示
                        if (!kpiHorVlaueStr
                                .matches("^([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]|[0-9][1-9][0-9]{2}|[1-9][0-9]{3})-([1-9]|0[1-9]|1[012])$")) {
                            // 月份只能输入
                            return JecnUtil.getValue("inputDateFormateError");
                        }

                    } else if ("1".equals(kpiHorType)) {// 季度
                        // 日期判断如果不是日期类型返回数据错误提示
                        if (!kpiHorVlaueStr
                                .matches("^([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]|[0-9][1-9][0-9]{2}|[1-9][0-9]{3})-([369]|0[369]|1[2])$")) {
                            // 日期格式错误
                            return JecnUtil.getValue("seasonCountMonthOnlyCanInput");
                        }

                    }

                    try {

                        kpiHorVlaue = JecnCommon.getDateByString(kpiHorVlaueStr, "yyyy-MM");
                        // 判断日期是不是在开始与结束日期范围之内

                        Calendar startCal = getCalendar(startTime);
                        Calendar endCal = getCalendar(endTime);
                        Calendar kpiHorVlaueCal = getCalendar(kpiHorVlaueStr);
                        if (kpiHorVlaueCal.before(startCal) || endCal.before(kpiHorVlaueCal)) {
                            // 填写的日期不在时间段之内！
                            return JecnUtil.getValue("inputDateNotIn");

                        }

                    } catch (Exception e) {
                        // 日期格式错误
                        return JecnUtil.getValue("inputDateFormateError");
                    }
                }

                // 判断内容是否合法
                String kpiValue = getCellText(contentSheet.getCell(1, k));
                if (!JecnCommon.isNullOrEmtryTrim(kpiValue)) {

                    // 判断值的类型是否是正确的
                    // 判断是否是正整形或小数
                    if (!kpiValue.matches("^[\\d]+[\\.]?[\\d]{1,2}+|[\\d]+$")) {
                        // 只能输入正整数或小数(精确到小数点后两位)！
                        return JecnUtil.getValue("onlyInputTwoDecimalPlaces");
                    }

                    // 可以转换
                    double kpiValueNum = Double.valueOf(kpiValue);
                    if (kpiValueNum > 1000000000) {
                        // 不能大于1000000000！
                        return JecnUtil.getValue("inputCannotLargeThan") + "1000000000！";
                    } else if (kpiValueNum < 0) {
                        // 不能小于0！
                        return JecnUtil.getValue("inputCannotLessThan") + "0！";

                    }

                    String[] vArray = kpiValue.split("\\.");
                    if (vArray != null && vArray.length >= 1) {
                        String value0 = vArray[0];
                        if (value0 != null && value0.matches("^0+$")) {
                            if (vArray.length == 2) {
                                kpiValue = "0." + vArray[1];
                            } else {
                                kpiValue = "0";
                            }
                        }
                    }

                }

                // 将Excel中的数据存入数据库
                JecnFlowKpi jecnFlowKpi = new JecnFlowKpi();
                jecnFlowKpi.setFlowId(flowId);
                jecnFlowKpi.setKpiAndId(kpiAndId);
                jecnFlowKpi.setKpiValue(kpiValue);
                jecnFlowKpi.setKpiHorVlaue(kpiHorVlaue);
                jecnFlowKpi.setCreateTime(new Date());
                jecnFlowKpi.setCreatePeopleId(loginUserId);
                jecnFlowKpi.setUpdateTime(new Date());
                jecnFlowKpi.setUpdatePeopleId(loginUserId);
                jecnFlowKpiList.add(jecnFlowKpi);
            }
            if (jecnFlowKpiList.size() > 0) {
                this.addKPIVaules(kpiAndId, jecnFlowKpiList, loginUserId, flowId);
                return "SUCCESS";// 导入成功
            } else {
                // 内容不能为空
                return JecnUtil.getValue("inputCannotNull");
            }

        } catch (Exception e) {
            log.error("KPI值导入异常", e);
        } finally {

            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error("关闭流异常", e);
                }
            }
            if (rwb != null) {
                rwb.close();
            }
        }
        return null;
    }

    /**
     * 根据输入的日期返回一个日历对象(忽略天)
     *
     * @param dateStr String yyyy-MM-dd
     * @return
     */
    private Calendar getCalendar(String dateStr) {
        String[] dateArray = dateStr.split("-");

        int year = Integer.valueOf(dateArray[0]).intValue();
        int month = Integer.valueOf(dateArray[1]).intValue();
        int day = 1;
        Calendar c = Calendar.getInstance();
        c.set(year, month - 1, day);
        return c;
    }

    @Override
    public Long getRelatedFileId(Long flowId) throws Exception {
        return flowStructureDao.getRelatedFileId(flowId);
    }

    /**
     * 创建不存在目录的节点
     */
    @Override
    public Long createRelatedFileDirByPath(Long id, Long projectId, Long createPeopleId) throws Exception {
        List<Object[]> parentRelateFiles;
        // 0:流程ID，1关联的文件ID,流程名称,3:sortId
        Long parentId = 0L;
        try {

            // 根据path 获取流程的目录,判断在文件目录中是否存在流程的目录结构
            parentRelateFiles = flowStructureDao.getParentRelatedFileByPath(id);

            for (Object[] objects : parentRelateFiles) {
                Long flowId = valueOfLong(objects[0]);
                Integer sortId = valueOfInt(objects[3]);
                if (objects[1] == null) {
                    // 创建文件目录
                    parentId = createFileDir(projectId, createPeopleId, parentId, objects[2].toString(), flowId, sortId);
                    // 更新关联文件ID
                    flowStructureDao.updateParentRelatedFileId(flowId, parentId);
                } else {
                    parentId = valueOfLong(objects[1]);
                }
            }
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
        return parentId;
    }

    private Long valueOfLong(Object obj) {
        return obj == null ? null : Long.valueOf(obj.toString());
    }

    private Integer valueOfInt(Object obj) {
        return obj == null ? null : Integer.valueOf(obj.toString());
    }

    private Long createFileDir(Long projectId, Long createPeopleId, Long parentId, String fileName, Long flowId,
                               Integer sortId) throws Exception {
        return fileService.addFileDir(JecnRelatedDirFileUtil.getFileBean(projectId, createPeopleId, parentId, fileName,
                flowId, sortId, 0));
    }

    @Override
    public FlowFileContent getFlowFileContent(Long contentId) throws Exception {
        FlowFileContent fileContent = (FlowFileContent) flowStructureDao.getSession().get(FlowFileContent.class,
                contentId);
        return fileContent;
    }

    /**
     * 流程标准化文件
     *
     * @param flowId
     * @return
     * @throws Exception
     */
    @Override
    public List<JecnTreeBean> getStandardizedFiles(Long flowId) throws Exception {
        List<Object[]> list = flowStructureDao.getStandardizedFiles(flowId);
        List<JecnTreeBean> treeBeanList = new ArrayList<JecnTreeBean>();
        JecnTreeBean treeBean = null;
        for (Object[] obj : list) {
            if (obj[2] == null) {
                continue;
            }
            treeBean = new JecnTreeBean();
            treeBean.setId(valueOfLong(obj[2]));
            treeBean.setName(obj[3].toString());
            treeBean.setNumberId(JecnUtil.objToStr(obj[4]));
            treeBeanList.add(treeBean);
        }
        return treeBeanList;
    }

    /**
     * 打开流程（文件） 根据内容ID
     */
    @Override
    public FileOpenBean openFileContent(Long id) throws Exception {
        try {
            FileOpenBean fileOpenBean = new FileOpenBean();
            String hql = "from FlowFileContent where id=?";
            FlowFileContent fileContent = flowStructureDao.getObjectHql(hql, id);
            if (fileContent != null) {
                fileOpenBean.setId(fileContent.getId());
                fileOpenBean.setFileByte(JecnDaoUtil.blobToBytes(fileContent.getFileStream()));
                fileOpenBean.setName(fileContent.getFileName());
            }
            return fileOpenBean;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public FileOpenBean openFileContent(Long flowId, boolean isPub) throws Exception {
        try {
            FileOpenBean fileOpenBean = new FileOpenBean();
            Object[] obj = flowStructureDao.getFlowFileContent(flowId, isPub ? "" : "_T");
            fileOpenBean.setId(valueOfLong(obj[1]));
            fileOpenBean.setFileByte(JecnDaoUtil.blobToBytes((Blob) obj[2]));
            fileOpenBean.setName(obj[0].toString());
            return fileOpenBean;
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * 流程相关风险
     *
     * @param flowId
     * @return
     * @throws Exception
     */
    @Override
    public List<JecnTreeBean> getRelatedRiskList(Long flowId) throws Exception {
        return JecnUtil.getJecnTreeBeanByObjectRisk(this.flowStructureDao.getRelatedRiskList(flowId));
    }

    @Override
    public void sendFlowDriverEmail() throws Exception {
        if (JecnConfigTool.isUseNewDriver()) {
            return;
        }
        if (!JecnConfigTool.isSendFlowDriverEmail()) {
            return;
        }
        List<FlowDriver> flowDrivers = listFlowDriver();
        if (flowDrivers.size() == 0) {
            return;
        }
        List<FlowDriver> needHandle = getNeedHandleFlowDrivers(flowDrivers);
        if (needHandle.size() == 0) {
            return;
        }
        Map<Long, FlowDriver> m = new HashMap<Long, FlowDriver>();
        Set<Long> s = new HashSet<Long>();
        for (FlowDriver driver : needHandle) {
            m.put(driver.getFlowId(), driver);
            s.add(driver.getFlowId());
        }
        // 查询待收件的人员等信息
        Map<Long, List<JecnUser>> userMap = getDriverUsers(s);
        BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.DRIVER_TIP);
        for (FlowDriver driver : needHandle) {
            List<JecnUser> users = userMap.get(driver.getFlowId());
            if (users != null && users.size() > 0) {
                driver.setUsers(users);
                emailBuilder.setData(users, driver);
                JecnUtil.saveEmail(emailBuilder.buildEmail());
            }
        }
    }

    private Map<Long, List<JecnUser>> getDriverUsers(Set<Long> ids) throws Exception {
        List<Object[]> userObjs = flowStructureDao.getDriverUsers(ids);
        Map<Long, List<JecnUser>> m = new HashMap<Long, List<JecnUser>>();
        for (Object[] user : userObjs) {
            if (user[0] == null || user[1] == null || user[2] == null || user[3] == null || user[4] == null) {
                continue;
            }
            JecnUser u = new JecnUser();
            u.setTrueName(user[1].toString());
            u.setEmail(user[2].toString());
            u.setEmailType(user[3].toString());
            u.setPeopleId(Long.valueOf(user[4].toString()));
            Long flowId = Long.valueOf(user[0].toString());
            if (!m.containsKey(flowId)) {
                m.put(flowId, new ArrayList<JecnUser>());
            }
            m.get(flowId).add(u);
        }
        return m;
    }

    private List<FlowDriver> getNeedHandleFlowDrivers(List<FlowDriver> flowDrivers) {
        // 要发两封邮件一封提前七天发一封当天发
        List<FlowDriver> needHandle = new ArrayList<FlowDriver>();
        needHandle.addAll(getNeedHandleFlowDrivers(flowDrivers, 7));
        needHandle.addAll(getNeedHandleFlowDrivers(flowDrivers, 0));
        return needHandle;
    }

    private List<FlowDriver> getNeedHandleFlowDrivers(List<FlowDriver> flowDrivers, int advanceDay) {

        List<FlowDriver> needHandle = new ArrayList<FlowDriver>();
        // 1日
        // 2周
        // 3月
        // 4季
        // 5年
        final Calendar c = Calendar.getInstance();
        Calendar temp = Calendar.getInstance();
        JecnUtil.calendarReset(c, temp);
        temp.add(Calendar.DAY_OF_MONTH, advanceDay);
        int weekday = c.get(Calendar.DAY_OF_WEEK);
        String advanceDriverDate = JecnUtil.DateToStr(temp.getTime(), "yyyy-MM-dd");
        String todayDriverDate = JecnUtil.DateToStr(c.getTime(), "yyyy-MM-dd");

        for (FlowDriver driver : flowDrivers) {
            String quantity = driver.getQuantity();
            if (quantity == null || "".equals(quantity)) {
                continue;
            }
            if ("1".equals(quantity)) {// 今天发
                if (advanceDay == 0) {
                    continue;
                }
                needHandle.add(driver);
                driver.setDriverTime(todayDriverDate);
            } else if ("2".equals(quantity)) {// 今天如果是周一今天发
                if (advanceDay == 0) {
                    continue;
                }
                if (weekday == 2) {// 周一
                    needHandle.add(driver);
                    driver.setDriverTime(todayDriverDate);
                }
            } else if ("3".equals(quantity)) {// 月
                // 有两种情况，今天加间隔时间可能在本月也可能在下月
                JecnUtil.calendarReset(c, temp);
                // 下月
                temp.set(Calendar.MONTH, c.get(Calendar.MONTH + 1));
                temp.set(Calendar.DAY_OF_MONTH, Integer.valueOf(driver.getStartTime()) + 1 - advanceDay);
                if (c.equals(temp)) {
                    needHandle.add(driver);
                    driver.setDriverTime(advanceDriverDate);
                } else {
                    JecnUtil.calendarReset(c, temp);
                    // 本月
                    temp.set(Calendar.DAY_OF_MONTH, Integer.valueOf(driver.getStartTime()) + 1 - advanceDay);
                    if (c.equals(temp)) {
                        needHandle.add(driver);
                        driver.setDriverTime(advanceDriverDate);
                    }
                }
            } else if ("4".equals(quantity)) {// 季度
                // 季度区间
                int startTime = Integer.valueOf(driver.getStartTime()) + 1 - advanceDay;
                JecnUtil.calendarReset(c, temp);
                temp.set(Calendar.DAY_OF_MONTH, startTime);
                temp.set(Calendar.MONTH, 0);
                if (c.equals(temp)) {
                    needHandle.add(driver);
                    driver.setDriverTime(advanceDriverDate);
                    continue;
                }
                JecnUtil.calendarReset(c, temp);
                temp.set(Calendar.DAY_OF_MONTH, startTime);
                temp.set(Calendar.MONTH, 3);
                if (c.equals(temp)) {
                    needHandle.add(driver);
                    driver.setDriverTime(advanceDriverDate);
                    continue;
                }
                JecnUtil.calendarReset(c, temp);
                temp.set(Calendar.DAY_OF_MONTH, startTime);
                temp.set(Calendar.MONTH, 6);
                if (c.equals(temp)) {
                    needHandle.add(driver);
                    driver.setDriverTime(advanceDriverDate);
                    continue;
                }
                JecnUtil.calendarReset(c, temp);
                temp.set(Calendar.DAY_OF_MONTH, startTime);
                temp.set(Calendar.MONTH, 9);
                if (c.equals(temp)) {
                    needHandle.add(driver);
                    driver.setDriverTime(advanceDriverDate);
                    continue;
                }
                // 来年的第一个季度
                JecnUtil.calendarReset(c, temp);
                temp.set(Calendar.DAY_OF_MONTH, startTime);
                temp.set(Calendar.MONTH, 0);
                temp.add(Calendar.YEAR, 1);
                if (c.equals(temp)) {
                    needHandle.add(driver);
                    driver.setDriverTime(advanceDriverDate);
                    continue;
                }
            } else if ("5".equals(quantity)) {// 年
                String[] monthAndDay = driver.getStartTime().split(",");
                // 月份和日都比实际值小1
                int month = Integer.valueOf(monthAndDay[0]);
                int day = Integer.valueOf(monthAndDay[1]) + 1;
                if (day > 28) {// 由于2月等并没有31天，所以如果这个日不存在那么设置为最后一天
                    JecnUtil.calendarReset(c, temp);
                    day = Math.min(day, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
                }
                // 明年
                // 有两种情况，今天加间隔时间可能在本月也可能在下月
                JecnUtil.calendarReset(c, temp);
                temp.set(Calendar.YEAR, c.get(Calendar.YEAR + 1));
                temp.set(Calendar.MONTH, month);
                temp.set(Calendar.DAY_OF_MONTH, day - advanceDay);
                if (c.equals(temp)) {
                    needHandle.add(driver);
                    driver.setDriverTime(advanceDriverDate);
                } else {// 今年
                    JecnUtil.calendarReset(c, temp);
                    temp.set(Calendar.MONTH, month);
                    temp.set(Calendar.DAY_OF_MONTH, day - advanceDay);
                    if (c.equals(temp)) {
                        needHandle.add(driver);
                        driver.setDriverTime(advanceDriverDate);
                    }
                }
            }
        }
        return needHandle;
    }

    private List<FlowDriver> listFlowDriver() throws Exception {
        String sql = "SELECT" + "		jfst.FLOW_ID," + "		jfst.FLOW_NAME," + "		jfd.START_TIME," + "		jfd.END_TIME,"
                + "		jfd.QUANTITY" + "	FROM" + "		JECN_FLOW_DRIVERT jfd"
                + "	INNER JOIN JECN_FLOW_STRUCTURE jfst ON jfst.FLOW_ID = jfd.FLOW_ID" + "	AND jfst.DEL_STATE = 0"
                + "	WHERE" + "		jfd.START_TIME IS NOT NULL" + "	AND jfd.QUANTITY IS NOT NULL";
        List<FlowDriver> flowDrivers = new ArrayList<FlowDriver>();
        List<Object[]> objs = flowStructureDao.listNativeSql(sql);
        FlowDriver driver = null;
        for (Object[] obj : objs) {
            driver = new FlowDriver();
            flowDrivers.add(driver);
            driver.setFlowId(Long.valueOf(obj[0].toString()));
            driver.setFlowName(obj[1].toString());
            driver.setStartTime(JecnUtil.nullToEmpty(obj[2]));
            driver.setEndTime(JecnUtil.nullToEmpty(obj[3]));
            driver.setQuantity(JecnUtil.nullToEmpty(obj[4]));
        }
        return flowDrivers;
    }

    @Override
    public void favoriteUpdateSendEmail() throws Exception {
        if (!JecnConfigTool.favoriteUpdateSendEmail()) {
            return;
        }
        // u.related_id,u.related_name,u.star_type,u.task_type,u.pub_time,ju.PEOPLE_ID,ju.TRUE_NAME,ju.EMAIL,ju.EMAIL_TYPE
        List<Object[]> objs = flowStructureDao.listFavoriteUpdateInfo();
        FavoriteUpdate up = null;
        JecnUser user = null;
        Date yesterdayDate = JecnUtil.getDateByInterval(new Date(), -1);
        String yesterday = JecnUtil.DateToStr(yesterdayDate, "yyyy-MM-dd");
        BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.FAVORITE_UPDATE);
        List<JecnUser> users = new ArrayList<JecnUser>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        for (Object[] obj : objs) {
            if (obj[3] == null || obj[4] == null || obj[5] == null || obj[6] == null || obj[7] == null
                    || obj[8] == null) {
                continue;
            }
            // 只发送昨天发布过而且没有看过的收藏文件
            String pubTime = JecnUtil.DateToStr((Date) obj[4], format);
            if (!pubTime.equals(yesterday)) {
                continue;
            }

            up = new FavoriteUpdate();
            up.setPeopleId(Long.valueOf(obj[5].toString()));
            up.setrId(Long.valueOf(obj[0].toString()));
            up.setrName(obj[1].toString());
            up.setStarType(Integer.valueOf(obj[2].toString()));
            up.setTaskType(Integer.valueOf(obj[3].toString()));

            user = new JecnUser();
            user.setPeopleId(Long.valueOf(obj[5].toString()));
            user.setEmail(obj[7].toString());
            user.setEmailType(obj[8].toString());
            users.clear();
            users.add(user);
            emailBuilder.setData(users, up);
            JecnUtil.saveEmail(emailBuilder.buildEmail());
        }

    }

    @Override
    public void updateProcessMapRelatedFiles(ProcessMapRelatedFileData relatedFileData) throws Exception {
        // 1、标准化文件
        saveOrUpdateRelatedStandardizedFiles(relatedFileData.getFlowId(), JecnCommon.getIdsByTreeBeans(relatedFileData
                .getRelatedStandardizeds()));
        // 2、相关制度
        saveOrUpdateRealtedRules(relatedFileData.getFlowId(), JecnCommon.getIdsByTreeBeans(relatedFileData
                .getRelatedRules()));

        // 更新日志
        this.updateFlowModifyRecord(relatedFileData.getFlowId(), relatedFileData.getUpdatePeopleId(), 5);
    }

    @Override
    public Object[] getFlowFileContentName(Long id, boolean isPub) throws Exception {
        return this.flowStructureDao.getFlowFileContentName(id, isPub ? "" : "_T");
    }

    @Override
    public Map<String, String> getProcessFileNameAndContentId(long id, boolean isPub) throws Exception {
        Map<String, String> content = new HashMap<String, String>();
        /* if (isHistory) { */
        FlowFileContent fileContent = getFlowFileContent(id);
        content.put("FileName", fileContent.getFileName());
        content.put("ContentId", fileContent.getId().toString());
        /*
         * } else { Object[] obj = flowStructureDao.getFlowFileContentName(id,
         * isPub ? "" : "_T"); content.put("FileName", obj[0].toString());
         * content.put("ContentId", obj[1].toString()); }
         */
        return content;
    }

    @Override
    public List<JecnTreeBean> getProcessApplyOrgs(Long id) throws Exception {
        String sql = "select jfo.org_id,jfo.org_name,jfo.per_org_id,0"
                + " from jecn_flow_org jfo,JECN_FLOW_APPLY_ORG_T t where jfo.org_id = t.org_id and t.RELATED_ID=? and jfo.del_state=0"
                + " order by jfo.per_org_id,jfo.sort_id,jfo.org_id";
        List<Object[]> listOrg = flowStructureDao.listNativeSql(sql, id);
        List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
        for (Object[] obj : listOrg) {
            JecnTreeBean jecnTreeBean = JecnUtil.getJecnTreeBeanByObjectOrg(obj);
            if (jecnTreeBean != null) {
                listResult.add(jecnTreeBean);
            }
        }
        return listResult;
    }

    @Override
    public List<JecnTreeBean> getDirverEmailPeopleByFlowId(Long flowId) throws Exception {
        JecnTreeBean jecnTreeBean = null;
        String sql = "SELECT JU.PEOPLE_ID,JU.TRUE_NAME FROM JECN_USER JU,JECN_FLOW_DRIVER_EMAIL_T T WHERE T.PEOPLE_ID = JU.PEOPLE_ID AND T.RELATED_ID =? AND JU.ISLOCK=0";
        List<Object[]> list = flowStructureDao.listNativeSql(sql, flowId);
        List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
        for (Object[] obj : list) {
            if (obj != null && obj[0] != null && obj[1] != null) {
                jecnTreeBean = new JecnTreeBean();
                jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
                jecnTreeBean.setName(obj[1].toString());
                listResult.add(jecnTreeBean);
            }
        }
        return listResult;
    }

    /**
     * 根据ID集合获取ID对应的父节点名称集合
     *
     * @return Map<Long       ,               String>
     */
    @Override
    public Map<Long, String> getNodeParentById(List<Long> ids, boolean isPub, TreeNodeType nodeType) throws Exception {
        String isPubStr = "";
        if (!isPub) {
            isPubStr = "_T";
        }
        String sql = JecnDaoUtil.getNodeParentSql(ids, nodeType, isPubStr);
        List<Object[]> parentNames = this.flowStructureDao.listNativeSql(sql);

        // 按流程分组 获取流程对应 父节点名称集合
        Map<Long, List<String>> parentNamesMap = new HashMap<Long, List<String>>();
        if (parentNames == null || parentNames.size() == 0) {
            return null;
        }

        for (Object[] obj : parentNames) {
            if (obj[0] == null || obj[1] == null) {
                continue;
            }
            Long id = Long.valueOf(obj[0].toString());
            if (!parentNamesMap.containsKey(id)) {
                parentNamesMap.put(id, new ArrayList<String>());
            }
            parentNamesMap.get(id).add(obj[1].toString());
        }

        // 根据分组后的名称获取
        Map<Long, String> parentNameMap = new HashMap<Long, String>();
        for (Entry<Long, List<String>> entry : parentNamesMap.entrySet()) {
            if (nodeType == nodeType.position) {
                parentNameMap.put(entry.getKey(), getParentNameByNamesThePost(entry.getValue()));
            } else {
                parentNameMap.put(entry.getKey(), getParentNameByNames(entry.getValue()));
            }
        }
        return parentNameMap;
    }

    private String getParentNameByNames(List<String> names) {
        if (JecnConfigTool.isMengNiuLogin()) {// 蒙牛情况下去除 最顶级部门
            names.remove(0);
        }
        String parentName = null;
        for (int i = 0; i < names.size() - 1; i++) {
            if (parentName == null) {
                parentName = names.get(i);
            } else {
                parentName += "/" + names.get(i);
            }
        }
        return parentName;

    }

    /**
     * 岗位搜索 上级部门情况下 不需要 去掉本级部门
     *
     * @param names
     * @return
     */
    private String getParentNameByNamesThePost(List<String> names) {
        if (JecnConfigTool.isMengNiuLogin()) {// 蒙牛情况下去除 最顶级部门
            names.remove(0);
        }
        String parentName = null;
        for (int i = 0; i < names.size(); i++) {
            if (parentName == null) {
                parentName = names.get(i);
            } else {
                parentName += "/" + names.get(i);
            }
        }
        return parentName;
    }

    @Override
    public void taskFinishSendEmail() throws Exception {
        // 查询昨天发布的任务
        List<JecnTaskBeanNew> tasks = getYesterdayFinishTaskBeans();
        // 循环发送邮件
        TaskSendEmail send = new TaskSendEmail(configItemDao, userDao, flowStructureDao, processBasicDao);
        for (JecnTaskBeanNew taskBean : tasks) {
            try {
                send.finishSendEmail(taskBean);
            } catch (Exception e) {
                log.error("", e);
            }
        }
    }

    private void setTaskApproveSourceName(JecnTaskBeanNew taskBeanNew) {
        try {
            /**
             * taskType=0时，表示流程ID，taskType=1时，表示文件ID，taskType=2时，表示制度模板ID,3:
             * 制度文件ID，4 ： 流程地图ID
             */
            long rid = taskBeanNew.getRid();
            if (0 == taskBeanNew.getTaskType() || 4 == taskBeanNew.getTaskType()) {
                JecnFlowStructureT jecnFlowStructureT = flowStructureDao.get(rid);
                taskBeanNew.setRname(jecnFlowStructureT.getFlowName());
            } else if (1 == taskBeanNew.getTaskType()) {
                JecnFileBeanT jecnFileBeanT = fileDao.get(rid);
                taskBeanNew.setRname(jecnFileBeanT.getFileName());
            } else if (2 == taskBeanNew.getTaskType() || 3 == taskBeanNew.getTaskType()) {
                RuleT ruleT = ruleDao.get(rid);
                taskBeanNew.setRname(ruleT.getRuleName());
            }
        } catch (Exception e) {
            log.error("setTaskApproveSourceName()异常", e);
        }
    }

    private List<JecnTaskBeanNew> getYesterdayFinishTaskBeans() {
        String conditon = SqlUtil.getEqualAccuracyDay("jtbn.UPDATE_TIME", SqlUtil.getYesterdayAccuracyDay());
        String sql = "	SELECT" + "		jtbn.id" + "	FROM" + "		JECN_TASK_BEAN_NEW jtbn" + "	WHERE" + "		jtbn.state = 5 "
                + "	AND jtbn.IS_LOCK = 1 and " + conditon;

        List<Long> idList = flowStructureDao.listNativeSql(sql);
        if (idList == null || idList.size() == 0) {
            return new ArrayList<JecnTaskBeanNew>();
        }
        String ids = JecnCommonSql.getIds(idList);
        String hql = "from JecnTaskBeanNew where id in " + ids;
        List<JecnTaskBeanNew> tasks = flowStructureDao.listHql(hql);
        if (tasks.size() > 0) {
            for (JecnTaskBeanNew task : tasks) {
                // 设置相关文件名称
                setTaskApproveSourceName(task);
            }
        }
        return tasks;
    }

    @Override
    public ProcessDownloadBaseBean getProcessDownloadBean(Long flowId, boolean isPub, String mark) throws Exception {
        return DownloadUtil.getProcessDownloadBean(flowId, isPub, configItemDao, docControlDao, flowStructureDao,
                processBasicDao, processKPIDao, processRecordDao, mark);
    }

    @Override
    public boolean isNotAbolishOrNotDelete(Long id) throws Exception {
        // TODO Auto-generated method stub
        String sql = "select count(t.flow_id) from jecn_flow_structure_t t where t.flow_id = ? and t.del_state = 0";
        int count = flowStructureDao.countAllByParamsNativeSql(sql, id);
        return count > 0 ? true : false;
    }

    @Override
    public void addFlowFileHeadInfo(FlowFileHeadData flowFileHeadData) {
        flowStructureDao.getSession().saveOrUpdate(flowFileHeadData);
    }

    @Override
    public FlowFileHeadData findFlowFileHeadInfo(Long selectId) {
        String hql = "FROM FlowFileHeadData where flowId = ?";
        return (FlowFileHeadData) flowStructureDao.getObjectHql(hql, selectId);

    }

    @Override
    public List<Object[]> getFlowElementById(List ids) {
        // TODO Auto-generated method stub
        String sql = "select jsts.flow_name perFlowName, jst.flow_name flowName, imt.link_flow_id flowId, imt.figure_type figureType"
                + "  from jecn_flow_structure_image_t imt"
                + " inner join jecn_flow_structure_t jst"
                + "    on imt.link_flow_id = jst.flow_id"
                + "  inner join jecn_flow_structure_t jsts"
                + "    on imt.flow_id = jsts.flow_id" + " where jst.flow_id in " + JecnCommonSql.getIds(ids);
        return flowStructureImageTDao.listNativeSql(sql);
    }

    @Override
    public List<Object[]> getFlowElementByLinkId(List ids) {
        // TODO Auto-generated method stub
        String sql = "select imt.flow_id flowId, imt.figure_type figureType,imt.figure_text figureText"
                + "  from jecn_flow_structure_image_t imt " + "where imt.link_flow_id in " + JecnCommonSql.getIds(ids);
        return flowStructureImageTDao.listNativeSql(sql);
    }

    @Override
    public void reNameFlowFigureText(Long flowLinkId, String flowName) {
        // TODO Auto-generated method stub
        String sql = "update jecn_flow_structure_image_t  set figure_text = ? where link_flow_id = ?";
        flowStructureImageTDao.execteNative(sql, flowName, flowLinkId);
    }

    @Override
    public List<FlowFileHeadData> findFlowFileHeadInfoVerification(Long id) {
        String sql = "select head.*" + "    from jecn_flow_structure_t T" + "    left join jecn_flow_structure_t jr"
                + "      on " + JecnCommonSqlTPath.INSTANCE.getSqlSubStr("jr")
                + "    left join JECN_FLOW_FILE_HEADINFO head" + "    on head.flow_id = jr.flow_id"
                + "   where T.flow_id = " + id + " and jr.isflow = 0 and jr.del_state=0 and head.id is not null"
                + "   order by  jr.t_path desc";
        return flowStructureDao.getSession().createSQLQuery(sql).addEntity(FlowFileHeadData.class).list();
    }

    @Override
    public PubSourceData getPubSourceData(Long rId, int rType) throws Exception {
        PubSourceData result = new PubSourceData();
        List<PubSourceCopyRelated> all = new ArrayList<PubSourceCopyRelated>();
        result.setAll(all);
        String sql = "select pscr.id,pscr.r_id,pscr.r_type,pscr.c_id,pscr.c_type,jfo.org_name as name from Pub_Source_Copy_Related pscr"
                + "  inner join jecn_flow_org jfo on pscr.c_id=jfo.org_id and pscr.c_type=0 and pscr.r_id=? and pscr.r_type=?"
                + "  union"
                + "  select pscr.id,pscr.r_id,pscr.r_type,pscr.c_id,pscr.c_type,jfoi.figure_text as name from Pub_Source_Copy_Related pscr"
                + "  inner join jecn_flow_org_image jfoi on pscr.c_id=jfoi.figure_id and pscr.c_type=2 and pscr.r_id=? and pscr.r_type=?"
                + "  union"
                + "  select pscr.id,pscr.r_id,pscr.r_type,pscr.c_id,pscr.c_type,ju.true_name as name from Pub_Source_Copy_Related pscr"
                + "  inner join jecn_user ju on pscr.c_id=ju.people_id and pscr.c_type=3 and pscr.r_id=? and pscr.r_type=?"
                + "  union"
                + "  select pscr.id,pscr.r_id,pscr.r_type,pscr.c_id,pscr.c_type,jpg.name as name from Pub_Source_Copy_Related pscr"
                + "  inner join jecn_position_group jpg on pscr.c_id=jpg.id and pscr.c_type=1 and pscr.r_id=? and pscr.r_type=?";
        List<Object[]> objs = flowStructureDao.listNativeSql(sql, rId, rType, rId, rType, rId, rType, rId, rType);
        PubSourceCopyRelated r = null;
        for (Object[] obj : objs) {
            r = new PubSourceCopyRelated();
            all.add(r);
            r.setId(obj[0].toString());
            r.setrId(rId);
            r.setrType(rType);
            r.setcId(Long.valueOf(obj[3].toString()));
            r.setcType(Integer.valueOf(obj[4].toString()));
            r.setcName(obj[5].toString());
        }
        return result;
    }

    @Override
    public void sendPubCopyNoteEmails(PubSourceSave pubNoteData) throws Exception {
        final int limit = 20;
        Set<Long> ids = new HashSet<Long>();
        JecnTreeBean bean = pubNoteData.getResources().get(0);
        if (pubNoteData.getOrgs().size() > 0) {
            delaySendCopyNoteEmail(bean);
            return;
        }
        if (pubNoteData.getGroups().size() > 0) {
            delaySendCopyNoteEmail(bean);
            return;
        }
        if (pubNoteData.getPoses().size() > 0) {
            List<JecnUser> posUsers = flowOrgImageDao.listPosUsers(pubNoteData.getPoses());
            if (posUsers.size() > limit) {
                delaySendCopyNoteEmail(bean);
                return;
            } else {
                for (JecnUser u : posUsers) {
                    ids.add(u.getPeopleId());
                }
            }
        }
        if (pubNoteData.getPeoples().size() > 0) {
            if (pubNoteData.getPeoples().size() > limit) {
                delaySendCopyNoteEmail(bean);
                return;
            } else {
                ids.addAll(pubNoteData.getPeoples());
            }
        }
        if (ids.size() == 0) {
            return;
        }
        if (ids.size() > limit) {
            delaySendCopyNoteEmail(bean);
            return;
        }
        List<JecnUser> users = userDao.getJecnUserList(ids);
        EmailResource resource = new EmailResource(bean.getId(), bean.getName(), getType(bean.getTreeNodeType()),
                getSubType(bean.getTreeNodeType()));
        BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.RESOURCE_PUBLISH);
        emailBuilder.setData(users, resource);
        JecnUtil.saveEmail(emailBuilder.buildEmail());
    }

    /**
     * 延迟到晚上发
     *
     * @param bean
     */
    private void delaySendCopyNoteEmail(JecnTreeBean bean) {
        String rId = String.valueOf(bean.getId());
        String rType = String.valueOf(getType(bean.getTreeNodeType()));
        // 先删后加
        String hql = "delete from NightEvent where rId=? and rType=?";
        baseDao.execteHql(hql, rId, rType);
        NightEvent event = new NightEvent();
        event.setEventType(NightEvent.HAND_PUBLISH_SEND_EMAIL);
        event.setrId(rId);
        event.setrType(rType);
        event.setrSubType(String.valueOf(getSubType(bean.getTreeNodeType())));
        event.setCreateDate(new Date());
        baseDao.getSession().save(event);
    }

    private Integer getSubType(TreeNodeType treeNodeType) {
        switch (treeNodeType) {
            case process:
            case processFile:
                return 0;
            case processMap:
                return 4;
            case file:
                return 1;
            case ruleFile:
                return 3;
            case ruleMode:
            case ruleModeFile:
                return 2;
            default:
                throw new IllegalArgumentException("getSubType不支持的类型：" + treeNodeType);
        }
    }

    @Override
    public void saveOrUpdatePubCopyNote(PubSourceSave pubNoteData) throws Exception {
        JecnTreeBean resource = pubNoteData.getResources().get(0);
        Long id = resource.getId();
        int type = getType(resource.getTreeNodeType());
        String sql = "delete from PUB_SOURCE_COPY_RELATED where r_id=? and r_type=?";
        flowStructureDao.execteNative(sql, id, type);

        pubCopyNote(pubNoteData.getOrgs(), PubSourceCopyRelated.ORG, id, type);
        pubCopyNote(pubNoteData.getGroups(), PubSourceCopyRelated.POS_GROUP, id, type);
        pubCopyNote(pubNoteData.getPoses(), PubSourceCopyRelated.POS, id, type);
        pubCopyNote(pubNoteData.getPeoples(), PubSourceCopyRelated.PEOPLE, id, type);

    }

    private int getType(TreeNodeType treeNodeType) {
        String typeStr = treeNodeType.toString().toLowerCase();
        if (typeStr.startsWith("process")) {
            return PubSourceCopyRelated.PROCESS;
        } else if (typeStr.startsWith("file")) {
            return PubSourceCopyRelated.FILE;
        } else if (typeStr.startsWith("rule")) {
            return PubSourceCopyRelated.RULE;
        } else {
            throw new IllegalArgumentException(this.getClass() + ".getType:" + treeNodeType);
        }
    }

    private void pubCopyNote(Set<Long> cIds, int cType, Long rId, int rType) {
        PubSourceCopyRelated related = null;
        for (Long cId : cIds) {
            related = new PubSourceCopyRelated();
            related.setcId(cId);
            related.setcType(cType);
            related.setrId(rId);
            related.setrType(rType);
            flowStructureDao.getSession().save(related);
        }

    }

    @Override
    public void handPubSendEmail() throws Exception {
        String sql = "select * from NIGHT_EVENT where EVENT_TYPE=? ";
        List<NightEvent> events = (List<NightEvent>) flowStructureDao.listNativeAddEntity(sql, NightEvent.class,
                NightEvent.HAND_PUBLISH_SEND_EMAIL);
        for (NightEvent e : events) {
            try {
                handPubSendEmail(e);
            } catch (Exception e2) {
                log.error("", e2);
            }
            // 删除事件
            flowStructureDao.getSession().delete(e);
        }
    }

    private void handPubSendEmail(NightEvent e) throws Exception {
        Long rId = Long.valueOf(e.getrId());
        Integer rType = Integer.valueOf(e.getrType());
        List<JecnUser> users = getPubCopyNoteUserByResource(rId, rType);
        if (users == null || users.size() == 0) {
            return;
        }
        Integer subType = null;
        String rName = null;
        if (rType == 0) {
            JecnFlowStructureT flowStructureT = flowStructureDao.get(rId);
            rName = flowStructureT.getFlowName();
            if (flowStructureT.getIsFlow() == 0) {
                subType = 4;
            } else {
                subType = 0;
            }
        } else if (rType == 1) {
            rName = fileDao.get(rId).getFileName();
            subType = 1;
        } else if (rType == 2) {
            RuleT ruleT = ruleDao.get(rId);
            rName = ruleT.getRuleName();
            if (ruleT.getIsDir() == 1) {
                subType = 2;
            } else if (ruleT.getIsDir() == 2) {
                subType = 3;
            }
        }
        EmailResource resource = new EmailResource(rId, rName, rType, subType);
        BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.RESOURCE_PUBLISH);
        emailBuilder.setData(users, resource);
        JecnUtil.saveEmail(emailBuilder.buildEmail());
    }

    private List<JecnUser> getPubCopyNoteUserByResource(Long rId, Integer rType) throws Exception {
        String hql = "from PubSourceCopyRelated where rId=? and rType=?";
        List<PubSourceCopyRelated> relateds = baseDao.listHql(hql, rId, rType);
        Set<Long> orgs = new HashSet<Long>();
        Set<Long> poses = new HashSet<Long>();
        Set<Long> peoples = new HashSet<Long>();
        Set<Long> groups = new HashSet<Long>();
        for (PubSourceCopyRelated related : relateds) {
            if (related.getcType() == PubSourceCopyRelated.ORG) {
                orgs.add(related.getcId());
            } else if (related.getcType() == PubSourceCopyRelated.POS) {
                poses.add(related.getcId());
            } else if (related.getcType() == PubSourceCopyRelated.PEOPLE) {
                peoples.add(related.getcId());
            } else if (related.getcType() == PubSourceCopyRelated.POS_GROUP) {
                groups.add(related.getcId());
            }
        }
        List<JecnUser> users = new ArrayList<JecnUser>();
        if (orgs.size() > 0) {
            users.addAll(flowOrgDao.listOrgUsers(orgs));
        }
        if (poses.size() > 0) {
            users.addAll(flowOrgImageDao.listPosUsers(poses));
        }
        if (peoples.size() > 0) {
            users.addAll(userDao.getJecnUserList(peoples));
        }
        if (groups.size() > 0) {
            users.addAll(flowOrgImageDao.listGroupUsers(groups));
        }
        Set<Long> ids = new HashSet<Long>();
        List<JecnUser> result = new ArrayList<JecnUser>();
        for (JecnUser u : users) {
            if (ids.contains(u.getPeopleId())) {
                continue;
            }
            result.add(u);
            ids.add(u.getPeopleId());
        }
        return result;
    }

    @Override
    public void replaceAlllElementAttrs(List<JecnFlowFigureNow> list) {
        String sql;
        JecnFlowStructureImageT image = null;
        for (JecnFlowFigureNow figureNow : list) {
            image = new JecnFlowStructureImageT();
            getColorStr(figureNow, image);
            sql = getReplaceAllEleAttrsSql(figureNow, "", image);
            flowStructureDao.execteNative(sql, figureNow.getFontSize(), figureNow.getFontName(), figureNow
                            .getFontStyle(), figureNow.getBorderColor(), image.getBGColor(), figureNow.getBorderStyle(),
                    figureNow.getFontColor(), figureNow.getBorderColor(), figureNow.getBodyWidth(), figureNow
                            .getShadowColor(), figureNow.getIs3D() == 0 ? 1 : 0, image.getFillEffects(), figureNow
                            .getIsShadow(), figureNow.getFigureName());
            sql = getReplaceAllEleAttrsSql(figureNow, "_T", image);
            flowStructureDao.execteNative(sql, figureNow.getFontSize(), figureNow.getFontName(), figureNow
                            .getFontStyle(), figureNow.getBorderColor(), image.getBGColor(), figureNow.getBorderStyle(),
                    figureNow.getFontColor(), figureNow.getBorderColor(), figureNow.getBodyWidth(), figureNow
                            .getShadowColor(), figureNow.getIs3D() == 0 ? 1 : 0, image.getFillEffects(), figureNow
                            .getIsShadow(), figureNow.getFigureName());
        }
    }

    private String getReplaceAllEleAttrsSql(JecnFlowFigureNow figureNow, String isPub, JecnFlowStructureImageT image) {
        return "UPDATE JECN_FLOW_STRUCTURE_IMAGE" + isPub + " SET FONT_SIZE = ? , FONT_TYPE = ?"
                + " ,FONT_BODY = ? ,BODYCOLOR = ? ,BGCOLOR = ? ,BODYLINE = ?"
                + " ,FONTCOLOR = ? ,LINECOLOR = ? ,LINE_THICKNESS =?  ,SHADOW_COLOR =? "
                + " ,IS_3D_EFFECT = ? ,FILL_EFFECTS = ? ,HAVESHADOW =?  WHERE FIGURE_TYPE=?";
    }

    public void getColorStr(JecnFlowFigureNow figureData, JecnFlowStructureImageT flowStructureImageT) {
        String backColor = "";
        // 图形填充色效果标识
        int colorChangType = 1;

        // 图行颜色的类型none为未填充,one为单色效果,two为双色效果 默认未填充单色效果
        if ("two".equals(figureData.getFillType())) {
            if ("topTilt".equals(figureData.getFillColorChangType())) {
                colorChangType = 1;
            } else if ("bottom".equals(figureData.getFillColorChangType())) {
                colorChangType = 2;
            } else if ("vertical".equals(figureData.getFillColorChangType())) {
                colorChangType = 3;
            } else if ("horizontal".equals(figureData.getFillColorChangType())) {
                colorChangType = 4;
            }
            // 双色填充
            backColor = figureData.getBgColor() + "," + figureData.getChangeColor() + "," + colorChangType;
            flowStructureImageT.setFillEffects(2);
        } else if ("one".equals(figureData.getFillType())) {
            // 图形填充色效果标识
            if (figureData.getBgColor() != null) {
                backColor = figureData.getBgColor();
            }
            flowStructureImageT.setFillEffects(1);
        } else {
            // 图形填充色效果标识
            flowStructureImageT.setFillEffects(0);
        }
        flowStructureImageT.setBGColor(backColor);
    }

    @Override
    public void pubEmailTip() {
        // 发布邮件提醒配置项
        boolean pubToAdminSendEmail = JecnConfigTool.pubToAdminSendEmail();
        boolean pubToFixedSendEmail = JecnConfigTool.pubToFixedSendEmail();
        boolean pubToPermSendEmail = JecnConfigTool.pubToPermSendEmail();
        boolean pubToJoinSendEmail = JecnConfigTool.pubToJoinSendEmail();
        if (!pubToAdminSendEmail && !pubToFixedSendEmail && !pubToPermSendEmail && !pubToJoinSendEmail) {
            return;
        }
        final Set<Long> peoples = new HashSet<Long>();
        if (pubToAdminSendEmail) {
            peoples.addAll(getAllAdmins());
        }
        // 查找发布的流程、文件、制度
        sendFlowTip(new HashSet<Long>(peoples));
        sendFileTip(new HashSet<Long>(peoples));
        sendRuleTip(new HashSet<Long>(peoples));
    }

    private void sendRuleTip(Set<Long> peoples) {
        List<EmailResource> rules = listRulePubYesterday();
        send(peoples, rules, 1);
    }

    private void sendFileTip(Set<Long> peoples) {
        List<EmailResource> files = listFilePubYesterday();
        send(peoples, files, 2);
    }

    private void sendFlowTip(Set<Long> peoples) {
        List<EmailResource> flows = listFlowPubYesterday();
        send(peoples, flows, 0);
    }

    private List<EmailResource> listRulePubYesterday() {
        String sql = "select id," + "       rule_name," + "       2 as type," + "       case"
                + "         when is_dir = 1 then" + "          2" + "         else" + "          3"
                + "       end as sub_type" + "  from JECN_RULE" + " where del_state = 0 and is_dir<>0 and "
                + SqlUtil.getEqualAccuracyDay("pub_time", SqlUtil.getYesterdayAccuracyDay());
        return execSqlToEmailResource(sql);
    }

    private List<EmailResource> listFlowPubYesterday() {
        String sql = "select flow_id," + "       flow_name," + "       0 as type," + "       case"
                + "         when isflow = 0 then" + "          4" + "         else" + "          0"
                + "       end as sub_type" + "  from jecn_flow_structure" + " where del_state = 0" + "   and "
                + SqlUtil.getEqualAccuracyDay("pub_time", SqlUtil.getYesterdayAccuracyDay());
        return execSqlToEmailResource(sql);
    }

    private List<EmailResource> listFilePubYesterday() {
        String sql = "select file_id," + "         file_name," + "         1 as type," + "         1 as sub_type"
                + "    from jecn_file" + "   where del_state = 0 and is_dir<>0 and "
                + SqlUtil.getEqualAccuracyDay("pub_time", SqlUtil.getYesterdayAccuracyDay());
        return execSqlToEmailResource(sql);
    }

    private List<EmailResource> execSqlToEmailResource(String sql) {
        List<Object[]> objs = flowOrgDao.listNativeSql(sql);
        List<EmailResource> result = new ArrayList<EmailResource>();
        EmailResource r = null;
        for (Object[] obj : objs) {
            r = new EmailResource(Long.valueOf(obj[0].toString()), obj[1].toString(), Integer
                    .valueOf(obj[2].toString()), Integer.valueOf(obj[3].toString()));
            result.add(r);
        }
        return result;
    }

    private void send(Set<Long> peoples, List<EmailResource> fs, int fixedType) {
        if (fs == null) {
            return;
        }
        boolean pubToFixedSendEmail = JecnConfigTool.pubToFixedSendEmail();
        if (pubToFixedSendEmail) {
            peoples.addAll(getFixedPeoples(fixedType));
        }

        for (EmailResource f : fs) {
            try {
                send(f, new HashSet<Long>(peoples));
            } catch (Exception e) {
                log.error("", e);
            }
        }
    }

    private void send(EmailResource f, Set<Long> s) {
        boolean pubToPermSendEmail = JecnConfigTool.pubToPermSendEmail();
        if (pubToPermSendEmail) {
            // 查阅权限的id
            Set<Long> accessIds = JecnTaskCommon.getAccessPeopleIds(docControlDao.getSession(), JecnUtil
                    .getAccessTypeByFileType(f.getType()), f.getId());
            s.addAll(accessIds);
        }

        boolean pubToJoinSendEmail = JecnConfigTool.pubToJoinSendEmail();
        // 只有流程有参与人的id
        if (pubToJoinSendEmail && f.getType() == 0) {
            s.addAll(getPartInPeopleIds(f.getId()));
        }
        if (s.size() == 0) {
            return;
        }
        try {
            List<JecnUser> users = JecnTaskCommon.getJecnUserList(s, docControlDao);
            sendEmail(f, users);
        } catch (Exception e) {
            log.error("", e);
        }
    }

    private List<Long> getAllAdmins() {
        List<Long> ids = new ArrayList<Long>();
        try {
            ids = userToId(userDao.getAllAdminUser());
        } catch (Exception e) {
            log.error("", e);
        }
        return ids;
    }

    private List<Long> getFixedPeoples(int type) {
        List<Long> ids = new ArrayList<Long>();
        try {
            ids = userToId(userDao.getFixedEmail(type));
        } catch (Exception e) {
            log.error("", e);
        }
        return ids;
    }

    private List<Long> userToId(List<JecnUser> adminEmail) {
        List<Long> ids = new ArrayList<Long>();
        if (adminEmail == null) {
            return ids;
        }
        for (JecnUser u : adminEmail) {
            ids.add(u.getPeopleId());
        }
        return ids;
    }

    private void sendEmail(EmailResource resource, List<JecnUser> users) throws Exception {
        BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.RESOURCE_PUBLISH);
        emailBuilder.setData(users, resource);
        List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
        JecnUtil.saveEmail(buildEmail);
    }

    /**
     * 获取流程中角色参与的人员ID集合
     *
     * @param docControlDao
     * @param flowId        流程ID
     * @return List<Long> 人员ID集合
     */
    private List<Long> getPartInPeopleIds(Long flowId) {
        /** 获取流程中参与的人员ID */
        String sql = "select distinct u.people_id"
                + "       from jecn_user u,"
                + "       jecn_user_position_related u_p"
                + "       where u.people_id = u_p.people_id and u.islock=0  and"
                + "      ( u_p.figure_id in"
                + "       (select jfoi.figure_id  from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related_t psrt"
                + "       ,jecn_flow_structure_image_t jfsi"
                + "       where psrt.type='0' and psrt.figure_position_id = jfoi.figure_id and jfoi.org_id = jfo.org_id"
                + "       and jfo.del_state=0 and psrt.figure_flow_id =jfsi.figure_id and jfsi.flow_id="
                + flowId
                + ")"
                + "       or"
                + "       u_p.figure_id in"
                + "        (select jfoi.figure_id  from jecn_flow_org jfo,jecn_flow_org_image jfoi,process_station_related_t psrt"
                + "       ,jecn_flow_structure_image_t jfsi,jecn_position_group_r jpgr"
                + "       where psrt.type='1' and psrt.figure_position_id = jpgr.group_id and jpgr.figure_id=jfoi.figure_id"
                + "        and jfoi.org_id = jfo.org_id"
                + "       and jfo.del_state=0 and psrt.figure_flow_id =jfsi.figure_id and jfsi.flow_id=" + flowId
                + "))";

        return flowStructureImageTDao.listObjectNativeSql(sql, "people_id", Hibernate.LONG);
    }

    @Override
    public List<Long> getRelatedFileIds(List<Long> flowIds) throws Exception {
        List<Long> dirIds = null;
        if (flowIds != null && flowIds.size() > 0) {
            dirIds = flowStructureDao.getRelatedFileIds(flowIds);
        }
        return dirIds;
    }

    @Override
    public boolean belowFlowExistFile(List<Long> ids) throws Exception {
        return belowSourceExistFile(ids, "FLOW_ID");
    }

    @Override
    public boolean belowRuleExistFile(List<Long> ids) throws Exception {
        return belowSourceExistFile(ids, "RULE_ID");
    }

    private boolean belowSourceExistFile(List<Long> ids, String column) {
        if (ids == null || ids.size() == 0) {
            return false;
        }
        String sql = "SELECT COUNT(*) FROM JECN_FILE_T C"
                + "	INNER JOIN JECN_FILE_T P ON C.T_PATH IS NOT NULL AND P.T_PATH IS NOT NULL AND C.DEL_STATE=0 AND C.T_PATH LIKE P.T_PATH"
                + JecnCommonSql.getConcatChar() + "'%' AND P.FILE_ID " + "  IN ("
                + "  SELECT FILE_ID FROM JECN_FILE_T WHERE IS_DIR=0 AND " + column + " IN " + JecnCommonSql.getIds(ids)
                + "  )" + " WHERE C.IS_DIR<>0 AND C.DEL_STATE=0";
        int count = flowStructureDao.countAllByParamsNativeSql(sql);
        return count == 0 ? false : true;
    }

    /**
     * 此处返回 字符串 是因为后期可能改为 要携带全路径显示。
     */
    @Override
    public String validateNamefullPath(String flowName, long id, long i, Long projectId) {
        try {
            String hql = "from JecnFlowStructureT where dataType<> 1 and perFlowId <> ? and isFlow = ? and flowName = ? and projectId = ? and delState = 0";
            List<JecnFlowStructureT> list = flowStructureDao.listHql(hql, id, i, flowName, projectId);
            if (list != null && list.size() > 0) {
                return list.get(0).getFlowName();
            }
        } catch (Exception e) {
            log.error("", e);
        }
        return "";
    }

    @Override
    public List<JecnFlowInoutData> getFlowInoutsByType(int type, Long flowId, boolean isPub) throws Exception {
        return flowStructureDao.getFlowInoutsByType(type, flowId, isPub);
    }

    @Override
    public List<CheckoutResult> checkout(Long id) {
        JecnFlowStructureT flowStructureT = get(id);
        // 获得流程id
        List<Long> ids = new ArrayList<Long>();
        if (flowStructureT.getIsFlow() == 0) {// 架构下所有的流程
            ids.addAll(getAllChildFlow(id));
        } else {
            ids.add(id);
        }
        List<CheckoutResult> result = new ArrayList<CheckoutResult>();
        for (Long flowId : ids) {
            result.add(checkoutSingle(flowId));
        }
        return result;
    }

    private List<Long> getAllChildFlow(Long id) {
        String sql = "select c.flow_id as flow_id" + "  from jecn_flow_structure_t c"
                + " inner join jecn_flow_structure_t p" + "    on c.t_path like p.t_path "
                + JecnCommonSql.getConcatChar() + " '%'" + "   and c.isflow = 1    " + "   and c.node_type = 0"
                + "   and c.del_state = 0" + "   and p.del_state = 0" + "   and p.flow_id = ? order by c.view_sort asc";
        return this.flowStructureDao.listObjectNativeSql(sql, "flow_id", Hibernate.LONG, id);
    }

    private CheckoutResult checkoutSingle(Long flowId) {
        ThroughCheck check = new ThroughCheck(this.flowStructureDao, flowId);
        return check.check();
    }

    @Override
    public ByteFile downLoadCheckout(Long id) {
        ByteFile result = new ByteFile();
        result.setFileName("流程贯通性检查.xls");
        ICheckout checkout = new FlowCheckout(checkout(id));
        result.setBytes(checkout.getByte());
        return result;
    }

    @Override
    public List<CheckoutRoleResult> checkoutRole(Long id) {
        JecnFlowStructureT flowStructureT = get(id);
        // 获得流程id
        List<Long> ids = new ArrayList<Long>();
        if (flowStructureT.getIsFlow() == 0) {// 架构下所有的流程
            ids.addAll(getAllChildFlow(id));
        } else {
            ids.add(id);
        }
        List<CheckoutRoleResult> result = new ArrayList<CheckoutRoleResult>();
        for (Long flowId : ids) {
            result.add(checkoutRoleSingle(flowId));
        }
        return result;
    }

    private CheckoutRoleResult checkoutRoleSingle(Long flowId) {
        RoleCheck check = new RoleCheck(configItemDao, flowId);
        return check.check();
    }

    @Override
    public void updateActivityPosRelation(Long flowId, List<CheckoutRoleItem> result) {
        if (JecnUtil.isEmpty(result)) {
            return;
        }
        // 删除要删除的
        String del = "delete from JecnFlowStationT where figureFlowId=? and figurePositionId=? and type=?";
        for (CheckoutRoleItem item : result) {
            if (item.isRelated() || item.getPosId() == null) {
                continue;
            }
            this.baseDao.execteHql(del, item.getFigureId(), item.getPosId(), String.valueOf(item.getType()));
        }
        String q = "select count(*) from JecnFlowStationT where figureFlowId=? and figurePositionId=? and type=?";
        for (CheckoutRoleItem item : result) {
            if (!item.isRelated() || item.getPosId() == null) {
                continue;
            }
            int c = this.baseDao.countAllByParamsHql(q, item.getFigureId(), item.getPosId(), String.valueOf(item
                    .getType()));
            if (c > 0) {
                continue;
            }
            JecnFlowStationT a = new JecnFlowStationT();
            a.setFigureFlowId(item.getFigureId());
            a.setFigurePositionId(item.getPosId());
            a.setType(String.valueOf(item.getType()));
            this.flowStructureDao.getSession().save(a);
        }
        // 更新主表时间
        JecnFlowStructureT flowStructureT = flowStructureDao.get(flowId);
        flowStructureT.setUpdateDate(new Date());
        flowStructureDao.update(flowStructureT);
    }

    @Override
    public ByteFile downLoadCheckoutRole(Long id) {
        ByteFile result = new ByteFile();
        result.setFileName("角色规范性检查检查.xls");
        ICheckout checkout = new RoleCheckout(checkoutRole(id));
        result.setBytes(checkout.getByte());
        return result;
    }

    @Override
    public void sendFlowDriverEmailNew() throws Exception {
        List<FlowDriver> needHandle = getNeedHandleFlowDriversNew();
        if (needHandle.size() == 0) {
            return;
        }
        Map<Long, FlowDriver> m = new HashMap<Long, FlowDriver>();
        for (FlowDriver driver : needHandle) {
            m.put(driver.getFlowId(), driver);
        }
        // 查询待收件的人员等信息
        Map<Long, List<JecnUser>> userMap = getDriverUsers(m.keySet());
        BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.DRIVER_TIP);
        for (FlowDriver driver : needHandle) {
            List<JecnUser> users = userMap.get(driver.getFlowId());
            if (users != null && users.size() > 0) {
                driver.setUsers(users);
                emailBuilder.setData(users, driver);
                JecnUtil.saveEmail(emailBuilder.buildEmail());
            }
        }
    }

    private List<FlowDriver> getNeedHandleFlowDriversNew() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int m = c.get(Calendar.MONTH) + 1;
        int d = c.get(Calendar.DAY_OF_MONTH);
        int h = c.get(Calendar.HOUR_OF_DAY);
        int w = c.get(Calendar.DAY_OF_WEEK) - 1;
        // 第几个季度
        int qm = m % 3;
        if (qm == 0) {
            qm = 3;
        }
        if (w == 0) {// JAVA周日算第一天，
            w = 7;
        }

        c.set(Calendar.DAY_OF_MONTH, 0);
        int lastDayOfMonth = c.get(Calendar.DAY_OF_MONTH);

        boolean isLastDayOfMonth = d == lastDayOfMonth;
        // 1:日，2:周，3:月，4:季，5:年
        List<FlowDriver> result = new ArrayList<FlowDriver>();
        result.addAll(getDriver("1", true, false, false, false, isLastDayOfMonth, h, d, w, m));
        result.addAll(getDriver("2", true, false, true, false, isLastDayOfMonth, h, d, w, m));
        result.addAll(getDriver("3", true, true, false, false, isLastDayOfMonth, h, d, w, m));
        result.addAll(getDriver("4", true, true, false, true, isLastDayOfMonth, h, d, w, qm));
        result.addAll(getDriver("5", true, true, false, true, isLastDayOfMonth, h, d, w, m));
        return result;
    }

    private List<FlowDriver> getDriver(String type, boolean isHour, boolean isDay, boolean isWeek, boolean isMonth,
                                       boolean isLastDayOfMonth, Integer h, Integer d, int w, Integer m) {
        String condition = "";
        if (isHour) {
            condition += " and jfdt.hour = " + h;
        }
        if (isDay) {
            condition += " and jfdt.day = " + d;
        }
        if (isWeek) {
            if (isLastDayOfMonth) {
                condition += " and jfdt.week >= " + w;
            } else {
                condition += " and jfdt.week = " + w;
            }
        }
        if (isMonth) {
            condition += " and jfdt.month = " + m;
        }
        String sql = "SELECT jfs.FLOW_ID,jfs.FLOW_NAME,jfd.START_TIME,jfd.END_TIME,jfd.QUANTITY"
                + "  from jecn_flow_drivert jfd" + " inner join jecn_flow_structure_t jfs"
                + "    on jfd.flow_id = jfs.flow_id" + "   and jfs.del_state = 0" + "   and jfd.quantity =?"
                + " where exists (select 1 from jecn_flow_driver_time_t jfdt"
                + "            where jfd.flow_id = jfdt.flow_id" + condition + "        )";

        List<FlowDriver> flowDrivers = new ArrayList<FlowDriver>();
        List<Object[]> objs = flowStructureDao.listNativeSql(sql, type);
        FlowDriver driver = null;
        String driverTime = JecnUtil.DateToStr(new Date(), "yyyy-MM-dd");
        for (Object[] obj : objs) {
            driver = new FlowDriver();
            flowDrivers.add(driver);
            driver.setFlowId(Long.valueOf(obj[0].toString()));
            driver.setFlowName(obj[1].toString());
            driver.setStartTime(JecnUtil.nullToEmpty(obj[2]));
            driver.setEndTime(JecnUtil.nullToEmpty(obj[3]));
            driver.setQuantity(JecnUtil.nullToEmpty(obj[4]));
            driver.setDriverTime(driverTime);
        }
        return flowDrivers;
    }

    @Override
    public void mnInit(int type) throws Exception {
        String hql2 = "from JecnTaskHistoryNew where id=?";
        if (type == 0) {// 流程
            String hql1 = "from JecnFlowStructureT where historyId is not null and delState = 0 and isFlow=1 and nodeType=0  order by viewSort asc";
            List<JecnFlowStructureT> els = flowStructureDao.listHql(hql1);
            if (JecnUtil.isEmpty(els)) {
                return;
            }
            for (JecnFlowStructureT e : els) {
                if (e == null) {
                    continue;
                }
                log.info("归档" + e.getFlowName());
                JecnTaskHistoryNew history = flowStructureDao.getObjectHql(hql2, e.getHistoryId());
                if (history == null) {
                    log.error("流程" + e.getFlowName() + "对应的文控为空");
                }
                JecnTaskCommon.archive(history, processBasicDao, e);
            }
        } else if (type == 2) {// 制度
            String hql1 = "from RuleT where historyId is not null and delState = 0 and isDir<>0 order by viewSort asc";
            List<RuleT> els = flowStructureDao.listHql(hql1);
            if (JecnUtil.isEmpty(els)) {
                return;
            }
            for (RuleT e : els) {
                if (e == null) {
                    continue;
                }
                log.info("归档" + e.getRuleName());
                JecnTaskHistoryNew history = flowStructureDao.getObjectHql(hql2, e.getHistoryId());
                if (history == null) {
                    log.error("制度" + e.getRuleName() + "对应的文控为空");
                }
                JecnTaskCommon.archive(history, ruleDao, fileDao, e);
            }
        }
        log.info("所有归档完毕");
    }

    @Override
    public void transToProcess(List<Long> nodes, Long peopleId) throws Exception {
        String hql = "from JecnFlowStructureT where flowId in " + JecnCommonSql.getIds(nodes);
        List<JecnFlowStructureT> flows = flowStructureDao.listHql(hql);
        Date updateTime = new Date();
        Session session = flowStructureDao.getSession();
        for (JecnFlowStructureT flow : flows) {
            flow.setUpdateDate(updateTime);
            flow.setNodeType(0);
            flow.setUpdatePeopleId(peopleId);
            session.update(flow);
            JecnUtil.saveJecnJournal(flow.getFlowId(), flow.getFlowName(), 1, 2, peopleId, configItemDao, 9);
        }
    }

    @Override
    public List<JecnFileContent> getJecnFlowFileContents(Long id) throws Exception {
        List<JecnFileContent> result = new ArrayList<JecnFileContent>();
        String hql = "from FlowFileContent where relatedId=? order by createTime desc";
        List<FlowFileContent> contents = flowStructureDao.listHql(hql, id);
        JecnFileContent r = null;
        for (FlowFileContent c : contents) {
            r = new JecnFileContent();
            result.add(r);
            r.setId(c.getId());
            r.setFileName(c.getFileName());
            r.setFileId(id);
            r.setUpdateTime(c.getCreateTime());
            r.setUpdatePeopleId(c.getCreatePeopleId());
            r.setIsVersionLocal(c.getIsVersionLocal());
        }
        return result;
    }

    @Override
    public List<JecnTreeBean> getChildFlows(List<Long> pids) throws Exception {
        try {
            if (pids == null || pids.isEmpty()) {
                return null;
            }
            List<Object[]> list = flowStructureDao.getChildFlowsT(pids);
            return findByListObjectsFlowAppend(list, null);
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Override
    public List<JecnEntry> fetchEntrys() {
        String hql = "from JecnEntry";
        return this.flowStructureDao.listHql(hql);
    }

    @Override
    public void saveEntry(JecnEntry entry) {
        this.flowStructureDao.getSession().save(entry);
    }

    @Override
    public List<JecnTreeBean> fetchEntryLimit(String name, int count) {
        Map<String, JecnEntry> words = JecnContants.ENTRY_MANAGER.getWords();
        Set<String> set = new TreeSet<String>(words.keySet());
        int i = 0;
        List<JecnTreeBean> result = new ArrayList<JecnTreeBean>();
        for (String key : set) {
            if (key.startsWith(name)) {
                if (i >= count) {
                    break;
                }
                i++;
                result.add(new JecnTreeBean(null, key, null));
            }
        }
        return result;
    }
}
