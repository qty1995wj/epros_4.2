/**
 * ISysNotifyTodoWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.nine999;

public interface ISysNotifyTodoWebService extends java.rmi.Remote {
    public com.jecn.webservice.nine999.NotifyTodoAppResult updateTodo(com.jecn.webservice.nine999.NotifyTodoUpdateContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.nine999.Exception;
    public com.jecn.webservice.nine999.NotifyTodoAppResult setTodoDone(com.jecn.webservice.nine999.NotifyTodoRemoveContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.nine999.Exception;
    public com.jecn.webservice.nine999.NotifyTodoAppResult getTodo(com.jecn.webservice.nine999.NotifyTodoGetContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.nine999.Exception;
    public com.jecn.webservice.nine999.NotifyTodoAppResult sendTodo(com.jecn.webservice.nine999.NotifyTodoSendContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.nine999.Exception;
    public com.jecn.webservice.nine999.NotifyTodoAppResult deleteTodo(com.jecn.webservice.nine999.NotifyTodoRemoveContext arg0) throws java.rmi.RemoteException, com.jecn.webservice.nine999.Exception;
}
