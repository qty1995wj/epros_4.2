package com.jecn.webservice.cetc29;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.HashMap;

import javax.xml.namespace.QName;

import org.codehaus.xfire.XFireRuntimeException;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.annotations.AnnotationServiceFactory;
import org.codehaus.xfire.annotations.jsr181.Jsr181WebAnnotations;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.jaxb2.JaxbTypeRegistry;
import org.codehaus.xfire.service.Endpoint;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.soap.AbstractSoapBinding;
import org.codehaus.xfire.transport.TransportManager;

import com.jecn.epros.server.action.web.login.ad.cetc29.JecnCetc29AfterItem;

public class MasterDataServiceClient {

	private static XFireProxyFactory proxyFactory = new XFireProxyFactory();
	private HashMap endpoints = new HashMap();
	private Service service0;

	public MasterDataServiceClient() {
		create0();
		Endpoint MasterDataServicePortTypeLocalEndpointEP = service0.addEndpoint(new QName(
				"http://www.meritit.com/datamanage/MasterDataService", "MasterDataServicePortTypeLocalEndpoint"),
				new QName("http://www.meritit.com/datamanage/MasterDataService",
						"MasterDataServicePortTypeLocalBinding"), "xfire.local://MasterDataService");
		endpoints.put(new QName("http://www.meritit.com/datamanage/MasterDataService",
				"MasterDataServicePortTypeLocalEndpoint"), MasterDataServicePortTypeLocalEndpointEP);
		Endpoint MasterDataServiceHttpPortEP = service0.addEndpoint(new QName(
				"http://www.meritit.com/datamanage/MasterDataService", "MasterDataServiceHttpPort"), new QName(
				"http://www.meritit.com/datamanage/MasterDataService", "MasterDataServiceHttpBinding"),
				JecnCetc29AfterItem.WSDL_URL);
		endpoints.put(new QName("http://www.meritit.com/datamanage/MasterDataService", "MasterDataServiceHttpPort"),
				MasterDataServiceHttpPortEP);
	}

	public Object getEndpoint(Endpoint endpoint) {
		try {
			return proxyFactory.create((endpoint).getBinding(), (endpoint).getUrl());
		} catch (MalformedURLException e) {
			throw new XFireRuntimeException("Invalid URL", e);
		}
	}

	public Object getEndpoint(QName name) {
		Endpoint endpoint = ((Endpoint) endpoints.get((name)));
		if ((endpoint) == null) {
			throw new IllegalStateException("No such endpoint!");
		}
		return getEndpoint((endpoint));
	}

	public Collection getEndpoints() {
		return endpoints.values();
	}

	private void create0() {
		TransportManager tm = (org.codehaus.xfire.XFireFactory.newInstance().getXFire().getTransportManager());
		HashMap props = new HashMap();
		props.put("annotations.allow.interface", true);
		AnnotationServiceFactory asf = new AnnotationServiceFactory(new Jsr181WebAnnotations(), tm,
				new AegisBindingProvider(new JaxbTypeRegistry()));
		asf.setBindingCreationEnabled(false);
		service0 = asf.create((com.jecn.webservice.cetc29.MasterDataServicePortType.class), props);
		{
			AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName(
					"http://www.meritit.com/datamanage/MasterDataService", "MasterDataServicePortTypeLocalBinding"),
					"urn:xfire:transport:local");
		}
		{
			AbstractSoapBinding soapBinding = asf.createSoap11Binding(service0, new QName(
					"http://www.meritit.com/datamanage/MasterDataService", "MasterDataServiceHttpBinding"),
					"http://schemas.xmlsoap.org/soap/http");
		}
	}

	public MasterDataServicePortType getMasterDataServicePortTypeLocalEndpoint() {
		return ((MasterDataServicePortType) (this).getEndpoint(new QName(
				"http://www.meritit.com/datamanage/MasterDataService", "MasterDataServicePortTypeLocalEndpoint")));
	}

	public MasterDataServicePortType getMasterDataServicePortTypeLocalEndpoint(String url) {
		MasterDataServicePortType var = getMasterDataServicePortTypeLocalEndpoint();
		org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
		return var;
	}

	public MasterDataServicePortType getMasterDataServiceHttpPort() {
		return ((MasterDataServicePortType) (this).getEndpoint(new QName(
				"http://www.meritit.com/datamanage/MasterDataService", "MasterDataServiceHttpPort")));
	}

	public MasterDataServicePortType getMasterDataServiceHttpPort(String url) {
		MasterDataServicePortType var = getMasterDataServiceHttpPort();
		org.codehaus.xfire.client.Client.getInstance(var).setUrl(url);
		return var;
	}

	public static void main(String[] args) {

		MasterDataServiceClient client = new MasterDataServiceClient();

		//Dept��RY
		// create a default service endpoint
		MasterDataServicePortType service = client.getMasterDataServiceHttpPort();

		// �ֶ����Ǵ�д��
		service.getMasterData("", "", "Dept", "", "");
		
		service.getMasterData("", "", "RY", "", "");
		// TODO: Add custom client code here
		//
		// service.yourServiceOperationHere();

		System.out.println("test client completed");
		System.exit(0);
	}

}
