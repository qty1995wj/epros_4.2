package com.jecn.webservice.goldenDragon.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;
import com.jecn.epros.server.service.task.search.IJecnTaskSearchService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.webservice.goldenDragon.IGoldenDragonWebService;
import com.jecn.webservice.goldenDragon.bean.TaskApprovalRecords;

@WebService
public class GoldenDragonWebServiceImpl implements IGoldenDragonWebService {
	private static final Logger log = Logger.getLogger(GoldenDragonWebServiceImpl.class);

	@Override
	@WebMethod
	public String releaseTask(String taskId, List<TaskApprovalRecords> approvalRecords) {
		IJecnBaseTaskService baseTaskService = (IJecnBaseTaskService) ApplicationContextUtil.getContext().getBean(
				"flowTaskServiceImpl");
		if (StringUtils.isBlank(taskId)) {
			return " 发布任务， 获取taskId 为空！";
		}
		try {
			List<JecnTaskHistoryFollow> taskHistorys = getHistoryFollows(approvalRecords);
			baseTaskService.releaseTaskByWebService(Long.valueOf(taskId), taskHistorys);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			log.error("任务发布异常！", e);
		}
		return "false";
	}

	@Override
	@WebMethod
	public String deleteTask(String taskId) {
		IJecnTaskSearchService searchService = (IJecnTaskSearchService) ApplicationContextUtil.getContext().getBean(
				"searchServiceImpl");
		if (StringUtils.isBlank(taskId)) {
			return " 删除任务， 获取taskId 为空！";
		}
		try {
			searchService.falseDeleteTask(Long.valueOf(taskId));
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			log.error("删除任务异常！", e);
		}
		return "false";
	}

	private List<JecnTaskHistoryFollow> getHistoryFollows(List<TaskApprovalRecords> approvalRecords) {
		if (approvalRecords == null) {
			return new ArrayList<JecnTaskHistoryFollow>();
		}
		JecnTaskHistoryFollow taskHistoryFollow = null;
		List<JecnTaskHistoryFollow> taskHistorys = new ArrayList<JecnTaskHistoryFollow>();
		int sort = 0;
		for (TaskApprovalRecords taskApprovalRecords : approvalRecords) {
			taskHistoryFollow = new JecnTaskHistoryFollow();
			// 审核阶段名称
			taskHistoryFollow.setAppellation(taskApprovalRecords.getAppellation());
			taskHistoryFollow.setSort(sort);
			// 更新时间(操作时间)
			taskHistoryFollow.setApprovalTime(JecnCommon.getDateByString(taskApprovalRecords.getApprovalTime()));
			taskHistoryFollow.setName(taskApprovalRecords.getName());
			// 审批阶段状态
			taskHistoryFollow.setStageMark(StringUtils.isBlank(taskApprovalRecords.getStage()) ? -1 : Integer
					.valueOf(taskApprovalRecords.getStage()));
			sort++;
			taskHistorys.add(taskHistoryFollow);
		}
		return taskHistorys;
	}
}
