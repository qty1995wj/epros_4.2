package com.jecn.epros.service.web.parameter;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by Administrator on 2018/11/27.
 */
public class IflytekParameter {
    //待办人域账号
    private String receiver;
    //待办处理状态(100:待办101:已办)，可传递空   只查待办
    private String handleStatus;
    //流程id，可传递空 （也是任务ID）
    private String flowId;
    //页容量(每页多少条数据)，约定15条
    private String pageSize;
    //页码(从1开始)
    private String pageIndex;
    //流程任务创建时间(yyyyMMdd)，可传递空
    private String startDate;
    //流程任务结束时间(yyyyMMdd)，可传递空
    private String endDate;


    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getHandleStatus() {
        return handleStatus;
    }

    public void setHandleStatus(String handleStatus) {
        this.handleStatus = handleStatus;
    }

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(String pageIndex) {
        this.pageIndex = pageIndex;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
