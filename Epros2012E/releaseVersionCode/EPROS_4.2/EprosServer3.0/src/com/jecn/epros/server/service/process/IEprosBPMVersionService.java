package com.jecn.epros.server.service.process;

import java.util.List;

import com.jecn.epros.server.bean.process.EprosBPMVersion;

public interface IEprosBPMVersionService {

	/**
	 * 根据流程ID获取 (EPROS与BPM对应版本信息)
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<EprosBPMVersion> findEprosBpmVersions(String flowId) throws Exception;
}
