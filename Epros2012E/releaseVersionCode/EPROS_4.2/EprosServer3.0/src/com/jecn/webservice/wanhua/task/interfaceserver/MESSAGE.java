package com.jecn.webservice.wanhua.task.interfaceserver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MESSAGE complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name=&quot;MESSAGE&quot;&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base=&quot;{http://www.w3.org/2001/XMLSchema}anyType&quot;&gt;
 *       &lt;sequence&gt;
 *         &lt;element name=&quot;HEADER&quot; type=&quot;{http://interfaceServer.portal.wanhua.com/}HEADER&quot;/&gt;
 *         &lt;element name=&quot;REQUEST&quot; type=&quot;{http://interfaceServer.portal.wanhua.com/}REQUEST&quot;/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MESSAGE", propOrder = { "header", "request" })
public class MESSAGE {

	@XmlElement(name = "HEADER", required = true)
	protected HEADER header;
	@XmlElement(name = "REQUEST", required = true)
	protected REQUEST request;

	/**
	 * Gets the value of the header property.
	 * 
	 * @return possible object is {@link HEADER }
	 * 
	 */
	public HEADER getHEADER() {
		return header;
	}

	/**
	 * Sets the value of the header property.
	 * 
	 * @param value
	 *            allowed object is {@link HEADER }
	 * 
	 */
	public void setHEADER(HEADER value) {
		this.header = value;
	}

	/**
	 * Gets the value of the request property.
	 * 
	 * @return possible object is {@link REQUEST }
	 * 
	 */
	public REQUEST getREQUEST() {
		return request;
	}

	/**
	 * Sets the value of the request property.
	 * 
	 * @param value
	 *            allowed object is {@link REQUEST }
	 * 
	 */
	public void setREQUEST(REQUEST value) {
		this.request = value;
	}

}
