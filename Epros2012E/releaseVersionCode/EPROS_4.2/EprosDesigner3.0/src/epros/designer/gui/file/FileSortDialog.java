package epros.designer.gui.file;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnSortDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class FileSortDialog extends JecnSortDialog {
	private static Logger log = Logger.getLogger(FileSortDialog.class);
	public FileSortDialog(JecnTreeNode pNode, JecnTree jTree){
		super(pNode,jTree);
	}
	@Override
	public List<JecnTreeBean> getChild() {
		try {
			return ConnectionPool.getFileAction().getChildFiles(this.getSelectNode().getJecnTreeBean().getId(),JecnConstants.projectId);
		} catch (Exception e) {
			log.error("FileSortDialog getChild is error",e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public boolean saveData(List<JecnTreeDragBean> list) {
		try {
			ConnectionPool.getFileAction().updateSortFiles(list, this.getSelectNode().getJecnTreeBean().getId(),JecnConstants.getUserId());
			return true;
		} catch (Exception e) {
			log.error("FileSortDialog saveData is error！",e);
		}
		return false;
	}

}
