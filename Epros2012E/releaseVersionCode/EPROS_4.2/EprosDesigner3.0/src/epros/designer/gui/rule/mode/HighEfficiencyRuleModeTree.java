package epros.designer.gui.rule.mode;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class HighEfficiencyRuleModeTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(HighEfficiencyRuleModeTree.class);
	private List<JecnTreeBean> list=new ArrayList<JecnTreeBean>();
	
	/** 制度模板对话框 */
	private RuleModeManageDialog jecnManageDialog = null;

	public HighEfficiencyRuleModeTree(RuleModeManageDialog jecnManageDialog){
		this.jecnManageDialog=jecnManageDialog;
	}
	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RuleModeTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.ruleModeRoot,JecnProperties.getValue("ruleMode"));//"制度模板"
		
		try {
			list=ConnectionPool.getRuleModeAction().getChildRuleMode(0L);
			JecnTreeCommon.addNLevelNodes(list, rootNode);
		} catch (Exception e) {
			log.error("HighEfficiencyRuleModeTree getTreeModel is error",e);
		}
		return new JecnTreeModel(rootNode) ;
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {

		/** 点击左键 */
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			if (evt.getClickCount() == 2) {
				TreePath[] treePath = this.getSelectionPaths();
				if (treePath != null && treePath.length == 1) {
					JecnTreeNode node = (JecnTreeNode) treePath[0]
							.getLastPathComponent();
					JecnTreeCommon.autoExpandNode(this, node);
				}
			}
		}
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = this.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick
						.getLastPathComponent();
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean()
						.getTreeNodeType();
				//保存去掉不同类型后的节点
				List<JecnTreeNode> listSelectNode = new ArrayList<JecnTreeNode>();
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath
								.getLastPathComponent();
						if (treeNodeType.equals(nodeSelect.getJecnTreeBean()
								.getTreeNodeType())) {
							listSelectNode.add(nodeSelect);
							listTreePaths.add(treePath);
						}
					}
				}
				if (listSelectNode.size() > 0) {
					RuleModeMenu menu = new RuleModeMenu(this,jecnManageDialog);

					if (listSelectNode.size() == 1) {
						this.setSelectionPath(listTreePaths.get(0));
						menu.setListNode(listSelectNode);
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
						return;
					}
					// 多选择时去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon.repeatChildNodes(listSelectNode, this);
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath
										.getLastPathComponent();
								if (node.getJecnTreeBean()
										.getId()
										.equals(nodeSelect.getJecnTreeBean()
												.getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}
						if (treePaths.length == 1) {
							this.setSelectionPath(treePaths[0]);
						} else {
							this.setSelectionPaths(treePaths);
						}
						menu.setListNode(listRemoveChild);
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
					}
				}

			}
		}
	
	}

}
