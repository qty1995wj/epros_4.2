package com.jecn.epros.server.connector.Libang;

/**
 * 立邦 - 待办完成对象
 * 
 * @author zxh
 * 
 */
public class LibangFinishBean {
	private String AppCode;
	private String RefID;
	private String Receiver;

	public String getAppCode() {
		return AppCode;
	}

	public void setAppCode(String appCode) {
		AppCode = appCode;
	}

	public String getRefID() {
		return RefID;
	}

	public void setRefID(String refID) {
		RefID = refID;
	}

	public String getReceiver() {
		return Receiver;
	}

	public void setReceiver(String receiver) {
		Receiver = receiver;
	}

	public String toString() {
		return "AppCode=" + AppCode + " RefID=" + RefID + " Receiver=" + Receiver;
	}
}
