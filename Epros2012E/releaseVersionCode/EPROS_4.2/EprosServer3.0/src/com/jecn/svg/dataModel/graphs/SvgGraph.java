package com.jecn.svg.dataModel.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jecn.svg.dataModel.SvgBaseGraph;
import com.jecn.svg.dataModel.graphs.SvgLinkIcon.IconName;

public class SvgGraph extends SvgBaseGraph {

	private Map<IconName, List<SvgLinkIcon>> linkIcons = new HashMap<IconName, List<SvgLinkIcon>>();

	private List<String[]> hoverTexts;

	private Long x;
	private Long y;
	// 宽和高 默认输出json时减去1像素（为和设计器保持一致）
	private Long width;
	private Long height;

	/** 用于存储Epros注释框中额外的连接线数据（对于其他图形无效，如果以后有其他图形需要额外的数据存储，也可使用此字段） */
	private String specialData;

	/** true：当前元素为登录人参与图形 */
	private boolean isMyLink;

	public Long getX() {
		return x;
	}

	public SvgGraph setX(Long x) {
		this.x = x;
		return this;
	}

	public Long getY() {
		return y;
	}

	public SvgGraph setY(Long y) {
		this.y = y;
		return this;
	}

	public Long getWidth() {

		return width;
	}

	public SvgGraph setWidth(Long width) {
		this.width = width;
		return this;
	}

	public Long getHeight() {
		return height;
	}

	public SvgGraph setHeight(Long height) {
		this.height = height;
		return this;
	}

	public Map<IconName, List<SvgLinkIcon>> getLinkIcons() {
		return linkIcons;
	}

	/**
	 * 添加引用的图标
	 * 
	 * @param linkIcon
	 */
	public void addLinkIcon(SvgLinkIcon linkIcon) {
		if (linkIcon == null) {
			return;
		}
		if (linkIcons.containsKey(linkIcon.getIconName())) {
			linkIcons.get(linkIcon.getIconName()).add(linkIcon);
		} else {
			List<SvgLinkIcon> store = new ArrayList<SvgLinkIcon>();
			store.add(linkIcon);
			linkIcons.put(linkIcon.getIconName(), store);
		}
	}

	public String getSpecialData() {
		return specialData;
	}

	public void setSpecialData(String specialData) {
		this.specialData = specialData;
	}

	public List<String[]> getHoverTexts() {
		return hoverTexts;
	}

	public void addHoverText(String[] hoverText) {
		if (hoverText.length != 2) {
			throw new RuntimeException("参数必须是长度为2的数组，[0]为key，[1]为value");
		}
		if (hoverTexts == null) {
			hoverTexts = new ArrayList<String[]>();
		}
		this.hoverTexts.add(hoverText);
	}

	@JsonProperty("isMyLink")
	public boolean isMyLink() {
		return isMyLink;
	}

	public void setMyLink(boolean isMyLink) {
		this.isMyLink = isMyLink;
	}

	@Deprecated
	public void resetWidthAndHeightByStyle() {
		if (getGraphStyle() == null) {
			return;
		}
		if (StringUtils.isBlank(getGraphStyle().getShadow()) && width != null) {
			if (getGraphStyle().getFill() != null && !getGraphStyle().getFill().equals(getGraphStyle().getStroke())) {
				width -= 1;
			}
		} else {
			this.width -= 4;
		}
		if (StringUtils.isBlank(getGraphStyle().getShadow()) && height != null) {
			if (getGraphStyle().getFill() != null && !getGraphStyle().getFill().equals(getGraphStyle().getStroke())) {
				height -= 1;
			}
		} else {
			height -= 4;
		}
	}
}
