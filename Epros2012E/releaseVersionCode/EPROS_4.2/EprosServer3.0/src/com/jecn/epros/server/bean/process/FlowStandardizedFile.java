package com.jecn.epros.server.bean.process;

import java.io.Serializable;

/**
 * 流程支撑文件 关联表(流程和文档库)
 * 
 * @author ZXH
 * @date 2017-4-21上午10:49:35
 */
public class FlowStandardizedFile implements Serializable {
	private String id;
	private Long flowId;
	private Long fileId;
	private String fileName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
