package wordxml.element.dom.paragraph;

import wordxml.element.bean.paragraph.GroupBean;
import wordxml.element.dom.IBody;
import wordxml.element.dom.IBodyElement;
import wordxml.element.dom.page.Sect;

/**
 * 图文框
 */
public class Group extends IBody implements IBodyElement {

	/** 所属节 */
	private Sect parent;
	private GroupBean groupBean = new GroupBean();

	public Group(Sect sect) {
		this.parent = sect;
	}

	public GroupBean getGroupBean() {
		return groupBean;
	}

	public void setGroupBean(GroupBean groupBean) {
		this.groupBean = groupBean;
	}

	public Sect getParent() {
		return parent;
	}

	public void setParent(Sect parent) {
		this.parent = parent;
	}
	
}
