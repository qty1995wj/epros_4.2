package com.jecn.epros.server.action.rmi.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.checkout.CheckoutResult;
import com.jecn.epros.bean.checkout.CheckoutRoleResult;
import com.jecn.epros.server.bean.download.JecnCreateDoc;
import com.jecn.epros.server.bean.process.temp.TempSearchKpiBean;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.service.process.IFlowPKIService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.reports.IReportDownExcelService;
import com.jecn.epros.server.service.reports.IWebReportsService;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceBean;
import com.jecn.epros.service.IProcessRPCService;

public class ProcessRPCServiceImpl implements IProcessRPCService {

	/** 任务log */
	private Logger log = Logger.getLogger(ProcessRPCServiceImpl.class);

	private IFlowStructureService structureService;
	private IFlowPKIService flowPKIService;
	private IWebReportsService webReportsService;
	private IReportDownExcelService reportDownExcelService;

	/**
	 * 流程KPI 导入
	 * 
	 * @param map
	 * @return
	 */
	@Override
	public String excelImporteFlowKpiImage(Map<String, Object> map) {
		Long flowId = Long.valueOf(map.get("flowId").toString());
		Long kpiAndId = Long.valueOf(map.get("kpiAndId").toString());

		ByteFile upload = (ByteFile) map.get("file");
		String kpiHorType = map.get("kpiHorType").toString();
		String startTime = map.get("startTime").toString();
		String endTime = map.get("endTime").toString();
		Long userId = Long.valueOf(map.get("userId").toString());
		File tempFile = null;
		// 写出到临时文件
		try {
			tempFile = JecnFinal.writeFileToTempDir(upload.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("流程KPI 导入 写入临时文件异常！", e);
		}
		return structureService.excelImporteFlowKpiImage(flowId, kpiAndId, tempFile, kpiHorType, startTime, endTime,
				userId);
	}

	@Override
	public ByteFile exportKpi(Map<String, Object> params) {
		ByteFile byteFile = null;
		try {
			TempSearchKpiBean searchKpiBean = new TempSearchKpiBean();
			searchKpiBean.setStartTime(params.get("startTime").toString());
			searchKpiBean.setEndTime(params.get("endTime").toString());
			searchKpiBean.setKpiId((Long) params.get("kpiId"));
			byteFile = new ByteFile();
			JecnCreateDoc exportFlowKpi = flowPKIService.exportFlowKpi(searchKpiBean);
			byteFile.setBytes(exportFlowKpi.getBytes());
			byteFile.setFileName(exportFlowKpi.getFileName());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("RMI导出kpi异常", e);
		}
		return byteFile;
	}

	@Override
	public ByteFile downloadExpiryMaintenance(String orgIds, String mapIds, Long projectId, int type) {
		ByteFile byteFile = new ByteFile();
		try {
			ProcessExpiryMaintenanceBean expiryMaintenanceBean = webReportsService.findProcessExpiryMaintenanceBean(
					orgIds, mapIds, projectId, type);
			byte[] bytes = reportDownExcelService.downloadProcessExpiryMaintenanceBean(expiryMaintenanceBean);
			String fileName = "有效期维护.xls";
			byteFile.setBytes(bytes);
			byteFile.setFileName(fileName);
		} catch (Exception e) {
			log.error("downloadExpiryMaintenance异常", e);
		}
		return byteFile;
	}

	@Override
	public ByteFileResult downloadExpiryMaintenanceErrorData(Long peopleId, int type) {
		ByteFileResult result = new ByteFileResult();
		try {
			byte[] bytes = reportDownExcelService.downloadProcessExpiryMaintenanceErrorDate(peopleId, type);
			if (bytes == null || bytes.length == 0) {
				result.setSuccess(false);
				result.setResultMessage("没有错误数据");
			} else {
				result.setSuccess(true);
				result.setBytes(bytes);
				result.setFileName("错误数据.xls");
			}

		} catch (Exception e) {
			result.setSuccess(false);
			result.setResultMessage("下载有效期维护信息错误");
		}

		return result;
	}

	@Override
	public MessageResult importExpiryMaintenance(ByteFile byteFile, Long peopleId, int type) {
		MessageResult result = new MessageResult();
		try {
			File tempFile = JecnFinal.writeFileToTempDir(byteFile.getBytes());
			// 返回是否有错误数据的信息
			int[] resultArray = structureService.importValidityData(tempFile, peopleId, type);
			result.setSuccess(true);
			// 导入 条，未导入 条
			result.setResultMessage("导入" + resultArray[0] + "条，未导入" + resultArray[1] + "条");
		} catch (Exception e) {
			result.setSuccess(false);
			result.setResultMessage("导入有效期维护数据异常");
			log.error("", e);
		}
		return result;
	}

	public IFlowStructureService getStructureService() {
		return structureService;
	}

	public void setStructureService(IFlowStructureService structureService) {
		this.structureService = structureService;
	}

	public IFlowPKIService getFlowPKIService() {
		return flowPKIService;
	}

	public void setFlowPKIService(IFlowPKIService flowPKIService) {
		this.flowPKIService = flowPKIService;
	}

	public IWebReportsService getWebReportsService() {
		return webReportsService;
	}

	public void setWebReportsService(IWebReportsService webReportsService) {
		this.webReportsService = webReportsService;
	}

	public IReportDownExcelService getReportDownExcelService() {
		return reportDownExcelService;
	}

	public void setReportDownExcelService(IReportDownExcelService reportDownExcelService) {
		this.reportDownExcelService = reportDownExcelService;
	}

	@Override
	public ByteFile downLoadCheckout(Map<String, Object> map) {
		return structureService.downLoadCheckout(Long.valueOf(map.get("flowId").toString()));
	}

	@Override
	public List<CheckoutResult> getCheckout(Map<String, Object> map) {
		return structureService.checkout(Long.valueOf(map.get("flowId").toString()));
	}

	@Override
	public ByteFile downLoadCheckoutRole(Map<String, Object> map) {
		return structureService.downLoadCheckoutRole(Long.valueOf(map.get("flowId").toString()));
	}

	@Override
	public List<CheckoutRoleResult> getCheckoutRole(Map<String, Object> map) {
		return structureService.checkoutRole(Long.valueOf(map.get("flowId").toString()));
	}

}
