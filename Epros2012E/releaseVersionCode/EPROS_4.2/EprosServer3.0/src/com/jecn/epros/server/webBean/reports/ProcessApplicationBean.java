package com.jecn.epros.server.webBean.reports;

import java.util.List;

import com.jecn.epros.server.webBean.process.ProcessRoleWebBean;
import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 流程活动数统计
 * 
 * @author fuzhh Feb 28, 2013
 * 
 */
public class ProcessApplicationBean {
	/** 流程名称 */
	private String processName;
	/** 流程ID */
	private long processId;
	/** 流程涉及角色个数 */
	private int roleNum;
	/** 人数 */
	private int peNum;

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public long getProcessId() {
		return processId;
	}

	public void setProcessId(long processId) {
		this.processId = processId;
	}

	public int getRoleNum() {
		return roleNum;
	}

	public void setRoleNum(int roleNum) {
		this.roleNum = roleNum;
	}

	public int getPeNum() {
		return peNum;
	}

	public void setPeNum(int peNum) {
		this.peNum = peNum;
	}
}
