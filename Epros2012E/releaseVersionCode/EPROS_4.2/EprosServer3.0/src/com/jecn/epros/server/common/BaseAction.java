package com.jecn.epros.server.common;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport {
	protected static final Logger log = Logger.getLogger(BaseAction.class);
	public static final String ERRORINFO = "errorInfo";
	/** 错误信息 */
	private String errorInfo;

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void outJsonString(String str) {
		getResponse().setContentType("text/html;charset=UTF-8");
		outString(str);
	}

	public void outJsonPage(String data, int total) {
		outJsonString("{\"root\":" + data + ",\"totalProperty\":" + total + "}");
	}

	public void outJsonPageError(String data, int total) {
		outJsonString("{\"root\":" + data + ",\"totalProperty\":" + total + "}");
	}

	/**
	 * 跨域请求，返回json
	 * 
	 * @param callbackFunc
	 *            跨域访问方法名
	 * @param data
	 *            json数据
	 * @param total
	 *            数据总数
	 */
	public void callBackOutPage(String callbackFunc, String data, int total) {
		outJsonString(callbackFunc + "({\"root\":" + data + ",\"totalProperty\":" + total + "})");
	}

	// 自定义的title
	public void outJeon(String title, String data) {
		outJsonString("{\"" + title + "\":" + data + "}");
	}

	public void outString(String str) {
		PrintWriter out = null;
		try {
			out = getResponse().getWriter();
			out.write(str);

			out.flush();
		} catch (IOException e) {
			log.error("", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception ex) {
					log.error("关闭PrintWriter out流程异常", ex);
				}
			}
		}
	}

	protected void outJsonForSuccessParam(Boolean flag, String param, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			response.setContentType("text/json; charset=UTF-8");
			out = response.getWriter();
			out.write("{\"success\":" + flag + "," + param + "}");
			out.flush();
		} catch (IOException e) {
			log.error("", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception ex) {
					log.error("outJsonForSuccessParam关闭PrintWriter out流程异常", ex);
				}
			}
		}
	}

	public void outXMLString(String xmlStr) {
		getResponse().setContentType("application/xml;charset=UTF-8");
		outString(xmlStr);
	}

	/**
	 * 获得request
	 * 
	 * @return
	 */
	public HttpServletRequest getRequest() {
		return ServletActionContext.getRequest();
	}

	/**
	 * 获得response
	 * 
	 * @return
	 */
	public HttpServletResponse getResponse() {
		return ServletActionContext.getResponse();
	}

	/**
	 * 获得session
	 * 
	 * @return
	 */
	public HttpSession getSession() {
		return getRequest().getSession();
	}

	public long getProjectId() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		return webLoginBean.getProjectId();
	}

	public long getPeopleId() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		return webLoginBean.getJecnUser().getPeopleId();
	}

	public JecnUser getJecnUser() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		return webLoginBean.getJecnUser();
	}

	public boolean isAdmin() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		boolean flag = false;
		if (webLoginBean.isAdmin()) {
			flag = true;
		} else if (webLoginBean.isViewAdmin()) {
			flag = true;
		}
		return flag;
	}

	public boolean isAdminExcludeViewAdmin() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		boolean flag = false;
		if (webLoginBean.isAdmin()) {
			flag = true;
		}
		return flag;
	}

	public boolean isSecondAdmin() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		boolean flag = false;
		if (webLoginBean.isSecondAdmin()) {
			flag = true;
		}
		return flag;
	}

	/**
	 * 
	 * 拼装前台界面提示页面
	 * 
	 * @param info
	 * @param href
	 */
	public void ResultHtml(String info, String href) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			getResponse().setContentType("text/html;charset=UTF-8");
			out = getResponse().getWriter();
			if (isNullObj(out)) {
				return;
			}
			out.println("<script language='javascript'>");
			out.println("alert('" + info + "');");
			if (!JecnCommon.isNullOrEmtryTrim(href)) {
				out.println("window.location.href='" + href + "';");
			}
			out.println(" window.opener=null;window.open('','_self');window.close();");
			out.println("</script>");

			out.flush();
		} catch (IOException e) {
			log.error("", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
					log.error("ResultHtml方法：关闭PrintWriter out流程异常", e);
				}
			}
		}
	}

	/**
	 * 
	 * 拼装前台界面提示页面
	 * 
	 * @param info
	 * @param href
	 */
	public void outResultHtml(String info, String href) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			getResponse().setContentType("text/html;charset=UTF-8");
			out = getResponse().getWriter();
			if (isNullObj(out)) {
				return;
			}
			out.println("<script language='javascript'>");
			out.println("alert('" + info + "');");
			if (!JecnCommon.isNullOrEmtryTrim(href)) {
				out.println("window.location.href='" + href + "';");
			} else {
				out.println("window.history.go(-1)");
			}
			out.println("</script>");

			out.flush();
		} catch (IOException e) {
			log.error("", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
					log.error("outResultHtml方法：关闭PrintWriter out流程异常", e);
				}
			}
		}
	}

	public void outOrgHtml(String info) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			getResponse().setContentType("text/html;charset=UTF-8");
			out = getResponse().getWriter();
			if (isNullObj(out)) {
				return;
			}
			out.println("<script language='javascript'>");
			out.println("alert('" + info + "');");
			out.println("window.opener=null;window.open('','_self');window.close();");
			out.println("</script>");
			out.println(info);

			out.flush();
		} catch (IOException e) {
			log.error("", e);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
					log.error("outOrgHtml方法：关闭PrintWriter out流程异常", e);
				}
			}
		}
	}

	/**
	 * 
	 * 给定参数是否为null
	 * 
	 * @param obj
	 *            Object
	 * @return
	 */
	public boolean isNullObj(Object obj) {
		return (obj == null) ? true : false;
	}

	/**
	 * 获得servlet上下文
	 * 
	 * @return
	 */
	public ServletContext getServletContext() {
		return ServletActionContext.getServletContext();
	}

	public String getRealyPath(String path) {
		return getServletContext().getRealPath(path);
	}
}
