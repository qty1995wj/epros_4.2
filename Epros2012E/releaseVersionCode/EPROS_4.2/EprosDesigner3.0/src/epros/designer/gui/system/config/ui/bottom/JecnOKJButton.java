package epros.designer.gui.system.config.ui.bottom;

import java.awt.event.ActionEvent;
import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.JecnD2;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigDesgBean;
import epros.designer.gui.system.config.ui.JecnAbstractBaseJButton;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnFlowConfigJDialog;
import epros.designer.gui.workflow.WorkFlowBakTimer;
import epros.designer.gui.workflow.WorkFlowTimer;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.system.data.JecnSystemStaticData;

/**
 * 
 * 确认按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnOKJButton extends JecnAbstractBaseJButton {
	public JecnOKJButton(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		this.setPreferredSize(null);
		this.setMaximumSize(null);
		this.setMinimumSize(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		boolean ret = dialog.getPropertyPanel().check();
		if (!ret) {// 校验没有通过
			return;
		}
		// 保存数据到数据库
		// 数据对象
		JecnConfigDesgBean configBean = dialog.getConfigBean();
		List<JecnConfigItemBean> itemList = configBean.getConfigItemBeanList();
		if (itemList == null || itemList.size() == 0) {
			return;
		}

		// 执行更新
		ret = ConnectionPool.getConfigAciton().update(itemList, dialog.getConfigBean().getBigType());

		if (!ret) {
			// 提交失败，请重新提交或联系管理员！
			dialog.getOkCancelJButtonPanel().getInfoLabel().setText(JecnProperties.getValue("submitFail"));
		} else {
			// 自动保存定时器刷新
			autoSaveRefresh(itemList);
			// 自动备份定时器刷新、
			autoBakTimerRefresh(itemList);
			dialog.setVisible(false);

			// 更新缓存数据
			updateConfigItemData(itemList);
		}
	}

	/**
	 * 自动备份定时器刷新
	 * 
	 * @author weidp
	 * @date 2014-10-27 下午07:13:17
	 * @param itemList
	 *            自动备份配置内容
	 */
	private void autoBakTimerRefresh(List<JecnConfigItemBean> itemList) {
		// 自动备份
		String autoBak = ConfigItemPartMapMark.saveRecovery.toString();
		// 自动备份时间
		String autoBakTime = ConfigItemPartMapMark.saveRecoveryValue.toString();
		JecnConfigItemBean[] configItemBean = new JecnConfigItemBean[2];
		for (JecnConfigItemBean itemBean : itemList) {
			if (autoBak.equals(itemBean.getMark())) { // 是否启用自动存档;自动存档时间：分钟数
				// 刷新定时器
				configItemBean[0] = itemBean;
			} else if (autoBakTime.equals(itemBean.getMark())) {
				configItemBean[1] = itemBean;
			}
		}
		WorkFlowBakTimer.getInstance().reset(configItemBean);
	}

	/**
	 * 
	 * 
	 * 更新缓存数据
	 * 
	 * @param itemBean
	 *            JecnConfigItemBean
	 */
	private static void updateConfigItemData(List<JecnConfigItemBean> itemList) {
		if (itemList == null) {
			return;
		}
		List<JecnConfigItemBean> peopleItemList = ConnectionPool.getConfigAciton().selectConfigItemBeanByType(1, 10);
		for (JecnConfigItemBean itemBean : itemList) {
			if (JecnConfigContents.TYPE_BIG_ITEM_PARTMAP == itemBean.getTypeBigModel()
					&& JecnConfigContents.TYPE_SMALL_FLOW_BASIC == itemBean.getTypeSmallModel()) {// 流程属性
				updateTaskAppData(peopleItemList, itemBean);
				if (ConfigItemPartMapMark.otherEditPort.toString().equals(itemBean.getMark())) {
					JecnSystemStaticData.setEditPortType(Integer.valueOf(itemBean.getValue()));
				}
			}
		}
	}

	/**
	 * 
	 * 使用场合必须保证taskAppList中已经存在数据(除文控主动类型)
	 * 
	 * 更新对应ID的审批环节数据,没有找到对应IP就不做更新
	 * 
	 * @param taskAppList
	 *            (List<JecnConfigItemBean> 缓存中数据
	 * @param dbItemBean
	 *            JecnConfigItemBean 待更新数据
	 */
	private static void updateTaskAppData(List<JecnConfigItemBean> taskAppList, JecnConfigItemBean dbItemBean) {
		for (JecnConfigItemBean oldItem : taskAppList) {
			int oldID = oldItem.getId();
			int newID = dbItemBean.getId();
			if (oldID != 0 && oldID == newID) {// 同一条数据
				// 名称
				oldItem.setName(dbItemBean.getName());
				// 排序
				oldItem.setSort(dbItemBean.getSort());
				// 必填项
				oldItem.setIsEmpty(dbItemBean.getIsEmpty());
				// 值
				oldItem.setValue(dbItemBean.getValue());
				return;
			}
		}
	}

	/**
	 * 
	 * 自动保存定时器刷新
	 * 
	 * @param itemList
	 */
	private void autoSaveRefresh(List<JecnConfigItemBean> itemList) {
		if (itemList == null || itemList.size() == 0) {
			return;
		}
		String basicIsAutoSave = ConfigItemPartMapMark.basicIsAutoSave.toString();
		String basicAutoSaveTime = ConfigItemPartMapMark.basicAutoSaveTime.toString();
		for (JecnConfigItemBean itemBean : itemList) {
			if (basicIsAutoSave.equals(itemBean.getMark()) || basicAutoSaveTime.equals(itemBean.getMark())) { // 是否启用自动存档;自动存档时间：分钟数
				// 刷新定时器
				WorkFlowTimer.refresh();
				return;
			}
		}
	}

	public void setEnabled(boolean b) {
		if (JecnConstants.versionType == 1 && JecnD2.isTmpKey && dialog != null
				&& dialog instanceof JecnFlowConfigJDialog) {// D2版本试用密钥
			super.setEnabled(false);
		} else {
			super.setEnabled(b);
		}
	}
}
