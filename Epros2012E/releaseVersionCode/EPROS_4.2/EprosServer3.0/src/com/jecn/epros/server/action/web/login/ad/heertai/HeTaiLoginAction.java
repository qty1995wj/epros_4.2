package com.jecn.epros.server.action.web.login.ad.heertai;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

public class HeTaiLoginAction extends JecnAbstractADLoginAction {

	public HeTaiLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		String sys = this.loginAction.getRequest().getParameter("sys");
		String userId = this.loginAction.getRequest().getParameter("userid");
		log.info(sys);
		if (HeTaiAfterItem.getValue("sysoa").equals(sys)) {
			boolean checkFlag = checkLogin(userId);
			if (checkFlag) {
				log.info(userId);
				return loginByLoginName(userId);
			}
		}
		log.error(userId);
		return LoginAction.INPUT;// 口令错误
	}

	public static boolean checkLogin(String userId) {
		try {
			// 直接引用远程的wsdl文件
			// String endpoint =
			// "http://portal.szhittech.com/services/HrLoginService";
			String endpoint = HeTaiAfterItem.getValue("webServiceUrl");
			Service service = new Service();
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(endpoint);
			// WSDL里面描述的接口名称
			call.setOperationName("login");
			call.addParameter("param", org.apache.axis.encoding.XMLType.XSD_DATE, javax.xml.rpc.ParameterMode.IN);// 接口的参数
			call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);//
			// 设置返回类型
			String result = (String) call.invoke(new Object[] { userId }); //
			// 给方法传递参数，并且调用方法
			log.info("SSO result = " + result);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("动态调用webservice接口异常！", e);
			return false;
		}
	}
}
