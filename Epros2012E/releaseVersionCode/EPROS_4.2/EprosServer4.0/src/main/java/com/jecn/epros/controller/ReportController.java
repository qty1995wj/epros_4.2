package com.jecn.epros.controller;

import com.jecn.epros.bean.ByteFileResult;
import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.ChineseEnglisListWebBean;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.home.TopNumberData;
import com.jecn.epros.domain.org.*;
import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.domain.report.*;
import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.FileService;
import com.jecn.epros.service.IChartRPCService;
import com.jecn.epros.service.home.HomeService;
import com.jecn.epros.service.org.OrgService;
import com.jecn.epros.service.process.ProcessService;
import com.jecn.epros.service.report.ReportService;
import com.jecn.epros.service.risk.RiskService;
import com.jecn.epros.service.rule.RuleService;
import com.jecn.epros.service.standard.StandardService;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant.TYPEAUTH;
import com.jecn.epros.util.AttachmentDownloadUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.ReportUtils;
import com.jecn.epros.util.excel.ExcelUtils;
import com.sun.net.httpserver.Authenticator;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class ReportController {

    @Autowired
    private ReportService reportService;

    @Autowired
    private HomeService homeService;

    @Autowired
    private ProcessService processService;

    @Autowired
    private RuleService ruleService;

    @Autowired
    private FileService fileService;

    @Autowired
    private StandardService standardService;

    @Autowired
    private OrgService orgService;

    @Autowired
    private RiskService riskService;

    @Autowired
    private IChartRPCService chartRPCService;

    @PreAuthorize("hasAuthority('reportProcessDetail')")
    @ApiOperation(value = "报表-流程-清单")
    @RequestMapping(value = "/reports/process", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> processDetailView(@ModelAttribute ReportSearchHasTime param, @RequestParam(required = false) String action, HttpServletRequest request)
            throws ParseException {

        List<Long> mapIds = param.getMapIds();
        List<Long> orgIds = param.getOrgIds();
        String endTime = param.getEndTime();
        if (validateMapAndOrgNotPass(mapIds, orgIds) || endTime == null) {
            throw new IllegalArgumentException("ReportController processDetailView is error");
        }

        if (StringUtils.isNotBlank(action) && action.equals("download")) {
            Map<String, Object> paramMap = searchParamToMapParam(param);
            ByteFileResult result = chartRPCService.downloadProcessDetailList(paramMap);
            if (result.isSuccess() == false) {
                throw new EprosBussinessException("ReportController downloadProcessDetailList is error");
            }
            Attachment attachment = new Attachment();
            attachment.setContent(result.getBytes());
            attachment.setName(result.getFileName());
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        } else {
            SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
            Calendar instance = Calendar.getInstance();
            // yyyy-mm-dd
            instance.setTime(formate.parse(endTime.substring(0, 10)));
            instance.set(Calendar.HOUR, 23);
            instance.set(Calendar.MINUTE, 59);
            instance.set(Calendar.SECOND, 59);
            Date endDate = instance.getTime();

            Map<String, Object> map = new HashMap<String, Object>();
            // 项目ID
            map.put("mapIds", mapIds);
            map.put("orgIds", orgIds);
            map.put("endTime", endDate);
            map.put("projectId", AuthenticatedUserUtil.getProjectId());
            return ResponseEntity.ok(reportService.processDetailView(map));
        }
    }

    private Map<String, Object> searchParamToMapParam(ReportSearchHasTime param) {
        Map<String, Object> result = new HashMap<>();
        result.put("orgIds", param.getOrgIds());
        result.put("mapIds", param.getMapIds());
        result.put("startTime", param.getStartTime());
        result.put("endTime", param.getEndTime());
        result.put("type", param.getType());
        result.put("projectId", AuthenticatedUserUtil.getProjectId());
        result.put("orgHasChild", param.isOrgHasChild());
        result.put("dutyIds",param.getDutyIds());
        return result;
    }


    /**
     * 校验参数未通过检查
     *
     * @param mapIds
     * @param orgIds
     * @return true未通过 false通过
     */
    private boolean validateMapAndOrgNotPass(List<Long> mapIds, List<Long> orgIds) {
        if (mapIds != null && mapIds.size() > 0) {
            return false;
        }
        if (orgIds != null && orgIds.size() > 0) {
            return false;
        }
        return true;
    }

    @PreAuthorize("hasAuthority('reportProcessScanOptimize')")
    @ApiOperation(value = "报表-流程、制度-审视优化")
    @RequestMapping(value = "/reports/process/reviews", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> processOptimize(@ModelAttribute ReportSearchHasTime param, @RequestParam(required = false) String action, HttpServletRequest request) {
        List<Long> mapIds = param.getMapIds();
        List<Long> orgIds = param.getOrgIds();
        List<Long> dutyIds = param.getDutyIds();
        String endTime = param.getEndTime();
        String startTime = param.getStartTime();

        if (validateMapAndOrgNotPass(mapIds, orgIds) || startTime == null
                || endTime == null) {
            throw new IllegalArgumentException("ReportController processOptimize is error");
        }

        if (StringUtils.isNotBlank(action) && action.equals("download")) {
            Map<String, Object> paramMap = searchParamToMapParam(param);
            ByteFileResult result = chartRPCService.downloadProcessScanOptimize(paramMap);
            if (result.isSuccess() == false) {
                throw new EprosBussinessException("ReportController downloadProcessScanOptimize is error");
            }
            Attachment attachment = new Attachment();
            attachment.setContent(result.getBytes());
            attachment.setName(result.getFileName());
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        } else {
            Date startDate;
            Date endDate;
            try {
                startDate = getFirstDateOfMonth(startTime);
                endDate = getLastDateOfMonth(endTime);
            } catch (ParseException ex) {
                throw new IllegalArgumentException("ReportController processOptimize is error");
            }

            Map<String, Object> map = new HashMap<String, Object>();
            // 项目ID
            map.put("mapIds", mapIds);
            map.put("orgIds", orgIds);
            map.put("dutyIds", dutyIds);
            map.put("startTime", startDate);
            map.put("endTime", endDate);
            map.put("projectId", AuthenticatedUserUtil.getProjectId());
            map.put("type", param.getType());

            return ResponseEntity.ok(reportService.processOptimizeView(map));
        }
    }

    @PreAuthorize("hasAuthority('reportProcessKpi')")
    @ApiOperation(value = "报表-流程-kpi跟踪")
    @RequestMapping(value = "/reports/process/kpis", method = RequestMethod.GET)
    public ResponseEntity<?> processKpi(@ModelAttribute ReportSearchHasTime param, @RequestParam(required = true) String action, HttpServletRequest request) {
        List<Long> mapIds = param.getMapIds();
        List<Long> orgIds = param.getOrgIds();
        String endTime = param.getEndTime();
        String startTime = param.getStartTime();

        if (validateMapAndOrgNotPass(mapIds, orgIds) || startTime == null
                || endTime == null) {
            throw new IllegalArgumentException("ReportController processKpi is error");
        }

        if (StringUtils.isNotBlank(action) && action.equals("download")) {
            ByteFileResult result = chartRPCService.downloadProcessKPIFollow(param.getOrgIds(), param.getMapIds(), startTime, endTime, AuthenticatedUserUtil.getProjectId());
            if (result.isSuccess() == false) {
                throw new EprosBussinessException("ReportController downloadProcessKPIFollow is error");
            }
            Attachment attachment = new Attachment();
            attachment.setContent(result.getBytes());
            attachment.setName(result.getFileName());
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        } else {
            Date startDate;
            Date endDate;
            try {
                startDate = getFirstDateOfMonth(startTime);
                endDate = getLastDateOfMonth(endTime);
            } catch (ParseException ex) {
                throw new IllegalArgumentException("ReportController  processKpi is error");
            }

            Map<String, Object> map = new HashMap<String, Object>();
            // 项目ID
            map.put("mapIds", mapIds);
            map.put("orgIds", orgIds);
            map.put("startTime", startDate);
            map.put("endTime", endDate);
            map.put("projectId", AuthenticatedUserUtil.getProjectId());

            List<KpiView> views = reportService.processKpiView(map);
            KpiView view = null;
            for (KpiView v : views) {
                if (v.getKpiValues() != null && v.getKpiValues().size() > 0) {
                    view = v;
                    break;
                }
            }
            if (view == null) {
                for (KpiView v : views) {
                    if (v.getKpiId() != null) {
                        view = v;
                        break;
                    }
                }
            }
            if (view == null) {
                view = views.get(0);
            }
            return ResponseEntity.ok(view);
        }
    }

    @PreAuthorize("hasAuthority('reportItPost')")
    @ApiOperation(value = "报表-岗位统计")
    @RequestMapping(value = "/reports/process/post", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> processPostStatistics(@RequestParam(defaultValue = "") String name, InventoryParam param, HttpServletRequest request) throws ParseException {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        String inventoryName = JecnProperties.getValue("position_list_xls") + ".xls";
        String action = param.getAction();
        Paging paging = new Paging();
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        paging.setPageNum(param.getPageNum());
        paging.setPageSize(param.getPageSize());
        map.put("paging", paging);
        map.put("name", name);
        map.put("isDownLoad", action);
        Inventory inventory = orgService.searchPosStatistics(map);
        if (StringUtils.isNotBlank(inventoryName) && StringUtils.isNotBlank(action) && action.equals("download")) {
            Attachment attachment = new Attachment();
            attachment.setContent(ExcelUtils.inventoryToByteArray(inventory));
            attachment.setName(inventoryName);
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        }
        return ResponseEntity.ok(inventory);
    }

    @PreAuthorize("hasAuthority('chineseEnglishList')")
    @ApiOperation(value = "中英文统计报表")
    @RequestMapping(value = "/reports/chineseEnglishList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> chineseEnglishList(InventoryParam param, HttpServletRequest request) throws ParseException {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        String inventoryName = JecnProperties.getValue("chineseEnglishReports") + ".xls";
        String action = param.getAction();
        Paging paging = new Paging();
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        paging.setPageNum(param.getPageNum());
        paging.setPageSize(param.getPageSize());
        map.put("paging", paging);
        map.put("id", param.getId() == null ? 0L : param.getId());
        map.put("isDownLoad", action);
        map.put("isPub", "");
        ChineseEnglisListWebBean webBean = reportService.getChineseEnglishList(map);
        if (StringUtils.isNotBlank(inventoryName) && StringUtils.isNotBlank(action) && action.equals("download")) {
            Attachment attachment = new Attachment();
            attachment.setContent(ExcelUtils.inventoryToByteArray(webBean.getInventory()));
            attachment.setName(inventoryName);
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        }
        return ResponseEntity.ok(webBean);
    }

    @PreAuthorize("hasAuthority('reportItPostGroup')")
    @ApiOperation(value = "报表-岗位组统计")
    @RequestMapping(value = "/reports/process/postGroup", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> processPostGroupStatistics(@RequestParam(defaultValue = "") String name, InventoryParam param, HttpServletRequest request) throws ParseException {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        String inventoryName = JecnProperties.getValue("positionGroup_list_xls") + ".xls";
        String action = param.getAction();
        Paging paging = new Paging();
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        paging.setPageNum(param.getPageNum());
        paging.setPageSize(param.getPageSize());
        map.put("paging", paging);
        map.put("name", name);
        map.put("isDownLoad", action);
        Inventory inventory = orgService.searchPosGroupStatistics(map);
        if (StringUtils.isNotBlank(inventoryName) && StringUtils.isNotBlank(action) && action.equals("download")) {
            Attachment attachment = new Attachment();
            attachment.setContent(ExcelUtils.inventoryToByteArray(inventory));
            attachment.setName(inventoryName);
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        }
        return ResponseEntity.ok(inventory);
    }


    //    @PreAuthorize("resource(#param.getId(),#param.getType())")
    @ApiOperation(value = "报表-清单")
    @RequestMapping(value = "/reports/inventory", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInventory(InventoryParam param, HttpServletRequest request) {
        if (param.getId() == null || param.getType() == null) {
            throw new EprosBussinessException("ReportController getInventory is error");
        }
        String type = param.getType();
        Long id = param.getId();
        String action = param.getAction();

        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("pub", " ");
        map.put("type", type);

        if (!"download".equals(action)) {// 查阅
            Paging paging = new Paging();
            paging.setPageNum(param.getPageNum());
            paging.setPageSize(param.getPageSize());
            map.put("isPaging", true);
            map.put("paging", paging);
        } else {// 下载
            map.put("isPaging", false);
            map.put("downloadType", param.getDownloadType());
        }

        String inventoryName = null;
        Inventory inventory = null;
        switch (type) {
            case "processMap":
                map.put("flowId", id);
                inventory = processService.getInventory(map);
                if (param.getDownloadType() == 0) {
                    //流程清单.xls
                    inventoryName = JecnProperties.getValue("Process_list_xls") + ".xls";
                } else if (param.getDownloadType() == 2) {
                    //流程架构清单.xls
                    inventoryName = JecnProperties.getValue("ProcessMap_list_xls") + ".xls";
                } else {
                    //流程版本清单.xls
                    inventoryName = JecnProperties.getValue("ProcessVersion_list_xls") + ".xls";
                }
                break;
            case "rule":
                map.put("ruleId", id);
                inventory = ruleService.getInventory(map);
                inventoryName = param.getDownloadType() == 0 ?
                        //制度清单
                        JecnProperties.getValue("rule_list_xls") + ".xls"
                        //制度版本清单
                        : JecnProperties.getValue("ruleVersion_list_xls") + ".xls";
                break;
            case "file":
                map.put("id", id);
                inventory = fileService.getInventory(map);
                inventoryName = param.getDownloadType() == 0 ?
                        //文件清单
                        JecnProperties.getValue("file_list_xls") + ".xls"
                        :
                        //文件版本清单
                        JecnProperties.getValue("fileVersion_list_xls") + ".xls";
                break;
            case "risk":
                map.put("id", id);
                inventory = riskService.getInventory(map);
                //风险清单
                inventoryName = JecnProperties.getValue("risk_list_xls") + ".xls";
                break;
            case "standard":
                map.put("id", id);
                inventory = standardService.getInventory(map);
                //标准清单
                inventoryName = JecnProperties.getValue("standard_list_xls") + ".xls";
                break;
            case "organization":
                map.put("orgId", id);
                inventory = orgService.getInventory(map);
                //组织清单
                inventoryName = JecnProperties.getValue("organization_list_xls") + ".xls";
                break;
            case "position":
                map.put("orgId", id);
                inventory = orgService.getPosInventory(map);
                //岗位清单
                inventoryName = JecnProperties.getValue("position_list_xls") + ".xls";
                break;
            default:
                throw new EprosBussinessException("ReportController getInventory is error：type " + type);
        }

        if (StringUtils.isNotBlank(inventoryName) && StringUtils.isNotBlank(action) && action.equals("download")) {
            Attachment attachment = new Attachment();
            attachment.setContent(ExcelUtils.inventoryToByteArray(inventory));
            attachment.setName(inventoryName);
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        }
        return ResponseEntity.ok(inventory);
    }


    @PreAuthorize("hasAuthority('reportIt')")
    @ApiOperation(value = "报表-流程-信息化(搜索条件：部门/架构 单选，结果：曲线图)")
    @RequestMapping(value = "/reports/process/it", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LineChart> processInformation(@ModelAttribute ReportSearchParam param) {
        List<Long> mapIds = param.getMapIds();
        List<Long> orgIds = param.getOrgIds();
        if (validateMapAndOrgNotPass(mapIds, orgIds)) {
            throw new IllegalArgumentException("ReportController processInformation is error");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("mapIds", mapIds);
        map.put("orgIds", orgIds);
        map.put("pub", "");
        map.put("projectId", AuthenticatedUserUtil.getProjectId());

        List<InformationStatistic> statistics = reportService.processInformation(map);
        return ResponseEntity.ok(ReportUtils.pie2ProcessInformationChart(statistics));
    }

    @ApiOperation(value = "报表-流程-信息化-详情(展开关联的活动)")
    @RequestMapping(value = "/reports/process/it/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Inventory> processInformationDetail(@PathVariable Long id,
                                                              @ModelAttribute PartOpenParam param, @ModelAttribute Paging paging) {

        if (param.getId() == null || param.getType() == null || param.getPart() == null) {
            throw new IllegalArgumentException("ReportController processInformationDetail is error");
        }

        Map<String, Object> map = new HashMap<String, Object>();
        List<Long> orgIds = new ArrayList<Long>();
        List<Long> mapIds = new ArrayList<Long>();

        if ("0".equals(param.getType())) {// 部门
            orgIds.add(Long.valueOf(param.getId()));
        } else if ("1".equals(param.getType())) {// 流程
            mapIds.add(Long.valueOf(param.getId()));
        }
        map.put("orgIds", orgIds);
        map.put("mapIds", mapIds);
        map.put("type", param.getType());
        map.put("itId", Long.valueOf(param.getPart()));
        map.put("paging", paging);
        map.put("pub", "");
        map.put("projectId", AuthenticatedUserUtil.getProjectId());

        Inventory inventory = reportService.getInformationInventory(map);
        return ResponseEntity.ok(inventory);
    }

    @PreAuthorize("hasAuthority('reportRole')")
    @ApiOperation(value = "报表-流程-角色")
    @RequestMapping(value = "/reports/process/roles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LineChart> processRoleNum(@ModelAttribute ReportSearchParam param) {
        List<Long> mapIds = param.getMapIds();
        List<Long> orgIds = param.getOrgIds();
        if (validateMapAndOrgNotPass(mapIds, orgIds)) {
            throw new IllegalArgumentException("ReportController processRoleNum is error");
        }

        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("mapIds", mapIds);
        map.put("orgIds", orgIds);
        map.put("pub", "");
        map.put("projectId", AuthenticatedUserUtil.getProjectId());

        List<RoleStatistic> statistics = reportService.processRoleNum(map);
        return ResponseEntity.ok(ReportUtils.pie2RoleLineChart(statistics));
    }

    @ApiOperation(value = "报表-流程-角色-详情")
    @RequestMapping(value = "/reports/process/roles/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Inventory> processRoleDetail(@PathVariable Long id, @ModelAttribute PartOpenParam param,
                                                       @ModelAttribute Paging paging) {
        if (param.getId() == null || param.getType() == null || param.getPart() == null) {
            throw new IllegalArgumentException("ReportController processRoleDetail is error");
        }

        Map<String, Object> map = new HashMap<String, Object>();
        List<Long> orgIds = new ArrayList<Long>();
        List<Long> mapIds = new ArrayList<Long>();
        if ("0".equals(param.getType())) {// 部门
            orgIds.add(Long.valueOf(param.getId()));
        } else if ("1".equals(param.getType())) {// 流程
            mapIds.add(Long.valueOf(param.getId()));
        }
        map.put("orgIds", orgIds);
        map.put("mapIds", mapIds);
        map.put("type", param.getType());
        map.put("part", param.getPart());
        map.put("paging", paging);
        map.put("pub", "");
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        String[] range = getRange(param.getPart());
        map.put("start", range[0]);
        if (range.length == 2) {
            map.put("end", range[1]);
        }
        Inventory inventory = reportService.getRoleInventory(map);
        return ResponseEntity.ok(inventory);
    }

    @PreAuthorize("hasAuthority('reportActivity')")
    @ApiOperation(value = "报表-流程-活动")
    @RequestMapping(value = "/reports/process/activities", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LineChart> processActivityNum(@ModelAttribute ReportSearchParam param) {

        List<Long> mapIds = param.getMapIds();
        List<Long> orgIds = param.getOrgIds();
        if (validateMapAndOrgNotPass(mapIds, orgIds)) {
            throw new IllegalArgumentException("ReportController processActivityNum is error");
        }

        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("mapIds", mapIds);
        map.put("orgIds", orgIds);
        map.put("pub", "");
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        List<ActivityStatistic> statistics = reportService.processActivityNum(map);
        return ResponseEntity.ok(ReportUtils.pie2ActivityLineChart(statistics));
    }

    @ApiOperation(value = "报表-流程-活动-详情")
    @RequestMapping(value = "/reports/process/activities/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Inventory> processActivityDetail(@PathVariable Long id,
                                                           @ModelAttribute PartOpenParam param, @ModelAttribute Paging paging) {

        if (param.getId() == null || param.getType() == null || param.getPart() == null) {
            throw new IllegalArgumentException("ReportController processActivityDetail is error");
        }

        Map<String, Object> map = new HashMap<String, Object>();
        List<Long> orgIds = new ArrayList<Long>();
        List<Long> mapIds = new ArrayList<Long>();
        if ("0".equals(param.getType())) {// 部门
            orgIds.add(Long.valueOf(param.getId()));
        } else if ("1".equals(param.getType())) {// 流程
            mapIds.add(Long.valueOf(param.getId()));
        }
        map.put("orgIds", orgIds);
        map.put("mapIds", mapIds);
        map.put("type", param.getType());
        map.put("part", param.getPart());
        map.put("paging", paging);
        map.put("pub", "");
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        String[] range = getRange(param.getPart());
        map.put("start", range[0]);
        if (range.length == 2) {
            map.put("end", range[1]);
        }
        Inventory inventory = reportService.getActivityInventory(map);
        return ResponseEntity.ok(inventory);
    }

    private String[] getRange(String part) {
        String[] parts = part.split("-");
        String[] range = new String[2];
        System.arraycopy(range, 0, parts, 0, parts.length);
        return range;
    }

    @ApiOperation(value = "报表-流程-topN")
    @RequestMapping(value = "/reports/process/top/{num}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TopNumberData>> findFlowVisitTopTen() {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("typeAuth", TYPEAUTH.FLOW);

        List<TopNumberData> top10 = homeService.findFlowVisitTopTen(map);
        return ResponseEntity.ok(top10);
    }

    @ApiOperation(value = "报表-合理化建议-topN")
    @RequestMapping(value = "/reports/suggests/top/{num}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TopNumberData>> findFlowProposeTopTen() {
        Map<String, Object> map = new HashMap<String, Object>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("typeAuth", TYPEAUTH.FLOW);
        List<TopNumberData> top10 = homeService.findFlowProposeTopTen(map);
        return ResponseEntity.ok(top10);
    }

    @PreAuthorize("hasAuthority('primaryManage')")
    @ApiOperation(value = "报表-合规管理")
    @RequestMapping(value = "/reports/primary_manage", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> primaryManage(HttpServletRequest request, PrimaryActivitySearchParam param, Paging paging) {
        Inventory inventory = reportService.primaryManage(param, paging);
        if (param != null && param.isDownload()) {
            String inventoryName = JecnProperties.getValue("PrimaryActivityManage") + ".xls";
            Attachment attachment = new Attachment();
            attachment.setContent(ExcelUtils.inventoryToByteArray(inventory));
            attachment.setName(inventoryName);
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        }
        return ResponseEntity.ok(inventory);
    }

    @ApiOperation(value = "报表-合规管理数量统计")
    @RequestMapping(value = "/reports/primary_manage_statistic", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> primaryManageStatistic() {
        return ResponseEntity.ok(reportService.primaryManageStatistic());
    }


    @PreAuthorize("hasAuthority('reportPermissions')")
    @ApiOperation(value = "报表-权限清单")
    @RequestMapping(value = "/reports/reportPermissions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> reportPermissionsReports(InventoryParam param, HttpServletRequest request) {
        String inventoryName = JecnProperties.getValue("rightsListReport") + ".xls";
        String action = param.getAction();
        Inventory inventory = reportService.reportPermissionsReports(param);
        if (StringUtils.isNotBlank(inventoryName) && StringUtils.isNotBlank(action) && action.equals("download")) {
            Attachment attachment = new Attachment();
            attachment.setContent(ExcelUtils.inventoryToByteArray(inventory));
            attachment.setName(inventoryName);
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        }
        return ResponseEntity.ok(inventory);
    }

    private Date getLastDateOfMonth(String endTime) throws ParseException {
        SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM");
        Calendar instance = Calendar.getInstance();
        // yyyy-mm
        instance.setTime(formate.parse(endTime.substring(0, 7)));
        instance.set(Calendar.DAY_OF_MONTH, instance.getMaximum(Calendar.DAY_OF_MONTH));
        instance.set(Calendar.HOUR, 23);
        instance.set(Calendar.MINUTE, 59);
        instance.set(Calendar.SECOND, 59);
        Date date = instance.getTime();
        return date;
    }

    private Date getFirstDateOfMonth(String startTime) throws ParseException {
        startTime = startTime.substring(0, 7);
        SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM");
        Calendar instance = Calendar.getInstance();
        instance.setTime(formate.parse(startTime));
        instance.set(Calendar.DAY_OF_MONTH, 1);
        instance.set(Calendar.HOUR, 0);
        instance.set(Calendar.MINUTE, 0);
        instance.set(Calendar.SECOND, 0);
        Date date = instance.getTime();
        return date;
    }
}