package com.jecn.epros.server.action.web.timer.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jecn.epros.server.action.web.timer.TimerAction;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

/**
 * 试运行定时器处理
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Jan 14, 2013 时间：3:24:27 PM
 */
public class TaskTestRunAction extends TimerAction {
	private AbstractConfigBean autoBean;
	/** 设置间隔天数 */
	private String invalDay = "1";
	/** 时间格式 */
	private String DATE_FORMAT = "HH:mm";

	/**
	 * 初始化
	 * 
	 * @return
	 * 
	 */
	public void init() {
		autoBean = new AbstractConfigBean();
		// 设置自动同步
		autoBean.setIaAuto(SyncConstant.AUTO_SYNC_USER_DATA_FLAG);
		// 设置间隔天数
		autoBean.setIntervalDay(invalDay);
		// 设置开始时间
		autoBean.setStartTime(getStringByDate());
	}

	/**
	 * 
	 * 重新开始定时监听
	 * 
	 */
	private void reStartTime() {
		if (userAutoBuss == null) {
			return;
		}
		// 停止定时
		userAutoBuss.timerStop();

		init();
		userAutoBuss.setTimerAction(this);
		userAutoBuss.timerStart();
	}

	/**
	 * 初始化定时器数据
	 * 
	 * @return AutoBean
	 */
	@Override
	public AbstractConfigBean getAbstractConfigBean() {
		return autoBean;
	}

	/**
	 * 同步数据
	 * 
	 */
	@Override
	public void synData() throws Exception {

	}

	/**
	 * 获取指定格式的时间 DATE_FORMAT
	 * 
	 * @return String格式化后的格式
	 */
	private String getStringByDate() {
		// 新建一个日期格式化类的对象，该对象可以按照指定模板格式化字符串
		SimpleDateFormat newFormat = new SimpleDateFormat(DATE_FORMAT);
		//
		return newFormat.format(new Date());
	}
}
