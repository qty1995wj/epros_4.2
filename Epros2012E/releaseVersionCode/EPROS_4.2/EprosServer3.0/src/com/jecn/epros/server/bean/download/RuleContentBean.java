package com.jecn.epros.server.bean.download;

import java.io.Serializable;

public class RuleContentBean implements Serializable {
	/** 制度标题ID */
	private Long ruleTitleId;
	/** 制度内容 */
	private String ruleContent;

	public Long getRuleTitleId() {
		return ruleTitleId;
	}

	public void setRuleTitleId(Long ruleTitleId) {
		this.ruleTitleId = ruleTitleId;
	}

	public String getRuleContent() {
		return ruleContent;
	}

	public void setRuleContent(String ruleContent) {
		this.ruleContent = ruleContent;
	}
}
