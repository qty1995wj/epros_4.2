package epros.draw.gui.designer;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;

import javax.swing.JPanel;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.figure.shape.IconFigure;
import epros.draw.gui.operationConfig.DrawOperationDescriptionConfigBean;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnDefaultFlowElementData;

/**
 * 
 * 
 * 设计器需要在画图工具上执行的功能点接口
 * 
 * @author ZHOUXY
 * 
 */
public interface JecnIDesigner {

	/** 保存流程图：保存成功返回NULL，保存失败返回错误信息 */
	public String saveMap();

	/** 新建流程图：新建成功返回NULL，保存失败返回错误信息 */
	public String newPartMap();

	/** 新建流程地图：新建成功返回NULL，保存失败返回错误信息 */
	public String newTotalMap();

	/** 添加不常用流程元素：添加成功返回NULL，保存失败返回错误信息 */
	public String addGenFlowElem();

	/** 获取当前流程元素属性值 */
	public List<JecnDefaultFlowElementData> readCerrFlowElementDataList();

	/** 获取系统默认流程元素属性值 */
	public List<JecnDefaultFlowElementData> readSystemDefaultFlowElementDataList();

	/** 流程操作说明配置项显示项 */
	public List<DrawOperationDescriptionConfigBean> operConfigList();

	/**
	 * 
	 * 修改当前流程元素属性值
	 * 
	 * @param dataList
	 *            List<JecnDefaultFlowElementData>当前流程元素属性值
	 * @return boolean 更新是否成功：true成功 false失败
	 */
	public boolean writeCurrFlowElementDataList(List<JecnDefaultFlowElementData> dataList);

	/** 活动明细 */
	public void activeDetailsDialog(JecnBaseActiveFigure activeFigure);

	/** 元素右键菜单 */
	public void beforeFlowElemPopupShow(JecnBaseFlowElementPanel flowElementPanel);

	/** 元素右键菜单 多选) */
	public void beforeFlowElemMutilsPopupShow();

	/** 画图面板右键菜单 */
	public void beforeWorkflowPopupShow();

	/**
	 * @author yxw 2012-8-10
	 * @description:保存模型
	 */
	public void saveTemplate();

	/**
	 * @author yxw 2012-8-10
	 * @description:删除模型
	 */
	public void deleteTemplate();

	/**
	 * @author yxw 2012-8-10
	 * @description:修改模型
	 */
	public void updateTemplate();

	/**
	 * @author yxw 2012-8-10
	 * @description:获取模型数据
	 */
	public void getJecnProcessTemplateBeanList();

	/**
	 * @author yxw 2012-8-14
	 * @description:退出
	 */
	public void loginOut();

	/**
	 * 
	 * 关闭占用
	 * 
	 * @param workflow
	 *            JecnDrawDesktopPane
	 * 
	 * @return boolean true：成功；false：失败
	 * 
	 */
	public boolean workFlowClose(JecnDrawDesktopPane workflow);

	/**
	 * @author yxw 2012-8-15
	 * @description:活动明细
	 */
	public void activeDetail();

	/**
	 * @author yxw 2012-8-16
	 * @description:切换面板
	 * @param id
	 *            面板ID
	 * @param mapType
	 *            面板类型
	 * @param modeType
	 *            模板类型
	 */
	public void swicthWorkFlow(long id, MapType mapType, ModeType modeType);

	/**
	 * 
	 * 是否能操作流程设计向导
	 * 
	 * @param mapType
	 * 
	 */
	public void isEditFlowDesignGuide(MapType mapType);

	/**
	 * @author yxw 2012-10-22
	 * @description:自动保存定时器重新启动
	 */
	public void timerRestart();

	/**
	 * 
	 * 添加连接到角色移动上的图形
	 * 
	 * @param figure
	 *            JecnBaseRoleFigure
	 */
	public void addLinkToroleMobile(JecnBaseRoleFigure figure);

	/**
	 * 
	 * 获取版本类型
	 * 
	 * @return boolean true： 完整版 false：标准版
	 */
	public boolean isVersionType();

	/**
	 * @author yxw 2014年5月9日
	 * @description:0(p1)：有浏览端标准版（默认）；99(p2)：有浏览端完整版；1(D2)：无浏览端标准版；100(D3)：无浏览端完整版 
	 *                                                                             1
	 *                                                                             或100返回false
	 * @return
	 */
	public boolean isPubShow();

	/**
	 * 获取图标插入框 图片
	 * 
	 * @author fuzhh 2013-7-25
	 * @param id
	 *            图片ID
	 * @return
	 */
	public void openFile(long id, IconFigure iconFigure);

	/**
	 * 双击面板标题处理 隐藏流程设计向导和资源管理器
	 * 
	 */
	public void doubleCheckTabbedPanel(MouseEvent e);

	/***************************************************************************
	 * 系统配置---基本信息下的公司名称及公司logo
	 * 
	 * @return
	 */
	public JPanel getConfigFileConPanyPanel();

	/**
	 * 自动备份
	 * 
	 * @author weidp
	 * @date 2014-10-27 下午07:43:47
	 * @param 画图面板
	 */
	public void saveWorkFlowAsBak(JecnDrawDesktopPane jecnDrawDesktopPane);

	/**
	 * 覆盖流程图/组织图/流程地图文件
	 * 
	 * @author weidp
	 * @date 2014-11-3 下午03:35:28
	 * @param flowFile
	 */
	public void recoveryFlowFile(File flowFile);

	/**
	 * 
	 * 检查权限，是否让操作
	 * 
	 * @return boolean true：有权限操作；false：没有权限操作
	 */
	public boolean checkAuth();

	/**
	 * 获取数据库中所有元素
	 * 
	 * @return
	 */
	public List<MapElemType> getShowElementsType(int type);

	void updateElementsType(List<MapElemType> mapElemTypes, int type);

	public List<MapElemType> getAllElementsType(int type);

	boolean isAdmin();

	/** 流程编号是否显示 29所需求，需要在树节点中根据配置项显示为流程编号 空格 流程名称 **/
	public boolean isNumShow();

	public String getFlowIdInput(Long flowId);

	public void activeAutoNums();

	/**
	 * 协作框内的活动编号是否保持一致
	 * 
	 * @return
	 */
	public boolean isSameNumberInDottedRect();

	/**
	 * 流程架构图，连线是否可不关联元素存在
	 * 
	 * @return true :连接线可不关联元素存在
	 */
	public boolean isShowLineHasNoFigure();

	boolean isUserActiveByItem();

	boolean isSecondAdmin();

	boolean isEditFlowMap();

	void addElemLink(JecnBaseFigurePanel elementPanel, MapType mapType);

	boolean isHiddenEditToolBarAttrs();

	boolean isShowProcessCustomer();

	boolean isShowBeginningEndElement();

	public boolean isActivityNumTypeShow();

	public boolean useNewInout();

	void editButAction(ActionEvent e);

	public boolean isShowWorkflowEdit();

	Component initEleFigurePanel();

	void showElementAttributePanel();

	public int getModelUpperLimit(int type);

	void updateElementPanel(JecnBaseFigurePanel curFigurePanel);

	void hiddelElementPanel();

	public boolean isShowGrid();

	public void setGridState(boolean showGrid);

}
