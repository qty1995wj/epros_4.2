package epros.draw.gui.line.resize;

import java.awt.Cursor;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.constant.JecnWorkflowConstant.ReSizeLineType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.swing.JecnBaseResizePanel;

/**
 * 
 * 分割线选中点
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDividingLineResizePanel extends JecnBaseResizePanel {

	/** 线选中点类型 start 开始点,center线段中心点,end 结束点 */
	private ReSizeLineType reSizeLineType = null;

	/** 选中点相关对象 */
	private JecnBaseDividingLinePanel baseLinePanel = null;

	private JecnDividingLineResizePanel(ReSizeLineType reSizeLineType,
			JecnBaseDividingLinePanel baseLinePanel) {
		if (reSizeLineType == null) {

			JecnLogConstants.LOG_JECN_BASE_RESIZE_PANEL
					.error("JecnFigureResizePanel类构造函数：参数为null。reSizeLineType="
							+ reSizeLineType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		if (ReSizeLineType.center.equals(reSizeLineType)) {
			this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		} else {
			this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		}
		// 显示点位置 start，center，end
		this.reSizeLineType = reSizeLineType;
		this.baseLinePanel = baseLinePanel;
		this.setSize(6, 6);
	}

	/**
	 * 鼠标移动到线段显示点，鼠标状态设置
	 * 
	 * @param lineDirectionEnum
	 */
	public void setMouseCursor(LineDirectionEnum lineDirectionEnum) {
		switch (lineDirectionEnum) {
		case horizontal:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
			break;
		case vertical:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 创建开始选中点
	 * 
	 * @return
	 */
	public static JecnDividingLineResizePanel createStartLineResizePanel(
			JecnBaseDividingLinePanel baseLinePanel) {
		return new JecnDividingLineResizePanel(ReSizeLineType.start,
				baseLinePanel);
	}

	/**
	 * 
	 * 创建中间选中点
	 * 
	 * @return
	 */
	public static JecnDividingLineResizePanel createCenterLineResizePanel(
			JecnBaseDividingLinePanel baseLinePanel) {
		return new JecnDividingLineResizePanel(ReSizeLineType.center,
				baseLinePanel);
	}

	/**
	 * 
	 * 创建结束选中点
	 * 
	 * @return
	 */
	public static JecnDividingLineResizePanel createEndLineResizePanel(
			JecnBaseDividingLinePanel baseLinePanel) {
		return new JecnDividingLineResizePanel(ReSizeLineType.end,
				baseLinePanel);
	}

	public ReSizeLineType getReSizeLineType() {
		return reSizeLineType;
	}

	public JecnBaseDividingLinePanel getBaseLinePanel() {
		return baseLinePanel;
	}
}
