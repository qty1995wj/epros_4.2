package wordxml.element.dom.paragraph;

import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.dom.graph.WordGraph;
import wordxml.util.Tools;

public class Run {

	/** 所属段落 */
	private Paragraph parent;
	/** 文本样式 */
	private FontBean fontBean;

	/** 图形 */
	private WordGraph graph;
	/** 文本 */
	private final String text;
	// 是否特殊文本（PAGE,
	private boolean isSpecialText = false;

	/****** 设置首页封皮页数，默认值为0，此属性是用来计算总页数的 ***************/
	private int titleNumPages = 0;

	private Run(Paragraph p, String text) {
		this.parent = p;
		this.text = text;
	}

	/**
	 * 创建文本实例
	 * 
	 * @author weidp
	 * @date 2014-10-23 上午10:34:23
	 * @param p
	 * @param text
	 * @return
	 */
	public static Run newInstance(Paragraph p, String text, boolean isSpecialText) {
		String temp = text;
		if (!isSpecialText && text != null) {
			temp = Tools.handleSpecialChar(temp);
		}
		Run run = new Run(p, temp);
		run.setSpecialText(isSpecialText);
		return run;
	}

	/****
	 * 此方法住要是为了解决首页封皮不止一页的情况
	 * 
	 * @param p
	 * @param text
	 * @param isSpecialText
	 * @param titleNumPages
	 * @return
	 */
	public static Run newInstance(Paragraph p, String text, boolean isSpecialText, int titleNumPages) {
		Run run = new Run(p, text);
		run.setSpecialText(isSpecialText);
		run.setTitleNumPages(titleNumPages);
		return run;
	}

	/**
	 * 创建文本实例(带样式)
	 * 
	 * @author weidp
	 * @date 2014-10-23 上午10:34:23
	 * @param p
	 * @param text
	 * @return
	 */
	public static Run newInstance(Paragraph p, String text, boolean isSpecialText, FontBean fontBean) {
		Run run = new Run(p, text);
		run.setFontBean(fontBean);
		run.setSpecialText(isSpecialText);
		return run;
	}

	/**
	 * 创建图形实例
	 * 
	 * @author weidp
	 * @date 2014-10-23 上午10:34:45
	 * @param p
	 * @param graph
	 * @return
	 */
	public static Run newInstance(Paragraph p, WordGraph graph) {
		Run run = new Run(p, "");
		run.setGraph(graph);
		return run;
	}

	public String getText() {
		return text;
	}

	public WordGraph getGraph() {
		return this.graph;
	}

	private void setGraph(WordGraph graph) {
		this.graph = graph;
	}

	public Paragraph getParent() {
		return parent;
	}

	public void setParent(Paragraph parent) {
		this.parent = parent;
	}

	public boolean isSpecialText() {
		return isSpecialText;
	}

	private void setSpecialText(boolean isSpecialText) {
		this.isSpecialText = isSpecialText;
	}

	public FontBean getFontBean() {
		return fontBean;
	}

	public void setFontBean(FontBean fontBean) {
		this.fontBean = fontBean;
	}

	public int getTitleNumPages() {
		return titleNumPages;
	}

	public void setTitleNumPages(int titleNumPages) {
		this.titleNumPages = titleNumPages;
	}

}
