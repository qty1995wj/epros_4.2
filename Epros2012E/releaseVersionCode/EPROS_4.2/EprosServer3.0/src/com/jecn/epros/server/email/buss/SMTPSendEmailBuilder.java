package com.jecn.epros.server.email.buss;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.bean.MessageResult;

/**
 * 根据文件信息发送邮件
 * 
 * @author xiaohu
 * 
 */

public class SMTPSendEmailBuilder {

	private static final Log log = LogFactory.getLog(SMTPSendEmailBuilder.class);

	/** 邮件配置项 */
	private EmailConfigItemBean emailConfig;
	/** 内外网发送邮件 true是内网 false 是外网 */
	private boolean isOuter;
	/** 邮件的session **/
	private Session mailSession;

	private String fromAddress;

	private String fromAlias;

	/**
	 * 初始化邮件生成器
	 * 
	 * @param emailConfig
	 *            邮箱配置项
	 * @param isOuter
	 *            是否是外网 true是外网 false 内网
	 */
	public SMTPSendEmailBuilder(EmailConfigItemBean emailConfig, boolean isOuter) {

		this.emailConfig = emailConfig;
		this.isOuter = isOuter;
		if (isOuter) {
			fromAddress = emailConfig.getOuterUserAddr();
			fromAlias = emailConfig.getOuterAlias();
		} else {
			fromAddress = emailConfig.getInnerUserAddr();
			fromAlias = emailConfig.getInnerAlias();
		}

	}

	/**
	 * 初始化发送邮件的session
	 * 
	 * @return true初始化成功 false初始化失败
	 */
	public boolean initSession() {

		// 服务器地址
		String smtpServer = null;
		// 发件箱的登陆名
		String loginName = null;
		// 发件箱的密码
		String password = null;
		// 端口号
		String port = "25";
		// 是否使用ssl加密
		boolean isUseSSL = false;
		// 是否匿名发送
		boolean isUseAnonymity = false;
		// 是否使用tls加密
		boolean isUseTLS = false;
		// 外网
		if (isOuter) {
			if (StringUtils.isBlank(emailConfig.getOuterSmtp())) {
				log.error("外网--发件箱的服务器地址为空");
				return false;
			} else {
				smtpServer = emailConfig.getOuterSmtp().trim();
			}

			// 如果外网的启用ssl不为空并且等于1 那么就是启用ssl
			if (!StringUtils.isBlank(emailConfig.getOuterUseSSL()) && "1".equals(emailConfig.getOuterUseSSL().trim())) {
				isUseSSL = true;
			} else {
				isUseSSL = false;
			}

			// 如果外网的启用tls不为空并且等于1 那么就是启用tls
			if (!StringUtils.isBlank(emailConfig.getOuterUseTLS()) && "1".equals(emailConfig.getOuterUseTLS().trim())) {
				isUseTLS = true;
			} else {
				isUseTLS = false;
			}

			// 如果外网的匿名配置项不为空并且等于1 那么就是匿名登陆
			if (!StringUtils.isBlank(emailConfig.getOuterUseAnonymity())
					&& "1".equals(emailConfig.getOuterUseAnonymity().trim())) {
				isUseAnonymity = true;
			} else {
				isUseAnonymity = false;
			}

			// 非匿名发送 需要用户名和密码
			if (!isUseAnonymity) {
				// 发件箱登录名
				loginName = emailConfig.getOuterLoginName();
				if (StringUtils.isBlank(loginName)) {
					log.error("外网--发件箱登陆名为空");
					return false;
				}

				password = emailConfig.getOuterPWd();
				// 发件箱密码
				if (StringUtils.isBlank(password)) {
					log.error("外网--发件箱密码为空");
					return false;
				}
			}
			// 端口
			port = emailConfig.getOuterPort();
			// 如果端口号为空 默认为25
			if (StringUtils.isBlank(port)) {
				port = "25";
			}

		} else {// 内网

			if (StringUtils.isBlank(emailConfig.getInnerSmtp())) {
				log.error("内网--发件箱的服务器地址为空");
				return false;
			} else {
				smtpServer = emailConfig.getInnerSmtp().trim();
			}

			// 如果内网的ssl项不为空并且等于1 那么就是启用ssl
			if (!StringUtils.isBlank(emailConfig.getInnerUseSSL()) && "1".equals(emailConfig.getInnerUseSSL().trim())) {
				isUseSSL = true;
			} else {
				isUseSSL = false;
			}

			// 如果内网网的启用tls不为空并且等于1 那么就是启用tls
			if (!StringUtils.isBlank(emailConfig.getInnerUseTLS()) && "1".equals(emailConfig.getInnerUseTLS().trim())) {
				isUseTLS = true;
			} else {
				isUseTLS = false;
			}

			// 如果内网的匿名配置项不为空并且等于1 那么就是匿名登陆
			if (!StringUtils.isBlank(emailConfig.getInnerUseAnonymity())
					&& "1".equals(emailConfig.getInnerUseAnonymity().trim())) {
				isUseAnonymity = true;
			} else {
				isUseAnonymity = false;
			}

			// 非匿名发送 需要用户名密码
			if (!isUseAnonymity) {
				// 发件箱登录名
				loginName = emailConfig.getInnerLoginName();
				if (StringUtils.isBlank(loginName)) {
					log.error("内网--发件箱登陆名为空");
					return false;
				}

				password = emailConfig.getInnerPWd();
				// 发件箱密码
				if (StringUtils.isBlank(password)) {
					log.error("内网--发件箱密码为空");
					return false;
				}
			}

			port = emailConfig.getInnerPort();
			// 如果端口号为空 默认为25
			if (StringUtils.isBlank(port)) {
				port = "25";
			}
		}

		Properties props = new Properties();

		// SMTP服务器
		props.put("mail.smtp.host", smtpServer);
		props.put("mail.smtp.port", port);

		// 是否使用ssl加密
		if (isUseSSL) {
			props.put("mail.smtp.ssl.enable", "true");
		}

		// 匿名发送
		if (isUseAnonymity) {
			props.put("mail.smtp.auth", "false");
			mailSession = Session.getInstance(props); // 获得邮件会话对象
		} else {// 验证用户名密码
			props.put("mail.smtp.auth", "true");
			SmtpAuth smtpAuth = new SmtpAuth(loginName, password);
			mailSession = Session.getInstance(props, smtpAuth); // 获得邮件会话对象
		}

		if (isUseTLS) {
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.ssl.trust", smtpServer);
		}
		if (isOuter) {
			log.info("使用外网发送......");
		} else {
			log.info("使用内网发送......");
		}

		log.info("服务器发件箱：" + smtpServer);
		log.info("登录名：" + loginName);
		log.info("端口：" + port);
		log.info("是否启用匿名：" + isUseAnonymity);
		log.info("是否启用ssl：" + isUseSSL);
		log.info("是否启用tls：" + isUseTLS);

		// 设置debug模式，控制台可查看发送邮件过程
		mailSession.setDebug(true);

		return true;
	}

	/**
	 * 发送邮件
	 * 
	 */
	public boolean sendEmail(EmailMessageBean emailMessageBean) {
		boolean sendSuccess = true;
		Transport trans = null;
		try {
			// 获取邮件信息
			MimeMessage msg = getMimeMessage(mailSession, emailMessageBean);
			trans = mailSession.getTransport("smtp");
			trans.connect();
			trans.sendMessage(msg, msg.getAllRecipients());

		} catch (AuthenticationFailedException e) {
			sendSuccess = false;
			log.error("邮件发送失败！错误原因：\n" + "身份验证错误!", e);

		} catch (MessagingException e) {
			sendSuccess = false;
			log.error("邮件发送失败! 错误原因：邮箱服务验证失败\n", e);
			if ("Host found in DNS blacklist".equals(e.getMessage())) {
				log.error("Host found in DNS blacklist=" + emailMessageBean.getUser().getEmail());
			}
		} catch (UnsupportedEncodingException e) {
			sendSuccess = false;
			log.error("邮件发送失败！错误原因：将邮件主题转码为utf-8时出现异常！", e);
		} catch (Exception e) {
			sendSuccess = false;
			log.error("邮件其它错误", e);
		} finally {
			try {
				if (trans != null) {
					trans.close();
				}
			} catch (MessagingException e) {// 连接关闭错误不用处理，并不代表邮件发送失败
				log.error("Transport对象关闭异常", e);
			}
		}

		return sendSuccess;
	}

	/**
	 * 测试邮件用户名和密码
	 * 
	 */
	public MessageResult testEmailAuthentication() {
		MessageResult result = new MessageResult();
		Transport trans = null;
		try {
			trans = mailSession.getTransport("smtp");
			trans.connect();
		} catch (AuthenticationFailedException e) {
			result.setSuccess(false);
			result.setResultMessage("身份验证错误");
			log.error(e);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setResultMessage("连接错误");
			log.error(e);
		} finally {
			try {
				if (trans != null) {
					trans.close();
				}
			} catch (MessagingException e) {// 连接关闭错误不用处理，并不代表邮件发送失败
				log.error("Transport对象关闭异常", e);
			}
		}

		return result;
	}

	/**
	 * 获取发送邮件message对象
	 * 
	 * @param mailSession
	 * @param emailMessageBean
	 * @return
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	private MimeMessage getMimeMessage(Session mailSession, EmailMessageBean emailMessageBean)
			throws MessagingException, UnsupportedEncodingException {
		// 设置session,和邮件服务器进行通讯。
		MimeMessage msg = new MimeMessage(mailSession);
		// 设置邮件发送日期
		msg.setSentDate(new Date());
		// 邮件标题 转码为UTF-8
		msg.setSubject(MimeUtility.encodeText(emailMessageBean.getSubject(), "UTF-8", "B"));
		String alias = fromAddress;
		try {
			alias = javax.mail.internet.MimeUtility.encodeText(fromAlias);
		} catch (Exception e) {
			log.error("", e);
		}
		// 发送邮件人
		msg.setFrom(new InternetAddress(alias + "<" + fromAddress + ">"));
		// 邮件接收者TO
		ArrayList<String> receiver = new ArrayList<String>();
		receiver.add(emailMessageBean.getUser().getEmail());
		msg.setRecipients(RecipientType.TO, ToolEmail.getArrayAddress(receiver));

		MimeBodyPart textBodyPart = new MimeBodyPart();
		// 邮件正文
		textBodyPart.setText(emailMessageBean.getContent());
		// 设置邮件格式
		textBodyPart.setContent(emailMessageBean.getContent(), "text/html;charset=UTF-8");
		Multipart container = new MimeMultipart();
		container.addBodyPart(textBodyPart);
		// 添加附件
		addFileElement(emailMessageBean, textBodyPart, container);
		// Multipart加入到信件
		msg.setContent(container);
		// 保存邮件
		msg.saveChanges();
		return msg;
	}

	/**
	 * 添加附件
	 * 
	 * @param emailMessageBean
	 * @param textBodyPart
	 * @param container
	 */
	private void addFileElement(EmailMessageBean emailMessageBean, MimeBodyPart textBodyPart, Multipart container) {
		// 添加附件
		Vector file = new Vector();
		if (emailMessageBean.getRunTestFile() != null) {
			// 添加附件(目前只有一个附件，多个附件时应先添加附件)
			File fileRun = emailMessageBean.getRunTestFile();
			String filename = emailMessageBean.getFileName();
			file.addElement(filename);
			// String filename = fileRun.getName();
			if (!file.isEmpty()) {// 有附件
				// 获取附件元素
				Enumeration efile = file.elements();
				while (efile.hasMoreElements()) {
					textBodyPart = new MimeBodyPart();
					// 选择出每一个附件名
					filename = efile.nextElement().toString();
					// 得到数据源
					FileDataSource fds = new FileDataSource(fileRun);
					try {
						// 得到数据源
						textBodyPart.setDataHandler(new DataHandler(fds));
						// // 得到附件本身并至入BodyPart
						// String fn = fds.getName();
						// 文件名称乱码处理
						filename = MimeUtility.encodeText(filename);
						// 得到文件名同样至入BodyPart
						textBodyPart.setFileName(filename);
						container.addBodyPart(textBodyPart);
					} catch (MessagingException e) {
						log.error("邮件发送失败！错误原因：\n" + e.getMessage());
					} catch (UnsupportedEncodingException e) {
						log.error("邮件乱码处理失败！错误原因：\n" + e.getMessage());
					}
				}
				file.removeAllElements();
			}
		}
	}
}
