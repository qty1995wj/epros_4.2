package epros.draw.gui.operationConfig.data;

import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.designer.JecnModeFileT;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.operationConfig.bean.JecnFlowOperationBean;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

public class JecnFlowOperationDataUnit {
	/**
	 * 单机版小设计器 获取操作说明数据
	 * 
	 * @return
	 */
	public static JecnFlowOperationBean getJecnFlowOperationBean() {
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.error("获取操作说明数据对象中，画图面板为空!");
			return null;
		}
		// 流程操作说明基本信息
		JecnFlowOperationBean flowOperationBean = new JecnFlowOperationBean();
		// 流程名称
		flowOperationBean.setFlowName(desktopPane.getFlowMapData().getName());

		// 记录所有活动数据集合
		List<JecnBaseActiveFigure> listActiveFigure = new ArrayList<JecnBaseActiveFigure>();

		// 记录所有角色数据集合
		List<JecnBaseRoleFigure> listRoleFigure = new ArrayList<JecnBaseRoleFigure>();

		List<JecnBaseVHLinePanel> listVHLinePanel = new ArrayList<JecnBaseVHLinePanel>();
		// 获取所有角色和活动、分割线
		getRloeBeanList(flowOperationBean, listActiveFigure, listRoleFigure, listVHLinePanel);
		// 所有的活动信息
		setActiveIdForRoleFigure(flowOperationBean, listActiveFigure, listRoleFigure, listVHLinePanel);
		// 关键活动类型：1为PA,2为KSF,3为KCP
		List<JecnActivityShowBean> activePAList = new ArrayList<JecnActivityShowBean>();// 问题区域
		List<JecnActivityShowBean> activeKSFList = new ArrayList<JecnActivityShowBean>();// 关键成功要素
		List<JecnActivityShowBean> activeKCPList = new ArrayList<JecnActivityShowBean>();// 关键控制点
		List<JecnActivityShowBean> activeKCPAllowList = new ArrayList<JecnActivityShowBean>();// 关键控制点(合规)

		for (JecnActivityShowBean activeBean : flowOperationBean.getListAllActivityShow()) {
			String activeKeyType = activeBean.getActiveFigure().getFlowElementData().getActiveKeyType();
			if (DrawCommon.isNullOrEmtryTrim(activeKeyType)) {// 不存在关键活动类型
				continue;
			}
			if ("1".equals(activeKeyType)) {
				activePAList.add(activeBean);
			} else if ("2".equals(activeKeyType)) {
				activeKSFList.add(activeBean);
			} else if ("3".equals(activeKeyType)) {
				activeKCPList.add(activeBean);
			} else if ("4".equals(activeKeyType)) {
				activeKCPAllowList.add(activeBean);
			}
		}
		// 关键活动类型：1为PA
		flowOperationBean.setListPAActivityShow(activePAList);
		// 关键活动类型：2为KSF,
		flowOperationBean.setListKSFActivityShow(activeKSFList);
		// 关键活动类型：3为KCP
		flowOperationBean.setListKCPActivityShow(activeKCPList);
		// 关键活动类型：4为KCP(合规)
		flowOperationBean.setListKCPActivityAllowShow(activeKCPAllowList);

		JecnFlowOperationBean operationData = desktopPane.getFlowMapData().getFlowOperationBean();
		flowOperationBean.setFlowCustom(operationData.getFlowCustom());
		flowOperationBean.setFlowInput(operationData.getFlowInput());
		flowOperationBean.setFlowOutput(operationData.getFlowOutput());
		flowOperationBean.setApplicability(operationData.getApplicability());
		flowOperationBean.setNoApplicability(operationData.getNoApplicability());
		flowOperationBean.setNoutGlossary(operationData.getNoutGlossary());
		flowOperationBean.setFlowSummarize(operationData.getFlowSummarize());
		flowOperationBean.setFlowSupplement(operationData.getFlowSupplement());
		flowOperationBean.setFlowPurpose(operationData.getFlowPurpose());
		// 流程记录
		flowOperationBean.setFlowFileStr(operationData.getFlowFileStr());
		// 操作规范
		flowOperationBean.setPracticesStr(operationData.getPracticesStr());
		// 相关流程
		flowOperationBean.setRelatedFlowStr(operationData.getRelatedFlowStr());
		// 相关制度
		flowOperationBean.setRelatedRuleStr(operationData.getRelatedRuleStr());
		// 流程评测及关键指标
		flowOperationBean.setKeyEvaluationIndiStr(operationData.getKeyEvaluationIndiStr());
		// 起始活动
		flowOperationBean.setFlowStartActive(operationData.getFlowStartActive());
		flowOperationBean.setFlowEndActive(operationData.getFlowEndActive());
		// 自定义
		flowOperationBean.setCustomOne(operationData.getCustomOne());
		flowOperationBean.setCustomTwo(operationData.getCustomTwo());
		flowOperationBean.setCustomThree(operationData.getCustomThree());
		flowOperationBean.setCustomFour(operationData.getCustomFour());
		flowOperationBean.setCustomFive(operationData.getCustomFive());

		// 所有活动数据
		operationData.setListAllActivityShow(flowOperationBean.getListAllActivityShow());

		return flowOperationBean;
	}

	/**
	 * 排序活动
	 * 
	 * @param list
	 * @return
	 */
	public static void getActivitySort(List<JecnBaseActiveFigure> list) {
		// 外循环控制比较的次数
		for (int i = 0; i < list.size(); i++) {
			// 内循环控制比较后移位
			for (int j = list.size() - 1; j > i; j--) {
				JecnBaseActiveFigure processActiveBeanOne = list.get(j - 1);
				JecnBaseActiveFigure processActiveBeanTwo = list.get(j);
				String oneActivesNumber = processActiveBeanOne.getFlowElementData().getActivityNum();
				String twoActivesNumber = processActiveBeanTwo.getFlowElementData().getActivityNum();
				if (oneActivesNumber != null && twoActivesNumber != null) {
					if (oneActivesNumber.indexOf("-") > 0) {
						oneActivesNumber = oneActivesNumber.substring(oneActivesNumber.lastIndexOf("-") + 1,
								oneActivesNumber.length());
					}
					if (!oneActivesNumber.matches("[0-9]+")) {
						list.set(j - 1, processActiveBeanTwo);
						list.set(j, processActiveBeanOne);
						continue;
					}

					if (twoActivesNumber.indexOf("-") > 0) {
						twoActivesNumber = twoActivesNumber.substring(twoActivesNumber.lastIndexOf("-") + 1,
								twoActivesNumber.length());
					}
					if (!twoActivesNumber.matches("[0-9]+")) {
						continue;
					}

					// 自动编号输入的字符最长为16位字符
					if (oneActivesNumber.length() > 16 || twoActivesNumber.length() > 16) {
						continue;
					}

					if (Long.valueOf(oneActivesNumber) > Long.valueOf(twoActivesNumber)) {
						list.set(j - 1, processActiveBeanTwo);
						list.set(j, processActiveBeanOne);
					}
				} else if (oneActivesNumber == null) {
					list.set(j - 1, processActiveBeanTwo);
					list.set(j, processActiveBeanOne);
				}
			}
		}
	}

	/**
	 * 获取所有排序后的活动
	 * 
	 * @author fuzhh 2013-11-26
	 * @return
	 */
	public static List<JecnBaseActiveFigure> getActinveFigure() {
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return null;
		}
		// 记录所有活动数据集合
		List<JecnBaseActiveFigure> listActiveFigure = new ArrayList<JecnBaseActiveFigure>();
		for (JecnBaseFlowElementPanel figurePanel : desktopPane.getPanelList()) {
			if (DrawCommon.isActive(figurePanel.getFlowElementData().getMapElemType())) {
				listActiveFigure.add((JecnBaseActiveFigure) figurePanel);
			}
		}
		// 活动按照坐标排序
		getActivitySort(listActiveFigure);

		return listActiveFigure;
	}

	/**
	 * 获取关键活动
	 * 
	 * @author fuzhh 2013-11-26
	 */
	public static void getKeyActiveFigure(List<JecnBaseActiveFigure> activeList,
			List<JecnBaseActiveFigure> paActiveList, List<JecnBaseActiveFigure> ksfActiveList,
			List<JecnBaseActiveFigure> kcpActiveList) {
		for (JecnBaseActiveFigure baseActiveFigure : activeList) {
			// 关键活动类型：1为PA,2为KSF,3为KCP
			if ("1".equals(baseActiveFigure.getActiveKeyType())) {
				paActiveList.add(baseActiveFigure);
			} else if ("2".equals(baseActiveFigure.getActiveKeyType())) {
				ksfActiveList.add(baseActiveFigure);
			} else if ("3".equals(baseActiveFigure.getActiveKeyType())) {
				kcpActiveList.add(baseActiveFigure);
			} else if ("4".equals(baseActiveFigure.getActiveKeyType())) {
				kcpActiveList.add(baseActiveFigure);
			}
		}
	}

	/**
	 * 
	 * 获取面板所有角色
	 * 
	 * @param flowOperationBean
	 * @param figureDataList
	 * @return
	 */
	private static void getRloeBeanList(JecnFlowOperationBean flowOperationBean,
			List<JecnBaseActiveFigure> listActiveFigure, List<JecnBaseRoleFigure> listRoleFigure,
			List<JecnBaseVHLinePanel> listVHLinePanel) {
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || listActiveFigure == null || listRoleFigure == null || listVHLinePanel == null) {
			JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.error("获取数据对象为空" + "listActiveFigure" + listActiveFigure
					+ "listRoleFigure" + listRoleFigure + "desktopPane" + desktopPane);
			return;
		}

		// 记录所有
		List<JecnActivityShowBean> listAllActivityShow = new ArrayList<JecnActivityShowBean>();

		for (JecnBaseFlowElementPanel figurePanel : desktopPane.getPanelList()) {
			MapElemType elemType = figurePanel.getFlowElementData().getMapElemType();
			if (DrawCommon.isRole(elemType)) {
				listRoleFigure.add((JecnBaseRoleFigure) figurePanel);
			} else if (DrawCommon.isActive(elemType)) {
				listActiveFigure.add((JecnBaseActiveFigure) figurePanel);
			} else if (elemType == MapElemType.ParallelLines || elemType == MapElemType.VerticalLine) {
				listVHLinePanel.add((JecnBaseVHLinePanel) figurePanel);
			}
		}

		// 活动按照坐标排序
		getActivitySort(listActiveFigure);

		for (JecnBaseActiveFigure activeFigure : listActiveFigure) {

			JecnActiveData activeData = activeFigure.getFlowElementData();
			JecnActivityShowBean activeBean = new JecnActivityShowBean();
			// 输入名称
			String fileImport = "";
			for (JecnActivityFileT activityFileT : activeData.getListJecnActivityFileT()) {
				if (activityFileT.getFileType() == 0) {
					fileImport += activityFileT.getFileName() + "/";
				}
			}
			if (!"".equals(fileImport)) {
				fileImport = fileImport.substring(0, fileImport.length() - 1);
				activeBean.setInput(fileImport);
			}

			// 输出名称
			String fileOutport = "";
			for (JecnModeFileT modeFileT : activeData.getListModeFileT()) {
				fileOutport += modeFileT.getModeName() + "/";
			}
			if (!"".equals(fileOutport)) {
				fileOutport = fileOutport.substring(0, fileOutport.length() - 1);
				activeBean.setOutput(fileOutport);
			}

			// 活动的图形
			activeBean.setActiveFigure(activeFigure);

			listAllActivityShow.add(activeBean);
		}
		// 记录所有活动数据对象
		flowOperationBean.setListAllActivityShow(listAllActivityShow);

		// 角色排序
		roleListSort(listRoleFigure);

		// 记录所有角色数据对象
		flowOperationBean.setBaseRoleFigureList(listRoleFigure);
	}

	/**
	 * 角色排序
	 * 
	 * @author fuzhh Oct 16, 2012
	 * @param listRoleBean
	 *            角色的集合
	 */
	public static void roleListSort(List<JecnBaseRoleFigure> listRoleFigure) {
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// true 为横向 false 为纵向
		boolean flag = desktopPane.getFlowMapData().isHFlag();
		if (flag) {
			// 角色排序
			for (int i = 0; i < listRoleFigure.size(); i++) {
				for (int j = listRoleFigure.size() - 1; j > i; j--) {
					JecnBaseRoleFigure baseRoleFigure = (JecnBaseRoleFigure) listRoleFigure.get(j);
					JecnBaseRoleFigure baseRoleNextFigure = (JecnBaseRoleFigure) listRoleFigure.get(j - 1);
					// 判断Y的坐标来判断顺序
					if (baseRoleFigure.getY() < baseRoleNextFigure.getY()) {
						listRoleFigure.set(j - 1, baseRoleFigure);
						listRoleFigure.set(j, baseRoleNextFigure);
					} else if (baseRoleFigure.getY() == baseRoleNextFigure.getY()) {
						if (baseRoleFigure.getX() <= baseRoleNextFigure.getX()) {
							listRoleFigure.set(j - 1, baseRoleFigure);
							listRoleFigure.set(j, baseRoleNextFigure);
						}
					}
				}
			}
		} else {
			// 角色排序
			for (int i = 0; i < listRoleFigure.size(); i++) {
				for (int j = listRoleFigure.size() - 1; j > i; j--) {
					JecnBaseRoleFigure baseRoleFigure = (JecnBaseRoleFigure) listRoleFigure.get(j);
					JecnBaseRoleFigure baseRoleNextFigure = (JecnBaseRoleFigure) listRoleFigure.get(j - 1);
					// 判断Y的坐标来判断顺序
					if (baseRoleFigure.getX() < baseRoleNextFigure.getX()) {
						listRoleFigure.set(j - 1, baseRoleFigure);
						listRoleFigure.set(j, baseRoleNextFigure);
					} else if (baseRoleFigure.getX() == baseRoleNextFigure.getX()) {
						if (baseRoleFigure.getY() <= baseRoleNextFigure.getY()) {
							listRoleFigure.set(j - 1, baseRoleFigure);
							listRoleFigure.set(j, baseRoleNextFigure);
						}
					}
				}
			}
		}
	}

	/**
	 * 根据坐标排序
	 * 
	 * @param listActiveData
	 *            活动数据对象集合
	 */
	public static void getActivitySortFigureAcitveT(List<JecnBaseActiveFigure> listActiveFigure) {
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane.getFlowMapData().isHFlag()) { // 面板横向
			for (int i = 0; i < listActiveFigure.size(); i++) {
				for (int j = listActiveFigure.size() - 1; j > i; j--) {
					if (listActiveFigure.get(j).getLocation().getX() < listActiveFigure.get(j - 1).getLocation().getX()) {
						JecnBaseActiveFigure activeFigure = listActiveFigure.get(j);
						listActiveFigure.set(j, listActiveFigure.get(j - 1));
						listActiveFigure.set(j - 1, activeFigure);
					}
				}
			}
			for (int i = 0; i < listActiveFigure.size(); i++) {
				for (int j = listActiveFigure.size() - 1; j > i; j--) {
					if (listActiveFigure.get(j).getLocation().getX() == listActiveFigure.get(j - 1).getLocation()
							.getX()) {
						if (listActiveFigure.get(j).getLocation().getY() <= listActiveFigure.get(j - 1).getLocation()
								.getY()) {
							JecnBaseActiveFigure figureTemp = listActiveFigure.get(j);
							listActiveFigure.set(j, listActiveFigure.get(j - 1));
							listActiveFigure.set(j - 1, figureTemp);
						}
					}
				}
			}
		} else {
			for (int i = 0; i < listActiveFigure.size(); i++) {
				for (int j = listActiveFigure.size() - 1; j > i; j--) {
					if (listActiveFigure.get(j).getLocation().getY() < listActiveFigure.get(j - 1).getLocation().getY()) {
						JecnBaseActiveFigure activeFigure = listActiveFigure.get(j);
						listActiveFigure.set(j, listActiveFigure.get(j - 1));
						listActiveFigure.set(j - 1, activeFigure);
					}
				}
			}
			for (int i = 0; i < listActiveFigure.size(); i++) {
				for (int j = listActiveFigure.size() - 1; j > i; j--) {
					if (listActiveFigure.get(j).getLocation().getY() == listActiveFigure.get(j - 1).getLocation()
							.getY()) {
						if (listActiveFigure.get(j).getLocation().getX() <= listActiveFigure.get(j - 1).getLocation()
								.getX()) {
							JecnBaseActiveFigure figureTemp = listActiveFigure.get(j);
							listActiveFigure.set(j, listActiveFigure.get(j - 1));
							listActiveFigure.set(j - 1, figureTemp);
						}
					}
				}
			}
		}
	}

	/**
	 * 获取角色活动关联
	 * 
	 * @param activeFigureDataList
	 *            活动集合
	 * @param roleFigureDataList
	 *            角色集合
	 * @param listVHLinePanel
	 *            分割线集合
	 * @return 角色与活动关联集合
	 */
	public static void setActiveIdForRoleFigure(JecnFlowOperationBean flowOperationBean,
			List<JecnBaseActiveFigure> listActiveFigure, List<JecnBaseRoleFigure> listRoleFigure,
			List<JecnBaseVHLinePanel> listVHLinePanel) {
		// 获取画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || listActiveFigure == null || listRoleFigure == null) {
			JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.error("获取数据对象为空" + "listActiveFigure" + listActiveFigure
					+ "listRoleData" + listRoleFigure + "desktopPane" + desktopPane);
			return;
		}
		// 循环角色
		for (JecnBaseRoleFigure roleFigure : listRoleFigure) {
			if (desktopPane.getFlowMapData().isHFlag()) { // 面板横向
				int oneVHLine = 0;
				int twoVHLine = desktopPane.getHeight();

				boolean isContinue = false;

				for (JecnBaseVHLinePanel vhLinePanel : listVHLinePanel) {
					// 获取分割线
					JecnVHLineData vHLineData = (JecnVHLineData) vhLinePanel.getFlowElementData();
					// 角色是否压线
					if (vHLineData.getVhY() >= roleFigure.getFlowElementData().getFigureDataCloneable().getY()
							&& vHLineData.getVhY() <= roleFigure.getFlowElementData().getFigureDataCloneable().getY()
									+ roleFigure.getFlowElementData().getFigureDataCloneable().getHeight()) {
						isContinue = true;

					}

					// 循环判断小于角色位置点的分割线，并且离角色最近的一条
					if (oneVHLine < vHLineData.getVhY()
							&& vHLineData.getVhY() < roleFigure.getFlowElementData().getFigureDataCloneable().getY()) {
						oneVHLine = (int) vHLineData.getVhY();

					}
					if (twoVHLine > vHLineData.getVhY()
							&& vHLineData.getVhY() > roleFigure.getFlowElementData().getFigureDataCloneable().getY()) {
						twoVHLine = (int) vHLineData.getVhY();
					}
				}

				if (isContinue) {
					continue;
				}

				if (listVHLinePanel.size() > 0) {
					// 数据不为默认值
					for (JecnBaseActiveFigure activeFigure : listActiveFigure) {
						// 活动是否压线
						if (oneVHLine >= activeFigure.getFlowElementData().getFigureDataCloneable().getY()
								&& oneVHLine <= activeFigure.getFlowElementData().getFigureDataCloneable().getY()
										+ activeFigure.getFlowElementData().getFigureDataCloneable().getHeight()) {
							continue;

						}
						// 活动是否压线
						if (twoVHLine >= activeFigure.getFlowElementData().getFigureDataCloneable().getY()
								&& twoVHLine <= activeFigure.getFlowElementData().getFigureDataCloneable().getY()
										+ activeFigure.getFlowElementData().getFigureDataCloneable().getHeight()) {
							continue;

						}
						if (activeFigure.getFlowElementData().getFigureDataCloneable().getY() < twoVHLine
								&& activeFigure.getFlowElementData().getFigureDataCloneable().getY() > oneVHLine) {
							JecnActivityShowBean activeBean = getActiveBeanByUUID(flowOperationBean
									.getListAllActivityShow(), activeFigure.getFlowElementData().getFlowElementUUID());

							activeBean.setRoleName(roleFigure.getFlowElementData().getFigureText());
							// 角色UIID
							activeBean.setRoleUIID(roleFigure.getFlowElementData().getFlowElementUUID());
						}
					}
				}
			} else { // 面板纵向
				int oneVHLine = 0;
				int twoVHLine = desktopPane.getWidth();
				boolean isContinue = false;
				for (JecnBaseVHLinePanel vhLinePanel : listVHLinePanel) {
					// 获取分割线
					JecnVHLineData vHLineData = (JecnVHLineData) vhLinePanel.getFlowElementData();

					// 角色是否压线
					if (vHLineData.getVhX() >= roleFigure.getFlowElementData().getFigureDataCloneable().getX()
							&& vHLineData.getVhX() <= roleFigure.getFlowElementData().getFigureDataCloneable().getX()
									+ roleFigure.getFlowElementData().getFigureDataCloneable().getWidth()) {
						isContinue = true;

					}

					// 循环判断小于角色位置点的分割线，并且离角色最近的一条
					if (oneVHLine < vHLineData.getVhX()
							&& vHLineData.getVhX() < roleFigure.getFlowElementData().getFigureDataCloneable().getX()) {
						oneVHLine = (int) vHLineData.getVhX();

					}
					if (twoVHLine > vHLineData.getVhX()
							&& vHLineData.getVhX() > roleFigure.getFlowElementData().getFigureDataCloneable().getX()) {
						twoVHLine = (int) vHLineData.getVhX();
					}
				}
				if (isContinue) {
					continue;
				}
				if (listVHLinePanel.size() > 0) {
					// 数据不为默认值
					for (JecnBaseActiveFigure activeFigure : listActiveFigure) {
						if (activeFigure.getFlowElementData().getFigureDataCloneable().getX() < twoVHLine
								&& activeFigure.getFlowElementData().getFigureDataCloneable().getX() > oneVHLine) {

							// 角色是否压线
							if (oneVHLine >= activeFigure.getFlowElementData().getFigureDataCloneable().getX()
									&& oneVHLine <= activeFigure.getFlowElementData().getFigureDataCloneable().getX()
											+ activeFigure.getFlowElementData().getFigureDataCloneable().getWidth()) {
								continue;

							}
							// 角色是否压线
							if (twoVHLine >= activeFigure.getFlowElementData().getFigureDataCloneable().getX()
									&& twoVHLine <= activeFigure.getFlowElementData().getFigureDataCloneable().getX()
											+ activeFigure.getFlowElementData().getFigureDataCloneable().getWidth()) {
								continue;

							}
							JecnActivityShowBean activeBean = getActiveBeanByUUID(flowOperationBean
									.getListAllActivityShow(), activeFigure.getFlowElementData().getFlowElementUUID());
							// 角色名称
							activeBean.setRoleName(roleFigure.getFlowElementData().getFigureText());
							// 角色UIID
							activeBean.setRoleUIID(roleFigure.getFlowElementData().getFlowElementUUID());
						}
					}
				}
			}
		}
	}

	/**
	 * 获取活动
	 * 
	 * @param listActiveList
	 *            活动数据集合
	 * @param activeUUID
	 *            活动UUID
	 * @return JecnActivityShowBean 活动操作说明数据对象
	 */
	private static JecnActivityShowBean getActiveBeanByUUID(List<JecnActivityShowBean> listActiveList, String activeUUID) {
		if (listActiveList == null || DrawCommon.isNullOrEmtryTrim(activeUUID)) {// 集合和数组是否存在
			JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.error("根据活动UUID获取活动数据对象异常" + "listActiveList"
					+ listActiveList + "activeUUID" + activeUUID);
			return null;
		}
		for (JecnActivityShowBean activeBean : listActiveList) {
			if (activeBean.getActiveFigure().getFlowElementData().getFlowElementUUID().equals(activeUUID)) {
				return activeBean;
			}
		}
		return null;
	}
}
