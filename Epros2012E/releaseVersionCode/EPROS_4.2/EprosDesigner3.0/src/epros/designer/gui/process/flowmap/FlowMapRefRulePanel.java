package epros.designer.gui.process.flowmap;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.rule.RuleChooseDialog;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;

/**
 * 流程地图关联制度Panel 2013-11-27
 * */
public class FlowMapRefRulePanel extends JecnPanel {
	private Logger log = Logger.getLogger(FlowMapRefRulePanel.class);
	/***/
	protected MapRefRuleTable flowMapRuleTable = null;
	/** 滚动面板 */
	private JScrollPane flowMapRuleScrollPane;
	/** 主面板 */
	protected JecnPanel mainPanel = new JecnPanel(600, 400);

	/** 底部按钮面板 */
	protected JecnPanel buttonPanel = new JecnPanel();
	/** 关联 */
	protected JButton refButton = new JButton(JecnProperties.getValue("relevance"));
	/** 删除 */
	protected JButton deleteButton = new JButton(JecnProperties.getValue("deleteBtn"));
	/** 制度 ===>制度选择窗体显示数据集合 */
	private List<JecnTreeBean> listRule = new ArrayList<JecnTreeBean>();
	private List<RuleT> listRuleBean = new ArrayList<RuleT>();
	/** 表格显示数据集合 */
	private Vector<Vector<Object>> listObjectRule = new Vector<Vector<Object>>();
	/** 制度ID */
	// private String ruleIds = null;
	private JecnTreeNode selectNode = null;
	/** 表格中临时制度Id */
	private Long ruleIdT = null;
	/** 提示Lab */
	private JLabel verfyLab = new JLabel("");
	private boolean isOperatin = false;

	public FlowMapRefRulePanel(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
		initComponents();
		initLayout();
	}

	private void initComponents() {
		// 获取流程地图与制度关联数据
		try {
			listRuleBean = ConnectionPool.getProcessAction().getRuleTByFlowId(selectNode.getJecnTreeBean().getId());
		} catch (Exception e1) {
			log.error("FlowMapRefRulePanel initComponents is error");
		}
		for (RuleT ruleT : listRuleBean) {
			// 制度选择窗体显示数据集合
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(ruleT.getId());
			jecnTreeBean.setName(ruleT.getRuleName());
			jecnTreeBean.setPid(ruleT.getPerId());
			if (ruleT.getIsDir() == 0) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleDir);
			} else if (ruleT.getIsDir() == 1) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleModeFile);
			} else if (ruleT.getIsDir() == 2) {
				jecnTreeBean.setTreeNodeType(TreeNodeType.ruleFile);
			}
			listRule.add(jecnTreeBean);
			// 表格显示数据集合
			Vector<Object> obj = new Vector<Object>();
			obj.add(ruleT.getId());// 制度ID
			obj.add(ruleT.getRuleName());// 制度名称
			obj.add(ruleT.getFileId());// 制度文件ID
			obj.add(ruleT.getIsDir());// 制度类别
			listObjectRule.add(obj);
		}
		flowMapRuleTable = new MapRefRuleTable(listObjectRule);
		flowMapRuleTable.getColumnModel().getColumn(4).setCellRenderer(new EditJLabel());

		flowMapRuleScrollPane = new JScrollPane();
		flowMapRuleScrollPane.setBorder(null);
		flowMapRuleScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		flowMapRuleScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// this.add(flowMapRuleScrollPane,BorderLayout.CENTER);
		flowMapRuleScrollPane.setViewportView(flowMapRuleTable);
		flowMapRuleScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置背景色
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		verfyLab.setForeground(Color.red);
		// 关联
		refButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				refButPerformed();
			}
		});
		// 删除
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}
		});
	}

	private void initLayout() {
		Insets insets = new Insets(3, 3, 3, 3);
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		// 表格面板
		mainPanel.add(flowMapRuleScrollPane, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		// 按钮面板
		mainPanel.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(refButton);
		buttonPanel.add(deleteButton);

		// 添加主面板
		Insets insets1 = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets1,
				0, 0);
		this.add(mainPanel, c);
	}

	private void refButPerformed() {
		RuleChooseDialog ruleChooseDialog = new RuleChooseDialog(listRule, 1L);
		ruleChooseDialog.setVisible(true);
		if (ruleChooseDialog.isOperation()) {
			Vector<Vector<String>> data = ((DefaultTableModel) flowMapRuleTable.getModel()).getDataVector();
			if (listRule != null && listRule.size() > 0) {
				if (data != null) {
					data.clear();
				}
				for (JecnTreeBean o : listRule) {
					Vector<Object> v = new Vector<Object>();
					v.add(String.valueOf(o.getId()));
					v.add(o.getName());
					if (TreeNodeType.ruleFile.toString().equals(o.getTreeNodeType().toString())) {
						// 获取文件ID
						try {
							RuleT rulet = ConnectionPool.getRuleAction().getRuleT(o.getId());
							v.add(String.valueOf(rulet.getFileId()));
							v.add("2");
						} catch (Exception e) {
							log.error("FlowMapRefRulePanel refButPerformed is error", e);
						}
					} else if (TreeNodeType.ruleDir.toString().equals(o.getTreeNodeType().toString())) {
						v.add("");
						v.add("0");
					} else if (TreeNodeType.ruleModeFile.toString().equals(o.getTreeNodeType().toString())) {
						v.add("");
						v.add("1");
					}
					v.add(new EditJLabel());
					((DefaultTableModel) this.flowMapRuleTable.getModel()).addRow(v);
				}
			} else {
				// 清空表格
				((DefaultTableModel) flowMapRuleTable.getModel()).setRowCount(0);
				// ruleIds = null;
			}
			isOperatin = true;
		}
	}

	public boolean isUpdate() {
		if (listRule.size() > 0 && listRuleBean.size() == 0) {
			return true;
		}
		if (listRule.size() == 0 && listRuleBean.size() > 0) {
			return true;
		}
		for (JecnTreeBean jecnTreeBean : listRule) {
			boolean isExist = false;
			for (RuleT rule : listRuleBean) {
				if (jecnTreeBean.getId().equals(rule.getId())) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				return true;
			}
		}
		return false;
	}

	public String getResultIds() {
		String ruleIds = null;
		Vector<Vector<Object>> data = ((DefaultTableModel) flowMapRuleTable.getModel()).getDataVector();
		for (Vector<Object> obj : data) {
			if (ruleIds == null) {
				ruleIds = String.valueOf(obj.get(0));
			} else {
				ruleIds = ruleIds + "," + String.valueOf(obj.get(0));
			}
		}
		return ruleIds;
	}

	/**
	 * 
	 * 渲染
	 * 
	 * @author Administrator
	 * @date： 日期：2013-11-20 时间：上午10:51:08
	 */
	class EditJLabel extends JLabel implements TableCellRenderer {
		// true：需要加色渲染
		private boolean isDef = false;

		EditJLabel() {
			this.setHorizontalAlignment(JLabel.CENTER);
			this.setVerticalAlignment(JLabel.CENTER);
			// 详情
			this.setText(JecnProperties.getValue("linkDetail"));
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			this.isDef = (flowMapRuleTable.getMyRow() == row && flowMapRuleTable.getMyColumn() == column);

			return this;
		}

		/**
		 * 
		 * 绘制组件
		 * 
		 */
		public void paint(Graphics g) {
			Graphics2D g2d = (Graphics2D) g;

			int w = this.getWidth();
			int h = this.getHeight();
			if (isDef) {
				// 渐变
				g2d.setPaint(new GradientPaint(0, 0, JecnUIUtil.getTopNoColor(), 0, h / 2, JecnUIUtil
						.getButtomNoColor()));
				g2d.fillRect(0, 0, w, h);
				this.setForeground(Color.red);
			} else {
				this.setForeground(Color.black);
			}

			super.paint(g);
		}
	}

	private void deleteButPerformed() {
		// 提示请选中行
		int[] selectRows = flowMapRuleTable.getSelectedRows();
		if (selectRows.length == 0) {
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		// 删除table选中的数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) flowMapRuleTable.getModel()).removeRow(selectRows[i]);
		}
		// 删除listObjectRule集合中对应数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			listRule.remove(selectRows[i]);
		}
		verfyLab.setText("");
	}

	/***
	 *双击Table打开制度
	 * 
	 * @return
	 */
	private void flowMapRuleTableMousePressed() {
		if (flowMapRuleTable.getSelectedColumn() != 4) {
			return;
		}
		int selectRow = this.flowMapRuleTable.getSelectedRow();
		if (selectRow == -1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		}
		// 获取制度类型
		TreeNodeType nodeType = null;
		if ("1".equals(flowMapRuleTable.getValueAt(selectRow, 3).toString())) {
			nodeType = TreeNodeType.ruleModeFile;
		} else if ("2".equals(flowMapRuleTable.getValueAt(selectRow, 3).toString())) {
			nodeType = TreeNodeType.ruleFile;
		}
		// 打开制度
		JecnDesignerCommon.openRuleInfoDialog(Long.valueOf(flowMapRuleTable.getValueAt(selectRow, 0).toString()),
				nodeType);
	}

	class MapRefRuleTable extends FlowMapRefRuleTable {
		public MapRefRuleTable(Vector<Vector<Object>> listObj) {
			super(listObj);
			this.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2) {
						flowMapRuleTableMousePressed();
					}
				}

				/**
				 * 鼠标移动到表格监听
				 */
				public void mouseExited(MouseEvent e) {
					mouseMovedProcess(e);
				}
			});

			/**
			 * 添加鼠标监听
			 * 
			 */
			this.addMouseMotionListener(new MouseAdapter() {
				public void mouseMoved(MouseEvent e) {
					mouseMovedProcess(e);
				}
			});
		}

		public Vector<Vector<Object>> getContent(Vector<Vector<Object>> list) {
			Vector<Vector<Object>> content = new Vector<Vector<Object>>();
			if (list != null) {
				for (Vector<Object> obj : list) {
					Vector<Object> data = new Vector<Object>();
					data.add(obj.get(0));
					data.add(obj.get(1));
					data.add(obj.get(2));
					data.add(obj.get(3));
					data.add(new EditJLabel());
					content.add(data);
				}
			}
			return content;
		}
	}

	public boolean isOperatin() {
		return isOperatin;
	}

	public void setOperatin(boolean isOperatin) {
		this.isOperatin = isOperatin;
	}

	public List<JecnTreeBean> getListRule() {
		return listRule;
	}

	public void setListRule(List<JecnTreeBean> listRule) {
		this.listRule = listRule;
	}

}
