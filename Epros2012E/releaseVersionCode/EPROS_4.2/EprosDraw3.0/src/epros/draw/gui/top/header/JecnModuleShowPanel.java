package epros.draw.gui.top.header;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import epros.draw.gui.box.JecnToolBoxMainPanel;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 显示/隐藏工具栏、流程元素库以及流程元素模型面板的操作面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnModuleShowPanel extends JPanel implements ItemListener {
	/** 画图面板横竖标题栏显示复选框 */
	private JCheckBox workflowTitleCheckBox = null;
	/** 工具栏显示复选框 */
	private JCheckBox toolBarCheckBox = null;
	/** 流程元素库显示复选框 */
	private JCheckBox flowElemCheckBox = null;
	/** 流程元素模型显示复选框 */
	private JCheckBox modelCheckBox = null;
	/** 扩展项集合 */
	private List<JCheckBox> desCheckBoxList = new ArrayList<JCheckBox>();

	public JecnModuleShowPanel() {
		initComponents();
		initLayout();
	}

	private void initComponents() {
		/** 画图面板横竖标题栏显示复选框 */
		workflowTitleCheckBox = new JCheckBox(JecnDrawMainPanel.getMainPanel().getResourceManager().getValue(
				"workflowHeader"));
		// 工具栏显示复选框
		toolBarCheckBox = new JCheckBox(JecnResourceUtil.getJecnResourceUtil().getValue("toolbarName"));
		// 流程元素显示复选框
		flowElemCheckBox = new JCheckBox(JecnDrawMainPanel.getMainPanel().getBoxPanel().getResourceManager().getValue(
				"title"));
		// 模型显示复选框
		modelCheckBox = new JCheckBox(JecnDrawMainPanel.getMainPanel().getBoxPanel().getResourceManager().getValue(
				"toolboxModel"));

		this.setLayout(new FlowLayout(FlowLayout.RIGHT, 0, 0));
		// 透明
		this.setOpaque(false);
		this.setBorder(null);

		// 背景颜色
		workflowTitleCheckBox.setOpaque(false);
		toolBarCheckBox.setOpaque(false);
		flowElemCheckBox.setOpaque(false);
		modelCheckBox.setOpaque(false);

		// 选中是显示焦点框
		workflowTitleCheckBox.setFocusable(false);
		toolBarCheckBox.setFocusable(false);
		flowElemCheckBox.setFocusable(false);
		modelCheckBox.setFocusable(false);

		// 事件
		workflowTitleCheckBox.addItemListener(this);
		toolBarCheckBox.addItemListener(this);
		flowElemCheckBox.addItemListener(this);
		modelCheckBox.addItemListener(this);

		// 默认选中
		workflowTitleCheckBox.setSelected(JecnSystemStaticData.isWorkflowTitleShow());
		toolBarCheckBox.setSelected(true);
		flowElemCheckBox.setSelected(true);
		modelCheckBox.setSelected(true);
	}

	private void initLayout() {
		addCheckBox(workflowTitleCheckBox);
		addCheckBox(toolBarCheckBox);
		addCheckBox(flowElemCheckBox);
		addCheckBox(modelCheckBox);
	}

	/**
	 * 
	 * 插入指定位置一个单选框
	 * 
	 * @param checkBox
	 *            JCheckBox 单选框
	 * @param index
	 *            int 插入位置
	 */
	public void insertCheckBox(JCheckBox checkBox, int index) {
		if (checkBox == null || index < 0 || index >= desCheckBoxList.size()) {
			return;
		}
		// 添加到集合
		desCheckBoxList.add(index, checkBox);

		// 重新添加
		this.removeAll();
		for (JCheckBox currCheckBox : desCheckBoxList) {
			this.add(currCheckBox);
		}
	}

	/**
	 * 
	 * 添加一个单选框
	 * 
	 * @param checkBox
	 *            JCheckBox 单选框
	 */
	public void addCheckBox(JCheckBox checkBox) {
		if (checkBox == null) {
			return;
		}
		// 添加到集合
		desCheckBoxList.add(checkBox);
		this.add(checkBox);
	}

	/**
	 * 
	 * 事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == workflowTitleCheckBox) {// 画图面板横竖标题栏显示复选框
			// 没有画图面板时，同样改变状态
			JecnSystemStaticData.setWorkflowTitleShow(workflowTitleCheckBox.isSelected());
			JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
			if (workflow == null) {
				return;
			}
			workflow.getScrollPanle().showScrollPanelHeader();
			return;
		}

		// 画图面板&流程元素库的容器
		JecnSplitPane splitPane = JecnDrawMainPanel.getMainPanel().getSplitPane();

		if (splitPane == null) {
			return;
		}

		// 流程元素库
		JecnToolBoxMainPanel boxPanel = JecnDrawMainPanel.getMainPanel().getBoxPanel();

		if (e.getSource() == toolBarCheckBox) {// 工具栏显示复选框
			JComponent comp = JecnDrawMainPanel.getMainPanel().getToolbar();
			if (toolBarCheckBox.isSelected()) {
				comp.setVisible(true);
			} else {
				comp.setVisible(false);
			}
		} else if (e.getSource() == flowElemCheckBox || e.getSource() == modelCheckBox) {// 流程元素显示复选框

			proTwoSplitPane(splitPane, boxPanel.getSplitPane(), flowElemCheckBox, modelCheckBox);
		}
		JecnDrawMainPanel.getMainPanel().revalidate();
		JecnDrawMainPanel.getMainPanel().repaint();
	}

	/**
	 * 
	 * 条件：allSplitPane中一端包含partSplitPane组件
	 * partSplitPane中left面板和right面板显示/隐藏导致allSplitPane的变化
	 * 
	 * @param allSplitPane
	 *            JecnSplitPane 外面一个分割面板
	 * @param partSplitPane
	 *            JecnSplitPane allSplitPane的子组件，放置到其中一个面板
	 * @param leftBox
	 *            JCheckBox 对应分割面板左面板的单选框
	 * @param rightBox
	 *            JCheckBox 对应分割面板右面板的单选框
	 */
	private void proTwoSplitPane(JecnSplitPane allSplitPane, JecnSplitPane partSplitPane, JCheckBox leftBox,
			JCheckBox rightBox) {
		// 记录分隔条的位置点
		// 外面一个分割面板
		allSplitPane.initdividerLocing();
		// 内部一个分割面板
		partSplitPane.initdividerLocing();

		if (leftBox.isSelected() || rightBox.isSelected()) {// 其中一个选中
			// 画图面板&流程元素库都显示
			allSplitPane.showBothPanel();
		} else {// 都没有选中
			// 只显示画图面板
			allSplitPane.showLeftPanel();
		}

		if (leftBox.isSelected() && rightBox.isSelected()) {// 两个都选中
			// 流程元素&模型都显示
			partSplitPane.showBothPanel();
		} else if (leftBox.isSelected()) {// 流程元素选中
			// 只显示流程元素
			partSplitPane.showLeftPanel();
		} else if (rightBox.isSelected()) {// 模型选中
			// 只显示模型
			partSplitPane.showRightPanel();
		}
	}

	public void modelCheckBoxFlase() {
		modelCheckBox.setSelected(false);
	}
}
