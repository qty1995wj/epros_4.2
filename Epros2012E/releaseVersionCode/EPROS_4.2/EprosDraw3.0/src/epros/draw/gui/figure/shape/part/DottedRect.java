package epros.draw.gui.figure.shape.part;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 
 * 协作框
 * 
 * @date： 日期：Mar 21, 2012 时间：3:33:32 PM
 * 
 * @author ZHANGXH
 */
public class DottedRect extends JecnBaseFigurePanel {

	// private Color color = Color.BLACK;
	private int x1 = 0;
	private int y1 = 0;

	/** Creates a new instance of DottedRect */
	public DottedRect(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawRect(x1, y1, userWidth, userHeight);
		// g2d.setFont(figureText.getFont());
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		g2d.fillRect(x1, y1, userWidth, userHeight);
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawRect(x1, y1, userWidth, userHeight);
	}
}
