package epros.designer.gui.popedom.role.choose;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.popedom.organization.OrgChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * @author yxw 2012-9-4
 * @description：人员选择
 */
public class RoleManagePersonChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(OrgChooseDialog.class);

	/** 搜索按钮 （科大讯飞要求人员精确搜索） */
	private JButton searchBtn = null;

	public RoleManagePersonChooseDialog(List<JecnTreeBean> list) {
		super(list, 30);
		this.setTitle(JecnProperties.getValue("personChoose"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				if (jecnTreeBean.getId() != null || jecnTreeBean.getName() != null) {
					Vector<String> data = new Vector<String>();
					data.add(jecnTreeBean.getId().toString());
					if (jecnTreeBean.getPname() != null && !jecnTreeBean.getPname().trim().equals("")) {
						data.add(jecnTreeBean.getName() + "(" + jecnTreeBean.getLoginName() + ")" + "        <"
								+ jecnTreeBean.getPname() + ">");
					} else {
						data.add(jecnTreeBean.getName());
					}
					content.add(data);
				}
			}
		}
		return content;
	}

	/**
	 * 科大讯飞人员搜索 只有科大讯飞 用
	 * 
	 * @return
	 */
	public void searchIfytekByName() {
		List<String> names = new ArrayList<String>();
		names.add(this.searchText.getText());
		try {
			searchListJecnTreeBean = ConnectionPool.getPersonAction().searchUserByIftekSyncLoginName(names,
					JecnConstants.projectId);
			this.searchTable.reAddVectors(getContent(searchListJecnTreeBean));
		} catch (Exception e) {
			log.error("PersonChooseDialog searchByName is error！", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
		}
	}

	protected void showSearchButton(JPanel searchPostPanel) {
		if (!JecnConfigTool.isIflytekLogin()) {
			return;
		}
		searchBtn = new JButton(JecnProperties.getValue("search"));
		searchPostPanel.add(searchBtn);
		searchBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				searchIfytekByName();
			}
		});

	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			// 科大讯飞人员搜索 禁用模糊搜索
			if (JecnConfigTool.isIflytekLogin()) {
				return new ArrayList<JecnTreeBean>();
			}
			return ConnectionPool.getPersonAction().searchRoleAuthByName(name, JecnConstants.projectId,
					JecnDesignerCommon.getSecondAdminUserId());
		} catch (Exception e) {
			log.error("RoleManagePersonChooseDialog searchByName is error！", e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

	@Override
	public String getSearchLabelName() {

		return JecnProperties.getValue("nameC");
	}

	@Override
	public JecnTree getJecnTree() {
		return new RoleManagePersonChooseTree();
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));// 名称
		return title;
	}

	protected void searchTablemouseReleased(MouseEvent evt) {
		if (evt.getClickCount() != 2) {
			return;
		}
		int row = searchTable.getSelectedRow();
		if (row != -1) {
			JecnTreeBean jecnTreeBean = this.searchListJecnTreeBean.get(row);
			// 判断选择人员是否存在角色权限，存在提示
			if (isRolePersonAuthValidate(jecnTreeBean)) {
				return;
			}
		}
		super.searchTablemousePressed(evt);
	}
}
