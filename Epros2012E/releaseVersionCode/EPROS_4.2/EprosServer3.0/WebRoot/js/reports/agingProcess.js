//流程实效性 3.06废弃
function agingProcessGrid() {
	agingProcessGridStore = new Ext.data.Store({
				autoLoad : {
					params : {
						start : 0,
						limit : pageSize
					}
				},
				proxy : new Ext.data.HttpProxy({
							url : "agingProcess.action"
						}),
				reader : new Ext.data.JsonReader({
							totalProperty : "totalProperty",
							root : "root",
							id : "id"
						}, [{
									name : "flowId",
									mapping : "flowId"
								}, {
									name : "flowName",
									mapping : "flowName"
								}, {
									name : "flowIdInput",
									mapping : "flowIdInput"
								}, {
									name : "resPeopleName",
									mapping : "resPeopleName"
								}, {
									name : "orgName",
									mapping : "orgName"
								}, {
									name : "pubDate",
									mapping : "pubDate"
								},{
									name : "updateType",
									mapping : "updateType"
								}])

			});

	agingProcessGridColumnModel = new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(), {
				header : i_processName,
				dataIndex : "flowName",
				align : "center",
				menuDisabled : true,
				sortable : true,
				renderer:processNameRenderer
			}, {
				header : i_processNumber,
				dataIndex : "flowIdInput",
				align : "center",
				menuDisabled : true
			}, {
				header : i_DutyPerson,
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_dutyOrg,
				dataIndex : "orgName",
				align : "center",
				menuDisabled : true
			}, {
				header : i_pubDate,
				dataIndex : "pubDate",
				align : "center",
				sortable : true,
				menuDisabled : true
			},{
				header : i_state,
				dataIndex : "updateType",
				align : "center",
				menuDisabled : true,
				renderer:secretUpdateType
			}

	]);
	agingProcessGridBbar = new Ext.PagingToolbar({
				pageSize : pageSize,
				store : agingProcessGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	agingProcessGridPanel = new Ext.grid.GridPanel({
				id : "agingProcessGridPanel_id",
				renderTo : 'agingProcessGrid_id',
				height :  Ext.getCmp("main-panel").getInnerHeight()-175,
				autoScroll: true,//滚动条
				stripeRows: true,//显示行的分隔符
				//columnLines:true,//显示列的分隔线
				viewConfig : {
					 forceFit : true
					// 让列的宽度和grid一样
				},
				frame : false,
				store : agingProcessGridStore,
				colModel : agingProcessGridColumnModel,
				bbar : agingProcessGridBbar
			})
             agingProcessPieCharts();
reports_index_id='reports_agingProcess_id';             
}

// 状态为未更新和已更新
function secretUpdateType(value){
	if(value==0){
		return i_failedToUpdate;
	}else if(value==1){
		return i_hasBeenUpdated;
	}
}


// 加载流程时效性统计
function agingProcessPieCharts(){
     //时间段
	var time=document.getElementById("timeType_id").value;
	$("#sysGrid_id").children().remove();
	$("#sysGrid_id").load('agingProcessPieCharts.action',{
	"resOrgId":document.getElementById("orgDutyId_aging_id").value,
	"processId":document.getElementById("processId_aging_id").value,
	"time":document.getElementById("timeType_id").value,
	"updateType":document.getElementById("state_id").value
	  },function(){
		
	});
}


/**
 * 流程时效性统计
 */
function agingProcessGridSearch(){
	 //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_aging_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_aging_id").value;
	 //时间段
	 var time=document.getElementById("timeType_id").value;
	  //更新状态
	 var updateType=document.getElementById("state_id").value;
	 time = time.replace(/[ ]/g,""); 
	 if(time==null || time == ""){
	    alert(i_timeCantBeEmpty);
        return;
	 }
	 var reg = new RegExp("^[0-9]*$");
     if(!reg.test(time)){
        alert(i_pleaseEnterTheNumbers);
        return;
     }
	 if(resOrgId&&resOrgId!=""){
     	agingProcessGridStore.setBaseParam("resOrgId", resOrgId);
     }
     if(processId&&processId!=""){
     	agingProcessGridStore.setBaseParam("processId", processId);
     }
     agingProcessGridStore.setBaseParam("time", time);
     agingProcessGridStore.setBaseParam("updateType", updateType);
	 agingProcessGridStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});
	agingProcessPieCharts();
}

/**
 * 流程时效性重置
 **/
function agingProcessSearchReset(){
        document.getElementById("orgDutyName_aging_id").value = "";
		document.getElementById("processName_aging_id").value = "";
		document.getElementById("orgDutyId_aging_id").value = "-1";
		document.getElementById("processId_aging_id").value = "-1";
		document.getElementById("timeType_id").value= "12";
		document.getElementById("state_id").options[0].selected=true;
}

// 流程时效性下载
function agingProcessDownloadExcel(){
    //查阅部门ID 
	 var resOrgId=document.getElementById("orgDutyId_aging_id").value;
	 //流程ID
	 var processId=document.getElementById("processId_aging_id").value;
	 //时间段
	 var time=document.getElementById("timeType_id").value;
	  //更新状态
	 var updateType=document.getElementById("state_id").value;
    window.location.href="downloadAgingProcess.action?resOrgId="+resOrgId+"&processId="+processId+"&time="+time+"&updateType="+updateType;
}