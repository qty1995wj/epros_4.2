package epros.draw.gui.figure.shape.part;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ToolTipManager;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.TermFigureData;
import epros.draw.gui.designer.JecnDesignerLinkPanel;
import epros.draw.gui.figure.BaseRect;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 术语 （勘探院新增元素）
 * 
 *@author ZXH
 * @date 2016-8-27上午11:42:07
 */
public class TermFigureAR extends BaseRect {

	private JLabel jLabel;

	public TermFigureAR(JecnFigureData flowElementData) {
		super(flowElementData);
		jLabel = new JLabel(JecnFileUtil.getToolBoxImageIcon("point"));
		initTermPoint();

		// 鼠标悬浮事件
		initMouseEvent();
	}

	public TermFigureData getFlowElementData() {
		TermFigureData termFigureData = (TermFigureData) flowElementData;
		return termFigureData;
	}

	/**
	 * 
	 * 提示信息事件
	 * 
	 */
	public void initMouseEvent() {

		this.addMouseListener(new MouseAdapter() {

			public void mouseEntered(MouseEvent paramMouseEvent) {
				// 初始化
				setToolTipText(null);

				// 术语名称
				String name = getFlowElementData().getFigureText();
				// 术语定义
				String termDefine = getFlowElementData().getTermDefine();

				if (DrawCommon.isNullOrEmtryTrim(name) && DrawCommon.isNullOrEmtryTrim(termDefine)) {// 如果存在活动说明或关键说明都为空
					return;
				}

				// 指定取消工具提示的延迟值 设置为一小时 3600秒
				ToolTipManager.sharedInstance().setDismissDelay(3600 * 1000);
				// 组织HTML格式字符串
				StringBuffer sbuf = new StringBuffer();
				sbuf
						.append("<html><div style= 'white-space:normal; display:block;min-width:200px; word-break:break-all'>");

				sbuf.append("<br><b>");
				// JecnDrawMainPanel.getMainPanel().getResourceManager().getValue("posName")
				sbuf.append(JecnResourceUtil.getJecnResourceUtil().getValue("nameC"));
				sbuf.append("</b><br><br>");
				sbuf.append(DrawCommon.getsubstring(name));
				sbuf.append("<br><br>");

				sbuf.append("<br><b>");
				sbuf.append(JecnResourceUtil.getJecnResourceUtil().getValue("definitionC"));
				sbuf.append("</b><br><br>");
				sbuf.append(DrawCommon.getsubstring(termDefine));
				sbuf.append("<br><br>");

				sbuf.append("</div><br></html>");

				// 注册要在工具提示中显示的文本。光标处于该组件上时显示该文本。如果 参数 为 null，则关闭此组件的工具提示
				setToolTipText(sbuf.toString());
			}

			public void mouseExited(MouseEvent paramMouseEvent) {
				// 返回初始延迟值
				int intD = ToolTipManager.sharedInstance().getInitialDelay();
				// 取消工具提示的延迟值
				ToolTipManager.sharedInstance().setDismissDelay(intD);
			}
		});
	}

	private void initTermPoint() {
		this.setLayout(new BorderLayout());
		JPanel jPanel = new JecnDesignerLinkPanel(this);
		jPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jPanel.add(jLabel);
		this.add(jPanel, BorderLayout.SOUTH);
	}
}
