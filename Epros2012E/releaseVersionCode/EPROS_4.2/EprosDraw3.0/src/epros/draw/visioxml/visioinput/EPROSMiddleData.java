package epros.draw.visioxml.visioinput;

import java.util.List;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnVHLineData;

/**
 * EPROS图形中间数据
 * 
 * @author admin
 * 
 */
class EPROSMiddleData {
	/** 面板宽度 **/
	private double panelWidth;
	/** 面板高度 */
	private double panelHeight;
	/** 面板上元素数据集合 */
	private List<JecnFigureData> figureDataLists;
	/** 面板上连接线集合 */
	private List<JecnManhattanLineData> manLines;
	/** 横竖分割线集合 */
	private List<JecnVHLineData> vHLines;
	/** 横线、竖线、斜线 */
	private List<JecnBaseDivedingLineData> divedingLines;

	public List<JecnFigureData> getFigureDataLists() {
		return figureDataLists;
	}

	public void setFigureDataLists(List<JecnFigureData> figureDataLists) {
		this.figureDataLists = figureDataLists;
	}

	public List<JecnManhattanLineData> getManLines() {
		return manLines;
	}

	public void setManLines(List<JecnManhattanLineData> manLines) {
		this.manLines = manLines;
	}

	public List<JecnVHLineData> getVHLines() {
		return vHLines;
	}

	public void setVHLines(List<JecnVHLineData> vHLines) {
		this.vHLines = vHLines;
	}

	public List<JecnBaseDivedingLineData> getDivedingLines() {
		return divedingLines;
	}

	public void setDivedingLines(List<JecnBaseDivedingLineData> divedingLines) {
		this.divedingLines = divedingLines;
	}

	public double getPanelWidth() {
		return panelWidth;
	}

	public void setPanelWidth(double panelWidth) {
		this.panelWidth = panelWidth;
	}

	public double getPanelHeight() {
		return panelHeight;
	}

	public void setPanelHeight(double panelHeight) {
		this.panelHeight = panelHeight;
	}
}
