package epros.draw.gui.operationConfig.unit;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.headerfooter.RtfHeaderFooter;
import com.lowagie.text.rtf.style.RtfFont;

import epros.draw.constant.JecnLogConstants;
import epros.draw.gui.figure.unit.JecnFlowElementCopyAndPaste;
import epros.draw.gui.operationConfig.DrawOperationDescriptionConfigBean;
import epros.draw.gui.operationConfig.OperationConfigUtil;
import epros.draw.gui.operationConfig.ProcessOperationIntrusDialog;
import epros.draw.gui.operationConfig.bean.JecnFlowOperationBean;
import epros.draw.gui.operationConfig.data.JecnFlowOperationDataUnit;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.io.ImportFlowFilter;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUserCheckUtil;

public class RtfCommon {
	private static final Log log = LogFactory.getLog(RtfCommon.class);

	/**
	 * 流程文件信息
	 * 
	 * @param document
	 * @param processBaseInfoBean
	 * @param recordBeanList
	 */
	public static void returnToFlowHeader(Document document, JecnFlowOperationBean operationBean, Image headerImg) {
		try {
			Table table = new Table(5);
			table.setWidth(100f);
			table.setWidths(new int[] { 33, 16, 17, 16, 17 });
			table.setBorderWidth(1);
			table.setBorderColor(new Color(255, 255, 255));
			Cell cell = null;
			if (headerImg != null) {
				headerImg.scaleAbsolute(150, 35);
				cell = new Cell(headerImg);
			} else {
				cell = new Cell("");
			}
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			cell.setRowspan(2);
			table.addCell(cell, new Point(0, 0));

			// 文件名称：
			cell = new Cell(ST_5_BOLD_MIDDLE(JecnResourceUtil.getJecnResourceUtil().getValue("fileNameC")));
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 1));
			cell = new Cell(operationBean.getFlowName());
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 2));
			// 文件编号：
			cell = new Cell(ST_5_BOLD_MIDDLE(JecnResourceUtil.getJecnResourceUtil().getValue("fileNumberC")));
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 1));
			cell = new Cell(operationBean.getFlowInputNum());
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 2));
			// 密集：
			cell = new Cell(ST_5_BOLD_MIDDLE(JecnResourceUtil.getJecnResourceUtil().getValue("intensiveC")));
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 3));
			cell = new Cell("");
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(0, 4));
			// 版本:
			cell = new Cell(ST_5_BOLD_MIDDLE(JecnResourceUtil.getJecnResourceUtil().getValue("versionC")));
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 3));
			cell = new Cell("");
			cell.setBorderWidth(1);
			cell.setBorderColor(new Color(255, 255, 255));
			table.addCell(cell, new Point(1, 4));

			document.setHeader(new RtfHeaderFooter(table));
		} catch (DocumentException e) {
			log.error("", e);
		}
	}

	/**
	 * 生成word 页脚
	 * 
	 * @author fuzhh Nov 8, 2012
	 * @param document
	 */
	public static void getFooterDoc(Document document) {
		HeaderFooter foot = null;
		foot = new HeaderFooter(new Phrase(JecnResourceUtil.getJecnResourceUtil().getValue("wordTheFooter")), false);
		foot.setAlignment(Rectangle.ALIGN_CENTER);
		document.setFooter(foot);
	}

	/**
	 * 获得Image对象
	 * 
	 * @param filePath
	 * @return
	 * @throws BadElementException
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws Exception
	 */
	public static Image getImage(String filePath) {
		try {
			if (filePath == null || "".equals(filePath)) {
				JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.info("JecnFinal中方法getImage：文件丢失");
				return null;
			}
			File file = new File(filePath);
			if (file.exists()) {

				return Image.getInstance(getByte(file));
			} else {
				JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.info("JecnFinal中方法getImage：文件丢失");
				return null;
			}
		} catch (Exception e) {
			JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.info("JecnFinal中方法getImage：IO异常");
		}
		return null;
	}

	/**
	 * 把一个文件转化为字节
	 * 
	 * @param file
	 * @return byte[]
	 * @throws Exception
	 */
	public static byte[] getByte(File file) {
		byte[] bytes = null;
		if (file != null) {
			InputStream is = null;
			try {
				is = new FileInputStream(file);
				int length = (int) file.length();
				if (length > Integer.MAX_VALUE) // 当文件的长度超过了int的最大值
				{
					return null;
				}
				bytes = new byte[length];
				int offset = 0;
				int numRead = 0;
				while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
					offset += numRead;
				}
				// 如果得到的字节长度和file实际的长度不一致就可能出错了
				if (offset < bytes.length) {
					return null;
				}
			} catch (Exception e) {
				JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.error("文件转换为字节流异常！", e);
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						JecnLogConstants.LOG_FLOW_OPERATION_DATA_BEAN.error("关闭输入流异常！", e);
					}
				}
			}
		}
		return bytes;
	}

	/**
	 * 宋体五号,加粗,居中,显示为11号,没有五号
	 */
	public static Paragraph ST_5_BOLD_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 11, Font.BOLD));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 宋体小三,居中
	public static Paragraph ST_X3_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 15));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	// 宋体小二,加粗,居中

	public static Paragraph ST_X2_BOLD_MIDDLE(String content) {
		Paragraph p = new Paragraph(content, new RtfFont("宋 体", 18, Font.BOLD));
		p.setAlignment(Element.ALIGN_CENTER);
		return p;
	}

	/**
	 * 宋体四号
	 * 
	 * @param content
	 * @return
	 */
	private static Paragraph ST_4(String content) {
		return new Paragraph(content, new RtfFont("宋 体", 14));
	}

	/**
	 * 流程操作说明下载处理
	 * 
	 */
	public static void downLoadMenuMouseClicked() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		// 角色移动
		if (JecnSystemStaticData.isShowRoleMove()) {
			// 面板
			JecnDrawMainPanel.getMainPanel().getToolbar().getFormatToolBarItemPanel().getRoleMoveCheckBox()
					.setSelected(false);
			JecnSystemStaticData.setShowRoleMove(false);
			// 移除角色移动
			desktopPane.removeJecnRoleMobile();
			desktopPane.repaint();
		}
		desktopPane.getWorkflowZoomProcess().zoomByNewWorkflowScale(1.0);
		// 设置界面风格居中
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ex) {
			log.error("", ex);
		}
		String fileDirectory = null;
		// 获取默认路径
		String sourseDirectory = JecnSystemData.readCurrSystemFileIOData().getDownLoadOperationDir();
		JFileChooser fileChooser = new JecnFileChooser(sourseDirectory);
		// 移除系统给定的文件过滤器
		fileChooser.setAcceptAllFileFilterUsed(false);
		// 过滤文件
		fileChooser.setFileFilter(new ImportFlowFilter("doc"));
		// 设置标题
		fileChooser.setDialogTitle("文件保存");
		// 文件名称获取当前面板名称
		fileChooser.setSelectedFile(new File(desktopPane.getFlowMapData().getName()));

		int i = fileChooser.showSaveDialog(null);
		// 选择确认（yes、ok）后返回该值。
		if (i == javax.swing.JFileChooser.APPROVE_OPTION) {
			File fileSelect = fileChooser.getSelectedFile();
			fileDirectory = fileSelect.getPath();
			String dirStr = fileSelect.getParent();
			String pathName = fileChooser.getSelectedFile().getName();
			String error = JecnUserCheckUtil.checkFileName(pathName);
			ProcessOperationIntrusDialog operationIntrus = new ProcessOperationIntrusDialog();
			if (!"".equals(error)) {
				JecnOptionPane.showMessageDialog(operationIntrus, error, null, JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (!sourseDirectory.equals(dirStr)) {
				// 修改配置文件中的路径
				JecnSystemData.readCurrSystemFileIOData().setDownLoadOperationDir(dirStr);
				JecnSystemData.writeCurrSystemFileIOData();
			}
		}
		if (fileDirectory == null) {
			return;
		}
		// 生成流程图片
		Image img = null;
		desktopPane.getCurrentSelectElement().clear();
		desktopPane.getCurrentSelectElement().addAll(desktopPane.getPanelList());
		JecnFlowElementCopyAndPaste.copyImageHandle(desktopPane.getCurrentSelectElement());
		// 隐藏流程元素选中点
		JecnWorkflowUtil.hideAllResizeHandle();

		// 获取图片目录路径
		String pathCopy = JecnFlowElementCopyAndPaste.COPY_PRINT;

		File flie = new File(pathCopy);
		// 图片路径
		String filePath = "";
		if (flie.exists()) {
			File[] files = flie.listFiles();
			filePath = files[0].getPath();
		}
		try {
			img = Image.getInstance(filePath);
		} catch (BadElementException e) {
			log.error("流程操作说明下载获取图片信息异常，方法downLoadMenuMouseClicked 原因：BadElementException", e);
		} catch (MalformedURLException e) {// 路径错误或路径存在无法解析字符串
			log.error("流程操作说明下载获取图片信息异常，方法downLoadMenuMouseClicked 原因：路径错误或路径存在无法解析字符串", e);
		} catch (IOException e) {// 读取文件异常
			log.error("流程操作说明下载获取图片信息异常，方法downLoadMenuMouseClicked 原因：读取文件异常", e);
		}
		// 获取操作说明数据
		JecnFlowOperationBean operationBean = JecnFlowOperationDataUnit.getJecnFlowOperationBean();
		// 获取操作说明配置文件显示项信息
		List<DrawOperationDescriptionConfigBean> operationDescriptionConfigBeanList = OperationConfigUtil
				.readOperationConfigXml();
		if (operationBean == null) {
			return;
		}
		try {
			// 导出流程操作说明 创建word文档
			DrawProcessOperationInfoRTF.createWordFile(operationBean, img, operationDescriptionConfigBeanList,
					fileDirectory, false);
		} catch (Exception ex) {
			log.error("", ex);
			return;
		}
	}
}
