package com.jecn.epros.server.bean.rule;

public class JecnFlowCommon implements java.io.Serializable {
	private Long flowId;//流程ID
	private String flowNumber;//流程编号
	private String flowName;//流程名称
	private Long ruleTitleId;//制度标题ID 
	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public String getFlowNumber() {
		return flowNumber;
	}
	public void setFlowNumber(String flowNumber) {
		this.flowNumber = flowNumber;
	}
	public String getFlowName() {
		return flowName;
	}
	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}
	public Long getRuleTitleId() {
		return ruleTitleId;
	}
	public void setRuleTitleId(Long ruleTitleId) {
		this.ruleTitleId = ruleTitleId;
	}
}

