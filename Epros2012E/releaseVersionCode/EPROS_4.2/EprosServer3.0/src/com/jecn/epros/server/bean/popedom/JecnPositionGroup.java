package com.jecn.epros.server.bean.popedom;

import java.util.Date;

/**
 * 岗位组
 * 
 * @author Administrator
 * 
 */
public class JecnPositionGroup implements java.io.Serializable {
	private Long id;// 岗位组ID
	private Long projectId;// 工程ID
	private String name;// 岗位组Name
	private Long perId;// 父节点
	private Integer isDir;// 目录（0是岗位组，1是目录）
	private Integer sortId;// 排序
	private Date createTime;// 创建时间
	private Long createPersonId;// 创建人
	private Date updateTime;// 更新时间
	private Long updatePersonId;// 更新人
	private String tPath;// tpath 节点层级关系
	private int tLevel;// level 节点级别，默认0开始
	private String viewSort;
	private Integer dataType;// 岗位组 创建类型 0 本地新建 1岗位组同步

	public Integer getDataType() {
		return dataType;
	}

	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}

	public String getViewSort() {
		return viewSort;
	}

	public void setViewSort(String viewSort) {
		this.viewSort = viewSort;
	}

	public String gettPath() {
		return tPath;
	}

	public void settPath(String tPath) {
		this.tPath = tPath;
	}

	public int gettLevel() {
		return tLevel;
	}

	public void settLevel(int tLevel) {
		this.tLevel = tLevel;
	}

	public Integer getSortId() {
		return sortId;
	}

	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getPerId() {
		return perId;
	}

	public void setPerId(Long perId) {
		this.perId = perId;
	}

	public Integer getIsDir() {
		return isDir;
	}

	public void setIsDir(Integer isDir) {
		this.isDir = isDir;
	}

	public Long getUpdatePersonId() {
		return updatePersonId;
	}

	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}
}
