package epros.tray;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import jecntool.file.JecnFileUtil;
import jecntool.util.Tool;

import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import epros.designer.util.ConnectionPool;
/**
 * 
 * tomcat，rmi，mysql端口处理类
 * 
 * 
 */
public class JecnPort {
	private static Logger log = Logger.getLogger(JecnPort.class);

	/** tomcatJVM默认端口类型 */
	private final String TYPE_TC_JVM = "tcJvm";
	/** tomcat服务默认端口类型 */
	private final String TYPE_TC_MAIN = "tcMain";
	/** tomcatAJP默认端口类型 */
	private final String TYPE_TC_AJP = "tcAjp";
	/** RMI默认端口类型 */
	private final String TYPE_RMI = "rmi";
	/** mysql默认端口类型 */
	private final String TYPE_MYSQL = "mysql";

	private final String PORT_NAME = "port";

	/** 已经选中空闲端口，tomcat3个，mysql1个，rmi1个 */
	private Set<Integer> portSet = new HashSet<Integer>();

	/** tomcatJVM默认端口 */
	private int tmcJvmPort = 8005;
	/** tomcat服务默认端口 */
	private int tmcPort = 8080;
	/** tomcatAJP默认端口 */
	private int tmcAjpPort = 8009;
	/** RMI默认端口 */
	private int rmiPort = 2088;
	/** mysql默认端口 */
	private int mysqlPort = 3306;

	/**
	 * 
	 * 配置rmi端口
	 * 
	 * @return boolean true：设置成功；false：失败
	 */
	public boolean rmiPort() {
		// rmi配置文件
		String path = JecnServerUtil.getRmiConfigPath();
		String jecnConfigPath = JecnServerUtil.getJecnConfigPath();
		try {
			// 读配置文件
			Properties props = JecnFileUtil.readProperties(path);
			String rmiPort = props.getProperty("rmi.port");
			
			// 获取空闲端口
			// 给定端口是空闲返回true；重新获取的空闲端口返回false
			boolean rmiRet = proPort(rmiPort, TYPE_RMI);
			
			String tempRmiPort=String.valueOf(getRmiPort());
			if (!rmiRet) {// 获取的端口是重新生成的，需要写入配置文件
				log.info("rmi port is "+getRmiPort());
				props.setProperty("rmi.port", tempRmiPort);//getRmiPort()
				JecnFileUtil.writeProperties(props, path);
			}
			
			Properties jecnProps = JecnFileUtil.readProperties(jecnConfigPath);
			jecnProps.setProperty("serverPort3", tempRmiPort);
			JecnFileUtil.writeProperties(jecnProps, jecnConfigPath);
			
			/*****设置当前制定的端口到ConnectionPool中*****/
			ConnectionPool.setServerPort(tempRmiPort);
		
			return true;
		} catch (Exception e) {
			log.error("rmiPort is error", e);
			return false;
		}
	}

	/**
	 * 
	 * MYSql端口重新获取时，修改database.properties文件中jdbc.url的值中端口
	 * 
	 * @return boolean n true：设置成功；false：失败
	 */
	public boolean dataMysqlUrlPort() {
		// rmi配置文件
		String path = JecnServerUtil.getRmiConfigPath();
		log.info("=======设置dataMysqlUrlPort================");
		try {
			// 读配置文件
			Properties props = JecnFileUtil.readProperties(path);
			props.setProperty("jdbc.url", "jdbc:mysql://localhost:" + mysqlPort
					+ "/epros?useUnicode=true&amp;characterEncoding=UTF-8");
			log.info("1.1======dataMysqlUrlPort========="+mysqlPort);
			JecnFileUtil.writeProperties(props, path);
			log.info("1.2========配置mysql url地址：成功");
			return true;
		} catch (Exception e) {
			log.error("1.3========配置mysql url地址：失败", e);
			return false;
		}
	}

	/**
	 * 
	 * 设置tomcat端口
	 * 
	 * @return boolean true：设置成功；false：失败
	 */
	public boolean tomcatPort() {
		boolean ret = false;
		String path = JecnServerUtil.getTomcatConfigPath();
		log.info("1.======配置tomcat端口path:"+path);
		// 写入xml对象
		XMLWriter output = null;
		FileWriter fileWriter = null;
		try {
			File file = new File(path);
			if (!file.exists() || !file.isFile()) {
				return ret;
			}

			// 读取xml对象
			SAXReader saxReader = new SAXReader();
			// 读取xml数据
			Document doc = saxReader.read(file);

			// 具有ID属性的节点
			// <Server ID="tcJvm"
			Element jecnJvmPortEle = doc.elementByID(TYPE_TC_JVM);
			Element jecnPortEle = doc.elementByID(TYPE_TC_MAIN);
			Element jecnAjpPortEle = doc.elementByID(TYPE_TC_AJP);

			// 节点port属性
			Attribute jecnJvmPortAtrr = jecnJvmPortEle.attribute(PORT_NAME);
			Attribute jecnPorAtrr = jecnPortEle.attribute(PORT_NAME);
			Attribute jecnAjpPortAtrr = jecnAjpPortEle.attribute(PORT_NAME);

			// port属性对应值
			String jecnJvmPort = jecnJvmPortAtrr.getValue();
			String jecnPorPort = jecnPorAtrr.getValue();
			String jecnAjpPort = jecnAjpPortAtrr.getValue();
			log.info("1.1========jecnJvmPort========"+jecnJvmPort);
			log.info("1.2========jecnPorPort========"+jecnPorPort);
			log.info("1.3========jecnAjpPort========"+jecnAjpPort);
			
			// 给定端口是空闲返回true；重新获取的空闲端口返回false
			boolean jvmRet = proPort(jecnJvmPort, TYPE_TC_JVM);
			boolean mainRet = proPort(jecnPorPort, TYPE_TC_MAIN);
			boolean ajpRet = proPort(jecnAjpPort, TYPE_TC_AJP);

			if (!jvmRet) {// 重新获取端口
				log.info("1.4====端口占用重行写入的端口====tmcJvmPort========="+tmcJvmPort);
				jecnJvmPortAtrr.setValue(tmcJvmPort + "");
			}
			if (!mainRet) {// 重新获取端口
				log.info("1.5====端口占用重行写入的端口====tmcPort========="+tmcPort);
				jecnPorAtrr.setValue(tmcPort + "");
			}
			if (!ajpRet) {// 重新获取端口
				log.info("1.6====端口占用重行写入的端口====tmcAjpPort========="+tmcAjpPort);
				jecnAjpPortAtrr.setValue(tmcAjpPort + "");
			}

			if (!jvmRet || !mainRet || !ajpRet) {// 有重新给定端口，写入配置文件
				log.info("1.7====重新写入端口=======");
				fileWriter = new FileWriter(file);
				output = new XMLWriter(fileWriter);
				output.write(doc);
				output.flush();
			}
			ret = true;
			log.info("1.8===配置tomcat端口结束==========");
			return ret;
		} catch (Exception e) {
			log.error("", e);
			return false;
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					log.info("输出流关闭异常", e);
				}
			}
			if (fileWriter != null) {
				try {
					fileWriter.close();
				} catch (IOException e) {
					log.info("输出流关闭异常", e);
				}
			}
		}
	}

	/**
	 * 
	 * 设置mysql端口
	 * 
	 * @return boolean true：设置成功；false:设置失败
	 */
	public boolean mysqlPort() {
		BufferedReader buffReader = null;
		BufferedWriter buffWriter = null;
		try {
			String path = JecnServerUtil.getMysqlConfigPath();
			log.info("===配置mysqlPort====path:"+path);
			// 读取my.ini文件
			buffReader = new BufferedReader(new FileReader(path));
			StringBuffer strBuff = new StringBuffer();
			String allLineY = "";

			// 是否执行到[client]行
			boolean isClient = false;
			// 是否执行到[mysqld]行
			boolean isMysqld = false;
			while ((allLineY = buffReader.readLine()) != null) {
				String allLine = allLineY.trim();
				if ("[client]".equals(allLine)) {
					isClient = true;
				}
				if ("[mysqld]".equals(allLine)) {
					isMysqld = true;
				}
				if (isClient) {
					String[] strArray = allLine.split("=");
					if (strArray != null && strArray.length == 2) {
						String currKey = strTrim(strArray[0]);// 键
						String currValue = strTrim(strArray[1]);// 值
						log.info("1.1==读取mysql port==="+currValue);
						if (currKey != null && "port".equals(currKey)) {
							if (!isMysqld) {// client获取过一次[mysqld]就不获取了
								if (proPort(currValue, TYPE_MYSQL)) {// 配置文件端口是空闲
									return true;
								}
							}
							// client,mysqld两处端口都需要设置新获取的端口
							strBuff.append("port=");
							log.info("1.2==设置mysql port==="+mysqlPort);
							strBuff.append(mysqlPort);
							strBuff.append("\r\n");
							if (isMysqld) {// 说明执行到[mysqld]下端口行以下不用解析处理
								isClient = false;
							}
							continue;
						}
					}
				}
				strBuff.append(allLineY + "\r\n");

			}

			// 写my.ini文件
			buffReader.close();
			// 写出流
			buffWriter = new BufferedWriter(new FileWriter(path, false));
			buffWriter.write(strBuff.toString());
			buffWriter.flush();

			// MySql端口重新获取时，修改database.properties文件中jdbc.url的值中端口
			// jdbc.url=jdbc:mysql://localhost:3306/epros?useUnicode=true&amp;characterEncoding=UTF-8
			if (!dataMysqlUrlPort()) {
				log.error("1.3====配置mysql url=====异常");
				return false;
			}
			log.info("====结束配置mysqlPort=========");
			return true;
		} catch (Exception e) {
			log.error("", e);
			return false;
		} finally {
			try {
				if (buffReader != null) {
					buffReader.close();
				}
				if (buffWriter != null) {
					buffWriter.close();
				}
			} catch (IOException e) {
				log.error("关闭流异常");
			}

		}
	}

	/**
	 * 
	 * 获取去空后的字符串
	 * 
	 * @param str
	 *            String
	 * @return String
	 */
	private String strTrim(String str) {
		if (Tool.isNotBlank(str)) {
			return str.trim();
		} else {
			return null;
		}
	}

	/**
	 * 
	 * 获取空闲端口
	 * 
	 * @param port
	 *            String
	 * @return boolean 给定端口是空闲返回true；重新获取的空闲端口返回false
	 */
	private boolean proPort(String port, String type) {
		boolean ret = false;
		int retPort = 0;
		// 检查给定端口是否空闲,>0值：空闲，0：占用
		int currPort = checkPort(port);

		if (currPort > 0) {// 配置文件中端口可用，不用写入文件了
			retPort = currPort;
			ret = true;
		} else {
			retPort = getFreePort(getDefaultport(type));
		}
		portSet.add(retPort);//retPort
		setFreeport(type, retPort);

		return ret;
	}

	/**
	 * 
	 * 判断给定端口是否空闲，空闲返回此端口，不空闲返回0
	 * 
	 * @param portStr
	 *            String
	 * @return int 具体端口值或0
	 */
	private int checkPort(String portStr) {
		int port = 0;
		if (Tool.isNotBlank(portStr)) {
			try {
				int tmpPort = Integer.valueOf(portStr).intValue();
				if (Tool.checkPort(tmpPort)) {// 空闲
					port = tmpPort;
				}
			} catch (Exception e) {
				log.error("", e);
			}
		}
		return port;
	}

	/**
	 * 
	 * 获取空闲端口,并且不属于已经选中端口
	 * 
	 * @param port
	 *            int
	 * @return int 当前选中空闲端口
	 */
	private int getFreePort(int port) {
		if (Tool.checkPort(port) && !portSet.contains(port)) {// 空闲且不属于已经选中端口
			return port;
		} else {// 占用
			return getFreePort(port + 1);
		}
	}

	/**
	 * 
	 * 
	 * @param type
	 *            String 类型
	 * @param port
	 *            int 空闲端口
	 */
	private void setFreeport(String type, int port) {
		if (TYPE_TC_JVM.equals(type)) {
			this.tmcJvmPort = port;
		} else if (TYPE_TC_MAIN.equals(type)) {
			this.tmcPort = port;
		} else if (TYPE_TC_AJP.equals(type)) {
			this.tmcAjpPort = port;
		} else if (TYPE_RMI.equals(type)) {
			this.rmiPort = port;
		} else if (TYPE_MYSQL.equals(type)) {
			this.mysqlPort = port;
		}
	}

	/**
	 * 
	 * 获取默认端口
	 * 
	 * @param type
	 * @return
	 */
	private int getDefaultport(String type) {
		int retPort = 0;
		if (TYPE_TC_JVM.equals(type)) {
			retPort = 8005;
		} else if (TYPE_TC_MAIN.equals(type)) {
			retPort = 8080;
		} else if (TYPE_TC_AJP.equals(type)) {
			retPort = 8009;
		} else if (TYPE_RMI.equals(type)) {
			retPort = 2088;
		} else if (TYPE_MYSQL.equals(type)) {
			retPort = 3306;
		}
		return retPort;
	}

	public int getTmcJvmPort() {
		return tmcJvmPort;
	}

	public int getTmcPort() {
		return tmcPort;
	}

	public int getTmcAjpPort() {
		return tmcAjpPort;
	}

	public int getRmiPort() {
		return rmiPort;
	}

	public int getMysqlPort() {
		return mysqlPort;
	}
}
