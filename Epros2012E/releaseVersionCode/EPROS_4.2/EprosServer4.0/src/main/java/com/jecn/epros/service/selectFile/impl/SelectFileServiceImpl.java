package com.jecn.epros.service.selectFile.impl;

import com.github.pagehelper.PageHelper;
import com.jecn.epros.domain.EnumClass;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.file.FileBaseInfoBean;
import com.jecn.epros.domain.process.FlowStructure;
import com.jecn.epros.domain.process.TreeNode;
import com.jecn.epros.domain.rule.Rule;
import com.jecn.epros.mapper.FileMapper;
import com.jecn.epros.mapper.ProcessMapper;
import com.jecn.epros.mapper.RuleMapper;
import com.jecn.epros.mapper.SelectFileMapper;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.security.OrgScope;
import com.jecn.epros.security.SecurityValidatorData;
import com.jecn.epros.service.ServiceExpandTreeData;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.service.process.ProcessService;
import com.jecn.epros.service.selectFile.SelectFileService;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.HttpUtil;
import com.jecn.epros.util.Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class SelectFileServiceImpl implements SelectFileService {

    @Autowired
    private SelectFileMapper selectFileMapper;
    @Autowired
    private FileMapper fileMapper;

    @Autowired
    private RuleMapper ruleMapper;
    @Autowired
    private ProcessMapper processMapper;
    @Autowired
    private ProcessService processService;

    @Override
    public List<TreeNode> treeNodes(Map<String, Object> map) {
        String type = map.get("type").toString();
        List<TreeNode> nodes = null;
        TreeNodeUtils.TreeType treeType = null;
        switch (type) {
            // 流程及流程架构
            case "process":
                nodes = selectFileMapper.findFlowTree(map);
                treeType = TreeNodeUtils.TreeType.process;
                break;
            // 制度
            case "rule":
                nodes = selectFileMapper.findRuleTree(map);
                treeType = TreeNodeUtils.TreeType.rule;
                break;
            // 标准
            case "standard":
                nodes = selectFileMapper.findStandardTree(map);
                treeType = TreeNodeUtils.TreeType.standard;
                break;
            // 风险
            case "risk":
                nodes = selectFileMapper.findRiskTree(map);
                treeType = TreeNodeUtils.TreeType.risk;
                break;
            // 文件
            case "file":
                nodes = selectFileMapper.findFileTree(map);
                treeType = TreeNodeUtils.TreeType.file;
                break;
            // 部门及岗位
            case "organization":
                nodes = selectFileMapper.findPosTree(map);
                treeType = TreeNodeUtils.TreeType.position;
                break;
            // 人员
            case "people":
                nodes = selectFileMapper.findPersonPosTree(map);
                treeType = TreeNodeUtils.TreeType.people;
                break;
            default:
                break;
        }
        // 树节点权限过滤
        nodes = ServiceExpandTreeData.findExpandNodesByDataList(nodes, treeType);
        TreeNodeUtils.setNodeType(nodes, treeType);
        return nodes;
    }

    @Override
    public List<TreeNode> searchName(Map<String, Object> map) {
        String type = map.get("type").toString();
        String[] subTypes = type.split(";");
        List<TreeNode> nodes = new ArrayList<>();
        for (String subType : subTypes) {
            if (StringUtils.isBlank(subType)) {
                continue;
            }
            nodes.addAll(getTreeNodes(map, subType));
        }
        return nodes;
    }

    private List<TreeNode> getTreeNodes(Map<String, Object> map, String type) {
        PageHelper.startPage(Paging.DEFAULT.getPageNum(), Paging.DEFAULT.getPageSize(), "temp.file_name");
        switch (type) {
            case "processMap":
                return TreeNodeUtils.setNodeType(selectFileMapper.searchFlowMap(map), TreeNodeUtils.TreeType.process);
            case "process":
                return TreeNodeUtils.setNodeType(selectFileMapper.searchFlow(map), TreeNodeUtils.TreeType.process);
            case "rule":
                return TreeNodeUtils.setNodeType(selectFileMapper.searchRule(map), TreeNodeUtils.TreeType.rule);
            case "standard":
                return TreeNodeUtils.setNodeType(selectFileMapper.searchStandard(map), TreeNodeUtils.TreeType.standard);
            case "risk":
                return TreeNodeUtils.setNodeType(selectFileMapper.searchRisk(map), TreeNodeUtils.TreeType.risk);
            case "file":
                return TreeNodeUtils.setNodeType(selectFileMapper.searchFile(map), TreeNodeUtils.TreeType.file);
            case "organization":
                return TreeNodeUtils.setNodeType(selectFileMapper.searchOrg(map), TreeNodeUtils.TreeType.organization);
            case "position":
                return TreeNodeUtils.setNodeType(selectFileMapper.searchPos(map), TreeNodeUtils.TreeType.position);
            case "people":
                if (ConfigUtils.isIflytekLogin()) {
                    // 返回结果集
                    return IfytekCommon.getSearchResultByTrueName(selectFileMapper, map);
                } else {
                    return TreeNodeUtils.setNodeType(selectFileMapper.searchPeople(map), TreeNodeUtils.TreeType.people);
                }
            default:
                return null;
        }
    }


    @Override
    public TreeNode treeNode(Map<String, Object> map) {
        String type = map.get("type").toString();
        TreeNode node = null;
        TreeNodeUtils.TreeType treeType = null;
        switch (type) {
            case "fileContent":
            case "processFileContent":
            case "ruleFileContent":
                setFileByContentId(map);
                return treeNode(map);
            // 流程及架构
            case "process":
                node = selectFileMapper.findFlowTreeNode(map);
                // 判断流程文件下载权限
                treeType = TreeNodeUtils.TreeType.process;
                getFileDownLoadAuth(node, map, treeType);
                break;
            // 制度
            case "rule":
                node = selectFileMapper.findRuleTreeNode(map);
                treeType = TreeNodeUtils.TreeType.rule;
                getFileDownLoadAuth(node, map, treeType);
                break;
            // 标准
            case "standard":
                node = selectFileMapper.findStandardTreeNode(map);
                if (StringUtils.isBlank(node.getId())) {
                    return null;
                }
                treeType = TreeNodeUtils.TreeType.standard;
                break;
            // 风险
            case "risk":
                node = selectFileMapper.findRiskTreeNode(map);
                treeType = TreeNodeUtils.TreeType.risk;
                break;
            // 文件
            case "file":
                node = selectFileMapper.findFileTreeNode(map);
                treeType = TreeNodeUtils.TreeType.file;
                getFileDownLoadAuth(node, map, treeType);
                break;
            // 部门
            case "organization":
                node = selectFileMapper.findOrgTreeNode(map);
                treeType = TreeNodeUtils.TreeType.organization;
                break;
            // 岗位
            case "position":
                node = selectFileMapper.findPosTreeNode(map);
                treeType = TreeNodeUtils.TreeType.position;
                break;
            // 人员选择-部门
            case "people":
//                node = selectFileMapper.findPersonOrgTreeNode(map);
                node = selectFileMapper.findPersonNode(map);
                treeType = TreeNodeUtils.TreeType.people;
                break;
            default:
                break;
        }

        TreeNodeUtils.setNodeType(node, treeType);
        return node;
    }

    /**
     * 根据 内容版本ID获取 节点主键ID
     *
     * @param map 内容节点类型
     */
    private void setFileByContentId(Map<String, Object> map) {
        String type = map.get("type").toString();
        Long id = null;
        switch (type) {
            case "processFileContent":
                map.put("type", "process");
                // id = selectFileMapper.getFileIdByContentId(map);
                break;
            case "fileContent":
                map.put("type", "file");
                id = selectFileMapper.getFileIdByContentId(map);
                break;
            case "ruleFileContent":
                map.put("type", "rule");
                id = selectFileMapper.getFileIdByRuleId(map);
                break;
        }
        map.put("id", id);
    }

    private void getFileDownLoadAuth(TreeNode node, Map<String, Object> map, TreeNodeUtils.TreeType treeType) {
        if (node == null) {
            return;
        }
        if (node.getIsFileLocal() != null && node.getIsFileLocal() == 2) { //制度链接文件 取消下载按钮
            node.setDownloadAuth(false);
            node.setZipDownloadAuth(false);
        } else if (AuthenticatedUserUtil.isAdmin() || AuthenticatedUserUtil.isViewAdmin()) {
            node.setDownloadAuth(true);
            node.setZipDownloadAuth(true);
        } else if (ConfigUtils.isProcessFileDownloadAuth()) {// 启用流程文件下载权限设置
            int count = selectFileMapper.processFileAuthCount(map);
            node.setDownloadAuth(count > 0 ? true : false);
        } else {
            Long id = Long.valueOf(map.get("id").toString());
            boolean isDownLoad = false;
            switch (treeType) {
                case rule:
                    if (ConfigUtils.valueIsZeroThenTrue("isShowRuleDownLoad") || SecurityValidatorData.isDownLoadNode(id, EnumClass.ResourceNodeEnum.RULE)) {
                        isDownLoad = true;
                    } else {
                        isDownLoad = false;
                    }
                    break;
                case process:
                    if (ConfigUtils.valueIsZeroThenTrue("isShowProcessDownLoad")
                            || SecurityValidatorData.isDownLoadNode(id, EnumClass.ResourceNodeEnum.FLOW)) {
                        isDownLoad = true;
                    } else {
                        isDownLoad = false;
                    }
                    break;
                case file:
                    if (ConfigUtils.valueIsZeroThenTrue("isShowFileDownLoad") || SecurityValidatorData.isDownLoadNode(id, EnumClass.ResourceNodeEnum.FILE)) {
                        isDownLoad = true;
                    } else {
                        isDownLoad = false;
                    }
                    break;
                default:
                    break;
            }

            node.setDownloadAuth(isDownLoad);
            //ConfigUtils.isShowZipFileDownloadAuth()  是否隐藏 带附件下载按钮
            node.setZipDownloadAuth(isDownLoad);
        }
    }

    @Override
    public TreeNode treeNodesOrientate(Map<String, Object> map) {
        String type = map.get("type").toString();
        // 固定结构的树，在一张表中可以查出上下级结构
        List<String> fixed = Arrays.asList("process", "rule", "standard", "risk", "file");
        TreeNode rootNode = null;
        if (fixed.contains(type)) {
            rootNode = createFixedTypeRootNode(type, map);
        } else {
            rootNode = createOrgTypeRootNode(type, map);
        }
        return rootNode;
    }

    @Override
    public LinkResource getFileLink(Map<String, Object> map) {
        TreeNode treeNode = (TreeNode) map.get("treeNode");
        // 0 未发布 ， 1 已发布
        int pubState = (int) map.get("pubState");
        boolean isPub = pubState == 1;
        map.put("isPub", pubState == 1 ? "" : "_T");
        map.put("historyId", "0");
        LinkResource link = null;
        switch (treeNode.getType()) {
            case processFile:
                map.put("flowId", treeNode.getId());
                FlowStructure structureInfo = processMapper.findFlowStructureByMap(map);
                link = new LinkResource(treeNode.getId(), treeNode.getText());
                link.setType(LinkResource.ResourceType.PROCESS_FILE);
                link.setUrl(HttpUtil.getDownLoadFileHttp(structureInfo.getFileContentId(), isPub, "processFile"));
                break;
            case ruleFile:
                Rule rule = findRule(map);
                link = new LinkResource(rule.getFileId(), treeNode.getText());
                link.setType(rule.getIsFileLocal() == 1 ? LinkResource.ResourceType.RULE : LinkResource.ResourceType.FILE);
                break;
            case file:
                FileBaseInfoBean baseInfo = fileMapper.findBaseInfo(map);
                link = new LinkResource(baseInfo.getFileId(), treeNode.getText());
                link.setType(LinkResource.ResourceType.FILE);
                break;
            case process:
                map.put("flowId", treeNode.getId());
                structureInfo = processMapper.findFlowStructureByMap(map);
                link = new LinkResource(treeNode.getId(), treeNode.getText());
                link.setType(LinkResource.ResourceType.PROCESS);
                link.setUrl(HttpUtil.getViewProcessDocUrl(structureInfo.getFlowId(), isPub, false));
                break;
        }
        return link;
    }

    @Override
    public List<TreeNode> inventedTreeNodes(Map<String, Object> map) {
        String type = map.get("type").toString();
        List<TreeNode> nodes = null;
        TreeNodeUtils.TreeType treeType = null;
        switch (type) {
            // 流程及流程架构
            case "process":
                nodes = processInventedTreeNodes(map);
                treeType = TreeNodeUtils.TreeType.process;
                break;
            // 制度
            case "rule":
                treeType = TreeNodeUtils.TreeType.rule;
                break;
            // 标准
            case "standard":
                treeType = TreeNodeUtils.TreeType.standard;
                break;
            // 风险
            case "risk":
                treeType = TreeNodeUtils.TreeType.risk;
                break;
            // 文件
            case "file":
                treeType = TreeNodeUtils.TreeType.file;
                break;
            // 部门及岗位
            case "organization":
                treeType = TreeNodeUtils.TreeType.position;
                break;
            // 人员
            case "people":
                treeType = TreeNodeUtils.TreeType.people;
                break;
            default:
                break;
        }
        TreeNodeUtils.setNodeType(nodes, treeType);
        return nodes;
    }

    private List<TreeNode> processInventedTreeNodes(Map<String, Object> map) {
        List<TreeNode> result = new ArrayList<>();
        TreeNodeUtils.NodeType inventedType = (TreeNodeUtils.NodeType) map.get("inventedType");
        TreeNodeUtils.NodeType inventedClickType = (TreeNodeUtils.NodeType) map.get("inventedClickType");
        if (TreeNodeUtils.NodeType.organization != inventedType) {
            return result;
        }
        // 适用部门ID
        Long inventedId = (Long) map.get("inventedId");
        OrgScope orgScope = AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getOrgScope();
        Set<Long> ids = orgScope.getOrgToId().get(inventedId);
        Set<Long> pIds = orgScope.getOrgToPid().get(inventedId);
        if (Utils.isEmpty(ids) && Utils.isEmpty(pIds)) {
            return result;
        }
        List<TreeNode> nodes = selectFileMapper.findFlowTree(map);
        for (TreeNode node : nodes) {
            Long id = Long.valueOf(node.getId());
            if (ids.contains(id) || pIds.contains(id)) {
                result.add(node);
                node.setInventedId(String.valueOf(inventedId));
                node.setInventedType(inventedType);
                if (!pIds.contains(id)) {
                    node.setIsLeaf(0);
                }
            }
        }
        return result;
    }


    public Rule findRule(Map<String, Object> map) {
        boolean isPub = Common.isPub(map.get("isPub"));
        // 制度对象
        Rule rule = null;
        if (isPub) {
            rule = ruleMapper.getRule(Long.valueOf(map.get("id").toString()));
        } else {
            rule = ruleMapper.getRuleByTRuleId(Long.valueOf(map.get("id").toString()));
        }
        return rule;
    }

    private TreeNode createOrgTypeRootNode(String type, Map<String, Object> map) {
        TreeNode rootNode = null;
        String orientateType = map.get("orientateType").toString();
        //      organization,position,people
        // 选择organization和people的区别是 organization下的岗位不能展开 而people可以展开
        // 人员定位，岗位定位先不做
        if ("organization".equals(type)) {
            String pIds = "";
            // 定位的是部门
            if ("organization".equals(orientateType)) {
                pIds = getPidsWithoutSelf(map);
            } else if ("position".equals(orientateType)) {
                //　查找该岗位上级的部门
                Long orgId = selectFileMapper.getPosOrgId(Long.valueOf(map.get("id").toString()));
                Map<String, Object> params = new HashMap<>();
                params.put("type", map.get("type"));
                params.put("id", orgId);
                String tPath = selectFileMapper.getNodePath(params);
                pIds = concatIdsAndAddRootId(tPath, true);
            }
            map.put("pIds", pIds);
            List<TreeNode> nodes = selectFileMapper.findPosTree(map);
            TreeNodeUtils.TreeType treeType = TreeNodeUtils.TreeType.position;
            TreeNodeUtils.setNodeType(nodes, treeType);
            String id = String.valueOf((Long) map.get("id"));
            rootNode = createOrgRootNode(nodes, treeType, id);
        }
        return rootNode;
    }


    private TreeNode createFixedTypeRootNode(String type, Map<String, Object> map) {
        List<TreeNode> nodes = null;
        String pIds = getPidsWithoutSelf(map);
        map.put("pIds", pIds);
        TreeNodeUtils.TreeType treeType = null;
        switch (type) {
            // 流程及流程架构
            case "process":
                nodes = selectFileMapper.findFlowTree(map);
                treeType = TreeNodeUtils.TreeType.process;
                break;
            // 制度
            case "rule":
                nodes = selectFileMapper.findRuleTree(map);
                treeType = TreeNodeUtils.TreeType.rule;
                break;
            // 标准
            case "standard":
                nodes = selectFileMapper.findStandardTree(map);
                treeType = TreeNodeUtils.TreeType.standard;
                break;
            // 风险
            case "risk":
                nodes = selectFileMapper.findRiskTree(map);
                treeType = TreeNodeUtils.TreeType.risk;
                break;
            // 文件
            case "file":
                nodes = selectFileMapper.findFileTree(map);
                treeType = TreeNodeUtils.TreeType.file;
                break;
        }
        // 树节点权限过滤
        nodes = ServiceExpandTreeData.findExpandNodesByDataList(nodes, treeType);

        TreeNodeUtils.setNodeType(nodes, treeType);
        String id = String.valueOf((Long) map.get("id"));
        TreeNode rootNode = createRootNode(nodes, treeType, id);
        // 添加虚拟架构
        addInvented(rootNode, type);
        return rootNode;
    }

    private void addInvented(TreeNode rootNode, String type) {
        List<TreeNode> nodes = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        switch (type) {
            // 流程及流程架构
            case "process":
                nodes = processService.findInventedFlows(map);
                break;
            // 制度
            case "rule":

                break;
            // 标准
            case "standard":

                break;
            // 风险
            case "risk":

                break;
            // 文件
            case "file":

                break;
        }
        rootNode.getChildren().addAll(nodes);
    }

    /**
     * 获得sql in结构的id的集合不包含本身例如：t_path 为1-2-3- pIds为(0,1,2)
     *
     * @param map
     * @return
     */
    private String getPidsWithoutSelf(Map<String, Object> map) {
        String tPath = selectFileMapper.getNodePath(map);
        return concatIdsAndAddRootId(tPath, false);
    }

    private TreeNode createRootNode(List<TreeNode> nodes, TreeNodeUtils.TreeType nodeType, String select) {
        Map<String, TreeNode> nodeMap = new HashMap<>();
        TreeNode rootNode = createRootNode(nodeType);
        nodeMap.put("0", rootNode);
        for (TreeNode node : nodes) {
            if (select.equals(node.getId())) {
                node.setSelect(true);
            }
            nodeMap.put(node.getId(), node);
        }
        // 填充子节点
        for (TreeNode node : nodes) {
            nodeMap.get(node.getpId()).getChildren().add(node);
        }

        rootNode.setIsLeaf(rootNode.getChildren().size());
        return rootNode;
    }

    private TreeNode createOrgRootNode(List<TreeNode> nodes, TreeNodeUtils.TreeType nodeType, String select) {
        Map<String, TreeNode> orgMap = new HashMap<>();
        TreeNode rootNode = createRootNode(nodeType);
        orgMap.put("0", rootNode);
        for (TreeNode node : nodes) {
            if (select.equals(node.getId())) {
                node.setSelect(true);
            }
            if (node.getIsDir() == 0) {
                orgMap.put(node.getId(), node);
            }
        }
        // 填充子节点
        for (TreeNode node : nodes) {
            orgMap.get(node.getpId()).getChildren().add(node);
        }
        rootNode.setIsLeaf(rootNode.getChildren().size());
        return rootNode;
    }

    private TreeNode createRootNode(TreeNodeUtils.TreeType nodeType) {
        TreeNode rootNode = new TreeNode();
        rootNode.setId("0");
        rootNode.setIsDir(0);
        TreeNodeUtils.setNodeType(rootNode, nodeType);
        return rootNode;
    }


    /**
     * 通过一个tPath 1-2-3 转换为sql in 类型 (0,1,2,3)或者(0,1,2)
     *
     * @param tPath
     * @return
     */
    private String concatIdsAndAddRootId(String tPath, boolean containsSelf) {
        String[] idArr = tPath.split("-");
        Set<String> pIdSet = new HashSet<>();
        pIdSet.add("0");
        int len = containsSelf ? idArr.length : idArr.length - 1;
        for (int i = 0; i < len; i++) {
            pIdSet.add(idArr[i]);
        }
        String ids = pIdSet.stream().collect(Collectors.joining(","));
        String inIds = "(" + ids + ")";
        return inIds;
    }
}
