package com.jecn.epros.server.service.dataImport.match.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;
/**
 * 人员同步属性验证
 * 
 * @author Administrator
 * 
 */
public class MatchTool {
	private static Logger log = Logger.getLogger(MatchTool.class);
	/** 人员导入存储数据库信息的xml路径 */
	private static String IMPORT_XML_PATH = "matchConfig/UserMatchConfig.xml";

	/**
	 * 
	 * 拼装前台界面提示页面
	 * 
	 * @param info
	 * @param href
	 */
	public static void ResultHtml(String info, String href) {
		if (JecnCommon.isNullOrEmtryTrim(info)
				|| JecnCommon.isNullOrEmtryTrim(href)) {
			return;
		}
		// 获取打印输出对象
		PrintWriter out = getPrintWriter();
		try {
			
		if (isNullObj(out)) {
			return;
		}

		out.println("<html>");
		out.println("<head>");
		out
				.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
		out.println("<title></title>");
		out.println("</head>");
		out.println("<script language='javascript'>");
		out.println("alert('" + info + "');");
		if (href != null) {
			out.println("window.location.href='" + href + "';");
		}
		out.println("window.close();");
		out.println("</script>");
		out.println("</html>");
		out.flush();
		} catch (Exception e) {
			log.error("",e);
		} finally{
			if(out!=null){
				try {
					out.close();
				} catch (Exception e) {
					log.error("关闭流异常",e);
				}
			}
		}
		
	}

	/**
	 * 
	 * 获取打印输出对象
	 * 
	 * @return
	 */
	private static PrintWriter getPrintWriter() {
		HttpServletResponse response = ServletActionContext.getResponse();
		if (response == null) {
			return null;
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			log.error("", e);
		}
		return out;
	}

	/**
	 * 
	 * 给定参数是否为null
	 * 
	 * @param obj
	 *            Object
	 * @return
	 */
	public static boolean isNullObj(Object obj) {
		return (obj == null) ? true : false;
	}

	/**
	 * 
	 * 去空
	 * 
	 * @param str
	 * @return
	 */
	public static String emtryToNull(String str) {
		if (JecnCommon.isNullOrEmtryTrim(str)) {
			return null;
		} else {
			return str.trim();
		}
	}

	/**
	 * 
	 * 获取配置文件的路径(/sync/config/UserSfConfig.xml)
	 * 
	 * @return String
	 */
	public static String getUserConfigPath() {
		String path = getSyncPath();
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			path = path + IMPORT_XML_PATH;
		}
		return path;
	}

	/**
	 * 
	 * 获取sync目录路径(../WebRoot/Release/com/jecn/dataimport/sync/)
	 * 
	 * @return
	 */
	public static String getSyncPath() {
		URL url = MatchTool.class.getResource("");
		String path = "";
		try {
			path = url.toURI().getPath();
		} catch (URISyntaxException e) {
			// 这儿一般是不进入此分支的
			LogFactory.getLog(MatchTool.class).error("Tool类getSyncPath方法：", e);
			path = url.getPath().replaceAll("%20", " ");
		}
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			path = path.substring(0, path.length() - "util/".length());
		}

		return path;
	}

	/**
	 * 
	 * 邮箱类型是否是0、1
	 * 
	 * @return
	 */
	public static boolean isEmailTypeStr(String emailType) {
		return ("0".equals(emailType) || "1".equals(emailType)) ? true : false;
	}

	/**
	 * 
	 * 
	 * 去空
	 * 
	 * @param str
	 * @return
	 */
	public static String nullToEmtry(String str) {
		if (JecnCommon.isNullOrEmtryTrim(str)) {
			return "";
		} else {
			return str.trim();
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param obj
	 * @return String
	 */
	public static String nullToEmtryByObj(Object obj) {
		if (isNullObj(obj)) {
			return "";
		} else {
			return obj.toString().trim();
		}
	}

	/**
	 * 
	 * 名称不能大于61个汉字或122字母
	 * 
	 * @param name
	 * @return 超过返回true，没有超过返回false
	 */
	public static boolean checkNameMaxLength(String name) {
		return (name != null && getTextLength(name.trim()) > 122) ? true
				: false;
	}

	/**
	 * 
	 * 获取给定字符串的字节长度，汉字为2个字节，字母数字等为一个字节
	 * 
	 * 字符串为null或""时返回0
	 * 
	 * @param text
	 *            字符串
	 * @return int 返回字符串的字节长度
	 */
	public static int getTextLength(String text) {
		try {
			return (text != null) ? text.getBytes("GBK").length : 0;
		} catch (UnsupportedEncodingException e) {
			return (text != null) ? text.getBytes().length : 0;
		}
	}

	/**
	 * 
	 * 验证是否含有/*?!,%;^#$&`~|:[]{}"等字符
	 * 
	 * 
	 * @param name
	 *            名称
	 * @return boolean
	 *         不是由部门名称由中文、英文、数字及“_”、“-”组成返回true；是由部门名称由中文、英文、数字及“_”、“-”组成返回false
	 */
	public static boolean checkNameFileSpecialChar(String name) {
		return (name != null && name.trim().replaceAll(MatchConstant.IN_STRING,
				"").length() != 0) ? true : false;
	}

	/**
	 * 
	 * 获取人员错误数据Excel导出模板路径
	 * 
	 * @return String
	 */
	public static String getOutputErrorExcelModelPath() {
		// ../jecn/dataimport/sync/doc/model
		String path = getExcelModelDict();
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			path = path + MatchConstant.OUTPUT_EXCEL_ERROR_MOULD_NAME;
		}
		return path;
	}

	/**
	 * 
	 * 获取人员错误数据导出Excel文件路径
	 * 
	 * @return String
	 */
	public static String getOutPutExcelErrorDataPath() {
		// ../jecn/dataimport/sync/
		String path = getExcelDataDict();
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			path = path + MatchConstant.OUTPUT_EXCEL_ERROR_DATA_NAME;
		}
		return path;
	}

	/**
	 * 
	 * 获取Excel模板所在目录路径(../WebRoot/Release/com/jecn/dataimport/sync/doc/model/)
	 * 
	 * @return
	 */
	public static String getExcelModelDict() {
		// ../jecn/dataimport/sync/
		String path = getSyncPath();
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			path = path + "doc/";
		}

		return path;
	}

	/**
	 * 
	 * 获取Excel数据所在目录路径(../WebRoot/Release/com/jecn/dataimport/sync/doc/data/)
	 * 
	 * @return
	 */
	public static String getExcelDataDict() {
		// ../jecn/dataimport/sync/
		String path = getSyncPath();
		if (!JecnCommon.isNullOrEmtryTrim(path)) {
			path = path + "doc/";
		}

		return path;
	}

	/**
	 * 人员同步数据校验异常类型
	 * 
	 * @author Administrator
	 * @date： 日期：May 30, 2013 时间：4:26:43 PM
	 */
	public enum DataErrorType {
		none, // 无验证错误
		rowsType, // 行间校验出错异常
		columnsType,
		// 行内校验出错异常
		serverCount
	}

}
