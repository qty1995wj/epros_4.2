package com.jecn.epros.server.service.dataImport.file;

import com.jecn.epros.server.common.IBaseService;

public interface IJecnSyncFileService extends IBaseService<String, String> {

	void syncFile(Long projectId) throws Exception;

}
