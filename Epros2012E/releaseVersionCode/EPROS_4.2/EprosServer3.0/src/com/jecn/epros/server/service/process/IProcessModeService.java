package com.jecn.epros.server.service.process;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;

/**
 * @author yxw 2012-6-25
 * @description：
 */
@Transactional
public interface IProcessModeService extends
		IBaseService<JecnFlowStructureT, Long> {
	/**
	 * 获得所有的流程图模板(流程图，流程地图)
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllFlowMapModels() throws Exception;

	/**
	 * 根据父ID 查询所有子节点
	 * 
	 * @param flowMapModelId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getFlowModels(Long flowMapModelId)
			throws Exception;

	/**
	 * 重命名
	 * 
	 * @param newName
	 * @param id
	 * @param updatePersonId
	 * @throws Exception
	 */

	public void reName(String newName, Long id, Long updatePersonId)
			throws Exception;

	/**
	 * 搜索
	 * 
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchByName(String name,int type) throws Exception;

	/**
	 * 节点排序
	 * 
	 * @param list
	 * @param pId
	 * @param projectId
	 * @throws Exception
	 */
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId)
			throws Exception;

	/**
	 * 节点移动
	 * 
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodes(List<Long> listIds, Long pId) throws Exception;

	/**
	 * 删除
	 * 
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteFlowModel(List<Long> listIds) throws Exception;

	/**
	 * 新建流程模板管理
	 * 
	 * @param jecnFlowStructureMode
	 * @param isXorY
	 *            0横向图，1 纵向图
	 * @return Long 流程主键ID
	 * @throws Exception
	 */
	public Long saveFlowModel(JecnFlowStructureT jecnFlowStructureT, int isXorY)
			throws Exception;

}
