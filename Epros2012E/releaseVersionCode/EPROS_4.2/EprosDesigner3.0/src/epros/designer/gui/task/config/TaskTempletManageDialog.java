package epros.designer.gui.task.config;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.task.TaskTemplet;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;

/***
 * 模板管理
 * 
 * @author 2012-05-23
 * 
 */
public class TaskTempletManageDialog extends JecnDialog {

	public static final Logger log = Logger.getLogger(TaskTempletManageDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 模板信息显示面板 */
	private JPanel projecPanel = null;

	/** 按钮面板1 */
	private JPanel buttonPanel1 = null;

	/** 按钮面板2 */
	private JPanel buttonPanel2 = null;

	/** 模板滚动面板 */
	private JScrollPane resultScrollPane = null;

	/** 模板Table */
	public JecnTable templetTable = null;

	/** 新建模板 */
	private JButton addProBut = null;

	/** 重命名 */
	private JButton realNameBut = null;

	/** 删除 */
	private JButton deleteBut = null;

	/** 取消 */
	private JButton cancelBut = null;

	/** 设置大小 */
	Dimension dimension = null;
	private List<TaskTemplet> taskTempletList = null;
	protected JLabel projectVerLab = new JLabel();

	private int taskType;
	private TaskConfigDialog parentDialog;
	private boolean isChange = false;

	public TaskTempletManageDialog(TaskConfigDialog parentDialog, int taskType) {
		this.parentDialog = parentDialog;
		this.taskType = taskType;
		initCompotents();
		initLayout();
	}

	/***
	 * 初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JPanel();

		// 模板信息显示面板
		projecPanel = new JPanel();

		// 按钮面板
		buttonPanel1 = new JPanel();

		// 按钮面板
		buttonPanel2 = new JPanel();

		// 滚动面板
		resultScrollPane = new JScrollPane();

		// 模板Table
		templetTable = new TaskTempletTable();

		// 新建模板
		addProBut = new JButton(JecnProperties.getValue("addBtn"));

		// 重命名
		realNameBut = new JButton(JecnProperties.getValue("rename"));

		// 删除
		deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));

		// 取消
		cancelBut = new JButton(JecnProperties.getValue("closeBtn"));

		// 设置大小
		dimension = new Dimension();

		// 设置窗体大小可改变
		this.setResizable(true);
		this.setSize(500, 430);
		this.setLocationRelativeTo(null);
		this.setTitle(JecnProperties.getValue("editTaskModel"));
		this.setModal(true);

		// 设置主面板默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		projecPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置button面板默认背景色
		buttonPanel1.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置button面板默认背景色
		buttonPanel2.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 滚动面板
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		projectVerLab.setForeground(Color.red);

		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}

		});
		// 新建模板
		addProBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addButPerformed();
				isChange = true;
			}
		});
		// 删除
		deleteBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
				isChange = true;
			}
		});
		// 重命名
		realNameBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reNameButPerformed();
				isChange = true;
			}
		});
		if (!JecnDesignerCommon.isAdmin()) {
			addProBut.setEnabled(false);
			deleteBut.setEnabled(false);
			realNameBut.setEnabled(false);
		}
	}

	/***
	 * 布局
	 */
	private void initLayout() {

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);

		// 模板信息面板布局
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(projecPanel, c);

		projecPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), ""));
		projecPanel.setLayout(new GridBagLayout());
		// 模板集合
		resultScrollPane.setViewportView(templetTable);
		// projecPanel.add(resultScrollPane);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		projecPanel.add(resultScrollPane, c);

		// 按钮面板1布局
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel1, c);
		buttonPanel1.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel1.add(addProBut);
		buttonPanel1.add(realNameBut);

		buttonPanel1.add(deleteBut);
		// 按钮面板2布局
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(buttonPanel2, c);
		buttonPanel2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel2.add(projectVerLab);
		buttonPanel2.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 取消按钮事件
	 */
	private void cancelButPerformed() {
		if (isChange) {
			parentDialog.initData();
		}
		this.dispose();
	}

	/***
	 * 新建模板
	 */
	private void addButPerformed() {
		AddTaskTempletDialog addProjectNameDialog = new AddTaskTempletDialog(taskType);
		addProjectNameDialog.setVisible(true);
		if (addProjectNameDialog.isOk()) {
			TaskTemplet taskTemplet = addProjectNameDialog.getTaskTemplet();
			taskTempletList.add(taskTemplet);
			Vector<String> v = new Vector<String>();
			v.add(taskTemplet.getName());
			templetTable.addRow(v);
			int contRow = templetTable.getRowCount();
			templetTable.getModel().setValueAt(taskTemplet.getName(), contRow - 1, 1);
			templetTable.getModel().setValueAt(taskTemplet.geteName(), contRow - 1, 2);
		}
	}

	/***
	 * 删除
	 */
	private void deleteButPerformed() {
		projectVerLab.setText("");
		int[] selectRows = templetTable.getSelectedRows();
		if (selectRows.length == 0) {
			projectVerLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		// 模板ID
		// Long projectId = Long.valueOf(projectList.get(selectRows[0])
		// .getProjectId());
		// 判断模板是否已经打开,打开则提示不能删除
		// for (int i = selectRows.length - 1; i >= 0; i--) {
		// if (JecnConstants.projectId != null) {
		// if (taskTempletList.get(selectRows[i]).getProjectId().longValue() ==
		// JecnConstants.projectId
		// .longValue()) {
		// projectVerLab.setText(JecnProperties
		// .getValue("notdelOpenProj"));
		// return;
		// }
		// }
		// }
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		// 保存要删除的id，传到后台删除数据
		List<String> listIds = new ArrayList<String>();
		for (int i = selectRows.length - 1; i >= 0; i--) {
			listIds.add(taskTempletList.get(selectRows[i]).getId());

		}
		// 更改模板删除状态
		if (!deleteData(listIds)) {
			// 提示删除失败
			projectVerLab.setText(JecnProperties.getValue("delfailure"));
			return;
		}
		// 删除table选中的数据
		// projecTable.removeRows(selectRows);

		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) templetTable.getModel()).removeRow(selectRows[i]);
		}
		// 删除模板结合projectList中的数据
		List<TaskTemplet> oldProjectList = taskTempletList;
		taskTempletList = new ArrayList<TaskTemplet>();
		for (TaskTemplet jecnProject : oldProjectList) {
			if (!listIds.contains(jecnProject.getId())) {
				taskTempletList.add(jecnProject);
			}
		}
	}

	private boolean deleteData(List<String> listIds) {
		try {
			ConnectionPool.getTaskRecordAction().deleteTaskTemplet(listIds);
			return true;
		} catch (Exception e) {
			log.error("TaskTempletManageDialog deleteData is error", e);
		}
		return false;
	}

	/***
	 * 重命名
	 */
	private void reNameButPerformed() {
		projectVerLab.setText("");
		int[] selectRows = templetTable.getSelectedRows();
		if (selectRows.length != 1) {
			// 请选择一行！
			projectVerLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		EditTaskTempletNameDialog editProjectNameDialog = new EditTaskTempletNameDialog(taskTempletList
				.get(selectRows[0]));
		editProjectNameDialog.setVisible(true);
		if (editProjectNameDialog.isOk()) {
			// 刷新名称
			this.templetTable.getModel().setValueAt(editProjectNameDialog.getName(), selectRows[0], 1);
			this.templetTable.getModel().setValueAt(editProjectNameDialog.geteName(), selectRows[0], 2);
		}
	}

	class TaskTempletTable extends JecnTable {

		public TaskTempletTable() {
			DefaultTableCellRenderer render = new DefaultTableCellRenderer();
			render.setHorizontalAlignment(SwingConstants.CENTER);
			TableColumn tableColumn = columnModel.getColumn(1);
			tableColumn.setCellRenderer(render);
			// tableColumn.setMinWidth(50);
			// tableColumn.setMaxWidth(50);
		}

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTitleTable(), getContentTable());
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0 };
		}

	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得table的标题
	 * @return
	 */
	private Vector<String> getTitleTable() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		/*
		 * 任务模板
		 */
		title.add(JecnProperties.getValue("chineseName"));
		title.add(JecnProperties.getValue("enName"));
		return title;
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得table的内容
	 * @return
	 */
	private Vector<Vector<String>> getContentTable() {
		Vector<Vector<String>> vectords = new Vector<Vector<String>>();

		try {
			taskTempletList = ConnectionPool.getTaskRecordAction().listTaskTempletsByType(taskType);
			for (TaskTemplet taskTemplet : taskTempletList) {
				Vector<String> data = new Vector<String>();
				data.add(taskTemplet.getId());
				data.add(taskTemplet.getName());
				data.add(taskTemplet.geteName());
				vectords.add(data);
			}
			return vectords;
		} catch (Exception e) {
			log.error("TaskTempletManageDialog getContentTable is error", e);
		}
		return null;
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	public void setMainPanel(JPanel mainPanel) {
		this.mainPanel = mainPanel;
	}

	public JPanel getProjecPanel() {
		return projecPanel;
	}

	public void setProjecPanel(JPanel projecPanel) {
		this.projecPanel = projecPanel;
	}

	public JPanel getButtonPanel1() {
		return buttonPanel1;
	}

	public void setButtonPanel1(JPanel buttonPanel1) {
		this.buttonPanel1 = buttonPanel1;
	}

	public JPanel getButtonPanel2() {
		return buttonPanel2;
	}

	public void setButtonPanel2(JPanel buttonPanel2) {
		this.buttonPanel2 = buttonPanel2;
	}

	public JScrollPane getResultScrollPane() {
		return resultScrollPane;
	}

	public void setResultScrollPane(JScrollPane resultScrollPane) {
		this.resultScrollPane = resultScrollPane;
	}

	public JecnTable getProjecTable() {
		return templetTable;
	}

	public void setProjecTable(JecnTable projecTable) {
		this.templetTable = projecTable;
	}

	public JButton getAddProBut() {
		return addProBut;
	}

	public void setAddProBut(JButton addProBut) {
		this.addProBut = addProBut;
	}

	public JButton getRealNameBut() {
		return realNameBut;
	}

	public void setRealNameBut(JButton realNameBut) {
		this.realNameBut = realNameBut;
	}

	public JButton getDeleteBut() {
		return deleteBut;
	}

	public void setDeleteBut(JButton deleteBut) {
		this.deleteBut = deleteBut;
	}

	public JButton getCancelBut() {
		return cancelBut;
	}

	public void setCancelBut(JButton cancelBut) {
		this.cancelBut = cancelBut;
	}

	public Dimension getDimension() {
		return dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	public List<TaskTemplet> getTaskTempletList() {
		return taskTempletList;
	}

	public void setTaskTempletList(List<TaskTemplet> taskTempletList) {
		this.taskTempletList = taskTempletList;
	}

	public JLabel getProjectVerLab() {
		return projectVerLab;
	}

	public void setProjectVerLab(JLabel projectVerLab) {
		this.projectVerLab = projectVerLab;
	}

	public int getTaskType() {
		return taskType;
	}

	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}

	public static Logger getLog() {
		return log;
	}

}
