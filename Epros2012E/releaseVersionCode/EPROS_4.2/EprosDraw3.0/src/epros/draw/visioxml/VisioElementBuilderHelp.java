package epros.draw.visioxml;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import visioxml.bean.BaseProperty;
import visioxml.bean.CharProperty;
import visioxml.bean.FillProperty;
import visioxml.bean.LineProperty;
import visioxml.bean.ParaProperty;
import visioxml.bean.TextBlockProperty;
import visioxml.bean.XForm1DProperty;
import visioxml.bean.XFormProperty;
import visioxml.constant.Constants.Align;
import visioxml.constant.Constants.FillPattern;
import visioxml.constant.Constants.FontStyle;
import visioxml.constant.Constants.LineArrowType;
import visioxml.constant.Constants.LinePattern;
import visioxml.constant.Constants.TextDirection;
import visioxml.constant.Constants.Valign;
import visioxml.model.Master;
import visioxml.model.VisioModel;
import visioxml.shape.BaseShape;
import visioxml.shape.LevelShape;
import visioxml.shape.TwoPartFunctionShape;
import visioxml.util.FileUtil;
import visioxml.util.Tools;
import visioxml.util.Unit;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.JecnXYData;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnDiviLineDataCloneable;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.visioxml.input.VisioInputTools;

class VisioElementBuilderHelp {
	// 字体固定缩小3pt
	private static final int fontOffset = 3;
	private static final Log log = LogFactory.getLog(VisioElementBuilderHelp.class);
	// 单向箭头
	private static final String CONSTANT_DEGREE_SINGLE = "JECN_DEGREE_SINGLE";
	// 双向箭头
	private static final String CONSTANT_DEGREE_DOUBLE = "JECN_DEGREE_DOUBLE";
	// 导入三角形 特殊处理一下(向上偏移一点)
	private static final List<MapElemType> mapElementType = Arrays.asList(MapElemType.Triangle,
			MapElemType.KeyPointFigure, MapElemType.EndFigure);
	// 面板高度
	private int panelHeight;
	// 面板宽度
	private int panelWidth;
	// visio图形对应关系
	private Map<String, String> configMap;

	public VisioElementBuilderHelp(int panelHeight, int panelWidth) {
		this.panelHeight = panelHeight;
		this.panelWidth = panelWidth;
		this.configMap = FileUtil.getProcessOutPutVisioModeConfig();
	}

	/**
	 * 按照Epros数据初始化Visio图形
	 * 
	 * @param elementData
	 * @return
	 */
	protected BaseShape initVisioShape(JecnAbstractFlowElementData elementData) {
		Master master = getVisioMaster(elementData.getMapElemType());
		if (master == null) {
			log.error("未找到对应的visio图形标识,Epros中图形标识(MapElemType)：" + elementData.getMapElemType());
			return null;
		}
		// 初始化通用的属性样式
		BaseProperty baseProperty = initCommentProperty(elementData);
		// 初始化特殊的样式
		initSpeicalProperty(baseProperty, elementData, master);
		// 图片插入框初始化图片数据
		if (MapElemType.IconFigure == elementData.getMapElemType()) {
			// ForeignDataProperty foreignData = new ForeignDataProperty();
			// baseProperty.setForeignData(foreignData);
			log.error("图标插入框暂时过滤");
			return null;
		}
		BaseShape baseShape = null;
		if (elementData instanceof JecnActiveData) {
			JecnActiveData activeData = (JecnActiveData) elementData;
			// 设置活动编号
			baseShape = new TwoPartFunctionShape(baseProperty, master).setNumber(activeData.getActivityNum());
		} else {
			if (MapElemType.FlowLevelFigureOne == elementData.getMapElemType()
					|| MapElemType.FlowLevelFigureTwo == elementData.getMapElemType()
					|| MapElemType.FlowLevelFigureThree == elementData.getMapElemType()
					|| MapElemType.FlowLevelFigureFour == elementData.getMapElemType()) {
				baseShape = new LevelShape(baseProperty, master);
			} else {
				baseShape = new BaseShape(baseProperty, master);
			}
		}
		baseShape.setzOrderIndex(elementData.getZOrderIndex());
		baseShape.setText(elementData.getFigureText());
		return baseShape;
	}

	/**
	 * 初始化特殊的样式根据master不同设置不同的属性
	 * 
	 * @param baseProperty
	 * @param eprosElement
	 * @param master
	 */
	private void initSpeicalProperty(BaseProperty baseProperty, JecnAbstractFlowElementData eprosElement, Master master) {
		if (master == null) {
			return;
		}
		if (master.isXForm1D()) {
			setXForm1DProperty(baseProperty, eprosElement, master);
		} else {
			setXFormProperty(baseProperty, (JecnFigureData) eprosElement);
		}
	}

	/**
	 * 设置XFormProperty
	 * 
	 * @param baseProperty
	 * @param figure
	 * @param master
	 */
	private void setXFormProperty(BaseProperty baseProperty, JecnFigureData figure) {
		double scale = VisioInputTools.getWorkflow().getWorkflowScale();
		JecnFigureDataCloneable figureCloneable = figure.getFigureDataCloneable().getZoomFigureDataProcess(scale);
		// 图形的X轴
		double pinX = figureCloneable.getDoubleX();
		// 图形的Y轴
		double pinY = figureCloneable.getDoubleY();
		// 图形的宽度
		int width = figureCloneable.getWidth();
		// 图型的高度
		int height = figureCloneable.getHeight();
		// 角度
		int angle = VisioInputTools.DoubleToInt(figure.getCurrentAngle());
		if (angle == 90 || angle == 270) {
			int temp = width;
			width = height;
			height = temp;
			// 返入返出
			if (MapElemType.ReverseArrowhead == figure.getMapElemType()
					|| MapElemType.RightArrowhead == figure.getMapElemType()) {
				pinX = pinX + width / 2;
				pinY = pinY - height / 4;
			}// 通用图形
			else {
				pinX = pinX + width / 15;
				pinY = pinY + height / 10;
			}

		}
		double visioPinX = VisioOutputTools.changeX(pinX, width);
		double visioPinY = VisioOutputTools.changeY(pinY, panelHeight, height);
		double visioWidth = Unit.pxToMM(width);
		double visioHeight = Unit.pxToMM(height);
		double visioAngule = Tools.eprosAngleToVisioAngle(angle);
		// 导出三角形向下偏移2毫米
		if (mapElementType.contains(figure.getMapElemType())) {
			visioPinY = visioPinY - 2;
		}
		XFormProperty xForm = new XFormProperty(visioPinX, visioPinY, visioWidth, visioHeight, visioAngule);
		baseProperty.setXForm(xForm);
	}

	/**
	 * 设置setXForm1DProperty
	 * 
	 * @param baseProperty
	 * @param eprosElement
	 * @param master
	 */
	private void setXForm1DProperty(BaseProperty baseProperty, JecnAbstractFlowElementData eprosElement, Master master) {
		XForm1DProperty xForm1D = null;
		// 动态连接线
		if (eprosElement instanceof JecnManhattanLineData) {
			JecnManhattanLineData data = (JecnManhattanLineData) eprosElement;
			JecnXYData start = data.getOriginalStartFigurePoint();
			double startX = start.getX();
			double startY = start.getY();
			JecnXYData end = data.getOriginalEndFigurePoint();
			double endX = end.getX();
			double endY = end.getY();
			xForm1D = new XForm1DProperty(VisioOutputTools.change1DX(startX), VisioOutputTools.change1DY(startY,
					panelHeight), VisioOutputTools.change1DX(endX), VisioOutputTools.change1DY(endY, panelHeight));
			baseProperty.getLine().setEndArrowType(LineArrowType._06);
			baseProperty.setXForm1D(xForm1D);
		}
		// 横竖分割线
		else if (eprosElement instanceof JecnVHLineData) {
			JecnVHLineData vHLineData = (JecnVHLineData) eprosElement;
			// 水平
			if (LineDirectionEnum.horizontal == vHLineData.getLineDirectionEnum()) {
				// 结束点Y的值
				double poLongy = vHLineData.getVhY();
				double x2 = VisioOutputTools.change1DX(panelWidth);
				double y = VisioOutputTools.change1DY(poLongy, panelHeight);
				xForm1D = new XForm1DProperty(0, y, x2, y);
			}
			// 垂直
			else if (LineDirectionEnum.vertical == vHLineData.getLineDirectionEnum()) {
				// 结束点X的值
				double poLongx = vHLineData.getVhX();
				double x = VisioOutputTools.change1DX(poLongx);
				double y1 = VisioOutputTools.change1DY(0, panelHeight);
				xForm1D = new XForm1DProperty(x, y1, x, 0);
			}
			baseProperty.getLine().setLinePattern(LinePattern.LINE_16);
			baseProperty.getLine().setLineColorTrans(0.5F);
			baseProperty.setXForm1D(xForm1D);
		}
		// // 斜线，横线，竖线
		else if (eprosElement instanceof JecnBaseDivedingLineData) {
			JecnDiviLineDataCloneable lineData = ((JecnBaseDivedingLineData) eprosElement).getDiviLineCloneable();
			// 开始点的X
			double x1 = VisioOutputTools.change1DX(lineData.getStartXInt());
			// 开始点的Y
			double y1 = VisioOutputTools.change1DY(lineData.getStartYInt(), panelHeight);
			// 结束点的X
			double x2 = VisioOutputTools.change1DX(lineData.getEndXInt());
			// 结束点的Y
			double y2 = VisioOutputTools.change1DY(lineData.getEndYInt(), panelHeight);
			xForm1D = new XForm1DProperty(x1, y1, x2, y2);
			baseProperty.setXForm1D(xForm1D);
		}
		// 注释框 // 批注
		else if (eprosElement instanceof JecnCommentLineData) {
			JecnCommentLineData figure = (JecnCommentLineData) eprosElement;
			// 开始点的X
			double x1 = VisioOutputTools.change1DX(figure.getStartX());
			// 开始点的Y
			double y1 = VisioOutputTools.change1DY(figure.getStartY(), panelHeight);
			// 结束点的X
			double x2 = VisioOutputTools.change1DX(figure.getEndX());
			// 结束点的Y
			double y2 = VisioOutputTools.change1DY(figure.getEndY(), panelHeight);
			xForm1D = new XForm1DProperty(x1, y1, x2, y2);
			baseProperty.setXForm1D(xForm1D);
		}
		// 以下是特殊图形 在epros中是一个点+宽高 在visio中是一个点
		if (CONSTANT_DEGREE_SINGLE.equals(master.getMasterNameU())
				|| CONSTANT_DEGREE_DOUBLE.equals(master.getMasterNameU())) {
			JecnFigureData figure = (JecnFigureData) eprosElement;
			// 图形的X轴
			double begX = figure.getFigureDataCloneable().getDoubleX();
			// 图形的Y轴
			double begY = figure.getFigureDataCloneable().getDoubleY();
			// 图形的宽度
			int width = figure.getFigureSizeX();
			// 图型的高度
			int height = figure.getFigureSizeY();
			// 角度
			double angle = figure.getCurrentAngle();
			xForm1D = getDegreeXForm1D(panelHeight, begX, begY, width, height, angle);
			baseProperty.setXForm1D(xForm1D);
			baseProperty.setXForm(new XFormProperty(-1, -1, -1, Unit.pxToMM(height), angle));
		}

	}

	/**
	 * 获取 单项双向箭头DegreeXForm1D
	 * 
	 * @param panelHeight
	 * @param begX
	 * @param begY
	 * @param width
	 * @param height
	 * @param angle
	 * @return XForm1DProperty
	 */
	private XForm1DProperty getDegreeXForm1D(int panelHeight, double begX, double begY, int width, int height,
			double angle) {
		double endX = 0;
		double endY = 0;
		if (angle == 0 || angle == 360) {
			begY = begY + height / 2;
			endX = begX + width;
			endY = begY;
		} else if (angle == 45) {
			endX = begX + width;
			endY = begY + height;
		} else if (angle == 90) {
			begX = begX + height / 2;
			endX = begX;
			endY = begY + height;
		} else if (angle == 135) {
			double bx = begX + height * 0.7;
			double by = begY + (width * 0.3);
			endX = begX + height * 0.1;
			endY = begY + width * 0.9;
			begX = bx;
			begY = by;
		} else if (angle == 180) {
			begY = begY + height / 2;
			endX = begX;
			begX = begX + width;
			endY = begY;
		} else if (angle == 225) {
			double bx = begX + height * 0.7;
			double by = begY + (width * 0.7);
			endX = begX + height * 0.1;
			endY = begY + width * 0.1;
			begX = bx;
			begY = by;
		} else if (angle == 270) {
			begX = begX + height / 2;
			endX = begX;
			endY = begY;
			begY = begY + width;
		} else if (angle == 315) {
			double bx = begX + height * 0.3;
			double by = begY + (width * 0.7);
			endX = begX + height * 0.9;
			endY = begY + width * 0.1;
			begX = bx;
			begY = by;
		}
		double x1 = VisioOutputTools.change1DX(begX);
		double y1 = VisioOutputTools.change1DY(begY, panelHeight);
		double x2 = VisioOutputTools.change1DX(endX);
		double y2 = VisioOutputTools.change1DY(endY, panelHeight);
		return new XForm1DProperty(x1, y1, x2, y2);
	}

	/**
	 * 初始化 通用的属性
	 * 
	 * @param eprosElement
	 * @return
	 */
	private BaseProperty initCommentProperty(JecnAbstractFlowElementData eprosElement) {
		BaseProperty baseProperty = new BaseProperty();
		// 0、字体位置
		// 初始化字体位置 根据
		initFontPosition(eprosElement, baseProperty);
		// 1、 设置字体属性字体属性
		CharProperty charPro = new CharProperty();
		createChartProperty(eprosElement, charPro);
		// 2、 设置填充属性
		FillProperty fill = new FillProperty();
		createFillProperty(eprosElement, fill);
		// 3、设置边框线属性
		LineProperty line = new LineProperty();
		createLineProperty(eprosElement, line);

		baseProperty.setLine(line);
		baseProperty.setFill(fill);
		baseProperty.setChart(charPro);
		baseProperty.setLayerMember(eprosElement.getZOrderIndex());
		return baseProperty;
	}

	/**
	 *设置字体属性
	 * 
	 * @param eprosElement
	 * @param charPro
	 */
	private void createChartProperty(JecnAbstractFlowElementData eprosElement, CharProperty charPro) {
		Color fontColor = eprosElement.getFontColor();
		int font = VisioModel.getFaceNameId(eprosElement.getFontName());
		if (font == -1) {
			font = VisioModel.getFaceNameId("宋体");
		}
		// 默认普通样式PLAIN（0） ，1 加粗
		FontStyle fontStyle = VisioOutputTools.getFontStyle(eprosElement.getFontStyle());
		charPro.setAsianFont(font);
		charPro.setFont(font);
		charPro.setColor(fontColor);
		charPro.setStyle(fontStyle);
		charPro.setColor(eprosElement.getFontColor());
		charPro.setSize(eprosElement.getFontSize() - fontOffset);
	}

	/**
	 * 设置边框线的属性
	 * 
	 * @param eprosElement
	 * @param line
	 */
	private void createLineProperty(JecnAbstractFlowElementData eprosElement, LineProperty line) {
		// 线类型
		LinePattern linePattern = VisioOutputTools.getLinePattern(eprosElement.getBodyStyle());
		// 线条颜色
		Color lineColor = eprosElement.getBodyColor();
		// 线条粗细默认1磅
		line.setLineWeight(0.1F);
		
		//横线  、竖线 、分割线 取粗细
		if (MapElemType.DividingLine == eprosElement.getMapElemType()
				|| MapElemType.HDividingLine == eprosElement.getMapElemType()
				|| MapElemType.VDividingLine == eprosElement.getMapElemType()) {
			line.setLineWeight(eprosElement.getBodyWidth());
		}
		line.setLinePattern(linePattern);
		line.setLineColor(lineColor);
	}

	/**
	 * 设置图形的填充信息
	 * 
	 * @param eprosElement
	 * @param fill
	 */
	private void createFillProperty(JecnAbstractFlowElementData eprosElement, FillProperty fill) {
		// 阴影
		if (eprosElement.isShadowsFlag()) {
			fill.setShdwPattern(FillPattern._01);
			fill.setShdwForegnd(eprosElement.getShadowColor());
		}
		// 填充:none为未填充,one为单色效果,two为双色效果 默认未填充单色效果
		FillType fillType = eprosElement.getFillType();
		switch (fillType) {
		case none:
			fill.setFillForegndTrans(1F);
			break;
		case one:
			fill.setFillForegnd(eprosElement.getFillColor());
			fill.setFillPattern(VisioOutputTools.getFillPattern(fillType));
			break;
		case two:
			// 渐变方向
			FillColorChangType colorChangeType = eprosElement.getFillColorChangType();
			fill.setFillForegnd(eprosElement.getChangeColor()); // 颜色1
			fill.setFillBkgnd(eprosElement.getFillColor()); // 颜色2
			fill.setFillPattern(VisioOutputTools.getFillPattern(colorChangeType));
			break;
		}
	}

	/**
	 * 初始化文本快的位置 根据 mapElemType过滤（0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下
	 * 8：右下），默认正中4
	 * 
	 * @param eprosElement
	 * @param baseProperty
	 */
	private void initFontPosition(JecnAbstractFlowElementData eprosElement, BaseProperty baseProperty) {
		// 过滤掉不设置文本块文字的图形 模版居中的
		if (!needToInitFontPosition(eprosElement)) {
			return;
		}
		ParaProperty paraBean = new ParaProperty();
		TextBlockProperty textBlock = new TextBlockProperty();
		if (eprosElement.getFontUpRight()) {
			textBlock.setTextDirection(TextDirection.VERTICALLY);
		}
		switch (eprosElement.getFontPosition()) {
		case 0:
			paraBean.setHorzAlign(Align.LEFT);
			textBlock.setVerticalAlign(Valign.TOP);
			break;
		case 1:
			paraBean.setHorzAlign(Align.CENTER);
			textBlock.setVerticalAlign(Valign.TOP);
			break;
		case 2:
			paraBean.setHorzAlign(Align.RIGHT);
			textBlock.setVerticalAlign(Valign.TOP);
			break;
		case 3:
			paraBean.setHorzAlign(Align.LEFT);
			textBlock.setVerticalAlign(Valign.MIDDLE);
			break;
		case 4:
			paraBean.setHorzAlign(Align.CENTER);
			textBlock.setVerticalAlign(Valign.MIDDLE);
			break;
		case 5:
			paraBean.setHorzAlign(Align.RIGHT);
			textBlock.setVerticalAlign(Valign.MIDDLE);
			break;
		case 6:
			paraBean.setHorzAlign(Align.LEFT);
			textBlock.setVerticalAlign(Valign.BOTTOM);
			break;
		case 7:
			paraBean.setHorzAlign(Align.CENTER);
			textBlock.setVerticalAlign(Valign.BOTTOM);
			break;
		case 8:
			paraBean.setHorzAlign(Align.RIGHT);
			textBlock.setVerticalAlign(Valign.BOTTOM);
			break;
		default:
			paraBean.setHorzAlign(Align.CENTER);
			textBlock.setVerticalAlign(Valign.MIDDLE);
		}
		baseProperty.setParaBean(paraBean);
		baseProperty.setTextBlock(textBlock);

	}

	/**
	 * 是否需要设置字体位置
	 * 
	 * @param eprosElement
	 * @return true 需要初始化 false 不需要
	 */
	private boolean needToInitFontPosition(JecnAbstractFlowElementData eprosElement) {
		// 以下图形不需要设置字体位置功能
		MapElemType mapElemType = eprosElement.getMapElemType();
		if (MapElemType.RoundRectWithLine == mapElemType // 两部分职能
				|| MapElemType.Rhombus == mapElemType // 决策
				|| MapElemType.ITRhombus == mapElemType// IT 决策
				|| MapElemType.DataImage == mapElemType// 信息系统
				|| MapElemType.ReverseArrowhead == mapElemType // 返入
				|| MapElemType.RightArrowhead == mapElemType// 返出
				|| MapElemType.CommonLine == mapElemType// 连接线
				|| MapElemType.ParallelLines == mapElemType// 横分割线
				|| MapElemType.VerticalLine == mapElemType// 竖分割线
				|| MapElemType.HDividingLine == mapElemType// 横线
				|| MapElemType.VDividingLine == mapElemType// 竖线
				|| MapElemType.ManhattanLine == mapElemType) {// 曼哈顿线
			return false;
		}
		return true;
	}

	protected Master getVisioMaster(MapElemType mapElement) {
		String masterNameU = configMap.get(mapElement.toString());
		return VisioModel.getVisioMaster(masterNameU);
	}
}
