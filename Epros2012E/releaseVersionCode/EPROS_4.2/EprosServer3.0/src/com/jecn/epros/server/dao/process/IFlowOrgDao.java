package com.jecn.epros.server.dao.process;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.FlowOrgT;
import com.jecn.epros.server.common.IBaseDao;

public interface IFlowOrgDao extends IBaseDao<FlowOrgT, Long>{

	List<JecnUser> listOrgUsers(Set<Long> orgs);

}
