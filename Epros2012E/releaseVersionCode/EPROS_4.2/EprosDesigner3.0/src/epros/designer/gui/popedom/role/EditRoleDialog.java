package epros.designer.gui.popedom.role;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 编辑角色
 * 
 * 修改人：chehuanbo 时间：2014-10-14 描述：编辑角色中添加风险权限选择 版本：V3.06
 * 
 * */
public class EditRoleDialog extends RoleDialog {

	private static Logger log = Logger.getLogger(EditRoleDialog.class);
	private JecnTree jTree = null;

	private JecnTreeBean jecnTreeBean;

	public EditRoleDialog(JecnTreeBean jecnTreeBean, JecnTree jTree) {
		this.jecnTreeBean = jecnTreeBean;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);

		initEditRoleInfo(jecnTreeBean);
	}

	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		JecnTreeNode selectNode = JecnTreeCommon.getTreeNode(jecnTreeBean, jTree);
		if (selectNode != null) {
			return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
		} else {
			try {
				return ConnectionPool.getJecnRole().validateRepeatNameEidt(name, jecnTreeBean.getId(),
						jecnTreeBean.getPid(), JecnConstants.projectId);
			} catch (Exception e) {
				log.error("EditRoleDialog validateNodeRepeat is error！", e);
				throw e;

			}
		}
	}

	@Override
	public void saveData() {
		// 更新角色信息
		jecnRoleInfo.setRoleName(this.getRoleName());
		jecnRoleInfo.setRoleRemark(this.getRoleContent());
		jecnRoleInfo.setUpdatePersonId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		try {
			ConnectionPool.getJecnRole().updateRole(jecnRoleInfo, getRelateIdMap(), JecnConstants.getUserId());
			this.setOperation(true);
			this.dispose();
		} catch (Exception e) {
			log.error("EditRoleDialog saveData is error！", e);
		}

	}

	@Override
	public String getDialogTitle() {

		return JecnProperties.getValue("editRole");
	}
}
