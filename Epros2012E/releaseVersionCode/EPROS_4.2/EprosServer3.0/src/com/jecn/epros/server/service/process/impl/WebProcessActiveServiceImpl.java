package com.jecn.epros.server.service.process.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT;
import com.jecn.epros.server.bean.integration.JecnControlPointT;
import com.jecn.epros.server.bean.process.JecnFlowStructureImage;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnRefIndicators;
import com.jecn.epros.server.bean.process.JecnRefIndicatorsT;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.service.process.IWebProcessActiveService;
import com.jecn.epros.server.webBean.process.ActiveModeBean;
import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;

/**
 * 活动明细获取数据
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-10 时间：上午11:07:35
 */
public class WebProcessActiveServiceImpl extends AbsBaseService<String, String> implements IWebProcessActiveService {
	private Logger log = Logger.getLogger(FlowStructureServiceImpl.class);
	/** 流程主表接口 */
	private IFlowStructureDao flowStructureDao;

	private IJecnConfigItemDao configItemDao;

	/**
	 * 点击活动，获取活动信息
	 * 
	 * @param activeId
	 *            活动主键ID
	 * 
	 * @param isPub
	 *            true:发布
	 * 
	 * @throws Exception
	 */
	@Override
	public ProcessActiveWebBean getProcessActiveWebBean(Long activeId, boolean isPub) throws Exception {
		ProcessActiveWebBean processActiveWebBean = new ProcessActiveWebBean();
		// 流程主键ID
		Long flowId;
		// 活动类型主键ID
		Long typeId;
		// 信息化 0：线下 ;1：线上
		int isOnLine;
		try {
			// 获取活动输入输出说明配置
			String activityNote = configItemDao.selectValue(1, ConfigItemPartMapMark.activityFileShow.toString());
			// 活动明细时间配置
			String activityTimeConfig = configItemDao.selectValue(6, ConfigItemPartMapMark.isShowDateActivity
					.toString());

			processActiveWebBean.setActivityNoteShow(activityNote);
			processActiveWebBean.setActivityTimeConfig(activityTimeConfig);
			if (isPub) {
				JecnFlowStructureImage jecnFlowStructureImage = (JecnFlowStructureImage) flowStructureDao.getSession()
						.get(JecnFlowStructureImage.class, activeId);
				if (jecnFlowStructureImage == null) {
					return null;
				}
				flowId = jecnFlowStructureImage.getFlowId();
				typeId = jecnFlowStructureImage.getActiveTypeId();
				isOnLine = jecnFlowStructureImage.getIsOnLine();

				// 活动主键
				processActiveWebBean.setActiveFigureId(jecnFlowStructureImage.getFigureId());
				// 活动名称
				processActiveWebBean.setActiveName(jecnFlowStructureImage.getFigureText());
				// 活动编号
				processActiveWebBean.setActiveId(jecnFlowStructureImage.getActivityId());
				// 活动说明
				processActiveWebBean.setActiveShow(jecnFlowStructureImage.getActivityShow());
				// 输入输出说明
				processActiveWebBean.setActivityInput(jecnFlowStructureImage.getActivityInput());
				processActiveWebBean.setActivityOutput(jecnFlowStructureImage.getActivityOutput());
				// 关键活动类型
				if (StringUtils.isNotBlank(jecnFlowStructureImage.getLineColor()))
					processActiveWebBean.setKeyActiveState(Integer.parseInt(jecnFlowStructureImage.getLineColor()));
				// 关键活动说明
				processActiveWebBean.setKeyActiveShow(jecnFlowStructureImage.getRoleRes());

				// 活动时间类型
				processActiveWebBean.setTimeType(jecnFlowStructureImage.getTimeType());
				// 开始时间
				processActiveWebBean.setStartTime(jecnFlowStructureImage.getStartTime());
				// 结束时间
				processActiveWebBean.setStopTime(jecnFlowStructureImage.getStopTime());

			} else {
				JecnFlowStructureImageT jecnFlowStructureImage = (JecnFlowStructureImageT) flowStructureDao
						.getSession().get(JecnFlowStructureImageT.class, activeId);
				if (jecnFlowStructureImage == null) {
					return null;
				}
				flowId = jecnFlowStructureImage.getFlowId();
				typeId = jecnFlowStructureImage.getActiveTypeId();
				isOnLine = jecnFlowStructureImage.getIsOnLine();
				// 活动主键
				processActiveWebBean.setActiveFigureId(jecnFlowStructureImage.getFigureId());
				// 活动名称
				processActiveWebBean.setActiveName(jecnFlowStructureImage.getFigureText());
				// 活动编号
				processActiveWebBean.setActiveId(jecnFlowStructureImage.getActivityId());
				// 活动说明
				processActiveWebBean.setActiveShow(jecnFlowStructureImage.getActivityShow());
				// 输入输出说明
				processActiveWebBean.setActivityInput(jecnFlowStructureImage.getActivityInput());
				processActiveWebBean.setActivityOutput(jecnFlowStructureImage.getActivityOutput());
				// 关键活动类型
				if (StringUtils.isNotBlank(jecnFlowStructureImage.getLineColor()))
					processActiveWebBean.setKeyActiveState(Integer.parseInt(jecnFlowStructureImage.getLineColor()));
				// 关键活动说明
				processActiveWebBean.setKeyActiveShow(jecnFlowStructureImage.getRoleRes());// 活动时间类型
				processActiveWebBean.setTimeType(jecnFlowStructureImage.getTimeType());
				// 开始时间
				processActiveWebBean.setStartTime(jecnFlowStructureImage.getStartTime());
				// 结束时间
				processActiveWebBean.setStopTime(jecnFlowStructureImage.getStopTime());

			}
			
			if (processActiveWebBean.getActiveShow() != null) {
				processActiveWebBean.setActiveShow(processActiveWebBean.getActiveShow().replaceAll("\n", "<br>"));
			}
			if (processActiveWebBean.getKeyActiveShow() != null) {
				processActiveWebBean.setKeyActiveShow(processActiveWebBean.getKeyActiveShow().replaceAll("\n", "<br>"));
			}
			if (processActiveWebBean.getActivityInput() != null) {
				processActiveWebBean.setActivityInput(processActiveWebBean.getActivityInput().replaceAll("\n", "<br>"));
			}
			if (processActiveWebBean.getActivityOutput() != null) {
				processActiveWebBean.setActivityOutput(processActiveWebBean.getActivityOutput().replaceAll("\n", "<br>"));
			}
			
			// 获取活动类型名称
			activeTypeName(processActiveWebBean, typeId);

			/**
			 * 活动相关角色，附件信息
			 * 
			 */
			getActiveRelateFiles(processActiveWebBean, activeId, flowId, isPub);

			if (isOnLine == 1) { // 0：线下 ;1：线上
				// 信息化
				getJecnActiveOnLine(processActiveWebBean, activeId, isPub, flowId);
			}

			return processActiveWebBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	/**
	 * 获取活动类型名称
	 * 
	 * @param processActiveWebBean
	 * @param typeId
	 *            类别主键ID
	 */
	private void activeTypeName(ProcessActiveWebBean processActiveWebBean, Long typeId) {
		if (typeId == null) {
			return;
		}
		String sql = "SELECT JAT.TYPE_NAME FROM JECN_ACTIVE_TYPE JAT WHERE JAT.TYPE_ID = ?";
		List<String> list = this.flowStructureDao.listNativeSql(sql, typeId);
		if (list.size() == 1) {
			processActiveWebBean.setActiveTypeName(list.get(0));
		}
	}

	/**
	 * 活动对应的执行角色
	 * 
	 * @param processActiveWebBean
	 * @param activeId
	 * @param isPub
	 */
	private void getActiveRelateRole(ProcessActiveWebBean processActiveWebBean, Long activeId, boolean isPub) {
		// 执行角色
		String sql = "";
		if (isPub) {
			sql = "select t.figure_id,t.figure_text from jecn_flow_structure_image t,jecn_role_active jra where t.figure_id = jra.figure_role_id and jra.figure_active_id=?";
		} else {
			sql = "select t.figure_id,t.figure_text from jecn_flow_structure_image_t t,jecn_role_active_t jra where t.figure_id = jra.figure_role_id and jra.figure_active_id=?";
		}
		List<Object[]> list = flowStructureDao.listNativeSql(sql, activeId);
		if (list.size() > 0) {
			Object[] obj = list.get(0);
			if (obj != null && obj[0] != null && obj[1] != null) {
				processActiveWebBean.setRoleName(obj[1].toString());
				processActiveWebBean.setRoleFigureId(Long.valueOf(obj[0].toString()));
			}
		} else {
			log.error("角色对应活动： getActiveRelateRole " + list);
		}
	}

	/**
	 * 获取活动关联文件
	 * 
	 * @param processActiveWebBean
	 *            活动明细 详细信息
	 * @param activeId
	 *            活动主键ID
	 * @param isPub
	 *            true 发布；false：未发布
	 * @throws Exception
	 */
	private void getActiveRelateFiles(ProcessActiveWebBean processActiveWebBean, Long activeId, Long flowId,
			boolean isPub) throws Exception {
		// 活动对应的执行角色
		getActiveRelateRole(processActiveWebBean, activeId, isPub);
		// 输入和操作规范
		getActiveInputAndOperationFiles(processActiveWebBean, activeId, isPub);
		// 输入，样例
		getActiveOuputFiles(processActiveWebBean, activeId, isPub);
		// 活动指标集合
		getJecnRefIndicatorsList(processActiveWebBean, activeId, isPub);

		// 活动控制点
		getJecnActiveControlT(processActiveWebBean, activeId, isPub, flowId);
		// 活动相关标准
		getJecnActiveStandard(processActiveWebBean, activeId, isPub, flowId);
	}

	/**
	 * 活动线上信息
	 * 
	 * @param processActiveWebBean
	 * @param activeId
	 * @param isPub
	 * @param flowId
	 */
	private void getJecnActiveOnLine(ProcessActiveWebBean processActiveWebBean, Long activeId, boolean isPub,
			Long flowId) {

		String table = isPub ? "JECN_ACTIVE_ONLINE" : "JECN_ACTIVE_ONLINE_T";
		String sql = "SELECT OT.ID," + "       OT.PROCESS_ID," + "       OT.ACTIVE_ID," + "       OT.TOOL_ID,"
				+ "       OT.ONLINE_TIME," + "       ST.FLOW_SUSTAIN_TOOL_DESCRIBE,OT.INFORMATION_DESCRIPTION"
				+ "  FROM " + table + " OT, JECN_FLOW_SUSTAIN_TOOL ST" + " WHERE OT.TOOL_ID = ST.FLOW_SUSTAIN_TOOL_ID"
				+ "   AND OT.PROCESS_ID =? AND OT.ACTIVE_ID=?";
		List<Object[]> list = this.flowStructureDao.listNativeSql(sql, flowId, activeId);
		List<JecnActiveOnLineTBean> listOnLineTBean = new ArrayList<JecnActiveOnLineTBean>();
		JecnActiveOnLineTBean activeOnLineTBean = null;
		for (Object[] objects : list) {
			if (objects[0] == null || objects[1] == null || objects[2] == null || objects[3] == null
					|| objects[4] == null || objects[5] == null) {
				return;
			}
			activeOnLineTBean = new JecnActiveOnLineTBean();
			activeOnLineTBean.setId(objects[0].toString());
			activeOnLineTBean.setProcessId(Long.valueOf(objects[1].toString()));
			activeOnLineTBean.setActiveId(Long.valueOf(objects[2].toString()));
			activeOnLineTBean.setToolId(Long.valueOf(objects[3].toString()));
			// 线上时间
			activeOnLineTBean.setOnLineTime(JecnCommon.getDateByString(objects[4].toString()));
			// 系统名称（支持工具名称）
			activeOnLineTBean.setSysName(objects[5].toString());
			if (objects[6] != null) {
				String lineShow = objects[6].toString().replaceAll("\n", "<br/>");
				activeOnLineTBean.setInformationDescription(lineShow);
			} else {
				activeOnLineTBean.setInformationDescription("");
			}
			listOnLineTBean.add(activeOnLineTBean);
		}
		processActiveWebBean.setListJecnActiveOnLineBean(listOnLineTBean);
	}

	/**
	 * 活动相关标准
	 * 
	 * @param processActiveWebBean
	 * @param activeId
	 *            活动主键ID
	 * @param isPub
	 * @param flowId
	 */
	private void getJecnActiveStandard(ProcessActiveWebBean processActiveWebBean, Long activeId, boolean isPub,
			Long flowId) {
		String sql = "";
		if (isPub) {
			sql = "SELECT AT.ID," + "       AT.ACTIVE_ID," + "       AT.CLAUSE_ID," + "       AT.RELA_TYPE,"
					+ "       AT.STANDARD_ID," + "       CC.CRITERION_CLASS_NAME"
					+ "  FROM JECN_ACTIVE_STANDARD      AT," + "       JECN_CRITERION_CLASSES      CC,"
					+ "       JECN_FLOW_STRUCTURE_IMAGE IT" + " WHERE AT.STANDARD_ID = CC.CRITERION_CLASS_ID"
					+ "   AND AT.ACTIVE_ID = IT.FIGURE_ID" + "   AND IT.FLOW_ID = ? AND IT.FIGURE_ID=?";
		} else {
			sql = "SELECT AT.ID," + "       AT.ACTIVE_ID," + "       AT.CLAUSE_ID," + "       AT.RELA_TYPE,"
					+ "       AT.STANDARD_ID," + "       CC.CRITERION_CLASS_NAME"
					+ "  FROM JECN_ACTIVE_STANDARD_T      AT," + "       JECN_CRITERION_CLASSES      CC,"
					+ "       JECN_FLOW_STRUCTURE_IMAGE_T IT" + " WHERE AT.STANDARD_ID = CC.CRITERION_CLASS_ID"
					+ "   AND AT.ACTIVE_ID = IT.FIGURE_ID" + "   AND IT.FLOW_ID = ? AND IT.FIGURE_ID=?";
		}

		List<Object[]> list = this.flowStructureDao.listNativeSql(sql, flowId, activeId);

		List<JecnActiveStandardBeanT> listStandardT = new ArrayList<JecnActiveStandardBeanT>();
		for (Object[] objects : list) {
			if (objects[0] == null || objects[1] == null || objects[3] == null || objects[4] == null
					|| objects[5] == null) {
				return;
			}
			JecnActiveStandardBeanT standardBeanT = new JecnActiveStandardBeanT();
			standardBeanT.setId(objects[0].toString());
			standardBeanT.setActiveId(Long.valueOf(objects[1].toString()));
			if (objects[2] != null) {// 存在条款要求父节点ID
				standardBeanT.setClauseId(Long.valueOf(objects[2].toString()));
			}
			// 关联标准或条款要求主键 ID(标准表主键ID)
			standardBeanT.setRelaType(Integer.parseInt(objects[3].toString()));
			standardBeanT.setStandardId(Long.valueOf(objects[4].toString()));
			standardBeanT.setRelaName(objects[5].toString());
			listStandardT.add(standardBeanT);
		}

		processActiveWebBean.setListJecnActiveStandardBean(listStandardT);
	}

	/**
	 * 活动控制点
	 * 
	 * @param processActiveWebBean
	 *            活动明细 详细信息
	 * @param activeId
	 *            活动主键ID
	 * @param isPub
	 *            true 发布；false：未发布
	 */
	private void getJecnActiveControlT(ProcessActiveWebBean processActiveWebBean, Long activeId, boolean isPub,
			Long flowId) {

		String sql = "";
		if (isPub) {
			sql = "SELECT CP.ID," + "       CP.ACTIVE_ID," + "       CP.CONTROL_CODE," + "       CP.KEY_POINT,"
					+ "       CP.FREQUENCY," + "       CP.METHOD," + "       CP.TYPE," + "       JS.RISK_CODE,"
					+ "       JC.DESCRIPTION" + "  FROM JECN_CONTROL_POINT        CP,"
					+ "       JECN_FLOW_STRUCTURE_IMAGE IT," + "       JECN_RISK                   JS,"
					+ "       JECN_CONTROLTARGET          JC" + " WHERE CP.ACTIVE_ID = IT.FIGURE_ID"
					+ "   AND CP.RISK_ID = JS.ID" + "   AND CP.TARGET_ID = JC.ID" + "   AND IT.FLOW_ID =?"
					+ " AND IT.FIGURE_ID =?";
		} else {
			sql = "SELECT CP.ID," + "       CP.ACTIVE_ID," + "       CP.CONTROL_CODE," + "       CP.KEY_POINT,"
					+ "       CP.FREQUENCY," + "       CP.METHOD," + "       CP.TYPE," + "       JS.RISK_CODE,"
					+ "       JC.DESCRIPTION" + "  FROM JECN_CONTROL_POINT_T        CP,"
					+ "       JECN_FLOW_STRUCTURE_IMAGE_T IT," + "       JECN_RISK                   JS,"
					+ "       JECN_CONTROLTARGET          JC" + " WHERE CP.ACTIVE_ID = IT.FIGURE_ID"
					+ "   AND CP.RISK_ID = JS.ID" + "   AND CP.TARGET_ID = JC.ID" + "   AND IT.FLOW_ID =?"
					+ " AND IT.FIGURE_ID =?";
		}

		List<Object[]> list = flowStructureDao.listNativeSql(sql, flowId, activeId);
		// if (list.size() != 1) {// 控制点只有一条数据
		// return;
		// }
		// Object[] objects = list.get(0);
		List<JecnControlPointT> controlPointTs = new ArrayList<JecnControlPointT>();
		for (Object[] objects : list) {
			if (objects[0] == null || objects[1] == null || objects[2] == null || objects[3] == null
					|| objects[4] == null || objects[6] == null || objects[7] == null || objects[8] == null) {
				return;
			}
			JecnControlPointT controlPointT = null;
			controlPointT = new JecnControlPointT();
			controlPointT.setId(objects[0].toString());
			controlPointT.setActiveId(Long.valueOf(objects[1].toString()));
			controlPointT.setControlCode(objects[2].toString());
			// 关键控制点：0：是，1：否
			controlPointT.setKeyPoint(Integer.parseInt(objects[3].toString()));
			controlPointT.setFrequency(Integer.parseInt(objects[4].toString()));
			controlPointT.setMethod(Integer.parseInt(objects[5].toString()));
			controlPointT.setType(Integer.parseInt(objects[6].toString()));
			// 风险编号
			controlPointT.setRiskNum(valueString(objects[7]));
			// 控制目标
			String strTarget = valueString(objects[8]);
			// 换行
			controlPointT.setControlTarget(strTarget.replaceAll("\n", "<br/>"));
			controlPointTs.add(controlPointT);
		}

		processActiveWebBean.setControlPointTs(controlPointTs);
	}

	private String valueString(Object obj) {
		return obj == null ? "" : obj.toString();
	}

	/**
	 * 获取活动指标集合
	 * 
	 * @param processActiveWebBean
	 * @param activeId
	 * @param isPub
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	private void getJecnRefIndicatorsList(ProcessActiveWebBean processActiveWebBean, Long activeId, boolean isPub)
			throws Exception {
		// 添加指标
		String hql = "";
		List<JecnRefIndicators> indicatorsList = null;
		if (isPub) {
			hql = "from JecnRefIndicators where activityId=?";
			indicatorsList = flowStructureDao.listHql(hql, activeId);
		} else {
			hql = "from JecnRefIndicatorsT where activityId=?";
			List<JecnRefIndicatorsT> indicatorsListT = flowStructureDao.listHql(hql, activeId);
			indicatorsList = new ArrayList<JecnRefIndicators>();
			for (JecnRefIndicatorsT jecnRefIndicatorsT : indicatorsListT) {
				JecnRefIndicators jecnRefIndicators = new JecnRefIndicators();
				BeanUtils.copyProperties(jecnRefIndicators, jecnRefIndicatorsT);
				indicatorsList.add(jecnRefIndicators);
			}
		}
		processActiveWebBean.setIndicatorsList(indicatorsList);
	}

	/**
	 * 操作规范
	 * 
	 * @param processActiveWebBean
	 * @param activeId
	 * @param isPub
	 */
	private void getActiveOuputFiles(ProcessActiveWebBean processActiveWebBean, Long activeId, boolean isPub) {
		// 活动输出
		List<ActiveModeBean> listMode = new ArrayList<ActiveModeBean>();
		String sql = "";
		if (isPub) {
			// 表单
			sql = "select jf.file_id,jf.file_name,jf.doc_id,jmf.mode_file_id from jecn_mode_file jmf,"
					+ "       jecn_file jf where jmf.file_m_id=jf.file_id and jf.del_state=0 and jmf.figure_id=?";
		} else {
			sql = "select jf.file_id,jf.file_name,jf.doc_id,jmf.mode_file_id from jecn_mode_file_t jmf,"
					+ "       jecn_file_t jf where jmf.file_m_id=jf.file_id and jf.del_state=0 and jmf.figure_id=?";
		}
		List<Object[]> listModeObj = flowStructureDao.listNativeSql(sql, activeId);
		if (isPub) {
			// 样例
			sql = "select jf.file_id,jf.file_name,jf.doc_id,jmf.mode_file_id from jecn_mode_file jmf,"
					+ "       jecn_file jf,jecn_templet jt where jt.mode_file_id=jmf.mode_file_id and jf.del_state=0 and jt.file_id=jf.file_id and jmf.figure_id=?";

		} else {
			sql = "select jf.file_id,jf.file_name,jf.doc_id,jmf.mode_file_id from jecn_mode_file_t jmf,"
					+ "       jecn_file_t jf,jecn_templet_t jt where jt.mode_file_id=jmf.mode_file_id and jf.del_state=0 and jt.file_id=jf.file_id and jmf.figure_id=?";
		}
		List<Object[]> listTempletObj = flowStructureDao.listNativeSql(sql, activeId);
		for (Object[] object : listModeObj) {
			if (object == null || object[0] == null || object[1] == null || object[3] == null) {
				continue;
			}
			ActiveModeBean activeModeBean = new ActiveModeBean();
			FileWebBean fileWebBean = new FileWebBean();
			// 文件主键
			fileWebBean.setFileId(Long.valueOf(object[0].toString()));
			// 文件名称
			fileWebBean.setFileName(object[1].toString());
			// 文件编号
			if (object[2] != null)
				fileWebBean.setFileNum(object[2].toString());
			activeModeBean.setModeFile(fileWebBean);
			List<FileWebBean> listTemplet = new ArrayList<FileWebBean>();
			// 添加样例
			for (Object[] objTemplet : listTempletObj) {
				if (objTemplet == null || objTemplet[0] == null || objTemplet[1] == null || objTemplet[3] == null) {
					continue;
				}
				if (object[3].equals(objTemplet[3])) {
					FileWebBean fileWebBeanTemplet = new FileWebBean();
					// 文件主键
					fileWebBeanTemplet.setFileId(Long.valueOf(objTemplet[0].toString()));
					// 文件名称
					fileWebBeanTemplet.setFileName(objTemplet[1].toString());
					// 文件编号
					if (objTemplet[2] != null)
						fileWebBeanTemplet.setFileNum(objTemplet[2].toString());
					listTemplet.add(fileWebBeanTemplet);
				}
			}
			activeModeBean.setListTemplet(listTemplet);
			listMode.add(activeModeBean);
		}
		processActiveWebBean.setListMode(listMode);
	}

	/**
	 * 获取活动对应的输入、操作规范
	 * 
	 * @param processActiveWebBean
	 * @param activeId
	 * @param isPub
	 *            true 发布；false：未发布
	 */
	private void getActiveInputAndOperationFiles(ProcessActiveWebBean processActiveWebBean, Long activeId, boolean isPub) {
		// 活动输入
		List<FileWebBean> listInput = null;
		// 活动操作规
		List<FileWebBean> listOperation = null;

		String sql = "";
		if (isPub) {// 发布
			sql = "select jaf.file_s_id,jf.file_name,jf.doc_id,jaf.file_type from jecn_activity_file "
					+ "jaf,jecn_file jf where jaf.file_s_id=jf.file_id and jf.del_state=0 and jaf.figure_id=?";
		} else {
			sql = "select jaf.file_s_id,jf.file_name,jf.doc_id,jaf.file_type from jecn_activity_file_t "
					+ "jaf,jecn_file_t jf where jaf.file_s_id=jf.file_id and jf.del_state=0 and  jaf.figure_id=?";
		}
		List<Object[]> listObj = flowStructureDao.listNativeSql(sql, activeId);
		if (listObj != null && listObj.size() > 0) {
			listInput = new ArrayList<FileWebBean>();
			// 活动操作规
			listOperation = new ArrayList<FileWebBean>();
		}
		for (Object[] object : listObj) {
			if (object == null || object[0] == null || object[1] == null || object[3] == null) {
				continue;
			}
			FileWebBean fileWebBean = new FileWebBean();
			// 文件主键
			fileWebBean.setFileId(Long.valueOf(object[0].toString()));
			// 文件名称
			fileWebBean.setFileName(object[1].toString());
			// 文件编号
			if (object[2] != null)
				fileWebBean.setFileNum(object[2].toString());
			if ("0".equals(object[3].toString())) {
				listInput.add(fileWebBean);
			} else {
				listOperation.add(fileWebBean);
			}
		}
		processActiveWebBean.setListInput(listInput);
		processActiveWebBean.setListOperation(listOperation);
	}

	public IFlowStructureDao getFlowStructureDao() {
		return flowStructureDao;
	}

	public void setFlowStructureDao(IFlowStructureDao flowStructureDao) {
		this.flowStructureDao = flowStructureDao;
	}

	public IJecnConfigItemDao getConfigItemDao() {
		return configItemDao;
	}

	public void setConfigItemDao(IJecnConfigItemDao configItemDao) {
		this.configItemDao = configItemDao;
	}
}
