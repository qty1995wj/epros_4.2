package com.jecn.epros.server.action.designer.define;

import java.util.List;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

public interface ITermDefinitionAction {
	public List<JecnTreeBean> getChilds(Long pId) throws Exception;

	public TermDefinitionLibrary createDir(TermDefinitionLibrary termDefinitionLibrary) throws Exception;

	public boolean isExistAdd(String name, Long id,int type) throws Exception;

	public boolean isExistEdit(String name, Long id,int type,Long pId) throws Exception;

	public void reName(String name, Long id,Long updatePeopleId) throws Exception;

	public TermDefinitionLibrary createTermDefine(TermDefinitionLibrary termDefinitionLibrary) throws Exception;

	public TermDefinitionLibrary editTermDefine(TermDefinitionLibrary termDefinitionLibrary) throws Exception;

	public List<JecnTreeBean> searchByName(String name) throws Exception;

	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception;

	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception;

	public void delete(List<Long> listIds, Long updatePersonId) throws Exception;
	
	public TermDefinitionLibrary getTermDefinitionLibraryById(Long id)throws Exception;
	
	public List<JecnTreeBean> getChildDirs(Long pId) throws Exception;

	public List<JecnTreeBean> getPnodes(Long id)throws Exception;

	public List<TermDefinitionLibrary> listTermsByIds(List<Long> idList)throws Exception;

	public List<JecnTreeBean> getRoleAuthChilds(long l, Long projectId, long userId);
	
	public List<JecnTreeBean> searchRoleAuthByName(String name,Long peopleId,Long projectId) throws Exception; 

}
