
package com.jecn.webservice.addon;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the addon package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: addon
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PROP }
     * 
     */
    public PROP createPROP() {
        return new PROP();
    }

    /**
     * Create an instance of {@link PROPS }
     * 
     */
    public PROPS createPROPS() {
        return new PROPS();
    }

    /**
     * Create an instance of {@link RETURNMSG }
     * 
     */
    public RETURNMSG createRETURNMSG() {
        return new RETURNMSG();
    }

}
