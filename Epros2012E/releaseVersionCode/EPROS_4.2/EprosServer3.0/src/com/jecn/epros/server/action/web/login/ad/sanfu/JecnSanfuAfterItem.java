package com.jecn.epros.server.action.web.login.ad.sanfu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnPath;

/**
 * 
 * 三幅单点登录所需配置信息，在系统启动时需要启动项
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSanfuAfterItem {
	private static Logger log = Logger.getLogger(JecnSanfuAfterItem.class);
	/** AD域IP */
	public static String value = "BCFD3E51207C0CF5";
	/** 单点登录url */
	public static String adURL = "http://172.28.9.202:9001/greenOffice/hmrcbRoute/CheckRandom.aspx?";

	/**
	 * 三幅单点登录 读取本地文件数据
	 * 
	 */
	public static void start() {
		// 读取配置文件
		InputStream fis = null;
		Properties properties = new Properties();

		String url = JecnPath.CLASS_PATH + "cfgFile/sanfu/ClientSSO.properties";
		log.info("配置文件路径:" + url);

		try {
			fis = new FileInputStream(url);
			properties.load(fis);

			// 读值
			value = properties.getProperty("sf.login.ssoKey");
			adURL = properties.getProperty("adURL");
		} catch (FileNotFoundException e) {
			log.error("获取文件路径失败:cfgFile/sanfu/ClientSSO.properties", e);
		} catch (IOException e) {
			log.error("读流异常:cfgFile/sanfu/ClientSSO.properties", e);
		} catch (Exception e) {
			log.error("其他异常:cfgFile/sanfu/ClientSSO.properties", e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					log
							.error(
									"关闭InputStream流异常:cfgFile/sanfu/ClientSSO.properties",
									e);
				}
			}
		}
	}
}
