package com.jecn.epros.server.action.web.timer;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;

/**
 * 定时器处理
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Jan 14, 2013 时间：2:42:14 PM
 */
public abstract class TimerAction extends BaseAction {
	/** 定时器处理类 */
	protected UserAutoBuss userAutoBuss;
	/** 前台必须传值：导入方式（excel、xml、hessian,db），默认是excel导入 */
	protected String importType = SyncConstant.IMPORT_TYPE_DB;
	protected final Log log = LogFactory.getLog(TimerAction.class);
	/** 1:Excel 2:DB;3:岗位匹配 */
	protected String importValue = String
			.valueOf(JecnContants.otherUserSyncType);
	/** 是否执行定时器 true:执行定时器 */
	protected boolean isTimer = false;
	
	/**
	 * 同步数据
	 */
	public abstract void synData() throws Exception;

	/**
	 * 初始化定时器数据
	 * 
	 * @return AutoBean
	 */
	public abstract AbstractConfigBean getAbstractConfigBean();

	public UserAutoBuss getUserAutoBuss() {
		return userAutoBuss;
	}

	/**
	 * 
	 * 拼装前台界面提示页面
	 * 
	 * @param info
	 * @param href
	 */
	public void ResultHtml(String info, String href) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			getResponse().setContentType("text/html;charset=UTF-8");
			out = getResponse().getWriter();

			if (isNullObj(out)) {
				return;
			}
			out.println("<html>");
			out.println("<head>");
			out
					.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
			out.println("<title></title>");
			out.println("</head>");
			out.println("<script language='javascript'>");
			// info = new String(info.getBytes(), "utf-8");
			out.println("alert('" + info + "');");
			if (href != null) {
				out.println("window.location.href='" + href + "';");
			}
			out.println("</script>");
			out.println("</html>");
			out.flush();
		} catch (IOException e) {
			log.error("",e);
		} finally {
			if (out!=null) {
				try {
					out.close();
				} catch (Exception e) {
					log.error("",e);
				}
			}
		}
	}

	/**
	 * 
	 * 给定参数是否为null
	 * 
	 * @param obj
	 *            Object
	 * @return
	 */
	public boolean isNullObj(Object obj) {
		return (obj == null) ? true : false;
	}

	/**
	 * 获取人员同步方式 1：Excel 同步；2：DB数据同步；3：岗位匹配同步；4：xml数据同步
	 * 
	 * @return
	 */
	public String getImportDataType() {
		// 获取json字符串
		String json = "{id:" + JecnContants.otherUserSyncType + "}";
		outJsonString(json);
		return null;
	}

	public String getImportType() {
		return importType;
	}

	public void setImportType(String importType) {
		this.importType = importType;
	}

	public void setUserAutoBuss(UserAutoBuss userAutoBuss) {
		this.userAutoBuss = userAutoBuss;
	}

	public String getImportValue() {
		return importValue;
	}

	public void setImportValue(String importValue) {
		this.importValue = importValue;
	}
}
