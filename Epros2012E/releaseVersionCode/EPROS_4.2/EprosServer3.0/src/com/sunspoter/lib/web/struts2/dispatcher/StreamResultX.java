package com.sunspoter.lib.web.struts2.dispatcher;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.dispatcher.StreamResult;

import com.opensymphony.xwork2.ActionInvocation;

/**
 * 
 * 为了解决IE下载文件弹出下载界面后点击取消是，后台报错误 [ERROR][2014-10-29 18:14:03] Exception occurred
 * during processing request: null ClientAbortException: java.io.IOException at
 * org.apache.catalina.connector.OutputBuffer.doFlush(OutputBuffer.java:330)问题
 * 
 * @author zhouxy
 * 
 */
public class StreamResultX extends StreamResult {

	protected void doExecute(String finalLocation, ActionInvocation invocation)
			throws Exception {
		resolveParamsFromStack(invocation.getStack(), invocation);

		OutputStream oOutput = null;
		try {
			if (this.inputStream == null) {
				this.inputStream = ((InputStream) invocation
						.getStack()
						.findValue(conditionalParse(this.inputName, invocation)));
			}

			if (this.inputStream == null) {
				String msg = "Can not find a java.io.InputStream with the name ["
						+ this.inputName
						+ "] in the invocation stack. "
						+ "Check the <param name=\"inputName\"> tag specified for this action.";
				
				try{//打印对应的后台action
					LOG.error("jecn:"+invocation.getProxy().getActionName());
				}catch(Exception e){
					LOG.error("", e);
				}
				
				LOG.error(msg, new String[0]);
				throw new IllegalArgumentException(msg);
			}

			HttpServletResponse oResponse = (HttpServletResponse) invocation
					.getInvocationContext()
					.get(
							"com.opensymphony.xwork2.dispatcher.HttpServletResponse");

			if ((this.contentCharSet != null)
					&& (!this.contentCharSet.equals(""))) {
				oResponse.setContentType(conditionalParse(this.contentType,
						invocation)
						+ ";charset=" + this.contentCharSet);
			} else {
				oResponse.setContentType(conditionalParse(this.contentType,
						invocation));
			}

			if (this.contentLength != null) {
				String _contentLength = conditionalParse(this.contentLength,
						invocation);
				int _contentLengthAsInt = -1;
				try {
					_contentLengthAsInt = Integer.parseInt(_contentLength);
					if (_contentLengthAsInt >= 0)
						oResponse.setContentLength(_contentLengthAsInt);
				} catch (NumberFormatException e) {
					if (LOG.isWarnEnabled()) {
						LOG
								.warn(
										"failed to recongnize "
												+ _contentLength
												+ " as a number, contentLength header will not be set",
										e, new String[0]);
					}
				}

			}

			if (this.contentDisposition != null) {
				oResponse.addHeader("Content-Disposition", conditionalParse(
						this.contentDisposition, invocation));
			}

			if (!this.allowCaching) {
				oResponse.addHeader("Pragma", "no-cache");
				oResponse.addHeader("Cache-Control", "no-cache");
			}

			oOutput = oResponse.getOutputStream();

			if (LOG.isDebugEnabled()) {
				LOG.debug("Streaming result [" + this.inputName + "] type=["
						+ this.contentType + "] length=[" + this.contentLength
						+ "] content-disposition=[" + this.contentDisposition
						+ "] charset=[" + this.contentCharSet + "]",
						new String[0]);
			}

			if (LOG.isDebugEnabled()) {
				LOG.debug("Streaming to output buffer +++ START +++",
						new String[0]);
			}

			try {
				byte[] oBuff = new byte[this.bufferSize];
				int iSize;
				while (-1 != (iSize = this.inputStream.read(oBuff))) {
					oOutput.write(oBuff, 0, iSize);
				}
				if (LOG.isDebugEnabled()) {
					LOG.debug("Streaming to output buffer +++ END +++",
							new String[0]);
				}

				oOutput.flush();
			} catch (IOException e) {
				LOG.warn("下载界面中点击取消后报错 ", e, new String[0]);
				if (this.inputStream != null) {
					try {
						this.inputStream.close();
					} catch (IOException ex) {
						LOG.error("inputStream关闭输入流异常", e);
					}
				}

				if (oOutput != null) {
					try {
						oOutput.close();
					} catch (IOException ex) {
						oOutput=null;
					}
				}
			}
		} finally {
			if (this.inputStream != null)
				this.inputStream.close();
			if (oOutput != null)
				oOutput.close();
		}
	}
}