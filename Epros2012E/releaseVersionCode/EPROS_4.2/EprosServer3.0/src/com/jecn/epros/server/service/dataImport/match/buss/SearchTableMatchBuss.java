package com.jecn.epros.server.service.dataImport.match.buss;

import java.util.List;

import com.jecn.epros.server.dao.dataImport.match.ISearchTableMatchDao;

public class SearchTableMatchBuss {
	private ISearchTableMatchDao searchTableMatchDao;

	public String getStationSearchResult(String name, Long projectId, int state)
			throws Exception {
		StringBuffer sqlBuf = new StringBuffer();
		if (state == 11) {
			sqlBuf
					.append("select b.actPosNum,b.actPosName,a.deptName from ActualDeptBean as a,ActualPosBean as b where ");
			sqlBuf
					.append(" b.actPosName like ? and  b.deptNum = a.deptNum and a.projectId="
							+ projectId);
		} else if (state == 12) {
			sqlBuf
					.append("select basePosNum,basePosName,0,0 from ActualPosBean where ");
			sqlBuf
					.append(" basePosName like ? group by basePosNum,basePosName");
		}
		String arg = "%" + name + "%";
		List<Object[]> list = searchTableMatchDao.getSqlResut(
				sqlBuf.toString(), arg);
		StringBuffer strBuf = new StringBuffer();
		int size = list.size();
		for (int i = 0; i < size; i++) {
			Object[] objects = (Object[]) list.get(i);
			String figureId = "";
			if (objects[0] != null) {
				figureId = objects[0].toString();
			}
			strBuf.append(figureId + "&");
			String stationName = "";
			if (objects[1] != null) {
				stationName = objects[1].toString();
			}
			strBuf.append(stationName + "&");
			String orgName = "";
			if (objects[2] != null) {
				orgName = objects[2].toString();
			}
			strBuf.append(orgName);
			if (i != (size - 1)) {
				strBuf.append("##");
			}
		}
		return strBuf.toString();
	}

	public ISearchTableMatchDao getISearchTableMatchDao() {
		return searchTableMatchDao;
	}

	public ISearchTableMatchDao getSearchTableMatchDao() {
		return searchTableMatchDao;
	}

	public void setSearchTableMatchDao(ISearchTableMatchDao searchTableMatchDao) {
		this.searchTableMatchDao = searchTableMatchDao;
	}
}
