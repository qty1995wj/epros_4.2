package epros.designer.gui.popedom.organization;

import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnRecycleManageDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 组织回收面板
 * 
 * @author 2012-08-08
 * 
 */
public class OrgRecycleDialog extends JecnRecycleManageDialog {

	private static Logger log = Logger.getLogger(OrgRecycleDialog.class);

	@Override
	public boolean deleteData(List<Long> ListIds, Long updatePeopleId) {
		try {
			this.deleteIds = ConnectionPool.getOrganizationAction()
					.deleteIdsOrg(ListIds, updatePeopleId);
		} catch (Exception e) {
			log.error("OrgRecycleDialog deleteData is error", e);
			return false;
		}
		return true;
	}

	@Override
	public Object[][] getTableData() {
		try {
			List<Object[]> list = ConnectionPool.getOrganizationAction()
					.getDelsOrg(JecnConstants.projectId);
			Object[][] obj = new Object[list.size()][6];
			for (int i = 0; i < list.size(); i++) {
				Object[] objects = list.get(i);
				if (objects == null || objects[0] == null || objects[1] == null
						|| objects[2] == null) {
					continue;
				}
				obj[i][0] = new JCheckBox();
				obj[i][1] = 2;
				obj[i][2] = objects[0].toString();
				obj[i][3] = objects[1].toString();
				obj[i][4] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.organization.toString() + ".gif");
				// 父ID
				obj[i][5] = objects[2].toString();
			}
			return obj;
		} catch (Exception e) {
			log.error("OrgRecycleDialog getTableData is error", e);
		}
		return null;
	}

	@Override
	public String getTableType() {
		return JecnProperties.getValue("orgName");
	}

	@Override
	public boolean isViewPathButton() {
		return true;
	}

	@Override
	public boolean recyData(List<Long> ids) {
		try {
			ConnectionPool.getOrganizationAction().updateRecoverDelOrg(ids);
		} catch (Exception e) {
			log.error("OrgRecycleDialog recyData is error", e);
			return false;
		}
		return true;
	}

	@Override
	public String getRecycleTitle() {
		return JecnProperties.getValue("orgRecover");
	}

	@Override
	public void searchButAction() {

	}

}
