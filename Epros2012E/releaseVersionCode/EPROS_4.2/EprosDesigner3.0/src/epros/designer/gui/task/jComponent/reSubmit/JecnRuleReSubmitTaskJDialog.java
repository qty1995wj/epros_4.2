package epros.designer.gui.task.jComponent.reSubmit;

import epros.designer.gui.task.jComponent.JecnRuleTaskJDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;

/**
 * 制度任务窗口
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 26, 2072 时间：10:05:29 PM
 */
public class JecnRuleReSubmitTaskJDialog extends JecnRuleTaskJDialog {

	public JecnRuleReSubmitTaskJDialog(JecnTree jTree, JecnTreeNode treeNode, int approveType) {
		super(jTree, treeNode, approveType);
	}
}
