package epros.draw.event;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

import jecntool.cmd.JecnCmd;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.toolbar.io.ImportFlowFilter;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.JecnEprosVersion;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏帮助动作处理类
 * 
 * @author Administrator
 * 
 */
public class JecnHelpActionProcess extends JecnAbstractActionProcess {
	protected void actionPerformed(ActionEvent paramActionEvent, ToolBarElemType toolBarElemType) {
		if (toolBarElemType == null) {
			return;
		}
		switch (toolBarElemType) {
		case helpAbout: // 关于
			helpAboutActionPerformed(paramActionEvent);
			break;
		case helpUserManual: // 用户手册
			helpUserManualActionPerformed(paramActionEvent);
			break;
		case helpFindJecn:// 联系我们
			findJecnActionPerformed(paramActionEvent);
			break;
		}
	}

	/**
	 * 
	 * 关于处理
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void helpAboutActionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnToolbarContentButton) {
			JecnToolbarContentButton helpBtn = (JecnToolbarContentButton) e.getSource();
			// 关于对话框
			JecnDialog dialog = new JecnDialog();

			// 图片
			ImageIcon icon = JecnFileUtil.getImageIconforPath(JecnEprosVersion.getEprosVersion().getHelpAboutIcon());

			// 显示图片的组件
			JLabel label = new JLabel();
			label.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			label.setBorder(null);
			label.setIcon(icon);

			// 模态
			dialog.setModal(true);
			dialog.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);
			// 大小
			dialog.setSize(icon.getIconWidth() + 6, icon.getIconHeight() + 3);
			// 依存组件即在哪个组件上居中
			dialog.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());
			// 不可拖动
			dialog.setResizable(false);
			dialog.setTitle(helpBtn.getText());

			dialog.getContentPane().add(label);
			dialog.setVisible(true);
		}
	}

	/**
	 * 
	 * 联系我们
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void findJecnActionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnToolbarContentButton) {
			JecnToolbarContentButton findJecn = (JecnToolbarContentButton) e.getSource();
			// 公司字体
			int comSize = 14;
			// 电话和网址字体
			int httpSize = 12;
			// 左间距
			int left = 32;
			// 电话
			String phone = JecnResourceUtil.getJecnResourceUtil().getValue("helpFindJecnComName");
			// 公司网址标题
			String jecnHttpTitle = JecnResourceUtil.getJecnResourceUtil().getValue("helpFindJecnHttpName");
			// 公司网址
			String jecnHttp = JecnResourceUtil.getJecnResourceUtil().getValue("helpFindJecnHttpCont");
			// 图片
			ImageIcon icon = JecnFileUtil.getImageIconforPath("/epros/draw/images/epros/findJecn.jpg");

			// 关于对话框
			JecnDialog dialog = new JecnDialog();

			// 容器
			JLabel mainLabel = new JLabel();
			// 公司名称
			JLabel comLabel = new JLabel();
			// 电话
			JLabel phoneLabel = new JLabel();
			// 网址
			JLabel httpLabel = new JLabel();
			// 公共微信号
			JLabel wxLabel = new JLabel();
			// 图片
			mainLabel.setIcon(icon);
			mainLabel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			mainLabel.setLayout(new GridBagLayout());

			comLabel.setFont(new Font(Font.DIALOG, Font.BOLD, comSize));
			phoneLabel.setFont(new Font(Font.DIALOG, Font.PLAIN, httpSize));
			wxLabel.setFont(new Font(Font.DIALOG, Font.PLAIN, httpSize));
			httpLabel.setFont(new Font(Font.DIALOG, Font.PLAIN, httpSize));

			comLabel.setText(phone);
			phoneLabel.setText("<html> <body style='font-size:10px;'>"
					+ JecnResourceUtil.getJecnResourceUtil().getValue("helpFindJecnPhoneName") + "</body></html>");
			String info = "<html> <body style='font-size:10px;'>" + jecnHttpTitle + "<a href='" + jecnHttp + "'>"
					+ jecnHttp + "</a></body></html>";
			httpLabel.setText(info);

			wxLabel.setText("<html> <body style='font-size:10px;'>"
					+ JecnResourceUtil.getJecnResourceUtil().getValue("helpFindJecnWX") + "</body></html>");
			Insets inset = new Insets(comSize, left, httpSize - 2, left);
			Insets inset1 = new Insets(0, left, 0, left);
			Insets inset2 = new Insets(0, left, comSize, left);

			// 空闲区域
			GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.BOTH, JecnUIUtil.getInsets0(), 0, 0);
			mainLabel.add(new JLabel(), c);
			// 公司名称
			c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, inset, 0, 0);
			mainLabel.add(comLabel, c);

			// 电话
			c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, inset1, 0, 0);
			mainLabel.add(phoneLabel, c);
			// 微信
			c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, inset1, 0, 0);
			mainLabel.add(wxLabel, c);
			// 网址
			c = new GridBagConstraints(0, 4, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, inset2, 0, 0);
			mainLabel.add(httpLabel, c);

			// 空闲区域
			c = new GridBagConstraints(0, 5, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
					JecnUIUtil.getInsets0(), 0, 0);
			mainLabel.add(new JLabel(), c);

			// 点击网址浏览器打开
			httpLabel.addMouseListener(new MouseAdapter() {
				public void mouseEntered(MouseEvent e) {
					JLabel label = (JLabel) e.getSource();
					label.setCursor(new Cursor(Cursor.HAND_CURSOR));
				}

				public void mouseExited(MouseEvent e) {
					JLabel label = (JLabel) e.getSource();
					label.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}

				public void mouseReleased(MouseEvent e) {
					JecnCmd.openExplorer(JecnResourceUtil.getJecnResourceUtil().getValue("helpFindJecnHttpCont"));
				}
			});

			// 模态
			dialog.setModal(true);
			dialog.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);
			// 大小
			dialog.setSize(250, 140);
			// 依存组件即在哪个组件上居中
			dialog.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());
			// 不可拖动
			dialog.setResizable(false);
			dialog.setTitle(findJecn.getText());

			dialog.getContentPane().add(mainLabel);
			dialog.setVisible(true);
		}
	}

	/**
	 * 
	 * 用户手册处理
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void helpUserManualActionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnToolbarContentButton) {
			JecnToolbarContentButton helpUserManualBtn = (JecnToolbarContentButton) e.getSource();
			// 文件路径
			String path = JecnFileUtil.getSyncPath("/epros/draw/configFile/toolbar/help/helpUserManual.doc");
			if (DrawCommon.isNullOrEmtryTrim(path)) {
				return;
			}
			File file = new File(path);
			// 判断文件是否存在
			if (!file.exists() || !file.isFile()) {
				return;
			}

			// 文件选择器
			JecnFileChooser fileChooser = new JecnFileChooser();
			// 移除系统给定的文件过滤器
			fileChooser.setAcceptAllFileFilterUsed(false);
			// 标题
			fileChooser.setDialogTitle(helpUserManualBtn.getText());
			// 过滤文件后缀
			fileChooser.setFileFilter(new ImportFlowFilter("doc"));
			// 设置按钮为打开
			fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
			// 选中模式：只能选中目录
			fileChooser.setSelectedFile(new File("helpUserManual.doc"));

			// 显示文件选择器
			int isOk = fileChooser.showSaveDialog(JecnDrawMainPanel.getMainPanel());

			// 选择确认（yes、ok）后返回该值。
			if (isOk == JFileChooser.APPROVE_OPTION) {
				File saveFile = fileChooser.getSelectedFile();
				String newPath = saveFile.getPath();
				if (DrawCommon.isNullOrEmtryTrim(newPath)) {
					return;
				}
				if (!saveFile.isFile() && !newPath.endsWith("doc")) {// 如果不是文件添加后缀名称
					newPath += ".doc";
				}

				// 成功:true，失败:false
				JecnFileUtil.writeFileToOPLocal(file, newPath);
			}
		}
	}
}
