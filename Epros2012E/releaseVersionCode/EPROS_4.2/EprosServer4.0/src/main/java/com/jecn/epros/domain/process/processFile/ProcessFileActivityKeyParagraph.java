package com.jecn.epros.domain.process.processFile;

import java.util.ArrayList;
import java.util.List;

/**
 * 关键活动段落
 *
 * @author ZXH
 */
public class ProcessFileActivityKeyParagraph extends BaseProcessFileBean {

    private List<ProcessFileActivityKeyTable> contents = new ArrayList<ProcessFileActivityKeyTable>();

    public List<ProcessFileActivityKeyTable> getContents() {
        return contents;
    }

    public void setContents(List<ProcessFileActivityKeyTable> contents) {
        this.contents = contents;
    }
}
