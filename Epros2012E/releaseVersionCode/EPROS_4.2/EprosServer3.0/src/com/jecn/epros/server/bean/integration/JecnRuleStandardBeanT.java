package com.jecn.epros.server.bean.integration;

import java.io.Serializable;

/**
 * 制度标准关联表
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-21 时间：下午02:03:13
 */
public class JecnRuleStandardBeanT implements Serializable {
	/** ID */
	private String id;
	/** 制度主键ID */
	private Long ruleId;
	/** 标准主键ID */
	private Long standardId;
	/** 关联类型 '1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求'; */
	private int relaType;
	/** 条款要求对应的条款ID（当关联条款时存在） */
	private Long clauseId;
	/** 标准名称 */
	private String standardName;
	/** 关联ID 类型为1时为文件ID */
	private Long relationId;

	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public Long getStandardId() {
		return standardId;
	}

	public void setStandardId(Long standardId) {
		this.standardId = standardId;
	}

	public int getRelaType() {
		return relaType;
	}

	public void setRelaType(int relaType) {
		this.relaType = relaType;
	}

	public Long getClauseId() {
		return clauseId;
	}

	public void setClauseId(Long clauseId) {
		this.clauseId = clauseId;
	}
}
