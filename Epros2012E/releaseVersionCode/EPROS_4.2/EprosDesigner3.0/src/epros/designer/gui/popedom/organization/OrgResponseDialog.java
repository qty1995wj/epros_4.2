package epros.designer.gui.popedom.organization;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/***
 * 组织职责 
 * @author 2012-06-08
 *
 */
public class OrgResponseDialog extends JecnDialog {
	
	private static Logger log = Logger.getLogger(OrgResponseDialog.class);
	
	/** 主面板*/
	private JecnPanel mainPanel = new JecnPanel(); //450,350
	
	/** 信息面板*/
	private JecnPanel infoPanel = new JecnPanel();//430,240
	
	/** 按钮面板*/
	private JecnPanel buttonPanel = new JecnPanel(450,50);//450,30
	/**编号Lab*/
	private JLabel numLab = new JLabel(JecnProperties.getValue("actBaseNumC"));
	
	/***编号填写框*/
	private JTextField numTextField = new JTextField();
	
	/** 组织职责Lab*/
	private JLabel resposeLab = new JLabel(JecnProperties.getValue("orgResponseC"));
	
	/** 组织职责Area*/
	private JecnTextArea resposeArea = new JecnTextArea();//400,180
	/** 组织职责滚动面板 */
	private JScrollPane resposeScrollPane = new JScrollPane(
			resposeArea);

	
	/** 组织职责验证提示信息*/
	private JLabel responseVerfy = new JLabel();
	/**岗位编号必填项 *号提示*/
	private JLabel requiredNumLab = new JLabel(JecnProperties.getValue("required"));
	
	/** 确定*/
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	
	/** 取消*/
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	
	private JecnTreeNode selectNode = null;
	/***判断 点击取消 是否提示关闭按钮*/
	private boolean isUpdate = false;
	//根据组织ID获取组织信息
	Long orgId = null;
	private JecnFlowOrg jecnFlowOrg = null;
	
	public OrgResponseDialog(JecnTreeNode selectNode){
		this.selectNode = selectNode;
		this.setModal(true);
		this.setSize(450,350);
		this.setLocationRelativeTo(null);
		
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		
		// 组织职责
		resposeArea.setBorder(null);
		resposeScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		resposeScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		resposeScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.setTitle(JecnProperties.getValue("orgDuty"));
		//换行
		resposeArea.setLineWrap(true);
		
		//根据组织ID获取组织信息
		orgId = selectNode.getJecnTreeBean().getId();
		try {
			jecnFlowOrg = ConnectionPool.getOrganizationAction().getOrgInfo(orgId);
			this.numTextField.setText(jecnFlowOrg.getOrgNumber());
			this.resposeArea.setText(jecnFlowOrg.getOrgRespon());
		} catch (Exception e1) {
			log.error("OrgResponseDialog OrgResponseDialog is error",e1);
		}
		
		initLayout();
		requiredNumLab.setForeground(Color.red);
		responseVerfy.setForeground(Color.red);
		
		//取消
		cancelBut.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				cancelButPerformed();
			}
		});
		// 确定
		okBut.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				okButPerformed();
			}
		});
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				if (checkUpdate()&&submit()) {
					// 关闭窗体
					return true;
				}
				return false;
			}
		});
	}
	/***
	 * 布局
	 */
	private void initLayout(){
		// 主面板
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 8, 5, 8);
		//infoPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		//组织编号
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(numLab, c);
		//组织职责显示框
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		infoPanel.add(numTextField, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 1), 0,
				0);
		infoPanel.add(requiredNumLab, c);
		
		//组织职责名称
		
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(resposeLab, c);
		//组织职责显示框
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(resposeScrollPane, c);
//		//组织职责验证提示信息
//		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
//				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0,
//				0);
//		infoPanel.add(responseVerfy, c);
		//按钮
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0,
				0);
		mainPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(responseVerfy);
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		
		this.getContentPane().add(mainPanel);
	}
	private boolean checkUpdate(){
		// 名称
		if(jecnFlowOrg.getOrgRespon() == null || "".equals(jecnFlowOrg.getOrgRespon())){
			jecnFlowOrg.setOrgRespon(null);
			if(this.resposeArea.getText()!= null && !"".equals(this.resposeArea.getText())){
				isUpdate = true;
			}
		}else if (!jecnFlowOrg.getOrgRespon().equals(
				this.resposeArea.getText().toString())) {
			isUpdate = true;
		}
		//编号
		if(jecnFlowOrg.getOrgNumber() == null || "".equals(jecnFlowOrg.getOrgNumber())){
			jecnFlowOrg.setOrgNumber(null);
			if(this.numTextField.getText()!= null && !"".equals(this.numTextField.getText())){
				isUpdate = true;
			}
		}else if (!jecnFlowOrg.getOrgNumber().equals(
				this.numTextField.getText().toString())) {
			isUpdate = true;
		}
		return true;
	}

	/***
	 * 取消
	 */
	private void cancelButPerformed() {
		if (checkUpdate() && submit()) {
			// 关闭窗体
			this.dispose();
		}
	}

	private boolean submit() {
		int optionTig = this.dialogCloseBeforeMsgTipAction();
		if (optionTig == 2) {// 是
			okButPerformed();
			return false;
		} else if (optionTig == 1) {// 否
			return true;
		} else {// 取消
			return false;
		}
	}
	public boolean validateName(String nameLab,String name, JLabel jLable){
//		return JecnValidateCommon.validateNameNoRestrict(name, promptLab);
		String s="";
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			s= JecnUserCheckInfoData.getNameNotNull();
		} else if (JecnUserCheckUtil.getTextLength(name) > 122) {
			// 名称不能超过122个字符或61个汉字
			s= JecnUserCheckInfoData.getNameLengthInfo();
		}
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			jLable.setText(nameLab+s);
			return false;
		}
		jLable.setText("");
		return true;
	}
	/**
	 * 确定
	 */
	private void okButPerformed(){
		//组织编号验证
		String num = numTextField.getText();
		//组织职责
		String orgResponse = this.resposeArea.getText();
		if (!validateName(JecnProperties.getValue("actBaseNum"),num, responseVerfy)) {
			return;
		}
		//验证编号全表唯一
		try {
			
			if(num!=null&&"0".equals(num.trim())){
				responseVerfy.setText(JecnProperties.getValue("orgNumber"));
	       		return;
			}
			
			if(ConnectionPool.getOrganizationAction().validateAddUpdateOrgNum(num, orgId)){
				responseVerfy.setText(JecnProperties.getValue("posNumHaved"));
				return;
			}else{
				responseVerfy.setText("");
			}
			
		} catch (Exception e) {
			log.error("OrgResponseDialog okButPerformed is error",e);
		}
		String s = JecnUserCheckUtil.checkNoteLength(orgResponse);
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			responseVerfy.setText(JecnProperties.getValue("orgResponse")+s);
			return;
		}
		responseVerfy.setText("");
		//更新组织职责
		try {
			ConnectionPool.getOrganizationAction().updateOrgDuty(orgId, orgResponse,num);
			this.dispose();
		} catch (Exception e2) {
			log.error("OrgResponseDialog okButPerformed is error",e2);
		}
	}
	
}
