package epros.designer.gui.error;

/**
 * 获取任务配置流程图属性规范 记录流程图属性
 * 
 * @author ZHANGXH
 * @date： 日期：Oct 19, 2012 时间：3:23:07 PM
 */
public class ErrorTempBean {
	/** 角色上限 */
	private int roleNumberMax = 0;
	/** 角色下限 */
	private int roleNumberMin = 0;
	/** 活动上限 */
	private int activityNumberMax = 0;
	/** 活动下限 */
	private int activityNumberMin = 0;
	/** 正反返工符数量对应 */
	private int reworkNumber = 0;
	/** 主责岗位唯一 */
	private int mainPosition = 0;
	/** 图形是否存在连接线 */
	private int figureExistLine = 0;
	/** 决策框是否存在连接线 */
	private int rhombusExistLine = 0;
	/** 角色活动是否对应 */
	private int existRoleAndActivites = 0;
	/** 活动编号 */
	private int activityNumber = 0;
	/** 活动名称 */
	private int activityName = 0;
	/** 活动输入 */
	private int activityInput = 0;
	/** 活动输出 */
	private int activityOutPut = 0;
	/** 活动说明 */
	private int activityShow = 0;
	/** 关键说明 */
	private int keyActiveShow = 0;
	/** 角色职责 */
	private int roleResponsibilities = 0;
	/** 角色是否存在岗位 */
	private int roleExistPosition = 0;
	/** 客户是否必须选择岗位 */
	private int customMustChosePost;

	
	
	public int getActivityInput() {
		return activityInput;
	}

	public void setActivityInput(int activityInput) {
		this.activityInput = activityInput;
	}

	public int getActivityOutPut() {
		return activityOutPut;
	}

	public void setActivityOutPut(int activityOutPut) {
		this.activityOutPut = activityOutPut;
	}

	public int getActivityName() {
		return activityName;
	}

	public void setActivityName(int activityName) {
		this.activityName = activityName;
	}

	public int getRoleNumberMax() {
		return roleNumberMax;
	}

	public void setRoleNumberMax(int roleNumberMax) {
		this.roleNumberMax = roleNumberMax;
	}

	public int getRoleNumberMin() {
		return roleNumberMin;
	}

	public void setRoleNumberMin(int roleNumberMin) {
		this.roleNumberMin = roleNumberMin;
	}

	public int getActivityNumberMax() {
		return activityNumberMax;
	}

	public void setActivityNumberMax(int activityNumberMax) {
		this.activityNumberMax = activityNumberMax;
	}

	public int getActivityNumberMin() {
		return activityNumberMin;
	}

	public void setActivityNumberMin(int activityNumberMin) {
		this.activityNumberMin = activityNumberMin;
	}

	public int getReworkNumber() {
		return reworkNumber;
	}

	public void setReworkNumber(int reworkNumber) {
		this.reworkNumber = reworkNumber;
	}

	public int getMainPosition() {
		return mainPosition;
	}

	public void setMainPosition(int mainPosition) {
		this.mainPosition = mainPosition;
	}

	public int getFigureExistLine() {
		return figureExistLine;
	}

	public void setFigureExistLine(int figureExistLine) {
		this.figureExistLine = figureExistLine;
	}

	public int getRhombusExistLine() {
		return rhombusExistLine;
	}

	public void setRhombusExistLine(int rhombusExistLine) {
		this.rhombusExistLine = rhombusExistLine;
	}

	public int getExistRoleAndActivites() {
		return existRoleAndActivites;
	}

	public void setExistRoleAndActivites(int existRoleAndActivites) {
		this.existRoleAndActivites = existRoleAndActivites;
	}

	public int getActivityNumber() {
		return activityNumber;
	}

	public void setActivityNumber(int activityNumber) {
		this.activityNumber = activityNumber;
	}

	public int getActivityShow() {
		return activityShow;
	}

	public void setActivityShow(int activityShow) {
		this.activityShow = activityShow;
	}

	public int getKeyActiveShow() {
		return keyActiveShow;
	}

	public void setKeyActiveShow(int keyActiveShow) {
		this.keyActiveShow = keyActiveShow;
	}

	public int getRoleResponsibilities() {
		return roleResponsibilities;
	}

	public void setRoleResponsibilities(int roleResponsibilities) {
		this.roleResponsibilities = roleResponsibilities;
	}

	public int getRoleExistPosition() {
		return roleExistPosition;
	}

	public void setRoleExistPosition(int roleExistPosition) {
		this.roleExistPosition = roleExistPosition;
	}

	public int getCustomMustChosePost() {
		return customMustChosePost;
	}

	public void setCustomMustChosePost(int customMustChosePost) {
		this.customMustChosePost = customMustChosePost;
	}
}
