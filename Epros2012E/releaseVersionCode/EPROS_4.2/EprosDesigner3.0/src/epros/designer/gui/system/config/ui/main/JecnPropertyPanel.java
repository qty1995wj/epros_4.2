package epros.designer.gui.system.config.ui.main;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.border.BevelBorder;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.JecnAbstractJPanel;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnFileConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnFlowConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnPubSetConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnRuleConfigJDialog;
import epros.designer.gui.system.config.ui.dialog.JecnTotalMapConfigJDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.type.JecnBasicInfoPanel;
import epros.designer.gui.system.config.ui.property.type.JecnBasicInfoPubPanel;
import epros.designer.gui.system.config.ui.property.type.JecnBasicInfoRulePanel;
import epros.designer.gui.system.config.ui.property.type.JecnDocumentControlPanel;
import epros.designer.gui.system.config.ui.property.type.JecnDownLoadCountPanel;
import epros.designer.gui.system.config.ui.property.type.JecnFileBasicInfoPanel;
import epros.designer.gui.system.config.ui.property.type.JecnFlowBasicItemPanel;
import epros.designer.gui.system.config.ui.property.type.JecnFlowFilePanel;
import epros.designer.gui.system.config.ui.property.type.JecnFlowKPIPanel;
import epros.designer.gui.system.config.ui.property.type.JecnFlowMapBasicInfoPanel;
import epros.designer.gui.system.config.ui.property.type.JecnFlowMapOtherConfigPanel;
import epros.designer.gui.system.config.ui.property.type.JecnFlowMapPublicItemPanel;
import epros.designer.gui.system.config.ui.property.type.JecnFlowPublicItemPanel;
import epros.designer.gui.system.config.ui.property.type.JecnFlowRuleTypePanel;
import epros.designer.gui.system.config.ui.property.type.JecnMailPanel;
import epros.designer.gui.system.config.ui.property.type.JecnMesEmailSetPanel;
import epros.designer.gui.system.config.ui.property.type.JecnPartMapStandedPanel;
import epros.designer.gui.system.config.ui.property.type.JecnPubPanel;
import epros.designer.gui.system.config.ui.property.type.JecnPublicPointPeoplePanel;
import epros.designer.gui.system.config.ui.property.type.JecnRationProPanel;
import epros.designer.gui.system.config.ui.property.type.JecnRuleBasicItemPanel;
import epros.designer.gui.system.config.ui.property.type.JecnRulePublicItemPanel;
import epros.designer.gui.system.config.ui.property.type.JecnSystemBasicItemPanel;
import epros.designer.gui.system.config.ui.property.type.JecnTaskApprPanel;
import epros.designer.gui.system.config.ui.property.type.JecnTaskOperatePanel;
import epros.designer.gui.system.config.ui.property.type.JecnTaskShowPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 属性面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnPropertyPanel extends JecnAbstractJPanel {
	/** 基本信息 */
	private JecnAbstractPropertyBasePanel basicInfoPanel = null;
	/** 流程建设规范 */
	private JecnAbstractPropertyBasePanel partMapStandedPanel = null;
	/** 任务审批环节配置界面 */
	private JecnAbstractPropertyBasePanel taskApprPanel = null;
	/** 流程文件面板 */
	private JecnAbstractPropertyBasePanel flowFilePanel = null;
	/** 任务界面显示项面板 */
	private JecnAbstractPropertyBasePanel taskShowPanel = null;
	/** 邮箱配置 */
	private JecnAbstractPropertyBasePanel mailPanel = null;
	/** 权限配置 */
	private JecnAbstractPropertyBasePanel pubPanel = null;
	/** 任务操作配置 */
	private JecnAbstractPropertyBasePanel taskOperatePanel = null;
	/** 消息邮件配置 */
	private JecnAbstractPropertyBasePanel messageEmailPanel = null;
	/** 合理化建议配置 */
	private JecnAbstractPropertyBasePanel rationProPanel = null;
	/** 流程其它配置 */
	private JecnAbstractPropertyBasePanel flowBasicItemPanel = null;
	
	/** 发布流程、流程地图、制度；相关文件是否发布相关配置 */
	private JecnAbstractPropertyBasePanel flowPublicRelateFileItemPanel = null;

	/** 文件下载次数配置 */
	private JecnAbstractPropertyBasePanel downLoadCountPanel = null;

	/** 文控信息配置 */
	private JecnAbstractPropertyBasePanel documentControlPanel = null;

	/** JecnPropertyPanel是propertyContPanel的容器 */
	private JecnAbstractPropertyBasePanel selectedPropContPanel = null;
	/** 数据对象 */
	private JecnConfigTypeDesgBean configTypeDesgBean = null;

	/** 流程KPI配置面板 */
	private JecnAbstractPropertyBasePanel flowKPIAttributePanel = null;
	/** 流程架构-其它配置 */
	private JecnAbstractPropertyBasePanel otherAttributePanel = null;

	public JecnPropertyPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		initComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {

		this.setLayout(new BorderLayout());
		// 边框
		this.setBorder(new BevelBorder(BevelBorder.LOWERED, JecnUIUtil.getDefaultBackgroundColor(), Color.GRAY));
	}

	/**
	 * 
	 * 刷新主界面属性面板
	 * 
	 */
	public void reShowProperty() {
		// 重新刷新属性面板
		reSetProperty(configTypeDesgBean);
	}

	/**
	 * 
	 * 显示默认值
	 * 
	 */
	public void showDefaultProperty() {
		if (configTypeDesgBean != null) {
			// 默认值替换当前值
			configTypeDesgBean.defaultDataToData();
		}
		// 重新刷新属性panel
		reShowProperty();
	}

	/**
	 * 
	 * 显示属性值 @
	 * 
	 */
	public void showProperty(JecnConfigTypeDesgBean configTypeDesgBean) {
		init(configTypeDesgBean);

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()
				&& configTypeDesgBean.getType() != 12 && configTypeDesgBean.getType() != 13) {
			return;
		}

		// 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置
		// 6：权限配置7:任务操作8：消息邮件配置
		switch (configTypeDesgBean.getType()) {
		case 0:// 0：审批环节配置
			addPropertyContPanel(taskApprPanel, configTypeDesgBean);
			break;
		case 1:// 1：任务界面显示项配置
			addPropertyContPanel(taskShowPanel, configTypeDesgBean);
			break;
		case 2:// 2：基本配置
			addPropertyContPanel(basicInfoPanel, configTypeDesgBean);
			break;
		case 3:// 3：流程图建设规范
			addPropertyContPanel(partMapStandedPanel, configTypeDesgBean);
			break;
		case 4:// 4：流程文件
			addPropertyContPanel(flowFilePanel, configTypeDesgBean);
			break;
		case 5:// 5：邮箱配置
			addPropertyContPanel(mailPanel, configTypeDesgBean);
			break;
		case 6:// 6：权限配置
			addPropertyContPanel(pubPanel, configTypeDesgBean);
			break;
		case 7:// 7：任务操作配置
			addPropertyContPanel(taskOperatePanel, configTypeDesgBean);
			break;
		case 8:// 8：消息邮件配置
			addPropertyContPanel(messageEmailPanel, configTypeDesgBean);
			break;
		case 9:// 合理化建议配置
			addPropertyContPanel(rationProPanel, configTypeDesgBean);
			break;
		case 10:// 流程属性
			addPropertyContPanel(flowBasicItemPanel, configTypeDesgBean);
			break;
		case 11:// 发布配置
			addPropertyContPanel(flowPublicRelateFileItemPanel, configTypeDesgBean);
			break;
		case 12:// 流程制度类型配置
		case 13:// 指定收件人
			addPropertyContPanel(null, configTypeDesgBean);
			dialog.getOkCancelJButtonPanel().getOkJButton().setEnabled(true);
			break;
		case 14:// 文件下载次数配置
			addPropertyContPanel(downLoadCountPanel, configTypeDesgBean);
			break;
		case 15:// 文控信息配置
			addPropertyContPanel(documentControlPanel, configTypeDesgBean);
			break;
		case 16:// 流程KPI配置
			addPropertyContPanel(flowKPIAttributePanel, configTypeDesgBean);
			break;
		case 18: // 流程架构-其它配置
			addPropertyContPanel(otherAttributePanel, configTypeDesgBean);
			break;
		}

		// 整个数据校验
		check();

		this.revalidate();
		this.repaint();
	}

	/**
	 * 
	 * 重置属性值
	 * 
	 * 
	 */
	private void reSetProperty(JecnConfigTypeDesgBean configTypeDesgBean) {

		init(configTypeDesgBean);

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}

		// 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
		switch (configTypeDesgBean.getType()) {
		case 0:// 0：审批环节配置
			reSetPropertyContPanel(getTaskApprPanel(), configTypeDesgBean);
			break;
		case 1:// 1：任务界面显示项配置
			reSetPropertyContPanel(getTaskShowPanel(), configTypeDesgBean);
			break;
		case 2:// 2：基本配置
			reSetPropertyContPanel(getBasicInfoPanel(), configTypeDesgBean);
			break;
		case 3:// 3：流程图建设规范
			reSetPropertyContPanel(getPartMapStandedPanel(), configTypeDesgBean);
			break;
		case 4:// 4：流程文件
			reSetPropertyContPanel(getFlowFilePanel(), configTypeDesgBean);
			break;
		case 5:// 5：邮箱配置
			reSetPropertyContPanel(getMailPanel(), configTypeDesgBean);
			break;
		case 6:// 6：权限配置
			reSetPropertyContPanel(getPubPanel(), configTypeDesgBean);
			break;
		case 7:// 7：任务操作配置
			reSetPropertyContPanel(getTaskOperatePanel(), configTypeDesgBean);
			break;
		case 8:// 8：消息邮件配置
			reSetPropertyContPanel(getMessageEmailPanel(), configTypeDesgBean);
			break;
		case 9:// 合理化建议配置
			reSetPropertyContPanel(getRationProPanel(), configTypeDesgBean);
			break;
		case 10:// 流程属性
			reSetPropertyContPanel(getFlowBasicItemPanel(), configTypeDesgBean);
			break;
		case 11:// 发布流程、流程地图、制度；相关文件是否发布相关配置
			reSetPropertyContPanel(getFlowPublicItemPanel(), configTypeDesgBean);
			break;
		case 14:// 下载次数配置
			reSetPropertyContPanel(getDownLoadCountPanel(), configTypeDesgBean);
			break;
		case 15:// 文控信息配置
			reSetPropertyContPanel(getDocumentControlPanel(), configTypeDesgBean);
			break;
		case 16:// 流程KPI配置
			reSetPropertyContPanel(getFlowKPIPanel(), configTypeDesgBean);
			break;
		case 18:// 流程架构-其它配置
			reSetPropertyContPanel(getMapOtherConfigPanel(), configTypeDesgBean);
			break;
		}

		// 整个数据校验
		check();

		this.revalidate();
		this.repaint();
	}

	/**
	 * 
	 * 获取当前选中的属性面板，选中根节点或者没有选中时返回值NULL
	 * 
	 * @return JecnAbstractPropertyBasePanel 当前选中的属性面板或NULL
	 */
	public JecnAbstractPropertyBasePanel getSelectedPropContPanel() {
		return selectedPropContPanel;
	}

	/**
	 * 
	 * 校验数据是否合法 ，合法才能更新到数据库
	 * 
	 * @return boolean true：合法；false：不合法
	 */
	public boolean check() {

		// 0：审批环节配置;1：任务界面显示项配置;2：基本配置;3：流程图建设规范;4：流程文件;5：邮箱配置;6：权限配置

		// 基本信息
		if (basicInfoPanel != null) {
			if (!basicInfoPanel.check()) {
				return false;
			}
		}

		// // 流程建设规范
		// if (partMapStandedPanel != null) {
		// if (ret && partMapStandedPanel.check()) {
		// ret = true;
		// }
		// }

		// 任务审批环节
		if (taskApprPanel != null) {
			if (!taskApprPanel.check()) {
				return false;
			}
		}

		// // 流程文件面板
		// if (flowFilePanel != null) {
		// if (ret && flowFilePanel.check()) {
		// ret = true;
		// }
		// }

		// 任务界面显示项面板
		if (taskShowPanel != null) {
			if (!taskShowPanel.check()) {
				return false;
			}
		}

		// 5：邮箱配置
		if (mailPanel != null) {
			if (!mailPanel.check()) {
				return false;
			}
		}

		// // 6：权限配置
		// if (pubPanel != null) {
		// if (ret && pubPanel.check()) {
		// ret = true;
		// }
		// }

		// 6:消息邮件配置
		if (messageEmailPanel != null) {
			if (!messageEmailPanel.check()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * 显示前初始化
	 * 
	 * @param configTypeDesgBean
	 */
	private void init(JecnConfigTypeDesgBean configTypeDesgBean) {
		// 初始化
		this.removeAll();
		this.selectedPropContPanel = null;
		// 右面面板的按钮编辑情况
		dialog.getRightPanel().initBtnsEnabled(configTypeDesgBean);
		dialog.getOkCancelJButtonPanel().getInfoLabel().setText("");
		dialog.getOkCancelJButtonPanel().initBtnsEnabled(configTypeDesgBean);

		this.configTypeDesgBean = configTypeDesgBean;
	}

	/**
	 * 
	 * 添加属性内容面板且初始化数据
	 * 
	 * @param propertyContPanel
	 *            JecnAbstractPropertyBasePanel
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	private void addPropertyContPanel(JecnAbstractPropertyBasePanel propertyContPanel,
			JecnConfigTypeDesgBean configTypeDesgBean) {

		JecnAbstractPropertyBasePanel newPropertyContPanel = null;
		if (propertyContPanel == null && configTypeDesgBean.getType() != 12 && configTypeDesgBean.getType() != 13) {
			// 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置
			switch (configTypeDesgBean.getType()) {
			case 0:// 0：审批环节配置
				newPropertyContPanel = getTaskApprPanel();
				break;
			case 1:// 1：任务界面显示项配置
				newPropertyContPanel = getTaskShowPanel();
				break;
			case 2:// 2：基本配置
				newPropertyContPanel = getBasicInfoPanel();
				break;
			case 3:// 3：流程图建设规范
				newPropertyContPanel = getPartMapStandedPanel();
				break;
			case 4:// 4：流程文件
				newPropertyContPanel = getFlowFilePanel();
				break;
			case 5:// 5：邮箱配置
				newPropertyContPanel = getMailPanel();
				break;
			case 6:// 6：权限配置
				newPropertyContPanel = getPubPanel();
				break;
			case 7:// 7：任务操作配置
				newPropertyContPanel = getTaskOperatePanel();
				break;
			case 8:// 8：消息邮件配置
				newPropertyContPanel = getMessageEmailPanel();
				break;
			case 9:// 合理化建议配置
				newPropertyContPanel = getRationProPanel();
				break;
			case 10://其他配置
				newPropertyContPanel = getFlowBasicItemPanel();
				break;
			case 11:
				newPropertyContPanel = getFlowPublicItemPanel();
				break;
			case 14:// 下载次数配置
				newPropertyContPanel = getDownLoadCountPanel();
				break;
			case 15:// 文控信息配置
				newPropertyContPanel = getDocumentControlPanel();
				break;
			case 16:// 流程KPI
				newPropertyContPanel = getFlowKPIPanel();
				break;
			case 18:// 流程架构其它配置
				newPropertyContPanel = getMapOtherConfigPanel();
				break;
			default:
				// 当前指定的属性面板对象
				selectedPropContPanel = null;
				return;
			}
			// 初始化数据
			newPropertyContPanel.initData(configTypeDesgBean);
		} else {
			newPropertyContPanel = propertyContPanel;
		}
		if (configTypeDesgBean.getType() == 12) {// 流程制度类别配置
			// 添加组件到属性面板容器中
			this.add(getFlowRuleTypePanel(), BorderLayout.CENTER);
		} else if (configTypeDesgBean.getType() == 13) {// 指定收件人
			// 添加组件到属性面板容器中
			this.add(getPublicPointPeoplePanel(), BorderLayout.CENTER);
		} else {
			// 添加组件到属性面板容器中
			this.add(newPropertyContPanel, BorderLayout.CENTER);
		}

		// 当前指定的属性面板对象
		selectedPropContPanel = newPropertyContPanel;
	}

	/**
	 * 
	 * 重置属性值
	 * 
	 * @param propertyContPanel
	 *            JecnAbstractPropertyBasePanel
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	private void reSetPropertyContPanel(JecnAbstractPropertyBasePanel propertyContPanel,
			JecnConfigTypeDesgBean configTypeDesgBean) {
		// 初始化数据
		propertyContPanel.initData(configTypeDesgBean);
		// 添加组件到属性面板容器中
		this.add(propertyContPanel, BorderLayout.CENTER);
		// 当前指定的属性面板对象
		selectedPropContPanel = propertyContPanel;
	}

	/**
	 * 
	 * 基本信息
	 * 
	 * @return JecnAbstractPropertyBasePanel
	 */
	private JecnAbstractPropertyBasePanel getBasicInfoPanel() {
		if (basicInfoPanel == null) {
			if (dialog instanceof JecnRuleConfigJDialog) {// 制度 
				basicInfoPanel = new JecnBasicInfoRulePanel(dialog);
			} else if (dialog instanceof JecnPubSetConfigJDialog) {// 权限
				basicInfoPanel = new JecnBasicInfoPubPanel(dialog);
			} else if (dialog instanceof JecnFileConfigJDialog) {// 文件
				basicInfoPanel = new JecnFileBasicInfoPanel(dialog);
			} else if (dialog instanceof JecnTotalMapConfigJDialog) {// 文件
				basicInfoPanel = new JecnFlowMapBasicInfoPanel(dialog);
			} else {
				basicInfoPanel = new JecnBasicInfoPanel(dialog);
			}
		}
		return basicInfoPanel;
	}

	private JecnAbstractPropertyBasePanel getMapOtherConfigPanel() {
		if (otherAttributePanel == null) {
			otherAttributePanel = new JecnFlowMapOtherConfigPanel(dialog);
		}
		return otherAttributePanel;
	}

	/**
	 * 
	 * 流程建设规范
	 * 
	 * @return JecnAbstractPropertyBasePanel
	 */
	private JecnAbstractPropertyBasePanel getPartMapStandedPanel() {
		if (partMapStandedPanel == null) {
			partMapStandedPanel = new JecnPartMapStandedPanel(dialog);
		}
		return partMapStandedPanel;
	}

	/**
	 * 
	 * 任务审批环节配置界面
	 * 
	 * @return JecnAbstractPropertyBasePanel
	 */
	private JecnAbstractPropertyBasePanel getTaskApprPanel() {
		if (taskApprPanel == null) {
			taskApprPanel = new JecnTaskApprPanel(dialog);
		}
		return taskApprPanel;
	}

	/**
	 * 
	 * 流程文件面板
	 * 
	 * @return JecnAbstractPropertyBasePanel
	 */
	private JecnAbstractPropertyBasePanel getFlowFilePanel() {
		if (flowFilePanel == null) {
			flowFilePanel = new JecnFlowFilePanel(dialog);
		}
		return flowFilePanel;
	}

	/**
	 * 
	 * 任务界面显示项面板
	 * 
	 * @return JecnAbstractPropertyBasePanel
	 */
	private JecnAbstractPropertyBasePanel getTaskShowPanel() {
		if (taskShowPanel == null) {
			taskShowPanel = new JecnTaskShowPanel(dialog);
		}
		return taskShowPanel;
	}

	/**
	 * 
	 * 邮箱配置面板
	 * 
	 * @return JecnAbstractPropertyBasePanel
	 */
	private JecnAbstractPropertyBasePanel getMailPanel() {
		if (mailPanel == null) {
			mailPanel = new JecnMailPanel(dialog);
		}
		return mailPanel;
	}

	/**
	 * 
	 * 权限配置面板
	 * 
	 * @return JecnAbstractPropertyBasePanel
	 */
	private JecnAbstractPropertyBasePanel getPubPanel() {
		if (pubPanel == null) {
			pubPanel = new JecnPubPanel(dialog);
		}
		return pubPanel;
	}

	/***************************************************************************
	 * 任务操作配置面板
	 * 
	 * @return taskOperatePanel
	 */
	public JecnAbstractPropertyBasePanel getTaskOperatePanel() {
		if (taskOperatePanel == null) {
			taskOperatePanel = new JecnTaskOperatePanel(dialog);
		}
		return taskOperatePanel;
	}

	/***************************************************************************
	 * 消息邮件配置面板
	 * 
	 * @return messageEmailPanel
	 */
	public JecnAbstractPropertyBasePanel getMessageEmailPanel() {
		if (messageEmailPanel == null) {
			messageEmailPanel = new JecnMesEmailSetPanel(dialog);
		}
		return messageEmailPanel;
	}

	/**
	 * 合理化建议配置
	 * 
	 * @return rationProPanel
	 */
	public JecnAbstractPropertyBasePanel getRationProPanel() {
		if (rationProPanel == null) {
			rationProPanel = new JecnRationProPanel(dialog);
		}
		return rationProPanel;
	}

	/**
	 * 文件下载次数配置
	 * 
	 * @return downLoadCountPanel
	 */
	public JecnAbstractPropertyBasePanel getDownLoadCountPanel() {
		if (downLoadCountPanel == null) {
			downLoadCountPanel = new JecnDownLoadCountPanel(dialog);
		}
		return downLoadCountPanel;
	}

	public JecnAbstractPropertyBasePanel getFlowBasicItemPanel() {
		if (flowBasicItemPanel == null) {
			if (dialog instanceof JecnRuleConfigJDialog) {// 制度
				flowBasicItemPanel = new JecnRuleBasicItemPanel(dialog);
			}else if (dialog instanceof JecnPubSetConfigJDialog) {// 制度
				flowBasicItemPanel = new JecnSystemBasicItemPanel(dialog);
			}else{
				flowBasicItemPanel = new JecnFlowBasicItemPanel(dialog);
			}
		}
		return flowBasicItemPanel;
	}

	/**
	 * 获取配置对话框相关发布配置的 窗体
	 * 
	 * @return JecnAbstractPropertyBasePanel
	 */
	public JecnAbstractPropertyBasePanel getFlowPublicItemPanel() {
		if (flowPublicRelateFileItemPanel == null) {
			if (dialog instanceof JecnRuleConfigJDialog) {// 制度配置对话框
				flowPublicRelateFileItemPanel = new JecnRulePublicItemPanel(dialog);
			} else if (dialog instanceof JecnFlowConfigJDialog) {// //流程配置对话框
				flowPublicRelateFileItemPanel = new JecnFlowPublicItemPanel(dialog);
			} else if (dialog instanceof JecnTotalMapConfigJDialog) {// 地图配置对话框
				flowPublicRelateFileItemPanel = new JecnFlowMapPublicItemPanel(dialog);
			}

		}
		return flowPublicRelateFileItemPanel;
	}

	/****
	 * 文控信息配置
	 * 
	 * @return
	 */
	public JecnAbstractPropertyBasePanel getDocumentControlPanel() {
		if (documentControlPanel == null) {
			documentControlPanel = new JecnDocumentControlPanel(dialog);
		}
		return documentControlPanel;
	}

	public JecnFlowRuleTypePanel getFlowRuleTypePanel() {
		JecnFlowRuleTypePanel flowRuleTypePanel = new JecnFlowRuleTypePanel();
		return flowRuleTypePanel;
	}

	public JecnPublicPointPeoplePanel getPublicPointPeoplePanel() {
		JecnPublicPointPeoplePanel publicPointPeoplePanel = new JecnPublicPointPeoplePanel();
		return publicPointPeoplePanel;
	}

	public JecnConfigTypeDesgBean getConfigTypeDesgBean() {
		return configTypeDesgBean;
	}

	public JecnAbstractPropertyBasePanel getFlowKPIPanel() {
		if (flowKPIAttributePanel == null) {
			flowKPIAttributePanel = new JecnFlowKPIPanel(dialog);
		}
		return flowKPIAttributePanel;
	}
}