package com.jecn.webservice.zhongsh.flowpush.bean;

import java.util.List;

/**
 * 线
 * 
 * @author user
 * 
 */
public class WBAction {

	private String guid = "";
	private String actionName = "";
	private String description = "";
	private String firstStateGUID = "";
	private String secondStateGUID = "";
	private String firstStateActionHandlerID = "";
	private String secondStateActionHandlerID = "";
	private String firstActivityGUID = "00000000-0000-0000-0000-000000000000";
	private String icon = "default";
	private String toolTip = "";
	private String allowBulkLaunch = "False";

	private List<WBActionBreakPoint> wBActionBreakPointCollection;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName == null ? "" : actionName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description == null ? "" : description;
	}

	public String getFirstStateGUID() {
		return firstStateGUID;
	}

	public void setFirstStateGUID(String firstStateGUID) {
		this.firstStateGUID = firstStateGUID;
	}

	public String getSecondStateGUID() {
		return secondStateGUID;
	}

	public void setSecondStateGUID(String secondStateGUID) {
		this.secondStateGUID = secondStateGUID;
	}

	public String getFirstStateActionHandlerID() {
		return firstStateActionHandlerID;
	}

	public void setFirstStateActionHandlerID(String firstStateActionHandlerID) {
		this.firstStateActionHandlerID = firstStateActionHandlerID;
	}

	public String getSecondStateActionHandlerID() {
		return secondStateActionHandlerID;
	}

	public void setSecondStateActionHandlerID(String secondStateActionHandlerID) {
		this.secondStateActionHandlerID = secondStateActionHandlerID;
	}

	public String getFirstActivityGUID() {
		return firstActivityGUID;
	}

	public void setFirstActivityGUID(String firstActivityGUID) {
		this.firstActivityGUID = firstActivityGUID;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getToolTip() {
		return toolTip;
	}

	public void setToolTip(String toolTip) {
		this.toolTip = toolTip;
	}

	public String getAllowBulkLaunch() {
		return allowBulkLaunch;
	}

	public void setAllowBulkLaunch(String allowBulkLaunch) {
		this.allowBulkLaunch = allowBulkLaunch;
	}

	public List<WBActionBreakPoint> getwBActionBreakPointCollection() {
		return wBActionBreakPointCollection;
	}

	public void setwBActionBreakPointCollection(List<WBActionBreakPoint> wBActionBreakPointCollection) {
		this.wBActionBreakPointCollection = wBActionBreakPointCollection;
	}

}
