package epros.designer.gui.standard;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/***
 * 标准重命名
 * @author 2012-06-12
 *
 */
public class EditStandarNameDialog extends JecnEditNameDialog{
	private static Logger log = Logger.getLogger(EditStandarNameDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;
	
	public EditStandarNameDialog(JecnTreeNode selectNode,JTree jTree){
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		try {
			this.setName(selectNode.getJecnTreeBean().getName());
		} catch (Exception e) {
			log.error("EditStandarNameDialog is error",e);
		}
	}
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}

	@Override
	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	@Override
	public void saveData() {
		try {
			ConnectionPool.getStandardAction().reName(this.getName(),
					selectNode.getJecnTreeBean().getId(),
					JecnConstants.loginBean.getJecnUser().getPeopleId());
		} catch (Exception e) {
			log.error("EditStandarNameDialog saveData is error", e);
		}
		// 重命名
		JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
		this.dispose();
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
	}

	protected void validateEnglishName() {// 验证英文名称
	}

	protected void addEnNamePanel(Insets insets) {
	}

}
