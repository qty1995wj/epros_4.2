<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<!-- 任务管理 -->
		<title><s:text name="taskManagement"></s:text></title>
		<%--搜索--%>
	</head>
	<body>
		<div id="div" class="div" style="height: 100%">
			<div id="search" class="divBorder" style="overflow: hidden;"
				align="center">
				<div id="taskSearchText" style="overflow: hidden;" class="minWidth">
					<table width="100%">
						<tr>
							<td width="10%" height="25px" align="right">
								<!-- 任务名称： -->
								<s:text name="taskNameC"></s:text>
							</td>
							<td width="35%" align="left">
								<input id="taskName" type="text" style="width: 80%"
									maxLength="32" />
							</td>

							<td width="10%" align="right">
								<!-- 创建人： -->
								<s:text name="createrC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="createUserName_task"
									onkeyup="search(createUserName_task,createUserId_task,search_task_createUser,'person');"
									onblur="divNode(search_task_createUser)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("person","createUserName_task","createUserId_task")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="search_task_createUser"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td style="vertical-align: middle;" width="10%;">
								<a onClick="toggle(taskSearchText)" style="cursor: pointer;"
									class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>

						<tr>
							<td height="25px" align="right">
								<!-- 任务类型: -->
								<s:text name="taskTypeC" />
							</td>
							<td align="left">
								<s:select list="listTaskTypeNameBean" listValue="typeName"
									listKey="taskType" value="listTaskTypeNameBean.typeName"
									name="listTaskTypeNameBean.taskType" cssStyle="width: 80%"
									id="searchTaskType" theme="simple" disabled="false"
									onchange="searchTaskType();">
								</s:select>
							</td>


							<td align="right">
								<!-- 任务阶段: -->
								<s:text name="taskStageC" />
							</td>
							<td align="left">
								<select style="width: 80%" id="taskType">
									<option value="-1">
										<s:text name="all" />
									</option>
								</select>
								<!-- 流程任务审批阶段 -->
								<s:select list="taskSearchBean.listFlowTaskState"
									listValue="auditLable" listKey="state"
									value="taskSearchBean.listFlowTaskState.auditLable"
									name="taskSearchBean.listFlowTaskState.state"
									cssStyle="width: 80%;display:none" id="flowState"
									theme="simple">
								</s:select>
								<!-- 流程地图任务审批阶段 -->
								<s:select list="taskSearchBean.listFlowMapTaskState"
									listValue="auditLable" listKey="state"
									value="taskSearchBean.listFlowMapTaskState.auditLable"
									name="taskSearchBean.listFlowMapTaskState.state"
									cssStyle="width: 80%;display:none" id="flowMapState"
									theme="simple">
								</s:select>
								<!-- 文件任务审批阶段 -->
								<s:select list="taskSearchBean.listFileTaskState"
									listValue="auditLable" listKey="state"
									value="taskSearchBean.listFileTaskState.auditLable"
									name="taskSearchBean.listFileTaskState.state"
									cssStyle="width: 80%;display:none" id="fileState"
									theme="simple">
								</s:select>
								<!-- 制度任务审批阶段 -->
								<s:select list="taskSearchBean.listRuleTaskState"
									listValue="auditLable" listKey="state"
									value="taskSearchBean.listRuleTaskState.auditLable"
									name="taskSearchBean.listRuleTaskState.state"
									cssStyle="width: 80%;display:none" id="ruleState"
									theme="simple">
								</s:select>
							</td>

							<td></td>
							<td></td>

							<td></td>
						</tr>

						<tr>
							<td height="25px" align="right">
								<!-- 时间段: -->
								<s:text name="timeC" />
							</td>
							<td align="left">
								<s:textfield readonly="true" cssClass="Wdate" id="startTime"
									cssStyle="width:36%;border-color: #7F9DB9;"
									onclick="WdatePicker();" theme="simple" />
								-
								<s:textfield readonly="true" cssClass="Wdate" id="endTime"
									cssStyle="width:36%;border-color: #7F9DB9;"
									onclick="WdatePicker();" theme="simple" />
								<!-- <input type="checkbox" checked="checked"
									onclick="taskCheckbox();" id="checkbox"> -->
							</td>
							<s:if test="#session.webLoginBean.admin||#session.webLoginBean.secondAdmin">
								<td height="25px" align="right">
									<s:text name="delayTask" />
								</td>
								<!-- 搁置任务: -->
								<td align="left">
									<s:checkbox id="delayTask" name="delayTask"
										cssClass="vertical-align: top; margin-right: 10px;"
										theme="simple"></s:checkbox>
								</td>
							</s:if>

							<td></td>
							<td></td>

							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="taskManagementSearch(0)" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="taskSearchReset()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/download.gif"
						onclick="taskManagementSearch(1)" />
				</div>
				<input type="hidden" id="createUserId_task">
			</div>

			<div id="taskManageGrid_id">

			</div>

			<div>
				<form action="taskManageSearchDownload.action" id="downloadTaskSearch" method="post"
					style="visibility: false">
					<!-- 责任部门ID -->
					<input name="delayTask" id="delayTaskHidden" type="hidden" />
					<input name="createUserId" id="createUserIdHidden" type="hidden" />
					<input name="taskType" id="taskTypeHidden" type="hidden" />
					<input name="taskStage" id="taskStageHidden" type="hidden" />
					<input name="taskName" id="taskNameHidden" type="hidden" />
					<input name="startTime" id="startTimeHidden" type="hidden" />
					<input name="endTime" id="endTimeHidden" type="hidden" />
				</form>
			</div>
		</div>
	</body>
</html>