package com.jecn.epros.server.dao.task.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.task.IJecnTaskApproveTipDao;

/**
 * 任务审批提醒Dao
 * 
 * @author weidp
 * @date： 日期：2014-5-14 时间：下午05:10:53
 */
public class JecnTaskApproveTipDaoImpl extends AbsBaseDao<JecnUser, Long>
		implements IJecnTaskApproveTipDao {
	
	@Override
	public List<Object[]> getTaskTipUserListByTipDay(int tipDay) {
		String sql = null;
		// 当前时间
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String now=sdf.format(date);
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			  sql = "SELECT JTBN.ID, ju.people_id,ju.email,ju.email_type,JTBN.TASK_NAME,JTBN.TASK_TYPE" +
			  "               FROM JECN_TASK_PEOPLE_NEW JTPN" + 
			  "               inner JOIN JECN_TASK_BEAN_NEW JTBN ON JTPN.TASK_ID = JTBN.ID" + 
			  "               inner join jecn_user ju on ju.people_id=JTPN.Approve_Pid" + 
			  "               WHERE FLOOR(to_date('"+now+"', 'yyyy-mm-dd') - JTBN.UPDATE_TIME) >= ?" + 
			  "               and JTBN.IS_LOCK = 1";
				
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			sql = "SELECT JTBN.ID, ju.people_id,ju.email,ju.email_type,JTBN.TASK_NAME,JTBN.TASK_TYPE" +
			"               FROM JECN_TASK_PEOPLE_NEW JTPN" + 
			"               inner JOIN JECN_TASK_BEAN_NEW JTBN ON JTPN.TASK_ID = JTBN.ID" + 
			"               inner join jecn_user ju on ju.people_id=JTPN.Approve_Pid" +
			"               WHERE DATEDIFF(DAY,JTBN.UPDATE_TIME,'"+now+"')>=?" + 
			"               and JTBN.IS_LOCK = 1";
		}
		return this.listNativeSql(sql, tipDay);
	}
	
}
