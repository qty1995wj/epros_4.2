package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;

/**
 * 
 * 文件导出处理
 * 
 * @author Administrator
 * 
 */
public abstract class AbstractFileDownBuss {
	protected final Log log = LogFactory.getLog(this.getClass());

	/**
	 * 
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() {

		// 获得文件
		File file = new File(getOutFilepath());
		if (!file.isFile() || !file.exists()) {
			return null;
		}

		// 输入流
		FileInputStream inputStream = null;
		try {
			// 创建输入流
			inputStream = new FileInputStream(file);
			// 读数据
			byte[] content = new byte[inputStream.available()];
			inputStream.read(content);

			return new ByteArrayInputStream(content);

		} catch (Exception e) {
			log.error(SyncErrorInfo.FILE_DOWN_FAIL);
			return null;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error(SyncErrorInfo.FILE_DOWN_FAIL);
					return null;
				}
			}
		}
	}

	/**
	 * 
	 * 对于配置中的 ${fileName}, 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	public String getFileName() {
		return getOutFileName();
	}

	/**
	 * 
	 * 导出文件路径
	 * 
	 * @return String 文件路径
	 */
	protected abstract String getOutFilepath();

	/**
	 * 
	 * 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	protected abstract String getOutFileName();

}
