package com.jecn.epros.server.action.rmi.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.bean.BaseTaskParam;
import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.TaskAssignParam;
import com.jecn.epros.bean.TaskChangeInfoApproveParam;
import com.jecn.epros.bean.TaskChangeInfoAssignParam;
import com.jecn.epros.bean.TaskControlSubmitParam;
import com.jecn.epros.bean.TaskEditSubmitParam;
import com.jecn.epros.bean.TaskReSubmitParam;
import com.jecn.epros.bean.TaskSearchBean;
import com.jecn.epros.bean.TaskTwoApproveParam;
import com.jecn.epros.bean.external.TaskParam;
import com.jecn.epros.server.action.rmi.utils.TaskHelper;
import com.jecn.epros.server.action.web.task.TaskIllegalArgumentException;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.temp.SimpleSubmitMessage;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.connector.JecnSendTaskTOMainSystemFactory;
import com.jecn.epros.server.dao.popedom.IJecnRoleDao;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.task.IJecnAbstractTaskDao;
import com.jecn.epros.server.service.task.app.IJecnBaseTaskService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.service.task.search.IJecnTaskSearchService;
import com.jecn.epros.server.util.JecnDaoUtil;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.epros.server.webBean.task.TaskSearchTempBean;
import com.jecn.epros.service.ITaskRPCService;

/**
 * 任务RMI
 * 
 * @author user
 * 
 */
@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
public class TaskRPCServiceImpl implements ITaskRPCService {

	/** 任务log */
	private Logger log = Logger.getLogger(TaskRPCServiceImpl.class);
	/** 提交审批 任务处理service接口 */
	private IJecnBaseTaskService flowTaskService;
	/** 提交审批 任务处理service接口 */
	private IJecnBaseTaskService fileTaskService;
	/** 提交审批 任务处理service接口 */
	private IJecnBaseTaskService ruleTaskService;
	/** 任务DAO数据通用类 */
	private IJecnAbstractTaskDao abstractTaskDao;
	private IJecnRoleDao jecnRoleDao;
	/** 我的任务，任务计划 搜索、删除、显示记录 service接口 */
	private IJecnTaskSearchService searchService;

	@Override
	public MessageResult approve(Long curPeopleId, BaseTaskParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	public IJecnBaseTaskService getFlowTaskService() {
		return flowTaskService;
	}

	public void setFlowTaskService(IJecnBaseTaskService flowTaskService) {
		this.flowTaskService = flowTaskService;
	}

	public IJecnBaseTaskService getFileTaskService() {
		return fileTaskService;
	}

	public void setFileTaskService(IJecnBaseTaskService fileTaskService) {
		this.fileTaskService = fileTaskService;
	}

	public IJecnBaseTaskService getRuleTaskService() {
		return ruleTaskService;
	}

	public void setRuleTaskService(IJecnBaseTaskService ruleTaskService) {
		this.ruleTaskService = ruleTaskService;
	}

	@Override
	public MessageResult assign(Long curPeopleId, TaskAssignParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	@Override
	public MessageResult controlSubmit(Long curPeopleId, TaskControlSubmitParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	@Override
	public MessageResult resubmit(Long curPeopleId, TaskReSubmitParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	@Override
	public MessageResult twoApprove(Long curPeopleId, TaskTwoApproveParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	@Override
	public MessageResult editRollBack(Long curPeopleId, TaskEditSubmitParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	@Override
	public MessageResult editSubmit(Long curPeopleId, TaskEditSubmitParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	@Override
	public MessageResult approveChangeInfo(Long curPeopleId, TaskChangeInfoApproveParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	@Override
	public MessageResult assignChangeInfo(Long curPeopleId, TaskChangeInfoAssignParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	@Override
	public MessageResult callback(Long curPeopleId, BaseTaskParam taskParam) {
		return handle(curPeopleId, taskParam);
	}

	/**
	 * 删除任务，接口取消代办处理
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public void deleteSendInfos(Long taskId) throws Exception {
		JecnTaskBeanNew taskBeanNew = abstractTaskDao.get(taskId);
		if (taskBeanNew == null) {
			return;
		}
		// 获取删除删除任务，未审批的人员
		List<Long> cancelPeoples = getCancelPeoples(taskBeanNew);
		// 发送代办处理
		if (!JecnConfigTool.sendTaskByInterface()) {// 非代办，并且非撤回操作
			return;
		}
		JecnSendTaskTOMainSystemFactory.INSTANCE.sendInfoCancels(taskBeanNew, personDao, null, cancelPeoples, null);
	}

	private List<Long> getCancelPeoples(JecnTaskBeanNew taskBeanNew) {
		// 待删除目标人集合
		String hql = "SELECT approvePid,id from JecnTaskPeopleNew where taskId=?";
		List<Object[]> cancelObjs = abstractTaskDao.listHql(hql, taskBeanNew.getId());
		List<Long> cancelPeoples = new ArrayList<Long>();
		Long peopleId = null;
		for (Object[] obj : cancelObjs) {
			peopleId = Long.valueOf(obj[0].toString());
			cancelPeoples.add(peopleId);
			taskBeanNew.getCanclePeopleMap().put(peopleId, Long.valueOf(obj[1].toString()));
		}
		return cancelPeoples;
	}

	private MessageResult handle(Long curPeopleId, BaseTaskParam taskParam) {
		TaskHelper taskHelper = createTaskHelper(curPeopleId, taskParam);
		if (taskHelper == null) {
			return new MessageResult(false, "任务不存在");
		}

		String taskOperation = taskParam.getTaskOperation();

		MessageResult result = chargeApproveByTaskOperation(taskHelper, taskOperation);
		if (!result.isSuccess()) {
			return result;
		}

		result = isCanCallback(taskHelper, taskOperation);
		if (!result.isSuccess()) {
			return result;
		}

		result = taskOperationAndResult(taskHelper);
		if (!result.isSuccess()) {
			return result;
		}

		return new MessageResult(true, getSuccessResultByTaskOperation(taskOperation));
	}

	private MessageResult isCanCallback(TaskHelper taskHelper, String taskOperation) {
		if ("13".equals(taskOperation)) {
			if (!taskHelper.getCurPeopleId().equals(taskHelper.getTaskBean().getFromPeopleId())) {
				return new MessageResult(false, "任务可能已被审批，撤回失败！");
			}
		}
		return new MessageResult();
	}

	private String getSuccessResultByTaskOperation(String taskOperation) {
		if ("11".equals(taskOperation) || "12".equals(taskOperation)) {
			return JecnTaskCommon.EDIT_TASK_SUCESS;
		} else if ("13".equals(taskOperation)) {
			return JecnTaskCommon.CALLBACK_SUCESS;
		} else {
			return JecnTaskCommon.APPROVE_SUCESS;
		}
	}

	/**
	 * 是否需要判断已审批，如果不需要返回直接返回成功标示
	 * 
	 * @param taskHelper
	 * @param taskOperation
	 * @return
	 */
	private MessageResult chargeApproveByTaskOperation(TaskHelper taskHelper, String taskOperation) {
		if (isNeedChargeTaskApprove(taskOperation)) {
			boolean isApprove = false;
			try {
				isApprove = taskHelper.isApprove();
			} catch (Exception e) {
				log.error("查询任务是否已被审批异常", e);
				return new MessageResult(false, "查询任务是否已被审批异常");
			}
			if (isApprove) {
				MessageResult result = new MessageResult(false, JecnTaskCommon.APPROVE_IS_SUBMIT);
				return result;
			}
		}
		return new MessageResult(true, "");
	}

	/**
	 * 判断是否需要判断任务是否已经被审批
	 * 
	 * @param taskOperation
	 * @return
	 */
	private boolean isNeedChargeTaskApprove(String taskOperation) {
		// 管理员编辑以及人员撤销任务是不需要判断任务是否被审批的
		if (!"11".equals(taskOperation) && !"12".equals(taskOperation) && !"13".equals(taskOperation)) {
			return true;
		}
		return false;
	}

	// @Transactional(readOnly=true,propagation=Propagation.NOT_SUPPORTED)
	private TaskHelper createTaskHelper(Long curPeopleId, BaseTaskParam taskParam) {
		JecnTaskBeanNew taskBean = abstractTaskDao.get(Long.valueOf(taskParam.getTaskId()));
		if (taskBean == null) {
			return null;
		}
		return new TaskHelper(curPeopleId, taskParam, taskBean, flowTaskService, fileTaskService, ruleTaskService);
	}

	private MessageResult taskOperationAndResult(TaskHelper taskHelper) {
		MessageResult result = new MessageResult();
		try {
			if (taskHelper.getCurPeopleId() == null) {
				throw new TaskIllegalArgumentException();
			}
			taskOperation(taskHelper);
		} catch (Exception e) {
			log.error("", e);
			result.setSuccess(false);
			handleExceptionForResult(e, result);
		}
		return result;
	}

	private void handleExceptionForResult(Exception e, MessageResult result) {
		log.error("任务审批异常！方法taskOperation", e);
		result.setSuccess(false);
		if (e instanceof TaskIllegalArgumentException) {
			int type = ((TaskIllegalArgumentException) e).getExpType();
			// 人员离职或岗位变更
			try {
				String error = "";
				if (type == 1) {// 返回操作，人员相同
					error = JecnTaskCommon.CUR_PEOPLE_IS_SAME_TO_PEOPLE;
				} else if (type == 2)// 当前为整理意见阶段，会审阶段人员为空
				{
					error = JecnTaskCommon.REVIEW_PEOPLE_NOT_NULL;
				} else if (type == -1) {
					error = JecnTaskCommon.TASK_PARAM_ERROR;
				} else if (type == 7) {
					error = JecnTaskCommon.TASK_APPROVE_REPEAT;
				} else if (type == 8) {
					error = JecnTaskCommon.TASK_APPROVE_OR_PEOPLE_LIVE;
				} else {
					// 人员已离职或没有岗位，请联系管理员！
					error = JecnTaskCommon.STAFF_TURNOVER_OR_NO_JOB;
				}
				result.setResultMessage(error);
				log.error(error, e);
			} catch (Exception e1) {
				log.error("", e);
			}
		} else {
			result.setResultMessage("任务出现异常");
		}
	}

	/**
	 * 数据处理
	 * 
	 * @param taskService
	 * @param taskParam
	 * @throws java.lang.Exception
	 */
	private void taskOperation(TaskHelper taskHelper) throws java.lang.Exception {
		SimpleSubmitMessage submitMessage = taskHelper.createSimpleSubmitMessage();
		taskHelper.getTaskService().taskOperation(submitMessage);
	}

	public IJecnAbstractTaskDao getAbstractTaskDao() {
		return abstractTaskDao;
	}

	public void setAbstractTaskDao(IJecnAbstractTaskDao abstractTaskDao) {
		this.abstractTaskDao = abstractTaskDao;
	}

	@Override
	public ByteFile downloadTask(Long peopleId, TaskSearchBean searchBean) {
		// 是不是流程管理员
		try {
			boolean isAdmin = this.jecnRoleDao.isExistDefaultRole(JecnCommonSql.getStrAdmin(), peopleId);
			// 是不是二级管理员
			boolean isSecondAdmin = this.jecnRoleDao.isExistDefaultRole(JecnCommonSql.getStrSecondAdmin(), peopleId);
			TaskSearchTempBean searchTempBean = new TaskSearchTempBean();
			PropertyUtils.copyProperties(searchTempBean, searchBean);
			List<MyTaskBean> listTask = new ArrayList<MyTaskBean>();
			if (!isAdmin && isSecondAdmin) {
				// 根据查询条件获取登录人相关任务
				listTask = searchService.getSecondAdminTaskManagementBySearch(searchTempBean, -1, -1, peopleId);
			} else {
				// 根据查询条件获取登录人相关任务
				listTask = searchService.getTaskManagementBySearch(searchTempBean, -1, -1, peopleId, isAdmin);
			}

			byte[] bytes = searchService.downloadTaskManagementBySearch(listTask);
			String fileName = "task.xls";
			ByteFile byteFile = new ByteFile();
			byteFile.setBytes(bytes);
			byteFile.setFileName(fileName);
			return byteFile;
		} catch (Exception e) {
			log.error("rmi下载任务异常", e);
		}
		return null;
	}

	@Resource(name = "PersonDaoImpl")
	private IPersonDao personDao;

	@Override
	public String taskCountsByUser(String userCode) {
		JecnUser jecnUser = null;
		try {
			jecnUser = personDao.getJecnUserByLoginName(userCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (jecnUser == null) {
			return null;
		}
		return searchService.getToDotasksCount(jecnUser.getPeopleId()) + "";
	}

	@Override
	public List<Map<String, Object>> tasksByUser(String userCode) {
		JecnUser jecnUser = null;
		try {
			if (JecnConfigTool.isLiBangLoginType()) {
				jecnUser = JecnDaoUtil.isLoginByDomainName(userCode, personDao);
			} else {
				jecnUser = personDao.getJecnUserByLoginName(userCode);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (jecnUser == null) {
			return null;
		}
		return searchService.getTasksByUser(jecnUser);
	}

	public IJecnRoleDao getJecnRoleDao() {
		return jecnRoleDao;
	}

	public void setJecnRoleDao(IJecnRoleDao jecnRoleDao) {
		this.jecnRoleDao = jecnRoleDao;
	}

	public IJecnTaskSearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(IJecnTaskSearchService searchService) {
		this.searchService = searchService;
	}

	@Override
	public MessageResult handleTaskExternal(TaskParam param) {
		return flowTaskService.handleTaskExternal(param);
	}

	private List<JecnTaskHistoryFollow> getHistoryFollows(TaskParam param) {

		return null;
	}

}
