package com.jecn.epros.server.service.project.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.dao.project.ICurProjectDao;
import com.jecn.epros.server.service.project.ICurProjectService;

/***
 * 设置主项目管理表处理
 * @author 2012-05-23
 *
 */
@Transactional
public class CurProjectServiceImpl extends AbsBaseService<JecnCurProject, Long> implements ICurProjectService {

	private final static Log log = LogFactory.getLog(CurProjectServiceImpl.class);
	private ICurProjectDao curProjectDao;
	
	public ICurProjectDao getCurProjectDao() {
		return curProjectDao;
	}

	public void setCurProjectDao(ICurProjectDao curProjectDao) {
		this.curProjectDao = curProjectDao;
		this.baseDao = curProjectDao;
	}

	JecnCurProject curProject = null;
	@Override
	public boolean updateCurProject(Long projectId) throws Exception {
		curProject = getJecnCurProjectById();
		if(curProject == null){
			curProject = new JecnCurProject();
			curProject.setCurProjectId(projectId);
			log.info("=========================bean新增======curProject==="+curProject+"=============");
		}else{
			curProject.setCurProjectId(projectId);
			log.info("===================bean更新============curProject==="+curProject+"=============");
		}
		if(curProject!= null){
			if(curProject.getCurId() == null){
				this.curProjectDao.save(curProject);
				log.info("=========================data新增======curProject==="+curProject+"=============");
			}else{
				this.curProjectDao.update(curProject);
				log.info("===================data更新============curProject==="+curProject+"=============");
			}
			return true;
		}
		return false;
	}

	@Override
	public JecnCurProject getJecnCurProjectById()
			throws Exception {
		return curProjectDao.getJecnCurProjectById();
	}

}
