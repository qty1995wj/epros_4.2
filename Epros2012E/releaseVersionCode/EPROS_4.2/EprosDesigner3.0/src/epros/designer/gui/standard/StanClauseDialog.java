package epros.designer.gui.standard;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.process.guide.explain.GuideJScrollPane;
import epros.designer.gui.system.UploadFileCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 标准条款双击弹出框
 * 
 * @author user
 * 
 */
public abstract class StanClauseDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(StanClauseDialog.class);
	/** 主面板 */
	protected JecnPanel mainPanel;
	/** 内容面板 */
	protected JecnPanel infoPanel;
	/** 按钮面板 */
	protected JecnPanel buttonPanel;
	/** 条款Lab */
	protected JLabel clauseLab;
	/** 条款内容Lab */
	protected JLabel clauseContentLab;
	/** 条款Field */
	protected JecnTextField clauseField;
	/** 条款必填 **/
	protected JLabel clauseRequired;
	/** 是否形成程序文件 */
	protected JCheckBox programFileCheck;
	/** 条款内容 */
	protected JecnTextArea clauseContentArea;
	/** 条款必填 **/
	protected JLabel clauseContentRequired;
	/** 条款内容滚动面板 */
	protected JScrollPane clauseContentScrollPane;
	/** 备注 */
	protected JLabel remarkLab;
	/** 备注内容 */
	protected JecnTextArea remarkArea;
	/** 条款内容滚动面板 */
	protected JScrollPane remarkScrollPane;
	/** 确定 */
	protected JButton okButton = null;
	/** 取消 */
	protected JButton cancelButton = null;
	/** 验证提示 */
	protected JLabel tipLabel = null;

	protected JecnTreeNode node = null;
	protected JTree jTree = null;

	/** 附件 */
	protected UploadFileCommon uploadFileCommon;

	public StanClauseDialog(JecnTreeNode node, JTree jTree) {
		this.node = node;
		this.jTree = jTree;
		initCompotents();
		initLayout();
		buttonActionInit();
		this.setModal(true);
		this.setLocationRelativeTo(null);
	}

	public StanClauseDialog() {
		initCompotents();
		initLayout();
		buttonActionInit();
		this.setModal(true);
		this.setLocationRelativeTo(null);
	}

	/**
	 * 初始化组件
	 * 
	 * @author yxw 2013-10-31
	 * @description:
	 */
	private void initCompotents() {
		this.setSize(getWidthMax(), 530);
		this.setMinimumSize(new Dimension(550, 500));
		mainPanel = new JecnPanel();
		infoPanel = new JecnPanel();
		buttonPanel = new JecnPanel();
		clauseLab = new JLabel(JecnProperties.getValue("clauseC"));
		clauseContentLab = new JLabel(JecnProperties.getValue("clauseContentC"));
		clauseField = new JecnTextField();
		clauseRequired = new JLabel(JecnProperties.getValue("required"));
		clauseRequired.setForeground(Color.red);
		programFileCheck = new JCheckBox(JecnProperties.getValue("programFile"));

		clauseContentArea = new JecnTextArea();
		clauseContentArea.setBorder(null);
		clauseContentArea.setRows(4);

		clauseContentRequired = new JLabel(JecnProperties.getValue("required"));
		clauseContentRequired.setForeground(Color.red);
		clauseContentScrollPane = new GuideJScrollPane();
		clauseContentScrollPane.setViewportView(clauseContentArea);

		remarkLab = new JLabel(JecnProperties.getValue("remarkC"));
		remarkArea = new JecnTextArea();
		remarkArea.setBorder(null);
		remarkArea.setRows(4);
		remarkScrollPane = new GuideJScrollPane();
		remarkScrollPane.setViewportView(remarkArea);

		okButton = new JButton(JecnProperties.getValue("okBtn"));
		cancelButton = new JButton(JecnProperties.getValue("cancelBtn"));
		tipLabel = new JLabel();
		tipLabel.setForeground(Color.red);
		clauseContentArea.setBorder(null);
		clauseContentArea.setLineWrap(true);
		programFileCheck.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		clauseContentScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));

	}

	/**
	 * @author yxw 2013-10-31
	 * @description:组件布局
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		// 附件
		uploadFileCommon = new UploadFileCommon(mainPanel, 1, 0, insets, getWidthMax(), this);

		// 确认按钮
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(buttonPanel, c);

		infoPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(clauseLab, c);

		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(clauseField, c);

		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(clauseRequired, c);

		// 是否形成程序文件
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(programFileCheck, c);

		// 条款
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(clauseContentLab, c);
		c = new GridBagConstraints(1, 1, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(clauseContentScrollPane, c);

		c = new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(clauseContentRequired, c);

		// 备注
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(remarkLab, c);

		c = new GridBagConstraints(1, 2, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(remarkScrollPane, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(tipLabel);
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);

		this.getContentPane().add(mainPanel);
	}

	private void buttonActionInit() {

		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});

		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ok();
			}
		});
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				cancelButPerformed();

			}
		});
	}

	/**
	 * @author yxw 2013年12月19日
	 * @description:数据验证
	 * @return
	 */
	protected boolean validateData() {
		String clauseName = clauseField.getText();
		clauseName = clauseName == null ? "" : clauseName.trim();
		if (DrawCommon.isNullOrEmtry(clauseName)) {
			tipLabel.setText(JecnProperties.getValue("clauseName") + JecnProperties.getValue("isNotEmpty"));
			return false;
		}
		String s = JecnUserCheckUtil.checkNameLength(clauseName);
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			tipLabel.setText(JecnProperties.getValue("clauseName") + s);
			return false;
		}
		String clauseContent = clauseContentArea.getText();
		clauseContent = clauseContent == null ? "" : clauseContent.trim();
		if (DrawCommon.isNullOrEmtry(clauseContent)) {
			tipLabel.setText(JecnProperties.getValue("clauseContent") + JecnProperties.getValue("isNotEmpty"));
			return false;
		}
		// s = JecnUserCheckUtil.checkNoteLength(clauseContent);
		// if (!DrawCommon.isNullOrEmtryTrim(s)) {
		// tipLabel.setText(JecnProperties.getValue("clauseContent") + s);
		// return false;
		// }
		if (validateNodeRepeat(clauseName)) {
			tipLabel.setText(JecnProperties.getValue("nameHaved"));
			return false;
		}

		// 备注
		// String remark = remarkArea.getText();
		// remark = remark == null ? "" : remark.trim();
		// s = JecnUserCheckUtil.checkNoteLength(remark);
		// if (!DrawCommon.isNullOrEmtryTrim(s)) {
		// tipLabel.setText(JecnProperties.getValue("remark") + s);
		// return false;
		// }
		return true;
	}

	/**
	 * @author yxw 2013-11-4
	 * @description:确定执行事件
	 */
	protected abstract boolean okButtonAction();

	private void ok() {
		if (!validateData()) {
			return;
		}
		if (okButtonAction()) {
			this.setVisible(false);
		}
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				ok();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	protected boolean check() {
		// 名称
		String clauseName = clauseField.getText();
		clauseName = clauseName == null ? "" : clauseName.trim();
		String s = JecnUserCheckUtil.checkNameLength(clauseName);
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			tipLabel.setText(JecnProperties.getValue("clauseName") + s);
			return false;
		}
		if (validateNodeRepeat(clauseName)) {
			tipLabel.setText(JecnProperties.getValue("nameHaved"));
			return false;
		}
		// 标准
		String clauseContent = clauseContentArea.getText();
		clauseContent = clauseContent == null ? "" : clauseContent.trim();
		if (JecnValidateCommon.validateAreaNotMaxPass(clauseContent, JecnProperties.getValue("clauseContent"))) {
			return false;
		}
		// 备注
		String remarkAreaText = remarkArea.getText();
		remarkAreaText = remarkAreaText == null ? "" : remarkAreaText.trim();
		if (JecnValidateCommon.validateAreaNotMaxPass(remarkAreaText, JecnProperties.getValue("remark"))) {
			return false;
		}
		return true;
	}

	protected abstract boolean validateNodeRepeat(String name);

	protected abstract boolean isUpdate();

}
