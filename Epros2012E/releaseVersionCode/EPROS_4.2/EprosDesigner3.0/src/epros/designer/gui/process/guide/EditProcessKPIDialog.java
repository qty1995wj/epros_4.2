package epros.designer.gui.process.guide;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSearchPopup;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.process.flowtool.FlowToolChooseDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.swing.textfield.search.JecnSearchDocumentListener;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程KPI
 * 
 * @author 2012-07-10
 * 
 */
public abstract class EditProcessKPIDialog extends JecnDialog {
	private Logger log = Logger.getLogger(EditProcessKPIDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息显示Panel */
	private JecnPanel infoPanel = new JecnPanel();

	/** 指标Panel */
	private JecnPanel targetPanel = new JecnPanel();

	/** 按钮 Panel */
	private JecnPanel butPanel = new JecnPanel();

	/** KPI名称Lab */
	private JLabel kpiNameLab = new JLabel(JecnProperties.getValue("kpiNameC"));

	/** KPI名称Field */
	protected JecnTextField kpiNameField = new JecnTextField();
	private JLabel oneLab = new JLabel("*");
	/** kpi类型Combox */
	protected JComboBox kpiTypeCombox = new JComboBox(new String[] { "", JecnProperties.getValue("effectTarget"),
			JecnProperties.getValue("efficiencyTarget") });

	/** kpi值单位名称Combox */
	protected JComboBox longitudinalCombox = new JComboBox(new String[] { JecnProperties.getValue("KPIPercentage"),
			JecnProperties.getValue("quarterLab"), JecnProperties.getValue("monthLab"),
			JecnProperties.getValue("weekLab"), JecnProperties.getValue("workdDay"), JecnProperties.getValue("dayLab"),
			JecnProperties.getValue("KPIHour"), JecnProperties.getValue("task_minutes"),
			JecnProperties.getValue("oneUnit"), JecnProperties.getValue("personUnit"), "ppm",
			JecnProperties.getValue("kpiYuan"), JecnProperties.getValue("kpiSecondary") });
	/** kpi数据统计时间\频率Combox */
	protected JComboBox transverseCombox = new JComboBox(new String[] { JecnProperties.getValue("monthLab"),
			JecnProperties.getValue("quarterLab") });

	/** kpi定义Area */
	protected JecnTextArea kpiDefinedArea = new JecnTextArea();

	/** KPI定义滚动面板 */
	private JScrollPane kpiDefinedScrollPane = new JScrollPane(kpiDefinedArea);

	/** DefinedVerfy */
	// private VerifyLabel definedVerfyLab = new VerifyLabel();

	/** 流程KPI计算公式Area */
	protected JecnTextArea kpiMethodsArea = new JecnTextArea();

	/** 流程KPI计算公式滚动面板 */
	private JScrollPane kpiMethodsScrollPane = new JScrollPane(kpiMethodsArea);

	/** MethodsVerfy */
	// private VerifyLabel methodsVerfyLab = new VerifyLabel();

	/** 流程KPI目标值Field */
	protected JecnTextField targetField = new JecnTextField();
	/** 流程KPI目标值Combox */
	protected JComboBox targetCombox = new JComboBox(new String[] { "=", ">", "<", ">=", "<=" });

	/** targetVerfy */
	// protected VerifyLabel targetVerfyLab = new VerifyLabel();
	/** 数据提供者Field */
	protected JSearchTextField dataProviderField = new JSearchTextField();
	/** 数据提供者名称隐藏列：用于判断输入数据提供者名称是否正确 */
	protected JecnTextField dataProviderHideField = new JecnTextField();
	/** 数据提供者弹出popup */
	private JecnSearchPopup dataProviderSearchPopup = new JecnSearchPopup(dataProviderField);
	/** 数据提供者Button */
	private JButton dataProviderBut = new JButton(JecnProperties.getValue("selectBtn"));
	/** 数据提供者ID */
	protected Long dataProviderId = null;
	protected String dataProviderName = null;
	/** 数据提供者JecnTreeBeanList */
	protected List<JecnTreeBean> dataProviderList = new ArrayList<JecnTreeBean>();
	/** 数据提供者搜索出的数据集合 */
	private List<JecnTreeBean> dataProviderTableList = null;

	/** 指标来源Combox */
	protected JComboBox targetSourceCombox = new JComboBox(new String[] { JecnProperties.getValue("twoLevelTarget"),
			JecnProperties.getValue("personPBCTarget"), JecnProperties.getValue("otherImpWork") });

	/** 支撑的一级指标Area */
	protected JecnTextArea kpiFirstTargetArea = new JecnTextArea();
	/** 支撑的一级指标滚动面板 */
	private JScrollPane kpiFirstTargetScrollPane = new JScrollPane(kpiFirstTargetArea);
	/** 支撑的一级指标Button */
	private JButton kpiFirstTargetBut = new JButton(JecnProperties.getValue("selectBtn"));
	/** 支撑的一级指标ID */
	protected JecnTextField kpiFirstTargetId = new JecnTextField();

	/** 相关度Combox */
	protected JComboBox kpiRelevanceCombox = new JComboBox(new String[] { JecnProperties.getValue("strongCorrelation"),
			JecnProperties.getValue("weakCorrelation"), JecnProperties.getValue("noCorrelation") });

	/** 数据获取方式Combox */
	protected JComboBox dataSourceMethodCombox = new JComboBox(new String[] { JecnProperties.getValue("conManual"),
			JecnProperties.getValue("conSys") });

	/** IT系统Area */
	protected JecnTextArea itSystemArea = new JecnTextArea();
	/** IT系统滚动面板 */
	private JScrollPane itSystemScrollPane = new JScrollPane(itSystemArea);
	/** IT系统Button */
	private JButton itSystemBut = new JButton(JecnProperties.getValue("selectBtn"));
	/** 选择出的IT系统Ids集合 */
	protected String itSystemIds = null;
	/** *数据库中IT系统Ids集合 */
	protected List<Long> listToolIds = new ArrayList<Long>();
	/** IT系统List */
	protected List<JecnTreeBean> listFlowTool = new ArrayList<JecnTreeBean>();

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

	/** 验证提示信息显示Lab */
	protected VerifyLabel verfyLab = new VerifyLabel();

	/** 设置大小 */
	Dimension dimension = null;

	// 设置目的
	protected JecnTextArea purposeArea = new JecnTextArea();
	// 测量点 KPI_POINT
	protected JecnTextArea pointArea = new JecnTextArea();
	// 统计周期
	protected JecnTextArea periodArea = new JecnTextArea();
	// 说明
	protected JecnTextArea explainArea = new JecnTextArea();

	/** 支撑的一级指标滚动面板 */
	protected JScrollPane purposeScrollPane = new JScrollPane(purposeArea);
	protected JScrollPane pointScrollPane = new JScrollPane(pointArea);
	protected JScrollPane periodScrollPane = new JScrollPane(periodArea);
	protected JScrollPane explainScrollPane = new JScrollPane(explainArea);

	protected List<JecnConfigItemBean> selectList;

	public EditProcessKPIDialog() {
		this.setSize(650, 600);
		this.setTitle(getKPITile());
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setResizable(true);
		oneLab.setForeground(Color.red);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		targetPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		butPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		dimension = new Dimension(55, 20);
		targetCombox.setPreferredSize(dimension);
		targetCombox.setMaximumSize(dimension);
		targetCombox.setMinimumSize(dimension);

		// 定义滚动
		kpiDefinedScrollPane.setBorder(null);
		kpiDefinedScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		kpiDefinedScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// kpiDefinedScrollPane.setBorder(BorderFactory.createLineBorder(Color.blue));

		kpiMethodsScrollPane.setBorder(null);
		kpiMethodsScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		kpiMethodsScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 支撑的一级指标
		kpiFirstTargetScrollPane.setBorder(null);
		kpiFirstTargetScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		kpiFirstTargetScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// IT系统
		itSystemScrollPane.setBorder(null);
		itSystemScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		itSystemScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 支持的一级指标Area 不可手动编辑
		kpiFirstTargetArea.setEditable(false);
		// IT系统Area 不可手动编辑
		itSystemArea.setEditable(false);
		// 默认显示
		kpiRelevanceCombox.setSelectedIndex(2);
		targetSourceCombox.setSelectedIndex(2);
		dataProviderSearchPopup.hiddenColumn(0, 2);

		// // 初始化数据，判断控件显示与隐藏
		// String showKpiPro = ConnectionPool.getConfigAciton().selectValue(1,
		// "showKPIProperty");
		// if ("0".equals(showKpiPro)) {
		// targetPanel.setVisible(false);
		// } else if ("1".equals(showKpiPro)) {
		// targetPanel.setVisible(true);
		// }

		purposeScrollPane.setBorder(null);
		pointScrollPane.setBorder(null);
		periodScrollPane.setBorder(null);
		explainScrollPane.setBorder(null);

		// 初始化监听事件
		initActionListener();

		// 查询数据库显示配置项中的内容
		selectList = ConnectionPool.getConfigAciton().selectTableConfigItemBeanByType(1, 16);
		// 初始化布局
		initLayout();
	}

	/**
	 * 初始化监听事件
	 */
	private void initActionListener() {
		// 确定
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		// 支撑的一级指标选择按钮
		kpiFirstTargetBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				kpiFirstTargetButPerformed();
			}
		});
		// 数据提供者 按钮
		dataProviderBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dataProviderButPerformed();
			}
		});
		// IT系统
		itSystemBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				itSystemButPerformed();
			}
		});
		// kpi值单位名称
		longitudinalCombox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				longitudinalComboxPerformed();
			}
		});
	}

	/***************************************************************************
	 * 数据提供者搜索框监听
	 * 
	 */
	protected void dataProviderListener() {
		// 数据提供者 名称显示Field
		dataProviderField.addDocumentListener(new JecnSearchDocumentListener(dataProviderField) {
			@Override
			public void removeUpdateJecn(DocumentEvent e) {
				dataProviderFieldSearch();
			}

			@Override
			public void insertUpdateJecn(DocumentEvent e) {
				dataProviderFieldSearch();
			}

			@Override
			public void keyEnterReleased(KeyEvent e) {
				dataProviderSearchPopup.searchTableMousePressed();
			}
		});
		dataProviderSearchPopup.addPopupMenuListener(new PopupMenuListener() {
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				if (dataProviderSearchPopup.getSelectTreeBean() != null) {
					dataProviderId = dataProviderSearchPopup.getSelectTreeBean().getId();
					dataProviderName = dataProviderSearchPopup.getSelectTreeBean().getName();
					TreeNodeType attchMenNode = dataProviderSearchPopup.getSelectTreeBean().getTreeNodeType();
					dataProviderSearchPopup.setSelectTreeBean(null);
					// 数据提供者集合dataProviderList 赋值
					dataProviderList.clear();
					JecnTreeBean jecnTreeBean = new JecnTreeBean();
					jecnTreeBean.setId(dataProviderId);
					jecnTreeBean.setName(dataProviderName);
					jecnTreeBean.setTreeNodeType(attchMenNode);
					dataProviderList.add(jecnTreeBean);
					dataProviderHideField.setText(dataProviderName);
				}
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
			}
		});
	}

	/**
	 * 布局
	 */
	private void initLayout() {

		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(3, 2, 4, 2);
		// Insets inset1 = new Insets(2, 2, 2, 0);
		Insets inset2 = new Insets(2, 0, 2, 2);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		int y = 0;
		for (JecnConfigItemBean configItem : selectList) {
			if (configItem.getIsTableData() == 0) {// 去除不是KPI现实项
				continue;
			}
			// label
			c = new GridBagConstraints(0, y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			infoPanel.add(new JLabel(configItem.getName(JecnResourceUtil.getLocale()) + "："), c);

			// 根据不同的字段添加不同的field
			if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiName.toString())) {
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(kpiNameField, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiType.toString())) {// KPI类型
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(kpiTypeCombox, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiUtilName.toString())) {// KPI单位名称
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(longitudinalCombox, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiTargetValue.toString())) {// KPI目标值
				c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(targetField, c);
				c = new GridBagConstraints(2, y, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
						inset2, 0, 0);
				infoPanel.add(targetCombox, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiTimeAndFrequency.toString())) {// 数据统计时间频率
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(transverseCombox, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDefined.toString())) {// KPI定义
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						insets, 0, 0);
				infoPanel.add(kpiDefinedScrollPane, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDesignFormulas.toString())) {// 流程KPI计算公式
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						insets, 0, 0);
				infoPanel.add(kpiMethodsScrollPane, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDataProvider.toString())) {// 数据提供者
				c = new GridBagConstraints(1, y, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(dataProviderField, c);
				c = new GridBagConstraints(2, y, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
						inset2, 0, 0);
				infoPanel.add(dataProviderBut, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiDataAccess.toString())) {// 数据获取方式
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(dataSourceMethodCombox, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiIdSystem.toString())) {// IT系统
				c = new GridBagConstraints(1, y, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						insets, 0, 0);
				infoPanel.add(itSystemScrollPane, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
				c = new GridBagConstraints(2, y, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
						inset2, 0, 0);
				infoPanel.add(itSystemBut, c);
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiSourceIndex.toString())) {// 指标来源
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(targetSourceCombox, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiRrelevancy.toString())) {// 相关度
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(kpiRelevanceCombox, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString()
					.equals(ConfigItemPartMapMark.kpiSupportLevelIndicator.toString())) {// 支撑的一级指标
				c = new GridBagConstraints(1, y, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						insets, 0, 0);
				infoPanel.add(kpiFirstTargetScrollPane, c);
				c = new GridBagConstraints(2, y, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
						inset2, 0, 0);
				infoPanel.add(kpiFirstTargetBut, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiPurpose.toString())) {// 设置目的
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						insets, 0, 0);
				infoPanel.add(purposeScrollPane, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiPoint.toString())) {// 测量点
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						insets, 0, 0);
				infoPanel.add(pointScrollPane, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiPeriod.toString())) {// 统计周期
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						insets, 0, 0);
				infoPanel.add(periodScrollPane, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			} else if (configItem.getMark().toString().equals(ConfigItemPartMapMark.kpiExplain.toString())) {// 说明
				c = new GridBagConstraints(1, y, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
						insets, 0, 0);
				infoPanel.add(explainScrollPane, c);
				addMustWriteItemLable(y, insets, configItem.getIsEmpty());
			}
			y++;
		}
		// 空
		c = new GridBagConstraints(0, y + 1, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(new JLabel(), c);

		// 按钮面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(butPanel, c);
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		butPanel.add(verfyLab);
		butPanel.add(okBut);
		butPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	private void addMustWriteItemLable(int y, Insets insets, Integer isNotEmpty) {
		if (isNotEmpty == null || isNotEmpty == 0) {
			return;
		}
		GridBagConstraints c = new GridBagConstraints(3, y, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		JLabel flowShowLab = new JLabel(JecnProperties.getValue("required"));
		flowShowLab.setForeground(Color.red);
		infoPanel.add(flowShowLab, c);
	}

	/**
	 * 数据提供者按钮
	 */
	private void dataProviderButPerformed() {
		// 选择人员
		dataProviderId = JecnDesignerCommon.setResponsiblePerson(dataProviderList, dataProviderField, this,
				dataProviderId);
		dataProviderHideField.setText(dataProviderField.getText());
	}

	/***************************************************************************
	 * 数据提供者 人员搜索
	 */
	private void dataProviderFieldSearch() {
		try {
			String name = dataProviderField.getText().trim();
			if (!"".equals(name)) {
				if (dataProviderSearchPopup.isCanSearch()) {
					dataProviderTableList = ConnectionPool.getPersonAction().searchUserByName(name,
							JecnConstants.projectId);
					dataProviderSearchPopup.setTableData(dataProviderTableList);
				}
				dataProviderSearchPopup.setCanSearch(true);
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			log.error("EditProcessKPIDialog dataProviderFieldSearch is error", e);
		}
	}

	/**
	 * IT系统
	 */
	private void itSystemButPerformed() {
		FlowToolChooseDialog flowToolChooseDialog = new FlowToolChooseDialog(listFlowTool);
		// flowToolChooseDialog.setSelectMutil(false);
		flowToolChooseDialog.setVisible(true);
		if (flowToolChooseDialog.isOperation()) {
			if (listFlowTool.size() > 0) {
				StringBuffer sbIds = new StringBuffer();
				StringBuffer sbNames = new StringBuffer();
				for (JecnTreeBean o : listFlowTool) {
					sbIds.append(o.getId() + ",");
					sbNames.append(o.getName() + "/");
				}
				itSystemIds = sbIds.substring(0, sbIds.length() - 1);
				itSystemArea.setText(sbNames.substring(0, sbNames.length() - 1));
			} else {
				itSystemArea.setText("");
				itSystemIds = null;
			}
		}
	}

	/**
	 * 支撑的一级指标选择按钮
	 */
	private void kpiFirstTargetButPerformed() {
		KPIFirstTargetDialog firstTargetDialog = new KPIFirstTargetDialog();
		firstTargetDialog.selectTableResult(kpiFirstTargetId.getText());
		firstTargetDialog.setVisible(true);
		// 点击确定保存数据
		if (firstTargetDialog.isOk()) {
			kpiFirstTargetArea.setText(firstTargetDialog.getTargetDes());
			kpiFirstTargetId.setText(firstTargetDialog.getTargetId());
		}
	}

	/***
	 * kpi值单位名称
	 */
	private void longitudinalComboxPerformed() {
		// "kpi值单位名称"选中季度
		if (longitudinalCombox.getSelectedIndex() == 1) {
			// "数据统计时间频率"选中季度且 不可编辑
			transverseCombox.setSelectedIndex(1);
			transverseCombox.setEnabled(false);
		} else {
			transverseCombox.setEnabled(true);
		}
	}

	/***************************************************************************
	 * 取消
	 */
	public abstract void cancelButPerformed();

	/***************************************************************************
	 * 确定
	 */
	private void okButPerformed() {

		if (dataValidateAccess()) {
			saveData();
		}

	}

	/**
	 * 验证数据是否正确 true正确 false错误
	 */
	private boolean dataValidateAccess() {
		verfyLab.setText("");

		for (JecnConfigItemBean configItem : selectList) {
			if (configItem.getMark().equals(ConfigItemPartMapMark.kpiTargetValue.toString())) {// KPI目标值
				// KPI目标值 只能输入数字验证
				String targetValue = this.targetField.getText();
				if (configItem.isNotEmpty() && StringUtils.isBlank(targetValue)) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
				if (targetValue != null && !"".equals(targetValue)) {
					// 判断是否是正整形或小数
					if (!targetValue.matches("^[\\d]+[\\.]?[\\d]{1,2}+|[\\d]+$")) {
						this.verfyLab.setText(JecnProperties.getValue("kpiTargetJustNum"));
						return false;
					}

					double tVfloat = Double.valueOf(targetValue).doubleValue();
					if (tVfloat > 1000000000) {// 不能大于1000000000
						this.verfyLab.setText(JecnProperties.getValue("kpiTargetMaxLimit"));
						return false;
					}

					// 判断“00000”或“0000.01”情况下转换成“0”或“0.01”
					String[] vArray = targetValue.split("\\.");
					if (vArray != null && vArray.length >= 1) {
						String value0 = vArray[0];
						if (value0 != null && value0.matches("^0+$")) {
							if (vArray.length == 2) {
								this.targetField.setText("0." + vArray[1]);
							} else {
								this.targetField.setText("0");
							}
						}
					}
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiDefined.toString())) {// KPI定义
				if (configItem.isNotEmpty() && StringUtils.isBlank(kpiDefinedArea.getText())) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
				// KPI定义
				if (!JecnValidateCommon.validateContent(this.kpiDefinedArea.getText(), this.verfyLab)) {
					this.verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + verfyLab.getText());
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiDesignFormulas.toString())) {// 流程KPI计算公式
				if (configItem.isNotEmpty() && StringUtils.isBlank(kpiMethodsArea.getText())) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
				if (!JecnValidateCommon.validateContent(this.kpiMethodsArea.getText(), this.verfyLab)) {
					this.verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + verfyLab.getText());
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiPurpose.toString())) {// 设置目的
				if (configItem.isNotEmpty() && StringUtils.isBlank(purposeArea.getText())) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
				if (!JecnValidateCommon.validateContent(this.purposeArea.getText(), this.verfyLab)) {
					this.verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + verfyLab.getText());
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiPoint.toString())) {// 测量点
				if (configItem.isNotEmpty() && StringUtils.isBlank(pointArea.getText())) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
				if (!JecnValidateCommon.validateContent(this.pointArea.getText(), this.verfyLab)) {
					this.verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + verfyLab.getText());
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiPeriod.toString())) {// 统计周期
				if (configItem.isNotEmpty() && StringUtils.isBlank(periodArea.getText())) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
				if (!JecnValidateCommon.validateContent(this.periodArea.getText(), this.verfyLab)) {
					this.verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + verfyLab.getText());
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiExplain.toString())) {// 说明
				if (configItem.isNotEmpty() && StringUtils.isBlank(explainArea.getText())) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
				if (!JecnValidateCommon.validateContent(this.explainArea.getText(), this.verfyLab)) {
					this.verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + verfyLab.getText());
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiName.toString())) {// kpi
				// 名称
				// 验证名称是否正确
				if (!JecnValidateCommon.validateNameNoRestrict(this.kpiNameField.getText(), this.verfyLab)) {
					this.verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + verfyLab.getText());
					return false;
				}
				// 验证重名
				if (validateNodeRepeat(this.kpiNameField.getText().trim())) {
					this.verfyLab.setText(JecnProperties.getValue("hasKPIName"));
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiIdSystem.toString())) {
				if (configItem.isNotEmpty() && StringUtils.isBlank(itSystemArea.getText())) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiType.toString())) {
				if (configItem.isNotEmpty() && kpiTypeCombox.getSelectedIndex() == 0) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiSupportLevelIndicator.toString())) {
				if (configItem.isNotEmpty() && StringUtils.isBlank(kpiFirstTargetArea.getText())) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
			} else if (configItem.getMark().equals(ConfigItemPartMapMark.kpiDataProvider.toString())) {
				if (configItem.isNotEmpty() && StringUtils.isBlank(dataProviderName)) {
					verfyLab.setText(configItem.getName(JecnResourceUtil.getLocale()) + JecnUserCheckInfoData.getNameNotNull());
					return false;
				}
			}
		}

		return true;
	}

	public abstract boolean validateNodeRepeat(String name);

	/** 保存数据 */
	public abstract void saveData();

	/** 获取窗体title */
	public abstract String getKPITile();

	class VerifyLabel extends JLabel {
		public VerifyLabel() {
			this.setForeground(Color.red);
		}
	}
}
