package com.jecn.epros.domain.journal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.service.CommonServiceUtil;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 用户操作日志-返回数据对象
 *
 * @author ZXH
 */
public class UserOperateResult {
    private Long id;
    /**
     * 文件ID
     */
    private Long relatedId;
    /**
     * 文件类型  日志类型 1是流程，2是文件，3是角色,4是制度,5是标准,6是组织,9是风险,15 术语定义 16岗位组 ，17支持工具， 18风险点，
     * 19制度模板 ， 20模型
     */
    private int relatedType;
    /**
     * 文件名称（当前名称）
     */
    private String relatedName;
    private String relateName;
    /**
     * 操作时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date operatorTime;
    /**
     * 日志类型 1是新增，2是修改，3是删除，4移动，5排序，6假删除放到回收站,10自动编号，13设置任务模板
     * 15记录恢复（本地上传的文件或者制度或者流程文件）16记录删除 17发布不记录文控 18撤销发布19复制方式新增
     */
    private int operatorType;
    /**
     * 访问人
     */
    private String peopleName;

    private String strRelatedType;

    /**
     * 当operatorType为2时 0 属性修改（默认值） 1 流程图修改 2 架构卡图修改 3 集成关系图修改 4 重命名 5 相关文件修改 6
     * 流程文件修改（操作说明） 7 查阅权限  8 文件内容（更新内容） 9 流程文件转换为流程图 10 批量修改专员责任人
     */
    private int secondOperatorType;

    public String getStrRelatedType() {
        return CommonServiceUtil.getStringTypeByRelatedOperateType(relatedType);
    }

    public String getStrOperationType() {
        return CommonServiceUtil.getStringTypeByOperateType(operatorType);
    }

    public String getStrSecondOperatorType() {
        if (operatorType == 2 && secondOperatorType == 0) {
            return JecnProperties.getValue("property");
        }
        return CommonServiceUtil.getStringSecondOperatorType(secondOperatorType);
    }

    public String getRelatedName() {
        if (StringUtils.isBlank(relatedName)) {
            return relateName;
        }
        return relatedName;
    }

    //--------------------

    public String getRelateName() {
        return relateName;
    }

    public void setRelateName(String relateName) {
        this.relateName = relateName;
    }

    public void setStrRelatedType(String strRelatedType) {
        this.strRelatedType = strRelatedType;
    }

    public int getSecondOperatorType() {
        return secondOperatorType;
    }

    public void setSecondOperatorType(int secondOperatorType) {
        this.secondOperatorType = secondOperatorType;
    }

    @JsonIgnore
    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public int getRelatedType() {
        return relatedType;
    }

    public void setRelatedType(int relatedType) {
        this.relatedType = relatedType;
    }

    public Date getOperatorTime() {
        return operatorTime;
    }

    public void setOperatorTime(Date operatorTime) {
        this.operatorTime = operatorTime;
    }

    @JsonIgnore
    public int getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(int operatorType) {
        this.operatorType = operatorType;
    }

    public String getPeopleName() {
        return peopleName;
    }

    public void setPeopleName(String peopleName) {
        this.peopleName = peopleName;
    }

    public void setRelatedName(String relatedName) {
        this.relatedName = relatedName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
