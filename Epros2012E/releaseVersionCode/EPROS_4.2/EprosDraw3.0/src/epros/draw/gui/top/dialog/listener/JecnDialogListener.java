package epros.draw.gui.top.dialog.listener;

/**
 * 
 * 对话框监听类
 * 
 * @author ZHOUXY
 * 
 */
public interface JecnDialogListener {
	/**
	 * 
	 * 是否关闭对话框 true：需要执行关闭 false：不要执行关闭
	 * 
	 */
	public boolean dialogCloseBefore(JecnDialogEvent e);
}
