package com.jecn.epros.server.action.designer.popedom;

import java.util.List;
import java.util.Map;

import com.jecn.epros.server.bean.log.JecnPersonalPermRecord;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.LoginBean;
import com.jecn.epros.server.bean.popedom.UserAddReturnBean;
import com.jecn.epros.server.bean.popedom.UserEditBean;
import com.jecn.epros.server.bean.popedom.UserPageBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

public interface IPersonAction {

	/**
	 * @author yxw 2012-5-22
	 * @description:添加人员
	 * @param jecnUser
	 * @param figureIds
	 *            岗位Id集合
	 * @param roleIds
	 *            角色Id集合
	 * @return
	 * @throws Exception
	 */
	public UserAddReturnBean savePerson(JecnUser jecnUser, String figureIds, String roleIds) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:编辑人员
	 * @param jecnUser
	 * @param figureIds
	 *            岗位Id集合
	 * @param roleIds
	 *            角色Id集合
	 * @return
	 * @throws Exception
	 */
	public UserAddReturnBean updatePerson(JecnUser jecnUser, String figureIds, String roleIds) throws Exception;

	/**
	 * @author yxw 2012-7-23
	 * @description:根据岗位查询人员
	 * @param positionId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPersonByPosition(Long positionId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:搜索
	 * @param orgId
	 * @param roleId
	 * @param loginName
	 * @param trueName
	 * @return
	 * @throws Exception
	 */
	public UserPageBean searchUser(Long orgId, String roleType, String loginName, String trueName, int curPage,
			String operationType, int pageSize, Long projectId, Long isDelete,boolean isLogin) throws Exception;

	/**
	 * @author yxw 2012-9-3
	 * @description:根据名称查询
	 * @param name
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchUserByName(String name, Long projectId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:获得人员信息
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public UserEditBean getUserEidtBeanByPeopleId(Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:解除登陆
	 * @param peopleId
	 * @throws Exception
	 */
	public void dischargeLogin(Long peopleId) throws Exception;

	/**
	 * @author yxw 2012-5-22
	 * @description:删除人员
	 * @param listPeopleIds
	 * @return 0正确删除 1不能删除所有管理员 至少留有一个
	 * @throws Exception
	 */
	public int delUsers(List<Long> listPeopleIds) throws Exception;

	/**
	 * @author 2012-12-24
	 * @description:恢复人员
	 * @param listPeopleIds
	 * @throws Exception
	 */
	public void restorationUsers(List<Long> listPeopleIds) throws Exception;

	/**
	 * 
	 * @author yxw 2012-5-28
	 * @description: 新建用户时判断登录名是否重复
	 * @param loginName
	 * @return 有相同的返回true
	 * @throws Exception
	 */
	public boolean validateAddLoginName(String loginName) throws Exception;

	/**
	 * @author yxw 2012-5-28
	 * @description:更新用户时判断登录名是否重复
	 * @param loginName
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public boolean validateUpdateLoginName(String loginName, Long id) throws Exception;

	/**
	 * @author yxw 2012-8-2
	 * @description:
	 * @param loginName
	 *            用户名
	 * @param passward
	 *            密码
	 * @param macAddress
	 *            用户电脑MAC地址
	 * @param loginType
	 *            0输入登录 1记住密码登录
	 * @return
	 * @throws Exception
	 */
	public LoginBean loginDesign(String loginName, String passward, String macAddressClient, int loginType)
			throws Exception;

	/**
	 * @author yxw 2012-8-14
	 * @description:用户退出解除此用户所占用的资源
	 * @param userId
	 * @throws Exception
	 */
	public void loginOut(Long userId) throws Exception;

	/**
	 * @author yxw 2013-1-11
	 * @description:用户密码修改
	 * @param userId
	 *            用户ID
	 * @param newPassword
	 *            新密码-- 加密后密码
	 * @return 加密后密码
	 * @throws Exception
	 */
	public String userPasswordUpdate(Long userId, String newPassword) throws Exception;

	/**
	 * @author yxw 2013-1-14
	 * @description:获取所有的管理员id集合
	 * @return
	 * @throws Exception
	 */
	public List<Long> getAllAdminList() throws Exception;

	/**
	 * 添加人员 验证最大用户
	 * 
	 * @return
	 * @throws Exception
	 */
	public int addPeopleValidate() throws Exception;

	List<JecnTreeBean> searchRoleAuthByName(String name, Long projectId, Long peopleId) throws Exception;

	public JecnUser getJecnUserByPeopleId(Long peopleId);

	public JecnPersonalPermRecord getPersionalPermRecord(Long id, int type, Long peopleId) throws Exception;

	public Map<String, String> getJecnIftekSyncUser(List<String> name);

	public List<JecnTreeBean> searchUserByIftekSyncLoginName(List<String> names, Long projectId);

	public String getListUserSearchBean(String loginName);
}
