package com.jecn.epros.server.bean.process.through;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.util.JecnUtil;

public class FlowInouts {
	/**
	 * 流程inouts 0 流程id 1流程名称 2流程编号 3输入输出id 4输入输出名称
	 */
	private List<Object[]> inouts;

	private IBaseDao baseDao;
	private int type;

	public FlowInouts(IBaseDao baseDao, int type, Long... flowIds) {
		this.baseDao = baseDao;
		this.type = type;
		String condition = "";
		if (flowIds.length == 0) {
			this.inouts = new ArrayList<Object[]>();
		} else {
			if (flowIds.length == 1) {
				condition = " = " + flowIds[0];
			} else {
				condition = " in " + JecnCommonSql.getIds(Arrays.asList(flowIds));
			}
			String sql = "select jfs.flow_id," + "         jfs.flow_name," + "         jfs.flow_id_input,"
					+ "         jfio.id," + "         jfio.name" + "    from jecn_flow_in_out_t jfio"
					+ "   inner join jecn_flow_structure_t jfs"
					+ "      on jfio.flow_id = jfs.flow_id and jfs.flow_id " + condition + "  where jfio.type=" + type
					+ "   order by jfs.flow_id, jfio.id";
			List<Object[]> objs = baseDao.listNativeSql(sql);
			this.inouts = objs;
		}
	}

	public FlowInouts(IBaseDao baseDao, int type, List<Long> flowIds) {
		this(baseDao, type, flowIds.toArray(new Long[flowIds.size()]));
	}

	/**
	 * 获得相同名称输入输出的流程名称
	 * 
	 * @param name
	 * @return
	 */
	public List<String> getFlowNames(String name) {
		Set<String> result = new HashSet<String>();
		if (StringUtils.isBlank(name)) {
			return new ArrayList<String>();
		}
		for (Object[] str : this.inouts) {
			if (name.equals(JecnUtil.nullToEmpty(str[4]))) {
				result.add(str[1].toString());
			}
		}
		return new ArrayList<String>(result);
	}

	public List<Object[]> getInouts() {
		return inouts;
	}
}
