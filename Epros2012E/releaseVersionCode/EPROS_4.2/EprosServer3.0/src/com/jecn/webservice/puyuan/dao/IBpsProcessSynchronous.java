package com.jecn.webservice.puyuan.dao;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowStructureImage;
import com.jecn.epros.server.common.IBaseDao;


public interface IBpsProcessSynchronous extends IBaseDao<JecnFlowStructureImage, Long> {
     
	/**
	 * 根据流程ID获取流程所属的流程架构以及当前要同步的流程信息
	 * @author chehuanbo
	 * @date 2014-12-29
	 * */
	public List<Object[]> findAllByFlowMapId(Long flowId);
	
	/**
	 * 
	 * 根据流程ID查询所有的流程元素
	 * @author chehuanbo
	 * 
	 * **/
	public List<Object[]> findAllByFlowImageId(Long flowId);
	

	/**
	 * 根据协作框，查找连接当前协作框的连接线
	 * 
	 * @author chehuanbo
	 * 
	 * */
    public List<Object[]> findAllByDottedRectId(Long figureId);
	
}
