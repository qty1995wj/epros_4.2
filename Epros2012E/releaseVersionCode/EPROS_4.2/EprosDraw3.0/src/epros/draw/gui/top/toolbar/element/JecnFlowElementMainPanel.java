package epros.draw.gui.top.toolbar.element;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnDefaultFlowElementData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程元素维护主界面:包括图形元素列表面板、流程元素面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementMainPanel extends JecnPanel implements ListSelectionListener {
	/** 元素属性面板：图形阅览区域和元素属性设置面板 */
	private JecnFlowElementMangPanel flowElementPanel = null;
	/** scrollPane容器 */
	private JPanel listMainPanel = null;
	/** mapList的容器 */
	private JScrollPane scrollPane = null;
	/** 流程元素列表 */
	private JList mapList = null;

	/** 承载流程元素面板的容器 */
	private Container comp = null;

	/** 需要写入到本地的流程元素记录集合 */
	private Map<MapElemType, JecnBaseFlowElementPanel> mapElemTypeMap = new HashMap<MapElemType, JecnBaseFlowElementPanel>();

	public JecnFlowElementMainPanel(Container comp) {
		if (comp == null) {
			JecnLogConstants.LOG_FLOW_ELEMENT_MAIN_PANEL.error("JecnFlowElementMainPanel类构造函数：参数为null。comp=" + comp);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.comp = comp;

		initComponents();
		initLayout();
	}

	private void initComponents() {
		listMainPanel = new JPanel();
		// mapList的容器
		scrollPane = new JScrollPane();
		// 流程元素列表
		mapList = new JList(getJListData());

		// 元素属性面板
		flowElementPanel = new JecnFlowElementMangPanel(comp, this, false);

		// 背景
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		listMainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 标题
		listMainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnResourceUtil
				.getJecnResourceUtil().getValue("figureElement")));
		listMainPanel.setLayout(new BorderLayout(5, 13));

		// totalMapList的容器
		// 背景
		scrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 大小
		// JList列表宽
		int widthJListWidth = Integer.valueOf(JecnResourceUtil.getJecnResourceUtil().getValue("jListMinWidth"));
		// JList列表高
		int widthJListHeight = Integer.valueOf(JecnResourceUtil.getJecnResourceUtil().getValue("jListMinHeight"));
		Dimension scrollPaneSize = new Dimension(widthJListWidth, widthJListHeight);
		scrollPane.setPreferredSize(scrollPaneSize);
		scrollPane.setMinimumSize(scrollPaneSize);

		// 选择模式
		mapList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// 事件
		mapList.addListSelectionListener(this);
		// 背景
		mapList.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		listMainPanel.add(scrollPane, BorderLayout.CENTER);
		scrollPane.setViewportView(mapList);

		mapList.setSelectedIndex(0);
	}

	private void initLayout() {

		// mapList的容器
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL,
				new Insets(25, 12, this.flowElementPanel.getSizeBottomBtnPanel().height, 0), 0, 0);
		this.add(listMainPanel, c);

		// 元素属性面板
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(25, 15, 0, 12), 0, 0);
		this.add(flowElementPanel, c);
	}

	/**
	 * 
	 * 获取JList对象的选项值
	 * 
	 * @return Vector<String>
	 */
	private Vector<String> getJListData() {
		// 所有能添加到面板的流程元素类型
		MapElemType[] flowElementSetType = JecnToolBoxConstant.flowElementSetType;
		Vector<String> array = new Vector<String>();
		for (int i = 0; i < flowElementSetType.length; i++) {
			MapElemType elemType = flowElementSetType[i];
			String value = JecnDrawMainPanel.getMainPanel().getBoxPanel().getResourceManager().getValue(
					elemType.toString());
			array.add(value);
		}
		return array;
	}

	/**
	 * 
	 * 把修改的属性值保存到本地XML文件中
	 * 
	 */
	public void writeFlowElementDataToXML() {

		for (MapElemType tmpMapElemType : mapElemTypeMap.keySet()) {
			// 待更新的属性对象
			JecnDefaultFlowElementData updateDefaultFlowElementData = JecnSystemData
					.getCurrFlowElementData(tmpMapElemType);
			// 获取流程元素的属性对象
			JecnAbstractFlowElementData flowElementData = mapElemTypeMap.get(tmpMapElemType).getFlowElementData();

			// 属性值->默认属性值
			updateDefaultFlowElementData.flowDataToDefaultData(flowElementData, updateDefaultFlowElementData);
		}

		// 执行写入操作
		JecnSystemData.writeCurrFlowElementDataList();

	}

	/**
	 * 
	 * JList选择项事件
	 * 
	 */
	public void valueChanged(ListSelectionEvent e) {

		if (e.getValueIsAdjusting()) {// 執行兩次p
			return;
		}
		// 选中项的索引
		int index = mapList.getSelectedIndex();

		if (index < 0 || (index > JecnToolBoxConstant.flowElementSetType.length - 1)) {// 不在数组范围内
			return;
		}

		MapElemType elemType = JecnToolBoxConstant.flowElementSetType[index];

		// 当前选中流程元素
		JecnBaseFlowElementPanel objPanel = getFlowElementPanel(elemType);

		// 添加图形以及其属性值
		flowElementPanel.addFlowELementObj(objPanel);

		this.repaint();
	}

	/**
	 * 
	 * 获取流程元素对象
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 * @return JecnBaseFlowElementPanel 流程元素对象
	 */
	private JecnBaseFlowElementPanel getFlowElementPanel(MapElemType mapElemType) {
		// 流程元素
		JecnBaseFlowElementPanel mapElemPanel = null;

		if (mapElemTypeMap.containsKey(mapElemType)) {// 已经创建了此流程元素
			mapElemPanel = mapElemTypeMap.get(mapElemType);
		} else {// 创建流程元素
			mapElemPanel = JecnCreateFlowElement.createFlowElementObj(mapElemType);
			if (mapElemPanel == null) {
				return null;
			}
			// 添加到集合中去
			mapElemTypeMap.put(mapElemType, mapElemPanel);
		}
		return mapElemPanel;
	}

	public JecnResourceUtil getResourceManager() {
		return JecnResourceUtil.getJecnResourceUtil();
	}

}
