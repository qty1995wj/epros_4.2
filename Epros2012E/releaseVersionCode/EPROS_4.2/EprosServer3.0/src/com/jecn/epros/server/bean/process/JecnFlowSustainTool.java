package com.jecn.epros.server.bean.process;

public class JecnFlowSustainTool implements java.io.Serializable {
	private Long flowSustainToolId;//主键ID
	private String flowSustainToolShow;//支持工具内容
	private Long preFlowSustainToolId;//父ID
	private Integer sortId;        //排序
	public JecnFlowSustainTool() {

	}

	public JecnFlowSustainTool(Long flowSustainToolId) {
		this.flowSustainToolId = flowSustainToolId;
	}

	public JecnFlowSustainTool(Long flowSustainToolId,
			String flowSustainToolShow) {
		this.flowSustainToolId = flowSustainToolId;
		this.flowSustainToolShow = flowSustainToolShow;
	}

	public Long getFlowSustainToolId() {
		return flowSustainToolId;
	}

	public void setFlowSustainToolId(Long flowSustainToolId) {
		this.flowSustainToolId = flowSustainToolId;
	}

	public String getFlowSustainToolShow() {
		return flowSustainToolShow;
	}

	public void setFlowSustainToolShow(String flowSustainToolShow) {
		this.flowSustainToolShow = flowSustainToolShow;
	}

	public Long getPreFlowSustainToolId() {
		return preFlowSustainToolId;
	}

	public void setPreFlowSustainToolId(Long preFlowSustainToolId) {
		this.preFlowSustainToolId = preFlowSustainToolId;
	}

	public Integer getSortId() {
		return sortId;
	}

	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}
	
	
	
}
