package epros.designer.gui.process.guide.explain;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.rule.RuleChooseDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;

/**
 * 相关制度
 * 
 * @author user
 * 
 */
public class JecnRelatedRule extends JecnFileDescriptionTable {

	private Logger log = Logger.getLogger(JecnRelatedRule.class);
	protected Long flowId = null;
	// 制度选择按钮初始化
	protected JButton selectBut = new JButton(JecnProperties.getValue("select"));

	protected List<JecnTreeBean> rules = new ArrayList<JecnTreeBean>();

	private List<Long> listIds = new ArrayList<Long>();

	public JecnRelatedRule(int index, String name, boolean isRequest, JecnPanel contentPanel, Long flowId, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowId = flowId;
		try {
			rules = ConnectionPool.getRuleAction().getRuleTreeBeanByFlowId(flowId);
		} catch (Exception e) {
			log.error("JecnRelatedRule is error", e);
		}
		titlePanel.add(selectBut);
		selectBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				select();
			}
		});

		initTable();
		table.setTableColumn(1, 180);
	}

	protected void select() {
		RuleChooseDialog ruleChooseDialog = new RuleChooseDialog(rules, 1L);
		ruleChooseDialog.setVisible(true);
		if (ruleChooseDialog.isOperation()) {
			Vector<Vector<String>> data = ((JecnTableModel) getTable().getModel()).getDataVector();
			if (rules != null && rules.size() > 0) {
				if (data != null) {
					data.clear();
				}
				for (JecnTreeBean jecnTreeBean : rules) {
					((DefaultTableModel) table.getModel()).addRow(this.getRowData(jecnTreeBean));
				}
			} else {
				// 清空表格
				((DefaultTableModel) table.getModel()).setRowCount(0);
			}
		}
	}

	@Override
	protected void dbClickMethod() {
		select();
	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		// 制度编号
		title.add(JecnProperties.getValue("ruleNum"));
		// 制度名称
		title.add(JecnProperties.getValue("systemName"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> ruleContent = new Vector<Vector<String>>();
		for (JecnTreeBean bean : rules) {
			// 制度ID
			listIds.add(bean.getId());
			ruleContent.add(getRowData(bean));
		}
		return ruleContent;
	}

	private Vector<String> getRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> ruleContentStr = new Vector<String>();
		// 制度ID
		ruleContentStr.add(String.valueOf(jecnTreeBean.getId()));
		// 制度编号
		ruleContentStr.add(jecnTreeBean.getNumberId() == null ? "" : jecnTreeBean.getNumberId());
		// 制度名称
		ruleContentStr.add(jecnTreeBean.getName());
		return ruleContentStr;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return JecnUtil.isChangeListLong(getResultListLong(), listIds);
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
