package com.jecn.epros.server.connector.impl;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMXMLBuilderFactory;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axis2.databinding.utils.BeanUtil;
import org.apache.axis2.util.StreamWrapper;
import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.NeedThrowException;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.connector.impl.JinkeTaskInfo.LinkResource.ResourceType;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.service.process.buss.FullPathManager;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.webservice.jinke.taskPub.BPMServiceLocator;
import com.jecn.webservice.jinke.taskPub.BPMServiceResult;
import com.jecn.webservice.jinke.taskPub.BPMServiceSoap_BindingStub;
import com.jecn.webservice.jinke.taskPub.DataItemParam;
import com.jecn.webservice.jinke.taskPub.StartWorkflow;

/**
 * 金科待办
 * 
 * @author hyl
 * 
 */
public class JinkeTaskInfo extends JecnBaseTaskSend {

	private static String TEST_ADDRESS = "http://218.70.38.42:8010/portal/WebServices/BPMService.asmx";
	private static String PRODUCE_ADDRESS = "";

	private IPersonDao personDao;

	/**
	 * 创建人物发送代办
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		BPMServiceResult result = createTaskSendToOA(beanNew, handlerPeopleIds, curPeopleId);
		log.info("金科待办创建：" + result.isSuccess());
	}

	private BPMServiceResult createTaskSendToOA(JecnTaskBeanNew beanNew, Set<Long> handlerPeopleIds, Long curPeopleId) {
		try {
			JecnUser user = personDao.get(curPeopleId);
			StartWorkflow workflow = new StartWorkflow();
			workflow.setFinishStart(false);
			workflow.setInstanceName(beanNew.getTaskName());
			workflow.setUserCode(user.getLoginName());
			workflow.setWorkflowCode("flowRelease");
			workflow.setParamValues(listToArr(getDataByTask(beanNew, user)));
			log.info("金科创建代办请求的数据" + JecnUtil.getJson(workflow));

			// 发送数据
			BPMServiceLocator service = new BPMServiceLocator();
			service.setBPMServiceSoapEndpointAddress(TEST_ADDRESS);
			BPMServiceSoap_BindingStub sub = new BPMServiceSoap_BindingStub(new URL(TEST_ADDRESS), service);
			BPMServiceResult result = sub.startWorkflow(workflow.getInstanceName(), workflow.getWorkflowCode(),
					workflow.getUserCode(), workflow.isFinishStart(), workflow.getParamValues());
			log.info("金科创建代办返回的结果集" + JecnUtil.getJson(result));
			if (result.isSuccess() == false) {
				throw new NeedThrowException("金科创建待办异常");
			}
			return result;
		} catch (Exception e) {
			log.error("", e);
			throw new NeedThrowException("金科创建待办异常", e);
		}
	}

	private DataItemParam[] listToArr(List<DataItemParam> result) {
		return result.toArray(new DataItemParam[result.size()]);
	}

	private List<DataItemParam> getDataByTask(JecnTaskBeanNew task, JecnUser user)
			throws UnsupportedEncodingException, XMLStreamException {

		List<DataItemParam> result = new ArrayList<DataItemParam>();
		// result.add(addParam("billId", "11"));
		// result.add(addParam("zhuti", "主题"));
		// result.add(addParam("att_name", "百度;搜狗"));
		// result.add(addParam("att_url",
		// "http://www.baidu.com;http://www.sougou.com"));
		// result.add(addParam("yjzrr", "zhouqiang2"));
		// result.add(addParam("ejzrr", "zhouqiang2"));

		result.add(addParam("billId", task.getId()));
		result.add(addParam("zhuti", task.getTaskName()));
		result.add(addParam("yjzrr", user.getLoginName()));
		result.add(addParam("ngr", user.getTrueName()));
		result.add(addParam("descStr", task.getTaskDesc()));
		if (task.getTaskType() == 0 || task.getTaskType() == 4) {
			result.add(addParam("lczrr", getDutyName(task)));
			result.add(addParam("lcjg", getFullPath(task)));
		}
		/** 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务 */
		// 附件链接
		addAttDataItem(task, result);

		// 审批记录,部门审核人作为
		String sql = "select c.* from jecn_task_people_conn_new a"
				+ "  inner join jecn_task_stage b on a.stage_id=b.id and b.stage_mark=2 and b.task_id=?"
				+ "  inner join jecn_user c on a.approve_pid=c.people_id";

		List<JecnUser> users = (List<JecnUser>) personDao.listNativeAddEntity(sql, JecnUser.class, task.getId());
		if (!JecnUtil.isEmpty(users)) {
			result.add(addParam("bmfzr", users.get(0).getLoginName()));
		}
		return result;
	}

	private String getDutyName(JecnTaskBeanNew task) {
		Integer typeResPeople = null;
		Long resPeopleId = null;
		if (task.getTaskType() == 0) {
			JecnFlowBasicInfoT jecnFlowBasicInfoT = (JecnFlowBasicInfoT) personDao.getSession().get(
					JecnFlowBasicInfoT.class, task.getRid());
			// 流程责任人
			typeResPeople = jecnFlowBasicInfoT.getTypeResPeople();
			resPeopleId = jecnFlowBasicInfoT.getResPeopleId();
		} else if (task.getTaskType() == 4) {
			JecnMainFlowT map = (JecnMainFlowT) personDao.getSession().get(JecnMainFlowT.class, task.getRid());
			typeResPeople = map.getTypeResPeople();
			resPeopleId = map.getResPeopleId();
		}
		String result = "";
		// 流程责任人
		if (typeResPeople != null && resPeopleId != null) {
			if (typeResPeople.intValue() == 0) {
				JecnUser jecnUser = this.personDao.get(resPeopleId);
				if (jecnUser != null) {
					result = jecnUser.getTrueName();
				}
			} else if (typeResPeople.intValue() == 1) {
				JecnFlowOrgImage jecnFlowOrgImage = (JecnFlowOrgImage) this.personDao.getSession().get(
						JecnFlowOrgImage.class, resPeopleId);
				if (jecnFlowOrgImage != null) {
					result = jecnFlowOrgImage.getFigureText();
				}
			}
		}
		return result;
	}

	private String getFullPath(JecnTaskBeanNew task) {
		FullPathManager manager = new FullPathManager(this.personDao, 0);
		manager.add(task.getRid());
		manager.generate();
		String fullPath = manager.getFullPath(task.getRid());
		if (StringUtils.isNotBlank(fullPath)) {
			return fullPath.replaceAll("/", "_");
		}
		return "";
	}

	public void addAttDataItem(JecnTaskBeanNew taskBean, List<DataItemParam> result)
			throws UnsupportedEncodingException, XMLStreamException {
		LinkResource mainFile = null;
		Long rid = taskBean.getRid();
		ResourceType type = null;
		String name = null;
		String hql = null;
		switch (taskBean.getTaskType()) {
		case 0:
		case 4:// 获取流程架构信息
			hql = "from JecnFlowStructureT where flowId=?";
			JecnFlowStructureT a = this.personDao.getObjectHql(hql, rid);
			name = a.getFlowName();
			type = ResourceType.PROCESS;
			break;
		case 1:// 获取文件信息
			hql = "from JecnFileBeanT where fileID=?";
			JecnFileBeanT f = this.personDao.getObjectHql(hql, rid);
			name = f.getFileName();
			type = ResourceType.FILE;
			break;
		case 2:// 获取制度
		case 3:// 制度
			hql = "from RuleT where id=?";
			RuleT r = this.personDao.getObjectHql(hql, rid);
			name = r.getRuleName();
			type = ResourceType.RULE;
			break;
		default:
			break;
		}

		mainFile = new LinkResource(rid, name, type);
		List<LinkResource> rs = getRelatedFiles(taskBean, "false");
		List<LinkResource> all = new ArrayList<LinkResource>();
		all.add(mainFile);
		all.addAll(rs);

		StringBuilder a = new StringBuilder();
		StringBuilder b = new StringBuilder();
		for (LinkResource r : all) {
			a.append(";");
			b.append(";");
			a.append(r.getName());
			b.append(getBrowerUrl(r));
		}

		result.add(addParam("att_name", a.toString().replaceFirst(";", "")));
		result.add(addParam("att_url", b.toString().replaceFirst(";", "")));
	}

	private String getBrowerUrl(LinkResource r) {
		String result = JecnContants.NEW_APP_CONTEXT;
		switch (r.getType()) {
		case PROCESS:
			result += "/processes/{id}/chart?p=0&f=task&h=0&abo=0";
			break;
		case FILE:
			result += "/files/{id}/info?p=0&f=task&h=0&abo=0";
			break;
		case RULE:
			result += "/rules/{id}/baseInfo?p=0&f=task&h=0&abo=0";
			break;
		default:
			break;
		}
		result = result.replaceAll("\\{id\\}", r.getId());
		return result;
	}

	/**
	 * 相关文件
	 * 
	 * @param taskBean
	 * @return
	 */
	private List<LinkResource> getRelatedFiles(JecnTaskBeanNew taskBean, String isApp) {
		ProcessMapper processMapper = new ProcessMapper();
		List<LinkResource> relatedFiles = new ArrayList<LinkResource>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("isPub", "_T");
		// 0为流程任务，1为文件任务，2为制度任务
		switch (taskBean.getTaskType()) {
		case 0:
			map.put("flowId", taskBean.getRid());
			List<BaseBean> processRelatedFiles = processMapper.findRelatedFiles(map);
			relatedFiles = getRelatedAppFiles(processRelatedFiles, ResourceType.FILE, isApp);
			break;
		case 4:// 获取流程架构信息
			map.put("flowId", taskBean.getRid());
			List<BaseBean> processMapRelatedFiles = processMapper.findMapRelatedFiles(map);
			relatedFiles = getRelatedAppFiles(processMapRelatedFiles, ResourceType.FILE, isApp);
			break;
		case 1:// 获取文件信息
			break;
		case 2:// 获取制度
		case 3:// 制度
			map.put("id", taskBean.getRid());
			relatedFiles = getRelatedAppFiles(processMapper.findTaskRuleFiles(map), ResourceType.FILE, isApp);
			break;
		default:
			break;
		}
		return relatedFiles;
	}

	/**
	 * 字符串转换成OMElement
	 * 
	 * @param xmlStr
	 *            需转换的字符串
	 * @return
	 * @throws XMLStreamException
	 * @throws UnsupportedEncodingException
	 */
	public static OMElement str2OMElement(String xmlStr) throws UnsupportedEncodingException, XMLStreamException {
		// OMElement xmlValue = new StAXOMBuilder(new
		// ByteArrayInputStream(xmlStr.getBytes("UTF-8"))).getDocumentElement();

		javax.xml.stream.XMLStreamReader reader = BeanUtil.getPullParser(xmlStr);
		StreamWrapper parser = new StreamWrapper(reader);
		StAXOMBuilder stAXOMBuilder = (StAXOMBuilder) OMXMLBuilderFactory.createStAXOMBuilder(OMAbstractFactory
				.getOMFactory(), parser);
		OMElement element = stAXOMBuilder.getDocumentElement();

		return element;
	}

	public void test() {

		javax.xml.stream.XMLStreamReader reader = BeanUtil.getPullParser("xx");
		StreamWrapper parser = new StreamWrapper(reader);
		StAXOMBuilder stAXOMBuilder = (StAXOMBuilder) OMXMLBuilderFactory.createStAXOMBuilder(OMAbstractFactory
				.getOMFactory(), parser);
		OMElement element = stAXOMBuilder.getDocumentElement();
	}

	public static void main(String[] args) throws Exception {
		JinkeTaskInfo task = new JinkeTaskInfo();
		task.createTaskSendToOA(null, null, null);
	}

	// ------------------------------------------------------------------

	protected class ProcessMapper {

		public List<BaseBean> findRelatedFiles(Map<String, Object> map) {
			String sql = findRelatedFilesSql(map).replaceAll("#\\{flowId\\}", map.get("flowId").toString());
			return toBaseBean(sql);
		}

		public List<BaseBean> findMapRelatedFiles(Map<String, Object> map) {
			String sql = findMapRelatedFilesSql(map).replaceAll("#\\{flowId\\}", map.get("flowId").toString());
			return toBaseBean(sql);
		}

		public List<BaseBean> findTaskRuleFiles(Map<String, Object> map) {
			String sql = findTaskRuleFileByRuleIdSql(map).replaceAll("#\\{id\\}", map.get("id").toString());
			return toBaseBean(sql);
		}
	}

	public String findTaskRuleFileByRuleIdSql(Map<String, Object> map) {
		String isPub = (String) map.get("isPub");
		String sql = "SELECT JF.VERSION_ID id, JF.FILE_NAME name" + "  FROM RULE_FILE" + isPub + " RF"
				+ " INNER JOIN RULE_TITLE" + isPub + " RT" + "    ON RT.ID = RF.RULE_TITLE_ID"
				+ "  INNER JOIN JECN_FILE" + isPub + " JF" + "    ON JF.FILE_ID = RF.RULE_FILE_ID"
				+ " WHERE RT.RULE_ID = #{id}" + " UNION " + "SELECT JF.VERSION_ID id, JF.FILE_NAME name"
				+ "  FROM RULE_STANDARDIZED_FILE" + isPub + " FSF" + " INNER JOIN JECN_FILE" + isPub + " JF"
				+ "    ON JF.FILE_ID = FSF.FILE_ID" + "   AND JF.del_state = 0" + " WHERE FSF.RELATED_ID = #{id}";
		return sql;
	}

	public List<BaseBean> toBaseBean(String sql) {
		List<Object[]> objs = this.personDao.listNativeSql(sql);
		List<BaseBean> result = new ArrayList<BaseBean>();
		for (Object[] obj : objs) {
			BaseBean b = new BaseBean();
			b.setId(Long.valueOf(obj[0].toString()));
			b.setName(obj[1].toString());
			result.add(b);
		}
		return result;
	}

	/**
	 * 获取流程相关附件
	 * 
	 * @param map
	 * @return
	 */
	public String findMapRelatedFilesSql(Map<String, Object> map) {
		String pub = map.get("isPub").toString();
		return "SELECT JF.VERSION_ID id, JF.FILE_NAME name" + "  FROM FLOW_STANDARDIZED_FILE" + pub + " FSF"
				+ " INNER JOIN JECN_FILE" + pub + " JF" + "    ON JF.FILE_ID = FSF.FILE_ID" + "   AND JF.del_state = 0"
				+ " WHERE FSF.FLOW_ID = #{flowId}" + " UNION " + "SELECT FT.VERSION_ID id, FT.FILE_NAME name"
				+ "  FROM JECN_FIGURE_FILE" + pub + " FF" + " INNER JOIN JECN_FILE" + pub + " FT"
				+ "    ON FF.FILE_ID = FT.FILE_ID" + "   AND FT.DEL_STATE = 0"
				+ " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + pub + " IT" + "    ON FF.FIGURE_ID = IT.FIGURE_ID"
				+ "   AND IT.FLOW_ID = #{flowId}";
	}

	/**
	 * 获取流程相关附件（表单样例）
	 * 
	 * @param map
	 * @return
	 */
	public String findRelatedFilesSql(Map<String, Object> map) {
		String pub = map.get("isPub").toString();
		String sql = "";
		if (JecnConfigTool.useNewInout()) {
			sql = findNewRelatedFilesSql(pub);
		} else {
			sql = findOldRelatedFilesSql(pub);
		}
		return sql;
	}

	public String findNewRelatedFilesSql(String pub) {
		return "SELECT JF.VERSION_ID id, JF.FILE_NAME name" + "  FROM JECN_FLOW_STRUCTURE_IMAGE" + pub + " JSI"
				+ "  LEFT JOIN JECN_ACTIVITY_FILE" + pub + " T" + "    ON T.FIGURE_ID = JSI.FIGURE_ID"
				+ "   AND T.FILE_TYPE = 0" + "  LEFT JOIN JECN_FILE" + pub + " JF" + "    ON T.FILE_S_ID = JF.FILE_ID"
				+ "   AND JF.DEL_STATE = 0" + " WHERE JSI.FLOW_ID = #{flowId}" + "   AND JF.VERSION_ID IS NOT NULL"
				+ " UNION " + " SELECT JF.VERSION_ID id, JF.FILE_NAME name" + "  FROM JECN_FLOW_STRUCTURE_IMAGE" + pub
				+ " JSI" + "  LEFT JOIN  JECN_FIGURE_IN_OUT_SAMPLE" + pub + " SAM" + "  ON SAM.FLOW_ID = JSI.FLOW_ID"
				+ "  AND SAM.TYPE = 0" + "   LEFT JOIN JECN_FILE" + pub + " JF" + "    ON SAM.FILE_ID = JF.FILE_ID"
				+ "   AND JF.DEL_STATE = 0" + " WHERE JSI.FLOW_ID = #{flowId}" + "   AND JF.VERSION_ID IS NOT NULL"
				+ " UNION " + "SELECT FT.VERSION_ID id, FT.FILE_NAME name" + "  FROM JECN_FIGURE_FILE" + pub + " FF"
				+ " INNER JOIN JECN_FILE" + pub + " FT" + "    ON FF.FILE_ID = FT.FILE_ID" + "   AND FT.DEL_STATE = 0"
				+ " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + pub + " IT" + "    ON FF.FIGURE_ID = IT.FIGURE_ID"
				+ "   AND IT.FLOW_ID = #{flowId}";
	}

	public String findOldRelatedFilesSql(String pub) {
		return "SELECT JF.VERSION_ID id, JF.FILE_NAME name" + "  FROM JECN_FLOW_STRUCTURE_IMAGE" + pub + " JSI"
				+ "  LEFT JOIN JECN_ACTIVITY_FILE" + pub + " T" + "    ON T.FIGURE_ID = JSI.FIGURE_ID"
				+ "  LEFT JOIN JECN_FILE" + pub + " JF" + "    ON T.FILE_S_ID = JF.FILE_ID" + "   AND JF.DEL_STATE = 0"
				+ " WHERE JSI.FLOW_ID = #{flowId}" + "   AND JF.VERSION_ID IS NOT NULL" + " UNION "
				+ "SELECT JF.VERSION_ID id, JF.FILE_NAME name" + "  FROM JECN_FLOW_STRUCTURE_IMAGE" + pub + " JSI"
				+ "  LEFT JOIN JECN_MODE_FILE" + pub + " T" + "    ON T.FIGURE_ID = JSI.FIGURE_ID"
				+ "  LEFT JOIN JECN_FILE" + pub + " JF" + "    ON T.FILE_M_ID = JF.FILE_ID" + "   AND JF.DEL_STATE = 0"
				+ " WHERE JSI.FLOW_ID = #{flowId}" + "   AND JF.VERSION_ID IS NOT NULL" + " UNION "
				+ "SELECT FT.VERSION_ID id, FT.FILE_NAME name" + "  FROM JECN_FIGURE_FILE" + pub + " FF"
				+ " INNER JOIN JECN_FILE" + pub + " FT" + "    ON FF.FILE_ID = FT.FILE_ID" + "   AND FT.DEL_STATE = 0"
				+ " INNER JOIN JECN_FLOW_STRUCTURE_IMAGE" + pub + " IT" + "    ON FF.FIGURE_ID = IT.FIGURE_ID"
				+ "   AND IT.FLOW_ID = #{flowId}";
	}

	private List<LinkResource> getRelatedAppFiles(List<BaseBean> baseBeans, ResourceType resourceType, String isApp) {
		List<LinkResource> relatedFiles = new ArrayList();
		if (baseBeans == null) {
			return relatedFiles;
		}
		LinkResource link = null;
		for (BaseBean bean : baseBeans) {
			link = new LinkResource(bean.getId(), bean.getName(), resourceType);
			link.setIsPublic(0);
			link.setAppRequest("true".equals(isApp));
			relatedFiles.add(link);
		}
		return relatedFiles;
	}

	/**
	 * @author weidp
	 */
	public static class LinkResource {

		private String id;

		private String name;
		/**
		 * 当前请求资源
		 */
		private ResourceType type;
		/**
		 * 属于哪个资源
		 */
		private TreeNodeUtils.NodeType from;

		/**
		 * 0 临时 1 正式
		 **/
		private int isPublic = 1;

		private String url;

		private Long historyId;

		private boolean appRequest;

		private Integer approveType;// 是否废止

		public enum ResourceType {
			PERSON("人员", "Personnel"), // 人员
			PROCESS("流程", "Process"), // 流程
			PROCESS_FILE("流程（文件）", "Process file"), // 流程（文件）
			PROCESS_MAP("流程架构", "Process Map"), // 流程地图
			ACTIVITY("活动", "Activitiy"), // 活动
			ORGANIZATION("组织", "Organization"), // 组织
			POSITION("岗位", "job"), // 岗位
			FILE("文件", "File"), // 文件
			RULE("制度", "Institution"), // 制度
			RULE_DIR("制度目录", "Institution"), // 制度
			STANDARD("标准", "Standard"), // 标准
			RISK("风险", "Risk"), // 风险
			FILE_VIEW, // 文件查看
			SUGGEST, // 合理化建议
			TASK_EDIT, // 编辑EDIT
			TASK_VIEW, // 阅读VIEW
			TASK_RETRACT, // 撤回RETRACT
			TASK_DELETE, // 删除DELETE
			TASK_APPROVE, // 审批APPROVE
			TASK_RESUBMIT, // 重新提交RESUBMIT
			HISTORY, // 文控
			TASK_ORGANIZE, // 整理意见ORGANIZE
			KPI, // 流程KPI
			TEST_RUN_FILE;// 任务试运行文件

			private String name = "";
			private String enName = "";

			ResourceType() {

			}

			ResourceType(String name, String enName) {
				this.name = name;
				this.enName = enName;
			}

			public String getName() {
				return this.name;
			}

			public String getEnName() {
				return enName;
			}

		}

		public LinkResource(Object id, String name) {
			this(id == null ? "" : id.toString(), name, null);
		}

		public LinkResource(Object id, String name, ResourceType type) {
			this(id == null ? "" : id.toString(), name, type);

		}

		public LinkResource(Object id, String name, ResourceType type, TreeNodeUtils.NodeType from) {
			this(id == null ? "" : id.toString(), name, type, from);
		}

		/*
		 * public LinkResource(Object id, String name, ResourceType type,
		 * TreeNodeUtils.NodeType from, String url) {
		 * this(StringUtils.toString(id), name, type, from); this.url = url; }
		 */

		public LinkResource(String id, String name, ResourceType type, int isPublic, TreeNodeUtils.NodeType from) {
			this(id, name, type);
			this.isPublic = isPublic;
			this.from = from;
		}

		public LinkResource(String id, String name, ResourceType type) {
			super();
			this.id = id;
			this.name = name;
			this.type = type;
		}

		public LinkResource(String id, String name, ResourceType type, TreeNodeUtils.NodeType from) {
			this(id, name, type);
			this.from = from;
		}

		public LinkResource(String id, String name, ResourceType type, int isPublic, TreeNodeUtils.NodeType from,
				String url) {
			this(id, name, type);
			this.isPublic = isPublic;
			this.from = from;
			this.url = url;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public ResourceType getType() {
			return type;
		}

		public void setType(ResourceType type) {
			this.type = type;
		}

		public int getIsPublic() {
			return isPublic;
		}

		public void setIsPublic(int isPublic) {
			this.isPublic = isPublic;
		}

		public TreeNodeUtils.NodeType getFrom() {
			return from;
		}

		public void setFrom(TreeNodeUtils.NodeType from) {
			this.from = from;
		}

		// public String getUrl() {
		// if (type == null || id == null || id == "") {
		// return url;
		// }
		// if (from == null && "FILE".equals(type.toString())) {
		// url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "",
		// appRequest);
		// } else if (from == null && "RULE".equals(type.toString())) {
		// url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true,
		// "ruleLocal", appRequest);
		// } else if (from == TreeNodeUtils.NodeType.process &&
		// "FILE".equals(type.toString()) && id != null) {
		// url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "",
		// appRequest);
		// } else if (from == TreeNodeUtils.NodeType.processMap &&
		// "FILE".equals(type.toString()) && id != null) {
		// url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "",
		// appRequest);
		// } else if (from == TreeNodeUtils.NodeType.position &&
		// "FILE".equals(type.toString()) && id != null) {
		// url = HttpUtil.getDownLoadFileHttp(Long.valueOf(id), true, "",
		// appRequest);
		// }
		// return url;
		// }

		public void setUrl(String url) {
			this.url = url;
		}

		public Long getHistoryId() {
			return historyId;
		}

		public void setHistoryId(Long historyId) {
			this.historyId = historyId;
		}

		public boolean isAppRequest() {
			return appRequest;
		}

		public void setAppRequest(boolean appRequest) {
			this.appRequest = appRequest;
		}

		public Integer getApproveType() {
			return approveType;
		}

		public void setApproveType(Integer approveType) {
			this.approveType = approveType;
		}
	}

	// private static class HttpUtil {
	// private static String DOWN_LOAD_FILE_ACTION = "downloadFile.action?";
	//
	// private static String DB_SYNC_CONFIG = "getSyncConfig.action";
	// /**
	// * 流程文件
	// */
	// private static String DOWN_LOAD_PROCESS_DOC = "printFlowDoc.action?";
	//
	// private static String VIEW_PROCESS_DOC = "viewProcess.action?";
	// /**
	// * 制度文件
	// */
	// private static String DOWN_LOAD_RULE_DOC = "downloadRuleFile.action?";
	// /**
	// * 流程图
	// */
	// private static String DOWN_LOAD_IMAGE = "downloadMapIconAction.action?";
	//
	// /**
	// * 流程图
	// */
	// private static String VIEW_PROCESS_IMAGE = "viewProcessIcon.action?";
	//
	// /**
	// * 邮箱登录
	// */
	// private static String LOGIN_MAIL = "loginMail.action";
	//
	// public static String getDownLoadFileHttp(Long fileId, boolean isPub,
	// String ruleLocal) {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL +
	// DOWN_LOAD_FILE_ACTION + "fileId=" + fileId
	// + "&isPub=" + isPub + "&isPerm=false&downFileType=" + ruleLocal;
	// }
	//
	// public static String getDownLoadFileHttp(Long fileId, boolean isPub,
	// String ruleLocal, boolean isApp) {
	// return getDownLoadFileHttp(fileId, isPub, ruleLocal) + (isApp == true ?
	// "&isApp=" + isApp : "");
	// }
	//
	// public static String getDownLoadHistoryFileHttp(Long fileId, boolean
	// isPub, String ruleLocal, boolean isHistory) {
	// return getDownLoadFileHttp(fileId, isPub, ruleLocal) + "&isHistory=" +
	// isHistory;
	// }
	//
	// public static String getSyncConfigUrl() {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + DB_SYNC_CONFIG;
	// }
	//
	// /**
	// * 查看制度
	// *
	// * @param id
	// * @return
	// */
	// public static String getRule(String id, String isPub, String userCode) {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + LOGIN_MAIL
	// + "?accessType=mailOpenDominoRuleModelFile&mailRuleId=" + id + "&isPub="
	// + isPub + "&userCode="
	// + userCode;
	// }
	//
	// /**
	// * 查看流程或架构详情
	// *
	// * @param id
	// * @return
	// */
	// public static String getProcessDetail(String id, String isPub, String
	// userCode) {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + LOGIN_MAIL
	// + "?accessType=mailOpenDominoMap&mailFlowId=" + id + "&isPub=" + isPub +
	// "&userCode=" + userCode;
	// }
	//
	// /**
	// * 制度文件链接
	// *
	// * @param rule
	// * @param isPub
	// * @return
	// */
	// public static String getRuleFileUrl(RuleT rule, boolean isPub, boolean
	// isApp) {
	// return getDownLoadFileHttp(rule.getFileId(), isPub, rule.getIsFileLocal()
	// == 1 ? "ruleLocal" : "")
	// + (isApp == true ? "&isApp=" + isApp : "");
	// }
	//
	// public static String getDownLoadRuleDocUrl(Long id, boolean isPub,
	// boolean isApp) {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL +
	// DOWN_LOAD_RULE_DOC + "ruleId=" + id + "&isPub="
	// + isPub + (isApp == true ? "&isApp=" + isApp : "");
	// }
	//
	// public static String getProcessDocUrl(Long id, boolean isPub, boolean
	// isApp, Long historyId) {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL +
	// DOWN_LOAD_PROCESS_DOC + "flowId=" + id + "&isPub="
	// + isPub + (isApp == true ? "&isApp=" + isApp : "" + "&historyId=" +
	// historyId);
	// }
	//
	// public static String getViewProcessDocUrl(Long id, boolean isPub, boolean
	// isApp) {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL
	// + VIEW_PROCESS_DOC
	// + "flowId="
	// + id
	// + "&relatedId="
	// + id
	// + "&isPub="
	// + isPub
	// + (isApp == true ? "&isApp=" + isApp : "" + "&userId="
	// + AuthenticatedUserUtil.getPrincipal().getId());
	// }
	//
	// public static String getProcessChartUrl(Long id) {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + "processes/" + id
	// + "/chart?opts=tree";
	// }
	//
	// /**
	// * 下载图片路径
	// *
	// * @param id
	// * @param mapIconPath
	// * @param isPub
	// * @return
	// */
	// public static String getProcessImageUrl(Long id, String mapIconPath,
	// String isPub, boolean isApp) {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL + DOWN_LOAD_IMAGE +
	// "flowId=" + id + "&mapIconPath="
	// + mapIconPath + "&isPub=" + isPub + "&isApp=" + isApp + "&userId="
	// + AuthenticatedUserUtil.getPrincipal().getId();
	// }
	//
	// /**
	// * 下载图片路径
	// *
	// * @param id
	// * @param mapIconPath
	// * @param isPub
	// * @return
	// */
	// public static String getViewProcessImageUrl(Long id, String mapIconPath,
	// String isPub, boolean isApp) {
	// return RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL +
	// VIEW_PROCESS_IMAGE + "relatedId=" + id
	// + "&mapIconPath=" + mapIconPath + "&isPub=" + isPub + "&isApp=" + isApp;
	// }
	// }

	private static class RPCServiceConfiguration {

		public static final String DEF_RPC_SERVER_HTTPURL = "";

	}

	private static class BaseBean {
		private Long id;
		private String name;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	private static class TreeNodeUtils {

		public enum TreeType {
			process, rule, standard, risk, file, organization, position, people, auth
		}

		public enum NodeType {
			process, // 流程
			processFile, // 流程（文件）
			processMap, // 流程地图
			ruleDir, ruleModelFile, ruleFile, rule, // 制度
			standardDir, standard, standardProcess, standardProcessMap, standardClauseRequire, standardClause, // 标准,
			riskDir, risk, // 风险
			fileDir, file, // 文件
			organization, position, people, // 组织、岗位、人员
			auth, // 权限
			task, inventory, search, fileContent, // 文件内容
			processFileContent, // 流程（文件）内容
			ruleFileContent, // 制度文件内容
			star, // 收藏
			main, // 主页
			activity, // 活动
			Abolish
			// 废止
		}

		public static void setNodeType(TreeNode node, TreeType treeType) {
			if (node == null) {
				return;
			}
			if (treeType == null) {
				treeType = TreeType.process;
			}
			switch (treeType) {
			case process:
				setNodeTypeByTreeType(node, treeType);
				break;
			case rule:
				setNodeTypeByTreeType(node, treeType);
				break;
			case standard:
				setNodeTypeByTreeType(node, treeType);
				break;
			case risk:
				setNodeType(node, NodeType.riskDir, NodeType.risk);
				break;
			case file:
				setNodeType(node, NodeType.fileDir, NodeType.file);
				break;
			case position:
			case organization:
				setNodeType(node, NodeType.organization, NodeType.position);
				break;
			case people:
				setNodeTypeByTreeType(node, treeType);
				break;
			default:
				break;
			}
		}

		private static class TreeNode {

			/**
			 * 子节点
			 */
			private List<TreeNode> children = new ArrayList<TreeNode>();
			/**
			 * 节点id
			 */
			private String id;
			/**
			 * 节点名称
			 */
			private String text;
			/**
			 * 是否为叶子节点
			 */
			private boolean isLeaf;
			/**
			 * 节点类型
			 */
			private NodeType type;
			/**
			 * 0目录 1 文件
			 */
			private int isDir;

			/**
			 * 父节点名称
			 */
			private String pText;
			/**
			 * 父节点id
			 */
			private String pId;

			/**
			 * 是否为选中的节点
			 */
			private boolean select;

			private boolean isChecked;
			/**
			 * 节点类型（流程节点时，默认是流程和架构，1：流程（文件）节点）
			 */
			private int nodeType;

			private String status;
			/**
			 * 是否有下载权限
			 */
			private boolean isDownloadAuth;

			/**
			 * 流程、制度 ZIP附件下载
			 */
			private boolean isZipDownloadAuth;

			private Integer isFileLocal;// g020 0是从文件库关联的1是从本地上传的

			// 人员登录名称
			private String loginName;

			/**
			 * 虚拟架构类型目前只有适用部门的
			 */
			private NodeType inventedType;
			private String inventedId;

			public TreeNode() {

			}

			public TreeNode(String id, String text, boolean isLeaf, NodeType type, int isDir, String pText, String pId,
					boolean select, boolean isChecked) {
				this.id = id;
				this.text = text;
				this.isLeaf = isLeaf;
				this.type = type;
				this.isDir = isDir;
				this.pText = pText;
				this.pId = pId;
				this.select = select;
				this.isChecked = isChecked;
			}

			public void setIsLeaf(int childsSize) {
				if (childsSize > 0) {
					this.isLeaf = false;
				} else {
					this.isLeaf = true;
				}
			}

			// ---------------------------------------------------------

			public NodeType getInventedType() {
				return inventedType;
			}

			public void setInventedType(NodeType inventedType) {
				this.inventedType = inventedType;
			}

			public String getInventedId() {
				return inventedId;
			}

			public void setInventedId(String inventedId) {
				this.inventedId = inventedId;
			}

			public boolean getIsLeaf() {
				return isLeaf;
			}

			public Integer getIsFileLocal() {
				return isFileLocal;
			}

			public void setIsFileLocal(Integer isFileLocal) {
				this.isFileLocal = isFileLocal;
			}

			public boolean isSelect() {
				return select;
			}

			public void setSelect(boolean select) {
				this.select = select;
			}

			public String getpId() {
				return pId;
			}

			public void setpId(String pId) {
				this.pId = pId;
			}

			public String getpText() {
				return pText;
			}

			public void setpText(String pText) {
				this.pText = pText;
			}

			public NodeType getType() {
				return type;
			}

			public void setType(NodeType type) {
				this.type = type;
			}

			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public String getText() {
				return text;
			}

			public void setText(String text) {
				this.text = text;
			}

			public List<TreeNode> getChildren() {
				return children;
			}

			public void setChildren(List<TreeNode> children) {
				this.children = children;
			}

			public boolean isLeaf() {
				return isLeaf;
			}

			public void setLeaf(boolean leaf) {
				isLeaf = leaf;
			}

			public int getIsDir() {
				return isDir;
			}

			public void setIsDir(int isDir) {
				this.isDir = isDir;
			}

			public boolean getIsChecked() {
				return isChecked;
			}

			public void setIsChecked(boolean isChecked) {
				isChecked = isChecked;
			}

			public int getNodeType() {
				return nodeType;
			}

			public void setNodeType(int nodeType) {
				this.nodeType = nodeType;
			}

			public String getStatus() {
				return status;
			}

			public void setStatus(String status) {
				this.status = status;
			}

			public boolean isDownloadAuth() {
				return isDownloadAuth;
			}

			public void setDownloadAuth(boolean downloadAuth) {
				isDownloadAuth = downloadAuth;
			}

			public boolean isZipDownloadAuth() {
				return isZipDownloadAuth;
			}

			public void setZipDownloadAuth(boolean zipDownloadAuth) {
				isZipDownloadAuth = zipDownloadAuth;
			}

			public String getLoginName() {
				return loginName;
			}

			public void setLoginName(String loginName) {
				this.loginName = loginName;
			}
		}

		private static void setNodeTypeByTreeType(TreeNode node, TreeType treeType) {
			int type = node.getIsDir();
			NodeType nodeType = null;
			if (treeType == TreeType.rule) {
				// 0：目录 1：制度模板文件 2：制度文件
				if (type == 0) {
					nodeType = NodeType.ruleDir;
				} else if (type == 1) {
					nodeType = NodeType.ruleModelFile;
				} else if (type == 2) {
					nodeType = NodeType.ruleFile;
				}

			} else if (treeType == TreeType.standard) {
				// 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
				if (type == 0) {
					nodeType = NodeType.standardDir;
				} else if (type == 1) {
					nodeType = NodeType.standard;
				} else if (type == 2) {
					nodeType = NodeType.standardProcess;
				} else if (type == 3) {
					nodeType = NodeType.standardProcessMap;
				} else if (type == 4) {
					nodeType = NodeType.standardClause;
				} else if (type == 5) {
					nodeType = NodeType.standardClauseRequire;
				}
			} else if (treeType == TreeType.people) {
				// 0部门 1岗位 2人员
				if (type == 0) {
					nodeType = NodeType.organization;
				} else if (type == 1) {
					nodeType = NodeType.position;
				} else if (type == 2) {
					nodeType = NodeType.people;
				}
			} else if (treeType == TreeType.process) {
				if (type == 1) {// 流程节点
					if (node.getNodeType() == 1) {// processFile节点
						nodeType = NodeType.processFile;
					} else {
						nodeType = NodeType.process;
					}
				} else {
					nodeType = NodeType.processMap;
				}
			}
			node.setType(nodeType);
		}

		private static void setNodeType(TreeNode node, NodeType dirType, NodeType baseType) {
			if (node.getIsDir() == 0) {
				node.setType(dirType);
			} else {
				node.setType(baseType);
			}
		}

		public static List<TreeNode> setNodeType(List<TreeNode> nodes, TreeType type) {
			if (nodes == null) {
				return nodes;
			}
			Iterator<TreeNode> iterator = nodes.iterator();
			while (iterator.hasNext()) {
				TreeNode next = iterator.next();
				setNodeType(next, type);
			}
			return nodes;
		}

		public static List<TreeNode> setNodeType(List<TreeNode> nodes, NodeType type) {
			if (nodes == null) {
				return nodes;
			}
			Iterator<TreeNode> iterator = nodes.iterator();
			while (iterator.hasNext()) {
				TreeNode next = iterator.next();
				next.setType(type);
			}
			return nodes;
		}

	}

	private static DataItemParam addParam(String key, Object value) throws UnsupportedEncodingException,
			XMLStreamException {
		DataItemParam result = new DataItemParam();
		result.setItemName(key);
		result.setItemValue(value);
		return result;
	}

	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew, JecnUser jecnUser, JecnUser createUser) {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		// TODO Auto-generated method stub

	}
}
