package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.action.web.login.ad.lianhedianzi.ADReader;
import com.jecn.epros.server.action.web.login.ad.lianhedianzi.LianHeDianZiAfterItem;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class ImportUserByAD extends AbstractImportUser {

	protected final Log log = LogFactory.getLog(this.getClass());

	private List<UserBean> userBeans = null;

	private List<DeptBean> deptBeans = null;

	public ImportUserByAD() {
		try {

			if (JecnContants.otherLoginType == 36) {
				ImportUserByReadAD byReadAD = new ImportUserByReadAD();
				userBeans = byReadAD.getUserBeanList();
			} else {
				ADReader reader = new ADReader();
				reader.searchInformation("ou=" + LianHeDianZiAfterItem.BASE + "," + LianHeDianZiAfterItem.ADDOMAIN,
						LianHeDianZiAfterItem.FILTER);
				userBeans = reader.getUserPosBeanList();
				deptBeans = reader.getDeptBeanList();
			}

		} catch (Exception e) {
			log.error("获取AD数据时出现异常", e);
		}
	}

	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		return deptBeans;
	}

	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		return userBeans;
	}

}
