package epros.designer.gui.timeline;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import epros.draw.data.figure.JecnActiveData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

public class TimeLimitPanelData {
	private JecnDrawDesktopPane workFlow = JecnDrawMainPanel.getMainPanel().getWorkflow();
	/** 竖线的高度 */
	public static final int PANEL_HEIGHT = 35;
	/** 横线X轴起点偏移量 */
	public static final int HLINE_OFFSET_END_X = 25;
	/** 横线X轴结束点偏移量 */
	public static final int HLINE_OFFSET_START_X = 15;
	private int panelX;
	private int panelY;
	private int panelHeight;

	private List<Integer> vLineStartList = new ArrayList<Integer>();

	/** 目标 */
	private List<BigDecimal> timeCostAmountTargetList = new ArrayList<BigDecimal>();
	/** 现状值 */
	private List<BigDecimal> timeCostAmountStatusList = new ArrayList<BigDecimal>();

	private List<JecnActiveData> maxTimeLimitCostActivities;
	// private JecnBaseVHLinePanel firstVLine = JecnRoleMobile.getFristVLine(workFlow);
	private BigDecimal timeLimitAmount;

	/** 目标值汇总 */
	private BigDecimal sumTargetValues;
	/** 现状值汇总 */
	private BigDecimal sumStatusValues;

	public TimeLimitPanelData() {
		init();
	}

	private void init() {
		MaxPathAlgo pathAlgo = new MaxPathAlgo();
		this.maxTimeLimitCostActivities = pathAlgo.getActiveDataListOfMaxTimeCost();
		if (maxTimeLimitCostActivities.size() == 0) {
			return;
		}
		// this.panelX = (int) firstVLine.getFlowElementData().getVhX();
		this.panelX = pathAlgo.getStartActive().getFigureDataCloneable().getZoomFigureDataProcess(
				workFlow.getWorkflowScale()).getX() - 20;
		this.panelY = 70;
		this.panelHeight = PANEL_HEIGHT + 20;
		this.timeLimitAmount = pathAlgo.getMaxTimeCostOfActive();
		// 办理时限累加
		BigDecimal targetAmount = new BigDecimal("0");
		BigDecimal statusAmount = new BigDecimal("0");
		for (JecnActiveData activeData : maxTimeLimitCostActivities) {
			targetAmount = targetAmount.add(activeData.getTargetValue());
			statusAmount = statusAmount.add(activeData.getStatusValue());
			this.addTimeTargetCost(activeData.getTargetValue());
			this.addTimeStatusCost(activeData.getStatusValue());
		}
		sumTargetValues = targetAmount;
		sumStatusValues = statusAmount;
		zoomData();
	}

	/**
	 * 获取最后一个活动的右上角位置点（此数据控制面板及时间轴的宽度【最后一个活动的右上角位置点X-角色移动竖线X+偏移量】）
	 * 
	 * @return
	 */
	private int getLastActiveX() {
		if (maxTimeLimitCostActivities.size() == 0) {
			return 0;
		}
		JecnActiveData lastActive = maxTimeLimitCostActivities.get(maxTimeLimitCostActivities.size() - 1);
		int val = getActiveRightTopX(lastActive) + HLINE_OFFSET_END_X;
		return val;
	}

	/**
	 * 获取活动元素的右上角位置点X
	 * 
	 * @param active
	 * @return
	 */
	private int getActiveRightTopX(JecnActiveData active) {
		// return (int)
		// (active.getFigureDataCloneable().getZoomFigureDataProcess(workFlow.getWorkflowScale())
		// .getResetTopRightPoint().getX()// 最后一个活动的右上角点X
		// - firstVLine.getFlowElementData().getVXInt());
		return (int) (active.getFigureDataCloneable().getZoomFigureDataProcess(workFlow.getWorkflowScale())
				.getResetTopRightPoint().getX()// 最后一个活动的右上角点X
		- this.panelX);
	}

	/**
	 * 重新获取竖线的位置点信息（在放大缩小或拖动活动时重新获取数据）
	 */
	public void zoomData() {
		vLineStartList.clear();
		for (JecnActiveData activeData : maxTimeLimitCostActivities) {
			this.addVLineX(getActiveRightTopX(activeData));
		}
	}

	public int getDataSize() {
		return maxTimeLimitCostActivities.size();
	}

	public JecnActiveData getActiveDataSource(int index) {
		if (index < 0 || index >= maxTimeLimitCostActivities.size()) {
			return null;
		} else {
			return maxTimeLimitCostActivities.get(index);
		}
	}

	public String getTimeCost(int index) {
		if (index < 0 || index >= timeCostAmountTargetList.size()) {
			return "Null";
		} else {
			String cost = timeCostAmountTargetList.get(index).toString();
			return cost.endsWith(".0") ? cost.substring(0, cost.indexOf(".0")) : cost;
		}
	}

	public String getStatusTimeCost(int index) {
		if (index < 0 || index >= timeCostAmountStatusList.size()) {
			return "Null";
		} else {
			String cost = timeCostAmountStatusList.get(index).toString();
			return cost.endsWith(".0") ? cost.substring(0, cost.indexOf(".0")) : cost;
		}
	}

	public Integer getVLineX(int index) {
		if (index < 0 || index >= vLineStartList.size()) {
			return null;
		} else {
			return vLineStartList.get(index);
		}
	}

	public boolean addTimeTargetCost(BigDecimal e) {
		return timeCostAmountTargetList.add(e);
	}

	public boolean addTimeStatusCost(BigDecimal e) {
		return timeCostAmountStatusList.add(e);
	}

	public boolean addVLineX(int vLineX) {
		return vLineStartList.add(vLineX);
	}

	public int getPanelX() {
		return panelX;
	}

	public void setPanelX(int panelX) {
		this.panelX = panelX;
	}

	public int getPanelWidth() {
		return getLastActiveX();
	}

	public int getPanelHeight() {
		return panelHeight;
	}

	public int getPanelY() {
		return panelY;
	}

	public int getVlineStartY() {
		return DrawCommon.convertDoubleToInt(PANEL_HEIGHT / 2 * workFlow.getWorkflowScale());
	}

	public int getVlineEndY() {
		return DrawCommon.convertDoubleToInt(PANEL_HEIGHT * workFlow.getWorkflowScale());
	}

	public BigDecimal getTimeLimitAmount() {
		return timeLimitAmount;
	}

	public BigDecimal getSumTargetValues() {
		return sumTargetValues;
	}

	public BigDecimal getSumStatusValues() {
		return sumStatusValues;
	}

}