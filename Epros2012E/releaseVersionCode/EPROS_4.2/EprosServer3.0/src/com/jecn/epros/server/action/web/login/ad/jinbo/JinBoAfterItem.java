package com.jecn.epros.server.action.web.login.ad.jinbo;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.util.JecnPath;

public class JinBoAfterItem {
	private static Logger log = Logger.getLogger(JinBoAfterItem.class);
	public static String SSOLoginUrl = null;
	public static String SSOServiceUrl = null;

	public static void start() {
		log.info("开始读取配置文件内容");
		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH + "cfgFile/jinbo/JinBoConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);
			SSOLoginUrl = JecnContants.NEW_APP_CONTEXT + "/page/login";
		} catch (Exception e) {
			log.error(e);
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
		}
	}
}