package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.sqlprovider.server3.Common;

public class FlowActiveRelatedStandardBean {
    private Long id;
    /**
     * 活动ID
     */
    private String name;
    /**
     * 关联类型 关联类型 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求
     */
    private int relaType;

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public int getRelaType() {
        return relaType;
    }

    public void setRelaType(int relaType) {
        this.relaType = relaType;
    }

    public LinkResource getLink() {
        return new LinkResource(id, name, LinkResource.ResourceType.STANDARD, TreeNodeUtils.NodeType.process);
    }

    public String getStandTypeStr() {
        if (AuthenticatedUserUtil.isLanguageType() == 0) {
            return Common.standardTypeVal(relaType);
        } else {
            return Common.standardTypeEnVal(relaType);
        }
    }
}
