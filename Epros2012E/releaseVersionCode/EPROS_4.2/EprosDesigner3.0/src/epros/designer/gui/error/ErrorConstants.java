package epros.designer.gui.error;

import epros.designer.util.JecnProperties;

public class ErrorConstants {
	/** 活动编号不能为空,请修改！ */
	public final static String ACTIVE_NUMBER = JecnProperties.getValue("active_number");
	/** 活动名称不能为空,请修改！ */
	public final static String ACTIVE_NANE = JecnProperties.getValue("active_name");
	/** 活动输入不能为空,请修改！ */
	public final static String ACTIVE_INPUT = JecnProperties.getValue("active_input");
	/** 活动输出不能为空,请修改！ */
	public final static String ACTIVE_OUTPUT = JecnProperties.getValue("active_output");
	/** 活动说明不能为空,请修改！ */
	public final static String ACTIVE_SHOW = JecnProperties.getValue("active_show");
	/** 关键说明不能为空,请修改！ */
	public final static String KEY_ACTIVE_SHOW = JecnProperties.getValue("key_active_show");
	/** 正反返工符未匹配,请修改！ */
	public final static String ARROW_HEAD = JecnProperties.getValue("arrow_head");
	/** 返工符名称不匹配,请修改！ */
	public final static String ARROW_HEAD_NAME = JecnProperties.getValue("arrow_head_name");
	/** 返工符名称不能为空,请修改！ */
	public final static String ARROW_HEAD_NAME_NULL = JecnProperties.getValue("arrow_head_name_null");
	/** 角色个数超过上限！ */
	public final static String ROLE_MAX_COUNT = JecnProperties.getValue("role_max_count");
	/** 角色个数低于下限！ */
	public final static String ROLE_MIN_COUNT = JecnProperties.getValue("role_min_count");
	/** 决策框无连接线,请添加！ */
	public final static String RHOMBUS_NO_LINES = JecnProperties.getValue("rhombus_no_lines");
	/** 活动个数超过上限！ */
	public final static String ACTIVE_MAX_COUNT = JecnProperties.getValue("active_max_count");
	/** 活动个数低于下限！ */
	public final static String ACTIVE_MIN_COUNT = JecnProperties.getValue("active_min_count");
	/** 决策框缺少连接线,请修改！ */
	public final static String RHOMBUS_LESS_LINES = JecnProperties.getValue("rhombus_less_lines");
	/** 存在角色、活动没有对应,请修改！ */
	public final static String ROLE_HASH_ACTIVE = JecnProperties.getValue("role_hash_active");
	/** 角色职责不能为空,请修改！ */
	public final static String ROLE_RULES = JecnProperties.getValue("role_rules");
	/** 流程应存在一个主责岗位（主责岗位唯一）,请修改！ */
	public final static String MAIN_POSITION = JecnProperties.getValue("main_position");
	/** 图形元素缺少连接线,请添加！ */
	public final static String FIGURE_NO_LINE = JecnProperties.getValue("figure_no_line");
	/** 图形压线,请修改！ */
	public final static String ISCOVER = JecnProperties.getValue("iscover");
	/** 角色: */
	public final static String ROLE_TEXT = JecnProperties.getValue("personInfoRoleName");
	/** 缺少岗位,请修改！ */
	public final static String ROLE_NO_POSITION = JecnProperties.getValue("role_no_position");
	/** 缺少主责岗位，请修改！ */
	public final static String missing_main_position = JecnProperties.getValue("missing_main_position");
	/** 存在分页符压线，请查看分页符设置！ */
	public final static String PAGE_SET_COVER_LINE = JecnProperties.getValue("page_set_cover_line");

	public final static String CUSTOM_POST_IS_NOT_EMPTY = JecnProperties.getValue("custom_post_is_not_empty");
}
