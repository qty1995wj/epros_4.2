package epros.draw.gui.top.toolbar.element;

import java.awt.GridBagConstraints;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.line.JecnBaseLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;

/**
 * 
 * 流程元素大小面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementSizePanel extends JecnAbstarctFlowElementPanel {

	/** 流程元素宽：标签 */
	private JLabel eleWidthLabel = null;
	/** 流程元素宽：内容 */
	private JSpinner eleWidthSpinner = null;
	/** 流程元素高：标签 */
	private JLabel eleHeightLabel = null;
	/** 流程元素高：内容 */
	private JSpinner eleHeightSpinner = null;

	public JecnFlowElementSizePanel(JecnFlowElementPanel flowElementPanel) {
		super(flowElementPanel);
		init();
	}

	@Override
	protected void initComponents() {

		// 元素属性宽
		eleWidthLabel = new JLabel(this.flowElementPanel.getResourceManager()
				.getValue("elementWidth"), SwingConstants.RIGHT);
		eleWidthSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 1000, 1));
		// 元素属性高
		eleHeightLabel = new JLabel(this.flowElementPanel.getResourceManager()
				.getValue("elementHeight"), SwingConstants.RIGHT);
		eleHeightSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 1000, 1));

		// 元素属性宽
		eleWidthSpinner.setPreferredSize(flowElementPanel.getSizeColor());
		eleWidthSpinner.setMinimumSize(flowElementPanel.getSizeColor());
		eleWidthSpinner.setMaximumSize(flowElementPanel.getSizeColor());
		eleWidthSpinner.addChangeListener(flowElementPanel);
		// 元素属性高
		eleHeightSpinner.setPreferredSize(flowElementPanel.getSizeColor());
		eleHeightSpinner.setMinimumSize(flowElementPanel.getSizeColor());
		eleHeightSpinner.setMaximumSize(flowElementPanel.getSizeColor());
		eleHeightSpinner.addChangeListener(flowElementPanel);

		// 边框标题
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), flowElementPanel.getResourceManager()
				.getValue("figureSzie")));
	}

	@Override
	protected void initLayout() {
		// 流程元素宽
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsFirst(), 0, 0);
		this.add(eleWidthLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsContent(), 0, 0);
		this.add(eleWidthSpinner, c);

		// 空闲区域
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				flowElementPanel.getInsetsLabel(), 0, 0);
		this.add(new JLabel(), c);

		// 流程元素高
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsLabel(), 0, 0);
		this.add(eleHeightLabel, c);
		c = new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsContent(), 0, 0);
		this.add(eleHeightSpinner, c);
	}

	@Override
	public void initData(JecnAbstractFlowElementData flowElementData) {
		if (flowElementData == null) {
			return;
		}
		// 元素属性宽
		this.getEleWidthSpinner().setValue(flowElementData.getFigureSizeX());
		// 元素属性高
		this.getEleHeightSpinner().setValue(flowElementData.getFigureSizeY());

		JecnBaseFlowElementPanel selectedBasePanel = flowElementPanel
				.getSelectedBasePanel();
		if (selectedBasePanel instanceof JecnBaseFigurePanel) {// 图形
			if (!this.getEleWidthSpinner().isEnabled()) {
				this.getEleWidthSpinner().setEnabled(true);
				this.getEleHeightSpinner().setEnabled(true);
			}
		} else if (selectedBasePanel instanceof JecnBaseLinePanel) {// 如果是线，不让改变大小
			if (this.getEleWidthSpinner().isEnabled()) {
				this.getEleWidthSpinner().setEnabled(false);
				this.getEleHeightSpinner().setEnabled(false);
			}
		}
	}

	/**
	 * 
	 * 编辑宽高动作事件处理方法
	 * 
	 * @param e
	 *            ChangeEvent 内容改变事件
	 */
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() != eleWidthSpinner
				&& e.getSource() != eleHeightSpinner) {
			return;
		}

		stateChangedProcess(e);
	}

	/**
	 * 
	 * 编辑宽高动作事件处理方法
	 * 
	 * @param e
	 *            ChangeEvent 内容改变事件
	 */
	private void stateChangedProcess(ChangeEvent e) {

		JSpinner spinner = (JSpinner) e.getSource();
		int valueInt = Integer.valueOf(spinner.getValue().toString());
		if (spinner == eleWidthSpinner) {// 流程元素宽
			if (valueInt != this.flowElementPanel.getSelectedBasePanel()
					.getFlowElementData().getFigureSizeX()) {
				this.flowElementPanel.getSelectedBasePanel()
						.getFlowElementData().setFigureSizeX(valueInt);
				setSelectedFigureSize();
			}
		} else if (spinner == eleHeightSpinner) {// 流程元素高
			if (valueInt != this.flowElementPanel.getSelectedBasePanel()
					.getFlowElementData().getFigureSizeY()) {
				this.flowElementPanel.getSelectedBasePanel()
						.getFlowElementData().setFigureSizeY(valueInt);
				setSelectedFigureSize();
			}
		}
	}

	/**
	 * 
	 * 设置图形的大小
	 * 
	 */
	private void setSelectedFigureSize() {
		this.flowElementPanel.setSelectedSize(this.flowElementPanel
				.getSelectedBasePanel().getFlowElementData().getFigureSizeX(),
				this.flowElementPanel.getSelectedBasePanel()
						.getFlowElementData().getFigureSizeY());
	}

	public JLabel getEleWidthLabel() {
		return eleWidthLabel;
	}

	public JSpinner getEleWidthSpinner() {
		return eleWidthSpinner;
	}

	public JLabel getEleHeightLabel() {
		return eleHeightLabel;
	}

	public JSpinner getEleHeightSpinner() {
		return eleHeightSpinner;
	}

}
