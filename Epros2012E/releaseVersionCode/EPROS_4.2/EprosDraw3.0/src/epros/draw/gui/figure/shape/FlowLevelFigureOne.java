package epros.draw.gui.figure.shape;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.gui.figure.BaseFlowLevelFigure;

/**
 * 流程等级图形 一级
 * 
 * @author Administrator
 * @date： 日期：Jul 24, 2012 时间：10:29:31 AM
 */
public class FlowLevelFigureOne extends BaseFlowLevelFigure {

	public FlowLevelFigureOne(JecnAbstractFlowElementData flowElementData) {
		super(flowElementData);
		intLevel = 1;
	}

}
