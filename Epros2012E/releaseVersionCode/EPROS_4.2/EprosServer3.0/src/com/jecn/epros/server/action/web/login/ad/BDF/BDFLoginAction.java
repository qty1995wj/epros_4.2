package com.jecn.epros.server.action.web.login.ad.BDF;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.jecn.authentication.AuthenticationFirefoxRequestUtil;
import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.webservice.common.TaskApiUrlHandler;

public class BDFLoginAction extends JecnAbstractADLoginAction {

	public static String USER_NAME = "epros.server.action.web.login.ad.BDF";

	public BDFLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		try {
			if (StringUtils.isNotBlank(this.loginAction.getLoginName())
					&& StringUtils.isNotBlank(this.loginAction.getPassword())) {// 存在用户名和密码
				log.info("普通登录，登录名:" + this.loginAction.getLoginName());
				return loginAction.loginGen();
			}

			// 单点登录传递过来的登录名称

			// String adLoginName = loginAction.getLoginName();

			String adLoginName = null;
			Object loginlotus = loginAction.getSession().getAttribute(USER_NAME);
			if (loginlotus != null) {
				adLoginName = String.valueOf(loginlotus);
			}

			log.info("登录名称(session) = " + adLoginName);

			if (StringUtils.isNotBlank(adLoginName)) {// 单点登录名称为空.
				boolean isChrome = AuthenticationFirefoxRequestUtil.isChromeBrower(loginAction.getRequest());
				boolean isIE = isIEBrower(loginAction.getRequest());
				// 必须是谷歌 而且不能低于 49 版本 IE 不能低于11
				if ((isChrome && chromeVersionSmallThan(loginAction.getRequest(), 49))
						|| (isIE && ieVersionSmallThan(loginAction.getRequest(), 11)) || (!isChrome && !isIE)) {
					// 跳转到火狐，执行单点登录
					AuthenticationFirefoxRequestUtil.fireFoxBrowserRequest(getLoginUrl(loginAction.getRequest(),
							adLoginName, loginAction.getRequest().getParameter(getParamId()), loginAction.getRequest()
									.getParameter(getParamType())), loginAction.getResponse(),
							JecnBDFAfterItem.BROWSER_ADDRESS, JecnBDFAfterItem.BROWSER_ADDRESS_TWO);
					return LoginAction.INPUT;
				}
			}
			// 获取 请求的 用户名 跳转IE的时候会传参
			String userName = this.loginAction.getLoginName();
			log.info("userName(request) = " + userName);

			// 返回跳转标识
			String userLoginGen = "";

			// 判断请求用户名不为空 验证用户
			if (StringUtils.isNotBlank(userName)) {
				loginAction.setLoginName(userName);
				// 不验证密码登录
				userLoginGen = loginAction.userLoginGen(false);
			}
			// 判断域用户名不为空 验证用户
			if (StringUtils.isNotBlank(adLoginName)) {
				userLoginGen = loginByLoginName(adLoginName);
			}

			// 获取跳转url
			String url = getRequestUrl();
			// 返回标识不为空 判断返回标识类型
			if (StringUtils.isNotBlank(userLoginGen)) {
				// 判断登陆成功后 如果存在 url 参数 则执行 跳转
				if (LoginAction.SUCCESS.equals(userLoginGen) || LoginAction.MAIN.equals(userLoginGen)) {
					if (StringUtils.isNotBlank(url)) {
						userLoginGen = this.loginAction.REDIRECT;
						loginAction.setCustomRedirectURL(url);
						log.info(" REDIRECT 跳转地址= " + loginAction.getCustomRedirectURL());
					}
				}
			}

			log.info("跳转标识 = " + userLoginGen);
			return userLoginGen;
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		} finally {
			// if (StringUtils.isNotBlank(loginAction.getLoginName())) {
			// EKPSSOUserData userData = EKPSSOUserData.getInstance();
			// userData.changeCurrentUser(loginAction.getLoginName());
			// }
		}
	}

	private String getLoginUrl(HttpServletRequest request, String loginName, String id, String type) {
		StringBuffer url = request.getRequestURL();
		/*
		 * if (StringUtils.isNotBlank(request.getQueryString())) {
		 * url.append("?").append(request.getQueryString()); }
		 */
		if (StringUtils.isNotBlank(loginName)) {// 普通登录
			url.append("?").append("loginName=").append(loginName).append("&").append(getParamId() + "=").append(id)
					.append("&").append(getParamType() + "=").append(type);
		}
		log.info(JecnBDFAfterItem.BROWSER_ADDRESS + " " + url.toString());
		return url.toString();
	}

	private boolean isIEBrower(HttpServletRequest request) {
		String agent = loginAction.getRequest().getHeader("User-Agent").toLowerCase();
		if (agent.indexOf("rv:") != -1) {
			return true;
		}
		return agent.indexOf("msie") != -1;
	}

	private boolean chromeVersionSmallThan(HttpServletRequest request, int compareVersion) {
		String agent = loginAction.getRequest().getHeader("User-Agent").toLowerCase();
		log.info(agent);
		String reg = "chrome/(\\d+)\\.\\d+";
		Pattern compile = Pattern.compile(reg);
		Matcher matcher = compile.matcher(agent);
		while (matcher.find()) {
			String version = matcher.group(1);
			log.info("谷歌版本:" + version);
			if (Integer.valueOf(version) < compareVersion) {
				return true;
			}
		}
		return false;
	}

	private boolean ieVersionSmallThan(HttpServletRequest request, int compareVersion) {
		String agent = loginAction.getRequest().getHeader("User-Agent").toLowerCase();
		log.info(agent);
		if (agent.indexOf("rv:") != -1) {
			return false;
		}
		String reg = "msie (\\d+)\\.\\d+";
		Pattern compile = Pattern.compile(reg);
		Matcher matcher = compile.matcher(agent);
		while (matcher.find()) {
			String version = matcher.group(1);
			log.info("ie版本:" + version);
			if (Integer.valueOf(version) < compareVersion) {
				return true;
			}
		}
		return false;
	}

}
