/**
 * GetDmFinishWorkItemsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class GetDmFinishWorkItemsResponse  implements java.io.Serializable {
    private java.lang.String getDmFinishWorkItemsResult;

    public GetDmFinishWorkItemsResponse() {
    }

    public GetDmFinishWorkItemsResponse(
           java.lang.String getDmFinishWorkItemsResult) {
           this.getDmFinishWorkItemsResult = getDmFinishWorkItemsResult;
    }


    /**
     * Gets the getDmFinishWorkItemsResult value for this GetDmFinishWorkItemsResponse.
     * 
     * @return getDmFinishWorkItemsResult
     */
    public java.lang.String getGetDmFinishWorkItemsResult() {
        return getDmFinishWorkItemsResult;
    }


    /**
     * Sets the getDmFinishWorkItemsResult value for this GetDmFinishWorkItemsResponse.
     * 
     * @param getDmFinishWorkItemsResult
     */
    public void setGetDmFinishWorkItemsResult(java.lang.String getDmFinishWorkItemsResult) {
        this.getDmFinishWorkItemsResult = getDmFinishWorkItemsResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDmFinishWorkItemsResponse)) return false;
        GetDmFinishWorkItemsResponse other = (GetDmFinishWorkItemsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getDmFinishWorkItemsResult==null && other.getGetDmFinishWorkItemsResult()==null) || 
             (this.getDmFinishWorkItemsResult!=null &&
              this.getDmFinishWorkItemsResult.equals(other.getGetDmFinishWorkItemsResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetDmFinishWorkItemsResult() != null) {
            _hashCode += getGetDmFinishWorkItemsResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDmFinishWorkItemsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">GetDmFinishWorkItemsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getDmFinishWorkItemsResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GetDmFinishWorkItemsResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
