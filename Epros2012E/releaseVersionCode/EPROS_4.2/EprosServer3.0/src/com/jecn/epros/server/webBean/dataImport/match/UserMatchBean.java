package com.jecn.epros.server.webBean.dataImport.match;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;

/**
 * 人员临时表
 * 
 * @author zhangjie 20120208
 */
public class UserMatchBean extends AbstractBaseBean {
	/** 主键ID */
	private Long id;

	/** 登录名称 */
	private String loginName;

	/** 真实名称 */
	private String trueName;

	/** 邮箱地址 */
	private String email;

	/** 联系电话 */
	private String phone;

	/** 内外网邮件标识 0:inner内网，1：outer外网 */
	private int emailType;

	/** 所属实际岗位编码 */
	private String posNum;

	// /**标识位 0：添加，1：更新，2：删除 */
	// private int personFlag;
	//	
	// /**错误信息 */
	// private String error;

	/** 存放人员岗位组合数据,即人员+关联岗位的数据 */
	private UserMatchBean userPosBean = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getEmailType() {
		return emailType;
	}

	public void setEmailType(int emailType) {
		this.emailType = emailType;
	}

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}

	// public int getPersonFlag() {
	// return personFlag;
	// }
	//
	// public void setPersonFlag(int personFlag) {
	// this.personFlag = personFlag;
	// }
	//	
	// public String getError() {
	// return error;
	// }
	//
	// public void setError(String error) {
	// this.error = error;
	// }

	public UserMatchBean getUserPosBean() {
		return userPosBean;
	}

	public void setUserPosBean(UserMatchBean userPosBean) {
		this.userPosBean = userPosBean;
	}

	/**
	 * 
	 * 判断所有数据都为空
	 * 
	 * @return 都为空返回true、其他情况返回false
	 */
	public boolean isAttributesNull() {
		if (JecnCommon.isNullOrEmtryTrim(this.getLoginName())// 人员编号
				&& JecnCommon.isNullOrEmtryTrim(this.getTrueName())// 真实姓名
				&& JecnCommon.isNullOrEmtryTrim(this.getPosNum()) // 任职岗位编号
				// && Tool.isNullOrEmtryTrim(this.get())// 任职岗位名称
				// && Tool.isNullOrEmtryTrim(this.getDeptNum())// 岗位所属部门编号
				&& JecnCommon.isNullOrEmtryTrim(this.getEmail())// 邮箱
				&& JecnCommon.isNullOrEmtryTrim(this.getPhone())// 联系电话
				&& MatchTool.isNullObj(this.getEmailType())// 内外网邮件标识
		) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * 人员编码、岗位编码都相同返回true，否则返回false
	 * 
	 * @param userBean
	 *            UserBean
	 * @return
	 */
	public boolean repeatAttributes(UserMatchBean userBean) {
		// 人员编号（登录名称）、岗位编号
		if (MatchTool.nullToEmtry(this.getLoginName()).equals(
				MatchTool.nullToEmtry(userBean.getLoginName()))
				&& MatchTool.nullToEmtry(this.getPosNum()).equals(
						MatchTool.nullToEmtry(userBean.getPosNum()))) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 
	 * 判断邮箱内外网标识是否相同，new Integer（null）情况不能判断
	 * 
	 * @param userBean
	 * @return
	 */
	public boolean isEailTypeSame(UserMatchBean userBean) {
		if (userBean == null) {
			return false;
		}
		String strEmailType = String.valueOf(this.getEmailType());
		String userEmailType = String.valueOf(userBean.getEmailType());
		if (strEmailType == null && userEmailType == null) {// 都为null,返回true
			return true;
		} else if (strEmailType == null && userEmailType != null) {// 一个为null，一个不为null或，返回false
			return false;
		} else if (strEmailType != null && userEmailType == null) {// 一个为null，一个不为null或，返回false
			return false;
		} else if (strEmailType.equals(userEmailType)) {// 两个都不为null，且相等返回true
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 
	 * 邮箱类型是否是0、1
	 * 
	 * @return
	 */
	public boolean checkEmailType() {
		return (String.valueOf(emailType) != null && (emailType == MatchConstant.EMAIL_TYPE_INNER_INT || emailType == MatchConstant.EMAIL_TYPE_OUTER_INT)) ? true
				: false;
	}

	@Override
	public String toInfo() {
		StringBuilder strBuf = new StringBuilder();
		// 人员编号
		strBuf.append("人员编号:");
		strBuf.append(this.getLoginName());
		strBuf.append(MatchConstant.EMTRY_SPACE);

		// 真实姓名
		strBuf.append("真实姓名:");
		strBuf.append(this.getTrueName());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		// 任职岗位编号
		strBuf.append("任职岗位编号:");
		strBuf.append(this.getPosNum());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		// 邮箱
		strBuf.append("邮箱:");
		strBuf.append(this.getEmail());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		// 联系电话
		strBuf.append("联系电话:");
		strBuf.append(this.getPhone());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		// 内外网邮件标识
		strBuf.append("内外网邮件标识:");
		strBuf.append(this.getEmailType());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		// 错误信息
		strBuf.append("错误信息:");
		strBuf.append(this.getError());
		strBuf.append(MatchConstant.EMTRY_SPACE);
		strBuf.append("\r\n");
		return strBuf.toString();
	}

}
