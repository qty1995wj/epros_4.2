package epros.draw.gui.figure.unit;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 
 * 横竖分割线拖动事件处理类
 * 
 * @author ZHANGXH
 * @date： 日期：Jul 13, 2012 时间：5:45:37 PM
 */
public class JecnDraggedVHLineUnit {
	/** 选中的图形元素集合 */
	private List<JecnBaseFigurePanel> allFigureList = new ArrayList<JecnBaseFigurePanel>();
	/** 未選中的图形元素集合 */
	private List<JecnBaseFigurePanel> otherFigureList = new ArrayList<JecnBaseFigurePanel>();
	/** 选中的连接线元素集合 */
	private Set<JecnBaseManhattanLinePanel> allManLineList = new HashSet<JecnBaseManhattanLinePanel>();
	/** 选中的连接线元素集合 */
	private List<JecnBaseDividingLinePanel> allDividLineList = new ArrayList<JecnBaseDividingLinePanel>();
	/** 拖动包含的横或竖分割线 */
	private List<JecnBaseVHLinePanel> allVHLineList = new ArrayList<JecnBaseVHLinePanel>();
	/** 线段方向 horizontal, 水平vertical, 垂直 */
	private LineDirectionEnum lineDirectionEnum = null;

	// 操作前数据
	private JecnUndoRedoData undoData = new JecnUndoRedoData();
	// 操作后数据
	private JecnUndoRedoData redoData = new JecnUndoRedoData();

	private Point oldPoint = null;

	/** true:添加泳道 */
	private boolean addModeRole;

	public JecnDraggedVHLineUnit(LineDirectionEnum lineDirectionEnum) {
		this.lineDirectionEnum = lineDirectionEnum;
	}

	/**
	 * 鼠标移动所有流程元素处理
	 * 
	 * @param oldLocalXY
	 *            横线或纵向的位置点（拖动的横向分割线记录的就是纵坐标，拖动的是纵向分割线记录的就是横坐标）
	 * @param diffXY
	 *            鼠标移动位移
	 */
	public void moveAllFiguresAndLines(int oldLocalXY, int diffXY) {

		// 获取当前画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		// 移动所有图形处理
		moveAllFigures(desktopPane, diffXY, undoData, redoData);
		// 移动所有分割线处理 （横线、竖线、斜线）
		moveAllDividLines(diffXY, undoData, redoData);
		// 移动的所有横向或竖向分割线
		moveAllVHLines(oldLocalXY, diffXY, undoData, redoData);

		// 拖动图形画图形相关连接线
		JecnDraggedFiguresPaintLine.paintLineByFigures(allFigureList);
		desktopPane.updateUI();

		// *************************撤销恢复*************************//
		if (!addModeRole) {// 非 添加永道 执行的是 横竖割线移动
			// 记录这次操作的数据集合
			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
			desktopPane.getUndoRedoManager().addEdit(unredoProcess);
		}
		// *************************撤销恢复*************************//
	}

	/**
	 * 获取分割线间拖动时未选中的图形的最大值
	 * 
	 * 
	 * @param type
	 *            :1表示竖线，0表示横线
	 * @return 未选中图形的最低点坐标
	 */
	public int getMaxFigureDiff() {
		int maxDiff = 0;
		int diff = 0;
		if (otherFigureList != null && otherFigureList.size() > 0) {
			for (JecnBaseFigurePanel figure : otherFigureList) {
				switch (lineDirectionEnum) {
				case horizontal:// // 横线 获取纵坐标
					// 获取图形的最低点坐标
					diff = figure.getLocation().y + figure.getHeight();
					if (diff < oldPoint.y && diff > maxDiff) {
						maxDiff = diff;
					}
					break;
				case vertical:// // 竖线获取横坐标
					diff = figure.getLocation().x + figure.getWidth();
					if (diff < oldPoint.x && diff > maxDiff) {
						maxDiff = diff;
					}
					break;
				}
			}
		}
		return maxDiff;
	}

	/**
	 * 移动所有图形处理
	 * 
	 * @param desktopPane
	 *            JecnDrawDesktopPane 画图面板
	 * @param diffXY
	 *            int 鼠标横线或纵向移动的位移
	 */
	private void moveAllFigures(JecnDrawDesktopPane desktopPane, int diffXY, JecnUndoRedoData undoData,
			JecnUndoRedoData redoData) {
		for (int i = 0; i < allFigureList.size(); i++) {
			JecnBaseFigurePanel curFigure = allFigureList.get(i);
			// 操作前
			undoData.recodeFlowElement(curFigure);
			// 如果是注释框
			if (curFigure instanceof CommentText) {
				CommentText commentText = (CommentText) curFigure;
				// 小线段位移算法
				commentText.isMove();
			}

			int scrollX = 0;
			int scrollY = 0;
			// 获取当前比例下数据对象
			JecnFigureDataCloneable figureClonebleOld = curFigure.getZoomJecnFigureDataCloneable();
			switch (lineDirectionEnum) {
			case horizontal:// // 横线 获取纵坐标
				// 获取图形的最低点坐标
				if (curFigure.getLocation().y + diffXY > 0) {
					scrollY = diffXY;
				}
				break;
			case vertical:// // 竖线获取横坐标
				// 纵线比较X坐标
				if (curFigure.getLocation().getX() >= 0) {
					if (curFigure.getLocation().x + diffXY > 0) {
						scrollX = diffXY;
					}
				}
				break;
			}

			Point ePoint = new Point();
			// 换算拖动后图形的位置点
			ePoint.setLocation(figureClonebleOld.getLeftUpXY().getX() + scrollX, figureClonebleOld.getLeftUpXY().getY()
					+ scrollY);
			// 移动鼠标，换算数据
			curFigure.getJecnFigureDataCloneable().moveFigure(ePoint, desktopPane.getWorkflowScale());

			// 获取换算后的数据
			JecnFigureDataCloneable figureCloneble = curFigure.getZoomJecnFigureDataCloneable();
			// 设置拖动后图形的位置点
			curFigure.setLocation(figureCloneble.getX(), figureCloneble.getY());

			// 操作后
			redoData.recodeFlowElement(curFigure);
		}
	}

	/**
	 * 移动所有分割线处理 （横线、竖线、斜线）
	 * 
	 * @param diffXY
	 *            int鼠标横线或纵向移动的位移
	 */
	private void moveAllDividLines(int diffXY, JecnUndoRedoData undoData, JecnUndoRedoData redoData) {
		// 如果存在分割线创建分割线临时开始点和结束点
		Point tempPointS = null;
		Point tempPointE = null;
		for (int i = 0; i < allDividLineList.size(); i++) {
			JecnBaseDividingLinePanel dividLine = allDividLineList.get(i);

			// 操作前
			undoData.recodeFlowElement(dividLine);

			int startX = dividLine.getStartPoint().x;
			int startY = dividLine.getStartPoint().y;
			int endX = dividLine.getEndPoint().x;
			int endY = dividLine.getEndPoint().y;

			switch (lineDirectionEnum) {
			case horizontal:// // 横线 获取纵坐标
				// 获取图形的最低点坐标
				// if (startY >= oldXY && endY >= oldXY) {
				if (startY >= 0 && endY >= 0) {
					tempPointS = new Point(startX, startY + diffXY);
					tempPointE = new Point(endX, endY + diffXY);
				}
				break;
			case vertical:// // 竖线获取横坐标
				// 纵线比较X坐标
				if (startX >= 0 && endX >= 0) {
					tempPointS = new Point(startX + diffXY, startY);
					tempPointE = new Point(endX + diffXY, endY);
				}
				break;
			}
			if (tempPointS == null || tempPointE == null) {
				continue;// 跳出当前循环
			}
			dividLine.originalCloneData(tempPointS, tempPointE);

			// 操作后
			redoData.recodeFlowElement(dividLine);
		}
	}

	/**
	 * 移动的所有横向或竖向分割线
	 * 
	 * @param oldLocalXY
	 *            横线或纵向的位置点（拖动的横向分割线记录的就是纵坐标，拖动的是纵向分割线记录的就是横坐标）
	 * @param diffXY
	 *            鼠标移动位移
	 */
	private void moveAllVHLines(int oldLocalXY, int diffXY, JecnUndoRedoData undoData, JecnUndoRedoData redoData) {
		for (int i = 0; i < allVHLineList.size(); i++) {
			// 横竖分割线
			JecnBaseVHLinePanel vhLianePanel = allVHLineList.get(i);

			// 操作前
			undoData.recodeFlowElement(vhLianePanel);

			switch (lineDirectionEnum) {
			case horizontal:// // 横线 获取纵坐标
				if (vhLianePanel instanceof VerticalLine) {
					return;
				}
				// 获取图形的最低点坐标
				// 纵线比较X坐标
				ParallelLines curHLine = (ParallelLines) vhLianePanel;
				if (allVHLineList.size() == 1) {
					// 设置X、Y坐标
					curHLine.getFlowElementData().setVHXY(new Point(curHLine.getX(), curHLine.getY() + diffXY));
					// 设置位置点和大小
					curHLine.setVHLineBounds();
				} else if (curHLine.getY() >= oldLocalXY) {
					// 设置X、Y坐标
					curHLine.getFlowElementData().setVHXY(new Point(curHLine.getX(), curHLine.getY() + diffXY));
					// 设置位置点和大小
					curHLine.setVHLineBounds();
				}
				break;
			case vertical:// // 竖线获取横坐标
				// 纵线比较X坐标
				if (vhLianePanel instanceof ParallelLines) {
					return;
				}
				VerticalLine curVLine = (VerticalLine) vhLianePanel;
				if (allVHLineList.size() == 1) {
					// 设置X、Y坐标
					curVLine.getFlowElementData().setVHXY(new Point(curVLine.getX() + diffXY, curVLine.getY()));
					// 设置位置点和大小
					curVLine.setVHLineBounds();
				} else if (curVLine.getX() >= oldLocalXY) {
					// 设置X、Y坐标
					curVLine.getFlowElementData().setVHXY(new Point(curVLine.getX() + diffXY, curVLine.getY()));
					// 设置位置点和大小
					curVLine.setVHLineBounds();
				}
				break;
			}

			// 操作后
			redoData.recodeFlowElement(vhLianePanel);
		}
	}

	/**
	 * 清空分割线记录的临时数据集合
	 * 
	 */
	public void cleanTempList() {
		allFigureList.clear();
		allDividLineList.clear();
		otherFigureList.clear();
		allVHLineList.clear();
		allManLineList.clear();
	}

	public List<JecnBaseFigurePanel> getAllFigureList() {
		return allFigureList;
	}

	public List<JecnBaseFigurePanel> getOtherFigureList() {
		return otherFigureList;
	}

	public Set<JecnBaseManhattanLinePanel> getAllManLineList() {
		return allManLineList;
	}

	public List<JecnBaseDividingLinePanel> getAllDividLineList() {
		return allDividLineList;
	}

	public List<JecnBaseVHLinePanel> getAllVHLineList() {
		return allVHLineList;
	}

	public Point getOldPoint() {
		return oldPoint;
	}

	public void setOldPoint(Point oldPoint) {
		this.oldPoint = oldPoint;
	}

	public JecnUndoRedoData getUndoData() {
		return undoData;
	}

	public JecnUndoRedoData getRedoData() {
		return redoData;
	}

	public boolean isAddModeRole() {
		return addModeRole;
	}

	public void setAddModeRole(boolean addModeRole) {
		this.addModeRole = addModeRole;
	}
}
