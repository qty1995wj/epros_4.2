package com.jecn.epros.server.dao.dataImport.match.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.impl.SessionImpl;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.JecnUserInfo;
import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.dataImport.match.IMatchUserImportDao;
import com.jecn.epros.server.service.dataImport.match.constant.MatchConstant;
import com.jecn.epros.server.service.dataImport.match.constant.MatchSqlConstant;
import com.jecn.epros.server.webBean.dataImport.match.ActualDeptBean;
import com.jecn.epros.server.webBean.dataImport.match.ActualPosBean;
import com.jecn.epros.server.webBean.dataImport.match.ActualUserBean;
import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.PosEprosRelBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;

/*******************************************************************************
 * 岗位匹配DAO层
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Apr 2, 2013 时间：7:53:44 PM
 */
public class MatchUserImportDaoImpl extends AbsBaseDao<DeptMatchBean, Long>
		implements IMatchUserImportDao {
	private final Log log = LogFactory.getLog(MatchUserImportDaoImpl.class);

	/**
	 * 
	 * 清除临时库、入库临时库
	 * 
	 * @param baseBean
	 * @throws Exception
	 */
	@Override
	public void deleteAndInsertTmpJecnIOTable(BaseBean baseBean)
			throws Exception {
		if (baseBean == null || baseBean.isNull()) {
			return;
		}

		log.info("删除临时表数据并且添加数据开始 ");

		// 删除部门
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_SANFU_IO_DEPT_SQL);
		query.executeUpdate();

		// 删除岗位
		query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_SANFU_IO_POS_SQL);
		query.executeUpdate();

		// 删除人员
		query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_SHAFU_IO_USER_SQL);
		query.executeUpdate();

		// 插入部门
		for (DeptMatchBean deptBean : baseBean.getDeptBeanList()) {
			this.getSession().save(deptBean);
		}
		// 插入岗位
		for (PosMatchBean posBean : baseBean.getPosBeanList()) {
			this.getSession().save(posBean);
		}
		// 插入人员
		for (UserMatchBean userBean : baseBean.getUserPosBeanList()) {
			this.getSession().save(userBean);
		}
		log.info("删除临时表数据并且添加数据结束 ");
	}

	/**
	 * 
	 * 通用部门临时表(JECN_MATCH_DEPT_BEAN)：同一部门下部门名称不能重复
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<DeptMatchBean> checkDeptNameSame() throws Exception {
		log.info("同一部门下部门名称不能重复：通用部门临时表(JECN_MATCH_DEPT_BEAN)数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_DEPT_NAME_SAME_PDEPT).addEntity(
				DeptMatchBean.class);
		List<DeptMatchBean> ret = query.list();
		log.info("同一部门下部门名称不能重复：通用部门临时表(JECN_MATCH_DEPT_BEAN)数据结束 ");
		return ret;
	}

	/**
	 * 
	 * 待导入部门、岗位以及人员中在各自真实库中存在的数据， 更新其操作标识设置为1
	 * 
	 * @throws Exception
	 */
	@Override
	public void updateProFlag(Long curProjectId) throws Exception {
		log.info("6.与真实库数据做验证数据开始 ");
		String deptSql = "";
		String posSql = "";
		String userSql = MatchSqlConstant.UPDATE_PRO_FLAG_USER_ORACLE;
		if (DBType.ORACLE.equals(getDBDType())) {
			deptSql = MatchSqlConstant.UPDATE_PRO_FLAG_DEPT_ORACLE;
			posSql = MatchSqlConstant.UPDATE_PRO_FLAG_POS_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			deptSql = MatchSqlConstant.UPDATE_PRO_FLAG_DEPT_SQLSERVER;
			posSql = MatchSqlConstant.UPDATE_PRO_FLAG_POS_SQLSERVER;
		} else {
			throw new SQLException("连接数据库不是oracle或者SQL server");
		}
		// --------------- 执行更新
		// 部门
		Query query = this.getSession().createSQLQuery(deptSql);
		query.setLong("PROJECTID", curProjectId);
		query.executeUpdate();
		log.info("6.部门 更新其操作标识设置为1");
		// 岗位
		query = this.getSession().createSQLQuery(posSql);
		query.setLong("PROJECTID", curProjectId);
		query.executeUpdate();
		log.info("6.岗位 更新其操作标识设置为1");
		// 人员
		query = this.getSession().createSQLQuery(userSql);
		query.executeUpdate();
		log.info("6.人员 更新其操作标识设置为1");
		// --------------- 执行更新
		log.info("6.与真实库数据做验证数据结束 ");
	}

	/**
	 * 
	 * 更新流程人员表：在人员实际表中不存在的数据 给予锁定状态,锁定 ISLOCK 0:解锁 1：锁定
	 * 
	 * @throws Exception
	 */
	@Override
	public void udateJecnUserIsLock() throws Exception {
		log.info("4.更新流程人员表：在人员实际表中不存在的数据 给予锁定状态,锁定 ISLOCK 0:解锁 1：锁定 ");
		Query query1 = this.getSession().createSQLQuery(
				MatchSqlConstant.UPDATE_JECN_USER_COUNT);
		int count = Integer.parseInt(query1.uniqueResult().toString());
		// --------------- 执行更新
		if (count > 0) {
			// 人员
			Query query = this.getSession().createSQLQuery(
					MatchSqlConstant.UPDATE_JECN_USER_ISLOCK);
			query.executeUpdate();
			log.info("4.更新流程人员表：在人员实际表中不存在的数据 给予锁定状态,锁定 ISLOCK 0:解锁 1：锁定 结束 ");
		}
	}

	/**
	 * 
	 * 查询实际部门表(JECN_MATCH_ACTUAL_DEPT)中的数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ActualDeptBean> selectActDept() throws Exception {
		log.info("查询实际部门表中的数据：实际部门表(JECN_MATCH_ACTUAL_DEPT)数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_ACT_DEPT).addEntity(
				ActualDeptBean.class);
		List<ActualDeptBean> ret = query.list();
		log.info("查询实际部门表中的数据：实际部门表(JECN_MATCH_ACTUAL_DEPT)数据结束 ");
		return ret;
	}

	/**
	 * 
	 * 查询流程岗位与人员关联表数据开始 Table JECN_USER_POSITION_RELATED
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnUserInfo> selectEpsUserPosRel() throws Exception {
		log.info("查询流程岗位与人员关联表数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_ACT_DEPT).addEntity(JecnUserInfo.class);
		List<JecnUserInfo> ret = query.list();
		log.info("查询实际部门表中的数据：实际部门表(JECN_MATCH_ACTUAL_DEPT)数据结束 ");
		return ret;
	}

	/**
	 * 
	 * 查询实际岗位表(JECN_MATCH_ACTUAL_POS)中的数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ActualPosBean> selectActPos() throws Exception {
		log.info("查询实际部门表中的数据：实际部门表(JECN_MATCH_ACTUAL_POS)数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_ACT_POS).addEntity(ActualPosBean.class);
		List<ActualPosBean> ret = query.list();

		log.info("查询实际部门表中的数据：实际部门表(JECN_MATCH_ACTUAL_POS)数据结束 ");
		return ret;
	}

	/**
	 * 
	 * 查询实际人员表(JECN_MATCH_ACTUAL_PERSON)中的数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ActualUserBean> selectActUser() throws Exception {
		log.info("查询实际部门表中的数据：实际部门表(JECN_MATCH_ACTUAL_PERSON)数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_ACT_USER).addEntity(
				ActualUserBean.class);
		List<ActualUserBean> ret = query.list();

		log.info("查询实际部门表中的数据：实际部门表(JECN_MATCH_ACTUAL_PERSON)数据结束 ");
		return ret;
	}

	/**
	 * 
	 * 获取通用部门临时表(TMP_JECN_IO_DEPT)项目ID集合
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> selectProjectId() throws Exception {
		log.info("查询上次导入项目ID：通用部门临时表(JECN_MATCH_DEPT_BEAN)数据开始 ");

		// 执行查询
		Query query = this
				.getSession()
				.createSQLQuery(
						"SELECT PROJECT_ID FROM JECN_MATCH_DEPT_BEAN T GROUP BY PROJECT_ID")
				.addScalar("PROJECT_ID", Hibernate.LONG);
		List<Long> ret = query.list();

		log.info("查询上次导入项目ID：通用部门临时表(TMP_JECN_IO_DEPT)数据结束 ");
		return ret;
	}

	/**
	 * 
	 * 根据部门临时表中标识获取插入的部门
	 * 
	 * @param this.getSession()
	 * @throws Exception
	 */
	@Override
	public void insertDept(Long curProjectId) throws Exception {
		// 插入部门临时表中标识是插入的部门
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = MatchSqlConstant.INSERT_SANFU_ACT_DEPT_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = MatchSqlConstant.INSERT_SANFU_ACT_DEPT_SQLSERVER;
		} else {
			throw new SQLException("连接数据库不是oracle或者SQL server");
		}

		log.info("插入部门临时表(JECN_MATCH_DEPT_BEAN)数据开始 ");

		Query query = this.getSession().createSQLQuery(sql);
		query.setLong("PROJECTID", curProjectId);
		query.executeUpdate();

		log.info("插入部门临时表(JECN_MATCH_DEPT_BEAN)数据结束 ");

	}

	/**
	 * 
	 * 插入部门临时表中标识是插入的岗位
	 * 
	 * @param this.getSession()
	 *            Session
	 * @throws Exception
	 */
	@Override
	public void insertPos(Long curProjectId) throws Exception {
		// 插入部门临时表中标识是插入的岗位
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = MatchSqlConstant.INSERT_SANFU_ACT_POS_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = MatchSqlConstant.INSERT_SANFU_ACT_POS_SQLSERVER;
		} else {
			throw new SQLException("连接数据库不是oracle或者SQL server");
		}

		log.info("插入岗位临时表(JECN_MATCH_POS_BEAN)数据开始 ");

		Query query = this.getSession().createSQLQuery(sql);
		query.executeUpdate();

		log.info("插入岗位临时表(JECN_MATCH_POS_BEAN)数据结束 ");
	}

	/**
	 * 
	 * 插入通用人员临时表中标识是插入的人员
	 * 
	 * @param this.getSession()
	 *            Session
	 * @throws Exception
	 */
	@Override
	public void insertUser() throws Exception {

		// 插入人员临时表中标识是插入的人员
		String sql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			sql = MatchSqlConstant.INSERT_SANFU_ACT_USER_ORACLE;
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			sql = MatchSqlConstant.INSERT_SANFU_ACT_USER_SQLSERVER;
		} else {
			throw new SQLException("连接数据库不是oracle或者SQL server");
		}

		log.info("插入人员临时表(JECN_MATCH_USER_BEAN)数据开始 ");

		Query query = getSession().createSQLQuery(sql);
		query.executeUpdate();

		log.info("插入人员临时表(JECN_MATCH_USER_BEAN)数据结束 ");

	}

	/**
	 * 
	 * 查询流程岗位与实际岗位关联表（JECN_MATCH_POS_EPROS_REL）中的数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<PosEprosRelBean> selectPosUserRel() throws Exception {
		log.info("查询人员岗位关联表（JECN_MATCH_POS_EPROS_REL）中的数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_POS_USER_REL).addEntity(
				PosEprosRelBean.class);
		// 获取当前主项目Id
		Long projectId=getCurJecnProject();
		query.setLong(0, projectId);
		List<PosEprosRelBean> ret = query.list();
		log.info("查询人员岗位关联表（JECN_MATCH_POS_EPROS_REL）中的数据结束 ");
		return ret;
	}

	/**
	 * 将人员临时表中标识为 1：更新 的数据插入到人员实际表中 将更新后的人员实际表信息 插入数据库
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public void updateActUserUpList(List<ActualUserBean> listUserInfo)
			throws Exception {
		log.info("更新人员数据（JECN_MATCH_ACTUAL_PERSON）中的数据开始 ");

		// 执行更新
		if (listUserInfo != null && listUserInfo.size() > 0) {
			for (ActualUserBean actualUserBean : listUserInfo) {
				this.getSession().update(actualUserBean);
			}
			log.info("更新人员数据（JECN_MATCH_ACTUAL_PERSON）中的数据结束 ");
		}
	}

	/**
	 * 将部门临时表中标识为 1：更新 的数据插入到人员实际表中 将更新后的部门实际表信息 插入数据库
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public void updateActDeptUpList(List<ActualDeptBean> listUserInfo)
			throws Exception {
		log.info("更新部门数据（JECN_MATCH_ACTUAL_DEPT）中的数据开始 ");

		// 执行插入
		for (ActualDeptBean actualDeptBean : listUserInfo) {
			this.getSession().update(actualDeptBean);
		}
		log.info("更新部门数据（JECN_MATCH_ACTUAL_DEPT）中的数据结束 ");
	}

	/**
	 * 将岗位临时表中标识为 1：更新 的数据插入到人员实际表中 将更新后的岗位实际表信息 插入数据库
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public void updateActPosUpList(List<ActualPosBean> listUserInfo)
			throws Exception {
		log.info("更新岗位数据（JECN_MATCH_ACTUAL_POS）中的数据开始 ");

		// 执行插入
		for (ActualPosBean actualPosBean : listUserInfo) {
			this.getSession().update(actualPosBean);
		}
		log.info("更新岗位数据（JECN_MATCH_ACTUAL_POS）中的数据结束 ");
	}

	/**
	 * 删除流程岗位与人员关联表 更具 获取流程中岗位ID（根据操作表要删除的岗位编号，和匹配关系表找出需要删除的流程岗位ID）
	 * 
	 * @return
	 * @throws Exception
	 */
	public void deleteEpsUserPos(List<JecnUserInfo> listUserInfo)
			throws Exception {
		log.info("删除流程岗位与人员关联表数据（JECN_USER_POSITION_RELATED）中的数据开始 ");

		// 执行插入
		for (JecnUserInfo jecnUserInfo : listUserInfo) {
			this.getSession().delete(jecnUserInfo);
		}
		log.info("删除流程岗位与人员关联表数据（JECN_USER_POSITION_RELATED）中的数据结束 ");
	}

	/**
	 * 流程人员岗位匹配表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public void insertJecnUserPos(List<JecnUserInfo> listUserInfo)
			throws Exception {
		log.info("更新流程人员岗位匹配数据（JECN_USER_POSITION_RELATED）中的数据开始 ");
		// 执行插入
		for (JecnUserInfo jecnUserInfo : listUserInfo) {
			this.getSession().save(jecnUserInfo);
		}
	}

	/**
	 * 
	 * 添加流程人员信息集合
	 * 
	 * @return
	 * @throws Exception
	 */
	public void insertJecnUserInfos() throws Exception {
		log.info("添加人员数据（JECN_USER）中的数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.INSERT_JECN_USER_INFO);
		// 邮箱内网标识
		query.setInteger("EMAIL_INNER", MatchConstant.EMAIL_TYPE_INNER_INT);
		// 邮箱外网标识
		query.setInteger("EMAIL_OUTER", MatchConstant.EMAIL_TYPE_OUTER_INT);
		query.executeUpdate();
		log.info("添加人员数据（JECN_USER）中的数据结束 ");
	}

	/**
	 * 
	 * 实际岗位表信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<ActualPosBean> selectActPosList() throws Exception {
		log.info("查询实际岗位表（JECN_MATCH_ACTUAL_POS）中的数据开始 ");
		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_ACT_POS_LIST).addEntity(
				ActualPosBean.class);
		List<ActualPosBean> ret = query.list();

		log.info("查询实际岗位表（JECN_MATCH_ACTUAL_POS）中的数据结束 ");
		return ret;
	}

	/**
	 * 获取要 添加 的人员临时表数据
	 * 
	 * @param flag
	 *            0:添加，1：更新
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<UserMatchBean> selectUserBeanList(int flag) throws Exception {
		log.info("查询人员表临时表（JECN_MATCH_USER_BEAN）中的数据开始 ");
		String sql = null;
		// 添加
		if (flag == 0) {// 获取临时表添加的人员，去除一人多岗登录名称相同的记录，只保留一条,并且取出记录为流程中不存在的记录
			sql = MatchSqlConstant.SELECT_NO_DIFF_LOGINNAME;
		} else {// 临时表更新信息
			sql = MatchSqlConstant.SELECT_USERBEAN_LIST_FLAG;
		}
		// 执行查询
		Query query = this.getSession().createSQLQuery(sql).addEntity(
				UserMatchBean.class);
		if (flag == 1) {
			query.setInteger("flag", flag);
		}
		List<UserMatchBean> ret = query.list();

		log.info("查询人员表临时表（JECN_MATCH_USER_BEAN）中的数据结束 ");
		return ret;

	}

	/**
	 * 获取要 添加 的人员临时表数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<DeptMatchBean> selectDeptBeanList(int flag) throws Exception {
		log.info("查询部门临时表中的数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_DEPTBEAN_LIST_FLAG).addEntity(
				DeptMatchBean.class);
		query.setInteger("flag", flag);
		List<DeptMatchBean> ret = query.list();
		log.info("查询部门临时表（JECN_MATCH_USER_BEAN）中的数据结束 ");
		return ret;
	}

	/**
	 * 获取要 添加 的人员临时表数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<PosMatchBean> selectPosBeanList(int flag) throws Exception {
		log.info("查询岗位临时表中的数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_POSBEAN_LIST_FLAG).addEntity(
				PosMatchBean.class);
		query.setInteger("flag", flag);
		List<PosMatchBean> ret = query.list();
		log.info("查询岗位临时表（JECN_MATCH_POS_BEAN）中的数据结束 ");
		return ret;
	}

	/**
	 * 获取流程人员信息 集合
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnUser> getJecnUserInfoList() throws Exception {
		log.info("查询人员表（JECN_USER）中的数据开始 ");
		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_JECN_USER_LISTINF).addEntity(
				JecnUser.class);
		// query.setInteger("LOGINAME", flag);
		List<JecnUser> ret = query.list();

		log.info("查询人员表（JECN_USER）中的数据结束 ");
		return ret;
	}

	/**
	 * 根据 实际岗位表数据在岗位临时表中不存在的岗位编码 查询流程岗位与实际岗位关联表数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getFlowPosIdList() throws Exception {
		log.info("查询流程岗位与实际岗位关联表（JECN_MATCH_POS_EPROS_REL）中的数据开始 ");

		// 执行查询
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_POS_EPS_REL_LIST);
		// query.setInteger("LOGINAME", flag);
		List<Long> ret = query.list();

		log.info("查询流程岗位与实际岗位关联表（JECN_MATCH_POS_EPROS_REL）中的数据结束 ");
		return ret;
	}

	/**
	 * 
	 * 删除部门实际表：在部门临时表中不存在的数据信息
	 * 
	 */
	@Override
	public void deleteActDeptByDeptNum() {
		// --------------- 执行删除、更新
		log.info("删除部门实际表(JECN_MATCH_ACTUAL_DEPT)数据开始 ");
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_ACT_DEPT_DEPTNUM);
		// query.setLong("PROJECTID", curProjectId);
		query.executeUpdate();
		log.info("删除部门实际表(JECN_MATCH_ACTUAL_DEPT）中的数据结束 ");
		// --------------- 执行删除、更新
	}

	/**
	 * 
	 * 删除部门实际表：在部门临时表中不存在的数据信息
	 * 
	 */
	public void deleteEpsUserPosByIds(List<Long> epsId) {
		// --------------- 执行删除、更新
		log.info("删除部门实际表(JECN_MATCH_ACTUAL_DEPT)数据开始 ");
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_ACT_DEPT_DEPTNUM);
		// query.setLong("PROJECTID", curProjectId);
		query.executeUpdate();
		log.info("删除部门实际表(JECN_MATCH_ACTUAL_DEPT）中的数据结束 ");
		// --------------- 执行删除、更新
	}

	/**
	 * 
	 * 删除岗位实际表：在岗位临时表中不存在的数据信息
	 * 
	 */
	@Override
	public void deleteActPosByActNum() {
		// --------------- 执行删除、更新
		log.info("删除岗位实际表(JECN_MATCH_ACTUAL_POS)数据开始 ");
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_ACT_POS_ACTNUM);
		// query.setLong("PROJECTID", curProjectId);
		query.executeUpdate();
		log.info("删除岗位实际表(JECN_MATCH_ACTUAL_POS）中的数据结束 ");
		// --------------- 执行删除、更新
	}

	/**
	 * 
	 * 删除人员实际表：在人员临时表中不存在的数据信息
	 * 
	 */
	@Override
	public void deleteActUserByLogiName() {
		// --------------- 执行删除、更新
		log.info("删除人员实际表(JECN_MATCH_ACTUAL_PERSON)数据开始 ");
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_ACT_USER_LOGINAME);
		// query.setLong("PROJECTID", curProjectId);
		query.executeUpdate();
		log.info("删除人员实际表(JECN_MATCH_ACTUAL_PERSON）中的数据结束 ");
		// --------------- 执行删除、更新
	}

	/***************************************************************************
	 * 删除流程岗位与实际岗位关联表
	 * 
	 * @param this.getSession()
	 */
	@Override
	public void deleteEpsActPos() throws Exception {
		// --------------- 执行删除、更新
		log.info(" 删除流程岗位与实际岗位关联表(JECN_MATCH_POS_EPROS_REL)数据开始 ");
		StringBuffer buffer = new StringBuffer();
		if (DBType.ORACLE.equals(getDBDType())) {
			buffer.append("delete FROM JECN_USER_POSITION_RELATED RU ");
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			buffer.append("delete RU FROM JECN_USER_POSITION_RELATED RU ");
		} else {
			throw new SQLException("连接数据库不是oracle或者SQL server");
		}
		buffer.append(" WHERE RU.FIGURE_ID NOT IN");
		buffer.append(" (SELECT FR.EPS_POSITION_ID");
		buffer.append(" FROM JECN_MATCH_POS_EPROS_REL FR,");
		buffer.append(" JECN_MATCH_ACTUAL_PERSON FU,");
		buffer.append(" JECN_USER                U");
		buffer.append(" WHERE U.LOGIN_NAME = FU.LOGIN_NAME");
		buffer.append(" AND U.PEOPLE_ID = RU.PEOPLE_ID");
		buffer.append(" AND FU.POSITION_NUM IN ");
		buffer.append(" ((SELECT distinct FP.ACT_POS_NUM");
		buffer.append(" FROM JECN_MATCH_ACTUAL_POS FP");
		buffer
				.append(" WHERE (FP.ACT_POS_NUM = FR.POSITION_NUM AND FR.REL_TYPE = 0)");
		buffer.append(" union");
		buffer.append(" SELECT distinct FP.ACT_POS_NUM");
		buffer.append(" FROM JECN_MATCH_ACTUAL_POS FP");
		buffer.append(" WHERE (FP.Base_Pos_Num = FR.POSITION_NUM AND");
		buffer.append(" FR.REL_TYPE = 1))))");
		Query query1 = this.getSession().createSQLQuery(buffer.toString());
		// query.setLong("PROJECTID", curProjectId);
		query1.executeUpdate();
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_JECN_POSITION_EPROS_REL);
		// query.setLong("PROJECTID", curProjectId);
		query.executeUpdate();
		log.info(" 删除流程岗位与实际岗位关联表(JECN_MATCH_POS_EPROS_REL）中的数据结束 ");
		// --------------- 执行删除、更新
	}

	/**
	 * 根据人员临时表获取待更新的人员信息
	 * 
	 * @param this.getSession()
	 * @return
	 */
	@Override
	public List<JecnUser> findJecnUser() throws Exception {
		log.info(" 根据人员临时表获取待更新的人员信息 start ");
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.GET_UPDATE_JECN_USER_LIST).addEntity(
				JecnUser.class);
		List<JecnUser> userList = query.list();
		log.info(" 根据人员临时表获取待更新的人员信息数据结束 ");
		return userList;
	}

	/**
	 * 获取岗位变更的信息
	 * 
	 * @param this.getSession()
	 * @return List<Object[]>userList :0:岗位编号，1：登录名称
	 */
	@Override
	public List<Object[]> findJecnUserObject() {
		log.info(" 根据人员临时表获取待更新的人员信息 start ");
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SF_PERSON_POS_CHANGE_LIST);
		List<Object[]> userList = query.list();
		log.info(" 根据人员临时表获取待更新的人员信息数据结束 ");
		return userList;
	}

	// 数据显示、添加、编辑-===================================================================start
	/***************************************************************************
	 * 添加 流程岗位与实际岗位关联表数据 JECN_MATCH_POS_EPROS_REL
	 * 
	 * @param posEprosRelBean
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean addNewPosEpsData(List<PosEprosRelBean> posEprosRelList,
			List<JecnUserInfo> addJecnUserInfoList) throws Exception {
		boolean isok = false;
		log.info("添JECN_POSITION_EPROS_REL加中的数据开始 ");

		// 执行插入
		for (PosEprosRelBean posEprosRelBean : posEprosRelList) {
			this.getSession().save(posEprosRelBean);
		}
		if (addJecnUserInfoList != null) {
			for (JecnUserInfo jecnUserInfo : addJecnUserInfoList) {
				this.getSession().save(jecnUserInfo);
			}
		}
		isok = true;
		log.info("添JECN_POSITION_EPROS_REL加中的数据结束 ");
		return isok;
	}

	/**
	 * 获取已存在的岗位Id集合
	 * 
	 * @param peopleIds
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> isExitsUserPos() throws Exception {
		String sql = "SELECT distinct PEOPLE_ID,FIGURE_ID from JECN_USER_POSITION_RELATED";
		Query query = this.getSession().createSQLQuery(sql);
		return query.list();
	}

	/***************************************************************************
	 * 获取岗位匹配总数
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getPosEpsCount() throws Exception {
		// 获取Session以便左数据库操作
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.GET_POS_EPS_REL_COUNT);
		return Integer.parseInt(query.uniqueResult().toString());
	}

	/**
	 * 获取操作表中基准岗位记录
	 * 
	 * @param this.getSession()
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getBasePosSfBeanList() throws Exception {
		log.info(" 根据操作表获取基准岗位 开始");
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_FLOW_BASE_POS);
		List<Object[]> objectList = query.list();

		log.info(" 根据操作表获取基准岗位数据结束 ");

		log.info("查询流程岗位与实际岗位关联表（JECN_MATCH_POS_EPROS_REL）中的数据结束 ");
		return objectList;
	}

	/**
	 * 
	 * 获得页的条数
	 * 
	 * @param pageSize
	 * @param startRow
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getJecnTempPositionBean(int pageSize, int start)
			throws Exception {
		return listHql(
				"select distinct epsPosId from PosEprosRelBean order by epsPosId",
				start, pageSize);
	}

	/**
	 * 根据岗位ID获取
	 * 
	 * @param posIds
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<PosEprosRelBean> getPosEpsListInf(List<Long> posIds)
			throws Exception {
		String hql = "from PosEprosRelBean where epsPosId in"
				+ JecnCommonSql.getIds(posIds);
		return this.listHql(hql);
	}

	/**
	 * 获取基准岗位对应岗位下的人员登录名称
	 * 
	 * @param baseNum
	 * @param type
	 *            1:基准岗位，0 岗位
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object> getLoginNameByBaseNum(List<String> posNumList, int type)
			throws Exception {
		String sql = null;
		if (type == 1) {
			sql = "select u.people_id"
					+ "     from jecn_user u"
					+ "    where u.login_name in"
					+ "          (select tp.login_name"
					+ "             from jecn_match_actual_person tp"
					+ "            where exists (select 1"
					+ "                     from jecn_match_actual_pos pm"
					+ "                    where pm.base_pos_num in "
					+ JecnCommonSql.getStrs(posNumList)
					+ "                      and pm.act_pos_num = tp.position_num)"
					+ "            group by tp.login_name)";

		} else {
			sql = "select u.people_id"
					+ "     from jecn_user u"
					+ "    where u.login_name in"
					+ "          (select tp.login_name"
					+ "             from jecn_match_actual_person tp"
					+ "            where exists (select 1"
					+ "                     from jecn_match_actual_pos pm"
					+ "                    where pm.act_pos_num in "
					+ JecnCommonSql.getStrs(posNumList)
					+ "                      and pm.act_pos_num = tp.position_num)"
					+ "            group by tp.login_name)";

		}
		// return this.listHql(hql);
		return this.listNativeSql(sql);
	}

	/**
	 * 根据登录名称获取登录人的主键ID
	 * 
	 * @param listNames
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getPeopleIds(List<String> listNames) throws Exception {
		String hql = null;
		if (listNames != null && listNames.size() > 0) {
			hql = "select peopleId from JecnUser where " + " loginName in "
					+ JecnCommonSql.getStrs(listNames);
		}
		return listHql(hql);
	}

	/***************************************************************************
	 * 根据人员ID 查询 关联表主键ID
	 * 
	 * @param strLoginNames
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRelID(List<Long> peoplesId) throws Exception {
		String sql = null;
		if (peoplesId != null && peoplesId.size() > 0) {
			sql = "SELECT USER_ID,PEOPLE_ID FROM JECN_USER_POSITION_RELATED WHERE "
					+ "PEOPLE_ID IN " + JecnCommonSql.getIds(peoplesId);
		}
		Query query = this.getSession().createSQLQuery(sql);
		return query.list();
	}

	/**
	 * 根据匹配岗位的编码、ID查找对应的岗位名称
	 * 
	 * @param this.getSession()
	 * @return List<Object[]> objecPosNametList
	 */
	@Override
	public List<Object[]> selectActEpsNames(Long curProjectId) throws Exception {
		log.info(" 根据匹配岗位的编码、ID查找对应的岗位名称 开始 ");
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.SELECT_POS_EPS_NAMES_LIST);
		query.setLong("PROJECTID", curProjectId);
		List<Object[]> objecPosNametList = query.list();
		log.info("根据匹配岗位的编码、ID查找对应的岗位名称结束 ");
		return objecPosNametList;
	}

	/***************************************************************************
	 * 删除 流程岗位与实际岗位关联表数据 JECN_MATCH_POS_EPROS_REL 删除 流程人员与岗位管理表
	 * JECN_USER_POSITION_RELATED
	 * 
	 * @param posEprosRelBean
	 * @return
	 * @throws Exception
	 */
	@Override
	public void deletePosEpsData(Long FlowPosID) throws Exception {
		log.info("删除JECN_POSITION_EPROS_REL数据开始 ");

		// 删除 流程岗位与实际岗位关联表数据 JECN_MATCH_POS_EPROS_REL
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_REL_ID);
		query.setLong("FlowPosID", FlowPosID);
		query.executeUpdate();
		// 删除流程人员与岗位管理表 JECN_USER_POSITION_RELATED
		query = this.getSession().createSQLQuery(
				MatchSqlConstant.DELETE_FLOWREL_ID);
		query.setLong("FlowPosID", FlowPosID);
		query.executeUpdate();
		log.info("删除JECN_POSITION_EPROS_REL加中的数据结束 ");
	}

	/**
	 * 根据搜索 条件获取匹配结果
	 * 
	 * @param psoName
	 *            流程中岗位名称
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getSearchPosCount(String psoName) throws Exception {
		String sql = "SELECT COUNT(distinct EPS_POSITION_ID) FROM JECN_MATCH_POS_EPROS_REL,JECN_FLOW_ORG_IMAGE FL_P WHERE FL_P.FIGURE_ID=EPS_POSITION_ID AND "
				+ "FL_P.FIGURE_TYPE='PositionFigure'";
		if (!JecnCommon.isNullOrEmtryTrim(psoName)) {
			sql = sql + "AND FL_P.FIGURE_TEXT LIKE '%" + psoName + "%'";
		}
		Query query = this.getSession().createSQLQuery(sql);
		return Integer.parseInt(query.uniqueResult().toString());
	}

	/**
	 * 分页查询获取岗位匹配数据
	 * 
	 * @param pageSize
	 * @param startRow
	 * @param sql
	 * @param args
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Long> getPosEpsListInfBySearch(int pageSize, int startRow,
			String sql, Object[] args) throws Exception {
		final int pageSize1 = pageSize;
		final int startRow1 = startRow;
		final String sqlStr = sql;
		Query query = this.getSession().createQuery(sqlStr);
		if (args != null) {
			for (int i = 0; i < args.length; i++) {
				query.setParameter(i, args[i]);
			}
		}
		query.setFirstResult(startRow1);
		query.setMaxResults(pageSize1);
		return query.list();
	}

	/**
	 * 根据岗位编号获取岗位名称 获取数据库驱动名称
	 * 
	 * @param listNames
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getPosRelList(List<Long> posIdList) throws Exception {
		String sql = null;
		if (posIdList != null && posIdList.size() > 0) {
			sql = "select mper.ID,mper.REL_TYPE,mper.POSITION_NUM,mper.EPS_POSITION_ID,jfoi.figure_Text "
					+ "from JECN_MATCH_POS_EPROS_REL mper,Jecn_Flow_Org_Image jfoi  where jfoi.figure_Id=mper.EPS_POSITION_ID and "
					+ " jfoi.figure_Type='PositionFigure' and mper.EPS_POSITION_ID in"
					+ JecnCommonSql.getIds(posIdList)
					+ " order by jfoi.figure_Id,jfoi.figure_Text,mper.POSITION_NUM";
		}
		return this.listNativeSql(sql);
	}

	/**
	 * 获取未匹配的岗位结果
	 * 
	 * @param pageSize
	 * @param startRow
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getActualPosBeanList(int pageSize, int start,
			String posName) throws Exception {
		log.info("获取没有匹配关系的记录数据开始 ");
		String sql = MatchSqlConstant.FIND_STATION_NO_MATCH;
		if (posName != null && !"".equals(posName)) {
			sql = sql + " and SF_P.ACT_POS_NAME like '%" + posName + "%'";
		}
		sql = sql + " ORDER BY SF_P.BASE_POS_NAME,ACT_POS_NAME";
		return listNativeSql(sql, start, pageSize);
	}

	/**
	 * 获取未匹配的岗位结果
	 * 
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getNoMatchObject(int type, int start, int pageSize,
			String posName) throws Exception {
		log.info("获取没有匹配关系的记录数据开始 ");
		String sql = null;
		if (type == 6) {// HR中未匹配的基准岗位
			sql = MatchSqlConstant.FIND_BASE__POS_NO_MATCH;
			if (posName != null && !"".equals(posName)) {
				sql = sql + " and BASE_POS_NAME like '%" + posName + "%' ";
			}
			sql = sql
					+ " GROUP BY BASE_POS_NUM, BASE_POS_NAME ORDER BY BASE_POS_NAME";
		} else if (type == 5) {// 流程中未匹配的岗位
			sql = MatchSqlConstant.FIND_FLOW_POS_NAME;
			if (posName != null && !"".equals(posName)) {
				sql = sql + "AND F_P.FIGURE_TEXT LIKE '%" + posName + "%' ";
			}
			sql = sql + " ORDER BY F_O.ORG_NAME,F_P.FIGURE_TEXT";
		}
		return this.listNativeSql(sql, start, pageSize);
	}

	/**
	 * 
	 * 获取未匹配的岗位或基准岗位或流程岗位
	 * 
	 * @param type
	 *            1:HR实际岗位；5：流程岗位；6基准岗位
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getNoMatchObjectCount(int type, String posName) throws Exception {
		// 获取Session以便左数据库操作
		String sql = "";
		// 1:HR实际岗位；5：流程岗位；6基准岗位
		if (type == 1) {
			sql = MatchSqlConstant.FIND_STATION_NO_MATCH_COUNT;
			if (posName != null && !"".equals(posName)) {
				sql = sql + " and SF_P.ACT_POS_NAME like '%" + posName + "%'";
			}
		} else if (type == 5) {
			sql = MatchSqlConstant.FIND_FLOW_POS_NAME_COUNT;
			if (posName != null && !"".equals(posName)) {
				sql = sql + " and F_P.Figure_Text like '%" + posName + "%' ";
			}
			sql = sql + " )TMP_POS";
		} else if (type == 6) {
			sql = MatchSqlConstant.FIND_BASE__POS_NO_MATCH_COUNT;
			if (posName != null && !"".equals(posName)) {
				sql = sql + "and BASE_POS_NAME like '%" + posName + "%'";
			}
		}
		return this.countAllByParamsNativeSql(sql);
	}

	/**
	 * 根据岗位编号获取岗位名称 获取数据库驱动名称
	 * 
	 * @param listNames
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getPosNamesByNum(List<String> listPosNums)
			throws Exception {
		String sql = null;
		if (listPosNums != null && listPosNums.size() > 0) {
			sql = "select actPosNum,actPosName from ActualPosBean where "
					+ " actPosNum in " + JecnCommonSql.getStrs(listPosNums);
		}
		return listHql(sql);
	}

	/**
	 * 根据基准岗位编号获取基准岗位名称
	 * 
	 * @param listPosBaseNums
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getPosNamesByBaseNum(List<String> listPosBaseNums)
			throws Exception {
		String sql = null;
		if (listPosBaseNums != null && listPosBaseNums.size() > 0) {
			sql = "select basePosNum,basePosName from ActualPosBean where "
					+ " basePosNum in "
					+ JecnCommonSql.getStrs(listPosBaseNums)
					+ " group by basePosNum,basePosName";
		}
		return listHql(sql);
	}

	/**
	 * 根据岗位ID获取岗位名称
	 * 
	 * @param posList
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Object[]> getPosNameList(List<Long> posList) throws Exception {
		String sql = "select figureId,figureText from "
				+ "JecnFlowOrgImage where  "
				+ "  figureType='PositionFigure' and figureId in "
				+ JecnCommonSql.getIds(posList) + "order by figureText";
		return listHql(sql);
	}

	/**
	 * 查询 数据库中 要更新删除的数据
	 * 
	 * @param stationId
	 * @param strNumList
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<PosEprosRelBean> findRelByJspNum(Long stationId,
			List<String> strNumList) throws Exception {
		String sql = "SELECT * FROM JECN_MATCH_POS_EPROS_REL WHERE EPS_POSITION_ID =  "
				+ stationId
				+ " AND POSITION_NUM NOT IN "
				+ JecnCommonSql.getStrs(strNumList);
		List<PosEprosRelBean> list = this.getSession().createSQLQuery(sql)
				.addEntity(PosEprosRelBean.class).list();
		return list;
	}

	/***************************************************************************
	 * 根据流程岗位ID 查询流程岗位与实际岗位关联表数据
	 * 
	 * @param stationId
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<String> findPosEprosRelList(Long stationId) throws Exception {
		Query query = this.getSession().createSQLQuery(
				MatchSqlConstant.FIND_REL_BY_FLOWID);
		query.setLong("STATIONID", stationId);
		List<String> posEpsRelList = query.list();
		return posEpsRelList;
	}

	/***************************************************************************
	 * 删除 编辑页面中没有的数据库中的数据
	 * 
	 * @param deleteNumList
	 * @throws Exception
	 */
	@Override
	public void deleteRelNum(List<PosEprosRelBean> deleteNumList,
			List<Long> deletePeopleIds, Long flowPosId) throws Exception {
		// 执行插入
		for (PosEprosRelBean posEprosRelBean : deleteNumList) {
			this.getSession().delete(posEprosRelBean);
		}
		if (deletePeopleIds != null && deletePeopleIds.size() > 0) {
			String sql = "delete from JECN_USER_POSITION_RELATED where FIGURE_ID="
					+ flowPosId
					+ " and PEOPLE_ID in "
					+ JecnCommonSql.getIds(deletePeopleIds);
			Query query = this.getSession().createSQLQuery(sql);
			query.executeUpdate();
		}
	}

	/**
	 * 根据岗位ID删除人员岗位关联信息
	 * 
	 * @param this.getSession()
	 * @param deletePosEpsRelList
	 * @throws Exception
	 */
	@Override
	public void deletePosRelUser() throws Exception {
		String sql = "delete from JECN_USER_POSITION_RELATED"
				+ "  where figure_Id in"
				+ "        (SELECT DISTINCT EPS_POSITION_ID"
				+ "           FROM JECN_MATCH_POS_EPROS_REL"
				+ "          WHERE (POSITION_NUM NOT IN"
				+ "                (SELECT SAP.ACT_POS_NUM FROM JECN_MATCH_POS_BEAN SAP) AND"
				+ "                REL_TYPE = 0)"
				+ "         union"
				+ "         SELECT DISTINCT EPS_POSITION_ID"
				+ "           FROM JECN_MATCH_POS_EPROS_REL"
				+ "          WHERE (POSITION_NUM NOT IN"
				+ "                (SELECT distinct S.BASE_POS_NUM FROM JECN_MATCH_POS_BEAN S) AND"
				+ "                REL_TYPE = 1))";

		Query query = this.getSession().createSQLQuery(sql);
		query.executeUpdate();
		log.info("删除人员岗位关联表(JecnUserInfo）中的数据结束 ");
	}

	/**
	 * 获得主项目
	 * 
	 * @author zhangchen
	 * @return
	 * @throws Exception
	 */
	public Long getCurJecnProject() throws Exception {
		String queryString = "select a from JecnProject as a,JecnCurProject as b where a.delSate = 0 and a.projectId = b.curProjectId";
		List<JecnProject> list = this.listHql(queryString);
		if (list.size() > 0) {
			return list.get(0).getProjectId();
		} else {
			return null;
		}
	}

	/**
	 * 
	 * 获取数据库驱动名称
	 * 
	 * @return
	 */
	public DBType getDBDType() {
		// 获取驱动方言名称
		String dialectName = ((SessionImpl) getSession()).getFactory()
				.getDialect().getClass().getName();

		if ("org.hibernate.dialect.OracleDialect".equals(dialectName)) {
			return DBType.ORACLE;
		} else if ("org.hibernate.dialect.MySQLDialect".equals(dialectName)) {
			return DBType.MYSQL;
		} else if ("org.hibernate.dialect.SQLServerDialect".equals(dialectName)) {
			return DBType.SQLSERVER;
		} else {
			return DBType.NONE;
		}
	}

	enum DBType {
		SQLSERVER, ORACLE, MYSQL, NONE
	}

	/***************************************************************************
	 * 删除部门，岗位，人员实际表中原有的数据
	 * 
	 * @throws Exception
	 */
	// /***
	@Override
	public void deleteActualMatchTableData() throws Exception {
		log.info("删除部门，岗位，人员实际表中原有的数据");
		// 删除 部门实际表中的数据JECN_MATCH_ACTUAL_DEPT
		Query query = this.getSession().createSQLQuery(
				"delete from JECN_MATCH_ACTUAL_DEPT");
		query.executeUpdate();
		// 删除人员实际表中的数据JECN_MATCH_ACTUAL_PERSON
		query = this.getSession().createSQLQuery(
				"delete from JECN_MATCH_ACTUAL_PERSON");
		query.executeUpdate();
		// 删除岗位实际表中的数据JECN_MATCH_ACTUAL_POS
		query = this.getSession().createSQLQuery(
				"delete from JECN_MATCH_ACTUAL_POS");
		query.executeUpdate();
		log.info("删除部门，岗位，人员实际表中原有的数据");
	}

	/***************************************************************************
	 * 将部门，岗位，人员临时表中的数据添加到部门，岗位，人员实际表中
	 * 
	 * @throws Exception
	 */
	// /***
	@Override
	public void insertActualMatchTableData() throws Exception {
		log.info("将部门，岗位，人员临时表中的数据添加到部门，岗位，人员实际表中");
		// 临时表数据添加到部门实际表JECN_MATCH_ACTUAL_DEPT
		String deptSql = null;
		String posSql = null;
		String userSql = null;
		if (DBType.ORACLE.equals(getDBDType())) {
			deptSql = "insert into jecn_match_actual_dept"
					+ "(DEPT_ID, DEPT_NUM, dept_name, p_dept_num, project_id)"
					+ "select SANFU_ACTUAL_DEPT_SQUENCE.NEXTVAL, dept_num, DEPT_NAME,P_DEPT_NUM,PROJECT_ID from jecn_match_dept_bean";
			posSql = "insert into jecn_match_actual_person "
					+ "(ID, LOGIN_NAME, TRUE_NAME, EMAIL, PHONE,EMAIL_TYPE,POSITION_NUM) "
					+ "select SANFU_ACTUAL_POS_SQUENCE.NEXTVAL, LOGIN_NAME, TRUE_NAME,EMAIL,PHONE,EMAIL_TYPE,POSITION_NUM from jecn_match_user_bean";

			userSql = "insert into jecn_match_actual_pos"
					+ "(ID, BASE_POS_NUM, BASE_POS_NAME, DEPT_NUM, ACT_POS_NUM,ACT_POS_NAME)"
					+ "select SANFU_ACTUAL_PERSON_SQUENCE.NEXTVAL, BASE_POS_NUM, BASE_POS_NAME,DEPT_NUM,ACT_POS_NUM,ACT_POS_NAME from jecn_match_pos_bean";
		} else if (DBType.SQLSERVER.equals(getDBDType())) {
			deptSql = "insert into jecn_match_actual_dept"
					+ "(DEPT_NUM, dept_name, p_dept_num, project_id)"
					+ "select  dept_num, DEPT_NAME,P_DEPT_NUM,PROJECT_ID from jecn_match_dept_bean";
			posSql = "insert into jecn_match_actual_person "
					+ "( LOGIN_NAME, TRUE_NAME, EMAIL, PHONE,EMAIL_TYPE,POSITION_NUM) "
					+ "select  LOGIN_NAME, TRUE_NAME,EMAIL,PHONE,EMAIL_TYPE,POSITION_NUM from jecn_match_user_bean";

			userSql = "insert into jecn_match_actual_pos"
					+ "( BASE_POS_NUM, BASE_POS_NAME, DEPT_NUM, ACT_POS_NUM,ACT_POS_NAME)"
					+ "select BASE_POS_NUM, BASE_POS_NAME,DEPT_NUM,ACT_POS_NUM,ACT_POS_NAME from jecn_match_pos_bean";
		}
		Query query = this.getSession().createSQLQuery(deptSql);
		query.executeUpdate();
		// 临时表数据添加到人员实际表JECN_MATCH_ACTUAL_PERSON
		query = this.getSession().createSQLQuery(posSql);
		query.executeUpdate();
		// 临时表数据添加到岗位实际表JECN_MATCH_ACTUAL_POS
		query = this.getSession().createSQLQuery(userSql);
		query.executeUpdate();
		log.info("将部门，岗位，人员临时表中的数据添加到部门，岗位，人员实际表中");
	}

}
