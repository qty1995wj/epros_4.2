package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseCircle;

/**
 * 石勘院，流程开始
 * 
 *@author ZXH
 * @date 2016-8-27上午11:23:19
 */
public class StartFigureAR extends BaseCircle {
	public StartFigureAR(JecnFigureData flowElementData) {
		super(flowElementData);
	}
}
