package com.jecn.webservice.xinghan.bean;

/*** 点属性 ***/
public class Point {
	/*** X坐标（连接线x坐标） *******/
	private int xLinkPoint;
	/**** Y坐标（连接线y坐标） *********/
	private int yLinkPoint;

	public String getxLinkPoint() {
		if (xLinkPoint == 0) {
			return "";
		}
		return String.valueOf(xLinkPoint);
	}

	public void setxLinkPoint(int xLinkPoint) {
		this.xLinkPoint = xLinkPoint;
	}

	public String getyLinkPoint() {
		if (yLinkPoint == 0) {
			return "";
		}
		return String.valueOf(yLinkPoint);
	}

	public void setyLinkPoint(int yLinkPoint) {
		this.yLinkPoint = yLinkPoint;
	}

}
