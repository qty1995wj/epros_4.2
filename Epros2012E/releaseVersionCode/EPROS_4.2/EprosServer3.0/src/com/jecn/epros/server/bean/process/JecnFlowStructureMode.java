package com.jecn.epros.server.bean.process;

import java.util.Date;

public class JecnFlowStructureMode implements java.io.Serializable {
	private Long flowId;//主键ID
	private String flowName;//流程名称
	private Long moLevel;//0流程地图模板   1是流程模板
	private String isEdit;//是否处于编辑
	private String moWidth;//画图面板的宽	
	private String moHeight;//画图面板的高
	private Long isShowLineX;//显示横分割线（1）
	private Long isShowLineY;//显示纵分割线（1）
	private Long parentId;// 父节点
	private Date createTime;// 创建时间
	private Date updateTime;// 更新时间
	private Long createPeopleId;// 创建人ID
	private Long updatePeopleId;// 更新人ID
	private Integer sortId;// 排序
	public JecnFlowStructureMode() {

	}


	public Long getFlowId() {
		return flowId;
	}


	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}


	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}


	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	public String getMoWidth() {
		return moWidth;
	}

	public void setMoWidth(String moWidth) {
		this.moWidth = moWidth;
	}

	public String getMoHeight() {
		return moHeight;
	}

	public void setMoHeight(String moHeight) {
		this.moHeight = moHeight;
	}

	public Long getMoLevel() {
		return moLevel;
	}
	public void setMoLevel(Long moLevel) {
		this.moLevel = moLevel;
	}
	public Long getIsShowLineX() {
		return isShowLineX;
	}
	public void setIsShowLineX(Long isShowLineX) {
		this.isShowLineX = isShowLineX;
	}
	public Long getIsShowLineY() {
		return isShowLineY;
	}
	public void setIsShowLineY(Long isShowLineY) {
		this.isShowLineY = isShowLineY;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Long getCreatePeopleId() {
		return createPeopleId;
	}
	public void setCreatePeopleId(Long createPeopleId) {
		this.createPeopleId = createPeopleId;
	}
	public Long getUpdatePeopleId() {
		return updatePeopleId;
	}
	public void setUpdatePeopleId(Long updatePeopleId) {
		this.updatePeopleId = updatePeopleId;
	}
	public Integer getSortId() {
		return sortId;
	}
	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}
}
