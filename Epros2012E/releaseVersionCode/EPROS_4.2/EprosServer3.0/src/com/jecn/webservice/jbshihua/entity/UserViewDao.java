package com.jecn.webservice.jbshihua.entity;

import java.util.List;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.dataImport.sync.JecnUserPosView;

public interface UserViewDao extends IBaseDao<String, String> {

	int deleteUser(String code) throws Exception;
	
	int insertUser(JecnUserPosView user) throws Exception;
	
	int updateUser(JecnUserPosView user) throws Exception;
	
	int updateUserJob(JecnUserPosView user) throws Exception;
	
	int updateUserOldJob(JecnUserPosView user) throws Exception;
	
	List<JecnUserPosView> getUser(String code) throws Exception;
	
	int getUserCounts(String code) throws Exception;
	
	JecnUserPosView getUserJob(String code,String jobNum,String status) throws Exception;
	
	JecnUserPosView getUserJob(String code,String jobNum) throws Exception;

	int updateUserAndPost(JecnUserPosView user) throws Exception;

	int updatePost(PostInfo postInfo) throws Exception;

	int insertPost(PostInfo postInfo) throws Exception;
}
