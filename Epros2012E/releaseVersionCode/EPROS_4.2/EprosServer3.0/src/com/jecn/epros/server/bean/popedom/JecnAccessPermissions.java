package com.jecn.epros.server.bean.popedom;

/**
 * 岗位权限查阅表
 * @author Administrator
 *
 */
public class JecnAccessPermissions extends BaseAccess implements java.io.Serializable{
	/**
	 * 主键Id
	 */
	private Long id;
	/**
	 * 岗位Id
	 */
	private Long figureId;
	/**
	 * 岗位名称
	 */
	private String figureName;
	/**
	 * 关联Id
	 */
	private Long relateId;
	/**
	 * 0是流程、1是文件、2是标准、3制度
	 */
	private Integer type;
	
	/**
	 * 关联名称
	 */
	private String relateName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getFigureId() {
		return figureId;
	}
	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}
	public Long getRelateId() {
		return relateId;
	}
	public void setRelateId(Long relateId) {
		this.relateId = relateId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getRelateName() {
		return relateName;
	}
	public void setRelateName(String relateName) {
		this.relateName = relateName;
	}
	public String getFigureName() {
		return figureName;
	}
	public void setFigureName(String figureName) {
		this.figureName = figureName;
	}
}
