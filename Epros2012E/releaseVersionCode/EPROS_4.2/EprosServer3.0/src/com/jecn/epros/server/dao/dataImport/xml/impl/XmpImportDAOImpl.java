package com.jecn.epros.server.dao.dataImport.xml.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnPositionFigInfo;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.JecnUserInfo;
import com.jecn.epros.server.bean.popedom.UserRole;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.dataImport.xml.IXmpImportDAO;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.DepartmentBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.JecnStationMatch;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.PersonBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.PersonPositionRefBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.PositionBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.StationAndPeopleRelate;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.XmlStructureBean;

/**
 * XML人员同步
 * 
 * @author Administrator
 * @date： 日期：Feb 25, 2013 时间：3:59:33 PM
 */
public class XmpImportDAOImpl extends AbsBaseDao<PersonBean, Long> implements
		IXmpImportDAO {
	// private static final List<Long> Long = null;
	@Override
	public void saveXmlStructureBean(XmlStructureBean xsb) throws Exception {
		LOGGER.debug("saving XmlStructureBean instance");
		this.getSession().save(xsb);
	}

	@Override
	public void updateXmlStructureBean(XmlStructureBean xsb) throws Exception {
		LOGGER.debug("update XmlStructureBean instance");
		this.getSession().update(xsb);
		LOGGER.debug("update successful");
	}

	@Override
	public boolean saveDepartmentBean(List<DepartmentBean> newDeptList)
			throws Exception {
		List<DepartmentBean> existDeptList;
		List<PositionBean> existPositionList;
		List<PersonBean> existPersonList;
		List<PersonPositionRefBean> existPPRList;
		boolean isChange = false;
		Session s = this.getSession();
		Query getAllDepment = s.createQuery("from DepartmentBean");
		existDeptList = getAllDepment.list();
		Query getAllPosition = s.createQuery("from PositionBean");
		existPositionList = getAllPosition.list();
		Query getAllPerson = s.createQuery("from PersonBean");
		existPersonList = getAllPerson.list();
		Query getAllPPR = s.createQuery("from PersonPositionRefBean");
		existPPRList = getAllPPR.list();
		if (newDeptList == null || newDeptList.size() == 0) {
			return false;
		}
		// 数据库为空
		if (existDeptList == null || existDeptList.size() == 0) {
			// 组织(开始)
			List<String> loginNameList = new ArrayList<String>();
			for (DepartmentBean dept : newDeptList) {
				if (dept.getParentDeptId() == null) {
					dept.setPid(0L);
					s.save(dept);
					addNewDeptChild(s, dept, loginNameList);
					saveDepartmentBeanIterator(s, dept, newDeptList,
							loginNameList);
				}
			}
			// 组织(结束)
		}
		// 数据库不为空
		else {
			List<DepartmentBean> updateDeptList = new ArrayList<DepartmentBean>();
			List<PositionBean> updatePositionList = new ArrayList<PositionBean>();
			List<PositionBean> delPositionList = new ArrayList<PositionBean>();
			List<PersonBean> updatePersonList = new ArrayList<PersonBean>();
			List<PersonBean> delPersonList = new ArrayList<PersonBean>();
			List<PersonPositionRefBean> updatePPRList = new ArrayList<PersonPositionRefBean>();
			List<PersonPositionRefBean> delPPRList = new ArrayList<PersonPositionRefBean>();

			List<DepartmentBean> addDeptList = new ArrayList<DepartmentBean>();
			for (DepartmentBean newDept : newDeptList) {
				boolean addDeptFlag = true;
				for (DepartmentBean existDept : existDeptList) {
					// 更新组织(开始)
					if (newDept.getDeptId().equals(existDept.getDeptId())) {
						existDept.setDeptName(newDept.getDeptName());
						// 更新关联
						Long oldPid = existDept.getPid();
						for (DepartmentBean existDept2 : existDeptList) {
							if (newDept.getParentDeptId() != null
									&& newDept.getParentDeptId().equals(
											existDept2.getDeptId())) {
								existDept.setPid(existDept2.getId());
							}
						}
						if (!oldPid.equals(existDept.getPid())) {
							isChange = true;
						}
						s.update(existDept);
						updateDeptList.add(existDept);
						addDeptFlag = false;
						// 添加组织下内容
						newDept.setId(existDept.getId());
						addDeptChildToDB(s, newDept, existPositionList,
								updatePositionList, existPersonList,
								updatePersonList, existPPRList, updatePPRList,
								isChange);
					}
					// 更新组织(结束)
				}
				if (addDeptFlag == true) {
					addDeptList.add(newDept);
				}
			}
			// 添加组织(开始)
			for (DepartmentBean addDept : addDeptList) {
				if (addDept.getParentDeptId() == null) {
					addDept.setPid(0L);
					s.save(addDept);
					isChange = true;
					// 添加组织下内容
					addDeptChildToDB(s, addDept, existPositionList,
							updatePositionList, existPersonList,
							updatePersonList, existPPRList, updatePPRList,
							isChange);
				} else {
					for (DepartmentBean dept2 : existDeptList) {
						if (dept2.getDeptId().equals(addDept.getParentDeptId())) {
							addDept.setPid(dept2.getId());
							s.save(addDept);
							isChange = true;
							// 添加组织下内容
							addDeptChildToDB(s, addDept, existPositionList,
									updatePositionList, existPersonList,
									updatePersonList, existPPRList,
									updatePPRList, isChange);
						}
					}
				}
				saveDepartmentBeanIterator2(s, addDept, addDeptList,
						existDeptList, existPositionList, updatePositionList,
						existPersonList, updatePersonList, existPPRList,
						updatePPRList, isChange);
			}
			// 添加组织(结束)

			// 删除组织及其关联
			existDeptList.removeAll(updateDeptList);
			for (DepartmentBean delDept : existDeptList) {
				s.delete(delDept);
				isChange = true;
				for (PositionBean delPost : existPositionList) {
					if (delPost.getDeptId().equals(delDept.getId())) {
						delPositionList.add(delPost);
						for (PersonPositionRefBean delPPR : existPPRList) {
							if (delPPR.getPosId().equals(delPost.getId())) {
								delPPRList.add(delPPR);
								for (PersonBean delperson : existPersonList) {
									if (delPPR.getPersonId().equals(
											delperson.getId())) {
										delPersonList.add(delperson);
									}
								}
							}
						}
					}
				}
				deleteDeptIterator(s, delDept, existDeptList,
						existPositionList, delPositionList, existPPRList,
						delPPRList, existPersonList, delPersonList, isChange);
			}

			existPositionList.removeAll(updatePositionList);
			delPositionList.addAll(existPositionList);
			existPersonList.removeAll(updatePersonList);
			delPersonList.addAll(existPersonList);
			existPPRList.removeAll(updatePPRList);
			delPPRList.addAll(existPPRList);

			if (delPositionList != null && delPositionList.size() > 0) {
				isChange = true;
				List<Long> delPositionIdList = new ArrayList<Long>();
				for (PositionBean delpos : delPositionList) {
					delPositionIdList.add(delpos.getId());
				}
				Query deletePositionList = s
						.createQuery("delete from PositionBean where id in"
								+ JecnCommonSql.getIds(delPositionIdList));
				deletePositionList.executeUpdate();
				s.flush();
			}

			if (delPersonList != null && delPersonList.size() > 0) {
				List<Long> delPersonIdList = new ArrayList<Long>();
				for (PersonBean delperson : delPersonList) {
					delPersonIdList.add(delperson.getId());
				}
				Query deletePersonList = s
						.createQuery("delete from PersonBean where id in"
								+ JecnCommonSql.getIds(delPersonIdList));
				deletePersonList.executeUpdate();
				s.flush();
			}

			if (delPPRList != null && delPPRList.size() > 0) {
				List<Long> delPPRIdList = new ArrayList<Long>();
				for (PersonPositionRefBean delppr : delPPRList) {
					delPPRIdList.add(delppr.getId());
				}
				Query deletePPRList = s
						.createQuery("delete from PersonPositionRefBean where id in"
								+ JecnCommonSql.getIds(delPPRIdList));
				deletePPRList.executeUpdate();
				s.flush();
			}
		}
		s.flush();
		return isChange;
	}

	private void saveDepartmentBeanIterator(Session s, DepartmentBean dept,
			List<DepartmentBean> deptList, List<String> loginNameList) {
		for (DepartmentBean dept2 : deptList) {
			if (dept2.getParentDeptId() != null
					&& dept2.getParentDeptId().equals(dept.getDeptId())) {
				dept2.setPid(dept.getId());
				s.save(dept2);
				addNewDeptChild(s, dept2, loginNameList);
				saveDepartmentBeanIterator(s, dept2, deptList, loginNameList);
			}
		}
	}

	private void saveDepartmentBeanIterator2(Session s, DepartmentBean dept,
			List<DepartmentBean> addDeptList,
			List<DepartmentBean> existDeptList,
			List<PositionBean> existPositionList,
			List<PositionBean> updatePositionList,
			List<PersonBean> existPersonList,
			List<PersonBean> updatePersonList,
			List<PersonPositionRefBean> existPPRList,
			List<PersonPositionRefBean> updatePPRList, boolean isChange) {
		for (DepartmentBean dept2 : existDeptList) {
			if (dept2.getDeptId().equals(dept.getParentDeptId())) {
				dept.setPid(dept2.getId());
				s.save(dept);
				isChange = true;
				// 添加组织下内容
				addDeptChildToDB(s, dept2, existPositionList,
						updatePositionList, existPersonList, updatePersonList,
						existPPRList, updatePPRList, isChange);
			}
		}

		for (DepartmentBean dept2 : addDeptList) {
			if (dept2.getParentDeptId() != null
					&& dept2.getParentDeptId().equals(dept.getDeptId())) {
				dept2.setPid(dept.getId());
				s.save(dept2);
				isChange = true;
				// 添加组织下内容
				addDeptChildToDB(s, dept2, existPositionList,
						updatePositionList, existPersonList, updatePersonList,
						existPPRList, updatePPRList, isChange);
				saveDepartmentBeanIterator2(s, dept2, addDeptList,
						existDeptList, existPositionList, updatePositionList,
						existPersonList, updatePersonList, existPPRList,
						updatePPRList, isChange);
			}
		}
	}

	public void deleteDeptIterator(Session s, DepartmentBean delDept,
			List<DepartmentBean> existDeptList,
			List<PositionBean> existPositionList,
			List<PositionBean> delPositionList,
			List<PersonPositionRefBean> existPPRList,
			List<PersonPositionRefBean> delPPRList,
			List<PersonBean> existPersonList, List<PersonBean> delPersonList,
			boolean isChange) {

		for (DepartmentBean delDept2 : existDeptList) {
			if (delDept2.getPid().equals(delDept.getId())) {
				s.delete(delDept2);
				isChange = true;
				for (PositionBean delPost : existPositionList) {
					if (delPost.getDeptId().equals(delDept2.getId())) {
						delPositionList.add(delPost);
						for (PersonPositionRefBean delPPR : existPPRList) {
							if (delPPR.getPosId().equals(delPost.getId())) {
								delPPRList.add(delPPR);
								for (PersonBean delperson : existPersonList) {
									if (delPPR.getPersonId().equals(
											delperson.getId())) {
										delPersonList.add(delperson);
									}
								}
							}
						}
					}
				}
				deleteDeptIterator(s, delDept2, existDeptList,
						existPositionList, delPositionList, existPPRList,
						delPPRList, existPersonList, delPersonList, isChange);
			}
		}
	}

	private void addNewDeptChild(Session s, DepartmentBean dept,
			List<String> loginNameList) {
		// 岗位(开始)
		List<String> posNameList = new ArrayList<String>();
		Long posId = 0L;
		for (PositionBean pos : dept.getPositionList()) {
			if (posNameList.size() == 0
					|| !posNameList.contains(pos.getPosName())) {
				pos.setDeptId(dept.getId());
				pos.setIsMatch(0);
				s.save(pos);
				posNameList.add(pos.getPosName());
				posId = pos.getId();
			}
			// 人员(开始)
			Long personId = 0L;
			for (PersonBean person : pos.getPersonList()) {
				if (loginNameList.size() == 0
						|| !loginNameList.contains(person.getLoginName())) {
					s.save(person);
					loginNameList.add(person.getLoginName());
					personId = person.getId();
				}
				PersonPositionRefBean ppr = new PersonPositionRefBean();
				ppr.setPersonId(personId);
				ppr.setPosId(posId);
				s.save(ppr);
			}
			// 人员(结束)
		}
		// 岗位(结束)
	}

	// 包含数据库操作
	private void addDeptChildToDB(Session s, DepartmentBean existDept,
			List<PositionBean> existPositionList,
			List<PositionBean> updatePositionList,
			List<PersonBean> existPersonList,
			List<PersonBean> updatePersonList,
			List<PersonPositionRefBean> existPPRList,
			List<PersonPositionRefBean> updatePPRList, boolean isChange) {
		List<String> posNameList = new ArrayList<String>();
		Long posId = 0L;

		// 没有岗位,直接新增
		if (existDept.getPositionList() == null
				|| existDept.getPositionList().size() == 0) {

		}
		for (PositionBean newPos : existDept.getPositionList()) {
			boolean addPosFlag = true;
			for (PositionBean existPos : existPositionList) {
				// 更新岗位
				if (newPos.getPosName().equals(existPos.getPosName())
						&& existDept.getId().equals(existPos.getDeptId())) {
					// ) {
					// s.update(existPos);
					updatePositionList.add(existPos);
					posNameList.add(existPos.getPosName());
					posId = existPos.getId();
					addPosFlag = false;
					// 新增人员及关联关系(开始)
					// 新增人员(开始)
					Long personId = 0L;
					for (PersonBean newPerson : newPos.getPersonList()) {
						boolean addPersonFlag = true;
						for (PersonBean existPerson : existPersonList) {
							if (newPerson.getLoginName().equals(
									existPerson.getLoginName())) {
								existPerson
										.setTrueName(newPerson.getTrueName());
								existPerson.setPhone(newPerson.getPhone());
								existPerson
										.setTrueName(newPerson.getTrueName());
								existPerson.setUserNumber(newPerson
										.getUserNumber());
								existPerson.setEmail(newPerson.getEmail());
								existPerson
										.setPersonId(newPerson.getPersonId());
								s.update(existPerson);
								updatePersonList.add(existPerson);
								addPersonFlag = false;
								personId = existPerson.getId();
							}
						}

						if (addPersonFlag == true) {
							s.save(newPerson);
							personId = newPerson.getId();
						}
						// 新增人员(结束)
						// 关联关系(开始)
						PersonPositionRefBean ppr = new PersonPositionRefBean();
						ppr.setPersonId(personId);
						ppr.setPosId(posId);
						boolean addPPRFlag = true;
						for (PersonPositionRefBean existPPR : existPPRList) {
							if (ppr.getPersonId()
									.equals(existPPR.getPersonId())
									&& ppr.getPosId().equals(
											existPPR.getPosId())) {
								updatePPRList.add(existPPR);
								addPPRFlag = false;
							}
						}
						if (addPPRFlag == true) {
							s.save(ppr);
						}
						// 关联关系(结束)
					}
					// 新增人员及关联关系(结束)
				}
			}
			// 新增岗位(开始)
			if (addPosFlag == true) {
				if (posNameList.size() == 0
						|| !posNameList.contains(newPos.getPosName())) {
					newPos.setDeptId(existDept.getId());
					// if (newPos.getDeptId() != null) {
					newPos.setDeptId(existDept.getId());
					newPos.setIsMatch(0);
					s.save(newPos);
					isChange = true;
					// }
					posNameList.add(newPos.getPosName());
					posId = newPos.getId();
					// 新增人员及关联关系(开始)
					// 新增人员(开始)
					Long personId = 0L;
					for (PersonBean newPerson : newPos.getPersonList()) {
						boolean addPersonFlag = true;
						for (PersonBean existPerson : existPersonList) {
							if (newPerson.getLoginName().equals(
									existPerson.getLoginName())) {
								existPerson
										.setTrueName(newPerson.getTrueName());
								existPerson.setPhone(newPerson.getPhone());
								existPerson
										.setTrueName(newPerson.getTrueName());
								existPerson.setUserNumber(newPerson
										.getUserNumber());
								existPerson.setEmail(newPerson.getEmail());
								existPerson
										.setPersonId(newPerson.getPersonId());
								s.update(existPerson);
								updatePersonList.add(existPerson);
								addPersonFlag = false;
								personId = existPerson.getId();
							}
						}
						if (addPersonFlag == true) {
							s.save(newPerson);
							personId = newPerson.getId();
						}
						// 新增人员(结束)
						// 关联关系(开始)
						PersonPositionRefBean ppr = new PersonPositionRefBean();
						ppr.setPersonId(personId);
						ppr.setPosId(posId);
						boolean addPPRFlag = true;
						for (PersonPositionRefBean existPPR : existPPRList) {
							if (ppr.getPersonId()
									.equals(existPPR.getPersonId())
									&& ppr.getPosId().equals(
											existPPR.getPosId())) {
								updatePPRList.add(existPPR);
								addPPRFlag = false;
							}
						}
						if (addPPRFlag == true) {
							s.save(ppr);
						}
						// 关联关系(结束)
					}
					// 新增人员及关联关系(结束)
				}
			}
			// 新增岗位(结束)
		}
	}

	@Override
	public List<DepartmentBean> getAllDeptment() throws Exception {
		LOGGER.debug("finding all DepartmentBean instances");
		String hql = "from DepartmentBean";
		return listHql(hql);
	}

	@Override
	public void clear() throws Exception {
		LOGGER.debug("clear JECN_DATA_XML_STRUCTURE table");
		String hql = "delete from XmlStructureBean";
		this.execteHql(hql);
	}

	@Override
	public List<XmlStructureBean> getAllXmlStructure() throws Exception {
		LOGGER.debug("finding all XmlStructureBean instances");
		String hql = "from XmlStructureBean order by id";
		return listHql(hql);
	}

	@Override
	public List<DepartmentBean> getAllOrg() throws Exception {
		LOGGER.debug("finding all XmlStructureBean instances");
		String hql = "from DepartmentBean order by id";
		return listHql(hql);
	}

	@Override
	public List<PositionBean> getAllPosition() throws Exception {
		LOGGER.debug("finding all XmlStructureBean instances");
		String hql = "from PositionBean";
		return listHql(hql);
	}

	@Override
	public List<PersonBean> getAllPersonBean() throws Exception {
		LOGGER.debug("finding all XmlStructureBean instances");
		String hql = "from PersonBean";
		return listHql(hql);
	}

	public List<JecnUserInfo> getNewJecnUserInfo(Long id, Long peopleId,
			List<PersonPositionRefBean> listPersonPositionRefBean,
			List<JecnStationMatch> listJecnStationMatch) {
		List<JecnUserInfo> listResult = new ArrayList<JecnUserInfo>();
		List<Long> listIds = new ArrayList<Long>();
		for (int i = 0; i < listPersonPositionRefBean.size(); i++) {
			if (id.equals(listPersonPositionRefBean.get(i).getPersonId())) {
				listIds.add(listPersonPositionRefBean.get(i).getPosId());
			}
		}
		for (int i = 0; i < listIds.size(); i++) {
			Long posId = listIds.get(i);
			for (int j = 0; j < listJecnStationMatch.size(); j++) {
				JecnStationMatch jecnStationMatch = listJecnStationMatch.get(j);
				String stationMatchIds = jecnStationMatch
						.getStationMatchNames();
				boolean flag = false;
				if (stationMatchIds != null && !"".equals(stationMatchIds)) {
					String[] matchIds = stationMatchIds.split(",");
					for (int k = 0; k < matchIds.length; k++) {
						if (matchIds[k] != null
								&& posId.toString().equals(matchIds[k])) {
							flag = true;
							break;
						}
					}
				}
				if (flag) {
					JecnUserInfo jecnUserInfo = new JecnUserInfo();
					jecnUserInfo.setFigureId(jecnStationMatch.getStationId());
					jecnUserInfo.setPeopleId(peopleId);
					listResult.add(jecnUserInfo);
				}
			}
		}
		return listResult;
	}

	public List<JecnUserInfo> getOldJecnUserInfo(Long peopleId,
			List<JecnUserInfo> listJecnUserInfo) {
		List<JecnUserInfo> listResult = new ArrayList<JecnUserInfo>();
		for (int i = 0; i < listJecnUserInfo.size(); i++) {
			if (peopleId.equals(listJecnUserInfo.get(i).getPeopleId())) {
				listResult.add(listJecnUserInfo.get(i));
			}
		}
		return listResult;
	}

	@Override
	public void synchronizationPeoples(List<JecnUser> listUpdate,
			List<Long> listdelete, List<JecnUser> listNew) throws Exception {
		Session s = this.getSession();
		List<PersonPositionRefBean> listPersonPositionRefBean = s.createQuery(
				"from PersonPositionRefBean").list();
		List<JecnUserInfo> listJecnUserInfo = s
				.createQuery("from JecnUserInfo").list();
		List<JecnStationMatch> listJecnStationMatch = s.createQuery(
				"from JecnStationMatch").list();
		// 获得浏览者权限Id
		List<Long> listLong = s.createQuery(
				"select roleId from JecnRoleInfo where filterId='view'").list();
		Long roleId = null;
		if (listLong.size() > 0) {
			roleId = listLong.get(0);
		}
		if (listdelete.size() > 0) {
			// 删除多余人员
			Query queryDelJecnUser = s
					.createQuery("delete from JecnUser where peopleId in"
							+ JecnCommonSql.getIds(listdelete));
			queryDelJecnUser.executeUpdate();
			s.flush();
			Query queryDelJecnUserInfo = s
					.createQuery("delete from JecnUserInfo where peopleId in"
							+ JecnCommonSql.getIds(listdelete));
			queryDelJecnUserInfo.executeUpdate();
			s.flush();
			Query queryDelUserRole = s
					.createQuery("delete from UserRole where peopleId in"
							+ JecnCommonSql.getIds(listdelete));
			queryDelUserRole.executeUpdate();
			s.flush();
		}
		// 更新人员
		for (int i = 0; i < listUpdate.size(); i++) {
			JecnUser jecnUser = listUpdate.get(i);
			s.update(jecnUser);
			Long id = jecnUser.getId();
			Long peopleId = jecnUser.getPeopleId();
			List<JecnUserInfo> listUpdateJecnUserInfo = this
					.getNewJecnUserInfo(id, peopleId,
							listPersonPositionRefBean, listJecnStationMatch);
			List<JecnUserInfo> listOldJecnUserInfo = this.getOldJecnUserInfo(
					peopleId, listJecnUserInfo);
			List<JecnUserInfo> listBeforeJecnUserInfo = new ArrayList<JecnUserInfo>();
			for (int j = 0; j < listUpdateJecnUserInfo.size(); j++) {
				JecnUserInfo newJecnUserInfo = listUpdateJecnUserInfo.get(j);
				boolean flag = false;
				for (int k = 0; k < listOldJecnUserInfo.size(); k++) {
					JecnUserInfo oldJecnUserInfo = listOldJecnUserInfo.get(k);
					if (newJecnUserInfo.getPeopleId().equals(
							oldJecnUserInfo.getPeopleId())
							&& newJecnUserInfo.getFigureId().equals(
									oldJecnUserInfo.getFigureId())) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					s.save(newJecnUserInfo);
				} else {
					listBeforeJecnUserInfo.add(newJecnUserInfo);
				}
			}
			for (int k = 0; k < listOldJecnUserInfo.size(); k++) {
				JecnUserInfo oldJecnUserInfo = listOldJecnUserInfo.get(k);
				boolean flag = false;
				for (int j = 0; j < listBeforeJecnUserInfo.size(); j++) {
					JecnUserInfo newJecnUserInfo = listBeforeJecnUserInfo
							.get(j);
					if (newJecnUserInfo.getPeopleId().equals(
							oldJecnUserInfo.getPeopleId())
							&& newJecnUserInfo.getFigureId().equals(
									oldJecnUserInfo.getFigureId())) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					s.delete(oldJecnUserInfo);
				}
			}
		}
		// 添加人员
		for (int i = 0; i < listNew.size(); i++) {
			JecnUser jecnUser = listNew.get(i);
			Long id = jecnUser.getId();
			jecnUser.setIsLock(0L);
			jecnUser.setPassword("CCC5C9CAC0CD");
			s.save(jecnUser);
			List<JecnUserInfo> listnewJecnUserInfo = this.getNewJecnUserInfo(
					id, jecnUser.getPeopleId(), listPersonPositionRefBean,
					listJecnStationMatch);
			for (int j = 0; j < listnewJecnUserInfo.size(); j++) {
				s.save(listnewJecnUserInfo.get(j));
			}
			if (roleId != null) {
				UserRole uerRole = new UserRole();
				uerRole.setPeopleId(jecnUser.getPeopleId());
				uerRole.setRoleId(roleId);
				s.save(uerRole);
			}
		}
		s.flush();
	}

	public void matchPosition(String matchIds, boolean isMatch)
			throws Exception {
		if (matchIds != null && !"".equals(matchIds)) {
			String[] posIds = matchIds.split(",");
			for (int i = 0; i < posIds.length; i++) {
				String id = posIds[i];
				PositionBean p = (PositionBean) getSession().get(
						PositionBean.class, new Long(id));
				if (p != null) {
					if (isMatch == true) {
						p.setIsMatch(p.getIsMatch() + 1);
					} else {
						p.setIsMatch(p.getIsMatch() <= 0 ? 0
								: p.getIsMatch() - 1);
					}
					this.getSession().update(p);
				}
			}
		}
		LOGGER.debug("update successful");
	}

	@Override
	public List<Object[]> getUnMatchPosition() {
		String queryString = "select db.deptName,pb.posName from DepartmentBean db, PositionBean pb where pb.isMatch=0 and pb.deptId=db.id";
		return listHql(queryString);
	}

	/**
	 * 数据库为空
	 * 
	 * @param listNewJecnFlowOrg
	 * @param projectId
	 * @throws Exception
	 */
	@Override
	public void saveNoDataOrgs(List<JecnFlowOrg> listNewJecnFlowOrg,
			Long projectId) throws Exception {
		Session s = null;
		// 组织更新
		s = this.getSession();
		for (int i = 0; i < listNewJecnFlowOrg.size(); i++) {
			JecnFlowOrg jecnFlowOrgNew = listNewJecnFlowOrg.get(i);
			if ("0".equals(jecnFlowOrgNew.getPreOrgNumber())) {
				jecnFlowOrgNew.setPreOrgId(0L);
				jecnFlowOrgNew.setProjectId(projectId);
				s.save(jecnFlowOrgNew);
				preUpdateOrg(listNewJecnFlowOrg, jecnFlowOrgNew, projectId, s);
			}
		}
	}

	/**
	 * 组织更新
	 * 
	 * @param listOrgAlls
	 * @param listNewJecnFlowOrg
	 * @param projectId
	 * @throws Exception
	 */
	@Override
	public void updateOrg(List<JecnFlowOrg> listOrgAlls,
			List<JecnFlowOrg> listNewJecnFlowOrg, Long projectId)
			throws Exception {
		Session s = null;
		// 组织更新
		s = this.getSession();
		List<JecnFlowOrg> listUpdateJecnFlowOrg = new ArrayList<JecnFlowOrg>();
		for (int j = 0; j < listOrgAlls.size(); j++) {
			JecnFlowOrg jecnFlowOrg = listOrgAlls.get(j);// 数据库得到的
			String numberOrgId = jecnFlowOrg.getOrgNumber();
			boolean flag = false;
			for (int i = 0; i < listNewJecnFlowOrg.size(); i++) {// 从Excel读取的数据
				JecnFlowOrg jecnFlowOrgNew = listNewJecnFlowOrg.get(i);
				String preOrgNumber = jecnFlowOrgNew.getPreOrgNumber();
				if (numberOrgId != null && !"".equals(numberOrgId)
						&& jecnFlowOrgNew.getOrgNumber().equals(numberOrgId)) {
					jecnFlowOrg.setPreOrgNumber(preOrgNumber);
					// 创建树头
					if ("0".equals(preOrgNumber)) {
						jecnFlowOrg.setPreOrgId(0L);
					} else {
						jecnFlowOrg.setPreOrgId(getOrgId(listOrgAlls,
								jecnFlowOrgNew.getPreOrgNumber()));
					}
					jecnFlowOrg.setProjectId(projectId);
					if (jecnFlowOrgNew.getOrgName() != null
							&& !"".equals(jecnFlowOrgNew.getOrgName())) {
						jecnFlowOrg.setOrgName(jecnFlowOrgNew.getOrgName());
					}
					jecnFlowOrg.setDelState(0L);
					listUpdateJecnFlowOrg.add(jecnFlowOrg);
					flag = true;
					break;
				}
			}
			if (!flag) {
				s.delete(jecnFlowOrg);
			}
		}
		for (int i = 0; i < listNewJecnFlowOrg.size(); i++) {
			JecnFlowOrg jecnFlowOrgNew = listNewJecnFlowOrg.get(i);
			String numberId = jecnFlowOrgNew.getOrgNumber();
			boolean flag = false;
			for (int j = 0; j < listUpdateJecnFlowOrg.size(); j++) {
				JecnFlowOrg jecnFlowOrg = listUpdateJecnFlowOrg.get(j);
				String numberOrgId = jecnFlowOrg.getOrgNumber();
				if (numberOrgId != null && !"".equals(numberOrgId)
						&& numberId.equals(numberOrgId)) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				if ("0".equals(jecnFlowOrgNew.getPreOrgNumber())) {
					jecnFlowOrgNew.setPreOrgId(0L);
				} else {
					jecnFlowOrgNew.setPreOrgId(getOrgId(listOrgAlls,
							jecnFlowOrgNew.getPreOrgNumber()));
				}
				jecnFlowOrgNew.setProjectId(projectId);
				s.save(jecnFlowOrgNew);
				listOrgAlls.add(jecnFlowOrgNew);
				Long orgId = jecnFlowOrgNew.getOrgId();
				for (int j = 0; j < listUpdateJecnFlowOrg.size(); j++) {
					JecnFlowOrg jecnFlowOrg = listUpdateJecnFlowOrg.get(j);
					String numberOrgId = jecnFlowOrg.getPreOrgNumber();
					if (numberId.equals(numberOrgId)) {
						jecnFlowOrg.setPreOrgId(orgId);
						s.update(jecnFlowOrg);
					}
				}
				listUpdateJecnFlowOrg.add(jecnFlowOrgNew);
			}
		}
	}

	/**
	 * 岗位更新
	 * 
	 * @param listOrgAlls
	 * @param getStationsList
	 * @param listJecnUser
	 * @param listJecnUserInfo
	 * @param listNewJecnFlowOrgImage
	 * @param listNewJecnUser
	 * @param listStationAndPeopleRelate
	 * @param projectId
	 * @throws Exception
	 */
	@Override
	public void updateUserFH(List<JecnFlowOrg> listOrgAlls,
			List<JecnFlowOrgImage> getStationsList,
			List<JecnUser> listJecnUser, List<JecnUserInfo> listJecnUserInfo,
			List<JecnFlowOrgImage> listNewJecnFlowOrgImage,
			List<JecnUser> listNewJecnUser,
			List<StationAndPeopleRelate> listStationAndPeopleRelate,
			Long projectId) throws Exception {
		Session s = null;
		s = this.getSession();

		// 岗位更新
		List<JecnFlowOrgImage> listUpdateJecnFlowOrgImage = new ArrayList<JecnFlowOrgImage>();
		List<Long> delLongFigureIdList = new ArrayList<Long>();
		for (int i = 0; i < getStationsList.size(); i++) {// 数据库得到岗位
			JecnFlowOrgImage jecnFlowOrgImage = getStationsList.get(i);
			String numberId = jecnFlowOrgImage.getFigureNumberId();
			boolean flag = false;
			for (int j = 0; j < listNewJecnFlowOrgImage.size(); j++) {
				JecnFlowOrgImage jecnFlowOrgImageNew = listNewJecnFlowOrgImage
						.get(j);
				if (jecnFlowOrgImageNew.getFigureNumberId().equals(numberId)) {
					flag = true;
					if (jecnFlowOrgImageNew.getFigureText() != null
							&& !"".equals(jecnFlowOrgImageNew.getFigureText())) {
						jecnFlowOrgImage.setFigureText(jecnFlowOrgImageNew
								.getFigureText());
					}

					Long orgId = this.getOrgId(listOrgAlls, jecnFlowOrgImageNew
							.getOrgNumberId());
					if (orgId == null) {
						delLongFigureIdList.add(jecnFlowOrgImage.getFigureId());
						s.delete(jecnFlowOrgImage);
						break;
					}

					jecnFlowOrgImage.setOrgNumberId(jecnFlowOrgImageNew
							.getOrgNumberId());
					jecnFlowOrgImage.setOrgId(orgId);
					if (!orgId.equals(jecnFlowOrgImage.getOrgId())) {
						jecnFlowOrgImage.setFigureImageId(String.valueOf(System
								.currentTimeMillis()));
						jecnFlowOrgImage.setStartFigure(null);
						jecnFlowOrgImage.setEndFigure(null);
						jecnFlowOrgImage.setPoLongx(null);
						jecnFlowOrgImage.setPoLongy(null);
						jecnFlowOrgImage.setWidth(null);
						jecnFlowOrgImage.setHeight(null);
						jecnFlowOrgImage.setFontSize(null);// 线的粗细大小
						// 或图形字体的大小
						jecnFlowOrgImage.setSortId(null);
					}
					s.update(jecnFlowOrgImage);
					listUpdateJecnFlowOrgImage.add(jecnFlowOrgImage);
					break;
				}
			}
			if (!flag) {
				s.delete(jecnFlowOrgImage);
				delLongFigureIdList.add(jecnFlowOrgImage.getFigureId());
			}
		}
		if (delLongFigureIdList.size() > 0) {
			s.createQuery(
					"delete from JecnPositionFigInfo where figureId in"
							+ JecnCommonSql.getIds(delLongFigureIdList))
					.executeUpdate();
		}
		for (int j = 0; j < listNewJecnFlowOrgImage.size(); j++) {// 添加崗位
			JecnFlowOrgImage jecnFlowOrgImageNew = listNewJecnFlowOrgImage
					.get(j);
			boolean flag = false;
			for (int i = 0; i < listUpdateJecnFlowOrgImage.size(); i++) {
				JecnFlowOrgImage jecnFlowOrgImage = listUpdateJecnFlowOrgImage
						.get(i);
				if (jecnFlowOrgImage.getFigureNumberId().equals(
						jecnFlowOrgImageNew.getFigureNumberId())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				Long orgId = this.getOrgId(listOrgAlls, jecnFlowOrgImageNew
						.getOrgNumberId());
				if (orgId != null) {// 如果找不到匹配部门退出当前
					jecnFlowOrgImageNew.setOrgNumberId(jecnFlowOrgImageNew
							.getOrgNumberId());
					jecnFlowOrgImageNew.setOrgId(orgId);

					jecnFlowOrgImageNew.setFigureImageId(String.valueOf(System
							.currentTimeMillis()));
					s.save(jecnFlowOrgImageNew);
					JecnPositionFigInfo jecnPositionFigInfo = new JecnPositionFigInfo();
					jecnPositionFigInfo.setFigureId(jecnFlowOrgImageNew
							.getFigureId());
					s.save(jecnPositionFigInfo);
					listUpdateJecnFlowOrgImage.add(jecnFlowOrgImageNew);
				}
			}
		}

		// 人员更新
		List<JecnUser> listUpdateJecnUser = new ArrayList<JecnUser>();
		List<Long> delLongPeopleIdList = new ArrayList<Long>();
		for (int i = 0; i < listJecnUser.size(); i++) {// 数据库获取的人员信息List
			JecnUser jecnUser = listJecnUser.get(i);
			if (jecnUser.getLoginName().equals("admin")) {// 系统管理员默认登录名admin不修改
				continue;
			}
			String numberId = jecnUser.getUserNumber();// 人员编号
			boolean flag = false;
			if (numberId != null && !"".equals(numberId)) {
				for (int j = 0; j < listNewJecnUser.size(); j++) {// 读取导入xml文件获取数据List
					JecnUser jecnUserNew = listNewJecnUser.get(j);
					if (jecnUserNew.getUserNumber().equals(numberId)) {// 根据人员编号获取匹配的人员信息,以读取的配置文件作为标准
						jecnUser.setUserNumber(numberId);
						jecnUser.setTrueName(jecnUserNew.getTrueName());// 用户真实姓名
						jecnUser.setLoginName(jecnUserNew.getLoginName());// 登陆名称
						if (jecnUserNew.getEmail() != null
								&& JecnCommon.checkMailFormat(jecnUserNew
										.getEmail())) {
							jecnUser.setEmail(jecnUserNew.getEmail());// email地址
						}
						// if (jecnUserNew.getPhone() != null&&
						// JecnCommonSql.isNumeric(String.valueOf(jecnUserNew.getPhone())))
						// {
						// jecnUser.setPhone(jecnUserNew.getPhone());
						// }
						// if (jecnUserNew.getPhoneStr() != null
						// && JecnCommon.isNumeric(String
						// .valueOf(jecnUserNew.getPhoneStr()))) {
						jecnUser.setPhoneStr(jecnUserNew.getPhoneStr());// 用户电话号码
						// }

						if (jecnUserNew.getEmailType() != null
								&& !"".equals(jecnUserNew.getEmailType())) {
							jecnUser.setEmailType(jecnUserNew.getEmailType());// 用户Email
							// 地址:inner内网,outer外网
						}
						// jecnUser.setPassword(jecnUserNew.getPassword());
						// jecnUser.setIsLock(jecnUserNew.getIsLock());
						s.update(jecnUser);
						listUpdateJecnUser.add(jecnUser);// 记录需要更新的用户集合
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				s.delete(jecnUser);// 删除配置文件中不存在的用户
				delLongPeopleIdList.add(jecnUser.getPeopleId());// 记录删除的用户ID
			}
		}
		// 读取文件获取的集合List 和 需要更新的用户的集合比较 获取需要添加的用户集合
		for (int j = 0; j < listNewJecnUser.size(); j++) {// 添加用户
			JecnUser jecnUserNew = listNewJecnUser.get(j);
			boolean flag = false;// True:当前用户已更新,false:当前用户需要添加
			for (int i = 0; i < listUpdateJecnUser.size(); i++) {
				JecnUser jecnUser = listUpdateJecnUser.get(i);
				if (jecnUserNew.getUserNumber()
						.equals(jecnUser.getUserNumber())) {
					flag = true;
					break;
				}
			}

			if (!flag) {// 添加用户
				s.save(jecnUserNew);
				// UserRole userRole = new UserRole();
				// userRole.setPeopleId(jecnUserNew.getPeopleId());userRole.setRoleId(2L);
				// s.save(userRole);
				listUpdateJecnUser.add(jecnUserNew);
			}
		}

		// 更新岗位和人员关联表
		List<JecnUserInfo> listUpdate = new ArrayList<JecnUserInfo>();
		for (int i = 0; i < listJecnUserInfo.size(); i++) {
			JecnUserInfo jecnUserInfo = listJecnUserInfo.get(i);
			boolean flag = false;
			for (int j = 0; j < delLongFigureIdList.size(); j++) {
				if (delLongFigureIdList.get(j).equals(
						jecnUserInfo.getFigureId())) {
					s.delete(jecnUserInfo);
					flag = true;
					break;
				}
			}
			if (!flag) {
				listUpdate.add(jecnUserInfo);
			}
		}
		List<JecnUserInfo> listOld = new ArrayList<JecnUserInfo>();
		for (int i = 0; i < listUpdate.size(); i++) {
			JecnUserInfo jecnUserInfo = listUpdate.get(i);
			boolean flag = false;
			for (int j = 0; j < delLongPeopleIdList.size(); j++) {
				if (delLongPeopleIdList.get(j).equals(
						jecnUserInfo.getPeopleId())) {
					s.delete(jecnUserInfo);
					flag = true;
					break;
				}
			}
			if (!flag) {
				listOld.add(jecnUserInfo);
			}
		}
		List<JecnUserInfo> listExist = new ArrayList<JecnUserInfo>();
		for (int i = 0; i < listStationAndPeopleRelate.size(); i++) {
			StationAndPeopleRelate stationAndPeopleRelatem = listStationAndPeopleRelate
					.get(i);
			Long figureId = this.getFigureId(listUpdateJecnFlowOrgImage,
					stationAndPeopleRelatem.getFigureIdNumber());
			Long peopleId = this.getPeopleId(listUpdateJecnUser,
					stationAndPeopleRelatem.getPeopleIdNumber());
			if (peopleId == null || figureId == null) {
				continue;
			}
			boolean flag = false;
			for (int j = 0; j < listOld.size(); j++) {
				JecnUserInfo jecnUserInfo = listOld.get(j);
				if (jecnUserInfo.getPeopleId().equals(peopleId)
						&& figureId.equals(jecnUserInfo.getFigureId())) {
					flag = true;
					listExist.add(jecnUserInfo);
					break;
				}
			}
			if (!flag) {
				JecnUserInfo jecnUserInfo = new JecnUserInfo();
				jecnUserInfo.setFigureId(figureId);
				jecnUserInfo.setPeopleId(peopleId);
				s.save(jecnUserInfo);
				listExist.add(jecnUserInfo);
			}
		}
		for (int i = 0; i < listOld.size(); i++) {
			JecnUserInfo jecnUserInfo = listOld.get(i);
			boolean flag = false;
			for (int j = 0; j < listExist.size(); j++) {
				if (jecnUserInfo.getFigureId().equals(
						listExist.get(j).getFigureId())
						&& jecnUserInfo.getPeopleId().equals(
								listExist.get(j).getPeopleId())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				s.delete(jecnUserInfo);
			}
		}
	}

	public void preUpdateOrg(List<JecnFlowOrg> listNewJecnFlowOrg,
			JecnFlowOrg jecnFlowOrg, Long projectId, Session s)
			throws Exception {
		for (int i = 0; i < listNewJecnFlowOrg.size(); i++) {
			JecnFlowOrg jecnOrg = listNewJecnFlowOrg.get(i);
			if (jecnOrg.getPreOrgNumber().equals((jecnFlowOrg.getOrgNumber()))) {
				jecnOrg.setPreOrgId(jecnFlowOrg.getOrgId());
				jecnOrg.setProjectId(projectId);
				s.save(jecnOrg);
				this.preUpdateOrg(listNewJecnFlowOrg, jecnOrg, projectId, s);
			}
		}
		return;
	}

	private Long getOrgId(List<JecnFlowOrg> listOrgAlls, String orgNumberId) {
		for (int j = 0; j < listOrgAlls.size(); j++) {
			JecnFlowOrg jecnFlowOrg = listOrgAlls.get(j);
			String numberId = jecnFlowOrg.getOrgNumber();
			if (numberId != null && !"".equals(numberId)
					&& orgNumberId.equals(numberId)) {
				return jecnFlowOrg.getOrgId();
			}
		}
		return null;
	}

	private Long getFigureId(List<JecnFlowOrgImage> listAll, String sNumberId) {
		for (int j = 0; j < listAll.size(); j++) {
			JecnFlowOrgImage jecnFlowOrgImage = listAll.get(j);
			String numberId = jecnFlowOrgImage.getFigureNumberId();
			if (numberId != null && !"".equals(numberId)
					&& sNumberId.equals(numberId)) {
				return jecnFlowOrgImage.getFigureId();
			}
		}
		return null;
	}

	private Long getPeopleId(List<JecnUser> listAll, String pNumberId) {
		for (int j = 0; j < listAll.size(); j++) {
			JecnUser jecnUser = listAll.get(j);
			String numberId = jecnUser.getUserNumber();
			if (numberId != null && !"".equals(numberId)
					&& pNumberId.equals(numberId)) {
				return jecnUser.getPeopleId();
			}
		}
		return null;
	}
}
