package epros.draw.gui.figure.shape;

import java.awt.Color;
import java.awt.Cursor;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.ReSizeFigureType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnBaseResizePanel;

/**
 * 
 * 图形八个选中点，其具有改变图形大小、位置点的功能
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFigureResizePanel extends JecnBaseResizePanel {
	/** 选中点方向类型 */
	private ReSizeFigureType reSizeFigureType = null;

	/** 记录鼠标点击时端点的原始位置 */

	private JecnFigureResizePanel(ReSizeFigureType reSizeFigureType) {
		if (reSizeFigureType == null) {

			JecnLogConstants.LOG_JECN_BASE_RESIZE_PANEL
					.error("JecnFigureResizePanel类构造函数：参数为null。reSizeFigureType="
							+ reSizeFigureType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.reSizeFigureType = reSizeFigureType;

		// 设置大小
		this.setSize(6, 6);
		// 设置背景颜色为绿色
		this.setBackground(Color.GREEN);
		// 获取鼠标光标的位图表示形式Cursor
		getMouseCursor(reSizeFigureType);
	}

	/**
	 * 获取鼠标光标的位图表示形式Cursor
	 */
	private void getMouseCursor(ReSizeFigureType reSizeFigureType) {
		switch (reSizeFigureType) {
		case topLeft:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
			break;
		case topCenter:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
			break;
		case topRight: // 右上角
			this.setCursor(Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
			break;
		case rightCenter:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
			break;
		case bottomLRight:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
			break;
		case bottomLCenter:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
			break;
		case bottomLeft:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
			break;
		case leftCenter:
			this.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 创建左上选中点
	 * 
	 * @return
	 */
	public static JecnFigureResizePanel createTopLeftResizePanel() {
		return new JecnFigureResizePanel(ReSizeFigureType.topLeft);
	}

	/**
	 * 
	 * 创建上中选中点
	 * 
	 * @return
	 */
	public static JecnFigureResizePanel createTopCenterResizePanel() {
		return new JecnFigureResizePanel(ReSizeFigureType.topCenter);
	}

	/**
	 * 
	 * 创建上右选中点
	 * 
	 * @return
	 */
	public static JecnFigureResizePanel createTopRightResizePanel() {
		return new JecnFigureResizePanel(ReSizeFigureType.topRight);
	}

	/**
	 * 
	 * 创建左中选中点
	 * 
	 * @return
	 */
	public static JecnFigureResizePanel createLeftCenterResizePanel() {
		return new JecnFigureResizePanel(ReSizeFigureType.leftCenter);
	}

	/**
	 * 
	 * 创建右中选中点
	 * 
	 * @return
	 */
	public static JecnFigureResizePanel createRightCenterResizePanel() {
		return new JecnFigureResizePanel(ReSizeFigureType.rightCenter);
	}

	/**
	 * 
	 * 创建下左选中点
	 * 
	 * @return
	 */
	public static JecnFigureResizePanel createBottomLeftResizePanel() {
		return new JecnFigureResizePanel(ReSizeFigureType.bottomLeft);
	}

	/**
	 * 
	 * 创建下中选中点
	 * 
	 * @return
	 */
	public static JecnFigureResizePanel createBottomLCenterResizePanel() {
		return new JecnFigureResizePanel(ReSizeFigureType.bottomLCenter);
	}

	/**
	 * 
	 * 创建下右选中点
	 * 
	 * @return
	 */
	public static JecnFigureResizePanel createBottomLRightResizePanel() {
		return new JecnFigureResizePanel(ReSizeFigureType.bottomLRight);
	}

	public ReSizeFigureType getReSizeFigureType() {
		return reSizeFigureType;
	}
}
