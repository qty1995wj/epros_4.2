package com.jecn.epros.domain.task;

import com.jecn.epros.domain.file.FileBeanT;
import com.jecn.epros.domain.process.FlowBasicInfoT;
import com.jecn.epros.domain.process.FlowStructureT;
import com.jecn.epros.domain.process.MainFlowT;
import com.jecn.epros.domain.rule.RuleT;

import java.util.List;
import java.util.Set;

/**
 * 提交审批 组装待处理信息
 *
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 14, 2012 时间：3:21:35 PM
 */
public class TempCreateTaskBean implements java.io.Serializable {
    /**
     * 任务主表
     */
    private TaskBean taskBeanNew;
    /**
     * 必填項选择表
     */
    private TaskApplicationNew taskApplicationNew;
    /**
     * 任务试运行文件
     */
    private TaskTestRunFile taskTestRunFile;
    /**
     * 各阶段审批人集合
     */
    private List<TaskApprovePeopleConn> taskApprovePeopleConnList;
    /**
     * 记录上传的文件
     */
    private List<FileBeanT> listFileBeanT;
    /**
     * 制定目标人
     */
    private Set<Long> listToPeople;
    /**
     * 流程属性信息
     */
    private FlowBasicInfoT flowBasicInfoT;
    /**
     * 流程信息
     */
    private FlowStructureT flowStructureT;
    /**
     * 流程地图属性信息
     */
    private MainFlowT mainFlowT;
    /**
     * 制度信息
     */
    private RuleT ruleT;
    /**
     * 查阅权限对象
     */
    private TempAccessBean accessBean;
    /**
     * 任务阶段表集合
     */
    private List<TaskStage> TaskStageList;

    public TaskBean getTaskBeanNew() {
        return taskBeanNew;
    }

    public void setTaskBeanNew(TaskBean taskBeanNew) {
        this.taskBeanNew = taskBeanNew;
    }

    public TaskApplicationNew getTaskApplicationNew() {
        return taskApplicationNew;
    }

    public void setTaskApplicationNew(TaskApplicationNew taskApplicationNew) {
        this.taskApplicationNew = taskApplicationNew;
    }

    public TaskTestRunFile getTaskTestRunFile() {
        return taskTestRunFile;
    }

    public void setTaskTestRunFile(TaskTestRunFile taskTestRunFile) {
        this.taskTestRunFile = taskTestRunFile;
    }

    public List<TaskApprovePeopleConn> getTaskApprovePeopleConnList() {
        return taskApprovePeopleConnList;
    }

    public void setTaskApprovePeopleConnList(List<TaskApprovePeopleConn> taskApprovePeopleConnList) {
        this.taskApprovePeopleConnList = taskApprovePeopleConnList;
    }

    public List<FileBeanT> getListFileBeanT() {
        return listFileBeanT;
    }

    public void setListFileBeanT(List<FileBeanT> listFileBeanT) {
        this.listFileBeanT = listFileBeanT;
    }

    public Set<Long> getListToPeople() {
        return listToPeople;
    }

    public void setListToPeople(Set<Long> listToPeople) {
        this.listToPeople = listToPeople;
    }

    public FlowBasicInfoT getFlowBasicInfoT() {
        return flowBasicInfoT;
    }

    public void setFlowBasicInfoT(FlowBasicInfoT flowBasicInfoT) {
        this.flowBasicInfoT = flowBasicInfoT;
    }

    public FlowStructureT getFlowStructureT() {
        return flowStructureT;
    }

    public void setFlowStructureT(FlowStructureT flowStructureT) {
        this.flowStructureT = flowStructureT;
    }

    public MainFlowT getMainFlowT() {
        return mainFlowT;
    }

    public void setMainFlowT(MainFlowT mainFlowT) {
        this.mainFlowT = mainFlowT;
    }

    public RuleT getRuleT() {
        return ruleT;
    }

    public void setRuleT(RuleT ruleT) {
        this.ruleT = ruleT;
    }

    public TempAccessBean getAccessBean() {
        return accessBean;
    }

    public void setAccessBean(TempAccessBean accessBean) {
        this.accessBean = accessBean;
    }

    public List<TaskStage> getTaskStageList() {
        return TaskStageList;
    }

    public void setTaskStageList(List<TaskStage> taskStageList) {
        TaskStageList = taskStageList;
    }


}
