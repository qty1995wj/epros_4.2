package epros.designer.gui.process;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class ProcessMoveTreeListener extends JecnTreeListener {
	private static Logger log = Logger.getLogger(ProcessMoveTreeListener.class);
	/** 待移动ID集合 */
	private List<Long> listIds = new ArrayList<Long>();

	private JTree jTree;

	public ProcessMoveTreeListener(List<Long> listIds, JTree jTree) {
		this.listIds = listIds;
		this.jTree = jTree;
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = null;
				if (JecnConstants.moveNodeType == TreeNodeType.process) {
					list = JecnDesignerCommon.getDesignerProcessTreeBeanList(node.getJecnTreeBean().getId(),false);
				} else {
					list = ConnectionPool.getProcessAction()
							.getChildProcessMap(node.getJecnTreeBean().getId(),
									JecnConstants.projectId);
				}
				if (list != null) {
					JecnTreeCommon.expansionTreeMoveNode(jTree, list, node,
							listIds);
				}

			} catch (Exception e) {
				log.error("ProcessMoveTreeListener treeExpanded is error", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

}
