package com.jecn.epros.server.service.tree.impl;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.dao.tree.IJecnTreeDao;
import com.jecn.epros.server.service.tree.IJecnTreeService;

@Transactional
public class JecnTreeServiceImpl implements IJecnTreeService {
	private IJecnTreeDao treeDao;

	public IJecnTreeDao getTreeDao() {
		return treeDao;
	}

	public void setTreeDao(IJecnTreeDao treeDao) {
		this.treeDao = treeDao;
	}

	/**
	 * 获取树节点的子节点sort最大值
	 * 
	 * @param treeBean
	 * @return sort+1
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public int getTreeChildMaxSortId(JecnTreeBean treeBean) throws Exception {
		return treeDao.getTreeChildMaxSortId(treeBean);
	}
}
