package com.jecn.epros.server.action.web.system;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnMessageReaded;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.system.IReadMessageService;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

/***
 * 已读消息 
 * @author Administrator
 *
 */
public class ReadedMessageAcion extends BaseAction {
	/** 开始行数 */
	private int start;
	/** 每页显示条数 */
	private int limit;
	/**消息主键ID*/
	private Long messageId;
	/**发送人名称*/
	private String peopleName;
	/**消息主键ID集合*/
	private String mesgIds;
	/**收件人ID*/
	private Long inceptPeopleId;

	public Long getInceptPeopleId() {
		return inceptPeopleId;
	}
	public void setInceptPeopleId(Long inceptPeopleId) {
		this.inceptPeopleId = inceptPeopleId;
	}
	private IReadMessageService readMessageService;
	private JecnMessageReaded jecnMessageReaded;
	
	private static final Logger log = Logger.getLogger(ReadedMessageAcion.class);

	/***
	 * 获取所有的已读消息
	 * @return
	 */
	public String getReadMessageList(){
		try {
			//获取收件人ID
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext()
			.getSession().get("webLoginBean");
			inceptPeopleId = webLoginBean.getJecnUser().getPeopleId();
			//已读数据条数
			int total = readMessageService.getTotalMessageList(inceptPeopleId);
			if(total>0){
				List<JecnMessageReaded> readMessageList = readMessageService.getAllReadMessage(start, limit,inceptPeopleId); 
				JSONArray jsonArray = JSONArray.fromObject(readMessageList);
				outJsonPage(jsonArray.toString(), total);
			}else{
				outJsonPage("[]", total);
			}
			
		} catch (Exception e) {
			log.error("",e);
		}
		return null;
	}
	/***
	 * 根据消息主键ID获取消息详细信息
	 * @return
	 */
	public String searchReadMessageBean(){
		try {
			jecnMessageReaded = readMessageService.searchReadMesgById(messageId);
		} catch (Exception e) {
			log.error("",e);
		}
		jecnMessageReaded.setPeopleName(peopleName);
		return SUCCESS;
	}
	/***
	 * 删除已读消息
	 * @return
	 */
	public String deleteReadMessage(){
		try {
			List<Long> mesgIdList = new ArrayList<Long>();
			String[] splitStr = mesgIds.split(",");
			for(String mesgId : splitStr){
				mesgIdList.add(Long.valueOf(mesgId));
			}
			readMessageService.deleteReadMesgs(mesgIdList);
		} catch (Exception e) {
			log.error("",e);
		}
		
		return null;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public IReadMessageService getReadMessageService() {
		return readMessageService;
	}

	public void setReadMessageService(IReadMessageService readMessageService) {
		this.readMessageService = readMessageService;
	}
	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public JecnMessageReaded getJecnMessageReaded() {
		return jecnMessageReaded;
	}

	public void setJecnMessageReaded(JecnMessageReaded jecnMessageReaded) {
		this.jecnMessageReaded = jecnMessageReaded;
	}
	public String getPeopleName() {
		return peopleName;
	}
	public void setPeopleName(String peopleName) {
		this.peopleName = peopleName;
	}
	public String getMesgIds() {
		return mesgIds;
	}
	public void setMesgIds(String mesgIds) {
		this.mesgIds = mesgIds;
	}
	

	
}
