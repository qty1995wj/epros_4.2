package com.jecn.epros.server.service.integration;

import java.util.List;

import com.jecn.epros.server.bean.integration.JecnControlTarget;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.integration.JecnRuleRiskBeanT;
import com.jecn.epros.server.bean.integration.JecnRuleStandardBeanT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.integration.RiskDetailBean;
import com.jecn.epros.server.webBean.integration.RiskList;
import com.jecn.epros.server.webBean.integration.RiskRelatedProcessBean;
import com.jecn.epros.server.webBean.integration.RiskRelatedRuleBean;
import com.jecn.epros.server.webBean.integration.RiskSystemSearchBean;
import com.jecn.epros.server.webBean.integration.RiskWebBean;

/***
 * 风险表操作类 2013-10-30
 * 
 */
public interface IJecnRiskService extends IBaseService<JecnRisk, Long> {

	/**
	 * 风险相关流程
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 * @author ZHF
	 */
	public List<RiskRelatedProcessBean> getRiskRelatedProcessBeanById(Long id) throws Exception;

	/**
	 * 风险相关制度
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 * @author ZHF
	 */
	public List<RiskRelatedRuleBean> getRiskRelatedRuleById(Long id) throws Exception;

	/***
	 * 新建风险点或风险目录
	 * 
	 * @param jecnRisk
	 * @return
	 * @throws Exception
	 */
	public Long addJecnRiskOrDir(JecnRisk jecnRisk) throws Exception;

	/**
	 * 重命名
	 * 
	 * @param newName
	 *            新名称
	 * @param riskId
	 *            风险ID
	 * @param updatePersonId
	 *            更新人Id
	 * @throws Exception
	 */
	public void reRiskName(String newName, Long riskId, Long updatePersonId) throws Exception;

	/**
	 * 获取风险目录下子节点
	 * 
	 * @param pid
	 *            父ID
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildsRisk(Long pid, Long projectId) throws Exception;

	/***
	 * 删除风险
	 * 
	 * @param riskIds
	 *            风险主键ID集合
	 * @return
	 * @throws Exception
	 */
	public int deleteRisk(List<Long> riskIds,Long peopleId) throws Exception;

	/***
	 * 节点排序
	 * 
	 * @param list
	 * @param pId
	 * @throws Exception
	 */
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception;

	/***
	 * 节点移动
	 * 
	 * @param listIds
	 * @param pId
	 * @throws Exception
	 */
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception;

	/**
	 * 获取所有的风险
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getAllRisk() throws Exception;

	/***
	 * 获取风险节点数据
	 * 
	 * @param fileId
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getPnodes(Long id) throws Exception;

	/***
	 * 根据主键ID获取风险基本信息，对应控制目标数据
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public JecnRisk getJecnRiskById(Long id) throws Exception;

	/***
	 * 风险点属性更新
	 * 
	 * @param jecnRisk
	 * @return
	 * @throws Exception
	 */
	public Long EditJecnRiskProperty(JecnRisk jecnRisk) throws Exception;

	/**
	 * 获取风险目录
	 * 
	 * @param pid
	 *            父ID
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChildsRiskDirs(Long pid, Long projectId) throws Exception;

	/**
	 * 添加控制目标
	 * 
	 * @param jecnControlTarget
	 * @return
	 * @throws Exception
	 */
	public void addJecnControlTarget(List<JecnControlTarget> jecnControlTargetList) throws Exception;

	/**
	 * 根据风险点描述内容查询相关风险数据
	 * 
	 * @param name
	 *            风险点描述内容
	 * @throws Exception
	 */
	public List<JecnTreeBean> searchRiskByName(String name) throws Exception;

	/***
	 * 获取所有的控制目标数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JecnControlTarget> getAllControlTargetByRiskId(Long risk) throws Exception;

	/***
	 * 根据风险主键ID获取内控指引知识库数据
	 * 
	 * @param riskId
	 *            风险主键ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnTreeBean> getJecnRiskGuide(Long riskId) throws Exception;

	/**
	 * 查询风险总数
	 * 
	 * @param riskSystemSearchBean
	 * @return
	 * @throws Exception
	 */
	public int getRiskTotal(RiskSystemSearchBean riskSystemSearchBean, Long projectId) throws Exception;

	/**
	 * 探索风险列表
	 * 
	 * @param riskSystemSearchBean
	 * @return
	 * @throws Exception
	 */
	public List<RiskWebBean> getRiskList(RiskSystemSearchBean riskSystemSearchBean, int start, int limit, Long projectId)
			throws Exception;

	/**
	 * 验证风险编号在数据库中是否存在
	 * 
	 * @param riskNum
	 * @param riskId
	 * @return
	 * @throws Exception
	 */
	public int riskNumIsRepeat(String riskNum, Long riskId) throws Exception;

	/**
	 * 根据制度ID获取制度关联风险信息
	 * 
	 * @param ruleId
	 *            制度主键ID
	 * @return List<JecnRuleRiskBeanT>制度相关风险点集合
	 */

	List<JecnTreeBean> findJecnRuleRiskBeanTByRuleId(Long ruleId) throws Exception;

	/**
	 * 制度相关标准
	 * 
	 * @param ruleId
	 * @return List<JecnRuleStandardBeanT>制度相关标准集合
	 * @throws Exception
	 */
	List<JecnTreeBean> findJecnRuleStandardBeanTByRuleId(Long ruleId) throws Exception;

	/**
	 * 根据风险主键ID查询风险基本信息
	 * 
	 * @param id
	 *            风险主键ID
	 * @return
	 * @throws Exception
	 */
	public RiskDetailBean getJecnRiskByIdForWeb(Long id) throws Exception;

	/**
	 * 根据风险目录ID查询风险总数
	 * 
	 * @param pid
	 * @return
	 * @throws Exception
	 */
	public int getRiskTotalByPid(Long pid) throws Exception;

	/**
	 * 根据风险目录ID查询所有风险信息分布方法
	 * 
	 * @param pid
	 *            当前父ID
	 * @param start
	 *            开始行数
	 * @param limit
	 *            每页显示条数
	 * @return
	 * @throws Exception
	 */
	public List<RiskWebBean> getRiskListByPid(Long pid, int start, int limit) throws Exception;

	/**
	 * 根据风险目录ID获取当前目录下所有风险控制清单
	 * 
	 * @author fuzhh 2013-12-16
	 * @param id
	 * @param projectId
	 * @param isShowAll
	 * @return
	 * @throws Exception
	 */
	public RiskList getRiskInventoryALL(Long id, Long projectId, int level, boolean isShowAll) throws Exception;

	/**
	 * 风险清单 节点处在的级别
	 * 
	 * @author fuzhh 2013-12-13
	 * @param id
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public int findRiskListLevel(Long id) throws Exception;

	/**
	 * 选择风险框中获取风险目录下子节点
	 * 
	 * @param pid
	 *            父ID
	 * @throws Exception
	 */
	public List<JecnTreeBean> getChooseDialogChildsRisk(Long pid, Long projectId) throws Exception;

	public List<JecnTreeBean> getRoleAuthChildsRisk(Long pid, Long projectId, Long peopleId) throws Exception;

	public List<JecnTreeBean> searchRoleAuthByName(String name, Long peopleId) throws Exception;

}
