package epros.designer.gui.popedom.person;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.UserPageBean;
import com.jecn.epros.server.bean.popedom.UserSearchBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSearchPopup;
import epros.designer.gui.popedom.role.JecnRoleCommon;
import epros.designer.gui.popedom.role.choose.RoleManageOrgChooseDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.swing.textfield.search.JecnSearchDocumentListener;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 人员管理对话框
 * 
 * @author Administrator
 * 
 */
public class PersonManageDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(PersonManageDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 搜索面板 */
	private JPanel searchPanel = null;
	/** 搜索按钮面板 */
	private JPanel searchBtnPanel = null;
	private JPanel radBtnPanel = null;
	private JPanel sernPanel = null;

	/** 结果面板 */
	private JPanel resultPanel = null;
	/** 结果滚动面板 */
	private JScrollPane resultScrollPanel = null;
	/** 表 */
	private JTable personTable = null;
	/** 按钮面板 */
	private JPanel btnPanel = null;

	/** 部门名称Label */
	private JLabel deptNameLabel = new JLabel(JecnProperties.getValue("deptNameC"));
	/** 部门名称Field */
	private JSearchTextField deptNameField = new JSearchTextField();
	/** 部门ID */
	private Long deptId = -1L;
	/** 部门查询是否删除已选择的deptId */
	private boolean deptFlag = false;
	/** 角色类型Label */
	private JLabel roleTypeLabel = new JLabel(JecnProperties.getValue("roleTypeC"));
	/** 角色类型ComboBox */
	private JComboBox roleTypeBox = new JComboBox();
	private String roleType = null;
	/** 真实名称Label */
	private JLabel realNameLabel = new JLabel(JecnProperties.getValue("realNameC"));
	/** 真实名称Field */
	private JTextField realNameField = new JTextField();
	/** 登录名称Label */
	private JLabel loginNameLabel = new JLabel(JecnProperties.getValue("loginNameC"));
	/** 登录名称Field */
	private JTextField loginNameField = new JTextField();
	/** 部门搜索弹出popup */
	private JecnSearchPopup deptSearchPopup = null;

	/** 部门选择按钮 */
	private JButton orgSelectBtn = new JButton(JecnProperties.getValue("selectBtn"));
	/** 搜索按钮 */
	private JButton searchBtn = new JButton(JecnProperties.getValue("search"));
	/** 清空搜索条件 */
	private JButton emptySearchBtn = new JButton(JecnProperties.getValue("emptyBtn"));
	private JLabel jLablePrompt = new JLabel();;
	/** 取消登录 */
	private JButton cancelLoadBtn = new JButton(JecnProperties.getValue("cancelLoadBtn"));
	/** 增加用户 */
	private JButton addBtn = new JButton(JecnProperties.getValue("addBtn"));
	/** 编辑用户 */
	private JButton editBtn = new JButton(JecnProperties.getValue("editBtn"));
	/** 删除用户 */
	private JButton deleteBtn = new JButton(JecnProperties.getValue("deleteBtn"));
	/** 关闭 */
	private JButton closeBtn = new JButton(JecnProperties.getValue("closeBtn"));

	/** 分页面板 */
	private JPanel pagePanel = new JPanel();
	private JLabel nowPage = new JLabel(" ");
	/** 上一页 */
	private JLabel previousPage = new JLabel(new ImageIcon("images/icons/previouspage.gif"));
	/** 下一页 */
	private JLabel nextPage = new JLabel(new ImageIcon("images/icons/nextpage.gif"));
	/** 首页 */
	private JLabel homePage = new JLabel(JecnProperties.getValue("homePage"));
	/** 尾页 */
	private JLabel endPage = new JLabel(JecnProperties.getValue("endPage"));
	private String operationType = null;

	/** 总页数 */
	private JLabel totalPages = new JLabel();

	/** 部门，只放一个对象 */
	private List<JecnTreeBean> listDept = new ArrayList<JecnTreeBean>();
	/** 搜索出的数据集合 */
	private List<JecnTreeBean> popupTableList = null;
	/** 人员搜索 正常按钮 */
	private JRadioButton normalBtn = new JRadioButton(JecnProperties.getValue("normal"));
	/** 人员搜索 已删除按钮 */
	private JRadioButton hasDeletedBtn = new JRadioButton(JecnProperties.getValue("hasDeleted"));
	/** 人员搜索 已登录按钮 */
	private JRadioButton hasLogintn = new JRadioButton(JecnProperties.getValue("loggedIn"));

	/** 恢复 已删除人员 */
	private JButton restorationBtn = new JButton(JecnProperties.getValue("recyBtn"));

	/** 是否已删除 */
	private Long isDelete = 0L;
	/** 是否登录 **/
	private boolean isLogin = false;

	/** 角色类型顺序及类别 */
	private Map<Integer, String> roleTypeMaps = new HashMap<Integer, String>();

	public PersonManageDialog() {
		initCompotents();
		initLayout();
		buttonActionInit();
		searchActionInit();
		searchPerson();
	}

	private void initCompotents() {

		this.setTitle(JecnProperties.getValue("personManage"));
		this.setSize(580, 580);
		this.setModal(true);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);
		deptNameField.setColumns(20);
		mainPanel = new JPanel();
		// 搜索面板
		searchPanel = new JPanel();
		// 结果面板
		resultPanel = new JPanel();

		// 搜索按钮面板
		searchBtnPanel = new JPanel();
		radBtnPanel = new JPanel();
		sernPanel = new JPanel();
		// table 滚动面板
		resultScrollPanel = new JScrollPane();
		// 表
		personTable = new PersonManageTable(this);
		// 按钮面板
		btnPanel = new JPanel();
		// 部门搜索弹出popup
		deptSearchPopup = new JecnSearchPopup(deptNameField);

		Dimension d = new Dimension(200, 505);
		d = new Dimension(70, 22);
		searchBtn.setPreferredSize(d);
		searchBtn.setMinimumSize(d);
		emptySearchBtn.setPreferredSize(d);
		emptySearchBtn.setMinimumSize(d);

		// 主面板布局
		mainPanel.setLayout(new GridBagLayout());
		// 搜索条件布局
		searchPanel.setLayout(new GridBagLayout());
		searchBtnPanel.setLayout(new GridBagLayout());
		radBtnPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		sernPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		resultPanel.setLayout(new BorderLayout());
		pagePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		searchPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("search")));

		initRoleItem();

		resultPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("result")));

		// 隐藏第1列和第3列
		deptSearchPopup.hiddenColumn(0, 2);

		// 设置各面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		searchPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		searchBtnPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		pagePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		btnPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		normalBtn.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		hasDeletedBtn.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		hasLogintn.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		radBtnPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		sernPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		homePage.setForeground(Color.BLUE);
		endPage.setForeground(Color.BLUE);
		jLablePrompt.setForeground(Color.red);

		resultScrollPanel.setViewportView(personTable);
		/** 正常和已删除 按钮实现 单选效果 */
		ButtonGroup bgp = new ButtonGroup();
		bgp.add(normalBtn);
		bgp.add(hasDeletedBtn);
		bgp.add(hasLogintn);
		// 默认正常按钮选中
		normalBtn.setSelected(true);
		restorationBtn.setEnabled(false);
	}

	/**
	 * @author yxw 2012-5-21
	 * @description:为按钮增加事件
	 */
	private void initLayout() {
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);

		this.getContentPane().add(this.mainPanel);

		// ************第一层布局************//
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(searchPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(resultPanel, c);
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(btnPanel, c);
		// ************第一层布局************//
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		searchPanel.add(loginNameLabel, c);

		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		searchPanel.add(loginNameField, c);

		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		searchPanel.add(realNameLabel, c);

		c = new GridBagConstraints(3, 0, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		searchPanel.add(realNameField, c);
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		searchPanel.add(roleTypeLabel, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		searchPanel.add(roleTypeBox, c);
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		searchPanel.add(deptNameLabel, c);
		c = new GridBagConstraints(3, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		searchPanel.add(deptNameField, c);
		c = new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		searchPanel.add(orgSelectBtn, c);

		c = new GridBagConstraints(0, 2, 5, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		searchPanel.add(searchBtnPanel, c);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		searchBtnPanel.add(radBtnPanel, c);
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0);
		radBtnPanel.add(normalBtn, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0);
		radBtnPanel.add(hasDeletedBtn, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0);
		radBtnPanel.add(hasLogintn, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0);
		searchBtnPanel.add(sernPanel, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0);
		sernPanel.add(searchBtn, c);
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0);
		sernPanel.add(emptySearchBtn, c);

		// searchBtnPanel.add(normalBtn);
		// searchBtnPanel.add(hasDeletedBtn);
		// searchBtnPanel.add(searchBtn);
		// searchBtnPanel.add(emptySearchBtn);

		// ************结果面板布局************//
		// 结果滚动面板
		resultPanel.add(resultScrollPanel, BorderLayout.CENTER);
		resultPanel.add(pagePanel, BorderLayout.SOUTH);
		// ************结果面板布局************//

		pagePanel.add(new JLabel(JecnProperties.getValue("regulation")));// 第
		pagePanel.add(nowPage);

		pagePanel.add(nowPage);

		pagePanel.add(new JLabel(JecnProperties.getValue("page")));// 页
		// 上一页
		pagePanel.add(previousPage);
		// 下一页
		pagePanel.add(nextPage);
		// 首页
		pagePanel.add(homePage);
		// 尾页
		pagePanel.add(endPage);
		JPanel p = new JPanel();
		p.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension = new Dimension(120, 25);
		p.setPreferredSize(dimension);
		p.setMinimumSize(dimension);
		pagePanel.add(p);
		// 总页数：
		pagePanel.add(new JLabel(JecnProperties.getValue("totalPages")));
		pagePanel.add(totalPages);
		pagePanel.add(new JLabel("  "));
		// 按钮面板
		btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		btnPanel.add(jLablePrompt);
		btnPanel.add(restorationBtn);
		btnPanel.add(cancelLoadBtn);
		if (!JecnDesignerCommon.isSecondAdmin()) {
			btnPanel.add(addBtn);
			btnPanel.add(deleteBtn);
		}
		btnPanel.add(editBtn);
		btnPanel.add(closeBtn);
	}

	// 和查询有关的事件
	private void searchActionInit() {
		roleTypeBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					int i = roleTypeBox.getSelectedIndex();
					roleType = roleTypeMaps.get(i);
				}
			}
		});

		// 上一页
		previousPage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// 如果为初始页或第一天时，点上一而直接返回，不再去请求后台
				if (DrawCommon.isNullOrEmtryTrim(nowPage.getText()) || nowPage.getText().equals("1")) {
					return;
				}
				operationType = "previous";
				searchPerson();
			}

		});
		// 下一页
		nextPage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				operationType = "next";
				searchPerson();
			}

		});
		// 首页
		homePage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				operationType = "home";
				searchPerson();
			}

		});
		// 尾页
		endPage.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				operationType = "end";
				searchPerson();
			}

		});
	}

	/**
	 * @author yxw 2012-5-23
	 * @description:为按钮初始化事件
	 */
	private void buttonActionInit() {
		deptNameField.addDocumentListener(new JecnSearchDocumentListener(deptNameField) {
			@Override
			public void removeUpdateJecn(DocumentEvent e) {
				deptNameFieldSearch();
			}

			@Override
			public void insertUpdateJecn(DocumentEvent e) {
				deptNameFieldSearch();
			}

			@Override
			public void keyEnterReleased(KeyEvent e) {
				deptSearchPopup.searchTableMousePressed();
			}
		});

		deptSearchPopup.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				if (deptSearchPopup.getSelectTreeBean() != null) {
					deptId = deptSearchPopup.getSelectTreeBean().getId();
					listDept.clear();
					listDept.add(deptSearchPopup.getSelectTreeBean());
					deptSearchPopup.setSelectTreeBean(null);
				}
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {

			}
		});
		orgSelectBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 管理员有权限的部门开始值
				if (JecnDesignerCommon.isSecondAdmin()) {
					RoleManageOrgChooseDialog orgChooseDialog = new RoleManageOrgChooseDialog(listDept);
					orgChooseDialog.setSelectMutil(false);
					orgChooseDialog.setVisible(true);
					if (orgChooseDialog.isOperation()) {
						if (listDept.size() > 0) {
							JecnTreeBean jecnTreeBean = listDept.get(0);
							deptNameField.setTextJecn(jecnTreeBean.getName());
							deptId = jecnTreeBean.getId();
						} else {
							deptNameField.setTextJecn("");
							deptId = -1L;
						}
					}
				} else {
					deptId = JecnDesignerCommon.setResponsibleDept(listDept, deptNameField, PersonManageDialog.this,
							deptId);
				}

			}
		});
		searchBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				operationType = "home";
				searchPerson();
			}
		});
		// 清空搜索条件
		emptySearchBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				emptySearchBtnAction();
			}
		});
		// 取消登录
		cancelLoadBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelLoadBtnAction();
			}
		});
		// 添加
		addBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addBtnAction();

			}
		});
		// 编辑
		editBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				editBtnAction();
			}
		});
		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteBtnAction();
			}
		});
		// 关闭窗口
		closeBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				closeBtnAction();
			}
		});
		// 正常
		normalBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isLogin = false;
				isDelete = 0L;
				restorationBtn.setEnabled(false);
				addBtn.setEnabled(true);
				editBtn.setEnabled(true);
				deleteBtn.setEnabled(true);
				operationType = "home";
				searchPerson();
			}
		});
		// 已删除
		hasDeletedBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isLogin = false;
				isDelete = 1L;
				restorationBtn.setEnabled(true);
				addBtn.setEnabled(false);
				editBtn.setEnabled(false);
				deleteBtn.setEnabled(false);
				operationType = "home";
				searchPerson();
			}
		});
		// 已登录
		hasLogintn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				isLogin = true;
				isDelete = 0L;
				restorationBtn.setEnabled(false);
				addBtn.setEnabled(true);
				editBtn.setEnabled(true);
				deleteBtn.setEnabled(true);
				operationType = "home";
				searchPerson();
			}
		});
		// 恢复
		restorationBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				restorationBtnAction();
			}
		});
	}

	/**
	 * 
	 * 部门搜索
	 * 
	 */
	private void deptNameFieldSearch() {
		if (DrawCommon.isNullOrEmtryTrim(deptNameField.getText()) || JecnConstants.projectId == null) {
			deptId = -1L;
			return;
		}
		try {
			// 部门名称
			String name = deptNameField.getText().trim();
			if (deptSearchPopup.isCanSearch()) {
				if (JecnDesignerCommon.isAdmin()) {
					popupTableList = ConnectionPool.getOrganizationAction().searchByName(name, JecnConstants.projectId);
				} else {
					popupTableList = ConnectionPool.getOrganizationAction().searchRoleAuthByName(name,
							JecnConstants.projectId, JecnDesignerCommon.getSecondAdminUserId());
				}
				deptSearchPopup.setTableData(popupTableList);
			}
			deptSearchPopup.setCanSearch(true);

		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			log.error("PersonManageDialog deptNameFieldSearch is error", e);
		}

	}

	// 清空搜索条件
	private void emptySearchBtnAction() {
		this.deptNameField.setText("");
		this.deptId = -1L;
		this.roleTypeBox.setSelectedIndex(0);
		this.realNameField.setText("");
		this.loginNameField.setText("");

	}

	// 取消登录
	private void cancelLoadBtnAction() {
		jLablePrompt.setText("");
		int[] rows = personTable.getSelectedRows();
		if (rows.length != 1) {
			// JecnOptionPane.showMessageDialog(this, JecnProperties
			// .getValue("chooseOneRow"));
			jLablePrompt.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		} else {
			if (!JecnProperties.getValue("isLab").toString().equals(personTable.getValueAt(rows[0], 4).toString())) {// !(Boolean)
				// personTable.getValueAt(rows[0],
				// 4)
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("userNoLogin"));
				return;
			}
			Long id = (Long) personTable.getValueAt(rows[0], 0);
			try {
				ConnectionPool.getPersonAction().dischargeLogin(id);
				personTable.setValueAt(JecnProperties.getValue("notLab"), rows[0], 4);
				jLablePrompt.setText("");
			} catch (Exception e) {
				log.error("PersonManageDialog cancelLoadBtnAction is error！", e);
			}

		}
	}

	// 添加
	private void addBtnAction() {
		try {
			int serverCount = ConnectionPool.getPersonAction().addPeopleValidate();
			if (serverCount != -1) {
				jLablePrompt.setText(JecnProperties.getValue("userMaximumNumber"));
				return;
			}
		} catch (Exception e) {
			log.error("", e);
		}
		jLablePrompt.setText("");
		AddPersonDialog adddialog = new AddPersonDialog(this);
		adddialog.setVisible(true);
	}

	// 编辑
	public void editBtnAction() {
		jLablePrompt.setText("");
		int[] rows = personTable.getSelectedRows();
		if (rows.length != 1) {
			// JecnOptionPane.showMessageDialog(this, JecnProperties
			// .getValue("chooseOneRow"));
			jLablePrompt.setText(JecnProperties.getValue("chooseOneRow"));
		} else {
			EditPersonDialog editDialog = new EditPersonDialog(this, rows[0]);
			editDialog.setVisible(true);
			jLablePrompt.setText("");
		}

	}

	// 删除
	private void deleteBtnAction() {
		jLablePrompt.setText("");
		int[] selectRows = personTable.getSelectedRows();
		if (selectRows.length == 0) {
			jLablePrompt.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		} else {
			// 判断要删除的集合中是否存在已经登录的用户,如果存在不能够执行删除
			for (int i = 0; i <= selectRows.length - 1; i++) {
				String loginState = (String) personTable.getValueAt(selectRows[i], 4);
				// 如果是登录状态，不能够执行删除
				if (loginState != null && !"".equals(loginState) && JecnProperties.getValue("isLab").equals(loginState)) {
					JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("notDelLoginPerson"));
					return;
				}
			}

			// 提示是否删除
			int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
					JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return;
			}
			// 保存要删除的id，传到后台删除数据
			List<Long> listIds = new ArrayList<Long>();
			for (int i = selectRows.length - 1; i >= 0; i--) {
				Long userId = (Long) personTable.getValueAt(selectRows[i], 0);
				if (userId == JecnConstants.getUserId()) {
					JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("canNotDelMyself"));
					return;
				}
				listIds.add(userId);

			}
			try {
				// 是否删除成功 0正确删除 1不能删除所有管理员 至少留有一个
				if (ConnectionPool.getPersonAction().delUsers(listIds) == 0) {
					for (int i = selectRows.length - 1; i >= 0; i--) {
						((DefaultTableModel) personTable.getModel()).removeRow(selectRows[i]);

					}
					jLablePrompt.setText("");
				} else {
					jLablePrompt.setText(JecnProperties.getValue("atLeastAdmin"));
				}

			} catch (Exception e) {
				log.error("PersonManageDialog deleteBtnAction is error！", e);
			}

		}
	}

	// 关闭
	private void closeBtnAction() {
		this.dispose();
	}

	/***
	 * 恢复
	 * 
	 */
	private void restorationBtnAction() {
		jLablePrompt.setText("");
		int[] selectRows = personTable.getSelectedRows();
		if (selectRows.length == 0) {
			jLablePrompt.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		} else {
			// 提示是否删除
			int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("ynRestoration"), null,
					JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return;
			}
			// 保存要删除的id，传到后台删除数据
			List<Long> listIds = new ArrayList<Long>();
			for (int i = selectRows.length - 1; i >= 0; i--) {
				listIds.add((Long) personTable.getValueAt(selectRows[i], 0));

			}
			try {
				ConnectionPool.getPersonAction().restorationUsers(listIds);
				for (int i = selectRows.length - 1; i >= 0; i--) {
					((DefaultTableModel) personTable.getModel()).removeRow(selectRows[i]);

				}
				jLablePrompt.setText("");
			} catch (Exception e) {
				log.error("PersonManageDialog restorationBtnAction is error！", e);
			}
		}
	}

	private void searchPerson() {
		try {
			if (DrawCommon.isNullOrEmtryTrim(this.deptNameField.getText())) {
				deptId = -1L;
			}
			if (JecnDesignerCommon.isSecondAdmin() && deptId.longValue() == -1) {
				deptId = JecnDesignerCommon.getSecondAdminOrgId();
			}
			UserPageBean pageBean = ConnectionPool.getPersonAction().searchUser(deptId, roleType,
					this.loginNameField.getText(), this.realNameField.getText(),
					DrawCommon.isNullOrEmtryTrim(nowPage.getText()) ? 0 : Integer.parseInt(nowPage.getText()),
					operationType, 20, JecnConstants.projectId, isDelete,isLogin);
			List<UserSearchBean> list = pageBean.getListUserSearchBean();
			if (list != null && list.size() > 0) {
				nowPage.setText(String.valueOf(pageBean.getCurPage()));
				totalPages.setText(String.valueOf(pageBean.getTotalPage()));
				// 清空
				for (int index = personTable.getModel().getRowCount() - 1; index >= 0; index--) {
					((DefaultTableModel) personTable.getModel()).removeRow(index);
				}
				Vector<Object> v = null;

				for (UserSearchBean u : list) {
					v = new Vector<Object>();
					v.add(u.getPeopleId());
					v.add(u.getLoginName());
					v.add(u.getTrueName());
					v.add(u.getEmail());
					v.add(u.isLogin() ? JecnProperties.getValue("isLab") : JecnProperties.getValue("notLab"));
					((DefaultTableModel) personTable.getModel()).addRow(v);
				}
			} else {
				// 清空
				for (int index = personTable.getModel().getRowCount() - 1; index >= 0; index--) {
					((DefaultTableModel) personTable.getModel()).removeRow(index);
				}
			}
		} catch (Exception e) {
			log.error("PersonManageDialog searchPerson is error！", e);
		}
		// }

	}

	/**
	 * 初始化角色类型
	 * 
	 * @throws Exception
	 */
	private void initRoleItem() {
		List<JecnTreeBean> list = null;
		try {
			list = JecnRoleCommon.getDefaultRoleTreeBeans();
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, e.getMessage());
			return;
		}
		roleTypeBox.addItem(JecnProperties.getValue("all"));
		Map<RoleTypeBean, String> roleNameMaps = new HashMap<RoleTypeBean, String>();
		for (JecnTreeBean jecnTreeBean : list) {
			roleNameMaps.put(RoleTypeBean.valueOf(jecnTreeBean.getNumberId()), jecnTreeBean.getName());
		}
		int count = 0;
		if (roleNameMaps.containsKey(RoleTypeBean.admin)) {
			// 系统管理员
			roleTypeBox.addItem(JecnProperties.getValue("admin"));
			count++;
			roleTypeMaps.put(count, RoleTypeBean.admin.toString());
		}

		if (JecnDesignerCommon.isAdmin() && JecnDesignerCommon.isShowSecondAdmin()) {// 系统管理员并且启用二级管理员
			roleTypeBox.addItem(JecnProperties.getValue("twoAdmin"));
			count++;
			roleTypeMaps.put(count, RoleTypeBean.secondAdmin.toString());
		}

		// 设计管理员
		roleTypeBox.addItem(JecnProperties.getValue("design"));
		count++;
		roleTypeMaps.put(count, RoleTypeBean.design.toString());

		if (JecnConstants.isPubShow() && roleNameMaps.containsKey(RoleTypeBean.viewAdmin)) {
			// 浏览管理员
			roleTypeBox.addItem(JecnProperties.getValue("viewAdmin"));
			count++;
			roleTypeMaps.put(count, RoleTypeBean.viewAdmin.toString());
		}
		// 文件管理员
		roleTypeBox.addItem(JecnProperties.getValue("isDesignFileAdmin"));
		count++;
		roleTypeMaps.put(count, RoleTypeBean.isDesignFileAdmin.toString());

		// 流程管理员
		roleTypeBox.addItem(JecnProperties.getValue("isDesignProcessAdmin"));
		count++;
		roleTypeMaps.put(count, RoleTypeBean.isDesignProcessAdmin.toString());

		// 是否存在浏览端，如果不存在则隐藏：制度管理员
		if (JecnConstants.isEpros() && roleNameMaps.containsKey(RoleTypeBean.isDesignRuleAdmin)) {
			// 制度管理员
			roleTypeBox.addItem(JecnProperties.getValue("isDesignRuleAdmin"));
			count++;
			roleTypeMaps.put(count, RoleTypeBean.isDesignRuleAdmin.toString());
		}
	}

	enum RoleTypeBean {
		admin, design, viewAdmin, isDesignFileAdmin, isDesignProcessAdmin, isDesignRuleAdmin, secondAdmin, all
	}

	public JTable getPersonTable() {
		return personTable;
	}
}
