package epros.tray;

import jecntool.cmd.JecnCmd;
import jecntool.cmd.ResultBean;

/**
 * 
 * 停止EPROS-D2
 * 
 * @author zhouxy
 * 
 */
class JecnStopD2 {

	void stopEprosD2() {
		// 停止tomcat
		ResultBean retBean = JecnCmd
				.cmdExec(JecnTrayConstants.CMD_NET_STOP_EPROSSERVER3);
		// 停止mysql
		retBean = JecnCmd.cmdExec(JecnTrayConstants.CMD_NET_STOP_MYSQL5);
	}
}
