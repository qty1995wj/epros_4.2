package com.jecn.epros.domain.journal;

import java.util.List;

public class LogResourceVisitsView {
    private int total;
    private List<ResourceVisitsResult> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ResourceVisitsResult> getData() {
        return data;
    }

    public void setData(
            List<ResourceVisitsResult> data) {
        this.data = data;
    }
}
