package com.jecn.epros.util;

import com.jecn.epros.domain.report.ActivityStatistic;
import com.jecn.epros.domain.report.InformationStatistic;
import com.jecn.epros.domain.report.LineChart;
import com.jecn.epros.domain.report.LineChart.ChartResource;
import com.jecn.epros.domain.report.Part;
import com.jecn.epros.domain.report.RoleStatistic;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thinkpad on 2017/1/12.
 */
public class ReportUtils {

	public static LineChart pie2RoleLineChart(List<RoleStatistic> reportBaseBeanList) {
		List<String> roleRanges = new ArrayList(Part.roleRange.values());
		LineChart lineChart = initLineChart(roleRanges);
		for (RoleStatistic bean : reportBaseBeanList) {
			initResource(lineChart.getResources(), bean.getId(), bean.getName(), bean.getType());
			setValueByParts(bean.getParts(), lineChart.getData(), Part.roleRange);
		}
		return lineChart;
	}

	public static LineChart pie2ActivityLineChart(List<ActivityStatistic> reportBaseBeanList) {
		List<String> roleRanges = new ArrayList(Part.activityRange.values());
		LineChart lineChart = initLineChart(roleRanges);
		for (ActivityStatistic bean : reportBaseBeanList) {
			initResource(lineChart.getResources(), bean.getId(), bean.getName(), bean.getType());
			setValueByParts(bean.getParts(), lineChart.getData(), Part.activityRange);
		}
		return lineChart;
	}

	private static LineChart initLineChart(List<String> roleRanges) {
		LineChart lineChart = new LineChart();
		List<ChartResource> resources = new ArrayList<>();
		Map<String, List<String>> data = initDataMap(roleRanges);
		lineChart.setResources(resources);
		lineChart.setNames(roleRanges);
		lineChart.setData(data);
		return lineChart;
	}

	private static void initResource(List<ChartResource> resources, Long id, String name, Integer type) {
		ChartResource r = new ChartResource();
		r.setId(id);
		r.setName(name);
		r.setType(type);
		resources.add(r);
	}

	private static void setValueByParts(List<Part> parts, Map<String, List<String>> data, Map<String, String> ranges) {

		List<Part> temp = new ArrayList<>();
		for (Part part : parts) {
			if (org.apache.commons.lang3.StringUtils.isNotBlank(part.getValue())
					&& org.apache.commons.lang3.StringUtils.isNotBlank(part.getPart())) {
				temp.add(part);
			}
		}
		parts.clear();
		parts.addAll(temp);
		for (String roleRange : ranges.keySet()) {
			boolean isExists = false;
			String partName = ranges.get(roleRange);
			for (Part part : parts) {
				if (part.getPart().equals(roleRange)) {
					part.setName(partName);
					isExists = true;
					break;
				}
			}
			if (!isExists) {
				Part p = new Part();
				p.setPart(roleRange);
				p.setName(partName);
				p.setValue("0");
				parts.add(p);
			}
		}
		int sum=0;
		for (Part part : parts) {
			sum += Integer.valueOf(part.getValue());
		}
		DecimalFormat format=new DecimalFormat("##.##");
		for (Part part : parts) {
			// 处理值生成百分比
			data.get(part.getName()).add(getPercent(Integer.valueOf(part.getValue()),sum,format));
		}
	}

	// 保存后两位例如 12.66
	private static String getPercent(int v, Integer sum,DecimalFormat format) {
		if (sum == 0) {
			return "0";
		}
		return format.format((double)v*100/sum);
	}

	private static Map<String, List<String>> initDataMap(List<String> roleRanges) {
		Map<String, List<String>> data = new LinkedHashMap<>();
		for (String range : roleRanges) {
			data.put(range, new ArrayList<>());
		}
		return data;
	}

	public static LineChart pie2ProcessInformationChart(List<InformationStatistic> statistics) {
		Map<String, String> informationMap = getInformationMap(statistics);
		List<String> roleRanges = new ArrayList(informationMap.values());
		LineChart lineChart = initLineChart(roleRanges);
		for (InformationStatistic bean : statistics) {
			initResource(lineChart.getResources(), bean.getId(), bean.getName(), bean.getType());
			setValueByParts(bean.getParts(), lineChart.getData(), informationMap);
		}
		return lineChart;
	}

	private static Map<String, String> getInformationMap(List<InformationStatistic> statistics) {
		Map<String, String> informationMap = new HashMap<String, String>();
		for (InformationStatistic inforamtion : statistics) {
			List<Part> parts = inforamtion.getParts();
			for (Part part : parts) {
				if (part.getName() == null || part.getValue() == null) {
					continue;
				}
				informationMap.put(part.getPart(), part.getName());
			}
		}
		return informationMap;
	}
}
