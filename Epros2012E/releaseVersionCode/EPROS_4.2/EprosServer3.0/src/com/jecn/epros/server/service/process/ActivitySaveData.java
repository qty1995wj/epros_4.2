package com.jecn.epros.server.service.process;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT;
import com.jecn.epros.server.bean.integration.JecnControlPointT;
import com.jecn.epros.server.bean.process.JecnActivityFileT;
import com.jecn.epros.server.bean.process.JecnFlowStructureImageT;
import com.jecn.epros.server.bean.process.JecnModeFileT;
import com.jecn.epros.server.bean.process.JecnRefIndicatorsT;
import com.jecn.epros.server.bean.process.inout.JecnFigureInoutT;

public class ActivitySaveData implements Serializable {
	/** 活动元素 */
	private JecnFlowStructureImageT jecnFlowStructureImageT;
	/** 输出 增加 */
	private List<JecnModeFileT> listModeFileT;
	/** 输入和操作规范 增加 */
	private List<JecnActivityFileT> listJecnActivityFileT;
	/** 指标 增加 */
	private List<JecnRefIndicatorsT> listRefIndicatorsT;
	/** 控制点 */
	private JecnControlPointT controlPoint;

	/** 控制点集合 C */
	private List<JecnControlPointT> listControlPoint;

	/** 活动标准关联表 */
	private List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT;
	/** 线上信息 */
	private List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean;

	/** new 活动输入、输出集合 */
	/** 活动输入、输出 new */
	private List<JecnFigureInoutT> listFigureInTs;

	/** 活动输入、输出 new */
	private List<JecnFigureInoutT> listFigureOutTs;

	public JecnControlPointT getControlPoint() {
		return controlPoint;
	}

	public void setControlPoint(JecnControlPointT controlPoint) {
		this.controlPoint = controlPoint;
	}

	public List<JecnActiveStandardBeanT> getListJecnActiveStandardBeanT() {
		return listJecnActiveStandardBeanT;
	}

	public void setListJecnActiveStandardBeanT(List<JecnActiveStandardBeanT> listJecnActiveStandardBeanT) {
		this.listJecnActiveStandardBeanT = listJecnActiveStandardBeanT;
	}

	public List<JecnActiveOnLineTBean> getListJecnActiveOnLineTBean() {
		return listJecnActiveOnLineTBean;
	}

	public void setListJecnActiveOnLineTBean(List<JecnActiveOnLineTBean> listJecnActiveOnLineTBean) {
		this.listJecnActiveOnLineTBean = listJecnActiveOnLineTBean;
	}

	public JecnFlowStructureImageT getJecnFlowStructureImageT() {
		return jecnFlowStructureImageT;
	}

	public void setJecnFlowStructureImageT(JecnFlowStructureImageT jecnFlowStructureImageT) {
		this.jecnFlowStructureImageT = jecnFlowStructureImageT;
	}

	public List<JecnModeFileT> getListModeFileT() {
		return listModeFileT;
	}

	public void setListModeFileT(List<JecnModeFileT> listModeFileT) {
		this.listModeFileT = listModeFileT;
	}

	public List<JecnActivityFileT> getListJecnActivityFileT() {
		return listJecnActivityFileT;
	}

	public void setListJecnActivityFileT(List<JecnActivityFileT> listJecnActivityFileT) {
		this.listJecnActivityFileT = listJecnActivityFileT;
	}

	public List<JecnRefIndicatorsT> getListRefIndicatorsT() {
		return listRefIndicatorsT;
	}

	public void setListRefIndicatorsT(List<JecnRefIndicatorsT> listRefIndicatorsT) {
		this.listRefIndicatorsT = listRefIndicatorsT;
	}

	public List<JecnControlPointT> getListControlPoint() {
		return listControlPoint;
	}

	public void setListControlPoint(List<JecnControlPointT> listControlPoint) {
		this.listControlPoint = listControlPoint;
	}

	public List<JecnFigureInoutT> getListFigureInTs() {
		return listFigureInTs;
	}

	public void setListFigureInTs(List<JecnFigureInoutT> listFigureInTs) {
		this.listFigureInTs = listFigureInTs;
	}

	public List<JecnFigureInoutT> getListFigureOutTs() {
		return listFigureOutTs;
	}

	public void setListFigureOutTs(List<JecnFigureInoutT> listFigureOutTs) {
		this.listFigureOutTs = listFigureOutTs;
	}

}
