package com.jecn.epros.domain.report;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色数统计
 *
 * @author user
 */
public class ActivityStatistic extends ReportBaseBean {
    /**
     * 0-10
     **/
    public final static int ONE = 1;
    /**
     * 11-20
     **/
    public final static int TWO = 2;
    /**
     * 20个以上
     **/
    public final static int THREE = 3;

    private List<Part> parts = new ArrayList<Part>();

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

}
