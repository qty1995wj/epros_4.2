package epros.designer.gui.system.config.ui.property.type;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.bottom.JecnOKJButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigSpinner;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextField;
import epros.designer.gui.system.config.ui.comp.JecnDocumentListener;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 系统管理：基本信息面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnBasicInfoPubPanel extends JecnAbstractPropertyBasePanel
		implements ChangeListener, ActionListener {

	// **************公司名称面板 **************//
	/** 公司名称面板 */
	private JecnPanel cmpPanel = null;
	/** 公司名称 */
	private JLabel cmpLabel = null;
	/** 公司名称输入框 */
	private JecnConfigTextField cmpTextField = null;
	/** 公司名称提示信息 */
	private JecnUserInfoTextArea cmpNameInfoArea = null;
	// **************公司名称面板 **************//

	// **************EPros登录URL面板 **************//
	/** EPros登录URL面板 */
	private JecnPanel eprosURLPanel = null;
	/** EPros登录URL标签 */
	private JLabel eprosURLLabel = null;
	/** EPros登录URL输入框 */
	private JecnConfigTextField eprosURLTextField = null;
	/** EPros登录URL提示信息 */
	private JecnUserInfoTextArea eprosURLNameInfoArea = null;
	// **************EPros登录URL面板 **************//

	// ******************自动存档******************//
	/** 自动存档面板 */
	private JecnPanel autoSavePanel = null;
	/** 自动存档设置时间 */
	private JLabel autoSaveLabel = null;
	/** 是否启动自动存档：选中启动；不选中不启动 */
	private JecnConfigCheckBox autoSaveCheckBox = null;
	/** 选择时间 */
	private JecnConfigSpinner autoSaveSpinner = null;
	/** 选择时间的单位：分钟 */
	private JLabel autoSaveUnitLabel = null;
	
	/**保存恢复数据间隔时间**/
	private JLabel saveRecoveryLabel = null;
	
	/** 保存恢复数据间隔时间：选中启动；不选中不启动 */
	private JecnConfigCheckBox saveRecoveryBox = null;
	/** 保存恢复数据间隔时间:选择时间 */
	private JecnConfigSpinner saveRecoverySpinner = null;
	/** 选择时间的单位：分钟 */
	private JLabel saveRecoveryUnitLabel = null;
//	/** 同一部门下允许岗位名称相同：选中启动；不选中不启动 */
//	private JecnConfigCheckBox allowOrgSamePosBox = null;
	/** 保存恢复数据间隔时间面板 */
	private JecnPanel saveRecoveryPanel = null;

	// ******************自动存档******************//

	public JecnBasicInfoPubPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);

		iniComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {

		// **************公司名称面板 start**************//
		// 公司名称面板
		cmpPanel = new JecnPanel();
		// 公司名称
		cmpLabel = new JLabel(JecnProperties.getValue("mailCmpSetName"));
		// 公司名称输入框
		cmpTextField = new JecnConfigTextField();
		// 公司名称提示信息
		cmpNameInfoArea = new JecnUserInfoTextArea();

		// **************公司名称面板 end**************//

		// **************EPros登录URL面板 **************//
		// EPros登录URL面板
		eprosURLPanel = new JecnPanel();
		// EPros登录URL标签
		eprosURLLabel = new JLabel(JecnProperties.getValue("eprosURLLabel"));
		// EPros登录URL输入框
		eprosURLTextField = new JecnConfigTextField();
		// EPros登录URL提示信息
		eprosURLNameInfoArea = new JecnUserInfoTextArea();
		// **************EPros登录URL面板 **************//

		// **************自动存档设置时间 start **************//
		// 自动存档面板
		autoSavePanel = new JecnPanel();
		// 标签
		autoSaveLabel = new JLabel(JecnProperties.getValue("task_autoSaveName"));
		// 是否启动自动存档：选中启动；不选中不启动
		autoSaveCheckBox = new JecnConfigCheckBox(JecnProperties
				.getValue("task_isAutoSave"));
		// 自动存档设置时间
		autoSaveSpinner = new JecnConfigSpinner(new SpinnerNumberModel(10, 1,
				600, 1));
		// 选择时间的单位：分钟
		autoSaveUnitLabel = new JLabel(JecnProperties.getValue("task_minutes"));
		// **************自动存档设置时间end **************//
		//保存恢复数据间隔时间
		saveRecoveryLabel = new JLabel(JecnProperties.getValue("saveRecoveryTime"));
		//保存恢复数据间隔时间:选中启用，不选中不启用
		saveRecoveryBox = new JecnConfigCheckBox(JecnProperties.getValue("task_isAutoSave"));
		//自动存档设置时间
		saveRecoverySpinner = new JecnConfigSpinner(new SpinnerNumberModel(10, 1,
				600, 1));
		//选择时间的单位：分钟
		saveRecoveryUnitLabel = new JLabel(JecnProperties.getValue("task_minutes"));
		
		saveRecoveryBox.setOpaque(false);
		saveRecoveryPanel = new JecnPanel();
		saveRecoveryPanel.setOpaque(false);
		saveRecoveryPanel.setBorder(null);
		
		
		
		this.setLayout(new GridBagLayout());

		// 自动存档面板
		autoSavePanel.setOpaque(false);
		autoSavePanel.setBorder(null);

		// 是否启动自动存档：选中启动；不选中不启动
		autoSaveCheckBox.setOpaque(false);
		autoSaveCheckBox.setSelected(true);

		autoSaveSpinner.setToolTipText("1~600" + autoSaveUnitLabel.getText());

		// 公司名称提示信息
		cmpNameInfoArea.setVisible(false);

		// 事件
		// 选择时间
		autoSaveSpinner.addChangeListener(this);
		// 是否启动自动存档：选中启动；不选中不启动
		autoSaveCheckBox.addActionListener(this);

		// 事件
		cmpTextField.getDocument().addDocumentListener(
				new JecnDocumentListener(this, cmpTextField, cmpNameInfoArea));
		// 事件
		eprosURLTextField.getDocument().addDocumentListener(
				new JecnDocumentListener(this, eprosURLTextField,
						eprosURLNameInfoArea));
		
		//保存恢复数据间隔时间:选择时间
		saveRecoverySpinner.setToolTipText("1~600" + saveRecoveryUnitLabel.getText());
		saveRecoverySpinner.addChangeListener(this);
		//保存恢复数据间隔时间：选中启动；不选中不启动
		saveRecoveryBox.addActionListener(this);
		
		// 是否存在浏览端，如果不存在，则不显示"浏览器登录平台地址"
		if (!JecnConstants.isPubShow()) {
			eprosURLPanel.setVisible(false);
		}
	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		Insets insets = new Insets(5, 5, 5, 5);

		// *************第一层布局*************//
		Insets firstInsets = new Insets(0, 0, 5, 0);
		// 公司面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				firstInsets, 0, 0);
		this.add(cmpPanel, c);
		// EPros登录URL面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				firstInsets, 0, 0);
		this.add(eprosURLPanel, c);
		// 自动存档设置时间面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				firstInsets, 0, 0);
		this.add(autoSavePanel, c);
		// *************第一层布局*************//

		// *************公司*************//
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		cmpPanel.add(cmpLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0);
		cmpPanel.add(cmpTextField, c);
		// 公司名称提示信息
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		cmpPanel.add(cmpNameInfoArea, c);

		// 公司logo图标
		JPanel compLogoPanel = JecnDesignerProcess.getDesignerProcess()
				.getConfigFileConPanyPanel();
		if (compLogoPanel != null) {
			c = new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0,
					GridBagConstraints.NORTHWEST,
					GridBagConstraints.HORIZONTAL, insets, 0, 0);
			cmpPanel.add(compLogoPanel, c);
		}
		// *************公司*************//

		// *************EPros登录URL*************//
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		eprosURLPanel.add(eprosURLLabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 0, 5), 0, 0);
		eprosURLPanel.add(eprosURLTextField, c);
		// 公司名称提示信息
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		eprosURLPanel.add(eprosURLNameInfoArea, c);
		// *************EPros登录URL*************//

		// **************自动存档设置时间 start **************//
		// 标签
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		autoSavePanel.add(autoSaveLabel, c);
		// 是否启动自动存档：选中启动；不选中不启动
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		autoSavePanel.add(autoSaveCheckBox, c);
		// 自动存档设置时间
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 1), 0, 0);
		autoSavePanel.add(autoSaveSpinner, c);
		// 选择时间的单位：分钟
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5,
						0, 5, 5), 0, 0);
		autoSavePanel.add(autoSaveUnitLabel, c);
		
		// **************自动存档设置时间end **************//
		//***************保存恢复数据间隔时间
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(saveRecoveryPanel, c);
		
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(5, 0, 5, 0), 0, 0);
		saveRecoveryPanel.add(saveRecoveryLabel, c);
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				insets, 0, 0);
		saveRecoveryPanel.add(saveRecoveryBox, c);
		c = new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(5, 5, 5, 0), 0, 0);
		saveRecoveryPanel.add(saveRecoverySpinner, c);
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				firstInsets, 0, 0);
		saveRecoveryPanel.add(saveRecoveryUnitLabel, c);
		
		c = new GridBagConstraints(0, 4, 3, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5,
						0, 5, 5), 0, 0);
		this.add(new JLabel(), c);
		
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null
				|| !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean
				.getOtherItemList()) {
			// 唯一标识
			String mark = itemBean.getMark();
			// 值
			String itemValue = itemBean.getValue();
			if (ConfigItemPartMapMark.basicIsAutoSave.toString().equals(mark)) {// 是否启用自动存档（1:启动;0:禁止）
				autoSaveCheckBox.setItemBeanAndValue(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(itemBean
						.getValue())) {// 启用
					autoSaveSpinner.setEnabled(true);
				} else {// 禁止
					autoSaveSpinner.setEnabled(false);
				}
			} else if (ConfigItemPartMapMark.basicAutoSaveTime.toString()
					.equals(mark)) {// 自动存档分钟数
				autoSaveSpinner.setItemBeanAndValue(itemBean);
			} else if (ConfigItemPartMapMark.basicCmp.toString().equals(mark)) {// 公司名称配置
				cmpTextField.setItemBean(itemBean);
				cmpTextField.setText(itemValue);
			} else if (ConfigItemPartMapMark.basicEprosURL.toString().equals(
					mark)) {// Epros登录地址
				eprosURLTextField.setItemBean(itemBean);
				eprosURLTextField.setText(itemValue);
			}else if (ConfigItemPartMapMark.saveRecovery.toString().equals(mark)) {// 保存恢复数据间隔时间：是否启用自动存档（1:启动;0:禁止）
				saveRecoveryBox.setItemBeanAndValue(itemBean);
				if (JecnConfigContents.ITEM_AUTO_SAVE_YES.equals(itemBean
						.getValue())) {// 启用
					saveRecoverySpinner.setEnabled(true);
				} else {// 禁止
					saveRecoverySpinner.setEnabled(false);
				}
			}  else if (ConfigItemPartMapMark.saveRecoveryValue.toString()
					.equals(mark)) {// 自动存档分钟数
				saveRecoverySpinner.setItemBeanAndValue(itemBean);
			}
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() instanceof JecnConfigSpinner) {
			JecnConfigSpinner configSpinner = (JecnConfigSpinner) e.getSource();
			if (configSpinner == autoSaveSpinner) {// 选择时间
				String autoSaveValue = autoSaveSpinner.getValue().toString();
				autoSaveSpinner.getItemBean().setValue(autoSaveValue);
				return;
			}else if (configSpinner == saveRecoverySpinner) {// 选择时间
				String saveRecoveryValue = saveRecoverySpinner.getValue().toString();
				saveRecoverySpinner.getItemBean().setValue(saveRecoveryValue);
				return;
			}

			// 整体校验
			check();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == autoSaveCheckBox) { // 是否启动自动存档：选中启动；不选中不启动(1：启动
				// 0：不启动)
				if (autoSaveCheckBox.isSelected()) {// 选中
					autoSaveSpinner.setEnabled(true);
					autoSaveCheckBox.getItemBean().setValue(
							JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					autoSaveSpinner.setEnabled(false);
					autoSaveCheckBox.getItemBean().setValue(
							JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}else if (e.getSource() == saveRecoveryBox) { //保存恢复数据间隔时间：是否启用自动存档（1:启动;0:禁止）
				// 0：不启动)
				if (saveRecoveryBox.isSelected()) {// 选中
					saveRecoverySpinner.setEnabled(true);
					saveRecoveryBox.getItemBean().setValue(
							JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					saveRecoverySpinner.setEnabled(false);
					saveRecoveryBox.getItemBean().setValue(
							JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}
		}
	}

	/**
	 * 
	 * 校验数据
	 * 
	 * @return boolean 校验成功：true ；校验失败：false
	 * 
	 */
	@Override
	public boolean check() {
		// true：校验成功；false：校验失败
		boolean success = true;

		// 公司名称提示信息:不能超过122个字符或61个汉字
		String info = JecnUserCheckUtil.checkNameLength(cmpTextField.getText());
		// boolean true:有错；false：无错
		success = !this.setErrorInfo(cmpNameInfoArea, info);

		// *EPros登录URL提示信息:不能超过122个字符或61个汉字
		info = JecnUserCheckUtil.checkNameLength(eprosURLTextField.getText());
		// boolean true:有错；false：无错
		boolean eprosUrlRet = !this.setErrorInfo(eprosURLNameInfoArea, info);
		if (success && eprosUrlRet) {
			success = true;
		} else {
			success = false;
		}

		JecnOKJButton btn = dialog.getOkCancelJButtonPanel().getOkJButton();
		if (success) {// 验证成功
			if (!btn.isEnabled()) {// 验证成功
				btn.setEnabled(true);
			}
		} else {
			if (btn.isEnabled()) {
				btn.setEnabled(false);
			}
		}
		return success;
	}

	/**
	 * 
	 * 设置错误信息
	 * 
	 * @param addrInfoTextArea
	 *            JecnUserInfoTextArea
	 * @param info
	 * @return boolean true:有错；false：无错
	 */
	@Override
	public boolean setErrorInfo(JecnUserInfoTextArea addrInfoTextArea,
			String info) {
		if (!DrawCommon.isNullOrEmtryTrim(info)) {// 有错
			if (!addrInfoTextArea.isVisible()) {
				addrInfoTextArea.setVisible(true);
			}
			addrInfoTextArea.setText(info);
			return true;
		} else {// 无错
			if (addrInfoTextArea.isVisible()) {
				addrInfoTextArea.setVisible(false);
			}
			addrInfoTextArea.setText("");
			return false;
		}
	}
}
