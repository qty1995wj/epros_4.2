<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="publicFileSearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%" style="vertical-align: middle;">
						<tr>
							<td width="10%" height="25px" align="right">
								<!-- 文件名称： -->
								<s:text name="fileNameC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%" id="publicName_id"
									maxLength="32" />
							</td>


							<td width="10%" align="right">
								<!-- 文件编号： -->
								<s:text name="fileNumberC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%" id="publicNumber_id"
									maxLength="32" />
							</td>

							<td style="vertical-align: middle; width: 10%;" align="center">
								<a onClick="toggle(publicFileSearchText)"
									style="cursor: pointer;" class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" /> </span> </a>
							</td>

						</tr>

						<tr>
							<td height="25px" align="right">
								<!-- 文件类型: -->
								<s:text name="fileTypeC" />
							</td>
							<td align="left">
								<select style="width: 80%" id="publicType_id">
									<option value="0">
										<s:text name="flow" />
									</option>
									<option value="1">
										<s:text name="file" />
									</option>
									<s:if test="#application.versionType">
										<option value="2">
											<s:text name="standard" />
										</option>
										<option value="3">
											<s:text name="rule" />
										</option>
									</s:if>
									<option value="-1">
										<s:text name="all"></s:text>
										<!-- 全部 -->
									</option>
								</select>
							</td>


							<td align="right">
								<!-- 责任部门: -->
								<s:text name="responsibilityDepartmentC" />
							</td>
							<td align="left">
								<input type="text" style="width: 63%; vertical-align: middle;"
									id="public_orgDutyName_id"
									onkeyup="search(public_orgDutyName_id,public_orgDutyId_id,search_publicFile_orgDutyName,'org');"
									onblur="divNode(search_publicFile_orgDutyName)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("organization","public_orgDutyName_id","public_orgDutyId_id")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br>
								<div id="search_publicFile_orgDutyName"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td></td>
						</tr>
						<tr>
							<td height="35px" align="right">
								<!-- 密级 -->
								<s:text name="intensiveC"></s:text>
							</td>
							<td align="left">
								<select style="width: 80%" id="is_public">
									<!-- 全部 -->
									<option value="-1">
										<s:text name="all"></s:text>
									</option>
									<!-- 公开 -->
									<option value="1" selected="selected">
										<s:text name="public"></s:text>
									</option>
									<!-- 秘密 -->
									<option value="0">
										<s:text name="secret"></s:text>
									</option>
								</select>
							</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</div>
				<!-- 责任部门ID -->
				<input type="hidden" id="public_orgDutyId_id" value="-1" />
				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="publicFileGridSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="publicFileReset()" />
				</div>
			</div>

			<div id="publicFileGrid_id">

			</div>
		</div>
	</body>
</html>