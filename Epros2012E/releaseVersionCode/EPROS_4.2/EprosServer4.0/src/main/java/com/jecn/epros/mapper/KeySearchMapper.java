package com.jecn.epros.mapper;

import com.jecn.epros.domain.keySearch.ResourceSearchBean;
import com.jecn.epros.sqlprovider.KeySearchProvider;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

public interface KeySearchMapper {

    /**
     * 资源检索搜索
     *
     * @param map
     * @return
     */
    @SelectProvider(type = KeySearchProvider.class, method = "findKeySearchResultSql")
    @ResultMap("mapper.Search.keySearch")
    public List<ResourceSearchBean> findKeySearchResult(Map<String, Object> map);

    /**
     * 资源检索，总数
     *
     * @param map
     * @return
     */
    @SelectProvider(type = KeySearchProvider.class, method = "findKeySearchCountResultSql")
    public List<Map<String, Object>> findKeySearchCountResult(Map<String, Object> map);
}
