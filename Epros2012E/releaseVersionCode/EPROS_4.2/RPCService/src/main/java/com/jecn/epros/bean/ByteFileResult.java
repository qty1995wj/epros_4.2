package com.jecn.epros.bean;

import java.io.Serializable;

public class ByteFileResult extends ByteFile implements Serializable, Result {

	private boolean isSuccess = true;
	private String resultMessage = "";

	public ByteFileResult() {

	}

	public ByteFileResult(boolean isSuccess, String resultMessage) {
		this.isSuccess = isSuccess;
		this.resultMessage = resultMessage;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
}
