package com.jecn.epros.domain.org;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.service.TreeNodeUtils;

public class OrgBaseInfoBean {
    private Long orgId;
    private String orgName;
    private String orgRespon;// 部门职责
    private String path;
    private Integer level;

    @JsonIgnore
    public LinkResource getOrgLink(TreeNodeUtils.NodeType from) {
        return new LinkResource(orgId, orgName,LinkResource.ResourceType.ORGANIZATION , from);
    }
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getOrgRespon() {
        return orgRespon;
    }

    public void setOrgRespon(String orgRespon) {
        this.orgRespon = orgRespon;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
