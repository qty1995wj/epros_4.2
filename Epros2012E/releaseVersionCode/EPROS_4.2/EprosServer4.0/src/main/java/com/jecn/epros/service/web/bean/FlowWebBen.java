package com.jecn.epros.service.web.bean;

import java.util.List;

/**
 * 接口 流程 架构 资源文件公用 bean
 * Created by Administrator on 2019/2/28.
 */
public class FlowWebBen {
    /**
     * ID
     */
    private Long flowId;
    /**
     * 名称
     */
    private String flowName;
    /**
     * 资源访问链接
     */
    private String url;

    /**
     * 子节点
     */
    private List<FlowWebBen> flowChildrens;

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<FlowWebBen> getFlowChildrens() {
        return flowChildrens;
    }

    public void setFlowChildrens(List<FlowWebBen> flowChildrens) {
        this.flowChildrens = flowChildrens;
    }
}
