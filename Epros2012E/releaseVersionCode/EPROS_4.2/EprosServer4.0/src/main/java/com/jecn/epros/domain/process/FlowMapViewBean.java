package com.jecn.epros.domain.process;

import java.util.List;

public class FlowMapViewBean {

    private int total;
    private List<FlowMapSearchResultBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<FlowMapSearchResultBean> getData() {
        return data;
    }

    public void setData(List<FlowMapSearchResultBean> data) {
        this.data = data;
    }
}
