package epros.draw.constant;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnFigureDataCloneable;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnLineSegmentDataCloneable;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.event.flowElement.JecnFlowResizeActionProcess;
import epros.draw.gui.box.JecnToolBoxAbstractScrollPane;
import epros.draw.gui.box.JecnToolBoxButton;
import epros.draw.gui.box.JecnToolBoxMainPanel;
import epros.draw.gui.figure.JecnBaseActiveFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnFigureRefManhattanLine;
import epros.draw.gui.line.JecnBaseLineSegmentPanel;
import epros.draw.gui.line.JecnMoveLineUnit;
import epros.draw.gui.line.shape.DrawArc;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.operationConfig.data.JecnFlowOperationDataUnit;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnBaseResizePanel;
import epros.draw.gui.swing.popup.popup.JecnPopupMenu;
import epros.draw.gui.top.button.JecnToolbarGroupButton;
import epros.draw.gui.top.button.JecnToolbarTitleButton;
import epros.draw.gui.top.dialog.JecnDialogDropTitlePanel;
import epros.draw.gui.top.dialog.JecnDialogResizeContentPanel;
import epros.draw.gui.top.toolbar.JecnAstractToolBarItemPanel;
import epros.draw.gui.top.toolbar.JecnOftenPartPanel;
import epros.draw.gui.top.toolbar.element.JecnAbstarctFlowElementPanel;
import epros.draw.gui.top.toolbar.element.JecnElementButtonPanel;
import epros.draw.gui.top.toolbar.element.JecnFlowElementAttributesPanel;
import epros.draw.gui.top.toolbar.element.JecnFlowElementButtonsPanel;
import epros.draw.gui.top.toolbar.element.JecnFlowElementMainPanel;
import epros.draw.gui.top.toolbar.element.JecnFlowElementMangPanel;
import epros.draw.gui.top.toolbar.element.JecnFlowElementPanel;
import epros.draw.gui.top.toolbar.io.JecnFlowImport;
import epros.draw.gui.workflow.aid.JecnAidCompCollection;
import epros.draw.gui.workflow.aid.JecnLinkFigureMainProcess;
import epros.draw.gui.workflow.aid.JecnLinkTrianglesButton;
import epros.draw.gui.workflow.aid.JecnPlainDocument;
import epros.draw.gui.workflow.grid.JecnShapeGridProcess;
import epros.draw.gui.workflow.grid.JecnShapeScalePanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnDrawScrollPane;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitleButton;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitlePanel;
import epros.draw.system.JecnFlowElementForXML;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnDefaultFlowElementData;
import epros.draw.unredo.JecnUndoRedoManager;
import epros.draw.unredo.data.JecnUnRedoDividingLineData;
import epros.draw.unredo.data.JecnUnRedoFigureData;
import epros.draw.unredo.data.JecnUnRedoManhattanLineData;
import epros.draw.unredo.data.JecnUnRedoVHLineData;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 日志对象类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnLogConstants {
	// ********************流程元素库 start********************//
	public static final Log LOG_TOOLBOX_PANEL = LogFactory
			.getLog(JecnToolBoxMainPanel.class);
	public static final Log LOG_TOOLBOX_BUTTON = LogFactory
			.getLog(JecnToolBoxButton.class);
	public static final Log LOG_JecnToolBoxAbstractScrollPane = LogFactory
			.getLog(JecnToolBoxAbstractScrollPane.class);
	// ********************流程元素库 end********************//

	// ********************工具栏 start********************//
	/** JecnToolbarGroupButton日志对象 */
	public static final Log LOG_TOOLBAR_GROUP_BUTTON = LogFactory
			.getLog(JecnToolbarGroupButton.class);
	/** JecnToolbarTitleButton日志对象 */
	public static final Log LOG_TOOLBAR_TITLE_BUTTON = LogFactory
			.getLog(JecnToolbarTitleButton.class);
	/** JecnOftenPartPanel日志对象 */
	public static final Log LOG_OFTEN_PART_PANEL = LogFactory
			.getLog(JecnOftenPartPanel.class);
	/** JecnAstractToolBarItemPanel日志对象 */
	public static final Log LOG_ASTRACT_TOOLBAR_ITEM_PANEL = LogFactory
			.getLog(JecnAstractToolBarItemPanel.class);
	public static final Log Log_JecnFlowImport = LogFactory
			.getLog(JecnFlowImport.class);
	// ********************工具栏 end********************//

	// ********************画图区域 start********************//
	/** 画图面板的标题面板中关闭按钮： JecnTabbedTitleButton日志对象 */
	public static final Log LOG_TABBED_TITLE_BUTTON = LogFactory
			.getLog(JecnTabbedTitleButton.class);
	/** JecnDrawDesktopPane日志对象 */
	public static final Log LOG_DRAW_DESKTOP_PANE = LogFactory
			.getLog(JecnDrawDesktopPane.class);
	/** JecnDrawScrollPane日志对象 */
	public static final Log LOG_DRAW_SCROLL_PANE = LogFactory
			.getLog(JecnDrawScrollPane.class);
	/** JecnTabbedTitlePanel日志对象 */
	public static final Log LOG_TABBED_TITLE_PANEL = LogFactory
			.getLog(JecnTabbedTitlePanel.class);
	/** JecnLinkFigurePanel日志对象 */
	public static final Log LOG_LINK_FIGURE_PANEL = LogFactory
			.getLog(JecnLinkFigureMainProcess.class);
	/** JecnLinkTrianglesButton日志对象 */
	public static final Log LOG_LINK_TRIANGLES_BUTTON = LogFactory
			.getLog(JecnLinkTrianglesButton.class);

	/** JecnShapeScalePanel日志对象 */
	public static final Log LOG_SHAPE_SCALE_PANEL = LogFactory
			.getLog(JecnShapeScalePanel.class);
	/** JecnShapeGridProcess日志对象 */
	public static final Log LOG_SHAPE_GRID_PROCESS = LogFactory
			.getLog(JecnShapeGridProcess.class);

	public static final Log LOG_JecnPlainDocument = LogFactory
			.getLog(JecnPlainDocument.class);
	// ********************画图区域 end********************//

	// ********************图形和线 start********************//
	/** 图形、线的基类：JecnBaseFigure日志对象 */
	public static final Log LOG_BASE_FIGURE = LogFactory
			.getLog(JecnBaseFigurePanel.class);
	/** JecnBaseLineSegmentPanel日志对象 */
	public static final Log LOG_BASE_LINE_SEGMENT_PANEL = LogFactory
			.getLog(JecnBaseLineSegmentPanel.class);
	/** LineSegment日志对象 */
	public static final Log LOG_LINE_SEGMENT = LogFactory
			.getLog(LineSegment.class);
	/** JecnBaseFlowElementPanel日志对象 */
	public static final Log LOG_BASE_FLOW_ELEMENT_PANEL = LogFactory
			.getLog(JecnBaseFlowElementPanel.class);
	public static final Log LOG_JecnBaseActiveFigure = LogFactory
			.getLog(JecnBaseActiveFigure.class);
	public static final Log LOG_JecnBaseRoleFigure = LogFactory
			.getLog(JecnBaseRoleFigure.class);
	/** DrawArc日志对象 */
	public static final Log LOG_DRAW_ARC = LogFactory.getLog(DrawArc.class);
	// ********************图形和线 end********************//

	// ********************对话框 start********************//
	/** JecnDialogDropTitlePanel日志对象 */
	public static final Log LOG_DIALOG_DROP_TITLE_PANEL = LogFactory
			.getLog(JecnDialogDropTitlePanel.class);
	/** JecnDialogResizeContentPanel日志对象 */
	public static final Log LOG_DIALOG_RESIZE_CONTENT_PANEL = LogFactory
			.getLog(JecnDialogResizeContentPanel.class);
	// ********************对话框 end********************//

	// ********************系统配置 start********************//
	public static final Log LOG_DEFAULT_FLOW_ELEMENT_DATA = LogFactory
			.getLog(JecnDefaultFlowElementData.class);
	public static final Log LOG_RESOURCE_UTIL = LogFactory
			.getLog(JecnResourceUtil.class);
	public static final Log LOG_FLOW_ELEMENT_FORXML = LogFactory
			.getLog(JecnFlowElementForXML.class);
	public static final Log LOG_SYSTEM_DATA = LogFactory
			.getLog(JecnSystemData.class);
	// ********************系统配置 end********************//

	// ********************数据层 start********************//
	public static final Log LOG_ABSTRACT_FLOW_ELEMENT_DATA = LogFactory
			.getLog(JecnAbstractFlowElementData.class);
	public static final Log LOG_JecnFigureDataCloneable = LogFactory
			.getLog(JecnFigureDataCloneable.class);
	/** JecnFigureData日志对象 */
	public static final Log LOG_JecnFigureData = LogFactory
			.getLog(JecnFigureData.class);
	/** JecnLineSegmentData日志对象 */
	public static final Log LOG_LINE_SEGMENT_DATA = LogFactory
			.getLog(JecnManhattanRouterResultData.class);
	/** JecnLineSegmentDataProcess日志对象 */
	public static final Log LOG_LINE_SEGMENT_DATA_PROCESS = LogFactory
			.getLog(JecnLineSegmentDataCloneable.class);
	/** JecnDividingLineData日志对象 */
	public static final Log LOG_DIVIDING_LINE_DATA = LogFactory
			.getLog(JecnBaseDivedingLineData.class);
	// ********************数据层 end********************//

	// ********************流程元素 start********************//
	/** JecnFlowElementButtonsPanel日志对象 */
	public static final Log LOG_FIGURE_BUTTONPANEL = LogFactory
			.getLog(JecnFlowElementButtonsPanel.class);
	/** JecnFlowElementMainPanel日志对象 */
	public static final Log LOG_FLOW_ELEMENT_MAIN_PANEL = LogFactory
			.getLog(JecnFlowElementMainPanel.class);
	/** JecnFlowElementPanel日志对象 */
	public static final Log LOG_FLOW_ELEMENT_PANEL = LogFactory
			.getLog(JecnFlowElementPanel.class);
	/** JecnFlowElementMangPanel日志对象 */
	public static final Log LOG_FLOW_ELEMENT_MANG_PANEL = LogFactory
			.getLog(JecnFlowElementMangPanel.class);
	/** JecnFlowElementAttributesPanel日志对象 */
	public static final Log LOG_FLOW_ELEMENT_ATTRIBUTES_PANEL = LogFactory
			.getLog(JecnFlowElementAttributesPanel.class);
	/** JecnElementButtonPanel日志对象 */
	public static final Log LOG_ELEMENT_BUTTON_PANEL = LogFactory
			.getLog(JecnElementButtonPanel.class);
	/** JecnAbstarctFlowElementPanel日志对象 */
	public static final Log log_JecnAbstarctFlowElementPanel = LogFactory
			.getLog(JecnAbstarctFlowElementPanel.class);
	// ********************流程元素 end********************//

	// ********************辅助集合 start********************//
	/** JecnAidCompCollection日志对象 */
	public static final Log log_JecnAidCompCollection = LogFactory
			.getLog(JecnAidCompCollection.class);
	// ********************辅助集合 end********************//

	// ********************撤销恢复 start********************//
	/** JecnUndoRedoManager撤销恢复管理对象 */
	public static final Log log_UndoRedoManager = LogFactory
			.getLog(JecnUndoRedoManager.class);
	public static final Log log_JecnUnRedoFigureData = LogFactory
			.getLog(JecnUnRedoFigureData.class);
	public static final Log log_JecnUnRedoManhattanLineData = LogFactory
			.getLog(JecnUnRedoManhattanLineData.class);
	public static final Log log_JecnUnRedoVHLineData = LogFactory
			.getLog(JecnUnRedoVHLineData.class);
	public static final Log log_JecnUnRedoDividingLineData = LogFactory
			.getLog(JecnUnRedoDividingLineData.class);
	// ********************撤销恢复 end********************//

	/** JecnBaseResizePanel 图形编辑点和选中点日志对象 */
	public static final Log LOG_JECN_BASE_RESIZE_PANEL = LogFactory
			.getLog(JecnBaseResizePanel.class);
	/** JecnMoveLineUnit 拖动曼哈顿小线段算法 */
	public static final Log LOG_JECN_MOVE_LINE_UNIT = LogFactory
			.getLog(JecnMoveLineUnit.class);

	/** JecnDraggedFiguresPaintLine 拖动图形执行脱线算法算法 */
	public static final Log LOG_JECN_PAINT_LINE_IF_DRAGGED_FIGURE = LogFactory
			.getLog(JecnDraggedFiguresPaintLine.class);

	/** JecnFigureRefManhattanLine记录图形相关连接线 */
	public static final Log LOG_FIGURE_REF_MANHATTANLINE = LogFactory
			.getLog(JecnFigureRefManhattanLine.class);

	/** JecnFlowResizeActionProcess图形8个方向点 */
	public static final Log LOG_FLOW_RESIZE_PROCESS = LogFactory
			.getLog(JecnFlowResizeActionProcess.class);

	/** JecnFlowOperationData操作说明数据处理类 */
	public static final Log LOG_FLOW_OPERATION_DATA_BEAN = LogFactory
			.getLog(JecnFlowOperationDataUnit.class);

	/** JecnOldFlowImport 流程图导入日志对象 */
	public static final Log LOG_FLOW_IMPORT = LogFactory
			.getLog(JecnFlowOperationDataUnit.class);

	/** JecnPopupMenu象 */
	public static final Log LOG_JecnPopupMenu = LogFactory
			.getLog(JecnPopupMenu.class);

}
