package com.jecn.epros.server.action.designer.project;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.project.JecnProject;

/***
 * 项目管理Action接口
 * @author 2012-05-23
 *
 */
public interface IProjectAction {
	
	/***
	 * 获得所有的项目数据
	 * @return List<JecnProject>
	 * @throws Exception
	 */
	public List<JecnProject> getListProject() throws Exception;
	
	/***
	 * 删除项目
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteProject(List<Long> listIds) throws Exception;
	
	/***
	 * 重命名
	 * @param newName  新名字
	 * @param id	   项目ID
	 */
	public void reProjectName(String newName, long id) throws Exception;
	
	/**
	 * 打开项目
	 * @param proId
	 */
	public void operProject(long proId) throws Exception;
	
	/***
	 * 添加项目
	 * @param jecnProject
	 * @return
	 * @throws Exception
	 */
	public JecnProject addProject(JecnProject jecnProject) throws Exception;
	
	/***
	 * 根据项目ID获取项目信息
	 * @return
	 */
	public JecnProject getProjectById(long proId) throws Exception;
	/**
	 * @author yxw 2012-8-7
	 * @description:获取假删的项目
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getDelsProject() throws Exception;
	/**
	 * 
	 * @author yxw 2012-8-7
	 * @description：恢复已删除的项目
	 * @param id
	 * @throws Exception
	 */
	public void updateRecoverDelProject(List<Long> ids) throws Exception;

	/**
	 * @author yxw 2012-8-7
	 * @description：彻底删除项目
	 * @param setIds
	 * @throws Exception
	 */
	public void deleteIdsProject(Set<Long> setIds) throws Exception;
	/**
	 * @author yxw 2012-9-5
	 * @description:新增判断是否重名
	 * @param name
	 * @return true 重名      false不重名
	 * @throws Exception
	 */
	public boolean validateAddName(String name) throws Exception;
	/**
	 * @author yxw 2012-9-5
	 * @description:修改判断是否重名
	 * @param name
	 * @param id
	 * @return   true 重名      false不重名
	 * @throws Exception
	 */
	public boolean validateUpdateName(String name, long id)throws Exception;
}
