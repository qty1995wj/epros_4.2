package com.jecn.epros.bean;

import java.util.Date;
import java.util.List;

public class TaskReSubmitParam extends BaseTaskParam {

    /**
     * 结束日期
     **/
    private Date endTime;
    /**
     * 任务描述
     **/
    private String taskDesc;

    /**
     * 自定义输入项1
     **/
    private String customInputItemOne;

    /**
     * 自定义输入项2
     **/
    private String customInputItemTwo;

    /**
     * 自定义输入项名称1
     **/
    private String customInputItemThree;


    /**
     * 页面提交新增审批人
     */
    private List<AuditPeopleBean> listAuditPeople;

    public String getCustomInputItemOne() {
        return customInputItemOne;
    }

    public void setCustomInputItemOne(String customInputItemOne) {
        this.customInputItemOne = customInputItemOne;
    }

    public String getCustomInputItemTwo() {
        return customInputItemTwo;
    }

    public void setCustomInputItemTwo(String customInputItemTwo) {
        this.customInputItemTwo = customInputItemTwo;
    }

    public String getCustomInputItemThree() {
        return customInputItemThree;
    }

    public void setCustomInputItemThree(String customInputItemThree) {
        this.customInputItemThree = customInputItemThree;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public List<AuditPeopleBean> getListAuditPeople() {
        return listAuditPeople;
    }

    public void setListAuditPeople(List<AuditPeopleBean> listAuditPeople) {
        this.listAuditPeople = listAuditPeople;
    }


}
