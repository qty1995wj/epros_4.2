package epros.designer.gui.system.combo;

import java.awt.Component;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class KeyValComboBox extends JComboBox {

	public KeyValComboBox(Vector values) {
		super(values);
		rendererData(); // 渲染数据
	}

	public void rendererData() {
		ListCellRenderer render = new DefaultListCellRenderer() {
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				if (value instanceof CheckBoxPo) {
					CheckBoxPo po = (CheckBoxPo) value;
					this.setText(po.value);
				}
				return this;
			}
		};
		this.setRenderer(render);
	}

	public void updateData(Vector values) {
		setModel(new DefaultComboBoxModel(values));
		rendererData();
	}

	@Override
	public void setSelectedItem(Object anObject) { // 选中key与传入的参数相同的项
		if (anObject != null) {
			if (anObject instanceof CheckBoxPo) {
				super.setSelectedItem(anObject);
			}
			if (anObject instanceof String) {
				if (anObject == null) {
					super.setSelectedIndex(0);
				} else {
					for (int index = 0; index < getItemCount(); index++) {
						CheckBoxPo po = (CheckBoxPo) getItemAt(index);
						if (po.key != null && po.key.equals(anObject.toString())) {
							super.setSelectedIndex(index);
							break;
						}
					}
				}
			}
		} else {
			super.setSelectedItem(anObject);
		}
	}

	public void setSelectedValue(Object anObject) { // 选中value与传入的参数相同的项
		if (anObject != null) {
			if (anObject instanceof CheckBoxPo) {
				super.setSelectedItem(anObject);
			}
			if (anObject instanceof String) {
				for (int index = 0; index < getItemCount(); index++) {
					CheckBoxPo po = (CheckBoxPo) getItemAt(index);
					if (po.value.equals(anObject.toString())) {
						super.setSelectedIndex(index);
					}
				}
			}
		} else {
			super.setSelectedItem(anObject);
		}
	}

	// 获得选中项的键值
	public String getSelectedKey() {
		if (getSelectedItem() instanceof CheckBoxPo) {
			CheckBoxPo po = (CheckBoxPo) getSelectedItem();
			return po.key;
		}
		return (getSelectedItem() != null) ? getSelectedItem().toString() : null;
	}

	// 获得选中项的显示文本
	public String getSelectedValue() {
		if (getSelectedItem() instanceof CheckBoxPo) {
			CheckBoxPo po = (CheckBoxPo) getSelectedItem();
			return po.value;
		}
		return (getSelectedItem() != null) ? getSelectedItem().toString() : null;
	}

	// 获得选中的一行数据
	public CheckBoxPo getSelectedObj() {
		if (getSelectedItem() instanceof CheckBoxPo) {
			CheckBoxPo po = (CheckBoxPo) getSelectedItem();
			return po;
		}
		return null;
	}

}
