package epros.draw.gui.figure.shape.total;

import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnModeFigureData;
import epros.draw.event.JecnFormatActionProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;

/**
 * 
 * 泳池 (注：泳池编辑区域为可添加文字的区域)
 * 
 * @author zhangxh
 * @date： 日期：Mar 22, 2012 时间：2:22:05 PM
 */
public class ModelFigure extends JecnBaseFigurePanel {
	/** 是否点击编辑区域 boolean:True 点击的为泳池编辑区域；False 点击的不是泳池的编辑区域 */
	private boolean isChick = false;

	public ModelFigure(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	public JecnModeFigureData getFlowElementData() {
		return (JecnModeFigureData) flowElementData;
	}

	/**
	 * 鼠标点击泳池，判断鼠标点击的位置是否在泳池的编辑区域(注：泳池编辑区域为可添加文字的区域)
	 * 
	 * @param point
	 * @return
	 */
	public boolean isChickEditText(Point point) {
		// 记录泳池分割点X坐标(目前为横向图)
		int dividingX = getZoomDividingX();
		// 横向图比较X坐标
		if (0 < point.x && dividingX > point.x && 0 < point.y && point.y < this.getHeight()) {
			isChick = true;
		} else {
			isChick = false;
		}
		return isChick;
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		paintFigure(g);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 记录泳池分割点X坐标(目前为横向图)
		int dividingX = getZoomDividingX();

		g2d.setStroke(flowElementData.getBasicStroke());

		// 整个泳池：背景渐变、填充
		paintBackColor(g2d);
		g2d.fillRect(0, 0, userWidth, userHeight);

		// 文字显示区域（左侧区域）渐变
		if (FillType.two == flowElementData.getFillType()) {
			g2d.setPaint(new GradientPaint(0, 0, flowElementData.getFillColor(), getZoomDividingX() / 2 + 4, 0,
					flowElementData.getChangeColor()));
		}
		// 文字显示区域（左侧区域）填充
		g2d.fillRect(0, 0, dividingX, userHeight);

		// 获取泳池边框色调
		g2d.setColor(this.getFlowElementData().getBodyColor());
		g2d.drawRect(0, 0, dividingX, userHeight);
		// 画泳池右侧侧，分割点左侧

		g2d.drawRect(dividingX, 0, userWidth - dividingX, userHeight);

	}

	/**
	 * 未填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		// 记录泳池分割点X坐标(目前为横向图)
		int dividingX = getZoomDividingX();
		g2d.setStroke(flowElementData.getBasicStroke());

		g2d.drawRect(0, 0, dividingX, userHeight);
		// 画泳池右侧侧，分割点左侧

		g2d.drawRect(dividingX, 0, userWidth - dividingX, userHeight);
	}

	/**
	 * 泳池克隆对象
	 * 
	 */
	public ModelFigure clone() {
		ModelFigure modelFigure = (ModelFigure) this.currClone();

		return modelFigure;
	}

	public boolean isChick() {
		return isChick;
	}

	public void setChick(boolean isChick) {
		this.isChick = isChick;
	}

	/**
	 * 获取当前面板下放大缩小倍数下的
	 * 
	 * @return
	 */
	public int getZoomDividingX() {
		// 获取当前面板放大缩小倍数分割点X坐标
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		return DrawCommon.convertDoubleToInt(scale * this.getFlowElementData().getDividingX());
	}

	/**
	 * 设置当前面板放大缩小倍数下的泳池分割点值
	 * 
	 * @param zoomDividingX
	 *            当前面板下泳池分割点
	 */
	public void setZommDividingX(int zoomDividingX) {
		// 设置当前面板放大缩小倍数下的泳池分割点值
		this.getFlowElementData().setZommDividingX(zoomDividingX);
	}

	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		if (JecnSystemStaticData.isShowModel()) {
			JecnFormatActionProcess.megerModel();
		}
	}
}
