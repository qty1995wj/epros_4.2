package epros.designer.gui.process.guide.explain;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.PeopleMutilSelectCommon;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionComponent;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.operationConfig.JecnTextArea;
import epros.draw.gui.operationConfig.JecnTextField;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;

public class JecnProcessDriveRule extends JecnFileDescriptionComponent {
	private static Logger log = Logger.getLogger(JecnProcessDriveRule.class);

	/** 流程驱动规则Lab */
	protected JLabel flowDriveRuleLab = new JLabel();

	/** 驱动类型Lab */
	protected JLabel driveTypeLab = new JLabel();

	/** 驱动类型ComboBox */
	protected JComboBox driveTypeCombox = new JComboBox(new String[] { JecnProperties.getValue("eventDriven"),
			JecnProperties.getValue("timeDrive"), JecnProperties.getValue("timeAndEventDriven") });

	/** 驱动规则Lab */
	protected JLabel driveRuleLab = new JLabel();

	/** 驱动规则Field */
	protected JecnTextArea driveRuleArea = new JecnTextArea();
	/** 驱动规则滚动面板 */
	protected JScrollPane driveRuleScrollPane = new JScrollPane(driveRuleArea);

	private boolean isShowTime;

	/** 数据 */
	private JecnFlowBasicInfoT jecnFlowBasicInfoT;

	private JecnFlowDriverT jecnFlowDriverT = null;

	private String tipName = "";

	PeopleMutilSelectCommon peopleSelectCommon = null;

	public JecnProcessDriveRule(int index, String paragrapHeadingName, JecnPanel contentPanel, boolean isRequest,
			JecnFlowBasicInfoT jecnFlowBasicInfoT, JecnDialog jecnDialog, String tip) {
		super(index, paragrapHeadingName, isRequest, contentPanel, tip);
		this.jecnFlowBasicInfoT = jecnFlowBasicInfoT;
		isShowTime = JecnConfigTool.isShowTimeDriverText();
		// 驱动规则
		driveRuleScrollPane.setBorder(null);
		driveRuleScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		driveRuleScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBorder(BorderFactory.createTitledBorder(""));
		JLabel driveTypeLab = new JLabel();
		// 流程驱动类型
		driveTypeLab.setText(JecnProperties.getValue("drivingType"));
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(driveTypeLab, c);
		// driveTypeLab.setBorder(BorderFactory.createTitledBorder(""));
		// 默认显示时间驱动
		driveTypeCombox.setSelectedIndex(1);
		c = new GridBagConstraints(1, 0, 3, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		centerPanel.add(driveTypeCombox, c);
		// 驱动规则
		driveRuleLab.setText(JecnProperties.getValue("driveRules"));
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		centerPanel.add(driveRuleLab, c);

		c = new GridBagConstraints(1, 1, 3, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		centerPanel.add(driveRuleScrollPane, c);
		if (isShowTime) {
			addTimePanel(c, insets);
			List<JecnTreeBean> jecnTreeBeans = null;
			try {
				jecnTreeBeans = ConnectionPool.getProcessAction().getDirverEmailPeopleByFlowId(
						jecnFlowBasicInfoT.getFlowId());
			} catch (Exception e) {
				log.error("JecnProcessDriveRule is error", e);
			}
			peopleSelectCommon = new PeopleMutilSelectCommon(4, centerPanel, insets, jecnTreeBeans, JecnProperties
					.getValue("remindPC"), jecnDialog, false);
		}
		initData();

	}

	/** 类型选择Panel */
	protected JecnPanel typeChangePanel = new JecnPanel(530, 50);

	protected JLabel typeChangeLab = new JLabel(JecnProperties.getValue("typeChoice"));

	/** 类型选择ComboBox */
	protected JComboBox typeChangeCombox = new JComboBox(new String[] { "", JecnProperties.getValue("day"),
			JecnProperties.getValue("weeks"), JecnProperties.getValue("month"), JecnProperties.getValue("season"),
			JecnProperties.getValue("years") });

	/** 类型选择空白 */
	protected JLabel emptyChange = new JLabel();

	/** 开始时间空白 */
	protected JLabel emptyStart = new JLabel();

	/** * 结束时间空白 */
	protected JLabel emptyEnd = new JLabel();

	/**
	 * 开始时间*****************************************************************
	 * Panel
	 */
	protected JecnPanel startTimePanel = new JecnPanel();

	/** 时间Panel */
	protected JecnPanel timePanel = new JecnPanel(540, 50);

	/** 日 类型 */
	protected JLabel startDayLab = new JLabel(JecnProperties.getValue("dayType"));
	/** 开始小时单位 */
	protected JLabel startHourUnitLab = new JLabel(JecnProperties.getValue("when"));
	/** 开始分钟单位 */
	protected JLabel startMinuteUnitLab = new JLabel(JecnProperties.getValue("points"));
	/** 开始秒单位 */
	protected JLabel startSecondUnitLab = new JLabel(JecnProperties.getValue("seconds"));
	protected JComboBox startHourCombox = new JComboBox(returnStrNum(0, 23));
	protected JComboBox startMinuteCombox = new JComboBox(returnStrNum(0, 59));
	protected JComboBox startSecondCombox = new JComboBox(returnStrNum(0, 59));

	/** 周 类型 */
	protected JLabel startWeekLab = new JLabel(JecnProperties.getValue("weeksType"));
	protected JComboBox startWeekCombox = new JComboBox(new String[] { JecnProperties.getValue("Monday"),
			JecnProperties.getValue("onTuesday"), JecnProperties.getValue("wednesday"),
			JecnProperties.getValue("thursday"), JecnProperties.getValue("friday"),
			JecnProperties.getValue("saturday"), JecnProperties.getValue("sunday") });

	/** 月 类型 */
	protected JLabel startMonthLab = new JLabel(JecnProperties.getValue("monthType"));
	/** 开始日 单位 */
	protected JLabel startMonthDayUnitLab = new JLabel(JecnProperties.getValue("day"));
	protected JComboBox startMonthCombox = new JComboBox(returnStrNum(1, 31));
	/** 季 类型 */
	protected JLabel startSeasonLab = new JLabel(JecnProperties.getValue("seasonType"));
	/** 开始季单位 */
	protected JLabel startSeaDayUnitLab = new JLabel(JecnProperties.getValue("day"));
	protected JComboBox startSeasonCombox = new JComboBox(returnStrNum(1, 92));
	/** 年 类型 */
	protected JLabel startYearLab = new JLabel(JecnProperties.getValue("yearsType"));
	/** 年--月 单位 */
	protected JLabel startYearMonthUnitLab = new JLabel(JecnProperties.getValue("month"));
	/** 年--日 单位 */
	protected JLabel startYearDayUnitLab = new JLabel(JecnProperties.getValue("day"));
	protected JComboBox startYearCombox = new JComboBox(returnStrNum(1, 12));
	protected JComboBox startYearDayCombox = new JComboBox(returnStrNum(1, 31));
	/**
	 * 结束时间*******************************************************************
	 * Panel
	 */
	protected JecnPanel endTimePanel = new JecnPanel();
	/** 日 类型 */
	protected JLabel endDayLab = new JLabel(JecnProperties.getValue("dayType"));
	/** 结束小时单位 */
	protected JLabel endHourUnitLab = new JLabel(JecnProperties.getValue("when"));
	/** 结束分钟单位 */
	protected JLabel endMinuteUnitLab = new JLabel(JecnProperties.getValue("points"));
	/** 结束秒单位 */
	protected JLabel endSecondUnitLab = new JLabel(JecnProperties.getValue("seconds"));
	protected JComboBox endHourCombox = new JComboBox(returnStrNum(0, 23));
	protected JComboBox endMinuteCombox = new JComboBox(returnStrNum(0, 59));
	protected JComboBox endSecondCombox = new JComboBox(returnStrNum(0, 59));

	/** 周 类型 */
	protected JLabel endWeekLab = new JLabel(JecnProperties.getValue("weeksType"));
	protected JComboBox endWeekCombox = new JComboBox(new String[] { JecnProperties.getValue("Monday"),
			JecnProperties.getValue("onTuesday"), JecnProperties.getValue("wednesday"),
			JecnProperties.getValue("thursday"), JecnProperties.getValue("friday"),
			JecnProperties.getValue("saturday"), JecnProperties.getValue("sunday") });

	/** 月 类型 */
	protected JLabel endMonthLab = new JLabel(JecnProperties.getValue("monthType"));
	/** 结束日 单位 */
	protected JLabel endMonthDayUnitLab = new JLabel(JecnProperties.getValue("day"));
	protected JComboBox endMonthCombox = new JComboBox(returnStrNum(1, 31));
	/** 季 类型 */
	protected JLabel endSeasonLab = new JLabel(JecnProperties.getValue("seasonType"));
	/** 结束季单位 */
	protected JLabel endSeaDayUnitLab = new JLabel(JecnProperties.getValue("day"));
	protected JComboBox endSeanCombox = new JComboBox(returnStrNum(1, 92));

	/** 年 类型 */
	protected JLabel endYearLab = new JLabel(JecnProperties.getValue("yearsType"));
	/** 结束年--月 单位 */
	protected JLabel endYearMonthUnitLab = new JLabel(JecnProperties.getValue("month"));
	/** 结束年--日 单位 */
	protected JLabel endYearDayUnitLab = new JLabel(JecnProperties.getValue("day"));
	protected JComboBox endYearCombox = new JComboBox(returnStrNum(1, 12));
	protected JComboBox endYearDayCombox = new JComboBox(returnStrNum(1, 31));

	/** 频率Lab */
	protected JLabel frequencyLab = new JLabel();

	/** 频率Field */
	protected JecnTextField frequencyField = new JecnTextField(100);

	/** 频率次数Lab */
	protected JLabel orderLab = new JLabel();

	/**
	 * 流程文件：时间驱动，时间类型和时间选择项
	 * 
	 * @return int
	 * @author cheshaowei
	 * @date 2015-11-3 下午03:23:04
	 */
	private void addTimePanel(GridBagConstraints c, Insets insets) {

		typeChangeCombox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				typeChangePerformed(e);
			}
		});

		Insets insetp = new Insets(0, 0, 0, 0);
		c = new GridBagConstraints(0, 2, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insetp, 0,
				0);
		centerPanel.add(typeChangePanel, c);
		typeChangePanel.setLayout(new GridBagLayout());
		typeChangePanel.setBorder(BorderFactory.createTitledBorder(""));
		// typeChangeLab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		typeChangePanel.add(typeChangeLab, c);
		// typeChangeCombox
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		typeChangePanel.add(typeChangeCombox, c);

		// emptyChange
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		typeChangePanel.add(emptyChange, c);

		// 频率 frequencyLab
		frequencyLab.setText(JecnProperties.getValue("frequency"));
		orderLab.setText(JecnProperties.getValue("number"));
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		typeChangePanel.add(frequencyLab, c);
		c = new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		typeChangePanel.add(frequencyField, c);
		c = new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		typeChangePanel.add(orderLab, c);
		// 时间Panel timePanel
		// 开始时间***************************************************Panel
		// startTimePanel
		c = new GridBagConstraints(0, 3, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insetp, 0,
				0);
		centerPanel.add(timePanel, c);
		timePanel.setLayout(new GridBagLayout());

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insetp, 0, 0);
		timePanel.add(startTimePanel, c);
		startTimePanel.setLayout(new GridBagLayout());
		startTimePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("startTime")));
		// 结束时间**********************************************************Panel
		// endTimePanel
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insetp, 0, 0);
		timePanel.add(endTimePanel, c);
		endTimePanel.setLayout(new GridBagLayout());
		endTimePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("endTime")));
	}

	private void initData() {
		if (jecnFlowBasicInfoT.getDriveType() != null) {
			if (jecnFlowBasicInfoT.getDriveType() == 0) {
				driveTypeCombox.setSelectedIndex(0);
			} else if (jecnFlowBasicInfoT.getDriveType() == 1) {
				driveTypeCombox.setSelectedIndex(1);
			} else {
				driveTypeCombox.setSelectedIndex(2);
			}
		}

		driveRuleArea.setRows(4);

		driveRuleArea
				.setText(this.jecnFlowBasicInfoT.getDriveRules() == null ? "" : jecnFlowBasicInfoT.getDriveRules());
		try {
			jecnFlowDriverT = ConnectionPool.getProcessAction().getJecnFlowDriverT(jecnFlowBasicInfoT.getFlowId());
			if (jecnFlowDriverT == null) {
				jecnFlowDriverT = new JecnFlowDriverT();
			}
		} catch (Exception e) {
			log.error("jecnProcessDriveRule initData is error", e);
		}
		if (jecnFlowDriverT != null) {
			// 类型选择选中项设置
			String quantity = null;
			// 频率
			String strFrequency = null;
			quantity = jecnFlowDriverT.getQuantity();
			strFrequency = jecnFlowDriverT.getFrequency();
			if (quantity != null && !"".equals(quantity)) {
				if ("1".equals(quantity)) {// 日
					typeChangeCombox.setSelectedIndex(1);
				} else if ("2".equals(quantity)) {// 周
					typeChangeCombox.setSelectedIndex(2);
				} else if ("3".equals(quantity)) {// 月
					typeChangeCombox.setSelectedIndex(3);
				} else if ("4".equals(quantity)) {// 季度
					typeChangeCombox.setSelectedIndex(4);
				} else if ("5".equals(quantity)) {// 年
					typeChangeCombox.setSelectedIndex(5);
				}
			}
			// 开始时间
			String startTime = jecnFlowDriverT.getStartTime();
			// 结束时间
			String endTime = jecnFlowDriverT.getEndTime();
			if (startTime != null && endTime != null) {
				if (typeChangeCombox.getSelectedIndex() == 1) {
					String[] startTimeStr = startTime.split(",");
					startHourCombox.setSelectedIndex(Integer.valueOf(startTimeStr[0]));
					startMinuteCombox.setSelectedIndex(Integer.valueOf(startTimeStr[1]));
					startSecondCombox.setSelectedIndex(Integer.valueOf(startTimeStr[2]));

					String[] endTimeStr = endTime.split(",");
					endHourCombox.setSelectedIndex(Integer.valueOf(endTimeStr[0]));
					endMinuteCombox.setSelectedIndex(Integer.valueOf(endTimeStr[1]));
					endSecondCombox.setSelectedIndex(Integer.valueOf(endTimeStr[2]));

				} else if (typeChangeCombox.getSelectedIndex() == 2) {
					startWeekCombox.setSelectedIndex(Integer.valueOf(startTime));
					endWeekCombox.setSelectedIndex(Integer.valueOf(endTime));
				} else if (typeChangeCombox.getSelectedIndex() == 3) {
					startMonthCombox.setSelectedIndex(Integer.valueOf(startTime));
					endMonthCombox.setSelectedIndex(Integer.valueOf(endTime));
				} else if (typeChangeCombox.getSelectedIndex() == 4) {
					startSeasonCombox.setSelectedIndex(Integer.valueOf(startTime));
					endSeanCombox.setSelectedIndex(Integer.valueOf(endTime));
				} else if (typeChangeCombox.getSelectedIndex() == 5) {
					String[] startTimeStr = startTime.split(",");
					startYearCombox.setSelectedIndex(Integer.valueOf(startTimeStr[0]));
					startYearDayCombox.setSelectedIndex(Integer.valueOf(startTimeStr[1]));

					String[] endTimeStr = endTime.split(",");
					endYearCombox.setSelectedIndex(Integer.valueOf(endTimeStr[0]));
					endYearDayCombox.setSelectedIndex(Integer.valueOf(endTimeStr[1]));
				}
			}
			frequencyField.setText(strFrequency);
		}
	}

	/**
	 * 驱动类型选择
	 */
	protected void driveTypeComboxPerformed(ItemEvent e) {
		if (e.getStateChange() == e.SELECTED) {
			if (driveTypeCombox.getSelectedIndex() == 0 || driveTypeCombox.getSelectedIndex() == 2) {
				typeChangeCombox.setEnabled(false);
				frequencyField.setEditable(false);
			} else {
				typeChangeCombox.setEnabled(true);
				frequencyField.setEditable(true);
			}
		}
	}

	private String[] returnStrNum(int startNum, int endNum) {
		List<Integer> strList = new ArrayList<Integer>();
		for (int i = startNum; i <= endNum; i++) {
			strList.add(i);
		}
		Object[] objs = strList.toArray();
		String[] strs = new String[objs.length];
		for (int j = 0; j < objs.length; j++) {
			strs[j] = objs[j].toString();
		}
		return strs;
	}

	protected void addRowToPanel(JecnPanel panel, JComponent leftLabel, int colsp) {
		GridBagConstraints c = new GridBagConstraints(colsp, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0);
		panel.add(leftLabel, c);
	}

	/**
	 * 时间 类型选择
	 */
	protected void typeChangePerformed(ItemEvent e) {

		if (e.getStateChange() == e.SELECTED) {
			// 数据库类型
			String getItemSel = typeChangeCombox.getSelectedItem().toString();
			// 日
			startDayLab.setVisible(false);
			startHourCombox.setVisible(false);
			startMinuteCombox.setVisible(false);
			startSecondCombox.setVisible(false);

			endDayLab.setVisible(false);
			endHourCombox.setVisible(false);
			endMinuteCombox.setVisible(false);
			endSecondCombox.setVisible(false);

			// 周
			startWeekLab.setVisible(false);
			startWeekCombox.setVisible(false);
			endWeekLab.setVisible(false);
			endWeekCombox.setVisible(false);
			// 月
			startMonthLab.setVisible(false);
			startMonthCombox.setVisible(false);
			endMonthLab.setVisible(false);
			endMonthCombox.setVisible(false);
			// 季
			startSeasonLab.setVisible(false);
			startSeasonCombox.setVisible(false);
			endSeasonLab.setVisible(false);
			endSeanCombox.setVisible(false);
			// 年
			startYearLab.setVisible(false);
			startYearCombox.setVisible(false);
			startYearDayCombox.setVisible(false);
			endYearLab.setVisible(false);
			endYearCombox.setVisible(false);
			endYearDayCombox.setVisible(false);
			// 单位名称
			// 日
			startHourUnitLab.setVisible(false);
			startMinuteUnitLab.setVisible(false);
			startSecondUnitLab.setVisible(false);

			endHourUnitLab.setVisible(false);
			endMinuteUnitLab.setVisible(false);
			endSecondUnitLab.setVisible(false);
			// 月
			startMonthDayUnitLab.setVisible(false);
			endMonthDayUnitLab.setVisible(false);
			// 季
			startSeaDayUnitLab.setVisible(false);
			endSeaDayUnitLab.setVisible(false);
			// 年
			startYearMonthUnitLab.setVisible(false);
			startYearDayUnitLab.setVisible(false);
			endYearMonthUnitLab.setVisible(false);
			endYearDayUnitLab.setVisible(false);

			String getQua = null;
			String[] strStarts = null;
			String[] strEnds = null;
			if (typeChangeCombox.getItemAt(1).equals(getItemSel)) {// 日
				// 日类型
				startDayLab.setVisible(true);
				startHourCombox.setVisible(true);
				startMinuteCombox.setVisible(true);
				startSecondCombox.setVisible(true);

				endDayLab.setVisible(true);
				endHourCombox.setVisible(true);
				endMinuteCombox.setVisible(true);
				endSecondCombox.setVisible(true);

				startHourUnitLab.setVisible(true);
				startMinuteUnitLab.setVisible(true);
				startSecondUnitLab.setVisible(true);

				endHourUnitLab.setVisible(true);
				endMinuteUnitLab.setVisible(true);
				endSecondUnitLab.setVisible(true);

				// 赋值 jecnFlowDriverT

				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(1).equals(getQua)) {
						int intStartHour = 0;
						int intEndHour = 0;
						int intStartMinute = 0;
						int intEndMinute = 0;
						int intStartSecond = 0;
						int intEndSecond = 0;
						// 时
						// 时
						intStartHour = Integer.parseInt(strStarts[0].toString());
						intEndHour = Integer.parseInt(strEnds[0].toString());
						// 分
						intStartMinute = Integer.parseInt(strStarts[1].toString());
						intEndMinute = Integer.parseInt(strEnds[1].toString());
						// 秒
						intStartSecond = Integer.parseInt(strStarts[2].toString());
						intEndSecond = Integer.parseInt(strEnds[2].toString());

						startHourCombox.setSelectedIndex(intStartHour);
						startMinuteCombox.setSelectedIndex(intStartMinute);
						startSecondCombox.setSelectedIndex(intStartSecond);

						endHourCombox.setSelectedIndex(intEndHour);
						endMinuteCombox.setSelectedIndex(intEndMinute);
						endSecondCombox.setSelectedIndex(intEndSecond);

					}
				}
				addRowToPanel(startTimePanel, startDayLab, 0);
				addRowToPanel(startTimePanel, startHourCombox, 1);
				// 时 单位
				addRowToPanel(startTimePanel, startHourUnitLab, 2);
				addRowToPanel(startTimePanel, startMinuteCombox, 3);
				// 分 单位endHourUnitLab
				addRowToPanel(startTimePanel, startMinuteUnitLab, 4);
				addRowToPanel(startTimePanel, startSecondCombox, 5);
				// 秒 单位endHourUnitLab
				addRowToPanel(startTimePanel, startSecondUnitLab, 6);

				addRowToPanel(endTimePanel, endDayLab, 0);
				addRowToPanel(endTimePanel, endHourCombox, 1);
				// 时 单位endHourUnitLab
				addRowToPanel(endTimePanel, endHourUnitLab, 2);
				addRowToPanel(endTimePanel, endMinuteCombox, 3);
				// 分 单位endHourUnitLab
				addRowToPanel(endTimePanel, endMinuteUnitLab, 4);
				addRowToPanel(endTimePanel, endSecondCombox, 5);
				// 秒 单位endHourUnitLab
				addRowToPanel(endTimePanel, endSecondUnitLab, 6);

			} else if (typeChangeCombox.getItemAt(2).equals(getItemSel)) {// 周

				// 周类型 startWeekLab
				startWeekLab.setVisible(true);
				startWeekCombox.setVisible(true);
				endWeekLab.setVisible(true);
				endWeekCombox.setVisible(true);
				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(2).equals(getQua)) {

						int intStart = Integer.parseInt(strStarts[0]);
						int intEnd = Integer.parseInt(strEnds[0]);
						// 开始周
						startWeekCombox.setSelectedIndex(intStart);
						// 结束周
						endWeekCombox.setSelectedIndex(intEnd);
					}
				}
				addRowToPanel(startTimePanel, startWeekLab, 0);
				addRowToPanel(startTimePanel, startWeekCombox, 1);
				// 结束
				addRowToPanel(endTimePanel, endWeekLab, 0);
				addRowToPanel(endTimePanel, endWeekCombox, 1);
			} else if (typeChangeCombox.getItemAt(3).equals(getItemSel)) {// 月
				// 月类型 startMonthLab
				startMonthLab.setVisible(true);
				startMonthCombox.setVisible(true);
				endMonthLab.setVisible(true);
				endMonthCombox.setVisible(true);
				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(3).equals(getQua)) {// 月
						int intStart = Integer.parseInt(strStarts[0]);
						int intEnd = Integer.parseInt(strEnds[0]);
						startMonthCombox.setSelectedIndex(intStart);
						endMonthCombox.setSelectedIndex(intEnd);
					}
				}

				startMonthDayUnitLab.setVisible(true);
				endMonthDayUnitLab.setVisible(true);

				addRowToPanel(startTimePanel, startMonthLab, 0);
				addRowToPanel(startTimePanel, startMonthCombox, 1);
				addRowToPanel(startTimePanel, startMonthDayUnitLab, 2);
				addRowToPanel(endTimePanel, endMonthLab, 0);
				addRowToPanel(endTimePanel, endMonthCombox, 1);
				addRowToPanel(endTimePanel, endMonthDayUnitLab, 2);
			} else if (typeChangeCombox.getItemAt(4).equals(getItemSel)) {// 季度
				// 季类型
				startSeasonLab.setVisible(true);
				startSeasonCombox.setVisible(true);
				endSeasonLab.setVisible(true);
				endSeanCombox.setVisible(true);
				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(4).equals(getQua)) {
						int intStart = Integer.parseInt(strStarts[0]);
						int intEnd = Integer.parseInt(strEnds[0]);
						startSeasonCombox.setSelectedIndex(intStart);
						endSeanCombox.setSelectedIndex(intEnd);
					}
				}
				startSeaDayUnitLab.setVisible(true);
				endSeaDayUnitLab.setVisible(true);

				addRowToPanel(startTimePanel, startSeasonLab, 0);
				addRowToPanel(startTimePanel, startSeasonCombox, 1);
				addRowToPanel(startTimePanel, startSeaDayUnitLab, 2);
				addRowToPanel(endTimePanel, endSeasonLab, 0);
				addRowToPanel(endTimePanel, endSeanCombox, 1);
				addRowToPanel(endTimePanel, endSeaDayUnitLab, 2);
			} else if (typeChangeCombox.getItemAt(5).equals(getItemSel)) {// 年
				// 年类型
				startYearLab.setVisible(true);
				startYearCombox.setVisible(true);
				startYearDayCombox.setVisible(true);
				endYearLab.setVisible(true);
				endYearCombox.setVisible(true);
				endYearDayCombox.setVisible(true);

				// 赋值 jecnFlowDriverT
				if (getQua != null && !"".equals(getQua)) {
					if (typeChangeCombox.getItemAt(5).equals(getQua)) {
						int intStartMonth = 0;
						int intEndMonth = 0;
						int intStartDay = 0;
						int intEndDay = 0;
						// 开始月
						intStartMonth = Integer.parseInt(strStarts[0]);
						// 结束月
						intEndMonth = Integer.parseInt(strEnds[0]);
						// 开始天
						intStartDay = Integer.parseInt(strStarts[1]);
						// 结束天
						intEndDay = Integer.parseInt(strEnds[1]);
						startYearCombox.setSelectedIndex(intStartMonth);
						startYearDayCombox.setSelectedIndex(intStartDay);

						endYearCombox.setSelectedIndex(intEndMonth);
						endYearDayCombox.setSelectedIndex(intEndDay);
					}
				}

				startYearMonthUnitLab.setVisible(true);
				startYearDayUnitLab.setVisible(true);
				endYearMonthUnitLab.setVisible(true);
				endYearDayUnitLab.setVisible(true);

				addRowToPanel(startTimePanel, startYearLab, 0);
				addRowToPanel(startTimePanel, startYearCombox, 1);
				addRowToPanel(startTimePanel, startYearMonthUnitLab, 2);
				addRowToPanel(startTimePanel, startYearDayCombox, 3);
				addRowToPanel(startTimePanel, startYearDayUnitLab, 4);
				addRowToPanel(endTimePanel, endYearLab, 0);
				addRowToPanel(endTimePanel, endYearCombox, 1);
				addRowToPanel(endTimePanel, endYearMonthUnitLab, 2);
				addRowToPanel(endTimePanel, endYearDayCombox, 3);
				addRowToPanel(endTimePanel, endYearDayUnitLab, 4);
			}
			startTimePanel.validate();
			startTimePanel.repaint();
			endTimePanel.validate();
			endTimePanel.repaint();
		}
	}

	/***
	 * 年类型：切换月份时，对应日类型数据显示
	 * 
	 * @param e
	 * @param isStart
	 *            ：0：开始月份；1：结束月份
	 */
	protected void yearComboxPerformed(ItemEvent e, int isStart) {
		if (e.getStateChange() == e.SELECTED) {
			returnStrNumYear(1, 31, isStart);
		}
	}

	protected void returnStrNumYear(int startNum, int endNum, int isStart) {
		List<Integer> strList = new ArrayList<Integer>();
		int yearInt = 0;
		if (isStart == 0) {// 开始月份
			yearInt = startYearCombox.getSelectedIndex();
		} else if (isStart == 1) {// 结束月份
			yearInt = endYearCombox.getSelectedIndex();
		}
		if (yearInt == 0 || yearInt == 2 || yearInt == 4 || yearInt == 6 || yearInt == 7 || yearInt == 9
				|| yearInt == 11) {// 1、3、5、7、8、10、12月
			endNum = 31;
		} else if (yearInt == 3 || yearInt == 5 || yearInt == 8 || yearInt == 10) {// 4、6、9、11月
			endNum = 30;
		} else if (yearInt == 1) {// 2月
			endNum = 29;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
			int date = Integer.parseInt(sdf.format(new Date()));
			if ((date % 4 == 0 && date % 100 != 0) || date % 400 == 0) {// 闰年
				endNum = 29;
			} else {
				endNum = 28;
			}
		}
		for (int i = startNum; i <= endNum; i++) {
			strList.add(i);
		}
		Object[] objs = strList.toArray();
		String[] strs = new String[objs.length];
		for (int j = 0; j < objs.length; j++) {
			// strs[j] = objs[j].toString();
			if (isStart == 0) {// 开始月份
				startYearDayCombox.addItem(objs[j].toString());
			} else if (isStart == 1) {// 结束月份
				endYearDayCombox.addItem(objs[j].toString());
			}
		}
		// return strs;
	}

	public boolean isUpdate() {
		JecnFlowDriverT jecnFlowDriver = this.getJecnFlowDriver();
		// 流程驱动类型
		if (!DrawCommon.checkStringSame(String.valueOf(driveTypeCombox.getSelectedIndex()), String
				.valueOf(jecnFlowBasicInfoT.getDriveType() == null ? 1 : jecnFlowBasicInfoT.getDriveType()))) {
			return true;
		}
		if (!DrawCommon.checkStringSame(driveRuleArea.getText(), jecnFlowBasicInfoT.getDriveRules())) {
			return true;
		}
		if (this.isShowTime) {
			if (!DrawCommon.checkStringSame(jecnFlowDriver.getStartTime(), jecnFlowDriverT.getStartTime())) {
				return true;
			}

			if (!DrawCommon.checkStringSame(jecnFlowDriver.getEndTime(), jecnFlowDriverT.getEndTime())) {
				return true;
			}

			if (!DrawCommon.checkStringSame(jecnFlowDriver.getFrequency(), jecnFlowDriverT.getFrequency())) {
				return true;
			}

			if (isFlowDriver(jecnFlowDriver.getQuantity(), jecnFlowDriverT.getQuantity())) {
				return true;
			}
			if (peopleSelectCommon != null) {
				if (peopleSelectCommon.isUpdate()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 判断操作说明 驱动规则 类型是否相等
	 * 
	 * @author fuzhh 2013-7-11
	 * @param quantityNow
	 *            本地 类型
	 * @param quantityDb
	 *            数据库存储类型
	 * @return
	 */
	private boolean isFlowDriver(String quantityNow, String quantityDb) {
		if ("0".equals(quantityNow)) {
			if (DrawCommon.isNullOrEmtryTrim(quantityDb) || "0".equals(quantityDb)) {
				return false;
			} else {
				return true;
			}
		} else {
			if (DrawCommon.checkStringSame(quantityNow, quantityDb)) {
				return false;
			} else {
				return true;
			}
		}
	}

	public boolean validateStr(JLabel verfyLab) {
		if (DrawCommon.checkNoteLength(driveRuleArea.getText().toString())) {
			verfyLab.setText(tipName + JecnProperties.getValue("lengthNotOut"));
			return true;
		}
		return false;
	}

	public String getDriveRules() {
		return this.driveRuleArea.getText().trim();
	}

	public Long getDriveType() {
		return Long.valueOf(driveTypeCombox.getSelectedIndex());
	}

	private Set<Long> getDriverEmailPeopleId() {
		if (peopleSelectCommon == null) {
			return new HashSet<Long>();
		}
		return this.peopleSelectCommon.getResultIdsSet();
	}

	/**
	 * 获取驱动规则数据
	 * 
	 * @author fuzhh Jan 7, 2013
	 * @return
	 */
	public JecnFlowDriverT getJecnFlowDriver() {
		JecnFlowDriverT jecnFlowDriver = new JecnFlowDriverT();
		jecnFlowDriver.setFlowId(jecnFlowBasicInfoT.getFlowId());
		if (this.isShowTime) {
			/** *******************更新流程驱动规则临时表数据********************* */
			String strQuantity = String.valueOf(typeChangeCombox.getSelectedIndex());
			jecnFlowDriver.setQuantity(strQuantity);
			jecnFlowDriver.setFrequency(frequencyField.getText());
			String strStartSelected = null;
			String strEndSelected = null;
			// 年--天 ///日类型--时
			String startYeaDaySel = null;
			// 年---天 //日类型---分
			String endYeaDaySel = null;
			String startSecSel = null;
			String endSecSel = null;
			// 根据索引获取类型
			if ("1".equals(strQuantity)) {
				// 开始
				strStartSelected = startHourCombox.getSelectedIndex() + ",";// 时
				startYeaDaySel = startMinuteCombox.getSelectedIndex() + ",";// 分
				startSecSel = String.valueOf(startSecondCombox.getSelectedIndex());// 秒
				// 结束
				strEndSelected = endHourCombox.getSelectedIndex() + ","; // 时
				endYeaDaySel = endMinuteCombox.getSelectedIndex() + ",";// 分
				endSecSel = String.valueOf(endSecondCombox.getSelectedIndex());// 秒

				jecnFlowDriver.setStartTime(strStartSelected + startYeaDaySel + startSecSel);
				jecnFlowDriver.setEndTime(strEndSelected + endYeaDaySel + endSecSel);

			} else if ("2".equals(strQuantity)) {
				jecnFlowDriver.setStartTime(String.valueOf(startWeekCombox.getSelectedIndex()));
				jecnFlowDriver.setEndTime(String.valueOf(endWeekCombox.getSelectedIndex()));
			} else if ("3".equals(strQuantity)) {
				jecnFlowDriver.setStartTime(String.valueOf(startMonthCombox.getSelectedIndex()));
				jecnFlowDriver.setEndTime(String.valueOf(endMonthCombox.getSelectedIndex()));
			} else if ("4".equals(strQuantity)) {
				jecnFlowDriver.setStartTime(String.valueOf(startSeasonCombox.getSelectedIndex()));
				jecnFlowDriver.setEndTime(String.valueOf(endSeanCombox.getSelectedIndex()));
			} else if ("5".equals(strQuantity)) {
				// 开始
				strStartSelected = startYearCombox.getSelectedIndex() + ",";// 月
				startYeaDaySel = String.valueOf(startYearDayCombox.getSelectedIndex());// 天
				// 结束
				strEndSelected = endYearCombox.getSelectedIndex() + ","; // 月
				endYeaDaySel = String.valueOf(endYearDayCombox.getSelectedIndex());// 天
				jecnFlowDriver.setStartTime(strStartSelected + startYeaDaySel);
				jecnFlowDriver.setEndTime(strEndSelected + endYeaDaySel);
			}
			jecnFlowDriver.setDriverEmailPeopleIds(getDriverEmailPeopleId());
		}
		return jecnFlowDriver;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return JecnValidateCommon.validateAreaNotPass(driveRuleArea.getText(), verfyLab, JecnProperties
				.getValue("driveRule"));
	}

}
