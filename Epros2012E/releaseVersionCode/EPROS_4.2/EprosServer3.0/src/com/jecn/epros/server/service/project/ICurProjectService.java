package com.jecn.epros.server.service.project;

import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.common.IBaseService;

/**
 * 项目管理  设置主项目
 * @author 2012-05-23
 *
 */
public interface ICurProjectService extends IBaseService<JecnCurProject, Long> {
	
	/**
	 * 设置主项目
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public boolean updateCurProject(Long projectId) throws Exception;
	
	/**
	 * 根据项目Id查询主项目表信息
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public JecnCurProject getJecnCurProjectById() throws Exception;		

}
