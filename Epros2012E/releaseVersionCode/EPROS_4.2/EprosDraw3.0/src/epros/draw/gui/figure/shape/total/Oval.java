/*
 * Oval.java
 *椭圆
 * Created on 2008年11月3日, 上午11:27
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.total;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseOval;

/**
 * 
 * 流程地图圆形
 * 
 * @author zhangxh
 * @date： 日期：Mar 22, 2012 时间：4:57:48 PM
 */
public class Oval extends BaseOval {

	public Oval(JecnFigureData figureData) {
		super(figureData);
	}
}
