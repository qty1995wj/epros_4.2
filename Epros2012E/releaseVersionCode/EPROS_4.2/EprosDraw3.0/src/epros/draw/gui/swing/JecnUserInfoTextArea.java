package epros.draw.gui.swing;

import java.awt.Color;

import javax.swing.JTextArea;

import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 用户提示信息显示的面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnUserInfoTextArea extends JTextArea {

	public JecnUserInfoTextArea() {
		initComponents();
	}

	private void initComponents() {
		// 界面不可编辑
		this.setEditable(false);
		// 支持换行
		this.setLineWrap(true);
		// 初始化隐藏
		this.setVisible(false);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 前景颜色
		this.setForeground(Color.red);
	}

	/**
	 * 
	 * 在名称输入框中输入字符时，验证内容是否正确
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	public boolean checkInfo(String info) {
		if (!DrawCommon.isNullOrEmtryTrim(info)) {// 不为空
			// 显示提示框、添加提示框内容
			if (!this.isVisible()) {
				this.setVisible(true);
			}
			this.setText(info);

			return false;
		}

		// 隐藏提示框、清除提示框内容
		if (this.isVisible()) {
			this.setVisible(false);
		}
		this.setText("");
		return true;
	}
}
