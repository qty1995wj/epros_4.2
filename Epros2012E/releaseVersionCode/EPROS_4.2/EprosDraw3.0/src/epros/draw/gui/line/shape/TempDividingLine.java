package epros.draw.gui.line.shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import javax.swing.JPanel;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.gui.figure.shape.JecnEditPortPanel;

/**
 * 
 * 画连接线时临时线
 * 
 * @author Administrator
 * 
 */
public class TempDividingLine extends JPanel {
	/** 临时线段开始点 */
	private Point startPoint = null;
	/** 临时线段结束点 */
	private Point endPoint = null;

	/** 临时线段记录线对应的图形 */
	private JecnEditPortPanel startPort;

	/** 临时线段记录线对应的图形 */
	private JecnEditPortPanel endPort;
	/** 分割线虚拟线段 */
	private BasicStroke basicStroke = new BasicStroke(1);
	LineDirectionEnum directionEnum = null;

	public TempDividingLine(Point startPoint, Point endPoint) {
		this.startPoint = startPoint;
		this.startPoint = startPoint;
	}

	public TempDividingLine() {

	}

	/**
	 * 画线
	 * 
	 * @param g
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// 画笔
		Graphics2D g2d = (Graphics2D) g;
		int intX = this.getWidth();
		int intY = this.getHeight();
		g2d.setColor(Color.WHITE);
		g2d.setStroke(getBasicStroke());// 
		// 设置虚线用
		g2d.setColor(Color.BLACK);
		switch (directionEnum) {
		case horizontal:
			g2d.fillRect(0, 0, intX, intY - 1);
			g2d.drawRect(0, 0, intX, intY - 1);
			break;
		case vertical:
			// 竖线
			g2d.fillRect(0, 0, intX - 1, intY);
			g2d.drawRect(0, 0, intX - 1, intY);
			break;
		default:
			break;
		}
	}

	/**
	 * 画线
	 * 
	 * @param g
	 */
	public void paintDiveLine(Graphics g) {
		Graphics2D g2d = (Graphics2D) g; // 定义画笔
		g2d.setStroke(getBasicStroke());// 
		// 设置虚线用
		g2d.setColor(Color.BLACK);
		g2d.drawLine(this.getStartPoint().x, this.getStartPoint().y, this
				.getEndPoint().x, this.getEndPoint().y);
	}

	/**
	 * 设置横向分隔符位置点
	 * 
	 * @param startPoint
	 * @param endPoint
	 */
	public void setBounds(Point startPoint, Point endPoint,
			LineDirectionEnum directionEnum) {
		this.directionEnum = directionEnum;
		if (startPoint == null || endPoint == null) {
			return;
		}
		// 线条宽度
		int lineSizeWidth = 2;
		// 纵向分隔符 开始点坐标
		int startX = startPoint.x;
		int startY = startPoint.y;
		// 纵向分隔符 结束点坐标
		int endX = endPoint.x;
		int endY = endPoint.y;
		switch (directionEnum) {
		case horizontal:
			// 设置位置
			this.setBounds(Math.min(startX, endX) - lineSizeWidth / 2, endY
					- lineSizeWidth / 2, Math.abs(startX - endX)
					+ lineSizeWidth, lineSizeWidth);
			break;
		case vertical:
			this.setBounds(startX - lineSizeWidth / 2, Math.min(startY, endY),
					lineSizeWidth, Math.abs(startY - endY));
			break;
		default:
			break;
		}

	}

	public Point getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}

	public Point getEndPoint() {
		return endPoint;
	}

	public JecnEditPortPanel getStartPort() {
		return startPort;
	}

	public void setStartPort(JecnEditPortPanel startPort) {
		this.startPort = startPort;
	}

	public JecnEditPortPanel getEndPort() {
		return endPort;
	}

	public void setEndPort(JecnEditPortPanel endPort) {
		this.endPort = endPort;
	}

	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
	}

	/**
	 * 获取画笔 点线
	 * 
	 * @return
	 */
	public BasicStroke getBasicStroke() {
		float[] pointLineSpace = { 2.0f * 1 };
		basicStroke = new BasicStroke(1, BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_MITER, 1.0f, pointLineSpace, 0.0f);
		return basicStroke;
	}

	/**
	 * 拖动分割线端点到达边界处理
	 * 
	 * @param curLine
	 *            JecnBaseDividingLinePanel当前分割线
	 * @param tempPoint
	 *            Point鼠标拖动后的位移坐标
	 */
	public void draggedTempLinePoint(Point tempPoint) {
		if (tempPoint == null) {
			return;
		}
		Point tempStartPos = null;
		Point tempEndPos = null;
		// 开始点处理
		if (this.getStartPoint().x + tempPoint.x > 0
				&& this.getStartPoint().y + tempPoint.y > 0) {// 开始点在面板内（未到达边界）
			tempStartPos = new Point(this.getStartPoint().x + tempPoint.x, this
					.getStartPoint().y
					+ tempPoint.y);
		} else if (this.getStartPoint().x + tempPoint.x <= 0) {// 开始点X坐标到达边界处理
			tempStartPos = new Point(0, this.getStartPoint().y + tempPoint.y);
		} else if (this.getStartPoint().y + tempPoint.y <= 0) {// 开始点Y坐标到达边界处理
			tempStartPos = new Point(this.getStartPoint().x + tempPoint.x, 0);
		}
		if (this.getEndPoint().x + tempPoint.x > 0
				&& this.getEndPoint().y + tempPoint.y > 0) {// 结束点在面板内处理
			tempEndPos = new Point(this.getEndPoint().x + tempPoint.x, this
					.getEndPoint().y
					+ tempPoint.y);
		} else if (this.getEndPoint().x + tempPoint.x <= 0) {// 结束点X坐标到达边界处理
			tempEndPos = new Point(0, this.getEndPoint().y + tempPoint.y);
		} else if (this.getEndPoint().y + tempPoint.y <= 0) {// 结束点Y坐标到达边界处理
			tempEndPos = new Point(this.getEndPoint().x + tempPoint.x, 0);
		}
		if (tempStartPos == null || tempEndPos == null) {
			return;
		}
		// 虚拟线段获取新的开始点和结束点（根据拖动的唯一坐标获取）
		this.setStartPoint(tempStartPos);
		this.setEndPoint(tempEndPos);
	}
}
