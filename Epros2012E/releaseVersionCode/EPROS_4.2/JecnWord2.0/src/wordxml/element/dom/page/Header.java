package wordxml.element.dom.page;

import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.element.dom.IBody;
import wordxml.element.dom.paragraph.AntiFake;

/**
 * 页眉
 * 
 * @author admin
 * 
 */
public class Header extends IBody {
	/** 所属节 */
	private Sect parent;
	/** 类型 */
	private HeaderOrFooterType type;

	protected Header(Sect sect, HeaderOrFooterType type) {
		this.parent = sect;
		setType(type);
	}

	public Sect getParent() {
		return parent;
	}

	public void setParent(Sect parent) {
		this.parent = parent;
	}

	public HeaderOrFooterType getType() {
		return type;
	}

	public void setType(HeaderOrFooterType type) {
		if (type == null) {
			return;
		}
		if (type.equals(HeaderOrFooterType.first)) {
			parent.getSectBean().setTitlePg(true);
		}
		this.type = type;
	}

	public void addAntiFake(AntiFake antiFake) {
		super.bodyElements.add(antiFake);
	}

}
