package com.jecn.epros.bean.checkout;

import java.io.Serializable;
import java.util.List;

public class CheckoutRoleItem implements Serializable {
    private Long figureId;
    private String figureText;
    private Long posId;
    private String posName;
    /**
     * 0 岗位 1岗位组
     */
    private int type;
    /**
     * 和岗位岗位组有关联关系
     */
    private boolean related;
    /**
     * 岗位或者岗位组与角色名称是否相同
     */
    private boolean fpEqual;

    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    public String getFigureText() {
        return figureText;
    }

    public void setFigureText(String figureText) {
        this.figureText = figureText;
    }

    public Long getPosId() {
        return posId;
    }

    public void setPosId(Long posId) {
        this.posId = posId;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isRelated() {
        return related;
    }

    public void setRelated(boolean related) {
        this.related = related;
    }

    public boolean isFpEqual() {
        return fpEqual;
    }

    public void setFpEqual(boolean fpEqual) {
        this.fpEqual = fpEqual;
    }
}
