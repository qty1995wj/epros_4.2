package epros.draw.gui.top.toolbar.io;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.draw.constant.JecnWorkflowConstant;
import epros.draw.gui.figure.unit.JecnSelectFigureXY;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.JecnSystemData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnImageUtil;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;
import epros.draw.util.DrawCommon;

/**
 * 导出为图片
 * 
 * @author fuzhh
 * 
 */
public class JecnSaveFlowImageJDialog extends JecnDialog implements
		ActionListener {
	/** 界面放大倍数下拉框 */
	private JComboBox multipleJcomboBox;
	/** 图片存放panel */
	private JecnImageToolTipPanel imagePanel;
	/** 主Panel */
	private JPanel mainPanel;
	/** 滚动面板 */
	private JScrollPane imageJScrollPanel;
	/** 确定面边 */
	private JPanel confirmPanel;
	/** 画图面板容器 */
	private JComponent jcomponent;
	/** 确定按钮 */
	private JButton confirmButton;
	/** 下拉框label */
	private JLabel multipleJlabel;
	/** 放大的倍数 */
	private double scaleRatio = 1;
	/** 面板 */
	private JecnDrawDesktopPane workflow = null;
	/** 所有图形List */
	private List<JecnBaseFlowElementPanel> allFigureList;
	/** 面板大小 */
	private JecnSelectFigureXY figureXY;

	public JecnSaveFlowImageJDialog() {
		workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取所有图形
		allFigureList = workflow.getPanelList();
		// 面板大小
		figureXY = JecnWorkflowUtil.getMaxSize(workflow, allFigureList);
		initComponents();
	}

	/**
	 * 界面初始化
	 */
	private void initComponents() {
		// 设置不可调整大小
		// this.setResizable(false);
		// 设置为模态窗口
		this.setModal(true);
		// 设置标题
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue("exportedAsPictures"));
		// 设置初始大小
		this.setSize(800, 620);
		// 设置居中 ， 在size 之后
		setLocationRelativeTo(null);
		// 初始化 panel
		mainPanel = new JPanel();
		// 设置主Panel 的布局方式
		mainPanel.setLayout(new BorderLayout());
		// 设置主panel的背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 初始化 Panel
		getPanel();
		// 把滚动面板放入主panel中
		mainPanel.add(imageJScrollPanel, BorderLayout.CENTER);
		// 把确定panel放入主panel中
		mainPanel.add(confirmPanel, BorderLayout.SOUTH);
		// 把主panel放入自定义的panel
		this.getContentPane().add(mainPanel);
	}

	private void getPanel() {
		if ((JecnDrawMainPanel.getMainPanel().getWorkflow() == null)
				|| (!(JecnDrawMainPanel.getMainPanel().getWorkflow() instanceof JComponent))) {
			return;
		}
		// 面板数据
		jcomponent = (JComponent) workflow;
		// 初始化图片存放panel
		imagePanel = new JecnImageToolTipPanel(jcomponent);
		imagePanel.setSx(scaleRatio);
		imagePanel.setSy(scaleRatio);
		// 设置panel大小
		imagePanel.setPreferredSize(new Dimension(figureXY.getMaxX(), figureXY
				.getMaxY()));
		// 设置panel的背景色
		imagePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 初始化滚动面边
		imageJScrollPanel = new JScrollPane();
		// 向滚动面板添加Panel
		imageJScrollPanel.getViewport().add(imagePanel);

		imageJScrollPanel.setAutoscrolls(true);

		// 初始确定面板
		confirmPanel = new JPanel();
		// 设置背景色
		confirmPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 设置panel的布局方式
		confirmPanel.setLayout(new GridBagLayout());

		// 初始化label
		multipleJlabel = new JLabel();
		// 给label 赋值
		multipleJlabel.setText(JecnResourceUtil.getJecnResourceUtil()
				.getValue("pleaseChooseTheMagnification"));
		// 初始化放大倍数下拉框
		multipleJcomboBox = new JComboBox();
		// 给下拉框赋值
		maximumNumber();
		// 设置大小
		multipleJcomboBox.setPreferredSize(new Dimension(60, 20));
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.EAST, new Insets(0,
						0, 0, 0), 0, 0);
		confirmPanel.add(multipleJlabel, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 30), 0, 0);
		// 把下拉框加入到 确定panel中
		confirmPanel.add(multipleJcomboBox, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0);
		// 设置下拉框事件
		multipleJcomboBox.addActionListener(this);
		// 设置下拉框动作事件名称
		multipleJcomboBox.setActionCommand("multiple");

		// 初始化 确定按钮
		confirmButton = new JButton();
		// 设置button 名称
		confirmButton.setText("保存");
		// 设置按鈕事件
		confirmButton.addActionListener(this);
		// 设置此按钮的动作事件名称
		confirmButton.setActionCommand("confirm");
		// 把确定按钮放入 确定panel中
		confirmPanel.add(confirmButton, c);
	}

	/**
	 * 下拉框显示的数据
	 */
	private void maximumNumber() {
		// 图片可放大的倍数
		int jComboBoxCount;

		int maxSize = 0;
		if (figureXY.getMaxX() > figureXY.getMaxY()) {
			maxSize = figureXY.getMaxX();
		} else {
			maxSize = figureXY.getMaxY();
		}
		jComboBoxCount = 30000 / maxSize;
		if (jComboBoxCount > 10) {
			jComboBoxCount = 10;
		} else if (jComboBoxCount == 0) {
			jComboBoxCount = 1;
		}
		String[] s = new String[jComboBoxCount];
		for (int i = 0; i < jComboBoxCount; i++) {
			s[i] = String.valueOf(i + 1);
		}
		multipleJcomboBox.setModel(new DefaultComboBoxModel(s));
	}

	/**
	 * 放大方法操作
	 */
	private void multipleJcomboBoxStateChanged() {
		// 获取放大的倍数
		scaleRatio = multipleJcomboBox.getSelectedIndex() + 1;
		// 设置放大倍数
		imagePanel.setSx(scaleRatio);
		imagePanel.setSy(scaleRatio);
		// 重新设置panel的大小
		imagePanel.setPreferredSize(new Dimension(
				(int) (figureXY.getMaxX() * scaleRatio), (int) (figureXY
						.getMaxY() * scaleRatio)));
		// 刷新panel
		imagePanel.updateUI();
	}

	/**
	 * 收集图片数据
	 * 
	 * @param path
	 *            图片存放路径
	 * @param jcomponent
	 *            容器
	 */
	public boolean extendImage(String path, JComponent jcomponent, int width,
			int height) {
		boolean isSave = false;
		// 清空画布
		super.repaint();
		// image缓冲区
		BufferedImage bufferedImage = drawImage(jcomponent, width, height,
				scaleRatio, scaleRatio);
		if (bufferedImage != null) {
			isSave = JecnFileUtil.getCreateImage(path, bufferedImage);
		}
		return isSave;
	}

	/**
	 * 获取image缓冲区
	 * 
	 * @param jcomponent
	 *            容器
	 * @param minX
	 *            最小的X坐标
	 * @param minY
	 *            最小的Y坐标
	 * @param width
	 *            图片宽度
	 * @param height
	 *            图片高度
	 * @param maxX
	 *            最大的X坐标
	 * @param maxY
	 *            最大的Y坐标
	 * @return
	 */
	public BufferedImage drawImage(JComponent jcomponent, int minX, int minY,
			int width, int height, int maxX, int maxY) {
		if (!isSaveImage(width, height)) {
			return null;
		}
		// image缓冲区
		BufferedImage bufferedImage = new BufferedImage(maxX, maxY,
				BufferedImage.TYPE_INT_RGB);
		// 2D 画笔
		Graphics2D g2d = bufferedImage.createGraphics();
		// 绘制图
		jcomponent.paint(g2d);

		bufferedImage = bufferedImage.getSubimage(minX, minY, width, height);

		return bufferedImage;
	}

	/**
	 * 获取image缓冲区
	 * 
	 * @param jcomponent
	 *            容器
	 * @param minX
	 *            最小的X坐标
	 * @param minY
	 *            最小的Y坐标
	 * @param width
	 *            图片宽度
	 * @param height
	 *            图片高度
	 * @param maxX
	 *            最大的X坐标
	 * @param maxY
	 *            最大的Y坐标
	 * @param doXY
	 *            图片的放大缩小倍数
	 * @return
	 */
	public BufferedImage drawImage(JComponent jcomponent, int minX, int minY,
			int width, int height, int maxX, int maxY, double doXY) {
		if (!isSaveImage(width, height)) {
			return null;
		}
		// image缓冲区
		BufferedImage bufferedImage = new BufferedImage(DrawCommon
				.convertDoubleToInt(maxX * doXY), DrawCommon.convertDoubleToInt(maxY
				* doXY), BufferedImage.TYPE_INT_RGB);
		// 2D 画笔
		Graphics2D g2d = bufferedImage.createGraphics();
		// 使用此 Graphics2D 中的 Transform 组合 AffineTransform 对象
		g2d.transform(AffineTransform.getScaleInstance(doXY, doXY));
		// 绘制图
		jcomponent.paint(g2d);
		if (minX < 0 || minY < 0) {
			return null;
		}
		bufferedImage = bufferedImage.getSubimage((int) (minX * doXY),
				(int) (minY * doXY), (int) (width * doXY),
				(int) (height * doXY));

		jcomponent.paint(g2d);
		g2d.dispose();
		return bufferedImage;
	}

	/**
	 * 获取image缓冲区
	 * 
	 * @param jcomponent
	 *            容器
	 * @param width
	 *            图片宽度
	 * @param height
	 *            图片高度
	 * @return
	 */
	public BufferedImage drawImage(JComponent jcomponent, int width,
			int height, double doX, double doY) {
		if (!isSaveImage(width, height)) {
			return null;
		}
		// image缓冲区
		BufferedImage bufferedImage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics graphics = bufferedImage.createGraphics();
		// 2D 画笔
		Graphics2D g2d = (Graphics2D) graphics;
		// 使用此 Graphics2D 中的 Transform 组合 AffineTransform 对象
		g2d.transform(AffineTransform.getScaleInstance(doX, doY));
		// 绘制图
		jcomponent.paint(g2d);
		g2d.dispose();
		return bufferedImage;
	}



	/** 点击事件 */
	public void actionPerformed(ActionEvent e) {
		// 判断点击的是否为JComboBox
		if (e.getSource() instanceof JComboBox) {
			JComboBox box = (JComboBox) e.getSource();
			// 判断是否点击选择下拉框的大小
			if (box.getActionCommand().equals("multiple")) {
				multipleJcomboBoxStateChanged();
			}
		}

		// 判断点击的是否为button
		if (e.getSource() instanceof JButton) {
			JButton box = (JButton) e.getSource();
			// 判断是否点击选择下拉框的大小
			if (box.getActionCommand().equals("confirm")) {
				// 获取默认路径
				String pathUrl = JecnSystemData.readCurrSystemFileIOData()
						.getSaveImageDirectory();
				// 获取截取后正确的路径
				if (pathUrl != null && !"".equals(pathUrl)) {
					pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
				}
				// 文件路径选择框
				JFileChooser fileChooser = new JecnFileChooser(pathUrl);
				// 设置窗口标题
				fileChooser.setDialogTitle(JecnResourceUtil.getJecnResourceUtil()
						.getValue("choosePhotosRepositoryPaths"));
				// 文件名称获取当前面板名称
				fileChooser.setSelectedFile(new File(workflow.getFlowMapData()
						.getShowName()));

				// 过滤文件类型为jpg
				fileChooser.setFileFilter(new ImportFlowFilter("jpg"));
				// 添加保存按钮
				int i = fileChooser.showSaveDialog(this);
				// 如果点击保存
				if (i == JFileChooser.APPROVE_OPTION) {
					// 获取文件路径
					String fileDirectory = fileChooser.getSelectedFile()
							.getPath();
					String pathName = fileChooser.getSelectedFile().getName();
					String error = JecnUserCheckUtil.checkFileName(pathName);
					if (!"".equals(error)) {
						JecnOptionPane.showMessageDialog(null, error,
								JecnResourceUtil.getJecnResourceUtil().getValue("tip"),
								JOptionPane.ERROR_MESSAGE);
						return;
					}
					// 修改配置文件中的路径
					JecnSystemData.readCurrSystemFileIOData()
							.setSaveImageDirectory(fileDirectory);
					JecnSystemData.writeCurrSystemFileIOData();
					// 文件路径
					String path = fileDirectory;
					if (!fileDirectory.endsWith(".jpg")) {
						path = fileDirectory + ".jpg";
					}
					// 判断文件是否存在
					File f = new File(path);
					if (f.exists()) {
						int result = JecnOptionPane
								.showConfirmDialog(
										this,
										JecnResourceUtil.getJecnResourceUtil()
												.getValue("theCurrentFileAlreadyExistsIsCovered"),
										JecnResourceUtil.getJecnResourceUtil().getValue("confirm"),
										JOptionPane.YES_NO_OPTION);
						if (result != JOptionPane.YES_NO_OPTION) {
							return;
						}
					}
					// 保存图片
					boolean isSave = extendImage(path, jcomponent,
							(int) (figureXY.getMaxX() * scaleRatio),
							(int) (figureXY.getMaxY() * scaleRatio));
					if (isSave) {
						// 修改配置文件中的路径
						JecnSystemData.readCurrSystemFileIOData()
								.setSaveImageDirectory(fileDirectory);
						this.dispose();
					}
				}
			}
		}
	}

	/**
	 * 判断是否可以执行保存图片
	 * 
	 * @author fuzhh Sep 4, 2012
	 * @param width
	 *            图片的宽度
	 * @param height
	 *            图片的高度
	 * @return
	 */
	protected boolean isSaveImage(int width, int height) {
		if (JecnImageUtil.getJVMHeap() < 512) {
			if (width * height > JecnWorkflowConstant.imageSize) {
				JecnOptionPane.showMessageDialog(this,
						JecnResourceUtil.getJecnResourceUtil()
						.getValue("RAMBigOrSmall"), null,
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
		}
		return true;
	}
}
