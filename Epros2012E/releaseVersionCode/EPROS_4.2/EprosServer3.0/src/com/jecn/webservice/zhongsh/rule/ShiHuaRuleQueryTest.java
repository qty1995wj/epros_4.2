package com.jecn.webservice.zhongsh.rule;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.xml.namespace.QName;

import com.jecn.webservice.zhongsh.rule.query.SmisSearchService;
import com.jecn.webservice.zhongsh.rule.query.SmisSearchServiceSoap;

public class ShiHuaRuleQueryTest {

	private static final QName SERVICE_NAME = new QName("http://tempuri.org/", "SmisSearchService");
	static SmisSearchService ss = new SmisSearchService(SmisSearchService.WSDL_LOCATION, SERVICE_NAME);
	static SmisSearchServiceSoap port = ss.getSmisSearchServiceSoap();

	static String username = "zhangtl66";
	static String password = "123456";

	static String storedDirPath = "E:/yingke/";

	public static void main(String args[]) throws java.lang.Exception {
		queryRuleTree();
		queryRuleByTree();
	}

	private static void queryRuleByTree() throws Exception {
		String[] bussCodes = new String[] { "A2", "A8", "A8.5.2", "A8.6", "A8.7.1", "A9.5.3", "B1", "B1.2",
				"B1.2.10", "B1.2.5", "B1.2.6", "B1.2.7", "B1.2.8", "B1.2.9", "B11", "B11.1.3", "B11.4",
				"B11.4.3", "B11.5", "B12.2.1", "B12.2.6", "B12.5", "B12.6" };
		for (String code : bussCodes) {
			String data = port.searchSystemByClassCode(username, password, code);
			createFile(storedDirPath + "/rules/", code + ".xml", data);
		}
	}

	private static void queryRuleTree() throws Exception {
		String dicBusinessCode = port.getDicBusinessCode();
		createFile(storedDirPath, "dicBusinessCode.xml", dicBusinessCode);
	}

	private static void createFile(String dirPath, String fileName, String data) throws Exception {
		File dir = new File(dirPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File f = new File(dirPath + fileName);
		f.delete();
		f.createNewFile();
		OutputStream stream = new FileOutputStream(f);
		stream.write(data.getBytes());
		stream.flush();
		stream.close();
	}

}
