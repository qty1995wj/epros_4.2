package com.jecn.epros.domain.report;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * Created by user on 2017/4/20.
 * 清单搜索查询条件
 */
public class InventoryParam {

    private Long id;
    private String type;
    private String action;
    @ApiModelProperty(value = "当前页数", required = true)
    private int pageNum;
    @ApiModelProperty(value = "每页显示的条数", required = true, allowableValues = "10,15,20")
    private int pageSize;
    /**
     * 下载类型：默认0，清单，1：版本清单
     */
    private int downloadType;

    /**
     * 责任部门Id
     */
    private Long orgId;
    //ids
    private List<Long> ids;

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getDownloadType() {
        return downloadType;
    }

    public void setDownloadType(int downloadType) {
        this.downloadType = downloadType;
    }
}
