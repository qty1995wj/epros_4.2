package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.file.FileBaseBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程操作模板
 *
 * @author admin
 */
public class FlowOperationModel {
    /**
     * 活动ID
     */
    private Long figureId;
    /**
     * 活动编号
     */
    private String activeNum;
    /**
     * 活动名称
     */
    private String activeName;
    /**
     * 活动输出、输入文本显示
     */
    private String activeInput;
    private String activeOutput;
    /**
     * 活动输入
     */
    private List<FileBaseBean> inputFiles;
    /**
     * 输出表单
     */
    private List<FileBaseBean> outputFiles;
    /**
     * 输出样例
     */
    private List<FileBaseBean> outputTmpFiles;
    /**
     * 操作规范
     */
    private List<FileBaseBean> operationLinksFiles;

    public LinkResource getLink() {
        return new LinkResource(figureId, activeName, ResourceType.ACTIVITY);
    }

    public List<LinkResource> getInputLinks() {
        if (inputFiles == null) {
            return null;
        }
        List<LinkResource> inputLinks = new ArrayList<>();
        for (FileBaseBean fileBaseBean : inputFiles) {
            inputLinks.add(fileBaseBean.getLink());
        }
        return inputLinks;
    }

    public List<LinkResource> getOutputLinks() {
        if (outputFiles == null) {
            return null;
        }
        List<LinkResource> outputLinks = new ArrayList<>();
        for (FileBaseBean fileBaseBean : outputFiles) {
            outputLinks.add(fileBaseBean.getLink());
        }
        return outputLinks;
    }

    public List<LinkResource> getOutputTmpLinks() {
        if (outputTmpFiles == null) {
            return null;
        }
        List<LinkResource> outputTmpLinks = new ArrayList<>();
        for (FileBaseBean fileBaseBean : outputTmpFiles) {
            outputTmpLinks.add(fileBaseBean.getLink());
        }
        return outputTmpLinks;
    }

    public List<LinkResource> getOperationFileLinks() {
        if (operationLinksFiles == null) {
            return null;
        }
        List<LinkResource> operationFileLinks = new ArrayList<>();
        for (FileBaseBean fileBaseBean : operationLinksFiles) {
            operationFileLinks.add(fileBaseBean.getLink());
        }
        return operationFileLinks;
    }

    @JsonIgnore
    public Long getFigureId() {
        return figureId;
    }

    public void setFigureId(Long figureId) {
        this.figureId = figureId;
    }

    public String getActiveNum() {
        return activeNum;
    }

    public void setActiveNum(String activeNum) {
        this.activeNum = activeNum;
    }

    @JsonIgnore
    public String getActiveName() {
        return activeName;
    }

    public void setActiveName(String activeName) {
        this.activeName = activeName;
    }

    @JsonIgnore
    public List<FileBaseBean> getInputFiles() {
        return inputFiles;
    }

    @JsonIgnore
    public List<FileBaseBean> getOutputFiles() {
        return outputFiles;
    }

    @JsonIgnore
    public List<FileBaseBean> getOutputTmpFiles() {
        return outputTmpFiles;
    }

    @JsonIgnore
    public List<FileBaseBean> getOperationLinksFiles() {
        return operationLinksFiles;
    }

    public void setInputFiles(List<FileBaseBean> inputFiles) {
        this.inputFiles = inputFiles;
    }

    public void setOutputFiles(List<FileBaseBean> outputFiles) {
        this.outputFiles = outputFiles;
    }

    public void setOutputTmpFiles(List<FileBaseBean> outputTmpFiles) {
        this.outputTmpFiles = outputTmpFiles;
    }

    public void setOperationLinksFiles(List<FileBaseBean> operationLinksFiles) {
        this.operationLinksFiles = operationLinksFiles;
    }

    public String getActiveInput() {
        return activeInput;
    }

    public void setActiveInput(String activeInput) {
        this.activeInput = activeInput;
    }

    public String getActiveOutput() {
        return activeOutput;
    }

    public void setActiveOutput(String activeOutput) {
        this.activeOutput = activeOutput;
    }

}
