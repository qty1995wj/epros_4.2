package com.jecn.epros.server.dao.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnLineSegmentT;
import com.jecn.epros.server.bean.process.JecnMapLineSegmentT;
import com.jecn.epros.server.common.IBaseDao;

public interface IJecnLineSegmentTDao extends IBaseDao<JecnLineSegmentT, Long> {
	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：通过流程Id获得所有连接线的线段
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnLineSegmentT> findAllByFlowId(Long flowId) throws Exception;

	List<JecnMapLineSegmentT> findMapRelatedLineById(Long flowId) throws Exception;
}
