package epros.draw.gui.top.button;

import javax.swing.JToggleButton;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.top.toolbar.JecnToolbarTitlePanel;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 流程元素按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolbarTitleButton extends JToggleButton {
	/** 工具栏标题栏按钮 */
	private JecnToolbarTitlePanel toolbarTitle = null;

	/**
	 * 
	 * 构造函数
	 * 
	 * @param toolbarTitle JecnToolbarTitlePanel
	 * @param text String 名称
	 * 
	 */
	public JecnToolbarTitleButton(JecnToolbarTitlePanel toolbarTitle,
			String text) {
		if (toolbarTitle == null) {
			JecnLogConstants.LOG_TOOLBAR_TITLE_BUTTON
					.error("ToolBoxButton类构造函数：toolbarTitle参数为null:toolbarTitle="
							+ toolbarTitle);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.toolbarTitle = toolbarTitle;

		initComponents(text);
	}

	/**
	 * 
	 * 构造函数
	 * 
	 * @param toolbarTitle JecnToolbarTitlePanel
	 * @param text String 名称
	 * @param tipText String 提示信息
	 * 
	 */
	public JecnToolbarTitleButton(JecnToolbarTitlePanel toolbarTitle,
			String text, String tipText) {
		this(toolbarTitle, text);
		if (!DrawCommon.isNullOrEmtryTrim(tipText)) {//提示信息
			this.setToolTipText(tipText);
		}
	}

	private void initComponents(String text) {
		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 大小
		this.setPreferredSize(JecnUIUtil.getTootBarTitleBtnSize());
		this.setMinimumSize(JecnUIUtil.getTootBarTitleBtnSize());
		this.setMaximumSize(JecnUIUtil.getTootBarTitleBtnSize());

		// 设置内容
		if (text != null) {
			this.setText(text);
		}
		// 不显示焦点状态
		this.setFocusPainted(false);

		// 不绘制内容
		this.setContentAreaFilled(false);
		// 无边框
		this.setBorder(null);
		// 透明
		this.setOpaque(false);

		// 添加事件
		this.addActionListener(toolbarTitle);
	}
}