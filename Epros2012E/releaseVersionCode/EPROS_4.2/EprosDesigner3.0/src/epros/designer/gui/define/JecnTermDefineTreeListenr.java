package epros.designer.gui.define;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class JecnTermDefineTreeListenr extends JecnTreeListener {
	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(JecnTermDefineTreeListenr.class);

	public JecnTermDefineTreeListenr(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}

	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath().getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = ConnectionPool.getTermDefinitionAction().getChilds(
						node.getJecnTreeBean().getId());
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error(JecnProperties.getValue("termDefineTreeOpenError"), e);
			}
			JecnTreeCommon.selectNode(jTree, node);
		}
	}

}
