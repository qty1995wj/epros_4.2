package com.jecn.epros.server.action.web.login.ad.juguang;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 
 * 聚光：单点登录
 * 
 * @author zhouxy
 * 
 */
public class JecnJuGuangWebLoginAction extends JecnAbstractADLoginAction {

	public JecnJuGuangWebLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 单点登录入口
	 * 
	 * @return String
	 */
	@Override
	public String login() {
		try {
			//单点登录传递过来的登录名称
			String adLoginName = null;
			
			Object loginlotus = loginAction.getSession().getAttribute(
					"edu.yale.its.tp.cas.client.filter.user");
			if (loginlotus != null) {
				adLoginName = String.valueOf(loginlotus);
			}

			if (StringUtils.isBlank(adLoginName)) {// 单点登录名称为空，执行普通登录路径
				return loginAction.loginGen();
			}
			
			// 验证登录
			return this.loginByLoginName(adLoginName);

		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}
}
