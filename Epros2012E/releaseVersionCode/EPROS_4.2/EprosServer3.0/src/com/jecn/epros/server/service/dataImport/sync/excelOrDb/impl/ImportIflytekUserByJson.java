package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.action.web.login.ad.iflytek.JecnIflytekLoginItem;
import com.jecn.epros.server.common.HttpRequest;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnPosGroup;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnPosGroupRelatedPos;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 基于Json同步数据
 * 
 * @author ZXH
 * @date 2018-11-16
 * 
 */
public class ImportIflytekUserByJson extends AbstractImportUser {
	protected final Log log = LogFactory.getLog(this.getClass());

	private List<JecnPosGroupRelatedPos> groupRelatedPos = new ArrayList<JecnPosGroupRelatedPos>();

	private List<JecnPosGroup> posGroups = new ArrayList<JecnPosGroup>();

	private Map<String, String> postMap = null;

	private List<String> existsUser = new ArrayList<String>();

	public ImportIflytekUserByJson(AbstractConfigBean cnfigBean) {
		super(cnfigBean);
	}

	/**
	 * 获取部门数据集合
	 */
	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		String deptUrl = JecnIflytekLoginItem.getValue("PS_ORG_SERVICE");
		// 执行读取Json数据
		String result = HttpRequest.sendPost(deptUrl, "parm={\"\":\"\"}");
		return getDpetList(result);
	}

	/**
	 * 获取人员数据集合
	 */
	@Override
	public List<UserBean> getUserBeanList() throws Exception {

		// 获取岗位和部门的对应关系
		postMap = intPostAndPostGroups();

		// 执行读取Json数据
		List<UserBean> userBeans = null;
		String userUrl = JecnIflytekLoginItem.getValue("PS_EMPL_SERVICE");

		int index = 1000;

		String result = HttpRequest.sendPost(userUrl, "parm={\"currentPageNo\":\"1\",\"pageSize\":\"" + index + "\"}");

		// 添加首页
		userBeans = getUserList(result);
		String pages = JSONObject.fromObject(result).getJSONObject("content").getString("pages");
		int total = Integer.valueOf(pages);

		// 当前页
		int curPageNo = 1;

		while (curPageNo < total) {
			curPageNo++;
			// index = index * curPageNo;
			result = HttpRequest.sendPost(userUrl, "parm={\"currentPageNo\":\"" + curPageNo + "\",\"pageSize\":\""
					+ index + "\"}");
			userBeans.addAll(getUserList(result));
		}

		log.info("获取人员数据成功！");
		return userBeans;
	}

	public static void main(String[] args) {

	}

	private List<String> existsOrgCodes = null;

	/**
	 * 把数据从json取出存储到bean对象中
	 * 
	 * @param result
	 */
	public List<DeptBean> getDpetList(String result) throws Exception {
		/** 部门 */
		List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
		DeptBean deptBean = null;
		String returnObj = JSONObject.fromObject(result).getString("content");
		log.info("获取部门数据成功！");
		JSONArray jsonArray = JSONArray.fromObject(returnObj);

		existsOrgCodes = new ArrayList<String>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			deptBean = new DeptBean();
			// 2018年12月12日 测试接口调整

			deptBean.setDeptNum(jsonObject.getString("deptId"));
			deptBean.setDeptName(jsonObject.getString("deptName"));

			Object object = jsonObject.get("parentOrg");

			deptBean.setPerDeptNum(object == null ? "0" : object.toString());

			if ("D000004185".equals(deptBean.getDeptNum())) {
				continue;
			}

			deptBeanList.add(deptBean);

			existsOrgCodes.add(deptBean.getDeptNum());

		}
		return deptBeanList;
	}

	/**
	 * 把数据从json取出存储到bean对象中
	 * 
	 * @param result
	 */
	public List<UserBean> getUserList(String result) throws Exception {
		/** 人员 */
		List<UserBean> userBeanList = new ArrayList<UserBean>();
		UserBean userBean = null;

		String returnObj = JSONObject.fromObject(result).getJSONObject("content").getString("list");
		JSONArray jsonArray = JSONArray.fromObject(returnObj);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			userBean = new UserBean();
			userBean.setUserNum(jsonObject.getString("accountName"));
			userBean.setTrueName(userBean.getUserNum());
			userBean.setPosNum(jsonObject.getString("positionCode"));
			userBean.setPosName(jsonObject.getString("positionName"));
			userBean.setDeptNum(postMap.get(userBean.getPosNum()));
			// userBean.setEmail(jsonObject.getString("email"));
			// userBean.setEmailType(0);
			if (StringUtils.isBlank(userBean.getPosNum()) || StringUtils.isBlank(userBean.getDeptNum())
					|| !existsOrgCodes.contains(userBean.getDeptNum())) {
				userBean.setDeptNum(null);
				userBean.setPosName(null);
				userBean.setPosNum(null);
			}
			String splitCode = userBean.getUserNum() + "_" + userBean.getPosNum();
			if (!existsUser.contains(splitCode)) {
				userBeanList.add(userBean);
				existsUser.add(splitCode);
			}
		}
		return userBeanList;
	}

	private Map<String, String> intPostAndPostGroups() {
		String postUrl = JecnIflytekLoginItem.getValue("PS_POSITION_SERVICE");
		// 执行读取Json数据
		String result = HttpRequest.sendPost(postUrl, "parm={\"\":\"\"}");

		String returnObj = JSONObject.fromObject(result).getString("content");

		log.info("获取岗位数据成功！");

		JSONArray jsonArray = JSONArray.fromObject(returnObj);

		Map<String, String> postionMap = new HashMap<String, String>();
		List<String> exists = new ArrayList<String>();

		JecnPosGroup posGroup = null;
		JecnPosGroupRelatedPos groupRelatedPost = null;

		// 目录对应的岗位组
		Map<String, List<String>> dirRelatedPosGroup = new HashMap<String, List<String>>();

		// 记录职务（岗位组）和职位（岗位）的对应关系
		Map<String, List<String>> proGroupRelatedPost = new HashMap<String, List<String>>();

		// 添加默认顶级目录（系统岗位组）

		addSystemPostGroup();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.opt(i);
			String positionCode = jsonObject.getString("positionCode");
			postionMap.put(positionCode, jsonObject.getString("orgCode"));

			// 序列 目录编号
			String jobFunCode = jsonObject.getString("jobFunc");
			// 序列 目录名称
			String jobFuncName = jsonObject.getString("jobFuncName");
			if (StringUtils.isBlank(jobFuncName) || StringUtils.isBlank(jobFunCode)) {
				continue;
			}

			// 职务（岗位组）编码
			String jobCode = jsonObject.getString("jobCode");
			// 职务（岗位组）名称
			String jobName = jsonObject.getString("jobName");

			if (!exists.contains(jobFunCode)) {
				exists.add(jobFunCode);
				posGroup = new JecnPosGroup();
				posGroup.setIsDir(1);
				posGroup.setParentNum(SYSTEM_POST_GROUP_NUM);
				posGroup.setName(jobFuncName);
				posGroup.setNum(jobFunCode);

				// 添加岗位组
				posGroups.add(posGroup);

				dirRelatedPosGroup.put(jobFunCode, new ArrayList<String>());
			}
			List<String> posGroupCodes = dirRelatedPosGroup.get(jobFunCode);

			String posGroupCode = jobCode + "_" + jobFunCode;
			if (!posGroupCodes.contains(posGroupCode)) {
				// 岗位组
				posGroup = new JecnPosGroup();
				posGroup.setIsDir(0);
				posGroup.setParentNum(jobFunCode);
				posGroup.setName(jobName);

				// 岗位组编号
				posGroup.setNum(posGroupCode);

				// 添加岗位组
				posGroups.add(posGroup);

				proGroupRelatedPost.put(posGroupCode, new ArrayList<String>());
				posGroupCodes.add(posGroupCode);
			}

			// 岗位组对应岗位集合
			List<String> posCodes = proGroupRelatedPost.get(posGroupCode);
			if (!posCodes.contains(positionCode)) {
				posCodes.add(positionCode);
				groupRelatedPost = new JecnPosGroupRelatedPos();
				groupRelatedPost.setPosGroupNum(posGroupCode);
				groupRelatedPost.setPosNum(positionCode);
				groupRelatedPos.add(groupRelatedPost);
			}
		}
		return postionMap;
	}

	private static final String SYSTEM_POST_GROUP_NUM = "SYS_01";

	/**
	 * 添加默认的系统岗位组
	 */
	private void addSystemPostGroup() {
		JecnPosGroup posGroup = new JecnPosGroup();
		posGroup.setIsDir(1);
		posGroup.setParentNum("0");
		posGroup.setName("系统岗位组");
		posGroup.setNum(SYSTEM_POST_GROUP_NUM);
		// 添加岗位组
		posGroups.add(posGroup);
	}

	@Override
	public List<JecnPosGroupRelatedPos> getGroupRelatedPos() throws Exception {
		return groupRelatedPos;
	}

	@Override
	public List<JecnPosGroup> getPosGroups() throws Exception {
		return posGroups;
	}
}
