package epros.designer.gui.rule;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.io.File;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.bean.rule.AddRuleFileData;
import com.jecn.epros.server.bean.rule.RuleT;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.system.JecnSystemData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;
import epros.draw.visio.JecnFileNameFilter;

/**
 * 
 * 本地方式上传制度文件
 * 
 * @author hyl
 * 
 */
public class G020UploadRuleFileDialog extends UploadRuleFileDialog {
	private int codeTotal = 0;
	private String parentCode = null;

	public G020UploadRuleFileDialog(JecnTreeNode pNode, JecnTree jTree) {
		super(pNode, jTree);
		if (JecnConfigTool.isStandardizedAutoCode()) {
			AutoCodeResultData resultData = JecnDesignerCommon.getStandardizedAutoCodeResultData(pNode
					.getJecnTreeBean().getId(), 2);
			// 获取目录编号
			parentCode = resultData.getDirCode();
			codeTotal = resultData.getCodeTotal();
		}
	}

	protected void initCompotents() {
		super.initCompotents();
		this.setTitle(JecnProperties.getValue("localUpload"));
		fileTable = new JecnAddFileTable();
	}

	/**
	 * 打开文件
	 */
	@Override
	protected void openFile() {
		int[] selectRows = fileTable.getSelectedRows();
		if (selectRows.length == 0) {
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		for (int i = 0; i < selectRows.length; i++) {
			String filePath = (String) fileTable.getValueAt(selectRows[i], 2);
			JecnUtil.openLocalFile(filePath);
		}
		verfyLab.setText("");
	}

	class JecnAddFileTable extends JTable {

		public JecnAddFileTable() {
			// 添加渲染
			addCellEditor(this);

			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}
			// this.addKeyListener(new CTableKeyAdapter(this));
			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			// 自定义表头UI
			this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
			if (JecnConfigTool.isAutoNumber()) {
				TableColumn tableColumn = columnModel.getColumn(1);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		public AddFileTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("fileNum"));
			title.add(JecnProperties.getValue("fileName"));
			title.add(JecnProperties.getValue("fileSrc"));

			return new AddFileTableMode(null, title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {
			return new int[] {};
		}

	}

	protected void addCellEditor(JTable fileTable) {
	}

	class AddFileTableMode extends DefaultTableModel {
		public AddFileTableMode(Vector<Vector<Object>> data, Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			if (colindex == 0) {
				return true;
			}
			return false;
		}

	}

	/**
	 * 点击确定执行的方法
	 */
	@Override
	protected boolean initAndCheckRuleList() {
		verfyLab.setText("");
		DefaultTableModel mode = (DefaultTableModel) fileTable.getModel();

		if (fileTable.isEditing()) {
			fileTable.getCellEditor().stopCellEditing();
		}
		int rows = fileTable.getRowCount();
		RuleT ruleT = null;
		// 获取父节点的hide值
		for (int i = 0; i < rows; i++) {
			String s = JecnUserCheckUtil.checkNameLength((String) mode.getValueAt(i, 0));
			if (!DrawCommon.isNullOrEmtryTrim(s)) {
				verfyLab.setText(JecnProperties.getValue("fileNum") + s);
				return false;
			}
			ruleT = new RuleT();
			ruleT.setPerId(pNode.getJecnTreeBean().getId());
			// 制度名称
			ruleT.setRuleName((String) mode.getValueAt(i, 1));
			// 制度编号
			if (mode.getValueAt(i, 0) != null && !"".equals(mode.getValueAt(i, 0).toString().trim())) {
				ruleT.setRuleNumber(((String) mode.getValueAt(i, 0)));
			}

			if (categoryCommon != null) {
				// 制度类别
				ruleT.setTypeId(this.categoryCommon.getTypeResultId());
			}
			// 责任部门
			ruleT.setOrgId(this.resDepartSelectCommon.getIdResult());
			// 专员
			if (commissionerPeopleSelectCommon != null) {
				ruleT.setCommissionerId(commissionerPeopleSelectCommon.getIdResult());
			}
			// 关键字
			if (this.keyWordField != null) {
				ruleT.setKeyword(keyWordField.getResultStr());
			}
			// 制度责任人id
			if (personliableSelect != null) {
				ruleT.setAttchMentId(personliableSelect.getSingleSelectCommon().getIdResult());
				// 制度责任人类型
				ruleT.setTypeResPeople(personliableSelect.getPeopleResType());
			}
			// 监护人
			if (guardianPeopleSelectCommon != null) {
				ruleT.setGuardianId(guardianPeopleSelectCommon.getIdResult());
			}

			ruleT.setCreatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
			ruleT.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
			ruleT.setIsDir(2);// (0是目录，1是制度,2是制度文件)
			// 密级
			ruleT.setIsPublic(accessAuthorityCommon.getPublicStatic() == 0 ? 0L : 1L);
			ruleT.setSortId(JecnTreeCommon.getMaxSort(pNode) + i);
			ruleT.setProjectId(JecnConstants.projectId);
			ruleT.setExpiry(Long.valueOf(JecnConfigTool.ruleValidityAllocation().toString()));
			if (StringUtils.isBlank(mode.getValueAt(i, 2).toString())) {
				JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("uplodeFileIsNull"));
				return false;
			}
			ruleT.setFileBytes(JecnUtil.changeFileToByte((String) mode.getValueAt(i, 2)));
			ruleT.setIsFileLocal(1);
			// 保密级别
			ruleT.setConfidentialityLevel(accessAuthorityCommon.getConfidentialityLevelStatic());
			list.add(ruleT);
			names.add(ruleT.getRuleName());
		}

		return true;

	}

	protected RuleT setRuleAttrs() {
		RuleT ruleT = new RuleT();
		ruleT.setPerId(pNode.getJecnTreeBean().getId());
		if (categoryCommon != null) {
			// 制度类别
			ruleT.setTypeId(this.categoryCommon.getTypeResultId());
		}
		// 责任部门
		ruleT.setOrgId(this.resDepartSelectCommon.getIdResult());

		// 制度责任人id
		ruleT.setAttchMentId(personliableSelect.getSingleSelectCommon().getIdResult());
		// 制度责任人类型
		ruleT.setTypeResPeople(personliableSelect.getPeopleResType());

		ruleT.setCreatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		ruleT.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser().getPeopleId());
		ruleT.setIsDir(2);// (0是目录，1是制度,2是制度文件)
		// 密级
		ruleT.setIsPublic(accessAuthorityCommon.getPublicStatic() == 0 ? 0L : 1L);

		ruleT.setProjectId(JecnConstants.projectId);
		ruleT.setSortId(JecnTreeCommon.getMaxSort(pNode));
		// 保密级别
		ruleT.setConfidentialityLevel(accessAuthorityCommon.getConfidentialityLevelStatic());
		list.add(ruleT);
		return ruleT;
	}
	
	protected AddRuleFileData getRueFileData() {
		AddRuleFileData ruleFileData = super.getRueFileData();
		// 保存当前值
		codeTotal--;
		ruleFileData.setTotalCode(codeTotal);
		return ruleFileData;
	}

	@Override
	protected void selectFile() {
		// 获取默认路径
		String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSavaRuleUpdateDirectory();
		// 获取截取后正确的路径
		if (pathUrl != null && !"".equals(pathUrl)) {
			pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
		}

		// 文件选择器
		JecnFileChooser fileChoose = new JecnFileChooser(pathUrl);

		fileChoose.setEidtTextFiled(false);

		String[] arrs = getFileTypeByConfig();
		if (arrs != null) {
			// 设置过滤条件
			fileChoose.setFileFilter(new JecnFileNameFilter(arrs));
			// 移除系统给定的文件过滤器
			fileChoose.setAcceptAllFileFilterUsed(false);
		}
		// 可以同时上传多个文件
		fileChoose.setMultiSelectionEnabled(true);
		// 设置标题
		fileChoose.setDialogTitle(JecnProperties.getValue("fileUpload"));
		int i = fileChoose.showSaveDialog(this);
		if (i == JFileChooser.APPROVE_OPTION) {
			// 选择的文件
			File[] files = fileChoose.getSelectedFiles();

			// 获取输出文件的路径
			String filePath = fileChoose.getSelectedFile().getPath();
			// 修改配置文件中的路径
			JecnSystemData.readCurrSystemFileIOData().setSavaRuleUpdateDirectory(filePath);
			JecnSystemData.writeCurrSystemFileIOData();
			addLocalFiles(files);
		}
	}

	private String[] getFileTypeByConfig() {
		String value = JecnConfigTool.getUploadFileType();
		return StringUtils.isBlank(value) ? null : value.split(",");
	}

	protected void addLocalFiles(File[] files) {
		Vector<Vector<Object>> vs = ((DefaultTableModel) fileTable.getModel()).getDataVector();
		F: for (File f : files) {
			for (Vector t : vs) {// 判断是否已上传
				if (f.getPath().equals((String) t.get(2))) {
					continue F;
				}
			}
			Vector<Object> v = new Vector<Object>();
			if (StringUtils.isNotBlank(parentCode)) {
				v.add(parentCode + getCodeString(codeTotal));
				codeTotal++;
			} else {
				v.add("");
			}
			v.add(JecnUtil.getFileName(f.getPath()));
			v.add(f.getPath());
			((DefaultTableModel) fileTable.getModel()).addRow(v);
		}
	}

	private String getCodeString(int codeTotal) {
		return codeTotal < 10 ? "0" + codeTotal : codeTotal + "";
	}

	protected void childDeleteData() {
		if (StringUtils.isNotBlank(parentCode)) {
			codeTotal--;
		}
	}

	protected void customPanel(int rows, JecnPanel mainPanel, Insets insets) {
		String value = JecnConfigTool.getUploadFileType();
		if (StringUtils.isNotBlank(value)) {
			//TitledBorder border = BorderFactory.createTitledBorder("请上传文件类型为" + value + "格式的文件！");
			TitledBorder border = BorderFactory.createTitledBorder(JecnProperties.getValue("uploadFileType") + value );
			border.setTitleColor(Color.red);
			border.setTitleFont(new Font("宋体", Font.PLAIN, 14));
			filePanel.setBorder(border);
		}
	}
}
