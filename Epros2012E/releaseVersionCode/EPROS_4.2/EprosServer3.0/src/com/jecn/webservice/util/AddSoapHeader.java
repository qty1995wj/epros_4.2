package com.jecn.webservice.util;

import java.util.Properties;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * 此类主要用来创建soap安全头
 * <GalaxySoapHeader xmlns="http://tempuri.org/"><WebKey>123</WebKey></GalaxySoapHeader>
 * @author chehuanbo
 * 
 * */
public class AddSoapHeader extends AbstractSoapInterceptor {
    
	Logger log =Logger.getLogger(AddSoapHeader.class);
	private Properties properties = null;
	
	public AddSoapHeader(Properties properties) {
		super(Phase.WRITE); // 在写之前
		this.properties=properties;
	}
	
	/**
	 * 创建安全头方法(soapHeader)
	 * 描述：如果有特殊要求请重写此方法
	 * 
	 * 星汉webservice安全头
	 * <?xmlversion="1.0"encoding="utf-8"?><GalaxySoapHeader xmlns="http://tempuri.org/"><WebKey>123</WebKey></GalaxySoapHeader>
	 * 
	 * @author chehuanbo
	 * 
	 * */
	public void handleMessage(SoapMessage message) throws Fault {
		try {
			// @parm args0 :命名空间  args1:安全头名称
			QName qname = new QName(properties.getProperty("nameSpace"), properties.getProperty("requestSoapHeader"));
			// 构造一个XML
			Document doc = DOMUtils.createDocument();
			// @parm args0: 命名空间  args1:安全头名称
			Element authElement = doc.createElementNS(properties.getProperty("nameSpace"),
					properties.getProperty("requestSoapHeader"));
			//对方接受安全验证参数名称
			Element tokenElement = doc.createElement(properties.getProperty("toKeny"));// 令牌
			tokenElement.setTextContent(properties.getProperty("toKenyValue")); // 参数值
			authElement.appendChild(tokenElement);
//			// 将创建的安全头打印出来，正式发布可以注掉
//			XMLUtils.printDOM(authElement);
			// SOAP头 将有authElement元素组成
			SoapHeader header = new SoapHeader(qname, authElement);
			// 把我们构造的这个头 添加到message中去
			message.getHeaders().add(header);
		} catch (DOMException e) {
			log.error("创建安全头出现异常！ AddSoapHeader handleMessage()", e);
		}
	}
	
}
