package epros.designer.gui.system.field;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.system.BasicComponent;
import epros.designer.util.JecnValidateCommon;

/**
 * 文本框的组件
 * 
 * @author user
 * 
 */
public class TextField implements BasicComponent {
	/** 流程输入Lab */
	private JLabel textFieldLab = new JLabel();
	/** 流程输入Field */
	private JecnTextField textField = new JecnTextField();
	private JScrollPane textAreaScrollPane = new JScrollPane(textField);

	private String text;

	/** 提示名称 */
	private String tipName;

	public TextField(int rows, JPanel infoPanel, Insets insets, String labName, String text, String tipName) {
		this.tipName = tipName;
		this.text = text == null ? "" : text;
		textFieldLab.setText(labName);
		textField.setText(text);
		// 输入
		textField.setBorder(null);
		textAreaScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		textAreaScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		textAreaScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// ==============流程输入==========================================
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(textFieldLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(textAreaScrollPane, c);
	}

	public boolean isUpdate() {
		if (!text.equals(textField.getText())) {
			return true;
		}
		return false;
	}

	public String getResultStr() {
		return textField.getText().trim();
	}

	public boolean isValidateNotPass(JLabel verfyLab) {
		return JecnValidateCommon.validateFieldNotPass(textField.getText(), verfyLab, this.tipName);
	}

	@Override
	public boolean isRequest() {
		return false;
	}

	@Override
	public void setEnabled(boolean enable) {
		
	}
}
