package epros.designer.gui.integration.risk;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/***
 * 风险选择框
 * 2013-11-11
 *
 */
public class RiskChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(RiskChooseDialog.class);
	public RiskChooseDialog(List<JecnTreeBean> list) {
		super(list, 21);
	}
	
	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public JecnTree getJecnTree() {
		return new HighEfficiencyRiskTree();
	}

	@Override
	public String getSearchLabelName() {
		/*
		 * 风险点名称
		 */
		return JecnProperties.getValue("riskTitleC");
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title=new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("riskTitle"));//风险点名称：
		return title;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			List<JecnTreeBean> listJTree = null;
			listJTree = ConnectionPool.getJecnRiskAction().searchRiskByName(name,null);
			return listJTree;
		} catch (Exception e) {
			log.error("RiskChooseDialog searchByName is error",e);
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			return new ArrayList<JecnTreeBean>();
		}
	}

}
