package com.jecn.epros.server.webBean.standard;

import java.util.List;

/**
 * 标准清单
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-16 时间：下午02:01:45
 */
public class StandardWebInfoBean {
	/** 清单数据集合 */
	private List<StandardInfoListBean> listInfoBean;
	/** 清单最大级别 */
	private int maxLevel;
	/** 当前节点级别 */
	private int curLevel;

	public int getCurLevel() {
		return curLevel;
	}

	public void setCurLevel(int curLevel) {
		this.curLevel = curLevel;
	}

	public List<StandardInfoListBean> getListInfoBean() {
		return listInfoBean;
	}

	public void setListInfoBean(List<StandardInfoListBean> listInfoBean) {
		this.listInfoBean = listInfoBean;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}
}
