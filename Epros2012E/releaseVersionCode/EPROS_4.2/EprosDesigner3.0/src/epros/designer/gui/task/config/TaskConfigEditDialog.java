package epros.designer.gui.task.config;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 
 * 流程、文件、制度关联任务审批配置项
 * 
 * @author 2012-06-06
 * 
 */
public class TaskConfigEditDialog extends TaskConfigDialog {

	
	private TaskConfigDialog parentDialog=null;

	public TaskConfigEditDialog(TaskConfigDialog parentDialog, String templetId, int taskType) {
		super(templetId, taskType, true);
		this.parentDialog=parentDialog;
	}

	@Override
	protected String getSelectButtonName() {

		return JecnProperties.getValue("editMode");
	}
	
	@Override
	protected void selectAction() {
		TaskTempletManageDialog pmd = new TaskTempletManageDialog(this,taskType);
		pmd.setVisible(true);
	}

	@Override
	protected void saveAction() {
		try {
			if(check()){
				ConnectionPool.getTaskRecordAction().updateTaskConfigItems(super.getConfigs());
				if(parentDialog!=null){
					parentDialog.initData();
				}
				
				this.dispose();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
	}
	
	@Override
	public void buttonEditable(boolean editable){
		taskEditPanel.getButtonPanel().buttonEditable(editable);
		okBut.setEnabled(editable);
		taskEditPanel.getDocControlLeadRadioButton().setEnabled(editable);
		taskEditPanel.getDocNotControlLeadRadioButton().setEnabled(editable);
	}

	private boolean check() {
		return this.getTaskEditPanel().check();
	}

}
