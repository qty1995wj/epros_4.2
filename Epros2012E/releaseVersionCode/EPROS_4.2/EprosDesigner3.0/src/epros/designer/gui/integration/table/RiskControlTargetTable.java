package epros.designer.gui.integration.table;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.integration.JecnControlTarget;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/***
 * 新建风险点===控制目标===table
 * 2013-11-01
 *
 */
public class RiskControlTargetTable extends JecnTable {
	private List<JecnControlTarget> controlTargetList = new ArrayList<JecnControlTarget>();
	
	public RiskControlTargetTable(List<JecnControlTarget> controlTargetList){
		this.controlTargetList = controlTargetList;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		//设置第一列宽度为0
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		int[] cols1 = new int[] { 1 };
		//设置第一列宽度为0
		if (cols1 != null && cols1.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols1) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(100);
				tableColumn.setMaxWidth(100);
			}
		}
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}
	public JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("sort"));
		title.add(JecnProperties.getValue("controlTarget"));
		return new JecnTableModel(title,getContent(controlTargetList));
	}
	public Vector<Vector<String>> getContent(List<JecnControlTarget> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (int i = 0; i<list.size();i++) {
				JecnControlTarget jecnControlTarget = list.get(i);
				Vector<String> data = new Vector<String>();
				data.add(jecnControlTarget.getId().toString());
				data.add(String.valueOf(i+1));
				data.add(jecnControlTarget.getDescription());
				content.add(data);
			}
		}
		return content;
	}
	public void remoeAll() {
		// 清空
		for (int index = this.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) this.getModel()).removeRow(index);
		}
	}

	public int[] gethiddenCols() {
		return new int[] { 0 };
	}

	public boolean isSelectMutil() {
		return true;
	}
	
}
