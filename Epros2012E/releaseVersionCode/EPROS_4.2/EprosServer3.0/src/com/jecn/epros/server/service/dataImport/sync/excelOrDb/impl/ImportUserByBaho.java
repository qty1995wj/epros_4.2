package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jecn.epros.server.common.HttpRequestUtil;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

public class ImportUserByBaho extends AbstractImportUser {

	private static final Logger log = Logger.getLogger(ImportUserByBaho.class);
	private List<DeptBean> depts = new ArrayList<DeptBean>();
	private List<UserBean> users = new ArrayList<UserBean>();

	public ImportUserByBaho() {
		getBahoJson();
	}

	
	
	public ImportUserByBaho(AbstractConfigBean configBean) {
		getBahoJson();
	}



	@Override
	public List<DeptBean> getDeptBeanList() throws Exception {
		return depts;
	}

	@Override
	public List<UserBean> getUserBeanList() throws Exception {
		return users;
	}

	public void getBahoJson() {
//		String jsonString = HttpRequestUtil.sendGet("http://47.99.165.110:9006/bahoinfo/page/bahoinfo/bahoUser/info/backJson", "");
		String jsonString = HttpRequestUtil.sendGet("http://47.99.165.110:9006/bahoinfo/page/bahoinfo/bahoUser/info/backJson", "");
		JSONObject jsonObj = JSONObject.parseObject(jsonString);
		JSONArray userList = jsonObj.getJSONArray("userList");
		JSONArray deptList = jsonObj.getJSONArray("deptList");

		DeptBean deptBean = null;
		for (Iterator iterator = deptList.iterator(); iterator.hasNext();) {
			JSONObject job = (JSONObject) iterator.next();
			deptBean = new DeptBean();
			deptBean.setDeptNum(job.get("id").toString());
			deptBean.setDeptName(job.get("name").toString());
			if("-1".equals(job.get("parentId").toString())){
				deptBean.setPerDeptNum("1");
				deptBean.setDeptNum("-1");
			}else{
				deptBean.setPerDeptNum(job.get("parentId").toString());
			}
			
			depts.add(deptBean);
		}
		DeptBean root = new DeptBean();
		root.setDeptName("贝豪婴童");
		root.setDeptNum("1");
		root.setPerDeptNum("0");
		depts.add(root);

		for (Iterator iterator = userList.iterator(); iterator.hasNext();) {
			JSONObject job = (JSONObject) iterator.next();
			UserBean userBean = null;
			userBean = new UserBean();
			userBean.setUserNum(job.get("name").toString());
			userBean.setTrueName(job.get("name").toString());
			userBean.setPhone(job.get("mobile").toString());
//			if("张三".equals(job.get("name").toString())) continue;
			
			// 添加岗位ID
			if (null == job.get("department") || "".equals(job.get("department").toString())) {
				// userBean.setDeptNum(job.get("department").toString());
				System.out.println(job.get("name").toString() + "没有部门");
			} else {
				JSONArray depts = JSONArray.parseArray(job.get("department").toString());
				for (Iterator d = depts.iterator(); d.hasNext();) {
					String dn = d.next().toString();
					if("".equals(dn)){
						continue;
					}
					if("0".equals(dn)){
						dn = "-1";
					}
					UserBean userJobBean = new UserBean();
					userJobBean.setUserNum(userBean.getUserNum());
					userJobBean.setTrueName(userBean.getTrueName());
					userJobBean.setDeptNum(dn);
					if (null != job.get("position") && !"".equals(job.get("position").toString())) {
						userJobBean.setPosNum(dn + job.get("position").toString());
						userJobBean.setPosName(job.get("position").toString());
					}
					users.add(userJobBean);
				}
			}
		}
	}

	public static void main(String[] args) {
		// getBahoJson();
		ImportUserByBaho baho = new ImportUserByBaho();
		try {
			List a = baho.getUserBeanList();
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
