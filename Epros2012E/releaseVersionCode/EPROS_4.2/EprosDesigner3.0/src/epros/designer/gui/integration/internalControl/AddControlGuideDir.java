package epros.designer.gui.integration.internalControl;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 创建内控指引知识库目录
 * 
 * @author Administrator
 * 
 */
public class AddControlGuideDir extends JecnEditNameDialog {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(AddControlGuideDir.class);

	/** 树节点 */
	private JecnTreeNode pNode = null;
	/** 树 */
	private JTree jTree = null;

	/**
	 * 创建内控指引知识库目录构造方法
	 * 
	 * @param pNode
	 *            树节点
	 * @param jTree
	 *            树
	 */
	public AddControlGuideDir(JecnTreeNode pNode, JTree jTree) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
	}

	/**
	 * 标题
	 */
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("addDirI");
	}

	/**
	 * 目录名称
	 */
	@Override
	public String getNameLab() {
		return JecnProperties.getValue("dirNameC");
	}

	/**
	 * 添加数据
	 */
	@Override
	public void saveData() {
		// 向内控指引知识库Bean中添加数据
		JecnControlGuide jecnControlGuide = new JecnControlGuide();
		// 目录名称
		jecnControlGuide.setName(getName());
		// 创建人
		jecnControlGuide.setCreatePerson(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 更新人
		jecnControlGuide.setUpdatePerson(JecnConstants.loginBean.getJecnUser().getPeopleId());
		// 父ID
		jecnControlGuide.setParentId(pNode.getJecnTreeBean().getId());
		// 序号
		jecnControlGuide.setSort(JecnTreeCommon.getMaxSort(pNode));
		// 类型，0目录，1条款
		jecnControlGuide.setType(0);
		// 目录创建时间
		jecnControlGuide.setCreateTime(new Date());
		// 更新时间
		jecnControlGuide.setUpdateTime(new Date());
		jecnControlGuide.setProjectId(JecnConstants.projectId);

		// 向数据库表中添加数据
		try {
			Long id = ConnectionPool.getControlGuideAction().addJecnControlGuideDir(jecnControlGuide);
			// 向树节点添加内控指引知识库目录
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(jecnControlGuide.getName());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			jecnTreeBean.setTreeNodeType(TreeNodeType.innerControlDir);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			this.dispose();
		} catch (Exception e) {
			log.error("AddControlGuideDir saveData is error", e);
		}
	}

	/**
	 * 验证是否重名
	 */
	@Override
	public boolean validateNodeRepeat(String name) throws Exception {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name, TreeNodeType.innerControlDir);
	}

	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}

}
