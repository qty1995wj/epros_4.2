<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
	</head>

	<body>
		<div id="peronImportMain_id" style="width: 100%; height: 100%"></div>
	</body>
</html>
