package com.jecn.epros.server.download.excel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.rule.RuleInfoBean;
import com.jecn.epros.server.webBean.rule.RuleSystemListBean;

/**
 * 制度清单Excel导出
 * 
 */
public class RuleTemplateDownload {
	/** 级别 */
	private static String level = "级";
	/** 统计 */
	private static String total = "统计：";
	private static final Logger log = Logger.getLogger(RuleTemplateDownload.class);
	/**
	 * 制度清单
	 * 
	 * @param processSystemListBean
	 * @return
	 * 
	 * 修改人：chehuanbo
	 * 描述：制度清单显示样式修改
	 * @since V3.06
	 */
	public static byte[] createExcelile(RuleSystemListBean ruleSystemListBean,
			int ruleSysLevel) throws Exception {
		WritableWorkbook wbookData = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path
					+ JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			File file = new File(filePath);
			wbookData = Workbook.createWorkbook(file);
			// Sheet1名字
			WritableSheet wsheet = wbookData.createSheet("制度清单", 0);
            
			//设置标题字体样式
			WritableFont fontTitle = new WritableFont(WritableFont.TIMES, 11,
					WritableFont.BOLD); //字体加粗
			WritableCellFormat wformatTitle=SetExcelStyle.setExcelCellStyle(new ExcelFont(fontTitle,Colour.YELLOW));
			//设置字体标题特殊样式
			WritableCellFormat wformatTitle2=SetExcelStyle.setExcelCellStyle(new ExcelFont(fontTitle,Colour.LIGHT_ORANGE));
			
			//设置字体样式（红色）
			WritableFont fontStyle1 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.RED);
			WritableCellFormat wformatContecnt1=SetExcelStyle.setExcelCellStyle(new ExcelFont(fontStyle1,Colour.WHITE));
			//设置字体样式（橙色）
			WritableFont fontStyle2 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.LIGHT_ORANGE);
			WritableCellFormat wformatContecnt2=SetExcelStyle.setExcelCellStyle(new ExcelFont(fontStyle2,Colour.WHITE));
			//设置字体样式（绿色）
			WritableFont fontStyle3 = new WritableFont(WritableFont.createFont("宋体"), 11, WritableFont.NO_BOLD, false,
					UnderlineStyle.NO_UNDERLINE, Colour.GREEN);
			WritableCellFormat wformatContecnt3=SetExcelStyle.setExcelCellStyle(new ExcelFont(fontStyle3,Colour.WHITE));
			//设置字体样式（普通）
			WritableFont fontStyle4 = new WritableFont(WritableFont.TIMES, 11,
					WritableFont.NO_BOLD);
			WritableCellFormat wformatContecnt4=SetExcelStyle.setExcelCellStyle(new ExcelFont(fontStyle4,Colour.WHITE));
			
			// 获取总的级别个数
			int levelTotal = ruleSystemListBean.getLevelTotal();
			int count = 1;
			//设置标题的行高
			wsheet.setRowView(count,440);
			for (int i = 0; i < levelTotal; i++) {// 创建级别标题
				if(i==0){
					// 创建除级别外的标题
					wsheet.addCell(new Label(0,1, "", wformatTitle));
				}
				if (i >= ruleSysLevel) {
					wsheet.addCell(new Label(count,1, i + level,
									wformatTitle));
					wsheet.setColumnView(i, 35);
					count++;
				}
			}
			// 创建除级别外的标题
			//设置标题背景颜色（橘黄）
			wsheet.addCell(new Label(count, 1, "制度名称", wformatTitle2));
			wsheet.addCell(new Label(count + 1, 1, "制度编号", wformatTitle2));
			//设置标题背景颜色（黄色）
			wsheet.addCell(new Label(count + 2, 1, "责任部门", wformatTitle));
			wsheet.addCell(new Label(count + 3, 1, "版本号", wformatTitle));
			wsheet.addCell(new Label(count + 4, 1, "发布日期", wformatTitle));
			
			//设置列宽
			wsheet.setColumnView(count, 25);
			wsheet.setColumnView(count + 1, 25);
			wsheet.setColumnView(count + 2, 25);
			wsheet.setColumnView(count + 3, 25);
			wsheet.setColumnView(count + 4, 25);
            //样式集合
			List<WritableCellFormat> wformatContentList=new ArrayList<WritableCellFormat>();
			wformatContentList.add(0, wformatContecnt1);
			wformatContentList.add(1, wformatContecnt2);
			wformatContentList.add(2, wformatContecnt3);
			wformatContentList.add(3, wformatContecnt4);
			// 添加数据
			addSheetData(ruleSystemListBean, wsheet,wformatContentList, ruleSysLevel);
			// 写入Exel工作表
			wbookData.write();
			// 关闭Excel工作薄对象
			wbookData.close();
			wbookData=null;
			
			// 把一个文件转化为字节
			byte[] tempDate = JecnUtil.getByte(file);
			if (file.exists()) {
				file.delete();
			}
			return tempDate;
		} catch (Exception e) {
			log.error("生成excel出现异常 RuleTempoateDownload createExcelile方法中",e);
		}finally {
			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("关闭WritableWorkbook异常", e);
				}
			}
		}
		return null;
	}
	

	
	

	/**
	 * 添加到处数据
	 * 
	 * @param processSystemListBean
	 * @param wsheet
	 * @param wformatTitle
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private static void addSheetData(RuleSystemListBean ruleSystemListBean,
			WritableSheet wsheet, List<WritableCellFormat> writableCellFormats,
			int ruleSysLevel) throws RowsExceededException, WriteException {
		// 获取制度统计数据集合
		List<RuleInfoBean> ruleInfoBeanList = ruleSystemListBean
				.getListRuleInfoBean();
		// 制度级别总数
		int levelTotal = ruleSystemListBean.getLevelTotal();
		//*********************不同制度数据和显示
		int counts = 0;
		for (int j = 0; j < levelTotal; j++) {
			if (j >= ruleSysLevel) {
				wsheet.addCell(new Label(counts, 0, "", writableCellFormats.get(3)));
				counts++;
			}
		}
		wsheet.setRowView(0,400);  //设置行高
		wsheet.addCell(new Label(0, 0, "", writableCellFormats.get(3)));
		wsheet.setColumnView(0,5); //设置列宽
		
		//总计
		wsheet.addCell(new Label(1, 0, JecnUtil.getValue("total") + "("
				+ ruleSystemListBean.getRuleTotal() + ")", writableCellFormats.get(3)));
		wsheet.setColumnView(1,25); //设置列宽
		
		// 发布日期 待建制度
		wsheet.addCell(new Label(2, 0, "待建制度"
				+ "(" + ruleSystemListBean.getNoPubRuleTotal() + ")",
				writableCellFormats.get(0)));
		wsheet.setColumnView(2,25); //设置列宽
		
		// 有效日期 待审批制度
		wsheet.addCell(new Label(3, 0, "待审批制度"
				+ "(" + ruleSystemListBean.getApproveRuleTotal() + ")",
				writableCellFormats.get(1)));
		wsheet.setColumnView(3,25); //设置列宽
		
		// 是否及时优化 发布制度
		wsheet.addCell(new Label(4, 0, "发布制度"
				+ "(" + ruleSystemListBean.getPubRuleTotal() + ")",
				writableCellFormats.get(2)));
		wsheet.setColumnView(4,45); //设置列宽
		//=========数据显示============
		
		int count = 2;    //第几行
		int rowNum = 1; // 行号
		
		for (int i = 0; i < ruleInfoBeanList.size(); i++) {
			//每条制度的所有父目录集合
			List<ExcelRuleBean> listRuleInfoBeans=new ArrayList<ExcelRuleBean>();  
			listRuleInfoBeans.clear(); //每循环一次将集合清空，主要是防止信息累加导致级别显示不正确
			wsheet.setRowView(count,360); //设置行高
			RuleInfoBean ruleInfoBean = ruleInfoBeanList.get(i);
			
			if (ruleInfoBean.getIsDir() == 0) {
				continue;
			}else {
				// 添加行号
				wsheet.addCell(new Label(0, count, String.valueOf(rowNum),
						writableCellFormats.get(3)));
				ExcelBean excelBean = new ExcelBean();
				excelBean.setRuleInfoBean(ruleInfoBean);//当前制度
				excelBean.setRuleSysLevle(ruleSysLevel);//当前制度的最大级别 0级：最大   
				excelBean.setRuleSystemListBean(ruleSystemListBean); //所有的制度集合 包含目录，制度
				excelBean.setLevelTotal(levelTotal);//级别总数
				excelBean.setListRuleInfoBeans(listRuleInfoBeans);//当前制度所有的父级目录集合  
				findPerDir(excelBean); //递归方法 （查找每条制度的所有父目录）
			}
			
			//添加到每条制度的所有父目录到excel中
			
			//获取每条制度所有的父目录长度
			int f=listRuleInfoBeans.size()-1;
			
			for (int j = 1; j<=levelTotal; j++) {
				ExcelRuleBean excelRuleBean = null;
				if (f >= 0) {
					excelRuleBean = listRuleInfoBeans.get(f);
					if (excelRuleBean != null
							&& !("").equals(excelRuleBean.getRuleName())) {
						// 添加级别名称
						wsheet.addCell(new Label(j, count, excelRuleBean
								.getRuleName(), writableCellFormats.get(3)));
					}
					f--;
				}else {  
					// 添加级别名称  
					wsheet.addCell(new Label(j, count, "", writableCellFormats.get(3)));
				}
			}
		    //============================
			
			if (ruleInfoBean.getIsDir() != 0) {
				WritableCellFormat cellFormat=null;
				//不同的制度状态，字体颜色不同
				if(ruleInfoBean.getTypeByData()==0){ //待建制度
					cellFormat=writableCellFormats.get(0);
				}else if(ruleInfoBean.getTypeByData()==1){ //待审批制度
					cellFormat=writableCellFormats.get(1);
				}else if(ruleInfoBean.getTypeByData()==2){//发布制度
					cellFormat=writableCellFormats.get(2);
				}
				// 制度名称
				wsheet.addCell(new Label(levelTotal - ruleSysLevel+1, count,
						ruleInfoBean.getRuleName(),cellFormat));
			}
			// 制度编号
			wsheet.addCell(new Label(levelTotal - ruleSysLevel + 2, count,
					ruleInfoBean.getRuleNum(), writableCellFormats.get(3)));
			// 责任部门
			wsheet.addCell(new Label(levelTotal - ruleSysLevel + 3, count,
					ruleInfoBean.getDutyOrg(), writableCellFormats.get(3)));
			// 版本号
			wsheet.addCell(new Label(levelTotal - ruleSysLevel + 4, count,
					ruleInfoBean.getVersionId(), writableCellFormats.get(3)));
			// 发布日期
			wsheet.addCell(new Label(levelTotal - ruleSysLevel + 5, count,
					ruleInfoBean.getPubDate(), writableCellFormats.get(3)));
			count++;
			rowNum++;
		}
	}
	
	/**
	 * 递归查找,当前制度的所有父级目录并且添加到execel中
	 * 
	 * @author chehuanbo
	 * @date 2014-10-24
	 * @param excelBean  
	 * @since V3.06
	 * 
	 */
	public static void findPerDir(ExcelBean excelBean) {
		
		try {
			// 用来标记是否达到最大级别结束递归，num:-1达到最大级别
			int num = 0;
			//获取制度详细信息
			RuleInfoBean ruleInfoBean = excelBean.getRuleInfoBean();
			//查找当前制度的所有父目录，并且存放到ExcelRuleBean中
			for (RuleInfoBean ruleInfoPerBean : excelBean
					.getRuleSystemListBean().getListRuleInfoBean()) {
				if (ruleInfoBean.getRulePerId() == ruleInfoPerBean.getRuleId()) {
					//制度父目录封装bean
					ExcelRuleBean excelRuleBean = new ExcelRuleBean();
					//制度编号,暂时没有用到
					excelRuleBean.setRuleNum(ruleInfoPerBean.getRuleId());
					//制度名称
					excelRuleBean.setRuleName(ruleInfoPerBean.getRuleName());
					//添加到制度父目录集合中
					excelBean.getListRuleInfoBeans().add(excelRuleBean);
         					
					ruleInfoBean = ruleInfoPerBean;
					excelBean.setRuleInfoBean(ruleInfoPerBean);
					//达到最大级别，修改num的状态
					if (ruleInfoPerBean.getLevel() == excelBean
							.getRuleSysLevle()) {
						num = -1;
					}
					break;
				}
			}
			// 如果没有达到最大级别继续查找 num:-1达到最大级别
			if (num != -1) {
				findPerDir(excelBean); // 递归查找
			}
		} catch (Exception e) {
			log.error("递归查找当前制度的上级目录的出现异常,RuleAction中的findPerDir()", e);
		}
	}
	
}
  



/***
 * 参数封装类 
 * 由于递归方法所需要的参数太多，由此出现该类
 * @author chehuanbo
 * @since V3.06
 */
class ExcelBean {
	private RuleInfoBean ruleInfoBean = null; // 制度基本信息bean
	private int ruleSysLevle; // 制度级别
	private RuleSystemListBean ruleSystemListBean = null;// 制度集合bean
    private int levelTotal; //级别总数
    //封装制度父目录集合
    private List<ExcelRuleBean> listRuleInfoBeans=null;
   
	public int getLevelTotal() {
		return levelTotal;
	}

	public void setLevelTotal(int levelTotal) {
		this.levelTotal = levelTotal;
	}

	public RuleInfoBean getRuleInfoBean() {
		return ruleInfoBean;
	}

	public void setRuleInfoBean(RuleInfoBean ruleInfoBean) {
		this.ruleInfoBean = ruleInfoBean;
	}

	public int getRuleSysLevle() {
		return ruleSysLevle;
	}

	public void setRuleSysLevle(int ruleSysLevle) {
		this.ruleSysLevle = ruleSysLevle;
	}

	public RuleSystemListBean getRuleSystemListBean() {
		return ruleSystemListBean;
	}

	public void setRuleSystemListBean(RuleSystemListBean ruleSystemListBean) {
		this.ruleSystemListBean = ruleSystemListBean;
	}


	public List<ExcelRuleBean> getListRuleInfoBeans() {
		return listRuleInfoBeans;
	}

	public void setListRuleInfoBeans(List<ExcelRuleBean> listRuleInfoBeans) {
		this.listRuleInfoBeans = listRuleInfoBeans;
	}
	
	
	
	
}

