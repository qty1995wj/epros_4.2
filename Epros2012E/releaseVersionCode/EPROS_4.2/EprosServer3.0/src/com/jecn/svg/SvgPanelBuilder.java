package com.jecn.svg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.util.JecnUtil;
import com.jecn.svg.dataModel.SvgBaseGraph;
import com.jecn.svg.dataModel.SvgPanel;
import com.jecn.svg.dataModel.SvgPanel.PanelType;
import com.jecn.svg.dataModel.epros.SvgEprosGraphData;
import com.jecn.svg.dataModel.epros.SvgEprosLineData;
import com.jecn.svg.dataModel.graphs.SvgGraph;
import com.jecn.svg.dataModel.graphs.SvgLinkIcon;
import com.jecn.svg.dataModel.line.SvgDividingLine;

public class SvgPanelBuilder {

	/** 所有figure元素集合 */
	private List<SvgEprosGraphData> eprosGraphResource;
	private List<SvgLinkIcon> linkIcons;
	private Object[] processInfo;
	private EprosConverter converter;
	private PanelType panelType;
	/** 参与的元素集合 */
	private Set<Long> participationElementIds;
	/** 活动信息化 */
	private List<Object[]> listJecnActiveOnLineTBean;

	public SvgPanelBuilder(List<SvgEprosGraphData> eprosGraphResource, List<SvgEprosLineData> mLineResource,
			List<SvgLinkIcon> linkIcons, List<Object[]> processInfo, PanelType panelType,
			Set<Long> participationElementIds, List<Object[]> listJecnActiveOnLineTBean, List<Object[]> listPosts) {
		this.eprosGraphResource = eprosGraphResource;
		this.converter = new EprosConverter(mLineResource, linkIcons, eprosGraphResource, listPosts);
		this.linkIcons = linkIcons;
		this.panelType = panelType;
		this.participationElementIds = participationElementIds;
		this.listJecnActiveOnLineTBean = listJecnActiveOnLineTBean;
		if (processInfo.size() == 1) {
			this.processInfo = processInfo.get(0);
		}
	}

	/**
	 * 进行数据转换
	 * 
	 * @return
	 */
	public SvgPanel buildSvgPanel(int languageType) {
		SvgPanel svgPanel = getSvgPanel();
		List<SvgBaseGraph> baseGraphs = new ArrayList<SvgBaseGraph>();
		Map<Long, SvgGraph> graphs = new HashMap<Long, SvgGraph>();
		double maxX = 0, maxY = 0, roleMove = 0;
		sortResource();
		for (SvgEprosGraphData eprosGraph : eprosGraphResource) {
			// 设置活动信息化
			getOnLineName(eprosGraph);
			SvgBaseGraph baseGraph = converter.newInstanceAndInitializationWithEprosGraph(eprosGraph, languageType);
			if (baseGraph instanceof SvgGraph) {
				SvgGraph graph = (SvgGraph) baseGraph;
				graphs.put(baseGraph.getFigureId(), graph);
				maxX = maxX < graph.getX() + graph.getWidth() ? graph.getX() + graph.getWidth() : maxX;
				maxY = maxY < graph.getY() + graph.getHeight() ? graph.getY() + graph.getHeight() : maxY;
				// 取消掉元素具有阴影效果时对数据层的修改
				// graph.resetWidthAndHeightByStyle();
				graph.setMyLink(participationElementIds.contains(graph.getFigureId()) ? true : false);
			} else if (baseGraph instanceof SvgDividingLine) {
				SvgDividingLine dvLine = (SvgDividingLine) baseGraph;
				if (svgPanel.getIsVerticalFlow()) {
					if (dvLine.getType().equals("ParallelLines")) {
						if (roleMove == 0) {
							roleMove = dvLine.getStartY();
						}
						roleMove = roleMove < dvLine.getStartY() ? dvLine.getStartY() : roleMove;
					}
				} else {
					if (dvLine.getType().equals("VerticalLine") && dvLine.getStartY() == 0) {
						if (roleMove == 0) {
							roleMove = dvLine.getStartX();
						}
						roleMove = roleMove > dvLine.getStartX() ? dvLine.getStartX() : roleMove;
					}
				}
			}
			baseGraphs.add(baseGraph);
		}
		// 获取曼哈顿连接线开始点，并排序连接线小线段集合

		setGraphLinkIcons(graphs);
		return svgPanel.setWidth(maxX + 110).setHeight(maxY + 110).setRoleMoveEndX(roleMove).setBaseGraphs(baseGraphs);
	}

	private SvgPanel getSvgPanel() {
		SvgPanel svgPanel = new SvgPanel();
		svgPanel.setPanelType(panelType);
		if (processInfo == null) {
			return svgPanel;
		}
		if (processInfo[0] != null) {
			svgPanel.setFlowName(processInfo[0].toString());
		}
		if (processInfo[1] != null) {
			svgPanel.setIsVerticalFlow("1".equals(processInfo[1].toString()) ? true : false);
		}
		if (processInfo[2] != null) {
			svgPanel.setFlowImagePath(processInfo[2].toString());
		}
		if (processInfo[3] != null) {
			svgPanel.setPreFlowId(processInfo[3].toString());
		}
		if (processInfo[4] != null) {
			svgPanel.setPreFlowType("1".equals(processInfo[4].toString()) ? "process" : "processMap");
		}
		if (processInfo[5] != null) {
			svgPanel.setFlowIdInput(processInfo[5].toString());
		}

		svgPanel.setShowName(JecnUtil.getShowName(svgPanel.getFlowName(), svgPanel.getFlowIdInput()));
		return svgPanel;
	}

	/**
	 * 排序元数据
	 */
	private void sortResource() {
		Collections.sort(eprosGraphResource, new Comparator<SvgEprosGraphData>() {
			@Override
			public int compare(SvgEprosGraphData o1, SvgEprosGraphData o2) {
				return o1.getOrderIndex().compareTo(o2.getOrderIndex());
			}
		});
	}

	/**
	 * 设置图标
	 * 
	 * @param graphs
	 */
	private void setGraphLinkIcons(Map<Long, SvgGraph> graphs) {
		for (SvgLinkIcon icon : linkIcons) {
			graphs.get(icon.getFigureId()).addLinkIcon(icon);
		}
	}

	private void getOnLineName(SvgEprosGraphData eprosGraph) {
		if (!JecnUtil.isActive(eprosGraph.getFigureType())) {
			return;
		}
		String onLineName = "";
		for (Object[] objects : listJecnActiveOnLineTBean) {
			if (eprosGraph.getIsOnLine() == 1 && eprosGraph.getFigureId().toString().equals(objects[0].toString())) {
				if (StringUtils.isEmpty(onLineName)) {
					onLineName = objects[1].toString();
				} else {
					onLineName = onLineName + "/" + objects[1].toString();
				}
			}
		}
		eprosGraph.setOnLineName(onLineName);
	}

}
