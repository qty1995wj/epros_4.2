package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ButtonCommendEnum;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.bottom.JecnOKCancelJButtonPanel;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.dialog.JecnAddItemJDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnTableScrollPane;
import epros.designer.util.JecnProperties;

/**
 * 
 * 加入按钮弹出对话框中主面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnAddItemPanel extends JecnAbstractPropertyBasePanel implements ActionListener {
	/** 配置界面 */
	private JecnAddItemJDialog addItemJDialog = null;
	/** 确认取消按钮面板 */
	private JecnOKCancelJButtonPanel okCancelBtnAddItemPanel = null;
	/** 0:加入成功，1：加入失败： */
	private int intFlag = 0;
	/** 输入和输出Value 值 */
	private String inOutValue = null;
	/** 输入 */
	private String inValue = null;
	/** 输出 */
	private String outValue = null;

	/** 流程边界 */
	private String processBoundary = null;

	public JecnAddItemPanel(JecnAbtractBaseConfigDialog dialog, JecnAddItemJDialog addItemJDialog) {
		super(dialog);
		this.addItemJDialog = addItemJDialog;

		initComponents();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	protected void initComponents() {
		// 表面板
		tableScrollPane = new JecnTableScrollPane(JecnProperties.getValue("task_TableHeaderName"), dialog);
		// 确认取消按钮面板
		okCancelBtnAddItemPanel = new JecnOKCancelJButtonPanel(dialog);

		this.add(tableScrollPane, BorderLayout.CENTER);
		this.add(okCancelBtnAddItemPanel, BorderLayout.SOUTH);

		// 事件
		okCancelBtnAddItemPanel.getOkJButton().addActionListener(this);
		okCancelBtnAddItemPanel.getCancelJButton().addActionListener(this);
		// 按钮命令
		okCancelBtnAddItemPanel.getOkJButton().setActionCommand(ButtonCommendEnum.addOKBtn.toString());
		okCancelBtnAddItemPanel.getCancelJButton().setActionCommand(ButtonCommendEnum.addCancelBtn.toString());

		// 初始化值
		initData(dialog.getSelectedPropContPanel().getConfigTypeDesgBean());
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {

		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null) {
			return;
		}
		// 过滤隐藏项排序
		// 显示表数据
		for (JecnConfigItemBean itemBean : configTypeDesgBean.getTableItemList()) {
			if (ConfigItemPartMapMark.a5.toString().equals(itemBean.getMark())) {// 输入
				inValue = itemBean.getValue();
			} else if (ConfigItemPartMapMark.a6.toString().equals(itemBean.getMark())) {// 输出
				outValue = itemBean.getValue();
			} else if (ConfigItemPartMapMark.a32.toString().equals(itemBean.getMark())) {// 输入和输出
				inOutValue = itemBean.getValue();
			} else if (ConfigItemPartMapMark.a33.toString().equals(itemBean.getMark())) {// 流程范围
				processBoundary = itemBean.getValue();
			}
		}
		tableScrollPane.initData(tableScrollPane.getHiddItem(configTypeDesgBean.getTableItemList(), configTypeDesgBean
				.getType()));
	}

	/**
	 * 
	 * 添加项到主界面属性面板中
	 * 
	 */
	private List<JecnConfigItemBean> addItem() {

		// 获取选中行数据
		List<JecnConfigItemBean> selectedItemBeanList = this.tableScrollPane.getSelectedItemBeanList();

		if (selectedItemBeanList == null || selectedItemBeanList.size() == 0) {
			return null;
		}

		Set<String> exists = new HashSet<String>();
		for (JecnConfigItemBean itemBean : selectedItemBeanList) {
			if (ConfigItemPartMapMark.a5.toString().equals(itemBean.getMark())
					|| ConfigItemPartMapMark.a6.toString().equals(itemBean.getMark())
					|| ConfigItemPartMapMark.a32.toString().equals(itemBean.getMark())
					|| ConfigItemPartMapMark.a33.toString().equals(itemBean.getMark())) {
				exists.add(itemBean.getMark());
			}
		}
		if (exists.size() > 1) {
			// 仅当size是两个时，并且只包含a5 a6是正确的，其它情况都是错误的
			boolean access = exists.size() == 2 && exists.contains(ConfigItemPartMapMark.a5.toString())
					&& exists.contains(ConfigItemPartMapMark.a6.toString());
			if (!access) {
				intFlag = 1;
				return null;
			}
		}

		// 修改状态
		for (JecnConfigItemBean itemBean : selectedItemBeanList) {
			// 存在输入、输出、输入和输出
			if (ConfigItemPartMapMark.a5.toString().equals(itemBean.getMark())
					|| ConfigItemPartMapMark.a6.toString().equals(itemBean.getMark())
					|| ConfigItemPartMapMark.a32.toString().equals(itemBean.getMark())
					|| ConfigItemPartMapMark.a33.toString().equals(itemBean.getMark())) {

				if (ConfigItemPartMapMark.a5.toString().equals(itemBean.getMark())
						|| ConfigItemPartMapMark.a6.toString().equals(itemBean.getMark())) {// 添加的是输入或输出
					if (JecnConfigContents.ITEM_IS_SHOW.equals(inOutValue)
							|| JecnConfigContents.ITEM_IS_SHOW.equals(processBoundary)) {
						intFlag = 1;
						return null;
					}
				} else if (ConfigItemPartMapMark.a32.toString().equals(itemBean.getMark())) {// 添加的是输入和输出
					if (JecnConfigContents.ITEM_IS_SHOW.equals(inValue)
							|| JecnConfigContents.ITEM_IS_SHOW.equals(outValue)
							|| JecnConfigContents.ITEM_IS_SHOW.equals(processBoundary)) {
						intFlag = 1;
						return null;
					}
				} else if (ConfigItemPartMapMark.a33.toString().equals(itemBean.getMark())) {// 添加的是流程范围
					if (JecnConfigContents.ITEM_IS_SHOW.equals(inValue)
							|| JecnConfigContents.ITEM_IS_SHOW.equals(outValue)
							|| JecnConfigContents.ITEM_IS_SHOW.equals(inOutValue)) {
						intFlag = 1;
						return null;
					}
				}
				itemBean.setValue(JecnConfigContents.ITEM_IS_SHOW);
			} else {
				itemBean.setValue(JecnConfigContents.ITEM_IS_SHOW);
			}
		}

		// 主属性面板中的表对象
		JecnTableScrollPane propertyTableScrollPane = this.dialog.getSelectedPropContPanel().getTableScrollPane();

		if (propertyTableScrollPane == null || configTypeDesgBean == null) {
			return null;
		}

		// 显示数据
		List<JecnConfigItemBean> showItemList = tableScrollPane.getShowItem(configTypeDesgBean.getTableItemList(),
				configTypeDesgBean.getType());
		propertyTableScrollPane.initData(showItemList);

		// 重新加载属性面板
		dialog.getPropertyPanel().reShowProperty();

		// 重新设置选中行
		propertyTableScrollPane.reSetSelectedItems(selectedItemBeanList);

		return showItemList;
	}

	/**
	 * 
	 * 点击事件处理方法
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if (ButtonCommendEnum.addOKBtn.toString().equals(btn.getActionCommand())) {// 确认按钮
				okActionPerformed(e);
			} else if (ButtonCommendEnum.addCancelBtn.toString().equals(btn.getActionCommand())) {// 取消按钮
				cancelActionPerformed(e);
			}
		}

	}

	/**
	 * 
	 * 确认按钮处理方法
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void okActionPerformed(ActionEvent e) {
		// 添加选中项到属性面板上
		List<JecnConfigItemBean> selectedItemBeanList = addItem();
		if (selectedItemBeanList == null || selectedItemBeanList.size() == 0) {
			if (intFlag == 1) {
				/*
				 * [输入、输出]、[输入和输出]、[流程范围]只能显示一项！
				 */
				okCancelBtnAddItemPanel.getInfoLabel().setText(JecnProperties.getValue("showIsOnep"));
				return;
			}
			okCancelBtnAddItemPanel.getInfoLabel().setText(JecnProperties.getValue("task_selectedRowsInfo"));
			return;
		} else {
			// 关闭加入对话框
			addItemJDialog.setVisible(false);
		}
	}

	/**
	 * 
	 * 取消按钮处理方法
	 * 
	 * @param e
	 *            ActionEvent
	 */
	private void cancelActionPerformed(ActionEvent e) {
		addItemJDialog.setVisible(false);
	}

	@Override
	public boolean check() {
		return false;
	}
}