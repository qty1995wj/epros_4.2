package com.jecn.epros.server.emailnew;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.email.buss.ToolEmail;

public class ProcessTestRunEmailBuilder extends FixedContentEmailBuilder {

	@Override
	protected EmailBasicInfo getEmailBasicInfo() {

		Object[] data = getData();
		JecnTaskBeanNew taskBeanNew = (JecnTaskBeanNew) data[0];
		int type = Integer.valueOf(data[1].toString().toString());

		return createEmail(taskBeanNew, type);
	}

	/**
	 * 试运行邮件提醒
	 * 
	 * @param jecnUser
	 * @param jecnTaskBeanNew
	 * @param type
	 *            0试运行报告邮件提醒，1：试运行到期提醒
	 */
	private EmailBasicInfo createEmail(
			JecnTaskBeanNew taskBeanNew, int type) {

		// 每个流程参与者都要接收邮件，点击邮件连接地址直接查看流程
		EmailBasicInfo basicInfo = new EmailBasicInfo();
		String subject = "";
		// 邮件内容
		String emailContent = "";
		if (type == 0) {
			// 请填写试运行报告！
			subject = ToolEmail.PLEASE_FILL_REPORT;
			// "任务" + taskBeanNew.getTaskName() + "试运行已发布，请填写试运行报告！<br>" +
			// getTaskDesc(taskBeanNew)
			emailContent = ToolEmail.TASK_TITLE + '"'
					+ taskBeanNew.getTaskName() + '"'
					+ ToolEmail.TEST_RUN_IS_PUFLIC + EmailContentBuilder.strBr
					+ ToolEmail.getTaskDesc(taskBeanNew);

		} else {
			// 任务试运行已到期！
			subject = ToolEmail.TEST_RUN_IS_EXPIRED;
			// "任务" + taskBeanNew.getTaskName() + "试运行已结束！ <br>" +
			// getTaskDesc(taskBeanNew)
			emailContent = ToolEmail.TASK_TITLE + '"'
					+ taskBeanNew.getTaskName() + '"'
					+ ToolEmail.TEST_RUN_IS_END + EmailContentBuilder.strBr
					+ ToolEmail.getTaskDesc(taskBeanNew);
		}

		String httpUrl = ToolEmail.getTestRunHttpUrl();
		EmailContentBuilder builder = new EmailContentBuilder();
		builder.setContent(emailContent);
		builder.setResourceUrl(httpUrl);
		basicInfo.setContent(builder.buildContent());
		basicInfo.setSubject(subject);

		if (type == 0) {
			if (JecnCommon.isNullOrEmtryTrim(ToolEmail.testRunFilePath)) {
				ToolEmail.testRunFilePath = ToolEmail.getSyncPath();
			}
			// 获取文件
			File file = new File(ToolEmail.testRunFilePath);
			if (file.exists()) {// 存在 添加附件
				List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
				EmailAttachment attachment = new EmailAttachment();
				attachment.setAttachment(file);
				attachment.setAttachmentName(ToolEmail.TASK_RUN_FILE);
				attachments.add(attachment);
				basicInfo.setAttachments(attachments);
			}
		}
		return basicInfo;

	}

}
