package com.jecn.epros.server.webBean.integration;

/**
 * 风险系统探索Bean
 * @author zhouhf
 *
 */
public class RiskSystemSearchBean {
	
	/** 风险名称ID */
	private long id;
	/** 风险名称 */
	private String name;
	/** 风险编号 */
	private String riskCode;
	/** 风险等级 */
	private String gradeStr;
	/**风险对应流程的责任部门名称*/
	private String orgName;
	/**风险对应流程的责任部门ID*/
	private Long orgId;
	
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRiskCode() {
		return riskCode;
	}
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}
	public String getGradeStr() {
		return gradeStr;
	}
	public void setGradeStr(String gradeStr) {
		this.gradeStr = gradeStr;
	}

}
