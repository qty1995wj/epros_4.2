package com.jecn.epros.controller;

import com.jecn.epros.EprosServerApplication;
import com.jecn.epros.bean.*;
import com.jecn.epros.bean.TaskSearchBean;
import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.ViewResult;
import com.jecn.epros.domain.common.abolish.AbolishBean;
import com.jecn.epros.domain.process.SearchBean;
import com.jecn.epros.domain.task.*;
import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.IMixRPCService;
import com.jecn.epros.service.ITaskRPCService;
import com.jecn.epros.service.task.TaskService;
import com.jecn.epros.service.user.UserService;
import com.jecn.epros.util.AttachmentDownloadUtils;
import com.jecn.epros.util.JecnProperties;
import com.jecn.epros.util.Params;
import com.jecn.epros.util.Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "/task", description = "任务管理", position = 7)
public class TaskController {

    @Autowired
    private TaskService taskService;
    @Autowired
    private ITaskRPCService taskRPCService;
    @Autowired
    private IMixRPCService mixRPCService;
    @Autowired
    private UserService userService;

    @PreAuthorize("hasAuthority('tasks')")
    @ApiOperation(value = "获取任务列表")
    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public ResponseEntity<TaskViewBean> findTasksByParam(@ModelAttribute TaskSearchTempBean params,
                                                         @ModelAttribute Paging paging) {
        Long curPeopleId = AuthenticatedUserUtil.getPeopleId();
        Boolean isAdmin = AuthenticatedUserUtil.isAdmin();
        Boolean isSecondAdmin = AuthenticatedUserUtil.isSecondAdmin();

        TaskViewBean viewBean = new TaskViewBean();
        // 根据查询条件获取登录人相关任务
        List<MyTaskBean> tasks = new ArrayList<MyTaskBean>();
        // 获取任务查询条件
        int total = taskService.getTaskCountBySearch(params, curPeopleId, isAdmin, isSecondAdmin);
        if (total > 0) {
            // 根据查询条件获取登录人相关任务
            tasks = taskService.fetchTasks(params, paging, curPeopleId, isAdmin, isSecondAdmin);
        }
        viewBean.setTotal(total);
        viewBean.setData(tasks);
        return ResponseEntity.ok(viewBean);
    }

    @ApiOperation(value = "返回任务展示信息")
    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.GET)
    public ResponseEntity<SimpleTaskBean> showTask(@PathVariable Long id, @RequestParam String taskOperation, @RequestParam(required = false) String isApp) {
        showTypeParamValidate(id, taskOperation);
        ResourceType operation = getResourceTypeByStr(taskOperation);
        SimpleTaskBean simpleTaskBean = taskService.getSimpleTaskBean(id, operation, isApp);

        return ResponseEntity.ok(simpleTaskBean);
    }

    @ApiOperation(value = "获取任务操作")
    @RequestMapping(value = "/tasks/{id}/operation", method = RequestMethod.GET)
    public ResponseEntity<ResourceType> getTaskOperation(@PathVariable Long id, @RequestParam(required = false) String isApp) {
        return ResponseEntity.ok(taskService.getTaskBean(id));
    }


    @ApiOperation(value = "点击任务删除链接")
    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> deleteTask(@PathVariable Long id) {
        try {
            taskRPCService.deleteSendInfos(id);
            taskService.markTaskToDelete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok().build();
    }


    @ApiOperation(value = "点击任务撤回链接")
    @RequestMapping(value = "/tasks/{id}/retract", method = RequestMethod.POST)
    public ResponseEntity<?> retractTask(@PathVariable Long id) {
        BaseTaskParam taskParam = new BaseTaskParam();
        taskParam.setOpinion(JecnProperties.getValue("nothing"));
        taskParam.setTaskId(String.valueOf(id));
        taskParam.setTaskOperation("13");
        Long curPeopleId = getCurPeopleId();
        MessageResult result = taskRPCService.callback(curPeopleId, taskParam);
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "通过，返回，打回，提交")
    @RequestMapping(value = "/tasks/{id}/approve", method = RequestMethod.POST)
    public ResponseEntity<?> approveTask(@PathVariable String id, @RequestBody BaseTaskParam baseForm) {
        Long curPeopleId = getCurPeopleId();
        MessageResult approve = taskRPCService.approve(curPeopleId, baseForm);
        return ResponseEntity.ok(approve);
    }

    @ApiOperation(value = "二次评审")
    @RequestMapping(value = "/tasks/{id}/second_review", method = RequestMethod.POST)
    public ResponseEntity<?> approveTaskTwo(@PathVariable String id, @RequestBody TaskTwoApproveParam submitParam) {
        Long curPeopleId = getCurPeopleId();
        MessageResult approve = taskRPCService.twoApprove(curPeopleId, submitParam);
        return ResponseEntity.ok(approve);
    }

    @ApiOperation(value = "转批，交办")
    @RequestMapping(value = "/tasks/{id}/reassign", method = RequestMethod.POST)
    public ResponseEntity<?> assignTask(@PathVariable String id, @RequestBody TaskAssignParam submitParam) {
        Long curPeopleId = getCurPeopleId();
        MessageResult approve = taskRPCService.assign(curPeopleId, submitParam);
        return ResponseEntity.ok(approve);
    }

    @ApiOperation(value = "文控主导审批,文控审批")
    @RequestMapping(value = "/tasks/{id}/dcc_submit", method = RequestMethod.POST)
    public ResponseEntity<?> controlSubmitTask(@PathVariable String id, @RequestBody TaskControlSubmitParam submitParam) {
        Long curPeopleId = getCurPeopleId();
        MessageResult approve = taskRPCService.controlSubmit(curPeopleId, submitParam);
        return ResponseEntity.ok(approve);
    }

    @ApiOperation(value = "拟稿人重新提交")
    @RequestMapping(value = "/tasks/{id}/resubmit", method = RequestMethod.POST)
    public ResponseEntity<?> reSubmitTask(@PathVariable String id, @RequestBody TaskReSubmitParam submitParam) {
        Long curPeopleId = getCurPeopleId();
        MessageResult approve = taskRPCService.resubmit(curPeopleId, submitParam);
        return ResponseEntity.ok(approve);
    }

    @ApiOperation(value = "管理员编辑打回、提交")
    @RequestMapping(value = "/tasks/{id}/edit", method = RequestMethod.POST)
    public ResponseEntity<?> editBackTask(@PathVariable String id, @RequestBody TaskEditSubmitParam submitParam) {
        Long curPeopleId = getCurPeopleId();
        if (!AuthenticatedUserUtil.isAdmin()) {
            return ResponseEntity.ok(new MessageResult(false, "Permission denied"));
        }
        MessageResult approve = taskRPCService.editRollBack(curPeopleId, submitParam);
        return ResponseEntity.ok(approve);
    }

//    @ApiOperation(value = "管理员编辑提交")
//    @RequestMapping(value = "/tasks/{id}/editSubmit", method = RequestMethod.POST)
//    public ResponseEntity<?> editSubmitTask(@ModelAttribute TaskEditSubmitParam taskParam) {
//        Long curPeopleId = getCurPeopleId();
//        MessageResult approve = taskRPCService.editSubmit(curPeopleId, taskParam);
//        return ResponseEntity.ok(approve);
//    }

    private Long getCurPeopleId() {
        return AuthenticatedUserUtil.getPeopleId();
    }

    @ApiOperation(value = "任务审批人替换", notes = "需要有管理员权限")
    @RequestMapping(value = "/tasks/user", method = RequestMethod.POST)
    public ResponseEntity<?> replaceApprovePeople(@RequestBody TaskReplacePeople people) {
        if (people.getFrom() == null || people.getTo() == null) {
            throw new EprosBussinessException("TaskController replaceApprovePeople is error ");
        }
        taskService.replaceApprovePeople(people.getFrom(), people.getTo());
        return ResponseEntity.ok(ViewResult.SUCCESS);
    }

    private static class People {
        Long from;
        Long to;

        public Long getFrom() {
            return from;
        }

        public void setFrom(Long from) {
            this.from = from;
        }

        public Long getTo() {
            return to;
        }

        public void setTo(Long to) {
            this.to = to;
        }
    }

    private ResourceType getResourceTypeByStr(String taskOperation) {
        ResourceType operation = ResourceType.valueOf(taskOperation);
        return operation;
    }

    private void showTypeParamValidate(Long taskId, String taskOperation) {
        if (taskId == null || StringUtils.isBlank(taskOperation)) {
            throw new IllegalArgumentException("TaskController showTypeParamValidate is error");
        }
        ResourceType operation = ResourceType.valueOf(taskOperation);
        List<ResourceType> resource = getTaskLawfulResourceTypes();
        if (!resource.contains(operation)) {
            throw new IllegalArgumentException("TaskController showTypeParamValidate is error");
        }
    }

    /**
     * 任务展示页面允许的链接的类型
     *
     * @return
     */
    private List<ResourceType> getTaskLawfulResourceTypes() {
        List<ResourceType> resources = new ArrayList<ResourceType>();
        resources.add(ResourceType.TASK_EDIT);
        resources.add(ResourceType.TASK_VIEW);
        resources.add(ResourceType.TASK_APPROVE);
        resources.add(ResourceType.TASK_RESUBMIT);
        resources.add(ResourceType.TASK_ORGANIZE);
        return resources;
    }

    @ApiOperation(value = "流程图建设规范")
    @RequestMapping(value = "/{processId}/norms", method = RequestMethod.GET)
    public ResponseEntity<?> flowNorms(@PathVariable Long processId) {
        if (processId == null) {
            throw new IllegalArgumentException("返回任务展示页面，参数非法");
        }

        boolean isNotHasError = taskService.flowNorms(processId);
        if (isNotHasError) {
            return ResponseEntity.ok("success");
        } else {
            return ResponseEntity.ok("error");
        }
    }

    @ApiOperation(value = "任务下载")
    @RequestMapping(value = "/tasks/download_task", method = RequestMethod.GET)
    public ResponseEntity<?> downloadTask(@ModelAttribute TaskSearchTempBean params, HttpServletRequest request) {
        TaskSearchBean searchBean = new TaskSearchBean();
        BeanUtils.copyProperties(params, searchBean);
        ByteFile byteFile = taskRPCService.downloadTask(AuthenticatedUserUtil.getPeopleId(), searchBean);
        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(byteFile));
    }

    @ApiOperation(value = "试运行文件下载")
    @RequestMapping(value = "/tasks/download_task_run_file", method = RequestMethod.GET)
    public ResponseEntity<?> downloadTaskRunFile(@RequestParam Long id, HttpServletRequest request) {
        TaskTestRunFile runFile = taskService.downloadTaskRunFile(id);
        if (runFile == null) {
            throw new EprosBussinessException("TaskController downloadTaskRunFile is error，id :" + id);
        }
        ByteFile byteFile = new ByteFile();
        byteFile.setBytes(runFile.getFileStream());
        byteFile.setFileName(runFile.getFileName());
        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(byteFile));
    }

    @ApiOperation(value = "审批过程中二次验证")
    @RequestMapping(value = "/tasks/two_validate", method = RequestMethod.POST)
    public ResponseEntity<?> validateEmailAuth(@RequestBody(required = true) TwoValidateParams params) {
        if (StringUtils.isBlank(params.getPwd())) {
            throw new EprosBussinessException("TaskController validateEmailAuth is error");
        }
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("peopleId", getCurPeopleId());
        JecnUser user = userService.getUser(paramMap);
        boolean isOuter = "outer".equalsIgnoreCase(user.getEmailType());
        MessageResult result = mixRPCService.validateEmailPassword(isOuter, user.getLoginName(), params.getPwd());
        return ResponseEntity.ok(result);
    }

    @PreAuthorize("hasAuthority('abolishManage')")
    @ApiOperation(value = "废止库")
    @RequestMapping(value = "/tasks/abolish", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MyAbolishBean> findAbolish(
            @ModelAttribute SearchBean searchBean,
            @ApiParam(value = "类型[-1:全部 0:流程 ;1：文件; 2:制度; 3:制度文件; 4：流程架构]", required = true, allowableValues = "-1,0,1,2,3,4")
            @RequestParam int type, @ModelAttribute Paging paging) {
        // TODO 文件权限校验
        Params params = new Params("type", type).add("orgId", searchBean.getOrgId()).add("name", searchBean.getName()).add("paging", paging);
        List<AbolishBean> abolishBean = taskService.findAbolish(params.toMap());
        int count = taskService.findCount(params.toMap());
        MyAbolishBean myAbolish = new MyAbolishBean();
        myAbolish.setData(abolishBean);
        myAbolish.setTotal(count);
        return ResponseEntity.ok(myAbolish);
    }


}
