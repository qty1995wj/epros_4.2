package com.jecn.epros.domain.process;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.InventoryCellStyle;
import com.jecn.epros.domain.InventoryTable;
import com.jecn.epros.domain.report.InventoryBaseInfo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Inventory implements Serializable {

    /**
     * 已发布
     **/
    private Integer pubNum = null;
    /**
     * 待建
     **/
    private Integer notPubNum = null;
    /**
     * 待审批
     **/
    private Integer approveNum = null;
    /**
     * 总数 (如果是流程包含流程架构以及流程的总数)
     */
    private Integer totalNum = null;
    // 开始节点LEVLE
    private int startLevel = 0;
    // 最大目录节点LEVEL;
    private int maxLevel = 0;
    /**
     * 部门
     **/
    private Integer orgNum;
    /**
     * 清单内容表
     **/
    private InventoryTable data = null;

    /**
     * 流程的总数
     **/
    private Integer processNum = null;
    /**
     * 流程架构的总数
     **/
    private Integer mapNum = null;

    public Inventory() {
        data = new InventoryTable();
    }

    public Inventory(InventoryBaseInfo baseInfo) {
        this.maxLevel = baseInfo.getMaxLevel();
        this.notPubNum = baseInfo.getNotPub();
        this.pubNum = baseInfo.getPub();
        this.approveNum = baseInfo.getApprove();
        this.totalNum = baseInfo.getTotal();
    }


    @JsonIgnore
    private Map<String, List<InventoryCellStyle>> styleMap;

    public Map<String, List<InventoryCellStyle>> getStyleMap() {
        return styleMap;
    }

    public void setStyleMap(Map<String, List<InventoryCellStyle>> styleMap) {
        this.styleMap = styleMap;
    }

    public Integer getProcessNum() {
        return processNum;
    }

    public void setProcessNum(Integer processNum) {
        this.processNum = processNum;
    }

    public Integer getMapNum() {
        return mapNum;
    }

    public void setMapNum(Integer mapNum) {
        this.mapNum = mapNum;
    }

    public Integer getPubNum() {
        return pubNum;
    }

    public void setPubNum(Integer pubNum) {
        this.pubNum = pubNum;
    }

    public Integer getNotPubNum() {
        return notPubNum;
    }

    public void setNotPubNum(Integer notPubNum) {
        this.notPubNum = notPubNum;
    }

    public Integer getApproveNum() {
        return approveNum;
    }

    public void setApproveNum(Integer approveNum) {
        this.approveNum = approveNum;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    @JsonIgnore
    public void setTotalNum() {
        totalNum = notPubNum + approveNum + pubNum;
    }


    public Integer getOrgNum() {
        return orgNum;
    }

    public void setOrgNum(Integer orgNum) {
        this.orgNum = orgNum;
    }

    public InventoryTable getData() {
        return data;
    }

    public void setData(InventoryTable data) {
        this.data = data;
    }

    public int getStartLevel() {
        return startLevel;
    }

    public void setStartLevel(int startLevel) {
        this.startLevel = startLevel;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

}
