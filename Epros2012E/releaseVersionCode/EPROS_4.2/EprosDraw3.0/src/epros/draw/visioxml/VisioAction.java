package epros.draw.visioxml;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.top.toolbar.JecnToolBarPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUserCheckUtil;
import epros.draw.visio.JecnFileNameFilter;

public class VisioAction {
	private static final Log log = LogFactory.getLog(VisioAction.class);

	/**
	 * 导出visio文件
	 */
	public void outPut() {
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;
		try {
			// 获取默认路径
			String pathUrl = JecnSystemData.readCurrSystemFileIOData().getSaveVisioDirectory();
			// 获取截取后正确的路径
			if (pathUrl != null && !"".equals(pathUrl)) {
				pathUrl = pathUrl.substring(0, pathUrl.lastIndexOf("\\"));
			}

			// 文件选择器
			JecnFileChooser fileChooser = new JecnFileChooser(pathUrl);

			// 移除系统给定的文件过滤器
			fileChooser.setAcceptAllFileFilterUsed(false);
			// 过滤文件
			fileChooser.setFileFilter(new JecnFileNameFilter(new String[] { "vdx" }));
			// 设置标题“：导出visio文件
			fileChooser.setDialogTitle(JecnResourceUtil.getJecnResourceUtil().getValue("outputVisio"));
			JecnDrawDesktopPane deskPanel = JecnDrawMainPanel.getMainPanel().getWorkflow();
			// 文件名称获取当前面板名称
			fileChooser.setSelectedFile(new File(deskPanel.getTabbedTitlePanel().getTabText()));
			int i = fileChooser.showSaveDialog(null);
			// 选择确认（yes、ok）后返回该值。
			if (i == JFileChooser.APPROVE_OPTION) {
				fileChooser.getTextFiled().getText();
				// 获取输出文件的路径
				String filePath = fileChooser.getSelectedFile().getPath();
				// 修改配置文件中的路径
				JecnSystemData.readCurrSystemFileIOData().setSaveVisioDirectory(filePath);
				JecnSystemData.writeCurrSystemFileIOData();
				String error = JecnUserCheckUtil.checkFileName(fileChooser.getSelectedFile().getName());
				if (!"".equals(error)) {
					JecnOptionPane.showMessageDialog(null, error, "", JecnOptionPane.ERROR_MESSAGE);
					return;
				}
				// 判断文件后缀名是否为vdx结尾
				if (!filePath.endsWith(".vdx")) {
					filePath = filePath + ".vdx";
				}
				// 判断文件是否存在
				File f = new File(filePath);
				if (f.exists()) {
					int result = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnResourceUtil
							.getJecnResourceUtil().getValue("theCurrentFileAlreadyExistsIsCovered"), JecnResourceUtil
							.getJecnResourceUtil().getValue("confirm"), JOptionPane.YES_NO_OPTION);
					if (result != JOptionPane.YES_NO_OPTION) {
						return;
					}
				}
				VisioOutPut visioOutPut = new VisioOutPut(deskPanel.getFlowMapData());
				visioOutPut.export(filePath);
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(JecnDrawMainPanel.getMainPanel(), JecnResourceUtil.getJecnResourceUtil()
					.getValue("outPutVisioError"));
			log.error("导出VISIO文件异常！！！", e);
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					log.error("关闭缓冲流异常！", e);
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("关闭文件输出流异常！", e);
				}
			}
		}

	}
}
