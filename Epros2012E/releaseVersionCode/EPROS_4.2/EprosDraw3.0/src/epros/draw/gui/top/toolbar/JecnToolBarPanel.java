package epros.draw.gui.top.toolbar;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.button.JecnToolbarTitleButton;
import epros.draw.gui.top.toolbar.right.JecnToolBarRightShowPanel;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏
 * 
 * @author ZHOUXY
 * 
 */
public class JecnToolBarPanel extends JecnPanel implements ItemListener {
	/** 工具栏标题 */
	private JecnToolbarTitlePanel toolbarTitle = null;

	/** 工具标内容总面板下：常用面板 */
	private JecnOftenPartPanel oftenPartPanel = null;
	/** eachPartPanel的容器 */
	private JScrollPane eachPartScrollPane = null;
	/** 工具标内容总面板下：各自大类显示具体内容 */
	private JecnPanel eachPartPanel = null;
	/** eachPartPanel的子组件：直接存放文件、编辑、格式、帮助面板 */
	private JPanel subEachPartPanel = null;
	/** 工具栏最右面面板：执行滚动eachPartScrollPane按钮 */
	private JecnToolBarRightShowPanel rightShowPanel = null;

	/** 文件类 */
	private JecnFileToolBarItemPanel fileToolBarItemPanel = null;
	/** 编辑类 */
	private JecnEidtToolBarItemPanel editToolBarItemPanel = null;
	/** 格式类 */
	private JecnFormatToolBarItemPanel formatToolBarItemPanel = null;
	/** 流程操作说明类 */
	private JecnOperInstrToolBarItemPanel operInstrToolBarItemPanel = null;
	/** EPROS流程管理平台 */
	private JecnEprosMangToolBarItemPanel eprosMangToolBarItemPanel = null;
	/** 帮助类 */
	private JecnHelpToolBarItemPanel helpToolBarItemPanel = null;

	public JecnToolBarPanel() {
		initComponents();
		initEvent();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void initComponents() {
		// 工具栏标题
		toolbarTitle = new JecnToolbarTitlePanel(this);

		// 工具标内容总面板下：常用面板
		oftenPartPanel = new JecnOftenPartPanel(this);
		// eachPartPanel的容器
		eachPartScrollPane = new JScrollPane();
		// 工具标内容总面板下：各自大类显示具体内容
		eachPartPanel = new JecnPanel();
		// eachPartPanel的子组件：直接存放文件、编辑、格式、帮助面板
		subEachPartPanel = new JPanel();
		// 工具栏最右面面板：执行滚动eachPartScrollPane按钮
		rightShowPanel = new JecnToolBarRightShowPanel(this);

		// 文件类
		fileToolBarItemPanel = new JecnFileToolBarItemPanel(this);
		// 编辑类
		editToolBarItemPanel = new JecnEidtToolBarItemPanel(this);
		// 格式类
		formatToolBarItemPanel = new JecnFormatToolBarItemPanel(this);
		// 流程操作说明类
		operInstrToolBarItemPanel = new JecnOperInstrToolBarItemPanel(this);
		// EPROS流程管理平台
		eprosMangToolBarItemPanel = new JecnEprosMangToolBarItemPanel(this);
		// 帮助
		helpToolBarItemPanel = new JecnHelpToolBarItemPanel(this);

		// 文件
		addTitleButton(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.fileTitle), fileToolBarItemPanel);
		// // 编辑
		addTitleButton(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.editTitle), editToolBarItemPanel);
		// 格式
		addTitleButton(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.formatTitle),
				formatToolBarItemPanel);

		// if (EprosType.eprosDraw == JecnSystemStaticData.getEprosType()) {//
		// 画图面板
		// TooBarTitleNumEnum num =
		// JecnSystemStaticData.getTooBarTitleNumEnum();
		// if (TooBarTitleNumEnum.six == num) {
		// // 流程文件
		// addTitleButton(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.operTitle),
		// operInstrToolBarItemPanel);
		// // EPROS流程管理平台
		// addTitleButton(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.eprosMagTitle),
		// JecnResourceUtil.getJecnResourceUtil()
		// .getValue("eprosMagTitleDetail"), eprosMangToolBarItemPanel);
		// } else if (TooBarTitleNumEnum.five == num) {
		// // 流程文件
		// addTitleButton(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.operTitle),
		// operInstrToolBarItemPanel);
		// }
		// }

		// 帮助
		addTitleButton(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.helpTitle), helpToolBarItemPanel);

		// 期望大小
		this.setPreferredSize(JecnUIUtil.getTootBarSize());
		// 最小大小
		this.setMinimumSize(JecnUIUtil.getTootBarSize());
		// 边框
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 0));

		// 横竖滚动条不显示
		eachPartScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		eachPartScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		eachPartScrollPane.setOpaque(false);
		eachPartScrollPane.setBorder(null);
		Dimension size = new Dimension(800, 15);
		eachPartScrollPane.setPreferredSize(size);
		eachPartScrollPane.setMinimumSize(size);

		// 无边框
		eachPartPanel.setBorder(null);

		// eachPartPanel的子组件：直接存放文件、编辑、格式、帮助面板
		// 布局管理器
		subEachPartPanel.setLayout(new BorderLayout());
		subEachPartPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		subEachPartPanel.setBorder(null);

		// 初始化工具栏上内容面板
		initTwoContentPanel();
		// 布局eachPartPanel面板下的子组件
		initEachPartPanel();

		// 显示文件面板
		toolbarTitle.setSelectedTitleButton(toolbarTitle.getTitleDataList().get(0).getTitleButton());

		subEachPartPanel.add(fileToolBarItemPanel);
		eachPartScrollPane.setViewportView(eachPartPanel);

		// 默认
		rightShowPanel.setVisible(false);

	}

	/**
	 * 
	 * 初始化事件
	 * 
	 */
	private void initEvent() {
		// 实现工具栏内容显示不全时，出现移动按钮
		this.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				componentResizedProcess();
			}
		});

		// 工具标内容总面板下：各自大类显示具体内容
		eachPartPanel.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				componentResizedProcess();
			}
		});
	}

	/**
	 * 
	 * 初始化工具栏内容
	 * 
	 */
	private void initTwoContentPanel() {
		// 工具栏常用面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets3(), 0, 0);
		this.add(oftenPartPanel, c);

		// 工具标内容总面板下：各自大类显示具体内容
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets3(), 0, 0);
		this.add(eachPartScrollPane, c);
		// 工具栏最右面面板：执行滚动eachPartScrollPane按钮
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets3(), 0, 0);
		this.add(rightShowPanel, c);
	}

	/**
	 * 
	 * 布局eachPartPanel面板下的子组件
	 * 
	 */
	private void initEachPartPanel() {
		// eachPartPanel的子组件：直接存放文件、编辑、格式、帮助面板
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets0(), 0, 0);
		eachPartPanel.add(subEachPartPanel, c);
		// 隐藏标签：为了获取多余空间
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				JecnUIUtil.getInsets0(), 0, 0);
		eachPartPanel.add(new JLabel(), c);
	}

	/**
	 * 
	 * 添加工具栏标题按钮
	 * 
	 * @param text
	 *            标题按钮名称
	 * @param jpanel
	 *            标题按钮对应的工具栏面板
	 */
	public void addTitleButton(String text, JPanel jpanel) {
		if (jpanel == null) {
			return;
		}
		// 创建工具栏标题按钮
		JecnToolbarTitleButton btn = new JecnToolbarTitleButton(toolbarTitle, text);
		toolbarTitle.addButton(new JecnToolBarTitleData(btn, jpanel));
	}

	/**
	 * 
	 * 添加工具栏标题按钮
	 * 
	 * @param text
	 *            标题按钮名称
	 * @param tipText
	 *            标题按钮提示名称
	 * @param jpanel
	 *            标题按钮对应的工具栏面板
	 */
	public void addTitleButton(String text, String tipText, JPanel jpanel) {
		if (jpanel == null) {
			return;
		}
		// 创建工具栏标题按钮
		JecnToolbarTitleButton btn = new JecnToolbarTitleButton(toolbarTitle, text, tipText);
		toolbarTitle.addButton(new JecnToolBarTitleData(btn, jpanel));
	}

	/**
	 * 
	 * 添加工具栏标题按钮
	 * 
	 * @param text
	 *            标题按钮名称
	 * @param jpanel
	 *            标题按钮对应的工具栏面板
	 */
	public void insertTitleButton(String text, JPanel jpanel, int index) {
		if (jpanel == null) {
			return;
		}
		// 创建工具栏标题按钮
		JecnToolbarTitleButton btn = new JecnToolbarTitleButton(toolbarTitle, text);
		toolbarTitle.insertButton(new JecnToolBarTitleData(btn, jpanel), index);
	}

	/**
	 * 
	 * 提示信息
	 * 
	 */
	void setToolBarBtnToolTipText(AbstractButton btn, String shortCut, String btnTextNote) {
		if (btn == null) {
			return;
		}
		String tmpShortCut = (shortCut == null) ? "" : shortCut;
		String tmpBtnTextNote = (btnTextNote == null) ? "" : btnTextNote;
		String tipText = "<html><b>" + btn.getText() + "(" + tmpShortCut + ")</b><br><br>"
				+ "<div style= 'white-space:normal; display:block;width:200px; word-break:break-all'>" + tmpBtnTextNote
				+ "</div><br><br></html>";

		btn.setToolTipText(tipText);
	}

	/**
	 * 
	 * 计算是否显示工具栏最右面按钮面板
	 * 
	 */
	private void componentResizedProcess() {
		// 工具栏宽
		int toobarW = JecnToolBarPanel.this.getWidth();
		// 工具栏常用面板宽
		int oftenW = oftenPartPanel.getWidth();
		// 当前大类面板宽
		int eachPartW = eachPartPanel.getWidth();

		// 工具栏宽小于指定的宽，且当前大类面板宽大于（工具栏面板-常用面板宽）
		if (toobarW < JecnUIUtil.getToolBarWidth() && eachPartW > (toobarW - oftenW)) {
			if (!rightShowPanel.isVisible()) {
				rightShowPanel.setVisible(true);
			}
		} else {
			if (rightShowPanel.isVisible()) {
				rightShowPanel.setVisible(false);
			}
		}
	}

	/**
	 * 
	 * 单选框选中事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
		// 格式面板
		this.formatToolBarItemPanel.itemStateChanged(e);
	}

	public JecnOftenPartPanel getOftenPartPanel() {
		return oftenPartPanel;
	}

	public JecnFileToolBarItemPanel getFileToolBarItemPanel() {
		return fileToolBarItemPanel;
	}

	public JecnEidtToolBarItemPanel getEditToolBarItemPanel() {
		return editToolBarItemPanel;
	}

	public JecnFormatToolBarItemPanel getFormatToolBarItemPanel() {
		return formatToolBarItemPanel;
	}

	public JecnHelpToolBarItemPanel getHelpToolBarItemPanel() {
		return helpToolBarItemPanel;
	}

	public JPanel getSubEachPartPanel() {
		return subEachPartPanel;
	}

	public JecnToolbarTitlePanel getToolbarTitle() {
		return toolbarTitle;
	}

	public JScrollPane getEachPartScrollPane() {
		return eachPartScrollPane;
	}

	public JecnPanel getEachPartPanel() {
		return eachPartPanel;
	}

}
