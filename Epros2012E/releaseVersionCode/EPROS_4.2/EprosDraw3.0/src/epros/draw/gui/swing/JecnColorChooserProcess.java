package epros.draw.gui.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 颜色选择器通用类
 * 
 * 对外接口：showColorChooserDialog此方法功能和JColorChooser的showDialog功能差不多
 * 
 * 通过选择颜色，点击确定按钮返回所选颜色；如果用户退出按钮，则返回 null
 * 
 * @author ZHOUXY
 * 
 */
public class JecnColorChooserProcess implements ActionListener {
	/** 对话框 */
	private JecnDialog dialog = null;
	/** 颜色选择器 */
	private JColorChooser colorChooser = null;
	/** 颜色选择器和按钮面板的容器 */
	private JPanel mainPanel = null;
	/** 按钮区域 */
	private JecnPanel btnPanel = null;
	/** 确定 */
	private JButton okBtn = null;
	/** 取消 */
	private JButton cancelBtn = null;
	/** 当前选择颜色 */
	private Color currColor = null;

	private JecnColorChooserProcess(String title, Color initialColor) {
		initComponents(title, initialColor);
		initLayout();
	}

	/**
	 * 
	 * 对外接口
	 * 
	 * 颜色选择器:返回所选颜色；如果用户退出，则返回 null
	 * 
	 * @param title
	 *            String 对话框标题
	 * @param initialColor
	 *            Color 颜色选择器初始值
	 * @return Color 返回所选颜色；如果用户退出，则返回 null
	 */
	public static Color showColorChooserDialog(String title, Color initialColor) {
		JecnColorChooserProcess dialogProcess = new JecnColorChooserProcess(title, initialColor);
		// 显示颜色选择器
		dialogProcess.dialog.setVisible(true);
		// 销毁
		dialogProcess.dialog.removeAll();
		dialogProcess.dialog.dispose();
		dialogProcess.dialog = null;
		return dialogProcess.currColor;
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 * @param title
	 *            String 对话框标题
	 * @param initialColor
	 *            Color 颜色选择器初始值
	 */
	private void initComponents(String title, Color initialColor) {
		// 对话框
		dialog = new JecnDialog();
		// 颜色选择器
		colorChooser = new JColorChooser();
		// 颜色选择器和按钮面板的容器
		mainPanel = new JPanel();
		// 按钮区域
		btnPanel = new JecnPanel();
		// 确认按钮
		okBtn = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("okBtn"));
		// 取消按钮
		cancelBtn = new JButton(JecnResourceUtil.getJecnResourceUtil().getValue("cancelBtn"));

		// 对话框大小
		Dimension size = new Dimension(500, 390);
		dialog.setSize(size);
		dialog.setMinimumSize(size);
		if (title != null) {
			dialog.setTitle(title);
		}
		// 居中显示
		dialog.setLocationRelativeTo(null);
		// 模态
		dialog.setModal(true);
		// 不支持拖动
		// dialog.setResizable(false);
		// 退出销毁
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 布局
		mainPanel.setLayout(new BorderLayout());

		// 初始颜色
		if (initialColor != null) {
			colorChooser.setColor(initialColor);
			currColor = initialColor;
		}
		// 修改颜色选择器背景颜色
		JecnUIUtil.setColorToSubComp(colorChooser);

		// 确定
		okBtn.addActionListener(this);
		okBtn.setActionCommand("okBtn");
		// okBtn.setPreferredSize(btnSize);

		// 取消
		cancelBtn.addActionListener(this);
		cancelBtn.setActionCommand("cancelBtn");

		// 添加组件
		dialog.getContentPane().add(mainPanel);
		mainPanel.add(colorChooser, BorderLayout.CENTER);
		mainPanel.add(btnPanel, BorderLayout.SOUTH);
	}

	/**
	 * 
	 * 布局
	 * 
	 */
	private void initLayout() {
		initButtomPanel();
	}

	private void initButtomPanel() {
		// 空闲区域
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.SOUTHEAST,
				GridBagConstraints.BOTH, JecnUIUtil.getInsets0(), 0, 0);
		btnPanel.add(new JLabel(), c);

		// 确定
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0555(), 0, 0);
		btnPanel.add(okBtn, c);

		// 取消
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				JecnUIUtil.getInsets0555(), 0, 0);
		btnPanel.add(cancelBtn, c);
	}

	/**
	 * 
	 * 按钮事件
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			if ("okBtn".equals(btn.getActionCommand())) {// 确认按钮
				if (colorChooser.getColor() != null) {
					currColor = colorChooser.getColor();
				}
				dialog.setVisible(false);
			} else if ("cancelBtn".equals(btn.getActionCommand())) {// 取消按钮
				currColor = null;
				dialog.setVisible(false);
			}
		}
	}
}
