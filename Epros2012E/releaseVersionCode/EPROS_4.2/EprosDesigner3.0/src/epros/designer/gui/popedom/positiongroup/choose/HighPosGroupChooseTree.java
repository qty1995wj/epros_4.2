package epros.designer.gui.popedom.positiongroup.choose;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.popedom.positiongroup.PosGroupCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class HighPosGroupChooseTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(HighPosGroupChooseTree.class);
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	private PosGropChooseDialog posGropChooseDialog = null;

	public HighPosGroupChooseTree(PosGropChooseDialog posGropChooseDialog) {
		this.posGropChooseDialog = posGropChooseDialog;
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new PosGroupChooseTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getPosGroup().getChildPositionGroup(Long.valueOf(0), JecnConstants.projectId);
		} catch (Exception e) {
			log.error("HighPosGroupChooseTree getTreeModel is error！", e);
		}
		return PosGroupCommon.getPosGroupTreeModel(list);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
