<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><s:text name="systemDetailedInformation" />
		</title>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css"
			href="${basePath}ext/resources/css/xtheme-gray.css" />
		<script type="text/javascript" src="${basePath}js/common.js"></script>
		<script type="text/javascript"
			src="${basePath}js/epros_${language}_${country}.js"></script>
		<script type="text/javascript"
			src="${basePath}ext/adapter/ext/ext-base.js"></script>
		<script type="text/javascript" src="${basePath}ext/ext-all.js"></script>
        <script type="text/javascript" src="${basePath}js/firstPage.js" charset="UTF-8"></script>
        <script type="text/javascript" src="${basePath}jquery/jquery-1.6.2.js"></script>
        <script type="text/javascript" src="${basePath}js/rule/singleRuleRtnlPropose.js" charset="UTF-8"></script>
		<script type="text/javascript">
		var versionType=${versionType};
		var isHiddenSystem = ${isHiddenSystem};
Ext.onReady(function(){
	var ruleFileTabPanel = new Ext.TabPanel({
			border : false,
			// resizeTabs:true,//True表示为自动调整各个标签页的宽度
			activeTab : 0,
			region : 'center',
			items : [{
						title : i_overviewInfo,// 制度体系内容
						id : "ruleSysContent_id",
						autoLoad : {
						    url:"${basePath}" + "getRuleContent.action?ruleId=${param.ruleId}",
						    scripts : true,
						    callback:function(){
									setShowRuleRelatedProcessWH('ruleRelatedProcessShowId');
							}
						}
								
					}, {
					title : i_relateFile,// 相关文件
					id : "ruleFileInfo_id",
					autoLoad : {
					    url:"${basePath}"
							+ "getRuleRelateFile.action?ruleId="
							+ "${param.ruleId}",
							scripts : true,
						callback : function() {
							setShowTaskProcessTemplate('ruleRelateFileId');
						}
					}
				},{
					title : i_rationalization,// 制度合理化建议
					id : "rtnlPropose_id",
					autoLoad : {
					    url: "${basePath}"+ "allRulertnlPropose.action?flowId="
							+"${param.ruleId}"+"&isFlowRtnl=1",
						callback : function() {
							onloadSingleRulePropose("${param.ruleId}",true,"${param.ruleName}");
						}
					}
				}
					]
		});
	if(${isDocControlPermission}||${webLoginBean.isAdmin} || ${webLoginBean.isViewAdmin}){//只有浏览管理员和系统管理员登录才可以显示制度清单
		ruleFileTabPanel.insert(3, {
					title : i_ruleHistoryInfo,// 制度文控信息
					id : "ruleHistoryInfo_id",
					autoLoad : {
						url : "${basePath}"
								+ "getRuleHistoryList.action?isRuleFile=true&ruleId="+"${param.ruleId}",
						callback : function() {
							setShowDocument('ruleDocumentDiv');
						}
					}
				});
	}
		var northPanle=new Ext.Panel({
		height:20,
		region : 'north',
		contentEl:'homePage_id'		
	});
		new Ext.Viewport({
		layout : 'border',
		items : [northPanle,ruleFileTabPanel]
	});
})


</script>
	</head>
	<body>
	    <div id="homePage_id"  style="height: 25px; width: 100%; overflow: hidden;"
			align="right" class="gradient minWidth">
			<%@include file="/navigation.jsp"%>
		</div>
	</body>
</html>