package epros.draw.event;

import java.awt.event.ActionEvent;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;

/**
 * 
 * 主面板监听的动作事件的基类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnAbstractActionProcess {
	protected abstract void actionPerformed(ActionEvent paramActionEvent,
			ToolBarElemType toolBarElemType);
}
