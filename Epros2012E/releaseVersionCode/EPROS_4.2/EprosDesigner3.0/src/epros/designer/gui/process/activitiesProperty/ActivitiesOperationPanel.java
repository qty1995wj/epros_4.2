package epros.designer.gui.process.activitiesProperty;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.table.JecnTableModel;
import epros.designer.util.JecnProperties;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.designer.JecnActivityFileT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 活动明细：操作规范
 * 
 * @author 2012-07-27
 * 
 */
public class ActivitiesOperationPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(ActivitiesOperationPanel.class);
	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel(450, 30);

	/** * 操作规范按钮 */
	private JButton operationBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** * 删除按钮 */
	private JButton deleteBut = new JButton(JecnProperties.getValue("deleteBtn"));

	/** * 打开文件按钮 */
	private JButton openBut = new JButton(JecnProperties.getValue("openFile"));

	/** 所在的滚动面板 */
	private JScrollPane activeOperationScrollPane = new JScrollPane();
	private JTable jecnTable = null;

	public JTable getJecnTable() {
		return jecnTable;
	}

	public void setJecnTable(JTable jecnTable) {
		this.jecnTable = jecnTable;
	}

	/** 设置大小 */
	private Dimension dimension = null;

	/** 活动的数据 */
	private JecnActiveData jecnActiveData;

	/** 操作规范文件 */
	private List<JecnTreeBean> listOPerationFile = null;

	/** 要删除的操作规范文件ID集合 */
	private List<Long> actOperationIds = null;

	/** 框的拥有者 */
	private JecnDialog jecnDialog;

	public List<Long> getActOperationIds() {
		return actOperationIds;
	}

	public ActivitiesOperationPanel(JecnActiveData jecnActiveData, JecnDialog jecnDialog) {
		this.jecnActiveData = jecnActiveData;
		this.jecnDialog = jecnDialog;

		if (JecnDesignerCommon.isShowEleAttribute()) {
			dimension = new Dimension(450, 80);
		} else {
			this.setSize(450, 350);
			dimension = new Dimension(450, 260);
		}

		activeOperationScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		activeOperationScrollPane.setPreferredSize(dimension);
		activeOperationScrollPane.setMaximumSize(dimension);
		activeOperationScrollPane.setMinimumSize(dimension);

		// 删除操作规范文件
		deleteBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteButFormed();
			}
		});

		/***********************************************************************
		 * 操作规范
		 */
		operationBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				operationButFormed();
			}
		});
		// 打开文件
		openBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 判断是否选中Table中的一行，没选中，提示选中一行
				int[] selectRows = jecnTable.getSelectedRows();
				if (selectRows.length == 1) {
					Long id = Long.valueOf((String) jecnTable.getValueAt(selectRows[0], 0));
					JecnDesignerCommon.openFile(id);
				} else {
					JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("chooseOneRow"));
				}
			}
		});
		initLayout();
	}

	public void initInfoPanelData(JecnActiveData jecnActiveData) {
		this.jecnActiveData = jecnActiveData;
		removeAllTable();
		Vector<Vector<String>> fileData = getContent();
		for (Vector<String> v : fileData) {
			((DefaultTableModel) jecnTable.getModel()).addRow(v);
		}
	}

	private void removeAllTable() {
		for (int index = jecnTable.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) jecnTable.getModel()).removeRow(index);
		}
	}

	private void initLayout() {
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 2, 2);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		this.add(activeOperationScrollPane, c);
		jecnTable = new JTable(getTableModel());
		// 设置默认背景色
		jecnTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		jecnTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		TableColumnModel columnModel = jecnTable.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(0);
		tableColumn.setMinWidth(0);
		tableColumn.setMaxWidth(0);
		activeOperationScrollPane.setViewportView(jecnTable);

		// buttonPanel
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		buttonPanel.add(operationBut, c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		buttonPanel.add(deleteBut, c);
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		buttonPanel.add(openBut, c);

	}

	/***************************************************************************
	 * 选择操作规范文件
	 */
	@SuppressWarnings("unchecked")
	private void operationButFormed() {
		listOPerationFile = new ArrayList<JecnTreeBean>();

		// 获取输入表格上的数据
		Vector<Vector<String>> listOperationFile = ((JecnTableModel) jecnTable.getModel()).getDataVector();
		if (listOperationFile != null) {
			for (Vector<String> vafs : listOperationFile) {
				JecnTreeBean jecnTreeBean = new JecnTreeBean();
				jecnTreeBean.setId(Long.valueOf(vafs.get(0)));
				jecnTreeBean.setName(vafs.get(1));
				listOPerationFile.add(jecnTreeBean);
			}
		}

		// 将输入表格上的数据传到文件选择框中显示
		FileChooseDialog fileChooseDialog = new FileChooseDialog(listOPerationFile, jecnDialog);
		// 只能选择一个文件
		fileChooseDialog.setSelectMutil(true);
		fileChooseDialog.setVisible(true);

		if (fileChooseDialog.isOperation()) {
			Vector<Vector<String>> fileData = ((JecnTableModel) fileChooseDialog.getResultTable().getModel())
					.getDataVector();
			for (int index = jecnTable.getModel().getRowCount() - 1; index >= 0; index--) {
				((DefaultTableModel) jecnTable.getModel()).removeRow(index);
			}
			for (Vector<String> v : fileData) {
				((DefaultTableModel) jecnTable.getModel()).addRow(v);
			}
		}
	}

	/***************************************************************************
	 * 删除操作规范文件
	 */
	@SuppressWarnings("unchecked")
	private void deleteButFormed() {
		int[] selectRows = jecnTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties
					.getValue("pleaseSelectDeleteOperationSpecificationDocuments"));
			return;
		}
		int dialog = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("isDeleteC"), null,
				JecnOptionPane.YES_NO_OPTION);
		if (dialog == JecnOptionPane.YES_OPTION) {
			Vector<Vector<String>> vectors = ((DefaultTableModel) jecnTable.getModel()).getDataVector();
			actOperationIds = new ArrayList<Long>();
			for (int i = 0; i < selectRows.length; i++) {
				for (int j = 0; j < vectors.size(); j++) {
					Vector<String> vector = vectors.get(i);
					actOperationIds.add(Long.valueOf(vector.get(0)));
				}
			}

			for (int i = selectRows.length - 1; i >= 0; i--) {
				((DefaultTableModel) jecnTable.getModel()).removeRow(selectRows[i]);
			}
		}
	}

	/***************************************************************************
	 * 初始化内容
	 * 
	 * @return
	 */
	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		// 获取流程记录保存数据
		try {
			if (jecnActiveData != null && jecnActiveData.getListJecnActivityFileT() != null) {
				for (JecnActivityFileT jecnActivityFileT : jecnActiveData.getListJecnActivityFileT()) {
					if (jecnActivityFileT.getFileType().intValue() == 1) {
						Vector<String> row = new Vector<String>();
						row.add(String.valueOf(jecnActivityFileT.getFileSId()));
						// 标准名称
						row.add(jecnActivityFileT.getFileName());
						vs.add(row);
					}
				}
			}

		} catch (Exception e) {
			log.error("ActivitiesOperationPanel getContent is error", e);
		}
		return vs;
	}

	/**
	 * 生成table的Model
	 * 
	 * @return
	 */
	private JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("fileName"));
		return new JecnTableModel(title, getContent());
	}

	public void setOperationResult() {
		// 获得行数
		int rowCount = jecnTable.getRowCount();
		JecnTableModel jecnTableModel = (JecnTableModel) jecnTable.getModel();
		// 记录活动输入文件ID
		List<Long> listIds = new ArrayList<Long>();
		// 添加和更新输入
		for (int i = 0; i < rowCount; i++) {
			// 如果ID为空 返回
			if (jecnTableModel.getValueAt(i, 0) == null || "".equals(jecnTableModel.getValueAt(i, 0).toString())) {
				return;
			}
			boolean isNewAdd = true;
			for (JecnActivityFileT jecnActivityFileT : jecnActiveData.getListJecnActivityFileT()) {
				if (jecnActivityFileT.getFileType().intValue() == 1) {
					if (jecnTableModel.getValueAt(i, 0).toString().equals(jecnActivityFileT.getFileSId().toString())) {
						jecnActivityFileT.setFileName(jecnTableModel.getValueAt(i, 1).toString());
						listIds.add(jecnActivityFileT.getFileSId());
						isNewAdd = false;
						break;
					}
				}
			}
			if (isNewAdd) {
				JecnActivityFileT jecnActivityFileT = new JecnActivityFileT();
				jecnActivityFileT.setFigureId(jecnActiveData.getFlowElementId());
				jecnActivityFileT.setFileSId(Long.valueOf(jecnTableModel.getValueAt(i, 0).toString()));
				jecnActivityFileT.setFileName(jecnTableModel.getValueAt(i, 1).toString());
				jecnActivityFileT.setFileType(1L);
				jecnActiveData.getListJecnActivityFileT().add(jecnActivityFileT);
				listIds.add(jecnActivityFileT.getFileSId());
			}
		}
		// 清除删除
		for (int i = jecnActiveData.getListJecnActivityFileT().size() - 1; i >= 0; i--) {
			JecnActivityFileT jecnActivityFileT = jecnActiveData.getListJecnActivityFileT().get(i);
			if (jecnActivityFileT.getFileType().intValue() == 1) {
				if (!listIds.contains(jecnActivityFileT.getFileSId())) {
					jecnActiveData.getListJecnActivityFileT().remove(i);
				}
			}
		}
	}

	public boolean isUpdate() {
		// 获得行数
		int rowCount = jecnTable.getRowCount();
		JecnTableModel jecnTableModel = (JecnTableModel) jecnTable.getModel();
		// 记录活动输入文件ID
		List<Long> listIds = new ArrayList<Long>();
		// 记录已存在的输入ID集合
		for (int i = jecnActiveData.getListJecnActivityFileT().size() - 1; i >= 0; i--) {
			JecnActivityFileT jecnActivityFileT = jecnActiveData.getListJecnActivityFileT().get(i);
			if (jecnActivityFileT.getFileType().intValue() == 1) {
				listIds.add(jecnActivityFileT.getFileSId());
			}
		}

		for (int i = 0; i < rowCount; i++) {
			// 如果ID为空 返回
			if (jecnTableModel.getValueAt(i, 0) == null || "".equals(jecnTableModel.getValueAt(i, 0).toString())) {
				continue;
			}

			Long newFileId = Long.valueOf(jecnTableModel.getValueAt(i, 0).toString());
			boolean isExist = false;
			if (listIds.contains(newFileId)) {
				isExist = true;
				// 删除存在的Id，剩余的ID集合为待删除的文件ID集合
				listIds.remove(newFileId);
			}
			if (!isExist) {
				// 不存在，当前ID为新增ID
				return true;
			}
		}
		if (listIds.size() > 0) {
			return true;
		}
		return false;
	}
}
