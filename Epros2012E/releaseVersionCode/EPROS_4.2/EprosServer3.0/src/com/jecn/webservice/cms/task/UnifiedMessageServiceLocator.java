/**
 * UnifiedMessageServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cms.task;

import com.jecn.epros.server.action.web.login.ad.cms.JecnCMSAfterItem;

public class UnifiedMessageServiceLocator extends org.apache.axis.client.Service implements com.jecn.webservice.cms.task.UnifiedMessageService {

    public UnifiedMessageServiceLocator() {
    }


    public UnifiedMessageServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public UnifiedMessageServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for UnifiedMessageServiceSoap12
	private java.lang.String UnifiedMessageServiceSoap12_address = JecnCMSAfterItem.getValue("TASK_SERVICE");

    public java.lang.String getUnifiedMessageServiceSoap12Address() {
        return UnifiedMessageServiceSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String UnifiedMessageServiceSoap12WSDDServiceName = "UnifiedMessageServiceSoap12";

    public java.lang.String getUnifiedMessageServiceSoap12WSDDServiceName() {
        return UnifiedMessageServiceSoap12WSDDServiceName;
    }

    public void setUnifiedMessageServiceSoap12WSDDServiceName(java.lang.String name) {
        UnifiedMessageServiceSoap12WSDDServiceName = name;
    }

    public com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType getUnifiedMessageServiceSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(UnifiedMessageServiceSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getUnifiedMessageServiceSoap12(endpoint);
    }

    public com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType getUnifiedMessageServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.jecn.webservice.cms.task.UnifiedMessageServiceSoap12Stub _stub = new com.jecn.webservice.cms.task.UnifiedMessageServiceSoap12Stub(portAddress, this);
            _stub.setPortName(getUnifiedMessageServiceSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setUnifiedMessageServiceSoap12EndpointAddress(java.lang.String address) {
        UnifiedMessageServiceSoap12_address = address;
    }


    // Use to get a proxy class for UnifiedMessageServiceSoap
    private java.lang.String UnifiedMessageServiceSoap_address = JecnCMSAfterItem.getValue("TASK_SERVICE");

    public java.lang.String getUnifiedMessageServiceSoapAddress() {
        return UnifiedMessageServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String UnifiedMessageServiceSoapWSDDServiceName = "UnifiedMessageServiceSoap";

    public java.lang.String getUnifiedMessageServiceSoapWSDDServiceName() {
        return UnifiedMessageServiceSoapWSDDServiceName;
    }

    public void setUnifiedMessageServiceSoapWSDDServiceName(java.lang.String name) {
        UnifiedMessageServiceSoapWSDDServiceName = name;
    }

    public com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType getUnifiedMessageServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(UnifiedMessageServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getUnifiedMessageServiceSoap(endpoint);
    }

    public com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType getUnifiedMessageServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_BindingStub _stub = new com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_BindingStub(portAddress, this);
            _stub.setPortName(getUnifiedMessageServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setUnifiedMessageServiceSoapEndpointAddress(java.lang.String address) {
        UnifiedMessageServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.jecn.webservice.cms.task.UnifiedMessageServiceSoap12Stub _stub = new com.jecn.webservice.cms.task.UnifiedMessageServiceSoap12Stub(new java.net.URL(UnifiedMessageServiceSoap12_address), this);
                _stub.setPortName(getUnifiedMessageServiceSoap12WSDDServiceName());
                return _stub;
            }
            if (com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_BindingStub _stub = new com.jecn.webservice.cms.task.UnifiedMessageServiceSoap_BindingStub(new java.net.URL(UnifiedMessageServiceSoap_address), this);
                _stub.setPortName(getUnifiedMessageServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("UnifiedMessageServiceSoap12".equals(inputPortName)) {
            return getUnifiedMessageServiceSoap12();
        }
        else if ("UnifiedMessageServiceSoap".equals(inputPortName)) {
            return getUnifiedMessageServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "UnifiedMessageService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "UnifiedMessageServiceSoap12"));
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "UnifiedMessageServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("UnifiedMessageServiceSoap12".equals(portName)) {
            setUnifiedMessageServiceSoap12EndpointAddress(address);
        }
        else 
if ("UnifiedMessageServiceSoap".equals(portName)) {
            setUnifiedMessageServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
