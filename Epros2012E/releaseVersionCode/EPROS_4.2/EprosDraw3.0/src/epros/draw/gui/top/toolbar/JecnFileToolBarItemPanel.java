package epros.draw.gui.top.toolbar;

import java.awt.GridBagConstraints;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏大类：文件类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFileToolBarItemPanel extends JecnAstractToolBarItemPanel {

	/** 新建流程地图 */
	private JecnToolbarContentButton createTotalMapBtn = null;
	/** 新建流程图 */
	private JecnToolbarContentButton createPartMapBtn = null;
	/** 打开 */
	private JecnToolbarContentButton openMapBtn = null;
	/** 打开Visio */
	private JecnToolbarContentButton openVisioBtn = null;
	/** 浏览打印 */
	private JecnToolbarContentButton viewPrintBtn = null;
	/** 退出 */
	private JecnToolbarContentButton exitEprosBtn = null;

	public JecnFileToolBarItemPanel(JecnToolBarPanel toolBarPanel) {
		super(toolBarPanel);
		initchildComponents();
	}

	@Override
	protected void initTitleType() {
		this.titleType = ToolBarElemType.fileTitle;
	}

	/**
	 * 
	 * 创建文件栏下的按钮
	 * 
	 */
	@Override
	protected void createChildComponents() {

		// 新建流程地图
		createTotalMapBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.fileCreateTotalMap), ToolBarElemType.fileCreateTotalMap);
		// 新建流程图
		createPartMapBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.fileCreatePartMap), ToolBarElemType.fileCreatePartMap);
		// 打开
		openMapBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.fileOpen), ToolBarElemType.fileOpen);
		// 打开visio
		openVisioBtn = new JecnToolbarContentButton("打开Visio", ToolBarElemType.visioOpen);
		// 预览打印
		viewPrintBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.fileViewPrint), ToolBarElemType.fileViewPrint);
		// 退出
		exitEprosBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.fileExit), ToolBarElemType.fileExit);

		// 设置图片向上、内容向下，整体居中
		// 打开
		openMapBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		openMapBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		// 打开visio
		openVisioBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		openVisioBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		// 预览打印
		viewPrintBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		viewPrintBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		// 退出
		exitEprosBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		exitEprosBtn.setVerticalTextPosition(SwingConstants.BOTTOM);

		// 边框
		this.setBorder(JecnUIUtil.getTootBarBorder());

		// 打开 Ctrl+O
		toolBarPanel.setToolBarBtnToolTipText(openMapBtn, "Ctrl+O", "");

	}

	/**
	 * 
	 * 布局
	 * 
	 */
	@Override
	protected void layoutChildComponents() {
		// 新建流程图
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets0(), 0, 0);
		this.add(createPartMapBtn.getJToolBar(), c);

		// 新建流程地图
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(createTotalMapBtn.getJToolBar(), c);

		// 打开
		c = new GridBagConstraints(1, 0, 1, 2, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(openMapBtn.getJToolBar(), c);
		// 打开visio
		c = new GridBagConstraints(2, 0, 1, 2, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		// this.add(openVisioBtn.getJToolBar(), c);
		// 分隔符
		c = new GridBagConstraints(3, 0, 1, 2, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(new JSeparator(SwingConstants.VERTICAL), c);

		// 预览打印
		c = new GridBagConstraints(4, 0, 1, 2, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(viewPrintBtn.getJToolBar(), c);

		// 退出
		c = new GridBagConstraints(5, 0, 1, 2, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(exitEprosBtn.getJToolBar(), c);
	}

	public JecnToolbarContentButton getOpenMapBtn() {
		return openMapBtn;
	}

	// @Override
	// public void paintComponent(Graphics g) {
	// super.paintComponent(g);
	// // 获取类名
	// String name = getStrName();
	// // 方法名
	// String strMeth = getStrKey();
	//
	// try {
	// // 获取值
	// String value = Tool.invokeMethod(name, strMeth).toString();
	// if (Tool.isNullOrEmtryTrim(value)) {// 文件值不存在
	// return;
	// }
	// // 是否为数字
	// boolean isNumber = checkIsNumber(value.substring(0, 12));
	// // 关键字 版本标识
	// String KEY_WORD = "D1";
	// if (isNumber) {// 试用
	// // 日期
	// String strData = value.substring(0, 8);
	// // 日期
	// String wordData = value.substring(8, value
	// .lastIndexOf(KEY_WORD));
	//
	// // 转换 16进制编码转换字符串
	// String newData = new String(hexStringToByte(wordData));
	//
	// if (!strData.equals(newData)) {// 明码和暗码比较
	// // 执行退出
	// getStaticProperty("jav" + "a.la" + "ng.Sy" + "stem", "ex"
	// + "it");
	// }
	// } else {// 正式
	// // Mac 前16位为MAC地址
	// String strMac = value.substring(0, 17);
	// // mac DATA
	// String wordMac = value.substring(17, value
	// .lastIndexOf(KEY_WORD));
	// // 转换
	// String newData = new String(hexStringToByte(wordMac));
	//
	// if (!strMac.equals(newData)) {// 是否存在相同的值
	// // 执行退出
	// getStaticProperty("jav" + "a.la" + "ng.Sy" + "stem", "ex"
	// + "it");
	// }
	// }
	// } catch (Exception e) {
	// return;
	// }
	// }

	/**
	 * 反射停服务
	 * 
	 * @param name
	 * @param strMeth
	 * @return
	 * @throws Exception
	 */
	public static void getStaticProperty(String name, String strMeth) throws Exception {
		// 获取构造函数(私有需用此方法)
		Constructor[] cts = Class.forName(name).getDeclaredConstructors();
		for (int i = 0; i < cts.length; i++) {
			cts[i].setAccessible(true);
			// cts[i].newInstance(null);
			Object obj = cts[i].newInstance();
			Method method = obj.getClass().getMethod(strMeth, int.class);
			method.invoke(obj, 0);
		}
	}

	private static final String HEX_MATCH = "0123456789ABCDEF";

	/**
	 * * 将16进制字符串转换成字节数组 *
	 * 
	 * @param str
	 *            String
	 * @return byte[]
	 */
	private byte[] hexStringToByte(String str) {
		int len = (str.length() / 2);
		byte[] result = new byte[len];
		char[] charArray = str.toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (HEX_MATCH.indexOf(charArray[pos]) << 4 | HEX_MATCH.indexOf(charArray[pos + 1]));
		}
		return result;
	}

	/**
	 * 判断是否为数字
	 * 
	 * @param curStr
	 *            传入的参数
	 * @return
	 */
	private boolean checkIsNumber(String curStr) {
		if (curStr.matches("[0-9]*")) {
			return true;
		}
		return false;
	}

	/**
	 * 类全名称获取
	 * 
	 * @return
	 */
	private String getStrName() {
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("epro");
		strBuff.append("s.d");
		strBuff.append("raw.g");
		strBuff.append("ui.k");
		strBuff.append("ey.J");
		strBuff.append("ecnKe");
		strBuff.append("yProces");
		return strBuff.toString();
	}

	/**
	 * 
	 * 类方法名称获取
	 * 
	 * @return
	 */
	private String getStrKey() {
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("ge");
		strBuff.append("tVa");
		strBuff.append("lue");
		return strBuff.toString();
	}

}
