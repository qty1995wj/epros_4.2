package com.jecn.epros.domain.task;

/**
 * 该异常标识该异常需要终止程序的继续执行，但是不需要讲错误信息返回给客户端
 */
public class SkipException extends RuntimeException {

    public SkipException(String msg) {
        super(msg);
    }
}
