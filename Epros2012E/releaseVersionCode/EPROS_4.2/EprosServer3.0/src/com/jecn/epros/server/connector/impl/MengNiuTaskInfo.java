package com.jecn.epros.server.connector.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.action.web.login.ad.mengniu.MengNiuAfterItem;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.HttpRequestUtil;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.connector.mengniu.MengNiuCreateTasksInterface;
import com.jecn.epros.server.connector.mengniu.MengNiuFinishWorkFlowBean;
import com.jecn.epros.server.connector.mengniu.MengNiuFinishWorkFlowInterface;
import com.jecn.epros.server.connector.mengniu.MengNiuProcessResultEnum;
import com.jecn.epros.server.connector.mengniu.MengNiuProcessTaskBean;
import com.jecn.epros.server.connector.mengniu.MengNiuProcessTasksInterface;
import com.jecn.epros.server.connector.mengniu.MengNiuStartWorkFlowInterface;
import com.jecn.epros.server.connector.mengniu.MengNiuTasksBean;
import com.jecn.epros.server.connector.task.JecnBaseTaskSend;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.webservice.cetc10.Cetc10TaskBean;

/**
 * 蒙牛代办
 * 
 * @author xiaohu
 * 
 */
public class MengNiuTaskInfo extends JecnBaseTaskSend {

	private IPersonDao personDao;

	private IPersonService personService;

	/**
	 * 任务代办处理
	 * 
	 * @param handlerPeopleIds
	 *            处理人集合
	 */
	public void sendTaskInfoToMainSystem(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			Long curPeopleId) throws Exception {
		this.personDao = personDao;
		MyTaskBean myTaskBean = null;
		if (beanNew.getUpState() == 0 && beanNew.getTaskElseState() == 0) {// 拟稿人提交任务
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
			// 启动流程
			startWorkFlow(myTaskBean);
			// 创建任务
			createTasks(myTaskBean);
		} else if (beanNew.getState() == 5) {// 任务完成
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 上一阶段改为已办
			getProcessTasksInterface(myTaskBean, curPeopleId);
			// 结束流程
			getFinishWorkFlowInterface(myTaskBean);

		} else {// 一条from的数据,一条to 的数据
			// 代办转已办,是否为多审判断
			// 设置上一环节任务待办任务状态为false，表示未已审批；
			myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
			myTaskBean.setTaskStage(beanNew.getUpState());

			// 上一个阶段，处理任务，代办转已办
			getProcessTasksInterface(myTaskBean, curPeopleId);

			if ((beanNew.getTaskElseState() == 2 || beanNew.getTaskElseState() == 3)
					|| beanNew.getState() != beanNew.getUpState()) {// 任务操作状态变更，创建新的代办
				myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
				// 审批任务，发送下一环节任务待办
				createTasks(myTaskBean);
			}
		}
	}

	private String setTaskInfo(String json, int taskType) {
		try {
			String result = null;
			switch (taskType) {
			case 1:// 启动流程
				result = HttpRequestUtil.sendPost(MengNiuAfterItem.START_WORK_FLOW, json);
				break;
			case 2:// 创建任务
				result = HttpRequestUtil.sendPost(MengNiuAfterItem.CREATE_TASKS, json);
				break;
			case 3:// 执行任务
				result = HttpRequestUtil.sendPost(MengNiuAfterItem.PROCESS_TASKS, json);
				break;
			case 4:// 结束流程
				result = HttpRequestUtil.sendPost(MengNiuAfterItem.FINISH_WORK_FLOW, json);
				break;

			default:
				break;
			}
			log.info("数据接口 taskType = " + taskType + " result = " + result + "    " + json);
			if (StringUtils.isBlank(result)) {
				log.error("数据接口异常 ： taskType = " + taskType + "    " + json);
			}
			String status = JSONObject.fromObject(result).getString("Status");
			if ("false".equals(status)) {
				log.error("数据接口异常 ：" + result);
				log.error("error taskType = " + taskType + "    " + json);
			}
			return result;

		} catch (Exception e) {
			log.error("", e);
		}
		return null;
	}

	/**
	 * 发起流程
	 * 
	 * @param myTaskBean
	 * @return
	 */
	private void startWorkFlow(MyTaskBean myTaskBean) throws Exception {
		MengNiuStartWorkFlowInterface startWorkFlowInterface = new MengNiuStartWorkFlowInterface();
		// 流程实例编号 (任务ID + 关联文件ID)
		startWorkFlowInterface.setEngineInstanceID(getInstanceId(myTaskBean));
		// 当前审批人名称
		startWorkFlowInterface.setSubmitUserID(myTaskBean.getCreatePeopleLoginName());
		startWorkFlowInterface.setProcessCode(MengNiuAfterItem.PROCESS_CODE);
		startWorkFlowInterface.setInstanceTitle(myTaskBean.getTaskName());
		// 发起流程 -查阅
		startWorkFlowInterface.setInsPCViewUrl(getMailUrl(myTaskBean.getTaskId(), myTaskBean.getCreatePeopleId(),
				false, true));
		startWorkFlowInterface.setInsMobileViewUrl(getMailUrl(myTaskBean.getTaskId(), myTaskBean.getCreatePeopleId(),
				true, false));
		log.info(startWorkFlowInterface);
		JSONObject jsonObject = JSONObject.fromObject(startWorkFlowInterface);
		setTaskInfo(jsonObject.toString(), 1);
	}

	private void createTasks(MyTaskBean myTaskBean) throws Exception {
		MengNiuCreateTasksInterface createTasksInterface = new MengNiuCreateTasksInterface();
		// 流程实例编号 (任务ID + 关联文件ID)
		createTasksInterface.setEngineInstanceID(getInstanceId(myTaskBean));
		createTasksInterface.setProcessCode(MengNiuAfterItem.PROCESS_CODE);
		// 任务代办
		List<MengNiuTasksBean> Tasks = new ArrayList<MengNiuTasksBean>();
		for (Long handlerPeopleId : myTaskBean.getHandlerPeopleIds()) {
			Tasks.add(getCreateTasksBean(myTaskBean, handlerPeopleId));
		}

		createTasksInterface.setTasks(Tasks);
		JSONObject jsonObject = JSONObject.fromObject(createTasksInterface);
		setTaskInfo(jsonObject.toString(), 2);
	}

	private MengNiuTasksBean getCreateTasksBean(MyTaskBean myTaskBean, Long handlerPeopleId) {
		MengNiuTasksBean bean = new MengNiuTasksBean();
		JecnUser jecnUser = personDao.get(handlerPeopleId);
		bean.setEngineTaskSN(getInstanceId(myTaskBean));
		bean.setTaskType(1);
		// 当前处理人
		bean.setHandlerUserID(jecnUser.getLoginName());
		bean.setTaskName(myTaskBean.getStageName());
		bean.setProcessPCURL(getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, false));
		bean.setProcessMobileURL(getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, true, false));

		log.info(bean);
		return bean;
	}

	private void getFinishWorkFlowInterface(MyTaskBean myTaskBean) throws Exception {
		MengNiuFinishWorkFlowInterface finishWorkflow = new MengNiuFinishWorkFlowInterface();

		MengNiuFinishWorkFlowBean Fin_WF = new MengNiuFinishWorkFlowBean();
		Fin_WF.setEngineInstanceID(getInstanceId(myTaskBean));
		Fin_WF.setProcessCode(MengNiuAfterItem.PROCESS_CODE);
		Fin_WF.setInstanceResult(getProcessResultByTaskElseState(myTaskBean));

		finishWorkflow.setFin_WF(Fin_WF);
		JSONObject jsonObject = JSONObject.fromObject(finishWorkflow);
		setTaskInfo(jsonObject.toString(), 4);
	}

	private String getInstanceId(MyTaskBean myTaskBean) {
		return myTaskBean.getTaskId() + "_" + myTaskBean.getRid();
	}

	/**
	 * 处理任务
	 * 
	 * @param myTaskBean
	 * @return
	 */
	private void getProcessTasksInterface(MyTaskBean myTaskBean, Long handlerPeopleId) throws Exception {
		MengNiuProcessTasksInterface processTasksInterface = new MengNiuProcessTasksInterface();
		List<MengNiuProcessTaskBean> Tasks = new ArrayList<MengNiuProcessTaskBean>();
		Tasks.add(getProcessTask(myTaskBean, handlerPeopleId));
		processTasksInterface.setTasksC(Tasks);
		JSONObject jsonObject = JSONObject.fromObject(processTasksInterface);
		setTaskInfo(jsonObject.toString(), 3);
	}

	private void getProcessTasksInterface(MyTaskBean myTaskBean, List<Long> handlerPeopleIds) throws Exception {
		MengNiuProcessTasksInterface processTasksInterface = new MengNiuProcessTasksInterface();
		List<MengNiuProcessTaskBean> Tasks = new ArrayList<MengNiuProcessTaskBean>();
		for (Long handlerPeopleId : handlerPeopleIds) {
			Tasks.add(getProcessTask(myTaskBean, handlerPeopleId));
		}
		processTasksInterface.setTasksC(Tasks);
		JSONObject jsonObject = JSONObject.fromObject(processTasksInterface);
		setTaskInfo(jsonObject.toString(), 3);
	}

	private MengNiuProcessTaskBean getProcessTask(MyTaskBean myTaskBean, Long handlerPeopleId) {
		MengNiuProcessTaskBean bean = new MengNiuProcessTaskBean();
		JecnUser jecnUser = null;
		if (personDao == null) {
			jecnUser = personService.get(handlerPeopleId);
		} else {
			jecnUser = personDao.get(handlerPeopleId);
		}
		bean.setEngineInstanceID(getInstanceId(myTaskBean));
		// 任务所有者，发起人
		bean.setHandlerUserID(jecnUser.getLoginName());
		bean.setProcessResult(getProcessResultByTaskElseState(myTaskBean));
		bean.setProcessUserID(jecnUser.getLoginName());

		bean.setTaskPCViewURL(getPCViewURL(myTaskBean, handlerPeopleId, true));
		bean.setTaskMobileViewURL(getMobileViewURL(myTaskBean, handlerPeopleId, true));
		return bean;
	}

	/***
	 * 子操作 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
	 * 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)） 10：交办人提交 11:编辑提交
	 * 12:编辑打回 13:撤回 14:反回
	 * 
	 * @param myTaskBean
	 * @return
	 */
	private String getProcessResultByTaskElseState(MyTaskBean myTaskBean) {
		String result = null;
		switch (myTaskBean.getOperationType()) {
		case 2:// 交办
			result = MengNiuProcessResultEnum.countersign.toString();
			break;
		case 3:// 转批
			result = MengNiuProcessResultEnum.redirect.toString();
			break;
		case 4:// 打回
			result = MengNiuProcessResultEnum.return_.toString();
			break;
		case 13:// 撤回
			result = MengNiuProcessResultEnum.cancel.toString();
			break;
		default:// 通过
			result = MengNiuProcessResultEnum.agree.toString();
			break;
		}
		return result;
	}

	public MyTaskBean getMyTaskBeanByTaskInfo(JecnTaskBeanNew beanNew) {
		JecnUser createUser = null;
		if (personDao == null) {
			createUser = personService.get(beanNew.getCreatePersonId());
		} else {
			createUser = personDao.get(beanNew.getCreatePersonId());
		}

		MyTaskBean myTaskBean = new MyTaskBean();
		myTaskBean.setUpdateTime(JecnCommon.getStringbyDateHMS(beanNew.getUpdateTime()));
		myTaskBean.setTaskId(beanNew.getId());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setTaskStage(beanNew.getState());
		myTaskBean.setTaskName(getTaskName(beanNew.getTaskName()) + "审批");
		// 当前审批人操作状态
		myTaskBean.setTaskElseState(beanNew.getTaskElseState());
		myTaskBean.setTaskDesc(beanNew.getTaskDesc());
		myTaskBean.setRid(beanNew.getRid().toString());
		myTaskBean.setCreatePeopleLoginName(createUser.getLoginName());
		myTaskBean.setOperationType(beanNew.getTaskElseState());
		myTaskBean.setStageName(beanNew.getStateMark());
		myTaskBean.setCreatePeopleId(beanNew.getCreatePersonId());
		return myTaskBean;
	}

	private String getTaskName(String name) {
		return name.indexOf(".") != -1 ? name.substring(0, name.lastIndexOf(".")) : name;
	}

	private String getPCViewURL(MyTaskBean myTaskBean, Long handlerPeopleId, boolean isView) {
		return getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, false, isView);
	}

	private String getMobileViewURL(MyTaskBean myTaskBean, Long handlerPeopleId, boolean isView) {
		return getMailUrl(myTaskBean.getTaskId(), handlerPeopleId, true, isView);
	}

	/**
	 * 取消任务代办
	 */
	@Override
	public void sendInfoCancel(Long curPeopleId, JecnTaskBeanNew jecnTaskBeanNew, JecnUser jecnUser, JecnUser createUser) {
		Cetc10TaskBean cetc10TaskBean = new Cetc10TaskBean();
		cetc10TaskBean.setIsProcessed("true");
		cetc10TaskBean.setLoginName(jecnUser.getEmail());
		cetc10TaskBean.setName(jecnUser.getTrueName());
		cetc10TaskBean.setTitle(jecnTaskBeanNew.getTaskName() + "(" + createUser.getTrueName() + ")");
		cetc10TaskBean.setSummery(jecnTaskBeanNew.getTaskDesc());
		cetc10TaskBean.setTaskid(jecnTaskBeanNew.getId() + "_" + curPeopleId + "_" + jecnTaskBeanNew.getState());
		cetc10TaskBean.setUrl(getMailUrl(jecnTaskBeanNew.getId(), jecnUser.getPeopleId(), false, false));
		cetc10TaskBean.setTime(JecnCommon.getStringbyDate(new Date()));
		log.info(cetc10TaskBean);
	}

	@Override
	public void sendInfoCancels(JecnTaskBeanNew beanNew, IPersonDao personDao, Set<Long> handlerPeopleIds,
			List<Long> cancelPeoples, Long curPeopleId) throws Exception {
		this.personDao = personDao;
		if (personDao == null) {
			personService = (IPersonService) ApplicationContextUtil.getContext().getBean("PersonServiceImpl");
		}
		MyTaskBean myTaskBean = getMyTaskBeanByTaskInfo(beanNew);
		myTaskBean.setTaskStage(beanNew.getUpState());

		// 上一个阶段，处理任务，代办转已办
		getProcessTasksInterface(myTaskBean, cancelPeoples);

		if (handlerPeopleIds == null || handlerPeopleIds.size() == 0) {
			return;
		}
		myTaskBean.setHandlerPeopleIds(handlerPeopleIds);
		// 审批任务，发送下一环节任务待办
		createTasks(myTaskBean);
	}

}
