package com.jecn.epros.server.bean.integration;

import java.io.Serializable;
import java.util.Date;

/**
 * 内控指引知识库Bean
 *  
 */
public class JecnControlGuide implements Serializable {

	private static final long serialVersionUID = -4612223094383706556L;
	
	/**主键ID*/
	private Long id;
	/**父ID*/
	private Long parentId;
	/**类型 0:目录1:条款*/
	private Integer type;
	/**名称*/
	private String name;
	/**详细描述*/
	private String description;
	/**序号*/
	private Integer sort;
	/**创建人*/
	private Long createPerson;
	/**创建日期*/
	private Date createTime;
	/**更新人*/
	private Long updatePerson;
	/**更新日期*/
	private Date updateTime;
	/**备注*/
	private String note;
	/** 项目ID */
	private Long projectId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public Long getCreatePerson() {
		return createPerson;
	}
	public void setCreatePerson(Long createPerson) {
		this.createPerson = createPerson;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getUpdatePerson() {
		return updatePerson;
	}
	public void setUpdatePerson(Long updatePerson) {
		this.updatePerson = updatePerson;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

}
