package com.jecn.viewdoc.convert.view;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.viewdoc.convert.ViewDocsManager;
import com.jecn.viewdoc.convert.ViewDocsPathManager;

/**
 * 以前的流程预览，本人未改逻辑
 * 
 * @author hyl
 * 
 */
public class ProcessCommonView extends ProcessAbsView {

	public ProcessCommonView(IWebProcessService webProcessService, Long relatedId, TreeNodeType nodeType,
			boolean isPub, Long userId, Long historyId, HttpServletRequest request, HttpServletResponse response) {
		super(webProcessService, relatedId, nodeType, isPub, userId, historyId, request, response);
	}

	@Override
	public String getPath() {
		String viewDocPath = null;
		// 先拿到文件字节流
		ByteFile result = null;
		try {
			result = new ByteFile();
			result.setFileName(getFileName());
			result.setBytes(createDoc.getBytes());
		} catch (Exception e) {
			log.error("获取流程操作说明字节流错误！", e);
		}
		// 变成PDF预览
		try {
			// TODO 文件名称暂时以文件id替代
			String tempName = String.valueOf(relatedId) + ".doc";
			String pdfPath = ViewDocsManager.buildProcessViewDoc(result.getBytes(), tempName, "PROCESS");
			String params = assembleFileNameAndDownloadURL(pdfPath);
			viewDocPath = ViewDocsPathManager.VIEWER_JS + params;
		} catch (Exception e) {
			log.error("", e);
		}
		return viewDocPath;
	}

}
