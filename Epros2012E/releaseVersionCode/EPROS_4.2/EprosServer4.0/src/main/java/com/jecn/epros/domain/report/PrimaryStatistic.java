package com.jecn.epros.domain.report;

import com.jecn.epros.domain.common.BaseBean;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.DictionaryEnum;

import java.io.Serializable;
import java.util.Date;

/**
 * 合规
 */
public class PrimaryStatistic implements Serializable {
    private Long flowId;
    private String flowName;
    private Long orgId;
    private String orgName;
    private int num;
    /**
     * 业务类型
     */
    private String bussType;
    /**
     * 所依法规
     */
    private String allowRule;

    // 业务类型
    public String getBussTypeStr() {
        return ConfigUtils.getDictionaryValue(bussType, DictionaryEnum.BUSS_TYPE);
    }

    // ----------------

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getBussType() {
        return bussType;
    }

    public void setBussType(String bussType) {
        this.bussType = bussType;
    }

    public String getAllowRule() {
        return allowRule;
    }

    public void setAllowRule(String allowRule) {
        this.allowRule = allowRule;
    }
}
