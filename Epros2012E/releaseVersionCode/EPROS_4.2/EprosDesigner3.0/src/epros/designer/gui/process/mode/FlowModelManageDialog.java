package epros.designer.gui.process.mode;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import epros.draw.gui.top.dialog.JecnDialog;

/**
 * 流程模板管理
 * 包括：流程模板，流程文件模板
 * @author 2012-06-26
 *
 */
public class FlowModelManageDialog extends JecnDialog {
	private JPanel mainPanel=new JPanel();
	private JScrollPane s=new JScrollPane();
	private FlowModelHighEfficiencyTree  flowTree=new FlowModelHighEfficiencyTree();
	
	private JPanel p=new JPanel();
	public FlowModelManageDialog(){
		this.setSize(300, 800);
		this.setLocationRelativeTo(null);
		Dimension d=new Dimension(200,700);
		s.setPreferredSize(d);
		s.setMinimumSize(d);
		s.setViewportView(flowTree);
		mainPanel.add(s);
		this.getContentPane().add(mainPanel);
	}
}
