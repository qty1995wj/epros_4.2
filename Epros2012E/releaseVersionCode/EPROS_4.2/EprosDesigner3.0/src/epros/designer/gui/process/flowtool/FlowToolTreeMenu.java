package epros.designer.gui.process.flowtool;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.designer.process.IFlowToolAction;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.flowtool.NodeMove.FlowToolMoveNodeDialog;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class FlowToolTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(FlowToolTreeMenu.class);
	private FlowToolManageDialog flowToolManageDialog = null;
	/** 新建支持工具 */
	private JMenuItem addFlowToolMenu;

	/** 更新支持工具 */
	private JMenuItem updateFlowToolMenu;

	/** 删除 */
	private JMenuItem delMenu;

	/** 刷新 */
	private JMenuItem refurbishMenu;

	/** 节点移动 */
	private JMenuItem dragNode;

	/** 节点排序 */
	private JMenuItem nodeSort;

	private JecnTree jTree = null;

	/** 单选节点 */
	private JecnTreeNode selectNode = null;

	/** 选中节点集合 */
	private List<JecnTreeNode> listNode = null;

	public FlowToolTreeMenu(JecnTree jTree, FlowToolManageDialog flowToolManageDialog) {
		initCompotents(jTree, flowToolManageDialog);
	}

	private void initCompotents(JecnTree jTree, FlowToolManageDialog flowToolManageDialog) {
		this.jTree = jTree;
		this.flowToolManageDialog = flowToolManageDialog;
		// 新建支持工具
		addFlowToolMenu = new JMenuItem(JecnProperties.getValue("add"),
				new ImageIcon("images/menuImage/newRuleDir.gif"));
		// 更新支持工具
		updateFlowToolMenu = new JMenuItem(JecnProperties.getValue("edit"));
		// 删除
		delMenu = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon("images/menuImage/delete.gif"));
		// 刷新
		refurbishMenu = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon("images/menuImage/refresh.gif"));
		// 节点排序
		nodeSort = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon("images/menuImage/nodeSort.gif"));// 排序
		// 节点移动
		dragNode = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon("images/menuImage/nodeMove.gif"));
		/**
		 * 新建支持工具监听
		 */
		addFlowToolMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addToolActionPerformed();
			}
		});
		/**
		 * 更新支持工具监听
		 */
		updateFlowToolMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateToolActionPerformed();
			}
		});
		/**
		 * 删除监听
		 */
		delMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delActionPerformed();
			}
		});

		/**
		 * 刷新监听
		 */
		refurbishMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refActionPerformed();
			}
		});

		/**
		 * 节点排序监听
		 */
		nodeSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nodeSortActionPerformed();
			}
		});
		/**
		 * 节点移动监听
		 */
		dragNode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dragNodeActionPerformed();
			}
		});

		this.add(addFlowToolMenu);
		this.add(updateFlowToolMenu);
		// 添加分隔符
		this.addSeparator();
		this.add(delMenu);
		this.add(refurbishMenu);
		this.add(nodeSort);
		this.add(dragNode);

	}

	/**
	 * 添加
	 */
	public void addToolActionPerformed() {
		if (selectNode == null) {
			return;
		}
		AddFlowToolDialog addFlowToolDialog = new AddFlowToolDialog(selectNode, jTree);
		addFlowToolDialog.setVisible(true);
	}

	/**
	 * 更新
	 */
	public void updateToolActionPerformed() {
		UpdateFlowToolDialog updateFlowToolDialog = new UpdateFlowToolDialog(selectNode.getJecnTreeBean(), jTree);
		updateFlowToolDialog.setVisible(true);
		if (updateFlowToolDialog.isOk()) {
			JecnTreeCommon.reNameNode(jTree, selectNode, updateFlowToolDialog.getName());
			JecnTreeCommon.selectNode(jTree, selectNode);
		}
	}

	/**
	 * 删除
	 */
	@SuppressWarnings("unchecked")
	public void delActionPerformed() {
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		// if (JecnConstants.isMysql) {
		// listIds = JecnTreeCommon.getAllChildIds(listNode);
		// } else {
		for (JecnTreeNode node : listNode) {
			listIds.add(node.getJecnTreeBean().getId());
		}
		// }
		if (listIds.size() == 0) {
			return;
		}
		try {
			ConnectionPool.getFlowTool().deleteSustainTools(listIds, JecnUtil.getUserId());
		} catch (Exception e) {
			log.error("FlowToolTreeMenu delActionPerformed is error", e);
		}
		/** 删除节点 */
		JecnTreeCommon.removeNodes(jTree, listNode);
		/**
		 * 删除搜索表格中的数据
		 */
		// 要删除的数据名称
		Vector<Vector<Object>> vs = ((DefaultTableModel) flowToolManageDialog.getJecnTable().getModel())
				.getDataVector();
		for (int i = vs.size() - 1; i >= 0; i--) {
			Vector<Object> vob = vs.get(i);
			for (Long longID : listIds) {
				if (vob.get(1).toString().equals(longID.toString())) {
					((DefaultTableModel) flowToolManageDialog.getJecnTable().getModel()).removeRow(i);
				}
			}
		}
		// flowToolManageDialog.getJecnTable().setRowSelectionInterval(i, i);
		// flowToolManageDialog.getJecnTable().removeSelectRows();
	}

	/**
	 * 刷新
	 */
	public void refActionPerformed() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			IFlowToolAction flowToolAction = ConnectionPool.getFlowTool();
			List<JecnTreeBean> list = flowToolAction.getChildSustainTools(id);
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error("FlowToolTreeMenu refActionPerformed is error", ex);
		}
	}

	/**
	 * 节点排序
	 */
	public void nodeSortActionPerformed() {
		if (selectNode == null) {
			return;
		}
		FlowToolSortDialog flowToolSortDialog = new FlowToolSortDialog(selectNode, jTree);
		flowToolSortDialog.setVisible(true);
	}

	/**
	 * 节点移动
	 */
	public void dragNodeActionPerformed() {
		// FlowToolNodeMoveDialog flowToolNodeMoveDialog = new
		// FlowToolNodeMoveDialog(listNode, jTree);
		// flowToolNodeMoveDialog.setFocusableWindowState(true);
		// flowToolNodeMoveDialog.setVisible(true);
		if (!JecnDesignerCommon.validateRepeatNodesName(listNode)) {
			return;
		}
		FlowToolMoveNodeDialog flowToolMoveNodeDialog = new FlowToolMoveNodeDialog(listNode, jTree);
		flowToolMoveNodeDialog.setFocusableWindowState(true);
		flowToolMoveNodeDialog.setVisible(true);
	}

	public List<JecnTreeNode> getListNode() {
		return listNode;
	}

	/**
	 * 初始化
	 */
	private void init() {
		addFlowToolMenu.setEnabled(false);
		updateFlowToolMenu.setEnabled(false);
		delMenu.setEnabled(false);
		refurbishMenu.setEnabled(false);
		nodeSort.setEnabled(false);
		dragNode.setEnabled(false);
	}

	/**
	 * 单选
	 */
	private void singleSelect() {
		JecnTreeCommon.autoExpandNode(jTree, selectNode);
		init();
		switch (selectNode.getJecnTreeBean().getTreeNodeType()) {
		// 根节点
		case toolRoot:
			// 新建支持工具
			addFlowToolMenu.setEnabled(true);
			// 排序
			nodeSort.setEnabled(true);
			// 刷新
			refurbishMenu.setEnabled(true);
			break;
		// 支持工具节点
		case tool:
			// 新建支持工具
			addFlowToolMenu.setEnabled(true);
			// 更新支持工具
			updateFlowToolMenu.setEnabled(true);
			// 删除
			delMenu.setEnabled(true);
			// 排序
			nodeSort.setEnabled(true);
			// 移动
			dragNode.setEnabled(true);
			// 刷新
			refurbishMenu.setEnabled(true);
			break;
		default:
			break;
		}

	}

	/**
	 * 多选设置
	 */
	private void mutilSelect() {
		init();
		// 删除
		delMenu.setEnabled(true);
		// 移动
		dragNode.setEnabled(true);
	}

	public void setListNode(List<JecnTreeNode> listNode) {
		this.listNode = listNode;
		if (listNode.size() == 1) {
			this.selectNode = listNode.get(0);
			this.singleSelect();
		} else {
			this.mutilSelect();
		}
	}

}
