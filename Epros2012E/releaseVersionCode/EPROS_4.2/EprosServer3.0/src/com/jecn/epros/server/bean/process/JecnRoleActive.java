package com.jecn.epros.server.bean.process;

/**
 * 活动和角色的关系（浏览端）
 * 
 * @author Administrator
 * 
 */
public class JecnRoleActive implements java.io.Serializable {
	// Fields
	private Long autoIncreaseId;// 主键ID
	private Long figureRoleId;// 对应角色JecnFlowStructureImageT的figureId
	private Long figureActiveId;// 对应活动JecnFlowStructureImageT的figureId

	public Long getAutoIncreaseId() {
		return autoIncreaseId;
	}

	public void setAutoIncreaseId(Long autoIncreaseId) {
		this.autoIncreaseId = autoIncreaseId;
	}

	public Long getFigureRoleId() {
		return figureRoleId;
	}

	public void setFigureRoleId(Long figureRoleId) {
		this.figureRoleId = figureRoleId;
	}

	public Long getFigureActiveId() {
		return figureActiveId;
	}

	public void setFigureActiveId(Long figureActiveId) {
		this.figureActiveId = figureActiveId;
	}
}
