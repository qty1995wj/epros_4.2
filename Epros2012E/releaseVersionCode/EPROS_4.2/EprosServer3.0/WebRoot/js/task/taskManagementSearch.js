//Ext.onReady(function(){
function taskManage() {

	taskManageStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize
			}
		},
		proxy : new Ext.data.HttpProxy( {
			url : "taskManageSearch.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "taskId",
			mapping : "taskId"
		}, {
			name : "taskName",
			mapping : "taskName"
		}, {
			name : "taskType",
			mapping : "taskType"
		}, {
			name : "createName",
			mapping : "createName"
		}, {
			name : "resPeopleName",
			mapping : "resPeopleName"
		}, {
			name : "taskStage",
			mapping : "taskStage"
		}, {
			name : "stageName",
			mapping : "stageName"
		}, {   
			name : "startTime",
			mapping : "startTime"
		}, {
			name : "endTime",
			mapping : "endTime"
		}, {
			name : "taskElseState",
			mapping : "taskElseState"
		}, {
			name : "publishTime",
			mapping : "publishTime"
		} ])

	});

	taskManageColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : "taskId",
				dataIndex : "taskId",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_taskName,
				dataIndex : "taskName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_taskType,
				dataIndex : "taskType",
				align : "center",
				menuDisabled : true,
				renderer : taskTypeRenderer
			}, {
				header : i_creatName,
				dataIndex : "createName",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : "责任人",
				dataIndex : "resPeopleName",
				align : "center",
				menuDisabled : true,
				sortable : true
			},{
				header : "taskStage",
				dataIndex : "taskStage",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_stage,
				dataIndex : "stageName",
				align : "center",
				menuDisabled : true
			}, {
				header : "taskElseState",
				dataIndex : "taskElseState",
				align : "center",
				menuDisabled : true,
				hidden : true
			}, {
				header : i_startTime,
				dataIndex : "startTime",
				align : "center",
				menuDisabled : true,
				sortable : true
			    
			}, { 
				header : i_endTime,
				dataIndex : "endTime",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, { 
				header : "实际结束时间",
				dataIndex : "publishTime",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_operation,
				align : "center",
				menuDisabled : true,
				renderer : taskManageRenderer
			} ]);
	taskManageBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : taskManageStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	taskManageGridPanel = new Ext.grid.GridPanel( {
		id : "taskManageGridPanel_id",
		renderTo : 'taskManageGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 166,

		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样。
		},
		frame : false,
		store : taskManageStore,
		colModel : taskManageColumnModel,
		bbar : taskManageBbar
	})

}

/**
 * 任务列表操作
 * @param {} value
 * @param {} cellmeta
 * @param {} record
 */
function taskManageRenderer(value, cellmeta, record) {
	//任务ID
	var taskId = record.data["taskId"];
	//任务阶段
	var taskStage = record.data["taskStage"];
	//操作 2为系统管理员可编辑和删除任务 
	var taskElseState = record.data["taskElseState"];
	//任务类型
	var taskType = record.data["taskType"];

	// 审批操作连接
	var varApp;
	// 取消操作连接
	var varCancel;
	// 拟稿人操作
	var varState;
	// div
	var varDiv;

	if (taskId == null || taskStage == null) {//任务主键ID或任务阶段不存在
		return;
	}
	//默认URL链接为任务ID和任务类型
	var appUrl = "taskId=" + taskId + "&taskType=" + taskType;
	//查阅
	var varView = "<a target='_blank' href='taskLookUp.action?" + appUrl
			+ "' class='a-operation'>" + i_lookUp + "</a>";

	if (taskElseState != null) {//拟稿人阶段
		if (taskElseState == 2) {//管理员执行编辑或删除
			//删除操作
			varCancel = "<a onclick='deleteTask()' class='a-operation'>"
					+ i_Delete + "</a>";
			if (taskStage == 5) {//任务审批完成
				//管理员编辑任务
				varState = i_Edit;
			} else {
				//管理员编辑任务
				varState = "<a target='_blank' href='taskApprove.action?"
						+ appUrl + "&taskOperation=" + 3
						+ "' class='a-operation'>" + i_Edit + "</a>";
			}

			//任务管理显示的按钮
			varDiv = "<div >" + varState + " | " + varView + " | " + varCancel
					+ "</a><a></a></div>";
		} else {
			//任务管理显示的按钮
			varDiv = "<div >" + varView + "</a><a></a></div>";
		}
	}
	return varDiv;
}

function taskCheckbox() {
	if (!document.getElementById("checkbox").checked) {
		document.getElementById("startTime").value = "";
		document.getElementById("endTime").value = "";
	}
}
/**
 * 删除
 * 
 * @param {}
 *            record
 */
function deleteTask() {
	// "是否删除！"
	Ext.MessageBox.confirm(i_tip, i_HAS_DELETE_TASK, function(btn) {
		if (btn == 'yes') {// 点击的是‘是’
				Ext.Ajax.request( {
					url : basePath + "taskManagementDelete.action",
					params : {
						taskId : taskManageGridPanel.getSelectionModel()
								.getSelected().data["taskId"]
					},
					success : function() {
						taskManageStore.remove(taskManageGridPanel
								.getSelectionModel().getSelected())
					}
				})
			}

		});
}
/**
 *任务管理重置
 */
function taskSearchReset() {
	//获取任务类型
	var taskType = document.getElementById("searchTaskType").value;
	if (taskType == 0) {//流程任务
		document.getElementById("flowState").style.display = "none";
	} else if (taskType == 1) {//文件任务
		document.getElementById("fileState").style.display = "none";
	} else if (taskType == 2) {//制度任务
		document.getElementById("ruleState").style.display = "none";
	} else if (taskType == 4) {//流程地图任务
		document.getElementById("flowMapState").style.display = "none";
	}
	// 任务类型默认显示全部
	document.getElementById("searchTaskType").options[0].selected = true;
	// 任务阶段默认显示全部
	document.getElementById("taskType").options[0].selected = true;
	//显示默认的阶段
	document.getElementById("taskType").style.display = "";

	document.getElementById("taskName").value = "";
	document.getElementById("createUserName_task").value = "";
	document.getElementById("createUserId_task").value = "";
	document.getElementById("startTime").value = "";
	document.getElementById("endTime").value = "";
	if (document.getElementById("delayTask") != null) {
		document.getElementById("delayTask").checked = false;
	}
}
/**
 *任务管理搜索按钮
 * type的值为0搜索 1下载
 */
function taskManagementSearch(type) {
	
	//开始时间
	var startTime = document.getElementById("startTime").value;
	//结束时间
	var endTime = document.getElementById("endTime").value;
	if (startTime > endTime) {// 结束时间大于开始时间
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip,i_startTimeIsLessEndTime);
		return;
	}
	
	//任务名称
	var taskName = document.getElementById("taskName").value;
	//任务创建人主键ID
	var createUserId = document.getElementById("createUserId_task").value;
	//任务类型
	var taskType = document.getElementById("searchTaskType").value;
	var taskStage;
	if (taskType == -1) {//查询所有任务
		//审批阶段
		taskStage = -1;
	} else if (taskType == 0) {//流程任务
		taskStage = document.getElementById("flowState").value;
	} else if (taskType == 1) {//文件任务
		taskStage = document.getElementById("fileState").value;
	} else if (taskType == 2) {//制度任务
		taskStage = document.getElementById("ruleState").value;
	} else if (taskType == 4) {//流程地图任务
		taskStage = document.getElementById("flowMapState").value;
	}
	if (createUserId == '') {
		createUserId = 0;
	}
	//是否勾选 "搁置任务"
	var delayTask=false;
	if (document.getElementById("delayTask") != null
			&& document.getElementById("delayTask").checked) {
		delayTask = true;
	}

	if (type == 0) {
        taskManageStore.setBaseParam("delayTask", delayTask);
		taskManageStore.setBaseParam("taskName", taskName);
		taskManageStore.setBaseParam("createUserId", createUserId);
		if (taskType && taskType != "") {
			taskManageStore.setBaseParam("taskType", taskType);
		}
		if (taskStage && taskStage != "") {
			taskManageStore.setBaseParam("taskStage", taskStage);
		}
		taskManageStore.setBaseParam("startTime", startTime);
		taskManageStore.setBaseParam("endTime", endTime);
		taskManageStore.load({
					params : {
						start : 0,
						limit : pageSize
					}
				});

	} else if(type==1){ //下载
		
		// 给隐藏域中的内容赋值
		$("#delayTaskHidden").val(delayTask);
		$("#taskNameHidden").val(taskName);
		$("#createUserIdHidden").val(createUserId);
		if (taskType && taskType != "") {
			$("#taskTypeHidden").val(taskType);
		}
		if (taskStage && taskStage != "") {
			$("#taskStageHidden").val(taskStage);
		}
		$("#startTimeHidden").val(startTime);
		$("#endTimeHidden").val(endTime);
		$("#downloadTaskSearch").submit();
	
	}

}
