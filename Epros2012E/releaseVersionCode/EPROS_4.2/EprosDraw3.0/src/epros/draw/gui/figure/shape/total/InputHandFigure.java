/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape.total;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnPaintEnum;

/**
 * "手动输入
 * 
 * @author zhangxh
 * @date： 日期：Mar 22, 2012 时间：2:12:52 PM
 */
public class InputHandFigure extends JecnBaseFigurePanel {
	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int y1;
	private int y2;
	private int y3;
	private int y4;

	public InputHandFigure(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	@Override
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		double scale = this.getFlowElementData().getCurrentAngle();
		if (scale == 0.0) {
			x1 = 0;
			y1 = userHeight / 3;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
		} else if (scale == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth * 2 / 3;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
		} else if (scale == 180.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight * 2 / 3;
			x4 = 0;
			y4 = userHeight;
		} else if (scale == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = userWidth / 3;
			y4 = userHeight;
		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paintNoneFigure(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				0, 0, y1, y2, y3, y4, 0, 0, userWidth, userHeight);
	}

	// /**
	// *
	// * 阴影效果
	// */
	// public void paintShadowsOnly(Graphics2D g2d) {
	// // 阴影
	// paintShadows(g2d);
	// }

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintInputHandTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintInputHandTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	private void paintInputHandTop(Graphics2D g2d, int shadowCount) {
		// 顶层 原图，榜值均为零
		JecnPaintEnum.INTANCE.paintHandTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				0, 0, y1, y2, y3, y4, 0, 0, shadowCount, shadowCount, userWidth, userHeight);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DHandLow(g2d, 0);
	}

	/**
	 * 既有3D又有阴影效果
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DHandLow(g2d, indent3Dvalue);
	}

	/**
	 * 3D效果顶层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影时底层缩进值
	 */
	private void paint3DHandLow(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DHandLow(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				0, 0, y1, y2, y3, y4, 0, 0, indent3Dvalue, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DInputHandLine(g2d, 0);
	}

	/**
	 * 3D效果边框颜色设置
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 *            存在阴影效果边框 缩进值
	 */
	private void paint3DInputHandLine(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DInputHandLine(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3,
				x4, 0, 0, y1, y2, y3, y4, 0, 0,indent3Dvalue, indent3Dvalue,userWidth,userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DInputHandLine(g2d, 5);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	public void paint3DPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				0, 0, y1, y2, y3, y4, 0, 0, 0,userWidth,userHeight);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	public void paint3DAndShadowsPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				0, 0, y1, y2, y3, y4, 0, 0, indent3Dvalue,userWidth,userHeight);
	}
}