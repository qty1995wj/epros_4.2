package com.jecn.epros.server.service.rule.history;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;

public class JecnCommonRuleHistory {

	/**
	 * JECN_RULE_HISTORY 制度主表
	 * 
	 * @param ruleId
	 * @param historyId
	 * @return
	 */
	private static String getJecnRuleHistorySql(Long ruleId, Long historyId) {
		return "INSERT INTO JECN_RULE_HISTORY" + "  (ID,PROJECT_ID," + "   PER_ID," + "   RULE_NUMBER,"
				+ "   RULE_NAME," + "   IS_PUBLIC," + "   TYPE_ID," + "   IS_DIR," + "   ORG_ID," + " SORT_ID,"
				+ "   FILE_ID," + "   TEMPLATE_ID," + "   TEST_RUN_NUMBER," + "   CREATE_DATE,"
				+ "   CREATE_PEOPLE_ID," + "   UPDATE_DATE," + "   UPDATE_PEOPLE_ID," + "   HISTORY_ID,"
				+ "   RES_PEOPLE_ID," + "   TYPE_RES_PEOPLE," + "   IS_FILE_LOCAL," + "   T_PATH," + "   T_LEVEL,"
				+ "   PUB_TIME," + "   VIEW_SORT," + "   CONFIDENTIALITY_LEVEL, KEYWORD,COMMISSIONER_ID, " + "   EXPIRY," + "   WARD_PEOPLE_ID,DEL_STATE,GUID,RULE_URL,EFFECTIVE_DATE)"
				+"SELECT T.ID,T.PROJECT_ID,T.PER_ID, T.RULE_NUMBER,                                                  "+
			    " T.RULE_NAME,  T.IS_PUBLIC,  T.TYPE_ID,  T.IS_DIR,                                                   "+
			    "          T.ORG_ID,  T.SORT_ID,  CASE                                                                 "+
			    "          WHEN T.IS_DIR = 2 AND T.IS_FILE_LOCAL = 0 THEN  JF.VERSION_ID                               "+
			    "       ELSE    T.FILE_ID    END  FILE_ID,   T.TEMPLATE_ID,                              "+
			    "       T.TEST_RUN_NUMBER,   T.CREATE_DATE,        T.CREATE_PEOPLE_ID,                               "+
			    "           T.UPDATE_DATE, T.UPDATE_PEOPLE_ID, "+historyId+" HISTORY_ID,  T.RES_PEOPLE_ID,   T.TYPE_RES_PEOPLE,    "+
			    "      T.IS_FILE_LOCAL,    T.T_PATH,  T.T_LEVEL, T.PUB_TIME,                                           "+
			    "        T.VIEW_SORT, T.CONFIDENTIALITY_LEVEL, T.KEYWORD,T.COMMISSIONER_ID, T.EXPIRY, T.WARD_PEOPLE_ID ,T.DEL_STATE,"+JecnCommonSql.getSqlGUID() +" GUID ,RULE_URL,EFFECTIVE_DATE  "+                            
			    "         FROM JECN_RULE_T  T                                                                          "+
			    " LEFT JOIN                                                                                 "+
			    "       JECN_FILE_T JF                                                                               "+
			    "           ON T.IS_DIR = 2                                                                    		   "+
			    "          AND T.IS_FILE_LOCAL = 0                                                                     "+
			    "         AND JF.FILE_ID = T.FILE_ID  WHERE ID ="+ruleId;                                                   
			    
	}
	
	/**
	 * JECN_AUTHORITY_HISTORY 权限表 岗位组查阅权限
	 * 
	 * @return ACCESS_TYPE : 2 岗位组
	 */
	private static String getJecnAuthorityGroupHistory(Long relateId, Long historyId) {
		return "insert into JECN_AUTHORITY_HISTORY" + " (ID,RELATE_ID, TYPE,ACCESS_ID,ACCESS_TYPE,HISTORY_ID)"
				+ " select "+JecnCommonSql.getSqlGUID()+", RELATE_ID,TYPE,POSTGROUP_ID, 2  ACCESS_TYPE," + historyId + " HISTORY_ID "
				+ " from JECN_GROUP_PERMISSIONS_T where type = 3 and RELATE_ID=" + relateId;
	}

	/**
	 * JECN_AUTHORITY_HISTORY 权限表 部门查阅权限
	 * 
	 * @return ACCESS_TYPE : 0 部门
	 */
	private static String getJecnAuthorityOrgHistory(Long relateId, Long historyId) {
		return "insert into JECN_AUTHORITY_HISTORY" + " (ID,RELATE_ID, TYPE,ACCESS_ID,ACCESS_TYPE,HISTORY_ID)"
				+ " select "+JecnCommonSql.getSqlGUID()+",  RELATE_ID, TYPE,ORG_ID, 0  ACCESS_TYPE ," + historyId + " HISTORY_ID"
				+ "  from JECN_ORG_ACCESS_PERM_T where type=3 and RELATE_ID=" + relateId;

	}

	/**
	 * JECN_AUTHORITY_HISTORY 权限表 岗位查阅权限
	 * @return ACCESS_TYPE : 1 岗位
	 */
	private static String getJecnAuthorityAccessHistory(Long relateId, Long historyId) {
		return "insert into JECN_AUTHORITY_HISTORY" + " (ID,RELATE_ID, TYPE,ACCESS_ID,ACCESS_TYPE,HISTORY_ID)"
				+ "  select "+JecnCommonSql.getSqlGUID()+",RELATE_ID, TYPE,FIGURE_ID, 1  ACCESS_TYPE, " + historyId + " HISTORY_ID"
				+ "   from JECN_ACCESS_PERMISSIONS_T where type=3 and RELATE_ID=" + relateId;
	}
 
	/**
	 * 相关风险 JECN_RELATE_RISK_HISTORY
	 * 
	 * @param relateId
	 * @param historyId
	 * @return sql: RELATE_TYPE ： 0 流程 1制度 2 文件
	 */
	private static String getJecnRuleRiskHistory(Long relateId, Long historyId) {
		return "INSERT INTO JECN_RELATE_RISK_HISTORY" + 
				"(ID,RELATE_ID, RELATE_TYPE, RISK_ID,HISTORY_ID)"
				+ "SELECT "+JecnCommonSql.getSqlGUID()+",RULE_ID,1 RELATE_TYPE,RISK_ID, " + historyId + " HISTORY_ID"
				+ "   FROM JECN_RULE_RISK_T WHERE RULE_ID=" + relateId;
	}

	/**
	 * JECN_STANDARD_HISTORY 相关标准
	 * @param relateId
	 * @param historyId
	 * @return RELATE_TYPE ： 0 流程 1制度 2 文件
	 */
	private static String getJecnStandardHistory(Long relateId, Long historyId) {
		return "INSERT INTO JECN_STANDARD_HISTORY " + "(ID,RELATE_ID,RELATE_TYPE,STANDARD_ID,HISTORY_ID)"
				+ "SELECT "+JecnCommonSql.getSqlGUID()+",RULE_ID,1  RELATE_TYPE,STANDARD_ID, " + historyId + " HISTORY_ID" +
				 "  FROM JECN_RULE_STANDARD_T WHERE RULE_ID=" + relateId;
	}
	
	/**
	 * JECN_STANDARD_HISTORY 相关制度
	 * @param relateId
	 * @param historyId
	 * @return RELATE_TYPE ： 0 流程 1制度 2 文件
	 */
	private static String getJecnRuleHistory(Long relateId, Long historyId) {
		return "INSERT INTO JECN_RULE_RELATED_RULE_H " + "(GUID,RULE_ID,RELATED_ID,HISTORY_ID)"
				+ "SELECT GUID,"+relateId+" RULE_ID,RELATED_ID," + historyId + " HISTORY_ID" +
				 "  FROM JECN_RULE_RELATED_RULE_T WHERE RULE_ID=" + relateId;
	}


	
	/**
	 * JECN_RULE_BASIC_INFO_HISTORY 添加制度基本信息表
	 * @param relateId
	 * @param historyId
	 * @return
	 */
	private static String getJecnRuleBasicInfoHistory(Long ruleId, Long historyId) {
		return "insert into JECN_RULE_BASIC_INFO_HISTORY(RULE_ID,BUSINESS_SCOPE," + "	OTHER_SCOPE,	PURPOSE,"
				+ "	APPLICABILITY,	WRITE_DEPT,	" + "	DRAFTMAN,		DRAFTING_UNIT	,	" + "	TEST_RUN_FILE,            CHANGE_VERSION_EXPLAIN,HISTORY_ID) "
				+ "select RULE_ID	,	BUSINESS_SCOPE,	" + "	OTHER_SCOPE,	PURPOSE," + "	APPLICABILITY,	WRITE_DEPT,	"
				+ "	DRAFTMAN,		DRAFTING_UNIT	,	" + "	TEST_RUN_FILE, CHANGE_VERSION_EXPLAIN," + historyId + "  HISTORY_ID"
				+ " from JECN_RULE_BASIC_INFO_T where RULE_ID=" + ruleId;
	} 

	/**
	 * JECN_RELATE_APPLY_ORG_HISTORY 组织范围
	 * @param ruleId
	 * @param historyId
	 * @return RELATE_TYPE ： 0 流程 1制度 2 文件
	 * 
	 */
	private static String getJecnRuleApplyOrgHistory(Long relateId, Long historyId) {
		return "insert into JECN_RELATE_APPLY_ORG_HISTORY" + 
				"(ID,RELATED_ID,RELATE_TYPE,HISTORY_ID,ORG_ID)" + 
				"select "+JecnCommonSql.getSqlGUID()+", RELATED_ID, 1  RELATE_TYPE ," + historyId + " HISTORY_ID ,ORG_ID" +
				" from JECN_RULE_APPLY_ORG_T where RELATED_ID="+ relateId;
	}

	/**
	 * RULE_FILE_HISTORY 制度文件
	 * @param ruleId
	 * @param historyId
	 * @return
	 */
	private static String getRuleFileHistory(Long ruleId, Long historyId) {
		return "insert into RULE_FILE_HISTORY(ID, RULE_TITLE_ID, RULE_FILE_ID,HISTORY_ID,GUID)"+
		"select rft.ID," + 
		"       rft.RULE_TITLE_ID," + 
		"       fc.VERSION_ID RULE_FILE_ID," + 
		 historyId + " HISTORY_ID," + JecnCommonSql.getSqlGUID()+" GUID" + 
		"  from RULE_TITLE_T rtt, RULE_FILE_T rft" + 
		" inner join jecn_file_t fc" + 
		"    on rft.RULE_FILE_ID = fc.FILE_ID" + 
		" where rft.rule_title_id = rtt.id" + 
		"   and rtt.rule_id ="+ruleId;
	}

	
	/**
	 * RULE_CONTENT_HISTORY 制度内容
	 * 
	 * @param ruleId
	 * @param historyId
	 * @return
	 */
	private static String getRuleContentHistory(Long ruleId, Long historyId) {
		return "insert  into RULE_CONTENT_HISTORY(ID,RULE_TITLE_ID,CONTENT,HISTORY_ID,GUID)"
				+ " select rft.ID,rft.RULE_TITLE_ID,rft.CONTENT," + historyId + " HISTORY_ID,"
				+JecnCommonSql.getSqlGUID()+ " GUID from rule_content_t rft,RULE_TITLE_T rtt where rft.rule_title_id=rtt.id "
				+ " and rtt.rule_id=" + ruleId;
	}

	
	/**
	 * RULE_TITLE_HISTORY 制度标题
	 * 
	 * @param ruleId
	 * @param historyId
	 * @return
	 */
	private static String getRuleTitleHistory(Long ruleId, Long historyId) {
		return "insert into RULE_TITLE_HISTORY(ID, RULE_ID, TITLE_NAME, ORDER_NUMBER, TYPE,HISTORY_ID,GUID,EN_NAME) "
				+ "select ID,RULE_ID,TITLE_NAME,ORDER_NUMBER,TYPE," + historyId
				+ "  HISTORY_ID, "+JecnCommonSql.getSqlGUID()+" GUID,EN_NAME from   rule_title_t  where rule_id="+ ruleId;
	}

	
	/**
	 * STANDARDIZED_FILE_HISTORY
	 * 
	 * @param ruleId
	 * @param historyId
	 * @return RELATE_TYPE ： 0 流程 1制度 2 文件
	 */
	private static String getRuleStandardizedFile(Long relatedId, Long historyId) {
		return "INSERT INTO STANDARDIZED_FILE_HISTORY " + 
		"(ID,RELATED_ID,FILE_ID,RELATE_TYPE,HISTORY_ID) " +
		"SELECT ht.ID, ht.RELATED_ID, fc.VERSION_ID, 1 RELATE_TYPE, "+historyId+" HISTORYID" +
		"  FROM RULE_STANDARDIZED_FILE_T ht" + 
		"  inner join jecn_file fc" + 
		"    on fc.FILE_ID = ht.file_id" + 
		" WHERE RELATED_ID = "+relatedId;
		 
	}

	/**
	 * 记录制度文件历史信息
	 * 
	 * @param ruleId
	 * @param historyId
	 * @param baseDao
	 */
	public static void recordRuleFileHistory(Long ruleId, Long historyId, IBaseDao baseDao) {
		baseDao.execteNative(getJecnRuleHistorySql(ruleId, historyId));// 制度主表
		baseDao.execteNative(getJecnAuthorityGroupHistory(ruleId, historyId));// 岗位组
		baseDao.execteNative(getJecnAuthorityOrgHistory(ruleId, historyId));// 部门
		baseDao.execteNative(getJecnAuthorityAccessHistory(ruleId, historyId));// 岗位
		baseDao.execteNative(getJecnRuleRiskHistory(ruleId, historyId));// 相关风险
		baseDao.execteNative(getJecnStandardHistory(ruleId, historyId));// 相关标准
		baseDao.execteNative(getJecnRuleHistory(ruleId,historyId)); //相关制度
		baseDao.execteNative(getJecnRuleBasicInfoHistory(ruleId, historyId));// 制度基本信息
		baseDao.execteNative(getJecnRuleApplyOrgHistory(ruleId, historyId));// 组织范围
	}

	/**
	 * 记录制度标准化文件历史信息
	 * 
	 * @param ruleId
	 * @param historyId
	 * @param baseDao
	 */
	public static void recordRuleStandardizedFileHistory(Long ruleId, Long historyId, IBaseDao baseDao) {
		baseDao.execteNative(getRuleStandardizedFile(ruleId, historyId));
	}

	/**
	 * 记录制度模板历史信息
	 * 
	 * @param ruleId
	 * @param historyId
	 * @param baseDao
	 */
	public static void recordRuleModeHistory(Long ruleId, Long historyId, IBaseDao baseDao) {
		baseDao.execteNative(getRuleFileHistory(ruleId, historyId));// 制度文件
		baseDao.execteNative(getRuleContentHistory(ruleId, historyId));// 制度内容
		baseDao.execteNative(getRuleTitleHistory(ruleId, historyId));// 制度标题
	}
}
