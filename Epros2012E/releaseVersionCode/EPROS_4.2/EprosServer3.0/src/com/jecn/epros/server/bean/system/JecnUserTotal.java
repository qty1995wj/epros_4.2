package com.jecn.epros.server.bean.system;

import java.io.Serializable;
import java.util.Date;

public class JecnUserTotal implements Serializable {

	/**
	 * 主键ID
	 */
	private Long id;
	/**
	 * 时间段
	 */
	private Date accessDate;
	/**
	 * 用户ID
	 */
	private Long peopleId;
	/**
	 * 流程ID
	 */
	private Long flowId;

	/**
	 *表的类型 0:查看流程统计(流程图) 1:下载次数统计（下载流程文件，包含设计器浏览端）
	 */
	private Integer type;
	/**
	 * 下载请求来源 0浏览器 1设计器
	 */
	private Integer accessFrom;

	public Integer getAccessFrom() {
		return accessFrom;
	}

	public void setAccessFrom(Integer accessFrom) {
		this.accessFrom = accessFrom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getAccessDate() {
		return accessDate;
	}

	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}

	public Long getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
}
