package epros.designer.gui.process;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.bean.checkout.CheckoutItem;
import com.jecn.epros.bean.checkout.CheckoutResult;

import epros.designer.gui.integration.MultiLineCellRenderer;
import epros.designer.gui.rule.RuleDoControlPanel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 活动贯通性检查
 * 
 * @author hyl
 * 
 */
public class ThroughCheckDialog extends JecnDialog {

	private Long flowId;
	private List<CheckoutResult> results = null;
	private static Logger log = Logger.getLogger(RuleDoControlPanel.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 结果滚动面板 */
	private JScrollPane resultScrollPane = new JScrollPane();

	/** 信息显示Table */
	private RuleDocControlTable table = null;

	/** 设置大小 */
	private Dimension dimension = null;

	/** 查阅 */
	private JButton searchBut = new JButton(JecnProperties.getValue("inspection"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties.getValue("closed"));
	private Set<Integer> flowLines = new HashSet<Integer>();
	private Set<Integer> inLines = new HashSet<Integer>();
	private Set<Integer> outLines = new HashSet<Integer>();
	private Set<Integer> inoutTitleLines = new HashSet<Integer>();
	private int index = 0;
	private String split = "\r\n";

	public ThroughCheckDialog(Long flowId) {
		this.flowId = flowId;
		try {
			results = ConnectionPool.getProcessAction().checkout(flowId);
		} catch (Exception e) {
			log.error("", e);
		}
		init();
	}

	private void init() {
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setSize(670, 500);
		// 设置窗体大小
		this.setSize(670, 500);
		this.setTitle(JecnProperties.getValue("flowCheckout"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(true);
		this.setModal(true);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		dimension = new Dimension(620, 360);
		resultScrollPane.setPreferredSize(dimension);
		resultScrollPane.setMaximumSize(dimension);
		resultScrollPane.setMinimumSize(dimension);

		table = new RuleDocControlTable();
		// 设置默认背景色
		table.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		table.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		table.setRowHeight(19);
		resultScrollPane.setViewportView(table);
		table.setDefaultRenderer(Object.class, new TableCellTextAreaRenderer());// 红色标记部分是用来渲染JTable的自定义绘制器

		initLayout();
		/***********************************************************************
		 * 查阅
		 */
		searchBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				searchButPerformed();

			}

		});
		// 取消
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelPerformed();
			}
		});
		// 选中一条文控信息 ，双击表格，打开文控编辑框
		table.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					ruleDocControlTableMousePressed();
				}
			}
		});

		// table.addMouseListener(new MouseAdapter() {
		// public void mousePressed(MouseEvent evt) {
		// searchTablemousePressed(evt);
		// }
		// });
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:双击Table
	 */
	protected void searchTablemousePressed(MouseEvent evt) {
		if (evt.getClickCount() == 1) {
			int row = table.getSelectedRow();
			int oldRowHeight = table.getRowHeight();
			int newRowHeight = table.getRowHeight(row);
			if (row != -1) {
				if (oldRowHeight >= newRowHeight) {
					MultiLineCellRenderer multiLineCellRenderer = new MultiLineCellRenderer(row, 1, 0);
					multiLineCellRenderer.setToolTipText(table.getValueAt(row, 1).toString());
					table.setDefaultRenderer(Object.class, multiLineCellRenderer);
				} else if (oldRowHeight < newRowHeight) {
					// 选中后高度设置
					table.setRowHeight(row, oldRowHeight);
					MultiLineCellRenderer multiLineCellRenderer = new MultiLineCellRenderer(row, 1, 1);
					multiLineCellRenderer.setToolTipText(table.getValueAt(row, 1).toString());
					table.setDefaultRenderer(Object.class, new MultiLineCellRenderer(row, 1, 1));
				}
			}
		}
	}

	/***************************************************************************
	 * 双击文控表格事件
	 * 
	 */
	private void ruleDocControlTableMousePressed() {

	}

	/***************************************************************************
	 * 文控编辑面板
	 * 
	 */
	private void openpublishPerformed(int selectRow) {

	}

	/***************************************************************************
	 * 查阅
	 * 
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 */
	@SuppressWarnings("unchecked")
	private void searchButPerformed() {

	}

	/**
	 * 布局
	 */
	private void initLayout() {
		searchBut.setVisible(false);
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 5, 5);

		// 制度文控信息表格
		c = new GridBagConstraints(0, 0, 5, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(resultScrollPane, c);
		// 查阅
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(searchBut, c);
		// 取消 cancelBut
		c = new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		mainPanel.add(cancelBut, c);

		this.getContentPane().add(mainPanel);
	}

	/***************************************************************************
	 * 取消
	 */
	private void cancelPerformed() {
		this.dispose();
	}

	/**
	 * 制度文控表格显示信息
	 * 
	 * @param list
	 * @return
	 */
	public Vector<Vector<Object>> updateTableContent() {
		Vector<Vector<Object>> vector = new Vector<Vector<Object>>();
		if (this.results != null && results.size() > 0) {
			for (int i = 0; i < results.size(); i++) {
				CheckoutResult checkout = results.get(i);
				Vector<Object> data = new Vector<Object>();
				data.add(checkout.getName() + "(" + JecnProperties.getValue("curFlow") + ")");
				data.add("");
				data.add("");
				vector.add(data);
				flowLines.add(index++);

				data = new Vector<Object>();
				data.add(JecnProperties.getValue("mainFlowIn"));// 主流程输入
				data.add(JecnProperties.getValue("hasSource"));
				data.add(JecnProperties.getValue("curFlowInUse"));// "是否在本流程被使用"
				vector.add(data);
				inoutTitleLines.add(index++);
				vector.addAll(addInouts(checkout.getIns(), 0));

				data = new Vector<Object>();
				data.add(JecnProperties.getValue("mainFlowOut"));// "主流程输出"
				data.add(JecnProperties.getValue("sonFlowInUse"));// "是否在下游流程被使用"
				data.add(JecnProperties.getValue("curFlowGenerate"));// "是否在本流程产生"
				vector.add(data);
				inoutTitleLines.add(index++);
				vector.addAll(addInouts(checkout.getOuts(), 1));

				if (JecnUtil.isNotEmpty(checkout.getSons())) {
					for (CheckoutResult item : checkout.getSons()) {
						data = new Vector<Object>();
						data.add(item.getName() + "(" + JecnProperties.getValue("sonFlow") + ")");// 子流程
						data.add("");
						data.add("");
						vector.add(data);
						flowLines.add(index++);

						data = new Vector<Object>();
						data.add(JecnProperties.getValue("sonFlowIn"));// "子流程输入"
						data.add(JecnProperties.getValue("hasSource"));// "是否有来源"
						data.add("");
						vector.add(data);
						inoutTitleLines.add(index++);
						vector.addAll(addInouts(item.getIns(), 0));

						data = new Vector<Object>();
						data.add(JecnProperties.getValue("sonFlowOut"));// "子流程输出"
						data.add(JecnProperties.getValue("curFlowInUse"));// "是否在本流程被使用"
						data.add("");
						vector.add(data);
						inoutTitleLines.add(index++);
						vector.addAll(addInouts(item.getOuts(), 1));
					}
				}
			}
		}
		return vector;
	}

	private List<Vector<Object>> addInouts(List<CheckoutItem> items, int type) {
		List<Vector<Object>> result = new ArrayList<Vector<Object>>();
		if (JecnUtil.isNotEmpty(items)) {
			for (CheckoutItem item : items) {
				if (type == 0) {
					inLines.add(index);
				} else if (type == 1) {
					outLines.add(index);
				}
				index++;
				Vector<Object> data = new Vector<Object>();
				result.add(data);
				data.add(item.getName());
				data.add(JecnUtil.concat(item.getCellA(), split));
				data.add(JecnUtil.concat(item.getCellB(), split));
			}
		}
		return result;
	}

	class RuleDocControlTable extends JTable {

		RuleDocControlTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			}
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				TableColumnModel columnModel = this.getColumnModel();
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
			this.getTableHeader().setVisible(false);
		}

		public RuleDocControlTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("");
			title.add("");
			title.add("");
			return new RuleDocControlTableMode(updateTableContent(), title);
		}

		public boolean isSelectMutil() {
			return true;
		}

		public int[] gethiddenCols() {

			return new int[] {};
		}

		class RuleDocControlTableMode extends DefaultTableModel {
			public RuleDocControlTableMode(Vector<Vector<Object>> data, Vector<String> title) {
				super(data, title);

			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return false;
			}
		}

		@Override
		public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
			Component component = super.prepareRenderer(renderer, row, column);
			component.setBackground(Color.WHITE);
			component.setFont(new Font("宋体", Font.PLAIN, 12));
			if (row == 0) {
				component.setBackground(new Color(116, 195, 247));
				component.setFont(new Font("宋体", Font.BOLD, 12));
			} else if (flowLines.contains(row)) {
				component.setBackground(new Color(160, 206, 250));
				component.setFont(new Font("宋体", Font.BOLD, 12));
			} else if (inLines.contains(row)) {
				if (column == 1 && isblack(row, column)) {
					component.setBackground(new Color(255, 0, 0));
				}
			} else if (outLines.contains(row)) {
				if (column == 1 && isblack(row, column)) {
					component.setBackground(new Color(253, 159, 2));
				}
			} else if (inoutTitleLines.contains(row)) {
				component.setFont(new Font("宋体", Font.BOLD, 12));
			} else {
				component.setBackground(Color.WHITE);
			}

			return component;
		}

		private boolean isblack(int row, int column) {
			return StringUtils.isBlank(JecnUtil.nullToEmpty(this.getValueAt(row, column)));
		}

	}

	// 自定义的表格绘制器
	class TableCellTextAreaRenderer extends JTextArea implements TableCellRenderer {
		public TableCellTextAreaRenderer() {
			setLineWrap(true);
			setWrapStyleWord(true);
		}

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			// 计算当下行的最佳高度
			int maxPreferredHeight = 0;
			for (int i = 0; i < table.getColumnCount(); i++) {
				setText("" + table.getValueAt(row, i));
				setSize(table.getColumnModel().getColumn(column).getWidth(), 0);
				maxPreferredHeight = Math.max(maxPreferredHeight, getPreferredSize().height);
			}

			if (table.getRowHeight(row) != maxPreferredHeight) // 少了这行则处理器瞎忙
				table.setRowHeight(row, maxPreferredHeight);

			setText(value == null ? "" : value.toString());
			return this;
		}
	}

}
