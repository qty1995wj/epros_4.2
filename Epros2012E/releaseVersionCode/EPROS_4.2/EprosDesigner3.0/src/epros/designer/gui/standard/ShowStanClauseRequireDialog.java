package epros.designer.gui.standard;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.standard.StandardBean;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 关联标准详情弹出
 * 
 * @author zhagnxiaohu
 * @date： 日期：2013-11-27 时间：下午02:41:22
 */
public class ShowStanClauseRequireDialog extends StanClauseRequireDialog {
	private static Logger log = Logger.getLogger(ShowStanClauseRequireDialog.class);
	/** 节点是否存在 */
	private boolean isExists = true;

	public boolean isExists() {
		return isExists;
	}

	public ShowStanClauseRequireDialog(Long standardId) {
		super();

		// 条款要求
		this.setTitle(JecnProperties.getValue("stanClauseRequire"));
		init(standardId);
	}

	private void init(Long standardId) {
		StandardBean standardBean = null;
		try {
			standardBean = ConnectionPool.getStandardAction().getStandardBean(
					standardId);
			if (standardBean == null) {
				// 条款要求已删除！
				JecnOptionPane.showMessageDialog(this, JecnProperties
						.getValue("termsOfRequirements"));
				isExists = false;
				return;
			}
			clauseRequireArea.setText(standardBean.getStanContent());
			clauseRequireArea.setEditable(false);
			this.okButton.setVisible(false);
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("accessClauseRequiresDataError"));
			log.error(JecnProperties.getValue("accessClauseRequiresDataError"), e);
		}
	}

	@Override
	public boolean isUpdate() {
		return false;
	}
	@Override
	public void okButtonAction() {
		this.setVisible(false);
	}
}
