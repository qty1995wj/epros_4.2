package epros.designer.gui.autoNum;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.autoCode.AutoCodeBean;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.ConfigCommon;
import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.file.FileManageDialog;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 流程、制度、文件自动编号管理页面
 * 
 * @author ZXH
 * 
 */
public class AutoNumSelectDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(AutoNumSelectDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 自动编号tree面板 */
	private AutoNumJecnTree jecnTree = null;

	private JecnPanel buttonPanel = null;

	/** 公司代码 */
	private TableScroll companyCodeScrollPanel = null;
	/** 生产线或小部门 */
	private TableScroll productionLineScrollPanel = null;
	/** 文件类型 */
	private TableScroll fileTypeScrollPanel = null;
	/** 小类别 */
	private TableScroll smallClassScrollPanel = null;

	/** 流程级别 */
	private TableScroll processLevelScrollPanel = null;

	/** 树面板 */
	private JScrollPane treePanel = null;

	private JButton okButton = null;

	private JecnConfigItemBean companyItem;
	private JecnConfigItemBean productionLineItem;
	private JecnConfigItemBean fileTypeItem;
	private JecnConfigItemBean smallClassItem;
	private JecnConfigItemBean processLevelItem;

	/** 名称验证提示 */
	private JLabel promptLab = null;
	private JLabel viewLab = null;

	private static final String SEPARATOR_N = "\n";

	private JecnTreeNode selectNode;
	/** 0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件 */
	private int relatedType;

	private AutoCodeBean codeBean = null;

	/** true:加载本地 */
	private boolean isLocal;

	private JecnDialog dialog;

	private int treeType;

	public AutoNumSelectDialog(JecnTreeNode selectNode) {
		this.selectNode = selectNode;
		initTreeType();
		initData();
		initCompotents();
		initLayout();
		// 树反选
		allowExpandByName();
	}

	public AutoNumSelectDialog(JecnTreeNode selectNode, JecnDialog dialog) {
		super(dialog);
		this.dialog = dialog;
		this.selectNode = selectNode;
		initTreeType();
		initData();
		initCompotents();
		initLayout();
		// 树反选
		allowExpandByName();
	}

	private void initTreeType() {
		TreeNodeType nodeType = selectNode.getJecnTreeBean().getTreeNodeType();
		if (nodeType == TreeNodeType.process) {
			treeType = 0;
		} else if (nodeType == TreeNodeType.file || nodeType == TreeNodeType.fileDir) {
			treeType = 1;
		} else if (nodeType == TreeNodeType.ruleFile || nodeType == TreeNodeType.ruleModeFile) {
			treeType = 2;
		}
	}

	private void initData() {
		companyItem = JecnConfigTool.getAutoComptyCode(treeType);
		productionLineItem = JecnConfigTool.getAutoProductionLine(treeType);
		fileTypeItem = JecnConfigTool.getAutoFileType(treeType);
		smallClassItem = JecnConfigTool.getAutoSmallClass(treeType);
		processLevelItem = JecnConfigTool.getProcessLevel();
		relatedType = getSelectNodeType(selectNode);

		try {
			codeBean = ConnectionPool.getAutoCodeAction().getAutoCodeBean(selectNode.getJecnTreeBean().getId(),
					relatedType);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(JecnProperties.getValue("autoNumDataERROR"), e);
			return;
		}
		if (codeBean == null) {
			// 读取本地配置
			codeBean = ConfigCommon.INSTANTCE.getAutoDataByConfig(relatedType);
			isLocal = true;
		}
	}

	private void initCompotents() {
		mainPanel = new JPanel();
		// 设置Dialog大小
		this.setSize(755, 450);
		this.setResizable(false);
		this.setModal(true);
		this.setTitle(JecnProperties.getValue("autoNum"));
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		jecnTree = getTree();

		buttonPanel = new JecnPanel();
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		companyCodeScrollPanel = new TableScroll(getStringArray(companyItem.getValue()), AutoCodeEnum.companyCode);
		productionLineScrollPanel = new TableScroll(getStringArray(productionLineItem.getValue()),
				AutoCodeEnum.productionLine);
		fileTypeScrollPanel = new TableScroll(getStringArray(fileTypeItem.getValue()), AutoCodeEnum.fileType);
		smallClassScrollPanel = new TableScroll(getStringArray(smallClassItem.getValue()), AutoCodeEnum.smallClass);

		okButton = new JButton(JecnProperties.getValue("autoNum"));

		companyCodeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				companyItem.getName()));
		productionLineScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				productionLineItem.getName()));
		fileTypeScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), fileTypeItem
				.getName()));
		smallClassScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				smallClassItem.getName()));

		processLevelScrollPanel = new TableScroll(getStringArray(processLevelItem.getValue()),
				AutoCodeEnum.processLevel);
		processLevelScrollPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				processLevelItem.getName()));

		treePanel = new JScrollPane();

		treePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 边框
		// 树滚动面板
		treePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("processCode")));
		treePanel.setViewportView(jecnTree);
		this.getContentPane().add(mainPanel);

		promptLab = createJLabel();
		if (relatedType > 1) {// 不是流程和架构
			promptLab.setText(JecnProperties.getValue("autoCodeTip"));
		}
		viewLab = createJLabel();

//		if (iscreateSingleNode()) {
			viewLab.setText(selectNode.getJecnTreeBean().getNumberId());
//		}

		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isCheckUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButtonAction();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	private JLabel createJLabel() {
		JLabel jLabel = new JLabel();
		jLabel.setForeground(Color.red);
		// 设置验证提示Label的大小
		return jLabel;
	}

	private String[] getStringArray(String value) {
		return value == null ? null : value.split(SEPARATOR_N);
	}

	private void okButtonAction() {

		// 1、判断是否允许为空
		String companyCode = companyCodeScrollPanel.getTableCellValue();
		if (DrawCommon.isNullOrEmtryTrim(companyCode) || "-1".equals(companyCode)) {
			promptLab.setText(JecnProperties.getValue("companyCodeIsNull"));
			return;
		}

		String processCode = getProcessCodeByTree();
		if (DrawCommon.isNullOrEmtryTrim(processCode) || "-1".equals(processCode)) {
			promptLab.setText(JecnProperties.getValue("processCodeIsNull"));
			return;
		}

		String processLevel = processLevelScrollPanel.getTableCellValue();
		if (relatedType == 1 && (DrawCommon.isNullOrEmtryTrim(processLevel) || "-1".equals(processLevel))) {// 流程自动编号，判断层级是否存在
			promptLab.setText(JecnProperties.getValue("hierarchyIsNull"));
			return;
		}

		// 2、true：更新
		boolean isUpdate = isCheckUpdate();
		if (!isUpdate && iscreateSingleNode()) {
			this.dispose();
			return;
		}

		if (isUpdate) {
			int dialog = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("isAutoCode"), null,
					JecnOptionPane.YES_NO_OPTION);
			// 是否自动编号
			if (dialog == JecnOptionPane.NO_OPTION) {
				return;
			}
		}

		String productionLine = productionLineScrollPanel.getTableCellValue();
		String fileType = fileTypeScrollPanel.getTableCellValue();
		String smallClass = smallClassScrollPanel.getTableCellValue();

		AutoCodeBean codeBean = new AutoCodeBean();
		codeBean.setCompanyCode(getSplitCode(companyCode));
		codeBean.setProcessCode(getSplitCode(processCode));
		codeBean.setProductionLine(getSplitCode(productionLine));
		codeBean.setFileType(getSplitCode(fileType));
		codeBean.setSmallClass(getSplitCode(smallClass));
		codeBean.setProcessLevel(getSplitCode(processLevel));
		codeBean.setCreatePersonId(JecnConstants.getUserId());
		try {
			// 3、获取数据源，更新数据库
			codeBean = ConnectionPool.getAutoCodeAction().saveAutoCode(codeBean, selectNode.getJecnTreeBean().getId(),
					relatedType);
			// 记录本地
			ConfigCommon.INSTANTCE.setAutoDataByCodeBean(codeBean, relatedType);
			// 保存成功,更新数据对象
			this.codeBean = codeBean;
			isLocal = false;
			if (iscreateSingleNode()) {// 自动编号
				selectNode.getJecnTreeBean().setNumberId(codeBean.getResultCode());
				viewLab.setText(codeBean.getResultCode());
			} else {
				Long id = selectNode.getJecnTreeBean().getId();
				if (this.relatedType == 0) {
					List<JecnTreeBean> list = JecnDesignerCommon.getDesignerProcessTreeBeanList(id, JecnDesignerCommon
							.isOnlyViewDesignAuthor());
					JecnTreeCommon
							.refreshNode(selectNode, list, JecnDesignerMainPanel.getDesignerMainPanel().getTree());
				} else if (this.relatedType == 2) {
					List<JecnTreeBean> list = ConnectionPool.getRuleAction().getChildRules(id, JecnConstants.projectId);
					JecnTreeCommon
							.refreshNode(selectNode, list, JecnDesignerMainPanel.getDesignerMainPanel().getTree());
				} else if (this.relatedType == 4) {
					List<JecnTreeBean> list = ConnectionPool.getFileAction().getChildFiles(id, JecnConstants.projectId);
					JecnTree jecnTree = null;
					if (dialog instanceof FileManageDialog) {
						jecnTree = ((FileManageDialog) dialog).getjTree();
					} else if (dialog instanceof FileChooseDialog) {
						jecnTree = ((FileChooseDialog) dialog).getjTree();
					}
					if (jecnTree != null) {
						JecnTreeCommon.refreshNode(selectNode, list, jecnTree);
					}
				}
				this.dispose();
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("systemError"));
			log.error("codeRule is error！", e);
			return;
		}

	}

	/**
	 * 检查是否更新
	 * 
	 * @return
	 */
	private boolean isCheckUpdate() {
		if (isLocal) {
			return true;
		}
		if (isEqualsUpdate(codeBean.getCompanyCode(), getSplitCode(companyCodeScrollPanel.getTableCellValue()))) {
			return true;
		} else if (isEqualsUpdate(codeBean.getProcessCode(), getSplitCode(getProcessCodeByTree()))) {
			return true;
		} else if (isEqualsUpdate(codeBean.getProductionLine(), getSplitCode(productionLineScrollPanel
				.getTableCellValue()))) {
			return true;
		} else if (isEqualsUpdate(codeBean.getFileType(), getSplitCode(fileTypeScrollPanel.getTableCellValue()))) {
			return true;
		} else if (isEqualsUpdate(codeBean.getSmallClass(), getSplitCode(smallClassScrollPanel.getTableCellValue()))) {
			return true;
		} else if (isEqualsUpdate(codeBean.getProcessLevel(), getSplitCode(processLevelScrollPanel.getTableCellValue()))) {
			return true;
		}
		return false;
	}

	private boolean isEqualsUpdate(String oldStr, String newStr) {
		if (StringUtils.isBlank(oldStr) && StringUtils.isBlank(newStr)) {
			return false;
		}
		return StringUtils.isBlank(oldStr) && StringUtils.isNotBlank(newStr) ? true : !oldStr.equals(newStr);
	}

	private void allowExpandByName() {
		try {
			if (codeBean == null) {
				return;
			}
			JecnTreeBean treeBean = ConnectionPool.getAutoCodeAction().getTreeBeanByName(codeBean.getProcessCode());
			if (treeBean == null) {
				return;
			}
			jecnTree.allowExpandByTreeBean(treeBean);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("allowExpandByName is error", e);
		}
	}

	/**
	 * 是否生成单个节点编号
	 * 
	 * @return
	 */
	private boolean iscreateSingleNode() {
		if (relatedType == 1 || relatedType == 3 || relatedType == 4 || relatedType == 5) {
			return true;
		}
		return false;
	}

	/**
	 * 0是流程架构、1是流程、2是制度目录、3是制度模板/制度文件（本地上传）、4是文件目录、5是文件
	 * 
	 * @param nodeType
	 * @return
	 */
	private int getSelectNodeType(JecnTreeNode jecnTreeNode) {
		int relatedType = 0;
		TreeNodeType nodeType = jecnTreeNode.getJecnTreeBean().getTreeNodeType();
		if (nodeType == TreeNodeType.processMap) {
			relatedType = 0;
		} else if (nodeType == TreeNodeType.process) {
			relatedType = 1;
		} else if (nodeType == TreeNodeType.ruleDir) {
			relatedType = 2;
		} else if (nodeType == TreeNodeType.ruleModeFile) {
			relatedType = 3;
		} else if (nodeType == TreeNodeType.ruleFile && jecnTreeNode.getJecnTreeBean().getDataType() == 1) {
			relatedType = 3;
		} else if (nodeType == TreeNodeType.fileDir) {
			relatedType = 4;
		} else if (nodeType == TreeNodeType.file) {
			relatedType = 5;
		}
		return relatedType;
	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(5, 5, 0, 5);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(companyCodeScrollPanel, c);

		if (relatedType == 1 || relatedType == 0) {
			addProcessAutoPanel(c, insets);
		} else {
			addOtherAutoPanel(c, insets);
		}
		// 添加底部面板
		JecnPanel bottom = new JecnPanel();

		c = new GridBagConstraints(0, 1, 5, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(bottom, c);
		addBottomPanel(bottom);
	}

	private void addOtherAutoPanel(GridBagConstraints c, Insets insets) {
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(treePanel, c);

		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(productionLineScrollPanel, c);

		c = new GridBagConstraints(3, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(fileTypeScrollPanel, c);

		c = new GridBagConstraints(4, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(smallClassScrollPanel, c);
	}

	/**
	 * 添加层级和过程代码
	 * 
	 * @param c
	 * @param insets
	 */
	private void addProcessAutoPanel(GridBagConstraints c, Insets insets) {
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(processLevelScrollPanel, c);

		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		mainPanel.add(treePanel, c);
	}

	private void addBottomPanel(JecnPanel bottom) {
		bottom.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0);
		bottom.add(viewLab, c);

		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(
				0, 5, 0, 5), 0, 0);
		bottom.add(buttonPanel, c);
		// 添加按钮
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(promptLab);
		buttonPanel.add(okButton);
	}

	class TableScroll extends JScrollPane {
		private ResultTable resultTable;

		private AutoCodeEnum codeEnum;

		public TableScroll(String[] strs, AutoCodeEnum codeEnum) {
			resultTable = new ResultTable(strs, codeEnum);
			initCompotents();
			this.codeEnum = codeEnum;
		}

		public ResultTable getResultTable() {
			return resultTable;
		}

		private void initCompotents() {
			Dimension dimension = new Dimension(50, 370);
			this.setMinimumSize(dimension);
			// 滚动条不显示
			this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			// 背景颜色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 边框
			this.setBorder(null);
			this.setViewportBorder(null);
			this.setViewportView(resultTable);

			this.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 2
							&& !(codeEnum == AutoCodeEnum.companyCode || codeEnum == AutoCodeEnum.processLevel)) {// 双击
						clearSelection();
					}
				}
			});
		}

		private void clearSelection() {
			resultTable.clearSelection();
		}

		/**
		 * 单元格编码
		 * 
		 * @return
		 */
		public String getTableCellValue() {
			if (resultTable.getSelectedRowCount() == 0) {
				return "-1";
			}
			if ("".equals(resultTable.getModel().getValueAt(resultTable.getSelectedRow(), 0).toString())) {
				return "-1";
			}
			return resultTable.getModel().getValueAt(resultTable.getSelectedRow(), 0).toString();
		}
	}

	private String getSplitCode(Object obj) {
		if (obj == null) {
			return "-1";
		}
		String[] strs = obj.toString().split(" ");
		return strs[0];
	}

	class ResultTable extends JecnTable {
		private String[] strs;
		private AutoCodeEnum codeEnum;
		private String curCode = "";

		public ResultTable(String[] strs, AutoCodeEnum codeEnum) {
			this.strs = strs;
			this.codeEnum = codeEnum;
			curCode = getCodeRules(codeEnum);
			initTableModel();
			hiddenTableHeader();
			// 表格反选
			selectCell();
		}

		/** 初始化model */
		void initTableModel() {
			this.setModel(getTableModel());
			this.setShowGrid(false);
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			// 自定义表头UI
			// this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		}

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent());
		}

		@Override
		public int[] gethiddenCols() {
			return null;
		}

		@Override
		public boolean isSelectMutil() {
			return false;
		}

		private Vector<Vector<String>> getContent() {
			Vector<Vector<String>> content = new Vector<Vector<String>>();
			if (strs != null) {
				for (String str : strs) {
					Vector<String> data = new Vector<String>();
					data.add(str);
					content.add(data);
				}
			}
			return content;
		}

		private Vector<String> getTableTitle() {
			Vector<String> title = new Vector<String>();
			title.add("");
			return title;
		}

		private void selectCell() {
			int row = 0;
			if (strs == null) {
				return;
			}
			for (String str : strs) {
				if (getSplitCode(str).equals(curCode)) {
					// 表格反选
					this.setRowSelectionInterval(0, row);
					return;
				}
				row++;
			}
		}
	}

	/**
	 * tree 选中节点名称编号
	 * 
	 * @return
	 */
	private String getProcessCodeByTree() {
		JecnTreeNode selectNode = jecnTree.getSelectNode();
		if (selectNode == null || selectNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processCodeRoot) {
			return null;
		}
		return selectNode.getJecnTreeBean().getName();
	}

	private String getCodeRules(AutoCodeEnum codeEnum) {
		if (codeBean == null) {
			return "";
		}
		switch (codeEnum) {
		case companyCode:
			return codeBean.getCompanyCode();
		case productionLine:
			return codeBean.getProductionLine();
		case fileType:
			return codeBean.getFileType();
		case smallClass:
			return codeBean.getSmallClass();
		case processLevel:
			return codeBean.getProcessLevel();
		default:
			break;
		}
		return "";
	}

	private AutoNumJecnTree getTree() {
		if (treeType == 0) {
			return new ProcessAutoNumJecnTree();
		} else if (treeType == 1) {
			return new FileAutoNumJecnTree();
		} else if (treeType == 2) {
			return new RuleAutoNumJecnTree();
		}
		return null;
	}

	/**
	 * 自动编号表格选项分类
	 * 
	 * @author ZXH
	 * 
	 */
	enum AutoCodeEnum {
		companyCode, productionLine, fileType, smallClass, processLevel
	}

	public AutoCodeBean getCodeBean() {
		return codeBean;
	}
}
