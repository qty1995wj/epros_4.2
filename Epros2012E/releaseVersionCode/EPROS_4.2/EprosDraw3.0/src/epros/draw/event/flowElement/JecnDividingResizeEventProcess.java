package epros.draw.event.flowElement;

import java.awt.Point;
import java.awt.event.MouseEvent;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.constant.JecnWorkflowConstant.ReSizeLineType;
import epros.draw.event.JecnTabbedEventProcess;
import epros.draw.gui.figure.unit.SnapToGrid;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.resize.JecnDividingLineResizePanel;
import epros.draw.gui.line.shape.CommentLine;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;

/**
 * 分割线拖动事件处理
 * 
 * @author ZHANGXH
 * @date： 日期：Jun 5, 2012 时间：3:04:38 PM
 */
public class JecnDividingResizeEventProcess {

	/** 拖拽时使用的原始位置 */
	private Point oldPosition = null;
	/** 是否拖动 boolean:True 拖动，false 未拖动 */
	private boolean isDraging = false;
	private Point startPos = null;
	private Point endPos = null;

	// 操作前数据
	private JecnUndoRedoData undoData = null;
	// 操作后数据
	private JecnUndoRedoData redoData = null;

	/**
	 * 鼠标点击事件处理
	 * 
	 * @param e
	 */
	public void mouseResizePressed(MouseEvent e) {
		if (e.getSource() == null || !(e.getSource() instanceof JecnDividingLineResizePanel)) {// 不存在实例或实例对象不为分割线显示点JecnDividingLineResizePanel返回
			return;
		}
		// 拖拽时使用的原始位置
		oldPosition = e.getPoint();
		// 操作前数据
		undoData = new JecnUndoRedoData();
		// 操作后数据
		redoData = new JecnUndoRedoData();

	}

	/**
	 * 
	 * 鼠标释放事件处理
	 * 
	 * @param e
	 */
	public void mouseResizeReleased(MouseEvent e) {

		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || !(e.getSource() instanceof JecnDividingLineResizePanel)) {// 不存在实例或实例对象不为分割线显示点JecnDividingLineResizePanel返回
			return;
		}
		// 是否点击Ctrl （是否时如果点击Ctrl）True:点击的是Ctrl;false:未点击Ctrl键
		boolean isCtrl = JecnWorkflowUtil.isPressCtril(e);

		JecnDividingLineResizePanel resizePanel = (JecnDividingLineResizePanel) e.getSource();
		if (isDraging && resizePanel.getReSizeLineType().equals(ReSizeLineType.center)) {
			// 拖动分割线中心点事件鼠标释放
			releasedCenterPoint(isCtrl);
			// 拖动是否执行边界处理 true:不执行边界处理 之拖动一条分割线时不执行边界处理
			JecnFlowElementEventProcess.isBound = false;

			desktopPane.updateUI();
		} else if (isDraging) {
			if (resizePanel.getBaseLinePanel() instanceof CommentLine) {
				CommentLine commentLine = (CommentLine) resizePanel.getBaseLinePanel();
				// 拖动状态为true
				commentLine.getCommentText().isMove();
				// 设置线段大小 记录线段原始状态下数值
				commentLine.getFlowElementData().getDiviLineCloneable().initOriginalArributs(startPos,
						commentLine.getEndPoint(), commentLine.getFlowElementData().getBodyWidth(),
						JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale());
			}
			// 记录操作后数据
			redoData.recodeFlowElement(resizePanel.getBaseLinePanel());
			// 记录这次操作的数据集合
			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
			desktopPane.getUndoRedoManager().addEdit(unredoProcess);
		}

		// 分割线
		if (isDraging && resizePanel.getBaseLinePanel() != null
				&& (resizePanel.getBaseLinePanel() instanceof DividingLine)) {
			// 目的：为了让面板重新设置大小
			resizePanel.getBaseLinePanel().setSize(resizePanel.getBaseLinePanel().getWidth(),
					resizePanel.getBaseLinePanel().getHeight());
		}
		isDraging = false;
	}

	/**
	 * 鼠标拖动事件处理
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseResizeDragged(MouseEvent e) {
		if (e.getSource() == null || !(e.getSource() instanceof JecnDividingLineResizePanel)) {// 不存在实例或实例对象不为分割线显示点JecnDividingLineResizePanel返回
			return;
		}
		// 当前画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// 获取鼠标水平方向位移
		int diffX = e.getX() - oldPosition.getLocation().x;
		// 获取鼠标垂直方向位移
		int diffY = e.getY() - oldPosition.getLocation().y;

		// 根据网格获取鼠标坐标点 拖动鼠标位移按照网格取值
		Point tempPoint = SnapToGrid.checkIsGrid(diffX, diffY);

		// 获取分割线显示点对象
		JecnDividingLineResizePanel lineResizePanel = (JecnDividingLineResizePanel) e.getSource();
		// 获取显示点对应的分割线对象
		JecnBaseDividingLinePanel curLine = lineResizePanel.getBaseLinePanel();

		// 根据拖动显示点位置重画分割线
		switch (lineResizePanel.getReSizeLineType()) {
		case start:
			draggedStartPoint(curLine, lineResizePanel, tempPoint);
			break;
		case end:
			draggedEndPoint(curLine, lineResizePanel, tempPoint);
			break;
		case center:
			// 记录拖动前位置点
			JecnFlowElementEventProcess.oldPoint = oldPosition;
			if (desktopPane.getCurrentSelectElement().size() == 1 && curLine instanceof DividingLine) {
				JecnFlowElementEventProcess.isBound = true;
			}
			// 拖动图形事件处理算法
			JecnFlowElementEventProcess.flowFigureDragged(e);
			// 是否执行拖动
			isDraging = JecnFlowElementEventProcess.isDragged;
			break;
		default:
			break;
		}

		desktopPane.updateUI();
	}

	/**
	 * 拖动分割线中心点事件鼠标释放
	 * 
	 * @param isCtrl
	 *            boolean （是否时如果点击Ctrl）True:点击的是Ctrl;false:未点击Ctrl键
	 */
	public static void releasedCenterPoint(boolean isCtrl) {
		// 获取画图面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 拖动图形鼠标释放后画图处理
		if (isCtrl) {
			if (workflow != null && workflow.getStartPoint() != null) {
				JecnFlowElementEventProcess.mouseReleaseCopyProcess(workflow.getStartPoint(), workflow
						.getCurrentSelectElement());
				// 删除拖动生成的虚拟图形
				JecnTabbedEventProcess.removeTempFigure();
				// 拖动状态恢复false
				JecnFlowElementEventProcess.isDragged = false;
			}
		} else {
			JecnFlowElementEventProcess.mouseReleaseDrawPanel(workflow.getCurrentSelectElement());
			// 删除拖动生成的虚拟图形
			JecnTabbedEventProcess.removeTempFigure();
			// 拖动状态恢复false
			JecnFlowElementEventProcess.isDragged = false;
		}

	}

	/**
	 * 拖动分割线开始点事件处理
	 * 
	 * @param curLine
	 *            JecnBaseDividingLinePanel拖动的分割线
	 * @param lineResizePanel
	 *            JecnDividingLineResizePanel拖动的分割线的显示点
	 * @param tempPoint
	 *            .x int 鼠标横向位移
	 * @param tempPoint
	 *            .y int 鼠标纵向位移
	 */
	private void draggedStartPoint(JecnBaseDividingLinePanel curLine, JecnDividingLineResizePanel lineResizePanel,
			Point tempPoint) {
		if (curLine == null) {
			return;
		}
		if (!isDraging && undoData != null) {
			isDraging = true;
			// 记录操作前数据
			undoData.recodeFlowElement(curLine);
		}

		if (curLine instanceof DividingLine || curLine instanceof CommentLine) {
			// 横竖分隔符拖动开始点
			startPos = draggedDiviStartPoint(curLine.getStartPoint(), tempPoint);
		} else {
			// 横竖分隔符拖动开始点
			startPos = draggedVHStartPoint(curLine.getStartPoint(), tempPoint, curLine.getDirectionEnum());
		}
		if (startPos == null) {
			return;
		}
		// 设置拖动点位置及分割线原始位置
		curLine.initOriginalArributs(startPos, curLine.getEndPoint(), curLine, lineResizePanel);

		// 分割线显示点
		curLine.showLineResizeHandles();
	}

	/**
	 * 拖动横竖分隔符开始点
	 * 
	 * @param curLine
	 * @param ePoint
	 */
	private Point draggedVHStartPoint(Point point, Point ePoint, LineDirectionEnum directionEnum) {
		if (point == null || ePoint == null || directionEnum == null) {
			return null;
		}
		Point tempPoint = null;
		switch (directionEnum) {
		case horizontal:
			if (point.getX() + ePoint.x <= 0) {// XY坐标都到达边界开始点坐标不能超出边界（边界X坐标为0，Y坐标为0）
				tempPoint = new Point(0, point.y);
			} else if (point.getX() + ePoint.x > 0) {// Y坐标到达边界开始点坐标不能超出边界（边界X坐标为0，Y坐标为0）
				tempPoint = new Point(point.x + ePoint.x, point.y);
			}
			break;
		case vertical:
			if (point.getY() + ePoint.y <= 0) {// XY坐标都到达边界开始点坐标不能超出边界（边界X坐标为0，Y坐标为0）
				tempPoint = new Point(point.x, 0);
			} else if (point.getY() + ePoint.y > 0) {
				tempPoint = new Point(point.x, point.y + ePoint.y);
			}
			break;
		default:
			break;
		}
		return tempPoint;
	}

	/**
	 * 
	 * 横竖分隔符拖动开始点
	 * 
	 * @param point
	 * @param ePoint
	 * @return
	 */
	private Point draggedDiviStartPoint(Point point, Point ePoint) {
		Point tempPoint = null;
		if (point.getX() + ePoint.x <= 0 && point.y + ePoint.y > 0) {// X坐标到达边界开始点坐标不能超出边界（边界X坐标为0，Y坐标为0）
			tempPoint = new Point(0, point.y + ePoint.y);
		} else if (point.getX() + ePoint.x > 0 && point.y + ePoint.y <= 0) {// Y坐标到达边界开始点坐标不能超出边界（边界X坐标为0，Y坐标为0）
			tempPoint = new Point(point.x + ePoint.x, 0);
		} else if (point.x + ePoint.x <= 0 && point.y + ePoint.y <= 0) {// XY坐标都到达边界开始点坐标不能超出边界（边界X坐标为0，Y坐标为0）
			tempPoint = new Point(0, 0);
		} else if (point.getX() + ePoint.x > 0 && point.y + ePoint.y > 0) {
			tempPoint = new Point(point.x + ePoint.x, point.y + ePoint.y);
		}
		return tempPoint;
	}

	/**
	 * 拖动分割线结束点事件处理
	 * 
	 * @param curLine
	 *            JecnBaseDividingLinePanel拖动的分割线
	 * @param lineResizePanel
	 *            JecnDividingLineResizePanel拖动的分割线的显示点
	 * @param tempPoint
	 *            .x int鼠标横向位移
	 * @param tempPoint
	 *            .y int鼠标纵向位移
	 */
	private void draggedEndPoint(JecnBaseDividingLinePanel curLine, JecnDividingLineResizePanel lineResizePanel,
			Point tempPoint) {
		if (!isDraging && undoData != null) {
			isDraging = true;
			// 记录操作前数据
			undoData.recodeFlowElement(curLine);
		}
		if (curLine instanceof DividingLine) {
			// 分隔符拖动开始点
			endPos = draggedDiviStartPoint(curLine.getEndPoint(), tempPoint);
		} else {
			// 横竖分隔符拖动开始点
			endPos = draggedVHStartPoint(curLine.getEndPoint(), tempPoint, curLine.getDirectionEnum());
		}
		if (endPos == null) {
			return;
		}
		// 设置拖动点位置及分割线原始位置
		curLine.initOriginalArributs(curLine.getStartPoint(), endPos, curLine, lineResizePanel);

		// 分割线显示点
		curLine.showLineResizeHandles();
	}
}
