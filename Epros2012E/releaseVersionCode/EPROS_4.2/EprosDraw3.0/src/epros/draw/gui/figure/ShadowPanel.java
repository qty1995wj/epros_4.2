package epros.draw.gui.figure;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public class ShadowPanel extends JPanel {
	private JecnFigureData figureData;

	public ShadowPanel(JecnFigureData figureData) {
		this.figureData = figureData;
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		RenderingHints originalRenderingHints = g2d.getRenderingHints();
		// 获取原始绘画颜色
		Color originalColor = g2d.getColor();

		// 指定RenderingHints
		g2d.setRenderingHints(JecnUIUtil.getRenderingHints());
		g2d.setPaint(figureData.getShadowColor());
		// 绘制图形
		paindShadow(g2d);
		// 还原原始RenderingHints对象
		g2d.setRenderingHints(originalRenderingHints);
		// 还原原始绘画颜色
		g2d.setColor(originalColor);

	}

	private void paindShadow(Graphics2D g2d) {
		switch (figureData.getMapElemType()) {
		case RoundRectWithLine:
			g2d.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), 20, 20);
			break;
		case DataImage:
		case ERPImage:
			paintDataImage(g2d);
			break;
		case Rhombus:
		case MapRhombus:
		case RiskPointFigure:// 风险点
		case ExpertRhombusAR:// 专家决策
			paintRhombus(g2d);
			break;
		case ITRhombus:
			paintITRhombus(g2d);
			break;
		case FromFigure:// 圆形
		case ToFigure:

		case AndFigure:
		case ORFigure:
		case XORFigure:
		case KCPFigure:// 关键活动
		case KCPFigureComp:
		case KSFFigure:
		case PAFigure:
		case Oval:// 流程地图：圆形
		case OvalSubFlowFigure:// 流程图：子流程 8 ht);
		case StartFigureAR:
		case EndFigureAR:
		case XORFigureAR:
		case ORFigureAR:
		case AndFigureAR:
		case ActivityOvalSubFlowFigure:// 接口（活动）
			g2d.fillOval(0, 0, this.getWidth(), this.getHeight());
			break;
		case EventFigure:// 事件
			paintEvent(g2d);
			break;

		case FlowFigureStart:// 流程开始
		case FlowFigureStop:// 流程结束
		case MapFigureEnd:
		case SystemDept:
			paintFlowFigure(g2d);
			break;

		case EndFigure:// 三角形
		case Triangle:// 三角形
		case FlowLevelFigureOne:
		case FlowLevelFigureTwo:
		case FlowLevelFigureThree:
		case FlowLevelFigureFour:
		case KeyPointFigure:// 关键控制点
			paintTriangle(g2d);
			break;
		case Rect:// 组织
		case PositionFigure:// 岗位
		case FunctionField:// 功能域
		case CustomFigure:// 客户
		case RoleFigure:// 角色
		case VirtualRoleFigure:// 虚拟角色

		case FreeText:// 自由文本框
		case IconFigure:// 图标插入框
		case DateFreeText:// 时间输入框
		case MapNameText:// 名称输入框
		case ModelFigure:// 泳池
		case DottedRect:
		case TermFigureAR:
		case RoleFigureAR:
			g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
			break;
		case ReverseArrowhead:// 风险箭头 返入
		case ReverseMapArrowhead:
			paintArrowHead(g2d);
			break;
		case RightArrowhead:
		case RightMapArrowhead:
			paintRightArrowHead(g2d);
			break;
		case RightHexagon:
		case InterfaceRightHexagon://接口六边形
			paintRightHexagon(g2d);
			break;
		case ImplFigure:// 接口
		case ActivityImplFigure:// 接口（活动）
			paintImplFigure(g2d);
			break;
		case Pentagon:// 五边形
		case SystemFigure:
		case StageSeparatorPentagon: // /流程图 阶段分隔符五边形
			paintPentagon(g2d);
			break;
		case OneArrowLine:
			paintOneArrowLine(g2d);
			break;
		case TwoArrowLine:
			paintTwoArrowLine(g2d);
			break;
		case IsoscelesTrapezoidFigure:
			paintTrapezoidFigure(g2d);
			break;
		case InputHandFigure:
			paintInputHandFigure(g2d);
			break;
		case FileImage:
			paintFileImage(g2d);
			break;
		case ActiveFigureAR:// 勘探院添加 元素开始
			g2d.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), 6, 6);
			break;
		case KCPFigureAR:
			paintTriangleAR(g2d);
			break;
		case TrialReport:
			paintTrialReport(g2d);
			break;
		default:
			break;
		}
	}

	/**
	 * 数据体
	 * 
	 * @param g2d
	 */
	private void paintDataImage(Graphics2D g2d) {
		// double sacle =
		// JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		// int ovalHeight = DrawCommon.convertDoubleToInt(20 * sacle);
		// int x1 = 0;
		// int y1 = 0 + DrawCommon.convertDoubleToInt(15 * sacle);
		//
		// int userHeight = this.getHeight() - DrawCommon.convertDoubleToInt(30
		// * sacle);
		// int userWidth = this.getWidth();
		// g2d.fillOval(x1, y1 - ovalHeight / 2 + userHeight, userWidth,
		// ovalHeight);

		// g2d.drawLine(x1, y1, x1, userHeight + ovalHeight / 2 +
		// DrawCommon.convertDoubleToInt(5 * sacle));
		// g2d.drawLine(userWidth, y1, userWidth, userHeight + ovalHeight / 2 +
		// DrawCommon.convertDoubleToInt(3 * sacle));

		// g2d.fillRect(x1, y1, userWidth, userHeight);
		// g2d.fillOval(x1, y1 - ovalHeight / 2, userWidth, ovalHeight);

		int x1 = 0;
		int y1 = 15;

		double sacle = 0;

		sacle = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		int ovalHeight = DrawCommon.convertDoubleToInt(20 * sacle);
		y1 = DrawCommon.convertDoubleToInt(15 * sacle);
		int userWidth = this.getWidth() - 1;
		int userHeight = this.getHeight() - DrawCommon.convertDoubleToInt(31 * sacle);

		// 顶层 原图，榜值均为零
		g2d.fillOval(x1, y1 - ovalHeight / 2 + userHeight, userWidth, ovalHeight);
		g2d.fillOval(x1, y1 - ovalHeight / 2, userWidth, ovalHeight);
		g2d.fillRect(x1, y1, userWidth, userHeight);
		// g2d.setPaint(flowElementData.getBodyColor());
		g2d.drawLine(x1, y1, x1, userHeight + ovalHeight / 2 + DrawCommon.convertDoubleToInt(5 * sacle));
		g2d.drawLine(userWidth, y1, userWidth, userHeight + ovalHeight / 2 + DrawCommon.convertDoubleToInt(3 * sacle));
		g2d.drawOval(x1, y1 - ovalHeight / 2, userWidth, ovalHeight);
	}

	/**
	 * 活动决策元素边框
	 * 
	 * @param g2
	 * @param this.getWidth()
	 * @param this.getHeight()
	 */
	private void paintRhombus(Graphics2D g2d) {
		int x1 = this.getWidth() / 2;
		int y1 = 0;
		int x2 = this.getWidth();
		int y2 = this.getHeight() / 2;
		int x3 = this.getWidth() / 2;
		int y3 = this.getHeight() - 1;
		int x4 = 0;
		int y4 = this.getHeight() / 2;
		// 圆弧矩形边框
		g2d.fillPolygon(new int[] { x1, x2, x3, x4 }, new int[] { y1, y2, y3, y4 }, 4);
	}

	private void paintITRhombus(Graphics2D g2d) {
		int x1 = this.getWidth() / 2;
		int y1 = 0;
		int x2 = this.getWidth();
		int y2 = this.getHeight() / 2;
		int x3 = this.getWidth() / 2;
		int y3 = this.getHeight() - 1;
		int x4 = 0;
		int y4 = this.getHeight() / 2;
		// 圆弧矩形边框
		g2d.fillPolygon(new int[] { x1, x2, x3, x4 }, new int[] { y1, y2, y3, y4 }, 4);
		// 圆弧矩形边框
		g2d.fillPolygon(new int[] { x1, x2 - 10, x3, x4 + 10 }, new int[] { y1 + 10, y2, y3 - 10, y4 }, 4);
	}

	/**
	 * 事件
	 * 
	 * @param g2d
	 */
	private void paintEvent(Graphics2D g2d) {
		int x1 = 0;
		int y1 = this.getHeight() / 2;
		int x2 = 20;
		int y2 = 0;
		int x3 = this.getWidth() - 20;
		int y3 = 0;
		int x4 = this.getWidth();
		int y4 = this.getHeight() / 2;
		int x5 = this.getWidth() - 20;
		int y5 = this.getHeight();
		int x6 = 20;
		int y6 = this.getHeight();

		g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6 }, new int[] { y1, y2, y3, y4, y5, y6 }, 6);
	}

	/**
	 * EPROS流程开始和流程结束
	 * 
	 * @param g2d
	 */
	private void paintFlowFigure(Graphics2D g2d) {
		int arcWidth;
		int arcHeight;
		if (this.getWidth() > this.getHeight()) {
			arcWidth = 3 * this.getWidth() / 5;
		} else {
			arcWidth = 3 * this.getHeight() / 5;
		}
		arcHeight = arcWidth;

		g2d.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), arcWidth, arcHeight);
	}

	public void paintTriangle(Graphics2D g2d) {
		int x1 = 0;
		int y1 = 0;
		int x2 = 0;
		int y2 = 0;
		int x3 = 0;
		int y3 = 0;
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = this.getWidth() / 2;
			y1 = 0;
			x2 = 0;
			y2 = this.getHeight();
			x3 = this.getWidth();
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = this.getHeight() / 2;
			x3 = 0;
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth() / 2;
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = this.getHeight() / 2;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
		}
		g2d.fillPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
	}

	/**
	 * 返工符 返入
	 * 
	 * @param g2d
	 * @param this.getWidth()
	 * @param this.getHeight()
	 * @param figureData
	 *            .getCurrentAngle() 旋转角度
	 */
	public void paintArrowHead(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = this.getHeight();
			x2 = this.getWidth() / 2;
			y2 = this.getHeight() / 2;
			x3 = this.getWidth();
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth() / 2;
			y2 = this.getHeight() / 2;
			x3 = 0;
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth() / 2;
			y3 = this.getHeight() / 2;
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = this.getWidth() / 2;
			y1 = this.getHeight() / 2;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
		}
		g2d.fillPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
		if (figureData.getCurrentAngle() == 90.0) {
			g2d.drawLine(this.getWidth() / 2, this.getHeight() / 2, this.getWidth(), this.getHeight() / 2);
		} else if (figureData.getCurrentAngle() == 180.0) {
			g2d.drawLine(this.getWidth() / 2, this.getHeight() / 2, this.getWidth() / 2, this.getHeight());
		} else if (figureData.getCurrentAngle() == 270.0) {
			g2d.drawLine(0, this.getHeight() / 2, this.getWidth() / 2, this.getHeight() / 2);
		} else if (figureData.getCurrentAngle() == 0.0) {
			g2d.drawLine(this.getWidth() / 2, 0, this.getWidth() / 2, this.getHeight() / 2);
		}
	}

	/**
	 * 返工符 返出
	 * 
	 * @param g2d
	 * @param this.getWidth()
	 * @param this.getHeight()
	 * @param figureData
	 *            .getCurrentAngle() 旋转角度
	 */
	public void paintRightArrowHead(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = this.getHeight() / 2;
			x2 = this.getWidth();
			y2 = this.getHeight() / 2;
			x3 = this.getWidth() / 2;
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = this.getHeight() / 2;
			x2 = this.getWidth() / 2;
			y2 = 0;
			x3 = this.getWidth() / 2;
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = this.getHeight() / 2;
			x2 = this.getWidth() / 2;
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight() / 2;
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = this.getWidth() / 2;
			y1 = 0;
			x2 = this.getWidth();
			y2 = this.getHeight() / 2;
			x3 = this.getWidth() / 2;
			y3 = this.getHeight();
		}
		g2d.fillPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
		if (figureData.getCurrentAngle() == 0.0) {
			g2d.drawLine(this.getWidth() / 2, 0, this.getWidth() / 2, this.getHeight() / 2);
		} else if (figureData.getCurrentAngle() == 90.0) {
			g2d.drawLine(this.getWidth() / 2, this.getHeight() / 2, this.getWidth(), this.getHeight() / 2);
		} else if (figureData.getCurrentAngle() == 180.0) {
			g2d.drawLine(this.getWidth() / 2, this.getHeight() / 2, this.getWidth() / 2, this.getHeight());
		} else if (figureData.getCurrentAngle() == 270.0) {
			g2d.drawLine(0, this.getHeight() / 2, this.getWidth() / 2, this.getHeight() / 2);
		}
	}

	/**
	 * 六边形
	 * 
	 * @param g2d
	 * @param 0
	 * @param 0
	 * @param this.getWidth()
	 * @param this.getHeight()
	 * @param figureData
	 *            .getCurrentAngle()
	 */
	public void paintRightHexagon(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int x6 = 0;
		int y6 = 0;
		int initVale = 20;
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		initVale = DrawCommon.convertDoubleToInt(initVale * scale);
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth() - initVale;
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight() / 2;
			x4 = this.getWidth() - initVale;
			y4 = this.getHeight();
			x5 = 0;
			y5 = this.getHeight();
			x6 = initVale;
			y6 = this.getHeight() / 2;

		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth() / 2;
			y2 = initVale;
			x3 = this.getWidth() - 1;
			y3 = 0;
			x4 = this.getWidth() - 1;
			y4 = this.getHeight() - initVale;
			x5 = this.getWidth() / 2;
			y5 = this.getHeight() - 1;
			x6 = 0;
			y6 = this.getHeight() - initVale;
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = initVale;
			y1 = 0;
			x2 = this.getWidth() - 1;
			y2 = 0;
			x3 = this.getWidth() - initVale;
			y3 = this.getHeight() / 2;
			x4 = this.getWidth() - 1;
			y4 = this.getHeight() - 1;
			x5 = initVale;
			y5 = this.getHeight() - 1;
			x6 = 0;
			y6 = this.getHeight() / 2;
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = initVale;
			x2 = this.getWidth() / 2;
			y2 = 0;
			x3 = this.getWidth();
			y3 = initVale;
			x4 = this.getWidth();
			y4 = this.getHeight();
			x5 = this.getWidth() / 2;
			y5 = this.getHeight() - initVale;
			x6 = 0;
			y6 = this.getHeight();
		}

		g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6 }, new int[] { y1, y2, y3, y4, y5, y6 }, 6);
	}

	/**
	 * 接口
	 * 
	 * @param g2d
	 * @param this.getWidth()
	 * @param this.getHeight()
	 */
	public void paintImplFigure(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int x6 = 0;
		int y6 = 0;
		x1 = this.getWidth() / 5;
		y1 = (this.getHeight() - this.getHeight() / 5) / 2 + this.getHeight() / 5;
		x2 = this.getWidth() / 5 + 20 - 10;
		y2 = this.getHeight() / 5;
		x3 = this.getWidth() - 20 + 10;
		y3 = this.getHeight() / 5;
		x4 = this.getWidth();
		y4 = (this.getHeight() - this.getHeight() / 5) / 2 + this.getHeight() / 5;
		x5 = this.getWidth() - 20 + 10;
		y5 = this.getHeight();
		x6 = this.getWidth() / 5 + 20 - 10;
		y6 = this.getHeight();

		g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6 }, new int[] { y1, y2, y3, y4, y5, y6 }, 6);
		g2d.fillRoundRect(0, 0, this.getWidth() * 7 / 10, this.getHeight() * 4 / 5, 15, 15);
	}

	/**
	 * 五边形
	 * 
	 * @param g2d
	 * @param this.getWidth()
	 * @param this.getHeight()
	 * @param figureData
	 *            .getCurrentAngle()
	 */
	public void paintPentagon(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int initVale = 20;
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		initVale = DrawCommon.convertDoubleToInt(initVale * scale);
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth() - initVale;
			y2 = 0;
			x3 = this.getWidth() - 1;
			y3 = this.getHeight() / 2;
			x4 = this.getWidth() - initVale;
			y4 = this.getHeight() - 1;
			x5 = 0;
			y5 = this.getHeight() - 1;
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth() - 1;
			y2 = 0;
			x3 = this.getWidth() - 1;
			y3 = this.getHeight() - initVale;
			x4 = this.getWidth() / 2;
			y4 = this.getHeight() - 1;
			x5 = 0;
			y5 = this.getHeight() - initVale;
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = this.getHeight() / 2;
			x2 = initVale;
			y2 = 0;
			x3 = this.getWidth() - 1;
			y3 = 0;
			x4 = this.getWidth() - 1;
			y4 = this.getHeight() - 1;
			x5 = initVale;
			y5 = this.getHeight() - 1;
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = this.getWidth() / 2;
			y1 = 0;
			x2 = this.getWidth() - 1;
			y2 = initVale;
			x3 = this.getWidth() - 1;
			y3 = this.getHeight() - 1;
			x4 = 0;
			y4 = this.getHeight() - 1;
			x5 = 0;
			y5 = initVale;
		}

		g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
	}

	/**
	 * 单向箭头
	 * 
	 * @param g2d
	 * @param this.getWidth()
	 * @param this.getHeight()
	 * @param figureData
	 *            .getCurrentAngle()
	 */
	public void paintOneArrowLine(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int x6 = 0;
		int y6 = 0;
		int x7 = 0;
		int y7 = 0;
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = this.getHeight() * 2 / 5;
			x2 = this.getWidth() * 4 / 5;
			y2 = this.getHeight() * 2 / 5;
			x3 = this.getWidth() * 4 / 5;
			y3 = this.getHeight() * 1 / 5;
			x4 = this.getWidth() - 1;
			y4 = this.getHeight() / 2;
			x5 = this.getWidth() * 4 / 5;
			y5 = this.getHeight() * 4 / 5;
			x6 = this.getWidth() * 4 / 5;
			y6 = this.getHeight() * 3 / 5;
			x7 = 0;
			y7 = this.getHeight() * 3 / 5;
		} else if (figureData.getCurrentAngle() == 45.0) {
			x1 = this.getWidth() * 16 / 50;
			y1 = this.getHeight() * 9 / 50;
			x2 = this.getWidth() * 41 / 50;
			y2 = this.getHeight() * 34 / 50;
			x3 = this.getWidth() * 19 / 20;
			y3 = this.getHeight() * 11 / 20;
			x4 = this.getWidth() * 9 / 10;
			y4 = this.getHeight() * 9 / 10;
			x5 = this.getWidth() * 11 / 20;
			y5 = this.getHeight() * 19 / 20;
			x6 = this.getWidth() * 34 / 50;
			y6 = this.getHeight() * 41 / 50;
			x7 = this.getWidth() * 9 / 50;
			y7 = this.getHeight() * 16 / 50;
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = this.getWidth() * 3 / 5;
			y1 = 0;
			x2 = this.getWidth() * 3 / 5;
			y2 = this.getHeight() * 4 / 5;
			x3 = this.getWidth() * 4 / 5;
			y3 = this.getHeight() * 4 / 5;
			x4 = this.getWidth() / 2;
			y4 = this.getHeight() - 1;
			x5 = this.getWidth() * 1 / 5;
			y5 = this.getHeight() * 4 / 5;
			x6 = this.getWidth() * 2 / 5;
			y6 = this.getHeight() * 4 / 5;
			x7 = this.getWidth() * 2 / 5;
			y7 = 0;
		} else if (figureData.getCurrentAngle() == 135.0) {
			x1 = this.getWidth() * 41 / 50;
			y1 = this.getHeight() * 16 / 50;
			x2 = this.getWidth() * 16 / 50;
			y2 = this.getHeight() * 41 / 50;
			x3 = this.getWidth() * 9 / 20;
			y3 = this.getHeight() * 19 / 20;
			x4 = this.getWidth() / 10;
			y4 = this.getHeight() * 9 / 10;
			x5 = this.getWidth() * 1 / 20;
			y5 = this.getHeight() * 11 / 20;
			x6 = this.getWidth() * 9 / 50;
			y6 = this.getHeight() * 34 / 50;
			x7 = this.getWidth() * 34 / 50;
			y7 = this.getHeight() * 9 / 50;
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = this.getHeight() / 2;
			x2 = this.getWidth() / 5;
			y2 = this.getHeight() / 5;
			x3 = this.getWidth() / 5;
			y3 = this.getHeight() * 2 / 5;
			x4 = this.getWidth();
			y4 = this.getHeight() * 2 / 5;
			x5 = this.getWidth();
			y5 = this.getHeight() * 3 / 5;
			x6 = this.getWidth() / 5;
			y6 = this.getHeight() * 3 / 5;
			x7 = this.getWidth() / 5;
			y7 = this.getHeight() * 4 / 5;

		} else if (figureData.getCurrentAngle() == 225.0) {
			x1 = this.getWidth() * 1 / 10;
			y1 = this.getHeight() * 1 / 10;
			x2 = this.getWidth() * 9 / 20;
			y2 = this.getHeight() * 1 / 20;
			x3 = this.getWidth() * 16 / 50;
			y3 = this.getHeight() * 9 / 50;
			x4 = this.getWidth() * 41 / 50;
			y4 = this.getHeight() * 34 / 50;
			x5 = this.getWidth() * 34 / 50;
			y5 = this.getHeight() * 41 / 50;
			x6 = this.getWidth() * 9 / 50;
			y6 = this.getHeight() * 16 / 50;
			x7 = this.getWidth() * 1 / 20;
			y7 = this.getHeight() * 9 / 20;
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = this.getWidth() / 2;
			y1 = 0;
			x2 = this.getWidth() * 4 / 5;
			y2 = this.getHeight() / 5;
			x3 = this.getWidth() * 3 / 5;
			y3 = this.getHeight() / 5;
			x4 = this.getWidth() * 3 / 5;
			y4 = this.getHeight() - 1;
			x5 = this.getWidth() * 2 / 5;
			y5 = this.getHeight() - 1;
			x6 = this.getWidth() * 2 / 5;
			y6 = this.getHeight() * 1 / 5;
			x7 = this.getWidth() / 5;
			y7 = this.getHeight() / 5;
		} else if (figureData.getCurrentAngle() == 315.0) {
			x1 = this.getWidth() * 9 / 10;
			y1 = this.getHeight() / 10;
			x2 = this.getWidth() * 19 / 20;
			y2 = this.getHeight() * 9 / 20;
			x3 = this.getWidth() * 41 / 50;
			y3 = this.getHeight() * 16 / 50;
			x4 = this.getWidth() * 16 / 50;
			y4 = this.getHeight() * 41 / 50;
			x5 = this.getWidth() * 9 / 50;
			y5 = this.getHeight() * 34 / 50;
			x6 = this.getWidth() * 34 / 50;
			y6 = this.getHeight() * 9 / 50;
			x7 = this.getWidth() * 11 / 20;
			y7 = this.getHeight() / 20;
		}
		g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7 }, new int[] { y1, y2, y3, y4, y5, y6, y7 }, 7);
	}

	/**
	 * 双向箭头
	 * 
	 * @param g2d
	 * @param this.getWidth()
	 * @param this.getHeight()
	 * @param figureData
	 *            .getCurrentAngle()
	 */
	public void paintTwoArrowLine(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		int x6 = 0;
		int y6 = 0;
		int x7 = 0;
		int y7 = 0;
		int x8 = 0;
		int y8 = 0;
		int x9 = 0;
		int y9 = 0;
		int x10 = 0;
		int y10 = 0;

		if (figureData.getCurrentAngle() == 0.0 || figureData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = this.getHeight() / 2;
			x2 = this.getWidth() / 5;
			y2 = this.getHeight() / 5;
			x3 = this.getWidth() / 5;
			y3 = this.getHeight() * 2 / 5;
			x4 = this.getWidth() * 4 / 5;
			y4 = this.getHeight() * 2 / 5;
			x5 = this.getWidth() * 4 / 5;
			y5 = this.getHeight() / 5;
			x6 = this.getWidth() - 1;
			y6 = this.getHeight() / 2;
			x7 = this.getWidth() * 4 / 5;
			y7 = this.getHeight() * 4 / 5;
			x8 = this.getWidth() * 4 / 5;
			y8 = this.getHeight() * 3 / 5;
			x9 = this.getWidth() / 5;
			y9 = this.getHeight() * 3 / 5;
			x10 = this.getWidth() / 5;
			y10 = this.getHeight() * 4 / 5;
		} else if (figureData.getCurrentAngle() == 45.0 || figureData.getCurrentAngle() == 225.0) {
			x1 = this.getWidth() * 1 / 10;
			y1 = this.getHeight() * 1 / 10;
			x2 = this.getWidth() * 9 / 20;
			y2 = this.getHeight() * 1 / 20;
			x3 = this.getWidth() * 16 / 50;
			y3 = this.getHeight() * 9 / 50;
			x4 = this.getWidth() * 41 / 50;
			y4 = this.getHeight() * 34 / 50;
			x5 = this.getWidth() * 19 / 20;
			y5 = this.getHeight() * 11 / 20;
			x6 = this.getWidth() * 9 / 10;
			y6 = this.getHeight() * 9 / 10;
			x7 = this.getWidth() * 11 / 20;
			y7 = this.getHeight() * 19 / 20;
			x8 = this.getWidth() * 34 / 50;
			y8 = this.getHeight() * 41 / 50;
			x9 = this.getWidth() * 8 / 50;
			y9 = this.getHeight() * 16 / 50;
			x10 = this.getWidth() * 1 / 20;
			y10 = this.getHeight() * 9 / 20;

		} else if (figureData.getCurrentAngle() == 90.0 || figureData.getCurrentAngle() == 270.0) {
			x1 = this.getWidth() / 2;
			y1 = 0;
			x2 = this.getWidth() * 4 / 5;
			y2 = this.getHeight() / 5;
			x3 = this.getWidth() * 3 / 5;
			y3 = this.getHeight() / 5;
			x4 = this.getWidth() * 3 / 5;
			y4 = this.getHeight() * 4 / 5;
			x5 = this.getWidth() * 4 / 5;
			y5 = this.getHeight() * 4 / 5;
			x6 = this.getWidth() / 2;
			y6 = this.getHeight() - 1;
			x7 = this.getWidth() / 5;
			y7 = this.getHeight() * 4 / 5;
			x8 = this.getWidth() * 2 / 5;
			y8 = this.getHeight() * 4 / 5;
			x9 = this.getWidth() * 2 / 5;
			y9 = this.getHeight() * 1 / 5;
			x10 = this.getWidth() / 5;
			y10 = this.getHeight() / 5;

		} else if (figureData.getCurrentAngle() == 135.0 || figureData.getCurrentAngle() == 315.0) {
			x1 = this.getWidth() * 9 / 10;
			y1 = this.getHeight() / 10;
			x2 = this.getWidth() * 19 / 20;
			y2 = this.getHeight() * 9 / 20;
			x3 = this.getWidth() * 41 / 50;
			y3 = this.getHeight() * 16 / 50;
			x4 = this.getWidth() * 16 / 50;
			y4 = this.getHeight() * 41 / 50;
			x5 = this.getWidth() * 9 / 20;
			y5 = this.getHeight() * 19 / 20;
			x6 = this.getWidth() / 10;
			y6 = this.getHeight() * 9 / 10;
			x7 = this.getWidth() * 1 / 20;
			y7 = this.getHeight() * 11 / 20;
			x8 = this.getWidth() * 9 / 50;
			y8 = this.getHeight() * 34 / 50;
			x9 = this.getWidth() * 34 / 50;
			y9 = this.getHeight() * 9 / 50;
			x10 = this.getWidth() * 11 / 20;
			y10 = this.getHeight() * 1 / 20;

		}
		g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 }, new int[] { y1, y2, y3, y4, y5, y6, y7,
				y8, y9, y10 }, 10);
	}

	/**
	 * 等腰梯形
	 * 
	 * @param g2d
	 * @param 0
	 * @param 0
	 * @param this.getWidth()
	 * @param this.getHeight()
	 * @param figureData
	 *            .getCurrentAngle()
	 */
	public void paintTrapezoidFigure(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int y4 = 0;
		if (figureData.getCurrentAngle() == 0) {// 默认零度
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth() - 25;
			y3 = this.getHeight();
			x4 = 25;
			y4 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 25;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
			x4 = 0;
			y4 = this.getHeight() - 25;
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = 25;
			y1 = 0;
			x2 = this.getWidth() - 25;
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
			x4 = 0;
			y4 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 25;
			x3 = this.getWidth();
			y3 = this.getHeight() - 25;
			x4 = 0;
			y4 = this.getHeight();
		}
		g2d.fillPolygon(new int[] { x1, x2, x3, x4 }, new int[] { y1, y2, y3, y4 }, 4);
	}

	/**
	 * 手动输入
	 * 
	 * @param g2d
	 * @param 0
	 * @param 0
	 * @param this.getWidth()
	 * @param this.getHeight()
	 * @param figureData
	 *            .getCurrentAngle()
	 */
	public void paintInputHandFigure(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int y4 = 0;
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = this.getHeight() / 3;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
			x4 = 0;
			y4 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth() * 2 / 3;
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
			x4 = 0;
			y4 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight() * 2 / 3;
			x4 = 0;
			y4 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
			x4 = this.getWidth() / 3;
			y4 = this.getHeight();
		}

		g2d.fillPolygon(new int[] { x1, x2, x3, x4 }, new int[] { y1, y2, y3, y4 }, 4);
	}

	/**
	 * 文档
	 * 
	 * @param g2d
	 * @param 0
	 * @param 0
	 * @param this.getWidth()
	 * @param this.getHeight()
	 * @param figureData
	 *            .getCurrentAngle()
	 */
	public void paintFileImage(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int y4 = 0;
		int y5 = 0;
		int y6 = 0;
		/** Double 的最大可能值 */
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		List<Integer> xList = new ArrayList<Integer>();
		List<Integer> yList = new ArrayList<Integer>();
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = this.getWidth();
			y1 = 0;

			x2 = 0;
			y2 = y1;

			x3 = x2;
			y3 = 7 * this.getHeight() / 8;

			x4 = this.getWidth() / 4;
			y4 = this.getHeight();

			x5 = 3 * this.getWidth() / 4;
			y5 = 3 * this.getHeight() / 4;

			x6 = this.getWidth();
			y6 = 7 * this.getHeight() / 8;
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = this.getWidth();
			y1 = 0;

			x2 = this.getWidth() / 8;
			y2 = y1;

			x3 = 0;
			y3 = this.getHeight() / 4;

			x4 = this.getWidth() / 4;
			y4 = 3 * this.getHeight() / 4;

			x5 = this.getWidth() / 8;
			y5 = this.getHeight();

			x6 = this.getWidth();
			y6 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = this.getWidth();
			y1 = this.getHeight() / 8;

			x2 = 3 * this.getWidth() / 4;
			y2 = 0;

			x3 = this.getWidth() / 4;
			y3 = this.getHeight() / 4;

			x4 = 0;
			y4 = this.getHeight() / 8;

			x5 = 0;
			y5 = this.getHeight();

			x6 = this.getWidth();
			y6 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = 7 * this.getWidth() / 8;
			y1 = 0;

			x2 = 0;
			y2 = y1;

			x3 = x2;
			y3 = this.getHeight();

			x4 = 7 * this.getWidth() / 8;
			y4 = this.getHeight();

			x5 = this.getWidth();
			y5 = 6 * this.getHeight() / 8;

			x6 = 6 * this.getWidth() / 8;
			y6 = 1 * this.getHeight() / 4;
		}

		if (figureData.getCurrentAngle() == 0.0) {
			double[] X = { x3, x4, x5, x6 };
			double[] Y = { y3, y4, y5, y6 };
			for (int i = 0; i < X.length; i++) {
				if (X[i] > max) {
					max = X[i];
				}
				if (X[i] < min) {
					min = X[i];
				}
			}
			for (double x = min; x <= max; x = x + 0.01) {
				xList.add((int) x);
				yList.add((int) lagrange(X, Y, x));
			}

			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[0] = x1;
			yy[0] = y1;
			xx[1] = x2;
			yy[1] = y2;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}

			g2d.fillPolygon(xx, yy, xx.length);
		} else if (figureData.getCurrentAngle() == 90.0) {
			double[] X = { x5, x4, x3, x2 };
			double[] Y = { y5, y4, y3, y2 };
			for (int i = 0; i < Y.length; i++) {
				if (Y[i] > max) {
					max = Y[i];
				}
				if (Y[i] < min) {
					min = Y[i];
				}
			}
			for (double y = min; y <= max; y = y + 0.01) {
				xList.add((int) lagrange(Y, X, y));
				yList.add((int) y);
			}

			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[0] = x1;
			yy[0] = y1;
			xx[yList.size() + 1] = x6;
			yy[yList.size() + 1] = y6;

			for (int i = 0; i < xList.size(); i++) {
				xx[i + 1] = xList.get(i);
				yy[i + 1] = yList.get(i);
			}

			g2d.fillPolygon(xx, yy, xx.length);

		} else if (figureData.getCurrentAngle() == 180.0) {

			double[] X = { x4, x3, x2, x1 };
			double[] Y = { y4, y3, y2, y1 };
			for (int i = 0; i < X.length; i++) {
				if (X[i] > max) {
					max = X[i];
				}
				if (X[i] < min) {
					min = X[i];
				}
			}
			for (double x = min; x <= max; x = x + 0.01) {
				xList.add((int) x);
				yList.add((int) lagrange(X, Y, x));
			}

			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[xList.size()] = x6;
			yy[xList.size()] = y6;
			xx[xList.size() + 1] = x5;
			yy[xList.size() + 1] = y5;

			for (int i = 0; i < xList.size(); i++) {
				xx[i] = xList.get(i);
				yy[i] = yList.get(i);
			}
			g2d.fillPolygon(xx, yy, xx.length);

		} else if (figureData.getCurrentAngle() == 270.0) {
			double[] X = { x1, x6, x5, x4 };
			double[] Y = { y1, y6, y5, y4 };
			min = Double.MAX_VALUE;
			max = Double.MIN_VALUE;
			for (int i = 0; i < Y.length; i++) {
				if (Y[i] > max) {
					max = Y[i];
				}
				if (Y[i] < min) {
					min = Y[i];
				}
			}
			for (double y = min; y <= max; y = y + 0.01) {
				xList.add((int) lagrange(Y, X, y));
				yList.add((int) y);
			}
			int[] xx = new int[xList.size() + 2];
			int[] yy = new int[yList.size() + 2];

			xx[0] = x3;
			yy[0] = y3;
			xx[1] = x2;
			yy[1] = y2;
			for (int i = 0; i < xList.size(); i++) {
				xx[i + 2] = xList.get(i);
				yy[i + 2] = yList.get(i);
			}

			g2d.fillPolygon(xx, yy, xx.length);
		}
	}

	/**
	 * 平滑曲线,多个点的变化
	 * 
	 * @param x
	 *            横向坐标集合
	 * @param y
	 *            纵向坐标集合
	 * @param value
	 * @return
	 */
	public double lagrange(double x[], double y[], double value) {
		double sum = 0;
		double L;
		for (int i = 0; i < x.length; i++) {
			L = 1;
			for (int j = 0; j < x.length; j++) {
				if (j != i) {
					L = L * (value - x[j]) / (x[i] - x[j]);
				}
			}
			sum = sum + L * y[i];
		}
		return sum;
	}

	/**
	 * 
	 * paintTriangleAR:KCP
	 * 
	 * @param g2d
	 * @param this.getWidth()
	 * @param this.getHeight()
	 */
	public void paintTriangleAR(Graphics2D g2d) {
		int x1 = 0;
		int y1 = 0;
		int x2 = 0;
		int y2 = 0;
		int x3 = 0;
		int y3 = 0;
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth() / 2;
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = this.getHeight() / 2;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = this.getWidth() / 2;
			y1 = 0;
			x2 = 0;
			y2 = this.getHeight();
			x3 = this.getWidth();
			y3 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = this.getHeight() / 2;
			x3 = 0;
			y3 = this.getHeight();
		}
		g2d.fillPolygon(new int[] { x1, x2, x3 }, new int[] { y1, y2, y3 }, 3);
	}

	public void paintTrialReport(Graphics2D g2d) {
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int y1 = 0;
		int y2 = 0;
		int y3 = 0;
		int x4 = 0;
		int y4 = 0;
		int x5 = 0;
		int y5 = 0;
		if (figureData.getCurrentAngle() == 0.0) {
			x1 = this.getWidth() / 5;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
			x4 = 0;
			y4 = this.getHeight();
			x5 = 0;
			y5 = this.getHeight() / 5;
		} else if (figureData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth() * 4 / 5;
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight() / 5;
			x4 = this.getWidth();
			y4 = this.getHeight();
			x5 = 0;
			y5 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight() * 4 / 5;
			x4 = this.getWidth() * 4 / 5;
			y4 = this.getHeight();
			x5 = 0;
			y5 = this.getHeight();
		} else if (figureData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = this.getWidth();
			y2 = 0;
			x3 = this.getWidth();
			y3 = this.getHeight();
			x4 = this.getWidth() / 5;
			y4 = this.getHeight();
			x5 = 0;
			y5 = this.getHeight() * 4 / 5;
		}

		g2d.fillPolygon(new int[] { x1, x2, x3, x4, x5 }, new int[] { y1, y2, y3, y4, y5 }, 5);
	}
}
