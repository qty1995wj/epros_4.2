package epros.designer.gui.popedom.role.choose;

import java.awt.event.MouseEvent;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class RoleManagePersonChooseTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(RoleManagePersonChooseTree.class);

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RoleManagePersonChooseTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.organizationRoot, JecnProperties
				.getValue("dept"));
		try {
			List<JecnTreeBean> list = ConnectionPool.getOrganizationAction().getRoleAuthChildPersons(0L,
					JecnConstants.projectId, JecnDesignerCommon.getSecondAdminUserId());
			JecnTreeCommon.addNLevelNodes(list, rootNode);
		} catch (Exception e) {
			log.error("RoleManagePersonChooseTree getTreeModel is error！", e);
		}
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {

	}

}
