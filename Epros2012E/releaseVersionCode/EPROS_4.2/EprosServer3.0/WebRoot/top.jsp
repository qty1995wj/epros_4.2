<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="css/top.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${basePath}js/top.js">
</script>
	</head>

	<body>
		<div><%@include file="compatibleTip.jsp" %></div>
		<div id="logo" style="min-width: 1024px">
			<div style="float: left">
				<s:if test="#application.versionType">
					<img src="images/top/logo.jpg" />
				</s:if>
				<s:else>
					<img src="images/top/logo-eps.jpg" />
				</s:else>
			</div>
			<div
				style="float: right; height: 55px; width: 550px; background-image: url('images/top/logoBack.jpg')">
				<div id="menuProject" style="padding-top: 5px;">
					<table align='right' style="table-layout: fixed; width: 485px">
						<tr>
							<s:if test='#session.webLoginBean.showProject==1'>
								<td class="top_projectName wordHidden"
									title="${webLoginBean.projectName}">
									<a href="javascript:;" onclick="tabProject();"><span>${webLoginBean.projectName}</span>
									</a>
								</td>
							</s:if>
							<s:else>
								<td></td>
							</s:else>
							<td class="top_loginName wordHidden"
								title="${webLoginBean.jecnUser.trueName}">
								<img src="images/top/topUser.png" style="padding-bottom: 3px;" />
								${webLoginBean.jecnUser.trueName}
							</td>
							<td style="width: 80px">
								<img src="images/top/information.gif"
									style="padding-bottom: 3px;" />
								<a class="a-black-none" onclick="swicthMessage()"> <!-- 消息 -->
									<s:text name="news" /> </a>
								<span id="messageNum">(${webLoginBean.messageNum})</span>
							</td>
							<td style="width: 60px">
								<img src="images/about1.jpg" style="padding-bottom: 3px;" />
								<a class="a-black-none" onclick="openAboutWindow()"> <!-- 关于 -->
									<s:text name="about" /> </a>

							</td>
							<td style="width: 60px">
								<img src="images/top/package.gif" style="padding-bottom: 3px;" />
								<a class="a-black-none" href="javascript:downloadJre()"> <!-- 插件 -->
									<s:text name="plugin" /> </a>
							</td>

							<td style="width: 60px">
								<img src="images/top/exit.gif" style="padding-bottom: 3px;" />
								<a class="a-black-none" onclick="exit()"
									style="cursor: pointer;"> <!-- 退出 --> <s:text name="exit" />
								</a>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>



		<div id="vdividermenu">
			<div style="width: 1024px;">
				<table>
					<tr id="ulTitle">
						<td id="titlemyHomepage"
							onclick="nTabs('myHomepage',this);swicthMyhome();">
							<%--我的主页--%>
							<s:text name="guide.MyFlow" />
						</td>
						<td id="titleprocessSystem"
							onclick="nTabs('processSystem',this);swicthProcessSys()">
							<%--流程体系--%>
							<s:text name="guide.ProcessPanorama" />
						</td>
						
						<!--[if IE]>
						<!--[if gte IE 9]>
						<td id="searchEverything"
											onclick="nTabs('searchEverything',this);switchSearchEverything()">
											<%--资源检索--%>
											<s:text name="searchResource" />
						</td> 
						<![endif]-->
						 
						<!--[if !IE]><!-->
						<td id="searchEverything"
											onclick="nTabs('searchEverything',this);switchSearchEverything()">
											<%--资源检索--%>
											<s:text name="searchResource" />
						        </td>
						 <!--<![endif]-->
						
						
						<s:if test="#application.isHiddenSystem==0">
							<td id="titleruleSystem"
								onclick="nTabs('ruleSystem',this);swicthRuleSystem();">
								<%--制度体系--%>
								<s:text name="guide.RuleSystem" />
							</td>
						</td>
						</s:if>
						<s:if test="#application.isHiddenStandard==0">
							<td id="titlestandardSystem"
								onclick="nTabs('standardSystem',this);swicthStandardSys()">
								<%--标准体系--%>
								<s:text name="guide.Standard" />
							</td>
						</td>
						</s:if>
						<s:if test="#application.isHiddenRisk==0">
							<td id="titleRiskSystem"
								onclick="nTabs('riskSystem',this);swicthRiskSystem()">
								<!-- 风险体系 -->
								${riskSys}
							</td>
						</s:if>	

						<td id="titlefileSystem"
							onclick="nTabs('fileSystem',this);swicthFileSystem()">
							<%--知识库--%>
							<s:text name="guide.Repository" />
						</td>
						<td id="titleorganizationAndPost"
							onclick="nTabs('organizationAndPost',this);swicthOrgPosition();">
							<%--组织与岗位--%>
							<s:text name="guide.OrganizationAndPosition" />
						</td>
						<td id="titletaskManagement"
							onclick="nTabs('taskManagement',this);taskManagement()">
							<%--任务管理--%>
							<s:text name="taskManagement" />
						</td>
						<td id="titlerationalizationProposal"
							onclick="nTabs('rationalizationProposal',this);navigationRtnlPropose()">
							<%--合理化建议--%>
							<s:text name="rationalizationproposals" />
						</td>
						<s:if
							test="(#session.webLoginBean.isAdmin||#session.webLoginBean.isViewAdmin)&&#application.versionType">
							<td id="titlestatements"
								onclick="nTabs('statements',this);swicthReportsSystem()">
								<%--报表--%>
								<s:text name="theReport" />
							</td>
						</s:if>
						<s:if test="#session.webLoginBean.isAdmin">
							<td id="titlesystemManagement"
								onclick="nTabs('systemManagement',this);swicthSysManageSystem()">
								<%--系统管理--%>
								<s:text name="systemManagement" />
							</td>
						</s:if>

					</tr>
				</table>
			</div>
			</div>
	</body>
</html>