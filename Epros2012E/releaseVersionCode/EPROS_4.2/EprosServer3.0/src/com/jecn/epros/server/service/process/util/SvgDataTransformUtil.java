package com.jecn.epros.server.service.process.util;

import java.sql.Clob;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;

import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnLineSegmentDep;
import com.jecn.epros.server.bean.process.JecnLineSegment;
import com.jecn.epros.server.bean.process.JecnLineSegmentT;
import com.jecn.svg.dataModel.epros.SvgEprosGraphData;
import com.jecn.svg.dataModel.epros.SvgEprosLineData;

public enum SvgDataTransformUtil {
	INSTANCE;

	public List<SvgEprosGraphData> getGraphDataLists(List<Object[]> objList, Long flowId) {
		List<SvgEprosGraphData> list = new ArrayList<SvgEprosGraphData>();
		for (Object[] imageT : objList) {
			if ("MapLineFigure".equals(imageT[1].toString())) {
				continue;
			}
			list.add(getSvgEprosGraphData(imageT, flowId));
		}
		return list;
	}

	public List<SvgEprosGraphData> getGraphDataListsOrg(List<JecnFlowOrgImage> images) {
		List<SvgEprosGraphData> list = new ArrayList<SvgEprosGraphData>();
		for (JecnFlowOrgImage imageT : images) {
			if (imageT.getPoLongx() == null || imageT.getPoLongy() == null) {
				continue;
			}
			list.add(getSvgEprosGraphDataOrg(imageT));
		}
		return list;
	}

	public List<SvgEprosLineData> getLineDatasT(List<JecnLineSegmentT> listLineSegmet) {
		List<SvgEprosLineData> lineDatas = new ArrayList<SvgEprosLineData>();
		for (JecnLineSegmentT lineSegmentT : listLineSegmet) {
			lineDatas.add(getLineDataT(lineSegmentT));
		}
		return lineDatas;
	}

	public List<SvgEprosLineData> getLineDatasOrg(List<JecnLineSegmentDep> listLineSegmetDep) {
		List<SvgEprosLineData> lineDatas = new ArrayList<SvgEprosLineData>();
		for (JecnLineSegmentDep lineSegmentT : listLineSegmetDep) {
			lineDatas.add(getLineDataDept(lineSegmentT));
		}
		return lineDatas;
	}

	private SvgEprosLineData getLineDataDept(JecnLineSegmentDep lineSegmentT) {
		SvgEprosLineData lineData = new SvgEprosLineData();
		lineData.setId(lineData.getId());
		lineData.setFigureId(lineSegmentT.getFigureId());
		lineData.setStartX(lineSegmentT.getStartX());
		lineData.setStartY(lineSegmentT.getStartY());
		lineData.setEndX(lineSegmentT.getEndX());
		lineData.setEndY(lineSegmentT.getEndY());
		return lineData;
	}

	public List<SvgEprosLineData> getLineDatas(List<JecnLineSegment> listLineSegmet) {
		List<SvgEprosLineData> lineDatas = new ArrayList<SvgEprosLineData>();
		for (JecnLineSegment lineSegment : listLineSegmet) {
			lineDatas.add(getLineData(lineSegment));
		}
		return lineDatas;
	}

	private SvgEprosGraphData getSvgEprosGraphDataOrg(JecnFlowOrgImage flowOrgImage) {
		SvgEprosGraphData graphData = new SvgEprosGraphData();
		graphData.setFigureId(flowOrgImage.getFigureId());
		graphData.setFigureType(flowOrgImage.getFigureType());
		graphData.setFigureText(flowOrgImage.getFigureText());
		graphData.setStartFigure(flowOrgImage.getStartFigure());
		graphData.setEndFigure(flowOrgImage.getEndFigure());

		graphData.setPoLongx(flowOrgImage.getPoLongx());
		graphData.setPoLongy(flowOrgImage.getPoLongy());
		graphData.setWidth(flowOrgImage.getWidth());
		graphData.setHeight(flowOrgImage.getHeight());
		graphData.setFontSize(flowOrgImage.getFontSize());

		graphData.setBGColor(flowOrgImage.getBGColor());
		graphData.setFontColor(flowOrgImage.getFontColor());
		graphData.setLineColor(flowOrgImage.getLineColor());

		graphData.setFontPosition(flowOrgImage.getFontPosition());
		graphData.setHavaShadow(flowOrgImage.getHavaShadow());
		graphData.setCircumgyrate(flowOrgImage.getCircumgyrate());
		graphData.setBodyColor(flowOrgImage.getBodyColor());
		graphData.setBodyLine(flowOrgImage.getBodyLine());
		graphData.setIsErect(flowOrgImage.getIsErect());

		graphData.setFigureImageId(flowOrgImage.getFigureImageId());
		if (flowOrgImage.getOrderIndex() == null) {
			graphData.setOrderIndex(-1L);
		} else {
			graphData.setOrderIndex(flowOrgImage.getOrderIndex());
		}
		graphData.setLinkFlowId(flowOrgImage.getLinkOrgId());

		graphData.setFontBody(flowOrgImage.getFontBody());
		graphData.setFrameBody(flowOrgImage.getFrameBody());
		if (StringUtils.isEmpty(flowOrgImage.getFontType())) {
			graphData.setFontType("宋体");
		} else {
			graphData.setFontType(flowOrgImage.getFontType());
		}
		graphData.setIs3DEffect(flowOrgImage.getIs3DEffect());
		graphData.setFillEffects(flowOrgImage.getFillEffects());
		if ("ManhattanLine".equals(flowOrgImage.getFigureType()) || "CommonLine".equals(flowOrgImage.getFigureType())) {
			graphData.setLineThickness(flowOrgImage.getLineThickness());
		} else {
			if (flowOrgImage.getFrameBody() != null) {
				graphData.setLineThickness(flowOrgImage.getFrameBody().intValue());
			} else {
				graphData.setLineThickness(1);
			}
		}
		graphData.setStartFigureUUID(flowOrgImage.getStartFigureUUID());
		graphData.setEndFigureUUID(flowOrgImage.getEndFigureUUID());
		graphData.setShadowColor(flowOrgImage.getShadowColor());

		return graphData;
	}

	private SvgEprosGraphData getSvgEprosGraphData(Object[] objArr, Long flowId) {
		SvgEprosGraphData graphData = new SvgEprosGraphData();

		// 图形ID
		Long figureId = valueLong(objArr[0]);
		// 图形类型
		String figureType = valueOf(objArr[1]);
		// 元素名称
		String figureText = valueOf(objArr[2]);
		// START_FIGURE 注释框小线段开始点X
		Long startFigure = valueLong(objArr[3]);
		// END_FIGURE 注释框小线段开始点Y
		Long endFigure = valueLong(objArr[4]);
		// X_POINT 图像元素开始点的X / 连接线对应的开始编辑点方向类型 / 横线、竖线以及分割线结束点X坐标
		Long xPoint = valueLong(objArr[5]);
		// Y_POINT 图像元素开始点的Y/连接线对应的结束编辑点方向类型 / 横线、竖线以及分割线结束点Y坐标
		Long yPoint = valueLong(objArr[6]);
		// 图像元素的宽
		Long width = valueLong(objArr[7]);
		// 图像元素的高
		Long height = valueLong(objArr[8]);
		// 字体大小
		Long fontSize = valueLong(objArr[9]);
		// 字体加粗
		Long fontBody = valueLong(objArr[10]);
		// 字体类型
		String fontType = valueOf(objArr[11]);
		// 图像元素的填充色
		String bgColor = valueOf(objArr[12]);
		// 字体颜色
		String fontColor = valueOf(objArr[13]);
		// LINK_FLOW_ID 连接流程Id或者制度ID
		Long linkFlowId = valueLong(objArr[14]);
		// 此字段三个意思
		// 线的颜色；当为活动元素时：1为PA,2为KSF,3为KCP；当为接口元素时
		String lineColor = valueOf(objArr[15]);
		// 字体位置
		String fontPosition = valueOf(objArr[16]);
		// 是否有阴影
		Long haveShadow = valueLong(objArr[17]);
		// 元素旋转角度
		String circumgyrate = valueOf(objArr[18]);
		// 边框的样式 线的样式
		String bodyLine = valueOf(objArr[19]);
		// 边框颜色
		String bodyColor = valueOf(objArr[20]);
		// 层级
		Long zOrderIndex = valueLong(objArr[21]);

		// ACTIVITY_SHOW 活动说明或者图片的临时名称
		String activityShow = valueStringByClob(objArr[22]);

		// ROLE_RES 角色职责或者关键活动的控制点
		String roleRes = valueOf(objArr[23]);
		// ACTIVITY_ID 活动编号/组织岗位
		String activityID = valueOf(objArr[24]);

		// 是否有3D效果
		String is3D = valueOf(objArr[25]);
		// 填充效果(填充类型)
		String fillEffects = valueOf(objArr[26]);
		// 边框宽度
		String lineThickness = valueOf(objArr[27]);
		// 阴影颜色
		String shadowColor = valueOf(objArr[28]);
		// 关联[LINK_FLOW_ID]ID对应流程图/流程地图类型；如果是制度对应的值为空
		String linkIsFlow = valueOf(objArr[29]);

		int isOnLine = intOf(objArr[31]);

		graphData.setFigureText(figureText);

		boolean isFlowMap = objArr.length < 37;

		String eleShape = null;
		if (isFlowMap) {
			if (objArr.length > 33) {
				eleShape = valueOf(objArr[33]);
			}
		} else {
			eleShape = valueOf(objArr[39]);
		}
		if ("FreeText".equals(eleShape)) {
			graphData.setFigureText(activityShow);
		} else {
			graphData.setFigureText(figureText);
		}
		// 2017/4/6 新增术语定义
		if (!isFlowMap && objArr.length >= 38) {
			// 流程 ，时间轴
			String targetValue = valueOf(objArr[32]);
			String statusValue = valueOf(objArr[33]);

			graphData.setTargetValue(targetValue);
			graphData.setStatusValue(statusValue);

			String implType = valueOf(objArr[34]);
			graphData.setImplType(StringUtils.isBlank(implType) ? 0 : Integer.valueOf(implType));

			// 术语
			graphData.setTermDefine(valueOf(objArr[35]));

			// 活动输入说明和输出说明
			graphData.setActivityInput(valueOf(objArr[36]));
			graphData.setActivityOutput(valueOf(objArr[37]));
			// 38 活动担当
			graphData.setCustomOne(valueOf(objArr[38]));
		}

		//
		// // FIGURE_ID 图形主键
		// figureData.setFlowElementId(Long.valueOf(figureId).longValue());
		graphData.setFlowId(flowId);
		graphData.setFigureId(figureId);
		graphData.setFigureType(figureType);

		graphData.setStartFigure(startFigure);
		graphData.setEndFigure(endFigure);

		graphData.setPoLongx(xPoint);
		graphData.setPoLongy(yPoint);
		graphData.setWidth(width);
		graphData.setHeight(height);
		graphData.setFontSize(fontSize);

		graphData.setBGColor(bgColor);
		graphData.setFontColor(fontColor);
		graphData.setLinkFlowId(linkFlowId);
		graphData.setLineColor(lineColor);

		if (StringUtils.isEmpty(fontPosition)) {
			graphData.setFontPosition("0");
		} else {
			graphData.setFontPosition(fontPosition);
		}
		graphData.setHavaShadow(haveShadow);
		graphData.setCircumgyrate(circumgyrate);
		graphData.setBodyColor(bodyColor);
		graphData.setBodyLine(bodyLine);
		// graphData.setIsErect(structureImageT.getIsErect());

		// graphData.setFigureImageId(structureImageT.getFigureImageId());
		graphData.setOrderIndex(zOrderIndex);
		graphData.setActivityId(activityID);
		graphData.setActivityShow(activityShow);

		graphData.setFontBody(fontBody);
		graphData.setFontType(fontType);
		graphData.setRoleRes(roleRes);
		if (StringUtils.isNotEmpty(is3D)) {
			graphData.setIs3DEffect(Integer.valueOf(is3D));
		}
		if (StringUtils.isNotEmpty(fillEffects)) {
			graphData.setFillEffects(Integer.valueOf(fillEffects));
		}
		if (StringUtils.isNotEmpty(lineThickness)) {
			graphData.setLineThickness(Integer.valueOf(lineThickness));
		}

		graphData.setShadowColor(shadowColor);

		graphData.setLinkIsFlow(linkIsFlow);
		graphData.setIsOnLine(isOnLine);

		if (isFlowMap) {// 流程架构
			// 此只针对于流程地图 泳池分割线X坐标
			String dividingX = valueOf(objArr[32]);
			graphData.setDividingX(Integer.valueOf(dividingX));
		}
		return graphData;
	}

	private SvgEprosLineData getLineDataT(JecnLineSegmentT lineSegmentT) {
		SvgEprosLineData lineData = new SvgEprosLineData();
		lineData.setId(lineData.getId());
		lineData.setFigureId(lineSegmentT.getFigureId());
		lineData.setStartX(lineSegmentT.getStartX());
		lineData.setStartY(lineSegmentT.getStartY());
		lineData.setEndX(lineSegmentT.getEndX());
		lineData.setEndY(lineSegmentT.getEndY());
		return lineData;
	}

	private SvgEprosLineData getLineData(JecnLineSegment lineSegment) {
		SvgEprosLineData lineData = new SvgEprosLineData();
		lineData.setId(lineData.getId());
		lineData.setFigureId(lineSegment.getFigureId());
		lineData.setStartX(lineSegment.getStartX());
		lineData.setStartY(lineSegment.getStartY());
		lineData.setEndX(lineSegment.getEndX());
		lineData.setEndY(lineSegment.getEndY());
		return lineData;
	}

	/**
	 * 
	 * 对象转换成字符串,如果为空返回“”
	 * 
	 * @param obj
	 *            Object
	 * @return String 字符串值或“”
	 */
	private String valueOf(Object obj) {
		return (obj == null) ? "" : obj.toString();
	}

	private int intOf(Object obj) {
		return (obj == null) ? 0 : Integer.valueOf(obj.toString());
	}

	/**
	 * 
	 * 对象转换成long型,如果为空返回“”
	 * 
	 * @param obj
	 *            Object
	 * @return Long 或NULL
	 */
	private Long valueLong(Object obj) {
		return (obj == null) ? null : Long.valueOf(obj.toString());
	}

	private String valueStringByClob(Object object) {
		if (object == null) {
			return "";
		}
		Clob clob = (Clob) object;
		try {
			return clob.getSubString(1L, (int) clob.length());
		} catch (Exception e) {
			Log.error("", e);
			e.printStackTrace();
		}
		return null;
	}
}
