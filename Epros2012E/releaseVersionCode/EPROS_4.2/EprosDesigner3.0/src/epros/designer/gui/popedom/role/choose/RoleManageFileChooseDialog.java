package epros.designer.gui.popedom.role.choose;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.file.HighEfficiencyFileTree;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.util.DrawCommon;

public class RoleManageFileChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(RoleManageFileChooseDialog.class);

	public RoleManageFileChooseDialog(List<JecnTreeBean> list) {
		super(list, 25);
		this.setTitle(JecnProperties.getValue("fileChoose"));
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		List<JecnTreeBean> listTreeBean = null;
		if (DrawCommon.checkNameMaxLength(name)) {
			listTreeBean = new ArrayList<JecnTreeBean>();
		} else {
			listTreeBean = new ArrayList<JecnTreeBean>();
			try {
				List<JecnFileBeanT> list = ConnectionPool.getFileAction().getFilesByName(name, JecnConstants.projectId,
						JecnDesignerCommon.getSecondAdminUserId());
				for (JecnFileBeanT fileBean : list) {
					JecnTreeBean treeBean = new JecnTreeBean();
					treeBean.setId(fileBean.getFileID());
					treeBean.setName(fileBean.getFileName());
					// 是图片
					if (getChooseType() == 17) {
						if (JecnUtil.isImage(fileBean.getFileName())) {
							listTreeBean.add(treeBean);
						}
					} else {
						listTreeBean.add(treeBean);
					}
				}

			} catch (Exception e) {
				log.error("RoleManageFileChooseDialog searchByName is error！", e);

			}
		}
		return listTreeBean;
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("fileNameC");
	}

	@Override
	public JecnTree getJecnTree() {
		// return new HighEfficiencyFileTree(this);
		// if (JecnConstants.isMysql) {
		// return new RoutineFileTree();
		// } else {
		return new RoleManageHighEfficiencyFileTree(1, this);
		// }
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		title.add(JecnProperties.getValue("actBaseNum"));
		return title;
	}

	@Override
	public int[] hiddenCols() {
		return new int[] { 0, 2 };
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {

		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				data.add(jecnTreeBean.getNumberId());
				content.add(data);
			}
		}
		return content;

	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		data.add(jecnTreeBean.getNumberId());
		return data;
	}

	/**
	 * 点击表格 定位树节点
	 */
	public void isClickTable() {
		int row = this.resultTable.getSelectedRow();
		if (row == -1) {
			return;
		}
		JecnTreeBean treeBean = getListJecnTreeBean().get(row);
		if (treeBean == null) {
			return;
		}
		treeBean.setTreeNodeType(TreeNodeType.file);
		try {
			HighEfficiencyFileTree jTree = ((HighEfficiencyFileTree) this.getjTree());
			jTree.setAllowExpand(false);
			JecnTreeCommon.searchNode(treeBean, jTree);
			jTree.setAllowExpand(true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("RoleManageFileChooseDialog isClickTable is error！", e);
		}
	}
}
