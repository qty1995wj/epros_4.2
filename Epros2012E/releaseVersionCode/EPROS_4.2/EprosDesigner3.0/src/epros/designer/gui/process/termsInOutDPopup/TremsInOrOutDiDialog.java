package epros.designer.gui.process.termsInOutDPopup;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.system.FileMutilSelectCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.designer.JecnFigureInoutSampleT;
import epros.draw.designer.JecnFigureInoutT;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

public class TremsInOrOutDiDialog extends JecnDialog {

	private JecnPanel contentPanel = new JecnPanel(300, 300);
	/** 名称Lab */
	private JLabel nameLab = new JLabel(JecnProperties.getValue("nameC"));
	/** 名称 */
	private JTextField nameField = new JecnTextField();

	/** 说明Lab */
	private JLabel explainLab = new JLabel(JecnProperties.getValue("actBaseDes"));
	/** 说明 */
	private JecnTextArea explainText = new JecnTextArea();
	/** 所在的滚动面板 */
	private JScrollPane noteTablePane = new JScrollPane();

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel(550, 30);

	private JButton addBut = new JButton(JecnProperties.getValue("task_okBtn"));

	private JButton delBut = new JButton(JecnProperties.getValue("task_cancelBtn"));

	private JLabel verfyLab = new JLabel();
	/**
	 * 表名称
	 */
	private List<String> inOutNames = new ArrayList<String>();
	/** 资源数据 **/
	private JecnFigureInoutT inOutBean = null;

	private List<JecnTreeBean> modelTreeBean = new ArrayList<JecnTreeBean>();
	// 模板组件
	FileMutilSelectCommon modelFile = null;
	// 样例组件
	FileMutilSelectCommon sampFile = null;

	private List<JecnTreeBean> sampleTreeBean = new ArrayList<JecnTreeBean>();

	/** 是否显示样例 */
	protected boolean type = true;
	// 是否确认
	protected boolean isOk;

	public TremsInOrOutDiDialog(boolean type, JecnFigureInoutT inOutBean, List<String> names) {
		this.type = type;
		this.isOk = false;
		if (inOutBean != null) {
			this.inOutBean = inOutBean;
		} else {
			this.inOutBean = new JecnFigureInoutT();
		}
		this.inOutNames = names;
		initData();
		initializationInterface();
		initEvent();

	}

	/**
	 * 初始化数据 NAME //名称 TEMPLATE //模板 SAMPLE // 样例 EXPLAIN //说明
	 */
	private void initData() {
		if (inOutBean != null) {
			// 名称
			nameField.setText(inOutBean.getName());

			getTreeBean(inOutBean.getListSampleT());
			// 说明
			explainText.setText(inOutBean.getExplain());
		}
	}

	/**
	 * 封装 JECN_TREEBEAN 1 是样例 0是模板
	 * 
	 * @param listSampleT
	 */
	private void getTreeBean(List<JecnFigureInoutSampleT> listSampleT) {
		Set<Long> sampleIds = new HashSet<Long>();
		Set<Long> moduleIds = new HashSet<Long>();
		if (listSampleT != null && !listSampleT.isEmpty()) {
			for (JecnFigureInoutSampleT jecnFigureInoutSampleT : listSampleT) {
				// 封装 JECN_TREEBEAN 1 是样例 0是模板
				if (jecnFigureInoutSampleT.getType() == 1) {
					sampleIds.add(jecnFigureInoutSampleT.getFileId());
				} else {
					moduleIds.add(jecnFigureInoutSampleT.getFileId());
				}
			}
			getFileTreeBean(sampleIds, sampleTreeBean);
			getFileTreeBean(moduleIds, modelTreeBean);
		}
	}

	private void getFileTreeBean(Set<Long> ids, List<JecnTreeBean> treeBean) {
		try {
			if (ids != null && !ids.isEmpty()) {
				List<JecnTreeBean> treeBeans = ConnectionPool.getFileAction().getJecnTreeBeanByIds(ids);
				if (treeBeans != null && !treeBeans.isEmpty()) {
					treeBean.addAll(treeBeans);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 初始化界面
	 * 
	 */
	protected void initializationInterface() {
		// 设置窗体大小
		if (type) {
			this.setSize(500, 450);
		} else {
			this.setSize(500, 350);
		}
		this.setTitle(JecnProperties.getValue("addBtn"));
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 设置窗体不可编辑
		this.setResizable(false);
		this.setModal(true);
		contentPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		noteTablePane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension = new Dimension(200, 200);
		noteTablePane.setPreferredSize(dimension);
		noteTablePane.setMaximumSize(dimension);
		noteTablePane.setMinimumSize(dimension);
		noteTablePane.setViewportView(explainText);
		verfyLab.setForeground(Color.red);

		GridBagConstraints c = null;
		Insets insets = new Insets(3, 3, 3, 3);
		// 名称
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		contentPanel.add(nameLab, c);
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		contentPanel.add(nameField, c);

		// 模板选择
		modelFile = new FileMutilSelectCommon(1, contentPanel, insets, modelTreeBean, this, false, JecnProperties
				.getValue("flowMapMdelC"), true);
		if (type) {
			// 样例选择
			sampFile = new FileMutilSelectCommon(2, contentPanel, insets, sampleTreeBean, this, false, JecnProperties
					.getValue("actSampleC"), true);
		}
		// 说明
		c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0,
				0);
		contentPanel.add(explainLab, c);
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		contentPanel.add(noteTablePane, c);

		c = new GridBagConstraints(1, 4, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		contentPanel.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(verfyLab);
		buttonPanel.add(addBut);
		buttonPanel.add(delBut);
		this.add(contentPanel);

	}

	private void initEvent() {

		addBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ok();
			}
		});
		delBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
	}

	private void close() {
		this.setVisible(false);
	}

	public JecnFigureInoutT getInOutBean() {
		return inOutBean;
	}

	public static String getFileNameNoEx(String filename) {
		if ((filename != null) && (filename.length() > 0)) {
			int dot = filename.lastIndexOf('.');
			if ((dot > -1) && (dot < (filename.length()))) {
				return filename.substring(0, dot);
			}
		}
		return filename;
	}

	private void ok() {
		// 拼装文件
		initFile();
		// 名称 122个字符或60个汉字
		if (JecnValidateCommon.validateFieldNotPass(nameField.getText(), verfyLab, nameLab.getText())) {
			return;
		}
		// 验证重名
		if (validationRename(nameField.getText())) {
			return;
		}
		// 说明 1200个字符或600个汉字
		if (JecnValidateCommon.validateAreaNotPass(explainText.getText(), verfyLab, explainLab.getText())) {
			return;
		}
		if (StringUtils.isBlank(nameField.getText()) && StringUtils.isBlank(explainText.getText())
				&& (inOutBean.getListSampleT() == null || inOutBean.getListSampleT().isEmpty())
				&& inOutBean.getFileId() == null) {
			// 至少选择一项!
			verfyLab.setText(JecnProperties.getValue("fillOneItem"));
			return;
		}
		inOutBean.setName(nameField.getText().trim());
		inOutBean.setExplain(explainText.getText());
		inOutBean.setListSampleT(inOutBean.getListSampleT());
		inOutBean.setFigureId(inOutBean.getFigureId());
		inOutBean.setFileId(inOutBean.getFileId());
		inOutBean.setFileName(inOutBean.getFileName());
		inOutBean.setType(type ? 1 : 0);
		this.isOk = true;
		this.setVisible(false);
	}

	private void initFile() {
		List<JecnFigureInoutSampleT> samples = new ArrayList<JecnFigureInoutSampleT>();
		// 如果模板样例都为空的情况下返回 空集合
		if ((modelTreeBean == null || modelTreeBean.isEmpty()) && (sampleTreeBean == null || sampleTreeBean.isEmpty())) {
			inOutBean.setListSampleT(samples);
		}

		// 样例
		if (sampleTreeBean != null && !sampleTreeBean.isEmpty()) {
			for (JecnTreeBean bean : sampleTreeBean) {
				JecnFigureInoutSampleT sample = new JecnFigureInoutSampleT();
				sample.setFileId(bean.getId());
				sample.setFileName(bean.getName());
				sample.setType(1);
				samples.add(sample);
			}
			inOutBean.setListSampleT(samples);
		}

		// 模板
		if (modelTreeBean != null && !modelTreeBean.isEmpty()) {
			for (JecnTreeBean bean : modelTreeBean) {
				JecnFigureInoutSampleT sample = new JecnFigureInoutSampleT();
				sample.setFileId(bean.getId());
				sample.setFileName(bean.getName());
				sample.setType(0);
				samples.add(sample);
			}
			inOutBean.setListSampleT(samples);
		}

	}

	/**
	 * 验证重名
	 * 
	 * 447@param resourceMap44
	 * 
	 * @return
	 */
	private boolean validationRename(String name) {
		if (StringUtils.isNotBlank(name)) {
			if (inOutNames != null && !inOutNames.isEmpty()) {
				for (String tableName : inOutNames) {
					if (name.equals(tableName)) {
						verfyLab.setText(JecnProperties.getValue("notRename"));
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean isOk() {
		return isOk;
	}

	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}

}
