package epros.designer.gui.process.mode;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.popedom.person.JecnPersonInfoPanel;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class RoutineProcessModelMoveTree extends JecnRoutineTree{
	private Logger log = Logger.getLogger(RoutineProcessModelMoveTree.class);
	public RoutineProcessModelMoveTree(List<Long> listIds){
		super(listIds);
	}
	
	@Override
	public JecnTreeModel getTreeModel() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		try {
			list = ConnectionPool.getProcessModeAction().getAllFlowMapModels();
		} catch (Exception e) {
			log.error("RoutineProcessModelMoveTree getTreeModel is error", e);
		}
		
		// 根节点 "流程模板"
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.processModeRoot,JecnProperties.getValue("flowModel"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, this.getListIds());
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		
	}

}
