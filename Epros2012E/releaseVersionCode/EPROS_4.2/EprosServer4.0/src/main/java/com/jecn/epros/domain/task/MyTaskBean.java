package com.jecn.epros.domain.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.util.JecnProperties;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 任务列表显示（我的任务、任务管理）
 *
 * @author ZHANGXIAOHU @date： 日期：Apr 25, 2013 时间：9:52:48 AM
 */
public class MyTaskBean {

    @ApiModelProperty("任务ID")
    private Long taskId;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty(value = "任务类型<br>[0-流程任务,1-文件任务,2,3-制度任务,4-流程架构任务]", allowableValues = "0,1,2,3,4")
    private Integer taskType;
    /**
     * 创建名称
     */
    private String createName;
    /**
     * 任务阶段 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段 7：IT总监审批阶段
     */
    private Integer taskStage;

    private Integer taskElseState = -1;
    /**
     * 各阶段审核人标题名称
     */
    private String stageName;
    /**
     * 驳回状态 1：当前审批人存在驳回状态
     */
    private Integer reJect;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    private Date updateTime;
    /**
     * 任务开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date startTime;
    /**
     * 任务预估结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date endTime;
    /**
     * 任务发布时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd  HH:mm", timezone = "GMT+08")
    private Date publishTime;
    /**
     *  废止类型
     */
    private int approveType;
    /**
     * 责任人
     */
    private String resPeopleName;
    /**
     * 责任人类型 0 人员 1岗位
     */
    private Integer resPeopleNameType;
    /**
     * 当前任务的操作
     *******/
    private List<LinkResource> links = new ArrayList<LinkResource>();

    public String getTaskTypeStr() {
        String taskTypeStr = "";
        if (taskType == 0) {
            taskTypeStr = JecnProperties.getValue("processTask");//流程任务
        } else if (taskType == 1) {
            taskTypeStr = JecnProperties.getValue("fileTask");//文件任务
        } else if (taskType == 2 || taskType == 3) {
            taskTypeStr = JecnProperties.getValue("ruleTask");//制度任务
        } else if (taskType == 4) {
            taskTypeStr = JecnProperties.getValue("processMapTask");//流程地图任务
        }
        return taskTypeStr;
    }

    public int getApproveType() {
        return approveType;
    }

    public void setApproveType(int approveType) {
        this.approveType = approveType;
    }

    public List<LinkResource> getLinks() {
        return links;
    }

    public void setLinks(List<LinkResource> links) {
        this.links = links;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public Integer getTaskStage() {
        return taskStage;
    }

    public void setTaskStage(Integer taskStage) {
        this.taskStage = taskStage;
    }

    public Integer getTaskElseState() {
        return taskElseState;
    }

    public void setTaskElseState(Integer taskElseState) {
        this.taskElseState = taskElseState;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public Integer getReJect() {
        return reJect;
    }

    public void setReJect(Integer reJect) {
        this.reJect = reJect;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public String getResPeopleName() {
        return resPeopleName;
    }

    public void setResPeopleName(String resPeopleName) {
        this.resPeopleName = resPeopleName;
    }

    public Integer getResPeopleNameType() {
        return resPeopleNameType;
    }

    public void setResPeopleNameType(Integer resPeopleNameType) {
        this.resPeopleNameType = resPeopleNameType;
    }
}
