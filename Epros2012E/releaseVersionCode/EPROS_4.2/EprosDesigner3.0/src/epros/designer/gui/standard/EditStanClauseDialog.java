package epros.designer.gui.standard;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class EditStanClauseDialog extends StanClauseDialog {
	private static Logger log = Logger.getLogger(EditStanClauseDialog.class);
	private StandardBean standardBean = null;

	public EditStanClauseDialog(JecnTreeNode node, JTree jTree) {
		super(node, jTree);
		this.setTitle(JecnProperties.getValue("editClause"));
		initData();
	}

	private void initData() {
		try {
			standardBean = ConnectionPool.getStandardAction().getStandardBean(node.getJecnTreeBean().getId());
			if (standardBean != null) {
				clauseField.setText(standardBean.getCriterionClassName());
				clauseContentArea.setText(standardBean.getStanContent());
				remarkArea.setText(standardBean.getNote());
				programFileCheck.setSelected(standardBean.getIsproFile() == 1);
			} else {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("standardClauseDoesNotExist"));
			}

			// 初始化表格
			uploadFileCommon.initTableData(standardBean.getFileContents());
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("termsDataDoesNotExist"));
			log.error(JecnProperties.getValue("termsDataDoesNotExist"), e);
		}
	}

	/**
	 * @author yxw 2013-11-4
	 * @description:确定执行事件
	 */
	protected boolean okButtonAction() {
		if (isUpdate()) {
			if (!check()) {
				return false;
			}
			String clauseName = clauseField.getText().trim();
			String clauseContent = clauseContentArea.getText().trim();

			standardBean.setCriterionClassName(clauseName);
			standardBean.setUpdatePeopleId(JecnConstants.getUserId());
			standardBean.setStanContent(clauseContent);
			standardBean.setNote(remarkArea.getText().trim());
			standardBean.setIsproFile(programFileCheck.isSelected() ? 1 : 0);

			standardBean.setFileContents(uploadFileCommon.getFileContents());
			try {
				ConnectionPool.getStandardAction().updateStandardBean(standardBean);
				JecnTreeBean jecnTreeBean = node.getJecnTreeBean();
				jecnTreeBean.setName(clauseName);
				jecnTreeBean.setContent(clauseContent);
				node.setUserObject(clauseName);
				((JecnTreeModel) jTree.getModel()).reload(node);
			} catch (Exception e) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("errorModifyingStandardTerms"));
				log.error(JecnProperties.getValue("errorModifyingStandardTerms"), e);
			}
		}
		return true;
	}

	@Override
	protected boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameEidt(name, node);
	}

	/**
	 * 判断两个字符串有没有变化
	 * 
	 * @param newString
	 * @param oldString
	 * @return
	 */
	private boolean isChange(String newString, String oldString) {
		if (oldString == null) {
			oldString = "";
		}
		if (newString == null) {
			newString = "";
		}
		if (newString.equals(oldString)) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * 内容是否变化
	 * 
	 * @author hyl
	 * @return 是返回true 否则false
	 */
	public boolean isUpdate() {
		if (standardBean == null) {
			return false;
		}
		if (isChange(clauseField.getText().trim(), standardBean.getCriterionClassName())) {
			return true;
		}
		if (isChange(clauseContentArea.getText(), standardBean.getStanContent())) {
			return true;
		}
		if (isChange(remarkArea.getText().trim(), standardBean.getNote())) {
			return true;
		}
		if (uploadFileCommon.isUpdate(uploadFileCommon.getFileContents(), standardBean.getFileContents())) {
			return true;
		}

		int isSelect = programFileCheck.isSelected() ? 1 : 0;
		if (isSelect != standardBean.getIsproFile()) {
			return true;
		}
		return false;
	}
}
