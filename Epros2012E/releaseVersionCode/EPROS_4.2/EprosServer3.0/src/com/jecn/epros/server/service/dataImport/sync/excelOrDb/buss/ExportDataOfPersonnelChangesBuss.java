package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.dao.dataImport.excelorDB.IExportDataOfPersonnelChangesDao;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;

/**
 * 人员变更数据导出 excel
 * 
 * @author zhangft 2013-5-30
 * 
 */
@Transactional(rollbackFor = Exception.class)
public class ExportDataOfPersonnelChangesBuss {

	private final Log log = LogFactory.getLog(this.getClass());
	/** 导出excel(人员同步变更信息) 数据路径 */
	private String outputExcelDataPath = null;
	/** Dao层处理类 */
	private IExportDataOfPersonnelChangesDao exportDataOfPersonnelChangesDao;
	/** 发布前 */
	private String BEFORE_PUBLIC = "发布前";
	/** 发布后 */
	private String AFTER_PUBLIC = "发布后";
	/** 人员 */
	private String REF_PEOPLE_NAME = "人员";
	/** 岗位 */
	private String REF_POS_NAME = "岗位";
	/** 外网 */
	private String OUT_EMAIL = "外网";
	/** 内网 */
	private String INN_EMAIL = "内网";

	/** 是否存在变更 */
	private boolean hasChange = false;

	/**
	 * 得到人员同步文件的名称 控制在5个文件,每次删除创建时间最早的文件
	 * 
	 * @author zhangft 2013-6-5
	 */
	private String getOutputExcelPath() {
		String fileName = null;
		try {
			// 得到文件夹路径 D:/eprosFileLibrary/personnelChangesData/
			String dirPath = JecnFinal.getAllTempPath(JecnFinal.savePersonnelChangesDataPath());
			File fileDir = new File(dirPath);
			if (fileDir.isDirectory()) {
				File[] fileList = fileDir.listFiles();
				if (fileList.length != 0) {
					File minFile = fileList[0];
					Long minTime = Long.parseLong(fileList[0].getName()
							.substring(0, fileList[0].getName().indexOf(".")));
					if (fileList.length >= 90) { // 控制在90(三个月的文件记录)个文件,每次删除创建时间最早的文件
						for (int i = 0; i < fileList.length; i++) {
							File tempFile = fileList[i];
							Long tempTime = Long.parseLong(fileList[i].getName().substring(0,
									fileList[i].getName().indexOf(".")));
							if (minTime > tempTime) {
								minTime = tempTime;
								minFile = tempFile;
							}
						}
						// System.out.println("------------------------------------>要删除的："+minFile.getName());
						minFile.delete();
					}
				}
				Long fileTime = new Date().getTime();// 文件名= new
				// Date().getTime()+".xls"
				fileName = dirPath + fileTime.toString() + ".xls";
				// System.out.println("------------------------------------>要创建的："+fileName);

			}
		} catch (Exception e) {
			log.error("人员同步数据文件操作失败：" + this.getClass().getName() + "类的getOutputExcelDataPath方法" + e.getMessage());
		}

		return fileName;
	}

	/**
	 * 
	 * 人员变更 生成excel保存到本地
	 * 
	 * @throws Exception
	 * 
	 * @throws Exception
	 * 
	 */
	public boolean personnelChangesExcelExport(Long currProjectID, boolean isSyncUserOnly) throws Exception {
		outputExcelDataPath = getOutputExcelPath();
		log.info("人员变更-->影响数据存储开始，数据存放路径" + outputExcelDataPath);
		WritableWorkbook writableWorkbookData = null;
		try {
			// 判断模板文件路径和待保存文件路径 是否为空
			if (SyncTool.isNullOrEmtryTrim(outputExcelDataPath)) {
				return false;
			}

			log.info("信息存储路径验证成功");
			// 导出数据文件 不是文件返回
			File fileData = new File(outputExcelDataPath);

			writableWorkbookData = Workbook.createWorkbook(fileData);

			createSheets(writableWorkbookData);

			// 获取sheet
			WritableSheet[] sheetArray = writableWorkbookData.getSheets();
			// if (sheetArray.length != 22) {
			// return false;
			// }

			int index = 0;
			// 流程图责任部门表
			WritableSheet processTaskDeptSheet = sheetArray[index];
			index++;
			// 文件表 责任部门
			WritableSheet dutyDeptFileSheet = sheetArray[index];
			index++;
			// 制度责任部门
			WritableSheet dutyDeptRuleSheet = sheetArray[index];
			index++;

			// 部门查阅权限 0是流程 ;1是文件; 2是标准 ;3制度
			WritableSheet deptAccessPermSheet = sheetArray[index];
			index++;
			// 删除岗位影响的岗位组
			WritableSheet deleteAffectedPositionGroupSheet = sheetArray[index];
			index++;
			// 流程基本信息
			WritableSheet flowBasicInfoSheet = sheetArray[index];
			index++;
			// 岗位相关角色
			WritableSheet flowRoleWithPositionSheet = sheetArray[index];
			index++;
			// 岗位查阅权限
			WritableSheet positionAccessPermSheet = sheetArray[index];
			index++;
			// 删除人员影响的流程
			WritableSheet deleteUserFlowSheet = sheetArray[index];
			index++;
			// 删除人员影响的相关文件
			WritableSheet deleteUserImpactFileInfoSheet = sheetArray[index];
			index++;

			// 添加部门 sheet
			WritableSheet addDeptSheet = sheetArray[index];
			index++;

			// 添加岗位组 sheet
			WritableSheet addPosGroupSheet = sheetArray[index];
			index++;
			// 添加岗位 sheet
			WritableSheet addPosSheet = sheetArray[index];
			index++;
			// 添加人员 sheet
			WritableSheet addUserSheet = sheetArray[index];
			index++;
			// 删除部门 sheet
			WritableSheet deleteDeptSheet = sheetArray[index];
			index++;
			// 删除岗位组 sheet
			WritableSheet deletePosGroupSheet = sheetArray[index];
			index++;
			// 删除岗位 sheet
			WritableSheet deletePosSheet = sheetArray[index];
			index++;
			// 删除人员 sheet
			WritableSheet deleteUserSheet = sheetArray[index];
			index++;
			// 更新部门 sheet
			WritableSheet updateDeptSheet = sheetArray[index];
			index++;
			// 更新岗位组 sheet
			WritableSheet updatePosGroupSheet = sheetArray[index];
			index++;
			// 更新岗位 sheet
			WritableSheet updatePosSheet = sheetArray[index];
			index++;
			// 更新人员 sheet
			WritableSheet updateUserSheet = sheetArray[index];
			index++;

			// 编辑 流程图责任部门 集合
			List<Object[]> editProcessTaskDeptList1 = null;
			// 编辑 文件表 责任部门 集合
			List<Object[]> editDutyDeptList2 = null;
			// 编辑制度责任部门
			List<Object[]> editDuptDeptRuleList3 = null;
			// 编辑 部门查阅权限 sheet 0是流程 ;1是文件; 2是标准 ;3制度；4：发布类型
			List<Object[]> editDeptAccessPerList3 = null;
			// 编辑 删除岗位影响的岗位组
			List<Object[]> editDeleteAffectedList4 = null;
			// 编辑 岗位相关角色
			List<Object[]> editFlowRoleWithList6 = null;
			// 编辑 岗位查阅权限
			List<Object[]> editPositionAccessList7 = null;

			// 编辑 添加部门
			List<Object[]> editAddDeptList10 = null;
			// 编辑 添加岗位
			List<Object[]> editAddPosList11 = null;
			// 编辑 删除部门
			List<Object[]> editDeleteDeptList13 = null;
			// 编辑 删除岗位
			List<Object[]> editDeletePosList14 = null;
			// 编辑 更新部门
			List<Object[]> editUpdateDeptList16 = null;
			// 编辑 更新岗位
			List<Object[]> editUpdatePosList17 = null;
			// 添加岗位组
			List<Object[]> addPosGroupList = null;
			// 删除岗位组
			List<Object[]> deletePosGroupList = null;
			// 更新岗位组
			List<Object[]> updatePosGroupList = null;
			if (isSyncUserOnly) {
				editProcessTaskDeptList1 = new ArrayList<Object[]>();
				// 编辑 文件表 责任部门 集合
				editDutyDeptList2 = new ArrayList<Object[]>();
				// 编辑制度责任部门
				editDuptDeptRuleList3 = new ArrayList<Object[]>();
				// 编辑 部门查阅权限 sheet 0是流程 ;1是文件; 2是标准 ;3制度；4：发布类型
				editDeptAccessPerList3 = new ArrayList<Object[]>();
				// 编辑 删除岗位影响的岗位组
				editDeleteAffectedList4 = new ArrayList<Object[]>();
				// 编辑 岗位相关角色
				editFlowRoleWithList6 = new ArrayList<Object[]>();
				// 编辑 岗位查阅权限
				editPositionAccessList7 = new ArrayList<Object[]>();

				// 编辑 添加部门
				editAddDeptList10 = new ArrayList<Object[]>();
				// 编辑 添加岗位
				editAddPosList11 = new ArrayList<Object[]>();
				// 编辑 删除部门
				editDeleteDeptList13 = new ArrayList<Object[]>();
				// 编辑 删除岗位
				editDeletePosList14 = new ArrayList<Object[]>();
				// 编辑 更新部门
				editUpdateDeptList16 = new ArrayList<Object[]>();
				// 编辑 更新岗位
				editUpdatePosList17 = new ArrayList<Object[]>();
				// 添加岗位组
				addPosGroupList = new ArrayList<Object[]>();
				// 删除岗位组
				deletePosGroupList = new ArrayList<Object[]>();
				// 更新岗位组
				updatePosGroupList = new ArrayList<Object[]>();
			} else {
				editProcessTaskDeptList1 = exportDataOfPersonnelChangesDao.getProcessTaskDeptList(currProjectID);
				// 编辑 文件表 责任部门 集合
				editDutyDeptList2 = exportDataOfPersonnelChangesDao.getDutyDeptFileList(currProjectID);
				// 编辑制度责任部门
				editDuptDeptRuleList3 = exportDataOfPersonnelChangesDao.getDutyDeptRuleList(currProjectID);
				// 编辑 部门查阅权限 sheet 0是流程 ;1是文件; 2是标准 ;3制度；4：发布类型
				editDeptAccessPerList3 = exportDataOfPersonnelChangesDao.getDeptAccessPermList(currProjectID);
				// 编辑 删除岗位影响的岗位组
				editDeleteAffectedList4 = exportDataOfPersonnelChangesDao
						.getDeleteAffectedPositionGroupList(currProjectID);
				// 编辑 岗位相关角色
				editFlowRoleWithList6 = exportDataOfPersonnelChangesDao.getFlowRoleWithPositionList(currProjectID);
				// 编辑 岗位查阅权限
				editPositionAccessList7 = exportDataOfPersonnelChangesDao.getPositionAccessPermList();

				// 编辑 添加部门
				editAddDeptList10 = exportDataOfPersonnelChangesDao.getAddDeptList(currProjectID);
				// 编辑 添加岗位
				editAddPosList11 = exportDataOfPersonnelChangesDao.getAddPosList();
				// 编辑 删除部门
				editDeleteDeptList13 = exportDataOfPersonnelChangesDao.getDeleteDeptList(currProjectID);
				// 编辑 删除岗位
				editDeletePosList14 = exportDataOfPersonnelChangesDao.getDeletePosList();
				// 编辑 更新部门
				editUpdateDeptList16 = exportDataOfPersonnelChangesDao.getUpdateDeptList(currProjectID);
				// 编辑 更新岗位
				editUpdatePosList17 = exportDataOfPersonnelChangesDao.getUpdatePosList(currProjectID);

				// 添加岗位组
				addPosGroupList = exportDataOfPersonnelChangesDao.getAddPosGroupList();
				// 删除岗位组
				deletePosGroupList = exportDataOfPersonnelChangesDao.getDeletePosGroupList();
				// 更新岗位组
				updatePosGroupList = exportDataOfPersonnelChangesDao.getUpdatePosGroupList();
			}

			// 编辑 流程基本信息
			List<Object[]> editFlowBasicInfoList5 = exportDataOfPersonnelChangesDao.getFlowBasicInfoList(currProjectID);

			// 编辑 删除人员影响的流程
			List<Object[]> editDeleteUserFlowList8 = exportDataOfPersonnelChangesDao.getDeleteUserFlowList();
			// 编辑 删除人员影响的相关文件
			List<Object[]> editDeleteUserImpactFileList9 = exportDataOfPersonnelChangesDao
					.getDeleteUserImpactFileInfoList();
			// 编辑 添加人员
			List<Object[]> editAddUserList12 = exportDataOfPersonnelChangesDao.getAddUserList();
			// 编辑 更新人员
			List<Object[]> editUpdateUserList18 = exportDataOfPersonnelChangesDao.getUpdateUserList();
			// 编辑 删除人员
			List<Object[]> editDeleteUserList15 = exportDataOfPersonnelChangesDao.getDeleteUserList();

			// 获取变更数据
			initChangeData(editProcessTaskDeptList1, editDutyDeptList2, editDuptDeptRuleList3, editDeptAccessPerList3,
					editDeleteAffectedList4, editFlowBasicInfoList5, editFlowRoleWithList6, editPositionAccessList7,
					editDeleteUserFlowList8, editDeleteUserImpactFileList9, editAddDeptList10, editAddPosList11,
					editAddUserList12, editDeleteDeptList13, editDeletePosList14, editDeleteUserList15,
					editUpdateDeptList16, editUpdatePosList17, editUpdateUserList18, addPosGroupList,
					deletePosGroupList, updatePosGroupList, currProjectID);

			// 编辑 流程图责任部门 sheet
			editProcessTaskDeptSheet(processTaskDeptSheet, editProcessTaskDeptList1);
			// 编辑 文件表 责任部门 sheet
			editDutyDeptFileSheet(dutyDeptFileSheet, editDutyDeptList2);
			// 编辑制度责任部门
			editDutyDeptRuleSheet(dutyDeptRuleSheet, editDuptDeptRuleList3);

			// 编辑 部门查阅权限 sheet 0是流程 ;1是文件; 2是标准 ;3制度；4：发布类型
			editDeptAccessPermSheet(deptAccessPermSheet, editDeptAccessPerList3);
			// 编辑 删除岗位影响的岗位组 sheet 删除岗位影响的岗位组 Object[]
			// 1:关联岗位名称,0:岗位组名称，,2:岗位所属流程;3:是否发布 1:发布后
			editDeleteAffectedPositionGroupSheet(deleteAffectedPositionGroupSheet, editDeleteAffectedList4);
			// 编辑 流程基本信息 sheet --流程责任人 责任人类型(0：人员：1岗位;) 发布状态(发布前：0；发布后：1)
			editFlowBasicInfoSheet(flowBasicInfoSheet, editFlowBasicInfoList5);
			// 编辑 岗位相关角色 sheet 删除岗位，影响的岗位相关角色 Object[]
			// 0:岗位名称；1：角色名称；2：流程名称；3：是否发布（标识1：已发布；0未发布）
			editFlowRoleWithPositionSheet(flowRoleWithPositionSheet, editFlowRoleWithList6);
			// 编辑 岗位查阅权限 sheet
			editPositionAccessPermSheet(positionAccessPermSheet, editPositionAccessList7);
			// 编辑 删除人员影响的流程 sheet
			editDeleteUserFlowSheet(deleteUserFlowSheet, editDeleteUserFlowList8);
			// 编辑 删除人员影响的相关文件 sheet
			editDeleteUserImpactFileInfoSheet(deleteUserImpactFileInfoSheet, editDeleteUserImpactFileList9);

			// 编辑 添加部门 sheet
			editAddDeptSheet(addDeptSheet, editAddDeptList10);
			// 编辑 添加岗位 sheet
			editAddPosSheet(addPosSheet, editAddPosList11);
			// 编辑 添加人员 sheet
			editAddUserSheet(addUserSheet, editAddUserList12);
			// 编辑 删除部门 sheet
			editDeleteDeptSheet(deleteDeptSheet, editDeleteDeptList13);
			// 编辑 删除岗位 sheet
			editDeletePosSheet(deletePosSheet, editDeletePosList14);
			// 编辑 删除人员 sheet
			editDeleteUserSheet(deleteUserSheet, editDeleteUserList15);
			// 编辑 更新部门 sheet
			editUpdateDeptSheet(updateDeptSheet, editUpdateDeptList16);
			// 编辑 更新岗位 sheet
			editUpdatePosSheet(updatePosSheet, editUpdatePosList17);
			// 编辑 更新人员 sheet
			editUpateUserSheet(updateUserSheet, editUpdateUserList18);

			// 岗位组变更
			// 1. 新增岗位组
			editAddPosGroupSheet(addPosGroupSheet, addPosGroupList);
			// 2. 删除岗位组
			editDeletePosGroupSheet(deletePosGroupSheet, deletePosGroupList);
			// 3. 更新岗位组
			editUpdatePosGroupSheet(updatePosGroupSheet, updatePosGroupList);
			// 写入本地
			writableWorkbookData.write();
			// workbook在调用close方法时，才向输出流中写入数据
			writableWorkbookData.close();
			writableWorkbookData = null;

			log.info("人员变更-->影响数据存储结束，数据存放路径：" + outputExcelDataPath);
		} catch (Exception e) {
			log.error("人员变更数据导出失败：" + this.getClass().getName() + "类的processTaskDeptExcel方法" + e);
			throw e;
		} finally {
			if (writableWorkbookData != null) {
				try {
					writableWorkbookData.close();
				} catch (Exception e2) {
					log.error("人员变更数据导出失败：" + this.getClass().getName() + "类的processTaskDeptExcel方法" + e2.getMessage());
					return false;
				}
			}
		}
		return true;
	}

	private void createSheets(WritableWorkbook writableWorkbookData) throws RowsExceededException, WriteException {
		int index = 0;
		createSheet(writableWorkbookData, index++, "流程责任部门", new String[] { "部门ID", "组织编号", "组织名称", "流程名称", "发布状态" });
		createSheet(writableWorkbookData, index++, "文件责任部门", new String[] { "部门ID", "组织名称", "文件ID", "文件名称", "发布状态" });
		createSheet(writableWorkbookData, index++, "制度责任部门", new String[] { "部门ID", "组织名称", "制度名称", "发布状态" });
		createSheet(writableWorkbookData, index++, "部门查阅权限", new String[] { "部门编号", "组织编号", "组织名称", "相关文件名称", "类型 " });
		createSheet(writableWorkbookData, index++, "删除岗位影响的岗位组", new String[] { "岗位名称", "岗位组名称", "岗位组所属流程名称" });
		createSheet(writableWorkbookData, index++, "流程责任人", new String[] { "流程ID", "流程名称", "名称", " 责任人类型", "发布状态" });
		createSheet(writableWorkbookData, index++, "岗位相关角色", new String[] { "岗位名称", "角色名称", "流程名称", "发布状态" });
		createSheet(writableWorkbookData, index++, "岗位查阅权限", new String[] { "岗位ID", "岗位编号", "岗位名称", "相关文件名称", "类型 " });
		createSheet(writableWorkbookData, index++, "删除人员影响的流程", new String[] { "流程名称", "登录名称", "真实姓名" });
		createSheet(writableWorkbookData, index++, "删除人员影响的文件", new String[] { "文件名称", "文件目录", "登录名称", "真实姓名" });
		createSheet(writableWorkbookData, index++, "添加部门", new String[] { "部门编号", "部门名称", "父编号" });
		createSheet(writableWorkbookData, index++, "添加岗位组", new String[] { "岗位组变化", "岗位组名称", "父编号", "目录" });
		createSheet(writableWorkbookData, index++, "添加岗位", new String[] { "岗位编号", "岗位名称", "岗位所属部门编号" });
		createSheet(writableWorkbookData, index++, "添加人员", new String[] { "人员编号", "真实姓名", "任职岗位编号", "邮箱", "EMAIL类型",
				"联系电话" });
		createSheet(writableWorkbookData, index++, "删除部门", new String[] { "组织编号", "组织名称" });
		createSheet(writableWorkbookData, index++, "删除岗位组", new String[] { "岗位组编号", "岗位组名称", "目录" });
		createSheet(writableWorkbookData, index++, "删除岗位", new String[] { "岗位编号", "岗位名称", "组织编号" });
		createSheet(writableWorkbookData, index++, "删除人员", new String[] { "登录名称", "真实姓名", "邮箱地址", "EMAIL类型", "电话号码" });
		createSheet(writableWorkbookData, index++, "更新部门", new String[] { "部门编号", "部门名称(新)", "上级部门编号(新)", "部门编号",
				"部门名称(原)", "上级部门编号(原)" });
		createSheet(writableWorkbookData, index++, "更新岗位组", new String[] { "岗位组编号", "岗位组名称(新)", "上级岗位组编号(新)", "岗位组编号",
				"岗位组名称(原)", "上级岗位组编号(原)" });
		createSheet(writableWorkbookData, index++, "更新岗位", new String[] { "岗位编号(原)", "岗位名称(原)", "部门编号(原)", "部门名称(原)",
				"岗位编号(新)", "岗位名称(新)", "部门编号(新)", "部门名称(新)" });
		createSheet(writableWorkbookData, index++, "更新人员", new String[] { "登录名称(原)", "真实姓名(原)", "邮箱(原)", "EMAIL类型(原)",
				"电话号码(原)", "登录名称(新)", "真实姓名(新)", "邮箱(新)", "EMAIL类型(新)", "电话号码(新)" });
	}

	private void createSheet(WritableWorkbook writableWorkbookData, int sheetIndex, String sheetName, String[] title)
			throws RowsExceededException, WriteException {
		WritableSheet createSheet = writableWorkbookData.createSheet(sheetName, sheetIndex);
		// 表头的样式 背景为黄色
		WritableCellFormat titleStyleY = getSheetTitleStyleY();
		for (int col = 0; col < title.length; col++) {
			createSheet.setColumnView(col, 28);
			createSheet.addCell(new Label(col, 0, title[col], titleStyleY));
		}
	}

	/**
	 * 设置表头的样式
	 */
	private WritableCellFormat getSheetTitleStyleY() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.YELLOW);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);

		return tempCellFormat;
	}

	/**
	 * 更新岗位组
	 * 
	 * @param updatePosGroupSheet
	 * @param updatePosGroupList
	 * @throws Exception
	 */
	private void editUpdatePosGroupSheet(WritableSheet updatePosGroupSheet, List<Object[]> updatePosGroupList)
			throws Exception {
		// 添加数据
		for (int i = 0; i < updatePosGroupList.size(); i++) {
			Object[] temp = updatePosGroupList.get(i);
			// 行数
			int rowNum = i + 1;

			// 岗位组编号
			if (SyncTool.isNotNullObj(temp[0]))
				updatePosGroupSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 岗位组名称
			if (SyncTool.isNotNullObj(temp[1]))
				updatePosGroupSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 父编号
			if (SyncTool.isNotNullObj(temp[2]))
				updatePosGroupSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 岗位组编号
			if (SyncTool.isNotNullObj(temp[3]))
				updatePosGroupSheet.addCell(new Label(3, rowNum, temp[3].toString()));
			// 岗位组名称
			if (SyncTool.isNotNullObj(temp[4]))
				updatePosGroupSheet.addCell(new Label(4, rowNum, temp[4].toString()));
			// 目录编号
			if (SyncTool.isNotNullObj(temp[5]))
				updatePosGroupSheet.addCell(new Label(5, rowNum, temp[5].toString()));
		}

	}

	/**
	 * 删除岗位组
	 * 
	 * @param deletePosGroupSheet
	 * @param deletePosGroupList
	 * @throws Exception
	 */
	private void editDeletePosGroupSheet(WritableSheet deletePosGroupSheet, List<Object[]> deletePosGroupList)
			throws Exception {
		// 添加数据
		for (int i = 0; i < deletePosGroupList.size(); i++) {
			Object[] temp = deletePosGroupList.get(i);
			// 行数
			int rowNum = i + 1;

			// 岗位组编号
			if (SyncTool.isNotNullObj(temp[0]))
				deletePosGroupSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 岗位组名称
			if (SyncTool.isNotNullObj(temp[1]))
				deletePosGroupSheet.addCell(new Label(1, rowNum, temp[1].toString()));
		}

	}

	/**
	 * 人员变更 ： 同步数据 导出excel
	 * 
	 * @param currProjectID
	 * @return
	 * @throws Exception
	 */
	public boolean syncDataExcelExport(Long currProjectID) throws Exception {
		// log.info("人员变更 -->数据同步存储开始，数据存放路径" + outputExcelDataPath);
		// Workbook workbookModel = null;
		// WritableWorkbook writableWorkbookData = null;
		// try {
		// // 判断模板文件路径和待保存文件路径 是否为空
		// if (SyncTool.isNullOrEmtryTrim(outputExcelModelPath)
		// || SyncTool.isNullOrEmtryTrim(outputExcelDataPath)) {
		// return false;
		// }
		// // 导出模板文件 不存在返回 , 要使用第一次操作完成的outputExcelDataPath当做模板
		// File fileModel = new File(outputExcelDataPath);
		// if (!fileModel.exists()) { // 模板文件是否存在
		// return false;
		// }
		// log.info("信息存储路径验证成功");
		// // 导出数据文件 不是文件返回
		// File fileData = new File(outputExcelDataPath);
		//
		// workbookModel = Workbook.getWorkbook(fileModel);
		// writableWorkbookData = Workbook.createWorkbook(fileData,
		// workbookModel);
		//
		// // 获取sheet
		// WritableSheet[] sheetArray = writableWorkbookData.getSheets();
		// if (sheetArray.length != 19) {
		// return false;
		// }
		//
		// // 添加部门 sheet
		// WritableSheet addDeptSheet = sheetArray[10];
		// // 添加岗位 sheet
		// WritableSheet addPosSheet = sheetArray[11];
		// // 添加人员 sheet
		// WritableSheet addUserSheet = sheetArray[12];
		// // 删除部门 sheet
		// WritableSheet deleteDeptSheet = sheetArray[13];
		// // 删除岗位 sheet
		// WritableSheet deletePosSheet = sheetArray[14];
		// // 删除人员 sheet
		// WritableSheet deleteUserSheet = sheetArray[15];
		// // 更新部门 sheet
		// WritableSheet updateDeptSheet = sheetArray[16];
		// // 更新岗位 sheet
		// WritableSheet updatePosSheet = sheetArray[17];
		// // 更新人员 sheet
		// WritableSheet updateUserSheet = sheetArray[18];
		//
		// // 编辑 添加部门 sheet
		// editAddDeptSheet(addDeptSheet, exportDataOfPersonnelChangesDao
		// .getAddDeptList(currProjectID));
		// // 编辑 添加岗位 sheet
		// editAddPosSheet(addPosSheet, exportDataOfPersonnelChangesDao
		// .getAddPosList());
		// // 编辑 添加人员 sheet
		// editAddUserSheet(addUserSheet, exportDataOfPersonnelChangesDao
		// .getAddUserList());
		// // 编辑 删除部门 sheet
		// editDeleteDeptSheet(deleteDeptSheet,
		// exportDataOfPersonnelChangesDao
		// .getDeleteDeptList(currProjectID));
		// // 编辑 删除岗位 sheet
		// editDeletePosSheet(deletePosSheet, exportDataOfPersonnelChangesDao
		// .getDeletePosList());
		// // 编辑 删除人员 sheet
		// editDeleteUserSheet(deleteUserSheet,
		// exportDataOfPersonnelChangesDao.getDeleteUserList());
		// // 编辑 更新部门 sheet
		// editUpdateDeptSheet(updateDeptSheet,
		// exportDataOfPersonnelChangesDao
		// .getUpdateDeptList(currProjectID));
		// // 编辑 更新岗位 sheet
		// editUpdatePosSheet(updatePosSheet, exportDataOfPersonnelChangesDao
		// .getUpdatePosList(currProjectID));
		// // 编辑 更新人员 sheet
		// editUpateUserSheet(updateUserSheet, exportDataOfPersonnelChangesDao
		// .getUpdateUserList());
		//
		// // 写入本地
		// writableWorkbookData.write();
		// log.info("人员变更 -->数据同步存储结束，数据存放路径：" + outputExcelDataPath);
		// } catch (Exception e) {
		// log.error("人员变更，同步数据导出失败：" + this.getClass().getName()
		// + "类的syncDataExcelExport方法" + e.getMessage());
		// e.printStackTrace();
		// throw e;
		// } finally {
		// if (writableWorkbookData != null) {
		// try {
		// writableWorkbookData.close();
		// } catch (Exception e2) {
		// log.error("人员变更，同步数据导出失败:" + this.getClass().getName()
		// + "类的syncDataExcelExport方法" + e2.getMessage());
		// return false;
		// }
		// }
		// }
		return true;
	}

	/**
	 * 编辑流程图责任部门sheet,把数据写入给定的sheet中
	 * 
	 * @param processTaskDeptSheet
	 *            ：流程图责任部门sheet
	 * @param deptBeanList
	 *            ： 流程图责任部门数据
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	private void editProcessTaskDeptSheet(WritableSheet processTaskDeptSheet, List<Object[]> deptBeanList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < deptBeanList.size(); i++) {
			Object[] temp = deptBeanList.get(i);
			// 行数
			int rowNum = i + 1;

			// 部门ID ORG_ID
			if (SyncTool.isNotNullObj(temp[0]))
				processTaskDeptSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 部门编号 ORG_NUMBER
			if (SyncTool.isNotNullObj(temp[1]))
				processTaskDeptSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 部门名称 ORG_NAME
			if (SyncTool.isNotNullObj(temp[2]))
				processTaskDeptSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 流程名称 FLOW_NAME
			if (SyncTool.isNotNullObj(temp[3]))
				processTaskDeptSheet.addCell(new Label(3, rowNum, temp[3].toString()));
			// 发布状态 0:未发布 1:已发布
			if (SyncTool.isNotNullObj(temp[4])) {
				String state = temp[4].toString();
				if ("0".equals(state)) {
					processTaskDeptSheet.addCell(new Label(4, rowNum, BEFORE_PUBLIC));
				}
				if ("1".equals(state)) {
					processTaskDeptSheet.addCell(new Label(4, rowNum, AFTER_PUBLIC));
				}
			}
		}
	}

	/**
	 * 文件表 责任部门
	 * 
	 * @param dutyDeptFileSheet
	 * @param dutyDeptFileList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editDutyDeptFileSheet(WritableSheet dutyDeptFileSheet, List<Object[]> dutyDeptFileList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < dutyDeptFileList.size(); i++) {
			Object[] temp = dutyDeptFileList.get(i);

			if (SyncTool.isNullOrEmtryTrim(temp[0].toString())) {
				break;
			}
			// 行数
			int rowNum = i + 1;

			// 部门ID ORG_ID
			if (SyncTool.isNotNullObj(temp[0]))
				dutyDeptFileSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 组织名称 ORG_NAME
			if (SyncTool.isNotNullObj(temp[1]))
				dutyDeptFileSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 文件ID file_id
			if (SyncTool.isNotNullObj(temp[2]))
				dutyDeptFileSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 文件名称 file_name
			if (SyncTool.isNotNullObj(temp[3]))
				dutyDeptFileSheet.addCell(new Label(3, rowNum, temp[3].toString()));
			// 发布状态 0:未发布 1:已发布
			if (SyncTool.isNotNullObj(temp[4])) {
				String state = temp[4].toString();
				if ("0".equals(state)) {
					dutyDeptFileSheet.addCell(new Label(4, rowNum, BEFORE_PUBLIC));
				}
				if ("1".equals(state)) {
					dutyDeptFileSheet.addCell(new Label(4, rowNum, AFTER_PUBLIC));
				}
			}
		}
	}

	/**
	 * 制度责任部门
	 * 
	 * @param dutyDeptRuleSheet
	 * @param dutyDeptRuleList
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	private void editDutyDeptRuleSheet(WritableSheet dutyDeptRuleSheet, List<Object[]> dutyDeptRuleList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < dutyDeptRuleList.size(); i++) {
			Object[] temp = dutyDeptRuleList.get(i);

			if (SyncTool.isNullOrEmtryTrim(temp[0].toString())) {
				break;
			}
			// 行数
			int rowNum = i + 1;

			// 部门ID ORG_ID
			if (SyncTool.isNotNullObj(temp[0]))
				dutyDeptRuleSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 组织名称 ORG_NAME
			if (SyncTool.isNotNullObj(temp[1]))
				dutyDeptRuleSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 制度名称 rule_name
			if (SyncTool.isNotNullObj(temp[2]))
				dutyDeptRuleSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 发布状态 0:未发布 1:已发布
			if (SyncTool.isNotNullObj(temp[3])) {
				String state = temp[3].toString();
				if ("0".equals(state)) {
					dutyDeptRuleSheet.addCell(new Label(3, rowNum, BEFORE_PUBLIC));
				}
				if ("1".equals(state)) {
					dutyDeptRuleSheet.addCell(new Label(3, rowNum, AFTER_PUBLIC));
				}
			}
		}

	}

	/**
	 * 部门查阅权限
	 * 
	 * @param deptAccessPermSheet
	 * @param deptAccessPermList
	 *            0是流程 ;1是文件; 2是标准 ;3制度；4：发布类型
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editDeptAccessPermSheet(WritableSheet deptAccessPermSheet, List<Object[]> deptAccessPermList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < deptAccessPermList.size(); i++) {
			Object[] temp = deptAccessPermList.get(i);

			if (SyncTool.isNullOrEmtryTrim(temp[0].toString())) {
				break;
			}
			// 行数
			int rowNum = i + 1;

			// 部门编号 org_id
			if (SyncTool.isNotNullObj(temp[0]))
				deptAccessPermSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 组织编号 ORG_NAME
			if (SyncTool.isNotNullObj(temp[1]))
				deptAccessPermSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 组织名称 file_id
			if (SyncTool.isNotNullObj(temp[2]))
				deptAccessPermSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 流程名称 file_name
			if (SyncTool.isNotNullObj(temp[3]))
				deptAccessPermSheet.addCell(new Label(3, rowNum, temp[3].toString()));
			// 类型 0是流程 ;1是文件; 2是标准 ;3制度
			if (SyncTool.isNotNullObj(temp[4])) {
				String state = temp[4].toString();
				if ("0".equals(state)) {
					deptAccessPermSheet.addCell(new Label(4, rowNum, "流程"));
				}
				if ("1".equals(state)) {
					deptAccessPermSheet.addCell(new Label(4, rowNum, "文件"));
				}
				if ("2".equals(state)) {
					deptAccessPermSheet.addCell(new Label(4, rowNum, "标准"));
				}
				if ("3".equals(state)) {
					deptAccessPermSheet.addCell(new Label(4, rowNum, "制度"));
				}
			}
		}
	}

	/**
	 * 删除岗位影响的岗位组
	 * 
	 * @param deleteAffectedPositionGroupSheet
	 * @param deleteAffectedPositionGroupList
	 *            Object[] 1:关联岗位名称,0:岗位组名称，,2:岗位所属流程;
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editDeleteAffectedPositionGroupSheet(WritableSheet deleteAffectedPositionGroupSheet,
			List<Object[]> deleteAffectedPositionGroupList) throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < deleteAffectedPositionGroupList.size(); i++) {
			Object[] temp = deleteAffectedPositionGroupList.get(i);

			if (SyncTool.isNullOrEmtryTrim(temp[0].toString())) {
				break;
			}
			// 行数
			int rowNum = i + 1;

			// 0:岗位组名称
			if (SyncTool.isNotNullObj(temp[0]))
				deleteAffectedPositionGroupSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 1:关联岗位名称
			if (SyncTool.isNotNullObj(temp[1]))
				deleteAffectedPositionGroupSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 2:岗位所属流程
			if (SyncTool.isNotNullObj(temp[2]))
				deleteAffectedPositionGroupSheet.addCell(new Label(2, rowNum, temp[2].toString()));
		}
	}

	/**
	 * 流程基本信息
	 * 
	 * @param flowBasicInfoSheet
	 * @param flowBasicInfoList
	 *            --流程责任人 责任人类型(0：人员：1岗位;) 发布状态(发布前：0；发布后：1)
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editFlowBasicInfoSheet(WritableSheet flowBasicInfoSheet, List<Object[]> flowBasicInfoList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < flowBasicInfoList.size(); i++) {
			Object[] temp = flowBasicInfoList.get(i);

			if (SyncTool.isNullOrEmtryTrim(temp[0].toString())) {
				break;
			}
			// 行数
			int rowNum = i + 1;

			// 流程ID flow_id
			if (SyncTool.isNotNullObj(temp[0]))
				flowBasicInfoSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 流程名称 flow_name
			if (SyncTool.isNotNullObj(temp[1]))
				flowBasicInfoSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 名称（岗位名称或人员姓名）RES_NAME
			if (SyncTool.isNotNullObj(temp[2]))
				flowBasicInfoSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 责任人类型 res_pe_type (0：人员：1岗位;)
			if (SyncTool.isNotNullObj(temp[3])) {
				String strType = temp[3].toString();
				if ("0".equals(strType)) {// 0：人员
					flowBasicInfoSheet.addCell(new Label(3, rowNum, REF_PEOPLE_NAME));
				} else if ("1".equals(strType)) {// 1岗位
					flowBasicInfoSheet.addCell(new Label(3, rowNum, REF_POS_NAME));
				}
			}
			// 发布状态 0:未发布 1:已发布
			if (SyncTool.isNotNullObj(temp[4])) {
				String state = temp[4].toString();
				if ("0".equals(state)) {
					flowBasicInfoSheet.addCell(new Label(4, rowNum, BEFORE_PUBLIC));
				} else if ("1".equals(state)) {
					flowBasicInfoSheet.addCell(new Label(4, rowNum, AFTER_PUBLIC));
				}
			}
		}
	}

	/**
	 * 岗位相关角色
	 * 
	 * @param flowRoleWithPositionSheet
	 * @param flowRoleWithPositionList
	 *            0:岗位名称；1：角色名称；2：流程名称；3：是否发布（标识1：已发布；0未发布）
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editFlowRoleWithPositionSheet(WritableSheet flowRoleWithPositionSheet,
			List<Object[]> flowRoleWithPositionList) throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < flowRoleWithPositionList.size(); i++) {
			Object[] temp = flowRoleWithPositionList.get(i);

			if (SyncTool.isNullOrEmtryTrim(temp[0].toString())) {
				break;
			}
			// 行数
			int rowNum = i + 1;

			// 主键ID FLOW_STATION_ID
			if (SyncTool.isNotNullObj(temp[0]))
				flowRoleWithPositionSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 流程元素主键ID(角色ID) FIGURE_FLOW_ID
			if (SyncTool.isNotNullObj(temp[1]))
				flowRoleWithPositionSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 岗位ID FIGURE_POSITION_ID
			if (SyncTool.isNotNullObj(temp[2]))
				flowRoleWithPositionSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 岗位类型 TYPE
			if (SyncTool.isNotNullObj(temp[3])) {
				String state = temp[3].toString();
				if ("0".equals(state)) {
					flowRoleWithPositionSheet.addCell(new Label(3, rowNum, BEFORE_PUBLIC));
				} else if ("1".equals(state)) {
					flowRoleWithPositionSheet.addCell(new Label(3, rowNum, AFTER_PUBLIC));
				}
			}
		}

	}

	/**
	 * 岗位查阅权限
	 * 
	 * @param positionAccessPermSheet
	 * @param positionAccessPermList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editPositionAccessPermSheet(WritableSheet positionAccessPermSheet,
			List<Object[]> positionAccessPermList) throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < positionAccessPermList.size(); i++) {
			Object[] temp = positionAccessPermList.get(i);

			if (SyncTool.isNullOrEmtryTrim(temp[0].toString())) {
				break;
			}
			// 行数
			int rowNum = i + 1;

			// 岗位元素表 主键ID figure_id
			if (SyncTool.isNotNullObj(temp[0]))
				positionAccessPermSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 岗位编号 figure_number_id
			if (SyncTool.isNotNullObj(temp[1]))
				positionAccessPermSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 元素名称 figure_text
			if (SyncTool.isNotNullObj(temp[2]))
				positionAccessPermSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 流程名称 FLOW_NAME
			if (SyncTool.isNotNullObj(temp[3]))
				positionAccessPermSheet.addCell(new Label(3, rowNum, temp[3].toString()));
			// 类型 0是流程 ;1是文件; 2是标准 ;3制度
			if (SyncTool.isNotNullObj(temp[4])) {
				String state = temp[4].toString();
				if ("0".equals(state)) {
					positionAccessPermSheet.addCell(new Label(4, rowNum, "流程"));
				}
				if ("1".equals(state)) {
					positionAccessPermSheet.addCell(new Label(4, rowNum, "文件"));
				}
				if ("2".equals(state)) {
					positionAccessPermSheet.addCell(new Label(4, rowNum, "标准"));
				}
				if ("3".equals(state)) {
					positionAccessPermSheet.addCell(new Label(4, rowNum, "制度"));
				}
			}
		}
	}

	/**
	 * 删除人员影响的流程
	 * 
	 * @param deleteUserFlowSheet
	 * @param deleteUserFlowList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editDeleteUserFlowSheet(WritableSheet deleteUserFlowSheet, List<Object[]> deleteUserFlowList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < deleteUserFlowList.size(); i++) {
			Object[] temp = deleteUserFlowList.get(i);

			if (SyncTool.isNullOrEmtryTrim(temp[0].toString())) {
				break;
			}
			// 行数
			int rowNum = i + 1;

			// 流程名称 flow_name
			if (SyncTool.isNotNullObj(temp[0]))
				deleteUserFlowSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 登录名称 LOGIN_NAME
			if (SyncTool.isNotNullObj(temp[1]))
				deleteUserFlowSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 真实姓名 TRUE_NAME
			if (SyncTool.isNotNullObj(temp[2]))
				deleteUserFlowSheet.addCell(new Label(2, rowNum, temp[2].toString()));
		}

	}

	/**
	 * 删除人员影响的相关文件
	 * 
	 * @param deleteUserImpactFileInfoSheet
	 * @param deleteUserImpactFileInfoList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editDeleteUserImpactFileInfoSheet(WritableSheet deleteUserImpactFileInfoSheet,
			List<Object[]> deleteUserImpactFileInfoList) throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < deleteUserImpactFileInfoList.size(); i++) {
			Object[] temp = deleteUserImpactFileInfoList.get(i);

			if (SyncTool.isNullOrEmtryTrim(temp[0].toString())) {
				break;
			}
			// 行数
			int rowNum = i + 1;

			// 文件名称 file_name
			if (SyncTool.isNotNullObj(temp[0]))
				deleteUserImpactFileInfoSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 文件目录 dir_Name
			if (SyncTool.isNotNullObj(temp[1]))
				deleteUserImpactFileInfoSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 登录名称 login_name
			if (SyncTool.isNotNullObj(temp[2]))
				deleteUserImpactFileInfoSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 真实姓名 true_name
			if (SyncTool.isNotNullObj(temp[3]))
				deleteUserImpactFileInfoSheet.addCell(new Label(3, rowNum, temp[3].toString()));
		}

	}

	/**
	 * 添加部门
	 * 
	 * @param addDeptSheet
	 * @param addDeptList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editAddDeptSheet(WritableSheet addDeptSheet, List<Object[]> addDeptList) throws RowsExceededException,
			WriteException {
		// 添加数据
		for (int i = 0; i < addDeptList.size(); i++) {
			Object[] temp = addDeptList.get(i);
			// 行数
			int rowNum = i + 1;
			// 部门编号 dept_num
			if (SyncTool.isNotNullObj(temp[0]))
				addDeptSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 部门名称 dept_name
			if (SyncTool.isNotNullObj(temp[1]))
				addDeptSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 父编号 p_dept_num
			if (SyncTool.isNotNullObj(temp[2]))
				addDeptSheet.addCell(new Label(2, rowNum, temp[2].toString()));
		}
	}

	/**
	 * 添加岗位组
	 * 
	 * @param addPosGroupSheet
	 * @param addPosGroupList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editAddPosGroupSheet(WritableSheet addPosGroupSheet, List<Object[]> addPosGroupList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < addPosGroupList.size(); i++) {
			Object[] temp = addPosGroupList.get(i);
			// 行数
			int rowNum = i + 1;
			// 岗位组编号
			if (SyncTool.isNotNullObj(temp[0]))
				addPosGroupSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 岗位组名称
			if (SyncTool.isNotNullObj(temp[1]))
				addPosGroupSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 父编号
			if (SyncTool.isNotNullObj(temp[2]))
				addPosGroupSheet.addCell(new Label(2, rowNum, temp[2].toString()));
		}
	}

	/**
	 * 添加岗位
	 * 
	 * @param addPosSheet
	 * @param addPosList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editAddPosSheet(WritableSheet addPosSheet, List<Object[]> addPosList) throws RowsExceededException,
			WriteException {
		// 添加数据
		for (int i = 0; i < addPosList.size(); i++) {
			Object[] temp = addPosList.get(i);
			// 行数
			int rowNum = i + 1;

			// 岗位编号 pos_num
			if (SyncTool.isNotNullObj(temp[0]))
				addPosSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 岗位名称 pos_name
			if (SyncTool.isNotNullObj(temp[1]))
				addPosSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 岗位所属部门编号 dept_num
			if (SyncTool.isNotNullObj(temp[2]))
				addPosSheet.addCell(new Label(2, rowNum, temp[2].toString()));
		}
	}

	/**
	 * 添加人员
	 * 
	 * @param addUserSheet
	 * @param addUserList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editAddUserSheet(WritableSheet addUserSheet, List<Object[]> addUserList) throws RowsExceededException,
			WriteException {
		// 添加数据
		for (int i = 0; i < addUserList.size(); i++) {
			Object[] temp = addUserList.get(i);
			// 行数
			int rowNum = i + 1;

			// 人员编号 user_num
			if (SyncTool.isNotNullObj(temp[0]))
				addUserSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 真实姓名 true_name
			if (SyncTool.isNotNullObj(temp[1]))
				addUserSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 任职岗位编号 pos_num
			if (SyncTool.isNotNullObj(temp[2]))
				addUserSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 邮箱 email
			if (SyncTool.isNotNullObj(temp[3]))
				addUserSheet.addCell(new Label(3, rowNum, temp[3].toString()));
			// 内外网邮件标识 email_type 0:内网，1:外网
			if (SyncTool.isNotNullObj(temp[4])) {
				String state = temp[4].toString();
				if ("0".equals(state)) {// 内网
					addUserSheet.addCell(new Label(4, rowNum, INN_EMAIL));
				} else if ("1".equals(state)) {// 外网
					addUserSheet.addCell(new Label(4, rowNum, OUT_EMAIL));
				}
			}
			// 联系电话 phone
			if (SyncTool.isNotNullObj(temp[5]))
				addUserSheet.addCell(new Label(5, rowNum, temp[5].toString()));
		}

	}

	/**
	 * 删除部门
	 * 
	 * @param deleteDeptSheet
	 * @param deleteDeptList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editDeleteDeptSheet(WritableSheet deleteDeptSheet, List<Object[]> deleteDeptList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < deleteDeptList.size(); i++) {
			Object[] temp = deleteDeptList.get(i);
			// 行数
			int rowNum = i + 1;

			// 组织编号 ORG_NUMBER
			if (SyncTool.isNotNullObj(temp[0]))
				deleteDeptSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 组织名称 org_name
			if (SyncTool.isNotNullObj(temp[1]))
				deleteDeptSheet.addCell(new Label(1, rowNum, temp[1].toString()));
		}

	}

	/**
	 * 删除岗位
	 * 
	 * @param deletePosSheet
	 * @param deletePosList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editDeletePosSheet(WritableSheet deletePosSheet, List<Object[]> deletePosList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < deletePosList.size(); i++) {
			Object[] temp = deletePosList.get(i);
			// 行数
			int rowNum = i + 1;

			// 岗位编号 figure_number_id
			if (SyncTool.isNotNullObj(temp[0]))
				deletePosSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 元素名称 figure_text
			if (SyncTool.isNotNullObj(temp[1]))
				deletePosSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 组织编号 org_number_id
			if (SyncTool.isNotNullObj(temp[2]))
				deletePosSheet.addCell(new Label(2, rowNum, temp[2].toString()));
		}
	}

	/**
	 * 删除人员
	 * 
	 * @param deleteUserSheet
	 * @param deleteUserList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editDeleteUserSheet(WritableSheet deleteUserSheet, List<Object[]> deleteUserList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < deleteUserList.size(); i++) {
			Object[] temp = deleteUserList.get(i);
			// 行数
			int rowNum = i + 1;

			// 登录名称 login_name
			if (SyncTool.isNotNullObj(temp[0]))
				deleteUserSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 真实姓名 true_name
			if (SyncTool.isNotNullObj(temp[1]))
				deleteUserSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 邮箱 email
			if (SyncTool.isNotNullObj(temp[2]))
				deleteUserSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 内外网邮件标识 email_type 0:内，1外
			if (SyncTool.isNotNullObj(temp[3])) {
				String state = temp[3].toString();
				if ("inner".equals(state)) {// 内网
					deleteUserSheet.addCell(new Label(3, rowNum, INN_EMAIL));
				} else if ("outer".equals(state)) {// 外网
					deleteUserSheet.addCell(new Label(3, rowNum, OUT_EMAIL));
				}
			}
			// 电话号码 phone_str
			if (SyncTool.isNotNullObj(temp[4]))
				deleteUserSheet.addCell(new Label(4, rowNum, temp[4].toString()));
		}

	}

	/**
	 * 更新部门
	 * 
	 * @param updateDeptSheet
	 * @param updateDeptList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editUpdateDeptSheet(WritableSheet updateDeptSheet, List<Object[]> updateDeptList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < updateDeptList.size(); i++) {
			Object[] temp = updateDeptList.get(i);
			// 行数
			int rowNum = i + 1;

			// 部门编号 dept_num
			if (SyncTool.isNotNullObj(temp[0]))
				updateDeptSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 部门名称 dept_name
			if (SyncTool.isNotNullObj(temp[1]))
				updateDeptSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 父编号 p_dept_num
			if (SyncTool.isNotNullObj(temp[2]))
				updateDeptSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 组织编号 org_number
			if (SyncTool.isNotNullObj(temp[3]))
				updateDeptSheet.addCell(new Label(3, rowNum, temp[3].toString()));
			// 组织名称 org_name
			if (SyncTool.isNotNullObj(temp[4]))
				updateDeptSheet.addCell(new Label(4, rowNum, temp[4].toString()));
			// 上级组织编号 preorg_number
			if (SyncTool.isNotNullObj(temp[5]))
				updateDeptSheet.addCell(new Label(5, rowNum, temp[5].toString()));
		}
	}

	/**
	 * 更新岗位
	 * 
	 * @param updatePosSheet
	 * @param updatePosList
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void editUpdatePosSheet(WritableSheet updatePosSheet, List<Object[]> updatePosList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < updatePosList.size(); i++) {
			Object[] temp = updatePosList.get(i);
			// 行数
			int rowNum = i + 1;

			// 岗位编号 figure_number_id
			if (SyncTool.isNotNullObj(temp[0]))
				updatePosSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 元素名称 figure_text
			if (SyncTool.isNotNullObj(temp[1]))
				updatePosSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 组织编号 org_number_id
			if (SyncTool.isNotNullObj(temp[2]))
				updatePosSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// 组织名称 org_name
			if (SyncTool.isNotNullObj(temp[3]))
				updatePosSheet.addCell(new Label(3, rowNum, temp[3].toString()));
			// 岗位编号 pos_num
			if (SyncTool.isNotNullObj(temp[4]))
				updatePosSheet.addCell(new Label(4, rowNum, temp[4].toString()));
			// 岗位名称 pos_name
			if (SyncTool.isNotNullObj(temp[5]))
				updatePosSheet.addCell(new Label(5, rowNum, temp[5].toString()));
			// 部门编号 dept_num
			if (SyncTool.isNotNullObj(temp[6]))
				updatePosSheet.addCell(new Label(6, rowNum, temp[6].toString()));
			// 部门名称 dept_name
			if (SyncTool.isNotNullObj(temp[7]))
				updatePosSheet.addCell(new Label(7, rowNum, temp[7].toString()));
		}
	}

	/**
	 * 更新人员
	 * 
	 * @param updateUserSheet
	 * @param updateUserList
	 * @throws WriteException
	 * @throws RowsExceededException
	 */
	private void editUpateUserSheet(WritableSheet updateUserSheet, List<Object[]> updateUserList)
			throws RowsExceededException, WriteException {
		// 添加数据
		for (int i = 0; i < updateUserList.size(); i++) {
			Object[] temp = updateUserList.get(i);
			// 行数
			int rowNum = i + 1;

			// 登录名称 login_name
			if (SyncTool.isNotNullObj(temp[0]))
				updateUserSheet.addCell(new Label(0, rowNum, temp[0].toString()));
			// 真实姓名 true_name
			if (SyncTool.isNotNullObj(temp[1]))
				updateUserSheet.addCell(new Label(1, rowNum, temp[1].toString()));
			// 邮箱 email
			if (SyncTool.isNotNullObj(temp[2]))
				updateUserSheet.addCell(new Label(2, rowNum, temp[2].toString()));
			// EMAIL类型 email_type 0:内，1:外
			if (SyncTool.isNotNullObj(temp[3])) {
				String state = temp[3].toString();
				if ("0".equals(state)) {// 内网
					updateUserSheet.addCell(new Label(3, rowNum, INN_EMAIL));
				} else if ("1".equals(state)) {// 外网
					updateUserSheet.addCell(new Label(3, rowNum, OUT_EMAIL));
				}
			}
			// 电话号码 phone_str
			if (SyncTool.isNotNullObj(temp[4]))
				updateUserSheet.addCell(new Label(4, rowNum, temp[4].toString()));

			/** ***************新人员************************** */
			// 登录名称 login_name
			if (SyncTool.isNotNullObj(temp[5]))
				updateUserSheet.addCell(new Label(5, rowNum, temp[5].toString()));
			// 真实姓名 true_name
			if (SyncTool.isNotNullObj(temp[6]))
				updateUserSheet.addCell(new Label(6, rowNum, temp[6].toString()));
			// 邮箱 email
			if (SyncTool.isNotNullObj(temp[7]))
				updateUserSheet.addCell(new Label(7, rowNum, temp[7].toString()));
			// EMAIL类型 email_type 0:内，1:外
			if (SyncTool.isNotNullObj(temp[8])) {
				String state = temp[8].toString();
				if ("0".equals(state)) {// 内网
					updateUserSheet.addCell(new Label(8, rowNum, INN_EMAIL));
				} else if ("1".equals(state)) {// 外网
					updateUserSheet.addCell(new Label(8, rowNum, OUT_EMAIL));
				}
			}
			// 电话号码 phone_str
			if (SyncTool.isNotNullObj(temp[9]))
				updateUserSheet.addCell(new Label(9, rowNum, temp[9].toString()));
		}

	}

	/**
	 * 是否存在变动
	 * 
	 * @param objectList
	 * @return
	 */
	public void initChangeData(List<Object[]> editProcessTaskDeptList1, List<Object[]> editDutyDeptList2,
			List<Object[]> editDuptDeptRuleList3, List<Object[]> editDeptAccessPerList3,
			List<Object[]> editDeleteAffectedList4, List<Object[]> editFlowBasicInfoList5,
			List<Object[]> editFlowRoleWithList6, List<Object[]> editPositionAccessList7,
			List<Object[]> editDeleteUserFlowList8, List<Object[]> editDeleteUserImpactFileList9,
			List<Object[]> editAddDeptList10, List<Object[]> editAddPosList11, List<Object[]> editAddUserList12,
			List<Object[]> editDeleteDeptList13, List<Object[]> editDeletePosList14,
			List<Object[]> editDeleteUserList15, List<Object[]> editUpdateDeptList16,
			List<Object[]> editUpdatePosList17, List<Object[]> editUpdateUserList18, List<Object[]> addPosGroupList,
			List<Object[]> deletePosGroupList, List<Object[]> updatePosGroupList, Long currProjectID) {
		// 是否存在变动
		if (isChange(editProcessTaskDeptList1) || isChange(editDutyDeptList2) || isChange(editDuptDeptRuleList3)
				|| isChange(editDeptAccessPerList3) || isChange(editDeleteAffectedList4)
				|| isChange(editFlowBasicInfoList5) || isChange(editFlowRoleWithList6)
				|| isChange(editPositionAccessList7) || isChange(editDeleteUserFlowList8)
				|| isChange(editDeleteUserImpactFileList9) || isChange(editAddDeptList10) || isChange(editAddPosList11)
				|| isChange(editAddUserList12) || isChange(editDeleteDeptList13) || isChange(editDeletePosList14)
				|| isChange(editDeleteUserList15) || isChange(editUpdateDeptList16) || isChange(editUpdatePosList17)
				|| isChange(editUpdateUserList18) || isChange(addPosGroupList) || isChange(deletePosGroupList)
				|| isChange(updatePosGroupList)) {
			hasChange = true;
		} else {
			hasChange = false;
		}
	}

	/**
	 * 是否存在变动
	 * 
	 * @param objectList
	 *            有值，存在变动
	 * @return true :存在变动
	 */
	private boolean isChange(List<Object[]> objectList) {
		if (objectList != null && objectList.size() > 0) {
			return true;
		}
		return false;
	}

	public void setHasChange(boolean hasChange) {
		this.hasChange = hasChange;
	}

	public boolean isHasChange() {
		return hasChange;
	}

	public IExportDataOfPersonnelChangesDao getExportDataOfPersonnelChangesDao() {
		return exportDataOfPersonnelChangesDao;
	}

	public void setExportDataOfPersonnelChangesDao(IExportDataOfPersonnelChangesDao exportDataOfPersonnelChangesDao) {
		this.exportDataOfPersonnelChangesDao = exportDataOfPersonnelChangesDao;
	}

	public String getOutputExcelDataPath() {
		return outputExcelDataPath;
	}
}
