package com.jecn.epros.server.webBean.reports;

public class ProcessDetailLevel {
	/** 流程编号*/
	private String levelNum;
	/** 流程名称*/
	private String levelName;
	public String getLevelNum() {
		return levelNum;
	}
	public void setLevelNum(String levelNum) {
		this.levelNum = levelNum;
	}
	public String getLevelName() {
		return levelName;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	

}
