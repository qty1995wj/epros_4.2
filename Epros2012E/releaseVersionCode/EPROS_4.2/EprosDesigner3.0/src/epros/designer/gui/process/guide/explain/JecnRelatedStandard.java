package epros.designer.gui.process.guide.explain;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.standard.StandardChooseDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;

/**
 * 相关标准
 * 
 * @author user
 * 
 */
public class JecnRelatedStandard extends JecnFileDescriptionTable {

	private Logger log = Logger.getLogger(JecnRelatedStandard.class);
	protected Long relatedId = null;
	// 制度选择按钮初始化
	protected JButton selectBut = new JButton(JecnProperties.getValue("select"));
	protected List<JecnTreeBean> listStrands = new ArrayList<JecnTreeBean>();
	protected List<Long> listIds = new ArrayList<Long>();

	public JecnRelatedStandard(int index, String name, boolean isRequest, JecnPanel contentPanel, Long relatedId,
			TreeNodeType nodeType) {
		this(index, name, isRequest, contentPanel, relatedId, nodeType, "");
	}

	public JecnRelatedStandard(int index, String name, boolean isRequest, JecnPanel contentPanel, Long relatedId,
			TreeNodeType nodeType, String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.relatedId = relatedId;
		titlePanel.add(selectBut);
		try {
			if (relatedId != null) {
				if (TreeNodeType.process == nodeType) {
					listStrands = ConnectionPool.getStandardAction().getStandardByFlowId(relatedId);
				} else if (TreeNodeType.ruleModeFile == nodeType) {
					listStrands = ConnectionPool.getJecnRiskAction().findJecnRuleStandardBeanTByRuleId(relatedId);
				}
			}

		} catch (Exception e) {
			log.error("JecnRelatedStandard is error", e);
		}
		selectBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				select();
			}
		});

		initTable();
		// 设置列宽
		setColumnToFixedCenter(1);
	}

	protected void select() {
		StandardChooseDialog chooseDialog = new StandardChooseDialog(listStrands, 0);
		chooseDialog.setVisible(true);
		if (chooseDialog.isOperation()) {// 操作
			Vector<Vector<String>> data = ((JecnTableModel) getTable().getModel()).getDataVector();
			if (listStrands != null && listStrands.size() > 0) {
				// 获取标准对应目录集合（目录由‘/’ 拼接）
				Map<Long, String> parentNameMap = getParentNameMap(listStrands);
				if (data != null) {
					data.clear();
				}
				for (JecnTreeBean jecnTreeBean : listStrands) {
					((DefaultTableModel) table.getModel()).addRow(this.getRowData(jecnTreeBean, parentNameMap));
				}
			} else {
				// 清空表格
				((DefaultTableModel) table.getModel()).setRowCount(0);
			}
		}
	}

	@Override
	protected void dbClickMethod() {
		select();
	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		if (JecnConfigTool.isBDFOperType()) {
			title.add(JecnProperties.getValue("fileName"));
		} else {
			title.add(JecnProperties.getValue("standardName"));
		}
		title.add(JecnProperties.getValue("dir"));
		// title.add(JecnProperties.getValue("type"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		// 相关标准
		Vector<Vector<String>> orderContent = new Vector<Vector<String>>();
		// 获取标准对应目录集合（目录由‘/’ 拼接）
		Map<Long, String> parentNameMap = getParentNameMap(listStrands);
		for (JecnTreeBean jecnTreeBean : listStrands) {
			listIds.add(jecnTreeBean.getId());
			orderContent.add(getRowData(jecnTreeBean, parentNameMap));
		}
		return orderContent;
	}

	private Map<Long, String> getParentNameMap(List<JecnTreeBean> listStrands) {
		List<Long> standardIds = new ArrayList<Long>();
		for (JecnTreeBean treeBean : listStrands) {
			standardIds.add(treeBean.getId());
		}
		if (standardIds.isEmpty()) {
			return new HashMap<Long, String>();
		}
		try {
			return ConnectionPool.getProcessAction().getNodeParentById(standardIds, false, TreeNodeType.standard);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("JecnRelatedStandard getParentNameMap is error!", e);
		}
		return new HashMap<Long, String>();
	}

	private Vector<String> getRowData(JecnTreeBean jecnTreeBean, Map<Long, String> parentNameMap) {
		Vector<String> vector = new Vector<String>();
		vector.add(String.valueOf(jecnTreeBean.getId()));
		// 标准名称
		vector.add(jecnTreeBean.getName());
		// 标准目录
		vector.add(parentNameMap.get(jecnTreeBean.getId()));
		// 标准类型
		// vector.add(JecnDesignerCommon.standardTypeVal(jecnTreeBean.getTreeNodeType()));
		return vector;
	}

	@Override
	protected int[] gethiddenCols() {
		if (JecnConfigTool.isBDFOperType()) {
			return new int[] { 0, 2 };
		} else {
			return new int[] { 0 };
		}

	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return JecnUtil.isChangeListLong(getResultListLong(), listIds);
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

	public Set<Long> getResultIds() {
		Set<Long> set = new HashSet<Long>();
		for (JecnTreeBean jecnTreeBean : listStrands) {
			set.add(jecnTreeBean.getId());
		}
		return set;
	}
}
