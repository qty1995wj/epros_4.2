/**
 * StartWorkflow_S.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class StartWorkflow_S  implements java.io.Serializable {
    private java.lang.String instanceName;

    private java.lang.String workflowCode;

    private java.lang.String userCode;

    private java.lang.String finishStart;

    private java.lang.String paramValues;

    public StartWorkflow_S() {
    }

    public StartWorkflow_S(
           java.lang.String instanceName,
           java.lang.String workflowCode,
           java.lang.String userCode,
           java.lang.String finishStart,
           java.lang.String paramValues) {
           this.instanceName = instanceName;
           this.workflowCode = workflowCode;
           this.userCode = userCode;
           this.finishStart = finishStart;
           this.paramValues = paramValues;
    }


    /**
     * Gets the instanceName value for this StartWorkflow_S.
     * 
     * @return instanceName
     */
    public java.lang.String getInstanceName() {
        return instanceName;
    }


    /**
     * Sets the instanceName value for this StartWorkflow_S.
     * 
     * @param instanceName
     */
    public void setInstanceName(java.lang.String instanceName) {
        this.instanceName = instanceName;
    }


    /**
     * Gets the workflowCode value for this StartWorkflow_S.
     * 
     * @return workflowCode
     */
    public java.lang.String getWorkflowCode() {
        return workflowCode;
    }


    /**
     * Sets the workflowCode value for this StartWorkflow_S.
     * 
     * @param workflowCode
     */
    public void setWorkflowCode(java.lang.String workflowCode) {
        this.workflowCode = workflowCode;
    }


    /**
     * Gets the userCode value for this StartWorkflow_S.
     * 
     * @return userCode
     */
    public java.lang.String getUserCode() {
        return userCode;
    }


    /**
     * Sets the userCode value for this StartWorkflow_S.
     * 
     * @param userCode
     */
    public void setUserCode(java.lang.String userCode) {
        this.userCode = userCode;
    }


    /**
     * Gets the finishStart value for this StartWorkflow_S.
     * 
     * @return finishStart
     */
    public java.lang.String getFinishStart() {
        return finishStart;
    }


    /**
     * Sets the finishStart value for this StartWorkflow_S.
     * 
     * @param finishStart
     */
    public void setFinishStart(java.lang.String finishStart) {
        this.finishStart = finishStart;
    }


    /**
     * Gets the paramValues value for this StartWorkflow_S.
     * 
     * @return paramValues
     */
    public java.lang.String getParamValues() {
        return paramValues;
    }


    /**
     * Sets the paramValues value for this StartWorkflow_S.
     * 
     * @param paramValues
     */
    public void setParamValues(java.lang.String paramValues) {
        this.paramValues = paramValues;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StartWorkflow_S)) return false;
        StartWorkflow_S other = (StartWorkflow_S) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.instanceName==null && other.getInstanceName()==null) || 
             (this.instanceName!=null &&
              this.instanceName.equals(other.getInstanceName()))) &&
            ((this.workflowCode==null && other.getWorkflowCode()==null) || 
             (this.workflowCode!=null &&
              this.workflowCode.equals(other.getWorkflowCode()))) &&
            ((this.userCode==null && other.getUserCode()==null) || 
             (this.userCode!=null &&
              this.userCode.equals(other.getUserCode()))) &&
            ((this.finishStart==null && other.getFinishStart()==null) || 
             (this.finishStart!=null &&
              this.finishStart.equals(other.getFinishStart()))) &&
            ((this.paramValues==null && other.getParamValues()==null) || 
             (this.paramValues!=null &&
              this.paramValues.equals(other.getParamValues())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInstanceName() != null) {
            _hashCode += getInstanceName().hashCode();
        }
        if (getWorkflowCode() != null) {
            _hashCode += getWorkflowCode().hashCode();
        }
        if (getUserCode() != null) {
            _hashCode += getUserCode().hashCode();
        }
        if (getFinishStart() != null) {
            _hashCode += getFinishStart().hashCode();
        }
        if (getParamValues() != null) {
            _hashCode += getParamValues().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StartWorkflow_S.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">StartWorkflow_S"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instanceName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "instanceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflowCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "workflowCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "userCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("finishStart");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "finishStart"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paramValues");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "paramValues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
