package epros.draw.gui.top.toolbar.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.designer.JecnFigureFileTBean;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnFigureRefManhattanLine;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.gui.workflow.util.JecnRemoveFlowElementUnit;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 
 * 流程地图排列 右键菜单
 */
public class JecnReplaceProcessMapPopupMenu extends JMenu implements ActionListener {
	/** 四边形 (矩形) */
	private JMenuItem flowMapFunctionField;
	/** 五边形 */
	private JMenuItem flowMapPentagon;
	/** 六边形 */
	private JMenuItem flowMapRightHexagon;
	/** 三角形 */
	private JMenuItem flowMapTriangle;
	/** 梯形 */
	private JMenuItem flowMapIsoscelesTrapezoidFigure;
	/** 手动输入 */
	private JMenuItem flowMapInputHandFigure;
	/** 椭圆 */
	private JMenuItem flowMapOval;

	public JecnReplaceProcessMapPopupMenu() {
		// JecnResourceUtil.getJecnResourceUtil().getValue("activityReplacement")
		this.setText(JecnResourceUtil.getJecnResourceUtil().getValue("elementReplacement"));// 活动置换
		this.setIcon(JecnFileUtil.getReplaceImageIcon("replaceEle"));

		// 矩形
		flowMapFunctionField = new JMenuItem();
		flowMapFunctionField.setText(JecnResourceUtil.getJecnResourceUtil().getValue("FunctionField"));
		// 添加事件监听
		flowMapFunctionField.addActionListener(this);
		flowMapFunctionField.setName(MapElemType.FunctionField.toString());
		flowMapFunctionField.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.FunctionField.toString()));

		// 五边形
		flowMapPentagon = new JMenuItem();
		flowMapPentagon.setText(JecnResourceUtil.getJecnResourceUtil().getValue("Pentagon"));
		// 添加事件监听
		flowMapPentagon.addActionListener(this);
		flowMapPentagon.setName(MapElemType.Pentagon.toString());
		flowMapPentagon.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.Pentagon.toString()));

		// 六边形
		flowMapRightHexagon = new JMenuItem();
		flowMapRightHexagon.setText(JecnResourceUtil.getJecnResourceUtil().getValue("RightHexagon"));
		// 添加事件监听
		flowMapRightHexagon.addActionListener(this);
		flowMapRightHexagon.setName(MapElemType.RightHexagon.toString());
		flowMapRightHexagon.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.RightHexagon.toString()));

		// 三角形
		flowMapTriangle = new JMenuItem();
		flowMapTriangle.setText(JecnResourceUtil.getJecnResourceUtil().getValue("Triangle"));
		// 添加事件监听
		flowMapTriangle.addActionListener(this);
		flowMapTriangle.setName(MapElemType.Triangle.toString());
		flowMapTriangle.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.Triangle.toString()));

		// 梯形
		flowMapIsoscelesTrapezoidFigure = new JMenuItem();
		flowMapIsoscelesTrapezoidFigure.setText(JecnResourceUtil.getJecnResourceUtil().getValue("trapezoid"));
		// 添加事件监听
		flowMapIsoscelesTrapezoidFigure.addActionListener(this);
		flowMapIsoscelesTrapezoidFigure.setName(MapElemType.IsoscelesTrapezoidFigure.toString());
		flowMapIsoscelesTrapezoidFigure.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.IsoscelesTrapezoidFigure
				.toString()));

		// 手动输入
		flowMapInputHandFigure = new JMenuItem();
		flowMapInputHandFigure.setText(JecnResourceUtil.getJecnResourceUtil().getValue("manualInput"));
		// 添加事件监听
		flowMapInputHandFigure.addActionListener(this);
		flowMapInputHandFigure.setName(MapElemType.InputHandFigure.toString());
		flowMapInputHandFigure.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.InputHandFigure.toString()));

		// 椭圆
		flowMapOval = new JMenuItem();
		flowMapOval.setText(JecnResourceUtil.getJecnResourceUtil().getValue("ellipse"));
		// 添加事件监听
		flowMapOval.addActionListener(this);
		flowMapOval.setName(MapElemType.Oval.toString());
		flowMapOval.setIcon(JecnFileUtil.getReplaceImageIcon(MapElemType.Oval.toString()));

		// 流程图显示节点
		List<MapElemType> showTypeList = JecnDesignerProcess.getDesignerProcess().getShowElementsType(1);
		if (showTypeList.contains(MapElemType.FunctionField)) {
			this.add(flowMapFunctionField);
		}
		if (showTypeList.contains(MapElemType.Pentagon)) {
			this.add(flowMapPentagon);
		}
		if (showTypeList.contains(MapElemType.RightHexagon)) {
			this.add(flowMapRightHexagon);
		}
		if (showTypeList.contains(MapElemType.Triangle)) {
			this.add(flowMapTriangle);
		}
		if (showTypeList.contains(MapElemType.IsoscelesTrapezoidFigure)) {
			this.add(flowMapIsoscelesTrapezoidFigure);
		}
		if (showTypeList.contains(MapElemType.Oval)) {
			this.add(flowMapOval);
		}
		if (showTypeList.contains(MapElemType.InputHandFigure)) {
			this.add(flowMapInputHandFigure);
		}
	}

	public void actionPerformed(ActionEvent e) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}
		// 选中的数据
		List<JecnBaseFlowElementPanel> jecnBaseFlowElementPanel = desktopPane.getCurrentSelectElement();
		for (JecnBaseFlowElementPanel baseBean : jecnBaseFlowElementPanel) {
			MapElemType mapElemType = baseBean.getFlowElementData().getMapElemType();
			if (mapElemType != mapElemType.Oval && mapElemType != mapElemType.Triangle
					&& mapElemType != mapElemType.FunctionField && mapElemType != mapElemType.Pentagon
					&& mapElemType != mapElemType.RightHexagon && mapElemType != mapElemType.InputHandFigure
					&& mapElemType != mapElemType.IsoscelesTrapezoidFigure) {
				JecnOptionPane.showMessageDialog(null, JecnResourceUtil.getJecnResourceUtil().getValue(mapElemType) + " "
						+ JecnResourceUtil.getJecnResourceUtil().getValue("nonReplaceable"));
				return;
			}
		}
		// 操作前数据
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();
		undoData.recodeFlowElement(jecnBaseFlowElementPanel);
		List<JecnBaseFlowElementPanel> jecnBaseOldFlowElementPanel = new ArrayList<JecnBaseFlowElementPanel>();
		if (e.getSource() instanceof JMenuItem) {
			for (JecnBaseFlowElementPanel selectFigurebean : jecnBaseFlowElementPanel) {
				// 获取JMenuItem
				JMenuItem menuItem = (JMenuItem) e.getSource();
				String menuName = menuItem.getName();

				// 创建元素
				JecnBaseFigurePanel newFigure = (JecnBaseFigurePanel) JecnCreateFlowElement
						.createFlowElementObj(MapElemType.valueOf(menuName));

				JecnBaseFigurePanel selectFigure = (JecnBaseFigurePanel) selectFigurebean;
				jecnBaseOldFlowElementPanel.add(newFigure);

				JecnRemoveFlowElementUnit.removeReplaceFigure(selectFigure);

				newFigure.getFlowElementData().setFigureArrts(selectFigure.getFlowElementData(),
						newFigure.getFlowElementData());

				// 流程元素文字内容
				newFigure.getFlowElementData().setFigureText(selectFigure.getFlowElementData().getFigureText());
				// 说明
				newFigure.getFlowElementData().setAvtivityShow(selectFigure.getFlowElementData().getAvtivityShow());
				// 设计器数据克隆
				newFigure.getFlowElementData().setDesignerFigureData(
						selectFigure.getFlowElementData().getDesignerFigureData().clone());

				JecnFigureData src = (JecnFigureData) selectFigure.getFlowElementData();
				JecnFigureData dest = (JecnFigureData) newFigure.getFlowElementData();
				if (src.getListFigureFileTBean() != null) {
					List<JecnFigureFileTBean> listFigureFileTBean = new ArrayList<JecnFigureFileTBean>();
					for (JecnFigureFileTBean figureFileTBean : src.getListFigureFileTBean()) {
						listFigureFileTBean.add(figureFileTBean.clone());
					}
					dest.setListFigureFileTBean(listFigureFileTBean);
				}

				JecnAddFlowElementUnit.addFigure(selectFigure.getLocation(), newFigure);

				// 设置连接线 开始图形和结束图形
				repalceLineActive(selectFigure, newFigure);

				// 重画图形相关的连接线
				List<JecnBaseFigurePanel> figureList = new ArrayList<JecnBaseFigurePanel>();
				figureList.add(newFigure);
				JecnDraggedFiguresPaintLine.paintLineByFigures(figureList);

				JecnDesignerProcess.getDesignerProcess().addElemLink(newFigure, null);

				// 清空虚拟图形
				desktopPane.updateUI();
			}
			redoData.recodeFlowElement(jecnBaseOldFlowElementPanel);
			JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createDeleteAndAdd(undoData, redoData);
			desktopPane.getUndoRedoManager().addEdit(unredoProcess);
		}

	}

	public static void repalceLineActive(JecnBaseFigurePanel selectFigure, JecnBaseFigurePanel newFigure) {
		// 连接线(记录选中的连接线集合,)
		Set<JecnBaseManhattanLinePanel> manLineSet = new HashSet<JecnBaseManhattanLinePanel>();
		// 获取当前选中图形或线
		// 图形关联的开始连接线
		JecnDraggedFiguresPaintLine.getManhattanLineList(manLineSet, selectFigure);
		List<JecnFigureRefManhattanLine> listRefLine = selectFigure.getListFigureRefManhattanLine();
		// *******************撤销恢复*******************//

		JecnFigureRefManhattanLine cloneLine = null;
		for (JecnFigureRefManhattanLine figureRefManhattanLine : listRefLine) {// 图形相关联线
			JecnBaseManhattanLinePanel manLinePanel = figureRefManhattanLine.getManhattanLinePanel();
			if (manLinePanel.getStartFigure() == selectFigure) {
				manLinePanel.setStartFigure(newFigure);
				manLinePanel.getFlowElementData().setStartId(newFigure.getFlowElementData().getFlowElementId());
				manLinePanel.getFlowElementData().setStartFigureUUID(
						newFigure.getFlowElementData().getFlowElementUUID());
				cloneLine = new JecnFigureRefManhattanLine(manLinePanel, figureRefManhattanLine.getEditPointType(),
						true);

				// 设置元素开始元素和结束元素
				manLinePanel.getFlowElementData().setStartFigureData(newFigure.getFlowElementData());
			} else if (manLinePanel.getEndFigure() == selectFigure) {
				manLinePanel.setEndFigure(newFigure);
				manLinePanel.getFlowElementData().setEndId(newFigure.getFlowElementData().getFlowElementId());
				manLinePanel.getFlowElementData().setEndFigureUUID(newFigure.getFlowElementData().getFlowElementUUID());
				cloneLine = new JecnFigureRefManhattanLine(manLinePanel, figureRefManhattanLine.getEditPointType(),
						false);

				// 设置元素开始元素和结束元素
				manLinePanel.getFlowElementData().setEndFigureData(newFigure.getFlowElementData());
			}
			newFigure.getListFigureRefManhattanLine().add(cloneLine);
		}
	}

}
