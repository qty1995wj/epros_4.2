package com.jecn.epros.server.webBean.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProcessScanWebListBean implements Serializable {
	
	/** 责任部门下的流程审视的详细*/
	private List<ProcessScanWebBean> allScanWebBeans=new ArrayList<ProcessScanWebBean>();
	/** 开始时间*/
	private String startTime;
	/** 结束时间*/
	private String endTime;
	/** 责任部门id*/
	private long id;
	/** 责任部门 name*/
	private String name;
	/** 在excel sheet中显示的name*/
	private String sheetName;
	/** 责任部门下完成的审视的流程数量*/
	private int scanCount=0;
	/** 责任部门下未审视的流程数量*/
	private int notScanCount=0;
	/** 责任部门下所有需要审视的流程数量*/
	private int allScanCount=0;
	/** 流程审视比例  审视完成的除以所有的*/
	private int scanPercent=0;
	/** 责任部门下次需要审视的流程数量 接下来的两个月*/
	private int nextTimeScanCount=0;
	/** 0为责任部门 1为流程地图*/
	private int type;
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public int getNextTimeScanCount() {
		return nextTimeScanCount;
	}
	public void setNextTimeScanCount(int nextTimeScanCount) {
		this.nextTimeScanCount = nextTimeScanCount;
	}
	public List<ProcessScanWebBean> getAllScanWebBeans() {
		return allScanWebBeans;
	}
	public void setAllScanWebBeans(List<ProcessScanWebBean> allScanWebBeans) {
		this.allScanWebBeans = allScanWebBeans;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getScanCount() {
		return scanCount;
	}
	public void setScanCount(int scanCount) {
		this.scanCount = scanCount;
	}
	public int getNotScanCount() {
		return notScanCount;
	}
	public void setNotScanCount(int notScanCount) {
		this.notScanCount = notScanCount;
	}
	public int getAllScanCount() {
		return allScanCount;
	}
	public void setAllScanCount(int allScanCount) {
		this.allScanCount = allScanCount;
	}
	public int getScanPercent() {
		return scanPercent;
	}
	public void setScanPercent(int scanPercent) {
		this.scanPercent = scanPercent;
	}
	
}