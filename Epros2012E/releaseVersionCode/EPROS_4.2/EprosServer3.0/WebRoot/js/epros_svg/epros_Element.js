var rebuildConnectingLinePath = function (line) {
    var diff = 7,
        right = line.startX < line.endX,
        d = "";

    $.each(line.crossOverPoints, function (i, item) {
        if (i == 0) {
            d += " M" + line.startX + "," + line.startY;
            d += " L" + (item[0] + (right ? -diff : diff)) + "," + item[1];
        } else {
            d += " H" + (item[0] + (right ? -diff : diff));
        }
        d += " M" + (item[0] + (right ? -diff : diff)) + "," + item[1];
        d += " Q" + item[0] + " " + (item[1] - diff * 2) + "," + (item[0] + (right ? diff : -diff )) + " " + item[1];
    });
    d += " H" + line.endX;
    return d;
};

var eprosGraphTypes = {
        // 连接线
        CommonLine: function () {
            var d = "",
                lineData = _d.lines;

            //  有数据情况下,生成path
            if (lineData.length) {
                for (var i = 0; i < lineData.length; i++) {
                    if (lineData[i].crossOverPoints.length) {
                        d += rebuildConnectingLinePath(lineData[i]);
                    } else if (i == 0) {
                        if (lineData[0].startX == lineData[0].endX) {
                            lineData[0].startY < lineData[0].endY ? (lineData[0].startY -= 1) : (lineData[0].startY += 1);
                        }
                        if (lineData[0].startY == lineData[0].endY) {
                            lineData[0].startX < lineData[0].endX ? (lineData[0].startX -= 1) : (lineData[0].startX += 1);
                        }
                        d += " M" + lineData[0].startX + "," + lineData[0].startY;
                        d += " L" + lineData[0].endX + "," + lineData[0].endY;
                    } else if (lineData[i].startX === lineData[i].endX) {
                        d += " V" + lineData[i].endY;
                    } else if (lineData[i].startY === lineData[i].endY) {
                        d += " H" + lineData[i].endX;
                    }
                }
                return paper.path(d);
            }
        },

        //  曼哈顿线
        ManhattanLine: function () {
            var linePath = eprosGraphTypes.CommonLine(),
            // -w:[number] -h:[number] 修改了raphael.js addArrow方法的源码 w:width h:height
                arrowEnd = "-w:9-h:9";
            if (_d.graphStyle["stroke-width"] !== "1") {
                arrowEnd = "-w:5-h:5";
            }
            if (linePath) {
                return linePath.attr({"arrow-end": "block" + arrowEnd});
            }
        },

        //  单向箭头
        OneArrowLine: function () {
            var d = "M" + _d.x + "," + (_d.y + _d.height * 0.4);
            d += " H" + (_d.x + _d.width * 0.8);
            d += " V" + (_d.y + _d.height * 0.2);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
            d += " L" + (_d.x + _d.width * 0.8) + "," + (_d.y + _d.height * 0.8);
            d += " V" + (_d.y + _d.height * 0.6);
            d += " H" + _d.x + "Z";

            return paper.path(d);
        },

        //  双向箭头
        TwoArrowLine: function () {
            var d = "M" + _d.x + "," + (_d.y + _d.height * 0.5);
            d += " L" + (_d.x + _d.width * 0.2) + "," + (_d.y + _d.height * 0.2);
            d += " V" + (_d.y + _d.height * 0.4);
            d += " H" + (_d.x + _d.width * 0.8);
            d += " V" + (_d.y + _d.height * 0.2);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
            d += " L" + (_d.x + _d.width * 0.8) + "," + (_d.y + _d.height * 0.8);
            d += " V" + (_d.y + _d.height * 0.6);
            d += " H" + (_d.x + _d.width * 0.2);
            d += " V" + (_d.y + _d.height * 0.8);
            d += " L" + _d.x + "," + (_d.y + _d.height * 0.5);

            return paper.path(d);
        },

        // 分割线
        DividingLine: function () {
            return paper.path("M" + (_d.startX) + "," + (_d.startY) + " L" + (_d.endX) + "," + (_d.endY));
        },

        // 横线
        VDividingLine: function () {
            return eprosGraphTypes.DividingLine();
        },

        // 纵线
        HDividingLine: function () {
            return eprosGraphTypes.DividingLine();
        },

        // 横分割线
        ParallelLines: function () {
            return paper.path("M" + _d.startX + "," + _d.startY + " H" + _c.paperWidth).data("type", "DividingLine");
        },

        // 纵分割线
        VerticalLine: function () {
            return paper.path("M" + _d.startX + "," + _d.startY + " V" + _c.paperHeight).data("type", "DividingLine");
        },


        // 流程图:子流程
        OvalSubFlowFigure: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // 流程地图:椭圆
        Oval: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // 流程地图 三角形
        Triangle: function () {
            var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
            d += " L" + _d.x + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
            return paper.path(d);
        },

        // 流程地图 矩形功能域
        FunctionField: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height);
        },

        // 五边形
        Pentagon: function () {
            var d = "M" + _d.x + "," + _d.y;
            d += " H" + (_d.x + _d.width - 19);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
            d += " L" + (_d.x + _d.width - 19) + "," + (_d.y + _d.height);
            d += " H" + _d.x;
            d += " V" + _d.y;

            return paper.path(d);
        },
        // 箭头六边形
        RightHexagon: function () {
            var d = "M" + _d.x + "," + _d.y;
            d += " L" + (_d.x + ( _d.width - 19)) + "," + _d.y;
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
            d += " L" + (_d.x + (_d.width - 19)) + "," + (_d.y + _d.height);
            d += " L" + _d.x + "," + (_d.y + _d.height);
            d += " L" + (_d.x + 19) + "," + (_d.y + _d.height * 0.5);
            d += " L" + _d.x + "," + _d.y;

            return paper.path(d);
        },

        // 泳池
        ModelFigure: function () {
            var leftRectWidth = Number(_d.specialData),
                fillColor = _d.graphStyle.fill.split("-"),
                leftRect = paper.rect(_d.x, _d.y, leftRectWidth, _d.height),
                rightRect = paper.rect(_d.x + leftRectWidth, _d.y, _d.width - leftRectWidth, _d.height),
                set = paper.set();
            set.push(leftRect, rightRect).attr(_d.graphStyle);
            leftRect.attr("fill", "180" + "-" + fillColor[1] + ":50" + "-" + fillColor[2]);
            return set;
        },

        // 协作框
        DottedRect: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height);
        },

        // 自由文本框
        FreeText: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height);
        },

        // 时间框
        DateFreeText: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height);
        },

        // 注释框
        CommentText: function () {
            var singerLine = _d.specialData.split(","),
                d;
            d = "M" + singerLine[0] + "," + singerLine[1];
            d += " L" + _d.x + "," + (_d.y + _d.height / 2);
            paper.path(d).toBack();
            d = "  M" + (_d.x + 10) + "," + _d.y;
            d += " H" + (_d.x);
            d += " V" + (_d.y + _d.height);
            d += " H" + (_d.x + 10);
            return paper.path(d);
        },

        // 阶段分隔符
        PartitionFigure: function () {
            var set = paper.set(),
                headD,
                footD;
            headD = "M" + _d.x + "," + _d.y;
            headD += " L" + (_d.x + _d.width) + "," + (_d.y + 17.5);
            headD += " L" + _d.x + "," + (_d.y + 35);
            footD = "M" + _d.x + "," + (_d.y + 35);
            footD += " V" + (_d.y + _d.height);
            set.push(
                paper.path(headD).attr("stroke-width", 3),
                paper.path(footD).attr({
                    "stroke-dasharray": "- ",
                    "stroke-width": 1.5
                })
            );
            return set;
        },

        // 图标插入框
        IconFigure: function () {
            if (_d.specialData) {
                return paper.image("writefileByte.action?isPub=false&fileId=" + _d.specialData, _d.x, _d.y, _d.width, _d.height);
            } else {
                return paper.rect(_d.x, _d.y, _d.width, _d.height);
            }
        },

        // 手动输入
        InputHandFigure: function () {
            var d = "M" + _d.x + "," + (_d.y + _d.height * 0.3);
            d += " L" + (_d.x + _d.width) + "," + _d.y;
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
            d += " L" + _d.x + "," + (_d.y + _d.height);
            d += " L" + _d.x + "," + (_d.y + _d.height * 0.3);
            return paper.path(d);
        },

        // 制度
        SystemFigure: function () {
            var d = "M" + _d.x + "," + _d.y;
            d += " H" + (_d.x + _d.width * 0.85);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.5);
            d += " L" + (_d.x + _d.width * 0.85) + "," + (_d.y + _d.height);
            d += " H" + _d.x;
            d += " L" + _d.x + "," + _d.y;

            return paper.path(d);
        },

        // 活动
        RoundRectWithLine: function () {
            // 横向直线
            var hLineStartX = _d.x,
                hLineEndX = _d.x + _d.width,
                hLineY = _d.y + +_d.height / 3,
                roundRect = paper.set(),
                rect,
                hLine;
            roundRect.push(
                rect = paper.rect(_d.x, _d.y, _d.width, _d.height, 10),
                hLine = paper.path("M" + hLineStartX + "," + hLineY + "L" + hLineEndX + "," + hLineY)
            );
            return roundRect;
        },

        // 角色
        RoleFigure: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height);
        },

        // 客户
        CustomFigure: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height);
        },

        // 返入
        ReverseArrowhead: function () {
            var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
            d += " V" + (_d.y + _d.height / 2);
            d += " L" + _d.x + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height / 2);
            return paper.path(d);
        },

        // 返出
        RightArrowhead: function () {
            var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
            d += " V" + (_d.y + _d.height / 2);
            d += " H" + _d.x;
            d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
            d += " H" + (_d.x + _d.width / 2);
            return paper.path(d);
        },

        // 决策
        Rhombus: function () {
            var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
            d += " L" + _d.x + "," + (_d.y + _d.height / 2);
            d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
            d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
            return paper.path(d);
        },

        // 事件
        EventFigure: function () {
            var d = "M" + _d.x + "," + (_d.y + _d.height / 2);
            d += " L" + (_d.x + _d.width * 0.2) + "," + _d.y;
            d += " H" + (_d.x + _d.width * 0.8);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
            d += " L" + (_d.x + _d.width * 0.8) + "," + (_d.y + _d.height);
            d += " H" + (_d.x + _d.width * 0.2);
            d += " L" + _d.x + "," + (_d.y + _d.height / 2);
            return paper.path(d);
        },

        // 接口
        ImplFigure: function () {
            var set = paper.set(),
                d;
            d = "M" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.6);
            d += " L" + (_d.x + _d.width - 10) + "," + (_d.y + _d.height * 0.2);
            d += " H" + (_d.x + _d.width * 0.2 + 10);
            d += " L" + (_d.x + _d.width * 0.2) + "," + (_d.y + _d.height * 0.6);
            d += " L" + (_d.x + _d.width * 0.2 + 10) + "," + (_d.y + _d.height);
            d += " H" + (_d.x + _d.width - 10);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.6);

            set.push(
                paper.path(d),
                paper.rect(_d.x, _d.y, _d.width * 0.7, _d.height * 0.8, 7)
            );
            return set;
        },

        // 文档
        FileImage: function () {
            var d = "M" + _d.x + "," + _d.y;
            d += " H" + (_d.x + _d.width);
            d += " V" + (_d.y + _d.height * 0.85);
            d += " M" + _d.x + "," + _d.y;
            d += " V" + (_d.y + _d.height * 0.85);
            d += " Q" + (_d.x + _d.width * 0.2) + ","
                + (_d.y + _d.height * 1.1) + " "
                + (_d.x + _d.width * 0.5) + "," + (_d.y + _d.height * 0.85);
            d += " S" + (_d.x + _d.width * 0.8) + ","
                + (_d.y + _d.height * 0.5) + " "
                + (_d.x + _d.width) + "," + (_d.y + _d.height * 0.85);

            return paper.path(d);
        },

        // 信息系统
        DataImage: function () {
            var set = paper.set(),
                d = "M" + _d.x + "," + (_d.y + 15);
            d += " V" + (_d.y + _d.height - 12);
            d += " C" + (_d.x + _d.width * 0.1) + " " + (_d.y + _d.height) + "," + (_d.x + _d.width * 0.9) + " " + (_d.y + _d.height) + "," + (_d.x + _d.width) + " " + (_d.y + _d.height - 12);
            d += " V" + (_d.y + 15);
            set.push(
                paper.path(d),
                paper.ellipse(_d.x + _d.width / 2, _d.y + 15, _d.width / 2, 10)
            );
            return set;
        },

        // 风险点
        RiskPointFigure: function () {
            var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
            d += " L" + _d.x + "," + (_d.y + _d.height / 2);
            d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
            d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
            return paper.path(d);
        },

        // 关键控制点
        KeyPointFigure: function () {
            var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
            d += " L" + _d.x + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
            return paper.path(d);
        },

        // 跳转:转出
        ToFigure: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // 跳转:转入
        FromFigure: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // 结束
        EndFigure: function () {
            var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
            d += " L" + _d.x + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
            return paper.path(d);
        },

        // PA:问题区域
        PAFigure: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // KSF:关键成功因素
        KSFFigure: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // KCP:关键控制点
        KCPFigure: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // XOR:多种情况下只能选中一种
        XORFigure: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // OR:多种情况下只能选中一种或多种
        ORFigure: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // 流程开始
        FlowFigureStart: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height, 30);
        },

        // 流程结束
        FlowFigureStop: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height, 30);
        },

        // AND
        AndFigure: function () {
            return paper.ellipse(_d.x + _d.width / 2, _d.y + _d.height / 2, _d.width / 2, _d.height / 2);
        },

        // 虚拟角色
        VirtualRoleFigure: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height);
        },

        // IT决策
        ITRhombus: function () {
            var set = paper.set(),
                x = _d.x,
                y = _d.y,
                width = _d.width,
                height = _d.height,
                buildPath = function (x, y, width, height) {
                    var d = "M" + (x + width / 2) + "," + y;
                    d += " L" + x + "," + (y + height / 2);
                    d += " L" + (x + width / 2) + "," + (y + height);
                    d += " L" + (x + width) + "," + (y + height / 2);
                    d += " L" + (x + width / 2) + "," + y;
                    return d;
                };

            set.push(
                paper.path(buildPath(x, y, width, height)),
                paper.path(buildPath(x + 10, y + 7, width - 20, height - 14))
            );
            return set;
        },

        // 等腰图形
        IsoscelesTrapezoidFigure: function () {
            var d = "M" + _d.x + "," + _d.y;
            d += " H" + (_d.x + _d.width);
            d += " L" + (_d.x + _d.width * 0.75) + "," + (_d.y + _d.height);
            d += " H" + (_d.x + _d.width * 0.25);
            d += " L" + _d.x + "," + _d.y;
            return paper.path(d);
        },

        // 岗位
        PositionFigure: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height);
        },

        // 0级
        FlowLevelFigureOne: function () {
            return flowLevelFigureBuilder.build(0);
        },

        // 1级
        FlowLevelFigureTwo: function () {
            return flowLevelFigureBuilder.build(1);
        },

        // 2级
        FlowLevelFigureThree: function () {
            return flowLevelFigureBuilder.build(2);
        },

        // 3级
        FlowLevelFigureFour: function () {
            return flowLevelFigureBuilder.build(3);
        },
		// 菱形
        MapRhombus: function () {
            var d = "M" + (_d.x + _d.width / 2) + "," + _d.y;
            d += " L" + _d.x + "," + (_d.y + _d.height / 2);
            d += " L" + (_d.x + _d.width / 2) + "," + (_d.y + _d.height);
            d += " L" + (_d.x + _d.width) + "," + (_d.y + _d.height / 2);
            d += " L" + (_d.x + _d.width / 2) + "," + _d.y;
            return paper.path(d);
        },
        // 流程图/流程地图/组织图名称文本框
        MapNameText: function () {
            return paper.rect(_d.x, _d.y, _d.width, _d.height);
        }
    },
// 0-3级流程构建处理
    flowLevelFigureBuilder = {

        build: function (i) {
            return paper.set(this.fill(this.getData(), i));
        },

        getData: function () {
            var standW = _d.width * 22 / 100,
                standH = _d.height * 22 / 100,
                spaceW = _d.width * 4 / 100,
                spaceH = _d.height * 4 / 100,
                triangleD,
                trapeziumD_1,
                trapeziumD_2,
                trapeziumD_3;

            // 顶部三角形
            triangleD = "M" + (_d.x + _d.width / 2) + "," + _d.y;
            triangleD += " L" + (_d.x + _d.width / 2 - standW / 2) + "," + (_d.y + standH);
            triangleD += " H" + (_d.x + _d.width / 2 + standW / 2) + "z";

            // 梯形1
            trapeziumD_1 = " M" + (_d.x + _d.width / 2 - standW / 2 - spaceW / 2 ) + "," + (_d.y + standH + spaceH);
            trapeziumD_1 += " H" + (_d.x + _d.width / 2 + standW / 2 + spaceW / 2);
            trapeziumD_1 += " L" + (_d.x + _d.width / 2 + standW + spaceW / 2) + "," + (_d.y + standH * 2 + spaceH);
            trapeziumD_1 += " H" + (_d.x + _d.width / 2 - standW - spaceW / 2) + "z";

            // 梯形2
            trapeziumD_2 = " M" + (_d.x + _d.width / 2 - standW - spaceW) + "," + (_d.y + standH * 2 + spaceH * 2);
            trapeziumD_2 += " H" + (_d.x + _d.width / 2 + standW + spaceW);
            trapeziumD_2 += " L" + (_d.x + _d.width / 2 + standW + spaceW + standW / 2) + "," + (_d.y + standH * 3 + spaceH * 2);
            trapeziumD_2 += " H" + (_d.x + _d.width / 2 - standW - spaceW - standW / 2) + "z";

            // 梯形3
            trapeziumD_3 = " M" + (_d.x + _d.width / 2 - standW - spaceW - standW / 2 - spaceW / 2 ) + "," + (_d.y + standH * 3 + spaceH * 3);
            trapeziumD_3 += " H" + (_d.x + _d.width / 2 + standW + spaceW + standW / 2 + spaceW / 2);
            trapeziumD_3 += " L" + (_d.x + _d.width / 2 + standW * 2 + spaceW * 2) + "," + (_d.y + standH * 4 + spaceH * 4);
            trapeziumD_3 += " H" + (_d.x + _d.width / 2 - standW * 2 - spaceW * 2) + "z";

            return [paper.path(triangleD), paper.path(trapeziumD_1), paper.path(trapeziumD_2), paper.path(trapeziumD_3)];
        },

        fill: function (data, i) {
            $.each(data, function (index, item) {
                if (index === i) {
                    item.attr("fill", "#dd194f");
                } else {
                    item.attr("fill", "#c6d8eb");
                }
                item.attr("stroke-width", 0);
            });
            return data;
        }
    };

