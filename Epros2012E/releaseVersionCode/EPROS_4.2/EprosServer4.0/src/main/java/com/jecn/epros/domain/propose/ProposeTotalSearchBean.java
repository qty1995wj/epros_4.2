package com.jecn.epros.domain.propose;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProposeTotalSearchBean {
	@ApiModelProperty(required = true)
	private String orgIds;
	@ApiModelProperty(value = "0组织，1岗位，2人员", required = true)
	private int orgType;
	private Long fileId;
	@ApiModelProperty(value = "0是流程架构 1是流程 2是制度 3是标准 4是风险 5是文件（页面）", required = true)
	private int fileType;
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
	private Date startTime;
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
	private Date endTime;

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public int getOrgType() {
		return orgType;
	}

	public void setOrgType(int orgType) {
		this.orgType = orgType;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	@JsonIgnore
	public List<Long> getOrgSetIds() {
		List<Long> ids = new ArrayList<Long>();
		if (orgIds != null && !"".equals(orgIds)) {
			String[] orgStrArr = orgIds.split(",");
			for (String str : orgStrArr) {
				if (str != null && !"".equals(str)) {
					ids.add(Long.valueOf(str));
				}
			}
		}
		return ids;
	}

	// fileType 0是流程架构 1是流程 2是制度 3是标准 4是风险 5是文件（页面）
	// proposeType 0流程，1制度，2流程架构，3文件，4风险，5标准 （数据库）
	@JsonIgnore
	public int convertToProposeType() {
		switch (fileType) {
		case 0:
			return 2;
		case 1:
			return 0;
		case 2:
			return 1;
		case 3:
			return 5;
		case 4:
			return 4;
		case 5:
			return 3;
		default:
			return -1;
		}
	}

	// fileType 0是流程架构 1是流程 2是制度 3是标准 4是风险 5是文件（页面）
	// logType 0是流程，1是制度，2是文件，3是标准，4风险，5是组织，6是岗位,7流程架构
	@JsonIgnore
	public int convertToLogType() {
		switch (fileType) {
		case 0:
			return 7;
		case 1:
			return 0;
		case 2:
			return 1;
		case 3:
			return 3;
		case 4:
			return 4;
		case 5:
			return 2;
		default:
			return -1;
		}
	}

}
