/**
 * 
 */
package com.jecn.epros.server.bean.dataimport;

/**
 * 
 * 基准岗位bean，从视图获取数据用
 * 
 * @author xiaobo
 * 
 */
public class BasePosBean {

	/*** 基准岗位编号 ***/
	private String basePosNum;

	/*** 基准岗位名称 **/
	private String basePosName;

	/** 岗位名称 **/
	private String posName;

	/** 岗位编号 **/
	private String posNum;

	/** 部门编号 **/
	private String deptNum;

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}

	public String getBasePosNum() {
		return basePosNum;
	}

	public void setBasePosNum(String basePosNum) {
		this.basePosNum = basePosNum;
	}

	public String getBasePosName() {
		return basePosName;
	}

	public void setBasePosName(String basePosName) {
		this.basePosName = basePosName;
	}

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

}
