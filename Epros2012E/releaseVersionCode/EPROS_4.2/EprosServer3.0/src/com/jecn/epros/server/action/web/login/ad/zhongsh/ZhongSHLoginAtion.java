package com.jecn.epros.server.action.web.login.ad.zhongsh;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.util.JecnUserSecurityKey;
import com.jecn.webservice.zhongsh.ArrayOfString;
import com.jecn.webservice.zhongsh.GetUserByTokenService;
import com.jecn.webservice.zhongsh.GetUserByTokenServiceSoap;

/**
 * 中石化单点
 * 
 * @author ZXH
 * @date 2015-12-9
 */
public class ZhongSHLoginAtion extends JecnAbstractADLoginAction {

	static {
		ZhongSHAfterItem.start();
	}

	public ZhongSHLoginAtion(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		if ("admin".equals(loginAction.getLoginName())) {
			return loginAction.userLoginGen(true);
		}
		String token = loginAction.getRequest().getParameter("token");
		// 获取跳转类型 1：单点登录请求；2:流程连接请求；3：活动连接请求
		String targetUrl = loginAction.getRequest().getParameter("targetUrl");
		if (StringUtils.isBlank(token) || StringUtils.isBlank(targetUrl)) {
			log.error("token = " + token + " targetUrl = " + targetUrl);
			return LoginAction.INPUT;
		}
		// URL解析token
		List<String> checkToken = getCheckToken(token);
		String loginName = checkToken.get(1);

		if (!checkLoginName(checkToken)) {
			return LoginAction.INPUT;
		}
		// 登录状态还原
		this.loginAction.getSession().setAttribute(JecnContants.SESSION_KEY, "");
		if ("1".equals(targetUrl)) {
			// 1、获取登录名称
			getLoginNameByToken(checkToken);
		} else if ("2".equals(targetUrl)) {// 直接打开流程图
			return loginToProcess(loginName);
		} else if ("3".equals(targetUrl)) { // 跳转到活动明细
			return loginToActivity(loginName);
		}
		return loginAction.userLoginGen(false);
	}

	/**
	 * 验证用户是否存在
	 * 
	 * @param loginName
	 * @return
	 */
	private boolean checkLoginName(List<String> checkToken) {
		try {
			if ("1".equals(checkToken.get(0))) { // 获取用户异常！
				loginAction.addFieldError("loginMessage", checkToken.get(2));
				return false;
			} else {
				boolean isExists = loginAction.getPersonService().isExistLoginNameAdd(checkToken.get(1), -1L);
				if (!isExists) {// 访问用户不存在
					// loginAction.addFieldError("loginMessage", "访问用户不存在！");
					insertUser(checkToken.get(1));
					return true;
				}
			}
		} catch (Exception e) {
			log.error("验证用户是否存在异常！" + checkToken);
		}
		return true;
	}

	/**
	 * 门户网站中不存在的情况，插入人员
	 * 
	 * @param loginName
	 */
	private void insertUser(String loginName) {
		JecnUser user = new JecnUser();
		user.setLoginName(loginName);
		user.setPassword(JecnUserSecurityKey.getsecret(getRandomNum()));
		user.setTrueName(loginName);
		try {
			loginAction.getPersonService().savePerson(user, null, null);
		} catch (Exception e) {
			log.error("获取用户不存在，插入人员异常！");
		}
	}

	/**
	 * 0到9 随机5位数字
	 * 
	 * @return
	 */
	private String getRandomNum() {
		int a[] = new int[10];
		for (int i = 0; i <= 5; i++) {
			a[i] = (int) (Math.random() * 10);
		}
		StringBuffer strBuff = new StringBuffer();
		for (int i = 0; i < 5; i++) {
			strBuff.append(a[i]);
		}
		return strBuff.toString();
	}

	private List<String> getCheckToken(String token) {
		GetUserByTokenService ss = new GetUserByTokenService(ZhongSHAfterItem.SERVICE_URL,
				ZhongSHAfterItem.SERVICE_NAME);
		GetUserByTokenServiceSoap port = ss.getGetUserByTokenServiceSoap();
		ArrayOfString _checkToken__return = port.checkToken(token);
		return _checkToken__return.getString();
	}

	private void getLoginNameByToken(List<String> checkToken) {
		if ("0".equals(checkToken.get(0))) {// 0为成功；1为失败
			// 获取用户登录名
			loginAction.setLoginName(checkToken.get(1));
			log.info("获取登录名成功！ loginName = " + loginAction.getLoginName());
		}
	}

	/**
	 * 跳转到流程
	 * 
	 * @return
	 */
	private String loginToProcess(String loginName) {
		boolean isLogin = isCheckLogin(loginName);
		String redirectURL = "";
		if (isLogin) {// 登录成功
			String flowId = loginAction.getRequest().getParameter("flowId");
			String type = getFlowType();
			log.error(" ZhongSHAfterItem.EPROS_LOGIN_URL = " + ZhongSHAfterItem.EPROS_LOGIN_URL);
			redirectURL = ZhongSHAfterItem.EPROS_LOGIN_URL + "process.action?type=" + type + "&flowId=" + flowId;
			// log.info("请求成功后登录流程地址：redirectURL = " + redirectURL);
			loginAction.setCustomRedirectURL(redirectURL);
			return LoginAction.REDIRECT;
		}
		return LoginAction.INPUT;
	}

	/**
	 * 是否登录
	 * 
	 * @param loginName
	 * @return true:登录
	 */
	private boolean isCheckLogin(String loginName) {
		loginAction.setLoginName(loginName);
		String ret = loginAction.userLoginGen(false);
		return LoginAction.MAIN.equals(ret) || LoginAction.SUCCESS.equals(ret);
	}

	/**
	 * 跳转到流程
	 * 
	 * @return
	 */
	private String loginToActivity(String loginName) {
		boolean isLogin = isCheckLogin(loginName);
		String redirectURL = null;
		if (isLogin) {// 登录成功
			String activeId = loginAction.getRequest().getParameter("activeId");

			log.error("redirectURL = " + ZhongSHAfterItem.EPROS_LOGIN_URL);
			redirectURL = ZhongSHAfterItem.EPROS_LOGIN_URL + "processActive.action?activeId=" + activeId;
			log.error("redirectURL = " + redirectURL);
			// log.info("请求成功后登录流程地址：redirectURL = " + redirectURL);
			loginAction.setCustomRedirectURL(redirectURL);
			return LoginAction.REDIRECT;
		}
		return LoginAction.INPUT;
	}

	/**
	 * 跳转的流程类型
	 * 
	 * @return
	 */
	private String getFlowType() {
		return "0".equals(loginAction.getRequest().getParameter("isflow")) ? "processMap" : "process";
	}
}
