package com.jecn.epros.domain.process.relatedFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.util.HttpUtil;

/**
 * Created by admin on 2017/5/27.
 */
public class StandardizedFile {
    private Long id;
    private String name;
    /**
     * 编号
     */
    private String docId;

    public LinkResource getLink(boolean isPubStr, Long historyId) {
        boolean isHistory = historyId == 0 ? false : true;
        return new LinkResource(id, name, LinkResource.ResourceType.FILE, TreeNodeUtils.NodeType.process);
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }


    @JsonIgnore
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }
}
