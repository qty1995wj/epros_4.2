package epros.draw.designer;

import java.util.List;

public class JecnFlowInoutT {
	private String id;
	private Long flowId;
	private Integer type;
	private String name;
	private String explain;
	private String pos;
	private Long sortId;
	private List<JecnFlowInoutPosT> listInoutPos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public List<JecnFlowInoutPosT> getListInoutPos() {
		return listInoutPos;
	}

	public void setListInoutPos(List<JecnFlowInoutPosT> listInoutPos) {
		this.listInoutPos = listInoutPos;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public Long getSortId() {
		return sortId;
	}

	public void setSortId(Long sortId) {
		this.sortId = sortId;
	}

}
