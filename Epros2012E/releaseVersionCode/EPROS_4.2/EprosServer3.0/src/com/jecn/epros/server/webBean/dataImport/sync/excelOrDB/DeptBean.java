package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;

public class DeptBean extends AbstractBaseBean {
	/** 部门编号 DEPT_NUM */
	private String deptNum = null;
	/** 部门名称 DEPT_NAME */
	private String deptName = null;
	/** 父编号 P_DEPT_NUM */
	private String perDeptNum = null;
	/** 项目ID */
	private Long projectId = null;

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getPerDeptNum() {
		return perDeptNum;
	}

	public void setPerDeptNum(String perDeptNum) {
		this.perDeptNum = perDeptNum;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	/**
	 * 
	 * 返回属性信息
	 * 
	 * @return
	 */
	public String toInfo() {
		StringBuilder strBuf = new StringBuilder();
		// 部门编号
		strBuf.append("部门编号:");
		strBuf.append(this.getDeptNum());
		strBuf.append(SyncConstant.EMTRY_SPACE);

		// 部门名称
		strBuf.append("部门名称:");
		strBuf.append(this.getDeptName());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 上级部门编号
		strBuf.append("上级部门编号:");
		strBuf.append(this.getPerDeptNum());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		// 错误信息
		strBuf.append("错误信息:");
		strBuf.append(this.getError());
		strBuf.append(SyncConstant.EMTRY_SPACE);
		strBuf.append("\r\n");
		return strBuf.toString();
	}

	/**
	 * 
	 * 通过String[]给属性赋值
	 * 
	 * @param strArrray
	 */
	public void setAttributes(String[] strArrray) {
		if (strArrray == null || strArrray.length != 3) {
			return;
		}

		// 部门编号
		this.setDeptNum(strArrray[0]);
		// 部门名称
		this.setDeptName(strArrray[1]);
		// 上级部门编号
		this.setPerDeptNum(strArrray[2]);

	}
}
