package com.jecn.epros.server.webBean.process;

import java.util.List;

import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.webBean.rule.RuleInfoBean;
import com.jecn.epros.server.webBean.standard.StandardWebBean;

/**
 * 相关流程、标准、制度
 * 
 * @author fuzhh Jan 16, 2013
 * 
 */
public class RelatedProcessRuleStandardBean {

	/** 相关流程 */
	private List<ProcessWebBean> relatedProcessList;
	/** 相关标准 */
	private List<StandardWebBean> relatedStandardList;
	/** 相关制度 */
	private List<RuleInfoBean> relatedRuleList;
	/** 相关风险 */
	private List<JecnRisk> relatedRiskList;

	public List<JecnRisk> getRelatedRiskList() {
		return relatedRiskList;
	}

	public void setRelatedRiskList(List<JecnRisk> relatedRiskList) {
		this.relatedRiskList = relatedRiskList;
	}

	public List<ProcessWebBean> getRelatedProcessList() {
		return relatedProcessList;
	}

	public void setRelatedProcessList(List<ProcessWebBean> relatedProcessList) {
		this.relatedProcessList = relatedProcessList;
	}

	public List<StandardWebBean> getRelatedStandardList() {
		return relatedStandardList;
	}

	public void setRelatedStandardList(List<StandardWebBean> relatedStandardList) {
		this.relatedStandardList = relatedStandardList;
	}

	public List<RuleInfoBean> getRelatedRuleList() {
		return relatedRuleList;
	}

	public void setRelatedRuleList(List<RuleInfoBean> relatedRuleList) {
		this.relatedRuleList = relatedRuleList;
	}

}
