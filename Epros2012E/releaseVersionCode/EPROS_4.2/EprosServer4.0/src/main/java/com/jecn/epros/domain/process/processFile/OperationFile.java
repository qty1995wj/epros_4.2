package com.jecn.epros.domain.process.processFile;

import java.util.List;

/**
 * 操作说明
 */
public class OperationFile {

    /**
     * 操作说明关联的流程、制度等的name
     **/
    private String name;
    private Long id;
    /**
     * 关联的资源的类型process rule
     **/
    private String type;

    /**
     * 1：发布
     */
    private int publish;
    /**
     * 操作说明各阶段处理
     */
    private String url;
    /**
     * 操作说明各阶段处理
     */
    private List<BaseProcessFileBean> fileBeans = null;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<BaseProcessFileBean> getFileBeans() {
        return fileBeans;
    }

    public void setFileBeans(List<BaseProcessFileBean> fileBeans) {
        this.fileBeans = fileBeans;
    }

    public int getPublish() {
        return publish;
    }

    public void setPublish(int publish) {
        this.publish = publish;
    }
}
