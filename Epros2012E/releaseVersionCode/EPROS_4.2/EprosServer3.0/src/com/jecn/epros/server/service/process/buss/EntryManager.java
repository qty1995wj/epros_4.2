package com.jecn.epros.server.service.process.buss;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.bean.process.entry.JecnEntry;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.util.ApplicationContextUtil;

/**
 * 初始化加载只加载一次，相当于单例。
 * 创建本来的目的是防止每次都查询一次词条，因为按照本人预估这个数据量应该很大 每次查询浪费时间 。
 * 但是要注意word的唯一性和数据安全性，目前word是全表唯一的，如果将来变成目录唯一，本类将不再适用。
 * 由于使用的是缓存，所以不能够手动删除数据库数据。
 * 
 * @author hyl
 * 
 */
public class EntryManager {
	private IFlowStructureService service;
	private final ConcurrentMap<String, JecnEntry> words = new ConcurrentHashMap<String, JecnEntry>();

	public EntryManager() {
		this.service = (IFlowStructureService) ApplicationContextUtil.getContext().getBean("FlowStructureServiceImpl");
		init();
	}

	public void init() {
		List<JecnEntry> entrys = service.fetchEntrys();
		for (JecnEntry entry : entrys) {
			words.put(entry.getWord(), entry);
		}
	}

	private boolean contains(String word) {
		return this.words.containsKey(word);
	}

	/**
	 * 保存关键字
	 * 
	 * @param word
	 * @return
	 */
	public JecnEntry saveElseGet(String word) {
		if (StringUtils.isBlank(word)) {
			throw new IllegalArgumentException("EntryManager save() word为空");
		}
		word = word.trim();
		if (contains(word)) {
			return words.get(word);
		}
		JecnEntry entry = new JecnEntry();
		entry.setWord(word);
		JecnEntry saved = words.putIfAbsent(word, entry);
		if (saved == null) {// 新保存的
			service.saveEntry(entry);
			return entry;
		}
		return saved;
	}

	public List<JecnEntry> values() {
		return (List<JecnEntry>) words.values();
	}

	public Map<String, JecnEntry> getWords() {
		return Collections.unmodifiableMap(this.words);
	}

}
