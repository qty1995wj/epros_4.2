package com.jecn.epros.server.service.process.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oracle.sql.BLOB;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.lob.SerializableBlob;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.process.JecnProcessTemplateBean;
import com.jecn.epros.server.bean.process.JecnProcessTemplateFollowBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.process.IProcessTemplateDao;
import com.jecn.epros.server.dao.process.IProcessTemplateFollowDao;
import com.jecn.epros.server.service.process.IProcessTemplateService;
import com.jecn.epros.server.util.JecnUtil;

/**
 * @author yxw 2012-8-10
 * @description：模型
 */
@Transactional
public class ProcessTemplateServiceImpl extends AbsBaseService<JecnProcessTemplateBean, Long> implements
		IProcessTemplateService {

	private Logger log = Logger.getLogger(ProcessTemplateServiceImpl.class);

	private IProcessTemplateDao processTemplateDao;
	private IProcessTemplateFollowDao processTemplateFollowDao;

	public void setProcessTemplateDao(IProcessTemplateDao processTemplateDao) {
		this.processTemplateDao = processTemplateDao;
	}

	public void setProcessTemplateFollowDao(IProcessTemplateFollowDao processTemplateFollowDao) {
		this.processTemplateFollowDao = processTemplateFollowDao;
	}

	public JecnProcessTemplateBean saveTemplate(JecnProcessTemplateBean processTemplateBean,
			JecnProcessTemplateFollowBean processTemplateFollowBean) throws Exception {
		try {
			Date date = new Date();
			// 保存数据库
			processTemplateBean.setCreateDate(date);
			processTemplateBean.setUpdateDate(date);
			processTemplateFollowBean.setCreateDate(date);
			processTemplateFollowBean.setUpdateDate(date);
			if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
				processTemplateBean.setContent(Hibernate.createBlob(processTemplateBean.getBytes()));
				processTemplateDao.saveOrUpdate(processTemplateBean);
				processTemplateFollowBean.setModeId(processTemplateBean.getId());
				processTemplateFollowBean.setImageContent(Hibernate.createBlob(processTemplateFollowBean.getBytes()));
				processTemplateFollowDao.save(processTemplateFollowBean);
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				processTemplateBean.setContent(Hibernate.createBlob(new byte[1]));
				processTemplateDao.saveOrUpdate(processTemplateBean);
				processTemplateDao.getSession().flush();
				processTemplateDao.getSession().refresh(processTemplateBean, LockMode.UPGRADE);
				SerializableBlob sb = (SerializableBlob) processTemplateBean.getContent();
				java.sql.Blob wrapBlob = sb.getWrappedBlob();
				BLOB blob = (BLOB) wrapBlob;
				try {
					OutputStream out = blob.getBinaryOutputStream();
					out.write(processTemplateBean.getBytes());
					out.close();
				} catch (SQLException ex) {
					log.error("写blob大字段错误！", ex);
					throw ex;
				} catch (IOException ex) {
					log.error("写blob大字段错误！", ex);
					throw ex;
				}
				processTemplateFollowBean.setModeId(processTemplateBean.getId());
				processTemplateFollowBean.setImageContent(Hibernate.createBlob(new byte[1]));
				processTemplateFollowDao.save(processTemplateFollowBean);
				processTemplateFollowDao.getSession().flush();
				processTemplateFollowDao.getSession().refresh(processTemplateFollowBean, LockMode.UPGRADE);
				sb = (SerializableBlob) processTemplateFollowBean.getImageContent();
				wrapBlob = sb.getWrappedBlob();
				blob = (BLOB) wrapBlob;
				OutputStream out = null;
				try {
					out = blob.getBinaryOutputStream();
					out.write(processTemplateFollowBean.getBytes());
					out.close();
				} catch (SQLException ex) {
					log.error("写blob大字段错误！", ex);
					throw ex;
				} catch (IOException ex) {
					log.error("写blob大字段错误！", ex);
					throw ex;
				} finally {
					if (out != null) {
						out.close();
					}
				}
			}
			processTemplateBean.getListJecnProcessTemplateFollowBean().add(processTemplateFollowBean);

			JecnProcessTemplateFollowBean processTemplateFollow = new JecnProcessTemplateFollowBean();
			processTemplateFollow.setBytes(processTemplateFollowBean.getBytes());
			processTemplateFollow.setCreateDate(processTemplateFollowBean.getCreateDate());
			processTemplateFollow.setCreatePeopleId(processTemplateFollowBean.getCreatePeopleId());
			processTemplateFollow.setId(processTemplateFollowBean.getId());
			processTemplateFollow.setImageName(processTemplateFollowBean.getImageName());
			processTemplateFollow.setModeId(processTemplateFollowBean.getModeId());
			processTemplateFollow.setName(processTemplateFollowBean.getName());
			processTemplateFollow.setUpdateDate(processTemplateFollowBean.getUpdateDate());
			processTemplateFollow.setUpdatePeopleId(processTemplateFollowBean.getUpdatePeopleId());

			JecnProcessTemplateBean processTemplate = new JecnProcessTemplateBean();
			processTemplate.setBytes(processTemplateBean.getBytes());
			processTemplate.setCreateDate(processTemplateBean.getCreateDate());
			processTemplate.setCreatePeopleId(processTemplateBean.getCreatePeopleId());
			processTemplate.setId(processTemplateBean.getId());
			processTemplate.getListJecnProcessTemplateFollowBean().add(processTemplateFollow);
			processTemplate.setType(processTemplateBean.getType());
			processTemplate.setUpdateDate(processTemplateBean.getUpdateDate());
			processTemplate.setUpdatePeopleId(processTemplateBean.getUpdatePeopleId());
			JecnUtil.saveJecnJournal(processTemplateFollowBean.getId(), processTemplateFollowBean.getName(), 20, 1,
					processTemplateFollowBean.getCreatePeopleId(), processTemplateDao);
			return processTemplate;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void deleteTemplate(JecnProcessTemplateBean processTemplateBean, long id) throws Exception {
		try {
			JecnProcessTemplateFollowBean fllow = this.processTemplateFollowDao.get(id);
			JecnUtil.saveJecnJournal(fllow.getId(), fllow.getName(), 20, 3, fllow.getUpdatePeopleId(),
					processTemplateDao);
			String hql = "delete from JecnProcessTemplateFollowBean where id=?";
			processTemplateDao.execteHql(hql, id);
			processTemplateBean.setUpdateDate(new Date());
			if (JecnContants.dbType.equals(DBType.SQLSERVER) || JecnContants.dbType.equals(DBType.MYSQL)) {
				processTemplateBean.setContent(Hibernate.createBlob(processTemplateBean.getBytes()));
				processTemplateDao.update(processTemplateBean);
			} else if (JecnContants.dbType.equals(DBType.ORACLE)) {
				processTemplateBean.setContent(Hibernate.createBlob(new byte[1]));
				processTemplateDao.update(processTemplateBean);
				processTemplateDao.getSession().flush();
				processTemplateDao.getSession().refresh(processTemplateBean, LockMode.UPGRADE);
				SerializableBlob sb = (SerializableBlob) processTemplateBean.getContent();
				java.sql.Blob wrapBlob = sb.getWrappedBlob();
				BLOB blob = (BLOB) wrapBlob;
				try {
					OutputStream out = blob.getBinaryOutputStream();
					out.write(processTemplateBean.getBytes());
					out.close();
				} catch (SQLException ex) {
					log.error("写blob大字段错误！", ex);
					throw ex;
				} catch (IOException ex) {
					log.error("写blob大字段错误！", ex);
					throw ex;
				}
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public void updateTemplate(long id, String name, Long updatePeopleId) throws Exception {
		try {
			String hql = "update JecnProcessTemplateFollowBean set name=? where id=?";
			processTemplateDao.execteHql(hql, name, id);
			JecnUtil.saveJecnJournal(id, name, 20, 2, updatePeopleId, processTemplateDao, 4);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<JecnProcessTemplateBean> getJecnProcessTemplateBeanList() throws Exception {
		try {
			String hql = "from JecnProcessTemplateBean";
			List<JecnProcessTemplateBean> listJecnProcessTemplateBean = processTemplateDao.listHql(hql);
			hql = "from JecnProcessTemplateFollowBean";
			List<JecnProcessTemplateFollowBean> listJecnProcessTemplateFollowBean = processTemplateFollowDao
					.listHql(hql);
			byte[] bytes = null;
			List<JecnProcessTemplateFollowBean> list = null;
			for (JecnProcessTemplateBean jecnProcessTemplateBean : listJecnProcessTemplateBean) {
				SerializableBlob sb = (SerializableBlob) jecnProcessTemplateBean.getContent();
				if (sb != null) {
					Blob wrapBlob = sb.getWrappedBlob();
					InputStream in = null;
					try {
						bytes = new byte[(int) wrapBlob.length()];

						in = wrapBlob.getBinaryStream();
						in.read(bytes);
						in.close();
						jecnProcessTemplateBean.setBytes(bytes);
						// jecnProcessTemplateBean.setContent(null);
						list = new ArrayList<JecnProcessTemplateFollowBean>();
						for (JecnProcessTemplateFollowBean jecnProcessTemplateFollowBean : listJecnProcessTemplateFollowBean) {
							if (jecnProcessTemplateFollowBean.getModeId() == jecnProcessTemplateBean.getId()) {
								sb = (SerializableBlob) jecnProcessTemplateFollowBean.getImageContent();
								wrapBlob = sb.getWrappedBlob();
								bytes = new byte[(int) wrapBlob.length()];
								in = wrapBlob.getBinaryStream();
								in.read(bytes);
								in.close();
								jecnProcessTemplateFollowBean.setBytes(bytes);
								// jecnProcessTemplateFollowBean.setImageContent(null);
								list.add(jecnProcessTemplateFollowBean);
							}
						}
						jecnProcessTemplateBean.setListJecnProcessTemplateFollowBean(list);
					} catch (IOException e) {
						if (in != null) {
							try {
								in.close();
							} catch (IOException e1) {
								log.error("", e);
								throw e;
							}
						}
						log.error("", e);
						throw e;
					}
				}
			}
			return listJecnProcessTemplateBean;
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}
}
