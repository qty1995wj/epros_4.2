package com.jecn.epros.server.action.web.login.ad.dongh;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;

/**
 * 
 * 验证
 * 
 * @author ZHOUXY
 * 
 */
class JecnSingleLoginUtil {
	private static final Logger log = Logger.getLogger(JecnSingleLoginUtil.class);

	/**
	 * 单点登录
	 */
	static LoginInJson singLeLogin(String userName, String userPass) {
		return dhSingLeLogin(userName, userPass);
	}

	/**
	 * 东航单点登录
	 */
	private static LoginInJson dhSingLeLogin(String userName, String userPass) {
		LoginInJson loginInJson = new LoginInJson();
		// AD域名
		String ADIP = JecnDonghAfterItem.ADIP;
		if (ADIP == null || "".equals(ADIP)) {
			loginInJson.setResult("255");
			loginInJson.setMessage("获取配置文件中AD服务器IP错误!");
			return loginInJson;
		}
		// 密码通过base64 解密
		userPass = JecnCommon.getDesString(userPass);
		// 验证AD域名
		int check = conncetionAD(userName, userPass, ADIP);
		// AD域验证通过
		if (check == 0) {
			loginInJson.setResult("0");
			loginInJson.setExpires(getDate());
		} else {
			loginInJson.setResult("1");
		}
		return loginInJson;
	}

	/**
	 * 算页面超时 时间
	 * 
	 * @return
	 */
	public static String getDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, 2);
		// cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 2);
		// 通过格式化输出日期
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String data = df.format(cal.getTime());
		return data;
	}

	/**
	 * 验证AD域
	 * 
	 */
	private static int conncetionAD(String userName, String passw, String hostAD) {
		log.info("AD域地址池连接开始...");
		// AD域IP
		String ip = JecnDonghAfterItem.ADIP1;
		int flag0 = getCheckAD(userName, passw, ip);
		if (flag0 == 0) {// 验证成功
			return flag0;
		}
		ip = JecnDonghAfterItem.ADIP2;
		flag0 = getCheckAD(userName, passw, ip);
		if (flag0 == 0) {// 验证成功
			return flag0;
		}
		ip = JecnDonghAfterItem.ADIP3;
		flag0 = getCheckAD(userName, passw, ip);
		if (flag0 == 0) {// 验证成功
			return flag0;
		}
		ip = hostAD;
		flag0 = getCheckAD(userName, passw, ip);
		log.info("AD域地址池连接结束...");
		return flag0;
	}

	/**
	 * 验证AD域
	 * 
	 * @return 0验证通过,1 密码为空,2账户错误,4连接AD服务器错误
	 */
	private static int getCheckAD(String userName, String passw, String hostAD) {
		log.info("用户名==" + userName + "AD域IP==" + hostAD);
		DirContext ctx = null;
		String account = userName; // 设置访问账号
		String password = passw; // 设置账号密码
		String host = hostAD; // AD服务器
		String root = "389"; // root
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.PROVIDER_URL, "ldap://" + host + ":" + root);
		String domain = JecnDonghAfterItem.domain; // 邮箱的后缀名
		// String domain = "@JECN.EPROS.COM";
		String user = account.indexOf(domain) > 0 ? account : account + domain;
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, user);
		if (userName == null || "".equals(userName)) {
			log.info("AD域用户名为空！");
			return 1;
		}
		if (password == null || "".equals(password.trim())) {
			password = null;
			log.info("AD域密码为空!");
			return 1;
		}
		env.put(Context.SECURITY_CREDENTIALS, password);
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		try {
			ctx = new InitialDirContext(env);
			log.info("AD域验证成功!");
			return 0;
		} catch (AuthenticationException e) {
			log.error("AD域验证失败！！！", e);
			return 2;
		} catch (Exception e) {
			log.error("AD域验证异常！！！", e);
			return 3;
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					log.error("InitialDirContext关闭异常...", e);
				}
			}
		}
	}
	
	
}
