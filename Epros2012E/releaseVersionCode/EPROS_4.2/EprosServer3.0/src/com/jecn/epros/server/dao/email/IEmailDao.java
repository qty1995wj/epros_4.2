package com.jecn.epros.server.dao.email;

import java.util.List;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.email.bean.JecnEmail;

public interface IEmailDao extends IBaseDao<JecnEmail, String>{

	public void saveObject(Object obj)throws Exception;

	public List<Object[]> getNeedSendEmails()throws Exception;

}
