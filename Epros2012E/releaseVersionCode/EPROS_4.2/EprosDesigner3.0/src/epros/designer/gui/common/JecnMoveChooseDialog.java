package epros.designer.gui.common;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.swing.tree.TreePath;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.util.JecnUIUtil;

public abstract class JecnMoveChooseDialog extends JecnDialog {
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 树面板 */
	private JScrollPane treePanel = null;
	/** 结果面板 */
	private JPanel resultPanel = null;
	/** 已选结果滚动面板 */
	private JScrollPane resultScrollPane = null;

	/** 按钮面板，上面放四个按钮 */
	private JPanel buttonsPanel = null;
	/** 确定 */
	private JButton okBtn = null;
	/** 清空 */
	private JButton deleteBtn = null;
	/** 取消 */
	private JButton cancelBtn = null;

	/** 结果table */
	protected JecnTable resultTable = null;

	/** Tree 对象 */
	private JecnTree jTree = null;

	private JecnTree moveTree = null;
	private List<JecnTreeNode> listMoveNodes = null;
	/** 要移动的节点ID集合 */
	private List<Long> listIds = new ArrayList<Long>();
	/** 待移动节点类型 */
	private TreeNodeType moveNodeType;

	public TreeNodeType getMoveNodeType() {
		return moveNodeType;
	}

	public void setMoveNodeType(TreeNodeType moveNodeType) {
		this.moveNodeType = moveNodeType;
	}

	public List<JecnTreeNode> getListMoveNodes() {
		return listMoveNodes;
	}

	public void setListMoveNodes(List<JecnTreeNode> listMoveNodes) {
		this.listMoveNodes = listMoveNodes;
	}

	public List<Long> getListIds() {
		return listIds;
	}

	public void setListIds(List<Long> listIds) {
		this.listIds = listIds;
	}

	public JecnMoveChooseDialog(List<JecnTreeNode> listMoveNodes, JecnTree moveTree) {
		this.listMoveNodes = listMoveNodes;
		this.moveTree = moveTree;
		for (JecnTreeNode jecnTreeNode : listMoveNodes) {
			listIds.add(jecnTreeNode.getJecnTreeBean().getId());
		}
		initCompotents();
		initLayout();
		buttonInit();
		this.setModal(true);
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:初始化组件
	 */
	public void initCompotents() {
		// 节点移动
		this.setTitle(JecnProperties.getValue("nodeMove"));
		this.setSize(710, 510);
		this.setResizable(false);
		this.setModal(true);
		// 局中，设置大小后使用
		this.setLocationRelativeTo(null);
		// 主面板
		this.mainPanel = new JPanel();

		// 树面板
		this.treePanel = new JScrollPane();
		Dimension dimension = new Dimension(200, 430);
		treePanel.setPreferredSize(dimension);
		treePanel.setMinimumSize(dimension);
		treePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("moveToNode")));

		// 结果面板
		this.resultPanel = new JPanel();
		dimension = new Dimension(475, 430);
		resultPanel.setPreferredSize(dimension);
		resultPanel.setMinimumSize(dimension);
		resultPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("moveNode")));

		// 已选结果滚动面板设置大小
		this.resultScrollPane = new JScrollPane();
		dimension = new Dimension(468, 400);
		resultScrollPane.setPreferredSize(dimension);
		resultScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setMinimumSize(dimension);
		// 按钮面板
		this.buttonsPanel = new JPanel();
		dimension = new Dimension(460, 30);
		buttonsPanel.setPreferredSize(dimension);
		buttonsPanel.setMinimumSize(dimension);
		this.deleteBtn = new JButton(JecnProperties.getValue("deleteBtn"));
		this.okBtn = new JButton(JecnProperties.getValue("okBtn"));
		this.cancelBtn = new JButton(JecnProperties.getValue("cancelBtn"));
		// 设置各面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		treePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		resultScrollPane.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:dialog布局方法
	 */
	public void initLayout() {
		// 主面板布局-开始
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(2, 2, 2, 2);
		c = new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.NONE, insets, 0,
				0);

		mainPanel.add(treePanel, c);
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0,
				0);
		mainPanel.add(resultPanel, c);
		c = new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(buttonsPanel, c);
		// 主面板布局-结束

		jTree = getJecnTree();
		// 把tree加入滚动面板
		treePanel.setViewportView(jTree);
		resultTable = new ChooseTable();
		resultScrollPane.setViewportView(resultTable);
		resultPanel.add(resultScrollPane);

		// 设置按钮面板为流布局
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonsPanel.add(deleteBtn);
		buttonsPanel.add(okBtn);
		buttonsPanel.add(cancelBtn);
		// 把主面板增加到dialog
		this.getContentPane().add(mainPanel);
	}

	// 为按钮增加事件
	private void buttonInit() {

		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteBtnAction();
			}
		});

		okBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okBtnAction();
			}
		});

		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeDialog();
			}
		});
	}

	private void okBtnAction() {
		MoveNodeOkButTask okButTask = new MoveNodeOkButTask();
		String error = JecnLoading.start(okButTask);
		if (error == null) {
			return;
		}
		try {
			okButTask.get();
		} catch (InterruptedException e) {
			log.error("okBtnAction MoveNodeOkButTask is error", e);
		} catch (ExecutionException e) {
			log.error("okBtnAction MoveNodeOkButTask is error", e);
		}
	}

	/**
	 * 节点移动,确定 进度条
	 * 
	 * @author ZXH
	 * @date 2015-12-17
	 */
	class MoveNodeOkButTask extends SwingWorker<String, Void> {
		@Override
		protected String doInBackground() throws Exception {
			return moveNodeOkBut();
		}

		@Override
		public void done() {
			JecnLoading.stop();
		}
	}

	private String moveNodeOkBut() {
		if (listIds.size() == 0) {
			// 增加提示
			// 请选择要移动到的节点
			JecnOptionPane.showMessageDialog(JecnMoveChooseDialog.this, JecnProperties.getValue("pleaeMoveToNode"));
			return null;
		}
		TreePath[] treePath = jTree.getSelectionPaths();
		if (treePath == null || treePath.length != 1) {
			JecnOptionPane.showMessageDialog(JecnMoveChooseDialog.this, JecnProperties
					.getValue("noMoveNodeClickCancel"));
			return null;
		}
		JecnTreeNode node = (JecnTreeNode) treePath[0].getLastPathComponent();
		// 目标节点类型
		TreeNodeType moveToNodeType = node.getJecnTreeBean().getTreeNodeType();

		// 角色移动只有二级管理员和系统管理员可以操作，不需要验证权限
		if (!TreeNodeType.roleDir.equals(moveToNodeType) && !TreeNodeType.role.equals(moveToNodeType)) {
			// 判断移动到的节点有没有权限
			int authType = JecnTreeCommon.isAuthNode(node);
			if (authType == 0) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("aimNodeNoPopedom"));
				return null;
			}
		}
		/***
		 * 目标节点处于废止中
		 */
		if (node.getJecnTreeBean().getApproveType() == 2) {
			JecnOptionPane.showMessageDialog(JecnMoveChooseDialog.this, node.getJecnTreeBean().getName()
					+ JecnProperties.getValue("inTheRevocation"));
			return null;
		}
		// 被移动节点类型
		moveNodeType = listMoveNodes.get(0).getJecnTreeBean().getTreeNodeType();

		switch (moveNodeType) {
		case process:
		case processMap:
			// 根节点不需要判断是不是发布
			if (node.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processRoot) {
				break;
			}
			if (node.getJecnTreeBean().isPub()) {
				break;
			}
			// 已发布的节点不能移动到未发布的节点下!
			for (JecnTreeNode moveNode : listMoveNodes) {
				if (moveNode.getJecnTreeBean().isPub()) {
					JecnOptionPane.showMessageDialog(JecnMoveChooseDialog.this, JecnProperties
							.getValue("pubNodeNoMoveToNoPubNode"));
					return null;
				}
			}

			break;
		case processMapMode:
			// 流程地图模板不能移动到流程图模板节点下
			if (moveToNodeType == TreeNodeType.processMode) {
				JecnOptionPane.showMessageDialog(JecnMoveChooseDialog.this, JecnProperties
						.getValue("processMapNotShift"));
				return null;
			}
			break;
		case standardDir:
			// 标准目录不能移动除目录或根节点的位置
			if (moveToNodeType != TreeNodeType.standardDir && moveToNodeType != TreeNodeType.standardRoot) {
				JecnOptionPane.showMessageDialog(JecnMoveChooseDialog.this, JecnProperties
						.getValue("onlyToMovestandardDirNode"));
				return null;
			}
			break;
		case standard:
		case standardProcess:
		case standardProcessMap:
		case standardClause:
			// 标准文件、流程标准、流程架构标准、条款只能移动标准目录下
			if (moveToNodeType != TreeNodeType.standardDir) {
				JecnOptionPane.showMessageDialog(JecnMoveChooseDialog.this, JecnProperties
						.getValue("onlyToMovestandardDirNode"));
				return null;
			}

			break;
		case standardClauseRequire:
			// 条款要求只能移动条款下
			if (moveToNodeType != TreeNodeType.standardClause) {
				JecnOptionPane.showMessageDialog(JecnMoveChooseDialog.this, JecnProperties
						.getValue("onlyToMovestandardClauseNode"));
				return null;
			}

			break;
		case position:
			// 岗位只能移动到组织下面
			if (moveToNodeType != TreeNodeType.organization) {
				JecnOptionPane
						.showMessageDialog(JecnMoveChooseDialog.this, JecnProperties.getValue("noToMoveRootNode"));
				return null;
			}
			break;
		case ruleFile:
		case ruleModeFile:
			// 制度文件和制度模板不能移动到制度根节点下
			if (moveToNodeType != TreeNodeType.ruleDir) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("notmoveRuleFileRoot"));
				return null;
			}
			break;
		case riskPoint:
			// 风险点不能移动到风险根节点下
			if (moveToNodeType != TreeNodeType.riskDir) {
				JecnOptionPane
						.showMessageDialog(JecnMoveChooseDialog.this, JecnProperties.getValue("noToMoveRootNode"));
				return null;
			}
			break;
		case file:
			if (moveToNodeType != TreeNodeType.fileDir) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("notmovefileRoot"));
				return null;
			}
			break;
		case ruleMode:
			if (moveToNodeType != TreeNodeType.ruleModeDir) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("ruleModelCannotMove"));
				return null;
			}
		default:
			break;
		}
		// 条款要求或岗位是可以重名
		if (moveNodeType != TreeNodeType.standardClauseRequire || moveNodeType != TreeNodeType.position) {
			if (validateName(listMoveNodes, node.getJecnTreeBean().getId())) {
				return null;
			}
		}
		// 保存
		boolean isFlag = false;
		if (TreeNodeType.position.equals(moveNodeType) || TreeNodeType.organization.equals(moveNodeType)) {
			isFlag = savePosOrgData(listIds, node.getJecnTreeBean().getId(), node.getJecnTreeBean().getNumberId());
		} else {
			isFlag = savaData(listIds, node.getJecnTreeBean().getId());
		}
		if (isFlag) {
			JecnTreeNode jecnTreeNode = JecnTreeCommon.getTreeNode(node.getJecnTreeBean().getTreeNodeType().toString()
					+ node.getJecnTreeBean().getId(), moveTree);
			if (jecnTreeNode == null
					|| (jecnTreeNode.getChildCount() == 0 && jecnTreeNode.getJecnTreeBean().isChildNode())) {// 如果新的上级树节点没有展开则移除移动的节点
				JecnTreeCommon.removeNodes(moveTree, listMoveNodes);
			} else {
				for (JecnTreeNode moveNode : listMoveNodes) {
					moveNode.getJecnTreeBean().setPid(jecnTreeNode.getJecnTreeBean().getId());
				}
				JecnTreeCommon.moveNodes(listMoveNodes, jecnTreeNode, moveTree);
				JecnTreeCommon.selectNode(moveTree, jecnTreeNode);
				
			}
		}
		closeDialog();
		return "success";
	}

	public void deleteBtnAction() {
		int[] selectRows = resultTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		}
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((JecnTableModel) resultTable.getModel()).removeRow(selectRows[i]);
			listMoveNodes.remove(selectRows[i]);
			listIds.remove(selectRows[i]);
		}
	}

	/**
	 * 组织、岗位节点移动保存
	 * 
	 * @param ids
	 *            要移动的节点ID集合
	 * @param pid
	 *            移动到的节点ID
	 * @param orgNumber
	 *            组织编号
	 * @return boolean true 节点移动成功 false 移动失败
	 */
	public boolean savePosOrgData(List<Long> ids, Long pid, String orgNumber) {
		return false;
	}

	/**
	 * @author yxw 2012-12-17
	 * @description: 移动保存
	 * @param ids
	 *            要移动的节点ID集合
	 * @param pid
	 *            移动到的节点ID
	 * @return
	 */
	public abstract boolean savaData(List<Long> ids, Long pid);

	/**
	 * @author yxw 2012-12-17
	 * @description:验证移动有没有重命名
	 * @param listMoveNodes
	 *            要移动的节点集合
	 * @param pid
	 * @return true 有重名 false无重名
	 */
	public abstract boolean validateName(List<JecnTreeNode> listMoveNodes, Long pid);

	/**
	 * @author yxw 2012-5-4
	 * @description:关闭Dialog
	 */
	public void closeDialog() {
		this.dispose();
	}

	/**
	 * @author yxw 2012-5-4
	 * @description:设置
	 * @return
	 */
	public abstract JecnTree getJecnTree();

	/**
	 * @author yxw 2012-5-9
	 * @description:获得table的标题
	 * @return
	 */
	public abstract Vector<String> getTableTitle();

	// button抽角方法--结束
	class ChooseTable extends JecnTable {

		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTableTitle(), getContent());
		}

		@Override
		public boolean isSelectMutil() {
			return false;
		}

		@Override
		public int[] gethiddenCols() {
			return new int[] { 0 };
		}
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得Table的内容
	 * @param list
	 * @return
	 */
	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		for (JecnTreeNode node : listMoveNodes) {
			Vector<String> data = new Vector<String>();
			data.add(node.getJecnTreeBean().getId().toString());
			data.add(node.getJecnTreeBean().getName());

			content.add(data);
		}
		return content;
	}

	public JecnTree getMoveTree() {
		return moveTree;
	}

	public void setMoveTree(JecnTree moveTree) {
		this.moveTree = moveTree;
	}

}
