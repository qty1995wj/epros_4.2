package com.jecn.epros.server.action.web.login.ad.topband;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.webservice.topband.TestWebServiceDelegate;
import com.jecn.webservice.topband.TestWebServiceService;

/**
 * 
 * 拓邦：单点登录
 * 
 * http://192.168.10.238:8080/login.action?sys_username=admin&sys_password=
 * topband&username=admin&password=XXXXXXX
 * 
 * @author ZXH
 * 
 */
public class TopbandLoginAction extends JecnAbstractADLoginAction {

	static {
		TopBandAfterItem.start();
	}

	public TopbandLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 通过单点登录
	 * 
	 * @return String input;main;success;null
	 * 
	 */
	public String login() {
		try {
			return httpLogin();
		} catch (Exception e) {
			e.printStackTrace();
			return LoginAction.INPUT;
		}
	}

	/**
	 * HTTP 地址解析
	 * 
	 * @return String
	 * @date 2015-8-19 下午03:34:39
	 * @throws
	 */
	private String httpLogin() throws Exception {
		// http://172.20.29.55/login.action?Login_Name=admin
		// request 获取连接传入 参数 ;URL 的加号 被转义为空格, 以下方式不会
		String sys_username = loginAction.getRequest().getParameter("sys_username");
		String sys_password = loginAction.getRequest().getParameter("sys_password");
		String username = loginAction.getRequest().getParameter("username");
		String password = loginAction.getRequest().getParameter("password");
		if (StringUtils.isEmpty(sys_username) || StringUtils.isEmpty(sys_password) || StringUtils.isEmpty(username)
				|| StringUtils.isEmpty(password)) {
			log.info(" sys_username = " + sys_username + " sys_password = " + sys_password + " username = " + username
					+ " password = " + password);
			return loginAction.loginGen();
		}

		// 管理员权限验证
		if (!isLoginAdmin(sys_username, sys_password)) {
			log.error("管理员权限验证失败！ sys_username = " + sys_username);
			return LoginAction.INPUT;
		}
		// 用户验证
		TestWebServiceDelegate testWebServiceDelegate = new TestWebServiceService().getTestWebServicePort();
		String result = testWebServiceDelegate.authCheck(username, password);
		// String result = "-1";
		if (StringUtils.isBlank(username)) {// 单点登录名称为空，执行普通登录路径
			return loginAction.loginGen();
		}

		if ("-1".equals(result)) {// 验证失败
			log.error("用户验证失败 username = " + username + " password = " + password);
			return LoginAction.INPUT;
		}
		log.info("username = " + username + "password = " + password);
		return loginByLoginName(username);
	}

	/**
	 * admin用户验证
	 * 
	 * @param name
	 * @param password
	 * @return
	 */
	private boolean isLoginAdmin(String name, String password) {
		log.info("SYS_NAME = " + TopBandAfterItem.SYS_NAME + " name = " + name);
		log.info("SYS_PASSWORD = " + TopBandAfterItem.SYS_PASSWORD + " password = " + password);
		return TopBandAfterItem.SYS_NAME.equals(name) && TopBandAfterItem.SYS_PASSWORD.equals(password);
	}
}
