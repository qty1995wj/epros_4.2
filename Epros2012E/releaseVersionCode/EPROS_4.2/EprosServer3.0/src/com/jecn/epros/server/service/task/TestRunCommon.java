package com.jecn.epros.server.service.task;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.JecnCommon;

/**
 * 试运行邮件到期提醒
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Jul 1, 2013 时间：11:56:14 AM
 */
public class TestRunCommon {

	/** 发送试用行报告的sql SQLSERVER */
	public static String SEND_TEST_RUN_REPORT_SQL_SQLSERVER = "select t.id,"
			+ "       t.task_name,"
			+ "       t.is_controlaudit_lead,"
			+ "       t.start_time,"
			+ "       t.end_time,"
			+ "       t.state,"
			+ "       t.up_state,"
			+ "       t.task_else_state,"
			+ "       t.is_lock,"
			+ "       t.task_desc,"
			+ "       t.test_run_time,"
			+ "       t.history_vistion,"
			+ "       t.flow_type,"
			+ "       t.send_runtime_value,"
			+ "       t.r_id,"
			+ "       t.task_type,"
			+ "       t.create_person_id,"
			+ "       t.create_time,"
			+ "       t.update_time,"
			+ "       t.from_person_id,"
			+ "       t.review_counts,"
			+ "       t.approve_no_counts,"
			+ "		  jts.stage_name,"
			+ "       flow.flow_name"
			+ "      from jecn_task_bean_new t left join Jecn_User ju on ju.people_id=t.Create_Person_Id and ju.ISLOCK=0"
			+ "		 left join jecn_task_stage jts on jts.task_id=t.id and t.state=jts.stage_mark	"	
			+ "      inner join jecn_flow_structure_t flow on t.r_id=flow.flow_id"
			+ "      where datediff(day,t.create_time,getdate())=t.send_runtime_value"
			+ "      and t.flow_type=0 and flow.del_state=0";

	/** 发送试用行报告的sql ORACLE */
	public static String SEND_TEST_RUN_REPORT_SQL_ORACLE = "select t.id,"
			+ "       t.task_name,"
			+ "       t.is_controlaudit_lead,"
			+ "       t.start_time,"
			+ "       t.end_time,"
			+ "       t.state,"
			+ "       t.up_state,"
			+ "       t.task_else_state,"
			+ "       t.is_lock,"
			+ "       t.task_desc,"
			+ "       t.test_run_time,"
			+ "       t.history_vistion,"
			+ "       t.flow_type,"
			+ "       t.send_runtime_value,"
			+ "       t.r_id,"
			+ "       t.task_type,"
			+ "       t.create_person_id,"
			+ "       t.create_time,"
			+ "       t.update_time,"
			+ "       t.from_person_id,"
			+ "       t.review_counts,"
			+ "       t.approve_no_counts,"
			+ "		  jts.stage_name,"
			+ "       flow.flow_name"
			+ "       from jecn_task_bean_new t left join Jecn_User ju on ju.people_id=t.Create_Person_Id and ju.ISLOCK=0"
			+ "		  left join jecn_task_stage jts on jts.task_id=t.id and t.state=jts.stage_mark	"	
			+ "       inner join jecn_flow_structure_t flow on t.r_id=flow.flow_id"
			+ "	   where (to_date (to_char(sysdate, 'yyyy-mm-dd'),'yyyy-mm-dd') -"
			+ "      to_date(to_char(t.create_time,'yyyy-mm-dd'),'yyyy-mm-dd'))=t.send_runtime_value"
			+ "      and t.flow_type=0 and flow.del_state=0";

	/** 试运行到期提醒的sql SQLSERVER */
	public static String SEND_TEST_RUN_END_SQL_SQLSERVER = "select t.id,"
			+ "       t.task_name,"
			+ "       t.is_controlaudit_lead,"
			+ "       t.start_time,"
			+ "       t.end_time,"
			+ "       t.state,"
			+ "       t.up_state,"
			+ "       t.task_else_state,"
			+ "       t.is_lock,"
			+ "       t.task_desc,"
			+ "       t.test_run_time,"
			+ "       t.history_vistion,"
			+ "       t.flow_type,"
			+ "       t.send_runtime_value,"
			+ "       t.r_id,"
			+ "       t.task_type,"
			+ "       t.create_person_id,"
			+ "       t.create_time,"
			+ "       t.update_time,"
			+ "       t.from_person_id,"
			+ "       t.review_counts,"
			+ "       t.approve_no_counts,"
			+ "		  jts.stage_name," 
			+ "       flow.flow_name"
			+ "      from jecn_task_bean_new t left join Jecn_User ju on ju.people_id=t.Create_Person_Id and ju.ISLOCK=0"
			+ "      inner join jecn_flow_structure_t flow on t.r_id=flow.flow_id"
			+ "		 left join jecn_task_stage jts on jts.task_id=t.id and t.state=jts.stage_mark"	
			+ "      where datediff(day,t.TEST_RUN_TIME,getdate())=0"
			+ "      and t.flow_type=0 and flow.del_state=0";

	/** 试运行到期提醒的sql ORACLE */
	public static String SEND_TEST_RUN_END_SQL_ORACLE = "select t.id,"
			+ "       t.task_name,"
			+ "       t.is_controlaudit_lead,"
			+ "       t.start_time,"
			+ "       t.end_time,"
			+ "       t.state,"
			+ "       t.up_state,"
			+ "       t.task_else_state,"
			+ "       t.is_lock,"
			+ "       t.task_desc,"
			+ "       t.test_run_time,"
			+ "       t.history_vistion,"
			+ "       t.flow_type,"
			+ "       t.send_runtime_value,"
			+ "       t.r_id,"
			+ "       t.task_type,"
			+ "       t.create_person_id,"
			+ "       t.create_time,"
			+ "       t.update_time,"
			+ "       t.from_person_id,"
			+ "       t.review_counts,"
			+ "       t.approve_no_counts,"
			+ "		  jts.stage_name,"
			+ "		  flow.flow_name"
			+ "      from jecn_task_bean_new t left join Jecn_User ju on ju.people_id=t.Create_Person_Id and ju.ISLOCK=0"
			+ "		  left join jecn_task_stage jts on jts.task_id=t.id and t.state=jts.stage_mark"
			+ "       inner join jecn_flow_structure_t flow on t.r_id=flow.flow_id"
			+ "	   where to_char(t.TEST_RUN_TIME, 'yyyy-mm-dd')=to_char(sysdate,'yyyy-mm-dd')"
			+ "      and t.flow_type=0 and flow.del_state=0";

	/**
	 * 通过数据库数据获得发邮件的对象
	 * 
	 * @param obj
	 * @param adminUserList
	 * @param listSendUser
	 * @param jecnTaskBeanNew
	 */
	public static void getSendEmailObject(Object[] obj,
			List<JecnUser> adminUserList, List<JecnUser> listSendUser,
			JecnTaskBeanNew jecnTaskBeanNew) {
		if (obj.length > 0 && obj[0] != null) {
			// jecnTaskBeanNew = new JecnTaskBeanNew();
			/** 主键ID */
			jecnTaskBeanNew.setId(Long.valueOf(obj[0].toString()));
			if (obj[1] != null) {
				/** 任务名称 */
				jecnTaskBeanNew.setTaskName(obj[1].toString());
			}
			if (obj[2] != null) {
				/** 0：拟稿人主导主导审批（默认） 1：文控审核人主导审批; */
				jecnTaskBeanNew.setIsControlauditLead(Integer.valueOf(obj[2]
						.toString()));
			}
			if (obj[3] != null) {
				/** 开始时间 */
				jecnTaskBeanNew.setStartTime(JecnCommon.getDateByString(obj[3]
						.toString()));
			}
			if (obj[4] != null) {
				/** 结束时间 */
				jecnTaskBeanNew.setEndTime(JecnCommon.getDateByString(obj[4]
						.toString()));
			}
			if (obj[5] != null) {
				/**
				 * 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段
				 * 7：IT总监审批阶段
				 */
				jecnTaskBeanNew.setState(Integer.valueOf(obj[5].toString()));
			}
			if (obj[6] != null) {
				/**
				 * 0：拟稿人阶段 1：文控审核阶段 2：部门审核阶段 3：评审阶段 4：批准阶段 5：完成 6：各业务体系审批阶段
				 * 7：IT总监审批阶段
				 */
				jecnTaskBeanNew.setUpState(Integer.valueOf(obj[6].toString()));
			}
			if (obj[7] != null) {
				/**
				 * 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人)
				 * 6：二次评审 拟稿人------>评审 7：拟稿人重新提交审批 8：完成
				 * 9:打回整理意见(批准------>拟稿人)）10：交办人提交
				 */
				jecnTaskBeanNew.setTaskElseState(Integer.valueOf(obj[7]
						.toString()));
			}
			if (obj[8] != null) {
				/**
				 * 任务锁定：所有流程动作都不能做，只有解锁后才能操作 1：未锁定（默认） 0：锁定
				 */
				jecnTaskBeanNew.setIsLock(Integer.valueOf(obj[8].toString()));
			}
			if (obj[9] != null) {
				/** 任务说明 */
				jecnTaskBeanNew.setTaskDesc(obj[9].toString());
			}
			if (obj[10] != null) {
				/** 试运行周期 */
				jecnTaskBeanNew.setTestRunTime(JecnCommon
						.getDateByString(obj[10].toString()));
			}
			if (obj[11] != null) {
				/** 版本号 */
				jecnTaskBeanNew.setHistoryVistion(obj[11].toString());
			}
			if (obj[12] != null) {
				/**
				 * 0：试运行 1：是正式 2：是升级
				 */
				jecnTaskBeanNew
						.setFlowType(Integer.valueOf(obj[12].toString()));
			}
			if (obj[13] != null) {
				/** 间隔多少天发送试运行报告 */
				jecnTaskBeanNew.setSendRunTimeValue(Integer.valueOf(obj[13]
						.toString()));
			}
			if (obj[14] != null) {
				/** taskType=0时，表示流程ID，taskType=1时，表示文件ID，taskType=2时，表示制度ID */
				jecnTaskBeanNew.setRid(Long.valueOf(obj[14].toString()));
			}
			if (obj[15] != null) {
				/** 0为流程任务，1为文件任务，2为制度任务 */
				jecnTaskBeanNew.setTaskType(Integer
						.parseInt(obj[15].toString()));
			}
			if (obj[16] != null) {
				/** 创建人 */
				jecnTaskBeanNew.setCreatePersonId(Long.valueOf(obj[16]
						.toString()));
			}

			if (obj[17] != null) {
				/** 创建时间 */
				jecnTaskBeanNew.setCreateTime(JecnCommon
						.getDateByString(obj[17].toString()));
			}
			if (obj[18] != null) {
				/** 更新时间 */
				jecnTaskBeanNew.setUpdateTime(JecnCommon
						.getDateByString(obj[18].toString()));
			}
			if (obj[19] != null) {
				/** 源人 */
				jecnTaskBeanNew.setFromPeopleId(Long
						.valueOf(obj[19].toString()));
			}
			if (obj[20] != null) {
				/** 评审次数 */
				jecnTaskBeanNew.setRevirewCounts(Integer.parseInt(obj[20]
						.toString()));
			}
			if (obj[21] != null) {
				/** 驳回次数 */
				jecnTaskBeanNew.setApproveNoCounts(Integer.parseInt(obj[21]
						.toString()));
			}
			if (obj[22] != null) {
				/** 阶段名称 */
				jecnTaskBeanNew.setStateMark(obj[22].toString());
			}
			if (obj[23] != null) {
				/** 文件名称 */
				jecnTaskBeanNew.setRname(obj[23].toString());
			}
//			/**拟稿人*/
//			if (obj.length > 23 && obj[23] != null) {
//				JecnUser jecnUser = new JecnUser();
//				jecnUser.setPeopleId(Long.valueOf(obj[23].toString()));
//				if (obj.length > 24 && obj[24] != null) {
//					jecnUser.setLoginName(obj[24].toString());
//				}
//				if (obj.length > 25 && obj[25] != null) {
//					jecnUser.setTrueName(obj[25].toString());
//					jecnTaskBeanNew.setCreatePersonTemporaryName(jecnUser
//							.getTrueName());
//				}
//
//				if (obj.length > 27 && obj[26] != null && obj[27] != null) {
//					jecnUser.setEmail(obj[26].toString());
//					jecnUser.setEmailType(obj[27].toString());
//					listSendUser.add(jecnUser);
//				}
//			} else {
//				for (JecnUser jecnUser : adminUserList) {
//					if (jecnUser.getPeopleId() == null
//							|| jecnUser.getEmail() == null
//							|| "".equals(jecnUser.getEmail())
//							|| jecnUser.getEmailType() == null
//							|| "".equals(jecnUser.getEmailType())) {
//						continue;
//					}
//					listSendUser.add(jecnUser);
//				}
//			}
		}
	}
}
