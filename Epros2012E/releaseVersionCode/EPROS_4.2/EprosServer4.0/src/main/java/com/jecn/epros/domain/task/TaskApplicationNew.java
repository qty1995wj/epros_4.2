package com.jecn.epros.domain.task;

/**
 * 必填項选择表
 *
 * @author xiaohu
 */
public class TaskApplicationNew implements java.io.Serializable {
    private Long id;
    /**
     * 任务Id
     */
    private Long taskId;
    /**
     * 文件类别 0：不显示（默认） 1：显示  2 ：必填
     */
    private Integer fileType = 0;
    /**
     * 密级 0：不显示（默认） 1：显示  2 ：必填
     */
    private Integer isPublic = 0;
    /**
     * 查阅权限 0：不显示（默认） 1：显示 2 ：必填
     */
    private Integer isAccess = 0;
    /**
     * 术语定义 0：不显示（默认） 1：显示 2 ：必填
     */
    private Integer isDefinition = 0;
    /**
     * 电话 0：不显示（默认） 1：显示 2 ：必填
     */
    private Integer isPhone = 0;
    /**
     * 流程编号 0：不显示（默认） 1：显示 2 ：必填
     */
    private Integer isFlowNumber = 0;
    /**
     * 文控审核主导审批 1：文控审核主导审批，0 拟稿人主导审批
     */
    private Integer isControlAuditLead = 0;
    /**
     * 目的
     */
    private int isPurpose;
    /**
     * 适用范围
     */
    private int isApplicability;
    /**
     * 保密级别
     */
    private int isSecurityLevel;

    /**
     * 自定义输入项 0是不显示，1是显示 2 ：必填
     */
    protected int isCustomOne = 0;

    /**
     * 自定义输入项 0是不显示，1是显示 2 ：必填
     */
    protected int isCustomTwo = 0;

    /**
     * 自定义输入项 0是不显示，1是显示 2 ：必填
     */
    protected int isCustomThree = 0;

    public int getIsCustomOne() {
        return isCustomOne;
    }

    public void setIsCustomOne(int isCustomOne) {
        this.isCustomOne = isCustomOne;
    }

    public int getIsCustomTwo() {
        return isCustomTwo;
    }

    public void setIsCustomTwo(int isCustomTwo) {
        this.isCustomTwo = isCustomTwo;
    }

    public int getIsCustomThree() {
        return isCustomThree;
    }

    public void setIsCustomThree(int isCustomThree) {
        this.isCustomThree = isCustomThree;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    public Integer getIsPhone() {
        return isPhone;
    }

    public void setIsPhone(Integer isPhone) {
        this.isPhone = isPhone;
    }

    public Integer getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Integer isPublic) {
        this.isPublic = isPublic;
    }

    public Integer getIsAccess() {
        return isAccess;
    }

    public void setIsAccess(Integer isAccess) {
        this.isAccess = isAccess;
    }

    public Integer getIsDefinition() {
        return isDefinition;
    }

    public void setIsDefinition(Integer isDefinition) {
        this.isDefinition = isDefinition;
    }

    public Integer getIsFlowNumber() {
        return isFlowNumber;
    }

    public void setIsFlowNumber(Integer isFlowNumber) {
        this.isFlowNumber = isFlowNumber;
    }

    public Integer getIsControlAuditLead() {
        return isControlAuditLead;
    }

    public void setIsControlAuditLead(Integer isControlAuditLead) {
        this.isControlAuditLead = isControlAuditLead;
    }

    public int getIsPurpose() {
        return isPurpose;
    }

    public void setIsPurpose(int isPurpose) {
        this.isPurpose = isPurpose;
    }

    public int getIsApplicability() {
        return isApplicability;
    }

    public void setIsApplicability(int isApplicability) {
        this.isApplicability = isApplicability;
    }

    public int getIsSecurityLevel() {
        return isSecurityLevel;
    }

    public void setIsSecurityLevel(int isSecurityLevel) {
        this.isSecurityLevel = isSecurityLevel;
    }
}
