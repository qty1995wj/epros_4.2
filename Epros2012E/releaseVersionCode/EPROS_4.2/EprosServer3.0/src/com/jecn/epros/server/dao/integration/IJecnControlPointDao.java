package com.jecn.epros.server.dao.integration;

import com.jecn.epros.server.bean.integration.JecnControlPoint;
import com.jecn.epros.server.common.IBaseDao;
/***
 * 风险控制点表操作类
 * 2013-10-30
 *
 */
public interface IJecnControlPointDao extends IBaseDao<JecnControlPoint, String>  {

}
