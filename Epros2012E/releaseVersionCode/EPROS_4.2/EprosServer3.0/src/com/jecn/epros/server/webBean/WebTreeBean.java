package com.jecn.epros.server.webBean;

public class WebTreeBean {
	/**节点id*/
	private String id;
	/**节点名称*/
	private String text;
	/**真实的节点名称*/
	private String trueText;
	/**节点图片*/
	private String icon;
	/**是否为叶子节点*/
	private boolean leaf;
	/**节点类型*/
	private String  type;//此处不能用nodeType否则ext树加载不显示
	/**关联ID*/
	private String relationId;
	
	public String getTrueText() {
		return trueText;
	}
	public void setTrueText(String trueText) {
		this.trueText = trueText;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public boolean isLeaf() {
		return leaf;
	}
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}
	public String getRelationId() {
		return relationId;
	}
	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}

}
