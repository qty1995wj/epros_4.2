package epros.draw.system.data;

/**
 * 
 * 各种导出、保存路径记录类：为了方便用户使用之前路径
 * 
 * @author ZHOUXY
 * 
 */
public class JecnSystemFileIOData {

	/** 保存、另存为保存路径 */
	private String saveFileDirectory = null;
	/** 保存为图像保存路径 */
	private String saveImageDirectory = null;

	/** 下载操作说明存储路径 */
	private String downLoadOperationDir = null;
	/**visio导入出路径*/
	private String saveVisioDirectory = null;
	/**制度操作说明下载路径*/
	private String saveRuleOpDirectory = null;
	/**文件管理路径*/
	private String saveFileManageDirectory = null;
	/**流程文件下载*/
	private String saveFlowDirectory = null;
	/**制度本地更新*/
	private String savaRuleUpdateDirectory = null;
	public String getSaveFileDirectory() {
		return saveFileDirectory;
	}

	public void setSaveFileDirectory(String saveFileDirectory) {
		this.saveFileDirectory = saveFileDirectory;
	}

	public String getSaveImageDirectory() {
		return saveImageDirectory;
	}

	public void setSaveImageDirectory(String saveImageDirectory) {
		this.saveImageDirectory = saveImageDirectory;
	}

	public String getDownLoadOperationDir() {
		return downLoadOperationDir;
	}

	public void setDownLoadOperationDir(String downLoadOperationDir) {
		this.downLoadOperationDir = downLoadOperationDir;
	}

	public String getSaveVisioDirectory() {
		return saveVisioDirectory;
	}

	public void setSaveVisioDirectory(String saveVisioDirectory) {
		this.saveVisioDirectory = saveVisioDirectory;
	}

	public String getSaveRuleOpDirectory() {
		return saveRuleOpDirectory;
	}

	public void setSaveRuleOpDirectory(String savaRuleOpDirectory) {
		this.saveRuleOpDirectory = savaRuleOpDirectory;
	}

	public String getSaveFileManageDirectory() {
		return saveFileManageDirectory;
	}

	public void setSaveFileManageDirectory(String savaFileManageDirectory) {
		this.saveFileManageDirectory = savaFileManageDirectory;
	}

	public String getSaveFlowDirectory() {
		return saveFlowDirectory;
	}

	public void setSaveFlowDirectory(String saveFlowDirectory) {
		this.saveFlowDirectory = saveFlowDirectory;
	}

	public String getSavaRuleUpdateDirectory() {
		return savaRuleUpdateDirectory;
	}

	public void setSavaRuleUpdateDirectory(String savaRuleUpdateDirectory) {
		this.savaRuleUpdateDirectory = savaRuleUpdateDirectory;
	}

}
