package epros.designer.gui.process.flowmap;

import java.awt.AWTEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnWorkflowConstant.FileTypeEnum;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.designer.JecnFigureFileTBean;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.popup.popup.JecnPopupMenu;
import epros.draw.gui.swing.popup.table.JecnPopupTable;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;

/**
 * 
 * 流程地图添加说明连接菜单
 * 
 * @author Administrator
 * 
 */
public class DescDetailPopupMenu extends JecnPopupMenu {
	private final static Logger log = Logger
			.getLogger(DescDetailPopupMenu.class);

	/** 表头数组[文件ID,文件名称，文件类型（显示图片）] */
	private String[] headerNameArray = null;
	/** 表对象 */
	private JecnActiveFigurePopupTable table;

	/** 活动数据对象 */
	private JecnFigureData figureData = null;
	/** 0是连接，1是角色，2是活动输入，3是活动的输出，4是活动操作规范 */
	private int type = -1;

	public DescDetailPopupMenu(JecnFigureData figureData, int type) {
		if (figureData == null) {
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.figureData = figureData;
		this.type = type;

		initComponents();

		// 根据给定数据对象，初始化表行数据
		initTableData();
	}

	private void initComponents() {
		// 表对象
		table = new JecnActiveFigurePopupTable();

		this.add(table.getTableScrollPane());

		// 隐藏第一列宽度
		TableColumn tableColumnOne = table.getColumnModel().getColumn(0);
		tableColumnOne.setMinWidth(0);
		tableColumnOne.setMaxWidth(0);
		// 设置第三列宽度
		TableColumn tableColumnThree = table.getColumnModel().getColumn(2);
		tableColumnThree.setMinWidth(20);
		tableColumnThree.setMaxWidth(20);
	}

	/**
	 * 
	 * 根据给定数据对象，初始化表行数据
	 * 
	 */
	private void initTableData() {
		List<JecnFigureFileTBean> listFigureFileTBean = figureData
				.getListFigureFileTBean();
		if (listFigureFileTBean != null && listFigureFileTBean.size() > 0) {
			for (JecnFigureFileTBean figureFileTBean : listFigureFileTBean) {
				Vector<Object> v = new Vector<Object>();
				// 行数据对象
				v.add(figureFileTBean.getFileId());
				// 文件名称
				v.add(figureFileTBean.getFileName());
				// 文件图片
				v.add(getFileTypeIcon(figureFileTBean.getFileName()));

				// 添加行
				this.table.getModel().addRow(v);
			}
		}
	}

	/**
	 * 
	 * 获取文件类型图片
	 * 
	 * @param fileName
	 * @return
	 */
	private ImageIcon getFileTypeIcon(String fileName) {
		String elseFileIcon = "fileImage/" + FileTypeEnum.elsefile.toString()
				+ ".gif";
		if (DrawCommon.isNullOrEmtryTrim(fileName)) {
			return JecnFileUtil.getImageIcon(elseFileIcon);
		}

		// 获取后缀名
		String afEx = getFileExtension(fileName);
		if (DrawCommon.isNullOrEmtryTrim(afEx)) {
			return JecnFileUtil.getImageIcon(elseFileIcon);
		}

		try {
			FileTypeEnum fileTypeEnum = FileTypeEnum.valueOf(afEx);
			return JecnFileUtil.getImageIcon("fileImage/"
					+ fileTypeEnum.toString() + ".gif");
		} catch (Exception ex) {
			return JecnFileUtil.getImageIcon(elseFileIcon);
		}
	}

	/**
	 * 双击面板活动明细显示的文件名称，打开文件
	 */
	private void openSelectedFile() {
		// 隐藏面板，优化打开效果
		this.setVisible(false);
		// 判断是否选中Table中的一行，没选中，提示选中一行
		int selectRow = table.getSelectedRow();
		if (selectRow != -1) {
			Long id = Long.valueOf(table.getValueAt(selectRow, 0).toString());
			JecnDesignerCommon.openFile(id);
		} else {
			JecnOptionPane.showMessageDialog(null, JecnProperties
					.getValue("chooseOneRow"));
		}
	}

	/**
	 * 
	 * 获取文件的扩展名
	 * 
	 * @param fileName
	 *            String
	 * @return String
	 */
	private String getFileExtension(String fileName) {
		return fileName.substring(fileName.lastIndexOf(".") + 1, fileName
				.length());
	}

	/**
	 * 
	 * 表
	 * 
	 * @author ZHOUXY
	 * 
	 */
	private class JecnActiveFigurePopupTable extends JecnPopupTable {

		/**
		 * 
		 * 按键Enter或点击单元格动作事件处理方法
		 * 
		 * @param row
		 *            int 鼠标对应的行索引
		 * @param column
		 *            int 鼠标对应的列索引
		 * @param e
		 *            AWTEvent 鼠标事件或键盘事件 (MouseEvent/ActionEvent)
		 */
		@Override
		protected void cellEventProcess(int row, int column, AWTEvent e) {
			if (e instanceof MouseEvent) {
				MouseEvent evt = (MouseEvent) e;
				if (MouseEvent.MOUSE_CLICKED == evt.getID()) {
					if (evt.getClickCount() == 2) {// 鼠标双击
						openSelectedFile();
					}
				}
			}
		}

		@Override
		protected Vector<Object> getRowVector(Object popupRowData) {
			return null;
		}

		@Override
		protected String[] getTableHeaderNames() {
			if (headerNameArray == null) {
				// 表头
				headerNameArray = new String[] { "id",
						JecnProperties.getValue("activeFigureFileName"), "" };
			}
			return headerNameArray;
		}
	}
}
