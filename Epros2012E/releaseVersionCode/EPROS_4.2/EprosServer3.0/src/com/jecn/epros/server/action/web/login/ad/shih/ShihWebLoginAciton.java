package com.jecn.epros.server.action.web.login.ad.shih;

import java.io.IOException;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;

/**
 * 
 * 南京物探院：单点登录
 * 
 * @author zhouxy
 * 
 */
public class ShihWebLoginAciton extends JecnAbstractADLoginAction {
	public ShihWebLoginAciton(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		try {
			// 登录名称
			String adLoginName = loginAction.getAdLoginName();
			if (StringUtils.isBlank(adLoginName)) {// 单点登录名称为空，执行普通登录路径
				return loginAction.loginGen();
			}
			// 单点登录：通过加密登录名称获取解密后登录名称
			adLoginName = getDecryptLoginName(adLoginName);
			// 验证登录
			return this.loginByLoginName(adLoginName);

		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	/**
	 * 
	 * 通过加密登录名称获取解密后登录名称
	 * 
	 * @param loginName
	 *            String 加密登录名称
	 * @return String 解密后登录名称
	 */
	private String getDecryptLoginName(String loginName) {
		// 构造HttpClient的实例
		HttpClient httpClient = new HttpClient();

		// 创建POST方法的实例
		String allUrl = JecnShihuaAfterItem.adURL + "?id=" + loginName;

		PostMethod postMethod = new PostMethod(allUrl);

		// 使用系统提供的默认的恢复策略
		postMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler());

		try {
			// 执行postMethod
			int statusCode = httpClient.executeMethod(postMethod);

			if (statusCode != HttpStatus.SC_OK) {
				log.error("请求解密失败。statusCode=" + statusCode + ";loginName="
						+ loginName);
				return null;
			}
			// 获取解密后的登录名称
			String response = postMethod.getResponseBodyAsString();

			return response;
		} catch (IOException e) {
			log.error("请求解密http过程中异常。loginName=" + loginName, e);
			return null;
		} finally {
			if (postMethod != null) {
				postMethod.releaseConnection();
			}
		}
	}
}
