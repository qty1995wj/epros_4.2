/*      Copyright (c) 2012 JECN Corporation                     */
/*      JECN CONFIDENTIAL AND PROPRIETARY                       */
/*      All rights reserved by JECN Corporation.                */
/*      This program must be used solely for the purpose for    */
/*      which it was furnished by JECN Corporation.   No part   */
/*      of this program may be reproduced  or  disclosed  to    */
/*      others,  in  any form,  without  the  prior  written    */
/*      permission of JECN Corporation. Use  of  copyright      */
/*      notice does not evidence publication of the program.    */
package epros.draw.unredo;

import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 
 * 撤销恢复管理类
 * 
 * @author ZHOUXY
 */
public class JecnUndoRedoManager {

	/** 画图面板 */
	private JecnDrawDesktopPane workflow = null;

	/** 撤销 */
	public List<JecnUnRedoProcess> undoList = new ArrayList<JecnUnRedoProcess>();
	/** 恢复 */
	public List<JecnUnRedoProcess> redoList = new ArrayList<JecnUnRedoProcess>();

	public JecnUndoRedoManager(JecnDrawDesktopPane workflow) {
		if (workflow == null) {
			JecnLogConstants.log_UndoRedoManager
					.error("JecnUndoRedoManager类构造函数：workflow参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.workflow = workflow;
	}

	/**
	 * 
	 * 添加撤销恢复动作数据
	 * 
	 * @param edit
	 *            JecnUndoableEditProcess
	 */
	public void addEdit(JecnUnRedoProcess edit) {
		if (edit == null) {
			return;
		}
		// 添加到撤销集合中
		undoList.add(edit);

		// 清除恢复集合数据：必须清除
		if (redoList.size() > 0) {
			redoList.clear();
		}

		// 同步工具栏的撤销恢复按钮
		syncToolBar();

		// 面板是否保存标识为True
		workflow.getFlowMapData().setSave(true);
	}

	/**
	 * 
	 * 撤销
	 * 
	 */
	public void undo() {
		if (undoList.size() > 0) {
			// 集合最后一条数据
			int index = undoList.size() - 1;
			// 当前需要撤销的动作
			JecnUnRedoProcess undoableEdit = undoList.get(index);

			// 执行撤销
			undoableEdit.undo();

			workflow.repaint();
			//
			redoList.add(undoableEdit);
			undoList.remove(undoableEdit);
		}

		// 同步工具栏的撤销恢复按钮
		syncToolBar();
		// 重新添加角色移动
		workflow.removeAddRoleMove();
	}

	public void redo() {
		if (redoList.size() > 0) {
			// 集合最后一条数据
			int index = redoList.size() - 1;
			// 当前需要恢复的动作
			JecnUnRedoProcess undoableEdit = redoList.get(index);

			// 执行恢复
			undoableEdit.redo();

			workflow.repaint();
			//
			undoList.add(undoableEdit);
			redoList.remove(undoableEdit);
		}

		// 同步工具栏的撤销恢复按钮
		syncToolBar();
		// 重新添加角色移动
		workflow.removeAddRoleMove();
	}

	/**
	 * 
	 * 同步工具栏的撤销恢复按钮
	 * 
	 */
	public void syncToolBar() {
		// 撤销按钮
		JecnToolbarContentButton undoButton = JecnDrawMainPanel.getMainPanel()
				.getToolbar().getOftenPartPanel().getUndoBtn();
		// 恢复按钮
		JecnToolbarContentButton redoButton = JecnDrawMainPanel.getMainPanel()
				.getToolbar().getOftenPartPanel().getRedoBtn();

		if (undoList.size() == 0) {
			if (undoButton.isEnabled()) {// 没有撤销动作时且按钮是可编辑时，修改为不可编辑
				undoButton.setEnabled(false);
			}
		} else {
			if (!undoButton.isEnabled()) {// 有撤销动作时且按钮是不可编辑，修改为可编辑
				undoButton.setEnabled(true);
			}
		}

		if (redoList.size() == 0) {
			if (redoButton.isEnabled()) {// 没有恢复动作时且按钮是可编辑时，修改为不可编辑
				redoButton.setEnabled(false);
			}
		} else {
			if (!redoButton.isEnabled()) {// 有恢复动作时且按钮是不可编辑，修改为可编辑
				redoButton.setEnabled(true);
			}
		}
	}

	/**
	 * 
	 * 获取撤销状态是否需要激活
	 * 
	 * @return boolean true：激活 false：不激活
	 * 
	 */
	public boolean isUndoShow() {
		return (undoList.size() > 0) ? true : false;
	}

	/**
	 * 
	 * 获取撤销状态是否需要激活
	 * 
	 * @return
	 */
	public boolean isRedoShow() {
		return (redoList.size() > 0) ? true : false;
	}

	/**
	 * 清空撤销、恢复list集合
	 * 
	 */
	public void cleanRedoAndUndo() {
		this.redoList.clear();
		this.undoList.clear();
		// 同步工具栏的撤销恢复按钮
		syncToolBar();
		// 保存成功，更新面板数据中保存状态
		workflow.getFlowMapData().setSave(false);
	}
}
