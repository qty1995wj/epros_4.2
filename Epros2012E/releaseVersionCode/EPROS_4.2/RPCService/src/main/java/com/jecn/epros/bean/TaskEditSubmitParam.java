package com.jecn.epros.bean;

import java.util.List;

public class TaskEditSubmitParam extends BaseTaskParam {
	
	/** 审批人 */
	private List<AuditPeopleBean> listAuditPeople;

	public List<AuditPeopleBean> getListAuditPeople() {
		return listAuditPeople;
	}

	public void setListAuditPeople(List<AuditPeopleBean> listAuditPeople) {
		this.listAuditPeople = listAuditPeople;
	}
	
	

}
