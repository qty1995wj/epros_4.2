package com.jecn.epros.server.bean.task;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.file.JecnFileBeanT;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.task.temp.JecnTempAccessBean;

/**
 * 提交审批 组装待处理信息
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 14, 2012 时间：3:21:35 PM
 */
public class JecnTempCreateTaskBean implements java.io.Serializable {
	/** 任务主表 */
	private JecnTaskBeanNew jecnTaskBeanNew;
	/** 必填項选择表 */
	private JecnTaskApplicationNew jecnTaskApplicationNew;
	/** 任务试运行文件 */
	private JecnTaskTestRunFile jecnTaskTestRunFile;
	/** 各阶段审批人集合 */
	private List<JecnTaskApprovePeopleConn> jecnTaskApprovePeopleConnList;
	/** 记录上传的文件 */
	private List<JecnFileBeanT> listFileBeanT;
	/** 制定目标人 */
	private Set<Long> listToPeople;
	/** 流程属性信息 */
	private JecnFlowBasicInfoT flowBasicInfoT;
	/** 流程信息 */
	private JecnFlowStructureT flowStructureT;
	/** 流程地图属性信息 */
	private JecnMainFlowT mainFlowT;
	/** 制度信息 */
	private RuleT ruleT;
	/** 查阅权限对象 */
	private JecnTempAccessBean accessBean;
	/** 任务阶段表集合*/
	private List<JecnTaskStage>  JecnTaskStageList;

	public List<JecnTaskStage> getJecnTaskStageList() {
		return JecnTaskStageList;
	}

	public void setJecnTaskStageList(List<JecnTaskStage> jecnTaskStageList) {
		JecnTaskStageList = jecnTaskStageList;
	}

	public JecnTaskBeanNew getJecnTaskBeanNew() {
		return jecnTaskBeanNew;
	}

	public void setJecnTaskBeanNew(JecnTaskBeanNew jecnTaskBeanNew) {
		this.jecnTaskBeanNew = jecnTaskBeanNew;
	}

	public JecnTaskApplicationNew getJecnTaskApplicationNew() {
		return jecnTaskApplicationNew;
	}

	public void setJecnTaskApplicationNew(
			JecnTaskApplicationNew jecnTaskApplicationNew) {
		this.jecnTaskApplicationNew = jecnTaskApplicationNew;
	}

	public JecnTaskTestRunFile getJecnTaskTestRunFile() {
		return jecnTaskTestRunFile;
	}

	public void setJecnTaskTestRunFile(JecnTaskTestRunFile jecnTaskTestRunFile) {
		this.jecnTaskTestRunFile = jecnTaskTestRunFile;
	}

	public List<JecnTaskApprovePeopleConn> getJecnTaskApprovePeopleConnList() {
		return jecnTaskApprovePeopleConnList;
	}

	public void setJecnTaskApprovePeopleConnList(
			List<JecnTaskApprovePeopleConn> jecnTaskApprovePeopleConnList) {
		this.jecnTaskApprovePeopleConnList = jecnTaskApprovePeopleConnList;
	}

	public Set<Long> getListToPeople() {
		return listToPeople;
	}

	public void setListToPeople(Set<Long> listToPeople) {
		this.listToPeople = listToPeople;
	}

	public JecnFlowBasicInfoT getFlowBasicInfoT() {
		return flowBasicInfoT;
	}

	public void setFlowBasicInfoT(JecnFlowBasicInfoT flowBasicInfoT) {
		this.flowBasicInfoT = flowBasicInfoT;
	}

	public RuleT getRuleT() {
		return ruleT;
	}

	public void setRuleT(RuleT ruleT) {
		this.ruleT = ruleT;
	}

	public JecnTempAccessBean getAccessBean() {
		return accessBean;
	}

	public void setAccessBean(JecnTempAccessBean accessBean) {
		this.accessBean = accessBean;
	}

	public JecnFlowStructureT getFlowStructureT() {
		return flowStructureT;
	}

	public void setFlowStructureT(JecnFlowStructureT flowStructureT) {
		this.flowStructureT = flowStructureT;
	}

	public JecnMainFlowT getMainFlowT() {
		return mainFlowT;
	}

	public void setMainFlowT(JecnMainFlowT mainFlowT) {
		this.mainFlowT = mainFlowT;
	}

	public List<JecnFileBeanT> getListFileBeanT() {
		return listFileBeanT;
	}

	public void setListFileBeanT(List<JecnFileBeanT> listFileBeanT) {
		this.listFileBeanT = listFileBeanT;
	}
}
