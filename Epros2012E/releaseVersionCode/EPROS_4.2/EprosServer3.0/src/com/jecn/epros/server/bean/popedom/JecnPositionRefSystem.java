package com.jecn.epros.server.bean.popedom;

import java.io.Serializable;

/**
 * 岗位与制度的关系
 * @author Administrator
 *
 */
public class JecnPositionRefSystem implements Serializable {

	private Long id;//主键ID
	private Long positionId;//岗位FigureID
	private Long systemId;//制度Id
	private String systemName;//制度名称（不存放在数据库中）
	private String type;//0是部门内的制度，1是部门外的制度

	public JecnPositionRefSystem() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getSystemId() {
		return systemId;
	}

	public void setSystemId(Long systemId) {
		this.systemId = systemId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
}
