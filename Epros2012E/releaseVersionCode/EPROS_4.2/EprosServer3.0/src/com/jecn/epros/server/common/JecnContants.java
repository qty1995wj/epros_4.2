package com.jecn.epros.server.common;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.system.JecnMaintainNode;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.service.process.buss.EntryManager;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统常量类以及系统启动时需要初始化数据类
 *
 * @author Administrator
 */
public class JecnContants {
    /**
     * 错误信息标识
     */
    public static final String ERROR_NOT_PROJECT = "not exists project";

    /**
     * 用户存储在session中key标识
     */
    public static final String SESSION_KEY = "webLoginBean";
    /**
     * 用户数 JecnConfigItemServiceImpl反射赋值
     */
    public static int userInt = 0;
    /**
     * 二级管理员总数 JecnConfigItemServiceImpl反射赋值
     */
    public static int secondInt = 0;
    /**
     * 管理员 格式
     */
    public static int adminInt = 0;
    /**
     * admin（系统管理员和流程管理员） 和 设计者 分离的秘钥
     */
    public static boolean isAdminKey;

    /**
     * 数据库类型
     */
    public static DBType dbType = DBType.SQLSERVER;

    // *****************系统配置*****************//
    /**
     * 保存方式，0本地保存，1数据库大字段保存（包括版本管理）
     */
    public static int fileSaveType = 0;
    /**
     * 制度任务默认初次发布版本号*
     */
    public static String theDefaultVersionNumber = "V1.0";
    /**
     * 文件存储的位置
     */
    public static String jecn_path = getJecn_path();

    public static String getJecn_path() {
        String os = System.getProperty("os.name");
        if (os.toLowerCase().startsWith("win")) {
            return "D:/";
        }
        return "/";
    }

    /**
     * 人员同步类型（1:Excel 2:DB;3:岗位匹配,4:JSON、webservice）8：DB附带基准岗位同步,9:岗位组导入
     */
    public static int otherUserSyncType = 1;
    /**
     * 操作说明模板类型（0：老版默认版本(不做默认版本，以前公司继续)；1:保隆操作说明下载；2:东航；6:南京石化（南京物探）；
     * 8:华帝；9:恒神；11:农夫山泉；12:成都29所；13:广州机电；14:联合电子；15:新版默认版本（默认））
     * <p>
     * 最新请参照：192.168.1.56\Epros2012E\qualityRecord\10-release\Jecn-epros3.0
     * 发布补充说明-zhouxy-20130228.xls ->“安装epros注意点”->关键点“操作说明下载”项。
     */
    public static int otherOperaType = 0;
    /**
     * 操作说明显示方式 0 为表格 1为段落
     */
    public static int flowFileType = 0;

    /**
     * 流程配置--流程其他配置
     */
    public static List<JecnConfigItemBean> peopleItemList;
    /**
     * 流程地图显示隐藏
     */
    public static List<JecnConfigItemBean> processMapItemList;
    /**
     * 系统-权限配置 相关文件配置
     */
    public static Map<String, String> sysPubItemMap = new HashMap<String, String>();
    /**
     * 发布配置
     */
    public static Map<String, String> pubItemMap = new HashMap<String, String>();

    /**
     * 是否启用下载配置次数： true 启用，false 不启用
     */
    public static boolean downLoadCountIsLimited = false;
    /**
     * 下载次数配置值
     */
    public static int downLoadCountValue = 0;
    /**
     * 流程配置-流程文件定义-角色/职责属性配置：显示岗位名称
     */
    public static boolean showPosNameBox = false;
    /**
     * 流程配置-流程文件定义-活动说明属性配置：显示活动输入、输出
     */
    public static boolean showActInOutBox = false;
    /**
     * 文控信息查阅权限配置:0:系统管理员、浏览管理员(默认)；1：具有流程、流程架构、文件、制度查阅权限的人
     */
    public static int docControlPermission = 0;

    /**
     * 数据字典
     **/
    public static Map<DictionaryEnum, List<JecnDictionary>> dictionaryMap = new HashMap<DictionaryEnum, List<JecnDictionary>>();
    /**
     * 登录类型（0：普通登录（默认）;
     * 1:东航;2:中国科学院计算机网络信息中心;3:三幅百货单点登录；4：中石化&南京物探院;5:中数通信息有限公司;
     * 6:聚光;7:上海保隆;8：华帝；9：中电投；10：海信；11:29所；12:安健致远；13:大唐移动；14:联合电子
     * ;28:立邦,30:东软医疗 ,31:厦门金旅；36:英维克,37:蒙牛，38：爱普生，39：时代新材,40:科大讯飞，41：木林森）
     * <p>
     * 最新请参照：192.168.1.56\Epros2012E\qualityRecord\10-release\Jecn-epros3.0
     * 发布补充说明-zhouxy-20130228.xls ->“安装epros注意点”->关键点“登录方式类型”项。
     */
    public static int otherLoginType = 0;
    /**
     * 版本类型 0(p1)：有浏览端标准版（默认）；99(p2)：有浏览端完整版；1(d2)：无浏览端标准版；100(d3)：无浏览端完整版
     */
    public static int versionType = 0;
    /**
     * 流程元素编辑点:1：左上进右下出（默认）；0：四个编辑点都能进出
     */
    public static int editPortType = 1;

    // //////////////权限////////////////
    /**
     * 浏览权限配置 流程地图 true是公开，false是密码
     */
    public static boolean isMainFlowAdmin = false;
    /**
     * 浏览权限配置 流程 true是公开，false是密码
     */
    public static boolean isFlowAdmin = false;
    /**
     * 浏览权限配置 组织 true是公开，false是密码
     */
    public static boolean isOrgAdmin = false;
    /**
     * 浏览权限配置 岗位 true是公开，false是密码
     */
    public static boolean isStationAdmin = false;
    /**
     * 浏览权限配置 标准 true是公开，false是密码
     */
    public static boolean isCriterionAdmin = false;
    /**
     * 浏览权限配置 制度 true是公开，false是密码
     */
    public static boolean isSystemAdmin = false;
    /**
     * 浏览权限配置 文件 true是公开，false是密码
     */
    public static boolean isFileAdmin = false;
    // //////////////权限////////////////

    // //////////////审批环节 按序号排序////////////////
    /**
     * 流程图审批环节
     */
    public static List<JecnConfigItemBean> partMapList = new ArrayList<JecnConfigItemBean>();
    /**
     * 流程地图审批环节
     */
    public static List<JecnConfigItemBean> totalMapList = new ArrayList<JecnConfigItemBean>();
    /**
     * 文件审批环节
     */
    public static List<JecnConfigItemBean> fileList = new ArrayList<JecnConfigItemBean>();
    /**
     * 制度审批环节
     */
    public static List<JecnConfigItemBean> roleList = new ArrayList<JecnConfigItemBean>();
    /**
     * 邮件审批环节
     */
    public static List<JecnConfigItemBean> emailList = null;
    /**
     * 系统配置--消息邮件配置
     */
    public static List<JecnConfigItemBean> flowEndDateList = null;
    /**
     * 密级：0：秘密，1：公开
     */
    public static boolean proPubOrSecRation = true;
    /**
     * 设计器系统管理员是否配置处理建议：0：默认都不选中，1：选中
     */
    public static boolean proSystemRation = true;
    /**
     * 设计器流程责任人是否配置处理建议：0：默认都不选中，1：选中
     */
    public static boolean proFlowRefRation = false;

    // //////////////审批环节////////////////
    // *****************系统配置 按序号排序*****************//
    /**
     * 邮箱格式
     */
    public static final String EMAIL_FORMAT = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

    public static final Map<String, JecnMaintainNode> LANGUAGES = new HashMap<String, JecnMaintainNode>();

    /**
     * 流程、架构树节点是否显示编号 0：默认都不选中，1：选中
     */
    public static boolean processShowNumber = false;

    /**
     * 制度、风险、标准、文件：浏览端双击目录默认显示清单，默认显示 true
     */
    public static boolean isShowDirList = true;

    public static String SHOW_VALUE = "1";

    /**
     * 系统配置缓存数据
     */
    public static Map<String, JecnConfigItemBean> configMaps = new HashMap<String, JecnConfigItemBean>();

    /**
     * 域后缀
     */
    public static String DOMAIN_SUFFIX = null;

    /**
     * 所有的审批环节
     */
    public static List<JecnConfigItemBean> allApproveList = new ArrayList<JecnConfigItemBean>();

    public static String NEW_PROJECT_NAME = "";
    public static String COOKIE_DOMAIN = null;
    public static String NEW_APP_CONTEXT = null;
    public static String NEW_APP_CONTEXT_APP = null;
    /**
     * 4.0系统配置项地址
     */
    public static String SERVER4_SYSTEM_CONFIG = "/system/sync_system_config";
    /****
     * 4.0项目后台服务ip地址
     */
    public static String NEW_APP_SERVER_ADDRESS = null;

    public static EntryManager ENTRY_MANAGER = null;

    /**
     * @return
     * @author yxw 2013-7-22
     * @description:0(p1)：有浏览端标准版（默认）；99(p2)：有浏览端完整版；1(D2)：无浏览端标准版；100(D3)：无浏览端完整版
     */
    public static boolean isPubShow() {
        if (versionType == 1 || versionType == 100) {
            return false;
        }
        return true;
    }

    public static String getValue(String mark) {
        JecnConfigItemBean itemBean = configMaps.get(mark);
        if (itemBean == null) {
            return null;
        }
        return itemBean.getValue();
    }

    public static boolean isAdminKey() {
        return isAdminKey;
    }

    /***
     * 获得数据字典的值
     *
     * @param code
     * @param s
     * @return
     */
    public static String getValueByCode(String code, DictionaryEnum s) {
        List<JecnDictionary> list = dictionaryMap.get(s);
        for (JecnDictionary dictionary : list) {
            if (dictionary != null && dictionary.getCode() != null && code.equals(dictionary.getCode().toString())) {
                return dictionary.getValue() == null ? "" : dictionary.getValue();
            }
        }
        return "";
    }

    public static boolean isFlowAdmin() {
        // 是不是搜索所有流程节点
        return JecnContants.isFlowAdmin || JecnCommon.isAdmin() ? true : false;
    }

    public static boolean isMainFlowAdmin() {
        // 是不是搜索所有流程节点
        return JecnContants.isMainFlowAdmin || JecnCommon.isAdmin() ? true : false;
    }

    public static boolean isSystemAdmin() {
        // 是不是搜索所有流程节点
        return JecnContants.isSystemAdmin || JecnCommon.isAdmin() ? true : false;
    }

    public static boolean isFileAdmin() {
        // 是不是搜索所有流程节点
        return JecnContants.isFileAdmin || JecnCommon.isAdmin() ? true : false;
    }

    public static boolean isCriterionAdmin() {
        // 是不是搜索所有流程节点
        return JecnContants.isCriterionAdmin || JecnCommon.isAdmin() ? true : false;
    }
}
