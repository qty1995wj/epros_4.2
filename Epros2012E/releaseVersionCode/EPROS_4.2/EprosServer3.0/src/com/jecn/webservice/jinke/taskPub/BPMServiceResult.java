/**
 * BPMServiceResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class BPMServiceResult  implements java.io.Serializable {
    private boolean success;

    private java.lang.String instanceID;

    private java.lang.String message;

    private java.lang.String workItemID;

    private java.lang.String workItemUrl;

    public BPMServiceResult() {
    }

    public BPMServiceResult(
           boolean success,
           java.lang.String instanceID,
           java.lang.String message,
           java.lang.String workItemID,
           java.lang.String workItemUrl) {
           this.success = success;
           this.instanceID = instanceID;
           this.message = message;
           this.workItemID = workItemID;
           this.workItemUrl = workItemUrl;
    }


    /**
     * Gets the success value for this BPMServiceResult.
     * 
     * @return success
     */
    public boolean isSuccess() {
        return success;
    }


    /**
     * Sets the success value for this BPMServiceResult.
     * 
     * @param success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }


    /**
     * Gets the instanceID value for this BPMServiceResult.
     * 
     * @return instanceID
     */
    public java.lang.String getInstanceID() {
        return instanceID;
    }


    /**
     * Sets the instanceID value for this BPMServiceResult.
     * 
     * @param instanceID
     */
    public void setInstanceID(java.lang.String instanceID) {
        this.instanceID = instanceID;
    }


    /**
     * Gets the message value for this BPMServiceResult.
     * 
     * @return message
     */
    public java.lang.String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this BPMServiceResult.
     * 
     * @param message
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }


    /**
     * Gets the workItemID value for this BPMServiceResult.
     * 
     * @return workItemID
     */
    public java.lang.String getWorkItemID() {
        return workItemID;
    }


    /**
     * Sets the workItemID value for this BPMServiceResult.
     * 
     * @param workItemID
     */
    public void setWorkItemID(java.lang.String workItemID) {
        this.workItemID = workItemID;
    }


    /**
     * Gets the workItemUrl value for this BPMServiceResult.
     * 
     * @return workItemUrl
     */
    public java.lang.String getWorkItemUrl() {
        return workItemUrl;
    }


    /**
     * Sets the workItemUrl value for this BPMServiceResult.
     * 
     * @param workItemUrl
     */
    public void setWorkItemUrl(java.lang.String workItemUrl) {
        this.workItemUrl = workItemUrl;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BPMServiceResult)) return false;
        BPMServiceResult other = (BPMServiceResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.success == other.isSuccess() &&
            ((this.instanceID==null && other.getInstanceID()==null) || 
             (this.instanceID!=null &&
              this.instanceID.equals(other.getInstanceID()))) &&
            ((this.message==null && other.getMessage()==null) || 
             (this.message!=null &&
              this.message.equals(other.getMessage()))) &&
            ((this.workItemID==null && other.getWorkItemID()==null) || 
             (this.workItemID!=null &&
              this.workItemID.equals(other.getWorkItemID()))) &&
            ((this.workItemUrl==null && other.getWorkItemUrl()==null) || 
             (this.workItemUrl!=null &&
              this.workItemUrl.equals(other.getWorkItemUrl())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isSuccess() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getInstanceID() != null) {
            _hashCode += getInstanceID().hashCode();
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        if (getWorkItemID() != null) {
            _hashCode += getWorkItemID().hashCode();
        }
        if (getWorkItemUrl() != null) {
            _hashCode += getWorkItemUrl().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BPMServiceResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "BPMServiceResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("success");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Success"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "InstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workItemID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "WorkItemID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workItemUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "WorkItemUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
