package com.jecn.epros.domain.task;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import com.jecn.epros.util.ConfigUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 任务信息
 *
 * @author xiaohu
 */
public class SimpleTaskBean {

    private MessageResult result;
    /**
     * 任务基本属性
     */
    private TaskBean taskBeanNew;

    /**
     * 显示项
     */
    private TaskApplicationNew taskApplicationNew;

    /**
     * 试运行 文件
     */
    private String taskRunFileId;
    /**
     * 试运行 文件
     */
    private String taskRunFileName;
    private Long historyId;


    /**
     * 提交人员名称信息 人员信息
     */
    private List<TempAuditPeopleBean> listAuditPeopleBean;
    /**
     * 任务审批记录
     */
    private List<TaskForRecodeNew> listTaskForRecodeNew;

    /**
     * 任务相关资源
     **/
    private SimpleTaskRelatedResource relatedResource;

    /**
     * 搁置原因
     */
    private String delayReason;

    private List<TaskButton> taskButtons;

    /**
     * 任务流转记录
     **/
    private List<ApproveRecord> approveRecords;

    /**
     * 任务界面编辑配置项
     */
    private TaskEidtConfig taskEditConfig;

    /**
     * 相关文件
     */
    private List<LinkResource> relatedFiles;


    /**
     * 支撑文件 相关制度 相关标准 相关风险 相关流程 相关标准化文件
     */
    private List<LinkResource> supportingFiles;

    private String isApp;

    /**
     * 自定义输入项名称1
     **/
    private String customInputItemOneName;
    /**
     * 自定义输入项名称1
     **/
    private String customInputItemTwoName;
    /**
     * 自定义输入项3
     **/
    private String customInputItemThreeName;

    /**
     * 是否隐藏重新提交人员选择按钮  0 隐藏 1显示
     */
    private int isShowPepoleButton = 1;

    public String getCustomInputItemOneName() {
        return customInputItemOneName;
    }


    public int getIsShowPepoleButton() {
        if (ConfigUtils.isJiuXinLoginType() && taskBeanNew.getTaskType() != 4) { //九新药业并且不为流程架构时隐藏人员选择按钮
            this.isShowPepoleButton = 0;
        }
        return isShowPepoleButton;
    }

    public List<LinkResource> getSupportingFiles() {
        return supportingFiles;
    }

    public void setSupportingFiles(List<LinkResource> supportingFiles) {
        this.supportingFiles = supportingFiles;
    }

    public void setIsShowPepoleButton(int isShowPepoleButton) {
        this.isShowPepoleButton = isShowPepoleButton;
    }

    public void setCustomInputItemOneName(String customInputItemOneName) {
        this.customInputItemOneName = customInputItemOneName;
    }

    public String getCustomInputItemTwoName() {
        return customInputItemTwoName;
    }

    public void setCustomInputItemTwoName(String customInputItemTwoName) {
        this.customInputItemTwoName = customInputItemTwoName;
    }

    public String getCustomInputItemThreeName() {
        return customInputItemThreeName;
    }

    public void setCustomInputItemThreeName(String customInputItemThreeName) {
        this.customInputItemThreeName = customInputItemThreeName;
    }

    /**
     * 页面显示人员名称信息 人员信息
     * 如下阶段就是必须存在的阶段
     */
    public List<TempAuditPeopleBean> getListAudit() {
        List<TempAuditPeopleBean> listAudit = new ArrayList<>();
        boolean joinIsExist = false;
        if (listAuditPeopleBean != null) {
            for (TempAuditPeopleBean tempAuditPeopleBean : listAuditPeopleBean) {
                if (tempAuditPeopleBean.getState() == 10 && joinIsExist) {
                    listAudit.add(tempAuditPeopleBean);
                } else {
                    if (tempAuditPeopleBean.getIsShow() == 1 && tempAuditPeopleBean.getIsSelectedUser() == 1) {
                        listAudit.add(tempAuditPeopleBean);
                        if (tempAuditPeopleBean.getState() == 3) {
                            joinIsExist = true;
                        }
                    }
                }
            }
        }
        return listAudit;
    }

    public LinkResource getTestRunFileLink() {
        if (StringUtils.isBlank(taskRunFileId)) {
            return null;
        }
        return new LinkResource(taskRunFileId, taskRunFileName, ResourceType.TEST_RUN_FILE);
    }

    public TaskEidtConfig getTaskEditConfig() {
        return taskEditConfig;
    }

    public void setTaskEditConfig(TaskEidtConfig taskEditConfig) {
        this.taskEditConfig = taskEditConfig;
    }

    public List<ApproveRecord> getApproveRecords() {
        return approveRecords;
    }

    public void setApproveRecords(List<ApproveRecord> approveRecords) {
        this.approveRecords = approveRecords;
    }

    public List<TaskButton> getTaskButtons() {
        return taskButtons;
    }

    public void setTaskButtons(List<TaskButton> taskButtons) {
        this.taskButtons = taskButtons;
    }

    /**
     * 根据任务类型获取对应的编号名称
     *
     * @return
     */

    public TaskBean getTaskBeanNew() {
        return taskBeanNew;
    }

    public void setTaskBeanNew(TaskBean taskBeanNew) {
        this.taskBeanNew = taskBeanNew;
    }

    public TaskApplicationNew getTaskApplicationNew() {
        return taskApplicationNew;
    }

    public void setTaskApplicationNew(TaskApplicationNew taskApplicationNew) {
        this.taskApplicationNew = taskApplicationNew;
    }

    @JsonIgnore
    public String getTaskRunFileId() {
        return taskRunFileId;
    }

    public void setTaskRunFileId(String taskRunFileId) {
        this.taskRunFileId = taskRunFileId;
    }

    @JsonIgnore
    public String getTaskRunFileName() {
        return taskRunFileName;
    }

    public void setTaskRunFileName(String taskRunFileName) {
        this.taskRunFileName = taskRunFileName;
    }

    public List<TempAuditPeopleBean> getListAuditPeopleBean() {
        return listAuditPeopleBean;
    }

    public void setListAuditPeopleBean(List<TempAuditPeopleBean> listAuditPeopleBean) {
        this.listAuditPeopleBean = listAuditPeopleBean;
    }

    public List<TaskForRecodeNew> getListTaskForRecodeNew() {
        return listTaskForRecodeNew;
    }

    public void setListTaskForRecodeNew(List<TaskForRecodeNew> listtaskForRecodeNew) {
        this.listTaskForRecodeNew = listtaskForRecodeNew;
    }

    public String getDelayReason() {
        return delayReason;
    }

    public void setDelayReason(String delayReason) {
        this.delayReason = delayReason;
    }

    public SimpleTaskRelatedResource getRelatedResource() {
        return relatedResource;
    }

    public void setRelatedResource(SimpleTaskRelatedResource relatedResource) {
        this.relatedResource = relatedResource;
    }

    public List<LinkResource> getRelatedFiles() {
        return relatedFiles;
    }

    public void setRelatedFiles(List<LinkResource> relatedFiles) {
        this.relatedFiles = relatedFiles;
    }

    public String getIsApp() {
        return isApp;
    }

    public void setIsApp(String isApp) {
        this.isApp = isApp;
    }

    public MessageResult getResult() {
        return result;
    }

    public void setResult(MessageResult result) {
        this.result = result;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }
}
