package epros.draw.visioxml.visioinput;

import visioxml.bean.CharProperty;
import visioxml.bean.FillProperty;
import visioxml.bean.LineProperty;
import visioxml.bean.ParaProperty;
import visioxml.bean.TextBlockProperty;
import visioxml.bean.XForm1DProperty;
import visioxml.bean.XFormProperty;

/**
 * visio原始数据对象
 * 
 * @author admin
 * 
 */
class VisioShape {
	/** 形状ID **/
	private String id;
	/** namuU属性 **/
	private String nameU;
	/** Xfrom图形位置点 **/
	private XFormProperty xfrom;
	/** Xfrom1D 1D图形才存在 **/
	private XForm1DProperty xfrom1D;
	/** 图形填充属性 **/
	private FillProperty fill;
	/** 图形字体属性* */
	private CharProperty visioChar;
	/** 线属性* */
	private LineProperty line;
	/** 文本快属性* */
	private TextBlockProperty textBlock;
	/** 段落属性* */
	private ParaProperty para;
	/** 显示的文本 **/
	private String text;
	/** 活动编号 **/
	private String actNum;
	/** 图形层级 **/
	private int zIndex;

	public String getNameU() {
		return nameU;
	}

	public void setNameU(String nameU) {
		this.nameU = nameU;
	}

	public XFormProperty getXfrom() {
		return xfrom;
	}

	public void setXfrom(XFormProperty xfrom) {
		this.xfrom = xfrom;
	}

	public XForm1DProperty getXfrom1D() {
		return xfrom1D;
	}

	public void setXfrom1D(XForm1DProperty xfrom1d) {
		xfrom1D = xfrom1d;
	}

	public FillProperty getFill() {
		return fill;
	}

	public void setFill(FillProperty fill) {
		this.fill = fill;
	}

	public CharProperty getVisioChar() {
		return visioChar;
	}

	public void setVisioChar(CharProperty visioChar) {
		this.visioChar = visioChar;
	}

	public LineProperty getLine() {
		return line;
	}

	public void setLine(LineProperty line) {
		this.line = line;
	}

	public TextBlockProperty getTextBlock() {
		return textBlock;
	}

	public void setTextBlock(TextBlockProperty textBlock) {
		this.textBlock = textBlock;
	}

	public ParaProperty getPara() {
		return para;
	}

	public void setPara(ParaProperty para) {
		this.para = para;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getActNum() {
		return actNum;
	}

	public void setActNum(String actNum) {
		this.actNum = actNum;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getzIndex() {
		return zIndex;
	}

	public void setzIndex(int zIndex) {
		this.zIndex = zIndex;
	}
}
