package epros.designer.gui.common;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;

import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

public abstract class JecnSortDialog extends JecnDialog {
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 结果滚动面板 */
	private JScrollPane tableScrollPane = null;

	/** 按钮面板 */
	private JPanel buttonsPanel = null;
	/** 上移按钮 */
	private JButton upButton = null;
	/** 下移按钮 */
	private JButton downButton = null;
	/** 确定按钮 */
	private JButton okButton = null;
	/** 取消按钮 */
	private JButton cancelButton = null;
	private List<JecnTreeBean> sortList = null;
	private JecnTable jecnTable = null;

	/** 选中的节点 */
	private JecnTreeNode selectNode = null;
	/** 是否确定 */
	private boolean isFlag = false;

	private JecnTree jTree = null;

	public JecnSortDialog(JecnTreeNode selectNode, JecnTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		sortList = getChild();
		initCompotents();
		initLayout();
		buttonAcitonInit();
		this.setModal(true);
		this.setLocationRelativeTo(null);

	}

	public JecnTreeNode getSelectNode() {
		return selectNode;
	}

	/**
	 * 获得子集的集合
	 * 
	 * @author yxw 2012-5-9
	 * @description:
	 * @return
	 */
	public abstract List<JecnTreeBean> getChild();

	/**
	 * @author yxw 2012-5-8
	 * @description:初始化组件
	 */
	public void initCompotents() {
		this.setTitle(JecnProperties.getValue("nodeSort"));
		this.setSize(getWidthMax(), 464);
		this.setResizable(false);
		this.setModal(true);
		this.mainPanel = new JPanel();
		this.mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		Dimension dimension = new Dimension(getWidthMax() - 14, 390);
		this.tableScrollPane = new JScrollPane();
		tableScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		tableScrollPane.setPreferredSize(dimension);
		tableScrollPane.setMinimumSize(dimension);

		this.buttonsPanel = new JPanel();
		this.buttonsPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		dimension = new Dimension(555, 35);
		buttonsPanel.setPreferredSize(dimension);
		buttonsPanel.setMinimumSize(dimension);
		this.upButton = new JButton(JecnProperties.getValue("upBtn"));
		this.downButton = new JButton(JecnProperties.getValue("downBtn"));
		this.okButton = new JButton(JecnProperties.getValue("okBtn"));
		this.cancelButton = new JButton(JecnProperties.getValue("cancelBtn"));

	}

	/**
	 * @author yxw 2012-5-8
	 * @description:dialog布局方法
	 */
	public void initLayout() {
		// 主面板布局-开始
		mainPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		mainPanel.add(tableScrollPane);
		mainPanel.add(buttonsPanel);
		jecnTable = new JecnSortTable();
		// 设置序号的宽度
		jecnTable.getColumnModel().getColumn(0).setMaxWidth(120);
		jecnTable.getColumnModel().getColumn(0).setMinWidth(120);

		tableScrollPane.setViewportView(jecnTable);

		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonsPanel.add(upButton);
		buttonsPanel.add(downButton);
		buttonsPanel.add(okButton);
		buttonsPanel.add(cancelButton);
		// 把主面板增加到dialog
		this.getContentPane().add(mainPanel);

	}

	/**
	 * @author yxw 2012-5-8
	 * @description:按钮事件
	 */
	private void buttonAcitonInit() {

		// 上移
		upButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				upButtonAction();
			}
		});
		// 下移
		downButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				downButtonAction();
			}
		});
		// 确定
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 取消
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
	}

	// 上移
	public void upButtonAction() {
		jecnTable.moveUpRows();
		JecnTableModel model = (JecnTableModel) jecnTable.getModel();
		int rows = model.getRowCount();
		for (int i = 0; i < rows; i++) {
			model.setValueAt(String.valueOf(i + 1), i, 0);
		}

	}

	// 下移
	public void downButtonAction() {
		jecnTable.moveDownRows();
		JecnTableModel model = (JecnTableModel) jecnTable.getModel();
		int rows = model.getRowCount();
		for (int i = 0; i < rows; i++) {
			model.setValueAt(String.valueOf(i + 1), i, 0);
		}
	}

	// 确定
	public void okButtonAction() {
		List<JecnTreeDragBean> listResult = updateRecord();

		if (listResult.size() > 0) {
			if (saveData(listResult)) {
				try {
					ConnectionPool.getFileAction().updateViewSort(selectNode.getJecnTreeBean().getTreeNodeType(),
							selectNode.getJecnTreeBean().getId());
				} catch (Exception e) {
					log.error("", e);
				}
				JecnTreeCommon.sortNodes(jTree, selectNode, listResult);
				this.dispose();
			}
		}
		isFlag = true;
	}

	// 取消
	private void cancelButtonAction() {
		this.dispose();
	}

	/**
	 * @author yxw 2012-5-9
	 * @description:保存数据库
	 */
	public abstract boolean saveData(List<JecnTreeDragBean> list);

	/**
	 * 重新封装结果
	 * 
	 * @return
	 */
	public List<JecnTreeDragBean> updateRecord() {
		JecnTableModel model = (JecnTableModel) jecnTable.getModel();
		int rows = model.getRowCount();
		List<JecnTreeDragBean> list = new ArrayList<JecnTreeDragBean>();
		for (int i = 0; i < rows; i++) {
			if (model.getValueAt(i, 0) == null || "".equals(model.getValueAt(i, 0).toString())) {
				continue;
			}
			if (model.getValueAt(i, 1) == null || "".equals(model.getValueAt(i, 1).toString())) {
				continue;
			}
			if (model.getValueAt(i, 4) == null || "".equals(model.getValueAt(i, 4).toString())) {
				continue;
			}

			JecnTreeDragBean jecnTreeDragBean = new JecnTreeDragBean();
			jecnTreeDragBean.setSortId(i + 1);
			jecnTreeDragBean.setTreeNodeType(JecnUtil.getTreeNodeType(model.getValueAt(i, 4).toString()));
			jecnTreeDragBean.setId(Long.valueOf(model.getValueAt(i, 1).toString()));
			list.add(jecnTreeDragBean);
		}
		return list;
	}

	class JecnSortTable extends JecnTable {
		@Override
		public JecnTableModel getTableModel() {
			return new JecnTableModel(getTitleTable(), getContentTable());
		}

		@Override
		public boolean isSelectMutil() {
			return true;
		}

		@Override
		public int[] gethiddenCols() {
			return getHiddenCols();
		}

	}

	protected int[] getHiddenCols() {
		return new int[] { 1, 4 };
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得table的标题
	 * @return
	 */
	private Vector<String> getTitleTable() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("sort"));
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		title.add(JecnProperties.getValue("type"));
		title.add("type");
		return title;
	}

	/**
	 * @author yxw 2012-5-10
	 * @description:获得table的内容
	 * @return
	 */
	private Vector<Vector<String>> getContentTable() {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		for (int i = 0; i < sortList.size(); i++) {
			JecnTreeBean jecnTreeBean = sortList.get(i);
			Vector<String> data = new Vector<String>();
			data.add(String.valueOf(i + 1));
			data.add(jecnTreeBean.getId().toString());
			data.add(jecnTreeBean.getName());
			String typeName = "";
			switch (jecnTreeBean.getTreeNodeType()) {
			case processMap:
			case processMapMode:
				typeName = JecnProperties.getValue("processMap");
				break;
			case process:
			case processMode:
			case processFile:
				typeName = JecnProperties.getValue("processFlow");
				break;
			case organization:
				typeName = JecnProperties.getValue("organization");
				break;
			case position:
				typeName = JecnProperties.getValue("position");
				break;
			case standard:
			case standardProcess:
			case standardProcessMap:
			case standardClause:
				typeName = JecnProperties.getValue("standard");
				break;
			case standardDir:
				typeName = JecnProperties.getValue("standardDir");
				break;
			case ruleFile:
			case ruleModeFile:
				typeName = JecnProperties.getValue("rule");
				break;
			case ruleDir:
				typeName = JecnProperties.getValue("ruleDir");
				break;
			case file:
				typeName = JecnProperties.getValue("file");
				break;
			case fileDir:
				typeName = JecnProperties.getValue("fileDir");
				break;
			case positionGroup:
				typeName = JecnProperties.getValue("positionGroup");
				break;
			case positionGroupDir:
			case roleDir:
			case ruleModeDir:
				typeName = JecnProperties.getValue("dir");
				break;
			case role:
				typeName = JecnProperties.getValue("role");
				break;
			case tool:
				typeName = JecnProperties.getValue("flowTool");
				break;
			case ruleMode:
				typeName = JecnProperties.getValue("ruleMode");
				break;
			case riskDir:
				typeName = JecnProperties.getValue("riskDir");
				break;
			case riskPoint:
				typeName = JecnProperties.getValue("riskPoint");
				break;
			case innerControlDir:
				typeName = JecnProperties.getValue("dir");
				break;
			case innerControlClause:
				typeName = JecnProperties.getValue("clause");
				break;
			default:
				break;
			}
			data.add(typeName);
			data.add(jecnTreeBean.getTreeNodeType().toString());
			content.add(data);
		}
		return content;
	}

	public JecnTree getjTree() {
		return jTree;
	}

	public boolean isFlag() {
		return isFlag;
	}

	public void setFlag(boolean isFlag) {
		this.isFlag = isFlag;
	}

}
