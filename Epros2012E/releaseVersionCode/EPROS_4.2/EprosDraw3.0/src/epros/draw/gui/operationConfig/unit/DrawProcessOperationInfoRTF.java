package epros.draw.gui.operationConfig.unit;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.style.RtfFont;
import com.lowagie.text.rtf.style.RtfParagraphStyle;
import com.lowagie.text.rtf.table.RtfBorder;
import com.lowagie.text.rtf.table.RtfBorderGroup;
import com.lowagie.text.rtf.table.RtfCell;

import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.operationConfig.DrawOperationDescriptionConfigBean;
import epros.draw.gui.operationConfig.bean.JecnActivityShowBean;
import epros.draw.gui.operationConfig.bean.JecnFlowOperationBean;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnResourceUtil;

/**
 * 操作说明下载
 * 
 * @author ZHANGXH
 */
public class DrawProcessOperationInfoRTF {
	private static final Log log = LogFactory.getLog(DrawProcessOperationInfoRTF.class);
	/** 无 */
	private static String none = JecnResourceUtil.getJecnResourceUtil().getValue("none");
	/** 关键活动 */
	private static String keyActivities = JecnResourceUtil.getJecnResourceUtil().getValue("keyActivities");
	/** 活动编号 */
	private static String activitynumbers = JecnResourceUtil.getJecnResourceUtil().getValue("activitynumbers");
	/** 活动名称 */
	private static String activityTitle = JecnResourceUtil.getJecnResourceUtil().getValue("activityTitle");
	/** 活动说明 */
	private static String activityIndicatingThat = JecnResourceUtil.getJecnResourceUtil().getValue(
			"activityIndicatingThat");
	/** 输入 */
	private static String activeInput = JecnResourceUtil.getJecnResourceUtil().getValue("input");
	/** 输出 */
	private static String activeOutput = JecnResourceUtil.getJecnResourceUtil().getValue("output");
	/** 关键说明 */
	private static String keyShow = JecnResourceUtil.getJecnResourceUtil().getValue("keyShow");
	/** 问题区域 */
	private static String problemAreas = JecnResourceUtil.getJecnResourceUtil().getValue("problemAreas");
	/** 关键成功因素 */
	private static String keySuccessFactors = JecnResourceUtil.getJecnResourceUtil().getValue("keySuccessFactors");
	/** 关键控制点 */
	private static String keyControlPoint = JecnResourceUtil.getJecnResourceUtil().getValue("keyControlPoint");
	/** 角色名称 */
	private static String roleName = JecnResourceUtil.getJecnResourceUtil().getValue("roleName");
	/** 角色职责 */
	private static String roleResponsibility = JecnResourceUtil.getJecnResourceUtil().getValue("roleResponsibility");
	/** 执行角色 */
	private static String executiveRole = JecnResourceUtil.getJecnResourceUtil().getValue("executiveRole");
	/** 时间 */
	private static String time = JecnResourceUtil.getJecnResourceUtil().getValue("time");
	/** 拟稿人 */
	private static String peopleMakeADraft = JecnResourceUtil.getJecnResourceUtil().getValue("peopleMakeADraft");
	/** 文控审核人 */
	private static String documentControlAuditPeople = JecnResourceUtil.getJecnResourceUtil().getValue(
			"documentControlAuditPeople");
	/** 部门审核人 */
	private static String departmentAuditPeople = JecnResourceUtil.getJecnResourceUtil().getValue(
			"departmentAuditPeople");
	/** 评审人 */
	private static String reviewPeople = JecnResourceUtil.getJecnResourceUtil().getValue("reviewPeople");
	/** 批准人 */
	private static String forPeople = JecnResourceUtil.getJecnResourceUtil().getValue("forPeople");

	/**
	 * 导出流程操作说明 创建word文档
	 * 
	 * @param operationBean
	 *            流程操作说明数据对象
	 * @param img
	 *            导出操作说明所需流程图图片
	 * @param operationDescriptionConfigBeanList
	 *            操作说明显示项配置信息
	 * @param path
	 *            导入操作说明的存储路径
	 * @param isEnglish
	 *            是否为英文
	 * @throws DocumentException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static void createWordFile(JecnFlowOperationBean operationBean, Image img,
			List<DrawOperationDescriptionConfigBean> operationDescriptionConfigBeanList, String path, boolean isEnglish)
			throws DocumentException, MalformedURLException {

		if (operationBean.getFlowName() == null) {// 流程名称
			return;
		}
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);
		if (!path.endsWith(".doc")) {
			path = path + ".doc";
		}
		// 判断文件是否存在
		File f = new File(path);
		if (f.exists()) {
			int result = JecnOptionPane.showConfirmDialog(null, JecnResourceUtil.getJecnResourceUtil().getValue(
					"theCurrentFileAlreadyExistsIsCovered"),
					JecnResourceUtil.getJecnResourceUtil().getValue("confirm"), JOptionPane.YES_NO_OPTION);
			if (result != JOptionPane.YES_NO_OPTION) {
				return;
			}
		}
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(path);
		} catch (FileNotFoundException e) {
			JecnOptionPane.showMessageDialog(null, e.getMessage(), null, JOptionPane.ERROR_MESSAGE);
			log.error("", e);
			return;
		}

		RtfWriter2.getInstance(document, fos);

		document.open();
		// 流程名称
		String companyName = operationBean.getFlowName();
		// 页眉图片
		Image loginImage = null;
		if (JecnSystemStaticData.isShowLogoIcon()) {
			loginImage = RtfCommon.getImage(JecnFileUtil.getLogoFilePath());
		}
		// 生成页眉
		RtfCommon.returnToFlowHeader(document, operationBean, loginImage);
		// 生成页脚
		RtfCommon.getFooterDoc(document);

		String line = "\n\n\n\n";
		document.add(new Paragraph(line));
		// 公司名称
		document.add(RtfCommon.ST_X3_MIDDLE(JecnSystemStaticData.getCompName()));

		String newline = "\n\n\n\n";
		document.add(new Paragraph(newline));

		// 流程名称
		document.add(RtfCommon.ST_X2_BOLD_MIDDLE(companyName));
		String nameLine = "\n\n\n";
		document.add(new Paragraph(nameLine));
		// 生成首页文控信息
		createPage(document);

		int count = 1;
		for (int i = 0; i < operationDescriptionConfigBeanList.size(); i++) {
			DrawOperationDescriptionConfigBean operationDescriptionConfigBean = operationDescriptionConfigBeanList
					.get(i);
			if (operationDescriptionConfigBean.getIsShow() == 0) {// 是否显示
				continue;
			}
			String titleName = count + "、" + operationDescriptionConfigBean.getName();
			if ("a1".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 1、目的
				Paragraph title1 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title1);
				String flowPurpose = "";
				if (operationBean.getFlowPurpose() != null && !"".equals(operationBean.getFlowPurpose())) {
					flowPurpose = operationBean.getFlowPurpose();
				} else {
					flowPurpose = "\t" + none;
				}
				document.add(getTitle(flowPurpose));
				document.add(new Paragraph());
				count++;
			} else if ("a2".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 2、适用范围
				Paragraph title2 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title2);
				String applicability = "";
				if (operationBean.getApplicability() != null && !"".equals(operationBean.getApplicability())) {
					applicability = operationBean.getApplicability();
				} else {
					applicability = "\t" + none;
				}
				document.add(getTitle(applicability));
				document.add(new Paragraph());
				count++;
			} else if ("a3".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 3、术语定义
				Paragraph title3 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title3);
				String nuotglOssary = "";
				if (operationBean.getNoutGlossary() != null && !"".equals(operationBean.getNoutGlossary())) {
					nuotglOssary = operationBean.getNoutGlossary();
				} else {
					nuotglOssary = "\t" + none;
				}
				document.add(getTitle(nuotglOssary));
				document.add(new Paragraph());
				count++;
			} else if ("a4".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {// 驱动规则
				// 4流程驱动规则
				Paragraph title8 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title8);
				Paragraph content = new Paragraph("\t" + none, new RtfFont("Arial", 12));
				document.add(content);
				document.add(new Paragraph());
				count++;
			} else if ("a5".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {// 流程输入
				// 5、输入
				Paragraph title4 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title4);
				String flowInput = "";
				if (operationBean.getFlowInput() != null && !"".equals(operationBean.getFlowInput())) {
					flowInput = operationBean.getFlowInput();
				} else {
					flowInput = "\t" + none;
				}
				document.add(getTitle(flowInput));
				document.add(new Paragraph());
				count++;
			} else if ("a6".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 6、输出
				Paragraph title5 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title5);
				String flowOutput = "";
				if (operationBean.getFlowOutput() != null && !"".equals(operationBean.getFlowOutput())) {
					flowOutput = operationBean.getFlowOutput();
				} else {
					flowOutput = "\t" + none;
				}
				document.add(getTitle(flowOutput));
				document.add(new Paragraph());
				count++;
			} else if ("a7".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) { // 7关键活动
				Paragraph title6 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				// Paragraph title6 = new Paragraph(formatSYMBOL_xiaosi("7
				// 关键活动"));//
				// operationBean.listPAActivityShow;
				int paSize = operationBean.getListPAActivityShow().size();
				int ksfSize = operationBean.getListKSFActivityShow().size();
				int kcpSize = operationBean.getListKCPActivityShow().size();
				if (paSize > 0 || ksfSize > 0 || kcpSize > 0) {
					Table table6 = null;
					table6 = new Table(5);
					table6.setWidths(new int[] { 15, 15, 25, 30, 20 });
					table6.setAlignment(Table.ALIGN_LEFT);
					table6.setWidth(100f);

					// 关键活动
					Cell cell5_00 = new Cell(RtfCommon.ST_5_BOLD_MIDDLE(keyActivities));
					cell5_00.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_00, 0, 0);
					// 活动编号
					Cell cell5_01 = new Cell(RtfCommon.ST_5_BOLD_MIDDLE(activitynumbers));
					cell5_01.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_01, 0, 1);
					// 活动名称
					Cell cell5_02 = new Cell(RtfCommon.ST_5_BOLD_MIDDLE(activityTitle));
					cell5_02.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_02, 0, 2);
					// 活动说明
					Cell cell5_03 = new Cell(RtfCommon.ST_5_BOLD_MIDDLE(activityIndicatingThat));
					cell5_03.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_03, 0, 3);
					// 关键说明
					Cell cell5_04 = new Cell(RtfCommon.ST_5_BOLD_MIDDLE(keyShow));
					cell5_04.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_04, 0, 4);
					// 问题区域
					Cell cell5_10 = new Cell(RtfCommon.ST_5_BOLD_MIDDLE(problemAreas));
					cell5_10.setHorizontalAlignment(Element.ALIGN_CENTER);
					table6.addCell(cell5_10, 1, 0);
					// 关键成功因素
					Cell cell5_20 = new Cell(RtfCommon.ST_5_BOLD_MIDDLE(keySuccessFactors));
					cell5_20.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (operationBean.getListPAActivityShow().size() > 0) {
						table6.addCell(cell5_20, operationBean.getListPAActivityShow().size() + 1, 0);
					}
					if (operationBean.getListPAActivityShow().size() == 0) {
						table6.addCell(cell5_20, 2, 0);
					}
					// 关键控制点
					Cell cell5_30 = new Cell(RtfCommon.ST_5_BOLD_MIDDLE(keyControlPoint));
					cell5_30.setHorizontalAlignment(Element.ALIGN_CENTER);
					// 设置关键控制点标题
					if (operationBean.getListPAActivityShow().size() > 0
							&& operationBean.getListKSFActivityShow().size() > 0) {
						table6.addCell(cell5_30, operationBean.getListPAActivityShow().size()
								+ operationBean.getListKSFActivityShow().size() + 1, 0);
					} else if (operationBean.getListPAActivityShow().size() > 0) {
						table6.addCell(cell5_30, operationBean.getListPAActivityShow().size() + 2, 0);
					} else if (operationBean.getListKSFActivityShow().size() > 0) {
						table6.addCell(cell5_30, operationBean.getListKSFActivityShow().size() + 2, 0);
					} else if (operationBean.getListPAActivityShow().size() == 0
							&& operationBean.getListKSFActivityShow().size() == 0) {
						table6.addCell(cell5_30, 3, 0);
					}

					int table6i = 1;
					for (JecnActivityShowBean activeBean : operationBean.getListPAActivityShow()) {// 问题区域
						String activityShow = changeNetToWordStyle(activeBean.getActiveFigure().getFlowElementData()
								.getAvtivityShow());
						String activityShowControl = changeNetToWordStyle(activeBean.getActiveFigure()
								.getFlowElementData().getAvtivityShowAndControl());
						table6.addCell(activeBean.getActiveFigure().getFlowElementData().getActivityNum(), new Point(
								table6i, 1));
						table6.addCell(activeBean.getActiveFigure().getFlowElementData().getFigureText(), new Point(
								table6i, 2));
						table6.addCell(activityShow, new Point(table6i, 3));

						table6.addCell(activityShowControl, new Point(table6i, 4));

						table6i++;
					}
					if (operationBean.getListPAActivityShow() == null
							|| operationBean.getListPAActivityShow().size() == 0) {
						table6i++;
					}
					for (JecnActivityShowBean activeBean : operationBean.getListKSFActivityShow()) {
						String activityShow = changeNetToWordStyle(activeBean.getActiveFigure().getFlowElementData()
								.getAvtivityShow());
						String activityShowControl = changeNetToWordStyle(activeBean.getActiveFigure()
								.getFlowElementData().getAvtivityShowAndControl());
						table6.addCell(activeBean.getActiveFigure().getFlowElementData().getActivityNum(), new Point(
								table6i, 1));
						table6.addCell(activeBean.getActiveFigure().getFlowElementData().getFigureText(), new Point(
								table6i, 2));
						table6.addCell(activityShow, new Point(table6i, 3));

						table6.addCell(activityShowControl, new Point(table6i, 4));
						table6i++;
					}
					if (operationBean.getListKSFActivityShow() == null
							|| operationBean.getListKSFActivityShow().size() == 0) {
						table6i++;
					}
					for (JecnActivityShowBean activeBean : operationBean.getListKCPActivityShow()) {
						String activityShow = changeNetToWordStyle(activeBean.getActiveFigure().getFlowElementData()
								.getAvtivityShow());
						String activityShowControl = changeNetToWordStyle(activeBean.getActiveFigure()
								.getFlowElementData().getAvtivityShowAndControl());
						table6.addCell(activeBean.getActiveFigure().getFlowElementData().getActivityNum(), new Point(
								table6i, 1));
						table6.addCell(activeBean.getActiveFigure().getFlowElementData().getFigureText(), new Point(
								table6i, 2));
						table6.addCell(activityShow, new Point(table6i, 3));
						table6.addCell(activityShowControl, new Point(table6i, 4));
						table6i++;
					}
					if (operationBean.getListKCPActivityShow() == null
							|| operationBean.getListKCPActivityShow().size() == 0) {
						table6i++;
					}
					if (operationBean.getListPAActivityShow().size() > 1) {
						cell5_10.setRowspan(operationBean.getListPAActivityShow().size());
					}
					if (operationBean.getListKSFActivityShow().size() > 1) {
						cell5_20.setRowspan(operationBean.getListKSFActivityShow().size());
					}
					if (operationBean.getListKCPActivityShow().size() > 1) {
						cell5_30.setRowspan(operationBean.getListKCPActivityShow().size());
					}
					title6.add(table6);
					// document.add(table5);
					document.add(title6);
					document.add(new Paragraph());
				} else {
					document.add(title6);
					Paragraph title11Content = new Paragraph("\t" + none, new RtfFont("Arial", 12));
					document.add(title11Content);
					document.add(new Paragraph());
				}
				count++;
			} else if ("a8".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 8、角色职责
				Paragraph title7 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title7);
				if (operationBean.getBaseRoleFigureList().size() > 0) {
					Table table7 = new Table(2);
					table7.setAlignment(Table.ALIGN_LEFT);
					table7.setWidth(100f);
					table7.setWidths(new int[] { 20, 80 });
					RtfCell cellDotted6 = new RtfCell("Dotted border");
					cellDotted6.setBorders(new RtfBorderGroup(Rectangle.BOX, RtfBorder.BORDER_DOTTED, 1, new Color(0,
							0, 0)));
					// 角色名称
					table7.addCell(RtfCommon.ST_5_BOLD_MIDDLE(roleName), new Point(0, 0));
					// 角色职责
					table7.addCell(RtfCommon.ST_5_BOLD_MIDDLE(roleResponsibility), new Point(0, 1));
					int title7i = 1;
					for (JecnBaseRoleFigure baseRoleFigure : operationBean.getBaseRoleFigureList()) {
						table7.addCell(baseRoleFigure.getFlowElementData().getFigureText(), new Point(title7i, 0));
						table7.addCell(repaceAllInfo(baseRoleFigure.getFlowElementData().getRoleRes()), new Point(
								title7i, 1));
						title7i++;
					}
					document.add(table7);
					document.add(new Paragraph());
				} else {
					Paragraph title11Content = new Paragraph("\t" + none, new RtfFont("Arial", 12));
					document.add(title11Content);
					document.add(new Paragraph());
				}
				count++;
			} else if ("a9".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 9、流程图
				Paragraph title9 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title9);

				// if (img != null) {
				// img.scaleAbsolute(500, 500);
				// document.add(img);
				// }
				if (img != null) {
					// 图像宽度大小
					float imgWidth = img.getWidth();
					if (imgWidth == 0) {
						// 除数不能为零
						imgWidth = 1;
					}
					float docWidth = document.getPageSize().getWidth() - 160;
					int scale = (int) ((docWidth / imgWidth) * 100) + 1;
					// 图像放大缩小倍数
					img.scalePercent(scale);
					document.add(img);
					document.add(new Paragraph());
				}
				count++;
			} else if ("a10".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				Paragraph title10 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title10);
				if (operationBean.getListAllActivityShow().size() > 0) {
					Table table10 = null;
					if (JecnSystemStaticData.isShowActiveIO()) {
						table10 = new Table(6);
						table10.setWidths(new int[] { 15, 15, 15, 25, 15, 15 });
					} else {
						table10 = new Table(4);
						table10.setWidths(new int[] { 15, 25, 25, 40 });
					}
					table10.setWidth(100f);
					RtfCell cellDotted10 = new RtfCell("Dotted border");
					cellDotted10.setBorders(new RtfBorderGroup(Rectangle.BOX, RtfBorder.BORDER_DOTTED, 1, new Color(0,
							0, 0)));
					// 活动编号
					table10.addCell(RtfCommon.ST_5_BOLD_MIDDLE(activitynumbers), new Point(0, 0));
					// 执行角色
					table10.addCell(RtfCommon.ST_5_BOLD_MIDDLE(executiveRole), new Point(0, 1));
					// 活动名称
					table10.addCell(RtfCommon.ST_5_BOLD_MIDDLE(activityTitle), new Point(0, 2));
					// 活动说明
					table10.addCell(RtfCommon.ST_5_BOLD_MIDDLE(activityIndicatingThat), new Point(0, 3));
					if (JecnSystemStaticData.isShowActiveIO()) {
						// 输入
						table10.addCell(RtfCommon.ST_5_BOLD_MIDDLE(activeInput), new Point(0, 4));
						// 输出
						table10.addCell(RtfCommon.ST_5_BOLD_MIDDLE(activeOutput), new Point(0, 5));
					}
					int title10i = 1;
					for (JecnActivityShowBean activityShowBean : operationBean.getListAllActivityShow()) {
						table10.addCell(activityShowBean.getActiveFigure().getFlowElementData().getActivityNum(),
								new Point(title10i, 0));
						table10.addCell(activityShowBean.getRoleName(), new Point(title10i, 1));
						table10.addCell(activityShowBean.getActiveFigure().getFlowElementData().getFigureText(),
								new Point(title10i, 2));
						table10.addCell(changeNetToWordStyle(activityShowBean.getActiveFigure().getFlowElementData()
								.getAvtivityShow()), new Point(title10i, 3));
						if (JecnSystemStaticData.isShowActiveIO()) {
							table10.addCell(changeNetToWordStyle(activityShowBean.getActiveFigure()
									.getFlowElementData().getActiveInput()), new Point(title10i, 4));
							table10.addCell(changeNetToWordStyle(activityShowBean.getActiveFigure()
									.getFlowElementData().getActiveOutPut()), new Point(title10i, 5));
						}
						title10i++;
					}
					document.add(table10);
					document.add(new Paragraph());
				} else {
					Paragraph title11Content = new Paragraph("\t" + none, new RtfFont("Arial", 12));
					document.add(title11Content);
					document.add(new Paragraph());
				}
				count++;
			} else if ("a11".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 11、流程记录
				Paragraph title11 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title11);
				String flowFile = "";
				if (operationBean.getFlowFileStr() != null && !"".equals(operationBean.getFlowFileStr())) {
					flowFile = operationBean.getFlowFileStr();
				} else {
					flowFile = "\t" + none;
				}
				document.add(getTitle(flowFile));
				document.add(new Paragraph());
				count++;
			} else if ("a12".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 12、操作模板
				Paragraph title12 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title12);
				String practices = "";
				if (operationBean.getPracticesStr() != null && !"".equals(operationBean.getPracticesStr())) {
					practices = operationBean.getPracticesStr();
				} else {
					practices = "\t" + none;
				}
				document.add(getTitle(practices));
				document.add(new Paragraph());
				count++;
			} else if ("a13".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 13、相关流程
				Paragraph title13 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title13);
				String relatedFlow = "";
				if (operationBean.getRelatedFlowStr() != null && !"".equals(operationBean.getRelatedFlowStr())) {
					relatedFlow = operationBean.getRelatedFlowStr();
				} else {
					relatedFlow = "\t" + none;
				}
				document.add(getTitle(relatedFlow));
				document.add(new Paragraph());
				count++;
			} else if ("a14".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 14、相关制度
				Paragraph title14 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title14);
				String relatedRule = "";
				if (operationBean.getRelatedRuleStr() != null && !"".equals(operationBean.getRelatedRuleStr())) {
					relatedRule = operationBean.getRelatedRuleStr();
				} else {
					relatedRule = "\t" + none;
				}
				document.add(getTitle(relatedRule));
				document.add(new Paragraph());
				count++;
			} else if ("a15".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 15、流程关键测评指标板
				Paragraph title15 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title15);
				String keyEvaluationIndi = "";
				if (operationBean.getKeyEvaluationIndiStr() != null
						&& !"".equals(operationBean.getKeyEvaluationIndiStr())) {
					keyEvaluationIndi = operationBean.getKeyEvaluationIndiStr();
				} else {
					keyEvaluationIndi = "\t" + none;
				}
				document.add(getTitle(keyEvaluationIndi));
				document.add(new Paragraph());
				count++;
			} else if ("a16".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 16、客户
				Paragraph title4 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title4);
				String flowCustom = "";
				if (operationBean.getFlowCustom() != null && !"".equals(operationBean.getFlowCustom())) {
					flowCustom = operationBean.getFlowCustom();
				} else {
					flowCustom = "\t" + none;
				}
				document.add(getTitle(flowCustom));
				document.add(new Paragraph());
				count++;
			} else if ("a17".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 17、不适用范围
				Paragraph title17 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title17);
				String noApplicability = "";
				if (operationBean.getNoApplicability() != null && !"".equals(operationBean.getNoApplicability())
						&& !"&nbsp".equals(operationBean.getNoApplicability())) {
					noApplicability = operationBean.getNoApplicability();
				} else {
					noApplicability = "\t" + none;
				}
				document.add(getTitle(noApplicability));
				document.add(new Paragraph());
				count++;
			} else if ("a18".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 18、概况信息
				Paragraph title18 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title18);
				String flowSummarize = "";
				if (operationBean.getFlowSummarize() != null && !"".equals(operationBean.getFlowSummarize())
						&& !"&nbsp".equals(operationBean.getFlowSummarize())) {
					flowSummarize = operationBean.getFlowSummarize();
				} else {
					flowSummarize = "\t" + none;
				}
				document.add(getTitle(flowSummarize));
				document.add(new Paragraph());
				count++;
			} else if ("a19".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				// 19、补充说明
				Paragraph title19 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title19);
				String flowSupplement = "";
				if (operationBean.getFlowSupplement() != null && !"".equals(operationBean.getFlowSupplement())
						&& !"&nbsp".equals(operationBean.getFlowSupplement())) {
					flowSupplement = operationBean.getFlowSupplement();
				} else {
					flowSupplement = "\t" + none;
				}
				document.add(getTitle(flowSupplement));
				document.add(new Paragraph());
				count++;
			} else if ("a20".equals(operationDescriptionConfigBean.getMark())
					&& operationDescriptionConfigBean.getTurn() != 1000) {
				Paragraph title10 = new Paragraph(titleName, RtfParagraphStyle.STYLE_HEADING_3);
				document.add(title10);
				String flowSupplement = "";
				flowSupplement = "\t" + none;
				document.add(getTitle(flowSupplement));
				document.add(new Paragraph());
				count++;
			}
		}
		document.close();
	}

	private static String repaceAllInfo(String title) {
		if (title != null && !title.equals("")) {
			title = title.replaceAll("<br>", "\n");
		}
		return title;
	}

	private static String changeNetToWordStyle(String str) {
		if (str != null && !"".equals(str)) {
			str = str.replaceAll("<br>", "\n");
			str = str.replaceAll("&nbsp;", "");
		}
		return str;
	}

	/**
	 * 生成首页
	 * 
	 * @author fuzhh 2014-1-3
	 * @throws DocumentException
	 */
	public static void createPage(Document document) throws DocumentException {
		Table table = new Table(4);
		table.setWidth(100f);
		table.setWidths(new int[] { 20, 30, 20, 30 });
		table.setBorderWidth(1);
		// 拟稿人
		table.addCell(peopleMakeADraft, new Point(0, 0));
		table.addCell("", new Point(0, 1));
		// 时间
		table.addCell(time, new Point(0, 2));
		table.addCell("", new Point(0, 3));
		// 文控审核人
		table.addCell(documentControlAuditPeople, new Point(1, 0));
		table.addCell("", new Point(1, 1));
		// 时间
		table.addCell(time, new Point(1, 2));
		table.addCell("", new Point(1, 3));
		// 部门审核人
		table.addCell(departmentAuditPeople, new Point(2, 0));
		table.addCell("", new Point(2, 1));
		// 时间
		table.addCell(time, new Point(2, 2));
		table.addCell("", new Point(2, 3));
		// 评审人
		table.addCell(reviewPeople, new Point(3, 0));
		table.addCell("", new Point(3, 1));
		// 时间
		table.addCell(time, new Point(3, 2));
		table.addCell("", new Point(3, 3));
		// 批准人
		table.addCell(forPeople, new Point(4, 0));
		table.addCell("", new Point(4, 1));
		// 时间
		table.addCell(time, new Point(4, 2));
		table.addCell("", new Point(4, 3));
		document.add(table);
		document.add(new Paragraph());

		StringBuffer str = new StringBuffer();
		for (int i = 0; i < 25; i++) {
			str.append("\n");
		}
		document.add(new Paragraph(str.toString()));
	}

	public static Paragraph getTitle(String title) {
		Paragraph title1Content = new Paragraph(changeNetToWordStyle(title), new RtfFont("Arial", 12));
		title1Content.setSpacingBefore(5f);
		title1Content.setSpacingAfter(5f);
		return title1Content;
	}
}
